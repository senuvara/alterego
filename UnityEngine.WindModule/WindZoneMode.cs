﻿using System;

namespace UnityEngine
{
	// Token: 0x02000002 RID: 2
	public enum WindZoneMode
	{
		// Token: 0x04000002 RID: 2
		Directional,
		// Token: 0x04000003 RID: 3
		Spherical
	}
}
