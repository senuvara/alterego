﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000003 RID: 3
	internal interface INativeTizenStore : INativeStore
	{
		// Token: 0x06000005 RID: 5
		void SetUnityPurchasingCallback(UnityNativePurchasingCallback AsyncCallback);

		// Token: 0x06000006 RID: 6
		void SetGroupId(string group);
	}
}
