﻿using System;
using System.Runtime.InteropServices;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000002 RID: 2
	// (Invoke) Token: 0x06000002 RID: 2
	internal delegate void UnityNativePurchasingCallback([MarshalAs(UnmanagedType.LPStr)] [In] string subject, [MarshalAs(UnmanagedType.LPStr)] [In] string payload, [MarshalAs(UnmanagedType.LPStr)] [In] string receipt, [MarshalAs(UnmanagedType.LPStr)] [In] string transactionId);
}
