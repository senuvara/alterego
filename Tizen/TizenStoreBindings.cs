﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000004 RID: 4
	internal class TizenStoreBindings : INativeTizenStore, INativeStore
	{
		// Token: 0x06000007 RID: 7 RVA: 0x00002050 File Offset: 0x00000250
		public void SetGroupId(string group)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000008 RID: 8 RVA: 0x00002050 File Offset: 0x00000250
		public void SetUnityPurchasingCallback(UnityNativePurchasingCallback AsyncCallback)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000009 RID: 9 RVA: 0x00002050 File Offset: 0x00000250
		public void RetrieveProducts(string json)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600000A RID: 10 RVA: 0x00002050 File Offset: 0x00000250
		public void Purchase(string productJSON, string developerPayload)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600000B RID: 11 RVA: 0x00002050 File Offset: 0x00000250
		public void FinishTransaction(string productJSON, string transactionId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600000C RID: 12 RVA: 0x00002050 File Offset: 0x00000250
		public void RestoreTransactions()
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600000D RID: 13 RVA: 0x00002050 File Offset: 0x00000250
		public void RefreshAppReceipt()
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600000E RID: 14 RVA: 0x00002050 File Offset: 0x00000250
		public void AddTransactionObserver()
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600000F RID: 15 RVA: 0x00002058 File Offset: 0x00000258
		public TizenStoreBindings()
		{
		}
	}
}
