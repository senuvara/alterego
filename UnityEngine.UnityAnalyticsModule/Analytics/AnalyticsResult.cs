﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x0200000A RID: 10
	public enum AnalyticsResult
	{
		// Token: 0x0400000F RID: 15
		Ok,
		// Token: 0x04000010 RID: 16
		NotInitialized,
		// Token: 0x04000011 RID: 17
		AnalyticsDisabled,
		// Token: 0x04000012 RID: 18
		TooManyItems,
		// Token: 0x04000013 RID: 19
		SizeLimitReached,
		// Token: 0x04000014 RID: 20
		TooManyRequests,
		// Token: 0x04000015 RID: 21
		InvalidData,
		// Token: 0x04000016 RID: 22
		UnsupportedPlatform
	}
}
