﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;

namespace UnityEngine.Analytics
{
	// Token: 0x0200000B RID: 11
	[NativeHeader("Modules/UnityAnalytics/Public/Events/UserCustomEvent.h")]
	[StructLayout(LayoutKind.Sequential)]
	internal class CustomEventData : IDisposable
	{
		// Token: 0x06000041 RID: 65 RVA: 0x000026DA File Offset: 0x000008DA
		private CustomEventData()
		{
		}

		// Token: 0x06000042 RID: 66 RVA: 0x000026E3 File Offset: 0x000008E3
		public CustomEventData(string name)
		{
			this.m_Ptr = CustomEventData.Internal_Create(this, name);
		}

		// Token: 0x06000043 RID: 67 RVA: 0x000026FC File Offset: 0x000008FC
		~CustomEventData()
		{
			this.Destroy();
		}

		// Token: 0x06000044 RID: 68 RVA: 0x0000272C File Offset: 0x0000092C
		private void Destroy()
		{
			if (this.m_Ptr != IntPtr.Zero)
			{
				CustomEventData.Internal_Destroy(this.m_Ptr);
				this.m_Ptr = IntPtr.Zero;
			}
		}

		// Token: 0x06000045 RID: 69 RVA: 0x0000275C File Offset: 0x0000095C
		public void Dispose()
		{
			this.Destroy();
			GC.SuppressFinalize(this);
		}

		// Token: 0x06000046 RID: 70
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern IntPtr Internal_Create(CustomEventData ced, string name);

		// Token: 0x06000047 RID: 71
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Internal_Destroy(IntPtr ptr);

		// Token: 0x06000048 RID: 72
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool AddString(string key, string value);

		// Token: 0x06000049 RID: 73
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool AddInt32(string key, int value);

		// Token: 0x0600004A RID: 74
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool AddUInt32(string key, uint value);

		// Token: 0x0600004B RID: 75
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool AddInt64(string key, long value);

		// Token: 0x0600004C RID: 76
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool AddUInt64(string key, ulong value);

		// Token: 0x0600004D RID: 77
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool AddBool(string key, bool value);

		// Token: 0x0600004E RID: 78
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool AddDouble(string key, double value);

		// Token: 0x0600004F RID: 79 RVA: 0x0000276C File Offset: 0x0000096C
		public bool AddDictionary(IDictionary<string, object> eventData)
		{
			foreach (KeyValuePair<string, object> keyValuePair in eventData)
			{
				string key = keyValuePair.Key;
				object value = keyValuePair.Value;
				if (value == null)
				{
					this.AddString(key, "null");
				}
				else
				{
					Type type = value.GetType();
					if (type == typeof(string))
					{
						this.AddString(key, (string)value);
					}
					else if (type == typeof(char))
					{
						this.AddString(key, char.ToString((char)value));
					}
					else if (type == typeof(sbyte))
					{
						this.AddInt32(key, (int)((sbyte)value));
					}
					else if (type == typeof(byte))
					{
						this.AddInt32(key, (int)((byte)value));
					}
					else if (type == typeof(short))
					{
						this.AddInt32(key, (int)((short)value));
					}
					else if (type == typeof(ushort))
					{
						this.AddUInt32(key, (uint)((ushort)value));
					}
					else if (type == typeof(int))
					{
						this.AddInt32(key, (int)value);
					}
					else if (type == typeof(uint))
					{
						this.AddUInt32(keyValuePair.Key, (uint)value);
					}
					else if (type == typeof(long))
					{
						this.AddInt64(key, (long)value);
					}
					else if (type == typeof(ulong))
					{
						this.AddUInt64(key, (ulong)value);
					}
					else if (type == typeof(bool))
					{
						this.AddBool(key, (bool)value);
					}
					else if (type == typeof(float))
					{
						this.AddDouble(key, (double)Convert.ToDecimal((float)value));
					}
					else if (type == typeof(double))
					{
						this.AddDouble(key, (double)value);
					}
					else if (type == typeof(decimal))
					{
						this.AddDouble(key, (double)Convert.ToDecimal((decimal)value));
					}
					else
					{
						if (!type.IsValueType)
						{
							throw new ArgumentException(string.Format("Invalid type: {0} passed", type));
						}
						this.AddString(key, value.ToString());
					}
				}
			}
			return true;
		}

		// Token: 0x04000017 RID: 23
		[NonSerialized]
		internal IntPtr m_Ptr;
	}
}
