﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Analytics
{
	// Token: 0x02000004 RID: 4
	[NativeHeader("UnityAnalyticsScriptingClasses.h")]
	[NativeHeader("Modules/UnityAnalytics/CoreStats/AnalyticsCoreStats.h")]
	[RequiredByNativeCode]
	public static class AnalyticsSessionInfo
	{
		// Token: 0x14000001 RID: 1
		// (add) Token: 0x06000008 RID: 8 RVA: 0x0000208C File Offset: 0x0000028C
		// (remove) Token: 0x06000009 RID: 9 RVA: 0x000020C0 File Offset: 0x000002C0
		public static event AnalyticsSessionInfo.SessionStateChanged sessionStateChanged
		{
			add
			{
				AnalyticsSessionInfo.SessionStateChanged sessionStateChanged = AnalyticsSessionInfo.sessionStateChanged;
				AnalyticsSessionInfo.SessionStateChanged sessionStateChanged2;
				do
				{
					sessionStateChanged2 = sessionStateChanged;
					sessionStateChanged = Interlocked.CompareExchange<AnalyticsSessionInfo.SessionStateChanged>(ref AnalyticsSessionInfo.sessionStateChanged, (AnalyticsSessionInfo.SessionStateChanged)Delegate.Combine(sessionStateChanged2, value), sessionStateChanged);
				}
				while (sessionStateChanged != sessionStateChanged2);
			}
			remove
			{
				AnalyticsSessionInfo.SessionStateChanged sessionStateChanged = AnalyticsSessionInfo.sessionStateChanged;
				AnalyticsSessionInfo.SessionStateChanged sessionStateChanged2;
				do
				{
					sessionStateChanged2 = sessionStateChanged;
					sessionStateChanged = Interlocked.CompareExchange<AnalyticsSessionInfo.SessionStateChanged>(ref AnalyticsSessionInfo.sessionStateChanged, (AnalyticsSessionInfo.SessionStateChanged)Delegate.Remove(sessionStateChanged2, value), sessionStateChanged);
				}
				while (sessionStateChanged != sessionStateChanged2);
			}
		}

		// Token: 0x0600000A RID: 10 RVA: 0x000020F4 File Offset: 0x000002F4
		[RequiredByNativeCode]
		internal static void CallSessionStateChanged(AnalyticsSessionState sessionState, long sessionId, long sessionElapsedTime, bool sessionChanged)
		{
			AnalyticsSessionInfo.SessionStateChanged sessionStateChanged = AnalyticsSessionInfo.sessionStateChanged;
			if (sessionStateChanged != null)
			{
				sessionStateChanged(sessionState, sessionId, sessionElapsedTime, sessionChanged);
			}
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x0600000B RID: 11
		public static extern AnalyticsSessionState sessionState { [NativeMethod("GetPlayerSessionState")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x0600000C RID: 12
		public static extern long sessionId { [NativeMethod("GetPlayerSessionId")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x0600000D RID: 13
		public static extern long sessionCount { [NativeMethod("GetPlayerSessionCount")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x0600000E RID: 14
		public static extern long sessionElapsedTime { [NativeMethod("GetPlayerSessionElapsedTime")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x0600000F RID: 15
		public static extern bool sessionFirstRun { [NativeMethod("GetPlayerSessionFirstRun", false, true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000010 RID: 16
		public static extern string userId { [NativeMethod("GetUserId")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x04000006 RID: 6
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private static AnalyticsSessionInfo.SessionStateChanged sessionStateChanged;

		// Token: 0x02000005 RID: 5
		// (Invoke) Token: 0x06000012 RID: 18
		public delegate void SessionStateChanged(AnalyticsSessionState sessionState, long sessionId, long sessionElapsedTime, bool sessionChanged);
	}
}
