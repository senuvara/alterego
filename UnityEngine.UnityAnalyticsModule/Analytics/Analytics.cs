﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;

namespace UnityEngine.Analytics
{
	// Token: 0x02000006 RID: 6
	[NativeHeader("Modules/UnityAnalytics/Public/Events/UserCustomEvent.h")]
	[NativeHeader("Modules/UnityAnalytics/Public/UnityAnalytics.h")]
	[StructLayout(LayoutKind.Sequential)]
	public static class Analytics
	{
		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000015 RID: 21 RVA: 0x00002118 File Offset: 0x00000318
		// (set) Token: 0x06000016 RID: 22 RVA: 0x00002143 File Offset: 0x00000343
		public static bool initializeOnStartup
		{
			get
			{
				return Analytics.IsInitialized() && Analytics.initializeOnStartupInternal;
			}
			set
			{
				if (Analytics.IsInitialized())
				{
					Analytics.initializeOnStartupInternal = value;
				}
			}
		}

		// Token: 0x06000017 RID: 23 RVA: 0x00002158 File Offset: 0x00000358
		public static AnalyticsResult ResumeInitialization()
		{
			AnalyticsResult result;
			if (!Analytics.IsInitialized())
			{
				result = AnalyticsResult.NotInitialized;
			}
			else
			{
				result = Analytics.ResumeInitializationInternal();
			}
			return result;
		}

		// Token: 0x06000018 RID: 24
		[StaticAccessor("GetUnityAnalytics()", StaticAccessorType.Dot)]
		[NativeMethod("ResumeInitialization")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AnalyticsResult ResumeInitializationInternal();

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000019 RID: 25
		// (set) Token: 0x0600001A RID: 26
		[StaticAccessor("GetUnityAnalytics()", StaticAccessorType.Dot)]
		private static extern bool initializeOnStartupInternal { [NativeMethod("GetInitializeOnStartup")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeMethod("SetInitializeOnStartup")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600001B RID: 27
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsInitialized();

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x0600001C RID: 28
		// (set) Token: 0x0600001D RID: 29
		[StaticAccessor("GetUnityAnalytics()", StaticAccessorType.Dot)]
		private static extern bool enabledInternal { [NativeMethod("GetEnabled")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeMethod("SetEnabled")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x0600001E RID: 30
		[StaticAccessor("GetUnityAnalytics()", StaticAccessorType.Dot)]
		private static extern bool playerOptedOutInternal { [NativeMethod("GetPlayerOptedOut")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x0600001F RID: 31
		// (set) Token: 0x06000020 RID: 32
		[StaticAccessor("GetUnityAnalytics()", StaticAccessorType.Dot)]
		private static extern bool limitUserTrackingInternal { [NativeMethod("GetLimitUserTracking")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeMethod("SetLimitUserTracking")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000021 RID: 33
		// (set) Token: 0x06000022 RID: 34
		[StaticAccessor("GetUnityAnalytics()", StaticAccessorType.Dot)]
		private static extern bool deviceStatsEnabledInternal { [NativeMethod("GetDeviceStatsEnabled")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeMethod("SetDeviceStatsEnabled")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000023 RID: 35
		[NativeMethod("FlushEvents")]
		[StaticAccessor("GetUnityAnalytics()", StaticAccessorType.Dot)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool FlushArchivedEvents();

		// Token: 0x06000024 RID: 36
		[StaticAccessor("GetUnityAnalytics()", StaticAccessorType.Dot)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AnalyticsResult Transaction(string productId, double amount, string currency, string receiptPurchaseData, string signature, bool usingIAPService);

		// Token: 0x06000025 RID: 37
		[StaticAccessor("GetUnityAnalytics()", StaticAccessorType.Dot)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AnalyticsResult SendCustomEventName(string customEventName);

		// Token: 0x06000026 RID: 38
		[StaticAccessor("GetUnityAnalytics()", StaticAccessorType.Dot)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AnalyticsResult SendCustomEvent(CustomEventData eventData);

		// Token: 0x06000027 RID: 39
		[StaticAccessor("GetUnityAnalytics()", StaticAccessorType.Dot)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern AnalyticsResult RegisterEventWithLimit(string eventName, int maxEventPerHour, int maxItems, string vendorKey, int ver, string prefix, string assemblyInfo, bool notifyServer);

		// Token: 0x06000028 RID: 40
		[StaticAccessor("GetUnityAnalytics()", StaticAccessorType.Dot)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern AnalyticsResult RegisterEventsWithLimit(string[] eventName, int maxEventPerHour, int maxItems, string vendorKey, int ver, string prefix, string assemblyInfo, bool notifyServer);

		// Token: 0x06000029 RID: 41
		[StaticAccessor("GetUnityAnalytics()", StaticAccessorType.Dot)]
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern AnalyticsResult SendEventWithLimit(string eventName, object parameters, int ver, string prefix);

		// Token: 0x0600002A RID: 42
		[StaticAccessor("GetUnityAnalytics()", StaticAccessorType.Dot)]
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool QueueEvent(string eventName, object parameters, int ver, string prefix);

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600002B RID: 43 RVA: 0x00002184 File Offset: 0x00000384
		public static bool playerOptedOut
		{
			get
			{
				return Analytics.IsInitialized() && Analytics.playerOptedOutInternal;
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x0600002C RID: 44 RVA: 0x000021B0 File Offset: 0x000003B0
		// (set) Token: 0x0600002D RID: 45 RVA: 0x000021DB File Offset: 0x000003DB
		public static bool limitUserTracking
		{
			get
			{
				return Analytics.IsInitialized() && Analytics.limitUserTrackingInternal;
			}
			set
			{
				if (Analytics.IsInitialized())
				{
					Analytics.limitUserTrackingInternal = value;
				}
			}
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x0600002E RID: 46 RVA: 0x000021F0 File Offset: 0x000003F0
		// (set) Token: 0x0600002F RID: 47 RVA: 0x0000221B File Offset: 0x0000041B
		public static bool deviceStatsEnabled
		{
			get
			{
				return Analytics.IsInitialized() && Analytics.deviceStatsEnabledInternal;
			}
			set
			{
				if (Analytics.IsInitialized())
				{
					Analytics.deviceStatsEnabledInternal = value;
				}
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000030 RID: 48 RVA: 0x00002230 File Offset: 0x00000430
		// (set) Token: 0x06000031 RID: 49 RVA: 0x0000225B File Offset: 0x0000045B
		public static bool enabled
		{
			get
			{
				return Analytics.IsInitialized() && Analytics.enabledInternal;
			}
			set
			{
				if (Analytics.IsInitialized())
				{
					Analytics.enabledInternal = value;
				}
			}
		}

		// Token: 0x06000032 RID: 50 RVA: 0x00002270 File Offset: 0x00000470
		public static AnalyticsResult FlushEvents()
		{
			AnalyticsResult result;
			if (!Analytics.IsInitialized())
			{
				result = AnalyticsResult.NotInitialized;
			}
			else
			{
				result = ((!Analytics.FlushArchivedEvents()) ? AnalyticsResult.NotInitialized : AnalyticsResult.Ok);
			}
			return result;
		}

		// Token: 0x06000033 RID: 51 RVA: 0x000022A8 File Offset: 0x000004A8
		public static AnalyticsResult SetUserId(string userId)
		{
			if (string.IsNullOrEmpty(userId))
			{
				throw new ArgumentException("Cannot set userId to an empty or null string");
			}
			return Analytics.SendUserInfoEvent(new Analytics.UserInfo
			{
				custom_userid = userId
			});
		}

		// Token: 0x06000034 RID: 52 RVA: 0x000022F0 File Offset: 0x000004F0
		public static AnalyticsResult SetUserGender(Gender gender)
		{
			return Analytics.SendUserInfoEvent(new Analytics.UserInfo
			{
				sex = ((gender != Gender.Male) ? ((gender != Gender.Female) ? "U" : "F") : "M")
			});
		}

		// Token: 0x06000035 RID: 53 RVA: 0x00002348 File Offset: 0x00000548
		public static AnalyticsResult SetUserBirthYear(int birthYear)
		{
			return Analytics.SendUserInfoEvent(new Analytics.UserInfoBirthYear
			{
				birth_year = birthYear
			});
		}

		// Token: 0x06000036 RID: 54 RVA: 0x00002378 File Offset: 0x00000578
		private static AnalyticsResult SendUserInfoEvent(object param)
		{
			AnalyticsResult result;
			if (!Analytics.IsInitialized())
			{
				result = AnalyticsResult.NotInitialized;
			}
			else
			{
				Analytics.QueueEvent("userInfo", param, 1, string.Empty);
				result = AnalyticsResult.Ok;
			}
			return result;
		}

		// Token: 0x06000037 RID: 55 RVA: 0x000023B4 File Offset: 0x000005B4
		public static AnalyticsResult Transaction(string productId, decimal amount, string currency)
		{
			return Analytics.Transaction(productId, amount, currency, null, null, false);
		}

		// Token: 0x06000038 RID: 56 RVA: 0x000023D4 File Offset: 0x000005D4
		public static AnalyticsResult Transaction(string productId, decimal amount, string currency, string receiptPurchaseData, string signature)
		{
			return Analytics.Transaction(productId, amount, currency, receiptPurchaseData, signature, false);
		}

		// Token: 0x06000039 RID: 57 RVA: 0x000023F8 File Offset: 0x000005F8
		public static AnalyticsResult Transaction(string productId, decimal amount, string currency, string receiptPurchaseData, string signature, bool usingIAPService)
		{
			if (string.IsNullOrEmpty(productId))
			{
				throw new ArgumentException("Cannot set productId to an empty or null string");
			}
			if (string.IsNullOrEmpty(currency))
			{
				throw new ArgumentException("Cannot set currency to an empty or null string");
			}
			AnalyticsResult result;
			if (!Analytics.IsInitialized())
			{
				result = AnalyticsResult.NotInitialized;
			}
			else
			{
				if (receiptPurchaseData == null)
				{
					receiptPurchaseData = string.Empty;
				}
				if (signature == null)
				{
					signature = string.Empty;
				}
				result = Analytics.Transaction(productId, Convert.ToDouble(amount), currency, receiptPurchaseData, signature, usingIAPService);
			}
			return result;
		}

		// Token: 0x0600003A RID: 58 RVA: 0x00002478 File Offset: 0x00000678
		public static AnalyticsResult CustomEvent(string customEventName)
		{
			if (string.IsNullOrEmpty(customEventName))
			{
				throw new ArgumentException("Cannot set custom event name to an empty or null string");
			}
			AnalyticsResult result;
			if (!Analytics.IsInitialized())
			{
				result = AnalyticsResult.NotInitialized;
			}
			else
			{
				result = Analytics.SendCustomEventName(customEventName);
			}
			return result;
		}

		// Token: 0x0600003B RID: 59 RVA: 0x000024BC File Offset: 0x000006BC
		public static AnalyticsResult CustomEvent(string customEventName, Vector3 position)
		{
			if (string.IsNullOrEmpty(customEventName))
			{
				throw new ArgumentException("Cannot set custom event name to an empty or null string");
			}
			AnalyticsResult result;
			if (!Analytics.IsInitialized())
			{
				result = AnalyticsResult.NotInitialized;
			}
			else
			{
				CustomEventData customEventData = new CustomEventData(customEventName);
				customEventData.AddDouble("x", (double)Convert.ToDecimal(position.x));
				customEventData.AddDouble("y", (double)Convert.ToDecimal(position.y));
				customEventData.AddDouble("z", (double)Convert.ToDecimal(position.z));
				AnalyticsResult analyticsResult = Analytics.SendCustomEvent(customEventData);
				customEventData.Dispose();
				result = analyticsResult;
			}
			return result;
		}

		// Token: 0x0600003C RID: 60 RVA: 0x00002564 File Offset: 0x00000764
		public static AnalyticsResult CustomEvent(string customEventName, IDictionary<string, object> eventData)
		{
			if (string.IsNullOrEmpty(customEventName))
			{
				throw new ArgumentException("Cannot set custom event name to an empty or null string");
			}
			AnalyticsResult result;
			if (!Analytics.IsInitialized())
			{
				result = AnalyticsResult.NotInitialized;
			}
			else if (eventData == null)
			{
				result = Analytics.SendCustomEventName(customEventName);
			}
			else
			{
				CustomEventData customEventData = new CustomEventData(customEventName);
				customEventData.AddDictionary(eventData);
				AnalyticsResult analyticsResult = Analytics.SendCustomEvent(customEventData);
				customEventData.Dispose();
				result = analyticsResult;
			}
			return result;
		}

		// Token: 0x0600003D RID: 61 RVA: 0x000025D0 File Offset: 0x000007D0
		[MethodImpl(MethodImplOptions.NoInlining)]
		public static AnalyticsResult RegisterEvent(string eventName, int maxEventPerHour, int maxItems, string vendorKey = "", string prefix = "")
		{
			string assemblyInfo = string.Empty;
			assemblyInfo = Assembly.GetCallingAssembly().FullName;
			return Analytics.RegisterEvent(eventName, maxEventPerHour, maxItems, vendorKey, 1, prefix, assemblyInfo);
		}

		// Token: 0x0600003E RID: 62 RVA: 0x00002604 File Offset: 0x00000804
		[MethodImpl(MethodImplOptions.NoInlining)]
		public static AnalyticsResult RegisterEvent(string eventName, int maxEventPerHour, int maxItems, string vendorKey, int ver, string prefix = "")
		{
			string assemblyInfo = string.Empty;
			assemblyInfo = Assembly.GetCallingAssembly().FullName;
			return Analytics.RegisterEvent(eventName, maxEventPerHour, maxItems, vendorKey, ver, prefix, assemblyInfo);
		}

		// Token: 0x0600003F RID: 63 RVA: 0x00002638 File Offset: 0x00000838
		private static AnalyticsResult RegisterEvent(string eventName, int maxEventPerHour, int maxItems, string vendorKey, int ver, string prefix, string assemblyInfo)
		{
			if (string.IsNullOrEmpty(eventName))
			{
				throw new ArgumentException("Cannot set event name to an empty or null string");
			}
			AnalyticsResult result;
			if (!Analytics.IsInitialized())
			{
				result = AnalyticsResult.NotInitialized;
			}
			else
			{
				result = Analytics.RegisterEventWithLimit(eventName, maxEventPerHour, maxItems, vendorKey, ver, prefix, assemblyInfo, true);
			}
			return result;
		}

		// Token: 0x06000040 RID: 64 RVA: 0x00002684 File Offset: 0x00000884
		public static AnalyticsResult SendEvent(string eventName, object parameters, int ver = 1, string prefix = "")
		{
			if (string.IsNullOrEmpty(eventName))
			{
				throw new ArgumentException("Cannot set event name to an empty or null string");
			}
			if (parameters == null)
			{
				throw new ArgumentException("Cannot set parameters to null");
			}
			AnalyticsResult result;
			if (!Analytics.IsInitialized())
			{
				result = AnalyticsResult.NotInitialized;
			}
			else
			{
				result = Analytics.SendEventWithLimit(eventName, parameters, ver, prefix);
			}
			return result;
		}

		// Token: 0x02000007 RID: 7
		[Serializable]
		private struct UserInfo
		{
			// Token: 0x04000007 RID: 7
			public string custom_userid;

			// Token: 0x04000008 RID: 8
			public string sex;
		}

		// Token: 0x02000008 RID: 8
		[Serializable]
		private struct UserInfoBirthYear
		{
			// Token: 0x04000009 RID: 9
			public int birth_year;
		}
	}
}
