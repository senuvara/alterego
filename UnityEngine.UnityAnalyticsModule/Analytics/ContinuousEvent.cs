﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Analytics
{
	// Token: 0x02000002 RID: 2
	[RequiredByNativeCode]
	[NativeHeader("Modules/UnityAnalytics/CoreStats/AnalyticsCoreStats.h")]
	[NativeHeader("Modules/UnityAnalytics/ContinuousEvent/Manager.h")]
	internal class ContinuousEvent
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public ContinuousEvent()
		{
		}

		// Token: 0x06000002 RID: 2 RVA: 0x00002058 File Offset: 0x00000258
		public static void RegisterCollector<T>(string collectorName, Func<T> del) where T : struct, IComparable<T>, IEquatable<T>
		{
			ContinuousEvent.RegisterCollector_Internal(typeof(T).ToString(), collectorName, del);
		}

		// Token: 0x06000003 RID: 3 RVA: 0x00002071 File Offset: 0x00000271
		public static void SetEventHistogramThresholds<T>(string eventName, int count, T[] data) where T : struct, IComparable<T>, IEquatable<T>
		{
			ContinuousEvent.SetEventHistogramThresholds_Internal(typeof(T).ToString(), eventName, count, data);
		}

		// Token: 0x06000004 RID: 4
		[StaticAccessor("::GetAnalyticsCoreStats().GetContinuousEventManager()", StaticAccessorType.Dot)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void RegisterCollector_Internal(string type, string collectorName, object collector);

		// Token: 0x06000005 RID: 5
		[StaticAccessor("::GetAnalyticsCoreStats().GetContinuousEventManager()", StaticAccessorType.Dot)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetEventHistogramThresholds_Internal(string type, string eventName, int count, object data);

		// Token: 0x06000006 RID: 6
		[StaticAccessor("::GetAnalyticsCoreStats().GetContinuousEventManager()", StaticAccessorType.Dot)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void EnableEvent(string eventName, bool enabled);

		// Token: 0x06000007 RID: 7
		[StaticAccessor("::GetAnalyticsCoreStats().GetContinuousEventManager()", StaticAccessorType.Dot)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void ConfigureEvent(string eventName, string collectorName, float interval, float period, bool enabled, bool custom = false);
	}
}
