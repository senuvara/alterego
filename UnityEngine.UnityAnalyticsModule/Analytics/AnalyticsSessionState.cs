﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Analytics
{
	// Token: 0x02000003 RID: 3
	[RequiredByNativeCode]
	public enum AnalyticsSessionState
	{
		// Token: 0x04000002 RID: 2
		kSessionStopped,
		// Token: 0x04000003 RID: 3
		kSessionStarted,
		// Token: 0x04000004 RID: 4
		kSessionPaused,
		// Token: 0x04000005 RID: 5
		kSessionResumed
	}
}
