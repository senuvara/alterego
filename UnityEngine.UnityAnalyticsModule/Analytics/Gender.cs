﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x02000009 RID: 9
	public enum Gender
	{
		// Token: 0x0400000B RID: 11
		Male,
		// Token: 0x0400000C RID: 12
		Female,
		// Token: 0x0400000D RID: 13
		Unknown
	}
}
