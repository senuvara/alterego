﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Bindings;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200000C RID: 12
	[NativeHeader("UnityAnalyticsScriptingClasses.h")]
	[NativeHeader("Modules/UnityAnalytics/RemoteSettings/RemoteSettings.h")]
	public static class RemoteSettings
	{
		// Token: 0x14000002 RID: 2
		// (add) Token: 0x06000050 RID: 80 RVA: 0x00002A50 File Offset: 0x00000C50
		// (remove) Token: 0x06000051 RID: 81 RVA: 0x00002A84 File Offset: 0x00000C84
		public static event RemoteSettings.UpdatedEventHandler Updated
		{
			add
			{
				RemoteSettings.UpdatedEventHandler updatedEventHandler = RemoteSettings.Updated;
				RemoteSettings.UpdatedEventHandler updatedEventHandler2;
				do
				{
					updatedEventHandler2 = updatedEventHandler;
					updatedEventHandler = Interlocked.CompareExchange<RemoteSettings.UpdatedEventHandler>(ref RemoteSettings.Updated, (RemoteSettings.UpdatedEventHandler)Delegate.Combine(updatedEventHandler2, value), updatedEventHandler);
				}
				while (updatedEventHandler != updatedEventHandler2);
			}
			remove
			{
				RemoteSettings.UpdatedEventHandler updatedEventHandler = RemoteSettings.Updated;
				RemoteSettings.UpdatedEventHandler updatedEventHandler2;
				do
				{
					updatedEventHandler2 = updatedEventHandler;
					updatedEventHandler = Interlocked.CompareExchange<RemoteSettings.UpdatedEventHandler>(ref RemoteSettings.Updated, (RemoteSettings.UpdatedEventHandler)Delegate.Remove(updatedEventHandler2, value), updatedEventHandler);
				}
				while (updatedEventHandler != updatedEventHandler2);
			}
		}

		// Token: 0x14000003 RID: 3
		// (add) Token: 0x06000052 RID: 82 RVA: 0x00002AB8 File Offset: 0x00000CB8
		// (remove) Token: 0x06000053 RID: 83 RVA: 0x00002AEC File Offset: 0x00000CEC
		public static event Action BeforeFetchFromServer
		{
			add
			{
				Action action = RemoteSettings.BeforeFetchFromServer;
				Action action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action>(ref RemoteSettings.BeforeFetchFromServer, (Action)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action action = RemoteSettings.BeforeFetchFromServer;
				Action action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action>(ref RemoteSettings.BeforeFetchFromServer, (Action)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x14000004 RID: 4
		// (add) Token: 0x06000054 RID: 84 RVA: 0x00002B20 File Offset: 0x00000D20
		// (remove) Token: 0x06000055 RID: 85 RVA: 0x00002B54 File Offset: 0x00000D54
		public static event Action<bool, bool, int> Completed
		{
			add
			{
				Action<bool, bool, int> action = RemoteSettings.Completed;
				Action<bool, bool, int> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<bool, bool, int>>(ref RemoteSettings.Completed, (Action<bool, bool, int>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<bool, bool, int> action = RemoteSettings.Completed;
				Action<bool, bool, int> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<bool, bool, int>>(ref RemoteSettings.Completed, (Action<bool, bool, int>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x06000056 RID: 86 RVA: 0x00002B88 File Offset: 0x00000D88
		[RequiredByNativeCode]
		internal static void RemoteSettingsUpdated(bool wasLastUpdatedFromServer)
		{
			RemoteSettings.UpdatedEventHandler updated = RemoteSettings.Updated;
			if (updated != null)
			{
				updated();
			}
		}

		// Token: 0x06000057 RID: 87 RVA: 0x00002BA8 File Offset: 0x00000DA8
		[RequiredByNativeCode]
		internal static void RemoteSettingsBeforeFetchFromServer()
		{
			Action beforeFetchFromServer = RemoteSettings.BeforeFetchFromServer;
			if (beforeFetchFromServer != null)
			{
				beforeFetchFromServer();
			}
		}

		// Token: 0x06000058 RID: 88 RVA: 0x00002BC8 File Offset: 0x00000DC8
		[RequiredByNativeCode]
		internal static void RemoteSettingsUpdateCompleted(bool wasLastUpdatedFromServer, bool settingsChanged, int response)
		{
			Action<bool, bool, int> completed = RemoteSettings.Completed;
			if (completed != null)
			{
				completed(wasLastUpdatedFromServer, settingsChanged, response);
			}
		}

		// Token: 0x06000059 RID: 89 RVA: 0x00002BEB File Offset: 0x00000DEB
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Calling CallOnUpdate() is not necessary any more and should be removed. Use RemoteSettingsUpdated instead", true)]
		public static void CallOnUpdate()
		{
			throw new NotSupportedException("Calling CallOnUpdate() is not necessary any more and should be removed.");
		}

		// Token: 0x0600005A RID: 90
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void ForceUpdate();

		// Token: 0x0600005B RID: 91
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool WasLastUpdatedFromServer();

		// Token: 0x0600005C RID: 92 RVA: 0x00002BF8 File Offset: 0x00000DF8
		[ExcludeFromDocs]
		public static int GetInt(string key)
		{
			return RemoteSettings.GetInt(key, 0);
		}

		// Token: 0x0600005D RID: 93
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetInt(string key, [UnityEngine.Internal.DefaultValue("0")] int defaultValue);

		// Token: 0x0600005E RID: 94 RVA: 0x00002C14 File Offset: 0x00000E14
		[ExcludeFromDocs]
		public static long GetLong(string key)
		{
			return RemoteSettings.GetLong(key, 0L);
		}

		// Token: 0x0600005F RID: 95
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern long GetLong(string key, [UnityEngine.Internal.DefaultValue("0")] long defaultValue);

		// Token: 0x06000060 RID: 96 RVA: 0x00002C34 File Offset: 0x00000E34
		[ExcludeFromDocs]
		public static float GetFloat(string key)
		{
			return RemoteSettings.GetFloat(key, 0f);
		}

		// Token: 0x06000061 RID: 97
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float GetFloat(string key, [UnityEngine.Internal.DefaultValue("0.0F")] float defaultValue);

		// Token: 0x06000062 RID: 98 RVA: 0x00002C54 File Offset: 0x00000E54
		[ExcludeFromDocs]
		public static string GetString(string key)
		{
			return RemoteSettings.GetString(key, "");
		}

		// Token: 0x06000063 RID: 99
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string GetString(string key, [UnityEngine.Internal.DefaultValue("\"\"")] string defaultValue);

		// Token: 0x06000064 RID: 100 RVA: 0x00002C74 File Offset: 0x00000E74
		[ExcludeFromDocs]
		public static bool GetBool(string key)
		{
			return RemoteSettings.GetBool(key, false);
		}

		// Token: 0x06000065 RID: 101
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool GetBool(string key, [UnityEngine.Internal.DefaultValue("false")] bool defaultValue);

		// Token: 0x06000066 RID: 102
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool HasKey(string key);

		// Token: 0x06000067 RID: 103
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetCount();

		// Token: 0x06000068 RID: 104
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string[] GetKeys();

		// Token: 0x04000018 RID: 24
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private static RemoteSettings.UpdatedEventHandler Updated;

		// Token: 0x04000019 RID: 25
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private static Action BeforeFetchFromServer;

		// Token: 0x0400001A RID: 26
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private static Action<bool, bool, int> Completed;

		// Token: 0x0200000D RID: 13
		// (Invoke) Token: 0x0600006A RID: 106
		public delegate void UpdatedEventHandler();
	}
}
