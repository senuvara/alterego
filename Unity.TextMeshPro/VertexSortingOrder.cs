﻿using System;

namespace TMPro
{
	// Token: 0x0200001E RID: 30
	public enum VertexSortingOrder
	{
		// Token: 0x04000100 RID: 256
		Normal,
		// Token: 0x04000101 RID: 257
		Reverse
	}
}
