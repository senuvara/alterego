﻿using System;
using UnityEngine;

namespace TMPro
{
	// Token: 0x02000019 RID: 25
	internal static class SetPropertyUtility
	{
		// Token: 0x0600021A RID: 538 RVA: 0x0001C078 File Offset: 0x0001A278
		public static bool SetColor(ref Color currentValue, Color newValue)
		{
			if (currentValue.r == newValue.r && currentValue.g == newValue.g && currentValue.b == newValue.b && currentValue.a == newValue.a)
			{
				return false;
			}
			currentValue = newValue;
			return true;
		}

		// Token: 0x0600021B RID: 539 RVA: 0x0001C0C7 File Offset: 0x0001A2C7
		public static bool SetEquatableStruct<T>(ref T currentValue, T newValue) where T : IEquatable<T>
		{
			if (currentValue.Equals(newValue))
			{
				return false;
			}
			currentValue = newValue;
			return true;
		}

		// Token: 0x0600021C RID: 540 RVA: 0x0001C0E2 File Offset: 0x0001A2E2
		public static bool SetStruct<T>(ref T currentValue, T newValue) where T : struct
		{
			if (currentValue.Equals(newValue))
			{
				return false;
			}
			currentValue = newValue;
			return true;
		}

		// Token: 0x0600021D RID: 541 RVA: 0x0001C104 File Offset: 0x0001A304
		public static bool SetClass<T>(ref T currentValue, T newValue) where T : class
		{
			if ((currentValue == null && newValue == null) || (currentValue != null && currentValue.Equals(newValue)))
			{
				return false;
			}
			currentValue = newValue;
			return true;
		}
	}
}
