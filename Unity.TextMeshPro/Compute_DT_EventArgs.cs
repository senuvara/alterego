﻿using System;
using UnityEngine;

namespace TMPro
{
	// Token: 0x02000045 RID: 69
	public class Compute_DT_EventArgs
	{
		// Token: 0x06000455 RID: 1109 RVA: 0x0002D793 File Offset: 0x0002B993
		public Compute_DT_EventArgs(Compute_DistanceTransform_EventTypes type, float progress)
		{
			this.EventType = type;
			this.ProgressPercentage = progress;
		}

		// Token: 0x06000456 RID: 1110 RVA: 0x0002D7A9 File Offset: 0x0002B9A9
		public Compute_DT_EventArgs(Compute_DistanceTransform_EventTypes type, Color[] colors)
		{
			this.EventType = type;
			this.Colors = colors;
		}

		// Token: 0x040002E5 RID: 741
		public Compute_DistanceTransform_EventTypes EventType;

		// Token: 0x040002E6 RID: 742
		public float ProgressPercentage;

		// Token: 0x040002E7 RID: 743
		public Color[] Colors;
	}
}
