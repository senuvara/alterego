﻿using System;
using UnityEngine;

namespace TMPro
{
	// Token: 0x02000058 RID: 88
	public struct Extents
	{
		// Token: 0x06000486 RID: 1158 RVA: 0x0002E2BD File Offset: 0x0002C4BD
		public Extents(Vector2 min, Vector2 max)
		{
			this.min = min;
			this.max = max;
		}

		// Token: 0x06000487 RID: 1159 RVA: 0x0002E2D0 File Offset: 0x0002C4D0
		public override string ToString()
		{
			return string.Concat(new string[]
			{
				"Min (",
				this.min.x.ToString("f2"),
				", ",
				this.min.y.ToString("f2"),
				")   Max (",
				this.max.x.ToString("f2"),
				", ",
				this.max.y.ToString("f2"),
				")"
			});
		}

		// Token: 0x0400036A RID: 874
		public Vector2 min;

		// Token: 0x0400036B RID: 875
		public Vector2 max;
	}
}
