﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace TMPro
{
	// Token: 0x02000022 RID: 34
	public class TMP_SelectionCaret : MaskableGraphic
	{
		// Token: 0x0600024F RID: 591 RVA: 0x0000297D File Offset: 0x00000B7D
		public override void Cull(Rect clipRect, bool validRect)
		{
		}

		// Token: 0x06000250 RID: 592 RVA: 0x0001DFA7 File Offset: 0x0001C1A7
		public TMP_SelectionCaret()
		{
		}
	}
}
