﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace TMPro
{
	// Token: 0x02000018 RID: 24
	[AddComponentMenu("UI/TextMeshPro - Input Field", 11)]
	public class TMP_InputField : Selectable, IUpdateSelectedHandler, IEventSystemHandler, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler, ISubmitHandler, ICanvasElement, IScrollHandler
	{
		// Token: 0x06000156 RID: 342 RVA: 0x00017DDC File Offset: 0x00015FDC
		protected TMP_InputField()
		{
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x06000157 RID: 343 RVA: 0x00017F04 File Offset: 0x00016104
		protected Mesh mesh
		{
			get
			{
				if (this.m_Mesh == null)
				{
					this.m_Mesh = new Mesh();
				}
				return this.m_Mesh;
			}
		}

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x06000159 RID: 345 RVA: 0x00017F34 File Offset: 0x00016134
		// (set) Token: 0x06000158 RID: 344 RVA: 0x00017F25 File Offset: 0x00016125
		public bool shouldHideMobileInput
		{
			get
			{
				RuntimePlatform platform = Application.platform;
				return (platform != RuntimePlatform.IPhonePlayer && platform != RuntimePlatform.Android && platform != RuntimePlatform.tvOS) || this.m_HideMobileInput;
			}
			set
			{
				SetPropertyUtility.SetStruct<bool>(ref this.m_HideMobileInput, value);
			}
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x0600015A RID: 346 RVA: 0x00017F5D File Offset: 0x0001615D
		// (set) Token: 0x0600015B RID: 347 RVA: 0x00017F68 File Offset: 0x00016168
		public string text
		{
			get
			{
				return this.m_Text;
			}
			set
			{
				if (this.text == value)
				{
					return;
				}
				if (value == null)
				{
					value = string.Empty;
				}
				this.m_Text = value;
				if (this.m_Keyboard != null)
				{
					this.m_Keyboard.text = this.m_Text;
				}
				if (this.m_StringPosition > this.m_Text.Length)
				{
					this.m_StringPosition = (this.m_StringSelectPosition = this.m_Text.Length);
				}
				this.AdjustTextPositionRelativeToViewport(0f);
				this.m_forceRectTransformAdjustment = true;
				this.SendOnValueChangedAndUpdateLabel();
			}
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x0600015C RID: 348 RVA: 0x00017FF3 File Offset: 0x000161F3
		public bool isFocused
		{
			get
			{
				return this.m_AllowInput;
			}
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x0600015D RID: 349 RVA: 0x00017FFB File Offset: 0x000161FB
		// (set) Token: 0x0600015E RID: 350 RVA: 0x00018003 File Offset: 0x00016203
		public float caretBlinkRate
		{
			get
			{
				return this.m_CaretBlinkRate;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<float>(ref this.m_CaretBlinkRate, value) && this.m_AllowInput)
				{
					this.SetCaretActive();
				}
			}
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x0600015F RID: 351 RVA: 0x00018021 File Offset: 0x00016221
		// (set) Token: 0x06000160 RID: 352 RVA: 0x00018029 File Offset: 0x00016229
		public int caretWidth
		{
			get
			{
				return this.m_CaretWidth;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<int>(ref this.m_CaretWidth, value))
				{
					this.MarkGeometryAsDirty();
				}
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x06000161 RID: 353 RVA: 0x0001803F File Offset: 0x0001623F
		// (set) Token: 0x06000162 RID: 354 RVA: 0x00018047 File Offset: 0x00016247
		public RectTransform textViewport
		{
			get
			{
				return this.m_TextViewport;
			}
			set
			{
				SetPropertyUtility.SetClass<RectTransform>(ref this.m_TextViewport, value);
			}
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x06000163 RID: 355 RVA: 0x00018056 File Offset: 0x00016256
		// (set) Token: 0x06000164 RID: 356 RVA: 0x0001805E File Offset: 0x0001625E
		public TMP_Text textComponent
		{
			get
			{
				return this.m_TextComponent;
			}
			set
			{
				SetPropertyUtility.SetClass<TMP_Text>(ref this.m_TextComponent, value);
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x06000165 RID: 357 RVA: 0x0001806D File Offset: 0x0001626D
		// (set) Token: 0x06000166 RID: 358 RVA: 0x00018075 File Offset: 0x00016275
		public Graphic placeholder
		{
			get
			{
				return this.m_Placeholder;
			}
			set
			{
				SetPropertyUtility.SetClass<Graphic>(ref this.m_Placeholder, value);
			}
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x06000167 RID: 359 RVA: 0x00018084 File Offset: 0x00016284
		// (set) Token: 0x06000168 RID: 360 RVA: 0x0001808C File Offset: 0x0001628C
		public Scrollbar verticalScrollbar
		{
			get
			{
				return this.m_VerticalScrollbar;
			}
			set
			{
				if (this.m_VerticalScrollbar != null)
				{
					this.m_VerticalScrollbar.onValueChanged.RemoveListener(new UnityAction<float>(this.OnScrollbarValueChange));
				}
				SetPropertyUtility.SetClass<Scrollbar>(ref this.m_VerticalScrollbar, value);
				if (this.m_VerticalScrollbar)
				{
					this.m_VerticalScrollbar.onValueChanged.AddListener(new UnityAction<float>(this.OnScrollbarValueChange));
				}
			}
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x06000169 RID: 361 RVA: 0x000180F9 File Offset: 0x000162F9
		// (set) Token: 0x0600016A RID: 362 RVA: 0x00018101 File Offset: 0x00016301
		public float scrollSensitivity
		{
			get
			{
				return this.m_ScrollSensitivity;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<float>(ref this.m_ScrollSensitivity, value))
				{
					this.MarkGeometryAsDirty();
				}
			}
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x0600016B RID: 363 RVA: 0x00018117 File Offset: 0x00016317
		// (set) Token: 0x0600016C RID: 364 RVA: 0x00018133 File Offset: 0x00016333
		public Color caretColor
		{
			get
			{
				if (!this.customCaretColor)
				{
					return this.textComponent.color;
				}
				return this.m_CaretColor;
			}
			set
			{
				if (SetPropertyUtility.SetColor(ref this.m_CaretColor, value))
				{
					this.MarkGeometryAsDirty();
				}
			}
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x0600016D RID: 365 RVA: 0x00018149 File Offset: 0x00016349
		// (set) Token: 0x0600016E RID: 366 RVA: 0x00018151 File Offset: 0x00016351
		public bool customCaretColor
		{
			get
			{
				return this.m_CustomCaretColor;
			}
			set
			{
				if (this.m_CustomCaretColor != value)
				{
					this.m_CustomCaretColor = value;
					this.MarkGeometryAsDirty();
				}
			}
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x0600016F RID: 367 RVA: 0x00018169 File Offset: 0x00016369
		// (set) Token: 0x06000170 RID: 368 RVA: 0x00018171 File Offset: 0x00016371
		public Color selectionColor
		{
			get
			{
				return this.m_SelectionColor;
			}
			set
			{
				if (SetPropertyUtility.SetColor(ref this.m_SelectionColor, value))
				{
					this.MarkGeometryAsDirty();
				}
			}
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x06000171 RID: 369 RVA: 0x00018187 File Offset: 0x00016387
		// (set) Token: 0x06000172 RID: 370 RVA: 0x0001818F File Offset: 0x0001638F
		public TMP_InputField.SubmitEvent onEndEdit
		{
			get
			{
				return this.m_OnEndEdit;
			}
			set
			{
				SetPropertyUtility.SetClass<TMP_InputField.SubmitEvent>(ref this.m_OnEndEdit, value);
			}
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x06000173 RID: 371 RVA: 0x0001819E File Offset: 0x0001639E
		// (set) Token: 0x06000174 RID: 372 RVA: 0x000181A6 File Offset: 0x000163A6
		public TMP_InputField.SubmitEvent onSubmit
		{
			get
			{
				return this.m_OnSubmit;
			}
			set
			{
				SetPropertyUtility.SetClass<TMP_InputField.SubmitEvent>(ref this.m_OnSubmit, value);
			}
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x06000175 RID: 373 RVA: 0x000181B5 File Offset: 0x000163B5
		// (set) Token: 0x06000176 RID: 374 RVA: 0x000181BD File Offset: 0x000163BD
		public TMP_InputField.SelectionEvent onSelect
		{
			get
			{
				return this.m_OnSelect;
			}
			set
			{
				SetPropertyUtility.SetClass<TMP_InputField.SelectionEvent>(ref this.m_OnSelect, value);
			}
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x06000177 RID: 375 RVA: 0x000181CC File Offset: 0x000163CC
		// (set) Token: 0x06000178 RID: 376 RVA: 0x000181D4 File Offset: 0x000163D4
		public TMP_InputField.SelectionEvent onDeselect
		{
			get
			{
				return this.m_OnDeselect;
			}
			set
			{
				SetPropertyUtility.SetClass<TMP_InputField.SelectionEvent>(ref this.m_OnDeselect, value);
			}
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x06000179 RID: 377 RVA: 0x000181E3 File Offset: 0x000163E3
		// (set) Token: 0x0600017A RID: 378 RVA: 0x000181EB File Offset: 0x000163EB
		public TMP_InputField.TextSelectionEvent onTextSelection
		{
			get
			{
				return this.m_OnTextSelection;
			}
			set
			{
				SetPropertyUtility.SetClass<TMP_InputField.TextSelectionEvent>(ref this.m_OnTextSelection, value);
			}
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x0600017B RID: 379 RVA: 0x000181FA File Offset: 0x000163FA
		// (set) Token: 0x0600017C RID: 380 RVA: 0x00018202 File Offset: 0x00016402
		public TMP_InputField.TextSelectionEvent onEndTextSelection
		{
			get
			{
				return this.m_OnEndTextSelection;
			}
			set
			{
				SetPropertyUtility.SetClass<TMP_InputField.TextSelectionEvent>(ref this.m_OnEndTextSelection, value);
			}
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x0600017D RID: 381 RVA: 0x00018211 File Offset: 0x00016411
		// (set) Token: 0x0600017E RID: 382 RVA: 0x00018219 File Offset: 0x00016419
		public TMP_InputField.OnChangeEvent onValueChanged
		{
			get
			{
				return this.m_OnValueChanged;
			}
			set
			{
				SetPropertyUtility.SetClass<TMP_InputField.OnChangeEvent>(ref this.m_OnValueChanged, value);
			}
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x0600017F RID: 383 RVA: 0x00018228 File Offset: 0x00016428
		// (set) Token: 0x06000180 RID: 384 RVA: 0x00018230 File Offset: 0x00016430
		public TMP_InputField.OnValidateInput onValidateInput
		{
			get
			{
				return this.m_OnValidateInput;
			}
			set
			{
				SetPropertyUtility.SetClass<TMP_InputField.OnValidateInput>(ref this.m_OnValidateInput, value);
			}
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x06000181 RID: 385 RVA: 0x0001823F File Offset: 0x0001643F
		// (set) Token: 0x06000182 RID: 386 RVA: 0x00018247 File Offset: 0x00016447
		public int characterLimit
		{
			get
			{
				return this.m_CharacterLimit;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<int>(ref this.m_CharacterLimit, Math.Max(0, value)))
				{
					this.UpdateLabel();
				}
			}
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x06000183 RID: 387 RVA: 0x00018263 File Offset: 0x00016463
		// (set) Token: 0x06000184 RID: 388 RVA: 0x0001826B File Offset: 0x0001646B
		public float pointSize
		{
			get
			{
				return this.m_GlobalPointSize;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<float>(ref this.m_GlobalPointSize, Math.Max(0f, value)))
				{
					this.SetGlobalPointSize(this.m_GlobalPointSize);
					this.UpdateLabel();
				}
			}
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x06000185 RID: 389 RVA: 0x00018297 File Offset: 0x00016497
		// (set) Token: 0x06000186 RID: 390 RVA: 0x0001829F File Offset: 0x0001649F
		public TMP_FontAsset fontAsset
		{
			get
			{
				return this.m_GlobalFontAsset;
			}
			set
			{
				if (SetPropertyUtility.SetClass<TMP_FontAsset>(ref this.m_GlobalFontAsset, value))
				{
					this.SetGlobalFontAsset(this.m_GlobalFontAsset);
					this.UpdateLabel();
				}
			}
		}

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x06000187 RID: 391 RVA: 0x000182C1 File Offset: 0x000164C1
		// (set) Token: 0x06000188 RID: 392 RVA: 0x000182C9 File Offset: 0x000164C9
		public bool onFocusSelectAll
		{
			get
			{
				return this.m_OnFocusSelectAll;
			}
			set
			{
				this.m_OnFocusSelectAll = value;
			}
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x06000189 RID: 393 RVA: 0x000182D2 File Offset: 0x000164D2
		// (set) Token: 0x0600018A RID: 394 RVA: 0x000182DA File Offset: 0x000164DA
		public bool resetOnDeActivation
		{
			get
			{
				return this.m_ResetOnDeActivation;
			}
			set
			{
				this.m_ResetOnDeActivation = value;
			}
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x0600018B RID: 395 RVA: 0x000182E3 File Offset: 0x000164E3
		// (set) Token: 0x0600018C RID: 396 RVA: 0x000182EB File Offset: 0x000164EB
		public bool restoreOriginalTextOnEscape
		{
			get
			{
				return this.m_RestoreOriginalTextOnEscape;
			}
			set
			{
				this.m_RestoreOriginalTextOnEscape = value;
			}
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x0600018D RID: 397 RVA: 0x000182F4 File Offset: 0x000164F4
		// (set) Token: 0x0600018E RID: 398 RVA: 0x000182FC File Offset: 0x000164FC
		public bool isRichTextEditingAllowed
		{
			get
			{
				return this.m_isRichTextEditingAllowed;
			}
			set
			{
				this.m_isRichTextEditingAllowed = value;
			}
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x0600018F RID: 399 RVA: 0x00018305 File Offset: 0x00016505
		// (set) Token: 0x06000190 RID: 400 RVA: 0x0001830D File Offset: 0x0001650D
		public TMP_InputField.ContentType contentType
		{
			get
			{
				return this.m_ContentType;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<TMP_InputField.ContentType>(ref this.m_ContentType, value))
				{
					this.EnforceContentType();
				}
			}
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x06000191 RID: 401 RVA: 0x00018323 File Offset: 0x00016523
		// (set) Token: 0x06000192 RID: 402 RVA: 0x0001832B File Offset: 0x0001652B
		public TMP_InputField.LineType lineType
		{
			get
			{
				return this.m_LineType;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<TMP_InputField.LineType>(ref this.m_LineType, value))
				{
					this.SetTextComponentWrapMode();
				}
				this.SetToCustomIfContentTypeIsNot(new TMP_InputField.ContentType[]
				{
					TMP_InputField.ContentType.Standard,
					TMP_InputField.ContentType.Autocorrected
				});
			}
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x06000193 RID: 403 RVA: 0x00018351 File Offset: 0x00016551
		// (set) Token: 0x06000194 RID: 404 RVA: 0x00018359 File Offset: 0x00016559
		public TMP_InputField.InputType inputType
		{
			get
			{
				return this.m_InputType;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<TMP_InputField.InputType>(ref this.m_InputType, value))
				{
					this.SetToCustom();
				}
			}
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x06000195 RID: 405 RVA: 0x0001836F File Offset: 0x0001656F
		// (set) Token: 0x06000196 RID: 406 RVA: 0x00018377 File Offset: 0x00016577
		public TouchScreenKeyboardType keyboardType
		{
			get
			{
				return this.m_KeyboardType;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<TouchScreenKeyboardType>(ref this.m_KeyboardType, value))
				{
					this.SetToCustom();
				}
			}
		}

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x06000197 RID: 407 RVA: 0x0001838D File Offset: 0x0001658D
		// (set) Token: 0x06000198 RID: 408 RVA: 0x00018395 File Offset: 0x00016595
		public TMP_InputField.CharacterValidation characterValidation
		{
			get
			{
				return this.m_CharacterValidation;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<TMP_InputField.CharacterValidation>(ref this.m_CharacterValidation, value))
				{
					this.SetToCustom();
				}
			}
		}

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x06000199 RID: 409 RVA: 0x000183AB File Offset: 0x000165AB
		// (set) Token: 0x0600019A RID: 410 RVA: 0x000183B3 File Offset: 0x000165B3
		public TMP_InputValidator inputValidator
		{
			get
			{
				return this.m_InputValidator;
			}
			set
			{
				if (SetPropertyUtility.SetClass<TMP_InputValidator>(ref this.m_InputValidator, value))
				{
					this.SetToCustom(TMP_InputField.CharacterValidation.CustomValidator);
				}
			}
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x0600019B RID: 411 RVA: 0x000183CA File Offset: 0x000165CA
		// (set) Token: 0x0600019C RID: 412 RVA: 0x000183D2 File Offset: 0x000165D2
		public bool readOnly
		{
			get
			{
				return this.m_ReadOnly;
			}
			set
			{
				this.m_ReadOnly = value;
			}
		}

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x0600019D RID: 413 RVA: 0x000183DB File Offset: 0x000165DB
		// (set) Token: 0x0600019E RID: 414 RVA: 0x000183E3 File Offset: 0x000165E3
		public bool richText
		{
			get
			{
				return this.m_RichText;
			}
			set
			{
				this.m_RichText = value;
				this.SetTextComponentRichTextMode();
			}
		}

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x0600019F RID: 415 RVA: 0x000183F2 File Offset: 0x000165F2
		public bool multiLine
		{
			get
			{
				return this.m_LineType == TMP_InputField.LineType.MultiLineNewline || this.lineType == TMP_InputField.LineType.MultiLineSubmit;
			}
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x060001A0 RID: 416 RVA: 0x00018408 File Offset: 0x00016608
		// (set) Token: 0x060001A1 RID: 417 RVA: 0x00018410 File Offset: 0x00016610
		public char asteriskChar
		{
			get
			{
				return this.m_AsteriskChar;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<char>(ref this.m_AsteriskChar, value))
				{
					this.UpdateLabel();
				}
			}
		}

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x060001A2 RID: 418 RVA: 0x00018426 File Offset: 0x00016626
		public bool wasCanceled
		{
			get
			{
				return this.m_WasCanceled;
			}
		}

		// Token: 0x060001A3 RID: 419 RVA: 0x0001842E File Offset: 0x0001662E
		protected void ClampStringPos(ref int pos)
		{
			if (pos < 0)
			{
				pos = 0;
				return;
			}
			if (pos > this.text.Length)
			{
				pos = this.text.Length;
			}
		}

		// Token: 0x060001A4 RID: 420 RVA: 0x00018455 File Offset: 0x00016655
		protected void ClampCaretPos(ref int pos)
		{
			if (pos < 0)
			{
				pos = 0;
				return;
			}
			if (pos > this.m_TextComponent.textInfo.characterCount - 1)
			{
				pos = this.m_TextComponent.textInfo.characterCount - 1;
			}
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x060001A5 RID: 421 RVA: 0x0001848A File Offset: 0x0001668A
		// (set) Token: 0x060001A6 RID: 422 RVA: 0x0001849D File Offset: 0x0001669D
		protected int caretPositionInternal
		{
			get
			{
				return this.m_CaretPosition + Input.compositionString.Length;
			}
			set
			{
				this.m_CaretPosition = value;
				this.ClampCaretPos(ref this.m_CaretPosition);
			}
		}

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x060001A7 RID: 423 RVA: 0x000184B2 File Offset: 0x000166B2
		// (set) Token: 0x060001A8 RID: 424 RVA: 0x000184C5 File Offset: 0x000166C5
		protected int stringPositionInternal
		{
			get
			{
				return this.m_StringPosition + Input.compositionString.Length;
			}
			set
			{
				this.m_StringPosition = value;
				this.ClampStringPos(ref this.m_StringPosition);
			}
		}

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x060001A9 RID: 425 RVA: 0x000184DA File Offset: 0x000166DA
		// (set) Token: 0x060001AA RID: 426 RVA: 0x000184ED File Offset: 0x000166ED
		protected int caretSelectPositionInternal
		{
			get
			{
				return this.m_CaretSelectPosition + Input.compositionString.Length;
			}
			set
			{
				this.m_CaretSelectPosition = value;
				this.ClampCaretPos(ref this.m_CaretSelectPosition);
			}
		}

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x060001AB RID: 427 RVA: 0x00018502 File Offset: 0x00016702
		// (set) Token: 0x060001AC RID: 428 RVA: 0x00018515 File Offset: 0x00016715
		protected int stringSelectPositionInternal
		{
			get
			{
				return this.m_StringSelectPosition + Input.compositionString.Length;
			}
			set
			{
				this.m_StringSelectPosition = value;
				this.ClampStringPos(ref this.m_StringSelectPosition);
			}
		}

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x060001AD RID: 429 RVA: 0x0001852A File Offset: 0x0001672A
		private bool hasSelection
		{
			get
			{
				return this.stringPositionInternal != this.stringSelectPositionInternal;
			}
		}

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x060001AE RID: 430 RVA: 0x0001853D File Offset: 0x0001673D
		// (set) Token: 0x060001AF RID: 431 RVA: 0x00018545 File Offset: 0x00016745
		public int caretPosition
		{
			get
			{
				return this.caretSelectPositionInternal;
			}
			set
			{
				this.selectionAnchorPosition = value;
				this.selectionFocusPosition = value;
				this.isStringPositionDirty = true;
			}
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x060001B0 RID: 432 RVA: 0x0001855C File Offset: 0x0001675C
		// (set) Token: 0x060001B1 RID: 433 RVA: 0x00018564 File Offset: 0x00016764
		public int selectionAnchorPosition
		{
			get
			{
				return this.caretPositionInternal;
			}
			set
			{
				if (Input.compositionString.Length != 0)
				{
					return;
				}
				this.caretPositionInternal = value;
				this.isStringPositionDirty = true;
			}
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x060001B2 RID: 434 RVA: 0x0001853D File Offset: 0x0001673D
		// (set) Token: 0x060001B3 RID: 435 RVA: 0x00018581 File Offset: 0x00016781
		public int selectionFocusPosition
		{
			get
			{
				return this.caretSelectPositionInternal;
			}
			set
			{
				if (Input.compositionString.Length != 0)
				{
					return;
				}
				this.caretSelectPositionInternal = value;
				this.isStringPositionDirty = true;
			}
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x060001B4 RID: 436 RVA: 0x0001859E File Offset: 0x0001679E
		// (set) Token: 0x060001B5 RID: 437 RVA: 0x000185A6 File Offset: 0x000167A6
		public int stringPosition
		{
			get
			{
				return this.stringSelectPositionInternal;
			}
			set
			{
				this.selectionStringAnchorPosition = value;
				this.selectionStringFocusPosition = value;
			}
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x060001B6 RID: 438 RVA: 0x000185B6 File Offset: 0x000167B6
		// (set) Token: 0x060001B7 RID: 439 RVA: 0x000185BE File Offset: 0x000167BE
		public int selectionStringAnchorPosition
		{
			get
			{
				return this.stringPositionInternal;
			}
			set
			{
				if (Input.compositionString.Length != 0)
				{
					return;
				}
				this.stringPositionInternal = value;
			}
		}

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x060001B8 RID: 440 RVA: 0x0001859E File Offset: 0x0001679E
		// (set) Token: 0x060001B9 RID: 441 RVA: 0x000185D4 File Offset: 0x000167D4
		public int selectionStringFocusPosition
		{
			get
			{
				return this.stringSelectPositionInternal;
			}
			set
			{
				if (Input.compositionString.Length != 0)
				{
					return;
				}
				this.stringSelectPositionInternal = value;
			}
		}

		// Token: 0x060001BA RID: 442 RVA: 0x000185EC File Offset: 0x000167EC
		protected override void OnEnable()
		{
			base.OnEnable();
			if (this.m_Text == null)
			{
				this.m_Text = string.Empty;
			}
			if (Application.isPlaying && this.m_CachedInputRenderer == null && this.m_TextComponent != null)
			{
				GameObject gameObject = new GameObject(base.transform.name + " Input Caret", new Type[]
				{
					typeof(RectTransform)
				});
				TMP_SelectionCaret tmp_SelectionCaret = gameObject.AddComponent<TMP_SelectionCaret>();
				tmp_SelectionCaret.raycastTarget = false;
				tmp_SelectionCaret.color = Color.clear;
				gameObject.hideFlags = HideFlags.DontSave;
				gameObject.transform.SetParent(this.m_TextComponent.transform.parent);
				gameObject.transform.SetAsFirstSibling();
				gameObject.layer = base.gameObject.layer;
				this.caretRectTrans = gameObject.GetComponent<RectTransform>();
				this.m_CachedInputRenderer = gameObject.GetComponent<CanvasRenderer>();
				this.m_CachedInputRenderer.SetMaterial(Graphic.defaultGraphicMaterial, Texture2D.whiteTexture);
				gameObject.AddComponent<LayoutElement>().ignoreLayout = true;
				this.AssignPositioningIfNeeded();
			}
			if (this.m_CachedInputRenderer != null)
			{
				this.m_CachedInputRenderer.SetMaterial(Graphic.defaultGraphicMaterial, Texture2D.whiteTexture);
			}
			if (this.m_TextComponent != null)
			{
				this.m_TextComponent.RegisterDirtyVerticesCallback(new UnityAction(this.MarkGeometryAsDirty));
				this.m_TextComponent.RegisterDirtyVerticesCallback(new UnityAction(this.UpdateLabel));
				this.m_DefaultTransformPosition = this.m_TextComponent.rectTransform.localPosition;
				if (this.m_VerticalScrollbar != null)
				{
					this.m_TextComponent.ignoreRectMaskCulling = true;
					this.m_VerticalScrollbar.onValueChanged.AddListener(new UnityAction<float>(this.OnScrollbarValueChange));
				}
				this.UpdateLabel();
			}
			TMPro_EventManager.TEXT_CHANGED_EVENT.Add(new Action<Object>(this.ON_TEXT_CHANGED));
		}

		// Token: 0x060001BB RID: 443 RVA: 0x000187D0 File Offset: 0x000169D0
		protected override void OnDisable()
		{
			this.m_BlinkCoroutine = null;
			this.DeactivateInputField();
			if (this.m_TextComponent != null)
			{
				this.m_TextComponent.UnregisterDirtyVerticesCallback(new UnityAction(this.MarkGeometryAsDirty));
				this.m_TextComponent.UnregisterDirtyVerticesCallback(new UnityAction(this.UpdateLabel));
				if (this.m_VerticalScrollbar != null)
				{
					this.m_VerticalScrollbar.onValueChanged.RemoveListener(new UnityAction<float>(this.OnScrollbarValueChange));
				}
			}
			CanvasUpdateRegistry.UnRegisterCanvasElementForRebuild(this);
			if (this.m_CachedInputRenderer != null)
			{
				this.m_CachedInputRenderer.Clear();
			}
			if (this.m_Mesh != null)
			{
				Object.DestroyImmediate(this.m_Mesh);
			}
			this.m_Mesh = null;
			TMPro_EventManager.TEXT_CHANGED_EVENT.Remove(new Action<Object>(this.ON_TEXT_CHANGED));
			base.OnDisable();
		}

		// Token: 0x060001BC RID: 444 RVA: 0x000188AB File Offset: 0x00016AAB
		private void ON_TEXT_CHANGED(Object obj)
		{
			if (obj == this.m_TextComponent && Application.isPlaying)
			{
				this.caretPositionInternal = this.GetCaretPositionFromStringIndex(this.stringPositionInternal);
				this.caretSelectPositionInternal = this.GetCaretPositionFromStringIndex(this.stringSelectPositionInternal);
			}
		}

		// Token: 0x060001BD RID: 445 RVA: 0x000188E6 File Offset: 0x00016AE6
		private IEnumerator CaretBlink()
		{
			this.m_CaretVisible = true;
			yield return null;
			while (this.m_CaretBlinkRate > 0f)
			{
				float num = 1f / this.m_CaretBlinkRate;
				bool flag = (Time.unscaledTime - this.m_BlinkStartTime) % num < num / 2f;
				if (this.m_CaretVisible != flag)
				{
					this.m_CaretVisible = flag;
					if (!this.hasSelection)
					{
						this.MarkGeometryAsDirty();
					}
				}
				yield return null;
			}
			this.m_BlinkCoroutine = null;
			yield break;
		}

		// Token: 0x060001BE RID: 446 RVA: 0x000188F5 File Offset: 0x00016AF5
		private void SetCaretVisible()
		{
			if (!this.m_AllowInput)
			{
				return;
			}
			this.m_CaretVisible = true;
			this.m_BlinkStartTime = Time.unscaledTime;
			this.SetCaretActive();
		}

		// Token: 0x060001BF RID: 447 RVA: 0x00018918 File Offset: 0x00016B18
		private void SetCaretActive()
		{
			if (!this.m_AllowInput)
			{
				return;
			}
			if (this.m_CaretBlinkRate > 0f)
			{
				if (this.m_BlinkCoroutine == null)
				{
					this.m_BlinkCoroutine = base.StartCoroutine(this.CaretBlink());
					return;
				}
			}
			else
			{
				this.m_CaretVisible = true;
			}
		}

		// Token: 0x060001C0 RID: 448 RVA: 0x00018952 File Offset: 0x00016B52
		protected void OnFocus()
		{
			if (this.m_OnFocusSelectAll)
			{
				this.SelectAll();
			}
		}

		// Token: 0x060001C1 RID: 449 RVA: 0x00018962 File Offset: 0x00016B62
		protected void SelectAll()
		{
			this.m_isSelectAll = true;
			this.stringPositionInternal = this.text.Length;
			this.stringSelectPositionInternal = 0;
		}

		// Token: 0x060001C2 RID: 450 RVA: 0x00018984 File Offset: 0x00016B84
		public void MoveTextEnd(bool shift)
		{
			if (this.m_isRichTextEditingAllowed)
			{
				int length = this.text.Length;
				if (shift)
				{
					this.stringSelectPositionInternal = length;
				}
				else
				{
					this.stringPositionInternal = length;
					this.stringSelectPositionInternal = this.stringPositionInternal;
				}
			}
			else
			{
				int num = this.m_TextComponent.textInfo.characterCount - 1;
				if (shift)
				{
					this.caretSelectPositionInternal = num;
					this.stringSelectPositionInternal = this.GetStringIndexFromCaretPosition(num);
				}
				else
				{
					this.caretPositionInternal = (this.caretSelectPositionInternal = num);
					this.stringSelectPositionInternal = (this.stringPositionInternal = this.GetStringIndexFromCaretPosition(num));
				}
			}
			this.UpdateLabel();
		}

		// Token: 0x060001C3 RID: 451 RVA: 0x00018A20 File Offset: 0x00016C20
		public void MoveTextStart(bool shift)
		{
			if (this.m_isRichTextEditingAllowed)
			{
				int num = 0;
				if (shift)
				{
					this.stringSelectPositionInternal = num;
				}
				else
				{
					this.stringPositionInternal = num;
					this.stringSelectPositionInternal = this.stringPositionInternal;
				}
			}
			else
			{
				int num2 = 0;
				if (shift)
				{
					this.caretSelectPositionInternal = num2;
					this.stringSelectPositionInternal = this.GetStringIndexFromCaretPosition(num2);
				}
				else
				{
					this.caretPositionInternal = (this.caretSelectPositionInternal = num2);
					this.stringSelectPositionInternal = (this.stringPositionInternal = this.GetStringIndexFromCaretPosition(num2));
				}
			}
			this.UpdateLabel();
		}

		// Token: 0x060001C4 RID: 452 RVA: 0x00018AA0 File Offset: 0x00016CA0
		public void MoveToEndOfLine(bool shift, bool ctrl)
		{
			int lineNumber = this.m_TextComponent.textInfo.characterInfo[this.caretPositionInternal].lineNumber;
			int num = ctrl ? (this.m_TextComponent.textInfo.characterCount - 1) : this.m_TextComponent.textInfo.lineInfo[lineNumber].lastCharacterIndex;
			num = this.GetStringIndexFromCaretPosition(num);
			if (shift)
			{
				this.stringSelectPositionInternal = num;
			}
			else
			{
				this.stringPositionInternal = num;
				this.stringSelectPositionInternal = this.stringPositionInternal;
			}
			this.UpdateLabel();
		}

		// Token: 0x060001C5 RID: 453 RVA: 0x00018B30 File Offset: 0x00016D30
		public void MoveToStartOfLine(bool shift, bool ctrl)
		{
			int lineNumber = this.m_TextComponent.textInfo.characterInfo[this.caretPositionInternal].lineNumber;
			int num = ctrl ? 0 : this.m_TextComponent.textInfo.lineInfo[lineNumber].firstCharacterIndex;
			num = this.GetStringIndexFromCaretPosition(num);
			if (shift)
			{
				this.stringSelectPositionInternal = num;
			}
			else
			{
				this.stringPositionInternal = num;
				this.stringSelectPositionInternal = this.stringPositionInternal;
			}
			this.UpdateLabel();
		}

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x060001C6 RID: 454 RVA: 0x00018BAD File Offset: 0x00016DAD
		// (set) Token: 0x060001C7 RID: 455 RVA: 0x00018BB4 File Offset: 0x00016DB4
		private static string clipboard
		{
			get
			{
				return GUIUtility.systemCopyBuffer;
			}
			set
			{
				GUIUtility.systemCopyBuffer = value;
			}
		}

		// Token: 0x060001C8 RID: 456 RVA: 0x00018BBC File Offset: 0x00016DBC
		private bool InPlaceEditing()
		{
			return !TouchScreenKeyboard.isSupported;
		}

		// Token: 0x060001C9 RID: 457 RVA: 0x00018BC8 File Offset: 0x00016DC8
		protected virtual void LateUpdate()
		{
			if (this.m_ShouldActivateNextUpdate)
			{
				if (!this.isFocused)
				{
					this.ActivateInputFieldInternal();
					this.m_ShouldActivateNextUpdate = false;
					return;
				}
				this.m_ShouldActivateNextUpdate = false;
			}
			if (this.m_IsScrollbarUpdateRequired)
			{
				this.UpdateScrollbar();
				this.m_IsScrollbarUpdateRequired = false;
			}
			if (this.InPlaceEditing() || !this.isFocused)
			{
				return;
			}
			this.AssignPositioningIfNeeded();
			if (this.m_Keyboard == null || !this.m_Keyboard.active)
			{
				if (this.m_Keyboard != null)
				{
					if (!this.m_ReadOnly)
					{
						this.text = this.m_Keyboard.text;
					}
					if (this.m_Keyboard.status == TouchScreenKeyboard.Status.Canceled)
					{
						this.m_WasCanceled = true;
					}
					if (this.m_Keyboard.status == TouchScreenKeyboard.Status.Done)
					{
						this.OnSubmit(null);
					}
				}
				this.OnDeselect(null);
				return;
			}
			string text = this.m_Keyboard.text;
			if (this.m_Text != text)
			{
				if (this.m_ReadOnly)
				{
					this.m_Keyboard.text = this.m_Text;
				}
				else
				{
					this.m_Text = "";
					foreach (char c in text)
					{
						if (c == '\r' || c == '\u0003')
						{
							c = '\n';
						}
						if (this.onValidateInput != null)
						{
							c = this.onValidateInput(this.m_Text, this.m_Text.Length, c);
						}
						else if (this.characterValidation != TMP_InputField.CharacterValidation.None)
						{
							c = this.Validate(this.m_Text, this.m_Text.Length, c);
						}
						if (this.lineType == TMP_InputField.LineType.MultiLineSubmit && c == '\n')
						{
							this.m_Keyboard.text = this.m_Text;
							this.OnSubmit(null);
							this.OnDeselect(null);
							return;
						}
						if (c != '\0')
						{
							this.m_Text += c.ToString();
						}
					}
					if (this.characterLimit > 0 && this.m_Text.Length > this.characterLimit)
					{
						this.m_Text = this.m_Text.Substring(0, this.characterLimit);
					}
					this.stringPositionInternal = (this.stringSelectPositionInternal = this.m_Text.Length);
					if (this.m_Text != text)
					{
						this.m_Keyboard.text = this.m_Text;
					}
					this.SendOnValueChangedAndUpdateLabel();
				}
			}
			if (this.m_Keyboard.status == TouchScreenKeyboard.Status.Done)
			{
				if (this.m_Keyboard.status == TouchScreenKeyboard.Status.Canceled)
				{
					this.m_WasCanceled = true;
				}
				this.OnDeselect(null);
			}
		}

		// Token: 0x060001CA RID: 458 RVA: 0x00018E2E File Offset: 0x0001702E
		private bool MayDrag(PointerEventData eventData)
		{
			return this.IsActive() && this.IsInteractable() && eventData.button == PointerEventData.InputButton.Left && this.m_TextComponent != null && this.m_Keyboard == null;
		}

		// Token: 0x060001CB RID: 459 RVA: 0x00018E61 File Offset: 0x00017061
		public virtual void OnBeginDrag(PointerEventData eventData)
		{
			if (!this.MayDrag(eventData))
			{
				return;
			}
			this.m_UpdateDrag = true;
		}

		// Token: 0x060001CC RID: 460 RVA: 0x00018E74 File Offset: 0x00017074
		public virtual void OnDrag(PointerEventData eventData)
		{
			if (!this.MayDrag(eventData))
			{
				return;
			}
			CaretPosition caretPosition;
			int cursorIndexFromPosition = TMP_TextUtilities.GetCursorIndexFromPosition(this.m_TextComponent, eventData.position, eventData.pressEventCamera, out caretPosition);
			if (caretPosition == CaretPosition.Left)
			{
				this.stringSelectPositionInternal = this.GetStringIndexFromCaretPosition(cursorIndexFromPosition);
			}
			else if (caretPosition == CaretPosition.Right)
			{
				this.stringSelectPositionInternal = this.GetStringIndexFromCaretPosition(cursorIndexFromPosition) + 1;
			}
			this.caretSelectPositionInternal = this.GetCaretPositionFromStringIndex(this.stringSelectPositionInternal);
			this.MarkGeometryAsDirty();
			this.m_DragPositionOutOfBounds = !RectTransformUtility.RectangleContainsScreenPoint(this.textViewport, eventData.position, eventData.pressEventCamera);
			if (this.m_DragPositionOutOfBounds && this.m_DragCoroutine == null)
			{
				this.m_DragCoroutine = base.StartCoroutine(this.MouseDragOutsideRect(eventData));
			}
			eventData.Use();
		}

		// Token: 0x060001CD RID: 461 RVA: 0x00018F31 File Offset: 0x00017131
		private IEnumerator MouseDragOutsideRect(PointerEventData eventData)
		{
			while (this.m_UpdateDrag && this.m_DragPositionOutOfBounds)
			{
				Vector2 vector;
				RectTransformUtility.ScreenPointToLocalPointInRectangle(this.textViewport, eventData.position, eventData.pressEventCamera, out vector);
				Rect rect = this.textViewport.rect;
				if (this.multiLine)
				{
					if (vector.y > rect.yMax)
					{
						this.MoveUp(true, true);
					}
					else if (vector.y < rect.yMin)
					{
						this.MoveDown(true, true);
					}
				}
				else if (vector.x < rect.xMin)
				{
					this.MoveLeft(true, false);
				}
				else if (vector.x > rect.xMax)
				{
					this.MoveRight(true, false);
				}
				this.UpdateLabel();
				float seconds = this.multiLine ? 0.1f : 0.05f;
				yield return new WaitForSeconds(seconds);
			}
			this.m_DragCoroutine = null;
			yield break;
		}

		// Token: 0x060001CE RID: 462 RVA: 0x00018F47 File Offset: 0x00017147
		public virtual void OnEndDrag(PointerEventData eventData)
		{
			if (!this.MayDrag(eventData))
			{
				return;
			}
			this.m_UpdateDrag = false;
		}

		// Token: 0x060001CF RID: 463 RVA: 0x00018F5C File Offset: 0x0001715C
		public override void OnPointerDown(PointerEventData eventData)
		{
			if (!this.MayDrag(eventData))
			{
				return;
			}
			EventSystem.current.SetSelectedGameObject(base.gameObject, eventData);
			bool allowInput = this.m_AllowInput;
			base.OnPointerDown(eventData);
			if (!this.InPlaceEditing() && (this.m_Keyboard == null || !this.m_Keyboard.active))
			{
				this.OnSelect(eventData);
				return;
			}
			bool flag = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
			bool flag2 = false;
			float unscaledTime = Time.unscaledTime;
			if (this.m_ClickStartTime + this.m_DoubleClickDelay > unscaledTime)
			{
				flag2 = true;
			}
			this.m_ClickStartTime = unscaledTime;
			if (allowInput || !this.m_OnFocusSelectAll)
			{
				CaretPosition caretPosition;
				int cursorIndexFromPosition = TMP_TextUtilities.GetCursorIndexFromPosition(this.m_TextComponent, eventData.position, eventData.pressEventCamera, out caretPosition);
				if (flag)
				{
					if (caretPosition == CaretPosition.Left)
					{
						this.stringSelectPositionInternal = this.GetStringIndexFromCaretPosition(cursorIndexFromPosition);
					}
					else if (caretPosition == CaretPosition.Right)
					{
						this.stringSelectPositionInternal = this.GetStringIndexFromCaretPosition(cursorIndexFromPosition) + 1;
					}
				}
				else if (caretPosition == CaretPosition.Left)
				{
					this.stringPositionInternal = (this.stringSelectPositionInternal = this.GetStringIndexFromCaretPosition(cursorIndexFromPosition));
				}
				else if (caretPosition == CaretPosition.Right)
				{
					this.stringPositionInternal = (this.stringSelectPositionInternal = this.GetStringIndexFromCaretPosition(cursorIndexFromPosition) + 1);
				}
				if (flag2)
				{
					int num = TMP_TextUtilities.FindIntersectingWord(this.m_TextComponent, eventData.position, eventData.pressEventCamera);
					if (num != -1)
					{
						this.caretPositionInternal = this.m_TextComponent.textInfo.wordInfo[num].firstCharacterIndex;
						this.caretSelectPositionInternal = this.m_TextComponent.textInfo.wordInfo[num].lastCharacterIndex + 1;
						this.stringPositionInternal = this.GetStringIndexFromCaretPosition(this.caretPositionInternal);
						this.stringSelectPositionInternal = this.GetStringIndexFromCaretPosition(this.caretSelectPositionInternal);
					}
					else
					{
						this.caretPositionInternal = this.GetCaretPositionFromStringIndex(this.stringPositionInternal);
						this.stringSelectPositionInternal++;
						this.caretSelectPositionInternal = this.caretPositionInternal + 1;
						this.caretSelectPositionInternal = this.GetCaretPositionFromStringIndex(this.stringSelectPositionInternal);
					}
				}
				else
				{
					this.caretPositionInternal = (this.caretSelectPositionInternal = this.GetCaretPositionFromStringIndex(this.stringPositionInternal));
				}
			}
			this.UpdateLabel();
			eventData.Use();
		}

		// Token: 0x060001D0 RID: 464 RVA: 0x00019190 File Offset: 0x00017390
		protected TMP_InputField.EditState KeyPressed(Event evt)
		{
			EventModifiers modifiers = evt.modifiers;
			RuntimePlatform platform = Application.platform;
			bool flag = (platform == RuntimePlatform.OSXEditor || platform == RuntimePlatform.OSXPlayer) ? ((modifiers & EventModifiers.Command) > EventModifiers.None) : ((modifiers & EventModifiers.Control) > EventModifiers.None);
			bool flag2 = (modifiers & EventModifiers.Shift) > EventModifiers.None;
			bool flag3 = (modifiers & EventModifiers.Alt) > EventModifiers.None;
			bool flag4 = flag && !flag3 && !flag2;
			KeyCode keyCode = evt.keyCode;
			if (keyCode <= KeyCode.A)
			{
				if (keyCode <= KeyCode.Return)
				{
					if (keyCode == KeyCode.Backspace)
					{
						this.Backspace();
						return TMP_InputField.EditState.Continue;
					}
					if (keyCode != KeyCode.Return)
					{
						goto IL_1E0;
					}
				}
				else
				{
					if (keyCode == KeyCode.Escape)
					{
						this.m_WasCanceled = true;
						return TMP_InputField.EditState.Finish;
					}
					if (keyCode != KeyCode.A)
					{
						goto IL_1E0;
					}
					if (flag4)
					{
						this.SelectAll();
						return TMP_InputField.EditState.Continue;
					}
					goto IL_1E0;
				}
			}
			else if (keyCode <= KeyCode.V)
			{
				if (keyCode != KeyCode.C)
				{
					if (keyCode != KeyCode.V)
					{
						goto IL_1E0;
					}
					if (flag4)
					{
						this.Append(TMP_InputField.clipboard);
						return TMP_InputField.EditState.Continue;
					}
					goto IL_1E0;
				}
				else
				{
					if (flag4)
					{
						if (this.inputType != TMP_InputField.InputType.Password)
						{
							TMP_InputField.clipboard = this.GetSelectedString();
						}
						else
						{
							TMP_InputField.clipboard = "";
						}
						return TMP_InputField.EditState.Continue;
					}
					goto IL_1E0;
				}
			}
			else if (keyCode != KeyCode.X)
			{
				if (keyCode == KeyCode.Delete)
				{
					this.ForwardSpace();
					return TMP_InputField.EditState.Continue;
				}
				switch (keyCode)
				{
				case KeyCode.KeypadEnter:
					break;
				case KeyCode.KeypadEquals:
				case KeyCode.Insert:
					goto IL_1E0;
				case KeyCode.UpArrow:
					this.MoveUp(flag2);
					return TMP_InputField.EditState.Continue;
				case KeyCode.DownArrow:
					this.MoveDown(flag2);
					return TMP_InputField.EditState.Continue;
				case KeyCode.RightArrow:
					this.MoveRight(flag2, flag);
					return TMP_InputField.EditState.Continue;
				case KeyCode.LeftArrow:
					this.MoveLeft(flag2, flag);
					return TMP_InputField.EditState.Continue;
				case KeyCode.Home:
					this.MoveToStartOfLine(flag2, flag);
					return TMP_InputField.EditState.Continue;
				case KeyCode.End:
					this.MoveToEndOfLine(flag2, flag);
					return TMP_InputField.EditState.Continue;
				case KeyCode.PageUp:
					this.MovePageUp(flag2);
					return TMP_InputField.EditState.Continue;
				case KeyCode.PageDown:
					this.MovePageDown(flag2);
					return TMP_InputField.EditState.Continue;
				default:
					goto IL_1E0;
				}
			}
			else
			{
				if (flag4)
				{
					if (this.inputType != TMP_InputField.InputType.Password)
					{
						TMP_InputField.clipboard = this.GetSelectedString();
					}
					else
					{
						TMP_InputField.clipboard = "";
					}
					this.Delete();
					this.SendOnValueChangedAndUpdateLabel();
					return TMP_InputField.EditState.Continue;
				}
				goto IL_1E0;
			}
			if (this.lineType != TMP_InputField.LineType.MultiLineNewline)
			{
				return TMP_InputField.EditState.Finish;
			}
			IL_1E0:
			char c = evt.character;
			if (!this.multiLine && (c == '\t' || c == '\r' || c == '\n'))
			{
				return TMP_InputField.EditState.Continue;
			}
			if (c == '\r' || c == '\u0003')
			{
				c = '\n';
			}
			if (this.IsValidChar(c))
			{
				this.Append(c);
			}
			if (c == '\0' && Input.compositionString.Length > 0)
			{
				this.UpdateLabel();
			}
			return TMP_InputField.EditState.Continue;
		}

		// Token: 0x060001D1 RID: 465 RVA: 0x000193DA File Offset: 0x000175DA
		private bool IsValidChar(char c)
		{
			return c != '\u007f' && (c == '\t' || c == '\n' || this.m_TextComponent.font.HasCharacter(c, true));
		}

		// Token: 0x060001D2 RID: 466 RVA: 0x00019401 File Offset: 0x00017601
		public void ProcessEvent(Event e)
		{
			this.KeyPressed(e);
		}

		// Token: 0x060001D3 RID: 467 RVA: 0x0001940C File Offset: 0x0001760C
		public virtual void OnUpdateSelected(BaseEventData eventData)
		{
			if (!this.isFocused)
			{
				return;
			}
			bool flag = false;
			while (Event.PopEvent(this.m_ProcessingEvent))
			{
				if (this.m_ProcessingEvent.rawType == EventType.KeyDown)
				{
					flag = true;
					if (this.KeyPressed(this.m_ProcessingEvent) == TMP_InputField.EditState.Finish)
					{
						this.SendOnSubmit();
						this.DeactivateInputField();
						break;
					}
				}
				EventType type = this.m_ProcessingEvent.type;
				if (type - EventType.ValidateCommand <= 1)
				{
					string commandName = this.m_ProcessingEvent.commandName;
					if (commandName == "SelectAll")
					{
						this.SelectAll();
						flag = true;
					}
				}
			}
			if (flag)
			{
				this.UpdateLabel();
			}
			eventData.Use();
		}

		// Token: 0x060001D4 RID: 468 RVA: 0x000194A4 File Offset: 0x000176A4
		public virtual void OnScroll(PointerEventData eventData)
		{
			if (this.m_TextComponent.preferredHeight < this.m_TextViewport.rect.height)
			{
				return;
			}
			float num = -eventData.scrollDelta.y;
			this.m_ScrollPosition += 1f / (float)this.m_TextComponent.textInfo.lineCount * num * this.m_ScrollSensitivity;
			this.m_ScrollPosition = Mathf.Clamp01(this.m_ScrollPosition);
			this.AdjustTextPositionRelativeToViewport(this.m_ScrollPosition);
			this.m_AllowInput = false;
			if (this.m_VerticalScrollbar)
			{
				this.m_IsUpdatingScrollbarValues = true;
				this.m_VerticalScrollbar.value = this.m_ScrollPosition;
			}
		}

		// Token: 0x060001D5 RID: 469 RVA: 0x00019558 File Offset: 0x00017758
		private string GetSelectedString()
		{
			if (!this.hasSelection)
			{
				return "";
			}
			int num = this.stringPositionInternal;
			int num2 = this.stringSelectPositionInternal;
			if (num > num2)
			{
				int num3 = num;
				num = num2;
				num2 = num3;
			}
			return this.text.Substring(num, num2 - num);
		}

		// Token: 0x060001D6 RID: 470 RVA: 0x00019598 File Offset: 0x00017798
		private int FindtNextWordBegin()
		{
			if (this.stringSelectPositionInternal + 1 >= this.text.Length)
			{
				return this.text.Length;
			}
			int num = this.text.IndexOfAny(TMP_InputField.kSeparators, this.stringSelectPositionInternal + 1);
			if (num == -1)
			{
				num = this.text.Length;
			}
			else
			{
				num++;
			}
			return num;
		}

		// Token: 0x060001D7 RID: 471 RVA: 0x000195F8 File Offset: 0x000177F8
		private void MoveRight(bool shift, bool ctrl)
		{
			if (this.hasSelection && !shift)
			{
				this.stringPositionInternal = (this.stringSelectPositionInternal = Mathf.Max(this.stringPositionInternal, this.stringSelectPositionInternal));
				this.caretPositionInternal = (this.caretSelectPositionInternal = this.GetCaretPositionFromStringIndex(this.stringSelectPositionInternal));
				return;
			}
			int num;
			if (ctrl)
			{
				num = this.FindtNextWordBegin();
			}
			else if (this.m_isRichTextEditingAllowed)
			{
				num = this.stringSelectPositionInternal + 1;
			}
			else
			{
				num = this.GetStringIndexFromCaretPosition(this.caretSelectPositionInternal + 1);
			}
			if (shift)
			{
				this.stringSelectPositionInternal = num;
				this.caretSelectPositionInternal = this.GetCaretPositionFromStringIndex(this.stringSelectPositionInternal);
				return;
			}
			this.stringSelectPositionInternal = (this.stringPositionInternal = num);
			this.caretSelectPositionInternal = (this.caretPositionInternal = this.GetCaretPositionFromStringIndex(this.stringSelectPositionInternal));
		}

		// Token: 0x060001D8 RID: 472 RVA: 0x000196C4 File Offset: 0x000178C4
		private int FindtPrevWordBegin()
		{
			if (this.stringSelectPositionInternal - 2 < 0)
			{
				return 0;
			}
			int num = this.text.LastIndexOfAny(TMP_InputField.kSeparators, this.stringSelectPositionInternal - 2);
			if (num == -1)
			{
				num = 0;
			}
			else
			{
				num++;
			}
			return num;
		}

		// Token: 0x060001D9 RID: 473 RVA: 0x00019704 File Offset: 0x00017904
		private void MoveLeft(bool shift, bool ctrl)
		{
			if (this.hasSelection && !shift)
			{
				this.stringPositionInternal = (this.stringSelectPositionInternal = Mathf.Min(this.stringPositionInternal, this.stringSelectPositionInternal));
				this.caretPositionInternal = (this.caretSelectPositionInternal = this.GetCaretPositionFromStringIndex(this.stringSelectPositionInternal));
				return;
			}
			int num;
			if (ctrl)
			{
				num = this.FindtPrevWordBegin();
			}
			else if (this.m_isRichTextEditingAllowed)
			{
				num = this.stringSelectPositionInternal - 1;
			}
			else
			{
				num = this.GetStringIndexFromCaretPosition(this.caretSelectPositionInternal - 1);
			}
			if (shift)
			{
				this.stringSelectPositionInternal = num;
				this.caretSelectPositionInternal = this.GetCaretPositionFromStringIndex(this.stringSelectPositionInternal);
				return;
			}
			this.stringSelectPositionInternal = (this.stringPositionInternal = num);
			this.caretSelectPositionInternal = (this.caretPositionInternal = this.GetCaretPositionFromStringIndex(this.stringSelectPositionInternal));
		}

		// Token: 0x060001DA RID: 474 RVA: 0x000197D0 File Offset: 0x000179D0
		private int LineUpCharacterPosition(int originalPos, bool goToFirstChar)
		{
			if (originalPos >= this.m_TextComponent.textInfo.characterCount)
			{
				originalPos--;
			}
			TMP_CharacterInfo tmp_CharacterInfo = this.m_TextComponent.textInfo.characterInfo[originalPos];
			int lineNumber = tmp_CharacterInfo.lineNumber;
			if (lineNumber - 1 < 0)
			{
				if (!goToFirstChar)
				{
					return originalPos;
				}
				return 0;
			}
			else
			{
				int num = this.m_TextComponent.textInfo.lineInfo[lineNumber].firstCharacterIndex - 1;
				int num2 = -1;
				float num3 = 32767f;
				float num4 = 0f;
				int i = this.m_TextComponent.textInfo.lineInfo[lineNumber - 1].firstCharacterIndex;
				while (i < num)
				{
					TMP_CharacterInfo tmp_CharacterInfo2 = this.m_TextComponent.textInfo.characterInfo[i];
					float num5 = tmp_CharacterInfo.origin - tmp_CharacterInfo2.origin;
					float num6 = num5 / (tmp_CharacterInfo2.xAdvance - tmp_CharacterInfo2.origin);
					if (num6 >= 0f && num6 <= 1f)
					{
						if (num6 < 0.5f)
						{
							return i;
						}
						return i + 1;
					}
					else
					{
						num5 = Mathf.Abs(num5);
						if (num5 < num3)
						{
							num2 = i;
							num3 = num5;
							num4 = num6;
						}
						i++;
					}
				}
				if (num2 == -1)
				{
					return num;
				}
				if (num4 < 0.5f)
				{
					return num2;
				}
				return num2 + 1;
			}
		}

		// Token: 0x060001DB RID: 475 RVA: 0x00019910 File Offset: 0x00017B10
		private int LineDownCharacterPosition(int originalPos, bool goToLastChar)
		{
			if (originalPos >= this.m_TextComponent.textInfo.characterCount)
			{
				return this.m_TextComponent.textInfo.characterCount - 1;
			}
			TMP_CharacterInfo tmp_CharacterInfo = this.m_TextComponent.textInfo.characterInfo[originalPos];
			int lineNumber = tmp_CharacterInfo.lineNumber;
			if (lineNumber + 1 >= this.m_TextComponent.textInfo.lineCount)
			{
				if (!goToLastChar)
				{
					return originalPos;
				}
				return this.m_TextComponent.textInfo.characterCount - 1;
			}
			else
			{
				int lastCharacterIndex = this.m_TextComponent.textInfo.lineInfo[lineNumber + 1].lastCharacterIndex;
				int num = -1;
				float num2 = 32767f;
				float num3 = 0f;
				int i = this.m_TextComponent.textInfo.lineInfo[lineNumber + 1].firstCharacterIndex;
				while (i < lastCharacterIndex)
				{
					TMP_CharacterInfo tmp_CharacterInfo2 = this.m_TextComponent.textInfo.characterInfo[i];
					float num4 = tmp_CharacterInfo.origin - tmp_CharacterInfo2.origin;
					float num5 = num4 / (tmp_CharacterInfo2.xAdvance - tmp_CharacterInfo2.origin);
					if (num5 >= 0f && num5 <= 1f)
					{
						if (num5 < 0.5f)
						{
							return i;
						}
						return i + 1;
					}
					else
					{
						num4 = Mathf.Abs(num4);
						if (num4 < num2)
						{
							num = i;
							num2 = num4;
							num3 = num5;
						}
						i++;
					}
				}
				if (num == -1)
				{
					return lastCharacterIndex;
				}
				if (num3 < 0.5f)
				{
					return num;
				}
				return num + 1;
			}
		}

		// Token: 0x060001DC RID: 476 RVA: 0x00019A7C File Offset: 0x00017C7C
		private int PageUpCharacterPosition(int originalPos, bool goToFirstChar)
		{
			if (originalPos >= this.m_TextComponent.textInfo.characterCount)
			{
				originalPos--;
			}
			TMP_CharacterInfo tmp_CharacterInfo = this.m_TextComponent.textInfo.characterInfo[originalPos];
			int lineNumber = tmp_CharacterInfo.lineNumber;
			if (lineNumber - 1 < 0)
			{
				if (!goToFirstChar)
				{
					return originalPos;
				}
				return 0;
			}
			else
			{
				float height = this.m_TextViewport.rect.height;
				int num = lineNumber - 1;
				while (num > 0 && this.m_TextComponent.textInfo.lineInfo[num].baseline <= this.m_TextComponent.textInfo.lineInfo[lineNumber].baseline + height)
				{
					num--;
				}
				int lastCharacterIndex = this.m_TextComponent.textInfo.lineInfo[num].lastCharacterIndex;
				int num2 = -1;
				float num3 = 32767f;
				float num4 = 0f;
				int i = this.m_TextComponent.textInfo.lineInfo[num].firstCharacterIndex;
				while (i < lastCharacterIndex)
				{
					TMP_CharacterInfo tmp_CharacterInfo2 = this.m_TextComponent.textInfo.characterInfo[i];
					float num5 = tmp_CharacterInfo.origin - tmp_CharacterInfo2.origin;
					float num6 = num5 / (tmp_CharacterInfo2.xAdvance - tmp_CharacterInfo2.origin);
					if (num6 >= 0f && num6 <= 1f)
					{
						if (num6 < 0.5f)
						{
							return i;
						}
						return i + 1;
					}
					else
					{
						num5 = Mathf.Abs(num5);
						if (num5 < num3)
						{
							num2 = i;
							num3 = num5;
							num4 = num6;
						}
						i++;
					}
				}
				if (num2 == -1)
				{
					return lastCharacterIndex;
				}
				if (num4 < 0.5f)
				{
					return num2;
				}
				return num2 + 1;
			}
		}

		// Token: 0x060001DD RID: 477 RVA: 0x00019C1C File Offset: 0x00017E1C
		private int PageDownCharacterPosition(int originalPos, bool goToLastChar)
		{
			if (originalPos >= this.m_TextComponent.textInfo.characterCount)
			{
				return this.m_TextComponent.textInfo.characterCount - 1;
			}
			TMP_CharacterInfo tmp_CharacterInfo = this.m_TextComponent.textInfo.characterInfo[originalPos];
			int lineNumber = tmp_CharacterInfo.lineNumber;
			if (lineNumber + 1 >= this.m_TextComponent.textInfo.lineCount)
			{
				if (!goToLastChar)
				{
					return originalPos;
				}
				return this.m_TextComponent.textInfo.characterCount - 1;
			}
			else
			{
				float height = this.m_TextViewport.rect.height;
				int num = lineNumber + 1;
				while (num < this.m_TextComponent.textInfo.lineCount - 1 && this.m_TextComponent.textInfo.lineInfo[num].baseline >= this.m_TextComponent.textInfo.lineInfo[lineNumber].baseline - height)
				{
					num++;
				}
				int lastCharacterIndex = this.m_TextComponent.textInfo.lineInfo[num].lastCharacterIndex;
				int num2 = -1;
				float num3 = 32767f;
				float num4 = 0f;
				int i = this.m_TextComponent.textInfo.lineInfo[num].firstCharacterIndex;
				while (i < lastCharacterIndex)
				{
					TMP_CharacterInfo tmp_CharacterInfo2 = this.m_TextComponent.textInfo.characterInfo[i];
					float num5 = tmp_CharacterInfo.origin - tmp_CharacterInfo2.origin;
					float num6 = num5 / (tmp_CharacterInfo2.xAdvance - tmp_CharacterInfo2.origin);
					if (num6 >= 0f && num6 <= 1f)
					{
						if (num6 < 0.5f)
						{
							return i;
						}
						return i + 1;
					}
					else
					{
						num5 = Mathf.Abs(num5);
						if (num5 < num3)
						{
							num2 = i;
							num3 = num5;
							num4 = num6;
						}
						i++;
					}
				}
				if (num2 == -1)
				{
					return lastCharacterIndex;
				}
				if (num4 < 0.5f)
				{
					return num2;
				}
				return num2 + 1;
			}
		}

		// Token: 0x060001DE RID: 478 RVA: 0x00019DFA File Offset: 0x00017FFA
		private void MoveDown(bool shift)
		{
			this.MoveDown(shift, true);
		}

		// Token: 0x060001DF RID: 479 RVA: 0x00019E04 File Offset: 0x00018004
		private void MoveDown(bool shift, bool goToLastChar)
		{
			if (this.hasSelection && !shift)
			{
				this.caretPositionInternal = (this.caretSelectPositionInternal = Mathf.Max(this.caretPositionInternal, this.caretSelectPositionInternal));
			}
			int num = this.multiLine ? this.LineDownCharacterPosition(this.caretSelectPositionInternal, goToLastChar) : (this.m_TextComponent.textInfo.characterCount - 1);
			if (shift)
			{
				this.caretSelectPositionInternal = num;
				this.stringSelectPositionInternal = this.GetStringIndexFromCaretPosition(this.caretSelectPositionInternal);
				return;
			}
			this.caretSelectPositionInternal = (this.caretPositionInternal = num);
			this.stringSelectPositionInternal = (this.stringPositionInternal = this.GetStringIndexFromCaretPosition(this.caretSelectPositionInternal));
		}

		// Token: 0x060001E0 RID: 480 RVA: 0x00019EAE File Offset: 0x000180AE
		private void MoveUp(bool shift)
		{
			this.MoveUp(shift, true);
		}

		// Token: 0x060001E1 RID: 481 RVA: 0x00019EB8 File Offset: 0x000180B8
		private void MoveUp(bool shift, bool goToFirstChar)
		{
			if (this.hasSelection && !shift)
			{
				this.caretPositionInternal = (this.caretSelectPositionInternal = Mathf.Min(this.caretPositionInternal, this.caretSelectPositionInternal));
			}
			int num = this.multiLine ? this.LineUpCharacterPosition(this.caretSelectPositionInternal, goToFirstChar) : 0;
			if (shift)
			{
				this.caretSelectPositionInternal = num;
				this.stringSelectPositionInternal = this.GetStringIndexFromCaretPosition(this.caretSelectPositionInternal);
				return;
			}
			this.caretSelectPositionInternal = (this.caretPositionInternal = num);
			this.stringSelectPositionInternal = (this.stringPositionInternal = this.GetStringIndexFromCaretPosition(this.caretSelectPositionInternal));
		}

		// Token: 0x060001E2 RID: 482 RVA: 0x00019F51 File Offset: 0x00018151
		private void MovePageUp(bool shift)
		{
			this.MovePageUp(shift, true);
		}

		// Token: 0x060001E3 RID: 483 RVA: 0x00019F5C File Offset: 0x0001815C
		private void MovePageUp(bool shift, bool goToFirstChar)
		{
			if (this.hasSelection && !shift)
			{
				this.caretPositionInternal = (this.caretSelectPositionInternal = Mathf.Min(this.caretPositionInternal, this.caretSelectPositionInternal));
			}
			int num = this.multiLine ? this.PageUpCharacterPosition(this.caretSelectPositionInternal, goToFirstChar) : 0;
			if (shift)
			{
				this.caretSelectPositionInternal = num;
				this.stringSelectPositionInternal = this.GetStringIndexFromCaretPosition(this.caretSelectPositionInternal);
			}
			else
			{
				this.caretSelectPositionInternal = (this.caretPositionInternal = num);
				this.stringSelectPositionInternal = (this.stringPositionInternal = this.GetStringIndexFromCaretPosition(this.caretSelectPositionInternal));
			}
			if (this.m_LineType != TMP_InputField.LineType.SingleLine)
			{
				float num2 = this.m_TextViewport.rect.height;
				float num3 = this.m_TextComponent.rectTransform.position.y + this.m_TextComponent.textBounds.max.y;
				float num4 = this.m_TextViewport.position.y + this.m_TextViewport.rect.yMax;
				num2 = ((num4 > num3 + num2) ? num2 : (num4 - num3));
				this.m_TextComponent.rectTransform.anchoredPosition += new Vector2(0f, num2);
				this.AssignPositioningIfNeeded();
				this.m_IsScrollbarUpdateRequired = true;
			}
		}

		// Token: 0x060001E4 RID: 484 RVA: 0x0001A0AF File Offset: 0x000182AF
		private void MovePageDown(bool shift)
		{
			this.MovePageDown(shift, true);
		}

		// Token: 0x060001E5 RID: 485 RVA: 0x0001A0BC File Offset: 0x000182BC
		private void MovePageDown(bool shift, bool goToLastChar)
		{
			if (this.hasSelection && !shift)
			{
				this.caretPositionInternal = (this.caretSelectPositionInternal = Mathf.Max(this.caretPositionInternal, this.caretSelectPositionInternal));
			}
			int num = this.multiLine ? this.PageDownCharacterPosition(this.caretSelectPositionInternal, goToLastChar) : (this.m_TextComponent.textInfo.characterCount - 1);
			if (shift)
			{
				this.caretSelectPositionInternal = num;
				this.stringSelectPositionInternal = this.GetStringIndexFromCaretPosition(this.caretSelectPositionInternal);
			}
			else
			{
				this.caretSelectPositionInternal = (this.caretPositionInternal = num);
				this.stringSelectPositionInternal = (this.stringPositionInternal = this.GetStringIndexFromCaretPosition(this.caretSelectPositionInternal));
			}
			if (this.m_LineType != TMP_InputField.LineType.SingleLine)
			{
				float num2 = this.m_TextViewport.rect.height;
				float num3 = this.m_TextComponent.rectTransform.position.y + this.m_TextComponent.textBounds.min.y;
				float num4 = this.m_TextViewport.position.y + this.m_TextViewport.rect.yMin;
				num2 = ((num4 > num3 + num2) ? num2 : (num4 - num3));
				this.m_TextComponent.rectTransform.anchoredPosition += new Vector2(0f, num2);
				this.AssignPositioningIfNeeded();
				this.m_IsScrollbarUpdateRequired = true;
			}
		}

		// Token: 0x060001E6 RID: 486 RVA: 0x0001A220 File Offset: 0x00018420
		private void Delete()
		{
			if (this.m_ReadOnly)
			{
				return;
			}
			if (this.stringPositionInternal == this.stringSelectPositionInternal)
			{
				return;
			}
			if (this.m_isRichTextEditingAllowed || this.m_isSelectAll)
			{
				if (this.stringPositionInternal < this.stringSelectPositionInternal)
				{
					this.m_Text = this.text.Substring(0, this.stringPositionInternal) + this.text.Substring(this.stringSelectPositionInternal, this.text.Length - this.stringSelectPositionInternal);
					this.stringSelectPositionInternal = this.stringPositionInternal;
				}
				else
				{
					this.m_Text = this.text.Substring(0, this.stringSelectPositionInternal) + this.text.Substring(this.stringPositionInternal, this.text.Length - this.stringPositionInternal);
					this.stringPositionInternal = this.stringSelectPositionInternal;
				}
				this.m_isSelectAll = false;
				return;
			}
			this.stringPositionInternal = this.GetStringIndexFromCaretPosition(this.caretPositionInternal);
			this.stringSelectPositionInternal = this.GetStringIndexFromCaretPosition(this.caretSelectPositionInternal);
			if (this.caretPositionInternal < this.caretSelectPositionInternal)
			{
				this.m_Text = this.text.Substring(0, this.stringPositionInternal) + this.text.Substring(this.stringSelectPositionInternal, this.text.Length - this.stringSelectPositionInternal);
				this.stringSelectPositionInternal = this.stringPositionInternal;
				this.caretSelectPositionInternal = this.caretPositionInternal;
				return;
			}
			this.m_Text = this.text.Substring(0, this.stringSelectPositionInternal) + this.text.Substring(this.stringPositionInternal, this.text.Length - this.stringPositionInternal);
			this.stringPositionInternal = this.stringSelectPositionInternal;
			this.stringPositionInternal = this.stringSelectPositionInternal;
			this.caretPositionInternal = this.caretSelectPositionInternal;
		}

		// Token: 0x060001E7 RID: 487 RVA: 0x0001A3F8 File Offset: 0x000185F8
		private void ForwardSpace()
		{
			if (this.m_ReadOnly)
			{
				return;
			}
			if (this.hasSelection)
			{
				this.Delete();
				this.SendOnValueChangedAndUpdateLabel();
				return;
			}
			if (this.m_isRichTextEditingAllowed)
			{
				if (this.stringPositionInternal < this.text.Length)
				{
					this.m_Text = this.text.Remove(this.stringPositionInternal, 1);
					this.SendOnValueChangedAndUpdateLabel();
					return;
				}
			}
			else if (this.caretPositionInternal < this.m_TextComponent.textInfo.characterCount - 1)
			{
				this.stringSelectPositionInternal = (this.stringPositionInternal = this.GetStringIndexFromCaretPosition(this.caretPositionInternal));
				this.m_Text = this.text.Remove(this.stringPositionInternal, 1);
				this.SendOnValueChangedAndUpdateLabel();
			}
		}

		// Token: 0x060001E8 RID: 488 RVA: 0x0001A4B0 File Offset: 0x000186B0
		private void Backspace()
		{
			if (this.m_ReadOnly)
			{
				return;
			}
			if (this.hasSelection)
			{
				this.Delete();
				this.SendOnValueChangedAndUpdateLabel();
				return;
			}
			if (this.m_isRichTextEditingAllowed)
			{
				if (this.stringPositionInternal > 0)
				{
					this.m_Text = this.text.Remove(this.stringPositionInternal - 1, 1);
					this.stringSelectPositionInternal = --this.stringPositionInternal;
					this.m_isLastKeyBackspace = true;
					this.SendOnValueChangedAndUpdateLabel();
					return;
				}
			}
			else
			{
				if (this.caretPositionInternal > 0)
				{
					this.m_Text = this.text.Remove(this.GetStringIndexFromCaretPosition(this.caretPositionInternal - 1), 1);
					this.caretSelectPositionInternal = --this.caretPositionInternal;
					this.stringSelectPositionInternal = (this.stringPositionInternal = this.GetStringIndexFromCaretPosition(this.caretPositionInternal));
				}
				this.m_isLastKeyBackspace = true;
				this.SendOnValueChangedAndUpdateLabel();
			}
		}

		// Token: 0x060001E9 RID: 489 RVA: 0x0001A598 File Offset: 0x00018798
		protected virtual void Append(string input)
		{
			if (this.m_ReadOnly)
			{
				return;
			}
			if (!this.InPlaceEditing())
			{
				return;
			}
			int i = 0;
			int length = input.Length;
			while (i < length)
			{
				char c = input[i];
				if (c >= ' ' || c == '\t' || c == '\r' || c == '\n' || c == '\n')
				{
					this.Append(c);
				}
				i++;
			}
		}

		// Token: 0x060001EA RID: 490 RVA: 0x0001A5F4 File Offset: 0x000187F4
		protected virtual void Append(char input)
		{
			if (this.m_ReadOnly)
			{
				return;
			}
			if (!this.InPlaceEditing())
			{
				return;
			}
			if (this.onValidateInput != null)
			{
				input = this.onValidateInput(this.text, this.stringPositionInternal, input);
			}
			else if (this.characterValidation == TMP_InputField.CharacterValidation.CustomValidator)
			{
				input = this.Validate(this.text, this.stringPositionInternal, input);
				if (input == '\0')
				{
					return;
				}
				this.SendOnValueChanged();
				this.UpdateLabel();
				return;
			}
			else if (this.characterValidation != TMP_InputField.CharacterValidation.None)
			{
				input = this.Validate(this.text, this.stringPositionInternal, input);
			}
			if (input == '\0')
			{
				return;
			}
			this.Insert(input);
		}

		// Token: 0x060001EB RID: 491 RVA: 0x0001A690 File Offset: 0x00018890
		private void Insert(char c)
		{
			if (this.m_ReadOnly)
			{
				return;
			}
			string text = c.ToString();
			this.Delete();
			if (this.characterLimit > 0 && this.text.Length >= this.characterLimit)
			{
				return;
			}
			this.m_Text = this.text.Insert(this.m_StringPosition, text);
			this.stringSelectPositionInternal = (this.stringPositionInternal += text.Length);
			this.SendOnValueChanged();
		}

		// Token: 0x060001EC RID: 492 RVA: 0x0001A70B File Offset: 0x0001890B
		private void SendOnValueChangedAndUpdateLabel()
		{
			this.SendOnValueChanged();
			this.UpdateLabel();
		}

		// Token: 0x060001ED RID: 493 RVA: 0x0001A719 File Offset: 0x00018919
		private void SendOnValueChanged()
		{
			if (this.onValueChanged != null)
			{
				this.onValueChanged.Invoke(this.text);
			}
		}

		// Token: 0x060001EE RID: 494 RVA: 0x0001A734 File Offset: 0x00018934
		protected void SendOnEndEdit()
		{
			if (this.onEndEdit != null)
			{
				this.onEndEdit.Invoke(this.m_Text);
			}
		}

		// Token: 0x060001EF RID: 495 RVA: 0x0001A74F File Offset: 0x0001894F
		protected void SendOnSubmit()
		{
			if (this.onSubmit != null)
			{
				this.onSubmit.Invoke(this.m_Text);
			}
		}

		// Token: 0x060001F0 RID: 496 RVA: 0x0001A76A File Offset: 0x0001896A
		protected void SendOnFocus()
		{
			if (this.onSelect != null)
			{
				this.onSelect.Invoke(this.m_Text);
			}
		}

		// Token: 0x060001F1 RID: 497 RVA: 0x0001A785 File Offset: 0x00018985
		protected void SendOnFocusLost()
		{
			if (this.onDeselect != null)
			{
				this.onDeselect.Invoke(this.m_Text);
			}
		}

		// Token: 0x060001F2 RID: 498 RVA: 0x0001A7A0 File Offset: 0x000189A0
		protected void SendOnTextSelection()
		{
			this.m_isSelected = true;
			if (this.onTextSelection != null)
			{
				this.onTextSelection.Invoke(this.m_Text, this.stringPositionInternal, this.stringSelectPositionInternal);
			}
		}

		// Token: 0x060001F3 RID: 499 RVA: 0x0001A7CE File Offset: 0x000189CE
		protected void SendOnEndTextSelection()
		{
			if (!this.m_isSelected)
			{
				return;
			}
			if (this.onEndTextSelection != null)
			{
				this.onEndTextSelection.Invoke(this.m_Text, this.stringPositionInternal, this.stringSelectPositionInternal);
			}
			this.m_isSelected = false;
		}

		// Token: 0x060001F4 RID: 500 RVA: 0x0001A808 File Offset: 0x00018A08
		protected void UpdateLabel()
		{
			if (this.m_TextComponent != null && this.m_TextComponent.font != null)
			{
				string text;
				if (Input.compositionString.Length > 0)
				{
					text = this.text.Substring(0, this.m_StringPosition) + Input.compositionString + this.text.Substring(this.m_StringPosition);
				}
				else
				{
					text = this.text;
				}
				string str;
				if (this.inputType == TMP_InputField.InputType.Password)
				{
					str = new string(this.asteriskChar, text.Length);
				}
				else
				{
					str = text;
				}
				bool flag = string.IsNullOrEmpty(text);
				if (this.m_Placeholder != null)
				{
					this.m_Placeholder.enabled = flag;
				}
				if (!flag)
				{
					this.SetCaretVisible();
				}
				this.m_TextComponent.text = str + "​";
				this.MarkGeometryAsDirty();
				this.m_IsScrollbarUpdateRequired = true;
			}
		}

		// Token: 0x060001F5 RID: 501 RVA: 0x0001A8EC File Offset: 0x00018AEC
		private void UpdateScrollbar()
		{
			if (this.m_VerticalScrollbar)
			{
				float size = this.m_TextViewport.rect.height / this.m_TextComponent.preferredHeight;
				this.m_IsUpdatingScrollbarValues = true;
				this.m_VerticalScrollbar.size = size;
				this.m_ScrollPosition = (this.m_VerticalScrollbar.value = this.m_TextComponent.rectTransform.anchoredPosition.y / (this.m_TextComponent.preferredHeight - this.m_TextViewport.rect.height));
			}
		}

		// Token: 0x060001F6 RID: 502 RVA: 0x0001A982 File Offset: 0x00018B82
		private void OnScrollbarValueChange(float value)
		{
			if (this.m_IsUpdatingScrollbarValues)
			{
				this.m_IsUpdatingScrollbarValues = false;
				return;
			}
			if (value < 0f || value > 1f)
			{
				return;
			}
			this.AdjustTextPositionRelativeToViewport(value);
			this.m_ScrollPosition = value;
		}

		// Token: 0x060001F7 RID: 503 RVA: 0x0001A9B4 File Offset: 0x00018BB4
		private void AdjustTextPositionRelativeToViewport(float relativePosition)
		{
			TMP_TextInfo textInfo = this.m_TextComponent.textInfo;
			if (textInfo == null || textInfo.lineInfo == null || textInfo.lineCount == 0 || textInfo.lineCount > textInfo.lineInfo.Length)
			{
				return;
			}
			this.m_TextComponent.rectTransform.anchoredPosition = new Vector2(this.m_TextComponent.rectTransform.anchoredPosition.x, (this.m_TextComponent.preferredHeight - this.m_TextViewport.rect.height) * relativePosition);
			this.AssignPositioningIfNeeded();
		}

		// Token: 0x060001F8 RID: 504 RVA: 0x0001AA44 File Offset: 0x00018C44
		private int GetCaretPositionFromStringIndex(int stringIndex)
		{
			int characterCount = this.m_TextComponent.textInfo.characterCount;
			for (int i = 0; i < characterCount; i++)
			{
				if (this.m_TextComponent.textInfo.characterInfo[i].index >= stringIndex)
				{
					return i;
				}
			}
			return characterCount;
		}

		// Token: 0x060001F9 RID: 505 RVA: 0x0001AA8F File Offset: 0x00018C8F
		private int GetStringIndexFromCaretPosition(int caretPosition)
		{
			this.ClampCaretPos(ref caretPosition);
			return this.m_TextComponent.textInfo.characterInfo[caretPosition].index;
		}

		// Token: 0x060001FA RID: 506 RVA: 0x0001AAB4 File Offset: 0x00018CB4
		public void ForceLabelUpdate()
		{
			this.UpdateLabel();
		}

		// Token: 0x060001FB RID: 507 RVA: 0x0001AABC File Offset: 0x00018CBC
		private void MarkGeometryAsDirty()
		{
			CanvasUpdateRegistry.RegisterCanvasElementForGraphicRebuild(this);
		}

		// Token: 0x060001FC RID: 508 RVA: 0x0001AAC4 File Offset: 0x00018CC4
		public virtual void Rebuild(CanvasUpdate update)
		{
			if (update == CanvasUpdate.LatePreRender)
			{
				this.UpdateGeometry();
			}
		}

		// Token: 0x060001FD RID: 509 RVA: 0x0000297D File Offset: 0x00000B7D
		public virtual void LayoutComplete()
		{
		}

		// Token: 0x060001FE RID: 510 RVA: 0x0000297D File Offset: 0x00000B7D
		public virtual void GraphicUpdateComplete()
		{
		}

		// Token: 0x060001FF RID: 511 RVA: 0x0001AAD0 File Offset: 0x00018CD0
		private void UpdateGeometry()
		{
			if (!this.shouldHideMobileInput)
			{
				return;
			}
			if (this.m_CachedInputRenderer == null)
			{
				return;
			}
			this.OnFillVBO(this.mesh);
			this.m_CachedInputRenderer.SetMesh(this.mesh);
		}

		// Token: 0x06000200 RID: 512 RVA: 0x0001AB08 File Offset: 0x00018D08
		private void AssignPositioningIfNeeded()
		{
			if (this.m_TextComponent != null && this.caretRectTrans != null && (this.caretRectTrans.localPosition != this.m_TextComponent.rectTransform.localPosition || this.caretRectTrans.localRotation != this.m_TextComponent.rectTransform.localRotation || this.caretRectTrans.localScale != this.m_TextComponent.rectTransform.localScale || this.caretRectTrans.anchorMin != this.m_TextComponent.rectTransform.anchorMin || this.caretRectTrans.anchorMax != this.m_TextComponent.rectTransform.anchorMax || this.caretRectTrans.anchoredPosition != this.m_TextComponent.rectTransform.anchoredPosition || this.caretRectTrans.sizeDelta != this.m_TextComponent.rectTransform.sizeDelta || this.caretRectTrans.pivot != this.m_TextComponent.rectTransform.pivot))
			{
				this.caretRectTrans.localPosition = this.m_TextComponent.rectTransform.localPosition;
				this.caretRectTrans.localRotation = this.m_TextComponent.rectTransform.localRotation;
				this.caretRectTrans.localScale = this.m_TextComponent.rectTransform.localScale;
				this.caretRectTrans.anchorMin = this.m_TextComponent.rectTransform.anchorMin;
				this.caretRectTrans.anchorMax = this.m_TextComponent.rectTransform.anchorMax;
				this.caretRectTrans.anchoredPosition = this.m_TextComponent.rectTransform.anchoredPosition;
				this.caretRectTrans.sizeDelta = this.m_TextComponent.rectTransform.sizeDelta;
				this.caretRectTrans.pivot = this.m_TextComponent.rectTransform.pivot;
			}
		}

		// Token: 0x06000201 RID: 513 RVA: 0x0001AD30 File Offset: 0x00018F30
		private void OnFillVBO(Mesh vbo)
		{
			using (VertexHelper vertexHelper = new VertexHelper())
			{
				if (!this.isFocused && this.m_ResetOnDeActivation)
				{
					vertexHelper.FillMesh(vbo);
				}
				else
				{
					if (this.isStringPositionDirty)
					{
						this.stringPositionInternal = this.GetStringIndexFromCaretPosition(this.m_CaretPosition);
						this.stringSelectPositionInternal = this.GetStringIndexFromCaretPosition(this.m_CaretSelectPosition);
						this.isStringPositionDirty = false;
					}
					if (!this.hasSelection)
					{
						this.GenerateCaret(vertexHelper, Vector2.zero);
						this.SendOnEndTextSelection();
					}
					else
					{
						this.GenerateHightlight(vertexHelper, Vector2.zero);
						this.SendOnTextSelection();
					}
					vertexHelper.FillMesh(vbo);
				}
			}
		}

		// Token: 0x06000202 RID: 514 RVA: 0x0001ADE0 File Offset: 0x00018FE0
		private void GenerateCaret(VertexHelper vbo, Vector2 roundingOffset)
		{
			if (!this.m_CaretVisible)
			{
				return;
			}
			if (this.m_CursorVerts == null)
			{
				this.CreateCursorVerts();
			}
			float num = (float)this.m_CaretWidth;
			int characterCount = this.m_TextComponent.textInfo.characterCount;
			Vector2 zero = Vector2.zero;
			this.caretPositionInternal = this.GetCaretPositionFromStringIndex(this.stringPositionInternal);
			TMP_CharacterInfo tmp_CharacterInfo;
			float num2;
			if (this.caretPositionInternal == 0)
			{
				tmp_CharacterInfo = this.m_TextComponent.textInfo.characterInfo[0];
				zero = new Vector2(tmp_CharacterInfo.origin, tmp_CharacterInfo.descender);
				num2 = tmp_CharacterInfo.ascender - tmp_CharacterInfo.descender;
			}
			else if (this.caretPositionInternal < characterCount)
			{
				tmp_CharacterInfo = this.m_TextComponent.textInfo.characterInfo[this.caretPositionInternal];
				zero = new Vector2(tmp_CharacterInfo.origin, tmp_CharacterInfo.descender);
				num2 = tmp_CharacterInfo.ascender - tmp_CharacterInfo.descender;
			}
			else
			{
				tmp_CharacterInfo = this.m_TextComponent.textInfo.characterInfo[characterCount - 1];
				zero = new Vector2(tmp_CharacterInfo.xAdvance, tmp_CharacterInfo.descender);
				num2 = tmp_CharacterInfo.ascender - tmp_CharacterInfo.descender;
			}
			if ((this.isFocused && zero != this.m_LastPosition) || this.m_forceRectTransformAdjustment)
			{
				this.AdjustRectTransformRelativeToViewport(zero, num2, tmp_CharacterInfo.isVisible);
			}
			this.m_LastPosition = zero;
			float num3 = zero.y + num2;
			float y = num3 - num2;
			this.m_CursorVerts[0].position = new Vector3(zero.x, y, 0f);
			this.m_CursorVerts[1].position = new Vector3(zero.x, num3, 0f);
			this.m_CursorVerts[2].position = new Vector3(zero.x + num, num3, 0f);
			this.m_CursorVerts[3].position = new Vector3(zero.x + num, y, 0f);
			this.m_CursorVerts[0].color = this.caretColor;
			this.m_CursorVerts[1].color = this.caretColor;
			this.m_CursorVerts[2].color = this.caretColor;
			this.m_CursorVerts[3].color = this.caretColor;
			vbo.AddUIVertexQuad(this.m_CursorVerts);
			int height = Screen.height;
			zero.y = (float)height - zero.y;
			Input.compositionCursorPos = zero;
		}

		// Token: 0x06000203 RID: 515 RVA: 0x0001B080 File Offset: 0x00019280
		private void CreateCursorVerts()
		{
			this.m_CursorVerts = new UIVertex[4];
			for (int i = 0; i < this.m_CursorVerts.Length; i++)
			{
				this.m_CursorVerts[i] = UIVertex.simpleVert;
				this.m_CursorVerts[i].uv0 = Vector2.zero;
			}
		}

		// Token: 0x06000204 RID: 516 RVA: 0x0001B0D4 File Offset: 0x000192D4
		private void GenerateHightlight(VertexHelper vbo, Vector2 roundingOffset)
		{
			TMP_TextInfo textInfo = this.m_TextComponent.textInfo;
			this.caretPositionInternal = (this.m_CaretPosition = this.GetCaretPositionFromStringIndex(this.stringPositionInternal));
			this.caretSelectPositionInternal = (this.m_CaretSelectPosition = this.GetCaretPositionFromStringIndex(this.stringSelectPositionInternal));
			Vector2 startPosition;
			float height;
			if (this.caretSelectPositionInternal < textInfo.characterCount)
			{
				startPosition = new Vector2(textInfo.characterInfo[this.caretSelectPositionInternal].origin, textInfo.characterInfo[this.caretSelectPositionInternal].descender);
				height = textInfo.characterInfo[this.caretSelectPositionInternal].ascender - textInfo.characterInfo[this.caretSelectPositionInternal].descender;
			}
			else
			{
				startPosition = new Vector2(textInfo.characterInfo[this.caretSelectPositionInternal - 1].xAdvance, textInfo.characterInfo[this.caretSelectPositionInternal - 1].descender);
				height = textInfo.characterInfo[this.caretSelectPositionInternal - 1].ascender - textInfo.characterInfo[this.caretSelectPositionInternal - 1].descender;
			}
			this.AdjustRectTransformRelativeToViewport(startPosition, height, true);
			int num = Mathf.Max(0, this.caretPositionInternal);
			int num2 = Mathf.Max(0, this.caretSelectPositionInternal);
			if (num > num2)
			{
				int num3 = num;
				num = num2;
				num2 = num3;
			}
			num2--;
			int num4 = textInfo.characterInfo[num].lineNumber;
			int lastCharacterIndex = textInfo.lineInfo[num4].lastCharacterIndex;
			UIVertex simpleVert = UIVertex.simpleVert;
			simpleVert.uv0 = Vector2.zero;
			simpleVert.color = this.selectionColor;
			int num5 = num;
			while (num5 <= num2 && num5 < textInfo.characterCount)
			{
				if (num5 == lastCharacterIndex || num5 == num2)
				{
					TMP_CharacterInfo tmp_CharacterInfo = textInfo.characterInfo[num];
					TMP_CharacterInfo tmp_CharacterInfo2 = textInfo.characterInfo[num5];
					if (num5 > 0 && tmp_CharacterInfo2.character == '\n' && textInfo.characterInfo[num5 - 1].character == '\r')
					{
						tmp_CharacterInfo2 = textInfo.characterInfo[num5 - 1];
					}
					Vector2 vector = new Vector2(tmp_CharacterInfo.origin, textInfo.lineInfo[num4].ascender);
					Vector2 vector2 = new Vector2(tmp_CharacterInfo2.xAdvance, textInfo.lineInfo[num4].descender);
					int currentVertCount = vbo.currentVertCount;
					simpleVert.position = new Vector3(vector.x, vector2.y, 0f);
					vbo.AddVert(simpleVert);
					simpleVert.position = new Vector3(vector2.x, vector2.y, 0f);
					vbo.AddVert(simpleVert);
					simpleVert.position = new Vector3(vector2.x, vector.y, 0f);
					vbo.AddVert(simpleVert);
					simpleVert.position = new Vector3(vector.x, vector.y, 0f);
					vbo.AddVert(simpleVert);
					vbo.AddTriangle(currentVertCount, currentVertCount + 1, currentVertCount + 2);
					vbo.AddTriangle(currentVertCount + 2, currentVertCount + 3, currentVertCount);
					num = num5 + 1;
					num4++;
					if (num4 < textInfo.lineCount)
					{
						lastCharacterIndex = textInfo.lineInfo[num4].lastCharacterIndex;
					}
				}
				num5++;
			}
			this.m_IsScrollbarUpdateRequired = true;
		}

		// Token: 0x06000205 RID: 517 RVA: 0x0001B444 File Offset: 0x00019644
		private void AdjustRectTransformRelativeToViewport(Vector2 startPosition, float height, bool isCharVisible)
		{
			float xMin = this.m_TextViewport.rect.xMin;
			float xMax = this.m_TextViewport.rect.xMax;
			float num = xMax - (this.m_TextComponent.rectTransform.anchoredPosition.x + startPosition.x + this.m_TextComponent.margin.z + (float)this.m_CaretWidth);
			if (num < 0f && (!this.multiLine || (this.multiLine && isCharVisible)))
			{
				this.m_TextComponent.rectTransform.anchoredPosition += new Vector2(num, 0f);
				this.AssignPositioningIfNeeded();
			}
			float num2 = this.m_TextComponent.rectTransform.anchoredPosition.x + startPosition.x - this.m_TextComponent.margin.x - xMin;
			if (num2 < 0f)
			{
				this.m_TextComponent.rectTransform.anchoredPosition += new Vector2(-num2, 0f);
				this.AssignPositioningIfNeeded();
			}
			if (this.m_LineType != TMP_InputField.LineType.SingleLine)
			{
				float num3 = this.m_TextViewport.rect.yMax - (this.m_TextComponent.rectTransform.anchoredPosition.y + startPosition.y + height);
				if (num3 < -0.0001f)
				{
					this.m_TextComponent.rectTransform.anchoredPosition += new Vector2(0f, num3);
					this.AssignPositioningIfNeeded();
					this.m_IsScrollbarUpdateRequired = true;
				}
				float num4 = this.m_TextComponent.rectTransform.anchoredPosition.y + startPosition.y - this.m_TextViewport.rect.yMin;
				if (num4 < 0f)
				{
					this.m_TextComponent.rectTransform.anchoredPosition -= new Vector2(0f, num4);
					this.AssignPositioningIfNeeded();
					this.m_IsScrollbarUpdateRequired = true;
				}
			}
			if (this.m_isLastKeyBackspace)
			{
				float num5 = this.m_TextComponent.rectTransform.anchoredPosition.x + this.m_TextComponent.textInfo.characterInfo[0].origin - this.m_TextComponent.margin.x;
				float num6 = this.m_TextComponent.rectTransform.anchoredPosition.x + this.m_TextComponent.textInfo.characterInfo[this.m_TextComponent.textInfo.characterCount - 1].origin + this.m_TextComponent.margin.z;
				if (this.m_TextComponent.rectTransform.anchoredPosition.x + startPosition.x <= xMin + 0.0001f)
				{
					if (num5 < xMin)
					{
						float x = Mathf.Min((xMax - xMin) / 2f, xMin - num5);
						this.m_TextComponent.rectTransform.anchoredPosition += new Vector2(x, 0f);
						this.AssignPositioningIfNeeded();
					}
				}
				else if (num6 < xMax && num5 < xMin)
				{
					float x2 = Mathf.Min(xMax - num6, xMin - num5);
					this.m_TextComponent.rectTransform.anchoredPosition += new Vector2(x2, 0f);
					this.AssignPositioningIfNeeded();
				}
				this.m_isLastKeyBackspace = false;
			}
			this.m_forceRectTransformAdjustment = false;
		}

		// Token: 0x06000206 RID: 518 RVA: 0x0001B7B4 File Offset: 0x000199B4
		protected char Validate(string text, int pos, char ch)
		{
			if (this.characterValidation == TMP_InputField.CharacterValidation.None || !base.enabled)
			{
				return ch;
			}
			if (this.characterValidation == TMP_InputField.CharacterValidation.Integer || this.characterValidation == TMP_InputField.CharacterValidation.Decimal)
			{
				bool flag = pos == 0 && text.Length > 0 && text[0] == '-';
				bool flag2 = this.stringPositionInternal == 0 || this.stringSelectPositionInternal == 0;
				if (!flag)
				{
					if (ch >= '0' && ch <= '9')
					{
						return ch;
					}
					if (ch == '-' && (pos == 0 || flag2))
					{
						return ch;
					}
					if (ch == '.' && this.characterValidation == TMP_InputField.CharacterValidation.Decimal && !text.Contains("."))
					{
						return ch;
					}
				}
			}
			else if (this.characterValidation == TMP_InputField.CharacterValidation.Digit)
			{
				if (ch >= '0' && ch <= '9')
				{
					return ch;
				}
			}
			else if (this.characterValidation == TMP_InputField.CharacterValidation.Alphanumeric)
			{
				if (ch >= 'A' && ch <= 'Z')
				{
					return ch;
				}
				if (ch >= 'a' && ch <= 'z')
				{
					return ch;
				}
				if (ch >= '0' && ch <= '9')
				{
					return ch;
				}
			}
			else if (this.characterValidation == TMP_InputField.CharacterValidation.Name)
			{
				char c = (text.Length > 0) ? text[Mathf.Clamp(pos, 0, text.Length - 1)] : ' ';
				char c2 = (text.Length > 0) ? text[Mathf.Clamp(pos + 1, 0, text.Length - 1)] : '\n';
				if (char.IsLetter(ch))
				{
					if (char.IsLower(ch) && c == ' ')
					{
						return char.ToUpper(ch);
					}
					if (char.IsUpper(ch) && c != ' ' && c != '\'')
					{
						return char.ToLower(ch);
					}
					return ch;
				}
				else if (ch == '\'')
				{
					if (c != ' ' && c != '\'' && c2 != '\'' && !text.Contains("'"))
					{
						return ch;
					}
				}
				else if (ch == ' ' && c != ' ' && c != '\'' && c2 != ' ' && c2 != '\'')
				{
					return ch;
				}
			}
			else if (this.characterValidation == TMP_InputField.CharacterValidation.EmailAddress)
			{
				if (ch >= 'A' && ch <= 'Z')
				{
					return ch;
				}
				if (ch >= 'a' && ch <= 'z')
				{
					return ch;
				}
				if (ch >= '0' && ch <= '9')
				{
					return ch;
				}
				if (ch == '@' && text.IndexOf('@') == -1)
				{
					return ch;
				}
				if ("!#$%&'*+-/=?^_`{|}~".IndexOf(ch) != -1)
				{
					return ch;
				}
				if (ch == '.')
				{
					int num = (int)((text.Length > 0) ? text[Mathf.Clamp(pos, 0, text.Length - 1)] : ' ');
					char c3 = (text.Length > 0) ? text[Mathf.Clamp(pos + 1, 0, text.Length - 1)] : '\n';
					if (num != 46 && c3 != '.')
					{
						return ch;
					}
				}
			}
			else if (this.characterValidation == TMP_InputField.CharacterValidation.Regex)
			{
				if (Regex.IsMatch(ch.ToString(), this.m_RegexValue))
				{
					return ch;
				}
			}
			else if (this.characterValidation == TMP_InputField.CharacterValidation.CustomValidator && this.m_InputValidator != null)
			{
				char result = this.m_InputValidator.Validate(ref text, ref pos, ch);
				this.m_Text = text;
				this.stringSelectPositionInternal = (this.stringPositionInternal = pos);
				return result;
			}
			return '\0';
		}

		// Token: 0x06000207 RID: 519 RVA: 0x0001BA9C File Offset: 0x00019C9C
		public void ActivateInputField()
		{
			if (this.m_TextComponent == null || this.m_TextComponent.font == null || !this.IsActive() || !this.IsInteractable())
			{
				return;
			}
			if (this.isFocused && this.m_Keyboard != null && !this.m_Keyboard.active)
			{
				this.m_Keyboard.active = true;
				this.m_Keyboard.text = this.m_Text;
			}
			this.m_ShouldActivateNextUpdate = true;
		}

		// Token: 0x06000208 RID: 520 RVA: 0x0001BB1C File Offset: 0x00019D1C
		private void ActivateInputFieldInternal()
		{
			if (EventSystem.current == null)
			{
				return;
			}
			if (EventSystem.current.currentSelectedGameObject != base.gameObject)
			{
				EventSystem.current.SetSelectedGameObject(base.gameObject);
			}
			if (TouchScreenKeyboard.isSupported)
			{
				if (Input.touchSupported)
				{
					TouchScreenKeyboard.hideInput = this.shouldHideMobileInput;
				}
				this.m_Keyboard = ((this.inputType == TMP_InputField.InputType.Password) ? TouchScreenKeyboard.Open(this.m_Text, this.keyboardType, false, this.multiLine, true) : TouchScreenKeyboard.Open(this.m_Text, this.keyboardType, this.inputType == TMP_InputField.InputType.AutoCorrect, this.multiLine));
				this.MoveTextEnd(false);
			}
			else
			{
				Input.imeCompositionMode = IMECompositionMode.On;
				this.OnFocus();
			}
			this.m_AllowInput = true;
			this.m_OriginalText = this.text;
			this.m_WasCanceled = false;
			this.SetCaretVisible();
			this.UpdateLabel();
		}

		// Token: 0x06000209 RID: 521 RVA: 0x0001BBFC File Offset: 0x00019DFC
		public override void OnSelect(BaseEventData eventData)
		{
			base.OnSelect(eventData);
			this.SendOnFocus();
			this.ActivateInputField();
		}

		// Token: 0x0600020A RID: 522 RVA: 0x0001BC11 File Offset: 0x00019E11
		public virtual void OnPointerClick(PointerEventData eventData)
		{
			if (eventData.button != PointerEventData.InputButton.Left)
			{
				return;
			}
			this.ActivateInputField();
		}

		// Token: 0x0600020B RID: 523 RVA: 0x0000297D File Offset: 0x00000B7D
		public void OnControlClick()
		{
		}

		// Token: 0x0600020C RID: 524 RVA: 0x0001BC24 File Offset: 0x00019E24
		public void DeactivateInputField()
		{
			if (!this.m_AllowInput)
			{
				return;
			}
			this.m_HasDoneFocusTransition = false;
			this.m_AllowInput = false;
			if (this.m_Placeholder != null)
			{
				this.m_Placeholder.enabled = string.IsNullOrEmpty(this.m_Text);
			}
			if (this.m_TextComponent != null && this.IsInteractable())
			{
				if (this.m_WasCanceled && this.m_RestoreOriginalTextOnEscape)
				{
					this.text = this.m_OriginalText;
				}
				if (this.m_Keyboard != null)
				{
					this.m_Keyboard.active = false;
					this.m_Keyboard = null;
				}
				if (this.m_ResetOnDeActivation)
				{
					this.m_StringPosition = (this.m_StringSelectPosition = 0);
					this.m_CaretPosition = (this.m_CaretSelectPosition = 0);
					this.m_TextComponent.rectTransform.localPosition = this.m_DefaultTransformPosition;
					if (this.caretRectTrans != null)
					{
						this.caretRectTrans.localPosition = Vector3.zero;
					}
				}
				this.SendOnEndEdit();
				this.SendOnEndTextSelection();
				Input.imeCompositionMode = IMECompositionMode.Auto;
			}
			this.MarkGeometryAsDirty();
			this.m_IsScrollbarUpdateRequired = true;
		}

		// Token: 0x0600020D RID: 525 RVA: 0x0001BD3F File Offset: 0x00019F3F
		public override void OnDeselect(BaseEventData eventData)
		{
			this.DeactivateInputField();
			base.OnDeselect(eventData);
			this.SendOnFocusLost();
		}

		// Token: 0x0600020E RID: 526 RVA: 0x0001BD54 File Offset: 0x00019F54
		public virtual void OnSubmit(BaseEventData eventData)
		{
			if (!this.IsActive() || !this.IsInteractable())
			{
				return;
			}
			if (!this.isFocused)
			{
				this.m_ShouldActivateNextUpdate = true;
			}
			this.SendOnSubmit();
		}

		// Token: 0x0600020F RID: 527 RVA: 0x0001BD7C File Offset: 0x00019F7C
		private void EnforceContentType()
		{
			switch (this.contentType)
			{
			case TMP_InputField.ContentType.Standard:
				this.m_InputType = TMP_InputField.InputType.Standard;
				this.m_KeyboardType = TouchScreenKeyboardType.Default;
				this.m_CharacterValidation = TMP_InputField.CharacterValidation.None;
				return;
			case TMP_InputField.ContentType.Autocorrected:
				this.m_InputType = TMP_InputField.InputType.AutoCorrect;
				this.m_KeyboardType = TouchScreenKeyboardType.Default;
				this.m_CharacterValidation = TMP_InputField.CharacterValidation.None;
				return;
			case TMP_InputField.ContentType.IntegerNumber:
				this.m_LineType = TMP_InputField.LineType.SingleLine;
				this.m_TextComponent.enableWordWrapping = false;
				this.m_InputType = TMP_InputField.InputType.Standard;
				this.m_KeyboardType = TouchScreenKeyboardType.NumberPad;
				this.m_CharacterValidation = TMP_InputField.CharacterValidation.Integer;
				return;
			case TMP_InputField.ContentType.DecimalNumber:
				this.m_LineType = TMP_InputField.LineType.SingleLine;
				this.m_TextComponent.enableWordWrapping = false;
				this.m_InputType = TMP_InputField.InputType.Standard;
				this.m_KeyboardType = TouchScreenKeyboardType.NumbersAndPunctuation;
				this.m_CharacterValidation = TMP_InputField.CharacterValidation.Decimal;
				return;
			case TMP_InputField.ContentType.Alphanumeric:
				this.m_LineType = TMP_InputField.LineType.SingleLine;
				this.m_TextComponent.enableWordWrapping = false;
				this.m_InputType = TMP_InputField.InputType.Standard;
				this.m_KeyboardType = TouchScreenKeyboardType.ASCIICapable;
				this.m_CharacterValidation = TMP_InputField.CharacterValidation.Alphanumeric;
				return;
			case TMP_InputField.ContentType.Name:
				this.m_LineType = TMP_InputField.LineType.SingleLine;
				this.m_TextComponent.enableWordWrapping = false;
				this.m_InputType = TMP_InputField.InputType.Standard;
				this.m_KeyboardType = TouchScreenKeyboardType.Default;
				this.m_CharacterValidation = TMP_InputField.CharacterValidation.Name;
				return;
			case TMP_InputField.ContentType.EmailAddress:
				this.m_LineType = TMP_InputField.LineType.SingleLine;
				this.m_TextComponent.enableWordWrapping = false;
				this.m_InputType = TMP_InputField.InputType.Standard;
				this.m_KeyboardType = TouchScreenKeyboardType.EmailAddress;
				this.m_CharacterValidation = TMP_InputField.CharacterValidation.EmailAddress;
				return;
			case TMP_InputField.ContentType.Password:
				this.m_LineType = TMP_InputField.LineType.SingleLine;
				this.m_TextComponent.enableWordWrapping = false;
				this.m_InputType = TMP_InputField.InputType.Password;
				this.m_KeyboardType = TouchScreenKeyboardType.Default;
				this.m_CharacterValidation = TMP_InputField.CharacterValidation.None;
				return;
			case TMP_InputField.ContentType.Pin:
				this.m_LineType = TMP_InputField.LineType.SingleLine;
				this.m_TextComponent.enableWordWrapping = false;
				this.m_InputType = TMP_InputField.InputType.Password;
				this.m_KeyboardType = TouchScreenKeyboardType.NumberPad;
				this.m_CharacterValidation = TMP_InputField.CharacterValidation.Digit;
				return;
			default:
				return;
			}
		}

		// Token: 0x06000210 RID: 528 RVA: 0x0001BF05 File Offset: 0x0001A105
		private void SetTextComponentWrapMode()
		{
			if (this.m_TextComponent == null)
			{
				return;
			}
			if (this.m_LineType == TMP_InputField.LineType.SingleLine)
			{
				this.m_TextComponent.enableWordWrapping = false;
				return;
			}
			this.m_TextComponent.enableWordWrapping = true;
		}

		// Token: 0x06000211 RID: 529 RVA: 0x0001BF37 File Offset: 0x0001A137
		private void SetTextComponentRichTextMode()
		{
			if (this.m_TextComponent == null)
			{
				return;
			}
			this.m_TextComponent.richText = this.m_RichText;
		}

		// Token: 0x06000212 RID: 530 RVA: 0x0001BF5C File Offset: 0x0001A15C
		private void SetToCustomIfContentTypeIsNot(params TMP_InputField.ContentType[] allowedContentTypes)
		{
			if (this.contentType == TMP_InputField.ContentType.Custom)
			{
				return;
			}
			for (int i = 0; i < allowedContentTypes.Length; i++)
			{
				if (this.contentType == allowedContentTypes[i])
				{
					return;
				}
			}
			this.contentType = TMP_InputField.ContentType.Custom;
		}

		// Token: 0x06000213 RID: 531 RVA: 0x0001BF96 File Offset: 0x0001A196
		private void SetToCustom()
		{
			if (this.contentType == TMP_InputField.ContentType.Custom)
			{
				return;
			}
			this.contentType = TMP_InputField.ContentType.Custom;
		}

		// Token: 0x06000214 RID: 532 RVA: 0x0001BFAB File Offset: 0x0001A1AB
		private void SetToCustom(TMP_InputField.CharacterValidation characterValidation)
		{
			if (this.contentType == TMP_InputField.ContentType.Custom)
			{
				return;
			}
			this.contentType = TMP_InputField.ContentType.Custom;
		}

		// Token: 0x06000215 RID: 533 RVA: 0x0001BFC6 File Offset: 0x0001A1C6
		protected override void DoStateTransition(Selectable.SelectionState state, bool instant)
		{
			if (this.m_HasDoneFocusTransition)
			{
				state = Selectable.SelectionState.Highlighted;
			}
			else if (state == Selectable.SelectionState.Pressed)
			{
				this.m_HasDoneFocusTransition = true;
			}
			base.DoStateTransition(state, instant);
		}

		// Token: 0x06000216 RID: 534 RVA: 0x0001BFE8 File Offset: 0x0001A1E8
		public void SetGlobalPointSize(float pointSize)
		{
			TMP_Text tmp_Text = this.m_Placeholder as TMP_Text;
			if (tmp_Text != null)
			{
				tmp_Text.fontSize = pointSize;
			}
			this.textComponent.fontSize = pointSize;
		}

		// Token: 0x06000217 RID: 535 RVA: 0x0001C020 File Offset: 0x0001A220
		public void SetGlobalFontAsset(TMP_FontAsset fontAsset)
		{
			TMP_Text tmp_Text = this.m_Placeholder as TMP_Text;
			if (tmp_Text != null)
			{
				tmp_Text.font = fontAsset;
			}
			this.textComponent.font = fontAsset;
		}

		// Token: 0x06000218 RID: 536 RVA: 0x0001C055 File Offset: 0x0001A255
		// Note: this type is marked as 'beforefieldinit'.
		static TMP_InputField()
		{
		}

		// Token: 0x06000219 RID: 537 RVA: 0x0001C06D File Offset: 0x0001A26D
		Transform ICanvasElement.get_transform()
		{
			return base.transform;
		}

		// Token: 0x04000099 RID: 153
		protected TouchScreenKeyboard m_Keyboard;

		// Token: 0x0400009A RID: 154
		private static readonly char[] kSeparators = new char[]
		{
			' ',
			'.',
			',',
			'\t',
			'\r',
			'\n'
		};

		// Token: 0x0400009B RID: 155
		[SerializeField]
		protected RectTransform m_TextViewport;

		// Token: 0x0400009C RID: 156
		[SerializeField]
		protected TMP_Text m_TextComponent;

		// Token: 0x0400009D RID: 157
		protected RectTransform m_TextComponentRectTransform;

		// Token: 0x0400009E RID: 158
		[SerializeField]
		protected Graphic m_Placeholder;

		// Token: 0x0400009F RID: 159
		[SerializeField]
		protected Scrollbar m_VerticalScrollbar;

		// Token: 0x040000A0 RID: 160
		[SerializeField]
		protected TMP_ScrollbarEventHandler m_VerticalScrollbarEventHandler;

		// Token: 0x040000A1 RID: 161
		private float m_ScrollPosition;

		// Token: 0x040000A2 RID: 162
		[SerializeField]
		protected float m_ScrollSensitivity = 1f;

		// Token: 0x040000A3 RID: 163
		[SerializeField]
		private TMP_InputField.ContentType m_ContentType;

		// Token: 0x040000A4 RID: 164
		[SerializeField]
		private TMP_InputField.InputType m_InputType;

		// Token: 0x040000A5 RID: 165
		[SerializeField]
		private char m_AsteriskChar = '*';

		// Token: 0x040000A6 RID: 166
		[SerializeField]
		private TouchScreenKeyboardType m_KeyboardType;

		// Token: 0x040000A7 RID: 167
		[SerializeField]
		private TMP_InputField.LineType m_LineType;

		// Token: 0x040000A8 RID: 168
		[SerializeField]
		private bool m_HideMobileInput;

		// Token: 0x040000A9 RID: 169
		[SerializeField]
		private TMP_InputField.CharacterValidation m_CharacterValidation;

		// Token: 0x040000AA RID: 170
		[SerializeField]
		private string m_RegexValue = string.Empty;

		// Token: 0x040000AB RID: 171
		[SerializeField]
		private float m_GlobalPointSize = 14f;

		// Token: 0x040000AC RID: 172
		[SerializeField]
		private int m_CharacterLimit;

		// Token: 0x040000AD RID: 173
		[SerializeField]
		private TMP_InputField.SubmitEvent m_OnEndEdit = new TMP_InputField.SubmitEvent();

		// Token: 0x040000AE RID: 174
		[SerializeField]
		private TMP_InputField.SubmitEvent m_OnSubmit = new TMP_InputField.SubmitEvent();

		// Token: 0x040000AF RID: 175
		[SerializeField]
		private TMP_InputField.SelectionEvent m_OnSelect = new TMP_InputField.SelectionEvent();

		// Token: 0x040000B0 RID: 176
		[SerializeField]
		private TMP_InputField.SelectionEvent m_OnDeselect = new TMP_InputField.SelectionEvent();

		// Token: 0x040000B1 RID: 177
		[SerializeField]
		private TMP_InputField.TextSelectionEvent m_OnTextSelection = new TMP_InputField.TextSelectionEvent();

		// Token: 0x040000B2 RID: 178
		[SerializeField]
		private TMP_InputField.TextSelectionEvent m_OnEndTextSelection = new TMP_InputField.TextSelectionEvent();

		// Token: 0x040000B3 RID: 179
		[SerializeField]
		private TMP_InputField.OnChangeEvent m_OnValueChanged = new TMP_InputField.OnChangeEvent();

		// Token: 0x040000B4 RID: 180
		[SerializeField]
		private TMP_InputField.OnValidateInput m_OnValidateInput;

		// Token: 0x040000B5 RID: 181
		[SerializeField]
		private Color m_CaretColor = new Color(0.19607843f, 0.19607843f, 0.19607843f, 1f);

		// Token: 0x040000B6 RID: 182
		[SerializeField]
		private bool m_CustomCaretColor;

		// Token: 0x040000B7 RID: 183
		[SerializeField]
		private Color m_SelectionColor = new Color(0.65882355f, 0.80784315f, 1f, 0.7529412f);

		// Token: 0x040000B8 RID: 184
		[SerializeField]
		[TextArea(3, 10)]
		protected string m_Text = string.Empty;

		// Token: 0x040000B9 RID: 185
		[SerializeField]
		[Range(0f, 4f)]
		private float m_CaretBlinkRate = 0.85f;

		// Token: 0x040000BA RID: 186
		[SerializeField]
		[Range(1f, 5f)]
		private int m_CaretWidth = 1;

		// Token: 0x040000BB RID: 187
		[SerializeField]
		private bool m_ReadOnly;

		// Token: 0x040000BC RID: 188
		[SerializeField]
		private bool m_RichText = true;

		// Token: 0x040000BD RID: 189
		protected int m_StringPosition;

		// Token: 0x040000BE RID: 190
		protected int m_StringSelectPosition;

		// Token: 0x040000BF RID: 191
		protected int m_CaretPosition;

		// Token: 0x040000C0 RID: 192
		protected int m_CaretSelectPosition;

		// Token: 0x040000C1 RID: 193
		private RectTransform caretRectTrans;

		// Token: 0x040000C2 RID: 194
		protected UIVertex[] m_CursorVerts;

		// Token: 0x040000C3 RID: 195
		private CanvasRenderer m_CachedInputRenderer;

		// Token: 0x040000C4 RID: 196
		private Vector2 m_DefaultTransformPosition;

		// Token: 0x040000C5 RID: 197
		private Vector2 m_LastPosition;

		// Token: 0x040000C6 RID: 198
		[NonSerialized]
		protected Mesh m_Mesh;

		// Token: 0x040000C7 RID: 199
		private bool m_AllowInput;

		// Token: 0x040000C8 RID: 200
		private bool m_ShouldActivateNextUpdate;

		// Token: 0x040000C9 RID: 201
		private bool m_UpdateDrag;

		// Token: 0x040000CA RID: 202
		private bool m_DragPositionOutOfBounds;

		// Token: 0x040000CB RID: 203
		private const float kHScrollSpeed = 0.05f;

		// Token: 0x040000CC RID: 204
		private const float kVScrollSpeed = 0.1f;

		// Token: 0x040000CD RID: 205
		protected bool m_CaretVisible;

		// Token: 0x040000CE RID: 206
		private Coroutine m_BlinkCoroutine;

		// Token: 0x040000CF RID: 207
		private float m_BlinkStartTime;

		// Token: 0x040000D0 RID: 208
		private Coroutine m_DragCoroutine;

		// Token: 0x040000D1 RID: 209
		private string m_OriginalText = "";

		// Token: 0x040000D2 RID: 210
		private bool m_WasCanceled;

		// Token: 0x040000D3 RID: 211
		private bool m_HasDoneFocusTransition;

		// Token: 0x040000D4 RID: 212
		private bool m_IsScrollbarUpdateRequired;

		// Token: 0x040000D5 RID: 213
		private bool m_IsUpdatingScrollbarValues;

		// Token: 0x040000D6 RID: 214
		private bool m_isLastKeyBackspace;

		// Token: 0x040000D7 RID: 215
		private float m_ClickStartTime;

		// Token: 0x040000D8 RID: 216
		private float m_DoubleClickDelay = 0.5f;

		// Token: 0x040000D9 RID: 217
		private const string kEmailSpecialCharacters = "!#$%&'*+-/=?^_`{|}~";

		// Token: 0x040000DA RID: 218
		[SerializeField]
		protected TMP_FontAsset m_GlobalFontAsset;

		// Token: 0x040000DB RID: 219
		[SerializeField]
		protected bool m_OnFocusSelectAll = true;

		// Token: 0x040000DC RID: 220
		protected bool m_isSelectAll;

		// Token: 0x040000DD RID: 221
		[SerializeField]
		protected bool m_ResetOnDeActivation = true;

		// Token: 0x040000DE RID: 222
		[SerializeField]
		private bool m_RestoreOriginalTextOnEscape = true;

		// Token: 0x040000DF RID: 223
		[SerializeField]
		protected bool m_isRichTextEditingAllowed = true;

		// Token: 0x040000E0 RID: 224
		[SerializeField]
		protected TMP_InputValidator m_InputValidator;

		// Token: 0x040000E1 RID: 225
		private bool m_isSelected;

		// Token: 0x040000E2 RID: 226
		private bool isStringPositionDirty;

		// Token: 0x040000E3 RID: 227
		private bool m_forceRectTransformAdjustment;

		// Token: 0x040000E4 RID: 228
		private Event m_ProcessingEvent = new Event();

		// Token: 0x0200006F RID: 111
		public enum ContentType
		{
			// Token: 0x0400041E RID: 1054
			Standard,
			// Token: 0x0400041F RID: 1055
			Autocorrected,
			// Token: 0x04000420 RID: 1056
			IntegerNumber,
			// Token: 0x04000421 RID: 1057
			DecimalNumber,
			// Token: 0x04000422 RID: 1058
			Alphanumeric,
			// Token: 0x04000423 RID: 1059
			Name,
			// Token: 0x04000424 RID: 1060
			EmailAddress,
			// Token: 0x04000425 RID: 1061
			Password,
			// Token: 0x04000426 RID: 1062
			Pin,
			// Token: 0x04000427 RID: 1063
			Custom
		}

		// Token: 0x02000070 RID: 112
		public enum InputType
		{
			// Token: 0x04000429 RID: 1065
			Standard,
			// Token: 0x0400042A RID: 1066
			AutoCorrect,
			// Token: 0x0400042B RID: 1067
			Password
		}

		// Token: 0x02000071 RID: 113
		public enum CharacterValidation
		{
			// Token: 0x0400042D RID: 1069
			None,
			// Token: 0x0400042E RID: 1070
			Digit,
			// Token: 0x0400042F RID: 1071
			Integer,
			// Token: 0x04000430 RID: 1072
			Decimal,
			// Token: 0x04000431 RID: 1073
			Alphanumeric,
			// Token: 0x04000432 RID: 1074
			Name,
			// Token: 0x04000433 RID: 1075
			Regex,
			// Token: 0x04000434 RID: 1076
			EmailAddress,
			// Token: 0x04000435 RID: 1077
			CustomValidator
		}

		// Token: 0x02000072 RID: 114
		public enum LineType
		{
			// Token: 0x04000437 RID: 1079
			SingleLine,
			// Token: 0x04000438 RID: 1080
			MultiLineSubmit,
			// Token: 0x04000439 RID: 1081
			MultiLineNewline
		}

		// Token: 0x02000073 RID: 115
		// (Invoke) Token: 0x060004BE RID: 1214
		public delegate char OnValidateInput(string text, int charIndex, char addedChar);

		// Token: 0x02000074 RID: 116
		[Serializable]
		public class SubmitEvent : UnityEvent<string>
		{
			// Token: 0x060004C1 RID: 1217 RVA: 0x0002F5C1 File Offset: 0x0002D7C1
			public SubmitEvent()
			{
			}
		}

		// Token: 0x02000075 RID: 117
		[Serializable]
		public class OnChangeEvent : UnityEvent<string>
		{
			// Token: 0x060004C2 RID: 1218 RVA: 0x0002F5C1 File Offset: 0x0002D7C1
			public OnChangeEvent()
			{
			}
		}

		// Token: 0x02000076 RID: 118
		[Serializable]
		public class SelectionEvent : UnityEvent<string>
		{
			// Token: 0x060004C3 RID: 1219 RVA: 0x0002F5C1 File Offset: 0x0002D7C1
			public SelectionEvent()
			{
			}
		}

		// Token: 0x02000077 RID: 119
		[Serializable]
		public class TextSelectionEvent : UnityEvent<string, int, int>
		{
			// Token: 0x060004C4 RID: 1220 RVA: 0x0002F5C9 File Offset: 0x0002D7C9
			public TextSelectionEvent()
			{
			}
		}

		// Token: 0x02000078 RID: 120
		protected enum EditState
		{
			// Token: 0x0400043B RID: 1083
			Continue,
			// Token: 0x0400043C RID: 1084
			Finish
		}

		// Token: 0x02000079 RID: 121
		[CompilerGenerated]
		private sealed class <CaretBlink>d__238 : IEnumerator<object>, IEnumerator, IDisposable
		{
			// Token: 0x060004C5 RID: 1221 RVA: 0x0002F5D1 File Offset: 0x0002D7D1
			[DebuggerHidden]
			public <CaretBlink>d__238(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x060004C6 RID: 1222 RVA: 0x0000297D File Offset: 0x00000B7D
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x060004C7 RID: 1223 RVA: 0x0002F5E0 File Offset: 0x0002D7E0
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				TMP_InputField tmp_InputField = this;
				switch (num)
				{
				case 0:
					this.<>1__state = -1;
					tmp_InputField.m_CaretVisible = true;
					this.<>2__current = null;
					this.<>1__state = 1;
					return true;
				case 1:
					this.<>1__state = -1;
					break;
				case 2:
					this.<>1__state = -1;
					break;
				default:
					return false;
				}
				if (tmp_InputField.m_CaretBlinkRate <= 0f)
				{
					tmp_InputField.m_BlinkCoroutine = null;
					return false;
				}
				float num2 = 1f / tmp_InputField.m_CaretBlinkRate;
				bool flag = (Time.unscaledTime - tmp_InputField.m_BlinkStartTime) % num2 < num2 / 2f;
				if (tmp_InputField.m_CaretVisible != flag)
				{
					tmp_InputField.m_CaretVisible = flag;
					if (!tmp_InputField.hasSelection)
					{
						tmp_InputField.MarkGeometryAsDirty();
					}
				}
				this.<>2__current = null;
				this.<>1__state = 2;
				return true;
			}

			// Token: 0x17000112 RID: 274
			// (get) Token: 0x060004C8 RID: 1224 RVA: 0x0002F6A5 File Offset: 0x0002D8A5
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060004C9 RID: 1225 RVA: 0x0002F3C7 File Offset: 0x0002D5C7
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000113 RID: 275
			// (get) Token: 0x060004CA RID: 1226 RVA: 0x0002F6A5 File Offset: 0x0002D8A5
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0400043D RID: 1085
			private int <>1__state;

			// Token: 0x0400043E RID: 1086
			private object <>2__current;

			// Token: 0x0400043F RID: 1087
			public TMP_InputField <>4__this;
		}

		// Token: 0x0200007A RID: 122
		[CompilerGenerated]
		private sealed class <MouseDragOutsideRect>d__255 : IEnumerator<object>, IEnumerator, IDisposable
		{
			// Token: 0x060004CB RID: 1227 RVA: 0x0002F6AD File Offset: 0x0002D8AD
			[DebuggerHidden]
			public <MouseDragOutsideRect>d__255(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x060004CC RID: 1228 RVA: 0x0000297D File Offset: 0x00000B7D
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x060004CD RID: 1229 RVA: 0x0002F6BC File Offset: 0x0002D8BC
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				TMP_InputField tmp_InputField = this;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
				}
				else
				{
					this.<>1__state = -1;
				}
				if (!tmp_InputField.m_UpdateDrag || !tmp_InputField.m_DragPositionOutOfBounds)
				{
					tmp_InputField.m_DragCoroutine = null;
					return false;
				}
				Vector2 vector;
				RectTransformUtility.ScreenPointToLocalPointInRectangle(tmp_InputField.textViewport, eventData.position, eventData.pressEventCamera, out vector);
				Rect rect = tmp_InputField.textViewport.rect;
				if (tmp_InputField.multiLine)
				{
					if (vector.y > rect.yMax)
					{
						tmp_InputField.MoveUp(true, true);
					}
					else if (vector.y < rect.yMin)
					{
						tmp_InputField.MoveDown(true, true);
					}
				}
				else if (vector.x < rect.xMin)
				{
					tmp_InputField.MoveLeft(true, false);
				}
				else if (vector.x > rect.xMax)
				{
					tmp_InputField.MoveRight(true, false);
				}
				tmp_InputField.UpdateLabel();
				float seconds = tmp_InputField.multiLine ? 0.1f : 0.05f;
				this.<>2__current = new WaitForSeconds(seconds);
				this.<>1__state = 1;
				return true;
			}

			// Token: 0x17000114 RID: 276
			// (get) Token: 0x060004CE RID: 1230 RVA: 0x0002F7DD File Offset: 0x0002D9DD
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060004CF RID: 1231 RVA: 0x0002F3C7 File Offset: 0x0002D5C7
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000115 RID: 277
			// (get) Token: 0x060004D0 RID: 1232 RVA: 0x0002F7DD File Offset: 0x0002D9DD
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x04000440 RID: 1088
			private int <>1__state;

			// Token: 0x04000441 RID: 1089
			private object <>2__current;

			// Token: 0x04000442 RID: 1090
			public TMP_InputField <>4__this;

			// Token: 0x04000443 RID: 1091
			public PointerEventData eventData;
		}
	}
}
