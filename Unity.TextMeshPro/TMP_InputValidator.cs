﻿using System;
using UnityEngine;

namespace TMPro
{
	// Token: 0x0200001A RID: 26
	[Serializable]
	public abstract class TMP_InputValidator : ScriptableObject
	{
		// Token: 0x0600021E RID: 542
		public abstract char Validate(ref string text, ref int pos, char ch);

		// Token: 0x0600021F RID: 543 RVA: 0x000155AE File Offset: 0x000137AE
		protected TMP_InputValidator()
		{
		}
	}
}
