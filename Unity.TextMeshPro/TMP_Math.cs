﻿using System;
using UnityEngine;

namespace TMPro
{
	// Token: 0x02000047 RID: 71
	public static class TMP_Math
	{
		// Token: 0x06000464 RID: 1124 RVA: 0x0002DC36 File Offset: 0x0002BE36
		public static bool Approximately(float a, float b)
		{
			return b - 0.0001f < a && a < b + 0.0001f;
		}

		// Token: 0x06000465 RID: 1125 RVA: 0x0002DC4E File Offset: 0x0002BE4E
		// Note: this type is marked as 'beforefieldinit'.
		static TMP_Math()
		{
		}

		// Token: 0x040002E8 RID: 744
		public const float FLOAT_MAX = 32767f;

		// Token: 0x040002E9 RID: 745
		public const float FLOAT_MIN = -32767f;

		// Token: 0x040002EA RID: 746
		public const int INT_MAX = 2147483647;

		// Token: 0x040002EB RID: 747
		public const int INT_MIN = -2147483647;

		// Token: 0x040002EC RID: 748
		public const float FLOAT_UNSET = -32767f;

		// Token: 0x040002ED RID: 749
		public const int INT_UNSET = -32767;

		// Token: 0x040002EE RID: 750
		public static Vector2 MAX_16BIT = new Vector2(32767f, 32767f);

		// Token: 0x040002EF RID: 751
		public static Vector2 MIN_16BIT = new Vector2(-32767f, -32767f);
	}
}
