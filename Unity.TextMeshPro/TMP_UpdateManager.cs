﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.UI;

namespace TMPro
{
	// Token: 0x0200003F RID: 63
	public class TMP_UpdateManager
	{
		// Token: 0x17000101 RID: 257
		// (get) Token: 0x06000424 RID: 1060 RVA: 0x0002CD24 File Offset: 0x0002AF24
		public static TMP_UpdateManager instance
		{
			get
			{
				if (TMP_UpdateManager.s_Instance == null)
				{
					TMP_UpdateManager.s_Instance = new TMP_UpdateManager();
				}
				return TMP_UpdateManager.s_Instance;
			}
		}

		// Token: 0x06000425 RID: 1061 RVA: 0x0002CD3C File Offset: 0x0002AF3C
		protected TMP_UpdateManager()
		{
			Camera.onPreCull = (Camera.CameraCallback)Delegate.Combine(Camera.onPreCull, new Camera.CameraCallback(this.OnCameraPreCull));
			RenderPipeline.beginFrameRendering += this.OnBeginFrameRendering;
		}

		// Token: 0x06000426 RID: 1062 RVA: 0x0002CDAC File Offset: 0x0002AFAC
		public static void RegisterTextElementForLayoutRebuild(TMP_Text element)
		{
			TMP_UpdateManager.instance.InternalRegisterTextElementForLayoutRebuild(element);
		}

		// Token: 0x06000427 RID: 1063 RVA: 0x0002CDBC File Offset: 0x0002AFBC
		private bool InternalRegisterTextElementForLayoutRebuild(TMP_Text element)
		{
			int instanceID = element.GetInstanceID();
			if (this.m_LayoutQueueLookup.ContainsKey(instanceID))
			{
				return false;
			}
			this.m_LayoutQueueLookup[instanceID] = instanceID;
			this.m_LayoutRebuildQueue.Add(element);
			return true;
		}

		// Token: 0x06000428 RID: 1064 RVA: 0x0002CDFA File Offset: 0x0002AFFA
		public static void RegisterTextElementForGraphicRebuild(TMP_Text element)
		{
			TMP_UpdateManager.instance.InternalRegisterTextElementForGraphicRebuild(element);
		}

		// Token: 0x06000429 RID: 1065 RVA: 0x0002CE08 File Offset: 0x0002B008
		private bool InternalRegisterTextElementForGraphicRebuild(TMP_Text element)
		{
			int instanceID = element.GetInstanceID();
			if (this.m_GraphicQueueLookup.ContainsKey(instanceID))
			{
				return false;
			}
			this.m_GraphicQueueLookup[instanceID] = instanceID;
			this.m_GraphicRebuildQueue.Add(element);
			return true;
		}

		// Token: 0x0600042A RID: 1066 RVA: 0x0002CE46 File Offset: 0x0002B046
		private void OnBeginFrameRendering(Camera[] cameras)
		{
			this.DoRebuilds();
		}

		// Token: 0x0600042B RID: 1067 RVA: 0x0002CE46 File Offset: 0x0002B046
		private void OnCameraPreCull(Camera cam)
		{
			this.DoRebuilds();
		}

		// Token: 0x0600042C RID: 1068 RVA: 0x0002CE50 File Offset: 0x0002B050
		private void DoRebuilds()
		{
			for (int i = 0; i < this.m_LayoutRebuildQueue.Count; i++)
			{
				this.m_LayoutRebuildQueue[i].Rebuild(CanvasUpdate.Prelayout);
			}
			if (this.m_LayoutRebuildQueue.Count > 0)
			{
				this.m_LayoutRebuildQueue.Clear();
				this.m_LayoutQueueLookup.Clear();
			}
			for (int j = 0; j < this.m_GraphicRebuildQueue.Count; j++)
			{
				this.m_GraphicRebuildQueue[j].Rebuild(CanvasUpdate.PreRender);
			}
			if (this.m_GraphicRebuildQueue.Count > 0)
			{
				this.m_GraphicRebuildQueue.Clear();
				this.m_GraphicQueueLookup.Clear();
			}
		}

		// Token: 0x0600042D RID: 1069 RVA: 0x0002CEF5 File Offset: 0x0002B0F5
		public static void UnRegisterTextElementForRebuild(TMP_Text element)
		{
			TMP_UpdateManager.instance.InternalUnRegisterTextElementForGraphicRebuild(element);
			TMP_UpdateManager.instance.InternalUnRegisterTextElementForLayoutRebuild(element);
		}

		// Token: 0x0600042E RID: 1070 RVA: 0x0002CF10 File Offset: 0x0002B110
		private void InternalUnRegisterTextElementForGraphicRebuild(TMP_Text element)
		{
			int instanceID = element.GetInstanceID();
			TMP_UpdateManager.instance.m_GraphicRebuildQueue.Remove(element);
			this.m_GraphicQueueLookup.Remove(instanceID);
		}

		// Token: 0x0600042F RID: 1071 RVA: 0x0002CF44 File Offset: 0x0002B144
		private void InternalUnRegisterTextElementForLayoutRebuild(TMP_Text element)
		{
			int instanceID = element.GetInstanceID();
			TMP_UpdateManager.instance.m_LayoutRebuildQueue.Remove(element);
			this.m_LayoutQueueLookup.Remove(instanceID);
		}

		// Token: 0x040002BC RID: 700
		private static TMP_UpdateManager s_Instance;

		// Token: 0x040002BD RID: 701
		private readonly List<TMP_Text> m_LayoutRebuildQueue = new List<TMP_Text>();

		// Token: 0x040002BE RID: 702
		private Dictionary<int, int> m_LayoutQueueLookup = new Dictionary<int, int>();

		// Token: 0x040002BF RID: 703
		private readonly List<TMP_Text> m_GraphicRebuildQueue = new List<TMP_Text>();

		// Token: 0x040002C0 RID: 704
		private Dictionary<int, int> m_GraphicQueueLookup = new Dictionary<int, int>();
	}
}
