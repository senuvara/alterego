﻿using System;
using UnityEngine;

namespace TMPro
{
	// Token: 0x0200003B RID: 59
	[Serializable]
	public class TMP_TextInfo
	{
		// Token: 0x06000400 RID: 1024 RVA: 0x0002AF18 File Offset: 0x00029118
		public TMP_TextInfo()
		{
			this.characterInfo = new TMP_CharacterInfo[8];
			this.wordInfo = new TMP_WordInfo[16];
			this.linkInfo = new TMP_LinkInfo[0];
			this.lineInfo = new TMP_LineInfo[2];
			this.pageInfo = new TMP_PageInfo[4];
			this.meshInfo = new TMP_MeshInfo[1];
		}

		// Token: 0x06000401 RID: 1025 RVA: 0x0002AF74 File Offset: 0x00029174
		public TMP_TextInfo(TMP_Text textComponent)
		{
			this.textComponent = textComponent;
			this.characterInfo = new TMP_CharacterInfo[8];
			this.wordInfo = new TMP_WordInfo[4];
			this.linkInfo = new TMP_LinkInfo[0];
			this.lineInfo = new TMP_LineInfo[2];
			this.pageInfo = new TMP_PageInfo[4];
			this.meshInfo = new TMP_MeshInfo[1];
			this.meshInfo[0].mesh = textComponent.mesh;
			this.materialCount = 1;
		}

		// Token: 0x06000402 RID: 1026 RVA: 0x0002AFF4 File Offset: 0x000291F4
		public void Clear()
		{
			this.characterCount = 0;
			this.spaceCount = 0;
			this.wordCount = 0;
			this.linkCount = 0;
			this.lineCount = 0;
			this.pageCount = 0;
			this.spriteCount = 0;
			for (int i = 0; i < this.meshInfo.Length; i++)
			{
				this.meshInfo[i].vertexCount = 0;
			}
		}

		// Token: 0x06000403 RID: 1027 RVA: 0x0002B058 File Offset: 0x00029258
		public void ClearMeshInfo(bool updateMesh)
		{
			for (int i = 0; i < this.meshInfo.Length; i++)
			{
				this.meshInfo[i].Clear(updateMesh);
			}
		}

		// Token: 0x06000404 RID: 1028 RVA: 0x0002B08C File Offset: 0x0002928C
		public void ClearAllMeshInfo()
		{
			for (int i = 0; i < this.meshInfo.Length; i++)
			{
				this.meshInfo[i].Clear(true);
			}
		}

		// Token: 0x06000405 RID: 1029 RVA: 0x0002B0C0 File Offset: 0x000292C0
		public void ResetVertexLayout(bool isVolumetric)
		{
			for (int i = 0; i < this.meshInfo.Length; i++)
			{
				this.meshInfo[i].ResizeMeshInfo(0, isVolumetric);
			}
		}

		// Token: 0x06000406 RID: 1030 RVA: 0x0002B0F4 File Offset: 0x000292F4
		public void ClearUnusedVertices(MaterialReference[] materials)
		{
			for (int i = 0; i < this.meshInfo.Length; i++)
			{
				int startIndex = 0;
				this.meshInfo[i].ClearUnusedVertices(startIndex);
			}
		}

		// Token: 0x06000407 RID: 1031 RVA: 0x0002B128 File Offset: 0x00029328
		public void ClearLineInfo()
		{
			if (this.lineInfo == null)
			{
				this.lineInfo = new TMP_LineInfo[2];
			}
			for (int i = 0; i < this.lineInfo.Length; i++)
			{
				this.lineInfo[i].characterCount = 0;
				this.lineInfo[i].spaceCount = 0;
				this.lineInfo[i].wordCount = 0;
				this.lineInfo[i].controlCharacterCount = 0;
				this.lineInfo[i].width = 0f;
				this.lineInfo[i].ascender = TMP_TextInfo.k_InfinityVectorNegative.x;
				this.lineInfo[i].descender = TMP_TextInfo.k_InfinityVectorPositive.x;
				this.lineInfo[i].lineExtents.min = TMP_TextInfo.k_InfinityVectorPositive;
				this.lineInfo[i].lineExtents.max = TMP_TextInfo.k_InfinityVectorNegative;
				this.lineInfo[i].maxAdvance = 0f;
			}
		}

		// Token: 0x06000408 RID: 1032 RVA: 0x0002B244 File Offset: 0x00029444
		public TMP_MeshInfo[] CopyMeshInfoVertexData()
		{
			if (this.m_CachedMeshInfo == null || this.m_CachedMeshInfo.Length != this.meshInfo.Length)
			{
				this.m_CachedMeshInfo = new TMP_MeshInfo[this.meshInfo.Length];
				for (int i = 0; i < this.m_CachedMeshInfo.Length; i++)
				{
					int num = this.meshInfo[i].vertices.Length;
					this.m_CachedMeshInfo[i].vertices = new Vector3[num];
					this.m_CachedMeshInfo[i].uvs0 = new Vector2[num];
					this.m_CachedMeshInfo[i].uvs2 = new Vector2[num];
					this.m_CachedMeshInfo[i].colors32 = new Color32[num];
				}
			}
			for (int j = 0; j < this.m_CachedMeshInfo.Length; j++)
			{
				int num2 = this.meshInfo[j].vertices.Length;
				if (this.m_CachedMeshInfo[j].vertices.Length != num2)
				{
					this.m_CachedMeshInfo[j].vertices = new Vector3[num2];
					this.m_CachedMeshInfo[j].uvs0 = new Vector2[num2];
					this.m_CachedMeshInfo[j].uvs2 = new Vector2[num2];
					this.m_CachedMeshInfo[j].colors32 = new Color32[num2];
				}
				Array.Copy(this.meshInfo[j].vertices, this.m_CachedMeshInfo[j].vertices, num2);
				Array.Copy(this.meshInfo[j].uvs0, this.m_CachedMeshInfo[j].uvs0, num2);
				Array.Copy(this.meshInfo[j].uvs2, this.m_CachedMeshInfo[j].uvs2, num2);
				Array.Copy(this.meshInfo[j].colors32, this.m_CachedMeshInfo[j].colors32, num2);
			}
			return this.m_CachedMeshInfo;
		}

		// Token: 0x06000409 RID: 1033 RVA: 0x0002B44C File Offset: 0x0002964C
		public static void Resize<T>(ref T[] array, int size)
		{
			int newSize = (size > 1024) ? (size + 256) : Mathf.NextPowerOfTwo(size);
			Array.Resize<T>(ref array, newSize);
		}

		// Token: 0x0600040A RID: 1034 RVA: 0x0002B478 File Offset: 0x00029678
		public static void Resize<T>(ref T[] array, int size, bool isBlockAllocated)
		{
			if (isBlockAllocated)
			{
				size = ((size > 1024) ? (size + 256) : Mathf.NextPowerOfTwo(size));
			}
			if (size == array.Length)
			{
				return;
			}
			Array.Resize<T>(ref array, size);
		}

		// Token: 0x0600040B RID: 1035 RVA: 0x0002B4A5 File Offset: 0x000296A5
		// Note: this type is marked as 'beforefieldinit'.
		static TMP_TextInfo()
		{
		}

		// Token: 0x040002A1 RID: 673
		private static Vector2 k_InfinityVectorPositive = new Vector2(32767f, 32767f);

		// Token: 0x040002A2 RID: 674
		private static Vector2 k_InfinityVectorNegative = new Vector2(-32767f, -32767f);

		// Token: 0x040002A3 RID: 675
		public TMP_Text textComponent;

		// Token: 0x040002A4 RID: 676
		public int characterCount;

		// Token: 0x040002A5 RID: 677
		public int spriteCount;

		// Token: 0x040002A6 RID: 678
		public int spaceCount;

		// Token: 0x040002A7 RID: 679
		public int wordCount;

		// Token: 0x040002A8 RID: 680
		public int linkCount;

		// Token: 0x040002A9 RID: 681
		public int lineCount;

		// Token: 0x040002AA RID: 682
		public int pageCount;

		// Token: 0x040002AB RID: 683
		public int materialCount;

		// Token: 0x040002AC RID: 684
		public TMP_CharacterInfo[] characterInfo;

		// Token: 0x040002AD RID: 685
		public TMP_WordInfo[] wordInfo;

		// Token: 0x040002AE RID: 686
		public TMP_LinkInfo[] linkInfo;

		// Token: 0x040002AF RID: 687
		public TMP_LineInfo[] lineInfo;

		// Token: 0x040002B0 RID: 688
		public TMP_PageInfo[] pageInfo;

		// Token: 0x040002B1 RID: 689
		public TMP_MeshInfo[] meshInfo;

		// Token: 0x040002B2 RID: 690
		private TMP_MeshInfo[] m_CachedMeshInfo;
	}
}
