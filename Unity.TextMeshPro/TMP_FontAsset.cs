﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace TMPro
{
	// Token: 0x02000017 RID: 23
	[Serializable]
	public class TMP_FontAsset : TMP_Asset
	{
		// Token: 0x17000033 RID: 51
		// (get) Token: 0x0600013F RID: 319 RVA: 0x00017214 File Offset: 0x00015414
		public static TMP_FontAsset defaultFontAsset
		{
			get
			{
				if (TMP_FontAsset.s_defaultFontAsset == null)
				{
					TMP_FontAsset.s_defaultFontAsset = Resources.Load<TMP_FontAsset>("Fonts & Materials/LiberationSans SDF");
				}
				return TMP_FontAsset.s_defaultFontAsset;
			}
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x06000140 RID: 320 RVA: 0x00017237 File Offset: 0x00015437
		public FaceInfo fontInfo
		{
			get
			{
				return this.m_fontInfo;
			}
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x06000141 RID: 321 RVA: 0x0001723F File Offset: 0x0001543F
		public Dictionary<int, TMP_Glyph> characterDictionary
		{
			get
			{
				if (this.m_characterDictionary == null)
				{
					this.ReadFontDefinition();
				}
				return this.m_characterDictionary;
			}
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x06000142 RID: 322 RVA: 0x00017255 File Offset: 0x00015455
		public Dictionary<int, KerningPair> kerningDictionary
		{
			get
			{
				return this.m_kerningDictionary;
			}
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x06000143 RID: 323 RVA: 0x0001725D File Offset: 0x0001545D
		public KerningTable kerningInfo
		{
			get
			{
				return this.m_kerningInfo;
			}
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x06000144 RID: 324 RVA: 0x00017265 File Offset: 0x00015465
		// (set) Token: 0x06000145 RID: 325 RVA: 0x0001726D File Offset: 0x0001546D
		public FontAssetCreationSettings creationSettings
		{
			get
			{
				return this.m_CreationSettings;
			}
			set
			{
				this.m_CreationSettings = value;
			}
		}

		// Token: 0x06000146 RID: 326 RVA: 0x0000297D File Offset: 0x00000B7D
		private void OnEnable()
		{
		}

		// Token: 0x06000147 RID: 327 RVA: 0x0000297D File Offset: 0x00000B7D
		private void OnDisable()
		{
		}

		// Token: 0x06000148 RID: 328 RVA: 0x00017276 File Offset: 0x00015476
		public void AddFaceInfo(FaceInfo faceInfo)
		{
			this.m_fontInfo = faceInfo;
		}

		// Token: 0x06000149 RID: 329 RVA: 0x00017280 File Offset: 0x00015480
		public void AddGlyphInfo(TMP_Glyph[] glyphInfo)
		{
			this.m_glyphInfoList = new List<TMP_Glyph>();
			int num = glyphInfo.Length;
			this.m_fontInfo.CharacterCount = num;
			this.m_characterSet = new int[num];
			for (int i = 0; i < num; i++)
			{
				TMP_Glyph tmp_Glyph = new TMP_Glyph();
				tmp_Glyph.id = glyphInfo[i].id;
				tmp_Glyph.x = glyphInfo[i].x;
				tmp_Glyph.y = glyphInfo[i].y;
				tmp_Glyph.width = glyphInfo[i].width;
				tmp_Glyph.height = glyphInfo[i].height;
				tmp_Glyph.xOffset = glyphInfo[i].xOffset;
				tmp_Glyph.yOffset = glyphInfo[i].yOffset;
				tmp_Glyph.xAdvance = glyphInfo[i].xAdvance;
				tmp_Glyph.scale = 1f;
				this.m_glyphInfoList.Add(tmp_Glyph);
				this.m_characterSet[i] = tmp_Glyph.id;
			}
			this.m_glyphInfoList = (from s in this.m_glyphInfoList
			orderby s.id
			select s).ToList<TMP_Glyph>();
		}

		// Token: 0x0600014A RID: 330 RVA: 0x00017396 File Offset: 0x00015596
		public void AddKerningInfo(KerningTable kerningTable)
		{
			this.m_kerningInfo = kerningTable;
		}

		// Token: 0x0600014B RID: 331 RVA: 0x000173A0 File Offset: 0x000155A0
		public void ReadFontDefinition()
		{
			if (this.m_fontInfo == null)
			{
				return;
			}
			this.m_characterDictionary = new Dictionary<int, TMP_Glyph>();
			for (int i = 0; i < this.m_glyphInfoList.Count; i++)
			{
				TMP_Glyph tmp_Glyph = this.m_glyphInfoList[i];
				if (!this.m_characterDictionary.ContainsKey(tmp_Glyph.id))
				{
					this.m_characterDictionary.Add(tmp_Glyph.id, tmp_Glyph);
				}
				if (tmp_Glyph.scale == 0f)
				{
					tmp_Glyph.scale = 1f;
				}
			}
			TMP_Glyph tmp_Glyph2 = new TMP_Glyph();
			if (this.m_characterDictionary.ContainsKey(32))
			{
				this.m_characterDictionary[32].width = this.m_characterDictionary[32].xAdvance;
				this.m_characterDictionary[32].height = this.m_fontInfo.Ascender - this.m_fontInfo.Descender;
				this.m_characterDictionary[32].yOffset = this.m_fontInfo.Ascender;
				this.m_characterDictionary[32].scale = 1f;
			}
			else
			{
				tmp_Glyph2 = new TMP_Glyph();
				tmp_Glyph2.id = 32;
				tmp_Glyph2.x = 0f;
				tmp_Glyph2.y = 0f;
				tmp_Glyph2.width = this.m_fontInfo.Ascender / 5f;
				tmp_Glyph2.height = this.m_fontInfo.Ascender - this.m_fontInfo.Descender;
				tmp_Glyph2.xOffset = 0f;
				tmp_Glyph2.yOffset = this.m_fontInfo.Ascender;
				tmp_Glyph2.xAdvance = this.m_fontInfo.PointSize / 4f;
				tmp_Glyph2.scale = 1f;
				this.m_characterDictionary.Add(32, tmp_Glyph2);
			}
			if (!this.m_characterDictionary.ContainsKey(160))
			{
				tmp_Glyph2 = TMP_Glyph.Clone(this.m_characterDictionary[32]);
				this.m_characterDictionary.Add(160, tmp_Glyph2);
			}
			if (!this.m_characterDictionary.ContainsKey(8203))
			{
				tmp_Glyph2 = TMP_Glyph.Clone(this.m_characterDictionary[32]);
				tmp_Glyph2.width = 0f;
				tmp_Glyph2.xAdvance = 0f;
				this.m_characterDictionary.Add(8203, tmp_Glyph2);
			}
			if (!this.m_characterDictionary.ContainsKey(8288))
			{
				tmp_Glyph2 = TMP_Glyph.Clone(this.m_characterDictionary[32]);
				tmp_Glyph2.width = 0f;
				tmp_Glyph2.xAdvance = 0f;
				this.m_characterDictionary.Add(8288, tmp_Glyph2);
			}
			if (!this.m_characterDictionary.ContainsKey(10))
			{
				tmp_Glyph2 = new TMP_Glyph();
				tmp_Glyph2.id = 10;
				tmp_Glyph2.x = 0f;
				tmp_Glyph2.y = 0f;
				tmp_Glyph2.width = 10f;
				tmp_Glyph2.height = this.m_characterDictionary[32].height;
				tmp_Glyph2.xOffset = 0f;
				tmp_Glyph2.yOffset = this.m_characterDictionary[32].yOffset;
				tmp_Glyph2.xAdvance = 0f;
				tmp_Glyph2.scale = 1f;
				this.m_characterDictionary.Add(10, tmp_Glyph2);
				if (!this.m_characterDictionary.ContainsKey(13))
				{
					this.m_characterDictionary.Add(13, tmp_Glyph2);
				}
			}
			if (!this.m_characterDictionary.ContainsKey(9))
			{
				tmp_Glyph2 = new TMP_Glyph();
				tmp_Glyph2.id = 9;
				tmp_Glyph2.x = this.m_characterDictionary[32].x;
				tmp_Glyph2.y = this.m_characterDictionary[32].y;
				tmp_Glyph2.width = this.m_characterDictionary[32].width * (float)this.tabSize + (this.m_characterDictionary[32].xAdvance - this.m_characterDictionary[32].width) * (float)(this.tabSize - 1);
				tmp_Glyph2.height = this.m_characterDictionary[32].height;
				tmp_Glyph2.xOffset = this.m_characterDictionary[32].xOffset;
				tmp_Glyph2.yOffset = this.m_characterDictionary[32].yOffset;
				tmp_Glyph2.xAdvance = this.m_characterDictionary[32].xAdvance * (float)this.tabSize;
				tmp_Glyph2.scale = 1f;
				this.m_characterDictionary.Add(9, tmp_Glyph2);
			}
			this.m_fontInfo.TabWidth = this.m_characterDictionary[9].xAdvance;
			if (this.m_fontInfo.CapHeight == 0f && this.m_characterDictionary.ContainsKey(72))
			{
				this.m_fontInfo.CapHeight = this.m_characterDictionary[72].yOffset;
			}
			if (this.m_fontInfo.Scale == 0f)
			{
				this.m_fontInfo.Scale = 1f;
			}
			if (this.m_fontInfo.strikethrough == 0f)
			{
				this.m_fontInfo.strikethrough = this.m_fontInfo.CapHeight / 2.5f;
			}
			if (this.m_fontInfo.Padding == 0f && this.material.HasProperty(ShaderUtilities.ID_GradientScale))
			{
				this.m_fontInfo.Padding = this.material.GetFloat(ShaderUtilities.ID_GradientScale) - 1f;
			}
			this.m_kerningDictionary = new Dictionary<int, KerningPair>();
			List<KerningPair> kerningPairs = this.m_kerningInfo.kerningPairs;
			for (int j = 0; j < kerningPairs.Count; j++)
			{
				KerningPair kerningPair = kerningPairs[j];
				if (kerningPair.xOffset != 0f)
				{
					kerningPairs[j].ConvertLegacyKerningData();
				}
				KerningPairKey kerningPairKey = new KerningPairKey(kerningPair.firstGlyph, kerningPair.secondGlyph);
				if (!this.m_kerningDictionary.ContainsKey((int)kerningPairKey.key))
				{
					this.m_kerningDictionary.Add((int)kerningPairKey.key, kerningPair);
				}
				else if (!TMP_Settings.warningsDisabled)
				{
					Debug.LogWarning(string.Concat(new object[]
					{
						"Kerning Key for [",
						kerningPairKey.ascii_Left,
						"] and [",
						kerningPairKey.ascii_Right,
						"] already exists."
					}));
				}
			}
			this.hashCode = TMP_TextUtilities.GetSimpleHashCode(base.name);
			this.materialHashCode = TMP_TextUtilities.GetSimpleHashCode(this.material.name);
		}

		// Token: 0x0600014C RID: 332 RVA: 0x00017A0C File Offset: 0x00015C0C
		public void SortGlyphs()
		{
			if (this.m_glyphInfoList == null || this.m_glyphInfoList.Count == 0)
			{
				return;
			}
			this.m_glyphInfoList = (from item in this.m_glyphInfoList
			orderby item.id
			select item).ToList<TMP_Glyph>();
		}

		// Token: 0x0600014D RID: 333 RVA: 0x00017A64 File Offset: 0x00015C64
		public bool HasCharacter(int character)
		{
			return this.m_characterDictionary != null && this.m_characterDictionary.ContainsKey(character);
		}

		// Token: 0x0600014E RID: 334 RVA: 0x00017A64 File Offset: 0x00015C64
		public bool HasCharacter(char character)
		{
			return this.m_characterDictionary != null && this.m_characterDictionary.ContainsKey((int)character);
		}

		// Token: 0x0600014F RID: 335 RVA: 0x00017A84 File Offset: 0x00015C84
		public bool HasCharacter(char character, bool searchFallbacks)
		{
			if (this.m_characterDictionary == null)
			{
				this.ReadFontDefinition();
				if (this.m_characterDictionary == null)
				{
					return false;
				}
			}
			if (this.m_characterDictionary.ContainsKey((int)character))
			{
				return true;
			}
			if (searchFallbacks)
			{
				if (this.fallbackFontAssets != null && this.fallbackFontAssets.Count > 0)
				{
					int num = 0;
					while (num < this.fallbackFontAssets.Count && this.fallbackFontAssets[num] != null)
					{
						if (this.fallbackFontAssets[num].HasCharacter_Internal(character, searchFallbacks))
						{
							return true;
						}
						num++;
					}
				}
				if (TMP_Settings.fallbackFontAssets != null && TMP_Settings.fallbackFontAssets.Count > 0)
				{
					int num2 = 0;
					while (num2 < TMP_Settings.fallbackFontAssets.Count && TMP_Settings.fallbackFontAssets[num2] != null)
					{
						if (TMP_Settings.fallbackFontAssets[num2].characterDictionary == null)
						{
							TMP_Settings.fallbackFontAssets[num2].ReadFontDefinition();
						}
						if (TMP_Settings.fallbackFontAssets[num2].characterDictionary != null && TMP_Settings.fallbackFontAssets[num2].HasCharacter_Internal(character, searchFallbacks))
						{
							return true;
						}
						num2++;
					}
				}
				if (TMP_Settings.defaultFontAsset != null)
				{
					if (TMP_Settings.defaultFontAsset.characterDictionary == null)
					{
						TMP_Settings.defaultFontAsset.ReadFontDefinition();
					}
					if (TMP_Settings.defaultFontAsset.characterDictionary != null && TMP_Settings.defaultFontAsset.HasCharacter_Internal(character, searchFallbacks))
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x06000150 RID: 336 RVA: 0x00017BDC File Offset: 0x00015DDC
		private bool HasCharacter_Internal(char character, bool searchFallbacks)
		{
			if (this.m_characterDictionary == null)
			{
				this.ReadFontDefinition();
				if (this.m_characterDictionary == null)
				{
					return false;
				}
			}
			if (this.m_characterDictionary.ContainsKey((int)character))
			{
				return true;
			}
			if (searchFallbacks && this.fallbackFontAssets != null && this.fallbackFontAssets.Count > 0)
			{
				int num = 0;
				while (num < this.fallbackFontAssets.Count && this.fallbackFontAssets[num] != null)
				{
					if (this.fallbackFontAssets[num].HasCharacter_Internal(character, searchFallbacks))
					{
						return true;
					}
					num++;
				}
			}
			return false;
		}

		// Token: 0x06000151 RID: 337 RVA: 0x00017C6C File Offset: 0x00015E6C
		public bool HasCharacters(string text, out List<char> missingCharacters)
		{
			if (this.m_characterDictionary == null)
			{
				missingCharacters = null;
				return false;
			}
			missingCharacters = new List<char>();
			for (int i = 0; i < text.Length; i++)
			{
				if (!this.m_characterDictionary.ContainsKey((int)text[i]))
				{
					missingCharacters.Add(text[i]);
				}
			}
			return missingCharacters.Count == 0;
		}

		// Token: 0x06000152 RID: 338 RVA: 0x00017CCC File Offset: 0x00015ECC
		public bool HasCharacters(string text)
		{
			if (this.m_characterDictionary == null)
			{
				return false;
			}
			for (int i = 0; i < text.Length; i++)
			{
				if (!this.m_characterDictionary.ContainsKey((int)text[i]))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06000153 RID: 339 RVA: 0x00017D0C File Offset: 0x00015F0C
		public static string GetCharacters(TMP_FontAsset fontAsset)
		{
			string text = string.Empty;
			for (int i = 0; i < fontAsset.m_glyphInfoList.Count; i++)
			{
				text += ((char)fontAsset.m_glyphInfoList[i].id).ToString();
			}
			return text;
		}

		// Token: 0x06000154 RID: 340 RVA: 0x00017D58 File Offset: 0x00015F58
		public static int[] GetCharactersArray(TMP_FontAsset fontAsset)
		{
			int[] array = new int[fontAsset.m_glyphInfoList.Count];
			for (int i = 0; i < fontAsset.m_glyphInfoList.Count; i++)
			{
				array[i] = fontAsset.m_glyphInfoList[i].id;
			}
			return array;
		}

		// Token: 0x06000155 RID: 341 RVA: 0x00017DA1 File Offset: 0x00015FA1
		public TMP_FontAsset()
		{
		}

		// Token: 0x04000085 RID: 133
		private static TMP_FontAsset s_defaultFontAsset;

		// Token: 0x04000086 RID: 134
		public TMP_FontAsset.FontAssetTypes fontAssetType;

		// Token: 0x04000087 RID: 135
		[SerializeField]
		private FaceInfo m_fontInfo;

		// Token: 0x04000088 RID: 136
		[SerializeField]
		public Texture2D atlas;

		// Token: 0x04000089 RID: 137
		[SerializeField]
		private List<TMP_Glyph> m_glyphInfoList;

		// Token: 0x0400008A RID: 138
		private Dictionary<int, TMP_Glyph> m_characterDictionary;

		// Token: 0x0400008B RID: 139
		private Dictionary<int, KerningPair> m_kerningDictionary;

		// Token: 0x0400008C RID: 140
		[SerializeField]
		private KerningTable m_kerningInfo;

		// Token: 0x0400008D RID: 141
		[SerializeField]
		private KerningPair m_kerningPair;

		// Token: 0x0400008E RID: 142
		[SerializeField]
		public List<TMP_FontAsset> fallbackFontAssets;

		// Token: 0x0400008F RID: 143
		[SerializeField]
		public FontAssetCreationSettings m_CreationSettings;

		// Token: 0x04000090 RID: 144
		[SerializeField]
		public TMP_FontWeights[] fontWeights = new TMP_FontWeights[10];

		// Token: 0x04000091 RID: 145
		private int[] m_characterSet;

		// Token: 0x04000092 RID: 146
		public float normalStyle;

		// Token: 0x04000093 RID: 147
		public float normalSpacingOffset;

		// Token: 0x04000094 RID: 148
		public float boldStyle = 0.75f;

		// Token: 0x04000095 RID: 149
		public float boldSpacing = 7f;

		// Token: 0x04000096 RID: 150
		public byte italicStyle = 35;

		// Token: 0x04000097 RID: 151
		public byte tabSize = 10;

		// Token: 0x04000098 RID: 152
		private byte m_oldTabSize;

		// Token: 0x0200006D RID: 109
		public enum FontAssetTypes
		{
			// Token: 0x04000417 RID: 1047
			None,
			// Token: 0x04000418 RID: 1048
			SDF,
			// Token: 0x04000419 RID: 1049
			Bitmap
		}

		// Token: 0x0200006E RID: 110
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x060004B9 RID: 1209 RVA: 0x0002F5AD File Offset: 0x0002D7AD
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x060004BA RID: 1210 RVA: 0x000159ED File Offset: 0x00013BED
			public <>c()
			{
			}

			// Token: 0x060004BB RID: 1211 RVA: 0x0002F5B9 File Offset: 0x0002D7B9
			internal int <AddGlyphInfo>b__37_0(TMP_Glyph s)
			{
				return s.id;
			}

			// Token: 0x060004BC RID: 1212 RVA: 0x0002F5B9 File Offset: 0x0002D7B9
			internal int <SortGlyphs>b__40_0(TMP_Glyph item)
			{
				return item.id;
			}

			// Token: 0x0400041A RID: 1050
			public static readonly TMP_FontAsset.<>c <>9 = new TMP_FontAsset.<>c();

			// Token: 0x0400041B RID: 1051
			public static Func<TMP_Glyph, int> <>9__37_0;

			// Token: 0x0400041C RID: 1052
			public static Func<TMP_Glyph, int> <>9__40_0;
		}
	}
}
