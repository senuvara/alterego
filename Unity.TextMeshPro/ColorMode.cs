﻿using System;

namespace TMPro
{
	// Token: 0x0200000D RID: 13
	public enum ColorMode
	{
		// Token: 0x04000056 RID: 86
		Single,
		// Token: 0x04000057 RID: 87
		HorizontalGradient,
		// Token: 0x04000058 RID: 88
		VerticalGradient,
		// Token: 0x04000059 RID: 89
		FourCornersGradient
	}
}
