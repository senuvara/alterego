﻿using System;

namespace TMPro
{
	// Token: 0x02000032 RID: 50
	public enum TextOverflowModes
	{
		// Token: 0x0400019D RID: 413
		Overflow,
		// Token: 0x0400019E RID: 414
		Ellipsis,
		// Token: 0x0400019F RID: 415
		Masking,
		// Token: 0x040001A0 RID: 416
		Truncate,
		// Token: 0x040001A1 RID: 417
		ScrollRect,
		// Token: 0x040001A2 RID: 418
		Page,
		// Token: 0x040001A3 RID: 419
		Linked
	}
}
