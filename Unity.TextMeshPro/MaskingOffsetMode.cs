﻿using System;

namespace TMPro
{
	// Token: 0x02000033 RID: 51
	public enum MaskingOffsetMode
	{
		// Token: 0x040001A5 RID: 421
		Percentage,
		// Token: 0x040001A6 RID: 422
		Pixel
	}
}
