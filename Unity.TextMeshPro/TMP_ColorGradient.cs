﻿using System;
using UnityEngine;

namespace TMPro
{
	// Token: 0x0200000E RID: 14
	[Serializable]
	public class TMP_ColorGradient : ScriptableObject
	{
		// Token: 0x060000DF RID: 223 RVA: 0x000155B8 File Offset: 0x000137B8
		public TMP_ColorGradient()
		{
			this.colorMode = ColorMode.FourCornersGradient;
			this.topLeft = TMP_ColorGradient.k_DefaultColor;
			this.topRight = TMP_ColorGradient.k_DefaultColor;
			this.bottomLeft = TMP_ColorGradient.k_DefaultColor;
			this.bottomRight = TMP_ColorGradient.k_DefaultColor;
		}

		// Token: 0x060000E0 RID: 224 RVA: 0x00015605 File Offset: 0x00013805
		public TMP_ColorGradient(Color color)
		{
			this.colorMode = ColorMode.FourCornersGradient;
			this.topLeft = color;
			this.topRight = color;
			this.bottomLeft = color;
			this.bottomRight = color;
		}

		// Token: 0x060000E1 RID: 225 RVA: 0x00015637 File Offset: 0x00013837
		public TMP_ColorGradient(Color color0, Color color1, Color color2, Color color3)
		{
			this.colorMode = ColorMode.FourCornersGradient;
			this.topLeft = color0;
			this.topRight = color1;
			this.bottomLeft = color2;
			this.bottomRight = color3;
		}

		// Token: 0x060000E2 RID: 226 RVA: 0x0001566A File Offset: 0x0001386A
		// Note: this type is marked as 'beforefieldinit'.
		static TMP_ColorGradient()
		{
		}

		// Token: 0x0400005A RID: 90
		public ColorMode colorMode = ColorMode.FourCornersGradient;

		// Token: 0x0400005B RID: 91
		public Color topLeft;

		// Token: 0x0400005C RID: 92
		public Color topRight;

		// Token: 0x0400005D RID: 93
		public Color bottomLeft;

		// Token: 0x0400005E RID: 94
		public Color bottomRight;

		// Token: 0x0400005F RID: 95
		private const ColorMode k_DefaultColorMode = ColorMode.FourCornersGradient;

		// Token: 0x04000060 RID: 96
		private static readonly Color k_DefaultColor = Color.white;
	}
}
