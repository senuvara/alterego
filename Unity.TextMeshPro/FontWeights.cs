﻿using System;

namespace TMPro
{
	// Token: 0x02000036 RID: 54
	public enum FontWeights
	{
		// Token: 0x040001B9 RID: 441
		Thin = 100,
		// Token: 0x040001BA RID: 442
		ExtraLight = 200,
		// Token: 0x040001BB RID: 443
		Light = 300,
		// Token: 0x040001BC RID: 444
		Normal = 400,
		// Token: 0x040001BD RID: 445
		Medium = 500,
		// Token: 0x040001BE RID: 446
		SemiBold = 600,
		// Token: 0x040001BF RID: 447
		Bold = 700,
		// Token: 0x040001C0 RID: 448
		Heavy = 800,
		// Token: 0x040001C1 RID: 449
		Black = 900
	}
}
