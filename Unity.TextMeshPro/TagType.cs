﻿using System;

namespace TMPro
{
	// Token: 0x02000038 RID: 56
	public enum TagType
	{
		// Token: 0x040001C7 RID: 455
		None,
		// Token: 0x040001C8 RID: 456
		NumericalValue,
		// Token: 0x040001C9 RID: 457
		StringValue,
		// Token: 0x040001CA RID: 458
		ColorValue = 4
	}
}
