﻿using System;

namespace TMPro
{
	// Token: 0x02000049 RID: 73
	[Serializable]
	public class TMP_Glyph : TMP_TextElement
	{
		// Token: 0x06000467 RID: 1127 RVA: 0x0002DC78 File Offset: 0x0002BE78
		public static TMP_Glyph Clone(TMP_Glyph source)
		{
			return new TMP_Glyph
			{
				id = source.id,
				x = source.x,
				y = source.y,
				width = source.width,
				height = source.height,
				xOffset = source.xOffset,
				yOffset = source.yOffset,
				xAdvance = source.xAdvance,
				scale = source.scale
			};
		}

		// Token: 0x06000468 RID: 1128 RVA: 0x0001E27D File Offset: 0x0001C47D
		public TMP_Glyph()
		{
		}
	}
}
