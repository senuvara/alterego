﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TMPro
{
	// Token: 0x02000026 RID: 38
	public class TMP_SpriteAsset : TMP_Asset
	{
		// Token: 0x1700008B RID: 139
		// (get) Token: 0x0600027D RID: 637 RVA: 0x0001E331 File Offset: 0x0001C531
		public static TMP_SpriteAsset defaultSpriteAsset
		{
			get
			{
				if (TMP_SpriteAsset.m_defaultSpriteAsset == null)
				{
					TMP_SpriteAsset.m_defaultSpriteAsset = Resources.Load<TMP_SpriteAsset>("Sprite Assets/Default Sprite Asset");
				}
				return TMP_SpriteAsset.m_defaultSpriteAsset;
			}
		}

		// Token: 0x0600027E RID: 638 RVA: 0x0000297D File Offset: 0x00000B7D
		private void OnEnable()
		{
		}

		// Token: 0x0600027F RID: 639 RVA: 0x0001E354 File Offset: 0x0001C554
		private Material GetDefaultSpriteMaterial()
		{
			ShaderUtilities.GetShaderPropertyIDs();
			Material material = new Material(Shader.Find("TextMeshPro/Sprite"));
			material.SetTexture(ShaderUtilities.ID_MainTex, this.spriteSheet);
			material.hideFlags = HideFlags.HideInHierarchy;
			return material;
		}

		// Token: 0x06000280 RID: 640 RVA: 0x0001E384 File Offset: 0x0001C584
		public void UpdateLookupTables()
		{
			if (this.m_NameLookup == null)
			{
				this.m_NameLookup = new Dictionary<int, int>();
			}
			this.m_NameLookup.Clear();
			if (this.m_UnicodeLookup == null)
			{
				this.m_UnicodeLookup = new Dictionary<int, int>();
			}
			this.m_UnicodeLookup.Clear();
			for (int i = 0; i < this.spriteInfoList.Count; i++)
			{
				int hashCode = this.spriteInfoList[i].hashCode;
				if (!this.m_NameLookup.ContainsKey(hashCode))
				{
					this.m_NameLookup.Add(hashCode, i);
				}
				int unicode = this.spriteInfoList[i].unicode;
				if (!this.m_UnicodeLookup.ContainsKey(unicode))
				{
					this.m_UnicodeLookup.Add(unicode, i);
				}
			}
		}

		// Token: 0x06000281 RID: 641 RVA: 0x0001E440 File Offset: 0x0001C640
		public int GetSpriteIndexFromHashcode(int hashCode)
		{
			if (this.m_NameLookup == null)
			{
				this.UpdateLookupTables();
			}
			int result = 0;
			if (this.m_NameLookup.TryGetValue(hashCode, out result))
			{
				return result;
			}
			return -1;
		}

		// Token: 0x06000282 RID: 642 RVA: 0x0001E470 File Offset: 0x0001C670
		public int GetSpriteIndexFromUnicode(int unicode)
		{
			if (this.m_UnicodeLookup == null)
			{
				this.UpdateLookupTables();
			}
			int result = 0;
			if (this.m_UnicodeLookup.TryGetValue(unicode, out result))
			{
				return result;
			}
			return -1;
		}

		// Token: 0x06000283 RID: 643 RVA: 0x0001E4A0 File Offset: 0x0001C6A0
		public int GetSpriteIndexFromName(string name)
		{
			if (this.m_NameLookup == null)
			{
				this.UpdateLookupTables();
			}
			int simpleHashCode = TMP_TextUtilities.GetSimpleHashCode(name);
			return this.GetSpriteIndexFromHashcode(simpleHashCode);
		}

		// Token: 0x06000284 RID: 644 RVA: 0x0001E4CC File Offset: 0x0001C6CC
		public static TMP_SpriteAsset SearchForSpriteByUnicode(TMP_SpriteAsset spriteAsset, int unicode, bool includeFallbacks, out int spriteIndex)
		{
			if (spriteAsset == null)
			{
				spriteIndex = -1;
				return null;
			}
			spriteIndex = spriteAsset.GetSpriteIndexFromUnicode(unicode);
			if (spriteIndex != -1)
			{
				return spriteAsset;
			}
			if (TMP_SpriteAsset.k_searchedSpriteAssets == null)
			{
				TMP_SpriteAsset.k_searchedSpriteAssets = new List<int>();
			}
			TMP_SpriteAsset.k_searchedSpriteAssets.Clear();
			int instanceID = spriteAsset.GetInstanceID();
			TMP_SpriteAsset.k_searchedSpriteAssets.Add(instanceID);
			if (includeFallbacks && spriteAsset.fallbackSpriteAssets != null && spriteAsset.fallbackSpriteAssets.Count > 0)
			{
				return TMP_SpriteAsset.SearchForSpriteByUnicodeInternal(spriteAsset.fallbackSpriteAssets, unicode, includeFallbacks, out spriteIndex);
			}
			if (includeFallbacks && TMP_Settings.defaultSpriteAsset != null)
			{
				return TMP_SpriteAsset.SearchForSpriteByUnicodeInternal(TMP_Settings.defaultSpriteAsset, unicode, includeFallbacks, out spriteIndex);
			}
			spriteIndex = -1;
			return null;
		}

		// Token: 0x06000285 RID: 645 RVA: 0x0001E570 File Offset: 0x0001C770
		private static TMP_SpriteAsset SearchForSpriteByUnicodeInternal(List<TMP_SpriteAsset> spriteAssets, int unicode, bool includeFallbacks, out int spriteIndex)
		{
			for (int i = 0; i < spriteAssets.Count; i++)
			{
				TMP_SpriteAsset tmp_SpriteAsset = spriteAssets[i];
				if (!(tmp_SpriteAsset == null))
				{
					int instanceID = tmp_SpriteAsset.GetInstanceID();
					if (!TMP_SpriteAsset.k_searchedSpriteAssets.Contains(instanceID))
					{
						TMP_SpriteAsset.k_searchedSpriteAssets.Add(instanceID);
						tmp_SpriteAsset = TMP_SpriteAsset.SearchForSpriteByUnicodeInternal(tmp_SpriteAsset, unicode, includeFallbacks, out spriteIndex);
						if (tmp_SpriteAsset != null)
						{
							return tmp_SpriteAsset;
						}
					}
				}
			}
			spriteIndex = -1;
			return null;
		}

		// Token: 0x06000286 RID: 646 RVA: 0x0001E5D7 File Offset: 0x0001C7D7
		private static TMP_SpriteAsset SearchForSpriteByUnicodeInternal(TMP_SpriteAsset spriteAsset, int unicode, bool includeFallbacks, out int spriteIndex)
		{
			spriteIndex = spriteAsset.GetSpriteIndexFromUnicode(unicode);
			if (spriteIndex != -1)
			{
				return spriteAsset;
			}
			if (includeFallbacks && spriteAsset.fallbackSpriteAssets != null && spriteAsset.fallbackSpriteAssets.Count > 0)
			{
				return TMP_SpriteAsset.SearchForSpriteByUnicodeInternal(spriteAsset.fallbackSpriteAssets, unicode, includeFallbacks, out spriteIndex);
			}
			spriteIndex = -1;
			return null;
		}

		// Token: 0x06000287 RID: 647 RVA: 0x0001E618 File Offset: 0x0001C818
		public static TMP_SpriteAsset SearchForSpriteByHashCode(TMP_SpriteAsset spriteAsset, int hashCode, bool includeFallbacks, out int spriteIndex)
		{
			if (spriteAsset == null)
			{
				spriteIndex = -1;
				return null;
			}
			spriteIndex = spriteAsset.GetSpriteIndexFromHashcode(hashCode);
			if (spriteIndex != -1)
			{
				return spriteAsset;
			}
			if (TMP_SpriteAsset.k_searchedSpriteAssets == null)
			{
				TMP_SpriteAsset.k_searchedSpriteAssets = new List<int>();
			}
			TMP_SpriteAsset.k_searchedSpriteAssets.Clear();
			int instanceID = spriteAsset.GetInstanceID();
			TMP_SpriteAsset.k_searchedSpriteAssets.Add(instanceID);
			if (includeFallbacks && spriteAsset.fallbackSpriteAssets != null && spriteAsset.fallbackSpriteAssets.Count > 0)
			{
				return TMP_SpriteAsset.SearchForSpriteByHashCodeInternal(spriteAsset.fallbackSpriteAssets, hashCode, includeFallbacks, out spriteIndex);
			}
			if (includeFallbacks && TMP_Settings.defaultSpriteAsset != null)
			{
				return TMP_SpriteAsset.SearchForSpriteByHashCodeInternal(TMP_Settings.defaultSpriteAsset, hashCode, includeFallbacks, out spriteIndex);
			}
			spriteIndex = -1;
			return null;
		}

		// Token: 0x06000288 RID: 648 RVA: 0x0001E6BC File Offset: 0x0001C8BC
		private static TMP_SpriteAsset SearchForSpriteByHashCodeInternal(List<TMP_SpriteAsset> spriteAssets, int hashCode, bool searchFallbacks, out int spriteIndex)
		{
			for (int i = 0; i < spriteAssets.Count; i++)
			{
				TMP_SpriteAsset tmp_SpriteAsset = spriteAssets[i];
				if (!(tmp_SpriteAsset == null))
				{
					int instanceID = tmp_SpriteAsset.GetInstanceID();
					if (!TMP_SpriteAsset.k_searchedSpriteAssets.Contains(instanceID))
					{
						TMP_SpriteAsset.k_searchedSpriteAssets.Add(instanceID);
						tmp_SpriteAsset = TMP_SpriteAsset.SearchForSpriteByHashCodeInternal(tmp_SpriteAsset, hashCode, searchFallbacks, out spriteIndex);
						if (tmp_SpriteAsset != null)
						{
							return tmp_SpriteAsset;
						}
					}
				}
			}
			spriteIndex = -1;
			return null;
		}

		// Token: 0x06000289 RID: 649 RVA: 0x0001E723 File Offset: 0x0001C923
		private static TMP_SpriteAsset SearchForSpriteByHashCodeInternal(TMP_SpriteAsset spriteAsset, int hashCode, bool searchFallbacks, out int spriteIndex)
		{
			spriteIndex = spriteAsset.GetSpriteIndexFromHashcode(hashCode);
			if (spriteIndex != -1)
			{
				return spriteAsset;
			}
			if (searchFallbacks && spriteAsset.fallbackSpriteAssets != null && spriteAsset.fallbackSpriteAssets.Count > 0)
			{
				return TMP_SpriteAsset.SearchForSpriteByHashCodeInternal(spriteAsset.fallbackSpriteAssets, hashCode, searchFallbacks, out spriteIndex);
			}
			spriteIndex = -1;
			return null;
		}

		// Token: 0x0600028A RID: 650 RVA: 0x0001E761 File Offset: 0x0001C961
		public TMP_SpriteAsset()
		{
		}

		// Token: 0x04000134 RID: 308
		internal Dictionary<int, int> m_UnicodeLookup;

		// Token: 0x04000135 RID: 309
		internal Dictionary<int, int> m_NameLookup;

		// Token: 0x04000136 RID: 310
		public static TMP_SpriteAsset m_defaultSpriteAsset;

		// Token: 0x04000137 RID: 311
		public Texture spriteSheet;

		// Token: 0x04000138 RID: 312
		public List<TMP_Sprite> spriteInfoList;

		// Token: 0x04000139 RID: 313
		[SerializeField]
		public List<TMP_SpriteAsset> fallbackSpriteAssets;

		// Token: 0x0400013A RID: 314
		private static List<int> k_searchedSpriteAssets;
	}
}
