﻿using System;

namespace TMPro
{
	// Token: 0x02000055 RID: 85
	public struct TMP_LinkInfo
	{
		// Token: 0x06000482 RID: 1154 RVA: 0x0002E19C File Offset: 0x0002C39C
		internal void SetLinkID(char[] text, int startIndex, int length)
		{
			if (this.linkID == null || this.linkID.Length < length)
			{
				this.linkID = new char[length];
			}
			for (int i = 0; i < length; i++)
			{
				this.linkID[i] = text[startIndex + i];
			}
		}

		// Token: 0x06000483 RID: 1155 RVA: 0x0002E1E4 File Offset: 0x0002C3E4
		public string GetLinkText()
		{
			string text = string.Empty;
			TMP_TextInfo textInfo = this.textComponent.textInfo;
			for (int i = this.linkTextfirstCharacterIndex; i < this.linkTextfirstCharacterIndex + this.linkTextLength; i++)
			{
				text += textInfo.characterInfo[i].character.ToString();
			}
			return text;
		}

		// Token: 0x06000484 RID: 1156 RVA: 0x0002E23E File Offset: 0x0002C43E
		public string GetLinkID()
		{
			if (this.textComponent == null)
			{
				return string.Empty;
			}
			return new string(this.linkID, 0, this.linkIdLength);
		}

		// Token: 0x0400035C RID: 860
		public TMP_Text textComponent;

		// Token: 0x0400035D RID: 861
		public int hashCode;

		// Token: 0x0400035E RID: 862
		public int linkIdFirstCharacterIndex;

		// Token: 0x0400035F RID: 863
		public int linkIdLength;

		// Token: 0x04000360 RID: 864
		public int linkTextfirstCharacterIndex;

		// Token: 0x04000361 RID: 865
		public int linkTextLength;

		// Token: 0x04000362 RID: 866
		internal char[] linkID;
	}
}
