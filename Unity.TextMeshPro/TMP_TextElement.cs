﻿using System;

namespace TMPro
{
	// Token: 0x0200003A RID: 58
	[Serializable]
	public class TMP_TextElement
	{
		// Token: 0x060003FF RID: 1023 RVA: 0x000159ED File Offset: 0x00013BED
		public TMP_TextElement()
		{
		}

		// Token: 0x04000298 RID: 664
		public int id;

		// Token: 0x04000299 RID: 665
		public float x;

		// Token: 0x0400029A RID: 666
		public float y;

		// Token: 0x0400029B RID: 667
		public float width;

		// Token: 0x0400029C RID: 668
		public float height;

		// Token: 0x0400029D RID: 669
		public float xOffset;

		// Token: 0x0400029E RID: 670
		public float yOffset;

		// Token: 0x0400029F RID: 671
		public float xAdvance;

		// Token: 0x040002A0 RID: 672
		public float scale;
	}
}
