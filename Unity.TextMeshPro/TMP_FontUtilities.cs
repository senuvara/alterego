﻿using System;
using System.Collections.Generic;

namespace TMPro
{
	// Token: 0x0200004F RID: 79
	public static class TMP_FontUtilities
	{
		// Token: 0x0600047C RID: 1148 RVA: 0x0002E03C File Offset: 0x0002C23C
		public static TMP_FontAsset SearchForGlyph(TMP_FontAsset font, int character, out TMP_Glyph glyph)
		{
			if (TMP_FontUtilities.k_searchedFontAssets == null)
			{
				TMP_FontUtilities.k_searchedFontAssets = new List<int>();
			}
			TMP_FontUtilities.k_searchedFontAssets.Clear();
			return TMP_FontUtilities.SearchForGlyphInternal(font, character, out glyph);
		}

		// Token: 0x0600047D RID: 1149 RVA: 0x0002E061 File Offset: 0x0002C261
		public static TMP_FontAsset SearchForGlyph(List<TMP_FontAsset> fonts, int character, out TMP_Glyph glyph)
		{
			return TMP_FontUtilities.SearchForGlyphInternal(fonts, character, out glyph);
		}

		// Token: 0x0600047E RID: 1150 RVA: 0x0002E06C File Offset: 0x0002C26C
		private static TMP_FontAsset SearchForGlyphInternal(TMP_FontAsset font, int character, out TMP_Glyph glyph)
		{
			glyph = null;
			if (font == null)
			{
				return null;
			}
			if (font.characterDictionary.TryGetValue(character, out glyph))
			{
				return font;
			}
			if (font.fallbackFontAssets != null && font.fallbackFontAssets.Count > 0)
			{
				int num = 0;
				while (num < font.fallbackFontAssets.Count && glyph == null)
				{
					TMP_FontAsset tmp_FontAsset = font.fallbackFontAssets[num];
					if (!(tmp_FontAsset == null))
					{
						int instanceID = tmp_FontAsset.GetInstanceID();
						if (!TMP_FontUtilities.k_searchedFontAssets.Contains(instanceID))
						{
							TMP_FontUtilities.k_searchedFontAssets.Add(instanceID);
							tmp_FontAsset = TMP_FontUtilities.SearchForGlyphInternal(tmp_FontAsset, character, out glyph);
							if (tmp_FontAsset != null)
							{
								return tmp_FontAsset;
							}
						}
					}
					num++;
				}
			}
			return null;
		}

		// Token: 0x0600047F RID: 1151 RVA: 0x0002E114 File Offset: 0x0002C314
		private static TMP_FontAsset SearchForGlyphInternal(List<TMP_FontAsset> fonts, int character, out TMP_Glyph glyph)
		{
			glyph = null;
			if (fonts != null && fonts.Count > 0)
			{
				for (int i = 0; i < fonts.Count; i++)
				{
					TMP_FontAsset tmp_FontAsset = TMP_FontUtilities.SearchForGlyphInternal(fonts[i], character, out glyph);
					if (tmp_FontAsset != null)
					{
						return tmp_FontAsset;
					}
				}
			}
			return null;
		}

		// Token: 0x04000322 RID: 802
		private static List<int> k_searchedFontAssets;
	}
}
