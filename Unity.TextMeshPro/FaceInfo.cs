﻿using System;

namespace TMPro
{
	// Token: 0x02000048 RID: 72
	[Serializable]
	public class FaceInfo
	{
		// Token: 0x06000466 RID: 1126 RVA: 0x000159ED File Offset: 0x00013BED
		public FaceInfo()
		{
		}

		// Token: 0x040002F0 RID: 752
		public string Name;

		// Token: 0x040002F1 RID: 753
		public float PointSize;

		// Token: 0x040002F2 RID: 754
		public float Scale;

		// Token: 0x040002F3 RID: 755
		public int CharacterCount;

		// Token: 0x040002F4 RID: 756
		public float LineHeight;

		// Token: 0x040002F5 RID: 757
		public float Baseline;

		// Token: 0x040002F6 RID: 758
		public float Ascender;

		// Token: 0x040002F7 RID: 759
		public float CapHeight;

		// Token: 0x040002F8 RID: 760
		public float Descender;

		// Token: 0x040002F9 RID: 761
		public float CenterLine;

		// Token: 0x040002FA RID: 762
		public float SuperscriptOffset;

		// Token: 0x040002FB RID: 763
		public float SubscriptOffset;

		// Token: 0x040002FC RID: 764
		public float SubSize;

		// Token: 0x040002FD RID: 765
		public float Underline;

		// Token: 0x040002FE RID: 766
		public float UnderlineThickness;

		// Token: 0x040002FF RID: 767
		public float strikethrough;

		// Token: 0x04000300 RID: 768
		public float strikethroughThickness;

		// Token: 0x04000301 RID: 769
		public float TabWidth;

		// Token: 0x04000302 RID: 770
		public float Padding;

		// Token: 0x04000303 RID: 771
		public float AtlasWidth;

		// Token: 0x04000304 RID: 772
		public float AtlasHeight;
	}
}
