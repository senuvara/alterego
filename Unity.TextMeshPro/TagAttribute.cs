﻿using System;

namespace TMPro
{
	// Token: 0x0200005B RID: 91
	public struct TagAttribute
	{
		// Token: 0x040003A5 RID: 933
		public int startIndex;

		// Token: 0x040003A6 RID: 934
		public int length;

		// Token: 0x040003A7 RID: 935
		public int hashCode;
	}
}
