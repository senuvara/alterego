﻿using System;
using UnityEngine;

namespace TMPro
{
	// Token: 0x02000053 RID: 83
	[Serializable]
	public struct VertexGradient
	{
		// Token: 0x06000480 RID: 1152 RVA: 0x0002E15C File Offset: 0x0002C35C
		public VertexGradient(Color color)
		{
			this.topLeft = color;
			this.topRight = color;
			this.bottomLeft = color;
			this.bottomRight = color;
		}

		// Token: 0x06000481 RID: 1153 RVA: 0x0002E17A File Offset: 0x0002C37A
		public VertexGradient(Color color0, Color color1, Color color2, Color color3)
		{
			this.topLeft = color0;
			this.topRight = color1;
			this.bottomLeft = color2;
			this.bottomRight = color3;
		}

		// Token: 0x04000353 RID: 851
		public Color topLeft;

		// Token: 0x04000354 RID: 852
		public Color topRight;

		// Token: 0x04000355 RID: 853
		public Color bottomLeft;

		// Token: 0x04000356 RID: 854
		public Color bottomRight;
	}
}
