﻿using System;

namespace TMPro
{
	// Token: 0x02000034 RID: 52
	public enum TextureMappingOptions
	{
		// Token: 0x040001A8 RID: 424
		Character,
		// Token: 0x040001A9 RID: 425
		Line,
		// Token: 0x040001AA RID: 426
		Paragraph,
		// Token: 0x040001AB RID: 427
		MatchAspect
	}
}
