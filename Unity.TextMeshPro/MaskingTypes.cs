﻿using System;

namespace TMPro
{
	// Token: 0x02000031 RID: 49
	public enum MaskingTypes
	{
		// Token: 0x04000199 RID: 409
		MaskOff,
		// Token: 0x0400019A RID: 410
		MaskHard,
		// Token: 0x0400019B RID: 411
		MaskSoft
	}
}
