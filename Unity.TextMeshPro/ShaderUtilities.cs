﻿using System;
using System.Linq;
using UnityEngine;

namespace TMPro
{
	// Token: 0x0200005D RID: 93
	public static class ShaderUtilities
	{
		// Token: 0x0600048A RID: 1162 RVA: 0x0002E428 File Offset: 0x0002C628
		static ShaderUtilities()
		{
			ShaderUtilities.GetShaderPropertyIDs();
		}

		// Token: 0x0600048B RID: 1163 RVA: 0x0002E4B0 File Offset: 0x0002C6B0
		public static void GetShaderPropertyIDs()
		{
			if (!ShaderUtilities.isInitialized)
			{
				ShaderUtilities.isInitialized = true;
				ShaderUtilities.ID_MainTex = Shader.PropertyToID("_MainTex");
				ShaderUtilities.ID_FaceTex = Shader.PropertyToID("_FaceTex");
				ShaderUtilities.ID_FaceColor = Shader.PropertyToID("_FaceColor");
				ShaderUtilities.ID_FaceDilate = Shader.PropertyToID("_FaceDilate");
				ShaderUtilities.ID_Shininess = Shader.PropertyToID("_FaceShininess");
				ShaderUtilities.ID_UnderlayColor = Shader.PropertyToID("_UnderlayColor");
				ShaderUtilities.ID_UnderlayOffsetX = Shader.PropertyToID("_UnderlayOffsetX");
				ShaderUtilities.ID_UnderlayOffsetY = Shader.PropertyToID("_UnderlayOffsetY");
				ShaderUtilities.ID_UnderlayDilate = Shader.PropertyToID("_UnderlayDilate");
				ShaderUtilities.ID_UnderlaySoftness = Shader.PropertyToID("_UnderlaySoftness");
				ShaderUtilities.ID_WeightNormal = Shader.PropertyToID("_WeightNormal");
				ShaderUtilities.ID_WeightBold = Shader.PropertyToID("_WeightBold");
				ShaderUtilities.ID_OutlineTex = Shader.PropertyToID("_OutlineTex");
				ShaderUtilities.ID_OutlineWidth = Shader.PropertyToID("_OutlineWidth");
				ShaderUtilities.ID_OutlineSoftness = Shader.PropertyToID("_OutlineSoftness");
				ShaderUtilities.ID_OutlineColor = Shader.PropertyToID("_OutlineColor");
				ShaderUtilities.ID_Padding = Shader.PropertyToID("_Padding");
				ShaderUtilities.ID_GradientScale = Shader.PropertyToID("_GradientScale");
				ShaderUtilities.ID_ScaleX = Shader.PropertyToID("_ScaleX");
				ShaderUtilities.ID_ScaleY = Shader.PropertyToID("_ScaleY");
				ShaderUtilities.ID_PerspectiveFilter = Shader.PropertyToID("_PerspectiveFilter");
				ShaderUtilities.ID_TextureWidth = Shader.PropertyToID("_TextureWidth");
				ShaderUtilities.ID_TextureHeight = Shader.PropertyToID("_TextureHeight");
				ShaderUtilities.ID_BevelAmount = Shader.PropertyToID("_Bevel");
				ShaderUtilities.ID_LightAngle = Shader.PropertyToID("_LightAngle");
				ShaderUtilities.ID_EnvMap = Shader.PropertyToID("_Cube");
				ShaderUtilities.ID_EnvMatrix = Shader.PropertyToID("_EnvMatrix");
				ShaderUtilities.ID_EnvMatrixRotation = Shader.PropertyToID("_EnvMatrixRotation");
				ShaderUtilities.ID_GlowColor = Shader.PropertyToID("_GlowColor");
				ShaderUtilities.ID_GlowOffset = Shader.PropertyToID("_GlowOffset");
				ShaderUtilities.ID_GlowPower = Shader.PropertyToID("_GlowPower");
				ShaderUtilities.ID_GlowOuter = Shader.PropertyToID("_GlowOuter");
				ShaderUtilities.ID_MaskCoord = Shader.PropertyToID("_MaskCoord");
				ShaderUtilities.ID_ClipRect = Shader.PropertyToID("_ClipRect");
				ShaderUtilities.ID_UseClipRect = Shader.PropertyToID("_UseClipRect");
				ShaderUtilities.ID_MaskSoftnessX = Shader.PropertyToID("_MaskSoftnessX");
				ShaderUtilities.ID_MaskSoftnessY = Shader.PropertyToID("_MaskSoftnessY");
				ShaderUtilities.ID_VertexOffsetX = Shader.PropertyToID("_VertexOffsetX");
				ShaderUtilities.ID_VertexOffsetY = Shader.PropertyToID("_VertexOffsetY");
				ShaderUtilities.ID_StencilID = Shader.PropertyToID("_Stencil");
				ShaderUtilities.ID_StencilOp = Shader.PropertyToID("_StencilOp");
				ShaderUtilities.ID_StencilComp = Shader.PropertyToID("_StencilComp");
				ShaderUtilities.ID_StencilReadMask = Shader.PropertyToID("_StencilReadMask");
				ShaderUtilities.ID_StencilWriteMask = Shader.PropertyToID("_StencilWriteMask");
				ShaderUtilities.ID_ShaderFlags = Shader.PropertyToID("_ShaderFlags");
				ShaderUtilities.ID_ScaleRatio_A = Shader.PropertyToID("_ScaleRatioA");
				ShaderUtilities.ID_ScaleRatio_B = Shader.PropertyToID("_ScaleRatioB");
				ShaderUtilities.ID_ScaleRatio_C = Shader.PropertyToID("_ScaleRatioC");
			}
		}

		// Token: 0x0600048C RID: 1164 RVA: 0x0002E7A0 File Offset: 0x0002C9A0
		public static void UpdateShaderRatios(Material mat)
		{
			bool flag = !mat.shaderKeywords.Contains(ShaderUtilities.Keyword_Ratios);
			float @float = mat.GetFloat(ShaderUtilities.ID_GradientScale);
			float float2 = mat.GetFloat(ShaderUtilities.ID_FaceDilate);
			float float3 = mat.GetFloat(ShaderUtilities.ID_OutlineWidth);
			float float4 = mat.GetFloat(ShaderUtilities.ID_OutlineSoftness);
			float num = Mathf.Max(mat.GetFloat(ShaderUtilities.ID_WeightNormal), mat.GetFloat(ShaderUtilities.ID_WeightBold)) / 4f;
			float num2 = Mathf.Max(1f, num + float2 + float3 + float4);
			float value = flag ? ((@float - ShaderUtilities.m_clamp) / (@float * num2)) : 1f;
			mat.SetFloat(ShaderUtilities.ID_ScaleRatio_A, value);
			if (mat.HasProperty(ShaderUtilities.ID_GlowOffset))
			{
				float float5 = mat.GetFloat(ShaderUtilities.ID_GlowOffset);
				float float6 = mat.GetFloat(ShaderUtilities.ID_GlowOuter);
				float num3 = (num + float2) * (@float - ShaderUtilities.m_clamp);
				num2 = Mathf.Max(1f, float5 + float6);
				float value2 = flag ? (Mathf.Max(0f, @float - ShaderUtilities.m_clamp - num3) / (@float * num2)) : 1f;
				mat.SetFloat(ShaderUtilities.ID_ScaleRatio_B, value2);
			}
			if (mat.HasProperty(ShaderUtilities.ID_UnderlayOffsetX))
			{
				float float7 = mat.GetFloat(ShaderUtilities.ID_UnderlayOffsetX);
				float float8 = mat.GetFloat(ShaderUtilities.ID_UnderlayOffsetY);
				float float9 = mat.GetFloat(ShaderUtilities.ID_UnderlayDilate);
				float float10 = mat.GetFloat(ShaderUtilities.ID_UnderlaySoftness);
				float num4 = (num + float2) * (@float - ShaderUtilities.m_clamp);
				num2 = Mathf.Max(1f, Mathf.Max(Mathf.Abs(float7), Mathf.Abs(float8)) + float9 + float10);
				float value3 = flag ? (Mathf.Max(0f, @float - ShaderUtilities.m_clamp - num4) / (@float * num2)) : 1f;
				mat.SetFloat(ShaderUtilities.ID_ScaleRatio_C, value3);
			}
		}

		// Token: 0x0600048D RID: 1165 RVA: 0x0002E98B File Offset: 0x0002CB8B
		public static Vector4 GetFontExtent(Material material)
		{
			return Vector4.zero;
		}

		// Token: 0x0600048E RID: 1166 RVA: 0x0002E994 File Offset: 0x0002CB94
		public static bool IsMaskingEnabled(Material material)
		{
			return !(material == null) && material.HasProperty(ShaderUtilities.ID_ClipRect) && (material.shaderKeywords.Contains(ShaderUtilities.Keyword_MASK_SOFT) || material.shaderKeywords.Contains(ShaderUtilities.Keyword_MASK_HARD) || material.shaderKeywords.Contains(ShaderUtilities.Keyword_MASK_TEX));
		}

		// Token: 0x0600048F RID: 1167 RVA: 0x0002E9F4 File Offset: 0x0002CBF4
		public static float GetPadding(Material material, bool enableExtraPadding, bool isBold)
		{
			if (!ShaderUtilities.isInitialized)
			{
				ShaderUtilities.GetShaderPropertyIDs();
			}
			if (material == null)
			{
				return 0f;
			}
			int num = enableExtraPadding ? 4 : 0;
			if (!material.HasProperty(ShaderUtilities.ID_GradientScale))
			{
				if (material.HasProperty(ShaderUtilities.ID_Padding))
				{
					num += (int)material.GetFloat(ShaderUtilities.ID_Padding);
				}
				return (float)num;
			}
			Vector4 vector = Vector4.zero;
			Vector4 zero = Vector4.zero;
			float num2 = 0f;
			float num3 = 0f;
			float num4 = 0f;
			float num5 = 0f;
			float num6 = 0f;
			float num7 = 0f;
			float num8 = 0f;
			float num9 = 0f;
			ShaderUtilities.UpdateShaderRatios(material);
			string[] shaderKeywords = material.shaderKeywords;
			if (material.HasProperty(ShaderUtilities.ID_ScaleRatio_A))
			{
				num5 = material.GetFloat(ShaderUtilities.ID_ScaleRatio_A);
			}
			if (material.HasProperty(ShaderUtilities.ID_FaceDilate))
			{
				num2 = material.GetFloat(ShaderUtilities.ID_FaceDilate) * num5;
			}
			if (material.HasProperty(ShaderUtilities.ID_OutlineSoftness))
			{
				num3 = material.GetFloat(ShaderUtilities.ID_OutlineSoftness) * num5;
			}
			if (material.HasProperty(ShaderUtilities.ID_OutlineWidth))
			{
				num4 = material.GetFloat(ShaderUtilities.ID_OutlineWidth) * num5;
			}
			float num10 = num4 + num3 + num2;
			if (material.HasProperty(ShaderUtilities.ID_GlowOffset) && shaderKeywords.Contains(ShaderUtilities.Keyword_Glow))
			{
				if (material.HasProperty(ShaderUtilities.ID_ScaleRatio_B))
				{
					num6 = material.GetFloat(ShaderUtilities.ID_ScaleRatio_B);
				}
				num8 = material.GetFloat(ShaderUtilities.ID_GlowOffset) * num6;
				num9 = material.GetFloat(ShaderUtilities.ID_GlowOuter) * num6;
			}
			num10 = Mathf.Max(num10, num2 + num8 + num9);
			if (material.HasProperty(ShaderUtilities.ID_UnderlaySoftness) && shaderKeywords.Contains(ShaderUtilities.Keyword_Underlay))
			{
				if (material.HasProperty(ShaderUtilities.ID_ScaleRatio_C))
				{
					num7 = material.GetFloat(ShaderUtilities.ID_ScaleRatio_C);
				}
				float num11 = material.GetFloat(ShaderUtilities.ID_UnderlayOffsetX) * num7;
				float num12 = material.GetFloat(ShaderUtilities.ID_UnderlayOffsetY) * num7;
				float num13 = material.GetFloat(ShaderUtilities.ID_UnderlayDilate) * num7;
				float num14 = material.GetFloat(ShaderUtilities.ID_UnderlaySoftness) * num7;
				vector.x = Mathf.Max(vector.x, num2 + num13 + num14 - num11);
				vector.y = Mathf.Max(vector.y, num2 + num13 + num14 - num12);
				vector.z = Mathf.Max(vector.z, num2 + num13 + num14 + num11);
				vector.w = Mathf.Max(vector.w, num2 + num13 + num14 + num12);
			}
			vector.x = Mathf.Max(vector.x, num10);
			vector.y = Mathf.Max(vector.y, num10);
			vector.z = Mathf.Max(vector.z, num10);
			vector.w = Mathf.Max(vector.w, num10);
			vector.x += (float)num;
			vector.y += (float)num;
			vector.z += (float)num;
			vector.w += (float)num;
			vector.x = Mathf.Min(vector.x, 1f);
			vector.y = Mathf.Min(vector.y, 1f);
			vector.z = Mathf.Min(vector.z, 1f);
			vector.w = Mathf.Min(vector.w, 1f);
			zero.x = ((zero.x < vector.x) ? vector.x : zero.x);
			zero.y = ((zero.y < vector.y) ? vector.y : zero.y);
			zero.z = ((zero.z < vector.z) ? vector.z : zero.z);
			zero.w = ((zero.w < vector.w) ? vector.w : zero.w);
			float @float = material.GetFloat(ShaderUtilities.ID_GradientScale);
			vector *= @float;
			num10 = Mathf.Max(vector.x, vector.y);
			num10 = Mathf.Max(vector.z, num10);
			num10 = Mathf.Max(vector.w, num10);
			return num10 + 0.5f;
		}

		// Token: 0x06000490 RID: 1168 RVA: 0x0002EE2C File Offset: 0x0002D02C
		public static float GetPadding(Material[] materials, bool enableExtraPadding, bool isBold)
		{
			if (!ShaderUtilities.isInitialized)
			{
				ShaderUtilities.GetShaderPropertyIDs();
			}
			if (materials == null)
			{
				return 0f;
			}
			int num = enableExtraPadding ? 4 : 0;
			if (materials[0].HasProperty(ShaderUtilities.ID_Padding))
			{
				return (float)num + materials[0].GetFloat(ShaderUtilities.ID_Padding);
			}
			Vector4 vector = Vector4.zero;
			Vector4 zero = Vector4.zero;
			float num2 = 0f;
			float num3 = 0f;
			float num4 = 0f;
			float num5 = 0f;
			float num6 = 0f;
			float num7 = 0f;
			float num8 = 0f;
			float num9 = 0f;
			float num10;
			for (int i = 0; i < materials.Length; i++)
			{
				ShaderUtilities.UpdateShaderRatios(materials[i]);
				string[] shaderKeywords = materials[i].shaderKeywords;
				if (materials[i].HasProperty(ShaderUtilities.ID_ScaleRatio_A))
				{
					num5 = materials[i].GetFloat(ShaderUtilities.ID_ScaleRatio_A);
				}
				if (materials[i].HasProperty(ShaderUtilities.ID_FaceDilate))
				{
					num2 = materials[i].GetFloat(ShaderUtilities.ID_FaceDilate) * num5;
				}
				if (materials[i].HasProperty(ShaderUtilities.ID_OutlineSoftness))
				{
					num3 = materials[i].GetFloat(ShaderUtilities.ID_OutlineSoftness) * num5;
				}
				if (materials[i].HasProperty(ShaderUtilities.ID_OutlineWidth))
				{
					num4 = materials[i].GetFloat(ShaderUtilities.ID_OutlineWidth) * num5;
				}
				num10 = num4 + num3 + num2;
				if (materials[i].HasProperty(ShaderUtilities.ID_GlowOffset) && shaderKeywords.Contains(ShaderUtilities.Keyword_Glow))
				{
					if (materials[i].HasProperty(ShaderUtilities.ID_ScaleRatio_B))
					{
						num6 = materials[i].GetFloat(ShaderUtilities.ID_ScaleRatio_B);
					}
					num8 = materials[i].GetFloat(ShaderUtilities.ID_GlowOffset) * num6;
					num9 = materials[i].GetFloat(ShaderUtilities.ID_GlowOuter) * num6;
				}
				num10 = Mathf.Max(num10, num2 + num8 + num9);
				if (materials[i].HasProperty(ShaderUtilities.ID_UnderlaySoftness) && shaderKeywords.Contains(ShaderUtilities.Keyword_Underlay))
				{
					if (materials[i].HasProperty(ShaderUtilities.ID_ScaleRatio_C))
					{
						num7 = materials[i].GetFloat(ShaderUtilities.ID_ScaleRatio_C);
					}
					float num11 = materials[i].GetFloat(ShaderUtilities.ID_UnderlayOffsetX) * num7;
					float num12 = materials[i].GetFloat(ShaderUtilities.ID_UnderlayOffsetY) * num7;
					float num13 = materials[i].GetFloat(ShaderUtilities.ID_UnderlayDilate) * num7;
					float num14 = materials[i].GetFloat(ShaderUtilities.ID_UnderlaySoftness) * num7;
					vector.x = Mathf.Max(vector.x, num2 + num13 + num14 - num11);
					vector.y = Mathf.Max(vector.y, num2 + num13 + num14 - num12);
					vector.z = Mathf.Max(vector.z, num2 + num13 + num14 + num11);
					vector.w = Mathf.Max(vector.w, num2 + num13 + num14 + num12);
				}
				vector.x = Mathf.Max(vector.x, num10);
				vector.y = Mathf.Max(vector.y, num10);
				vector.z = Mathf.Max(vector.z, num10);
				vector.w = Mathf.Max(vector.w, num10);
				vector.x += (float)num;
				vector.y += (float)num;
				vector.z += (float)num;
				vector.w += (float)num;
				vector.x = Mathf.Min(vector.x, 1f);
				vector.y = Mathf.Min(vector.y, 1f);
				vector.z = Mathf.Min(vector.z, 1f);
				vector.w = Mathf.Min(vector.w, 1f);
				zero.x = ((zero.x < vector.x) ? vector.x : zero.x);
				zero.y = ((zero.y < vector.y) ? vector.y : zero.y);
				zero.z = ((zero.z < vector.z) ? vector.z : zero.z);
				zero.w = ((zero.w < vector.w) ? vector.w : zero.w);
			}
			float @float = materials[0].GetFloat(ShaderUtilities.ID_GradientScale);
			vector *= @float;
			num10 = Mathf.Max(vector.x, vector.y);
			num10 = Mathf.Max(vector.z, num10);
			num10 = Mathf.Max(vector.w, num10);
			return num10 + 0.25f;
		}

		// Token: 0x040003AD RID: 941
		public static int ID_MainTex;

		// Token: 0x040003AE RID: 942
		public static int ID_FaceTex;

		// Token: 0x040003AF RID: 943
		public static int ID_FaceColor;

		// Token: 0x040003B0 RID: 944
		public static int ID_FaceDilate;

		// Token: 0x040003B1 RID: 945
		public static int ID_Shininess;

		// Token: 0x040003B2 RID: 946
		public static int ID_UnderlayColor;

		// Token: 0x040003B3 RID: 947
		public static int ID_UnderlayOffsetX;

		// Token: 0x040003B4 RID: 948
		public static int ID_UnderlayOffsetY;

		// Token: 0x040003B5 RID: 949
		public static int ID_UnderlayDilate;

		// Token: 0x040003B6 RID: 950
		public static int ID_UnderlaySoftness;

		// Token: 0x040003B7 RID: 951
		public static int ID_WeightNormal;

		// Token: 0x040003B8 RID: 952
		public static int ID_WeightBold;

		// Token: 0x040003B9 RID: 953
		public static int ID_OutlineTex;

		// Token: 0x040003BA RID: 954
		public static int ID_OutlineWidth;

		// Token: 0x040003BB RID: 955
		public static int ID_OutlineSoftness;

		// Token: 0x040003BC RID: 956
		public static int ID_OutlineColor;

		// Token: 0x040003BD RID: 957
		public static int ID_Padding;

		// Token: 0x040003BE RID: 958
		public static int ID_GradientScale;

		// Token: 0x040003BF RID: 959
		public static int ID_ScaleX;

		// Token: 0x040003C0 RID: 960
		public static int ID_ScaleY;

		// Token: 0x040003C1 RID: 961
		public static int ID_PerspectiveFilter;

		// Token: 0x040003C2 RID: 962
		public static int ID_TextureWidth;

		// Token: 0x040003C3 RID: 963
		public static int ID_TextureHeight;

		// Token: 0x040003C4 RID: 964
		public static int ID_BevelAmount;

		// Token: 0x040003C5 RID: 965
		public static int ID_GlowColor;

		// Token: 0x040003C6 RID: 966
		public static int ID_GlowOffset;

		// Token: 0x040003C7 RID: 967
		public static int ID_GlowPower;

		// Token: 0x040003C8 RID: 968
		public static int ID_GlowOuter;

		// Token: 0x040003C9 RID: 969
		public static int ID_LightAngle;

		// Token: 0x040003CA RID: 970
		public static int ID_EnvMap;

		// Token: 0x040003CB RID: 971
		public static int ID_EnvMatrix;

		// Token: 0x040003CC RID: 972
		public static int ID_EnvMatrixRotation;

		// Token: 0x040003CD RID: 973
		public static int ID_MaskCoord;

		// Token: 0x040003CE RID: 974
		public static int ID_ClipRect;

		// Token: 0x040003CF RID: 975
		public static int ID_MaskSoftnessX;

		// Token: 0x040003D0 RID: 976
		public static int ID_MaskSoftnessY;

		// Token: 0x040003D1 RID: 977
		public static int ID_VertexOffsetX;

		// Token: 0x040003D2 RID: 978
		public static int ID_VertexOffsetY;

		// Token: 0x040003D3 RID: 979
		public static int ID_UseClipRect;

		// Token: 0x040003D4 RID: 980
		public static int ID_StencilID;

		// Token: 0x040003D5 RID: 981
		public static int ID_StencilOp;

		// Token: 0x040003D6 RID: 982
		public static int ID_StencilComp;

		// Token: 0x040003D7 RID: 983
		public static int ID_StencilReadMask;

		// Token: 0x040003D8 RID: 984
		public static int ID_StencilWriteMask;

		// Token: 0x040003D9 RID: 985
		public static int ID_ShaderFlags;

		// Token: 0x040003DA RID: 986
		public static int ID_ScaleRatio_A;

		// Token: 0x040003DB RID: 987
		public static int ID_ScaleRatio_B;

		// Token: 0x040003DC RID: 988
		public static int ID_ScaleRatio_C;

		// Token: 0x040003DD RID: 989
		public static string Keyword_Bevel = "BEVEL_ON";

		// Token: 0x040003DE RID: 990
		public static string Keyword_Glow = "GLOW_ON";

		// Token: 0x040003DF RID: 991
		public static string Keyword_Underlay = "UNDERLAY_ON";

		// Token: 0x040003E0 RID: 992
		public static string Keyword_Ratios = "RATIOS_OFF";

		// Token: 0x040003E1 RID: 993
		public static string Keyword_MASK_SOFT = "MASK_SOFT";

		// Token: 0x040003E2 RID: 994
		public static string Keyword_MASK_HARD = "MASK_HARD";

		// Token: 0x040003E3 RID: 995
		public static string Keyword_MASK_TEX = "MASK_TEX";

		// Token: 0x040003E4 RID: 996
		public static string Keyword_Outline = "OUTLINE_ON";

		// Token: 0x040003E5 RID: 997
		public static string ShaderTag_ZTestMode = "unity_GUIZTestMode";

		// Token: 0x040003E6 RID: 998
		public static string ShaderTag_CullMode = "_CullMode";

		// Token: 0x040003E7 RID: 999
		private static float m_clamp = 1f;

		// Token: 0x040003E8 RID: 1000
		public static bool isInitialized = false;
	}
}
