﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;

namespace TMPro
{
	// Token: 0x02000020 RID: 32
	internal class TMP_ObjectPool<T> where T : new()
	{
		// Token: 0x1700006D RID: 109
		// (get) Token: 0x06000244 RID: 580 RVA: 0x0001DE6E File Offset: 0x0001C06E
		// (set) Token: 0x06000245 RID: 581 RVA: 0x0001DE76 File Offset: 0x0001C076
		public int countAll
		{
			[CompilerGenerated]
			get
			{
				return this.<countAll>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<countAll>k__BackingField = value;
			}
		}

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x06000246 RID: 582 RVA: 0x0001DE7F File Offset: 0x0001C07F
		public int countActive
		{
			get
			{
				return this.countAll - this.countInactive;
			}
		}

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x06000247 RID: 583 RVA: 0x0001DE8E File Offset: 0x0001C08E
		public int countInactive
		{
			get
			{
				return this.m_Stack.Count;
			}
		}

		// Token: 0x06000248 RID: 584 RVA: 0x0001DE9B File Offset: 0x0001C09B
		public TMP_ObjectPool(UnityAction<T> actionOnGet, UnityAction<T> actionOnRelease)
		{
			this.m_ActionOnGet = actionOnGet;
			this.m_ActionOnRelease = actionOnRelease;
		}

		// Token: 0x06000249 RID: 585 RVA: 0x0001DEBC File Offset: 0x0001C0BC
		public T Get()
		{
			T t;
			if (this.m_Stack.Count == 0)
			{
				t = Activator.CreateInstance<T>();
				int countAll = this.countAll;
				this.countAll = countAll + 1;
			}
			else
			{
				t = this.m_Stack.Pop();
			}
			if (this.m_ActionOnGet != null)
			{
				this.m_ActionOnGet(t);
			}
			return t;
		}

		// Token: 0x0600024A RID: 586 RVA: 0x0001DF10 File Offset: 0x0001C110
		public void Release(T element)
		{
			if (this.m_Stack.Count > 0 && this.m_Stack.Peek() == element)
			{
				Debug.LogError("Internal error. Trying to destroy object that is already released to pool.");
			}
			if (this.m_ActionOnRelease != null)
			{
				this.m_ActionOnRelease(element);
			}
			this.m_Stack.Push(element);
		}

		// Token: 0x0400010E RID: 270
		private readonly Stack<T> m_Stack = new Stack<T>();

		// Token: 0x0400010F RID: 271
		private readonly UnityAction<T> m_ActionOnGet;

		// Token: 0x04000110 RID: 272
		private readonly UnityAction<T> m_ActionOnRelease;

		// Token: 0x04000111 RID: 273
		[CompilerGenerated]
		private int <countAll>k__BackingField;
	}
}
