﻿using System;
using UnityEngine;

namespace TMPro
{
	// Token: 0x02000044 RID: 68
	public static class TMPro_EventManager
	{
		// Token: 0x06000447 RID: 1095 RVA: 0x0002D653 File Offset: 0x0002B853
		public static void ON_PRE_RENDER_OBJECT_CHANGED()
		{
			TMPro_EventManager.OnPreRenderObject_Event.Call();
		}

		// Token: 0x06000448 RID: 1096 RVA: 0x0002D65F File Offset: 0x0002B85F
		public static void ON_MATERIAL_PROPERTY_CHANGED(bool isChanged, Material mat)
		{
			TMPro_EventManager.MATERIAL_PROPERTY_EVENT.Call(isChanged, mat);
		}

		// Token: 0x06000449 RID: 1097 RVA: 0x0002D66D File Offset: 0x0002B86D
		public static void ON_FONT_PROPERTY_CHANGED(bool isChanged, TMP_FontAsset font)
		{
			TMPro_EventManager.FONT_PROPERTY_EVENT.Call(isChanged, font);
		}

		// Token: 0x0600044A RID: 1098 RVA: 0x0002D67B File Offset: 0x0002B87B
		public static void ON_SPRITE_ASSET_PROPERTY_CHANGED(bool isChanged, Object obj)
		{
			TMPro_EventManager.SPRITE_ASSET_PROPERTY_EVENT.Call(isChanged, obj);
		}

		// Token: 0x0600044B RID: 1099 RVA: 0x0002D689 File Offset: 0x0002B889
		public static void ON_TEXTMESHPRO_PROPERTY_CHANGED(bool isChanged, TextMeshPro obj)
		{
			TMPro_EventManager.TEXTMESHPRO_PROPERTY_EVENT.Call(isChanged, obj);
		}

		// Token: 0x0600044C RID: 1100 RVA: 0x0002D697 File Offset: 0x0002B897
		public static void ON_DRAG_AND_DROP_MATERIAL_CHANGED(GameObject sender, Material currentMaterial, Material newMaterial)
		{
			TMPro_EventManager.DRAG_AND_DROP_MATERIAL_EVENT.Call(sender, currentMaterial, newMaterial);
		}

		// Token: 0x0600044D RID: 1101 RVA: 0x0002D6A6 File Offset: 0x0002B8A6
		public static void ON_TEXT_STYLE_PROPERTY_CHANGED(bool isChanged)
		{
			TMPro_EventManager.TEXT_STYLE_PROPERTY_EVENT.Call(isChanged);
		}

		// Token: 0x0600044E RID: 1102 RVA: 0x0002D6B3 File Offset: 0x0002B8B3
		public static void ON_COLOR_GRAIDENT_PROPERTY_CHANGED(TMP_ColorGradient gradient)
		{
			TMPro_EventManager.COLOR_GRADIENT_PROPERTY_EVENT.Call(gradient);
		}

		// Token: 0x0600044F RID: 1103 RVA: 0x0002D6C0 File Offset: 0x0002B8C0
		public static void ON_TEXT_CHANGED(Object obj)
		{
			TMPro_EventManager.TEXT_CHANGED_EVENT.Call(obj);
		}

		// Token: 0x06000450 RID: 1104 RVA: 0x0002D6CD File Offset: 0x0002B8CD
		public static void ON_TMP_SETTINGS_CHANGED()
		{
			TMPro_EventManager.TMP_SETTINGS_PROPERTY_EVENT.Call();
		}

		// Token: 0x06000451 RID: 1105 RVA: 0x0002D6D9 File Offset: 0x0002B8D9
		public static void ON_RESOURCES_LOADED()
		{
			TMPro_EventManager.RESOURCE_LOAD_EVENT.Call();
		}

		// Token: 0x06000452 RID: 1106 RVA: 0x0002D6E5 File Offset: 0x0002B8E5
		public static void ON_TEXTMESHPRO_UGUI_PROPERTY_CHANGED(bool isChanged, TextMeshProUGUI obj)
		{
			TMPro_EventManager.TEXTMESHPRO_UGUI_PROPERTY_EVENT.Call(isChanged, obj);
		}

		// Token: 0x06000453 RID: 1107 RVA: 0x0002D6F3 File Offset: 0x0002B8F3
		public static void ON_COMPUTE_DT_EVENT(object Sender, Compute_DT_EventArgs e)
		{
			TMPro_EventManager.COMPUTE_DT_EVENT.Call(Sender, e);
		}

		// Token: 0x06000454 RID: 1108 RVA: 0x0002D704 File Offset: 0x0002B904
		// Note: this type is marked as 'beforefieldinit'.
		static TMPro_EventManager()
		{
		}

		// Token: 0x040002D8 RID: 728
		public static readonly FastAction<object, Compute_DT_EventArgs> COMPUTE_DT_EVENT = new FastAction<object, Compute_DT_EventArgs>();

		// Token: 0x040002D9 RID: 729
		public static readonly FastAction<bool, Material> MATERIAL_PROPERTY_EVENT = new FastAction<bool, Material>();

		// Token: 0x040002DA RID: 730
		public static readonly FastAction<bool, TMP_FontAsset> FONT_PROPERTY_EVENT = new FastAction<bool, TMP_FontAsset>();

		// Token: 0x040002DB RID: 731
		public static readonly FastAction<bool, Object> SPRITE_ASSET_PROPERTY_EVENT = new FastAction<bool, Object>();

		// Token: 0x040002DC RID: 732
		public static readonly FastAction<bool, TextMeshPro> TEXTMESHPRO_PROPERTY_EVENT = new FastAction<bool, TextMeshPro>();

		// Token: 0x040002DD RID: 733
		public static readonly FastAction<GameObject, Material, Material> DRAG_AND_DROP_MATERIAL_EVENT = new FastAction<GameObject, Material, Material>();

		// Token: 0x040002DE RID: 734
		public static readonly FastAction<bool> TEXT_STYLE_PROPERTY_EVENT = new FastAction<bool>();

		// Token: 0x040002DF RID: 735
		public static readonly FastAction<TMP_ColorGradient> COLOR_GRADIENT_PROPERTY_EVENT = new FastAction<TMP_ColorGradient>();

		// Token: 0x040002E0 RID: 736
		public static readonly FastAction TMP_SETTINGS_PROPERTY_EVENT = new FastAction();

		// Token: 0x040002E1 RID: 737
		public static readonly FastAction RESOURCE_LOAD_EVENT = new FastAction();

		// Token: 0x040002E2 RID: 738
		public static readonly FastAction<bool, TextMeshProUGUI> TEXTMESHPRO_UGUI_PROPERTY_EVENT = new FastAction<bool, TextMeshProUGUI>();

		// Token: 0x040002E3 RID: 739
		public static readonly FastAction OnPreRenderObject_Event = new FastAction();

		// Token: 0x040002E4 RID: 740
		public static readonly FastAction<Object> TEXT_CHANGED_EVENT = new FastAction<Object>();
	}
}
