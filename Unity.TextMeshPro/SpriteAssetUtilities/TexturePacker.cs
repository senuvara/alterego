﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TMPro.SpriteAssetUtilities
{
	// Token: 0x0200005F RID: 95
	public class TexturePacker
	{
		// Token: 0x06000491 RID: 1169 RVA: 0x000159ED File Offset: 0x00013BED
		public TexturePacker()
		{
		}

		// Token: 0x0200008A RID: 138
		[Serializable]
		public struct SpriteFrame
		{
			// Token: 0x060004F0 RID: 1264 RVA: 0x0002FD24 File Offset: 0x0002DF24
			public override string ToString()
			{
				return string.Concat(new string[]
				{
					"x: ",
					this.x.ToString("f2"),
					" y: ",
					this.y.ToString("f2"),
					" h: ",
					this.h.ToString("f2"),
					" w: ",
					this.w.ToString("f2")
				});
			}

			// Token: 0x04000473 RID: 1139
			public float x;

			// Token: 0x04000474 RID: 1140
			public float y;

			// Token: 0x04000475 RID: 1141
			public float w;

			// Token: 0x04000476 RID: 1142
			public float h;
		}

		// Token: 0x0200008B RID: 139
		[Serializable]
		public struct SpriteSize
		{
			// Token: 0x060004F1 RID: 1265 RVA: 0x0002FDA8 File Offset: 0x0002DFA8
			public override string ToString()
			{
				return "w: " + this.w.ToString("f2") + " h: " + this.h.ToString("f2");
			}

			// Token: 0x04000477 RID: 1143
			public float w;

			// Token: 0x04000478 RID: 1144
			public float h;
		}

		// Token: 0x0200008C RID: 140
		[Serializable]
		public struct SpriteData
		{
			// Token: 0x04000479 RID: 1145
			public string filename;

			// Token: 0x0400047A RID: 1146
			public TexturePacker.SpriteFrame frame;

			// Token: 0x0400047B RID: 1147
			public bool rotated;

			// Token: 0x0400047C RID: 1148
			public bool trimmed;

			// Token: 0x0400047D RID: 1149
			public TexturePacker.SpriteFrame spriteSourceSize;

			// Token: 0x0400047E RID: 1150
			public TexturePacker.SpriteSize sourceSize;

			// Token: 0x0400047F RID: 1151
			public Vector2 pivot;
		}

		// Token: 0x0200008D RID: 141
		[Serializable]
		public class SpriteDataObject
		{
			// Token: 0x060004F2 RID: 1266 RVA: 0x000159ED File Offset: 0x00013BED
			public SpriteDataObject()
			{
			}

			// Token: 0x04000480 RID: 1152
			public List<TexturePacker.SpriteData> frames;
		}
	}
}
