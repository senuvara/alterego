﻿using System;

namespace TMPro
{
	// Token: 0x02000037 RID: 55
	public enum TagUnits
	{
		// Token: 0x040001C3 RID: 451
		Pixels,
		// Token: 0x040001C4 RID: 452
		FontUnits,
		// Token: 0x040001C5 RID: 453
		Percentage
	}
}
