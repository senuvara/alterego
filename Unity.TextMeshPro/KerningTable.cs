﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace TMPro
{
	// Token: 0x0200004E RID: 78
	[Serializable]
	public class KerningTable
	{
		// Token: 0x06000475 RID: 1141 RVA: 0x0002DE27 File Offset: 0x0002C027
		public KerningTable()
		{
			this.kerningPairs = new List<KerningPair>();
		}

		// Token: 0x06000476 RID: 1142 RVA: 0x0002DE3C File Offset: 0x0002C03C
		public void AddKerningPair()
		{
			if (this.kerningPairs.Count == 0)
			{
				this.kerningPairs.Add(new KerningPair(0U, 0U, 0f));
				return;
			}
			uint firstGlyph = this.kerningPairs.Last<KerningPair>().firstGlyph;
			uint secondGlyph = this.kerningPairs.Last<KerningPair>().secondGlyph;
			float xOffset = this.kerningPairs.Last<KerningPair>().xOffset;
			this.kerningPairs.Add(new KerningPair(firstGlyph, secondGlyph, xOffset));
		}

		// Token: 0x06000477 RID: 1143 RVA: 0x0002DEB4 File Offset: 0x0002C0B4
		public int AddKerningPair(uint first, uint second, float offset)
		{
			if (this.kerningPairs.FindIndex((KerningPair item) => item.firstGlyph == first && item.secondGlyph == second) == -1)
			{
				this.kerningPairs.Add(new KerningPair(first, second, offset));
				return 0;
			}
			return -1;
		}

		// Token: 0x06000478 RID: 1144 RVA: 0x0002DF10 File Offset: 0x0002C110
		public int AddGlyphPairAdjustmentRecord(uint first, GlyphValueRecord firstAdjustments, uint second, GlyphValueRecord secondAdjustments)
		{
			if (this.kerningPairs.FindIndex((KerningPair item) => item.firstGlyph == first && item.secondGlyph == second) == -1)
			{
				this.kerningPairs.Add(new KerningPair(first, firstAdjustments, second, secondAdjustments));
				return 0;
			}
			return -1;
		}

		// Token: 0x06000479 RID: 1145 RVA: 0x0002DF70 File Offset: 0x0002C170
		public void RemoveKerningPair(int left, int right)
		{
			int num = this.kerningPairs.FindIndex((KerningPair item) => (ulong)item.firstGlyph == (ulong)((long)left) && (ulong)item.secondGlyph == (ulong)((long)right));
			if (num != -1)
			{
				this.kerningPairs.RemoveAt(num);
			}
		}

		// Token: 0x0600047A RID: 1146 RVA: 0x0002DFB9 File Offset: 0x0002C1B9
		public void RemoveKerningPair(int index)
		{
			this.kerningPairs.RemoveAt(index);
		}

		// Token: 0x0600047B RID: 1147 RVA: 0x0002DFC8 File Offset: 0x0002C1C8
		public void SortKerningPairs()
		{
			if (this.kerningPairs.Count > 0)
			{
				this.kerningPairs = (from s in this.kerningPairs
				orderby s.firstGlyph, s.secondGlyph
				select s).ToList<KerningPair>();
			}
		}

		// Token: 0x04000321 RID: 801
		public List<KerningPair> kerningPairs;

		// Token: 0x02000086 RID: 134
		[CompilerGenerated]
		private sealed class <>c__DisplayClass3_0
		{
			// Token: 0x060004E6 RID: 1254 RVA: 0x000159ED File Offset: 0x00013BED
			public <>c__DisplayClass3_0()
			{
			}

			// Token: 0x060004E7 RID: 1255 RVA: 0x0002FCA1 File Offset: 0x0002DEA1
			internal bool <AddKerningPair>b__0(KerningPair item)
			{
				return item.firstGlyph == this.first && item.secondGlyph == this.second;
			}

			// Token: 0x0400046A RID: 1130
			public uint first;

			// Token: 0x0400046B RID: 1131
			public uint second;
		}

		// Token: 0x02000087 RID: 135
		[CompilerGenerated]
		private sealed class <>c__DisplayClass4_0
		{
			// Token: 0x060004E8 RID: 1256 RVA: 0x000159ED File Offset: 0x00013BED
			public <>c__DisplayClass4_0()
			{
			}

			// Token: 0x060004E9 RID: 1257 RVA: 0x0002FCC1 File Offset: 0x0002DEC1
			internal bool <AddGlyphPairAdjustmentRecord>b__0(KerningPair item)
			{
				return item.firstGlyph == this.first && item.secondGlyph == this.second;
			}

			// Token: 0x0400046C RID: 1132
			public uint first;

			// Token: 0x0400046D RID: 1133
			public uint second;
		}

		// Token: 0x02000088 RID: 136
		[CompilerGenerated]
		private sealed class <>c__DisplayClass5_0
		{
			// Token: 0x060004EA RID: 1258 RVA: 0x000159ED File Offset: 0x00013BED
			public <>c__DisplayClass5_0()
			{
			}

			// Token: 0x060004EB RID: 1259 RVA: 0x0002FCE1 File Offset: 0x0002DEE1
			internal bool <RemoveKerningPair>b__0(KerningPair item)
			{
				return (ulong)item.firstGlyph == (ulong)((long)this.left) && (ulong)item.secondGlyph == (ulong)((long)this.right);
			}

			// Token: 0x0400046E RID: 1134
			public int left;

			// Token: 0x0400046F RID: 1135
			public int right;
		}

		// Token: 0x02000089 RID: 137
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x060004EC RID: 1260 RVA: 0x0002FD05 File Offset: 0x0002DF05
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x060004ED RID: 1261 RVA: 0x000159ED File Offset: 0x00013BED
			public <>c()
			{
			}

			// Token: 0x060004EE RID: 1262 RVA: 0x0002FD11 File Offset: 0x0002DF11
			internal uint <SortKerningPairs>b__7_0(KerningPair s)
			{
				return s.firstGlyph;
			}

			// Token: 0x060004EF RID: 1263 RVA: 0x0002FD19 File Offset: 0x0002DF19
			internal uint <SortKerningPairs>b__7_1(KerningPair s)
			{
				return s.secondGlyph;
			}

			// Token: 0x04000470 RID: 1136
			public static readonly KerningTable.<>c <>9 = new KerningTable.<>c();

			// Token: 0x04000471 RID: 1137
			public static Func<KerningPair, uint> <>9__7_0;

			// Token: 0x04000472 RID: 1138
			public static Func<KerningPair, uint> <>9__7_1;
		}
	}
}
