﻿using System;

namespace TMPro
{
	// Token: 0x02000030 RID: 48
	public enum TMP_TextElementType
	{
		// Token: 0x04000196 RID: 406
		Character,
		// Token: 0x04000197 RID: 407
		Sprite
	}
}
