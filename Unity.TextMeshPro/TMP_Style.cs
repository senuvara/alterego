﻿using System;
using UnityEngine;

namespace TMPro
{
	// Token: 0x02000027 RID: 39
	[Serializable]
	public class TMP_Style
	{
		// Token: 0x1700008C RID: 140
		// (get) Token: 0x0600028B RID: 651 RVA: 0x0001E769 File Offset: 0x0001C969
		// (set) Token: 0x0600028C RID: 652 RVA: 0x0001E771 File Offset: 0x0001C971
		public string name
		{
			get
			{
				return this.m_Name;
			}
			set
			{
				if (value != this.m_Name)
				{
					this.m_Name = value;
				}
			}
		}

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x0600028D RID: 653 RVA: 0x0001E788 File Offset: 0x0001C988
		// (set) Token: 0x0600028E RID: 654 RVA: 0x0001E790 File Offset: 0x0001C990
		public int hashCode
		{
			get
			{
				return this.m_HashCode;
			}
			set
			{
				if (value != this.m_HashCode)
				{
					this.m_HashCode = value;
				}
			}
		}

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x0600028F RID: 655 RVA: 0x0001E7A2 File Offset: 0x0001C9A2
		public string styleOpeningDefinition
		{
			get
			{
				return this.m_OpeningDefinition;
			}
		}

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x06000290 RID: 656 RVA: 0x0001E7AA File Offset: 0x0001C9AA
		public string styleClosingDefinition
		{
			get
			{
				return this.m_ClosingDefinition;
			}
		}

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x06000291 RID: 657 RVA: 0x0001E7B2 File Offset: 0x0001C9B2
		public int[] styleOpeningTagArray
		{
			get
			{
				return this.m_OpeningTagArray;
			}
		}

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x06000292 RID: 658 RVA: 0x0001E7BA File Offset: 0x0001C9BA
		public int[] styleClosingTagArray
		{
			get
			{
				return this.m_ClosingTagArray;
			}
		}

		// Token: 0x06000293 RID: 659 RVA: 0x0001E7C4 File Offset: 0x0001C9C4
		public void RefreshStyle()
		{
			this.m_HashCode = TMP_TextUtilities.GetSimpleHashCode(this.m_Name);
			this.m_OpeningTagArray = new int[this.m_OpeningDefinition.Length];
			for (int i = 0; i < this.m_OpeningDefinition.Length; i++)
			{
				this.m_OpeningTagArray[i] = (int)this.m_OpeningDefinition[i];
			}
			this.m_ClosingTagArray = new int[this.m_ClosingDefinition.Length];
			for (int j = 0; j < this.m_ClosingDefinition.Length; j++)
			{
				this.m_ClosingTagArray[j] = (int)this.m_ClosingDefinition[j];
			}
		}

		// Token: 0x06000294 RID: 660 RVA: 0x000159ED File Offset: 0x00013BED
		public TMP_Style()
		{
		}

		// Token: 0x0400013B RID: 315
		[SerializeField]
		private string m_Name;

		// Token: 0x0400013C RID: 316
		[SerializeField]
		private int m_HashCode;

		// Token: 0x0400013D RID: 317
		[SerializeField]
		private string m_OpeningDefinition;

		// Token: 0x0400013E RID: 318
		[SerializeField]
		private string m_ClosingDefinition;

		// Token: 0x0400013F RID: 319
		[SerializeField]
		private int[] m_OpeningTagArray;

		// Token: 0x04000140 RID: 320
		[SerializeField]
		private int[] m_ClosingTagArray;
	}
}
