﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace TMPro
{
	// Token: 0x0200004D RID: 77
	[Serializable]
	public class KerningPair
	{
		// Token: 0x17000103 RID: 259
		// (get) Token: 0x0600046B RID: 1131 RVA: 0x0002DD72 File Offset: 0x0002BF72
		// (set) Token: 0x0600046C RID: 1132 RVA: 0x0002DD7A File Offset: 0x0002BF7A
		public uint firstGlyph
		{
			get
			{
				return this.m_FirstGlyph;
			}
			set
			{
				this.m_FirstGlyph = value;
			}
		}

		// Token: 0x17000104 RID: 260
		// (get) Token: 0x0600046D RID: 1133 RVA: 0x0002DD83 File Offset: 0x0002BF83
		public GlyphValueRecord firstGlyphAdjustments
		{
			get
			{
				return this.m_FirstGlyphAdjustments;
			}
		}

		// Token: 0x17000105 RID: 261
		// (get) Token: 0x0600046E RID: 1134 RVA: 0x0002DD8B File Offset: 0x0002BF8B
		// (set) Token: 0x0600046F RID: 1135 RVA: 0x0002DD93 File Offset: 0x0002BF93
		public uint secondGlyph
		{
			get
			{
				return this.m_SecondGlyph;
			}
			set
			{
				this.m_SecondGlyph = value;
			}
		}

		// Token: 0x17000106 RID: 262
		// (get) Token: 0x06000470 RID: 1136 RVA: 0x0002DD9C File Offset: 0x0002BF9C
		public GlyphValueRecord secondGlyphAdjustments
		{
			get
			{
				return this.m_SecondGlyphAdjustments;
			}
		}

		// Token: 0x06000471 RID: 1137 RVA: 0x0002DDA4 File Offset: 0x0002BFA4
		public KerningPair()
		{
			this.m_FirstGlyph = 0U;
			this.m_FirstGlyphAdjustments = default(GlyphValueRecord);
			this.m_SecondGlyph = 0U;
			this.m_SecondGlyphAdjustments = default(GlyphValueRecord);
		}

		// Token: 0x06000472 RID: 1138 RVA: 0x0002DDD2 File Offset: 0x0002BFD2
		public KerningPair(uint left, uint right, float offset)
		{
			this.firstGlyph = left;
			this.m_SecondGlyph = right;
			this.xOffset = offset;
		}

		// Token: 0x06000473 RID: 1139 RVA: 0x0002DDEF File Offset: 0x0002BFEF
		public KerningPair(uint firstGlyph, GlyphValueRecord firstGlyphAdjustments, uint secondGlyph, GlyphValueRecord secondGlyphAdjustments)
		{
			this.m_FirstGlyph = firstGlyph;
			this.m_FirstGlyphAdjustments = firstGlyphAdjustments;
			this.m_SecondGlyph = secondGlyph;
			this.m_SecondGlyphAdjustments = secondGlyphAdjustments;
		}

		// Token: 0x06000474 RID: 1140 RVA: 0x0002DE14 File Offset: 0x0002C014
		internal void ConvertLegacyKerningData()
		{
			this.m_FirstGlyphAdjustments.xAdvance = this.xOffset;
		}

		// Token: 0x0400031C RID: 796
		[FormerlySerializedAs("AscII_Left")]
		[SerializeField]
		private uint m_FirstGlyph;

		// Token: 0x0400031D RID: 797
		[SerializeField]
		private GlyphValueRecord m_FirstGlyphAdjustments;

		// Token: 0x0400031E RID: 798
		[FormerlySerializedAs("AscII_Right")]
		[SerializeField]
		private uint m_SecondGlyph;

		// Token: 0x0400031F RID: 799
		[SerializeField]
		private GlyphValueRecord m_SecondGlyphAdjustments;

		// Token: 0x04000320 RID: 800
		[FormerlySerializedAs("XadvanceOffset")]
		public float xOffset;
	}
}
