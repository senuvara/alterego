﻿using System;

namespace TMPro
{
	// Token: 0x02000057 RID: 87
	public struct TMP_SpriteInfo
	{
		// Token: 0x04000367 RID: 871
		public int spriteIndex;

		// Token: 0x04000368 RID: 872
		public int characterIndex;

		// Token: 0x04000369 RID: 873
		public int vertexIndex;
	}
}
