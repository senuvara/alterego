﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace TMPro
{
	// Token: 0x02000025 RID: 37
	[DisallowMultipleComponent]
	public class TMP_SpriteAnimator : MonoBehaviour
	{
		// Token: 0x06000276 RID: 630 RVA: 0x0001E285 File Offset: 0x0001C485
		private void Awake()
		{
			this.m_TextComponent = base.GetComponent<TMP_Text>();
		}

		// Token: 0x06000277 RID: 631 RVA: 0x0000297D File Offset: 0x00000B7D
		private void OnEnable()
		{
		}

		// Token: 0x06000278 RID: 632 RVA: 0x0000297D File Offset: 0x00000B7D
		private void OnDisable()
		{
		}

		// Token: 0x06000279 RID: 633 RVA: 0x0001E293 File Offset: 0x0001C493
		public void StopAllAnimations()
		{
			base.StopAllCoroutines();
			this.m_animations.Clear();
		}

		// Token: 0x0600027A RID: 634 RVA: 0x0001E2A8 File Offset: 0x0001C4A8
		public void DoSpriteAnimation(int currentCharacter, TMP_SpriteAsset spriteAsset, int start, int end, int framerate)
		{
			bool flag = false;
			if (!this.m_animations.TryGetValue(currentCharacter, out flag))
			{
				base.StartCoroutine(this.DoSpriteAnimationInternal(currentCharacter, spriteAsset, start, end, framerate));
				this.m_animations.Add(currentCharacter, true);
			}
		}

		// Token: 0x0600027B RID: 635 RVA: 0x0001E2E8 File Offset: 0x0001C4E8
		private IEnumerator DoSpriteAnimationInternal(int currentCharacter, TMP_SpriteAsset spriteAsset, int start, int end, int framerate)
		{
			if (this.m_TextComponent == null)
			{
				yield break;
			}
			yield return null;
			int currentFrame = start;
			if (end > spriteAsset.spriteInfoList.Count)
			{
				end = spriteAsset.spriteInfoList.Count - 1;
			}
			TMP_CharacterInfo charInfo = this.m_TextComponent.textInfo.characterInfo[currentCharacter];
			int materialIndex = charInfo.materialReferenceIndex;
			int vertexIndex = charInfo.vertexIndex;
			TMP_MeshInfo meshInfo = this.m_TextComponent.textInfo.meshInfo[materialIndex];
			float elapsedTime = 0f;
			float targetTime = 1f / (float)Mathf.Abs(framerate);
			for (;;)
			{
				if (elapsedTime > targetTime)
				{
					elapsedTime = 0f;
					TMP_Sprite tmp_Sprite = spriteAsset.spriteInfoList[currentFrame];
					Vector3[] vertices = meshInfo.vertices;
					Vector2 vector = new Vector2(charInfo.origin, charInfo.baseLine);
					float num = charInfo.fontAsset.fontInfo.Ascender / tmp_Sprite.height * tmp_Sprite.scale * charInfo.scale;
					Vector3 vector2 = new Vector3(vector.x + tmp_Sprite.xOffset * num, vector.y + (tmp_Sprite.yOffset - tmp_Sprite.height) * num);
					Vector3 vector3 = new Vector3(vector2.x, vector.y + tmp_Sprite.yOffset * num);
					Vector3 vector4 = new Vector3(vector.x + (tmp_Sprite.xOffset + tmp_Sprite.width) * num, vector3.y);
					Vector3 vector5 = new Vector3(vector4.x, vector2.y);
					vertices[vertexIndex] = vector2;
					vertices[vertexIndex + 1] = vector3;
					vertices[vertexIndex + 2] = vector4;
					vertices[vertexIndex + 3] = vector5;
					Vector2[] uvs = meshInfo.uvs0;
					Vector2 vector6 = new Vector2(tmp_Sprite.x / (float)spriteAsset.spriteSheet.width, tmp_Sprite.y / (float)spriteAsset.spriteSheet.height);
					Vector2 vector7 = new Vector2(vector6.x, (tmp_Sprite.y + tmp_Sprite.height) / (float)spriteAsset.spriteSheet.height);
					Vector2 vector8 = new Vector2((tmp_Sprite.x + tmp_Sprite.width) / (float)spriteAsset.spriteSheet.width, vector7.y);
					Vector2 vector9 = new Vector2(vector8.x, vector6.y);
					uvs[vertexIndex] = vector6;
					uvs[vertexIndex + 1] = vector7;
					uvs[vertexIndex + 2] = vector8;
					uvs[vertexIndex + 3] = vector9;
					meshInfo.mesh.vertices = vertices;
					meshInfo.mesh.uv = uvs;
					this.m_TextComponent.UpdateGeometry(meshInfo.mesh, materialIndex);
					if (framerate > 0)
					{
						if (currentFrame < end)
						{
							currentFrame++;
						}
						else
						{
							currentFrame = start;
						}
					}
					else if (currentFrame > start)
					{
						currentFrame--;
					}
					else
					{
						currentFrame = end;
					}
				}
				elapsedTime += Time.deltaTime;
				yield return null;
			}
			yield break;
		}

		// Token: 0x0600027C RID: 636 RVA: 0x0001E31C File Offset: 0x0001C51C
		public TMP_SpriteAnimator()
		{
		}

		// Token: 0x04000132 RID: 306
		private Dictionary<int, bool> m_animations = new Dictionary<int, bool>(16);

		// Token: 0x04000133 RID: 307
		private TMP_Text m_TextComponent;

		// Token: 0x02000083 RID: 131
		[CompilerGenerated]
		private sealed class <DoSpriteAnimationInternal>d__7 : IEnumerator<object>, IEnumerator, IDisposable
		{
			// Token: 0x060004DF RID: 1247 RVA: 0x0002F845 File Offset: 0x0002DA45
			[DebuggerHidden]
			public <DoSpriteAnimationInternal>d__7(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x060004E0 RID: 1248 RVA: 0x0000297D File Offset: 0x00000B7D
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x060004E1 RID: 1249 RVA: 0x0002F854 File Offset: 0x0002DA54
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				TMP_SpriteAnimator tmp_SpriteAnimator = this;
				switch (num)
				{
				case 0:
					this.<>1__state = -1;
					if (tmp_SpriteAnimator.m_TextComponent == null)
					{
						return false;
					}
					this.<>2__current = null;
					this.<>1__state = 1;
					return true;
				case 1:
					this.<>1__state = -1;
					currentFrame = start;
					if (end > spriteAsset.spriteInfoList.Count)
					{
						end = spriteAsset.spriteInfoList.Count - 1;
					}
					charInfo = tmp_SpriteAnimator.m_TextComponent.textInfo.characterInfo[currentCharacter];
					materialIndex = charInfo.materialReferenceIndex;
					vertexIndex = charInfo.vertexIndex;
					meshInfo = tmp_SpriteAnimator.m_TextComponent.textInfo.meshInfo[materialIndex];
					elapsedTime = 0f;
					targetTime = 1f / (float)Mathf.Abs(framerate);
					break;
				case 2:
					this.<>1__state = -1;
					break;
				default:
					return false;
				}
				if (elapsedTime > targetTime)
				{
					elapsedTime = 0f;
					TMP_Sprite tmp_Sprite = spriteAsset.spriteInfoList[currentFrame];
					Vector3[] vertices = meshInfo.vertices;
					Vector2 vector = new Vector2(charInfo.origin, charInfo.baseLine);
					float num2 = charInfo.fontAsset.fontInfo.Ascender / tmp_Sprite.height * tmp_Sprite.scale * charInfo.scale;
					Vector3 vector2 = new Vector3(vector.x + tmp_Sprite.xOffset * num2, vector.y + (tmp_Sprite.yOffset - tmp_Sprite.height) * num2);
					Vector3 vector3 = new Vector3(vector2.x, vector.y + tmp_Sprite.yOffset * num2);
					Vector3 vector4 = new Vector3(vector.x + (tmp_Sprite.xOffset + tmp_Sprite.width) * num2, vector3.y);
					Vector3 vector5 = new Vector3(vector4.x, vector2.y);
					vertices[vertexIndex] = vector2;
					vertices[vertexIndex + 1] = vector3;
					vertices[vertexIndex + 2] = vector4;
					vertices[vertexIndex + 3] = vector5;
					Vector2[] uvs = meshInfo.uvs0;
					Vector2 vector6 = new Vector2(tmp_Sprite.x / (float)spriteAsset.spriteSheet.width, tmp_Sprite.y / (float)spriteAsset.spriteSheet.height);
					Vector2 vector7 = new Vector2(vector6.x, (tmp_Sprite.y + tmp_Sprite.height) / (float)spriteAsset.spriteSheet.height);
					Vector2 vector8 = new Vector2((tmp_Sprite.x + tmp_Sprite.width) / (float)spriteAsset.spriteSheet.width, vector7.y);
					Vector2 vector9 = new Vector2(vector8.x, vector6.y);
					uvs[vertexIndex] = vector6;
					uvs[vertexIndex + 1] = vector7;
					uvs[vertexIndex + 2] = vector8;
					uvs[vertexIndex + 3] = vector9;
					meshInfo.mesh.vertices = vertices;
					meshInfo.mesh.uv = uvs;
					tmp_SpriteAnimator.m_TextComponent.UpdateGeometry(meshInfo.mesh, materialIndex);
					if (framerate > 0)
					{
						if (currentFrame < end)
						{
							currentFrame++;
						}
						else
						{
							currentFrame = start;
						}
					}
					else if (currentFrame > start)
					{
						currentFrame--;
					}
					else
					{
						currentFrame = end;
					}
				}
				elapsedTime += Time.deltaTime;
				this.<>2__current = null;
				this.<>1__state = 2;
				return true;
			}

			// Token: 0x17000116 RID: 278
			// (get) Token: 0x060004E2 RID: 1250 RVA: 0x0002FC89 File Offset: 0x0002DE89
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060004E3 RID: 1251 RVA: 0x0002F3C7 File Offset: 0x0002D5C7
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000117 RID: 279
			// (get) Token: 0x060004E4 RID: 1252 RVA: 0x0002FC89 File Offset: 0x0002DE89
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x04000454 RID: 1108
			private int <>1__state;

			// Token: 0x04000455 RID: 1109
			private object <>2__current;

			// Token: 0x04000456 RID: 1110
			public TMP_SpriteAnimator <>4__this;

			// Token: 0x04000457 RID: 1111
			public int start;

			// Token: 0x04000458 RID: 1112
			public int end;

			// Token: 0x04000459 RID: 1113
			public TMP_SpriteAsset spriteAsset;

			// Token: 0x0400045A RID: 1114
			public int currentCharacter;

			// Token: 0x0400045B RID: 1115
			public int framerate;

			// Token: 0x0400045C RID: 1116
			private int <currentFrame>5__2;

			// Token: 0x0400045D RID: 1117
			private TMP_CharacterInfo <charInfo>5__3;

			// Token: 0x0400045E RID: 1118
			private int <materialIndex>5__4;

			// Token: 0x0400045F RID: 1119
			private int <vertexIndex>5__5;

			// Token: 0x04000460 RID: 1120
			private TMP_MeshInfo <meshInfo>5__6;

			// Token: 0x04000461 RID: 1121
			private float <elapsedTime>5__7;

			// Token: 0x04000462 RID: 1122
			private float <targetTime>5__8;
		}
	}
}
