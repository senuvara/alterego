﻿using System;

namespace TMPro
{
	// Token: 0x0200002D RID: 45
	public enum _HorizontalAlignmentOptions
	{
		// Token: 0x04000185 RID: 389
		Left = 1,
		// Token: 0x04000186 RID: 390
		Center,
		// Token: 0x04000187 RID: 391
		Right = 4,
		// Token: 0x04000188 RID: 392
		Justified = 8,
		// Token: 0x04000189 RID: 393
		Flush = 16,
		// Token: 0x0400018A RID: 394
		Geometry = 32
	}
}
