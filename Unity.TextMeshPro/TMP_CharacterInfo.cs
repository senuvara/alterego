﻿using System;
using UnityEngine;

namespace TMPro
{
	// Token: 0x02000051 RID: 81
	public struct TMP_CharacterInfo
	{
		// Token: 0x0400032B RID: 811
		public char character;

		// Token: 0x0400032C RID: 812
		public int index;

		// Token: 0x0400032D RID: 813
		public TMP_TextElementType elementType;

		// Token: 0x0400032E RID: 814
		public TMP_TextElement textElement;

		// Token: 0x0400032F RID: 815
		public TMP_FontAsset fontAsset;

		// Token: 0x04000330 RID: 816
		public TMP_SpriteAsset spriteAsset;

		// Token: 0x04000331 RID: 817
		public int spriteIndex;

		// Token: 0x04000332 RID: 818
		public Material material;

		// Token: 0x04000333 RID: 819
		public int materialReferenceIndex;

		// Token: 0x04000334 RID: 820
		public bool isUsingAlternateTypeface;

		// Token: 0x04000335 RID: 821
		public float pointSize;

		// Token: 0x04000336 RID: 822
		public int lineNumber;

		// Token: 0x04000337 RID: 823
		public int pageNumber;

		// Token: 0x04000338 RID: 824
		public int vertexIndex;

		// Token: 0x04000339 RID: 825
		public TMP_Vertex vertex_TL;

		// Token: 0x0400033A RID: 826
		public TMP_Vertex vertex_BL;

		// Token: 0x0400033B RID: 827
		public TMP_Vertex vertex_TR;

		// Token: 0x0400033C RID: 828
		public TMP_Vertex vertex_BR;

		// Token: 0x0400033D RID: 829
		public Vector3 topLeft;

		// Token: 0x0400033E RID: 830
		public Vector3 bottomLeft;

		// Token: 0x0400033F RID: 831
		public Vector3 topRight;

		// Token: 0x04000340 RID: 832
		public Vector3 bottomRight;

		// Token: 0x04000341 RID: 833
		public float origin;

		// Token: 0x04000342 RID: 834
		public float ascender;

		// Token: 0x04000343 RID: 835
		public float baseLine;

		// Token: 0x04000344 RID: 836
		public float descender;

		// Token: 0x04000345 RID: 837
		public float xAdvance;

		// Token: 0x04000346 RID: 838
		public float aspectRatio;

		// Token: 0x04000347 RID: 839
		public float scale;

		// Token: 0x04000348 RID: 840
		public Color32 color;

		// Token: 0x04000349 RID: 841
		public Color32 underlineColor;

		// Token: 0x0400034A RID: 842
		public Color32 strikethroughColor;

		// Token: 0x0400034B RID: 843
		public Color32 highlightColor;

		// Token: 0x0400034C RID: 844
		public FontStyles style;

		// Token: 0x0400034D RID: 845
		public bool isVisible;
	}
}
