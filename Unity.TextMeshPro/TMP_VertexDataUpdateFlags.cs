﻿using System;

namespace TMPro
{
	// Token: 0x02000050 RID: 80
	public enum TMP_VertexDataUpdateFlags
	{
		// Token: 0x04000324 RID: 804
		None,
		// Token: 0x04000325 RID: 805
		Vertices,
		// Token: 0x04000326 RID: 806
		Uv0,
		// Token: 0x04000327 RID: 807
		Uv2 = 4,
		// Token: 0x04000328 RID: 808
		Uv4 = 8,
		// Token: 0x04000329 RID: 809
		Colors32 = 16,
		// Token: 0x0400032A RID: 810
		All = 255
	}
}
