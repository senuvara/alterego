﻿using System;

namespace TMPro
{
	// Token: 0x0200004B RID: 75
	public struct KerningPairKey
	{
		// Token: 0x06000469 RID: 1129 RVA: 0x0002DCF6 File Offset: 0x0002BEF6
		public KerningPairKey(uint ascii_left, uint ascii_right)
		{
			this.ascii_Left = ascii_left;
			this.ascii_Right = ascii_right;
			this.key = (ascii_right << 16) + ascii_left;
		}

		// Token: 0x04000315 RID: 789
		public uint ascii_Left;

		// Token: 0x04000316 RID: 790
		public uint ascii_Right;

		// Token: 0x04000317 RID: 791
		public uint key;
	}
}
