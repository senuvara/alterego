﻿using System;

namespace TMPro
{
	// Token: 0x0200003C RID: 60
	public enum CaretPosition
	{
		// Token: 0x040002B4 RID: 692
		None,
		// Token: 0x040002B5 RID: 693
		Left,
		// Token: 0x040002B6 RID: 694
		Right
	}
}
