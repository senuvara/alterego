﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace TMPro
{
	// Token: 0x02000039 RID: 57
	public abstract class TMP_Text : MaskableGraphic
	{
		// Token: 0x170000AB RID: 171
		// (get) Token: 0x060002F4 RID: 756 RVA: 0x0001F56F File Offset: 0x0001D76F
		// (set) Token: 0x060002F5 RID: 757 RVA: 0x0001F578 File Offset: 0x0001D778
		public string text
		{
			get
			{
				return this.m_text;
			}
			set
			{
				if (this.m_text == value)
				{
					return;
				}
				this.old_text = value;
				this.m_text = value;
				this.m_inputSource = TMP_Text.TextInputSources.String;
				this.m_havePropertiesChanged = true;
				this.m_isCalculateSizeRequired = true;
				this.m_isInputParsingRequired = true;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000AC RID: 172
		// (get) Token: 0x060002F6 RID: 758 RVA: 0x0001F5CC File Offset: 0x0001D7CC
		// (set) Token: 0x060002F7 RID: 759 RVA: 0x0001F5D4 File Offset: 0x0001D7D4
		public bool isRightToLeftText
		{
			get
			{
				return this.m_isRightToLeft;
			}
			set
			{
				if (this.m_isRightToLeft == value)
				{
					return;
				}
				this.m_isRightToLeft = value;
				this.m_havePropertiesChanged = true;
				this.m_isCalculateSizeRequired = true;
				this.m_isInputParsingRequired = true;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000AD RID: 173
		// (get) Token: 0x060002F8 RID: 760 RVA: 0x0001F608 File Offset: 0x0001D808
		// (set) Token: 0x060002F9 RID: 761 RVA: 0x0001F610 File Offset: 0x0001D810
		public TMP_FontAsset font
		{
			get
			{
				return this.m_fontAsset;
			}
			set
			{
				if (this.m_fontAsset == value)
				{
					return;
				}
				this.m_fontAsset = value;
				this.LoadFontAsset();
				this.m_havePropertiesChanged = true;
				this.m_isCalculateSizeRequired = true;
				this.m_isInputParsingRequired = true;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000AE RID: 174
		// (get) Token: 0x060002FA RID: 762 RVA: 0x0001F64F File Offset: 0x0001D84F
		// (set) Token: 0x060002FB RID: 763 RVA: 0x0001F657 File Offset: 0x0001D857
		public virtual Material fontSharedMaterial
		{
			get
			{
				return this.m_sharedMaterial;
			}
			set
			{
				if (this.m_sharedMaterial == value)
				{
					return;
				}
				this.SetSharedMaterial(value);
				this.m_havePropertiesChanged = true;
				this.m_isInputParsingRequired = true;
				this.SetVerticesDirty();
				this.SetMaterialDirty();
			}
		}

		// Token: 0x170000AF RID: 175
		// (get) Token: 0x060002FC RID: 764 RVA: 0x0001F689 File Offset: 0x0001D889
		// (set) Token: 0x060002FD RID: 765 RVA: 0x0001F691 File Offset: 0x0001D891
		public virtual Material[] fontSharedMaterials
		{
			get
			{
				return this.GetSharedMaterials();
			}
			set
			{
				this.SetSharedMaterials(value);
				this.m_havePropertiesChanged = true;
				this.m_isInputParsingRequired = true;
				this.SetVerticesDirty();
				this.SetMaterialDirty();
			}
		}

		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x060002FE RID: 766 RVA: 0x0001F6B4 File Offset: 0x0001D8B4
		// (set) Token: 0x060002FF RID: 767 RVA: 0x0001F6C4 File Offset: 0x0001D8C4
		public Material fontMaterial
		{
			get
			{
				return this.GetMaterial(this.m_sharedMaterial);
			}
			set
			{
				if (this.m_sharedMaterial != null && this.m_sharedMaterial.GetInstanceID() == value.GetInstanceID())
				{
					return;
				}
				this.m_sharedMaterial = value;
				this.m_padding = this.GetPaddingForMaterial();
				this.m_havePropertiesChanged = true;
				this.m_isInputParsingRequired = true;
				this.SetVerticesDirty();
				this.SetMaterialDirty();
			}
		}

		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x06000300 RID: 768 RVA: 0x0001F720 File Offset: 0x0001D920
		// (set) Token: 0x06000301 RID: 769 RVA: 0x0001F691 File Offset: 0x0001D891
		public virtual Material[] fontMaterials
		{
			get
			{
				return this.GetMaterials(this.m_fontSharedMaterials);
			}
			set
			{
				this.SetSharedMaterials(value);
				this.m_havePropertiesChanged = true;
				this.m_isInputParsingRequired = true;
				this.SetVerticesDirty();
				this.SetMaterialDirty();
			}
		}

		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x06000302 RID: 770 RVA: 0x0001F72E File Offset: 0x0001D92E
		// (set) Token: 0x06000303 RID: 771 RVA: 0x0001F736 File Offset: 0x0001D936
		public override Color color
		{
			get
			{
				return this.m_fontColor;
			}
			set
			{
				if (this.m_fontColor == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_fontColor = value;
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x06000304 RID: 772 RVA: 0x0001F75B File Offset: 0x0001D95B
		// (set) Token: 0x06000305 RID: 773 RVA: 0x0001F768 File Offset: 0x0001D968
		public float alpha
		{
			get
			{
				return this.m_fontColor.a;
			}
			set
			{
				if (this.m_fontColor.a == value)
				{
					return;
				}
				this.m_fontColor.a = value;
				this.m_havePropertiesChanged = true;
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000B4 RID: 180
		// (get) Token: 0x06000306 RID: 774 RVA: 0x0001F792 File Offset: 0x0001D992
		// (set) Token: 0x06000307 RID: 775 RVA: 0x0001F79A File Offset: 0x0001D99A
		public bool enableVertexGradient
		{
			get
			{
				return this.m_enableVertexGradient;
			}
			set
			{
				if (this.m_enableVertexGradient == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_enableVertexGradient = value;
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x06000308 RID: 776 RVA: 0x0001F7BA File Offset: 0x0001D9BA
		// (set) Token: 0x06000309 RID: 777 RVA: 0x0001F7C2 File Offset: 0x0001D9C2
		public VertexGradient colorGradient
		{
			get
			{
				return this.m_fontColorGradient;
			}
			set
			{
				this.m_havePropertiesChanged = true;
				this.m_fontColorGradient = value;
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000B6 RID: 182
		// (get) Token: 0x0600030A RID: 778 RVA: 0x0001F7D8 File Offset: 0x0001D9D8
		// (set) Token: 0x0600030B RID: 779 RVA: 0x0001F7E0 File Offset: 0x0001D9E0
		public TMP_ColorGradient colorGradientPreset
		{
			get
			{
				return this.m_fontColorGradientPreset;
			}
			set
			{
				this.m_havePropertiesChanged = true;
				this.m_fontColorGradientPreset = value;
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000B7 RID: 183
		// (get) Token: 0x0600030C RID: 780 RVA: 0x0001F7F6 File Offset: 0x0001D9F6
		// (set) Token: 0x0600030D RID: 781 RVA: 0x0001F7FE File Offset: 0x0001D9FE
		public TMP_SpriteAsset spriteAsset
		{
			get
			{
				return this.m_spriteAsset;
			}
			set
			{
				this.m_spriteAsset = value;
				this.m_havePropertiesChanged = true;
				this.m_isInputParsingRequired = true;
				this.m_isCalculateSizeRequired = true;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000B8 RID: 184
		// (get) Token: 0x0600030E RID: 782 RVA: 0x0001F828 File Offset: 0x0001DA28
		// (set) Token: 0x0600030F RID: 783 RVA: 0x0001F830 File Offset: 0x0001DA30
		public bool tintAllSprites
		{
			get
			{
				return this.m_tintAllSprites;
			}
			set
			{
				if (this.m_tintAllSprites == value)
				{
					return;
				}
				this.m_tintAllSprites = value;
				this.m_havePropertiesChanged = true;
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x06000310 RID: 784 RVA: 0x0001F850 File Offset: 0x0001DA50
		// (set) Token: 0x06000311 RID: 785 RVA: 0x0001F858 File Offset: 0x0001DA58
		public bool overrideColorTags
		{
			get
			{
				return this.m_overrideHtmlColors;
			}
			set
			{
				if (this.m_overrideHtmlColors == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_overrideHtmlColors = value;
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000BA RID: 186
		// (get) Token: 0x06000312 RID: 786 RVA: 0x0001F878 File Offset: 0x0001DA78
		// (set) Token: 0x06000313 RID: 787 RVA: 0x0001F8B0 File Offset: 0x0001DAB0
		public Color32 faceColor
		{
			get
			{
				if (this.m_sharedMaterial == null)
				{
					return this.m_faceColor;
				}
				this.m_faceColor = this.m_sharedMaterial.GetColor(ShaderUtilities.ID_FaceColor);
				return this.m_faceColor;
			}
			set
			{
				if (this.m_faceColor.Compare(value))
				{
					return;
				}
				this.SetFaceColor(value);
				this.m_havePropertiesChanged = true;
				this.m_faceColor = value;
				this.SetVerticesDirty();
				this.SetMaterialDirty();
			}
		}

		// Token: 0x170000BB RID: 187
		// (get) Token: 0x06000314 RID: 788 RVA: 0x0001F8E2 File Offset: 0x0001DAE2
		// (set) Token: 0x06000315 RID: 789 RVA: 0x0001F91A File Offset: 0x0001DB1A
		public Color32 outlineColor
		{
			get
			{
				if (this.m_sharedMaterial == null)
				{
					return this.m_outlineColor;
				}
				this.m_outlineColor = this.m_sharedMaterial.GetColor(ShaderUtilities.ID_OutlineColor);
				return this.m_outlineColor;
			}
			set
			{
				if (this.m_outlineColor.Compare(value))
				{
					return;
				}
				this.SetOutlineColor(value);
				this.m_havePropertiesChanged = true;
				this.m_outlineColor = value;
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000BC RID: 188
		// (get) Token: 0x06000316 RID: 790 RVA: 0x0001F946 File Offset: 0x0001DB46
		// (set) Token: 0x06000317 RID: 791 RVA: 0x0001F979 File Offset: 0x0001DB79
		public float outlineWidth
		{
			get
			{
				if (this.m_sharedMaterial == null)
				{
					return this.m_outlineWidth;
				}
				this.m_outlineWidth = this.m_sharedMaterial.GetFloat(ShaderUtilities.ID_OutlineWidth);
				return this.m_outlineWidth;
			}
			set
			{
				if (this.m_outlineWidth == value)
				{
					return;
				}
				this.SetOutlineThickness(value);
				this.m_havePropertiesChanged = true;
				this.m_outlineWidth = value;
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000BD RID: 189
		// (get) Token: 0x06000318 RID: 792 RVA: 0x0001F9A0 File Offset: 0x0001DBA0
		// (set) Token: 0x06000319 RID: 793 RVA: 0x0001F9A8 File Offset: 0x0001DBA8
		public float fontSize
		{
			get
			{
				return this.m_fontSize;
			}
			set
			{
				if (this.m_fontSize == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_isCalculateSizeRequired = true;
				this.m_fontSize = value;
				if (!this.m_enableAutoSizing)
				{
					this.m_fontSizeBase = this.m_fontSize;
				}
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000BE RID: 190
		// (get) Token: 0x0600031A RID: 794 RVA: 0x0001F9F4 File Offset: 0x0001DBF4
		public float fontScale
		{
			get
			{
				return this.m_fontScale;
			}
		}

		// Token: 0x170000BF RID: 191
		// (get) Token: 0x0600031B RID: 795 RVA: 0x0001F9FC File Offset: 0x0001DBFC
		// (set) Token: 0x0600031C RID: 796 RVA: 0x0001FA04 File Offset: 0x0001DC04
		public int fontWeight
		{
			get
			{
				return this.m_fontWeight;
			}
			set
			{
				if (this.m_fontWeight == value)
				{
					return;
				}
				this.m_fontWeight = value;
				this.m_havePropertiesChanged = true;
				this.m_isCalculateSizeRequired = true;
				this.m_isInputParsingRequired = true;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000C0 RID: 192
		// (get) Token: 0x0600031D RID: 797 RVA: 0x0001FA38 File Offset: 0x0001DC38
		public float pixelsPerUnit
		{
			get
			{
				Canvas canvas = base.canvas;
				if (!canvas)
				{
					return 1f;
				}
				if (!this.font)
				{
					return canvas.scaleFactor;
				}
				if (this.m_currentFontAsset == null || this.m_currentFontAsset.fontInfo.PointSize <= 0f || this.m_fontSize <= 0f)
				{
					return 1f;
				}
				return this.m_fontSize / this.m_currentFontAsset.fontInfo.PointSize;
			}
		}

		// Token: 0x170000C1 RID: 193
		// (get) Token: 0x0600031E RID: 798 RVA: 0x0001FABD File Offset: 0x0001DCBD
		// (set) Token: 0x0600031F RID: 799 RVA: 0x0001FAC5 File Offset: 0x0001DCC5
		public bool enableAutoSizing
		{
			get
			{
				return this.m_enableAutoSizing;
			}
			set
			{
				if (this.m_enableAutoSizing == value)
				{
					return;
				}
				this.m_enableAutoSizing = value;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000C2 RID: 194
		// (get) Token: 0x06000320 RID: 800 RVA: 0x0001FAE4 File Offset: 0x0001DCE4
		// (set) Token: 0x06000321 RID: 801 RVA: 0x0001FAEC File Offset: 0x0001DCEC
		public float fontSizeMin
		{
			get
			{
				return this.m_fontSizeMin;
			}
			set
			{
				if (this.m_fontSizeMin == value)
				{
					return;
				}
				this.m_fontSizeMin = value;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000C3 RID: 195
		// (get) Token: 0x06000322 RID: 802 RVA: 0x0001FB0B File Offset: 0x0001DD0B
		// (set) Token: 0x06000323 RID: 803 RVA: 0x0001FB13 File Offset: 0x0001DD13
		public float fontSizeMax
		{
			get
			{
				return this.m_fontSizeMax;
			}
			set
			{
				if (this.m_fontSizeMax == value)
				{
					return;
				}
				this.m_fontSizeMax = value;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000C4 RID: 196
		// (get) Token: 0x06000324 RID: 804 RVA: 0x0001FB32 File Offset: 0x0001DD32
		// (set) Token: 0x06000325 RID: 805 RVA: 0x0001FB3A File Offset: 0x0001DD3A
		public FontStyles fontStyle
		{
			get
			{
				return this.m_fontStyle;
			}
			set
			{
				if (this.m_fontStyle == value)
				{
					return;
				}
				this.m_fontStyle = value;
				this.m_havePropertiesChanged = true;
				this.m_isCalculateSizeRequired = true;
				this.m_isInputParsingRequired = true;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000C5 RID: 197
		// (get) Token: 0x06000326 RID: 806 RVA: 0x0001FB6E File Offset: 0x0001DD6E
		public bool isUsingBold
		{
			get
			{
				return this.m_isUsingBold;
			}
		}

		// Token: 0x170000C6 RID: 198
		// (get) Token: 0x06000327 RID: 807 RVA: 0x0001FB76 File Offset: 0x0001DD76
		// (set) Token: 0x06000328 RID: 808 RVA: 0x0001FB7E File Offset: 0x0001DD7E
		public TextAlignmentOptions alignment
		{
			get
			{
				return this.m_textAlignment;
			}
			set
			{
				if (this.m_textAlignment == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_textAlignment = value;
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000C7 RID: 199
		// (get) Token: 0x06000329 RID: 809 RVA: 0x0001FB9E File Offset: 0x0001DD9E
		// (set) Token: 0x0600032A RID: 810 RVA: 0x0001FBA6 File Offset: 0x0001DDA6
		public float characterSpacing
		{
			get
			{
				return this.m_characterSpacing;
			}
			set
			{
				if (this.m_characterSpacing == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_isCalculateSizeRequired = true;
				this.m_characterSpacing = value;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000C8 RID: 200
		// (get) Token: 0x0600032B RID: 811 RVA: 0x0001FBD3 File Offset: 0x0001DDD3
		// (set) Token: 0x0600032C RID: 812 RVA: 0x0001FBDB File Offset: 0x0001DDDB
		public float wordSpacing
		{
			get
			{
				return this.m_wordSpacing;
			}
			set
			{
				if (this.m_wordSpacing == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_isCalculateSizeRequired = true;
				this.m_wordSpacing = value;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000C9 RID: 201
		// (get) Token: 0x0600032D RID: 813 RVA: 0x0001FC08 File Offset: 0x0001DE08
		// (set) Token: 0x0600032E RID: 814 RVA: 0x0001FC10 File Offset: 0x0001DE10
		public float lineSpacing
		{
			get
			{
				return this.m_lineSpacing;
			}
			set
			{
				if (this.m_lineSpacing == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_isCalculateSizeRequired = true;
				this.m_lineSpacing = value;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000CA RID: 202
		// (get) Token: 0x0600032F RID: 815 RVA: 0x0001FC3D File Offset: 0x0001DE3D
		// (set) Token: 0x06000330 RID: 816 RVA: 0x0001FC45 File Offset: 0x0001DE45
		public float lineSpacingAdjustment
		{
			get
			{
				return this.m_lineSpacingMax;
			}
			set
			{
				if (this.m_lineSpacingMax == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_isCalculateSizeRequired = true;
				this.m_lineSpacingMax = value;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000CB RID: 203
		// (get) Token: 0x06000331 RID: 817 RVA: 0x0001FC72 File Offset: 0x0001DE72
		// (set) Token: 0x06000332 RID: 818 RVA: 0x0001FC7A File Offset: 0x0001DE7A
		public float paragraphSpacing
		{
			get
			{
				return this.m_paragraphSpacing;
			}
			set
			{
				if (this.m_paragraphSpacing == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_isCalculateSizeRequired = true;
				this.m_paragraphSpacing = value;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000CC RID: 204
		// (get) Token: 0x06000333 RID: 819 RVA: 0x0001FCA7 File Offset: 0x0001DEA7
		// (set) Token: 0x06000334 RID: 820 RVA: 0x0001FCAF File Offset: 0x0001DEAF
		public float characterWidthAdjustment
		{
			get
			{
				return this.m_charWidthMaxAdj;
			}
			set
			{
				if (this.m_charWidthMaxAdj == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_isCalculateSizeRequired = true;
				this.m_charWidthMaxAdj = value;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000CD RID: 205
		// (get) Token: 0x06000335 RID: 821 RVA: 0x0001FCDC File Offset: 0x0001DEDC
		// (set) Token: 0x06000336 RID: 822 RVA: 0x0001FCE4 File Offset: 0x0001DEE4
		public bool enableWordWrapping
		{
			get
			{
				return this.m_enableWordWrapping;
			}
			set
			{
				if (this.m_enableWordWrapping == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_isInputParsingRequired = true;
				this.m_isCalculateSizeRequired = true;
				this.m_enableWordWrapping = value;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000CE RID: 206
		// (get) Token: 0x06000337 RID: 823 RVA: 0x0001FD18 File Offset: 0x0001DF18
		// (set) Token: 0x06000338 RID: 824 RVA: 0x0001FD20 File Offset: 0x0001DF20
		public float wordWrappingRatios
		{
			get
			{
				return this.m_wordWrappingRatios;
			}
			set
			{
				if (this.m_wordWrappingRatios == value)
				{
					return;
				}
				this.m_wordWrappingRatios = value;
				this.m_havePropertiesChanged = true;
				this.m_isCalculateSizeRequired = true;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000CF RID: 207
		// (get) Token: 0x06000339 RID: 825 RVA: 0x0001FD4D File Offset: 0x0001DF4D
		// (set) Token: 0x0600033A RID: 826 RVA: 0x0001FD55 File Offset: 0x0001DF55
		public TextOverflowModes overflowMode
		{
			get
			{
				return this.m_overflowMode;
			}
			set
			{
				if (this.m_overflowMode == value)
				{
					return;
				}
				this.m_overflowMode = value;
				this.m_havePropertiesChanged = true;
				this.m_isCalculateSizeRequired = true;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000D0 RID: 208
		// (get) Token: 0x0600033B RID: 827 RVA: 0x0001FD82 File Offset: 0x0001DF82
		public bool isTextOverflowing
		{
			get
			{
				return this.m_firstOverflowCharacterIndex != -1;
			}
		}

		// Token: 0x170000D1 RID: 209
		// (get) Token: 0x0600033C RID: 828 RVA: 0x0001FD90 File Offset: 0x0001DF90
		public int firstOverflowCharacterIndex
		{
			get
			{
				return this.m_firstOverflowCharacterIndex;
			}
		}

		// Token: 0x170000D2 RID: 210
		// (get) Token: 0x0600033D RID: 829 RVA: 0x0001FD98 File Offset: 0x0001DF98
		// (set) Token: 0x0600033E RID: 830 RVA: 0x0001FDA0 File Offset: 0x0001DFA0
		public TMP_Text linkedTextComponent
		{
			get
			{
				return this.m_linkedTextComponent;
			}
			set
			{
				if (this.m_linkedTextComponent != value)
				{
					if (this.m_linkedTextComponent != null)
					{
						this.m_linkedTextComponent.overflowMode = TextOverflowModes.Overflow;
						this.m_linkedTextComponent.linkedTextComponent = null;
						this.m_linkedTextComponent.isLinkedTextComponent = false;
					}
					this.m_linkedTextComponent = value;
					if (this.m_linkedTextComponent != null)
					{
						this.m_linkedTextComponent.isLinkedTextComponent = true;
					}
				}
				this.m_havePropertiesChanged = true;
				this.m_isCalculateSizeRequired = true;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000D3 RID: 211
		// (get) Token: 0x0600033F RID: 831 RVA: 0x0001FE28 File Offset: 0x0001E028
		// (set) Token: 0x06000340 RID: 832 RVA: 0x0001FE30 File Offset: 0x0001E030
		public bool isLinkedTextComponent
		{
			get
			{
				return this.m_isLinkedTextComponent;
			}
			set
			{
				this.m_isLinkedTextComponent = value;
				if (!this.m_isLinkedTextComponent)
				{
					this.m_firstVisibleCharacter = 0;
				}
				this.m_havePropertiesChanged = true;
				this.m_isCalculateSizeRequired = true;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000D4 RID: 212
		// (get) Token: 0x06000341 RID: 833 RVA: 0x0001FE62 File Offset: 0x0001E062
		public bool isTextTruncated
		{
			get
			{
				return this.m_isTextTruncated;
			}
		}

		// Token: 0x170000D5 RID: 213
		// (get) Token: 0x06000342 RID: 834 RVA: 0x0001FE6A File Offset: 0x0001E06A
		// (set) Token: 0x06000343 RID: 835 RVA: 0x0001FE72 File Offset: 0x0001E072
		public bool enableKerning
		{
			get
			{
				return this.m_enableKerning;
			}
			set
			{
				if (this.m_enableKerning == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_isCalculateSizeRequired = true;
				this.m_enableKerning = value;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000D6 RID: 214
		// (get) Token: 0x06000344 RID: 836 RVA: 0x0001FE9F File Offset: 0x0001E09F
		// (set) Token: 0x06000345 RID: 837 RVA: 0x0001FEA7 File Offset: 0x0001E0A7
		public bool extraPadding
		{
			get
			{
				return this.m_enableExtraPadding;
			}
			set
			{
				if (this.m_enableExtraPadding == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_enableExtraPadding = value;
				this.UpdateMeshPadding();
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000D7 RID: 215
		// (get) Token: 0x06000346 RID: 838 RVA: 0x0001FECD File Offset: 0x0001E0CD
		// (set) Token: 0x06000347 RID: 839 RVA: 0x0001FED5 File Offset: 0x0001E0D5
		public bool richText
		{
			get
			{
				return this.m_isRichText;
			}
			set
			{
				if (this.m_isRichText == value)
				{
					return;
				}
				this.m_isRichText = value;
				this.m_havePropertiesChanged = true;
				this.m_isCalculateSizeRequired = true;
				this.m_isInputParsingRequired = true;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000D8 RID: 216
		// (get) Token: 0x06000348 RID: 840 RVA: 0x0001FF09 File Offset: 0x0001E109
		// (set) Token: 0x06000349 RID: 841 RVA: 0x0001FF11 File Offset: 0x0001E111
		public bool parseCtrlCharacters
		{
			get
			{
				return this.m_parseCtrlCharacters;
			}
			set
			{
				if (this.m_parseCtrlCharacters == value)
				{
					return;
				}
				this.m_parseCtrlCharacters = value;
				this.m_havePropertiesChanged = true;
				this.m_isCalculateSizeRequired = true;
				this.m_isInputParsingRequired = true;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000D9 RID: 217
		// (get) Token: 0x0600034A RID: 842 RVA: 0x0001FF45 File Offset: 0x0001E145
		// (set) Token: 0x0600034B RID: 843 RVA: 0x0001FF4D File Offset: 0x0001E14D
		public bool isOverlay
		{
			get
			{
				return this.m_isOverlay;
			}
			set
			{
				if (this.m_isOverlay == value)
				{
					return;
				}
				this.m_isOverlay = value;
				this.SetShaderDepth();
				this.m_havePropertiesChanged = true;
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000DA RID: 218
		// (get) Token: 0x0600034C RID: 844 RVA: 0x0001FF73 File Offset: 0x0001E173
		// (set) Token: 0x0600034D RID: 845 RVA: 0x0001FF7B File Offset: 0x0001E17B
		public bool isOrthographic
		{
			get
			{
				return this.m_isOrthographic;
			}
			set
			{
				if (this.m_isOrthographic == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_isOrthographic = value;
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000DB RID: 219
		// (get) Token: 0x0600034E RID: 846 RVA: 0x0001FF9B File Offset: 0x0001E19B
		// (set) Token: 0x0600034F RID: 847 RVA: 0x0001FFA3 File Offset: 0x0001E1A3
		public bool enableCulling
		{
			get
			{
				return this.m_isCullingEnabled;
			}
			set
			{
				if (this.m_isCullingEnabled == value)
				{
					return;
				}
				this.m_isCullingEnabled = value;
				this.SetCulling();
				this.m_havePropertiesChanged = true;
			}
		}

		// Token: 0x170000DC RID: 220
		// (get) Token: 0x06000350 RID: 848 RVA: 0x0001FFC3 File Offset: 0x0001E1C3
		// (set) Token: 0x06000351 RID: 849 RVA: 0x0001FFCB File Offset: 0x0001E1CB
		public bool ignoreRectMaskCulling
		{
			get
			{
				return this.m_ignoreRectMaskCulling;
			}
			set
			{
				if (this.m_ignoreRectMaskCulling == value)
				{
					return;
				}
				this.m_ignoreRectMaskCulling = value;
				this.m_havePropertiesChanged = true;
			}
		}

		// Token: 0x170000DD RID: 221
		// (get) Token: 0x06000352 RID: 850 RVA: 0x0001FFE5 File Offset: 0x0001E1E5
		// (set) Token: 0x06000353 RID: 851 RVA: 0x0001FFED File Offset: 0x0001E1ED
		public bool ignoreVisibility
		{
			get
			{
				return this.m_ignoreCulling;
			}
			set
			{
				if (this.m_ignoreCulling == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_ignoreCulling = value;
			}
		}

		// Token: 0x170000DE RID: 222
		// (get) Token: 0x06000354 RID: 852 RVA: 0x00020007 File Offset: 0x0001E207
		// (set) Token: 0x06000355 RID: 853 RVA: 0x0002000F File Offset: 0x0001E20F
		public TextureMappingOptions horizontalMapping
		{
			get
			{
				return this.m_horizontalMapping;
			}
			set
			{
				if (this.m_horizontalMapping == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_horizontalMapping = value;
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000DF RID: 223
		// (get) Token: 0x06000356 RID: 854 RVA: 0x0002002F File Offset: 0x0001E22F
		// (set) Token: 0x06000357 RID: 855 RVA: 0x00020037 File Offset: 0x0001E237
		public TextureMappingOptions verticalMapping
		{
			get
			{
				return this.m_verticalMapping;
			}
			set
			{
				if (this.m_verticalMapping == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_verticalMapping = value;
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000E0 RID: 224
		// (get) Token: 0x06000358 RID: 856 RVA: 0x00020057 File Offset: 0x0001E257
		// (set) Token: 0x06000359 RID: 857 RVA: 0x0002005F File Offset: 0x0001E25F
		public float mappingUvLineOffset
		{
			get
			{
				return this.m_uvLineOffset;
			}
			set
			{
				if (this.m_uvLineOffset == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_uvLineOffset = value;
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000E1 RID: 225
		// (get) Token: 0x0600035A RID: 858 RVA: 0x0002007F File Offset: 0x0001E27F
		// (set) Token: 0x0600035B RID: 859 RVA: 0x00020087 File Offset: 0x0001E287
		public TextRenderFlags renderMode
		{
			get
			{
				return this.m_renderMode;
			}
			set
			{
				if (this.m_renderMode == value)
				{
					return;
				}
				this.m_renderMode = value;
				this.m_havePropertiesChanged = true;
			}
		}

		// Token: 0x170000E2 RID: 226
		// (get) Token: 0x0600035C RID: 860 RVA: 0x000200A1 File Offset: 0x0001E2A1
		// (set) Token: 0x0600035D RID: 861 RVA: 0x000200A9 File Offset: 0x0001E2A9
		public VertexSortingOrder geometrySortingOrder
		{
			get
			{
				return this.m_geometrySortingOrder;
			}
			set
			{
				this.m_geometrySortingOrder = value;
				this.m_havePropertiesChanged = true;
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000E3 RID: 227
		// (get) Token: 0x0600035E RID: 862 RVA: 0x000200BF File Offset: 0x0001E2BF
		// (set) Token: 0x0600035F RID: 863 RVA: 0x000200C7 File Offset: 0x0001E2C7
		public int firstVisibleCharacter
		{
			get
			{
				return this.m_firstVisibleCharacter;
			}
			set
			{
				if (this.m_firstVisibleCharacter == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_firstVisibleCharacter = value;
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000E4 RID: 228
		// (get) Token: 0x06000360 RID: 864 RVA: 0x000200E7 File Offset: 0x0001E2E7
		// (set) Token: 0x06000361 RID: 865 RVA: 0x000200EF File Offset: 0x0001E2EF
		public int maxVisibleCharacters
		{
			get
			{
				return this.m_maxVisibleCharacters;
			}
			set
			{
				if (this.m_maxVisibleCharacters == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_maxVisibleCharacters = value;
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000E5 RID: 229
		// (get) Token: 0x06000362 RID: 866 RVA: 0x0002010F File Offset: 0x0001E30F
		// (set) Token: 0x06000363 RID: 867 RVA: 0x00020117 File Offset: 0x0001E317
		public int maxVisibleWords
		{
			get
			{
				return this.m_maxVisibleWords;
			}
			set
			{
				if (this.m_maxVisibleWords == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_maxVisibleWords = value;
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000E6 RID: 230
		// (get) Token: 0x06000364 RID: 868 RVA: 0x00020137 File Offset: 0x0001E337
		// (set) Token: 0x06000365 RID: 869 RVA: 0x0002013F File Offset: 0x0001E33F
		public int maxVisibleLines
		{
			get
			{
				return this.m_maxVisibleLines;
			}
			set
			{
				if (this.m_maxVisibleLines == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_isInputParsingRequired = true;
				this.m_maxVisibleLines = value;
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000E7 RID: 231
		// (get) Token: 0x06000366 RID: 870 RVA: 0x00020166 File Offset: 0x0001E366
		// (set) Token: 0x06000367 RID: 871 RVA: 0x0002016E File Offset: 0x0001E36E
		public bool useMaxVisibleDescender
		{
			get
			{
				return this.m_useMaxVisibleDescender;
			}
			set
			{
				if (this.m_useMaxVisibleDescender == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_isInputParsingRequired = true;
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000E8 RID: 232
		// (get) Token: 0x06000368 RID: 872 RVA: 0x0002018E File Offset: 0x0001E38E
		// (set) Token: 0x06000369 RID: 873 RVA: 0x00020196 File Offset: 0x0001E396
		public int pageToDisplay
		{
			get
			{
				return this.m_pageToDisplay;
			}
			set
			{
				if (this.m_pageToDisplay == value)
				{
					return;
				}
				this.m_havePropertiesChanged = true;
				this.m_pageToDisplay = value;
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000E9 RID: 233
		// (get) Token: 0x0600036A RID: 874 RVA: 0x000201B6 File Offset: 0x0001E3B6
		// (set) Token: 0x0600036B RID: 875 RVA: 0x000201BE File Offset: 0x0001E3BE
		public virtual Vector4 margin
		{
			get
			{
				return this.m_margin;
			}
			set
			{
				if (this.m_margin == value)
				{
					return;
				}
				this.m_margin = value;
				this.ComputeMarginSize();
				this.m_havePropertiesChanged = true;
				this.SetVerticesDirty();
			}
		}

		// Token: 0x170000EA RID: 234
		// (get) Token: 0x0600036C RID: 876 RVA: 0x000201E9 File Offset: 0x0001E3E9
		public TMP_TextInfo textInfo
		{
			get
			{
				return this.m_textInfo;
			}
		}

		// Token: 0x170000EB RID: 235
		// (get) Token: 0x0600036D RID: 877 RVA: 0x000201F1 File Offset: 0x0001E3F1
		// (set) Token: 0x0600036E RID: 878 RVA: 0x000201F9 File Offset: 0x0001E3F9
		public bool havePropertiesChanged
		{
			get
			{
				return this.m_havePropertiesChanged;
			}
			set
			{
				if (this.m_havePropertiesChanged == value)
				{
					return;
				}
				this.m_havePropertiesChanged = value;
				this.m_isInputParsingRequired = true;
				this.SetAllDirty();
			}
		}

		// Token: 0x170000EC RID: 236
		// (get) Token: 0x0600036F RID: 879 RVA: 0x00020219 File Offset: 0x0001E419
		// (set) Token: 0x06000370 RID: 880 RVA: 0x00020221 File Offset: 0x0001E421
		public bool isUsingLegacyAnimationComponent
		{
			get
			{
				return this.m_isUsingLegacyAnimationComponent;
			}
			set
			{
				this.m_isUsingLegacyAnimationComponent = value;
			}
		}

		// Token: 0x170000ED RID: 237
		// (get) Token: 0x06000371 RID: 881 RVA: 0x00002E59 File Offset: 0x00001059
		public new Transform transform
		{
			get
			{
				if (this.m_transform == null)
				{
					this.m_transform = base.GetComponent<Transform>();
				}
				return this.m_transform;
			}
		}

		// Token: 0x170000EE RID: 238
		// (get) Token: 0x06000372 RID: 882 RVA: 0x0002022A File Offset: 0x0001E42A
		public new RectTransform rectTransform
		{
			get
			{
				if (this.m_rectTransform == null)
				{
					this.m_rectTransform = base.GetComponent<RectTransform>();
				}
				return this.m_rectTransform;
			}
		}

		// Token: 0x170000EF RID: 239
		// (get) Token: 0x06000373 RID: 883 RVA: 0x0002024C File Offset: 0x0001E44C
		// (set) Token: 0x06000374 RID: 884 RVA: 0x00020254 File Offset: 0x0001E454
		public virtual bool autoSizeTextContainer
		{
			[CompilerGenerated]
			get
			{
				return this.<autoSizeTextContainer>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<autoSizeTextContainer>k__BackingField = value;
			}
		}

		// Token: 0x170000F0 RID: 240
		// (get) Token: 0x06000375 RID: 885 RVA: 0x0000C075 File Offset: 0x0000A275
		public virtual Mesh mesh
		{
			get
			{
				return this.m_mesh;
			}
		}

		// Token: 0x170000F1 RID: 241
		// (get) Token: 0x06000376 RID: 886 RVA: 0x0002025D File Offset: 0x0001E45D
		// (set) Token: 0x06000377 RID: 887 RVA: 0x00020265 File Offset: 0x0001E465
		public bool isVolumetricText
		{
			get
			{
				return this.m_isVolumetricText;
			}
			set
			{
				if (this.m_isVolumetricText == value)
				{
					return;
				}
				this.m_havePropertiesChanged = value;
				this.m_textInfo.ResetVertexLayout(value);
				this.m_isInputParsingRequired = true;
				this.SetVerticesDirty();
				this.SetLayoutDirty();
			}
		}

		// Token: 0x170000F2 RID: 242
		// (get) Token: 0x06000378 RID: 888 RVA: 0x00020298 File Offset: 0x0001E498
		public Bounds bounds
		{
			get
			{
				if (this.m_mesh == null)
				{
					return default(Bounds);
				}
				return this.GetCompoundBounds();
			}
		}

		// Token: 0x170000F3 RID: 243
		// (get) Token: 0x06000379 RID: 889 RVA: 0x000202C4 File Offset: 0x0001E4C4
		public Bounds textBounds
		{
			get
			{
				if (this.m_textInfo == null)
				{
					return default(Bounds);
				}
				return this.GetTextBounds();
			}
		}

		// Token: 0x170000F4 RID: 244
		// (get) Token: 0x0600037A RID: 890 RVA: 0x000202EC File Offset: 0x0001E4EC
		protected TMP_SpriteAnimator spriteAnimator
		{
			get
			{
				if (this.m_spriteAnimator == null)
				{
					this.m_spriteAnimator = base.GetComponent<TMP_SpriteAnimator>();
					if (this.m_spriteAnimator == null)
					{
						this.m_spriteAnimator = base.gameObject.AddComponent<TMP_SpriteAnimator>();
					}
				}
				return this.m_spriteAnimator;
			}
		}

		// Token: 0x170000F5 RID: 245
		// (get) Token: 0x0600037B RID: 891 RVA: 0x00020338 File Offset: 0x0001E538
		public float flexibleHeight
		{
			get
			{
				return this.m_flexibleHeight;
			}
		}

		// Token: 0x170000F6 RID: 246
		// (get) Token: 0x0600037C RID: 892 RVA: 0x00020340 File Offset: 0x0001E540
		public float flexibleWidth
		{
			get
			{
				return this.m_flexibleWidth;
			}
		}

		// Token: 0x170000F7 RID: 247
		// (get) Token: 0x0600037D RID: 893 RVA: 0x00020348 File Offset: 0x0001E548
		public float minWidth
		{
			get
			{
				return this.m_minWidth;
			}
		}

		// Token: 0x170000F8 RID: 248
		// (get) Token: 0x0600037E RID: 894 RVA: 0x00020350 File Offset: 0x0001E550
		public float minHeight
		{
			get
			{
				return this.m_minHeight;
			}
		}

		// Token: 0x170000F9 RID: 249
		// (get) Token: 0x0600037F RID: 895 RVA: 0x00020358 File Offset: 0x0001E558
		public float maxWidth
		{
			get
			{
				return this.m_maxWidth;
			}
		}

		// Token: 0x170000FA RID: 250
		// (get) Token: 0x06000380 RID: 896 RVA: 0x00020360 File Offset: 0x0001E560
		public float maxHeight
		{
			get
			{
				return this.m_maxHeight;
			}
		}

		// Token: 0x170000FB RID: 251
		// (get) Token: 0x06000381 RID: 897 RVA: 0x00020368 File Offset: 0x0001E568
		protected LayoutElement layoutElement
		{
			get
			{
				if (this.m_LayoutElement == null)
				{
					this.m_LayoutElement = base.GetComponent<LayoutElement>();
				}
				return this.m_LayoutElement;
			}
		}

		// Token: 0x170000FC RID: 252
		// (get) Token: 0x06000382 RID: 898 RVA: 0x0002038A File Offset: 0x0001E58A
		public virtual float preferredWidth
		{
			get
			{
				if (!this.m_isPreferredWidthDirty)
				{
					return this.m_preferredWidth;
				}
				this.m_preferredWidth = this.GetPreferredWidth();
				return this.m_preferredWidth;
			}
		}

		// Token: 0x170000FD RID: 253
		// (get) Token: 0x06000383 RID: 899 RVA: 0x000203AD File Offset: 0x0001E5AD
		public virtual float preferredHeight
		{
			get
			{
				if (!this.m_isPreferredHeightDirty)
				{
					return this.m_preferredHeight;
				}
				this.m_preferredHeight = this.GetPreferredHeight();
				return this.m_preferredHeight;
			}
		}

		// Token: 0x170000FE RID: 254
		// (get) Token: 0x06000384 RID: 900 RVA: 0x000203D0 File Offset: 0x0001E5D0
		public virtual float renderedWidth
		{
			get
			{
				return this.GetRenderedWidth();
			}
		}

		// Token: 0x170000FF RID: 255
		// (get) Token: 0x06000385 RID: 901 RVA: 0x000203D8 File Offset: 0x0001E5D8
		public virtual float renderedHeight
		{
			get
			{
				return this.GetRenderedHeight();
			}
		}

		// Token: 0x17000100 RID: 256
		// (get) Token: 0x06000386 RID: 902 RVA: 0x000203E0 File Offset: 0x0001E5E0
		public int layoutPriority
		{
			get
			{
				return this.m_layoutPriority;
			}
		}

		// Token: 0x06000387 RID: 903 RVA: 0x0000297D File Offset: 0x00000B7D
		protected virtual void LoadFontAsset()
		{
		}

		// Token: 0x06000388 RID: 904 RVA: 0x0000297D File Offset: 0x00000B7D
		protected virtual void SetSharedMaterial(Material mat)
		{
		}

		// Token: 0x06000389 RID: 905 RVA: 0x00002E56 File Offset: 0x00001056
		protected virtual Material GetMaterial(Material mat)
		{
			return null;
		}

		// Token: 0x0600038A RID: 906 RVA: 0x0000297D File Offset: 0x00000B7D
		protected virtual void SetFontBaseMaterial(Material mat)
		{
		}

		// Token: 0x0600038B RID: 907 RVA: 0x00002E56 File Offset: 0x00001056
		protected virtual Material[] GetSharedMaterials()
		{
			return null;
		}

		// Token: 0x0600038C RID: 908 RVA: 0x0000297D File Offset: 0x00000B7D
		protected virtual void SetSharedMaterials(Material[] materials)
		{
		}

		// Token: 0x0600038D RID: 909 RVA: 0x00002E56 File Offset: 0x00001056
		protected virtual Material[] GetMaterials(Material[] mats)
		{
			return null;
		}

		// Token: 0x0600038E RID: 910 RVA: 0x0001EE2B File Offset: 0x0001D02B
		protected virtual Material CreateMaterialInstance(Material source)
		{
			Material material = new Material(source);
			material.shaderKeywords = source.shaderKeywords;
			material.name += " (Instance)";
			return material;
		}

		// Token: 0x0600038F RID: 911 RVA: 0x000203E8 File Offset: 0x0001E5E8
		protected void SetVertexColorGradient(TMP_ColorGradient gradient)
		{
			if (gradient == null)
			{
				return;
			}
			this.m_fontColorGradient.bottomLeft = gradient.bottomLeft;
			this.m_fontColorGradient.bottomRight = gradient.bottomRight;
			this.m_fontColorGradient.topLeft = gradient.topLeft;
			this.m_fontColorGradient.topRight = gradient.topRight;
			this.SetVerticesDirty();
		}

		// Token: 0x06000390 RID: 912 RVA: 0x0000297D File Offset: 0x00000B7D
		protected void SetTextSortingOrder(VertexSortingOrder order)
		{
		}

		// Token: 0x06000391 RID: 913 RVA: 0x0000297D File Offset: 0x00000B7D
		protected void SetTextSortingOrder(int[] order)
		{
		}

		// Token: 0x06000392 RID: 914 RVA: 0x0000297D File Offset: 0x00000B7D
		protected virtual void SetFaceColor(Color32 color)
		{
		}

		// Token: 0x06000393 RID: 915 RVA: 0x0000297D File Offset: 0x00000B7D
		protected virtual void SetOutlineColor(Color32 color)
		{
		}

		// Token: 0x06000394 RID: 916 RVA: 0x0000297D File Offset: 0x00000B7D
		protected virtual void SetOutlineThickness(float thickness)
		{
		}

		// Token: 0x06000395 RID: 917 RVA: 0x0000297D File Offset: 0x00000B7D
		protected virtual void SetShaderDepth()
		{
		}

		// Token: 0x06000396 RID: 918 RVA: 0x0000297D File Offset: 0x00000B7D
		protected virtual void SetCulling()
		{
		}

		// Token: 0x06000397 RID: 919 RVA: 0x00020449 File Offset: 0x0001E649
		protected virtual float GetPaddingForMaterial()
		{
			return 0f;
		}

		// Token: 0x06000398 RID: 920 RVA: 0x00020449 File Offset: 0x0001E649
		protected virtual float GetPaddingForMaterial(Material mat)
		{
			return 0f;
		}

		// Token: 0x06000399 RID: 921 RVA: 0x00002E56 File Offset: 0x00001056
		protected virtual Vector3[] GetTextContainerLocalCorners()
		{
			return null;
		}

		// Token: 0x0600039A RID: 922 RVA: 0x0000297D File Offset: 0x00000B7D
		public virtual void ForceMeshUpdate()
		{
		}

		// Token: 0x0600039B RID: 923 RVA: 0x0000297D File Offset: 0x00000B7D
		public virtual void ForceMeshUpdate(bool ignoreActiveState)
		{
		}

		// Token: 0x0600039C RID: 924 RVA: 0x00020450 File Offset: 0x0001E650
		internal void SetTextInternal(string text)
		{
			this.m_text = text;
			this.m_renderMode = TextRenderFlags.DontRender;
			this.m_isInputParsingRequired = true;
			this.ForceMeshUpdate();
			this.m_renderMode = TextRenderFlags.Render;
		}

		// Token: 0x0600039D RID: 925 RVA: 0x0000297D File Offset: 0x00000B7D
		public virtual void UpdateGeometry(Mesh mesh, int index)
		{
		}

		// Token: 0x0600039E RID: 926 RVA: 0x0000297D File Offset: 0x00000B7D
		public virtual void UpdateVertexData(TMP_VertexDataUpdateFlags flags)
		{
		}

		// Token: 0x0600039F RID: 927 RVA: 0x0000297D File Offset: 0x00000B7D
		public virtual void UpdateVertexData()
		{
		}

		// Token: 0x060003A0 RID: 928 RVA: 0x0000297D File Offset: 0x00000B7D
		public virtual void SetVertices(Vector3[] vertices)
		{
		}

		// Token: 0x060003A1 RID: 929 RVA: 0x0000297D File Offset: 0x00000B7D
		public virtual void UpdateMeshPadding()
		{
		}

		// Token: 0x060003A2 RID: 930 RVA: 0x00020478 File Offset: 0x0001E678
		public override void CrossFadeColor(Color targetColor, float duration, bool ignoreTimeScale, bool useAlpha)
		{
			base.CrossFadeColor(targetColor, duration, ignoreTimeScale, useAlpha);
			this.InternalCrossFadeColor(targetColor, duration, ignoreTimeScale, useAlpha);
		}

		// Token: 0x060003A3 RID: 931 RVA: 0x00020490 File Offset: 0x0001E690
		public override void CrossFadeAlpha(float alpha, float duration, bool ignoreTimeScale)
		{
			base.CrossFadeAlpha(alpha, duration, ignoreTimeScale);
			this.InternalCrossFadeAlpha(alpha, duration, ignoreTimeScale);
		}

		// Token: 0x060003A4 RID: 932 RVA: 0x0000297D File Offset: 0x00000B7D
		protected virtual void InternalCrossFadeColor(Color targetColor, float duration, bool ignoreTimeScale, bool useAlpha)
		{
		}

		// Token: 0x060003A5 RID: 933 RVA: 0x0000297D File Offset: 0x00000B7D
		protected virtual void InternalCrossFadeAlpha(float alpha, float duration, bool ignoreTimeScale)
		{
		}

		// Token: 0x060003A6 RID: 934 RVA: 0x000204A4 File Offset: 0x0001E6A4
		protected void ParseInputText()
		{
			this.m_isInputParsingRequired = false;
			switch (this.m_inputSource)
			{
			case TMP_Text.TextInputSources.Text:
			case TMP_Text.TextInputSources.String:
				this.StringToCharArray(this.m_text, ref this.m_char_buffer);
				break;
			case TMP_Text.TextInputSources.SetText:
				this.SetTextArrayToCharArray(this.m_input_CharArray, ref this.m_char_buffer);
				break;
			}
			this.SetArraySizes(this.m_char_buffer);
		}

		// Token: 0x060003A7 RID: 935 RVA: 0x0002050A File Offset: 0x0001E70A
		public void SetText(string text)
		{
			this.SetText(text, true);
		}

		// Token: 0x060003A8 RID: 936 RVA: 0x00020514 File Offset: 0x0001E714
		public void SetText(string text, bool syncTextInputBox)
		{
			this.m_inputSource = TMP_Text.TextInputSources.SetCharArray;
			this.StringToCharArray(text, ref this.m_char_buffer);
			this.m_isInputParsingRequired = true;
			this.m_havePropertiesChanged = true;
			this.m_isCalculateSizeRequired = true;
			this.SetVerticesDirty();
			this.SetLayoutDirty();
		}

		// Token: 0x060003A9 RID: 937 RVA: 0x0002054B File Offset: 0x0001E74B
		public void SetText(string text, float arg0)
		{
			this.SetText(text, arg0, 255f, 255f);
		}

		// Token: 0x060003AA RID: 938 RVA: 0x0002055F File Offset: 0x0001E75F
		public void SetText(string text, float arg0, float arg1)
		{
			this.SetText(text, arg0, arg1, 255f);
		}

		// Token: 0x060003AB RID: 939 RVA: 0x00020570 File Offset: 0x0001E770
		public void SetText(string text, float arg0, float arg1, float arg2)
		{
			int precision = 0;
			int num = 0;
			for (int i = 0; i < text.Length; i++)
			{
				char c = text[i];
				if (c == '{')
				{
					if (text[i + 2] == ':')
					{
						precision = (int)(text[i + 3] - '0');
					}
					switch (text[i + 1])
					{
					case '0':
						this.AddFloatToCharArray(arg0, ref num, precision);
						break;
					case '1':
						this.AddFloatToCharArray(arg1, ref num, precision);
						break;
					case '2':
						this.AddFloatToCharArray(arg2, ref num, precision);
						break;
					}
					if (text[i + 2] == ':')
					{
						i += 4;
					}
					else
					{
						i += 2;
					}
				}
				else
				{
					this.m_input_CharArray[num] = c;
					num++;
				}
			}
			this.m_input_CharArray[num] = '\0';
			this.m_charArray_Length = num;
			this.m_inputSource = TMP_Text.TextInputSources.SetText;
			this.m_isInputParsingRequired = true;
			this.m_havePropertiesChanged = true;
			this.m_isCalculateSizeRequired = true;
			this.SetVerticesDirty();
			this.SetLayoutDirty();
		}

		// Token: 0x060003AC RID: 940 RVA: 0x00020663 File Offset: 0x0001E863
		public void SetText(StringBuilder text)
		{
			this.m_inputSource = TMP_Text.TextInputSources.SetCharArray;
			this.StringBuilderToIntArray(text, ref this.m_char_buffer);
			this.m_isInputParsingRequired = true;
			this.m_havePropertiesChanged = true;
			this.m_isCalculateSizeRequired = true;
			this.SetVerticesDirty();
			this.SetLayoutDirty();
		}

		// Token: 0x060003AD RID: 941 RVA: 0x0002069C File Offset: 0x0001E89C
		public void SetCharArray(char[] sourceText)
		{
			if (this.m_char_buffer == null)
			{
				this.m_char_buffer = new int[8];
			}
			this.m_styleStack.Clear();
			int num = 0;
			int num2 = 0;
			while (sourceText != null && num2 < sourceText.Length)
			{
				if (sourceText[num2] != '\\' || num2 >= sourceText.Length - 1)
				{
					goto IL_E1;
				}
				int num3 = (int)sourceText[num2 + 1];
				if (num3 != 110)
				{
					if (num3 != 114)
					{
						if (num3 != 116)
						{
							goto IL_E1;
						}
						if (num == this.m_char_buffer.Length)
						{
							this.ResizeInternalArray<int>(ref this.m_char_buffer);
						}
						this.m_char_buffer[num] = 9;
						num2++;
						num++;
					}
					else
					{
						if (num == this.m_char_buffer.Length)
						{
							this.ResizeInternalArray<int>(ref this.m_char_buffer);
						}
						this.m_char_buffer[num] = 13;
						num2++;
						num++;
					}
				}
				else
				{
					if (num == this.m_char_buffer.Length)
					{
						this.ResizeInternalArray<int>(ref this.m_char_buffer);
					}
					this.m_char_buffer[num] = 10;
					num2++;
					num++;
				}
				IL_19F:
				num2++;
				continue;
				IL_E1:
				if (sourceText[num2] == '<')
				{
					if (this.IsTagName(ref sourceText, "<BR>", num2))
					{
						if (num == this.m_char_buffer.Length)
						{
							this.ResizeInternalArray<int>(ref this.m_char_buffer);
						}
						this.m_char_buffer[num] = 10;
						num++;
						num2 += 3;
						goto IL_19F;
					}
					if (this.IsTagName(ref sourceText, "<STYLE=", num2))
					{
						int num4 = 0;
						if (this.ReplaceOpeningStyleTag(ref sourceText, num2, out num4, ref this.m_char_buffer, ref num))
						{
							num2 = num4;
							goto IL_19F;
						}
					}
					else if (this.IsTagName(ref sourceText, "</STYLE>", num2))
					{
						this.ReplaceClosingStyleTag(ref sourceText, num2, ref this.m_char_buffer, ref num);
						num2 += 7;
						goto IL_19F;
					}
				}
				if (num == this.m_char_buffer.Length)
				{
					this.ResizeInternalArray<int>(ref this.m_char_buffer);
				}
				this.m_char_buffer[num] = (int)sourceText[num2];
				num++;
				goto IL_19F;
			}
			if (num == this.m_char_buffer.Length)
			{
				this.ResizeInternalArray<int>(ref this.m_char_buffer);
			}
			this.m_char_buffer[num] = 0;
			this.m_inputSource = TMP_Text.TextInputSources.SetCharArray;
			this.m_isInputParsingRequired = true;
			this.m_havePropertiesChanged = true;
			this.m_isCalculateSizeRequired = true;
			this.SetVerticesDirty();
			this.SetLayoutDirty();
		}

		// Token: 0x060003AE RID: 942 RVA: 0x000208A0 File Offset: 0x0001EAA0
		public void SetCharArray(char[] sourceText, int start, int length)
		{
			if (this.m_char_buffer == null)
			{
				this.m_char_buffer = new int[8];
			}
			this.m_styleStack.Clear();
			int num = 0;
			int i = start;
			int num2 = start + length;
			while (i < num2)
			{
				if (sourceText[i] != '\\' || i >= length - 1)
				{
					goto IL_E3;
				}
				int num3 = (int)sourceText[i + 1];
				if (num3 != 110)
				{
					if (num3 != 114)
					{
						if (num3 != 116)
						{
							goto IL_E3;
						}
						if (num == this.m_char_buffer.Length)
						{
							this.ResizeInternalArray<int>(ref this.m_char_buffer);
						}
						this.m_char_buffer[num] = 9;
						i++;
						num++;
					}
					else
					{
						if (num == this.m_char_buffer.Length)
						{
							this.ResizeInternalArray<int>(ref this.m_char_buffer);
						}
						this.m_char_buffer[num] = 13;
						i++;
						num++;
					}
				}
				else
				{
					if (num == this.m_char_buffer.Length)
					{
						this.ResizeInternalArray<int>(ref this.m_char_buffer);
					}
					this.m_char_buffer[num] = 10;
					i++;
					num++;
				}
				IL_1A3:
				i++;
				continue;
				IL_E3:
				if (sourceText[i] == '<')
				{
					if (this.IsTagName(ref sourceText, "<BR>", i))
					{
						if (num == this.m_char_buffer.Length)
						{
							this.ResizeInternalArray<int>(ref this.m_char_buffer);
						}
						this.m_char_buffer[num] = 10;
						num++;
						i += 3;
						goto IL_1A3;
					}
					if (this.IsTagName(ref sourceText, "<STYLE=", i))
					{
						int num4 = 0;
						if (this.ReplaceOpeningStyleTag(ref sourceText, i, out num4, ref this.m_char_buffer, ref num))
						{
							i = num4;
							goto IL_1A3;
						}
					}
					else if (this.IsTagName(ref sourceText, "</STYLE>", i))
					{
						this.ReplaceClosingStyleTag(ref sourceText, i, ref this.m_char_buffer, ref num);
						i += 7;
						goto IL_1A3;
					}
				}
				if (num == this.m_char_buffer.Length)
				{
					this.ResizeInternalArray<int>(ref this.m_char_buffer);
				}
				this.m_char_buffer[num] = (int)sourceText[i];
				num++;
				goto IL_1A3;
			}
			if (num == this.m_char_buffer.Length)
			{
				this.ResizeInternalArray<int>(ref this.m_char_buffer);
			}
			this.m_char_buffer[num] = 0;
			this.m_inputSource = TMP_Text.TextInputSources.SetCharArray;
			this.m_havePropertiesChanged = true;
			this.m_isInputParsingRequired = true;
			this.m_isCalculateSizeRequired = true;
			this.SetVerticesDirty();
			this.SetLayoutDirty();
		}

		// Token: 0x060003AF RID: 943 RVA: 0x00020AA4 File Offset: 0x0001ECA4
		public void SetCharArray(int[] sourceText, int start, int length)
		{
			if (this.m_char_buffer == null)
			{
				this.m_char_buffer = new int[8];
			}
			this.m_styleStack.Clear();
			int num = 0;
			int num2 = start + length;
			int i = start;
			while (i < num2)
			{
				if (sourceText[i] != 92 || i >= length - 1)
				{
					goto IL_E3;
				}
				int num3 = sourceText[i + 1];
				if (num3 != 110)
				{
					if (num3 != 114)
					{
						if (num3 != 116)
						{
							goto IL_E3;
						}
						if (num == this.m_char_buffer.Length)
						{
							this.ResizeInternalArray<int>(ref this.m_char_buffer);
						}
						this.m_char_buffer[num] = 9;
						i++;
						num++;
					}
					else
					{
						if (num == this.m_char_buffer.Length)
						{
							this.ResizeInternalArray<int>(ref this.m_char_buffer);
						}
						this.m_char_buffer[num] = 13;
						i++;
						num++;
					}
				}
				else
				{
					if (num == this.m_char_buffer.Length)
					{
						this.ResizeInternalArray<int>(ref this.m_char_buffer);
					}
					this.m_char_buffer[num] = 10;
					i++;
					num++;
				}
				IL_1A3:
				i++;
				continue;
				IL_E3:
				if (sourceText[i] == 60)
				{
					if (this.IsTagName(ref sourceText, "<BR>", i))
					{
						if (num == this.m_char_buffer.Length)
						{
							this.ResizeInternalArray<int>(ref this.m_char_buffer);
						}
						this.m_char_buffer[num] = 10;
						num++;
						i += 3;
						goto IL_1A3;
					}
					if (this.IsTagName(ref sourceText, "<STYLE=", i))
					{
						int num4 = 0;
						if (this.ReplaceOpeningStyleTag(ref sourceText, i, out num4, ref this.m_char_buffer, ref num))
						{
							i = num4;
							goto IL_1A3;
						}
					}
					else if (this.IsTagName(ref sourceText, "</STYLE>", i))
					{
						this.ReplaceClosingStyleTag(ref sourceText, i, ref this.m_char_buffer, ref num);
						i += 7;
						goto IL_1A3;
					}
				}
				if (num == this.m_char_buffer.Length)
				{
					this.ResizeInternalArray<int>(ref this.m_char_buffer);
				}
				this.m_char_buffer[num] = sourceText[i];
				num++;
				goto IL_1A3;
			}
			if (num == this.m_char_buffer.Length)
			{
				this.ResizeInternalArray<int>(ref this.m_char_buffer);
			}
			this.m_char_buffer[num] = 0;
			this.m_inputSource = TMP_Text.TextInputSources.SetCharArray;
			this.m_havePropertiesChanged = true;
			this.m_isInputParsingRequired = true;
			this.m_isCalculateSizeRequired = true;
			this.SetVerticesDirty();
			this.SetLayoutDirty();
		}

		// Token: 0x060003B0 RID: 944 RVA: 0x00020CA8 File Offset: 0x0001EEA8
		protected void SetTextArrayToCharArray(char[] sourceText, ref int[] charBuffer)
		{
			if (sourceText == null || this.m_charArray_Length == 0)
			{
				return;
			}
			if (charBuffer == null)
			{
				charBuffer = new int[8];
			}
			this.m_styleStack.Clear();
			int num = 0;
			for (int i = 0; i < this.m_charArray_Length; i++)
			{
				if (char.IsHighSurrogate(sourceText[i]) && char.IsLowSurrogate(sourceText[i + 1]))
				{
					if (num == charBuffer.Length)
					{
						this.ResizeInternalArray<int>(ref charBuffer);
					}
					charBuffer[num] = char.ConvertToUtf32(sourceText[i], sourceText[i + 1]);
					i++;
					num++;
				}
				else
				{
					if (sourceText[i] == '<')
					{
						if (this.IsTagName(ref sourceText, "<BR>", i))
						{
							if (num == charBuffer.Length)
							{
								this.ResizeInternalArray<int>(ref charBuffer);
							}
							charBuffer[num] = 10;
							num++;
							i += 3;
							goto IL_105;
						}
						if (this.IsTagName(ref sourceText, "<STYLE=", i))
						{
							int num2 = 0;
							if (this.ReplaceOpeningStyleTag(ref sourceText, i, out num2, ref charBuffer, ref num))
							{
								i = num2;
								goto IL_105;
							}
						}
						else if (this.IsTagName(ref sourceText, "</STYLE>", i))
						{
							this.ReplaceClosingStyleTag(ref sourceText, i, ref charBuffer, ref num);
							i += 7;
							goto IL_105;
						}
					}
					if (num == charBuffer.Length)
					{
						this.ResizeInternalArray<int>(ref charBuffer);
					}
					charBuffer[num] = (int)sourceText[i];
					num++;
				}
				IL_105:;
			}
			if (num == charBuffer.Length)
			{
				this.ResizeInternalArray<int>(ref charBuffer);
			}
			charBuffer[num] = 0;
		}

		// Token: 0x060003B1 RID: 945 RVA: 0x00020DE0 File Offset: 0x0001EFE0
		protected void StringToCharArray(string sourceText, ref int[] charBuffer)
		{
			if (sourceText == null)
			{
				charBuffer[0] = 0;
				return;
			}
			if (charBuffer == null)
			{
				charBuffer = new int[8];
			}
			this.m_styleStack.SetDefault(0);
			int num = 0;
			int i = 0;
			while (i < sourceText.Length)
			{
				if (this.m_inputSource != TMP_Text.TextInputSources.Text || sourceText[i] != '\\' || sourceText.Length <= i + 1)
				{
					goto IL_1D3;
				}
				int num2 = (int)sourceText[i + 1];
				if (num2 <= 92)
				{
					if (num2 != 85)
					{
						if (num2 != 92)
						{
							goto IL_1D3;
						}
						if (!this.m_parseCtrlCharacters || sourceText.Length <= i + 2)
						{
							goto IL_1D3;
						}
						if (num + 2 > charBuffer.Length)
						{
							this.ResizeInternalArray<int>(ref charBuffer);
						}
						charBuffer[num] = (int)sourceText[i + 1];
						charBuffer[num + 1] = (int)sourceText[i + 2];
						i += 2;
						num += 2;
					}
					else
					{
						if (sourceText.Length <= i + 9)
						{
							goto IL_1D3;
						}
						if (num == charBuffer.Length)
						{
							this.ResizeInternalArray<int>(ref charBuffer);
						}
						charBuffer[num] = this.GetUTF32(sourceText, i + 2);
						i += 9;
						num++;
					}
				}
				else if (num2 != 110)
				{
					switch (num2)
					{
					case 114:
						if (!this.m_parseCtrlCharacters)
						{
							goto IL_1D3;
						}
						if (num == charBuffer.Length)
						{
							this.ResizeInternalArray<int>(ref charBuffer);
						}
						charBuffer[num] = 13;
						i++;
						num++;
						break;
					case 115:
						goto IL_1D3;
					case 116:
						if (!this.m_parseCtrlCharacters)
						{
							goto IL_1D3;
						}
						if (num == charBuffer.Length)
						{
							this.ResizeInternalArray<int>(ref charBuffer);
						}
						charBuffer[num] = 9;
						i++;
						num++;
						break;
					case 117:
						if (sourceText.Length <= i + 5)
						{
							goto IL_1D3;
						}
						if (num == charBuffer.Length)
						{
							this.ResizeInternalArray<int>(ref charBuffer);
						}
						charBuffer[num] = (int)((ushort)this.GetUTF16(sourceText, i + 2));
						i += 5;
						num++;
						break;
					default:
						goto IL_1D3;
					}
				}
				else
				{
					if (!this.m_parseCtrlCharacters)
					{
						goto IL_1D3;
					}
					if (num == charBuffer.Length)
					{
						this.ResizeInternalArray<int>(ref charBuffer);
					}
					charBuffer[num] = 10;
					i++;
					num++;
				}
				IL_2CC:
				i++;
				continue;
				IL_1D3:
				if (char.IsHighSurrogate(sourceText[i]) && char.IsLowSurrogate(sourceText[i + 1]))
				{
					if (num == charBuffer.Length)
					{
						this.ResizeInternalArray<int>(ref charBuffer);
					}
					charBuffer[num] = char.ConvertToUtf32(sourceText[i], sourceText[i + 1]);
					i++;
					num++;
					goto IL_2CC;
				}
				if (sourceText[i] == '<' && this.m_isRichText)
				{
					if (this.IsTagName(ref sourceText, "<BR>", i))
					{
						if (num == charBuffer.Length)
						{
							this.ResizeInternalArray<int>(ref charBuffer);
						}
						charBuffer[num] = 10;
						num++;
						i += 3;
						goto IL_2CC;
					}
					if (this.IsTagName(ref sourceText, "<STYLE=", i))
					{
						int num3 = 0;
						if (this.ReplaceOpeningStyleTag(ref sourceText, i, out num3, ref charBuffer, ref num))
						{
							i = num3;
							goto IL_2CC;
						}
					}
					else if (this.IsTagName(ref sourceText, "</STYLE>", i))
					{
						this.ReplaceClosingStyleTag(ref sourceText, i, ref charBuffer, ref num);
						i += 7;
						goto IL_2CC;
					}
				}
				if (num == charBuffer.Length)
				{
					this.ResizeInternalArray<int>(ref charBuffer);
				}
				charBuffer[num] = (int)sourceText[i];
				num++;
				goto IL_2CC;
			}
			if (num == charBuffer.Length)
			{
				this.ResizeInternalArray<int>(ref charBuffer);
			}
			charBuffer[num] = 0;
		}

		// Token: 0x060003B2 RID: 946 RVA: 0x000210DC File Offset: 0x0001F2DC
		protected void StringBuilderToIntArray(StringBuilder sourceText, ref int[] charBuffer)
		{
			if (sourceText == null)
			{
				charBuffer[0] = 0;
				return;
			}
			if (charBuffer == null)
			{
				charBuffer = new int[8];
			}
			this.m_styleStack.Clear();
			int num = 0;
			int i = 0;
			while (i < sourceText.Length)
			{
				if (!this.m_parseCtrlCharacters || sourceText[i] != '\\' || sourceText.Length <= i + 1)
				{
					goto IL_1AC;
				}
				int num2 = (int)sourceText[i + 1];
				if (num2 <= 92)
				{
					if (num2 != 85)
					{
						if (num2 != 92)
						{
							goto IL_1AC;
						}
						if (sourceText.Length <= i + 2)
						{
							goto IL_1AC;
						}
						if (num + 2 > charBuffer.Length)
						{
							this.ResizeInternalArray<int>(ref charBuffer);
						}
						charBuffer[num] = (int)sourceText[i + 1];
						charBuffer[num + 1] = (int)sourceText[i + 2];
						i += 2;
						num += 2;
					}
					else
					{
						if (sourceText.Length <= i + 9)
						{
							goto IL_1AC;
						}
						if (num == charBuffer.Length)
						{
							this.ResizeInternalArray<int>(ref charBuffer);
						}
						charBuffer[num] = this.GetUTF32(sourceText, i + 2);
						i += 9;
						num++;
					}
				}
				else if (num2 != 110)
				{
					switch (num2)
					{
					case 114:
						if (num == charBuffer.Length)
						{
							this.ResizeInternalArray<int>(ref charBuffer);
						}
						charBuffer[num] = 13;
						i++;
						num++;
						break;
					case 115:
						goto IL_1AC;
					case 116:
						if (num == charBuffer.Length)
						{
							this.ResizeInternalArray<int>(ref charBuffer);
						}
						charBuffer[num] = 9;
						i++;
						num++;
						break;
					case 117:
						if (sourceText.Length <= i + 5)
						{
							goto IL_1AC;
						}
						if (num == charBuffer.Length)
						{
							this.ResizeInternalArray<int>(ref charBuffer);
						}
						charBuffer[num] = (int)((ushort)this.GetUTF16(sourceText, i + 2));
						i += 5;
						num++;
						break;
					default:
						goto IL_1AC;
					}
				}
				else
				{
					if (num == charBuffer.Length)
					{
						this.ResizeInternalArray<int>(ref charBuffer);
					}
					charBuffer[num] = 10;
					i++;
					num++;
				}
				IL_29D:
				i++;
				continue;
				IL_1AC:
				if (char.IsHighSurrogate(sourceText[i]) && char.IsLowSurrogate(sourceText[i + 1]))
				{
					if (num == charBuffer.Length)
					{
						this.ResizeInternalArray<int>(ref charBuffer);
					}
					charBuffer[num] = char.ConvertToUtf32(sourceText[i], sourceText[i + 1]);
					i++;
					num++;
					goto IL_29D;
				}
				if (sourceText[i] == '<')
				{
					if (this.IsTagName(ref sourceText, "<BR>", i))
					{
						if (num == charBuffer.Length)
						{
							this.ResizeInternalArray<int>(ref charBuffer);
						}
						charBuffer[num] = 10;
						num++;
						i += 3;
						goto IL_29D;
					}
					if (this.IsTagName(ref sourceText, "<STYLE=", i))
					{
						int num3 = 0;
						if (this.ReplaceOpeningStyleTag(ref sourceText, i, out num3, ref charBuffer, ref num))
						{
							i = num3;
							goto IL_29D;
						}
					}
					else if (this.IsTagName(ref sourceText, "</STYLE>", i))
					{
						this.ReplaceClosingStyleTag(ref sourceText, i, ref charBuffer, ref num);
						i += 7;
						goto IL_29D;
					}
				}
				if (num == charBuffer.Length)
				{
					this.ResizeInternalArray<int>(ref charBuffer);
				}
				charBuffer[num] = (int)sourceText[i];
				num++;
				goto IL_29D;
			}
			if (num == charBuffer.Length)
			{
				this.ResizeInternalArray<int>(ref charBuffer);
			}
			charBuffer[num] = 0;
		}

		// Token: 0x060003B3 RID: 947 RVA: 0x000213AC File Offset: 0x0001F5AC
		private bool ReplaceOpeningStyleTag(ref string sourceText, int srcIndex, out int srcOffset, ref int[] charBuffer, ref int writeIndex)
		{
			TMP_Style style = TMP_StyleSheet.GetStyle(this.GetTagHashCode(ref sourceText, srcIndex + 7, out srcOffset));
			if (style == null || srcOffset == 0)
			{
				return false;
			}
			this.m_styleStack.Add(style.hashCode);
			int num = style.styleOpeningTagArray.Length;
			int[] styleOpeningTagArray = style.styleOpeningTagArray;
			int i = 0;
			while (i < num)
			{
				int num2 = styleOpeningTagArray[i];
				if (num2 != 60)
				{
					goto IL_D6;
				}
				if (this.IsTagName(ref styleOpeningTagArray, "<BR>", i))
				{
					if (writeIndex == charBuffer.Length)
					{
						this.ResizeInternalArray<int>(ref charBuffer);
					}
					charBuffer[writeIndex] = 10;
					writeIndex++;
					i += 3;
				}
				else if (this.IsTagName(ref styleOpeningTagArray, "<STYLE=", i))
				{
					int num3 = 0;
					if (!this.ReplaceOpeningStyleTag(ref styleOpeningTagArray, i, out num3, ref charBuffer, ref writeIndex))
					{
						goto IL_D6;
					}
					i = num3;
				}
				else
				{
					if (!this.IsTagName(ref styleOpeningTagArray, "</STYLE>", i))
					{
						goto IL_D6;
					}
					this.ReplaceClosingStyleTag(ref styleOpeningTagArray, i, ref charBuffer, ref writeIndex);
					i += 7;
				}
				IL_F9:
				i++;
				continue;
				IL_D6:
				if (writeIndex == charBuffer.Length)
				{
					this.ResizeInternalArray<int>(ref charBuffer);
				}
				charBuffer[writeIndex] = num2;
				writeIndex++;
				goto IL_F9;
			}
			return true;
		}

		// Token: 0x060003B4 RID: 948 RVA: 0x000214C0 File Offset: 0x0001F6C0
		private bool ReplaceOpeningStyleTag(ref int[] sourceText, int srcIndex, out int srcOffset, ref int[] charBuffer, ref int writeIndex)
		{
			TMP_Style style = TMP_StyleSheet.GetStyle(this.GetTagHashCode(ref sourceText, srcIndex + 7, out srcOffset));
			if (style == null || srcOffset == 0)
			{
				return false;
			}
			this.m_styleStack.Add(style.hashCode);
			int num = style.styleOpeningTagArray.Length;
			int[] styleOpeningTagArray = style.styleOpeningTagArray;
			int i = 0;
			while (i < num)
			{
				int num2 = styleOpeningTagArray[i];
				if (num2 != 60)
				{
					goto IL_D6;
				}
				if (this.IsTagName(ref styleOpeningTagArray, "<BR>", i))
				{
					if (writeIndex == charBuffer.Length)
					{
						this.ResizeInternalArray<int>(ref charBuffer);
					}
					charBuffer[writeIndex] = 10;
					writeIndex++;
					i += 3;
				}
				else if (this.IsTagName(ref styleOpeningTagArray, "<STYLE=", i))
				{
					int num3 = 0;
					if (!this.ReplaceOpeningStyleTag(ref styleOpeningTagArray, i, out num3, ref charBuffer, ref writeIndex))
					{
						goto IL_D6;
					}
					i = num3;
				}
				else
				{
					if (!this.IsTagName(ref styleOpeningTagArray, "</STYLE>", i))
					{
						goto IL_D6;
					}
					this.ReplaceClosingStyleTag(ref styleOpeningTagArray, i, ref charBuffer, ref writeIndex);
					i += 7;
				}
				IL_F9:
				i++;
				continue;
				IL_D6:
				if (writeIndex == charBuffer.Length)
				{
					this.ResizeInternalArray<int>(ref charBuffer);
				}
				charBuffer[writeIndex] = num2;
				writeIndex++;
				goto IL_F9;
			}
			return true;
		}

		// Token: 0x060003B5 RID: 949 RVA: 0x000215D4 File Offset: 0x0001F7D4
		private bool ReplaceOpeningStyleTag(ref char[] sourceText, int srcIndex, out int srcOffset, ref int[] charBuffer, ref int writeIndex)
		{
			TMP_Style style = TMP_StyleSheet.GetStyle(this.GetTagHashCode(ref sourceText, srcIndex + 7, out srcOffset));
			if (style == null || srcOffset == 0)
			{
				return false;
			}
			this.m_styleStack.Add(style.hashCode);
			int num = style.styleOpeningTagArray.Length;
			int[] styleOpeningTagArray = style.styleOpeningTagArray;
			int i = 0;
			while (i < num)
			{
				int num2 = styleOpeningTagArray[i];
				if (num2 != 60)
				{
					goto IL_D6;
				}
				if (this.IsTagName(ref styleOpeningTagArray, "<BR>", i))
				{
					if (writeIndex == charBuffer.Length)
					{
						this.ResizeInternalArray<int>(ref charBuffer);
					}
					charBuffer[writeIndex] = 10;
					writeIndex++;
					i += 3;
				}
				else if (this.IsTagName(ref styleOpeningTagArray, "<STYLE=", i))
				{
					int num3 = 0;
					if (!this.ReplaceOpeningStyleTag(ref styleOpeningTagArray, i, out num3, ref charBuffer, ref writeIndex))
					{
						goto IL_D6;
					}
					i = num3;
				}
				else
				{
					if (!this.IsTagName(ref styleOpeningTagArray, "</STYLE>", i))
					{
						goto IL_D6;
					}
					this.ReplaceClosingStyleTag(ref styleOpeningTagArray, i, ref charBuffer, ref writeIndex);
					i += 7;
				}
				IL_F9:
				i++;
				continue;
				IL_D6:
				if (writeIndex == charBuffer.Length)
				{
					this.ResizeInternalArray<int>(ref charBuffer);
				}
				charBuffer[writeIndex] = num2;
				writeIndex++;
				goto IL_F9;
			}
			return true;
		}

		// Token: 0x060003B6 RID: 950 RVA: 0x000216E8 File Offset: 0x0001F8E8
		private bool ReplaceOpeningStyleTag(ref StringBuilder sourceText, int srcIndex, out int srcOffset, ref int[] charBuffer, ref int writeIndex)
		{
			TMP_Style style = TMP_StyleSheet.GetStyle(this.GetTagHashCode(ref sourceText, srcIndex + 7, out srcOffset));
			if (style == null || srcOffset == 0)
			{
				return false;
			}
			this.m_styleStack.Add(style.hashCode);
			int num = style.styleOpeningTagArray.Length;
			int[] styleOpeningTagArray = style.styleOpeningTagArray;
			int i = 0;
			while (i < num)
			{
				int num2 = styleOpeningTagArray[i];
				if (num2 != 60)
				{
					goto IL_D6;
				}
				if (this.IsTagName(ref styleOpeningTagArray, "<BR>", i))
				{
					if (writeIndex == charBuffer.Length)
					{
						this.ResizeInternalArray<int>(ref charBuffer);
					}
					charBuffer[writeIndex] = 10;
					writeIndex++;
					i += 3;
				}
				else if (this.IsTagName(ref styleOpeningTagArray, "<STYLE=", i))
				{
					int num3 = 0;
					if (!this.ReplaceOpeningStyleTag(ref styleOpeningTagArray, i, out num3, ref charBuffer, ref writeIndex))
					{
						goto IL_D6;
					}
					i = num3;
				}
				else
				{
					if (!this.IsTagName(ref styleOpeningTagArray, "</STYLE>", i))
					{
						goto IL_D6;
					}
					this.ReplaceClosingStyleTag(ref styleOpeningTagArray, i, ref charBuffer, ref writeIndex);
					i += 7;
				}
				IL_F9:
				i++;
				continue;
				IL_D6:
				if (writeIndex == charBuffer.Length)
				{
					this.ResizeInternalArray<int>(ref charBuffer);
				}
				charBuffer[writeIndex] = num2;
				writeIndex++;
				goto IL_F9;
			}
			return true;
		}

		// Token: 0x060003B7 RID: 951 RVA: 0x000217FC File Offset: 0x0001F9FC
		private bool ReplaceClosingStyleTag(ref string sourceText, int srcIndex, ref int[] charBuffer, ref int writeIndex)
		{
			TMP_Style style = TMP_StyleSheet.GetStyle(this.m_styleStack.CurrentItem());
			this.m_styleStack.Remove();
			if (style == null)
			{
				return false;
			}
			int num = style.styleClosingTagArray.Length;
			int[] styleClosingTagArray = style.styleClosingTagArray;
			int i = 0;
			while (i < num)
			{
				int num2 = styleClosingTagArray[i];
				if (num2 != 60)
				{
					goto IL_C8;
				}
				if (this.IsTagName(ref styleClosingTagArray, "<BR>", i))
				{
					if (writeIndex == charBuffer.Length)
					{
						this.ResizeInternalArray<int>(ref charBuffer);
					}
					charBuffer[writeIndex] = 10;
					writeIndex++;
					i += 3;
				}
				else if (this.IsTagName(ref styleClosingTagArray, "<STYLE=", i))
				{
					int num3 = 0;
					if (!this.ReplaceOpeningStyleTag(ref styleClosingTagArray, i, out num3, ref charBuffer, ref writeIndex))
					{
						goto IL_C8;
					}
					i = num3;
				}
				else
				{
					if (!this.IsTagName(ref styleClosingTagArray, "</STYLE>", i))
					{
						goto IL_C8;
					}
					this.ReplaceClosingStyleTag(ref styleClosingTagArray, i, ref charBuffer, ref writeIndex);
					i += 7;
				}
				IL_E8:
				i++;
				continue;
				IL_C8:
				if (writeIndex == charBuffer.Length)
				{
					this.ResizeInternalArray<int>(ref charBuffer);
				}
				charBuffer[writeIndex] = num2;
				writeIndex++;
				goto IL_E8;
			}
			return true;
		}

		// Token: 0x060003B8 RID: 952 RVA: 0x00021900 File Offset: 0x0001FB00
		private bool ReplaceClosingStyleTag(ref int[] sourceText, int srcIndex, ref int[] charBuffer, ref int writeIndex)
		{
			TMP_Style style = TMP_StyleSheet.GetStyle(this.m_styleStack.CurrentItem());
			this.m_styleStack.Remove();
			if (style == null)
			{
				return false;
			}
			int num = style.styleClosingTagArray.Length;
			int[] styleClosingTagArray = style.styleClosingTagArray;
			int i = 0;
			while (i < num)
			{
				int num2 = styleClosingTagArray[i];
				if (num2 != 60)
				{
					goto IL_C8;
				}
				if (this.IsTagName(ref styleClosingTagArray, "<BR>", i))
				{
					if (writeIndex == charBuffer.Length)
					{
						this.ResizeInternalArray<int>(ref charBuffer);
					}
					charBuffer[writeIndex] = 10;
					writeIndex++;
					i += 3;
				}
				else if (this.IsTagName(ref styleClosingTagArray, "<STYLE=", i))
				{
					int num3 = 0;
					if (!this.ReplaceOpeningStyleTag(ref styleClosingTagArray, i, out num3, ref charBuffer, ref writeIndex))
					{
						goto IL_C8;
					}
					i = num3;
				}
				else
				{
					if (!this.IsTagName(ref styleClosingTagArray, "</STYLE>", i))
					{
						goto IL_C8;
					}
					this.ReplaceClosingStyleTag(ref styleClosingTagArray, i, ref charBuffer, ref writeIndex);
					i += 7;
				}
				IL_E8:
				i++;
				continue;
				IL_C8:
				if (writeIndex == charBuffer.Length)
				{
					this.ResizeInternalArray<int>(ref charBuffer);
				}
				charBuffer[writeIndex] = num2;
				writeIndex++;
				goto IL_E8;
			}
			return true;
		}

		// Token: 0x060003B9 RID: 953 RVA: 0x00021A04 File Offset: 0x0001FC04
		private bool ReplaceClosingStyleTag(ref char[] sourceText, int srcIndex, ref int[] charBuffer, ref int writeIndex)
		{
			TMP_Style style = TMP_StyleSheet.GetStyle(this.m_styleStack.CurrentItem());
			this.m_styleStack.Remove();
			if (style == null)
			{
				return false;
			}
			int num = style.styleClosingTagArray.Length;
			int[] styleClosingTagArray = style.styleClosingTagArray;
			int i = 0;
			while (i < num)
			{
				int num2 = styleClosingTagArray[i];
				if (num2 != 60)
				{
					goto IL_C8;
				}
				if (this.IsTagName(ref styleClosingTagArray, "<BR>", i))
				{
					if (writeIndex == charBuffer.Length)
					{
						this.ResizeInternalArray<int>(ref charBuffer);
					}
					charBuffer[writeIndex] = 10;
					writeIndex++;
					i += 3;
				}
				else if (this.IsTagName(ref styleClosingTagArray, "<STYLE=", i))
				{
					int num3 = 0;
					if (!this.ReplaceOpeningStyleTag(ref styleClosingTagArray, i, out num3, ref charBuffer, ref writeIndex))
					{
						goto IL_C8;
					}
					i = num3;
				}
				else
				{
					if (!this.IsTagName(ref styleClosingTagArray, "</STYLE>", i))
					{
						goto IL_C8;
					}
					this.ReplaceClosingStyleTag(ref styleClosingTagArray, i, ref charBuffer, ref writeIndex);
					i += 7;
				}
				IL_E8:
				i++;
				continue;
				IL_C8:
				if (writeIndex == charBuffer.Length)
				{
					this.ResizeInternalArray<int>(ref charBuffer);
				}
				charBuffer[writeIndex] = num2;
				writeIndex++;
				goto IL_E8;
			}
			return true;
		}

		// Token: 0x060003BA RID: 954 RVA: 0x00021B08 File Offset: 0x0001FD08
		private bool ReplaceClosingStyleTag(ref StringBuilder sourceText, int srcIndex, ref int[] charBuffer, ref int writeIndex)
		{
			TMP_Style style = TMP_StyleSheet.GetStyle(this.m_styleStack.CurrentItem());
			this.m_styleStack.Remove();
			if (style == null)
			{
				return false;
			}
			int num = style.styleClosingTagArray.Length;
			int[] styleClosingTagArray = style.styleClosingTagArray;
			int i = 0;
			while (i < num)
			{
				int num2 = styleClosingTagArray[i];
				if (num2 != 60)
				{
					goto IL_C8;
				}
				if (this.IsTagName(ref styleClosingTagArray, "<BR>", i))
				{
					if (writeIndex == charBuffer.Length)
					{
						this.ResizeInternalArray<int>(ref charBuffer);
					}
					charBuffer[writeIndex] = 10;
					writeIndex++;
					i += 3;
				}
				else if (this.IsTagName(ref styleClosingTagArray, "<STYLE=", i))
				{
					int num3 = 0;
					if (!this.ReplaceOpeningStyleTag(ref styleClosingTagArray, i, out num3, ref charBuffer, ref writeIndex))
					{
						goto IL_C8;
					}
					i = num3;
				}
				else
				{
					if (!this.IsTagName(ref styleClosingTagArray, "</STYLE>", i))
					{
						goto IL_C8;
					}
					this.ReplaceClosingStyleTag(ref styleClosingTagArray, i, ref charBuffer, ref writeIndex);
					i += 7;
				}
				IL_E8:
				i++;
				continue;
				IL_C8:
				if (writeIndex == charBuffer.Length)
				{
					this.ResizeInternalArray<int>(ref charBuffer);
				}
				charBuffer[writeIndex] = num2;
				writeIndex++;
				goto IL_E8;
			}
			return true;
		}

		// Token: 0x060003BB RID: 955 RVA: 0x00021C0C File Offset: 0x0001FE0C
		private bool IsTagName(ref string text, string tag, int index)
		{
			if (text.Length < index + tag.Length)
			{
				return false;
			}
			for (int i = 0; i < tag.Length; i++)
			{
				if (TMP_TextUtilities.ToUpperFast(text[index + i]) != tag[i])
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x060003BC RID: 956 RVA: 0x00021C58 File Offset: 0x0001FE58
		private bool IsTagName(ref char[] text, string tag, int index)
		{
			if (text.Length < index + tag.Length)
			{
				return false;
			}
			for (int i = 0; i < tag.Length; i++)
			{
				if (TMP_TextUtilities.ToUpperFast(text[index + i]) != tag[i])
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x060003BD RID: 957 RVA: 0x00021CA0 File Offset: 0x0001FEA0
		private bool IsTagName(ref int[] text, string tag, int index)
		{
			if (text.Length < index + tag.Length)
			{
				return false;
			}
			for (int i = 0; i < tag.Length; i++)
			{
				if (TMP_TextUtilities.ToUpperFast((char)text[index + i]) != tag[i])
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x060003BE RID: 958 RVA: 0x00021CE8 File Offset: 0x0001FEE8
		private bool IsTagName(ref StringBuilder text, string tag, int index)
		{
			if (text.Length < index + tag.Length)
			{
				return false;
			}
			for (int i = 0; i < tag.Length; i++)
			{
				if (TMP_TextUtilities.ToUpperFast(text[index + i]) != tag[i])
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x060003BF RID: 959 RVA: 0x00021D34 File Offset: 0x0001FF34
		private int GetTagHashCode(ref string text, int index, out int closeIndex)
		{
			int num = 0;
			closeIndex = 0;
			for (int i = index; i < text.Length; i++)
			{
				if (text[i] != '"')
				{
					if (text[i] == '>')
					{
						closeIndex = i;
						break;
					}
					num = ((num << 5) + num ^ (int)text[i]);
				}
			}
			return num;
		}

		// Token: 0x060003C0 RID: 960 RVA: 0x00021D88 File Offset: 0x0001FF88
		private int GetTagHashCode(ref char[] text, int index, out int closeIndex)
		{
			int num = 0;
			closeIndex = 0;
			for (int i = index; i < text.Length; i++)
			{
				if (text[i] != '"')
				{
					if (text[i] == '>')
					{
						closeIndex = i;
						break;
					}
					num = ((num << 5) + num ^ (int)text[i]);
				}
			}
			return num;
		}

		// Token: 0x060003C1 RID: 961 RVA: 0x00021DCC File Offset: 0x0001FFCC
		private int GetTagHashCode(ref int[] text, int index, out int closeIndex)
		{
			int num = 0;
			closeIndex = 0;
			for (int i = index; i < text.Length; i++)
			{
				if (text[i] != 34)
				{
					if (text[i] == 62)
					{
						closeIndex = i;
						break;
					}
					num = ((num << 5) + num ^ text[i]);
				}
			}
			return num;
		}

		// Token: 0x060003C2 RID: 962 RVA: 0x00021E10 File Offset: 0x00020010
		private int GetTagHashCode(ref StringBuilder text, int index, out int closeIndex)
		{
			int num = 0;
			closeIndex = 0;
			for (int i = index; i < text.Length; i++)
			{
				if (text[i] != '"')
				{
					if (text[i] == '>')
					{
						closeIndex = i;
						break;
					}
					num = ((num << 5) + num ^ (int)text[i]);
				}
			}
			return num;
		}

		// Token: 0x060003C3 RID: 963 RVA: 0x00021E64 File Offset: 0x00020064
		private void ResizeInternalArray<T>(ref T[] array)
		{
			int newSize = Mathf.NextPowerOfTwo(array.Length + 1);
			Array.Resize<T>(ref array, newSize);
		}

		// Token: 0x060003C4 RID: 964 RVA: 0x00021E84 File Offset: 0x00020084
		protected void AddFloatToCharArray(float number, ref int index, int precision)
		{
			if (number < 0f)
			{
				char[] input_CharArray = this.m_input_CharArray;
				int num = index;
				index = num + 1;
				input_CharArray[num] = 45;
				number = -number;
			}
			number += this.k_Power[Mathf.Min(9, precision)];
			int num2 = (int)number;
			this.AddIntToCharArray(num2, ref index, precision);
			if (precision > 0)
			{
				char[] input_CharArray2 = this.m_input_CharArray;
				int num = index;
				index = num + 1;
				input_CharArray2[num] = 46;
				number -= (float)num2;
				for (int i = 0; i < precision; i++)
				{
					number *= 10f;
					int num3 = (int)number;
					char[] input_CharArray3 = this.m_input_CharArray;
					num = index;
					index = num + 1;
					input_CharArray3[num] = (ushort)(num3 + 48);
					number -= (float)num3;
				}
			}
		}

		// Token: 0x060003C5 RID: 965 RVA: 0x00021F20 File Offset: 0x00020120
		protected void AddIntToCharArray(int number, ref int index, int precision)
		{
			if (number < 0)
			{
				char[] input_CharArray = this.m_input_CharArray;
				int num = index;
				index = num + 1;
				input_CharArray[num] = 45;
				number = -number;
			}
			int num2 = index;
			do
			{
				this.m_input_CharArray[num2++] = (char)(number % 10 + 48);
				number /= 10;
			}
			while (number > 0);
			int num3 = num2;
			while (index + 1 < num2)
			{
				num2--;
				char c = this.m_input_CharArray[index];
				this.m_input_CharArray[index] = this.m_input_CharArray[num2];
				this.m_input_CharArray[num2] = c;
				index++;
			}
			index = num3;
		}

		// Token: 0x060003C6 RID: 966 RVA: 0x00021FA4 File Offset: 0x000201A4
		protected virtual int SetArraySizes(int[] chars)
		{
			return 0;
		}

		// Token: 0x060003C7 RID: 967 RVA: 0x0000297D File Offset: 0x00000B7D
		protected virtual void GenerateTextMesh()
		{
		}

		// Token: 0x060003C8 RID: 968 RVA: 0x00021FA8 File Offset: 0x000201A8
		public Vector2 GetPreferredValues()
		{
			if (this.m_isInputParsingRequired || this.m_isTextTruncated)
			{
				this.m_isCalculatingPreferredValues = true;
				this.ParseInputText();
			}
			float preferredWidth = this.GetPreferredWidth();
			float preferredHeight = this.GetPreferredHeight();
			return new Vector2(preferredWidth, preferredHeight);
		}

		// Token: 0x060003C9 RID: 969 RVA: 0x00021FE8 File Offset: 0x000201E8
		public Vector2 GetPreferredValues(float width, float height)
		{
			if (this.m_isInputParsingRequired || this.m_isTextTruncated)
			{
				this.m_isCalculatingPreferredValues = true;
				this.ParseInputText();
			}
			Vector2 margin = new Vector2(width, height);
			float preferredWidth = this.GetPreferredWidth(margin);
			float preferredHeight = this.GetPreferredHeight(margin);
			return new Vector2(preferredWidth, preferredHeight);
		}

		// Token: 0x060003CA RID: 970 RVA: 0x00022030 File Offset: 0x00020230
		public Vector2 GetPreferredValues(string text)
		{
			this.m_isCalculatingPreferredValues = true;
			this.StringToCharArray(text, ref this.m_char_buffer);
			this.SetArraySizes(this.m_char_buffer);
			Vector2 margin = TMP_Text.k_LargePositiveVector2;
			float preferredWidth = this.GetPreferredWidth(margin);
			float preferredHeight = this.GetPreferredHeight(margin);
			return new Vector2(preferredWidth, preferredHeight);
		}

		// Token: 0x060003CB RID: 971 RVA: 0x0002207C File Offset: 0x0002027C
		public Vector2 GetPreferredValues(string text, float width, float height)
		{
			this.m_isCalculatingPreferredValues = true;
			this.StringToCharArray(text, ref this.m_char_buffer);
			this.SetArraySizes(this.m_char_buffer);
			Vector2 margin = new Vector2(width, height);
			float preferredWidth = this.GetPreferredWidth(margin);
			float preferredHeight = this.GetPreferredHeight(margin);
			return new Vector2(preferredWidth, preferredHeight);
		}

		// Token: 0x060003CC RID: 972 RVA: 0x000220C8 File Offset: 0x000202C8
		protected float GetPreferredWidth()
		{
			if (TMP_Settings.instance == null)
			{
				return 0f;
			}
			float defaultFontSize = this.m_enableAutoSizing ? this.m_fontSizeMax : this.m_fontSize;
			this.m_minFontSize = this.m_fontSizeMin;
			this.m_maxFontSize = this.m_fontSizeMax;
			this.m_charWidthAdjDelta = 0f;
			Vector2 marginSize = TMP_Text.k_LargePositiveVector2;
			if (this.m_isInputParsingRequired || this.m_isTextTruncated)
			{
				this.m_isCalculatingPreferredValues = true;
				this.ParseInputText();
			}
			this.m_recursiveCount = 0;
			float x = this.CalculatePreferredValues(defaultFontSize, marginSize, true).x;
			this.m_isPreferredWidthDirty = false;
			return x;
		}

		// Token: 0x060003CD RID: 973 RVA: 0x00022164 File Offset: 0x00020364
		protected float GetPreferredWidth(Vector2 margin)
		{
			float defaultFontSize = this.m_enableAutoSizing ? this.m_fontSizeMax : this.m_fontSize;
			this.m_minFontSize = this.m_fontSizeMin;
			this.m_maxFontSize = this.m_fontSizeMax;
			this.m_charWidthAdjDelta = 0f;
			this.m_recursiveCount = 0;
			return this.CalculatePreferredValues(defaultFontSize, margin, true).x;
		}

		// Token: 0x060003CE RID: 974 RVA: 0x000221C0 File Offset: 0x000203C0
		protected float GetPreferredHeight()
		{
			if (TMP_Settings.instance == null)
			{
				return 0f;
			}
			float defaultFontSize = this.m_enableAutoSizing ? this.m_fontSizeMax : this.m_fontSize;
			this.m_minFontSize = this.m_fontSizeMin;
			this.m_maxFontSize = this.m_fontSizeMax;
			this.m_charWidthAdjDelta = 0f;
			Vector2 marginSize = new Vector2((this.m_marginWidth != 0f) ? this.m_marginWidth : TMP_Text.k_LargePositiveFloat, TMP_Text.k_LargePositiveFloat);
			if (this.m_isInputParsingRequired || this.m_isTextTruncated)
			{
				this.m_isCalculatingPreferredValues = true;
				this.ParseInputText();
			}
			this.m_recursiveCount = 0;
			float y = this.CalculatePreferredValues(defaultFontSize, marginSize, !this.m_enableAutoSizing).y;
			this.m_isPreferredHeightDirty = false;
			return y;
		}

		// Token: 0x060003CF RID: 975 RVA: 0x00022284 File Offset: 0x00020484
		protected float GetPreferredHeight(Vector2 margin)
		{
			float defaultFontSize = this.m_enableAutoSizing ? this.m_fontSizeMax : this.m_fontSize;
			this.m_minFontSize = this.m_fontSizeMin;
			this.m_maxFontSize = this.m_fontSizeMax;
			this.m_charWidthAdjDelta = 0f;
			this.m_recursiveCount = 0;
			return this.CalculatePreferredValues(defaultFontSize, margin, true).y;
		}

		// Token: 0x060003D0 RID: 976 RVA: 0x000222E0 File Offset: 0x000204E0
		public Vector2 GetRenderedValues()
		{
			return this.GetTextBounds().size;
		}

		// Token: 0x060003D1 RID: 977 RVA: 0x00022300 File Offset: 0x00020500
		public Vector2 GetRenderedValues(bool onlyVisibleCharacters)
		{
			return this.GetTextBounds(onlyVisibleCharacters).size;
		}

		// Token: 0x060003D2 RID: 978 RVA: 0x00022321 File Offset: 0x00020521
		protected float GetRenderedWidth()
		{
			return this.GetRenderedValues().x;
		}

		// Token: 0x060003D3 RID: 979 RVA: 0x0002232E File Offset: 0x0002052E
		protected float GetRenderedWidth(bool onlyVisibleCharacters)
		{
			return this.GetRenderedValues(onlyVisibleCharacters).x;
		}

		// Token: 0x060003D4 RID: 980 RVA: 0x0002233C File Offset: 0x0002053C
		protected float GetRenderedHeight()
		{
			return this.GetRenderedValues().y;
		}

		// Token: 0x060003D5 RID: 981 RVA: 0x00022349 File Offset: 0x00020549
		protected float GetRenderedHeight(bool onlyVisibleCharacters)
		{
			return this.GetRenderedValues(onlyVisibleCharacters).y;
		}

		// Token: 0x060003D6 RID: 982 RVA: 0x00022358 File Offset: 0x00020558
		protected virtual Vector2 CalculatePreferredValues(float defaultFontSize, Vector2 marginSize, bool ignoreTextAutoSizing)
		{
			if (this.m_fontAsset == null || this.m_fontAsset.characterDictionary == null)
			{
				Debug.LogWarning("Can't Generate Mesh! No Font Asset has been assigned to Object ID: " + base.GetInstanceID());
				return Vector2.zero;
			}
			if (this.m_char_buffer == null || this.m_char_buffer.Length == 0 || this.m_char_buffer[0] == 0)
			{
				return Vector2.zero;
			}
			this.m_currentFontAsset = this.m_fontAsset;
			this.m_currentMaterial = this.m_sharedMaterial;
			this.m_currentMaterialIndex = 0;
			this.m_materialReferenceStack.SetDefault(new MaterialReference(0, this.m_currentFontAsset, null, this.m_currentMaterial, this.m_padding));
			int totalCharacterCount = this.m_totalCharacterCount;
			if (this.m_internalCharacterInfo == null || totalCharacterCount > this.m_internalCharacterInfo.Length)
			{
				this.m_internalCharacterInfo = new TMP_CharacterInfo[(totalCharacterCount > 1024) ? (totalCharacterCount + 256) : Mathf.NextPowerOfTwo(totalCharacterCount)];
			}
			float num = this.m_fontScale = defaultFontSize / this.m_fontAsset.fontInfo.PointSize * this.m_fontAsset.fontInfo.Scale * (this.m_isOrthographic ? 1f : 0.1f);
			float num2 = num;
			this.m_fontScaleMultiplier = 1f;
			this.m_currentFontSize = defaultFontSize;
			this.m_sizeStack.SetDefault(this.m_currentFontSize);
			this.m_style = this.m_fontStyle;
			this.m_lineJustification = this.m_textAlignment;
			this.m_lineJustificationStack.SetDefault(this.m_lineJustification);
			this.m_baselineOffset = 0f;
			this.m_baselineOffsetStack.Clear();
			this.m_lineOffset = 0f;
			this.m_lineHeight = -32767f;
			float num3 = this.m_currentFontAsset.fontInfo.LineHeight - (this.m_currentFontAsset.fontInfo.Ascender - this.m_currentFontAsset.fontInfo.Descender);
			this.m_cSpacing = 0f;
			this.m_monoSpacing = 0f;
			this.m_xAdvance = 0f;
			float a = 0f;
			this.tag_LineIndent = 0f;
			this.tag_Indent = 0f;
			this.m_indentStack.SetDefault(0f);
			this.tag_NoParsing = false;
			this.m_characterCount = 0;
			this.m_firstCharacterOfLine = 0;
			this.m_maxLineAscender = TMP_Text.k_LargeNegativeFloat;
			this.m_maxLineDescender = TMP_Text.k_LargePositiveFloat;
			this.m_lineNumber = 0;
			float x = marginSize.x;
			this.m_marginLeft = 0f;
			this.m_marginRight = 0f;
			this.m_width = -1f;
			float num4 = 0f;
			float num5 = 0f;
			float num6 = 0f;
			this.m_isCalculatingPreferredValues = true;
			this.m_maxAscender = 0f;
			this.m_maxDescender = 0f;
			bool flag = true;
			bool flag2 = false;
			WordWrapState wordWrapState = default(WordWrapState);
			this.SaveWordWrappingState(ref wordWrapState, 0, 0);
			WordWrapState wordWrapState2 = default(WordWrapState);
			int num7 = 0;
			this.m_recursiveCount++;
			int num8 = 0;
			int num9 = 0;
			while (this.m_char_buffer[num9] != 0)
			{
				int num10 = this.m_char_buffer[num9];
				if (!this.m_isRichText || num10 != 60)
				{
					this.m_textElementType = this.m_textInfo.characterInfo[this.m_characterCount].elementType;
					this.m_currentMaterialIndex = this.m_textInfo.characterInfo[this.m_characterCount].materialReferenceIndex;
					this.m_currentFontAsset = this.m_textInfo.characterInfo[this.m_characterCount].fontAsset;
					goto IL_3AA;
				}
				this.m_isParsingText = true;
				this.m_textElementType = TMP_TextElementType.Character;
				if (!this.ValidateHtmlTag(this.m_char_buffer, num9 + 1, out num8))
				{
					goto IL_3AA;
				}
				num9 = num8;
				if (this.m_textElementType != TMP_TextElementType.Character)
				{
					goto IL_3AA;
				}
				IL_13B0:
				num9++;
				continue;
				IL_3AA:
				int currentMaterialIndex = this.m_currentMaterialIndex;
				bool isUsingAlternateTypeface = this.m_textInfo.characterInfo[this.m_characterCount].isUsingAlternateTypeface;
				this.m_isParsingText = false;
				float num11 = 1f;
				if (this.m_textElementType == TMP_TextElementType.Character)
				{
					if ((this.m_style & FontStyles.UpperCase) == FontStyles.UpperCase)
					{
						if (char.IsLower((char)num10))
						{
							num10 = (int)char.ToUpper((char)num10);
						}
					}
					else if ((this.m_style & FontStyles.LowerCase) == FontStyles.LowerCase)
					{
						if (char.IsUpper((char)num10))
						{
							num10 = (int)char.ToLower((char)num10);
						}
					}
					else if (((this.m_fontStyle & FontStyles.SmallCaps) == FontStyles.SmallCaps || (this.m_style & FontStyles.SmallCaps) == FontStyles.SmallCaps) && char.IsLower((char)num10))
					{
						num11 = 0.8f;
						num10 = (int)char.ToUpper((char)num10);
					}
				}
				if (this.m_textElementType == TMP_TextElementType.Sprite)
				{
					this.m_currentSpriteAsset = this.m_textInfo.characterInfo[this.m_characterCount].spriteAsset;
					this.m_spriteIndex = this.m_textInfo.characterInfo[this.m_characterCount].spriteIndex;
					TMP_Sprite tmp_Sprite = this.m_currentSpriteAsset.spriteInfoList[this.m_spriteIndex];
					if (tmp_Sprite == null)
					{
						goto IL_13B0;
					}
					if (num10 == 60)
					{
						num10 = 57344 + this.m_spriteIndex;
					}
					this.m_currentFontAsset = this.m_fontAsset;
					float num12 = this.m_currentFontSize / this.m_fontAsset.fontInfo.PointSize * this.m_fontAsset.fontInfo.Scale * (this.m_isOrthographic ? 1f : 0.1f);
					num2 = this.m_fontAsset.fontInfo.Ascender / tmp_Sprite.height * tmp_Sprite.scale * num12;
					this.m_cached_TextElement = tmp_Sprite;
					this.m_internalCharacterInfo[this.m_characterCount].elementType = TMP_TextElementType.Sprite;
					this.m_internalCharacterInfo[this.m_characterCount].scale = num12;
					this.m_currentMaterialIndex = currentMaterialIndex;
				}
				else if (this.m_textElementType == TMP_TextElementType.Character)
				{
					this.m_cached_TextElement = this.m_textInfo.characterInfo[this.m_characterCount].textElement;
					if (this.m_cached_TextElement == null)
					{
						goto IL_13B0;
					}
					this.m_currentMaterialIndex = this.m_textInfo.characterInfo[this.m_characterCount].materialReferenceIndex;
					this.m_fontScale = this.m_currentFontSize * num11 / this.m_currentFontAsset.fontInfo.PointSize * this.m_currentFontAsset.fontInfo.Scale * (this.m_isOrthographic ? 1f : 0.1f);
					num2 = this.m_fontScale * this.m_fontScaleMultiplier * this.m_cached_TextElement.scale;
					this.m_internalCharacterInfo[this.m_characterCount].elementType = TMP_TextElementType.Character;
				}
				float num13 = num2;
				if (num10 == 173)
				{
					num2 = 0f;
				}
				this.m_internalCharacterInfo[this.m_characterCount].character = (char)num10;
				GlyphValueRecord glyphValueRecord = default(GlyphValueRecord);
				if (this.m_enableKerning)
				{
					KerningPair kerningPair = null;
					if (this.m_characterCount < totalCharacterCount - 1)
					{
						uint character = (uint)this.m_textInfo.characterInfo[this.m_characterCount + 1].character;
						KerningPairKey kerningPairKey = new KerningPairKey((uint)num10, character);
						this.m_currentFontAsset.kerningDictionary.TryGetValue((int)kerningPairKey.key, out kerningPair);
						if (kerningPair != null)
						{
							glyphValueRecord = kerningPair.firstGlyphAdjustments;
						}
					}
					if (this.m_characterCount >= 1)
					{
						uint character2 = (uint)this.m_textInfo.characterInfo[this.m_characterCount - 1].character;
						KerningPairKey kerningPairKey2 = new KerningPairKey(character2, (uint)num10);
						this.m_currentFontAsset.kerningDictionary.TryGetValue((int)kerningPairKey2.key, out kerningPair);
						if (kerningPair != null)
						{
							glyphValueRecord += kerningPair.secondGlyphAdjustments;
						}
					}
				}
				float num14 = 0f;
				if (this.m_monoSpacing != 0f)
				{
					num14 = this.m_monoSpacing / 2f - (this.m_cached_TextElement.width / 2f + this.m_cached_TextElement.xOffset) * num2;
					this.m_xAdvance += num14;
				}
				float num15;
				if (this.m_textElementType == TMP_TextElementType.Character && !isUsingAlternateTypeface && ((this.m_style & FontStyles.Bold) == FontStyles.Bold || (this.m_fontStyle & FontStyles.Bold) == FontStyles.Bold))
				{
					num15 = 1f + this.m_currentFontAsset.boldSpacing * 0.01f;
				}
				else
				{
					num15 = 1f;
				}
				this.m_internalCharacterInfo[this.m_characterCount].baseLine = 0f - this.m_lineOffset + this.m_baselineOffset;
				float num16 = this.m_currentFontAsset.fontInfo.Ascender * ((this.m_textElementType == TMP_TextElementType.Character) ? (num2 / num11) : this.m_internalCharacterInfo[this.m_characterCount].scale) + this.m_baselineOffset;
				this.m_internalCharacterInfo[this.m_characterCount].ascender = num16 - this.m_lineOffset;
				this.m_maxLineAscender = ((num16 > this.m_maxLineAscender) ? num16 : this.m_maxLineAscender);
				float num17 = this.m_currentFontAsset.fontInfo.Descender * ((this.m_textElementType == TMP_TextElementType.Character) ? (num2 / num11) : this.m_internalCharacterInfo[this.m_characterCount].scale) + this.m_baselineOffset;
				float num18 = this.m_internalCharacterInfo[this.m_characterCount].descender = num17 - this.m_lineOffset;
				this.m_maxLineDescender = ((num17 < this.m_maxLineDescender) ? num17 : this.m_maxLineDescender);
				if ((this.m_style & FontStyles.Subscript) == FontStyles.Subscript || (this.m_style & FontStyles.Superscript) == FontStyles.Superscript)
				{
					float num19 = (num16 - this.m_baselineOffset) / this.m_currentFontAsset.fontInfo.SubSize;
					num16 = this.m_maxLineAscender;
					this.m_maxLineAscender = ((num19 > this.m_maxLineAscender) ? num19 : this.m_maxLineAscender);
					float num20 = (num17 - this.m_baselineOffset) / this.m_currentFontAsset.fontInfo.SubSize;
					num17 = this.m_maxLineDescender;
					this.m_maxLineDescender = ((num20 < this.m_maxLineDescender) ? num20 : this.m_maxLineDescender);
				}
				if (this.m_lineNumber == 0)
				{
					this.m_maxAscender = ((this.m_maxAscender > num16) ? this.m_maxAscender : num16);
				}
				if (num10 == 9 || num10 == 160 || num10 == 8199 || (!char.IsWhiteSpace((char)num10) && num10 != 8203) || this.m_textElementType == TMP_TextElementType.Sprite)
				{
					float num21 = (this.m_width != -1f) ? Mathf.Min(x + 0.0001f - this.m_marginLeft - this.m_marginRight, this.m_width) : (x + 0.0001f - this.m_marginLeft - this.m_marginRight);
					bool flag3 = (this.m_lineJustification & (TextAlignmentOptions)16) == (TextAlignmentOptions)16 || (this.m_lineJustification & (TextAlignmentOptions)8) == (TextAlignmentOptions)8;
					num6 = this.m_xAdvance + this.m_cached_TextElement.xAdvance * (1f - this.m_charWidthAdjDelta) * ((num10 != 173) ? num2 : num13);
					if (num6 > num21 * (flag3 ? 1.05f : 1f))
					{
						if (this.enableWordWrapping && this.m_characterCount != this.m_firstCharacterOfLine)
						{
							if (num7 == wordWrapState2.previous_WordBreak || flag)
							{
								if (!ignoreTextAutoSizing && this.m_currentFontSize > this.m_fontSizeMin)
								{
									if (this.m_charWidthAdjDelta < this.m_charWidthMaxAdj / 100f)
									{
										this.m_recursiveCount = 0;
										this.m_charWidthAdjDelta += 0.01f;
										return this.CalculatePreferredValues(defaultFontSize, marginSize, false);
									}
									this.m_maxFontSize = defaultFontSize;
									defaultFontSize -= Mathf.Max((defaultFontSize - this.m_minFontSize) / 2f, 0.05f);
									defaultFontSize = (float)((int)(Mathf.Max(defaultFontSize, this.m_fontSizeMin) * 20f + 0.5f)) / 20f;
									if (this.m_recursiveCount > 20)
									{
										return new Vector2(num4, num5);
									}
									return this.CalculatePreferredValues(defaultFontSize, marginSize, false);
								}
								else if (!this.m_isCharacterWrappingEnabled)
								{
									this.m_isCharacterWrappingEnabled = true;
								}
								else
								{
									flag2 = true;
								}
							}
							num9 = this.RestoreWordWrappingState(ref wordWrapState2);
							num7 = num9;
							if (this.m_char_buffer[num9] == 173)
							{
								this.m_isTextTruncated = true;
								this.m_char_buffer[num9] = 45;
								return this.CalculatePreferredValues(defaultFontSize, marginSize, true);
							}
							if (this.m_lineNumber > 0 && !TMP_Math.Approximately(this.m_maxLineAscender, this.m_startOfLineAscender) && this.m_lineHeight == -32767f)
							{
								float num22 = this.m_maxLineAscender - this.m_startOfLineAscender;
								this.m_lineOffset += num22;
								wordWrapState2.lineOffset = this.m_lineOffset;
								wordWrapState2.previousLineAscender = this.m_maxLineAscender;
							}
							float num23 = this.m_maxLineAscender - this.m_lineOffset;
							float num24 = this.m_maxLineDescender - this.m_lineOffset;
							this.m_maxDescender = ((this.m_maxDescender < num24) ? this.m_maxDescender : num24);
							this.m_firstCharacterOfLine = this.m_characterCount;
							num4 += this.m_xAdvance;
							if (this.m_enableWordWrapping)
							{
								num5 = this.m_maxAscender - this.m_maxDescender;
							}
							else
							{
								num5 = Mathf.Max(num5, num23 - num24);
							}
							this.SaveWordWrappingState(ref wordWrapState, num9, this.m_characterCount - 1);
							this.m_lineNumber++;
							if (this.m_lineHeight == -32767f)
							{
								float num25 = this.m_internalCharacterInfo[this.m_characterCount].ascender - this.m_internalCharacterInfo[this.m_characterCount].baseLine;
								float num26 = 0f - this.m_maxLineDescender + num25 + (num3 + this.m_lineSpacing + this.m_lineSpacingDelta) * num;
								this.m_lineOffset += num26;
								this.m_startOfLineAscender = num25;
							}
							else
							{
								this.m_lineOffset += this.m_lineHeight + this.m_lineSpacing * num;
							}
							this.m_maxLineAscender = TMP_Text.k_LargeNegativeFloat;
							this.m_maxLineDescender = TMP_Text.k_LargePositiveFloat;
							this.m_xAdvance = 0f + this.tag_Indent;
							goto IL_13B0;
						}
						else if (!ignoreTextAutoSizing && defaultFontSize > this.m_fontSizeMin)
						{
							if (this.m_charWidthAdjDelta < this.m_charWidthMaxAdj / 100f)
							{
								this.m_recursiveCount = 0;
								this.m_charWidthAdjDelta += 0.01f;
								return this.CalculatePreferredValues(defaultFontSize, marginSize, false);
							}
							this.m_maxFontSize = defaultFontSize;
							defaultFontSize -= Mathf.Max((defaultFontSize - this.m_minFontSize) / 2f, 0.05f);
							defaultFontSize = (float)((int)(Mathf.Max(defaultFontSize, this.m_fontSizeMin) * 20f + 0.5f)) / 20f;
							if (this.m_recursiveCount > 20)
							{
								return new Vector2(num4, num5);
							}
							return this.CalculatePreferredValues(defaultFontSize, marginSize, false);
						}
					}
				}
				if (this.m_lineNumber > 0 && !TMP_Math.Approximately(this.m_maxLineAscender, this.m_startOfLineAscender) && this.m_lineHeight == -32767f && !this.m_isNewPage)
				{
					float num27 = this.m_maxLineAscender - this.m_startOfLineAscender;
					num18 -= num27;
					this.m_lineOffset += num27;
					this.m_startOfLineAscender += num27;
					wordWrapState2.lineOffset = this.m_lineOffset;
					wordWrapState2.previousLineAscender = this.m_startOfLineAscender;
				}
				if (num10 == 9)
				{
					float num28 = this.m_currentFontAsset.fontInfo.TabWidth * num2;
					float num29 = Mathf.Ceil(this.m_xAdvance / num28) * num28;
					this.m_xAdvance = ((num29 > this.m_xAdvance) ? num29 : (this.m_xAdvance + num28));
				}
				else if (this.m_monoSpacing != 0f)
				{
					this.m_xAdvance += (this.m_monoSpacing - num14 + (this.m_characterSpacing + this.m_currentFontAsset.normalSpacingOffset) * num2 + this.m_cSpacing) * (1f - this.m_charWidthAdjDelta);
					if (char.IsWhiteSpace((char)num10) || num10 == 8203)
					{
						this.m_xAdvance += this.m_wordSpacing * num2;
					}
				}
				else
				{
					this.m_xAdvance += ((this.m_cached_TextElement.xAdvance * num15 + this.m_characterSpacing + this.m_currentFontAsset.normalSpacingOffset + glyphValueRecord.xAdvance) * num2 + this.m_cSpacing) * (1f - this.m_charWidthAdjDelta);
					if (char.IsWhiteSpace((char)num10) || num10 == 8203)
					{
						this.m_xAdvance += this.m_wordSpacing * num2;
					}
				}
				if (num10 == 13)
				{
					a = Mathf.Max(a, num4 + this.m_xAdvance);
					num4 = 0f;
					this.m_xAdvance = 0f + this.tag_Indent;
				}
				if (num10 == 10 || this.m_characterCount == totalCharacterCount - 1)
				{
					if (this.m_lineNumber > 0 && !TMP_Math.Approximately(this.m_maxLineAscender, this.m_startOfLineAscender) && this.m_lineHeight == -32767f)
					{
						float num30 = this.m_maxLineAscender - this.m_startOfLineAscender;
						num18 -= num30;
						this.m_lineOffset += num30;
					}
					float num31 = this.m_maxLineDescender - this.m_lineOffset;
					this.m_maxDescender = ((this.m_maxDescender < num31) ? this.m_maxDescender : num31);
					this.m_firstCharacterOfLine = this.m_characterCount + 1;
					if (num10 == 10 && this.m_characterCount != totalCharacterCount - 1)
					{
						a = Mathf.Max(a, num4 + num6);
						num4 = 0f;
					}
					else
					{
						num4 = Mathf.Max(a, num4 + num6);
					}
					num5 = this.m_maxAscender - this.m_maxDescender;
					if (num10 == 10)
					{
						this.SaveWordWrappingState(ref wordWrapState, num9, this.m_characterCount);
						this.SaveWordWrappingState(ref wordWrapState2, num9, this.m_characterCount);
						this.m_lineNumber++;
						if (this.m_lineHeight == -32767f)
						{
							float num26 = 0f - this.m_maxLineDescender + num16 + (num3 + this.m_lineSpacing + this.m_paragraphSpacing + this.m_lineSpacingDelta) * num;
							this.m_lineOffset += num26;
						}
						else
						{
							this.m_lineOffset += this.m_lineHeight + (this.m_lineSpacing + this.m_paragraphSpacing) * num;
						}
						this.m_maxLineAscender = TMP_Text.k_LargeNegativeFloat;
						this.m_maxLineDescender = TMP_Text.k_LargePositiveFloat;
						this.m_startOfLineAscender = num16;
						this.m_xAdvance = 0f + this.tag_LineIndent + this.tag_Indent;
						this.m_characterCount++;
						goto IL_13B0;
					}
				}
				if (this.m_enableWordWrapping || this.m_overflowMode == TextOverflowModes.Truncate || this.m_overflowMode == TextOverflowModes.Ellipsis)
				{
					if ((char.IsWhiteSpace((char)num10) || num10 == 8203 || num10 == 45 || num10 == 173) && !this.m_isNonBreakingSpace && num10 != 160 && num10 != 8209 && num10 != 8239 && num10 != 8288)
					{
						this.SaveWordWrappingState(ref wordWrapState2, num9, this.m_characterCount);
						this.m_isCharacterWrappingEnabled = false;
						flag = false;
					}
					else if (((num10 > 4352 && num10 < 4607) || (num10 > 11904 && num10 < 40959) || (num10 > 43360 && num10 < 43391) || (num10 > 44032 && num10 < 55295) || (num10 > 63744 && num10 < 64255) || (num10 > 65072 && num10 < 65103) || (num10 > 65280 && num10 < 65519)) && !this.m_isNonBreakingSpace)
					{
						if (flag || flag2 || (!TMP_Settings.linebreakingRules.leadingCharacters.ContainsKey(num10) && this.m_characterCount < totalCharacterCount - 1 && !TMP_Settings.linebreakingRules.followingCharacters.ContainsKey((int)this.m_internalCharacterInfo[this.m_characterCount + 1].character)))
						{
							this.SaveWordWrappingState(ref wordWrapState2, num9, this.m_characterCount);
							this.m_isCharacterWrappingEnabled = false;
							flag = false;
						}
					}
					else if (flag || this.m_isCharacterWrappingEnabled || flag2)
					{
						this.SaveWordWrappingState(ref wordWrapState2, num9, this.m_characterCount);
					}
				}
				this.m_characterCount++;
				goto IL_13B0;
			}
			float num32 = this.m_maxFontSize - this.m_minFontSize;
			if (this.m_isCharacterWrappingEnabled || ignoreTextAutoSizing || num32 <= 0.051f || defaultFontSize >= this.m_fontSizeMax)
			{
				this.m_isCharacterWrappingEnabled = false;
				this.m_isCalculatingPreferredValues = false;
				num4 += ((this.m_margin.x > 0f) ? this.m_margin.x : 0f);
				num4 += ((this.m_margin.z > 0f) ? this.m_margin.z : 0f);
				num5 += ((this.m_margin.y > 0f) ? this.m_margin.y : 0f);
				num5 += ((this.m_margin.w > 0f) ? this.m_margin.w : 0f);
				num4 = (float)((int)(num4 * 100f + 1f)) / 100f;
				num5 = (float)((int)(num5 * 100f + 1f)) / 100f;
				return new Vector2(num4, num5);
			}
			this.m_minFontSize = defaultFontSize;
			defaultFontSize += Mathf.Max((this.m_maxFontSize - defaultFontSize) / 2f, 0.05f);
			defaultFontSize = (float)((int)(Mathf.Min(defaultFontSize, this.m_fontSizeMax) * 20f + 0.5f)) / 20f;
			if (this.m_recursiveCount > 20)
			{
				return new Vector2(num4, num5);
			}
			return this.CalculatePreferredValues(defaultFontSize, marginSize, false);
		}

		// Token: 0x060003D7 RID: 983 RVA: 0x000238A4 File Offset: 0x00021AA4
		protected virtual Bounds GetCompoundBounds()
		{
			return default(Bounds);
		}

		// Token: 0x060003D8 RID: 984 RVA: 0x000238BC File Offset: 0x00021ABC
		protected Bounds GetTextBounds()
		{
			if (this.m_textInfo == null || this.m_textInfo.characterCount > this.m_textInfo.characterInfo.Length)
			{
				return default(Bounds);
			}
			Extents extents = new Extents(TMP_Text.k_LargePositiveVector2, TMP_Text.k_LargeNegativeVector2);
			int num = 0;
			while (num < this.m_textInfo.characterCount && num < this.m_textInfo.characterInfo.Length)
			{
				if (this.m_textInfo.characterInfo[num].isVisible)
				{
					extents.min.x = Mathf.Min(extents.min.x, this.m_textInfo.characterInfo[num].bottomLeft.x);
					extents.min.y = Mathf.Min(extents.min.y, this.m_textInfo.characterInfo[num].descender);
					extents.max.x = Mathf.Max(extents.max.x, this.m_textInfo.characterInfo[num].xAdvance);
					extents.max.y = Mathf.Max(extents.max.y, this.m_textInfo.characterInfo[num].ascender);
				}
				num++;
			}
			Vector2 v;
			v.x = extents.max.x - extents.min.x;
			v.y = extents.max.y - extents.min.y;
			return new Bounds((extents.min + extents.max) / 2f, v);
		}

		// Token: 0x060003D9 RID: 985 RVA: 0x00023A84 File Offset: 0x00021C84
		protected Bounds GetTextBounds(bool onlyVisibleCharacters)
		{
			if (this.m_textInfo == null)
			{
				return default(Bounds);
			}
			Extents extents = new Extents(TMP_Text.k_LargePositiveVector2, TMP_Text.k_LargeNegativeVector2);
			int num = 0;
			while (num < this.m_textInfo.characterCount && ((num <= this.maxVisibleCharacters && this.m_textInfo.characterInfo[num].lineNumber <= this.m_maxVisibleLines) || !onlyVisibleCharacters))
			{
				if (!onlyVisibleCharacters || this.m_textInfo.characterInfo[num].isVisible)
				{
					extents.min.x = Mathf.Min(extents.min.x, this.m_textInfo.characterInfo[num].origin);
					extents.min.y = Mathf.Min(extents.min.y, this.m_textInfo.characterInfo[num].descender);
					extents.max.x = Mathf.Max(extents.max.x, this.m_textInfo.characterInfo[num].xAdvance);
					extents.max.y = Mathf.Max(extents.max.y, this.m_textInfo.characterInfo[num].ascender);
				}
				num++;
			}
			Vector2 v;
			v.x = extents.max.x - extents.min.x;
			v.y = extents.max.y - extents.min.y;
			return new Bounds((extents.min + extents.max) / 2f, v);
		}

		// Token: 0x060003DA RID: 986 RVA: 0x0000297D File Offset: 0x00000B7D
		protected virtual void AdjustLineOffset(int startIndex, int endIndex, float offset)
		{
		}

		// Token: 0x060003DB RID: 987 RVA: 0x00023C50 File Offset: 0x00021E50
		protected void ResizeLineExtents(int size)
		{
			size = ((size > 1024) ? (size + 256) : Mathf.NextPowerOfTwo(size + 1));
			TMP_LineInfo[] array = new TMP_LineInfo[size];
			for (int i = 0; i < size; i++)
			{
				if (i < this.m_textInfo.lineInfo.Length)
				{
					array[i] = this.m_textInfo.lineInfo[i];
				}
				else
				{
					array[i].lineExtents.min = TMP_Text.k_LargePositiveVector2;
					array[i].lineExtents.max = TMP_Text.k_LargeNegativeVector2;
					array[i].ascender = TMP_Text.k_LargeNegativeFloat;
					array[i].descender = TMP_Text.k_LargePositiveFloat;
				}
			}
			this.m_textInfo.lineInfo = array;
		}

		// Token: 0x060003DC RID: 988 RVA: 0x00002E56 File Offset: 0x00001056
		public virtual TMP_TextInfo GetTextInfo(string text)
		{
			return null;
		}

		// Token: 0x060003DD RID: 989 RVA: 0x0000297D File Offset: 0x00000B7D
		public virtual void ComputeMarginSize()
		{
		}

		// Token: 0x060003DE RID: 990 RVA: 0x00023D10 File Offset: 0x00021F10
		protected void SaveWordWrappingState(ref WordWrapState state, int index, int count)
		{
			state.currentFontAsset = this.m_currentFontAsset;
			state.currentSpriteAsset = this.m_currentSpriteAsset;
			state.currentMaterial = this.m_currentMaterial;
			state.currentMaterialIndex = this.m_currentMaterialIndex;
			state.previous_WordBreak = index;
			state.total_CharacterCount = count;
			state.visible_CharacterCount = this.m_lineVisibleCharacterCount;
			state.visible_LinkCount = this.m_textInfo.linkCount;
			state.firstCharacterIndex = this.m_firstCharacterOfLine;
			state.firstVisibleCharacterIndex = this.m_firstVisibleCharacterOfLine;
			state.lastVisibleCharIndex = this.m_lastVisibleCharacterOfLine;
			state.fontStyle = this.m_style;
			state.fontScale = this.m_fontScale;
			state.fontScaleMultiplier = this.m_fontScaleMultiplier;
			state.currentFontSize = this.m_currentFontSize;
			state.xAdvance = this.m_xAdvance;
			state.maxCapHeight = this.m_maxCapHeight;
			state.maxAscender = this.m_maxAscender;
			state.maxDescender = this.m_maxDescender;
			state.maxLineAscender = this.m_maxLineAscender;
			state.maxLineDescender = this.m_maxLineDescender;
			state.previousLineAscender = this.m_startOfLineAscender;
			state.preferredWidth = this.m_preferredWidth;
			state.preferredHeight = this.m_preferredHeight;
			state.meshExtents = this.m_meshExtents;
			state.lineNumber = this.m_lineNumber;
			state.lineOffset = this.m_lineOffset;
			state.baselineOffset = this.m_baselineOffset;
			state.vertexColor = this.m_htmlColor;
			state.underlineColor = this.m_underlineColor;
			state.strikethroughColor = this.m_strikethroughColor;
			state.highlightColor = this.m_highlightColor;
			state.isNonBreakingSpace = this.m_isNonBreakingSpace;
			state.tagNoParsing = this.tag_NoParsing;
			state.basicStyleStack = this.m_fontStyleStack;
			state.colorStack = this.m_colorStack;
			state.underlineColorStack = this.m_underlineColorStack;
			state.strikethroughColorStack = this.m_strikethroughColorStack;
			state.highlightColorStack = this.m_highlightColorStack;
			state.colorGradientStack = this.m_colorGradientStack;
			state.sizeStack = this.m_sizeStack;
			state.indentStack = this.m_indentStack;
			state.fontWeightStack = this.m_fontWeightStack;
			state.styleStack = this.m_styleStack;
			state.baselineStack = this.m_baselineOffsetStack;
			state.actionStack = this.m_actionStack;
			state.materialReferenceStack = this.m_materialReferenceStack;
			state.lineJustificationStack = this.m_lineJustificationStack;
			state.spriteAnimationID = this.m_spriteAnimationID;
			if (this.m_lineNumber < this.m_textInfo.lineInfo.Length)
			{
				state.lineInfo = this.m_textInfo.lineInfo[this.m_lineNumber];
			}
		}

		// Token: 0x060003DF RID: 991 RVA: 0x00023F98 File Offset: 0x00022198
		protected int RestoreWordWrappingState(ref WordWrapState state)
		{
			int previous_WordBreak = state.previous_WordBreak;
			this.m_currentFontAsset = state.currentFontAsset;
			this.m_currentSpriteAsset = state.currentSpriteAsset;
			this.m_currentMaterial = state.currentMaterial;
			this.m_currentMaterialIndex = state.currentMaterialIndex;
			this.m_characterCount = state.total_CharacterCount + 1;
			this.m_lineVisibleCharacterCount = state.visible_CharacterCount;
			this.m_textInfo.linkCount = state.visible_LinkCount;
			this.m_firstCharacterOfLine = state.firstCharacterIndex;
			this.m_firstVisibleCharacterOfLine = state.firstVisibleCharacterIndex;
			this.m_lastVisibleCharacterOfLine = state.lastVisibleCharIndex;
			this.m_style = state.fontStyle;
			this.m_fontScale = state.fontScale;
			this.m_fontScaleMultiplier = state.fontScaleMultiplier;
			this.m_currentFontSize = state.currentFontSize;
			this.m_xAdvance = state.xAdvance;
			this.m_maxCapHeight = state.maxCapHeight;
			this.m_maxAscender = state.maxAscender;
			this.m_maxDescender = state.maxDescender;
			this.m_maxLineAscender = state.maxLineAscender;
			this.m_maxLineDescender = state.maxLineDescender;
			this.m_startOfLineAscender = state.previousLineAscender;
			this.m_preferredWidth = state.preferredWidth;
			this.m_preferredHeight = state.preferredHeight;
			this.m_meshExtents = state.meshExtents;
			this.m_lineNumber = state.lineNumber;
			this.m_lineOffset = state.lineOffset;
			this.m_baselineOffset = state.baselineOffset;
			this.m_htmlColor = state.vertexColor;
			this.m_underlineColor = state.underlineColor;
			this.m_strikethroughColor = state.strikethroughColor;
			this.m_highlightColor = state.highlightColor;
			this.m_isNonBreakingSpace = state.isNonBreakingSpace;
			this.tag_NoParsing = state.tagNoParsing;
			this.m_fontStyleStack = state.basicStyleStack;
			this.m_colorStack = state.colorStack;
			this.m_underlineColorStack = state.underlineColorStack;
			this.m_strikethroughColorStack = state.strikethroughColorStack;
			this.m_highlightColorStack = state.highlightColorStack;
			this.m_colorGradientStack = state.colorGradientStack;
			this.m_sizeStack = state.sizeStack;
			this.m_indentStack = state.indentStack;
			this.m_fontWeightStack = state.fontWeightStack;
			this.m_styleStack = state.styleStack;
			this.m_baselineOffsetStack = state.baselineStack;
			this.m_actionStack = state.actionStack;
			this.m_materialReferenceStack = state.materialReferenceStack;
			this.m_lineJustificationStack = state.lineJustificationStack;
			this.m_spriteAnimationID = state.spriteAnimationID;
			if (this.m_lineNumber < this.m_textInfo.lineInfo.Length)
			{
				this.m_textInfo.lineInfo[this.m_lineNumber] = state.lineInfo;
			}
			return previous_WordBreak;
		}

		// Token: 0x060003E0 RID: 992 RVA: 0x00024224 File Offset: 0x00022424
		protected virtual void SaveGlyphVertexInfo(float padding, float style_padding, Color32 vertexColor)
		{
			this.m_textInfo.characterInfo[this.m_characterCount].vertex_BL.position = this.m_textInfo.characterInfo[this.m_characterCount].bottomLeft;
			this.m_textInfo.characterInfo[this.m_characterCount].vertex_TL.position = this.m_textInfo.characterInfo[this.m_characterCount].topLeft;
			this.m_textInfo.characterInfo[this.m_characterCount].vertex_TR.position = this.m_textInfo.characterInfo[this.m_characterCount].topRight;
			this.m_textInfo.characterInfo[this.m_characterCount].vertex_BR.position = this.m_textInfo.characterInfo[this.m_characterCount].bottomRight;
			vertexColor.a = ((this.m_fontColor32.a < vertexColor.a) ? this.m_fontColor32.a : vertexColor.a);
			if (!this.m_enableVertexGradient)
			{
				this.m_textInfo.characterInfo[this.m_characterCount].vertex_BL.color = vertexColor;
				this.m_textInfo.characterInfo[this.m_characterCount].vertex_TL.color = vertexColor;
				this.m_textInfo.characterInfo[this.m_characterCount].vertex_TR.color = vertexColor;
				this.m_textInfo.characterInfo[this.m_characterCount].vertex_BR.color = vertexColor;
			}
			else if (!this.m_overrideHtmlColors && this.m_colorStack.index > 1)
			{
				this.m_textInfo.characterInfo[this.m_characterCount].vertex_BL.color = vertexColor;
				this.m_textInfo.characterInfo[this.m_characterCount].vertex_TL.color = vertexColor;
				this.m_textInfo.characterInfo[this.m_characterCount].vertex_TR.color = vertexColor;
				this.m_textInfo.characterInfo[this.m_characterCount].vertex_BR.color = vertexColor;
			}
			else if (this.m_fontColorGradientPreset != null)
			{
				this.m_textInfo.characterInfo[this.m_characterCount].vertex_BL.color = this.m_fontColorGradientPreset.bottomLeft * vertexColor;
				this.m_textInfo.characterInfo[this.m_characterCount].vertex_TL.color = this.m_fontColorGradientPreset.topLeft * vertexColor;
				this.m_textInfo.characterInfo[this.m_characterCount].vertex_TR.color = this.m_fontColorGradientPreset.topRight * vertexColor;
				this.m_textInfo.characterInfo[this.m_characterCount].vertex_BR.color = this.m_fontColorGradientPreset.bottomRight * vertexColor;
			}
			else
			{
				this.m_textInfo.characterInfo[this.m_characterCount].vertex_BL.color = this.m_fontColorGradient.bottomLeft * vertexColor;
				this.m_textInfo.characterInfo[this.m_characterCount].vertex_TL.color = this.m_fontColorGradient.topLeft * vertexColor;
				this.m_textInfo.characterInfo[this.m_characterCount].vertex_TR.color = this.m_fontColorGradient.topRight * vertexColor;
				this.m_textInfo.characterInfo[this.m_characterCount].vertex_BR.color = this.m_fontColorGradient.bottomRight * vertexColor;
			}
			if (this.m_colorGradientPreset != null)
			{
				TMP_CharacterInfo[] characterInfo = this.m_textInfo.characterInfo;
				int characterCount = this.m_characterCount;
				characterInfo[characterCount].vertex_BL.color = characterInfo[characterCount].vertex_BL.color * this.m_colorGradientPreset.bottomLeft;
				TMP_CharacterInfo[] characterInfo2 = this.m_textInfo.characterInfo;
				int characterCount2 = this.m_characterCount;
				characterInfo2[characterCount2].vertex_TL.color = characterInfo2[characterCount2].vertex_TL.color * this.m_colorGradientPreset.topLeft;
				TMP_CharacterInfo[] characterInfo3 = this.m_textInfo.characterInfo;
				int characterCount3 = this.m_characterCount;
				characterInfo3[characterCount3].vertex_TR.color = characterInfo3[characterCount3].vertex_TR.color * this.m_colorGradientPreset.topRight;
				TMP_CharacterInfo[] characterInfo4 = this.m_textInfo.characterInfo;
				int characterCount4 = this.m_characterCount;
				characterInfo4[characterCount4].vertex_BR.color = characterInfo4[characterCount4].vertex_BR.color * this.m_colorGradientPreset.bottomRight;
			}
			if (!this.m_isSDFShader)
			{
				style_padding = 0f;
			}
			FaceInfo fontInfo = this.m_currentFontAsset.fontInfo;
			Vector2 vector;
			vector.x = (this.m_cached_TextElement.x - padding - style_padding) / fontInfo.AtlasWidth;
			vector.y = 1f - (this.m_cached_TextElement.y + padding + style_padding + this.m_cached_TextElement.height) / fontInfo.AtlasHeight;
			Vector2 vector2;
			vector2.x = vector.x;
			vector2.y = 1f - (this.m_cached_TextElement.y - padding - style_padding) / fontInfo.AtlasHeight;
			Vector2 vector3;
			vector3.x = (this.m_cached_TextElement.x + padding + style_padding + this.m_cached_TextElement.width) / fontInfo.AtlasWidth;
			vector3.y = vector2.y;
			Vector2 uv;
			uv.x = vector3.x;
			uv.y = vector.y;
			this.m_textInfo.characterInfo[this.m_characterCount].vertex_BL.uv = vector;
			this.m_textInfo.characterInfo[this.m_characterCount].vertex_TL.uv = vector2;
			this.m_textInfo.characterInfo[this.m_characterCount].vertex_TR.uv = vector3;
			this.m_textInfo.characterInfo[this.m_characterCount].vertex_BR.uv = uv;
		}

		// Token: 0x060003E1 RID: 993 RVA: 0x00024904 File Offset: 0x00022B04
		protected virtual void SaveSpriteVertexInfo(Color32 vertexColor)
		{
			this.m_textInfo.characterInfo[this.m_characterCount].vertex_BL.position = this.m_textInfo.characterInfo[this.m_characterCount].bottomLeft;
			this.m_textInfo.characterInfo[this.m_characterCount].vertex_TL.position = this.m_textInfo.characterInfo[this.m_characterCount].topLeft;
			this.m_textInfo.characterInfo[this.m_characterCount].vertex_TR.position = this.m_textInfo.characterInfo[this.m_characterCount].topRight;
			this.m_textInfo.characterInfo[this.m_characterCount].vertex_BR.position = this.m_textInfo.characterInfo[this.m_characterCount].bottomRight;
			if (this.m_tintAllSprites)
			{
				this.m_tintSprite = true;
			}
			Color32 color = this.m_tintSprite ? this.m_spriteColor.Multiply(vertexColor) : this.m_spriteColor;
			color.a = ((color.a < this.m_fontColor32.a) ? (color.a = ((color.a < vertexColor.a) ? color.a : vertexColor.a)) : this.m_fontColor32.a);
			Color32 color2 = color;
			Color32 color3 = color;
			Color32 color4 = color;
			Color32 color5 = color;
			if (this.m_enableVertexGradient)
			{
				if (this.m_fontColorGradientPreset != null)
				{
					color2 = (this.m_tintSprite ? color2.Multiply(this.m_fontColorGradientPreset.bottomLeft) : color2);
					color3 = (this.m_tintSprite ? color3.Multiply(this.m_fontColorGradientPreset.topLeft) : color3);
					color4 = (this.m_tintSprite ? color4.Multiply(this.m_fontColorGradientPreset.topRight) : color4);
					color5 = (this.m_tintSprite ? color5.Multiply(this.m_fontColorGradientPreset.bottomRight) : color5);
				}
				else
				{
					color2 = (this.m_tintSprite ? color2.Multiply(this.m_fontColorGradient.bottomLeft) : color2);
					color3 = (this.m_tintSprite ? color3.Multiply(this.m_fontColorGradient.topLeft) : color3);
					color4 = (this.m_tintSprite ? color4.Multiply(this.m_fontColorGradient.topRight) : color4);
					color5 = (this.m_tintSprite ? color5.Multiply(this.m_fontColorGradient.bottomRight) : color5);
				}
			}
			if (this.m_colorGradientPreset != null)
			{
				color2 = (this.m_tintSprite ? color2.Multiply(this.m_colorGradientPreset.bottomLeft) : color2);
				color3 = (this.m_tintSprite ? color3.Multiply(this.m_colorGradientPreset.topLeft) : color3);
				color4 = (this.m_tintSprite ? color4.Multiply(this.m_colorGradientPreset.topRight) : color4);
				color5 = (this.m_tintSprite ? color5.Multiply(this.m_colorGradientPreset.bottomRight) : color5);
			}
			this.m_textInfo.characterInfo[this.m_characterCount].vertex_BL.color = color2;
			this.m_textInfo.characterInfo[this.m_characterCount].vertex_TL.color = color3;
			this.m_textInfo.characterInfo[this.m_characterCount].vertex_TR.color = color4;
			this.m_textInfo.characterInfo[this.m_characterCount].vertex_BR.color = color5;
			Vector2 vector = new Vector2(this.m_cached_TextElement.x / (float)this.m_currentSpriteAsset.spriteSheet.width, this.m_cached_TextElement.y / (float)this.m_currentSpriteAsset.spriteSheet.height);
			Vector2 vector2 = new Vector2(vector.x, (this.m_cached_TextElement.y + this.m_cached_TextElement.height) / (float)this.m_currentSpriteAsset.spriteSheet.height);
			Vector2 vector3 = new Vector2((this.m_cached_TextElement.x + this.m_cached_TextElement.width) / (float)this.m_currentSpriteAsset.spriteSheet.width, vector2.y);
			Vector2 uv = new Vector2(vector3.x, vector.y);
			this.m_textInfo.characterInfo[this.m_characterCount].vertex_BL.uv = vector;
			this.m_textInfo.characterInfo[this.m_characterCount].vertex_TL.uv = vector2;
			this.m_textInfo.characterInfo[this.m_characterCount].vertex_TR.uv = vector3;
			this.m_textInfo.characterInfo[this.m_characterCount].vertex_BR.uv = uv;
		}

		// Token: 0x060003E2 RID: 994 RVA: 0x00024E28 File Offset: 0x00023028
		protected virtual void FillCharacterVertexBuffers(int i, int index_X4)
		{
			int materialReferenceIndex = this.m_textInfo.characterInfo[i].materialReferenceIndex;
			index_X4 = this.m_textInfo.meshInfo[materialReferenceIndex].vertexCount;
			TMP_CharacterInfo[] characterInfo = this.m_textInfo.characterInfo;
			this.m_textInfo.characterInfo[i].vertexIndex = index_X4;
			this.m_textInfo.meshInfo[materialReferenceIndex].vertices[index_X4] = characterInfo[i].vertex_BL.position;
			this.m_textInfo.meshInfo[materialReferenceIndex].vertices[1 + index_X4] = characterInfo[i].vertex_TL.position;
			this.m_textInfo.meshInfo[materialReferenceIndex].vertices[2 + index_X4] = characterInfo[i].vertex_TR.position;
			this.m_textInfo.meshInfo[materialReferenceIndex].vertices[3 + index_X4] = characterInfo[i].vertex_BR.position;
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs0[index_X4] = characterInfo[i].vertex_BL.uv;
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs0[1 + index_X4] = characterInfo[i].vertex_TL.uv;
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs0[2 + index_X4] = characterInfo[i].vertex_TR.uv;
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs0[3 + index_X4] = characterInfo[i].vertex_BR.uv;
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs2[index_X4] = characterInfo[i].vertex_BL.uv2;
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs2[1 + index_X4] = characterInfo[i].vertex_TL.uv2;
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs2[2 + index_X4] = characterInfo[i].vertex_TR.uv2;
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs2[3 + index_X4] = characterInfo[i].vertex_BR.uv2;
			this.m_textInfo.meshInfo[materialReferenceIndex].colors32[index_X4] = characterInfo[i].vertex_BL.color;
			this.m_textInfo.meshInfo[materialReferenceIndex].colors32[1 + index_X4] = characterInfo[i].vertex_TL.color;
			this.m_textInfo.meshInfo[materialReferenceIndex].colors32[2 + index_X4] = characterInfo[i].vertex_TR.color;
			this.m_textInfo.meshInfo[materialReferenceIndex].colors32[3 + index_X4] = characterInfo[i].vertex_BR.color;
			this.m_textInfo.meshInfo[materialReferenceIndex].vertexCount = index_X4 + 4;
		}

		// Token: 0x060003E3 RID: 995 RVA: 0x00025188 File Offset: 0x00023388
		protected virtual void FillCharacterVertexBuffers(int i, int index_X4, bool isVolumetric)
		{
			int materialReferenceIndex = this.m_textInfo.characterInfo[i].materialReferenceIndex;
			index_X4 = this.m_textInfo.meshInfo[materialReferenceIndex].vertexCount;
			TMP_CharacterInfo[] characterInfo = this.m_textInfo.characterInfo;
			this.m_textInfo.characterInfo[i].vertexIndex = index_X4;
			this.m_textInfo.meshInfo[materialReferenceIndex].vertices[index_X4] = characterInfo[i].vertex_BL.position;
			this.m_textInfo.meshInfo[materialReferenceIndex].vertices[1 + index_X4] = characterInfo[i].vertex_TL.position;
			this.m_textInfo.meshInfo[materialReferenceIndex].vertices[2 + index_X4] = characterInfo[i].vertex_TR.position;
			this.m_textInfo.meshInfo[materialReferenceIndex].vertices[3 + index_X4] = characterInfo[i].vertex_BR.position;
			if (isVolumetric)
			{
				Vector3 b = new Vector3(0f, 0f, this.m_fontSize * this.m_fontScale);
				this.m_textInfo.meshInfo[materialReferenceIndex].vertices[4 + index_X4] = characterInfo[i].vertex_BL.position + b;
				this.m_textInfo.meshInfo[materialReferenceIndex].vertices[5 + index_X4] = characterInfo[i].vertex_TL.position + b;
				this.m_textInfo.meshInfo[materialReferenceIndex].vertices[6 + index_X4] = characterInfo[i].vertex_TR.position + b;
				this.m_textInfo.meshInfo[materialReferenceIndex].vertices[7 + index_X4] = characterInfo[i].vertex_BR.position + b;
			}
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs0[index_X4] = characterInfo[i].vertex_BL.uv;
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs0[1 + index_X4] = characterInfo[i].vertex_TL.uv;
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs0[2 + index_X4] = characterInfo[i].vertex_TR.uv;
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs0[3 + index_X4] = characterInfo[i].vertex_BR.uv;
			if (isVolumetric)
			{
				this.m_textInfo.meshInfo[materialReferenceIndex].uvs0[4 + index_X4] = characterInfo[i].vertex_BL.uv;
				this.m_textInfo.meshInfo[materialReferenceIndex].uvs0[5 + index_X4] = characterInfo[i].vertex_TL.uv;
				this.m_textInfo.meshInfo[materialReferenceIndex].uvs0[6 + index_X4] = characterInfo[i].vertex_TR.uv;
				this.m_textInfo.meshInfo[materialReferenceIndex].uvs0[7 + index_X4] = characterInfo[i].vertex_BR.uv;
			}
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs2[index_X4] = characterInfo[i].vertex_BL.uv2;
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs2[1 + index_X4] = characterInfo[i].vertex_TL.uv2;
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs2[2 + index_X4] = characterInfo[i].vertex_TR.uv2;
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs2[3 + index_X4] = characterInfo[i].vertex_BR.uv2;
			if (isVolumetric)
			{
				this.m_textInfo.meshInfo[materialReferenceIndex].uvs2[4 + index_X4] = characterInfo[i].vertex_BL.uv2;
				this.m_textInfo.meshInfo[materialReferenceIndex].uvs2[5 + index_X4] = characterInfo[i].vertex_TL.uv2;
				this.m_textInfo.meshInfo[materialReferenceIndex].uvs2[6 + index_X4] = characterInfo[i].vertex_TR.uv2;
				this.m_textInfo.meshInfo[materialReferenceIndex].uvs2[7 + index_X4] = characterInfo[i].vertex_BR.uv2;
			}
			this.m_textInfo.meshInfo[materialReferenceIndex].colors32[index_X4] = characterInfo[i].vertex_BL.color;
			this.m_textInfo.meshInfo[materialReferenceIndex].colors32[1 + index_X4] = characterInfo[i].vertex_TL.color;
			this.m_textInfo.meshInfo[materialReferenceIndex].colors32[2 + index_X4] = characterInfo[i].vertex_TR.color;
			this.m_textInfo.meshInfo[materialReferenceIndex].colors32[3 + index_X4] = characterInfo[i].vertex_BR.color;
			if (isVolumetric)
			{
				Color32 color = new Color32(byte.MaxValue, byte.MaxValue, 128, byte.MaxValue);
				this.m_textInfo.meshInfo[materialReferenceIndex].colors32[4 + index_X4] = color;
				this.m_textInfo.meshInfo[materialReferenceIndex].colors32[5 + index_X4] = color;
				this.m_textInfo.meshInfo[materialReferenceIndex].colors32[6 + index_X4] = color;
				this.m_textInfo.meshInfo[materialReferenceIndex].colors32[7 + index_X4] = color;
			}
			this.m_textInfo.meshInfo[materialReferenceIndex].vertexCount = index_X4 + ((!isVolumetric) ? 4 : 8);
		}

		// Token: 0x060003E4 RID: 996 RVA: 0x00025808 File Offset: 0x00023A08
		protected virtual void FillSpriteVertexBuffers(int i, int index_X4)
		{
			int materialReferenceIndex = this.m_textInfo.characterInfo[i].materialReferenceIndex;
			index_X4 = this.m_textInfo.meshInfo[materialReferenceIndex].vertexCount;
			TMP_CharacterInfo[] characterInfo = this.m_textInfo.characterInfo;
			this.m_textInfo.characterInfo[i].vertexIndex = index_X4;
			this.m_textInfo.meshInfo[materialReferenceIndex].vertices[index_X4] = characterInfo[i].vertex_BL.position;
			this.m_textInfo.meshInfo[materialReferenceIndex].vertices[1 + index_X4] = characterInfo[i].vertex_TL.position;
			this.m_textInfo.meshInfo[materialReferenceIndex].vertices[2 + index_X4] = characterInfo[i].vertex_TR.position;
			this.m_textInfo.meshInfo[materialReferenceIndex].vertices[3 + index_X4] = characterInfo[i].vertex_BR.position;
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs0[index_X4] = characterInfo[i].vertex_BL.uv;
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs0[1 + index_X4] = characterInfo[i].vertex_TL.uv;
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs0[2 + index_X4] = characterInfo[i].vertex_TR.uv;
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs0[3 + index_X4] = characterInfo[i].vertex_BR.uv;
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs2[index_X4] = characterInfo[i].vertex_BL.uv2;
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs2[1 + index_X4] = characterInfo[i].vertex_TL.uv2;
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs2[2 + index_X4] = characterInfo[i].vertex_TR.uv2;
			this.m_textInfo.meshInfo[materialReferenceIndex].uvs2[3 + index_X4] = characterInfo[i].vertex_BR.uv2;
			this.m_textInfo.meshInfo[materialReferenceIndex].colors32[index_X4] = characterInfo[i].vertex_BL.color;
			this.m_textInfo.meshInfo[materialReferenceIndex].colors32[1 + index_X4] = characterInfo[i].vertex_TL.color;
			this.m_textInfo.meshInfo[materialReferenceIndex].colors32[2 + index_X4] = characterInfo[i].vertex_TR.color;
			this.m_textInfo.meshInfo[materialReferenceIndex].colors32[3 + index_X4] = characterInfo[i].vertex_BR.color;
			this.m_textInfo.meshInfo[materialReferenceIndex].vertexCount = index_X4 + 4;
		}

		// Token: 0x060003E5 RID: 997 RVA: 0x00025B68 File Offset: 0x00023D68
		protected virtual void DrawUnderlineMesh(Vector3 start, Vector3 end, ref int index, float startScale, float endScale, float maxScale, float sdfScale, Color32 underlineColor)
		{
			if (this.m_cached_Underline_GlyphInfo == null)
			{
				if (!TMP_Settings.warningsDisabled)
				{
					Debug.LogWarning("Unable to add underline since the Font Asset doesn't contain the underline character.", this);
				}
				return;
			}
			int num = index + 12;
			if (num > this.m_textInfo.meshInfo[0].vertices.Length)
			{
				this.m_textInfo.meshInfo[0].ResizeMeshInfo(num / 4);
			}
			start.y = Mathf.Min(start.y, end.y);
			end.y = Mathf.Min(start.y, end.y);
			float num2 = this.m_cached_Underline_GlyphInfo.width / 2f * maxScale;
			if (end.x - start.x < this.m_cached_Underline_GlyphInfo.width * maxScale)
			{
				num2 = (end.x - start.x) / 2f;
			}
			float num3 = this.m_padding * startScale / maxScale;
			float num4 = this.m_padding * endScale / maxScale;
			float height = this.m_cached_Underline_GlyphInfo.height;
			Vector3[] vertices = this.m_textInfo.meshInfo[0].vertices;
			vertices[index] = start + new Vector3(0f, 0f - (height + this.m_padding) * maxScale, 0f);
			vertices[index + 1] = start + new Vector3(0f, this.m_padding * maxScale, 0f);
			vertices[index + 2] = vertices[index + 1] + new Vector3(num2, 0f, 0f);
			vertices[index + 3] = vertices[index] + new Vector3(num2, 0f, 0f);
			vertices[index + 4] = vertices[index + 3];
			vertices[index + 5] = vertices[index + 2];
			vertices[index + 6] = end + new Vector3(-num2, this.m_padding * maxScale, 0f);
			vertices[index + 7] = end + new Vector3(-num2, -(height + this.m_padding) * maxScale, 0f);
			vertices[index + 8] = vertices[index + 7];
			vertices[index + 9] = vertices[index + 6];
			vertices[index + 10] = end + new Vector3(0f, this.m_padding * maxScale, 0f);
			vertices[index + 11] = end + new Vector3(0f, -(height + this.m_padding) * maxScale, 0f);
			Vector2[] uvs = this.m_textInfo.meshInfo[0].uvs0;
			Vector2 vector = new Vector2((this.m_cached_Underline_GlyphInfo.x - num3) / this.m_fontAsset.fontInfo.AtlasWidth, 1f - (this.m_cached_Underline_GlyphInfo.y + this.m_padding + this.m_cached_Underline_GlyphInfo.height) / this.m_fontAsset.fontInfo.AtlasHeight);
			Vector2 vector2 = new Vector2(vector.x, 1f - (this.m_cached_Underline_GlyphInfo.y - this.m_padding) / this.m_fontAsset.fontInfo.AtlasHeight);
			Vector2 vector3 = new Vector2((this.m_cached_Underline_GlyphInfo.x - num3 + this.m_cached_Underline_GlyphInfo.width / 2f) / this.m_fontAsset.fontInfo.AtlasWidth, vector2.y);
			Vector2 vector4 = new Vector2(vector3.x, vector.y);
			Vector2 vector5 = new Vector2((this.m_cached_Underline_GlyphInfo.x + num4 + this.m_cached_Underline_GlyphInfo.width / 2f) / this.m_fontAsset.fontInfo.AtlasWidth, vector2.y);
			Vector2 vector6 = new Vector2(vector5.x, vector.y);
			Vector2 vector7 = new Vector2((this.m_cached_Underline_GlyphInfo.x + num4 + this.m_cached_Underline_GlyphInfo.width) / this.m_fontAsset.fontInfo.AtlasWidth, vector2.y);
			Vector2 vector8 = new Vector2(vector7.x, vector.y);
			uvs[index] = vector;
			uvs[1 + index] = vector2;
			uvs[2 + index] = vector3;
			uvs[3 + index] = vector4;
			uvs[4 + index] = new Vector2(vector3.x - vector3.x * 0.001f, vector.y);
			uvs[5 + index] = new Vector2(vector3.x - vector3.x * 0.001f, vector2.y);
			uvs[6 + index] = new Vector2(vector3.x + vector3.x * 0.001f, vector2.y);
			uvs[7 + index] = new Vector2(vector3.x + vector3.x * 0.001f, vector.y);
			uvs[8 + index] = vector6;
			uvs[9 + index] = vector5;
			uvs[10 + index] = vector7;
			uvs[11 + index] = vector8;
			float x = (vertices[index + 2].x - start.x) / (end.x - start.x);
			float scale = Mathf.Abs(sdfScale);
			Vector2[] uvs2 = this.m_textInfo.meshInfo[0].uvs2;
			uvs2[index] = this.PackUV(0f, 0f, scale);
			uvs2[1 + index] = this.PackUV(0f, 1f, scale);
			uvs2[2 + index] = this.PackUV(x, 1f, scale);
			uvs2[3 + index] = this.PackUV(x, 0f, scale);
			float x2 = (vertices[index + 4].x - start.x) / (end.x - start.x);
			x = (vertices[index + 6].x - start.x) / (end.x - start.x);
			uvs2[4 + index] = this.PackUV(x2, 0f, scale);
			uvs2[5 + index] = this.PackUV(x2, 1f, scale);
			uvs2[6 + index] = this.PackUV(x, 1f, scale);
			uvs2[7 + index] = this.PackUV(x, 0f, scale);
			x2 = (vertices[index + 8].x - start.x) / (end.x - start.x);
			x = (vertices[index + 6].x - start.x) / (end.x - start.x);
			uvs2[8 + index] = this.PackUV(x2, 0f, scale);
			uvs2[9 + index] = this.PackUV(x2, 1f, scale);
			uvs2[10 + index] = this.PackUV(1f, 1f, scale);
			uvs2[11 + index] = this.PackUV(1f, 0f, scale);
			underlineColor.a = ((this.m_fontColor32.a < underlineColor.a) ? this.m_fontColor32.a : underlineColor.a);
			Color32[] colors = this.m_textInfo.meshInfo[0].colors32;
			colors[index] = underlineColor;
			colors[1 + index] = underlineColor;
			colors[2 + index] = underlineColor;
			colors[3 + index] = underlineColor;
			colors[4 + index] = underlineColor;
			colors[5 + index] = underlineColor;
			colors[6 + index] = underlineColor;
			colors[7 + index] = underlineColor;
			colors[8 + index] = underlineColor;
			colors[9 + index] = underlineColor;
			colors[10 + index] = underlineColor;
			colors[11 + index] = underlineColor;
			index += 12;
		}

		// Token: 0x060003E6 RID: 998 RVA: 0x000263D0 File Offset: 0x000245D0
		protected virtual void DrawTextHighlight(Vector3 start, Vector3 end, ref int index, Color32 highlightColor)
		{
			if (this.m_cached_Underline_GlyphInfo == null)
			{
				if (!TMP_Settings.warningsDisabled)
				{
					Debug.LogWarning("Unable to add underline since the Font Asset doesn't contain the underline character.", this);
				}
				return;
			}
			int num = index + 4;
			if (num > this.m_textInfo.meshInfo[0].vertices.Length)
			{
				this.m_textInfo.meshInfo[0].ResizeMeshInfo(num / 4);
			}
			Vector3[] vertices = this.m_textInfo.meshInfo[0].vertices;
			vertices[index] = start;
			vertices[index + 1] = new Vector3(start.x, end.y, 0f);
			vertices[index + 2] = end;
			vertices[index + 3] = new Vector3(end.x, start.y, 0f);
			Vector2[] uvs = this.m_textInfo.meshInfo[0].uvs0;
			Vector2 vector = new Vector2((this.m_cached_Underline_GlyphInfo.x + this.m_cached_Underline_GlyphInfo.width / 2f) / this.m_fontAsset.fontInfo.AtlasWidth, 1f - (this.m_cached_Underline_GlyphInfo.y + this.m_cached_Underline_GlyphInfo.height / 2f) / this.m_fontAsset.fontInfo.AtlasHeight);
			uvs[index] = vector;
			uvs[1 + index] = vector;
			uvs[2 + index] = vector;
			uvs[3 + index] = vector;
			Vector2[] uvs2 = this.m_textInfo.meshInfo[0].uvs2;
			Vector2 vector2 = new Vector2(0f, 1f);
			uvs2[index] = vector2;
			uvs2[1 + index] = vector2;
			uvs2[2 + index] = vector2;
			uvs2[3 + index] = vector2;
			highlightColor.a = ((this.m_fontColor32.a < highlightColor.a) ? this.m_fontColor32.a : highlightColor.a);
			Color32[] colors = this.m_textInfo.meshInfo[0].colors32;
			colors[index] = highlightColor;
			colors[1 + index] = highlightColor;
			colors[2 + index] = highlightColor;
			colors[3 + index] = highlightColor;
			index += 4;
		}

		// Token: 0x060003E7 RID: 999 RVA: 0x0002660C File Offset: 0x0002480C
		protected void LoadDefaultSettings()
		{
			if (this.m_text == null || this.m_isWaitingOnResourceLoad)
			{
				if (TMP_Settings.autoSizeTextContainer)
				{
					this.autoSizeTextContainer = true;
				}
				else
				{
					this.m_rectTransform = this.rectTransform;
					if (base.GetType() == typeof(TextMeshPro))
					{
						this.m_rectTransform.sizeDelta = TMP_Settings.defaultTextMeshProTextContainerSize;
					}
					else
					{
						this.m_rectTransform.sizeDelta = TMP_Settings.defaultTextMeshProUITextContainerSize;
					}
				}
				this.m_enableWordWrapping = TMP_Settings.enableWordWrapping;
				this.m_enableKerning = TMP_Settings.enableKerning;
				this.m_enableExtraPadding = TMP_Settings.enableExtraPadding;
				this.m_tintAllSprites = TMP_Settings.enableTintAllSprites;
				this.m_parseCtrlCharacters = TMP_Settings.enableParseEscapeCharacters;
				this.m_fontSize = (this.m_fontSizeBase = TMP_Settings.defaultFontSize);
				this.m_fontSizeMin = this.m_fontSize * TMP_Settings.defaultTextAutoSizingMinRatio;
				this.m_fontSizeMax = this.m_fontSize * TMP_Settings.defaultTextAutoSizingMaxRatio;
				this.m_isAlignmentEnumConverted = true;
				this.m_isWaitingOnResourceLoad = false;
				return;
			}
			if (!this.m_isAlignmentEnumConverted)
			{
				this.m_isAlignmentEnumConverted = true;
				this.m_textAlignment = TMP_Compatibility.ConvertTextAlignmentEnumValues(this.m_textAlignment);
			}
		}

		// Token: 0x060003E8 RID: 1000 RVA: 0x0002671F File Offset: 0x0002491F
		protected void GetSpecialCharacters(TMP_FontAsset fontAsset)
		{
			fontAsset.characterDictionary.TryGetValue(95, out this.m_cached_Underline_GlyphInfo);
			fontAsset.characterDictionary.TryGetValue(8230, out this.m_cached_Ellipsis_GlyphInfo);
		}

		// Token: 0x060003E9 RID: 1001 RVA: 0x0002674C File Offset: 0x0002494C
		protected void ReplaceTagWithCharacter(int[] chars, int insertionIndex, int tagLength, char c)
		{
			chars[insertionIndex] = (int)c;
			for (int i = insertionIndex + tagLength; i < chars.Length; i++)
			{
				chars[i - 3] = chars[i];
			}
		}

		// Token: 0x060003EA RID: 1002 RVA: 0x00026778 File Offset: 0x00024978
		protected TMP_FontAsset GetFontAssetForWeight(int fontWeight)
		{
			bool flag = (this.m_style & FontStyles.Italic) == FontStyles.Italic || (this.m_fontStyle & FontStyles.Italic) == FontStyles.Italic;
			int num = fontWeight / 100;
			TMP_FontAsset result;
			if (flag)
			{
				result = this.m_currentFontAsset.fontWeights[num].italicTypeface;
			}
			else
			{
				result = this.m_currentFontAsset.fontWeights[num].regularTypeface;
			}
			return result;
		}

		// Token: 0x060003EB RID: 1003 RVA: 0x0000297D File Offset: 0x00000B7D
		protected virtual void SetActiveSubMeshes(bool state)
		{
		}

		// Token: 0x060003EC RID: 1004 RVA: 0x0000297D File Offset: 0x00000B7D
		protected virtual void ClearSubMeshObjects()
		{
		}

		// Token: 0x060003ED RID: 1005 RVA: 0x0000297D File Offset: 0x00000B7D
		public virtual void ClearMesh()
		{
		}

		// Token: 0x060003EE RID: 1006 RVA: 0x0000297D File Offset: 0x00000B7D
		public virtual void ClearMesh(bool uploadGeometry)
		{
		}

		// Token: 0x060003EF RID: 1007 RVA: 0x000267D8 File Offset: 0x000249D8
		public virtual string GetParsedText()
		{
			if (this.m_textInfo == null)
			{
				return string.Empty;
			}
			int characterCount = this.m_textInfo.characterCount;
			char[] array = new char[characterCount];
			int num = 0;
			while (num < characterCount && num < this.m_textInfo.characterInfo.Length)
			{
				array[num] = this.m_textInfo.characterInfo[num].character;
				num++;
			}
			return new string(array);
		}

		// Token: 0x060003F0 RID: 1008 RVA: 0x00026844 File Offset: 0x00024A44
		protected Vector2 PackUV(float x, float y, float scale)
		{
			Vector2 vector;
			vector.x = (float)((int)(x * 511f));
			vector.y = (float)((int)(y * 511f));
			vector.x = vector.x * 4096f + vector.y;
			vector.y = scale;
			return vector;
		}

		// Token: 0x060003F1 RID: 1009 RVA: 0x00026894 File Offset: 0x00024A94
		protected float PackUV(float x, float y)
		{
			float num = (float)((double)((int)(x * 511f)));
			double num2 = (double)((int)(y * 511f));
			return (float)((double)num * 4096.0 + num2);
		}

		// Token: 0x060003F2 RID: 1010 RVA: 0x000268C4 File Offset: 0x00024AC4
		protected int HexToInt(char hex)
		{
			switch (hex)
			{
			case '0':
				return 0;
			case '1':
				return 1;
			case '2':
				return 2;
			case '3':
				return 3;
			case '4':
				return 4;
			case '5':
				return 5;
			case '6':
				return 6;
			case '7':
				return 7;
			case '8':
				return 8;
			case '9':
				return 9;
			case ':':
			case ';':
			case '<':
			case '=':
			case '>':
			case '?':
			case '@':
				break;
			case 'A':
				return 10;
			case 'B':
				return 11;
			case 'C':
				return 12;
			case 'D':
				return 13;
			case 'E':
				return 14;
			case 'F':
				return 15;
			default:
				switch (hex)
				{
				case 'a':
					return 10;
				case 'b':
					return 11;
				case 'c':
					return 12;
				case 'd':
					return 13;
				case 'e':
					return 14;
				case 'f':
					return 15;
				}
				break;
			}
			return 15;
		}

		// Token: 0x060003F3 RID: 1011 RVA: 0x00026994 File Offset: 0x00024B94
		protected int GetUTF16(string text, int i)
		{
			return 0 + (this.HexToInt(text[i]) << 12) + (this.HexToInt(text[i + 1]) << 8) + (this.HexToInt(text[i + 2]) << 4) + this.HexToInt(text[i + 3]);
		}

		// Token: 0x060003F4 RID: 1012 RVA: 0x000269E8 File Offset: 0x00024BE8
		protected int GetUTF16(StringBuilder text, int i)
		{
			return 0 + (this.HexToInt(text[i]) << 12) + (this.HexToInt(text[i + 1]) << 8) + (this.HexToInt(text[i + 2]) << 4) + this.HexToInt(text[i + 3]);
		}

		// Token: 0x060003F5 RID: 1013 RVA: 0x00026A3C File Offset: 0x00024C3C
		protected int GetUTF32(string text, int i)
		{
			return 0 + (this.HexToInt(text[i]) << 30) + (this.HexToInt(text[i + 1]) << 24) + (this.HexToInt(text[i + 2]) << 20) + (this.HexToInt(text[i + 3]) << 16) + (this.HexToInt(text[i + 4]) << 12) + (this.HexToInt(text[i + 5]) << 8) + (this.HexToInt(text[i + 6]) << 4) + this.HexToInt(text[i + 7]);
		}

		// Token: 0x060003F6 RID: 1014 RVA: 0x00026ADC File Offset: 0x00024CDC
		protected int GetUTF32(StringBuilder text, int i)
		{
			return 0 + (this.HexToInt(text[i]) << 30) + (this.HexToInt(text[i + 1]) << 24) + (this.HexToInt(text[i + 2]) << 20) + (this.HexToInt(text[i + 3]) << 16) + (this.HexToInt(text[i + 4]) << 12) + (this.HexToInt(text[i + 5]) << 8) + (this.HexToInt(text[i + 6]) << 4) + this.HexToInt(text[i + 7]);
		}

		// Token: 0x060003F7 RID: 1015 RVA: 0x00026B7C File Offset: 0x00024D7C
		protected Color32 HexCharsToColor(char[] hexChars, int tagCount)
		{
			if (tagCount == 4)
			{
				byte r = (byte)(this.HexToInt(hexChars[1]) * 16 + this.HexToInt(hexChars[1]));
				byte g = (byte)(this.HexToInt(hexChars[2]) * 16 + this.HexToInt(hexChars[2]));
				byte b = (byte)(this.HexToInt(hexChars[3]) * 16 + this.HexToInt(hexChars[3]));
				return new Color32(r, g, b, byte.MaxValue);
			}
			if (tagCount == 5)
			{
				byte r2 = (byte)(this.HexToInt(hexChars[1]) * 16 + this.HexToInt(hexChars[1]));
				byte g2 = (byte)(this.HexToInt(hexChars[2]) * 16 + this.HexToInt(hexChars[2]));
				byte b2 = (byte)(this.HexToInt(hexChars[3]) * 16 + this.HexToInt(hexChars[3]));
				byte a = (byte)(this.HexToInt(hexChars[4]) * 16 + this.HexToInt(hexChars[4]));
				return new Color32(r2, g2, b2, a);
			}
			if (tagCount == 7)
			{
				byte r3 = (byte)(this.HexToInt(hexChars[1]) * 16 + this.HexToInt(hexChars[2]));
				byte g3 = (byte)(this.HexToInt(hexChars[3]) * 16 + this.HexToInt(hexChars[4]));
				byte b3 = (byte)(this.HexToInt(hexChars[5]) * 16 + this.HexToInt(hexChars[6]));
				return new Color32(r3, g3, b3, byte.MaxValue);
			}
			if (tagCount == 9)
			{
				byte r4 = (byte)(this.HexToInt(hexChars[1]) * 16 + this.HexToInt(hexChars[2]));
				byte g4 = (byte)(this.HexToInt(hexChars[3]) * 16 + this.HexToInt(hexChars[4]));
				byte b4 = (byte)(this.HexToInt(hexChars[5]) * 16 + this.HexToInt(hexChars[6]));
				byte a2 = (byte)(this.HexToInt(hexChars[7]) * 16 + this.HexToInt(hexChars[8]));
				return new Color32(r4, g4, b4, a2);
			}
			if (tagCount == 10)
			{
				byte r5 = (byte)(this.HexToInt(hexChars[7]) * 16 + this.HexToInt(hexChars[7]));
				byte g5 = (byte)(this.HexToInt(hexChars[8]) * 16 + this.HexToInt(hexChars[8]));
				byte b5 = (byte)(this.HexToInt(hexChars[9]) * 16 + this.HexToInt(hexChars[9]));
				return new Color32(r5, g5, b5, byte.MaxValue);
			}
			if (tagCount == 11)
			{
				byte r6 = (byte)(this.HexToInt(hexChars[7]) * 16 + this.HexToInt(hexChars[7]));
				byte g6 = (byte)(this.HexToInt(hexChars[8]) * 16 + this.HexToInt(hexChars[8]));
				byte b6 = (byte)(this.HexToInt(hexChars[9]) * 16 + this.HexToInt(hexChars[9]));
				byte a3 = (byte)(this.HexToInt(hexChars[10]) * 16 + this.HexToInt(hexChars[10]));
				return new Color32(r6, g6, b6, a3);
			}
			if (tagCount == 13)
			{
				byte r7 = (byte)(this.HexToInt(hexChars[7]) * 16 + this.HexToInt(hexChars[8]));
				byte g7 = (byte)(this.HexToInt(hexChars[9]) * 16 + this.HexToInt(hexChars[10]));
				byte b7 = (byte)(this.HexToInt(hexChars[11]) * 16 + this.HexToInt(hexChars[12]));
				return new Color32(r7, g7, b7, byte.MaxValue);
			}
			if (tagCount == 15)
			{
				byte r8 = (byte)(this.HexToInt(hexChars[7]) * 16 + this.HexToInt(hexChars[8]));
				byte g8 = (byte)(this.HexToInt(hexChars[9]) * 16 + this.HexToInt(hexChars[10]));
				byte b8 = (byte)(this.HexToInt(hexChars[11]) * 16 + this.HexToInt(hexChars[12]));
				byte a4 = (byte)(this.HexToInt(hexChars[13]) * 16 + this.HexToInt(hexChars[14]));
				return new Color32(r8, g8, b8, a4);
			}
			return new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
		}

		// Token: 0x060003F8 RID: 1016 RVA: 0x00026EE8 File Offset: 0x000250E8
		protected Color32 HexCharsToColor(char[] hexChars, int startIndex, int length)
		{
			if (length == 7)
			{
				byte r = (byte)(this.HexToInt(hexChars[startIndex + 1]) * 16 + this.HexToInt(hexChars[startIndex + 2]));
				byte g = (byte)(this.HexToInt(hexChars[startIndex + 3]) * 16 + this.HexToInt(hexChars[startIndex + 4]));
				byte b = (byte)(this.HexToInt(hexChars[startIndex + 5]) * 16 + this.HexToInt(hexChars[startIndex + 6]));
				return new Color32(r, g, b, byte.MaxValue);
			}
			if (length == 9)
			{
				byte r2 = (byte)(this.HexToInt(hexChars[startIndex + 1]) * 16 + this.HexToInt(hexChars[startIndex + 2]));
				byte g2 = (byte)(this.HexToInt(hexChars[startIndex + 3]) * 16 + this.HexToInt(hexChars[startIndex + 4]));
				byte b2 = (byte)(this.HexToInt(hexChars[startIndex + 5]) * 16 + this.HexToInt(hexChars[startIndex + 6]));
				byte a = (byte)(this.HexToInt(hexChars[startIndex + 7]) * 16 + this.HexToInt(hexChars[startIndex + 8]));
				return new Color32(r2, g2, b2, a);
			}
			return TMP_Text.s_colorWhite;
		}

		// Token: 0x060003F9 RID: 1017 RVA: 0x00026FE0 File Offset: 0x000251E0
		private int GetAttributeParameters(char[] chars, int startIndex, int length, ref float[] parameters)
		{
			int i = startIndex;
			int num = 0;
			while (i < startIndex + length)
			{
				parameters[num] = this.ConvertToFloat(chars, startIndex, length, out i);
				length -= i - startIndex + 1;
				startIndex = i + 1;
				num++;
			}
			return num;
		}

		// Token: 0x060003FA RID: 1018 RVA: 0x0002701C File Offset: 0x0002521C
		protected float ConvertToFloat(char[] chars, int startIndex, int length)
		{
			int num = 0;
			return this.ConvertToFloat(chars, startIndex, length, out num);
		}

		// Token: 0x060003FB RID: 1019 RVA: 0x00027038 File Offset: 0x00025238
		protected float ConvertToFloat(char[] chars, int startIndex, int length, out int lastIndex)
		{
			if (startIndex == 0)
			{
				lastIndex = 0;
				return -9999f;
			}
			int num = startIndex + length;
			bool flag = true;
			float num2 = 0f;
			int num3 = 1;
			if (chars[startIndex] == '+')
			{
				num3 = 1;
				startIndex++;
			}
			else if (chars[startIndex] == '-')
			{
				num3 = -1;
				startIndex++;
			}
			float num4 = 0f;
			for (int i = startIndex; i < num; i++)
			{
				uint num5 = (uint)chars[i];
				if ((num5 >= 48U && num5 <= 57U) || num5 == 46U)
				{
					if (num5 == 46U)
					{
						flag = false;
						num2 = 0.1f;
					}
					else if (flag)
					{
						num4 = num4 * 10f + (float)((ulong)(num5 - 48U) * (ulong)((long)num3));
					}
					else
					{
						num4 += (num5 - 48U) * num2 * (float)num3;
						num2 *= 0.1f;
					}
				}
				else if (num5 == 44U)
				{
					if (i + 1 < num && chars[i + 1] == ' ')
					{
						lastIndex = i + 1;
					}
					else
					{
						lastIndex = i;
					}
					return num4;
				}
			}
			lastIndex = num;
			return num4;
		}

		// Token: 0x060003FC RID: 1020 RVA: 0x00027128 File Offset: 0x00025328
		protected bool ValidateHtmlTag(int[] chars, int startIndex, out int endIndex)
		{
			int num = 0;
			byte b = 0;
			TagUnits tagUnits = TagUnits.Pixels;
			TagType tagType = TagType.None;
			int num2 = 0;
			this.m_xmlAttribute[num2].nameHashCode = 0;
			this.m_xmlAttribute[num2].valueType = TagType.None;
			this.m_xmlAttribute[num2].valueHashCode = 0;
			this.m_xmlAttribute[num2].valueStartIndex = 0;
			this.m_xmlAttribute[num2].valueLength = 0;
			this.m_xmlAttribute[1].nameHashCode = 0;
			this.m_xmlAttribute[2].nameHashCode = 0;
			this.m_xmlAttribute[3].nameHashCode = 0;
			this.m_xmlAttribute[4].nameHashCode = 0;
			endIndex = startIndex;
			bool flag = false;
			bool flag2 = false;
			int num3 = startIndex;
			while (num3 < chars.Length && chars[num3] != 0 && num < this.m_htmlTag.Length && chars[num3] != 60)
			{
				if (chars[num3] == 62)
				{
					flag2 = true;
					endIndex = num3;
					this.m_htmlTag[num] = '\0';
					break;
				}
				this.m_htmlTag[num] = (char)chars[num3];
				num++;
				if (b == 1)
				{
					if (tagType == TagType.None)
					{
						if (chars[num3] == 43 || chars[num3] == 45 || chars[num3] == 46 || (chars[num3] >= 48 && chars[num3] <= 57))
						{
							tagType = TagType.NumericalValue;
							this.m_xmlAttribute[num2].valueType = TagType.NumericalValue;
							this.m_xmlAttribute[num2].valueStartIndex = num - 1;
							XML_TagAttribute[] xmlAttribute = this.m_xmlAttribute;
							int num4 = num2;
							xmlAttribute[num4].valueLength = xmlAttribute[num4].valueLength + 1;
						}
						else if (chars[num3] == 35)
						{
							tagType = TagType.ColorValue;
							this.m_xmlAttribute[num2].valueType = TagType.ColorValue;
							this.m_xmlAttribute[num2].valueStartIndex = num - 1;
							XML_TagAttribute[] xmlAttribute2 = this.m_xmlAttribute;
							int num5 = num2;
							xmlAttribute2[num5].valueLength = xmlAttribute2[num5].valueLength + 1;
						}
						else if (chars[num3] == 34)
						{
							tagType = TagType.StringValue;
							this.m_xmlAttribute[num2].valueType = TagType.StringValue;
							this.m_xmlAttribute[num2].valueStartIndex = num;
						}
						else
						{
							tagType = TagType.StringValue;
							this.m_xmlAttribute[num2].valueType = TagType.StringValue;
							this.m_xmlAttribute[num2].valueStartIndex = num - 1;
							this.m_xmlAttribute[num2].valueHashCode = ((this.m_xmlAttribute[num2].valueHashCode << 5) + this.m_xmlAttribute[num2].valueHashCode ^ chars[num3]);
							XML_TagAttribute[] xmlAttribute3 = this.m_xmlAttribute;
							int num6 = num2;
							xmlAttribute3[num6].valueLength = xmlAttribute3[num6].valueLength + 1;
						}
					}
					else if (tagType == TagType.NumericalValue)
					{
						if (chars[num3] == 112 || chars[num3] == 101 || chars[num3] == 37 || chars[num3] == 32)
						{
							b = 2;
							tagType = TagType.None;
							num2++;
							this.m_xmlAttribute[num2].nameHashCode = 0;
							this.m_xmlAttribute[num2].valueType = TagType.None;
							this.m_xmlAttribute[num2].valueHashCode = 0;
							this.m_xmlAttribute[num2].valueStartIndex = 0;
							this.m_xmlAttribute[num2].valueLength = 0;
							if (chars[num3] == 101)
							{
								tagUnits = TagUnits.FontUnits;
							}
							else if (chars[num3] == 37)
							{
								tagUnits = TagUnits.Percentage;
							}
						}
						else if (b != 2)
						{
							XML_TagAttribute[] xmlAttribute4 = this.m_xmlAttribute;
							int num7 = num2;
							xmlAttribute4[num7].valueLength = xmlAttribute4[num7].valueLength + 1;
						}
					}
					else if (tagType == TagType.ColorValue)
					{
						if (chars[num3] != 32)
						{
							XML_TagAttribute[] xmlAttribute5 = this.m_xmlAttribute;
							int num8 = num2;
							xmlAttribute5[num8].valueLength = xmlAttribute5[num8].valueLength + 1;
						}
						else
						{
							b = 2;
							tagType = TagType.None;
							num2++;
							this.m_xmlAttribute[num2].nameHashCode = 0;
							this.m_xmlAttribute[num2].valueType = TagType.None;
							this.m_xmlAttribute[num2].valueHashCode = 0;
							this.m_xmlAttribute[num2].valueStartIndex = 0;
							this.m_xmlAttribute[num2].valueLength = 0;
						}
					}
					else if (tagType == TagType.StringValue)
					{
						if (chars[num3] != 34)
						{
							this.m_xmlAttribute[num2].valueHashCode = ((this.m_xmlAttribute[num2].valueHashCode << 5) + this.m_xmlAttribute[num2].valueHashCode ^ chars[num3]);
							XML_TagAttribute[] xmlAttribute6 = this.m_xmlAttribute;
							int num9 = num2;
							xmlAttribute6[num9].valueLength = xmlAttribute6[num9].valueLength + 1;
						}
						else
						{
							b = 2;
							tagType = TagType.None;
							num2++;
							this.m_xmlAttribute[num2].nameHashCode = 0;
							this.m_xmlAttribute[num2].valueType = TagType.None;
							this.m_xmlAttribute[num2].valueHashCode = 0;
							this.m_xmlAttribute[num2].valueStartIndex = 0;
							this.m_xmlAttribute[num2].valueLength = 0;
						}
					}
				}
				if (chars[num3] == 61)
				{
					b = 1;
				}
				if (b == 0 && chars[num3] == 32)
				{
					if (flag)
					{
						return false;
					}
					flag = true;
					b = 2;
					tagType = TagType.None;
					num2++;
					this.m_xmlAttribute[num2].nameHashCode = 0;
					this.m_xmlAttribute[num2].valueType = TagType.None;
					this.m_xmlAttribute[num2].valueHashCode = 0;
					this.m_xmlAttribute[num2].valueStartIndex = 0;
					this.m_xmlAttribute[num2].valueLength = 0;
				}
				if (b == 0)
				{
					this.m_xmlAttribute[num2].nameHashCode = (this.m_xmlAttribute[num2].nameHashCode << 3) - this.m_xmlAttribute[num2].nameHashCode + chars[num3];
				}
				if (b == 2 && chars[num3] == 32)
				{
					b = 0;
				}
				num3++;
			}
			if (!flag2)
			{
				return false;
			}
			if (this.tag_NoParsing && this.m_xmlAttribute[0].nameHashCode != 53822163 && this.m_xmlAttribute[0].nameHashCode != 49429939)
			{
				return false;
			}
			if (this.m_xmlAttribute[0].nameHashCode == 53822163 || this.m_xmlAttribute[0].nameHashCode == 49429939)
			{
				this.tag_NoParsing = false;
				return true;
			}
			if (this.m_htmlTag[0] == '#' && num == 4)
			{
				this.m_htmlColor = this.HexCharsToColor(this.m_htmlTag, num);
				this.m_colorStack.Add(this.m_htmlColor);
				return true;
			}
			if (this.m_htmlTag[0] == '#' && num == 5)
			{
				this.m_htmlColor = this.HexCharsToColor(this.m_htmlTag, num);
				this.m_colorStack.Add(this.m_htmlColor);
				return true;
			}
			if (this.m_htmlTag[0] == '#' && num == 7)
			{
				this.m_htmlColor = this.HexCharsToColor(this.m_htmlTag, num);
				this.m_colorStack.Add(this.m_htmlColor);
				return true;
			}
			if (this.m_htmlTag[0] == '#' && num == 9)
			{
				this.m_htmlColor = this.HexCharsToColor(this.m_htmlTag, num);
				this.m_colorStack.Add(this.m_htmlColor);
				return true;
			}
			int num10 = this.m_xmlAttribute[0].nameHashCode;
			float num13;
			if (num10 <= 186622)
			{
				if (num10 <= 2963)
				{
					if (num10 > 98)
					{
						if (num10 <= 434)
						{
							if (num10 <= 402)
							{
								if (num10 <= 115)
								{
									if (num10 == 105)
									{
										goto IL_129F;
									}
									if (num10 != 115)
									{
										return false;
									}
									goto IL_12DB;
								}
								else
								{
									if (num10 == 117)
									{
										goto IL_13E5;
									}
									if (num10 != 395)
									{
										if (num10 != 402)
										{
											return false;
										}
										goto IL_12BC;
									}
								}
							}
							else if (num10 <= 414)
							{
								if (num10 == 412)
								{
									goto IL_13B8;
								}
								if (num10 != 414)
								{
									return false;
								}
								goto IL_14C0;
							}
							else
							{
								if (num10 == 426)
								{
									return true;
								}
								if (num10 != 427)
								{
									if (num10 != 434)
									{
										return false;
									}
									goto IL_12BC;
								}
							}
							if ((this.m_fontStyle & FontStyles.Bold) != FontStyles.Bold)
							{
								this.m_fontWeightInternal = this.m_fontWeightStack.Remove();
								if (this.m_fontStyleStack.Remove(FontStyles.Bold) == 0)
								{
									this.m_style &= (FontStyles)(-2);
								}
							}
							return true;
							IL_12BC:
							if (this.m_fontStyleStack.Remove(FontStyles.Italic) == 0)
							{
								this.m_style &= (FontStyles)(-3);
							}
							return true;
						}
						if (num10 <= 670)
						{
							if (num10 <= 446)
							{
								if (num10 == 444)
								{
									goto IL_13B8;
								}
								if (num10 != 446)
								{
									return false;
								}
								goto IL_14C0;
							}
							else
							{
								if (num10 == 656)
								{
									goto IL_3906;
								}
								if (num10 == 660)
								{
									return true;
								}
								if (num10 != 670)
								{
									return false;
								}
							}
						}
						else if (num10 <= 916)
						{
							if (num10 == 912)
							{
								goto IL_3906;
							}
							if (num10 != 916)
							{
								return false;
							}
							return true;
						}
						else if (num10 != 926)
						{
							if (num10 == 2959)
							{
								return true;
							}
							if (num10 != 2963)
							{
								return false;
							}
							return true;
						}
						return true;
						IL_3906:
						int num11 = 1;
						while (num11 < this.m_xmlAttribute.Length && this.m_xmlAttribute[num11].nameHashCode != 0)
						{
							num10 = this.m_xmlAttribute[num11].nameHashCode;
							if (num10 != 275917)
							{
								if (num10 == 327550)
								{
									float num12 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[num11].valueStartIndex, this.m_xmlAttribute[num11].valueLength);
									switch (tagUnits)
									{
									case TagUnits.Pixels:
										Debug.Log("Table width = " + num12 + "px.");
										break;
									case TagUnits.FontUnits:
										Debug.Log("Table width = " + num12 + "em.");
										break;
									case TagUnits.Percentage:
										Debug.Log("Table width = " + num12 + "%.");
										break;
									}
								}
							}
							else
							{
								num10 = this.m_xmlAttribute[num11].valueHashCode;
								if (num10 <= -458210101)
								{
									if (num10 != -523808257)
									{
										if (num10 == -458210101)
										{
											Debug.Log("TD align=\"center\".");
										}
									}
									else
									{
										Debug.Log("TD align=\"justified\".");
									}
								}
								else if (num10 != 3774683)
								{
									if (num10 == 136703040)
									{
										Debug.Log("TD align=\"right\".");
									}
								}
								else
								{
									Debug.Log("TD align=\"left\".");
								}
							}
							num11++;
						}
						return true;
						IL_13B8:
						if ((this.m_fontStyle & FontStyles.Strikethrough) != FontStyles.Strikethrough && this.m_fontStyleStack.Remove(FontStyles.Strikethrough) == 0)
						{
							this.m_style &= (FontStyles)(-65);
						}
						return true;
						IL_14C0:
						if ((this.m_fontStyle & FontStyles.Underline) != FontStyles.Underline)
						{
							this.m_underlineColor = this.m_underlineColorStack.Remove();
							if (this.m_fontStyleStack.Remove(FontStyles.Underline) == 0)
							{
								this.m_style &= (FontStyles)(-5);
							}
						}
						return true;
					}
					if (num10 <= -855002522)
					{
						if (num10 <= -1690034531)
						{
							if (num10 <= -1883544150)
							{
								if (num10 == -1885698441)
								{
									goto IL_19AD;
								}
								if (num10 != -1883544150)
								{
									return false;
								}
							}
							else
							{
								if (num10 == -1847322671)
								{
									goto IL_32F5;
								}
								if (num10 == -1831660941)
								{
									goto IL_32B6;
								}
								if (num10 != -1690034531)
								{
									return false;
								}
								goto IL_34F9;
							}
						}
						else if (num10 <= -1632103439)
						{
							if (num10 != -1668324918)
							{
								if (num10 != -1632103439)
								{
									return false;
								}
								goto IL_32F5;
							}
						}
						else
						{
							if (num10 == -1616441709)
							{
								goto IL_32B6;
							}
							if (num10 == -884817987)
							{
								goto IL_34F9;
							}
							if (num10 != -855002522)
							{
								return false;
							}
							goto IL_3419;
						}
						if (this.m_fontStyleStack.Remove(FontStyles.LowerCase) == 0)
						{
							this.m_style &= (FontStyles)(-9);
						}
						return true;
						IL_32F5:
						if (this.m_fontStyleStack.Remove(FontStyles.SmallCaps) == 0)
						{
							this.m_style &= (FontStyles)(-33);
						}
						return true;
						IL_34F9:
						num13 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength);
						if (num13 == -9999f)
						{
							return false;
						}
						this.m_marginRight = num13;
						switch (tagUnits)
						{
						case TagUnits.FontUnits:
							this.m_marginRight *= this.m_fontScale * this.m_fontAsset.fontInfo.TabWidth / (float)this.m_fontAsset.tabSize;
							break;
						case TagUnits.Percentage:
							this.m_marginRight = (this.m_marginWidth - ((this.m_width != -1f) ? this.m_width : 0f)) * this.m_marginRight / 100f;
							break;
						}
						this.m_marginRight = ((this.m_marginRight >= 0f) ? this.m_marginRight : 0f);
						return true;
					}
					else
					{
						if (num10 > -330774850)
						{
							if (num10 <= 73)
							{
								if (num10 != 66)
								{
									if (num10 != 73)
									{
										return false;
									}
									goto IL_129F;
								}
							}
							else
							{
								if (num10 == 83)
								{
									goto IL_12DB;
								}
								if (num10 == 85)
								{
									goto IL_13E5;
								}
								if (num10 != 98)
								{
									return false;
								}
							}
							this.m_style |= FontStyles.Bold;
							this.m_fontStyleStack.Add(FontStyles.Bold);
							this.m_fontWeightInternal = 700;
							this.m_fontWeightStack.Add(700);
							return true;
						}
						if (num10 <= -842656867)
						{
							if (num10 == -842693512)
							{
								goto IL_35D9;
							}
							if (num10 != -842656867)
							{
								return false;
							}
							goto IL_2CF3;
						}
						else
						{
							if (num10 == -445573839)
							{
								goto IL_368B;
							}
							if (num10 == -445537194)
							{
								goto IL_2DB1;
							}
							if (num10 != -330774850)
							{
								return false;
							}
							goto IL_1846;
						}
					}
					IL_129F:
					this.m_style |= FontStyles.Italic;
					this.m_fontStyleStack.Add(FontStyles.Italic);
					return true;
					IL_12DB:
					this.m_style |= FontStyles.Strikethrough;
					this.m_fontStyleStack.Add(FontStyles.Strikethrough);
					if (this.m_xmlAttribute[1].nameHashCode == 281955 || this.m_xmlAttribute[1].nameHashCode == 192323)
					{
						this.m_strikethroughColor = this.HexCharsToColor(this.m_htmlTag, this.m_xmlAttribute[1].valueStartIndex, this.m_xmlAttribute[1].valueLength);
						this.m_strikethroughColor.a = ((this.m_htmlColor.a < this.m_strikethroughColor.a) ? this.m_htmlColor.a : this.m_strikethroughColor.a);
					}
					else
					{
						this.m_strikethroughColor = this.m_htmlColor;
					}
					this.m_strikethroughColorStack.Add(this.m_strikethroughColor);
					return true;
					IL_13E5:
					this.m_style |= FontStyles.Underline;
					this.m_fontStyleStack.Add(FontStyles.Underline);
					if (this.m_xmlAttribute[1].nameHashCode == 281955 || this.m_xmlAttribute[1].nameHashCode == 192323)
					{
						this.m_underlineColor = this.HexCharsToColor(this.m_htmlTag, this.m_xmlAttribute[1].valueStartIndex, this.m_xmlAttribute[1].valueLength);
						this.m_underlineColor.a = ((this.m_htmlColor.a < this.m_underlineColor.a) ? this.m_htmlColor.a : this.m_underlineColor.a);
					}
					else
					{
						this.m_underlineColor = this.m_htmlColor;
					}
					this.m_underlineColorStack.Add(this.m_underlineColor);
					return true;
				}
				if (num10 > 31169)
				{
					if (num10 > 143092)
					{
						if (num10 <= 155892)
						{
							if (num10 <= 144016)
							{
								if (num10 == 143113)
								{
									goto IL_250E;
								}
								if (num10 != 144016)
								{
									return false;
								}
							}
							else
							{
								if (num10 == 145592)
								{
									goto IL_1DCC;
								}
								if (num10 == 154158)
								{
									goto IL_2101;
								}
								if (num10 != 155892)
								{
									return false;
								}
								goto IL_15A0;
							}
						}
						else if (num10 <= 156816)
						{
							if (num10 == 155913)
							{
								goto IL_250E;
							}
							if (num10 != 156816)
							{
								return false;
							}
						}
						else
						{
							if (num10 == 158392)
							{
								goto IL_1DCC;
							}
							if (num10 == 186285)
							{
								goto IL_2594;
							}
							if (num10 != 186622)
							{
								return false;
							}
							goto IL_238C;
						}
						this.m_isNonBreakingSpace = false;
						return true;
						IL_1DCC:
						this.m_currentFontSize = this.m_sizeStack.Remove();
						this.m_fontScale = this.m_currentFontSize / this.m_currentFontAsset.fontInfo.PointSize * this.m_currentFontAsset.fontInfo.Scale * (this.m_isOrthographic ? 1f : 0.1f);
						return true;
						IL_250E:
						if (this.m_isParsingText && !this.m_isCalculatingPreferredValues && this.m_textInfo.linkCount < this.m_textInfo.linkInfo.Length)
						{
							this.m_textInfo.linkInfo[this.m_textInfo.linkCount].linkTextLength = this.m_characterCount - this.m_textInfo.linkInfo[this.m_textInfo.linkCount].linkTextfirstCharacterIndex;
							this.m_textInfo.linkCount++;
						}
						return true;
					}
					if (num10 <= 43066)
					{
						if (num10 <= 32745)
						{
							if (num10 != 31191)
							{
								if (num10 != 32745)
								{
									return false;
								}
								goto IL_1B68;
							}
						}
						else
						{
							if (num10 == 41311)
							{
								goto IL_1E22;
							}
							if (num10 == 43045)
							{
								goto IL_14FB;
							}
							if (num10 != 43066)
							{
								return false;
							}
							goto IL_23D4;
						}
					}
					else if (num10 <= 43991)
					{
						if (num10 == 43969)
						{
							goto IL_1B56;
						}
						if (num10 != 43991)
						{
							return false;
						}
					}
					else
					{
						if (num10 == 45545)
						{
							goto IL_1B68;
						}
						if (num10 == 141358)
						{
							goto IL_2101;
						}
						if (num10 != 143092)
						{
							return false;
						}
						goto IL_15A0;
					}
					if (this.m_overflowMode == TextOverflowModes.Page)
					{
						this.m_xAdvance = 0f + this.tag_LineIndent + this.tag_Indent;
						this.m_lineOffset = 0f;
						this.m_pageNumber++;
						this.m_isNewPage = true;
					}
					return true;
					IL_1B68:
					num13 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength);
					if (num13 == -9999f)
					{
						return false;
					}
					switch (tagUnits)
					{
					case TagUnits.Pixels:
						if (this.m_htmlTag[5] == '+')
						{
							this.m_currentFontSize = this.m_fontSize + num13;
							this.m_sizeStack.Add(this.m_currentFontSize);
							this.m_fontScale = this.m_currentFontSize / this.m_currentFontAsset.fontInfo.PointSize * this.m_currentFontAsset.fontInfo.Scale * (this.m_isOrthographic ? 1f : 0.1f);
							return true;
						}
						if (this.m_htmlTag[5] == '-')
						{
							this.m_currentFontSize = this.m_fontSize + num13;
							this.m_sizeStack.Add(this.m_currentFontSize);
							this.m_fontScale = this.m_currentFontSize / this.m_currentFontAsset.fontInfo.PointSize * this.m_currentFontAsset.fontInfo.Scale * (this.m_isOrthographic ? 1f : 0.1f);
							return true;
						}
						this.m_currentFontSize = num13;
						this.m_sizeStack.Add(this.m_currentFontSize);
						this.m_fontScale = this.m_currentFontSize / this.m_currentFontAsset.fontInfo.PointSize * this.m_currentFontAsset.fontInfo.Scale * (this.m_isOrthographic ? 1f : 0.1f);
						return true;
					case TagUnits.FontUnits:
						this.m_currentFontSize = this.m_fontSize * num13;
						this.m_sizeStack.Add(this.m_currentFontSize);
						this.m_fontScale = this.m_currentFontSize / this.m_currentFontAsset.fontInfo.PointSize * this.m_currentFontAsset.fontInfo.Scale * (this.m_isOrthographic ? 1f : 0.1f);
						return true;
					case TagUnits.Percentage:
						this.m_currentFontSize = this.m_fontSize * num13 / 100f;
						this.m_sizeStack.Add(this.m_currentFontSize);
						this.m_fontScale = this.m_currentFontSize / this.m_currentFontAsset.fontInfo.PointSize * this.m_currentFontAsset.fontInfo.Scale * (this.m_isOrthographic ? 1f : 0.1f);
						return true;
					default:
						return false;
					}
					IL_15A0:
					if ((this.m_fontStyle & FontStyles.Highlight) != FontStyles.Highlight)
					{
						this.m_highlightColor = this.m_highlightColorStack.Remove();
						if (this.m_fontStyleStack.Remove(FontStyles.Highlight) == 0)
						{
							this.m_style &= (FontStyles)(-513);
						}
					}
					return true;
					IL_2101:
					MaterialReference materialReference = this.m_materialReferenceStack.Remove();
					this.m_currentFontAsset = materialReference.fontAsset;
					this.m_currentMaterial = materialReference.material;
					this.m_currentMaterialIndex = materialReference.index;
					this.m_fontScale = this.m_currentFontSize / this.m_currentFontAsset.fontInfo.PointSize * this.m_currentFontAsset.fontInfo.Scale * (this.m_isOrthographic ? 1f : 0.1f);
					return true;
				}
				if (num10 > 6566)
				{
					if (num10 <= 22673)
					{
						if (num10 <= 20849)
						{
							if (num10 == 20677)
							{
								goto IL_1A7D;
							}
							if (num10 != 20849)
							{
								return false;
							}
						}
						else
						{
							if (num10 == 20863)
							{
								goto IL_17B4;
							}
							if (num10 == 22501)
							{
								goto IL_1A7D;
							}
							if (num10 != 22673)
							{
								return false;
							}
						}
						if ((this.m_style & FontStyles.Subscript) == FontStyles.Subscript)
						{
							if (this.m_fontScaleMultiplier < 1f)
							{
								this.m_baselineOffset = this.m_baselineOffsetStack.Pop();
								this.m_fontScaleMultiplier /= ((this.m_currentFontAsset.fontInfo.SubSize > 0f) ? this.m_currentFontAsset.fontInfo.SubSize : 1f);
							}
							if (this.m_fontStyleStack.Remove(FontStyles.Subscript) == 0)
							{
								this.m_style &= (FontStyles)(-257);
							}
						}
						return true;
						IL_1A7D:
						this.m_isIgnoringAlignment = false;
						return true;
					}
					if (num10 <= 28511)
					{
						if (num10 != 22687)
						{
							if (num10 != 28511)
							{
								return false;
							}
							goto IL_1E22;
						}
					}
					else
					{
						if (num10 == 30245)
						{
							goto IL_14FB;
						}
						if (num10 == 30266)
						{
							goto IL_23D4;
						}
						if (num10 != 31169)
						{
							return false;
						}
						goto IL_1B56;
					}
					IL_17B4:
					if ((this.m_style & FontStyles.Superscript) == FontStyles.Superscript)
					{
						if (this.m_fontScaleMultiplier < 1f)
						{
							this.m_baselineOffset = this.m_baselineOffsetStack.Pop();
							this.m_fontScaleMultiplier /= ((this.m_currentFontAsset.fontInfo.SubSize > 0f) ? this.m_currentFontAsset.fontInfo.SubSize : 1f);
						}
						if (this.m_fontStyleStack.Remove(FontStyles.Superscript) == 0)
						{
							this.m_style &= (FontStyles)(-129);
						}
					}
					return true;
				}
				if (num10 <= 4556)
				{
					if (num10 <= 3215)
					{
						if (num10 != 2973)
						{
							if (num10 != 3215)
							{
								return false;
							}
							return true;
						}
					}
					else
					{
						if (num10 == 3219)
						{
							return true;
						}
						if (num10 != 3229)
						{
							if (num10 != 4556)
							{
								return false;
							}
							goto IL_19DC;
						}
					}
					return true;
				}
				if (num10 <= 4742)
				{
					if (num10 != 4728)
					{
						if (num10 != 4742)
						{
							return false;
						}
						goto IL_1718;
					}
				}
				else
				{
					if (num10 == 6380)
					{
						goto IL_19DC;
					}
					if (num10 != 6552)
					{
						if (num10 != 6566)
						{
							return false;
						}
						goto IL_1718;
					}
				}
				this.m_fontScaleMultiplier *= ((this.m_currentFontAsset.fontInfo.SubSize > 0f) ? this.m_currentFontAsset.fontInfo.SubSize : 1f);
				this.m_baselineOffsetStack.Push(this.m_baselineOffset);
				this.m_baselineOffset += this.m_currentFontAsset.fontInfo.SubscriptOffset * this.m_fontScale * this.m_fontScaleMultiplier;
				this.m_fontStyleStack.Add(FontStyles.Subscript);
				this.m_style |= FontStyles.Subscript;
				return true;
				IL_1718:
				this.m_fontScaleMultiplier *= ((this.m_currentFontAsset.fontInfo.SubSize > 0f) ? this.m_currentFontAsset.fontInfo.SubSize : 1f);
				this.m_baselineOffsetStack.Push(this.m_baselineOffset);
				this.m_baselineOffset += this.m_currentFontAsset.fontInfo.SuperscriptOffset * this.m_fontScale * this.m_fontScaleMultiplier;
				this.m_fontStyleStack.Add(FontStyles.Superscript);
				this.m_style |= FontStyles.Superscript;
				return true;
				IL_19DC:
				num13 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength);
				if (num13 == -9999f)
				{
					return false;
				}
				switch (tagUnits)
				{
				case TagUnits.Pixels:
					this.m_xAdvance = num13;
					return true;
				case TagUnits.FontUnits:
					this.m_xAdvance = num13 * this.m_fontScale * this.m_fontAsset.fontInfo.TabWidth / (float)this.m_fontAsset.tabSize;
					return true;
				case TagUnits.Percentage:
					this.m_xAdvance = this.m_marginWidth * num13 / 100f;
					return true;
				default:
					return false;
				}
				IL_14FB:
				this.m_style |= FontStyles.Highlight;
				this.m_fontStyleStack.Add(FontStyles.Highlight);
				this.m_highlightColor = this.HexCharsToColor(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength);
				this.m_highlightColor.a = ((this.m_htmlColor.a < this.m_highlightColor.a) ? this.m_htmlColor.a : this.m_highlightColor.a);
				this.m_highlightColorStack.Add(this.m_highlightColor);
				return true;
				IL_1B56:
				this.m_isNonBreakingSpace = true;
				return true;
				IL_1E22:
				int valueHashCode = this.m_xmlAttribute[0].valueHashCode;
				int nameHashCode = this.m_xmlAttribute[1].nameHashCode;
				int valueHashCode2 = this.m_xmlAttribute[1].valueHashCode;
				if (valueHashCode == 764638571 || valueHashCode == 523367755)
				{
					this.m_currentFontAsset = this.m_materialReferences[0].fontAsset;
					this.m_currentMaterial = this.m_materialReferences[0].material;
					this.m_currentMaterialIndex = 0;
					this.m_fontScale = this.m_currentFontSize / this.m_currentFontAsset.fontInfo.PointSize * this.m_currentFontAsset.fontInfo.Scale * (this.m_isOrthographic ? 1f : 0.1f);
					this.m_materialReferenceStack.Add(this.m_materialReferences[0]);
					return true;
				}
				TMP_FontAsset tmp_FontAsset;
				if (!MaterialReferenceManager.TryGetFontAsset(valueHashCode, out tmp_FontAsset))
				{
					tmp_FontAsset = Resources.Load<TMP_FontAsset>(TMP_Settings.defaultFontAssetPath + new string(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength));
					if (tmp_FontAsset == null)
					{
						return false;
					}
					MaterialReferenceManager.AddFontAsset(tmp_FontAsset);
				}
				if (nameHashCode == 0 && valueHashCode2 == 0)
				{
					this.m_currentMaterial = tmp_FontAsset.material;
					this.m_currentMaterialIndex = MaterialReference.AddMaterialReference(this.m_currentMaterial, tmp_FontAsset, this.m_materialReferences, this.m_materialReferenceIndexLookup);
					this.m_materialReferenceStack.Add(this.m_materialReferences[this.m_currentMaterialIndex]);
				}
				else
				{
					if (nameHashCode != 103415287 && nameHashCode != 72669687)
					{
						return false;
					}
					Material material;
					if (MaterialReferenceManager.TryGetMaterial(valueHashCode2, out material))
					{
						this.m_currentMaterial = material;
						this.m_currentMaterialIndex = MaterialReference.AddMaterialReference(this.m_currentMaterial, tmp_FontAsset, this.m_materialReferences, this.m_materialReferenceIndexLookup);
						this.m_materialReferenceStack.Add(this.m_materialReferences[this.m_currentMaterialIndex]);
					}
					else
					{
						material = Resources.Load<Material>(TMP_Settings.defaultFontAssetPath + new string(this.m_htmlTag, this.m_xmlAttribute[1].valueStartIndex, this.m_xmlAttribute[1].valueLength));
						if (material == null)
						{
							return false;
						}
						MaterialReferenceManager.AddFontMaterial(valueHashCode2, material);
						this.m_currentMaterial = material;
						this.m_currentMaterialIndex = MaterialReference.AddMaterialReference(this.m_currentMaterial, tmp_FontAsset, this.m_materialReferences, this.m_materialReferenceIndexLookup);
						this.m_materialReferenceStack.Add(this.m_materialReferences[this.m_currentMaterialIndex]);
					}
				}
				this.m_currentFontAsset = tmp_FontAsset;
				this.m_fontScale = this.m_currentFontSize / this.m_currentFontAsset.fontInfo.PointSize * this.m_currentFontAsset.fontInfo.Scale * (this.m_isOrthographic ? 1f : 0.1f);
				return true;
				IL_23D4:
				if (this.m_isParsingText && !this.m_isCalculatingPreferredValues)
				{
					int linkCount = this.m_textInfo.linkCount;
					if (linkCount + 1 > this.m_textInfo.linkInfo.Length)
					{
						TMP_TextInfo.Resize<TMP_LinkInfo>(ref this.m_textInfo.linkInfo, linkCount + 1);
					}
					this.m_textInfo.linkInfo[linkCount].textComponent = this;
					this.m_textInfo.linkInfo[linkCount].hashCode = this.m_xmlAttribute[0].valueHashCode;
					this.m_textInfo.linkInfo[linkCount].linkTextfirstCharacterIndex = this.m_characterCount;
					this.m_textInfo.linkInfo[linkCount].linkIdFirstCharacterIndex = startIndex + this.m_xmlAttribute[0].valueStartIndex;
					this.m_textInfo.linkInfo[linkCount].linkIdLength = this.m_xmlAttribute[0].valueLength;
					this.m_textInfo.linkInfo[linkCount].SetLinkID(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength);
				}
				return true;
			}
			if (num10 <= 6886018)
			{
				if (num10 <= 1071884)
				{
					if (num10 > 315682)
					{
						if (num10 <= 982252)
						{
							if (num10 <= 320078)
							{
								if (num10 == 317446)
								{
									goto IL_3842;
								}
								if (num10 != 320078)
								{
									return false;
								}
								goto IL_22F2;
							}
							else
							{
								if (num10 == 327550)
								{
									goto IL_2698;
								}
								if (num10 != 976214)
								{
									if (num10 != 982252)
									{
										return false;
									}
									goto IL_2C05;
								}
							}
						}
						else if (num10 <= 1017743)
						{
							if (num10 == 1015979)
							{
								goto IL_37C6;
							}
							if (num10 != 1017743)
							{
								return false;
							}
							return true;
						}
						else
						{
							if (num10 == 1027847)
							{
								goto IL_270A;
							}
							if (num10 != 1065846)
							{
								if (num10 != 1071884)
								{
									return false;
								}
								goto IL_2C05;
							}
						}
						this.m_lineJustification = this.m_lineJustificationStack.Remove();
						return true;
						IL_2C05:
						this.m_htmlColor = this.m_colorStack.Remove();
						return true;
					}
					if (num10 <= 237918)
					{
						if (num10 <= 226050)
						{
							if (num10 != 192323)
							{
								if (num10 != 226050)
								{
									return false;
								}
								goto IL_375C;
							}
						}
						else
						{
							if (num10 == 227814)
							{
								goto IL_3842;
							}
							if (num10 == 230446)
							{
								goto IL_22F2;
							}
							if (num10 != 237918)
							{
								return false;
							}
							goto IL_2698;
						}
					}
					else if (num10 <= 276254)
					{
						if (num10 == 275917)
						{
							goto IL_2594;
						}
						if (num10 != 276254)
						{
							return false;
						}
						goto IL_238C;
					}
					else
					{
						if (num10 == 280416)
						{
							return false;
						}
						if (num10 != 281955)
						{
							if (num10 != 315682)
							{
								return false;
							}
							goto IL_375C;
						}
					}
					if (this.m_htmlTag[6] == '#' && num == 10)
					{
						this.m_htmlColor = this.HexCharsToColor(this.m_htmlTag, num);
						this.m_colorStack.Add(this.m_htmlColor);
						return true;
					}
					if (this.m_htmlTag[6] == '#' && num == 11)
					{
						this.m_htmlColor = this.HexCharsToColor(this.m_htmlTag, num);
						this.m_colorStack.Add(this.m_htmlColor);
						return true;
					}
					if (this.m_htmlTag[6] == '#' && num == 13)
					{
						this.m_htmlColor = this.HexCharsToColor(this.m_htmlTag, num);
						this.m_colorStack.Add(this.m_htmlColor);
						return true;
					}
					if (this.m_htmlTag[6] == '#' && num == 15)
					{
						this.m_htmlColor = this.HexCharsToColor(this.m_htmlTag, num);
						this.m_colorStack.Add(this.m_htmlColor);
						return true;
					}
					num10 = this.m_xmlAttribute[0].valueHashCode;
					if (num10 <= 26556144)
					{
						if (num10 <= 125395)
						{
							if (num10 == -36881330)
							{
								this.m_htmlColor = new Color32(160, 32, 240, byte.MaxValue);
								this.m_colorStack.Add(this.m_htmlColor);
								return true;
							}
							if (num10 == 125395)
							{
								this.m_htmlColor = Color.red;
								this.m_colorStack.Add(this.m_htmlColor);
								return true;
							}
						}
						else
						{
							if (num10 == 3573310)
							{
								this.m_htmlColor = Color.blue;
								this.m_colorStack.Add(this.m_htmlColor);
								return true;
							}
							if (num10 == 26556144)
							{
								this.m_htmlColor = new Color32(byte.MaxValue, 128, 0, byte.MaxValue);
								this.m_colorStack.Add(this.m_htmlColor);
								return true;
							}
						}
					}
					else if (num10 <= 121463835)
					{
						if (num10 == 117905991)
						{
							this.m_htmlColor = Color.black;
							this.m_colorStack.Add(this.m_htmlColor);
							return true;
						}
						if (num10 == 121463835)
						{
							this.m_htmlColor = Color.green;
							this.m_colorStack.Add(this.m_htmlColor);
							return true;
						}
					}
					else
					{
						if (num10 == 140357351)
						{
							this.m_htmlColor = Color.white;
							this.m_colorStack.Add(this.m_htmlColor);
							return true;
						}
						if (num10 == 554054276)
						{
							this.m_htmlColor = Color.yellow;
							this.m_colorStack.Add(this.m_htmlColor);
							return true;
						}
					}
					return false;
					IL_375C:
					num13 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength);
					if (num13 == -9999f)
					{
						return false;
					}
					this.m_FXMatrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, new Vector3(num13, 1f, 1f));
					this.m_isFXMatrixSet = true;
					return true;
					IL_22F2:
					num13 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength);
					if (num13 == -9999f)
					{
						return false;
					}
					switch (tagUnits)
					{
					case TagUnits.Pixels:
						this.m_xAdvance += num13;
						return true;
					case TagUnits.FontUnits:
						this.m_xAdvance += num13 * this.m_fontScale * this.m_fontAsset.fontInfo.TabWidth / (float)this.m_fontAsset.tabSize;
						return true;
					case TagUnits.Percentage:
						return false;
					default:
						return false;
					}
					IL_2698:
					num13 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength);
					if (num13 == -9999f)
					{
						return false;
					}
					switch (tagUnits)
					{
					case TagUnits.Pixels:
						this.m_width = num13;
						break;
					case TagUnits.FontUnits:
						return false;
					case TagUnits.Percentage:
						this.m_width = this.m_marginWidth * num13 / 100f;
						break;
					}
					return true;
					IL_3842:
					num10 = this.m_xmlAttribute[1].nameHashCode;
					if (num10 == 327550)
					{
						float num14 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[1].valueStartIndex, this.m_xmlAttribute[1].valueLength);
						switch (tagUnits)
						{
						case TagUnits.Pixels:
							Debug.Log("Table width = " + num14 + "px.");
							break;
						case TagUnits.FontUnits:
							Debug.Log("Table width = " + num14 + "em.");
							break;
						case TagUnits.Percentage:
							Debug.Log("Table width = " + num14 + "%.");
							break;
						}
					}
					return true;
				}
				if (num10 <= 1619421)
				{
					if (num10 <= 1356515)
					{
						if (num10 <= 1107375)
						{
							if (num10 == 1105611)
							{
								goto IL_37C6;
							}
							if (num10 != 1107375)
							{
								return false;
							}
							return true;
						}
						else
						{
							if (num10 == 1117479)
							{
								goto IL_270A;
							}
							if (num10 == 1286342)
							{
								goto IL_36A1;
							}
							if (num10 != 1356515)
							{
								return false;
							}
						}
					}
					else if (num10 <= 1482398)
					{
						if (num10 == 1441524)
						{
							goto IL_2C18;
						}
						if (num10 != 1482398)
						{
							return false;
						}
						goto IL_3315;
					}
					else
					{
						if (num10 == 1524585)
						{
							goto IL_2B5E;
						}
						if (num10 == 1600507)
						{
							goto IL_37CF;
						}
						if (num10 != 1619421)
						{
							return false;
						}
						goto IL_2DBE;
					}
				}
				else if (num10 <= 2109854)
				{
					if (num10 <= 1913798)
					{
						if (num10 == 1750458)
						{
							return false;
						}
						if (num10 != 1913798)
						{
							return false;
						}
						goto IL_36A1;
					}
					else if (num10 != 1983971)
					{
						if (num10 == 2068980)
						{
							goto IL_2C18;
						}
						if (num10 != 2109854)
						{
							return false;
						}
						goto IL_3315;
					}
				}
				else if (num10 <= 2227963)
				{
					if (num10 == 2152041)
					{
						goto IL_2B5E;
					}
					if (num10 != 2227963)
					{
						return false;
					}
					goto IL_37CF;
				}
				else
				{
					if (num10 == 2246877)
					{
						goto IL_2DBE;
					}
					if (num10 == 6815845)
					{
						goto IL_3703;
					}
					if (num10 != 6886018)
					{
						return false;
					}
					goto IL_2B08;
				}
				num13 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength);
				if (num13 == -9999f)
				{
					return false;
				}
				switch (tagUnits)
				{
				case TagUnits.Pixels:
					this.m_cSpacing = num13;
					break;
				case TagUnits.FontUnits:
					this.m_cSpacing = num13;
					this.m_cSpacing *= this.m_fontScale * this.m_fontAsset.fontInfo.TabWidth / (float)this.m_fontAsset.tabSize;
					break;
				case TagUnits.Percentage:
					return false;
				}
				return true;
				IL_2B5E:
				num13 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength);
				if (num13 == -9999f)
				{
					return false;
				}
				switch (tagUnits)
				{
				case TagUnits.Pixels:
					this.m_monoSpacing = num13;
					break;
				case TagUnits.FontUnits:
					this.m_monoSpacing = num13;
					this.m_monoSpacing *= this.m_fontScale * this.m_fontAsset.fontInfo.TabWidth / (float)this.m_fontAsset.tabSize;
					break;
				case TagUnits.Percentage:
					return false;
				}
				return true;
				IL_2C18:
				num13 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength);
				if (num13 == -9999f)
				{
					return false;
				}
				switch (tagUnits)
				{
				case TagUnits.Pixels:
					this.tag_Indent = num13;
					break;
				case TagUnits.FontUnits:
					this.tag_Indent = num13;
					this.tag_Indent *= this.m_fontScale * this.m_fontAsset.fontInfo.TabWidth / (float)this.m_fontAsset.tabSize;
					break;
				case TagUnits.Percentage:
					this.tag_Indent = this.m_marginWidth * num13 / 100f;
					break;
				}
				this.m_indentStack.Add(this.tag_Indent);
				this.m_xAdvance = this.tag_Indent;
				return true;
				IL_2DBE:
				int valueHashCode3 = this.m_xmlAttribute[0].valueHashCode;
				this.m_spriteIndex = -1;
				TMP_SpriteAsset tmp_SpriteAsset;
				if (this.m_xmlAttribute[0].valueType == TagType.None || this.m_xmlAttribute[0].valueType == TagType.NumericalValue)
				{
					if (this.m_spriteAsset != null)
					{
						this.m_currentSpriteAsset = this.m_spriteAsset;
					}
					else if (this.m_defaultSpriteAsset != null)
					{
						this.m_currentSpriteAsset = this.m_defaultSpriteAsset;
					}
					else if (this.m_defaultSpriteAsset == null)
					{
						if (TMP_Settings.defaultSpriteAsset != null)
						{
							this.m_defaultSpriteAsset = TMP_Settings.defaultSpriteAsset;
						}
						else
						{
							this.m_defaultSpriteAsset = Resources.Load<TMP_SpriteAsset>("Sprite Assets/Default Sprite Asset");
						}
						this.m_currentSpriteAsset = this.m_defaultSpriteAsset;
					}
					if (this.m_currentSpriteAsset == null)
					{
						return false;
					}
				}
				else if (MaterialReferenceManager.TryGetSpriteAsset(valueHashCode3, out tmp_SpriteAsset))
				{
					this.m_currentSpriteAsset = tmp_SpriteAsset;
				}
				else
				{
					if (tmp_SpriteAsset == null)
					{
						tmp_SpriteAsset = Resources.Load<TMP_SpriteAsset>(TMP_Settings.defaultSpriteAssetPath + new string(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength));
					}
					if (tmp_SpriteAsset == null)
					{
						return false;
					}
					MaterialReferenceManager.AddSpriteAsset(valueHashCode3, tmp_SpriteAsset);
					this.m_currentSpriteAsset = tmp_SpriteAsset;
				}
				if (this.m_xmlAttribute[0].valueType == TagType.NumericalValue)
				{
					int num15 = (int)this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength);
					if (num15 == -9999)
					{
						return false;
					}
					if (num15 > this.m_currentSpriteAsset.spriteInfoList.Count - 1)
					{
						return false;
					}
					this.m_spriteIndex = num15;
				}
				this.m_spriteColor = TMP_Text.s_colorWhite;
				this.m_tintSprite = false;
				int num16 = 0;
				while (num16 < this.m_xmlAttribute.Length && this.m_xmlAttribute[num16].nameHashCode != 0)
				{
					int nameHashCode2 = this.m_xmlAttribute[num16].nameHashCode;
					int num17 = 0;
					if (nameHashCode2 <= 43347)
					{
						if (nameHashCode2 <= 30547)
						{
							if (nameHashCode2 == 26705)
							{
								goto IL_315F;
							}
							if (nameHashCode2 != 30547)
							{
								goto IL_31E2;
							}
						}
						else
						{
							if (nameHashCode2 == 33019)
							{
								goto IL_30DF;
							}
							if (nameHashCode2 == 39505)
							{
								goto IL_315F;
							}
							if (nameHashCode2 != 43347)
							{
								goto IL_31E2;
							}
						}
						this.m_currentSpriteAsset = TMP_SpriteAsset.SearchForSpriteByHashCode(this.m_currentSpriteAsset, this.m_xmlAttribute[num16].valueHashCode, true, out num17);
						if (num17 == -1)
						{
							return false;
						}
						this.m_spriteIndex = num17;
						goto IL_31F6;
						IL_315F:
						if (this.GetAttributeParameters(this.m_htmlTag, this.m_xmlAttribute[num16].valueStartIndex, this.m_xmlAttribute[num16].valueLength, ref this.m_attributeParameterValues) != 3)
						{
							return false;
						}
						this.m_spriteIndex = (int)this.m_attributeParameterValues[0];
						if (this.m_isParsingText)
						{
							this.spriteAnimator.DoSpriteAnimation(this.m_characterCount, this.m_currentSpriteAsset, this.m_spriteIndex, (int)this.m_attributeParameterValues[1], (int)this.m_attributeParameterValues[2]);
						}
					}
					else
					{
						if (nameHashCode2 <= 192323)
						{
							if (nameHashCode2 == 45819)
							{
								goto IL_30DF;
							}
							if (nameHashCode2 != 192323)
							{
								goto IL_31E2;
							}
						}
						else
						{
							if (nameHashCode2 != 205930)
							{
								if (nameHashCode2 == 281955)
								{
									goto IL_3124;
								}
								if (nameHashCode2 != 295562)
								{
									goto IL_31E2;
								}
							}
							num17 = (int)this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[1].valueStartIndex, this.m_xmlAttribute[1].valueLength);
							if (num17 == -9999)
							{
								return false;
							}
							if (num17 > this.m_currentSpriteAsset.spriteInfoList.Count - 1)
							{
								return false;
							}
							this.m_spriteIndex = num17;
							goto IL_31F6;
						}
						IL_3124:
						this.m_spriteColor = this.HexCharsToColor(this.m_htmlTag, this.m_xmlAttribute[num16].valueStartIndex, this.m_xmlAttribute[num16].valueLength);
					}
					IL_31F6:
					num16++;
					continue;
					IL_30DF:
					this.m_tintSprite = (this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[num16].valueStartIndex, this.m_xmlAttribute[num16].valueLength) != 0f);
					goto IL_31F6;
					IL_31E2:
					if (nameHashCode2 != 2246877 && nameHashCode2 != 1619421)
					{
						return false;
					}
					goto IL_31F6;
				}
				if (this.m_spriteIndex == -1)
				{
					return false;
				}
				this.m_currentMaterialIndex = MaterialReference.AddMaterialReference(this.m_currentSpriteAsset.material, this.m_currentSpriteAsset, this.m_materialReferences, this.m_materialReferenceIndexLookup);
				this.m_textElementType = TMP_TextElementType.Sprite;
				return true;
				IL_3315:
				num13 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength);
				if (num13 == -9999f)
				{
					return false;
				}
				this.m_marginLeft = num13;
				switch (tagUnits)
				{
				case TagUnits.FontUnits:
					this.m_marginLeft *= this.m_fontScale * this.m_fontAsset.fontInfo.TabWidth / (float)this.m_fontAsset.tabSize;
					break;
				case TagUnits.Percentage:
					this.m_marginLeft = (this.m_marginWidth - ((this.m_width != -1f) ? this.m_width : 0f)) * this.m_marginLeft / 100f;
					break;
				}
				this.m_marginLeft = ((this.m_marginLeft >= 0f) ? this.m_marginLeft : 0f);
				this.m_marginRight = this.m_marginLeft;
				return true;
				IL_36A1:
				int valueHashCode4 = this.m_xmlAttribute[0].valueHashCode;
				if (this.m_isParsingText)
				{
					this.m_actionStack.Add(valueHashCode4);
					Debug.Log(string.Concat(new object[]
					{
						"Action ID: [",
						valueHashCode4,
						"] First character index: ",
						this.m_characterCount
					}));
				}
				return true;
				IL_37CF:
				num13 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength);
				if (num13 == -9999f)
				{
					return false;
				}
				this.m_FXMatrix = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0f, 0f, num13), Vector3.one);
				this.m_isFXMatrixSet = true;
				return true;
				IL_270A:
				this.m_width = -1f;
				return true;
				IL_37C6:
				this.m_isFXMatrixSet = false;
				return true;
			}
			if (num10 > 54741026)
			{
				if (num10 <= 514803617)
				{
					if (num10 <= 340349191)
					{
						if (num10 <= 72669687)
						{
							if (num10 == 69403544)
							{
								goto IL_29BD;
							}
							if (num10 != 72669687)
							{
								return false;
							}
						}
						else
						{
							if (num10 == 100149144)
							{
								goto IL_29BD;
							}
							if (num10 != 103415287)
							{
								if (num10 != 340349191)
								{
									return false;
								}
								goto IL_2A5D;
							}
						}
						int valueHashCode2 = this.m_xmlAttribute[0].valueHashCode;
						if (valueHashCode2 == 764638571 || valueHashCode2 == 523367755)
						{
							this.m_currentMaterial = this.m_materialReferences[0].material;
							this.m_currentMaterialIndex = 0;
							this.m_materialReferenceStack.Add(this.m_materialReferences[0]);
							return true;
						}
						Material material;
						if (MaterialReferenceManager.TryGetMaterial(valueHashCode2, out material))
						{
							this.m_currentMaterial = material;
							this.m_currentMaterialIndex = MaterialReference.AddMaterialReference(this.m_currentMaterial, this.m_currentFontAsset, this.m_materialReferences, this.m_materialReferenceIndexLookup);
							this.m_materialReferenceStack.Add(this.m_materialReferences[this.m_currentMaterialIndex]);
						}
						else
						{
							material = Resources.Load<Material>(TMP_Settings.defaultFontAssetPath + new string(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength));
							if (material == null)
							{
								return false;
							}
							MaterialReferenceManager.AddFontMaterial(valueHashCode2, material);
							this.m_currentMaterial = material;
							this.m_currentMaterialIndex = MaterialReference.AddMaterialReference(this.m_currentMaterial, this.m_currentFontAsset, this.m_materialReferences, this.m_materialReferenceIndexLookup);
							this.m_materialReferenceStack.Add(this.m_materialReferences[this.m_currentMaterialIndex]);
						}
						return true;
						IL_29BD:
						int valueHashCode5 = this.m_xmlAttribute[0].valueHashCode;
						TMP_ColorGradient tmp_ColorGradient;
						if (MaterialReferenceManager.TryGetColorGradientPreset(valueHashCode5, out tmp_ColorGradient))
						{
							this.m_colorGradientPreset = tmp_ColorGradient;
						}
						else
						{
							if (tmp_ColorGradient == null)
							{
								tmp_ColorGradient = Resources.Load<TMP_ColorGradient>(TMP_Settings.defaultColorGradientPresetsPath + new string(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength));
							}
							if (tmp_ColorGradient == null)
							{
								return false;
							}
							MaterialReferenceManager.AddColorGradientPreset(valueHashCode5, tmp_ColorGradient);
							this.m_colorGradientPreset = tmp_ColorGradient;
						}
						this.m_colorGradientStack.Add(this.m_colorGradientPreset);
						return true;
					}
					if (num10 <= 371094791)
					{
						if (num10 != 343615334)
						{
							if (num10 != 371094791)
							{
								return false;
							}
							goto IL_2A5D;
						}
					}
					else if (num10 != 374360934)
					{
						if (num10 == 457225591)
						{
							goto IL_19AD;
						}
						if (num10 != 514803617)
						{
							return false;
						}
						goto IL_325B;
					}
					MaterialReference materialReference2 = this.m_materialReferenceStack.Remove();
					this.m_currentMaterial = materialReference2.material;
					this.m_currentMaterialIndex = materialReference2.index;
					return true;
					IL_2A5D:
					this.m_colorGradientPreset = this.m_colorGradientStack.Remove();
					return true;
				}
				if (num10 <= 781906058)
				{
					if (num10 <= 566686826)
					{
						if (num10 != 551025096)
						{
							if (num10 != 566686826)
							{
								return false;
							}
							goto IL_3297;
						}
					}
					else
					{
						if (num10 == 730022849)
						{
							goto IL_325B;
						}
						if (num10 != 766244328)
						{
							if (num10 != 781906058)
							{
								return false;
							}
							goto IL_3297;
						}
					}
					this.m_style |= FontStyles.SmallCaps;
					this.m_fontStyleStack.Add(FontStyles.SmallCaps);
					return true;
				}
				if (num10 <= 1109386397)
				{
					if (num10 == 1100728678)
					{
						goto IL_3419;
					}
					if (num10 == 1109349752)
					{
						goto IL_35D9;
					}
					if (num10 != 1109386397)
					{
						return false;
					}
					goto IL_2CF3;
				}
				else
				{
					if (num10 == 1897350193)
					{
						goto IL_368B;
					}
					if (num10 == 1897386838)
					{
						goto IL_2DB1;
					}
					if (num10 != 2012149182)
					{
						return false;
					}
					goto IL_1846;
				}
				IL_325B:
				this.m_style |= FontStyles.LowerCase;
				this.m_fontStyleStack.Add(FontStyles.LowerCase);
				return true;
			}
			if (num10 <= 7757466)
			{
				if (num10 <= 7443301)
				{
					if (num10 <= 7011901)
					{
						if (num10 == 6971027)
						{
							goto IL_2CE0;
						}
						if (num10 != 7011901)
						{
							return false;
						}
						goto IL_3401;
					}
					else if (num10 != 7054088)
					{
						if (num10 == 7130010)
						{
							goto IL_3839;
						}
						if (num10 != 7443301)
						{
							return false;
						}
						goto IL_3703;
					}
				}
				else if (num10 <= 7598483)
				{
					if (num10 == 7513474)
					{
						goto IL_2B08;
					}
					if (num10 != 7598483)
					{
						return false;
					}
					goto IL_2CE0;
				}
				else
				{
					if (num10 == 7639357)
					{
						goto IL_3401;
					}
					if (num10 != 7681544)
					{
						if (num10 != 7757466)
						{
							return false;
						}
						goto IL_3839;
					}
				}
				this.m_monoSpacing = 0f;
				return true;
				IL_2CE0:
				this.tag_Indent = this.m_indentStack.Remove();
				return true;
				IL_3401:
				this.m_marginLeft = 0f;
				this.m_marginRight = 0f;
				return true;
				IL_3839:
				this.m_isFXMatrixSet = false;
				return true;
			}
			if (num10 <= 15115642)
			{
				if (num10 <= 10723418)
				{
					if (num10 == 9133802)
					{
						goto IL_3297;
					}
					if (num10 != 10723418)
					{
						return false;
					}
				}
				else
				{
					if (num10 == 11642281)
					{
						goto IL_1A86;
					}
					if (num10 == 13526026)
					{
						goto IL_3297;
					}
					if (num10 != 15115642)
					{
						return false;
					}
				}
				this.tag_NoParsing = true;
				return true;
			}
			if (num10 > 47840323)
			{
				if (num10 != 50348802)
				{
					if (num10 == 52232547)
					{
						goto IL_32B6;
					}
					if (num10 != 54741026)
					{
						return false;
					}
				}
				this.m_baselineOffset = 0f;
				return true;
			}
			if (num10 != 16034505)
			{
				if (num10 != 47840323)
				{
					return false;
				}
				goto IL_32B6;
			}
			IL_1A86:
			num13 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength);
			if (num13 == -9999f)
			{
				return false;
			}
			switch (tagUnits)
			{
			case TagUnits.Pixels:
				this.m_baselineOffset = num13;
				return true;
			case TagUnits.FontUnits:
				this.m_baselineOffset = num13 * this.m_fontScale * this.m_fontAsset.fontInfo.Ascender;
				return true;
			case TagUnits.Percentage:
				return false;
			default:
				return false;
			}
			IL_3297:
			this.m_style |= FontStyles.UpperCase;
			this.m_fontStyleStack.Add(FontStyles.UpperCase);
			return true;
			IL_2B08:
			if (!this.m_isParsingText)
			{
				return true;
			}
			if (this.m_characterCount > 0)
			{
				this.m_xAdvance -= this.m_cSpacing;
				this.m_textInfo.characterInfo[this.m_characterCount - 1].xAdvance = this.m_xAdvance;
			}
			this.m_cSpacing = 0f;
			return true;
			IL_3703:
			if (this.m_isParsingText)
			{
				Debug.Log(string.Concat(new object[]
				{
					"Action ID: [",
					this.m_actionStack.CurrentItem(),
					"] Last character index: ",
					this.m_characterCount - 1
				}));
			}
			this.m_actionStack.Remove();
			return true;
			IL_1846:
			num13 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength);
			if (num13 == -9999f)
			{
				return false;
			}
			if ((this.m_fontStyle & FontStyles.Bold) == FontStyles.Bold)
			{
				return true;
			}
			this.m_style &= (FontStyles)(-2);
			num10 = (int)num13;
			if (num10 <= 400)
			{
				if (num10 <= 200)
				{
					if (num10 != 100)
					{
						if (num10 == 200)
						{
							this.m_fontWeightInternal = 200;
						}
					}
					else
					{
						this.m_fontWeightInternal = 100;
					}
				}
				else if (num10 != 300)
				{
					if (num10 == 400)
					{
						this.m_fontWeightInternal = 400;
					}
				}
				else
				{
					this.m_fontWeightInternal = 300;
				}
			}
			else if (num10 <= 600)
			{
				if (num10 != 500)
				{
					if (num10 == 600)
					{
						this.m_fontWeightInternal = 600;
					}
				}
				else
				{
					this.m_fontWeightInternal = 500;
				}
			}
			else if (num10 != 700)
			{
				if (num10 != 800)
				{
					if (num10 == 900)
					{
						this.m_fontWeightInternal = 900;
					}
				}
				else
				{
					this.m_fontWeightInternal = 800;
				}
			}
			else
			{
				this.m_fontWeightInternal = 700;
				this.m_style |= FontStyles.Bold;
			}
			this.m_fontWeightStack.Add(this.m_fontWeightInternal);
			return true;
			IL_19AD:
			this.m_fontWeightInternal = this.m_fontWeightStack.Remove();
			if (this.m_fontWeightInternal == 400)
			{
				this.m_style &= (FontStyles)(-2);
			}
			return true;
			IL_238C:
			if (this.m_xmlAttribute[0].valueLength != 3)
			{
				return false;
			}
			this.m_htmlColor.a = (byte)(this.HexToInt(this.m_htmlTag[7]) * 16 + this.HexToInt(this.m_htmlTag[8]));
			return true;
			IL_2594:
			num10 = this.m_xmlAttribute[0].valueHashCode;
			if (num10 <= -458210101)
			{
				if (num10 == -523808257)
				{
					this.m_lineJustification = TextAlignmentOptions.Justified;
					this.m_lineJustificationStack.Add(this.m_lineJustification);
					return true;
				}
				if (num10 == -458210101)
				{
					this.m_lineJustification = TextAlignmentOptions.Center;
					this.m_lineJustificationStack.Add(this.m_lineJustification);
					return true;
				}
			}
			else
			{
				if (num10 == 3774683)
				{
					this.m_lineJustification = TextAlignmentOptions.Left;
					this.m_lineJustificationStack.Add(this.m_lineJustification);
					return true;
				}
				if (num10 == 122383428)
				{
					this.m_lineJustification = TextAlignmentOptions.Flush;
					this.m_lineJustificationStack.Add(this.m_lineJustification);
					return true;
				}
				if (num10 == 136703040)
				{
					this.m_lineJustification = TextAlignmentOptions.Right;
					this.m_lineJustificationStack.Add(this.m_lineJustification);
					return true;
				}
			}
			return false;
			IL_2CF3:
			num13 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength);
			if (num13 == -9999f)
			{
				return false;
			}
			switch (tagUnits)
			{
			case TagUnits.Pixels:
				this.tag_LineIndent = num13;
				break;
			case TagUnits.FontUnits:
				this.tag_LineIndent = num13;
				this.tag_LineIndent *= this.m_fontScale * this.m_fontAsset.fontInfo.TabWidth / (float)this.m_fontAsset.tabSize;
				break;
			case TagUnits.Percentage:
				this.tag_LineIndent = this.m_marginWidth * num13 / 100f;
				break;
			}
			this.m_xAdvance += this.tag_LineIndent;
			return true;
			IL_2DB1:
			this.tag_LineIndent = 0f;
			return true;
			IL_32B6:
			if (this.m_fontStyleStack.Remove(FontStyles.UpperCase) == 0)
			{
				this.m_style &= (FontStyles)(-17);
			}
			return true;
			IL_3419:
			num13 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength);
			if (num13 == -9999f)
			{
				return false;
			}
			this.m_marginLeft = num13;
			switch (tagUnits)
			{
			case TagUnits.FontUnits:
				this.m_marginLeft *= this.m_fontScale * this.m_fontAsset.fontInfo.TabWidth / (float)this.m_fontAsset.tabSize;
				break;
			case TagUnits.Percentage:
				this.m_marginLeft = (this.m_marginWidth - ((this.m_width != -1f) ? this.m_width : 0f)) * this.m_marginLeft / 100f;
				break;
			}
			this.m_marginLeft = ((this.m_marginLeft >= 0f) ? this.m_marginLeft : 0f);
			return true;
			IL_35D9:
			num13 = this.ConvertToFloat(this.m_htmlTag, this.m_xmlAttribute[0].valueStartIndex, this.m_xmlAttribute[0].valueLength);
			if (num13 == -9999f || num13 == 0f)
			{
				return false;
			}
			this.m_lineHeight = num13;
			switch (tagUnits)
			{
			case TagUnits.FontUnits:
				this.m_lineHeight *= this.m_fontAsset.fontInfo.LineHeight * this.m_fontScale;
				break;
			case TagUnits.Percentage:
				this.m_lineHeight = this.m_fontAsset.fontInfo.LineHeight * this.m_lineHeight / 100f * this.m_fontScale;
				break;
			}
			return true;
			IL_368B:
			this.m_lineHeight = -32767f;
			return true;
		}

		// Token: 0x060003FD RID: 1021 RVA: 0x0002ABB8 File Offset: 0x00028DB8
		protected TMP_Text()
		{
		}

		// Token: 0x060003FE RID: 1022 RVA: 0x0002AE9C File Offset: 0x0002909C
		// Note: this type is marked as 'beforefieldinit'.
		static TMP_Text()
		{
		}

		// Token: 0x040001CB RID: 459
		[SerializeField]
		[TextArea(3, 10)]
		protected string m_text;

		// Token: 0x040001CC RID: 460
		[SerializeField]
		protected bool m_isRightToLeft;

		// Token: 0x040001CD RID: 461
		[SerializeField]
		protected TMP_FontAsset m_fontAsset;

		// Token: 0x040001CE RID: 462
		protected TMP_FontAsset m_currentFontAsset;

		// Token: 0x040001CF RID: 463
		protected bool m_isSDFShader;

		// Token: 0x040001D0 RID: 464
		[SerializeField]
		protected Material m_sharedMaterial;

		// Token: 0x040001D1 RID: 465
		protected Material m_currentMaterial;

		// Token: 0x040001D2 RID: 466
		protected MaterialReference[] m_materialReferences = new MaterialReference[32];

		// Token: 0x040001D3 RID: 467
		protected Dictionary<int, int> m_materialReferenceIndexLookup = new Dictionary<int, int>();

		// Token: 0x040001D4 RID: 468
		protected TMP_XmlTagStack<MaterialReference> m_materialReferenceStack = new TMP_XmlTagStack<MaterialReference>(new MaterialReference[16]);

		// Token: 0x040001D5 RID: 469
		protected int m_currentMaterialIndex;

		// Token: 0x040001D6 RID: 470
		[SerializeField]
		protected Material[] m_fontSharedMaterials;

		// Token: 0x040001D7 RID: 471
		[SerializeField]
		protected Material m_fontMaterial;

		// Token: 0x040001D8 RID: 472
		[SerializeField]
		protected Material[] m_fontMaterials;

		// Token: 0x040001D9 RID: 473
		protected bool m_isMaterialDirty;

		// Token: 0x040001DA RID: 474
		[SerializeField]
		protected Color32 m_fontColor32 = Color.white;

		// Token: 0x040001DB RID: 475
		[SerializeField]
		protected Color m_fontColor = Color.white;

		// Token: 0x040001DC RID: 476
		protected static Color32 s_colorWhite = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);

		// Token: 0x040001DD RID: 477
		protected Color32 m_underlineColor = TMP_Text.s_colorWhite;

		// Token: 0x040001DE RID: 478
		protected Color32 m_strikethroughColor = TMP_Text.s_colorWhite;

		// Token: 0x040001DF RID: 479
		protected Color32 m_highlightColor = TMP_Text.s_colorWhite;

		// Token: 0x040001E0 RID: 480
		[SerializeField]
		protected bool m_enableVertexGradient;

		// Token: 0x040001E1 RID: 481
		[SerializeField]
		protected ColorMode m_colorMode = ColorMode.FourCornersGradient;

		// Token: 0x040001E2 RID: 482
		[SerializeField]
		protected VertexGradient m_fontColorGradient = new VertexGradient(Color.white);

		// Token: 0x040001E3 RID: 483
		[SerializeField]
		protected TMP_ColorGradient m_fontColorGradientPreset;

		// Token: 0x040001E4 RID: 484
		[SerializeField]
		protected TMP_SpriteAsset m_spriteAsset;

		// Token: 0x040001E5 RID: 485
		[SerializeField]
		protected bool m_tintAllSprites;

		// Token: 0x040001E6 RID: 486
		protected bool m_tintSprite;

		// Token: 0x040001E7 RID: 487
		protected Color32 m_spriteColor;

		// Token: 0x040001E8 RID: 488
		[SerializeField]
		protected bool m_overrideHtmlColors;

		// Token: 0x040001E9 RID: 489
		[SerializeField]
		protected Color32 m_faceColor = Color.white;

		// Token: 0x040001EA RID: 490
		[SerializeField]
		protected Color32 m_outlineColor = Color.black;

		// Token: 0x040001EB RID: 491
		protected float m_outlineWidth;

		// Token: 0x040001EC RID: 492
		[SerializeField]
		protected float m_fontSize = 36f;

		// Token: 0x040001ED RID: 493
		protected float m_currentFontSize;

		// Token: 0x040001EE RID: 494
		[SerializeField]
		protected float m_fontSizeBase = 36f;

		// Token: 0x040001EF RID: 495
		protected TMP_XmlTagStack<float> m_sizeStack = new TMP_XmlTagStack<float>(new float[16]);

		// Token: 0x040001F0 RID: 496
		[SerializeField]
		protected int m_fontWeight = 400;

		// Token: 0x040001F1 RID: 497
		protected int m_fontWeightInternal;

		// Token: 0x040001F2 RID: 498
		protected TMP_XmlTagStack<int> m_fontWeightStack = new TMP_XmlTagStack<int>(new int[16]);

		// Token: 0x040001F3 RID: 499
		[SerializeField]
		protected bool m_enableAutoSizing;

		// Token: 0x040001F4 RID: 500
		protected float m_maxFontSize;

		// Token: 0x040001F5 RID: 501
		protected float m_minFontSize;

		// Token: 0x040001F6 RID: 502
		[SerializeField]
		protected float m_fontSizeMin;

		// Token: 0x040001F7 RID: 503
		[SerializeField]
		protected float m_fontSizeMax;

		// Token: 0x040001F8 RID: 504
		[SerializeField]
		protected FontStyles m_fontStyle;

		// Token: 0x040001F9 RID: 505
		protected FontStyles m_style;

		// Token: 0x040001FA RID: 506
		protected TMP_BasicXmlTagStack m_fontStyleStack;

		// Token: 0x040001FB RID: 507
		protected bool m_isUsingBold;

		// Token: 0x040001FC RID: 508
		[SerializeField]
		[FormerlySerializedAs("m_lineJustification")]
		protected TextAlignmentOptions m_textAlignment = TextAlignmentOptions.TopLeft;

		// Token: 0x040001FD RID: 509
		protected TextAlignmentOptions m_lineJustification;

		// Token: 0x040001FE RID: 510
		protected TMP_XmlTagStack<TextAlignmentOptions> m_lineJustificationStack = new TMP_XmlTagStack<TextAlignmentOptions>(new TextAlignmentOptions[16]);

		// Token: 0x040001FF RID: 511
		protected Vector3[] m_textContainerLocalCorners = new Vector3[4];

		// Token: 0x04000200 RID: 512
		[SerializeField]
		protected bool m_isAlignmentEnumConverted;

		// Token: 0x04000201 RID: 513
		[SerializeField]
		protected float m_characterSpacing;

		// Token: 0x04000202 RID: 514
		protected float m_cSpacing;

		// Token: 0x04000203 RID: 515
		protected float m_monoSpacing;

		// Token: 0x04000204 RID: 516
		[SerializeField]
		protected float m_wordSpacing;

		// Token: 0x04000205 RID: 517
		[SerializeField]
		protected float m_lineSpacing;

		// Token: 0x04000206 RID: 518
		protected float m_lineSpacingDelta;

		// Token: 0x04000207 RID: 519
		protected float m_lineHeight = -32767f;

		// Token: 0x04000208 RID: 520
		[SerializeField]
		protected float m_lineSpacingMax;

		// Token: 0x04000209 RID: 521
		[SerializeField]
		protected float m_paragraphSpacing;

		// Token: 0x0400020A RID: 522
		[SerializeField]
		protected float m_charWidthMaxAdj;

		// Token: 0x0400020B RID: 523
		protected float m_charWidthAdjDelta;

		// Token: 0x0400020C RID: 524
		[SerializeField]
		protected bool m_enableWordWrapping;

		// Token: 0x0400020D RID: 525
		protected bool m_isCharacterWrappingEnabled;

		// Token: 0x0400020E RID: 526
		protected bool m_isNonBreakingSpace;

		// Token: 0x0400020F RID: 527
		protected bool m_isIgnoringAlignment;

		// Token: 0x04000210 RID: 528
		[SerializeField]
		protected float m_wordWrappingRatios = 0.4f;

		// Token: 0x04000211 RID: 529
		[SerializeField]
		protected TextOverflowModes m_overflowMode;

		// Token: 0x04000212 RID: 530
		[SerializeField]
		protected int m_firstOverflowCharacterIndex = -1;

		// Token: 0x04000213 RID: 531
		[SerializeField]
		protected TMP_Text m_linkedTextComponent;

		// Token: 0x04000214 RID: 532
		[SerializeField]
		protected bool m_isLinkedTextComponent;

		// Token: 0x04000215 RID: 533
		[SerializeField]
		protected bool m_isTextTruncated;

		// Token: 0x04000216 RID: 534
		[SerializeField]
		protected bool m_enableKerning;

		// Token: 0x04000217 RID: 535
		[SerializeField]
		protected bool m_enableExtraPadding;

		// Token: 0x04000218 RID: 536
		[SerializeField]
		protected bool checkPaddingRequired;

		// Token: 0x04000219 RID: 537
		[SerializeField]
		protected bool m_isRichText = true;

		// Token: 0x0400021A RID: 538
		[SerializeField]
		protected bool m_parseCtrlCharacters = true;

		// Token: 0x0400021B RID: 539
		protected bool m_isOverlay;

		// Token: 0x0400021C RID: 540
		[SerializeField]
		protected bool m_isOrthographic;

		// Token: 0x0400021D RID: 541
		[SerializeField]
		protected bool m_isCullingEnabled;

		// Token: 0x0400021E RID: 542
		[SerializeField]
		protected bool m_ignoreRectMaskCulling;

		// Token: 0x0400021F RID: 543
		[SerializeField]
		protected bool m_ignoreCulling = true;

		// Token: 0x04000220 RID: 544
		[SerializeField]
		protected TextureMappingOptions m_horizontalMapping;

		// Token: 0x04000221 RID: 545
		[SerializeField]
		protected TextureMappingOptions m_verticalMapping;

		// Token: 0x04000222 RID: 546
		[SerializeField]
		protected float m_uvLineOffset;

		// Token: 0x04000223 RID: 547
		protected TextRenderFlags m_renderMode = TextRenderFlags.Render;

		// Token: 0x04000224 RID: 548
		[SerializeField]
		protected VertexSortingOrder m_geometrySortingOrder;

		// Token: 0x04000225 RID: 549
		[SerializeField]
		protected int m_firstVisibleCharacter;

		// Token: 0x04000226 RID: 550
		protected int m_maxVisibleCharacters = 99999;

		// Token: 0x04000227 RID: 551
		protected int m_maxVisibleWords = 99999;

		// Token: 0x04000228 RID: 552
		protected int m_maxVisibleLines = 99999;

		// Token: 0x04000229 RID: 553
		[SerializeField]
		protected bool m_useMaxVisibleDescender = true;

		// Token: 0x0400022A RID: 554
		[SerializeField]
		protected int m_pageToDisplay = 1;

		// Token: 0x0400022B RID: 555
		protected bool m_isNewPage;

		// Token: 0x0400022C RID: 556
		[SerializeField]
		protected Vector4 m_margin = new Vector4(0f, 0f, 0f, 0f);

		// Token: 0x0400022D RID: 557
		protected float m_marginLeft;

		// Token: 0x0400022E RID: 558
		protected float m_marginRight;

		// Token: 0x0400022F RID: 559
		protected float m_marginWidth;

		// Token: 0x04000230 RID: 560
		protected float m_marginHeight;

		// Token: 0x04000231 RID: 561
		protected float m_width = -1f;

		// Token: 0x04000232 RID: 562
		[SerializeField]
		protected TMP_TextInfo m_textInfo;

		// Token: 0x04000233 RID: 563
		[SerializeField]
		protected bool m_havePropertiesChanged;

		// Token: 0x04000234 RID: 564
		[SerializeField]
		protected bool m_isUsingLegacyAnimationComponent;

		// Token: 0x04000235 RID: 565
		protected Transform m_transform;

		// Token: 0x04000236 RID: 566
		protected RectTransform m_rectTransform;

		// Token: 0x04000237 RID: 567
		[CompilerGenerated]
		private bool <autoSizeTextContainer>k__BackingField;

		// Token: 0x04000238 RID: 568
		protected bool m_autoSizeTextContainer;

		// Token: 0x04000239 RID: 569
		protected Mesh m_mesh;

		// Token: 0x0400023A RID: 570
		[SerializeField]
		protected bool m_isVolumetricText;

		// Token: 0x0400023B RID: 571
		[SerializeField]
		protected TMP_SpriteAnimator m_spriteAnimator;

		// Token: 0x0400023C RID: 572
		protected float m_flexibleHeight = -1f;

		// Token: 0x0400023D RID: 573
		protected float m_flexibleWidth = -1f;

		// Token: 0x0400023E RID: 574
		protected float m_minWidth;

		// Token: 0x0400023F RID: 575
		protected float m_minHeight;

		// Token: 0x04000240 RID: 576
		protected float m_maxWidth;

		// Token: 0x04000241 RID: 577
		protected float m_maxHeight;

		// Token: 0x04000242 RID: 578
		protected LayoutElement m_LayoutElement;

		// Token: 0x04000243 RID: 579
		protected float m_preferredWidth;

		// Token: 0x04000244 RID: 580
		protected float m_renderedWidth;

		// Token: 0x04000245 RID: 581
		protected bool m_isPreferredWidthDirty;

		// Token: 0x04000246 RID: 582
		protected float m_preferredHeight;

		// Token: 0x04000247 RID: 583
		protected float m_renderedHeight;

		// Token: 0x04000248 RID: 584
		protected bool m_isPreferredHeightDirty;

		// Token: 0x04000249 RID: 585
		protected bool m_isCalculatingPreferredValues;

		// Token: 0x0400024A RID: 586
		private int m_recursiveCount;

		// Token: 0x0400024B RID: 587
		protected int m_layoutPriority;

		// Token: 0x0400024C RID: 588
		protected bool m_isCalculateSizeRequired;

		// Token: 0x0400024D RID: 589
		protected bool m_isLayoutDirty;

		// Token: 0x0400024E RID: 590
		protected bool m_verticesAlreadyDirty;

		// Token: 0x0400024F RID: 591
		protected bool m_layoutAlreadyDirty;

		// Token: 0x04000250 RID: 592
		protected bool m_isAwake;

		// Token: 0x04000251 RID: 593
		protected bool m_isWaitingOnResourceLoad;

		// Token: 0x04000252 RID: 594
		[SerializeField]
		protected bool m_isInputParsingRequired;

		// Token: 0x04000253 RID: 595
		[SerializeField]
		protected TMP_Text.TextInputSources m_inputSource;

		// Token: 0x04000254 RID: 596
		protected string old_text;

		// Token: 0x04000255 RID: 597
		protected float m_fontScale;

		// Token: 0x04000256 RID: 598
		protected float m_fontScaleMultiplier;

		// Token: 0x04000257 RID: 599
		protected char[] m_htmlTag = new char[128];

		// Token: 0x04000258 RID: 600
		protected XML_TagAttribute[] m_xmlAttribute = new XML_TagAttribute[8];

		// Token: 0x04000259 RID: 601
		protected float[] m_attributeParameterValues = new float[16];

		// Token: 0x0400025A RID: 602
		protected float tag_LineIndent;

		// Token: 0x0400025B RID: 603
		protected float tag_Indent;

		// Token: 0x0400025C RID: 604
		protected TMP_XmlTagStack<float> m_indentStack = new TMP_XmlTagStack<float>(new float[16]);

		// Token: 0x0400025D RID: 605
		protected bool tag_NoParsing;

		// Token: 0x0400025E RID: 606
		protected bool m_isParsingText;

		// Token: 0x0400025F RID: 607
		protected Matrix4x4 m_FXMatrix;

		// Token: 0x04000260 RID: 608
		protected bool m_isFXMatrixSet;

		// Token: 0x04000261 RID: 609
		protected int[] m_char_buffer;

		// Token: 0x04000262 RID: 610
		private TMP_CharacterInfo[] m_internalCharacterInfo;

		// Token: 0x04000263 RID: 611
		protected char[] m_input_CharArray = new char[256];

		// Token: 0x04000264 RID: 612
		private int m_charArray_Length;

		// Token: 0x04000265 RID: 613
		protected int m_totalCharacterCount;

		// Token: 0x04000266 RID: 614
		protected WordWrapState m_SavedWordWrapState;

		// Token: 0x04000267 RID: 615
		protected WordWrapState m_SavedLineState;

		// Token: 0x04000268 RID: 616
		protected int m_characterCount;

		// Token: 0x04000269 RID: 617
		protected int m_firstCharacterOfLine;

		// Token: 0x0400026A RID: 618
		protected int m_firstVisibleCharacterOfLine;

		// Token: 0x0400026B RID: 619
		protected int m_lastCharacterOfLine;

		// Token: 0x0400026C RID: 620
		protected int m_lastVisibleCharacterOfLine;

		// Token: 0x0400026D RID: 621
		protected int m_lineNumber;

		// Token: 0x0400026E RID: 622
		protected int m_lineVisibleCharacterCount;

		// Token: 0x0400026F RID: 623
		protected int m_pageNumber;

		// Token: 0x04000270 RID: 624
		protected float m_maxAscender;

		// Token: 0x04000271 RID: 625
		protected float m_maxCapHeight;

		// Token: 0x04000272 RID: 626
		protected float m_maxDescender;

		// Token: 0x04000273 RID: 627
		protected float m_maxLineAscender;

		// Token: 0x04000274 RID: 628
		protected float m_maxLineDescender;

		// Token: 0x04000275 RID: 629
		protected float m_startOfLineAscender;

		// Token: 0x04000276 RID: 630
		protected float m_lineOffset;

		// Token: 0x04000277 RID: 631
		protected Extents m_meshExtents;

		// Token: 0x04000278 RID: 632
		protected Color32 m_htmlColor = new Color(255f, 255f, 255f, 128f);

		// Token: 0x04000279 RID: 633
		protected TMP_XmlTagStack<Color32> m_colorStack = new TMP_XmlTagStack<Color32>(new Color32[16]);

		// Token: 0x0400027A RID: 634
		protected TMP_XmlTagStack<Color32> m_underlineColorStack = new TMP_XmlTagStack<Color32>(new Color32[16]);

		// Token: 0x0400027B RID: 635
		protected TMP_XmlTagStack<Color32> m_strikethroughColorStack = new TMP_XmlTagStack<Color32>(new Color32[16]);

		// Token: 0x0400027C RID: 636
		protected TMP_XmlTagStack<Color32> m_highlightColorStack = new TMP_XmlTagStack<Color32>(new Color32[16]);

		// Token: 0x0400027D RID: 637
		protected TMP_ColorGradient m_colorGradientPreset;

		// Token: 0x0400027E RID: 638
		protected TMP_XmlTagStack<TMP_ColorGradient> m_colorGradientStack = new TMP_XmlTagStack<TMP_ColorGradient>(new TMP_ColorGradient[16]);

		// Token: 0x0400027F RID: 639
		protected float m_tabSpacing;

		// Token: 0x04000280 RID: 640
		protected float m_spacing;

		// Token: 0x04000281 RID: 641
		protected TMP_XmlTagStack<int> m_styleStack = new TMP_XmlTagStack<int>(new int[16]);

		// Token: 0x04000282 RID: 642
		protected TMP_XmlTagStack<int> m_actionStack = new TMP_XmlTagStack<int>(new int[16]);

		// Token: 0x04000283 RID: 643
		protected float m_padding;

		// Token: 0x04000284 RID: 644
		protected float m_baselineOffset;

		// Token: 0x04000285 RID: 645
		protected TMP_XmlTagStack<float> m_baselineOffsetStack = new TMP_XmlTagStack<float>(new float[16]);

		// Token: 0x04000286 RID: 646
		protected float m_xAdvance;

		// Token: 0x04000287 RID: 647
		protected TMP_TextElementType m_textElementType;

		// Token: 0x04000288 RID: 648
		protected TMP_TextElement m_cached_TextElement;

		// Token: 0x04000289 RID: 649
		protected TMP_Glyph m_cached_Underline_GlyphInfo;

		// Token: 0x0400028A RID: 650
		protected TMP_Glyph m_cached_Ellipsis_GlyphInfo;

		// Token: 0x0400028B RID: 651
		protected TMP_SpriteAsset m_defaultSpriteAsset;

		// Token: 0x0400028C RID: 652
		protected TMP_SpriteAsset m_currentSpriteAsset;

		// Token: 0x0400028D RID: 653
		protected int m_spriteCount;

		// Token: 0x0400028E RID: 654
		protected int m_spriteIndex;

		// Token: 0x0400028F RID: 655
		protected int m_spriteAnimationID;

		// Token: 0x04000290 RID: 656
		protected bool m_ignoreActiveState;

		// Token: 0x04000291 RID: 657
		private readonly float[] k_Power = new float[]
		{
			0.5f,
			0.05f,
			0.005f,
			0.0005f,
			5E-05f,
			5E-06f,
			5E-07f,
			5E-08f,
			5E-09f,
			5E-10f
		};

		// Token: 0x04000292 RID: 658
		protected static Vector2 k_LargePositiveVector2 = new Vector2(2.1474836E+09f, 2.1474836E+09f);

		// Token: 0x04000293 RID: 659
		protected static Vector2 k_LargeNegativeVector2 = new Vector2(-2.1474836E+09f, -2.1474836E+09f);

		// Token: 0x04000294 RID: 660
		protected static float k_LargePositiveFloat = 32767f;

		// Token: 0x04000295 RID: 661
		protected static float k_LargeNegativeFloat = -32767f;

		// Token: 0x04000296 RID: 662
		protected static int k_LargePositiveInt = int.MaxValue;

		// Token: 0x04000297 RID: 663
		protected static int k_LargeNegativeInt = -2147483647;

		// Token: 0x02000084 RID: 132
		protected enum TextInputSources
		{
			// Token: 0x04000464 RID: 1124
			Text,
			// Token: 0x04000465 RID: 1125
			SetText,
			// Token: 0x04000466 RID: 1126
			SetCharArray,
			// Token: 0x04000467 RID: 1127
			String
		}
	}
}
