﻿using System;

namespace TMPro
{
	// Token: 0x0200002C RID: 44
	public enum TextAlignmentOptions
	{
		// Token: 0x04000160 RID: 352
		TopLeft = 257,
		// Token: 0x04000161 RID: 353
		Top,
		// Token: 0x04000162 RID: 354
		TopRight = 260,
		// Token: 0x04000163 RID: 355
		TopJustified = 264,
		// Token: 0x04000164 RID: 356
		TopFlush = 272,
		// Token: 0x04000165 RID: 357
		TopGeoAligned = 288,
		// Token: 0x04000166 RID: 358
		Left = 513,
		// Token: 0x04000167 RID: 359
		Center,
		// Token: 0x04000168 RID: 360
		Right = 516,
		// Token: 0x04000169 RID: 361
		Justified = 520,
		// Token: 0x0400016A RID: 362
		Flush = 528,
		// Token: 0x0400016B RID: 363
		CenterGeoAligned = 544,
		// Token: 0x0400016C RID: 364
		BottomLeft = 1025,
		// Token: 0x0400016D RID: 365
		Bottom,
		// Token: 0x0400016E RID: 366
		BottomRight = 1028,
		// Token: 0x0400016F RID: 367
		BottomJustified = 1032,
		// Token: 0x04000170 RID: 368
		BottomFlush = 1040,
		// Token: 0x04000171 RID: 369
		BottomGeoAligned = 1056,
		// Token: 0x04000172 RID: 370
		BaselineLeft = 2049,
		// Token: 0x04000173 RID: 371
		Baseline,
		// Token: 0x04000174 RID: 372
		BaselineRight = 2052,
		// Token: 0x04000175 RID: 373
		BaselineJustified = 2056,
		// Token: 0x04000176 RID: 374
		BaselineFlush = 2064,
		// Token: 0x04000177 RID: 375
		BaselineGeoAligned = 2080,
		// Token: 0x04000178 RID: 376
		MidlineLeft = 4097,
		// Token: 0x04000179 RID: 377
		Midline,
		// Token: 0x0400017A RID: 378
		MidlineRight = 4100,
		// Token: 0x0400017B RID: 379
		MidlineJustified = 4104,
		// Token: 0x0400017C RID: 380
		MidlineFlush = 4112,
		// Token: 0x0400017D RID: 381
		MidlineGeoAligned = 4128,
		// Token: 0x0400017E RID: 382
		CaplineLeft = 8193,
		// Token: 0x0400017F RID: 383
		Capline,
		// Token: 0x04000180 RID: 384
		CaplineRight = 8196,
		// Token: 0x04000181 RID: 385
		CaplineJustified = 8200,
		// Token: 0x04000182 RID: 386
		CaplineFlush = 8208,
		// Token: 0x04000183 RID: 387
		CaplineGeoAligned = 8224
	}
}
