﻿using System;

namespace TMPro
{
	// Token: 0x02000035 RID: 53
	public enum FontStyles
	{
		// Token: 0x040001AD RID: 429
		Normal,
		// Token: 0x040001AE RID: 430
		Bold,
		// Token: 0x040001AF RID: 431
		Italic,
		// Token: 0x040001B0 RID: 432
		Underline = 4,
		// Token: 0x040001B1 RID: 433
		LowerCase = 8,
		// Token: 0x040001B2 RID: 434
		UpperCase = 16,
		// Token: 0x040001B3 RID: 435
		SmallCaps = 32,
		// Token: 0x040001B4 RID: 436
		Strikethrough = 64,
		// Token: 0x040001B5 RID: 437
		Superscript = 128,
		// Token: 0x040001B6 RID: 438
		Subscript = 256,
		// Token: 0x040001B7 RID: 439
		Highlight = 512
	}
}
