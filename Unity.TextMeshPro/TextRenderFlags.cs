﻿using System;

namespace TMPro
{
	// Token: 0x0200002F RID: 47
	public enum TextRenderFlags
	{
		// Token: 0x04000193 RID: 403
		DontRender,
		// Token: 0x04000194 RID: 404
		Render = 255
	}
}
