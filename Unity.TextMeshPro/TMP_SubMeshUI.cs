﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace TMPro
{
	// Token: 0x0200002A RID: 42
	[ExecuteInEditMode]
	public class TMP_SubMeshUI : MaskableGraphic, IClippable, IMaskable, IMaterialModifier
	{
		// Token: 0x1700009E RID: 158
		// (get) Token: 0x060002C1 RID: 705 RVA: 0x0001EF34 File Offset: 0x0001D134
		// (set) Token: 0x060002C2 RID: 706 RVA: 0x0001EF3C File Offset: 0x0001D13C
		public TMP_FontAsset fontAsset
		{
			get
			{
				return this.m_fontAsset;
			}
			set
			{
				this.m_fontAsset = value;
			}
		}

		// Token: 0x1700009F RID: 159
		// (get) Token: 0x060002C3 RID: 707 RVA: 0x0001EF45 File Offset: 0x0001D145
		// (set) Token: 0x060002C4 RID: 708 RVA: 0x0001EF4D File Offset: 0x0001D14D
		public TMP_SpriteAsset spriteAsset
		{
			get
			{
				return this.m_spriteAsset;
			}
			set
			{
				this.m_spriteAsset = value;
			}
		}

		// Token: 0x170000A0 RID: 160
		// (get) Token: 0x060002C5 RID: 709 RVA: 0x0001EF56 File Offset: 0x0001D156
		public override Texture mainTexture
		{
			get
			{
				if (this.sharedMaterial != null)
				{
					return this.sharedMaterial.GetTexture(ShaderUtilities.ID_MainTex);
				}
				return null;
			}
		}

		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x060002C6 RID: 710 RVA: 0x0001EF78 File Offset: 0x0001D178
		// (set) Token: 0x060002C7 RID: 711 RVA: 0x0001EF88 File Offset: 0x0001D188
		public override Material material
		{
			get
			{
				return this.GetMaterial(this.m_sharedMaterial);
			}
			set
			{
				if (this.m_sharedMaterial != null && this.m_sharedMaterial.GetInstanceID() == value.GetInstanceID())
				{
					return;
				}
				this.m_material = value;
				this.m_sharedMaterial = value;
				this.m_padding = this.GetPaddingForMaterial();
				this.SetVerticesDirty();
				this.SetMaterialDirty();
			}
		}

		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x060002C8 RID: 712 RVA: 0x0001EFDF File Offset: 0x0001D1DF
		// (set) Token: 0x060002C9 RID: 713 RVA: 0x0001EFE7 File Offset: 0x0001D1E7
		public Material sharedMaterial
		{
			get
			{
				return this.m_sharedMaterial;
			}
			set
			{
				this.SetSharedMaterial(value);
			}
		}

		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x060002CA RID: 714 RVA: 0x0001EFF0 File Offset: 0x0001D1F0
		// (set) Token: 0x060002CB RID: 715 RVA: 0x0001EFF8 File Offset: 0x0001D1F8
		public Material fallbackMaterial
		{
			get
			{
				return this.m_fallbackMaterial;
			}
			set
			{
				if (this.m_fallbackMaterial == value)
				{
					return;
				}
				if (this.m_fallbackMaterial != null && this.m_fallbackMaterial != value)
				{
					TMP_MaterialManager.ReleaseFallbackMaterial(this.m_fallbackMaterial);
				}
				this.m_fallbackMaterial = value;
				TMP_MaterialManager.AddFallbackMaterialReference(this.m_fallbackMaterial);
				this.SetSharedMaterial(this.m_fallbackMaterial);
			}
		}

		// Token: 0x170000A4 RID: 164
		// (get) Token: 0x060002CC RID: 716 RVA: 0x0001F059 File Offset: 0x0001D259
		// (set) Token: 0x060002CD RID: 717 RVA: 0x0001F061 File Offset: 0x0001D261
		public Material fallbackSourceMaterial
		{
			get
			{
				return this.m_fallbackSourceMaterial;
			}
			set
			{
				this.m_fallbackSourceMaterial = value;
			}
		}

		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x060002CE RID: 718 RVA: 0x0001F06A File Offset: 0x0001D26A
		public override Material materialForRendering
		{
			get
			{
				return TMP_MaterialManager.GetMaterialForRendering(this, this.m_sharedMaterial);
			}
		}

		// Token: 0x170000A6 RID: 166
		// (get) Token: 0x060002CF RID: 719 RVA: 0x0001F078 File Offset: 0x0001D278
		// (set) Token: 0x060002D0 RID: 720 RVA: 0x0001F080 File Offset: 0x0001D280
		public bool isDefaultMaterial
		{
			get
			{
				return this.m_isDefaultMaterial;
			}
			set
			{
				this.m_isDefaultMaterial = value;
			}
		}

		// Token: 0x170000A7 RID: 167
		// (get) Token: 0x060002D1 RID: 721 RVA: 0x0001F089 File Offset: 0x0001D289
		// (set) Token: 0x060002D2 RID: 722 RVA: 0x0001F091 File Offset: 0x0001D291
		public float padding
		{
			get
			{
				return this.m_padding;
			}
			set
			{
				this.m_padding = value;
			}
		}

		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x060002D3 RID: 723 RVA: 0x0001F09A File Offset: 0x0001D29A
		public new CanvasRenderer canvasRenderer
		{
			get
			{
				if (this.m_canvasRenderer == null)
				{
					this.m_canvasRenderer = base.GetComponent<CanvasRenderer>();
				}
				return this.m_canvasRenderer;
			}
		}

		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x060002D4 RID: 724 RVA: 0x0001F0BC File Offset: 0x0001D2BC
		// (set) Token: 0x060002D5 RID: 725 RVA: 0x0001F0EA File Offset: 0x0001D2EA
		public Mesh mesh
		{
			get
			{
				if (this.m_mesh == null)
				{
					this.m_mesh = new Mesh();
					this.m_mesh.hideFlags = HideFlags.HideAndDontSave;
				}
				return this.m_mesh;
			}
			set
			{
				this.m_mesh = value;
			}
		}

		// Token: 0x060002D6 RID: 726 RVA: 0x0001F0F4 File Offset: 0x0001D2F4
		public static TMP_SubMeshUI AddSubTextObject(TextMeshProUGUI textComponent, MaterialReference materialReference)
		{
			GameObject gameObject = new GameObject("TMP UI SubObject [" + materialReference.material.name + "]", new Type[]
			{
				typeof(RectTransform)
			});
			gameObject.transform.SetParent(textComponent.transform, false);
			gameObject.layer = textComponent.gameObject.layer;
			RectTransform component = gameObject.GetComponent<RectTransform>();
			component.anchorMin = Vector2.zero;
			component.anchorMax = Vector2.one;
			component.sizeDelta = Vector2.zero;
			component.pivot = textComponent.rectTransform.pivot;
			TMP_SubMeshUI tmp_SubMeshUI = gameObject.AddComponent<TMP_SubMeshUI>();
			tmp_SubMeshUI.m_canvasRenderer = tmp_SubMeshUI.canvasRenderer;
			tmp_SubMeshUI.m_TextComponent = textComponent;
			tmp_SubMeshUI.m_materialReferenceIndex = materialReference.index;
			tmp_SubMeshUI.m_fontAsset = materialReference.fontAsset;
			tmp_SubMeshUI.m_spriteAsset = materialReference.spriteAsset;
			tmp_SubMeshUI.m_isDefaultMaterial = materialReference.isDefaultMaterial;
			tmp_SubMeshUI.SetSharedMaterial(materialReference.material);
			return tmp_SubMeshUI;
		}

		// Token: 0x060002D7 RID: 727 RVA: 0x0001F1E1 File Offset: 0x0001D3E1
		protected override void OnEnable()
		{
			if (!this.m_isRegisteredForEvents)
			{
				this.m_isRegisteredForEvents = true;
			}
			this.m_ShouldRecalculateStencil = true;
			this.RecalculateClipping();
			this.RecalculateMasking();
		}

		// Token: 0x060002D8 RID: 728 RVA: 0x0001F208 File Offset: 0x0001D408
		protected override void OnDisable()
		{
			TMP_UpdateRegistry.UnRegisterCanvasElementForRebuild(this);
			if (this.m_MaskMaterial != null)
			{
				TMP_MaterialManager.ReleaseStencilMaterial(this.m_MaskMaterial);
				this.m_MaskMaterial = null;
			}
			if (this.m_fallbackMaterial != null)
			{
				TMP_MaterialManager.ReleaseFallbackMaterial(this.m_fallbackMaterial);
				this.m_fallbackMaterial = null;
			}
			base.OnDisable();
		}

		// Token: 0x060002D9 RID: 729 RVA: 0x0001F264 File Offset: 0x0001D464
		protected override void OnDestroy()
		{
			if (this.m_mesh != null)
			{
				Object.DestroyImmediate(this.m_mesh);
			}
			if (this.m_MaskMaterial != null)
			{
				TMP_MaterialManager.ReleaseStencilMaterial(this.m_MaskMaterial);
			}
			if (this.m_fallbackMaterial != null)
			{
				TMP_MaterialManager.ReleaseFallbackMaterial(this.m_fallbackMaterial);
				this.m_fallbackMaterial = null;
			}
			this.m_isRegisteredForEvents = false;
			this.RecalculateClipping();
		}

		// Token: 0x060002DA RID: 730 RVA: 0x0001F2D0 File Offset: 0x0001D4D0
		protected override void OnTransformParentChanged()
		{
			if (!this.IsActive())
			{
				return;
			}
			this.m_ShouldRecalculateStencil = true;
			this.RecalculateClipping();
			this.RecalculateMasking();
		}

		// Token: 0x060002DB RID: 731 RVA: 0x0001F2F0 File Offset: 0x0001D4F0
		public override Material GetModifiedMaterial(Material baseMaterial)
		{
			Material material = baseMaterial;
			if (this.m_ShouldRecalculateStencil)
			{
				this.m_StencilValue = TMP_MaterialManager.GetStencilID(base.gameObject);
				this.m_ShouldRecalculateStencil = false;
			}
			if (this.m_StencilValue > 0)
			{
				material = TMP_MaterialManager.GetStencilMaterial(baseMaterial, this.m_StencilValue);
				if (this.m_MaskMaterial != null)
				{
					TMP_MaterialManager.ReleaseStencilMaterial(this.m_MaskMaterial);
				}
				this.m_MaskMaterial = material;
			}
			return material;
		}

		// Token: 0x060002DC RID: 732 RVA: 0x0001F356 File Offset: 0x0001D556
		public float GetPaddingForMaterial()
		{
			return ShaderUtilities.GetPadding(this.m_sharedMaterial, this.m_TextComponent.extraPadding, this.m_TextComponent.isUsingBold);
		}

		// Token: 0x060002DD RID: 733 RVA: 0x0001F379 File Offset: 0x0001D579
		public float GetPaddingForMaterial(Material mat)
		{
			return ShaderUtilities.GetPadding(mat, this.m_TextComponent.extraPadding, this.m_TextComponent.isUsingBold);
		}

		// Token: 0x060002DE RID: 734 RVA: 0x0001F397 File Offset: 0x0001D597
		public void UpdateMeshPadding(bool isExtraPadding, bool isUsingBold)
		{
			this.m_padding = ShaderUtilities.GetPadding(this.m_sharedMaterial, isExtraPadding, isUsingBold);
		}

		// Token: 0x060002DF RID: 735 RVA: 0x0000297D File Offset: 0x00000B7D
		public override void SetAllDirty()
		{
		}

		// Token: 0x060002E0 RID: 736 RVA: 0x0001F3AC File Offset: 0x0001D5AC
		public override void SetVerticesDirty()
		{
			if (!this.IsActive())
			{
				return;
			}
			if (this.m_TextComponent != null)
			{
				this.m_TextComponent.havePropertiesChanged = true;
				this.m_TextComponent.SetVerticesDirty();
			}
		}

		// Token: 0x060002E1 RID: 737 RVA: 0x0000297D File Offset: 0x00000B7D
		public override void SetLayoutDirty()
		{
		}

		// Token: 0x060002E2 RID: 738 RVA: 0x0001F3DC File Offset: 0x0001D5DC
		public override void SetMaterialDirty()
		{
			this.m_materialDirty = true;
			this.UpdateMaterial();
			if (this.m_OnDirtyMaterialCallback != null)
			{
				this.m_OnDirtyMaterialCallback();
			}
		}

		// Token: 0x060002E3 RID: 739 RVA: 0x0001F3FE File Offset: 0x0001D5FE
		public void SetPivotDirty()
		{
			if (!this.IsActive())
			{
				return;
			}
			base.rectTransform.pivot = this.m_TextComponent.rectTransform.pivot;
		}

		// Token: 0x060002E4 RID: 740 RVA: 0x0001F424 File Offset: 0x0001D624
		public override void Cull(Rect clipRect, bool validRect)
		{
			if (this.m_TextComponent.ignoreRectMaskCulling)
			{
				return;
			}
			base.Cull(clipRect, validRect);
		}

		// Token: 0x060002E5 RID: 741 RVA: 0x0001F43C File Offset: 0x0001D63C
		protected override void UpdateGeometry()
		{
			Debug.Log("UpdateGeometry()");
		}

		// Token: 0x060002E6 RID: 742 RVA: 0x0001F448 File Offset: 0x0001D648
		public override void Rebuild(CanvasUpdate update)
		{
			if (update == CanvasUpdate.PreRender)
			{
				if (!this.m_materialDirty)
				{
					return;
				}
				this.UpdateMaterial();
				this.m_materialDirty = false;
			}
		}

		// Token: 0x060002E7 RID: 743 RVA: 0x00002F9F File Offset: 0x0000119F
		public void RefreshMaterial()
		{
			this.UpdateMaterial();
		}

		// Token: 0x060002E8 RID: 744 RVA: 0x0001F464 File Offset: 0x0001D664
		protected override void UpdateMaterial()
		{
			if (this.m_canvasRenderer == null)
			{
				this.m_canvasRenderer = this.canvasRenderer;
			}
			this.m_canvasRenderer.materialCount = 1;
			this.m_canvasRenderer.SetMaterial(this.materialForRendering, 0);
			this.m_canvasRenderer.SetTexture(this.mainTexture);
		}

		// Token: 0x060002E9 RID: 745 RVA: 0x0000C3AA File Offset: 0x0000A5AA
		public override void RecalculateClipping()
		{
			base.RecalculateClipping();
		}

		// Token: 0x060002EA RID: 746 RVA: 0x0000C3B2 File Offset: 0x0000A5B2
		public override void RecalculateMasking()
		{
			this.m_ShouldRecalculateStencil = true;
			this.SetMaterialDirty();
		}

		// Token: 0x060002EB RID: 747 RVA: 0x0001EFDF File Offset: 0x0001D1DF
		private Material GetMaterial()
		{
			return this.m_sharedMaterial;
		}

		// Token: 0x060002EC RID: 748 RVA: 0x0001F4BC File Offset: 0x0001D6BC
		private Material GetMaterial(Material mat)
		{
			if (this.m_material == null || this.m_material.GetInstanceID() != mat.GetInstanceID())
			{
				this.m_material = this.CreateMaterialInstance(mat);
			}
			this.m_sharedMaterial = this.m_material;
			this.m_padding = this.GetPaddingForMaterial();
			this.SetVerticesDirty();
			this.SetMaterialDirty();
			return this.m_sharedMaterial;
		}

		// Token: 0x060002ED RID: 749 RVA: 0x0001EE2B File Offset: 0x0001D02B
		private Material CreateMaterialInstance(Material source)
		{
			Material material = new Material(source);
			material.shaderKeywords = source.shaderKeywords;
			material.name += " (Instance)";
			return material;
		}

		// Token: 0x060002EE RID: 750 RVA: 0x0001F521 File Offset: 0x0001D721
		private Material GetSharedMaterial()
		{
			if (this.m_canvasRenderer == null)
			{
				this.m_canvasRenderer = base.GetComponent<CanvasRenderer>();
			}
			return this.m_canvasRenderer.GetMaterial();
		}

		// Token: 0x060002EF RID: 751 RVA: 0x0001F548 File Offset: 0x0001D748
		private void SetSharedMaterial(Material mat)
		{
			this.m_sharedMaterial = mat;
			this.m_Material = this.m_sharedMaterial;
			this.m_padding = this.GetPaddingForMaterial();
			this.SetMaterialDirty();
		}

		// Token: 0x060002F0 RID: 752 RVA: 0x0001DFA7 File Offset: 0x0001C1A7
		public TMP_SubMeshUI()
		{
		}

		// Token: 0x04000151 RID: 337
		[SerializeField]
		private TMP_FontAsset m_fontAsset;

		// Token: 0x04000152 RID: 338
		[SerializeField]
		private TMP_SpriteAsset m_spriteAsset;

		// Token: 0x04000153 RID: 339
		[SerializeField]
		private Material m_material;

		// Token: 0x04000154 RID: 340
		[SerializeField]
		private Material m_sharedMaterial;

		// Token: 0x04000155 RID: 341
		private Material m_fallbackMaterial;

		// Token: 0x04000156 RID: 342
		private Material m_fallbackSourceMaterial;

		// Token: 0x04000157 RID: 343
		[SerializeField]
		private bool m_isDefaultMaterial;

		// Token: 0x04000158 RID: 344
		[SerializeField]
		private float m_padding;

		// Token: 0x04000159 RID: 345
		[SerializeField]
		private CanvasRenderer m_canvasRenderer;

		// Token: 0x0400015A RID: 346
		private Mesh m_mesh;

		// Token: 0x0400015B RID: 347
		[SerializeField]
		private TextMeshProUGUI m_TextComponent;

		// Token: 0x0400015C RID: 348
		[NonSerialized]
		private bool m_isRegisteredForEvents;

		// Token: 0x0400015D RID: 349
		private bool m_materialDirty;

		// Token: 0x0400015E RID: 350
		[SerializeField]
		private int m_materialReferenceIndex;
	}
}
