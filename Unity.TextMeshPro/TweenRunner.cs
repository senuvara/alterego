﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace TMPro
{
	// Token: 0x02000013 RID: 19
	internal class TweenRunner<T> where T : struct, ITweenValue
	{
		// Token: 0x06000104 RID: 260 RVA: 0x00015941 File Offset: 0x00013B41
		private static IEnumerator Start(T tweenInfo)
		{
			if (!tweenInfo.ValidTarget())
			{
				yield break;
			}
			float elapsedTime = 0f;
			while (elapsedTime < tweenInfo.duration)
			{
				elapsedTime += (tweenInfo.ignoreTimeScale ? Time.unscaledDeltaTime : Time.deltaTime);
				float floatPercentage = Mathf.Clamp01(elapsedTime / tweenInfo.duration);
				tweenInfo.TweenValue(floatPercentage);
				yield return null;
			}
			tweenInfo.TweenValue(1f);
			yield break;
		}

		// Token: 0x06000105 RID: 261 RVA: 0x00015950 File Offset: 0x00013B50
		public void Init(MonoBehaviour coroutineContainer)
		{
			this.m_CoroutineContainer = coroutineContainer;
		}

		// Token: 0x06000106 RID: 262 RVA: 0x0001595C File Offset: 0x00013B5C
		public void StartTween(T info)
		{
			if (this.m_CoroutineContainer == null)
			{
				Debug.LogWarning("Coroutine container not configured... did you forget to call Init?");
				return;
			}
			this.StopTween();
			if (!this.m_CoroutineContainer.gameObject.activeInHierarchy)
			{
				info.TweenValue(1f);
				return;
			}
			this.m_Tween = TweenRunner<T>.Start(info);
			this.m_CoroutineContainer.StartCoroutine(this.m_Tween);
		}

		// Token: 0x06000107 RID: 263 RVA: 0x000159CB File Offset: 0x00013BCB
		public void StopTween()
		{
			if (this.m_Tween != null)
			{
				this.m_CoroutineContainer.StopCoroutine(this.m_Tween);
				this.m_Tween = null;
			}
		}

		// Token: 0x06000108 RID: 264 RVA: 0x000159ED File Offset: 0x00013BED
		public TweenRunner()
		{
		}

		// Token: 0x0400006C RID: 108
		protected MonoBehaviour m_CoroutineContainer;

		// Token: 0x0400006D RID: 109
		protected IEnumerator m_Tween;

		// Token: 0x02000065 RID: 101
		[CompilerGenerated]
		private sealed class <Start>d__2 : IEnumerator<object>, IEnumerator, IDisposable
		{
			// Token: 0x06000494 RID: 1172 RVA: 0x0002F2BE File Offset: 0x0002D4BE
			[DebuggerHidden]
			public <Start>d__2(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x06000495 RID: 1173 RVA: 0x0000297D File Offset: 0x00000B7D
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06000496 RID: 1174 RVA: 0x0002F2D0 File Offset: 0x0002D4D0
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
				}
				else
				{
					this.<>1__state = -1;
					if (!tweenInfo.ValidTarget())
					{
						return false;
					}
					elapsedTime = 0f;
				}
				if (elapsedTime >= tweenInfo.duration)
				{
					tweenInfo.TweenValue(1f);
					return false;
				}
				elapsedTime += (tweenInfo.ignoreTimeScale ? Time.unscaledDeltaTime : Time.deltaTime);
				float floatPercentage = Mathf.Clamp01(elapsedTime / tweenInfo.duration);
				tweenInfo.TweenValue(floatPercentage);
				this.<>2__current = null;
				this.<>1__state = 1;
				return true;
			}

			// Token: 0x17000107 RID: 263
			// (get) Token: 0x06000497 RID: 1175 RVA: 0x0002F3BF File Offset: 0x0002D5BF
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000498 RID: 1176 RVA: 0x0002F3C7 File Offset: 0x0002D5C7
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000108 RID: 264
			// (get) Token: 0x06000499 RID: 1177 RVA: 0x0002F3BF File Offset: 0x0002D5BF
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x040003FE RID: 1022
			private int <>1__state;

			// Token: 0x040003FF RID: 1023
			private object <>2__current;

			// Token: 0x04000400 RID: 1024
			public T tweenInfo;

			// Token: 0x04000401 RID: 1025
			private float <elapsedTime>5__2;
		}
	}
}
