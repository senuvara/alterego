﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace TMPro
{
	// Token: 0x0200001C RID: 28
	internal static class TMP_ListPool<T>
	{
		// Token: 0x06000220 RID: 544 RVA: 0x0001C151 File Offset: 0x0001A351
		public static List<T> Get()
		{
			return TMP_ListPool<T>.s_ListPool.Get();
		}

		// Token: 0x06000221 RID: 545 RVA: 0x0001C15D File Offset: 0x0001A35D
		public static void Release(List<T> toRelease)
		{
			TMP_ListPool<T>.s_ListPool.Release(toRelease);
		}

		// Token: 0x06000222 RID: 546 RVA: 0x0001C16A File Offset: 0x0001A36A
		// Note: this type is marked as 'beforefieldinit'.
		static TMP_ListPool()
		{
		}

		// Token: 0x040000F9 RID: 249
		private static readonly TMP_ObjectPool<List<T>> s_ListPool = new TMP_ObjectPool<List<T>>(null, delegate(List<T> l)
		{
			l.Clear();
		});

		// Token: 0x0200007B RID: 123
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x060004D1 RID: 1233 RVA: 0x0002F7E5 File Offset: 0x0002D9E5
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x060004D2 RID: 1234 RVA: 0x000159ED File Offset: 0x00013BED
			public <>c()
			{
			}

			// Token: 0x060004D3 RID: 1235 RVA: 0x0002F7F1 File Offset: 0x0002D9F1
			internal void <.cctor>b__3_0(List<T> l)
			{
				l.Clear();
			}

			// Token: 0x04000444 RID: 1092
			public static readonly TMP_ListPool<T>.<>c <>9 = new TMP_ListPool<T>.<>c();
		}
	}
}
