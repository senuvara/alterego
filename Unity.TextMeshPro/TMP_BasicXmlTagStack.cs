﻿using System;

namespace TMPro
{
	// Token: 0x02000041 RID: 65
	public struct TMP_BasicXmlTagStack
	{
		// Token: 0x0600043B RID: 1083 RVA: 0x0002D1CC File Offset: 0x0002B3CC
		public void Clear()
		{
			this.bold = 0;
			this.italic = 0;
			this.underline = 0;
			this.strikethrough = 0;
			this.highlight = 0;
			this.superscript = 0;
			this.subscript = 0;
			this.uppercase = 0;
			this.lowercase = 0;
			this.smallcaps = 0;
		}

		// Token: 0x0600043C RID: 1084 RVA: 0x0002D220 File Offset: 0x0002B420
		public byte Add(FontStyles style)
		{
			if (style <= FontStyles.Strikethrough)
			{
				switch (style)
				{
				case FontStyles.Bold:
					this.bold += 1;
					return this.bold;
				case FontStyles.Italic:
					this.italic += 1;
					return this.italic;
				case (FontStyles)3:
					break;
				case FontStyles.Underline:
					this.underline += 1;
					return this.underline;
				default:
					if (style == FontStyles.Strikethrough)
					{
						this.strikethrough += 1;
						return this.strikethrough;
					}
					break;
				}
			}
			else
			{
				if (style == FontStyles.Superscript)
				{
					this.superscript += 1;
					return this.superscript;
				}
				if (style == FontStyles.Subscript)
				{
					this.subscript += 1;
					return this.subscript;
				}
				if (style == FontStyles.Highlight)
				{
					this.highlight += 1;
					return this.highlight;
				}
			}
			return 0;
		}

		// Token: 0x0600043D RID: 1085 RVA: 0x0002D310 File Offset: 0x0002B510
		public byte Remove(FontStyles style)
		{
			if (style <= FontStyles.Strikethrough)
			{
				switch (style)
				{
				case FontStyles.Bold:
					if (this.bold > 1)
					{
						this.bold -= 1;
					}
					else
					{
						this.bold = 0;
					}
					return this.bold;
				case FontStyles.Italic:
					if (this.italic > 1)
					{
						this.italic -= 1;
					}
					else
					{
						this.italic = 0;
					}
					return this.italic;
				case (FontStyles)3:
					break;
				case FontStyles.Underline:
					if (this.underline > 1)
					{
						this.underline -= 1;
					}
					else
					{
						this.underline = 0;
					}
					return this.underline;
				default:
					if (style == FontStyles.Strikethrough)
					{
						if (this.strikethrough > 1)
						{
							this.strikethrough -= 1;
						}
						else
						{
							this.strikethrough = 0;
						}
						return this.strikethrough;
					}
					break;
				}
			}
			else
			{
				if (style == FontStyles.Superscript)
				{
					if (this.superscript > 1)
					{
						this.superscript -= 1;
					}
					else
					{
						this.superscript = 0;
					}
					return this.superscript;
				}
				if (style == FontStyles.Subscript)
				{
					if (this.subscript > 1)
					{
						this.subscript -= 1;
					}
					else
					{
						this.subscript = 0;
					}
					return this.subscript;
				}
				if (style == FontStyles.Highlight)
				{
					if (this.highlight > 1)
					{
						this.highlight -= 1;
					}
					else
					{
						this.highlight = 0;
					}
					return this.highlight;
				}
			}
			return 0;
		}

		// Token: 0x040002C6 RID: 710
		public byte bold;

		// Token: 0x040002C7 RID: 711
		public byte italic;

		// Token: 0x040002C8 RID: 712
		public byte underline;

		// Token: 0x040002C9 RID: 713
		public byte strikethrough;

		// Token: 0x040002CA RID: 714
		public byte highlight;

		// Token: 0x040002CB RID: 715
		public byte superscript;

		// Token: 0x040002CC RID: 716
		public byte subscript;

		// Token: 0x040002CD RID: 717
		public byte uppercase;

		// Token: 0x040002CE RID: 718
		public byte lowercase;

		// Token: 0x040002CF RID: 719
		public byte smallcaps;
	}
}
