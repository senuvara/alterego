﻿using System;

namespace TMPro
{
	// Token: 0x02000042 RID: 66
	public struct TMP_XmlTagStack<T>
	{
		// Token: 0x0600043E RID: 1086 RVA: 0x0002D486 File Offset: 0x0002B686
		public TMP_XmlTagStack(T[] tagStack)
		{
			this.itemStack = tagStack;
			this.m_capacity = tagStack.Length;
			this.index = 0;
			this.m_defaultItem = default(T);
		}

		// Token: 0x0600043F RID: 1087 RVA: 0x0002D4AB File Offset: 0x0002B6AB
		public void Clear()
		{
			this.index = 0;
		}

		// Token: 0x06000440 RID: 1088 RVA: 0x0002D4B4 File Offset: 0x0002B6B4
		public void SetDefault(T item)
		{
			this.itemStack[0] = item;
			this.index = 1;
		}

		// Token: 0x06000441 RID: 1089 RVA: 0x0002D4CA File Offset: 0x0002B6CA
		public void Add(T item)
		{
			if (this.index < this.itemStack.Length)
			{
				this.itemStack[this.index] = item;
				this.index++;
			}
		}

		// Token: 0x06000442 RID: 1090 RVA: 0x0002D4FC File Offset: 0x0002B6FC
		public T Remove()
		{
			this.index--;
			if (this.index <= 0)
			{
				this.index = 1;
				return this.itemStack[0];
			}
			return this.itemStack[this.index - 1];
		}

		// Token: 0x06000443 RID: 1091 RVA: 0x0002D53C File Offset: 0x0002B73C
		public void Push(T item)
		{
			if (this.index == this.m_capacity)
			{
				this.m_capacity *= 2;
				if (this.m_capacity == 0)
				{
					this.m_capacity = 4;
				}
				Array.Resize<T>(ref this.itemStack, this.m_capacity);
			}
			this.itemStack[this.index] = item;
			this.index++;
		}

		// Token: 0x06000444 RID: 1092 RVA: 0x0002D5A8 File Offset: 0x0002B7A8
		public T Pop()
		{
			if (this.index == 0)
			{
				return default(T);
			}
			this.index--;
			T result = this.itemStack[this.index];
			this.itemStack[this.index] = this.m_defaultItem;
			return result;
		}

		// Token: 0x06000445 RID: 1093 RVA: 0x0002D5FD File Offset: 0x0002B7FD
		public T CurrentItem()
		{
			if (this.index > 0)
			{
				return this.itemStack[this.index - 1];
			}
			return this.itemStack[0];
		}

		// Token: 0x06000446 RID: 1094 RVA: 0x0002D628 File Offset: 0x0002B828
		public T PreviousItem()
		{
			if (this.index > 1)
			{
				return this.itemStack[this.index - 2];
			}
			return this.itemStack[0];
		}

		// Token: 0x040002D0 RID: 720
		public T[] itemStack;

		// Token: 0x040002D1 RID: 721
		public int index;

		// Token: 0x040002D2 RID: 722
		private int m_capacity;

		// Token: 0x040002D3 RID: 723
		private T m_defaultItem;

		// Token: 0x040002D4 RID: 724
		private const int k_defaultCapacity = 4;
	}
}
