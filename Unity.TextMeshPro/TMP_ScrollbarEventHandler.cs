﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TMPro
{
	// Token: 0x02000021 RID: 33
	public class TMP_ScrollbarEventHandler : MonoBehaviour, IPointerClickHandler, IEventSystemHandler, ISelectHandler, IDeselectHandler
	{
		// Token: 0x0600024B RID: 587 RVA: 0x0001DF6D File Offset: 0x0001C16D
		public void OnPointerClick(PointerEventData eventData)
		{
			Debug.Log("Scrollbar click...");
		}

		// Token: 0x0600024C RID: 588 RVA: 0x0001DF79 File Offset: 0x0001C179
		public void OnSelect(BaseEventData eventData)
		{
			Debug.Log("Scrollbar selected");
			this.isSelected = true;
		}

		// Token: 0x0600024D RID: 589 RVA: 0x0001DF8C File Offset: 0x0001C18C
		public void OnDeselect(BaseEventData eventData)
		{
			Debug.Log("Scrollbar De-Selected");
			this.isSelected = false;
		}

		// Token: 0x0600024E RID: 590 RVA: 0x0001DF9F File Offset: 0x0001C19F
		public TMP_ScrollbarEventHandler()
		{
		}

		// Token: 0x04000112 RID: 274
		public bool isSelected;
	}
}
