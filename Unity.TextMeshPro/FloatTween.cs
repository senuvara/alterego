﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace TMPro
{
	// Token: 0x02000012 RID: 18
	internal struct FloatTween : ITweenValue
	{
		// Token: 0x17000026 RID: 38
		// (get) Token: 0x060000F7 RID: 247 RVA: 0x00015899 File Offset: 0x00013A99
		// (set) Token: 0x060000F8 RID: 248 RVA: 0x000158A1 File Offset: 0x00013AA1
		public float startValue
		{
			get
			{
				return this.m_StartValue;
			}
			set
			{
				this.m_StartValue = value;
			}
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x060000F9 RID: 249 RVA: 0x000158AA File Offset: 0x00013AAA
		// (set) Token: 0x060000FA RID: 250 RVA: 0x000158B2 File Offset: 0x00013AB2
		public float targetValue
		{
			get
			{
				return this.m_TargetValue;
			}
			set
			{
				this.m_TargetValue = value;
			}
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x060000FB RID: 251 RVA: 0x000158BB File Offset: 0x00013ABB
		// (set) Token: 0x060000FC RID: 252 RVA: 0x000158C3 File Offset: 0x00013AC3
		public float duration
		{
			get
			{
				return this.m_Duration;
			}
			set
			{
				this.m_Duration = value;
			}
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x060000FD RID: 253 RVA: 0x000158CC File Offset: 0x00013ACC
		// (set) Token: 0x060000FE RID: 254 RVA: 0x000158D4 File Offset: 0x00013AD4
		public bool ignoreTimeScale
		{
			get
			{
				return this.m_IgnoreTimeScale;
			}
			set
			{
				this.m_IgnoreTimeScale = value;
			}
		}

		// Token: 0x060000FF RID: 255 RVA: 0x000158E0 File Offset: 0x00013AE0
		public void TweenValue(float floatPercentage)
		{
			if (!this.ValidTarget())
			{
				return;
			}
			float arg = Mathf.Lerp(this.m_StartValue, this.m_TargetValue, floatPercentage);
			this.m_Target.Invoke(arg);
		}

		// Token: 0x06000100 RID: 256 RVA: 0x00015915 File Offset: 0x00013B15
		public void AddOnChangedCallback(UnityAction<float> callback)
		{
			if (this.m_Target == null)
			{
				this.m_Target = new FloatTween.FloatTweenCallback();
			}
			this.m_Target.AddListener(callback);
		}

		// Token: 0x06000101 RID: 257 RVA: 0x000158CC File Offset: 0x00013ACC
		public bool GetIgnoreTimescale()
		{
			return this.m_IgnoreTimeScale;
		}

		// Token: 0x06000102 RID: 258 RVA: 0x000158BB File Offset: 0x00013ABB
		public float GetDuration()
		{
			return this.m_Duration;
		}

		// Token: 0x06000103 RID: 259 RVA: 0x00015936 File Offset: 0x00013B36
		public bool ValidTarget()
		{
			return this.m_Target != null;
		}

		// Token: 0x04000067 RID: 103
		private FloatTween.FloatTweenCallback m_Target;

		// Token: 0x04000068 RID: 104
		private float m_StartValue;

		// Token: 0x04000069 RID: 105
		private float m_TargetValue;

		// Token: 0x0400006A RID: 106
		private float m_Duration;

		// Token: 0x0400006B RID: 107
		private bool m_IgnoreTimeScale;

		// Token: 0x02000064 RID: 100
		public class FloatTweenCallback : UnityEvent<float>
		{
			// Token: 0x06000493 RID: 1171 RVA: 0x0002F2B6 File Offset: 0x0002D4B6
			public FloatTweenCallback()
			{
			}
		}
	}
}
