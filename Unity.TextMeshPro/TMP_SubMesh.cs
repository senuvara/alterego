﻿using System;
using UnityEngine;

namespace TMPro
{
	// Token: 0x02000029 RID: 41
	[ExecuteInEditMode]
	[RequireComponent(typeof(MeshRenderer))]
	[RequireComponent(typeof(MeshFilter))]
	public class TMP_SubMesh : MonoBehaviour
	{
		// Token: 0x17000093 RID: 147
		// (get) Token: 0x0600029E RID: 670 RVA: 0x0001E9F8 File Offset: 0x0001CBF8
		// (set) Token: 0x0600029F RID: 671 RVA: 0x0001EA00 File Offset: 0x0001CC00
		public TMP_FontAsset fontAsset
		{
			get
			{
				return this.m_fontAsset;
			}
			set
			{
				this.m_fontAsset = value;
			}
		}

		// Token: 0x17000094 RID: 148
		// (get) Token: 0x060002A0 RID: 672 RVA: 0x0001EA09 File Offset: 0x0001CC09
		// (set) Token: 0x060002A1 RID: 673 RVA: 0x0001EA11 File Offset: 0x0001CC11
		public TMP_SpriteAsset spriteAsset
		{
			get
			{
				return this.m_spriteAsset;
			}
			set
			{
				this.m_spriteAsset = value;
			}
		}

		// Token: 0x17000095 RID: 149
		// (get) Token: 0x060002A2 RID: 674 RVA: 0x0001EA1A File Offset: 0x0001CC1A
		// (set) Token: 0x060002A3 RID: 675 RVA: 0x0001EA28 File Offset: 0x0001CC28
		public Material material
		{
			get
			{
				return this.GetMaterial(this.m_sharedMaterial);
			}
			set
			{
				if (this.m_sharedMaterial.GetInstanceID() == value.GetInstanceID())
				{
					return;
				}
				this.m_material = value;
				this.m_sharedMaterial = value;
				this.m_padding = this.GetPaddingForMaterial();
				this.SetVerticesDirty();
				this.SetMaterialDirty();
			}
		}

		// Token: 0x17000096 RID: 150
		// (get) Token: 0x060002A4 RID: 676 RVA: 0x0001EA71 File Offset: 0x0001CC71
		// (set) Token: 0x060002A5 RID: 677 RVA: 0x0001EA79 File Offset: 0x0001CC79
		public Material sharedMaterial
		{
			get
			{
				return this.m_sharedMaterial;
			}
			set
			{
				this.SetSharedMaterial(value);
			}
		}

		// Token: 0x17000097 RID: 151
		// (get) Token: 0x060002A6 RID: 678 RVA: 0x0001EA82 File Offset: 0x0001CC82
		// (set) Token: 0x060002A7 RID: 679 RVA: 0x0001EA8C File Offset: 0x0001CC8C
		public Material fallbackMaterial
		{
			get
			{
				return this.m_fallbackMaterial;
			}
			set
			{
				if (this.m_fallbackMaterial == value)
				{
					return;
				}
				if (this.m_fallbackMaterial != null && this.m_fallbackMaterial != value)
				{
					TMP_MaterialManager.ReleaseFallbackMaterial(this.m_fallbackMaterial);
				}
				this.m_fallbackMaterial = value;
				TMP_MaterialManager.AddFallbackMaterialReference(this.m_fallbackMaterial);
				this.SetSharedMaterial(this.m_fallbackMaterial);
			}
		}

		// Token: 0x17000098 RID: 152
		// (get) Token: 0x060002A8 RID: 680 RVA: 0x0001EAED File Offset: 0x0001CCED
		// (set) Token: 0x060002A9 RID: 681 RVA: 0x0001EAF5 File Offset: 0x0001CCF5
		public Material fallbackSourceMaterial
		{
			get
			{
				return this.m_fallbackSourceMaterial;
			}
			set
			{
				this.m_fallbackSourceMaterial = value;
			}
		}

		// Token: 0x17000099 RID: 153
		// (get) Token: 0x060002AA RID: 682 RVA: 0x0001EAFE File Offset: 0x0001CCFE
		// (set) Token: 0x060002AB RID: 683 RVA: 0x0001EB06 File Offset: 0x0001CD06
		public bool isDefaultMaterial
		{
			get
			{
				return this.m_isDefaultMaterial;
			}
			set
			{
				this.m_isDefaultMaterial = value;
			}
		}

		// Token: 0x1700009A RID: 154
		// (get) Token: 0x060002AC RID: 684 RVA: 0x0001EB0F File Offset: 0x0001CD0F
		// (set) Token: 0x060002AD RID: 685 RVA: 0x0001EB17 File Offset: 0x0001CD17
		public float padding
		{
			get
			{
				return this.m_padding;
			}
			set
			{
				this.m_padding = value;
			}
		}

		// Token: 0x1700009B RID: 155
		// (get) Token: 0x060002AE RID: 686 RVA: 0x0001EB20 File Offset: 0x0001CD20
		public Renderer renderer
		{
			get
			{
				if (this.m_renderer == null)
				{
					this.m_renderer = base.GetComponent<Renderer>();
				}
				return this.m_renderer;
			}
		}

		// Token: 0x1700009C RID: 156
		// (get) Token: 0x060002AF RID: 687 RVA: 0x0001EB42 File Offset: 0x0001CD42
		public MeshFilter meshFilter
		{
			get
			{
				if (this.m_meshFilter == null)
				{
					this.m_meshFilter = base.GetComponent<MeshFilter>();
				}
				return this.m_meshFilter;
			}
		}

		// Token: 0x1700009D RID: 157
		// (get) Token: 0x060002B0 RID: 688 RVA: 0x0001EB64 File Offset: 0x0001CD64
		// (set) Token: 0x060002B1 RID: 689 RVA: 0x0001EBA3 File Offset: 0x0001CDA3
		public Mesh mesh
		{
			get
			{
				if (this.m_mesh == null)
				{
					this.m_mesh = new Mesh();
					this.m_mesh.hideFlags = HideFlags.HideAndDontSave;
					this.meshFilter.mesh = this.m_mesh;
				}
				return this.m_mesh;
			}
			set
			{
				this.m_mesh = value;
			}
		}

		// Token: 0x060002B2 RID: 690 RVA: 0x0001EBAC File Offset: 0x0001CDAC
		private void OnEnable()
		{
			if (!this.m_isRegisteredForEvents)
			{
				this.m_isRegisteredForEvents = true;
			}
			this.meshFilter.sharedMesh = this.mesh;
			if (this.m_sharedMaterial != null)
			{
				this.m_sharedMaterial.SetVector(ShaderUtilities.ID_ClipRect, new Vector4(-32767f, -32767f, 32767f, 32767f));
			}
		}

		// Token: 0x060002B3 RID: 691 RVA: 0x0001EC10 File Offset: 0x0001CE10
		private void OnDisable()
		{
			this.m_meshFilter.sharedMesh = null;
			if (this.m_fallbackMaterial != null)
			{
				TMP_MaterialManager.ReleaseFallbackMaterial(this.m_fallbackMaterial);
				this.m_fallbackMaterial = null;
			}
		}

		// Token: 0x060002B4 RID: 692 RVA: 0x0001EC40 File Offset: 0x0001CE40
		private void OnDestroy()
		{
			if (this.m_mesh != null)
			{
				Object.DestroyImmediate(this.m_mesh);
			}
			if (this.m_fallbackMaterial != null)
			{
				TMP_MaterialManager.ReleaseFallbackMaterial(this.m_fallbackMaterial);
				this.m_fallbackMaterial = null;
			}
			this.m_isRegisteredForEvents = false;
		}

		// Token: 0x060002B5 RID: 693 RVA: 0x0001EC90 File Offset: 0x0001CE90
		public static TMP_SubMesh AddSubTextObject(TextMeshPro textComponent, MaterialReference materialReference)
		{
			GameObject gameObject = new GameObject("TMP SubMesh [" + materialReference.material.name + "]", new Type[]
			{
				typeof(TMP_SubMesh)
			});
			TMP_SubMesh component = gameObject.GetComponent<TMP_SubMesh>();
			gameObject.transform.SetParent(textComponent.transform, false);
			gameObject.transform.localPosition = Vector3.zero;
			gameObject.transform.localRotation = Quaternion.identity;
			gameObject.transform.localScale = Vector3.one;
			gameObject.layer = textComponent.gameObject.layer;
			component.m_meshFilter = gameObject.GetComponent<MeshFilter>();
			component.m_TextComponent = textComponent;
			component.m_fontAsset = materialReference.fontAsset;
			component.m_spriteAsset = materialReference.spriteAsset;
			component.m_isDefaultMaterial = materialReference.isDefaultMaterial;
			component.SetSharedMaterial(materialReference.material);
			component.renderer.sortingLayerID = textComponent.renderer.sortingLayerID;
			component.renderer.sortingOrder = textComponent.renderer.sortingOrder;
			return component;
		}

		// Token: 0x060002B6 RID: 694 RVA: 0x0001ED98 File Offset: 0x0001CF98
		public void DestroySelf()
		{
			Object.Destroy(base.gameObject, 1f);
		}

		// Token: 0x060002B7 RID: 695 RVA: 0x0001EDAC File Offset: 0x0001CFAC
		private Material GetMaterial(Material mat)
		{
			if (this.m_renderer == null)
			{
				this.m_renderer = base.GetComponent<Renderer>();
			}
			if (this.m_material == null || this.m_material.GetInstanceID() != mat.GetInstanceID())
			{
				this.m_material = this.CreateMaterialInstance(mat);
			}
			this.m_sharedMaterial = this.m_material;
			this.m_padding = this.GetPaddingForMaterial();
			this.SetVerticesDirty();
			this.SetMaterialDirty();
			return this.m_sharedMaterial;
		}

		// Token: 0x060002B8 RID: 696 RVA: 0x0001EE2B File Offset: 0x0001D02B
		private Material CreateMaterialInstance(Material source)
		{
			Material material = new Material(source);
			material.shaderKeywords = source.shaderKeywords;
			material.name += " (Instance)";
			return material;
		}

		// Token: 0x060002B9 RID: 697 RVA: 0x0001EE55 File Offset: 0x0001D055
		private Material GetSharedMaterial()
		{
			if (this.m_renderer == null)
			{
				this.m_renderer = base.GetComponent<Renderer>();
			}
			return this.m_renderer.sharedMaterial;
		}

		// Token: 0x060002BA RID: 698 RVA: 0x0001EE7C File Offset: 0x0001D07C
		private void SetSharedMaterial(Material mat)
		{
			this.m_sharedMaterial = mat;
			this.m_padding = this.GetPaddingForMaterial();
			this.SetMaterialDirty();
		}

		// Token: 0x060002BB RID: 699 RVA: 0x0001EE97 File Offset: 0x0001D097
		public float GetPaddingForMaterial()
		{
			return ShaderUtilities.GetPadding(this.m_sharedMaterial, this.m_TextComponent.extraPadding, this.m_TextComponent.isUsingBold);
		}

		// Token: 0x060002BC RID: 700 RVA: 0x0001EEBA File Offset: 0x0001D0BA
		public void UpdateMeshPadding(bool isExtraPadding, bool isUsingBold)
		{
			this.m_padding = ShaderUtilities.GetPadding(this.m_sharedMaterial, isExtraPadding, isUsingBold);
		}

		// Token: 0x060002BD RID: 701 RVA: 0x0001EECF File Offset: 0x0001D0CF
		public void SetVerticesDirty()
		{
			if (!base.enabled)
			{
				return;
			}
			if (this.m_TextComponent != null)
			{
				this.m_TextComponent.havePropertiesChanged = true;
				this.m_TextComponent.SetVerticesDirty();
			}
		}

		// Token: 0x060002BE RID: 702 RVA: 0x0001EEFF File Offset: 0x0001D0FF
		public void SetMaterialDirty()
		{
			this.UpdateMaterial();
		}

		// Token: 0x060002BF RID: 703 RVA: 0x0001EF07 File Offset: 0x0001D107
		protected void UpdateMaterial()
		{
			if (this.m_renderer == null)
			{
				this.m_renderer = this.renderer;
			}
			this.m_renderer.sharedMaterial = this.m_sharedMaterial;
		}

		// Token: 0x060002C0 RID: 704 RVA: 0x0001DF9F File Offset: 0x0001C19F
		public TMP_SubMesh()
		{
		}

		// Token: 0x04000144 RID: 324
		[SerializeField]
		private TMP_FontAsset m_fontAsset;

		// Token: 0x04000145 RID: 325
		[SerializeField]
		private TMP_SpriteAsset m_spriteAsset;

		// Token: 0x04000146 RID: 326
		[SerializeField]
		private Material m_material;

		// Token: 0x04000147 RID: 327
		[SerializeField]
		private Material m_sharedMaterial;

		// Token: 0x04000148 RID: 328
		private Material m_fallbackMaterial;

		// Token: 0x04000149 RID: 329
		private Material m_fallbackSourceMaterial;

		// Token: 0x0400014A RID: 330
		[SerializeField]
		private bool m_isDefaultMaterial;

		// Token: 0x0400014B RID: 331
		[SerializeField]
		private float m_padding;

		// Token: 0x0400014C RID: 332
		[SerializeField]
		private Renderer m_renderer;

		// Token: 0x0400014D RID: 333
		[SerializeField]
		private MeshFilter m_meshFilter;

		// Token: 0x0400014E RID: 334
		private Mesh m_mesh;

		// Token: 0x0400014F RID: 335
		[SerializeField]
		private TextMeshPro m_TextComponent;

		// Token: 0x04000150 RID: 336
		[NonSerialized]
		private bool m_isRegisteredForEvents;
	}
}
