﻿using System;
using UnityEngine;

namespace TMPro
{
	// Token: 0x02000052 RID: 82
	public struct TMP_Vertex
	{
		// Token: 0x0400034E RID: 846
		public Vector3 position;

		// Token: 0x0400034F RID: 847
		public Vector2 uv;

		// Token: 0x04000350 RID: 848
		public Vector2 uv2;

		// Token: 0x04000351 RID: 849
		public Vector2 uv4;

		// Token: 0x04000352 RID: 850
		public Color32 color;
	}
}
