﻿using System;
using UnityEngine;

namespace TMPro
{
	// Token: 0x02000059 RID: 89
	[Serializable]
	public struct Mesh_Extents
	{
		// Token: 0x06000488 RID: 1160 RVA: 0x0002E371 File Offset: 0x0002C571
		public Mesh_Extents(Vector2 min, Vector2 max)
		{
			this.min = min;
			this.max = max;
		}

		// Token: 0x06000489 RID: 1161 RVA: 0x0002E384 File Offset: 0x0002C584
		public override string ToString()
		{
			return string.Concat(new string[]
			{
				"Min (",
				this.min.x.ToString("f2"),
				", ",
				this.min.y.ToString("f2"),
				")   Max (",
				this.max.x.ToString("f2"),
				", ",
				this.max.y.ToString("f2"),
				")"
			});
		}

		// Token: 0x0400036C RID: 876
		public Vector2 min;

		// Token: 0x0400036D RID: 877
		public Vector2 max;
	}
}
