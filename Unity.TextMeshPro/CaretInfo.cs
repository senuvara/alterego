﻿using System;

namespace TMPro
{
	// Token: 0x0200003D RID: 61
	public struct CaretInfo
	{
		// Token: 0x0600040C RID: 1036 RVA: 0x0002B4CF File Offset: 0x000296CF
		public CaretInfo(int index, CaretPosition position)
		{
			this.index = index;
			this.position = position;
		}

		// Token: 0x040002B7 RID: 695
		public int index;

		// Token: 0x040002B8 RID: 696
		public CaretPosition position;
	}
}
