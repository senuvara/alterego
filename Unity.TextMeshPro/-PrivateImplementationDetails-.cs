﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Token: 0x02000060 RID: 96
[CompilerGenerated]
internal sealed class <PrivateImplementationDetails>
{
	// Token: 0x040003EC RID: 1004 RVA: 0x00049884 File Offset: 0x00047A84
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=12 7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46;

	// Token: 0x040003ED RID: 1005 RVA: 0x00049894 File Offset: 0x00047A94
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=40 9E6378168821DBABB7EE3D0154346480FAC8AEF1;

	// Token: 0x0200008E RID: 142
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 12)]
	private struct __StaticArrayInitTypeSize=12
	{
	}

	// Token: 0x0200008F RID: 143
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 40)]
	private struct __StaticArrayInitTypeSize=40
	{
	}
}
