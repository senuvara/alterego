﻿using System;

namespace TMPro
{
	// Token: 0x02000010 RID: 16
	internal interface ITweenValue
	{
		// Token: 0x060000E4 RID: 228
		void TweenValue(float floatPercentage);

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x060000E5 RID: 229
		bool ignoreTimeScale { get; }

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x060000E6 RID: 230
		float duration { get; }

		// Token: 0x060000E7 RID: 231
		bool ValidTarget();
	}
}
