﻿using System;

namespace TMPro
{
	// Token: 0x02000054 RID: 84
	public struct TMP_PageInfo
	{
		// Token: 0x04000357 RID: 855
		public int firstCharacterIndex;

		// Token: 0x04000358 RID: 856
		public int lastCharacterIndex;

		// Token: 0x04000359 RID: 857
		public float ascender;

		// Token: 0x0400035A RID: 858
		public float baseLine;

		// Token: 0x0400035B RID: 859
		public float descender;
	}
}
