﻿using System;

namespace TMPro
{
	// Token: 0x02000016 RID: 22
	[Serializable]
	public struct TMP_FontWeights
	{
		// Token: 0x04000083 RID: 131
		public TMP_FontAsset regularTypeface;

		// Token: 0x04000084 RID: 132
		public TMP_FontAsset italicTypeface;
	}
}
