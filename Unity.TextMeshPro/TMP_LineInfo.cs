﻿using System;

namespace TMPro
{
	// Token: 0x0200001B RID: 27
	public struct TMP_LineInfo
	{
		// Token: 0x040000E5 RID: 229
		internal int controlCharacterCount;

		// Token: 0x040000E6 RID: 230
		public int characterCount;

		// Token: 0x040000E7 RID: 231
		public int visibleCharacterCount;

		// Token: 0x040000E8 RID: 232
		public int spaceCount;

		// Token: 0x040000E9 RID: 233
		public int wordCount;

		// Token: 0x040000EA RID: 234
		public int firstCharacterIndex;

		// Token: 0x040000EB RID: 235
		public int firstVisibleCharacterIndex;

		// Token: 0x040000EC RID: 236
		public int lastCharacterIndex;

		// Token: 0x040000ED RID: 237
		public int lastVisibleCharacterIndex;

		// Token: 0x040000EE RID: 238
		public float length;

		// Token: 0x040000EF RID: 239
		public float lineHeight;

		// Token: 0x040000F0 RID: 240
		public float ascender;

		// Token: 0x040000F1 RID: 241
		public float baseline;

		// Token: 0x040000F2 RID: 242
		public float descender;

		// Token: 0x040000F3 RID: 243
		public float maxAdvance;

		// Token: 0x040000F4 RID: 244
		public float width;

		// Token: 0x040000F5 RID: 245
		public float marginLeft;

		// Token: 0x040000F6 RID: 246
		public float marginRight;

		// Token: 0x040000F7 RID: 247
		public TextAlignmentOptions alignment;

		// Token: 0x040000F8 RID: 248
		public Extents lineExtents;
	}
}
