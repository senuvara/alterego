﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TMPro
{
	// Token: 0x02000023 RID: 35
	[ExecuteInEditMode]
	[Serializable]
	public class TMP_Settings : ScriptableObject
	{
		// Token: 0x17000070 RID: 112
		// (get) Token: 0x06000251 RID: 593 RVA: 0x0001DFAF File Offset: 0x0001C1AF
		public static string version
		{
			get
			{
				return "1.3.0";
			}
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x06000252 RID: 594 RVA: 0x0001DFB6 File Offset: 0x0001C1B6
		public static bool enableWordWrapping
		{
			get
			{
				return TMP_Settings.instance.m_enableWordWrapping;
			}
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x06000253 RID: 595 RVA: 0x0001DFC2 File Offset: 0x0001C1C2
		public static bool enableKerning
		{
			get
			{
				return TMP_Settings.instance.m_enableKerning;
			}
		}

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x06000254 RID: 596 RVA: 0x0001DFCE File Offset: 0x0001C1CE
		public static bool enableExtraPadding
		{
			get
			{
				return TMP_Settings.instance.m_enableExtraPadding;
			}
		}

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x06000255 RID: 597 RVA: 0x0001DFDA File Offset: 0x0001C1DA
		public static bool enableTintAllSprites
		{
			get
			{
				return TMP_Settings.instance.m_enableTintAllSprites;
			}
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x06000256 RID: 598 RVA: 0x0001DFE6 File Offset: 0x0001C1E6
		public static bool enableParseEscapeCharacters
		{
			get
			{
				return TMP_Settings.instance.m_enableParseEscapeCharacters;
			}
		}

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x06000257 RID: 599 RVA: 0x0001DFF2 File Offset: 0x0001C1F2
		public static int missingGlyphCharacter
		{
			get
			{
				return TMP_Settings.instance.m_missingGlyphCharacter;
			}
		}

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x06000258 RID: 600 RVA: 0x0001DFFE File Offset: 0x0001C1FE
		public static bool warningsDisabled
		{
			get
			{
				return TMP_Settings.instance.m_warningsDisabled;
			}
		}

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x06000259 RID: 601 RVA: 0x0001E00A File Offset: 0x0001C20A
		public static TMP_FontAsset defaultFontAsset
		{
			get
			{
				return TMP_Settings.instance.m_defaultFontAsset;
			}
		}

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x0600025A RID: 602 RVA: 0x0001E016 File Offset: 0x0001C216
		public static string defaultFontAssetPath
		{
			get
			{
				return TMP_Settings.instance.m_defaultFontAssetPath;
			}
		}

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x0600025B RID: 603 RVA: 0x0001E022 File Offset: 0x0001C222
		public static float defaultFontSize
		{
			get
			{
				return TMP_Settings.instance.m_defaultFontSize;
			}
		}

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x0600025C RID: 604 RVA: 0x0001E02E File Offset: 0x0001C22E
		public static float defaultTextAutoSizingMinRatio
		{
			get
			{
				return TMP_Settings.instance.m_defaultAutoSizeMinRatio;
			}
		}

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x0600025D RID: 605 RVA: 0x0001E03A File Offset: 0x0001C23A
		public static float defaultTextAutoSizingMaxRatio
		{
			get
			{
				return TMP_Settings.instance.m_defaultAutoSizeMaxRatio;
			}
		}

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x0600025E RID: 606 RVA: 0x0001E046 File Offset: 0x0001C246
		public static Vector2 defaultTextMeshProTextContainerSize
		{
			get
			{
				return TMP_Settings.instance.m_defaultTextMeshProTextContainerSize;
			}
		}

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x0600025F RID: 607 RVA: 0x0001E052 File Offset: 0x0001C252
		public static Vector2 defaultTextMeshProUITextContainerSize
		{
			get
			{
				return TMP_Settings.instance.m_defaultTextMeshProUITextContainerSize;
			}
		}

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x06000260 RID: 608 RVA: 0x0001E05E File Offset: 0x0001C25E
		public static bool autoSizeTextContainer
		{
			get
			{
				return TMP_Settings.instance.m_autoSizeTextContainer;
			}
		}

		// Token: 0x17000080 RID: 128
		// (get) Token: 0x06000261 RID: 609 RVA: 0x0001E06A File Offset: 0x0001C26A
		public static List<TMP_FontAsset> fallbackFontAssets
		{
			get
			{
				return TMP_Settings.instance.m_fallbackFontAssets;
			}
		}

		// Token: 0x17000081 RID: 129
		// (get) Token: 0x06000262 RID: 610 RVA: 0x0001E076 File Offset: 0x0001C276
		public static bool matchMaterialPreset
		{
			get
			{
				return TMP_Settings.instance.m_matchMaterialPreset;
			}
		}

		// Token: 0x17000082 RID: 130
		// (get) Token: 0x06000263 RID: 611 RVA: 0x0001E082 File Offset: 0x0001C282
		public static TMP_SpriteAsset defaultSpriteAsset
		{
			get
			{
				return TMP_Settings.instance.m_defaultSpriteAsset;
			}
		}

		// Token: 0x17000083 RID: 131
		// (get) Token: 0x06000264 RID: 612 RVA: 0x0001E08E File Offset: 0x0001C28E
		public static string defaultSpriteAssetPath
		{
			get
			{
				return TMP_Settings.instance.m_defaultSpriteAssetPath;
			}
		}

		// Token: 0x17000084 RID: 132
		// (get) Token: 0x06000265 RID: 613 RVA: 0x0001E09A File Offset: 0x0001C29A
		public static string defaultColorGradientPresetsPath
		{
			get
			{
				return TMP_Settings.instance.m_defaultColorGradientPresetsPath;
			}
		}

		// Token: 0x17000085 RID: 133
		// (get) Token: 0x06000266 RID: 614 RVA: 0x0001E0A6 File Offset: 0x0001C2A6
		// (set) Token: 0x06000267 RID: 615 RVA: 0x0001E0B2 File Offset: 0x0001C2B2
		public static bool enableEmojiSupport
		{
			get
			{
				return TMP_Settings.instance.m_enableEmojiSupport;
			}
			set
			{
				TMP_Settings.instance.m_enableEmojiSupport = value;
			}
		}

		// Token: 0x17000086 RID: 134
		// (get) Token: 0x06000268 RID: 616 RVA: 0x0001E0BF File Offset: 0x0001C2BF
		public static TMP_StyleSheet defaultStyleSheet
		{
			get
			{
				return TMP_Settings.instance.m_defaultStyleSheet;
			}
		}

		// Token: 0x17000087 RID: 135
		// (get) Token: 0x06000269 RID: 617 RVA: 0x0001E0CB File Offset: 0x0001C2CB
		public static TextAsset leadingCharacters
		{
			get
			{
				return TMP_Settings.instance.m_leadingCharacters;
			}
		}

		// Token: 0x17000088 RID: 136
		// (get) Token: 0x0600026A RID: 618 RVA: 0x0001E0D7 File Offset: 0x0001C2D7
		public static TextAsset followingCharacters
		{
			get
			{
				return TMP_Settings.instance.m_followingCharacters;
			}
		}

		// Token: 0x17000089 RID: 137
		// (get) Token: 0x0600026B RID: 619 RVA: 0x0001E0E3 File Offset: 0x0001C2E3
		public static TMP_Settings.LineBreakingTable linebreakingRules
		{
			get
			{
				if (TMP_Settings.instance.m_linebreakingRules == null)
				{
					TMP_Settings.LoadLinebreakingRules();
				}
				return TMP_Settings.instance.m_linebreakingRules;
			}
		}

		// Token: 0x1700008A RID: 138
		// (get) Token: 0x0600026C RID: 620 RVA: 0x0001E100 File Offset: 0x0001C300
		public static TMP_Settings instance
		{
			get
			{
				if (TMP_Settings.s_Instance == null)
				{
					TMP_Settings.s_Instance = Resources.Load<TMP_Settings>("TMP Settings");
				}
				return TMP_Settings.s_Instance;
			}
		}

		// Token: 0x0600026D RID: 621 RVA: 0x0001E124 File Offset: 0x0001C324
		public static TMP_Settings LoadDefaultSettings()
		{
			if (TMP_Settings.s_Instance == null)
			{
				TMP_Settings x = Resources.Load<TMP_Settings>("TMP Settings");
				if (x != null)
				{
					TMP_Settings.s_Instance = x;
				}
			}
			return TMP_Settings.s_Instance;
		}

		// Token: 0x0600026E RID: 622 RVA: 0x0001E15D File Offset: 0x0001C35D
		public static TMP_Settings GetSettings()
		{
			if (TMP_Settings.instance == null)
			{
				return null;
			}
			return TMP_Settings.instance;
		}

		// Token: 0x0600026F RID: 623 RVA: 0x0001E173 File Offset: 0x0001C373
		public static TMP_FontAsset GetFontAsset()
		{
			if (TMP_Settings.instance == null)
			{
				return null;
			}
			return TMP_Settings.instance.m_defaultFontAsset;
		}

		// Token: 0x06000270 RID: 624 RVA: 0x0001E18E File Offset: 0x0001C38E
		public static TMP_SpriteAsset GetSpriteAsset()
		{
			if (TMP_Settings.instance == null)
			{
				return null;
			}
			return TMP_Settings.instance.m_defaultSpriteAsset;
		}

		// Token: 0x06000271 RID: 625 RVA: 0x0001E1A9 File Offset: 0x0001C3A9
		public static TMP_StyleSheet GetStyleSheet()
		{
			if (TMP_Settings.instance == null)
			{
				return null;
			}
			return TMP_Settings.instance.m_defaultStyleSheet;
		}

		// Token: 0x06000272 RID: 626 RVA: 0x0001E1C4 File Offset: 0x0001C3C4
		public static void LoadLinebreakingRules()
		{
			if (TMP_Settings.instance == null)
			{
				return;
			}
			if (TMP_Settings.s_Instance.m_linebreakingRules == null)
			{
				TMP_Settings.s_Instance.m_linebreakingRules = new TMP_Settings.LineBreakingTable();
			}
			TMP_Settings.s_Instance.m_linebreakingRules.leadingCharacters = TMP_Settings.GetCharacters(TMP_Settings.s_Instance.m_leadingCharacters);
			TMP_Settings.s_Instance.m_linebreakingRules.followingCharacters = TMP_Settings.GetCharacters(TMP_Settings.s_Instance.m_followingCharacters);
		}

		// Token: 0x06000273 RID: 627 RVA: 0x0001E238 File Offset: 0x0001C438
		private static Dictionary<int, char> GetCharacters(TextAsset file)
		{
			Dictionary<int, char> dictionary = new Dictionary<int, char>();
			foreach (char c in file.text)
			{
				if (!dictionary.ContainsKey((int)c))
				{
					dictionary.Add((int)c, c);
				}
			}
			return dictionary;
		}

		// Token: 0x06000274 RID: 628 RVA: 0x000155AE File Offset: 0x000137AE
		public TMP_Settings()
		{
		}

		// Token: 0x04000113 RID: 275
		private static TMP_Settings s_Instance;

		// Token: 0x04000114 RID: 276
		[SerializeField]
		private bool m_enableWordWrapping;

		// Token: 0x04000115 RID: 277
		[SerializeField]
		private bool m_enableKerning;

		// Token: 0x04000116 RID: 278
		[SerializeField]
		private bool m_enableExtraPadding;

		// Token: 0x04000117 RID: 279
		[SerializeField]
		private bool m_enableTintAllSprites;

		// Token: 0x04000118 RID: 280
		[SerializeField]
		private bool m_enableParseEscapeCharacters;

		// Token: 0x04000119 RID: 281
		[SerializeField]
		private int m_missingGlyphCharacter;

		// Token: 0x0400011A RID: 282
		[SerializeField]
		private bool m_warningsDisabled;

		// Token: 0x0400011B RID: 283
		[SerializeField]
		private TMP_FontAsset m_defaultFontAsset;

		// Token: 0x0400011C RID: 284
		[SerializeField]
		private string m_defaultFontAssetPath;

		// Token: 0x0400011D RID: 285
		[SerializeField]
		private float m_defaultFontSize;

		// Token: 0x0400011E RID: 286
		[SerializeField]
		private float m_defaultAutoSizeMinRatio;

		// Token: 0x0400011F RID: 287
		[SerializeField]
		private float m_defaultAutoSizeMaxRatio;

		// Token: 0x04000120 RID: 288
		[SerializeField]
		private Vector2 m_defaultTextMeshProTextContainerSize;

		// Token: 0x04000121 RID: 289
		[SerializeField]
		private Vector2 m_defaultTextMeshProUITextContainerSize;

		// Token: 0x04000122 RID: 290
		[SerializeField]
		private bool m_autoSizeTextContainer;

		// Token: 0x04000123 RID: 291
		[SerializeField]
		private List<TMP_FontAsset> m_fallbackFontAssets;

		// Token: 0x04000124 RID: 292
		[SerializeField]
		private bool m_matchMaterialPreset;

		// Token: 0x04000125 RID: 293
		[SerializeField]
		private TMP_SpriteAsset m_defaultSpriteAsset;

		// Token: 0x04000126 RID: 294
		[SerializeField]
		private string m_defaultSpriteAssetPath;

		// Token: 0x04000127 RID: 295
		[SerializeField]
		private string m_defaultColorGradientPresetsPath;

		// Token: 0x04000128 RID: 296
		[SerializeField]
		private bool m_enableEmojiSupport;

		// Token: 0x04000129 RID: 297
		[SerializeField]
		private TMP_StyleSheet m_defaultStyleSheet;

		// Token: 0x0400012A RID: 298
		[SerializeField]
		private TextAsset m_leadingCharacters;

		// Token: 0x0400012B RID: 299
		[SerializeField]
		private TextAsset m_followingCharacters;

		// Token: 0x0400012C RID: 300
		[SerializeField]
		private TMP_Settings.LineBreakingTable m_linebreakingRules;

		// Token: 0x02000082 RID: 130
		public class LineBreakingTable
		{
			// Token: 0x060004DE RID: 1246 RVA: 0x000159ED File Offset: 0x00013BED
			public LineBreakingTable()
			{
			}

			// Token: 0x04000452 RID: 1106
			public Dictionary<int, char> leadingCharacters;

			// Token: 0x04000453 RID: 1107
			public Dictionary<int, char> followingCharacters;
		}
	}
}
