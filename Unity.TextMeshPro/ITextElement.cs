﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace TMPro
{
	// Token: 0x0200002B RID: 43
	public interface ITextElement
	{
		// Token: 0x170000AA RID: 170
		// (get) Token: 0x060002F1 RID: 753
		Material sharedMaterial { get; }

		// Token: 0x060002F2 RID: 754
		void Rebuild(CanvasUpdate update);

		// Token: 0x060002F3 RID: 755
		int GetInstanceID();
	}
}
