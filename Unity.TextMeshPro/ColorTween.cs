﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace TMPro
{
	// Token: 0x02000011 RID: 17
	internal struct ColorTween : ITweenValue
	{
		// Token: 0x17000021 RID: 33
		// (get) Token: 0x060000E8 RID: 232 RVA: 0x00015787 File Offset: 0x00013987
		// (set) Token: 0x060000E9 RID: 233 RVA: 0x0001578F File Offset: 0x0001398F
		public Color startColor
		{
			get
			{
				return this.m_StartColor;
			}
			set
			{
				this.m_StartColor = value;
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x060000EA RID: 234 RVA: 0x00015798 File Offset: 0x00013998
		// (set) Token: 0x060000EB RID: 235 RVA: 0x000157A0 File Offset: 0x000139A0
		public Color targetColor
		{
			get
			{
				return this.m_TargetColor;
			}
			set
			{
				this.m_TargetColor = value;
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x060000EC RID: 236 RVA: 0x000157A9 File Offset: 0x000139A9
		// (set) Token: 0x060000ED RID: 237 RVA: 0x000157B1 File Offset: 0x000139B1
		public ColorTween.ColorTweenMode tweenMode
		{
			get
			{
				return this.m_TweenMode;
			}
			set
			{
				this.m_TweenMode = value;
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x060000EE RID: 238 RVA: 0x000157BA File Offset: 0x000139BA
		// (set) Token: 0x060000EF RID: 239 RVA: 0x000157C2 File Offset: 0x000139C2
		public float duration
		{
			get
			{
				return this.m_Duration;
			}
			set
			{
				this.m_Duration = value;
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x060000F0 RID: 240 RVA: 0x000157CB File Offset: 0x000139CB
		// (set) Token: 0x060000F1 RID: 241 RVA: 0x000157D3 File Offset: 0x000139D3
		public bool ignoreTimeScale
		{
			get
			{
				return this.m_IgnoreTimeScale;
			}
			set
			{
				this.m_IgnoreTimeScale = value;
			}
		}

		// Token: 0x060000F2 RID: 242 RVA: 0x000157DC File Offset: 0x000139DC
		public void TweenValue(float floatPercentage)
		{
			if (!this.ValidTarget())
			{
				return;
			}
			Color arg = Color.Lerp(this.m_StartColor, this.m_TargetColor, floatPercentage);
			if (this.m_TweenMode == ColorTween.ColorTweenMode.Alpha)
			{
				arg.r = this.m_StartColor.r;
				arg.g = this.m_StartColor.g;
				arg.b = this.m_StartColor.b;
			}
			else if (this.m_TweenMode == ColorTween.ColorTweenMode.RGB)
			{
				arg.a = this.m_StartColor.a;
			}
			this.m_Target.Invoke(arg);
		}

		// Token: 0x060000F3 RID: 243 RVA: 0x0001586D File Offset: 0x00013A6D
		public void AddOnChangedCallback(UnityAction<Color> callback)
		{
			if (this.m_Target == null)
			{
				this.m_Target = new ColorTween.ColorTweenCallback();
			}
			this.m_Target.AddListener(callback);
		}

		// Token: 0x060000F4 RID: 244 RVA: 0x000157CB File Offset: 0x000139CB
		public bool GetIgnoreTimescale()
		{
			return this.m_IgnoreTimeScale;
		}

		// Token: 0x060000F5 RID: 245 RVA: 0x000157BA File Offset: 0x000139BA
		public float GetDuration()
		{
			return this.m_Duration;
		}

		// Token: 0x060000F6 RID: 246 RVA: 0x0001588E File Offset: 0x00013A8E
		public bool ValidTarget()
		{
			return this.m_Target != null;
		}

		// Token: 0x04000061 RID: 97
		private ColorTween.ColorTweenCallback m_Target;

		// Token: 0x04000062 RID: 98
		private Color m_StartColor;

		// Token: 0x04000063 RID: 99
		private Color m_TargetColor;

		// Token: 0x04000064 RID: 100
		private ColorTween.ColorTweenMode m_TweenMode;

		// Token: 0x04000065 RID: 101
		private float m_Duration;

		// Token: 0x04000066 RID: 102
		private bool m_IgnoreTimeScale;

		// Token: 0x02000062 RID: 98
		public enum ColorTweenMode
		{
			// Token: 0x040003FB RID: 1019
			All,
			// Token: 0x040003FC RID: 1020
			RGB,
			// Token: 0x040003FD RID: 1021
			Alpha
		}

		// Token: 0x02000063 RID: 99
		public class ColorTweenCallback : UnityEvent<Color>
		{
			// Token: 0x06000492 RID: 1170 RVA: 0x0002F2AE File Offset: 0x0002D4AE
			public ColorTweenCallback()
			{
			}
		}
	}
}
