﻿using System;

namespace TMPro
{
	// Token: 0x02000056 RID: 86
	public struct TMP_WordInfo
	{
		// Token: 0x06000485 RID: 1157 RVA: 0x0002E268 File Offset: 0x0002C468
		public string GetWord()
		{
			string text = string.Empty;
			TMP_CharacterInfo[] characterInfo = this.textComponent.textInfo.characterInfo;
			for (int i = this.firstCharacterIndex; i < this.lastCharacterIndex + 1; i++)
			{
				text += characterInfo[i].character.ToString();
			}
			return text;
		}

		// Token: 0x04000363 RID: 867
		public TMP_Text textComponent;

		// Token: 0x04000364 RID: 868
		public int firstCharacterIndex;

		// Token: 0x04000365 RID: 869
		public int lastCharacterIndex;

		// Token: 0x04000366 RID: 870
		public int characterCount;
	}
}
