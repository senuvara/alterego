﻿using System;

namespace TMPro
{
	// Token: 0x0200002E RID: 46
	public enum _VerticalAlignmentOptions
	{
		// Token: 0x0400018C RID: 396
		Top = 256,
		// Token: 0x0400018D RID: 397
		Middle = 512,
		// Token: 0x0400018E RID: 398
		Bottom = 1024,
		// Token: 0x0400018F RID: 399
		Baseline = 2048,
		// Token: 0x04000190 RID: 400
		Geometry = 4096,
		// Token: 0x04000191 RID: 401
		Capline = 8192
	}
}
