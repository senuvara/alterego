﻿using System;

namespace TMPro
{
	// Token: 0x0200004C RID: 76
	[Serializable]
	public struct GlyphValueRecord
	{
		// Token: 0x0600046A RID: 1130 RVA: 0x0002DD14 File Offset: 0x0002BF14
		public static GlyphValueRecord operator +(GlyphValueRecord a, GlyphValueRecord b)
		{
			GlyphValueRecord result;
			result.xPlacement = a.xPlacement + b.xPlacement;
			result.yPlacement = a.yPlacement + b.yPlacement;
			result.xAdvance = a.xAdvance + b.xAdvance;
			result.yAdvance = a.yAdvance + b.yAdvance;
			return result;
		}

		// Token: 0x04000318 RID: 792
		public float xPlacement;

		// Token: 0x04000319 RID: 793
		public float yPlacement;

		// Token: 0x0400031A RID: 794
		public float xAdvance;

		// Token: 0x0400031B RID: 795
		public float yAdvance;
	}
}
