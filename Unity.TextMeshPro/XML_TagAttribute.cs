﻿using System;

namespace TMPro
{
	// Token: 0x0200005C RID: 92
	public struct XML_TagAttribute
	{
		// Token: 0x040003A8 RID: 936
		public int nameHashCode;

		// Token: 0x040003A9 RID: 937
		public TagType valueType;

		// Token: 0x040003AA RID: 938
		public int valueStartIndex;

		// Token: 0x040003AB RID: 939
		public int valueLength;

		// Token: 0x040003AC RID: 940
		public int valueHashCode;
	}
}
