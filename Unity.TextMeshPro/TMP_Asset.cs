﻿using System;
using UnityEngine;

namespace TMPro
{
	// Token: 0x0200000C RID: 12
	[Serializable]
	public class TMP_Asset : ScriptableObject
	{
		// Token: 0x060000DE RID: 222 RVA: 0x000155AE File Offset: 0x000137AE
		public TMP_Asset()
		{
		}

		// Token: 0x04000052 RID: 82
		public int hashCode;

		// Token: 0x04000053 RID: 83
		public Material material;

		// Token: 0x04000054 RID: 84
		public int materialHashCode;
	}
}
