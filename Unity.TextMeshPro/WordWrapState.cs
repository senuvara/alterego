﻿using System;
using UnityEngine;

namespace TMPro
{
	// Token: 0x0200005A RID: 90
	public struct WordWrapState
	{
		// Token: 0x0400036E RID: 878
		public int previous_WordBreak;

		// Token: 0x0400036F RID: 879
		public int total_CharacterCount;

		// Token: 0x04000370 RID: 880
		public int visible_CharacterCount;

		// Token: 0x04000371 RID: 881
		public int visible_SpriteCount;

		// Token: 0x04000372 RID: 882
		public int visible_LinkCount;

		// Token: 0x04000373 RID: 883
		public int firstCharacterIndex;

		// Token: 0x04000374 RID: 884
		public int firstVisibleCharacterIndex;

		// Token: 0x04000375 RID: 885
		public int lastCharacterIndex;

		// Token: 0x04000376 RID: 886
		public int lastVisibleCharIndex;

		// Token: 0x04000377 RID: 887
		public int lineNumber;

		// Token: 0x04000378 RID: 888
		public float maxCapHeight;

		// Token: 0x04000379 RID: 889
		public float maxAscender;

		// Token: 0x0400037A RID: 890
		public float maxDescender;

		// Token: 0x0400037B RID: 891
		public float maxLineAscender;

		// Token: 0x0400037C RID: 892
		public float maxLineDescender;

		// Token: 0x0400037D RID: 893
		public float previousLineAscender;

		// Token: 0x0400037E RID: 894
		public float xAdvance;

		// Token: 0x0400037F RID: 895
		public float preferredWidth;

		// Token: 0x04000380 RID: 896
		public float preferredHeight;

		// Token: 0x04000381 RID: 897
		public float previousLineScale;

		// Token: 0x04000382 RID: 898
		public int wordCount;

		// Token: 0x04000383 RID: 899
		public FontStyles fontStyle;

		// Token: 0x04000384 RID: 900
		public float fontScale;

		// Token: 0x04000385 RID: 901
		public float fontScaleMultiplier;

		// Token: 0x04000386 RID: 902
		public float currentFontSize;

		// Token: 0x04000387 RID: 903
		public float baselineOffset;

		// Token: 0x04000388 RID: 904
		public float lineOffset;

		// Token: 0x04000389 RID: 905
		public TMP_TextInfo textInfo;

		// Token: 0x0400038A RID: 906
		public TMP_LineInfo lineInfo;

		// Token: 0x0400038B RID: 907
		public Color32 vertexColor;

		// Token: 0x0400038C RID: 908
		public Color32 underlineColor;

		// Token: 0x0400038D RID: 909
		public Color32 strikethroughColor;

		// Token: 0x0400038E RID: 910
		public Color32 highlightColor;

		// Token: 0x0400038F RID: 911
		public TMP_BasicXmlTagStack basicStyleStack;

		// Token: 0x04000390 RID: 912
		public TMP_XmlTagStack<Color32> colorStack;

		// Token: 0x04000391 RID: 913
		public TMP_XmlTagStack<Color32> underlineColorStack;

		// Token: 0x04000392 RID: 914
		public TMP_XmlTagStack<Color32> strikethroughColorStack;

		// Token: 0x04000393 RID: 915
		public TMP_XmlTagStack<Color32> highlightColorStack;

		// Token: 0x04000394 RID: 916
		public TMP_XmlTagStack<TMP_ColorGradient> colorGradientStack;

		// Token: 0x04000395 RID: 917
		public TMP_XmlTagStack<float> sizeStack;

		// Token: 0x04000396 RID: 918
		public TMP_XmlTagStack<float> indentStack;

		// Token: 0x04000397 RID: 919
		public TMP_XmlTagStack<int> fontWeightStack;

		// Token: 0x04000398 RID: 920
		public TMP_XmlTagStack<int> styleStack;

		// Token: 0x04000399 RID: 921
		public TMP_XmlTagStack<float> baselineStack;

		// Token: 0x0400039A RID: 922
		public TMP_XmlTagStack<int> actionStack;

		// Token: 0x0400039B RID: 923
		public TMP_XmlTagStack<MaterialReference> materialReferenceStack;

		// Token: 0x0400039C RID: 924
		public TMP_XmlTagStack<TextAlignmentOptions> lineJustificationStack;

		// Token: 0x0400039D RID: 925
		public int spriteAnimationID;

		// Token: 0x0400039E RID: 926
		public TMP_FontAsset currentFontAsset;

		// Token: 0x0400039F RID: 927
		public TMP_SpriteAsset currentSpriteAsset;

		// Token: 0x040003A0 RID: 928
		public Material currentMaterial;

		// Token: 0x040003A1 RID: 929
		public int currentMaterialIndex;

		// Token: 0x040003A2 RID: 930
		public Extents meshExtents;

		// Token: 0x040003A3 RID: 931
		public bool tagNoParsing;

		// Token: 0x040003A4 RID: 932
		public bool isNonBreakingSpace;
	}
}
