﻿using System;

namespace TMPro
{
	// Token: 0x02000043 RID: 67
	public enum Compute_DistanceTransform_EventTypes
	{
		// Token: 0x040002D6 RID: 726
		Processing,
		// Token: 0x040002D7 RID: 727
		Completed
	}
}
