﻿using System;
using UnityEngine;

namespace TMPro
{
	// Token: 0x02000024 RID: 36
	[Serializable]
	public class TMP_Sprite : TMP_TextElement
	{
		// Token: 0x06000275 RID: 629 RVA: 0x0001E27D File Offset: 0x0001C47D
		public TMP_Sprite()
		{
		}

		// Token: 0x0400012D RID: 301
		public string name;

		// Token: 0x0400012E RID: 302
		public int hashCode;

		// Token: 0x0400012F RID: 303
		public int unicode;

		// Token: 0x04000130 RID: 304
		public Vector2 pivot;

		// Token: 0x04000131 RID: 305
		public Sprite sprite;
	}
}
