﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace TMPro
{
	// Token: 0x02000015 RID: 21
	[AddComponentMenu("UI/TMP Dropdown", 35)]
	[RequireComponent(typeof(RectTransform))]
	public class TMP_Dropdown : Selectable, IPointerClickHandler, IEventSystemHandler, ISubmitHandler, ICancelHandler
	{
		// Token: 0x1700002A RID: 42
		// (get) Token: 0x06000113 RID: 275 RVA: 0x00016455 File Offset: 0x00014655
		// (set) Token: 0x06000114 RID: 276 RVA: 0x0001645D File Offset: 0x0001465D
		public RectTransform template
		{
			get
			{
				return this.m_Template;
			}
			set
			{
				this.m_Template = value;
				this.RefreshShownValue();
			}
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x06000115 RID: 277 RVA: 0x0001646C File Offset: 0x0001466C
		// (set) Token: 0x06000116 RID: 278 RVA: 0x00016474 File Offset: 0x00014674
		public TMP_Text captionText
		{
			get
			{
				return this.m_CaptionText;
			}
			set
			{
				this.m_CaptionText = value;
				this.RefreshShownValue();
			}
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x06000117 RID: 279 RVA: 0x00016483 File Offset: 0x00014683
		// (set) Token: 0x06000118 RID: 280 RVA: 0x0001648B File Offset: 0x0001468B
		public Image captionImage
		{
			get
			{
				return this.m_CaptionImage;
			}
			set
			{
				this.m_CaptionImage = value;
				this.RefreshShownValue();
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x06000119 RID: 281 RVA: 0x0001649A File Offset: 0x0001469A
		// (set) Token: 0x0600011A RID: 282 RVA: 0x000164A2 File Offset: 0x000146A2
		public TMP_Text itemText
		{
			get
			{
				return this.m_ItemText;
			}
			set
			{
				this.m_ItemText = value;
				this.RefreshShownValue();
			}
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x0600011B RID: 283 RVA: 0x000164B1 File Offset: 0x000146B1
		// (set) Token: 0x0600011C RID: 284 RVA: 0x000164B9 File Offset: 0x000146B9
		public Image itemImage
		{
			get
			{
				return this.m_ItemImage;
			}
			set
			{
				this.m_ItemImage = value;
				this.RefreshShownValue();
			}
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x0600011D RID: 285 RVA: 0x000164C8 File Offset: 0x000146C8
		// (set) Token: 0x0600011E RID: 286 RVA: 0x000164D5 File Offset: 0x000146D5
		public List<TMP_Dropdown.OptionData> options
		{
			get
			{
				return this.m_Options.options;
			}
			set
			{
				this.m_Options.options = value;
				this.RefreshShownValue();
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x0600011F RID: 287 RVA: 0x000164E9 File Offset: 0x000146E9
		// (set) Token: 0x06000120 RID: 288 RVA: 0x000164F1 File Offset: 0x000146F1
		public TMP_Dropdown.DropdownEvent onValueChanged
		{
			get
			{
				return this.m_OnValueChanged;
			}
			set
			{
				this.m_OnValueChanged = value;
			}
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x06000121 RID: 289 RVA: 0x000164FA File Offset: 0x000146FA
		// (set) Token: 0x06000122 RID: 290 RVA: 0x00016504 File Offset: 0x00014704
		public int value
		{
			get
			{
				return this.m_Value;
			}
			set
			{
				if (Application.isPlaying && (value == this.m_Value || this.options.Count == 0))
				{
					return;
				}
				this.m_Value = Mathf.Clamp(value, 0, this.options.Count - 1);
				this.RefreshShownValue();
				this.m_OnValueChanged.Invoke(this.m_Value);
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x06000123 RID: 291 RVA: 0x00016560 File Offset: 0x00014760
		public bool IsExpanded
		{
			get
			{
				return this.m_Dropdown != null;
			}
		}

		// Token: 0x06000124 RID: 292 RVA: 0x0001656E File Offset: 0x0001476E
		protected TMP_Dropdown()
		{
		}

		// Token: 0x06000125 RID: 293 RVA: 0x00016598 File Offset: 0x00014798
		protected override void Awake()
		{
			this.m_AlphaTweenRunner = new TweenRunner<FloatTween>();
			this.m_AlphaTweenRunner.Init(this);
			if (this.m_CaptionImage)
			{
				this.m_CaptionImage.enabled = (this.m_CaptionImage.sprite != null);
			}
			if (this.m_Template)
			{
				this.m_Template.gameObject.SetActive(false);
			}
		}

		// Token: 0x06000126 RID: 294 RVA: 0x00016604 File Offset: 0x00014804
		public void RefreshShownValue()
		{
			TMP_Dropdown.OptionData optionData = TMP_Dropdown.s_NoOptionData;
			if (this.options.Count > 0)
			{
				optionData = this.options[Mathf.Clamp(this.m_Value, 0, this.options.Count - 1)];
			}
			if (this.m_CaptionText)
			{
				if (optionData != null && optionData.text != null)
				{
					this.m_CaptionText.text = optionData.text;
				}
				else
				{
					this.m_CaptionText.text = "";
				}
			}
			if (this.m_CaptionImage)
			{
				if (optionData != null)
				{
					this.m_CaptionImage.sprite = optionData.image;
				}
				else
				{
					this.m_CaptionImage.sprite = null;
				}
				this.m_CaptionImage.enabled = (this.m_CaptionImage.sprite != null);
			}
		}

		// Token: 0x06000127 RID: 295 RVA: 0x000166D0 File Offset: 0x000148D0
		public void AddOptions(List<TMP_Dropdown.OptionData> options)
		{
			this.options.AddRange(options);
			this.RefreshShownValue();
		}

		// Token: 0x06000128 RID: 296 RVA: 0x000166E4 File Offset: 0x000148E4
		public void AddOptions(List<string> options)
		{
			for (int i = 0; i < options.Count; i++)
			{
				this.options.Add(new TMP_Dropdown.OptionData(options[i]));
			}
			this.RefreshShownValue();
		}

		// Token: 0x06000129 RID: 297 RVA: 0x00016720 File Offset: 0x00014920
		public void AddOptions(List<Sprite> options)
		{
			for (int i = 0; i < options.Count; i++)
			{
				this.options.Add(new TMP_Dropdown.OptionData(options[i]));
			}
			this.RefreshShownValue();
		}

		// Token: 0x0600012A RID: 298 RVA: 0x0001675B File Offset: 0x0001495B
		public void ClearOptions()
		{
			this.options.Clear();
			this.RefreshShownValue();
		}

		// Token: 0x0600012B RID: 299 RVA: 0x00016770 File Offset: 0x00014970
		private void SetupTemplate()
		{
			this.validTemplate = false;
			if (!this.m_Template)
			{
				Debug.LogError("The dropdown template is not assigned. The template needs to be assigned and must have a child GameObject with a Toggle component serving as the item.", this);
				return;
			}
			GameObject gameObject = this.m_Template.gameObject;
			gameObject.SetActive(true);
			Toggle componentInChildren = this.m_Template.GetComponentInChildren<Toggle>();
			this.validTemplate = true;
			if (!componentInChildren || componentInChildren.transform == this.template)
			{
				this.validTemplate = false;
				Debug.LogError("The dropdown template is not valid. The template must have a child GameObject with a Toggle component serving as the item.", this.template);
			}
			else if (!(componentInChildren.transform.parent is RectTransform))
			{
				this.validTemplate = false;
				Debug.LogError("The dropdown template is not valid. The child GameObject with a Toggle component (the item) must have a RectTransform on its parent.", this.template);
			}
			else if (this.itemText != null && !this.itemText.transform.IsChildOf(componentInChildren.transform))
			{
				this.validTemplate = false;
				Debug.LogError("The dropdown template is not valid. The Item Text must be on the item GameObject or children of it.", this.template);
			}
			else if (this.itemImage != null && !this.itemImage.transform.IsChildOf(componentInChildren.transform))
			{
				this.validTemplate = false;
				Debug.LogError("The dropdown template is not valid. The Item Image must be on the item GameObject or children of it.", this.template);
			}
			if (!this.validTemplate)
			{
				gameObject.SetActive(false);
				return;
			}
			TMP_Dropdown.DropdownItem dropdownItem = componentInChildren.gameObject.AddComponent<TMP_Dropdown.DropdownItem>();
			dropdownItem.text = this.m_ItemText;
			dropdownItem.image = this.m_ItemImage;
			dropdownItem.toggle = componentInChildren;
			dropdownItem.rectTransform = (RectTransform)componentInChildren.transform;
			Canvas orAddComponent = TMP_Dropdown.GetOrAddComponent<Canvas>(gameObject);
			orAddComponent.overrideSorting = true;
			orAddComponent.sortingOrder = 30000;
			TMP_Dropdown.GetOrAddComponent<GraphicRaycaster>(gameObject);
			TMP_Dropdown.GetOrAddComponent<CanvasGroup>(gameObject);
			gameObject.SetActive(false);
			this.validTemplate = true;
		}

		// Token: 0x0600012C RID: 300 RVA: 0x00016920 File Offset: 0x00014B20
		private static T GetOrAddComponent<T>(GameObject go) where T : Component
		{
			T t = go.GetComponent<T>();
			if (!t)
			{
				t = go.AddComponent<T>();
			}
			return t;
		}

		// Token: 0x0600012D RID: 301 RVA: 0x00016949 File Offset: 0x00014B49
		public virtual void OnPointerClick(PointerEventData eventData)
		{
			this.Show();
		}

		// Token: 0x0600012E RID: 302 RVA: 0x00016949 File Offset: 0x00014B49
		public virtual void OnSubmit(BaseEventData eventData)
		{
			this.Show();
		}

		// Token: 0x0600012F RID: 303 RVA: 0x00016951 File Offset: 0x00014B51
		public virtual void OnCancel(BaseEventData eventData)
		{
			this.Hide();
		}

		// Token: 0x06000130 RID: 304 RVA: 0x0001695C File Offset: 0x00014B5C
		public void Show()
		{
			if (!this.IsActive() || !this.IsInteractable() || this.m_Dropdown != null)
			{
				return;
			}
			if (!this.validTemplate)
			{
				this.SetupTemplate();
				if (!this.validTemplate)
				{
					return;
				}
			}
			List<Canvas> list = TMP_ListPool<Canvas>.Get();
			base.gameObject.GetComponentsInParent<Canvas>(false, list);
			if (list.Count == 0)
			{
				return;
			}
			Canvas canvas = list[0];
			TMP_ListPool<Canvas>.Release(list);
			this.m_Template.gameObject.SetActive(true);
			this.m_Dropdown = this.CreateDropdownList(this.m_Template.gameObject);
			this.m_Dropdown.name = "Dropdown List";
			this.m_Dropdown.SetActive(true);
			RectTransform rectTransform = this.m_Dropdown.transform as RectTransform;
			rectTransform.SetParent(this.m_Template.transform.parent, false);
			TMP_Dropdown.DropdownItem componentInChildren = this.m_Dropdown.GetComponentInChildren<TMP_Dropdown.DropdownItem>();
			RectTransform rectTransform2 = componentInChildren.rectTransform.parent.gameObject.transform as RectTransform;
			componentInChildren.rectTransform.gameObject.SetActive(true);
			Rect rect = rectTransform2.rect;
			Rect rect2 = componentInChildren.rectTransform.rect;
			Vector2 vector = rect2.min - rect.min + componentInChildren.rectTransform.localPosition;
			Vector2 vector2 = rect2.max - rect.max + componentInChildren.rectTransform.localPosition;
			Vector2 size = rect2.size;
			this.m_Items.Clear();
			Toggle toggle = null;
			for (int i = 0; i < this.options.Count; i++)
			{
				TMP_Dropdown.OptionData data = this.options[i];
				TMP_Dropdown.DropdownItem item = this.AddItem(data, this.value == i, componentInChildren, this.m_Items);
				if (!(item == null))
				{
					item.toggle.isOn = (this.value == i);
					item.toggle.onValueChanged.AddListener(delegate(bool x)
					{
						this.OnSelectItem(item.toggle);
					});
					if (item.toggle.isOn)
					{
						item.toggle.Select();
					}
					if (toggle != null)
					{
						Navigation navigation = toggle.navigation;
						Navigation navigation2 = item.toggle.navigation;
						navigation.mode = Navigation.Mode.Explicit;
						navigation2.mode = Navigation.Mode.Explicit;
						navigation.selectOnDown = item.toggle;
						navigation.selectOnRight = item.toggle;
						navigation2.selectOnLeft = toggle;
						navigation2.selectOnUp = toggle;
						toggle.navigation = navigation;
						item.toggle.navigation = navigation2;
					}
					toggle = item.toggle;
				}
			}
			Vector2 sizeDelta = rectTransform2.sizeDelta;
			sizeDelta.y = size.y * (float)this.m_Items.Count + vector.y - vector2.y;
			rectTransform2.sizeDelta = sizeDelta;
			float num = rectTransform.rect.height - rectTransform2.rect.height;
			if (num > 0f)
			{
				rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, rectTransform.sizeDelta.y - num);
			}
			Vector3[] array = new Vector3[4];
			rectTransform.GetWorldCorners(array);
			RectTransform rectTransform3 = canvas.transform as RectTransform;
			Rect rect3 = rectTransform3.rect;
			for (int j = 0; j < 2; j++)
			{
				bool flag = false;
				for (int k = 0; k < 4; k++)
				{
					Vector3 vector3 = rectTransform3.InverseTransformPoint(array[k]);
					if (vector3[j] < rect3.min[j] || vector3[j] > rect3.max[j])
					{
						flag = true;
						break;
					}
				}
				if (flag)
				{
					RectTransformUtility.FlipLayoutOnAxis(rectTransform, j, false, false);
				}
			}
			for (int l = 0; l < this.m_Items.Count; l++)
			{
				RectTransform rectTransform4 = this.m_Items[l].rectTransform;
				rectTransform4.anchorMin = new Vector2(rectTransform4.anchorMin.x, 0f);
				rectTransform4.anchorMax = new Vector2(rectTransform4.anchorMax.x, 0f);
				rectTransform4.anchoredPosition = new Vector2(rectTransform4.anchoredPosition.x, vector.y + size.y * (float)(this.m_Items.Count - 1 - l) + size.y * rectTransform4.pivot.y);
				rectTransform4.sizeDelta = new Vector2(rectTransform4.sizeDelta.x, size.y);
			}
			this.AlphaFadeList(0.15f, 0f, 1f);
			this.m_Template.gameObject.SetActive(false);
			componentInChildren.gameObject.SetActive(false);
			this.m_Blocker = this.CreateBlocker(canvas);
		}

		// Token: 0x06000131 RID: 305 RVA: 0x00016EAC File Offset: 0x000150AC
		protected virtual GameObject CreateBlocker(Canvas rootCanvas)
		{
			GameObject gameObject = new GameObject("Blocker");
			RectTransform rectTransform = gameObject.AddComponent<RectTransform>();
			rectTransform.SetParent(rootCanvas.transform, false);
			rectTransform.anchorMin = Vector3.zero;
			rectTransform.anchorMax = Vector3.one;
			rectTransform.sizeDelta = Vector2.zero;
			Canvas canvas = gameObject.AddComponent<Canvas>();
			canvas.overrideSorting = true;
			Canvas component = this.m_Dropdown.GetComponent<Canvas>();
			canvas.sortingLayerID = component.sortingLayerID;
			canvas.sortingOrder = component.sortingOrder - 1;
			gameObject.AddComponent<GraphicRaycaster>();
			gameObject.AddComponent<Image>().color = Color.clear;
			gameObject.AddComponent<Button>().onClick.AddListener(new UnityAction(this.Hide));
			return gameObject;
		}

		// Token: 0x06000132 RID: 306 RVA: 0x00016F65 File Offset: 0x00015165
		protected virtual void DestroyBlocker(GameObject blocker)
		{
			Object.Destroy(blocker);
		}

		// Token: 0x06000133 RID: 307 RVA: 0x00016F6D File Offset: 0x0001516D
		protected virtual GameObject CreateDropdownList(GameObject template)
		{
			return Object.Instantiate<GameObject>(template);
		}

		// Token: 0x06000134 RID: 308 RVA: 0x00016F65 File Offset: 0x00015165
		protected virtual void DestroyDropdownList(GameObject dropdownList)
		{
			Object.Destroy(dropdownList);
		}

		// Token: 0x06000135 RID: 309 RVA: 0x00016F75 File Offset: 0x00015175
		protected virtual TMP_Dropdown.DropdownItem CreateItem(TMP_Dropdown.DropdownItem itemTemplate)
		{
			return Object.Instantiate<TMP_Dropdown.DropdownItem>(itemTemplate);
		}

		// Token: 0x06000136 RID: 310 RVA: 0x0000297D File Offset: 0x00000B7D
		protected virtual void DestroyItem(TMP_Dropdown.DropdownItem item)
		{
		}

		// Token: 0x06000137 RID: 311 RVA: 0x00016F80 File Offset: 0x00015180
		private TMP_Dropdown.DropdownItem AddItem(TMP_Dropdown.OptionData data, bool selected, TMP_Dropdown.DropdownItem itemTemplate, List<TMP_Dropdown.DropdownItem> items)
		{
			TMP_Dropdown.DropdownItem dropdownItem = this.CreateItem(itemTemplate);
			dropdownItem.rectTransform.SetParent(itemTemplate.rectTransform.parent, false);
			dropdownItem.gameObject.SetActive(true);
			dropdownItem.gameObject.name = "Item " + items.Count + ((data.text != null) ? (": " + data.text) : "");
			if (dropdownItem.toggle != null)
			{
				dropdownItem.toggle.isOn = false;
			}
			if (dropdownItem.text)
			{
				dropdownItem.text.text = data.text;
			}
			if (dropdownItem.image)
			{
				dropdownItem.image.sprite = data.image;
				dropdownItem.image.enabled = (dropdownItem.image.sprite != null);
			}
			items.Add(dropdownItem);
			return dropdownItem;
		}

		// Token: 0x06000138 RID: 312 RVA: 0x00017074 File Offset: 0x00015274
		private void AlphaFadeList(float duration, float alpha)
		{
			CanvasGroup component = this.m_Dropdown.GetComponent<CanvasGroup>();
			this.AlphaFadeList(duration, component.alpha, alpha);
		}

		// Token: 0x06000139 RID: 313 RVA: 0x0001709C File Offset: 0x0001529C
		private void AlphaFadeList(float duration, float start, float end)
		{
			if (end.Equals(start))
			{
				return;
			}
			FloatTween info = new FloatTween
			{
				duration = duration,
				startValue = start,
				targetValue = end
			};
			info.AddOnChangedCallback(new UnityAction<float>(this.SetAlpha));
			info.ignoreTimeScale = true;
			this.m_AlphaTweenRunner.StartTween(info);
		}

		// Token: 0x0600013A RID: 314 RVA: 0x000170FD File Offset: 0x000152FD
		private void SetAlpha(float alpha)
		{
			if (!this.m_Dropdown)
			{
				return;
			}
			this.m_Dropdown.GetComponent<CanvasGroup>().alpha = alpha;
		}

		// Token: 0x0600013B RID: 315 RVA: 0x00017120 File Offset: 0x00015320
		public void Hide()
		{
			if (this.m_Dropdown != null)
			{
				this.AlphaFadeList(0.15f, 0f);
				if (this.IsActive())
				{
					base.StartCoroutine(this.DelayedDestroyDropdownList(0.15f));
				}
			}
			if (this.m_Blocker != null)
			{
				this.DestroyBlocker(this.m_Blocker);
			}
			this.m_Blocker = null;
			this.Select();
		}

		// Token: 0x0600013C RID: 316 RVA: 0x0001718C File Offset: 0x0001538C
		private IEnumerator DelayedDestroyDropdownList(float delay)
		{
			yield return new WaitForSecondsRealtime(delay);
			for (int i = 0; i < this.m_Items.Count; i++)
			{
				if (this.m_Items[i] != null)
				{
					this.DestroyItem(this.m_Items[i]);
				}
				this.m_Items.Clear();
			}
			if (this.m_Dropdown != null)
			{
				this.DestroyDropdownList(this.m_Dropdown);
			}
			this.m_Dropdown = null;
			yield break;
		}

		// Token: 0x0600013D RID: 317 RVA: 0x000171A4 File Offset: 0x000153A4
		private void OnSelectItem(Toggle toggle)
		{
			if (!toggle.isOn)
			{
				toggle.isOn = true;
			}
			int num = -1;
			Transform transform = toggle.transform;
			Transform parent = transform.parent;
			for (int i = 0; i < parent.childCount; i++)
			{
				if (parent.GetChild(i) == transform)
				{
					num = i - 1;
					break;
				}
			}
			if (num < 0)
			{
				return;
			}
			this.value = num;
			this.Hide();
		}

		// Token: 0x0600013E RID: 318 RVA: 0x00017208 File Offset: 0x00015408
		// Note: this type is marked as 'beforefieldinit'.
		static TMP_Dropdown()
		{
		}

		// Token: 0x04000075 RID: 117
		[SerializeField]
		private RectTransform m_Template;

		// Token: 0x04000076 RID: 118
		[SerializeField]
		private TMP_Text m_CaptionText;

		// Token: 0x04000077 RID: 119
		[SerializeField]
		private Image m_CaptionImage;

		// Token: 0x04000078 RID: 120
		[Space]
		[SerializeField]
		private TMP_Text m_ItemText;

		// Token: 0x04000079 RID: 121
		[SerializeField]
		private Image m_ItemImage;

		// Token: 0x0400007A RID: 122
		[Space]
		[SerializeField]
		private int m_Value;

		// Token: 0x0400007B RID: 123
		[Space]
		[SerializeField]
		private TMP_Dropdown.OptionDataList m_Options = new TMP_Dropdown.OptionDataList();

		// Token: 0x0400007C RID: 124
		[Space]
		[SerializeField]
		private TMP_Dropdown.DropdownEvent m_OnValueChanged = new TMP_Dropdown.DropdownEvent();

		// Token: 0x0400007D RID: 125
		private GameObject m_Dropdown;

		// Token: 0x0400007E RID: 126
		private GameObject m_Blocker;

		// Token: 0x0400007F RID: 127
		private List<TMP_Dropdown.DropdownItem> m_Items = new List<TMP_Dropdown.DropdownItem>();

		// Token: 0x04000080 RID: 128
		private TweenRunner<FloatTween> m_AlphaTweenRunner;

		// Token: 0x04000081 RID: 129
		private bool validTemplate;

		// Token: 0x04000082 RID: 130
		private static TMP_Dropdown.OptionData s_NoOptionData = new TMP_Dropdown.OptionData();

		// Token: 0x02000067 RID: 103
		protected internal class DropdownItem : MonoBehaviour, IPointerEnterHandler, IEventSystemHandler, ICancelHandler
		{
			// Token: 0x17000109 RID: 265
			// (get) Token: 0x0600049A RID: 1178 RVA: 0x0002F3CE File Offset: 0x0002D5CE
			// (set) Token: 0x0600049B RID: 1179 RVA: 0x0002F3D6 File Offset: 0x0002D5D6
			public TMP_Text text
			{
				get
				{
					return this.m_Text;
				}
				set
				{
					this.m_Text = value;
				}
			}

			// Token: 0x1700010A RID: 266
			// (get) Token: 0x0600049C RID: 1180 RVA: 0x0002F3DF File Offset: 0x0002D5DF
			// (set) Token: 0x0600049D RID: 1181 RVA: 0x0002F3E7 File Offset: 0x0002D5E7
			public Image image
			{
				get
				{
					return this.m_Image;
				}
				set
				{
					this.m_Image = value;
				}
			}

			// Token: 0x1700010B RID: 267
			// (get) Token: 0x0600049E RID: 1182 RVA: 0x0002F3F0 File Offset: 0x0002D5F0
			// (set) Token: 0x0600049F RID: 1183 RVA: 0x0002F3F8 File Offset: 0x0002D5F8
			public RectTransform rectTransform
			{
				get
				{
					return this.m_RectTransform;
				}
				set
				{
					this.m_RectTransform = value;
				}
			}

			// Token: 0x1700010C RID: 268
			// (get) Token: 0x060004A0 RID: 1184 RVA: 0x0002F401 File Offset: 0x0002D601
			// (set) Token: 0x060004A1 RID: 1185 RVA: 0x0002F409 File Offset: 0x0002D609
			public Toggle toggle
			{
				get
				{
					return this.m_Toggle;
				}
				set
				{
					this.m_Toggle = value;
				}
			}

			// Token: 0x060004A2 RID: 1186 RVA: 0x0002F412 File Offset: 0x0002D612
			public virtual void OnPointerEnter(PointerEventData eventData)
			{
				EventSystem.current.SetSelectedGameObject(base.gameObject);
			}

			// Token: 0x060004A3 RID: 1187 RVA: 0x0002F424 File Offset: 0x0002D624
			public virtual void OnCancel(BaseEventData eventData)
			{
				TMP_Dropdown componentInParent = base.GetComponentInParent<TMP_Dropdown>();
				if (componentInParent)
				{
					componentInParent.Hide();
				}
			}

			// Token: 0x060004A4 RID: 1188 RVA: 0x0001DF9F File Offset: 0x0001C19F
			public DropdownItem()
			{
			}

			// Token: 0x04000409 RID: 1033
			[SerializeField]
			private TMP_Text m_Text;

			// Token: 0x0400040A RID: 1034
			[SerializeField]
			private Image m_Image;

			// Token: 0x0400040B RID: 1035
			[SerializeField]
			private RectTransform m_RectTransform;

			// Token: 0x0400040C RID: 1036
			[SerializeField]
			private Toggle m_Toggle;
		}

		// Token: 0x02000068 RID: 104
		[Serializable]
		public class OptionData
		{
			// Token: 0x1700010D RID: 269
			// (get) Token: 0x060004A5 RID: 1189 RVA: 0x0002F446 File Offset: 0x0002D646
			// (set) Token: 0x060004A6 RID: 1190 RVA: 0x0002F44E File Offset: 0x0002D64E
			public string text
			{
				get
				{
					return this.m_Text;
				}
				set
				{
					this.m_Text = value;
				}
			}

			// Token: 0x1700010E RID: 270
			// (get) Token: 0x060004A7 RID: 1191 RVA: 0x0002F457 File Offset: 0x0002D657
			// (set) Token: 0x060004A8 RID: 1192 RVA: 0x0002F45F File Offset: 0x0002D65F
			public Sprite image
			{
				get
				{
					return this.m_Image;
				}
				set
				{
					this.m_Image = value;
				}
			}

			// Token: 0x060004A9 RID: 1193 RVA: 0x000159ED File Offset: 0x00013BED
			public OptionData()
			{
			}

			// Token: 0x060004AA RID: 1194 RVA: 0x0002F468 File Offset: 0x0002D668
			public OptionData(string text)
			{
				this.text = text;
			}

			// Token: 0x060004AB RID: 1195 RVA: 0x0002F477 File Offset: 0x0002D677
			public OptionData(Sprite image)
			{
				this.image = image;
			}

			// Token: 0x060004AC RID: 1196 RVA: 0x0002F486 File Offset: 0x0002D686
			public OptionData(string text, Sprite image)
			{
				this.text = text;
				this.image = image;
			}

			// Token: 0x0400040D RID: 1037
			[SerializeField]
			private string m_Text;

			// Token: 0x0400040E RID: 1038
			[SerializeField]
			private Sprite m_Image;
		}

		// Token: 0x02000069 RID: 105
		[Serializable]
		public class OptionDataList
		{
			// Token: 0x1700010F RID: 271
			// (get) Token: 0x060004AD RID: 1197 RVA: 0x0002F49C File Offset: 0x0002D69C
			// (set) Token: 0x060004AE RID: 1198 RVA: 0x0002F4A4 File Offset: 0x0002D6A4
			public List<TMP_Dropdown.OptionData> options
			{
				get
				{
					return this.m_Options;
				}
				set
				{
					this.m_Options = value;
				}
			}

			// Token: 0x060004AF RID: 1199 RVA: 0x0002F4AD File Offset: 0x0002D6AD
			public OptionDataList()
			{
				this.options = new List<TMP_Dropdown.OptionData>();
			}

			// Token: 0x0400040F RID: 1039
			[SerializeField]
			private List<TMP_Dropdown.OptionData> m_Options;
		}

		// Token: 0x0200006A RID: 106
		[Serializable]
		public class DropdownEvent : UnityEvent<int>
		{
			// Token: 0x060004B0 RID: 1200 RVA: 0x0002F4C0 File Offset: 0x0002D6C0
			public DropdownEvent()
			{
			}
		}

		// Token: 0x0200006B RID: 107
		[CompilerGenerated]
		private sealed class <>c__DisplayClass56_0
		{
			// Token: 0x060004B1 RID: 1201 RVA: 0x000159ED File Offset: 0x00013BED
			public <>c__DisplayClass56_0()
			{
			}

			// Token: 0x060004B2 RID: 1202 RVA: 0x0002F4C8 File Offset: 0x0002D6C8
			internal void <Show>b__0(bool x)
			{
				this.<>4__this.OnSelectItem(this.item.toggle);
			}

			// Token: 0x04000410 RID: 1040
			public TMP_Dropdown.DropdownItem item;

			// Token: 0x04000411 RID: 1041
			public TMP_Dropdown <>4__this;
		}

		// Token: 0x0200006C RID: 108
		[CompilerGenerated]
		private sealed class <DelayedDestroyDropdownList>d__68 : IEnumerator<object>, IEnumerator, IDisposable
		{
			// Token: 0x060004B3 RID: 1203 RVA: 0x0002F4E0 File Offset: 0x0002D6E0
			[DebuggerHidden]
			public <DelayedDestroyDropdownList>d__68(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x060004B4 RID: 1204 RVA: 0x0000297D File Offset: 0x00000B7D
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x060004B5 RID: 1205 RVA: 0x0002F4F0 File Offset: 0x0002D6F0
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				TMP_Dropdown tmp_Dropdown = this;
				if (num == 0)
				{
					this.<>1__state = -1;
					this.<>2__current = new WaitForSecondsRealtime(delay);
					this.<>1__state = 1;
					return true;
				}
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				for (int i = 0; i < tmp_Dropdown.m_Items.Count; i++)
				{
					if (tmp_Dropdown.m_Items[i] != null)
					{
						tmp_Dropdown.DestroyItem(tmp_Dropdown.m_Items[i]);
					}
					tmp_Dropdown.m_Items.Clear();
				}
				if (tmp_Dropdown.m_Dropdown != null)
				{
					tmp_Dropdown.DestroyDropdownList(tmp_Dropdown.m_Dropdown);
				}
				tmp_Dropdown.m_Dropdown = null;
				return false;
			}

			// Token: 0x17000110 RID: 272
			// (get) Token: 0x060004B6 RID: 1206 RVA: 0x0002F5A5 File Offset: 0x0002D7A5
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060004B7 RID: 1207 RVA: 0x0002F3C7 File Offset: 0x0002D5C7
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000111 RID: 273
			// (get) Token: 0x060004B8 RID: 1208 RVA: 0x0002F5A5 File Offset: 0x0002D7A5
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x04000412 RID: 1042
			private int <>1__state;

			// Token: 0x04000413 RID: 1043
			private object <>2__current;

			// Token: 0x04000414 RID: 1044
			public float delay;

			// Token: 0x04000415 RID: 1045
			public TMP_Dropdown <>4__this;
		}
	}
}
