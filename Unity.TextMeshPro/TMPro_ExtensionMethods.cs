﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TMPro
{
	// Token: 0x02000046 RID: 70
	public static class TMPro_ExtensionMethods
	{
		// Token: 0x06000457 RID: 1111 RVA: 0x0002D7C0 File Offset: 0x0002B9C0
		public static string ArrayToString(this char[] chars)
		{
			string text = string.Empty;
			int num = 0;
			while (num < chars.Length && chars[num] != '\0')
			{
				text += chars[num].ToString();
				num++;
			}
			return text;
		}

		// Token: 0x06000458 RID: 1112 RVA: 0x0002D7FC File Offset: 0x0002B9FC
		public static string IntToString(this int[] unicodes)
		{
			char[] array = new char[unicodes.Length];
			for (int i = 0; i < unicodes.Length; i++)
			{
				array[i] = (char)unicodes[i];
			}
			return new string(array);
		}

		// Token: 0x06000459 RID: 1113 RVA: 0x0002D830 File Offset: 0x0002BA30
		public static string IntToString(this int[] unicodes, int start, int length)
		{
			char[] array = new char[length];
			int num = start + length;
			int num2 = start;
			while (num2 < num && num2 < unicodes.Length)
			{
				array[num2] = (char)unicodes[num2];
				num2++;
			}
			return new string(array, start, length);
		}

		// Token: 0x0600045A RID: 1114 RVA: 0x0002D86C File Offset: 0x0002BA6C
		public static int FindInstanceID<T>(this List<T> list, T target) where T : Object
		{
			int instanceID = target.GetInstanceID();
			for (int i = 0; i < list.Count; i++)
			{
				if (list[i].GetInstanceID() == instanceID)
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x0600045B RID: 1115 RVA: 0x0002D8AD File Offset: 0x0002BAAD
		public static bool Compare(this Color32 a, Color32 b)
		{
			return a.r == b.r && a.g == b.g && a.b == b.b && a.a == b.a;
		}

		// Token: 0x0600045C RID: 1116 RVA: 0x0002D8E9 File Offset: 0x0002BAE9
		public static bool CompareRGB(this Color32 a, Color32 b)
		{
			return a.r == b.r && a.g == b.g && a.b == b.b;
		}

		// Token: 0x0600045D RID: 1117 RVA: 0x0002D917 File Offset: 0x0002BB17
		public static bool Compare(this Color a, Color b)
		{
			return a.r == b.r && a.g == b.g && a.b == b.b && a.a == b.a;
		}

		// Token: 0x0600045E RID: 1118 RVA: 0x0002D953 File Offset: 0x0002BB53
		public static bool CompareRGB(this Color a, Color b)
		{
			return a.r == b.r && a.g == b.g && a.b == b.b;
		}

		// Token: 0x0600045F RID: 1119 RVA: 0x0002D984 File Offset: 0x0002BB84
		public static Color32 Multiply(this Color32 c1, Color32 c2)
		{
			byte r = (byte)((float)c1.r / 255f * ((float)c2.r / 255f) * 255f);
			byte g = (byte)((float)c1.g / 255f * ((float)c2.g / 255f) * 255f);
			byte b = (byte)((float)c1.b / 255f * ((float)c2.b / 255f) * 255f);
			byte a = (byte)((float)c1.a / 255f * ((float)c2.a / 255f) * 255f);
			return new Color32(r, g, b, a);
		}

		// Token: 0x06000460 RID: 1120 RVA: 0x0002DA24 File Offset: 0x0002BC24
		public static Color32 Tint(this Color32 c1, Color32 c2)
		{
			byte r = (byte)((float)c1.r / 255f * ((float)c2.r / 255f) * 255f);
			byte g = (byte)((float)c1.g / 255f * ((float)c2.g / 255f) * 255f);
			byte b = (byte)((float)c1.b / 255f * ((float)c2.b / 255f) * 255f);
			byte a = (byte)((float)c1.a / 255f * ((float)c2.a / 255f) * 255f);
			return new Color32(r, g, b, a);
		}

		// Token: 0x06000461 RID: 1121 RVA: 0x0002DAC4 File Offset: 0x0002BCC4
		public static Color32 Tint(this Color32 c1, float tint)
		{
			byte r = (byte)Mathf.Clamp((float)c1.r / 255f * tint * 255f, 0f, 255f);
			byte g = (byte)Mathf.Clamp((float)c1.g / 255f * tint * 255f, 0f, 255f);
			byte b = (byte)Mathf.Clamp((float)c1.b / 255f * tint * 255f, 0f, 255f);
			byte a = (byte)Mathf.Clamp((float)c1.a / 255f * tint * 255f, 0f, 255f);
			return new Color32(r, g, b, a);
		}

		// Token: 0x06000462 RID: 1122 RVA: 0x0002DB70 File Offset: 0x0002BD70
		public static bool Compare(this Vector3 v1, Vector3 v2, int accuracy)
		{
			bool flag = (int)(v1.x * (float)accuracy) == (int)(v2.x * (float)accuracy);
			bool flag2 = (int)(v1.y * (float)accuracy) == (int)(v2.y * (float)accuracy);
			bool flag3 = (int)(v1.z * (float)accuracy) == (int)(v2.z * (float)accuracy);
			return flag && flag2 && flag3;
		}

		// Token: 0x06000463 RID: 1123 RVA: 0x0002DBC8 File Offset: 0x0002BDC8
		public static bool Compare(this Quaternion q1, Quaternion q2, int accuracy)
		{
			bool flag = (int)(q1.x * (float)accuracy) == (int)(q2.x * (float)accuracy);
			bool flag2 = (int)(q1.y * (float)accuracy) == (int)(q2.y * (float)accuracy);
			bool flag3 = (int)(q1.z * (float)accuracy) == (int)(q2.z * (float)accuracy);
			bool flag4 = (int)(q1.w * (float)accuracy) == (int)(q2.w * (float)accuracy);
			return flag && flag2 && flag3 && flag4;
		}
	}
}
