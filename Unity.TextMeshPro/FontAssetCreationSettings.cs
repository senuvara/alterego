﻿using System;

namespace TMPro
{
	// Token: 0x0200004A RID: 74
	[Serializable]
	public struct FontAssetCreationSettings
	{
		// Token: 0x04000305 RID: 773
		public string sourceFontFileName;

		// Token: 0x04000306 RID: 774
		public string sourceFontFileGUID;

		// Token: 0x04000307 RID: 775
		public int pointSizeSamplingMode;

		// Token: 0x04000308 RID: 776
		public int pointSize;

		// Token: 0x04000309 RID: 777
		public int padding;

		// Token: 0x0400030A RID: 778
		public int packingMode;

		// Token: 0x0400030B RID: 779
		public int atlasWidth;

		// Token: 0x0400030C RID: 780
		public int atlasHeight;

		// Token: 0x0400030D RID: 781
		public int characterSetSelectionMode;

		// Token: 0x0400030E RID: 782
		public string characterSequence;

		// Token: 0x0400030F RID: 783
		public string referencedFontAssetGUID;

		// Token: 0x04000310 RID: 784
		public string referencedTextAssetGUID;

		// Token: 0x04000311 RID: 785
		public int fontStyle;

		// Token: 0x04000312 RID: 786
		public float fontStyleModifier;

		// Token: 0x04000313 RID: 787
		public int renderMode;

		// Token: 0x04000314 RID: 788
		public bool includeFontFeatures;
	}
}
