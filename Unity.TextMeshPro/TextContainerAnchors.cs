﻿using System;

namespace TMPro
{
	// Token: 0x02000008 RID: 8
	public enum TextContainerAnchors
	{
		// Token: 0x04000018 RID: 24
		TopLeft,
		// Token: 0x04000019 RID: 25
		Top,
		// Token: 0x0400001A RID: 26
		TopRight,
		// Token: 0x0400001B RID: 27
		Left,
		// Token: 0x0400001C RID: 28
		Middle,
		// Token: 0x0400001D RID: 29
		Right,
		// Token: 0x0400001E RID: 30
		BottomLeft,
		// Token: 0x0400001F RID: 31
		Bottom,
		// Token: 0x04000020 RID: 32
		BottomRight,
		// Token: 0x04000021 RID: 33
		Custom
	}
}
