﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TMPro
{
	// Token: 0x02000028 RID: 40
	[Serializable]
	public class TMP_StyleSheet : ScriptableObject
	{
		// Token: 0x17000092 RID: 146
		// (get) Token: 0x06000295 RID: 661 RVA: 0x0001E864 File Offset: 0x0001CA64
		public static TMP_StyleSheet instance
		{
			get
			{
				if (TMP_StyleSheet.s_Instance == null)
				{
					TMP_StyleSheet.s_Instance = TMP_Settings.defaultStyleSheet;
					if (TMP_StyleSheet.s_Instance == null)
					{
						TMP_StyleSheet.s_Instance = Resources.Load<TMP_StyleSheet>("Style Sheets/TMP Default Style Sheet");
					}
					if (TMP_StyleSheet.s_Instance == null)
					{
						return null;
					}
					TMP_StyleSheet.s_Instance.LoadStyleDictionaryInternal();
				}
				return TMP_StyleSheet.s_Instance;
			}
		}

		// Token: 0x06000296 RID: 662 RVA: 0x0001E8C2 File Offset: 0x0001CAC2
		public static TMP_StyleSheet LoadDefaultStyleSheet()
		{
			return TMP_StyleSheet.instance;
		}

		// Token: 0x06000297 RID: 663 RVA: 0x0001E8C9 File Offset: 0x0001CAC9
		public static TMP_Style GetStyle(int hashCode)
		{
			return TMP_StyleSheet.instance.GetStyleInternal(hashCode);
		}

		// Token: 0x06000298 RID: 664 RVA: 0x0001E8D8 File Offset: 0x0001CAD8
		private TMP_Style GetStyleInternal(int hashCode)
		{
			TMP_Style result;
			if (this.m_StyleDictionary.TryGetValue(hashCode, out result))
			{
				return result;
			}
			return null;
		}

		// Token: 0x06000299 RID: 665 RVA: 0x0001E8F8 File Offset: 0x0001CAF8
		public void UpdateStyleDictionaryKey(int old_key, int new_key)
		{
			if (this.m_StyleDictionary.ContainsKey(old_key))
			{
				TMP_Style value = this.m_StyleDictionary[old_key];
				this.m_StyleDictionary.Add(new_key, value);
				this.m_StyleDictionary.Remove(old_key);
			}
		}

		// Token: 0x0600029A RID: 666 RVA: 0x0001E93A File Offset: 0x0001CB3A
		public static void UpdateStyleSheet()
		{
			TMP_StyleSheet.s_Instance = null;
			TMP_StyleSheet.RefreshStyles();
		}

		// Token: 0x0600029B RID: 667 RVA: 0x0001E947 File Offset: 0x0001CB47
		public static void RefreshStyles()
		{
			TMP_StyleSheet.instance.LoadStyleDictionaryInternal();
		}

		// Token: 0x0600029C RID: 668 RVA: 0x0001E954 File Offset: 0x0001CB54
		private void LoadStyleDictionaryInternal()
		{
			this.m_StyleDictionary.Clear();
			for (int i = 0; i < this.m_StyleList.Count; i++)
			{
				this.m_StyleList[i].RefreshStyle();
				if (!this.m_StyleDictionary.ContainsKey(this.m_StyleList[i].hashCode))
				{
					this.m_StyleDictionary.Add(this.m_StyleList[i].hashCode, this.m_StyleList[i]);
				}
			}
		}

		// Token: 0x0600029D RID: 669 RVA: 0x0001E9D9 File Offset: 0x0001CBD9
		public TMP_StyleSheet()
		{
		}

		// Token: 0x04000141 RID: 321
		private static TMP_StyleSheet s_Instance;

		// Token: 0x04000142 RID: 322
		[SerializeField]
		private List<TMP_Style> m_StyleList = new List<TMP_Style>(1);

		// Token: 0x04000143 RID: 323
		private Dictionary<int, TMP_Style> m_StyleDictionary = new Dictionary<int, TMP_Style>();
	}
}
