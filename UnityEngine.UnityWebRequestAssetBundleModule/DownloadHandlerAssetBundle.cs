﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;

namespace UnityEngine.Networking
{
	// Token: 0x02000003 RID: 3
	[NativeHeader("Modules/UnityWebRequestAssetBundle/Public/DownloadHandlerAssetBundle.h")]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class DownloadHandlerAssetBundle : DownloadHandler
	{
		// Token: 0x0600000B RID: 11 RVA: 0x000021F8 File Offset: 0x000003F8
		public DownloadHandlerAssetBundle(string url, uint crc)
		{
			this.InternalCreateAssetBundle(url, crc);
		}

		// Token: 0x0600000C RID: 12 RVA: 0x00002209 File Offset: 0x00000409
		public DownloadHandlerAssetBundle(string url, uint version, uint crc)
		{
			this.InternalCreateAssetBundleCached(url, "", new Hash128(0U, 0U, 0U, version), crc);
		}

		// Token: 0x0600000D RID: 13 RVA: 0x00002228 File Offset: 0x00000428
		public DownloadHandlerAssetBundle(string url, Hash128 hash, uint crc)
		{
			this.InternalCreateAssetBundleCached(url, "", hash, crc);
		}

		// Token: 0x0600000E RID: 14 RVA: 0x0000223F File Offset: 0x0000043F
		public DownloadHandlerAssetBundle(string url, string name, Hash128 hash, uint crc)
		{
			this.InternalCreateAssetBundleCached(url, name, hash, crc);
		}

		// Token: 0x0600000F RID: 15 RVA: 0x00002253 File Offset: 0x00000453
		public DownloadHandlerAssetBundle(string url, CachedAssetBundle cachedBundle, uint crc)
		{
			this.InternalCreateAssetBundleCached(url, cachedBundle.name, cachedBundle.hash, crc);
		}

		// Token: 0x06000010 RID: 16
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr Create(DownloadHandlerAssetBundle obj, string url, uint crc);

		// Token: 0x06000011 RID: 17 RVA: 0x00002272 File Offset: 0x00000472
		private static IntPtr CreateCached(DownloadHandlerAssetBundle obj, string url, string name, Hash128 hash, uint crc)
		{
			return DownloadHandlerAssetBundle.CreateCached_Injected(obj, url, name, ref hash, crc);
		}

		// Token: 0x06000012 RID: 18 RVA: 0x00002280 File Offset: 0x00000480
		private void InternalCreateAssetBundle(string url, uint crc)
		{
			this.m_Ptr = DownloadHandlerAssetBundle.Create(this, url, crc);
		}

		// Token: 0x06000013 RID: 19 RVA: 0x00002291 File Offset: 0x00000491
		private void InternalCreateAssetBundleCached(string url, string name, Hash128 hash, uint crc)
		{
			this.m_Ptr = DownloadHandlerAssetBundle.CreateCached(this, url, name, hash, crc);
		}

		// Token: 0x06000014 RID: 20 RVA: 0x000022A5 File Offset: 0x000004A5
		protected override byte[] GetData()
		{
			throw new NotSupportedException("Raw data access is not supported for asset bundles");
		}

		// Token: 0x06000015 RID: 21 RVA: 0x000022B2 File Offset: 0x000004B2
		protected override string GetText()
		{
			throw new NotSupportedException("String access is not supported for asset bundles");
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000016 RID: 22
		public extern AssetBundle assetBundle { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000017 RID: 23 RVA: 0x000022C0 File Offset: 0x000004C0
		public static AssetBundle GetContent(UnityWebRequest www)
		{
			return DownloadHandler.GetCheckedDownloader<DownloadHandlerAssetBundle>(www).assetBundle;
		}

		// Token: 0x06000018 RID: 24
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr CreateCached_Injected(DownloadHandlerAssetBundle obj, string url, string name, ref Hash128 hash, uint crc);
	}
}
