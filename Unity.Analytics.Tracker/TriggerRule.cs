﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x02000019 RID: 25
	[Serializable]
	public class TriggerRule
	{
		// Token: 0x0600005D RID: 93 RVA: 0x00002F34 File Offset: 0x00001134
		public TriggerRule()
		{
		}

		// Token: 0x0600005E RID: 94 RVA: 0x00002F3C File Offset: 0x0000113C
		public bool Test()
		{
			bool flag;
			string text;
			return this.Test(out flag, out text);
		}

		// Token: 0x0600005F RID: 95 RVA: 0x00002F54 File Offset: 0x00001154
		public bool Test(out bool error, out string message)
		{
			error = false;
			message = null;
			if (this.m_Target == null || this.m_Target.GetValue() == null)
			{
				error = true;
				message = "rule target is not set";
				return false;
			}
			if (this.m_Value == null || this.m_Value.target == null || this.m_Value.propertyValue == null)
			{
				error = true;
				message = "rule value is not set";
				return false;
			}
			object value = this.m_Target.GetValue();
			string valueType = this.m_Value.valueType;
			if (valueType == typeof(string).ToString())
			{
				return this.TestByString((string)value);
			}
			if (valueType == typeof(bool).ToString())
			{
				return this.TestByBool((bool)value);
			}
			if (valueType == typeof(float).ToString())
			{
				return this.TestByDouble((double)((float)value));
			}
			if (valueType == typeof(double).ToString())
			{
				return this.TestByDouble((double)value);
			}
			if (valueType == typeof(decimal).ToString())
			{
				return this.TestByDouble((double)((decimal)value));
			}
			if (valueType == typeof(int).ToString())
			{
				return this.TestByDouble((double)((int)value));
			}
			if (valueType == typeof(short).ToString())
			{
				return this.TestByDouble((double)((short)value));
			}
			if (valueType == typeof(long).ToString())
			{
				return this.TestByDouble((double)((long)value));
			}
			if (valueType == "enum")
			{
				return this.TestByEnum(((Enum)value).ToString().ToLower());
			}
			return this.TestByObject(value);
		}

		// Token: 0x06000060 RID: 96 RVA: 0x0000314C File Offset: 0x0000134C
		private bool TestByObject(object currentValue)
		{
			bool result = false;
			TriggerOperator @operator = this.m_Operator;
			if (@operator != TriggerOperator.Equals)
			{
				if (@operator == TriggerOperator.DoesNotEqual)
				{
					result = !currentValue.Equals(this.m_Value.propertyValue);
				}
			}
			else
			{
				result = currentValue.Equals(this.m_Value.propertyValue);
			}
			return result;
		}

		// Token: 0x06000061 RID: 97 RVA: 0x000031A8 File Offset: 0x000013A8
		private bool TestByEnum(string currentValue)
		{
			bool result = false;
			TriggerOperator @operator = this.m_Operator;
			if (@operator != TriggerOperator.Equals)
			{
				if (@operator == TriggerOperator.DoesNotEqual)
				{
					result = !currentValue.Equals(this.m_Value.propertyValue.ToString().ToLower());
				}
			}
			else
			{
				result = currentValue.Equals(this.m_Value.propertyValue.ToString().ToLower());
			}
			return result;
		}

		// Token: 0x06000062 RID: 98 RVA: 0x00003218 File Offset: 0x00001418
		private bool TestByString(string currentValue)
		{
			bool result = false;
			TriggerOperator @operator = this.m_Operator;
			if (@operator != TriggerOperator.Equals)
			{
				if (@operator == TriggerOperator.DoesNotEqual)
				{
					result = !currentValue.Equals(this.m_Value.propertyValue);
				}
			}
			else
			{
				result = currentValue.Equals(this.m_Value.propertyValue);
			}
			return result;
		}

		// Token: 0x06000063 RID: 99 RVA: 0x00003274 File Offset: 0x00001474
		private bool TestByBool(bool currentValue)
		{
			bool result = false;
			bool obj = bool.Parse(this.m_Value.propertyValue);
			TriggerOperator @operator = this.m_Operator;
			if (@operator != TriggerOperator.Equals)
			{
				if (@operator == TriggerOperator.DoesNotEqual)
				{
					result = !currentValue.Equals(obj);
				}
			}
			else
			{
				result = currentValue.Equals(obj);
			}
			return result;
		}

		// Token: 0x06000064 RID: 100 RVA: 0x000032D0 File Offset: 0x000014D0
		private bool TestByDouble(double currentValue)
		{
			bool result = false;
			double @double = this.GetDouble(this.m_Value.propertyValue);
			switch (this.m_Operator)
			{
			case TriggerOperator.Equals:
				result = this.SafeEquals(currentValue, @double);
				break;
			case TriggerOperator.DoesNotEqual:
				result = !this.SafeEquals(currentValue, @double);
				break;
			case TriggerOperator.IsGreaterThan:
				result = (currentValue > @double);
				break;
			case TriggerOperator.IsGreaterThanOrEqualTo:
				result = (currentValue > @double || this.SafeEquals(currentValue, @double));
				break;
			case TriggerOperator.IsLessThan:
				result = (currentValue < @double);
				break;
			case TriggerOperator.IsLessThanOrEqualTo:
				result = (currentValue < @double || this.SafeEquals(currentValue, @double));
				break;
			case TriggerOperator.IsBetween:
			{
				double double2 = this.GetDouble(this.m_Value2.propertyValue);
				result = (currentValue > @double && currentValue < double2);
				break;
			}
			case TriggerOperator.IsBetweenOrEqualTo:
			{
				double double3 = this.GetDouble(this.m_Value2.propertyValue);
				result = (this.SafeEquals(currentValue, @double) || this.SafeEquals(currentValue, double3) || (currentValue > @double && currentValue < double3));
				break;
			}
			}
			return result;
		}

		// Token: 0x06000065 RID: 101 RVA: 0x000033F3 File Offset: 0x000015F3
		private bool SafeEquals(double double1, double double2)
		{
			return Math.Abs(double1 - double2) < 1E-07;
		}

		// Token: 0x06000066 RID: 102 RVA: 0x00003408 File Offset: 0x00001608
		private double GetDouble(object value)
		{
			return double.Parse(value.ToString());
		}

		// Token: 0x04000068 RID: 104
		[SerializeField]
		private TrackableField m_Target;

		// Token: 0x04000069 RID: 105
		[SerializeField]
		private TriggerOperator m_Operator;

		// Token: 0x0400006A RID: 106
		[SerializeField]
		private ValueProperty m_Value;

		// Token: 0x0400006B RID: 107
		[SerializeField]
		private ValueProperty m_Value2;
	}
}
