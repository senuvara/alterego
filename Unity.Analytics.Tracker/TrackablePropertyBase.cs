﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x02000009 RID: 9
	[Serializable]
	public abstract class TrackablePropertyBase
	{
		// Token: 0x06000021 RID: 33 RVA: 0x00002778 File Offset: 0x00000978
		protected TrackablePropertyBase()
		{
		}

		// Token: 0x04000018 RID: 24
		[SerializeField]
		protected Object m_Target;

		// Token: 0x04000019 RID: 25
		[SerializeField]
		protected string m_Path;
	}
}
