﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x02000012 RID: 18
	public enum TriggerOperator
	{
		// Token: 0x0400004C RID: 76
		Equals,
		// Token: 0x0400004D RID: 77
		DoesNotEqual,
		// Token: 0x0400004E RID: 78
		IsGreaterThan,
		// Token: 0x0400004F RID: 79
		IsGreaterThanOrEqualTo,
		// Token: 0x04000050 RID: 80
		IsLessThan,
		// Token: 0x04000051 RID: 81
		IsLessThanOrEqualTo,
		// Token: 0x04000052 RID: 82
		IsBetween,
		// Token: 0x04000053 RID: 83
		IsBetweenOrEqualTo
	}
}
