﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Analytics
{
	// Token: 0x02000007 RID: 7
	[Serializable]
	public class StandardEventPayload
	{
		// Token: 0x06000016 RID: 22 RVA: 0x00002417 File Offset: 0x00000617
		public StandardEventPayload()
		{
			this.m_Parameters = new AnalyticsEventParamListContainer();
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000017 RID: 23 RVA: 0x00002447 File Offset: 0x00000647
		public AnalyticsEventParamListContainer parameters
		{
			get
			{
				return this.m_Parameters;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000018 RID: 24 RVA: 0x0000244F File Offset: 0x0000064F
		// (set) Token: 0x06000019 RID: 25 RVA: 0x00002457 File Offset: 0x00000657
		public string name
		{
			get
			{
				return this.m_Name;
			}
			set
			{
				this.m_Name = value;
			}
		}

		// Token: 0x0600001A RID: 26 RVA: 0x00002460 File Offset: 0x00000660
		public virtual AnalyticsResult Send()
		{
			if (string.IsNullOrEmpty(this.name))
			{
				throw new Exception("Analtyics Event Tracker failed to send the CustomEvent. The event Name field cannot be empty.");
			}
			if (!this.IsCustomDataValid())
			{
				throw new Exception("Analytics event tracker failed to send. The event data is not valid. Parameter names cannot be null or empty.");
			}
			if (!this.IsRequiredDataValid())
			{
				throw new Exception("Analytics event tracker failed to send. The event data is not valid. Please check the values of required parameters.");
			}
			return AnalyticsEvent.Custom(this.name.Trim(), this.GetParameters());
		}

		// Token: 0x0600001B RID: 27 RVA: 0x000024CC File Offset: 0x000006CC
		private IDictionary<string, object> GetParameters()
		{
			StandardEventPayload.m_EventData.Clear();
			List<AnalyticsEventParam> parameters = this.parameters.parameters;
			for (int i = 0; i < parameters.Count; i++)
			{
				if (parameters[i] != null && parameters[i].valueProperty.IsValid())
				{
					StandardEventPayload.m_EventData.Add(parameters[i].name, parameters[i].value);
				}
			}
			return StandardEventPayload.m_EventData;
		}

		// Token: 0x0600001C RID: 28 RVA: 0x00002550 File Offset: 0x00000750
		private bool IsCustomDataValid()
		{
			List<AnalyticsEventParam> parameters = this.parameters.parameters;
			for (int i = 0; i < parameters.Count; i++)
			{
				if (parameters[i] != null)
				{
					if (string.IsNullOrEmpty(parameters[i].name) && parameters[i].valueProperty.IsValid())
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x0600001D RID: 29 RVA: 0x000025C0 File Offset: 0x000007C0
		private bool IsRequiredDataValid()
		{
			List<AnalyticsEventParam> parameters = this.parameters.parameters;
			Dictionary<string, List<bool>> dictionary = new Dictionary<string, List<bool>>();
			for (int i = 0; i < parameters.Count; i++)
			{
				if (parameters[i] != null && parameters[i].requirementType == AnalyticsEventParam.RequirementType.Required)
				{
					if (string.IsNullOrEmpty(parameters[i].groupID))
					{
						if (!dictionary.ContainsKey("none"))
						{
							dictionary.Add("none", new List<bool>());
						}
						dictionary["none"].Add(parameters[i].valueProperty.IsValid());
					}
					else
					{
						if (!dictionary.ContainsKey(parameters[i].groupID))
						{
							dictionary.Add(parameters[i].groupID, new List<bool>());
						}
						dictionary[parameters[i].groupID].Add(parameters[i].valueProperty.IsValid());
					}
				}
			}
			foreach (string text in dictionary.Keys)
			{
				if (text != null)
				{
					if (text == "none")
					{
						if (dictionary[text].Contains(false))
						{
							return false;
						}
						continue;
					}
				}
				if (!dictionary[text].Contains(true))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x0600001E RID: 30 RVA: 0x0000276C File Offset: 0x0000096C
		// Note: this type is marked as 'beforefieldinit'.
		static StandardEventPayload()
		{
		}

		// Token: 0x0400000F RID: 15
		[SerializeField]
		private bool m_IsEventExpanded = true;

		// Token: 0x04000010 RID: 16
		[SerializeField]
		private string m_StandardEventType = "CustomEvent";

		// Token: 0x04000011 RID: 17
		public Type standardEventType;

		// Token: 0x04000012 RID: 18
		[SerializeField]
		private AnalyticsEventParamListContainer m_Parameters;

		// Token: 0x04000013 RID: 19
		private static Dictionary<string, object> m_EventData = new Dictionary<string, object>();

		// Token: 0x04000014 RID: 20
		[SerializeField]
		private string m_Name = string.Empty;
	}
}
