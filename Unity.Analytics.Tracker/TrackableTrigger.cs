﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x02000017 RID: 23
	[Serializable]
	public class TrackableTrigger
	{
		// Token: 0x0600005B RID: 91 RVA: 0x00002F24 File Offset: 0x00001124
		public TrackableTrigger()
		{
		}

		// Token: 0x04000066 RID: 102
		[SerializeField]
		private GameObject m_Target;

		// Token: 0x04000067 RID: 103
		[SerializeField]
		private string m_MethodPath;
	}
}
