﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Analytics
{
	// Token: 0x0200000C RID: 12
	[AddComponentMenu("")]
	[Obsolete("Analytics Tracker is deprecated. Use Analytics Event Tracker instead!")]
	public class AnalyticsTracker : MonoBehaviour
	{
		// Token: 0x06000028 RID: 40 RVA: 0x000029A2 File Offset: 0x00000BA2
		public AnalyticsTracker()
		{
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000029 RID: 41 RVA: 0x000029C0 File Offset: 0x00000BC0
		// (set) Token: 0x0600002A RID: 42 RVA: 0x000029C8 File Offset: 0x00000BC8
		public string eventName
		{
			get
			{
				return this.m_EventName;
			}
			set
			{
				this.m_EventName = value;
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x0600002B RID: 43 RVA: 0x000029D1 File Offset: 0x00000BD1
		// (set) Token: 0x0600002C RID: 44 RVA: 0x000029D9 File Offset: 0x00000BD9
		internal TrackableProperty TP
		{
			get
			{
				return this.m_TrackableProperty;
			}
			set
			{
				this.m_TrackableProperty = value;
			}
		}

		// Token: 0x0600002D RID: 45 RVA: 0x000029E2 File Offset: 0x00000BE2
		private void Awake()
		{
			if (this.m_Trigger == AnalyticsTracker.Trigger.Awake)
			{
				this.TriggerEvent();
			}
		}

		// Token: 0x0600002E RID: 46 RVA: 0x000029F6 File Offset: 0x00000BF6
		private void Start()
		{
			if (this.m_Trigger == AnalyticsTracker.Trigger.Start)
			{
				this.TriggerEvent();
			}
		}

		// Token: 0x0600002F RID: 47 RVA: 0x00002A0A File Offset: 0x00000C0A
		private void OnEnable()
		{
			if (this.m_Trigger == AnalyticsTracker.Trigger.OnEnable)
			{
				this.TriggerEvent();
			}
		}

		// Token: 0x06000030 RID: 48 RVA: 0x00002A1E File Offset: 0x00000C1E
		private void OnDisable()
		{
			if (this.m_Trigger == AnalyticsTracker.Trigger.OnDisable)
			{
				this.TriggerEvent();
			}
		}

		// Token: 0x06000031 RID: 49 RVA: 0x00002A32 File Offset: 0x00000C32
		private void OnApplicationPause()
		{
			if (this.m_Trigger == AnalyticsTracker.Trigger.OnApplicationPause)
			{
				this.TriggerEvent();
			}
		}

		// Token: 0x06000032 RID: 50 RVA: 0x00002A46 File Offset: 0x00000C46
		private void OnDestroy()
		{
			if (this.m_Trigger == AnalyticsTracker.Trigger.OnDestroy)
			{
				this.TriggerEvent();
			}
		}

		// Token: 0x06000033 RID: 51 RVA: 0x00002A5A File Offset: 0x00000C5A
		public void TriggerEvent()
		{
			this.BuildParameters();
			this.SendEvent();
		}

		// Token: 0x06000034 RID: 52 RVA: 0x00002A68 File Offset: 0x00000C68
		private void SendEvent()
		{
			Analytics.CustomEvent(this.m_EventName, this.m_Dict);
		}

		// Token: 0x06000035 RID: 53 RVA: 0x00002A7C File Offset: 0x00000C7C
		private void BuildParameters()
		{
			int hashCode = this.m_TrackableProperty.GetHashCode();
			if (hashCode != this.m_PrevDictHash)
			{
				this.m_Dict.Clear();
			}
			this.m_PrevDictHash = hashCode;
			int i = 0;
			int count = this.m_TrackableProperty.fields.Count;
			while (i < count)
			{
				TrackableProperty.FieldWithTarget fieldWithTarget = this.m_TrackableProperty.fields[i];
				if (fieldWithTarget.target != null || fieldWithTarget.doStatic)
				{
					string value = fieldWithTarget.GetValue().ToString();
					this.m_Dict[fieldWithTarget.paramName] = value;
				}
				i++;
			}
		}

		// Token: 0x04000029 RID: 41
		[SerializeField]
		private string m_EventName;

		// Token: 0x0400002A RID: 42
		private Dictionary<string, object> m_Dict = new Dictionary<string, object>();

		// Token: 0x0400002B RID: 43
		private int m_PrevDictHash;

		// Token: 0x0400002C RID: 44
		[SerializeField]
		private TrackableProperty m_TrackableProperty = new TrackableProperty();

		// Token: 0x0400002D RID: 45
		[SerializeField]
		internal AnalyticsTracker.Trigger m_Trigger;

		// Token: 0x0200000D RID: 13
		[Serializable]
		internal enum Trigger
		{
			// Token: 0x0400002F RID: 47
			External,
			// Token: 0x04000030 RID: 48
			Awake,
			// Token: 0x04000031 RID: 49
			Start,
			// Token: 0x04000032 RID: 50
			OnEnable,
			// Token: 0x04000033 RID: 51
			OnDisable,
			// Token: 0x04000034 RID: 52
			OnApplicationPause,
			// Token: 0x04000035 RID: 53
			OnDestroy
		}
	}
}
