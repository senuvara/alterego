﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x02000011 RID: 17
	public enum TriggerLifecycleEvent
	{
		// Token: 0x04000043 RID: 67
		None,
		// Token: 0x04000044 RID: 68
		Awake,
		// Token: 0x04000045 RID: 69
		Start,
		// Token: 0x04000046 RID: 70
		OnEnable,
		// Token: 0x04000047 RID: 71
		OnDisable,
		// Token: 0x04000048 RID: 72
		OnApplicationPause,
		// Token: 0x04000049 RID: 73
		OnApplicationUnpause,
		// Token: 0x0400004A RID: 74
		OnDestroy
	}
}
