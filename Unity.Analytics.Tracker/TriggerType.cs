﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x02000013 RID: 19
	public enum TriggerType
	{
		// Token: 0x04000055 RID: 85
		Lifecycle,
		// Token: 0x04000056 RID: 86
		External,
		// Token: 0x04000057 RID: 87
		Timer,
		// Token: 0x04000058 RID: 88
		ExposedMethod
	}
}
