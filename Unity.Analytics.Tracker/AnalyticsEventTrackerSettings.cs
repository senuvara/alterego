﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x02000003 RID: 3
	public static class AnalyticsEventTrackerSettings
	{
		// Token: 0x0600000C RID: 12 RVA: 0x00002398 File Offset: 0x00000598
		// Note: this type is marked as 'beforefieldinit'.
		static AnalyticsEventTrackerSettings()
		{
		}

		// Token: 0x04000003 RID: 3
		public static readonly int paramCountMax = 10;

		// Token: 0x04000004 RID: 4
		public static readonly int triggerRuleCountMax = 5;
	}
}
