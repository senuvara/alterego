﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Analytics
{
	// Token: 0x02000006 RID: 6
	[Serializable]
	public class AnalyticsEventParamListContainer
	{
		// Token: 0x06000013 RID: 19 RVA: 0x000023F3 File Offset: 0x000005F3
		public AnalyticsEventParamListContainer()
		{
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000014 RID: 20 RVA: 0x00002406 File Offset: 0x00000606
		// (set) Token: 0x06000015 RID: 21 RVA: 0x0000240E File Offset: 0x0000060E
		public List<AnalyticsEventParam> parameters
		{
			get
			{
				return this.m_Parameters;
			}
			set
			{
				this.m_Parameters = value;
			}
		}

		// Token: 0x0400000E RID: 14
		[SerializeField]
		private List<AnalyticsEventParam> m_Parameters = new List<AnalyticsEventParam>();
	}
}
