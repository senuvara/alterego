﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x02000015 RID: 21
	[Serializable]
	public class EventTrigger
	{
		// Token: 0x0600004B RID: 75 RVA: 0x00002CDF File Offset: 0x00000EDF
		public EventTrigger()
		{
			this.m_Rules = new TriggerListContainer();
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x0600004C RID: 76 RVA: 0x00002D0F File Offset: 0x00000F0F
		public TriggerType triggerType
		{
			get
			{
				return this.m_Type;
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x0600004D RID: 77 RVA: 0x00002D17 File Offset: 0x00000F17
		public TriggerLifecycleEvent lifecycleEvent
		{
			get
			{
				return this.m_LifecycleEvent;
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x0600004E RID: 78 RVA: 0x00002D1F File Offset: 0x00000F1F
		// (set) Token: 0x0600004F RID: 79 RVA: 0x00002D27 File Offset: 0x00000F27
		public float initTime
		{
			get
			{
				return this.m_InitTime;
			}
			set
			{
				this.m_InitTime = value;
			}
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000050 RID: 80 RVA: 0x00002D30 File Offset: 0x00000F30
		// (set) Token: 0x06000051 RID: 81 RVA: 0x00002D38 File Offset: 0x00000F38
		public float repeatTime
		{
			get
			{
				return this.m_RepeatTime;
			}
			set
			{
				this.m_RepeatTime = value;
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000052 RID: 82 RVA: 0x00002D41 File Offset: 0x00000F41
		// (set) Token: 0x06000053 RID: 83 RVA: 0x00002D49 File Offset: 0x00000F49
		public int repetitions
		{
			get
			{
				return this.m_Repetitions;
			}
			set
			{
				this.m_Repetitions = value;
			}
		}

		// Token: 0x06000054 RID: 84 RVA: 0x00002D54 File Offset: 0x00000F54
		public void AddRule()
		{
			TriggerRule item = new TriggerRule();
			this.m_Rules.rules.Add(item);
		}

		// Token: 0x06000055 RID: 85 RVA: 0x00002D78 File Offset: 0x00000F78
		public void RemoveRule(int index)
		{
			this.m_Rules.rules.RemoveAt(index);
		}

		// Token: 0x06000056 RID: 86 RVA: 0x00002D8C File Offset: 0x00000F8C
		public bool Test(GameObject gameObject = null)
		{
			if (!this.m_ApplyRules)
			{
				return true;
			}
			if (this.repetitions > 0 && this.repetitionCount >= this.repetitions)
			{
				return false;
			}
			bool flag = false;
			int num = 0;
			int num2 = 0;
			foreach (TriggerRule triggerRule in this.m_Rules.rules)
			{
				num2++;
				bool flag2;
				string text;
				if (triggerRule.Test(out flag2, out text))
				{
					num++;
				}
				else if (flag2)
				{
					Debug.LogWarningFormat("Event trigger rule {0}{2} is incomplete ({1}). Result is false.", new object[]
					{
						num2,
						text,
						(!(gameObject == null)) ? string.Format(" on GameObject '{0}'", gameObject.name) : null
					});
				}
				TriggerBool triggerBool = this.m_TriggerBool;
				if (triggerBool != TriggerBool.All)
				{
					if (triggerBool != TriggerBool.None)
					{
						if (triggerBool == TriggerBool.Any)
						{
							if (num > 0)
							{
								flag = true;
							}
						}
					}
					else if (num > 0)
					{
						flag = false;
					}
				}
				else if (num < num2)
				{
					flag = false;
				}
			}
			if ((this.m_TriggerBool == TriggerBool.All && num == num2) || (this.m_TriggerBool == TriggerBool.None && num == 0))
			{
				flag = true;
			}
			if (this.repetitions > 0 && flag)
			{
				this.repetitionCount++;
			}
			return flag;
		}

		// Token: 0x0400005A RID: 90
		[SerializeField]
		private bool m_IsTriggerExpanded = true;

		// Token: 0x0400005B RID: 91
		[SerializeField]
		private TriggerType m_Type;

		// Token: 0x0400005C RID: 92
		[SerializeField]
		private TriggerLifecycleEvent m_LifecycleEvent;

		// Token: 0x0400005D RID: 93
		[SerializeField]
		private bool m_ApplyRules;

		// Token: 0x0400005E RID: 94
		[SerializeField]
		private TriggerListContainer m_Rules;

		// Token: 0x0400005F RID: 95
		[SerializeField]
		private TriggerBool m_TriggerBool;

		// Token: 0x04000060 RID: 96
		[SerializeField]
		private float m_InitTime = 5f;

		// Token: 0x04000061 RID: 97
		[SerializeField]
		private float m_RepeatTime = 5f;

		// Token: 0x04000062 RID: 98
		[SerializeField]
		private int m_Repetitions;

		// Token: 0x04000063 RID: 99
		public int repetitionCount;

		// Token: 0x04000064 RID: 100
		private EventTrigger.OnTrigger m_TriggerFunction;

		// Token: 0x04000065 RID: 101
		[SerializeField]
		private TriggerMethod m_Method;

		// Token: 0x02000016 RID: 22
		// (Invoke) Token: 0x06000058 RID: 88
		internal delegate void OnTrigger();
	}
}
