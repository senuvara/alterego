﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace UnityEngine.Analytics
{
	// Token: 0x0200000E RID: 14
	[Serializable]
	internal class TrackableProperty
	{
		// Token: 0x06000036 RID: 54 RVA: 0x00002B24 File Offset: 0x00000D24
		public TrackableProperty()
		{
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000037 RID: 55 RVA: 0x00002B2C File Offset: 0x00000D2C
		// (set) Token: 0x06000038 RID: 56 RVA: 0x00002B34 File Offset: 0x00000D34
		public List<TrackableProperty.FieldWithTarget> fields
		{
			get
			{
				return this.m_Fields;
			}
			set
			{
				this.m_Fields = value;
			}
		}

		// Token: 0x06000039 RID: 57 RVA: 0x00002B40 File Offset: 0x00000D40
		public override int GetHashCode()
		{
			int num = 17;
			foreach (TrackableProperty.FieldWithTarget fieldWithTarget in this.m_Fields)
			{
				num = num * 23 + fieldWithTarget.paramName.GetHashCode();
			}
			return num;
		}

		// Token: 0x04000036 RID: 54
		public const int kMaxParams = 10;

		// Token: 0x04000037 RID: 55
		[SerializeField]
		private List<TrackableProperty.FieldWithTarget> m_Fields;

		// Token: 0x0200000F RID: 15
		[Serializable]
		internal class FieldWithTarget
		{
			// Token: 0x0600003A RID: 58 RVA: 0x00002BAC File Offset: 0x00000DAC
			public FieldWithTarget()
			{
			}

			// Token: 0x17000010 RID: 16
			// (get) Token: 0x0600003B RID: 59 RVA: 0x00002BB4 File Offset: 0x00000DB4
			// (set) Token: 0x0600003C RID: 60 RVA: 0x00002BBC File Offset: 0x00000DBC
			public string paramName
			{
				get
				{
					return this.m_ParamName;
				}
				set
				{
					this.m_ParamName = value;
				}
			}

			// Token: 0x17000011 RID: 17
			// (get) Token: 0x0600003D RID: 61 RVA: 0x00002BC5 File Offset: 0x00000DC5
			// (set) Token: 0x0600003E RID: 62 RVA: 0x00002BCD File Offset: 0x00000DCD
			public Object target
			{
				get
				{
					return this.m_Target;
				}
				set
				{
					this.m_Target = value;
				}
			}

			// Token: 0x17000012 RID: 18
			// (get) Token: 0x0600003F RID: 63 RVA: 0x00002BD6 File Offset: 0x00000DD6
			// (set) Token: 0x06000040 RID: 64 RVA: 0x00002BDE File Offset: 0x00000DDE
			public string fieldPath
			{
				get
				{
					return this.m_FieldPath;
				}
				set
				{
					this.m_FieldPath = value;
				}
			}

			// Token: 0x17000013 RID: 19
			// (get) Token: 0x06000041 RID: 65 RVA: 0x00002BE7 File Offset: 0x00000DE7
			// (set) Token: 0x06000042 RID: 66 RVA: 0x00002BEF File Offset: 0x00000DEF
			public string typeString
			{
				get
				{
					return this.m_TypeString;
				}
				set
				{
					this.m_TypeString = value;
				}
			}

			// Token: 0x17000014 RID: 20
			// (get) Token: 0x06000043 RID: 67 RVA: 0x00002BF8 File Offset: 0x00000DF8
			// (set) Token: 0x06000044 RID: 68 RVA: 0x00002C00 File Offset: 0x00000E00
			public bool doStatic
			{
				get
				{
					return this.m_DoStatic;
				}
				set
				{
					this.m_DoStatic = value;
				}
			}

			// Token: 0x17000015 RID: 21
			// (get) Token: 0x06000045 RID: 69 RVA: 0x00002C09 File Offset: 0x00000E09
			// (set) Token: 0x06000046 RID: 70 RVA: 0x00002C11 File Offset: 0x00000E11
			public string staticString
			{
				get
				{
					return this.m_StaticString;
				}
				set
				{
					this.m_StaticString = value;
				}
			}

			// Token: 0x06000047 RID: 71 RVA: 0x00002C1C File Offset: 0x00000E1C
			public object GetValue()
			{
				if (this.m_DoStatic)
				{
					return this.m_StaticString;
				}
				object obj = this.m_Target;
				foreach (string name in this.m_FieldPath.Split(new char[]
				{
					'.'
				}))
				{
					Type type = obj.GetType();
					PropertyInfo property = type.GetProperty(name);
					FieldInfo field = type.GetField(name);
					if (property != null)
					{
						obj = property.GetValue(obj, null);
					}
					else
					{
						if (field == null)
						{
							return null;
						}
						obj = field.GetValue(obj);
					}
				}
				return obj;
			}

			// Token: 0x04000038 RID: 56
			[SerializeField]
			private string m_ParamName;

			// Token: 0x04000039 RID: 57
			[SerializeField]
			private Object m_Target;

			// Token: 0x0400003A RID: 58
			[SerializeField]
			private string m_FieldPath;

			// Token: 0x0400003B RID: 59
			[SerializeField]
			private string m_TypeString;

			// Token: 0x0400003C RID: 60
			[SerializeField]
			private bool m_DoStatic;

			// Token: 0x0400003D RID: 61
			[SerializeField]
			private string m_StaticString;
		}
	}
}
