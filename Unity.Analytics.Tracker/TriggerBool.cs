﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x02000010 RID: 16
	public enum TriggerBool
	{
		// Token: 0x0400003F RID: 63
		All,
		// Token: 0x04000040 RID: 64
		Any,
		// Token: 0x04000041 RID: 65
		None
	}
}
