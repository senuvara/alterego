﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x0200000A RID: 10
	[Serializable]
	public class ValueProperty
	{
		// Token: 0x06000022 RID: 34 RVA: 0x00002888 File Offset: 0x00000A88
		public ValueProperty()
		{
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000023 RID: 35 RVA: 0x000028A9 File Offset: 0x00000AA9
		// (set) Token: 0x06000024 RID: 36 RVA: 0x000028B1 File Offset: 0x00000AB1
		public string valueType
		{
			get
			{
				return this.m_ValueType;
			}
			set
			{
				this.m_ValueType = value;
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000025 RID: 37 RVA: 0x000028BC File Offset: 0x00000ABC
		public string propertyValue
		{
			get
			{
				if (this.m_PropertyType == ValueProperty.PropertyType.Dynamic && this.m_Target != null)
				{
					object value = this.m_Target.GetValue();
					return (value != null) ? value.ToString().Trim() : null;
				}
				return (this.m_Value != null) ? this.m_Value.Trim() : null;
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000026 RID: 38 RVA: 0x00002920 File Offset: 0x00000B20
		public TrackableField target
		{
			get
			{
				return this.m_Target;
			}
		}

		// Token: 0x06000027 RID: 39 RVA: 0x00002928 File Offset: 0x00000B28
		public bool IsValid()
		{
			switch (this.m_PropertyType)
			{
			case ValueProperty.PropertyType.Static:
				return !string.IsNullOrEmpty(this.m_Value) || Type.GetType(this.m_ValueType) != typeof(string);
			case ValueProperty.PropertyType.Dynamic:
				return this.m_Target != null && this.m_Target.GetValue() != null;
			}
			return false;
		}

		// Token: 0x0400001A RID: 26
		[SerializeField]
		private bool m_EditingCustomValue;

		// Token: 0x0400001B RID: 27
		[SerializeField]
		private int m_PopupIndex;

		// Token: 0x0400001C RID: 28
		[SerializeField]
		private string m_CustomValue;

		// Token: 0x0400001D RID: 29
		[SerializeField]
		private bool m_FixedType;

		// Token: 0x0400001E RID: 30
		[SerializeField]
		private string m_EnumType;

		// Token: 0x0400001F RID: 31
		[SerializeField]
		private bool m_EnumTypeIsCustomizable = true;

		// Token: 0x04000020 RID: 32
		[SerializeField]
		private bool m_CanDisable;

		// Token: 0x04000021 RID: 33
		[SerializeField]
		private ValueProperty.PropertyType m_PropertyType = ValueProperty.PropertyType.Static;

		// Token: 0x04000022 RID: 34
		[SerializeField]
		private string m_ValueType;

		// Token: 0x04000023 RID: 35
		[SerializeField]
		private string m_Value = string.Empty;

		// Token: 0x04000024 RID: 36
		[SerializeField]
		private TrackableField m_Target;

		// Token: 0x0200000B RID: 11
		public enum PropertyType
		{
			// Token: 0x04000026 RID: 38
			Disabled,
			// Token: 0x04000027 RID: 39
			Static,
			// Token: 0x04000028 RID: 40
			Dynamic
		}
	}
}
