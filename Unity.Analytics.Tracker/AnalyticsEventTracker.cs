﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Analytics
{
	// Token: 0x02000002 RID: 2
	[AddComponentMenu("Analytics/Analytics Event Tracker")]
	public class AnalyticsEventTracker : MonoBehaviour
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public AnalyticsEventTracker()
		{
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000002 RID: 2 RVA: 0x0000206E File Offset: 0x0000026E
		public StandardEventPayload payload
		{
			get
			{
				return this.m_EventPayload;
			}
		}

		// Token: 0x06000003 RID: 3 RVA: 0x00002076 File Offset: 0x00000276
		public void TriggerEvent()
		{
			this.SendEvent();
		}

		// Token: 0x06000004 RID: 4 RVA: 0x0000207F File Offset: 0x0000027F
		private AnalyticsResult SendEvent()
		{
			if (this.m_Trigger.Test(base.gameObject))
			{
				return this.payload.Send();
			}
			return AnalyticsResult.Ok;
		}

		// Token: 0x06000005 RID: 5 RVA: 0x000020A4 File Offset: 0x000002A4
		private void Awake()
		{
			if (this.m_Trigger.triggerType == TriggerType.Lifecycle && this.m_Trigger.lifecycleEvent == TriggerLifecycleEvent.Awake)
			{
				this.SendEvent();
			}
		}

		// Token: 0x06000006 RID: 6 RVA: 0x000020D0 File Offset: 0x000002D0
		private void Start()
		{
			if (this.m_Trigger.triggerType == TriggerType.Lifecycle && this.m_Trigger.lifecycleEvent == TriggerLifecycleEvent.Start)
			{
				this.SendEvent();
			}
			else if (this.m_Trigger.triggerType == TriggerType.Timer)
			{
				base.StartCoroutine(this.TimedTrigger());
			}
		}

		// Token: 0x06000007 RID: 7 RVA: 0x00002128 File Offset: 0x00000328
		private void OnEnable()
		{
			if (this.m_Trigger.triggerType == TriggerType.Lifecycle && this.m_Trigger.lifecycleEvent == TriggerLifecycleEvent.OnEnable)
			{
				this.SendEvent();
			}
		}

		// Token: 0x06000008 RID: 8 RVA: 0x00002152 File Offset: 0x00000352
		private void OnDisable()
		{
			if (this.m_Trigger.triggerType == TriggerType.Lifecycle && this.m_Trigger.lifecycleEvent == TriggerLifecycleEvent.OnDisable)
			{
				this.SendEvent();
			}
		}

		// Token: 0x06000009 RID: 9 RVA: 0x0000217C File Offset: 0x0000037C
		private void OnApplicationPause(bool paused)
		{
			if (this.m_Trigger.triggerType == TriggerType.Lifecycle)
			{
				if (paused && this.m_Trigger.lifecycleEvent == TriggerLifecycleEvent.OnApplicationPause)
				{
					this.SendEvent();
				}
				else if (!paused && this.m_Trigger.lifecycleEvent == TriggerLifecycleEvent.OnApplicationUnpause)
				{
					this.SendEvent();
				}
			}
		}

		// Token: 0x0600000A RID: 10 RVA: 0x000021DA File Offset: 0x000003DA
		private void OnDestroy()
		{
			if (this.m_Trigger.triggerType == TriggerType.Lifecycle && this.m_Trigger.lifecycleEvent == TriggerLifecycleEvent.OnDestroy)
			{
				this.SendEvent();
			}
		}

		// Token: 0x0600000B RID: 11 RVA: 0x00002204 File Offset: 0x00000404
		private IEnumerator TimedTrigger()
		{
			if (this.m_Trigger.initTime > 0f)
			{
				yield return new WaitForSeconds(this.m_Trigger.initTime);
			}
			this.SendEvent();
			while (this.m_Trigger.repetitions == 0 || this.m_Trigger.repetitionCount <= this.m_Trigger.repetitions)
			{
				if (this.m_Trigger.repeatTime > 0f)
				{
					yield return new WaitForSeconds(this.m_Trigger.repeatTime);
				}
				else
				{
					yield return null;
				}
				this.SendEvent();
			}
			yield break;
		}

		// Token: 0x04000001 RID: 1
		[SerializeField]
		public EventTrigger m_Trigger = new EventTrigger();

		// Token: 0x04000002 RID: 2
		[SerializeField]
		private StandardEventPayload m_EventPayload = new StandardEventPayload();

		// Token: 0x0200001A RID: 26
		[CompilerGenerated]
		private sealed class <TimedTrigger>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x06000067 RID: 103 RVA: 0x0000221F File Offset: 0x0000041F
			[DebuggerHidden]
			public <TimedTrigger>c__Iterator0()
			{
			}

			// Token: 0x06000068 RID: 104 RVA: 0x00002228 File Offset: 0x00000428
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					if (this.m_Trigger.initTime > 0f)
					{
						this.$current = new WaitForSeconds(this.m_Trigger.initTime);
						if (!this.$disposing)
						{
							this.$PC = 1;
						}
						return true;
					}
					break;
				case 1U:
					break;
				case 2U:
					goto IL_EC;
				case 3U:
					goto IL_EC;
				default:
					return false;
				}
				base.SendEvent();
				goto IL_F8;
				IL_EC:
				base.SendEvent();
				IL_F8:
				if (this.m_Trigger.repetitions != 0 && this.m_Trigger.repetitionCount > this.m_Trigger.repetitions)
				{
					this.$PC = -1;
				}
				else
				{
					if (this.m_Trigger.repeatTime > 0f)
					{
						this.$current = new WaitForSeconds(this.m_Trigger.repeatTime);
						if (!this.$disposing)
						{
							this.$PC = 2;
						}
						return true;
					}
					this.$current = null;
					if (!this.$disposing)
					{
						this.$PC = 3;
					}
					return true;
				}
				return false;
			}

			// Token: 0x1700001C RID: 28
			// (get) Token: 0x06000069 RID: 105 RVA: 0x00002371 File Offset: 0x00000571
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x1700001D RID: 29
			// (get) Token: 0x0600006A RID: 106 RVA: 0x00002379 File Offset: 0x00000579
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x0600006B RID: 107 RVA: 0x00002381 File Offset: 0x00000581
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x0600006C RID: 108 RVA: 0x00002391 File Offset: 0x00000591
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x0400006C RID: 108
			internal AnalyticsEventTracker $this;

			// Token: 0x0400006D RID: 109
			internal object $current;

			// Token: 0x0400006E RID: 110
			internal bool $disposing;

			// Token: 0x0400006F RID: 111
			internal int $PC;
		}
	}
}
