﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Analytics
{
	// Token: 0x02000014 RID: 20
	[Serializable]
	public class TriggerListContainer
	{
		// Token: 0x06000048 RID: 72 RVA: 0x00002CBB File Offset: 0x00000EBB
		public TriggerListContainer()
		{
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000049 RID: 73 RVA: 0x00002CCE File Offset: 0x00000ECE
		// (set) Token: 0x0600004A RID: 74 RVA: 0x00002CD6 File Offset: 0x00000ED6
		internal List<TriggerRule> rules
		{
			get
			{
				return this.m_Rules;
			}
			set
			{
				this.m_Rules = value;
			}
		}

		// Token: 0x04000059 RID: 89
		[SerializeField]
		private List<TriggerRule> m_Rules = new List<TriggerRule>();
	}
}
