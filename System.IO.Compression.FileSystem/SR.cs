﻿using System;

// Token: 0x02000003 RID: 3
internal class SR
{
	// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
	public SR()
	{
	}

	// Token: 0x0400002A RID: 42
	public const string IO_DirectoryNameWithData = "Zip entry name ends in directory separator character but contains data.";

	// Token: 0x0400002B RID: 43
	public const string IO_ExtractingResultsInOutside = "Extracting Zip entry would have resulted in a file outside the specified destination directory.";

	// Token: 0x0400002C RID: 44
	public const string ArgumentException_BufferNotFromPool = "The buffer is not associated with this pool and may not be returned to it.";
}
