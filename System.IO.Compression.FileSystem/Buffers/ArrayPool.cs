﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;

namespace System.Buffers
{
	// Token: 0x02000004 RID: 4
	internal abstract class ArrayPool<T>
	{
		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000002 RID: 2 RVA: 0x00002058 File Offset: 0x00000258
		public static ArrayPool<T> Shared
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				return Volatile.Read<ArrayPool<T>>(ref ArrayPool<T>.s_sharedInstance) ?? ArrayPool<T>.EnsureSharedCreated();
			}
		}

		// Token: 0x06000003 RID: 3 RVA: 0x0000206D File Offset: 0x0000026D
		[MethodImpl(MethodImplOptions.NoInlining)]
		private static ArrayPool<T> EnsureSharedCreated()
		{
			Interlocked.CompareExchange<ArrayPool<T>>(ref ArrayPool<T>.s_sharedInstance, ArrayPool<T>.Create(), null);
			return ArrayPool<T>.s_sharedInstance;
		}

		// Token: 0x06000004 RID: 4 RVA: 0x00002085 File Offset: 0x00000285
		public static ArrayPool<T> Create()
		{
			return new DefaultArrayPool<T>();
		}

		// Token: 0x06000005 RID: 5 RVA: 0x0000208C File Offset: 0x0000028C
		public static ArrayPool<T> Create(int maxArrayLength, int maxArraysPerBucket)
		{
			return new DefaultArrayPool<T>(maxArrayLength, maxArraysPerBucket);
		}

		// Token: 0x06000006 RID: 6
		public abstract T[] Rent(int minimumLength);

		// Token: 0x06000007 RID: 7
		public abstract void Return(T[] array, bool clearArray = false);

		// Token: 0x06000008 RID: 8 RVA: 0x00002050 File Offset: 0x00000250
		protected ArrayPool()
		{
		}

		// Token: 0x06000009 RID: 9 RVA: 0x00002095 File Offset: 0x00000295
		// Note: this type is marked as 'beforefieldinit'.
		static ArrayPool()
		{
		}

		// Token: 0x0400002D RID: 45
		private static ArrayPool<T> s_sharedInstance;
	}
}
