﻿using System;
using System.Diagnostics.Tracing;

namespace System.Buffers
{
	// Token: 0x02000005 RID: 5
	[EventSource(Name = "System.Buffers.ArrayPoolEventSource")]
	internal sealed class ArrayPoolEventSource : EventSource
	{
		// Token: 0x0600000A RID: 10 RVA: 0x00002098 File Offset: 0x00000298
		[Event(1, Level = EventLevel.Verbose)]
		internal unsafe void BufferRented(int bufferId, int bufferSize, int poolId, int bucketId)
		{
			EventSource.EventData* ptr = stackalloc EventSource.EventData[checked(unchecked((UIntPtr)4) * (UIntPtr)sizeof(EventSource.EventData))];
			ptr->Size = 4;
			ptr->DataPointer = (IntPtr)((void*)(&bufferId));
			ptr[1].Size = 4;
			ptr[1].DataPointer = (IntPtr)((void*)(&bufferSize));
			ptr[2].Size = 4;
			ptr[2].DataPointer = (IntPtr)((void*)(&poolId));
			ptr[3].Size = 4;
			ptr[3].DataPointer = (IntPtr)((void*)(&bucketId));
			base.WriteEventCore(1, 4, ptr);
		}

		// Token: 0x0600000B RID: 11 RVA: 0x00002144 File Offset: 0x00000344
		[Event(2, Level = EventLevel.Informational)]
		internal unsafe void BufferAllocated(int bufferId, int bufferSize, int poolId, int bucketId, ArrayPoolEventSource.BufferAllocatedReason reason)
		{
			EventSource.EventData* ptr = stackalloc EventSource.EventData[checked(unchecked((UIntPtr)5) * (UIntPtr)sizeof(EventSource.EventData))];
			ptr->Size = 4;
			ptr->DataPointer = (IntPtr)((void*)(&bufferId));
			ptr[1].Size = 4;
			ptr[1].DataPointer = (IntPtr)((void*)(&bufferSize));
			ptr[2].Size = 4;
			ptr[2].DataPointer = (IntPtr)((void*)(&poolId));
			ptr[3].Size = 4;
			ptr[3].DataPointer = (IntPtr)((void*)(&bucketId));
			ptr[4].Size = 4;
			ptr[4].DataPointer = (IntPtr)((void*)(&reason));
			base.WriteEventCore(2, 5, ptr);
		}

		// Token: 0x0600000C RID: 12 RVA: 0x00002219 File Offset: 0x00000419
		[Event(3, Level = EventLevel.Verbose)]
		internal void BufferReturned(int bufferId, int bufferSize, int poolId)
		{
			base.WriteEvent(3, bufferId, bufferSize, poolId);
		}

		// Token: 0x0600000D RID: 13 RVA: 0x00002225 File Offset: 0x00000425
		public ArrayPoolEventSource()
		{
		}

		// Token: 0x0600000E RID: 14 RVA: 0x0000222D File Offset: 0x0000042D
		// Note: this type is marked as 'beforefieldinit'.
		static ArrayPoolEventSource()
		{
		}

		// Token: 0x0400002E RID: 46
		internal static readonly ArrayPoolEventSource Log = new ArrayPoolEventSource();

		// Token: 0x02000006 RID: 6
		internal enum BufferAllocatedReason
		{
			// Token: 0x04000030 RID: 48
			Pooled,
			// Token: 0x04000031 RID: 49
			OverMaximumSize,
			// Token: 0x04000032 RID: 50
			PoolExhausted
		}
	}
}
