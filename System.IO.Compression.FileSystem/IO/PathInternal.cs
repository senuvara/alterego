﻿using System;

namespace System.IO
{
	// Token: 0x0200000A RID: 10
	internal static class PathInternal
	{
		// Token: 0x17000004 RID: 4
		// (get) Token: 0x0600001A RID: 26 RVA: 0x00002622 File Offset: 0x00000822
		internal static StringComparison StringComparison
		{
			get
			{
				if (!PathInternal.s_isCaseSensitive)
				{
					return StringComparison.OrdinalIgnoreCase;
				}
				return StringComparison.Ordinal;
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x0600001B RID: 27 RVA: 0x0000262E File Offset: 0x0000082E
		internal static bool IsCaseSensitive
		{
			get
			{
				return PathInternal.s_isCaseSensitive;
			}
		}

		// Token: 0x0600001C RID: 28 RVA: 0x00002638 File Offset: 0x00000838
		private static bool GetIsCaseSensitive()
		{
			bool result;
			try
			{
				string text = Path.Combine(Path.GetTempPath(), "CASESENSITIVETEST" + Guid.NewGuid().ToString("N"));
				using (new FileStream(text, FileMode.CreateNew, FileAccess.ReadWrite, FileShare.None, 4096, FileOptions.DeleteOnClose))
				{
					result = !File.Exists(text.ToLowerInvariant());
				}
			}
			catch (Exception)
			{
				result = false;
			}
			return result;
		}

		// Token: 0x0600001D RID: 29 RVA: 0x000026C0 File Offset: 0x000008C0
		// Note: this type is marked as 'beforefieldinit'.
		static PathInternal()
		{
		}

		// Token: 0x0400003C RID: 60
		private static readonly bool s_isCaseSensitive = PathInternal.GetIsCaseSensitive();
	}
}
