﻿using System;

namespace UnityEngineInternal.Input
{
	// Token: 0x02000002 RID: 2
	// (Invoke) Token: 0x06000002 RID: 2
	public unsafe delegate void NativeUpdateCallback(NativeInputUpdateType updateType, NativeInputEventBuffer* buffer);
}
