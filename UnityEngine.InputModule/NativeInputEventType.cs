﻿using System;

namespace UnityEngineInternal.Input
{
	// Token: 0x02000003 RID: 3
	public enum NativeInputEventType
	{
		// Token: 0x04000002 RID: 2
		DeviceRemoved = 1146242381,
		// Token: 0x04000003 RID: 3
		DeviceConfigChanged = 1145259591,
		// Token: 0x04000004 RID: 4
		Text = 1413830740,
		// Token: 0x04000005 RID: 5
		State = 1398030676,
		// Token: 0x04000006 RID: 6
		Delta = 1145852993
	}
}
