﻿using System;

namespace UnityEngineInternal.Input
{
	// Token: 0x02000006 RID: 6
	[Flags]
	public enum NativeInputUpdateType
	{
		// Token: 0x04000011 RID: 17
		Dynamic = 1,
		// Token: 0x04000012 RID: 18
		Fixed = 2,
		// Token: 0x04000013 RID: 19
		BeforeRender = 4,
		// Token: 0x04000014 RID: 20
		Editor = 8,
		// Token: 0x04000015 RID: 21
		IgnoreFocus = -2147483648
	}
}
