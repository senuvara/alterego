﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Security.Permissions;
using UnityEngine;

[assembly: AssemblyVersion("0.0.0.0")]
[assembly: InternalsVisibleTo("UnityEditor.SpatialTracking")]
[assembly: UnityAPICompatibilityVersion("2018.3.14f1")]
[assembly: SecurityPermission(SecurityAction.RequestMinimum, SkipVerification = true)]
