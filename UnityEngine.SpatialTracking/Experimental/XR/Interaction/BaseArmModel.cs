﻿using System;
using UnityEngine.SpatialTracking;

namespace UnityEngine.Experimental.XR.Interaction
{
	// Token: 0x02000002 RID: 2
	[Serializable]
	public abstract class BaseArmModel : BasePoseProvider
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002058 File Offset: 0x00000258
		protected BaseArmModel()
		{
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000002 RID: 2 RVA: 0x00002060 File Offset: 0x00000260
		// (set) Token: 0x06000003 RID: 3 RVA: 0x0000207B File Offset: 0x0000027B
		public TrackedPoseDriver.TrackedPose headPoseSource
		{
			get
			{
				return this.m_HeadPoseSource;
			}
			internal set
			{
				this.m_HeadPoseSource = value;
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000004 RID: 4 RVA: 0x00002088 File Offset: 0x00000288
		// (set) Token: 0x06000005 RID: 5 RVA: 0x000020A3 File Offset: 0x000002A3
		public TrackedPoseDriver.TrackedPose poseSource
		{
			get
			{
				return this.m_PoseSource;
			}
			internal set
			{
				this.m_PoseSource = value;
			}
		}

		// Token: 0x06000006 RID: 6 RVA: 0x000020B0 File Offset: 0x000002B0
		protected bool TryGetTrackingDataFromSource(TrackedPoseDriver.TrackedPose poseSource, out Pose resultPose)
		{
			return PoseDataSource.TryGetDataFromSource(poseSource, out resultPose);
		}

		// Token: 0x04000001 RID: 1
		[SerializeField]
		[Tooltip("The head pose that will be used by the arm model")]
		private TrackedPoseDriver.TrackedPose m_HeadPoseSource;

		// Token: 0x04000002 RID: 2
		[SerializeField]
		[Tooltip("The arm source pose that will be used by the arm model")]
		private TrackedPoseDriver.TrackedPose m_PoseSource;
	}
}
