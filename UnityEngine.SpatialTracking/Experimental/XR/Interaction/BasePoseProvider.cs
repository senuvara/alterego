﻿using System;

namespace UnityEngine.Experimental.XR.Interaction
{
	// Token: 0x02000003 RID: 3
	[Serializable]
	public abstract class BasePoseProvider : MonoBehaviour
	{
		// Token: 0x06000007 RID: 7 RVA: 0x00002050 File Offset: 0x00000250
		protected BasePoseProvider()
		{
		}

		// Token: 0x06000008 RID: 8
		public abstract bool TryGetPoseFromProvider(out Pose output);
	}
}
