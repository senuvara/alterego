﻿using System;
using System.Collections.Generic;
using UnityEngine.XR;
using UnityEngine.XR.Tango;

namespace UnityEngine.SpatialTracking
{
	// Token: 0x02000006 RID: 6
	public static class PoseDataSource
	{
		// Token: 0x0600000B RID: 11 RVA: 0x00002204 File Offset: 0x00000404
		internal static bool TryGetNodePoseData(XRNode node, out Pose resultPose)
		{
			InputTracking.GetNodeStates(PoseDataSource.nodeStates);
			foreach (XRNodeState xrnodeState in PoseDataSource.nodeStates)
			{
				if (xrnodeState.nodeType == node)
				{
					bool flag = xrnodeState.TryGetPosition(out resultPose.position);
					return flag | xrnodeState.TryGetRotation(out resultPose.rotation);
				}
			}
			resultPose = Pose.identity;
			return false;
		}

		// Token: 0x0600000C RID: 12 RVA: 0x000022AC File Offset: 0x000004AC
		public static bool TryGetDataFromSource(TrackedPoseDriver.TrackedPose poseSource, out Pose resultPose)
		{
			switch (poseSource)
			{
			case TrackedPoseDriver.TrackedPose.LeftEye:
				return PoseDataSource.TryGetNodePoseData(XRNode.LeftEye, out resultPose);
			case TrackedPoseDriver.TrackedPose.RightEye:
				return PoseDataSource.TryGetNodePoseData(XRNode.RightEye, out resultPose);
			case TrackedPoseDriver.TrackedPose.Center:
				return PoseDataSource.TryGetNodePoseData(XRNode.CenterEye, out resultPose);
			case TrackedPoseDriver.TrackedPose.Head:
				return PoseDataSource.TryGetNodePoseData(XRNode.Head, out resultPose);
			case TrackedPoseDriver.TrackedPose.LeftPose:
				return PoseDataSource.TryGetNodePoseData(XRNode.LeftHand, out resultPose);
			case TrackedPoseDriver.TrackedPose.RightPose:
				return PoseDataSource.TryGetNodePoseData(XRNode.RightHand, out resultPose);
			case TrackedPoseDriver.TrackedPose.ColorCamera:
				return PoseDataSource.TryGetTangoPose(out resultPose) || PoseDataSource.TryGetNodePoseData(XRNode.CenterEye, out resultPose);
			case TrackedPoseDriver.TrackedPose.RemotePose:
				return PoseDataSource.TryGetNodePoseData(XRNode.RightHand, out resultPose) || PoseDataSource.TryGetNodePoseData(XRNode.LeftHand, out resultPose);
			}
			Debug.LogWarningFormat("Unable to retrieve pose data for poseSource:{0}", new object[]
			{
				poseSource.ToString()
			});
			resultPose = Pose.identity;
			return false;
		}

		// Token: 0x0600000D RID: 13 RVA: 0x000023C0 File Offset: 0x000005C0
		private static bool TryGetTangoPose(out Pose pose)
		{
			PoseData poseData;
			bool result;
			if (TangoInputTracking.TryGetPoseAtTime(out poseData) && poseData.statusCode == PoseStatus.Valid)
			{
				pose.position = poseData.position;
				pose.rotation = poseData.rotation;
				result = true;
			}
			else
			{
				pose = Pose.identity;
				result = false;
			}
			return result;
		}

		// Token: 0x0600000E RID: 14 RVA: 0x0000241C File Offset: 0x0000061C
		// Note: this type is marked as 'beforefieldinit'.
		static PoseDataSource()
		{
		}

		// Token: 0x04000006 RID: 6
		private static List<XRNodeState> nodeStates = new List<XRNodeState>();
	}
}
