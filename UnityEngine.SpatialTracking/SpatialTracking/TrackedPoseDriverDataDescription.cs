﻿using System;
using System.Collections.Generic;

namespace UnityEngine.SpatialTracking
{
	// Token: 0x02000004 RID: 4
	internal class TrackedPoseDriverDataDescription
	{
		// Token: 0x06000009 RID: 9 RVA: 0x000020CC File Offset: 0x000002CC
		public TrackedPoseDriverDataDescription()
		{
		}

		// Token: 0x0600000A RID: 10 RVA: 0x000020D4 File Offset: 0x000002D4
		// Note: this type is marked as 'beforefieldinit'.
		static TrackedPoseDriverDataDescription()
		{
		}

		// Token: 0x04000003 RID: 3
		public static List<TrackedPoseDriverDataDescription.PoseData> DeviceData = new List<TrackedPoseDriverDataDescription.PoseData>
		{
			new TrackedPoseDriverDataDescription.PoseData
			{
				PoseNames = new List<string>
				{
					"Left Eye",
					" Right Eye",
					"Center Eye",
					"Head",
					"Color Camera"
				},
				Poses = new List<TrackedPoseDriver.TrackedPose>
				{
					TrackedPoseDriver.TrackedPose.LeftEye,
					TrackedPoseDriver.TrackedPose.RightEye,
					TrackedPoseDriver.TrackedPose.Center,
					TrackedPoseDriver.TrackedPose.Head,
					TrackedPoseDriver.TrackedPose.ColorCamera
				}
			},
			new TrackedPoseDriverDataDescription.PoseData
			{
				PoseNames = new List<string>
				{
					"Left Controller",
					" Right Controller"
				},
				Poses = new List<TrackedPoseDriver.TrackedPose>
				{
					TrackedPoseDriver.TrackedPose.LeftPose,
					TrackedPoseDriver.TrackedPose.RightPose
				}
			},
			new TrackedPoseDriverDataDescription.PoseData
			{
				PoseNames = new List<string>
				{
					"Device Pose"
				},
				Poses = new List<TrackedPoseDriver.TrackedPose>
				{
					TrackedPoseDriver.TrackedPose.RemotePose
				}
			}
		};

		// Token: 0x02000005 RID: 5
		public struct PoseData
		{
			// Token: 0x04000004 RID: 4
			public List<string> PoseNames;

			// Token: 0x04000005 RID: 5
			public List<TrackedPoseDriver.TrackedPose> Poses;
		}
	}
}
