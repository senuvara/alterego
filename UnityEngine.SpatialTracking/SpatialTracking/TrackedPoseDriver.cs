﻿using System;
using UnityEngine.Experimental.XR.Interaction;
using UnityEngine.XR;

namespace UnityEngine.SpatialTracking
{
	// Token: 0x02000007 RID: 7
	[DefaultExecutionOrder(-30000)]
	[AddComponentMenu("XR/Tracked Pose Driver")]
	[Serializable]
	public class TrackedPoseDriver : MonoBehaviour
	{
		// Token: 0x0600000F RID: 15 RVA: 0x00002428 File Offset: 0x00000628
		public TrackedPoseDriver()
		{
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000010 RID: 16 RVA: 0x00002448 File Offset: 0x00000648
		// (set) Token: 0x06000011 RID: 17 RVA: 0x00002463 File Offset: 0x00000663
		public TrackedPoseDriver.DeviceType deviceType
		{
			get
			{
				return this.m_Device;
			}
			internal set
			{
				this.m_Device = value;
			}
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000012 RID: 18 RVA: 0x00002470 File Offset: 0x00000670
		// (set) Token: 0x06000013 RID: 19 RVA: 0x0000248B File Offset: 0x0000068B
		public TrackedPoseDriver.TrackedPose poseSource
		{
			get
			{
				return this.m_PoseSource;
			}
			internal set
			{
				this.m_PoseSource = value;
			}
		}

		// Token: 0x06000014 RID: 20 RVA: 0x00002498 File Offset: 0x00000698
		public bool SetPoseSource(TrackedPoseDriver.DeviceType deviceType, TrackedPoseDriver.TrackedPose pose)
		{
			if (deviceType < (TrackedPoseDriver.DeviceType)TrackedPoseDriverDataDescription.DeviceData.Count)
			{
				TrackedPoseDriverDataDescription.PoseData poseData = TrackedPoseDriverDataDescription.DeviceData[(int)deviceType];
				for (int i = 0; i < poseData.Poses.Count; i++)
				{
					if (poseData.Poses[i] == pose)
					{
						this.deviceType = deviceType;
						this.poseSource = pose;
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000015 RID: 21 RVA: 0x00002514 File Offset: 0x00000714
		// (set) Token: 0x06000016 RID: 22 RVA: 0x0000252F File Offset: 0x0000072F
		public BasePoseProvider poseProviderComponent
		{
			get
			{
				return this.m_PoseProviderComponent;
			}
			set
			{
				this.m_PoseProviderComponent = value;
			}
		}

		// Token: 0x06000017 RID: 23 RVA: 0x0000253C File Offset: 0x0000073C
		private bool TryGetPoseData(TrackedPoseDriver.DeviceType device, TrackedPoseDriver.TrackedPose poseSource, out Pose resultPose)
		{
			bool result;
			if (this.m_PoseProviderComponent != null)
			{
				result = this.m_PoseProviderComponent.TryGetPoseFromProvider(out resultPose);
			}
			else
			{
				result = PoseDataSource.TryGetDataFromSource(poseSource, out resultPose);
			}
			return result;
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000018 RID: 24 RVA: 0x0000257C File Offset: 0x0000077C
		// (set) Token: 0x06000019 RID: 25 RVA: 0x00002597 File Offset: 0x00000797
		public TrackedPoseDriver.TrackingType trackingType
		{
			get
			{
				return this.m_TrackingType;
			}
			set
			{
				this.m_TrackingType = value;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600001A RID: 26 RVA: 0x000025A4 File Offset: 0x000007A4
		// (set) Token: 0x0600001B RID: 27 RVA: 0x000025BF File Offset: 0x000007BF
		public TrackedPoseDriver.UpdateType updateType
		{
			get
			{
				return this.m_UpdateType;
			}
			set
			{
				this.m_UpdateType = value;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x0600001C RID: 28 RVA: 0x000025CC File Offset: 0x000007CC
		// (set) Token: 0x0600001D RID: 29 RVA: 0x000025E7 File Offset: 0x000007E7
		public bool UseRelativeTransform
		{
			get
			{
				return this.m_UseRelativeTransform;
			}
			set
			{
				this.m_UseRelativeTransform = value;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x0600001E RID: 30 RVA: 0x000025F4 File Offset: 0x000007F4
		// (set) Token: 0x0600001F RID: 31 RVA: 0x0000260F File Offset: 0x0000080F
		public Pose originPose
		{
			get
			{
				return this.m_OriginPose;
			}
			set
			{
				this.m_OriginPose = value;
			}
		}

		// Token: 0x06000020 RID: 32 RVA: 0x00002619 File Offset: 0x00000819
		private void CacheLocalPosition()
		{
			this.m_OriginPose.position = base.transform.localPosition;
			this.m_OriginPose.rotation = base.transform.localRotation;
		}

		// Token: 0x06000021 RID: 33 RVA: 0x00002648 File Offset: 0x00000848
		private void ResetToCachedLocalPosition()
		{
			this.SetLocalTransform(this.m_OriginPose.position, this.m_OriginPose.rotation);
		}

		// Token: 0x06000022 RID: 34 RVA: 0x00002667 File Offset: 0x00000867
		protected virtual void Awake()
		{
			this.CacheLocalPosition();
			if (this.HasStereoCamera())
			{
				XRDevice.DisableAutoXRCameraTracking(base.GetComponent<Camera>(), true);
			}
		}

		// Token: 0x06000023 RID: 35 RVA: 0x00002689 File Offset: 0x00000889
		protected virtual void OnDestroy()
		{
			if (this.HasStereoCamera())
			{
			}
		}

		// Token: 0x06000024 RID: 36 RVA: 0x00002699 File Offset: 0x00000899
		protected virtual void OnEnable()
		{
			Application.onBeforeRender += this.OnBeforeRender;
		}

		// Token: 0x06000025 RID: 37 RVA: 0x000026AE File Offset: 0x000008AE
		protected virtual void OnDisable()
		{
			this.ResetToCachedLocalPosition();
			Application.onBeforeRender -= this.OnBeforeRender;
		}

		// Token: 0x06000026 RID: 38 RVA: 0x000026C9 File Offset: 0x000008C9
		protected virtual void FixedUpdate()
		{
			if (this.m_UpdateType == TrackedPoseDriver.UpdateType.Update || this.m_UpdateType == TrackedPoseDriver.UpdateType.UpdateAndBeforeRender)
			{
				this.PerformUpdate();
			}
		}

		// Token: 0x06000027 RID: 39 RVA: 0x000026EB File Offset: 0x000008EB
		protected virtual void Update()
		{
			if (this.m_UpdateType == TrackedPoseDriver.UpdateType.Update || this.m_UpdateType == TrackedPoseDriver.UpdateType.UpdateAndBeforeRender)
			{
				this.PerformUpdate();
			}
		}

		// Token: 0x06000028 RID: 40 RVA: 0x0000270D File Offset: 0x0000090D
		protected virtual void OnBeforeRender()
		{
			if (this.m_UpdateType == TrackedPoseDriver.UpdateType.BeforeRender || this.m_UpdateType == TrackedPoseDriver.UpdateType.UpdateAndBeforeRender)
			{
				this.PerformUpdate();
			}
		}

		// Token: 0x06000029 RID: 41 RVA: 0x00002730 File Offset: 0x00000930
		protected virtual void SetLocalTransform(Vector3 newPosition, Quaternion newRotation)
		{
			if (this.m_TrackingType == TrackedPoseDriver.TrackingType.RotationAndPosition || this.m_TrackingType == TrackedPoseDriver.TrackingType.RotationOnly)
			{
				base.transform.localRotation = newRotation;
			}
			if (this.m_TrackingType == TrackedPoseDriver.TrackingType.RotationAndPosition || this.m_TrackingType == TrackedPoseDriver.TrackingType.PositionOnly)
			{
				base.transform.localPosition = newPosition;
			}
		}

		// Token: 0x0600002A RID: 42 RVA: 0x00002788 File Offset: 0x00000988
		protected Pose TransformPoseByOriginIfNeeded(Pose pose)
		{
			Pose result;
			if (this.m_UseRelativeTransform)
			{
				result = pose.GetTransformedBy(this.m_OriginPose);
			}
			else
			{
				result = pose;
			}
			return result;
		}

		// Token: 0x0600002B RID: 43 RVA: 0x000027C0 File Offset: 0x000009C0
		private bool HasStereoCamera()
		{
			Camera component = base.GetComponent<Camera>();
			return component != null && component.stereoEnabled;
		}

		// Token: 0x0600002C RID: 44 RVA: 0x000027F4 File Offset: 0x000009F4
		protected virtual void PerformUpdate()
		{
			if (base.enabled)
			{
				Pose pose = default(Pose);
				pose = Pose.identity;
				if (this.TryGetPoseData(this.m_Device, this.m_PoseSource, out pose))
				{
					Pose pose2 = this.TransformPoseByOriginIfNeeded(pose);
					this.SetLocalTransform(pose2.position, pose2.rotation);
				}
			}
		}

		// Token: 0x04000007 RID: 7
		[SerializeField]
		private TrackedPoseDriver.DeviceType m_Device;

		// Token: 0x04000008 RID: 8
		[SerializeField]
		private TrackedPoseDriver.TrackedPose m_PoseSource;

		// Token: 0x04000009 RID: 9
		[SerializeField]
		private BasePoseProvider m_PoseProviderComponent = null;

		// Token: 0x0400000A RID: 10
		[SerializeField]
		private TrackedPoseDriver.TrackingType m_TrackingType;

		// Token: 0x0400000B RID: 11
		[SerializeField]
		private TrackedPoseDriver.UpdateType m_UpdateType = TrackedPoseDriver.UpdateType.UpdateAndBeforeRender;

		// Token: 0x0400000C RID: 12
		[SerializeField]
		private bool m_UseRelativeTransform = true;

		// Token: 0x0400000D RID: 13
		protected Pose m_OriginPose;

		// Token: 0x02000008 RID: 8
		public enum DeviceType
		{
			// Token: 0x0400000F RID: 15
			GenericXRDevice,
			// Token: 0x04000010 RID: 16
			GenericXRController,
			// Token: 0x04000011 RID: 17
			GenericXRRemote
		}

		// Token: 0x02000009 RID: 9
		public enum TrackedPose
		{
			// Token: 0x04000013 RID: 19
			LeftEye,
			// Token: 0x04000014 RID: 20
			RightEye,
			// Token: 0x04000015 RID: 21
			Center,
			// Token: 0x04000016 RID: 22
			Head,
			// Token: 0x04000017 RID: 23
			LeftPose,
			// Token: 0x04000018 RID: 24
			RightPose,
			// Token: 0x04000019 RID: 25
			ColorCamera,
			// Token: 0x0400001A RID: 26
			DepthCameraDeprecated,
			// Token: 0x0400001B RID: 27
			FisheyeCameraDeprecated,
			// Token: 0x0400001C RID: 28
			DeviceDeprecated,
			// Token: 0x0400001D RID: 29
			RemotePose
		}

		// Token: 0x0200000A RID: 10
		public enum TrackingType
		{
			// Token: 0x0400001F RID: 31
			RotationAndPosition,
			// Token: 0x04000020 RID: 32
			RotationOnly,
			// Token: 0x04000021 RID: 33
			PositionOnly
		}

		// Token: 0x0200000B RID: 11
		public enum UpdateType
		{
			// Token: 0x04000023 RID: 35
			UpdateAndBeforeRender,
			// Token: 0x04000024 RID: 36
			Update,
			// Token: 0x04000025 RID: 37
			BeforeRender
		}
	}
}
