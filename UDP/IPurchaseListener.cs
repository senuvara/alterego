﻿using System;

namespace UnityEngine.UDP
{
	// Token: 0x02000008 RID: 8
	public interface IPurchaseListener
	{
		// Token: 0x06000025 RID: 37
		void OnPurchase(PurchaseInfo purchaseInfo);

		// Token: 0x06000026 RID: 38
		void OnPurchaseFailed(string message, PurchaseInfo purchaseInfo);

		// Token: 0x06000027 RID: 39
		void OnPurchaseRepeated(string productId);

		// Token: 0x06000028 RID: 40
		void OnPurchaseConsume(PurchaseInfo purchaseInfo);

		// Token: 0x06000029 RID: 41
		void OnPurchaseConsumeFailed(string message, PurchaseInfo purchaseInfo);

		// Token: 0x0600002A RID: 42
		void OnQueryInventory(Inventory inventory);

		// Token: 0x0600002B RID: 43
		void OnQueryInventoryFailed(string message);
	}
}
