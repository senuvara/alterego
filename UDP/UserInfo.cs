﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine.UDP
{
	// Token: 0x02000011 RID: 17
	public class UserInfo
	{
		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000075 RID: 117 RVA: 0x000032D9 File Offset: 0x000014D9
		// (set) Token: 0x06000076 RID: 118 RVA: 0x000032E1 File Offset: 0x000014E1
		[Preserve]
		public string Channel
		{
			[CompilerGenerated]
			get
			{
				return this.<Channel>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Channel>k__BackingField = value;
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000077 RID: 119 RVA: 0x000032EA File Offset: 0x000014EA
		// (set) Token: 0x06000078 RID: 120 RVA: 0x000032F2 File Offset: 0x000014F2
		[Preserve]
		public string UserId
		{
			[CompilerGenerated]
			get
			{
				return this.<UserId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<UserId>k__BackingField = value;
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000079 RID: 121 RVA: 0x000032FB File Offset: 0x000014FB
		// (set) Token: 0x0600007A RID: 122 RVA: 0x00003303 File Offset: 0x00001503
		[Preserve]
		public string UserLoginToken
		{
			[CompilerGenerated]
			get
			{
				return this.<UserLoginToken>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<UserLoginToken>k__BackingField = value;
			}
		}

		// Token: 0x0600007B RID: 123 RVA: 0x000022FC File Offset: 0x000004FC
		public UserInfo()
		{
		}

		// Token: 0x0400003F RID: 63
		[CompilerGenerated]
		private string <Channel>k__BackingField;

		// Token: 0x04000040 RID: 64
		[CompilerGenerated]
		private string <UserId>k__BackingField;

		// Token: 0x04000041 RID: 65
		[CompilerGenerated]
		private string <UserLoginToken>k__BackingField;
	}
}
