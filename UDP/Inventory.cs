﻿using System;
using System.Collections.Generic;

namespace UnityEngine.UDP
{
	// Token: 0x02000007 RID: 7
	public class Inventory
	{
		// Token: 0x0600001A RID: 26 RVA: 0x000023C4 File Offset: 0x000005C4
		public PurchaseInfo GetPurchaseInfo(string productId)
		{
			PurchaseInfo result = new PurchaseInfo();
			if (this._purchaseDictionary.TryGetValue(productId, out result))
			{
				return result;
			}
			return null;
		}

		// Token: 0x0600001B RID: 27 RVA: 0x000023EC File Offset: 0x000005EC
		public ProductInfo GetProductInfo(string productId)
		{
			ProductInfo result;
			if (this._productDictionary.TryGetValue(productId, out result))
			{
				return result;
			}
			return null;
		}

		// Token: 0x0600001C RID: 28 RVA: 0x0000240C File Offset: 0x0000060C
		public bool HasPurchase(string productId)
		{
			return this._purchaseDictionary.ContainsKey(productId);
		}

		// Token: 0x0600001D RID: 29 RVA: 0x0000241A File Offset: 0x0000061A
		public bool HasProduct(string productId)
		{
			return this._productDictionary.ContainsKey(productId);
		}

		// Token: 0x0600001E RID: 30 RVA: 0x00002428 File Offset: 0x00000628
		public IDictionary<string, PurchaseInfo> GetPurchaseDictionary()
		{
			return new Dictionary<string, PurchaseInfo>(this._purchaseDictionary);
		}

		// Token: 0x0600001F RID: 31 RVA: 0x00002435 File Offset: 0x00000635
		public IDictionary<string, ProductInfo> GetProductDictionary()
		{
			return new Dictionary<string, ProductInfo>(this._productDictionary);
		}

		// Token: 0x06000020 RID: 32 RVA: 0x00002444 File Offset: 0x00000644
		public IList<ProductInfo> GetProductList()
		{
			List<ProductInfo> list = new List<ProductInfo>();
			foreach (KeyValuePair<string, ProductInfo> keyValuePair in this._productDictionary)
			{
				list.Add(keyValuePair.Value);
			}
			return list;
		}

		// Token: 0x06000021 RID: 33 RVA: 0x000024A4 File Offset: 0x000006A4
		public List<PurchaseInfo> GetPurchaseList()
		{
			List<PurchaseInfo> list = new List<PurchaseInfo>();
			foreach (KeyValuePair<string, PurchaseInfo> keyValuePair in this._purchaseDictionary)
			{
				list.Add(keyValuePair.Value);
			}
			return list;
		}

		// Token: 0x06000022 RID: 34 RVA: 0x00002504 File Offset: 0x00000704
		internal void AddProduct(ProductInfo productInfo)
		{
			this._productDictionary.Add(productInfo.ProductId, productInfo);
		}

		// Token: 0x06000023 RID: 35 RVA: 0x00002518 File Offset: 0x00000718
		internal void AddPurchase(PurchaseInfo purchaseInfo)
		{
			this._purchaseDictionary.Add(purchaseInfo.ProductId, purchaseInfo);
		}

		// Token: 0x06000024 RID: 36 RVA: 0x0000252C File Offset: 0x0000072C
		public Inventory()
		{
		}

		// Token: 0x04000021 RID: 33
		private readonly Dictionary<string, PurchaseInfo> _purchaseDictionary = new Dictionary<string, PurchaseInfo>();

		// Token: 0x04000022 RID: 34
		private readonly Dictionary<string, ProductInfo> _productDictionary = new Dictionary<string, ProductInfo>();
	}
}
