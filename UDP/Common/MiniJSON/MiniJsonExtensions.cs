﻿using System;
using System.Collections.Generic;

namespace UnityEngine.UDP.Common.MiniJSON
{
	// Token: 0x02000015 RID: 21
	public static class MiniJsonExtensions
	{
		// Token: 0x06000083 RID: 131 RVA: 0x0000334E File Offset: 0x0000154E
		public static Dictionary<string, object> GetHash(this Dictionary<string, object> dic, string key)
		{
			return (Dictionary<string, object>)dic[key];
		}

		// Token: 0x06000084 RID: 132 RVA: 0x0000335C File Offset: 0x0000155C
		public static T GetEnum<T>(this Dictionary<string, object> dic, string key)
		{
			if (dic.ContainsKey(key))
			{
				return (T)((object)Enum.Parse(typeof(T), dic[key].ToString(), true));
			}
			return default(T);
		}

		// Token: 0x06000085 RID: 133 RVA: 0x0000339D File Offset: 0x0000159D
		public static string GetString(this Dictionary<string, object> dic, string key, string defaultValue = "")
		{
			if (dic.ContainsKey(key))
			{
				return dic[key].ToString();
			}
			return defaultValue;
		}

		// Token: 0x06000086 RID: 134 RVA: 0x000033B6 File Offset: 0x000015B6
		public static long GetLong(this Dictionary<string, object> dic, string key)
		{
			if (dic.ContainsKey(key))
			{
				return long.Parse(dic[key].ToString());
			}
			return 0L;
		}

		// Token: 0x06000087 RID: 135 RVA: 0x000033D8 File Offset: 0x000015D8
		public static List<string> GetStringList(this Dictionary<string, object> dic, string key)
		{
			if (dic.ContainsKey(key))
			{
				List<string> list = new List<string>();
				foreach (object obj in ((List<object>)dic[key]))
				{
					list.Add(obj.ToString());
				}
				return list;
			}
			return new List<string>();
		}

		// Token: 0x06000088 RID: 136 RVA: 0x0000344C File Offset: 0x0000164C
		public static bool GetBool(this Dictionary<string, object> dic, string key)
		{
			return dic.ContainsKey(key) && bool.Parse(dic[key].ToString());
		}

		// Token: 0x06000089 RID: 137 RVA: 0x0000346C File Offset: 0x0000166C
		public static T Get<T>(this Dictionary<string, object> dic, string key)
		{
			if (dic.ContainsKey(key))
			{
				return (T)((object)dic[key]);
			}
			return default(T);
		}

		// Token: 0x0600008A RID: 138 RVA: 0x00003498 File Offset: 0x00001698
		public static string toJson(this Dictionary<string, object> obj)
		{
			return MiniJson.JsonEncode(obj);
		}

		// Token: 0x0600008B RID: 139 RVA: 0x00003498 File Offset: 0x00001698
		public static string toJson(this Dictionary<string, string> obj)
		{
			return MiniJson.JsonEncode(obj);
		}

		// Token: 0x0600008C RID: 140 RVA: 0x000034A0 File Offset: 0x000016A0
		public static string toJson(this string[] array)
		{
			List<object> list = new List<object>();
			foreach (string item in array)
			{
				list.Add(item);
			}
			return MiniJson.JsonEncode(list);
		}

		// Token: 0x0600008D RID: 141 RVA: 0x000034D4 File Offset: 0x000016D4
		public static Dictionary<string, object> HashtableFromJson(this string json)
		{
			return MiniJson.JsonDecode(json);
		}
	}
}
