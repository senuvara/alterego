﻿using System;
using System.Collections.Generic;
using UnityEngine.UDP.Common.MiniJSON;

namespace UnityEngine.UDP.Common
{
	// Token: 0x02000013 RID: 19
	public class MiniJson
	{
		// Token: 0x0600007E RID: 126 RVA: 0x00003324 File Offset: 0x00001524
		public static string JsonEncode(object json)
		{
			return Json.Serialize(json);
		}

		// Token: 0x0600007F RID: 127 RVA: 0x0000332C File Offset: 0x0000152C
		public static Dictionary<string, object> JsonDecode(string json)
		{
			return (Dictionary<string, object>)Json.Deserialize(json);
		}

		// Token: 0x06000080 RID: 128 RVA: 0x000022FC File Offset: 0x000004FC
		public MiniJson()
		{
		}
	}
}
