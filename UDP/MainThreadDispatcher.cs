﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.UDP
{
	// Token: 0x02000004 RID: 4
	[HideInInspector]
	internal class MainThreadDispatcher : MonoBehaviour
	{
		// Token: 0x06000009 RID: 9 RVA: 0x0000211C File Offset: 0x0000031C
		public static void RunOnMainThread(Action runnable)
		{
			List<Action> obj = MainThreadDispatcher.s_Callbacks;
			lock (obj)
			{
				MainThreadDispatcher.s_Callbacks.Add(runnable);
				MainThreadDispatcher.s_CallbacksPending = true;
			}
		}

		// Token: 0x0600000A RID: 10 RVA: 0x00002164 File Offset: 0x00000364
		public static void DispatchDelayJob(float waitTime, Action runnable)
		{
			List<Action> obj = MainThreadDispatcher.s_Callbacks;
			lock (obj)
			{
				MainThreadDispatcher.delayAction[waitTime] = runnable;
				MainThreadDispatcher.s_CallbacksPending = true;
			}
		}

		// Token: 0x0600000B RID: 11 RVA: 0x000021AC File Offset: 0x000003AC
		private IEnumerator<WaitForSeconds> WaitAndDo(float waitTime, Action runnable)
		{
			yield return new WaitForSeconds(waitTime);
			runnable();
			yield break;
		}

		// Token: 0x0600000C RID: 12 RVA: 0x000021C2 File Offset: 0x000003C2
		private void Start()
		{
			Object.DontDestroyOnLoad(base.gameObject);
		}

		// Token: 0x0600000D RID: 13 RVA: 0x000021D0 File Offset: 0x000003D0
		private void Update()
		{
			if (!MainThreadDispatcher.s_CallbacksPending)
			{
				return;
			}
			List<Action> obj = MainThreadDispatcher.s_Callbacks;
			Action[] array;
			Dictionary<float, Action> dictionary;
			lock (obj)
			{
				if (MainThreadDispatcher.s_Callbacks.Count == 0 && MainThreadDispatcher.delayAction.Count == 0)
				{
					return;
				}
				array = new Action[MainThreadDispatcher.s_Callbacks.Count];
				MainThreadDispatcher.s_Callbacks.CopyTo(array);
				MainThreadDispatcher.s_Callbacks.Clear();
				dictionary = new Dictionary<float, Action>(MainThreadDispatcher.delayAction);
				MainThreadDispatcher.delayAction.Clear();
				MainThreadDispatcher.s_CallbacksPending = false;
			}
			Action[] array2 = array;
			for (int i = 0; i < array2.Length; i++)
			{
				array2[i]();
			}
			foreach (KeyValuePair<float, Action> keyValuePair in dictionary)
			{
				base.StartCoroutine(this.WaitAndDo(keyValuePair.Key, keyValuePair.Value));
			}
		}

		// Token: 0x0600000E RID: 14 RVA: 0x00002108 File Offset: 0x00000308
		public MainThreadDispatcher()
		{
		}

		// Token: 0x0600000F RID: 15 RVA: 0x000022DC File Offset: 0x000004DC
		// Note: this type is marked as 'beforefieldinit'.
		static MainThreadDispatcher()
		{
		}

		// Token: 0x0400000F RID: 15
		public static readonly string OBJECT_NAME = "UnityChannelMainThreadDispatcher";

		// Token: 0x04000010 RID: 16
		private static List<Action> s_Callbacks = new List<Action>();

		// Token: 0x04000011 RID: 17
		private static Dictionary<float, Action> delayAction = new Dictionary<float, Action>();

		// Token: 0x04000012 RID: 18
		private static volatile bool s_CallbacksPending;

		// Token: 0x02000026 RID: 38
		[CompilerGenerated]
		private sealed class <WaitAndDo>d__6 : IEnumerator<WaitForSeconds>, IEnumerator, IDisposable
		{
			// Token: 0x060000EA RID: 234 RVA: 0x00003EC1 File Offset: 0x000020C1
			[DebuggerHidden]
			public <WaitAndDo>d__6(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x060000EB RID: 235 RVA: 0x000020E3 File Offset: 0x000002E3
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x060000EC RID: 236 RVA: 0x00003ED0 File Offset: 0x000020D0
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				if (num == 0)
				{
					this.<>1__state = -1;
					this.<>2__current = new WaitForSeconds(waitTime);
					this.<>1__state = 1;
					return true;
				}
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				runnable();
				return false;
			}

			// Token: 0x17000022 RID: 34
			// (get) Token: 0x060000ED RID: 237 RVA: 0x00003F21 File Offset: 0x00002121
			WaitForSeconds IEnumerator<WaitForSeconds>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060000EE RID: 238 RVA: 0x00003F29 File Offset: 0x00002129
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000023 RID: 35
			// (get) Token: 0x060000EF RID: 239 RVA: 0x00003F21 File Offset: 0x00002121
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x04000085 RID: 133
			private int <>1__state;

			// Token: 0x04000086 RID: 134
			private WaitForSeconds <>2__current;

			// Token: 0x04000087 RID: 135
			public float waitTime;

			// Token: 0x04000088 RID: 136
			public Action runnable;
		}
	}
}
