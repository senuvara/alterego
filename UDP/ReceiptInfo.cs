﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.UDP
{
	// Token: 0x0200000D RID: 13
	public class ReceiptInfo
	{
		// Token: 0x17000013 RID: 19
		// (get) Token: 0x0600005D RID: 93 RVA: 0x00002A9B File Offset: 0x00000C9B
		// (set) Token: 0x0600005E RID: 94 RVA: 0x00002AA3 File Offset: 0x00000CA3
		public string gameOrderId
		{
			[CompilerGenerated]
			get
			{
				return this.<gameOrderId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<gameOrderId>k__BackingField = value;
			}
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x0600005F RID: 95 RVA: 0x00002AAC File Offset: 0x00000CAC
		// (set) Token: 0x06000060 RID: 96 RVA: 0x00002AB4 File Offset: 0x00000CB4
		public string signData
		{
			[CompilerGenerated]
			get
			{
				return this.<signData>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<signData>k__BackingField = value;
			}
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000061 RID: 97 RVA: 0x00002ABD File Offset: 0x00000CBD
		// (set) Token: 0x06000062 RID: 98 RVA: 0x00002AC5 File Offset: 0x00000CC5
		public string signature
		{
			[CompilerGenerated]
			get
			{
				return this.<signature>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<signature>k__BackingField = value;
			}
		}

		// Token: 0x06000063 RID: 99 RVA: 0x000022FC File Offset: 0x000004FC
		public ReceiptInfo()
		{
		}

		// Token: 0x04000036 RID: 54
		[CompilerGenerated]
		private string <gameOrderId>k__BackingField;

		// Token: 0x04000037 RID: 55
		[CompilerGenerated]
		private string <signData>k__BackingField;

		// Token: 0x04000038 RID: 56
		[CompilerGenerated]
		private string <signature>k__BackingField;
	}
}
