﻿using System;

namespace UnityEngine.UDP
{
	// Token: 0x02000005 RID: 5
	public class ResultCode
	{
		// Token: 0x06000010 RID: 16 RVA: 0x000022FC File Offset: 0x000004FC
		public ResultCode()
		{
		}

		// Token: 0x04000013 RID: 19
		public const int SDK_INIT_SUCCESS = 0;

		// Token: 0x04000014 RID: 20
		public const int SDK_INIT_ERROR = -100;

		// Token: 0x04000015 RID: 21
		public const int SDK_NOT_INIT = -101;

		// Token: 0x04000016 RID: 22
		public const int SDK_PURCHASE_SUCCESS = 0;

		// Token: 0x04000017 RID: 23
		public const int SDK_PURCHASE_FAILED = -300;

		// Token: 0x04000018 RID: 24
		public const int SDK_PURCHASE_CANCEL = -301;

		// Token: 0x04000019 RID: 25
		public const int SDK_PURCHASE_REPEAT = -302;

		// Token: 0x0400001A RID: 26
		public const int SDK_CONSUME_PURCHASE_SUCCESS = 0;

		// Token: 0x0400001B RID: 27
		public const int SDK_CONSUME_PURCHASE_FAILED = -600;

		// Token: 0x0400001C RID: 28
		public const int SDK_QUERY_INVENTORY_SUCCESS = 0;

		// Token: 0x0400001D RID: 29
		public const int SDK_QUERY_INVENTORY_FAILED = -700;

		// Token: 0x0400001E RID: 30
		public const int SDK_SERVER_INVALID = -999;
	}
}
