﻿using System;

namespace UnityEngine.UDP
{
	// Token: 0x02000012 RID: 18
	public class Utils
	{
		// Token: 0x0600007C RID: 124 RVA: 0x000022FC File Offset: 0x000004FC
		public Utils()
		{
		}

		// Token: 0x0600007D RID: 125 RVA: 0x0000330C File Offset: 0x0000150C
		// Note: this type is marked as 'beforefieldinit'.
		static Utils()
		{
		}

		// Token: 0x04000042 RID: 66
		public static int[] RETRY_WAIT_TIME = new int[]
		{
			0,
			3,
			10,
			60,
			180,
			300
		};
	}
}
