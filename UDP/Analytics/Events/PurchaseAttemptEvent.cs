﻿using System;
using System.Collections.Generic;
using UnityEngine.UDP.Common;

namespace UnityEngine.UDP.Analytics.Events
{
	// Token: 0x02000022 RID: 34
	internal class PurchaseAttemptEvent : AndroidJavaProxy
	{
		// Token: 0x060000E1 RID: 225 RVA: 0x00003D58 File Offset: 0x00001F58
		public PurchaseAttemptEvent(string productId, string uuid) : base("com.unity.udp.sdk.internal.analytics.IEvent")
		{
			SessionInfo sessionInfo = AnalyticsClient.GetSessionInfo();
			this._params = Common.GetCommonParams(sessionInfo);
			this._params.Add("product_id", productId);
			this._params.Add("cp_order_id", uuid);
		}

		// Token: 0x060000E2 RID: 226 RVA: 0x00003DA4 File Offset: 0x00001FA4
		public string GetEventName()
		{
			return "appRuntimePurchaseAttempt";
		}

		// Token: 0x060000E3 RID: 227 RVA: 0x00003DAB File Offset: 0x00001FAB
		public string GetParams()
		{
			return MiniJson.JsonEncode(this._params);
		}

		// Token: 0x0400007E RID: 126
		private const string EVENT_NAME = "appRuntimePurchaseAttempt";

		// Token: 0x0400007F RID: 127
		private readonly Dictionary<string, object> _params;
	}
}
