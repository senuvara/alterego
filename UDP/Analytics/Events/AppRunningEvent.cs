﻿using System;
using System.Collections.Generic;
using UnityEngine.UDP.Common;

namespace UnityEngine.UDP.Analytics.Events
{
	// Token: 0x0200001F RID: 31
	internal class AppRunningEvent : AndroidJavaProxy
	{
		// Token: 0x060000D8 RID: 216 RVA: 0x00003CB9 File Offset: 0x00001EB9
		public AppRunningEvent(SessionInfo sessionInfo, ulong duration) : base("com.unity.udp.sdk.internal.analytics.IEvent")
		{
			this._params = Common.GetCommonParams(sessionInfo);
			this._params.Add("duration", duration);
		}

		// Token: 0x060000D9 RID: 217 RVA: 0x00003CE8 File Offset: 0x00001EE8
		public string GetEventName()
		{
			return "appRuntimeAppRunning";
		}

		// Token: 0x060000DA RID: 218 RVA: 0x00003CEF File Offset: 0x00001EEF
		public string GetParams()
		{
			return MiniJson.JsonEncode(this._params);
		}

		// Token: 0x04000078 RID: 120
		private const string EVENT_NAME = "appRuntimeAppRunning";

		// Token: 0x04000079 RID: 121
		private Dictionary<string, object> _params;
	}
}
