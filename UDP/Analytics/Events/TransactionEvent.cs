﻿using System;
using System.Collections.Generic;
using UnityEngine.UDP.Common;

namespace UnityEngine.UDP.Analytics.Events
{
	// Token: 0x02000023 RID: 35
	internal class TransactionEvent : AndroidJavaProxy
	{
		// Token: 0x060000E4 RID: 228 RVA: 0x00003DB8 File Offset: 0x00001FB8
		public TransactionEvent(string cpOrderId, string productId, string currency, string price, string receipt) : base("com.unity.udp.sdk.internal.analytics.IEvent")
		{
			SessionInfo sessionInfo = AnalyticsClient.GetSessionInfo();
			this._params = Common.GetCommonParams(sessionInfo);
			this._params.Add("cp_order_id", cpOrderId);
			this._params.Add("product_id", productId);
			this._params.Add("receipt", receipt);
			this._params.Add("currency", currency);
			this._params.Add("price", price);
		}

		// Token: 0x060000E5 RID: 229 RVA: 0x00003E39 File Offset: 0x00002039
		public string GetEventName()
		{
			return "appRuntimeTransaction";
		}

		// Token: 0x060000E6 RID: 230 RVA: 0x00003E40 File Offset: 0x00002040
		public string GetParams()
		{
			return MiniJson.JsonEncode(this._params);
		}

		// Token: 0x04000080 RID: 128
		private const string EVENT_NAME = "appRuntimeTransaction";

		// Token: 0x04000081 RID: 129
		private readonly Dictionary<string, object> _params;
	}
}
