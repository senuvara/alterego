﻿using System;
using System.Collections.Generic;
using UnityEngine.UDP.Common;

namespace UnityEngine.UDP.Analytics.Events
{
	// Token: 0x02000020 RID: 32
	internal class AppStartEvent : AndroidJavaProxy
	{
		// Token: 0x060000DB RID: 219 RVA: 0x00003CFC File Offset: 0x00001EFC
		public AppStartEvent(SessionInfo sessionInfo) : base("com.unity.udp.sdk.internal.analytics.IEvent")
		{
			this._params = Common.GetCommonParams(sessionInfo);
		}

		// Token: 0x060000DC RID: 220 RVA: 0x00003D15 File Offset: 0x00001F15
		public string GetEventName()
		{
			return "appRuntimeAppStart";
		}

		// Token: 0x060000DD RID: 221 RVA: 0x00003D1C File Offset: 0x00001F1C
		public string GetParams()
		{
			return MiniJson.JsonEncode(this._params);
		}

		// Token: 0x0400007A RID: 122
		private const string EVENT_NAME = "appRuntimeAppStart";

		// Token: 0x0400007B RID: 123
		private readonly Dictionary<string, object> _params;
	}
}
