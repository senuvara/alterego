﻿using System;
using System.Collections.Generic;
using UnityEngine.UDP.Common;

namespace UnityEngine.UDP.Analytics.Events
{
	// Token: 0x02000021 RID: 33
	internal class AppStopEvent : AndroidJavaProxy
	{
		// Token: 0x060000DE RID: 222 RVA: 0x00003D29 File Offset: 0x00001F29
		public AppStopEvent(SessionInfo sessionInfo) : base("com.unity.udp.sdk.internal.analytics.IEvent")
		{
			this._params = Common.GetCommonParams(sessionInfo);
		}

		// Token: 0x060000DF RID: 223 RVA: 0x00003D42 File Offset: 0x00001F42
		public string GetEventName()
		{
			return "appRuntimeAppStop";
		}

		// Token: 0x060000E0 RID: 224 RVA: 0x00003D49 File Offset: 0x00001F49
		public string GetParams()
		{
			return MiniJson.JsonEncode(this._params);
		}

		// Token: 0x0400007C RID: 124
		private const string EVENT_NAME = "appRuntimeAppStop";

		// Token: 0x0400007D RID: 125
		private readonly Dictionary<string, object> _params;
	}
}
