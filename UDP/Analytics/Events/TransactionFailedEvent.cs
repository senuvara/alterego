﻿using System;
using System.Collections.Generic;
using UnityEngine.UDP.Common;

namespace UnityEngine.UDP.Analytics.Events
{
	// Token: 0x02000024 RID: 36
	internal class TransactionFailedEvent : AndroidJavaProxy
	{
		// Token: 0x060000E7 RID: 231 RVA: 0x00003E50 File Offset: 0x00002050
		public TransactionFailedEvent(string cpOrderId, string productId, string reason) : base("com.unity.udp.sdk.internal.analytics.IEvent")
		{
			SessionInfo sessionInfo = AnalyticsClient.GetSessionInfo();
			this._params = Common.GetCommonParams(sessionInfo);
			this._params.Add("cp_order_id", cpOrderId);
			this._params.Add("product_id", productId);
			this._params.Add("reason", reason);
		}

		// Token: 0x060000E8 RID: 232 RVA: 0x00003EAD File Offset: 0x000020AD
		public string GetEventName()
		{
			return "appRuntimeFailedTransaction";
		}

		// Token: 0x060000E9 RID: 233 RVA: 0x00003EB4 File Offset: 0x000020B4
		public string GetParams()
		{
			return MiniJson.JsonEncode(this._params);
		}

		// Token: 0x04000082 RID: 130
		private const string EVENT_NAME = "appRuntimeFailedTransaction";

		// Token: 0x04000083 RID: 131
		private readonly Dictionary<string, object> _params;
	}
}
