﻿using System;
using System.Collections.Generic;
using UnityEngine.UDP.Common;

namespace UnityEngine.UDP.Analytics.Events
{
	// Token: 0x0200001E RID: 30
	internal class AppInstallEvent : AndroidJavaProxy
	{
		// Token: 0x060000D5 RID: 213 RVA: 0x00003C8C File Offset: 0x00001E8C
		public AppInstallEvent(SessionInfo sessionInfo) : base("com.unity.udp.sdk.internal.analytics.IEvent")
		{
			this._params = Common.GetCommonParams(sessionInfo);
		}

		// Token: 0x060000D6 RID: 214 RVA: 0x00003CA5 File Offset: 0x00001EA5
		public string GetEventName()
		{
			return "appRuntimeAppInstall";
		}

		// Token: 0x060000D7 RID: 215 RVA: 0x00003CAC File Offset: 0x00001EAC
		public string GetParams()
		{
			return MiniJson.JsonEncode(this._params);
		}

		// Token: 0x04000076 RID: 118
		private const string EVENT_NAME = "appRuntimeAppInstall";

		// Token: 0x04000077 RID: 119
		private readonly Dictionary<string, object> _params;
	}
}
