﻿using System;

namespace UnityEngine.UDP.Analytics
{
	// Token: 0x02000017 RID: 23
	internal class EventDispatcher
	{
		// Token: 0x060000A5 RID: 165 RVA: 0x000037D6 File Offset: 0x000019D6
		private static void init()
		{
			EventDispatcher.serviceClass = new AndroidJavaClass("com.unity.udp.sdk.internal.analytics.Dispatcher");
		}

		// Token: 0x060000A6 RID: 166 RVA: 0x000037E7 File Offset: 0x000019E7
		public static void DispatchEvent(object e)
		{
			if (EventDispatcher.serviceClass == null)
			{
				EventDispatcher.init();
				return;
			}
			EventDispatcher.serviceClass.CallStatic("Send", new object[]
			{
				e
			});
		}

		// Token: 0x060000A7 RID: 167 RVA: 0x000022FC File Offset: 0x000004FC
		public EventDispatcher()
		{
		}

		// Token: 0x0400004B RID: 75
		private const string ANALYTICS_DISPATCHER_SERVICE = "com.unity.udp.sdk.internal.analytics.Dispatcher";

		// Token: 0x0400004C RID: 76
		private static AndroidJavaClass serviceClass;
	}
}
