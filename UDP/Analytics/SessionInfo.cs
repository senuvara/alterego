﻿using System;

namespace UnityEngine.UDP.Analytics
{
	// Token: 0x0200001D RID: 29
	[HideInInspector]
	internal class SessionInfo
	{
		// Token: 0x1700001A RID: 26
		// (get) Token: 0x060000C4 RID: 196 RVA: 0x00003C04 File Offset: 0x00001E04
		// (set) Token: 0x060000C5 RID: 197 RVA: 0x00003C0C File Offset: 0x00001E0C
		public string MAppId
		{
			get
			{
				return this.m_AppId;
			}
			set
			{
				this.m_AppId = value;
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x060000C6 RID: 198 RVA: 0x00003C15 File Offset: 0x00001E15
		// (set) Token: 0x060000C7 RID: 199 RVA: 0x00003C1D File Offset: 0x00001E1D
		public string MSessionId
		{
			get
			{
				return this.m_SessionId;
			}
			set
			{
				this.m_SessionId = value;
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x060000C8 RID: 200 RVA: 0x00003C26 File Offset: 0x00001E26
		// (set) Token: 0x060000C9 RID: 201 RVA: 0x00003C2E File Offset: 0x00001E2E
		public string MClientId
		{
			get
			{
				return this.m_ClientId;
			}
			set
			{
				this.m_ClientId = value;
			}
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x060000CA RID: 202 RVA: 0x00003C37 File Offset: 0x00001E37
		// (set) Token: 0x060000CB RID: 203 RVA: 0x00003C3F File Offset: 0x00001E3F
		public string MDeviceId
		{
			get
			{
				return this.m_DeviceId;
			}
			set
			{
				this.m_DeviceId = value;
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x060000CC RID: 204 RVA: 0x00003C48 File Offset: 0x00001E48
		// (set) Token: 0x060000CD RID: 205 RVA: 0x00003C50 File Offset: 0x00001E50
		public string MPlatform
		{
			get
			{
				return this.m_Platform;
			}
			set
			{
				this.m_Platform = value;
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x060000CE RID: 206 RVA: 0x00003C59 File Offset: 0x00001E59
		// (set) Token: 0x060000CF RID: 207 RVA: 0x00003C61 File Offset: 0x00001E61
		public string MTargetStore
		{
			get
			{
				return this.m_TargetStore;
			}
			set
			{
				this.m_TargetStore = value;
			}
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x060000D0 RID: 208 RVA: 0x00003C6A File Offset: 0x00001E6A
		// (set) Token: 0x060000D1 RID: 209 RVA: 0x00003C72 File Offset: 0x00001E72
		public string MSystemInfo
		{
			get
			{
				return this.m_SystemInfo;
			}
			set
			{
				this.m_SystemInfo = value;
			}
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x060000D2 RID: 210 RVA: 0x00003C7B File Offset: 0x00001E7B
		// (set) Token: 0x060000D3 RID: 211 RVA: 0x00003C83 File Offset: 0x00001E83
		public bool MVr
		{
			get
			{
				return this.m_Vr;
			}
			set
			{
				this.m_Vr = value;
			}
		}

		// Token: 0x060000D4 RID: 212 RVA: 0x000022FC File Offset: 0x000004FC
		public SessionInfo()
		{
		}

		// Token: 0x0400006E RID: 110
		private string m_AppId;

		// Token: 0x0400006F RID: 111
		private string m_SessionId;

		// Token: 0x04000070 RID: 112
		private string m_ClientId;

		// Token: 0x04000071 RID: 113
		private string m_DeviceId;

		// Token: 0x04000072 RID: 114
		private string m_Platform;

		// Token: 0x04000073 RID: 115
		private string m_TargetStore;

		// Token: 0x04000074 RID: 116
		private string m_SystemInfo;

		// Token: 0x04000075 RID: 117
		private bool m_Vr;
	}
}
