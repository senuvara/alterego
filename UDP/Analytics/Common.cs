﻿using System;
using System.Collections.Generic;

namespace UnityEngine.UDP.Analytics
{
	// Token: 0x02000018 RID: 24
	internal class Common
	{
		// Token: 0x060000A8 RID: 168 RVA: 0x00003810 File Offset: 0x00001A10
		public static Dictionary<string, object> GetCommonParams(SessionInfo sessionInfo)
		{
			return new Dictionary<string, object>
			{
				{
					"client_id",
					sessionInfo.MClientId
				},
				{
					"device_id",
					sessionInfo.MDeviceId
				},
				{
					"event_type",
					"runtime"
				},
				{
					"platform",
					sessionInfo.MPlatform
				},
				{
					"system_info",
					sessionInfo.MSystemInfo
				},
				{
					"target_store",
					sessionInfo.MTargetStore
				},
				{
					"vr",
					sessionInfo.MVr
				},
				{
					"ts",
					PlatformWrapper.GetCurrentMillisecondsInUTC()
				},
				{
					"source",
					"sdk"
				}
			};
		}

		// Token: 0x060000A9 RID: 169 RVA: 0x000022FC File Offset: 0x000004FC
		public Common()
		{
		}

		// Token: 0x0400004D RID: 77
		private const string k_ClientId = "client_id";

		// Token: 0x0400004E RID: 78
		private const string k_DeviceId = "device_id";

		// Token: 0x0400004F RID: 79
		private const string k_EventType = "event_type";

		// Token: 0x04000050 RID: 80
		private const string k_Platform = "platform";

		// Token: 0x04000051 RID: 81
		private const string k_SystemInfo = "system_info";

		// Token: 0x04000052 RID: 82
		private const string k_TargetStore = "target_store";

		// Token: 0x04000053 RID: 83
		private const string k_TimeStamp = "ts";

		// Token: 0x04000054 RID: 84
		private const string k_Vr = "vr";

		// Token: 0x04000055 RID: 85
		private const string m_EventType_Runtime = "runtime";

		// Token: 0x04000056 RID: 86
		private const string k_Source = "source";

		// Token: 0x04000057 RID: 87
		private const string k_Source_Value = "sdk";

		// Token: 0x04000058 RID: 88
		public const string k_Duration = "duration";

		// Token: 0x04000059 RID: 89
		public const string k_Currency = "currency";

		// Token: 0x0400005A RID: 90
		public const string k_Price = "price";

		// Token: 0x0400005B RID: 91
		public const string k_ProductId = "product_id";

		// Token: 0x0400005C RID: 92
		public const string k_Receipt = "receipt";

		// Token: 0x0400005D RID: 93
		public const string k_CpOrderId = "cp_order_id";

		// Token: 0x0400005E RID: 94
		public const string k_Reason = "reason";
	}
}
