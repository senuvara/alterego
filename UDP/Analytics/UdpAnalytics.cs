﻿using System;
using UnityEngine.UDP.Analytics.Events;

namespace UnityEngine.UDP.Analytics
{
	// Token: 0x0200001C RID: 28
	public class UdpAnalytics
	{
		// Token: 0x060000BE RID: 190 RVA: 0x00003B82 File Offset: 0x00001D82
		public static AnalyticsResult Transaction(string productionId, string price, string currency, string receipt, string cpOrderId)
		{
			if (!UdpAnalytics.isInitialized())
			{
				return AnalyticsResult.kNotInitialized;
			}
			if (string.IsNullOrEmpty(productionId))
			{
				return AnalyticsResult.kInvalidData;
			}
			return UdpAnalytics.dispatchEvent(new TransactionEvent(cpOrderId, productionId, currency, price, receipt));
		}

		// Token: 0x060000BF RID: 191 RVA: 0x00003BA7 File Offset: 0x00001DA7
		public static AnalyticsResult TransactionFailed(string productionId, string cpOrderId, string reason)
		{
			if (!UdpAnalytics.isInitialized())
			{
				return AnalyticsResult.kNotInitialized;
			}
			if (string.IsNullOrEmpty(productionId))
			{
				return AnalyticsResult.kInvalidData;
			}
			return UdpAnalytics.dispatchEvent(new TransactionFailedEvent(cpOrderId, productionId, reason));
		}

		// Token: 0x060000C0 RID: 192 RVA: 0x00003BC9 File Offset: 0x00001DC9
		public static AnalyticsResult PurchaseAttempt(string productionId, string uuid)
		{
			if (!UdpAnalytics.isInitialized())
			{
				return AnalyticsResult.kNotInitialized;
			}
			if (string.IsNullOrEmpty(productionId))
			{
				return AnalyticsResult.kInvalidData;
			}
			return UdpAnalytics.dispatchEvent(new PurchaseAttemptEvent(productionId, uuid));
		}

		// Token: 0x060000C1 RID: 193 RVA: 0x00003BEA File Offset: 0x00001DEA
		private static AnalyticsResult dispatchEvent(object e)
		{
			EventDispatcher.DispatchEvent(e);
			return AnalyticsResult.kOk;
		}

		// Token: 0x060000C2 RID: 194 RVA: 0x00003BF3 File Offset: 0x00001DF3
		private static bool isInitialized()
		{
			return !string.IsNullOrEmpty(AnalyticsClient.GetSessionId());
		}

		// Token: 0x060000C3 RID: 195 RVA: 0x000022FC File Offset: 0x000004FC
		public UdpAnalytics()
		{
		}
	}
}
