﻿using System;

namespace UnityEngine.UDP.Analytics
{
	// Token: 0x0200001A RID: 26
	[HideInInspector]
	internal class AnalyticsService
	{
		// Token: 0x060000B5 RID: 181 RVA: 0x000039A8 File Offset: 0x00001BA8
		public static void Initialize()
		{
			AnalyticsService.m_PlayerSessionState = AnalyticsService.SessionState.kSessionStopped;
			AnalyticsService.m_PlayerSessionId = "";
			AnalyticsService.m_PlayerSessionElapsedTime = 0UL;
			AnalyticsService.m_PlayerSessionForegroundTime = 0UL;
			AnalyticsService.m_PlayerSessionBackgroundTime = 0UL;
		}

		// Token: 0x060000B6 RID: 182 RVA: 0x000039CF File Offset: 0x00001BCF
		public static void OnPlayerQuit()
		{
			AnalyticsService.onPlayerStateChanged(AnalyticsService.SessionState.kSessionStopped);
		}

		// Token: 0x060000B7 RID: 183 RVA: 0x000039CF File Offset: 0x00001BCF
		public static void OnExitPlayMode()
		{
			AnalyticsService.onPlayerStateChanged(AnalyticsService.SessionState.kSessionStopped);
		}

		// Token: 0x060000B8 RID: 184 RVA: 0x000039D7 File Offset: 0x00001BD7
		public static void OnPlayerPaused(bool paused)
		{
			AnalyticsService.onPlayerStateChanged(paused ? AnalyticsService.SessionState.kSessionPaused : AnalyticsService.SessionState.kSessionResumed);
		}

		// Token: 0x060000B9 RID: 185 RVA: 0x000039CF File Offset: 0x00001BCF
		public static void OnDidReloadMonoDomain()
		{
			AnalyticsService.onPlayerStateChanged(AnalyticsService.SessionState.kSessionStopped);
		}

		// Token: 0x060000BA RID: 186 RVA: 0x000039E5 File Offset: 0x00001BE5
		public static void OnAppAwake()
		{
			AnalyticsService.onPlayerStateChanged(AnalyticsService.SessionState.kSessionStarted);
		}

		// Token: 0x060000BB RID: 187 RVA: 0x000039F0 File Offset: 0x00001BF0
		private static void onPlayerStateChanged(AnalyticsService.SessionState sessionState)
		{
			if (AnalyticsService.m_PlayerSessionState == sessionState)
			{
				return;
			}
			if (AnalyticsService.m_PlayerSessionState == AnalyticsService.SessionState.kSessionStopped && sessionState != AnalyticsService.SessionState.kSessionStarted)
			{
				return;
			}
			ulong currentMillisecondsInUTC = PlatformWrapper.GetCurrentMillisecondsInUTC();
			AnalyticsService.m_PlayerSessionState = sessionState;
			if (sessionState == AnalyticsService.SessionState.kSessionStarted || sessionState == AnalyticsService.SessionState.kSessionResumed)
			{
				if (sessionState == AnalyticsService.SessionState.kSessionStarted)
				{
					AnalyticsService.m_PlayerSessionId = PlatformWrapper.GetPlayerPrefsString("udp.player_sessionId");
					AnalyticsService.m_PlayerSessionElapsedTime = PlatformWrapper.GetPlayerPrefsUInt64("udp.player_session_elapsed_time");
					AnalyticsService.m_PlayerSessionBackgroundTime = PlatformWrapper.GetPlayerPrefsUInt64("udp.player_session_background_time");
				}
				ulong num = currentMillisecondsInUTC - AnalyticsService.m_PlayerSessionBackgroundTime;
				AnalyticsService.m_PlayerSessionForegroundTime = currentMillisecondsInUTC;
				if (AnalyticsService.m_PlayerSessionId == "" || AnalyticsService.m_PlayerSessionElapsedTime == 0UL || num > 1800000UL)
				{
					AnalyticsService.m_PlayerSessionElapsedTime = 0UL;
					AnalyticsService.m_PlayerSessionId = PlatformWrapper.GenerateRandomId();
					PlatformWrapper.SetPlayerPrefsString("udp.player_sessionId", AnalyticsService.m_PlayerSessionId);
					PlatformWrapper.SetPlayerPrefsUInt64("udp.player_session_elapsed_time", AnalyticsService.m_PlayerSessionElapsedTime);
				}
			}
			else if (sessionState == AnalyticsService.SessionState.kSessionStopped)
			{
				PlatformWrapper.SetPlayerPrefsString("udp.player_sessionId", "");
				PlatformWrapper.SetPlayerPrefsUInt64("udp.player_session_elapsed_time", 0UL);
				PlatformWrapper.SetPlayerPrefsUInt64("udp.player_session_background_time", 0UL);
			}
			else
			{
				ulong num2 = 0UL;
				if (AnalyticsService.m_PlayerSessionForegroundTime != 0UL)
				{
					num2 = currentMillisecondsInUTC - AnalyticsService.m_PlayerSessionForegroundTime;
				}
				AnalyticsService.m_PlayerSessionElapsedTime += num2;
				AnalyticsService.m_PlayerSessionBackgroundTime = currentMillisecondsInUTC;
				PlatformWrapper.SetPlayerPrefsUInt64("udp.player_session_elapsed_time", AnalyticsService.m_PlayerSessionElapsedTime);
				PlatformWrapper.SetPlayerPrefsUInt64("udp.player_session_background_time", AnalyticsService.m_PlayerSessionBackgroundTime);
			}
			AnalyticsClient.OnPlayerSessionStateChanged(AnalyticsService.m_PlayerSessionState, AnalyticsService.m_PlayerSessionId, AnalyticsService.m_PlayerSessionElapsedTime);
		}

		// Token: 0x060000BC RID: 188 RVA: 0x00003B40 File Offset: 0x00001D40
		public static ulong GetPlayerSessionElapsedTime()
		{
			if (AnalyticsService.m_PlayerSessionState == AnalyticsService.SessionState.kSessionStarted || AnalyticsService.m_PlayerSessionState == AnalyticsService.SessionState.kSessionResumed)
			{
				ulong currentMillisecondsInUTC = PlatformWrapper.GetCurrentMillisecondsInUTC();
				ulong num = 0UL;
				if (AnalyticsService.m_PlayerSessionForegroundTime != 0UL)
				{
					num = currentMillisecondsInUTC - AnalyticsService.m_PlayerSessionForegroundTime;
				}
				return AnalyticsService.m_PlayerSessionElapsedTime + num;
			}
			return AnalyticsService.m_PlayerSessionElapsedTime;
		}

		// Token: 0x060000BD RID: 189 RVA: 0x000022FC File Offset: 0x000004FC
		public AnalyticsService()
		{
		}

		// Token: 0x04000060 RID: 96
		private const uint kResumeTimeoutInSeconds = 1800U;

		// Token: 0x04000061 RID: 97
		private const string kPlayerSessionIdKey = "udp.player_sessionId";

		// Token: 0x04000062 RID: 98
		private const string kPlayerSessionElapsedTime = "udp.player_session_elapsed_time";

		// Token: 0x04000063 RID: 99
		private const string kPlayerSessionBackgroundTime = "udp.player_session_background_time";

		// Token: 0x04000064 RID: 100
		private static AnalyticsService.SessionState m_PlayerSessionState;

		// Token: 0x04000065 RID: 101
		private static string m_PlayerSessionId;

		// Token: 0x04000066 RID: 102
		private static ulong m_PlayerSessionElapsedTime;

		// Token: 0x04000067 RID: 103
		private static ulong m_PlayerSessionForegroundTime;

		// Token: 0x04000068 RID: 104
		private static ulong m_PlayerSessionBackgroundTime;

		// Token: 0x02000030 RID: 48
		public enum SessionState
		{
			// Token: 0x040000A2 RID: 162
			kSessionStopped,
			// Token: 0x040000A3 RID: 163
			kSessionStarted,
			// Token: 0x040000A4 RID: 164
			kSessionPaused,
			// Token: 0x040000A5 RID: 165
			kSessionResumed
		}
	}
}
