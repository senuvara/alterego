﻿using System;

namespace UnityEngine.UDP.Analytics
{
	// Token: 0x0200001B RID: 27
	public enum AnalyticsResult
	{
		// Token: 0x0400006A RID: 106
		kOk,
		// Token: 0x0400006B RID: 107
		kError,
		// Token: 0x0400006C RID: 108
		kNotInitialized,
		// Token: 0x0400006D RID: 109
		kInvalidData
	}
}
