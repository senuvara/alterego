﻿using System;
using UnityEngine.UDP.Analytics.Events;

namespace UnityEngine.UDP.Analytics
{
	// Token: 0x02000016 RID: 22
	[HideInInspector]
	internal static class AnalyticsClient
	{
		// Token: 0x0600008E RID: 142 RVA: 0x000034DC File Offset: 0x000016DC
		public static void Initialize(string clientId, string appId, string targetStore)
		{
			AnalyticsClient.m_ClientId = clientId;
			AnalyticsClient.m_AppId = appId;
			AnalyticsClient.m_TargetStore = targetStore;
			AnalyticsClient.m_sessionInfo = new SessionInfo();
			AnalyticsClient.m_AppInstalled = PlatformWrapper.GetAppInstalled();
			AnalyticsClient.RequestStateChange(AnalyticsClient.State.kStateReady);
		}

		// Token: 0x0600008F RID: 143 RVA: 0x0000350B File Offset: 0x0000170B
		public static void OnPlayerSessionStateChanged(AnalyticsService.SessionState sessionState, string sessionId, ulong sessionElapsedTime)
		{
			AnalyticsClient.SetSessionId(sessionId);
			if (sessionState == AnalyticsService.SessionState.kSessionStopped)
			{
				AnalyticsClient.CloseService();
				return;
			}
			if (sessionState == AnalyticsService.SessionState.kSessionPaused)
			{
				AnalyticsClient.PauseSession();
				return;
			}
			if (sessionState == AnalyticsService.SessionState.kSessionResumed && !AnalyticsClient.m_IsNewSession)
			{
				AnalyticsClient.ResumeSession();
				return;
			}
			AnalyticsClient.StartSession();
		}

		// Token: 0x06000090 RID: 144 RVA: 0x00003542 File Offset: 0x00001742
		private static bool StartSession()
		{
			return AnalyticsClient.RequestStateChange(AnalyticsClient.State.kStateStarted);
		}

		// Token: 0x06000091 RID: 145 RVA: 0x00003542 File Offset: 0x00001742
		private static bool ResumeSession()
		{
			return AnalyticsClient.RequestStateChange(AnalyticsClient.State.kStateStarted);
		}

		// Token: 0x06000092 RID: 146 RVA: 0x0000354A File Offset: 0x0000174A
		private static bool PauseSession()
		{
			return AnalyticsClient.RequestStateChange(AnalyticsClient.State.kStatePaused);
		}

		// Token: 0x06000093 RID: 147 RVA: 0x00003552 File Offset: 0x00001752
		private static bool StopSession()
		{
			return AnalyticsClient.RequestStateChange(AnalyticsClient.State.kStateStopped);
		}

		// Token: 0x06000094 RID: 148 RVA: 0x0000355A File Offset: 0x0000175A
		private static bool CloseService()
		{
			if (AnalyticsClient.m_State == AnalyticsClient.State.kStateNotReady)
			{
				return false;
			}
			AnalyticsClient.StopSession();
			return true;
		}

		// Token: 0x06000095 RID: 149 RVA: 0x0000356C File Offset: 0x0000176C
		private static bool RequestStateChange(AnalyticsClient.State state)
		{
			bool result = false;
			if (!AnalyticsClient.m_InStateTransition)
			{
				AnalyticsClient.State nextState = AnalyticsClient.State.kStateNotReady;
				AnalyticsClient.m_InStateTransition = true;
				if (AnalyticsClient.DetermineNextState(state, ref nextState))
				{
					result = AnalyticsClient.ProcessState(nextState);
				}
				AnalyticsClient.m_InStateTransition = false;
			}
			return result;
		}

		// Token: 0x06000096 RID: 150 RVA: 0x000035A4 File Offset: 0x000017A4
		private static bool DetermineNextState(AnalyticsClient.State requestedState, ref AnalyticsClient.State nextState)
		{
			nextState = requestedState;
			if (requestedState == AnalyticsClient.m_State)
			{
				return false;
			}
			switch (AnalyticsClient.m_State)
			{
			case AnalyticsClient.State.kStateNotReady:
			case AnalyticsClient.State.kStateStopped:
				if (requestedState != AnalyticsClient.State.kStateReady)
				{
					return false;
				}
				break;
			case AnalyticsClient.State.kStateReady:
				if (requestedState == AnalyticsClient.State.kStateStarted)
				{
					nextState = AnalyticsClient.State.kStatePrepared;
				}
				else if (requestedState == AnalyticsClient.State.kStatePaused)
				{
					return false;
				}
				break;
			}
			return true;
		}

		// Token: 0x06000097 RID: 151 RVA: 0x000035FC File Offset: 0x000017FC
		private static bool ProcessState(AnalyticsClient.State nextState)
		{
			switch (nextState)
			{
			case AnalyticsClient.State.kStateReady:
				AnalyticsClient.OnEnterStateReady();
				AnalyticsClient.OnEnterStatePrepared();
				break;
			case AnalyticsClient.State.kStatePrepared:
				AnalyticsClient.OnEnterStatePrepared();
				break;
			case AnalyticsClient.State.kStateStarted:
				AnalyticsClient.OnEnterStateStarted();
				break;
			case AnalyticsClient.State.kStatePaused:
				AnalyticsClient.OnEnterStatePaused();
				break;
			case AnalyticsClient.State.kStateStopped:
				AnalyticsClient.OnEnterStateStopped();
				break;
			default:
				return false;
			}
			return true;
		}

		// Token: 0x06000098 RID: 152 RVA: 0x00003654 File Offset: 0x00001854
		private static void OnEnterStateReady()
		{
			AnalyticsClient.SetState(AnalyticsClient.State.kStateReady);
			AnalyticsClient.m_sessionInfo.MAppId = AnalyticsClient.m_AppId;
			AnalyticsClient.m_sessionInfo.MClientId = AnalyticsClient.m_ClientId;
			AnalyticsClient.m_sessionInfo.MTargetStore = AnalyticsClient.m_TargetStore;
			AnalyticsClient.m_sessionInfo.MPlatform = PlatformWrapper.GetRuntimePlatformString();
			AnalyticsClient.m_sessionInfo.MSystemInfo = PlatformWrapper.GetSystemInfo();
			AnalyticsClient.m_sessionInfo.MDeviceId = SystemInfo.deviceUniqueIdentifier;
			AnalyticsClient.m_sessionInfo.MVr = false;
		}

		// Token: 0x06000099 RID: 153 RVA: 0x000036CC File Offset: 0x000018CC
		private static void OnEnterStatePrepared()
		{
			AnalyticsClient.State state = AnalyticsClient.m_State;
			AnalyticsClient.SetState(AnalyticsClient.State.kStatePrepared);
		}

		// Token: 0x0600009A RID: 154 RVA: 0x000036DC File Offset: 0x000018DC
		private static void OnEnterStateStarted()
		{
			AnalyticsClient.SetState(AnalyticsClient.State.kStateStarted);
			if (AnalyticsClient.m_IsNewSession)
			{
				EventDispatcher.DispatchEvent(new AppStartEvent(AnalyticsClient.m_sessionInfo));
				if (!AnalyticsClient.m_AppInstalled)
				{
					EventDispatcher.DispatchEvent(new AppInstallEvent(AnalyticsClient.m_sessionInfo));
					AnalyticsClient.m_AppInstalled = true;
					AnalyticsClient.SavePersistentValue();
				}
			}
			AnalyticsClient.m_IsNewSession = false;
		}

		// Token: 0x0600009B RID: 155 RVA: 0x0000372C File Offset: 0x0000192C
		private static void OnEnterStatePaused()
		{
			AnalyticsClient.OnEnteringStatePausedOrStopped();
			AnalyticsClient.SetState(AnalyticsClient.State.kStatePaused);
		}

		// Token: 0x0600009C RID: 156 RVA: 0x00003739 File Offset: 0x00001939
		private static void OnEnterStateStopped()
		{
			if (AnalyticsClient.m_State == AnalyticsClient.State.kStateStarted)
			{
				AnalyticsClient.OnEnteringStatePausedOrStopped();
			}
			EventDispatcher.DispatchEvent(new AppStopEvent(AnalyticsClient.m_sessionInfo));
		}

		// Token: 0x0600009D RID: 157 RVA: 0x00003757 File Offset: 0x00001957
		private static void OnEnteringStatePausedOrStopped()
		{
			AnalyticsClient.SendAppRunningEvent();
			AnalyticsClient.SavePersistentValue();
		}

		// Token: 0x0600009E RID: 158 RVA: 0x00003763 File Offset: 0x00001963
		private static void SendAppRunningEvent()
		{
			EventDispatcher.DispatchEvent(new AppRunningEvent(AnalyticsClient.m_sessionInfo, AnalyticsClient.GetPlayerSessionElapsedTime()));
		}

		// Token: 0x0600009F RID: 159 RVA: 0x00003779 File Offset: 0x00001979
		private static void SavePersistentValue()
		{
			PlatformWrapper.SetAppInstalled(AnalyticsClient.m_AppInstalled);
		}

		// Token: 0x060000A0 RID: 160 RVA: 0x00003785 File Offset: 0x00001985
		private static ulong GetPlayerSessionElapsedTime()
		{
			return AnalyticsService.GetPlayerSessionElapsedTime();
		}

		// Token: 0x060000A1 RID: 161 RVA: 0x0000378C File Offset: 0x0000198C
		private static void SetSessionId(string sessionId)
		{
			AnalyticsClient.m_IsNewSession = (AnalyticsClient.m_sessionInfo.MSessionId != sessionId);
			AnalyticsClient.m_sessionInfo.MSessionId = sessionId;
		}

		// Token: 0x060000A2 RID: 162 RVA: 0x000037AE File Offset: 0x000019AE
		private static void SetState(AnalyticsClient.State state)
		{
			AnalyticsClient.m_State = state;
		}

		// Token: 0x060000A3 RID: 163 RVA: 0x000037B6 File Offset: 0x000019B6
		public static SessionInfo GetSessionInfo()
		{
			return AnalyticsClient.m_sessionInfo;
		}

		// Token: 0x060000A4 RID: 164 RVA: 0x000037BD File Offset: 0x000019BD
		public static string GetSessionId()
		{
			if (AnalyticsClient.m_sessionInfo != null)
			{
				return AnalyticsClient.m_sessionInfo.MSessionId;
			}
			return "";
		}

		// Token: 0x04000043 RID: 67
		private static SessionInfo m_sessionInfo;

		// Token: 0x04000044 RID: 68
		private static bool m_IsNewSession;

		// Token: 0x04000045 RID: 69
		private static AnalyticsClient.State m_State;

		// Token: 0x04000046 RID: 70
		private static bool m_InStateTransition;

		// Token: 0x04000047 RID: 71
		private static string m_AppId;

		// Token: 0x04000048 RID: 72
		private static string m_ClientId;

		// Token: 0x04000049 RID: 73
		private static string m_TargetStore;

		// Token: 0x0400004A RID: 74
		private static bool m_AppInstalled;

		// Token: 0x0200002F RID: 47
		private enum State
		{
			// Token: 0x0400009B RID: 155
			kStateNotReady,
			// Token: 0x0400009C RID: 156
			kStateReady,
			// Token: 0x0400009D RID: 157
			kStatePrepared,
			// Token: 0x0400009E RID: 158
			kStateStarted,
			// Token: 0x0400009F RID: 159
			kStatePaused,
			// Token: 0x040000A0 RID: 160
			kStateStopped
		}
	}
}
