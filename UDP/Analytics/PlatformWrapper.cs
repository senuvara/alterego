﻿using System;

namespace UnityEngine.UDP.Analytics
{
	// Token: 0x02000019 RID: 25
	internal class PlatformWrapper
	{
		// Token: 0x060000AA RID: 170 RVA: 0x000038C4 File Offset: 0x00001AC4
		public static ulong GetCurrentMillisecondsInUTC()
		{
			return (ulong)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
		}

		// Token: 0x060000AB RID: 171 RVA: 0x000038F4 File Offset: 0x00001AF4
		public static string GetRuntimePlatformString()
		{
			RuntimePlatform platform = Application.platform;
			if (platform <= RuntimePlatform.WindowsEditor)
			{
				if (platform != RuntimePlatform.OSXEditor && platform != RuntimePlatform.WindowsEditor)
				{
					goto IL_2B;
				}
			}
			else
			{
				if (platform == RuntimePlatform.Android)
				{
					return "Android";
				}
				if (platform != RuntimePlatform.LinuxEditor)
				{
					goto IL_2B;
				}
			}
			return "";
			IL_2B:
			return "";
		}

		// Token: 0x060000AC RID: 172 RVA: 0x00003931 File Offset: 0x00001B31
		public static string GetSystemInfo()
		{
			return SystemInfo.deviceModel;
		}

		// Token: 0x060000AD RID: 173 RVA: 0x00003938 File Offset: 0x00001B38
		public static string GetPlayerPrefsString(string name)
		{
			return PlayerPrefs.GetString(name, "");
		}

		// Token: 0x060000AE RID: 174 RVA: 0x00003945 File Offset: 0x00001B45
		public static void SetPlayerPrefsString(string name, string value)
		{
			PlayerPrefs.SetString(name, value);
		}

		// Token: 0x060000AF RID: 175 RVA: 0x0000394E File Offset: 0x00001B4E
		public static ulong GetPlayerPrefsUInt64(string name)
		{
			return (ulong)((long)PlayerPrefs.GetInt(name, 0));
		}

		// Token: 0x060000B0 RID: 176 RVA: 0x00003958 File Offset: 0x00001B58
		public static void SetPlayerPrefsUInt64(string name, ulong value)
		{
			PlayerPrefs.SetInt(name, (int)value);
		}

		// Token: 0x060000B1 RID: 177 RVA: 0x00003962 File Offset: 0x00001B62
		public static bool GetAppInstalled()
		{
			return PlayerPrefs.GetInt("udp.app_install", 0) != 0;
		}

		// Token: 0x060000B2 RID: 178 RVA: 0x00003972 File Offset: 0x00001B72
		public static void SetAppInstalled(bool v)
		{
			PlayerPrefs.SetInt("udp.app_install", v ? 1 : 0);
		}

		// Token: 0x060000B3 RID: 179 RVA: 0x00003988 File Offset: 0x00001B88
		public static string GenerateRandomId()
		{
			return Guid.NewGuid().ToString();
		}

		// Token: 0x060000B4 RID: 180 RVA: 0x000022FC File Offset: 0x000004FC
		public PlatformWrapper()
		{
		}

		// Token: 0x0400005F RID: 95
		private const string kAppInstall = "udp.app_install";
	}
}
