﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.UDP.Analytics;
using UnityEngine.UDP.Common;

namespace UnityEngine.UDP
{
	// Token: 0x0200000B RID: 11
	public class PurchaseForwardCallback : AndroidJavaProxy
	{
		// Token: 0x06000046 RID: 70 RVA: 0x00002616 File Offset: 0x00000816
		public PurchaseForwardCallback(IPurchaseListener purchaseListener) : base("com.unity.udp.sdk.PurchaseCallback")
		{
			this.purchaseListener = purchaseListener;
		}

		// Token: 0x06000047 RID: 71 RVA: 0x0000262C File Offset: 0x0000082C
		public void onPurchaseFinished(int resultCode, string message, string purchaseInfoString)
		{
			Debug.Log(string.Format("Purchased Finished. ResultCode: {0}, message: {1}, purchaseInfoString: {2}", resultCode, message, purchaseInfoString));
			if (this.purchaseListener == null)
			{
				return;
			}
			PurchaseInfo purchaseInfo = PurchaseForwardCallback.ConvertPurchaseInfo(purchaseInfoString);
			if (resultCode == 0)
			{
				MainThreadDispatcher.RunOnMainThread(delegate
				{
					this.purchaseListener.OnPurchase(purchaseInfo);
				});
				TransactionEventHandler listener = new TransactionEventHandler(purchaseInfo);
				StoreService.QueryInventory(new List<string>
				{
					purchaseInfo.ProductId
				}, listener);
				return;
			}
			MainThreadDispatcher.RunOnMainThread(delegate
			{
				this.purchaseListener.OnPurchaseFailed(message, purchaseInfo);
			});
			if (purchaseInfo != null)
			{
				UdpAnalytics.TransactionFailed(purchaseInfo.ProductId, purchaseInfo.GameOrderId, message);
			}
		}

		// Token: 0x06000048 RID: 72 RVA: 0x000026F8 File Offset: 0x000008F8
		public void onConsumeFinished(int resultCode, string message, string purchaseInfoString)
		{
			if (this.purchaseListener == null)
			{
				return;
			}
			PurchaseInfo purchaseInfo = PurchaseForwardCallback.ConvertPurchaseInfo(purchaseInfoString);
			Debug.Log("onConsumeFinished, purchaseInfoString: " + purchaseInfoString);
			if (resultCode != -600)
			{
				if (resultCode == 0)
				{
					MainThreadDispatcher.RunOnMainThread(delegate
					{
						this.purchaseListener.OnPurchaseConsume(purchaseInfo);
					});
					return;
				}
			}
			else
			{
				MainThreadDispatcher.RunOnMainThread(delegate
				{
					this.purchaseListener.OnPurchaseConsumeFailed(message, purchaseInfo);
				});
			}
		}

		// Token: 0x06000049 RID: 73 RVA: 0x0000276C File Offset: 0x0000096C
		public void onQueryInventory(int resultCode, string message, string inventoryString)
		{
			PurchaseForwardCallback.<>c__DisplayClass4_0 CS$<>8__locals1 = new PurchaseForwardCallback.<>c__DisplayClass4_0();
			CS$<>8__locals1.<>4__this = this;
			CS$<>8__locals1.message = message;
			Debug.Log("onQueryInventory, inventoryString: " + inventoryString);
			if (resultCode != -700)
			{
				if (resultCode == 0)
				{
					Inventory inventory = PurchaseForwardCallback.ConvertInventory(inventoryString);
					MainThreadDispatcher.RunOnMainThread(delegate
					{
						CS$<>8__locals1.<>4__this.purchaseListener.OnQueryInventory(inventory);
					});
					return;
				}
			}
			else
			{
				MainThreadDispatcher.RunOnMainThread(delegate
				{
					CS$<>8__locals1.<>4__this.purchaseListener.OnQueryInventoryFailed(CS$<>8__locals1.message);
				});
			}
		}

		// Token: 0x0600004A RID: 74 RVA: 0x000027E9 File Offset: 0x000009E9
		private static PurchaseInfo ConvertPurchaseInfo(string purchaseInfoString)
		{
			if (string.IsNullOrEmpty(purchaseInfoString))
			{
				return null;
			}
			return PurchaseForwardCallback.ConvertPurchaseInfo(MiniJson.JsonDecode(purchaseInfoString));
		}

		// Token: 0x0600004B RID: 75 RVA: 0x00002800 File Offset: 0x00000A00
		private static PurchaseInfo ConvertPurchaseInfo(Dictionary<string, object> purchaseInfoMap)
		{
			return new PurchaseInfo
			{
				GameOrderId = PurchaseForwardCallback.GetValueOfDictionary<string>(purchaseInfoMap, "gameOrderId", null),
				ItemType = PurchaseForwardCallback.GetValueOfDictionary<string>(purchaseInfoMap, "itemType", null),
				OrderQueryToken = PurchaseForwardCallback.GetValueOfDictionary<string>(purchaseInfoMap, "orderQueryToken", null),
				ProductId = PurchaseForwardCallback.GetValueOfDictionary<string>(purchaseInfoMap, "productId", null),
				StorePurchaseJsonString = PurchaseForwardCallback.GetValueOfDictionary<string>(purchaseInfoMap, "storePurchaseJsonString", null),
				DeveloperPayload = PurchaseForwardCallback.GetValueOfDictionary<string>(purchaseInfoMap, "developerPayload", null)
			};
		}

		// Token: 0x0600004C RID: 76 RVA: 0x0000287E File Offset: 0x00000A7E
		private static ProductInfo ConvertProductInfo(string productInfoString)
		{
			if (string.IsNullOrEmpty(productInfoString))
			{
				return null;
			}
			return PurchaseForwardCallback.ConvertProductInfo(MiniJson.JsonDecode(productInfoString));
		}

		// Token: 0x0600004D RID: 77 RVA: 0x00002898 File Offset: 0x00000A98
		private static ProductInfo ConvertProductInfo(Dictionary<string, object> productInfoMap)
		{
			return new ProductInfo
			{
				Consumable = new bool?(PurchaseForwardCallback.GetValueOfDictionary<bool>(productInfoMap, "consumable", false)),
				Currency = PurchaseForwardCallback.GetValueOfDictionary<string>(productInfoMap, "currency", null),
				Description = PurchaseForwardCallback.GetValueOfDictionary<string>(productInfoMap, "description", null),
				ItemType = PurchaseForwardCallback.GetValueOfDictionary<string>(productInfoMap, "itemType", null),
				Price = PurchaseForwardCallback.GetValueOfDictionary<string>(productInfoMap, "price", null),
				PriceAmountMicros = PurchaseForwardCallback.GetValueOfDictionary<long>(productInfoMap, "priceAmountMicros", 0L),
				ProductId = PurchaseForwardCallback.GetValueOfDictionary<string>(productInfoMap, "productId", null),
				Title = PurchaseForwardCallback.GetValueOfDictionary<string>(productInfoMap, "title", null)
			};
		}

		// Token: 0x0600004E RID: 78 RVA: 0x00002940 File Offset: 0x00000B40
		private static Inventory ConvertInventory(string inventoryString)
		{
			if (string.IsNullOrEmpty(inventoryString))
			{
				return null;
			}
			Inventory inventory = new Inventory();
			Dictionary<string, object> dictionary = MiniJson.JsonDecode(inventoryString);
			List<object> list = (List<object>)dictionary["purchases"];
			List<object> list2 = (List<object>)dictionary["products"];
			foreach (object obj in list)
			{
				inventory.AddPurchase(PurchaseForwardCallback.ConvertPurchaseInfo((Dictionary<string, object>)obj));
			}
			foreach (object obj2 in list2)
			{
				inventory.AddProduct(PurchaseForwardCallback.ConvertProductInfo((Dictionary<string, object>)obj2));
			}
			return inventory;
		}

		// Token: 0x0600004F RID: 79 RVA: 0x00002A1C File Offset: 0x00000C1C
		private static T GetValueOfDictionary<T>(IDictionary<string, object> dictionary, string key, T defaultValue)
		{
			if (dictionary.ContainsKey(key))
			{
				return (T)((object)dictionary[key]);
			}
			return defaultValue;
		}

		// Token: 0x0400002F RID: 47
		private IPurchaseListener purchaseListener;

		// Token: 0x02000027 RID: 39
		[CompilerGenerated]
		private sealed class <>c__DisplayClass2_0
		{
			// Token: 0x060000F0 RID: 240 RVA: 0x000022FC File Offset: 0x000004FC
			public <>c__DisplayClass2_0()
			{
			}

			// Token: 0x060000F1 RID: 241 RVA: 0x00003F30 File Offset: 0x00002130
			internal void <onPurchaseFinished>b__1()
			{
				this.<>4__this.purchaseListener.OnPurchase(this.purchaseInfo);
			}

			// Token: 0x060000F2 RID: 242 RVA: 0x00003F48 File Offset: 0x00002148
			internal void <onPurchaseFinished>b__0()
			{
				this.<>4__this.purchaseListener.OnPurchaseFailed(this.message, this.purchaseInfo);
			}

			// Token: 0x04000089 RID: 137
			public PurchaseForwardCallback <>4__this;

			// Token: 0x0400008A RID: 138
			public PurchaseInfo purchaseInfo;

			// Token: 0x0400008B RID: 139
			public string message;
		}

		// Token: 0x02000028 RID: 40
		[CompilerGenerated]
		private sealed class <>c__DisplayClass3_0
		{
			// Token: 0x060000F3 RID: 243 RVA: 0x000022FC File Offset: 0x000004FC
			public <>c__DisplayClass3_0()
			{
			}

			// Token: 0x060000F4 RID: 244 RVA: 0x00003F66 File Offset: 0x00002166
			internal void <onConsumeFinished>b__0()
			{
				this.<>4__this.purchaseListener.OnPurchaseConsume(this.purchaseInfo);
			}

			// Token: 0x060000F5 RID: 245 RVA: 0x00003F7E File Offset: 0x0000217E
			internal void <onConsumeFinished>b__1()
			{
				this.<>4__this.purchaseListener.OnPurchaseConsumeFailed(this.message, this.purchaseInfo);
			}

			// Token: 0x0400008C RID: 140
			public PurchaseForwardCallback <>4__this;

			// Token: 0x0400008D RID: 141
			public PurchaseInfo purchaseInfo;

			// Token: 0x0400008E RID: 142
			public string message;
		}

		// Token: 0x02000029 RID: 41
		[CompilerGenerated]
		private sealed class <>c__DisplayClass4_0
		{
			// Token: 0x060000F6 RID: 246 RVA: 0x000022FC File Offset: 0x000004FC
			public <>c__DisplayClass4_0()
			{
			}

			// Token: 0x060000F7 RID: 247 RVA: 0x00003F9C File Offset: 0x0000219C
			internal void <onQueryInventory>b__1()
			{
				this.<>4__this.purchaseListener.OnQueryInventoryFailed(this.message);
			}

			// Token: 0x0400008F RID: 143
			public PurchaseForwardCallback <>4__this;

			// Token: 0x04000090 RID: 144
			public string message;
		}

		// Token: 0x0200002A RID: 42
		[CompilerGenerated]
		private sealed class <>c__DisplayClass4_1
		{
			// Token: 0x060000F8 RID: 248 RVA: 0x000022FC File Offset: 0x000004FC
			public <>c__DisplayClass4_1()
			{
			}

			// Token: 0x060000F9 RID: 249 RVA: 0x00003FB4 File Offset: 0x000021B4
			internal void <onQueryInventory>b__0()
			{
				this.CS$<>8__locals1.<>4__this.purchaseListener.OnQueryInventory(this.inventory);
			}

			// Token: 0x04000091 RID: 145
			public Inventory inventory;

			// Token: 0x04000092 RID: 146
			public PurchaseForwardCallback.<>c__DisplayClass4_0 CS$<>8__locals1;
		}
	}
}
