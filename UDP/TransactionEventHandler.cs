﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.UDP.Analytics;

namespace UnityEngine.UDP
{
	// Token: 0x02000006 RID: 6
	public class TransactionEventHandler : IPurchaseListener
	{
		// Token: 0x06000011 RID: 17 RVA: 0x00002304 File Offset: 0x00000504
		public TransactionEventHandler(PurchaseInfo purchaseInfo)
		{
			this._purchaseInfo = purchaseInfo;
		}

		// Token: 0x06000012 RID: 18 RVA: 0x000020E3 File Offset: 0x000002E3
		public void OnPurchase(PurchaseInfo purchaseInfo)
		{
		}

		// Token: 0x06000013 RID: 19 RVA: 0x000020E3 File Offset: 0x000002E3
		public void OnPurchaseFailed(string message, PurchaseInfo purchaseInfo)
		{
		}

		// Token: 0x06000014 RID: 20 RVA: 0x000020E3 File Offset: 0x000002E3
		public void OnPurchaseRepeated(string productId)
		{
		}

		// Token: 0x06000015 RID: 21 RVA: 0x000020E3 File Offset: 0x000002E3
		public void OnPurchaseConsume(PurchaseInfo purchaseInfo)
		{
		}

		// Token: 0x06000016 RID: 22 RVA: 0x000020E3 File Offset: 0x000002E3
		public void OnPurchaseConsumeFailed(string message, PurchaseInfo purchaseInfo)
		{
		}

		// Token: 0x06000017 RID: 23 RVA: 0x00002314 File Offset: 0x00000514
		public void OnQueryInventory(Inventory inventory)
		{
			ProductInfo productInfo = inventory.GetProductInfo(this._purchaseInfo.ProductId);
			UdpAnalytics.Transaction(this._purchaseInfo.ProductId, productInfo.Price, productInfo.Currency, this._purchaseInfo.StorePurchaseJsonString, this._purchaseInfo.GameOrderId);
		}

		// Token: 0x06000018 RID: 24 RVA: 0x00002366 File Offset: 0x00000566
		public void OnQueryInventoryFailed(string message)
		{
			if (this.retryTime < Utils.RETRY_WAIT_TIME.Length)
			{
				this.retryTime++;
				MainThreadDispatcher.DispatchDelayJob((float)Utils.RETRY_WAIT_TIME[this.retryTime], delegate
				{
					StoreService.QueryInventory(new List<string>
					{
						this._purchaseInfo.ProductId
					}, this);
				});
			}
		}

		// Token: 0x06000019 RID: 25 RVA: 0x000023A3 File Offset: 0x000005A3
		[CompilerGenerated]
		private void <OnQueryInventoryFailed>b__9_0()
		{
			StoreService.QueryInventory(new List<string>
			{
				this._purchaseInfo.ProductId
			}, this);
		}

		// Token: 0x0400001F RID: 31
		private PurchaseInfo _purchaseInfo;

		// Token: 0x04000020 RID: 32
		private int retryTime;
	}
}
