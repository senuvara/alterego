﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.UDP
{
	// Token: 0x0200000C RID: 12
	[Serializable]
	public class PurchaseInfo
	{
		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000050 RID: 80 RVA: 0x00002A35 File Offset: 0x00000C35
		// (set) Token: 0x06000051 RID: 81 RVA: 0x00002A3D File Offset: 0x00000C3D
		public string ItemType
		{
			[CompilerGenerated]
			get
			{
				return this.<ItemType>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ItemType>k__BackingField = value;
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x06000052 RID: 82 RVA: 0x00002A46 File Offset: 0x00000C46
		// (set) Token: 0x06000053 RID: 83 RVA: 0x00002A4E File Offset: 0x00000C4E
		public string ProductId
		{
			[CompilerGenerated]
			get
			{
				return this.<ProductId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ProductId>k__BackingField = value;
			}
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000054 RID: 84 RVA: 0x00002A57 File Offset: 0x00000C57
		// (set) Token: 0x06000055 RID: 85 RVA: 0x00002A5F File Offset: 0x00000C5F
		public string GameOrderId
		{
			[CompilerGenerated]
			get
			{
				return this.<GameOrderId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<GameOrderId>k__BackingField = value;
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000056 RID: 86 RVA: 0x00002A68 File Offset: 0x00000C68
		// (set) Token: 0x06000057 RID: 87 RVA: 0x00002A70 File Offset: 0x00000C70
		public string OrderQueryToken
		{
			[CompilerGenerated]
			get
			{
				return this.<OrderQueryToken>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<OrderQueryToken>k__BackingField = value;
			}
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000058 RID: 88 RVA: 0x00002A79 File Offset: 0x00000C79
		// (set) Token: 0x06000059 RID: 89 RVA: 0x00002A81 File Offset: 0x00000C81
		public string DeveloperPayload
		{
			[CompilerGenerated]
			get
			{
				return this.<DeveloperPayload>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<DeveloperPayload>k__BackingField = value;
			}
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x0600005A RID: 90 RVA: 0x00002A8A File Offset: 0x00000C8A
		// (set) Token: 0x0600005B RID: 91 RVA: 0x00002A92 File Offset: 0x00000C92
		public string StorePurchaseJsonString
		{
			[CompilerGenerated]
			get
			{
				return this.<StorePurchaseJsonString>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<StorePurchaseJsonString>k__BackingField = value;
			}
		}

		// Token: 0x0600005C RID: 92 RVA: 0x000022FC File Offset: 0x000004FC
		public PurchaseInfo()
		{
		}

		// Token: 0x04000030 RID: 48
		[CompilerGenerated]
		private string <ItemType>k__BackingField;

		// Token: 0x04000031 RID: 49
		[CompilerGenerated]
		private string <ProductId>k__BackingField;

		// Token: 0x04000032 RID: 50
		[CompilerGenerated]
		private string <GameOrderId>k__BackingField;

		// Token: 0x04000033 RID: 51
		[CompilerGenerated]
		private string <OrderQueryToken>k__BackingField;

		// Token: 0x04000034 RID: 52
		[CompilerGenerated]
		private string <DeveloperPayload>k__BackingField;

		// Token: 0x04000035 RID: 53
		[CompilerGenerated]
		private string <StorePurchaseJsonString>k__BackingField;
	}
}
