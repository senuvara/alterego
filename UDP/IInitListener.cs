﻿using System;

namespace UnityEngine.UDP
{
	// Token: 0x0200000F RID: 15
	public interface IInitListener
	{
		// Token: 0x06000071 RID: 113
		void OnInitialized(UserInfo userInfo);

		// Token: 0x06000072 RID: 114
		void OnInitializeFailed(string message);
	}
}
