﻿using System;

namespace UnityEngine.UDP
{
	// Token: 0x02000002 RID: 2
	[Serializable]
	public class AppStoreSettings : ScriptableObject
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public AppStoreSettings()
		{
		}

		// Token: 0x04000001 RID: 1
		public string UnityProjectID = "";

		// Token: 0x04000002 RID: 2
		public string UnityClientID = "";

		// Token: 0x04000003 RID: 3
		public string UnityClientKey = "";

		// Token: 0x04000004 RID: 4
		public string UnityClientRSAPublicKey = "";

		// Token: 0x04000005 RID: 5
		public string AppName = "";

		// Token: 0x04000006 RID: 6
		public string AppSlug = "";

		// Token: 0x04000007 RID: 7
		public string AppItemId = "";

		// Token: 0x04000008 RID: 8
		public string Permission = "";

		// Token: 0x04000009 RID: 9
		internal const string resourceFileName = "UDP Settings";

		// Token: 0x0400000A RID: 10
		public const string appStoreSettingsAssetFolder = "Assets/Plugins/UDP/Resources";

		// Token: 0x0400000B RID: 11
		public const string appStoreSettingsAssetPath = "Assets/Plugins/UDP/Resources/UDP Settings.asset";

		// Token: 0x0400000C RID: 12
		public const string appStoreSettingsPropFolder = "Assets/Plugins/Android/assets";

		// Token: 0x0400000D RID: 13
		public const string appStoreSettingsPropPath = "Assets/Plugins/Android/assets/GameSettings.prop";
	}
}
