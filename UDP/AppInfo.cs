﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.UDP
{
	// Token: 0x0200000A RID: 10
	public class AppInfo
	{
		// Token: 0x17000009 RID: 9
		// (get) Token: 0x0600003D RID: 61 RVA: 0x000025D2 File Offset: 0x000007D2
		// (set) Token: 0x0600003E RID: 62 RVA: 0x000025DA File Offset: 0x000007DA
		public string ClientId
		{
			[CompilerGenerated]
			get
			{
				return this.<ClientId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ClientId>k__BackingField = value;
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x0600003F RID: 63 RVA: 0x000025E3 File Offset: 0x000007E3
		// (set) Token: 0x06000040 RID: 64 RVA: 0x000025EB File Offset: 0x000007EB
		public string AppSlug
		{
			[CompilerGenerated]
			get
			{
				return this.<AppSlug>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<AppSlug>k__BackingField = value;
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000041 RID: 65 RVA: 0x000025F4 File Offset: 0x000007F4
		// (set) Token: 0x06000042 RID: 66 RVA: 0x000025FC File Offset: 0x000007FC
		public string ClientKey
		{
			[CompilerGenerated]
			get
			{
				return this.<ClientKey>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ClientKey>k__BackingField = value;
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000043 RID: 67 RVA: 0x00002605 File Offset: 0x00000805
		// (set) Token: 0x06000044 RID: 68 RVA: 0x0000260D File Offset: 0x0000080D
		public string RSAPublicKey
		{
			[CompilerGenerated]
			get
			{
				return this.<RSAPublicKey>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<RSAPublicKey>k__BackingField = value;
			}
		}

		// Token: 0x06000045 RID: 69 RVA: 0x000022FC File Offset: 0x000004FC
		public AppInfo()
		{
		}

		// Token: 0x0400002B RID: 43
		[CompilerGenerated]
		private string <ClientId>k__BackingField;

		// Token: 0x0400002C RID: 44
		[CompilerGenerated]
		private string <AppSlug>k__BackingField;

		// Token: 0x0400002D RID: 45
		[CompilerGenerated]
		private string <ClientKey>k__BackingField;

		// Token: 0x0400002E RID: 46
		[CompilerGenerated]
		private string <RSAPublicKey>k__BackingField;
	}
}
