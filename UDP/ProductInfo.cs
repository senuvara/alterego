﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.UDP
{
	// Token: 0x02000009 RID: 9
	[Serializable]
	public class ProductInfo
	{
		// Token: 0x17000001 RID: 1
		// (get) Token: 0x0600002C RID: 44 RVA: 0x0000254A File Offset: 0x0000074A
		// (set) Token: 0x0600002D RID: 45 RVA: 0x00002552 File Offset: 0x00000752
		public string ItemType
		{
			[CompilerGenerated]
			get
			{
				return this.<ItemType>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ItemType>k__BackingField = value;
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x0600002E RID: 46 RVA: 0x0000255B File Offset: 0x0000075B
		// (set) Token: 0x0600002F RID: 47 RVA: 0x00002563 File Offset: 0x00000763
		public string ProductId
		{
			[CompilerGenerated]
			get
			{
				return this.<ProductId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ProductId>k__BackingField = value;
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000030 RID: 48 RVA: 0x0000256C File Offset: 0x0000076C
		// (set) Token: 0x06000031 RID: 49 RVA: 0x00002574 File Offset: 0x00000774
		public bool? Consumable
		{
			[CompilerGenerated]
			get
			{
				return this.<Consumable>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Consumable>k__BackingField = value;
			}
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000032 RID: 50 RVA: 0x0000257D File Offset: 0x0000077D
		// (set) Token: 0x06000033 RID: 51 RVA: 0x00002585 File Offset: 0x00000785
		public string Price
		{
			[CompilerGenerated]
			get
			{
				return this.<Price>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Price>k__BackingField = value;
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000034 RID: 52 RVA: 0x0000258E File Offset: 0x0000078E
		// (set) Token: 0x06000035 RID: 53 RVA: 0x00002596 File Offset: 0x00000796
		public long PriceAmountMicros
		{
			[CompilerGenerated]
			get
			{
				return this.<PriceAmountMicros>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<PriceAmountMicros>k__BackingField = value;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000036 RID: 54 RVA: 0x0000259F File Offset: 0x0000079F
		// (set) Token: 0x06000037 RID: 55 RVA: 0x000025A7 File Offset: 0x000007A7
		public string Currency
		{
			[CompilerGenerated]
			get
			{
				return this.<Currency>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Currency>k__BackingField = value;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000038 RID: 56 RVA: 0x000025B0 File Offset: 0x000007B0
		// (set) Token: 0x06000039 RID: 57 RVA: 0x000025B8 File Offset: 0x000007B8
		public string Title
		{
			[CompilerGenerated]
			get
			{
				return this.<Title>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Title>k__BackingField = value;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x0600003A RID: 58 RVA: 0x000025C1 File Offset: 0x000007C1
		// (set) Token: 0x0600003B RID: 59 RVA: 0x000025C9 File Offset: 0x000007C9
		public string Description
		{
			[CompilerGenerated]
			get
			{
				return this.<Description>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Description>k__BackingField = value;
			}
		}

		// Token: 0x0600003C RID: 60 RVA: 0x000022FC File Offset: 0x000004FC
		public ProductInfo()
		{
		}

		// Token: 0x04000023 RID: 35
		[CompilerGenerated]
		private string <ItemType>k__BackingField;

		// Token: 0x04000024 RID: 36
		[CompilerGenerated]
		private string <ProductId>k__BackingField;

		// Token: 0x04000025 RID: 37
		[CompilerGenerated]
		private bool? <Consumable>k__BackingField;

		// Token: 0x04000026 RID: 38
		[CompilerGenerated]
		private string <Price>k__BackingField;

		// Token: 0x04000027 RID: 39
		[CompilerGenerated]
		private long <PriceAmountMicros>k__BackingField;

		// Token: 0x04000028 RID: 40
		[CompilerGenerated]
		private string <Currency>k__BackingField;

		// Token: 0x04000029 RID: 41
		[CompilerGenerated]
		private string <Title>k__BackingField;

		// Token: 0x0400002A RID: 42
		[CompilerGenerated]
		private string <Description>k__BackingField;
	}
}
