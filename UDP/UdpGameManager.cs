﻿using System;
using UnityEngine.UDP.Analytics;

namespace UnityEngine.UDP
{
	// Token: 0x02000003 RID: 3
	[HideInInspector]
	internal class UdpGameManager : MonoBehaviour
	{
		// Token: 0x06000002 RID: 2 RVA: 0x000020BB File Offset: 0x000002BB
		private void Awake()
		{
			Debug.Log("udp.gameManager.awake");
			AnalyticsService.OnAppAwake();
		}

		// Token: 0x06000003 RID: 3 RVA: 0x000020CC File Offset: 0x000002CC
		private void Start()
		{
			Debug.Log("udp.gameManager.start");
			Object.DontDestroyOnLoad(base.gameObject);
		}

		// Token: 0x06000004 RID: 4 RVA: 0x000020E3 File Offset: 0x000002E3
		private void Update()
		{
		}

		// Token: 0x06000005 RID: 5 RVA: 0x000020E5 File Offset: 0x000002E5
		private void OnApplicationPause(bool pauseStatus)
		{
			Debug.Log("udp.gameManager.OnApplicationPause");
			AnalyticsService.OnPlayerPaused(pauseStatus);
		}

		// Token: 0x06000006 RID: 6 RVA: 0x000020F7 File Offset: 0x000002F7
		private void OnApplicationQuit()
		{
			Debug.Log("udp.gameManager.OnApplicationQuit");
			AnalyticsService.OnPlayerQuit();
		}

		// Token: 0x06000007 RID: 7 RVA: 0x00002108 File Offset: 0x00000308
		public UdpGameManager()
		{
		}

		// Token: 0x06000008 RID: 8 RVA: 0x00002110 File Offset: 0x00000310
		// Note: this type is marked as 'beforefieldinit'.
		static UdpGameManager()
		{
		}

		// Token: 0x0400000E RID: 14
		public static readonly string OBJECT_NAME = "UnityChannelGameManager";
	}
}
