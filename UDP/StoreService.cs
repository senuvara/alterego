﻿using System;
using System.Collections.Generic;
using UnityEngine.UDP.Analytics;

namespace UnityEngine.UDP
{
	// Token: 0x0200000E RID: 14
	public class StoreService
	{
		// Token: 0x06000064 RID: 100 RVA: 0x00002AD0 File Offset: 0x00000CD0
		public static void Initialize(IInitListener listener, AppInfo appInfo = null)
		{
			RuntimePlatform platform = Application.platform;
			if (platform <= RuntimePlatform.WindowsEditor)
			{
				if (platform != RuntimePlatform.OSXEditor && platform != RuntimePlatform.WindowsEditor)
				{
					goto IL_3E;
				}
			}
			else
			{
				if (platform == RuntimePlatform.Android)
				{
					StoreService.serviceClass = new AndroidJavaClass("com.unity.udp.sdk.ChannelService");
					if (GameObject.Find(MainThreadDispatcher.OBJECT_NAME) == null)
					{
						GameObject gameObject = new GameObject(MainThreadDispatcher.OBJECT_NAME);
						Object.DontDestroyOnLoad(gameObject);
						gameObject.hideFlags = (HideFlags.HideInHierarchy | HideFlags.HideInInspector);
						gameObject.AddComponent<MainThreadDispatcher>();
					}
					AndroidJavaObject @static = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
					InitLoginForwardCallback initLoginForwardCallback = new InitLoginForwardCallback(listener);
					AndroidJavaObject androidJavaObject = new AndroidJavaObject("com.unity.udp.sdk.AppInfo", new object[0]);
					string text;
					string val;
					string appSlug;
					string val2;
					if (appInfo == null)
					{
						AppStoreSettings appStoreSettings = Resources.Load<AppStoreSettings>("UDP Settings");
						if (appStoreSettings == null)
						{
							throw new InvalidOperationException("StoreService cannot find validUDP Settings file! Initialization failed!");
						}
						text = appStoreSettings.UnityClientID;
						val = appStoreSettings.UnityClientKey;
						appSlug = appStoreSettings.AppSlug;
						val2 = appStoreSettings.UnityClientRSAPublicKey;
					}
					else
					{
						text = appInfo.ClientId;
						val = appInfo.ClientKey;
						appSlug = appInfo.AppSlug;
						val2 = appInfo.RSAPublicKey;
					}
					androidJavaObject.Set<string>("clientId", text);
					androidJavaObject.Set<string>("clientSecret", val);
					androidJavaObject.Set<string>("appSlug", appSlug);
					androidJavaObject.Set<string>("RSAPublicKey", val2);
					StoreService.serviceClass.CallStatic("init", new object[]
					{
						@static,
						androidJavaObject,
						initLoginForwardCallback
					});
					AnalyticsService.Initialize();
					AnalyticsClient.Initialize(text, appSlug, StoreService.StoreName);
					if (GameObject.Find(UdpGameManager.OBJECT_NAME) == null)
					{
						GameObject gameObject2 = new GameObject(UdpGameManager.OBJECT_NAME);
						Object.DontDestroyOnLoad(gameObject2);
						gameObject2.hideFlags = (HideFlags.HideInHierarchy | HideFlags.HideInInspector);
						gameObject2.AddComponent<UdpGameManager>();
					}
					return;
				}
				if (platform != RuntimePlatform.LinuxEditor)
				{
					goto IL_3E;
				}
			}
			listener.OnInitialized(null);
			return;
			IL_3E:
			throw new InvalidOperationException("StoreService doesn't support current platform!");
		}

		// Token: 0x06000065 RID: 101 RVA: 0x00002C78 File Offset: 0x00000E78
		[Obsolete("gameOrderId will not be passed and will be generated automatically to make sure of its uniqueness")]
		public static void Purchase(string productId, string gameOrderId, string developerPayload, IPurchaseListener listener)
		{
			string text = Guid.NewGuid().ToString();
			UdpAnalytics.PurchaseAttempt(productId, text);
			PurchaseForwardCallback purchaseForwardCallback = new PurchaseForwardCallback(listener);
			AndroidJavaObject androidJavaObject = new AndroidJavaObject("com.unity.udp.sdk.PurchaseInfo", new object[0]);
			androidJavaObject.Set<string>("productId", productId);
			androidJavaObject.Set<string>("gameOrderId", text);
			androidJavaObject.Set<string>("developerPayload", developerPayload);
			StoreService.serviceClass.CallStatic("purchase", new object[]
			{
				androidJavaObject,
				purchaseForwardCallback
			});
		}

		// Token: 0x06000066 RID: 102 RVA: 0x00002CFC File Offset: 0x00000EFC
		public static void Purchase(string productId, string developerPayload, IPurchaseListener listener)
		{
			string text = Guid.NewGuid().ToString().Replace("-", string.Empty);
			UdpAnalytics.PurchaseAttempt(productId, text);
			PurchaseForwardCallback purchaseForwardCallback = new PurchaseForwardCallback(listener);
			AndroidJavaObject androidJavaObject = new AndroidJavaObject("com.unity.udp.sdk.PurchaseInfo", new object[0]);
			androidJavaObject.Set<string>("productId", productId);
			androidJavaObject.Set<string>("gameOrderId", text);
			androidJavaObject.Set<string>("developerPayload", developerPayload);
			StoreService.serviceClass.CallStatic("purchase", new object[]
			{
				androidJavaObject,
				purchaseForwardCallback
			});
		}

		// Token: 0x06000067 RID: 103 RVA: 0x00002D90 File Offset: 0x00000F90
		public static void QueryInventory(IPurchaseListener listener)
		{
			PurchaseForwardCallback purchaseForwardCallback = new PurchaseForwardCallback(listener);
			StoreService.serviceClass.CallStatic("queryInventory", new object[]
			{
				null,
				purchaseForwardCallback
			});
		}

		// Token: 0x06000068 RID: 104 RVA: 0x00002DC0 File Offset: 0x00000FC0
		public static void QueryInventory(List<string> productIds, IPurchaseListener listener)
		{
			PurchaseForwardCallback purchaseForwardCallback = new PurchaseForwardCallback(listener);
			StoreService.serviceClass.CallStatic("queryInventory", new object[]
			{
				StoreService.javaArrayFromCSList(productIds),
				purchaseForwardCallback
			});
		}

		// Token: 0x06000069 RID: 105 RVA: 0x00002DF8 File Offset: 0x00000FF8
		public static void ConsumePurchase(PurchaseInfo purchaseInfo, IPurchaseListener listener)
		{
			PurchaseForwardCallback purchaseForwardCallback = new PurchaseForwardCallback(listener);
			AndroidJavaObject androidJavaObject = new AndroidJavaObject("com.unity.udp.sdk.PurchaseInfo", new object[0]);
			androidJavaObject.Call<AndroidJavaObject>("setItemType", new object[]
			{
				purchaseInfo.ItemType
			});
			androidJavaObject.Call<AndroidJavaObject>("setProductId", new object[]
			{
				purchaseInfo.ProductId
			});
			androidJavaObject.Call<AndroidJavaObject>("setDeveloperPayload", new object[]
			{
				purchaseInfo.DeveloperPayload
			});
			androidJavaObject.Call<AndroidJavaObject>("setGameOrderId", new object[]
			{
				purchaseInfo.GameOrderId
			});
			androidJavaObject.Call<AndroidJavaObject>("setOrderQueryToken", new object[]
			{
				purchaseInfo.OrderQueryToken
			});
			androidJavaObject.Call<AndroidJavaObject>("setStorePurchaseJsonString", new object[]
			{
				purchaseInfo.StorePurchaseJsonString
			});
			StoreService.serviceClass.CallStatic("consumePurchase", new object[]
			{
				androidJavaObject,
				purchaseForwardCallback
			});
		}

		// Token: 0x0600006A RID: 106 RVA: 0x00002EDC File Offset: 0x000010DC
		public static void ConsumePurchase(List<PurchaseInfo> purchaseInfos, IPurchaseListener listener)
		{
			PurchaseForwardCallback purchaseForwardCallback = new PurchaseForwardCallback(listener);
			AndroidJavaObject androidJavaObject = new AndroidJavaObject("java.util.ArrayList", new object[0]);
			foreach (PurchaseInfo purchaseInfo in purchaseInfos)
			{
				AndroidJavaObject androidJavaObject2 = new AndroidJavaObject("com.unity.udp.sdk.PurchaseInfo", new object[0]);
				androidJavaObject2.Call<AndroidJavaObject>("setItemType", new object[]
				{
					purchaseInfo.ItemType
				});
				androidJavaObject2.Call<AndroidJavaObject>("setProductId", new object[]
				{
					purchaseInfo.ProductId
				});
				androidJavaObject2.Call<AndroidJavaObject>("setDeveloperPayload", new object[]
				{
					purchaseInfo.DeveloperPayload
				});
				androidJavaObject2.Call<AndroidJavaObject>("setGameOrderId", new object[]
				{
					purchaseInfo.GameOrderId
				});
				androidJavaObject2.Call<AndroidJavaObject>("setOrderQueryToken", new object[]
				{
					purchaseInfo.OrderQueryToken
				});
				androidJavaObject2.Call<AndroidJavaObject>("setStorePurchaseJsonString", new object[]
				{
					purchaseInfo.StorePurchaseJsonString
				});
				androidJavaObject.Call<bool>("add", new object[]
				{
					androidJavaObject2
				});
			}
			StoreService.serviceClass.CallStatic("consumePurchases", new object[]
			{
				androidJavaObject,
				purchaseForwardCallback
			});
		}

		// Token: 0x0600006B RID: 107 RVA: 0x00003030 File Offset: 0x00001230
		public static void EnableDebugLogging(bool enable)
		{
			StoreService.serviceClass.CallStatic("enableDebugLogging", new object[]
			{
				enable,
				""
			});
		}

		// Token: 0x0600006C RID: 108 RVA: 0x00003058 File Offset: 0x00001258
		public static void EnableDebugLogging(bool enable, string tag)
		{
			StoreService.serviceClass.CallStatic("enableDebugLogging", new object[]
			{
				enable,
				(tag == null) ? "" : tag
			});
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x0600006D RID: 109 RVA: 0x00003086 File Offset: 0x00001286
		public static string StoreName
		{
			get
			{
				if (StoreService.serviceClass != null)
				{
					return StoreService.serviceClass.CallStatic<string>("getStoreName", new object[0]);
				}
				return "UDP";
			}
		}

		// Token: 0x0600006E RID: 110 RVA: 0x000030AC File Offset: 0x000012AC
		internal static AndroidJavaObject javaArrayFromCS(string[] values)
		{
			AndroidJavaClass androidJavaClass = new AndroidJavaClass("java.lang.reflect.Array");
			AndroidJavaObject androidJavaObject = androidJavaClass.CallStatic<AndroidJavaObject>("newInstance", new object[]
			{
				new AndroidJavaClass("java.lang.String"),
				values.Length
			});
			for (int i = 0; i < values.Length; i++)
			{
				androidJavaClass.CallStatic("set", new object[]
				{
					androidJavaObject,
					i,
					new AndroidJavaObject("java.lang.String", new object[]
					{
						values[i]
					})
				});
			}
			return androidJavaObject;
		}

		// Token: 0x0600006F RID: 111 RVA: 0x00003134 File Offset: 0x00001334
		internal static AndroidJavaObject javaArrayFromCSList(List<string> values)
		{
			if (values == null)
			{
				return null;
			}
			AndroidJavaClass androidJavaClass = new AndroidJavaClass("java.lang.reflect.Array");
			AndroidJavaObject androidJavaObject = androidJavaClass.CallStatic<AndroidJavaObject>("newInstance", new object[]
			{
				new AndroidJavaClass("java.lang.String"),
				values.Count
			});
			for (int i = 0; i < values.Count; i++)
			{
				androidJavaClass.CallStatic("set", new object[]
				{
					androidJavaObject,
					i,
					new AndroidJavaObject("java.lang.String", new object[]
					{
						values[i]
					})
				});
			}
			return androidJavaObject;
		}

		// Token: 0x06000070 RID: 112 RVA: 0x000022FC File Offset: 0x000004FC
		public StoreService()
		{
		}

		// Token: 0x04000039 RID: 57
		private static AndroidJavaClass serviceClass;

		// Token: 0x0400003A RID: 58
		private const string CHANNEL_SERVICE = "com.unity.udp.sdk.ChannelService";

		// Token: 0x0400003B RID: 59
		private const string UNITY_PLAYER = "com.unity3d.player.UnityPlayer";

		// Token: 0x0400003C RID: 60
		private const string APP_INFO = "com.unity.udp.sdk.AppInfo";

		// Token: 0x0400003D RID: 61
		private const string PURCHASE_INFO = "com.unity.udp.sdk.PurchaseInfo";
	}
}
