﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.UDP
{
	// Token: 0x02000010 RID: 16
	public class InitLoginForwardCallback : AndroidJavaProxy
	{
		// Token: 0x06000073 RID: 115 RVA: 0x000031CB File Offset: 0x000013CB
		public InitLoginForwardCallback(IInitListener initListener) : base("com.unity.udp.sdk.InitCallback")
		{
			this._initListener = initListener;
		}

		// Token: 0x06000074 RID: 116 RVA: 0x000031E0 File Offset: 0x000013E0
		public void onInitFinished(int resultCode, string message, AndroidJavaObject jo)
		{
			if (this._initListener == null)
			{
				Debug.LogError("LoginCallback cannot be null");
			}
			if (resultCode == 0)
			{
				UserInfo userInfo = null;
				if (jo != null)
				{
					userInfo = new UserInfo();
					userInfo.UserId = userInfo.Channel + "_" + jo.Call<string>("getUserId", new object[0]);
					userInfo.UserLoginToken = jo.Call<string>("getLoginReceipt", new object[0]);
					userInfo.Channel = jo.Call<string>("getChannel", new object[0]);
				}
				if (userInfo == null)
				{
					Debug.Log("No UserInfo is returned.");
				}
				MainThreadDispatcher.RunOnMainThread(delegate
				{
					this._initListener.OnInitialized(userInfo);
				});
				return;
			}
			MainThreadDispatcher.RunOnMainThread(delegate
			{
				this._initListener.OnInitializeFailed(message);
			});
		}

		// Token: 0x0400003E RID: 62
		private IInitListener _initListener;

		// Token: 0x0200002B RID: 43
		[CompilerGenerated]
		private sealed class <>c__DisplayClass2_0
		{
			// Token: 0x060000FA RID: 250 RVA: 0x000022FC File Offset: 0x000004FC
			public <>c__DisplayClass2_0()
			{
			}

			// Token: 0x060000FB RID: 251 RVA: 0x00003FD1 File Offset: 0x000021D1
			internal void <onInitFinished>b__0()
			{
				this.<>4__this._initListener.OnInitializeFailed(this.message);
			}

			// Token: 0x04000093 RID: 147
			public InitLoginForwardCallback <>4__this;

			// Token: 0x04000094 RID: 148
			public string message;
		}

		// Token: 0x0200002C RID: 44
		[CompilerGenerated]
		private sealed class <>c__DisplayClass2_1
		{
			// Token: 0x060000FC RID: 252 RVA: 0x000022FC File Offset: 0x000004FC
			public <>c__DisplayClass2_1()
			{
			}

			// Token: 0x060000FD RID: 253 RVA: 0x00003FE9 File Offset: 0x000021E9
			internal void <onInitFinished>b__1()
			{
				this.CS$<>8__locals1.<>4__this._initListener.OnInitialized(this.userInfo);
			}

			// Token: 0x04000095 RID: 149
			public UserInfo userInfo;

			// Token: 0x04000096 RID: 150
			public InitLoginForwardCallback.<>c__DisplayClass2_0 CS$<>8__locals1;
		}
	}
}
