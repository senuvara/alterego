﻿using System;
using System.Collections;
using System.Linq;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Filters;

namespace UnityEngine.TestRunner.NUnitExtensions.Filters
{
	// Token: 0x02000025 RID: 37
	internal class CategoryFilterExtended : CategoryFilter
	{
		// Token: 0x060000BF RID: 191 RVA: 0x00005402 File Offset: 0x00003602
		public CategoryFilterExtended(string name) : base(name)
		{
		}

		// Token: 0x060000C0 RID: 192 RVA: 0x0000540C File Offset: 0x0000360C
		public override bool Match(ITest test)
		{
			IList list = test.Properties["Category"].Cast<string>().ToList<string>();
			if (test is TestMethod)
			{
				IList list2 = test.Parent.Properties["Category"].Cast<string>().ToList<string>();
				if (list2.Count > 0)
				{
					return false;
				}
			}
			return (list.Count == 0 && base.ExpectedValue == CategoryFilterExtended.k_DefaultCategory && test is TestMethod) || base.Match(test);
		}

		// Token: 0x060000C1 RID: 193 RVA: 0x000054B5 File Offset: 0x000036B5
		// Note: this type is marked as 'beforefieldinit'.
		static CategoryFilterExtended()
		{
		}

		// Token: 0x04000041 RID: 65
		public static string k_DefaultCategory = "Uncategorized";
	}
}
