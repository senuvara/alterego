﻿using System;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal.Filters;

namespace UnityEngine.TestRunner.NUnitExtensions.Filters
{
	// Token: 0x02000024 RID: 36
	internal class AssemblyNameFilter : ValueMatchFilter
	{
		// Token: 0x060000BC RID: 188 RVA: 0x0000534A File Offset: 0x0000354A
		public AssemblyNameFilter(string assemblyName) : base(assemblyName)
		{
		}

		// Token: 0x060000BD RID: 189 RVA: 0x00005354 File Offset: 0x00003554
		public override bool Match(ITest test)
		{
			string value = string.Empty;
			if (test.TypeInfo != null && test.TypeInfo.Assembly != null && test.TypeInfo.FullName != null)
			{
				value = test.TypeInfo.Assembly.FullName.Substring(0, test.TypeInfo.Assembly.FullName.IndexOf(',')).TrimEnd(new char[]
				{
					','
				});
			}
			return base.ExpectedValue.Equals(value, StringComparison.OrdinalIgnoreCase);
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x060000BE RID: 190 RVA: 0x000053E8 File Offset: 0x000035E8
		protected override string ElementName
		{
			get
			{
				return "id";
			}
		}
	}
}
