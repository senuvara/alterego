﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Commands;
using NUnit.Framework.Internal.Execution;
using UnityEngine.TestTools;

namespace UnityEngine.TestRunner.NUnitExtensions.Runner
{
	// Token: 0x0200002A RID: 42
	internal class DefaultTestWorkItem : UnityWorkItem
	{
		// Token: 0x060000DC RID: 220 RVA: 0x00006C04 File Offset: 0x00004E04
		public DefaultTestWorkItem(TestMethod test, ITestFilter filter) : base(test, null)
		{
			this._command = ((test.RunState != RunState.Runnable && (test.RunState != RunState.Explicit || !filter.IsExplicitMatch(test))) ? CommandBuilder.MakeSkipCommand(test) : CommandBuilder.MakeTestCommand(test));
		}

		// Token: 0x060000DD RID: 221 RVA: 0x00006C58 File Offset: 0x00004E58
		protected override IEnumerable PerformWork()
		{
			if (this.m_DontRunRestoringResult && EditModeTestCallbacks.RestoringTestContext != null)
			{
				EditModeTestCallbacks.RestoringTestContext();
				base.Result = base.Context.CurrentResult;
				yield break;
			}
			try
			{
				if (this._command is SkipCommand)
				{
					base.Result = this._command.Execute(base.Context);
					yield break;
				}
				if (this._command is NUnit.Framework.Internal.Commands.SetUpTearDownCommand)
				{
					NUnit.Framework.Internal.Commands.SetUpTearDownCommand setUpTearDownCommand = this._command as NUnit.Framework.Internal.Commands.SetUpTearDownCommand;
					TestCommand testCommand = setUpTearDownCommand.GetInnerCommand();
					if (DefaultTestWorkItem.GetFirstInnerCommandOfType<UnityLogCheckDelegatingCommand>(testCommand) == null)
					{
						testCommand = new UnityLogCheckDelegatingCommand(testCommand);
					}
					this._command = new UnityEngine.TestTools.SetUpTearDownCommand(testCommand);
				}
				else if (DefaultTestWorkItem.GetFirstInnerCommandOfType<UnityLogCheckDelegatingCommand>(this._command) == null)
				{
					this._command = new UnityLogCheckDelegatingCommand(this._command);
				}
				this._command = new UnityEngine.TestTools.TestActionCommand(this._command);
				this._command = new EnumerableSetUpTearDownCommand(this._command);
				this._command = new OuterUnityTestActionCommand(this._command);
				IEnumerator enumerator2 = ((IEnumerableTestMethodCommand)this._command).ExecuteEnumerable(base.Context).GetEnumerator();
				try
				{
					while (enumerator2.MoveNext())
					{
						object workItemStep = enumerator2.Current;
						base.ResultedInDomainReload = false;
						if (workItemStep is IEditModeTestYieldInstruction)
						{
							IEditModeTestYieldInstruction editModeTestYieldInstruction = (IEditModeTestYieldInstruction)workItemStep;
							yield return editModeTestYieldInstruction;
							IEnumerator enumerator = editModeTestYieldInstruction.Perform();
							for (;;)
							{
								bool moveNext;
								try
								{
									moveNext = enumerator.MoveNext();
								}
								catch (Exception ex)
								{
									base.Context.CurrentResult.RecordException(ex);
									break;
								}
								if (!moveNext)
								{
									break;
								}
								base.ResultedInDomainReload = editModeTestYieldInstruction.ExpectDomainReload;
								yield return null;
							}
						}
						else
						{
							yield return workItemStep;
						}
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator2 as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
				base.Result = base.Context.CurrentResult;
			}
			finally
			{
				base.WorkItemComplete();
			}
			yield break;
		}

		// Token: 0x060000DE RID: 222 RVA: 0x00006C84 File Offset: 0x00004E84
		private static T GetFirstInnerCommandOfType<T>(TestCommand command) where T : TestCommand
		{
			T result;
			if (command is T)
			{
				result = (T)((object)command);
			}
			else if (command is DelegatingTestCommand)
			{
				DelegatingTestCommand delegatingTestCommand = (DelegatingTestCommand)command;
				result = DefaultTestWorkItem.GetFirstInnerCommandOfType<T>(delegatingTestCommand.GetInnerCommand());
			}
			else
			{
				result = (T)((object)null);
			}
			return result;
		}

		// Token: 0x0400004D RID: 77
		private TestCommand _command;

		// Token: 0x0200007B RID: 123
		[CompilerGenerated]
		private sealed class <PerformWork>c__Iterator0 : IEnumerable, IEnumerable<object>, IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x0600027F RID: 639 RVA: 0x00006CDB File Offset: 0x00004EDB
			[DebuggerHidden]
			public <PerformWork>c__Iterator0()
			{
			}

			// Token: 0x06000280 RID: 640 RVA: 0x00006CE4 File Offset: 0x00004EE4
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				bool flag = false;
				switch (num)
				{
				case 0U:
					if (this.m_DontRunRestoringResult && EditModeTestCallbacks.RestoringTestContext != null)
					{
						EditModeTestCallbacks.RestoringTestContext();
						base.Result = base.Context.CurrentResult;
						return false;
					}
					num = 4294967293U;
					break;
				case 1U:
				case 2U:
				case 3U:
					break;
				default:
					return false;
				}
				try
				{
					switch (num)
					{
					case 1U:
					case 2U:
					case 3U:
						break;
					default:
						if (this._command is SkipCommand)
						{
							base.Result = this._command.Execute(base.Context);
							return false;
						}
						if (this._command is NUnit.Framework.Internal.Commands.SetUpTearDownCommand)
						{
							NUnit.Framework.Internal.Commands.SetUpTearDownCommand setUpTearDownCommand = this._command as NUnit.Framework.Internal.Commands.SetUpTearDownCommand;
							TestCommand testCommand = setUpTearDownCommand.GetInnerCommand();
							if (DefaultTestWorkItem.GetFirstInnerCommandOfType<UnityLogCheckDelegatingCommand>(testCommand) == null)
							{
								testCommand = new UnityLogCheckDelegatingCommand(testCommand);
							}
							this._command = new UnityEngine.TestTools.SetUpTearDownCommand(testCommand);
						}
						else if (DefaultTestWorkItem.GetFirstInnerCommandOfType<UnityLogCheckDelegatingCommand>(this._command) == null)
						{
							this._command = new UnityLogCheckDelegatingCommand(this._command);
						}
						this._command = new UnityEngine.TestTools.TestActionCommand(this._command);
						this._command = new EnumerableSetUpTearDownCommand(this._command);
						this._command = new OuterUnityTestActionCommand(this._command);
						enumerator2 = ((IEnumerableTestMethodCommand)this._command).ExecuteEnumerable(base.Context).GetEnumerator();
						num = 4294967293U;
						break;
					}
					try
					{
						switch (num)
						{
						case 1U:
							enumerator = editModeTestYieldInstruction.Perform();
							break;
						case 2U:
							break;
						case 3U:
							goto IL_310;
						default:
							goto IL_311;
						}
						try
						{
							moveNext = enumerator.MoveNext();
						}
						catch (Exception ex)
						{
							base.Context.CurrentResult.RecordException(ex);
							goto IL_2E6;
						}
						if (moveNext)
						{
							base.ResultedInDomainReload = editModeTestYieldInstruction.ExpectDomainReload;
							this.$current = null;
							if (!this.$disposing)
							{
								this.$PC = 2;
							}
							flag = true;
							return true;
						}
						IL_2E6:
						IL_310:
						IL_311:
						if (enumerator2.MoveNext())
						{
							workItemStep = enumerator2.Current;
							base.ResultedInDomainReload = false;
							if (workItemStep is IEditModeTestYieldInstruction)
							{
								editModeTestYieldInstruction = (IEditModeTestYieldInstruction)workItemStep;
								this.$current = editModeTestYieldInstruction;
								if (!this.$disposing)
								{
									this.$PC = 1;
								}
								flag = true;
								return true;
							}
							this.$current = workItemStep;
							if (!this.$disposing)
							{
								this.$PC = 3;
							}
							flag = true;
							return true;
						}
					}
					finally
					{
						if (!flag)
						{
							if ((disposable = (enumerator2 as IDisposable)) != null)
							{
								disposable.Dispose();
							}
						}
					}
					base.Result = base.Context.CurrentResult;
				}
				finally
				{
					if (!flag)
					{
						this.<>__Finally0();
					}
				}
				this.$PC = -1;
				return false;
			}

			// Token: 0x17000089 RID: 137
			// (get) Token: 0x06000281 RID: 641 RVA: 0x000070C4 File Offset: 0x000052C4
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x1700008A RID: 138
			// (get) Token: 0x06000282 RID: 642 RVA: 0x000070E0 File Offset: 0x000052E0
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x06000283 RID: 643 RVA: 0x000070FC File Offset: 0x000052FC
			[DebuggerHidden]
			public void Dispose()
			{
				uint num = (uint)this.$PC;
				this.$disposing = true;
				this.$PC = -1;
				switch (num)
				{
				case 1U:
				case 2U:
				case 3U:
					try
					{
						try
						{
						}
						finally
						{
							if ((disposable = (enumerator2 as IDisposable)) != null)
							{
								disposable.Dispose();
							}
						}
					}
					finally
					{
						this.<>__Finally0();
					}
					break;
				}
			}

			// Token: 0x06000284 RID: 644 RVA: 0x0000718C File Offset: 0x0000538C
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x06000285 RID: 645 RVA: 0x00007194 File Offset: 0x00005394
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<object>.GetEnumerator();
			}

			// Token: 0x06000286 RID: 646 RVA: 0x000071B0 File Offset: 0x000053B0
			[DebuggerHidden]
			IEnumerator<object> IEnumerable<object>.GetEnumerator()
			{
				if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
				{
					return this;
				}
				DefaultTestWorkItem.<PerformWork>c__Iterator0 <PerformWork>c__Iterator = new DefaultTestWorkItem.<PerformWork>c__Iterator0();
				<PerformWork>c__Iterator.$this = this;
				return <PerformWork>c__Iterator;
			}

			// Token: 0x06000287 RID: 647 RVA: 0x000071E4 File Offset: 0x000053E4
			private void <>__Finally0()
			{
				base.WorkItemComplete();
			}

			// Token: 0x04000161 RID: 353
			internal IEnumerator $locvar0;

			// Token: 0x04000162 RID: 354
			internal object <workItemStep>__1;

			// Token: 0x04000163 RID: 355
			internal IDisposable $locvar1;

			// Token: 0x04000164 RID: 356
			internal IEditModeTestYieldInstruction <editModeTestYieldInstruction>__2;

			// Token: 0x04000165 RID: 357
			internal IEnumerator <enumerator>__2;

			// Token: 0x04000166 RID: 358
			internal bool <moveNext>__3;

			// Token: 0x04000167 RID: 359
			internal DefaultTestWorkItem $this;

			// Token: 0x04000168 RID: 360
			internal object $current;

			// Token: 0x04000169 RID: 361
			internal bool $disposing;

			// Token: 0x0400016A RID: 362
			internal int $PC;
		}
	}
}
