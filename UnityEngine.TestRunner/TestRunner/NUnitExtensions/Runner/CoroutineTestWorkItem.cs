﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Commands;
using NUnit.Framework.Internal.Execution;
using UnityEngine.TestTools.Utils;

namespace UnityEngine.TestRunner.NUnitExtensions.Runner
{
	// Token: 0x02000028 RID: 40
	internal class CoroutineTestWorkItem : UnityWorkItem
	{
		// Token: 0x060000D5 RID: 213 RVA: 0x0000680C File Offset: 0x00004A0C
		public CoroutineTestWorkItem(TestMethod test, ITestFilter filter) : base(test, null)
		{
			this.m_Command = ((test.RunState != RunState.Runnable && (test.RunState != RunState.Explicit || !filter.IsExplicitMatch(test))) ? CommandBuilder.MakeSkipCommand(test) : CommandBuilder.MakeTestCommand(test));
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x060000D6 RID: 214 RVA: 0x00006860 File Offset: 0x00004A60
		// (set) Token: 0x060000D7 RID: 215 RVA: 0x00006896 File Offset: 0x00004A96
		public static MonoBehaviour monoBehaviourCoroutineRunner
		{
			get
			{
				if (CoroutineTestWorkItem.m_MonoBehaviourCoroutineRunner == null)
				{
					throw new NullReferenceException("MonoBehaviour coroutine runner not set");
				}
				return CoroutineTestWorkItem.m_MonoBehaviourCoroutineRunner;
			}
			set
			{
				CoroutineTestWorkItem.m_MonoBehaviourCoroutineRunner = value;
			}
		}

		// Token: 0x060000D8 RID: 216 RVA: 0x000068A0 File Offset: 0x00004AA0
		protected override IEnumerable PerformWork()
		{
			if (this.m_Command is SkipCommand)
			{
				this.m_Command.Execute(base.Context);
				base.Result = base.Context.CurrentResult;
				base.WorkItemComplete();
				yield break;
			}
			if (this.m_Command is ApplyChangesToContextCommand)
			{
				ApplyChangesToContextCommand applyChangesToContextCommand = (ApplyChangesToContextCommand)this.m_Command;
				applyChangesToContextCommand.ApplyChanges(base.Context);
				this.m_Command = applyChangesToContextCommand.GetInnerCommand();
			}
			IEnumerableTestMethodCommand enumerableTestMethodCommand = (IEnumerableTestMethodCommand)this.m_Command;
			try
			{
				IEnumerator executeEnumerable = enumerableTestMethodCommand.ExecuteEnumerable(base.Context).GetEnumerator();
				CoroutineRunner coroutineRunner = new CoroutineRunner(CoroutineTestWorkItem.monoBehaviourCoroutineRunner, base.Context);
				yield return coroutineRunner.HandleEnumerableTest(executeEnumerable);
				if (coroutineRunner.HasFailedWithTimeout())
				{
					base.Context.CurrentResult.SetResult(ResultState.Failure, string.Format("Test exceeded Timeout value of {0}ms", base.Context.TestCaseTimeout));
				}
				while (executeEnumerable.MoveNext())
				{
				}
				base.Result = base.Context.CurrentResult;
			}
			finally
			{
				base.WorkItemComplete();
			}
			yield break;
		}

		// Token: 0x0400004A RID: 74
		private static MonoBehaviour m_MonoBehaviourCoroutineRunner;

		// Token: 0x0400004B RID: 75
		private TestCommand m_Command;

		// Token: 0x0200007A RID: 122
		[CompilerGenerated]
		private sealed class <PerformWork>c__Iterator0 : IEnumerable, IEnumerable<object>, IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x06000276 RID: 630 RVA: 0x000068CA File Offset: 0x00004ACA
			[DebuggerHidden]
			public <PerformWork>c__Iterator0()
			{
			}

			// Token: 0x06000277 RID: 631 RVA: 0x000068D4 File Offset: 0x00004AD4
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				bool flag = false;
				switch (num)
				{
				case 0U:
					if (this.m_Command is SkipCommand)
					{
						this.m_Command.Execute(base.Context);
						base.Result = base.Context.CurrentResult;
						base.WorkItemComplete();
						return false;
					}
					if (this.m_Command is ApplyChangesToContextCommand)
					{
						ApplyChangesToContextCommand applyChangesToContextCommand = (ApplyChangesToContextCommand)this.m_Command;
						applyChangesToContextCommand.ApplyChanges(base.Context);
						this.m_Command = applyChangesToContextCommand.GetInnerCommand();
					}
					enumerableTestMethodCommand = (IEnumerableTestMethodCommand)this.m_Command;
					num = 4294967293U;
					break;
				case 1U:
					break;
				default:
					return false;
				}
				try
				{
					switch (num)
					{
					case 1U:
						if (coroutineRunner.HasFailedWithTimeout())
						{
							base.Context.CurrentResult.SetResult(ResultState.Failure, string.Format("Test exceeded Timeout value of {0}ms", base.Context.TestCaseTimeout));
						}
						while (executeEnumerable.MoveNext())
						{
						}
						base.Result = base.Context.CurrentResult;
						break;
					default:
						executeEnumerable = enumerableTestMethodCommand.ExecuteEnumerable(base.Context).GetEnumerator();
						coroutineRunner = new CoroutineRunner(CoroutineTestWorkItem.monoBehaviourCoroutineRunner, base.Context);
						this.$current = coroutineRunner.HandleEnumerableTest(executeEnumerable);
						if (!this.$disposing)
						{
							this.$PC = 1;
						}
						flag = true;
						return true;
					}
				}
				finally
				{
					if (!flag)
					{
						this.<>__Finally0();
					}
				}
				this.$PC = -1;
				return false;
			}

			// Token: 0x17000087 RID: 135
			// (get) Token: 0x06000278 RID: 632 RVA: 0x00006AE4 File Offset: 0x00004CE4
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x17000088 RID: 136
			// (get) Token: 0x06000279 RID: 633 RVA: 0x00006B00 File Offset: 0x00004D00
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x0600027A RID: 634 RVA: 0x00006B1C File Offset: 0x00004D1C
			[DebuggerHidden]
			public void Dispose()
			{
				uint num = (uint)this.$PC;
				this.$disposing = true;
				this.$PC = -1;
				switch (num)
				{
				case 1U:
					try
					{
					}
					finally
					{
						this.<>__Finally0();
					}
					break;
				}
			}

			// Token: 0x0600027B RID: 635 RVA: 0x00006B70 File Offset: 0x00004D70
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x0600027C RID: 636 RVA: 0x00006B78 File Offset: 0x00004D78
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<object>.GetEnumerator();
			}

			// Token: 0x0600027D RID: 637 RVA: 0x00006B94 File Offset: 0x00004D94
			[DebuggerHidden]
			IEnumerator<object> IEnumerable<object>.GetEnumerator()
			{
				if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
				{
					return this;
				}
				CoroutineTestWorkItem.<PerformWork>c__Iterator0 <PerformWork>c__Iterator = new CoroutineTestWorkItem.<PerformWork>c__Iterator0();
				<PerformWork>c__Iterator.$this = this;
				return <PerformWork>c__Iterator;
			}

			// Token: 0x0600027E RID: 638 RVA: 0x00006BC8 File Offset: 0x00004DC8
			private void <>__Finally0()
			{
				base.WorkItemComplete();
			}

			// Token: 0x0400015A RID: 346
			internal IEnumerableTestMethodCommand <enumerableTestMethodCommand>__0;

			// Token: 0x0400015B RID: 347
			internal IEnumerator <executeEnumerable>__1;

			// Token: 0x0400015C RID: 348
			internal CoroutineRunner <coroutineRunner>__1;

			// Token: 0x0400015D RID: 349
			internal CoroutineTestWorkItem $this;

			// Token: 0x0400015E RID: 350
			internal object $current;

			// Token: 0x0400015F RID: 351
			internal bool $disposing;

			// Token: 0x04000160 RID: 352
			internal int $PC;
		}
	}
}
