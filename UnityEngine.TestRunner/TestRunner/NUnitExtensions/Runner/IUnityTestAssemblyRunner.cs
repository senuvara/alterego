﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using NUnit.Framework.Interfaces;

namespace UnityEngine.TestRunner.NUnitExtensions.Runner
{
	// Token: 0x0200002F RID: 47
	internal interface IUnityTestAssemblyRunner
	{
		// Token: 0x17000029 RID: 41
		// (get) Token: 0x060000E6 RID: 230
		ITest LoadedTest { get; }

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x060000E7 RID: 231
		ITestResult Result { get; }

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x060000E8 RID: 232
		bool IsTestLoaded { get; }

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x060000E9 RID: 233
		bool IsTestRunning { get; }

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x060000EA RID: 234
		bool IsTestComplete { get; }

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x060000EB RID: 235
		// (set) Token: 0x060000EC RID: 236
		UnityWorkItem TopLevelWorkItem { get; set; }

		// Token: 0x060000ED RID: 237
		UnityTestExecutionContext GetCurrentContext();

		// Token: 0x060000EE RID: 238
		ITest Load(Assembly[] assemblies, IDictionary<string, object> settings);

		// Token: 0x060000EF RID: 239
		IEnumerable Run(ITestListener listener, ITestFilter filter);

		// Token: 0x060000F0 RID: 240
		void StopRun();
	}
}
