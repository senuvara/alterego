﻿using System;
using System.Collections.Generic;

namespace UnityEngine.TestRunner.NUnitExtensions.Runner
{
	// Token: 0x02000033 RID: 51
	internal class UnityWorkItemDataHolder
	{
		// Token: 0x0600014E RID: 334 RVA: 0x00007E0C File Offset: 0x0000600C
		public UnityWorkItemDataHolder()
		{
		}

		// Token: 0x0600014F RID: 335 RVA: 0x00007E14 File Offset: 0x00006014
		// Note: this type is marked as 'beforefieldinit'.
		static UnityWorkItemDataHolder()
		{
		}

		// Token: 0x04000078 RID: 120
		public static List<string> alreadyStartedTests = new List<string>();

		// Token: 0x04000079 RID: 121
		public static List<string> alreadyExecutedTests;
	}
}
