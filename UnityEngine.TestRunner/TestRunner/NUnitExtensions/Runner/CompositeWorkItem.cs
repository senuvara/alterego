﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Commands;
using NUnit.Framework.Internal.Execution;
using UnityEngine.TestTools.Logging;
using UnityEngine.TestTools.TestRunner;

namespace UnityEngine.TestRunner.NUnitExtensions.Runner
{
	// Token: 0x02000026 RID: 38
	internal class CompositeWorkItem : UnityWorkItem
	{
		// Token: 0x060000C2 RID: 194 RVA: 0x000058ED File Offset: 0x00003AED
		public CompositeWorkItem(TestSuite suite, ITestFilter childFilter, WorkItemFactory factory) : base(suite, factory)
		{
			this._suite = suite;
			this._suiteResult = (base.Result as TestSuiteResult);
			this._childFilter = childFilter;
			this._countOrder = 0;
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x060000C3 RID: 195 RVA: 0x00005920 File Offset: 0x00003B20
		// (set) Token: 0x060000C4 RID: 196 RVA: 0x0000593A File Offset: 0x00003B3A
		public List<UnityWorkItem> Children
		{
			[CompilerGenerated]
			get
			{
				return this.<Children>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Children>k__BackingField = value;
			}
		}

		// Token: 0x060000C5 RID: 197 RVA: 0x00005944 File Offset: 0x00003B44
		protected override IEnumerable PerformWork()
		{
			this.InitializeSetUpAndTearDownCommands();
			if (UnityTestExecutionContext.CurrentContext != null && this.m_DontRunRestoringResult && EditModeTestCallbacks.RestoringTestContext != null)
			{
				EditModeTestCallbacks.RestoringTestContext();
			}
			if (!this.CheckForCancellation())
			{
				if (base.Test.RunState == RunState.Explicit && !this._childFilter.IsExplicitMatch(base.Test))
				{
					this.SkipFixture(ResultState.Explicit, this.GetSkipReason(), null);
				}
				else
				{
					switch (base.Test.RunState)
					{
					case RunState.NotRunnable:
						this.SkipFixture(ResultState.NotRunnable, this.GetSkipReason(), this.GetProviderStackTrace());
						break;
					default:
						base.Result.SetResult(ResultState.Success);
						this.CreateChildWorkItems();
						if (this.Children.Count > 0)
						{
							if (!this.m_DontRunRestoringResult)
							{
								yield return null;
								this.PerformOneTimeSetUp();
							}
							if (!this.CheckForCancellation())
							{
								switch (base.Result.ResultState.Status)
								{
								case TestStatus.Inconclusive:
								case TestStatus.Skipped:
								case TestStatus.Failed:
									this.SkipChildren(this._suite, base.Result.ResultState.WithSite(FailureSite.Parent), "OneTimeSetUp: " + base.Result.Message);
									break;
								case TestStatus.Passed:
								{
									IEnumerator enumerator = this.RunChildren().GetEnumerator();
									try
									{
										while (enumerator.MoveNext())
										{
											object child = enumerator.Current;
											if (this.CheckForCancellation())
											{
												yield break;
											}
											yield return child;
										}
									}
									finally
									{
										IDisposable disposable;
										if ((disposable = (enumerator as IDisposable)) != null)
										{
											disposable.Dispose();
										}
									}
									break;
								}
								}
							}
							if (base.Context.ExecutionStatus != TestExecutionStatus.AbortRequested && !this.m_DontRunRestoringResult)
							{
								this.PerformOneTimeTearDown();
							}
						}
						break;
					case RunState.Skipped:
						this.SkipFixture(ResultState.Skipped, this.GetSkipReason(), null);
						break;
					case RunState.Ignored:
						this.SkipFixture(ResultState.Ignored, this.GetSkipReason(), null);
						break;
					}
				}
			}
			if (!base.ResultedInDomainReload)
			{
				base.WorkItemComplete();
			}
			yield break;
		}

		// Token: 0x060000C6 RID: 198 RVA: 0x00005970 File Offset: 0x00003B70
		private bool CheckForCancellation()
		{
			bool result;
			if (base.Context.ExecutionStatus != TestExecutionStatus.Running)
			{
				base.Result.SetResult(ResultState.Cancelled, "Test cancelled by user");
				result = true;
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x060000C7 RID: 199 RVA: 0x000059B4 File Offset: 0x00003BB4
		private void InitializeSetUpAndTearDownCommands()
		{
			List<SetUpTearDownItem> list = (this._suite.TypeInfo == null) ? new List<SetUpTearDownItem>() : CommandBuilder.BuildSetUpTearDownList(this._suite.TypeInfo.Type, typeof(OneTimeSetUpAttribute), typeof(OneTimeTearDownAttribute));
			List<TestActionItem> list2 = new List<TestActionItem>();
			foreach (ITestAction testAction in base.Actions)
			{
				bool flag = (testAction.Targets & ActionTargets.Suite) == ActionTargets.Suite || (testAction.Targets == ActionTargets.Default && !(base.Test is ParameterizedMethodSuite));
				bool flag2 = (testAction.Targets & ActionTargets.Test) == ActionTargets.Test && !(base.Test is ParameterizedMethodSuite);
				if (flag)
				{
					list2.Add(new TestActionItem(testAction));
				}
				if (flag2)
				{
					base.Context.UpstreamActions.Add(testAction);
				}
			}
			this._setupCommand = CommandBuilder.MakeOneTimeSetUpCommand(this._suite, list, list2);
			this._teardownCommand = CommandBuilder.MakeOneTimeTearDownCommand(this._suite, list, list2);
		}

		// Token: 0x060000C8 RID: 200 RVA: 0x00005B00 File Offset: 0x00003D00
		private void PerformOneTimeSetUp()
		{
			LogScope logScope = new LogScope();
			try
			{
				this._setupCommand.Execute(base.Context);
			}
			catch (Exception innerException)
			{
				if (innerException is NUnitException || innerException is TargetInvocationException)
				{
					innerException = innerException.InnerException;
				}
				base.Result.RecordException(innerException, FailureSite.SetUp);
			}
			if (logScope.AnyFailingLogs())
			{
				base.Result.RecordException(new UnhandledLogMessageException(logScope.FailingLogs.First<LogEvent>()));
			}
			logScope.Dispose();
		}

		// Token: 0x060000C9 RID: 201 RVA: 0x00005BA0 File Offset: 0x00003DA0
		private IEnumerable RunChildren()
		{
			int childCount = this.Children.Count;
			if (childCount == 0)
			{
				throw new InvalidOperationException("RunChildren called but item has no children");
			}
			this._childTestCountdown = new NUnit.Framework.Internal.Execution.CountdownEvent(childCount);
			foreach (UnityWorkItem child in this.Children)
			{
				if (this.CheckForCancellation())
				{
					yield break;
				}
				UnityTestExecutionContext unityTestExecutionContext = new UnityTestExecutionContext(base.Context);
				child.InitializeContext(unityTestExecutionContext);
				IEnumerator enumerable = child.Execute().GetEnumerator();
				while (enumerable.MoveNext())
				{
					base.ResultedInDomainReload |= child.ResultedInDomainReload;
					yield return enumerable.Current;
				}
				this._suiteResult.AddResult(child.Result);
				childCount--;
			}
			if (childCount > 0)
			{
				for (;;)
				{
					int num;
					childCount = (num = childCount) - 1;
					if (num <= 0)
					{
						break;
					}
					this.CountDownChildTest();
				}
			}
			yield break;
		}

		// Token: 0x060000CA RID: 202 RVA: 0x00005BCC File Offset: 0x00003DCC
		private void CreateChildWorkItems()
		{
			this.Children = new List<UnityWorkItem>();
			TestSuite suite = this._suite;
			foreach (ITest test in suite.Tests)
			{
				if (this._childFilter.Pass(test))
				{
					UnityWorkItem item = this.m_Factory.Create(test, this._childFilter);
					if (test.Properties.ContainsKey("Order"))
					{
						this.Children.Insert(0, item);
						this._countOrder++;
					}
					else
					{
						this.Children.Add(item);
					}
				}
			}
			if (this._countOrder != 0)
			{
				this.SortChildren();
			}
		}

		// Token: 0x060000CB RID: 203 RVA: 0x00005CB0 File Offset: 0x00003EB0
		private void SortChildren()
		{
			this.Children.Sort(0, this._countOrder, new CompositeWorkItem.UnityWorkItemOrderComparer());
		}

		// Token: 0x060000CC RID: 204 RVA: 0x00005CCA File Offset: 0x00003ECA
		private void SkipFixture(ResultState resultState, string message, string stackTrace)
		{
			base.Result.SetResult(resultState.WithSite(FailureSite.SetUp), message, StackFilter.Filter(stackTrace));
			this.SkipChildren(this._suite, resultState.WithSite(FailureSite.Parent), "OneTimeSetUp: " + message);
		}

		// Token: 0x060000CD RID: 205 RVA: 0x00005D04 File Offset: 0x00003F04
		private void SkipChildren(TestSuite suite, ResultState resultState, string message)
		{
			foreach (ITest test in suite.Tests)
			{
				Test test2 = (Test)test;
				if (this._childFilter.Pass(test2))
				{
					base.Context.Listener.TestStarted(test2);
					TestResult testResult = test2.MakeTestResult();
					testResult.SetResult(resultState, message);
					this._suiteResult.AddResult(testResult);
					if (test2.IsSuite)
					{
						this.SkipChildren((TestSuite)test2, resultState, message);
					}
					base.Context.Listener.TestFinished(testResult);
				}
			}
		}

		// Token: 0x060000CE RID: 206 RVA: 0x00005DC8 File Offset: 0x00003FC8
		private void PerformOneTimeTearDown()
		{
			this._teardownCommand.Execute(base.Context);
		}

		// Token: 0x060000CF RID: 207 RVA: 0x00005DE0 File Offset: 0x00003FE0
		private string GetSkipReason()
		{
			return (string)base.Test.Properties.Get("_SKIPREASON");
		}

		// Token: 0x060000D0 RID: 208 RVA: 0x00005E10 File Offset: 0x00004010
		private string GetProviderStackTrace()
		{
			return (string)base.Test.Properties.Get("_PROVIDERSTACKTRACE");
		}

		// Token: 0x060000D1 RID: 209 RVA: 0x00005E40 File Offset: 0x00004040
		private void CountDownChildTest()
		{
			this._childTestCountdown.Signal();
			if (this._childTestCountdown.CurrentCount == 0)
			{
				if (base.Context.ExecutionStatus != TestExecutionStatus.AbortRequested)
				{
					this.PerformOneTimeTearDown();
				}
				foreach (ITestResult testResult in this._suiteResult.Children)
				{
					if (testResult.ResultState == ResultState.Cancelled)
					{
						base.Result.SetResult(ResultState.Cancelled, "Cancelled by user");
						break;
					}
				}
				base.WorkItemComplete();
			}
		}

		// Token: 0x060000D2 RID: 210 RVA: 0x00005F00 File Offset: 0x00004100
		public override void Cancel(bool force)
		{
			if (this.Children != null)
			{
				foreach (UnityWorkItem unityWorkItem in this.Children)
				{
					UnityTestExecutionContext context = unityWorkItem.Context;
					if (context != null)
					{
						context.ExecutionStatus = ((!force) ? TestExecutionStatus.StopRequested : TestExecutionStatus.AbortRequested);
					}
					if (unityWorkItem.State == WorkItemState.Running)
					{
						unityWorkItem.Cancel(force);
					}
				}
			}
		}

		// Token: 0x04000042 RID: 66
		private readonly TestSuite _suite;

		// Token: 0x04000043 RID: 67
		private readonly TestSuiteResult _suiteResult;

		// Token: 0x04000044 RID: 68
		private readonly ITestFilter _childFilter;

		// Token: 0x04000045 RID: 69
		private TestCommand _setupCommand;

		// Token: 0x04000046 RID: 70
		private TestCommand _teardownCommand;

		// Token: 0x04000047 RID: 71
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private List<UnityWorkItem> <Children>k__BackingField;

		// Token: 0x04000048 RID: 72
		private int _countOrder;

		// Token: 0x04000049 RID: 73
		private NUnit.Framework.Internal.Execution.CountdownEvent _childTestCountdown;

		// Token: 0x02000027 RID: 39
		private class UnityWorkItemOrderComparer : IComparer<UnityWorkItem>
		{
			// Token: 0x060000D3 RID: 211 RVA: 0x00005F9C File Offset: 0x0000419C
			public UnityWorkItemOrderComparer()
			{
			}

			// Token: 0x060000D4 RID: 212 RVA: 0x00005FA4 File Offset: 0x000041A4
			public int Compare(UnityWorkItem x, UnityWorkItem y)
			{
				int num = int.MaxValue;
				int value = int.MaxValue;
				if (x.Test.Properties.ContainsKey("Order"))
				{
					num = (int)x.Test.Properties["Order"][0];
				}
				if (y.Test.Properties.ContainsKey("Order"))
				{
					value = (int)y.Test.Properties["Order"][0];
				}
				return num.CompareTo(value);
			}
		}

		// Token: 0x02000078 RID: 120
		[CompilerGenerated]
		private sealed class <PerformWork>c__Iterator0 : IEnumerable, IEnumerable<object>, IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x06000266 RID: 614 RVA: 0x00006043 File Offset: 0x00004243
			[DebuggerHidden]
			public <PerformWork>c__Iterator0()
			{
			}

			// Token: 0x06000267 RID: 615 RVA: 0x0000604C File Offset: 0x0000424C
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				bool flag = false;
				switch (num)
				{
				case 0U:
				{
					base.InitializeSetUpAndTearDownCommands();
					if (UnityTestExecutionContext.CurrentContext != null && this.m_DontRunRestoringResult && EditModeTestCallbacks.RestoringTestContext != null)
					{
						EditModeTestCallbacks.RestoringTestContext();
					}
					if (base.CheckForCancellation())
					{
						goto IL_36A;
					}
					if (base.Test.RunState == RunState.Explicit && !this._childFilter.IsExplicitMatch(base.Test))
					{
						base.SkipFixture(ResultState.Explicit, base.GetSkipReason(), null);
						goto IL_36A;
					}
					RunState runState = base.Test.RunState;
					switch (runState)
					{
					case RunState.NotRunnable:
						base.SkipFixture(ResultState.NotRunnable, base.GetSkipReason(), base.GetProviderStackTrace());
						goto IL_36A;
					default:
						base.Result.SetResult(ResultState.Success);
						base.CreateChildWorkItems();
						if (base.Children.Count <= 0)
						{
							goto IL_2F8;
						}
						if (!this.m_DontRunRestoringResult)
						{
							this.$current = null;
							if (!this.$disposing)
							{
								this.$PC = 1;
							}
							return true;
						}
						break;
					case RunState.Skipped:
						base.SkipFixture(ResultState.Skipped, base.GetSkipReason(), null);
						goto IL_36A;
					case RunState.Ignored:
						base.SkipFixture(ResultState.Ignored, base.GetSkipReason(), null);
						goto IL_36A;
					}
					break;
				}
				case 1U:
					base.PerformOneTimeSetUp();
					break;
				case 2U:
					Block_13:
					try
					{
						switch (num)
						{
						}
						if (enumerator.MoveNext())
						{
							child = enumerator.Current;
							if (base.CheckForCancellation())
							{
								return false;
							}
							this.$current = child;
							if (!this.$disposing)
							{
								this.$PC = 2;
							}
							flag = true;
							return true;
						}
					}
					finally
					{
						if (!flag)
						{
							if ((disposable = (enumerator as IDisposable)) != null)
							{
								disposable.Dispose();
							}
						}
					}
					goto IL_2C3;
				default:
					return false;
				}
				if (!base.CheckForCancellation())
				{
					TestStatus status = base.Result.ResultState.Status;
					switch (status)
					{
					case TestStatus.Inconclusive:
					case TestStatus.Skipped:
					case TestStatus.Failed:
						base.SkipChildren(this._suite, base.Result.ResultState.WithSite(FailureSite.Parent), "OneTimeSetUp: " + base.Result.Message);
						break;
					case TestStatus.Passed:
						enumerator = base.RunChildren().GetEnumerator();
						num = 4294967293U;
						goto Block_13;
					}
				}
				IL_2C3:
				if (base.Context.ExecutionStatus != TestExecutionStatus.AbortRequested && !this.m_DontRunRestoringResult)
				{
					base.PerformOneTimeTearDown();
				}
				IL_2F8:
				IL_36A:
				if (!base.ResultedInDomainReload)
				{
					base.WorkItemComplete();
				}
				this.$PC = -1;
				return false;
			}

			// Token: 0x17000083 RID: 131
			// (get) Token: 0x06000268 RID: 616 RVA: 0x000063FC File Offset: 0x000045FC
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x17000084 RID: 132
			// (get) Token: 0x06000269 RID: 617 RVA: 0x00006418 File Offset: 0x00004618
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x0600026A RID: 618 RVA: 0x00006434 File Offset: 0x00004634
			[DebuggerHidden]
			public void Dispose()
			{
				uint num = (uint)this.$PC;
				this.$disposing = true;
				this.$PC = -1;
				switch (num)
				{
				case 2U:
					try
					{
					}
					finally
					{
						if ((disposable = (enumerator as IDisposable)) != null)
						{
							disposable.Dispose();
						}
					}
					break;
				}
			}

			// Token: 0x0600026B RID: 619 RVA: 0x000064A8 File Offset: 0x000046A8
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x0600026C RID: 620 RVA: 0x000064B0 File Offset: 0x000046B0
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<object>.GetEnumerator();
			}

			// Token: 0x0600026D RID: 621 RVA: 0x000064CC File Offset: 0x000046CC
			[DebuggerHidden]
			IEnumerator<object> IEnumerable<object>.GetEnumerator()
			{
				if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
				{
					return this;
				}
				CompositeWorkItem.<PerformWork>c__Iterator0 <PerformWork>c__Iterator = new CompositeWorkItem.<PerformWork>c__Iterator0();
				<PerformWork>c__Iterator.$this = this;
				return <PerformWork>c__Iterator;
			}

			// Token: 0x04000148 RID: 328
			internal RunState $locvar0;

			// Token: 0x04000149 RID: 329
			internal TestStatus $locvar1;

			// Token: 0x0400014A RID: 330
			internal IEnumerator $locvar2;

			// Token: 0x0400014B RID: 331
			internal object <child>__1;

			// Token: 0x0400014C RID: 332
			internal IDisposable $locvar3;

			// Token: 0x0400014D RID: 333
			internal CompositeWorkItem $this;

			// Token: 0x0400014E RID: 334
			internal object $current;

			// Token: 0x0400014F RID: 335
			internal bool $disposing;

			// Token: 0x04000150 RID: 336
			internal int $PC;
		}

		// Token: 0x02000079 RID: 121
		[CompilerGenerated]
		private sealed class <RunChildren>c__Iterator1 : IEnumerable, IEnumerable<object>, IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x0600026E RID: 622 RVA: 0x00006500 File Offset: 0x00004700
			[DebuggerHidden]
			public <RunChildren>c__Iterator1()
			{
			}

			// Token: 0x0600026F RID: 623 RVA: 0x00006508 File Offset: 0x00004708
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				bool flag = false;
				switch (num)
				{
				case 0U:
					childCount = base.Children.Count;
					if (childCount == 0)
					{
						throw new InvalidOperationException("RunChildren called but item has no children");
					}
					this._childTestCountdown = new NUnit.Framework.Internal.Execution.CountdownEvent(childCount);
					enumerator = base.Children.GetEnumerator();
					num = 4294967293U;
					break;
				case 1U:
					break;
				default:
					return false;
				}
				try
				{
					switch (num)
					{
					case 1U:
						break;
					default:
						goto IL_182;
					}
					IL_F6:
					if (enumerable.MoveNext())
					{
						base.ResultedInDomainReload |= child.ResultedInDomainReload;
						this.$current = enumerable.Current;
						if (!this.$disposing)
						{
							this.$PC = 1;
						}
						flag = true;
						return true;
					}
					this._suiteResult.AddResult(child.Result);
					childCount--;
					IL_182:
					if (enumerator.MoveNext())
					{
						child = enumerator.Current;
						if (base.CheckForCancellation())
						{
							return false;
						}
						unityTestExecutionContext = new UnityTestExecutionContext(base.Context);
						child.InitializeContext(unityTestExecutionContext);
						enumerable = child.Execute().GetEnumerator();
						goto IL_F6;
					}
				}
				finally
				{
					if (!flag)
					{
						((IDisposable)enumerator).Dispose();
					}
				}
				if (childCount > 0)
				{
					while (childCount-- > 0)
					{
						base.CountDownChildTest();
					}
				}
				this.$PC = -1;
				return false;
			}

			// Token: 0x17000085 RID: 133
			// (get) Token: 0x06000270 RID: 624 RVA: 0x00006720 File Offset: 0x00004920
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x17000086 RID: 134
			// (get) Token: 0x06000271 RID: 625 RVA: 0x0000673C File Offset: 0x0000493C
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x06000272 RID: 626 RVA: 0x00006758 File Offset: 0x00004958
			[DebuggerHidden]
			public void Dispose()
			{
				uint num = (uint)this.$PC;
				this.$disposing = true;
				this.$PC = -1;
				switch (num)
				{
				case 1U:
					try
					{
					}
					finally
					{
						((IDisposable)enumerator).Dispose();
					}
					break;
				}
			}

			// Token: 0x06000273 RID: 627 RVA: 0x000067B4 File Offset: 0x000049B4
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x06000274 RID: 628 RVA: 0x000067BC File Offset: 0x000049BC
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<object>.GetEnumerator();
			}

			// Token: 0x06000275 RID: 629 RVA: 0x000067D8 File Offset: 0x000049D8
			[DebuggerHidden]
			IEnumerator<object> IEnumerable<object>.GetEnumerator()
			{
				if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
				{
					return this;
				}
				CompositeWorkItem.<RunChildren>c__Iterator1 <RunChildren>c__Iterator = new CompositeWorkItem.<RunChildren>c__Iterator1();
				<RunChildren>c__Iterator.$this = this;
				return <RunChildren>c__Iterator;
			}

			// Token: 0x04000151 RID: 337
			internal int <childCount>__0;

			// Token: 0x04000152 RID: 338
			internal List<UnityWorkItem>.Enumerator $locvar0;

			// Token: 0x04000153 RID: 339
			internal UnityWorkItem <child>__1;

			// Token: 0x04000154 RID: 340
			internal UnityTestExecutionContext <unityTestExecutionContext>__2;

			// Token: 0x04000155 RID: 341
			internal IEnumerator <enumerable>__2;

			// Token: 0x04000156 RID: 342
			internal CompositeWorkItem $this;

			// Token: 0x04000157 RID: 343
			internal object $current;

			// Token: 0x04000158 RID: 344
			internal bool $disposing;

			// Token: 0x04000159 RID: 345
			internal int $PC;
		}
	}
}
