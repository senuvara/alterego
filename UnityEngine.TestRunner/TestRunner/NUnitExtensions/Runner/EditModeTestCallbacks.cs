﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.TestRunner.NUnitExtensions.Runner
{
	// Token: 0x02000029 RID: 41
	internal class EditModeTestCallbacks
	{
		// Token: 0x060000D9 RID: 217 RVA: 0x00006BD7 File Offset: 0x00004DD7
		public EditModeTestCallbacks()
		{
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x060000DA RID: 218 RVA: 0x00006BE0 File Offset: 0x00004DE0
		// (set) Token: 0x060000DB RID: 219 RVA: 0x00006BF9 File Offset: 0x00004DF9
		public static Action RestoringTestContext
		{
			[CompilerGenerated]
			get
			{
				return EditModeTestCallbacks.<RestoringTestContext>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				EditModeTestCallbacks.<RestoringTestContext>k__BackingField = value;
			}
		}

		// Token: 0x0400004C RID: 76
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static Action <RestoringTestContext>k__BackingField;
	}
}
