﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Commands;
using UnityEngine.TestTools.Logging;
using UnityEngine.TestTools.TestRunner;

namespace UnityEngine.TestRunner.NUnitExtensions.Runner
{
	// Token: 0x0200002E RID: 46
	internal class UnityLogCheckDelegatingCommand : DelegatingTestCommand, IEnumerableTestMethodCommand
	{
		// Token: 0x060000E3 RID: 227 RVA: 0x00007298 File Offset: 0x00005498
		public UnityLogCheckDelegatingCommand(TestCommand innerCommand) : base(innerCommand)
		{
		}

		// Token: 0x060000E4 RID: 228 RVA: 0x000072A4 File Offset: 0x000054A4
		public override TestResult Execute(ITestExecutionContext context)
		{
			LogScope logScope = new LogScope();
			try
			{
				this.innerCommand.Execute(context);
				if (logScope.AnyFailingLogs())
				{
					LogEvent log = logScope.FailingLogs.First<LogEvent>();
					throw new UnhandledLogMessageException(log);
				}
				if (logScope.ExpectedLogs.Any<LogMatch>())
				{
					throw new UnexpectedLogMessageException(logScope.ExpectedLogs.Peek());
				}
			}
			catch (Exception ex)
			{
				context.CurrentResult.RecordException(ex);
			}
			logScope.Dispose();
			return context.CurrentResult;
		}

		// Token: 0x060000E5 RID: 229 RVA: 0x00007344 File Offset: 0x00005544
		public IEnumerable ExecuteEnumerable(ITestExecutionContext context)
		{
			LogScope logCollector = new LogScope();
			if (!(this.innerCommand is IEnumerableTestMethodCommand))
			{
				this.Execute(context);
				yield break;
			}
			IEnumerableTestMethodCommand enumerableTestMethodCommand = (IEnumerableTestMethodCommand)this.innerCommand;
			IEnumerable executeEnumerable;
			try
			{
				executeEnumerable = enumerableTestMethodCommand.ExecuteEnumerable(context);
			}
			catch (Exception ex)
			{
				context.CurrentResult.RecordException(ex);
				yield break;
			}
			IEnumerator enumerator = executeEnumerable.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object step = enumerator.Current;
					try
					{
						if (logCollector.AnyFailingLogs())
						{
							LogEvent log = logCollector.FailingLogs.First<LogEvent>();
							throw new UnhandledLogMessageException(log);
						}
					}
					catch (Exception ex2)
					{
						context.CurrentResult.RecordException(ex2);
						break;
					}
					yield return step;
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
			try
			{
				if (logCollector.AnyFailingLogs())
				{
					LogEvent log2 = logCollector.FailingLogs.First<LogEvent>();
					throw new UnhandledLogMessageException(log2);
				}
				logCollector.ProcessExpectedLogs();
				if (logCollector.ExpectedLogs.Any<LogMatch>())
				{
					throw new UnexpectedLogMessageException(LogScope.Current.ExpectedLogs.Peek());
				}
			}
			catch (Exception ex3)
			{
				context.CurrentResult.RecordException(ex3);
			}
			logCollector.Dispose();
			yield break;
		}

		// Token: 0x0200007C RID: 124
		[CompilerGenerated]
		private sealed class <ExecuteEnumerable>c__Iterator0 : IEnumerable, IEnumerable<object>, IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x06000288 RID: 648 RVA: 0x00007375 File Offset: 0x00005575
			[DebuggerHidden]
			public <ExecuteEnumerable>c__Iterator0()
			{
			}

			// Token: 0x06000289 RID: 649 RVA: 0x00007380 File Offset: 0x00005580
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				bool flag = false;
				switch (num)
				{
				case 0U:
					logCollector = new LogScope();
					if (!(this.innerCommand is IEnumerableTestMethodCommand))
					{
						this.Execute(context);
						return false;
					}
					enumerableTestMethodCommand = (IEnumerableTestMethodCommand)this.innerCommand;
					try
					{
						executeEnumerable = enumerableTestMethodCommand.ExecuteEnumerable(context);
					}
					catch (Exception ex)
					{
						context.CurrentResult.RecordException(ex);
						return false;
					}
					enumerator = executeEnumerable.GetEnumerator();
					num = 4294967293U;
					break;
				case 1U:
					break;
				default:
					return false;
				}
				try
				{
					switch (num)
					{
					}
					if (enumerator.MoveNext())
					{
						step = enumerator.Current;
						try
						{
							if (logCollector.AnyFailingLogs())
							{
								LogEvent log = logCollector.FailingLogs.First<LogEvent>();
								throw new UnhandledLogMessageException(log);
							}
						}
						catch (Exception ex2)
						{
							context.CurrentResult.RecordException(ex2);
							goto IL_15D;
						}
						this.$current = step;
						if (!this.$disposing)
						{
							this.$PC = 1;
						}
						flag = true;
						return true;
					}
					IL_15D:;
				}
				finally
				{
					if (!flag)
					{
						if ((disposable = (enumerator as IDisposable)) != null)
						{
							disposable.Dispose();
						}
					}
				}
				try
				{
					if (logCollector.AnyFailingLogs())
					{
						LogEvent log2 = logCollector.FailingLogs.First<LogEvent>();
						throw new UnhandledLogMessageException(log2);
					}
					logCollector.ProcessExpectedLogs();
					if (logCollector.ExpectedLogs.Any<LogMatch>())
					{
						throw new UnexpectedLogMessageException(LogScope.Current.ExpectedLogs.Peek());
					}
				}
				catch (Exception ex3)
				{
					context.CurrentResult.RecordException(ex3);
				}
				logCollector.Dispose();
				this.$PC = -1;
				return false;
			}

			// Token: 0x1700008B RID: 139
			// (get) Token: 0x0600028A RID: 650 RVA: 0x000075E8 File Offset: 0x000057E8
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x1700008C RID: 140
			// (get) Token: 0x0600028B RID: 651 RVA: 0x00007604 File Offset: 0x00005804
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x0600028C RID: 652 RVA: 0x00007620 File Offset: 0x00005820
			[DebuggerHidden]
			public void Dispose()
			{
				uint num = (uint)this.$PC;
				this.$disposing = true;
				this.$PC = -1;
				switch (num)
				{
				case 1U:
					try
					{
					}
					finally
					{
						if ((disposable = (enumerator as IDisposable)) != null)
						{
							disposable.Dispose();
						}
					}
					break;
				}
			}

			// Token: 0x0600028D RID: 653 RVA: 0x00007690 File Offset: 0x00005890
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x0600028E RID: 654 RVA: 0x00007698 File Offset: 0x00005898
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<object>.GetEnumerator();
			}

			// Token: 0x0600028F RID: 655 RVA: 0x000076B4 File Offset: 0x000058B4
			[DebuggerHidden]
			IEnumerator<object> IEnumerable<object>.GetEnumerator()
			{
				if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
				{
					return this;
				}
				UnityLogCheckDelegatingCommand.<ExecuteEnumerable>c__Iterator0 <ExecuteEnumerable>c__Iterator = new UnityLogCheckDelegatingCommand.<ExecuteEnumerable>c__Iterator0();
				<ExecuteEnumerable>c__Iterator.$this = this;
				<ExecuteEnumerable>c__Iterator.context = context;
				return <ExecuteEnumerable>c__Iterator;
			}

			// Token: 0x0400016B RID: 363
			internal LogScope <logCollector>__0;

			// Token: 0x0400016C RID: 364
			internal ITestExecutionContext context;

			// Token: 0x0400016D RID: 365
			internal IEnumerableTestMethodCommand <enumerableTestMethodCommand>__0;

			// Token: 0x0400016E RID: 366
			internal IEnumerable <executeEnumerable>__1;

			// Token: 0x0400016F RID: 367
			internal IEnumerator $locvar0;

			// Token: 0x04000170 RID: 368
			internal object <step>__2;

			// Token: 0x04000171 RID: 369
			internal IDisposable $locvar1;

			// Token: 0x04000172 RID: 370
			internal UnityLogCheckDelegatingCommand $this;

			// Token: 0x04000173 RID: 371
			internal object $current;

			// Token: 0x04000174 RID: 372
			internal bool $disposing;

			// Token: 0x04000175 RID: 373
			internal int $PC;
		}
	}
}
