﻿using System;
using System.Collections;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace UnityEngine.TestRunner.NUnitExtensions.Runner
{
	// Token: 0x02000034 RID: 52
	internal abstract class WorkItemFactory
	{
		// Token: 0x06000150 RID: 336 RVA: 0x000071F3 File Offset: 0x000053F3
		protected WorkItemFactory()
		{
		}

		// Token: 0x06000151 RID: 337 RVA: 0x000071FC File Offset: 0x000053FC
		public UnityWorkItem Create(ITest loadedTest, ITestFilter filter)
		{
			TestSuite testSuite = loadedTest as TestSuite;
			UnityWorkItem result;
			if (testSuite != null)
			{
				result = new CompositeWorkItem(testSuite, filter, this);
			}
			else
			{
				TestMethod testMethod = (TestMethod)loadedTest;
				if (testMethod.Method.ReturnType.Type != typeof(IEnumerator))
				{
					result = new DefaultTestWorkItem(testMethod, filter);
				}
				else
				{
					result = this.Create(testMethod, filter, loadedTest);
				}
			}
			return result;
		}

		// Token: 0x06000152 RID: 338
		protected abstract UnityWorkItem Create(TestMethod method, ITestFilter filter, ITest loadedTest);
	}
}
