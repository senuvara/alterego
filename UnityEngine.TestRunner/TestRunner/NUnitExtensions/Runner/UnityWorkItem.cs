﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Execution;

namespace UnityEngine.TestRunner.NUnitExtensions.Runner
{
	// Token: 0x02000032 RID: 50
	internal abstract class UnityWorkItem
	{
		// Token: 0x06000137 RID: 311 RVA: 0x000054C4 File Offset: 0x000036C4
		protected UnityWorkItem(Test test, WorkItemFactory factory)
		{
			this.m_Factory = factory;
			this.Test = test;
			this.Actions = new List<ITestAction>();
			this.Result = test.MakeTestResult();
			this.State = WorkItemState.Ready;
			this.m_ExecuteTestStartEvent = this.ShouldExecuteStartEvent();
			this.m_DontRunRestoringResult = UnityWorkItem.ShouldRestore(test);
		}

		// Token: 0x14000001 RID: 1
		// (add) Token: 0x06000138 RID: 312 RVA: 0x0000551C File Offset: 0x0000371C
		// (remove) Token: 0x06000139 RID: 313 RVA: 0x00005554 File Offset: 0x00003754
		public event EventHandler Completed
		{
			add
			{
				EventHandler eventHandler = this.Completed;
				EventHandler eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					eventHandler = Interlocked.CompareExchange<EventHandler>(ref this.Completed, (EventHandler)Delegate.Combine(eventHandler2, value), eventHandler);
				}
				while (eventHandler != eventHandler2);
			}
			remove
			{
				EventHandler eventHandler = this.Completed;
				EventHandler eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					eventHandler = Interlocked.CompareExchange<EventHandler>(ref this.Completed, (EventHandler)Delegate.Remove(eventHandler2, value), eventHandler);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x0600013A RID: 314 RVA: 0x0000558C File Offset: 0x0000378C
		// (set) Token: 0x0600013B RID: 315 RVA: 0x000055A6 File Offset: 0x000037A6
		public bool ResultedInDomainReload
		{
			[CompilerGenerated]
			get
			{
				return this.<ResultedInDomainReload>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<ResultedInDomainReload>k__BackingField = value;
			}
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x0600013C RID: 316 RVA: 0x000055B0 File Offset: 0x000037B0
		// (set) Token: 0x0600013D RID: 317 RVA: 0x000055CA File Offset: 0x000037CA
		public UnityTestExecutionContext Context
		{
			[CompilerGenerated]
			get
			{
				return this.<Context>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Context>k__BackingField = value;
			}
		}

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x0600013E RID: 318 RVA: 0x000055D4 File Offset: 0x000037D4
		// (set) Token: 0x0600013F RID: 319 RVA: 0x000055EE File Offset: 0x000037EE
		public Test Test
		{
			[CompilerGenerated]
			get
			{
				return this.<Test>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Test>k__BackingField = value;
			}
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x06000140 RID: 320 RVA: 0x000055F8 File Offset: 0x000037F8
		// (set) Token: 0x06000141 RID: 321 RVA: 0x00005612 File Offset: 0x00003812
		public TestResult Result
		{
			[CompilerGenerated]
			get
			{
				return this.<Result>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<Result>k__BackingField = value;
			}
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x06000142 RID: 322 RVA: 0x0000561C File Offset: 0x0000381C
		// (set) Token: 0x06000143 RID: 323 RVA: 0x00005636 File Offset: 0x00003836
		public WorkItemState State
		{
			[CompilerGenerated]
			get
			{
				return this.<State>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<State>k__BackingField = value;
			}
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x06000144 RID: 324 RVA: 0x00005640 File Offset: 0x00003840
		// (set) Token: 0x06000145 RID: 325 RVA: 0x0000565A File Offset: 0x0000385A
		public List<ITestAction> Actions
		{
			[CompilerGenerated]
			get
			{
				return this.<Actions>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Actions>k__BackingField = value;
			}
		}

		// Token: 0x06000146 RID: 326 RVA: 0x00005664 File Offset: 0x00003864
		protected static bool ShouldRestore(ITest loadedTest)
		{
			return UnityWorkItemDataHolder.alreadyExecutedTests != null && UnityWorkItemDataHolder.alreadyExecutedTests.Contains(loadedTest.FullName);
		}

		// Token: 0x06000147 RID: 327 RVA: 0x00005698 File Offset: 0x00003898
		protected bool ShouldExecuteStartEvent()
		{
			return UnityWorkItemDataHolder.alreadyStartedTests != null && UnityWorkItemDataHolder.alreadyStartedTests.All((string x) => x != this.Test.FullName);
		}

		// Token: 0x06000148 RID: 328
		protected abstract IEnumerable PerformWork();

		// Token: 0x06000149 RID: 329 RVA: 0x000056D0 File Offset: 0x000038D0
		public void InitializeContext(UnityTestExecutionContext context)
		{
			this.Context = context;
			if (this.Test is TestAssembly)
			{
				this.Actions.AddRange(ActionsHelper.GetActionsFromTestAssembly((TestAssembly)this.Test));
			}
			else if (this.Test is ParameterizedMethodSuite)
			{
				this.Actions.AddRange(ActionsHelper.GetActionsFromTestMethodInfo(this.Test.Method));
			}
			else if (this.Test.TypeInfo != null)
			{
				this.Actions.AddRange(ActionsHelper.GetActionsFromTypesAttributes(this.Test.TypeInfo.Type));
			}
		}

		// Token: 0x0600014A RID: 330 RVA: 0x00005778 File Offset: 0x00003978
		public virtual IEnumerable Execute()
		{
			this.Context.CurrentTest = this.Test;
			this.Context.CurrentResult = this.Result;
			if (this.m_ExecuteTestStartEvent)
			{
				this.Context.Listener.TestStarted(this.Test);
			}
			this.Context.StartTime = DateTime.UtcNow;
			this.Context.StartTicks = Stopwatch.GetTimestamp();
			this.State = WorkItemState.Running;
			return this.PerformWork();
		}

		// Token: 0x0600014B RID: 331 RVA: 0x00005800 File Offset: 0x00003A00
		protected void WorkItemComplete()
		{
			this.State = WorkItemState.Complete;
			this.Result.StartTime = this.Context.StartTime;
			this.Result.EndTime = DateTime.UtcNow;
			long num = Stopwatch.GetTimestamp() - this.Context.StartTicks;
			double duration = (double)num / (double)Stopwatch.Frequency;
			this.Result.Duration = duration;
			this.Context.Listener.TestFinished(this.Result);
			if (this.Completed != null)
			{
				this.Completed(this, EventArgs.Empty);
			}
			this.Context.TestObject = null;
			this.Test.Fixture = null;
		}

		// Token: 0x0600014C RID: 332 RVA: 0x000058AD File Offset: 0x00003AAD
		public virtual void Cancel(bool force)
		{
			this.Context.Listener.TestFinished(this.Result);
		}

		// Token: 0x0600014D RID: 333 RVA: 0x000058C8 File Offset: 0x00003AC8
		[CompilerGenerated]
		private bool <ShouldExecuteStartEvent>m__0(string x)
		{
			return x != this.Test.FullName;
		}

		// Token: 0x0400006E RID: 110
		protected readonly WorkItemFactory m_Factory;

		// Token: 0x0400006F RID: 111
		protected bool m_ExecuteTestStartEvent;

		// Token: 0x04000070 RID: 112
		protected bool m_DontRunRestoringResult;

		// Token: 0x04000071 RID: 113
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private EventHandler Completed;

		// Token: 0x04000072 RID: 114
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <ResultedInDomainReload>k__BackingField;

		// Token: 0x04000073 RID: 115
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private UnityTestExecutionContext <Context>k__BackingField;

		// Token: 0x04000074 RID: 116
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Test <Test>k__BackingField;

		// Token: 0x04000075 RID: 117
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private TestResult <Result>k__BackingField;

		// Token: 0x04000076 RID: 118
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private WorkItemState <State>k__BackingField;

		// Token: 0x04000077 RID: 119
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private List<ITestAction> <Actions>k__BackingField;
	}
}
