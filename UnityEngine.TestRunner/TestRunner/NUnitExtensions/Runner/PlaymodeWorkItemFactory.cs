﻿using System;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace UnityEngine.TestRunner.NUnitExtensions.Runner
{
	// Token: 0x0200002C RID: 44
	internal class PlaymodeWorkItemFactory : WorkItemFactory
	{
		// Token: 0x060000E0 RID: 224 RVA: 0x0000726A File Offset: 0x0000546A
		public PlaymodeWorkItemFactory()
		{
		}

		// Token: 0x060000E1 RID: 225 RVA: 0x00007274 File Offset: 0x00005474
		protected override UnityWorkItem Create(TestMethod method, ITestFilter filter, ITest loadedTest)
		{
			return new CoroutineTestWorkItem(method, filter);
		}
	}
}
