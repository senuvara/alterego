﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Execution;
using UnityEngine.TestTools;

namespace UnityEngine.TestRunner.NUnitExtensions.Runner
{
	// Token: 0x02000031 RID: 49
	internal class UnityTestExecutionContext : ITestExecutionContext
	{
		// Token: 0x06000102 RID: 258 RVA: 0x00007957 File Offset: 0x00005B57
		public UnityTestExecutionContext()
		{
			this.UpstreamActions = new List<ITestAction>();
			UnityTestExecutionContext.CurrentContext = this;
		}

		// Token: 0x06000103 RID: 259 RVA: 0x00007974 File Offset: 0x00005B74
		public UnityTestExecutionContext(UnityTestExecutionContext other)
		{
			this._priorContext = other;
			this.CurrentTest = other.CurrentTest;
			this.CurrentResult = other.CurrentResult;
			this.TestObject = other.TestObject;
			this.WorkDirectory = other.WorkDirectory;
			this.Listener = other.Listener;
			this.TestCaseTimeout = other.TestCaseTimeout;
			this.UpstreamActions = new List<ITestAction>(other.UpstreamActions);
			this.SetUpTearDownState = other.SetUpTearDownState;
			this.OuterUnityTestActionState = other.OuterUnityTestActionState;
			TestContext.CurrentTestExecutionContext = this;
			this.CurrentCulture = other.CurrentCulture;
			this.CurrentUICulture = other.CurrentUICulture;
			UnityTestExecutionContext.CurrentContext = this;
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x06000104 RID: 260 RVA: 0x00007A24 File Offset: 0x00005C24
		// (set) Token: 0x06000105 RID: 261 RVA: 0x00007A3D File Offset: 0x00005C3D
		public static UnityTestExecutionContext CurrentContext
		{
			[CompilerGenerated]
			get
			{
				return UnityTestExecutionContext.<CurrentContext>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				UnityTestExecutionContext.<CurrentContext>k__BackingField = value;
			}
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x06000106 RID: 262 RVA: 0x00007A48 File Offset: 0x00005C48
		// (set) Token: 0x06000107 RID: 263 RVA: 0x00007A62 File Offset: 0x00005C62
		public UnityTestExecutionContext Context
		{
			[CompilerGenerated]
			get
			{
				return this.<Context>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Context>k__BackingField = value;
			}
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x06000108 RID: 264 RVA: 0x00007A6C File Offset: 0x00005C6C
		// (set) Token: 0x06000109 RID: 265 RVA: 0x00007A86 File Offset: 0x00005C86
		public Test CurrentTest
		{
			[CompilerGenerated]
			get
			{
				return this.<CurrentTest>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<CurrentTest>k__BackingField = value;
			}
		}

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x0600010A RID: 266 RVA: 0x00007A90 File Offset: 0x00005C90
		// (set) Token: 0x0600010B RID: 267 RVA: 0x00007AAA File Offset: 0x00005CAA
		public DateTime StartTime
		{
			[CompilerGenerated]
			get
			{
				return this.<StartTime>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<StartTime>k__BackingField = value;
			}
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x0600010C RID: 268 RVA: 0x00007AB4 File Offset: 0x00005CB4
		// (set) Token: 0x0600010D RID: 269 RVA: 0x00007ACE File Offset: 0x00005CCE
		public long StartTicks
		{
			[CompilerGenerated]
			get
			{
				return this.<StartTicks>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<StartTicks>k__BackingField = value;
			}
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x0600010E RID: 270 RVA: 0x00007AD8 File Offset: 0x00005CD8
		// (set) Token: 0x0600010F RID: 271 RVA: 0x00007AF3 File Offset: 0x00005CF3
		public TestResult CurrentResult
		{
			get
			{
				return this._currentResult;
			}
			set
			{
				this._currentResult = value;
				if (value != null)
				{
					this.OutWriter = value.OutWriter;
				}
			}
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x06000110 RID: 272 RVA: 0x00007B10 File Offset: 0x00005D10
		// (set) Token: 0x06000111 RID: 273 RVA: 0x00007B2A File Offset: 0x00005D2A
		public object TestObject
		{
			[CompilerGenerated]
			get
			{
				return this.<TestObject>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<TestObject>k__BackingField = value;
			}
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x06000112 RID: 274 RVA: 0x00007B34 File Offset: 0x00005D34
		// (set) Token: 0x06000113 RID: 275 RVA: 0x00007B4E File Offset: 0x00005D4E
		public string WorkDirectory
		{
			[CompilerGenerated]
			get
			{
				return this.<WorkDirectory>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<WorkDirectory>k__BackingField = value;
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x06000114 RID: 276 RVA: 0x00007B58 File Offset: 0x00005D58
		// (set) Token: 0x06000115 RID: 277 RVA: 0x00007B9A File Offset: 0x00005D9A
		public TestExecutionStatus ExecutionStatus
		{
			get
			{
				if (this._executionStatus == TestExecutionStatus.Running && this._priorContext != null)
				{
					this._executionStatus = this._priorContext.ExecutionStatus;
				}
				return this._executionStatus;
			}
			set
			{
				this._executionStatus = value;
				if (this._priorContext != null)
				{
					this._priorContext.ExecutionStatus = value;
				}
			}
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x06000116 RID: 278 RVA: 0x00007BBC File Offset: 0x00005DBC
		// (set) Token: 0x06000117 RID: 279 RVA: 0x00007BD6 File Offset: 0x00005DD6
		public List<ITestAction> UpstreamActions
		{
			[CompilerGenerated]
			get
			{
				return this.<UpstreamActions>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<UpstreamActions>k__BackingField = value;
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x06000118 RID: 280 RVA: 0x00007BE0 File Offset: 0x00005DE0
		// (set) Token: 0x06000119 RID: 281 RVA: 0x00007BFA File Offset: 0x00005DFA
		public int TestCaseTimeout
		{
			[CompilerGenerated]
			get
			{
				return this.<TestCaseTimeout>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<TestCaseTimeout>k__BackingField = value;
			}
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x0600011A RID: 282 RVA: 0x00007C04 File Offset: 0x00005E04
		// (set) Token: 0x0600011B RID: 283 RVA: 0x00007C1E File Offset: 0x00005E1E
		public CultureInfo CurrentCulture
		{
			[CompilerGenerated]
			get
			{
				return this.<CurrentCulture>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<CurrentCulture>k__BackingField = value;
			}
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x0600011C RID: 284 RVA: 0x00007C28 File Offset: 0x00005E28
		// (set) Token: 0x0600011D RID: 285 RVA: 0x00007C42 File Offset: 0x00005E42
		public CultureInfo CurrentUICulture
		{
			[CompilerGenerated]
			get
			{
				return this.<CurrentUICulture>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<CurrentUICulture>k__BackingField = value;
			}
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x0600011E RID: 286 RVA: 0x00007C4C File Offset: 0x00005E4C
		// (set) Token: 0x0600011F RID: 287 RVA: 0x00007C66 File Offset: 0x00005E66
		public ITestListener Listener
		{
			[CompilerGenerated]
			get
			{
				return this.<Listener>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Listener>k__BackingField = value;
			}
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x06000120 RID: 288 RVA: 0x00007C70 File Offset: 0x00005E70
		// (set) Token: 0x06000121 RID: 289 RVA: 0x00007C8A File Offset: 0x00005E8A
		public TextWriter OutWriter
		{
			[CompilerGenerated]
			get
			{
				return this.<OutWriter>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<OutWriter>k__BackingField = value;
			}
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x06000122 RID: 290 RVA: 0x00007C94 File Offset: 0x00005E94
		// (set) Token: 0x06000123 RID: 291 RVA: 0x00007CAE File Offset: 0x00005EAE
		public bool StopOnError
		{
			[CompilerGenerated]
			get
			{
				return this.<StopOnError>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<StopOnError>k__BackingField = value;
			}
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x06000124 RID: 292 RVA: 0x00007CB8 File Offset: 0x00005EB8
		// (set) Token: 0x06000125 RID: 293 RVA: 0x00007CD2 File Offset: 0x00005ED2
		public IWorkItemDispatcher Dispatcher
		{
			[CompilerGenerated]
			get
			{
				return this.<Dispatcher>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Dispatcher>k__BackingField = value;
			}
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x06000126 RID: 294 RVA: 0x00007CDC File Offset: 0x00005EDC
		// (set) Token: 0x06000127 RID: 295 RVA: 0x00007CF6 File Offset: 0x00005EF6
		public ParallelScope ParallelScope
		{
			[CompilerGenerated]
			get
			{
				return this.<ParallelScope>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ParallelScope>k__BackingField = value;
			}
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x06000128 RID: 296 RVA: 0x00007D00 File Offset: 0x00005F00
		// (set) Token: 0x06000129 RID: 297 RVA: 0x00007D1A File Offset: 0x00005F1A
		public string WorkerId
		{
			[CompilerGenerated]
			get
			{
				return this.<WorkerId>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<WorkerId>k__BackingField = value;
			}
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x0600012A RID: 298 RVA: 0x00007D24 File Offset: 0x00005F24
		// (set) Token: 0x0600012B RID: 299 RVA: 0x00007D3E File Offset: 0x00005F3E
		public Randomizer RandomGenerator
		{
			[CompilerGenerated]
			get
			{
				return this.<RandomGenerator>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<RandomGenerator>k__BackingField = value;
			}
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x0600012C RID: 300 RVA: 0x00007D48 File Offset: 0x00005F48
		// (set) Token: 0x0600012D RID: 301 RVA: 0x00007D62 File Offset: 0x00005F62
		public ValueFormatter CurrentValueFormatter
		{
			[CompilerGenerated]
			get
			{
				return this.<CurrentValueFormatter>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<CurrentValueFormatter>k__BackingField = value;
			}
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x0600012E RID: 302 RVA: 0x00007D6C File Offset: 0x00005F6C
		// (set) Token: 0x0600012F RID: 303 RVA: 0x00007D86 File Offset: 0x00005F86
		public bool IsSingleThreaded
		{
			[CompilerGenerated]
			get
			{
				return this.<IsSingleThreaded>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<IsSingleThreaded>k__BackingField = value;
			}
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x06000130 RID: 304 RVA: 0x00007D90 File Offset: 0x00005F90
		// (set) Token: 0x06000131 RID: 305 RVA: 0x00007DAA File Offset: 0x00005FAA
		public BeforeAfterTestCommandState SetUpTearDownState
		{
			[CompilerGenerated]
			get
			{
				return this.<SetUpTearDownState>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<SetUpTearDownState>k__BackingField = value;
			}
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x06000132 RID: 306 RVA: 0x00007DB4 File Offset: 0x00005FB4
		// (set) Token: 0x06000133 RID: 307 RVA: 0x00007DCE File Offset: 0x00005FCE
		public BeforeAfterTestCommandState OuterUnityTestActionState
		{
			[CompilerGenerated]
			get
			{
				return this.<OuterUnityTestActionState>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<OuterUnityTestActionState>k__BackingField = value;
			}
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x06000134 RID: 308 RVA: 0x00007DD8 File Offset: 0x00005FD8
		internal int AssertCount
		{
			get
			{
				return this._assertCount;
			}
		}

		// Token: 0x06000135 RID: 309 RVA: 0x00007DF3 File Offset: 0x00005FF3
		public void IncrementAssertCount()
		{
			this._assertCount++;
		}

		// Token: 0x06000136 RID: 310 RVA: 0x00007E04 File Offset: 0x00006004
		public void AddFormatter(ValueFormatterFactory formatterFactory)
		{
			throw new NotImplementedException();
		}

		// Token: 0x04000054 RID: 84
		private readonly UnityTestExecutionContext _priorContext;

		// Token: 0x04000055 RID: 85
		private TestResult _currentResult;

		// Token: 0x04000056 RID: 86
		private int _assertCount;

		// Token: 0x04000057 RID: 87
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static UnityTestExecutionContext <CurrentContext>k__BackingField;

		// Token: 0x04000058 RID: 88
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private UnityTestExecutionContext <Context>k__BackingField;

		// Token: 0x04000059 RID: 89
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Test <CurrentTest>k__BackingField;

		// Token: 0x0400005A RID: 90
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private DateTime <StartTime>k__BackingField;

		// Token: 0x0400005B RID: 91
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private long <StartTicks>k__BackingField;

		// Token: 0x0400005C RID: 92
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private object <TestObject>k__BackingField;

		// Token: 0x0400005D RID: 93
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <WorkDirectory>k__BackingField;

		// Token: 0x0400005E RID: 94
		private TestExecutionStatus _executionStatus;

		// Token: 0x0400005F RID: 95
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private List<ITestAction> <UpstreamActions>k__BackingField;

		// Token: 0x04000060 RID: 96
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <TestCaseTimeout>k__BackingField;

		// Token: 0x04000061 RID: 97
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private CultureInfo <CurrentCulture>k__BackingField;

		// Token: 0x04000062 RID: 98
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private CultureInfo <CurrentUICulture>k__BackingField;

		// Token: 0x04000063 RID: 99
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ITestListener <Listener>k__BackingField;

		// Token: 0x04000064 RID: 100
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private TextWriter <OutWriter>k__BackingField;

		// Token: 0x04000065 RID: 101
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <StopOnError>k__BackingField;

		// Token: 0x04000066 RID: 102
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private IWorkItemDispatcher <Dispatcher>k__BackingField;

		// Token: 0x04000067 RID: 103
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ParallelScope <ParallelScope>k__BackingField;

		// Token: 0x04000068 RID: 104
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <WorkerId>k__BackingField;

		// Token: 0x04000069 RID: 105
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Randomizer <RandomGenerator>k__BackingField;

		// Token: 0x0400006A RID: 106
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ValueFormatter <CurrentValueFormatter>k__BackingField;

		// Token: 0x0400006B RID: 107
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <IsSingleThreaded>k__BackingField;

		// Token: 0x0400006C RID: 108
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private BeforeAfterTestCommandState <SetUpTearDownState>k__BackingField;

		// Token: 0x0400006D RID: 109
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private BeforeAfterTestCommandState <OuterUnityTestActionState>k__BackingField;
	}
}
