﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Execution;
using UnityEngine.TestTools.NUnitExtensions;

namespace UnityEngine.TestRunner.NUnitExtensions.Runner
{
	// Token: 0x02000030 RID: 48
	internal class UnityTestAssemblyRunner : IUnityTestAssemblyRunner
	{
		// Token: 0x060000F1 RID: 241 RVA: 0x000076F4 File Offset: 0x000058F4
		public UnityTestAssemblyRunner(UnityTestAssemblyBuilder builder, WorkItemFactory factory)
		{
			this.unityBuilder = builder;
			this.m_Factory = factory;
			this.Context = new UnityTestExecutionContext();
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x060000F2 RID: 242 RVA: 0x00007718 File Offset: 0x00005918
		// (set) Token: 0x060000F3 RID: 243 RVA: 0x00007732 File Offset: 0x00005932
		protected UnityTestExecutionContext Context
		{
			[CompilerGenerated]
			get
			{
				return this.<Context>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Context>k__BackingField = value;
			}
		}

		// Token: 0x060000F4 RID: 244 RVA: 0x0000773C File Offset: 0x0000593C
		public UnityTestExecutionContext GetCurrentContext()
		{
			return UnityTestExecutionContext.CurrentContext;
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x060000F5 RID: 245 RVA: 0x00007758 File Offset: 0x00005958
		// (set) Token: 0x060000F6 RID: 246 RVA: 0x00007772 File Offset: 0x00005972
		protected IDictionary<string, object> Settings
		{
			[CompilerGenerated]
			get
			{
				return this.<Settings>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Settings>k__BackingField = value;
			}
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x060000F7 RID: 247 RVA: 0x0000777C File Offset: 0x0000597C
		// (set) Token: 0x060000F8 RID: 248 RVA: 0x00007796 File Offset: 0x00005996
		public ITest LoadedTest
		{
			[CompilerGenerated]
			get
			{
				return this.<LoadedTest>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<LoadedTest>k__BackingField = value;
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x060000F9 RID: 249 RVA: 0x000077A0 File Offset: 0x000059A0
		public ITestResult Result
		{
			get
			{
				return (this.TopLevelWorkItem != null) ? this.TopLevelWorkItem.Result : null;
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x060000FA RID: 250 RVA: 0x000077D4 File Offset: 0x000059D4
		public bool IsTestLoaded
		{
			get
			{
				return this.LoadedTest != null;
			}
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x060000FB RID: 251 RVA: 0x000077F8 File Offset: 0x000059F8
		public bool IsTestRunning
		{
			get
			{
				return this.TopLevelWorkItem != null && this.TopLevelWorkItem.State == WorkItemState.Running;
			}
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x060000FC RID: 252 RVA: 0x0000782C File Offset: 0x00005A2C
		public bool IsTestComplete
		{
			get
			{
				return this.TopLevelWorkItem != null && this.TopLevelWorkItem.State == WorkItemState.Complete;
			}
		}

		// Token: 0x060000FD RID: 253 RVA: 0x00007860 File Offset: 0x00005A60
		public ITest Load(Assembly[] assemblies, IDictionary<string, object> settings)
		{
			this.Settings = settings;
			if (settings.ContainsKey("RandomSeed"))
			{
				Randomizer.InitialSeed = (int)settings["RandomSeed"];
			}
			ITest test = this.unityBuilder.Build(assemblies, settings);
			this.LoadedTest = test;
			return test;
		}

		// Token: 0x060000FE RID: 254 RVA: 0x000078B8 File Offset: 0x00005AB8
		public IEnumerable Run(ITestListener listener, ITestFilter filter)
		{
			this.TopLevelWorkItem = this.m_Factory.Create(this.LoadedTest, filter);
			this.TopLevelWorkItem.InitializeContext(this.Context);
			UnityTestExecutionContext.CurrentContext = this.Context;
			this.Context.Listener = listener;
			return this.TopLevelWorkItem.Execute();
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x060000FF RID: 255 RVA: 0x00007918 File Offset: 0x00005B18
		// (set) Token: 0x06000100 RID: 256 RVA: 0x00007932 File Offset: 0x00005B32
		public UnityWorkItem TopLevelWorkItem
		{
			[CompilerGenerated]
			get
			{
				return this.<TopLevelWorkItem>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<TopLevelWorkItem>k__BackingField = value;
			}
		}

		// Token: 0x06000101 RID: 257 RVA: 0x0000793B File Offset: 0x00005B3B
		public void StopRun()
		{
			if (this.IsTestRunning)
			{
				this.TopLevelWorkItem.Cancel(false);
			}
		}

		// Token: 0x0400004E RID: 78
		private readonly UnityTestAssemblyBuilder unityBuilder;

		// Token: 0x0400004F RID: 79
		private readonly WorkItemFactory m_Factory;

		// Token: 0x04000050 RID: 80
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private UnityTestExecutionContext <Context>k__BackingField;

		// Token: 0x04000051 RID: 81
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private IDictionary<string, object> <Settings>k__BackingField;

		// Token: 0x04000052 RID: 82
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private ITest <LoadedTest>k__BackingField;

		// Token: 0x04000053 RID: 83
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private UnityWorkItem <TopLevelWorkItem>k__BackingField;
	}
}
