﻿using System;
using System.Collections;
using NUnit.Framework.Internal;

namespace UnityEngine.TestRunner.NUnitExtensions.Runner
{
	// Token: 0x0200002B RID: 43
	internal interface IEnumerableTestMethodCommand
	{
		// Token: 0x060000DF RID: 223
		IEnumerable ExecuteEnumerable(ITestExecutionContext context);
	}
}
