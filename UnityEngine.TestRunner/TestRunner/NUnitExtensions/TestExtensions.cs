﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using UnityEngine.TestRunner.NUnitExtensions.Filters;

namespace UnityEngine.TestRunner.NUnitExtensions
{
	// Token: 0x02000013 RID: 19
	internal static class TestExtensions
	{
		// Token: 0x0600006C RID: 108 RVA: 0x000034B8 File Offset: 0x000016B8
		private static IEnumerable<string> GetTestCategories(this ITest test)
		{
			List<string> list = test.Properties["Category"].Cast<string>().ToList<string>();
			if (list.Count == 0 && test is TestMethod)
			{
				List<string> list2 = test.Parent.Properties["Category"].Cast<string>().ToList<string>();
				if (list2.Count == 0)
				{
					list.Add(CategoryFilterExtended.k_DefaultCategory);
				}
			}
			return list;
		}

		// Token: 0x0600006D RID: 109 RVA: 0x00003538 File Offset: 0x00001738
		public static bool HasCategory(this ITest test, string[] categoryFilter)
		{
			IEnumerable<string> categories = test.GetAllCategoriesFromTest().Distinct<string>();
			return categoryFilter.Any((string c) => categories.Any((string r) => r == c));
		}

		// Token: 0x0600006E RID: 110 RVA: 0x00003578 File Offset: 0x00001778
		public static List<string> GetAllCategoriesFromTest(this ITest test)
		{
			List<string> result;
			if (test.Parent == null)
			{
				result = test.GetTestCategories().ToList<string>();
			}
			else
			{
				List<string> allCategoriesFromTest = test.Parent.GetAllCategoriesFromTest();
				allCategoriesFromTest.AddRange(test.GetTestCategories());
				result = allCategoriesFromTest;
			}
			return result;
		}

		// Token: 0x0600006F RID: 111 RVA: 0x000035C4 File Offset: 0x000017C4
		public static void ParseForNameDuplicates(this ITest test)
		{
			Dictionary<string, int> dictionary = new Dictionary<string, int>();
			for (int i = 0; i < test.Tests.Count; i++)
			{
				ITest test2 = test.Tests[i];
				int num;
				if (dictionary.TryGetValue(test2.FullName, out num))
				{
					num++;
					test2.Properties.Add("childIndex", num);
					dictionary[test2.FullName] = num;
				}
				else
				{
					dictionary.Add(test2.FullName, 1);
				}
				test2.ParseForNameDuplicates();
			}
		}

		// Token: 0x06000070 RID: 112 RVA: 0x0000365C File Offset: 0x0000185C
		public static int GetChildIndex(this ITest test)
		{
			IList list = test.Properties["childIndex"];
			return (int)list[0];
		}

		// Token: 0x06000071 RID: 113 RVA: 0x00003690 File Offset: 0x00001890
		public static bool HasChildIndex(this ITest test)
		{
			IList list = test.Properties["childIndex"];
			return list.Count > 0;
		}

		// Token: 0x0200006D RID: 109
		[CompilerGenerated]
		private sealed class <HasCategory>c__AnonStorey0
		{
			// Token: 0x06000229 RID: 553 RVA: 0x000036BF File Offset: 0x000018BF
			public <HasCategory>c__AnonStorey0()
			{
			}

			// Token: 0x0600022A RID: 554 RVA: 0x000036C8 File Offset: 0x000018C8
			internal bool <>m__0(string c)
			{
				return this.categories.Any((string r) => r == c);
			}

			// Token: 0x0400010B RID: 267
			internal IEnumerable<string> categories;

			// Token: 0x0200006E RID: 110
			private sealed class <HasCategory>c__AnonStorey1
			{
				// Token: 0x0600022B RID: 555 RVA: 0x00003707 File Offset: 0x00001907
				public <HasCategory>c__AnonStorey1()
				{
				}

				// Token: 0x0600022C RID: 556 RVA: 0x00003710 File Offset: 0x00001910
				internal bool <>m__0(string r)
				{
					return r == this.c;
				}

				// Token: 0x0400010C RID: 268
				internal string c;

				// Token: 0x0400010D RID: 269
				internal TestExtensions.<HasCategory>c__AnonStorey0 <>f__ref$0;
			}
		}
	}
}
