﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using NUnit.Framework.Interfaces;

namespace UnityEngine.TestRunner.TestLaunchers
{
	// Token: 0x0200004B RID: 75
	[Serializable]
	internal class RemoteTestData
	{
		// Token: 0x060001B1 RID: 433 RVA: 0x00009B88 File Offset: 0x00007D88
		private RemoteTestData(ITest test)
		{
			this.id = test.Id;
			this.name = test.Name;
			this.fullName = test.FullName;
			this.testCaseCount = test.TestCaseCount;
			this.ChildIndex = -1;
			if (test.Properties["childIndex"].Count > 0)
			{
				this.ChildIndex = (int)test.Properties["childIndex"][0];
			}
			this.hasChildren = test.HasChildren;
			this.isSuite = test.IsSuite;
			this.childrenIds = (from t in test.Tests
			select t.Id).ToArray<string>();
		}

		// Token: 0x060001B2 RID: 434 RVA: 0x00009C5C File Offset: 0x00007E5C
		internal static RemoteTestData[] GetTestDataList(ITest test)
		{
			List<RemoteTestData> list = new List<RemoteTestData>();
			list.Add(new RemoteTestData(test));
			List<RemoteTestData> list2 = list;
			IEnumerable<ITest> tests = test.Tests;
			if (RemoteTestData.<>f__mg$cache0 == null)
			{
				RemoteTestData.<>f__mg$cache0 = new Func<ITest, IEnumerable<RemoteTestData>>(RemoteTestData.GetTestDataList);
			}
			list2.AddRange(tests.SelectMany(RemoteTestData.<>f__mg$cache0));
			return list.ToArray();
		}

		// Token: 0x060001B3 RID: 435 RVA: 0x00009CB8 File Offset: 0x00007EB8
		[CompilerGenerated]
		private static string <RemoteTestData>m__0(ITest t)
		{
			return t.Id;
		}

		// Token: 0x040000BD RID: 189
		public string id;

		// Token: 0x040000BE RID: 190
		public string name;

		// Token: 0x040000BF RID: 191
		public string fullName;

		// Token: 0x040000C0 RID: 192
		public int testCaseCount;

		// Token: 0x040000C1 RID: 193
		public int ChildIndex;

		// Token: 0x040000C2 RID: 194
		public bool hasChildren;

		// Token: 0x040000C3 RID: 195
		public bool isSuite;

		// Token: 0x040000C4 RID: 196
		public string[] childrenIds;

		// Token: 0x040000C5 RID: 197
		public int testCaseTimeout;

		// Token: 0x040000C6 RID: 198
		[CompilerGenerated]
		private static Func<ITest, string> <>f__am$cache0;

		// Token: 0x040000C7 RID: 199
		[CompilerGenerated]
		private static Func<ITest, IEnumerable<RemoteTestData>> <>f__mg$cache0;
	}
}
