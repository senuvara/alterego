﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using NUnit.Framework.Interfaces;

namespace UnityEngine.TestRunner.TestLaunchers
{
	// Token: 0x0200004C RID: 76
	[Serializable]
	internal class RemoteTestResultData
	{
		// Token: 0x060001B4 RID: 436 RVA: 0x00009CD4 File Offset: 0x00007ED4
		private RemoteTestResultData(ITestResult result)
		{
			this.testId = result.Test.Id;
			this.name = result.Name;
			this.fullName = result.FullName;
			this.resultState = result.ResultState.ToString();
			this.testStatus = result.ResultState.Status;
			this.duration = result.Duration;
			this.startTime = result.StartTime;
			this.endTime = result.EndTime;
			this.message = result.Message;
			this.stackTrace = result.StackTrace;
			this.assertCount = result.AssertCount;
			this.failCount = result.FailCount;
			this.passCount = result.PassCount;
			this.skipCount = result.SkipCount;
			this.inconclusiveCount = result.InconclusiveCount;
			this.hasChildren = result.HasChildren;
			this.output = result.Output;
			this.xml = result.ToXml(true).OuterXml;
			this.childrenIds = (from child in result.Children
			select child.Test.Id).ToArray<string>();
		}

		// Token: 0x060001B5 RID: 437 RVA: 0x00009E08 File Offset: 0x00008008
		internal static RemoteTestResultData[] GetTestResultDataList(ITestResult result)
		{
			List<RemoteTestResultData> list = new List<RemoteTestResultData>();
			list.Add(new RemoteTestResultData(result));
			List<RemoteTestResultData> list2 = list;
			IEnumerable<ITestResult> children = result.Children;
			if (RemoteTestResultData.<>f__mg$cache0 == null)
			{
				RemoteTestResultData.<>f__mg$cache0 = new Func<ITestResult, IEnumerable<RemoteTestResultData>>(RemoteTestResultData.GetTestResultDataList);
			}
			list2.AddRange(children.SelectMany(RemoteTestResultData.<>f__mg$cache0));
			return list.ToArray();
		}

		// Token: 0x060001B6 RID: 438 RVA: 0x00009E64 File Offset: 0x00008064
		[CompilerGenerated]
		private static string <RemoteTestResultData>m__0(ITestResult child)
		{
			return child.Test.Id;
		}

		// Token: 0x040000C8 RID: 200
		public string testId;

		// Token: 0x040000C9 RID: 201
		public string name;

		// Token: 0x040000CA RID: 202
		public string fullName;

		// Token: 0x040000CB RID: 203
		public string resultState;

		// Token: 0x040000CC RID: 204
		public TestStatus testStatus;

		// Token: 0x040000CD RID: 205
		public double duration;

		// Token: 0x040000CE RID: 206
		public DateTime startTime;

		// Token: 0x040000CF RID: 207
		public DateTime endTime;

		// Token: 0x040000D0 RID: 208
		public string message;

		// Token: 0x040000D1 RID: 209
		public string stackTrace;

		// Token: 0x040000D2 RID: 210
		public int assertCount;

		// Token: 0x040000D3 RID: 211
		public int failCount;

		// Token: 0x040000D4 RID: 212
		public int passCount;

		// Token: 0x040000D5 RID: 213
		public int skipCount;

		// Token: 0x040000D6 RID: 214
		public int inconclusiveCount;

		// Token: 0x040000D7 RID: 215
		public bool hasChildren;

		// Token: 0x040000D8 RID: 216
		public string output;

		// Token: 0x040000D9 RID: 217
		public string xml;

		// Token: 0x040000DA RID: 218
		public string[] childrenIds;

		// Token: 0x040000DB RID: 219
		[CompilerGenerated]
		private static Func<ITestResult, string> <>f__am$cache0;

		// Token: 0x040000DC RID: 220
		[CompilerGenerated]
		private static Func<ITestResult, IEnumerable<RemoteTestResultData>> <>f__mg$cache0;
	}
}
