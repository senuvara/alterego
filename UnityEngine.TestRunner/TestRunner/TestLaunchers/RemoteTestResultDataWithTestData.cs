﻿using System;
using System.Linq;
using NUnit.Framework.Interfaces;
using UnityEngine.TestRunner.NUnitExtensions.Runner;

namespace UnityEngine.TestRunner.TestLaunchers
{
	// Token: 0x0200004D RID: 77
	[Serializable]
	internal class RemoteTestResultDataWithTestData
	{
		// Token: 0x060001B7 RID: 439 RVA: 0x00009E83 File Offset: 0x00008083
		private RemoteTestResultDataWithTestData()
		{
		}

		// Token: 0x060001B8 RID: 440 RVA: 0x00009E8C File Offset: 0x0000808C
		internal static RemoteTestResultDataWithTestData FromTestResult(ITestResult result)
		{
			RemoteTestData[] testDataList = RemoteTestData.GetTestDataList(result.Test);
			testDataList.First<RemoteTestData>().testCaseTimeout = UnityTestExecutionContext.CurrentContext.TestCaseTimeout;
			return new RemoteTestResultDataWithTestData
			{
				results = RemoteTestResultData.GetTestResultDataList(result),
				tests = testDataList
			};
		}

		// Token: 0x060001B9 RID: 441 RVA: 0x00009EDC File Offset: 0x000080DC
		internal static RemoteTestResultDataWithTestData FromTest(ITest test)
		{
			RemoteTestData[] testDataList = RemoteTestData.GetTestDataList(test);
			testDataList.First<RemoteTestData>().testCaseTimeout = UnityTestExecutionContext.CurrentContext.TestCaseTimeout;
			return new RemoteTestResultDataWithTestData
			{
				tests = testDataList
			};
		}

		// Token: 0x040000DD RID: 221
		public RemoteTestResultData[] results;

		// Token: 0x040000DE RID: 222
		public RemoteTestData[] tests;
	}
}
