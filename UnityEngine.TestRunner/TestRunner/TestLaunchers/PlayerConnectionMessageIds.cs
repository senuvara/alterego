﻿using System;

namespace UnityEngine.TestRunner.TestLaunchers
{
	// Token: 0x0200004A RID: 74
	internal static class PlayerConnectionMessageIds
	{
		// Token: 0x1700005B RID: 91
		// (get) Token: 0x060001AC RID: 428 RVA: 0x00009AE8 File Offset: 0x00007CE8
		public static Guid runStartedMessageId
		{
			get
			{
				return new Guid("6a7f53dd-4672-461d-a7b5-9467e9393fd3");
			}
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x060001AD RID: 429 RVA: 0x00009B08 File Offset: 0x00007D08
		public static Guid testResultMessageId
		{
			get
			{
				return new Guid("83bcdc98-a846-4b8b-bf30-6a3924bd9aa0");
			}
		}

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x060001AE RID: 430 RVA: 0x00009B28 File Offset: 0x00007D28
		public static Guid runFinishedMessageId
		{
			get
			{
				return new Guid("ffb622fc-34ad-4901-8d7b-47fb04b0bdd4");
			}
		}

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x060001AF RID: 431 RVA: 0x00009B48 File Offset: 0x00007D48
		public static Guid testStartedMessageId
		{
			get
			{
				return new Guid("b54d241e-d88d-4dba-8c8f-ee415d11c030");
			}
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x060001B0 RID: 432 RVA: 0x00009B68 File Offset: 0x00007D68
		public static Guid testFinishedMessageId
		{
			get
			{
				return new Guid("72f7b7f4-6829-4cd1-afde-78872b9d5adc");
			}
		}
	}
}
