﻿using System;
using NUnit.Framework;

namespace UnityEngine.TestTools
{
	// Token: 0x02000018 RID: 24
	[AttributeUsage(AttributeTargets.Method)]
	public class UnitySetUpAttribute : NUnitAttribute
	{
		// Token: 0x06000085 RID: 133 RVA: 0x00003CC9 File Offset: 0x00001EC9
		public UnitySetUpAttribute()
		{
		}
	}
}
