﻿using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Commands;
using UnityEngine.TestRunner.NUnitExtensions.Runner;

namespace UnityEngine.TestTools
{
	// Token: 0x0200001E RID: 30
	internal class EnumerableSetUpTearDownCommand : BeforeAfterTestCommandBase<MethodInfo>
	{
		// Token: 0x060000A1 RID: 161 RVA: 0x000047A4 File Offset: 0x000029A4
		public EnumerableSetUpTearDownCommand(TestCommand innerCommand) : base(innerCommand)
		{
			if (base.Test.TypeInfo.Type != null)
			{
				this.BeforeActions = EnumerableSetUpTearDownCommand.GetMethodsWithAttributeFromFixture(base.Test.TypeInfo.Type, typeof(UnitySetUpAttribute));
				this.AfterActions = EnumerableSetUpTearDownCommand.GetMethodsWithAttributeFromFixture(base.Test.TypeInfo.Type, typeof(UnityTearDownAttribute)).Reverse<MethodInfo>().ToArray<MethodInfo>();
			}
		}

		// Token: 0x060000A2 RID: 162 RVA: 0x00004824 File Offset: 0x00002A24
		private static MethodInfo[] GetMethodsWithAttributeFromFixture(Type fixtureType, Type setUpType)
		{
			MethodInfo[] methodsWithAttribute = Reflect.GetMethodsWithAttribute(fixtureType, setUpType, true);
			return (from x in methodsWithAttribute
			where x.ReturnType == typeof(IEnumerator)
			select x).ToArray<MethodInfo>();
		}

		// Token: 0x060000A3 RID: 163 RVA: 0x0000486C File Offset: 0x00002A6C
		protected override IEnumerator InvokeBefore(MethodInfo action, Test test, UnityTestExecutionContext context)
		{
			return (IEnumerator)Reflect.InvokeMethod(action, context.TestObject);
		}

		// Token: 0x060000A4 RID: 164 RVA: 0x00004894 File Offset: 0x00002A94
		protected override IEnumerator InvokeAfter(MethodInfo action, Test test, UnityTestExecutionContext context)
		{
			return (IEnumerator)Reflect.InvokeMethod(action, context.TestObject);
		}

		// Token: 0x060000A5 RID: 165 RVA: 0x000048BC File Offset: 0x00002ABC
		protected override BeforeAfterTestCommandState GetState(UnityTestExecutionContext context)
		{
			return context.SetUpTearDownState;
		}

		// Token: 0x060000A6 RID: 166 RVA: 0x000048D8 File Offset: 0x00002AD8
		[CompilerGenerated]
		private static bool <GetMethodsWithAttributeFromFixture>m__0(MethodInfo x)
		{
			return x.ReturnType == typeof(IEnumerator);
		}

		// Token: 0x0400003F RID: 63
		[CompilerGenerated]
		private static Func<MethodInfo, bool> <>f__am$cache0;
	}
}
