﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.TestTools
{
	// Token: 0x02000056 RID: 86
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
	public class PrebuildSetupAttribute : Attribute
	{
		// Token: 0x060001D8 RID: 472 RVA: 0x0000A723 File Offset: 0x00008923
		public PrebuildSetupAttribute(Type targetClass)
		{
			this.TargetClass = targetClass;
		}

		// Token: 0x060001D9 RID: 473 RVA: 0x0000A733 File Offset: 0x00008933
		public PrebuildSetupAttribute(string targetClassName)
		{
			this.TargetClass = AttributeHelper.GetTargetClassFromName(targetClassName, typeof(IPrebuildSetup));
		}

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x060001DA RID: 474 RVA: 0x0000A754 File Offset: 0x00008954
		// (set) Token: 0x060001DB RID: 475 RVA: 0x0000A76E File Offset: 0x0000896E
		internal Type TargetClass
		{
			[CompilerGenerated]
			get
			{
				return this.<TargetClass>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<TargetClass>k__BackingField = value;
			}
		}

		// Token: 0x040000EF RID: 239
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Type <TargetClass>k__BackingField;
	}
}
