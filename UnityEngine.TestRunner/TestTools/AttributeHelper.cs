﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace UnityEngine.TestTools
{
	// Token: 0x0200004E RID: 78
	internal static class AttributeHelper
	{
		// Token: 0x060001BA RID: 442 RVA: 0x00009F1C File Offset: 0x0000811C
		internal static Type GetTargetClassFromName(string targetClassName, Type attributeInterface)
		{
			Type type = null;
			foreach (string path in ScriptingRuntime.GetAllUserAssemblies())
			{
				string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(path);
				type = Type.GetType(targetClassName + "," + fileNameWithoutExtension);
				if (type != null)
				{
					break;
				}
			}
			Type result;
			if (type == null)
			{
				Debug.LogWarningFormat("Class type not found: " + targetClassName, new object[0]);
				result = null;
			}
			else
			{
				AttributeHelper.ValidateTargetClass(type, attributeInterface);
				result = type;
			}
			return result;
		}

		// Token: 0x060001BB RID: 443 RVA: 0x00009FAC File Offset: 0x000081AC
		private static void ValidateTargetClass(Type targetClass, Type attributeInterface)
		{
			ConstructorInfo[] constructors = targetClass.GetConstructors();
			if (constructors.All((ConstructorInfo constructor) => constructor.GetParameters().Length != 0))
			{
				Debug.LogWarningFormat("{0} does not implement default constructor", new object[]
				{
					targetClass.Name
				});
			}
			if (!attributeInterface.IsAssignableFrom(targetClass))
			{
				Debug.LogWarningFormat("{0} does not implement {1}", new object[]
				{
					targetClass.Name,
					attributeInterface.Name
				});
			}
		}

		// Token: 0x060001BC RID: 444 RVA: 0x0000A034 File Offset: 0x00008234
		[CompilerGenerated]
		private static bool <ValidateTargetClass>m__0(ConstructorInfo constructor)
		{
			return constructor.GetParameters().Length != 0;
		}

		// Token: 0x040000DF RID: 223
		[CompilerGenerated]
		private static Func<ConstructorInfo, bool> <>f__am$cache0;
	}
}
