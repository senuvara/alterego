﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using NUnit.Framework;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Commands;
using UnityEngine.TestRunner.NUnitExtensions.Runner;

namespace UnityEngine.TestTools
{
	// Token: 0x02000022 RID: 34
	internal class TestActionCommand : BeforeAfterTestCommandBase<ITestAction>
	{
		// Token: 0x060000B4 RID: 180 RVA: 0x0000505C File Offset: 0x0000325C
		public TestActionCommand(TestCommand innerCommand) : base(innerCommand)
		{
			if (base.Test.TypeInfo.Type != null)
			{
				if (base.Test.Method.MethodInfo.ReturnType == typeof(IEnumerator))
				{
					this.BeforeActions = TestActionCommand.GetTestActionsFromMethod(base.Test.Method.MethodInfo);
					this.AfterActions = this.BeforeActions;
				}
			}
		}

		// Token: 0x060000B5 RID: 181 RVA: 0x000050D8 File Offset: 0x000032D8
		private static ITestAction[] GetTestActionsFromMethod(MethodInfo method)
		{
			object[] customAttributes = method.GetCustomAttributes(false);
			List<ITestAction> list = new List<ITestAction>();
			foreach (object obj in customAttributes)
			{
				if (obj is ITestAction)
				{
					list.Add(obj as ITestAction);
				}
			}
			return list.ToArray();
		}

		// Token: 0x060000B6 RID: 182 RVA: 0x0000513C File Offset: 0x0000333C
		protected override IEnumerator InvokeBefore(ITestAction action, Test test, UnityTestExecutionContext context)
		{
			action.BeforeTest(test);
			yield return null;
			yield break;
		}

		// Token: 0x060000B7 RID: 183 RVA: 0x00005168 File Offset: 0x00003368
		protected override IEnumerator InvokeAfter(ITestAction action, Test test, UnityTestExecutionContext context)
		{
			action.AfterTest(test);
			yield return null;
			yield break;
		}

		// Token: 0x060000B8 RID: 184 RVA: 0x00005194 File Offset: 0x00003394
		protected override BeforeAfterTestCommandState GetState(UnityTestExecutionContext context)
		{
			return null;
		}

		// Token: 0x02000076 RID: 118
		[CompilerGenerated]
		private sealed class <InvokeBefore>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x0600025A RID: 602 RVA: 0x000051AA File Offset: 0x000033AA
			[DebuggerHidden]
			public <InvokeBefore>c__Iterator0()
			{
			}

			// Token: 0x0600025B RID: 603 RVA: 0x000051B4 File Offset: 0x000033B4
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					action.BeforeTest(test);
					this.$current = null;
					if (!this.$disposing)
					{
						this.$PC = 1;
					}
					return true;
				case 1U:
					this.$PC = -1;
					break;
				}
				return false;
			}

			// Token: 0x1700007F RID: 127
			// (get) Token: 0x0600025C RID: 604 RVA: 0x0000521C File Offset: 0x0000341C
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x17000080 RID: 128
			// (get) Token: 0x0600025D RID: 605 RVA: 0x00005238 File Offset: 0x00003438
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x0600025E RID: 606 RVA: 0x00005252 File Offset: 0x00003452
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x0600025F RID: 607 RVA: 0x00005262 File Offset: 0x00003462
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x0400013E RID: 318
			internal ITestAction action;

			// Token: 0x0400013F RID: 319
			internal Test test;

			// Token: 0x04000140 RID: 320
			internal object $current;

			// Token: 0x04000141 RID: 321
			internal bool $disposing;

			// Token: 0x04000142 RID: 322
			internal int $PC;
		}

		// Token: 0x02000077 RID: 119
		[CompilerGenerated]
		private sealed class <InvokeAfter>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x06000260 RID: 608 RVA: 0x00005269 File Offset: 0x00003469
			[DebuggerHidden]
			public <InvokeAfter>c__Iterator1()
			{
			}

			// Token: 0x06000261 RID: 609 RVA: 0x00005274 File Offset: 0x00003474
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					action.AfterTest(test);
					this.$current = null;
					if (!this.$disposing)
					{
						this.$PC = 1;
					}
					return true;
				case 1U:
					this.$PC = -1;
					break;
				}
				return false;
			}

			// Token: 0x17000081 RID: 129
			// (get) Token: 0x06000262 RID: 610 RVA: 0x000052DC File Offset: 0x000034DC
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x17000082 RID: 130
			// (get) Token: 0x06000263 RID: 611 RVA: 0x000052F8 File Offset: 0x000034F8
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x06000264 RID: 612 RVA: 0x00005312 File Offset: 0x00003512
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x06000265 RID: 613 RVA: 0x00005322 File Offset: 0x00003522
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x04000143 RID: 323
			internal ITestAction action;

			// Token: 0x04000144 RID: 324
			internal Test test;

			// Token: 0x04000145 RID: 325
			internal object $current;

			// Token: 0x04000146 RID: 326
			internal bool $disposing;

			// Token: 0x04000147 RID: 327
			internal int $PC;
		}
	}
}
