﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine.TestRunner.NUnitExtensions.Runner;
using UnityEngine.TestTools.Logging;
using UnityEngine.TestTools.TestRunner;

namespace UnityEngine.TestTools.NUnitExtensions
{
	// Token: 0x0200000F RID: 15
	internal class ActionDelegator : BaseDelegator
	{
		// Token: 0x06000055 RID: 85 RVA: 0x00002FF5 File Offset: 0x000011F5
		public ActionDelegator()
		{
		}

		// Token: 0x06000056 RID: 86 RVA: 0x00003000 File Offset: 0x00001200
		public object Delegate(Action action)
		{
			return this.Delegate(delegate()
			{
				action();
				return null;
			});
		}

		// Token: 0x06000057 RID: 87 RVA: 0x00003034 File Offset: 0x00001234
		public object Delegate(Func<object> action)
		{
			object result;
			if (this.m_Aborted)
			{
				result = null;
			}
			else
			{
				this.AssertState();
				this.m_Context = UnityTestExecutionContext.CurrentContext;
				this.m_Signal.Reset();
				this.m_Action = action;
				base.WaitForSignal();
				result = base.HandleResult();
			}
			return result;
		}

		// Token: 0x06000058 RID: 88 RVA: 0x0000308C File Offset: 0x0000128C
		private void AssertState()
		{
			if (this.m_Action != null)
			{
				throw new Exception("Action not executed yet");
			}
		}

		// Token: 0x06000059 RID: 89 RVA: 0x000030A8 File Offset: 0x000012A8
		public bool HasAction()
		{
			return this.m_Action != null;
		}

		// Token: 0x0600005A RID: 90 RVA: 0x000030CC File Offset: 0x000012CC
		public void Execute(LogScope logScope)
		{
			try
			{
				base.SetCurrentTestContext();
				this.m_Result = this.m_Action();
				if (logScope.AnyFailingLogs())
				{
					LogEvent log = logScope.FailingLogs.First<LogEvent>();
					throw new UnhandledLogMessageException(log);
				}
				if (logScope.ExpectedLogs.Any<LogMatch>())
				{
					throw new UnexpectedLogMessageException(LogScope.Current.ExpectedLogs.Peek());
				}
			}
			catch (Exception exception)
			{
				this.m_Exception = exception;
			}
			finally
			{
				this.m_Action = null;
				this.m_Signal.Set();
			}
		}

		// Token: 0x0400001C RID: 28
		private Func<object> m_Action;

		// Token: 0x0200006C RID: 108
		[CompilerGenerated]
		private sealed class <Delegate>c__AnonStorey0
		{
			// Token: 0x06000227 RID: 551 RVA: 0x00003180 File Offset: 0x00001380
			public <Delegate>c__AnonStorey0()
			{
			}

			// Token: 0x06000228 RID: 552 RVA: 0x00003188 File Offset: 0x00001388
			internal object <>m__0()
			{
				this.action();
				return null;
			}

			// Token: 0x0400010A RID: 266
			internal Action action;
		}
	}
}
