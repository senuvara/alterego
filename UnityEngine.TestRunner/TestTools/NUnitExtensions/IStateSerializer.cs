﻿using System;

namespace UnityEngine.TestTools.NUnitExtensions
{
	// Token: 0x02000012 RID: 18
	internal interface IStateSerializer
	{
		// Token: 0x06000068 RID: 104
		ScriptableObject RestoreScriptableObjectInstance();

		// Token: 0x06000069 RID: 105
		void RestoreClassFromJson(ref object instance);

		// Token: 0x0600006A RID: 106
		bool CanRestoreFromJson(Type requestedType);

		// Token: 0x0600006B RID: 107
		bool CanRestoreFromScriptableObject(Type requestedType);
	}
}
