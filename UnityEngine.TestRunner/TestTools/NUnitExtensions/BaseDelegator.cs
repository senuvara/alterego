﻿using System;
using System.Reflection;
using System.Threading;
using NUnit.Framework.Internal;

namespace UnityEngine.TestTools.NUnitExtensions
{
	// Token: 0x02000010 RID: 16
	internal abstract class BaseDelegator
	{
		// Token: 0x0600005B RID: 91 RVA: 0x00002F19 File Offset: 0x00001119
		protected BaseDelegator()
		{
		}

		// Token: 0x0600005C RID: 92 RVA: 0x00002F30 File Offset: 0x00001130
		protected object HandleResult()
		{
			this.SetCurrentTestContext();
			if (this.m_Exception != null)
			{
				Exception exception = this.m_Exception;
				this.m_Exception = null;
				throw exception;
			}
			object result = this.m_Result;
			this.m_Result = null;
			return result;
		}

		// Token: 0x0600005D RID: 93 RVA: 0x00002F76 File Offset: 0x00001176
		protected void WaitForSignal()
		{
			while (!this.m_Signal.WaitOne(100))
			{
				if (this.m_Aborted)
				{
					this.m_Aborted = false;
					Reflect.MethodCallWrapper = null;
					throw new Exception();
				}
			}
		}

		// Token: 0x0600005E RID: 94 RVA: 0x00002FB1 File Offset: 0x000011B1
		public void Abort()
		{
			this.m_Aborted = true;
		}

		// Token: 0x0600005F RID: 95 RVA: 0x00002FBC File Offset: 0x000011BC
		protected void SetCurrentTestContext()
		{
			PropertyInfo property = typeof(TestExecutionContext).GetProperty("CurrentContext");
			if (property != null)
			{
				property.SetValue(null, this.m_Context, null);
			}
		}

		// Token: 0x0400001D RID: 29
		protected ManualResetEvent m_Signal = new ManualResetEvent(false);

		// Token: 0x0400001E RID: 30
		protected object m_Result;

		// Token: 0x0400001F RID: 31
		protected Exception m_Exception;

		// Token: 0x04000020 RID: 32
		protected ITestExecutionContext m_Context;

		// Token: 0x04000021 RID: 33
		protected bool m_Aborted;
	}
}
