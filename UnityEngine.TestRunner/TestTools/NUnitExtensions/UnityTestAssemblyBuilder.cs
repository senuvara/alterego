﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using NUnit.Framework.Api;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace UnityEngine.TestTools.NUnitExtensions
{
	// Token: 0x02000014 RID: 20
	internal class UnityTestAssemblyBuilder : DefaultTestAssemblyBuilder
	{
		// Token: 0x06000072 RID: 114 RVA: 0x00003730 File Offset: 0x00001930
		public UnityTestAssemblyBuilder()
		{
		}

		// Token: 0x06000073 RID: 115 RVA: 0x00003738 File Offset: 0x00001938
		public ITest Build(Assembly[] assemblies, IDictionary<string, object> options)
		{
			string name = string.Join("_", Application.productName.Split(Path.GetInvalidFileNameChars()));
			TestSuite testSuite = new TestSuite(name);
			foreach (Assembly assembly in assemblies)
			{
				TestSuite testSuite2 = base.Build(assembly, options) as TestSuite;
				if (testSuite2 != null && testSuite2.HasChildren)
				{
					testSuite.Add(testSuite2);
				}
			}
			return testSuite;
		}

		// Token: 0x06000074 RID: 116 RVA: 0x000037C0 File Offset: 0x000019C0
		public static UnityTestAssemblyBuilder GetNUnitTestBuilder(TestPlatform testPlatform)
		{
			return new UnityTestAssemblyBuilder();
		}

		// Token: 0x06000075 RID: 117 RVA: 0x000037DC File Offset: 0x000019DC
		public static Dictionary<string, object> GetNUnitTestBuilderSettings(TestPlatform testPlatform)
		{
			return new Dictionary<string, object>
			{
				{
					"TestParameters",
					"platform=" + testPlatform
				}
			};
		}
	}
}
