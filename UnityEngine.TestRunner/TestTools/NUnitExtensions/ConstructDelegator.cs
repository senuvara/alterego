﻿using System;
using System.Linq;
using System.Reflection;
using NUnit.Framework.Internal;
using UnityEngine.TestRunner.NUnitExtensions.Runner;
using UnityEngine.TestTools.Logging;
using UnityEngine.TestTools.TestRunner;

namespace UnityEngine.TestTools.NUnitExtensions
{
	// Token: 0x02000011 RID: 17
	internal class ConstructDelegator
	{
		// Token: 0x06000060 RID: 96 RVA: 0x000031A9 File Offset: 0x000013A9
		public ConstructDelegator(IStateSerializer stateSerializer)
		{
			this.m_StateSerializer = stateSerializer;
		}

		// Token: 0x06000061 RID: 97 RVA: 0x000031BC File Offset: 0x000013BC
		protected object HandleResult()
		{
			this.SetCurrentTestContext();
			if (this.m_Exception != null)
			{
				Exception exception = this.m_Exception;
				this.m_Exception = null;
				throw exception;
			}
			object result = this.m_Result;
			this.m_Result = null;
			return result;
		}

		// Token: 0x06000062 RID: 98 RVA: 0x00003204 File Offset: 0x00001404
		protected void SetCurrentTestContext()
		{
			PropertyInfo property = typeof(UnityTestExecutionContext).GetProperty("CurrentContext");
			if (property != null)
			{
				property.SetValue(null, this.m_Context, null);
			}
		}

		// Token: 0x06000063 RID: 99 RVA: 0x00003240 File Offset: 0x00001440
		public object Delegate(Type type, object[] arguments)
		{
			this.AssertState();
			this.m_Context = UnityTestExecutionContext.CurrentContext;
			this.m_RequestedType = type;
			this.m_Arguments = arguments;
			using (LogScope logScope = new LogScope())
			{
				this.Execute(logScope);
			}
			return this.HandleResult();
		}

		// Token: 0x06000064 RID: 100 RVA: 0x000032AC File Offset: 0x000014AC
		private void AssertState()
		{
			if (this.m_RequestedType != null)
			{
				throw new Exception("Constructor not executed yet");
			}
		}

		// Token: 0x06000065 RID: 101 RVA: 0x000032C8 File Offset: 0x000014C8
		public bool HasAction()
		{
			return this.m_RequestedType != null;
		}

		// Token: 0x06000066 RID: 102 RVA: 0x000032EC File Offset: 0x000014EC
		public void Execute(LogScope logScope)
		{
			try
			{
				if (typeof(ScriptableObject).IsAssignableFrom(this.m_RequestedType))
				{
					if (this.m_CurrentRunningTest != null && this.m_RequestedType != this.m_CurrentRunningTest.GetType())
					{
						this.DestroyCurrentTestObjectIfExists();
					}
					if (this.m_CurrentRunningTest == null)
					{
						if (this.m_StateSerializer.CanRestoreFromScriptableObject(this.m_RequestedType))
						{
							this.m_CurrentRunningTest = this.m_StateSerializer.RestoreScriptableObjectInstance();
						}
						else
						{
							this.m_CurrentRunningTest = ScriptableObject.CreateInstance(this.m_RequestedType);
						}
					}
					this.m_Result = this.m_CurrentRunningTest;
				}
				else
				{
					this.DestroyCurrentTestObjectIfExists();
					this.m_Result = Activator.CreateInstance(this.m_RequestedType, this.m_Arguments);
					if (this.m_StateSerializer.CanRestoreFromJson(this.m_RequestedType))
					{
						this.m_StateSerializer.RestoreClassFromJson(ref this.m_Result);
					}
				}
				if (logScope.AnyFailingLogs())
				{
					LogEvent log = logScope.FailingLogs.First<LogEvent>();
					throw new UnhandledLogMessageException(log);
				}
				if (logScope.ExpectedLogs.Any<LogMatch>())
				{
					throw new UnexpectedLogMessageException(LogScope.Current.ExpectedLogs.Peek());
				}
			}
			catch (Exception exception)
			{
				this.m_Exception = exception;
			}
			finally
			{
				this.m_RequestedType = null;
				this.m_Arguments = null;
			}
		}

		// Token: 0x06000067 RID: 103 RVA: 0x00003494 File Offset: 0x00001694
		public void DestroyCurrentTestObjectIfExists()
		{
			if (!(this.m_CurrentRunningTest == null))
			{
				Object.DestroyImmediate(this.m_CurrentRunningTest);
			}
		}

		// Token: 0x04000022 RID: 34
		private Type m_RequestedType;

		// Token: 0x04000023 RID: 35
		private object[] m_Arguments;

		// Token: 0x04000024 RID: 36
		private ScriptableObject m_CurrentRunningTest;

		// Token: 0x04000025 RID: 37
		private readonly IStateSerializer m_StateSerializer;

		// Token: 0x04000026 RID: 38
		protected Exception m_Exception;

		// Token: 0x04000027 RID: 39
		protected object m_Result;

		// Token: 0x04000028 RID: 40
		protected ITestExecutionContext m_Context;
	}
}
