﻿using System;
using NUnit.Framework.Interfaces;
using UnityEngine.Events;

namespace UnityEngine.TestTools.TestRunner
{
	// Token: 0x02000039 RID: 57
	[Serializable]
	internal class RunStartedEvent : UnityEvent<ITest>
	{
		// Token: 0x0600015A RID: 346 RVA: 0x00007E38 File Offset: 0x00006038
		public RunStartedEvent()
		{
		}
	}
}
