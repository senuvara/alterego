﻿using System;
using NUnit.Framework.Interfaces;

namespace UnityEngine.TestTools.TestRunner
{
	// Token: 0x02000035 RID: 53
	internal interface ITestRunnerListener
	{
		// Token: 0x06000153 RID: 339
		void RunStarted(ITest testsToRun);

		// Token: 0x06000154 RID: 340
		void RunFinished(ITestResult testResults);

		// Token: 0x06000155 RID: 341
		void TestStarted(ITest test);

		// Token: 0x06000156 RID: 342
		void TestFinished(ITestResult result);
	}
}
