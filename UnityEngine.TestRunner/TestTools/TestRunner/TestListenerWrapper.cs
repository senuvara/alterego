﻿using System;
using NUnit.Framework.Interfaces;

namespace UnityEngine.TestTools.TestRunner
{
	// Token: 0x0200003D RID: 61
	internal class TestListenerWrapper : ITestListener
	{
		// Token: 0x0600016A RID: 362 RVA: 0x000085D0 File Offset: 0x000067D0
		public TestListenerWrapper(TestStartedEvent testStartedEvent, TestFinishedEvent testFinishedEvent)
		{
			this.m_TestStartedEvent = testStartedEvent;
			this.m_TestFinishedEvent = testFinishedEvent;
		}

		// Token: 0x0600016B RID: 363 RVA: 0x000085E7 File Offset: 0x000067E7
		public void TestStarted(ITest test)
		{
			this.m_TestStartedEvent.Invoke(test);
		}

		// Token: 0x0600016C RID: 364 RVA: 0x000085F6 File Offset: 0x000067F6
		public void TestFinished(ITestResult result)
		{
			this.m_TestFinishedEvent.Invoke(result);
		}

		// Token: 0x0600016D RID: 365 RVA: 0x00008605 File Offset: 0x00006805
		public void TestOutput(TestOutput output)
		{
		}

		// Token: 0x04000088 RID: 136
		private readonly TestFinishedEvent m_TestFinishedEvent;

		// Token: 0x04000089 RID: 137
		private readonly TestStartedEvent m_TestStartedEvent;
	}
}
