﻿using System;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using UnityEngine.TestTools.Logging;

namespace UnityEngine.TestTools.TestRunner
{
	// Token: 0x02000008 RID: 8
	internal class UnexpectedLogMessageException : ResultStateException
	{
		// Token: 0x06000012 RID: 18 RVA: 0x00002381 File Offset: 0x00000581
		public UnexpectedLogMessageException(LogMatch log) : base(UnexpectedLogMessageException.BuildMessage(log))
		{
			this.LogEvent = log;
		}

		// Token: 0x06000013 RID: 19 RVA: 0x00002398 File Offset: 0x00000598
		private static string BuildMessage(LogMatch log)
		{
			return string.Format("Expected log did not appear: {0}", log);
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000014 RID: 20 RVA: 0x000023B8 File Offset: 0x000005B8
		public override ResultState ResultState
		{
			get
			{
				return ResultState.Failure;
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000015 RID: 21 RVA: 0x000023D4 File Offset: 0x000005D4
		public override string StackTrace
		{
			get
			{
				return null;
			}
		}

		// Token: 0x04000002 RID: 2
		public LogMatch LogEvent;
	}
}
