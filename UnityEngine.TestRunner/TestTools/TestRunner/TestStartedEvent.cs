﻿using System;
using NUnit.Framework.Interfaces;
using UnityEngine.Events;

namespace UnityEngine.TestTools.TestRunner
{
	// Token: 0x02000037 RID: 55
	[Serializable]
	internal class TestStartedEvent : UnityEvent<ITest>
	{
		// Token: 0x06000158 RID: 344 RVA: 0x00007E28 File Offset: 0x00006028
		public TestStartedEvent()
		{
		}
	}
}
