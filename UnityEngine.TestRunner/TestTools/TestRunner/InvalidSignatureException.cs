﻿using System;
using NUnit.Framework;
using NUnit.Framework.Interfaces;

namespace UnityEngine.TestTools.TestRunner
{
	// Token: 0x02000005 RID: 5
	internal class InvalidSignatureException : ResultStateException
	{
		// Token: 0x06000009 RID: 9 RVA: 0x00002254 File Offset: 0x00000454
		public InvalidSignatureException(string message) : base(message)
		{
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x0600000A RID: 10 RVA: 0x00002260 File Offset: 0x00000460
		public override ResultState ResultState
		{
			get
			{
				return ResultState.NotRunnable;
			}
		}
	}
}
