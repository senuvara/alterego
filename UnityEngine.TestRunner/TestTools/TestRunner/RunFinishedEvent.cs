﻿using System;
using NUnit.Framework.Interfaces;
using UnityEngine.Events;

namespace UnityEngine.TestTools.TestRunner
{
	// Token: 0x02000038 RID: 56
	[Serializable]
	internal class RunFinishedEvent : UnityEvent<ITestResult>
	{
		// Token: 0x06000159 RID: 345 RVA: 0x00007E30 File Offset: 0x00006030
		public RunFinishedEvent()
		{
		}
	}
}
