﻿using System;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using UnityEngine.TestTools.Logging;
using UnityEngine.TestTools.Utils;

namespace UnityEngine.TestTools.TestRunner
{
	// Token: 0x02000009 RID: 9
	internal class UnhandledLogMessageException : ResultStateException
	{
		// Token: 0x06000016 RID: 22 RVA: 0x000023EA File Offset: 0x000005EA
		public UnhandledLogMessageException(LogEvent log) : base(UnhandledLogMessageException.BuildMessage(log))
		{
			this.LogEvent = log;
			this.m_CustomStackTrace = StackTraceFilter.Filter(log.StackTrace);
		}

		// Token: 0x06000017 RID: 23 RVA: 0x00002414 File Offset: 0x00000614
		private static string BuildMessage(LogEvent log)
		{
			return string.Format("Unhandled log message: '{0}'. Use UnityEngine.TestTools.LogAssert.Expect", log);
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000018 RID: 24 RVA: 0x00002434 File Offset: 0x00000634
		public override ResultState ResultState
		{
			get
			{
				return ResultState.Failure;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000019 RID: 25 RVA: 0x00002450 File Offset: 0x00000650
		public override string StackTrace
		{
			get
			{
				return this.m_CustomStackTrace;
			}
		}

		// Token: 0x04000003 RID: 3
		public LogEvent LogEvent;

		// Token: 0x04000004 RID: 4
		private readonly string m_CustomStackTrace;
	}
}
