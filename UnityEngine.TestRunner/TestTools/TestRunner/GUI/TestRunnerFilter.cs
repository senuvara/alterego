﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Filters;
using UnityEngine.TestRunner.NUnitExtensions.Filters;

namespace UnityEngine.TestTools.TestRunner.GUI
{
	// Token: 0x02000040 RID: 64
	[Serializable]
	internal class TestRunnerFilter
	{
		// Token: 0x0600016F RID: 367 RVA: 0x00008624 File Offset: 0x00006824
		public TestRunnerFilter()
		{
		}

		// Token: 0x06000170 RID: 368 RVA: 0x0000862C File Offset: 0x0000682C
		public static string AssemblyNameFromPath(string path)
		{
			string fileName = Path.GetFileName(path);
			string result;
			if (fileName != null && fileName.EndsWith(".dll", StringComparison.OrdinalIgnoreCase))
			{
				result = fileName.Substring(0, fileName.Length - 4);
			}
			else
			{
				result = fileName;
			}
			return result;
		}

		// Token: 0x06000171 RID: 369 RVA: 0x00008678 File Offset: 0x00006878
		private bool CategoryMatches(IEnumerable<string> categories)
		{
			bool result;
			if (this.categoryNames == null || this.categoryNames.Length == 0)
			{
				result = true;
			}
			else
			{
				foreach (string value in categories)
				{
					if (this.categoryNames.Contains(value))
					{
						return true;
					}
				}
				result = false;
			}
			return result;
		}

		// Token: 0x06000172 RID: 370 RVA: 0x00008708 File Offset: 0x00006908
		private bool IDMatchesAssembly(string id)
		{
			bool result;
			if (this.AreOptionalFiltersEmpty())
			{
				result = true;
			}
			else if (this.assemblyNames == null)
			{
				result = false;
			}
			else
			{
				int num = id.IndexOf('[');
				int num2 = id.IndexOf(']');
				if (num >= 0 && num < id.Length && num2 > num && num < id.Length)
				{
					string value = TestRunnerFilter.AssemblyNameFromPath(id.Substring(num + 1, num2 - num - 1));
					foreach (string text in this.assemblyNames)
					{
						if (text.Equals(value, StringComparison.OrdinalIgnoreCase))
						{
							return true;
						}
					}
				}
				result = false;
			}
			return result;
		}

		// Token: 0x06000173 RID: 371 RVA: 0x000087D0 File Offset: 0x000069D0
		private bool NameMatches(string name)
		{
			bool result;
			if (this.AreOptionalFiltersEmpty())
			{
				result = true;
			}
			else if (this.groupNames == null)
			{
				result = false;
			}
			else
			{
				foreach (string text in this.groupNames)
				{
					if (Regex.IsMatch(name, text))
					{
						return true;
					}
					string pattern = text.TrimEnd(new char[]
					{
						'$'
					}) + "[\\.|\\(.*\\)]";
					if (Regex.IsMatch(name, pattern))
					{
						return true;
					}
				}
				result = false;
			}
			return result;
		}

		// Token: 0x06000174 RID: 372 RVA: 0x00008870 File Offset: 0x00006A70
		private bool AreOptionalFiltersEmpty()
		{
			return (this.assemblyNames == null || this.assemblyNames.Length == 0) && (this.groupNames == null || this.groupNames.Length == 0) && (this.testNames == null || this.testNames.Length == 0);
		}

		// Token: 0x06000175 RID: 373 RVA: 0x000088E4 File Offset: 0x00006AE4
		private bool NameMatchesExactly(string name)
		{
			bool result;
			if (this.AreOptionalFiltersEmpty())
			{
				result = true;
			}
			else if (this.testNames == null)
			{
				result = false;
			}
			else
			{
				foreach (string b in this.testNames)
				{
					if (name == b)
					{
						return true;
					}
				}
				result = false;
			}
			return result;
		}

		// Token: 0x06000176 RID: 374 RVA: 0x00008954 File Offset: 0x00006B54
		private static void ClearAncestors(IEnumerable<TestRunnerResult> newResultList, string parentID)
		{
			if (!string.IsNullOrEmpty(parentID))
			{
				foreach (TestRunnerResult testRunnerResult in newResultList)
				{
					if (testRunnerResult.id == parentID)
					{
						testRunnerResult.Clear();
						TestRunnerFilter.ClearAncestors(newResultList, testRunnerResult.parentID);
						break;
					}
				}
			}
		}

		// Token: 0x06000177 RID: 375 RVA: 0x000089E0 File Offset: 0x00006BE0
		public void ClearResults(List<TestRunnerResult> newResultList)
		{
			foreach (TestRunnerResult testRunnerResult in newResultList)
			{
				if (!testRunnerResult.isSuite && this.CategoryMatches(testRunnerResult.categories))
				{
					if (this.IDMatchesAssembly(testRunnerResult.id) || this.NameMatches(testRunnerResult.fullName) || this.NameMatchesExactly(testRunnerResult.fullName))
					{
						testRunnerResult.Clear();
						TestRunnerFilter.ClearAncestors(newResultList, testRunnerResult.parentID);
					}
				}
			}
		}

		// Token: 0x06000178 RID: 376 RVA: 0x00008A9C File Offset: 0x00006C9C
		public ITestFilter BuildNUnitFilter()
		{
			TestFilter testFilter = TestFilter.Empty;
			List<ITestFilter> list = new List<ITestFilter>();
			if (this.testNames != null && this.testNames.Length != 0)
			{
				OrFilter item = new OrFilter((from n in this.testNames
				select new FullNameFilter(n)).ToArray<FullNameFilter>());
				list.Add(item);
			}
			if (this.groupNames != null && this.groupNames.Length != 0)
			{
				OrFilter item2 = new OrFilter((from n in this.groupNames
				select new FullNameFilter(n)
				{
					IsRegex = true
				}).ToArray<FullNameFilter>());
				list.Add(item2);
			}
			if (this.assemblyNames != null && this.assemblyNames.Length != 0)
			{
				OrFilter item3 = new OrFilter((from c in this.assemblyNames
				select new AssemblyNameFilter(c)).ToArray<AssemblyNameFilter>());
				list.Add(item3);
			}
			if (list.Any<ITestFilter>())
			{
				testFilter = new OrFilter(list.ToArray());
			}
			if (this.categoryNames != null && this.categoryNames.Length != 0)
			{
				OrFilter orFilter = new OrFilter((from c in this.categoryNames
				select new CategoryFilterExtended(c)
				{
					IsRegex = true
				}).ToArray<CategoryFilterExtended>());
				testFilter = new AndFilter(new ITestFilter[]
				{
					orFilter,
					testFilter
				});
			}
			if (this.testsToSkip != null && this.testsToSkip.Any<string>())
			{
				AndFilter andFilter = new AndFilter((from t in this.testsToSkip
				select new NotFilter(new FullNameFilter(t))).ToArray<NotFilter>());
				testFilter = new AndFilter(new ITestFilter[]
				{
					andFilter,
					testFilter
				});
			}
			return testFilter;
		}

		// Token: 0x06000179 RID: 377 RVA: 0x00008C9E File Offset: 0x00006E9E
		// Note: this type is marked as 'beforefieldinit'.
		static TestRunnerFilter()
		{
		}

		// Token: 0x0600017A RID: 378 RVA: 0x00008CAC File Offset: 0x00006EAC
		[CompilerGenerated]
		private static FullNameFilter <BuildNUnitFilter>m__0(string n)
		{
			return new FullNameFilter(n);
		}

		// Token: 0x0600017B RID: 379 RVA: 0x00008CC8 File Offset: 0x00006EC8
		[CompilerGenerated]
		private static FullNameFilter <BuildNUnitFilter>m__1(string n)
		{
			return new FullNameFilter(n)
			{
				IsRegex = true
			};
		}

		// Token: 0x0600017C RID: 380 RVA: 0x00008CEC File Offset: 0x00006EEC
		[CompilerGenerated]
		private static AssemblyNameFilter <BuildNUnitFilter>m__2(string c)
		{
			return new AssemblyNameFilter(c);
		}

		// Token: 0x0600017D RID: 381 RVA: 0x00008D08 File Offset: 0x00006F08
		[CompilerGenerated]
		private static CategoryFilterExtended <BuildNUnitFilter>m__3(string c)
		{
			return new CategoryFilterExtended(c)
			{
				IsRegex = true
			};
		}

		// Token: 0x0600017E RID: 382 RVA: 0x00008D2C File Offset: 0x00006F2C
		[CompilerGenerated]
		private static NotFilter <BuildNUnitFilter>m__4(string t)
		{
			return new NotFilter(new FullNameFilter(t));
		}

		// Token: 0x0400008E RID: 142
		public string[] assemblyNames;

		// Token: 0x0400008F RID: 143
		public string[] groupNames;

		// Token: 0x04000090 RID: 144
		public string[] categoryNames;

		// Token: 0x04000091 RID: 145
		public static TestRunnerFilter empty = new TestRunnerFilter();

		// Token: 0x04000092 RID: 146
		public string[] testNames;

		// Token: 0x04000093 RID: 147
		public string[] testsToSkip;

		// Token: 0x04000094 RID: 148
		[CompilerGenerated]
		private static Func<string, FullNameFilter> <>f__am$cache0;

		// Token: 0x04000095 RID: 149
		[CompilerGenerated]
		private static Func<string, FullNameFilter> <>f__am$cache1;

		// Token: 0x04000096 RID: 150
		[CompilerGenerated]
		private static Func<string, AssemblyNameFilter> <>f__am$cache2;

		// Token: 0x04000097 RID: 151
		[CompilerGenerated]
		private static Func<string, CategoryFilterExtended> <>f__am$cache3;

		// Token: 0x04000098 RID: 152
		[CompilerGenerated]
		private static Func<string, NotFilter> <>f__am$cache4;
	}
}
