﻿using System;
using System.Collections.Generic;
using NUnit.Framework.Interfaces;
using UnityEngine.TestRunner.NUnitExtensions;

namespace UnityEngine.TestTools.TestRunner.GUI
{
	// Token: 0x02000041 RID: 65
	[Serializable]
	internal class TestRunnerResult
	{
		// Token: 0x0600017F RID: 383 RVA: 0x00008D4C File Offset: 0x00006F4C
		internal TestRunnerResult(ITest test)
		{
			this.id = TestRunnerResult.GetId(test);
			this.fullName = test.FullName;
			this.name = test.Name;
			this.description = (string)test.Properties.Get("Description");
			this.isSuite = test.IsSuite;
			this.ignoredOrSkipped = (test.RunState == RunState.Ignored || test.RunState == RunState.Skipped);
			this.notRunnable = (test.RunState == RunState.NotRunnable);
			if (this.ignoredOrSkipped)
			{
				if (test.Properties.ContainsKey("_SKIPREASON"))
				{
					this.messages = (string)test.Properties.Get("_SKIPREASON");
				}
			}
			if (this.notRunnable)
			{
				this.resultStatus = TestRunnerResult.ResultStatus.Failed;
				if (test.Properties.ContainsKey("_SKIPREASON"))
				{
					this.messages = (string)test.Properties.Get("_SKIPREASON");
				}
			}
			this.categories = test.GetAllCategoriesFromTest();
			if (test.Parent != null)
			{
				this.parentID = TestRunnerResult.GetId(test.Parent);
			}
		}

		// Token: 0x06000180 RID: 384 RVA: 0x00008E88 File Offset: 0x00007088
		internal TestRunnerResult(ITestResult testResult) : this(testResult.Test)
		{
			this.notOutdated = true;
			this.messages = testResult.Message;
			this.output = testResult.Output;
			this.stacktrace = testResult.StackTrace;
			this.duration = (float)testResult.Duration;
			if (testResult.Test.IsSuite && testResult.ResultState == ResultState.Ignored)
			{
				this.resultStatus = TestRunnerResult.ResultStatus.Passed;
			}
			else
			{
				this.resultStatus = TestRunnerResult.ParseNUnitResultStatus(testResult.ResultState.Status);
			}
		}

		// Token: 0x06000181 RID: 385 RVA: 0x00008F20 File Offset: 0x00007120
		public static string GetId(ITest test)
		{
			string text = TestRunnerResult.GetFullName(test);
			if (test.HasChildIndex())
			{
				int childIndex = test.GetChildIndex();
				if (childIndex >= 0)
				{
					text += childIndex;
				}
			}
			if (test.IsSuite)
			{
				text += "[suite]";
			}
			return text;
		}

		// Token: 0x06000182 RID: 386 RVA: 0x00008F80 File Offset: 0x00007180
		private static string GetFullName(ITest test)
		{
			string result;
			if (test.TypeInfo == null && (test.Parent == null || test.Parent.TypeInfo == null))
			{
				result = "[" + test.FullName + "]";
			}
			else
			{
				string arg = (test.TypeInfo != null) ? test.TypeInfo.Assembly.GetName().Name : test.Parent.TypeInfo.Assembly.GetName().Name;
				result = string.Format("[{0}][{1}]", arg, test.FullName);
			}
			return result;
		}

		// Token: 0x06000183 RID: 387 RVA: 0x00009028 File Offset: 0x00007228
		public void Update(TestRunnerResult result)
		{
			if (!object.ReferenceEquals(result, null))
			{
				this.resultStatus = result.resultStatus;
				this.duration = result.duration;
				this.messages = result.messages;
				this.output = result.output;
				this.stacktrace = result.stacktrace;
				this.ignoredOrSkipped = result.ignoredOrSkipped;
				this.notRunnable = result.notRunnable;
				this.description = result.description;
				this.notOutdated = result.notOutdated;
				if (this.m_OnResultUpdate != null)
				{
					this.m_OnResultUpdate(this);
				}
			}
		}

		// Token: 0x06000184 RID: 388 RVA: 0x000090CA File Offset: 0x000072CA
		public void SetResultChangedCallback(Action<TestRunnerResult> resultUpdated)
		{
			this.m_OnResultUpdate = resultUpdated;
		}

		// Token: 0x06000185 RID: 389 RVA: 0x000090D4 File Offset: 0x000072D4
		private static TestRunnerResult.ResultStatus ParseNUnitResultStatus(TestStatus status)
		{
			TestRunnerResult.ResultStatus result;
			switch (status)
			{
			case TestStatus.Inconclusive:
				result = TestRunnerResult.ResultStatus.Inconclusive;
				break;
			case TestStatus.Skipped:
				result = TestRunnerResult.ResultStatus.Skipped;
				break;
			case TestStatus.Passed:
				result = TestRunnerResult.ResultStatus.Passed;
				break;
			case TestStatus.Failed:
				result = TestRunnerResult.ResultStatus.Failed;
				break;
			default:
				result = TestRunnerResult.ResultStatus.NotRun;
				break;
			}
			return result;
		}

		// Token: 0x06000186 RID: 390 RVA: 0x00009124 File Offset: 0x00007324
		public override string ToString()
		{
			return string.Format("{0} ({1})", this.name, this.fullName);
		}

		// Token: 0x06000187 RID: 391 RVA: 0x0000914F File Offset: 0x0000734F
		public void Clear()
		{
			this.resultStatus = TestRunnerResult.ResultStatus.NotRun;
			if (this.m_OnResultUpdate != null)
			{
				this.m_OnResultUpdate(this);
			}
		}

		// Token: 0x04000099 RID: 153
		public string id;

		// Token: 0x0400009A RID: 154
		public string name;

		// Token: 0x0400009B RID: 155
		public string fullName;

		// Token: 0x0400009C RID: 156
		public TestRunnerResult.ResultStatus resultStatus = TestRunnerResult.ResultStatus.NotRun;

		// Token: 0x0400009D RID: 157
		public float duration;

		// Token: 0x0400009E RID: 158
		public string messages;

		// Token: 0x0400009F RID: 159
		public string output;

		// Token: 0x040000A0 RID: 160
		public string stacktrace;

		// Token: 0x040000A1 RID: 161
		public bool notRunnable;

		// Token: 0x040000A2 RID: 162
		public bool ignoredOrSkipped;

		// Token: 0x040000A3 RID: 163
		public string description;

		// Token: 0x040000A4 RID: 164
		public bool isSuite;

		// Token: 0x040000A5 RID: 165
		public List<string> categories;

		// Token: 0x040000A6 RID: 166
		public string parentID;

		// Token: 0x040000A7 RID: 167
		[NonSerialized]
		public bool notOutdated;

		// Token: 0x040000A8 RID: 168
		protected Action<TestRunnerResult> m_OnResultUpdate;

		// Token: 0x02000042 RID: 66
		[Serializable]
		internal enum ResultStatus
		{
			// Token: 0x040000AA RID: 170
			NotRun,
			// Token: 0x040000AB RID: 171
			Passed,
			// Token: 0x040000AC RID: 172
			Failed,
			// Token: 0x040000AD RID: 173
			Inconclusive,
			// Token: 0x040000AE RID: 174
			Skipped
		}
	}
}
