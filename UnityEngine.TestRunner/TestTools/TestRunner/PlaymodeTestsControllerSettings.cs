﻿using System;
using UnityEngine.TestTools.TestRunner.GUI;

namespace UnityEngine.TestTools.TestRunner
{
	// Token: 0x0200003B RID: 59
	[Serializable]
	internal class PlaymodeTestsControllerSettings
	{
		// Token: 0x06000165 RID: 357 RVA: 0x0000841A File Offset: 0x0000661A
		public PlaymodeTestsControllerSettings()
		{
		}

		// Token: 0x06000166 RID: 358 RVA: 0x00008424 File Offset: 0x00006624
		public static PlaymodeTestsControllerSettings CreateRunnerSettings(TestRunnerFilter filter)
		{
			return new PlaymodeTestsControllerSettings
			{
				filter = filter,
				sceneBased = false,
				originalScene = null,
				bootstrapScene = null
			};
		}

		// Token: 0x04000083 RID: 131
		[SerializeField]
		public TestRunnerFilter filter;

		// Token: 0x04000084 RID: 132
		public bool sceneBased;

		// Token: 0x04000085 RID: 133
		public string originalScene;

		// Token: 0x04000086 RID: 134
		public string bootstrapScene;
	}
}
