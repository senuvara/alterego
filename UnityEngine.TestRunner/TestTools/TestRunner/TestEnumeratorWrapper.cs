﻿using System;
using System.Collections;
using System.Reflection;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace UnityEngine.TestTools.TestRunner
{
	// Token: 0x0200003C RID: 60
	internal class TestEnumeratorWrapper
	{
		// Token: 0x06000167 RID: 359 RVA: 0x0000845E File Offset: 0x0000665E
		public TestEnumeratorWrapper(TestMethod testMethod)
		{
			this.m_TestMethod = testMethod;
		}

		// Token: 0x06000168 RID: 360 RVA: 0x00008470 File Offset: 0x00006670
		public IEnumerator GetEnumerator(ITestExecutionContext context)
		{
			if (this.m_TestMethod.Method.ReturnType.Type == typeof(IEnumerator))
			{
				return this.HandleEnumerableTest(context);
			}
			string text = string.Format("Return type {0} of {1} in {2} is not supported.", this.m_TestMethod.Method.ReturnType, this.m_TestMethod.Method.Name, this.m_TestMethod.Method.TypeInfo.FullName);
			if (this.m_TestMethod.Method.ReturnType.Type == typeof(IEnumerable))
			{
				text += "\nDid you mean IEnumerator?";
			}
			throw new InvalidSignatureException(text);
		}

		// Token: 0x06000169 RID: 361 RVA: 0x0000852C File Offset: 0x0000672C
		private IEnumerator HandleEnumerableTest(ITestExecutionContext context)
		{
			IEnumerator result;
			try
			{
				result = (this.m_TestMethod.Method.MethodInfo.Invoke(context.TestObject, (this.m_TestMethod.parms == null) ? null : this.m_TestMethod.parms.OriginalArguments) as IEnumerator);
			}
			catch (TargetInvocationException ex)
			{
				if (!(ex.InnerException is IgnoreException))
				{
					throw;
				}
				context.CurrentResult.SetResult(ResultState.Ignored, ex.InnerException.Message);
				result = null;
			}
			return result;
		}

		// Token: 0x04000087 RID: 135
		private readonly TestMethod m_TestMethod;
	}
}
