﻿using System;
using NUnit.Framework;
using NUnit.Framework.Interfaces;

namespace UnityEngine.TestTools.TestRunner
{
	// Token: 0x0200000A RID: 10
	internal class UnityTestTimeoutException : ResultStateException
	{
		// Token: 0x0600001A RID: 26 RVA: 0x0000246B File Offset: 0x0000066B
		public UnityTestTimeoutException(int timeout) : base(UnityTestTimeoutException.BuildMessage(timeout))
		{
		}

		// Token: 0x0600001B RID: 27 RVA: 0x0000247C File Offset: 0x0000067C
		private static string BuildMessage(int timeout)
		{
			return string.Format("UnityTest exceeded Timeout value of {0}ms", timeout);
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x0600001C RID: 28 RVA: 0x000024A4 File Offset: 0x000006A4
		public override ResultState ResultState
		{
			get
			{
				return ResultState.Failure;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x0600001D RID: 29 RVA: 0x000024C0 File Offset: 0x000006C0
		public override string StackTrace
		{
			get
			{
				return "";
			}
		}
	}
}
