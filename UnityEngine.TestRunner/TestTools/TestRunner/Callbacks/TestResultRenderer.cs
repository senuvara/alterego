﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace UnityEngine.TestTools.TestRunner.Callbacks
{
	// Token: 0x02000046 RID: 70
	internal class TestResultRenderer
	{
		// Token: 0x0600019D RID: 413 RVA: 0x00009788 File Offset: 0x00007988
		public TestResultRenderer(ITestResult testResults)
		{
			this.m_FailedTestCollection = new List<ITestResult>();
			this.GetFailedTests(testResults);
		}

		// Token: 0x0600019E RID: 414 RVA: 0x000097A4 File Offset: 0x000079A4
		private void GetFailedTests(ITestResult testResults)
		{
			if (testResults is TestCaseResult)
			{
				if (testResults.ResultState.Status == TestStatus.Failed)
				{
					this.m_FailedTestCollection.Add(testResults);
				}
			}
			else if (testResults.HasChildren)
			{
				foreach (ITestResult testResults2 in testResults.Children)
				{
					this.GetFailedTests(testResults2);
				}
			}
		}

		// Token: 0x0600019F RID: 415 RVA: 0x00009840 File Offset: 0x00007A40
		public void ShowResults()
		{
			this.m_ShowResults = true;
			Cursor.visible = true;
		}

		// Token: 0x060001A0 RID: 416 RVA: 0x00009850 File Offset: 0x00007A50
		public void Draw()
		{
			if (this.m_ShowResults)
			{
				if (this.m_FailedTestCollection.Count == 0)
				{
					GUILayout.Label("All test succeeded", TestResultRenderer.Styles.SucceedLabelStyle, new GUILayoutOption[]
					{
						GUILayout.Width(600f)
					});
				}
				else
				{
					int count = this.m_FailedTestCollection.Count;
					GUILayout.Label(count + " tests failed!", TestResultRenderer.Styles.FailedLabelStyle, new GUILayoutOption[0]);
					this.m_ScrollPosition = GUILayout.BeginScrollView(this.m_ScrollPosition, new GUILayoutOption[]
					{
						GUILayout.ExpandWidth(true)
					});
					string text = "";
					text += "<b><size=18>Code-based tests</size></b>\n";
					text += string.Join("\n", (from result in this.m_FailedTestCollection
					select string.Concat(new object[]
					{
						result.Name,
						" ",
						result.ResultState,
						"\n",
						result.Message
					})).ToArray<string>());
					if (text.Length > 15000)
					{
						text = text.Substring(0, 15000);
					}
					GUILayout.TextArea(text, TestResultRenderer.Styles.FailedMessagesStyle, new GUILayoutOption[0]);
					GUILayout.EndScrollView();
				}
				if (GUILayout.Button("Close", new GUILayoutOption[0]))
				{
					Application.Quit();
				}
			}
		}

		// Token: 0x060001A1 RID: 417 RVA: 0x00009994 File Offset: 0x00007B94
		[CompilerGenerated]
		private static string <Draw>m__0(ITestResult result)
		{
			return string.Concat(new object[]
			{
				result.Name,
				" ",
				result.ResultState,
				"\n",
				result.Message
			});
		}

		// Token: 0x040000B4 RID: 180
		private readonly List<ITestResult> m_FailedTestCollection;

		// Token: 0x040000B5 RID: 181
		private bool m_ShowResults;

		// Token: 0x040000B6 RID: 182
		private Vector2 m_ScrollPosition;

		// Token: 0x040000B7 RID: 183
		private const int k_MaxStringLength = 15000;

		// Token: 0x040000B8 RID: 184
		[CompilerGenerated]
		private static Func<ITestResult, string> <>f__am$cache0;

		// Token: 0x02000047 RID: 71
		private static class Styles
		{
			// Token: 0x060001A2 RID: 418 RVA: 0x000099E0 File Offset: 0x00007BE0
			static Styles()
			{
				TestResultRenderer.Styles.SucceedLabelStyle.normal.textColor = Color.green;
				TestResultRenderer.Styles.SucceedLabelStyle.fontSize = 48;
				TestResultRenderer.Styles.FailedLabelStyle = new GUIStyle("label");
				TestResultRenderer.Styles.FailedLabelStyle.normal.textColor = Color.red;
				TestResultRenderer.Styles.FailedLabelStyle.fontSize = 32;
				TestResultRenderer.Styles.FailedMessagesStyle = new GUIStyle("label");
				TestResultRenderer.Styles.FailedMessagesStyle.wordWrap = false;
				TestResultRenderer.Styles.FailedMessagesStyle.richText = true;
			}

			// Token: 0x040000B9 RID: 185
			public static readonly GUIStyle SucceedLabelStyle = new GUIStyle("label");

			// Token: 0x040000BA RID: 186
			public static readonly GUIStyle FailedLabelStyle;

			// Token: 0x040000BB RID: 187
			public static readonly GUIStyle FailedMessagesStyle;
		}
	}
}
