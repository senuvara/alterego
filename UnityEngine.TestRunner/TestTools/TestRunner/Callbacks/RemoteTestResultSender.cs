﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using NUnit.Framework.Interfaces;
using UnityEngine.Events;
using UnityEngine.Networking.PlayerConnection;
using UnityEngine.TestRunner.TestLaunchers;
using UnityEngine.TestTools.TestRunner.GUI;

namespace UnityEngine.TestTools.TestRunner.Callbacks
{
	// Token: 0x02000044 RID: 68
	[AddComponentMenu("")]
	internal class RemoteTestResultSender : MonoBehaviour, ITestRunnerListener
	{
		// Token: 0x0600018F RID: 399 RVA: 0x00009217 File Offset: 0x00007417
		public RemoteTestResultSender()
		{
		}

		// Token: 0x06000190 RID: 400 RVA: 0x00009235 File Offset: 0x00007435
		public void Start()
		{
			PlayerConnection.instance.Register(PlayerConnectionMessageIds.runFinishedMessageId, new UnityAction<MessageEventArgs>(this.EditorProccessedTheResult));
			base.StartCoroutine(this.SendDataRoutine());
		}

		// Token: 0x06000191 RID: 401 RVA: 0x00009260 File Offset: 0x00007460
		private void EditorProccessedTheResult(MessageEventArgs arg0)
		{
			if (arg0.data == null)
			{
				PlayerConnection.instance.DisconnectAll();
				if (Application.platform != RuntimePlatform.XboxOne)
				{
					Application.Quit();
				}
			}
		}

		// Token: 0x06000192 RID: 402 RVA: 0x00009298 File Offset: 0x00007498
		private byte[] SerializeObject(object objectToSerialize)
		{
			return Encoding.UTF8.GetBytes(JsonUtility.ToJson(objectToSerialize));
		}

		// Token: 0x06000193 RID: 403 RVA: 0x000092C0 File Offset: 0x000074C0
		public void RunStarted(ITest testsToRun)
		{
			byte[] data = this.SerializeObject(RemoteTestResultDataWithTestData.FromTest(testsToRun));
			object lockQueue = this.m_LockQueue;
			lock (lockQueue)
			{
				this.m_SendQueue.Enqueue(new RemoteTestResultSender.QueueData
				{
					id = PlayerConnectionMessageIds.runStartedMessageId,
					data = data
				});
			}
		}

		// Token: 0x06000194 RID: 404 RVA: 0x0000932C File Offset: 0x0000752C
		public void RunFinished(ITestResult testResults)
		{
			byte[] data = this.SerializeObject(RemoteTestResultDataWithTestData.FromTestResult(testResults));
			object lockQueue = this.m_LockQueue;
			lock (lockQueue)
			{
				this.m_SendQueue.Enqueue(new RemoteTestResultSender.QueueData
				{
					id = PlayerConnectionMessageIds.runFinishedMessageId,
					data = data
				});
			}
		}

		// Token: 0x06000195 RID: 405 RVA: 0x00009398 File Offset: 0x00007598
		public void TestStarted(ITest test)
		{
			byte[] data = this.SerializeObject(RemoteTestResultDataWithTestData.FromTest(test));
			object lockQueue = this.m_LockQueue;
			lock (lockQueue)
			{
				this.m_SendQueue.Enqueue(new RemoteTestResultSender.QueueData
				{
					id = PlayerConnectionMessageIds.testStartedMessageId,
					data = data
				});
			}
		}

		// Token: 0x06000196 RID: 406 RVA: 0x00009404 File Offset: 0x00007604
		public void TestFinished(ITestResult result)
		{
			TestRunnerResult objectToSerialize = new TestRunnerResult(result);
			byte[] data = this.SerializeObject(objectToSerialize);
			object lockQueue = this.m_LockQueue;
			lock (lockQueue)
			{
				this.m_SendQueue.Enqueue(new RemoteTestResultSender.QueueData
				{
					id = PlayerConnectionMessageIds.testResultMessageId,
					data = data
				});
			}
			RemoteTestResultDataWithTestData objectToSerialize2 = RemoteTestResultDataWithTestData.FromTestResult(result);
			byte[] data2 = this.SerializeObject(objectToSerialize2);
			object lockQueue2 = this.m_LockQueue;
			lock (lockQueue2)
			{
				this.m_SendQueue.Enqueue(new RemoteTestResultSender.QueueData
				{
					id = PlayerConnectionMessageIds.testFinishedMessageId,
					data = data2
				});
			}
		}

		// Token: 0x06000197 RID: 407 RVA: 0x000094D4 File Offset: 0x000076D4
		public IEnumerator SendDataRoutine()
		{
			while (!PlayerConnection.instance.isConnected)
			{
				yield return new WaitForSeconds(1f);
			}
			for (;;)
			{
				object lockQueue = this.m_LockQueue;
				lock (lockQueue)
				{
					if (PlayerConnection.instance.isConnected && this.m_SendQueue.Count > 0)
					{
						RemoteTestResultSender.QueueData queueData = this.m_SendQueue.Dequeue();
						PlayerConnection.instance.Send(queueData.id, queueData.data);
						yield return null;
					}
					if (!this.m_SendQueue.Any<RemoteTestResultSender.QueueData>())
					{
						yield return new WaitForSeconds(0.02f);
					}
				}
			}
			yield break;
		}

		// Token: 0x040000B0 RID: 176
		private readonly Queue<RemoteTestResultSender.QueueData> m_SendQueue = new Queue<RemoteTestResultSender.QueueData>();

		// Token: 0x040000B1 RID: 177
		private readonly object m_LockQueue = new object();

		// Token: 0x02000045 RID: 69
		private class QueueData
		{
			// Token: 0x06000198 RID: 408 RVA: 0x000094F6 File Offset: 0x000076F6
			public QueueData()
			{
			}

			// Token: 0x17000057 RID: 87
			// (get) Token: 0x06000199 RID: 409 RVA: 0x00009500 File Offset: 0x00007700
			// (set) Token: 0x0600019A RID: 410 RVA: 0x0000951A File Offset: 0x0000771A
			public Guid id
			{
				[CompilerGenerated]
				get
				{
					return this.<id>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<id>k__BackingField = value;
				}
			}

			// Token: 0x17000058 RID: 88
			// (get) Token: 0x0600019B RID: 411 RVA: 0x00009524 File Offset: 0x00007724
			// (set) Token: 0x0600019C RID: 412 RVA: 0x0000953E File Offset: 0x0000773E
			public byte[] data
			{
				[CompilerGenerated]
				get
				{
					return this.<data>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<data>k__BackingField = value;
				}
			}

			// Token: 0x040000B2 RID: 178
			[CompilerGenerated]
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			private Guid <id>k__BackingField;

			// Token: 0x040000B3 RID: 179
			[CompilerGenerated]
			[DebuggerBrowsable(DebuggerBrowsableState.Never)]
			private byte[] <data>k__BackingField;
		}

		// Token: 0x02000080 RID: 128
		[CompilerGenerated]
		private sealed class <SendDataRoutine>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x060002A3 RID: 675 RVA: 0x00009547 File Offset: 0x00007747
			[DebuggerHidden]
			public <SendDataRoutine>c__Iterator0()
			{
			}

			// Token: 0x060002A4 RID: 676 RVA: 0x00009550 File Offset: 0x00007750
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				bool flag = false;
				switch (num)
				{
				case 0U:
					break;
				case 1U:
					break;
				case 2U:
				case 3U:
					Block_3:
					try
					{
						switch (num)
						{
						case 2U:
							break;
						case 3U:
							goto IL_14F;
						default:
							if (PlayerConnection.instance.isConnected && this.m_SendQueue.Count > 0)
							{
								queueData = this.m_SendQueue.Dequeue();
								PlayerConnection.instance.Send(queueData.id, queueData.data);
								this.$current = null;
								if (!this.$disposing)
								{
									this.$PC = 2;
								}
								flag = true;
								return true;
							}
							break;
						}
						if (!this.m_SendQueue.Any<RemoteTestResultSender.QueueData>())
						{
							this.$current = new WaitForSeconds(0.02f);
							if (!this.$disposing)
							{
								this.$PC = 3;
							}
							flag = true;
							return true;
						}
						IL_14F:;
					}
					finally
					{
						if (!flag)
						{
							this.<>__Finally0();
						}
					}
					goto IL_66;
				default:
					return false;
				}
				if (!PlayerConnection.instance.isConnected)
				{
					this.$current = new WaitForSeconds(1f);
					if (!this.$disposing)
					{
						this.$PC = 1;
					}
					return true;
				}
				IL_66:
				lockQueue = this.m_LockQueue;
				Monitor.Enter(lockQueue);
				num = 4294967293U;
				goto Block_3;
			}

			// Token: 0x17000093 RID: 147
			// (get) Token: 0x060002A5 RID: 677 RVA: 0x000096E0 File Offset: 0x000078E0
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x17000094 RID: 148
			// (get) Token: 0x060002A6 RID: 678 RVA: 0x000096FC File Offset: 0x000078FC
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x060002A7 RID: 679 RVA: 0x00009718 File Offset: 0x00007918
			[DebuggerHidden]
			public void Dispose()
			{
				uint num = (uint)this.$PC;
				this.$disposing = true;
				this.$PC = -1;
				switch (num)
				{
				case 2U:
				case 3U:
					try
					{
					}
					finally
					{
						this.<>__Finally0();
					}
					break;
				}
			}

			// Token: 0x060002A8 RID: 680 RVA: 0x00009774 File Offset: 0x00007974
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x060002A9 RID: 681 RVA: 0x0000977B File Offset: 0x0000797B
			private void <>__Finally0()
			{
				Monitor.Exit(lockQueue);
			}

			// Token: 0x04000186 RID: 390
			internal object $locvar0;

			// Token: 0x04000187 RID: 391
			internal RemoteTestResultSender.QueueData <queueData>__1;

			// Token: 0x04000188 RID: 392
			internal RemoteTestResultSender $this;

			// Token: 0x04000189 RID: 393
			internal object $current;

			// Token: 0x0400018A RID: 394
			internal bool $disposing;

			// Token: 0x0400018B RID: 395
			internal int $PC;
		}
	}
}
