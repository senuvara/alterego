﻿using System;
using NUnit.Framework;
using NUnit.Framework.Interfaces;

namespace UnityEngine.TestTools.TestRunner.Callbacks
{
	// Token: 0x02000043 RID: 67
	[AddComponentMenu("")]
	internal class PlayModeRunnerCallback : MonoBehaviour, ITestRunnerListener
	{
		// Token: 0x06000188 RID: 392 RVA: 0x00009170 File Offset: 0x00007370
		public PlayModeRunnerCallback()
		{
		}

		// Token: 0x06000189 RID: 393 RVA: 0x00009178 File Offset: 0x00007378
		public void RunFinished(ITestResult testResults)
		{
			Application.logMessageReceivedThreaded -= this.LogRecieved;
			if (Camera.main == null)
			{
				base.gameObject.AddComponent<Camera>();
			}
			this.m_ResultRenderer = new TestResultRenderer(testResults);
			this.m_ResultRenderer.ShowResults();
		}

		// Token: 0x0600018A RID: 394 RVA: 0x000091CC File Offset: 0x000073CC
		public void TestFinished(ITestResult result)
		{
		}

		// Token: 0x0600018B RID: 395 RVA: 0x000091CF File Offset: 0x000073CF
		public void OnGUI()
		{
			if (this.m_ResultRenderer != null)
			{
				this.m_ResultRenderer.Draw();
			}
		}

		// Token: 0x0600018C RID: 396 RVA: 0x000091E8 File Offset: 0x000073E8
		public void RunStarted(ITest testsToRun)
		{
			Application.logMessageReceivedThreaded += this.LogRecieved;
		}

		// Token: 0x0600018D RID: 397 RVA: 0x000091FC File Offset: 0x000073FC
		public void TestStarted(ITest test)
		{
		}

		// Token: 0x0600018E RID: 398 RVA: 0x000091FF File Offset: 0x000073FF
		private void LogRecieved(string message, string stacktrace, LogType type)
		{
			if (TestContext.Out != null)
			{
				TestContext.Out.WriteLine(message);
			}
		}

		// Token: 0x040000AF RID: 175
		private TestResultRenderer m_ResultRenderer;
	}
}
