﻿using System;
using NUnit.Framework.Interfaces;

namespace UnityEngine.TestTools.TestRunner.Callbacks
{
	// Token: 0x02000048 RID: 72
	internal class TestResultRendererCallback : MonoBehaviour, ITestRunnerListener
	{
		// Token: 0x060001A3 RID: 419 RVA: 0x00009A80 File Offset: 0x00007C80
		public TestResultRendererCallback()
		{
		}

		// Token: 0x060001A4 RID: 420 RVA: 0x00009A88 File Offset: 0x00007C88
		public void RunStarted(ITest testsToRun)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060001A5 RID: 421 RVA: 0x00009A90 File Offset: 0x00007C90
		public void RunFinished(ITestResult testResults)
		{
			if (Camera.main == null)
			{
				base.gameObject.AddComponent<Camera>();
			}
			this.m_ResultRenderer = new TestResultRenderer(testResults);
			this.m_ResultRenderer.ShowResults();
		}

		// Token: 0x060001A6 RID: 422 RVA: 0x00009AC8 File Offset: 0x00007CC8
		public void OnGUI()
		{
			if (this.m_ResultRenderer != null)
			{
				this.m_ResultRenderer.Draw();
			}
		}

		// Token: 0x060001A7 RID: 423 RVA: 0x00009AE1 File Offset: 0x00007CE1
		public void TestStarted(ITest test)
		{
		}

		// Token: 0x060001A8 RID: 424 RVA: 0x00009AE4 File Offset: 0x00007CE4
		public void TestFinished(ITestResult result)
		{
		}

		// Token: 0x040000BC RID: 188
		private TestResultRenderer m_ResultRenderer;
	}
}
