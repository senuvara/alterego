﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using NUnit.Framework.Interfaces;
using UnityEngine.SceneManagement;
using UnityEngine.TestRunner.NUnitExtensions;
using UnityEngine.TestRunner.NUnitExtensions.Runner;
using UnityEngine.TestTools.NUnitExtensions;
using UnityEngine.TestTools.Utils;

namespace UnityEngine.TestTools.TestRunner
{
	// Token: 0x0200003A RID: 58
	[AddComponentMenu("")]
	[Serializable]
	internal class PlaymodeTestsController : MonoBehaviour
	{
		// Token: 0x0600015B RID: 347 RVA: 0x00007E40 File Offset: 0x00006040
		public PlaymodeTestsController()
		{
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x0600015C RID: 348 RVA: 0x00007E80 File Offset: 0x00006080
		// (set) Token: 0x0600015D RID: 349 RVA: 0x00007E9B File Offset: 0x0000609B
		public List<string> AssembliesWithTests
		{
			get
			{
				return this.m_AssembliesWithTests;
			}
			set
			{
				this.m_AssembliesWithTests = value;
			}
		}

		// Token: 0x0600015E RID: 350 RVA: 0x00007EA8 File Offset: 0x000060A8
		public IEnumerator Start()
		{
			yield return null;
			yield return null;
			base.StartCoroutine(this.Run());
			yield break;
		}

		// Token: 0x0600015F RID: 351 RVA: 0x00007ECC File Offset: 0x000060CC
		internal static bool IsControllerOnScene()
		{
			return GameObject.Find("Code-based tests runner") != null;
		}

		// Token: 0x06000160 RID: 352 RVA: 0x00007EF4 File Offset: 0x000060F4
		internal static PlaymodeTestsController GetController()
		{
			return GameObject.Find("Code-based tests runner").GetComponent<PlaymodeTestsController>();
		}

		// Token: 0x06000161 RID: 353 RVA: 0x00007F18 File Offset: 0x00006118
		public IEnumerator TestRunnerCorotine()
		{
			while (this.m_TestSteps.MoveNext())
			{
				yield return this.m_TestSteps.Current;
			}
			if (this.m_Runner.IsTestComplete)
			{
				this.runFinishedEvent.Invoke(this.m_Runner.Result);
				this.Cleanup();
				yield return null;
			}
			yield break;
		}

		// Token: 0x06000162 RID: 354 RVA: 0x00007F3C File Offset: 0x0000613C
		public IEnumerator Run()
		{
			CoroutineTestWorkItem.monoBehaviourCoroutineRunner = this;
			base.gameObject.hideFlags |= HideFlags.DontSave;
			if (this.settings.sceneBased)
			{
				SceneManager.LoadScene(1, LoadSceneMode.Additive);
				yield return null;
			}
			PlayerTestAssemblyProvider testListUtil = new PlayerTestAssemblyProvider(new AssemblyLoadProxy(), this.m_AssembliesWithTests);
			this.m_Runner = new UnityTestAssemblyRunner(UnityTestAssemblyBuilder.GetNUnitTestBuilder(TestPlatform.PlayMode), new PlaymodeWorkItemFactory());
			ITest loadedTests = this.m_Runner.Load((from a in testListUtil.GetUserAssemblies()
			select a.Assembly).ToArray<Assembly>(), UnityTestAssemblyBuilder.GetNUnitTestBuilderSettings(TestPlatform.PlayMode));
			loadedTests.ParseForNameDuplicates();
			this.runStartedEvent.Invoke(this.m_Runner.LoadedTest);
			TestListenerWrapper testListenerWrapper = new TestListenerWrapper(this.testStartedEvent, this.testFinishedEvent);
			this.m_TestSteps = this.m_Runner.Run(testListenerWrapper, this.settings.filter.BuildNUnitFilter()).GetEnumerator();
			yield return this.TestRunnerCorotine();
			yield break;
		}

		// Token: 0x06000163 RID: 355 RVA: 0x00007F5E File Offset: 0x0000615E
		public void Cleanup()
		{
			if (this.m_Runner != null)
			{
				this.m_Runner.StopRun();
				this.m_Runner = null;
			}
			if (Application.isEditor)
			{
				Object.Destroy(base.gameObject);
			}
		}

		// Token: 0x06000164 RID: 356 RVA: 0x00007F98 File Offset: 0x00006198
		public static void TryCleanup()
		{
			PlaymodeTestsController controller = PlaymodeTestsController.GetController();
			if (controller != null)
			{
				controller.Cleanup();
			}
		}

		// Token: 0x0400007A RID: 122
		private IEnumerator m_TestSteps;

		// Token: 0x0400007B RID: 123
		[SerializeField]
		private List<string> m_AssembliesWithTests;

		// Token: 0x0400007C RID: 124
		[SerializeField]
		internal TestStartedEvent testStartedEvent = new TestStartedEvent();

		// Token: 0x0400007D RID: 125
		[SerializeField]
		internal TestFinishedEvent testFinishedEvent = new TestFinishedEvent();

		// Token: 0x0400007E RID: 126
		[SerializeField]
		internal RunStartedEvent runStartedEvent = new RunStartedEvent();

		// Token: 0x0400007F RID: 127
		[SerializeField]
		internal RunFinishedEvent runFinishedEvent = new RunFinishedEvent();

		// Token: 0x04000080 RID: 128
		internal const string kPlaymodeTestControllerName = "Code-based tests runner";

		// Token: 0x04000081 RID: 129
		[SerializeField]
		public PlaymodeTestsControllerSettings settings = new PlaymodeTestsControllerSettings();

		// Token: 0x04000082 RID: 130
		internal UnityTestAssemblyRunner m_Runner;

		// Token: 0x0200007D RID: 125
		[CompilerGenerated]
		private sealed class <Start>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x06000290 RID: 656 RVA: 0x00007FC0 File Offset: 0x000061C0
			[DebuggerHidden]
			public <Start>c__Iterator0()
			{
			}

			// Token: 0x06000291 RID: 657 RVA: 0x00007FC8 File Offset: 0x000061C8
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					this.$current = null;
					if (!this.$disposing)
					{
						this.$PC = 1;
					}
					return true;
				case 1U:
					this.$current = null;
					if (!this.$disposing)
					{
						this.$PC = 2;
					}
					return true;
				case 2U:
					base.StartCoroutine(base.Run());
					this.$PC = -1;
					break;
				}
				return false;
			}

			// Token: 0x1700008D RID: 141
			// (get) Token: 0x06000292 RID: 658 RVA: 0x00008054 File Offset: 0x00006254
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x1700008E RID: 142
			// (get) Token: 0x06000293 RID: 659 RVA: 0x00008070 File Offset: 0x00006270
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x06000294 RID: 660 RVA: 0x0000808A File Offset: 0x0000628A
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x06000295 RID: 661 RVA: 0x0000809A File Offset: 0x0000629A
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x04000176 RID: 374
			internal PlaymodeTestsController $this;

			// Token: 0x04000177 RID: 375
			internal object $current;

			// Token: 0x04000178 RID: 376
			internal bool $disposing;

			// Token: 0x04000179 RID: 377
			internal int $PC;
		}

		// Token: 0x0200007E RID: 126
		[CompilerGenerated]
		private sealed class <TestRunnerCorotine>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x06000296 RID: 662 RVA: 0x000080A1 File Offset: 0x000062A1
			[DebuggerHidden]
			public <TestRunnerCorotine>c__Iterator1()
			{
			}

			// Token: 0x06000297 RID: 663 RVA: 0x000080AC File Offset: 0x000062AC
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					break;
				case 1U:
					break;
				case 2U:
					goto IL_C9;
				default:
					return false;
				}
				if (!this.m_TestSteps.MoveNext())
				{
					if (!this.m_Runner.IsTestComplete)
					{
						goto IL_C9;
					}
					this.runFinishedEvent.Invoke(this.m_Runner.Result);
					base.Cleanup();
					this.$current = null;
					if (!this.$disposing)
					{
						this.$PC = 2;
					}
				}
				else
				{
					this.$current = this.m_TestSteps.Current;
					if (!this.$disposing)
					{
						this.$PC = 1;
					}
				}
				return true;
				IL_C9:
				this.$PC = -1;
				return false;
			}

			// Token: 0x1700008F RID: 143
			// (get) Token: 0x06000298 RID: 664 RVA: 0x0000818C File Offset: 0x0000638C
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x17000090 RID: 144
			// (get) Token: 0x06000299 RID: 665 RVA: 0x000081A8 File Offset: 0x000063A8
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x0600029A RID: 666 RVA: 0x000081C2 File Offset: 0x000063C2
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x0600029B RID: 667 RVA: 0x000081D2 File Offset: 0x000063D2
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x0400017A RID: 378
			internal PlaymodeTestsController $this;

			// Token: 0x0400017B RID: 379
			internal object $current;

			// Token: 0x0400017C RID: 380
			internal bool $disposing;

			// Token: 0x0400017D RID: 381
			internal int $PC;
		}

		// Token: 0x0200007F RID: 127
		[CompilerGenerated]
		private sealed class <Run>c__Iterator2 : IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x0600029C RID: 668 RVA: 0x000081D9 File Offset: 0x000063D9
			[DebuggerHidden]
			public <Run>c__Iterator2()
			{
			}

			// Token: 0x0600029D RID: 669 RVA: 0x000081E4 File Offset: 0x000063E4
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					CoroutineTestWorkItem.monoBehaviourCoroutineRunner = this;
					base.gameObject.hideFlags |= HideFlags.DontSave;
					if (this.settings.sceneBased)
					{
						SceneManager.LoadScene(1, LoadSceneMode.Additive);
						this.$current = null;
						if (!this.$disposing)
						{
							this.$PC = 1;
						}
						return true;
					}
					break;
				case 1U:
					break;
				case 2U:
					this.$PC = -1;
					return false;
				default:
					return false;
				}
				testListUtil = new PlayerTestAssemblyProvider(new AssemblyLoadProxy(), this.m_AssembliesWithTests);
				this.m_Runner = new UnityTestAssemblyRunner(UnityTestAssemblyBuilder.GetNUnitTestBuilder(TestPlatform.PlayMode), new PlaymodeWorkItemFactory());
				loadedTests = this.m_Runner.Load((from a in testListUtil.GetUserAssemblies()
				select a.Assembly).ToArray<Assembly>(), UnityTestAssemblyBuilder.GetNUnitTestBuilderSettings(TestPlatform.PlayMode));
				loadedTests.ParseForNameDuplicates();
				this.runStartedEvent.Invoke(this.m_Runner.LoadedTest);
				testListenerWrapper = new TestListenerWrapper(this.testStartedEvent, this.testFinishedEvent);
				this.m_TestSteps = this.m_Runner.Run(testListenerWrapper, this.settings.filter.BuildNUnitFilter()).GetEnumerator();
				this.$current = base.TestRunnerCorotine();
				if (!this.$disposing)
				{
					this.$PC = 2;
				}
				return true;
			}

			// Token: 0x17000091 RID: 145
			// (get) Token: 0x0600029E RID: 670 RVA: 0x000083B0 File Offset: 0x000065B0
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x17000092 RID: 146
			// (get) Token: 0x0600029F RID: 671 RVA: 0x000083CC File Offset: 0x000065CC
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x060002A0 RID: 672 RVA: 0x000083E6 File Offset: 0x000065E6
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x060002A1 RID: 673 RVA: 0x000083F6 File Offset: 0x000065F6
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x060002A2 RID: 674 RVA: 0x00008400 File Offset: 0x00006600
			private static Assembly <>m__0(IAssemblyWrapper a)
			{
				return a.Assembly;
			}

			// Token: 0x0400017E RID: 382
			internal PlayerTestAssemblyProvider <testListUtil>__0;

			// Token: 0x0400017F RID: 383
			internal ITest <loadedTests>__0;

			// Token: 0x04000180 RID: 384
			internal TestListenerWrapper <testListenerWrapper>__0;

			// Token: 0x04000181 RID: 385
			internal PlaymodeTestsController $this;

			// Token: 0x04000182 RID: 386
			internal object $current;

			// Token: 0x04000183 RID: 387
			internal bool $disposing;

			// Token: 0x04000184 RID: 388
			internal int $PC;

			// Token: 0x04000185 RID: 389
			private static Func<IAssemblyWrapper, Assembly> <>f__am$cache0;
		}
	}
}
