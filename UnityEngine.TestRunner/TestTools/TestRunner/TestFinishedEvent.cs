﻿using System;
using NUnit.Framework.Interfaces;
using UnityEngine.Events;

namespace UnityEngine.TestTools.TestRunner
{
	// Token: 0x02000036 RID: 54
	[Serializable]
	internal class TestFinishedEvent : UnityEvent<ITestResult>
	{
		// Token: 0x06000157 RID: 343 RVA: 0x00007E20 File Offset: 0x00006020
		public TestFinishedEvent()
		{
		}
	}
}
