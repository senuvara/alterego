﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;

namespace UnityEngine.TestTools.Logging
{
	// Token: 0x0200000E RID: 14
	internal class LogScope : IDisposable
	{
		// Token: 0x06000032 RID: 50 RVA: 0x000027C8 File Offset: 0x000009C8
		public LogScope()
		{
			this.AllLogs = new List<LogEvent>();
			this.FailingLogs = new List<LogEvent>();
			this.ExpectedLogs = new Queue<LogMatch>();
			this.IgnoreFailingMessages = false;
			this.Activate();
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000033 RID: 51 RVA: 0x00002818 File Offset: 0x00000A18
		// (set) Token: 0x06000034 RID: 52 RVA: 0x00002832 File Offset: 0x00000A32
		public Queue<LogMatch> ExpectedLogs
		{
			[CompilerGenerated]
			get
			{
				return this.<ExpectedLogs>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ExpectedLogs>k__BackingField = value;
			}
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000035 RID: 53 RVA: 0x0000283C File Offset: 0x00000A3C
		public List<LogEvent> AllLogs
		{
			[CompilerGenerated]
			get
			{
				return this.<AllLogs>k__BackingField;
			}
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000036 RID: 54 RVA: 0x00002858 File Offset: 0x00000A58
		public List<LogEvent> FailingLogs
		{
			[CompilerGenerated]
			get
			{
				return this.<FailingLogs>k__BackingField;
			}
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000037 RID: 55 RVA: 0x00002874 File Offset: 0x00000A74
		// (set) Token: 0x06000038 RID: 56 RVA: 0x0000288E File Offset: 0x00000A8E
		public bool IgnoreFailingMessages
		{
			[CompilerGenerated]
			get
			{
				return this.<IgnoreFailingMessages>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<IgnoreFailingMessages>k__BackingField = value;
			}
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000039 RID: 57 RVA: 0x00002898 File Offset: 0x00000A98
		// (set) Token: 0x0600003A RID: 58 RVA: 0x000028B2 File Offset: 0x00000AB2
		public bool IsNUnitException
		{
			[CompilerGenerated]
			get
			{
				return this.<IsNUnitException>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<IsNUnitException>k__BackingField = value;
			}
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x0600003B RID: 59 RVA: 0x000028BC File Offset: 0x00000ABC
		// (set) Token: 0x0600003C RID: 60 RVA: 0x000028D6 File Offset: 0x00000AD6
		public bool IsNUnitSuccessException
		{
			[CompilerGenerated]
			get
			{
				return this.<IsNUnitSuccessException>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<IsNUnitSuccessException>k__BackingField = value;
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x0600003D RID: 61 RVA: 0x000028E0 File Offset: 0x00000AE0
		// (set) Token: 0x0600003E RID: 62 RVA: 0x000028FA File Offset: 0x00000AFA
		public bool IsNUnitInconclusiveException
		{
			[CompilerGenerated]
			get
			{
				return this.<IsNUnitInconclusiveException>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<IsNUnitInconclusiveException>k__BackingField = value;
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x0600003F RID: 63 RVA: 0x00002904 File Offset: 0x00000B04
		// (set) Token: 0x06000040 RID: 64 RVA: 0x0000291E File Offset: 0x00000B1E
		public bool IsNUnitIgnoreException
		{
			[CompilerGenerated]
			get
			{
				return this.<IsNUnitIgnoreException>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<IsNUnitIgnoreException>k__BackingField = value;
			}
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000041 RID: 65 RVA: 0x00002928 File Offset: 0x00000B28
		// (set) Token: 0x06000042 RID: 66 RVA: 0x00002942 File Offset: 0x00000B42
		public string NUnitExceptionMessage
		{
			[CompilerGenerated]
			get
			{
				return this.<NUnitExceptionMessage>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<NUnitExceptionMessage>k__BackingField = value;
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000043 RID: 67 RVA: 0x0000294C File Offset: 0x00000B4C
		internal static LogScope Current
		{
			get
			{
				if (LogScope.s_ActiveScopes.Count == 0)
				{
					throw new InvalidOperationException("No log scope is available");
				}
				return LogScope.s_ActiveScopes[0];
			}
		}

		// Token: 0x06000044 RID: 68 RVA: 0x00002988 File Offset: 0x00000B88
		internal static bool HasCurrentLogScope()
		{
			return LogScope.s_ActiveScopes.Count > 0;
		}

		// Token: 0x06000045 RID: 69 RVA: 0x000029AA File Offset: 0x00000BAA
		private void Activate()
		{
			LogScope.s_ActiveScopes.Insert(0, this);
			LogScope.RegisterScope(this);
			Application.logMessageReceivedThreaded -= this.AddLog;
			Application.logMessageReceivedThreaded += this.AddLog;
		}

		// Token: 0x06000046 RID: 70 RVA: 0x000029E1 File Offset: 0x00000BE1
		private void Deactivate()
		{
			Application.logMessageReceivedThreaded -= this.AddLog;
			LogScope.s_ActiveScopes.Remove(this);
			LogScope.UnregisterScope(this);
		}

		// Token: 0x06000047 RID: 71 RVA: 0x00002A07 File Offset: 0x00000C07
		private static void RegisterScope(LogScope logScope)
		{
			Application.logMessageReceivedThreaded += logScope.AddLog;
		}

		// Token: 0x06000048 RID: 72 RVA: 0x00002A1B File Offset: 0x00000C1B
		private static void UnregisterScope(LogScope logScope)
		{
			Application.logMessageReceivedThreaded -= logScope.AddLog;
		}

		// Token: 0x06000049 RID: 73 RVA: 0x00002A30 File Offset: 0x00000C30
		public void AddLog(string message, string stacktrace, LogType type)
		{
			object @lock = this._lock;
			lock (@lock)
			{
				this.m_NeedToProcessLogs = true;
				LogEvent item = new LogEvent
				{
					LogType = type,
					Message = message,
					StackTrace = stacktrace
				};
				this.AllLogs.Add(item);
				if (LogScope.IsNUnitResultStateException(stacktrace, type))
				{
					if (message.StartsWith("SuccessException"))
					{
						this.IsNUnitException = true;
						this.IsNUnitSuccessException = true;
						if (message.StartsWith("SuccessException: "))
						{
							this.NUnitExceptionMessage = message.Substring("SuccessException: ".Length);
							return;
						}
					}
					else if (message.StartsWith("InconclusiveException"))
					{
						this.IsNUnitException = true;
						this.IsNUnitInconclusiveException = true;
						if (message.StartsWith("InconclusiveException: "))
						{
							this.NUnitExceptionMessage = message.Substring("InconclusiveException: ".Length);
							return;
						}
					}
					else if (message.StartsWith("IgnoreException"))
					{
						this.IsNUnitException = true;
						this.IsNUnitIgnoreException = true;
						if (message.StartsWith("IgnoreException: "))
						{
							this.NUnitExceptionMessage = message.Substring("IgnoreException: ".Length);
							return;
						}
					}
				}
				if (LogScope.IsFailingLog(type) && !this.IgnoreFailingMessages)
				{
					this.FailingLogs.Add(item);
				}
			}
		}

		// Token: 0x0600004A RID: 74 RVA: 0x00002BC8 File Offset: 0x00000DC8
		public bool IsAllLogsHandled()
		{
			object @lock = this._lock;
			bool result;
			lock (@lock)
			{
				result = this.AllLogs.All((LogEvent x) => x.IsHandled);
			}
			return result;
		}

		// Token: 0x0600004B RID: 75 RVA: 0x00002C2C File Offset: 0x00000E2C
		public LogEvent GetUnhandledLog()
		{
			object @lock = this._lock;
			LogEvent result;
			lock (@lock)
			{
				result = this.AllLogs.First((LogEvent x) => !x.IsHandled);
			}
			return result;
		}

		// Token: 0x0600004C RID: 76 RVA: 0x00002C90 File Offset: 0x00000E90
		private static bool IsNUnitResultStateException(string stacktrace, LogType logType)
		{
			return logType == LogType.Exception && (string.IsNullOrEmpty(stacktrace) || stacktrace.StartsWith("NUnit.Framework.Assert."));
		}

		// Token: 0x0600004D RID: 77 RVA: 0x00002CCC File Offset: 0x00000ECC
		private static bool IsFailingLog(LogType type)
		{
			switch (type)
			{
			case LogType.Error:
			case LogType.Assert:
			case LogType.Exception:
				return true;
			}
			return false;
		}

		// Token: 0x0600004E RID: 78 RVA: 0x00002D08 File Offset: 0x00000F08
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x0600004F RID: 79 RVA: 0x00002D18 File Offset: 0x00000F18
		protected virtual void Dispose(bool disposing)
		{
			if (!this.m_Disposed)
			{
				this.m_Disposed = true;
				if (disposing)
				{
					this.Deactivate();
				}
			}
		}

		// Token: 0x06000050 RID: 80 RVA: 0x00002D44 File Offset: 0x00000F44
		internal bool AnyFailingLogs()
		{
			this.ProcessExpectedLogs();
			return this.FailingLogs.Any<LogEvent>();
		}

		// Token: 0x06000051 RID: 81 RVA: 0x00002D6C File Offset: 0x00000F6C
		internal void ProcessExpectedLogs()
		{
			object @lock = this._lock;
			lock (@lock)
			{
				if (this.m_NeedToProcessLogs && this.ExpectedLogs.Any<LogMatch>())
				{
					LogMatch logMatch = null;
					foreach (LogEvent logEvent in this.AllLogs)
					{
						if (!this.ExpectedLogs.Any<LogMatch>())
						{
							break;
						}
						if (logMatch == null && this.ExpectedLogs.Any<LogMatch>())
						{
							logMatch = this.ExpectedLogs.Peek();
						}
						if (logMatch != null && logMatch.Matches(logEvent))
						{
							this.ExpectedLogs.Dequeue();
							logEvent.IsHandled = true;
							if (this.FailingLogs.Any(new Func<LogEvent, bool>(logMatch.Matches)))
							{
								LogEvent item = this.FailingLogs.First(new Func<LogEvent, bool>(logMatch.Matches));
								this.FailingLogs.Remove(item);
							}
							logMatch = null;
						}
					}
					this.m_NeedToProcessLogs = false;
				}
			}
		}

		// Token: 0x06000052 RID: 82 RVA: 0x00002ED4 File Offset: 0x000010D4
		// Note: this type is marked as 'beforefieldinit'.
		static LogScope()
		{
		}

		// Token: 0x06000053 RID: 83 RVA: 0x00002EE0 File Offset: 0x000010E0
		[CompilerGenerated]
		private static bool <IsAllLogsHandled>m__0(LogEvent x)
		{
			return x.IsHandled;
		}

		// Token: 0x06000054 RID: 84 RVA: 0x00002EFC File Offset: 0x000010FC
		[CompilerGenerated]
		private static bool <GetUnhandledLog>m__1(LogEvent x)
		{
			return !x.IsHandled;
		}

		// Token: 0x0400000D RID: 13
		private bool m_Disposed;

		// Token: 0x0400000E RID: 14
		private readonly object _lock = new object();

		// Token: 0x0400000F RID: 15
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Queue<LogMatch> <ExpectedLogs>k__BackingField;

		// Token: 0x04000010 RID: 16
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private readonly List<LogEvent> <AllLogs>k__BackingField;

		// Token: 0x04000011 RID: 17
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private readonly List<LogEvent> <FailingLogs>k__BackingField;

		// Token: 0x04000012 RID: 18
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <IgnoreFailingMessages>k__BackingField;

		// Token: 0x04000013 RID: 19
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <IsNUnitException>k__BackingField;

		// Token: 0x04000014 RID: 20
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <IsNUnitSuccessException>k__BackingField;

		// Token: 0x04000015 RID: 21
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <IsNUnitInconclusiveException>k__BackingField;

		// Token: 0x04000016 RID: 22
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <IsNUnitIgnoreException>k__BackingField;

		// Token: 0x04000017 RID: 23
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <NUnitExceptionMessage>k__BackingField;

		// Token: 0x04000018 RID: 24
		private bool m_NeedToProcessLogs;

		// Token: 0x04000019 RID: 25
		private static List<LogScope> s_ActiveScopes = new List<LogScope>();

		// Token: 0x0400001A RID: 26
		[CompilerGenerated]
		private static Func<LogEvent, bool> <>f__am$cache0;

		// Token: 0x0400001B RID: 27
		[CompilerGenerated]
		private static Func<LogEvent, bool> <>f__am$cache1;
	}
}
