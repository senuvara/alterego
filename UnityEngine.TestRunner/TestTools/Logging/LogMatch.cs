﻿using System;
using System.Text.RegularExpressions;

namespace UnityEngine.TestTools.Logging
{
	// Token: 0x0200000D RID: 13
	[Serializable]
	internal class LogMatch
	{
		// Token: 0x06000029 RID: 41 RVA: 0x000025A4 File Offset: 0x000007A4
		public LogMatch()
		{
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x0600002A RID: 42 RVA: 0x000025AC File Offset: 0x000007AC
		// (set) Token: 0x0600002B RID: 43 RVA: 0x000025C7 File Offset: 0x000007C7
		public string Message
		{
			get
			{
				return this.m_Message;
			}
			set
			{
				this.m_Message = value;
				this.m_UseRegex = false;
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x0600002C RID: 44 RVA: 0x000025D8 File Offset: 0x000007D8
		// (set) Token: 0x0600002D RID: 45 RVA: 0x0000260B File Offset: 0x0000080B
		public Regex MessageRegex
		{
			get
			{
				Regex result;
				if (!this.m_UseRegex)
				{
					result = null;
				}
				else
				{
					result = new Regex(this.m_MessageRegex);
				}
				return result;
			}
			set
			{
				if (value != null)
				{
					this.m_MessageRegex = value.ToString();
					this.m_UseRegex = true;
				}
				else
				{
					this.m_MessageRegex = null;
					this.m_UseRegex = false;
				}
			}
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x0600002E RID: 46 RVA: 0x00002640 File Offset: 0x00000840
		// (set) Token: 0x0600002F RID: 47 RVA: 0x00002694 File Offset: 0x00000894
		public LogType? LogType
		{
			get
			{
				LogType? result;
				if (!string.IsNullOrEmpty(this.m_LogType))
				{
					result = (Enum.Parse(typeof(LogType), this.m_LogType) as LogType?);
				}
				else
				{
					result = null;
				}
				return result;
			}
			set
			{
				if (value != null)
				{
					this.m_LogType = value.Value.ToString();
				}
				else
				{
					this.m_LogType = null;
				}
			}
		}

		// Token: 0x06000030 RID: 48 RVA: 0x000026DC File Offset: 0x000008DC
		public bool Matches(LogEvent log)
		{
			bool result;
			if (this.LogType != null && this.LogType != log.LogType)
			{
				result = false;
			}
			else if (this.m_UseRegex)
			{
				result = this.MessageRegex.IsMatch(log.Message);
			}
			else
			{
				result = this.Message.Equals(log.Message);
			}
			return result;
		}

		// Token: 0x06000031 RID: 49 RVA: 0x0000276C File Offset: 0x0000096C
		public override string ToString()
		{
			string result;
			if (this.m_UseRegex)
			{
				result = string.Format("[{0}] Regex: {1}", this.LogType, this.MessageRegex);
			}
			else
			{
				result = string.Format("[{0}] {1}", this.LogType, this.Message);
			}
			return result;
		}

		// Token: 0x04000009 RID: 9
		[SerializeField]
		private bool m_UseRegex;

		// Token: 0x0400000A RID: 10
		[SerializeField]
		private string m_Message;

		// Token: 0x0400000B RID: 11
		[SerializeField]
		private string m_MessageRegex;

		// Token: 0x0400000C RID: 12
		[SerializeField]
		private string m_LogType;
	}
}
