﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.TestTools.Logging
{
	// Token: 0x0200000C RID: 12
	internal class LogEvent
	{
		// Token: 0x0600001F RID: 31 RVA: 0x000024DA File Offset: 0x000006DA
		public LogEvent()
		{
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000020 RID: 32 RVA: 0x000024E4 File Offset: 0x000006E4
		// (set) Token: 0x06000021 RID: 33 RVA: 0x000024FE File Offset: 0x000006FE
		public string Message
		{
			[CompilerGenerated]
			get
			{
				return this.<Message>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Message>k__BackingField = value;
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000022 RID: 34 RVA: 0x00002508 File Offset: 0x00000708
		// (set) Token: 0x06000023 RID: 35 RVA: 0x00002522 File Offset: 0x00000722
		public string StackTrace
		{
			[CompilerGenerated]
			get
			{
				return this.<StackTrace>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<StackTrace>k__BackingField = value;
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000024 RID: 36 RVA: 0x0000252C File Offset: 0x0000072C
		// (set) Token: 0x06000025 RID: 37 RVA: 0x00002546 File Offset: 0x00000746
		public LogType LogType
		{
			[CompilerGenerated]
			get
			{
				return this.<LogType>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<LogType>k__BackingField = value;
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x06000026 RID: 38 RVA: 0x00002550 File Offset: 0x00000750
		// (set) Token: 0x06000027 RID: 39 RVA: 0x0000256A File Offset: 0x0000076A
		public bool IsHandled
		{
			[CompilerGenerated]
			get
			{
				return this.<IsHandled>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<IsHandled>k__BackingField = value;
			}
		}

		// Token: 0x06000028 RID: 40 RVA: 0x00002574 File Offset: 0x00000774
		public override string ToString()
		{
			return string.Format("[{0}] {1}", this.LogType, this.Message);
		}

		// Token: 0x04000005 RID: 5
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <Message>k__BackingField;

		// Token: 0x04000006 RID: 6
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <StackTrace>k__BackingField;

		// Token: 0x04000007 RID: 7
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private LogType <LogType>k__BackingField;

		// Token: 0x04000008 RID: 8
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <IsHandled>k__BackingField;
	}
}
