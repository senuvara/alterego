﻿using System;
using System.Collections.Generic;

namespace UnityEngine.TestTools.Logging
{
	// Token: 0x0200000B RID: 11
	internal interface ILogScope : IDisposable
	{
		// Token: 0x1700000A RID: 10
		// (get) Token: 0x0600001E RID: 30
		List<LogEvent> LogEvents { get; }
	}
}
