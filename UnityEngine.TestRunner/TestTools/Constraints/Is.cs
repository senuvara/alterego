﻿using System;
using NUnit.Framework;

namespace UnityEngine.TestTools.Constraints
{
	// Token: 0x02000006 RID: 6
	public class Is : Is
	{
		// Token: 0x0600000B RID: 11 RVA: 0x0000227A File Offset: 0x0000047A
		public Is()
		{
		}

		// Token: 0x0600000C RID: 12 RVA: 0x00002284 File Offset: 0x00000484
		public static AllocatingGCMemoryConstraint AllocatingGCMemory()
		{
			return new AllocatingGCMemoryConstraint();
		}
	}
}
