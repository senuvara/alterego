﻿using System;
using NUnit.Framework.Constraints;

namespace UnityEngine.TestTools.Constraints
{
	// Token: 0x02000004 RID: 4
	public static class ConstraintExtensions
	{
		// Token: 0x06000008 RID: 8 RVA: 0x00002230 File Offset: 0x00000430
		public static AllocatingGCMemoryConstraint AllocatingGCMemory(this ConstraintExpression chain)
		{
			AllocatingGCMemoryConstraint allocatingGCMemoryConstraint = new AllocatingGCMemoryConstraint();
			chain.Append(allocatingGCMemoryConstraint);
			return allocatingGCMemoryConstraint;
		}
	}
}
