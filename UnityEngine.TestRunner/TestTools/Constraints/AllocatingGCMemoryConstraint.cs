﻿using System;
using System.Runtime.CompilerServices;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using UnityEngine.Profiling;

namespace UnityEngine.TestTools.Constraints
{
	// Token: 0x02000002 RID: 2
	public class AllocatingGCMemoryConstraint : Constraint
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public AllocatingGCMemoryConstraint() : base(new object[0])
		{
		}

		// Token: 0x06000002 RID: 2 RVA: 0x00002060 File Offset: 0x00000260
		private ConstraintResult ApplyTo(Action action, object original)
		{
			Recorder recorder = Recorder.Get("GC.Alloc");
			recorder.enabled = false;
			recorder.FilterToCurrentThread();
			recorder.enabled = true;
			try
			{
				action();
			}
			finally
			{
				recorder.enabled = false;
				recorder.CollectFromAllThreads();
			}
			return new AllocatingGCMemoryConstraint.AllocatingGCMemoryResult(this, original, recorder.sampleBlockCount);
		}

		// Token: 0x06000003 RID: 3 RVA: 0x000020D0 File Offset: 0x000002D0
		public override ConstraintResult ApplyTo(object obj)
		{
			if (obj == null)
			{
				throw new ArgumentNullException();
			}
			TestDelegate d = obj as TestDelegate;
			if (d == null)
			{
				throw new ArgumentException(string.Format("The actual value must be a TestDelegate but was {0}", obj.GetType()));
			}
			return this.ApplyTo(delegate()
			{
				d();
			}, obj);
		}

		// Token: 0x06000004 RID: 4 RVA: 0x00002138 File Offset: 0x00000338
		public override ConstraintResult ApplyTo<TActual>(ActualValueDelegate<TActual> del)
		{
			if (del == null)
			{
				throw new ArgumentNullException();
			}
			return this.ApplyTo(delegate()
			{
				del();
			}, del);
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000005 RID: 5 RVA: 0x00002184 File Offset: 0x00000384
		public override string Description
		{
			get
			{
				return "allocates GC memory";
			}
		}

		// Token: 0x02000003 RID: 3
		private class AllocatingGCMemoryResult : ConstraintResult
		{
			// Token: 0x06000006 RID: 6 RVA: 0x0000219E File Offset: 0x0000039E
			public AllocatingGCMemoryResult(IConstraint constraint, object actualValue, int diff) : base(constraint, actualValue, diff > 0)
			{
				this.diff = diff;
			}

			// Token: 0x06000007 RID: 7 RVA: 0x000021B4 File Offset: 0x000003B4
			public override void WriteMessageTo(MessageWriter writer)
			{
				if (this.diff == 0)
				{
					writer.WriteMessageLine("The provided delegate did not make any GC allocations.", new object[0]);
				}
				else
				{
					writer.WriteMessageLine("The provided delegate made {0} GC allocation(s).", new object[]
					{
						this.diff
					});
				}
			}

			// Token: 0x04000001 RID: 1
			private readonly int diff;
		}

		// Token: 0x0200006A RID: 106
		[CompilerGenerated]
		private sealed class <ApplyTo>c__AnonStorey0
		{
			// Token: 0x06000223 RID: 547 RVA: 0x00002202 File Offset: 0x00000402
			public <ApplyTo>c__AnonStorey0()
			{
			}

			// Token: 0x06000224 RID: 548 RVA: 0x0000220A File Offset: 0x0000040A
			internal void <>m__0()
			{
				this.d();
			}

			// Token: 0x04000108 RID: 264
			internal TestDelegate d;
		}

		// Token: 0x0200006B RID: 107
		[CompilerGenerated]
		private sealed class <ApplyTo>c__AnonStorey1<TActual>
		{
			// Token: 0x06000225 RID: 549 RVA: 0x00002217 File Offset: 0x00000417
			public <ApplyTo>c__AnonStorey1()
			{
			}

			// Token: 0x06000226 RID: 550 RVA: 0x0000221F File Offset: 0x0000041F
			internal void <>m__0()
			{
				this.del();
			}

			// Token: 0x04000109 RID: 265
			internal ActualValueDelegate<TActual> del;
		}
	}
}
