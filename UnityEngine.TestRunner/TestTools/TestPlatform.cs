﻿using System;

namespace UnityEngine.TestTools
{
	// Token: 0x0200003E RID: 62
	[Flags]
	[Serializable]
	public enum TestPlatform : byte
	{
		// Token: 0x0400008B RID: 139
		All = 255,
		// Token: 0x0400008C RID: 140
		EditMode = 2,
		// Token: 0x0400008D RID: 141
		PlayMode = 4
	}
}
