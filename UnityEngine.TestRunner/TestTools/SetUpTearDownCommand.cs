﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using NUnit.Framework;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Commands;
using NUnit.Framework.Internal.Execution;
using UnityEngine.TestRunner.NUnitExtensions.Runner;

namespace UnityEngine.TestTools
{
	// Token: 0x02000021 RID: 33
	internal class SetUpTearDownCommand : BeforeAfterTestCommandBase<SetUpTearDownItem>
	{
		// Token: 0x060000B0 RID: 176 RVA: 0x00004E04 File Offset: 0x00003004
		public SetUpTearDownCommand(TestCommand innerCommand) : base(innerCommand)
		{
			List<SetUpTearDownItem> list = CommandBuilder.BuildSetUpTearDownList(base.Test.TypeInfo.Type, typeof(SetUpAttribute), typeof(TearDownAttribute));
			list.Reverse();
			this.BeforeActions = list.ToArray();
			this.AfterActions = this.BeforeActions.Reverse<SetUpTearDownItem>().ToArray<SetUpTearDownItem>();
		}

		// Token: 0x060000B1 RID: 177 RVA: 0x00004E6C File Offset: 0x0000306C
		protected override IEnumerator InvokeBefore(SetUpTearDownItem action, Test test, UnityTestExecutionContext context)
		{
			action.RunSetUp(context);
			yield return null;
			yield break;
		}

		// Token: 0x060000B2 RID: 178 RVA: 0x00004E98 File Offset: 0x00003098
		protected override IEnumerator InvokeAfter(SetUpTearDownItem action, Test test, UnityTestExecutionContext context)
		{
			action.RunTearDown(context);
			yield return null;
			yield break;
		}

		// Token: 0x060000B3 RID: 179 RVA: 0x00004EC4 File Offset: 0x000030C4
		protected override BeforeAfterTestCommandState GetState(UnityTestExecutionContext context)
		{
			return null;
		}

		// Token: 0x02000074 RID: 116
		[CompilerGenerated]
		private sealed class <InvokeBefore>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x0600024E RID: 590 RVA: 0x00004EDA File Offset: 0x000030DA
			[DebuggerHidden]
			public <InvokeBefore>c__Iterator0()
			{
			}

			// Token: 0x0600024F RID: 591 RVA: 0x00004EE4 File Offset: 0x000030E4
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					action.RunSetUp(context);
					this.$current = null;
					if (!this.$disposing)
					{
						this.$PC = 1;
					}
					return true;
				case 1U:
					this.$PC = -1;
					break;
				}
				return false;
			}

			// Token: 0x1700007B RID: 123
			// (get) Token: 0x06000250 RID: 592 RVA: 0x00004F4C File Offset: 0x0000314C
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x1700007C RID: 124
			// (get) Token: 0x06000251 RID: 593 RVA: 0x00004F68 File Offset: 0x00003168
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x06000252 RID: 594 RVA: 0x00004F82 File Offset: 0x00003182
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x06000253 RID: 595 RVA: 0x00004F92 File Offset: 0x00003192
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x04000134 RID: 308
			internal SetUpTearDownItem action;

			// Token: 0x04000135 RID: 309
			internal UnityTestExecutionContext context;

			// Token: 0x04000136 RID: 310
			internal object $current;

			// Token: 0x04000137 RID: 311
			internal bool $disposing;

			// Token: 0x04000138 RID: 312
			internal int $PC;
		}

		// Token: 0x02000075 RID: 117
		[CompilerGenerated]
		private sealed class <InvokeAfter>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x06000254 RID: 596 RVA: 0x00004F99 File Offset: 0x00003199
			[DebuggerHidden]
			public <InvokeAfter>c__Iterator1()
			{
			}

			// Token: 0x06000255 RID: 597 RVA: 0x00004FA4 File Offset: 0x000031A4
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					action.RunTearDown(context);
					this.$current = null;
					if (!this.$disposing)
					{
						this.$PC = 1;
					}
					return true;
				case 1U:
					this.$PC = -1;
					break;
				}
				return false;
			}

			// Token: 0x1700007D RID: 125
			// (get) Token: 0x06000256 RID: 598 RVA: 0x0000500C File Offset: 0x0000320C
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x1700007E RID: 126
			// (get) Token: 0x06000257 RID: 599 RVA: 0x00005028 File Offset: 0x00003228
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x06000258 RID: 600 RVA: 0x00005042 File Offset: 0x00003242
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x06000259 RID: 601 RVA: 0x00005052 File Offset: 0x00003252
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x04000139 RID: 313
			internal SetUpTearDownItem action;

			// Token: 0x0400013A RID: 314
			internal UnityTestExecutionContext context;

			// Token: 0x0400013B RID: 315
			internal object $current;

			// Token: 0x0400013C RID: 316
			internal bool $disposing;

			// Token: 0x0400013D RID: 317
			internal int $PC;
		}
	}
}
