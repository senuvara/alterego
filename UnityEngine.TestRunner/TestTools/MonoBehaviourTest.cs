﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.TestTools
{
	// Token: 0x02000069 RID: 105
	public class MonoBehaviourTest<T> : CustomYieldInstruction where T : MonoBehaviour, IMonoBehaviourTest
	{
		// Token: 0x0600021F RID: 543 RVA: 0x0000B000 File Offset: 0x00009200
		public MonoBehaviourTest(bool dontDestroyOnLoad = true)
		{
			GameObject gameObject = new GameObject("MonoBehaviourTest: " + typeof(T).FullName);
			this.component = gameObject.AddComponent<T>();
			if (dontDestroyOnLoad)
			{
				Object.DontDestroyOnLoad(gameObject);
			}
		}

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x06000220 RID: 544 RVA: 0x0000B050 File Offset: 0x00009250
		public T component
		{
			[CompilerGenerated]
			get
			{
				return this.<component>k__BackingField;
			}
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x06000221 RID: 545 RVA: 0x0000B06C File Offset: 0x0000926C
		public GameObject gameObject
		{
			get
			{
				T component = this.component;
				return component.gameObject;
			}
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x06000222 RID: 546 RVA: 0x0000B098 File Offset: 0x00009298
		public override bool keepWaiting
		{
			get
			{
				T component = this.component;
				return !component.IsTestFinished;
			}
		}

		// Token: 0x04000107 RID: 263
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private readonly T <component>k__BackingField;
	}
}
