﻿using System;

namespace UnityEngine.TestTools
{
	// Token: 0x0200003F RID: 63
	internal static class TestPlatformEnumExtensions
	{
		// Token: 0x0600016E RID: 366 RVA: 0x00008608 File Offset: 0x00006808
		public static bool IsFlagIncluded(this TestPlatform flags, TestPlatform flag)
		{
			return (flags & flag) == flag;
		}
	}
}
