﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Commands;
using UnityEngine.TestRunner.NUnitExtensions.Runner;

namespace UnityEngine.TestTools
{
	// Token: 0x02000020 RID: 32
	internal class OuterUnityTestActionCommand : BeforeAfterTestCommandBase<IOuterUnityTestAction>
	{
		// Token: 0x060000AB RID: 171 RVA: 0x00004CF8 File Offset: 0x00002EF8
		public OuterUnityTestActionCommand(TestCommand innerCommand) : base(innerCommand)
		{
			if (base.Test.TypeInfo.Type != null)
			{
				this.BeforeActions = OuterUnityTestActionCommand.GetUnityTestActionsFromMethod(base.Test.Method.MethodInfo);
				this.AfterActions = this.BeforeActions;
			}
		}

		// Token: 0x060000AC RID: 172 RVA: 0x00004D4C File Offset: 0x00002F4C
		private static IOuterUnityTestAction[] GetUnityTestActionsFromMethod(MethodInfo method)
		{
			object[] customAttributes = method.GetCustomAttributes(false);
			List<IOuterUnityTestAction> list = new List<IOuterUnityTestAction>();
			foreach (object obj in customAttributes)
			{
				if (obj is IOuterUnityTestAction)
				{
					list.Add(obj as IOuterUnityTestAction);
				}
			}
			return list.ToArray();
		}

		// Token: 0x060000AD RID: 173 RVA: 0x00004DB0 File Offset: 0x00002FB0
		protected override IEnumerator InvokeBefore(IOuterUnityTestAction action, Test test, UnityTestExecutionContext context)
		{
			return action.BeforeTest(test);
		}

		// Token: 0x060000AE RID: 174 RVA: 0x00004DCC File Offset: 0x00002FCC
		protected override IEnumerator InvokeAfter(IOuterUnityTestAction action, Test test, UnityTestExecutionContext context)
		{
			return action.AfterTest(test);
		}

		// Token: 0x060000AF RID: 175 RVA: 0x00004DE8 File Offset: 0x00002FE8
		protected override BeforeAfterTestCommandState GetState(UnityTestExecutionContext context)
		{
			return context.OuterUnityTestActionState;
		}
	}
}
