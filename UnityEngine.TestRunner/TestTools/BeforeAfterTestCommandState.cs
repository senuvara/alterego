﻿using System;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace UnityEngine.TestTools
{
	// Token: 0x0200001C RID: 28
	internal class BeforeAfterTestCommandState : ScriptableObject
	{
		// Token: 0x06000092 RID: 146 RVA: 0x000045E8 File Offset: 0x000027E8
		public BeforeAfterTestCommandState()
		{
		}

		// Token: 0x06000093 RID: 147 RVA: 0x000045F0 File Offset: 0x000027F0
		public void Reset()
		{
			this.NextBeforeStepIndex = 0;
			this.NextBeforeStepPc = 0;
			this.NextAfterStepIndex = 0;
			this.NextAfterStepPc = 0;
			this.TestHasRun = false;
			this.CurrentTestResultStatus = TestStatus.Inconclusive;
			this.CurrentTestResultLabel = null;
			this.CurrentTestResultSite = FailureSite.Test;
			this.CurrentTestMessage = null;
			this.CurrentTestStrackTrace = null;
			this.TestAfterStarted = false;
		}

		// Token: 0x06000094 RID: 148 RVA: 0x0000464C File Offset: 0x0000284C
		public void StoreTestResult(TestResult result)
		{
			this.CurrentTestResultStatus = result.ResultState.Status;
			this.CurrentTestResultLabel = result.ResultState.Label;
			this.CurrentTestResultSite = result.ResultState.Site;
			this.CurrentTestMessage = result.Message;
			this.CurrentTestStrackTrace = result.StackTrace;
		}

		// Token: 0x06000095 RID: 149 RVA: 0x000046A5 File Offset: 0x000028A5
		public void ApplyTestResult(TestResult result)
		{
			result.SetResult(new ResultState(this.CurrentTestResultStatus, this.CurrentTestResultLabel, this.CurrentTestResultSite), this.CurrentTestMessage, this.CurrentTestStrackTrace);
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x06000096 RID: 150 RVA: 0x000046D4 File Offset: 0x000028D4
		// (set) Token: 0x06000097 RID: 151 RVA: 0x000046EF File Offset: 0x000028EF
		public int NextSetUpStepIndex
		{
			get
			{
				return this.NextBeforeStepIndex;
			}
			set
			{
				this.NextBeforeStepIndex = value;
			}
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x06000098 RID: 152 RVA: 0x000046FC File Offset: 0x000028FC
		// (set) Token: 0x06000099 RID: 153 RVA: 0x00004717 File Offset: 0x00002917
		public int NextSetUpStepPc
		{
			get
			{
				return this.NextBeforeStepPc;
			}
			set
			{
				this.NextBeforeStepPc = value;
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x0600009A RID: 154 RVA: 0x00004724 File Offset: 0x00002924
		// (set) Token: 0x0600009B RID: 155 RVA: 0x0000473F File Offset: 0x0000293F
		public int NextTearDownStepIndex
		{
			get
			{
				return this.NextAfterStepIndex;
			}
			set
			{
				this.NextAfterStepIndex = value;
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x0600009C RID: 156 RVA: 0x0000474C File Offset: 0x0000294C
		// (set) Token: 0x0600009D RID: 157 RVA: 0x00004767 File Offset: 0x00002967
		public int NextTearDownStepPc
		{
			get
			{
				return this.NextAfterStepPc;
			}
			set
			{
				this.NextAfterStepPc = value;
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x0600009E RID: 158 RVA: 0x00004774 File Offset: 0x00002974
		// (set) Token: 0x0600009F RID: 159 RVA: 0x0000478F File Offset: 0x0000298F
		public bool TestTearDownStarted
		{
			get
			{
				return this.TestAfterStarted;
			}
			set
			{
				this.TestAfterStarted = value;
			}
		}

		// Token: 0x04000034 RID: 52
		public int NextBeforeStepIndex;

		// Token: 0x04000035 RID: 53
		public int NextBeforeStepPc;

		// Token: 0x04000036 RID: 54
		public int NextAfterStepIndex;

		// Token: 0x04000037 RID: 55
		public int NextAfterStepPc;

		// Token: 0x04000038 RID: 56
		public bool TestHasRun;

		// Token: 0x04000039 RID: 57
		public TestStatus CurrentTestResultStatus;

		// Token: 0x0400003A RID: 58
		public string CurrentTestResultLabel;

		// Token: 0x0400003B RID: 59
		public FailureSite CurrentTestResultSite;

		// Token: 0x0400003C RID: 60
		public string CurrentTestMessage;

		// Token: 0x0400003D RID: 61
		public string CurrentTestStrackTrace;

		// Token: 0x0400003E RID: 62
		public bool TestAfterStarted;
	}
}
