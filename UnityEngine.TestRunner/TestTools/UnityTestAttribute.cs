﻿using System;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Builders;
using NUnit.Framework.Internal.Commands;
using UnityEngine.TestRunner.NUnitExtensions.Runner;

namespace UnityEngine.TestTools
{
	// Token: 0x0200001A RID: 26
	[AttributeUsage(AttributeTargets.Method)]
	public class UnityTestAttribute : CombiningStrategyAttribute, IWrapSetUpTearDown, ISimpleTestBuilder, IImplyFixture, ICommandWrapper
	{
		// Token: 0x06000087 RID: 135 RVA: 0x00003CD9 File Offset: 0x00001ED9
		public UnityTestAttribute() : base(new UnityCombinatorialStrategy(), new ParameterDataSourceProvider())
		{
		}

		// Token: 0x06000088 RID: 136 RVA: 0x00003CF8 File Offset: 0x00001EF8
		TestMethod ISimpleTestBuilder.BuildFrom(IMethodInfo method, Test suite)
		{
			TestCaseParameters parms = new TestCaseParameters
			{
				ExpectedResult = new object(),
				HasExpectedResult = true
			};
			TestMethod testMethod = this._builder.BuildTestMethod(method, suite, parms);
			if (testMethod.parms != null)
			{
				testMethod.parms.HasExpectedResult = false;
			}
			return testMethod;
		}

		// Token: 0x06000089 RID: 137 RVA: 0x00003D50 File Offset: 0x00001F50
		public TestCommand Wrap(TestCommand command)
		{
			return new OuterUnityTestActionCommand(new EnumerableSetUpTearDownCommand(new SetUpTearDownCommand(new TestActionCommand(new UnityLogCheckDelegatingCommand(new EnumerableTestMethodCommand((TestMethod)command.Test))))));
		}

		// Token: 0x04000030 RID: 48
		private readonly NUnitTestCaseBuilder _builder = new NUnitTestCaseBuilder();
	}
}
