﻿using System;

namespace UnityEngine.TestTools.Utils
{
	// Token: 0x02000064 RID: 100
	internal interface IScriptingRuntimeProxy
	{
		// Token: 0x06000213 RID: 531
		string[] GetAllUserAssemblies();
	}
}
