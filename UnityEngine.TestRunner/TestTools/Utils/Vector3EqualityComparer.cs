﻿using System;
using System.Collections.Generic;

namespace UnityEngine.TestTools.Utils
{
	// Token: 0x0200005D RID: 93
	public class Vector3EqualityComparer : IEqualityComparer<Vector3>
	{
		// Token: 0x060001F8 RID: 504 RVA: 0x0000ABE2 File Offset: 0x00008DE2
		private Vector3EqualityComparer() : this(0.0001f)
		{
		}

		// Token: 0x060001F9 RID: 505 RVA: 0x0000ABF0 File Offset: 0x00008DF0
		public Vector3EqualityComparer(float allowedError)
		{
			this.AllowedError = allowedError;
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x060001FA RID: 506 RVA: 0x0000AC00 File Offset: 0x00008E00
		public static Vector3EqualityComparer Instance
		{
			get
			{
				return Vector3EqualityComparer.m_Instance;
			}
		}

		// Token: 0x060001FB RID: 507 RVA: 0x0000AC1C File Offset: 0x00008E1C
		public bool Equals(Vector3 expected, Vector3 actual)
		{
			return Utils.AreFloatsEqual(expected.x, actual.x, this.AllowedError) && Utils.AreFloatsEqual(expected.y, actual.y, this.AllowedError) && Utils.AreFloatsEqual(expected.z, actual.z, this.AllowedError);
		}

		// Token: 0x060001FC RID: 508 RVA: 0x0000AC8C File Offset: 0x00008E8C
		public int GetHashCode(Vector3 vec3)
		{
			return 0;
		}

		// Token: 0x060001FD RID: 509 RVA: 0x0000ACA2 File Offset: 0x00008EA2
		// Note: this type is marked as 'beforefieldinit'.
		static Vector3EqualityComparer()
		{
		}

		// Token: 0x040000FB RID: 251
		private const float k_DefaultError = 0.0001f;

		// Token: 0x040000FC RID: 252
		private readonly float AllowedError;

		// Token: 0x040000FD RID: 253
		private static readonly Vector3EqualityComparer m_Instance = new Vector3EqualityComparer();
	}
}
