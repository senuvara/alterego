﻿using System;
using NUnit.Framework.Interfaces;

namespace UnityEngine.TestTools.Utils
{
	// Token: 0x02000065 RID: 101
	internal interface ITestAssemblyProvider
	{
		// Token: 0x06000214 RID: 532
		ITest GetTestsWithNUnit();

		// Token: 0x06000215 RID: 533
		IAssemblyWrapper[] GetUserAssemblies();
	}
}
