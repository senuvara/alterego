﻿using System;

namespace UnityEngine.TestTools.Utils
{
	// Token: 0x02000059 RID: 89
	public static class Utils
	{
		// Token: 0x060001E4 RID: 484 RVA: 0x0000A95C File Offset: 0x00008B5C
		public static bool AreFloatsEqual(float expected, float actual, float allowedRelativeError)
		{
			return (Utils.IsFloatCloseToZero(actual) && Utils.IsFloatCloseToZero(expected)) || (expected == float.PositiveInfinity && actual == float.PositiveInfinity) || (expected == float.NegativeInfinity && actual == float.NegativeInfinity) || Math.Abs(actual - expected) < 0.0001f || Math.Abs((actual - expected) / expected) <= allowedRelativeError;
		}

		// Token: 0x060001E5 RID: 485 RVA: 0x0000A9E0 File Offset: 0x00008BE0
		private static bool IsFloatCloseToZero(float a)
		{
			return Math.Abs(a) < 0.0001f;
		}

		// Token: 0x060001E6 RID: 486 RVA: 0x0000AA04 File Offset: 0x00008C04
		public static bool AreFloatsEqualAbsoluteError(float expected, float actual, float allowedAbsoluteError)
		{
			return Math.Abs(actual - expected) <= allowedAbsoluteError;
		}

		// Token: 0x060001E7 RID: 487 RVA: 0x0000AA28 File Offset: 0x00008C28
		public static GameObject CreatePrimitive(PrimitiveType type)
		{
			GameObject gameObject = GameObject.CreatePrimitive(type);
			Renderer component = gameObject.GetComponent<Renderer>();
			if (component)
			{
				component.sharedMaterial = new Material(Shader.Find("VertexLit"));
			}
			return gameObject;
		}

		// Token: 0x040000F5 RID: 245
		private const float k_FEpsilon = 0.0001f;
	}
}
