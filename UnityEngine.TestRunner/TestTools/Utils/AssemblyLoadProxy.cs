﻿using System;
using System.Reflection;

namespace UnityEngine.TestTools.Utils
{
	// Token: 0x02000060 RID: 96
	internal class AssemblyLoadProxy : IAssemblyLoadProxy
	{
		// Token: 0x06000209 RID: 521 RVA: 0x0000ADFA File Offset: 0x00008FFA
		public AssemblyLoadProxy()
		{
		}

		// Token: 0x0600020A RID: 522 RVA: 0x0000AE04 File Offset: 0x00009004
		public IAssemblyWrapper Load(string assemblyString)
		{
			return new AssemblyWrapper(Assembly.Load(assemblyString));
		}
	}
}
