﻿using System;

namespace UnityEngine.TestTools.Utils
{
	// Token: 0x02000067 RID: 103
	internal class ScriptingRuntimeProxy : IScriptingRuntimeProxy
	{
		// Token: 0x0600021C RID: 540 RVA: 0x0000AFDA File Offset: 0x000091DA
		public ScriptingRuntimeProxy()
		{
		}

		// Token: 0x0600021D RID: 541 RVA: 0x0000AFE4 File Offset: 0x000091E4
		public string[] GetAllUserAssemblies()
		{
			return ScriptingRuntime.GetAllUserAssemblies();
		}
	}
}
