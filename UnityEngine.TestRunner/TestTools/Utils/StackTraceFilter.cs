﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace UnityEngine.TestTools.Utils
{
	// Token: 0x02000058 RID: 88
	internal static class StackTraceFilter
	{
		// Token: 0x060001E2 RID: 482 RVA: 0x0000A808 File Offset: 0x00008A08
		public static string Filter(string inputStackTrace)
		{
			foreach (string value in StackTraceFilter.s_LastMessages)
			{
				int num = inputStackTrace.IndexOf(value);
				if (num != -1)
				{
					inputStackTrace = inputStackTrace.Substring(0, num);
				}
			}
			string[] array2 = inputStackTrace.Split(new char[]
			{
				'\n'
			});
			StringBuilder stringBuilder = new StringBuilder();
			string[] array3 = array2;
			for (int j = 0; j < array3.Length; j++)
			{
				string line = array3[j];
				if (!StackTraceFilter.s_FilteredLogMessages.Any((string s) => line.StartsWith(s)))
				{
					stringBuilder.AppendLine(line);
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x060001E3 RID: 483 RVA: 0x0000A8E0 File Offset: 0x00008AE0
		// Note: this type is marked as 'beforefieldinit'.
		static StackTraceFilter()
		{
		}

		// Token: 0x040000F3 RID: 243
		private static readonly string[] s_FilteredLogMessages = new string[]
		{
			"UnityEngine.DebugLogHandler:Internal_Log",
			"UnityEngine.DebugLogHandler:Log",
			"UnityEngine.Logger:Log",
			"UnityEngine.Debug"
		};

		// Token: 0x040000F4 RID: 244
		private static readonly string[] s_LastMessages = new string[]
		{
			"System.Reflection.MonoMethod:InternalInvoke(Object, Object[], Exception&)",
			"UnityEditor.TestTools.TestRunner.EditModeRunner:InvokeDelegator"
		};

		// Token: 0x02000084 RID: 132
		[CompilerGenerated]
		private sealed class <Filter>c__AnonStorey0
		{
			// Token: 0x060002BD RID: 701 RVA: 0x0000A933 File Offset: 0x00008B33
			public <Filter>c__AnonStorey0()
			{
			}

			// Token: 0x060002BE RID: 702 RVA: 0x0000A93C File Offset: 0x00008B3C
			internal bool <>m__0(string s)
			{
				return this.line.StartsWith(s);
			}

			// Token: 0x0400019E RID: 414
			internal string line;
		}
	}
}
