﻿using System;
using System.Collections.Generic;

namespace UnityEngine.TestTools.Utils
{
	// Token: 0x0200005B RID: 91
	public class Vector2EqualityComparer : IEqualityComparer<Vector2>
	{
		// Token: 0x060001ED RID: 493 RVA: 0x0000AAD2 File Offset: 0x00008CD2
		private Vector2EqualityComparer() : this(0.0001f)
		{
		}

		// Token: 0x060001EE RID: 494 RVA: 0x0000AAE0 File Offset: 0x00008CE0
		public Vector2EqualityComparer(float error)
		{
			this.AllowedError = error;
		}

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x060001EF RID: 495 RVA: 0x0000AAF0 File Offset: 0x00008CF0
		public static Vector2EqualityComparer Instance
		{
			get
			{
				return Vector2EqualityComparer.m_Instance;
			}
		}

		// Token: 0x060001F0 RID: 496 RVA: 0x0000AB0C File Offset: 0x00008D0C
		public bool Equals(Vector2 expected, Vector2 actual)
		{
			return Utils.AreFloatsEqual(expected.x, actual.x, this.AllowedError) && Utils.AreFloatsEqual(expected.y, actual.y, this.AllowedError);
		}

		// Token: 0x060001F1 RID: 497 RVA: 0x0000AB5C File Offset: 0x00008D5C
		public int GetHashCode(Vector2 vec2)
		{
			return 0;
		}

		// Token: 0x060001F2 RID: 498 RVA: 0x0000AB72 File Offset: 0x00008D72
		// Note: this type is marked as 'beforefieldinit'.
		static Vector2EqualityComparer()
		{
		}

		// Token: 0x040000F7 RID: 247
		private const float k_DefaultError = 0.0001f;

		// Token: 0x040000F8 RID: 248
		private readonly float AllowedError;

		// Token: 0x040000F9 RID: 249
		private static readonly Vector2EqualityComparer m_Instance = new Vector2EqualityComparer();
	}
}
