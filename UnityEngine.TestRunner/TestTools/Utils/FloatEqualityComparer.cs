﻿using System;
using System.Collections.Generic;

namespace UnityEngine.TestTools.Utils
{
	// Token: 0x02000051 RID: 81
	public class FloatEqualityComparer : IEqualityComparer<float>
	{
		// Token: 0x060001CA RID: 458 RVA: 0x0000A64D File Offset: 0x0000884D
		private FloatEqualityComparer() : this(0.0001f)
		{
		}

		// Token: 0x060001CB RID: 459 RVA: 0x0000A65B File Offset: 0x0000885B
		public FloatEqualityComparer(float allowedError)
		{
			this.AllowedError = allowedError;
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x060001CC RID: 460 RVA: 0x0000A66C File Offset: 0x0000886C
		public static FloatEqualityComparer Instance
		{
			get
			{
				return FloatEqualityComparer.m_Instance;
			}
		}

		// Token: 0x060001CD RID: 461 RVA: 0x0000A688 File Offset: 0x00008888
		public bool Equals(float expected, float actual)
		{
			return Utils.AreFloatsEqual(expected, actual, this.AllowedError);
		}

		// Token: 0x060001CE RID: 462 RVA: 0x0000A6AC File Offset: 0x000088AC
		public int GetHashCode(float value)
		{
			return 0;
		}

		// Token: 0x060001CF RID: 463 RVA: 0x0000A6C2 File Offset: 0x000088C2
		// Note: this type is marked as 'beforefieldinit'.
		static FloatEqualityComparer()
		{
		}

		// Token: 0x040000EB RID: 235
		private const float k_DefaultError = 0.0001f;

		// Token: 0x040000EC RID: 236
		private readonly float AllowedError;

		// Token: 0x040000ED RID: 237
		private static readonly FloatEqualityComparer m_Instance = new FloatEqualityComparer();
	}
}
