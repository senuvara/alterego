﻿using System;
using System.Collections.Generic;

namespace UnityEngine.TestTools.Utils
{
	// Token: 0x0200005A RID: 90
	public class Vector2ComparerWithEqualsOperator : IEqualityComparer<Vector2>
	{
		// Token: 0x060001E8 RID: 488 RVA: 0x0000AA6C File Offset: 0x00008C6C
		private Vector2ComparerWithEqualsOperator()
		{
		}

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x060001E9 RID: 489 RVA: 0x0000AA78 File Offset: 0x00008C78
		public static Vector2ComparerWithEqualsOperator Instance
		{
			get
			{
				return Vector2ComparerWithEqualsOperator.m_Instance;
			}
		}

		// Token: 0x060001EA RID: 490 RVA: 0x0000AA94 File Offset: 0x00008C94
		public bool Equals(Vector2 expected, Vector2 actual)
		{
			return expected == actual;
		}

		// Token: 0x060001EB RID: 491 RVA: 0x0000AAB0 File Offset: 0x00008CB0
		public int GetHashCode(Vector2 vec2)
		{
			return 0;
		}

		// Token: 0x060001EC RID: 492 RVA: 0x0000AAC6 File Offset: 0x00008CC6
		// Note: this type is marked as 'beforefieldinit'.
		static Vector2ComparerWithEqualsOperator()
		{
		}

		// Token: 0x040000F6 RID: 246
		private static readonly Vector2ComparerWithEqualsOperator m_Instance = new Vector2ComparerWithEqualsOperator();
	}
}
