﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using NUnit.Framework.Internal;
using UnityEngine.TestRunner.NUnitExtensions.Runner;

namespace UnityEngine.TestTools.Utils
{
	// Token: 0x02000050 RID: 80
	internal class CoroutineRunner
	{
		// Token: 0x060001C3 RID: 451 RVA: 0x0000A13E File Offset: 0x0000833E
		public CoroutineRunner(MonoBehaviour playmodeTestsController, UnityTestExecutionContext context)
		{
			this.m_Controller = playmodeTestsController;
			this.m_Context = context;
		}

		// Token: 0x060001C4 RID: 452 RVA: 0x0000A158 File Offset: 0x00008358
		public IEnumerator HandleEnumerableTest(IEnumerator testEnumerator)
		{
			if (this.m_Context.TestCaseTimeout == 0)
			{
				this.m_Context.TestCaseTimeout = 30000;
			}
			for (;;)
			{
				if (!this.m_Running)
				{
					this.m_Running = true;
					this.m_TestCoroutine = this.ExMethod(testEnumerator, this.m_Context.TestCaseTimeout);
					this.m_Controller.StartCoroutine(this.m_TestCoroutine);
				}
				if (this.m_TestFailed)
				{
					break;
				}
				if (this.m_Context.ExecutionStatus == TestExecutionStatus.StopRequested || this.m_Context.ExecutionStatus == TestExecutionStatus.AbortRequested)
				{
					goto IL_102;
				}
				yield return null;
				if (!this.m_Running)
				{
					goto Block_5;
				}
			}
			this.StopAllRunningCoroutines();
			yield break;
			IL_102:
			this.StopAllRunningCoroutines();
			yield break;
			Block_5:
			yield break;
		}

		// Token: 0x060001C5 RID: 453 RVA: 0x0000A181 File Offset: 0x00008381
		private void StopAllRunningCoroutines()
		{
			if (this.m_TimeOutCoroutine != null)
			{
				this.m_Controller.StopCoroutine(this.m_TimeOutCoroutine);
			}
			if (this.m_TestCoroutine != null)
			{
				this.m_Controller.StopCoroutine(this.m_TestCoroutine);
			}
		}

		// Token: 0x060001C6 RID: 454 RVA: 0x0000A1C0 File Offset: 0x000083C0
		private IEnumerator ExMethod(IEnumerator e, int timeout)
		{
			this.m_TimeOutCoroutine = this.m_Controller.StartCoroutine(this.StartTimer(e, timeout, delegate
			{
				this.m_TestFailed = true;
				this.m_Timeout = true;
				this.m_Running = false;
			}));
			yield return this.m_Controller.StartCoroutine(e);
			this.m_Controller.StopCoroutine(this.m_TimeOutCoroutine);
			this.m_Running = false;
			yield break;
		}

		// Token: 0x060001C7 RID: 455 RVA: 0x0000A1F0 File Offset: 0x000083F0
		private IEnumerator StartTimer(IEnumerator coroutineToBeKilled, int timeout, Action onTimeout)
		{
			yield return new WaitForSeconds((float)timeout / 1000f);
			if (coroutineToBeKilled != null)
			{
				this.m_Controller.StopCoroutine(coroutineToBeKilled);
			}
			if (onTimeout != null)
			{
				onTimeout();
			}
			yield break;
		}

		// Token: 0x060001C8 RID: 456 RVA: 0x0000A228 File Offset: 0x00008428
		public bool HasFailedWithTimeout()
		{
			return this.m_Timeout;
		}

		// Token: 0x060001C9 RID: 457 RVA: 0x0000A244 File Offset: 0x00008444
		public int GetDefaultTimeout()
		{
			return 30000;
		}

		// Token: 0x040000E3 RID: 227
		private bool m_Running;

		// Token: 0x040000E4 RID: 228
		private bool m_TestFailed;

		// Token: 0x040000E5 RID: 229
		private bool m_Timeout;

		// Token: 0x040000E6 RID: 230
		private readonly MonoBehaviour m_Controller;

		// Token: 0x040000E7 RID: 231
		private readonly UnityTestExecutionContext m_Context;

		// Token: 0x040000E8 RID: 232
		private Coroutine m_TimeOutCoroutine;

		// Token: 0x040000E9 RID: 233
		private IEnumerator m_TestCoroutine;

		// Token: 0x040000EA RID: 234
		internal const int k_DefaultTimeout = 30000;

		// Token: 0x02000081 RID: 129
		[CompilerGenerated]
		private sealed class <HandleEnumerableTest>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x060002AA RID: 682 RVA: 0x0000A25E File Offset: 0x0000845E
			[DebuggerHidden]
			public <HandleEnumerableTest>c__Iterator0()
			{
			}

			// Token: 0x060002AB RID: 683 RVA: 0x0000A268 File Offset: 0x00008468
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					if (this.m_Context.TestCaseTimeout == 0)
					{
						this.m_Context.TestCaseTimeout = 30000;
					}
					break;
				case 1U:
					if (!this.m_Running)
					{
						this.$PC = -1;
						return false;
					}
					break;
				default:
					return false;
				}
				if (!this.m_Running)
				{
					this.m_Running = true;
					this.m_TestCoroutine = base.ExMethod(testEnumerator, this.m_Context.TestCaseTimeout);
					this.m_Controller.StartCoroutine(this.m_TestCoroutine);
				}
				if (this.m_TestFailed)
				{
					base.StopAllRunningCoroutines();
				}
				else
				{
					if (this.m_Context.ExecutionStatus != TestExecutionStatus.StopRequested && this.m_Context.ExecutionStatus != TestExecutionStatus.AbortRequested)
					{
						this.$current = null;
						if (!this.$disposing)
						{
							this.$PC = 1;
						}
						return true;
					}
					base.StopAllRunningCoroutines();
				}
				return false;
			}

			// Token: 0x17000095 RID: 149
			// (get) Token: 0x060002AC RID: 684 RVA: 0x0000A3C0 File Offset: 0x000085C0
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x17000096 RID: 150
			// (get) Token: 0x060002AD RID: 685 RVA: 0x0000A3DC File Offset: 0x000085DC
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x060002AE RID: 686 RVA: 0x0000A3F6 File Offset: 0x000085F6
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x060002AF RID: 687 RVA: 0x0000A406 File Offset: 0x00008606
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x0400018C RID: 396
			internal IEnumerator testEnumerator;

			// Token: 0x0400018D RID: 397
			internal CoroutineRunner $this;

			// Token: 0x0400018E RID: 398
			internal object $current;

			// Token: 0x0400018F RID: 399
			internal bool $disposing;

			// Token: 0x04000190 RID: 400
			internal int $PC;
		}

		// Token: 0x02000082 RID: 130
		[CompilerGenerated]
		private sealed class <ExMethod>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x060002B0 RID: 688 RVA: 0x0000A40D File Offset: 0x0000860D
			[DebuggerHidden]
			public <ExMethod>c__Iterator1()
			{
			}

			// Token: 0x060002B1 RID: 689 RVA: 0x0000A418 File Offset: 0x00008618
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					this.m_TimeOutCoroutine = this.m_Controller.StartCoroutine(base.StartTimer(e, timeout, delegate
					{
						this.m_TestFailed = true;
						this.m_Timeout = true;
						this.m_Running = false;
					}));
					this.$current = this.m_Controller.StartCoroutine(e);
					if (!this.$disposing)
					{
						this.$PC = 1;
					}
					return true;
				case 1U:
					this.m_Controller.StopCoroutine(this.m_TimeOutCoroutine);
					this.m_Running = false;
					this.$PC = -1;
					break;
				}
				return false;
			}

			// Token: 0x17000097 RID: 151
			// (get) Token: 0x060002B2 RID: 690 RVA: 0x0000A4E8 File Offset: 0x000086E8
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x17000098 RID: 152
			// (get) Token: 0x060002B3 RID: 691 RVA: 0x0000A504 File Offset: 0x00008704
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x060002B4 RID: 692 RVA: 0x0000A51E File Offset: 0x0000871E
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x060002B5 RID: 693 RVA: 0x0000A52E File Offset: 0x0000872E
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x060002B6 RID: 694 RVA: 0x0000A535 File Offset: 0x00008735
			internal void <>m__0()
			{
				this.m_TestFailed = true;
				this.m_Timeout = true;
				this.m_Running = false;
			}

			// Token: 0x04000191 RID: 401
			internal IEnumerator e;

			// Token: 0x04000192 RID: 402
			internal int timeout;

			// Token: 0x04000193 RID: 403
			internal CoroutineRunner $this;

			// Token: 0x04000194 RID: 404
			internal object $current;

			// Token: 0x04000195 RID: 405
			internal bool $disposing;

			// Token: 0x04000196 RID: 406
			internal int $PC;
		}

		// Token: 0x02000083 RID: 131
		[CompilerGenerated]
		private sealed class <StartTimer>c__Iterator2 : IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x060002B7 RID: 695 RVA: 0x0000A55C File Offset: 0x0000875C
			[DebuggerHidden]
			public <StartTimer>c__Iterator2()
			{
			}

			// Token: 0x060002B8 RID: 696 RVA: 0x0000A564 File Offset: 0x00008764
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					this.$current = new WaitForSeconds((float)timeout / 1000f);
					if (!this.$disposing)
					{
						this.$PC = 1;
					}
					return true;
				case 1U:
					if (coroutineToBeKilled != null)
					{
						this.m_Controller.StopCoroutine(coroutineToBeKilled);
					}
					if (onTimeout != null)
					{
						onTimeout();
					}
					this.$PC = -1;
					break;
				}
				return false;
			}

			// Token: 0x17000099 RID: 153
			// (get) Token: 0x060002B9 RID: 697 RVA: 0x0000A600 File Offset: 0x00008800
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x1700009A RID: 154
			// (get) Token: 0x060002BA RID: 698 RVA: 0x0000A61C File Offset: 0x0000881C
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x060002BB RID: 699 RVA: 0x0000A636 File Offset: 0x00008836
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x060002BC RID: 700 RVA: 0x0000A646 File Offset: 0x00008846
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x04000197 RID: 407
			internal int timeout;

			// Token: 0x04000198 RID: 408
			internal IEnumerator coroutineToBeKilled;

			// Token: 0x04000199 RID: 409
			internal Action onTimeout;

			// Token: 0x0400019A RID: 410
			internal CoroutineRunner $this;

			// Token: 0x0400019B RID: 411
			internal object $current;

			// Token: 0x0400019C RID: 412
			internal bool $disposing;

			// Token: 0x0400019D RID: 413
			internal int $PC;
		}
	}
}
