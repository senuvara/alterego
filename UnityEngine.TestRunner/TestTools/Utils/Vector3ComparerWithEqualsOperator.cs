﻿using System;
using System.Collections.Generic;

namespace UnityEngine.TestTools.Utils
{
	// Token: 0x0200005C RID: 92
	public class Vector3ComparerWithEqualsOperator : IEqualityComparer<Vector3>
	{
		// Token: 0x060001F3 RID: 499 RVA: 0x0000AB7E File Offset: 0x00008D7E
		private Vector3ComparerWithEqualsOperator()
		{
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x060001F4 RID: 500 RVA: 0x0000AB88 File Offset: 0x00008D88
		public static Vector3ComparerWithEqualsOperator Instance
		{
			get
			{
				return Vector3ComparerWithEqualsOperator.m_Instance;
			}
		}

		// Token: 0x060001F5 RID: 501 RVA: 0x0000ABA4 File Offset: 0x00008DA4
		public bool Equals(Vector3 expected, Vector3 actual)
		{
			return expected == actual;
		}

		// Token: 0x060001F6 RID: 502 RVA: 0x0000ABC0 File Offset: 0x00008DC0
		public int GetHashCode(Vector3 vec3)
		{
			return 0;
		}

		// Token: 0x060001F7 RID: 503 RVA: 0x0000ABD6 File Offset: 0x00008DD6
		// Note: this type is marked as 'beforefieldinit'.
		static Vector3ComparerWithEqualsOperator()
		{
		}

		// Token: 0x040000FA RID: 250
		private static readonly Vector3ComparerWithEqualsOperator m_Instance = new Vector3ComparerWithEqualsOperator();
	}
}
