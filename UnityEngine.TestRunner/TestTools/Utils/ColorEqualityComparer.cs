﻿using System;
using System.Collections.Generic;

namespace UnityEngine.TestTools.Utils
{
	// Token: 0x0200004F RID: 79
	public class ColorEqualityComparer : IEqualityComparer<Color>
	{
		// Token: 0x060001BD RID: 445 RVA: 0x0000A056 File Offset: 0x00008256
		private ColorEqualityComparer() : this(0.01f)
		{
		}

		// Token: 0x060001BE RID: 446 RVA: 0x0000A064 File Offset: 0x00008264
		public ColorEqualityComparer(float error)
		{
			this.AllowedError = error;
		}

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x060001BF RID: 447 RVA: 0x0000A074 File Offset: 0x00008274
		public static ColorEqualityComparer Instance
		{
			get
			{
				return ColorEqualityComparer.m_Instance;
			}
		}

		// Token: 0x060001C0 RID: 448 RVA: 0x0000A090 File Offset: 0x00008290
		public bool Equals(Color expected, Color actual)
		{
			return Utils.AreFloatsEqualAbsoluteError(expected.r, actual.r, this.AllowedError) && Utils.AreFloatsEqualAbsoluteError(expected.g, actual.g, this.AllowedError) && Utils.AreFloatsEqualAbsoluteError(expected.b, actual.b, this.AllowedError) && Utils.AreFloatsEqualAbsoluteError(expected.a, actual.a, this.AllowedError);
		}

		// Token: 0x060001C1 RID: 449 RVA: 0x0000A11C File Offset: 0x0000831C
		public int GetHashCode(Color color)
		{
			return 0;
		}

		// Token: 0x060001C2 RID: 450 RVA: 0x0000A132 File Offset: 0x00008332
		// Note: this type is marked as 'beforefieldinit'.
		static ColorEqualityComparer()
		{
		}

		// Token: 0x040000E0 RID: 224
		private const float k_DefaultError = 0.01f;

		// Token: 0x040000E1 RID: 225
		private readonly float AllowedError;

		// Token: 0x040000E2 RID: 226
		private static readonly ColorEqualityComparer m_Instance = new ColorEqualityComparer();
	}
}
