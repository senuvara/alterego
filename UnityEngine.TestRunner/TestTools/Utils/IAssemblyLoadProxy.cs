﻿using System;

namespace UnityEngine.TestTools.Utils
{
	// Token: 0x02000062 RID: 98
	internal interface IAssemblyLoadProxy
	{
		// Token: 0x0600020F RID: 527
		IAssemblyWrapper Load(string assemblyString);
	}
}
