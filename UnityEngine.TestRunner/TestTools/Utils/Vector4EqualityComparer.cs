﻿using System;
using System.Collections.Generic;

namespace UnityEngine.TestTools.Utils
{
	// Token: 0x0200005F RID: 95
	public class Vector4EqualityComparer : IEqualityComparer<Vector4>
	{
		// Token: 0x06000203 RID: 515 RVA: 0x0000AD12 File Offset: 0x00008F12
		private Vector4EqualityComparer() : this(0.0001f)
		{
		}

		// Token: 0x06000204 RID: 516 RVA: 0x0000AD20 File Offset: 0x00008F20
		public Vector4EqualityComparer(float allowedError)
		{
			this.AllowedError = allowedError;
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x06000205 RID: 517 RVA: 0x0000AD30 File Offset: 0x00008F30
		public static Vector4EqualityComparer Instance
		{
			get
			{
				return Vector4EqualityComparer.m_Instance;
			}
		}

		// Token: 0x06000206 RID: 518 RVA: 0x0000AD4C File Offset: 0x00008F4C
		public bool Equals(Vector4 expected, Vector4 actual)
		{
			return Utils.AreFloatsEqual(expected.x, actual.x, this.AllowedError) && Utils.AreFloatsEqual(expected.y, actual.y, this.AllowedError) && Utils.AreFloatsEqual(expected.z, actual.z, this.AllowedError) && Utils.AreFloatsEqual(expected.w, actual.w, this.AllowedError);
		}

		// Token: 0x06000207 RID: 519 RVA: 0x0000ADD8 File Offset: 0x00008FD8
		public int GetHashCode(Vector4 vec4)
		{
			return 0;
		}

		// Token: 0x06000208 RID: 520 RVA: 0x0000ADEE File Offset: 0x00008FEE
		// Note: this type is marked as 'beforefieldinit'.
		static Vector4EqualityComparer()
		{
		}

		// Token: 0x040000FF RID: 255
		private const float k_DefaultError = 0.0001f;

		// Token: 0x04000100 RID: 256
		private readonly float AllowedError;

		// Token: 0x04000101 RID: 257
		private static readonly Vector4EqualityComparer m_Instance = new Vector4EqualityComparer();
	}
}
