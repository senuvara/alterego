﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace UnityEngine.TestTools.Utils
{
	// Token: 0x02000061 RID: 97
	internal class AssemblyWrapper : IAssemblyWrapper
	{
		// Token: 0x0600020B RID: 523 RVA: 0x0000AE24 File Offset: 0x00009024
		public AssemblyWrapper(Assembly assembly)
		{
			this.Assembly = assembly;
		}

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x0600020C RID: 524 RVA: 0x0000AE34 File Offset: 0x00009034
		public Assembly Assembly
		{
			[CompilerGenerated]
			get
			{
				return this.<Assembly>k__BackingField;
			}
		}

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x0600020D RID: 525 RVA: 0x0000AE4E File Offset: 0x0000904E
		public virtual string Location
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x0600020E RID: 526 RVA: 0x0000AE56 File Offset: 0x00009056
		public virtual AssemblyName[] GetReferencedAssemblies()
		{
			throw new NotImplementedException();
		}

		// Token: 0x04000102 RID: 258
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private readonly Assembly <Assembly>k__BackingField;
	}
}
