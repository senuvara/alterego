﻿using System;
using System.Collections.Generic;

namespace UnityEngine.TestTools.Utils
{
	// Token: 0x02000057 RID: 87
	public class QuaternionEqualityComparer : IEqualityComparer<Quaternion>
	{
		// Token: 0x060001DC RID: 476 RVA: 0x0000A777 File Offset: 0x00008977
		private QuaternionEqualityComparer() : this(1E-05f)
		{
		}

		// Token: 0x060001DD RID: 477 RVA: 0x0000A785 File Offset: 0x00008985
		public QuaternionEqualityComparer(float allowedError)
		{
			this.AllowedError = allowedError;
		}

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x060001DE RID: 478 RVA: 0x0000A798 File Offset: 0x00008998
		public static QuaternionEqualityComparer Instance
		{
			get
			{
				return QuaternionEqualityComparer.m_Instance;
			}
		}

		// Token: 0x060001DF RID: 479 RVA: 0x0000A7B4 File Offset: 0x000089B4
		public bool Equals(Quaternion expected, Quaternion actual)
		{
			return Mathf.Abs(Quaternion.Dot(expected, actual)) > 1f - this.AllowedError;
		}

		// Token: 0x060001E0 RID: 480 RVA: 0x0000A7E4 File Offset: 0x000089E4
		public int GetHashCode(Quaternion quaternion)
		{
			return 0;
		}

		// Token: 0x060001E1 RID: 481 RVA: 0x0000A7FA File Offset: 0x000089FA
		// Note: this type is marked as 'beforefieldinit'.
		static QuaternionEqualityComparer()
		{
		}

		// Token: 0x040000F0 RID: 240
		private const float k_DefaultError = 1E-05f;

		// Token: 0x040000F1 RID: 241
		private readonly float AllowedError;

		// Token: 0x040000F2 RID: 242
		private static readonly QuaternionEqualityComparer m_Instance = new QuaternionEqualityComparer();
	}
}
