﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using NUnit.Framework.Interfaces;
using UnityEngine.TestTools.NUnitExtensions;

namespace UnityEngine.TestTools.Utils
{
	// Token: 0x02000066 RID: 102
	internal class PlayerTestAssemblyProvider
	{
		// Token: 0x06000216 RID: 534 RVA: 0x0000AE5E File Offset: 0x0000905E
		internal PlayerTestAssemblyProvider(IAssemblyLoadProxy assemblyLoadProxy, List<string> assembliesToLoad)
		{
			this.m_AssemblyLoadProxy = assemblyLoadProxy;
			this.m_AssembliesToLoad = assembliesToLoad;
			this.LoadAssemblies();
		}

		// Token: 0x06000217 RID: 535 RVA: 0x0000AE7C File Offset: 0x0000907C
		public ITest GetTestsWithNUnit()
		{
			return PlayerTestAssemblyProvider.BuildTests(TestPlatform.PlayMode, PlayerTestAssemblyProvider.m_LoadedAssemblies.ToArray());
		}

		// Token: 0x06000218 RID: 536 RVA: 0x0000AEA4 File Offset: 0x000090A4
		public List<IAssemblyWrapper> GetUserAssemblies()
		{
			return PlayerTestAssemblyProvider.m_LoadedAssemblies;
		}

		// Token: 0x06000219 RID: 537 RVA: 0x0000AEC0 File Offset: 0x000090C0
		protected static ITest BuildTests(TestPlatform testPlatform, IAssemblyWrapper[] assemblies)
		{
			Dictionary<string, object> nunitTestBuilderSettings = UnityTestAssemblyBuilder.GetNUnitTestBuilderSettings(testPlatform);
			UnityTestAssemblyBuilder nunitTestBuilder = UnityTestAssemblyBuilder.GetNUnitTestBuilder(testPlatform);
			return nunitTestBuilder.Build((from a in assemblies
			select a.Assembly).ToArray<Assembly>(), nunitTestBuilderSettings);
		}

		// Token: 0x0600021A RID: 538 RVA: 0x0000AF14 File Offset: 0x00009114
		private void LoadAssemblies()
		{
			if (PlayerTestAssemblyProvider.m_LoadedAssemblies == null)
			{
				PlayerTestAssemblyProvider.m_LoadedAssemblies = new List<IAssemblyWrapper>();
				foreach (string assemblyString in this.m_AssembliesToLoad)
				{
					IAssemblyWrapper assemblyWrapper;
					try
					{
						assemblyWrapper = this.m_AssemblyLoadProxy.Load(assemblyString);
					}
					catch (FileNotFoundException)
					{
						continue;
					}
					if (assemblyWrapper != null)
					{
						PlayerTestAssemblyProvider.m_LoadedAssemblies.Add(assemblyWrapper);
					}
				}
			}
		}

		// Token: 0x0600021B RID: 539 RVA: 0x0000AFC0 File Offset: 0x000091C0
		[CompilerGenerated]
		private static Assembly <BuildTests>m__0(IAssemblyWrapper a)
		{
			return a.Assembly;
		}

		// Token: 0x04000103 RID: 259
		private IAssemblyLoadProxy m_AssemblyLoadProxy;

		// Token: 0x04000104 RID: 260
		private readonly List<string> m_AssembliesToLoad;

		// Token: 0x04000105 RID: 261
		private static List<IAssemblyWrapper> m_LoadedAssemblies;

		// Token: 0x04000106 RID: 262
		[CompilerGenerated]
		private static Func<IAssemblyWrapper, Assembly> <>f__am$cache0;
	}
}
