﻿using System;
using System.Reflection;

namespace UnityEngine.TestTools.Utils
{
	// Token: 0x02000063 RID: 99
	internal interface IAssemblyWrapper
	{
		// Token: 0x1700006D RID: 109
		// (get) Token: 0x06000210 RID: 528
		Assembly Assembly { get; }

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x06000211 RID: 529
		string Location { get; }

		// Token: 0x06000212 RID: 530
		AssemblyName[] GetReferencedAssemblies();
	}
}
