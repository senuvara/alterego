﻿using System;
using System.Collections.Generic;

namespace UnityEngine.TestTools.Utils
{
	// Token: 0x0200005E RID: 94
	public class Vector4ComparerWithEqualsOperator : IEqualityComparer<Vector4>
	{
		// Token: 0x060001FE RID: 510 RVA: 0x0000ACAE File Offset: 0x00008EAE
		private Vector4ComparerWithEqualsOperator()
		{
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x060001FF RID: 511 RVA: 0x0000ACB8 File Offset: 0x00008EB8
		public static Vector4ComparerWithEqualsOperator Instance
		{
			get
			{
				return Vector4ComparerWithEqualsOperator.m_Instance;
			}
		}

		// Token: 0x06000200 RID: 512 RVA: 0x0000ACD4 File Offset: 0x00008ED4
		public bool Equals(Vector4 expected, Vector4 actual)
		{
			return expected == actual;
		}

		// Token: 0x06000201 RID: 513 RVA: 0x0000ACF0 File Offset: 0x00008EF0
		public int GetHashCode(Vector4 vec4)
		{
			return 0;
		}

		// Token: 0x06000202 RID: 514 RVA: 0x0000AD06 File Offset: 0x00008F06
		// Note: this type is marked as 'beforefieldinit'.
		static Vector4ComparerWithEqualsOperator()
		{
		}

		// Token: 0x040000FE RID: 254
		private static readonly Vector4ComparerWithEqualsOperator m_Instance = new Vector4ComparerWithEqualsOperator();
	}
}
