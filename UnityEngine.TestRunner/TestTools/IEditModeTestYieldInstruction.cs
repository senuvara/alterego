﻿using System;
using System.Collections;

namespace UnityEngine.TestTools
{
	// Token: 0x02000049 RID: 73
	public interface IEditModeTestYieldInstruction
	{
		// Token: 0x17000059 RID: 89
		// (get) Token: 0x060001A9 RID: 425
		bool ExpectDomainReload { get; }

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x060001AA RID: 426
		bool ExpectedPlaymodeState { get; }

		// Token: 0x060001AB RID: 427
		IEnumerator Perform();
	}
}
