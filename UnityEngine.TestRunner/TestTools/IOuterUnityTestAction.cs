﻿using System;
using System.Collections;
using NUnit.Framework.Interfaces;

namespace UnityEngine.TestTools
{
	// Token: 0x02000052 RID: 82
	public interface IOuterUnityTestAction
	{
		// Token: 0x060001D0 RID: 464
		IEnumerator BeforeTest(ITest test);

		// Token: 0x060001D1 RID: 465
		IEnumerator AfterTest(ITest test);
	}
}
