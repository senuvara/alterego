﻿using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal.Builders;

namespace UnityEngine.TestTools
{
	// Token: 0x02000016 RID: 22
	internal class UnityCombinatorialStrategy : CombinatorialStrategy, ICombiningStrategy
	{
		// Token: 0x06000079 RID: 121 RVA: 0x000039C9 File Offset: 0x00001BC9
		public UnityCombinatorialStrategy()
		{
		}

		// Token: 0x0600007A RID: 122 RVA: 0x000039D4 File Offset: 0x00001BD4
		public new IEnumerable<ITestCaseData> GetTestCases(IEnumerable[] sources)
		{
			IEnumerable<ITestCaseData> testCases = base.GetTestCases(sources);
			foreach (ITestCaseData testCaseData in testCases)
			{
				testCaseData.GetType().GetProperty("ExpectedResult").SetValue(testCaseData, new object(), null);
			}
			return testCases;
		}
	}
}
