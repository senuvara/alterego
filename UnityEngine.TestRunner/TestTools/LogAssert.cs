﻿using System;
using System.Text.RegularExpressions;
using UnityEngine.TestTools.Logging;
using UnityEngine.TestTools.TestRunner;

namespace UnityEngine.TestTools
{
	// Token: 0x02000007 RID: 7
	public static class LogAssert
	{
		// Token: 0x0600000D RID: 13 RVA: 0x000022A0 File Offset: 0x000004A0
		public static void Expect(LogType type, string message)
		{
			LogScope.Current.ExpectedLogs.Enqueue(new LogMatch
			{
				LogType = new LogType?(type),
				Message = message
			});
		}

		// Token: 0x0600000E RID: 14 RVA: 0x000022D8 File Offset: 0x000004D8
		public static void Expect(LogType type, Regex message)
		{
			LogScope.Current.ExpectedLogs.Enqueue(new LogMatch
			{
				LogType = new LogType?(type),
				MessageRegex = message
			});
		}

		// Token: 0x0600000F RID: 15 RVA: 0x00002310 File Offset: 0x00000510
		public static void NoUnexpectedReceived()
		{
			LogScope.Current.ProcessExpectedLogs();
			bool flag = LogScope.Current.IsAllLogsHandled();
			if (flag)
			{
				return;
			}
			LogEvent unhandledLog = LogScope.Current.GetUnhandledLog();
			throw new UnhandledLogMessageException(unhandledLog);
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000010 RID: 16 RVA: 0x00002354 File Offset: 0x00000554
		// (set) Token: 0x06000011 RID: 17 RVA: 0x00002373 File Offset: 0x00000573
		public static bool ignoreFailingMessages
		{
			get
			{
				return LogScope.Current.IgnoreFailingMessages;
			}
			set
			{
				LogScope.Current.IgnoreFailingMessages = value;
			}
		}
	}
}
