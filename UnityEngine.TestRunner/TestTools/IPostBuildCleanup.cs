﻿using System;

namespace UnityEngine.TestTools
{
	// Token: 0x02000053 RID: 83
	public interface IPostBuildCleanup
	{
		// Token: 0x060001D2 RID: 466
		void Cleanup();
	}
}
