﻿using System;
using NUnit.Framework;

namespace UnityEngine.TestTools
{
	// Token: 0x02000019 RID: 25
	[AttributeUsage(AttributeTargets.Method)]
	public class UnityTearDownAttribute : NUnitAttribute
	{
		// Token: 0x06000086 RID: 134 RVA: 0x00003CD1 File Offset: 0x00001ED1
		public UnityTearDownAttribute()
		{
		}
	}
}
