﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Commands;
using UnityEngine.TestRunner.NUnitExtensions.Runner;
using UnityEngine.TestTools.Logging;
using UnityEngine.TestTools.TestRunner;

namespace UnityEngine.TestTools
{
	// Token: 0x0200001B RID: 27
	internal abstract class BeforeAfterTestCommandBase<T> : DelegatingTestCommand, IEnumerableTestMethodCommand
	{
		// Token: 0x0600008A RID: 138 RVA: 0x00003D8E File Offset: 0x00001F8E
		protected BeforeAfterTestCommandBase(TestCommand innerCommand) : base(innerCommand)
		{
		}

		// Token: 0x0600008B RID: 139
		protected abstract IEnumerator InvokeBefore(T action, Test test, UnityTestExecutionContext context);

		// Token: 0x0600008C RID: 140
		protected abstract IEnumerator InvokeAfter(T action, Test test, UnityTestExecutionContext context);

		// Token: 0x0600008D RID: 141
		protected abstract BeforeAfterTestCommandState GetState(UnityTestExecutionContext context);

		// Token: 0x0600008E RID: 142 RVA: 0x00003DB0 File Offset: 0x00001FB0
		public IEnumerable ExecuteEnumerable(ITestExecutionContext context)
		{
			UnityTestExecutionContext unityContext = (UnityTestExecutionContext)context;
			BeforeAfterTestCommandState state = this.GetState(unityContext);
			if (state == null)
			{
				state = ScriptableObject.CreateInstance<BeforeAfterTestCommandState>();
			}
			state.ApplyTestResult(context.CurrentResult);
			while (state.NextBeforeStepIndex < this.BeforeActions.Length)
			{
				T action = this.BeforeActions[state.NextBeforeStepIndex];
				IEnumerator enumerator = this.InvokeBefore(action, base.Test, unityContext);
				BeforeAfterTestCommandBase<T>.ActivePcHelper.SetEnumeratorPC(enumerator, state.NextBeforeStepPc);
				using (LogScope logScope = new LogScope())
				{
					for (;;)
					{
						try
						{
							if (!enumerator.MoveNext())
							{
								break;
							}
						}
						catch (Exception ex)
						{
							state.TestHasRun = true;
							context.CurrentResult.RecordException(ex);
							state.StoreTestResult(context.CurrentResult);
							break;
						}
						state.NextBeforeStepPc = BeforeAfterTestCommandBase<T>.ActivePcHelper.GetEnumeratorPC(enumerator);
						state.StoreTestResult(context.CurrentResult);
						yield return enumerator.Current;
					}
					if (logScope.AnyFailingLogs())
					{
						state.TestHasRun = true;
						context.CurrentResult.RecordException(new UnhandledLogMessageException(logScope.FailingLogs.First<LogEvent>()));
						state.StoreTestResult(context.CurrentResult);
					}
				}
				state.NextBeforeStepIndex++;
				state.NextBeforeStepPc = 0;
			}
			if (!state.TestHasRun)
			{
				if (this.innerCommand is IEnumerableTestMethodCommand)
				{
					IEnumerable executeEnumerable = ((IEnumerableTestMethodCommand)this.innerCommand).ExecuteEnumerable(context);
					IEnumerator enumerator3 = executeEnumerable.GetEnumerator();
					try
					{
						while (enumerator3.MoveNext())
						{
							object iterator = enumerator3.Current;
							state.StoreTestResult(context.CurrentResult);
							yield return iterator;
						}
					}
					finally
					{
						IDisposable disposable;
						if ((disposable = (enumerator3 as IDisposable)) != null)
						{
							disposable.Dispose();
						}
					}
				}
				else
				{
					context.CurrentResult = this.innerCommand.Execute(context);
					state.StoreTestResult(context.CurrentResult);
				}
				state.TestHasRun = true;
			}
			while (state.NextAfterStepIndex < this.AfterActions.Length)
			{
				state.TestAfterStarted = true;
				T action2 = this.AfterActions[state.NextAfterStepIndex];
				IEnumerator enumerator2 = this.InvokeAfter(action2, base.Test, unityContext);
				BeforeAfterTestCommandBase<T>.ActivePcHelper.SetEnumeratorPC(enumerator2, state.NextAfterStepPc);
				using (LogScope logScope2 = new LogScope())
				{
					for (;;)
					{
						try
						{
							if (!enumerator2.MoveNext())
							{
								break;
							}
						}
						catch (Exception ex2)
						{
							context.CurrentResult.RecordException(ex2);
							state.StoreTestResult(context.CurrentResult);
							break;
						}
						state.NextAfterStepPc = BeforeAfterTestCommandBase<T>.ActivePcHelper.GetEnumeratorPC(enumerator2);
						state.StoreTestResult(context.CurrentResult);
						yield return enumerator2.Current;
					}
					if (logScope2.AnyFailingLogs())
					{
						state.TestHasRun = true;
						context.CurrentResult.RecordException(new UnhandledLogMessageException(logScope2.FailingLogs.First<LogEvent>()));
						state.StoreTestResult(context.CurrentResult);
					}
				}
				state.NextAfterStepIndex++;
				state.NextAfterStepPc = 0;
			}
			state.Reset();
			yield break;
		}

		// Token: 0x0600008F RID: 143 RVA: 0x00003DE1 File Offset: 0x00001FE1
		public override TestResult Execute(ITestExecutionContext context)
		{
			throw new NotImplementedException("Use ExecuteEnumerable");
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000090 RID: 144 RVA: 0x00003DF0 File Offset: 0x00001FF0
		// (set) Token: 0x06000091 RID: 145 RVA: 0x00003E20 File Offset: 0x00002020
		internal static TestCommandPcHelper ActivePcHelper
		{
			get
			{
				if (BeforeAfterTestCommandBase<T>.pcHelper == null)
				{
					BeforeAfterTestCommandBase<T>.pcHelper = new TestCommandPcHelper();
				}
				return BeforeAfterTestCommandBase<T>.pcHelper;
			}
			set
			{
				BeforeAfterTestCommandBase<T>.pcHelper = value;
			}
		}

		// Token: 0x04000031 RID: 49
		protected T[] BeforeActions = new T[0];

		// Token: 0x04000032 RID: 50
		protected T[] AfterActions = new T[0];

		// Token: 0x04000033 RID: 51
		private static TestCommandPcHelper pcHelper;

		// Token: 0x02000071 RID: 113
		[CompilerGenerated]
		private sealed class <ExecuteEnumerable>c__Iterator0 : IEnumerable, IEnumerable<object>, IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x06000236 RID: 566 RVA: 0x00003E29 File Offset: 0x00002029
			[DebuggerHidden]
			public <ExecuteEnumerable>c__Iterator0()
			{
			}

			// Token: 0x06000237 RID: 567 RVA: 0x00003E34 File Offset: 0x00002034
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				bool flag = false;
				switch (num)
				{
				case 0U:
					unityContext = (UnityTestExecutionContext)context;
					state = this.GetState(unityContext);
					if (state == null)
					{
						state = ScriptableObject.CreateInstance<BeforeAfterTestCommandState>();
					}
					state.ApplyTestResult(context.CurrentResult);
					break;
				case 1U:
					Block_3:
					try
					{
						switch (num)
						{
						}
						try
						{
							if (!enumerator.MoveNext())
							{
								goto IL_1C4;
							}
						}
						catch (Exception ex)
						{
							state.TestHasRun = true;
							context.CurrentResult.RecordException(ex);
							state.StoreTestResult(context.CurrentResult);
							goto IL_1C4;
						}
						state.NextBeforeStepPc = BeforeAfterTestCommandBase<T>.ActivePcHelper.GetEnumeratorPC(enumerator);
						state.StoreTestResult(context.CurrentResult);
						this.$current = enumerator.Current;
						if (!this.$disposing)
						{
							this.$PC = 1;
						}
						flag = true;
						return true;
						IL_1C4:
						if (logScope.AnyFailingLogs())
						{
							state.TestHasRun = true;
							context.CurrentResult.RecordException(new UnhandledLogMessageException(logScope.FailingLogs.First<LogEvent>()));
							state.StoreTestResult(context.CurrentResult);
						}
					}
					finally
					{
						if (!flag)
						{
							this.<>__Finally0();
						}
					}
					state.NextBeforeStepIndex++;
					state.NextBeforeStepPc = 0;
					break;
				case 2U:
					Block_7:
					try
					{
						switch (num)
						{
						}
						if (enumerator3.MoveNext())
						{
							iterator = enumerator3.Current;
							state.StoreTestResult(context.CurrentResult);
							this.$current = iterator;
							if (!this.$disposing)
							{
								this.$PC = 2;
							}
							flag = true;
							return true;
						}
					}
					finally
					{
						if (!flag)
						{
							if ((disposable = (enumerator3 as IDisposable)) != null)
							{
								disposable.Dispose();
							}
						}
					}
					goto IL_3A1;
				case 3U:
					Block_8:
					try
					{
						switch (num)
						{
						}
						try
						{
							if (!enumerator2.MoveNext())
							{
								goto IL_4EC;
							}
						}
						catch (Exception ex2)
						{
							context.CurrentResult.RecordException(ex2);
							state.StoreTestResult(context.CurrentResult);
							goto IL_4EC;
						}
						state.NextAfterStepPc = BeforeAfterTestCommandBase<T>.ActivePcHelper.GetEnumeratorPC(enumerator2);
						state.StoreTestResult(context.CurrentResult);
						this.$current = enumerator2.Current;
						if (!this.$disposing)
						{
							this.$PC = 3;
						}
						flag = true;
						return true;
						IL_4EC:
						if (logScope2.AnyFailingLogs())
						{
							state.TestHasRun = true;
							context.CurrentResult.RecordException(new UnhandledLogMessageException(logScope2.FailingLogs.First<LogEvent>()));
							state.StoreTestResult(context.CurrentResult);
						}
					}
					finally
					{
						if (!flag)
						{
							this.<>__Finally1();
						}
					}
					state.NextAfterStepIndex++;
					state.NextAfterStepPc = 0;
					goto IL_576;
				default:
					return false;
				}
				if (state.NextBeforeStepIndex < this.BeforeActions.Length)
				{
					action = this.BeforeActions[state.NextBeforeStepIndex];
					enumerator = this.InvokeBefore(action, base.Test, unityContext);
					BeforeAfterTestCommandBase<T>.ActivePcHelper.SetEnumeratorPC(enumerator, state.NextBeforeStepPc);
					logScope = new LogScope();
					num = 4294967293U;
					goto Block_3;
				}
				if (state.TestHasRun)
				{
					goto IL_3AE;
				}
				if (this.innerCommand is IEnumerableTestMethodCommand)
				{
					executeEnumerable = ((IEnumerableTestMethodCommand)this.innerCommand).ExecuteEnumerable(context);
					enumerator3 = executeEnumerable.GetEnumerator();
					num = 4294967293U;
					goto Block_7;
				}
				context.CurrentResult = this.innerCommand.Execute(context);
				state.StoreTestResult(context.CurrentResult);
				IL_3A1:
				state.TestHasRun = true;
				IL_3AE:
				IL_576:
				if (state.NextAfterStepIndex < this.AfterActions.Length)
				{
					state.TestAfterStarted = true;
					action2 = this.AfterActions[state.NextAfterStepIndex];
					enumerator2 = this.InvokeAfter(action2, base.Test, unityContext);
					BeforeAfterTestCommandBase<T>.ActivePcHelper.SetEnumeratorPC(enumerator2, state.NextAfterStepPc);
					logScope2 = new LogScope();
					num = 4294967293U;
					goto Block_8;
				}
				state.Reset();
				this.$PC = -1;
				return false;
			}

			// Token: 0x17000075 RID: 117
			// (get) Token: 0x06000238 RID: 568 RVA: 0x00004468 File Offset: 0x00002668
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x17000076 RID: 118
			// (get) Token: 0x06000239 RID: 569 RVA: 0x00004484 File Offset: 0x00002684
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x0600023A RID: 570 RVA: 0x000044A0 File Offset: 0x000026A0
			[DebuggerHidden]
			public void Dispose()
			{
				uint num = (uint)this.$PC;
				this.$disposing = true;
				this.$PC = -1;
				switch (num)
				{
				case 1U:
					try
					{
					}
					finally
					{
						this.<>__Finally0();
					}
					break;
				case 2U:
					try
					{
					}
					finally
					{
						if ((disposable = (enumerator3 as IDisposable)) != null)
						{
							disposable.Dispose();
						}
					}
					break;
				case 3U:
					try
					{
					}
					finally
					{
						this.<>__Finally1();
					}
					break;
				}
			}

			// Token: 0x0600023B RID: 571 RVA: 0x00004554 File Offset: 0x00002754
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x0600023C RID: 572 RVA: 0x0000455C File Offset: 0x0000275C
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<object>.GetEnumerator();
			}

			// Token: 0x0600023D RID: 573 RVA: 0x00004578 File Offset: 0x00002778
			[DebuggerHidden]
			IEnumerator<object> IEnumerable<object>.GetEnumerator()
			{
				if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
				{
					return this;
				}
				BeforeAfterTestCommandBase<T>.<ExecuteEnumerable>c__Iterator0 <ExecuteEnumerable>c__Iterator = new BeforeAfterTestCommandBase<T>.<ExecuteEnumerable>c__Iterator0();
				<ExecuteEnumerable>c__Iterator.$this = this;
				<ExecuteEnumerable>c__Iterator.context = context;
				return <ExecuteEnumerable>c__Iterator;
			}

			// Token: 0x0600023E RID: 574 RVA: 0x000045B8 File Offset: 0x000027B8
			private void <>__Finally0()
			{
				if (logScope != null)
				{
					((IDisposable)logScope).Dispose();
				}
			}

			// Token: 0x0600023F RID: 575 RVA: 0x000045D0 File Offset: 0x000027D0
			private void <>__Finally1()
			{
				if (logScope2 != null)
				{
					((IDisposable)logScope2).Dispose();
				}
			}

			// Token: 0x04000114 RID: 276
			internal ITestExecutionContext context;

			// Token: 0x04000115 RID: 277
			internal UnityTestExecutionContext <unityContext>__0;

			// Token: 0x04000116 RID: 278
			internal BeforeAfterTestCommandState <state>__0;

			// Token: 0x04000117 RID: 279
			internal T <action>__1;

			// Token: 0x04000118 RID: 280
			internal IEnumerator <enumerator>__1;

			// Token: 0x04000119 RID: 281
			internal LogScope <logScope>__2;

			// Token: 0x0400011A RID: 282
			internal IEnumerable <executeEnumerable>__3;

			// Token: 0x0400011B RID: 283
			internal IEnumerator $locvar0;

			// Token: 0x0400011C RID: 284
			internal object <iterator>__4;

			// Token: 0x0400011D RID: 285
			internal IDisposable $locvar1;

			// Token: 0x0400011E RID: 286
			internal T <action>__5;

			// Token: 0x0400011F RID: 287
			internal IEnumerator <enumerator>__5;

			// Token: 0x04000120 RID: 288
			internal LogScope <logScope>__6;

			// Token: 0x04000121 RID: 289
			internal BeforeAfterTestCommandBase<T> $this;

			// Token: 0x04000122 RID: 290
			internal object $current;

			// Token: 0x04000123 RID: 291
			internal bool $disposing;

			// Token: 0x04000124 RID: 292
			internal int $PC;
		}
	}
}
