﻿using System;
using System.Collections;

namespace UnityEngine.TestTools
{
	// Token: 0x02000023 RID: 35
	internal class TestCommandPcHelper
	{
		// Token: 0x060000B9 RID: 185 RVA: 0x00005329 File Offset: 0x00003529
		public TestCommandPcHelper()
		{
		}

		// Token: 0x060000BA RID: 186 RVA: 0x00005331 File Offset: 0x00003531
		public virtual void SetEnumeratorPC(IEnumerator enumerator, int pc)
		{
		}

		// Token: 0x060000BB RID: 187 RVA: 0x00005334 File Offset: 0x00003534
		public virtual int GetEnumeratorPC(IEnumerator enumerator)
		{
			return 0;
		}
	}
}
