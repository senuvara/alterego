﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace UnityEngine.TestTools
{
	// Token: 0x02000017 RID: 23
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	public class UnityPlatformAttribute : NUnitAttribute, IApplyToTest
	{
		// Token: 0x0600007B RID: 123 RVA: 0x00003A54 File Offset: 0x00001C54
		public UnityPlatformAttribute()
		{
			this.include = new List<RuntimePlatform>().ToArray();
			this.exclude = new List<RuntimePlatform>().ToArray();
		}

		// Token: 0x0600007C RID: 124 RVA: 0x00003A7D File Offset: 0x00001C7D
		public UnityPlatformAttribute(params RuntimePlatform[] include) : this()
		{
			this.include = include;
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x0600007D RID: 125 RVA: 0x00003A90 File Offset: 0x00001C90
		// (set) Token: 0x0600007E RID: 126 RVA: 0x00003AAA File Offset: 0x00001CAA
		public RuntimePlatform[] include
		{
			[CompilerGenerated]
			get
			{
				return this.<include>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<include>k__BackingField = value;
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x0600007F RID: 127 RVA: 0x00003AB4 File Offset: 0x00001CB4
		// (set) Token: 0x06000080 RID: 128 RVA: 0x00003ACE File Offset: 0x00001CCE
		public RuntimePlatform[] exclude
		{
			[CompilerGenerated]
			get
			{
				return this.<exclude>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<exclude>k__BackingField = value;
			}
		}

		// Token: 0x06000081 RID: 129 RVA: 0x00003AD8 File Offset: 0x00001CD8
		public void ApplyToTest(Test test)
		{
			if (test.RunState != RunState.NotRunnable && test.RunState != RunState.Ignored && !this.IsPlatformSupported(Application.platform))
			{
				test.RunState = RunState.Skipped;
				test.Properties.Add("_SKIPREASON", this.m_skippedReason);
			}
		}

		// Token: 0x06000082 RID: 130 RVA: 0x00003B30 File Offset: 0x00001D30
		internal bool IsPlatformSupported(RuntimePlatform testTargetPlatform)
		{
			bool result;
			if (this.include.Any<RuntimePlatform>() && !this.include.Any((RuntimePlatform x) => x == testTargetPlatform))
			{
				this.m_skippedReason = string.Format("Only supported on {0}", string.Join(", ", (from x in this.include
				select x.ToString()).ToArray<string>()));
				result = false;
			}
			else if (this.exclude.Any((RuntimePlatform x) => x == testTargetPlatform))
			{
				this.m_skippedReason = string.Format("Not supported on  {0}", string.Join(", ", (from x in this.include
				select x.ToString()).ToArray<string>()));
				result = false;
			}
			else
			{
				result = true;
			}
			return result;
		}

		// Token: 0x06000083 RID: 131 RVA: 0x00003C3C File Offset: 0x00001E3C
		[CompilerGenerated]
		private static string <IsPlatformSupported>m__0(RuntimePlatform x)
		{
			return x.ToString();
		}

		// Token: 0x06000084 RID: 132 RVA: 0x00003C60 File Offset: 0x00001E60
		[CompilerGenerated]
		private static string <IsPlatformSupported>m__1(RuntimePlatform x)
		{
			return x.ToString();
		}

		// Token: 0x0400002B RID: 43
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private RuntimePlatform[] <include>k__BackingField;

		// Token: 0x0400002C RID: 44
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private RuntimePlatform[] <exclude>k__BackingField;

		// Token: 0x0400002D RID: 45
		private string m_skippedReason;

		// Token: 0x0400002E RID: 46
		[CompilerGenerated]
		private static Func<RuntimePlatform, string> <>f__am$cache0;

		// Token: 0x0400002F RID: 47
		[CompilerGenerated]
		private static Func<RuntimePlatform, string> <>f__am$cache1;

		// Token: 0x02000070 RID: 112
		[CompilerGenerated]
		private sealed class <IsPlatformSupported>c__AnonStorey0
		{
			// Token: 0x06000233 RID: 563 RVA: 0x00003C81 File Offset: 0x00001E81
			public <IsPlatformSupported>c__AnonStorey0()
			{
			}

			// Token: 0x06000234 RID: 564 RVA: 0x00003C8C File Offset: 0x00001E8C
			internal bool <>m__0(RuntimePlatform x)
			{
				return x == this.testTargetPlatform;
			}

			// Token: 0x06000235 RID: 565 RVA: 0x00003CAC File Offset: 0x00001EAC
			internal bool <>m__1(RuntimePlatform x)
			{
				return x == this.testTargetPlatform;
			}

			// Token: 0x04000113 RID: 275
			internal RuntimePlatform testTargetPlatform;
		}
	}
}
