﻿using System;

namespace UnityEngine.TestTools
{
	// Token: 0x02000054 RID: 84
	public interface IPrebuildSetup
	{
		// Token: 0x060001D3 RID: 467
		void Setup();
	}
}
