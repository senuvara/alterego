﻿using System;

namespace UnityEngine.TestTools
{
	// Token: 0x02000068 RID: 104
	public interface IMonoBehaviourTest
	{
		// Token: 0x1700006F RID: 111
		// (get) Token: 0x0600021E RID: 542
		bool IsTestFinished { get; }
	}
}
