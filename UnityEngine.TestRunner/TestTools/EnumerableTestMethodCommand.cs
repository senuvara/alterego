﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Commands;
using UnityEngine.TestRunner.NUnitExtensions.Runner;
using UnityEngine.TestTools.TestRunner;

namespace UnityEngine.TestTools
{
	// Token: 0x0200001F RID: 31
	internal class EnumerableTestMethodCommand : TestCommand, IEnumerableTestMethodCommand
	{
		// Token: 0x060000A7 RID: 167 RVA: 0x000048FE File Offset: 0x00002AFE
		public EnumerableTestMethodCommand(TestMethod testMethod) : base(testMethod)
		{
			this.testMethod = testMethod;
		}

		// Token: 0x060000A8 RID: 168 RVA: 0x00004910 File Offset: 0x00002B10
		public IEnumerable ExecuteEnumerable(ITestExecutionContext context)
		{
			yield return null;
			IEnumerator currentExecutingTestEnumerator = new TestEnumeratorWrapper(this.testMethod).GetEnumerator(context);
			if (currentExecutingTestEnumerator != null)
			{
				TestEnumerator testEnumeraterYieldInstruction = new TestEnumerator(context, currentExecutingTestEnumerator);
				yield return testEnumeraterYieldInstruction;
				IEnumerator enumerator = testEnumeraterYieldInstruction.Execute();
				IEnumerator executingEnumerator = EnumerableTestMethodCommand.ExecuteEnumerableAndRecordExceptions(enumerator, context);
				while (executingEnumerator.MoveNext())
				{
					object obj = executingEnumerator.Current;
					yield return obj;
				}
			}
			else if (context.CurrentResult.ResultState != ResultState.Ignored)
			{
				context.CurrentResult.SetResult(ResultState.Success);
			}
			yield break;
		}

		// Token: 0x060000A9 RID: 169 RVA: 0x00004944 File Offset: 0x00002B44
		private static IEnumerator ExecuteEnumerableAndRecordExceptions(IEnumerator enumerator, ITestExecutionContext context)
		{
			for (;;)
			{
				try
				{
					if (!enumerator.MoveNext())
					{
						break;
					}
				}
				catch (Exception ex)
				{
					context.CurrentResult.RecordException(ex);
					break;
				}
				if (enumerator.Current is IEnumerator)
				{
					IEnumerator current = (IEnumerator)enumerator.Current;
					yield return EnumerableTestMethodCommand.ExecuteEnumerableAndRecordExceptions(current, context);
				}
				else
				{
					yield return enumerator.Current;
				}
			}
			yield break;
		}

		// Token: 0x060000AA RID: 170 RVA: 0x0000496D File Offset: 0x00002B6D
		public override TestResult Execute(ITestExecutionContext context)
		{
			throw new NotImplementedException("Use ExecuteEnumerable");
		}

		// Token: 0x04000040 RID: 64
		private readonly TestMethod testMethod;

		// Token: 0x02000072 RID: 114
		[CompilerGenerated]
		private sealed class <ExecuteEnumerable>c__Iterator0 : IEnumerable, IEnumerable<object>, IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x06000240 RID: 576 RVA: 0x0000497A File Offset: 0x00002B7A
			[DebuggerHidden]
			public <ExecuteEnumerable>c__Iterator0()
			{
			}

			// Token: 0x06000241 RID: 577 RVA: 0x00004984 File Offset: 0x00002B84
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					this.$current = null;
					if (!this.$disposing)
					{
						this.$PC = 1;
					}
					return true;
				case 1U:
					currentExecutingTestEnumerator = new TestEnumeratorWrapper(this.testMethod).GetEnumerator(context);
					if (currentExecutingTestEnumerator != null)
					{
						testEnumeraterYieldInstruction = new TestEnumerator(context, currentExecutingTestEnumerator);
						this.$current = testEnumeraterYieldInstruction;
						if (!this.$disposing)
						{
							this.$PC = 2;
						}
						return true;
					}
					if (context.CurrentResult.ResultState != ResultState.Ignored)
					{
						context.CurrentResult.SetResult(ResultState.Success);
					}
					goto IL_146;
				case 2U:
					enumerator = testEnumeraterYieldInstruction.Execute();
					executingEnumerator = EnumerableTestMethodCommand.ExecuteEnumerableAndRecordExceptions(enumerator, context);
					break;
				case 3U:
					break;
				default:
					return false;
				}
				if (executingEnumerator.MoveNext())
				{
					this.$current = executingEnumerator.Current;
					if (!this.$disposing)
					{
						this.$PC = 3;
					}
					return true;
				}
				IL_146:
				this.$PC = -1;
				return false;
			}

			// Token: 0x17000077 RID: 119
			// (get) Token: 0x06000242 RID: 578 RVA: 0x00004AE4 File Offset: 0x00002CE4
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x17000078 RID: 120
			// (get) Token: 0x06000243 RID: 579 RVA: 0x00004B00 File Offset: 0x00002D00
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x06000244 RID: 580 RVA: 0x00004B1A File Offset: 0x00002D1A
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x06000245 RID: 581 RVA: 0x00004B2A File Offset: 0x00002D2A
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x06000246 RID: 582 RVA: 0x00004B34 File Offset: 0x00002D34
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<object>.GetEnumerator();
			}

			// Token: 0x06000247 RID: 583 RVA: 0x00004B50 File Offset: 0x00002D50
			[DebuggerHidden]
			IEnumerator<object> IEnumerable<object>.GetEnumerator()
			{
				if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
				{
					return this;
				}
				EnumerableTestMethodCommand.<ExecuteEnumerable>c__Iterator0 <ExecuteEnumerable>c__Iterator = new EnumerableTestMethodCommand.<ExecuteEnumerable>c__Iterator0();
				<ExecuteEnumerable>c__Iterator.$this = this;
				<ExecuteEnumerable>c__Iterator.context = context;
				return <ExecuteEnumerable>c__Iterator;
			}

			// Token: 0x04000125 RID: 293
			internal ITestExecutionContext context;

			// Token: 0x04000126 RID: 294
			internal IEnumerator <currentExecutingTestEnumerator>__0;

			// Token: 0x04000127 RID: 295
			internal TestEnumerator <testEnumeraterYieldInstruction>__1;

			// Token: 0x04000128 RID: 296
			internal IEnumerator <enumerator>__1;

			// Token: 0x04000129 RID: 297
			internal IEnumerator <executingEnumerator>__1;

			// Token: 0x0400012A RID: 298
			internal EnumerableTestMethodCommand $this;

			// Token: 0x0400012B RID: 299
			internal object $current;

			// Token: 0x0400012C RID: 300
			internal bool $disposing;

			// Token: 0x0400012D RID: 301
			internal int $PC;
		}

		// Token: 0x02000073 RID: 115
		[CompilerGenerated]
		private sealed class <ExecuteEnumerableAndRecordExceptions>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x06000248 RID: 584 RVA: 0x00004B90 File Offset: 0x00002D90
			[DebuggerHidden]
			public <ExecuteEnumerableAndRecordExceptions>c__Iterator1()
			{
			}

			// Token: 0x06000249 RID: 585 RVA: 0x00004B98 File Offset: 0x00002D98
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					break;
				case 1U:
					break;
				case 2U:
					break;
				default:
					return false;
				}
				IL_26:
				try
				{
					if (!enumerator.MoveNext())
					{
						goto IL_E7;
					}
				}
				catch (Exception ex)
				{
					context.CurrentResult.RecordException(ex);
					goto IL_E7;
				}
				if (enumerator.Current is IEnumerator)
				{
					current = (IEnumerator)enumerator.Current;
					this.$current = EnumerableTestMethodCommand.ExecuteEnumerableAndRecordExceptions(current, context);
					if (!this.$disposing)
					{
						this.$PC = 1;
					}
				}
				else
				{
					this.$current = enumerator.Current;
					if (!this.$disposing)
					{
						this.$PC = 2;
					}
				}
				return true;
				IL_E7:
				this.$PC = -1;
				return false;
				goto IL_26;
			}

			// Token: 0x17000079 RID: 121
			// (get) Token: 0x0600024A RID: 586 RVA: 0x00004CA8 File Offset: 0x00002EA8
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x1700007A RID: 122
			// (get) Token: 0x0600024B RID: 587 RVA: 0x00004CC4 File Offset: 0x00002EC4
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x0600024C RID: 588 RVA: 0x00004CDE File Offset: 0x00002EDE
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x0600024D RID: 589 RVA: 0x00004CEE File Offset: 0x00002EEE
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x0400012E RID: 302
			internal IEnumerator enumerator;

			// Token: 0x0400012F RID: 303
			internal ITestExecutionContext context;

			// Token: 0x04000130 RID: 304
			internal IEnumerator <current>__1;

			// Token: 0x04000131 RID: 305
			internal object $current;

			// Token: 0x04000132 RID: 306
			internal bool $disposing;

			// Token: 0x04000133 RID: 307
			internal int $PC;
		}
	}
}
