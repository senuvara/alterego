﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.TestTools
{
	// Token: 0x02000055 RID: 85
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
	public class PostBuildCleanupAttribute : Attribute
	{
		// Token: 0x060001D4 RID: 468 RVA: 0x0000A6CE File Offset: 0x000088CE
		public PostBuildCleanupAttribute(Type targetClass)
		{
			this.TargetClass = targetClass;
		}

		// Token: 0x060001D5 RID: 469 RVA: 0x0000A6DE File Offset: 0x000088DE
		public PostBuildCleanupAttribute(string targetClassName)
		{
			this.TargetClass = AttributeHelper.GetTargetClassFromName(targetClassName, typeof(IPostBuildCleanup));
		}

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x060001D6 RID: 470 RVA: 0x0000A700 File Offset: 0x00008900
		// (set) Token: 0x060001D7 RID: 471 RVA: 0x0000A71A File Offset: 0x0000891A
		internal Type TargetClass
		{
			[CompilerGenerated]
			get
			{
				return this.<TargetClass>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<TargetClass>k__BackingField = value;
			}
		}

		// Token: 0x040000EE RID: 238
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Type <TargetClass>k__BackingField;
	}
}
