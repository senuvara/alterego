﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace UnityEngine.TestTools
{
	// Token: 0x02000015 RID: 21
	internal class TestEnumerator
	{
		// Token: 0x06000076 RID: 118 RVA: 0x00003813 File Offset: 0x00001A13
		public TestEnumerator(ITestExecutionContext context, IEnumerator testEnumerator)
		{
			this.m_Context = context;
			TestEnumerator.m_TestEnumerator = testEnumerator;
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000077 RID: 119 RVA: 0x0000382C File Offset: 0x00001A2C
		public static IEnumerator Enumerator
		{
			get
			{
				return TestEnumerator.m_TestEnumerator;
			}
		}

		// Token: 0x06000078 RID: 120 RVA: 0x00003848 File Offset: 0x00001A48
		public IEnumerator Execute()
		{
			this.m_Context.CurrentResult.SetResult(ResultState.Success);
			for (;;)
			{
				object current = null;
				try
				{
					if (!TestEnumerator.m_TestEnumerator.MoveNext())
					{
						yield break;
					}
					if (!this.m_Context.CurrentResult.ResultState.Equals(ResultState.Success))
					{
						yield break;
					}
					current = TestEnumerator.m_TestEnumerator.Current;
				}
				catch (Exception ex)
				{
					this.m_Context.CurrentResult.RecordException(ex);
					yield break;
				}
				yield return current;
			}
			yield break;
		}

		// Token: 0x04000029 RID: 41
		private readonly ITestExecutionContext m_Context;

		// Token: 0x0400002A RID: 42
		private static IEnumerator m_TestEnumerator;

		// Token: 0x0200006F RID: 111
		[CompilerGenerated]
		private sealed class <Execute>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
		{
			// Token: 0x0600022D RID: 557 RVA: 0x0000386A File Offset: 0x00001A6A
			[DebuggerHidden]
			public <Execute>c__Iterator0()
			{
			}

			// Token: 0x0600022E RID: 558 RVA: 0x00003874 File Offset: 0x00001A74
			public bool MoveNext()
			{
				uint num = (uint)this.$PC;
				this.$PC = -1;
				switch (num)
				{
				case 0U:
					this.m_Context.CurrentResult.SetResult(ResultState.Success);
					break;
				case 1U:
					break;
				default:
					return false;
				}
				current = null;
				try
				{
					if (!TestEnumerator.m_TestEnumerator.MoveNext())
					{
						return false;
					}
					if (!this.m_Context.CurrentResult.ResultState.Equals(ResultState.Success))
					{
						return false;
					}
					current = TestEnumerator.m_TestEnumerator.Current;
				}
				catch (Exception ex)
				{
					this.m_Context.CurrentResult.RecordException(ex);
					return false;
				}
				this.$current = current;
				if (!this.$disposing)
				{
					this.$PC = 1;
				}
				return true;
			}

			// Token: 0x17000073 RID: 115
			// (get) Token: 0x0600022F RID: 559 RVA: 0x0000397C File Offset: 0x00001B7C
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x17000074 RID: 116
			// (get) Token: 0x06000230 RID: 560 RVA: 0x00003998 File Offset: 0x00001B98
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.$current;
				}
			}

			// Token: 0x06000231 RID: 561 RVA: 0x000039B2 File Offset: 0x00001BB2
			[DebuggerHidden]
			public void Dispose()
			{
				this.$disposing = true;
				this.$PC = -1;
			}

			// Token: 0x06000232 RID: 562 RVA: 0x000039C2 File Offset: 0x00001BC2
			[DebuggerHidden]
			public void Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x0400010E RID: 270
			internal object <current>__1;

			// Token: 0x0400010F RID: 271
			internal TestEnumerator $this;

			// Token: 0x04000110 RID: 272
			internal object $current;

			// Token: 0x04000111 RID: 273
			internal bool $disposing;

			// Token: 0x04000112 RID: 274
			internal int $PC;
		}
	}
}
