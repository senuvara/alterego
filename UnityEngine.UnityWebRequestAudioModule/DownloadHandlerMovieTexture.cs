﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;

namespace UnityEngine.Networking
{
	// Token: 0x02000004 RID: 4
	[NativeHeader("Modules/UnityWebRequestAudio/Public/DownloadHandlerMovieTexture.h")]
	[NativeHeader("Runtime/Video/MovieTexture.h")]
	[Obsolete("MovieTexture is deprecated. Use VideoPlayer instead.", false)]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class DownloadHandlerMovieTexture : DownloadHandler
	{
		// Token: 0x0600000F RID: 15 RVA: 0x00002120 File Offset: 0x00000320
		public DownloadHandlerMovieTexture()
		{
		}
	}
}
