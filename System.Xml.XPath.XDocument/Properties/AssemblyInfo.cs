﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Xml.XPath;

[assembly: AssemblyVersion("4.1.1.0")]
[assembly: AssemblyTitle("System.Xml.XPath.XDocument")]
[assembly: AssemblyDescription("System.Xml.XPath.XDocument")]
[assembly: AssemblyDefaultAlias("System.Xml.XPath.XDocument")]
[assembly: AssemblyCompany("Mono development team")]
[assembly: AssemblyProduct("Mono Common Language Infrastructure")]
[assembly: AssemblyCopyright("(c) Various Mono authors")]
[assembly: AssemblyInformationalVersion("4.0.0.0")]
[assembly: AssemblyFileVersion("4.0.0.0")]
// System.Xml.Linq, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
[assembly: TypeForwardedTo(typeof(Extensions))]
