﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.Networking
{
	// Token: 0x02000002 RID: 2
	[NativeHeader("Modules/UnityWebRequestAudio/Public/DownloadHandlerAudioClip.h")]
	[NativeHeader("Modules/UnityWebRequestAudio/Public/DownloadHandlerMovieTexture.h")]
	internal static class WebRequestWWW
	{
		// Token: 0x06000001 RID: 1
		[FreeFunction("UnityWebRequestCreateAudioClip")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern AudioClip InternalCreateAudioClipUsingDH(DownloadHandler dh, string url, bool stream, bool compressed, AudioType audioType);
	}
}
