﻿using System;

namespace UnityEngine
{
	// Token: 0x02000004 RID: 4
	public static class WWWAudioExtensions
	{
		// Token: 0x0600002F RID: 47 RVA: 0x00002B08 File Offset: 0x00000D08
		[Obsolete("WWWAudioExtensions.GetAudioClip extension method has been replaced by WWW.GetAudioClip instance method. (UnityUpgradable) -> WWW.GetAudioClip()", true)]
		public static AudioClip GetAudioClip(this WWW www)
		{
			return www.GetAudioClip();
		}

		// Token: 0x06000030 RID: 48 RVA: 0x00002B24 File Offset: 0x00000D24
		[Obsolete("WWWAudioExtensions.GetAudioClip extension method has been replaced by WWW.GetAudioClip instance method. (UnityUpgradable) -> WWW.GetAudioClip([mscorlib] System.Boolean)", true)]
		public static AudioClip GetAudioClip(this WWW www, bool threeD)
		{
			return www.GetAudioClip(threeD);
		}

		// Token: 0x06000031 RID: 49 RVA: 0x00002B40 File Offset: 0x00000D40
		[Obsolete("WWWAudioExtensions.GetAudioClip extension method has been replaced by WWW.GetAudioClip instance method. (UnityUpgradable) -> WWW.GetAudioClip([mscorlib] System.Boolean, [mscorlib] System.Boolean)", true)]
		public static AudioClip GetAudioClip(this WWW www, bool threeD, bool stream)
		{
			return www.GetAudioClip(threeD, stream);
		}

		// Token: 0x06000032 RID: 50 RVA: 0x00002B60 File Offset: 0x00000D60
		[Obsolete("WWWAudioExtensions.GetAudioClip extension method has been replaced by WWW.GetAudioClip instance method. (UnityUpgradable) -> WWW.GetAudioClip([mscorlib] System.Boolean, [mscorlib] System.Boolean, UnityEngine.AudioType)", true)]
		public static AudioClip GetAudioClip(this WWW www, bool threeD, bool stream, AudioType audioType)
		{
			return www.GetAudioClip(threeD, stream, audioType);
		}

		// Token: 0x06000033 RID: 51 RVA: 0x00002B80 File Offset: 0x00000D80
		[Obsolete("WWWAudioExtensions.GetAudioClipCompressed extension method has been replaced by WWW.GetAudioClipCompressed instance method. (UnityUpgradable) -> WWW.GetAudioClipCompressed()", true)]
		public static AudioClip GetAudioClipCompressed(this WWW www)
		{
			return www.GetAudioClipCompressed();
		}

		// Token: 0x06000034 RID: 52 RVA: 0x00002B9C File Offset: 0x00000D9C
		[Obsolete("WWWAudioExtensions.GetAudioClipCompressed extension method has been replaced by WWW.GetAudioClipCompressed instance method. (UnityUpgradable) -> WWW.GetAudioClipCompressed([mscorlib] System.Boolean)", true)]
		public static AudioClip GetAudioClipCompressed(this WWW www, bool threeD)
		{
			return www.GetAudioClipCompressed(threeD);
		}

		// Token: 0x06000035 RID: 53 RVA: 0x00002BB8 File Offset: 0x00000DB8
		[Obsolete("WWWAudioExtensions.GetAudioClipCompressed extension method has been replaced by WWW.GetAudioClipCompressed instance method. (UnityUpgradable) -> WWW.GetAudioClipCompressed([mscorlib] System.Boolean, UnityEngine.AudioType)", true)]
		public static AudioClip GetAudioClipCompressed(this WWW www, bool threeD, AudioType audioType)
		{
			return www.GetAudioClipCompressed(threeD, audioType);
		}
	}
}
