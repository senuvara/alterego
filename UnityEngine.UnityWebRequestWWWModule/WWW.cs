﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine.Networking;

namespace UnityEngine
{
	// Token: 0x02000003 RID: 3
	[Obsolete("Use UnityWebRequest, a fully featured replacement which is more efficient and has additional features")]
	public class WWW : CustomYieldInstruction, IDisposable
	{
		// Token: 0x06000002 RID: 2 RVA: 0x00002050 File Offset: 0x00000250
		public WWW(string url)
		{
			this._uwr = UnityWebRequest.Get(url);
			this._uwr.SendWebRequest();
		}

		// Token: 0x06000003 RID: 3 RVA: 0x00002071 File Offset: 0x00000271
		public WWW(string url, WWWForm form)
		{
			this._uwr = UnityWebRequest.Post(url, form);
			this._uwr.chunkedTransfer = false;
			this._uwr.SendWebRequest();
		}

		// Token: 0x06000004 RID: 4 RVA: 0x000020A0 File Offset: 0x000002A0
		public WWW(string url, byte[] postData)
		{
			this._uwr = new UnityWebRequest(url, "POST");
			this._uwr.chunkedTransfer = false;
			UploadHandler uploadHandler = new UploadHandlerRaw(postData);
			uploadHandler.contentType = "application/x-www-form-urlencoded";
			this._uwr.uploadHandler = uploadHandler;
			this._uwr.downloadHandler = new DownloadHandlerBuffer();
			this._uwr.SendWebRequest();
		}

		// Token: 0x06000005 RID: 5 RVA: 0x0000210C File Offset: 0x0000030C
		[Obsolete("This overload is deprecated. Use UnityEngine.WWW.WWW(string, byte[], System.Collections.Generic.Dictionary<string, string>) instead.")]
		public WWW(string url, byte[] postData, Hashtable headers)
		{
			string method = (postData != null) ? "POST" : "GET";
			this._uwr = new UnityWebRequest(url, method);
			this._uwr.chunkedTransfer = false;
			UploadHandler uploadHandler = new UploadHandlerRaw(postData);
			uploadHandler.contentType = "application/x-www-form-urlencoded";
			this._uwr.uploadHandler = uploadHandler;
			this._uwr.downloadHandler = new DownloadHandlerBuffer();
			IEnumerator enumerator = headers.Keys.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					this._uwr.SetRequestHeader((string)obj, (string)headers[obj]);
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
			this._uwr.SendWebRequest();
		}

		// Token: 0x06000006 RID: 6 RVA: 0x000021F8 File Offset: 0x000003F8
		public WWW(string url, byte[] postData, Dictionary<string, string> headers)
		{
			string method = (postData != null) ? "POST" : "GET";
			this._uwr = new UnityWebRequest(url, method);
			this._uwr.chunkedTransfer = false;
			UploadHandler uploadHandler = new UploadHandlerRaw(postData);
			uploadHandler.contentType = "application/x-www-form-urlencoded";
			this._uwr.uploadHandler = uploadHandler;
			this._uwr.downloadHandler = new DownloadHandlerBuffer();
			foreach (KeyValuePair<string, string> keyValuePair in headers)
			{
				this._uwr.SetRequestHeader(keyValuePair.Key, keyValuePair.Value);
			}
			this._uwr.SendWebRequest();
		}

		// Token: 0x06000007 RID: 7 RVA: 0x000022D4 File Offset: 0x000004D4
		internal WWW(string url, string name, Hash128 hash, uint crc)
		{
			this._uwr = UnityWebRequestAssetBundle.GetAssetBundle(url, new CachedAssetBundle(name, hash), crc);
			this._uwr.SendWebRequest();
		}

		// Token: 0x06000008 RID: 8 RVA: 0x00002300 File Offset: 0x00000500
		public static string EscapeURL(string s)
		{
			return WWW.EscapeURL(s, Encoding.UTF8);
		}

		// Token: 0x06000009 RID: 9 RVA: 0x00002320 File Offset: 0x00000520
		public static string EscapeURL(string s, Encoding e)
		{
			return UnityWebRequest.EscapeURL(s, e);
		}

		// Token: 0x0600000A RID: 10 RVA: 0x0000233C File Offset: 0x0000053C
		public static string UnEscapeURL(string s)
		{
			return WWW.UnEscapeURL(s, Encoding.UTF8);
		}

		// Token: 0x0600000B RID: 11 RVA: 0x0000235C File Offset: 0x0000055C
		public static string UnEscapeURL(string s, Encoding e)
		{
			return UnityWebRequest.UnEscapeURL(s, e);
		}

		// Token: 0x0600000C RID: 12 RVA: 0x00002378 File Offset: 0x00000578
		public static WWW LoadFromCacheOrDownload(string url, int version)
		{
			return WWW.LoadFromCacheOrDownload(url, version, 0U);
		}

		// Token: 0x0600000D RID: 13 RVA: 0x00002398 File Offset: 0x00000598
		public static WWW LoadFromCacheOrDownload(string url, int version, uint crc)
		{
			Hash128 hash = new Hash128(0U, 0U, 0U, (uint)version);
			return WWW.LoadFromCacheOrDownload(url, hash, crc);
		}

		// Token: 0x0600000E RID: 14 RVA: 0x000023C0 File Offset: 0x000005C0
		public static WWW LoadFromCacheOrDownload(string url, Hash128 hash)
		{
			return WWW.LoadFromCacheOrDownload(url, hash, 0U);
		}

		// Token: 0x0600000F RID: 15 RVA: 0x000023E0 File Offset: 0x000005E0
		public static WWW LoadFromCacheOrDownload(string url, Hash128 hash, uint crc)
		{
			return new WWW(url, "", hash, crc);
		}

		// Token: 0x06000010 RID: 16 RVA: 0x00002404 File Offset: 0x00000604
		public static WWW LoadFromCacheOrDownload(string url, CachedAssetBundle cachedBundle, uint crc = 0U)
		{
			return new WWW(url, cachedBundle.name, cachedBundle.hash, crc);
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000011 RID: 17 RVA: 0x00002430 File Offset: 0x00000630
		public AssetBundle assetBundle
		{
			get
			{
				if (this._assetBundle == null)
				{
					if (!this.WaitUntilDoneIfPossible())
					{
						return null;
					}
					if (this._uwr.isNetworkError)
					{
						return null;
					}
					DownloadHandlerAssetBundle downloadHandlerAssetBundle = this._uwr.downloadHandler as DownloadHandlerAssetBundle;
					if (downloadHandlerAssetBundle != null)
					{
						this._assetBundle = downloadHandlerAssetBundle.assetBundle;
					}
					else
					{
						byte[] bytes = this.bytes;
						if (bytes == null)
						{
							return null;
						}
						this._assetBundle = AssetBundle.LoadFromMemory(bytes);
					}
				}
				return this._assetBundle;
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000012 RID: 18 RVA: 0x000024D4 File Offset: 0x000006D4
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Obsolete msg (UnityUpgradable) -> * UnityEngine.WWW.GetAudioClip()", true)]
		public Object audioClip
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000013 RID: 19 RVA: 0x000024EC File Offset: 0x000006EC
		public byte[] bytes
		{
			get
			{
				byte[] result;
				if (!this.WaitUntilDoneIfPossible())
				{
					result = new byte[0];
				}
				else if (this._uwr.isNetworkError)
				{
					result = new byte[0];
				}
				else
				{
					DownloadHandler downloadHandler = this._uwr.downloadHandler;
					if (downloadHandler == null)
					{
						result = new byte[0];
					}
					else
					{
						result = downloadHandler.data;
					}
				}
				return result;
			}
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000014 RID: 20 RVA: 0x00002558 File Offset: 0x00000758
		[Obsolete("WWW.size is obsolete. Please use WWW.bytesDownloaded instead")]
		public int size
		{
			get
			{
				return this.bytesDownloaded;
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000015 RID: 21 RVA: 0x00002574 File Offset: 0x00000774
		public int bytesDownloaded
		{
			get
			{
				return (int)this._uwr.downloadedBytes;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000016 RID: 22 RVA: 0x00002598 File Offset: 0x00000798
		public string error
		{
			get
			{
				string result;
				if (!this._uwr.isDone)
				{
					result = null;
				}
				else if (this._uwr.isNetworkError)
				{
					result = this._uwr.error;
				}
				else if (this._uwr.responseCode >= 400L)
				{
					string httpstatusString = UnityWebRequest.GetHTTPStatusString(this._uwr.responseCode);
					result = string.Format("{0} {1}", this._uwr.responseCode, httpstatusString);
				}
				else
				{
					result = null;
				}
				return result;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000017 RID: 23 RVA: 0x00002630 File Offset: 0x00000830
		public bool isDone
		{
			get
			{
				return this._uwr.isDone;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000018 RID: 24 RVA: 0x00002650 File Offset: 0x00000850
		public float progress
		{
			get
			{
				float num = this._uwr.downloadProgress;
				if (num < 0f)
				{
					num = 0f;
				}
				return num;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000019 RID: 25 RVA: 0x00002684 File Offset: 0x00000884
		public Dictionary<string, string> responseHeaders
		{
			get
			{
				Dictionary<string, string> result;
				if (!this.isDone)
				{
					result = new Dictionary<string, string>();
				}
				else
				{
					if (this._responseHeaders == null)
					{
						this._responseHeaders = this._uwr.GetResponseHeaders();
						if (this._responseHeaders != null)
						{
							string httpstatusString = UnityWebRequest.GetHTTPStatusString(this._uwr.responseCode);
							this._responseHeaders["STATUS"] = string.Format("HTTP/1.1 {0} {1}", this._uwr.responseCode, httpstatusString);
						}
						else
						{
							this._responseHeaders = new Dictionary<string, string>();
						}
					}
					result = this._responseHeaders;
				}
				return result;
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x0600001A RID: 26 RVA: 0x0000272C File Offset: 0x0000092C
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Please use WWW.text instead. (UnityUpgradable) -> text", true)]
		public string data
		{
			get
			{
				return this.text;
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x0600001B RID: 27 RVA: 0x00002748 File Offset: 0x00000948
		public string text
		{
			get
			{
				string result;
				if (!this.WaitUntilDoneIfPossible())
				{
					result = "";
				}
				else if (this._uwr.isNetworkError)
				{
					result = "";
				}
				else
				{
					DownloadHandler downloadHandler = this._uwr.downloadHandler;
					if (downloadHandler == null)
					{
						result = "";
					}
					else
					{
						result = downloadHandler.text;
					}
				}
				return result;
			}
		}

		// Token: 0x0600001C RID: 28 RVA: 0x000027B4 File Offset: 0x000009B4
		private Texture2D CreateTextureFromDownloadedData(bool markNonReadable)
		{
			Texture2D result;
			if (!this.WaitUntilDoneIfPossible())
			{
				result = new Texture2D(2, 2);
			}
			else if (this._uwr.isNetworkError)
			{
				result = null;
			}
			else
			{
				DownloadHandler downloadHandler = this._uwr.downloadHandler;
				if (downloadHandler == null)
				{
					result = null;
				}
				else
				{
					Texture2D texture2D = new Texture2D(2, 2);
					texture2D.LoadImage(downloadHandler.data, markNonReadable);
					result = texture2D;
				}
			}
			return result;
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x0600001D RID: 29 RVA: 0x00002828 File Offset: 0x00000A28
		public Texture2D texture
		{
			get
			{
				return this.CreateTextureFromDownloadedData(false);
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600001E RID: 30 RVA: 0x00002844 File Offset: 0x00000A44
		public Texture2D textureNonReadable
		{
			get
			{
				return this.CreateTextureFromDownloadedData(true);
			}
		}

		// Token: 0x0600001F RID: 31 RVA: 0x00002860 File Offset: 0x00000A60
		public void LoadImageIntoTexture(Texture2D texture)
		{
			if (this.WaitUntilDoneIfPossible())
			{
				if (this._uwr.isNetworkError)
				{
					Debug.LogError("Cannot load image: download failed");
				}
				else
				{
					DownloadHandler downloadHandler = this._uwr.downloadHandler;
					if (downloadHandler == null)
					{
						Debug.LogError("Cannot load image: internal error");
					}
					else
					{
						texture.LoadImage(downloadHandler.data, false);
					}
				}
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x06000020 RID: 32 RVA: 0x000028D0 File Offset: 0x00000AD0
		// (set) Token: 0x06000021 RID: 33 RVA: 0x000028EA File Offset: 0x00000AEA
		public ThreadPriority threadPriority
		{
			[CompilerGenerated]
			get
			{
				return this.<threadPriority>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<threadPriority>k__BackingField = value;
			}
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000022 RID: 34 RVA: 0x000028F4 File Offset: 0x00000AF4
		public float uploadProgress
		{
			get
			{
				float num = this._uwr.uploadProgress;
				if (num < 0f)
				{
					num = 0f;
				}
				return num;
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000023 RID: 35 RVA: 0x00002928 File Offset: 0x00000B28
		public string url
		{
			get
			{
				return this._uwr.url;
			}
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000024 RID: 36 RVA: 0x00002948 File Offset: 0x00000B48
		public override bool keepWaiting
		{
			get
			{
				return !this._uwr.isDone;
			}
		}

		// Token: 0x06000025 RID: 37 RVA: 0x0000296B File Offset: 0x00000B6B
		public void Dispose()
		{
			this._uwr.Dispose();
		}

		// Token: 0x06000026 RID: 38 RVA: 0x0000297C File Offset: 0x00000B7C
		internal Object GetAudioClipInternal(bool threeD, bool stream, bool compressed, AudioType audioType)
		{
			return WebRequestWWW.InternalCreateAudioClipUsingDH(this._uwr.downloadHandler, this._uwr.url, stream, compressed, audioType);
		}

		// Token: 0x06000027 RID: 39 RVA: 0x000029B0 File Offset: 0x00000BB0
		public AudioClip GetAudioClip()
		{
			return this.GetAudioClip(true, false, AudioType.UNKNOWN);
		}

		// Token: 0x06000028 RID: 40 RVA: 0x000029D0 File Offset: 0x00000BD0
		public AudioClip GetAudioClip(bool threeD)
		{
			return this.GetAudioClip(threeD, false, AudioType.UNKNOWN);
		}

		// Token: 0x06000029 RID: 41 RVA: 0x000029F0 File Offset: 0x00000BF0
		public AudioClip GetAudioClip(bool threeD, bool stream)
		{
			return this.GetAudioClip(threeD, stream, AudioType.UNKNOWN);
		}

		// Token: 0x0600002A RID: 42 RVA: 0x00002A10 File Offset: 0x00000C10
		public AudioClip GetAudioClip(bool threeD, bool stream, AudioType audioType)
		{
			return (AudioClip)this.GetAudioClipInternal(threeD, stream, false, audioType);
		}

		// Token: 0x0600002B RID: 43 RVA: 0x00002A34 File Offset: 0x00000C34
		public AudioClip GetAudioClipCompressed()
		{
			return this.GetAudioClipCompressed(false, AudioType.UNKNOWN);
		}

		// Token: 0x0600002C RID: 44 RVA: 0x00002A54 File Offset: 0x00000C54
		public AudioClip GetAudioClipCompressed(bool threeD)
		{
			return this.GetAudioClipCompressed(threeD, AudioType.UNKNOWN);
		}

		// Token: 0x0600002D RID: 45 RVA: 0x00002A74 File Offset: 0x00000C74
		public AudioClip GetAudioClipCompressed(bool threeD, AudioType audioType)
		{
			return (AudioClip)this.GetAudioClipInternal(threeD, false, true, audioType);
		}

		// Token: 0x0600002E RID: 46 RVA: 0x00002A98 File Offset: 0x00000C98
		private bool WaitUntilDoneIfPossible()
		{
			bool result;
			if (this._uwr.isDone)
			{
				result = true;
			}
			else if (this.url.StartsWith("file://", StringComparison.OrdinalIgnoreCase))
			{
				while (!this._uwr.isDone)
				{
				}
				result = true;
			}
			else
			{
				Debug.LogError("You are trying to load data from a www stream which has not completed the download yet.\nYou need to yield the download or wait until isDone returns true.");
				result = false;
			}
			return result;
		}

		// Token: 0x04000001 RID: 1
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private ThreadPriority <threadPriority>k__BackingField;

		// Token: 0x04000002 RID: 2
		private UnityWebRequest _uwr;

		// Token: 0x04000003 RID: 3
		private AssetBundle _assetBundle;

		// Token: 0x04000004 RID: 4
		private Dictionary<string, string> _responseHeaders;
	}
}
