﻿using System;
using System.IO;

namespace LipingShare.LCLib.Asn1Processor
{
	// Token: 0x02000026 RID: 38
	internal class Asn1Parser
	{
		// Token: 0x1700005A RID: 90
		// (get) Token: 0x06000113 RID: 275 RVA: 0x0000643C File Offset: 0x0000463C
		// (set) Token: 0x06000114 RID: 276 RVA: 0x00006459 File Offset: 0x00004659
		private bool ParseEncapsulatedData
		{
			get
			{
				return this.rootNode.ParseEncapsulatedData;
			}
			set
			{
				this.rootNode.ParseEncapsulatedData = value;
			}
		}

		// Token: 0x06000115 RID: 277 RVA: 0x00006469 File Offset: 0x00004669
		public Asn1Parser()
		{
		}

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x06000116 RID: 278 RVA: 0x00006480 File Offset: 0x00004680
		public byte[] RawData
		{
			get
			{
				return this.rawData;
			}
		}

		// Token: 0x06000117 RID: 279 RVA: 0x00006498 File Offset: 0x00004698
		public void LoadData(string fileName)
		{
			FileStream fileStream = new FileStream(fileName, FileMode.Open);
			this.rawData = new byte[fileStream.Length];
			fileStream.Read(this.rawData, 0, (int)fileStream.Length);
			fileStream.Close();
			MemoryStream stream = new MemoryStream(this.rawData);
			this.LoadData(stream);
		}

		// Token: 0x06000118 RID: 280 RVA: 0x000064F0 File Offset: 0x000046F0
		public void LoadPemData(string fileName)
		{
			FileStream fileStream = new FileStream(fileName, FileMode.Open);
			byte[] array = new byte[fileStream.Length];
			fileStream.Read(array, 0, array.Length);
			fileStream.Close();
			string pemStr = Asn1Util.BytesToString(array);
			bool flag = Asn1Util.IsPemFormated(pemStr);
			if (flag)
			{
				Stream stream = Asn1Util.PemToStream(pemStr);
				stream.Position = 0L;
				this.LoadData(stream);
				return;
			}
			throw new Exception("It is a invalid PEM file: " + fileName);
		}

		// Token: 0x06000119 RID: 281 RVA: 0x00006568 File Offset: 0x00004768
		public void LoadData(Stream stream)
		{
			stream.Position = 0L;
			bool flag = !this.rootNode.LoadData(stream);
			if (flag)
			{
				throw new ArgumentException("Failed to load data.");
			}
			this.rawData = new byte[stream.Length];
			stream.Position = 0L;
			stream.Read(this.rawData, 0, this.rawData.Length);
		}

		// Token: 0x0600011A RID: 282 RVA: 0x000065D0 File Offset: 0x000047D0
		public void SaveData(string fileName)
		{
			FileStream fileStream = new FileStream(fileName, FileMode.Create);
			this.rootNode.SaveData(fileStream);
			fileStream.Close();
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x0600011B RID: 283 RVA: 0x000065FC File Offset: 0x000047FC
		public Asn1Node RootNode
		{
			get
			{
				return this.rootNode;
			}
		}

		// Token: 0x0600011C RID: 284 RVA: 0x00006614 File Offset: 0x00004814
		public Asn1Node GetNodeByPath(string nodePath)
		{
			return this.rootNode.GetDescendantNodeByPath(nodePath);
		}

		// Token: 0x0600011D RID: 285 RVA: 0x00006634 File Offset: 0x00004834
		public Asn1Node GetNodeByOid(string oid)
		{
			return Asn1Node.GetDecendantNodeByOid(oid, this.rootNode);
		}

		// Token: 0x0600011E RID: 286 RVA: 0x00006654 File Offset: 0x00004854
		public static string GetNodeTextHeader(int lineLen)
		{
			string str = string.Format("Offset| Len  |LenByte|\r\n", new object[0]);
			return str + "======+======+=======+" + Asn1Util.GenStr(lineLen + 10, '=') + "\r\n";
		}

		// Token: 0x0600011F RID: 287 RVA: 0x00006694 File Offset: 0x00004894
		public override string ToString()
		{
			return Asn1Parser.GetNodeText(this.rootNode, 100);
		}

		// Token: 0x06000120 RID: 288 RVA: 0x000066B4 File Offset: 0x000048B4
		public static string GetNodeText(Asn1Node node, int lineLen)
		{
			string nodeTextHeader = Asn1Parser.GetNodeTextHeader(lineLen);
			return nodeTextHeader + node.GetText(node, lineLen);
		}

		// Token: 0x04000063 RID: 99
		private byte[] rawData;

		// Token: 0x04000064 RID: 100
		private Asn1Node rootNode = new Asn1Node();
	}
}
