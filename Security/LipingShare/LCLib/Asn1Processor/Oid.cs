﻿using System;
using System.Collections.Specialized;
using System.IO;

namespace LipingShare.LCLib.Asn1Processor
{
	// Token: 0x0200002A RID: 42
	internal class Oid
	{
		// Token: 0x06000140 RID: 320 RVA: 0x00007618 File Offset: 0x00005818
		public string GetOidName(string inOidStr)
		{
			bool flag = Oid.oidDictionary == null;
			if (flag)
			{
				Oid.oidDictionary = new StringDictionary();
			}
			return Oid.oidDictionary[inOidStr];
		}

		// Token: 0x06000141 RID: 321 RVA: 0x00007650 File Offset: 0x00005850
		public byte[] Encode(string oidStr)
		{
			MemoryStream memoryStream = new MemoryStream();
			this.Encode(memoryStream, oidStr);
			memoryStream.Position = 0L;
			byte[] array = new byte[memoryStream.Length];
			memoryStream.Read(array, 0, array.Length);
			memoryStream.Close();
			return array;
		}

		// Token: 0x06000142 RID: 322 RVA: 0x0000769C File Offset: 0x0000589C
		public string Decode(byte[] data)
		{
			MemoryStream memoryStream = new MemoryStream(data);
			memoryStream.Position = 0L;
			string result = this.Decode(memoryStream);
			memoryStream.Close();
			return result;
		}

		// Token: 0x06000143 RID: 323 RVA: 0x000076D0 File Offset: 0x000058D0
		public virtual void Encode(Stream bt, string oidStr)
		{
			string[] array = oidStr.Split(new char[]
			{
				'.'
			});
			bool flag = array.Length < 2;
			if (flag)
			{
				throw new Exception("Invalid OID string.");
			}
			ulong[] array2 = new ulong[array.Length];
			for (int i = 0; i < array.Length; i++)
			{
				array2[i] = Convert.ToUInt64(array[i]);
			}
			bt.WriteByte((byte)(array2[0] * 40UL + array2[1]));
			for (int j = 2; j < array2.Length; j++)
			{
				this.EncodeValue(bt, array2[j]);
			}
		}

		// Token: 0x06000144 RID: 324 RVA: 0x00007768 File Offset: 0x00005968
		public virtual string Decode(Stream bt)
		{
			string text = "";
			ulong num = 0UL;
			byte b = (byte)bt.ReadByte();
			text += Convert.ToString((int)(b / 40));
			text = text + "." + Convert.ToString((int)(b % 40));
			while (bt.Position < bt.Length)
			{
				try
				{
					this.DecodeValue(bt, ref num);
					text = text + "." + num.ToString();
				}
				catch (Exception ex)
				{
					throw new Exception("Failed to decode OID value: " + ex.Message);
				}
			}
			return text;
		}

		// Token: 0x06000145 RID: 325 RVA: 0x000043FB File Offset: 0x000025FB
		public Oid()
		{
		}

		// Token: 0x06000146 RID: 326 RVA: 0x00007814 File Offset: 0x00005A14
		protected void EncodeValue(Stream bt, ulong v)
		{
			for (int i = (Asn1Util.BitPrecision(v) - 1) / 7; i > 0; i--)
			{
				bt.WriteByte((byte)(128UL | (v >> i * 7 & 127UL)));
			}
			bt.WriteByte((byte)(v & 127UL));
		}

		// Token: 0x06000147 RID: 327 RVA: 0x00007868 File Offset: 0x00005A68
		protected int DecodeValue(Stream bt, ref ulong v)
		{
			int num = 0;
			v = 0UL;
			bool flag;
			do
			{
				byte b = (byte)bt.ReadByte();
				num++;
				v <<= 7;
				v += (ulong)((long)(b & 127));
				flag = ((b & 128) == 0);
			}
			while (!flag);
			return num;
		}

		// Token: 0x06000148 RID: 328 RVA: 0x000078B4 File Offset: 0x00005AB4
		// Note: this type is marked as 'beforefieldinit'.
		static Oid()
		{
		}

		// Token: 0x04000089 RID: 137
		private static StringDictionary oidDictionary = null;
	}
}
