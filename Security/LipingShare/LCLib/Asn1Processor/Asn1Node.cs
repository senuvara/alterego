﻿using System;
using System.Collections;
using System.IO;
using System.Text;

namespace LipingShare.LCLib.Asn1Processor
{
	// Token: 0x02000025 RID: 37
	internal class Asn1Node : IAsn1Node
	{
		// Token: 0x060000D9 RID: 217 RVA: 0x00004408 File Offset: 0x00002608
		private Asn1Node(Asn1Node parentNode, long dataOffset)
		{
			this.Init();
			this.deepness = parentNode.Deepness + 1L;
			this.parentNode = parentNode;
			this.dataOffset = dataOffset;
		}

		// Token: 0x060000DA RID: 218 RVA: 0x00004464 File Offset: 0x00002664
		private void Init()
		{
			this.childNodeList = new ArrayList();
			this.data = null;
			this.dataLength = 0L;
			this.lengthFieldBytes = 0L;
			this.unusedBits = 0;
			this.tag = 48;
			this.childNodeList.Clear();
			this.deepness = 0L;
			this.parentNode = null;
		}

		// Token: 0x060000DB RID: 219 RVA: 0x000044C0 File Offset: 0x000026C0
		private string GetHexPrintingStr(Asn1Node startNode, string baseLine, string lStr, int lineLen)
		{
			string text = "";
			string indentStr = this.GetIndentStr(startNode);
			string text2 = Asn1Util.ToHexString(this.data);
			bool flag = text2.Length > 0;
			if (flag)
			{
				bool flag2 = baseLine.Length + text2.Length < lineLen;
				if (flag2)
				{
					text = string.Concat(new string[]
					{
						text,
						baseLine,
						"'",
						text2,
						"'"
					});
				}
				else
				{
					text = text + baseLine + this.FormatLineHexString(lStr, indentStr.Length, lineLen, text2);
				}
			}
			else
			{
				text += baseLine;
			}
			return text + "\r\n";
		}

		// Token: 0x060000DC RID: 220 RVA: 0x00004574 File Offset: 0x00002774
		private string FormatLineString(string lStr, int indent, int lineLen, string msg)
		{
			string text = "";
			indent += 3;
			int num = lineLen - indent;
			int len = indent;
			for (int i = 0; i < msg.Length; i += num)
			{
				bool flag = i + num > msg.Length;
				if (flag)
				{
					text = string.Concat(new string[]
					{
						text,
						"\r\n",
						lStr,
						Asn1Util.GenStr(len, ' '),
						"'",
						msg.Substring(i, msg.Length - i),
						"'"
					});
				}
				else
				{
					text = string.Concat(new string[]
					{
						text,
						"\r\n",
						lStr,
						Asn1Util.GenStr(len, ' '),
						"'",
						msg.Substring(i, num),
						"'"
					});
				}
			}
			return text;
		}

		// Token: 0x060000DD RID: 221 RVA: 0x00004660 File Offset: 0x00002860
		private string FormatLineHexString(string lStr, int indent, int lineLen, string msg)
		{
			string text = "";
			indent += 3;
			int num = lineLen - indent;
			int len = indent;
			for (int i = 0; i < msg.Length; i += num)
			{
				bool flag = i + num > msg.Length;
				if (flag)
				{
					text = string.Concat(new string[]
					{
						text,
						"\r\n",
						lStr,
						Asn1Util.GenStr(len, ' '),
						msg.Substring(i, msg.Length - i)
					});
				}
				else
				{
					text = string.Concat(new string[]
					{
						text,
						"\r\n",
						lStr,
						Asn1Util.GenStr(len, ' '),
						msg.Substring(i, num)
					});
				}
			}
			return text;
		}

		// Token: 0x060000DE RID: 222 RVA: 0x0000472C File Offset: 0x0000292C
		public Asn1Node()
		{
			this.Init();
			this.dataOffset = 0L;
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x060000DF RID: 223 RVA: 0x00004768 File Offset: 0x00002968
		// (set) Token: 0x060000E0 RID: 224 RVA: 0x00004780 File Offset: 0x00002980
		public bool IsIndefiniteLength
		{
			get
			{
				return this.isIndefiniteLength;
			}
			set
			{
				this.isIndefiniteLength = value;
			}
		}

		// Token: 0x060000E1 RID: 225 RVA: 0x0000478C File Offset: 0x0000298C
		public Asn1Node Clone()
		{
			MemoryStream memoryStream = new MemoryStream();
			this.SaveData(memoryStream);
			memoryStream.Position = 0L;
			Asn1Node asn1Node = new Asn1Node();
			asn1Node.LoadData(memoryStream);
			return asn1Node;
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x060000E2 RID: 226 RVA: 0x000047C4 File Offset: 0x000029C4
		// (set) Token: 0x060000E3 RID: 227 RVA: 0x000047DC File Offset: 0x000029DC
		public byte Tag
		{
			get
			{
				return this.tag;
			}
			set
			{
				this.tag = value;
			}
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x060000E4 RID: 228 RVA: 0x000047E8 File Offset: 0x000029E8
		public byte MaskedTag
		{
			get
			{
				return this.tag & 31;
			}
		}

		// Token: 0x060000E5 RID: 229 RVA: 0x00004804 File Offset: 0x00002A04
		public bool LoadData(byte[] byteData)
		{
			bool result = true;
			try
			{
				MemoryStream memoryStream = new MemoryStream(byteData);
				memoryStream.Position = 0L;
				result = this.LoadData(memoryStream);
				memoryStream.Close();
			}
			catch
			{
				result = false;
			}
			return result;
		}

		// Token: 0x060000E6 RID: 230 RVA: 0x00004854 File Offset: 0x00002A54
		public static long GetDescendantNodeCount(Asn1Node node)
		{
			long num = 0L;
			num += node.ChildNodeCount;
			int num2 = 0;
			while ((long)num2 < node.ChildNodeCount)
			{
				num += Asn1Node.GetDescendantNodeCount(node.GetChildNode(num2));
				num2++;
			}
			return num;
		}

		// Token: 0x060000E7 RID: 231 RVA: 0x0000489C File Offset: 0x00002A9C
		public bool LoadData(Stream xdata)
		{
			bool result;
			try
			{
				this.RequireRecalculatePar = false;
				bool flag = this.InternalLoadData(xdata);
				result = flag;
			}
			finally
			{
				this.RequireRecalculatePar = true;
				this.RecalculateTreePar();
			}
			return result;
		}

		// Token: 0x060000E8 RID: 232 RVA: 0x000048E4 File Offset: 0x00002AE4
		public byte[] GetRawData()
		{
			MemoryStream memoryStream = new MemoryStream();
			this.SaveData(memoryStream);
			byte[] array = new byte[memoryStream.Length];
			memoryStream.Position = 0L;
			memoryStream.Read(array, 0, (int)memoryStream.Length);
			memoryStream.Close();
			return array;
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x060000E9 RID: 233 RVA: 0x00004934 File Offset: 0x00002B34
		public bool IsEmptyData
		{
			get
			{
				bool flag = this.data == null;
				bool result;
				if (flag)
				{
					result = true;
				}
				else
				{
					bool flag2 = this.data.Length < 1;
					result = flag2;
				}
				return result;
			}
		}

		// Token: 0x060000EA RID: 234 RVA: 0x0000496C File Offset: 0x00002B6C
		public bool SaveData(Stream xdata)
		{
			bool result = true;
			long childNodeCount = this.ChildNodeCount;
			xdata.WriteByte(this.tag);
			Asn1Util.DERLengthEncode(xdata, (ulong)this.dataLength);
			bool flag = this.tag == 3;
			if (flag)
			{
				xdata.WriteByte(this.unusedBits);
			}
			bool flag2 = childNodeCount == 0L;
			if (flag2)
			{
				bool flag3 = this.data != null;
				if (flag3)
				{
					xdata.Write(this.data, 0, this.data.Length);
				}
			}
			else
			{
				int num = 0;
				while ((long)num < childNodeCount)
				{
					Asn1Node childNode = this.GetChildNode(num);
					result = childNode.SaveData(xdata);
					num++;
				}
			}
			return result;
		}

		// Token: 0x060000EB RID: 235 RVA: 0x00004A24 File Offset: 0x00002C24
		public void ClearAll()
		{
			this.data = null;
			for (int i = 0; i < this.childNodeList.Count; i++)
			{
				Asn1Node asn1Node = (Asn1Node)this.childNodeList[i];
				asn1Node.ClearAll();
			}
			this.childNodeList.Clear();
			this.RecalculateTreePar();
		}

		// Token: 0x060000EC RID: 236 RVA: 0x00004A81 File Offset: 0x00002C81
		public void AddChild(Asn1Node xdata)
		{
			this.childNodeList.Add(xdata);
			this.RecalculateTreePar();
		}

		// Token: 0x060000ED RID: 237 RVA: 0x00004A98 File Offset: 0x00002C98
		public int InsertChild(Asn1Node xdata, int index)
		{
			this.childNodeList.Insert(index, xdata);
			this.RecalculateTreePar();
			return index;
		}

		// Token: 0x060000EE RID: 238 RVA: 0x00004AC0 File Offset: 0x00002CC0
		public int InsertChild(Asn1Node xdata, Asn1Node indexNode)
		{
			int num = this.childNodeList.IndexOf(indexNode);
			this.childNodeList.Insert(num, xdata);
			this.RecalculateTreePar();
			return num;
		}

		// Token: 0x060000EF RID: 239 RVA: 0x00004AF8 File Offset: 0x00002CF8
		public int InsertChildAfter(Asn1Node xdata, Asn1Node indexNode)
		{
			int num = this.childNodeList.IndexOf(indexNode) + 1;
			this.childNodeList.Insert(num, xdata);
			this.RecalculateTreePar();
			return num;
		}

		// Token: 0x060000F0 RID: 240 RVA: 0x00004B30 File Offset: 0x00002D30
		public int InsertChildAfter(Asn1Node xdata, int index)
		{
			int num = index + 1;
			this.childNodeList.Insert(num, xdata);
			this.RecalculateTreePar();
			return num;
		}

		// Token: 0x060000F1 RID: 241 RVA: 0x00004B5C File Offset: 0x00002D5C
		public Asn1Node RemoveChild(int index)
		{
			Asn1Node asn1Node = null;
			bool flag = index < this.childNodeList.Count - 1;
			if (flag)
			{
				asn1Node = (Asn1Node)this.childNodeList[index + 1];
			}
			this.childNodeList.RemoveAt(index);
			bool flag2 = asn1Node == null;
			if (flag2)
			{
				bool flag3 = this.childNodeList.Count > 0;
				if (flag3)
				{
					asn1Node = (Asn1Node)this.childNodeList[this.childNodeList.Count - 1];
				}
				else
				{
					asn1Node = this;
				}
			}
			this.RecalculateTreePar();
			return asn1Node;
		}

		// Token: 0x060000F2 RID: 242 RVA: 0x00004BF4 File Offset: 0x00002DF4
		public Asn1Node RemoveChild(Asn1Node node)
		{
			int index = this.childNodeList.IndexOf(node);
			return this.RemoveChild(index);
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x060000F3 RID: 243 RVA: 0x00004C20 File Offset: 0x00002E20
		public long ChildNodeCount
		{
			get
			{
				return (long)this.childNodeList.Count;
			}
		}

		// Token: 0x060000F4 RID: 244 RVA: 0x00004C40 File Offset: 0x00002E40
		public Asn1Node GetChildNode(int index)
		{
			Asn1Node result = null;
			bool flag = (long)index < this.ChildNodeCount;
			if (flag)
			{
				result = (Asn1Node)this.childNodeList[index];
			}
			return result;
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x060000F5 RID: 245 RVA: 0x00004C78 File Offset: 0x00002E78
		public string TagName
		{
			get
			{
				return Asn1Util.GetTagName(this.tag);
			}
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x060000F6 RID: 246 RVA: 0x00004C98 File Offset: 0x00002E98
		public Asn1Node ParentNode
		{
			get
			{
				return this.parentNode;
			}
		}

		// Token: 0x060000F7 RID: 247 RVA: 0x00004CB0 File Offset: 0x00002EB0
		public string GetText(Asn1Node startNode, int lineLen)
		{
			string text = "";
			byte b = this.tag;
			switch (b)
			{
			case 2:
			{
				bool flag = this.data != null && this.dataLength < 8L;
				if (flag)
				{
					text += string.Format("{0,6}|{1,6}|{2,7}|{3} {4} : {5}\r\n", new object[]
					{
						this.dataOffset,
						this.dataLength,
						this.lengthFieldBytes,
						this.GetIndentStr(startNode),
						this.TagName,
						Asn1Util.BytesToLong(this.data).ToString()
					});
				}
				else
				{
					string text2 = string.Format("{0,6}|{1,6}|{2,7}|{3} {4} : ", new object[]
					{
						this.dataOffset,
						this.dataLength,
						this.lengthFieldBytes,
						this.GetIndentStr(startNode),
						this.TagName
					});
					text += this.GetHexPrintingStr(startNode, text2, "      |      |       | ", lineLen);
				}
				goto IL_5C8;
			}
			case 3:
			{
				string text2 = string.Format("{0,6}|{1,6}|{2,7}|{3} {4} UnusedBits:{5} : ", new object[]
				{
					this.dataOffset,
					this.dataLength,
					this.lengthFieldBytes,
					this.GetIndentStr(startNode),
					this.TagName,
					this.unusedBits
				});
				string text3 = Asn1Util.ToHexString(this.data);
				bool flag2 = text2.Length + text3.Length < lineLen;
				if (flag2)
				{
					bool flag3 = text3.Length < 1;
					if (flag3)
					{
						text = text + text2 + "\r\n";
					}
					else
					{
						text = string.Concat(new string[]
						{
							text,
							text2,
							"'",
							text3,
							"'\r\n"
						});
					}
				}
				else
				{
					text = text + text2 + this.FormatLineHexString("      |      |       | ", this.GetIndentStr(startNode).Length, lineLen, text3 + "\r\n");
				}
				goto IL_5C8;
			}
			case 4:
			case 5:
				break;
			case 6:
			{
				Oid oid = new Oid();
				string text4 = oid.Decode(new MemoryStream(this.data));
				string text5 = oid.GetOidName(text4);
				text += string.Format("{0,6}|{1,6}|{2,7}|{3} {4} : {5} [{6}]\r\n", new object[]
				{
					this.dataOffset,
					this.dataLength,
					this.lengthFieldBytes,
					this.GetIndentStr(startNode),
					this.TagName,
					text5,
					text4
				});
				goto IL_5C8;
			}
			default:
				switch (b)
				{
				case 12:
				case 18:
				case 19:
				case 22:
				case 23:
				case 24:
				case 26:
				case 27:
				case 28:
				case 30:
				{
					string text2 = string.Format("{0,6}|{1,6}|{2,7}|{3} {4} : ", new object[]
					{
						this.dataOffset,
						this.dataLength,
						this.lengthFieldBytes,
						this.GetIndentStr(startNode),
						this.TagName
					});
					bool flag4 = this.tag == 12;
					string text3;
					if (flag4)
					{
						UTF8Encoding utf8Encoding = new UTF8Encoding();
						text3 = utf8Encoding.GetString(this.data);
					}
					else
					{
						text3 = Asn1Util.BytesToString(this.data);
					}
					bool flag5 = text2.Length + text3.Length < lineLen;
					if (flag5)
					{
						text = string.Concat(new string[]
						{
							text,
							text2,
							"'",
							text3,
							"'\r\n"
						});
					}
					else
					{
						text = text + text2 + this.FormatLineString("      |      |       | ", this.GetIndentStr(startNode).Length, lineLen, text3) + "\r\n";
					}
					goto IL_5C8;
				}
				case 13:
				{
					RelativeOid relativeOid = new RelativeOid();
					string text4 = relativeOid.Decode(new MemoryStream(this.data));
					string text5 = "";
					text += string.Format("{0,6}|{1,6}|{2,7}|{3} {4} : {5} [{6}]\r\n", new object[]
					{
						this.dataOffset,
						this.dataLength,
						this.lengthFieldBytes,
						this.GetIndentStr(startNode),
						this.TagName,
						text5,
						text4
					});
					goto IL_5C8;
				}
				}
				break;
			}
			bool flag6 = (this.tag & 31) == 6;
			if (flag6)
			{
				string text2 = string.Format("{0,6}|{1,6}|{2,7}|{3} {4} : ", new object[]
				{
					this.dataOffset,
					this.dataLength,
					this.lengthFieldBytes,
					this.GetIndentStr(startNode),
					this.TagName
				});
				string text3 = Asn1Util.BytesToString(this.data);
				bool flag7 = text2.Length + text3.Length < lineLen;
				if (flag7)
				{
					text = string.Concat(new string[]
					{
						text,
						text2,
						"'",
						text3,
						"'\r\n"
					});
				}
				else
				{
					text = text + text2 + this.FormatLineString("      |      |       | ", this.GetIndentStr(startNode).Length, lineLen, text3) + "\r\n";
				}
			}
			else
			{
				string text2 = string.Format("{0,6}|{1,6}|{2,7}|{3} {4} : ", new object[]
				{
					this.dataOffset,
					this.dataLength,
					this.lengthFieldBytes,
					this.GetIndentStr(startNode),
					this.TagName
				});
				text += this.GetHexPrintingStr(startNode, text2, "      |      |       | ", lineLen);
			}
			IL_5C8:
			bool flag8 = this.childNodeList.Count >= 0;
			if (flag8)
			{
				text += this.GetListStr(startNode, lineLen);
			}
			return text;
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x060000F8 RID: 248 RVA: 0x000052B8 File Offset: 0x000034B8
		public string Path
		{
			get
			{
				return this.path;
			}
		}

		// Token: 0x060000F9 RID: 249 RVA: 0x000052D0 File Offset: 0x000034D0
		public string GetDataStr(bool pureHexMode)
		{
			string result;
			if (pureHexMode)
			{
				result = Asn1Util.FormatString(Asn1Util.ToHexString(this.data), 32, 2);
			}
			else
			{
				byte b = this.tag;
				switch (b)
				{
				case 2:
					result = Asn1Util.FormatString(Asn1Util.ToHexString(this.data), 32, 2);
					goto IL_16E;
				case 3:
					result = Asn1Util.FormatString(Asn1Util.ToHexString(this.data), 32, 2);
					goto IL_16E;
				case 4:
				case 5:
					break;
				case 6:
				{
					Oid oid = new Oid();
					result = oid.Decode(new MemoryStream(this.data));
					goto IL_16E;
				}
				default:
					switch (b)
					{
					case 12:
					{
						UTF8Encoding utf8Encoding = new UTF8Encoding();
						result = utf8Encoding.GetString(this.data);
						goto IL_16E;
					}
					case 13:
					{
						RelativeOid relativeOid = new RelativeOid();
						result = relativeOid.Decode(new MemoryStream(this.data));
						goto IL_16E;
					}
					case 18:
					case 19:
					case 22:
					case 23:
					case 24:
					case 26:
					case 27:
					case 28:
					case 30:
						result = Asn1Util.BytesToString(this.data);
						goto IL_16E;
					}
					break;
				}
				bool flag = (this.tag & 31) == 6;
				if (flag)
				{
					result = Asn1Util.BytesToString(this.data);
				}
				else
				{
					result = Asn1Util.FormatString(Asn1Util.ToHexString(this.data), 32, 2);
				}
				IL_16E:;
			}
			return result;
		}

		// Token: 0x060000FA RID: 250 RVA: 0x00005454 File Offset: 0x00003654
		public string GetLabel(uint mask)
		{
			string text = "";
			bool flag = (mask & 4U) > 0U;
			string str;
			if (flag)
			{
				bool flag2 = (mask & 8U) > 0U;
				if (flag2)
				{
					str = string.Format("(0x{0:X2},0x{1:X6},0x{2:X4})", this.tag, this.dataOffset, this.dataLength);
				}
				else
				{
					str = string.Format("(0x{0:X6},0x{1:X4})", this.dataOffset, this.dataLength);
				}
			}
			else
			{
				bool flag3 = (mask & 8U) > 0U;
				if (flag3)
				{
					str = string.Format("({0},{1},{2})", this.tag, this.dataOffset, this.dataLength);
				}
				else
				{
					str = string.Format("({0},{1})", this.dataOffset, this.dataLength);
				}
			}
			byte b = this.tag;
			switch (b)
			{
			case 2:
			{
				bool flag4 = (mask & 1U) > 0U;
				if (flag4)
				{
					text += str;
				}
				text = text + " " + this.TagName;
				bool flag5 = (mask & 2U) > 0U;
				if (flag5)
				{
					bool flag6 = this.data != null && this.dataLength < 8L;
					string text2;
					if (flag6)
					{
						text2 = Asn1Util.BytesToLong(this.data).ToString();
					}
					else
					{
						text2 = Asn1Util.ToHexString(this.data);
					}
					text += ((text2.Length > 0) ? (" : '" + text2 + "'") : "");
				}
				goto IL_4D7;
			}
			case 3:
			{
				bool flag7 = (mask & 1U) > 0U;
				if (flag7)
				{
					text += str;
				}
				text = string.Concat(new string[]
				{
					text,
					" ",
					this.TagName,
					" UnusedBits: ",
					this.unusedBits.ToString()
				});
				bool flag8 = (mask & 2U) > 0U;
				if (flag8)
				{
					string text2 = Asn1Util.ToHexString(this.data);
					text += ((text2.Length > 0) ? (" : '" + text2 + "'") : "");
				}
				goto IL_4D7;
			}
			case 4:
			case 5:
				break;
			case 6:
			{
				Oid oid = new Oid();
				string text3 = oid.Decode(this.data);
				string str2 = oid.GetOidName(text3);
				bool flag9 = (mask & 1U) > 0U;
				if (flag9)
				{
					text += str;
				}
				text = text + " " + this.TagName;
				text = text + " : " + str2;
				bool flag10 = (mask & 2U) > 0U;
				if (flag10)
				{
					text += ((text3.Length > 0) ? (" : '" + text3 + "'") : "");
				}
				goto IL_4D7;
			}
			default:
				switch (b)
				{
				case 12:
				case 18:
				case 19:
				case 22:
				case 23:
				case 24:
				case 26:
				case 27:
				case 28:
				case 30:
				{
					bool flag11 = (mask & 1U) > 0U;
					if (flag11)
					{
						text += str;
					}
					text = text + " " + this.TagName;
					bool flag12 = (mask & 2U) > 0U;
					if (flag12)
					{
						bool flag13 = this.tag == 12;
						string text2;
						if (flag13)
						{
							UTF8Encoding utf8Encoding = new UTF8Encoding();
							text2 = utf8Encoding.GetString(this.data);
						}
						else
						{
							text2 = Asn1Util.BytesToString(this.data);
						}
						text += ((text2.Length > 0) ? (" : '" + text2 + "'") : "");
					}
					goto IL_4D7;
				}
				case 13:
				{
					RelativeOid relativeOid = new RelativeOid();
					string text3 = relativeOid.Decode(this.data);
					string str2 = "";
					bool flag14 = (mask & 1U) > 0U;
					if (flag14)
					{
						text += str;
					}
					text = text + " " + this.TagName;
					text = text + " : " + str2;
					bool flag15 = (mask & 2U) > 0U;
					if (flag15)
					{
						text += ((text3.Length > 0) ? (" : '" + text3 + "'") : "");
					}
					goto IL_4D7;
				}
				}
				break;
			}
			bool flag16 = (mask & 1U) > 0U;
			if (flag16)
			{
				text += str;
			}
			text = text + " " + this.TagName;
			bool flag17 = (mask & 2U) > 0U;
			if (flag17)
			{
				bool flag18 = (this.tag & 31) == 6;
				string text2;
				if (flag18)
				{
					text2 = Asn1Util.BytesToString(this.data);
				}
				else
				{
					text2 = Asn1Util.ToHexString(this.data);
				}
				text += ((text2.Length > 0) ? (" : '" + text2 + "'") : "");
			}
			IL_4D7:
			bool flag19 = (mask & 16U) > 0U;
			if (flag19)
			{
				text = "(" + this.path + ") " + text;
			}
			return text;
		}

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x060000FB RID: 251 RVA: 0x00005968 File Offset: 0x00003B68
		public long DataLength
		{
			get
			{
				return this.dataLength;
			}
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x060000FC RID: 252 RVA: 0x00005980 File Offset: 0x00003B80
		public long LengthFieldBytes
		{
			get
			{
				return this.lengthFieldBytes;
			}
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x060000FD RID: 253 RVA: 0x00005998 File Offset: 0x00003B98
		// (set) Token: 0x060000FE RID: 254 RVA: 0x00005A49 File Offset: 0x00003C49
		public byte[] Data
		{
			get
			{
				MemoryStream memoryStream = new MemoryStream();
				long childNodeCount = this.ChildNodeCount;
				bool flag = childNodeCount == 0L;
				if (flag)
				{
					bool flag2 = this.data != null;
					if (flag2)
					{
						memoryStream.Write(this.data, 0, this.data.Length);
					}
				}
				else
				{
					int num = 0;
					while ((long)num < childNodeCount)
					{
						Asn1Node childNode = this.GetChildNode(num);
						childNode.SaveData(memoryStream);
						num++;
					}
				}
				byte[] array = new byte[memoryStream.Length];
				memoryStream.Position = 0L;
				memoryStream.Read(array, 0, (int)memoryStream.Length);
				memoryStream.Close();
				return array;
			}
			set
			{
				this.SetData(value);
			}
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x060000FF RID: 255 RVA: 0x00005A54 File Offset: 0x00003C54
		public long Deepness
		{
			get
			{
				return this.deepness;
			}
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x06000100 RID: 256 RVA: 0x00005A6C File Offset: 0x00003C6C
		public long DataOffset
		{
			get
			{
				return this.dataOffset;
			}
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x06000101 RID: 257 RVA: 0x00005A84 File Offset: 0x00003C84
		// (set) Token: 0x06000102 RID: 258 RVA: 0x00005A9C File Offset: 0x00003C9C
		public byte UnusedBits
		{
			get
			{
				return this.unusedBits;
			}
			set
			{
				this.unusedBits = value;
			}
		}

		// Token: 0x06000103 RID: 259 RVA: 0x00005AA8 File Offset: 0x00003CA8
		public Asn1Node GetDescendantNodeByPath(string nodePath)
		{
			Asn1Node asn1Node = this;
			bool flag = nodePath == null;
			Asn1Node result;
			if (flag)
			{
				result = asn1Node;
			}
			else
			{
				nodePath = nodePath.TrimEnd(new char[0]).TrimStart(new char[0]);
				bool flag2 = nodePath.Length < 1;
				if (flag2)
				{
					result = asn1Node;
				}
				else
				{
					string[] array = nodePath.Split(new char[]
					{
						'/'
					});
					try
					{
						for (int i = 1; i < array.Length; i++)
						{
							asn1Node = asn1Node.GetChildNode(Convert.ToInt32(array[i]));
						}
					}
					catch
					{
						asn1Node = null;
					}
					result = asn1Node;
				}
			}
			return result;
		}

		// Token: 0x06000104 RID: 260 RVA: 0x00005B4C File Offset: 0x00003D4C
		public static Asn1Node GetDecendantNodeByOid(string oid, Asn1Node startNode)
		{
			Asn1Node asn1Node = null;
			Oid oid2 = new Oid();
			int num = 0;
			while ((long)num < startNode.ChildNodeCount)
			{
				Asn1Node childNode = startNode.GetChildNode(num);
				int num2 = (int)(childNode.tag & 31);
				bool flag = num2 == 6;
				if (flag)
				{
					bool flag2 = oid == oid2.Decode(childNode.Data);
					if (flag2)
					{
						asn1Node = childNode;
						break;
					}
				}
				asn1Node = Asn1Node.GetDecendantNodeByOid(oid, childNode);
				bool flag3 = asn1Node != null;
				if (flag3)
				{
					break;
				}
				num++;
			}
			return asn1Node;
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x06000105 RID: 261 RVA: 0x00005BD4 File Offset: 0x00003DD4
		// (set) Token: 0x06000106 RID: 262 RVA: 0x00005BEC File Offset: 0x00003DEC
		protected bool RequireRecalculatePar
		{
			get
			{
				return this.requireRecalculatePar;
			}
			set
			{
				this.requireRecalculatePar = value;
			}
		}

		// Token: 0x06000107 RID: 263 RVA: 0x00005BF8 File Offset: 0x00003DF8
		protected void RecalculateTreePar()
		{
			bool flag = !this.requireRecalculatePar;
			if (!flag)
			{
				Asn1Node asn1Node = this;
				while (asn1Node.ParentNode != null)
				{
					asn1Node = asn1Node.ParentNode;
				}
				Asn1Node.ResetBranchDataLength(asn1Node);
				asn1Node.dataOffset = 0L;
				asn1Node.deepness = 0L;
				long subOffset = asn1Node.dataOffset + 1L + asn1Node.lengthFieldBytes;
				this.ResetChildNodePar(asn1Node, subOffset);
			}
		}

		// Token: 0x06000108 RID: 264 RVA: 0x00005C60 File Offset: 0x00003E60
		protected static long ResetBranchDataLength(Asn1Node node)
		{
			long num = 0L;
			bool flag = node.ChildNodeCount < 1L;
			if (flag)
			{
				bool flag2 = node.data != null;
				if (flag2)
				{
					num += (long)node.data.Length;
				}
			}
			else
			{
				int num2 = 0;
				while ((long)num2 < node.ChildNodeCount)
				{
					num += Asn1Node.ResetBranchDataLength(node.GetChildNode(num2));
					num2++;
				}
			}
			node.dataLength = num;
			bool flag3 = node.tag == 3;
			if (flag3)
			{
				node.dataLength += 1L;
			}
			Asn1Node.ResetDataLengthFieldWidth(node);
			return node.dataLength + 1L + node.lengthFieldBytes;
		}

		// Token: 0x06000109 RID: 265 RVA: 0x00005D14 File Offset: 0x00003F14
		protected static void ResetDataLengthFieldWidth(Asn1Node node)
		{
			MemoryStream memoryStream = new MemoryStream();
			Asn1Util.DERLengthEncode(memoryStream, (ulong)node.dataLength);
			node.lengthFieldBytes = memoryStream.Length;
			memoryStream.Close();
		}

		// Token: 0x0600010A RID: 266 RVA: 0x00005D48 File Offset: 0x00003F48
		protected void ResetChildNodePar(Asn1Node xNode, long subOffset)
		{
			bool flag = xNode.tag == 3;
			if (flag)
			{
				subOffset += 1L;
			}
			int num = 0;
			while ((long)num < xNode.ChildNodeCount)
			{
				Asn1Node childNode = xNode.GetChildNode(num);
				childNode.parentNode = xNode;
				childNode.dataOffset = subOffset;
				childNode.deepness = xNode.deepness + 1L;
				childNode.path = xNode.path + "/" + num.ToString();
				subOffset += 1L + childNode.lengthFieldBytes;
				this.ResetChildNodePar(childNode, subOffset);
				subOffset += childNode.dataLength;
				num++;
			}
		}

		// Token: 0x0600010B RID: 267 RVA: 0x00005DE8 File Offset: 0x00003FE8
		protected string GetListStr(Asn1Node startNode, int lineLen)
		{
			string text = "";
			for (int i = 0; i < this.childNodeList.Count; i++)
			{
				Asn1Node asn1Node = (Asn1Node)this.childNodeList[i];
				text += asn1Node.GetText(startNode, lineLen);
			}
			return text;
		}

		// Token: 0x0600010C RID: 268 RVA: 0x00005E40 File Offset: 0x00004040
		protected string GetIndentStr(Asn1Node startNode)
		{
			string text = "";
			long num = 0L;
			bool flag = startNode != null;
			if (flag)
			{
				num = startNode.Deepness;
			}
			for (long num2 = 0L; num2 < this.deepness - num; num2 += 1L)
			{
				text += "   ";
			}
			return text;
		}

		// Token: 0x0600010D RID: 269 RVA: 0x00005E98 File Offset: 0x00004098
		protected bool GeneralDecode(Stream xdata)
		{
			bool flag = false;
			long num = xdata.Length - xdata.Position;
			this.tag = (byte)xdata.ReadByte();
			long position = xdata.Position;
			this.dataLength = Asn1Util.DerLengthDecode(xdata, ref this.isIndefiniteLength);
			bool flag2 = this.dataLength < 0L;
			bool result;
			if (flag2)
			{
				result = flag;
			}
			else
			{
				long position2 = xdata.Position;
				this.lengthFieldBytes = position2 - position;
				bool flag3 = num < this.dataLength + 1L + this.lengthFieldBytes;
				if (flag3)
				{
					result = flag;
				}
				else
				{
					bool flag4 = this.ParentNode == null || (this.ParentNode.tag & 32) == 0;
					if (flag4)
					{
						bool flag5 = (this.tag & 31) <= 0 || (this.tag & 31) > 30;
						if (flag5)
						{
							return flag;
						}
					}
					bool flag6 = this.tag == 3;
					if (flag6)
					{
						bool flag7 = this.dataLength < 1L;
						if (flag7)
						{
							return flag;
						}
						this.unusedBits = (byte)xdata.ReadByte();
						this.data = new byte[this.dataLength - 1L];
						xdata.Read(this.data, 0, (int)(this.dataLength - 1L));
					}
					else
					{
						this.data = new byte[this.dataLength];
						xdata.Read(this.data, 0, (int)this.dataLength);
					}
					flag = true;
					result = flag;
				}
			}
			return result;
		}

		// Token: 0x0600010E RID: 270 RVA: 0x00006008 File Offset: 0x00004208
		protected bool ListDecode(Stream xdata)
		{
			bool flag = false;
			long position = xdata.Position;
			try
			{
				long num = xdata.Length - xdata.Position;
				this.tag = (byte)xdata.ReadByte();
				long position2 = xdata.Position;
				this.dataLength = Asn1Util.DerLengthDecode(xdata, ref this.isIndefiniteLength);
				bool flag2 = this.dataLength < 0L || num < this.dataLength;
				if (flag2)
				{
					return flag;
				}
				long position3 = xdata.Position;
				this.lengthFieldBytes = position3 - position2;
				long num2 = this.dataOffset + 1L + this.lengthFieldBytes;
				bool flag3 = this.tag == 3;
				if (flag3)
				{
					this.unusedBits = (byte)xdata.ReadByte();
					this.dataLength -= 1L;
					num2 += 1L;
				}
				bool flag4 = this.dataLength <= 0L;
				if (flag4)
				{
					return flag;
				}
				Stream stream = new MemoryStream((int)this.dataLength);
				byte[] array = new byte[this.dataLength];
				xdata.Read(array, 0, (int)this.dataLength);
				bool flag5 = this.tag == 3;
				if (flag5)
				{
					this.dataLength += 1L;
				}
				stream.Write(array, 0, array.Length);
				stream.Position = 0L;
				while (stream.Position < stream.Length)
				{
					Asn1Node asn1Node = new Asn1Node(this, num2);
					asn1Node.parseEncapsulatedData = this.parseEncapsulatedData;
					position2 = stream.Position;
					bool flag6 = !asn1Node.InternalLoadData(stream);
					if (flag6)
					{
						return flag;
					}
					this.AddChild(asn1Node);
					position3 = stream.Position;
					num2 += position3 - position2;
				}
				flag = true;
			}
			finally
			{
				bool flag7 = !flag;
				if (flag7)
				{
					xdata.Position = position;
					this.ClearAll();
				}
			}
			return flag;
		}

		// Token: 0x0600010F RID: 271 RVA: 0x00006204 File Offset: 0x00004404
		protected void SetData(byte[] xdata)
		{
			bool flag = this.childNodeList.Count > 0;
			if (flag)
			{
				throw new Exception("Constructed node can't hold simple data.");
			}
			this.data = xdata;
			bool flag2 = this.data != null;
			if (flag2)
			{
				this.dataLength = (long)this.data.Length;
			}
			else
			{
				this.dataLength = 0L;
			}
			this.RecalculateTreePar();
		}

		// Token: 0x06000110 RID: 272 RVA: 0x00006268 File Offset: 0x00004468
		protected bool InternalLoadData(Stream xdata)
		{
			bool result = true;
			this.ClearAll();
			long position = xdata.Position;
			byte b = (byte)xdata.ReadByte();
			xdata.Position = position;
			int num = (int)(b & 31);
			bool flag = (b & 32) != 0 || (this.parseEncapsulatedData && (num == 3 || num == 8 || num == 27 || num == 24 || num == 25 || num == 22 || num == 4 || num == 19 || num == 16 || num == 17 || num == 20 || num == 28 || num == 12 || num == 21 || num == 26));
			if (flag)
			{
				bool flag2 = !this.ListDecode(xdata);
				if (flag2)
				{
					bool flag3 = !this.GeneralDecode(xdata);
					if (flag3)
					{
						result = false;
					}
				}
			}
			else
			{
				bool flag4 = !this.GeneralDecode(xdata);
				if (flag4)
				{
					result = false;
				}
			}
			return result;
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x06000111 RID: 273 RVA: 0x00006348 File Offset: 0x00004548
		// (set) Token: 0x06000112 RID: 274 RVA: 0x00006360 File Offset: 0x00004560
		public bool ParseEncapsulatedData
		{
			get
			{
				return this.parseEncapsulatedData;
			}
			set
			{
				bool flag = this.parseEncapsulatedData == value;
				if (!flag)
				{
					byte[] buffer = this.Data;
					this.parseEncapsulatedData = value;
					this.ClearAll();
					bool flag2 = (this.tag & 32) != 0 || this.parseEncapsulatedData;
					if (flag2)
					{
						MemoryStream memoryStream = new MemoryStream(buffer);
						memoryStream.Position = 0L;
						bool flag3 = true;
						while (memoryStream.Position < memoryStream.Length)
						{
							Asn1Node asn1Node = new Asn1Node();
							asn1Node.ParseEncapsulatedData = this.parseEncapsulatedData;
							bool flag4 = !asn1Node.LoadData(memoryStream);
							if (flag4)
							{
								this.ClearAll();
								flag3 = false;
								break;
							}
							this.AddChild(asn1Node);
						}
						bool flag5 = !flag3;
						if (flag5)
						{
							this.Data = buffer;
						}
					}
					else
					{
						this.Data = buffer;
					}
				}
			}
		}

		// Token: 0x04000051 RID: 81
		private byte tag;

		// Token: 0x04000052 RID: 82
		private long dataOffset;

		// Token: 0x04000053 RID: 83
		private long dataLength;

		// Token: 0x04000054 RID: 84
		private long lengthFieldBytes;

		// Token: 0x04000055 RID: 85
		private byte[] data;

		// Token: 0x04000056 RID: 86
		private ArrayList childNodeList;

		// Token: 0x04000057 RID: 87
		private byte unusedBits;

		// Token: 0x04000058 RID: 88
		private long deepness;

		// Token: 0x04000059 RID: 89
		private string path = "";

		// Token: 0x0400005A RID: 90
		private const int indentStep = 3;

		// Token: 0x0400005B RID: 91
		private Asn1Node parentNode;

		// Token: 0x0400005C RID: 92
		private bool requireRecalculatePar = true;

		// Token: 0x0400005D RID: 93
		private bool isIndefiniteLength = false;

		// Token: 0x0400005E RID: 94
		private bool parseEncapsulatedData = true;

		// Token: 0x0400005F RID: 95
		public const int defaultLineLen = 80;

		// Token: 0x04000060 RID: 96
		public const int minLineLen = 60;

		// Token: 0x04000061 RID: 97
		public const int TagLength = 1;

		// Token: 0x04000062 RID: 98
		public const int BitStringUnusedFiledLength = 1;

		// Token: 0x02000030 RID: 48
		public class TagTextMask
		{
			// Token: 0x06000152 RID: 338 RVA: 0x000038B4 File Offset: 0x00001AB4
			public TagTextMask()
			{
			}

			// Token: 0x0400008D RID: 141
			public const uint SHOW_OFFSET = 1U;

			// Token: 0x0400008E RID: 142
			public const uint SHOW_DATA = 2U;

			// Token: 0x0400008F RID: 143
			public const uint USE_HEX_OFFSET = 4U;

			// Token: 0x04000090 RID: 144
			public const uint SHOW_TAG_NUMBER = 8U;

			// Token: 0x04000091 RID: 145
			public const uint SHOW_PATH = 16U;
		}
	}
}
