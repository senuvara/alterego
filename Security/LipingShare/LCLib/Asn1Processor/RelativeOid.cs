﻿using System;
using System.IO;

namespace LipingShare.LCLib.Asn1Processor
{
	// Token: 0x0200002B RID: 43
	internal class RelativeOid : Oid
	{
		// Token: 0x06000149 RID: 329 RVA: 0x000078BC File Offset: 0x00005ABC
		public RelativeOid()
		{
		}

		// Token: 0x0600014A RID: 330 RVA: 0x000078C8 File Offset: 0x00005AC8
		public override void Encode(Stream bt, string oidStr)
		{
			string[] array = oidStr.Split(new char[]
			{
				'.'
			});
			ulong[] array2 = new ulong[array.Length];
			for (int i = 0; i < array.Length; i++)
			{
				array2[i] = Convert.ToUInt64(array[i]);
			}
			for (int j = 0; j < array2.Length; j++)
			{
				base.EncodeValue(bt, array2[j]);
			}
		}

		// Token: 0x0600014B RID: 331 RVA: 0x00007934 File Offset: 0x00005B34
		public override string Decode(Stream bt)
		{
			string text = "";
			ulong num = 0UL;
			bool flag = true;
			while (bt.Position < bt.Length)
			{
				try
				{
					base.DecodeValue(bt, ref num);
					bool flag2 = flag;
					if (flag2)
					{
						text = num.ToString();
						flag = false;
					}
					else
					{
						text = text + "." + num.ToString();
					}
				}
				catch (Exception ex)
				{
					throw new Exception("Failed to decode OID value: " + ex.Message);
				}
			}
			return text;
		}
	}
}
