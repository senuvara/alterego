﻿using System;
using System.IO;
using Microsoft.Win32;

namespace LipingShare.LCLib.Asn1Processor
{
	// Token: 0x02000029 RID: 41
	internal class Asn1Util
	{
		// Token: 0x06000123 RID: 291 RVA: 0x000066E0 File Offset: 0x000048E0
		public static bool IsAsn1EncodedHexStr(string dataStr)
		{
			bool result = false;
			try
			{
				byte[] array = Asn1Util.HexStrToBytes(dataStr);
				bool flag = array.Length != 0;
				if (flag)
				{
					Asn1Node asn1Node = new Asn1Node();
					result = asn1Node.LoadData(array);
				}
			}
			catch
			{
				result = false;
			}
			return result;
		}

		// Token: 0x06000124 RID: 292 RVA: 0x00006734 File Offset: 0x00004934
		public static string FormatString(string inStr, int lineLen, int groupLen)
		{
			char[] array = new char[inStr.Length * 2];
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			for (int i = 0; i < inStr.Length; i++)
			{
				array[num++] = inStr[i];
				num3++;
				num2++;
				bool flag = num3 >= groupLen && groupLen > 0;
				if (flag)
				{
					array[num++] = ' ';
					num3 = 0;
				}
				bool flag2 = num2 >= lineLen;
				if (flag2)
				{
					array[num++] = '\r';
					array[num++] = '\n';
					num2 = 0;
				}
			}
			string text = new string(array);
			text = text.TrimEnd(new char[1]);
			text = text.TrimEnd(new char[]
			{
				'\n'
			});
			return text.TrimEnd(new char[]
			{
				'\r'
			});
		}

		// Token: 0x06000125 RID: 293 RVA: 0x00006814 File Offset: 0x00004A14
		public static string GenStr(int len, char xch)
		{
			char[] array = new char[len];
			for (int i = 0; i < len; i++)
			{
				array[i] = xch;
			}
			return new string(array);
		}

		// Token: 0x06000126 RID: 294 RVA: 0x0000684C File Offset: 0x00004A4C
		public static long BytesToLong(byte[] bytes)
		{
			long num = 0L;
			for (int i = 0; i < bytes.Length; i++)
			{
				num = (num << 8 | (long)((ulong)bytes[i]));
			}
			return num;
		}

		// Token: 0x06000127 RID: 295 RVA: 0x00006880 File Offset: 0x00004A80
		public static string BytesToString(byte[] bytes)
		{
			string text = "";
			bool flag = bytes == null || bytes.Length < 1;
			string result;
			if (flag)
			{
				result = text;
			}
			else
			{
				char[] array = new char[bytes.Length];
				int i = 0;
				int num = 0;
				while (i < bytes.Length)
				{
					bool flag2 = bytes[i] > 0;
					if (flag2)
					{
						array[num++] = (char)bytes[i];
					}
					i++;
				}
				text = new string(array);
				text = text.TrimEnd(new char[1]);
				result = text;
			}
			return result;
		}

		// Token: 0x06000128 RID: 296 RVA: 0x00006904 File Offset: 0x00004B04
		public static byte[] StringToBytes(string msg)
		{
			byte[] array = new byte[msg.Length];
			for (int i = 0; i < msg.Length; i++)
			{
				array[i] = (byte)msg[i];
			}
			return array;
		}

		// Token: 0x06000129 RID: 297 RVA: 0x00006948 File Offset: 0x00004B48
		public static bool IsEqual(byte[] source, byte[] target)
		{
			bool flag = source == null;
			bool result;
			if (flag)
			{
				result = false;
			}
			else
			{
				bool flag2 = target == null;
				if (flag2)
				{
					result = false;
				}
				else
				{
					bool flag3 = source.Length != target.Length;
					if (flag3)
					{
						result = false;
					}
					else
					{
						for (int i = 0; i < source.Length; i++)
						{
							bool flag4 = source[i] != target[i];
							if (flag4)
							{
								return false;
							}
						}
						result = true;
					}
				}
			}
			return result;
		}

		// Token: 0x0600012A RID: 298 RVA: 0x000069B8 File Offset: 0x00004BB8
		public static string ToHexString(byte[] bytes)
		{
			bool flag = bytes == null;
			string result;
			if (flag)
			{
				result = "";
			}
			else
			{
				char[] array = new char[bytes.Length * 2];
				for (int i = 0; i < bytes.Length; i++)
				{
					int num = (int)bytes[i];
					array[i * 2] = Asn1Util.hexDigits[num >> 4];
					array[i * 2 + 1] = Asn1Util.hexDigits[num & 15];
				}
				result = new string(array);
			}
			return result;
		}

		// Token: 0x0600012B RID: 299 RVA: 0x00006A28 File Offset: 0x00004C28
		public static bool IsValidHexDigits(char ch)
		{
			bool result = false;
			for (int i = 0; i < Asn1Util.hexDigits.Length; i++)
			{
				bool flag = Asn1Util.hexDigits[i] == ch;
				if (flag)
				{
					result = true;
					break;
				}
			}
			return result;
		}

		// Token: 0x0600012C RID: 300 RVA: 0x00006A6C File Offset: 0x00004C6C
		public static byte GetHexDigitsVal(char ch)
		{
			byte result = 0;
			for (int i = 0; i < Asn1Util.hexDigits.Length; i++)
			{
				bool flag = Asn1Util.hexDigits[i] == ch;
				if (flag)
				{
					result = (byte)i;
					break;
				}
			}
			return result;
		}

		// Token: 0x0600012D RID: 301 RVA: 0x00006AB0 File Offset: 0x00004CB0
		public static byte[] HexStrToBytes(string hexStr)
		{
			hexStr = hexStr.Replace(" ", "");
			hexStr = hexStr.Replace("\r", "");
			hexStr = hexStr.Replace("\n", "");
			hexStr = hexStr.ToUpper();
			bool flag = hexStr.Length % 2 != 0;
			if (flag)
			{
				throw new Exception("Invalid Hex string: odd length.");
			}
			for (int i = 0; i < hexStr.Length; i++)
			{
				bool flag2 = !Asn1Util.IsValidHexDigits(hexStr[i]);
				if (flag2)
				{
					throw new Exception("Invalid Hex string: included invalid character [" + hexStr[i].ToString() + "]");
				}
			}
			int num = hexStr.Length / 2;
			byte[] array = new byte[num];
			for (int i = 0; i < num; i++)
			{
				int hexDigitsVal = (int)Asn1Util.GetHexDigitsVal(hexStr[i * 2]);
				int hexDigitsVal2 = (int)Asn1Util.GetHexDigitsVal(hexStr[i * 2 + 1]);
				int num2 = hexDigitsVal << 4 | hexDigitsVal2;
				array[i] = (byte)num2;
			}
			return array;
		}

		// Token: 0x0600012E RID: 302 RVA: 0x00006BC8 File Offset: 0x00004DC8
		public static bool IsHexStr(string hexStr)
		{
			byte[] array = null;
			try
			{
				array = Asn1Util.HexStrToBytes(hexStr);
			}
			catch
			{
				return false;
			}
			bool flag = array == null || array.Length < 0;
			return !flag;
		}

		// Token: 0x0600012F RID: 303 RVA: 0x00006C14 File Offset: 0x00004E14
		public static bool IsPemFormated(string pemStr)
		{
			byte[] array = null;
			try
			{
				array = Asn1Util.PemToBytes(pemStr);
			}
			catch
			{
				return false;
			}
			return array.Length != 0;
		}

		// Token: 0x06000130 RID: 304 RVA: 0x00006C50 File Offset: 0x00004E50
		public static bool IsPemFormatedFile(string fileName)
		{
			bool result = false;
			try
			{
				FileStream fileStream = new FileStream(fileName, FileMode.Open);
				byte[] array = new byte[fileStream.Length];
				fileStream.Read(array, 0, array.Length);
				fileStream.Close();
				string pemStr = Asn1Util.BytesToString(array);
				result = Asn1Util.IsPemFormated(pemStr);
			}
			catch
			{
				result = false;
			}
			return result;
		}

		// Token: 0x06000131 RID: 305 RVA: 0x00006CB8 File Offset: 0x00004EB8
		public static Stream PemToStream(string pemStr)
		{
			byte[] buffer = Asn1Util.PemToBytes(pemStr);
			return new MemoryStream(buffer)
			{
				Position = 0L
			};
		}

		// Token: 0x06000132 RID: 306 RVA: 0x00006CE4 File Offset: 0x00004EE4
		public static byte[] PemToBytes(string pemStr)
		{
			string[] array = pemStr.Split(new char[]
			{
				'\n'
			});
			string text = "";
			bool flag = false;
			bool flag2 = false;
			for (int i = 0; i < array.Length; i++)
			{
				string text2 = array[i].ToUpper();
				bool flag3 = text2 == "";
				if (!flag3)
				{
					bool flag4 = text2.Length > "-----BEGIN".Length;
					if (flag4)
					{
						bool flag5 = !flag && text2.Substring(0, "-----BEGIN".Length) == "-----BEGIN";
						if (flag5)
						{
							flag = true;
							goto IL_F2;
						}
					}
					bool flag6 = text2.Length > "-----END".Length;
					if (flag6)
					{
						bool flag7 = text2.Substring(0, "-----END".Length) == "-----END";
						if (flag7)
						{
							flag2 = true;
							break;
						}
					}
					bool flag8 = flag;
					if (flag8)
					{
						text += array[i];
					}
				}
				IL_F2:;
			}
			bool flag9 = !flag || !flag2;
			if (flag9)
			{
				throw new Exception("'BEGIN'/'END' line is missing.");
			}
			text = text.Replace("\r", "");
			text = text.Replace("\n", "");
			text = text.Replace("\n", " ");
			return Convert.FromBase64String(text);
		}

		// Token: 0x06000133 RID: 307 RVA: 0x00006E54 File Offset: 0x00005054
		public static string BytesToPem(byte[] data)
		{
			return Asn1Util.BytesToPem(data, "");
		}

		// Token: 0x06000134 RID: 308 RVA: 0x00006E74 File Offset: 0x00005074
		public static string GetPemFileHeader(string fileName)
		{
			string result;
			try
			{
				FileStream fileStream = new FileStream(fileName, FileMode.Open);
				byte[] array = new byte[fileStream.Length];
				fileStream.Read(array, 0, array.Length);
				fileStream.Close();
				string pemStr = Asn1Util.BytesToString(array);
				result = Asn1Util.GetPemHeader(pemStr);
			}
			catch
			{
				result = "";
			}
			return result;
		}

		// Token: 0x06000135 RID: 309 RVA: 0x00006ED8 File Offset: 0x000050D8
		public static string GetPemHeader(string pemStr)
		{
			string[] array = pemStr.Split(new char[]
			{
				'\n'
			});
			bool flag = false;
			for (int i = 0; i < array.Length; i++)
			{
				string text = array[i].ToUpper().Replace("\r", "");
				bool flag2 = text == "";
				if (!flag2)
				{
					bool flag3 = text.Length > "-----BEGIN".Length;
					if (flag3)
					{
						bool flag4 = !flag && text.Substring(0, "-----BEGIN".Length) == "-----BEGIN";
						if (flag4)
						{
							string text2 = array[i].Substring("-----BEGIN".Length, array[i].Length - "-----BEGIN".Length).Replace("-----", "");
							return text2.Replace("\r", "");
						}
					}
				}
			}
			return "";
		}

		// Token: 0x06000136 RID: 310 RVA: 0x00006FEC File Offset: 0x000051EC
		public static string BytesToPem(byte[] data, string pemHeader)
		{
			bool flag = pemHeader == null || pemHeader.Length < 1;
			if (flag)
			{
				pemHeader = "ASN.1 Editor Generated PEM File";
			}
			bool flag2 = pemHeader.Length > 0 && pemHeader[0] != ' ';
			if (flag2)
			{
				pemHeader = " " + pemHeader;
			}
			string text = Convert.ToBase64String(data);
			text = Asn1Util.FormatString(text, 64, 0);
			return string.Concat(new string[]
			{
				"-----BEGIN",
				pemHeader,
				"-----\r\n",
				text,
				"\r\n-----END",
				pemHeader,
				"-----\r\n"
			});
		}

		// Token: 0x06000137 RID: 311 RVA: 0x00007098 File Offset: 0x00005298
		public static int BitPrecision(ulong ivalue)
		{
			bool flag = ivalue == 0UL;
			int result;
			if (flag)
			{
				result = 0;
			}
			else
			{
				int num = 0;
				int num2 = 32;
				while (num2 - num > 1)
				{
					int num3 = (num + num2) / 2;
					bool flag2 = ivalue >> num3 > 0UL;
					if (flag2)
					{
						num = num3;
					}
					else
					{
						num2 = num3;
					}
				}
				result = num2;
			}
			return result;
		}

		// Token: 0x06000138 RID: 312 RVA: 0x000070F0 File Offset: 0x000052F0
		public static int BytePrecision(ulong value)
		{
			int i;
			for (i = 4; i > 0; i--)
			{
				bool flag = value >> (i - 1) * 8 > 0UL;
				if (flag)
				{
					break;
				}
			}
			return i;
		}

		// Token: 0x06000139 RID: 313 RVA: 0x00007128 File Offset: 0x00005328
		public static int DERLengthEncode(Stream xdata, ulong length)
		{
			int num = 0;
			bool flag = length <= 127UL;
			if (flag)
			{
				xdata.WriteByte((byte)length);
				num++;
			}
			else
			{
				xdata.WriteByte((byte)(Asn1Util.BytePrecision(length) | 128));
				num++;
				for (int i = Asn1Util.BytePrecision(length); i > 0; i--)
				{
					xdata.WriteByte((byte)(length >> (i - 1) * 8));
					num++;
				}
			}
			return num;
		}

		// Token: 0x0600013A RID: 314 RVA: 0x000071A4 File Offset: 0x000053A4
		public static long DerLengthDecode(Stream bt, ref bool isIndefiniteLength)
		{
			isIndefiniteLength = false;
			byte b = (byte)bt.ReadByte();
			bool flag = (b & 128) == 0;
			long num;
			if (flag)
			{
				num = (long)((ulong)b);
			}
			else
			{
				long num2 = (long)(b & 127);
				bool flag2 = num2 == 0L;
				if (flag2)
				{
					isIndefiniteLength = true;
					return -2L;
				}
				num = 0L;
				for (;;)
				{
					long num3 = num2;
					num2 = num3 - 1L;
					if (num3 <= 0L)
					{
						goto IL_7D;
					}
					bool flag3 = num >> 24 > 0L;
					if (flag3)
					{
						break;
					}
					b = (byte)bt.ReadByte();
					num = (num << 8 | (long)((ulong)b));
				}
				return -1L;
			}
			IL_7D:
			return num;
		}

		// Token: 0x0600013B RID: 315 RVA: 0x00007238 File Offset: 0x00005438
		public static string GetTagName(byte tag)
		{
			string text = "";
			bool flag = (tag & 192) > 0;
			if (flag)
			{
				int num = (int)(tag & 192);
				if (num <= 32)
				{
					if (num != 0)
					{
						if (num == 32)
						{
							text = text + "CONSTRUCTED (" + ((int)(tag & 31)).ToString() + ")";
						}
					}
					else
					{
						text = text + "UNIVERSAL (" + ((int)(tag & 31)).ToString() + ")";
					}
				}
				else if (num != 64)
				{
					if (num != 128)
					{
						if (num == 192)
						{
							text = text + "PRIVATE (" + ((int)(tag & 31)).ToString() + ")";
						}
					}
					else
					{
						text = text + "CONTEXT SPECIFIC (" + ((int)(tag & 31)).ToString() + ")";
					}
				}
				else
				{
					text = text + "APPLICATION (" + ((int)(tag & 31)).ToString() + ")";
				}
			}
			else
			{
				switch (tag & 31)
				{
				case 1:
					text += "BOOLEAN";
					goto IL_334;
				case 2:
					text += "INTEGER";
					goto IL_334;
				case 3:
					text += "BIT STRING";
					goto IL_334;
				case 4:
					text += "OCTET STRING";
					goto IL_334;
				case 5:
					text += "NULL";
					goto IL_334;
				case 6:
					text += "OBJECT IDENTIFIER";
					goto IL_334;
				case 7:
					text += "OBJECT DESCRIPTOR";
					goto IL_334;
				case 8:
					text += "EXTERNAL";
					goto IL_334;
				case 9:
					text += "REAL";
					goto IL_334;
				case 10:
					text += "ENUMERATED";
					goto IL_334;
				case 12:
					text += "UTF8 STRING";
					goto IL_334;
				case 13:
					text += "RELATIVE-OID";
					goto IL_334;
				case 16:
					text += "SEQUENCE";
					goto IL_334;
				case 17:
					text += "SET";
					goto IL_334;
				case 18:
					text += "NUMERIC STRING";
					goto IL_334;
				case 19:
					text += "PRINTABLE STRING";
					goto IL_334;
				case 20:
					text += "T61 STRING";
					goto IL_334;
				case 21:
					text += "VIDEOTEXT STRING";
					goto IL_334;
				case 22:
					text += "IA5 STRING";
					goto IL_334;
				case 23:
					text += "UTC TIME";
					goto IL_334;
				case 24:
					text += "GENERALIZED TIME";
					goto IL_334;
				case 25:
					text += "GRAPHIC STRING";
					goto IL_334;
				case 26:
					text += "VISIBLE STRING";
					goto IL_334;
				case 27:
					text += "GENERAL STRING";
					goto IL_334;
				case 28:
					text += "UNIVERSAL STRING";
					goto IL_334;
				case 30:
					text += "BMP STRING";
					goto IL_334;
				}
				text += "UNKNOWN TAG";
				IL_334:;
			}
			return text;
		}

		// Token: 0x0600013C RID: 316 RVA: 0x00007584 File Offset: 0x00005784
		public static object ReadRegInfo(string path, string name)
		{
			object result = null;
			RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(path, false);
			bool flag = registryKey != null;
			if (flag)
			{
				result = registryKey.GetValue(name);
			}
			return result;
		}

		// Token: 0x0600013D RID: 317 RVA: 0x000075B8 File Offset: 0x000057B8
		public static void WriteRegInfo(string path, string name, object data)
		{
			RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(path, true);
			bool flag = registryKey == null;
			if (flag)
			{
				registryKey = Registry.LocalMachine.CreateSubKey(path);
			}
			bool flag2 = registryKey != null;
			if (flag2)
			{
				registryKey.SetValue(name, data);
			}
		}

		// Token: 0x0600013E RID: 318 RVA: 0x000043FB File Offset: 0x000025FB
		private Asn1Util()
		{
		}

		// Token: 0x0600013F RID: 319 RVA: 0x000075FC File Offset: 0x000057FC
		// Note: this type is marked as 'beforefieldinit'.
		static Asn1Util()
		{
		}

		// Token: 0x04000086 RID: 134
		private static char[] hexDigits = new char[]
		{
			'0',
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
			'8',
			'9',
			'A',
			'B',
			'C',
			'D',
			'E',
			'F'
		};

		// Token: 0x04000087 RID: 135
		private const string PemStartStr = "-----BEGIN";

		// Token: 0x04000088 RID: 136
		private const string PemEndStr = "-----END";
	}
}
