﻿using System;

namespace LipingShare.LCLib.Asn1Processor
{
	// Token: 0x02000027 RID: 39
	internal class Asn1Tag
	{
		// Token: 0x06000121 RID: 289 RVA: 0x000043FB File Offset: 0x000025FB
		public Asn1Tag()
		{
		}

		// Token: 0x04000065 RID: 101
		public const byte TAG_MASK = 31;

		// Token: 0x04000066 RID: 102
		public const byte BOOLEAN = 1;

		// Token: 0x04000067 RID: 103
		public const byte INTEGER = 2;

		// Token: 0x04000068 RID: 104
		public const byte BIT_STRING = 3;

		// Token: 0x04000069 RID: 105
		public const byte OCTET_STRING = 4;

		// Token: 0x0400006A RID: 106
		public const byte TAG_NULL = 5;

		// Token: 0x0400006B RID: 107
		public const byte OBJECT_IDENTIFIER = 6;

		// Token: 0x0400006C RID: 108
		public const byte OBJECT_DESCRIPTOR = 7;

		// Token: 0x0400006D RID: 109
		public const byte EXTERNAL = 8;

		// Token: 0x0400006E RID: 110
		public const byte REAL = 9;

		// Token: 0x0400006F RID: 111
		public const byte ENUMERATED = 10;

		// Token: 0x04000070 RID: 112
		public const byte UTF8_STRING = 12;

		// Token: 0x04000071 RID: 113
		public const byte RELATIVE_OID = 13;

		// Token: 0x04000072 RID: 114
		public const byte SEQUENCE = 16;

		// Token: 0x04000073 RID: 115
		public const byte SET = 17;

		// Token: 0x04000074 RID: 116
		public const byte NUMERIC_STRING = 18;

		// Token: 0x04000075 RID: 117
		public const byte PRINTABLE_STRING = 19;

		// Token: 0x04000076 RID: 118
		public const byte T61_STRING = 20;

		// Token: 0x04000077 RID: 119
		public const byte VIDEOTEXT_STRING = 21;

		// Token: 0x04000078 RID: 120
		public const byte IA5_STRING = 22;

		// Token: 0x04000079 RID: 121
		public const byte UTC_TIME = 23;

		// Token: 0x0400007A RID: 122
		public const byte GENERALIZED_TIME = 24;

		// Token: 0x0400007B RID: 123
		public const byte GRAPHIC_STRING = 25;

		// Token: 0x0400007C RID: 124
		public const byte VISIBLE_STRING = 26;

		// Token: 0x0400007D RID: 125
		public const byte GENERAL_STRING = 27;

		// Token: 0x0400007E RID: 126
		public const byte UNIVERSAL_STRING = 28;

		// Token: 0x0400007F RID: 127
		public const byte BMPSTRING = 30;
	}
}
