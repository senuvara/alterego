﻿using System;

namespace LipingShare.LCLib.Asn1Processor
{
	// Token: 0x02000028 RID: 40
	internal class Asn1TagClasses
	{
		// Token: 0x06000122 RID: 290 RVA: 0x000043FB File Offset: 0x000025FB
		public Asn1TagClasses()
		{
		}

		// Token: 0x04000080 RID: 128
		public const byte CLASS_MASK = 192;

		// Token: 0x04000081 RID: 129
		public const byte UNIVERSAL = 0;

		// Token: 0x04000082 RID: 130
		public const byte CONSTRUCTED = 32;

		// Token: 0x04000083 RID: 131
		public const byte APPLICATION = 64;

		// Token: 0x04000084 RID: 132
		public const byte CONTEXT_SPECIFIC = 128;

		// Token: 0x04000085 RID: 133
		public const byte PRIVATE = 192;
	}
}
