﻿using System;
using System.IO;

namespace LipingShare.LCLib.Asn1Processor
{
	// Token: 0x02000024 RID: 36
	internal interface IAsn1Node
	{
		// Token: 0x060000B9 RID: 185
		bool LoadData(Stream xdata);

		// Token: 0x060000BA RID: 186
		bool SaveData(Stream xdata);

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x060000BB RID: 187
		Asn1Node ParentNode { get; }

		// Token: 0x060000BC RID: 188
		void AddChild(Asn1Node xdata);

		// Token: 0x060000BD RID: 189
		int InsertChild(Asn1Node xdata, int index);

		// Token: 0x060000BE RID: 190
		int InsertChild(Asn1Node xdata, Asn1Node indexNode);

		// Token: 0x060000BF RID: 191
		int InsertChildAfter(Asn1Node xdata, int index);

		// Token: 0x060000C0 RID: 192
		int InsertChildAfter(Asn1Node xdata, Asn1Node indexNode);

		// Token: 0x060000C1 RID: 193
		Asn1Node RemoveChild(int index);

		// Token: 0x060000C2 RID: 194
		Asn1Node RemoveChild(Asn1Node node);

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x060000C3 RID: 195
		long ChildNodeCount { get; }

		// Token: 0x060000C4 RID: 196
		Asn1Node GetChildNode(int index);

		// Token: 0x060000C5 RID: 197
		Asn1Node GetDescendantNodeByPath(string nodePath);

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x060000C6 RID: 198
		// (set) Token: 0x060000C7 RID: 199
		byte Tag { get; set; }

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x060000C8 RID: 200
		byte MaskedTag { get; }

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x060000C9 RID: 201
		string TagName { get; }

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x060000CA RID: 202
		long DataLength { get; }

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x060000CB RID: 203
		long LengthFieldBytes { get; }

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x060000CC RID: 204
		long DataOffset { get; }

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x060000CD RID: 205
		byte UnusedBits { get; }

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x060000CE RID: 206
		// (set) Token: 0x060000CF RID: 207
		byte[] Data { get; set; }

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x060000D0 RID: 208
		// (set) Token: 0x060000D1 RID: 209
		bool ParseEncapsulatedData { get; set; }

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x060000D2 RID: 210
		long Deepness { get; }

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x060000D3 RID: 211
		string Path { get; }

		// Token: 0x060000D4 RID: 212
		string GetText(Asn1Node startNode, int lineLen);

		// Token: 0x060000D5 RID: 213
		string GetDataStr(bool pureHexMode);

		// Token: 0x060000D6 RID: 214
		string GetLabel(uint mask);

		// Token: 0x060000D7 RID: 215
		Asn1Node Clone();

		// Token: 0x060000D8 RID: 216
		void ClearAll();
	}
}
