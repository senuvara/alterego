﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Token: 0x0200002C RID: 44
[CompilerGenerated]
internal sealed class <PrivateImplementationDetails>
{
	// Token: 0x0600014C RID: 332 RVA: 0x000079CC File Offset: 0x00005BCC
	internal static uint ComputeStringHash(string s)
	{
		uint num;
		if (s != null)
		{
			num = 2166136261U;
			for (int i = 0; i < s.Length; i++)
			{
				num = ((uint)s[i] ^ num) * 16777619U;
			}
		}
		return num;
	}

	// Token: 0x0400008A RID: 138 RVA: 0x0000EB5C File Offset: 0x0000CD5C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=32 59F5BD34B6C013DEACC784F69C67E95150033A84;

	// Token: 0x02000031 RID: 49
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 32)]
	private struct __StaticArrayInitTypeSize=32
	{
	}
}
