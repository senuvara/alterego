﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using LipingShare.LCLib.Asn1Processor;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x0200000E RID: 14
	internal class RSAKey
	{
		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000043 RID: 67 RVA: 0x00002F6B File Offset: 0x0000116B
		// (set) Token: 0x06000044 RID: 68 RVA: 0x00002F73 File Offset: 0x00001173
		public RSACryptoServiceProvider rsa
		{
			[CompilerGenerated]
			get
			{
				return this.<rsa>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<rsa>k__BackingField = value;
			}
		}

		// Token: 0x06000045 RID: 69 RVA: 0x00002F7C File Offset: 0x0000117C
		public RSAKey(Asn1Node n)
		{
			this.rsa = this.ParseNode(n);
		}

		// Token: 0x06000046 RID: 70 RVA: 0x00002F94 File Offset: 0x00001194
		public RSAKey(byte[] data)
		{
			using (MemoryStream memoryStream = new MemoryStream(data))
			{
				Asn1Parser asn1Parser = new Asn1Parser();
				asn1Parser.LoadData(memoryStream);
				this.rsa = this.ParseNode(asn1Parser.RootNode);
			}
		}

		// Token: 0x06000047 RID: 71 RVA: 0x00002FF0 File Offset: 0x000011F0
		public bool Verify(byte[] message, byte[] signature)
		{
			SHA1Managed sha1Managed = new SHA1Managed();
			byte[] rgbHash = sha1Managed.ComputeHash(message);
			return this.rsa.VerifyHash(rgbHash, null, signature);
		}

		// Token: 0x06000048 RID: 72 RVA: 0x00003020 File Offset: 0x00001220
		private RSACryptoServiceProvider ParseNode(Asn1Node n)
		{
			bool flag = (n.Tag & 31) == 16 && n.ChildNodeCount == 2L && (n.GetChildNode(0).Tag & 31) == 16 && (n.GetChildNode(0).GetChildNode(0).Tag & 31) == 6 && n.GetChildNode(0).GetChildNode(0).GetDataStr(false) == "1.2.840.113549.1.1.1" && (n.GetChildNode(1).Tag & 31) == 3;
			if (flag)
			{
				Asn1Node childNode = n.GetChildNode(1).GetChildNode(0);
				bool flag2 = childNode.ChildNodeCount == 2L;
				if (flag2)
				{
					byte[] data = childNode.GetChildNode(0).Data;
					byte[] array = new byte[data.Length - 1];
					Array.Copy(data, 1, array, 0, data.Length - 1);
					string modulus = Convert.ToBase64String(array);
					string exponent = Convert.ToBase64String(childNode.GetChildNode(1).Data);
					RSACryptoServiceProvider rsacryptoServiceProvider = new RSACryptoServiceProvider();
					rsacryptoServiceProvider.FromXmlString(this.ToXML(modulus, exponent));
					return rsacryptoServiceProvider;
				}
			}
			throw new InvalidRSAData();
		}

		// Token: 0x06000049 RID: 73 RVA: 0x00003138 File Offset: 0x00001338
		private string ToXML(string modulus, string exponent)
		{
			return string.Concat(new string[]
			{
				"<RSAKeyValue><Modulus>",
				modulus,
				"</Modulus><Exponent>",
				exponent,
				"</Exponent></RSAKeyValue>"
			});
		}

		// Token: 0x0400001A RID: 26
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private RSACryptoServiceProvider <rsa>k__BackingField;
	}
}
