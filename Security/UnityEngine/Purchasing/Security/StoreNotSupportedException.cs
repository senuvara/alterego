﻿using System;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x02000016 RID: 22
	public class StoreNotSupportedException : IAPSecurityException
	{
		// Token: 0x0600007F RID: 127 RVA: 0x00003BD2 File Offset: 0x00001DD2
		public StoreNotSupportedException(string message) : base(message)
		{
		}
	}
}
