﻿using System;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x02000009 RID: 9
	public class InvalidPKCS7Data : IAPSecurityException
	{
		// Token: 0x0600003E RID: 62 RVA: 0x000028C3 File Offset: 0x00000AC3
		public InvalidPKCS7Data()
		{
		}
	}
}
