﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x02000010 RID: 16
	internal class GooglePlayValidator
	{
		// Token: 0x0600004B RID: 75 RVA: 0x00003175 File Offset: 0x00001375
		public GooglePlayValidator(byte[] rsaKey)
		{
			this.key = new RSAKey(rsaKey);
		}

		// Token: 0x0600004C RID: 76 RVA: 0x0000318C File Offset: 0x0000138C
		public GooglePlayReceipt Validate(string receipt, string signature)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(receipt);
			byte[] signature2 = Convert.FromBase64String(signature);
			bool flag = !this.key.Verify(bytes, signature2);
			if (flag)
			{
				throw new InvalidSignatureException();
			}
			Dictionary<string, object> dictionary = (Dictionary<string, object>)MiniJson.JsonDecode(receipt);
			object obj;
			dictionary.TryGetValue("orderId", out obj);
			object obj2;
			dictionary.TryGetValue("packageName", out obj2);
			object obj3;
			dictionary.TryGetValue("productId", out obj3);
			object obj4;
			dictionary.TryGetValue("purchaseToken", out obj4);
			object obj5;
			dictionary.TryGetValue("purchaseTime", out obj5);
			object obj6;
			dictionary.TryGetValue("purchaseState", out obj6);
			DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
			DateTime purchaseTime = dateTime.AddMilliseconds((double)((long)obj5));
			GooglePurchaseState purchaseState = (GooglePurchaseState)((long)obj6);
			return new GooglePlayReceipt((string)obj3, (string)obj, (string)obj2, (string)obj4, purchaseTime, purchaseState);
		}

		// Token: 0x0400001B RID: 27
		private RSAKey key;
	}
}
