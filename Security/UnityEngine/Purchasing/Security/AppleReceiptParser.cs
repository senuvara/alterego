﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using LipingShare.LCLib.Asn1Processor;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x02000012 RID: 18
	public class AppleReceiptParser
	{
		// Token: 0x0600004F RID: 79 RVA: 0x000032E8 File Offset: 0x000014E8
		public AppleReceipt Parse(byte[] receiptData)
		{
			PKCS7 pkcs;
			return this.Parse(receiptData, out pkcs);
		}

		// Token: 0x06000050 RID: 80 RVA: 0x00003304 File Offset: 0x00001504
		internal AppleReceipt Parse(byte[] receiptData, out PKCS7 receipt)
		{
			bool flag = AppleReceiptParser._mostRecentReceiptData.ContainsKey("k_AppleReceiptKey") && AppleReceiptParser._mostRecentReceiptData.ContainsKey("k_PKCS7Key") && AppleReceiptParser._mostRecentReceiptData.ContainsKey("k_ReceiptBytesKey") && AppleReceiptParser.ArrayEquals<byte>(receiptData, (byte[])AppleReceiptParser._mostRecentReceiptData["k_ReceiptBytesKey"]);
			AppleReceipt result;
			if (flag)
			{
				receipt = (PKCS7)AppleReceiptParser._mostRecentReceiptData["k_PKCS7Key"];
				result = (AppleReceipt)AppleReceiptParser._mostRecentReceiptData["k_AppleReceiptKey"];
			}
			else
			{
				using (MemoryStream memoryStream = new MemoryStream(receiptData))
				{
					Asn1Parser asn1Parser = new Asn1Parser();
					asn1Parser.LoadData(memoryStream);
					receipt = new PKCS7(asn1Parser.RootNode);
					AppleReceipt appleReceipt = this.ParseReceipt(receipt.data);
					AppleReceiptParser._mostRecentReceiptData["k_AppleReceiptKey"] = appleReceipt;
					AppleReceiptParser._mostRecentReceiptData["k_PKCS7Key"] = receipt;
					AppleReceiptParser._mostRecentReceiptData["k_ReceiptBytesKey"] = receiptData;
					result = appleReceipt;
				}
			}
			return result;
		}

		// Token: 0x06000051 RID: 81 RVA: 0x0000341C File Offset: 0x0000161C
		private AppleReceipt ParseReceipt(Asn1Node data)
		{
			bool flag = data == null || data.ChildNodeCount != 1L;
			if (flag)
			{
				throw new InvalidPKCS7Data();
			}
			Asn1Node childNode = data.GetChildNode(0);
			AppleReceipt appleReceipt = new AppleReceipt();
			List<AppleInAppPurchaseReceipt> list = new List<AppleInAppPurchaseReceipt>();
			int num = 0;
			while ((long)num < childNode.ChildNodeCount)
			{
				Asn1Node childNode2 = childNode.GetChildNode(num);
				bool flag2 = childNode2.ChildNodeCount == 3L;
				if (flag2)
				{
					long num2 = Asn1Util.BytesToLong(childNode2.GetChildNode(0).Data);
					Asn1Node childNode3 = childNode2.GetChildNode(2);
					long num3 = num2;
					if (num3 <= 12L)
					{
						long num4 = num3 - 2L;
						if (num4 <= 3L)
						{
							switch ((uint)num4)
							{
							case 0U:
								appleReceipt.bundleID = Encoding.UTF8.GetString(childNode3.GetChildNode(0).Data);
								goto IL_1A2;
							case 1U:
								appleReceipt.appVersion = Encoding.UTF8.GetString(childNode3.GetChildNode(0).Data);
								goto IL_1A2;
							case 2U:
								appleReceipt.opaque = childNode3.Data;
								goto IL_1A2;
							case 3U:
								appleReceipt.hash = childNode3.Data;
								goto IL_1A2;
							}
						}
						if (num3 == 12L)
						{
							string @string = Encoding.UTF8.GetString(childNode3.GetChildNode(0).Data);
							appleReceipt.receiptCreationDate = DateTime.Parse(@string).ToUniversalTime();
						}
					}
					else if (num3 != 17L)
					{
						if (num3 == 19L)
						{
							appleReceipt.originalApplicationVersion = Encoding.UTF8.GetString(childNode3.GetChildNode(0).Data);
						}
					}
					else
					{
						list.Add(this.ParseInAppReceipt(childNode3.GetChildNode(0)));
					}
					IL_1A2:;
				}
				num++;
			}
			appleReceipt.inAppPurchaseReceipts = list.ToArray();
			return appleReceipt;
		}

		// Token: 0x06000052 RID: 82 RVA: 0x000035FC File Offset: 0x000017FC
		private AppleInAppPurchaseReceipt ParseInAppReceipt(Asn1Node inApp)
		{
			AppleInAppPurchaseReceipt appleInAppPurchaseReceipt = new AppleInAppPurchaseReceipt();
			int num = 0;
			while ((long)num < inApp.ChildNodeCount)
			{
				Asn1Node childNode = inApp.GetChildNode(num);
				bool flag = childNode.ChildNodeCount == 3L;
				if (flag)
				{
					long num2 = Asn1Util.BytesToLong(childNode.GetChildNode(0).Data);
					Asn1Node childNode2 = childNode.GetChildNode(2);
					long num3 = num2;
					long num4 = num3 - 1701L;
					if (num4 <= 18L)
					{
						switch ((uint)num4)
						{
						case 0U:
							appleInAppPurchaseReceipt.quantity = (int)Asn1Util.BytesToLong(childNode2.GetChildNode(0).Data);
							break;
						case 1U:
							appleInAppPurchaseReceipt.productID = Encoding.UTF8.GetString(childNode2.GetChildNode(0).Data);
							break;
						case 2U:
							appleInAppPurchaseReceipt.transactionID = Encoding.UTF8.GetString(childNode2.GetChildNode(0).Data);
							break;
						case 3U:
							appleInAppPurchaseReceipt.purchaseDate = AppleReceiptParser.TryParseDateTimeNode(childNode2);
							break;
						case 4U:
							appleInAppPurchaseReceipt.originalTransactionIdentifier = Encoding.UTF8.GetString(childNode2.GetChildNode(0).Data);
							break;
						case 5U:
							appleInAppPurchaseReceipt.originalPurchaseDate = AppleReceiptParser.TryParseDateTimeNode(childNode2);
							break;
						case 6U:
							appleInAppPurchaseReceipt.productType = (int)Asn1Util.BytesToLong(childNode2.GetChildNode(0).Data);
							break;
						case 7U:
							appleInAppPurchaseReceipt.subscriptionExpirationDate = AppleReceiptParser.TryParseDateTimeNode(childNode2);
							break;
						case 11U:
							appleInAppPurchaseReceipt.cancellationDate = AppleReceiptParser.TryParseDateTimeNode(childNode2);
							break;
						case 12U:
							appleInAppPurchaseReceipt.isFreeTrial = (int)Asn1Util.BytesToLong(childNode2.GetChildNode(0).Data);
							break;
						case 18U:
							appleInAppPurchaseReceipt.isIntroductoryPricePeriod = (int)Asn1Util.BytesToLong(childNode2.GetChildNode(0).Data);
							break;
						}
					}
				}
				num++;
			}
			return appleInAppPurchaseReceipt;
		}

		// Token: 0x06000053 RID: 83 RVA: 0x00003800 File Offset: 0x00001A00
		private static DateTime TryParseDateTimeNode(Asn1Node node)
		{
			string @string = Encoding.UTF8.GetString(node.GetChildNode(0).Data);
			bool flag = !string.IsNullOrEmpty(@string);
			DateTime result;
			if (flag)
			{
				result = DateTime.Parse(@string).ToUniversalTime();
			}
			else
			{
				result = DateTime.MinValue;
			}
			return result;
		}

		// Token: 0x06000054 RID: 84 RVA: 0x00003850 File Offset: 0x00001A50
		public static bool ArrayEquals<T>(T[] a, T[] b) where T : IEquatable<T>
		{
			bool flag = a.Length != b.Length;
			bool result;
			if (flag)
			{
				result = false;
			}
			else
			{
				for (int i = 0; i < a.Length; i++)
				{
					bool flag2 = !a[i].Equals(b[i]);
					if (flag2)
					{
						return false;
					}
				}
				result = true;
			}
			return result;
		}

		// Token: 0x06000055 RID: 85 RVA: 0x000038B4 File Offset: 0x00001AB4
		public AppleReceiptParser()
		{
		}

		// Token: 0x06000056 RID: 86 RVA: 0x000038BD File Offset: 0x00001ABD
		// Note: this type is marked as 'beforefieldinit'.
		static AppleReceiptParser()
		{
		}

		// Token: 0x0400001E RID: 30
		private static Dictionary<string, object> _mostRecentReceiptData = new Dictionary<string, object>();

		// Token: 0x0400001F RID: 31
		private const string k_AppleReceiptKey = "k_AppleReceiptKey";

		// Token: 0x04000020 RID: 32
		private const string k_PKCS7Key = "k_PKCS7Key";

		// Token: 0x04000021 RID: 33
		private const string k_ReceiptBytesKey = "k_ReceiptBytesKey";
	}
}
