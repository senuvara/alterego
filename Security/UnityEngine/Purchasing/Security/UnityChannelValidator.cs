﻿using System;
using System.Text;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x02000021 RID: 33
	public class UnityChannelValidator
	{
		// Token: 0x060000B1 RID: 177 RVA: 0x000040E8 File Offset: 0x000022E8
		public UnityChannelValidator(byte[] rsaKey)
		{
			try
			{
				this.key = new RSAKey(rsaKey);
			}
			catch (Exception arg)
			{
				throw new Exception("Cannot instantiate self with an invalid public key. (" + arg + ")");
			}
		}

		// Token: 0x060000B2 RID: 178 RVA: 0x00004140 File Offset: 0x00002340
		public UnityChannelReceipt Validate(string receipt, string signature)
		{
			byte[] signature2 = Convert.FromBase64String(signature);
			byte[] bytes = Encoding.UTF8.GetBytes(receipt);
			bool flag = !this.key.Verify(bytes, signature2);
			if (flag)
			{
				throw new InvalidSignatureException();
			}
			return this.parser.ParseUnityChannelReceipt(receipt);
		}

		// Token: 0x0400004F RID: 79
		private readonly RSAKey key;

		// Token: 0x04000050 RID: 80
		private UnityChannelReceiptParser parser = new UnityChannelReceiptParser();
	}
}
