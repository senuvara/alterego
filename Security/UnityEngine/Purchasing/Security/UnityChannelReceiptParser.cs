﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x02000022 RID: 34
	public class UnityChannelReceiptParser
	{
		// Token: 0x060000B3 RID: 179 RVA: 0x0000418C File Offset: 0x0000238C
		public UnityChannelReceipt ParseUnityChannelReceipt(string receipt)
		{
			Dictionary<string, object> dictionary = (Dictionary<string, object>)MiniJson.JsonDecode(receipt);
			object obj;
			dictionary.TryGetValue("cpOrderId", out obj);
			object obj2 = "";
			object obj3;
			dictionary.TryGetValue("productId", out obj3);
			object obj4;
			dictionary.TryGetValue("paidTime", out obj4);
			object obj5;
			dictionary.TryGetValue("quantity", out obj5);
			object obj6;
			dictionary.TryGetValue("status", out obj6);
			object obj7;
			dictionary.TryGetValue("clientId", out obj7);
			object obj8;
			dictionary.TryGetValue("payFee", out obj8);
			object obj9;
			dictionary.TryGetValue("orderAttemptId", out obj9);
			object obj10;
			dictionary.TryGetValue("country", out obj10);
			object obj11;
			dictionary.TryGetValue("currency", out obj11);
			DateTime purchaseDate = default(DateTime);
			bool flag = DateTime.TryParse((string)obj4, out purchaseDate);
			bool flag2 = !flag;
			if (flag2)
			{
				purchaseDate = DateTime.MinValue;
			}
			return new UnityChannelReceipt((string)obj, (string)obj2, (string)obj3, purchaseDate, (string)obj6, (string)obj7, (string)obj8, (string)obj9, (string)obj10, (string)obj11, (string)obj5);
		}

		// Token: 0x060000B4 RID: 180 RVA: 0x000038B4 File Offset: 0x00001AB4
		public UnityChannelReceiptParser()
		{
		}
	}
}
