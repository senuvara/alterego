﻿using System;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x02000011 RID: 17
	public class AppleValidator
	{
		// Token: 0x0600004D RID: 77 RVA: 0x00003281 File Offset: 0x00001481
		public AppleValidator(byte[] appleRootCertificate)
		{
			this.cert = new X509Cert(appleRootCertificate);
		}

		// Token: 0x0600004E RID: 78 RVA: 0x000032A4 File Offset: 0x000014A4
		public AppleReceipt Validate(byte[] receiptData)
		{
			PKCS7 pkcs;
			AppleReceipt appleReceipt = this.parser.Parse(receiptData, out pkcs);
			bool flag = !pkcs.Verify(this.cert, appleReceipt.receiptCreationDate);
			if (flag)
			{
				throw new InvalidSignatureException();
			}
			return appleReceipt;
		}

		// Token: 0x0400001C RID: 28
		private X509Cert cert;

		// Token: 0x0400001D RID: 29
		private AppleReceiptParser parser = new AppleReceiptParser();
	}
}
