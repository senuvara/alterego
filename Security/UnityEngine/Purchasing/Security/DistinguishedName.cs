﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;
using LipingShare.LCLib.Asn1Processor;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x02000002 RID: 2
	internal class DistinguishedName
	{
		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		// (set) Token: 0x06000002 RID: 2 RVA: 0x00002058 File Offset: 0x00000258
		public string Country
		{
			[CompilerGenerated]
			get
			{
				return this.<Country>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Country>k__BackingField = value;
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000003 RID: 3 RVA: 0x00002061 File Offset: 0x00000261
		// (set) Token: 0x06000004 RID: 4 RVA: 0x00002069 File Offset: 0x00000269
		public string Organization
		{
			[CompilerGenerated]
			get
			{
				return this.<Organization>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Organization>k__BackingField = value;
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000005 RID: 5 RVA: 0x00002072 File Offset: 0x00000272
		// (set) Token: 0x06000006 RID: 6 RVA: 0x0000207A File Offset: 0x0000027A
		public string OrganizationalUnit
		{
			[CompilerGenerated]
			get
			{
				return this.<OrganizationalUnit>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<OrganizationalUnit>k__BackingField = value;
			}
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000007 RID: 7 RVA: 0x00002083 File Offset: 0x00000283
		// (set) Token: 0x06000008 RID: 8 RVA: 0x0000208B File Offset: 0x0000028B
		public string Dnq
		{
			[CompilerGenerated]
			get
			{
				return this.<Dnq>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Dnq>k__BackingField = value;
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000009 RID: 9 RVA: 0x00002094 File Offset: 0x00000294
		// (set) Token: 0x0600000A RID: 10 RVA: 0x0000209C File Offset: 0x0000029C
		public string State
		{
			[CompilerGenerated]
			get
			{
				return this.<State>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<State>k__BackingField = value;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x0600000B RID: 11 RVA: 0x000020A5 File Offset: 0x000002A5
		// (set) Token: 0x0600000C RID: 12 RVA: 0x000020AD File Offset: 0x000002AD
		public string CommonName
		{
			[CompilerGenerated]
			get
			{
				return this.<CommonName>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<CommonName>k__BackingField = value;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600000D RID: 13 RVA: 0x000020B6 File Offset: 0x000002B6
		// (set) Token: 0x0600000E RID: 14 RVA: 0x000020BE File Offset: 0x000002BE
		public string SerialNumber
		{
			[CompilerGenerated]
			get
			{
				return this.<SerialNumber>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<SerialNumber>k__BackingField = value;
			}
		}

		// Token: 0x0600000F RID: 15 RVA: 0x000020C8 File Offset: 0x000002C8
		public DistinguishedName(Asn1Node n)
		{
			bool flag = n.MaskedTag == 16;
			if (flag)
			{
				int num = 0;
				while ((long)num < n.ChildNodeCount)
				{
					Asn1Node childNode = n.GetChildNode(num);
					bool flag2 = childNode.MaskedTag != 17 || childNode.ChildNodeCount != 1L;
					if (flag2)
					{
						throw new InvalidX509Data();
					}
					childNode = childNode.GetChildNode(0);
					bool flag3 = childNode.MaskedTag != 16 || childNode.ChildNodeCount != 2L;
					if (flag3)
					{
						throw new InvalidX509Data();
					}
					Asn1Node childNode2 = childNode.GetChildNode(0);
					Asn1Node childNode3 = childNode.GetChildNode(1);
					bool flag4 = childNode2.MaskedTag != 6 || (childNode3.MaskedTag != 19 && childNode3.MaskedTag != 12);
					if (flag4)
					{
						throw new InvalidX509Data();
					}
					Oid oid = new Oid();
					string text = oid.Decode(childNode2.Data);
					UTF8Encoding utf8Encoding = new UTF8Encoding();
					string text2 = text;
					uint num2 = <PrivateImplementationDetails>.ComputeStringHash(text2);
					if (num2 <= 184344010U)
					{
						if (num2 != 134011153U)
						{
							if (num2 != 167566391U)
							{
								if (num2 == 184344010U)
								{
									if (text2 == "2.5.4.6")
									{
										this.Country = utf8Encoding.GetString(childNode3.Data);
									}
								}
							}
							else if (text2 == "2.5.4.5")
							{
								this.SerialNumber = Asn1Util.ToHexString(childNode3.Data);
							}
						}
						else if (text2 == "2.5.4.3")
						{
							this.CommonName = utf8Encoding.GetString(childNode3.Data);
						}
					}
					else if (num2 <= 1208264641U)
					{
						if (num2 != 1191487022U)
						{
							if (num2 == 1208264641U)
							{
								if (text2 == "2.5.4.10")
								{
									this.Organization = utf8Encoding.GetString(childNode3.Data);
								}
							}
						}
						else if (text2 == "2.5.4.11")
						{
							this.OrganizationalUnit = utf8Encoding.GetString(childNode3.Data);
						}
					}
					else if (num2 != 3087799254U)
					{
						if (num2 == 4244424640U)
						{
							if (text2 == "2.5.4.8")
							{
								this.State = utf8Encoding.GetString(childNode3.Data);
							}
						}
					}
					else if (text2 == "2.5.4.46")
					{
						this.Dnq = utf8Encoding.GetString(childNode3.Data);
					}
					num++;
				}
			}
		}

		// Token: 0x06000010 RID: 16 RVA: 0x0000238C File Offset: 0x0000058C
		public bool Equals(DistinguishedName n2)
		{
			return this.Organization == n2.Organization && this.OrganizationalUnit == n2.OrganizationalUnit && this.Dnq == n2.Dnq && this.Country == n2.Country && this.State == n2.State && this.CommonName == n2.CommonName;
		}

		// Token: 0x06000011 RID: 17 RVA: 0x00002414 File Offset: 0x00000614
		public override string ToString()
		{
			return string.Concat(new string[]
			{
				"CN: ",
				this.CommonName,
				"\nON: ",
				this.Organization,
				"\nUnit Name: ",
				this.OrganizationalUnit,
				"\nCountry: ",
				this.Country
			});
		}

		// Token: 0x04000001 RID: 1
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <Country>k__BackingField;

		// Token: 0x04000002 RID: 2
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <Organization>k__BackingField;

		// Token: 0x04000003 RID: 3
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <OrganizationalUnit>k__BackingField;

		// Token: 0x04000004 RID: 4
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <Dnq>k__BackingField;

		// Token: 0x04000005 RID: 5
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <State>k__BackingField;

		// Token: 0x04000006 RID: 6
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <CommonName>k__BackingField;

		// Token: 0x04000007 RID: 7
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <SerialNumber>k__BackingField;
	}
}
