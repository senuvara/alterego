﻿using System;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x02000007 RID: 7
	public class IAPSecurityException : Exception
	{
		// Token: 0x0600003B RID: 59 RVA: 0x00002F56 File Offset: 0x00001156
		public IAPSecurityException()
		{
		}

		// Token: 0x0600003C RID: 60 RVA: 0x00002F60 File Offset: 0x00001160
		public IAPSecurityException(string message) : base(message)
		{
		}
	}
}
