﻿using System;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x02000018 RID: 24
	public class InvalidReceiptDataException : IAPSecurityException
	{
		// Token: 0x06000081 RID: 129 RVA: 0x000028C3 File Offset: 0x00000AC3
		public InvalidReceiptDataException()
		{
		}
	}
}
