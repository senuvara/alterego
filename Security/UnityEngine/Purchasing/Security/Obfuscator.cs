﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x02000015 RID: 21
	public static class Obfuscator
	{
		// Token: 0x0600007D RID: 125 RVA: 0x000039FC File Offset: 0x00001BFC
		public static byte[] DeObfuscate(byte[] data, int[] order, int key)
		{
			byte[] array = new byte[data.Length];
			int num = data.Length / 20 + 1;
			bool flag = data.Length % 20 != 0;
			Array.Copy(data, array, data.Length);
			for (int i = order.Length - 1; i >= 0; i--)
			{
				int num2 = order[i];
				int num3 = (flag && num2 == num - 1) ? (data.Length % 20) : 20;
				byte[] sourceArray = array.Skip(i * 20).Take(num3).ToArray<byte>();
				Array.Copy(array, num2 * 20, array, i * 20, num3);
				Array.Copy(sourceArray, 0, array, num2 * 20, num3);
			}
			return (from x in array
			select (byte)((int)x ^ key)).ToArray<byte>();
		}

		// Token: 0x0600007E RID: 126 RVA: 0x00003AD4 File Offset: 0x00001CD4
		public static byte[] Obfuscate(byte[] data, int[] order, out int rkey)
		{
			Random random = new Random();
			int key = random.Next(2, 255);
			byte[] array = new byte[data.Length];
			int num = data.Length / 20 + 1;
			bool flag = order == null || order.Length < num;
			if (flag)
			{
				throw new Obfuscator.InvalidOrderArray();
			}
			Array.Copy(data, array, data.Length);
			for (int i = 0; i < num - 1; i++)
			{
				int num2 = random.Next(i, num - 1);
				order[i] = num2;
				int num3 = 20;
				byte[] sourceArray = array.Skip(i * 20).Take(num3).ToArray<byte>();
				Array.Copy(array, num2 * 20, array, i * 20, num3);
				Array.Copy(sourceArray, 0, array, num2 * 20, num3);
			}
			order[num - 1] = num - 1;
			rkey = key;
			return (from x in array
			select (byte)((int)x ^ key)).ToArray<byte>();
		}

		// Token: 0x0200002D RID: 45
		public class InvalidOrderArray : Exception
		{
			// Token: 0x0600014D RID: 333 RVA: 0x00007A06 File Offset: 0x00005C06
			public InvalidOrderArray()
			{
			}
		}

		// Token: 0x0200002E RID: 46
		[CompilerGenerated]
		private sealed class <>c__DisplayClass1_0
		{
			// Token: 0x0600014E RID: 334 RVA: 0x000038B4 File Offset: 0x00001AB4
			public <>c__DisplayClass1_0()
			{
			}

			// Token: 0x0600014F RID: 335 RVA: 0x00007A0F File Offset: 0x00005C0F
			internal byte <DeObfuscate>b__0(byte x)
			{
				return (byte)((int)x ^ this.key);
			}

			// Token: 0x0400008B RID: 139
			public int key;
		}

		// Token: 0x0200002F RID: 47
		[CompilerGenerated]
		private sealed class <>c__DisplayClass2_0
		{
			// Token: 0x06000150 RID: 336 RVA: 0x000038B4 File Offset: 0x00001AB4
			public <>c__DisplayClass2_0()
			{
			}

			// Token: 0x06000151 RID: 337 RVA: 0x00007A1A File Offset: 0x00005C1A
			internal byte <Obfuscate>b__0(byte x)
			{
				return (byte)((int)x ^ this.key);
			}

			// Token: 0x0400008C RID: 140
			public int key;
		}
	}
}
