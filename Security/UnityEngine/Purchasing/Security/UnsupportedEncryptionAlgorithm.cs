﻿using System;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x0200000D RID: 13
	public class UnsupportedEncryptionAlgorithm : IAPSecurityException
	{
		// Token: 0x06000042 RID: 66 RVA: 0x000028C3 File Offset: 0x00000AC3
		public UnsupportedEncryptionAlgorithm()
		{
		}
	}
}
