﻿using System;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x02000008 RID: 8
	public class InvalidSignatureException : IAPSecurityException
	{
		// Token: 0x0600003D RID: 61 RVA: 0x000028C3 File Offset: 0x00000AC3
		public InvalidSignatureException()
		{
		}
	}
}
