﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using LipingShare.LCLib.Asn1Processor;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x02000003 RID: 3
	internal class X509Cert
	{
		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000012 RID: 18 RVA: 0x00002475 File Offset: 0x00000675
		// (set) Token: 0x06000013 RID: 19 RVA: 0x0000247D File Offset: 0x0000067D
		public string SerialNumber
		{
			[CompilerGenerated]
			get
			{
				return this.<SerialNumber>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<SerialNumber>k__BackingField = value;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000014 RID: 20 RVA: 0x00002486 File Offset: 0x00000686
		// (set) Token: 0x06000015 RID: 21 RVA: 0x0000248E File Offset: 0x0000068E
		public DateTime ValidAfter
		{
			[CompilerGenerated]
			get
			{
				return this.<ValidAfter>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ValidAfter>k__BackingField = value;
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000016 RID: 22 RVA: 0x00002497 File Offset: 0x00000697
		// (set) Token: 0x06000017 RID: 23 RVA: 0x0000249F File Offset: 0x0000069F
		public DateTime ValidBefore
		{
			[CompilerGenerated]
			get
			{
				return this.<ValidBefore>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ValidBefore>k__BackingField = value;
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000018 RID: 24 RVA: 0x000024A8 File Offset: 0x000006A8
		// (set) Token: 0x06000019 RID: 25 RVA: 0x000024B0 File Offset: 0x000006B0
		public RSAKey PubKey
		{
			[CompilerGenerated]
			get
			{
				return this.<PubKey>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<PubKey>k__BackingField = value;
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x0600001A RID: 26 RVA: 0x000024B9 File Offset: 0x000006B9
		// (set) Token: 0x0600001B RID: 27 RVA: 0x000024C1 File Offset: 0x000006C1
		public bool SelfSigned
		{
			[CompilerGenerated]
			get
			{
				return this.<SelfSigned>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<SelfSigned>k__BackingField = value;
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600001C RID: 28 RVA: 0x000024CA File Offset: 0x000006CA
		// (set) Token: 0x0600001D RID: 29 RVA: 0x000024D2 File Offset: 0x000006D2
		public DistinguishedName Subject
		{
			[CompilerGenerated]
			get
			{
				return this.<Subject>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Subject>k__BackingField = value;
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x0600001E RID: 30 RVA: 0x000024DB File Offset: 0x000006DB
		// (set) Token: 0x0600001F RID: 31 RVA: 0x000024E3 File Offset: 0x000006E3
		public DistinguishedName Issuer
		{
			[CompilerGenerated]
			get
			{
				return this.<Issuer>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Issuer>k__BackingField = value;
			}
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000020 RID: 32 RVA: 0x000024EC File Offset: 0x000006EC
		// (set) Token: 0x06000021 RID: 33 RVA: 0x000024F4 File Offset: 0x000006F4
		public Asn1Node Signature
		{
			[CompilerGenerated]
			get
			{
				return this.<Signature>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Signature>k__BackingField = value;
			}
		}

		// Token: 0x06000022 RID: 34 RVA: 0x000024FD File Offset: 0x000006FD
		public X509Cert(Asn1Node n)
		{
			this.ParseNode(n);
		}

		// Token: 0x06000023 RID: 35 RVA: 0x00002510 File Offset: 0x00000710
		public X509Cert(byte[] data)
		{
			using (MemoryStream memoryStream = new MemoryStream(data))
			{
				Asn1Parser asn1Parser = new Asn1Parser();
				asn1Parser.LoadData(memoryStream);
				this.ParseNode(asn1Parser.RootNode);
			}
		}

		// Token: 0x06000024 RID: 36 RVA: 0x00002568 File Offset: 0x00000768
		public bool CheckCertTime(DateTime time)
		{
			return time.CompareTo(this.ValidAfter) >= 0 && time.CompareTo(this.ValidBefore) <= 0;
		}

		// Token: 0x06000025 RID: 37 RVA: 0x000025A0 File Offset: 0x000007A0
		public bool CheckSignature(X509Cert signer)
		{
			bool flag = this.Issuer.Equals(signer.Subject);
			return flag && signer.PubKey.Verify(this.rawTBSCertificate, this.Signature.Data);
		}

		// Token: 0x06000026 RID: 38 RVA: 0x000025E8 File Offset: 0x000007E8
		private void ParseNode(Asn1Node root)
		{
			bool flag = (root.Tag & 31) != 16 || root.ChildNodeCount != 3L;
			if (flag)
			{
				throw new InvalidX509Data();
			}
			this.TbsCertificate = root.GetChildNode(0);
			bool flag2 = this.TbsCertificate.ChildNodeCount < 7L;
			if (flag2)
			{
				throw new InvalidX509Data();
			}
			this.rawTBSCertificate = new byte[this.TbsCertificate.DataLength + 4L];
			Array.Copy(root.Data, 0, this.rawTBSCertificate, 0, this.rawTBSCertificate.Length);
			Asn1Node childNode = this.TbsCertificate.GetChildNode(1);
			bool flag3 = (childNode.Tag & 31) != 2;
			if (flag3)
			{
				throw new InvalidX509Data();
			}
			this.SerialNumber = Asn1Util.ToHexString(childNode.Data);
			this.Issuer = new DistinguishedName(this.TbsCertificate.GetChildNode(3));
			this.Subject = new DistinguishedName(this.TbsCertificate.GetChildNode(5));
			Asn1Node childNode2 = this.TbsCertificate.GetChildNode(4);
			bool flag4 = (childNode2.Tag & 31) != 16 || childNode2.ChildNodeCount != 2L;
			if (flag4)
			{
				throw new InvalidX509Data();
			}
			this.ValidAfter = this.ParseTime(childNode2.GetChildNode(0));
			this.ValidBefore = this.ParseTime(childNode2.GetChildNode(1));
			this.SelfSigned = this.Subject.Equals(this.Issuer);
			this.PubKey = new RSAKey(this.TbsCertificate.GetChildNode(6));
			this.Signature = root.GetChildNode(2);
		}

		// Token: 0x06000027 RID: 39 RVA: 0x00002780 File Offset: 0x00000980
		private DateTime ParseTime(Asn1Node n)
		{
			string @string = new UTF8Encoding().GetString(n.Data);
			bool flag = @string.Length != 13 && @string.Length != 15;
			if (flag)
			{
				throw new InvalidTimeFormat();
			}
			bool flag2 = @string[@string.Length - 1] != 'Z';
			if (flag2)
			{
				throw new InvalidTimeFormat();
			}
			int num = 0;
			bool flag3 = @string.Length == 13;
			int num2;
			if (flag3)
			{
				num2 = int.Parse(@string.Substring(0, 2));
				bool flag4 = num2 >= 50;
				if (flag4)
				{
					num2 += 1900;
				}
				else
				{
					bool flag5 = num2 < 50;
					if (flag5)
					{
						num2 += 2000;
					}
				}
				num += 2;
			}
			else
			{
				num2 = int.Parse(@string.Substring(0, 4));
				num += 4;
			}
			int month = int.Parse(@string.Substring(num, 2));
			num += 2;
			int day = int.Parse(@string.Substring(num, 2));
			num += 2;
			int hour = int.Parse(@string.Substring(num, 2));
			num += 2;
			int minute = int.Parse(@string.Substring(num, 2));
			num += 2;
			int second = int.Parse(@string.Substring(num, 2));
			num += 2;
			return new DateTime(num2, month, day, hour, minute, second, DateTimeKind.Utc);
		}

		// Token: 0x04000008 RID: 8
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <SerialNumber>k__BackingField;

		// Token: 0x04000009 RID: 9
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private DateTime <ValidAfter>k__BackingField;

		// Token: 0x0400000A RID: 10
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private DateTime <ValidBefore>k__BackingField;

		// Token: 0x0400000B RID: 11
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private RSAKey <PubKey>k__BackingField;

		// Token: 0x0400000C RID: 12
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <SelfSigned>k__BackingField;

		// Token: 0x0400000D RID: 13
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private DistinguishedName <Subject>k__BackingField;

		// Token: 0x0400000E RID: 14
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private DistinguishedName <Issuer>k__BackingField;

		// Token: 0x0400000F RID: 15
		private Asn1Node TbsCertificate;

		// Token: 0x04000010 RID: 16
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Asn1Node <Signature>k__BackingField;

		// Token: 0x04000011 RID: 17
		public byte[] rawTBSCertificate;
	}
}
