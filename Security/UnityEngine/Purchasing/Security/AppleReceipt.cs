﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x02000013 RID: 19
	public class AppleReceipt
	{
		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000057 RID: 87 RVA: 0x000038C9 File Offset: 0x00001AC9
		// (set) Token: 0x06000058 RID: 88 RVA: 0x000038D1 File Offset: 0x00001AD1
		public string bundleID
		{
			[CompilerGenerated]
			get
			{
				return this.<bundleID>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<bundleID>k__BackingField = value;
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000059 RID: 89 RVA: 0x000038DA File Offset: 0x00001ADA
		// (set) Token: 0x0600005A RID: 90 RVA: 0x000038E2 File Offset: 0x00001AE2
		public string appVersion
		{
			[CompilerGenerated]
			get
			{
				return this.<appVersion>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<appVersion>k__BackingField = value;
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x0600005B RID: 91 RVA: 0x000038EB File Offset: 0x00001AEB
		// (set) Token: 0x0600005C RID: 92 RVA: 0x000038F3 File Offset: 0x00001AF3
		public DateTime expirationDate
		{
			[CompilerGenerated]
			get
			{
				return this.<expirationDate>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<expirationDate>k__BackingField = value;
			}
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x0600005D RID: 93 RVA: 0x000038FC File Offset: 0x00001AFC
		// (set) Token: 0x0600005E RID: 94 RVA: 0x00003904 File Offset: 0x00001B04
		public byte[] opaque
		{
			[CompilerGenerated]
			get
			{
				return this.<opaque>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<opaque>k__BackingField = value;
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x0600005F RID: 95 RVA: 0x0000390D File Offset: 0x00001B0D
		// (set) Token: 0x06000060 RID: 96 RVA: 0x00003915 File Offset: 0x00001B15
		public byte[] hash
		{
			[CompilerGenerated]
			get
			{
				return this.<hash>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<hash>k__BackingField = value;
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000061 RID: 97 RVA: 0x0000391E File Offset: 0x00001B1E
		// (set) Token: 0x06000062 RID: 98 RVA: 0x00003926 File Offset: 0x00001B26
		public string originalApplicationVersion
		{
			[CompilerGenerated]
			get
			{
				return this.<originalApplicationVersion>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<originalApplicationVersion>k__BackingField = value;
			}
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x06000063 RID: 99 RVA: 0x0000392F File Offset: 0x00001B2F
		// (set) Token: 0x06000064 RID: 100 RVA: 0x00003937 File Offset: 0x00001B37
		public DateTime receiptCreationDate
		{
			[CompilerGenerated]
			get
			{
				return this.<receiptCreationDate>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<receiptCreationDate>k__BackingField = value;
			}
		}

		// Token: 0x06000065 RID: 101 RVA: 0x000038B4 File Offset: 0x00001AB4
		public AppleReceipt()
		{
		}

		// Token: 0x04000022 RID: 34
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <bundleID>k__BackingField;

		// Token: 0x04000023 RID: 35
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <appVersion>k__BackingField;

		// Token: 0x04000024 RID: 36
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private DateTime <expirationDate>k__BackingField;

		// Token: 0x04000025 RID: 37
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private byte[] <opaque>k__BackingField;

		// Token: 0x04000026 RID: 38
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private byte[] <hash>k__BackingField;

		// Token: 0x04000027 RID: 39
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <originalApplicationVersion>k__BackingField;

		// Token: 0x04000028 RID: 40
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private DateTime <receiptCreationDate>k__BackingField;

		// Token: 0x04000029 RID: 41
		public AppleInAppPurchaseReceipt[] inAppPurchaseReceipts;
	}
}
