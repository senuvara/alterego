﻿using System;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x0200001E RID: 30
	public enum GooglePurchaseState
	{
		// Token: 0x0400003B RID: 59
		Purchased,
		// Token: 0x0400003C RID: 60
		Cancelled,
		// Token: 0x0400003D RID: 61
		Refunded
	}
}
