﻿using System;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x0200001D RID: 29
	public interface IPurchaseReceipt
	{
		// Token: 0x17000029 RID: 41
		// (get) Token: 0x0600008A RID: 138
		string transactionID { get; }

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x0600008B RID: 139
		string productID { get; }

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x0600008C RID: 140
		DateTime purchaseDate { get; }
	}
}
