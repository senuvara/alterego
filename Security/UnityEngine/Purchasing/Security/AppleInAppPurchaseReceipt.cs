﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x02000014 RID: 20
	public class AppleInAppPurchaseReceipt : IPurchaseReceipt
	{
		// Token: 0x1700001E RID: 30
		// (get) Token: 0x06000066 RID: 102 RVA: 0x00003940 File Offset: 0x00001B40
		// (set) Token: 0x06000067 RID: 103 RVA: 0x00003948 File Offset: 0x00001B48
		public int quantity
		{
			[CompilerGenerated]
			get
			{
				return this.<quantity>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<quantity>k__BackingField = value;
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000068 RID: 104 RVA: 0x00003951 File Offset: 0x00001B51
		// (set) Token: 0x06000069 RID: 105 RVA: 0x00003959 File Offset: 0x00001B59
		public string productID
		{
			[CompilerGenerated]
			get
			{
				return this.<productID>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<productID>k__BackingField = value;
			}
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x0600006A RID: 106 RVA: 0x00003962 File Offset: 0x00001B62
		// (set) Token: 0x0600006B RID: 107 RVA: 0x0000396A File Offset: 0x00001B6A
		public string transactionID
		{
			[CompilerGenerated]
			get
			{
				return this.<transactionID>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<transactionID>k__BackingField = value;
			}
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x0600006C RID: 108 RVA: 0x00003973 File Offset: 0x00001B73
		// (set) Token: 0x0600006D RID: 109 RVA: 0x0000397B File Offset: 0x00001B7B
		public string originalTransactionIdentifier
		{
			[CompilerGenerated]
			get
			{
				return this.<originalTransactionIdentifier>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<originalTransactionIdentifier>k__BackingField = value;
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x0600006E RID: 110 RVA: 0x00003984 File Offset: 0x00001B84
		// (set) Token: 0x0600006F RID: 111 RVA: 0x0000398C File Offset: 0x00001B8C
		public DateTime purchaseDate
		{
			[CompilerGenerated]
			get
			{
				return this.<purchaseDate>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<purchaseDate>k__BackingField = value;
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x06000070 RID: 112 RVA: 0x00003995 File Offset: 0x00001B95
		// (set) Token: 0x06000071 RID: 113 RVA: 0x0000399D File Offset: 0x00001B9D
		public DateTime originalPurchaseDate
		{
			[CompilerGenerated]
			get
			{
				return this.<originalPurchaseDate>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<originalPurchaseDate>k__BackingField = value;
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x06000072 RID: 114 RVA: 0x000039A6 File Offset: 0x00001BA6
		// (set) Token: 0x06000073 RID: 115 RVA: 0x000039AE File Offset: 0x00001BAE
		public DateTime subscriptionExpirationDate
		{
			[CompilerGenerated]
			get
			{
				return this.<subscriptionExpirationDate>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<subscriptionExpirationDate>k__BackingField = value;
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x06000074 RID: 116 RVA: 0x000039B7 File Offset: 0x00001BB7
		// (set) Token: 0x06000075 RID: 117 RVA: 0x000039BF File Offset: 0x00001BBF
		public DateTime cancellationDate
		{
			[CompilerGenerated]
			get
			{
				return this.<cancellationDate>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<cancellationDate>k__BackingField = value;
			}
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000076 RID: 118 RVA: 0x000039C8 File Offset: 0x00001BC8
		// (set) Token: 0x06000077 RID: 119 RVA: 0x000039D0 File Offset: 0x00001BD0
		public int isFreeTrial
		{
			[CompilerGenerated]
			get
			{
				return this.<isFreeTrial>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<isFreeTrial>k__BackingField = value;
			}
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000078 RID: 120 RVA: 0x000039D9 File Offset: 0x00001BD9
		// (set) Token: 0x06000079 RID: 121 RVA: 0x000039E1 File Offset: 0x00001BE1
		public int productType
		{
			[CompilerGenerated]
			get
			{
				return this.<productType>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<productType>k__BackingField = value;
			}
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x0600007A RID: 122 RVA: 0x000039EA File Offset: 0x00001BEA
		// (set) Token: 0x0600007B RID: 123 RVA: 0x000039F2 File Offset: 0x00001BF2
		public int isIntroductoryPricePeriod
		{
			[CompilerGenerated]
			get
			{
				return this.<isIntroductoryPricePeriod>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<isIntroductoryPricePeriod>k__BackingField = value;
			}
		}

		// Token: 0x0600007C RID: 124 RVA: 0x000038B4 File Offset: 0x00001AB4
		public AppleInAppPurchaseReceipt()
		{
		}

		// Token: 0x0400002A RID: 42
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <quantity>k__BackingField;

		// Token: 0x0400002B RID: 43
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <productID>k__BackingField;

		// Token: 0x0400002C RID: 44
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <transactionID>k__BackingField;

		// Token: 0x0400002D RID: 45
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <originalTransactionIdentifier>k__BackingField;

		// Token: 0x0400002E RID: 46
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private DateTime <purchaseDate>k__BackingField;

		// Token: 0x0400002F RID: 47
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private DateTime <originalPurchaseDate>k__BackingField;

		// Token: 0x04000030 RID: 48
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private DateTime <subscriptionExpirationDate>k__BackingField;

		// Token: 0x04000031 RID: 49
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private DateTime <cancellationDate>k__BackingField;

		// Token: 0x04000032 RID: 50
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <isFreeTrial>k__BackingField;

		// Token: 0x04000033 RID: 51
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <productType>k__BackingField;

		// Token: 0x04000034 RID: 52
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <isIntroductoryPricePeriod>k__BackingField;
	}
}
