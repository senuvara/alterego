﻿using System;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x02000019 RID: 25
	public class MissingStoreSecretException : IAPSecurityException
	{
		// Token: 0x06000082 RID: 130 RVA: 0x00003BD2 File Offset: 0x00001DD2
		public MissingStoreSecretException(string message) : base(message)
		{
		}
	}
}
