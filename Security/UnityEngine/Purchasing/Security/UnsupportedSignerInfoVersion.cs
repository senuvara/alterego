﻿using System;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x0200000B RID: 11
	public class UnsupportedSignerInfoVersion : IAPSecurityException
	{
		// Token: 0x06000040 RID: 64 RVA: 0x000028C3 File Offset: 0x00000AC3
		public UnsupportedSignerInfoVersion()
		{
		}
	}
}
