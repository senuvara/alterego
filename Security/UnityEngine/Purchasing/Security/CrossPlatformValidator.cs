﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x0200001C RID: 28
	public class CrossPlatformValidator
	{
		// Token: 0x06000085 RID: 133 RVA: 0x00003BDD File Offset: 0x00001DDD
		public CrossPlatformValidator(byte[] googlePublicKey, byte[] appleRootCert, string appBundleId) : this(googlePublicKey, appleRootCert, null, appBundleId, appBundleId, null)
		{
		}

		// Token: 0x06000086 RID: 134 RVA: 0x00003BED File Offset: 0x00001DED
		public CrossPlatformValidator(byte[] googlePublicKey, byte[] appleRootCert, byte[] unityChannelPublicKey, string appBundleId) : this(googlePublicKey, appleRootCert, unityChannelPublicKey, appBundleId, appBundleId, appBundleId)
		{
		}

		// Token: 0x06000087 RID: 135 RVA: 0x00003C00 File Offset: 0x00001E00
		public CrossPlatformValidator(byte[] googlePublicKey, byte[] appleRootCert, string googleBundleId, string appleBundleId) : this(googlePublicKey, appleRootCert, null, googleBundleId, appleBundleId, null)
		{
		}

		// Token: 0x06000088 RID: 136 RVA: 0x00003C14 File Offset: 0x00001E14
		public CrossPlatformValidator(byte[] googlePublicKey, byte[] appleRootCert, byte[] unityChannelPublicKey, string googleBundleId, string appleBundleId, string xiaomiBundleId_not_used)
		{
			try
			{
				bool flag = googlePublicKey != null;
				if (flag)
				{
					this.google = new GooglePlayValidator(googlePublicKey);
				}
				bool flag2 = unityChannelPublicKey != null;
				if (flag2)
				{
					this.unityChannel = new UnityChannelValidator(unityChannelPublicKey);
				}
				bool flag3 = appleRootCert != null;
				if (flag3)
				{
					this.apple = new AppleValidator(appleRootCert);
				}
			}
			catch (Exception ex)
			{
				throw new InvalidPublicKeyException("Cannot instantiate self with an invalid public key. (" + ex.ToString() + ")");
			}
			this.googleBundleId = googleBundleId;
			this.appleBundleId = appleBundleId;
		}

		// Token: 0x06000089 RID: 137 RVA: 0x00003CAC File Offset: 0x00001EAC
		public IPurchaseReceipt[] Validate(string unityIAPReceipt)
		{
			IPurchaseReceipt[] result;
			try
			{
				Dictionary<string, object> dictionary = (Dictionary<string, object>)MiniJson.JsonDecode(unityIAPReceipt);
				bool flag = dictionary == null;
				if (flag)
				{
					throw new InvalidReceiptDataException();
				}
				string text = (string)dictionary["Store"];
				string text2 = (string)dictionary["Payload"];
				string a = text;
				if (!(a == "GooglePlay"))
				{
					if (!(a == "XiaomiMiPay"))
					{
						if (!(a == "AppleAppStore") && !(a == "MacAppStore"))
						{
							throw new StoreNotSupportedException("Store not supported: " + text);
						}
						bool flag2 = this.apple == null;
						if (flag2)
						{
							throw new MissingStoreSecretException("Cannot validate an Apple receipt without supplying an Apple root certificate");
						}
						AppleReceipt appleReceipt = this.apple.Validate(Convert.FromBase64String(text2));
						bool flag3 = !this.appleBundleId.Equals(appleReceipt.bundleID);
						if (flag3)
						{
							throw new InvalidBundleIdException();
						}
						result = appleReceipt.inAppPurchaseReceipts.ToArray<AppleInAppPurchaseReceipt>();
					}
					else
					{
						bool flag4 = this.unityChannel == null;
						if (flag4)
						{
							throw new MissingStoreSecretException("Cannot validate a UnityChannel receipt without a UnityChannel public key.");
						}
						Dictionary<string, object> dictionary2 = (Dictionary<string, object>)MiniJson.JsonDecode(text2);
						string receipt = (string)dictionary2["json"];
						string signature = (string)dictionary2["signature"];
						UnityChannelReceipt unityChannelReceipt = this.unityChannel.Validate(receipt, signature);
						result = new IPurchaseReceipt[]
						{
							unityChannelReceipt
						};
					}
				}
				else
				{
					bool flag5 = this.google == null;
					if (flag5)
					{
						throw new MissingStoreSecretException("Cannot validate a Google Play receipt without a Google Play public key.");
					}
					Dictionary<string, object> dictionary3 = (Dictionary<string, object>)MiniJson.JsonDecode(text2);
					string receipt2 = (string)dictionary3["json"];
					string signature2 = (string)dictionary3["signature"];
					GooglePlayReceipt googlePlayReceipt = this.google.Validate(receipt2, signature2);
					bool flag6 = !this.googleBundleId.Equals(googlePlayReceipt.packageName);
					if (flag6)
					{
						throw new InvalidBundleIdException();
					}
					result = new IPurchaseReceipt[]
					{
						googlePlayReceipt
					};
				}
			}
			catch (IAPSecurityException ex)
			{
				throw ex;
			}
			catch (Exception arg)
			{
				throw new GenericValidationException("Cannot validate due to unhandled exception. (" + arg + ")");
			}
			return result;
		}

		// Token: 0x04000035 RID: 53
		private GooglePlayValidator google;

		// Token: 0x04000036 RID: 54
		private UnityChannelValidator unityChannel;

		// Token: 0x04000037 RID: 55
		private AppleValidator apple;

		// Token: 0x04000038 RID: 56
		private string googleBundleId;

		// Token: 0x04000039 RID: 57
		private string appleBundleId;
	}
}
