﻿using System;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x02000017 RID: 23
	public class InvalidBundleIdException : IAPSecurityException
	{
		// Token: 0x06000080 RID: 128 RVA: 0x000028C3 File Offset: 0x00000AC3
		public InvalidBundleIdException()
		{
		}
	}
}
