﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x0200001F RID: 31
	public class GooglePlayReceipt : IPurchaseReceipt
	{
		// Token: 0x1700002C RID: 44
		// (get) Token: 0x0600008D RID: 141 RVA: 0x00003F10 File Offset: 0x00002110
		// (set) Token: 0x0600008E RID: 142 RVA: 0x00003F18 File Offset: 0x00002118
		public string productID
		{
			[CompilerGenerated]
			get
			{
				return this.<productID>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<productID>k__BackingField = value;
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x0600008F RID: 143 RVA: 0x00003F21 File Offset: 0x00002121
		// (set) Token: 0x06000090 RID: 144 RVA: 0x00003F29 File Offset: 0x00002129
		public string transactionID
		{
			[CompilerGenerated]
			get
			{
				return this.<transactionID>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<transactionID>k__BackingField = value;
			}
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x06000091 RID: 145 RVA: 0x00003F32 File Offset: 0x00002132
		// (set) Token: 0x06000092 RID: 146 RVA: 0x00003F3A File Offset: 0x0000213A
		public string packageName
		{
			[CompilerGenerated]
			get
			{
				return this.<packageName>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<packageName>k__BackingField = value;
			}
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x06000093 RID: 147 RVA: 0x00003F43 File Offset: 0x00002143
		// (set) Token: 0x06000094 RID: 148 RVA: 0x00003F4B File Offset: 0x0000214B
		public string purchaseToken
		{
			[CompilerGenerated]
			get
			{
				return this.<purchaseToken>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<purchaseToken>k__BackingField = value;
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x06000095 RID: 149 RVA: 0x00003F54 File Offset: 0x00002154
		// (set) Token: 0x06000096 RID: 150 RVA: 0x00003F5C File Offset: 0x0000215C
		public DateTime purchaseDate
		{
			[CompilerGenerated]
			get
			{
				return this.<purchaseDate>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<purchaseDate>k__BackingField = value;
			}
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x06000097 RID: 151 RVA: 0x00003F65 File Offset: 0x00002165
		// (set) Token: 0x06000098 RID: 152 RVA: 0x00003F6D File Offset: 0x0000216D
		public GooglePurchaseState purchaseState
		{
			[CompilerGenerated]
			get
			{
				return this.<purchaseState>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<purchaseState>k__BackingField = value;
			}
		}

		// Token: 0x06000099 RID: 153 RVA: 0x00003F76 File Offset: 0x00002176
		public GooglePlayReceipt(string productID, string transactionID, string packageName, string purchaseToken, DateTime purchaseTime, GooglePurchaseState purchaseState)
		{
			this.productID = productID;
			this.transactionID = transactionID;
			this.packageName = packageName;
			this.purchaseToken = purchaseToken;
			this.purchaseDate = purchaseTime;
			this.purchaseState = purchaseState;
		}

		// Token: 0x0400003E RID: 62
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <productID>k__BackingField;

		// Token: 0x0400003F RID: 63
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <transactionID>k__BackingField;

		// Token: 0x04000040 RID: 64
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <packageName>k__BackingField;

		// Token: 0x04000041 RID: 65
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <purchaseToken>k__BackingField;

		// Token: 0x04000042 RID: 66
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private DateTime <purchaseDate>k__BackingField;

		// Token: 0x04000043 RID: 67
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private GooglePurchaseState <purchaseState>k__BackingField;
	}
}
