﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x02000020 RID: 32
	public class UnityChannelReceipt : IPurchaseReceipt
	{
		// Token: 0x17000032 RID: 50
		// (get) Token: 0x0600009A RID: 154 RVA: 0x00003FB3 File Offset: 0x000021B3
		// (set) Token: 0x0600009B RID: 155 RVA: 0x00003FBB File Offset: 0x000021BB
		public string transactionID
		{
			[CompilerGenerated]
			get
			{
				return this.<transactionID>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<transactionID>k__BackingField = value;
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x0600009C RID: 156 RVA: 0x00003FC4 File Offset: 0x000021C4
		// (set) Token: 0x0600009D RID: 157 RVA: 0x00003FCC File Offset: 0x000021CC
		public string productID
		{
			[CompilerGenerated]
			get
			{
				return this.<productID>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<productID>k__BackingField = value;
			}
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x0600009E RID: 158 RVA: 0x00003FD5 File Offset: 0x000021D5
		// (set) Token: 0x0600009F RID: 159 RVA: 0x00003FDD File Offset: 0x000021DD
		public DateTime purchaseDate
		{
			[CompilerGenerated]
			get
			{
				return this.<purchaseDate>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<purchaseDate>k__BackingField = value;
			}
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x060000A0 RID: 160 RVA: 0x00003FE6 File Offset: 0x000021E6
		// (set) Token: 0x060000A1 RID: 161 RVA: 0x00003FEE File Offset: 0x000021EE
		public string packageName
		{
			[CompilerGenerated]
			get
			{
				return this.<packageName>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<packageName>k__BackingField = value;
			}
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x060000A2 RID: 162 RVA: 0x00003FF7 File Offset: 0x000021F7
		// (set) Token: 0x060000A3 RID: 163 RVA: 0x00003FFF File Offset: 0x000021FF
		public string status
		{
			[CompilerGenerated]
			get
			{
				return this.<status>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<status>k__BackingField = value;
			}
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x060000A4 RID: 164 RVA: 0x00004008 File Offset: 0x00002208
		// (set) Token: 0x060000A5 RID: 165 RVA: 0x00004010 File Offset: 0x00002210
		public string clientId
		{
			[CompilerGenerated]
			get
			{
				return this.<clientId>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<clientId>k__BackingField = value;
			}
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x060000A6 RID: 166 RVA: 0x00004019 File Offset: 0x00002219
		// (set) Token: 0x060000A7 RID: 167 RVA: 0x00004021 File Offset: 0x00002221
		public string payFee
		{
			[CompilerGenerated]
			get
			{
				return this.<payFee>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<payFee>k__BackingField = value;
			}
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x060000A8 RID: 168 RVA: 0x0000402A File Offset: 0x0000222A
		// (set) Token: 0x060000A9 RID: 169 RVA: 0x00004032 File Offset: 0x00002232
		public string orderAttemptId
		{
			[CompilerGenerated]
			get
			{
				return this.<orderAttemptId>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<orderAttemptId>k__BackingField = value;
			}
		}

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x060000AA RID: 170 RVA: 0x0000403B File Offset: 0x0000223B
		// (set) Token: 0x060000AB RID: 171 RVA: 0x00004043 File Offset: 0x00002243
		public string country
		{
			[CompilerGenerated]
			get
			{
				return this.<country>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<country>k__BackingField = value;
			}
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x060000AC RID: 172 RVA: 0x0000404C File Offset: 0x0000224C
		// (set) Token: 0x060000AD RID: 173 RVA: 0x00004054 File Offset: 0x00002254
		public string currency
		{
			[CompilerGenerated]
			get
			{
				return this.<currency>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<currency>k__BackingField = value;
			}
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x060000AE RID: 174 RVA: 0x0000405D File Offset: 0x0000225D
		// (set) Token: 0x060000AF RID: 175 RVA: 0x00004065 File Offset: 0x00002265
		public string quantity
		{
			[CompilerGenerated]
			get
			{
				return this.<quantity>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<quantity>k__BackingField = value;
			}
		}

		// Token: 0x060000B0 RID: 176 RVA: 0x00004070 File Offset: 0x00002270
		public UnityChannelReceipt(string transactionId, string packageName, string productId, DateTime purchaseDate, string status, string clientId, string payFee, string orderAttemptId, string country, string currency, string quantity)
		{
			this.transactionID = transactionId;
			this.productID = productId;
			this.purchaseDate = purchaseDate;
			this.packageName = packageName;
			this.status = status;
			this.clientId = clientId;
			this.payFee = payFee;
			this.orderAttemptId = orderAttemptId;
			this.country = country;
			this.currency = currency;
			this.quantity = quantity;
		}

		// Token: 0x04000044 RID: 68
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <transactionID>k__BackingField;

		// Token: 0x04000045 RID: 69
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <productID>k__BackingField;

		// Token: 0x04000046 RID: 70
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private DateTime <purchaseDate>k__BackingField;

		// Token: 0x04000047 RID: 71
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <packageName>k__BackingField;

		// Token: 0x04000048 RID: 72
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <status>k__BackingField;

		// Token: 0x04000049 RID: 73
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <clientId>k__BackingField;

		// Token: 0x0400004A RID: 74
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <payFee>k__BackingField;

		// Token: 0x0400004B RID: 75
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <orderAttemptId>k__BackingField;

		// Token: 0x0400004C RID: 76
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <country>k__BackingField;

		// Token: 0x0400004D RID: 77
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <currency>k__BackingField;

		// Token: 0x0400004E RID: 78
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <quantity>k__BackingField;
	}
}
