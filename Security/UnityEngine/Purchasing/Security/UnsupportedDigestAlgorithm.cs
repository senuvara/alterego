﻿using System;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x0200000C RID: 12
	public class UnsupportedDigestAlgorithm : IAPSecurityException
	{
		// Token: 0x06000041 RID: 65 RVA: 0x000028C3 File Offset: 0x00000AC3
		public UnsupportedDigestAlgorithm()
		{
		}
	}
}
