﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using LipingShare.LCLib.Asn1Processor;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x02000005 RID: 5
	internal class PKCS7
	{
		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000029 RID: 41 RVA: 0x000028CC File Offset: 0x00000ACC
		// (set) Token: 0x0600002A RID: 42 RVA: 0x000028D4 File Offset: 0x00000AD4
		public Asn1Node data
		{
			[CompilerGenerated]
			get
			{
				return this.<data>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<data>k__BackingField = value;
			}
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x0600002B RID: 43 RVA: 0x000028DD File Offset: 0x00000ADD
		// (set) Token: 0x0600002C RID: 44 RVA: 0x000028E5 File Offset: 0x00000AE5
		public List<SignerInfo> sinfos
		{
			[CompilerGenerated]
			get
			{
				return this.<sinfos>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<sinfos>k__BackingField = value;
			}
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x0600002D RID: 45 RVA: 0x000028EE File Offset: 0x00000AEE
		// (set) Token: 0x0600002E RID: 46 RVA: 0x000028F6 File Offset: 0x00000AF6
		public List<X509Cert> certChain
		{
			[CompilerGenerated]
			get
			{
				return this.<certChain>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<certChain>k__BackingField = value;
			}
		}

		// Token: 0x0600002F RID: 47 RVA: 0x00002900 File Offset: 0x00000B00
		public static PKCS7 Load(byte[] data)
		{
			PKCS7 result;
			using (MemoryStream memoryStream = new MemoryStream(data))
			{
				Asn1Parser asn1Parser = new Asn1Parser();
				asn1Parser.LoadData(memoryStream);
				result = new PKCS7(asn1Parser.RootNode);
			}
			return result;
		}

		// Token: 0x06000030 RID: 48 RVA: 0x00002950 File Offset: 0x00000B50
		public PKCS7(Asn1Node node)
		{
			this.root = node;
			this.CheckStructure();
		}

		// Token: 0x06000031 RID: 49 RVA: 0x00002968 File Offset: 0x00000B68
		public bool Verify(X509Cert cert, DateTime certificateCreationTime)
		{
			bool flag = this.validStructure;
			bool result;
			if (flag)
			{
				bool flag2 = true;
				foreach (SignerInfo signerInfo in this.sinfos)
				{
					X509Cert x509Cert = null;
					foreach (X509Cert x509Cert2 in this.certChain)
					{
						bool flag3 = x509Cert2.SerialNumber == signerInfo.IssuerSerialNumber;
						if (flag3)
						{
							x509Cert = x509Cert2;
							break;
						}
					}
					bool flag4 = x509Cert != null && x509Cert.PubKey != null;
					if (flag4)
					{
						flag2 = (flag2 && x509Cert.CheckCertTime(certificateCreationTime));
						flag2 = (flag2 && x509Cert.PubKey.Verify(this.data.Data, signerInfo.EncryptedDigest));
						flag2 = (flag2 && this.ValidateChain(cert, x509Cert, certificateCreationTime));
					}
				}
				result = (flag2 && this.sinfos.Count > 0);
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x06000032 RID: 50 RVA: 0x00002AB0 File Offset: 0x00000CB0
		private bool ValidateChain(X509Cert root, X509Cert cert, DateTime certificateCreationTime)
		{
			bool flag = cert.Issuer.Equals(root.Subject);
			bool result;
			if (flag)
			{
				result = cert.CheckSignature(root);
			}
			else
			{
				foreach (X509Cert x509Cert in this.certChain)
				{
					bool flag2 = x509Cert != cert && x509Cert.Subject.Equals(cert.Issuer) && x509Cert.CheckCertTime(certificateCreationTime);
					if (flag2)
					{
						bool flag3 = x509Cert.Issuer.Equals(root.Subject) && x509Cert.SerialNumber == root.SerialNumber;
						if (flag3)
						{
							return x509Cert.CheckSignature(root);
						}
						bool flag4 = cert.CheckSignature(x509Cert);
						if (flag4)
						{
							return this.ValidateChain(root, x509Cert, certificateCreationTime);
						}
					}
				}
				result = false;
			}
			return result;
		}

		// Token: 0x06000033 RID: 51 RVA: 0x00002BAC File Offset: 0x00000DAC
		private void CheckStructure()
		{
			this.validStructure = false;
			bool flag = (this.root.Tag & 31) == 16 && this.root.ChildNodeCount == 2L;
			if (flag)
			{
				Asn1Node childNode = this.root.GetChildNode(0);
				bool flag2 = (childNode.Tag & 31) != 6 || childNode.GetDataStr(false) != "1.2.840.113549.1.7.2";
				if (flag2)
				{
					throw new InvalidPKCS7Data();
				}
				childNode = this.root.GetChildNode(1);
				bool flag3 = childNode.ChildNodeCount != 1L;
				if (flag3)
				{
					throw new InvalidPKCS7Data();
				}
				int num = 0;
				childNode = childNode.GetChildNode(num++);
				bool flag4 = childNode.ChildNodeCount < 4L || (childNode.Tag & 31) != 16;
				if (flag4)
				{
					throw new InvalidPKCS7Data();
				}
				Asn1Node childNode2 = childNode.GetChildNode(0);
				bool flag5 = (childNode2.Tag & 31) != 2;
				if (flag5)
				{
					throw new InvalidPKCS7Data();
				}
				childNode2 = childNode.GetChildNode(num++);
				bool flag6 = (childNode2.Tag & 31) != 17;
				if (flag6)
				{
					throw new InvalidPKCS7Data();
				}
				childNode2 = childNode.GetChildNode(num++);
				bool flag7 = (childNode2.Tag & 31) != 16 && childNode2.ChildNodeCount != 2L;
				if (flag7)
				{
					throw new InvalidPKCS7Data();
				}
				this.data = childNode2.GetChildNode(1).GetChildNode(0);
				bool flag8 = childNode.ChildNodeCount == 5L;
				if (flag8)
				{
					this.certChain = new List<X509Cert>();
					childNode2 = childNode.GetChildNode(num++);
					bool flag9 = childNode2.ChildNodeCount == 0L;
					if (flag9)
					{
						throw new InvalidPKCS7Data();
					}
					int num2 = 0;
					while ((long)num2 < childNode2.ChildNodeCount)
					{
						this.certChain.Add(new X509Cert(childNode2.GetChildNode(num2)));
						num2++;
					}
				}
				childNode2 = childNode.GetChildNode(num++);
				bool flag10 = (childNode2.Tag & 31) != 17 || childNode2.ChildNodeCount == 0L;
				if (flag10)
				{
					throw new InvalidPKCS7Data();
				}
				this.sinfos = new List<SignerInfo>();
				int num3 = 0;
				while ((long)num3 < childNode2.ChildNodeCount)
				{
					this.sinfos.Add(new SignerInfo(childNode2.GetChildNode(num3)));
					num3++;
				}
				this.validStructure = true;
			}
		}

		// Token: 0x04000012 RID: 18
		private Asn1Node root;

		// Token: 0x04000013 RID: 19
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Asn1Node <data>k__BackingField;

		// Token: 0x04000014 RID: 20
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private List<SignerInfo> <sinfos>k__BackingField;

		// Token: 0x04000015 RID: 21
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private List<X509Cert> <certChain>k__BackingField;

		// Token: 0x04000016 RID: 22
		private bool validStructure;
	}
}
