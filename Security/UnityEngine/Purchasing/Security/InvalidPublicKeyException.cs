﻿using System;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x0200001A RID: 26
	public class InvalidPublicKeyException : IAPSecurityException
	{
		// Token: 0x06000083 RID: 131 RVA: 0x00003BD2 File Offset: 0x00001DD2
		public InvalidPublicKeyException(string message) : base(message)
		{
		}
	}
}
