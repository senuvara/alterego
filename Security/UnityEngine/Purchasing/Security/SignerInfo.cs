﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using LipingShare.LCLib.Asn1Processor;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x02000006 RID: 6
	internal class SignerInfo
	{
		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000034 RID: 52 RVA: 0x00002E0C File Offset: 0x0000100C
		// (set) Token: 0x06000035 RID: 53 RVA: 0x00002E14 File Offset: 0x00001014
		public int Version
		{
			[CompilerGenerated]
			get
			{
				return this.<Version>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Version>k__BackingField = value;
			}
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000036 RID: 54 RVA: 0x00002E1D File Offset: 0x0000101D
		// (set) Token: 0x06000037 RID: 55 RVA: 0x00002E25 File Offset: 0x00001025
		public string IssuerSerialNumber
		{
			[CompilerGenerated]
			get
			{
				return this.<IssuerSerialNumber>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<IssuerSerialNumber>k__BackingField = value;
			}
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000038 RID: 56 RVA: 0x00002E2E File Offset: 0x0000102E
		// (set) Token: 0x06000039 RID: 57 RVA: 0x00002E36 File Offset: 0x00001036
		public byte[] EncryptedDigest
		{
			[CompilerGenerated]
			get
			{
				return this.<EncryptedDigest>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<EncryptedDigest>k__BackingField = value;
			}
		}

		// Token: 0x0600003A RID: 58 RVA: 0x00002E40 File Offset: 0x00001040
		public SignerInfo(Asn1Node n)
		{
			bool flag = n.ChildNodeCount != 5L;
			if (flag)
			{
				throw new InvalidPKCS7Data();
			}
			Asn1Node childNode = n.GetChildNode(0);
			bool flag2 = (childNode.Tag & 31) != 2;
			if (flag2)
			{
				throw new InvalidPKCS7Data();
			}
			this.Version = (int)childNode.Data[0];
			bool flag3 = this.Version != 1 || childNode.Data.Length != 1;
			if (flag3)
			{
				throw new UnsupportedSignerInfoVersion();
			}
			childNode = n.GetChildNode(1);
			bool flag4 = (childNode.Tag & 31) != 16 || childNode.ChildNodeCount != 2L;
			if (flag4)
			{
				throw new InvalidPKCS7Data();
			}
			childNode = childNode.GetChildNode(1);
			bool flag5 = (childNode.Tag & 31) != 2;
			if (flag5)
			{
				throw new InvalidPKCS7Data();
			}
			this.IssuerSerialNumber = Asn1Util.ToHexString(childNode.Data);
			childNode = n.GetChildNode(4);
			bool flag6 = (childNode.Tag & 31) != 4;
			if (flag6)
			{
				throw new InvalidPKCS7Data();
			}
			this.EncryptedDigest = childNode.Data;
		}

		// Token: 0x04000017 RID: 23
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <Version>k__BackingField;

		// Token: 0x04000018 RID: 24
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <IssuerSerialNumber>k__BackingField;

		// Token: 0x04000019 RID: 25
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private byte[] <EncryptedDigest>k__BackingField;
	}
}
