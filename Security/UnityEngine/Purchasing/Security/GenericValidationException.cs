﻿using System;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x0200001B RID: 27
	public class GenericValidationException : IAPSecurityException
	{
		// Token: 0x06000084 RID: 132 RVA: 0x00003BD2 File Offset: 0x00001DD2
		public GenericValidationException(string message) : base(message)
		{
		}
	}
}
