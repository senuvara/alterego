﻿using System;
using System.IO;

namespace LCLib.Asn1Processor
{
	// Token: 0x02000023 RID: 35
	internal class Asn1Util
	{
		// Token: 0x060000B5 RID: 181 RVA: 0x000042B4 File Offset: 0x000024B4
		public static int BytePrecision(ulong value)
		{
			int i;
			for (i = 8; i > 0; i--)
			{
				bool flag = value >> (i - 1) * 8 > 0UL;
				if (flag)
				{
					break;
				}
			}
			return i;
		}

		// Token: 0x060000B6 RID: 182 RVA: 0x000042EC File Offset: 0x000024EC
		public static int DERLengthEncode(Stream xdata, ulong length)
		{
			int num = 0;
			bool flag = length <= 127UL;
			if (flag)
			{
				xdata.WriteByte((byte)length);
				num++;
			}
			else
			{
				xdata.WriteByte((byte)(Asn1Util.BytePrecision(length) | 128));
				num++;
				for (int i = Asn1Util.BytePrecision(length); i > 0; i--)
				{
					xdata.WriteByte((byte)(length >> (i - 1) * 8));
					num++;
				}
			}
			return num;
		}

		// Token: 0x060000B7 RID: 183 RVA: 0x00004368 File Offset: 0x00002568
		public static long DerLengthDecode(Stream bt)
		{
			byte b = (byte)bt.ReadByte();
			bool flag = (b & 128) == 0;
			long num;
			if (flag)
			{
				num = (long)((ulong)b);
			}
			else
			{
				long num2 = (long)(b & 127);
				bool flag2 = num2 == 0L;
				if (flag2)
				{
					throw new Exception("Indefinite length.");
				}
				num = 0L;
				for (;;)
				{
					long num3 = num2;
					num2 = num3 - 1L;
					if (num3 <= 0L)
					{
						return num;
					}
					bool flag3 = num >> 56 > 0L;
					if (flag3)
					{
						break;
					}
					b = (byte)bt.ReadByte();
					num = (num << 8 | (long)((ulong)b));
				}
				throw new Exception("Length overflow.");
			}
			return num;
		}

		// Token: 0x060000B8 RID: 184 RVA: 0x000043FB File Offset: 0x000025FB
		private Asn1Util()
		{
		}
	}
}
