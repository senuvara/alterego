﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Xml;

[assembly: AssemblyVersion("4.1.1.0")]
[assembly: AssemblyTitle("System.Runtime.Serialization.Xml")]
[assembly: AssemblyDescription("System.Runtime.Serialization.Xml")]
[assembly: AssemblyDefaultAlias("System.Runtime.Serialization.Xml")]
[assembly: AssemblyCompany("Mono development team")]
[assembly: AssemblyProduct("Mono Common Language Infrastructure")]
[assembly: AssemblyCopyright("(c) Various Mono authors")]
[assembly: AssemblyInformationalVersion("4.0.0.0")]
[assembly: AssemblyFileVersion("4.0.0.0")]
// System.Runtime.Serialization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e
[assembly: TypeForwardedTo(typeof(DataContractResolver))]
[assembly: TypeForwardedTo(typeof(DataContractSerializer))]
[assembly: TypeForwardedTo(typeof(DataContractSerializerSettings))]
[assembly: TypeForwardedTo(typeof(InvalidDataContractException))]
[assembly: TypeForwardedTo(typeof(IXmlDictionary))]
[assembly: TypeForwardedTo(typeof(OnXmlDictionaryReaderClose))]
[assembly: TypeForwardedTo(typeof(UniqueId))]
[assembly: TypeForwardedTo(typeof(XmlBinaryReaderSession))]
[assembly: TypeForwardedTo(typeof(XmlBinaryWriterSession))]
[assembly: TypeForwardedTo(typeof(XmlDictionary))]
[assembly: TypeForwardedTo(typeof(XmlDictionaryReader))]
[assembly: TypeForwardedTo(typeof(XmlDictionaryReaderQuotas))]
[assembly: TypeForwardedTo(typeof(XmlDictionaryReaderQuotaTypes))]
[assembly: TypeForwardedTo(typeof(XmlDictionaryString))]
[assembly: TypeForwardedTo(typeof(XmlDictionaryWriter))]
[assembly: TypeForwardedTo(typeof(XmlObjectSerializer))]
