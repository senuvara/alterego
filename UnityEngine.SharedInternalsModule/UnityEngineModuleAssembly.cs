﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200000A RID: 10
	[VisibleToOtherModules]
	[AttributeUsage(AttributeTargets.Assembly)]
	internal class UnityEngineModuleAssembly : Attribute
	{
		// Token: 0x06000017 RID: 23 RVA: 0x00002133 File Offset: 0x00000333
		public UnityEngineModuleAssembly()
		{
		}
	}
}
