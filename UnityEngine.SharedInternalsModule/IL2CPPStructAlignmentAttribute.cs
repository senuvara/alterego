﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000007 RID: 7
	[AttributeUsage(AttributeTargets.Struct)]
	[VisibleToOtherModules]
	internal class IL2CPPStructAlignmentAttribute : Attribute
	{
		// Token: 0x06000014 RID: 20 RVA: 0x0000218A File Offset: 0x0000038A
		public IL2CPPStructAlignmentAttribute()
		{
			this.Align = 1;
		}

		// Token: 0x04000007 RID: 7
		public int Align;
	}
}
