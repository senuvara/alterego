﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine.Scripting
{
	// Token: 0x02000004 RID: 4
	[VisibleToOtherModules]
	internal class GeneratedByOldBindingsGeneratorAttribute : Attribute
	{
		// Token: 0x0600000F RID: 15 RVA: 0x00002133 File Offset: 0x00000333
		public GeneratedByOldBindingsGeneratorAttribute()
		{
		}
	}
}
