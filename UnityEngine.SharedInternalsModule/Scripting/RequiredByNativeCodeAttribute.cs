﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.Scripting
{
	// Token: 0x02000003 RID: 3
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Interface, Inherited = false)]
	[VisibleToOtherModules]
	internal class RequiredByNativeCodeAttribute : Attribute
	{
		// Token: 0x06000005 RID: 5 RVA: 0x00002050 File Offset: 0x00000250
		public RequiredByNativeCodeAttribute()
		{
		}

		// Token: 0x06000006 RID: 6 RVA: 0x0000208F File Offset: 0x0000028F
		public RequiredByNativeCodeAttribute(string name)
		{
			this.Name = name;
		}

		// Token: 0x06000007 RID: 7 RVA: 0x0000209F File Offset: 0x0000029F
		public RequiredByNativeCodeAttribute(bool optional)
		{
			this.Optional = optional;
		}

		// Token: 0x06000008 RID: 8 RVA: 0x000020AF File Offset: 0x000002AF
		public RequiredByNativeCodeAttribute(string name, bool optional)
		{
			this.Name = name;
			this.Optional = optional;
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000009 RID: 9 RVA: 0x000020C8 File Offset: 0x000002C8
		// (set) Token: 0x0600000A RID: 10 RVA: 0x000020E2 File Offset: 0x000002E2
		public string Name
		{
			[CompilerGenerated]
			get
			{
				return this.<Name>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Name>k__BackingField = value;
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x0600000B RID: 11 RVA: 0x000020EC File Offset: 0x000002EC
		// (set) Token: 0x0600000C RID: 12 RVA: 0x00002106 File Offset: 0x00000306
		public bool Optional
		{
			[CompilerGenerated]
			get
			{
				return this.<Optional>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Optional>k__BackingField = value;
			}
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x0600000D RID: 13 RVA: 0x00002110 File Offset: 0x00000310
		// (set) Token: 0x0600000E RID: 14 RVA: 0x0000212A File Offset: 0x0000032A
		public bool GenerateProxy
		{
			[CompilerGenerated]
			get
			{
				return this.<GenerateProxy>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<GenerateProxy>k__BackingField = value;
			}
		}

		// Token: 0x04000002 RID: 2
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <Name>k__BackingField;

		// Token: 0x04000003 RID: 3
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <Optional>k__BackingField;

		// Token: 0x04000004 RID: 4
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <GenerateProxy>k__BackingField;
	}
}
