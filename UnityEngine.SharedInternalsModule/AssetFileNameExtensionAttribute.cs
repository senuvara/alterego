﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000005 RID: 5
	[AttributeUsage(AttributeTargets.Class, Inherited = false)]
	[VisibleToOtherModules]
	internal sealed class AssetFileNameExtensionAttribute : Attribute
	{
		// Token: 0x06000010 RID: 16 RVA: 0x0000213B File Offset: 0x0000033B
		public AssetFileNameExtensionAttribute(string preferredExtension, params string[] otherExtensions)
		{
			this.preferredExtension = preferredExtension;
			this.otherExtensions = otherExtensions;
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000011 RID: 17 RVA: 0x00002154 File Offset: 0x00000354
		public string preferredExtension
		{
			[CompilerGenerated]
			get
			{
				return this.<preferredExtension>k__BackingField;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000012 RID: 18 RVA: 0x00002170 File Offset: 0x00000370
		public IEnumerable<string> otherExtensions
		{
			[CompilerGenerated]
			get
			{
				return this.<otherExtensions>k__BackingField;
			}
		}

		// Token: 0x04000005 RID: 5
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private readonly string <preferredExtension>k__BackingField;

		// Token: 0x04000006 RID: 6
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private readonly IEnumerable<string> <otherExtensions>k__BackingField;
	}
}
