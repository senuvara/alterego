﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000008 RID: 8
	[VisibleToOtherModules]
	[AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false)]
	internal class WritableAttribute : Attribute
	{
		// Token: 0x06000015 RID: 21 RVA: 0x00002133 File Offset: 0x00000333
		public WritableAttribute()
		{
		}
	}
}
