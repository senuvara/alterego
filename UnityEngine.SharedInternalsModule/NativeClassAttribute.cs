﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200000B RID: 11
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, Inherited = false)]
	[VisibleToOtherModules]
	internal sealed class NativeClassAttribute : Attribute
	{
		// Token: 0x06000018 RID: 24 RVA: 0x0000219A File Offset: 0x0000039A
		public NativeClassAttribute(string qualifiedCppName)
		{
			this.QualifiedNativeName = qualifiedCppName;
			this.Declaration = "class " + qualifiedCppName;
		}

		// Token: 0x06000019 RID: 25 RVA: 0x000021BB File Offset: 0x000003BB
		public NativeClassAttribute(string qualifiedCppName, string declaration)
		{
			this.QualifiedNativeName = qualifiedCppName;
			this.Declaration = declaration;
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600001A RID: 26 RVA: 0x000021D4 File Offset: 0x000003D4
		// (set) Token: 0x0600001B RID: 27 RVA: 0x000021EE File Offset: 0x000003EE
		public string QualifiedNativeName
		{
			[CompilerGenerated]
			get
			{
				return this.<QualifiedNativeName>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<QualifiedNativeName>k__BackingField = value;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x0600001C RID: 28 RVA: 0x000021F8 File Offset: 0x000003F8
		// (set) Token: 0x0600001D RID: 29 RVA: 0x00002212 File Offset: 0x00000412
		public string Declaration
		{
			[CompilerGenerated]
			get
			{
				return this.<Declaration>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Declaration>k__BackingField = value;
			}
		}

		// Token: 0x04000008 RID: 8
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <QualifiedNativeName>k__BackingField;

		// Token: 0x04000009 RID: 9
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <Declaration>k__BackingField;
	}
}
