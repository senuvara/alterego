﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000009 RID: 9
	[VisibleToOtherModules]
	[AttributeUsage(AttributeTargets.Class)]
	internal class RejectDragAndDropMaterial : Attribute
	{
		// Token: 0x06000016 RID: 22 RVA: 0x00002133 File Offset: 0x00000333
		public RejectDragAndDropMaterial()
		{
		}
	}
}
