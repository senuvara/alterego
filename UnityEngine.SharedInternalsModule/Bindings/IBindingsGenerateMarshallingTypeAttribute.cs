﻿using System;

namespace UnityEngine.Bindings
{
	// Token: 0x02000013 RID: 19
	internal interface IBindingsGenerateMarshallingTypeAttribute : IBindingsAttribute
	{
		// Token: 0x1700000F RID: 15
		// (get) Token: 0x0600002C RID: 44
		// (set) Token: 0x0600002D RID: 45
		CodegenOptions CodegenOptions { get; set; }
	}
}
