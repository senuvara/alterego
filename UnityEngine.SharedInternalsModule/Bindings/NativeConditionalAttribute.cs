﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Bindings
{
	// Token: 0x02000015 RID: 21
	[VisibleToOtherModules]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Method | AttributeTargets.Property)]
	internal class NativeConditionalAttribute : Attribute, IBindingsAttribute
	{
		// Token: 0x06000030 RID: 48 RVA: 0x00002050 File Offset: 0x00000250
		public NativeConditionalAttribute()
		{
		}

		// Token: 0x06000031 RID: 49 RVA: 0x0000221B File Offset: 0x0000041B
		public NativeConditionalAttribute(string condition)
		{
			this.Condition = condition;
			this.Enabled = true;
		}

		// Token: 0x06000032 RID: 50 RVA: 0x00002232 File Offset: 0x00000432
		public NativeConditionalAttribute(bool enabled)
		{
			this.Enabled = enabled;
		}

		// Token: 0x06000033 RID: 51 RVA: 0x00002242 File Offset: 0x00000442
		public NativeConditionalAttribute(string condition, bool enabled) : this(condition)
		{
			this.Enabled = enabled;
		}

		// Token: 0x06000034 RID: 52 RVA: 0x00002253 File Offset: 0x00000453
		public NativeConditionalAttribute(string condition, string stubReturnStatement, bool enabled) : this(condition, stubReturnStatement)
		{
			this.Enabled = enabled;
		}

		// Token: 0x06000035 RID: 53 RVA: 0x00002265 File Offset: 0x00000465
		public NativeConditionalAttribute(string condition, string stubReturnStatement) : this(condition)
		{
			this.StubReturnStatement = stubReturnStatement;
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000036 RID: 54 RVA: 0x00002278 File Offset: 0x00000478
		// (set) Token: 0x06000037 RID: 55 RVA: 0x00002292 File Offset: 0x00000492
		public string Condition
		{
			[CompilerGenerated]
			get
			{
				return this.<Condition>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Condition>k__BackingField = value;
			}
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000038 RID: 56 RVA: 0x0000229C File Offset: 0x0000049C
		// (set) Token: 0x06000039 RID: 57 RVA: 0x000022B6 File Offset: 0x000004B6
		public string StubReturnStatement
		{
			[CompilerGenerated]
			get
			{
				return this.<StubReturnStatement>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<StubReturnStatement>k__BackingField = value;
			}
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x0600003A RID: 58 RVA: 0x000022C0 File Offset: 0x000004C0
		// (set) Token: 0x0600003B RID: 59 RVA: 0x000022DA File Offset: 0x000004DA
		public bool Enabled
		{
			[CompilerGenerated]
			get
			{
				return this.<Enabled>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Enabled>k__BackingField = value;
			}
		}

		// Token: 0x0400000A RID: 10
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <Condition>k__BackingField;

		// Token: 0x0400000B RID: 11
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <StubReturnStatement>k__BackingField;

		// Token: 0x0400000C RID: 12
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <Enabled>k__BackingField;
	}
}
