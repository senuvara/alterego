﻿using System;

namespace UnityEngine.Bindings
{
	// Token: 0x0200001C RID: 28
	[VisibleToOtherModules]
	internal enum CodegenOptions
	{
		// Token: 0x0400001B RID: 27
		Auto,
		// Token: 0x0400001C RID: 28
		Custom,
		// Token: 0x0400001D RID: 29
		Force
	}
}
