﻿using System;

namespace UnityEngine.Bindings
{
	// Token: 0x02000014 RID: 20
	internal interface IBindingsWritableSelfProviderAttribute : IBindingsAttribute
	{
		// Token: 0x17000010 RID: 16
		// (get) Token: 0x0600002E RID: 46
		// (set) Token: 0x0600002F RID: 47
		bool WritableSelf { get; set; }
	}
}
