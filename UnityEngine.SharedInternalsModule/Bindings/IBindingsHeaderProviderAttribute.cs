﻿using System;

namespace UnityEngine.Bindings
{
	// Token: 0x0200000F RID: 15
	internal interface IBindingsHeaderProviderAttribute : IBindingsAttribute
	{
		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000022 RID: 34
		// (set) Token: 0x06000023 RID: 35
		string Header { get; set; }
	}
}
