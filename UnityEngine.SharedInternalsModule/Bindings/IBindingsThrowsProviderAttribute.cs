﻿using System;

namespace UnityEngine.Bindings
{
	// Token: 0x02000012 RID: 18
	internal interface IBindingsThrowsProviderAttribute : IBindingsAttribute
	{
		// Token: 0x1700000E RID: 14
		// (get) Token: 0x0600002A RID: 42
		// (set) Token: 0x0600002B RID: 43
		bool ThrowsException { get; set; }
	}
}
