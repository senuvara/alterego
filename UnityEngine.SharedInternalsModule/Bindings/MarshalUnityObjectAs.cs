﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Bindings
{
	// Token: 0x02000027 RID: 39
	[AttributeUsage(AttributeTargets.Class)]
	[VisibleToOtherModules]
	internal class MarshalUnityObjectAs : Attribute, IBindingsAttribute
	{
		// Token: 0x06000081 RID: 129 RVA: 0x000027F7 File Offset: 0x000009F7
		public MarshalUnityObjectAs(Type marshalAsType)
		{
			this.MarshalAsType = marshalAsType;
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x06000082 RID: 130 RVA: 0x00002808 File Offset: 0x00000A08
		// (set) Token: 0x06000083 RID: 131 RVA: 0x00002822 File Offset: 0x00000A22
		public Type MarshalAsType
		{
			[CompilerGenerated]
			get
			{
				return this.<MarshalAsType>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<MarshalAsType>k__BackingField = value;
			}
		}

		// Token: 0x0400002A RID: 42
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Type <MarshalAsType>k__BackingField;
	}
}
