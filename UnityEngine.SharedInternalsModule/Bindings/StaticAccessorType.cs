﻿using System;

namespace UnityEngine.Bindings
{
	// Token: 0x02000023 RID: 35
	[VisibleToOtherModules]
	internal enum StaticAccessorType
	{
		// Token: 0x04000022 RID: 34
		Dot,
		// Token: 0x04000023 RID: 35
		Arrow,
		// Token: 0x04000024 RID: 36
		DoubleColon,
		// Token: 0x04000025 RID: 37
		ArrowWithDefaultReturnIfNull
	}
}
