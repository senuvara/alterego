﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Bindings
{
	// Token: 0x02000026 RID: 38
	[AttributeUsage(AttributeTargets.Field)]
	[VisibleToOtherModules]
	internal class IgnoreAttribute : Attribute, IBindingsAttribute
	{
		// Token: 0x0600007E RID: 126 RVA: 0x00002133 File Offset: 0x00000333
		public IgnoreAttribute()
		{
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x0600007F RID: 127 RVA: 0x000027D4 File Offset: 0x000009D4
		// (set) Token: 0x06000080 RID: 128 RVA: 0x000027EE File Offset: 0x000009EE
		public bool DoesNotContributeToSize
		{
			[CompilerGenerated]
			get
			{
				return this.<DoesNotContributeToSize>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<DoesNotContributeToSize>k__BackingField = value;
			}
		}

		// Token: 0x04000029 RID: 41
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <DoesNotContributeToSize>k__BackingField;
	}
}
