﻿using System;

namespace UnityEngine.Bindings
{
	// Token: 0x0200001F RID: 31
	[AttributeUsage(AttributeTargets.Parameter)]
	[VisibleToOtherModules]
	internal class NotNullAttribute : Attribute, IBindingsAttribute
	{
		// Token: 0x0600006C RID: 108 RVA: 0x00002050 File Offset: 0x00000250
		public NotNullAttribute()
		{
		}
	}
}
