﻿using System;

namespace UnityEngine.Bindings
{
	// Token: 0x0200001D RID: 29
	[VisibleToOtherModules]
	[AttributeUsage(AttributeTargets.Class)]
	internal class NativeAsStructAttribute : Attribute, IBindingsAttribute
	{
		// Token: 0x06000060 RID: 96 RVA: 0x00002133 File Offset: 0x00000333
		public NativeAsStructAttribute()
		{
		}
	}
}
