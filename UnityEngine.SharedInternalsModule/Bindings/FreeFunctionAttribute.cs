﻿using System;

namespace UnityEngine.Bindings
{
	// Token: 0x02000021 RID: 33
	[AttributeUsage(AttributeTargets.Method)]
	[VisibleToOtherModules]
	internal class FreeFunctionAttribute : NativeMethodAttribute
	{
		// Token: 0x0600006E RID: 110 RVA: 0x000026D7 File Offset: 0x000008D7
		public FreeFunctionAttribute()
		{
			base.IsFreeFunction = true;
		}

		// Token: 0x0600006F RID: 111 RVA: 0x000026E7 File Offset: 0x000008E7
		public FreeFunctionAttribute(string name) : base(name, true)
		{
		}

		// Token: 0x06000070 RID: 112 RVA: 0x000026F2 File Offset: 0x000008F2
		public FreeFunctionAttribute(string name, bool isThreadSafe) : base(name, true, isThreadSafe)
		{
		}
	}
}
