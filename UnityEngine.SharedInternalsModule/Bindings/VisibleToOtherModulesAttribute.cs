﻿using System;

namespace UnityEngine.Bindings
{
	// Token: 0x0200000C RID: 12
	[VisibleToOtherModules]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Interface | AttributeTargets.Delegate, Inherited = false)]
	internal class VisibleToOtherModulesAttribute : Attribute
	{
		// Token: 0x0600001E RID: 30 RVA: 0x00002050 File Offset: 0x00000250
		public VisibleToOtherModulesAttribute()
		{
		}

		// Token: 0x0600001F RID: 31 RVA: 0x00002050 File Offset: 0x00000250
		public VisibleToOtherModulesAttribute(params string[] modules)
		{
		}
	}
}
