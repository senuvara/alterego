﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Bindings
{
	// Token: 0x02000019 RID: 25
	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Property)]
	[VisibleToOtherModules]
	internal class NativeMethodAttribute : Attribute, IBindingsNameProviderAttribute, IBindingsIsThreadSafeProviderAttribute, IBindingsIsFreeFunctionProviderAttribute, IBindingsThrowsProviderAttribute, IBindingsAttribute
	{
		// Token: 0x06000048 RID: 72 RVA: 0x00002050 File Offset: 0x00000250
		public NativeMethodAttribute()
		{
		}

		// Token: 0x06000049 RID: 73 RVA: 0x00002408 File Offset: 0x00000608
		public NativeMethodAttribute(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name == "")
			{
				throw new ArgumentException("name cannot be empty", "name");
			}
			this.Name = name;
		}

		// Token: 0x0600004A RID: 74 RVA: 0x00002454 File Offset: 0x00000654
		public NativeMethodAttribute(string name, bool isFreeFunction) : this(name)
		{
			this.IsFreeFunction = isFreeFunction;
		}

		// Token: 0x0600004B RID: 75 RVA: 0x00002465 File Offset: 0x00000665
		public NativeMethodAttribute(string name, bool isFreeFunction, bool isThreadSafe) : this(name, isFreeFunction)
		{
			this.IsThreadSafe = isThreadSafe;
		}

		// Token: 0x0600004C RID: 76 RVA: 0x00002477 File Offset: 0x00000677
		public NativeMethodAttribute(string name, bool isFreeFunction, bool isThreadSafe, bool throws) : this(name, isFreeFunction, isThreadSafe)
		{
			this.ThrowsException = throws;
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x0600004D RID: 77 RVA: 0x0000248C File Offset: 0x0000068C
		// (set) Token: 0x0600004E RID: 78 RVA: 0x000024A6 File Offset: 0x000006A6
		public string Name
		{
			[CompilerGenerated]
			get
			{
				return this.<Name>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Name>k__BackingField = value;
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x0600004F RID: 79 RVA: 0x000024B0 File Offset: 0x000006B0
		// (set) Token: 0x06000050 RID: 80 RVA: 0x000024CA File Offset: 0x000006CA
		public bool IsThreadSafe
		{
			[CompilerGenerated]
			get
			{
				return this.<IsThreadSafe>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<IsThreadSafe>k__BackingField = value;
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000051 RID: 81 RVA: 0x000024D4 File Offset: 0x000006D4
		// (set) Token: 0x06000052 RID: 82 RVA: 0x000024EE File Offset: 0x000006EE
		public bool IsFreeFunction
		{
			[CompilerGenerated]
			get
			{
				return this.<IsFreeFunction>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<IsFreeFunction>k__BackingField = value;
			}
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000053 RID: 83 RVA: 0x000024F8 File Offset: 0x000006F8
		// (set) Token: 0x06000054 RID: 84 RVA: 0x00002512 File Offset: 0x00000712
		public bool ThrowsException
		{
			[CompilerGenerated]
			get
			{
				return this.<ThrowsException>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ThrowsException>k__BackingField = value;
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000055 RID: 85 RVA: 0x0000251C File Offset: 0x0000071C
		// (set) Token: 0x06000056 RID: 86 RVA: 0x00002536 File Offset: 0x00000736
		public bool HasExplicitThis
		{
			[CompilerGenerated]
			get
			{
				return this.<HasExplicitThis>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<HasExplicitThis>k__BackingField = value;
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000057 RID: 87 RVA: 0x00002540 File Offset: 0x00000740
		// (set) Token: 0x06000058 RID: 88 RVA: 0x0000255A File Offset: 0x0000075A
		public bool WritableSelf
		{
			[CompilerGenerated]
			get
			{
				return this.<WritableSelf>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<WritableSelf>k__BackingField = value;
			}
		}

		// Token: 0x04000010 RID: 16
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <Name>k__BackingField;

		// Token: 0x04000011 RID: 17
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <IsThreadSafe>k__BackingField;

		// Token: 0x04000012 RID: 18
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <IsFreeFunction>k__BackingField;

		// Token: 0x04000013 RID: 19
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <ThrowsException>k__BackingField;

		// Token: 0x04000014 RID: 20
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <HasExplicitThis>k__BackingField;

		// Token: 0x04000015 RID: 21
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <WritableSelf>k__BackingField;
	}
}
