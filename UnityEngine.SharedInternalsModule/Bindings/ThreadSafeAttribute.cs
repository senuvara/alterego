﻿using System;

namespace UnityEngine.Bindings
{
	// Token: 0x02000022 RID: 34
	[AttributeUsage(AttributeTargets.Method)]
	[VisibleToOtherModules]
	internal class ThreadSafeAttribute : NativeMethodAttribute
	{
		// Token: 0x06000071 RID: 113 RVA: 0x000026FE File Offset: 0x000008FE
		public ThreadSafeAttribute()
		{
			base.IsThreadSafe = true;
		}
	}
}
