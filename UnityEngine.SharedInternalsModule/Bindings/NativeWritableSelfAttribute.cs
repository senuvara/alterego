﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Bindings
{
	// Token: 0x02000018 RID: 24
	[AttributeUsage(AttributeTargets.Method)]
	[VisibleToOtherModules]
	internal sealed class NativeWritableSelfAttribute : Attribute, IBindingsWritableSelfProviderAttribute, IBindingsAttribute
	{
		// Token: 0x06000044 RID: 68 RVA: 0x000023C3 File Offset: 0x000005C3
		public NativeWritableSelfAttribute()
		{
			this.WritableSelf = true;
		}

		// Token: 0x06000045 RID: 69 RVA: 0x000023D3 File Offset: 0x000005D3
		public NativeWritableSelfAttribute(bool writable)
		{
			this.WritableSelf = writable;
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000046 RID: 70 RVA: 0x000023E4 File Offset: 0x000005E4
		// (set) Token: 0x06000047 RID: 71 RVA: 0x000023FE File Offset: 0x000005FE
		public bool WritableSelf
		{
			[CompilerGenerated]
			get
			{
				return this.<WritableSelf>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<WritableSelf>k__BackingField = value;
			}
		}

		// Token: 0x0400000F RID: 15
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <WritableSelf>k__BackingField;
	}
}
