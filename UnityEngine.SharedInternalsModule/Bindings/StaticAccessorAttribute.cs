﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Bindings
{
	// Token: 0x02000024 RID: 36
	[VisibleToOtherModules]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Method | AttributeTargets.Property)]
	internal class StaticAccessorAttribute : Attribute, IBindingsAttribute
	{
		// Token: 0x06000072 RID: 114 RVA: 0x00002050 File Offset: 0x00000250
		public StaticAccessorAttribute()
		{
		}

		// Token: 0x06000073 RID: 115 RVA: 0x0000270E File Offset: 0x0000090E
		[VisibleToOtherModules]
		internal StaticAccessorAttribute(string name)
		{
			this.Name = name;
		}

		// Token: 0x06000074 RID: 116 RVA: 0x0000271E File Offset: 0x0000091E
		public StaticAccessorAttribute(StaticAccessorType type)
		{
			this.Type = type;
		}

		// Token: 0x06000075 RID: 117 RVA: 0x0000272E File Offset: 0x0000092E
		public StaticAccessorAttribute(string name, StaticAccessorType type)
		{
			this.Name = name;
			this.Type = type;
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x06000076 RID: 118 RVA: 0x00002748 File Offset: 0x00000948
		// (set) Token: 0x06000077 RID: 119 RVA: 0x00002762 File Offset: 0x00000962
		public string Name
		{
			[CompilerGenerated]
			get
			{
				return this.<Name>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Name>k__BackingField = value;
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000078 RID: 120 RVA: 0x0000276C File Offset: 0x0000096C
		// (set) Token: 0x06000079 RID: 121 RVA: 0x00002786 File Offset: 0x00000986
		public StaticAccessorType Type
		{
			[CompilerGenerated]
			get
			{
				return this.<Type>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Type>k__BackingField = value;
			}
		}

		// Token: 0x04000026 RID: 38
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <Name>k__BackingField;

		// Token: 0x04000027 RID: 39
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private StaticAccessorType <Type>k__BackingField;
	}
}
