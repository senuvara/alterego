﻿using System;

namespace UnityEngine.Bindings
{
	// Token: 0x02000011 RID: 17
	internal interface IBindingsIsFreeFunctionProviderAttribute : IBindingsAttribute
	{
		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000026 RID: 38
		// (set) Token: 0x06000027 RID: 39
		bool IsFreeFunction { get; set; }

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000028 RID: 40
		// (set) Token: 0x06000029 RID: 41
		bool HasExplicitThis { get; set; }
	}
}
