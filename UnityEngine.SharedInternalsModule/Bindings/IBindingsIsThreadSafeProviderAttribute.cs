﻿using System;

namespace UnityEngine.Bindings
{
	// Token: 0x02000010 RID: 16
	internal interface IBindingsIsThreadSafeProviderAttribute : IBindingsAttribute
	{
		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000024 RID: 36
		// (set) Token: 0x06000025 RID: 37
		bool IsThreadSafe { get; set; }
	}
}
