﻿using System;

namespace UnityEngine.Bindings
{
	// Token: 0x0200000E RID: 14
	internal interface IBindingsNameProviderAttribute : IBindingsAttribute
	{
		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000020 RID: 32
		// (set) Token: 0x06000021 RID: 33
		string Name { get; set; }
	}
}
