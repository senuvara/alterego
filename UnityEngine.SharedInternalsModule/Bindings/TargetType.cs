﻿using System;

namespace UnityEngine.Bindings
{
	// Token: 0x0200001A RID: 26
	[VisibleToOtherModules]
	internal enum TargetType
	{
		// Token: 0x04000017 RID: 23
		Function,
		// Token: 0x04000018 RID: 24
		Field
	}
}
