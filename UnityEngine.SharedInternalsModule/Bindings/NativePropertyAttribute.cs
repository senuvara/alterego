﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Bindings
{
	// Token: 0x0200001B RID: 27
	[VisibleToOtherModules]
	[AttributeUsage(AttributeTargets.Property)]
	internal class NativePropertyAttribute : NativeMethodAttribute
	{
		// Token: 0x06000059 RID: 89 RVA: 0x00002563 File Offset: 0x00000763
		public NativePropertyAttribute()
		{
		}

		// Token: 0x0600005A RID: 90 RVA: 0x0000256C File Offset: 0x0000076C
		public NativePropertyAttribute(string name) : base(name)
		{
		}

		// Token: 0x0600005B RID: 91 RVA: 0x00002576 File Offset: 0x00000776
		public NativePropertyAttribute(string name, TargetType targetType) : base(name)
		{
			this.TargetType = targetType;
		}

		// Token: 0x0600005C RID: 92 RVA: 0x00002587 File Offset: 0x00000787
		public NativePropertyAttribute(string name, bool isFree, TargetType targetType) : base(name, isFree)
		{
			this.TargetType = targetType;
		}

		// Token: 0x0600005D RID: 93 RVA: 0x00002599 File Offset: 0x00000799
		public NativePropertyAttribute(string name, bool isFree, TargetType targetType, bool isThreadSafe) : base(name, isFree, isThreadSafe)
		{
			this.TargetType = targetType;
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x0600005E RID: 94 RVA: 0x000025B0 File Offset: 0x000007B0
		// (set) Token: 0x0600005F RID: 95 RVA: 0x000025CA File Offset: 0x000007CA
		public TargetType TargetType
		{
			[CompilerGenerated]
			get
			{
				return this.<TargetType>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<TargetType>k__BackingField = value;
			}
		}

		// Token: 0x04000019 RID: 25
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private TargetType <TargetType>k__BackingField;
	}
}
