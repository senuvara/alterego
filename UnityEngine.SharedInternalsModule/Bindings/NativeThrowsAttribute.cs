﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Bindings
{
	// Token: 0x02000025 RID: 37
	[VisibleToOtherModules]
	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Property)]
	internal class NativeThrowsAttribute : Attribute, IBindingsThrowsProviderAttribute, IBindingsAttribute
	{
		// Token: 0x0600007A RID: 122 RVA: 0x0000278F File Offset: 0x0000098F
		public NativeThrowsAttribute()
		{
			this.ThrowsException = true;
		}

		// Token: 0x0600007B RID: 123 RVA: 0x0000279F File Offset: 0x0000099F
		public NativeThrowsAttribute(bool throwsException)
		{
			this.ThrowsException = throwsException;
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x0600007C RID: 124 RVA: 0x000027B0 File Offset: 0x000009B0
		// (set) Token: 0x0600007D RID: 125 RVA: 0x000027CA File Offset: 0x000009CA
		public bool ThrowsException
		{
			[CompilerGenerated]
			get
			{
				return this.<ThrowsException>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ThrowsException>k__BackingField = value;
			}
		}

		// Token: 0x04000028 RID: 40
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <ThrowsException>k__BackingField;
	}
}
