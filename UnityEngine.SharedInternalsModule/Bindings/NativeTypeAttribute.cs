﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Bindings
{
	// Token: 0x0200001E RID: 30
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum)]
	[VisibleToOtherModules]
	internal class NativeTypeAttribute : Attribute, IBindingsHeaderProviderAttribute, IBindingsGenerateMarshallingTypeAttribute, IBindingsAttribute
	{
		// Token: 0x06000061 RID: 97 RVA: 0x000025D3 File Offset: 0x000007D3
		public NativeTypeAttribute()
		{
			this.CodegenOptions = CodegenOptions.Auto;
		}

		// Token: 0x06000062 RID: 98 RVA: 0x000025E3 File Offset: 0x000007E3
		public NativeTypeAttribute(CodegenOptions codegenOptions)
		{
			this.CodegenOptions = codegenOptions;
		}

		// Token: 0x06000063 RID: 99 RVA: 0x000025F4 File Offset: 0x000007F4
		public NativeTypeAttribute(string header)
		{
			if (header == null)
			{
				throw new ArgumentNullException("header");
			}
			if (header == "")
			{
				throw new ArgumentException("header cannot be empty", "header");
			}
			this.CodegenOptions = CodegenOptions.Auto;
			this.Header = header;
		}

		// Token: 0x06000064 RID: 100 RVA: 0x00002647 File Offset: 0x00000847
		public NativeTypeAttribute(string header, CodegenOptions codegenOptions) : this(header)
		{
			this.CodegenOptions = codegenOptions;
		}

		// Token: 0x06000065 RID: 101 RVA: 0x00002658 File Offset: 0x00000858
		public NativeTypeAttribute(CodegenOptions codegenOptions, string intermediateStructName) : this(codegenOptions)
		{
			this.IntermediateScriptingStructName = intermediateStructName;
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x06000066 RID: 102 RVA: 0x0000266C File Offset: 0x0000086C
		// (set) Token: 0x06000067 RID: 103 RVA: 0x00002686 File Offset: 0x00000886
		public string Header
		{
			[CompilerGenerated]
			get
			{
				return this.<Header>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Header>k__BackingField = value;
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000068 RID: 104 RVA: 0x00002690 File Offset: 0x00000890
		// (set) Token: 0x06000069 RID: 105 RVA: 0x000026AA File Offset: 0x000008AA
		public string IntermediateScriptingStructName
		{
			[CompilerGenerated]
			get
			{
				return this.<IntermediateScriptingStructName>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<IntermediateScriptingStructName>k__BackingField = value;
			}
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x0600006A RID: 106 RVA: 0x000026B4 File Offset: 0x000008B4
		// (set) Token: 0x0600006B RID: 107 RVA: 0x000026CE File Offset: 0x000008CE
		public CodegenOptions CodegenOptions
		{
			[CompilerGenerated]
			get
			{
				return this.<CodegenOptions>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<CodegenOptions>k__BackingField = value;
			}
		}

		// Token: 0x0400001E RID: 30
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <Header>k__BackingField;

		// Token: 0x0400001F RID: 31
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private string <IntermediateScriptingStructName>k__BackingField;

		// Token: 0x04000020 RID: 32
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private CodegenOptions <CodegenOptions>k__BackingField;
	}
}
