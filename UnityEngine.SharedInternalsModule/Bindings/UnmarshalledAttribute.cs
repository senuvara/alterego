﻿using System;

namespace UnityEngine.Bindings
{
	// Token: 0x02000020 RID: 32
	[AttributeUsage(AttributeTargets.Parameter)]
	[VisibleToOtherModules]
	internal class UnmarshalledAttribute : Attribute, IBindingsAttribute
	{
		// Token: 0x0600006D RID: 109 RVA: 0x00002050 File Offset: 0x00000250
		public UnmarshalledAttribute()
		{
		}
	}
}
