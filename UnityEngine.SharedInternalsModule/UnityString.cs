﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000028 RID: 40
	[VisibleToOtherModules]
	internal sealed class UnityString
	{
		// Token: 0x06000084 RID: 132 RVA: 0x0000282B File Offset: 0x00000A2B
		public UnityString()
		{
		}

		// Token: 0x06000085 RID: 133 RVA: 0x00002834 File Offset: 0x00000A34
		public static string Format(string fmt, params object[] args)
		{
			return string.Format(fmt, args);
		}
	}
}
