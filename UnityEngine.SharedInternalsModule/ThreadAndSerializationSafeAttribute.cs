﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000006 RID: 6
	[VisibleToOtherModules]
	internal class ThreadAndSerializationSafeAttribute : Attribute
	{
		// Token: 0x06000013 RID: 19 RVA: 0x00002050 File Offset: 0x00000250
		public ThreadAndSerializationSafeAttribute()
		{
		}
	}
}
