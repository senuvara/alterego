﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Scripting;

namespace UnityEngineInternal
{
	// Token: 0x0200000B RID: 11
	internal static class WebRequestUtils
	{
		// Token: 0x060000C5 RID: 197 RVA: 0x000045A4 File Offset: 0x000027A4
		[RequiredByNativeCode]
		internal static string RedirectTo(string baseUri, string redirectUri)
		{
			Uri uri;
			if (redirectUri[0] == '/')
			{
				uri = new Uri(redirectUri, UriKind.Relative);
			}
			else
			{
				uri = new Uri(redirectUri, UriKind.RelativeOrAbsolute);
			}
			string absoluteUri;
			if (uri.IsAbsoluteUri)
			{
				absoluteUri = uri.AbsoluteUri;
			}
			else
			{
				Uri baseUri2 = new Uri(baseUri, UriKind.Absolute);
				Uri uri2 = new Uri(baseUri2, uri);
				absoluteUri = uri2.AbsoluteUri;
			}
			return absoluteUri;
		}

		// Token: 0x060000C6 RID: 198 RVA: 0x0000460C File Offset: 0x0000280C
		internal static string MakeInitialUrl(string targetUrl, string localUrl)
		{
			string result;
			if (string.IsNullOrEmpty(targetUrl))
			{
				result = "";
			}
			else
			{
				bool prependProtocol = false;
				Uri uri = new Uri(localUrl);
				Uri uri2 = null;
				if (targetUrl[0] == '/')
				{
					uri2 = new Uri(uri, targetUrl);
					prependProtocol = true;
				}
				if (uri2 == null && WebRequestUtils.domainRegex.IsMatch(targetUrl))
				{
					targetUrl = uri.Scheme + "://" + targetUrl;
					prependProtocol = true;
				}
				FormatException ex = null;
				try
				{
					if (uri2 == null && targetUrl[0] != '.')
					{
						uri2 = new Uri(targetUrl);
					}
				}
				catch (FormatException ex2)
				{
					ex = ex2;
				}
				if (uri2 == null)
				{
					try
					{
						uri2 = new Uri(uri, targetUrl);
						prependProtocol = true;
					}
					catch (FormatException)
					{
						throw ex;
					}
				}
				result = WebRequestUtils.MakeUriString(uri2, targetUrl, prependProtocol);
			}
			return result;
		}

		// Token: 0x060000C7 RID: 199 RVA: 0x00004710 File Offset: 0x00002910
		internal static string MakeUriString(Uri targetUri, string targetUrl, bool prependProtocol)
		{
			string result;
			if (targetUri.IsFile)
			{
				if (!targetUri.IsLoopback)
				{
					result = targetUri.OriginalString;
				}
				else
				{
					string text = targetUri.AbsolutePath;
					if (text.Contains("%"))
					{
						text = WebRequestUtils.URLDecode(text);
					}
					if (text.Length > 0 && text[0] != '/')
					{
						text = '/' + text;
					}
					result = "file://" + text;
				}
			}
			else
			{
				string scheme = targetUri.Scheme;
				if (!prependProtocol && targetUrl.Length >= scheme.Length + 2 && targetUrl[scheme.Length + 1] != '/')
				{
					StringBuilder stringBuilder = new StringBuilder(scheme, targetUrl.Length);
					stringBuilder.Append(':');
					if (scheme == "jar")
					{
						string text2 = targetUri.AbsolutePath;
						if (text2.Contains("%"))
						{
							text2 = WebRequestUtils.URLDecode(text2);
						}
						if (text2.StartsWith("file:/") && text2.Length > 6 && text2[6] != '/')
						{
							stringBuilder.Append("file://");
							stringBuilder.Append(text2.Substring(5));
						}
						else
						{
							stringBuilder.Append(text2);
						}
						result = stringBuilder.ToString();
					}
					else
					{
						stringBuilder.Append(targetUri.PathAndQuery);
						stringBuilder.Append(targetUri.Fragment);
						result = stringBuilder.ToString();
					}
				}
				else if (targetUrl.Contains("%"))
				{
					result = targetUri.OriginalString;
				}
				else
				{
					result = targetUri.AbsoluteUri;
				}
			}
			return result;
		}

		// Token: 0x060000C8 RID: 200 RVA: 0x000048CC File Offset: 0x00002ACC
		private static string URLDecode(string encoded)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(encoded);
			byte[] bytes2 = WWWTranscoder.URLDecode(bytes);
			return Encoding.UTF8.GetString(bytes2);
		}

		// Token: 0x060000C9 RID: 201 RVA: 0x000048FF File Offset: 0x00002AFF
		// Note: this type is marked as 'beforefieldinit'.
		static WebRequestUtils()
		{
		}

		// Token: 0x0400004A RID: 74
		private static Regex domainRegex = new Regex("^\\s*\\w+(?:\\.\\w+)+(\\/.*)?$");
	}
}
