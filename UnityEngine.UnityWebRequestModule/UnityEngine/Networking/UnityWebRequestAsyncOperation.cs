﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Networking
{
	// Token: 0x02000005 RID: 5
	[UsedByNativeCode]
	[NativeHeader("Modules/UnityWebRequest/Public/UnityWebRequestAsyncOperation.h")]
	[NativeHeader("UnityWebRequestScriptingClasses.h")]
	[StructLayout(LayoutKind.Sequential)]
	public class UnityWebRequestAsyncOperation : AsyncOperation
	{
		// Token: 0x0600001B RID: 27 RVA: 0x0000237B File Offset: 0x0000057B
		public UnityWebRequestAsyncOperation()
		{
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600001C RID: 28 RVA: 0x00002384 File Offset: 0x00000584
		// (set) Token: 0x0600001D RID: 29 RVA: 0x0000239E File Offset: 0x0000059E
		public UnityWebRequest webRequest
		{
			[CompilerGenerated]
			get
			{
				return this.<webRequest>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<webRequest>k__BackingField = value;
			}
		}

		// Token: 0x04000008 RID: 8
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private UnityWebRequest <webRequest>k__BackingField;
	}
}
