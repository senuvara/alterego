﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Networking
{
	// Token: 0x0200000D RID: 13
	[NativeHeader("Modules/UnityWebRequest/Public/DownloadHandler/DownloadHandler.h")]
	[StructLayout(LayoutKind.Sequential)]
	public class DownloadHandler : IDisposable
	{
		// Token: 0x060000D1 RID: 209 RVA: 0x000049B7 File Offset: 0x00002BB7
		[VisibleToOtherModules]
		internal DownloadHandler()
		{
		}

		// Token: 0x060000D2 RID: 210
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Release();

		// Token: 0x060000D3 RID: 211 RVA: 0x000049C0 File Offset: 0x00002BC0
		~DownloadHandler()
		{
			this.Dispose();
		}

		// Token: 0x060000D4 RID: 212 RVA: 0x000049F0 File Offset: 0x00002BF0
		public void Dispose()
		{
			if (this.m_Ptr != IntPtr.Zero)
			{
				this.Release();
				this.m_Ptr = IntPtr.Zero;
			}
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x060000D5 RID: 213 RVA: 0x00004A1C File Offset: 0x00002C1C
		public bool isDone
		{
			get
			{
				return this.IsDone();
			}
		}

		// Token: 0x060000D6 RID: 214
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool IsDone();

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x060000D7 RID: 215 RVA: 0x00004A38 File Offset: 0x00002C38
		public byte[] data
		{
			get
			{
				return this.GetData();
			}
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x060000D8 RID: 216 RVA: 0x00004A54 File Offset: 0x00002C54
		public string text
		{
			get
			{
				return this.GetText();
			}
		}

		// Token: 0x060000D9 RID: 217 RVA: 0x00004A70 File Offset: 0x00002C70
		protected virtual byte[] GetData()
		{
			return null;
		}

		// Token: 0x060000DA RID: 218 RVA: 0x00004A88 File Offset: 0x00002C88
		protected virtual string GetText()
		{
			byte[] data = this.GetData();
			string result;
			if (data != null && data.Length > 0)
			{
				result = this.GetTextEncoder().GetString(data, 0, data.Length);
			}
			else
			{
				result = "";
			}
			return result;
		}

		// Token: 0x060000DB RID: 219 RVA: 0x00004AD0 File Offset: 0x00002CD0
		private Encoding GetTextEncoder()
		{
			string contentType = this.GetContentType();
			if (!string.IsNullOrEmpty(contentType))
			{
				int num = contentType.IndexOf("charset", StringComparison.OrdinalIgnoreCase);
				if (num > -1)
				{
					int num2 = contentType.IndexOf('=', num);
					if (num2 > -1)
					{
						string text = contentType.Substring(num2 + 1).Trim().Trim(new char[]
						{
							'\'',
							'"'
						}).Trim();
						int num3 = text.IndexOf(';');
						if (num3 > -1)
						{
							text = text.Substring(0, num3);
						}
						try
						{
							return Encoding.GetEncoding(text);
						}
						catch (ArgumentException ex)
						{
							Debug.LogWarning(string.Format("Unsupported encoding '{0}': {1}", text, ex.Message));
						}
					}
				}
			}
			return Encoding.UTF8;
		}

		// Token: 0x060000DC RID: 220
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern string GetContentType();

		// Token: 0x060000DD RID: 221 RVA: 0x00004BAC File Offset: 0x00002DAC
		[UsedByNativeCode]
		protected virtual bool ReceiveData(byte[] data, int dataLength)
		{
			return true;
		}

		// Token: 0x060000DE RID: 222 RVA: 0x00004BC2 File Offset: 0x00002DC2
		[UsedByNativeCode]
		protected virtual void ReceiveContentLength(int contentLength)
		{
		}

		// Token: 0x060000DF RID: 223 RVA: 0x00004BC2 File Offset: 0x00002DC2
		[UsedByNativeCode]
		protected virtual void CompleteContent()
		{
		}

		// Token: 0x060000E0 RID: 224 RVA: 0x00004BC8 File Offset: 0x00002DC8
		[UsedByNativeCode]
		protected virtual float GetProgress()
		{
			return 0f;
		}

		// Token: 0x060000E1 RID: 225 RVA: 0x00004BE4 File Offset: 0x00002DE4
		protected static T GetCheckedDownloader<T>(UnityWebRequest www) where T : DownloadHandler
		{
			if (www == null)
			{
				throw new NullReferenceException("Cannot get content from a null UnityWebRequest object");
			}
			if (!www.isDone)
			{
				throw new InvalidOperationException("Cannot get content from an unfinished UnityWebRequest object");
			}
			if (www.isNetworkError)
			{
				throw new InvalidOperationException(www.error);
			}
			return (T)((object)www.downloadHandler);
		}

		// Token: 0x060000E2 RID: 226
		[VisibleToOtherModules]
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern byte[] InternalGetByteArray(DownloadHandler dh);

		// Token: 0x0400004C RID: 76
		[VisibleToOtherModules]
		[NonSerialized]
		internal IntPtr m_Ptr;
	}
}
