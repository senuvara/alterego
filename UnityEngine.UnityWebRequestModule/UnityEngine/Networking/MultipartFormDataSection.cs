﻿using System;
using System.Text;

namespace UnityEngine.Networking
{
	// Token: 0x02000003 RID: 3
	public class MultipartFormDataSection : IMultipartFormSection
	{
		// Token: 0x06000005 RID: 5 RVA: 0x00002050 File Offset: 0x00000250
		public MultipartFormDataSection(string name, byte[] data, string contentType)
		{
			if (data == null || data.Length < 1)
			{
				throw new ArgumentException("Cannot create a multipart form data section without body data");
			}
			this.name = name;
			this.data = data;
			this.content = contentType;
		}

		// Token: 0x06000006 RID: 6 RVA: 0x00002089 File Offset: 0x00000289
		public MultipartFormDataSection(string name, byte[] data) : this(name, data, null)
		{
		}

		// Token: 0x06000007 RID: 7 RVA: 0x00002095 File Offset: 0x00000295
		public MultipartFormDataSection(byte[] data) : this(null, data)
		{
		}

		// Token: 0x06000008 RID: 8 RVA: 0x000020A0 File Offset: 0x000002A0
		public MultipartFormDataSection(string name, string data, Encoding encoding, string contentType)
		{
			if (data == null || data.Length < 1)
			{
				throw new ArgumentException("Cannot create a multipart form data section without body data");
			}
			byte[] bytes = encoding.GetBytes(data);
			this.name = name;
			this.data = bytes;
			if (contentType != null && !contentType.Contains("encoding="))
			{
				contentType = contentType.Trim() + "; encoding=" + encoding.WebName;
			}
			this.content = contentType;
		}

		// Token: 0x06000009 RID: 9 RVA: 0x00002123 File Offset: 0x00000323
		public MultipartFormDataSection(string name, string data, string contentType) : this(name, data, Encoding.UTF8, contentType)
		{
		}

		// Token: 0x0600000A RID: 10 RVA: 0x00002134 File Offset: 0x00000334
		public MultipartFormDataSection(string name, string data) : this(name, data, "text/plain")
		{
		}

		// Token: 0x0600000B RID: 11 RVA: 0x00002144 File Offset: 0x00000344
		public MultipartFormDataSection(string data) : this(null, data)
		{
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x0600000C RID: 12 RVA: 0x00002150 File Offset: 0x00000350
		public string sectionName
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x0600000D RID: 13 RVA: 0x0000216C File Offset: 0x0000036C
		public byte[] sectionData
		{
			get
			{
				return this.data;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600000E RID: 14 RVA: 0x00002188 File Offset: 0x00000388
		public string fileName
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x0600000F RID: 15 RVA: 0x000021A0 File Offset: 0x000003A0
		public string contentType
		{
			get
			{
				return this.content;
			}
		}

		// Token: 0x04000001 RID: 1
		private string name;

		// Token: 0x04000002 RID: 2
		private byte[] data;

		// Token: 0x04000003 RID: 3
		private string content;
	}
}
