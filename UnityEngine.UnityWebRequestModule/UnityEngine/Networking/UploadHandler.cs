﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;

namespace UnityEngine.Networking
{
	// Token: 0x02000011 RID: 17
	[NativeHeader("Modules/UnityWebRequest/Public/UploadHandler/UploadHandler.h")]
	[StructLayout(LayoutKind.Sequential)]
	public class UploadHandler : IDisposable
	{
		// Token: 0x060000F6 RID: 246 RVA: 0x000049B7 File Offset: 0x00002BB7
		internal UploadHandler()
		{
		}

		// Token: 0x060000F7 RID: 247
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Release();

		// Token: 0x060000F8 RID: 248 RVA: 0x00004D74 File Offset: 0x00002F74
		~UploadHandler()
		{
			this.Dispose();
		}

		// Token: 0x060000F9 RID: 249 RVA: 0x00004DA4 File Offset: 0x00002FA4
		public void Dispose()
		{
			if (this.m_Ptr != IntPtr.Zero)
			{
				this.Release();
				this.m_Ptr = IntPtr.Zero;
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x060000FA RID: 250 RVA: 0x00004DD0 File Offset: 0x00002FD0
		public byte[] data
		{
			get
			{
				return this.GetData();
			}
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x060000FB RID: 251 RVA: 0x00004DEC File Offset: 0x00002FEC
		// (set) Token: 0x060000FC RID: 252 RVA: 0x00004E07 File Offset: 0x00003007
		public string contentType
		{
			get
			{
				return this.GetContentType();
			}
			set
			{
				this.SetContentType(value);
			}
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x060000FD RID: 253 RVA: 0x00004E14 File Offset: 0x00003014
		public float progress
		{
			get
			{
				return this.GetProgress();
			}
		}

		// Token: 0x060000FE RID: 254 RVA: 0x00004E30 File Offset: 0x00003030
		internal virtual byte[] GetData()
		{
			return null;
		}

		// Token: 0x060000FF RID: 255 RVA: 0x00004E48 File Offset: 0x00003048
		internal virtual string GetContentType()
		{
			return "text/plain";
		}

		// Token: 0x06000100 RID: 256 RVA: 0x00004BC2 File Offset: 0x00002DC2
		internal virtual void SetContentType(string newContentType)
		{
		}

		// Token: 0x06000101 RID: 257 RVA: 0x00004E64 File Offset: 0x00003064
		internal virtual float GetProgress()
		{
			return 0.5f;
		}

		// Token: 0x0400004D RID: 77
		[NonSerialized]
		internal IntPtr m_Ptr;
	}
}
