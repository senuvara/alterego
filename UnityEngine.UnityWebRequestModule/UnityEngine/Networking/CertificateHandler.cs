﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Networking
{
	// Token: 0x0200000C RID: 12
	[NativeHeader("Modules/UnityWebRequest/Public/CertificateHandler/CertificateHandlerScript.h")]
	[StructLayout(LayoutKind.Sequential)]
	public class CertificateHandler : IDisposable
	{
		// Token: 0x060000CA RID: 202 RVA: 0x00004910 File Offset: 0x00002B10
		protected CertificateHandler()
		{
			this.m_Ptr = CertificateHandler.Create(this);
		}

		// Token: 0x060000CB RID: 203
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr Create(CertificateHandler obj);

		// Token: 0x060000CC RID: 204
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Release();

		// Token: 0x060000CD RID: 205 RVA: 0x00004928 File Offset: 0x00002B28
		~CertificateHandler()
		{
			this.Dispose();
		}

		// Token: 0x060000CE RID: 206 RVA: 0x00004958 File Offset: 0x00002B58
		protected virtual bool ValidateCertificate(byte[] certificateData)
		{
			return false;
		}

		// Token: 0x060000CF RID: 207 RVA: 0x00004970 File Offset: 0x00002B70
		[RequiredByNativeCode]
		internal bool ValidateCertificateNative(byte[] certificateData)
		{
			return this.ValidateCertificate(certificateData);
		}

		// Token: 0x060000D0 RID: 208 RVA: 0x0000498C File Offset: 0x00002B8C
		public void Dispose()
		{
			if (this.m_Ptr != IntPtr.Zero)
			{
				this.Release();
				this.m_Ptr = IntPtr.Zero;
			}
		}

		// Token: 0x0400004B RID: 75
		[NonSerialized]
		internal IntPtr m_Ptr;
	}
}
