﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;

namespace UnityEngine.Networking
{
	// Token: 0x0200000E RID: 14
	[NativeHeader("Modules/UnityWebRequest/Public/DownloadHandler/DownloadHandlerBuffer.h")]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class DownloadHandlerBuffer : DownloadHandler
	{
		// Token: 0x060000E3 RID: 227 RVA: 0x00004C42 File Offset: 0x00002E42
		public DownloadHandlerBuffer()
		{
			this.InternalCreateBuffer();
		}

		// Token: 0x060000E4 RID: 228
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr Create(DownloadHandlerBuffer obj);

		// Token: 0x060000E5 RID: 229 RVA: 0x00004C51 File Offset: 0x00002E51
		private void InternalCreateBuffer()
		{
			this.m_Ptr = DownloadHandlerBuffer.Create(this);
		}

		// Token: 0x060000E6 RID: 230 RVA: 0x00004C60 File Offset: 0x00002E60
		protected override byte[] GetData()
		{
			return this.InternalGetData();
		}

		// Token: 0x060000E7 RID: 231 RVA: 0x00004C7C File Offset: 0x00002E7C
		private byte[] InternalGetData()
		{
			return DownloadHandler.InternalGetByteArray(this);
		}

		// Token: 0x060000E8 RID: 232 RVA: 0x00004C98 File Offset: 0x00002E98
		public static string GetContent(UnityWebRequest www)
		{
			return DownloadHandler.GetCheckedDownloader<DownloadHandlerBuffer>(www).text;
		}
	}
}
