﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;

namespace UnityEngine.Networking
{
	// Token: 0x02000012 RID: 18
	[NativeHeader("Modules/UnityWebRequest/Public/UploadHandler/UploadHandlerRaw.h")]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class UploadHandlerRaw : UploadHandler
	{
		// Token: 0x06000102 RID: 258 RVA: 0x00004E7E File Offset: 0x0000307E
		public UploadHandlerRaw(byte[] data)
		{
			if (data != null && data.Length == 0)
			{
				throw new ArgumentException("Cannot create a data handler without payload data");
			}
			this.m_Ptr = UploadHandlerRaw.Create(this, data);
		}

		// Token: 0x06000103 RID: 259
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr Create(UploadHandlerRaw self, byte[] data);

		// Token: 0x06000104 RID: 260
		[NativeMethod("GetContentType")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern string InternalGetContentType();

		// Token: 0x06000105 RID: 261
		[NativeMethod("SetContentType")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void InternalSetContentType(string newContentType);

		// Token: 0x06000106 RID: 262
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern byte[] InternalGetData();

		// Token: 0x06000107 RID: 263
		[NativeMethod("GetProgress")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern float InternalGetProgress();

		// Token: 0x06000108 RID: 264 RVA: 0x00004EB0 File Offset: 0x000030B0
		internal override string GetContentType()
		{
			return this.InternalGetContentType();
		}

		// Token: 0x06000109 RID: 265 RVA: 0x00004ECB File Offset: 0x000030CB
		internal override void SetContentType(string newContentType)
		{
			this.InternalSetContentType(newContentType);
		}

		// Token: 0x0600010A RID: 266 RVA: 0x00004ED8 File Offset: 0x000030D8
		internal override byte[] GetData()
		{
			return this.InternalGetData();
		}

		// Token: 0x0600010B RID: 267 RVA: 0x00004EF4 File Offset: 0x000030F4
		internal override float GetProgress()
		{
			return this.InternalGetProgress();
		}
	}
}
