﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;

namespace UnityEngine.Networking
{
	// Token: 0x02000010 RID: 16
	[NativeHeader("Modules/UnityWebRequest/Public/DownloadHandler/DownloadHandlerVFS.h")]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class DownloadHandlerFile : DownloadHandler
	{
		// Token: 0x060000EF RID: 239 RVA: 0x00004D11 File Offset: 0x00002F11
		public DownloadHandlerFile(string path)
		{
			this.InternalCreateVFS(path);
		}

		// Token: 0x060000F0 RID: 240
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr Create(DownloadHandlerFile obj, string path);

		// Token: 0x060000F1 RID: 241 RVA: 0x00004D24 File Offset: 0x00002F24
		private void InternalCreateVFS(string path)
		{
			string directoryName = Path.GetDirectoryName(path);
			if (!Directory.Exists(directoryName))
			{
				Directory.CreateDirectory(directoryName);
			}
			this.m_Ptr = DownloadHandlerFile.Create(this, path);
		}

		// Token: 0x060000F2 RID: 242 RVA: 0x00004D58 File Offset: 0x00002F58
		protected override byte[] GetData()
		{
			throw new NotSupportedException("Raw data access is not supported");
		}

		// Token: 0x060000F3 RID: 243 RVA: 0x00004D65 File Offset: 0x00002F65
		protected override string GetText()
		{
			throw new NotSupportedException("String access is not supported");
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x060000F4 RID: 244
		// (set) Token: 0x060000F5 RID: 245
		public extern bool removeFileOnAbort { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
