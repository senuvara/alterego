﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine.Bindings;
using UnityEngineInternal;

namespace UnityEngine.Networking
{
	// Token: 0x02000006 RID: 6
	[NativeHeader("Modules/UnityWebRequest/Public/UnityWebRequest.h")]
	[StructLayout(LayoutKind.Sequential)]
	public class UnityWebRequest : IDisposable
	{
		// Token: 0x0600001E RID: 30 RVA: 0x000023A7 File Offset: 0x000005A7
		public UnityWebRequest()
		{
			this.m_Ptr = UnityWebRequest.Create();
			this.InternalSetDefaults();
		}

		// Token: 0x0600001F RID: 31 RVA: 0x000023C1 File Offset: 0x000005C1
		public UnityWebRequest(string url)
		{
			this.m_Ptr = UnityWebRequest.Create();
			this.InternalSetDefaults();
			this.url = url;
		}

		// Token: 0x06000020 RID: 32 RVA: 0x000023E2 File Offset: 0x000005E2
		public UnityWebRequest(Uri uri)
		{
			this.m_Ptr = UnityWebRequest.Create();
			this.InternalSetDefaults();
			this.uri = uri;
		}

		// Token: 0x06000021 RID: 33 RVA: 0x00002403 File Offset: 0x00000603
		public UnityWebRequest(string url, string method)
		{
			this.m_Ptr = UnityWebRequest.Create();
			this.InternalSetDefaults();
			this.url = url;
			this.method = method;
		}

		// Token: 0x06000022 RID: 34 RVA: 0x0000242B File Offset: 0x0000062B
		public UnityWebRequest(Uri uri, string method)
		{
			this.m_Ptr = UnityWebRequest.Create();
			this.InternalSetDefaults();
			this.uri = uri;
			this.method = method;
		}

		// Token: 0x06000023 RID: 35 RVA: 0x00002453 File Offset: 0x00000653
		public UnityWebRequest(string url, string method, DownloadHandler downloadHandler, UploadHandler uploadHandler)
		{
			this.m_Ptr = UnityWebRequest.Create();
			this.InternalSetDefaults();
			this.url = url;
			this.method = method;
			this.downloadHandler = downloadHandler;
			this.uploadHandler = uploadHandler;
		}

		// Token: 0x06000024 RID: 36 RVA: 0x0000248A File Offset: 0x0000068A
		public UnityWebRequest(Uri uri, string method, DownloadHandler downloadHandler, UploadHandler uploadHandler)
		{
			this.m_Ptr = UnityWebRequest.Create();
			this.InternalSetDefaults();
			this.uri = uri;
			this.method = method;
			this.downloadHandler = downloadHandler;
			this.uploadHandler = uploadHandler;
		}

		// Token: 0x06000025 RID: 37
		[NativeMethod(IsThreadSafe = true)]
		[NativeConditional("ENABLE_UNITYWEBREQUEST")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetWebErrorString(UnityWebRequest.UnityWebRequestError err);

		// Token: 0x06000026 RID: 38
		[VisibleToOtherModules]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string GetHTTPStatusString(long responseCode);

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x06000027 RID: 39 RVA: 0x000024C4 File Offset: 0x000006C4
		// (set) Token: 0x06000028 RID: 40 RVA: 0x000024DE File Offset: 0x000006DE
		public bool disposeCertificateHandlerOnDispose
		{
			[CompilerGenerated]
			get
			{
				return this.<disposeCertificateHandlerOnDispose>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<disposeCertificateHandlerOnDispose>k__BackingField = value;
			}
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000029 RID: 41 RVA: 0x000024E8 File Offset: 0x000006E8
		// (set) Token: 0x0600002A RID: 42 RVA: 0x00002502 File Offset: 0x00000702
		public bool disposeDownloadHandlerOnDispose
		{
			[CompilerGenerated]
			get
			{
				return this.<disposeDownloadHandlerOnDispose>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<disposeDownloadHandlerOnDispose>k__BackingField = value;
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x0600002B RID: 43 RVA: 0x0000250C File Offset: 0x0000070C
		// (set) Token: 0x0600002C RID: 44 RVA: 0x00002526 File Offset: 0x00000726
		public bool disposeUploadHandlerOnDispose
		{
			[CompilerGenerated]
			get
			{
				return this.<disposeUploadHandlerOnDispose>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<disposeUploadHandlerOnDispose>k__BackingField = value;
			}
		}

		// Token: 0x0600002D RID: 45 RVA: 0x0000252F File Offset: 0x0000072F
		public static void ClearCookieCache()
		{
			UnityWebRequest.ClearCookieCache(null, null);
		}

		// Token: 0x0600002E RID: 46 RVA: 0x0000253C File Offset: 0x0000073C
		public static void ClearCookieCache(Uri uri)
		{
			if (uri == null)
			{
				UnityWebRequest.ClearCookieCache(null, null);
			}
			else
			{
				string host = uri.Host;
				string text = uri.AbsolutePath;
				if (text == "/")
				{
					text = null;
				}
				UnityWebRequest.ClearCookieCache(host, text);
			}
		}

		// Token: 0x0600002F RID: 47
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ClearCookieCache(string domain, string path);

		// Token: 0x06000030 RID: 48
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern IntPtr Create();

		// Token: 0x06000031 RID: 49
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Release();

		// Token: 0x06000032 RID: 50 RVA: 0x0000258B File Offset: 0x0000078B
		internal void InternalDestroy()
		{
			if (this.m_Ptr != IntPtr.Zero)
			{
				this.Abort();
				this.Release();
				this.m_Ptr = IntPtr.Zero;
			}
		}

		// Token: 0x06000033 RID: 51 RVA: 0x000025BC File Offset: 0x000007BC
		private void InternalSetDefaults()
		{
			this.disposeDownloadHandlerOnDispose = true;
			this.disposeUploadHandlerOnDispose = true;
			this.disposeCertificateHandlerOnDispose = true;
		}

		// Token: 0x06000034 RID: 52 RVA: 0x000025D4 File Offset: 0x000007D4
		~UnityWebRequest()
		{
			this.DisposeHandlers();
			this.InternalDestroy();
		}

		// Token: 0x06000035 RID: 53 RVA: 0x0000260C File Offset: 0x0000080C
		public void Dispose()
		{
			this.DisposeHandlers();
			this.InternalDestroy();
			GC.SuppressFinalize(this);
		}

		// Token: 0x06000036 RID: 54 RVA: 0x00002624 File Offset: 0x00000824
		private void DisposeHandlers()
		{
			if (this.disposeDownloadHandlerOnDispose)
			{
				DownloadHandler downloadHandler = this.downloadHandler;
				if (downloadHandler != null)
				{
					downloadHandler.Dispose();
				}
			}
			if (this.disposeUploadHandlerOnDispose)
			{
				UploadHandler uploadHandler = this.uploadHandler;
				if (uploadHandler != null)
				{
					uploadHandler.Dispose();
				}
			}
			if (this.disposeCertificateHandlerOnDispose)
			{
				CertificateHandler certificateHandler = this.certificateHandler;
				if (certificateHandler != null)
				{
					certificateHandler.Dispose();
				}
			}
		}

		// Token: 0x06000037 RID: 55
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern UnityWebRequestAsyncOperation BeginWebRequest();

		// Token: 0x06000038 RID: 56 RVA: 0x00002698 File Offset: 0x00000898
		[Obsolete("Use SendWebRequest.  It returns a UnityWebRequestAsyncOperation which contains a reference to the WebRequest object.", false)]
		public AsyncOperation Send()
		{
			return this.SendWebRequest();
		}

		// Token: 0x06000039 RID: 57 RVA: 0x000026B4 File Offset: 0x000008B4
		public UnityWebRequestAsyncOperation SendWebRequest()
		{
			UnityWebRequestAsyncOperation unityWebRequestAsyncOperation = this.BeginWebRequest();
			if (unityWebRequestAsyncOperation != null)
			{
				unityWebRequestAsyncOperation.webRequest = this;
			}
			return unityWebRequestAsyncOperation;
		}

		// Token: 0x0600003A RID: 58
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Abort();

		// Token: 0x0600003B RID: 59
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern UnityWebRequest.UnityWebRequestError SetMethod(UnityWebRequest.UnityWebRequestMethod methodType);

		// Token: 0x0600003C RID: 60 RVA: 0x000026E0 File Offset: 0x000008E0
		internal void InternalSetMethod(UnityWebRequest.UnityWebRequestMethod methodType)
		{
			if (!this.isModifiable)
			{
				throw new InvalidOperationException("UnityWebRequest has already been sent and its request method can no longer be altered");
			}
			UnityWebRequest.UnityWebRequestError unityWebRequestError = this.SetMethod(methodType);
			if (unityWebRequestError != UnityWebRequest.UnityWebRequestError.OK)
			{
				throw new InvalidOperationException(UnityWebRequest.GetWebErrorString(unityWebRequestError));
			}
		}

		// Token: 0x0600003D RID: 61
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern UnityWebRequest.UnityWebRequestError SetCustomMethod(string customMethodName);

		// Token: 0x0600003E RID: 62 RVA: 0x00002720 File Offset: 0x00000920
		internal void InternalSetCustomMethod(string customMethodName)
		{
			if (!this.isModifiable)
			{
				throw new InvalidOperationException("UnityWebRequest has already been sent and its request method can no longer be altered");
			}
			UnityWebRequest.UnityWebRequestError unityWebRequestError = this.SetCustomMethod(customMethodName);
			if (unityWebRequestError != UnityWebRequest.UnityWebRequestError.OK)
			{
				throw new InvalidOperationException(UnityWebRequest.GetWebErrorString(unityWebRequestError));
			}
		}

		// Token: 0x0600003F RID: 63
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern UnityWebRequest.UnityWebRequestMethod GetMethod();

		// Token: 0x06000040 RID: 64
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern string GetCustomMethod();

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000041 RID: 65 RVA: 0x00002760 File Offset: 0x00000960
		// (set) Token: 0x06000042 RID: 66 RVA: 0x000027CC File Offset: 0x000009CC
		public string method
		{
			get
			{
				string result;
				switch (this.GetMethod())
				{
				case UnityWebRequest.UnityWebRequestMethod.Get:
					result = "GET";
					break;
				case UnityWebRequest.UnityWebRequestMethod.Post:
					result = "POST";
					break;
				case UnityWebRequest.UnityWebRequestMethod.Put:
					result = "PUT";
					break;
				case UnityWebRequest.UnityWebRequestMethod.Head:
					result = "HEAD";
					break;
				default:
					result = this.GetCustomMethod();
					break;
				}
				return result;
			}
			set
			{
				if (string.IsNullOrEmpty(value))
				{
					throw new ArgumentException("Cannot set a UnityWebRequest's method to an empty or null string");
				}
				string text = value.ToUpper();
				if (text != null)
				{
					if (text == "GET")
					{
						this.InternalSetMethod(UnityWebRequest.UnityWebRequestMethod.Get);
						return;
					}
					if (text == "POST")
					{
						this.InternalSetMethod(UnityWebRequest.UnityWebRequestMethod.Post);
						return;
					}
					if (text == "PUT")
					{
						this.InternalSetMethod(UnityWebRequest.UnityWebRequestMethod.Put);
						return;
					}
					if (text == "HEAD")
					{
						this.InternalSetMethod(UnityWebRequest.UnityWebRequestMethod.Head);
						return;
					}
				}
				this.InternalSetCustomMethod(value.ToUpper());
			}
		}

		// Token: 0x06000043 RID: 67
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern UnityWebRequest.UnityWebRequestError GetError();

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000044 RID: 68 RVA: 0x00002884 File Offset: 0x00000A84
		public string error
		{
			get
			{
				string result;
				if (!this.isNetworkError && !this.isHttpError)
				{
					result = null;
				}
				else if (this.isHttpError)
				{
					string httpstatusString = UnityWebRequest.GetHTTPStatusString(this.responseCode);
					result = string.Format("HTTP/1.1 {0} {1}", this.responseCode, httpstatusString);
				}
				else
				{
					result = UnityWebRequest.GetWebErrorString(this.GetError());
				}
				return result;
			}
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000045 RID: 69
		// (set) Token: 0x06000046 RID: 70
		private extern bool use100Continue { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000047 RID: 71 RVA: 0x000028F8 File Offset: 0x00000AF8
		// (set) Token: 0x06000048 RID: 72 RVA: 0x00002913 File Offset: 0x00000B13
		public bool useHttpContinue
		{
			get
			{
				return this.use100Continue;
			}
			set
			{
				if (!this.isModifiable)
				{
					throw new InvalidOperationException("UnityWebRequest has already been sent and its 100-Continue setting cannot be altered");
				}
				this.use100Continue = value;
			}
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000049 RID: 73 RVA: 0x00002934 File Offset: 0x00000B34
		// (set) Token: 0x0600004A RID: 74 RVA: 0x00002950 File Offset: 0x00000B50
		public string url
		{
			get
			{
				return this.GetUrl();
			}
			set
			{
				string localUrl = "http://localhost/";
				this.InternalSetUrl(WebRequestUtils.MakeInitialUrl(value, localUrl));
			}
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x0600004B RID: 75 RVA: 0x00002974 File Offset: 0x00000B74
		// (set) Token: 0x0600004C RID: 76 RVA: 0x00002994 File Offset: 0x00000B94
		public Uri uri
		{
			get
			{
				return new Uri(this.GetUrl());
			}
			set
			{
				if (!value.IsAbsoluteUri)
				{
					throw new ArgumentException("URI must be absolute");
				}
				this.InternalSetUrl(WebRequestUtils.MakeUriString(value, value.OriginalString, false));
				this.m_Uri = value;
			}
		}

		// Token: 0x0600004D RID: 77
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern string GetUrl();

		// Token: 0x0600004E RID: 78
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern UnityWebRequest.UnityWebRequestError SetUrl(string url);

		// Token: 0x0600004F RID: 79 RVA: 0x000029C8 File Offset: 0x00000BC8
		private void InternalSetUrl(string url)
		{
			if (!this.isModifiable)
			{
				throw new InvalidOperationException("UnityWebRequest has already been sent and its URL cannot be altered");
			}
			UnityWebRequest.UnityWebRequestError unityWebRequestError = this.SetUrl(url);
			if (unityWebRequestError != UnityWebRequest.UnityWebRequestError.OK)
			{
				throw new InvalidOperationException(UnityWebRequest.GetWebErrorString(unityWebRequestError));
			}
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000050 RID: 80
		public extern long responseCode { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000051 RID: 81
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern float GetUploadProgress();

		// Token: 0x06000052 RID: 82
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool IsExecuting();

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000053 RID: 83 RVA: 0x00002A08 File Offset: 0x00000C08
		public float uploadProgress
		{
			get
			{
				float result;
				if (!this.IsExecuting() && !this.isDone)
				{
					result = -1f;
				}
				else
				{
					result = this.GetUploadProgress();
				}
				return result;
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000054 RID: 84
		public extern bool isModifiable { [NativeMethod("IsModifiable")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000055 RID: 85
		public extern bool isDone { [NativeMethod("IsDone")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000056 RID: 86
		public extern bool isNetworkError { [NativeMethod("IsNetworkError")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000057 RID: 87
		public extern bool isHttpError { [NativeMethod("IsHttpError")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000058 RID: 88
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern float GetDownloadProgress();

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x06000059 RID: 89 RVA: 0x00002A44 File Offset: 0x00000C44
		public float downloadProgress
		{
			get
			{
				float result;
				if (!this.IsExecuting() && !this.isDone)
				{
					result = -1f;
				}
				else
				{
					result = this.GetDownloadProgress();
				}
				return result;
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x0600005A RID: 90
		public extern ulong uploadedBytes { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x0600005B RID: 91
		public extern ulong downloadedBytes { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600005C RID: 92
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int GetRedirectLimit();

		// Token: 0x0600005D RID: 93
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetRedirectLimitFromScripting(int limit);

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x0600005E RID: 94 RVA: 0x00002A80 File Offset: 0x00000C80
		// (set) Token: 0x0600005F RID: 95 RVA: 0x00002A9B File Offset: 0x00000C9B
		public int redirectLimit
		{
			get
			{
				return this.GetRedirectLimit();
			}
			set
			{
				this.SetRedirectLimitFromScripting(value);
			}
		}

		// Token: 0x06000060 RID: 96
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool GetChunked();

		// Token: 0x06000061 RID: 97
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern UnityWebRequest.UnityWebRequestError SetChunked(bool chunked);

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x06000062 RID: 98 RVA: 0x00002AA8 File Offset: 0x00000CA8
		// (set) Token: 0x06000063 RID: 99 RVA: 0x00002AC4 File Offset: 0x00000CC4
		public bool chunkedTransfer
		{
			get
			{
				return this.GetChunked();
			}
			set
			{
				if (!this.isModifiable)
				{
					throw new InvalidOperationException("UnityWebRequest has already been sent and its chunked transfer encoding setting cannot be altered");
				}
				UnityWebRequest.UnityWebRequestError unityWebRequestError = this.SetChunked(value);
				if (unityWebRequestError != UnityWebRequest.UnityWebRequestError.OK)
				{
					throw new InvalidOperationException(UnityWebRequest.GetWebErrorString(unityWebRequestError));
				}
			}
		}

		// Token: 0x06000064 RID: 100
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern string GetRequestHeader(string name);

		// Token: 0x06000065 RID: 101
		[NativeMethod("SetRequestHeader")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern UnityWebRequest.UnityWebRequestError InternalSetRequestHeader(string name, string value);

		// Token: 0x06000066 RID: 102 RVA: 0x00002B04 File Offset: 0x00000D04
		public void SetRequestHeader(string name, string value)
		{
			if (string.IsNullOrEmpty(name))
			{
				throw new ArgumentException("Cannot set a Request Header with a null or empty name");
			}
			if (value == null)
			{
				throw new ArgumentException("Cannot set a Request header with a null");
			}
			if (!this.isModifiable)
			{
				throw new InvalidOperationException("UnityWebRequest has already been sent and its request headers cannot be altered");
			}
			UnityWebRequest.UnityWebRequestError unityWebRequestError = this.InternalSetRequestHeader(name, value);
			if (unityWebRequestError != UnityWebRequest.UnityWebRequestError.OK)
			{
				throw new InvalidOperationException(UnityWebRequest.GetWebErrorString(unityWebRequestError));
			}
		}

		// Token: 0x06000067 RID: 103
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern string GetResponseHeader(string name);

		// Token: 0x06000068 RID: 104
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern string[] GetResponseHeaderKeys();

		// Token: 0x06000069 RID: 105 RVA: 0x00002B6C File Offset: 0x00000D6C
		public Dictionary<string, string> GetResponseHeaders()
		{
			string[] responseHeaderKeys = this.GetResponseHeaderKeys();
			Dictionary<string, string> result;
			if (responseHeaderKeys == null || responseHeaderKeys.Length == 0)
			{
				result = null;
			}
			else
			{
				Dictionary<string, string> dictionary = new Dictionary<string, string>(responseHeaderKeys.Length, StringComparer.OrdinalIgnoreCase);
				for (int i = 0; i < responseHeaderKeys.Length; i++)
				{
					string responseHeader = this.GetResponseHeader(responseHeaderKeys[i]);
					dictionary.Add(responseHeaderKeys[i], responseHeader);
				}
				result = dictionary;
			}
			return result;
		}

		// Token: 0x0600006A RID: 106
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern UnityWebRequest.UnityWebRequestError SetUploadHandler(UploadHandler uh);

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x0600006B RID: 107 RVA: 0x00002BDC File Offset: 0x00000DDC
		// (set) Token: 0x0600006C RID: 108 RVA: 0x00002BF8 File Offset: 0x00000DF8
		public UploadHandler uploadHandler
		{
			get
			{
				return this.m_UploadHandler;
			}
			set
			{
				if (!this.isModifiable)
				{
					throw new InvalidOperationException("UnityWebRequest has already been sent; cannot modify the upload handler");
				}
				UnityWebRequest.UnityWebRequestError unityWebRequestError = this.SetUploadHandler(value);
				if (unityWebRequestError != UnityWebRequest.UnityWebRequestError.OK)
				{
					throw new InvalidOperationException(UnityWebRequest.GetWebErrorString(unityWebRequestError));
				}
				this.m_UploadHandler = value;
			}
		}

		// Token: 0x0600006D RID: 109
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern UnityWebRequest.UnityWebRequestError SetDownloadHandler(DownloadHandler dh);

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x0600006E RID: 110 RVA: 0x00002C40 File Offset: 0x00000E40
		// (set) Token: 0x0600006F RID: 111 RVA: 0x00002C5C File Offset: 0x00000E5C
		public DownloadHandler downloadHandler
		{
			get
			{
				return this.m_DownloadHandler;
			}
			set
			{
				if (!this.isModifiable)
				{
					throw new InvalidOperationException("UnityWebRequest has already been sent; cannot modify the download handler");
				}
				UnityWebRequest.UnityWebRequestError unityWebRequestError = this.SetDownloadHandler(value);
				if (unityWebRequestError != UnityWebRequest.UnityWebRequestError.OK)
				{
					throw new InvalidOperationException(UnityWebRequest.GetWebErrorString(unityWebRequestError));
				}
				this.m_DownloadHandler = value;
			}
		}

		// Token: 0x06000070 RID: 112
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern UnityWebRequest.UnityWebRequestError SetCertificateHandler(CertificateHandler ch);

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x06000071 RID: 113 RVA: 0x00002CA4 File Offset: 0x00000EA4
		// (set) Token: 0x06000072 RID: 114 RVA: 0x00002CC0 File Offset: 0x00000EC0
		public CertificateHandler certificateHandler
		{
			get
			{
				return this.m_CertificateHandler;
			}
			set
			{
				if (!this.isModifiable)
				{
					throw new InvalidOperationException("UnityWebRequest has already been sent; cannot modify the certificate handler");
				}
				UnityWebRequest.UnityWebRequestError unityWebRequestError = this.SetCertificateHandler(value);
				if (unityWebRequestError != UnityWebRequest.UnityWebRequestError.OK)
				{
					throw new InvalidOperationException(UnityWebRequest.GetWebErrorString(unityWebRequestError));
				}
				this.m_CertificateHandler = value;
			}
		}

		// Token: 0x06000073 RID: 115
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int GetTimeoutMsec();

		// Token: 0x06000074 RID: 116
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern UnityWebRequest.UnityWebRequestError SetTimeoutMsec(int timeout);

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x06000075 RID: 117 RVA: 0x00002D08 File Offset: 0x00000F08
		// (set) Token: 0x06000076 RID: 118 RVA: 0x00002D2C File Offset: 0x00000F2C
		public int timeout
		{
			get
			{
				return this.GetTimeoutMsec() / 1000;
			}
			set
			{
				if (!this.isModifiable)
				{
					throw new InvalidOperationException("UnityWebRequest has already been sent; cannot modify the timeout");
				}
				value = Math.Max(value, 0);
				UnityWebRequest.UnityWebRequestError unityWebRequestError = this.SetTimeoutMsec(value * 1000);
				if (unityWebRequestError != UnityWebRequest.UnityWebRequestError.OK)
				{
					throw new InvalidOperationException(UnityWebRequest.GetWebErrorString(unityWebRequestError));
				}
			}
		}

		// Token: 0x06000077 RID: 119 RVA: 0x00002D7C File Offset: 0x00000F7C
		public static UnityWebRequest Get(string uri)
		{
			return new UnityWebRequest(uri, "GET", new DownloadHandlerBuffer(), null);
		}

		// Token: 0x06000078 RID: 120 RVA: 0x00002DA4 File Offset: 0x00000FA4
		public static UnityWebRequest Get(Uri uri)
		{
			return new UnityWebRequest(uri, "GET", new DownloadHandlerBuffer(), null);
		}

		// Token: 0x06000079 RID: 121 RVA: 0x00002DCC File Offset: 0x00000FCC
		public static UnityWebRequest Delete(string uri)
		{
			return new UnityWebRequest(uri, "DELETE");
		}

		// Token: 0x0600007A RID: 122 RVA: 0x00002DF0 File Offset: 0x00000FF0
		public static UnityWebRequest Delete(Uri uri)
		{
			return new UnityWebRequest(uri, "DELETE");
		}

		// Token: 0x0600007B RID: 123 RVA: 0x00002E14 File Offset: 0x00001014
		public static UnityWebRequest Head(string uri)
		{
			return new UnityWebRequest(uri, "HEAD");
		}

		// Token: 0x0600007C RID: 124 RVA: 0x00002E38 File Offset: 0x00001038
		public static UnityWebRequest Head(Uri uri)
		{
			return new UnityWebRequest(uri, "HEAD");
		}

		// Token: 0x0600007D RID: 125 RVA: 0x00002E5A File Offset: 0x0000105A
		[Obsolete("UnityWebRequest.GetTexture is obsolete. Use UnityWebRequestTexture.GetTexture instead (UnityUpgradable) -> [UnityEngine] UnityWebRequestTexture.GetTexture(*)", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static UnityWebRequest GetTexture(string uri)
		{
			throw new NotSupportedException("UnityWebRequest.GetTexture is obsolete. Use UnityWebRequestTexture.GetTexture instead.");
		}

		// Token: 0x0600007E RID: 126 RVA: 0x00002E5A File Offset: 0x0000105A
		[Obsolete("UnityWebRequest.GetTexture is obsolete. Use UnityWebRequestTexture.GetTexture instead (UnityUpgradable) -> [UnityEngine] UnityWebRequestTexture.GetTexture(*)", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static UnityWebRequest GetTexture(string uri, bool nonReadable)
		{
			throw new NotSupportedException("UnityWebRequest.GetTexture is obsolete. Use UnityWebRequestTexture.GetTexture instead.");
		}

		// Token: 0x0600007F RID: 127 RVA: 0x00002E68 File Offset: 0x00001068
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("UnityWebRequest.GetAudioClip is obsolete. Use UnityWebRequestMultimedia.GetAudioClip instead (UnityUpgradable) -> [UnityEngine] UnityWebRequestMultimedia.GetAudioClip(*)", true)]
		public static UnityWebRequest GetAudioClip(string uri, AudioType audioType)
		{
			return null;
		}

		// Token: 0x06000080 RID: 128 RVA: 0x00002E80 File Offset: 0x00001080
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("UnityWebRequest.GetAssetBundle is obsolete. Use UnityWebRequestAssetBundle.GetAssetBundle instead (UnityUpgradable) -> [UnityEngine] UnityWebRequestAssetBundle.GetAssetBundle(*)", true)]
		public static UnityWebRequest GetAssetBundle(string uri)
		{
			return null;
		}

		// Token: 0x06000081 RID: 129 RVA: 0x00002E98 File Offset: 0x00001098
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("UnityWebRequest.GetAssetBundle is obsolete. Use UnityWebRequestAssetBundle.GetAssetBundle instead (UnityUpgradable) -> [UnityEngine] UnityWebRequestAssetBundle.GetAssetBundle(*)", true)]
		public static UnityWebRequest GetAssetBundle(string uri, uint crc)
		{
			return null;
		}

		// Token: 0x06000082 RID: 130 RVA: 0x00002EB0 File Offset: 0x000010B0
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("UnityWebRequest.GetAssetBundle is obsolete. Use UnityWebRequestAssetBundle.GetAssetBundle instead (UnityUpgradable) -> [UnityEngine] UnityWebRequestAssetBundle.GetAssetBundle(*)", true)]
		public static UnityWebRequest GetAssetBundle(string uri, uint version, uint crc)
		{
			return null;
		}

		// Token: 0x06000083 RID: 131 RVA: 0x00002EC8 File Offset: 0x000010C8
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("UnityWebRequest.GetAssetBundle is obsolete. Use UnityWebRequestAssetBundle.GetAssetBundle instead (UnityUpgradable) -> [UnityEngine] UnityWebRequestAssetBundle.GetAssetBundle(*)", true)]
		public static UnityWebRequest GetAssetBundle(string uri, Hash128 hash, uint crc)
		{
			return null;
		}

		// Token: 0x06000084 RID: 132 RVA: 0x00002EE0 File Offset: 0x000010E0
		[Obsolete("UnityWebRequest.GetAssetBundle is obsolete. Use UnityWebRequestAssetBundle.GetAssetBundle instead (UnityUpgradable) -> [UnityEngine] UnityWebRequestAssetBundle.GetAssetBundle(*)", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static UnityWebRequest GetAssetBundle(string uri, CachedAssetBundle cachedAssetBundle, uint crc)
		{
			return null;
		}

		// Token: 0x06000085 RID: 133 RVA: 0x00002EF8 File Offset: 0x000010F8
		public static UnityWebRequest Put(string uri, byte[] bodyData)
		{
			return new UnityWebRequest(uri, "PUT", new DownloadHandlerBuffer(), new UploadHandlerRaw(bodyData));
		}

		// Token: 0x06000086 RID: 134 RVA: 0x00002F28 File Offset: 0x00001128
		public static UnityWebRequest Put(Uri uri, byte[] bodyData)
		{
			return new UnityWebRequest(uri, "PUT", new DownloadHandlerBuffer(), new UploadHandlerRaw(bodyData));
		}

		// Token: 0x06000087 RID: 135 RVA: 0x00002F58 File Offset: 0x00001158
		public static UnityWebRequest Put(string uri, string bodyData)
		{
			return new UnityWebRequest(uri, "PUT", new DownloadHandlerBuffer(), new UploadHandlerRaw(Encoding.UTF8.GetBytes(bodyData)));
		}

		// Token: 0x06000088 RID: 136 RVA: 0x00002F90 File Offset: 0x00001190
		public static UnityWebRequest Put(Uri uri, string bodyData)
		{
			return new UnityWebRequest(uri, "PUT", new DownloadHandlerBuffer(), new UploadHandlerRaw(Encoding.UTF8.GetBytes(bodyData)));
		}

		// Token: 0x06000089 RID: 137 RVA: 0x00002FC8 File Offset: 0x000011C8
		public static UnityWebRequest Post(string uri, string postData)
		{
			UnityWebRequest unityWebRequest = new UnityWebRequest(uri, "POST");
			UnityWebRequest.SetupPost(unityWebRequest, postData);
			return unityWebRequest;
		}

		// Token: 0x0600008A RID: 138 RVA: 0x00002FF4 File Offset: 0x000011F4
		public static UnityWebRequest Post(Uri uri, string postData)
		{
			UnityWebRequest unityWebRequest = new UnityWebRequest(uri, "POST");
			UnityWebRequest.SetupPost(unityWebRequest, postData);
			return unityWebRequest;
		}

		// Token: 0x0600008B RID: 139 RVA: 0x00003020 File Offset: 0x00001220
		private static void SetupPost(UnityWebRequest request, string postData)
		{
			byte[] data = null;
			if (!string.IsNullOrEmpty(postData))
			{
				string s = WWWTranscoder.DataEncode(postData, Encoding.UTF8);
				data = Encoding.UTF8.GetBytes(s);
			}
			request.uploadHandler = new UploadHandlerRaw(data);
			request.uploadHandler.contentType = "application/x-www-form-urlencoded";
			request.downloadHandler = new DownloadHandlerBuffer();
		}

		// Token: 0x0600008C RID: 140 RVA: 0x0000307C File Offset: 0x0000127C
		public static UnityWebRequest Post(string uri, WWWForm formData)
		{
			UnityWebRequest unityWebRequest = new UnityWebRequest(uri, "POST");
			UnityWebRequest.SetupPost(unityWebRequest, formData);
			return unityWebRequest;
		}

		// Token: 0x0600008D RID: 141 RVA: 0x000030A8 File Offset: 0x000012A8
		public static UnityWebRequest Post(Uri uri, WWWForm formData)
		{
			UnityWebRequest unityWebRequest = new UnityWebRequest(uri, "POST");
			UnityWebRequest.SetupPost(unityWebRequest, formData);
			return unityWebRequest;
		}

		// Token: 0x0600008E RID: 142 RVA: 0x000030D4 File Offset: 0x000012D4
		private static void SetupPost(UnityWebRequest request, WWWForm formData)
		{
			byte[] array = null;
			if (formData != null)
			{
				array = formData.data;
				if (array.Length == 0)
				{
					array = null;
				}
			}
			request.uploadHandler = new UploadHandlerRaw(array);
			request.downloadHandler = new DownloadHandlerBuffer();
			if (formData != null)
			{
				Dictionary<string, string> headers = formData.headers;
				foreach (KeyValuePair<string, string> keyValuePair in headers)
				{
					request.SetRequestHeader(keyValuePair.Key, keyValuePair.Value);
				}
			}
		}

		// Token: 0x0600008F RID: 143 RVA: 0x0000317C File Offset: 0x0000137C
		public static UnityWebRequest Post(string uri, List<IMultipartFormSection> multipartFormSections)
		{
			byte[] boundary = UnityWebRequest.GenerateBoundary();
			return UnityWebRequest.Post(uri, multipartFormSections, boundary);
		}

		// Token: 0x06000090 RID: 144 RVA: 0x000031A0 File Offset: 0x000013A0
		public static UnityWebRequest Post(Uri uri, List<IMultipartFormSection> multipartFormSections)
		{
			byte[] boundary = UnityWebRequest.GenerateBoundary();
			return UnityWebRequest.Post(uri, multipartFormSections, boundary);
		}

		// Token: 0x06000091 RID: 145 RVA: 0x000031C4 File Offset: 0x000013C4
		public static UnityWebRequest Post(string uri, List<IMultipartFormSection> multipartFormSections, byte[] boundary)
		{
			UnityWebRequest unityWebRequest = new UnityWebRequest(uri, "POST");
			UnityWebRequest.SetupPost(unityWebRequest, multipartFormSections, boundary);
			return unityWebRequest;
		}

		// Token: 0x06000092 RID: 146 RVA: 0x000031F0 File Offset: 0x000013F0
		public static UnityWebRequest Post(Uri uri, List<IMultipartFormSection> multipartFormSections, byte[] boundary)
		{
			UnityWebRequest unityWebRequest = new UnityWebRequest(uri, "POST");
			UnityWebRequest.SetupPost(unityWebRequest, multipartFormSections, boundary);
			return unityWebRequest;
		}

		// Token: 0x06000093 RID: 147 RVA: 0x0000321C File Offset: 0x0000141C
		private static void SetupPost(UnityWebRequest request, List<IMultipartFormSection> multipartFormSections, byte[] boundary)
		{
			byte[] data = null;
			if (multipartFormSections != null && multipartFormSections.Count != 0)
			{
				data = UnityWebRequest.SerializeFormSections(multipartFormSections, boundary);
			}
			request.uploadHandler = new UploadHandlerRaw(data)
			{
				contentType = "multipart/form-data; boundary=" + Encoding.UTF8.GetString(boundary, 0, boundary.Length)
			};
			request.downloadHandler = new DownloadHandlerBuffer();
		}

		// Token: 0x06000094 RID: 148 RVA: 0x00003280 File Offset: 0x00001480
		public static UnityWebRequest Post(string uri, Dictionary<string, string> formFields)
		{
			UnityWebRequest unityWebRequest = new UnityWebRequest(uri, "POST");
			UnityWebRequest.SetupPost(unityWebRequest, formFields);
			return unityWebRequest;
		}

		// Token: 0x06000095 RID: 149 RVA: 0x000032AC File Offset: 0x000014AC
		public static UnityWebRequest Post(Uri uri, Dictionary<string, string> formFields)
		{
			UnityWebRequest unityWebRequest = new UnityWebRequest(uri, "POST");
			UnityWebRequest.SetupPost(unityWebRequest, formFields);
			return unityWebRequest;
		}

		// Token: 0x06000096 RID: 150 RVA: 0x000032D8 File Offset: 0x000014D8
		private static void SetupPost(UnityWebRequest request, Dictionary<string, string> formFields)
		{
			byte[] data = null;
			if (formFields != null && formFields.Count != 0)
			{
				data = UnityWebRequest.SerializeSimpleForm(formFields);
			}
			request.uploadHandler = new UploadHandlerRaw(data)
			{
				contentType = "application/x-www-form-urlencoded"
			};
			request.downloadHandler = new DownloadHandlerBuffer();
		}

		// Token: 0x06000097 RID: 151 RVA: 0x00003324 File Offset: 0x00001524
		public static string EscapeURL(string s)
		{
			return UnityWebRequest.EscapeURL(s, Encoding.UTF8);
		}

		// Token: 0x06000098 RID: 152 RVA: 0x00003344 File Offset: 0x00001544
		public static string EscapeURL(string s, Encoding e)
		{
			string result;
			if (s == null)
			{
				result = null;
			}
			else if (s == "")
			{
				result = "";
			}
			else if (e == null)
			{
				result = null;
			}
			else
			{
				byte[] bytes = Encoding.UTF8.GetBytes(s);
				byte[] bytes2 = WWWTranscoder.URLEncode(bytes);
				result = e.GetString(bytes2);
			}
			return result;
		}

		// Token: 0x06000099 RID: 153 RVA: 0x000033A8 File Offset: 0x000015A8
		public static string UnEscapeURL(string s)
		{
			return UnityWebRequest.UnEscapeURL(s, Encoding.UTF8);
		}

		// Token: 0x0600009A RID: 154 RVA: 0x000033C8 File Offset: 0x000015C8
		public static string UnEscapeURL(string s, Encoding e)
		{
			string result;
			if (s == null)
			{
				result = null;
			}
			else if (s.IndexOf('%') == -1 && s.IndexOf('+') == -1)
			{
				result = s;
			}
			else
			{
				byte[] bytes = Encoding.UTF8.GetBytes(s);
				byte[] bytes2 = WWWTranscoder.URLDecode(bytes);
				result = e.GetString(bytes2);
			}
			return result;
		}

		// Token: 0x0600009B RID: 155 RVA: 0x00003428 File Offset: 0x00001628
		public static byte[] SerializeFormSections(List<IMultipartFormSection> multipartFormSections, byte[] boundary)
		{
			byte[] result;
			if (multipartFormSections == null || multipartFormSections.Count == 0)
			{
				result = null;
			}
			else
			{
				byte[] bytes = Encoding.UTF8.GetBytes("\r\n");
				byte[] bytes2 = WWWForm.DefaultEncoding.GetBytes("--");
				int num = 0;
				foreach (IMultipartFormSection multipartFormSection in multipartFormSections)
				{
					num += 64 + multipartFormSection.sectionData.Length;
				}
				List<byte> list = new List<byte>(num);
				foreach (IMultipartFormSection multipartFormSection2 in multipartFormSections)
				{
					string str = "form-data";
					string sectionName = multipartFormSection2.sectionName;
					string fileName = multipartFormSection2.fileName;
					string text = "Content-Disposition: " + str;
					if (!string.IsNullOrEmpty(sectionName))
					{
						text = text + "; name=\"" + sectionName + "\"";
					}
					if (!string.IsNullOrEmpty(fileName))
					{
						text = text + "; filename=\"" + fileName + "\"";
					}
					text += "\r\n";
					string contentType = multipartFormSection2.contentType;
					if (!string.IsNullOrEmpty(contentType))
					{
						text = text + "Content-Type: " + contentType + "\r\n";
					}
					list.AddRange(bytes);
					list.AddRange(bytes2);
					list.AddRange(boundary);
					list.AddRange(bytes);
					list.AddRange(Encoding.UTF8.GetBytes(text));
					list.AddRange(bytes);
					list.AddRange(multipartFormSection2.sectionData);
				}
				list.AddRange(bytes);
				list.AddRange(bytes2);
				list.AddRange(boundary);
				list.AddRange(bytes2);
				list.AddRange(bytes);
				result = list.ToArray();
			}
			return result;
		}

		// Token: 0x0600009C RID: 156 RVA: 0x00003654 File Offset: 0x00001854
		public static byte[] GenerateBoundary()
		{
			byte[] array = new byte[40];
			for (int i = 0; i < 40; i++)
			{
				int num = Random.Range(48, 110);
				if (num > 57)
				{
					num += 7;
				}
				if (num > 90)
				{
					num += 6;
				}
				array[i] = (byte)num;
			}
			return array;
		}

		// Token: 0x0600009D RID: 157 RVA: 0x000036B0 File Offset: 0x000018B0
		public static byte[] SerializeSimpleForm(Dictionary<string, string> formFields)
		{
			string text = "";
			foreach (KeyValuePair<string, string> keyValuePair in formFields)
			{
				if (text.Length > 0)
				{
					text += "&";
				}
				text = text + WWWTranscoder.DataEncode(keyValuePair.Key) + "=" + WWWTranscoder.DataEncode(keyValuePair.Value);
			}
			return Encoding.UTF8.GetBytes(text);
		}

		// Token: 0x04000009 RID: 9
		[NonSerialized]
		internal IntPtr m_Ptr;

		// Token: 0x0400000A RID: 10
		[NonSerialized]
		internal DownloadHandler m_DownloadHandler;

		// Token: 0x0400000B RID: 11
		[NonSerialized]
		internal UploadHandler m_UploadHandler;

		// Token: 0x0400000C RID: 12
		[NonSerialized]
		internal CertificateHandler m_CertificateHandler;

		// Token: 0x0400000D RID: 13
		[NonSerialized]
		internal Uri m_Uri;

		// Token: 0x0400000E RID: 14
		public const string kHttpVerbGET = "GET";

		// Token: 0x0400000F RID: 15
		public const string kHttpVerbHEAD = "HEAD";

		// Token: 0x04000010 RID: 16
		public const string kHttpVerbPOST = "POST";

		// Token: 0x04000011 RID: 17
		public const string kHttpVerbPUT = "PUT";

		// Token: 0x04000012 RID: 18
		public const string kHttpVerbCREATE = "CREATE";

		// Token: 0x04000013 RID: 19
		public const string kHttpVerbDELETE = "DELETE";

		// Token: 0x04000014 RID: 20
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <disposeCertificateHandlerOnDispose>k__BackingField;

		// Token: 0x04000015 RID: 21
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private bool <disposeDownloadHandlerOnDispose>k__BackingField;

		// Token: 0x04000016 RID: 22
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private bool <disposeUploadHandlerOnDispose>k__BackingField;

		// Token: 0x02000007 RID: 7
		internal enum UnityWebRequestMethod
		{
			// Token: 0x04000018 RID: 24
			Get,
			// Token: 0x04000019 RID: 25
			Post,
			// Token: 0x0400001A RID: 26
			Put,
			// Token: 0x0400001B RID: 27
			Head,
			// Token: 0x0400001C RID: 28
			Custom
		}

		// Token: 0x02000008 RID: 8
		internal enum UnityWebRequestError
		{
			// Token: 0x0400001E RID: 30
			OK,
			// Token: 0x0400001F RID: 31
			Unknown,
			// Token: 0x04000020 RID: 32
			SDKError,
			// Token: 0x04000021 RID: 33
			UnsupportedProtocol,
			// Token: 0x04000022 RID: 34
			MalformattedUrl,
			// Token: 0x04000023 RID: 35
			CannotResolveProxy,
			// Token: 0x04000024 RID: 36
			CannotResolveHost,
			// Token: 0x04000025 RID: 37
			CannotConnectToHost,
			// Token: 0x04000026 RID: 38
			AccessDenied,
			// Token: 0x04000027 RID: 39
			GenericHttpError,
			// Token: 0x04000028 RID: 40
			WriteError,
			// Token: 0x04000029 RID: 41
			ReadError,
			// Token: 0x0400002A RID: 42
			OutOfMemory,
			// Token: 0x0400002B RID: 43
			Timeout,
			// Token: 0x0400002C RID: 44
			HTTPPostError,
			// Token: 0x0400002D RID: 45
			SSLCannotConnect,
			// Token: 0x0400002E RID: 46
			Aborted,
			// Token: 0x0400002F RID: 47
			TooManyRedirects,
			// Token: 0x04000030 RID: 48
			ReceivedNoData,
			// Token: 0x04000031 RID: 49
			SSLNotSupported,
			// Token: 0x04000032 RID: 50
			FailedToSendData,
			// Token: 0x04000033 RID: 51
			FailedToReceiveData,
			// Token: 0x04000034 RID: 52
			SSLCertificateError,
			// Token: 0x04000035 RID: 53
			SSLCipherNotAvailable,
			// Token: 0x04000036 RID: 54
			SSLCACertError,
			// Token: 0x04000037 RID: 55
			UnrecognizedContentEncoding,
			// Token: 0x04000038 RID: 56
			LoginFailed,
			// Token: 0x04000039 RID: 57
			SSLShutdownFailed,
			// Token: 0x0400003A RID: 58
			NoInternetConnection
		}
	}
}
