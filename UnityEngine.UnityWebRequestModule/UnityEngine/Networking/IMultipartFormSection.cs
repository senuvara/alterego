﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000002 RID: 2
	public interface IMultipartFormSection
	{
		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000001 RID: 1
		string sectionName { get; }

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000002 RID: 2
		byte[] sectionData { get; }

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000003 RID: 3
		string fileName { get; }

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000004 RID: 4
		string contentType { get; }
	}
}
