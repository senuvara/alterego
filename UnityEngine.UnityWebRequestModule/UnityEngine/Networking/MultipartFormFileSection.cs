﻿using System;
using System.Text;

namespace UnityEngine.Networking
{
	// Token: 0x02000004 RID: 4
	public class MultipartFormFileSection : IMultipartFormSection
	{
		// Token: 0x06000010 RID: 16 RVA: 0x000021BC File Offset: 0x000003BC
		public MultipartFormFileSection(string name, byte[] data, string fileName, string contentType)
		{
			if (data == null || data.Length < 1)
			{
				throw new ArgumentException("Cannot create a multipart form file section without body data");
			}
			if (string.IsNullOrEmpty(fileName))
			{
				fileName = "file.dat";
			}
			if (string.IsNullOrEmpty(contentType))
			{
				contentType = "application/octet-stream";
			}
			this.Init(name, data, fileName, contentType);
		}

		// Token: 0x06000011 RID: 17 RVA: 0x0000221F File Offset: 0x0000041F
		public MultipartFormFileSection(byte[] data) : this(null, data, null, null)
		{
		}

		// Token: 0x06000012 RID: 18 RVA: 0x0000222C File Offset: 0x0000042C
		public MultipartFormFileSection(string fileName, byte[] data) : this(null, data, fileName, null)
		{
		}

		// Token: 0x06000013 RID: 19 RVA: 0x0000223C File Offset: 0x0000043C
		public MultipartFormFileSection(string name, string data, Encoding dataEncoding, string fileName)
		{
			if (data == null || data.Length < 1)
			{
				throw new ArgumentException("Cannot create a multipart form file section without body data");
			}
			if (dataEncoding == null)
			{
				dataEncoding = Encoding.UTF8;
			}
			byte[] bytes = dataEncoding.GetBytes(data);
			if (string.IsNullOrEmpty(fileName))
			{
				fileName = "file.txt";
			}
			if (string.IsNullOrEmpty(this.content))
			{
				this.content = "text/plain; charset=" + dataEncoding.WebName;
			}
			this.Init(name, bytes, fileName, this.content);
		}

		// Token: 0x06000014 RID: 20 RVA: 0x000022D2 File Offset: 0x000004D2
		public MultipartFormFileSection(string data, Encoding dataEncoding, string fileName) : this(null, data, dataEncoding, fileName)
		{
		}

		// Token: 0x06000015 RID: 21 RVA: 0x000022DF File Offset: 0x000004DF
		public MultipartFormFileSection(string data, string fileName) : this(data, null, fileName)
		{
		}

		// Token: 0x06000016 RID: 22 RVA: 0x000022EB File Offset: 0x000004EB
		private void Init(string name, byte[] data, string fileName, string contentType)
		{
			this.name = name;
			this.data = data;
			this.file = fileName;
			this.content = contentType;
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000017 RID: 23 RVA: 0x0000230C File Offset: 0x0000050C
		public string sectionName
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000018 RID: 24 RVA: 0x00002328 File Offset: 0x00000528
		public byte[] sectionData
		{
			get
			{
				return this.data;
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000019 RID: 25 RVA: 0x00002344 File Offset: 0x00000544
		public string fileName
		{
			get
			{
				return this.file;
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x0600001A RID: 26 RVA: 0x00002360 File Offset: 0x00000560
		public string contentType
		{
			get
			{
				return this.content;
			}
		}

		// Token: 0x04000004 RID: 4
		private string name;

		// Token: 0x04000005 RID: 5
		private byte[] data;

		// Token: 0x04000006 RID: 6
		private string file;

		// Token: 0x04000007 RID: 7
		private string content;
	}
}
