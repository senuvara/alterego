﻿using System;
using System.IO;
using System.Text;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200000A RID: 10
	[VisibleToOtherModules(new string[]
	{
		"UnityEngine.UnityWebRequestWWWModule"
	})]
	internal class WWWTranscoder
	{
		// Token: 0x060000A8 RID: 168 RVA: 0x00003E48 File Offset: 0x00002048
		public WWWTranscoder()
		{
		}

		// Token: 0x060000A9 RID: 169 RVA: 0x00003E50 File Offset: 0x00002050
		private static byte Hex2Byte(byte[] b, int offset)
		{
			byte b2 = 0;
			for (int i = offset; i < offset + 2; i++)
			{
				b2 *= 16;
				int num = (int)b[i];
				if (num >= 48 && num <= 57)
				{
					num -= 48;
				}
				else if (num >= 65 && num <= 75)
				{
					num -= 55;
				}
				else if (num >= 97 && num <= 102)
				{
					num -= 87;
				}
				if (num > 15)
				{
					return 63;
				}
				b2 += (byte)num;
			}
			return b2;
		}

		// Token: 0x060000AA RID: 170 RVA: 0x00003EE8 File Offset: 0x000020E8
		private static byte[] Byte2Hex(byte b, byte[] hexChars)
		{
			return new byte[]
			{
				hexChars[b >> 4],
				hexChars[(int)(b & 15)]
			};
		}

		// Token: 0x060000AB RID: 171 RVA: 0x00003F18 File Offset: 0x00002118
		public static string URLEncode(string toEncode)
		{
			return WWWTranscoder.URLEncode(toEncode, Encoding.UTF8);
		}

		// Token: 0x060000AC RID: 172 RVA: 0x00003F38 File Offset: 0x00002138
		public static string URLEncode(string toEncode, Encoding e)
		{
			byte[] array = WWWTranscoder.Encode(e.GetBytes(toEncode), WWWTranscoder.urlEscapeChar, WWWTranscoder.urlSpace, WWWTranscoder.urlForbidden, false);
			return WWWForm.DefaultEncoding.GetString(array, 0, array.Length);
		}

		// Token: 0x060000AD RID: 173 RVA: 0x00003F7C File Offset: 0x0000217C
		public static byte[] URLEncode(byte[] toEncode)
		{
			return WWWTranscoder.Encode(toEncode, WWWTranscoder.urlEscapeChar, WWWTranscoder.urlSpace, WWWTranscoder.urlForbidden, false);
		}

		// Token: 0x060000AE RID: 174 RVA: 0x00003FA8 File Offset: 0x000021A8
		public static string DataEncode(string toEncode)
		{
			return WWWTranscoder.DataEncode(toEncode, Encoding.UTF8);
		}

		// Token: 0x060000AF RID: 175 RVA: 0x00003FC8 File Offset: 0x000021C8
		public static string DataEncode(string toEncode, Encoding e)
		{
			byte[] array = WWWTranscoder.Encode(e.GetBytes(toEncode), WWWTranscoder.urlEscapeChar, WWWTranscoder.dataSpace, WWWTranscoder.urlForbidden, false);
			return WWWForm.DefaultEncoding.GetString(array, 0, array.Length);
		}

		// Token: 0x060000B0 RID: 176 RVA: 0x0000400C File Offset: 0x0000220C
		public static byte[] DataEncode(byte[] toEncode)
		{
			return WWWTranscoder.Encode(toEncode, WWWTranscoder.urlEscapeChar, WWWTranscoder.dataSpace, WWWTranscoder.urlForbidden, false);
		}

		// Token: 0x060000B1 RID: 177 RVA: 0x00004038 File Offset: 0x00002238
		public static string QPEncode(string toEncode)
		{
			return WWWTranscoder.QPEncode(toEncode, Encoding.UTF8);
		}

		// Token: 0x060000B2 RID: 178 RVA: 0x00004058 File Offset: 0x00002258
		public static string QPEncode(string toEncode, Encoding e)
		{
			byte[] array = WWWTranscoder.Encode(e.GetBytes(toEncode), WWWTranscoder.qpEscapeChar, WWWTranscoder.qpSpace, WWWTranscoder.qpForbidden, true);
			return WWWForm.DefaultEncoding.GetString(array, 0, array.Length);
		}

		// Token: 0x060000B3 RID: 179 RVA: 0x0000409C File Offset: 0x0000229C
		public static byte[] QPEncode(byte[] toEncode)
		{
			return WWWTranscoder.Encode(toEncode, WWWTranscoder.qpEscapeChar, WWWTranscoder.qpSpace, WWWTranscoder.qpForbidden, true);
		}

		// Token: 0x060000B4 RID: 180 RVA: 0x000040C8 File Offset: 0x000022C8
		public static byte[] Encode(byte[] input, byte escapeChar, byte[] space, byte[] forbidden, bool uppercase)
		{
			byte[] result;
			using (MemoryStream memoryStream = new MemoryStream(input.Length * 2))
			{
				for (int i = 0; i < input.Length; i++)
				{
					if (input[i] == 32)
					{
						memoryStream.Write(space, 0, space.Length);
					}
					else if (input[i] < 32 || input[i] > 126 || WWWTranscoder.ByteArrayContains(forbidden, input[i]))
					{
						memoryStream.WriteByte(escapeChar);
						memoryStream.Write(WWWTranscoder.Byte2Hex(input[i], (!uppercase) ? WWWTranscoder.lcHexChars : WWWTranscoder.ucHexChars), 0, 2);
					}
					else
					{
						memoryStream.WriteByte(input[i]);
					}
				}
				result = memoryStream.ToArray();
			}
			return result;
		}

		// Token: 0x060000B5 RID: 181 RVA: 0x000041A0 File Offset: 0x000023A0
		private static bool ByteArrayContains(byte[] array, byte b)
		{
			int num = array.Length;
			for (int i = 0; i < num; i++)
			{
				if (array[i] == b)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060000B6 RID: 182 RVA: 0x000041E0 File Offset: 0x000023E0
		public static string URLDecode(string toEncode)
		{
			return WWWTranscoder.URLDecode(toEncode, Encoding.UTF8);
		}

		// Token: 0x060000B7 RID: 183 RVA: 0x00004200 File Offset: 0x00002400
		public static string URLDecode(string toEncode, Encoding e)
		{
			byte[] array = WWWTranscoder.Decode(WWWForm.DefaultEncoding.GetBytes(toEncode), WWWTranscoder.urlEscapeChar, WWWTranscoder.urlSpace);
			return e.GetString(array, 0, array.Length);
		}

		// Token: 0x060000B8 RID: 184 RVA: 0x0000423C File Offset: 0x0000243C
		public static byte[] URLDecode(byte[] toEncode)
		{
			return WWWTranscoder.Decode(toEncode, WWWTranscoder.urlEscapeChar, WWWTranscoder.urlSpace);
		}

		// Token: 0x060000B9 RID: 185 RVA: 0x00004264 File Offset: 0x00002464
		public static string DataDecode(string toDecode)
		{
			return WWWTranscoder.DataDecode(toDecode, Encoding.UTF8);
		}

		// Token: 0x060000BA RID: 186 RVA: 0x00004284 File Offset: 0x00002484
		public static string DataDecode(string toDecode, Encoding e)
		{
			byte[] array = WWWTranscoder.Decode(WWWForm.DefaultEncoding.GetBytes(toDecode), WWWTranscoder.urlEscapeChar, WWWTranscoder.dataSpace);
			return e.GetString(array, 0, array.Length);
		}

		// Token: 0x060000BB RID: 187 RVA: 0x000042C0 File Offset: 0x000024C0
		public static byte[] DataDecode(byte[] toDecode)
		{
			return WWWTranscoder.Decode(toDecode, WWWTranscoder.urlEscapeChar, WWWTranscoder.dataSpace);
		}

		// Token: 0x060000BC RID: 188 RVA: 0x000042E8 File Offset: 0x000024E8
		public static string QPDecode(string toEncode)
		{
			return WWWTranscoder.QPDecode(toEncode, Encoding.UTF8);
		}

		// Token: 0x060000BD RID: 189 RVA: 0x00004308 File Offset: 0x00002508
		public static string QPDecode(string toEncode, Encoding e)
		{
			byte[] array = WWWTranscoder.Decode(WWWForm.DefaultEncoding.GetBytes(toEncode), WWWTranscoder.qpEscapeChar, WWWTranscoder.qpSpace);
			return e.GetString(array, 0, array.Length);
		}

		// Token: 0x060000BE RID: 190 RVA: 0x00004344 File Offset: 0x00002544
		public static byte[] QPDecode(byte[] toEncode)
		{
			return WWWTranscoder.Decode(toEncode, WWWTranscoder.qpEscapeChar, WWWTranscoder.qpSpace);
		}

		// Token: 0x060000BF RID: 191 RVA: 0x0000436C File Offset: 0x0000256C
		private static bool ByteSubArrayEquals(byte[] array, int index, byte[] comperand)
		{
			bool result;
			if (array.Length - index < comperand.Length)
			{
				result = false;
			}
			else
			{
				for (int i = 0; i < comperand.Length; i++)
				{
					if (array[index + i] != comperand[i])
					{
						return false;
					}
				}
				result = true;
			}
			return result;
		}

		// Token: 0x060000C0 RID: 192 RVA: 0x000043C0 File Offset: 0x000025C0
		public static byte[] Decode(byte[] input, byte escapeChar, byte[] space)
		{
			byte[] result;
			using (MemoryStream memoryStream = new MemoryStream(input.Length))
			{
				for (int i = 0; i < input.Length; i++)
				{
					if (WWWTranscoder.ByteSubArrayEquals(input, i, space))
					{
						i += space.Length - 1;
						memoryStream.WriteByte(32);
					}
					else if (input[i] == escapeChar && i + 2 < input.Length)
					{
						i++;
						memoryStream.WriteByte(WWWTranscoder.Hex2Byte(input, i++));
					}
					else
					{
						memoryStream.WriteByte(input[i]);
					}
				}
				result = memoryStream.ToArray();
			}
			return result;
		}

		// Token: 0x060000C1 RID: 193 RVA: 0x00004478 File Offset: 0x00002678
		public static bool SevenBitClean(string s)
		{
			return WWWTranscoder.SevenBitClean(s, Encoding.UTF8);
		}

		// Token: 0x060000C2 RID: 194 RVA: 0x00004498 File Offset: 0x00002698
		public static bool SevenBitClean(string s, Encoding e)
		{
			return WWWTranscoder.SevenBitClean(e.GetBytes(s));
		}

		// Token: 0x060000C3 RID: 195 RVA: 0x000044BC File Offset: 0x000026BC
		public static bool SevenBitClean(byte[] input)
		{
			for (int i = 0; i < input.Length; i++)
			{
				if (input[i] < 32 || input[i] > 126)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x060000C4 RID: 196 RVA: 0x00004504 File Offset: 0x00002704
		// Note: this type is marked as 'beforefieldinit'.
		static WWWTranscoder()
		{
		}

		// Token: 0x04000041 RID: 65
		private static byte[] ucHexChars = WWWForm.DefaultEncoding.GetBytes("0123456789ABCDEF");

		// Token: 0x04000042 RID: 66
		private static byte[] lcHexChars = WWWForm.DefaultEncoding.GetBytes("0123456789abcdef");

		// Token: 0x04000043 RID: 67
		private static byte urlEscapeChar = 37;

		// Token: 0x04000044 RID: 68
		private static byte[] urlSpace = new byte[]
		{
			43
		};

		// Token: 0x04000045 RID: 69
		private static byte[] dataSpace = WWWForm.DefaultEncoding.GetBytes("%20");

		// Token: 0x04000046 RID: 70
		private static byte[] urlForbidden = WWWForm.DefaultEncoding.GetBytes("@&;:<>=?\"'/\\!#%+$,{}|^[]`");

		// Token: 0x04000047 RID: 71
		private static byte qpEscapeChar = 61;

		// Token: 0x04000048 RID: 72
		private static byte[] qpSpace = new byte[]
		{
			95
		};

		// Token: 0x04000049 RID: 73
		private static byte[] qpForbidden = WWWForm.DefaultEncoding.GetBytes("&;=?\"'%+_");
	}
}
