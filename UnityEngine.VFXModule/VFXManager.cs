﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.VFX
{
	// Token: 0x0200000B RID: 11
	[StaticAccessor("GetVFXManager()", StaticAccessorType.Dot)]
	[NativeHeader("Modules/VFX/Public/VFXManager.h")]
	[RequiredByNativeCode]
	public static class VFXManager
	{
		// Token: 0x06000065 RID: 101
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern VisualEffect[] GetComponents();

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000066 RID: 102
		// (set) Token: 0x06000067 RID: 103
		public static extern float fixedTimeStep { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000068 RID: 104
		// (set) Token: 0x06000069 RID: 105
		public static extern float maxDeltaTime { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x0600006A RID: 106
		internal static extern string renderPipeSettingsPath { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600006B RID: 107
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void ProcessCamera(Camera cam);

		// Token: 0x0600006C RID: 108 RVA: 0x00002715 File Offset: 0x00000915
		[RequiredByNativeCode]
		internal static void RegisterPerCameraCallback()
		{
			if (VFXManager.<>f__mg$cache0 == null)
			{
				VFXManager.<>f__mg$cache0 = new Action<Camera>(VFXManager.ProcessCamera);
			}
			RenderPipeline.beginCameraRendering += VFXManager.<>f__mg$cache0;
		}

		// Token: 0x0600006D RID: 109 RVA: 0x0000273A File Offset: 0x0000093A
		[RequiredByNativeCode]
		internal static void UnregisterPerCameraCallback()
		{
			if (VFXManager.<>f__mg$cache1 == null)
			{
				VFXManager.<>f__mg$cache1 = new Action<Camera>(VFXManager.ProcessCamera);
			}
			RenderPipeline.beginCameraRendering -= VFXManager.<>f__mg$cache1;
		}

		// Token: 0x04000085 RID: 133
		[CompilerGenerated]
		private static Action<Camera> <>f__mg$cache0;

		// Token: 0x04000086 RID: 134
		[CompilerGenerated]
		private static Action<Camera> <>f__mg$cache1;
	}
}
