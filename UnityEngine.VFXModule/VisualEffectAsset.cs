﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.VFX
{
	// Token: 0x0200000E RID: 14
	[NativeHeader("Modules/VFX/Public/VisualEffectAsset.h")]
	[UsedByNativeCode]
	[NativeHeader("Modules/VFX/Public/ScriptBindings/VisualEffectAssetBindings.h")]
	[NativeHeader("VFXScriptingClasses.h")]
	public class VisualEffectAsset : Object
	{
		// Token: 0x06000083 RID: 131 RVA: 0x0000283F File Offset: 0x00000A3F
		public VisualEffectAsset()
		{
		}
	}
}
