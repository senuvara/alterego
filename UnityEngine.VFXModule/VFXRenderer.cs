﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine.Experimental.VFX
{
	// Token: 0x02000010 RID: 16
	[NativeType(Header = "Modules/VFX/Public/VFXRenderer.h")]
	internal sealed class VFXRenderer : Renderer
	{
		// Token: 0x060000F1 RID: 241 RVA: 0x00002DE5 File Offset: 0x00000FE5
		public VFXRenderer()
		{
		}
	}
}
