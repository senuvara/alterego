﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Rendering;

namespace UnityEngine.Experimental.VFX
{
	// Token: 0x0200000F RID: 15
	[RequireComponent(typeof(Transform))]
	[NativeHeader("Modules/VFX/Public/VisualEffect.h")]
	public class VisualEffect : Behaviour
	{
		// Token: 0x06000084 RID: 132 RVA: 0x00002847 File Offset: 0x00000A47
		public VisualEffect()
		{
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000085 RID: 133
		// (set) Token: 0x06000086 RID: 134
		public extern bool pause { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000087 RID: 135
		// (set) Token: 0x06000088 RID: 136
		public extern float playRate { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000089 RID: 137
		// (set) Token: 0x0600008A RID: 138
		public extern uint startSeed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x0600008B RID: 139
		// (set) Token: 0x0600008C RID: 140
		public extern bool resetSeedOnPlay { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600008D RID: 141
		public extern bool culled { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x0600008E RID: 142
		// (set) Token: 0x0600008F RID: 143
		public extern VisualEffectAsset visualEffectAsset { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000090 RID: 144 RVA: 0x00002850 File Offset: 0x00000A50
		public VFXEventAttribute CreateVFXEventAttribute()
		{
			VFXEventAttribute result;
			if (this.visualEffectAsset == null)
			{
				result = null;
			}
			else
			{
				VFXEventAttribute vfxeventAttribute = VFXEventAttribute.Internal_InstanciateVFXEventAttribute(this.visualEffectAsset);
				result = vfxeventAttribute;
			}
			return result;
		}

		// Token: 0x06000091 RID: 145
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Play(VFXEventAttribute eventAttribute);

		// Token: 0x06000092 RID: 146 RVA: 0x0000288A File Offset: 0x00000A8A
		public void Play()
		{
			this.Play(null);
		}

		// Token: 0x06000093 RID: 147
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Stop(VFXEventAttribute eventAttribute);

		// Token: 0x06000094 RID: 148 RVA: 0x00002894 File Offset: 0x00000A94
		public void Stop()
		{
			this.Stop(null);
		}

		// Token: 0x06000095 RID: 149
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SendEvent(int eventNameID, VFXEventAttribute eventAttribute);

		// Token: 0x06000096 RID: 150 RVA: 0x0000289E File Offset: 0x00000A9E
		public void SendEvent(string eventName)
		{
			this.SendEvent(Shader.PropertyToID(eventName), null);
		}

		// Token: 0x06000097 RID: 151 RVA: 0x000028AE File Offset: 0x00000AAE
		public void SendEvent(string eventName, VFXEventAttribute eventAttribute)
		{
			this.SendEvent(Shader.PropertyToID(eventName), eventAttribute);
		}

		// Token: 0x06000098 RID: 152
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Reinit();

		// Token: 0x06000099 RID: 153
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void AdvanceOneFrame();

		// Token: 0x0600009A RID: 154
		[NativeName("ResetOverrideFromScript")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ResetOverride(int nameID);

		// Token: 0x0600009B RID: 155
		[NativeName("GetTextureDimensionFromScript")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern TextureDimension GetTextureDimension(int nameID);

		// Token: 0x0600009C RID: 156
		[NativeName("HasValueFromScript<bool>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasBool(int nameID);

		// Token: 0x0600009D RID: 157
		[NativeName("HasValueFromScript<int>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasInt(int nameID);

		// Token: 0x0600009E RID: 158
		[NativeName("HasValueFromScript<UInt32>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasUInt(int nameID);

		// Token: 0x0600009F RID: 159
		[NativeName("HasValueFromScript<float>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasFloat(int nameID);

		// Token: 0x060000A0 RID: 160
		[NativeName("HasValueFromScript<Vector2f>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasVector2(int nameID);

		// Token: 0x060000A1 RID: 161
		[NativeName("HasValueFromScript<Vector3f>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasVector3(int nameID);

		// Token: 0x060000A2 RID: 162
		[NativeName("HasValueFromScript<Vector4f>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasVector4(int nameID);

		// Token: 0x060000A3 RID: 163
		[NativeName("HasValueFromScript<Matrix4x4f>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasMatrix4x4(int nameID);

		// Token: 0x060000A4 RID: 164
		[NativeName("HasValueFromScript<Texture*>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasTexture(int nameID);

		// Token: 0x060000A5 RID: 165
		[NativeName("HasValueFromScript<AnimationCurve*>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasAnimationCurve(int nameID);

		// Token: 0x060000A6 RID: 166
		[NativeName("HasValueFromScript<Gradient*>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasGradient(int nameID);

		// Token: 0x060000A7 RID: 167
		[NativeName("HasValueFromScript<Mesh*>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasMesh(int nameID);

		// Token: 0x060000A8 RID: 168
		[NativeName("SetValueFromScript<bool>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetBool(int nameID, bool b);

		// Token: 0x060000A9 RID: 169
		[NativeName("SetValueFromScript<int>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetInt(int nameID, int i);

		// Token: 0x060000AA RID: 170
		[NativeName("SetValueFromScript<UInt32>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetUInt(int nameID, uint i);

		// Token: 0x060000AB RID: 171
		[NativeName("SetValueFromScript<float>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetFloat(int nameID, float f);

		// Token: 0x060000AC RID: 172 RVA: 0x000028BE File Offset: 0x00000ABE
		[NativeName("SetValueFromScript<Vector2f>")]
		public void SetVector2(int nameID, Vector2 v)
		{
			this.SetVector2_Injected(nameID, ref v);
		}

		// Token: 0x060000AD RID: 173 RVA: 0x000028C9 File Offset: 0x00000AC9
		[NativeName("SetValueFromScript<Vector3f>")]
		public void SetVector3(int nameID, Vector3 v)
		{
			this.SetVector3_Injected(nameID, ref v);
		}

		// Token: 0x060000AE RID: 174 RVA: 0x000028D4 File Offset: 0x00000AD4
		[NativeName("SetValueFromScript<Vector4f>")]
		public void SetVector4(int nameID, Vector4 v)
		{
			this.SetVector4_Injected(nameID, ref v);
		}

		// Token: 0x060000AF RID: 175 RVA: 0x000028DF File Offset: 0x00000ADF
		[NativeName("SetValueFromScript<Matrix4x4f>")]
		public void SetMatrix4x4(int nameID, Matrix4x4 v)
		{
			this.SetMatrix4x4_Injected(nameID, ref v);
		}

		// Token: 0x060000B0 RID: 176
		[NativeName("SetValueFromScript<Texture*>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetTexture(int nameID, Texture t);

		// Token: 0x060000B1 RID: 177
		[NativeName("SetValueFromScript<AnimationCurve*>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetAnimationCurve(int nameID, AnimationCurve c);

		// Token: 0x060000B2 RID: 178
		[NativeName("SetValueFromScript<Gradient*>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetGradient(int nameID, Gradient g);

		// Token: 0x060000B3 RID: 179
		[NativeName("SetValueFromScript<Mesh*>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetMesh(int nameID, Mesh m);

		// Token: 0x060000B4 RID: 180
		[NativeName("GetValueFromScript<bool>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool GetBool(int nameID);

		// Token: 0x060000B5 RID: 181
		[NativeName("GetValueFromScript<int>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetInt(int nameID);

		// Token: 0x060000B6 RID: 182
		[NativeName("GetValueFromScript<UInt32>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern uint GetUInt(int nameID);

		// Token: 0x060000B7 RID: 183
		[NativeName("GetValueFromScript<float>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetFloat(int nameID);

		// Token: 0x060000B8 RID: 184 RVA: 0x000028EC File Offset: 0x00000AEC
		[NativeName("GetValueFromScript<Vector2f>")]
		public Vector2 GetVector2(int nameID)
		{
			Vector2 result;
			this.GetVector2_Injected(nameID, out result);
			return result;
		}

		// Token: 0x060000B9 RID: 185 RVA: 0x00002904 File Offset: 0x00000B04
		[NativeName("GetValueFromScript<Vector3f>")]
		public Vector3 GetVector3(int nameID)
		{
			Vector3 result;
			this.GetVector3_Injected(nameID, out result);
			return result;
		}

		// Token: 0x060000BA RID: 186 RVA: 0x0000291C File Offset: 0x00000B1C
		[NativeName("GetValueFromScript<Vector4f>")]
		public Vector4 GetVector4(int nameID)
		{
			Vector4 result;
			this.GetVector4_Injected(nameID, out result);
			return result;
		}

		// Token: 0x060000BB RID: 187 RVA: 0x00002934 File Offset: 0x00000B34
		[NativeName("GetValueFromScript<Matrix4x4f>")]
		public Matrix4x4 GetMatrix4x4(int nameID)
		{
			Matrix4x4 result;
			this.GetMatrix4x4_Injected(nameID, out result);
			return result;
		}

		// Token: 0x060000BC RID: 188
		[NativeName("GetValueFromScript<Texture*>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Texture GetTexture(int nameID);

		// Token: 0x060000BD RID: 189
		[NativeName("GetValueFromScript<Mesh*>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Mesh GetMesh(int nameID);

		// Token: 0x060000BE RID: 190 RVA: 0x0000294C File Offset: 0x00000B4C
		public Gradient GetGradient(int nameID)
		{
			Gradient gradient = new Gradient();
			this.Internal_GetGradient(nameID, gradient);
			return gradient;
		}

		// Token: 0x060000BF RID: 191
		[NativeName("Internal_GetGradientFromScript")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_GetGradient(int nameID, Gradient gradient);

		// Token: 0x060000C0 RID: 192 RVA: 0x00002970 File Offset: 0x00000B70
		public AnimationCurve GetAnimationCurve(int nameID)
		{
			AnimationCurve animationCurve = new AnimationCurve();
			this.Internal_GetAnimationCurve(nameID, animationCurve);
			return animationCurve;
		}

		// Token: 0x060000C1 RID: 193
		[NativeName("Internal_GetAnimationCurveFromScript")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_GetAnimationCurve(int nameID, AnimationCurve curve);

		// Token: 0x060000C2 RID: 194 RVA: 0x00002994 File Offset: 0x00000B94
		public void ResetOverride(string name)
		{
			this.ResetOverride(Shader.PropertyToID(name));
		}

		// Token: 0x060000C3 RID: 195 RVA: 0x000029A4 File Offset: 0x00000BA4
		public bool HasInt(string name)
		{
			return this.HasInt(Shader.PropertyToID(name));
		}

		// Token: 0x060000C4 RID: 196 RVA: 0x000029C8 File Offset: 0x00000BC8
		public bool HasUInt(string name)
		{
			return this.HasUInt(Shader.PropertyToID(name));
		}

		// Token: 0x060000C5 RID: 197 RVA: 0x000029EC File Offset: 0x00000BEC
		public bool HasFloat(string name)
		{
			return this.HasFloat(Shader.PropertyToID(name));
		}

		// Token: 0x060000C6 RID: 198 RVA: 0x00002A10 File Offset: 0x00000C10
		public bool HasVector2(string name)
		{
			return this.HasVector2(Shader.PropertyToID(name));
		}

		// Token: 0x060000C7 RID: 199 RVA: 0x00002A34 File Offset: 0x00000C34
		public bool HasVector3(string name)
		{
			return this.HasVector3(Shader.PropertyToID(name));
		}

		// Token: 0x060000C8 RID: 200 RVA: 0x00002A58 File Offset: 0x00000C58
		public bool HasVector4(string name)
		{
			return this.HasVector4(Shader.PropertyToID(name));
		}

		// Token: 0x060000C9 RID: 201 RVA: 0x00002A7C File Offset: 0x00000C7C
		public bool HasMatrix4x4(string name)
		{
			return this.HasMatrix4x4(Shader.PropertyToID(name));
		}

		// Token: 0x060000CA RID: 202 RVA: 0x00002AA0 File Offset: 0x00000CA0
		public bool HasTexture(string name)
		{
			return this.HasTexture(Shader.PropertyToID(name));
		}

		// Token: 0x060000CB RID: 203 RVA: 0x00002AC4 File Offset: 0x00000CC4
		public TextureDimension GetTextureDimension(string name)
		{
			return this.GetTextureDimension(Shader.PropertyToID(name));
		}

		// Token: 0x060000CC RID: 204 RVA: 0x00002AE8 File Offset: 0x00000CE8
		public bool HasAnimationCurve(string name)
		{
			return this.HasAnimationCurve(Shader.PropertyToID(name));
		}

		// Token: 0x060000CD RID: 205 RVA: 0x00002B0C File Offset: 0x00000D0C
		public bool HasGradient(string name)
		{
			return this.HasGradient(Shader.PropertyToID(name));
		}

		// Token: 0x060000CE RID: 206 RVA: 0x00002B30 File Offset: 0x00000D30
		public bool HasMesh(string name)
		{
			return this.HasMesh(Shader.PropertyToID(name));
		}

		// Token: 0x060000CF RID: 207 RVA: 0x00002B54 File Offset: 0x00000D54
		public bool HasBool(string name)
		{
			return this.HasBool(Shader.PropertyToID(name));
		}

		// Token: 0x060000D0 RID: 208 RVA: 0x00002B75 File Offset: 0x00000D75
		public void SetInt(string name, int i)
		{
			this.SetInt(Shader.PropertyToID(name), i);
		}

		// Token: 0x060000D1 RID: 209 RVA: 0x00002B85 File Offset: 0x00000D85
		public void SetUInt(string name, uint i)
		{
			this.SetUInt(Shader.PropertyToID(name), i);
		}

		// Token: 0x060000D2 RID: 210 RVA: 0x00002B95 File Offset: 0x00000D95
		public void SetFloat(string name, float f)
		{
			this.SetFloat(Shader.PropertyToID(name), f);
		}

		// Token: 0x060000D3 RID: 211 RVA: 0x00002BA5 File Offset: 0x00000DA5
		public void SetVector2(string name, Vector2 v)
		{
			this.SetVector2(Shader.PropertyToID(name), v);
		}

		// Token: 0x060000D4 RID: 212 RVA: 0x00002BB5 File Offset: 0x00000DB5
		public void SetVector3(string name, Vector3 v)
		{
			this.SetVector3(Shader.PropertyToID(name), v);
		}

		// Token: 0x060000D5 RID: 213 RVA: 0x00002BC5 File Offset: 0x00000DC5
		public void SetVector4(string name, Vector4 v)
		{
			this.SetVector4(Shader.PropertyToID(name), v);
		}

		// Token: 0x060000D6 RID: 214 RVA: 0x00002BD5 File Offset: 0x00000DD5
		public void SetMatrix4x4(string name, Matrix4x4 v)
		{
			this.SetMatrix4x4(Shader.PropertyToID(name), v);
		}

		// Token: 0x060000D7 RID: 215 RVA: 0x00002BE5 File Offset: 0x00000DE5
		public void SetTexture(string name, Texture t)
		{
			this.SetTexture(Shader.PropertyToID(name), t);
		}

		// Token: 0x060000D8 RID: 216 RVA: 0x00002BF5 File Offset: 0x00000DF5
		public void SetAnimationCurve(string name, AnimationCurve c)
		{
			this.SetAnimationCurve(Shader.PropertyToID(name), c);
		}

		// Token: 0x060000D9 RID: 217 RVA: 0x00002C05 File Offset: 0x00000E05
		public void SetGradient(string name, Gradient g)
		{
			this.SetGradient(Shader.PropertyToID(name), g);
		}

		// Token: 0x060000DA RID: 218 RVA: 0x00002C15 File Offset: 0x00000E15
		public void SetMesh(string name, Mesh m)
		{
			this.SetMesh(Shader.PropertyToID(name), m);
		}

		// Token: 0x060000DB RID: 219 RVA: 0x00002C25 File Offset: 0x00000E25
		public void SetBool(string name, bool b)
		{
			this.SetBool(Shader.PropertyToID(name), b);
		}

		// Token: 0x060000DC RID: 220 RVA: 0x00002C38 File Offset: 0x00000E38
		public int GetInt(string name)
		{
			return this.GetInt(Shader.PropertyToID(name));
		}

		// Token: 0x060000DD RID: 221 RVA: 0x00002C5C File Offset: 0x00000E5C
		public uint GetUInt(string name)
		{
			return this.GetUInt(Shader.PropertyToID(name));
		}

		// Token: 0x060000DE RID: 222 RVA: 0x00002C80 File Offset: 0x00000E80
		public float GetFloat(string name)
		{
			return this.GetFloat(Shader.PropertyToID(name));
		}

		// Token: 0x060000DF RID: 223 RVA: 0x00002CA4 File Offset: 0x00000EA4
		public Vector2 GetVector2(string name)
		{
			return this.GetVector2(Shader.PropertyToID(name));
		}

		// Token: 0x060000E0 RID: 224 RVA: 0x00002CC8 File Offset: 0x00000EC8
		public Vector3 GetVector3(string name)
		{
			return this.GetVector3(Shader.PropertyToID(name));
		}

		// Token: 0x060000E1 RID: 225 RVA: 0x00002CEC File Offset: 0x00000EEC
		public Vector4 GetVector4(string name)
		{
			return this.GetVector4(Shader.PropertyToID(name));
		}

		// Token: 0x060000E2 RID: 226 RVA: 0x00002D10 File Offset: 0x00000F10
		public Matrix4x4 GetMatrix4x4(string name)
		{
			return this.GetMatrix4x4(Shader.PropertyToID(name));
		}

		// Token: 0x060000E3 RID: 227 RVA: 0x00002D34 File Offset: 0x00000F34
		public Texture GetTexture(string name)
		{
			return this.GetTexture(Shader.PropertyToID(name));
		}

		// Token: 0x060000E4 RID: 228 RVA: 0x00002D58 File Offset: 0x00000F58
		public Mesh GetMesh(string name)
		{
			return this.GetMesh(Shader.PropertyToID(name));
		}

		// Token: 0x060000E5 RID: 229 RVA: 0x00002D7C File Offset: 0x00000F7C
		public bool GetBool(string name)
		{
			return this.GetBool(Shader.PropertyToID(name));
		}

		// Token: 0x060000E6 RID: 230 RVA: 0x00002DA0 File Offset: 0x00000FA0
		public AnimationCurve GetAnimationCurve(string name)
		{
			return this.GetAnimationCurve(Shader.PropertyToID(name));
		}

		// Token: 0x060000E7 RID: 231 RVA: 0x00002DC4 File Offset: 0x00000FC4
		public Gradient GetGradient(string name)
		{
			return this.GetGradient(Shader.PropertyToID(name));
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x060000E8 RID: 232
		public extern int aliveParticleCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060000E9 RID: 233
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetVector2_Injected(int nameID, ref Vector2 v);

		// Token: 0x060000EA RID: 234
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetVector3_Injected(int nameID, ref Vector3 v);

		// Token: 0x060000EB RID: 235
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetVector4_Injected(int nameID, ref Vector4 v);

		// Token: 0x060000EC RID: 236
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetMatrix4x4_Injected(int nameID, ref Matrix4x4 v);

		// Token: 0x060000ED RID: 237
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetVector2_Injected(int nameID, out Vector2 ret);

		// Token: 0x060000EE RID: 238
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetVector3_Injected(int nameID, out Vector3 ret);

		// Token: 0x060000EF RID: 239
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetVector4_Injected(int nameID, out Vector4 ret);

		// Token: 0x060000F0 RID: 240
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetMatrix4x4_Injected(int nameID, out Matrix4x4 ret);
	}
}
