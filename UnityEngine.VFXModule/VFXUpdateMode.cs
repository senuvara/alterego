﻿using System;

namespace UnityEngine.Experimental.VFX
{
	// Token: 0x02000008 RID: 8
	internal enum VFXUpdateMode
	{
		// Token: 0x04000080 RID: 128
		FixedDeltaTime,
		// Token: 0x04000081 RID: 129
		DeltaTime
	}
}
