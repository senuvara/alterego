﻿using System;

namespace UnityEngine.Experimental.VFX
{
	// Token: 0x02000006 RID: 6
	internal enum VFXSystemType
	{
		// Token: 0x04000077 RID: 119
		Spawner,
		// Token: 0x04000078 RID: 120
		Particle,
		// Token: 0x04000079 RID: 121
		Mesh
	}
}
