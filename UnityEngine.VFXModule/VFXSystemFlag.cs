﻿using System;

namespace UnityEngine.Experimental.VFX
{
	// Token: 0x02000007 RID: 7
	internal enum VFXSystemFlag
	{
		// Token: 0x0400007B RID: 123
		SystemDefault,
		// Token: 0x0400007C RID: 124
		SystemHasKill,
		// Token: 0x0400007D RID: 125
		SystemHasIndirectBuffer,
		// Token: 0x0400007E RID: 126
		SystemReceivedEventGPU = 4
	}
}
