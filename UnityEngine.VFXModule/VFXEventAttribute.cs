﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.VFX
{
	// Token: 0x02000009 RID: 9
	[NativeType(Header = "Modules/VFX/Public/VFXEventAttribute.h")]
	[RequiredByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class VFXEventAttribute : IDisposable
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		private VFXEventAttribute(IntPtr ptr, bool owner)
		{
			this.m_Ptr = ptr;
			this.m_Owner = owner;
		}

		// Token: 0x06000002 RID: 2 RVA: 0x00002067 File Offset: 0x00000267
		private VFXEventAttribute() : this(IntPtr.Zero, false)
		{
		}

		// Token: 0x06000003 RID: 3 RVA: 0x00002076 File Offset: 0x00000276
		public VFXEventAttribute(VFXEventAttribute original)
		{
			if (original == null)
			{
				throw new ArgumentNullException("VFXEventAttribute expect a non null attribute");
			}
			this.m_Ptr = VFXEventAttribute.Internal_Create();
			this.Internal_InitFromEventAttribute(original);
		}

		// Token: 0x06000004 RID: 4
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern IntPtr Internal_Create();

		// Token: 0x06000005 RID: 5 RVA: 0x000020A4 File Offset: 0x000002A4
		internal static VFXEventAttribute Internal_InstanciateVFXEventAttribute(VisualEffectAsset vfxAsset)
		{
			VFXEventAttribute vfxeventAttribute = new VFXEventAttribute(VFXEventAttribute.Internal_Create(), true);
			vfxeventAttribute.Internal_InitFromAsset(vfxAsset);
			return vfxeventAttribute;
		}

		// Token: 0x06000006 RID: 6
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void Internal_InitFromAsset(VisualEffectAsset vfxAsset);

		// Token: 0x06000007 RID: 7
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void Internal_InitFromEventAttribute(VFXEventAttribute vfxEventAttribute);

		// Token: 0x06000008 RID: 8 RVA: 0x000020CD File Offset: 0x000002CD
		private void Release()
		{
			if (this.m_Owner && this.m_Ptr != IntPtr.Zero)
			{
				VFXEventAttribute.Internal_Destroy(this.m_Ptr);
			}
			this.m_Ptr = IntPtr.Zero;
		}

		// Token: 0x06000009 RID: 9 RVA: 0x00002108 File Offset: 0x00000308
		~VFXEventAttribute()
		{
			this.Release();
		}

		// Token: 0x0600000A RID: 10 RVA: 0x00002138 File Offset: 0x00000338
		public void Dispose()
		{
			this.Release();
			GC.SuppressFinalize(this);
		}

		// Token: 0x0600000B RID: 11
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Internal_Destroy(IntPtr ptr);

		// Token: 0x0600000C RID: 12
		[NativeName("HasValueFromScript<bool>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasBool(int nameID);

		// Token: 0x0600000D RID: 13
		[NativeName("HasValueFromScript<int>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasInt(int nameID);

		// Token: 0x0600000E RID: 14
		[NativeName("HasValueFromScript<UInt32>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasUint(int nameID);

		// Token: 0x0600000F RID: 15
		[NativeName("HasValueFromScript<float>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasFloat(int nameID);

		// Token: 0x06000010 RID: 16
		[NativeName("HasValueFromScript<Vector2f>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasVector2(int nameID);

		// Token: 0x06000011 RID: 17
		[NativeName("HasValueFromScript<Vector3f>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasVector3(int nameID);

		// Token: 0x06000012 RID: 18
		[NativeName("HasValueFromScript<Vector4f>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasVector4(int nameID);

		// Token: 0x06000013 RID: 19
		[NativeName("HasValueFromScript<Matrix4x4f>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasMatrix4x4(int nameID);

		// Token: 0x06000014 RID: 20
		[NativeName("SetValueFromScript<bool>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetBool(int nameID, bool b);

		// Token: 0x06000015 RID: 21
		[NativeName("SetValueFromScript<int>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetInt(int nameID, int i);

		// Token: 0x06000016 RID: 22
		[NativeName("SetValueFromScript<UInt32>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetUint(int nameID, uint i);

		// Token: 0x06000017 RID: 23
		[NativeName("SetValueFromScript<float>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetFloat(int nameID, float f);

		// Token: 0x06000018 RID: 24 RVA: 0x00002147 File Offset: 0x00000347
		[NativeName("SetValueFromScript<Vector2f>")]
		public void SetVector2(int nameID, Vector2 v)
		{
			this.SetVector2_Injected(nameID, ref v);
		}

		// Token: 0x06000019 RID: 25 RVA: 0x00002152 File Offset: 0x00000352
		[NativeName("SetValueFromScript<Vector3f>")]
		public void SetVector3(int nameID, Vector3 v)
		{
			this.SetVector3_Injected(nameID, ref v);
		}

		// Token: 0x0600001A RID: 26 RVA: 0x0000215D File Offset: 0x0000035D
		[NativeName("SetValueFromScript<Vector4f>")]
		public void SetVector4(int nameID, Vector4 v)
		{
			this.SetVector4_Injected(nameID, ref v);
		}

		// Token: 0x0600001B RID: 27 RVA: 0x00002168 File Offset: 0x00000368
		[NativeName("SetValueFromScript<Matrix4x4f>")]
		public void SetMatrix4x4(int nameID, Matrix4x4 v)
		{
			this.SetMatrix4x4_Injected(nameID, ref v);
		}

		// Token: 0x0600001C RID: 28
		[NativeName("GetValueFromScript<bool>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool GetBool(int nameID);

		// Token: 0x0600001D RID: 29
		[NativeName("GetValueFromScript<int>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetInt(int nameID);

		// Token: 0x0600001E RID: 30
		[NativeName("GetValueFromScript<UInt32>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern uint GetUint(int nameID);

		// Token: 0x0600001F RID: 31
		[NativeName("GetValueFromScript<float>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetFloat(int nameID);

		// Token: 0x06000020 RID: 32 RVA: 0x00002174 File Offset: 0x00000374
		[NativeName("GetValueFromScript<Vector2f>")]
		public Vector2 GetVector2(int nameID)
		{
			Vector2 result;
			this.GetVector2_Injected(nameID, out result);
			return result;
		}

		// Token: 0x06000021 RID: 33 RVA: 0x0000218C File Offset: 0x0000038C
		[NativeName("GetValueFromScript<Vector3f>")]
		public Vector3 GetVector3(int nameID)
		{
			Vector3 result;
			this.GetVector3_Injected(nameID, out result);
			return result;
		}

		// Token: 0x06000022 RID: 34 RVA: 0x000021A4 File Offset: 0x000003A4
		[NativeName("GetValueFromScript<Vector4f>")]
		public Vector4 GetVector4(int nameID)
		{
			Vector4 result;
			this.GetVector4_Injected(nameID, out result);
			return result;
		}

		// Token: 0x06000023 RID: 35 RVA: 0x000021BC File Offset: 0x000003BC
		[NativeName("GetValueFromScript<Matrix4x4f>")]
		public Matrix4x4 GetMatrix4x4(int nameID)
		{
			Matrix4x4 result;
			this.GetMatrix4x4_Injected(nameID, out result);
			return result;
		}

		// Token: 0x06000024 RID: 36 RVA: 0x000021D4 File Offset: 0x000003D4
		public bool HasBool(string name)
		{
			return this.HasBool(Shader.PropertyToID(name));
		}

		// Token: 0x06000025 RID: 37 RVA: 0x000021F8 File Offset: 0x000003F8
		public bool HasInt(string name)
		{
			return this.HasInt(Shader.PropertyToID(name));
		}

		// Token: 0x06000026 RID: 38 RVA: 0x0000221C File Offset: 0x0000041C
		public bool HasUint(string name)
		{
			return this.HasUint(Shader.PropertyToID(name));
		}

		// Token: 0x06000027 RID: 39 RVA: 0x00002240 File Offset: 0x00000440
		public bool HasFloat(string name)
		{
			return this.HasFloat(Shader.PropertyToID(name));
		}

		// Token: 0x06000028 RID: 40 RVA: 0x00002264 File Offset: 0x00000464
		public bool HasVector2(string name)
		{
			return this.HasVector2(Shader.PropertyToID(name));
		}

		// Token: 0x06000029 RID: 41 RVA: 0x00002288 File Offset: 0x00000488
		public bool HasVector3(string name)
		{
			return this.HasVector3(Shader.PropertyToID(name));
		}

		// Token: 0x0600002A RID: 42 RVA: 0x000022AC File Offset: 0x000004AC
		public bool HasVector4(string name)
		{
			return this.HasVector4(Shader.PropertyToID(name));
		}

		// Token: 0x0600002B RID: 43 RVA: 0x000022D0 File Offset: 0x000004D0
		public bool HasMatrix4x4(string name)
		{
			return this.HasMatrix4x4(Shader.PropertyToID(name));
		}

		// Token: 0x0600002C RID: 44 RVA: 0x000022F1 File Offset: 0x000004F1
		public void SetBool(string name, bool b)
		{
			this.SetBool(Shader.PropertyToID(name), b);
		}

		// Token: 0x0600002D RID: 45 RVA: 0x00002301 File Offset: 0x00000501
		public void SetInt(string name, int i)
		{
			this.SetInt(Shader.PropertyToID(name), i);
		}

		// Token: 0x0600002E RID: 46 RVA: 0x00002311 File Offset: 0x00000511
		public void SetUint(string name, uint i)
		{
			this.SetUint(Shader.PropertyToID(name), i);
		}

		// Token: 0x0600002F RID: 47 RVA: 0x00002321 File Offset: 0x00000521
		public void SetFloat(string name, float f)
		{
			this.SetFloat(Shader.PropertyToID(name), f);
		}

		// Token: 0x06000030 RID: 48 RVA: 0x00002331 File Offset: 0x00000531
		public void SetVector2(string name, Vector2 v)
		{
			this.SetVector2(Shader.PropertyToID(name), v);
		}

		// Token: 0x06000031 RID: 49 RVA: 0x00002341 File Offset: 0x00000541
		public void SetVector3(string name, Vector3 v)
		{
			this.SetVector3(Shader.PropertyToID(name), v);
		}

		// Token: 0x06000032 RID: 50 RVA: 0x00002351 File Offset: 0x00000551
		public void SetVector4(string name, Vector4 v)
		{
			this.SetVector4(Shader.PropertyToID(name), v);
		}

		// Token: 0x06000033 RID: 51 RVA: 0x00002361 File Offset: 0x00000561
		public void SetMatrix4x4(string name, Matrix4x4 v)
		{
			this.SetMatrix4x4(Shader.PropertyToID(name), v);
		}

		// Token: 0x06000034 RID: 52 RVA: 0x00002374 File Offset: 0x00000574
		public bool GetBool(string name)
		{
			return this.GetBool(Shader.PropertyToID(name));
		}

		// Token: 0x06000035 RID: 53 RVA: 0x00002398 File Offset: 0x00000598
		public int GetInt(string name)
		{
			return this.GetInt(Shader.PropertyToID(name));
		}

		// Token: 0x06000036 RID: 54 RVA: 0x000023BC File Offset: 0x000005BC
		public uint GetUint(string name)
		{
			return this.GetUint(Shader.PropertyToID(name));
		}

		// Token: 0x06000037 RID: 55 RVA: 0x000023E0 File Offset: 0x000005E0
		public float GetFloat(string name)
		{
			return this.GetFloat(Shader.PropertyToID(name));
		}

		// Token: 0x06000038 RID: 56 RVA: 0x00002404 File Offset: 0x00000604
		public Vector2 GetVector2(string name)
		{
			return this.GetVector2(Shader.PropertyToID(name));
		}

		// Token: 0x06000039 RID: 57 RVA: 0x00002428 File Offset: 0x00000628
		public Vector3 GetVector3(string name)
		{
			return this.GetVector3(Shader.PropertyToID(name));
		}

		// Token: 0x0600003A RID: 58 RVA: 0x0000244C File Offset: 0x0000064C
		public Vector4 GetVector4(string name)
		{
			return this.GetVector4(Shader.PropertyToID(name));
		}

		// Token: 0x0600003B RID: 59 RVA: 0x00002470 File Offset: 0x00000670
		public Matrix4x4 GetMatrix4x4(string name)
		{
			return this.GetMatrix4x4(Shader.PropertyToID(name));
		}

		// Token: 0x0600003C RID: 60
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void CopyValuesFrom([NotNull] VFXEventAttribute eventAttibute);

		// Token: 0x0600003D RID: 61
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetVector2_Injected(int nameID, ref Vector2 v);

		// Token: 0x0600003E RID: 62
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetVector3_Injected(int nameID, ref Vector3 v);

		// Token: 0x0600003F RID: 63
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetVector4_Injected(int nameID, ref Vector4 v);

		// Token: 0x06000040 RID: 64
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetMatrix4x4_Injected(int nameID, ref Matrix4x4 v);

		// Token: 0x06000041 RID: 65
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetVector2_Injected(int nameID, out Vector2 ret);

		// Token: 0x06000042 RID: 66
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetVector3_Injected(int nameID, out Vector3 ret);

		// Token: 0x06000043 RID: 67
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetVector4_Injected(int nameID, out Vector4 ret);

		// Token: 0x06000044 RID: 68
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetMatrix4x4_Injected(int nameID, out Matrix4x4 ret);

		// Token: 0x04000082 RID: 130
		private IntPtr m_Ptr;

		// Token: 0x04000083 RID: 131
		private bool m_Owner;
	}
}
