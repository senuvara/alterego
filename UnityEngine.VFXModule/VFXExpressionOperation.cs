﻿using System;

namespace UnityEngine.Experimental.VFX
{
	// Token: 0x02000003 RID: 3
	internal enum VFXExpressionOperation
	{
		// Token: 0x04000007 RID: 7
		None,
		// Token: 0x04000008 RID: 8
		Value,
		// Token: 0x04000009 RID: 9
		Combine2f,
		// Token: 0x0400000A RID: 10
		Combine3f,
		// Token: 0x0400000B RID: 11
		Combine4f,
		// Token: 0x0400000C RID: 12
		ExtractComponent,
		// Token: 0x0400000D RID: 13
		DeltaTime,
		// Token: 0x0400000E RID: 14
		TotalTime,
		// Token: 0x0400000F RID: 15
		SystemSeed,
		// Token: 0x04000010 RID: 16
		LocalToWorld,
		// Token: 0x04000011 RID: 17
		WorldToLocal,
		// Token: 0x04000012 RID: 18
		Sin,
		// Token: 0x04000013 RID: 19
		Cos,
		// Token: 0x04000014 RID: 20
		Tan,
		// Token: 0x04000015 RID: 21
		ASin,
		// Token: 0x04000016 RID: 22
		ACos,
		// Token: 0x04000017 RID: 23
		ATan,
		// Token: 0x04000018 RID: 24
		Abs,
		// Token: 0x04000019 RID: 25
		Sign,
		// Token: 0x0400001A RID: 26
		Floor,
		// Token: 0x0400001B RID: 27
		Log2,
		// Token: 0x0400001C RID: 28
		Mul,
		// Token: 0x0400001D RID: 29
		Divide,
		// Token: 0x0400001E RID: 30
		Add,
		// Token: 0x0400001F RID: 31
		Subtract,
		// Token: 0x04000020 RID: 32
		Min,
		// Token: 0x04000021 RID: 33
		Max,
		// Token: 0x04000022 RID: 34
		Pow,
		// Token: 0x04000023 RID: 35
		ATan2,
		// Token: 0x04000024 RID: 36
		TRSToMatrix,
		// Token: 0x04000025 RID: 37
		InverseMatrix,
		// Token: 0x04000026 RID: 38
		ExtractPositionFromMatrix,
		// Token: 0x04000027 RID: 39
		ExtractAnglesFromMatrix,
		// Token: 0x04000028 RID: 40
		ExtractScaleFromMatrix,
		// Token: 0x04000029 RID: 41
		TransformMatrix,
		// Token: 0x0400002A RID: 42
		TransformPos,
		// Token: 0x0400002B RID: 43
		TransformVec,
		// Token: 0x0400002C RID: 44
		TransformDir,
		// Token: 0x0400002D RID: 45
		Vector3sToMatrix,
		// Token: 0x0400002E RID: 46
		Vector4sToMatrix,
		// Token: 0x0400002F RID: 47
		MatrixToVector3s,
		// Token: 0x04000030 RID: 48
		MatrixToVector4s,
		// Token: 0x04000031 RID: 49
		SampleCurve,
		// Token: 0x04000032 RID: 50
		SampleGradient,
		// Token: 0x04000033 RID: 51
		BakeCurve,
		// Token: 0x04000034 RID: 52
		BakeGradient,
		// Token: 0x04000035 RID: 53
		BitwiseLeftShift,
		// Token: 0x04000036 RID: 54
		BitwiseRightShift,
		// Token: 0x04000037 RID: 55
		BitwiseOr,
		// Token: 0x04000038 RID: 56
		BitwiseAnd,
		// Token: 0x04000039 RID: 57
		BitwiseXor,
		// Token: 0x0400003A RID: 58
		BitwiseComplement,
		// Token: 0x0400003B RID: 59
		CastUintToFloat,
		// Token: 0x0400003C RID: 60
		CastIntToFloat,
		// Token: 0x0400003D RID: 61
		CastFloatToUint,
		// Token: 0x0400003E RID: 62
		CastIntToUint,
		// Token: 0x0400003F RID: 63
		CastFloatToInt,
		// Token: 0x04000040 RID: 64
		CastUintToInt,
		// Token: 0x04000041 RID: 65
		RGBtoHSV,
		// Token: 0x04000042 RID: 66
		HSVtoRGB,
		// Token: 0x04000043 RID: 67
		Condition,
		// Token: 0x04000044 RID: 68
		Branch,
		// Token: 0x04000045 RID: 69
		GenerateRandom,
		// Token: 0x04000046 RID: 70
		GenerateFixedRandom,
		// Token: 0x04000047 RID: 71
		ExtractMatrixFromMainCamera,
		// Token: 0x04000048 RID: 72
		ExtractFOVFromMainCamera,
		// Token: 0x04000049 RID: 73
		ExtractNearPlaneFromMainCamera,
		// Token: 0x0400004A RID: 74
		ExtractFarPlaneFromMainCamera,
		// Token: 0x0400004B RID: 75
		ExtractAspectRatioFromMainCamera,
		// Token: 0x0400004C RID: 76
		ExtractPixelDimensionsFromMainCamera,
		// Token: 0x0400004D RID: 77
		LogicalAnd,
		// Token: 0x0400004E RID: 78
		LogicalOr,
		// Token: 0x0400004F RID: 79
		LogicalNot,
		// Token: 0x04000050 RID: 80
		InverseTRS = 30
	}
}
