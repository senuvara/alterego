﻿using System;

namespace UnityEngine.Experimental.VFX
{
	// Token: 0x02000004 RID: 4
	internal enum VFXValueType
	{
		// Token: 0x04000052 RID: 82
		None,
		// Token: 0x04000053 RID: 83
		Float,
		// Token: 0x04000054 RID: 84
		Float2,
		// Token: 0x04000055 RID: 85
		Float3,
		// Token: 0x04000056 RID: 86
		Float4,
		// Token: 0x04000057 RID: 87
		Int32,
		// Token: 0x04000058 RID: 88
		Uint32,
		// Token: 0x04000059 RID: 89
		Texture2D,
		// Token: 0x0400005A RID: 90
		Texture2DArray,
		// Token: 0x0400005B RID: 91
		Texture3D,
		// Token: 0x0400005C RID: 92
		TextureCube,
		// Token: 0x0400005D RID: 93
		TextureCubeArray,
		// Token: 0x0400005E RID: 94
		Matrix4x4,
		// Token: 0x0400005F RID: 95
		Curve,
		// Token: 0x04000060 RID: 96
		ColorGradient,
		// Token: 0x04000061 RID: 97
		Mesh,
		// Token: 0x04000062 RID: 98
		Spline,
		// Token: 0x04000063 RID: 99
		Boolean
	}
}
