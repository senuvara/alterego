﻿using System;

namespace UnityEngine.Experimental.VFX
{
	// Token: 0x02000005 RID: 5
	internal enum VFXTaskType
	{
		// Token: 0x04000065 RID: 101
		None,
		// Token: 0x04000066 RID: 102
		Spawner = 268435456,
		// Token: 0x04000067 RID: 103
		Initialize = 536870912,
		// Token: 0x04000068 RID: 104
		Update = 805306368,
		// Token: 0x04000069 RID: 105
		Output = 1073741824,
		// Token: 0x0400006A RID: 106
		CameraSort = 805306369,
		// Token: 0x0400006B RID: 107
		ParticlePointOutput = 1073741824,
		// Token: 0x0400006C RID: 108
		ParticleLineOutput,
		// Token: 0x0400006D RID: 109
		ParticleQuadOutput,
		// Token: 0x0400006E RID: 110
		ParticleHexahedronOutput,
		// Token: 0x0400006F RID: 111
		ParticleMeshOutput,
		// Token: 0x04000070 RID: 112
		ConstantRateSpawner = 268435456,
		// Token: 0x04000071 RID: 113
		BurstSpawner,
		// Token: 0x04000072 RID: 114
		PeriodicBurstSpawner,
		// Token: 0x04000073 RID: 115
		VariableRateSpawner,
		// Token: 0x04000074 RID: 116
		CustomCallbackSpawner,
		// Token: 0x04000075 RID: 117
		SetAttributeSpawner
	}
}
