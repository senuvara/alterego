﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.VFX
{
	// Token: 0x0200000D RID: 13
	[RequiredByNativeCode]
	[NativeType(Header = "Modules/VFX/Public/VFXSpawnerState.h")]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class VFXSpawnerState : IDisposable
	{
		// Token: 0x06000072 RID: 114 RVA: 0x00002767 File Offset: 0x00000967
		internal VFXSpawnerState(IntPtr ptr, bool owner)
		{
			this.m_Ptr = ptr;
			this.m_Owner = owner;
		}

		// Token: 0x06000073 RID: 115
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern IntPtr Internal_Create();

		// Token: 0x06000074 RID: 116 RVA: 0x00002780 File Offset: 0x00000980
		[RequiredByNativeCode]
		internal static VFXSpawnerState CreateSpawnerStateWrapper()
		{
			return new VFXSpawnerState(IntPtr.Zero, false);
		}

		// Token: 0x06000075 RID: 117 RVA: 0x000027A2 File Offset: 0x000009A2
		[RequiredByNativeCode]
		internal void SetWrapValue(IntPtr ptr)
		{
			if (this.m_Owner)
			{
				throw new Exception("VFXSpawnerState : SetWrapValue is reserved to CreateWrapper object");
			}
			this.m_Ptr = ptr;
		}

		// Token: 0x06000076 RID: 118 RVA: 0x000027C3 File Offset: 0x000009C3
		private void Release()
		{
			if (this.m_Ptr != IntPtr.Zero && this.m_Owner)
			{
				VFXSpawnerState.Internal_Destroy(this.m_Ptr);
			}
			this.m_Ptr = IntPtr.Zero;
		}

		// Token: 0x06000077 RID: 119 RVA: 0x00002800 File Offset: 0x00000A00
		~VFXSpawnerState()
		{
			this.Release();
		}

		// Token: 0x06000078 RID: 120 RVA: 0x00002830 File Offset: 0x00000A30
		public void Dispose()
		{
			this.Release();
			GC.SuppressFinalize(this);
		}

		// Token: 0x06000079 RID: 121
		[NativeMethod(IsThreadSafe = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Destroy(IntPtr ptr);

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x0600007A RID: 122
		// (set) Token: 0x0600007B RID: 123
		public extern bool playing { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x0600007C RID: 124
		// (set) Token: 0x0600007D RID: 125
		public extern float spawnCount { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x0600007E RID: 126
		// (set) Token: 0x0600007F RID: 127
		public extern float deltaTime { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000080 RID: 128
		// (set) Token: 0x06000081 RID: 129
		public extern float totalTime { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000082 RID: 130
		public extern VFXEventAttribute vfxEventAttribute { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x04000087 RID: 135
		private IntPtr m_Ptr;

		// Token: 0x04000088 RID: 136
		private bool m_Owner;
	}
}
