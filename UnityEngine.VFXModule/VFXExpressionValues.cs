﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.VFX
{
	// Token: 0x0200000A RID: 10
	[NativeType(Header = "Modules/VFX/Public/VFXExpressionValues.h")]
	[RequiredByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public class VFXExpressionValues
	{
		// Token: 0x06000045 RID: 69 RVA: 0x00002491 File Offset: 0x00000691
		private VFXExpressionValues()
		{
		}

		// Token: 0x06000046 RID: 70 RVA: 0x0000249C File Offset: 0x0000069C
		[RequiredByNativeCode]
		internal static VFXExpressionValues CreateExpressionValuesWrapper(IntPtr ptr)
		{
			return new VFXExpressionValues
			{
				m_Ptr = ptr
			};
		}

		// Token: 0x06000047 RID: 71
		[NativeThrows]
		[NativeName("GetValueFromScript<bool>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool GetBool(int nameID);

		// Token: 0x06000048 RID: 72
		[NativeThrows]
		[NativeName("GetValueFromScript<int>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetInt(int nameID);

		// Token: 0x06000049 RID: 73
		[NativeName("GetValueFromScript<UInt32>")]
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern uint GetUInt(int nameID);

		// Token: 0x0600004A RID: 74
		[NativeThrows]
		[NativeName("GetValueFromScript<float>")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetFloat(int nameID);

		// Token: 0x0600004B RID: 75 RVA: 0x000024C0 File Offset: 0x000006C0
		[NativeThrows]
		[NativeName("GetValueFromScript<Vector2f>")]
		public Vector2 GetVector2(int nameID)
		{
			Vector2 result;
			this.GetVector2_Injected(nameID, out result);
			return result;
		}

		// Token: 0x0600004C RID: 76 RVA: 0x000024D8 File Offset: 0x000006D8
		[NativeName("GetValueFromScript<Vector3f>")]
		[NativeThrows]
		public Vector3 GetVector3(int nameID)
		{
			Vector3 result;
			this.GetVector3_Injected(nameID, out result);
			return result;
		}

		// Token: 0x0600004D RID: 77 RVA: 0x000024F0 File Offset: 0x000006F0
		[NativeThrows]
		[NativeName("GetValueFromScript<Vector4f>")]
		public Vector4 GetVector4(int nameID)
		{
			Vector4 result;
			this.GetVector4_Injected(nameID, out result);
			return result;
		}

		// Token: 0x0600004E RID: 78 RVA: 0x00002508 File Offset: 0x00000708
		[NativeName("GetValueFromScript<Matrix4x4f>")]
		[NativeThrows]
		public Matrix4x4 GetMatrix4x4(int nameID)
		{
			Matrix4x4 result;
			this.GetMatrix4x4_Injected(nameID, out result);
			return result;
		}

		// Token: 0x0600004F RID: 79
		[NativeName("GetValueFromScript<Texture*>")]
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Texture GetTexture(int nameID);

		// Token: 0x06000050 RID: 80
		[NativeName("GetValueFromScript<Mesh*>")]
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Mesh GetMesh(int nameID);

		// Token: 0x06000051 RID: 81 RVA: 0x00002520 File Offset: 0x00000720
		public AnimationCurve GetAnimationCurve(int nameID)
		{
			AnimationCurve animationCurve = new AnimationCurve();
			this.Internal_GetAnimationCurveFromScript(nameID, animationCurve);
			return animationCurve;
		}

		// Token: 0x06000052 RID: 82
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void Internal_GetAnimationCurveFromScript(int nameID, AnimationCurve curve);

		// Token: 0x06000053 RID: 83 RVA: 0x00002544 File Offset: 0x00000744
		public Gradient GetGradient(int nameID)
		{
			Gradient gradient = new Gradient();
			this.Internal_GetGradientFromScript(nameID, gradient);
			return gradient;
		}

		// Token: 0x06000054 RID: 84
		[NativeThrows]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void Internal_GetGradientFromScript(int nameID, Gradient gradient);

		// Token: 0x06000055 RID: 85 RVA: 0x00002568 File Offset: 0x00000768
		public bool GetBool(string name)
		{
			return this.GetBool(Shader.PropertyToID(name));
		}

		// Token: 0x06000056 RID: 86 RVA: 0x0000258C File Offset: 0x0000078C
		public int GetInt(string name)
		{
			return this.GetInt(Shader.PropertyToID(name));
		}

		// Token: 0x06000057 RID: 87 RVA: 0x000025B0 File Offset: 0x000007B0
		public uint GetUInt(string name)
		{
			return this.GetUInt(Shader.PropertyToID(name));
		}

		// Token: 0x06000058 RID: 88 RVA: 0x000025D4 File Offset: 0x000007D4
		public float GetFloat(string name)
		{
			return this.GetFloat(Shader.PropertyToID(name));
		}

		// Token: 0x06000059 RID: 89 RVA: 0x000025F8 File Offset: 0x000007F8
		public Vector2 GetVector2(string name)
		{
			return this.GetVector2(Shader.PropertyToID(name));
		}

		// Token: 0x0600005A RID: 90 RVA: 0x0000261C File Offset: 0x0000081C
		public Vector3 GetVector3(string name)
		{
			return this.GetVector3(Shader.PropertyToID(name));
		}

		// Token: 0x0600005B RID: 91 RVA: 0x00002640 File Offset: 0x00000840
		public Vector4 GetVector4(string name)
		{
			return this.GetVector4(Shader.PropertyToID(name));
		}

		// Token: 0x0600005C RID: 92 RVA: 0x00002664 File Offset: 0x00000864
		public Matrix4x4 GetMatrix4x4(string name)
		{
			return this.GetMatrix4x4(Shader.PropertyToID(name));
		}

		// Token: 0x0600005D RID: 93 RVA: 0x00002688 File Offset: 0x00000888
		public Texture GetTexture(string name)
		{
			return this.GetTexture(Shader.PropertyToID(name));
		}

		// Token: 0x0600005E RID: 94 RVA: 0x000026AC File Offset: 0x000008AC
		public AnimationCurve GetAnimationCurve(string name)
		{
			return this.GetAnimationCurve(Shader.PropertyToID(name));
		}

		// Token: 0x0600005F RID: 95 RVA: 0x000026D0 File Offset: 0x000008D0
		public Gradient GetGradient(string name)
		{
			return this.GetGradient(Shader.PropertyToID(name));
		}

		// Token: 0x06000060 RID: 96 RVA: 0x000026F4 File Offset: 0x000008F4
		public Mesh GetMesh(string name)
		{
			return this.GetMesh(Shader.PropertyToID(name));
		}

		// Token: 0x06000061 RID: 97
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetVector2_Injected(int nameID, out Vector2 ret);

		// Token: 0x06000062 RID: 98
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetVector3_Injected(int nameID, out Vector3 ret);

		// Token: 0x06000063 RID: 99
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetVector4_Injected(int nameID, out Vector4 ret);

		// Token: 0x06000064 RID: 100
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetMatrix4x4_Injected(int nameID, out Matrix4x4 ret);

		// Token: 0x04000084 RID: 132
		internal IntPtr m_Ptr;
	}
}
