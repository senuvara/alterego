﻿using System;

namespace UnityEngine.XR
{
	// Token: 0x02000015 RID: 21
	[Flags]
	internal enum AvailableTrackingData
	{
		// Token: 0x04000022 RID: 34
		None = 0,
		// Token: 0x04000023 RID: 35
		PositionAvailable = 1,
		// Token: 0x04000024 RID: 36
		RotationAvailable = 2,
		// Token: 0x04000025 RID: 37
		VelocityAvailable = 4,
		// Token: 0x04000026 RID: 38
		AngularVelocityAvailable = 8,
		// Token: 0x04000027 RID: 39
		AccelerationAvailable = 16,
		// Token: 0x04000028 RID: 40
		AngularAccelerationAvailable = 32
	}
}
