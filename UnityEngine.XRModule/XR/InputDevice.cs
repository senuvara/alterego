﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.XR
{
	// Token: 0x02000018 RID: 24
	[NativeConditional("ENABLE_VR")]
	[NativeHeader("Modules/XR/Subsystems/Input/Public/XRInputTrackingFacade.h")]
	[UsedByNativeCode]
	public struct InputDevice : IEquatable<InputDevice>
	{
		// Token: 0x06000076 RID: 118 RVA: 0x00002F27 File Offset: 0x00001127
		internal InputDevice(ulong deviceId)
		{
			this.m_DeviceId = deviceId;
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000077 RID: 119 RVA: 0x00002F34 File Offset: 0x00001134
		public bool IsValid
		{
			get
			{
				return InputTracking.IsDeviceValid(this.m_DeviceId);
			}
		}

		// Token: 0x06000078 RID: 120 RVA: 0x00002F54 File Offset: 0x00001154
		public bool SendHapticImpulse(uint channel, float amplitude, float duration = 1f)
		{
			return InputTracking.SendHapticImpulse(this.m_DeviceId, channel, amplitude, duration);
		}

		// Token: 0x06000079 RID: 121 RVA: 0x00002F78 File Offset: 0x00001178
		public bool SendHapticBuffer(uint channel, byte[] buffer)
		{
			return InputTracking.SendHapticBuffer(this.m_DeviceId, channel, buffer);
		}

		// Token: 0x0600007A RID: 122 RVA: 0x00002F9C File Offset: 0x0000119C
		public bool TryGetHapticCapabilities(out HapticCapabilities capabilities)
		{
			return InputTracking.TryGetHapticCapabilities(this.m_DeviceId, out capabilities);
		}

		// Token: 0x0600007B RID: 123 RVA: 0x00002FBD File Offset: 0x000011BD
		public void StopHaptics()
		{
			InputTracking.StopHaptics(this.m_DeviceId);
		}

		// Token: 0x0600007C RID: 124 RVA: 0x00002FCC File Offset: 0x000011CC
		public override bool Equals(object obj)
		{
			return obj is InputDevice && this.Equals((InputDevice)obj);
		}

		// Token: 0x0600007D RID: 125 RVA: 0x00003000 File Offset: 0x00001200
		public bool Equals(InputDevice other)
		{
			return this.m_DeviceId == other.m_DeviceId;
		}

		// Token: 0x0600007E RID: 126 RVA: 0x00003024 File Offset: 0x00001224
		public override int GetHashCode()
		{
			return this.m_DeviceId.GetHashCode();
		}

		// Token: 0x0600007F RID: 127 RVA: 0x0000304C File Offset: 0x0000124C
		public static bool operator ==(InputDevice a, InputDevice b)
		{
			return a.Equals(b);
		}

		// Token: 0x06000080 RID: 128 RVA: 0x0000306C File Offset: 0x0000126C
		public static bool operator !=(InputDevice a, InputDevice b)
		{
			return !(a == b);
		}

		// Token: 0x04000037 RID: 55
		private ulong m_DeviceId;
	}
}
