﻿using System;

namespace UnityEngine.XR
{
	// Token: 0x02000014 RID: 20
	public enum XRNode
	{
		// Token: 0x04000018 RID: 24
		LeftEye,
		// Token: 0x04000019 RID: 25
		RightEye,
		// Token: 0x0400001A RID: 26
		CenterEye,
		// Token: 0x0400001B RID: 27
		Head,
		// Token: 0x0400001C RID: 28
		LeftHand,
		// Token: 0x0400001D RID: 29
		RightHand,
		// Token: 0x0400001E RID: 30
		GameController,
		// Token: 0x0400001F RID: 31
		TrackingReference,
		// Token: 0x04000020 RID: 32
		HardwareTracker
	}
}
