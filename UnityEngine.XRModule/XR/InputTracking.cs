﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.XR
{
	// Token: 0x02000012 RID: 18
	[StaticAccessor("XRInputTrackingFacade::Get()", StaticAccessorType.Dot)]
	[NativeConditional("ENABLE_VR")]
	[NativeHeader("Modules/XR/Subsystems/Input/Public/XRInputTrackingFacade.h")]
	[RequiredByNativeCode]
	public static class InputTracking
	{
		// Token: 0x14000001 RID: 1
		// (add) Token: 0x0600003B RID: 59 RVA: 0x00002860 File Offset: 0x00000A60
		// (remove) Token: 0x0600003C RID: 60 RVA: 0x00002894 File Offset: 0x00000A94
		public static event Action<XRNodeState> trackingAcquired
		{
			add
			{
				Action<XRNodeState> action = InputTracking.trackingAcquired;
				Action<XRNodeState> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<XRNodeState>>(ref InputTracking.trackingAcquired, (Action<XRNodeState>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<XRNodeState> action = InputTracking.trackingAcquired;
				Action<XRNodeState> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<XRNodeState>>(ref InputTracking.trackingAcquired, (Action<XRNodeState>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x14000002 RID: 2
		// (add) Token: 0x0600003D RID: 61 RVA: 0x000028C8 File Offset: 0x00000AC8
		// (remove) Token: 0x0600003E RID: 62 RVA: 0x000028FC File Offset: 0x00000AFC
		public static event Action<XRNodeState> trackingLost
		{
			add
			{
				Action<XRNodeState> action = InputTracking.trackingLost;
				Action<XRNodeState> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<XRNodeState>>(ref InputTracking.trackingLost, (Action<XRNodeState>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<XRNodeState> action = InputTracking.trackingLost;
				Action<XRNodeState> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<XRNodeState>>(ref InputTracking.trackingLost, (Action<XRNodeState>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x14000003 RID: 3
		// (add) Token: 0x0600003F RID: 63 RVA: 0x00002930 File Offset: 0x00000B30
		// (remove) Token: 0x06000040 RID: 64 RVA: 0x00002964 File Offset: 0x00000B64
		public static event Action<XRNodeState> nodeAdded
		{
			add
			{
				Action<XRNodeState> action = InputTracking.nodeAdded;
				Action<XRNodeState> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<XRNodeState>>(ref InputTracking.nodeAdded, (Action<XRNodeState>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<XRNodeState> action = InputTracking.nodeAdded;
				Action<XRNodeState> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<XRNodeState>>(ref InputTracking.nodeAdded, (Action<XRNodeState>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x14000004 RID: 4
		// (add) Token: 0x06000041 RID: 65 RVA: 0x00002998 File Offset: 0x00000B98
		// (remove) Token: 0x06000042 RID: 66 RVA: 0x000029CC File Offset: 0x00000BCC
		public static event Action<XRNodeState> nodeRemoved
		{
			add
			{
				Action<XRNodeState> action = InputTracking.nodeRemoved;
				Action<XRNodeState> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<XRNodeState>>(ref InputTracking.nodeRemoved, (Action<XRNodeState>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<XRNodeState> action = InputTracking.nodeRemoved;
				Action<XRNodeState> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<XRNodeState>>(ref InputTracking.nodeRemoved, (Action<XRNodeState>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x06000043 RID: 67 RVA: 0x00002A00 File Offset: 0x00000C00
		[RequiredByNativeCode]
		private static void InvokeTrackingEvent(InputTracking.TrackingStateEventType eventType, XRNode nodeType, long uniqueID, bool tracked)
		{
			XRNodeState obj = default(XRNodeState);
			obj.uniqueID = (ulong)uniqueID;
			obj.nodeType = nodeType;
			obj.tracked = tracked;
			Action<XRNodeState> action;
			switch (eventType)
			{
			case InputTracking.TrackingStateEventType.NodeAdded:
				action = InputTracking.nodeAdded;
				break;
			case InputTracking.TrackingStateEventType.NodeRemoved:
				action = InputTracking.nodeRemoved;
				break;
			case InputTracking.TrackingStateEventType.TrackingAcquired:
				action = InputTracking.trackingAcquired;
				break;
			case InputTracking.TrackingStateEventType.TrackingLost:
				action = InputTracking.trackingLost;
				break;
			default:
				throw new ArgumentException("TrackingEventHandler - Invalid EventType: " + eventType);
			}
			if (action != null)
			{
				action(obj);
			}
		}

		// Token: 0x06000044 RID: 68 RVA: 0x00002A9C File Offset: 0x00000C9C
		[NativeConditional("ENABLE_VR", "Vector3f::zero")]
		public static Vector3 GetLocalPosition(XRNode node)
		{
			Vector3 result;
			InputTracking.GetLocalPosition_Injected(node, out result);
			return result;
		}

		// Token: 0x06000045 RID: 69 RVA: 0x00002AB4 File Offset: 0x00000CB4
		[NativeConditional("ENABLE_VR", "Quaternionf::identity()")]
		public static Quaternion GetLocalRotation(XRNode node)
		{
			Quaternion result;
			InputTracking.GetLocalRotation_Injected(node, out result);
			return result;
		}

		// Token: 0x06000046 RID: 70
		[NativeConditional("ENABLE_VR")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Recenter();

		// Token: 0x06000047 RID: 71
		[NativeConditional("ENABLE_VR")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string GetNodeName(ulong uniqueId);

		// Token: 0x06000048 RID: 72 RVA: 0x00002ACA File Offset: 0x00000CCA
		public static void GetNodeStates(List<XRNodeState> nodeStates)
		{
			if (nodeStates == null)
			{
				throw new ArgumentNullException("nodeStates");
			}
			nodeStates.Clear();
			InputTracking.GetNodeStates_Internal(nodeStates);
		}

		// Token: 0x06000049 RID: 73
		[NativeConditional("ENABLE_VR && !ENABLE_DOTNET")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetNodeStates_Internal(List<XRNodeState> nodeStates);

		// Token: 0x0600004A RID: 74
		[NativeConditional("ENABLE_VR && ENABLE_DOTNET")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern XRNodeState[] GetNodeStates_Internal_WinRT();

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x0600004B RID: 75
		// (set) Token: 0x0600004C RID: 76
		[NativeConditional("ENABLE_VR")]
		public static extern bool disablePositionalTracking { [NativeName("GetPositionalTrackingDisabled")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeName("SetPositionalTrackingDisabled")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600004D RID: 77
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool SendHapticImpulse(ulong deviceId, uint channel, float amplitude, float duration);

		// Token: 0x0600004E RID: 78
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool SendHapticBuffer(ulong deviceId, uint channel, byte[] buffer);

		// Token: 0x0600004F RID: 79
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool TryGetHapticCapabilities(ulong deviceId, out HapticCapabilities capabilities);

		// Token: 0x06000050 RID: 80
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void StopHaptics(ulong deviceId);

		// Token: 0x06000051 RID: 81
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool IsDeviceValid(ulong deviceId);

		// Token: 0x06000052 RID: 82
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern ulong GetDeviceIdAtXRNode(XRNode node);

		// Token: 0x06000053 RID: 83 RVA: 0x00002AEB File Offset: 0x00000CEB
		// Note: this type is marked as 'beforefieldinit'.
		static InputTracking()
		{
		}

		// Token: 0x06000054 RID: 84
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetLocalPosition_Injected(XRNode node, out Vector3 ret);

		// Token: 0x06000055 RID: 85
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetLocalRotation_Injected(XRNode node, out Quaternion ret);

		// Token: 0x0400000E RID: 14
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static Action<XRNodeState> trackingAcquired = null;

		// Token: 0x0400000F RID: 15
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private static Action<XRNodeState> trackingLost = null;

		// Token: 0x04000010 RID: 16
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private static Action<XRNodeState> nodeAdded = null;

		// Token: 0x04000011 RID: 17
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private static Action<XRNodeState> nodeRemoved = null;

		// Token: 0x02000013 RID: 19
		private enum TrackingStateEventType
		{
			// Token: 0x04000013 RID: 19
			NodeAdded,
			// Token: 0x04000014 RID: 20
			NodeRemoved,
			// Token: 0x04000015 RID: 21
			TrackingAcquired,
			// Token: 0x04000016 RID: 22
			TrackingLost
		}
	}
}
