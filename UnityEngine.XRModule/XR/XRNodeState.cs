﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.XR
{
	// Token: 0x02000016 RID: 22
	[UsedByNativeCode]
	public struct XRNodeState
	{
		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000056 RID: 86 RVA: 0x00002B08 File Offset: 0x00000D08
		// (set) Token: 0x06000057 RID: 87 RVA: 0x00002B23 File Offset: 0x00000D23
		public ulong uniqueID
		{
			get
			{
				return this.m_UniqueID;
			}
			set
			{
				this.m_UniqueID = value;
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000058 RID: 88 RVA: 0x00002B30 File Offset: 0x00000D30
		// (set) Token: 0x06000059 RID: 89 RVA: 0x00002B4B File Offset: 0x00000D4B
		public XRNode nodeType
		{
			get
			{
				return this.m_Type;
			}
			set
			{
				this.m_Type = value;
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x0600005A RID: 90 RVA: 0x00002B58 File Offset: 0x00000D58
		// (set) Token: 0x0600005B RID: 91 RVA: 0x00002B76 File Offset: 0x00000D76
		public bool tracked
		{
			get
			{
				return this.m_Tracked == 1;
			}
			set
			{
				this.m_Tracked = ((!value) ? 0 : 1);
			}
		}

		// Token: 0x1700000D RID: 13
		// (set) Token: 0x0600005C RID: 92 RVA: 0x00002B8C File Offset: 0x00000D8C
		public Vector3 position
		{
			set
			{
				this.m_Position = value;
				this.m_AvailableFields |= AvailableTrackingData.PositionAvailable;
			}
		}

		// Token: 0x1700000E RID: 14
		// (set) Token: 0x0600005D RID: 93 RVA: 0x00002BA4 File Offset: 0x00000DA4
		public Quaternion rotation
		{
			set
			{
				this.m_Rotation = value;
				this.m_AvailableFields |= AvailableTrackingData.RotationAvailable;
			}
		}

		// Token: 0x1700000F RID: 15
		// (set) Token: 0x0600005E RID: 94 RVA: 0x00002BBC File Offset: 0x00000DBC
		public Vector3 velocity
		{
			set
			{
				this.m_Velocity = value;
				this.m_AvailableFields |= AvailableTrackingData.VelocityAvailable;
			}
		}

		// Token: 0x17000010 RID: 16
		// (set) Token: 0x0600005F RID: 95 RVA: 0x00002BD4 File Offset: 0x00000DD4
		public Vector3 angularVelocity
		{
			set
			{
				this.m_AngularVelocity = value;
				this.m_AvailableFields |= AvailableTrackingData.AngularVelocityAvailable;
			}
		}

		// Token: 0x17000011 RID: 17
		// (set) Token: 0x06000060 RID: 96 RVA: 0x00002BEC File Offset: 0x00000DEC
		public Vector3 acceleration
		{
			set
			{
				this.m_Acceleration = value;
				this.m_AvailableFields |= AvailableTrackingData.AccelerationAvailable;
			}
		}

		// Token: 0x17000012 RID: 18
		// (set) Token: 0x06000061 RID: 97 RVA: 0x00002C05 File Offset: 0x00000E05
		public Vector3 angularAcceleration
		{
			set
			{
				this.m_AngularAcceleration = value;
				this.m_AvailableFields |= AvailableTrackingData.AngularAccelerationAvailable;
			}
		}

		// Token: 0x06000062 RID: 98 RVA: 0x00002C20 File Offset: 0x00000E20
		public bool TryGetPosition(out Vector3 position)
		{
			return this.TryGet<Vector3>(this.m_Position, AvailableTrackingData.PositionAvailable, out position);
		}

		// Token: 0x06000063 RID: 99 RVA: 0x00002C44 File Offset: 0x00000E44
		public bool TryGetRotation(out Quaternion rotation)
		{
			return this.TryGet<Quaternion>(this.m_Rotation, AvailableTrackingData.RotationAvailable, out rotation);
		}

		// Token: 0x06000064 RID: 100 RVA: 0x00002C68 File Offset: 0x00000E68
		public bool TryGetVelocity(out Vector3 velocity)
		{
			return this.TryGet<Vector3>(this.m_Velocity, AvailableTrackingData.VelocityAvailable, out velocity);
		}

		// Token: 0x06000065 RID: 101 RVA: 0x00002C8C File Offset: 0x00000E8C
		public bool TryGetAngularVelocity(out Vector3 angularVelocity)
		{
			return this.TryGet<Vector3>(this.m_AngularVelocity, AvailableTrackingData.AngularVelocityAvailable, out angularVelocity);
		}

		// Token: 0x06000066 RID: 102 RVA: 0x00002CB0 File Offset: 0x00000EB0
		public bool TryGetAcceleration(out Vector3 acceleration)
		{
			return this.TryGet<Vector3>(this.m_Acceleration, AvailableTrackingData.AccelerationAvailable, out acceleration);
		}

		// Token: 0x06000067 RID: 103 RVA: 0x00002CD4 File Offset: 0x00000ED4
		public bool TryGetAngularAcceleration(out Vector3 angularAcceleration)
		{
			return this.TryGet<Vector3>(this.m_AngularAcceleration, AvailableTrackingData.AngularAccelerationAvailable, out angularAcceleration);
		}

		// Token: 0x06000068 RID: 104 RVA: 0x00002CF8 File Offset: 0x00000EF8
		private bool TryGet<T>(T inValue, AvailableTrackingData availabilityFlag, out T outValue) where T : new()
		{
			bool result;
			if (this.m_Tracked == 1 && (this.m_AvailableFields & availabilityFlag) > AvailableTrackingData.None)
			{
				outValue = inValue;
				result = true;
			}
			else
			{
				outValue = Activator.CreateInstance<T>();
				result = false;
			}
			return result;
		}

		// Token: 0x04000029 RID: 41
		private XRNode m_Type;

		// Token: 0x0400002A RID: 42
		private AvailableTrackingData m_AvailableFields;

		// Token: 0x0400002B RID: 43
		private Vector3 m_Position;

		// Token: 0x0400002C RID: 44
		private Quaternion m_Rotation;

		// Token: 0x0400002D RID: 45
		private Vector3 m_Velocity;

		// Token: 0x0400002E RID: 46
		private Vector3 m_AngularVelocity;

		// Token: 0x0400002F RID: 47
		private Vector3 m_Acceleration;

		// Token: 0x04000030 RID: 48
		private Vector3 m_AngularAcceleration;

		// Token: 0x04000031 RID: 49
		private int m_Tracked;

		// Token: 0x04000032 RID: 50
		private ulong m_UniqueID;
	}
}
