﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine.XR
{
	// Token: 0x02000019 RID: 25
	[NativeConditional("ENABLE_VR")]
	[StaticAccessor("XRInputTrackingFacade::Get()", StaticAccessorType.Dot)]
	[NativeHeader("Modules/XR/Subsystems/Input/Public/XRInputTrackingFacade.h")]
	public class InputDevices
	{
		// Token: 0x06000081 RID: 129 RVA: 0x0000222F File Offset: 0x0000042F
		public InputDevices()
		{
		}

		// Token: 0x06000082 RID: 130 RVA: 0x0000308C File Offset: 0x0000128C
		[NativeConditional("ENABLE_VR", "InputDevice::Identity")]
		public static InputDevice GetDeviceAtXRNode(XRNode node)
		{
			ulong deviceIdAtXRNode = InputTracking.GetDeviceIdAtXRNode(node);
			return new InputDevice(deviceIdAtXRNode);
		}
	}
}
