﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine.XR
{
	// Token: 0x02000017 RID: 23
	[NativeConditional("ENABLE_VR")]
	public struct HapticCapabilities : IEquatable<HapticCapabilities>
	{
		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000069 RID: 105 RVA: 0x00002D44 File Offset: 0x00000F44
		// (set) Token: 0x0600006A RID: 106 RVA: 0x00002D5F File Offset: 0x00000F5F
		public uint numChannels
		{
			get
			{
				return this.m_NumChannels;
			}
			internal set
			{
				this.m_NumChannels = value;
			}
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x0600006B RID: 107 RVA: 0x00002D6C File Offset: 0x00000F6C
		// (set) Token: 0x0600006C RID: 108 RVA: 0x00002D87 File Offset: 0x00000F87
		public bool supportsImpulse
		{
			get
			{
				return this.m_SupportsImpulse;
			}
			internal set
			{
				this.m_SupportsImpulse = value;
			}
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x0600006D RID: 109 RVA: 0x00002D94 File Offset: 0x00000F94
		// (set) Token: 0x0600006E RID: 110 RVA: 0x00002DAF File Offset: 0x00000FAF
		public bool supportsBuffer
		{
			get
			{
				return this.m_SupportsBuffer;
			}
			internal set
			{
				this.m_SupportsBuffer = value;
			}
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x0600006F RID: 111 RVA: 0x00002DBC File Offset: 0x00000FBC
		// (set) Token: 0x06000070 RID: 112 RVA: 0x00002DD7 File Offset: 0x00000FD7
		public uint bufferFrequencyHz
		{
			get
			{
				return this.m_BufferFrequencyHz;
			}
			internal set
			{
				this.m_BufferFrequencyHz = value;
			}
		}

		// Token: 0x06000071 RID: 113 RVA: 0x00002DE4 File Offset: 0x00000FE4
		public override bool Equals(object obj)
		{
			return obj is HapticCapabilities && this.Equals((HapticCapabilities)obj);
		}

		// Token: 0x06000072 RID: 114 RVA: 0x00002E18 File Offset: 0x00001018
		public bool Equals(HapticCapabilities other)
		{
			return this.numChannels == other.numChannels && this.supportsImpulse == other.supportsImpulse && this.supportsBuffer == other.supportsBuffer && this.bufferFrequencyHz == other.bufferFrequencyHz;
		}

		// Token: 0x06000073 RID: 115 RVA: 0x00002E78 File Offset: 0x00001078
		public override int GetHashCode()
		{
			return this.numChannels.GetHashCode() ^ this.supportsImpulse.GetHashCode() << 1 ^ this.supportsBuffer.GetHashCode() >> 1 ^ this.bufferFrequencyHz.GetHashCode() << 2;
		}

		// Token: 0x06000074 RID: 116 RVA: 0x00002EE8 File Offset: 0x000010E8
		public static bool operator ==(HapticCapabilities a, HapticCapabilities b)
		{
			return a.Equals(b);
		}

		// Token: 0x06000075 RID: 117 RVA: 0x00002F08 File Offset: 0x00001108
		public static bool operator !=(HapticCapabilities a, HapticCapabilities b)
		{
			return !(a == b);
		}

		// Token: 0x04000033 RID: 51
		private uint m_NumChannels;

		// Token: 0x04000034 RID: 52
		private bool m_SupportsImpulse;

		// Token: 0x04000035 RID: 53
		private bool m_SupportsBuffer;

		// Token: 0x04000036 RID: 54
		private uint m_BufferFrequencyHz;
	}
}
