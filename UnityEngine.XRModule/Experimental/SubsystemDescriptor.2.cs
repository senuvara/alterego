﻿using System;

namespace UnityEngine.Experimental
{
	// Token: 0x02000009 RID: 9
	public class SubsystemDescriptor<TSubsystem> : SubsystemDescriptor where TSubsystem : Subsystem
	{
		// Token: 0x06000017 RID: 23 RVA: 0x00002317 File Offset: 0x00000517
		public SubsystemDescriptor()
		{
		}

		// Token: 0x06000018 RID: 24 RVA: 0x00002320 File Offset: 0x00000520
		public TSubsystem Create()
		{
			TSubsystem tsubsystem = Activator.CreateInstance(base.subsystemImplementationType) as TSubsystem;
			tsubsystem.m_subsystemDescriptor = this;
			Internal_SubsystemInstances.Internal_AddStandaloneSubsystem(tsubsystem);
			return tsubsystem;
		}
	}
}
