﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental
{
	// Token: 0x02000007 RID: 7
	public abstract class SubsystemDescriptor : ISubsystemDescriptor
	{
		// Token: 0x06000010 RID: 16 RVA: 0x0000222F File Offset: 0x0000042F
		protected SubsystemDescriptor()
		{
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000011 RID: 17 RVA: 0x00002280 File Offset: 0x00000480
		// (set) Token: 0x06000012 RID: 18 RVA: 0x0000229A File Offset: 0x0000049A
		public string id
		{
			[CompilerGenerated]
			get
			{
				return this.<id>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<id>k__BackingField = value;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000013 RID: 19 RVA: 0x000022A4 File Offset: 0x000004A4
		// (set) Token: 0x06000014 RID: 20 RVA: 0x000022BE File Offset: 0x000004BE
		public Type subsystemImplementationType
		{
			[CompilerGenerated]
			get
			{
				return this.<subsystemImplementationType>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<subsystemImplementationType>k__BackingField = value;
			}
		}

		// Token: 0x04000005 RID: 5
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <id>k__BackingField;

		// Token: 0x04000006 RID: 6
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Type <subsystemImplementationType>k__BackingField;
	}
}
