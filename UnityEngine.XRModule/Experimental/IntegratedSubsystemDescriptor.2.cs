﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental
{
	// Token: 0x02000008 RID: 8
	[NativeType(Header = "Modules/XR/XRSubsystemDescriptor.h")]
	[UsedByNativeCode("XRSubsystemDescriptor")]
	[StructLayout(LayoutKind.Sequential)]
	public class IntegratedSubsystemDescriptor<TSubsystem> : IntegratedSubsystemDescriptor where TSubsystem : IntegratedSubsystem
	{
		// Token: 0x06000015 RID: 21 RVA: 0x000022C7 File Offset: 0x000004C7
		public IntegratedSubsystemDescriptor()
		{
		}

		// Token: 0x06000016 RID: 22 RVA: 0x000022D0 File Offset: 0x000004D0
		public TSubsystem Create()
		{
			IntPtr ptr = Internal_SubsystemDescriptors.Create(this.m_Ptr);
			TSubsystem tsubsystem = (TSubsystem)((object)Internal_SubsystemInstances.Internal_GetInstanceByPtr(ptr));
			if (tsubsystem != null)
			{
				tsubsystem.m_subsystemDescriptor = this;
			}
			return tsubsystem;
		}
	}
}
