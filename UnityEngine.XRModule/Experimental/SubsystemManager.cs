﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.Experimental
{
	// Token: 0x0200000C RID: 12
	[NativeType(Header = "Modules/XR/XRSubsystemManager.h")]
	public static class SubsystemManager
	{
		// Token: 0x06000025 RID: 37 RVA: 0x00002636 File Offset: 0x00000836
		static SubsystemManager()
		{
			SubsystemManager.StaticConstructScriptingClassMap();
		}

		// Token: 0x06000026 RID: 38
		[NativeConditional("ENABLE_XR")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void ReportSingleSubsystemAnalytics(string id);

		// Token: 0x06000027 RID: 39 RVA: 0x00002640 File Offset: 0x00000840
		public static void GetSubsystemDescriptors<T>(List<T> descriptors) where T : ISubsystemDescriptor
		{
			descriptors.Clear();
			foreach (ISubsystemDescriptorImpl subsystemDescriptorImpl in Internal_SubsystemDescriptors.s_IntegratedSubsystemDescriptors)
			{
				if (subsystemDescriptorImpl is T)
				{
					descriptors.Add((T)((object)subsystemDescriptorImpl));
				}
			}
			foreach (ISubsystemDescriptor subsystemDescriptor in Internal_SubsystemDescriptors.s_StandaloneSubsystemDescriptors)
			{
				if (subsystemDescriptor is T)
				{
					descriptors.Add((T)((object)subsystemDescriptor));
				}
			}
		}

		// Token: 0x06000028 RID: 40 RVA: 0x00002714 File Offset: 0x00000914
		public static void GetInstances<T>(List<T> instances) where T : ISubsystem
		{
			instances.Clear();
			foreach (ISubsystem subsystem in Internal_SubsystemInstances.s_IntegratedSubsystemInstances)
			{
				if (subsystem is T)
				{
					instances.Add((T)((object)subsystem));
				}
			}
			foreach (ISubsystem subsystem2 in Internal_SubsystemInstances.s_StandaloneSubsystemInstances)
			{
				if (subsystem2 is T)
				{
					instances.Add((T)((object)subsystem2));
				}
			}
		}

		// Token: 0x06000029 RID: 41
		[NativeConditional("ENABLE_XR")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void DestroyInstance_Internal(IntPtr instancePtr);

		// Token: 0x0600002A RID: 42
		[NativeConditional("ENABLE_XR")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void StaticConstructScriptingClassMap();
	}
}
