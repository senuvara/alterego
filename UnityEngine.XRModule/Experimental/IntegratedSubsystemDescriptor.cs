﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental
{
	// Token: 0x02000006 RID: 6
	[UsedByNativeCode("XRSubsystemDescriptorBase")]
	[StructLayout(LayoutKind.Sequential)]
	public class IntegratedSubsystemDescriptor : ISubsystemDescriptor, ISubsystemDescriptorImpl
	{
		// Token: 0x0600000C RID: 12 RVA: 0x0000222F File Offset: 0x0000042F
		public IntegratedSubsystemDescriptor()
		{
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x0600000D RID: 13 RVA: 0x00002238 File Offset: 0x00000438
		public string id
		{
			get
			{
				return Internal_SubsystemDescriptors.GetId(this.m_Ptr);
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x0600000E RID: 14 RVA: 0x00002258 File Offset: 0x00000458
		// (set) Token: 0x0600000F RID: 15 RVA: 0x00002273 File Offset: 0x00000473
		IntPtr ISubsystemDescriptorImpl.ptr
		{
			get
			{
				return this.m_Ptr;
			}
			set
			{
				this.m_Ptr = value;
			}
		}

		// Token: 0x04000004 RID: 4
		internal IntPtr m_Ptr;
	}
}
