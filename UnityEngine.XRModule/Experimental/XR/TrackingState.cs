﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x0200003D RID: 61
	[UsedByNativeCode]
	public enum TrackingState
	{
		// Token: 0x0400007F RID: 127
		Unknown,
		// Token: 0x04000080 RID: 128
		Tracking,
		// Token: 0x04000081 RID: 129
		Unavailable
	}
}
