﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x02000033 RID: 51
	[NativeHeader("Modules/XR/XRPrefix.h")]
	[NativeConditional("ENABLE_XR")]
	[NativeHeader("Modules/XR/Subsystems/Planes/XRPlaneSubsystem.h")]
	[UsedByNativeCode]
	public class XRPlaneSubsystem : IntegratedSubsystem<XRPlaneSubsystemDescriptor>
	{
		// Token: 0x060000FA RID: 250 RVA: 0x00003A77 File Offset: 0x00001C77
		public XRPlaneSubsystem()
		{
		}

		// Token: 0x14000007 RID: 7
		// (add) Token: 0x060000FB RID: 251 RVA: 0x00003A80 File Offset: 0x00001C80
		// (remove) Token: 0x060000FC RID: 252 RVA: 0x00003AB8 File Offset: 0x00001CB8
		public event Action<PlaneAddedEventArgs> PlaneAdded
		{
			add
			{
				Action<PlaneAddedEventArgs> action = this.PlaneAdded;
				Action<PlaneAddedEventArgs> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<PlaneAddedEventArgs>>(ref this.PlaneAdded, (Action<PlaneAddedEventArgs>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<PlaneAddedEventArgs> action = this.PlaneAdded;
				Action<PlaneAddedEventArgs> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<PlaneAddedEventArgs>>(ref this.PlaneAdded, (Action<PlaneAddedEventArgs>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x14000008 RID: 8
		// (add) Token: 0x060000FD RID: 253 RVA: 0x00003AF0 File Offset: 0x00001CF0
		// (remove) Token: 0x060000FE RID: 254 RVA: 0x00003B28 File Offset: 0x00001D28
		public event Action<PlaneUpdatedEventArgs> PlaneUpdated
		{
			add
			{
				Action<PlaneUpdatedEventArgs> action = this.PlaneUpdated;
				Action<PlaneUpdatedEventArgs> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<PlaneUpdatedEventArgs>>(ref this.PlaneUpdated, (Action<PlaneUpdatedEventArgs>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<PlaneUpdatedEventArgs> action = this.PlaneUpdated;
				Action<PlaneUpdatedEventArgs> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<PlaneUpdatedEventArgs>>(ref this.PlaneUpdated, (Action<PlaneUpdatedEventArgs>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x14000009 RID: 9
		// (add) Token: 0x060000FF RID: 255 RVA: 0x00003B60 File Offset: 0x00001D60
		// (remove) Token: 0x06000100 RID: 256 RVA: 0x00003B98 File Offset: 0x00001D98
		public event Action<PlaneRemovedEventArgs> PlaneRemoved
		{
			add
			{
				Action<PlaneRemovedEventArgs> action = this.PlaneRemoved;
				Action<PlaneRemovedEventArgs> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<PlaneRemovedEventArgs>>(ref this.PlaneRemoved, (Action<PlaneRemovedEventArgs>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<PlaneRemovedEventArgs> action = this.PlaneRemoved;
				Action<PlaneRemovedEventArgs> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<PlaneRemovedEventArgs>>(ref this.PlaneRemoved, (Action<PlaneRemovedEventArgs>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x06000101 RID: 257
		public extern int LastUpdatedFrame { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000102 RID: 258 RVA: 0x00003BCE File Offset: 0x00001DCE
		public bool TryGetPlane(TrackableId planeId, out BoundedPlane plane)
		{
			return this.TryGetPlane_Injected(ref planeId, out plane);
		}

		// Token: 0x06000103 RID: 259 RVA: 0x00003BD9 File Offset: 0x00001DD9
		public void GetAllPlanes(List<BoundedPlane> planesOut)
		{
			if (planesOut == null)
			{
				throw new ArgumentNullException("planesOut");
			}
			this.GetAllPlanesAsList(planesOut);
		}

		// Token: 0x06000104 RID: 260 RVA: 0x00003BF4 File Offset: 0x00001DF4
		public bool TryGetPlaneBoundary(TrackableId planeId, List<Vector3> boundaryOut)
		{
			if (boundaryOut == null)
			{
				throw new ArgumentNullException("boundaryOut");
			}
			return this.Internal_GetBoundaryAsList(planeId, boundaryOut);
		}

		// Token: 0x06000105 RID: 261 RVA: 0x00003C24 File Offset: 0x00001E24
		[RequiredByNativeCode]
		private void InvokePlaneAddedEvent(BoundedPlane plane)
		{
			if (this.PlaneAdded != null)
			{
				this.PlaneAdded(new PlaneAddedEventArgs
				{
					PlaneSubsystem = this,
					Plane = plane
				});
			}
		}

		// Token: 0x06000106 RID: 262 RVA: 0x00003C64 File Offset: 0x00001E64
		[RequiredByNativeCode]
		private void InvokePlaneUpdatedEvent(BoundedPlane plane)
		{
			if (this.PlaneUpdated != null)
			{
				this.PlaneUpdated(new PlaneUpdatedEventArgs
				{
					PlaneSubsystem = this,
					Plane = plane
				});
			}
		}

		// Token: 0x06000107 RID: 263 RVA: 0x00003CA4 File Offset: 0x00001EA4
		[RequiredByNativeCode]
		private void InvokePlaneRemovedEvent(BoundedPlane removedPlane)
		{
			if (this.PlaneRemoved != null)
			{
				this.PlaneRemoved(new PlaneRemovedEventArgs
				{
					PlaneSubsystem = this,
					Plane = removedPlane
				});
			}
		}

		// Token: 0x06000108 RID: 264
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern BoundedPlane[] GetAllPlanesAsFixedArray();

		// Token: 0x06000109 RID: 265
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetAllPlanesAsList(List<BoundedPlane> planes);

		// Token: 0x0600010A RID: 266 RVA: 0x00003CE3 File Offset: 0x00001EE3
		private bool Internal_GetBoundaryAsList(TrackableId planeId, List<Vector3> boundaryOut)
		{
			return this.Internal_GetBoundaryAsList_Injected(ref planeId, boundaryOut);
		}

		// Token: 0x0600010B RID: 267 RVA: 0x00003CEE File Offset: 0x00001EEE
		private Vector3[] Internal_GetBoundaryAsFixedArray(TrackableId planeId)
		{
			return this.Internal_GetBoundaryAsFixedArray_Injected(ref planeId);
		}

		// Token: 0x0600010C RID: 268
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool TryGetPlane_Injected(ref TrackableId planeId, out BoundedPlane plane);

		// Token: 0x0600010D RID: 269
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool Internal_GetBoundaryAsList_Injected(ref TrackableId planeId, List<Vector3> boundaryOut);

		// Token: 0x0600010E RID: 270
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Vector3[] Internal_GetBoundaryAsFixedArray_Injected(ref TrackableId planeId);

		// Token: 0x04000067 RID: 103
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Action<PlaneAddedEventArgs> PlaneAdded;

		// Token: 0x04000068 RID: 104
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Action<PlaneUpdatedEventArgs> PlaneUpdated;

		// Token: 0x04000069 RID: 105
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Action<PlaneRemovedEventArgs> PlaneRemoved;
	}
}
