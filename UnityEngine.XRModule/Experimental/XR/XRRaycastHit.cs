﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x02000036 RID: 54
	[NativeHeader("Modules/XR/Subsystems/Raycast/XRRaycastSubsystem.h")]
	[UsedByNativeCode]
	public struct XRRaycastHit
	{
		// Token: 0x17000043 RID: 67
		// (get) Token: 0x06000110 RID: 272 RVA: 0x00003D00 File Offset: 0x00001F00
		// (set) Token: 0x06000111 RID: 273 RVA: 0x00003D1A File Offset: 0x00001F1A
		public TrackableId TrackableId
		{
			[CompilerGenerated]
			get
			{
				return this.<TrackableId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<TrackableId>k__BackingField = value;
			}
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x06000112 RID: 274 RVA: 0x00003D24 File Offset: 0x00001F24
		// (set) Token: 0x06000113 RID: 275 RVA: 0x00003D3E File Offset: 0x00001F3E
		public Pose Pose
		{
			[CompilerGenerated]
			get
			{
				return this.<Pose>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Pose>k__BackingField = value;
			}
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x06000114 RID: 276 RVA: 0x00003D48 File Offset: 0x00001F48
		// (set) Token: 0x06000115 RID: 277 RVA: 0x00003D62 File Offset: 0x00001F62
		public float Distance
		{
			[CompilerGenerated]
			get
			{
				return this.<Distance>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Distance>k__BackingField = value;
			}
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x06000116 RID: 278 RVA: 0x00003D6C File Offset: 0x00001F6C
		// (set) Token: 0x06000117 RID: 279 RVA: 0x00003D86 File Offset: 0x00001F86
		public TrackableType HitType
		{
			[CompilerGenerated]
			get
			{
				return this.<HitType>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<HitType>k__BackingField = value;
			}
		}

		// Token: 0x04000073 RID: 115
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private TrackableId <TrackableId>k__BackingField;

		// Token: 0x04000074 RID: 116
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Pose <Pose>k__BackingField;

		// Token: 0x04000075 RID: 117
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private float <Distance>k__BackingField;

		// Token: 0x04000076 RID: 118
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private TrackableType <HitType>k__BackingField;
	}
}
