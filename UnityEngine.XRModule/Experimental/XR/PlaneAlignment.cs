﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x0200002E RID: 46
	[Flags]
	[UsedByNativeCode]
	public enum PlaneAlignment
	{
		// Token: 0x04000057 RID: 87
		Horizontal = 1,
		// Token: 0x04000058 RID: 88
		Vertical = 2,
		// Token: 0x04000059 RID: 89
		NonAxis = 4
	}
}
