﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x02000037 RID: 55
	[NativeConditional("ENABLE_XR")]
	[NativeHeader("Modules/XR/Subsystems/Raycast/XRRaycastSubsystem.h")]
	[NativeHeader("Modules/XR/XRPrefix.h")]
	[UsedByNativeCode]
	public class XRRaycastSubsystem : IntegratedSubsystem<XRRaycastSubsystemDescriptor>
	{
		// Token: 0x06000118 RID: 280 RVA: 0x00003D8F File Offset: 0x00001F8F
		public XRRaycastSubsystem()
		{
		}

		// Token: 0x06000119 RID: 281 RVA: 0x00003D98 File Offset: 0x00001F98
		public bool Raycast(Vector3 screenPoint, List<XRRaycastHit> hitResults, TrackableType trackableTypeMask = TrackableType.All)
		{
			if (hitResults == null)
			{
				throw new ArgumentNullException("hitResults");
			}
			float screenX = Mathf.Clamp01(screenPoint.x / (float)Screen.width);
			float screenY = Mathf.Clamp01(screenPoint.y / (float)Screen.height);
			return this.Internal_ScreenRaycastAsList(screenX, screenY, trackableTypeMask, hitResults);
		}

		// Token: 0x0600011A RID: 282 RVA: 0x00003DF0 File Offset: 0x00001FF0
		public static void Raycast(Ray ray, XRDepthSubsystem depthSubsystem, XRPlaneSubsystem planeSubsystem, List<XRRaycastHit> hitResults, TrackableType trackableTypeMask = TrackableType.All, float pointCloudRaycastAngleInDegrees = 5f)
		{
			if (hitResults == null)
			{
				throw new ArgumentNullException("hitResults");
			}
			IntPtr depthSubsystem2 = (depthSubsystem != null) ? depthSubsystem.m_Ptr : IntPtr.Zero;
			IntPtr planeSubsystem2 = (planeSubsystem != null) ? planeSubsystem.m_Ptr : IntPtr.Zero;
			XRRaycastSubsystem.Internal_RaycastAsList(ray, pointCloudRaycastAngleInDegrees, depthSubsystem2, planeSubsystem2, trackableTypeMask, hitResults);
		}

		// Token: 0x0600011B RID: 283 RVA: 0x00003E4A File Offset: 0x0000204A
		private static void Internal_RaycastAsList(Ray ray, float pointCloudRaycastAngleInDegrees, IntPtr depthSubsystem, IntPtr planeSubsystem, TrackableType trackableTypeMask, List<XRRaycastHit> hitResultsOut)
		{
			XRRaycastSubsystem.Internal_RaycastAsList_Injected(ref ray, pointCloudRaycastAngleInDegrees, depthSubsystem, planeSubsystem, trackableTypeMask, hitResultsOut);
		}

		// Token: 0x0600011C RID: 284 RVA: 0x00003E5A File Offset: 0x0000205A
		private static XRRaycastHit[] Internal_RaycastAsFixedArray(Ray ray, float pointCloudRaycastAngleInDegrees, IntPtr depthSubsystem, IntPtr planeSubsystem, TrackableType trackableTypeMask)
		{
			return XRRaycastSubsystem.Internal_RaycastAsFixedArray_Injected(ref ray, pointCloudRaycastAngleInDegrees, depthSubsystem, planeSubsystem, trackableTypeMask);
		}

		// Token: 0x0600011D RID: 285
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool Internal_ScreenRaycastAsList(float screenX, float screenY, TrackableType hitMask, List<XRRaycastHit> hitResultsOut);

		// Token: 0x0600011E RID: 286
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern XRRaycastHit[] Internal_ScreenRaycastAsFixedArray(float screenX, float screenY, TrackableType hitMask);

		// Token: 0x0600011F RID: 287
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_RaycastAsList_Injected(ref Ray ray, float pointCloudRaycastAngleInDegrees, IntPtr depthSubsystem, IntPtr planeSubsystem, TrackableType trackableTypeMask, List<XRRaycastHit> hitResultsOut);

		// Token: 0x06000120 RID: 288
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern XRRaycastHit[] Internal_RaycastAsFixedArray_Injected(ref Ray ray, float pointCloudRaycastAngleInDegrees, IntPtr depthSubsystem, IntPtr planeSubsystem, TrackableType trackableTypeMask);
	}
}
