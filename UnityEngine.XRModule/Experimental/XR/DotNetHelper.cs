﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x02000002 RID: 2
	internal static class DotNetHelper
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public static bool TryCopyFixedArrayToList<T>(T[] fixedArrayIn, List<T> listOut)
		{
			bool result;
			if (fixedArrayIn == null)
			{
				result = false;
			}
			else
			{
				int num = fixedArrayIn.Length;
				listOut.Clear();
				if (listOut.Capacity < num)
				{
					listOut.Capacity = num;
				}
				listOut.AddRange(fixedArrayIn);
				result = true;
			}
			return result;
		}
	}
}
