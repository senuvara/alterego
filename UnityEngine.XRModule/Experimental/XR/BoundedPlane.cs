﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x0200002F RID: 47
	[UsedByNativeCode]
	[NativeHeader("Modules/XR/Subsystems/Planes/XRBoundedPlane.h")]
	[NativeHeader("Modules/XR/XRPrefix.h")]
	[NativeHeader("XRScriptingClasses.h")]
	[NativeConditional("ENABLE_XR")]
	public struct BoundedPlane
	{
		// Token: 0x17000032 RID: 50
		// (get) Token: 0x060000D8 RID: 216 RVA: 0x00003734 File Offset: 0x00001934
		// (set) Token: 0x060000D9 RID: 217 RVA: 0x0000374E File Offset: 0x0000194E
		public TrackableId Id
		{
			[CompilerGenerated]
			get
			{
				return this.<Id>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Id>k__BackingField = value;
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x060000DA RID: 218 RVA: 0x00003758 File Offset: 0x00001958
		// (set) Token: 0x060000DB RID: 219 RVA: 0x00003772 File Offset: 0x00001972
		public TrackableId SubsumedById
		{
			[CompilerGenerated]
			get
			{
				return this.<SubsumedById>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<SubsumedById>k__BackingField = value;
			}
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x060000DC RID: 220 RVA: 0x0000377C File Offset: 0x0000197C
		// (set) Token: 0x060000DD RID: 221 RVA: 0x00003796 File Offset: 0x00001996
		public Pose Pose
		{
			[CompilerGenerated]
			get
			{
				return this.<Pose>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Pose>k__BackingField = value;
			}
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x060000DE RID: 222 RVA: 0x000037A0 File Offset: 0x000019A0
		// (set) Token: 0x060000DF RID: 223 RVA: 0x000037BA File Offset: 0x000019BA
		public Vector3 Center
		{
			[CompilerGenerated]
			get
			{
				return this.<Center>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Center>k__BackingField = value;
			}
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x060000E0 RID: 224 RVA: 0x000037C4 File Offset: 0x000019C4
		// (set) Token: 0x060000E1 RID: 225 RVA: 0x000037DE File Offset: 0x000019DE
		public Vector2 Size
		{
			[CompilerGenerated]
			get
			{
				return this.<Size>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Size>k__BackingField = value;
			}
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x060000E2 RID: 226 RVA: 0x000037E8 File Offset: 0x000019E8
		// (set) Token: 0x060000E3 RID: 227 RVA: 0x00003802 File Offset: 0x00001A02
		public PlaneAlignment Alignment
		{
			[CompilerGenerated]
			get
			{
				return this.<Alignment>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Alignment>k__BackingField = value;
			}
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x060000E4 RID: 228 RVA: 0x0000380C File Offset: 0x00001A0C
		public float Width
		{
			get
			{
				return this.Size.x;
			}
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x060000E5 RID: 229 RVA: 0x00003830 File Offset: 0x00001A30
		public float Height
		{
			get
			{
				return this.Size.y;
			}
		}

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x060000E6 RID: 230 RVA: 0x00003854 File Offset: 0x00001A54
		public Vector3 Normal
		{
			get
			{
				return this.Pose.up;
			}
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x060000E7 RID: 231 RVA: 0x00003878 File Offset: 0x00001A78
		public Plane Plane
		{
			get
			{
				return new Plane(this.Normal, this.Center);
			}
		}

		// Token: 0x060000E8 RID: 232 RVA: 0x000038A0 File Offset: 0x00001AA0
		public void GetCorners(out Vector3 p0, out Vector3 p1, out Vector3 p2, out Vector3 p3)
		{
			Vector3 b = this.Pose.right * (this.Width * 0.5f);
			Vector3 b2 = this.Pose.forward * (this.Height * 0.5f);
			p0 = this.Center - b - b2;
			p1 = this.Center - b + b2;
			p2 = this.Center + b + b2;
			p3 = this.Center + b - b2;
		}

		// Token: 0x060000E9 RID: 233 RVA: 0x00003950 File Offset: 0x00001B50
		public bool TryGetBoundary(List<Vector3> boundaryOut)
		{
			if (boundaryOut == null)
			{
				throw new ArgumentNullException("boundaryOut");
			}
			return BoundedPlane.Internal_GetBoundaryAsList(this.m_InstanceId, this.Id, boundaryOut);
		}

		// Token: 0x060000EA RID: 234 RVA: 0x00003988 File Offset: 0x00001B88
		private static Vector3[] Internal_GetBoundaryAsFixedArray(uint instanceId, TrackableId id)
		{
			return BoundedPlane.Internal_GetBoundaryAsFixedArray_Injected(instanceId, ref id);
		}

		// Token: 0x060000EB RID: 235 RVA: 0x00003992 File Offset: 0x00001B92
		private static bool Internal_GetBoundaryAsList(uint instanceId, TrackableId id, List<Vector3> boundaryOut)
		{
			return BoundedPlane.Internal_GetBoundaryAsList_Injected(instanceId, ref id, boundaryOut);
		}

		// Token: 0x060000EC RID: 236
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Vector3[] Internal_GetBoundaryAsFixedArray_Injected(uint instanceId, ref TrackableId id);

		// Token: 0x060000ED RID: 237
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Internal_GetBoundaryAsList_Injected(uint instanceId, ref TrackableId id, List<Vector3> boundaryOut);

		// Token: 0x0400005A RID: 90
		private uint m_InstanceId;

		// Token: 0x0400005B RID: 91
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private TrackableId <Id>k__BackingField;

		// Token: 0x0400005C RID: 92
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private TrackableId <SubsumedById>k__BackingField;

		// Token: 0x0400005D RID: 93
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Pose <Pose>k__BackingField;

		// Token: 0x0400005E RID: 94
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Vector3 <Center>k__BackingField;

		// Token: 0x0400005F RID: 95
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Vector2 <Size>k__BackingField;

		// Token: 0x04000060 RID: 96
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private PlaneAlignment <Alignment>k__BackingField;
	}
}
