﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x02000034 RID: 52
	[UsedByNativeCode]
	[NativeType(Header = "Modules/XR/Subsystems/Planes/XRPlaneSubsystemDescriptor.h")]
	[NativeHeader("Modules/XR/XRPrefix.h")]
	public class XRPlaneSubsystemDescriptor : IntegratedSubsystemDescriptor<XRPlaneSubsystem>
	{
		// Token: 0x0600010F RID: 271 RVA: 0x00003CF8 File Offset: 0x00001EF8
		public XRPlaneSubsystemDescriptor()
		{
		}
	}
}
