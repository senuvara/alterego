﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x02000032 RID: 50
	public struct PlaneRemovedEventArgs
	{
		// Token: 0x17000040 RID: 64
		// (get) Token: 0x060000F6 RID: 246 RVA: 0x00003A30 File Offset: 0x00001C30
		// (set) Token: 0x060000F7 RID: 247 RVA: 0x00003A4A File Offset: 0x00001C4A
		public XRPlaneSubsystem PlaneSubsystem
		{
			[CompilerGenerated]
			get
			{
				return this.<PlaneSubsystem>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<PlaneSubsystem>k__BackingField = value;
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x060000F8 RID: 248 RVA: 0x00003A54 File Offset: 0x00001C54
		// (set) Token: 0x060000F9 RID: 249 RVA: 0x00003A6E File Offset: 0x00001C6E
		public BoundedPlane Plane
		{
			[CompilerGenerated]
			get
			{
				return this.<Plane>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<Plane>k__BackingField = value;
			}
		}

		// Token: 0x04000065 RID: 101
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private XRPlaneSubsystem <PlaneSubsystem>k__BackingField;

		// Token: 0x04000066 RID: 102
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private BoundedPlane <Plane>k__BackingField;
	}
}
