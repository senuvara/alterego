﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x02000003 RID: 3
	[NativeHeader("Modules/XR/XRManagedBindings.h")]
	[UsedByNativeCode]
	public struct TrackableId : IEquatable<TrackableId>
	{
		// Token: 0x06000002 RID: 2 RVA: 0x00002098 File Offset: 0x00000298
		public override string ToString()
		{
			return string.Format("{0}-{1}", this.m_SubId1.ToString("X16"), this.m_SubId2.ToString("X16"));
		}

		// Token: 0x06000003 RID: 3 RVA: 0x000020D8 File Offset: 0x000002D8
		public override int GetHashCode()
		{
			return this.m_SubId1.GetHashCode() ^ this.m_SubId2.GetHashCode();
		}

		// Token: 0x06000004 RID: 4 RVA: 0x00002110 File Offset: 0x00000310
		public override bool Equals(object obj)
		{
			return obj is TrackableId && this.Equals((TrackableId)obj);
		}

		// Token: 0x06000005 RID: 5 RVA: 0x00002140 File Offset: 0x00000340
		public bool Equals(TrackableId other)
		{
			return this.m_SubId1 == other.m_SubId1 && this.m_SubId2 == other.m_SubId2;
		}

		// Token: 0x06000006 RID: 6 RVA: 0x0000217C File Offset: 0x0000037C
		public static bool operator ==(TrackableId id1, TrackableId id2)
		{
			return id1.m_SubId1 == id2.m_SubId1 && id1.m_SubId2 == id2.m_SubId2;
		}

		// Token: 0x06000007 RID: 7 RVA: 0x000021B8 File Offset: 0x000003B8
		public static bool operator !=(TrackableId id1, TrackableId id2)
		{
			return id1.m_SubId1 != id2.m_SubId1 || id1.m_SubId2 != id2.m_SubId2;
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000008 RID: 8 RVA: 0x000021F8 File Offset: 0x000003F8
		public static TrackableId InvalidId
		{
			get
			{
				return TrackableId.s_InvalidId;
			}
		}

		// Token: 0x06000009 RID: 9 RVA: 0x00002214 File Offset: 0x00000414
		// Note: this type is marked as 'beforefieldinit'.
		static TrackableId()
		{
		}

		// Token: 0x04000001 RID: 1
		private static TrackableId s_InvalidId = default(TrackableId);

		// Token: 0x04000002 RID: 2
		private ulong m_SubId1;

		// Token: 0x04000003 RID: 3
		private ulong m_SubId2;
	}
}
