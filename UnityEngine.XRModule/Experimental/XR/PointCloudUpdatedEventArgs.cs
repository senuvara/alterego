﻿using System;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x0200001D RID: 29
	public struct PointCloudUpdatedEventArgs
	{
		// Token: 0x17000022 RID: 34
		// (get) Token: 0x0600009F RID: 159 RVA: 0x000031BC File Offset: 0x000013BC
		public XRDepthSubsystem DepthSubsystem
		{
			get
			{
				return this.m_DepthSubsystem;
			}
		}

		// Token: 0x0400003A RID: 58
		internal XRDepthSubsystem m_DepthSubsystem;
	}
}
