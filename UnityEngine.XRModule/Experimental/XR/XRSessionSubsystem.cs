﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x0200003F RID: 63
	[UsedByNativeCode]
	[NativeHeader("Modules/XR/XRPrefix.h")]
	[NativeHeader("Modules/XR/Subsystems/Session/XRSessionSubsystem.h")]
	[NativeConditional("ENABLE_XR")]
	public class XRSessionSubsystem : IntegratedSubsystem<XRSessionSubsystemDescriptor>
	{
		// Token: 0x06000141 RID: 321 RVA: 0x000040B7 File Offset: 0x000022B7
		public XRSessionSubsystem()
		{
		}

		// Token: 0x1400000B RID: 11
		// (add) Token: 0x06000142 RID: 322 RVA: 0x000040C0 File Offset: 0x000022C0
		// (remove) Token: 0x06000143 RID: 323 RVA: 0x000040F8 File Offset: 0x000022F8
		public event Action<SessionTrackingStateChangedEventArgs> TrackingStateChanged
		{
			add
			{
				Action<SessionTrackingStateChangedEventArgs> action = this.TrackingStateChanged;
				Action<SessionTrackingStateChangedEventArgs> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<SessionTrackingStateChangedEventArgs>>(ref this.TrackingStateChanged, (Action<SessionTrackingStateChangedEventArgs>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<SessionTrackingStateChangedEventArgs> action = this.TrackingStateChanged;
				Action<SessionTrackingStateChangedEventArgs> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<SessionTrackingStateChangedEventArgs>>(ref this.TrackingStateChanged, (Action<SessionTrackingStateChangedEventArgs>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x06000144 RID: 324
		[NativeConditional("ENABLE_XR", StubReturnStatement = "kUnityXRTrackingStateUnknown")]
		public extern TrackingState TrackingState { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x06000145 RID: 325
		public extern int LastUpdatedFrame { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000146 RID: 326 RVA: 0x00004130 File Offset: 0x00002330
		[RequiredByNativeCode]
		private void InvokeTrackingStateChangedEvent(TrackingState newState)
		{
			if (this.TrackingStateChanged != null)
			{
				this.TrackingStateChanged(new SessionTrackingStateChangedEventArgs
				{
					m_Session = this,
					NewState = newState
				});
			}
		}

		// Token: 0x04000084 RID: 132
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Action<SessionTrackingStateChangedEventArgs> TrackingStateChanged;
	}
}
