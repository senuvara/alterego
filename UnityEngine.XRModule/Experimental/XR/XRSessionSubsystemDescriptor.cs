﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x02000040 RID: 64
	[UsedByNativeCode]
	[NativeHeader("Modules/XR/XRPrefix.h")]
	[NativeType(Header = "Modules/XR/Subsystems/Session/XRSessionSubsystemDescriptor.h")]
	public class XRSessionSubsystemDescriptor : IntegratedSubsystemDescriptor<XRSessionSubsystem>
	{
		// Token: 0x06000147 RID: 327 RVA: 0x0000416F File Offset: 0x0000236F
		public XRSessionSubsystemDescriptor()
		{
		}
	}
}
