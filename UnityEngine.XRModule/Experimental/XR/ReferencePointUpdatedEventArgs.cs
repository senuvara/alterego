﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x0200003A RID: 58
	[NativeHeader("Modules/XR/Subsystems/Session/XRSessionSubsystem.h")]
	public struct ReferencePointUpdatedEventArgs
	{
		// Token: 0x1700004A RID: 74
		// (get) Token: 0x06000128 RID: 296 RVA: 0x00003EDC File Offset: 0x000020DC
		// (set) Token: 0x06000129 RID: 297 RVA: 0x00003EF6 File Offset: 0x000020F6
		public ReferencePoint ReferencePoint
		{
			[CompilerGenerated]
			get
			{
				return this.<ReferencePoint>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<ReferencePoint>k__BackingField = value;
			}
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x0600012A RID: 298 RVA: 0x00003F00 File Offset: 0x00002100
		// (set) Token: 0x0600012B RID: 299 RVA: 0x00003F1A File Offset: 0x0000211A
		public TrackingState PreviousTrackingState
		{
			[CompilerGenerated]
			get
			{
				return this.<PreviousTrackingState>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<PreviousTrackingState>k__BackingField = value;
			}
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x0600012C RID: 300 RVA: 0x00003F24 File Offset: 0x00002124
		// (set) Token: 0x0600012D RID: 301 RVA: 0x00003F3E File Offset: 0x0000213E
		public Pose PreviousPose
		{
			[CompilerGenerated]
			get
			{
				return this.<PreviousPose>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<PreviousPose>k__BackingField = value;
			}
		}

		// Token: 0x0400007A RID: 122
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private ReferencePoint <ReferencePoint>k__BackingField;

		// Token: 0x0400007B RID: 123
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private TrackingState <PreviousTrackingState>k__BackingField;

		// Token: 0x0400007C RID: 124
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Pose <PreviousPose>k__BackingField;
	}
}
