﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x02000030 RID: 48
	public struct PlaneAddedEventArgs
	{
		// Token: 0x1700003C RID: 60
		// (get) Token: 0x060000EE RID: 238 RVA: 0x000039A0 File Offset: 0x00001BA0
		// (set) Token: 0x060000EF RID: 239 RVA: 0x000039BA File Offset: 0x00001BBA
		public XRPlaneSubsystem PlaneSubsystem
		{
			[CompilerGenerated]
			get
			{
				return this.<PlaneSubsystem>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<PlaneSubsystem>k__BackingField = value;
			}
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x060000F0 RID: 240 RVA: 0x000039C4 File Offset: 0x00001BC4
		// (set) Token: 0x060000F1 RID: 241 RVA: 0x000039DE File Offset: 0x00001BDE
		public BoundedPlane Plane
		{
			[CompilerGenerated]
			get
			{
				return this.<Plane>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<Plane>k__BackingField = value;
			}
		}

		// Token: 0x04000061 RID: 97
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private XRPlaneSubsystem <PlaneSubsystem>k__BackingField;

		// Token: 0x04000062 RID: 98
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private BoundedPlane <Plane>k__BackingField;
	}
}
