﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x02000022 RID: 34
	[NativeType(Header = "Modules/XR/Subsystems/Example/XRExampleSubsystem.h")]
	[UsedByNativeCode]
	public class XRExampleSubsystem : IntegratedSubsystem<XRExampleSubsystemDescriptor>
	{
		// Token: 0x060000AF RID: 175 RVA: 0x000032D3 File Offset: 0x000014D3
		public XRExampleSubsystem()
		{
		}

		// Token: 0x060000B0 RID: 176
		[NativeConditional("ENABLE_XR")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void PrintExample();

		// Token: 0x060000B1 RID: 177
		[NativeConditional("ENABLE_XR")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool GetBool();
	}
}
