﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x02000038 RID: 56
	[NativeHeader("Modules/XR/XRPrefix.h")]
	[NativeType(Header = "Modules/XR/Subsystems/Raycast/XRRaycastSubsystemDescriptor.h")]
	[UsedByNativeCode]
	public class XRRaycastSubsystemDescriptor : IntegratedSubsystemDescriptor<XRRaycastSubsystem>
	{
		// Token: 0x06000121 RID: 289 RVA: 0x00003E68 File Offset: 0x00002068
		public XRRaycastSubsystemDescriptor()
		{
		}
	}
}
