﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x02000023 RID: 35
	[NativeType(Header = "Modules/XR/Subsystems/Example/XRExampleSubsystemDescriptor.h")]
	[UsedByNativeCode]
	public class XRExampleSubsystemDescriptor : IntegratedSubsystemDescriptor<XRExampleSubsystem>
	{
		// Token: 0x060000B2 RID: 178 RVA: 0x000032DB File Offset: 0x000014DB
		public XRExampleSubsystemDescriptor()
		{
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x060000B3 RID: 179
		public extern bool supportsEditorMode { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x060000B4 RID: 180
		public extern bool disableBackbufferMSAA { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x060000B5 RID: 181
		public extern bool stereoscopicBackbuffer { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x060000B6 RID: 182
		public extern bool usePBufferEGL { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
