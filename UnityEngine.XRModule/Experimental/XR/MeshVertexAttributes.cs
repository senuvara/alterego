﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x02000029 RID: 41
	[UsedByNativeCode]
	[NativeHeader("Modules/XR/Subsystems/Meshing/XRMeshBindings.h")]
	[Flags]
	public enum MeshVertexAttributes
	{
		// Token: 0x04000049 RID: 73
		None = 0,
		// Token: 0x0400004A RID: 74
		Normals = 1,
		// Token: 0x0400004B RID: 75
		Tangents = 2,
		// Token: 0x0400004C RID: 76
		UVs = 4,
		// Token: 0x0400004D RID: 77
		Colors = 8
	}
}
