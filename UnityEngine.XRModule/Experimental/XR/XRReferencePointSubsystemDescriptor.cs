﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x0200003C RID: 60
	[NativeType(Header = "Modules/XR/Subsystems/ReferencePoints/XRReferencePointSubsystemDescriptor.h")]
	[NativeHeader("Modules/XR/XRPrefix.h")]
	[UsedByNativeCode]
	public class XRReferencePointSubsystemDescriptor : IntegratedSubsystemDescriptor<XRReferencePointSubsystem>
	{
		// Token: 0x0600013D RID: 317 RVA: 0x0000406D File Offset: 0x0000226D
		public XRReferencePointSubsystemDescriptor()
		{
		}
	}
}
