﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x0200003B RID: 59
	[NativeConditional("ENABLE_XR")]
	[UsedByNativeCode]
	[NativeHeader("Modules/XR/Subsystems/ReferencePoints/XRReferencePointSubsystem.h")]
	[NativeHeader("Modules/XR/XRPrefix.h")]
	public class XRReferencePointSubsystem : IntegratedSubsystem<XRReferencePointSubsystemDescriptor>
	{
		// Token: 0x0600012E RID: 302 RVA: 0x00003F47 File Offset: 0x00002147
		public XRReferencePointSubsystem()
		{
		}

		// Token: 0x1400000A RID: 10
		// (add) Token: 0x0600012F RID: 303 RVA: 0x00003F50 File Offset: 0x00002150
		// (remove) Token: 0x06000130 RID: 304 RVA: 0x00003F88 File Offset: 0x00002188
		public event Action<ReferencePointUpdatedEventArgs> ReferencePointUpdated
		{
			add
			{
				Action<ReferencePointUpdatedEventArgs> action = this.ReferencePointUpdated;
				Action<ReferencePointUpdatedEventArgs> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<ReferencePointUpdatedEventArgs>>(ref this.ReferencePointUpdated, (Action<ReferencePointUpdatedEventArgs>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<ReferencePointUpdatedEventArgs> action = this.ReferencePointUpdated;
				Action<ReferencePointUpdatedEventArgs> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<ReferencePointUpdatedEventArgs>>(ref this.ReferencePointUpdated, (Action<ReferencePointUpdatedEventArgs>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x06000131 RID: 305
		public extern int LastUpdatedFrame { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000132 RID: 306 RVA: 0x00003FBE File Offset: 0x000021BE
		public bool TryAddReferencePoint(Vector3 position, Quaternion rotation, out TrackableId referencePointId)
		{
			return this.TryAddReferencePoint_Injected(ref position, ref rotation, out referencePointId);
		}

		// Token: 0x06000133 RID: 307 RVA: 0x00003FCC File Offset: 0x000021CC
		public bool TryAddReferencePoint(Pose pose, out TrackableId referencePointId)
		{
			return this.TryAddReferencePoint(pose.position, pose.rotation, out referencePointId);
		}

		// Token: 0x06000134 RID: 308 RVA: 0x00003FF6 File Offset: 0x000021F6
		public bool TryRemoveReferencePoint(TrackableId referencePointId)
		{
			return this.TryRemoveReferencePoint_Injected(ref referencePointId);
		}

		// Token: 0x06000135 RID: 309 RVA: 0x00004000 File Offset: 0x00002200
		public bool TryGetReferencePoint(TrackableId referencePointId, out ReferencePoint referencePoint)
		{
			return this.TryGetReferencePoint_Injected(ref referencePointId, out referencePoint);
		}

		// Token: 0x06000136 RID: 310 RVA: 0x0000400B File Offset: 0x0000220B
		public void GetAllReferencePoints(List<ReferencePoint> referencePointsOut)
		{
			if (referencePointsOut == null)
			{
				throw new ArgumentNullException("referencePointsOut");
			}
			this.Internal_GetAllReferencePointsAsList(referencePointsOut);
		}

		// Token: 0x06000137 RID: 311 RVA: 0x00004028 File Offset: 0x00002228
		[RequiredByNativeCode]
		private void InvokeReferencePointUpdatedEvent(ReferencePoint updatedReferencePoint, TrackingState previousTrackingState, Pose previousPose)
		{
			if (this.ReferencePointUpdated != null)
			{
				this.ReferencePointUpdated(new ReferencePointUpdatedEventArgs
				{
					ReferencePoint = updatedReferencePoint,
					PreviousTrackingState = previousTrackingState,
					PreviousPose = previousPose
				});
			}
		}

		// Token: 0x06000138 RID: 312
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_GetAllReferencePointsAsList(List<ReferencePoint> referencePointsOut);

		// Token: 0x06000139 RID: 313
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern ReferencePoint[] Internal_GetAllReferencePointsAsFixedArray();

		// Token: 0x0600013A RID: 314
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool TryAddReferencePoint_Injected(ref Vector3 position, ref Quaternion rotation, out TrackableId referencePointId);

		// Token: 0x0600013B RID: 315
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool TryRemoveReferencePoint_Injected(ref TrackableId referencePointId);

		// Token: 0x0600013C RID: 316
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool TryGetReferencePoint_Injected(ref TrackableId referencePointId, out ReferencePoint referencePoint);

		// Token: 0x0400007D RID: 125
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Action<ReferencePointUpdatedEventArgs> ReferencePointUpdated;
	}
}
