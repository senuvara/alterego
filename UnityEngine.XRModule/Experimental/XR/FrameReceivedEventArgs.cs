﻿using System;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x0200001A RID: 26
	public struct FrameReceivedEventArgs
	{
		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000083 RID: 131 RVA: 0x000030B0 File Offset: 0x000012B0
		public XRCameraSubsystem CameraSubsystem
		{
			get
			{
				return this.m_CameraSubsystem;
			}
		}

		// Token: 0x04000038 RID: 56
		internal XRCameraSubsystem m_CameraSubsystem;
	}
}
