﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x0200001C RID: 28
	[UsedByNativeCode]
	[NativeHeader("Modules/XR/XRPrefix.h")]
	[NativeConditional("ENABLE_XR")]
	[NativeType(Header = "Modules/XR/Subsystems/Camera/XRCameraSubsystemDescriptor.h")]
	public class XRCameraSubsystemDescriptor : IntegratedSubsystemDescriptor<XRCameraSubsystem>
	{
		// Token: 0x06000099 RID: 153 RVA: 0x000031B3 File Offset: 0x000013B3
		public XRCameraSubsystemDescriptor()
		{
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x0600009A RID: 154
		public extern bool ProvidesAverageBrightness { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x0600009B RID: 155
		public extern bool ProvidesAverageColorTemperature { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x0600009C RID: 156
		public extern bool ProvidesProjectionMatrix { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x0600009D RID: 157
		public extern bool ProvidesDisplayMatrix { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x0600009E RID: 158
		public extern bool ProvidesTimestamp { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
