﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x02000028 RID: 40
	[NativeHeader("Modules/XR/Subsystems/Meshing/XRMeshBindings.h")]
	[RequiredByNativeCode]
	public struct MeshGenerationResult : IEquatable<MeshGenerationResult>
	{
		// Token: 0x1700002A RID: 42
		// (get) Token: 0x060000BB RID: 187 RVA: 0x00003314 File Offset: 0x00001514
		public TrackableId MeshId
		{
			[CompilerGenerated]
			get
			{
				return this.<MeshId>k__BackingField;
			}
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x060000BC RID: 188 RVA: 0x00003330 File Offset: 0x00001530
		public Mesh Mesh
		{
			[CompilerGenerated]
			get
			{
				return this.<Mesh>k__BackingField;
			}
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x060000BD RID: 189 RVA: 0x0000334C File Offset: 0x0000154C
		public MeshCollider MeshCollider
		{
			[CompilerGenerated]
			get
			{
				return this.<MeshCollider>k__BackingField;
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x060000BE RID: 190 RVA: 0x00003368 File Offset: 0x00001568
		public MeshGenerationStatus Status
		{
			[CompilerGenerated]
			get
			{
				return this.<Status>k__BackingField;
			}
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x060000BF RID: 191 RVA: 0x00003384 File Offset: 0x00001584
		public MeshVertexAttributes Attributes
		{
			[CompilerGenerated]
			get
			{
				return this.<Attributes>k__BackingField;
			}
		}

		// Token: 0x060000C0 RID: 192 RVA: 0x000033A0 File Offset: 0x000015A0
		public override bool Equals(object obj)
		{
			return obj is MeshGenerationResult && this.Equals((MeshGenerationResult)obj);
		}

		// Token: 0x060000C1 RID: 193 RVA: 0x000033D4 File Offset: 0x000015D4
		public bool Equals(MeshGenerationResult other)
		{
			return this.MeshId.Equals(other.MeshId) && this.Mesh.Equals(other.Mesh) && this.MeshCollider.Equals(other.MeshCollider) && this.Status.Equals(other.Status) && this.Attributes.Equals(other.Attributes);
		}

		// Token: 0x060000C2 RID: 194 RVA: 0x0000347C File Offset: 0x0000167C
		public static bool operator ==(MeshGenerationResult lhs, MeshGenerationResult rhs)
		{
			return lhs.Equals(rhs);
		}

		// Token: 0x060000C3 RID: 195 RVA: 0x0000349C File Offset: 0x0000169C
		public static bool operator !=(MeshGenerationResult lhs, MeshGenerationResult rhs)
		{
			return !lhs.Equals(rhs);
		}

		// Token: 0x060000C4 RID: 196 RVA: 0x000034BC File Offset: 0x000016BC
		public override int GetHashCode()
		{
			return HashCodeHelper.Combine(HashCodeHelper.Combine(HashCodeHelper.Combine(HashCodeHelper.Combine(this.MeshId.GetHashCode(), this.Mesh.GetHashCode()), this.MeshCollider.GetHashCode()), this.Status.GetHashCode()), this.Attributes.GetHashCode());
		}

		// Token: 0x04000043 RID: 67
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private readonly TrackableId <MeshId>k__BackingField;

		// Token: 0x04000044 RID: 68
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private readonly Mesh <Mesh>k__BackingField;

		// Token: 0x04000045 RID: 69
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private readonly MeshCollider <MeshCollider>k__BackingField;

		// Token: 0x04000046 RID: 70
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private readonly MeshGenerationStatus <Status>k__BackingField;

		// Token: 0x04000047 RID: 71
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private readonly MeshVertexAttributes <Attributes>k__BackingField;
	}
}
