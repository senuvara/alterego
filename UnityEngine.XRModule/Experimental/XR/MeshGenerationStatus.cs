﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x02000026 RID: 38
	[RequiredByNativeCode]
	[NativeHeader("Modules/XR/Subsystems/Meshing/XRMeshBindings.h")]
	public enum MeshGenerationStatus
	{
		// Token: 0x0400003D RID: 61
		Success,
		// Token: 0x0400003E RID: 62
		InvalidMeshId,
		// Token: 0x0400003F RID: 63
		GenerationAlreadyInProgress,
		// Token: 0x04000040 RID: 64
		Canceled,
		// Token: 0x04000041 RID: 65
		UnknownError
	}
}
