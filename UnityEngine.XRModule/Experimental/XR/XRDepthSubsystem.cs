﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x0200001E RID: 30
	[NativeHeader("Modules/XR/XRPrefix.h")]
	[NativeHeader("Modules/XR/Subsystems/Depth/XRDepthSubsystem.h")]
	[NativeConditional("ENABLE_XR")]
	[UsedByNativeCode]
	public class XRDepthSubsystem : IntegratedSubsystem<XRDepthSubsystemDescriptor>
	{
		// Token: 0x060000A0 RID: 160 RVA: 0x000031D7 File Offset: 0x000013D7
		public XRDepthSubsystem()
		{
		}

		// Token: 0x14000006 RID: 6
		// (add) Token: 0x060000A1 RID: 161 RVA: 0x000031E0 File Offset: 0x000013E0
		// (remove) Token: 0x060000A2 RID: 162 RVA: 0x00003218 File Offset: 0x00001418
		public event Action<PointCloudUpdatedEventArgs> PointCloudUpdated
		{
			add
			{
				Action<PointCloudUpdatedEventArgs> action = this.PointCloudUpdated;
				Action<PointCloudUpdatedEventArgs> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<PointCloudUpdatedEventArgs>>(ref this.PointCloudUpdated, (Action<PointCloudUpdatedEventArgs>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<PointCloudUpdatedEventArgs> action = this.PointCloudUpdated;
				Action<PointCloudUpdatedEventArgs> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<PointCloudUpdatedEventArgs>>(ref this.PointCloudUpdated, (Action<PointCloudUpdatedEventArgs>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x060000A3 RID: 163
		public extern int LastUpdatedFrame { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060000A4 RID: 164 RVA: 0x0000324E File Offset: 0x0000144E
		public void GetPoints(List<Vector3> pointsOut)
		{
			if (pointsOut == null)
			{
				throw new ArgumentNullException("pointsOut");
			}
			this.Internal_GetPointCloudPointsAsList(pointsOut);
		}

		// Token: 0x060000A5 RID: 165 RVA: 0x00003269 File Offset: 0x00001469
		public void GetConfidence(List<float> confidenceOut)
		{
			if (confidenceOut == null)
			{
				throw new ArgumentNullException("confidenceOut");
			}
			this.Internal_GetPointCloudConfidenceAsList(confidenceOut);
		}

		// Token: 0x060000A6 RID: 166 RVA: 0x00003284 File Offset: 0x00001484
		[RequiredByNativeCode]
		private void InvokePointCloudUpdatedEvent()
		{
			if (this.PointCloudUpdated != null)
			{
				this.PointCloudUpdated(new PointCloudUpdatedEventArgs
				{
					m_DepthSubsystem = this
				});
			}
		}

		// Token: 0x060000A7 RID: 167
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_GetPointCloudPointsAsList(List<Vector3> pointsOut);

		// Token: 0x060000A8 RID: 168
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_GetPointCloudConfidenceAsList(List<float> confidenceOut);

		// Token: 0x060000A9 RID: 169
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Vector3[] Internal_GetPointCloudPointsAsFixedArray();

		// Token: 0x060000AA RID: 170
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern float[] Internal_GetPointCloudConfidenceAsFixedArray();

		// Token: 0x0400003B RID: 59
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Action<PointCloudUpdatedEventArgs> PointCloudUpdated;
	}
}
