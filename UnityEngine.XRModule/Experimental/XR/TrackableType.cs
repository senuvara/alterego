﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x02000035 RID: 53
	[UsedByNativeCode]
	[Flags]
	public enum TrackableType
	{
		// Token: 0x0400006B RID: 107
		None = 0,
		// Token: 0x0400006C RID: 108
		PlaneWithinPolygon = 1,
		// Token: 0x0400006D RID: 109
		PlaneWithinBounds = 2,
		// Token: 0x0400006E RID: 110
		PlaneWithinInfinity = 4,
		// Token: 0x0400006F RID: 111
		PlaneEstimated = 8,
		// Token: 0x04000070 RID: 112
		Planes = 15,
		// Token: 0x04000071 RID: 113
		FeaturePoint = 16,
		// Token: 0x04000072 RID: 114
		All = 31
	}
}
