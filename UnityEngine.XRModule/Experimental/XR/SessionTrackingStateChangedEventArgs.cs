﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x0200003E RID: 62
	public struct SessionTrackingStateChangedEventArgs
	{
		// Token: 0x1700004E RID: 78
		// (get) Token: 0x0600013E RID: 318 RVA: 0x00004078 File Offset: 0x00002278
		public XRSessionSubsystem SessionSubsystem
		{
			get
			{
				return this.m_Session;
			}
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x0600013F RID: 319 RVA: 0x00004094 File Offset: 0x00002294
		// (set) Token: 0x06000140 RID: 320 RVA: 0x000040AE File Offset: 0x000022AE
		public TrackingState NewState
		{
			[CompilerGenerated]
			get
			{
				return this.<NewState>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<NewState>k__BackingField = value;
			}
		}

		// Token: 0x04000082 RID: 130
		internal XRSessionSubsystem m_Session;

		// Token: 0x04000083 RID: 131
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private TrackingState <NewState>k__BackingField;
	}
}
