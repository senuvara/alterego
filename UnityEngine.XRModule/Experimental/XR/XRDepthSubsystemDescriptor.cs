﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x0200001F RID: 31
	[NativeConditional("ENABLE_XR")]
	[NativeType(Header = "Modules/XR/Subsystems/Depth/XRDepthSubsystemDescriptor.h")]
	[NativeHeader("Modules/XR/XRPrefix.h")]
	[UsedByNativeCode]
	public class XRDepthSubsystemDescriptor : IntegratedSubsystemDescriptor<XRDepthSubsystem>
	{
		// Token: 0x060000AB RID: 171 RVA: 0x000032BB File Offset: 0x000014BB
		public XRDepthSubsystemDescriptor()
		{
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x060000AC RID: 172
		public extern bool SupportsFeaturePoints { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
