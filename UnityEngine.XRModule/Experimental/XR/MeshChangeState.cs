﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x0200002A RID: 42
	[NativeHeader("Modules/XR/Subsystems/Meshing/XRMeshBindings.h")]
	[UsedByNativeCode]
	public enum MeshChangeState
	{
		// Token: 0x0400004F RID: 79
		Added,
		// Token: 0x04000050 RID: 80
		Updated,
		// Token: 0x04000051 RID: 81
		Removed,
		// Token: 0x04000052 RID: 82
		Unchanged
	}
}
