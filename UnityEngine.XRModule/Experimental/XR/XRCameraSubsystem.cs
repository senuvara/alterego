﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x0200001B RID: 27
	[NativeConditional("ENABLE_XR")]
	[UsedByNativeCode]
	[NativeType(Header = "Modules/XR/Subsystems/Camera/XRCameraSubsystem.h")]
	[NativeHeader("Modules/XR/XRPrefix.h")]
	public class XRCameraSubsystem : IntegratedSubsystem<XRCameraSubsystemDescriptor>
	{
		// Token: 0x06000084 RID: 132 RVA: 0x000030CB File Offset: 0x000012CB
		public XRCameraSubsystem()
		{
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000085 RID: 133
		public extern int LastUpdatedFrame { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000086 RID: 134
		// (set) Token: 0x06000087 RID: 135
		public extern bool LightEstimationRequested { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000088 RID: 136
		// (set) Token: 0x06000089 RID: 137
		public extern Material Material { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x0600008A RID: 138
		// (set) Token: 0x0600008B RID: 139
		public extern Camera Camera { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600008C RID: 140
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool TryGetAverageBrightness(ref float averageBrightness);

		// Token: 0x0600008D RID: 141
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool TryGetAverageColorTemperature(ref float averageColorTemperature);

		// Token: 0x0600008E RID: 142
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool TryGetProjectionMatrix(ref Matrix4x4 projectionMatrix);

		// Token: 0x0600008F RID: 143
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool TryGetDisplayMatrix(ref Matrix4x4 displayMatrix);

		// Token: 0x06000090 RID: 144
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool TryGetTimestamp(ref long timestampNs);

		// Token: 0x06000091 RID: 145 RVA: 0x000030D4 File Offset: 0x000012D4
		public bool TryGetShaderName(ref string shaderName)
		{
			return this.Internal_TryGetShaderName(ref shaderName);
		}

		// Token: 0x06000092 RID: 146
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool Internal_TryGetShaderName(ref string shaderName);

		// Token: 0x06000093 RID: 147 RVA: 0x000030F0 File Offset: 0x000012F0
		public void GetTextures(List<Texture2D> texturesOut)
		{
			if (texturesOut == null)
			{
				throw new ArgumentNullException("texturesOut");
			}
			this.GetTexturesAsList(texturesOut);
		}

		// Token: 0x06000094 RID: 148
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetTexturesAsList(List<Texture2D> textures);

		// Token: 0x06000095 RID: 149
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Texture2D[] GetTexturesAsFixedArray();

		// Token: 0x14000005 RID: 5
		// (add) Token: 0x06000096 RID: 150 RVA: 0x0000310C File Offset: 0x0000130C
		// (remove) Token: 0x06000097 RID: 151 RVA: 0x00003144 File Offset: 0x00001344
		public event Action<FrameReceivedEventArgs> FrameReceived
		{
			add
			{
				Action<FrameReceivedEventArgs> action = this.FrameReceived;
				Action<FrameReceivedEventArgs> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<FrameReceivedEventArgs>>(ref this.FrameReceived, (Action<FrameReceivedEventArgs>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<FrameReceivedEventArgs> action = this.FrameReceived;
				Action<FrameReceivedEventArgs> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<FrameReceivedEventArgs>>(ref this.FrameReceived, (Action<FrameReceivedEventArgs>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x06000098 RID: 152 RVA: 0x0000317C File Offset: 0x0000137C
		[RequiredByNativeCode]
		private void InvokeFrameReceivedEvent()
		{
			if (this.FrameReceived != null)
			{
				this.FrameReceived(new FrameReceivedEventArgs
				{
					m_CameraSubsystem = this
				});
			}
		}

		// Token: 0x04000039 RID: 57
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Action<FrameReceivedEventArgs> FrameReceived;
	}
}
