﻿using System;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x02000027 RID: 39
	internal static class HashCodeHelper
	{
		// Token: 0x060000BA RID: 186 RVA: 0x000032F4 File Offset: 0x000014F4
		public static int Combine(int hash1, int hash2)
		{
			return hash1 * 486187739 + hash2;
		}

		// Token: 0x04000042 RID: 66
		private const int k_HashCodeMultiplier = 486187739;
	}
}
