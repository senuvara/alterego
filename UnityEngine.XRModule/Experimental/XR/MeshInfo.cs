﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x0200002B RID: 43
	[NativeHeader("Modules/XR/Subsystems/Meshing/XRMeshBindings.h")]
	[UsedByNativeCode]
	public struct MeshInfo : IEquatable<MeshInfo>
	{
		// Token: 0x1700002F RID: 47
		// (get) Token: 0x060000C5 RID: 197 RVA: 0x00003538 File Offset: 0x00001738
		// (set) Token: 0x060000C6 RID: 198 RVA: 0x00003552 File Offset: 0x00001752
		public TrackableId MeshId
		{
			[CompilerGenerated]
			get
			{
				return this.<MeshId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<MeshId>k__BackingField = value;
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x060000C7 RID: 199 RVA: 0x0000355C File Offset: 0x0000175C
		// (set) Token: 0x060000C8 RID: 200 RVA: 0x00003576 File Offset: 0x00001776
		public MeshChangeState ChangeState
		{
			[CompilerGenerated]
			get
			{
				return this.<ChangeState>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ChangeState>k__BackingField = value;
			}
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x060000C9 RID: 201 RVA: 0x00003580 File Offset: 0x00001780
		// (set) Token: 0x060000CA RID: 202 RVA: 0x0000359A File Offset: 0x0000179A
		public int PriorityHint
		{
			[CompilerGenerated]
			get
			{
				return this.<PriorityHint>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<PriorityHint>k__BackingField = value;
			}
		}

		// Token: 0x060000CB RID: 203 RVA: 0x000035A4 File Offset: 0x000017A4
		public override bool Equals(object obj)
		{
			return obj is MeshInfo && this.Equals((MeshInfo)obj);
		}

		// Token: 0x060000CC RID: 204 RVA: 0x000035D8 File Offset: 0x000017D8
		public bool Equals(MeshInfo other)
		{
			return this.MeshId.Equals(other.MeshId) && this.ChangeState.Equals(other.ChangeState) && this.PriorityHint.Equals(other.PriorityHint);
		}

		// Token: 0x060000CD RID: 205 RVA: 0x00003644 File Offset: 0x00001844
		public static bool operator ==(MeshInfo lhs, MeshInfo rhs)
		{
			return lhs.Equals(rhs);
		}

		// Token: 0x060000CE RID: 206 RVA: 0x00003664 File Offset: 0x00001864
		public static bool operator !=(MeshInfo lhs, MeshInfo rhs)
		{
			return !lhs.Equals(rhs);
		}

		// Token: 0x060000CF RID: 207 RVA: 0x00003684 File Offset: 0x00001884
		public override int GetHashCode()
		{
			return HashCodeHelper.Combine(HashCodeHelper.Combine(this.MeshId.GetHashCode(), this.ChangeState.GetHashCode()), this.PriorityHint);
		}

		// Token: 0x04000053 RID: 83
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private TrackableId <MeshId>k__BackingField;

		// Token: 0x04000054 RID: 84
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private MeshChangeState <ChangeState>k__BackingField;

		// Token: 0x04000055 RID: 85
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <PriorityHint>k__BackingField;
	}
}
