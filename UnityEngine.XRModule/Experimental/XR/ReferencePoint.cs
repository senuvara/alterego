﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x02000039 RID: 57
	[NativeHeader("Modules/XR/Subsystems/ReferencePoints/XRManagedReferencePoint.h")]
	[NativeHeader("Modules/XR/Subsystems/Session/XRSessionSubsystem.h")]
	[UsedByNativeCode]
	public struct ReferencePoint
	{
		// Token: 0x17000047 RID: 71
		// (get) Token: 0x06000122 RID: 290 RVA: 0x00003E70 File Offset: 0x00002070
		// (set) Token: 0x06000123 RID: 291 RVA: 0x00003E8A File Offset: 0x0000208A
		public TrackableId Id
		{
			[CompilerGenerated]
			get
			{
				return this.<Id>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<Id>k__BackingField = value;
			}
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x06000124 RID: 292 RVA: 0x00003E94 File Offset: 0x00002094
		// (set) Token: 0x06000125 RID: 293 RVA: 0x00003EAE File Offset: 0x000020AE
		public TrackingState TrackingState
		{
			[CompilerGenerated]
			get
			{
				return this.<TrackingState>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<TrackingState>k__BackingField = value;
			}
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x06000126 RID: 294 RVA: 0x00003EB8 File Offset: 0x000020B8
		// (set) Token: 0x06000127 RID: 295 RVA: 0x00003ED2 File Offset: 0x000020D2
		public Pose Pose
		{
			[CompilerGenerated]
			get
			{
				return this.<Pose>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<Pose>k__BackingField = value;
			}
		}

		// Token: 0x04000077 RID: 119
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private TrackableId <Id>k__BackingField;

		// Token: 0x04000078 RID: 120
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private TrackingState <TrackingState>k__BackingField;

		// Token: 0x04000079 RID: 121
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Pose <Pose>k__BackingField;
	}
}
