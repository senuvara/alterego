﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x02000031 RID: 49
	public struct PlaneUpdatedEventArgs
	{
		// Token: 0x1700003E RID: 62
		// (get) Token: 0x060000F2 RID: 242 RVA: 0x000039E8 File Offset: 0x00001BE8
		// (set) Token: 0x060000F3 RID: 243 RVA: 0x00003A02 File Offset: 0x00001C02
		public XRPlaneSubsystem PlaneSubsystem
		{
			[CompilerGenerated]
			get
			{
				return this.<PlaneSubsystem>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<PlaneSubsystem>k__BackingField = value;
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x060000F4 RID: 244 RVA: 0x00003A0C File Offset: 0x00001C0C
		// (set) Token: 0x060000F5 RID: 245 RVA: 0x00003A26 File Offset: 0x00001C26
		public BoundedPlane Plane
		{
			[CompilerGenerated]
			get
			{
				return this.<Plane>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<Plane>k__BackingField = value;
			}
		}

		// Token: 0x04000063 RID: 99
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private XRPlaneSubsystem <PlaneSubsystem>k__BackingField;

		// Token: 0x04000064 RID: 100
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private BoundedPlane <Plane>k__BackingField;
	}
}
