﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x02000025 RID: 37
	[UsedByNativeCode]
	[NativeConditional("ENABLE_XR")]
	[NativeType(Header = "Modules/XR/Subsystems/Input/XRInputSubsystemDescriptor.h")]
	[NativeHeader("Modules/XR/XRPrefix.h")]
	public class XRInputSubsystemDescriptor : IntegratedSubsystemDescriptor<XRInputSubsystem>
	{
		// Token: 0x060000B8 RID: 184 RVA: 0x000032EB File Offset: 0x000014EB
		public XRInputSubsystemDescriptor()
		{
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x060000B9 RID: 185
		[NativeConditional("ENABLE_XR")]
		public extern bool disablesLegacyInput { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
