﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x0200002D RID: 45
	[UsedByNativeCode]
	[NativeType(Header = "Modules/XR/Subsystems/Planes/XRMeshSubsystemDescriptor.h")]
	[NativeHeader("Modules/XR/XRPrefix.h")]
	public class XRMeshSubsystemDescriptor : IntegratedSubsystemDescriptor<XRMeshSubsystem>
	{
		// Token: 0x060000D7 RID: 215 RVA: 0x00003729 File Offset: 0x00001929
		public XRMeshSubsystemDescriptor()
		{
		}
	}
}
