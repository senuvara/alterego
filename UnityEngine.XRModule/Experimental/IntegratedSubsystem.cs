﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental
{
	// Token: 0x0200000E RID: 14
	[UsedByNativeCode]
	[NativeType(Header = "Modules/XR/XRSubsystem.h")]
	[StructLayout(LayoutKind.Sequential)]
	public class IntegratedSubsystem : ISubsystem
	{
		// Token: 0x0600002E RID: 46 RVA: 0x0000222F File Offset: 0x0000042F
		public IntegratedSubsystem()
		{
		}

		// Token: 0x0600002F RID: 47
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetHandle(IntegratedSubsystem inst);

		// Token: 0x06000030 RID: 48
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Start();

		// Token: 0x06000031 RID: 49
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Stop();

		// Token: 0x06000032 RID: 50 RVA: 0x000027E8 File Offset: 0x000009E8
		public void Destroy()
		{
			IntPtr ptr = this.m_Ptr;
			Internal_SubsystemInstances.Internal_RemoveInstanceByPtr(this.m_Ptr);
			SubsystemManager.DestroyInstance_Internal(ptr);
		}

		// Token: 0x0400000B RID: 11
		internal IntPtr m_Ptr;

		// Token: 0x0400000C RID: 12
		internal ISubsystemDescriptor m_subsystemDescriptor;
	}
}
