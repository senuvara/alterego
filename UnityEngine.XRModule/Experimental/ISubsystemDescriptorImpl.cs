﻿using System;

namespace UnityEngine.Experimental
{
	// Token: 0x02000005 RID: 5
	internal interface ISubsystemDescriptorImpl
	{
		// Token: 0x17000002 RID: 2
		// (get) Token: 0x0600000A RID: 10
		// (set) Token: 0x0600000B RID: 11
		IntPtr ptr { get; set; }
	}
}
