﻿using System;

namespace UnityEngine.Experimental
{
	// Token: 0x02000010 RID: 16
	public abstract class Subsystem : ISubsystem
	{
		// Token: 0x06000035 RID: 53 RVA: 0x0000222F File Offset: 0x0000042F
		protected Subsystem()
		{
		}

		// Token: 0x06000036 RID: 54
		public abstract void Start();

		// Token: 0x06000037 RID: 55
		public abstract void Stop();

		// Token: 0x06000038 RID: 56
		public abstract void Destroy();

		// Token: 0x0400000D RID: 13
		internal ISubsystemDescriptor m_subsystemDescriptor;
	}
}
