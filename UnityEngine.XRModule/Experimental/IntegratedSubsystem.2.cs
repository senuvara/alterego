﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental
{
	// Token: 0x0200000F RID: 15
	[UsedByNativeCode("XRSubsystem_TXRSubsystemDescriptor")]
	public class IntegratedSubsystem<TSubsystemDescriptor> : IntegratedSubsystem where TSubsystemDescriptor : ISubsystemDescriptor
	{
		// Token: 0x06000033 RID: 51 RVA: 0x0000280E File Offset: 0x00000A0E
		public IntegratedSubsystem()
		{
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000034 RID: 52 RVA: 0x00002818 File Offset: 0x00000A18
		public TSubsystemDescriptor SubsystemDescriptor
		{
			get
			{
				return (TSubsystemDescriptor)((object)this.m_subsystemDescriptor);
			}
		}
	}
}
