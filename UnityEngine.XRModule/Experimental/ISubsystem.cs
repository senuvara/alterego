﻿using System;

namespace UnityEngine.Experimental
{
	// Token: 0x0200000D RID: 13
	public interface ISubsystem
	{
		// Token: 0x0600002B RID: 43
		void Start();

		// Token: 0x0600002C RID: 44
		void Stop();

		// Token: 0x0600002D RID: 45
		void Destroy();
	}
}
