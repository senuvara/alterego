﻿using System;

namespace UnityEngine.Experimental
{
	// Token: 0x02000011 RID: 17
	public abstract class Subsystem<TSubsystemDescriptor> : Subsystem where TSubsystemDescriptor : ISubsystemDescriptor
	{
		// Token: 0x06000039 RID: 57 RVA: 0x00002838 File Offset: 0x00000A38
		protected Subsystem()
		{
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x0600003A RID: 58 RVA: 0x00002840 File Offset: 0x00000A40
		public TSubsystemDescriptor SubsystemDescriptor
		{
			get
			{
				return (TSubsystemDescriptor)((object)this.m_subsystemDescriptor);
			}
		}
	}
}
