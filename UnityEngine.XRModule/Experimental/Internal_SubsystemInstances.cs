﻿using System;
using System.Collections.Generic;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental
{
	// Token: 0x0200000A RID: 10
	internal static class Internal_SubsystemInstances
	{
		// Token: 0x06000019 RID: 25 RVA: 0x00002363 File Offset: 0x00000563
		[RequiredByNativeCode]
		internal static void Internal_InitializeManagedInstance(IntPtr ptr, IntegratedSubsystem inst)
		{
			inst.m_Ptr = ptr;
			inst.SetHandle(inst);
			Internal_SubsystemInstances.s_IntegratedSubsystemInstances.Add(inst);
		}

		// Token: 0x0600001A RID: 26 RVA: 0x00002380 File Offset: 0x00000580
		[RequiredByNativeCode]
		internal static void Internal_ClearManagedInstances()
		{
			foreach (ISubsystem subsystem in Internal_SubsystemInstances.s_IntegratedSubsystemInstances)
			{
				((IntegratedSubsystem)subsystem).m_Ptr = IntPtr.Zero;
			}
			Internal_SubsystemInstances.s_IntegratedSubsystemInstances.Clear();
			Internal_SubsystemInstances.s_StandaloneSubsystemInstances.Clear();
		}

		// Token: 0x0600001B RID: 27 RVA: 0x000023FC File Offset: 0x000005FC
		[RequiredByNativeCode]
		internal static void Internal_RemoveInstanceByPtr(IntPtr ptr)
		{
			for (int i = Internal_SubsystemInstances.s_IntegratedSubsystemInstances.Count - 1; i >= 0; i--)
			{
				if (((IntegratedSubsystem)Internal_SubsystemInstances.s_IntegratedSubsystemInstances[i]).m_Ptr == ptr)
				{
					((IntegratedSubsystem)Internal_SubsystemInstances.s_IntegratedSubsystemInstances[i]).m_Ptr = IntPtr.Zero;
					Internal_SubsystemInstances.s_IntegratedSubsystemInstances.RemoveAt(i);
				}
			}
		}

		// Token: 0x0600001C RID: 28 RVA: 0x00002470 File Offset: 0x00000670
		internal static IntegratedSubsystem Internal_GetInstanceByPtr(IntPtr ptr)
		{
			foreach (ISubsystem subsystem in Internal_SubsystemInstances.s_IntegratedSubsystemInstances)
			{
				IntegratedSubsystem integratedSubsystem = (IntegratedSubsystem)subsystem;
				if (integratedSubsystem.m_Ptr == ptr)
				{
					return integratedSubsystem;
				}
			}
			return null;
		}

		// Token: 0x0600001D RID: 29 RVA: 0x000024F0 File Offset: 0x000006F0
		internal static void Internal_AddStandaloneSubsystem(Subsystem inst)
		{
			Internal_SubsystemInstances.s_StandaloneSubsystemInstances.Add(inst);
		}

		// Token: 0x0600001E RID: 30 RVA: 0x000024FE File Offset: 0x000006FE
		// Note: this type is marked as 'beforefieldinit'.
		static Internal_SubsystemInstances()
		{
		}

		// Token: 0x04000007 RID: 7
		internal static List<ISubsystem> s_IntegratedSubsystemInstances = new List<ISubsystem>();

		// Token: 0x04000008 RID: 8
		internal static List<ISubsystem> s_StandaloneSubsystemInstances = new List<ISubsystem>();
	}
}
