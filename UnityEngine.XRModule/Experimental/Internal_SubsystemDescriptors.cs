﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental
{
	// Token: 0x0200000B RID: 11
	internal static class Internal_SubsystemDescriptors
	{
		// Token: 0x0600001F RID: 31 RVA: 0x00002514 File Offset: 0x00000714
		[RequiredByNativeCode]
		internal static bool Internal_AddDescriptor(SubsystemDescriptor descriptor)
		{
			foreach (ISubsystemDescriptor subsystemDescriptor in Internal_SubsystemDescriptors.s_StandaloneSubsystemDescriptors)
			{
				if (subsystemDescriptor == descriptor)
				{
					return false;
				}
			}
			Internal_SubsystemDescriptors.s_StandaloneSubsystemDescriptors.Add(descriptor);
			SubsystemManager.ReportSingleSubsystemAnalytics(descriptor.id);
			return true;
		}

		// Token: 0x06000020 RID: 32 RVA: 0x00002598 File Offset: 0x00000798
		[RequiredByNativeCode]
		internal static void Internal_InitializeManagedDescriptor(IntPtr ptr, ISubsystemDescriptorImpl desc)
		{
			desc.ptr = ptr;
			Internal_SubsystemDescriptors.s_IntegratedSubsystemDescriptors.Add(desc);
		}

		// Token: 0x06000021 RID: 33 RVA: 0x000025B0 File Offset: 0x000007B0
		[RequiredByNativeCode]
		internal static void Internal_ClearManagedDescriptors()
		{
			foreach (ISubsystemDescriptorImpl subsystemDescriptorImpl in Internal_SubsystemDescriptors.s_IntegratedSubsystemDescriptors)
			{
				subsystemDescriptorImpl.ptr = IntPtr.Zero;
			}
			Internal_SubsystemDescriptors.s_IntegratedSubsystemDescriptors.Clear();
		}

		// Token: 0x06000022 RID: 34
		[NativeConditional("ENABLE_XR")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern IntPtr Create(IntPtr descriptorPtr);

		// Token: 0x06000023 RID: 35
		[NativeConditional("ENABLE_XR")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string GetId(IntPtr descriptorPtr);

		// Token: 0x06000024 RID: 36 RVA: 0x00002620 File Offset: 0x00000820
		// Note: this type is marked as 'beforefieldinit'.
		static Internal_SubsystemDescriptors()
		{
		}

		// Token: 0x04000009 RID: 9
		internal static List<ISubsystemDescriptorImpl> s_IntegratedSubsystemDescriptors = new List<ISubsystemDescriptorImpl>();

		// Token: 0x0400000A RID: 10
		internal static List<ISubsystemDescriptor> s_StandaloneSubsystemDescriptors = new List<ISubsystemDescriptor>();
	}
}
