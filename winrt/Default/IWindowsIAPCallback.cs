﻿using System;

namespace UnityEngine.Purchasing.Default
{
	// Token: 0x02000004 RID: 4
	public interface IWindowsIAPCallback
	{
		// Token: 0x06000006 RID: 6
		void OnProductListReceived(WinProductDescription[] winProducts);

		// Token: 0x06000007 RID: 7
		void OnProductListError(string message);

		// Token: 0x06000008 RID: 8
		void OnPurchaseSucceeded(string productId, string receipt, string transactionId);

		// Token: 0x06000009 RID: 9
		void OnPurchaseFailed(string productId, string error);

		// Token: 0x0600000A RID: 10
		void logError(string error);

		// Token: 0x0600000B RID: 11
		void log(string message);
	}
}
