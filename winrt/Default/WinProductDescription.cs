﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.Purchasing.Default
{
	// Token: 0x02000006 RID: 6
	public class WinProductDescription
	{
		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000011 RID: 17 RVA: 0x000020DC File Offset: 0x000002DC
		// (set) Token: 0x06000012 RID: 18 RVA: 0x000020E4 File Offset: 0x000002E4
		public string platformSpecificID
		{
			[CompilerGenerated]
			get
			{
				return this.<platformSpecificID>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<platformSpecificID>k__BackingField = value;
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000013 RID: 19 RVA: 0x000020ED File Offset: 0x000002ED
		// (set) Token: 0x06000014 RID: 20 RVA: 0x000020F5 File Offset: 0x000002F5
		public string price
		{
			[CompilerGenerated]
			get
			{
				return this.<price>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<price>k__BackingField = value;
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000015 RID: 21 RVA: 0x000020FE File Offset: 0x000002FE
		// (set) Token: 0x06000016 RID: 22 RVA: 0x00002106 File Offset: 0x00000306
		public string title
		{
			[CompilerGenerated]
			get
			{
				return this.<title>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<title>k__BackingField = value;
			}
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000017 RID: 23 RVA: 0x0000210F File Offset: 0x0000030F
		// (set) Token: 0x06000018 RID: 24 RVA: 0x00002117 File Offset: 0x00000317
		public string description
		{
			[CompilerGenerated]
			get
			{
				return this.<description>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<description>k__BackingField = value;
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000019 RID: 25 RVA: 0x00002120 File Offset: 0x00000320
		// (set) Token: 0x0600001A RID: 26 RVA: 0x00002128 File Offset: 0x00000328
		public string ISOCurrencyCode
		{
			[CompilerGenerated]
			get
			{
				return this.<ISOCurrencyCode>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ISOCurrencyCode>k__BackingField = value;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x0600001B RID: 27 RVA: 0x00002131 File Offset: 0x00000331
		// (set) Token: 0x0600001C RID: 28 RVA: 0x00002139 File Offset: 0x00000339
		public decimal priceDecimal
		{
			[CompilerGenerated]
			get
			{
				return this.<priceDecimal>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<priceDecimal>k__BackingField = value;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600001D RID: 29 RVA: 0x00002142 File Offset: 0x00000342
		// (set) Token: 0x0600001E RID: 30 RVA: 0x0000214A File Offset: 0x0000034A
		public string receipt
		{
			[CompilerGenerated]
			get
			{
				return this.<receipt>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<receipt>k__BackingField = value;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x0600001F RID: 31 RVA: 0x00002153 File Offset: 0x00000353
		// (set) Token: 0x06000020 RID: 32 RVA: 0x0000215B File Offset: 0x0000035B
		public string transactionID
		{
			[CompilerGenerated]
			get
			{
				return this.<transactionID>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<transactionID>k__BackingField = value;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000021 RID: 33 RVA: 0x00002164 File Offset: 0x00000364
		// (set) Token: 0x06000022 RID: 34 RVA: 0x0000216C File Offset: 0x0000036C
		public bool consumable
		{
			[CompilerGenerated]
			get
			{
				return this.<consumable>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<consumable>k__BackingField = value;
			}
		}

		// Token: 0x06000023 RID: 35 RVA: 0x00002178 File Offset: 0x00000378
		public WinProductDescription(string id, string price, string title, string description, string isoCode, decimal priceD, string receipt = null, string transactionId = null, bool consumable = false)
		{
			this.platformSpecificID = id;
			this.price = price;
			this.title = title;
			this.description = description;
			this.ISOCurrencyCode = isoCode;
			this.priceDecimal = priceD;
			this.receipt = receipt;
			this.transactionID = transactionId;
			this.consumable = consumable;
		}

		// Token: 0x04000001 RID: 1
		[CompilerGenerated]
		private string <platformSpecificID>k__BackingField;

		// Token: 0x04000002 RID: 2
		[CompilerGenerated]
		private string <price>k__BackingField;

		// Token: 0x04000003 RID: 3
		[CompilerGenerated]
		private string <title>k__BackingField;

		// Token: 0x04000004 RID: 4
		[CompilerGenerated]
		private string <description>k__BackingField;

		// Token: 0x04000005 RID: 5
		[CompilerGenerated]
		private string <ISOCurrencyCode>k__BackingField;

		// Token: 0x04000006 RID: 6
		[CompilerGenerated]
		private decimal <priceDecimal>k__BackingField;

		// Token: 0x04000007 RID: 7
		[CompilerGenerated]
		private string <receipt>k__BackingField;

		// Token: 0x04000008 RID: 8
		[CompilerGenerated]
		private string <transactionID>k__BackingField;

		// Token: 0x04000009 RID: 9
		[CompilerGenerated]
		private bool <consumable>k__BackingField;
	}
}
