﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Purchasing.Default
{
	// Token: 0x02000005 RID: 5
	public interface IWindowsIAP
	{
		// Token: 0x0600000C RID: 12
		void BuildDummyProducts(List<WinProductDescription> products);

		// Token: 0x0600000D RID: 13
		void Initialize(IWindowsIAPCallback callback);

		// Token: 0x0600000E RID: 14
		void RetrieveProducts(bool retryIfOffline);

		// Token: 0x0600000F RID: 15
		void Purchase(string productId);

		// Token: 0x06000010 RID: 16
		void FinaliseTransaction(string transactionId);
	}
}
