﻿using System;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000002 RID: 2
	public class HttpManager
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public void GetAsyncHttpWebData(string httpAddress, Action<IAsyncResult> asyncCompleate, Action<object, bool> asyncTimeout, string param = null, string requestMothod = "GET", int timeout = 5000)
		{
			ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(this.VerifyServerCertificate);
			HttpWebRequest httpWebRequest = WebRequest.Create(httpAddress + ((param == null) ? "" : param)) as HttpWebRequest;
			httpWebRequest.Method = requestMothod;
			httpWebRequest.Timeout = timeout;
			ThreadPool.RegisterWaitForSingleObject(httpWebRequest.BeginGetResponse(new AsyncCallback(asyncCompleate.Invoke), httpWebRequest).AsyncWaitHandle, new WaitOrTimerCallback(asyncTimeout.Invoke), httpWebRequest, timeout, true);
		}

		// Token: 0x06000002 RID: 2 RVA: 0x000020CA File Offset: 0x000002CA
		private bool VerifyServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
		{
			return true;
		}

		// Token: 0x06000003 RID: 3 RVA: 0x000020CD File Offset: 0x000002CD
		public HttpManager()
		{
		}
	}
}
