﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine.Accessibility
{
	// Token: 0x02000002 RID: 2
	[UsedByNativeCode]
	public static class VisionUtility
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		internal static float ComputePerceivedLuminance(Color color)
		{
			color = color.linear;
			return Mathf.LinearToGammaSpace(0.2126f * color.r + 0.7152f * color.g + 0.0722f * color.b);
		}

		// Token: 0x06000002 RID: 2 RVA: 0x0000209C File Offset: 0x0000029C
		public static int GetColorBlindSafePalette(Color[] palette, float minimumLuminance, float maximumLuminance)
		{
			if (palette == null)
			{
				throw new ArgumentNullException("palette");
			}
			Color[] array = (from i in Enumerable.Range(0, VisionUtility.s_ColorBlindSafePalette.Length)
			where VisionUtility.s_ColorBlindSafePaletteLuminanceValues[i] >= minimumLuminance && VisionUtility.s_ColorBlindSafePaletteLuminanceValues[i] <= maximumLuminance
			select VisionUtility.s_ColorBlindSafePalette[i]).ToArray<Color>();
			int num = Mathf.Min(palette.Length, array.Length);
			if (num > 0)
			{
				int k = 0;
				int num2 = palette.Length;
				while (k < num2)
				{
					palette[k] = array[k % num];
					k++;
				}
			}
			else
			{
				int j = 0;
				int num3 = palette.Length;
				while (j < num3)
				{
					palette[j] = default(Color);
					j++;
				}
			}
			return num;
		}

		// Token: 0x06000003 RID: 3 RVA: 0x000021A0 File Offset: 0x000003A0
		// Note: this type is marked as 'beforefieldinit'.
		static VisionUtility()
		{
		}

		// Token: 0x06000004 RID: 4 RVA: 0x00002378 File Offset: 0x00000578
		[CompilerGenerated]
		private static Color <GetColorBlindSafePalette>m__0(int i)
		{
			return VisionUtility.s_ColorBlindSafePalette[i];
		}

		// Token: 0x06000005 RID: 5 RVA: 0x0000239C File Offset: 0x0000059C
		[CompilerGenerated]
		private static float <s_ColorBlindSafePaletteLuminanceValues>m__1(Color c)
		{
			return VisionUtility.ComputePerceivedLuminance(c);
		}

		// Token: 0x04000001 RID: 1
		private static readonly Color[] s_ColorBlindSafePalette = new Color[]
		{
			new Color32(0, 0, 0, byte.MaxValue),
			new Color32(73, 0, 146, byte.MaxValue),
			new Color32(7, 71, 81, byte.MaxValue),
			new Color32(0, 146, 146, byte.MaxValue),
			new Color32(182, 109, byte.MaxValue, byte.MaxValue),
			new Color32(byte.MaxValue, 109, 182, byte.MaxValue),
			new Color32(109, 182, byte.MaxValue, byte.MaxValue),
			new Color32(36, byte.MaxValue, 36, byte.MaxValue),
			new Color32(byte.MaxValue, 182, 219, byte.MaxValue),
			new Color32(182, 219, byte.MaxValue, byte.MaxValue),
			new Color32(byte.MaxValue, byte.MaxValue, 109, byte.MaxValue)
		};

		// Token: 0x04000002 RID: 2
		private static readonly float[] s_ColorBlindSafePaletteLuminanceValues = (from c in VisionUtility.s_ColorBlindSafePalette
		select VisionUtility.ComputePerceivedLuminance(c)).ToArray<float>();

		// Token: 0x04000003 RID: 3
		[CompilerGenerated]
		private static Func<int, Color> <>f__am$cache0;

		// Token: 0x02000003 RID: 3
		[CompilerGenerated]
		private sealed class <GetColorBlindSafePalette>c__AnonStorey0
		{
			// Token: 0x06000006 RID: 6 RVA: 0x000023B6 File Offset: 0x000005B6
			public <GetColorBlindSafePalette>c__AnonStorey0()
			{
			}

			// Token: 0x06000007 RID: 7 RVA: 0x000023C0 File Offset: 0x000005C0
			internal bool <>m__0(int i)
			{
				return VisionUtility.s_ColorBlindSafePaletteLuminanceValues[i] >= this.minimumLuminance && VisionUtility.s_ColorBlindSafePaletteLuminanceValues[i] <= this.maximumLuminance;
			}

			// Token: 0x04000004 RID: 4
			internal float minimumLuminance;

			// Token: 0x04000005 RID: 5
			internal float maximumLuminance;
		}
	}
}
