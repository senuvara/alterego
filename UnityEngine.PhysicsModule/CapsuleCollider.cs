﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200001D RID: 29
	[RequiredByNativeCode]
	[NativeHeader("Runtime/Dynamics/CapsuleCollider.h")]
	public class CapsuleCollider : Collider
	{
		// Token: 0x06000136 RID: 310 RVA: 0x000030E9 File Offset: 0x000012E9
		public CapsuleCollider()
		{
		}

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x06000137 RID: 311 RVA: 0x0000318C File Offset: 0x0000138C
		// (set) Token: 0x06000138 RID: 312 RVA: 0x000031A2 File Offset: 0x000013A2
		public Vector3 center
		{
			get
			{
				Vector3 result;
				this.get_center_Injected(out result);
				return result;
			}
			set
			{
				this.set_center_Injected(ref value);
			}
		}

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x06000139 RID: 313
		// (set) Token: 0x0600013A RID: 314
		public extern float radius { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x0600013B RID: 315
		// (set) Token: 0x0600013C RID: 316
		public extern float height { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x0600013D RID: 317
		// (set) Token: 0x0600013E RID: 318
		public extern int direction { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600013F RID: 319 RVA: 0x000031AC File Offset: 0x000013AC
		internal Vector2 GetGlobalExtents()
		{
			Vector2 result;
			this.GetGlobalExtents_Injected(out result);
			return result;
		}

		// Token: 0x06000140 RID: 320 RVA: 0x000031C4 File Offset: 0x000013C4
		internal Matrix4x4 CalculateTransform()
		{
			Matrix4x4 result;
			this.CalculateTransform_Injected(out result);
			return result;
		}

		// Token: 0x06000141 RID: 321
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_center_Injected(out Vector3 ret);

		// Token: 0x06000142 RID: 322
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_center_Injected(ref Vector3 value);

		// Token: 0x06000143 RID: 323
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetGlobalExtents_Injected(out Vector2 ret);

		// Token: 0x06000144 RID: 324
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void CalculateTransform_Injected(out Matrix4x4 ret);
	}
}
