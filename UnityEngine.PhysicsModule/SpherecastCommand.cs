﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using Unity.Jobs.LowLevel.Unsafe;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200002C RID: 44
	[NativeHeader("Runtime/Jobs/ScriptBindings/JobsBindingsTypes.h")]
	[NativeHeader("Runtime/Dynamics/BatchCommands/SpherecastCommand.h")]
	public struct SpherecastCommand
	{
		// Token: 0x0600034E RID: 846 RVA: 0x000057D1 File Offset: 0x000039D1
		public SpherecastCommand(Vector3 origin, float radius, Vector3 direction, float distance = 3.4028235E+38f, int layerMask = -5)
		{
			this.origin = origin;
			this.direction = direction;
			this.radius = radius;
			this.distance = distance;
			this.layerMask = layerMask;
			this.maxHits = 1;
		}

		// Token: 0x170000EB RID: 235
		// (get) Token: 0x0600034F RID: 847 RVA: 0x00005800 File Offset: 0x00003A00
		// (set) Token: 0x06000350 RID: 848 RVA: 0x0000581A File Offset: 0x00003A1A
		public Vector3 origin
		{
			[CompilerGenerated]
			get
			{
				return this.<origin>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<origin>k__BackingField = value;
			}
		}

		// Token: 0x170000EC RID: 236
		// (get) Token: 0x06000351 RID: 849 RVA: 0x00005824 File Offset: 0x00003A24
		// (set) Token: 0x06000352 RID: 850 RVA: 0x0000583E File Offset: 0x00003A3E
		public float radius
		{
			[CompilerGenerated]
			get
			{
				return this.<radius>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<radius>k__BackingField = value;
			}
		}

		// Token: 0x170000ED RID: 237
		// (get) Token: 0x06000353 RID: 851 RVA: 0x00005848 File Offset: 0x00003A48
		// (set) Token: 0x06000354 RID: 852 RVA: 0x00005862 File Offset: 0x00003A62
		public Vector3 direction
		{
			[CompilerGenerated]
			get
			{
				return this.<direction>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<direction>k__BackingField = value;
			}
		}

		// Token: 0x170000EE RID: 238
		// (get) Token: 0x06000355 RID: 853 RVA: 0x0000586C File Offset: 0x00003A6C
		// (set) Token: 0x06000356 RID: 854 RVA: 0x00005886 File Offset: 0x00003A86
		public float distance
		{
			[CompilerGenerated]
			get
			{
				return this.<distance>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<distance>k__BackingField = value;
			}
		}

		// Token: 0x170000EF RID: 239
		// (get) Token: 0x06000357 RID: 855 RVA: 0x00005890 File Offset: 0x00003A90
		// (set) Token: 0x06000358 RID: 856 RVA: 0x000058AA File Offset: 0x00003AAA
		public int layerMask
		{
			[CompilerGenerated]
			get
			{
				return this.<layerMask>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<layerMask>k__BackingField = value;
			}
		}

		// Token: 0x170000F0 RID: 240
		// (get) Token: 0x06000359 RID: 857 RVA: 0x000058B4 File Offset: 0x00003AB4
		// (set) Token: 0x0600035A RID: 858 RVA: 0x000058CE File Offset: 0x00003ACE
		internal int maxHits
		{
			[CompilerGenerated]
			get
			{
				return this.<maxHits>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<maxHits>k__BackingField = value;
			}
		}

		// Token: 0x0600035B RID: 859 RVA: 0x000058D8 File Offset: 0x00003AD8
		public static JobHandle ScheduleBatch(NativeArray<SpherecastCommand> commands, NativeArray<RaycastHit> results, int minCommandsPerJob, JobHandle dependsOn = default(JobHandle))
		{
			BatchQueryJob<SpherecastCommand, RaycastHit> batchQueryJob = new BatchQueryJob<SpherecastCommand, RaycastHit>(commands, results);
			JobsUtility.JobScheduleParameters jobScheduleParameters = new JobsUtility.JobScheduleParameters(UnsafeUtility.AddressOf<BatchQueryJob<SpherecastCommand, RaycastHit>>(ref batchQueryJob), BatchQueryJobStruct<BatchQueryJob<SpherecastCommand, RaycastHit>>.Initialize(), dependsOn, ScheduleMode.Batched);
			return SpherecastCommand.ScheduleSpherecastBatch(ref jobScheduleParameters, NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks<SpherecastCommand>(commands), commands.Length, NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks<RaycastHit>(results), results.Length, minCommandsPerJob);
		}

		// Token: 0x0600035C RID: 860 RVA: 0x00005930 File Offset: 0x00003B30
		[FreeFunction("ScheduleSpherecastCommandBatch")]
		private unsafe static JobHandle ScheduleSpherecastBatch(ref JobsUtility.JobScheduleParameters parameters, void* commands, int commandLen, void* result, int resultLen, int minCommandsPerJob)
		{
			JobHandle result2;
			SpherecastCommand.ScheduleSpherecastBatch_Injected(ref parameters, commands, commandLen, result, resultLen, minCommandsPerJob, out result2);
			return result2;
		}

		// Token: 0x0600035D RID: 861
		[MethodImpl(MethodImplOptions.InternalCall)]
		private unsafe static extern void ScheduleSpherecastBatch_Injected(ref JobsUtility.JobScheduleParameters parameters, void* commands, int commandLen, void* result, int resultLen, int minCommandsPerJob, out JobHandle ret);

		// Token: 0x04000084 RID: 132
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Vector3 <origin>k__BackingField;

		// Token: 0x04000085 RID: 133
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private float <radius>k__BackingField;

		// Token: 0x04000086 RID: 134
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Vector3 <direction>k__BackingField;

		// Token: 0x04000087 RID: 135
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private float <distance>k__BackingField;

		// Token: 0x04000088 RID: 136
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <layerMask>k__BackingField;

		// Token: 0x04000089 RID: 137
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <maxHits>k__BackingField;
	}
}
