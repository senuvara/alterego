﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using Unity.Jobs.LowLevel.Unsafe;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200002B RID: 43
	[NativeHeader("Runtime/Dynamics/BatchCommands/RaycastCommand.h")]
	[NativeHeader("Runtime/Jobs/ScriptBindings/JobsBindingsTypes.h")]
	public struct RaycastCommand
	{
		// Token: 0x06000340 RID: 832 RVA: 0x0000567F File Offset: 0x0000387F
		public RaycastCommand(Vector3 from, Vector3 direction, float distance = 3.4028235E+38f, int layerMask = -5, int maxHits = 1)
		{
			this.from = from;
			this.direction = direction;
			this.distance = distance;
			this.layerMask = layerMask;
			this.maxHits = maxHits;
		}

		// Token: 0x170000E6 RID: 230
		// (get) Token: 0x06000341 RID: 833 RVA: 0x000056A8 File Offset: 0x000038A8
		// (set) Token: 0x06000342 RID: 834 RVA: 0x000056C2 File Offset: 0x000038C2
		public Vector3 from
		{
			[CompilerGenerated]
			get
			{
				return this.<from>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<from>k__BackingField = value;
			}
		}

		// Token: 0x170000E7 RID: 231
		// (get) Token: 0x06000343 RID: 835 RVA: 0x000056CC File Offset: 0x000038CC
		// (set) Token: 0x06000344 RID: 836 RVA: 0x000056E6 File Offset: 0x000038E6
		public Vector3 direction
		{
			[CompilerGenerated]
			get
			{
				return this.<direction>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<direction>k__BackingField = value;
			}
		}

		// Token: 0x170000E8 RID: 232
		// (get) Token: 0x06000345 RID: 837 RVA: 0x000056F0 File Offset: 0x000038F0
		// (set) Token: 0x06000346 RID: 838 RVA: 0x0000570A File Offset: 0x0000390A
		public float distance
		{
			[CompilerGenerated]
			get
			{
				return this.<distance>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<distance>k__BackingField = value;
			}
		}

		// Token: 0x170000E9 RID: 233
		// (get) Token: 0x06000347 RID: 839 RVA: 0x00005714 File Offset: 0x00003914
		// (set) Token: 0x06000348 RID: 840 RVA: 0x0000572E File Offset: 0x0000392E
		public int layerMask
		{
			[CompilerGenerated]
			get
			{
				return this.<layerMask>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<layerMask>k__BackingField = value;
			}
		}

		// Token: 0x170000EA RID: 234
		// (get) Token: 0x06000349 RID: 841 RVA: 0x00005738 File Offset: 0x00003938
		// (set) Token: 0x0600034A RID: 842 RVA: 0x00005752 File Offset: 0x00003952
		public int maxHits
		{
			[CompilerGenerated]
			get
			{
				return this.<maxHits>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<maxHits>k__BackingField = value;
			}
		}

		// Token: 0x0600034B RID: 843 RVA: 0x0000575C File Offset: 0x0000395C
		public static JobHandle ScheduleBatch(NativeArray<RaycastCommand> commands, NativeArray<RaycastHit> results, int minCommandsPerJob, JobHandle dependsOn = default(JobHandle))
		{
			BatchQueryJob<RaycastCommand, RaycastHit> batchQueryJob = new BatchQueryJob<RaycastCommand, RaycastHit>(commands, results);
			JobsUtility.JobScheduleParameters jobScheduleParameters = new JobsUtility.JobScheduleParameters(UnsafeUtility.AddressOf<BatchQueryJob<RaycastCommand, RaycastHit>>(ref batchQueryJob), BatchQueryJobStruct<BatchQueryJob<RaycastCommand, RaycastHit>>.Initialize(), dependsOn, ScheduleMode.Batched);
			return RaycastCommand.ScheduleRaycastBatch(ref jobScheduleParameters, NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks<RaycastCommand>(commands), commands.Length, NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks<RaycastHit>(results), results.Length, minCommandsPerJob);
		}

		// Token: 0x0600034C RID: 844 RVA: 0x000057B4 File Offset: 0x000039B4
		[FreeFunction("ScheduleRaycastCommandBatch")]
		private unsafe static JobHandle ScheduleRaycastBatch(ref JobsUtility.JobScheduleParameters parameters, void* commands, int commandLen, void* result, int resultLen, int minCommandsPerJob)
		{
			JobHandle result2;
			RaycastCommand.ScheduleRaycastBatch_Injected(ref parameters, commands, commandLen, result, resultLen, minCommandsPerJob, out result2);
			return result2;
		}

		// Token: 0x0600034D RID: 845
		[MethodImpl(MethodImplOptions.InternalCall)]
		private unsafe static extern void ScheduleRaycastBatch_Injected(ref JobsUtility.JobScheduleParameters parameters, void* commands, int commandLen, void* result, int resultLen, int minCommandsPerJob, out JobHandle ret);

		// Token: 0x0400007F RID: 127
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Vector3 <from>k__BackingField;

		// Token: 0x04000080 RID: 128
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Vector3 <direction>k__BackingField;

		// Token: 0x04000081 RID: 129
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private float <distance>k__BackingField;

		// Token: 0x04000082 RID: 130
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <layerMask>k__BackingField;

		// Token: 0x04000083 RID: 131
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <maxHits>k__BackingField;
	}
}
