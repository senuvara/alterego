﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using Unity.Jobs.LowLevel.Unsafe;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200002E RID: 46
	[NativeHeader("Runtime/Dynamics/BatchCommands/BoxcastCommand.h")]
	[NativeHeader("Runtime/Jobs/ScriptBindings/JobsBindingsTypes.h")]
	public struct BoxcastCommand
	{
		// Token: 0x06000370 RID: 880 RVA: 0x00005AF5 File Offset: 0x00003CF5
		public BoxcastCommand(Vector3 center, Vector3 halfExtents, Quaternion orientation, Vector3 direction, float distance = 3.4028235E+38f, int layerMask = -5)
		{
			this.center = center;
			this.halfExtents = halfExtents;
			this.orientation = orientation;
			this.direction = direction;
			this.distance = distance;
			this.layerMask = layerMask;
			this.maxHits = 1;
		}

		// Token: 0x170000F8 RID: 248
		// (get) Token: 0x06000371 RID: 881 RVA: 0x00005B2C File Offset: 0x00003D2C
		// (set) Token: 0x06000372 RID: 882 RVA: 0x00005B46 File Offset: 0x00003D46
		public Vector3 center
		{
			[CompilerGenerated]
			get
			{
				return this.<center>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<center>k__BackingField = value;
			}
		}

		// Token: 0x170000F9 RID: 249
		// (get) Token: 0x06000373 RID: 883 RVA: 0x00005B50 File Offset: 0x00003D50
		// (set) Token: 0x06000374 RID: 884 RVA: 0x00005B6A File Offset: 0x00003D6A
		public Vector3 halfExtents
		{
			[CompilerGenerated]
			get
			{
				return this.<halfExtents>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<halfExtents>k__BackingField = value;
			}
		}

		// Token: 0x170000FA RID: 250
		// (get) Token: 0x06000375 RID: 885 RVA: 0x00005B74 File Offset: 0x00003D74
		// (set) Token: 0x06000376 RID: 886 RVA: 0x00005B8E File Offset: 0x00003D8E
		public Quaternion orientation
		{
			[CompilerGenerated]
			get
			{
				return this.<orientation>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<orientation>k__BackingField = value;
			}
		}

		// Token: 0x170000FB RID: 251
		// (get) Token: 0x06000377 RID: 887 RVA: 0x00005B98 File Offset: 0x00003D98
		// (set) Token: 0x06000378 RID: 888 RVA: 0x00005BB2 File Offset: 0x00003DB2
		public Vector3 direction
		{
			[CompilerGenerated]
			get
			{
				return this.<direction>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<direction>k__BackingField = value;
			}
		}

		// Token: 0x170000FC RID: 252
		// (get) Token: 0x06000379 RID: 889 RVA: 0x00005BBC File Offset: 0x00003DBC
		// (set) Token: 0x0600037A RID: 890 RVA: 0x00005BD6 File Offset: 0x00003DD6
		public float distance
		{
			[CompilerGenerated]
			get
			{
				return this.<distance>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<distance>k__BackingField = value;
			}
		}

		// Token: 0x170000FD RID: 253
		// (get) Token: 0x0600037B RID: 891 RVA: 0x00005BE0 File Offset: 0x00003DE0
		// (set) Token: 0x0600037C RID: 892 RVA: 0x00005BFA File Offset: 0x00003DFA
		public int layerMask
		{
			[CompilerGenerated]
			get
			{
				return this.<layerMask>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<layerMask>k__BackingField = value;
			}
		}

		// Token: 0x170000FE RID: 254
		// (get) Token: 0x0600037D RID: 893 RVA: 0x00005C04 File Offset: 0x00003E04
		// (set) Token: 0x0600037E RID: 894 RVA: 0x00005C1E File Offset: 0x00003E1E
		internal int maxHits
		{
			[CompilerGenerated]
			get
			{
				return this.<maxHits>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<maxHits>k__BackingField = value;
			}
		}

		// Token: 0x0600037F RID: 895 RVA: 0x00005C28 File Offset: 0x00003E28
		public static JobHandle ScheduleBatch(NativeArray<BoxcastCommand> commands, NativeArray<RaycastHit> results, int minCommandsPerJob, JobHandle dependsOn = default(JobHandle))
		{
			BatchQueryJob<BoxcastCommand, RaycastHit> batchQueryJob = new BatchQueryJob<BoxcastCommand, RaycastHit>(commands, results);
			JobsUtility.JobScheduleParameters jobScheduleParameters = new JobsUtility.JobScheduleParameters(UnsafeUtility.AddressOf<BatchQueryJob<BoxcastCommand, RaycastHit>>(ref batchQueryJob), BatchQueryJobStruct<BatchQueryJob<BoxcastCommand, RaycastHit>>.Initialize(), dependsOn, ScheduleMode.Batched);
			return BoxcastCommand.ScheduleBoxcastBatch(ref jobScheduleParameters, NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks<BoxcastCommand>(commands), commands.Length, NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks<RaycastHit>(results), results.Length, minCommandsPerJob);
		}

		// Token: 0x06000380 RID: 896 RVA: 0x00005C80 File Offset: 0x00003E80
		[FreeFunction("ScheduleBoxcastCommandBatch")]
		private unsafe static JobHandle ScheduleBoxcastBatch(ref JobsUtility.JobScheduleParameters parameters, void* commands, int commandLen, void* result, int resultLen, int minCommandsPerJob)
		{
			JobHandle result2;
			BoxcastCommand.ScheduleBoxcastBatch_Injected(ref parameters, commands, commandLen, result, resultLen, minCommandsPerJob, out result2);
			return result2;
		}

		// Token: 0x06000381 RID: 897
		[MethodImpl(MethodImplOptions.InternalCall)]
		private unsafe static extern void ScheduleBoxcastBatch_Injected(ref JobsUtility.JobScheduleParameters parameters, void* commands, int commandLen, void* result, int resultLen, int minCommandsPerJob, out JobHandle ret);

		// Token: 0x04000091 RID: 145
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Vector3 <center>k__BackingField;

		// Token: 0x04000092 RID: 146
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Vector3 <halfExtents>k__BackingField;

		// Token: 0x04000093 RID: 147
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Quaternion <orientation>k__BackingField;

		// Token: 0x04000094 RID: 148
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Vector3 <direction>k__BackingField;

		// Token: 0x04000095 RID: 149
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private float <distance>k__BackingField;

		// Token: 0x04000096 RID: 150
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <layerMask>k__BackingField;

		// Token: 0x04000097 RID: 151
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <maxHits>k__BackingField;
	}
}
