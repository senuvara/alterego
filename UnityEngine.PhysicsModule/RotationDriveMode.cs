﻿using System;

namespace UnityEngine
{
	// Token: 0x02000016 RID: 22
	public enum RotationDriveMode
	{
		// Token: 0x04000067 RID: 103
		XYAndZ,
		// Token: 0x04000068 RID: 104
		Slerp
	}
}
