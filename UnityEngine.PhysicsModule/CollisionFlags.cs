﻿using System;

namespace UnityEngine
{
	// Token: 0x02000012 RID: 18
	public enum CollisionFlags
	{
		// Token: 0x04000052 RID: 82
		None,
		// Token: 0x04000053 RID: 83
		Sides,
		// Token: 0x04000054 RID: 84
		Above,
		// Token: 0x04000055 RID: 85
		Below = 4,
		// Token: 0x04000056 RID: 86
		CollidedSides = 1,
		// Token: 0x04000057 RID: 87
		CollidedAbove,
		// Token: 0x04000058 RID: 88
		CollidedBelow = 4
	}
}
