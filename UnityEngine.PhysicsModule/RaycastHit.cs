﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000018 RID: 24
	[NativeHeader("Runtime/Dynamics/RaycastHit.h")]
	[NativeHeader("PhysicsScriptingClasses.h")]
	[UsedByNativeCode]
	[NativeHeader("Runtime/Interfaces/IRaycast.h")]
	public struct RaycastHit
	{
		// Token: 0x17000039 RID: 57
		// (get) Token: 0x06000066 RID: 102 RVA: 0x000028E0 File Offset: 0x00000AE0
		public Collider collider
		{
			get
			{
				return Object.FindObjectFromInstanceID(this.m_Collider) as Collider;
			}
		}

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x06000067 RID: 103 RVA: 0x00002908 File Offset: 0x00000B08
		// (set) Token: 0x06000068 RID: 104 RVA: 0x00002923 File Offset: 0x00000B23
		public Vector3 point
		{
			get
			{
				return this.m_Point;
			}
			set
			{
				this.m_Point = value;
			}
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x06000069 RID: 105 RVA: 0x00002930 File Offset: 0x00000B30
		// (set) Token: 0x0600006A RID: 106 RVA: 0x0000294B File Offset: 0x00000B4B
		public Vector3 normal
		{
			get
			{
				return this.m_Normal;
			}
			set
			{
				this.m_Normal = value;
			}
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x0600006B RID: 107 RVA: 0x00002958 File Offset: 0x00000B58
		// (set) Token: 0x0600006C RID: 108 RVA: 0x000029A5 File Offset: 0x00000BA5
		public Vector3 barycentricCoordinate
		{
			get
			{
				return new Vector3(1f - (this.m_UV.y + this.m_UV.x), this.m_UV.x, this.m_UV.y);
			}
			set
			{
				this.m_UV = value;
			}
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x0600006D RID: 109 RVA: 0x000029B4 File Offset: 0x00000BB4
		// (set) Token: 0x0600006E RID: 110 RVA: 0x000029CF File Offset: 0x00000BCF
		public float distance
		{
			get
			{
				return this.m_Distance;
			}
			set
			{
				this.m_Distance = value;
			}
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x0600006F RID: 111 RVA: 0x000029DC File Offset: 0x00000BDC
		public int triangleIndex
		{
			get
			{
				return (int)this.m_FaceID;
			}
		}

		// Token: 0x06000070 RID: 112 RVA: 0x000029F8 File Offset: 0x00000BF8
		[FreeFunction]
		private static Vector2 CalculateRaycastTexCoord(Collider collider, Vector2 uv, Vector3 pos, uint face, int textcoord)
		{
			Vector2 result;
			RaycastHit.CalculateRaycastTexCoord_Injected(collider, ref uv, ref pos, face, textcoord, out result);
			return result;
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x06000071 RID: 113 RVA: 0x00002A18 File Offset: 0x00000C18
		public Vector2 textureCoord
		{
			get
			{
				return RaycastHit.CalculateRaycastTexCoord(this.collider, this.m_UV, this.m_Point, this.m_FaceID, 0);
			}
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x06000072 RID: 114 RVA: 0x00002A4C File Offset: 0x00000C4C
		public Vector2 textureCoord2
		{
			get
			{
				return RaycastHit.CalculateRaycastTexCoord(this.collider, this.m_UV, this.m_Point, this.m_FaceID, 1);
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x06000073 RID: 115 RVA: 0x00002A80 File Offset: 0x00000C80
		[Obsolete("Use textureCoord2 instead. (UnityUpgradable) -> textureCoord2")]
		public Vector2 textureCoord1
		{
			get
			{
				return this.textureCoord2;
			}
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x06000074 RID: 116 RVA: 0x00002A9C File Offset: 0x00000C9C
		public Transform transform
		{
			get
			{
				Rigidbody rigidbody = this.rigidbody;
				Transform result;
				if (rigidbody != null)
				{
					result = rigidbody.transform;
				}
				else if (this.collider != null)
				{
					result = this.collider.transform;
				}
				else
				{
					result = null;
				}
				return result;
			}
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x06000075 RID: 117 RVA: 0x00002AF4 File Offset: 0x00000CF4
		public Rigidbody rigidbody
		{
			get
			{
				return (!(this.collider != null)) ? null : this.collider.attachedRigidbody;
			}
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x06000076 RID: 118 RVA: 0x00002B2C File Offset: 0x00000D2C
		public Vector2 lightmapCoord
		{
			get
			{
				Vector2 result = RaycastHit.CalculateRaycastTexCoord(this.collider, this.m_UV, this.m_Point, this.m_FaceID, 1);
				if (this.collider.GetComponent<Renderer>() != null)
				{
					Vector4 lightmapScaleOffset = this.collider.GetComponent<Renderer>().lightmapScaleOffset;
					result.x = result.x * lightmapScaleOffset.x + lightmapScaleOffset.z;
					result.y = result.y * lightmapScaleOffset.y + lightmapScaleOffset.w;
				}
				return result;
			}
		}

		// Token: 0x06000077 RID: 119
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void CalculateRaycastTexCoord_Injected(Collider collider, ref Vector2 uv, ref Vector3 pos, uint face, int textcoord, out Vector2 ret);

		// Token: 0x04000069 RID: 105
		[NativeName("point")]
		internal Vector3 m_Point;

		// Token: 0x0400006A RID: 106
		[NativeName("normal")]
		internal Vector3 m_Normal;

		// Token: 0x0400006B RID: 107
		[NativeName("faceID")]
		internal uint m_FaceID;

		// Token: 0x0400006C RID: 108
		[NativeName("distance")]
		internal float m_Distance;

		// Token: 0x0400006D RID: 109
		[NativeName("uv")]
		internal Vector2 m_UV;

		// Token: 0x0400006E RID: 110
		[NativeName("collider")]
		internal int m_Collider;
	}
}
