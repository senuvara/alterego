﻿using System;

namespace UnityEngine
{
	// Token: 0x02000004 RID: 4
	[Flags]
	[Obsolete("JointDriveMode is no longer supported")]
	public enum JointDriveMode
	{
		// Token: 0x04000012 RID: 18
		[Obsolete("JointDriveMode.None is no longer supported")]
		None = 0,
		// Token: 0x04000013 RID: 19
		[Obsolete("JointDriveMode.Position is no longer supported")]
		Position = 1,
		// Token: 0x04000014 RID: 20
		[Obsolete("JointDriveMode.Velocity is no longer supported")]
		Velocity = 2,
		// Token: 0x04000015 RID: 21
		[Obsolete("JointDriveMode.PositionAndvelocity is no longer supported")]
		PositionAndVelocity = 3
	}
}
