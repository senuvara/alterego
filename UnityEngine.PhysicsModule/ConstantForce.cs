﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000020 RID: 32
	[NativeHeader("Runtime/Dynamics/ConstantForce.h")]
	[RequireComponent(typeof(Rigidbody))]
	public class ConstantForce : Behaviour
	{
		// Token: 0x06000157 RID: 343 RVA: 0x00003278 File Offset: 0x00001478
		public ConstantForce()
		{
		}

		// Token: 0x17000083 RID: 131
		// (get) Token: 0x06000158 RID: 344 RVA: 0x00003280 File Offset: 0x00001480
		// (set) Token: 0x06000159 RID: 345 RVA: 0x00003296 File Offset: 0x00001496
		public Vector3 force
		{
			get
			{
				Vector3 result;
				this.get_force_Injected(out result);
				return result;
			}
			set
			{
				this.set_force_Injected(ref value);
			}
		}

		// Token: 0x17000084 RID: 132
		// (get) Token: 0x0600015A RID: 346 RVA: 0x000032A0 File Offset: 0x000014A0
		// (set) Token: 0x0600015B RID: 347 RVA: 0x000032B6 File Offset: 0x000014B6
		public Vector3 relativeForce
		{
			get
			{
				Vector3 result;
				this.get_relativeForce_Injected(out result);
				return result;
			}
			set
			{
				this.set_relativeForce_Injected(ref value);
			}
		}

		// Token: 0x17000085 RID: 133
		// (get) Token: 0x0600015C RID: 348 RVA: 0x000032C0 File Offset: 0x000014C0
		// (set) Token: 0x0600015D RID: 349 RVA: 0x000032D6 File Offset: 0x000014D6
		public Vector3 torque
		{
			get
			{
				Vector3 result;
				this.get_torque_Injected(out result);
				return result;
			}
			set
			{
				this.set_torque_Injected(ref value);
			}
		}

		// Token: 0x17000086 RID: 134
		// (get) Token: 0x0600015E RID: 350 RVA: 0x000032E0 File Offset: 0x000014E0
		// (set) Token: 0x0600015F RID: 351 RVA: 0x000032F6 File Offset: 0x000014F6
		public Vector3 relativeTorque
		{
			get
			{
				Vector3 result;
				this.get_relativeTorque_Injected(out result);
				return result;
			}
			set
			{
				this.set_relativeTorque_Injected(ref value);
			}
		}

		// Token: 0x06000160 RID: 352
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_force_Injected(out Vector3 ret);

		// Token: 0x06000161 RID: 353
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_force_Injected(ref Vector3 value);

		// Token: 0x06000162 RID: 354
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_relativeForce_Injected(out Vector3 ret);

		// Token: 0x06000163 RID: 355
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_relativeForce_Injected(ref Vector3 value);

		// Token: 0x06000164 RID: 356
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_torque_Injected(out Vector3 ret);

		// Token: 0x06000165 RID: 357
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_torque_Injected(ref Vector3 value);

		// Token: 0x06000166 RID: 358
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_relativeTorque_Injected(out Vector3 ret);

		// Token: 0x06000167 RID: 359
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_relativeTorque_Injected(ref Vector3 value);
	}
}
