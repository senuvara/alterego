﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000027 RID: 39
	[UsedByNativeCode]
	[NativeHeader("Runtime/Dynamics/MessageParameters.h")]
	public struct ContactPoint
	{
		// Token: 0x170000CA RID: 202
		// (get) Token: 0x06000231 RID: 561 RVA: 0x00003760 File Offset: 0x00001960
		public Vector3 point
		{
			get
			{
				return this.m_Point;
			}
		}

		// Token: 0x170000CB RID: 203
		// (get) Token: 0x06000232 RID: 562 RVA: 0x0000377C File Offset: 0x0000197C
		public Vector3 normal
		{
			get
			{
				return this.m_Normal;
			}
		}

		// Token: 0x170000CC RID: 204
		// (get) Token: 0x06000233 RID: 563 RVA: 0x00003798 File Offset: 0x00001998
		public Collider thisCollider
		{
			get
			{
				return ContactPoint.GetColliderByInstanceID(this.m_ThisColliderInstanceID);
			}
		}

		// Token: 0x170000CD RID: 205
		// (get) Token: 0x06000234 RID: 564 RVA: 0x000037B8 File Offset: 0x000019B8
		public Collider otherCollider
		{
			get
			{
				return ContactPoint.GetColliderByInstanceID(this.m_OtherColliderInstanceID);
			}
		}

		// Token: 0x170000CE RID: 206
		// (get) Token: 0x06000235 RID: 565 RVA: 0x000037D8 File Offset: 0x000019D8
		public float separation
		{
			get
			{
				return this.m_Separation;
			}
		}

		// Token: 0x06000236 RID: 566
		[FreeFunction]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Collider GetColliderByInstanceID(int instanceID);

		// Token: 0x04000072 RID: 114
		internal Vector3 m_Point;

		// Token: 0x04000073 RID: 115
		internal Vector3 m_Normal;

		// Token: 0x04000074 RID: 116
		internal int m_ThisColliderInstanceID;

		// Token: 0x04000075 RID: 117
		internal int m_OtherColliderInstanceID;

		// Token: 0x04000076 RID: 118
		internal float m_Separation;
	}
}
