﻿using System;

namespace UnityEngine
{
	// Token: 0x02000010 RID: 16
	public enum PhysicMaterialCombine
	{
		// Token: 0x04000046 RID: 70
		Average,
		// Token: 0x04000047 RID: 71
		Minimum = 2,
		// Token: 0x04000048 RID: 72
		Multiply = 1,
		// Token: 0x04000049 RID: 73
		Maximum = 3
	}
}
