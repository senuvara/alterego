﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200001A RID: 26
	[RequiredByNativeCode]
	[RequireComponent(typeof(Transform))]
	[NativeHeader("Runtime/Dynamics/Collider.h")]
	public class Collider : Component
	{
		// Token: 0x060000F6 RID: 246 RVA: 0x00002BC6 File Offset: 0x00000DC6
		public Collider()
		{
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x060000F7 RID: 247
		// (set) Token: 0x060000F8 RID: 248
		public extern bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x060000F9 RID: 249
		public extern Rigidbody attachedRigidbody { [NativeMethod("GetRigidbody")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x060000FA RID: 250
		// (set) Token: 0x060000FB RID: 251
		public extern bool isTrigger { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x060000FC RID: 252
		// (set) Token: 0x060000FD RID: 253
		public extern float contactOffset { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060000FE RID: 254 RVA: 0x0000303C File Offset: 0x0000123C
		public Vector3 ClosestPoint(Vector3 position)
		{
			Vector3 result;
			this.ClosestPoint_Injected(ref position, out result);
			return result;
		}

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x060000FF RID: 255 RVA: 0x00003054 File Offset: 0x00001254
		public Bounds bounds
		{
			get
			{
				Bounds result;
				this.get_bounds_Injected(out result);
				return result;
			}
		}

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x06000100 RID: 256
		// (set) Token: 0x06000101 RID: 257
		[NativeMethod("Material")]
		public extern PhysicMaterial sharedMaterial { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x06000102 RID: 258
		// (set) Token: 0x06000103 RID: 259
		public extern PhysicMaterial material { [NativeMethod("GetClonedMaterial")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeMethod("SetMaterial")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000104 RID: 260 RVA: 0x0000306C File Offset: 0x0000126C
		private RaycastHit Raycast(Ray ray, float maxDistance, ref bool hasHit)
		{
			RaycastHit result;
			this.Raycast_Injected(ref ray, maxDistance, ref hasHit, out result);
			return result;
		}

		// Token: 0x06000105 RID: 261 RVA: 0x00003088 File Offset: 0x00001288
		public bool Raycast(Ray ray, out RaycastHit hitInfo, float maxDistance)
		{
			bool result = false;
			hitInfo = this.Raycast(ray, maxDistance, ref result);
			return result;
		}

		// Token: 0x06000106 RID: 262 RVA: 0x000030B0 File Offset: 0x000012B0
		[NativeName("ClosestPointOnBounds")]
		private void Internal_ClosestPointOnBounds(Vector3 point, ref Vector3 outPos, ref float distance)
		{
			this.Internal_ClosestPointOnBounds_Injected(ref point, ref outPos, ref distance);
		}

		// Token: 0x06000107 RID: 263 RVA: 0x000030BC File Offset: 0x000012BC
		public Vector3 ClosestPointOnBounds(Vector3 position)
		{
			float num = 0f;
			Vector3 zero = Vector3.zero;
			this.Internal_ClosestPointOnBounds(position, ref zero, ref num);
			return zero;
		}

		// Token: 0x06000108 RID: 264
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ClosestPoint_Injected(ref Vector3 position, out Vector3 ret);

		// Token: 0x06000109 RID: 265
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_bounds_Injected(out Bounds ret);

		// Token: 0x0600010A RID: 266
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Raycast_Injected(ref Ray ray, float maxDistance, ref bool hasHit, out RaycastHit ret);

		// Token: 0x0600010B RID: 267
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_ClosestPointOnBounds_Injected(ref Vector3 point, ref Vector3 outPos, ref float distance);
	}
}
