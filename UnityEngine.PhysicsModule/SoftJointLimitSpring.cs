﻿using System;

namespace UnityEngine
{
	// Token: 0x02000009 RID: 9
	public struct SoftJointLimitSpring
	{
		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000017 RID: 23 RVA: 0x000021E8 File Offset: 0x000003E8
		// (set) Token: 0x06000018 RID: 24 RVA: 0x00002203 File Offset: 0x00000403
		public float spring
		{
			get
			{
				return this.m_Spring;
			}
			set
			{
				this.m_Spring = value;
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000019 RID: 25 RVA: 0x00002210 File Offset: 0x00000410
		// (set) Token: 0x0600001A RID: 26 RVA: 0x0000222B File Offset: 0x0000042B
		public float damper
		{
			get
			{
				return this.m_Damper;
			}
			set
			{
				this.m_Damper = value;
			}
		}

		// Token: 0x04000028 RID: 40
		private float m_Spring;

		// Token: 0x04000029 RID: 41
		private float m_Damper;
	}
}
