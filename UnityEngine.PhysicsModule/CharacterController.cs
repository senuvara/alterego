﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200001B RID: 27
	[NativeHeader("Runtime/Dynamics/CharacterController.h")]
	public class CharacterController : Collider
	{
		// Token: 0x0600010C RID: 268 RVA: 0x000030E9 File Offset: 0x000012E9
		public CharacterController()
		{
		}

		// Token: 0x0600010D RID: 269 RVA: 0x000030F1 File Offset: 0x000012F1
		public bool SimpleMove(Vector3 speed)
		{
			return this.SimpleMove_Injected(ref speed);
		}

		// Token: 0x0600010E RID: 270 RVA: 0x000030FB File Offset: 0x000012FB
		public CollisionFlags Move(Vector3 motion)
		{
			return this.Move_Injected(ref motion);
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x0600010F RID: 271 RVA: 0x00003108 File Offset: 0x00001308
		public Vector3 velocity
		{
			get
			{
				Vector3 result;
				this.get_velocity_Injected(out result);
				return result;
			}
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x06000110 RID: 272
		public extern bool isGrounded { [NativeName("IsGrounded")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x06000111 RID: 273
		public extern CollisionFlags collisionFlags { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x06000112 RID: 274
		// (set) Token: 0x06000113 RID: 275
		public extern float radius { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x06000114 RID: 276
		// (set) Token: 0x06000115 RID: 277
		public extern float height { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x06000116 RID: 278 RVA: 0x00003120 File Offset: 0x00001320
		// (set) Token: 0x06000117 RID: 279 RVA: 0x00003136 File Offset: 0x00001336
		public Vector3 center
		{
			get
			{
				Vector3 result;
				this.get_center_Injected(out result);
				return result;
			}
			set
			{
				this.set_center_Injected(ref value);
			}
		}

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x06000118 RID: 280
		// (set) Token: 0x06000119 RID: 281
		public extern float slopeLimit { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x0600011A RID: 282
		// (set) Token: 0x0600011B RID: 283
		public extern float stepOffset { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x0600011C RID: 284
		// (set) Token: 0x0600011D RID: 285
		public extern float skinWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x0600011E RID: 286
		// (set) Token: 0x0600011F RID: 287
		public extern float minMoveDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x06000120 RID: 288
		// (set) Token: 0x06000121 RID: 289
		public extern bool detectCollisions { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x06000122 RID: 290
		// (set) Token: 0x06000123 RID: 291
		public extern bool enableOverlapRecovery { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000124 RID: 292
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool SimpleMove_Injected(ref Vector3 speed);

		// Token: 0x06000125 RID: 293
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern CollisionFlags Move_Injected(ref Vector3 motion);

		// Token: 0x06000126 RID: 294
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_velocity_Injected(out Vector3 ret);

		// Token: 0x06000127 RID: 295
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_center_Injected(out Vector3 ret);

		// Token: 0x06000128 RID: 296
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_center_Injected(ref Vector3 value);
	}
}
