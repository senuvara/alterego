﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000024 RID: 36
	[NativeHeader("Runtime/Dynamics/FixedJoint.h")]
	[NativeClass("Unity::FixedJoint")]
	public class FixedJoint : Joint
	{
		// Token: 0x060001A8 RID: 424 RVA: 0x000033B8 File Offset: 0x000015B8
		public FixedJoint()
		{
		}
	}
}
