﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.SceneManagement;

namespace UnityEngine
{
	// Token: 0x02000029 RID: 41
	public static class PhysicsSceneExtensions
	{
		// Token: 0x0600024D RID: 589 RVA: 0x00003B00 File Offset: 0x00001D00
		public static PhysicsScene GetPhysicsScene(this Scene scene)
		{
			if (!scene.IsValid())
			{
				throw new ArgumentException("Cannot get physics scene; Unity scene is invalid.", "scene");
			}
			PhysicsScene physicsScene_Internal = PhysicsSceneExtensions.GetPhysicsScene_Internal(scene);
			if (physicsScene_Internal.IsValid())
			{
				return physicsScene_Internal;
			}
			throw new Exception("The physics scene associated with the Unity scene is invalid.");
		}

		// Token: 0x0600024E RID: 590 RVA: 0x00003B50 File Offset: 0x00001D50
		[StaticAccessor("GetPhysicsManager()", StaticAccessorType.Dot)]
		[NativeMethod("GetPhysicsSceneFromUnityScene")]
		private static PhysicsScene GetPhysicsScene_Internal(Scene scene)
		{
			PhysicsScene result;
			PhysicsSceneExtensions.GetPhysicsScene_Internal_Injected(ref scene, out result);
			return result;
		}

		// Token: 0x0600024F RID: 591
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetPhysicsScene_Internal_Injected(ref Scene scene, out PhysicsScene ret);
	}
}
