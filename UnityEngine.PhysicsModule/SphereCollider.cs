﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200001F RID: 31
	[RequiredByNativeCode]
	[NativeHeader("Runtime/Dynamics/SphereCollider.h")]
	public class SphereCollider : Collider
	{
		// Token: 0x06000150 RID: 336 RVA: 0x000030E9 File Offset: 0x000012E9
		public SphereCollider()
		{
		}

		// Token: 0x17000081 RID: 129
		// (get) Token: 0x06000151 RID: 337 RVA: 0x00003258 File Offset: 0x00001458
		// (set) Token: 0x06000152 RID: 338 RVA: 0x0000326E File Offset: 0x0000146E
		public Vector3 center
		{
			get
			{
				Vector3 result;
				this.get_center_Injected(out result);
				return result;
			}
			set
			{
				this.set_center_Injected(ref value);
			}
		}

		// Token: 0x17000082 RID: 130
		// (get) Token: 0x06000153 RID: 339
		// (set) Token: 0x06000154 RID: 340
		public extern float radius { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000155 RID: 341
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_center_Injected(out Vector3 ret);

		// Token: 0x06000156 RID: 342
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_center_Injected(ref Vector3 value);
	}
}
