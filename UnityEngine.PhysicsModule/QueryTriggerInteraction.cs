﻿using System;

namespace UnityEngine
{
	// Token: 0x02000013 RID: 19
	public enum QueryTriggerInteraction
	{
		// Token: 0x0400005A RID: 90
		UseGlobal,
		// Token: 0x0400005B RID: 91
		Ignore,
		// Token: 0x0400005C RID: 92
		Collide
	}
}
