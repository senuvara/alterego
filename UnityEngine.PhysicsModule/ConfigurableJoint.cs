﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000026 RID: 38
	[NativeClass("Unity::ConfigurableJoint")]
	[NativeHeader("Runtime/Dynamics/ConfigurableJoint.h")]
	public class ConfigurableJoint : Joint
	{
		// Token: 0x060001CC RID: 460 RVA: 0x000033B8 File Offset: 0x000015B8
		public ConfigurableJoint()
		{
		}

		// Token: 0x170000AB RID: 171
		// (get) Token: 0x060001CD RID: 461 RVA: 0x00003500 File Offset: 0x00001700
		// (set) Token: 0x060001CE RID: 462 RVA: 0x00003516 File Offset: 0x00001716
		public Vector3 secondaryAxis
		{
			get
			{
				Vector3 result;
				this.get_secondaryAxis_Injected(out result);
				return result;
			}
			set
			{
				this.set_secondaryAxis_Injected(ref value);
			}
		}

		// Token: 0x170000AC RID: 172
		// (get) Token: 0x060001CF RID: 463
		// (set) Token: 0x060001D0 RID: 464
		public extern ConfigurableJointMotion xMotion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000AD RID: 173
		// (get) Token: 0x060001D1 RID: 465
		// (set) Token: 0x060001D2 RID: 466
		public extern ConfigurableJointMotion yMotion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000AE RID: 174
		// (get) Token: 0x060001D3 RID: 467
		// (set) Token: 0x060001D4 RID: 468
		public extern ConfigurableJointMotion zMotion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000AF RID: 175
		// (get) Token: 0x060001D5 RID: 469
		// (set) Token: 0x060001D6 RID: 470
		public extern ConfigurableJointMotion angularXMotion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x060001D7 RID: 471
		// (set) Token: 0x060001D8 RID: 472
		public extern ConfigurableJointMotion angularYMotion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x060001D9 RID: 473
		// (set) Token: 0x060001DA RID: 474
		public extern ConfigurableJointMotion angularZMotion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x060001DB RID: 475 RVA: 0x00003520 File Offset: 0x00001720
		// (set) Token: 0x060001DC RID: 476 RVA: 0x00003536 File Offset: 0x00001736
		public SoftJointLimitSpring linearLimitSpring
		{
			get
			{
				SoftJointLimitSpring result;
				this.get_linearLimitSpring_Injected(out result);
				return result;
			}
			set
			{
				this.set_linearLimitSpring_Injected(ref value);
			}
		}

		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x060001DD RID: 477 RVA: 0x00003540 File Offset: 0x00001740
		// (set) Token: 0x060001DE RID: 478 RVA: 0x00003556 File Offset: 0x00001756
		public SoftJointLimitSpring angularXLimitSpring
		{
			get
			{
				SoftJointLimitSpring result;
				this.get_angularXLimitSpring_Injected(out result);
				return result;
			}
			set
			{
				this.set_angularXLimitSpring_Injected(ref value);
			}
		}

		// Token: 0x170000B4 RID: 180
		// (get) Token: 0x060001DF RID: 479 RVA: 0x00003560 File Offset: 0x00001760
		// (set) Token: 0x060001E0 RID: 480 RVA: 0x00003576 File Offset: 0x00001776
		public SoftJointLimitSpring angularYZLimitSpring
		{
			get
			{
				SoftJointLimitSpring result;
				this.get_angularYZLimitSpring_Injected(out result);
				return result;
			}
			set
			{
				this.set_angularYZLimitSpring_Injected(ref value);
			}
		}

		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x060001E1 RID: 481 RVA: 0x00003580 File Offset: 0x00001780
		// (set) Token: 0x060001E2 RID: 482 RVA: 0x00003596 File Offset: 0x00001796
		public SoftJointLimit linearLimit
		{
			get
			{
				SoftJointLimit result;
				this.get_linearLimit_Injected(out result);
				return result;
			}
			set
			{
				this.set_linearLimit_Injected(ref value);
			}
		}

		// Token: 0x170000B6 RID: 182
		// (get) Token: 0x060001E3 RID: 483 RVA: 0x000035A0 File Offset: 0x000017A0
		// (set) Token: 0x060001E4 RID: 484 RVA: 0x000035B6 File Offset: 0x000017B6
		public SoftJointLimit lowAngularXLimit
		{
			get
			{
				SoftJointLimit result;
				this.get_lowAngularXLimit_Injected(out result);
				return result;
			}
			set
			{
				this.set_lowAngularXLimit_Injected(ref value);
			}
		}

		// Token: 0x170000B7 RID: 183
		// (get) Token: 0x060001E5 RID: 485 RVA: 0x000035C0 File Offset: 0x000017C0
		// (set) Token: 0x060001E6 RID: 486 RVA: 0x000035D6 File Offset: 0x000017D6
		public SoftJointLimit highAngularXLimit
		{
			get
			{
				SoftJointLimit result;
				this.get_highAngularXLimit_Injected(out result);
				return result;
			}
			set
			{
				this.set_highAngularXLimit_Injected(ref value);
			}
		}

		// Token: 0x170000B8 RID: 184
		// (get) Token: 0x060001E7 RID: 487 RVA: 0x000035E0 File Offset: 0x000017E0
		// (set) Token: 0x060001E8 RID: 488 RVA: 0x000035F6 File Offset: 0x000017F6
		public SoftJointLimit angularYLimit
		{
			get
			{
				SoftJointLimit result;
				this.get_angularYLimit_Injected(out result);
				return result;
			}
			set
			{
				this.set_angularYLimit_Injected(ref value);
			}
		}

		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x060001E9 RID: 489 RVA: 0x00003600 File Offset: 0x00001800
		// (set) Token: 0x060001EA RID: 490 RVA: 0x00003616 File Offset: 0x00001816
		public SoftJointLimit angularZLimit
		{
			get
			{
				SoftJointLimit result;
				this.get_angularZLimit_Injected(out result);
				return result;
			}
			set
			{
				this.set_angularZLimit_Injected(ref value);
			}
		}

		// Token: 0x170000BA RID: 186
		// (get) Token: 0x060001EB RID: 491 RVA: 0x00003620 File Offset: 0x00001820
		// (set) Token: 0x060001EC RID: 492 RVA: 0x00003636 File Offset: 0x00001836
		public Vector3 targetPosition
		{
			get
			{
				Vector3 result;
				this.get_targetPosition_Injected(out result);
				return result;
			}
			set
			{
				this.set_targetPosition_Injected(ref value);
			}
		}

		// Token: 0x170000BB RID: 187
		// (get) Token: 0x060001ED RID: 493 RVA: 0x00003640 File Offset: 0x00001840
		// (set) Token: 0x060001EE RID: 494 RVA: 0x00003656 File Offset: 0x00001856
		public Vector3 targetVelocity
		{
			get
			{
				Vector3 result;
				this.get_targetVelocity_Injected(out result);
				return result;
			}
			set
			{
				this.set_targetVelocity_Injected(ref value);
			}
		}

		// Token: 0x170000BC RID: 188
		// (get) Token: 0x060001EF RID: 495 RVA: 0x00003660 File Offset: 0x00001860
		// (set) Token: 0x060001F0 RID: 496 RVA: 0x00003676 File Offset: 0x00001876
		public JointDrive xDrive
		{
			get
			{
				JointDrive result;
				this.get_xDrive_Injected(out result);
				return result;
			}
			set
			{
				this.set_xDrive_Injected(ref value);
			}
		}

		// Token: 0x170000BD RID: 189
		// (get) Token: 0x060001F1 RID: 497 RVA: 0x00003680 File Offset: 0x00001880
		// (set) Token: 0x060001F2 RID: 498 RVA: 0x00003696 File Offset: 0x00001896
		public JointDrive yDrive
		{
			get
			{
				JointDrive result;
				this.get_yDrive_Injected(out result);
				return result;
			}
			set
			{
				this.set_yDrive_Injected(ref value);
			}
		}

		// Token: 0x170000BE RID: 190
		// (get) Token: 0x060001F3 RID: 499 RVA: 0x000036A0 File Offset: 0x000018A0
		// (set) Token: 0x060001F4 RID: 500 RVA: 0x000036B6 File Offset: 0x000018B6
		public JointDrive zDrive
		{
			get
			{
				JointDrive result;
				this.get_zDrive_Injected(out result);
				return result;
			}
			set
			{
				this.set_zDrive_Injected(ref value);
			}
		}

		// Token: 0x170000BF RID: 191
		// (get) Token: 0x060001F5 RID: 501 RVA: 0x000036C0 File Offset: 0x000018C0
		// (set) Token: 0x060001F6 RID: 502 RVA: 0x000036D6 File Offset: 0x000018D6
		public Quaternion targetRotation
		{
			get
			{
				Quaternion result;
				this.get_targetRotation_Injected(out result);
				return result;
			}
			set
			{
				this.set_targetRotation_Injected(ref value);
			}
		}

		// Token: 0x170000C0 RID: 192
		// (get) Token: 0x060001F7 RID: 503 RVA: 0x000036E0 File Offset: 0x000018E0
		// (set) Token: 0x060001F8 RID: 504 RVA: 0x000036F6 File Offset: 0x000018F6
		public Vector3 targetAngularVelocity
		{
			get
			{
				Vector3 result;
				this.get_targetAngularVelocity_Injected(out result);
				return result;
			}
			set
			{
				this.set_targetAngularVelocity_Injected(ref value);
			}
		}

		// Token: 0x170000C1 RID: 193
		// (get) Token: 0x060001F9 RID: 505
		// (set) Token: 0x060001FA RID: 506
		public extern RotationDriveMode rotationDriveMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000C2 RID: 194
		// (get) Token: 0x060001FB RID: 507 RVA: 0x00003700 File Offset: 0x00001900
		// (set) Token: 0x060001FC RID: 508 RVA: 0x00003716 File Offset: 0x00001916
		public JointDrive angularXDrive
		{
			get
			{
				JointDrive result;
				this.get_angularXDrive_Injected(out result);
				return result;
			}
			set
			{
				this.set_angularXDrive_Injected(ref value);
			}
		}

		// Token: 0x170000C3 RID: 195
		// (get) Token: 0x060001FD RID: 509 RVA: 0x00003720 File Offset: 0x00001920
		// (set) Token: 0x060001FE RID: 510 RVA: 0x00003736 File Offset: 0x00001936
		public JointDrive angularYZDrive
		{
			get
			{
				JointDrive result;
				this.get_angularYZDrive_Injected(out result);
				return result;
			}
			set
			{
				this.set_angularYZDrive_Injected(ref value);
			}
		}

		// Token: 0x170000C4 RID: 196
		// (get) Token: 0x060001FF RID: 511 RVA: 0x00003740 File Offset: 0x00001940
		// (set) Token: 0x06000200 RID: 512 RVA: 0x00003756 File Offset: 0x00001956
		public JointDrive slerpDrive
		{
			get
			{
				JointDrive result;
				this.get_slerpDrive_Injected(out result);
				return result;
			}
			set
			{
				this.set_slerpDrive_Injected(ref value);
			}
		}

		// Token: 0x170000C5 RID: 197
		// (get) Token: 0x06000201 RID: 513
		// (set) Token: 0x06000202 RID: 514
		public extern JointProjectionMode projectionMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000C6 RID: 198
		// (get) Token: 0x06000203 RID: 515
		// (set) Token: 0x06000204 RID: 516
		public extern float projectionDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000C7 RID: 199
		// (get) Token: 0x06000205 RID: 517
		// (set) Token: 0x06000206 RID: 518
		public extern float projectionAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000C8 RID: 200
		// (get) Token: 0x06000207 RID: 519
		// (set) Token: 0x06000208 RID: 520
		public extern bool configuredInWorldSpace { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000C9 RID: 201
		// (get) Token: 0x06000209 RID: 521
		// (set) Token: 0x0600020A RID: 522
		public extern bool swapBodies { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600020B RID: 523
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_secondaryAxis_Injected(out Vector3 ret);

		// Token: 0x0600020C RID: 524
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_secondaryAxis_Injected(ref Vector3 value);

		// Token: 0x0600020D RID: 525
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_linearLimitSpring_Injected(out SoftJointLimitSpring ret);

		// Token: 0x0600020E RID: 526
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_linearLimitSpring_Injected(ref SoftJointLimitSpring value);

		// Token: 0x0600020F RID: 527
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_angularXLimitSpring_Injected(out SoftJointLimitSpring ret);

		// Token: 0x06000210 RID: 528
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_angularXLimitSpring_Injected(ref SoftJointLimitSpring value);

		// Token: 0x06000211 RID: 529
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_angularYZLimitSpring_Injected(out SoftJointLimitSpring ret);

		// Token: 0x06000212 RID: 530
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_angularYZLimitSpring_Injected(ref SoftJointLimitSpring value);

		// Token: 0x06000213 RID: 531
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_linearLimit_Injected(out SoftJointLimit ret);

		// Token: 0x06000214 RID: 532
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_linearLimit_Injected(ref SoftJointLimit value);

		// Token: 0x06000215 RID: 533
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_lowAngularXLimit_Injected(out SoftJointLimit ret);

		// Token: 0x06000216 RID: 534
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_lowAngularXLimit_Injected(ref SoftJointLimit value);

		// Token: 0x06000217 RID: 535
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_highAngularXLimit_Injected(out SoftJointLimit ret);

		// Token: 0x06000218 RID: 536
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_highAngularXLimit_Injected(ref SoftJointLimit value);

		// Token: 0x06000219 RID: 537
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_angularYLimit_Injected(out SoftJointLimit ret);

		// Token: 0x0600021A RID: 538
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_angularYLimit_Injected(ref SoftJointLimit value);

		// Token: 0x0600021B RID: 539
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_angularZLimit_Injected(out SoftJointLimit ret);

		// Token: 0x0600021C RID: 540
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_angularZLimit_Injected(ref SoftJointLimit value);

		// Token: 0x0600021D RID: 541
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_targetPosition_Injected(out Vector3 ret);

		// Token: 0x0600021E RID: 542
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_targetPosition_Injected(ref Vector3 value);

		// Token: 0x0600021F RID: 543
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_targetVelocity_Injected(out Vector3 ret);

		// Token: 0x06000220 RID: 544
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_targetVelocity_Injected(ref Vector3 value);

		// Token: 0x06000221 RID: 545
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_xDrive_Injected(out JointDrive ret);

		// Token: 0x06000222 RID: 546
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_xDrive_Injected(ref JointDrive value);

		// Token: 0x06000223 RID: 547
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_yDrive_Injected(out JointDrive ret);

		// Token: 0x06000224 RID: 548
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_yDrive_Injected(ref JointDrive value);

		// Token: 0x06000225 RID: 549
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_zDrive_Injected(out JointDrive ret);

		// Token: 0x06000226 RID: 550
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_zDrive_Injected(ref JointDrive value);

		// Token: 0x06000227 RID: 551
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_targetRotation_Injected(out Quaternion ret);

		// Token: 0x06000228 RID: 552
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_targetRotation_Injected(ref Quaternion value);

		// Token: 0x06000229 RID: 553
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_targetAngularVelocity_Injected(out Vector3 ret);

		// Token: 0x0600022A RID: 554
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_targetAngularVelocity_Injected(ref Vector3 value);

		// Token: 0x0600022B RID: 555
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_angularXDrive_Injected(out JointDrive ret);

		// Token: 0x0600022C RID: 556
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_angularXDrive_Injected(ref JointDrive value);

		// Token: 0x0600022D RID: 557
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_angularYZDrive_Injected(out JointDrive ret);

		// Token: 0x0600022E RID: 558
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_angularYZDrive_Injected(ref JointDrive value);

		// Token: 0x0600022F RID: 559
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_slerpDrive_Injected(out JointDrive ret);

		// Token: 0x06000230 RID: 560
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_slerpDrive_Injected(ref JointDrive value);
	}
}
