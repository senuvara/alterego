﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000011 RID: 17
	[RequiredByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public class Collision
	{
		// Token: 0x0600003F RID: 63 RVA: 0x00002411 File Offset: 0x00000611
		public Collision()
		{
		}

		// Token: 0x06000040 RID: 64 RVA: 0x0000255C File Offset: 0x0000075C
		private ContactPoint[] GetContacts_Internal()
		{
			return (this.m_LegacyContacts != null) ? this.m_LegacyContacts : this.m_RecycledContacts;
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x06000041 RID: 65 RVA: 0x00002590 File Offset: 0x00000790
		public Vector3 relativeVelocity
		{
			get
			{
				return this.m_RelativeVelocity;
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x06000042 RID: 66 RVA: 0x000025AC File Offset: 0x000007AC
		public Rigidbody rigidbody
		{
			get
			{
				return this.m_Rigidbody;
			}
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000043 RID: 67 RVA: 0x000025C8 File Offset: 0x000007C8
		public Collider collider
		{
			get
			{
				return this.m_Collider;
			}
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000044 RID: 68 RVA: 0x000025E4 File Offset: 0x000007E4
		public Transform transform
		{
			get
			{
				return (!(this.rigidbody != null)) ? this.collider.transform : this.rigidbody.transform;
			}
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x06000045 RID: 69 RVA: 0x00002628 File Offset: 0x00000828
		public GameObject gameObject
		{
			get
			{
				return (!(this.m_Rigidbody != null)) ? this.m_Collider.gameObject : this.m_Rigidbody.gameObject;
			}
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x06000046 RID: 70 RVA: 0x0000266C File Offset: 0x0000086C
		public int contactCount
		{
			get
			{
				return this.m_ContactCount;
			}
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x06000047 RID: 71 RVA: 0x00002688 File Offset: 0x00000888
		public ContactPoint[] contacts
		{
			get
			{
				if (this.m_LegacyContacts == null)
				{
					this.m_LegacyContacts = new ContactPoint[this.m_ContactCount];
					Array.Copy(this.m_RecycledContacts, this.m_LegacyContacts, this.m_ContactCount);
				}
				return this.m_LegacyContacts;
			}
		}

		// Token: 0x06000048 RID: 72 RVA: 0x000026D8 File Offset: 0x000008D8
		public ContactPoint GetContact(int index)
		{
			if (index < 0 || index >= this.m_ContactCount)
			{
				throw new ArgumentOutOfRangeException(string.Format("Cannot get contact at index {0}. There are {1} contact(s).", index, this.m_ContactCount));
			}
			return this.GetContacts_Internal()[index];
		}

		// Token: 0x06000049 RID: 73 RVA: 0x00002734 File Offset: 0x00000934
		public int GetContacts(ContactPoint[] contacts)
		{
			if (contacts == null)
			{
				throw new NullReferenceException("Cannot get contacts as the provided array is NULL.");
			}
			int num = Mathf.Min(this.m_ContactCount, contacts.Length);
			Array.Copy(this.GetContacts_Internal(), contacts, num);
			return num;
		}

		// Token: 0x0600004A RID: 74 RVA: 0x00002778 File Offset: 0x00000978
		public virtual IEnumerator GetEnumerator()
		{
			return this.contacts.GetEnumerator();
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x0600004B RID: 75 RVA: 0x00002798 File Offset: 0x00000998
		public Vector3 impulse
		{
			get
			{
				return this.m_Impulse;
			}
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x0600004C RID: 76 RVA: 0x000027B4 File Offset: 0x000009B4
		[Obsolete("Use Collision.relativeVelocity instead.", false)]
		public Vector3 impactForceSum
		{
			get
			{
				return this.relativeVelocity;
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x0600004D RID: 77 RVA: 0x000027D0 File Offset: 0x000009D0
		[Obsolete("Will always return zero.", false)]
		public Vector3 frictionForceSum
		{
			get
			{
				return Vector3.zero;
			}
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x0600004E RID: 78 RVA: 0x000027EC File Offset: 0x000009EC
		[Obsolete("Please use Collision.rigidbody, Collision.transform or Collision.collider instead", false)]
		public Component other
		{
			get
			{
				return (!(this.m_Rigidbody != null)) ? this.m_Collider : this.m_Rigidbody;
			}
		}

		// Token: 0x0400004A RID: 74
		internal Vector3 m_Impulse;

		// Token: 0x0400004B RID: 75
		internal Vector3 m_RelativeVelocity;

		// Token: 0x0400004C RID: 76
		internal Rigidbody m_Rigidbody;

		// Token: 0x0400004D RID: 77
		internal Collider m_Collider;

		// Token: 0x0400004E RID: 78
		internal int m_ContactCount;

		// Token: 0x0400004F RID: 79
		internal ContactPoint[] m_RecycledContacts;

		// Token: 0x04000050 RID: 80
		internal ContactPoint[] m_LegacyContacts;
	}
}
