﻿using System;

namespace UnityEngine
{
	// Token: 0x0200000B RID: 11
	public enum RigidbodyInterpolation
	{
		// Token: 0x0400002E RID: 46
		None,
		// Token: 0x0400002F RID: 47
		Interpolate,
		// Token: 0x04000030 RID: 48
		Extrapolate
	}
}
