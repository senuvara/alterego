﻿using System;

namespace UnityEngine
{
	// Token: 0x0200000D RID: 13
	public struct JointSpring
	{
		// Token: 0x04000034 RID: 52
		public float spring;

		// Token: 0x04000035 RID: 53
		public float damper;

		// Token: 0x04000036 RID: 54
		public float targetPosition;
	}
}
