﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000021 RID: 33
	[NativeClass("Unity::Joint")]
	[RequireComponent(typeof(Rigidbody))]
	[NativeHeader("Runtime/Dynamics/Joint.h")]
	public class Joint : Component
	{
		// Token: 0x06000168 RID: 360 RVA: 0x00002BC6 File Offset: 0x00000DC6
		public Joint()
		{
		}

		// Token: 0x17000087 RID: 135
		// (get) Token: 0x06000169 RID: 361
		// (set) Token: 0x0600016A RID: 362
		public extern Rigidbody connectedBody { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000088 RID: 136
		// (get) Token: 0x0600016B RID: 363 RVA: 0x00003300 File Offset: 0x00001500
		// (set) Token: 0x0600016C RID: 364 RVA: 0x00003316 File Offset: 0x00001516
		public Vector3 axis
		{
			get
			{
				Vector3 result;
				this.get_axis_Injected(out result);
				return result;
			}
			set
			{
				this.set_axis_Injected(ref value);
			}
		}

		// Token: 0x17000089 RID: 137
		// (get) Token: 0x0600016D RID: 365 RVA: 0x00003320 File Offset: 0x00001520
		// (set) Token: 0x0600016E RID: 366 RVA: 0x00003336 File Offset: 0x00001536
		public Vector3 anchor
		{
			get
			{
				Vector3 result;
				this.get_anchor_Injected(out result);
				return result;
			}
			set
			{
				this.set_anchor_Injected(ref value);
			}
		}

		// Token: 0x1700008A RID: 138
		// (get) Token: 0x0600016F RID: 367 RVA: 0x00003340 File Offset: 0x00001540
		// (set) Token: 0x06000170 RID: 368 RVA: 0x00003356 File Offset: 0x00001556
		public Vector3 connectedAnchor
		{
			get
			{
				Vector3 result;
				this.get_connectedAnchor_Injected(out result);
				return result;
			}
			set
			{
				this.set_connectedAnchor_Injected(ref value);
			}
		}

		// Token: 0x1700008B RID: 139
		// (get) Token: 0x06000171 RID: 369
		// (set) Token: 0x06000172 RID: 370
		public extern bool autoConfigureConnectedAnchor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700008C RID: 140
		// (get) Token: 0x06000173 RID: 371
		// (set) Token: 0x06000174 RID: 372
		public extern float breakForce { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x06000175 RID: 373
		// (set) Token: 0x06000176 RID: 374
		public extern float breakTorque { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x06000177 RID: 375
		// (set) Token: 0x06000178 RID: 376
		public extern bool enableCollision { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x06000179 RID: 377
		// (set) Token: 0x0600017A RID: 378
		public extern bool enablePreprocessing { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x0600017B RID: 379
		// (set) Token: 0x0600017C RID: 380
		public extern float massScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x0600017D RID: 381
		// (set) Token: 0x0600017E RID: 382
		public extern float connectedMassScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600017F RID: 383
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetCurrentForces(ref Vector3 linearForce, ref Vector3 angularForce);

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x06000180 RID: 384 RVA: 0x00003360 File Offset: 0x00001560
		public Vector3 currentForce
		{
			get
			{
				Vector3 zero = Vector3.zero;
				Vector3 zero2 = Vector3.zero;
				this.GetCurrentForces(ref zero, ref zero2);
				return zero;
			}
		}

		// Token: 0x17000093 RID: 147
		// (get) Token: 0x06000181 RID: 385 RVA: 0x0000338C File Offset: 0x0000158C
		public Vector3 currentTorque
		{
			get
			{
				Vector3 zero = Vector3.zero;
				Vector3 zero2 = Vector3.zero;
				this.GetCurrentForces(ref zero, ref zero2);
				return zero2;
			}
		}

		// Token: 0x06000182 RID: 386
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_axis_Injected(out Vector3 ret);

		// Token: 0x06000183 RID: 387
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_axis_Injected(ref Vector3 value);

		// Token: 0x06000184 RID: 388
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_anchor_Injected(out Vector3 ret);

		// Token: 0x06000185 RID: 389
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_anchor_Injected(ref Vector3 value);

		// Token: 0x06000186 RID: 390
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_connectedAnchor_Injected(out Vector3 ret);

		// Token: 0x06000187 RID: 391
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_connectedAnchor_Injected(ref Vector3 value);
	}
}
