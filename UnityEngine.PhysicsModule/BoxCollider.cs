﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200001E RID: 30
	[RequiredByNativeCode]
	[NativeHeader("Runtime/Dynamics/BoxCollider.h")]
	public class BoxCollider : Collider
	{
		// Token: 0x06000145 RID: 325 RVA: 0x000030E9 File Offset: 0x000012E9
		public BoxCollider()
		{
		}

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x06000146 RID: 326 RVA: 0x000031DC File Offset: 0x000013DC
		// (set) Token: 0x06000147 RID: 327 RVA: 0x000031F2 File Offset: 0x000013F2
		public Vector3 center
		{
			get
			{
				Vector3 result;
				this.get_center_Injected(out result);
				return result;
			}
			set
			{
				this.set_center_Injected(ref value);
			}
		}

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x06000148 RID: 328 RVA: 0x000031FC File Offset: 0x000013FC
		// (set) Token: 0x06000149 RID: 329 RVA: 0x00003212 File Offset: 0x00001412
		public Vector3 size
		{
			get
			{
				Vector3 result;
				this.get_size_Injected(out result);
				return result;
			}
			set
			{
				this.set_size_Injected(ref value);
			}
		}

		// Token: 0x17000080 RID: 128
		// (get) Token: 0x0600014A RID: 330 RVA: 0x0000321C File Offset: 0x0000141C
		// (set) Token: 0x0600014B RID: 331 RVA: 0x00003241 File Offset: 0x00001441
		[Obsolete("Use BoxCollider.size instead.")]
		public Vector3 extents
		{
			get
			{
				return this.size * 0.5f;
			}
			set
			{
				this.size = value * 2f;
			}
		}

		// Token: 0x0600014C RID: 332
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_center_Injected(out Vector3 ret);

		// Token: 0x0600014D RID: 333
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_center_Injected(ref Vector3 value);

		// Token: 0x0600014E RID: 334
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_size_Injected(out Vector3 ret);

		// Token: 0x0600014F RID: 335
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_size_Injected(ref Vector3 value);
	}
}
