﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using Unity.Jobs.LowLevel.Unsafe;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200002D RID: 45
	[NativeHeader("Runtime/Jobs/ScriptBindings/JobsBindingsTypes.h")]
	[NativeHeader("Runtime/Dynamics/BatchCommands/CapsulecastCommand.h")]
	public struct CapsulecastCommand
	{
		// Token: 0x0600035E RID: 862 RVA: 0x0000594D File Offset: 0x00003B4D
		public CapsulecastCommand(Vector3 p1, Vector3 p2, float radius, Vector3 direction, float distance = 3.4028235E+38f, int layerMask = -5)
		{
			this.point1 = p1;
			this.point2 = p2;
			this.direction = direction;
			this.radius = radius;
			this.distance = distance;
			this.layerMask = layerMask;
			this.maxHits = 1;
		}

		// Token: 0x170000F1 RID: 241
		// (get) Token: 0x0600035F RID: 863 RVA: 0x00005984 File Offset: 0x00003B84
		// (set) Token: 0x06000360 RID: 864 RVA: 0x0000599E File Offset: 0x00003B9E
		public Vector3 point1
		{
			[CompilerGenerated]
			get
			{
				return this.<point1>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<point1>k__BackingField = value;
			}
		}

		// Token: 0x170000F2 RID: 242
		// (get) Token: 0x06000361 RID: 865 RVA: 0x000059A8 File Offset: 0x00003BA8
		// (set) Token: 0x06000362 RID: 866 RVA: 0x000059C2 File Offset: 0x00003BC2
		public Vector3 point2
		{
			[CompilerGenerated]
			get
			{
				return this.<point2>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<point2>k__BackingField = value;
			}
		}

		// Token: 0x170000F3 RID: 243
		// (get) Token: 0x06000363 RID: 867 RVA: 0x000059CC File Offset: 0x00003BCC
		// (set) Token: 0x06000364 RID: 868 RVA: 0x000059E6 File Offset: 0x00003BE6
		public float radius
		{
			[CompilerGenerated]
			get
			{
				return this.<radius>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<radius>k__BackingField = value;
			}
		}

		// Token: 0x170000F4 RID: 244
		// (get) Token: 0x06000365 RID: 869 RVA: 0x000059F0 File Offset: 0x00003BF0
		// (set) Token: 0x06000366 RID: 870 RVA: 0x00005A0A File Offset: 0x00003C0A
		public Vector3 direction
		{
			[CompilerGenerated]
			get
			{
				return this.<direction>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<direction>k__BackingField = value;
			}
		}

		// Token: 0x170000F5 RID: 245
		// (get) Token: 0x06000367 RID: 871 RVA: 0x00005A14 File Offset: 0x00003C14
		// (set) Token: 0x06000368 RID: 872 RVA: 0x00005A2E File Offset: 0x00003C2E
		public float distance
		{
			[CompilerGenerated]
			get
			{
				return this.<distance>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<distance>k__BackingField = value;
			}
		}

		// Token: 0x170000F6 RID: 246
		// (get) Token: 0x06000369 RID: 873 RVA: 0x00005A38 File Offset: 0x00003C38
		// (set) Token: 0x0600036A RID: 874 RVA: 0x00005A52 File Offset: 0x00003C52
		public int layerMask
		{
			[CompilerGenerated]
			get
			{
				return this.<layerMask>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<layerMask>k__BackingField = value;
			}
		}

		// Token: 0x170000F7 RID: 247
		// (get) Token: 0x0600036B RID: 875 RVA: 0x00005A5C File Offset: 0x00003C5C
		// (set) Token: 0x0600036C RID: 876 RVA: 0x00005A76 File Offset: 0x00003C76
		internal int maxHits
		{
			[CompilerGenerated]
			get
			{
				return this.<maxHits>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<maxHits>k__BackingField = value;
			}
		}

		// Token: 0x0600036D RID: 877 RVA: 0x00005A80 File Offset: 0x00003C80
		public static JobHandle ScheduleBatch(NativeArray<CapsulecastCommand> commands, NativeArray<RaycastHit> results, int minCommandsPerJob, JobHandle dependsOn = default(JobHandle))
		{
			BatchQueryJob<CapsulecastCommand, RaycastHit> batchQueryJob = new BatchQueryJob<CapsulecastCommand, RaycastHit>(commands, results);
			JobsUtility.JobScheduleParameters jobScheduleParameters = new JobsUtility.JobScheduleParameters(UnsafeUtility.AddressOf<BatchQueryJob<CapsulecastCommand, RaycastHit>>(ref batchQueryJob), BatchQueryJobStruct<BatchQueryJob<CapsulecastCommand, RaycastHit>>.Initialize(), dependsOn, ScheduleMode.Batched);
			return CapsulecastCommand.ScheduleCapsulecastBatch(ref jobScheduleParameters, NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks<CapsulecastCommand>(commands), commands.Length, NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks<RaycastHit>(results), results.Length, minCommandsPerJob);
		}

		// Token: 0x0600036E RID: 878 RVA: 0x00005AD8 File Offset: 0x00003CD8
		[FreeFunction("ScheduleCapsulecastCommandBatch")]
		private unsafe static JobHandle ScheduleCapsulecastBatch(ref JobsUtility.JobScheduleParameters parameters, void* commands, int commandLen, void* result, int resultLen, int minCommandsPerJob)
		{
			JobHandle result2;
			CapsulecastCommand.ScheduleCapsulecastBatch_Injected(ref parameters, commands, commandLen, result, resultLen, minCommandsPerJob, out result2);
			return result2;
		}

		// Token: 0x0600036F RID: 879
		[MethodImpl(MethodImplOptions.InternalCall)]
		private unsafe static extern void ScheduleCapsulecastBatch_Injected(ref JobsUtility.JobScheduleParameters parameters, void* commands, int commandLen, void* result, int resultLen, int minCommandsPerJob, out JobHandle ret);

		// Token: 0x0400008A RID: 138
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Vector3 <point1>k__BackingField;

		// Token: 0x0400008B RID: 139
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private Vector3 <point2>k__BackingField;

		// Token: 0x0400008C RID: 140
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private float <radius>k__BackingField;

		// Token: 0x0400008D RID: 141
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private Vector3 <direction>k__BackingField;

		// Token: 0x0400008E RID: 142
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private float <distance>k__BackingField;

		// Token: 0x0400008F RID: 143
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <layerMask>k__BackingField;

		// Token: 0x04000090 RID: 144
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int <maxHits>k__BackingField;
	}
}
