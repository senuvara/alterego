﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000025 RID: 37
	[NativeClass("Unity::CharacterJoint")]
	[NativeHeader("Runtime/Dynamics/CharacterJoint.h")]
	public class CharacterJoint : Joint
	{
		// Token: 0x060001A9 RID: 425 RVA: 0x000033B8 File Offset: 0x000015B8
		public CharacterJoint()
		{
		}

		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x060001AA RID: 426 RVA: 0x00003420 File Offset: 0x00001620
		// (set) Token: 0x060001AB RID: 427 RVA: 0x00003436 File Offset: 0x00001636
		public Vector3 swingAxis
		{
			get
			{
				Vector3 result;
				this.get_swingAxis_Injected(out result);
				return result;
			}
			set
			{
				this.set_swingAxis_Injected(ref value);
			}
		}

		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x060001AC RID: 428 RVA: 0x00003440 File Offset: 0x00001640
		// (set) Token: 0x060001AD RID: 429 RVA: 0x00003456 File Offset: 0x00001656
		public SoftJointLimitSpring twistLimitSpring
		{
			get
			{
				SoftJointLimitSpring result;
				this.get_twistLimitSpring_Injected(out result);
				return result;
			}
			set
			{
				this.set_twistLimitSpring_Injected(ref value);
			}
		}

		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x060001AE RID: 430 RVA: 0x00003460 File Offset: 0x00001660
		// (set) Token: 0x060001AF RID: 431 RVA: 0x00003476 File Offset: 0x00001676
		public SoftJointLimitSpring swingLimitSpring
		{
			get
			{
				SoftJointLimitSpring result;
				this.get_swingLimitSpring_Injected(out result);
				return result;
			}
			set
			{
				this.set_swingLimitSpring_Injected(ref value);
			}
		}

		// Token: 0x170000A4 RID: 164
		// (get) Token: 0x060001B0 RID: 432 RVA: 0x00003480 File Offset: 0x00001680
		// (set) Token: 0x060001B1 RID: 433 RVA: 0x00003496 File Offset: 0x00001696
		public SoftJointLimit lowTwistLimit
		{
			get
			{
				SoftJointLimit result;
				this.get_lowTwistLimit_Injected(out result);
				return result;
			}
			set
			{
				this.set_lowTwistLimit_Injected(ref value);
			}
		}

		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x060001B2 RID: 434 RVA: 0x000034A0 File Offset: 0x000016A0
		// (set) Token: 0x060001B3 RID: 435 RVA: 0x000034B6 File Offset: 0x000016B6
		public SoftJointLimit highTwistLimit
		{
			get
			{
				SoftJointLimit result;
				this.get_highTwistLimit_Injected(out result);
				return result;
			}
			set
			{
				this.set_highTwistLimit_Injected(ref value);
			}
		}

		// Token: 0x170000A6 RID: 166
		// (get) Token: 0x060001B4 RID: 436 RVA: 0x000034C0 File Offset: 0x000016C0
		// (set) Token: 0x060001B5 RID: 437 RVA: 0x000034D6 File Offset: 0x000016D6
		public SoftJointLimit swing1Limit
		{
			get
			{
				SoftJointLimit result;
				this.get_swing1Limit_Injected(out result);
				return result;
			}
			set
			{
				this.set_swing1Limit_Injected(ref value);
			}
		}

		// Token: 0x170000A7 RID: 167
		// (get) Token: 0x060001B6 RID: 438 RVA: 0x000034E0 File Offset: 0x000016E0
		// (set) Token: 0x060001B7 RID: 439 RVA: 0x000034F6 File Offset: 0x000016F6
		public SoftJointLimit swing2Limit
		{
			get
			{
				SoftJointLimit result;
				this.get_swing2Limit_Injected(out result);
				return result;
			}
			set
			{
				this.set_swing2Limit_Injected(ref value);
			}
		}

		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x060001B8 RID: 440
		// (set) Token: 0x060001B9 RID: 441
		public extern bool enableProjection { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x060001BA RID: 442
		// (set) Token: 0x060001BB RID: 443
		public extern float projectionDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000AA RID: 170
		// (get) Token: 0x060001BC RID: 444
		// (set) Token: 0x060001BD RID: 445
		public extern float projectionAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060001BE RID: 446
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_swingAxis_Injected(out Vector3 ret);

		// Token: 0x060001BF RID: 447
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_swingAxis_Injected(ref Vector3 value);

		// Token: 0x060001C0 RID: 448
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_twistLimitSpring_Injected(out SoftJointLimitSpring ret);

		// Token: 0x060001C1 RID: 449
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_twistLimitSpring_Injected(ref SoftJointLimitSpring value);

		// Token: 0x060001C2 RID: 450
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_swingLimitSpring_Injected(out SoftJointLimitSpring ret);

		// Token: 0x060001C3 RID: 451
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_swingLimitSpring_Injected(ref SoftJointLimitSpring value);

		// Token: 0x060001C4 RID: 452
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_lowTwistLimit_Injected(out SoftJointLimit ret);

		// Token: 0x060001C5 RID: 453
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_lowTwistLimit_Injected(ref SoftJointLimit value);

		// Token: 0x060001C6 RID: 454
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_highTwistLimit_Injected(out SoftJointLimit ret);

		// Token: 0x060001C7 RID: 455
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_highTwistLimit_Injected(ref SoftJointLimit value);

		// Token: 0x060001C8 RID: 456
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_swing1Limit_Injected(out SoftJointLimit ret);

		// Token: 0x060001C9 RID: 457
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_swing1Limit_Injected(ref SoftJointLimit value);

		// Token: 0x060001CA RID: 458
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_swing2Limit_Injected(out SoftJointLimit ret);

		// Token: 0x060001CB RID: 459
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_swing2Limit_Injected(ref SoftJointLimit value);

		// Token: 0x0400006F RID: 111
		[Obsolete("TargetRotation not in use for Unity 5 and assumed disabled.", true)]
		public Quaternion targetRotation;

		// Token: 0x04000070 RID: 112
		[Obsolete("TargetAngularVelocity not in use for Unity 5 and assumed disabled.", true)]
		public Vector3 targetAngularVelocity;

		// Token: 0x04000071 RID: 113
		[Obsolete("RotationDrive not in use for Unity 5 and assumed disabled.")]
		public JointDrive rotationDrive;
	}
}
