﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200000F RID: 15
	[RequiredByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public class ControllerColliderHit
	{
		// Token: 0x06000033 RID: 51 RVA: 0x00002411 File Offset: 0x00000611
		public ControllerColliderHit()
		{
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000034 RID: 52 RVA: 0x0000241C File Offset: 0x0000061C
		public CharacterController controller
		{
			get
			{
				return this.m_Controller;
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000035 RID: 53 RVA: 0x00002438 File Offset: 0x00000638
		public Collider collider
		{
			get
			{
				return this.m_Collider;
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000036 RID: 54 RVA: 0x00002454 File Offset: 0x00000654
		public Rigidbody rigidbody
		{
			get
			{
				return this.m_Collider.attachedRigidbody;
			}
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x06000037 RID: 55 RVA: 0x00002474 File Offset: 0x00000674
		public GameObject gameObject
		{
			get
			{
				return this.m_Collider.gameObject;
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x06000038 RID: 56 RVA: 0x00002494 File Offset: 0x00000694
		public Transform transform
		{
			get
			{
				return this.m_Collider.transform;
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000039 RID: 57 RVA: 0x000024B4 File Offset: 0x000006B4
		public Vector3 point
		{
			get
			{
				return this.m_Point;
			}
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x0600003A RID: 58 RVA: 0x000024D0 File Offset: 0x000006D0
		public Vector3 normal
		{
			get
			{
				return this.m_Normal;
			}
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x0600003B RID: 59 RVA: 0x000024EC File Offset: 0x000006EC
		public Vector3 moveDirection
		{
			get
			{
				return this.m_MoveDirection;
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x0600003C RID: 60 RVA: 0x00002508 File Offset: 0x00000708
		public float moveLength
		{
			get
			{
				return this.m_MoveLength;
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x0600003D RID: 61 RVA: 0x00002524 File Offset: 0x00000724
		// (set) Token: 0x0600003E RID: 62 RVA: 0x00002545 File Offset: 0x00000745
		private bool push
		{
			get
			{
				return this.m_Push != 0;
			}
			set
			{
				this.m_Push = ((!value) ? 0 : 1);
			}
		}

		// Token: 0x0400003E RID: 62
		internal CharacterController m_Controller;

		// Token: 0x0400003F RID: 63
		internal Collider m_Collider;

		// Token: 0x04000040 RID: 64
		internal Vector3 m_Point;

		// Token: 0x04000041 RID: 65
		internal Vector3 m_Normal;

		// Token: 0x04000042 RID: 66
		internal Vector3 m_MoveDirection;

		// Token: 0x04000043 RID: 67
		internal float m_MoveLength;

		// Token: 0x04000044 RID: 68
		internal int m_Push;
	}
}
