﻿using System;

namespace UnityEngine
{
	// Token: 0x02000014 RID: 20
	public enum CollisionDetectionMode
	{
		// Token: 0x0400005E RID: 94
		Discrete,
		// Token: 0x0400005F RID: 95
		Continuous,
		// Token: 0x04000060 RID: 96
		ContinuousDynamic,
		// Token: 0x04000061 RID: 97
		ContinuousSpeculative
	}
}
