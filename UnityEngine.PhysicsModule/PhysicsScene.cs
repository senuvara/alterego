﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x02000028 RID: 40
	[NativeHeader("Runtime/Dynamics/Public/PhysicsSceneHandle.h")]
	public struct PhysicsScene : IEquatable<PhysicsScene>
	{
		// Token: 0x06000237 RID: 567 RVA: 0x000037F4 File Offset: 0x000019F4
		public override string ToString()
		{
			return UnityString.Format("({0})", new object[]
			{
				this.m_Handle
			});
		}

		// Token: 0x06000238 RID: 568 RVA: 0x00003828 File Offset: 0x00001A28
		public static bool operator ==(PhysicsScene lhs, PhysicsScene rhs)
		{
			return lhs.m_Handle == rhs.m_Handle;
		}

		// Token: 0x06000239 RID: 569 RVA: 0x00003850 File Offset: 0x00001A50
		public static bool operator !=(PhysicsScene lhs, PhysicsScene rhs)
		{
			return lhs.m_Handle != rhs.m_Handle;
		}

		// Token: 0x0600023A RID: 570 RVA: 0x00003878 File Offset: 0x00001A78
		public override int GetHashCode()
		{
			return this.m_Handle;
		}

		// Token: 0x0600023B RID: 571 RVA: 0x00003894 File Offset: 0x00001A94
		public override bool Equals(object other)
		{
			bool result;
			if (!(other is PhysicsScene))
			{
				result = false;
			}
			else
			{
				PhysicsScene physicsScene = (PhysicsScene)other;
				result = (this.m_Handle == physicsScene.m_Handle);
			}
			return result;
		}

		// Token: 0x0600023C RID: 572 RVA: 0x000038D4 File Offset: 0x00001AD4
		public bool Equals(PhysicsScene other)
		{
			return this.m_Handle == other.m_Handle;
		}

		// Token: 0x0600023D RID: 573 RVA: 0x000038F8 File Offset: 0x00001AF8
		public bool IsValid()
		{
			return PhysicsScene.IsValid_Internal(this);
		}

		// Token: 0x0600023E RID: 574 RVA: 0x00003918 File Offset: 0x00001B18
		[NativeMethod("IsPhysicsSceneValid")]
		[StaticAccessor("GetPhysicsManager()", StaticAccessorType.Dot)]
		private static bool IsValid_Internal(PhysicsScene physicsScene)
		{
			return PhysicsScene.IsValid_Internal_Injected(ref physicsScene);
		}

		// Token: 0x0600023F RID: 575 RVA: 0x00003924 File Offset: 0x00001B24
		public bool IsEmpty()
		{
			if (this.IsValid())
			{
				return PhysicsScene.IsEmpty_Internal(this);
			}
			throw new InvalidOperationException("Cannot check if physics scene is empty as it is invalid.");
		}

		// Token: 0x06000240 RID: 576 RVA: 0x0000395A File Offset: 0x00001B5A
		[NativeMethod("IsPhysicsWorldEmpty")]
		[StaticAccessor("GetPhysicsManager()", StaticAccessorType.Dot)]
		private static bool IsEmpty_Internal(PhysicsScene physicsScene)
		{
			return PhysicsScene.IsEmpty_Internal_Injected(ref physicsScene);
		}

		// Token: 0x06000241 RID: 577 RVA: 0x00003964 File Offset: 0x00001B64
		public void Simulate(float step)
		{
			if (this.IsValid())
			{
				if (this == Physics.defaultPhysicsScene && Physics.autoSimulation)
				{
					Debug.LogWarning("PhysicsScene.Simulate(...) was called but auto simulation is active. You should disable auto simulation first before calling this function therefore the simulation was not run.");
				}
				else
				{
					Physics.Simulate_Internal(this, step);
				}
				return;
			}
			throw new InvalidOperationException("Cannot simulate the physics scene as it is invalid.");
		}

		// Token: 0x06000242 RID: 578 RVA: 0x000039CC File Offset: 0x00001BCC
		public bool Raycast(Vector3 origin, Vector3 direction, [DefaultValue("Mathf.Infinity")] float maxDistance = float.PositiveInfinity, [DefaultValue("Physics.DefaultRaycastLayers")] int layerMask = -5, [DefaultValue("QueryTriggerInteraction.UseGlobal")] QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
		{
			float magnitude = direction.magnitude;
			bool result;
			if (magnitude > 1E-45f)
			{
				Vector3 direction2 = direction / magnitude;
				Ray ray = new Ray(origin, direction2);
				result = PhysicsScene.Internal_RaycastTest(this, ray, maxDistance, layerMask, queryTriggerInteraction);
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x06000243 RID: 579 RVA: 0x00003A1E File Offset: 0x00001C1E
		[StaticAccessor("GetPhysicsManager().GetPhysicsQuery()", StaticAccessorType.Dot)]
		[NativeName("RaycastTest")]
		private static bool Internal_RaycastTest(PhysicsScene physicsScene, Ray ray, float maxDistance, int layerMask, QueryTriggerInteraction queryTriggerInteraction)
		{
			return PhysicsScene.Internal_RaycastTest_Injected(ref physicsScene, ref ray, maxDistance, layerMask, queryTriggerInteraction);
		}

		// Token: 0x06000244 RID: 580 RVA: 0x00003A30 File Offset: 0x00001C30
		public bool Raycast(Vector3 origin, Vector3 direction, out RaycastHit hitInfo, [DefaultValue("Mathf.Infinity")] float maxDistance = float.PositiveInfinity, [DefaultValue("Physics.DefaultRaycastLayers")] int layerMask = -5, [DefaultValue("QueryTriggerInteraction.UseGlobal")] QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
		{
			hitInfo = default(RaycastHit);
			float magnitude = direction.magnitude;
			bool result;
			if (magnitude > 1E-45f)
			{
				Vector3 direction2 = direction / magnitude;
				Ray ray = new Ray(origin, direction2);
				result = PhysicsScene.Internal_Raycast(this, ray, maxDistance, ref hitInfo, layerMask, queryTriggerInteraction);
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x06000245 RID: 581 RVA: 0x00003A8B File Offset: 0x00001C8B
		[NativeName("Raycast")]
		[StaticAccessor("GetPhysicsManager().GetPhysicsQuery()", StaticAccessorType.Dot)]
		private static bool Internal_Raycast(PhysicsScene physicsScene, Ray ray, float maxDistance, ref RaycastHit hit, int layerMask, QueryTriggerInteraction queryTriggerInteraction)
		{
			return PhysicsScene.Internal_Raycast_Injected(ref physicsScene, ref ray, maxDistance, ref hit, layerMask, queryTriggerInteraction);
		}

		// Token: 0x06000246 RID: 582 RVA: 0x00003A9C File Offset: 0x00001C9C
		public int Raycast(Vector3 origin, Vector3 direction, RaycastHit[] raycastHits, [DefaultValue("Mathf.Infinity")] float maxDistance = float.PositiveInfinity, [DefaultValue("Physics.DefaultRaycastLayers")] int layerMask = -5, [DefaultValue("QueryTriggerInteraction.UseGlobal")] QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
		{
			float magnitude = direction.magnitude;
			int result;
			if (magnitude > 1E-45f)
			{
				Ray ray = new Ray(origin, direction.normalized);
				result = PhysicsScene.Internal_RaycastNonAlloc(this, ray, raycastHits, maxDistance, layerMask, queryTriggerInteraction);
			}
			else
			{
				result = 0;
			}
			return result;
		}

		// Token: 0x06000247 RID: 583 RVA: 0x00003AEE File Offset: 0x00001CEE
		[NativeName("RaycastNonAlloc")]
		[StaticAccessor("GetPhysicsManager().GetPhysicsQuery()")]
		private static int Internal_RaycastNonAlloc(PhysicsScene physicsScene, Ray ray, RaycastHit[] raycastHits, float maxDistance, int mask, QueryTriggerInteraction queryTriggerInteraction)
		{
			return PhysicsScene.Internal_RaycastNonAlloc_Injected(ref physicsScene, ref ray, raycastHits, maxDistance, mask, queryTriggerInteraction);
		}

		// Token: 0x06000248 RID: 584
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsValid_Internal_Injected(ref PhysicsScene physicsScene);

		// Token: 0x06000249 RID: 585
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsEmpty_Internal_Injected(ref PhysicsScene physicsScene);

		// Token: 0x0600024A RID: 586
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Internal_RaycastTest_Injected(ref PhysicsScene physicsScene, ref Ray ray, float maxDistance, int layerMask, QueryTriggerInteraction queryTriggerInteraction);

		// Token: 0x0600024B RID: 587
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Internal_Raycast_Injected(ref PhysicsScene physicsScene, ref Ray ray, float maxDistance, ref RaycastHit hit, int layerMask, QueryTriggerInteraction queryTriggerInteraction);

		// Token: 0x0600024C RID: 588
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Internal_RaycastNonAlloc_Injected(ref PhysicsScene physicsScene, ref Ray ray, RaycastHit[] raycastHits, float maxDistance, int mask, QueryTriggerInteraction queryTriggerInteraction);

		// Token: 0x04000077 RID: 119
		private int m_Handle;
	}
}
