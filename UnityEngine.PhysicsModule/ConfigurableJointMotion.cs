﻿using System;

namespace UnityEngine
{
	// Token: 0x02000015 RID: 21
	public enum ConfigurableJointMotion
	{
		// Token: 0x04000063 RID: 99
		Locked,
		// Token: 0x04000064 RID: 100
		Limited,
		// Token: 0x04000065 RID: 101
		Free
	}
}
