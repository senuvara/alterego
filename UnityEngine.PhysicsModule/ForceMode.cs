﻿using System;

namespace UnityEngine
{
	// Token: 0x02000003 RID: 3
	public enum ForceMode
	{
		// Token: 0x0400000D RID: 13
		Force,
		// Token: 0x0400000E RID: 14
		Acceleration = 5,
		// Token: 0x0400000F RID: 15
		Impulse = 1,
		// Token: 0x04000010 RID: 16
		VelocityChange
	}
}
