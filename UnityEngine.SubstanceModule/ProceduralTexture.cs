﻿using System;

namespace UnityEngine
{
	// Token: 0x02000009 RID: 9
	[Obsolete("Built-in support for Substance Designer materials has been removed from Unity. To continue using Substance Designer materials, you will need to install Allegorithmic's external importer from the Asset Store.", true)]
	[ExcludeFromPreset]
	public sealed class ProceduralTexture : Texture
	{
		// Token: 0x0600002F RID: 47 RVA: 0x0000207C File Offset: 0x0000027C
		private ProceduralTexture()
		{
			throw new Exception("Built-in support for Substance Designer materials has been removed from Unity. To continue using Substance Designer materials, you will need to install Allegorithmic's external importer from the Asset Store.");
		}

		// Token: 0x06000030 RID: 48 RVA: 0x0000205F File Offset: 0x0000025F
		public ProceduralOutputType GetProceduralOutputType()
		{
			throw new Exception("Built-in support for Substance Designer materials has been removed from Unity. To continue using Substance Designer materials, you will need to install Allegorithmic's external importer from the Asset Store.");
		}

		// Token: 0x06000031 RID: 49 RVA: 0x0000205F File Offset: 0x0000025F
		internal ProceduralMaterial GetProceduralMaterial()
		{
			throw new Exception("Built-in support for Substance Designer materials has been removed from Unity. To continue using Substance Designer materials, you will need to install Allegorithmic's external importer from the Asset Store.");
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000032 RID: 50 RVA: 0x0000205F File Offset: 0x0000025F
		public bool hasAlpha
		{
			get
			{
				throw new Exception("Built-in support for Substance Designer materials has been removed from Unity. To continue using Substance Designer materials, you will need to install Allegorithmic's external importer from the Asset Store.");
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000033 RID: 51 RVA: 0x0000205F File Offset: 0x0000025F
		public TextureFormat format
		{
			get
			{
				throw new Exception("Built-in support for Substance Designer materials has been removed from Unity. To continue using Substance Designer materials, you will need to install Allegorithmic's external importer from the Asset Store.");
			}
		}

		// Token: 0x06000034 RID: 52 RVA: 0x0000205F File Offset: 0x0000025F
		public Color32[] GetPixels32(int x, int y, int blockWidth, int blockHeight)
		{
			throw new Exception("Built-in support for Substance Designer materials has been removed from Unity. To continue using Substance Designer materials, you will need to install Allegorithmic's external importer from the Asset Store.");
		}
	}
}
