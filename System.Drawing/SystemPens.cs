﻿using System;
using Unity;

namespace System.Drawing
{
	/// <summary>Each property of the <see cref="T:System.Drawing.SystemPens" /> class is a <see cref="T:System.Drawing.Pen" /> that is the color of a Windows display element and that has a width of 1 pixel.</summary>
	// Token: 0x02000088 RID: 136
	public sealed class SystemPens
	{
		// Token: 0x06000779 RID: 1913 RVA: 0x00004644 File Offset: 0x00002844
		internal SystemPens()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the color of the active window's border.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the color of the active window's border.</returns>
		// Token: 0x1700030E RID: 782
		// (get) Token: 0x0600077A RID: 1914 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen ActiveBorder
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the color of the background of the active window's title bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the color of the background of the active window's title bar.</returns>
		// Token: 0x1700030F RID: 783
		// (get) Token: 0x0600077B RID: 1915 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen ActiveCaption
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the color of the text in the active window's title bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the color of the text in the active window's title bar.</returns>
		// Token: 0x17000310 RID: 784
		// (get) Token: 0x0600077C RID: 1916 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen ActiveCaptionText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the color of the application workspace.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the color of the application workspace.</returns>
		// Token: 0x17000311 RID: 785
		// (get) Token: 0x0600077D RID: 1917 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen AppWorkspace
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the face color of a 3-D element.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the face color of a 3-D element.</returns>
		// Token: 0x17000312 RID: 786
		// (get) Token: 0x0600077E RID: 1918 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen ButtonFace
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the highlight color of a 3-D element. </summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the highlight color of a 3-D element.</returns>
		// Token: 0x17000313 RID: 787
		// (get) Token: 0x0600077F RID: 1919 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen ButtonHighlight
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the shadow color of a 3-D element. </summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the shadow color of a 3-D element.</returns>
		// Token: 0x17000314 RID: 788
		// (get) Token: 0x06000780 RID: 1920 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen ButtonShadow
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the face color of a 3-D element.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the face color of a 3-D element.</returns>
		// Token: 0x17000315 RID: 789
		// (get) Token: 0x06000781 RID: 1921 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Control
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the shadow color of a 3-D element. </summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the shadow color of a 3-D element.</returns>
		// Token: 0x17000316 RID: 790
		// (get) Token: 0x06000782 RID: 1922 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen ControlDark
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the dark shadow color of a 3-D element. </summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the dark shadow color of a 3-D element.</returns>
		// Token: 0x17000317 RID: 791
		// (get) Token: 0x06000783 RID: 1923 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen ControlDarkDark
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the light color of a 3-D element. </summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the light color of a 3-D element.</returns>
		// Token: 0x17000318 RID: 792
		// (get) Token: 0x06000784 RID: 1924 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen ControlLight
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the highlight color of a 3-D element. </summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the highlight color of a 3-D element.</returns>
		// Token: 0x17000319 RID: 793
		// (get) Token: 0x06000785 RID: 1925 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen ControlLightLight
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the color of text in a 3-D element.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the color of text in a 3-D element.</returns>
		// Token: 0x1700031A RID: 794
		// (get) Token: 0x06000786 RID: 1926 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen ControlText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the color of the Windows desktop.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the color of the Windows desktop.</returns>
		// Token: 0x1700031B RID: 795
		// (get) Token: 0x06000787 RID: 1927 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Desktop
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the lightest color in the color gradient of an active window's title bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the lightest color in the color gradient of an active window's title bar.</returns>
		// Token: 0x1700031C RID: 796
		// (get) Token: 0x06000788 RID: 1928 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen GradientActiveCaption
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the lightest color in the color gradient of an inactive window's title bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the lightest color in the color gradient of an inactive window's title bar.</returns>
		// Token: 0x1700031D RID: 797
		// (get) Token: 0x06000789 RID: 1929 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen GradientInactiveCaption
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the color of dimmed text. </summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the color of dimmed text.</returns>
		// Token: 0x1700031E RID: 798
		// (get) Token: 0x0600078A RID: 1930 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen GrayText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the color of the background of selected items. </summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the color of the background of selected items.</returns>
		// Token: 0x1700031F RID: 799
		// (get) Token: 0x0600078B RID: 1931 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Highlight
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the color of the text of selected items. </summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the color of the text of selected items.</returns>
		// Token: 0x17000320 RID: 800
		// (get) Token: 0x0600078C RID: 1932 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen HighlightText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the color used to designate a hot-tracked item.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the color used to designate a hot-tracked item.</returns>
		// Token: 0x17000321 RID: 801
		// (get) Token: 0x0600078D RID: 1933 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen HotTrack
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> is the color of the border of an inactive window.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the color of the border of an inactive window.</returns>
		// Token: 0x17000322 RID: 802
		// (get) Token: 0x0600078E RID: 1934 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen InactiveBorder
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the color of the title bar caption of an inactive window.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the color of the title bar caption of an inactive window.</returns>
		// Token: 0x17000323 RID: 803
		// (get) Token: 0x0600078F RID: 1935 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen InactiveCaption
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the color of the text in an inactive window's title bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the color of the text in an inactive window's title bar.</returns>
		// Token: 0x17000324 RID: 804
		// (get) Token: 0x06000790 RID: 1936 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen InactiveCaptionText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the color of the background of a ToolTip.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the color of the background of a ToolTip.</returns>
		// Token: 0x17000325 RID: 805
		// (get) Token: 0x06000791 RID: 1937 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Info
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the color of the text of a ToolTip.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the color of the text of a ToolTip.</returns>
		// Token: 0x17000326 RID: 806
		// (get) Token: 0x06000792 RID: 1938 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen InfoText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the color of a menu's background.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the color of a menu's background.</returns>
		// Token: 0x17000327 RID: 807
		// (get) Token: 0x06000793 RID: 1939 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Menu
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the color of the background of a menu bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the color of the background of a menu bar.</returns>
		// Token: 0x17000328 RID: 808
		// (get) Token: 0x06000794 RID: 1940 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen MenuBar
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the color used to highlight menu items when the menu appears as a flat menu.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the color used to highlight menu items when the menu appears as a flat menu.</returns>
		// Token: 0x17000329 RID: 809
		// (get) Token: 0x06000795 RID: 1941 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen MenuHighlight
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the color of a menu's text.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the color of a menu's text.</returns>
		// Token: 0x1700032A RID: 810
		// (get) Token: 0x06000796 RID: 1942 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen MenuText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the color of the background of a scroll bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the color of the background of a scroll bar.</returns>
		// Token: 0x1700032B RID: 811
		// (get) Token: 0x06000797 RID: 1943 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen ScrollBar
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the color of the background in the client area of a window.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the color of the background in the client area of a window.</returns>
		// Token: 0x1700032C RID: 812
		// (get) Token: 0x06000798 RID: 1944 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Window
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the color of a window frame.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the color of a window frame.</returns>
		// Token: 0x1700032D RID: 813
		// (get) Token: 0x06000799 RID: 1945 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen WindowFrame
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Pen" /> that is the color of the text in the client area of a window.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> that is the color of the text in the client area of a window.</returns>
		// Token: 0x1700032E RID: 814
		// (get) Token: 0x0600079A RID: 1946 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen WindowText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Creates a <see cref="T:System.Drawing.Pen" /> from the specified <see cref="T:System.Drawing.Color" />.</summary>
		/// <param name="c">The <see cref="T:System.Drawing.Color" /> for the new <see cref="T:System.Drawing.Pen" />. </param>
		/// <returns>The <see cref="T:System.Drawing.Pen" /> this method creates.</returns>
		// Token: 0x0600079B RID: 1947 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen FromSystemColor(Color c)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
