﻿using System;
using System.Drawing.Drawing2D;
using Unity;

namespace System.Drawing
{
	/// <summary>Describes the interior of a graphics shape composed of rectangles and paths. This class cannot be inherited.</summary>
	// Token: 0x02000018 RID: 24
	public sealed class Region : MarshalByRefObject, IDisposable
	{
		/// <summary>Initializes a new <see cref="T:System.Drawing.Region" />.</summary>
		// Token: 0x0600026C RID: 620 RVA: 0x00004644 File Offset: 0x00002844
		public Region()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.Region" /> with the specified <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="path">A <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> that defines the new <see cref="T:System.Drawing.Region" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> is <see langword="null" />.</exception>
		// Token: 0x0600026D RID: 621 RVA: 0x00004644 File Offset: 0x00002844
		public Region(GraphicsPath path)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.Region" /> from the specified data.</summary>
		/// <param name="rgnData">A <see cref="T:System.Drawing.Drawing2D.RegionData" /> that defines the interior of the new <see cref="T:System.Drawing.Region" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="rgnData" /> is <see langword="null" />.</exception>
		// Token: 0x0600026E RID: 622 RVA: 0x00004644 File Offset: 0x00002844
		public Region(RegionData rgnData)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.Region" /> from the specified <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <param name="rect">A <see cref="T:System.Drawing.Rectangle" /> structure that defines the interior of the new <see cref="T:System.Drawing.Region" />. </param>
		// Token: 0x0600026F RID: 623 RVA: 0x00004644 File Offset: 0x00002844
		public Region(Rectangle rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.Region" /> from the specified <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <param name="rect">A <see cref="T:System.Drawing.RectangleF" /> structure that defines the interior of the new <see cref="T:System.Drawing.Region" />. </param>
		// Token: 0x06000270 RID: 624 RVA: 0x00004644 File Offset: 0x00002844
		public Region(RectangleF rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates an exact copy of this <see cref="T:System.Drawing.Region" />.</summary>
		/// <returns>The <see cref="T:System.Drawing.Region" /> that this method creates.</returns>
		// Token: 0x06000271 RID: 625 RVA: 0x00004667 File Offset: 0x00002867
		public Region Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Updates this <see cref="T:System.Drawing.Region" /> to contain the portion of the specified <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> that does not intersect with this <see cref="T:System.Drawing.Region" />.</summary>
		/// <param name="path">The <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> to complement this <see cref="T:System.Drawing.Region" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> is <see langword="null" />.</exception>
		// Token: 0x06000272 RID: 626 RVA: 0x00004644 File Offset: 0x00002844
		public void Complement(GraphicsPath path)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates this <see cref="T:System.Drawing.Region" /> to contain the portion of the specified <see cref="T:System.Drawing.Rectangle" /> structure that does not intersect with this <see cref="T:System.Drawing.Region" />.</summary>
		/// <param name="rect">The <see cref="T:System.Drawing.Rectangle" /> structure to complement this <see cref="T:System.Drawing.Region" />. </param>
		// Token: 0x06000273 RID: 627 RVA: 0x00004644 File Offset: 0x00002844
		public void Complement(Rectangle rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates this <see cref="T:System.Drawing.Region" /> to contain the portion of the specified <see cref="T:System.Drawing.RectangleF" /> structure that does not intersect with this <see cref="T:System.Drawing.Region" />.</summary>
		/// <param name="rect">The <see cref="T:System.Drawing.RectangleF" /> structure to complement this <see cref="T:System.Drawing.Region" />. </param>
		// Token: 0x06000274 RID: 628 RVA: 0x00004644 File Offset: 0x00002844
		public void Complement(RectangleF rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates this <see cref="T:System.Drawing.Region" /> to contain the portion of the specified <see cref="T:System.Drawing.Region" /> that does not intersect with this <see cref="T:System.Drawing.Region" />.</summary>
		/// <param name="region">The <see cref="T:System.Drawing.Region" /> object to complement this <see cref="T:System.Drawing.Region" /> object. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="region" /> is <see langword="null" />.</exception>
		// Token: 0x06000275 RID: 629 RVA: 0x00004644 File Offset: 0x00002844
		public void Complement(Region region)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Releases all resources used by this <see cref="T:System.Drawing.Region" />.</summary>
		// Token: 0x06000276 RID: 630 RVA: 0x00004644 File Offset: 0x00002844
		public void Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Tests whether the specified <see cref="T:System.Drawing.Region" /> is identical to this <see cref="T:System.Drawing.Region" /> on the specified drawing surface.</summary>
		/// <param name="region">The <see cref="T:System.Drawing.Region" /> to test. </param>
		/// <param name="g">A <see cref="T:System.Drawing.Graphics" /> that represents a drawing surface. </param>
		/// <returns>
		///     <see langword="true" /> if the interior of region is identical to the interior of this region when the transformation associated with the <paramref name="g" /> parameter is applied; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="g" /> or <paramref name="region" /> is <see langword="null" />.</exception>
		// Token: 0x06000277 RID: 631 RVA: 0x00004B24 File Offset: 0x00002D24
		public bool Equals(Region region, Graphics g)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Updates this <see cref="T:System.Drawing.Region" /> to contain only the portion of its interior that does not intersect with the specified <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="path">The <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> to exclude from this <see cref="T:System.Drawing.Region" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> is <see langword="null" />.</exception>
		// Token: 0x06000278 RID: 632 RVA: 0x00004644 File Offset: 0x00002844
		public void Exclude(GraphicsPath path)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates this <see cref="T:System.Drawing.Region" /> to contain only the portion of its interior that does not intersect with the specified <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <param name="rect">The <see cref="T:System.Drawing.Rectangle" /> structure to exclude from this <see cref="T:System.Drawing.Region" />. </param>
		// Token: 0x06000279 RID: 633 RVA: 0x00004644 File Offset: 0x00002844
		public void Exclude(Rectangle rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates this <see cref="T:System.Drawing.Region" /> to contain only the portion of its interior that does not intersect with the specified <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <param name="rect">The <see cref="T:System.Drawing.RectangleF" /> structure to exclude from this <see cref="T:System.Drawing.Region" />. </param>
		// Token: 0x0600027A RID: 634 RVA: 0x00004644 File Offset: 0x00002844
		public void Exclude(RectangleF rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates this <see cref="T:System.Drawing.Region" /> to contain only the portion of its interior that does not intersect with the specified <see cref="T:System.Drawing.Region" />.</summary>
		/// <param name="region">The <see cref="T:System.Drawing.Region" /> to exclude from this <see cref="T:System.Drawing.Region" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="region" /> is <see langword="null" />.</exception>
		// Token: 0x0600027B RID: 635 RVA: 0x00004644 File Offset: 0x00002844
		public void Exclude(Region region)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.Region" /> from a handle to the specified existing GDI region.</summary>
		/// <param name="hrgn">A handle to an existing <see cref="T:System.Drawing.Region" />. </param>
		/// <returns>The new <see cref="T:System.Drawing.Region" />.</returns>
		// Token: 0x0600027C RID: 636 RVA: 0x00004667 File Offset: 0x00002867
		public static Region FromHrgn(IntPtr hrgn)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets a <see cref="T:System.Drawing.RectangleF" /> structure that represents a rectangle that bounds this <see cref="T:System.Drawing.Region" /> on the drawing surface of a <see cref="T:System.Drawing.Graphics" /> object.</summary>
		/// <param name="g">The <see cref="T:System.Drawing.Graphics" /> on which this <see cref="T:System.Drawing.Region" /> is drawn. </param>
		/// <returns>A <see cref="T:System.Drawing.RectangleF" /> structure that represents the bounding rectangle for this <see cref="T:System.Drawing.Region" /> on the specified drawing surface.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="g" /> is <see langword="null" />.</exception>
		// Token: 0x0600027D RID: 637 RVA: 0x00004B40 File Offset: 0x00002D40
		public RectangleF GetBounds(Graphics g)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(RectangleF);
		}

		/// <summary>Returns a Windows handle to this <see cref="T:System.Drawing.Region" /> in the specified graphics context.</summary>
		/// <param name="g">The <see cref="T:System.Drawing.Graphics" /> on which this <see cref="T:System.Drawing.Region" /> is drawn. </param>
		/// <returns>A Windows handle to this <see cref="T:System.Drawing.Region" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="g" /> is <see langword="null" />.</exception>
		// Token: 0x0600027E RID: 638 RVA: 0x00004B5C File Offset: 0x00002D5C
		public IntPtr GetHrgn(Graphics g)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Returns a <see cref="T:System.Drawing.Drawing2D.RegionData" /> that represents the information that describes this <see cref="T:System.Drawing.Region" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.Drawing2D.RegionData" /> that represents the information that describes this <see cref="T:System.Drawing.Region" />.</returns>
		// Token: 0x0600027F RID: 639 RVA: 0x00004667 File Offset: 0x00002867
		public RegionData GetRegionData()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns an array of <see cref="T:System.Drawing.RectangleF" /> structures that approximate this <see cref="T:System.Drawing.Region" /> after the specified matrix transformation is applied.</summary>
		/// <param name="matrix">A <see cref="T:System.Drawing.Drawing2D.Matrix" /> that represents a geometric transformation to apply to the region. </param>
		/// <returns>An array of <see cref="T:System.Drawing.RectangleF" /> structures that approximate this <see cref="T:System.Drawing.Region" /> after the specified matrix transformation is applied.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="matrix" /> is <see langword="null" />.</exception>
		// Token: 0x06000280 RID: 640 RVA: 0x00004667 File Offset: 0x00002867
		public RectangleF[] GetRegionScans(Matrix matrix)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Updates this <see cref="T:System.Drawing.Region" /> to the intersection of itself with the specified <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="path">The <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> to intersect with this <see cref="T:System.Drawing.Region" />. </param>
		// Token: 0x06000281 RID: 641 RVA: 0x00004644 File Offset: 0x00002844
		public void Intersect(GraphicsPath path)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates this <see cref="T:System.Drawing.Region" /> to the intersection of itself with the specified <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <param name="rect">The <see cref="T:System.Drawing.Rectangle" /> structure to intersect with this <see cref="T:System.Drawing.Region" />. </param>
		// Token: 0x06000282 RID: 642 RVA: 0x00004644 File Offset: 0x00002844
		public void Intersect(Rectangle rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates this <see cref="T:System.Drawing.Region" /> to the intersection of itself with the specified <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <param name="rect">The <see cref="T:System.Drawing.RectangleF" /> structure to intersect with this <see cref="T:System.Drawing.Region" />. </param>
		// Token: 0x06000283 RID: 643 RVA: 0x00004644 File Offset: 0x00002844
		public void Intersect(RectangleF rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates this <see cref="T:System.Drawing.Region" /> to the intersection of itself with the specified <see cref="T:System.Drawing.Region" />.</summary>
		/// <param name="region">The <see cref="T:System.Drawing.Region" /> to intersect with this <see cref="T:System.Drawing.Region" />. </param>
		// Token: 0x06000284 RID: 644 RVA: 0x00004644 File Offset: 0x00002844
		public void Intersect(Region region)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Tests whether this <see cref="T:System.Drawing.Region" /> has an empty interior on the specified drawing surface.</summary>
		/// <param name="g">A <see cref="T:System.Drawing.Graphics" /> that represents a drawing surface. </param>
		/// <returns>
		///     <see langword="true" /> if the interior of this <see cref="T:System.Drawing.Region" /> is empty when the transformation associated with <paramref name="g" /> is applied; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="g" /> is <see langword="null" />.</exception>
		// Token: 0x06000285 RID: 645 RVA: 0x00004B78 File Offset: 0x00002D78
		public bool IsEmpty(Graphics g)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Tests whether this <see cref="T:System.Drawing.Region" /> has an infinite interior on the specified drawing surface.</summary>
		/// <param name="g">A <see cref="T:System.Drawing.Graphics" /> that represents a drawing surface. </param>
		/// <returns>
		///     <see langword="true" /> if the interior of this <see cref="T:System.Drawing.Region" /> is infinite when the transformation associated with <paramref name="g" /> is applied; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="g" /> is <see langword="null" />.</exception>
		// Token: 0x06000286 RID: 646 RVA: 0x00004B94 File Offset: 0x00002D94
		public bool IsInfinite(Graphics g)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Tests whether the specified <see cref="T:System.Drawing.Point" /> structure is contained within this <see cref="T:System.Drawing.Region" />.</summary>
		/// <param name="point">The <see cref="T:System.Drawing.Point" /> structure to test. </param>
		/// <returns>
		///     <see langword="true" /> when <paramref name="point" /> is contained within this <see cref="T:System.Drawing.Region" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000287 RID: 647 RVA: 0x00004BB0 File Offset: 0x00002DB0
		public bool IsVisible(Point point)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Tests whether the specified <see cref="T:System.Drawing.Point" /> structure is contained within this <see cref="T:System.Drawing.Region" /> when drawn using the specified <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="point">The <see cref="T:System.Drawing.Point" /> structure to test. </param>
		/// <param name="g">A <see cref="T:System.Drawing.Graphics" /> that represents a graphics context. </param>
		/// <returns>
		///     <see langword="true" /> when <paramref name="point" /> is contained within this <see cref="T:System.Drawing.Region" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000288 RID: 648 RVA: 0x00004BCC File Offset: 0x00002DCC
		public bool IsVisible(Point point, Graphics g)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Tests whether the specified <see cref="T:System.Drawing.PointF" /> structure is contained within this <see cref="T:System.Drawing.Region" />.</summary>
		/// <param name="point">The <see cref="T:System.Drawing.PointF" /> structure to test. </param>
		/// <returns>
		///     <see langword="true" /> when <paramref name="point" /> is contained within this <see cref="T:System.Drawing.Region" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000289 RID: 649 RVA: 0x00004BE8 File Offset: 0x00002DE8
		public bool IsVisible(PointF point)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Tests whether the specified <see cref="T:System.Drawing.PointF" /> structure is contained within this <see cref="T:System.Drawing.Region" /> when drawn using the specified <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="point">The <see cref="T:System.Drawing.PointF" /> structure to test. </param>
		/// <param name="g">A <see cref="T:System.Drawing.Graphics" /> that represents a graphics context. </param>
		/// <returns>
		///     <see langword="true" /> when <paramref name="point" /> is contained within this <see cref="T:System.Drawing.Region" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600028A RID: 650 RVA: 0x00004C04 File Offset: 0x00002E04
		public bool IsVisible(PointF point, Graphics g)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Tests whether any portion of the specified <see cref="T:System.Drawing.Rectangle" /> structure is contained within this <see cref="T:System.Drawing.Region" />.</summary>
		/// <param name="rect">The <see cref="T:System.Drawing.Rectangle" /> structure to test. </param>
		/// <returns>This method returns <see langword="true" /> when any portion of <paramref name="rect" /> is contained within this <see cref="T:System.Drawing.Region" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600028B RID: 651 RVA: 0x00004C20 File Offset: 0x00002E20
		public bool IsVisible(Rectangle rect)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Tests whether any portion of the specified <see cref="T:System.Drawing.Rectangle" /> structure is contained within this <see cref="T:System.Drawing.Region" /> when drawn using the specified <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="rect">The <see cref="T:System.Drawing.Rectangle" /> structure to test. </param>
		/// <param name="g">A <see cref="T:System.Drawing.Graphics" /> that represents a graphics context. </param>
		/// <returns>
		///     <see langword="true" /> when any portion of the <paramref name="rect" /> is contained within this <see cref="T:System.Drawing.Region" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600028C RID: 652 RVA: 0x00004C3C File Offset: 0x00002E3C
		public bool IsVisible(Rectangle rect, Graphics g)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Tests whether any portion of the specified <see cref="T:System.Drawing.RectangleF" /> structure is contained within this <see cref="T:System.Drawing.Region" />.</summary>
		/// <param name="rect">The <see cref="T:System.Drawing.RectangleF" /> structure to test. </param>
		/// <returns>
		///     <see langword="true" /> when any portion of <paramref name="rect" /> is contained within this <see cref="T:System.Drawing.Region" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600028D RID: 653 RVA: 0x00004C58 File Offset: 0x00002E58
		public bool IsVisible(RectangleF rect)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Tests whether any portion of the specified <see cref="T:System.Drawing.RectangleF" /> structure is contained within this <see cref="T:System.Drawing.Region" /> when drawn using the specified <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="rect">The <see cref="T:System.Drawing.RectangleF" /> structure to test. </param>
		/// <param name="g">A <see cref="T:System.Drawing.Graphics" /> that represents a graphics context. </param>
		/// <returns>
		///     <see langword="true" /> when <paramref name="rect" /> is contained within this <see cref="T:System.Drawing.Region" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600028E RID: 654 RVA: 0x00004C74 File Offset: 0x00002E74
		public bool IsVisible(RectangleF rect, Graphics g)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Tests whether the specified point is contained within this <see cref="T:System.Drawing.Region" /> object when drawn using the specified <see cref="T:System.Drawing.Graphics" /> object.</summary>
		/// <param name="x">The x-coordinate of the point to test. </param>
		/// <param name="y">The y-coordinate of the point to test. </param>
		/// <param name="g">A <see cref="T:System.Drawing.Graphics" /> that represents a graphics context. </param>
		/// <returns>
		///     <see langword="true" /> when the specified point is contained within this <see cref="T:System.Drawing.Region" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600028F RID: 655 RVA: 0x00004C90 File Offset: 0x00002E90
		public bool IsVisible(int x, int y, Graphics g)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Tests whether any portion of the specified rectangle is contained within this <see cref="T:System.Drawing.Region" />.</summary>
		/// <param name="x">The x-coordinate of the upper-left corner of the rectangle to test. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the rectangle to test. </param>
		/// <param name="width">The width of the rectangle to test. </param>
		/// <param name="height">The height of the rectangle to test. </param>
		/// <returns>
		///     <see langword="true" /> when any portion of the specified rectangle is contained within this <see cref="T:System.Drawing.Region" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000290 RID: 656 RVA: 0x00004CAC File Offset: 0x00002EAC
		public bool IsVisible(int x, int y, int width, int height)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Tests whether any portion of the specified rectangle is contained within this <see cref="T:System.Drawing.Region" /> when drawn using the specified <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="x">The x-coordinate of the upper-left corner of the rectangle to test. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the rectangle to test. </param>
		/// <param name="width">The width of the rectangle to test. </param>
		/// <param name="height">The height of the rectangle to test. </param>
		/// <param name="g">A <see cref="T:System.Drawing.Graphics" /> that represents a graphics context. </param>
		/// <returns>
		///     <see langword="true" /> when any portion of the specified rectangle is contained within this <see cref="T:System.Drawing.Region" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000291 RID: 657 RVA: 0x00004CC8 File Offset: 0x00002EC8
		public bool IsVisible(int x, int y, int width, int height, Graphics g)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Tests whether the specified point is contained within this <see cref="T:System.Drawing.Region" />.</summary>
		/// <param name="x">The x-coordinate of the point to test. </param>
		/// <param name="y">The y-coordinate of the point to test. </param>
		/// <returns>
		///     <see langword="true" /> when the specified point is contained within this <see cref="T:System.Drawing.Region" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000292 RID: 658 RVA: 0x00004CE4 File Offset: 0x00002EE4
		public bool IsVisible(float x, float y)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Tests whether the specified point is contained within this <see cref="T:System.Drawing.Region" /> when drawn using the specified <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="x">The x-coordinate of the point to test. </param>
		/// <param name="y">The y-coordinate of the point to test. </param>
		/// <param name="g">A <see cref="T:System.Drawing.Graphics" /> that represents a graphics context. </param>
		/// <returns>
		///     <see langword="true" /> when the specified point is contained within this <see cref="T:System.Drawing.Region" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000293 RID: 659 RVA: 0x00004D00 File Offset: 0x00002F00
		public bool IsVisible(float x, float y, Graphics g)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Tests whether any portion of the specified rectangle is contained within this <see cref="T:System.Drawing.Region" />.</summary>
		/// <param name="x">The x-coordinate of the upper-left corner of the rectangle to test. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the rectangle to test. </param>
		/// <param name="width">The width of the rectangle to test. </param>
		/// <param name="height">The height of the rectangle to test. </param>
		/// <returns>
		///     <see langword="true" /> when any portion of the specified rectangle is contained within this <see cref="T:System.Drawing.Region" /> object; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000294 RID: 660 RVA: 0x00004D1C File Offset: 0x00002F1C
		public bool IsVisible(float x, float y, float width, float height)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Tests whether any portion of the specified rectangle is contained within this <see cref="T:System.Drawing.Region" /> when drawn using the specified <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="x">The x-coordinate of the upper-left corner of the rectangle to test. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the rectangle to test. </param>
		/// <param name="width">The width of the rectangle to test. </param>
		/// <param name="height">The height of the rectangle to test. </param>
		/// <param name="g">A <see cref="T:System.Drawing.Graphics" /> that represents a graphics context. </param>
		/// <returns>
		///     <see langword="true" /> when any portion of the specified rectangle is contained within this <see cref="T:System.Drawing.Region" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000295 RID: 661 RVA: 0x00004D38 File Offset: 0x00002F38
		public bool IsVisible(float x, float y, float width, float height, Graphics g)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Initializes this <see cref="T:System.Drawing.Region" /> to an empty interior.</summary>
		// Token: 0x06000296 RID: 662 RVA: 0x00004644 File Offset: 0x00002844
		public void MakeEmpty()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes this <see cref="T:System.Drawing.Region" /> object to an infinite interior.</summary>
		// Token: 0x06000297 RID: 663 RVA: 0x00004644 File Offset: 0x00002844
		public void MakeInfinite()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Releases the handle of the <see cref="T:System.Drawing.Region" />.</summary>
		/// <param name="regionHandle">The handle to the <see cref="T:System.Drawing.Region" />.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="regionHandle" /> is <see langword="null" />.</exception>
		// Token: 0x06000298 RID: 664 RVA: 0x00004644 File Offset: 0x00002844
		public void ReleaseHrgn(IntPtr regionHandle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Transforms this <see cref="T:System.Drawing.Region" /> by the specified <see cref="T:System.Drawing.Drawing2D.Matrix" />.</summary>
		/// <param name="matrix">The <see cref="T:System.Drawing.Drawing2D.Matrix" /> by which to transform this <see cref="T:System.Drawing.Region" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="matrix" /> is <see langword="null" />.</exception>
		// Token: 0x06000299 RID: 665 RVA: 0x00004644 File Offset: 0x00002844
		public void Transform(Matrix matrix)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Offsets the coordinates of this <see cref="T:System.Drawing.Region" /> by the specified amount.</summary>
		/// <param name="dx">The amount to offset this <see cref="T:System.Drawing.Region" /> horizontally. </param>
		/// <param name="dy">The amount to offset this <see cref="T:System.Drawing.Region" /> vertically. </param>
		// Token: 0x0600029A RID: 666 RVA: 0x00004644 File Offset: 0x00002844
		public void Translate(int dx, int dy)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Offsets the coordinates of this <see cref="T:System.Drawing.Region" /> by the specified amount.</summary>
		/// <param name="dx">The amount to offset this <see cref="T:System.Drawing.Region" /> horizontally. </param>
		/// <param name="dy">The amount to offset this <see cref="T:System.Drawing.Region" /> vertically. </param>
		// Token: 0x0600029B RID: 667 RVA: 0x00004644 File Offset: 0x00002844
		public void Translate(float dx, float dy)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates this <see cref="T:System.Drawing.Region" /> to the union of itself and the specified <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="path">The <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> to unite with this <see cref="T:System.Drawing.Region" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> is <see langword="null" />.</exception>
		// Token: 0x0600029C RID: 668 RVA: 0x00004644 File Offset: 0x00002844
		public void Union(GraphicsPath path)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates this <see cref="T:System.Drawing.Region" /> to the union of itself and the specified <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <param name="rect">The <see cref="T:System.Drawing.Rectangle" /> structure to unite with this <see cref="T:System.Drawing.Region" />. </param>
		// Token: 0x0600029D RID: 669 RVA: 0x00004644 File Offset: 0x00002844
		public void Union(Rectangle rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates this <see cref="T:System.Drawing.Region" /> to the union of itself and the specified <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <param name="rect">The <see cref="T:System.Drawing.RectangleF" /> structure to unite with this <see cref="T:System.Drawing.Region" />. </param>
		// Token: 0x0600029E RID: 670 RVA: 0x00004644 File Offset: 0x00002844
		public void Union(RectangleF rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates this <see cref="T:System.Drawing.Region" /> to the union of itself and the specified <see cref="T:System.Drawing.Region" />.</summary>
		/// <param name="region">The <see cref="T:System.Drawing.Region" /> to unite with this <see cref="T:System.Drawing.Region" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="region" /> is <see langword="null" />.</exception>
		// Token: 0x0600029F RID: 671 RVA: 0x00004644 File Offset: 0x00002844
		public void Union(Region region)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates this <see cref="T:System.Drawing.Region" /> to the union minus the intersection of itself with the specified <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="path">The <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> to <see cref="Overload:System.Drawing.Region.Xor" /> with this <see cref="T:System.Drawing.Region" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> is <see langword="null" />.</exception>
		// Token: 0x060002A0 RID: 672 RVA: 0x00004644 File Offset: 0x00002844
		public void Xor(GraphicsPath path)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates this <see cref="T:System.Drawing.Region" /> to the union minus the intersection of itself with the specified <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <param name="rect">The <see cref="T:System.Drawing.Rectangle" /> structure to <see cref="Overload:System.Drawing.Region.Xor" /> with this <see cref="T:System.Drawing.Region" />. </param>
		// Token: 0x060002A1 RID: 673 RVA: 0x00004644 File Offset: 0x00002844
		public void Xor(Rectangle rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates this <see cref="T:System.Drawing.Region" /> to the union minus the intersection of itself with the specified <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <param name="rect">The <see cref="T:System.Drawing.RectangleF" /> structure to <see cref="M:System.Drawing.Region.Xor(System.Drawing.Drawing2D.GraphicsPath)" /> with this <see cref="T:System.Drawing.Region" />. </param>
		// Token: 0x060002A2 RID: 674 RVA: 0x00004644 File Offset: 0x00002844
		public void Xor(RectangleF rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates this <see cref="T:System.Drawing.Region" /> to the union minus the intersection of itself with the specified <see cref="T:System.Drawing.Region" />.</summary>
		/// <param name="region">The <see cref="T:System.Drawing.Region" /> to <see cref="Overload:System.Drawing.Region.Xor" /> with this <see cref="T:System.Drawing.Region" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="region" /> is <see langword="null" />.</exception>
		// Token: 0x060002A3 RID: 675 RVA: 0x00004644 File Offset: 0x00002844
		public void Xor(Region region)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
