﻿using System;
using Unity;

namespace System.Drawing
{
	/// <summary>Specifies the fonts used to display text in Windows display elements.</summary>
	// Token: 0x02000086 RID: 134
	public sealed class SystemFonts
	{
		// Token: 0x06000764 RID: 1892 RVA: 0x00004644 File Offset: 0x00002844
		internal SystemFonts()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Font" /> that is used to display text in the title bars of windows.</summary>
		/// <returns>A <see cref="T:System.Drawing.Font" /> that is used to display text in the title bars of windows.</returns>
		// Token: 0x170002FC RID: 764
		// (get) Token: 0x06000765 RID: 1893 RVA: 0x00004667 File Offset: 0x00002867
		public static Font CaptionFont
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the default font that applications can use for dialog boxes and forms.</summary>
		/// <returns>The default <see cref="T:System.Drawing.Font" /> of the system. The value returned will vary depending on the user's operating system and the local culture setting of their system.</returns>
		// Token: 0x170002FD RID: 765
		// (get) Token: 0x06000766 RID: 1894 RVA: 0x00004667 File Offset: 0x00002867
		public static Font DefaultFont
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a font that applications can use for dialog boxes and forms.</summary>
		/// <returns>A <see cref="T:System.Drawing.Font" /> that can be used for dialog boxes and forms, depending on the operating system and local culture setting of the system.</returns>
		// Token: 0x170002FE RID: 766
		// (get) Token: 0x06000767 RID: 1895 RVA: 0x00004667 File Offset: 0x00002867
		public static Font DialogFont
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Font" /> that is used for icon titles.</summary>
		/// <returns>A <see cref="T:System.Drawing.Font" /> that is used for icon titles.</returns>
		// Token: 0x170002FF RID: 767
		// (get) Token: 0x06000768 RID: 1896 RVA: 0x00004667 File Offset: 0x00002867
		public static Font IconTitleFont
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Font" /> that is used for menus.</summary>
		/// <returns>A <see cref="T:System.Drawing.Font" /> that is used for menus.</returns>
		// Token: 0x17000300 RID: 768
		// (get) Token: 0x06000769 RID: 1897 RVA: 0x00004667 File Offset: 0x00002867
		public static Font MenuFont
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Font" /> that is used for message boxes.</summary>
		/// <returns>A <see cref="T:System.Drawing.Font" /> that is used for message boxes</returns>
		// Token: 0x17000301 RID: 769
		// (get) Token: 0x0600076A RID: 1898 RVA: 0x00004667 File Offset: 0x00002867
		public static Font MessageBoxFont
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Font" /> that is used to display text in the title bars of small windows, such as tool windows.</summary>
		/// <returns>A <see cref="T:System.Drawing.Font" /> that is used to display text in the title bars of small windows, such as tool windows.</returns>
		// Token: 0x17000302 RID: 770
		// (get) Token: 0x0600076B RID: 1899 RVA: 0x00004667 File Offset: 0x00002867
		public static Font SmallCaptionFont
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Font" /> that is used to display text in the status bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.Font" /> that is used to display text in the status bar.</returns>
		// Token: 0x17000303 RID: 771
		// (get) Token: 0x0600076C RID: 1900 RVA: 0x00004667 File Offset: 0x00002867
		public static Font StatusFont
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Returns a font object that corresponds to the specified system font name.</summary>
		/// <param name="systemFontName">The name of the system font you need a font object for.</param>
		/// <returns>A <see cref="T:System.Drawing.Font" /> if the specified name matches a value in <see cref="T:System.Drawing.SystemFonts" />; otherwise, <see langword="null" />.</returns>
		// Token: 0x0600076D RID: 1901 RVA: 0x00004667 File Offset: 0x00002867
		public static Font GetFontByName(string systemFontName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
