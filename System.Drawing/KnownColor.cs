﻿using System;

namespace System.Drawing
{
	/// <summary>Specifies the known system colors.</summary>
	// Token: 0x02000005 RID: 5
	public enum KnownColor
	{
		/// <summary>The system-defined color of the active window's border.</summary>
		// Token: 0x04000036 RID: 54
		ActiveBorder = 1,
		/// <summary>The system-defined color of the background of the active window's title bar.</summary>
		// Token: 0x04000037 RID: 55
		ActiveCaption,
		/// <summary>The system-defined color of the text in the active window's title bar.</summary>
		// Token: 0x04000038 RID: 56
		ActiveCaptionText,
		/// <summary>The system-defined color of the application workspace. The application workspace is the area in a multiple-document view that is not being occupied by documents.</summary>
		// Token: 0x04000039 RID: 57
		AppWorkspace,
		/// <summary>The system-defined face color of a 3-D element.</summary>
		// Token: 0x0400003A RID: 58
		Control,
		/// <summary>The system-defined shadow color of a 3-D element. The shadow color is applied to parts of a 3-D element that face away from the light source.</summary>
		// Token: 0x0400003B RID: 59
		ControlDark,
		/// <summary>The system-defined color that is the dark shadow color of a 3-D element. The dark shadow color is applied to the parts of a 3-D element that are the darkest color.</summary>
		// Token: 0x0400003C RID: 60
		ControlDarkDark,
		/// <summary>The system-defined color that is the light color of a 3-D element. The light color is applied to parts of a 3-D element that face the light source.</summary>
		// Token: 0x0400003D RID: 61
		ControlLight,
		/// <summary>The system-defined highlight color of a 3-D element. The highlight color is applied to the parts of a 3-D element that are the lightest color.</summary>
		// Token: 0x0400003E RID: 62
		ControlLightLight,
		/// <summary>The system-defined color of text in a 3-D element.</summary>
		// Token: 0x0400003F RID: 63
		ControlText,
		/// <summary>The system-defined color of the desktop.</summary>
		// Token: 0x04000040 RID: 64
		Desktop,
		/// <summary>The system-defined color of dimmed text. Items in a list that are disabled are displayed in dimmed text.</summary>
		// Token: 0x04000041 RID: 65
		GrayText,
		/// <summary>The system-defined color of the background of selected items. This includes selected menu items as well as selected text. </summary>
		// Token: 0x04000042 RID: 66
		Highlight,
		/// <summary>The system-defined color of the text of selected items.</summary>
		// Token: 0x04000043 RID: 67
		HighlightText,
		/// <summary>The system-defined color used to designate a hot-tracked item. Single-clicking a hot-tracked item executes the item.</summary>
		// Token: 0x04000044 RID: 68
		HotTrack,
		/// <summary>The system-defined color of an inactive window's border.</summary>
		// Token: 0x04000045 RID: 69
		InactiveBorder,
		/// <summary>The system-defined color of the background of an inactive window's title bar.</summary>
		// Token: 0x04000046 RID: 70
		InactiveCaption,
		/// <summary>The system-defined color of the text in an inactive window's title bar.</summary>
		// Token: 0x04000047 RID: 71
		InactiveCaptionText,
		/// <summary>The system-defined color of the background of a ToolTip.</summary>
		// Token: 0x04000048 RID: 72
		Info,
		/// <summary>The system-defined color of the text of a ToolTip.</summary>
		// Token: 0x04000049 RID: 73
		InfoText,
		/// <summary>The system-defined color of a menu's background.</summary>
		// Token: 0x0400004A RID: 74
		Menu,
		/// <summary>The system-defined color of a menu's text.</summary>
		// Token: 0x0400004B RID: 75
		MenuText,
		/// <summary>The system-defined color of the background of a scroll bar.</summary>
		// Token: 0x0400004C RID: 76
		ScrollBar,
		/// <summary>The system-defined color of the background in the client area of a window.</summary>
		// Token: 0x0400004D RID: 77
		Window,
		/// <summary>The system-defined color of a window frame.</summary>
		// Token: 0x0400004E RID: 78
		WindowFrame,
		/// <summary>The system-defined color of the text in the client area of a window.</summary>
		// Token: 0x0400004F RID: 79
		WindowText,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000050 RID: 80
		Transparent,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000051 RID: 81
		AliceBlue,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000052 RID: 82
		AntiqueWhite,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000053 RID: 83
		Aqua,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000054 RID: 84
		Aquamarine,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000055 RID: 85
		Azure,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000056 RID: 86
		Beige,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000057 RID: 87
		Bisque,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000058 RID: 88
		Black,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000059 RID: 89
		BlanchedAlmond,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400005A RID: 90
		Blue,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400005B RID: 91
		BlueViolet,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400005C RID: 92
		Brown,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400005D RID: 93
		BurlyWood,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400005E RID: 94
		CadetBlue,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400005F RID: 95
		Chartreuse,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000060 RID: 96
		Chocolate,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000061 RID: 97
		Coral,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000062 RID: 98
		CornflowerBlue,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000063 RID: 99
		Cornsilk,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000064 RID: 100
		Crimson,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000065 RID: 101
		Cyan,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000066 RID: 102
		DarkBlue,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000067 RID: 103
		DarkCyan,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000068 RID: 104
		DarkGoldenrod,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000069 RID: 105
		DarkGray,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400006A RID: 106
		DarkGreen,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400006B RID: 107
		DarkKhaki,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400006C RID: 108
		DarkMagenta,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400006D RID: 109
		DarkOliveGreen,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400006E RID: 110
		DarkOrange,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400006F RID: 111
		DarkOrchid,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000070 RID: 112
		DarkRed,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000071 RID: 113
		DarkSalmon,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000072 RID: 114
		DarkSeaGreen,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000073 RID: 115
		DarkSlateBlue,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000074 RID: 116
		DarkSlateGray,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000075 RID: 117
		DarkTurquoise,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000076 RID: 118
		DarkViolet,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000077 RID: 119
		DeepPink,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000078 RID: 120
		DeepSkyBlue,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000079 RID: 121
		DimGray,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400007A RID: 122
		DodgerBlue,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400007B RID: 123
		Firebrick,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400007C RID: 124
		FloralWhite,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400007D RID: 125
		ForestGreen,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400007E RID: 126
		Fuchsia,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400007F RID: 127
		Gainsboro,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000080 RID: 128
		GhostWhite,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000081 RID: 129
		Gold,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000082 RID: 130
		Goldenrod,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000083 RID: 131
		Gray,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000084 RID: 132
		Green,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000085 RID: 133
		GreenYellow,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000086 RID: 134
		Honeydew,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000087 RID: 135
		HotPink,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000088 RID: 136
		IndianRed,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000089 RID: 137
		Indigo,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400008A RID: 138
		Ivory,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400008B RID: 139
		Khaki,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400008C RID: 140
		Lavender,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400008D RID: 141
		LavenderBlush,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400008E RID: 142
		LawnGreen,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400008F RID: 143
		LemonChiffon,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000090 RID: 144
		LightBlue,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000091 RID: 145
		LightCoral,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000092 RID: 146
		LightCyan,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000093 RID: 147
		LightGoldenrodYellow,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000094 RID: 148
		LightGray,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000095 RID: 149
		LightGreen,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000096 RID: 150
		LightPink,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000097 RID: 151
		LightSalmon,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000098 RID: 152
		LightSeaGreen,
		/// <summary>A system-defined color.</summary>
		// Token: 0x04000099 RID: 153
		LightSkyBlue,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400009A RID: 154
		LightSlateGray,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400009B RID: 155
		LightSteelBlue,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400009C RID: 156
		LightYellow,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400009D RID: 157
		Lime,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400009E RID: 158
		LimeGreen,
		/// <summary>A system-defined color.</summary>
		// Token: 0x0400009F RID: 159
		Linen,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000A0 RID: 160
		Magenta,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000A1 RID: 161
		Maroon,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000A2 RID: 162
		MediumAquamarine,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000A3 RID: 163
		MediumBlue,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000A4 RID: 164
		MediumOrchid,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000A5 RID: 165
		MediumPurple,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000A6 RID: 166
		MediumSeaGreen,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000A7 RID: 167
		MediumSlateBlue,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000A8 RID: 168
		MediumSpringGreen,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000A9 RID: 169
		MediumTurquoise,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000AA RID: 170
		MediumVioletRed,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000AB RID: 171
		MidnightBlue,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000AC RID: 172
		MintCream,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000AD RID: 173
		MistyRose,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000AE RID: 174
		Moccasin,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000AF RID: 175
		NavajoWhite,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000B0 RID: 176
		Navy,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000B1 RID: 177
		OldLace,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000B2 RID: 178
		Olive,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000B3 RID: 179
		OliveDrab,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000B4 RID: 180
		Orange,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000B5 RID: 181
		OrangeRed,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000B6 RID: 182
		Orchid,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000B7 RID: 183
		PaleGoldenrod,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000B8 RID: 184
		PaleGreen,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000B9 RID: 185
		PaleTurquoise,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000BA RID: 186
		PaleVioletRed,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000BB RID: 187
		PapayaWhip,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000BC RID: 188
		PeachPuff,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000BD RID: 189
		Peru,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000BE RID: 190
		Pink,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000BF RID: 191
		Plum,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000C0 RID: 192
		PowderBlue,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000C1 RID: 193
		Purple,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000C2 RID: 194
		Red,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000C3 RID: 195
		RosyBrown,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000C4 RID: 196
		RoyalBlue,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000C5 RID: 197
		SaddleBrown,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000C6 RID: 198
		Salmon,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000C7 RID: 199
		SandyBrown,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000C8 RID: 200
		SeaGreen,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000C9 RID: 201
		SeaShell,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000CA RID: 202
		Sienna,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000CB RID: 203
		Silver,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000CC RID: 204
		SkyBlue,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000CD RID: 205
		SlateBlue,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000CE RID: 206
		SlateGray,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000CF RID: 207
		Snow,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000D0 RID: 208
		SpringGreen,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000D1 RID: 209
		SteelBlue,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000D2 RID: 210
		Tan,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000D3 RID: 211
		Teal,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000D4 RID: 212
		Thistle,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000D5 RID: 213
		Tomato,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000D6 RID: 214
		Turquoise,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000D7 RID: 215
		Violet,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000D8 RID: 216
		Wheat,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000D9 RID: 217
		White,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000DA RID: 218
		WhiteSmoke,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000DB RID: 219
		Yellow,
		/// <summary>A system-defined color.</summary>
		// Token: 0x040000DC RID: 220
		YellowGreen,
		/// <summary>The system-defined face color of a 3-D element.</summary>
		// Token: 0x040000DD RID: 221
		ButtonFace,
		/// <summary>The system-defined color that is the highlight color of a 3-D element. This color is applied to parts of a 3-D element that face the light source.</summary>
		// Token: 0x040000DE RID: 222
		ButtonHighlight,
		/// <summary>The system-defined color that is the shadow color of a 3-D element. This color is applied to parts of a 3-D element that face away from the light source.</summary>
		// Token: 0x040000DF RID: 223
		ButtonShadow,
		/// <summary>The system-defined color of the lightest color in the color gradient of an active window's title bar.</summary>
		// Token: 0x040000E0 RID: 224
		GradientActiveCaption,
		/// <summary>The system-defined color of the lightest color in the color gradient of an inactive window's title bar. </summary>
		// Token: 0x040000E1 RID: 225
		GradientInactiveCaption,
		/// <summary>The system-defined color of the background of a menu bar.</summary>
		// Token: 0x040000E2 RID: 226
		MenuBar,
		/// <summary>The system-defined color used to highlight menu items when the menu appears as a flat menu.</summary>
		// Token: 0x040000E3 RID: 227
		MenuHighlight
	}
}
