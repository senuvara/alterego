﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Drawing
{
	/// <summary>Converts colors from one data type to another. Access this class through the <see cref="T:System.ComponentModel.TypeDescriptor" />.</summary>
	// Token: 0x0200007A RID: 122
	public class ColorConverter : TypeConverter
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.ColorConverter" /> class.</summary>
		// Token: 0x0600067A RID: 1658 RVA: 0x00004644 File Offset: 0x00002844
		public ColorConverter()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
