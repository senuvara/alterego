﻿using System;
using System.ComponentModel;
using System.Drawing.Design;

namespace System.Drawing
{
	/// <summary>Specifies alignment of content on the drawing surface.</summary>
	// Token: 0x02000069 RID: 105
	[Editor("System.Drawing.Design.ContentAlignmentEditor, System.Drawing.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
	public enum ContentAlignment
	{
		/// <summary>Content is vertically aligned at the bottom, and horizontally aligned at the center.</summary>
		// Token: 0x0400031F RID: 799
		BottomCenter = 512,
		/// <summary>Content is vertically aligned at the bottom, and horizontally aligned on the left.</summary>
		// Token: 0x04000320 RID: 800
		BottomLeft = 256,
		/// <summary>Content is vertically aligned at the bottom, and horizontally aligned on the right.</summary>
		// Token: 0x04000321 RID: 801
		BottomRight = 1024,
		/// <summary>Content is vertically aligned in the middle, and horizontally aligned at the center.</summary>
		// Token: 0x04000322 RID: 802
		MiddleCenter = 32,
		/// <summary>Content is vertically aligned in the middle, and horizontally aligned on the left.</summary>
		// Token: 0x04000323 RID: 803
		MiddleLeft = 16,
		/// <summary>Content is vertically aligned in the middle, and horizontally aligned on the right.</summary>
		// Token: 0x04000324 RID: 804
		MiddleRight = 64,
		/// <summary>Content is vertically aligned at the top, and horizontally aligned at the center.</summary>
		// Token: 0x04000325 RID: 805
		TopCenter = 2,
		/// <summary>Content is vertically aligned at the top, and horizontally aligned on the left.</summary>
		// Token: 0x04000326 RID: 806
		TopLeft = 1,
		/// <summary>Content is vertically aligned at the top, and horizontally aligned on the right.</summary>
		// Token: 0x04000327 RID: 807
		TopRight = 4
	}
}
