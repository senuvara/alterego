﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Drawing
{
	/// <summary>
	///     <see cref="T:System.Drawing.ImageConverter" />  is a class that can be used to convert <see cref="T:System.Drawing.Image" /> objects from one data type to another. Access this class through the <see cref="T:System.ComponentModel.TypeDescriptor" /> object.</summary>
	// Token: 0x0200004E RID: 78
	public class ImageConverter : TypeConverter
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.ImageConverter" /> class.</summary>
		// Token: 0x0600045C RID: 1116 RVA: 0x00004644 File Offset: 0x00002844
		public ImageConverter()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
