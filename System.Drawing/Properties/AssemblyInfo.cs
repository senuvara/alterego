﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;

[assembly: AssemblyVersion("4.0.0.0")]
[assembly: AssemblyTitle("System.Drawing.dll")]
[assembly: AssemblyDescription("System.Drawing.dll")]
[assembly: AssemblyDefaultAlias("System.Drawing.dll")]
[assembly: AssemblyCompany("Mono development team")]
[assembly: AssemblyProduct("Mono Common Language Infrastructure")]
[assembly: AssemblyCopyright("(c) Various Mono authors")]
[assembly: SatelliteContractVersion("4.0.0.0")]
[assembly: AssemblyInformationalVersion("4.0.30319.17020")]
[assembly: NeutralResourcesLanguage("en-US")]
[assembly: ComVisible(false)]
[assembly: ComCompatibleVersion(1, 0, 3300, 0)]
[assembly: AllowPartiallyTrustedCallers]
[assembly: CLSCompliant(true)]
[assembly: AssemblyDelaySign(true)]
[assembly: AssemblyFileVersion("4.0.30319.17020")]
[assembly: Dependency("System,", LoadHint.Always)]
[assembly: SecurityPermission(SecurityAction.RequestMinimum, SkipVerification = true)]
