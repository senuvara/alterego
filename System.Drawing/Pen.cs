﻿using System;
using System.Drawing.Drawing2D;
using Unity;

namespace System.Drawing
{
	/// <summary>Defines an object used to draw lines and curves. This class cannot be inherited.</summary>
	// Token: 0x02000029 RID: 41
	public sealed class Pen : MarshalByRefObject, ICloneable, IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Pen" /> class with the specified <see cref="T:System.Drawing.Brush" />.</summary>
		/// <param name="brush">A <see cref="T:System.Drawing.Brush" /> that determines the fill properties of this <see cref="T:System.Drawing.Pen" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.</exception>
		// Token: 0x0600035A RID: 858 RVA: 0x00004644 File Offset: 0x00002844
		public Pen(Brush brush)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Pen" /> class with the specified <see cref="T:System.Drawing.Brush" /> and <see cref="P:System.Drawing.Pen.Width" />.</summary>
		/// <param name="brush">A <see cref="T:System.Drawing.Brush" /> that determines the characteristics of this <see cref="T:System.Drawing.Pen" />. </param>
		/// <param name="width">The width of the new <see cref="T:System.Drawing.Pen" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.</exception>
		// Token: 0x0600035B RID: 859 RVA: 0x00004644 File Offset: 0x00002844
		public Pen(Brush brush, float width)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Pen" /> class with the specified color.</summary>
		/// <param name="color">A <see cref="T:System.Drawing.Color" /> structure that indicates the color of this <see cref="T:System.Drawing.Pen" />. </param>
		// Token: 0x0600035C RID: 860 RVA: 0x00004644 File Offset: 0x00002844
		public Pen(Color color)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Pen" /> class with the specified <see cref="T:System.Drawing.Color" /> and <see cref="P:System.Drawing.Pen.Width" /> properties.</summary>
		/// <param name="color">A <see cref="T:System.Drawing.Color" /> structure that indicates the color of this <see cref="T:System.Drawing.Pen" />. </param>
		/// <param name="width">A value indicating the width of this <see cref="T:System.Drawing.Pen" />. </param>
		// Token: 0x0600035D RID: 861 RVA: 0x00004644 File Offset: 0x00002844
		public Pen(Color color, float width)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the alignment for this <see cref="T:System.Drawing.Pen" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.Drawing2D.PenAlignment" /> that represents the alignment for this <see cref="T:System.Drawing.Pen" />.</returns>
		/// <exception cref="T:System.ComponentModel.InvalidEnumArgumentException">The specified value is not a member of <see cref="T:System.Drawing.Drawing2D.PenAlignment" />.</exception>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Pen.Alignment" /> property is set on an immutable <see cref="T:System.Drawing.Pen" />, such as those returned by the <see cref="T:System.Drawing.Pens" /> class.</exception>
		// Token: 0x170000EE RID: 238
		// (get) Token: 0x0600035E RID: 862 RVA: 0x000051EC File Offset: 0x000033EC
		// (set) Token: 0x0600035F RID: 863 RVA: 0x00004644 File Offset: 0x00002844
		public PenAlignment Alignment
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return PenAlignment.Center;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Drawing.Brush" /> that determines attributes of this <see cref="T:System.Drawing.Pen" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> that determines attributes of this <see cref="T:System.Drawing.Pen" />.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Pen.Brush" /> property is set on an immutable <see cref="T:System.Drawing.Pen" />, such as those returned by the <see cref="T:System.Drawing.Pens" /> class.</exception>
		// Token: 0x170000EF RID: 239
		// (get) Token: 0x06000360 RID: 864 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000361 RID: 865 RVA: 0x00004644 File Offset: 0x00002844
		public Brush Brush
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the color of this <see cref="T:System.Drawing.Pen" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> structure that represents the color of this <see cref="T:System.Drawing.Pen" />.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Pen.Color" /> property is set on an immutable <see cref="T:System.Drawing.Pen" />, such as those returned by the <see cref="T:System.Drawing.Pens" /> class.</exception>
		// Token: 0x170000F0 RID: 240
		// (get) Token: 0x06000362 RID: 866 RVA: 0x00005208 File Offset: 0x00003408
		// (set) Token: 0x06000363 RID: 867 RVA: 0x00004644 File Offset: 0x00002844
		public Color Color
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets an array of values that specifies a compound pen. A compound pen draws a compound line made up of parallel lines and spaces.</summary>
		/// <returns>An array of real numbers that specifies the compound array. The elements in the array must be in increasing order, not less than 0, and not greater than 1.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Pen.CompoundArray" /> property is set on an immutable <see cref="T:System.Drawing.Pen" />, such as those returned by the <see cref="T:System.Drawing.Pens" /> class.</exception>
		// Token: 0x170000F1 RID: 241
		// (get) Token: 0x06000364 RID: 868 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000365 RID: 869 RVA: 0x00004644 File Offset: 0x00002844
		public float[] CompoundArray
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a custom cap to use at the end of lines drawn with this <see cref="T:System.Drawing.Pen" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.Drawing2D.CustomLineCap" /> that represents the cap used at the end of lines drawn with this <see cref="T:System.Drawing.Pen" />.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Pen.CustomEndCap" /> property is set on an immutable <see cref="T:System.Drawing.Pen" />, such as those returned by the <see cref="T:System.Drawing.Pens" /> class.</exception>
		// Token: 0x170000F2 RID: 242
		// (get) Token: 0x06000366 RID: 870 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000367 RID: 871 RVA: 0x00004644 File Offset: 0x00002844
		public CustomLineCap CustomEndCap
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a custom cap to use at the beginning of lines drawn with this <see cref="T:System.Drawing.Pen" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.Drawing2D.CustomLineCap" /> that represents the cap used at the beginning of lines drawn with this <see cref="T:System.Drawing.Pen" />.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Pen.CustomStartCap" /> property is set on an immutable <see cref="T:System.Drawing.Pen" />, such as those returned by the <see cref="T:System.Drawing.Pens" /> class.</exception>
		// Token: 0x170000F3 RID: 243
		// (get) Token: 0x06000368 RID: 872 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000369 RID: 873 RVA: 0x00004644 File Offset: 0x00002844
		public CustomLineCap CustomStartCap
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the cap style used at the end of the dashes that make up dashed lines drawn with this <see cref="T:System.Drawing.Pen" />.</summary>
		/// <returns>One of the <see cref="T:System.Drawing.Drawing2D.DashCap" /> values that represents the cap style used at the beginning and end of the dashes that make up dashed lines drawn with this <see cref="T:System.Drawing.Pen" />.</returns>
		/// <exception cref="T:System.ComponentModel.InvalidEnumArgumentException">The specified value is not a member of <see cref="T:System.Drawing.Drawing2D.DashCap" />.</exception>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Pen.DashCap" /> property is set on an immutable <see cref="T:System.Drawing.Pen" />, such as those returned by the <see cref="T:System.Drawing.Pens" /> class.</exception>
		// Token: 0x170000F4 RID: 244
		// (get) Token: 0x0600036A RID: 874 RVA: 0x00005224 File Offset: 0x00003424
		// (set) Token: 0x0600036B RID: 875 RVA: 0x00004644 File Offset: 0x00002844
		public DashCap DashCap
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return DashCap.Flat;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the distance from the start of a line to the beginning of a dash pattern.</summary>
		/// <returns>The distance from the start of a line to the beginning of a dash pattern.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Pen.DashOffset" /> property is set on an immutable <see cref="T:System.Drawing.Pen" />, such as those returned by the <see cref="T:System.Drawing.Pens" /> class.</exception>
		// Token: 0x170000F5 RID: 245
		// (get) Token: 0x0600036C RID: 876 RVA: 0x00005240 File Offset: 0x00003440
		// (set) Token: 0x0600036D RID: 877 RVA: 0x00004644 File Offset: 0x00002844
		public float DashOffset
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets an array of custom dashes and spaces.</summary>
		/// <returns>An array of real numbers that specifies the lengths of alternating dashes and spaces in dashed lines.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Pen.DashPattern" /> property is set on an immutable <see cref="T:System.Drawing.Pen" />, such as those returned by the <see cref="T:System.Drawing.Pens" /> class.</exception>
		// Token: 0x170000F6 RID: 246
		// (get) Token: 0x0600036E RID: 878 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x0600036F RID: 879 RVA: 0x00004644 File Offset: 0x00002844
		public float[] DashPattern
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the style used for dashed lines drawn with this <see cref="T:System.Drawing.Pen" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.Drawing2D.DashStyle" /> that represents the style used for dashed lines drawn with this <see cref="T:System.Drawing.Pen" />.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Pen.DashStyle" /> property is set on an immutable <see cref="T:System.Drawing.Pen" />, such as those returned by the <see cref="T:System.Drawing.Pens" /> class.</exception>
		// Token: 0x170000F7 RID: 247
		// (get) Token: 0x06000370 RID: 880 RVA: 0x0000525C File Offset: 0x0000345C
		// (set) Token: 0x06000371 RID: 881 RVA: 0x00004644 File Offset: 0x00002844
		public DashStyle DashStyle
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return DashStyle.Solid;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the cap style used at the end of lines drawn with this <see cref="T:System.Drawing.Pen" />.</summary>
		/// <returns>One of the <see cref="T:System.Drawing.Drawing2D.LineCap" /> values that represents the cap style used at the end of lines drawn with this <see cref="T:System.Drawing.Pen" />.</returns>
		/// <exception cref="T:System.ComponentModel.InvalidEnumArgumentException">The specified value is not a member of <see cref="T:System.Drawing.Drawing2D.LineCap" />.</exception>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Pen.EndCap" /> property is set on an immutable <see cref="T:System.Drawing.Pen" />, such as those returned by the <see cref="T:System.Drawing.Pens" /> class.</exception>
		// Token: 0x170000F8 RID: 248
		// (get) Token: 0x06000372 RID: 882 RVA: 0x00005278 File Offset: 0x00003478
		// (set) Token: 0x06000373 RID: 883 RVA: 0x00004644 File Offset: 0x00002844
		public LineCap EndCap
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return LineCap.Flat;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the join style for the ends of two consecutive lines drawn with this <see cref="T:System.Drawing.Pen" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.Drawing2D.LineJoin" /> that represents the join style for the ends of two consecutive lines drawn with this <see cref="T:System.Drawing.Pen" />.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Pen.LineJoin" /> property is set on an immutable <see cref="T:System.Drawing.Pen" />, such as those returned by the <see cref="T:System.Drawing.Pens" /> class.</exception>
		// Token: 0x170000F9 RID: 249
		// (get) Token: 0x06000374 RID: 884 RVA: 0x00005294 File Offset: 0x00003494
		// (set) Token: 0x06000375 RID: 885 RVA: 0x00004644 File Offset: 0x00002844
		public LineJoin LineJoin
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return LineJoin.Miter;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the limit of the thickness of the join on a mitered corner.</summary>
		/// <returns>The limit of the thickness of the join on a mitered corner.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Pen.MiterLimit" /> property is set on an immutable <see cref="T:System.Drawing.Pen" />, such as those returned by the <see cref="T:System.Drawing.Pens" /> class.</exception>
		// Token: 0x170000FA RID: 250
		// (get) Token: 0x06000376 RID: 886 RVA: 0x000052B0 File Offset: 0x000034B0
		// (set) Token: 0x06000377 RID: 887 RVA: 0x00004644 File Offset: 0x00002844
		public float MiterLimit
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the style of lines drawn with this <see cref="T:System.Drawing.Pen" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.Drawing2D.PenType" /> enumeration that specifies the style of lines drawn with this <see cref="T:System.Drawing.Pen" />.</returns>
		// Token: 0x170000FB RID: 251
		// (get) Token: 0x06000378 RID: 888 RVA: 0x000052CC File Offset: 0x000034CC
		public PenType PenType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return PenType.SolidColor;
			}
		}

		/// <summary>Gets or sets the cap style used at the beginning of lines drawn with this <see cref="T:System.Drawing.Pen" />.</summary>
		/// <returns>One of the <see cref="T:System.Drawing.Drawing2D.LineCap" /> values that represents the cap style used at the beginning of lines drawn with this <see cref="T:System.Drawing.Pen" />.</returns>
		/// <exception cref="T:System.ComponentModel.InvalidEnumArgumentException">The specified value is not a member of <see cref="T:System.Drawing.Drawing2D.LineCap" />.</exception>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Pen.StartCap" /> property is set on an immutable <see cref="T:System.Drawing.Pen" />, such as those returned by the <see cref="T:System.Drawing.Pens" /> class.</exception>
		// Token: 0x170000FC RID: 252
		// (get) Token: 0x06000379 RID: 889 RVA: 0x000052E8 File Offset: 0x000034E8
		// (set) Token: 0x0600037A RID: 890 RVA: 0x00004644 File Offset: 0x00002844
		public LineCap StartCap
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return LineCap.Flat;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a copy of the geometric transformation for this <see cref="T:System.Drawing.Pen" />.</summary>
		/// <returns>A copy of the <see cref="T:System.Drawing.Drawing2D.Matrix" /> that represents the geometric transformation for this <see cref="T:System.Drawing.Pen" />.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Pen.Transform" /> property is set on an immutable <see cref="T:System.Drawing.Pen" />, such as those returned by the <see cref="T:System.Drawing.Pens" /> class.</exception>
		// Token: 0x170000FD RID: 253
		// (get) Token: 0x0600037B RID: 891 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x0600037C RID: 892 RVA: 0x00004644 File Offset: 0x00002844
		public Matrix Transform
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the width of this <see cref="T:System.Drawing.Pen" />, in units of the <see cref="T:System.Drawing.Graphics" /> object used for drawing.</summary>
		/// <returns>The width of this <see cref="T:System.Drawing.Pen" />.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Pen.Width" /> property is set on an immutable <see cref="T:System.Drawing.Pen" />, such as those returned by the <see cref="T:System.Drawing.Pens" /> class.</exception>
		// Token: 0x170000FE RID: 254
		// (get) Token: 0x0600037D RID: 893 RVA: 0x00005304 File Offset: 0x00003504
		// (set) Token: 0x0600037E RID: 894 RVA: 0x00004644 File Offset: 0x00002844
		public float Width
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Creates an exact copy of this <see cref="T:System.Drawing.Pen" />.</summary>
		/// <returns>An <see cref="T:System.Object" /> that can be cast to a <see cref="T:System.Drawing.Pen" />.</returns>
		// Token: 0x0600037F RID: 895 RVA: 0x00004667 File Offset: 0x00002867
		public object Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Releases all resources used by this <see cref="T:System.Drawing.Pen" />.</summary>
		// Token: 0x06000380 RID: 896 RVA: 0x00004644 File Offset: 0x00002844
		public void Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Multiplies the transformation matrix for this <see cref="T:System.Drawing.Pen" /> by the specified <see cref="T:System.Drawing.Drawing2D.Matrix" />.</summary>
		/// <param name="matrix">The <see cref="T:System.Drawing.Drawing2D.Matrix" /> object by which to multiply the transformation matrix. </param>
		// Token: 0x06000381 RID: 897 RVA: 0x00004644 File Offset: 0x00002844
		public void MultiplyTransform(Matrix matrix)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Multiplies the transformation matrix for this <see cref="T:System.Drawing.Pen" /> by the specified <see cref="T:System.Drawing.Drawing2D.Matrix" /> in the specified order.</summary>
		/// <param name="matrix">The <see cref="T:System.Drawing.Drawing2D.Matrix" /> by which to multiply the transformation matrix. </param>
		/// <param name="order">The order in which to perform the multiplication operation. </param>
		// Token: 0x06000382 RID: 898 RVA: 0x00004644 File Offset: 0x00002844
		public void MultiplyTransform(Matrix matrix, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Resets the geometric transformation matrix for this <see cref="T:System.Drawing.Pen" /> to identity.</summary>
		// Token: 0x06000383 RID: 899 RVA: 0x00004644 File Offset: 0x00002844
		public void ResetTransform()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Rotates the local geometric transformation by the specified angle. This method prepends the rotation to the transformation.</summary>
		/// <param name="angle">The angle of rotation. </param>
		// Token: 0x06000384 RID: 900 RVA: 0x00004644 File Offset: 0x00002844
		public void RotateTransform(float angle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Rotates the local geometric transformation by the specified angle in the specified order.</summary>
		/// <param name="angle">The angle of rotation. </param>
		/// <param name="order">A <see cref="T:System.Drawing.Drawing2D.MatrixOrder" /> that specifies whether to append or prepend the rotation matrix. </param>
		// Token: 0x06000385 RID: 901 RVA: 0x00004644 File Offset: 0x00002844
		public void RotateTransform(float angle, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Scales the local geometric transformation by the specified factors. This method prepends the scaling matrix to the transformation.</summary>
		/// <param name="sx">The factor by which to scale the transformation in the x-axis direction. </param>
		/// <param name="sy">The factor by which to scale the transformation in the y-axis direction. </param>
		// Token: 0x06000386 RID: 902 RVA: 0x00004644 File Offset: 0x00002844
		public void ScaleTransform(float sx, float sy)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Scales the local geometric transformation by the specified factors in the specified order.</summary>
		/// <param name="sx">The factor by which to scale the transformation in the x-axis direction. </param>
		/// <param name="sy">The factor by which to scale the transformation in the y-axis direction. </param>
		/// <param name="order">A <see cref="T:System.Drawing.Drawing2D.MatrixOrder" /> that specifies whether to append or prepend the scaling matrix. </param>
		// Token: 0x06000387 RID: 903 RVA: 0x00004644 File Offset: 0x00002844
		public void ScaleTransform(float sx, float sy, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the values that determine the style of cap used to end lines drawn by this <see cref="T:System.Drawing.Pen" />.</summary>
		/// <param name="startCap">A <see cref="T:System.Drawing.Drawing2D.LineCap" /> that represents the cap style to use at the beginning of lines drawn with this <see cref="T:System.Drawing.Pen" />. </param>
		/// <param name="endCap">A <see cref="T:System.Drawing.Drawing2D.LineCap" /> that represents the cap style to use at the end of lines drawn with this <see cref="T:System.Drawing.Pen" />. </param>
		/// <param name="dashCap">A <see cref="T:System.Drawing.Drawing2D.LineCap" /> that represents the cap style to use at the beginning or end of dashed lines drawn with this <see cref="T:System.Drawing.Pen" />. </param>
		// Token: 0x06000388 RID: 904 RVA: 0x00004644 File Offset: 0x00002844
		public void SetLineCap(LineCap startCap, LineCap endCap, DashCap dashCap)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Translates the local geometric transformation by the specified dimensions. This method prepends the translation to the transformation.</summary>
		/// <param name="dx">The value of the translation in x. </param>
		/// <param name="dy">The value of the translation in y. </param>
		// Token: 0x06000389 RID: 905 RVA: 0x00004644 File Offset: 0x00002844
		public void TranslateTransform(float dx, float dy)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Translates the local geometric transformation by the specified dimensions in the specified order.</summary>
		/// <param name="dx">The value of the translation in x. </param>
		/// <param name="dy">The value of the translation in y. </param>
		/// <param name="order">The order (prepend or append) in which to apply the translation. </param>
		// Token: 0x0600038A RID: 906 RVA: 0x00004644 File Offset: 0x00002844
		public void TranslateTransform(float dx, float dy, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
