﻿using System;

namespace System.Drawing
{
	/// <summary>Specifies how much an image is rotated and the axis used to flip the image.</summary>
	// Token: 0x0200004B RID: 75
	public enum RotateFlipType
	{
		/// <summary>Specifies a 180-degree clockwise rotation without flipping.</summary>
		// Token: 0x040002C7 RID: 711
		Rotate180FlipNone = 2,
		/// <summary>Specifies a 180-degree clockwise rotation followed by a horizontal flip.</summary>
		// Token: 0x040002C8 RID: 712
		Rotate180FlipX = 6,
		/// <summary>Specifies a 180-degree clockwise rotation followed by a horizontal and vertical flip.</summary>
		// Token: 0x040002C9 RID: 713
		Rotate180FlipXY = 0,
		/// <summary>Specifies a 180-degree clockwise rotation followed by a vertical flip.</summary>
		// Token: 0x040002CA RID: 714
		Rotate180FlipY = 4,
		/// <summary>Specifies a 270-degree clockwise rotation without flipping.</summary>
		// Token: 0x040002CB RID: 715
		Rotate270FlipNone = 3,
		/// <summary>Specifies a 270-degree clockwise rotation followed by a horizontal flip.</summary>
		// Token: 0x040002CC RID: 716
		Rotate270FlipX = 7,
		/// <summary>Specifies a 270-degree clockwise rotation followed by a horizontal and vertical flip.</summary>
		// Token: 0x040002CD RID: 717
		Rotate270FlipXY = 1,
		/// <summary>Specifies a 270-degree clockwise rotation followed by a vertical flip.</summary>
		// Token: 0x040002CE RID: 718
		Rotate270FlipY = 5,
		/// <summary>Specifies a 90-degree clockwise rotation without flipping.</summary>
		// Token: 0x040002CF RID: 719
		Rotate90FlipNone = 1,
		/// <summary>Specifies a 90-degree clockwise rotation followed by a horizontal flip.</summary>
		// Token: 0x040002D0 RID: 720
		Rotate90FlipX = 5,
		/// <summary>Specifies a 90-degree clockwise rotation followed by a horizontal and vertical flip.</summary>
		// Token: 0x040002D1 RID: 721
		Rotate90FlipXY = 3,
		/// <summary>Specifies a 90-degree clockwise rotation followed by a vertical flip.</summary>
		// Token: 0x040002D2 RID: 722
		Rotate90FlipY = 7,
		/// <summary>Specifies no clockwise rotation and no flipping.</summary>
		// Token: 0x040002D3 RID: 723
		RotateNoneFlipNone = 0,
		/// <summary>Specifies no clockwise rotation followed by a horizontal flip.</summary>
		// Token: 0x040002D4 RID: 724
		RotateNoneFlipX = 4,
		/// <summary>Specifies no clockwise rotation followed by a horizontal and vertical flip.</summary>
		// Token: 0x040002D5 RID: 725
		RotateNoneFlipXY = 2,
		/// <summary>Specifies no clockwise rotation followed by a vertical flip.</summary>
		// Token: 0x040002D6 RID: 726
		RotateNoneFlipY = 6
	}
}
