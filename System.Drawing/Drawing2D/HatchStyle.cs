﻿using System;

namespace System.Drawing.Drawing2D
{
	/// <summary>Specifies the different patterns available for <see cref="T:System.Drawing.Drawing2D.HatchBrush" /> objects.</summary>
	// Token: 0x020000B8 RID: 184
	public enum HatchStyle
	{
		/// <summary>A pattern of lines on a diagonal from upper right to lower left.</summary>
		// Token: 0x04000408 RID: 1032
		BackwardDiagonal = 3,
		/// <summary>Specifies horizontal and vertical lines that cross.</summary>
		// Token: 0x04000409 RID: 1033
		Cross,
		/// <summary>Specifies diagonal lines that slant to the right from top points to bottom points, are spaced 50 percent closer together than, and are twice the width of <see cref="F:System.Drawing.Drawing2D.HatchStyle.ForwardDiagonal" />. This hatch pattern is not antialiased.</summary>
		// Token: 0x0400040A RID: 1034
		DarkDownwardDiagonal = 20,
		/// <summary>Specifies horizontal lines that are spaced 50 percent closer together than <see cref="F:System.Drawing.Drawing2D.HatchStyle.Horizontal" /> and are twice the width of <see cref="F:System.Drawing.Drawing2D.HatchStyle.Horizontal" />.</summary>
		// Token: 0x0400040B RID: 1035
		DarkHorizontal = 29,
		/// <summary>Specifies diagonal lines that slant to the left from top points to bottom points, are spaced 50 percent closer together than <see cref="F:System.Drawing.Drawing2D.HatchStyle.BackwardDiagonal" />, and are twice its width, but the lines are not antialiased.</summary>
		// Token: 0x0400040C RID: 1036
		DarkUpwardDiagonal = 21,
		/// <summary>Specifies vertical lines that are spaced 50 percent closer together than <see cref="F:System.Drawing.Drawing2D.HatchStyle.Vertical" /> and are twice its width.</summary>
		// Token: 0x0400040D RID: 1037
		DarkVertical = 28,
		/// <summary>Specifies dashed diagonal lines, that slant to the right from top points to bottom points.</summary>
		// Token: 0x0400040E RID: 1038
		DashedDownwardDiagonal = 30,
		/// <summary>Specifies dashed horizontal lines.</summary>
		// Token: 0x0400040F RID: 1039
		DashedHorizontal = 32,
		/// <summary>Specifies dashed diagonal lines, that slant to the left from top points to bottom points.</summary>
		// Token: 0x04000410 RID: 1040
		DashedUpwardDiagonal = 31,
		/// <summary>Specifies dashed vertical lines.</summary>
		// Token: 0x04000411 RID: 1041
		DashedVertical = 33,
		/// <summary>Specifies a hatch that has the appearance of layered bricks that slant to the left from top points to bottom points.</summary>
		// Token: 0x04000412 RID: 1042
		DiagonalBrick = 38,
		/// <summary>A pattern of crisscross diagonal lines.</summary>
		// Token: 0x04000413 RID: 1043
		DiagonalCross = 5,
		/// <summary>Specifies a hatch that has the appearance of divots.</summary>
		// Token: 0x04000414 RID: 1044
		Divot = 42,
		/// <summary>Specifies forward diagonal and backward diagonal lines, each of which is composed of dots, that cross.</summary>
		// Token: 0x04000415 RID: 1045
		DottedDiamond = 44,
		/// <summary>Specifies horizontal and vertical lines, each of which is composed of dots, that cross.</summary>
		// Token: 0x04000416 RID: 1046
		DottedGrid = 43,
		/// <summary>A pattern of lines on a diagonal from upper left to lower right.</summary>
		// Token: 0x04000417 RID: 1047
		ForwardDiagonal = 2,
		/// <summary>A pattern of horizontal lines.</summary>
		// Token: 0x04000418 RID: 1048
		Horizontal = 0,
		/// <summary>Specifies a hatch that has the appearance of horizontally layered bricks.</summary>
		// Token: 0x04000419 RID: 1049
		HorizontalBrick = 39,
		/// <summary>Specifies a hatch that has the appearance of a checkerboard with squares that are twice the size of <see cref="F:System.Drawing.Drawing2D.HatchStyle.SmallCheckerBoard" />.</summary>
		// Token: 0x0400041A RID: 1050
		LargeCheckerBoard = 50,
		/// <summary>Specifies a hatch that has the appearance of confetti, and is composed of larger pieces than <see cref="F:System.Drawing.Drawing2D.HatchStyle.SmallConfetti" />.</summary>
		// Token: 0x0400041B RID: 1051
		LargeConfetti = 35,
		/// <summary>Specifies the hatch style <see cref="F:System.Drawing.Drawing2D.HatchStyle.Cross" />.</summary>
		// Token: 0x0400041C RID: 1052
		LargeGrid = 4,
		/// <summary>Specifies diagonal lines that slant to the right from top points to bottom points and are spaced 50 percent closer together than <see cref="F:System.Drawing.Drawing2D.HatchStyle.ForwardDiagonal" />, but are not antialiased.</summary>
		// Token: 0x0400041D RID: 1053
		LightDownwardDiagonal = 18,
		/// <summary>Specifies horizontal lines that are spaced 50 percent closer together than <see cref="F:System.Drawing.Drawing2D.HatchStyle.Horizontal" />.</summary>
		// Token: 0x0400041E RID: 1054
		LightHorizontal = 25,
		/// <summary>Specifies diagonal lines that slant to the left from top points to bottom points and are spaced 50 percent closer together than <see cref="F:System.Drawing.Drawing2D.HatchStyle.BackwardDiagonal" />, but they are not antialiased.</summary>
		// Token: 0x0400041F RID: 1055
		LightUpwardDiagonal = 19,
		/// <summary>Specifies vertical lines that are spaced 50 percent closer together than <see cref="F:System.Drawing.Drawing2D.HatchStyle.Vertical" />.</summary>
		// Token: 0x04000420 RID: 1056
		LightVertical = 24,
		/// <summary>Specifies hatch style <see cref="F:System.Drawing.Drawing2D.HatchStyle.SolidDiamond" />.</summary>
		// Token: 0x04000421 RID: 1057
		Max = 4,
		/// <summary>Specifies hatch style <see cref="F:System.Drawing.Drawing2D.HatchStyle.Horizontal" />.</summary>
		// Token: 0x04000422 RID: 1058
		Min = 0,
		/// <summary>Specifies horizontal lines that are spaced 75 percent closer together than hatch style <see cref="F:System.Drawing.Drawing2D.HatchStyle.Horizontal" /> (or 25 percent closer together than <see cref="F:System.Drawing.Drawing2D.HatchStyle.LightHorizontal" />).</summary>
		// Token: 0x04000423 RID: 1059
		NarrowHorizontal = 27,
		/// <summary>Specifies vertical lines that are spaced 75 percent closer together than hatch style <see cref="F:System.Drawing.Drawing2D.HatchStyle.Vertical" /> (or 25 percent closer together than <see cref="F:System.Drawing.Drawing2D.HatchStyle.LightVertical" />).</summary>
		// Token: 0x04000424 RID: 1060
		NarrowVertical = 26,
		/// <summary>Specifies forward diagonal and backward diagonal lines that cross but are not antialiased.</summary>
		// Token: 0x04000425 RID: 1061
		OutlinedDiamond = 51,
		/// <summary>Specifies a 5-percent hatch. The ratio of foreground color to background color is 5:95.</summary>
		// Token: 0x04000426 RID: 1062
		Percent05 = 6,
		/// <summary>Specifies a 10-percent hatch. The ratio of foreground color to background color is 10:90.</summary>
		// Token: 0x04000427 RID: 1063
		Percent10,
		/// <summary>Specifies a 20-percent hatch. The ratio of foreground color to background color is 20:80.</summary>
		// Token: 0x04000428 RID: 1064
		Percent20,
		/// <summary>Specifies a 25-percent hatch. The ratio of foreground color to background color is 25:75.</summary>
		// Token: 0x04000429 RID: 1065
		Percent25,
		/// <summary>Specifies a 30-percent hatch. The ratio of foreground color to background color is 30:70.</summary>
		// Token: 0x0400042A RID: 1066
		Percent30,
		/// <summary>Specifies a 40-percent hatch. The ratio of foreground color to background color is 40:60.</summary>
		// Token: 0x0400042B RID: 1067
		Percent40,
		/// <summary>Specifies a 50-percent hatch. The ratio of foreground color to background color is 50:50.</summary>
		// Token: 0x0400042C RID: 1068
		Percent50,
		/// <summary>Specifies a 60-percent hatch. The ratio of foreground color to background color is 60:40.</summary>
		// Token: 0x0400042D RID: 1069
		Percent60,
		/// <summary>Specifies a 70-percent hatch. The ratio of foreground color to background color is 70:30.</summary>
		// Token: 0x0400042E RID: 1070
		Percent70,
		/// <summary>Specifies a 75-percent hatch. The ratio of foreground color to background color is 75:25.</summary>
		// Token: 0x0400042F RID: 1071
		Percent75,
		/// <summary>Specifies a 80-percent hatch. The ratio of foreground color to background color is 80:100.</summary>
		// Token: 0x04000430 RID: 1072
		Percent80,
		/// <summary>Specifies a 90-percent hatch. The ratio of foreground color to background color is 90:10.</summary>
		// Token: 0x04000431 RID: 1073
		Percent90,
		/// <summary>Specifies a hatch that has the appearance of a plaid material.</summary>
		// Token: 0x04000432 RID: 1074
		Plaid = 41,
		/// <summary>Specifies a hatch that has the appearance of diagonally layered shingles that slant to the right from top points to bottom points.</summary>
		// Token: 0x04000433 RID: 1075
		Shingle = 45,
		/// <summary>Specifies a hatch that has the appearance of a checkerboard.</summary>
		// Token: 0x04000434 RID: 1076
		SmallCheckerBoard = 49,
		/// <summary>Specifies a hatch that has the appearance of confetti.</summary>
		// Token: 0x04000435 RID: 1077
		SmallConfetti = 34,
		/// <summary>Specifies horizontal and vertical lines that cross and are spaced 50 percent closer together than hatch style <see cref="F:System.Drawing.Drawing2D.HatchStyle.Cross" />.</summary>
		// Token: 0x04000436 RID: 1078
		SmallGrid = 48,
		/// <summary>Specifies a hatch that has the appearance of a checkerboard placed diagonally.</summary>
		// Token: 0x04000437 RID: 1079
		SolidDiamond = 52,
		/// <summary>Specifies a hatch that has the appearance of spheres laid adjacent to one another.</summary>
		// Token: 0x04000438 RID: 1080
		Sphere = 47,
		/// <summary>Specifies a hatch that has the appearance of a trellis.</summary>
		// Token: 0x04000439 RID: 1081
		Trellis = 46,
		/// <summary>A pattern of vertical lines.</summary>
		// Token: 0x0400043A RID: 1082
		Vertical = 1,
		/// <summary>Specifies horizontal lines that are composed of tildes.</summary>
		// Token: 0x0400043B RID: 1083
		Wave = 37,
		/// <summary>Specifies a hatch that has the appearance of a woven material.</summary>
		// Token: 0x0400043C RID: 1084
		Weave = 40,
		/// <summary>Specifies diagonal lines that slant to the right from top points to bottom points, have the same spacing as hatch style <see cref="F:System.Drawing.Drawing2D.HatchStyle.ForwardDiagonal" />, and are triple its width, but are not antialiased.</summary>
		// Token: 0x0400043D RID: 1085
		WideDownwardDiagonal = 22,
		/// <summary>Specifies diagonal lines that slant to the left from top points to bottom points, have the same spacing as hatch style <see cref="F:System.Drawing.Drawing2D.HatchStyle.BackwardDiagonal" />, and are triple its width, but are not antialiased.</summary>
		// Token: 0x0400043E RID: 1086
		WideUpwardDiagonal,
		/// <summary>Specifies horizontal lines that are composed of zigzags.</summary>
		// Token: 0x0400043F RID: 1087
		ZigZag = 36
	}
}
