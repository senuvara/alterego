﻿using System;
using Unity;

namespace System.Drawing.Drawing2D
{
	/// <summary>Defines a blend pattern for a <see cref="T:System.Drawing.Drawing2D.LinearGradientBrush" /> object. This class cannot be inherited.</summary>
	// Token: 0x020000B4 RID: 180
	public sealed class Blend
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.Blend" /> class.</summary>
		// Token: 0x060008BA RID: 2234 RVA: 0x00004644 File Offset: 0x00002844
		public Blend()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.Blend" /> class with the specified number of factors and positions.</summary>
		/// <param name="count">The number of elements in the <see cref="P:System.Drawing.Drawing2D.Blend.Factors" /> and <see cref="P:System.Drawing.Drawing2D.Blend.Positions" /> arrays. </param>
		// Token: 0x060008BB RID: 2235 RVA: 0x00004644 File Offset: 0x00002844
		public Blend(int count)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets an array of blend factors for the gradient.</summary>
		/// <returns>An array of blend factors that specify the percentages of the starting color and the ending color to be used at the corresponding position.</returns>
		// Token: 0x17000383 RID: 899
		// (get) Token: 0x060008BC RID: 2236 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x060008BD RID: 2237 RVA: 0x00004644 File Offset: 0x00002844
		public float[] Factors
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets an array of blend positions for the gradient.</summary>
		/// <returns>An array of blend positions that specify the percentages of distance along the gradient line.</returns>
		// Token: 0x17000384 RID: 900
		// (get) Token: 0x060008BE RID: 2238 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x060008BF RID: 2239 RVA: 0x00004644 File Offset: 0x00002844
		public float[] Positions
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
