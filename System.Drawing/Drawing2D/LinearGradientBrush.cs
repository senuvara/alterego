﻿using System;
using Unity;

namespace System.Drawing.Drawing2D
{
	/// <summary>Encapsulates a <see cref="T:System.Drawing.Brush" /> with a linear gradient. This class cannot be inherited.</summary>
	// Token: 0x020000B9 RID: 185
	public sealed class LinearGradientBrush : Brush
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.LinearGradientBrush" /> class with the specified points and colors.</summary>
		/// <param name="point1">A <see cref="T:System.Drawing.Point" /> structure that represents the starting point of the linear gradient. </param>
		/// <param name="point2">A <see cref="T:System.Drawing.Point" /> structure that represents the endpoint of the linear gradient. </param>
		/// <param name="color1">A <see cref="T:System.Drawing.Color" /> structure that represents the starting color of the linear gradient. </param>
		/// <param name="color2">A <see cref="T:System.Drawing.Color" /> structure that represents the ending color of the linear gradient. </param>
		// Token: 0x060008D9 RID: 2265 RVA: 0x00004644 File Offset: 0x00002844
		public LinearGradientBrush(Point point1, Point point2, Color color1, Color color2)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.LinearGradientBrush" /> class with the specified points and colors.</summary>
		/// <param name="point1">A <see cref="T:System.Drawing.PointF" /> structure that represents the starting point of the linear gradient. </param>
		/// <param name="point2">A <see cref="T:System.Drawing.PointF" /> structure that represents the endpoint of the linear gradient. </param>
		/// <param name="color1">A <see cref="T:System.Drawing.Color" /> structure that represents the starting color of the linear gradient. </param>
		/// <param name="color2">A <see cref="T:System.Drawing.Color" /> structure that represents the ending color of the linear gradient. </param>
		// Token: 0x060008DA RID: 2266 RVA: 0x00004644 File Offset: 0x00002844
		public LinearGradientBrush(PointF point1, PointF point2, Color color1, Color color2)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Drawing.Drawing2D.LinearGradientBrush" /> class based on a rectangle, starting and ending colors, and orientation.</summary>
		/// <param name="rect">A <see cref="T:System.Drawing.Rectangle" /> structure that specifies the bounds of the linear gradient. </param>
		/// <param name="color1">A <see cref="T:System.Drawing.Color" /> structure that represents the starting color for the gradient. </param>
		/// <param name="color2">A <see cref="T:System.Drawing.Color" /> structure that represents the ending color for the gradient. </param>
		/// <param name="linearGradientMode">A <see cref="T:System.Drawing.Drawing2D.LinearGradientMode" /> enumeration element that specifies the orientation of the gradient. The orientation determines the starting and ending points of the gradient. For example, <see langword="LinearGradientMode.ForwardDiagonal" /> specifies that the starting point is the upper-left corner of the rectangle and the ending point is the lower-right corner of the rectangle. </param>
		// Token: 0x060008DB RID: 2267 RVA: 0x00004644 File Offset: 0x00002844
		public LinearGradientBrush(Rectangle rect, Color color1, Color color2, LinearGradientMode linearGradientMode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Drawing.Drawing2D.LinearGradientBrush" /> class based on a rectangle, starting and ending colors, and an orientation angle.</summary>
		/// <param name="rect">A <see cref="T:System.Drawing.Rectangle" /> structure that specifies the bounds of the linear gradient. </param>
		/// <param name="color1">A <see cref="T:System.Drawing.Color" /> structure that represents the starting color for the gradient. </param>
		/// <param name="color2">A <see cref="T:System.Drawing.Color" /> structure that represents the ending color for the gradient. </param>
		/// <param name="angle">The angle, measured in degrees clockwise from the x-axis, of the gradient's orientation line. </param>
		// Token: 0x060008DC RID: 2268 RVA: 0x00004644 File Offset: 0x00002844
		public LinearGradientBrush(Rectangle rect, Color color1, Color color2, float angle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Drawing.Drawing2D.LinearGradientBrush" /> class based on a rectangle, starting and ending colors, and an orientation angle.</summary>
		/// <param name="rect">A <see cref="T:System.Drawing.Rectangle" /> structure that specifies the bounds of the linear gradient. </param>
		/// <param name="color1">A <see cref="T:System.Drawing.Color" /> structure that represents the starting color for the gradient. </param>
		/// <param name="color2">A <see cref="T:System.Drawing.Color" /> structure that represents the ending color for the gradient. </param>
		/// <param name="angle">The angle, measured in degrees clockwise from the x-axis, of the gradient's orientation line. </param>
		/// <param name="isAngleScaleable">Set to <see langword="true" /> to specify that the angle is affected by the transform associated with this <see cref="T:System.Drawing.Drawing2D.LinearGradientBrush" />; otherwise, <see langword="false" />. </param>
		// Token: 0x060008DD RID: 2269 RVA: 0x00004644 File Offset: 0x00002844
		public LinearGradientBrush(Rectangle rect, Color color1, Color color2, float angle, bool isAngleScaleable)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Drawing.Drawing2D.LinearGradientBrush" /> based on a rectangle, starting and ending colors, and an orientation mode.</summary>
		/// <param name="rect">A <see cref="T:System.Drawing.RectangleF" /> structure that specifies the bounds of the linear gradient. </param>
		/// <param name="color1">A <see cref="T:System.Drawing.Color" /> structure that represents the starting color for the gradient. </param>
		/// <param name="color2">A <see cref="T:System.Drawing.Color" /> structure that represents the ending color for the gradient. </param>
		/// <param name="linearGradientMode">A <see cref="T:System.Drawing.Drawing2D.LinearGradientMode" /> enumeration element that specifies the orientation of the gradient. The orientation determines the starting and ending points of the gradient. For example, <see langword="LinearGradientMode.ForwardDiagonal" /> specifies that the starting point is the upper-left corner of the rectangle and the ending point is the lower-right corner of the rectangle. </param>
		// Token: 0x060008DE RID: 2270 RVA: 0x00004644 File Offset: 0x00002844
		public LinearGradientBrush(RectangleF rect, Color color1, Color color2, LinearGradientMode linearGradientMode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Drawing.Drawing2D.LinearGradientBrush" /> class based on a rectangle, starting and ending colors, and an orientation angle.</summary>
		/// <param name="rect">A <see cref="T:System.Drawing.RectangleF" /> structure that specifies the bounds of the linear gradient. </param>
		/// <param name="color1">A <see cref="T:System.Drawing.Color" /> structure that represents the starting color for the gradient. </param>
		/// <param name="color2">A <see cref="T:System.Drawing.Color" /> structure that represents the ending color for the gradient. </param>
		/// <param name="angle">The angle, measured in degrees clockwise from the x-axis, of the gradient's orientation line. </param>
		// Token: 0x060008DF RID: 2271 RVA: 0x00004644 File Offset: 0x00002844
		public LinearGradientBrush(RectangleF rect, Color color1, Color color2, float angle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Drawing.Drawing2D.LinearGradientBrush" /> class based on a rectangle, starting and ending colors, and an orientation angle.</summary>
		/// <param name="rect">A <see cref="T:System.Drawing.RectangleF" /> structure that specifies the bounds of the linear gradient. </param>
		/// <param name="color1">A <see cref="T:System.Drawing.Color" /> structure that represents the starting color for the gradient. </param>
		/// <param name="color2">A <see cref="T:System.Drawing.Color" /> structure that represents the ending color for the gradient. </param>
		/// <param name="angle">The angle, measured in degrees clockwise from the x-axis, of the gradient's orientation line. </param>
		/// <param name="isAngleScaleable">Set to <see langword="true" /> to specify that the angle is affected by the transform associated with this <see cref="T:System.Drawing.Drawing2D.LinearGradientBrush" />; otherwise, <see langword="false" />. </param>
		// Token: 0x060008E0 RID: 2272 RVA: 0x00004644 File Offset: 0x00002844
		public LinearGradientBrush(RectangleF rect, Color color1, Color color2, float angle, bool isAngleScaleable)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets a <see cref="T:System.Drawing.Drawing2D.Blend" /> that specifies positions and factors that define a custom falloff for the gradient.</summary>
		/// <returns>A <see cref="T:System.Drawing.Drawing2D.Blend" /> that represents a custom falloff for the gradient.</returns>
		// Token: 0x1700038C RID: 908
		// (get) Token: 0x060008E1 RID: 2273 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x060008E2 RID: 2274 RVA: 0x00004644 File Offset: 0x00002844
		public Blend Blend
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether gamma correction is enabled for this <see cref="T:System.Drawing.Drawing2D.LinearGradientBrush" />.</summary>
		/// <returns>The value is <see langword="true" /> if gamma correction is enabled for this <see cref="T:System.Drawing.Drawing2D.LinearGradientBrush" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700038D RID: 909
		// (get) Token: 0x060008E3 RID: 2275 RVA: 0x0000708C File Offset: 0x0000528C
		// (set) Token: 0x060008E4 RID: 2276 RVA: 0x00004644 File Offset: 0x00002844
		public bool GammaCorrection
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Drawing.Drawing2D.ColorBlend" /> that defines a multicolor linear gradient.</summary>
		/// <returns>A <see cref="T:System.Drawing.Drawing2D.ColorBlend" /> that defines a multicolor linear gradient.</returns>
		// Token: 0x1700038E RID: 910
		// (get) Token: 0x060008E5 RID: 2277 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x060008E6 RID: 2278 RVA: 0x00004644 File Offset: 0x00002844
		public ColorBlend InterpolationColors
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the starting and ending colors of the gradient.</summary>
		/// <returns>An array of two <see cref="T:System.Drawing.Color" /> structures that represents the starting and ending colors of the gradient.</returns>
		// Token: 0x1700038F RID: 911
		// (get) Token: 0x060008E7 RID: 2279 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x060008E8 RID: 2280 RVA: 0x00004644 File Offset: 0x00002844
		public Color[] LinearColors
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a rectangular region that defines the starting and ending points of the gradient.</summary>
		/// <returns>A <see cref="T:System.Drawing.RectangleF" /> structure that specifies the starting and ending points of the gradient.</returns>
		// Token: 0x17000390 RID: 912
		// (get) Token: 0x060008E9 RID: 2281 RVA: 0x000070A8 File Offset: 0x000052A8
		public RectangleF Rectangle
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(RectangleF);
			}
		}

		/// <summary>Gets or sets a copy <see cref="T:System.Drawing.Drawing2D.Matrix" /> that defines a local geometric transform for this <see cref="T:System.Drawing.Drawing2D.LinearGradientBrush" />.</summary>
		/// <returns>A copy of the <see cref="T:System.Drawing.Drawing2D.Matrix" /> that defines a geometric transform that applies only to fills drawn with this <see cref="T:System.Drawing.Drawing2D.LinearGradientBrush" />.</returns>
		// Token: 0x17000391 RID: 913
		// (get) Token: 0x060008EA RID: 2282 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x060008EB RID: 2283 RVA: 0x00004644 File Offset: 0x00002844
		public Matrix Transform
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Drawing.Drawing2D.WrapMode" /> enumeration that indicates the wrap mode for this <see cref="T:System.Drawing.Drawing2D.LinearGradientBrush" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.Drawing2D.WrapMode" /> that specifies how fills drawn with this <see cref="T:System.Drawing.Drawing2D.LinearGradientBrush" /> are tiled.</returns>
		// Token: 0x17000392 RID: 914
		// (get) Token: 0x060008EC RID: 2284 RVA: 0x000070C4 File Offset: 0x000052C4
		// (set) Token: 0x060008ED RID: 2285 RVA: 0x00004644 File Offset: 0x00002844
		public WrapMode WrapMode
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return WrapMode.Tile;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Creates an exact copy of this <see cref="T:System.Drawing.Drawing2D.LinearGradientBrush" />.</summary>
		/// <returns>The <see cref="T:System.Drawing.Drawing2D.LinearGradientBrush" /> this method creates, cast as an object.</returns>
		// Token: 0x060008EE RID: 2286 RVA: 0x00004667 File Offset: 0x00002867
		public override object Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Multiplies the <see cref="T:System.Drawing.Drawing2D.Matrix" /> that represents the local geometric transform of this <see cref="T:System.Drawing.Drawing2D.LinearGradientBrush" /> by the specified <see cref="T:System.Drawing.Drawing2D.Matrix" /> by prepending the specified <see cref="T:System.Drawing.Drawing2D.Matrix" />.</summary>
		/// <param name="matrix">The <see cref="T:System.Drawing.Drawing2D.Matrix" /> by which to multiply the geometric transform. </param>
		// Token: 0x060008EF RID: 2287 RVA: 0x00004644 File Offset: 0x00002844
		public void MultiplyTransform(Matrix matrix)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Multiplies the <see cref="T:System.Drawing.Drawing2D.Matrix" /> that represents the local geometric transform of this <see cref="T:System.Drawing.Drawing2D.LinearGradientBrush" /> by the specified <see cref="T:System.Drawing.Drawing2D.Matrix" /> in the specified order.</summary>
		/// <param name="matrix">The <see cref="T:System.Drawing.Drawing2D.Matrix" /> by which to multiply the geometric transform. </param>
		/// <param name="order">A <see cref="T:System.Drawing.Drawing2D.MatrixOrder" /> that specifies in which order to multiply the two matrices. </param>
		// Token: 0x060008F0 RID: 2288 RVA: 0x00004644 File Offset: 0x00002844
		public void MultiplyTransform(Matrix matrix, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Resets the <see cref="P:System.Drawing.Drawing2D.LinearGradientBrush.Transform" /> property to identity.</summary>
		// Token: 0x060008F1 RID: 2289 RVA: 0x00004644 File Offset: 0x00002844
		public void ResetTransform()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Rotates the local geometric transform by the specified amount. This method prepends the rotation to the transform.</summary>
		/// <param name="angle">The angle of rotation. </param>
		// Token: 0x060008F2 RID: 2290 RVA: 0x00004644 File Offset: 0x00002844
		public void RotateTransform(float angle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Rotates the local geometric transform by the specified amount in the specified order.</summary>
		/// <param name="angle">The angle of rotation. </param>
		/// <param name="order">A <see cref="T:System.Drawing.Drawing2D.MatrixOrder" /> that specifies whether to append or prepend the rotation matrix. </param>
		// Token: 0x060008F3 RID: 2291 RVA: 0x00004644 File Offset: 0x00002844
		public void RotateTransform(float angle, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Scales the local geometric transform by the specified amounts. This method prepends the scaling matrix to the transform.</summary>
		/// <param name="sx">The amount by which to scale the transform in the x-axis direction. </param>
		/// <param name="sy">The amount by which to scale the transform in the y-axis direction. </param>
		// Token: 0x060008F4 RID: 2292 RVA: 0x00004644 File Offset: 0x00002844
		public void ScaleTransform(float sx, float sy)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Scales the local geometric transform by the specified amounts in the specified order.</summary>
		/// <param name="sx">The amount by which to scale the transform in the x-axis direction. </param>
		/// <param name="sy">The amount by which to scale the transform in the y-axis direction. </param>
		/// <param name="order">A <see cref="T:System.Drawing.Drawing2D.MatrixOrder" /> that specifies whether to append or prepend the scaling matrix. </param>
		// Token: 0x060008F5 RID: 2293 RVA: 0x00004644 File Offset: 0x00002844
		public void ScaleTransform(float sx, float sy, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a linear gradient with a center color and a linear falloff to a single color on both ends.</summary>
		/// <param name="focus">A value from 0 through 1 that specifies the center of the gradient (the point where the gradient is composed of only the ending color). </param>
		// Token: 0x060008F6 RID: 2294 RVA: 0x00004644 File Offset: 0x00002844
		public void SetBlendTriangularShape(float focus)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a linear gradient with a center color and a linear falloff to a single color on both ends.</summary>
		/// <param name="focus">A value from 0 through 1 that specifies the center of the gradient (the point where the gradient is composed of only the ending color). </param>
		/// <param name="scale">A value from 0 through1 that specifies how fast the colors falloff from the starting color to <paramref name="focus" /> (ending color) </param>
		// Token: 0x060008F7 RID: 2295 RVA: 0x00004644 File Offset: 0x00002844
		public void SetBlendTriangularShape(float focus, float scale)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a gradient falloff based on a bell-shaped curve.</summary>
		/// <param name="focus">A value from 0 through 1 that specifies the center of the gradient (the point where the starting color and ending color are blended equally). </param>
		// Token: 0x060008F8 RID: 2296 RVA: 0x00004644 File Offset: 0x00002844
		public void SetSigmaBellShape(float focus)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a gradient falloff based on a bell-shaped curve.</summary>
		/// <param name="focus">A value from 0 through 1 that specifies the center of the gradient (the point where the gradient is composed of only the ending color). </param>
		/// <param name="scale">A value from 0 through 1 that specifies how fast the colors falloff from the <paramref name="focus" />. </param>
		// Token: 0x060008F9 RID: 2297 RVA: 0x00004644 File Offset: 0x00002844
		public void SetSigmaBellShape(float focus, float scale)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Translates the local geometric transform by the specified dimensions. This method prepends the translation to the transform.</summary>
		/// <param name="dx">The value of the translation in x. </param>
		/// <param name="dy">The value of the translation in y. </param>
		// Token: 0x060008FA RID: 2298 RVA: 0x00004644 File Offset: 0x00002844
		public void TranslateTransform(float dx, float dy)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Translates the local geometric transform by the specified dimensions in the specified order.</summary>
		/// <param name="dx">The value of the translation in x. </param>
		/// <param name="dy">The value of the translation in y. </param>
		/// <param name="order">The order (prepend or append) in which to apply the translation. </param>
		// Token: 0x060008FB RID: 2299 RVA: 0x00004644 File Offset: 0x00002844
		public void TranslateTransform(float dx, float dy, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
