﻿using System;

namespace System.Drawing.Drawing2D
{
	/// <summary>The <see cref="T:System.Drawing.Drawing2D.InterpolationMode" /> enumeration specifies the algorithm that is used when images are scaled or rotated. </summary>
	// Token: 0x02000036 RID: 54
	public enum InterpolationMode
	{
		/// <summary>Specifies bicubic interpolation. No prefiltering is done. This mode is not suitable for shrinking an image below 25 percent of its original size.</summary>
		// Token: 0x04000263 RID: 611
		Bicubic = 4,
		/// <summary>Specifies bilinear interpolation. No prefiltering is done. This mode is not suitable for shrinking an image below 50 percent of its original size. </summary>
		// Token: 0x04000264 RID: 612
		Bilinear = 3,
		/// <summary>Specifies default mode.</summary>
		// Token: 0x04000265 RID: 613
		Default = 0,
		/// <summary>Specifies high quality interpolation.</summary>
		// Token: 0x04000266 RID: 614
		High = 2,
		/// <summary>Specifies high-quality, bicubic interpolation. Prefiltering is performed to ensure high-quality shrinking. This mode produces the highest quality transformed images.</summary>
		// Token: 0x04000267 RID: 615
		HighQualityBicubic = 7,
		/// <summary>Specifies high-quality, bilinear interpolation. Prefiltering is performed to ensure high-quality shrinking. </summary>
		// Token: 0x04000268 RID: 616
		HighQualityBilinear = 6,
		/// <summary>Equivalent to the <see cref="F:System.Drawing.Drawing2D.QualityMode.Invalid" /> element of the <see cref="T:System.Drawing.Drawing2D.QualityMode" /> enumeration.</summary>
		// Token: 0x04000269 RID: 617
		Invalid = -1,
		/// <summary>Specifies low quality interpolation.</summary>
		// Token: 0x0400026A RID: 618
		Low = 1,
		/// <summary>Specifies nearest-neighbor interpolation.</summary>
		// Token: 0x0400026B RID: 619
		NearestNeighbor = 5
	}
}
