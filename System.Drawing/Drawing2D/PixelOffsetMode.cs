﻿using System;

namespace System.Drawing.Drawing2D
{
	/// <summary>Specifies how pixels are offset during rendering.</summary>
	// Token: 0x02000038 RID: 56
	public enum PixelOffsetMode
	{
		/// <summary>Specifies the default mode.</summary>
		// Token: 0x04000275 RID: 629
		Default,
		/// <summary>Specifies that pixels are offset by -.5 units, both horizontally and vertically, for high speed antialiasing.</summary>
		// Token: 0x04000276 RID: 630
		Half = 4,
		/// <summary>Specifies high quality, low speed rendering.</summary>
		// Token: 0x04000277 RID: 631
		HighQuality = 2,
		/// <summary>Specifies high speed, low quality rendering.</summary>
		// Token: 0x04000278 RID: 632
		HighSpeed = 1,
		/// <summary>Specifies an invalid mode.</summary>
		// Token: 0x04000279 RID: 633
		Invalid = -1,
		/// <summary>Specifies no pixel offset.</summary>
		// Token: 0x0400027A RID: 634
		None = 3
	}
}
