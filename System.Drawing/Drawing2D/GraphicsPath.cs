﻿using System;
using Unity;

namespace System.Drawing.Drawing2D
{
	/// <summary>Represents a series of connected lines and curves. This class cannot be inherited.</summary>
	// Token: 0x02000019 RID: 25
	public sealed class GraphicsPath : MarshalByRefObject, ICloneable, IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> class with a <see cref="P:System.Drawing.Drawing2D.GraphicsPath.FillMode" /> value of <see cref="F:System.Drawing.Drawing2D.FillMode.Alternate" />.</summary>
		// Token: 0x060002A4 RID: 676 RVA: 0x00004644 File Offset: 0x00002844
		public GraphicsPath()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> class with the specified <see cref="T:System.Drawing.Drawing2D.FillMode" /> enumeration.</summary>
		/// <param name="fillMode">The <see cref="T:System.Drawing.Drawing2D.FillMode" /> enumeration that determines how the interior of this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> is filled. </param>
		// Token: 0x060002A5 RID: 677 RVA: 0x00004644 File Offset: 0x00002844
		public GraphicsPath(FillMode fillMode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> array with the specified <see cref="T:System.Drawing.Drawing2D.PathPointType" /> and <see cref="T:System.Drawing.PointF" /> arrays.</summary>
		/// <param name="pts">An array of <see cref="T:System.Drawing.PointF" /> structures that defines the coordinates of the points that make up this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />. </param>
		/// <param name="types">An array of <see cref="T:System.Drawing.Drawing2D.PathPointType" /> enumeration elements that specifies the type of each corresponding point in the <paramref name="pts" /> array. </param>
		// Token: 0x060002A6 RID: 678 RVA: 0x00004644 File Offset: 0x00002844
		public GraphicsPath(PointF[] pts, byte[] types)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> array with the specified <see cref="T:System.Drawing.Drawing2D.PathPointType" /> and <see cref="T:System.Drawing.PointF" /> arrays and with the specified <see cref="T:System.Drawing.Drawing2D.FillMode" /> enumeration element.</summary>
		/// <param name="pts">An array of <see cref="T:System.Drawing.PointF" /> structures that defines the coordinates of the points that make up this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />. </param>
		/// <param name="types">An array of <see cref="T:System.Drawing.Drawing2D.PathPointType" /> enumeration elements that specifies the type of each corresponding point in the <paramref name="pts" /> array. </param>
		/// <param name="fillMode">A <see cref="T:System.Drawing.Drawing2D.FillMode" /> enumeration that specifies how the interiors of shapes in this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> are filled. </param>
		// Token: 0x060002A7 RID: 679 RVA: 0x00004644 File Offset: 0x00002844
		public GraphicsPath(PointF[] pts, byte[] types, FillMode fillMode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> class with the specified <see cref="T:System.Drawing.Drawing2D.PathPointType" /> and <see cref="T:System.Drawing.Point" /> arrays.</summary>
		/// <param name="pts">An array of <see cref="T:System.Drawing.Point" /> structures that defines the coordinates of the points that make up this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />. </param>
		/// <param name="types">An array of <see cref="T:System.Drawing.Drawing2D.PathPointType" /> enumeration elements that specifies the type of each corresponding point in the <paramref name="pts" /> array. </param>
		// Token: 0x060002A8 RID: 680 RVA: 0x00004644 File Offset: 0x00002844
		public GraphicsPath(Point[] pts, byte[] types)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> class with the specified <see cref="T:System.Drawing.Drawing2D.PathPointType" /> and <see cref="T:System.Drawing.Point" /> arrays and with the specified <see cref="T:System.Drawing.Drawing2D.FillMode" /> enumeration element.</summary>
		/// <param name="pts">An array of <see cref="T:System.Drawing.Point" /> structures that defines the coordinates of the points that make up this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />. </param>
		/// <param name="types">An array of <see cref="T:System.Drawing.Drawing2D.PathPointType" /> enumeration elements that specifies the type of each corresponding point in the <paramref name="pts" /> array. </param>
		/// <param name="fillMode">A <see cref="T:System.Drawing.Drawing2D.FillMode" /> enumeration that specifies how the interiors of shapes in this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> are filled. </param>
		// Token: 0x060002A9 RID: 681 RVA: 0x00004644 File Offset: 0x00002844
		public GraphicsPath(Point[] pts, byte[] types, FillMode fillMode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets a <see cref="T:System.Drawing.Drawing2D.FillMode" /> enumeration that determines how the interiors of shapes in this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> are filled.</summary>
		/// <returns>A <see cref="T:System.Drawing.Drawing2D.FillMode" /> enumeration that specifies how the interiors of shapes in this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> are filled.</returns>
		// Token: 0x170000D1 RID: 209
		// (get) Token: 0x060002AA RID: 682 RVA: 0x00004D54 File Offset: 0x00002F54
		// (set) Token: 0x060002AB RID: 683 RVA: 0x00004644 File Offset: 0x00002844
		public FillMode FillMode
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return FillMode.Alternate;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Drawing2D.PathData" /> that encapsulates arrays of points (<paramref name="points" />) and types (<paramref name="types" />) for this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.Drawing2D.PathData" /> that encapsulates arrays for both the points and types for this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</returns>
		// Token: 0x170000D2 RID: 210
		// (get) Token: 0x060002AC RID: 684 RVA: 0x00004667 File Offset: 0x00002867
		public PathData PathData
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the points in the path.</summary>
		/// <returns>An array of <see cref="T:System.Drawing.PointF" /> objects that represent the path.</returns>
		// Token: 0x170000D3 RID: 211
		// (get) Token: 0x060002AD RID: 685 RVA: 0x00004667 File Offset: 0x00002867
		public PointF[] PathPoints
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the types of the corresponding points in the <see cref="P:System.Drawing.Drawing2D.GraphicsPath.PathPoints" /> array.</summary>
		/// <returns>An array of bytes that specifies the types of the corresponding points in the path.</returns>
		// Token: 0x170000D4 RID: 212
		// (get) Token: 0x060002AE RID: 686 RVA: 0x00004667 File Offset: 0x00002867
		public byte[] PathTypes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the number of elements in the <see cref="P:System.Drawing.Drawing2D.GraphicsPath.PathPoints" /> or the <see cref="P:System.Drawing.Drawing2D.GraphicsPath.PathTypes" /> array.</summary>
		/// <returns>An integer that specifies the number of elements in the <see cref="P:System.Drawing.Drawing2D.GraphicsPath.PathPoints" /> or the <see cref="P:System.Drawing.Drawing2D.GraphicsPath.PathTypes" /> array.</returns>
		// Token: 0x170000D5 RID: 213
		// (get) Token: 0x060002AF RID: 687 RVA: 0x00004D70 File Offset: 0x00002F70
		public int PointCount
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Appends an elliptical arc to the current figure.</summary>
		/// <param name="rect">A <see cref="T:System.Drawing.Rectangle" /> that represents the rectangular bounds of the ellipse from which the arc is taken. </param>
		/// <param name="startAngle">The starting angle of the arc, measured in degrees clockwise from the x-axis. </param>
		/// <param name="sweepAngle">The angle between <paramref name="startAngle" /> and the end of the arc. </param>
		// Token: 0x060002B0 RID: 688 RVA: 0x00004644 File Offset: 0x00002844
		public void AddArc(Rectangle rect, float startAngle, float sweepAngle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Appends an elliptical arc to the current figure.</summary>
		/// <param name="rect">A <see cref="T:System.Drawing.RectangleF" /> that represents the rectangular bounds of the ellipse from which the arc is taken. </param>
		/// <param name="startAngle">The starting angle of the arc, measured in degrees clockwise from the x-axis. </param>
		/// <param name="sweepAngle">The angle between <paramref name="startAngle" /> and the end of the arc. </param>
		// Token: 0x060002B1 RID: 689 RVA: 0x00004644 File Offset: 0x00002844
		public void AddArc(RectangleF rect, float startAngle, float sweepAngle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Appends an elliptical arc to the current figure.</summary>
		/// <param name="x">The x-coordinate of the upper-left corner of the rectangular region that defines the ellipse from which the arc is drawn. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the rectangular region that defines the ellipse from which the arc is drawn. </param>
		/// <param name="width">The width of the rectangular region that defines the ellipse from which the arc is drawn. </param>
		/// <param name="height">The height of the rectangular region that defines the ellipse from which the arc is drawn. </param>
		/// <param name="startAngle">The starting angle of the arc, measured in degrees clockwise from the x-axis. </param>
		/// <param name="sweepAngle">The angle between <paramref name="startAngle" /> and the end of the arc. </param>
		// Token: 0x060002B2 RID: 690 RVA: 0x00004644 File Offset: 0x00002844
		public void AddArc(int x, int y, int width, int height, float startAngle, float sweepAngle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Appends an elliptical arc to the current figure.</summary>
		/// <param name="x">The x-coordinate of the upper-left corner of the rectangular region that defines the ellipse from which the arc is drawn. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the rectangular region that defines the ellipse from which the arc is drawn. </param>
		/// <param name="width">The width of the rectangular region that defines the ellipse from which the arc is drawn. </param>
		/// <param name="height">The height of the rectangular region that defines the ellipse from which the arc is drawn. </param>
		/// <param name="startAngle">The starting angle of the arc, measured in degrees clockwise from the x-axis. </param>
		/// <param name="sweepAngle">The angle between <paramref name="startAngle" /> and the end of the arc. </param>
		// Token: 0x060002B3 RID: 691 RVA: 0x00004644 File Offset: 0x00002844
		public void AddArc(float x, float y, float width, float height, float startAngle, float sweepAngle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a cubic Bézier curve to the current figure.</summary>
		/// <param name="pt1">A <see cref="T:System.Drawing.Point" /> that represents the starting point of the curve. </param>
		/// <param name="pt2">A <see cref="T:System.Drawing.Point" /> that represents the first control point for the curve. </param>
		/// <param name="pt3">A <see cref="T:System.Drawing.Point" /> that represents the second control point for the curve. </param>
		/// <param name="pt4">A <see cref="T:System.Drawing.Point" /> that represents the endpoint of the curve. </param>
		// Token: 0x060002B4 RID: 692 RVA: 0x00004644 File Offset: 0x00002844
		public void AddBezier(Point pt1, Point pt2, Point pt3, Point pt4)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a cubic Bézier curve to the current figure.</summary>
		/// <param name="pt1">A <see cref="T:System.Drawing.PointF" /> that represents the starting point of the curve. </param>
		/// <param name="pt2">A <see cref="T:System.Drawing.PointF" /> that represents the first control point for the curve. </param>
		/// <param name="pt3">A <see cref="T:System.Drawing.PointF" /> that represents the second control point for the curve. </param>
		/// <param name="pt4">A <see cref="T:System.Drawing.PointF" /> that represents the endpoint of the curve. </param>
		// Token: 0x060002B5 RID: 693 RVA: 0x00004644 File Offset: 0x00002844
		public void AddBezier(PointF pt1, PointF pt2, PointF pt3, PointF pt4)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a cubic Bézier curve to the current figure.</summary>
		/// <param name="x1">The x-coordinate of the starting point of the curve. </param>
		/// <param name="y1">The y-coordinate of the starting point of the curve. </param>
		/// <param name="x2">The x-coordinate of the first control point for the curve. </param>
		/// <param name="y2">The y-coordinate of the first control point for the curve. </param>
		/// <param name="x3">The x-coordinate of the second control point for the curve. </param>
		/// <param name="y3">The y-coordinate of the second control point for the curve. </param>
		/// <param name="x4">The x-coordinate of the endpoint of the curve. </param>
		/// <param name="y4">The y-coordinate of the endpoint of the curve. </param>
		// Token: 0x060002B6 RID: 694 RVA: 0x00004644 File Offset: 0x00002844
		public void AddBezier(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a cubic Bézier curve to the current figure.</summary>
		/// <param name="x1">The x-coordinate of the starting point of the curve. </param>
		/// <param name="y1">The y-coordinate of the starting point of the curve. </param>
		/// <param name="x2">The x-coordinate of the first control point for the curve. </param>
		/// <param name="y2">The y-coordinate of the first control point for the curve. </param>
		/// <param name="x3">The x-coordinate of the second control point for the curve. </param>
		/// <param name="y3">The y-coordinate of the second control point for the curve. </param>
		/// <param name="x4">The x-coordinate of the endpoint of the curve. </param>
		/// <param name="y4">The y-coordinate of the endpoint of the curve. </param>
		// Token: 0x060002B7 RID: 695 RVA: 0x00004644 File Offset: 0x00002844
		public void AddBezier(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a sequence of connected cubic Bézier curves to the current figure.</summary>
		/// <param name="points">An array of <see cref="T:System.Drawing.PointF" /> structures that represents the points that define the curves. </param>
		// Token: 0x060002B8 RID: 696 RVA: 0x00004644 File Offset: 0x00002844
		public void AddBeziers(PointF[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a sequence of connected cubic Bézier curves to the current figure.</summary>
		/// <param name="points">An array of <see cref="T:System.Drawing.Point" /> structures that represents the points that define the curves. </param>
		// Token: 0x060002B9 RID: 697 RVA: 0x00004644 File Offset: 0x00002844
		public void AddBeziers(Point[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a closed curve to this path. A cardinal spline curve is used because the curve travels through each of the points in the array.</summary>
		/// <param name="points">An array of <see cref="T:System.Drawing.PointF" /> structures that represents the points that define the curve. </param>
		// Token: 0x060002BA RID: 698 RVA: 0x00004644 File Offset: 0x00002844
		public void AddClosedCurve(PointF[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a closed curve to this path. A cardinal spline curve is used because the curve travels through each of the points in the array.</summary>
		/// <param name="points">An array of <see cref="T:System.Drawing.PointF" /> structures that represents the points that define the curve. </param>
		/// <param name="tension">A value between from 0 through 1 that specifies the amount that the curve bends between points, with 0 being the smallest curve (sharpest corner) and 1 being the smoothest curve. </param>
		// Token: 0x060002BB RID: 699 RVA: 0x00004644 File Offset: 0x00002844
		public void AddClosedCurve(PointF[] points, float tension)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a closed curve to this path. A cardinal spline curve is used because the curve travels through each of the points in the array.</summary>
		/// <param name="points">An array of <see cref="T:System.Drawing.Point" /> structures that represents the points that define the curve. </param>
		// Token: 0x060002BC RID: 700 RVA: 0x00004644 File Offset: 0x00002844
		public void AddClosedCurve(Point[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a closed curve to this path. A cardinal spline curve is used because the curve travels through each of the points in the array.</summary>
		/// <param name="points">An array of <see cref="T:System.Drawing.Point" /> structures that represents the points that define the curve. </param>
		/// <param name="tension">A value between from 0 through 1 that specifies the amount that the curve bends between points, with 0 being the smallest curve (sharpest corner) and 1 being the smoothest curve. </param>
		// Token: 0x060002BD RID: 701 RVA: 0x00004644 File Offset: 0x00002844
		public void AddClosedCurve(Point[] points, float tension)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a spline curve to the current figure. A cardinal spline curve is used because the curve travels through each of the points in the array.</summary>
		/// <param name="points">An array of <see cref="T:System.Drawing.PointF" /> structures that represents the points that define the curve. </param>
		// Token: 0x060002BE RID: 702 RVA: 0x00004644 File Offset: 0x00002844
		public void AddCurve(PointF[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a spline curve to the current figure.</summary>
		/// <param name="points">An array of <see cref="T:System.Drawing.PointF" /> structures that represents the points that define the curve. </param>
		/// <param name="offset">The index of the element in the <paramref name="points" /> array that is used as the first point in the curve. </param>
		/// <param name="numberOfSegments">The number of segments used to draw the curve. A segment can be thought of as a line connecting two points. </param>
		/// <param name="tension">A value that specifies the amount that the curve bends between control points. Values greater than 1 produce unpredictable results. </param>
		// Token: 0x060002BF RID: 703 RVA: 0x00004644 File Offset: 0x00002844
		public void AddCurve(PointF[] points, int offset, int numberOfSegments, float tension)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a spline curve to the current figure.</summary>
		/// <param name="points">An array of <see cref="T:System.Drawing.PointF" /> structures that represents the points that define the curve. </param>
		/// <param name="tension">A value that specifies the amount that the curve bends between control points. Values greater than 1 produce unpredictable results. </param>
		// Token: 0x060002C0 RID: 704 RVA: 0x00004644 File Offset: 0x00002844
		public void AddCurve(PointF[] points, float tension)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a spline curve to the current figure. A cardinal spline curve is used because the curve travels through each of the points in the array.</summary>
		/// <param name="points">An array of <see cref="T:System.Drawing.Point" /> structures that represents the points that define the curve. </param>
		// Token: 0x060002C1 RID: 705 RVA: 0x00004644 File Offset: 0x00002844
		public void AddCurve(Point[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a spline curve to the current figure.</summary>
		/// <param name="points">An array of <see cref="T:System.Drawing.Point" /> structures that represents the points that define the curve. </param>
		/// <param name="offset">The index of the element in the <paramref name="points" /> array that is used as the first point in the curve. </param>
		/// <param name="numberOfSegments">A value that specifies the amount that the curve bends between control points. Values greater than 1 produce unpredictable results. </param>
		/// <param name="tension">A value that specifies the amount that the curve bends between control points. Values greater than 1 produce unpredictable results. </param>
		// Token: 0x060002C2 RID: 706 RVA: 0x00004644 File Offset: 0x00002844
		public void AddCurve(Point[] points, int offset, int numberOfSegments, float tension)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a spline curve to the current figure.</summary>
		/// <param name="points">An array of <see cref="T:System.Drawing.Point" /> structures that represents the points that define the curve. </param>
		/// <param name="tension">A value that specifies the amount that the curve bends between control points. Values greater than 1 produce unpredictable results. </param>
		// Token: 0x060002C3 RID: 707 RVA: 0x00004644 File Offset: 0x00002844
		public void AddCurve(Point[] points, float tension)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds an ellipse to the current path.</summary>
		/// <param name="rect">A <see cref="T:System.Drawing.Rectangle" /> that represents the bounding rectangle that defines the ellipse. </param>
		// Token: 0x060002C4 RID: 708 RVA: 0x00004644 File Offset: 0x00002844
		public void AddEllipse(Rectangle rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds an ellipse to the current path.</summary>
		/// <param name="rect">A <see cref="T:System.Drawing.RectangleF" /> that represents the bounding rectangle that defines the ellipse. </param>
		// Token: 0x060002C5 RID: 709 RVA: 0x00004644 File Offset: 0x00002844
		public void AddEllipse(RectangleF rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds an ellipse to the current path.</summary>
		/// <param name="x">The x-coordinate of the upper-left corner of the bounding rectangle that defines the ellipse. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the bounding rectangle that defines the ellipse. </param>
		/// <param name="width">The width of the bounding rectangle that defines the ellipse. </param>
		/// <param name="height">The height of the bounding rectangle that defines the ellipse. </param>
		// Token: 0x060002C6 RID: 710 RVA: 0x00004644 File Offset: 0x00002844
		public void AddEllipse(int x, int y, int width, int height)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds an ellipse to the current path.</summary>
		/// <param name="x">The x-coordinate of the upper-left corner of the bounding rectangle that defines the ellipse. </param>
		/// <param name="y">The y-coordinate of the upper left corner of the bounding rectangle that defines the ellipse. </param>
		/// <param name="width">The width of the bounding rectangle that defines the ellipse. </param>
		/// <param name="height">The height of the bounding rectangle that defines the ellipse. </param>
		// Token: 0x060002C7 RID: 711 RVA: 0x00004644 File Offset: 0x00002844
		public void AddEllipse(float x, float y, float width, float height)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Appends a line segment to this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="pt1">A <see cref="T:System.Drawing.Point" /> that represents the starting point of the line. </param>
		/// <param name="pt2">A <see cref="T:System.Drawing.Point" /> that represents the endpoint of the line. </param>
		// Token: 0x060002C8 RID: 712 RVA: 0x00004644 File Offset: 0x00002844
		public void AddLine(Point pt1, Point pt2)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Appends a line segment to this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="pt1">A <see cref="T:System.Drawing.PointF" /> that represents the starting point of the line. </param>
		/// <param name="pt2">A <see cref="T:System.Drawing.PointF" /> that represents the endpoint of the line. </param>
		// Token: 0x060002C9 RID: 713 RVA: 0x00004644 File Offset: 0x00002844
		public void AddLine(PointF pt1, PointF pt2)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Appends a line segment to the current figure.</summary>
		/// <param name="x1">The x-coordinate of the starting point of the line. </param>
		/// <param name="y1">The y-coordinate of the starting point of the line. </param>
		/// <param name="x2">The x-coordinate of the endpoint of the line. </param>
		/// <param name="y2">The y-coordinate of the endpoint of the line. </param>
		// Token: 0x060002CA RID: 714 RVA: 0x00004644 File Offset: 0x00002844
		public void AddLine(int x1, int y1, int x2, int y2)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Appends a line segment to this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="x1">The x-coordinate of the starting point of the line. </param>
		/// <param name="y1">The y-coordinate of the starting point of the line. </param>
		/// <param name="x2">The x-coordinate of the endpoint of the line. </param>
		/// <param name="y2">The y-coordinate of the endpoint of the line. </param>
		// Token: 0x060002CB RID: 715 RVA: 0x00004644 File Offset: 0x00002844
		public void AddLine(float x1, float y1, float x2, float y2)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Appends a series of connected line segments to the end of this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="points">An array of <see cref="T:System.Drawing.PointF" /> structures that represents the points that define the line segments to add. </param>
		// Token: 0x060002CC RID: 716 RVA: 0x00004644 File Offset: 0x00002844
		public void AddLines(PointF[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Appends a series of connected line segments to the end of this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="points">An array of <see cref="T:System.Drawing.Point" /> structures that represents the points that define the line segments to add. </param>
		// Token: 0x060002CD RID: 717 RVA: 0x00004644 File Offset: 0x00002844
		public void AddLines(Point[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Appends the specified <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> to this path.</summary>
		/// <param name="addingPath">The <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> to add. </param>
		/// <param name="connect">A Boolean value that specifies whether the first figure in the added path is part of the last figure in this path. A value of <see langword="true" /> specifies that (if possible) the first figure in the added path is part of the last figure in this path. A value of <see langword="false" /> specifies that the first figure in the added path is separate from the last figure in this path. </param>
		// Token: 0x060002CE RID: 718 RVA: 0x00004644 File Offset: 0x00002844
		public void AddPath(GraphicsPath addingPath, bool connect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds the outline of a pie shape to this path.</summary>
		/// <param name="rect">A <see cref="T:System.Drawing.Rectangle" /> that represents the bounding rectangle that defines the ellipse from which the pie is drawn. </param>
		/// <param name="startAngle">The starting angle for the pie section, measured in degrees clockwise from the x-axis. </param>
		/// <param name="sweepAngle">The angle between <paramref name="startAngle" /> and the end of the pie section, measured in degrees clockwise from <paramref name="startAngle" />. </param>
		// Token: 0x060002CF RID: 719 RVA: 0x00004644 File Offset: 0x00002844
		public void AddPie(Rectangle rect, float startAngle, float sweepAngle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds the outline of a pie shape to this path.</summary>
		/// <param name="x">The x-coordinate of the upper-left corner of the bounding rectangle that defines the ellipse from which the pie is drawn. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the bounding rectangle that defines the ellipse from which the pie is drawn. </param>
		/// <param name="width">The width of the bounding rectangle that defines the ellipse from which the pie is drawn. </param>
		/// <param name="height">The height of the bounding rectangle that defines the ellipse from which the pie is drawn. </param>
		/// <param name="startAngle">The starting angle for the pie section, measured in degrees clockwise from the x-axis. </param>
		/// <param name="sweepAngle">The angle between <paramref name="startAngle" /> and the end of the pie section, measured in degrees clockwise from <paramref name="startAngle" />. </param>
		// Token: 0x060002D0 RID: 720 RVA: 0x00004644 File Offset: 0x00002844
		public void AddPie(int x, int y, int width, int height, float startAngle, float sweepAngle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds the outline of a pie shape to this path.</summary>
		/// <param name="x">The x-coordinate of the upper-left corner of the bounding rectangle that defines the ellipse from which the pie is drawn. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the bounding rectangle that defines the ellipse from which the pie is drawn. </param>
		/// <param name="width">The width of the bounding rectangle that defines the ellipse from which the pie is drawn. </param>
		/// <param name="height">The height of the bounding rectangle that defines the ellipse from which the pie is drawn. </param>
		/// <param name="startAngle">The starting angle for the pie section, measured in degrees clockwise from the x-axis. </param>
		/// <param name="sweepAngle">The angle between <paramref name="startAngle" /> and the end of the pie section, measured in degrees clockwise from <paramref name="startAngle" />. </param>
		// Token: 0x060002D1 RID: 721 RVA: 0x00004644 File Offset: 0x00002844
		public void AddPie(float x, float y, float width, float height, float startAngle, float sweepAngle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a polygon to this path.</summary>
		/// <param name="points">An array of <see cref="T:System.Drawing.PointF" /> structures that defines the polygon to add. </param>
		// Token: 0x060002D2 RID: 722 RVA: 0x00004644 File Offset: 0x00002844
		public void AddPolygon(PointF[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a polygon to this path.</summary>
		/// <param name="points">An array of <see cref="T:System.Drawing.Point" /> structures that defines the polygon to add. </param>
		// Token: 0x060002D3 RID: 723 RVA: 0x00004644 File Offset: 0x00002844
		public void AddPolygon(Point[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a rectangle to this path.</summary>
		/// <param name="rect">A <see cref="T:System.Drawing.Rectangle" /> that represents the rectangle to add. </param>
		// Token: 0x060002D4 RID: 724 RVA: 0x00004644 File Offset: 0x00002844
		public void AddRectangle(Rectangle rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a rectangle to this path.</summary>
		/// <param name="rect">A <see cref="T:System.Drawing.RectangleF" /> that represents the rectangle to add. </param>
		// Token: 0x060002D5 RID: 725 RVA: 0x00004644 File Offset: 0x00002844
		public void AddRectangle(RectangleF rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a series of rectangles to this path.</summary>
		/// <param name="rects">An array of <see cref="T:System.Drawing.RectangleF" /> structures that represents the rectangles to add. </param>
		// Token: 0x060002D6 RID: 726 RVA: 0x00004644 File Offset: 0x00002844
		public void AddRectangles(RectangleF[] rects)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a series of rectangles to this path.</summary>
		/// <param name="rects">An array of <see cref="T:System.Drawing.Rectangle" /> structures that represents the rectangles to add. </param>
		// Token: 0x060002D7 RID: 727 RVA: 0x00004644 File Offset: 0x00002844
		public void AddRectangles(Rectangle[] rects)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a text string to this path.</summary>
		/// <param name="s">The <see cref="T:System.String" /> to add. </param>
		/// <param name="family">A <see cref="T:System.Drawing.FontFamily" /> that represents the name of the font with which the test is drawn. </param>
		/// <param name="style">A <see cref="T:System.Drawing.FontStyle" /> enumeration that represents style information about the text (bold, italic, and so on). This must be cast as an integer (see the example code later in this section). </param>
		/// <param name="emSize">The height of the em square box that bounds the character. </param>
		/// <param name="origin">A <see cref="T:System.Drawing.Point" /> that represents the point where the text starts. </param>
		/// <param name="format">A <see cref="T:System.Drawing.StringFormat" /> that specifies text formatting information, such as line spacing and alignment. </param>
		// Token: 0x060002D8 RID: 728 RVA: 0x00004644 File Offset: 0x00002844
		public void AddString(string s, FontFamily family, int style, float emSize, Point origin, StringFormat format)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a text string to this path.</summary>
		/// <param name="s">The <see cref="T:System.String" /> to add. </param>
		/// <param name="family">A <see cref="T:System.Drawing.FontFamily" /> that represents the name of the font with which the test is drawn. </param>
		/// <param name="style">A <see cref="T:System.Drawing.FontStyle" /> enumeration that represents style information about the text (bold, italic, and so on). This must be cast as an integer (see the example code later in this section). </param>
		/// <param name="emSize">The height of the em square box that bounds the character. </param>
		/// <param name="origin">A <see cref="T:System.Drawing.PointF" /> that represents the point where the text starts. </param>
		/// <param name="format">A <see cref="T:System.Drawing.StringFormat" /> that specifies text formatting information, such as line spacing and alignment. </param>
		// Token: 0x060002D9 RID: 729 RVA: 0x00004644 File Offset: 0x00002844
		public void AddString(string s, FontFamily family, int style, float emSize, PointF origin, StringFormat format)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a text string to this path.</summary>
		/// <param name="s">The <see cref="T:System.String" /> to add. </param>
		/// <param name="family">A <see cref="T:System.Drawing.FontFamily" /> that represents the name of the font with which the test is drawn. </param>
		/// <param name="style">A <see cref="T:System.Drawing.FontStyle" /> enumeration that represents style information about the text (bold, italic, and so on). This must be cast as an integer (see the example code later in this section). </param>
		/// <param name="emSize">The height of the em square box that bounds the character. </param>
		/// <param name="layoutRect">A <see cref="T:System.Drawing.Rectangle" /> that represents the rectangle that bounds the text. </param>
		/// <param name="format">A <see cref="T:System.Drawing.StringFormat" /> that specifies text formatting information, such as line spacing and alignment. </param>
		// Token: 0x060002DA RID: 730 RVA: 0x00004644 File Offset: 0x00002844
		public void AddString(string s, FontFamily family, int style, float emSize, Rectangle layoutRect, StringFormat format)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a text string to this path.</summary>
		/// <param name="s">The <see cref="T:System.String" /> to add. </param>
		/// <param name="family">A <see cref="T:System.Drawing.FontFamily" /> that represents the name of the font with which the test is drawn. </param>
		/// <param name="style">A <see cref="T:System.Drawing.FontStyle" /> enumeration that represents style information about the text (bold, italic, and so on). This must be cast as an integer (see the example code later in this section). </param>
		/// <param name="emSize">The height of the em square box that bounds the character. </param>
		/// <param name="layoutRect">A <see cref="T:System.Drawing.RectangleF" /> that represents the rectangle that bounds the text. </param>
		/// <param name="format">A <see cref="T:System.Drawing.StringFormat" /> that specifies text formatting information, such as line spacing and alignment. </param>
		// Token: 0x060002DB RID: 731 RVA: 0x00004644 File Offset: 0x00002844
		public void AddString(string s, FontFamily family, int style, float emSize, RectangleF layoutRect, StringFormat format)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Clears all markers from this path.</summary>
		// Token: 0x060002DC RID: 732 RVA: 0x00004644 File Offset: 0x00002844
		public void ClearMarkers()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates an exact copy of this path.</summary>
		/// <returns>The <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> this method creates, cast as an object.</returns>
		// Token: 0x060002DD RID: 733 RVA: 0x00004667 File Offset: 0x00002867
		public object Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Closes all open figures in this path and starts a new figure. It closes each open figure by connecting a line from its endpoint to its starting point.</summary>
		// Token: 0x060002DE RID: 734 RVA: 0x00004644 File Offset: 0x00002844
		public void CloseAllFigures()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Closes the current figure and starts a new figure. If the current figure contains a sequence of connected lines and curves, the method closes the loop by connecting a line from the endpoint to the starting point.</summary>
		// Token: 0x060002DF RID: 735 RVA: 0x00004644 File Offset: 0x00002844
		public void CloseFigure()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Releases all resources used by this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		// Token: 0x060002E0 RID: 736 RVA: 0x00004644 File Offset: 0x00002844
		public void Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Converts each curve in this path into a sequence of connected line segments.</summary>
		// Token: 0x060002E1 RID: 737 RVA: 0x00004644 File Offset: 0x00002844
		public void Flatten()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Applies the specified transform and then converts each curve in this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> into a sequence of connected line segments.</summary>
		/// <param name="matrix">A <see cref="T:System.Drawing.Drawing2D.Matrix" /> by which to transform this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> before flattening. </param>
		// Token: 0x060002E2 RID: 738 RVA: 0x00004644 File Offset: 0x00002844
		public void Flatten(Matrix matrix)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Converts each curve in this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> into a sequence of connected line segments.</summary>
		/// <param name="matrix">A <see cref="T:System.Drawing.Drawing2D.Matrix" /> by which to transform this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> before flattening. </param>
		/// <param name="flatness">Specifies the maximum permitted error between the curve and its flattened approximation. A value of 0.25 is the default. Reducing the flatness value will increase the number of line segments in the approximation. </param>
		// Token: 0x060002E3 RID: 739 RVA: 0x00004644 File Offset: 0x00002844
		public void Flatten(Matrix matrix, float flatness)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns a rectangle that bounds this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.RectangleF" /> that represents a rectangle that bounds this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</returns>
		// Token: 0x060002E4 RID: 740 RVA: 0x00004D8C File Offset: 0x00002F8C
		public RectangleF GetBounds()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(RectangleF);
		}

		/// <summary>Returns a rectangle that bounds this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> when this path is transformed by the specified <see cref="T:System.Drawing.Drawing2D.Matrix" />.</summary>
		/// <param name="matrix">The <see cref="T:System.Drawing.Drawing2D.Matrix" /> that specifies a transformation to be applied to this path before the bounding rectangle is calculated. This path is not permanently transformed; the transformation is used only during the process of calculating the bounding rectangle. </param>
		/// <returns>A <see cref="T:System.Drawing.RectangleF" /> that represents a rectangle that bounds this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</returns>
		// Token: 0x060002E5 RID: 741 RVA: 0x00004DA8 File Offset: 0x00002FA8
		public RectangleF GetBounds(Matrix matrix)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(RectangleF);
		}

		/// <summary>Returns a rectangle that bounds this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> when the current path is transformed by the specified <see cref="T:System.Drawing.Drawing2D.Matrix" /> and drawn with the specified <see cref="T:System.Drawing.Pen" />.</summary>
		/// <param name="matrix">The <see cref="T:System.Drawing.Drawing2D.Matrix" /> that specifies a transformation to be applied to this path before the bounding rectangle is calculated. This path is not permanently transformed; the transformation is used only during the process of calculating the bounding rectangle. </param>
		/// <param name="pen">The <see cref="T:System.Drawing.Pen" /> with which to draw the <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />. </param>
		/// <returns>A <see cref="T:System.Drawing.RectangleF" /> that represents a rectangle that bounds this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</returns>
		// Token: 0x060002E6 RID: 742 RVA: 0x00004DC4 File Offset: 0x00002FC4
		public RectangleF GetBounds(Matrix matrix, Pen pen)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(RectangleF);
		}

		/// <summary>Gets the last point in the <see cref="P:System.Drawing.Drawing2D.GraphicsPath.PathPoints" /> array of this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.PointF" /> that represents the last point in this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</returns>
		// Token: 0x060002E7 RID: 743 RVA: 0x00004DE0 File Offset: 0x00002FE0
		public PointF GetLastPoint()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(PointF);
		}

		/// <summary>Indicates whether the specified point is contained within (under) the outline of this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> when drawn with the specified <see cref="T:System.Drawing.Pen" />.</summary>
		/// <param name="point">A <see cref="T:System.Drawing.Point" /> that specifies the location to test. </param>
		/// <param name="pen">The <see cref="T:System.Drawing.Pen" /> to test. </param>
		/// <returns>This method returns <see langword="true" /> if the specified point is contained within the outline of this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> when drawn with the specified <see cref="T:System.Drawing.Pen" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002E8 RID: 744 RVA: 0x00004DFC File Offset: 0x00002FFC
		public bool IsOutlineVisible(Point point, Pen pen)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the specified point is contained within (under) the outline of this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> when drawn with the specified <see cref="T:System.Drawing.Pen" /> and using the specified <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="pt">A <see cref="T:System.Drawing.Point" /> that specifies the location to test. </param>
		/// <param name="pen">The <see cref="T:System.Drawing.Pen" /> to test. </param>
		/// <param name="graphics">The <see cref="T:System.Drawing.Graphics" /> for which to test visibility. </param>
		/// <returns>This method returns <see langword="true" /> if the specified point is contained within the outline of this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> as drawn with the specified <see cref="T:System.Drawing.Pen" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002E9 RID: 745 RVA: 0x00004E18 File Offset: 0x00003018
		public bool IsOutlineVisible(Point pt, Pen pen, Graphics graphics)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the specified point is contained within (under) the outline of this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> when drawn with the specified <see cref="T:System.Drawing.Pen" />.</summary>
		/// <param name="point">A <see cref="T:System.Drawing.PointF" /> that specifies the location to test. </param>
		/// <param name="pen">The <see cref="T:System.Drawing.Pen" /> to test. </param>
		/// <returns>This method returns <see langword="true" /> if the specified point is contained within the outline of this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> when drawn with the specified <see cref="T:System.Drawing.Pen" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002EA RID: 746 RVA: 0x00004E34 File Offset: 0x00003034
		public bool IsOutlineVisible(PointF point, Pen pen)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the specified point is contained within (under) the outline of this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> when drawn with the specified <see cref="T:System.Drawing.Pen" /> and using the specified <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="pt">A <see cref="T:System.Drawing.PointF" /> that specifies the location to test. </param>
		/// <param name="pen">The <see cref="T:System.Drawing.Pen" /> to test. </param>
		/// <param name="graphics">The <see cref="T:System.Drawing.Graphics" /> for which to test visibility. </param>
		/// <returns>This method returns <see langword="true" /> if the specified point is contained within (under) the outline of this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> as drawn with the specified <see cref="T:System.Drawing.Pen" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002EB RID: 747 RVA: 0x00004E50 File Offset: 0x00003050
		public bool IsOutlineVisible(PointF pt, Pen pen, Graphics graphics)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the specified point is contained within (under) the outline of this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> when drawn with the specified <see cref="T:System.Drawing.Pen" />.</summary>
		/// <param name="x">The x-coordinate of the point to test. </param>
		/// <param name="y">The y-coordinate of the point to test. </param>
		/// <param name="pen">The <see cref="T:System.Drawing.Pen" /> to test. </param>
		/// <returns>This method returns <see langword="true" /> if the specified point is contained within the outline of this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> when drawn with the specified <see cref="T:System.Drawing.Pen" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002EC RID: 748 RVA: 0x00004E6C File Offset: 0x0000306C
		public bool IsOutlineVisible(int x, int y, Pen pen)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the specified point is contained within (under) the outline of this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> when drawn with the specified <see cref="T:System.Drawing.Pen" /> and using the specified <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="x">The x-coordinate of the point to test. </param>
		/// <param name="y">The y-coordinate of the point to test. </param>
		/// <param name="pen">The <see cref="T:System.Drawing.Pen" /> to test. </param>
		/// <param name="graphics">The <see cref="T:System.Drawing.Graphics" /> for which to test visibility. </param>
		/// <returns>This method returns <see langword="true" /> if the specified point is contained within the outline of this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> as drawn with the specified <see cref="T:System.Drawing.Pen" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002ED RID: 749 RVA: 0x00004E88 File Offset: 0x00003088
		public bool IsOutlineVisible(int x, int y, Pen pen, Graphics graphics)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the specified point is contained within (under) the outline of this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> when drawn with the specified <see cref="T:System.Drawing.Pen" />.</summary>
		/// <param name="x">The x-coordinate of the point to test. </param>
		/// <param name="y">The y-coordinate of the point to test. </param>
		/// <param name="pen">The <see cref="T:System.Drawing.Pen" /> to test. </param>
		/// <returns>This method returns <see langword="true" /> if the specified point is contained within the outline of this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> when drawn with the specified <see cref="T:System.Drawing.Pen" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002EE RID: 750 RVA: 0x00004EA4 File Offset: 0x000030A4
		public bool IsOutlineVisible(float x, float y, Pen pen)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the specified point is contained within (under) the outline of this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> when drawn with the specified <see cref="T:System.Drawing.Pen" /> and using the specified <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="x">The x-coordinate of the point to test. </param>
		/// <param name="y">The y-coordinate of the point to test. </param>
		/// <param name="pen">The <see cref="T:System.Drawing.Pen" /> to test. </param>
		/// <param name="graphics">The <see cref="T:System.Drawing.Graphics" /> for which to test visibility. </param>
		/// <returns>This method returns <see langword="true" /> if the specified point is contained within (under) the outline of this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> as drawn with the specified <see cref="T:System.Drawing.Pen" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002EF RID: 751 RVA: 0x00004EC0 File Offset: 0x000030C0
		public bool IsOutlineVisible(float x, float y, Pen pen, Graphics graphics)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the specified point is contained within this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="point">A <see cref="T:System.Drawing.Point" /> that represents the point to test. </param>
		/// <returns>This method returns <see langword="true" /> if the specified point is contained within this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002F0 RID: 752 RVA: 0x00004EDC File Offset: 0x000030DC
		public bool IsVisible(Point point)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the specified point is contained within this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="pt">A <see cref="T:System.Drawing.Point" /> that represents the point to test. </param>
		/// <param name="graphics">The <see cref="T:System.Drawing.Graphics" /> for which to test visibility. </param>
		/// <returns>This method returns <see langword="true" /> if the specified point is contained within this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002F1 RID: 753 RVA: 0x00004EF8 File Offset: 0x000030F8
		public bool IsVisible(Point pt, Graphics graphics)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the specified point is contained within this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="point">A <see cref="T:System.Drawing.PointF" /> that represents the point to test. </param>
		/// <returns>This method returns <see langword="true" /> if the specified point is contained within this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002F2 RID: 754 RVA: 0x00004F14 File Offset: 0x00003114
		public bool IsVisible(PointF point)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the specified point is contained within this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="pt">A <see cref="T:System.Drawing.PointF" /> that represents the point to test. </param>
		/// <param name="graphics">The <see cref="T:System.Drawing.Graphics" /> for which to test visibility. </param>
		/// <returns>This method returns <see langword="true" /> if the specified point is contained within this; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002F3 RID: 755 RVA: 0x00004F30 File Offset: 0x00003130
		public bool IsVisible(PointF pt, Graphics graphics)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the specified point is contained within this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="x">The x-coordinate of the point to test. </param>
		/// <param name="y">The y-coordinate of the point to test. </param>
		/// <returns>This method returns <see langword="true" /> if the specified point is contained within this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002F4 RID: 756 RVA: 0x00004F4C File Offset: 0x0000314C
		public bool IsVisible(int x, int y)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the specified point is contained within this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />, using the specified <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="x">The x-coordinate of the point to test. </param>
		/// <param name="y">The y-coordinate of the point to test. </param>
		/// <param name="graphics">The <see cref="T:System.Drawing.Graphics" /> for which to test visibility. </param>
		/// <returns>This method returns <see langword="true" /> if the specified point is contained within this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002F5 RID: 757 RVA: 0x00004F68 File Offset: 0x00003168
		public bool IsVisible(int x, int y, Graphics graphics)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the specified point is contained within this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="x">The x-coordinate of the point to test. </param>
		/// <param name="y">The y-coordinate of the point to test. </param>
		/// <returns>This method returns <see langword="true" /> if the specified point is contained within this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002F6 RID: 758 RVA: 0x00004F84 File Offset: 0x00003184
		public bool IsVisible(float x, float y)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the specified point is contained within this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> in the visible clip region of the specified <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="x">The x-coordinate of the point to test. </param>
		/// <param name="y">The y-coordinate of the point to test. </param>
		/// <param name="graphics">The <see cref="T:System.Drawing.Graphics" /> for which to test visibility. </param>
		/// <returns>This method returns <see langword="true" /> if the specified point is contained within this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002F7 RID: 759 RVA: 0x00004FA0 File Offset: 0x000031A0
		public bool IsVisible(float x, float y, Graphics graphics)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Empties the <see cref="P:System.Drawing.Drawing2D.GraphicsPath.PathPoints" /> and <see cref="P:System.Drawing.Drawing2D.GraphicsPath.PathTypes" /> arrays and sets the <see cref="T:System.Drawing.Drawing2D.FillMode" /> to <see cref="F:System.Drawing.Drawing2D.FillMode.Alternate" />.</summary>
		// Token: 0x060002F8 RID: 760 RVA: 0x00004644 File Offset: 0x00002844
		public void Reset()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Reverses the order of points in the <see cref="P:System.Drawing.Drawing2D.GraphicsPath.PathPoints" /> array of this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		// Token: 0x060002F9 RID: 761 RVA: 0x00004644 File Offset: 0x00002844
		public void Reverse()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets a marker on this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		// Token: 0x060002FA RID: 762 RVA: 0x00004644 File Offset: 0x00002844
		public void SetMarkers()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Starts a new figure without closing the current figure. All subsequent points added to the path are added to this new figure.</summary>
		// Token: 0x060002FB RID: 763 RVA: 0x00004644 File Offset: 0x00002844
		public void StartFigure()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Applies a transform matrix to this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="matrix">A <see cref="T:System.Drawing.Drawing2D.Matrix" /> that represents the transformation to apply. </param>
		// Token: 0x060002FC RID: 764 RVA: 0x00004644 File Offset: 0x00002844
		public void Transform(Matrix matrix)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Applies a warp transform, defined by a rectangle and a parallelogram, to this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="destPoints">An array of <see cref="T:System.Drawing.PointF" /> structures that define a parallelogram to which the rectangle defined by <paramref name="srcRect" /> is transformed. The array can contain either three or four elements. If the array contains three elements, the lower-right corner of the parallelogram is implied by the first three points. </param>
		/// <param name="srcRect">A <see cref="T:System.Drawing.RectangleF" /> that represents the rectangle that is transformed to the parallelogram defined by <paramref name="destPoints" />. </param>
		// Token: 0x060002FD RID: 765 RVA: 0x00004644 File Offset: 0x00002844
		public void Warp(PointF[] destPoints, RectangleF srcRect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Applies a warp transform, defined by a rectangle and a parallelogram, to this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="destPoints">An array of <see cref="T:System.Drawing.PointF" /> structures that define a parallelogram to which the rectangle defined by <paramref name="srcRect" /> is transformed. The array can contain either three or four elements. If the array contains three elements, the lower-right corner of the parallelogram is implied by the first three points. </param>
		/// <param name="srcRect">A <see cref="T:System.Drawing.RectangleF" /> that represents the rectangle that is transformed to the parallelogram defined by <paramref name="destPoints" />. </param>
		/// <param name="matrix">A <see cref="T:System.Drawing.Drawing2D.Matrix" /> that specifies a geometric transform to apply to the path. </param>
		// Token: 0x060002FE RID: 766 RVA: 0x00004644 File Offset: 0x00002844
		public void Warp(PointF[] destPoints, RectangleF srcRect, Matrix matrix)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Applies a warp transform, defined by a rectangle and a parallelogram, to this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="destPoints">An array of <see cref="T:System.Drawing.PointF" /> structures that defines a parallelogram to which the rectangle defined by <paramref name="srcRect" /> is transformed. The array can contain either three or four elements. If the array contains three elements, the lower-right corner of the parallelogram is implied by the first three points. </param>
		/// <param name="srcRect">A <see cref="T:System.Drawing.RectangleF" /> that represents the rectangle that is transformed to the parallelogram defined by <paramref name="destPoints" />. </param>
		/// <param name="matrix">A <see cref="T:System.Drawing.Drawing2D.Matrix" /> that specifies a geometric transform to apply to the path. </param>
		/// <param name="warpMode">A <see cref="T:System.Drawing.Drawing2D.WarpMode" /> enumeration that specifies whether this warp operation uses perspective or bilinear mode. </param>
		// Token: 0x060002FF RID: 767 RVA: 0x00004644 File Offset: 0x00002844
		public void Warp(PointF[] destPoints, RectangleF srcRect, Matrix matrix, WarpMode warpMode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Applies a warp transform, defined by a rectangle and a parallelogram, to this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="destPoints">An array of <see cref="T:System.Drawing.PointF" /> structures that define a parallelogram to which the rectangle defined by <paramref name="srcRect" /> is transformed. The array can contain either three or four elements. If the array contains three elements, the lower-right corner of the parallelogram is implied by the first three points. </param>
		/// <param name="srcRect">A <see cref="T:System.Drawing.RectangleF" /> that represents the rectangle that is transformed to the parallelogram defined by <paramref name="destPoints" />. </param>
		/// <param name="matrix">A <see cref="T:System.Drawing.Drawing2D.Matrix" /> that specifies a geometric transform to apply to the path. </param>
		/// <param name="warpMode">A <see cref="T:System.Drawing.Drawing2D.WarpMode" /> enumeration that specifies whether this warp operation uses perspective or bilinear mode. </param>
		/// <param name="flatness">A value from 0 through 1 that specifies how flat the resulting path is. For more information, see the <see cref="M:System.Drawing.Drawing2D.GraphicsPath.Flatten" /> methods. </param>
		// Token: 0x06000300 RID: 768 RVA: 0x00004644 File Offset: 0x00002844
		public void Warp(PointF[] destPoints, RectangleF srcRect, Matrix matrix, WarpMode warpMode, float flatness)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds an additional outline to the path.</summary>
		/// <param name="pen">A <see cref="T:System.Drawing.Pen" /> that specifies the width between the original outline of the path and the new outline this method creates. </param>
		// Token: 0x06000301 RID: 769 RVA: 0x00004644 File Offset: 0x00002844
		public void Widen(Pen pen)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds an additional outline to the <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="pen">A <see cref="T:System.Drawing.Pen" /> that specifies the width between the original outline of the path and the new outline this method creates. </param>
		/// <param name="matrix">A <see cref="T:System.Drawing.Drawing2D.Matrix" /> that specifies a transform to apply to the path before widening. </param>
		// Token: 0x06000302 RID: 770 RVA: 0x00004644 File Offset: 0x00002844
		public void Widen(Pen pen, Matrix matrix)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Replaces this <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> with curves that enclose the area that is filled when this path is drawn by the specified pen.</summary>
		/// <param name="pen">A <see cref="T:System.Drawing.Pen" /> that specifies the width between the original outline of the path and the new outline this method creates. </param>
		/// <param name="matrix">A <see cref="T:System.Drawing.Drawing2D.Matrix" /> that specifies a transform to apply to the path before widening. </param>
		/// <param name="flatness">A value that specifies the flatness for curves. </param>
		// Token: 0x06000303 RID: 771 RVA: 0x00004644 File Offset: 0x00002844
		public void Widen(Pen pen, Matrix matrix, float flatness)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
