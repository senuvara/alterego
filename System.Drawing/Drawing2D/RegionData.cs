﻿using System;
using Unity;

namespace System.Drawing.Drawing2D
{
	/// <summary>Encapsulates the data that makes up a <see cref="T:System.Drawing.Region" /> object. This class cannot be inherited.</summary>
	// Token: 0x02000033 RID: 51
	public sealed class RegionData
	{
		// Token: 0x060003A0 RID: 928 RVA: 0x00004644 File Offset: 0x00002844
		internal RegionData()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets an array of bytes that specify the <see cref="T:System.Drawing.Region" /> object.</summary>
		/// <returns>An array of bytes that specify the <see cref="T:System.Drawing.Region" /> object.</returns>
		// Token: 0x17000103 RID: 259
		// (get) Token: 0x060003A1 RID: 929 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x060003A2 RID: 930 RVA: 0x00004644 File Offset: 0x00002844
		public byte[] Data
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
