﻿using System;
using Unity;

namespace System.Drawing.Drawing2D
{
	/// <summary>Represents the state of a <see cref="T:System.Drawing.Graphics" /> object. This object is returned by a call to the <see cref="M:System.Drawing.Graphics.Save" /> methods. This class cannot be inherited.</summary>
	// Token: 0x02000065 RID: 101
	public sealed class GraphicsState : MarshalByRefObject
	{
		// Token: 0x06000564 RID: 1380 RVA: 0x00004644 File Offset: 0x00002844
		internal GraphicsState()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
