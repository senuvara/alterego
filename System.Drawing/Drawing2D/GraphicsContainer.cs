﻿using System;
using Unity;

namespace System.Drawing.Drawing2D
{
	/// <summary>Represents the internal data of a graphics container. This class is used when saving the state of a <see cref="T:System.Drawing.Graphics" /> object using the <see cref="M:System.Drawing.Graphics.BeginContainer" /> and <see cref="M:System.Drawing.Graphics.EndContainer(System.Drawing.Drawing2D.GraphicsContainer)" /> methods. This class cannot be inherited.</summary>
	// Token: 0x0200003B RID: 59
	public sealed class GraphicsContainer : MarshalByRefObject
	{
		// Token: 0x060003A3 RID: 931 RVA: 0x00004644 File Offset: 0x00002844
		internal GraphicsContainer()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
