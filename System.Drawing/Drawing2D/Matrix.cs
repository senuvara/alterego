﻿using System;
using Unity;

namespace System.Drawing.Drawing2D
{
	/// <summary>Encapsulates a 3-by-3 affine matrix that represents a geometric transform. This class cannot be inherited.</summary>
	// Token: 0x02000027 RID: 39
	public sealed class Matrix : MarshalByRefObject, IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.Matrix" /> class as the identity matrix.</summary>
		// Token: 0x0600033C RID: 828 RVA: 0x00004644 File Offset: 0x00002844
		public Matrix()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.Matrix" /> class to the geometric transform defined by the specified rectangle and array of points.</summary>
		/// <param name="rect">A <see cref="T:System.Drawing.Rectangle" /> structure that represents the rectangle to be transformed. </param>
		/// <param name="plgpts">An array of three <see cref="T:System.Drawing.Point" /> structures that represents the points of a parallelogram to which the upper-left, upper-right, and lower-left corners of the rectangle is to be transformed. The lower-right corner of the parallelogram is implied by the first three corners. </param>
		// Token: 0x0600033D RID: 829 RVA: 0x00004644 File Offset: 0x00002844
		public Matrix(Rectangle rect, Point[] plgpts)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.Matrix" /> class to the geometric transform defined by the specified rectangle and array of points.</summary>
		/// <param name="rect">A <see cref="T:System.Drawing.RectangleF" /> structure that represents the rectangle to be transformed. </param>
		/// <param name="plgpts">An array of three <see cref="T:System.Drawing.PointF" /> structures that represents the points of a parallelogram to which the upper-left, upper-right, and lower-left corners of the rectangle is to be transformed. The lower-right corner of the parallelogram is implied by the first three corners. </param>
		// Token: 0x0600033E RID: 830 RVA: 0x00004644 File Offset: 0x00002844
		public Matrix(RectangleF rect, PointF[] plgpts)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.Matrix" /> class with the specified elements.</summary>
		/// <param name="m11">The value in the first row and first column of the new <see cref="T:System.Drawing.Drawing2D.Matrix" />. </param>
		/// <param name="m12">The value in the first row and second column of the new <see cref="T:System.Drawing.Drawing2D.Matrix" />. </param>
		/// <param name="m21">The value in the second row and first column of the new <see cref="T:System.Drawing.Drawing2D.Matrix" />. </param>
		/// <param name="m22">The value in the second row and second column of the new <see cref="T:System.Drawing.Drawing2D.Matrix" />. </param>
		/// <param name="dx">The value in the third row and first column of the new <see cref="T:System.Drawing.Drawing2D.Matrix" />. </param>
		/// <param name="dy">The value in the third row and second column of the new <see cref="T:System.Drawing.Drawing2D.Matrix" />. </param>
		// Token: 0x0600033F RID: 831 RVA: 0x00004644 File Offset: 0x00002844
		public Matrix(float m11, float m12, float m21, float m22, float dx, float dy)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets an array of floating-point values that represents the elements of this <see cref="T:System.Drawing.Drawing2D.Matrix" />.</summary>
		/// <returns>An array of floating-point values that represents the elements of this <see cref="T:System.Drawing.Drawing2D.Matrix" />.</returns>
		// Token: 0x170000E9 RID: 233
		// (get) Token: 0x06000340 RID: 832 RVA: 0x00004667 File Offset: 0x00002867
		public float[] Elements
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a value indicating whether this <see cref="T:System.Drawing.Drawing2D.Matrix" /> is the identity matrix.</summary>
		/// <returns>This property is <see langword="true" /> if this <see cref="T:System.Drawing.Drawing2D.Matrix" /> is identity; otherwise, <see langword="false" />.</returns>
		// Token: 0x170000EA RID: 234
		// (get) Token: 0x06000341 RID: 833 RVA: 0x0000517C File Offset: 0x0000337C
		public bool IsIdentity
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value indicating whether this <see cref="T:System.Drawing.Drawing2D.Matrix" /> is invertible.</summary>
		/// <returns>This property is <see langword="true" /> if this <see cref="T:System.Drawing.Drawing2D.Matrix" /> is invertible; otherwise, <see langword="false" />.</returns>
		// Token: 0x170000EB RID: 235
		// (get) Token: 0x06000342 RID: 834 RVA: 0x00005198 File Offset: 0x00003398
		public bool IsInvertible
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets the x translation value (the dx value, or the element in the third row and first column) of this <see cref="T:System.Drawing.Drawing2D.Matrix" />.</summary>
		/// <returns>The x translation value of this <see cref="T:System.Drawing.Drawing2D.Matrix" />.</returns>
		// Token: 0x170000EC RID: 236
		// (get) Token: 0x06000343 RID: 835 RVA: 0x000051B4 File Offset: 0x000033B4
		public float OffsetX
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
		}

		/// <summary>Gets the y translation value (the dy value, or the element in the third row and second column) of this <see cref="T:System.Drawing.Drawing2D.Matrix" />.</summary>
		/// <returns>The y translation value of this <see cref="T:System.Drawing.Drawing2D.Matrix" />.</returns>
		// Token: 0x170000ED RID: 237
		// (get) Token: 0x06000344 RID: 836 RVA: 0x000051D0 File Offset: 0x000033D0
		public float OffsetY
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
		}

		/// <summary>Creates an exact copy of this <see cref="T:System.Drawing.Drawing2D.Matrix" />.</summary>
		/// <returns>The <see cref="T:System.Drawing.Drawing2D.Matrix" /> that this method creates.</returns>
		// Token: 0x06000345 RID: 837 RVA: 0x00004667 File Offset: 0x00002867
		public Matrix Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Releases all resources used by this <see cref="T:System.Drawing.Drawing2D.Matrix" />.</summary>
		// Token: 0x06000346 RID: 838 RVA: 0x00004644 File Offset: 0x00002844
		public void Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Inverts this <see cref="T:System.Drawing.Drawing2D.Matrix" />, if it is invertible.</summary>
		// Token: 0x06000347 RID: 839 RVA: 0x00004644 File Offset: 0x00002844
		public void Invert()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Multiplies this <see cref="T:System.Drawing.Drawing2D.Matrix" /> by the matrix specified in the <paramref name="matrix" /> parameter, by prepending the specified <see cref="T:System.Drawing.Drawing2D.Matrix" />.</summary>
		/// <param name="matrix">The <see cref="T:System.Drawing.Drawing2D.Matrix" /> by which this <see cref="T:System.Drawing.Drawing2D.Matrix" /> is to be multiplied. </param>
		// Token: 0x06000348 RID: 840 RVA: 0x00004644 File Offset: 0x00002844
		public void Multiply(Matrix matrix)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Multiplies this <see cref="T:System.Drawing.Drawing2D.Matrix" /> by the matrix specified in the <paramref name="matrix" /> parameter, and in the order specified in the <paramref name="order" /> parameter.</summary>
		/// <param name="matrix">The <see cref="T:System.Drawing.Drawing2D.Matrix" /> by which this <see cref="T:System.Drawing.Drawing2D.Matrix" /> is to be multiplied. </param>
		/// <param name="order">The <see cref="T:System.Drawing.Drawing2D.MatrixOrder" /> that represents the order of the multiplication. </param>
		// Token: 0x06000349 RID: 841 RVA: 0x00004644 File Offset: 0x00002844
		public void Multiply(Matrix matrix, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Resets this <see cref="T:System.Drawing.Drawing2D.Matrix" /> to have the elements of the identity matrix.</summary>
		// Token: 0x0600034A RID: 842 RVA: 0x00004644 File Offset: 0x00002844
		public void Reset()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Prepend to this <see cref="T:System.Drawing.Drawing2D.Matrix" /> a clockwise rotation, around the origin and by the specified angle.</summary>
		/// <param name="angle">The angle of the rotation, in degrees. </param>
		// Token: 0x0600034B RID: 843 RVA: 0x00004644 File Offset: 0x00002844
		public void Rotate(float angle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Applies a clockwise rotation of an amount specified in the <paramref name="angle" /> parameter, around the origin (zero x and y coordinates) for this <see cref="T:System.Drawing.Drawing2D.Matrix" />.</summary>
		/// <param name="angle">The angle (extent) of the rotation, in degrees. </param>
		/// <param name="order">A <see cref="T:System.Drawing.Drawing2D.MatrixOrder" /> that specifies the order (append or prepend) in which the rotation is applied to this <see cref="T:System.Drawing.Drawing2D.Matrix" />. </param>
		// Token: 0x0600034C RID: 844 RVA: 0x00004644 File Offset: 0x00002844
		public void Rotate(float angle, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Applies a clockwise rotation to this <see cref="T:System.Drawing.Drawing2D.Matrix" /> around the point specified in the <paramref name="point" /> parameter, and by prepending the rotation.</summary>
		/// <param name="angle">The angle (extent) of the rotation, in degrees. </param>
		/// <param name="point">A <see cref="T:System.Drawing.PointF" /> that represents the center of the rotation. </param>
		// Token: 0x0600034D RID: 845 RVA: 0x00004644 File Offset: 0x00002844
		public void RotateAt(float angle, PointF point)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Applies a clockwise rotation about the specified point to this <see cref="T:System.Drawing.Drawing2D.Matrix" /> in the specified order.</summary>
		/// <param name="angle">The angle of the rotation, in degrees. </param>
		/// <param name="point">A <see cref="T:System.Drawing.PointF" /> that represents the center of the rotation. </param>
		/// <param name="order">A <see cref="T:System.Drawing.Drawing2D.MatrixOrder" /> that specifies the order (append or prepend) in which the rotation is applied. </param>
		// Token: 0x0600034E RID: 846 RVA: 0x00004644 File Offset: 0x00002844
		public void RotateAt(float angle, PointF point, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Applies the specified scale vector to this <see cref="T:System.Drawing.Drawing2D.Matrix" /> by prepending the scale vector.</summary>
		/// <param name="scaleX">The value by which to scale this <see cref="T:System.Drawing.Drawing2D.Matrix" /> in the x-axis direction. </param>
		/// <param name="scaleY">The value by which to scale this <see cref="T:System.Drawing.Drawing2D.Matrix" /> in the y-axis direction. </param>
		// Token: 0x0600034F RID: 847 RVA: 0x00004644 File Offset: 0x00002844
		public void Scale(float scaleX, float scaleY)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Applies the specified scale vector (<paramref name="scaleX" /> and <paramref name="scaleY" />) to this <see cref="T:System.Drawing.Drawing2D.Matrix" /> using the specified order.</summary>
		/// <param name="scaleX">The value by which to scale this <see cref="T:System.Drawing.Drawing2D.Matrix" /> in the x-axis direction. </param>
		/// <param name="scaleY">The value by which to scale this <see cref="T:System.Drawing.Drawing2D.Matrix" /> in the y-axis direction. </param>
		/// <param name="order">A <see cref="T:System.Drawing.Drawing2D.MatrixOrder" /> that specifies the order (append or prepend) in which the scale vector is applied to this <see cref="T:System.Drawing.Drawing2D.Matrix" />. </param>
		// Token: 0x06000350 RID: 848 RVA: 0x00004644 File Offset: 0x00002844
		public void Scale(float scaleX, float scaleY, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Applies the specified shear vector to this <see cref="T:System.Drawing.Drawing2D.Matrix" /> by prepending the shear transformation.</summary>
		/// <param name="shearX">The horizontal shear factor. </param>
		/// <param name="shearY">The vertical shear factor. </param>
		// Token: 0x06000351 RID: 849 RVA: 0x00004644 File Offset: 0x00002844
		public void Shear(float shearX, float shearY)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Applies the specified shear vector to this <see cref="T:System.Drawing.Drawing2D.Matrix" /> in the specified order.</summary>
		/// <param name="shearX">The horizontal shear factor. </param>
		/// <param name="shearY">The vertical shear factor. </param>
		/// <param name="order">A <see cref="T:System.Drawing.Drawing2D.MatrixOrder" /> that specifies the order (append or prepend) in which the shear is applied. </param>
		// Token: 0x06000352 RID: 850 RVA: 0x00004644 File Offset: 0x00002844
		public void Shear(float shearX, float shearY, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Applies the geometric transform represented by this <see cref="T:System.Drawing.Drawing2D.Matrix" /> to a specified array of points.</summary>
		/// <param name="pts">An array of <see cref="T:System.Drawing.PointF" /> structures that represents the points to transform. </param>
		// Token: 0x06000353 RID: 851 RVA: 0x00004644 File Offset: 0x00002844
		public void TransformPoints(PointF[] pts)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Applies the geometric transform represented by this <see cref="T:System.Drawing.Drawing2D.Matrix" /> to a specified array of points.</summary>
		/// <param name="pts">An array of <see cref="T:System.Drawing.Point" /> structures that represents the points to transform. </param>
		// Token: 0x06000354 RID: 852 RVA: 0x00004644 File Offset: 0x00002844
		public void TransformPoints(Point[] pts)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Multiplies each vector in an array by the matrix. The translation elements of this matrix (third row) are ignored.</summary>
		/// <param name="pts">An array of <see cref="T:System.Drawing.Point" /> structures that represents the points to transform. </param>
		// Token: 0x06000355 RID: 853 RVA: 0x00004644 File Offset: 0x00002844
		public void TransformVectors(PointF[] pts)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Applies only the scale and rotate components of this <see cref="T:System.Drawing.Drawing2D.Matrix" /> to the specified array of points.</summary>
		/// <param name="pts">An array of <see cref="T:System.Drawing.Point" /> structures that represents the points to transform. </param>
		// Token: 0x06000356 RID: 854 RVA: 0x00004644 File Offset: 0x00002844
		public void TransformVectors(Point[] pts)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Applies the specified translation vector (<paramref name="offsetX" /> and <paramref name="offsetY" />) to this <see cref="T:System.Drawing.Drawing2D.Matrix" /> by prepending the translation vector.</summary>
		/// <param name="offsetX">The x value by which to translate this <see cref="T:System.Drawing.Drawing2D.Matrix" />. </param>
		/// <param name="offsetY">The y value by which to translate this <see cref="T:System.Drawing.Drawing2D.Matrix" />. </param>
		// Token: 0x06000357 RID: 855 RVA: 0x00004644 File Offset: 0x00002844
		public void Translate(float offsetX, float offsetY)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Applies the specified translation vector to this <see cref="T:System.Drawing.Drawing2D.Matrix" /> in the specified order.</summary>
		/// <param name="offsetX">The x value by which to translate this <see cref="T:System.Drawing.Drawing2D.Matrix" />. </param>
		/// <param name="offsetY">The y value by which to translate this <see cref="T:System.Drawing.Drawing2D.Matrix" />. </param>
		/// <param name="order">A <see cref="T:System.Drawing.Drawing2D.MatrixOrder" /> that specifies the order (append or prepend) in which the translation is applied to this <see cref="T:System.Drawing.Drawing2D.Matrix" />. </param>
		// Token: 0x06000358 RID: 856 RVA: 0x00004644 File Offset: 0x00002844
		public void Translate(float offsetX, float offsetY, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Multiplies each vector in an array by the matrix. The translation elements of this matrix (third row) are ignored.</summary>
		/// <param name="pts">An array of <see cref="T:System.Drawing.Point" /> structures that represents the points to transform.</param>
		// Token: 0x06000359 RID: 857 RVA: 0x00004644 File Offset: 0x00002844
		public void VectorTransformPoints(Point[] pts)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
