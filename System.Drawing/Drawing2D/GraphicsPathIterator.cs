﻿using System;
using Unity;

namespace System.Drawing.Drawing2D
{
	/// <summary>Provides the ability to iterate through subpaths in a <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> and test the types of shapes contained in each subpath. This class cannot be inherited.</summary>
	// Token: 0x020000B6 RID: 182
	public sealed class GraphicsPathIterator : MarshalByRefObject, IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.GraphicsPathIterator" /> class with the specified <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> object.</summary>
		/// <param name="path">The <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> object for which this helper class is to be initialized. </param>
		// Token: 0x060008C6 RID: 2246 RVA: 0x00004644 File Offset: 0x00002844
		public GraphicsPathIterator(GraphicsPath path)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the number of points in the path.</summary>
		/// <returns>The number of points in the path.</returns>
		// Token: 0x17000387 RID: 903
		// (get) Token: 0x060008C7 RID: 2247 RVA: 0x00006F20 File Offset: 0x00005120
		public int Count
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the number of subpaths in the path.</summary>
		/// <returns>The number of subpaths in the path.</returns>
		// Token: 0x17000388 RID: 904
		// (get) Token: 0x060008C8 RID: 2248 RVA: 0x00006F3C File Offset: 0x0000513C
		public int SubpathCount
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Copies the <see cref="P:System.Drawing.Drawing2D.GraphicsPath.PathPoints" /> property and <see cref="P:System.Drawing.Drawing2D.GraphicsPath.PathTypes" /> property arrays of the associated <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> into the two specified arrays.</summary>
		/// <param name="points">Upon return, contains an array of <see cref="T:System.Drawing.PointF" /> structures that represents the points in the path. </param>
		/// <param name="types">Upon return, contains an array of bytes that represents the types of points in the path. </param>
		/// <param name="startIndex">Specifies the starting index of the arrays. </param>
		/// <param name="endIndex">Specifies the ending index of the arrays. </param>
		/// <returns>The number of points copied.</returns>
		// Token: 0x060008C9 RID: 2249 RVA: 0x00006F58 File Offset: 0x00005158
		public int CopyData(ref PointF[] points, ref byte[] types, int startIndex, int endIndex)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Releases all resources used by this <see cref="T:System.Drawing.Drawing2D.GraphicsPathIterator" /> object.</summary>
		// Token: 0x060008CA RID: 2250 RVA: 0x00004644 File Offset: 0x00002844
		public void Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Copies the <see cref="P:System.Drawing.Drawing2D.GraphicsPath.PathPoints" /> property and <see cref="P:System.Drawing.Drawing2D.GraphicsPath.PathTypes" /> property arrays of the associated <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> into the two specified arrays.</summary>
		/// <param name="points">Upon return, contains an array of <see cref="T:System.Drawing.PointF" /> structures that represents the points in the path. </param>
		/// <param name="types">Upon return, contains an array of bytes that represents the types of points in the path. </param>
		/// <returns>The number of points copied.</returns>
		// Token: 0x060008CB RID: 2251 RVA: 0x00006F74 File Offset: 0x00005174
		public int Enumerate(ref PointF[] points, ref byte[] types)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Indicates whether the path associated with this <see cref="T:System.Drawing.Drawing2D.GraphicsPathIterator" /> contains a curve.</summary>
		/// <returns>This method returns <see langword="true" /> if the current subpath contains a curve; otherwise, <see langword="false" />.</returns>
		// Token: 0x060008CC RID: 2252 RVA: 0x00006F90 File Offset: 0x00005190
		public bool HasCurve()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>This <see cref="T:System.Drawing.Drawing2D.GraphicsPathIterator" /> object has a <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> object associated with it. The <see cref="M:System.Drawing.Drawing2D.GraphicsPathIterator.NextMarker(System.Drawing.Drawing2D.GraphicsPath)" /> method increments the associated <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> to the next marker in its path and copies all the points contained between the current marker and the next marker (or end of path) to a second <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> object passed in to the parameter.</summary>
		/// <param name="path">The <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> object to which the points will be copied. </param>
		/// <returns>The number of points between this marker and the next.</returns>
		// Token: 0x060008CD RID: 2253 RVA: 0x00006FAC File Offset: 0x000051AC
		public int NextMarker(GraphicsPath path)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Increments the <see cref="T:System.Drawing.Drawing2D.GraphicsPathIterator" /> to the next marker in the path and returns the start and stop indexes by way of the [out] parameters.</summary>
		/// <param name="startIndex">[out] The integer reference supplied to this parameter receives the index of the point that starts a subpath. </param>
		/// <param name="endIndex">[out] The integer reference supplied to this parameter receives the index of the point that ends the subpath to which <paramref name="startIndex" /> points. </param>
		/// <returns>The number of points between this marker and the next.</returns>
		// Token: 0x060008CE RID: 2254 RVA: 0x00006FC8 File Offset: 0x000051C8
		public int NextMarker(out int startIndex, out int endIndex)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Gets the starting index and the ending index of the next group of data points that all have the same type.</summary>
		/// <param name="pathType">[out] Receives the point type shared by all points in the group. Possible types can be retrieved from the <see cref="T:System.Drawing.Drawing2D.PathPointType" /> enumeration. </param>
		/// <param name="startIndex">[out] Receives the starting index of the group of points. </param>
		/// <param name="endIndex">[out] Receives the ending index of the group of points. </param>
		/// <returns>This method returns the number of data points in the group. If there are no more groups in the path, this method returns 0.</returns>
		// Token: 0x060008CF RID: 2255 RVA: 0x00006FE4 File Offset: 0x000051E4
		public int NextPathType(out byte pathType, out int startIndex, out int endIndex)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Gets the next figure (subpath) from the associated path of this <see cref="T:System.Drawing.Drawing2D.GraphicsPathIterator" />.</summary>
		/// <param name="path">A <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> that is to have its data points set to match the data points of the retrieved figure (subpath) for this iterator. </param>
		/// <param name="isClosed">[out] Indicates whether the current subpath is closed. It is <see langword="true" /> if the if the figure is closed, otherwise it is <see langword="false" />. </param>
		/// <returns>The number of data points in the retrieved figure (subpath). If there are no more figures to retrieve, zero is returned.</returns>
		// Token: 0x060008D0 RID: 2256 RVA: 0x00007000 File Offset: 0x00005200
		public int NextSubpath(GraphicsPath path, out bool isClosed)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Moves the <see cref="T:System.Drawing.Drawing2D.GraphicsPathIterator" /> to the next subpath in the path. The start index and end index of the next subpath are contained in the [out] parameters.</summary>
		/// <param name="startIndex">[out] Receives the starting index of the next subpath. </param>
		/// <param name="endIndex">[out] Receives the ending index of the next subpath. </param>
		/// <param name="isClosed">[out] Indicates whether the subpath is closed. </param>
		/// <returns>The number of subpaths in the <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> object.</returns>
		// Token: 0x060008D1 RID: 2257 RVA: 0x0000701C File Offset: 0x0000521C
		public int NextSubpath(out int startIndex, out int endIndex, out bool isClosed)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Rewinds this <see cref="T:System.Drawing.Drawing2D.GraphicsPathIterator" /> to the beginning of its associated path.</summary>
		// Token: 0x060008D2 RID: 2258 RVA: 0x00004644 File Offset: 0x00002844
		public void Rewind()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
