﻿using System;

namespace System.Drawing.Drawing2D
{
	/// <summary>Specifies the order for matrix transform operations.</summary>
	// Token: 0x02000028 RID: 40
	public enum MatrixOrder
	{
		/// <summary>The new operation is applied after the old operation.</summary>
		// Token: 0x0400022B RID: 555
		Append = 1,
		/// <summary>The new operation is applied before the old operation.</summary>
		// Token: 0x0400022C RID: 556
		Prepend = 0
	}
}
