﻿using System;
using Unity;

namespace System.Drawing.Drawing2D
{
	/// <summary>Encapsulates a <see cref="T:System.Drawing.Brush" /> object that fills the interior of a <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> object with a gradient. This class cannot be inherited.</summary>
	// Token: 0x020000BB RID: 187
	public sealed class PathGradientBrush : Brush
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.PathGradientBrush" /> class with the specified path.</summary>
		/// <param name="path">The <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> that defines the area filled by this <see cref="T:System.Drawing.Drawing2D.PathGradientBrush" />. </param>
		// Token: 0x060008FC RID: 2300 RVA: 0x00004644 File Offset: 0x00002844
		public PathGradientBrush(GraphicsPath path)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.PathGradientBrush" /> class with the specified points.</summary>
		/// <param name="points">An array of <see cref="T:System.Drawing.PointF" /> structures that represents the points that make up the vertices of the path. </param>
		// Token: 0x060008FD RID: 2301 RVA: 0x00004644 File Offset: 0x00002844
		public PathGradientBrush(PointF[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.PathGradientBrush" /> class with the specified points and wrap mode.</summary>
		/// <param name="points">An array of <see cref="T:System.Drawing.PointF" /> structures that represents the points that make up the vertices of the path. </param>
		/// <param name="wrapMode">A <see cref="T:System.Drawing.Drawing2D.WrapMode" /> that specifies how fills drawn with this <see cref="T:System.Drawing.Drawing2D.PathGradientBrush" /> are tiled. </param>
		// Token: 0x060008FE RID: 2302 RVA: 0x00004644 File Offset: 0x00002844
		public PathGradientBrush(PointF[] points, WrapMode wrapMode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.PathGradientBrush" /> class with the specified points.</summary>
		/// <param name="points">An array of <see cref="T:System.Drawing.Point" /> structures that represents the points that make up the vertices of the path. </param>
		// Token: 0x060008FF RID: 2303 RVA: 0x00004644 File Offset: 0x00002844
		public PathGradientBrush(Point[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.PathGradientBrush" /> class with the specified points and wrap mode.</summary>
		/// <param name="points">An array of <see cref="T:System.Drawing.Point" /> structures that represents the points that make up the vertices of the path. </param>
		/// <param name="wrapMode">A <see cref="T:System.Drawing.Drawing2D.WrapMode" /> that specifies how fills drawn with this <see cref="T:System.Drawing.Drawing2D.PathGradientBrush" /> are tiled. </param>
		// Token: 0x06000900 RID: 2304 RVA: 0x00004644 File Offset: 0x00002844
		public PathGradientBrush(Point[] points, WrapMode wrapMode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets a <see cref="T:System.Drawing.Drawing2D.Blend" /> that specifies positions and factors that define a custom falloff for the gradient.</summary>
		/// <returns>A <see cref="T:System.Drawing.Drawing2D.Blend" /> that represents a custom falloff for the gradient.</returns>
		// Token: 0x17000393 RID: 915
		// (get) Token: 0x06000901 RID: 2305 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000902 RID: 2306 RVA: 0x00004644 File Offset: 0x00002844
		public Blend Blend
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the color at the center of the path gradient.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that represents the color at the center of the path gradient.</returns>
		// Token: 0x17000394 RID: 916
		// (get) Token: 0x06000903 RID: 2307 RVA: 0x000070E0 File Offset: 0x000052E0
		// (set) Token: 0x06000904 RID: 2308 RVA: 0x00004644 File Offset: 0x00002844
		public Color CenterColor
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the center point of the path gradient.</summary>
		/// <returns>A <see cref="T:System.Drawing.PointF" /> that represents the center point of the path gradient.</returns>
		// Token: 0x17000395 RID: 917
		// (get) Token: 0x06000905 RID: 2309 RVA: 0x000070FC File Offset: 0x000052FC
		// (set) Token: 0x06000906 RID: 2310 RVA: 0x00004644 File Offset: 0x00002844
		public PointF CenterPoint
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(PointF);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the focus point for the gradient falloff.</summary>
		/// <returns>A <see cref="T:System.Drawing.PointF" /> that represents the focus point for the gradient falloff.</returns>
		// Token: 0x17000396 RID: 918
		// (get) Token: 0x06000907 RID: 2311 RVA: 0x00007118 File Offset: 0x00005318
		// (set) Token: 0x06000908 RID: 2312 RVA: 0x00004644 File Offset: 0x00002844
		public PointF FocusScales
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(PointF);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Drawing.Drawing2D.ColorBlend" /> that defines a multicolor linear gradient.</summary>
		/// <returns>A <see cref="T:System.Drawing.Drawing2D.ColorBlend" /> that defines a multicolor linear gradient.</returns>
		// Token: 0x17000397 RID: 919
		// (get) Token: 0x06000909 RID: 2313 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x0600090A RID: 2314 RVA: 0x00004644 File Offset: 0x00002844
		public ColorBlend InterpolationColors
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a bounding rectangle for this <see cref="T:System.Drawing.Drawing2D.PathGradientBrush" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.RectangleF" /> that represents a rectangular region that bounds the path this <see cref="T:System.Drawing.Drawing2D.PathGradientBrush" /> fills.</returns>
		// Token: 0x17000398 RID: 920
		// (get) Token: 0x0600090B RID: 2315 RVA: 0x00007134 File Offset: 0x00005334
		public RectangleF Rectangle
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(RectangleF);
			}
		}

		/// <summary>Gets or sets an array of colors that correspond to the points in the path this <see cref="T:System.Drawing.Drawing2D.PathGradientBrush" /> fills.</summary>
		/// <returns>An array of <see cref="T:System.Drawing.Color" /> structures that represents the colors associated with each point in the path this <see cref="T:System.Drawing.Drawing2D.PathGradientBrush" /> fills.</returns>
		// Token: 0x17000399 RID: 921
		// (get) Token: 0x0600090C RID: 2316 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x0600090D RID: 2317 RVA: 0x00004644 File Offset: 0x00002844
		public Color[] SurroundColors
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a copy of the <see cref="T:System.Drawing.Drawing2D.Matrix" /> that defines a local geometric transform for this <see cref="T:System.Drawing.Drawing2D.PathGradientBrush" />.</summary>
		/// <returns>A copy of the <see cref="T:System.Drawing.Drawing2D.Matrix" /> that defines a geometric transform that applies only to fills drawn with this <see cref="T:System.Drawing.Drawing2D.PathGradientBrush" />.</returns>
		// Token: 0x1700039A RID: 922
		// (get) Token: 0x0600090E RID: 2318 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x0600090F RID: 2319 RVA: 0x00004644 File Offset: 0x00002844
		public Matrix Transform
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Drawing.Drawing2D.WrapMode" /> that indicates the wrap mode for this <see cref="T:System.Drawing.Drawing2D.PathGradientBrush" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.Drawing2D.WrapMode" /> that specifies how fills drawn with this <see cref="T:System.Drawing.Drawing2D.PathGradientBrush" /> are tiled.</returns>
		// Token: 0x1700039B RID: 923
		// (get) Token: 0x06000910 RID: 2320 RVA: 0x00007150 File Offset: 0x00005350
		// (set) Token: 0x06000911 RID: 2321 RVA: 0x00004644 File Offset: 0x00002844
		public WrapMode WrapMode
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return WrapMode.Tile;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Creates an exact copy of this <see cref="T:System.Drawing.Drawing2D.PathGradientBrush" />.</summary>
		/// <returns>The <see cref="T:System.Drawing.Drawing2D.PathGradientBrush" /> this method creates, cast as an object.</returns>
		// Token: 0x06000912 RID: 2322 RVA: 0x00004667 File Offset: 0x00002867
		public override object Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Updates the brush's transformation matrix with the product of brush's transformation matrix multiplied by another matrix.</summary>
		/// <param name="matrix">The <see cref="T:System.Drawing.Drawing2D.Matrix" /> that will be multiplied by the brush's current transformation matrix. </param>
		// Token: 0x06000913 RID: 2323 RVA: 0x00004644 File Offset: 0x00002844
		public void MultiplyTransform(Matrix matrix)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates the brush's transformation matrix with the product of the brush's transformation matrix multiplied by another matrix.</summary>
		/// <param name="matrix">The <see cref="T:System.Drawing.Drawing2D.Matrix" /> that will be multiplied by the brush's current transformation matrix. </param>
		/// <param name="order">A <see cref="T:System.Drawing.Drawing2D.MatrixOrder" /> that specifies in which order to multiply the two matrices. </param>
		// Token: 0x06000914 RID: 2324 RVA: 0x00004644 File Offset: 0x00002844
		public void MultiplyTransform(Matrix matrix, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Resets the <see cref="P:System.Drawing.Drawing2D.PathGradientBrush.Transform" /> property to identity.</summary>
		// Token: 0x06000915 RID: 2325 RVA: 0x00004644 File Offset: 0x00002844
		public void ResetTransform()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Rotates the local geometric transform by the specified amount. This method prepends the rotation to the transform.</summary>
		/// <param name="angle">The angle (extent) of rotation. </param>
		// Token: 0x06000916 RID: 2326 RVA: 0x00004644 File Offset: 0x00002844
		public void RotateTransform(float angle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Rotates the local geometric transform by the specified amount in the specified order.</summary>
		/// <param name="angle">The angle (extent) of rotation. </param>
		/// <param name="order">A <see cref="T:System.Drawing.Drawing2D.MatrixOrder" /> that specifies whether to append or prepend the rotation matrix. </param>
		// Token: 0x06000917 RID: 2327 RVA: 0x00004644 File Offset: 0x00002844
		public void RotateTransform(float angle, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Scales the local geometric transform by the specified amounts. This method prepends the scaling matrix to the transform.</summary>
		/// <param name="sx">The transform scale factor in the x-axis direction. </param>
		/// <param name="sy">The transform scale factor in the y-axis direction. </param>
		// Token: 0x06000918 RID: 2328 RVA: 0x00004644 File Offset: 0x00002844
		public void ScaleTransform(float sx, float sy)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Scales the local geometric transform by the specified amounts in the specified order.</summary>
		/// <param name="sx">The transform scale factor in the x-axis direction. </param>
		/// <param name="sy">The transform scale factor in the y-axis direction. </param>
		/// <param name="order">A <see cref="T:System.Drawing.Drawing2D.MatrixOrder" /> that specifies whether to append or prepend the scaling matrix. </param>
		// Token: 0x06000919 RID: 2329 RVA: 0x00004644 File Offset: 0x00002844
		public void ScaleTransform(float sx, float sy, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a gradient with a center color and a linear falloff to one surrounding color.</summary>
		/// <param name="focus">A value from 0 through 1 that specifies where, along any radial from the center of the path to the path's boundary, the center color will be at its highest intensity. A value of 1 (the default) places the highest intensity at the center of the path. </param>
		// Token: 0x0600091A RID: 2330 RVA: 0x00004644 File Offset: 0x00002844
		public void SetBlendTriangularShape(float focus)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a gradient with a center color and a linear falloff to each surrounding color.</summary>
		/// <param name="focus">A value from 0 through 1 that specifies where, along any radial from the center of the path to the path's boundary, the center color will be at its highest intensity. A value of 1 (the default) places the highest intensity at the center of the path. </param>
		/// <param name="scale">A value from 0 through 1 that specifies the maximum intensity of the center color that gets blended with the boundary color. A value of 1 causes the highest possible intensity of the center color, and it is the default value. </param>
		// Token: 0x0600091B RID: 2331 RVA: 0x00004644 File Offset: 0x00002844
		public void SetBlendTriangularShape(float focus, float scale)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a gradient brush that changes color starting from the center of the path outward to the path's boundary. The transition from one color to another is based on a bell-shaped curve.</summary>
		/// <param name="focus">A value from 0 through 1 that specifies where, along any radial from the center of the path to the path's boundary, the center color will be at its highest intensity. A value of 1 (the default) places the highest intensity at the center of the path. </param>
		// Token: 0x0600091C RID: 2332 RVA: 0x00004644 File Offset: 0x00002844
		public void SetSigmaBellShape(float focus)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a gradient brush that changes color starting from the center of the path outward to the path's boundary. The transition from one color to another is based on a bell-shaped curve.</summary>
		/// <param name="focus">A value from 0 through 1 that specifies where, along any radial from the center of the path to the path's boundary, the center color will be at its highest intensity. A value of 1 (the default) places the highest intensity at the center of the path. </param>
		/// <param name="scale">A value from 0 through 1 that specifies the maximum intensity of the center color that gets blended with the boundary color. A value of 1 causes the highest possible intensity of the center color, and it is the default value. </param>
		// Token: 0x0600091D RID: 2333 RVA: 0x00004644 File Offset: 0x00002844
		public void SetSigmaBellShape(float focus, float scale)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Applies the specified translation to the local geometric transform. This method prepends the translation to the transform.</summary>
		/// <param name="dx">The value of the translation in x. </param>
		/// <param name="dy">The value of the translation in y. </param>
		// Token: 0x0600091E RID: 2334 RVA: 0x00004644 File Offset: 0x00002844
		public void TranslateTransform(float dx, float dy)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Applies the specified translation to the local geometric transform in the specified order.</summary>
		/// <param name="dx">The value of the translation in x. </param>
		/// <param name="dy">The value of the translation in y. </param>
		/// <param name="order">The order (prepend or append) in which to apply the translation. </param>
		// Token: 0x0600091F RID: 2335 RVA: 0x00004644 File Offset: 0x00002844
		public void TranslateTransform(float dx, float dy, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
