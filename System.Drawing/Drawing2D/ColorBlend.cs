﻿using System;
using Unity;

namespace System.Drawing.Drawing2D
{
	/// <summary>Defines arrays of colors and positions used for interpolating color blending in a multicolor gradient. This class cannot be inherited.</summary>
	// Token: 0x020000B5 RID: 181
	public sealed class ColorBlend
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.ColorBlend" /> class.</summary>
		// Token: 0x060008C0 RID: 2240 RVA: 0x00004644 File Offset: 0x00002844
		public ColorBlend()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.ColorBlend" /> class with the specified number of colors and positions.</summary>
		/// <param name="count">The number of colors and positions in this <see cref="T:System.Drawing.Drawing2D.ColorBlend" />. </param>
		// Token: 0x060008C1 RID: 2241 RVA: 0x00004644 File Offset: 0x00002844
		public ColorBlend(int count)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets an array of colors that represents the colors to use at corresponding positions along a gradient.</summary>
		/// <returns>An array of <see cref="T:System.Drawing.Color" /> structures that represents the colors to use at corresponding positions along a gradient.</returns>
		// Token: 0x17000385 RID: 901
		// (get) Token: 0x060008C2 RID: 2242 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x060008C3 RID: 2243 RVA: 0x00004644 File Offset: 0x00002844
		public Color[] Colors
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the positions along a gradient line.</summary>
		/// <returns>An array of values that specify percentages of distance along the gradient line.</returns>
		// Token: 0x17000386 RID: 902
		// (get) Token: 0x060008C4 RID: 2244 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x060008C5 RID: 2245 RVA: 0x00004644 File Offset: 0x00002844
		public float[] Positions
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
