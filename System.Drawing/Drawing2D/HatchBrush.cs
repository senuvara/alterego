﻿using System;
using Unity;

namespace System.Drawing.Drawing2D
{
	/// <summary>Defines a rectangular brush with a hatch style, a foreground color, and a background color. This class cannot be inherited.</summary>
	// Token: 0x020000B7 RID: 183
	public sealed class HatchBrush : Brush
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.HatchBrush" /> class with the specified <see cref="T:System.Drawing.Drawing2D.HatchStyle" /> enumeration and foreground color.</summary>
		/// <param name="hatchstyle">One of the <see cref="T:System.Drawing.Drawing2D.HatchStyle" /> values that represents the pattern drawn by this <see cref="T:System.Drawing.Drawing2D.HatchBrush" />. </param>
		/// <param name="foreColor">The <see cref="T:System.Drawing.Color" /> structure that represents the color of lines drawn by this <see cref="T:System.Drawing.Drawing2D.HatchBrush" />. </param>
		// Token: 0x060008D3 RID: 2259 RVA: 0x00004644 File Offset: 0x00002844
		public HatchBrush(HatchStyle hatchstyle, Color foreColor)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.HatchBrush" /> class with the specified <see cref="T:System.Drawing.Drawing2D.HatchStyle" /> enumeration, foreground color, and background color.</summary>
		/// <param name="hatchstyle">One of the <see cref="T:System.Drawing.Drawing2D.HatchStyle" /> values that represents the pattern drawn by this <see cref="T:System.Drawing.Drawing2D.HatchBrush" />. </param>
		/// <param name="foreColor">The <see cref="T:System.Drawing.Color" /> structure that represents the color of lines drawn by this <see cref="T:System.Drawing.Drawing2D.HatchBrush" />. </param>
		/// <param name="backColor">The <see cref="T:System.Drawing.Color" /> structure that represents the color of spaces between the lines drawn by this <see cref="T:System.Drawing.Drawing2D.HatchBrush" />. </param>
		// Token: 0x060008D4 RID: 2260 RVA: 0x00004644 File Offset: 0x00002844
		public HatchBrush(HatchStyle hatchstyle, Color foreColor, Color backColor)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the color of spaces between the hatch lines drawn by this <see cref="T:System.Drawing.Drawing2D.HatchBrush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> structure that represents the background color for this <see cref="T:System.Drawing.Drawing2D.HatchBrush" />.</returns>
		// Token: 0x17000389 RID: 905
		// (get) Token: 0x060008D5 RID: 2261 RVA: 0x00007038 File Offset: 0x00005238
		public Color BackgroundColor
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets the color of hatch lines drawn by this <see cref="T:System.Drawing.Drawing2D.HatchBrush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> structure that represents the foreground color for this <see cref="T:System.Drawing.Drawing2D.HatchBrush" />.</returns>
		// Token: 0x1700038A RID: 906
		// (get) Token: 0x060008D6 RID: 2262 RVA: 0x00007054 File Offset: 0x00005254
		public Color ForegroundColor
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets the hatch style of this <see cref="T:System.Drawing.Drawing2D.HatchBrush" /> object.</summary>
		/// <returns>One of the <see cref="T:System.Drawing.Drawing2D.HatchStyle" /> values that represents the pattern of this <see cref="T:System.Drawing.Drawing2D.HatchBrush" />.</returns>
		// Token: 0x1700038B RID: 907
		// (get) Token: 0x060008D7 RID: 2263 RVA: 0x00007070 File Offset: 0x00005270
		public HatchStyle HatchStyle
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return HatchStyle.Horizontal;
			}
		}

		/// <summary>Creates an exact copy of this <see cref="T:System.Drawing.Drawing2D.HatchBrush" /> object.</summary>
		/// <returns>The <see cref="T:System.Drawing.Drawing2D.HatchBrush" /> this method creates, cast as an object.</returns>
		// Token: 0x060008D8 RID: 2264 RVA: 0x00004667 File Offset: 0x00002867
		public override object Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
