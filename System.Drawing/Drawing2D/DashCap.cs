﻿using System;

namespace System.Drawing.Drawing2D
{
	/// <summary>Specifies the type of graphic shape to use on both ends of each dash in a dashed line.</summary>
	// Token: 0x0200002F RID: 47
	public enum DashCap
	{
		/// <summary>Specifies a square cap that squares off both ends of each dash.</summary>
		// Token: 0x04000245 RID: 581
		Flat,
		/// <summary>Specifies a circular cap that rounds off both ends of each dash.</summary>
		// Token: 0x04000246 RID: 582
		Round = 2,
		/// <summary>Specifies a triangular cap that points both ends of each dash.</summary>
		// Token: 0x04000247 RID: 583
		Triangle
	}
}
