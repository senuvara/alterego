﻿using System;

namespace System.Drawing.Drawing2D
{
	/// <summary>Specifies the type of warp transformation applied in a <see cref="Overload:System.Drawing.Drawing2D.GraphicsPath.Warp" /> method.</summary>
	// Token: 0x02000032 RID: 50
	public enum WarpMode
	{
		/// <summary>Specifies a bilinear warp.</summary>
		// Token: 0x04000256 RID: 598
		Bilinear = 1,
		/// <summary>Specifies a perspective warp.</summary>
		// Token: 0x04000257 RID: 599
		Perspective = 0
	}
}
