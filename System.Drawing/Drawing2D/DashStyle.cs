﻿using System;

namespace System.Drawing.Drawing2D
{
	/// <summary>Specifies the style of dashed lines drawn with a <see cref="T:System.Drawing.Pen" /> object.</summary>
	// Token: 0x02000030 RID: 48
	public enum DashStyle
	{
		/// <summary>Specifies a user-defined custom dash style.</summary>
		// Token: 0x04000249 RID: 585
		Custom = 5,
		/// <summary>Specifies a line consisting of dashes.</summary>
		// Token: 0x0400024A RID: 586
		Dash = 1,
		/// <summary>Specifies a line consisting of a repeating pattern of dash-dot.</summary>
		// Token: 0x0400024B RID: 587
		DashDot = 3,
		/// <summary>Specifies a line consisting of a repeating pattern of dash-dot-dot.</summary>
		// Token: 0x0400024C RID: 588
		DashDotDot,
		/// <summary>Specifies a line consisting of dots.</summary>
		// Token: 0x0400024D RID: 589
		Dot = 2,
		/// <summary>Specifies a solid line.</summary>
		// Token: 0x0400024E RID: 590
		Solid = 0
	}
}
