﻿using System;

namespace System.Drawing.Drawing2D
{
	/// <summary>Specifies the quality level to use during compositing.</summary>
	// Token: 0x02000035 RID: 53
	public enum CompositingQuality
	{
		/// <summary>Assume linear values.</summary>
		// Token: 0x0400025C RID: 604
		AssumeLinear = 4,
		/// <summary>Default quality.</summary>
		// Token: 0x0400025D RID: 605
		Default = 0,
		/// <summary>Gamma correction is used.</summary>
		// Token: 0x0400025E RID: 606
		GammaCorrected = 3,
		/// <summary>High quality, low speed compositing.</summary>
		// Token: 0x0400025F RID: 607
		HighQuality = 2,
		/// <summary>High speed, low quality.</summary>
		// Token: 0x04000260 RID: 608
		HighSpeed = 1,
		/// <summary>Invalid quality.</summary>
		// Token: 0x04000261 RID: 609
		Invalid = -1
	}
}
