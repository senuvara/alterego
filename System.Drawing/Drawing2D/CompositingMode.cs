﻿using System;

namespace System.Drawing.Drawing2D
{
	/// <summary>Specifies how the source colors are combined with the background colors.</summary>
	// Token: 0x02000034 RID: 52
	public enum CompositingMode
	{
		/// <summary>Specifies that when a color is rendered, it overwrites the background color.</summary>
		// Token: 0x04000259 RID: 601
		SourceCopy = 1,
		/// <summary>Specifies that when a color is rendered, it is blended with the background color. The blend is determined by the alpha component of the color being rendered.</summary>
		// Token: 0x0400025A RID: 602
		SourceOver = 0
	}
}
