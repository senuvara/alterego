﻿using System;

namespace System.Drawing.Drawing2D
{
	/// <summary>Specifies the available cap styles with which a <see cref="T:System.Drawing.Pen" /> object can end a line.</summary>
	// Token: 0x0200002D RID: 45
	public enum LineCap
	{
		/// <summary>Specifies a mask used to check whether a line cap is an anchor cap.</summary>
		// Token: 0x04000234 RID: 564
		AnchorMask = 240,
		/// <summary>Specifies an arrow-shaped anchor cap.</summary>
		// Token: 0x04000235 RID: 565
		ArrowAnchor = 20,
		/// <summary>Specifies a custom line cap.</summary>
		// Token: 0x04000236 RID: 566
		Custom = 255,
		/// <summary>Specifies a diamond anchor cap.</summary>
		// Token: 0x04000237 RID: 567
		DiamondAnchor = 19,
		/// <summary>Specifies a flat line cap.</summary>
		// Token: 0x04000238 RID: 568
		Flat = 0,
		/// <summary>Specifies no anchor.</summary>
		// Token: 0x04000239 RID: 569
		NoAnchor = 16,
		/// <summary>Specifies a round line cap.</summary>
		// Token: 0x0400023A RID: 570
		Round = 2,
		/// <summary>Specifies a round anchor cap.</summary>
		// Token: 0x0400023B RID: 571
		RoundAnchor = 18,
		/// <summary>Specifies a square line cap.</summary>
		// Token: 0x0400023C RID: 572
		Square = 1,
		/// <summary>Specifies a square anchor line cap.</summary>
		// Token: 0x0400023D RID: 573
		SquareAnchor = 17,
		/// <summary>Specifies a triangular line cap.</summary>
		// Token: 0x0400023E RID: 574
		Triangle = 3
	}
}
