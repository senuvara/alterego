﻿using System;

namespace System.Drawing.Drawing2D
{
	/// <summary>Specifies how to join consecutive line or curve segments in a figure (subpath) contained in a <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> object.</summary>
	// Token: 0x0200002E RID: 46
	public enum LineJoin
	{
		/// <summary>Specifies a beveled join. This produces a diagonal corner.</summary>
		// Token: 0x04000240 RID: 576
		Bevel = 1,
		/// <summary>Specifies a mitered join. This produces a sharp corner or a clipped corner, depending on whether the length of the miter exceeds the miter limit.</summary>
		// Token: 0x04000241 RID: 577
		Miter = 0,
		/// <summary>Specifies a mitered join. This produces a sharp corner or a beveled corner, depending on whether the length of the miter exceeds the miter limit.</summary>
		// Token: 0x04000242 RID: 578
		MiterClipped = 3,
		/// <summary>Specifies a circular join. This produces a smooth, circular arc between the lines.</summary>
		// Token: 0x04000243 RID: 579
		Round = 2
	}
}
