﻿using System;
using Unity;

namespace System.Drawing.Drawing2D
{
	/// <summary>Represents an adjustable arrow-shaped line cap. This class cannot be inherited.</summary>
	// Token: 0x020000B3 RID: 179
	public sealed class AdjustableArrowCap : CustomLineCap
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.AdjustableArrowCap" /> class with the specified width and height. The arrow end caps created with this constructor are always filled.</summary>
		/// <param name="width">The width of the arrow. </param>
		/// <param name="height">The height of the arrow. </param>
		// Token: 0x060008B0 RID: 2224 RVA: 0x00004644 File Offset: 0x00002844
		public AdjustableArrowCap(float width, float height)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.AdjustableArrowCap" /> class with the specified width, height, and fill property. Whether an arrow end cap is filled depends on the argument passed to the <paramref name="isFilled" /> parameter.</summary>
		/// <param name="width">The width of the arrow. </param>
		/// <param name="height">The height of the arrow. </param>
		/// <param name="isFilled">
		///       <see langword="true" /> to fill the arrow cap; otherwise, <see langword="false" />. </param>
		// Token: 0x060008B1 RID: 2225 RVA: 0x00004644 File Offset: 0x00002844
		public AdjustableArrowCap(float width, float height, bool isFilled)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets whether the arrow cap is filled.</summary>
		/// <returns>This property is <see langword="true" /> if the arrow cap is filled; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700037F RID: 895
		// (get) Token: 0x060008B2 RID: 2226 RVA: 0x00006EB0 File Offset: 0x000050B0
		// (set) Token: 0x060008B3 RID: 2227 RVA: 0x00004644 File Offset: 0x00002844
		public bool Filled
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the height of the arrow cap.</summary>
		/// <returns>The height of the arrow cap.</returns>
		// Token: 0x17000380 RID: 896
		// (get) Token: 0x060008B4 RID: 2228 RVA: 0x00006ECC File Offset: 0x000050CC
		// (set) Token: 0x060008B5 RID: 2229 RVA: 0x00004644 File Offset: 0x00002844
		public float Height
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the number of units between the outline of the arrow cap and the fill.</summary>
		/// <returns>The number of units between the outline of the arrow cap and the fill of the arrow cap.</returns>
		// Token: 0x17000381 RID: 897
		// (get) Token: 0x060008B6 RID: 2230 RVA: 0x00006EE8 File Offset: 0x000050E8
		// (set) Token: 0x060008B7 RID: 2231 RVA: 0x00004644 File Offset: 0x00002844
		public float MiddleInset
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the width of the arrow cap.</summary>
		/// <returns>The width, in units, of the arrow cap.</returns>
		// Token: 0x17000382 RID: 898
		// (get) Token: 0x060008B8 RID: 2232 RVA: 0x00006F04 File Offset: 0x00005104
		// (set) Token: 0x060008B9 RID: 2233 RVA: 0x00004644 File Offset: 0x00002844
		public float Width
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
