﻿using System;

namespace System.Drawing.Drawing2D
{
	/// <summary>Specifies how a texture or gradient is tiled when it is smaller than the area being filled.</summary>
	// Token: 0x02000058 RID: 88
	public enum WrapMode
	{
		/// <summary>The texture or gradient is not tiled.</summary>
		// Token: 0x040002F9 RID: 761
		Clamp = 4,
		/// <summary>Tiles the gradient or texture.</summary>
		// Token: 0x040002FA RID: 762
		Tile = 0,
		/// <summary>Reverses the texture or gradient horizontally and then tiles the texture or gradient.</summary>
		// Token: 0x040002FB RID: 763
		TileFlipX,
		/// <summary>Reverses the texture or gradient horizontally and vertically and then tiles the texture or gradient.</summary>
		// Token: 0x040002FC RID: 764
		TileFlipXY = 3,
		/// <summary>Reverses the texture or gradient vertically and then tiles the texture or gradient.</summary>
		// Token: 0x040002FD RID: 765
		TileFlipY = 2
	}
}
