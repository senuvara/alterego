﻿using System;

namespace System.Drawing.Drawing2D
{
	/// <summary>Specifies whether smoothing (antialiasing) is applied to lines and curves and the edges of filled areas.</summary>
	// Token: 0x02000039 RID: 57
	public enum SmoothingMode
	{
		/// <summary>Specifies antialiased rendering.</summary>
		// Token: 0x0400027C RID: 636
		AntiAlias = 4,
		/// <summary>Specifies no antialiasing.</summary>
		// Token: 0x0400027D RID: 637
		Default = 0,
		/// <summary>Specifies antialiased rendering.</summary>
		// Token: 0x0400027E RID: 638
		HighQuality = 2,
		/// <summary>Specifies no antialiasing.</summary>
		// Token: 0x0400027F RID: 639
		HighSpeed = 1,
		/// <summary>Specifies an invalid mode.</summary>
		// Token: 0x04000280 RID: 640
		Invalid = -1,
		/// <summary>Specifies no antialiasing.</summary>
		// Token: 0x04000281 RID: 641
		None = 3
	}
}
