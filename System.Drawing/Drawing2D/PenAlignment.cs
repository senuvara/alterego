﻿using System;

namespace System.Drawing.Drawing2D
{
	/// <summary>Specifies the alignment of a <see cref="T:System.Drawing.Pen" /> object in relation to the theoretical, zero-width line.</summary>
	// Token: 0x0200002B RID: 43
	public enum PenAlignment
	{
		/// <summary>Specifies that the <see cref="T:System.Drawing.Pen" /> object is centered over the theoretical line.</summary>
		// Token: 0x0400022E RID: 558
		Center,
		/// <summary>Specifies that the <see cref="T:System.Drawing.Pen" /> is positioned on the inside of the theoretical line.</summary>
		// Token: 0x0400022F RID: 559
		Inset,
		/// <summary>Specifies the <see cref="T:System.Drawing.Pen" /> is positioned to the left of the theoretical line.</summary>
		// Token: 0x04000230 RID: 560
		Left = 3,
		/// <summary>Specifies the <see cref="T:System.Drawing.Pen" /> is positioned on the outside of the theoretical line.</summary>
		// Token: 0x04000231 RID: 561
		Outset = 2,
		/// <summary>Specifies the <see cref="T:System.Drawing.Pen" /> is positioned to the right of the theoretical line.</summary>
		// Token: 0x04000232 RID: 562
		Right = 4
	}
}
