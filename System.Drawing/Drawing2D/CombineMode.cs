﻿using System;

namespace System.Drawing.Drawing2D
{
	/// <summary>Specifies how different clipping regions can be combined.</summary>
	// Token: 0x02000066 RID: 102
	public enum CombineMode
	{
		/// <summary>Specifies that the existing region is replaced by the result of the existing region being removed from the new region. Said differently, the existing region is excluded from the new region.</summary>
		// Token: 0x04000314 RID: 788
		Complement = 5,
		/// <summary>Specifies that the existing region is replaced by the result of the new region being removed from the existing region. Said differently, the new region is excluded from the existing region.</summary>
		// Token: 0x04000315 RID: 789
		Exclude = 4,
		/// <summary>Two clipping regions are combined by taking their intersection.</summary>
		// Token: 0x04000316 RID: 790
		Intersect = 1,
		/// <summary>One clipping region is replaced by another.</summary>
		// Token: 0x04000317 RID: 791
		Replace = 0,
		/// <summary>Two clipping regions are combined by taking the union of both.</summary>
		// Token: 0x04000318 RID: 792
		Union = 2,
		/// <summary>Two clipping regions are combined by taking only the areas enclosed by one or the other region, but not both.</summary>
		// Token: 0x04000319 RID: 793
		Xor
	}
}
