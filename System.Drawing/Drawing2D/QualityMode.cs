﻿using System;

namespace System.Drawing.Drawing2D
{
	/// <summary>Specifies the overall quality when rendering GDI+ objects.</summary>
	// Token: 0x020000BD RID: 189
	public enum QualityMode
	{
		/// <summary>Specifies the default mode.</summary>
		// Token: 0x0400044F RID: 1103
		Default,
		/// <summary>Specifies high quality, low speed rendering.</summary>
		// Token: 0x04000450 RID: 1104
		High = 2,
		/// <summary>Specifies an invalid mode.</summary>
		// Token: 0x04000451 RID: 1105
		Invalid = -1,
		/// <summary>Specifies low quality, high speed rendering.</summary>
		// Token: 0x04000452 RID: 1106
		Low = 1
	}
}
