﻿using System;
using Unity;

namespace System.Drawing.Drawing2D
{
	/// <summary>Contains the graphical data that makes up a <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> object. This class cannot be inherited.</summary>
	// Token: 0x0200001B RID: 27
	public sealed class PathData
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.PathData" /> class.</summary>
		// Token: 0x06000304 RID: 772 RVA: 0x00004644 File Offset: 0x00002844
		public PathData()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets an array of <see cref="T:System.Drawing.PointF" /> structures that represents the points through which the path is constructed.</summary>
		/// <returns>An array of <see cref="T:System.Drawing.PointF" /> objects that represents the points through which the path is constructed.</returns>
		// Token: 0x170000D6 RID: 214
		// (get) Token: 0x06000305 RID: 773 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000306 RID: 774 RVA: 0x00004644 File Offset: 0x00002844
		public PointF[] Points
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the types of the corresponding points in the path.</summary>
		/// <returns>An array of bytes that specify the types of the corresponding points in the path.</returns>
		// Token: 0x170000D7 RID: 215
		// (get) Token: 0x06000307 RID: 775 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000308 RID: 776 RVA: 0x00004644 File Offset: 0x00002844
		public byte[] Types
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
