﻿using System;

namespace System.Drawing.Drawing2D
{
	/// <summary>Specifies the type of point in a <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> object.</summary>
	// Token: 0x020000BC RID: 188
	public enum PathPointType
	{
		/// <summary>A default Bézier curve.</summary>
		// Token: 0x04000446 RID: 1094
		Bezier = 3,
		/// <summary>A cubic Bézier curve.</summary>
		// Token: 0x04000447 RID: 1095
		Bezier3 = 3,
		/// <summary>The endpoint of a subpath.</summary>
		// Token: 0x04000448 RID: 1096
		CloseSubpath = 128,
		/// <summary>The corresponding segment is dashed.</summary>
		// Token: 0x04000449 RID: 1097
		DashMode = 16,
		/// <summary>A line segment.</summary>
		// Token: 0x0400044A RID: 1098
		Line = 1,
		/// <summary>A path marker.</summary>
		// Token: 0x0400044B RID: 1099
		PathMarker = 32,
		/// <summary>A mask point.</summary>
		// Token: 0x0400044C RID: 1100
		PathTypeMask = 7,
		/// <summary>The starting point of a <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> object.</summary>
		// Token: 0x0400044D RID: 1101
		Start = 0
	}
}
