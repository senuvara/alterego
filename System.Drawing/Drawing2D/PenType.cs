﻿using System;

namespace System.Drawing.Drawing2D
{
	/// <summary>Specifies the type of fill a <see cref="T:System.Drawing.Pen" /> object uses to fill lines.</summary>
	// Token: 0x02000031 RID: 49
	public enum PenType
	{
		/// <summary>Specifies a hatch fill.</summary>
		// Token: 0x04000250 RID: 592
		HatchFill = 1,
		/// <summary>Specifies a linear gradient fill.</summary>
		// Token: 0x04000251 RID: 593
		LinearGradient = 4,
		/// <summary>Specifies a path gradient fill.</summary>
		// Token: 0x04000252 RID: 594
		PathGradient = 3,
		/// <summary>Specifies a solid fill.</summary>
		// Token: 0x04000253 RID: 595
		SolidColor = 0,
		/// <summary>Specifies a bitmap texture fill.</summary>
		// Token: 0x04000254 RID: 596
		TextureFill = 2
	}
}
