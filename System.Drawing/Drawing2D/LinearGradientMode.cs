﻿using System;

namespace System.Drawing.Drawing2D
{
	/// <summary>Specifies the direction of a linear gradient.</summary>
	// Token: 0x020000BA RID: 186
	public enum LinearGradientMode
	{
		/// <summary>Specifies a gradient from upper right to lower left.</summary>
		// Token: 0x04000441 RID: 1089
		BackwardDiagonal = 3,
		/// <summary>Specifies a gradient from upper left to lower right.</summary>
		// Token: 0x04000442 RID: 1090
		ForwardDiagonal = 2,
		/// <summary>Specifies a gradient from left to right.</summary>
		// Token: 0x04000443 RID: 1091
		Horizontal = 0,
		/// <summary>Specifies a gradient from top to bottom.</summary>
		// Token: 0x04000444 RID: 1092
		Vertical
	}
}
