﻿using System;

namespace System.Drawing.Drawing2D
{
	/// <summary>Specifies the system to use when evaluating coordinates.</summary>
	// Token: 0x02000067 RID: 103
	public enum CoordinateSpace
	{
		/// <summary>Specifies that coordinates are in the device coordinate context. On a computer screen the device coordinates are usually measured in pixels.</summary>
		// Token: 0x0400031B RID: 795
		Device = 2,
		/// <summary>Specifies that coordinates are in the page coordinate context. Their units are defined by the <see cref="P:System.Drawing.Graphics.PageUnit" /> property, and must be one of the elements of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration.</summary>
		// Token: 0x0400031C RID: 796
		Page = 1,
		/// <summary>Specifies that coordinates are in the world coordinate context. World coordinates are used in a nonphysical environment, such as a modeling environment.</summary>
		// Token: 0x0400031D RID: 797
		World = 0
	}
}
