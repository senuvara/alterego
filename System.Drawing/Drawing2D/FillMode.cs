﻿using System;

namespace System.Drawing.Drawing2D
{
	/// <summary>Specifies how the interior of a closed path is filled.</summary>
	// Token: 0x0200001A RID: 26
	public enum FillMode
	{
		/// <summary>Specifies the alternate fill mode.</summary>
		// Token: 0x040001FF RID: 511
		Alternate,
		/// <summary>Specifies the winding fill mode.</summary>
		// Token: 0x04000200 RID: 512
		Winding
	}
}
