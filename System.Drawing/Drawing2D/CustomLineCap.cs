﻿using System;
using Unity;

namespace System.Drawing.Drawing2D
{
	/// <summary>Encapsulates a custom user-defined line cap.</summary>
	// Token: 0x0200002C RID: 44
	public class CustomLineCap : MarshalByRefObject, ICloneable, IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.CustomLineCap" /> class with the specified outline and fill.</summary>
		/// <param name="fillPath">A <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> object that defines the fill for the custom cap. </param>
		/// <param name="strokePath">A <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> object that defines the outline of the custom cap. </param>
		// Token: 0x06000390 RID: 912 RVA: 0x00004644 File Offset: 0x00002844
		public CustomLineCap(GraphicsPath fillPath, GraphicsPath strokePath)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.CustomLineCap" /> class from the specified existing <see cref="T:System.Drawing.Drawing2D.LineCap" /> enumeration with the specified outline and fill.</summary>
		/// <param name="fillPath">A <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> object that defines the fill for the custom cap. </param>
		/// <param name="strokePath">A <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> object that defines the outline of the custom cap. </param>
		/// <param name="baseCap">The line cap from which to create the custom cap. </param>
		// Token: 0x06000391 RID: 913 RVA: 0x00004644 File Offset: 0x00002844
		public CustomLineCap(GraphicsPath fillPath, GraphicsPath strokePath, LineCap baseCap)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Drawing2D.CustomLineCap" /> class from the specified existing <see cref="T:System.Drawing.Drawing2D.LineCap" /> enumeration with the specified outline, fill, and inset.</summary>
		/// <param name="fillPath">A <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> object that defines the fill for the custom cap. </param>
		/// <param name="strokePath">A <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> object that defines the outline of the custom cap. </param>
		/// <param name="baseCap">The line cap from which to create the custom cap. </param>
		/// <param name="baseInset">The distance between the cap and the line. </param>
		// Token: 0x06000392 RID: 914 RVA: 0x00004644 File Offset: 0x00002844
		public CustomLineCap(GraphicsPath fillPath, GraphicsPath strokePath, LineCap baseCap, float baseInset)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the <see cref="T:System.Drawing.Drawing2D.LineCap" /> enumeration on which this <see cref="T:System.Drawing.Drawing2D.CustomLineCap" /> is based.</summary>
		/// <returns>The <see cref="T:System.Drawing.Drawing2D.LineCap" /> enumeration on which this <see cref="T:System.Drawing.Drawing2D.CustomLineCap" /> is based.</returns>
		// Token: 0x170000FF RID: 255
		// (get) Token: 0x06000393 RID: 915 RVA: 0x00005320 File Offset: 0x00003520
		// (set) Token: 0x06000394 RID: 916 RVA: 0x00004644 File Offset: 0x00002844
		public LineCap BaseCap
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return LineCap.Flat;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the distance between the cap and the line.</summary>
		/// <returns>The distance between the beginning of the cap and the end of the line.</returns>
		// Token: 0x17000100 RID: 256
		// (get) Token: 0x06000395 RID: 917 RVA: 0x0000533C File Offset: 0x0000353C
		// (set) Token: 0x06000396 RID: 918 RVA: 0x00004644 File Offset: 0x00002844
		public float BaseInset
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Drawing.Drawing2D.LineJoin" /> enumeration that determines how lines that compose this <see cref="T:System.Drawing.Drawing2D.CustomLineCap" /> object are joined.</summary>
		/// <returns>The <see cref="T:System.Drawing.Drawing2D.LineJoin" /> enumeration this <see cref="T:System.Drawing.Drawing2D.CustomLineCap" /> object uses to join lines.</returns>
		// Token: 0x17000101 RID: 257
		// (get) Token: 0x06000397 RID: 919 RVA: 0x00005358 File Offset: 0x00003558
		// (set) Token: 0x06000398 RID: 920 RVA: 0x00004644 File Offset: 0x00002844
		public LineJoin StrokeJoin
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return LineJoin.Miter;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the amount by which to scale this <see cref="T:System.Drawing.Drawing2D.CustomLineCap" /> Class object with respect to the width of the <see cref="T:System.Drawing.Pen" /> object.</summary>
		/// <returns>The amount by which to scale the cap.</returns>
		// Token: 0x17000102 RID: 258
		// (get) Token: 0x06000399 RID: 921 RVA: 0x00005374 File Offset: 0x00003574
		// (set) Token: 0x0600039A RID: 922 RVA: 0x00004644 File Offset: 0x00002844
		public float WidthScale
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Creates an exact copy of this <see cref="T:System.Drawing.Drawing2D.CustomLineCap" />.</summary>
		/// <returns>The <see cref="T:System.Drawing.Drawing2D.CustomLineCap" /> this method creates, cast as an object.</returns>
		// Token: 0x0600039B RID: 923 RVA: 0x00004667 File Offset: 0x00002867
		public object Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Releases all resources used by this <see cref="T:System.Drawing.Drawing2D.CustomLineCap" /> object.</summary>
		// Token: 0x0600039C RID: 924 RVA: 0x00004644 File Offset: 0x00002844
		public void Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Drawing.Drawing2D.CustomLineCap" /> and optionally releases the managed resources. </summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x0600039D RID: 925 RVA: 0x00004644 File Offset: 0x00002844
		protected virtual void Dispose(bool disposing)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the caps used to start and end lines that make up this custom cap.</summary>
		/// <param name="startCap">The <see cref="T:System.Drawing.Drawing2D.LineCap" /> enumeration used at the beginning of a line within this cap. </param>
		/// <param name="endCap">The <see cref="T:System.Drawing.Drawing2D.LineCap" /> enumeration used at the end of a line within this cap. </param>
		// Token: 0x0600039E RID: 926 RVA: 0x00004644 File Offset: 0x00002844
		public void GetStrokeCaps(out LineCap startCap, out LineCap endCap)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the caps used to start and end lines that make up this custom cap.</summary>
		/// <param name="startCap">The <see cref="T:System.Drawing.Drawing2D.LineCap" /> enumeration used at the beginning of a line within this cap. </param>
		/// <param name="endCap">The <see cref="T:System.Drawing.Drawing2D.LineCap" /> enumeration used at the end of a line within this cap. </param>
		// Token: 0x0600039F RID: 927 RVA: 0x00004644 File Offset: 0x00002844
		public void SetStrokeCaps(LineCap startCap, LineCap endCap)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
