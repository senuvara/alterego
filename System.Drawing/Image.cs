﻿using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Unity;

namespace System.Drawing
{
	/// <summary>An abstract base class that provides functionality for the <see cref="T:System.Drawing.Bitmap" /> and <see cref="T:System.Drawing.Imaging.Metafile" /> descended classes.</summary>
	// Token: 0x0200003F RID: 63
	[TypeConverter(typeof(ImageConverter))]
	[ImmutableObject(true)]
	[Editor("System.Drawing.Design.ImageEditor, System.Drawing.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
	[ComVisible(true)]
	[Serializable]
	public abstract class Image : MarshalByRefObject, ICloneable, IDisposable, ISerializable
	{
		// Token: 0x060003D3 RID: 979 RVA: 0x00004644 File Offset: 0x00002844
		internal Image()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets attribute flags for the pixel data of this <see cref="T:System.Drawing.Image" />.</summary>
		/// <returns>The integer representing a bitwise combination of <see cref="T:System.Drawing.Imaging.ImageFlags" /> for this <see cref="T:System.Drawing.Image" />.</returns>
		// Token: 0x17000108 RID: 264
		// (get) Token: 0x060003D4 RID: 980 RVA: 0x00005470 File Offset: 0x00003670
		public int Flags
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets an array of GUIDs that represent the dimensions of frames within this <see cref="T:System.Drawing.Image" />.</summary>
		/// <returns>An array of GUIDs that specify the dimensions of frames within this <see cref="T:System.Drawing.Image" /> from most significant to least significant.</returns>
		// Token: 0x17000109 RID: 265
		// (get) Token: 0x060003D5 RID: 981 RVA: 0x00004667 File Offset: 0x00002867
		public Guid[] FrameDimensionsList
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the height, in pixels, of this <see cref="T:System.Drawing.Image" />.</summary>
		/// <returns>The height, in pixels, of this <see cref="T:System.Drawing.Image" />.</returns>
		// Token: 0x1700010A RID: 266
		// (get) Token: 0x060003D6 RID: 982 RVA: 0x0000548C File Offset: 0x0000368C
		public int Height
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the horizontal resolution, in pixels per inch, of this <see cref="T:System.Drawing.Image" />.</summary>
		/// <returns>The horizontal resolution, in pixels per inch, of this <see cref="T:System.Drawing.Image" />.</returns>
		// Token: 0x1700010B RID: 267
		// (get) Token: 0x060003D7 RID: 983 RVA: 0x000054A8 File Offset: 0x000036A8
		public float HorizontalResolution
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
		}

		/// <summary>Gets or sets the color palette used for this <see cref="T:System.Drawing.Image" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.Imaging.ColorPalette" /> that represents the color palette used for this <see cref="T:System.Drawing.Image" />.</returns>
		// Token: 0x1700010C RID: 268
		// (get) Token: 0x060003D8 RID: 984 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x060003D9 RID: 985 RVA: 0x00004644 File Offset: 0x00002844
		public ColorPalette Palette
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the width and height of this image.</summary>
		/// <returns>A <see cref="T:System.Drawing.SizeF" /> structure that represents the width and height of this <see cref="T:System.Drawing.Image" />.</returns>
		// Token: 0x1700010D RID: 269
		// (get) Token: 0x060003DA RID: 986 RVA: 0x000054C4 File Offset: 0x000036C4
		public SizeF PhysicalDimension
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(SizeF);
			}
		}

		/// <summary>Gets the pixel format for this <see cref="T:System.Drawing.Image" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.Imaging.PixelFormat" /> that represents the pixel format for this <see cref="T:System.Drawing.Image" />.</returns>
		// Token: 0x1700010E RID: 270
		// (get) Token: 0x060003DB RID: 987 RVA: 0x000054E0 File Offset: 0x000036E0
		public PixelFormat PixelFormat
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return PixelFormat.DontCare;
			}
		}

		/// <summary>Gets IDs of the property items stored in this <see cref="T:System.Drawing.Image" />.</summary>
		/// <returns>An array of the property IDs, one for each property item stored in this image.</returns>
		// Token: 0x1700010F RID: 271
		// (get) Token: 0x060003DC RID: 988 RVA: 0x00004667 File Offset: 0x00002867
		public int[] PropertyIdList
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets all the property items (pieces of metadata) stored in this <see cref="T:System.Drawing.Image" />.</summary>
		/// <returns>An array of <see cref="T:System.Drawing.Imaging.PropertyItem" /> objects, one for each property item stored in the image.</returns>
		// Token: 0x17000110 RID: 272
		// (get) Token: 0x060003DD RID: 989 RVA: 0x00004667 File Offset: 0x00002867
		public PropertyItem[] PropertyItems
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the file format of this <see cref="T:System.Drawing.Image" />.</summary>
		/// <returns>The <see cref="T:System.Drawing.Imaging.ImageFormat" /> that represents the file format of this <see cref="T:System.Drawing.Image" />.</returns>
		// Token: 0x17000111 RID: 273
		// (get) Token: 0x060003DE RID: 990 RVA: 0x00004667 File Offset: 0x00002867
		public ImageFormat RawFormat
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the width and height, in pixels, of this image.</summary>
		/// <returns>A <see cref="T:System.Drawing.Size" /> structure that represents the width and height, in pixels, of this image.</returns>
		// Token: 0x17000112 RID: 274
		// (get) Token: 0x060003DF RID: 991 RVA: 0x000054FC File Offset: 0x000036FC
		public Size Size
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Size);
			}
		}

		/// <summary>Gets or sets an object that provides additional data about the image.</summary>
		/// <returns>The <see cref="T:System.Object" /> that provides additional data about the image.</returns>
		// Token: 0x17000113 RID: 275
		// (get) Token: 0x060003E0 RID: 992 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x060003E1 RID: 993 RVA: 0x00004644 File Offset: 0x00002844
		public object Tag
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the vertical resolution, in pixels per inch, of this <see cref="T:System.Drawing.Image" />.</summary>
		/// <returns>The vertical resolution, in pixels per inch, of this <see cref="T:System.Drawing.Image" />.</returns>
		// Token: 0x17000114 RID: 276
		// (get) Token: 0x060003E2 RID: 994 RVA: 0x00005518 File Offset: 0x00003718
		public float VerticalResolution
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
		}

		/// <summary>Gets the width, in pixels, of this <see cref="T:System.Drawing.Image" />.</summary>
		/// <returns>The width, in pixels, of this <see cref="T:System.Drawing.Image" />.</returns>
		// Token: 0x17000115 RID: 277
		// (get) Token: 0x060003E3 RID: 995 RVA: 0x00005534 File Offset: 0x00003734
		public int Width
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Creates an exact copy of this <see cref="T:System.Drawing.Image" />.</summary>
		/// <returns>The <see cref="T:System.Drawing.Image" /> this method creates, cast as an object.</returns>
		// Token: 0x060003E4 RID: 996 RVA: 0x00004667 File Offset: 0x00002867
		public object Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Releases all resources used by this <see cref="T:System.Drawing.Image" />.</summary>
		// Token: 0x060003E5 RID: 997 RVA: 0x00004644 File Offset: 0x00002844
		public void Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Drawing.Image" /> and optionally releases the managed resources. </summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources.</param>
		// Token: 0x060003E6 RID: 998 RVA: 0x00004644 File Offset: 0x00002844
		protected virtual void Dispose(bool disposing)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates an <see cref="T:System.Drawing.Image" /> from the specified file.</summary>
		/// <param name="filename">A string that contains the name of the file from which to create the <see cref="T:System.Drawing.Image" />. </param>
		/// <returns>The <see cref="T:System.Drawing.Image" /> this method creates.</returns>
		/// <exception cref="T:System.OutOfMemoryException">The file does not have a valid image format.-or-
		///         GDI+ does not support the pixel format of the file.</exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The specified file does not exist.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="filename" /> is a <see cref="T:System.Uri" />.</exception>
		// Token: 0x060003E7 RID: 999 RVA: 0x00004667 File Offset: 0x00002867
		public static Image FromFile(string filename)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates an <see cref="T:System.Drawing.Image" /> from the specified file using embedded color management information in that file.</summary>
		/// <param name="filename">A string that contains the name of the file from which to create the <see cref="T:System.Drawing.Image" />. </param>
		/// <param name="useEmbeddedColorManagement">Set to <see langword="true" /> to use color management information embedded in the image file; otherwise, <see langword="false" />. </param>
		/// <returns>The <see cref="T:System.Drawing.Image" /> this method creates.</returns>
		/// <exception cref="T:System.OutOfMemoryException">The file does not have a valid image format.-or-
		///         GDI+ does not support the pixel format of the file.</exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The specified file does not exist.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="filename" /> is a <see cref="T:System.Uri" />.</exception>
		// Token: 0x060003E8 RID: 1000 RVA: 0x00004667 File Offset: 0x00002867
		public static Image FromFile(string filename, bool useEmbeddedColorManagement)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates a <see cref="T:System.Drawing.Bitmap" /> from a handle to a GDI bitmap.</summary>
		/// <param name="hbitmap">The GDI bitmap handle from which to create the <see cref="T:System.Drawing.Bitmap" />. </param>
		/// <returns>The <see cref="T:System.Drawing.Bitmap" /> this method creates.</returns>
		// Token: 0x060003E9 RID: 1001 RVA: 0x00004667 File Offset: 0x00002867
		public static Bitmap FromHbitmap(IntPtr hbitmap)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates a <see cref="T:System.Drawing.Bitmap" /> from a handle to a GDI bitmap and a handle to a GDI palette.</summary>
		/// <param name="hbitmap">The GDI bitmap handle from which to create the <see cref="T:System.Drawing.Bitmap" />. </param>
		/// <param name="hpalette">A handle to a GDI palette used to define the bitmap colors if the bitmap specified in the <paramref name="hBitmap" /> parameter is not a device-independent bitmap (DIB). </param>
		/// <returns>The <see cref="T:System.Drawing.Bitmap" /> this method creates.</returns>
		// Token: 0x060003EA RID: 1002 RVA: 0x00004667 File Offset: 0x00002867
		public static Bitmap FromHbitmap(IntPtr hbitmap, IntPtr hpalette)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates an <see cref="T:System.Drawing.Image" /> from the specified data stream.</summary>
		/// <param name="stream">A <see cref="T:System.IO.Stream" /> that contains the data for this <see cref="T:System.Drawing.Image" />. </param>
		/// <returns>The <see cref="T:System.Drawing.Image" /> this method creates.</returns>
		/// <exception cref="T:System.ArgumentException">The stream does not have a valid image format-or-
		///         <paramref name="stream" /> is <see langword="null" />.</exception>
		// Token: 0x060003EB RID: 1003 RVA: 0x00004667 File Offset: 0x00002867
		public static Image FromStream(Stream stream)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates an <see cref="T:System.Drawing.Image" /> from the specified data stream, optionally using embedded color management information in that stream.</summary>
		/// <param name="stream">A <see cref="T:System.IO.Stream" /> that contains the data for this <see cref="T:System.Drawing.Image" />. </param>
		/// <param name="useEmbeddedColorManagement">
		///       <see langword="true" /> to use color management information embedded in the data stream; otherwise, <see langword="false" />. </param>
		/// <returns>The <see cref="T:System.Drawing.Image" /> this method creates.</returns>
		/// <exception cref="T:System.ArgumentException">The stream does not have a valid image format -or-
		///         <paramref name="stream" /> is <see langword="null" />.</exception>
		// Token: 0x060003EC RID: 1004 RVA: 0x00004667 File Offset: 0x00002867
		public static Image FromStream(Stream stream, bool useEmbeddedColorManagement)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates an <see cref="T:System.Drawing.Image" /> from the specified data stream, optionally using embedded color management information and validating the image data.</summary>
		/// <param name="stream">A <see cref="T:System.IO.Stream" /> that contains the data for this <see cref="T:System.Drawing.Image" />. </param>
		/// <param name="useEmbeddedColorManagement">
		///       <see langword="true" /> to use color management information embedded in the data stream; otherwise, <see langword="false" />. </param>
		/// <param name="validateImageData">
		///       <see langword="true" /> to validate the image data; otherwise, <see langword="false" />.</param>
		/// <returns>The <see cref="T:System.Drawing.Image" /> this method creates.</returns>
		/// <exception cref="T:System.ArgumentException">The stream does not have a valid image format.</exception>
		// Token: 0x060003ED RID: 1005 RVA: 0x00004667 File Offset: 0x00002867
		public static Image FromStream(Stream stream, bool useEmbeddedColorManagement, bool validateImageData)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the bounds of the image in the specified unit.</summary>
		/// <param name="pageUnit">One of the <see cref="T:System.Drawing.GraphicsUnit" /> values indicating the unit of measure for the bounding rectangle.</param>
		/// <returns>The <see cref="T:System.Drawing.RectangleF" /> that represents the bounds of the image, in the specified unit.</returns>
		// Token: 0x060003EE RID: 1006 RVA: 0x00005550 File Offset: 0x00003750
		public RectangleF GetBounds(ref GraphicsUnit pageUnit)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(RectangleF);
		}

		/// <summary>Returns information about the parameters supported by the specified image encoder.</summary>
		/// <param name="encoder">A GUID that specifies the image encoder. </param>
		/// <returns>An <see cref="T:System.Drawing.Imaging.EncoderParameters" /> that contains an array of <see cref="T:System.Drawing.Imaging.EncoderParameter" /> objects. Each <see cref="T:System.Drawing.Imaging.EncoderParameter" /> contains information about one of the parameters supported by the specified image encoder.</returns>
		// Token: 0x060003EF RID: 1007 RVA: 0x00004667 File Offset: 0x00002867
		public EncoderParameters GetEncoderParameterList(Guid encoder)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the number of frames of the specified dimension.</summary>
		/// <param name="dimension">A <see cref="T:System.Drawing.Imaging.FrameDimension" /> that specifies the identity of the dimension type. </param>
		/// <returns>The number of frames in the specified dimension.</returns>
		// Token: 0x060003F0 RID: 1008 RVA: 0x0000556C File Offset: 0x0000376C
		public int GetFrameCount(FrameDimension dimension)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Returns the color depth, in number of bits per pixel, of the specified pixel format.</summary>
		/// <param name="pixfmt">The <see cref="T:System.Drawing.Imaging.PixelFormat" /> member that specifies the format for which to find the size. </param>
		/// <returns>The color depth of the specified pixel format.</returns>
		// Token: 0x060003F1 RID: 1009 RVA: 0x00005588 File Offset: 0x00003788
		public static int GetPixelFormatSize(PixelFormat pixfmt)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Gets the specified property item from this <see cref="T:System.Drawing.Image" />.</summary>
		/// <param name="propid">The ID of the property item to get. </param>
		/// <returns>The <see cref="T:System.Drawing.Imaging.PropertyItem" /> this method gets.</returns>
		/// <exception cref="T:System.ArgumentException">The image format of this image does not support property items.</exception>
		// Token: 0x060003F2 RID: 1010 RVA: 0x00004667 File Offset: 0x00002867
		public PropertyItem GetPropertyItem(int propid)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns a thumbnail for this <see cref="T:System.Drawing.Image" />.</summary>
		/// <param name="thumbWidth">The width, in pixels, of the requested thumbnail image. </param>
		/// <param name="thumbHeight">The height, in pixels, of the requested thumbnail image. </param>
		/// <param name="callback">A <see cref="T:System.Drawing.Image.GetThumbnailImageAbort" /> delegate. 
		///       Note   You must create a delegate and pass a reference to the delegate as the <paramref name="callback" /> parameter, but the delegate is not used.</param>
		/// <param name="callbackData">Must be <see cref="F:System.IntPtr.Zero" />. </param>
		/// <returns>An <see cref="T:System.Drawing.Image" /> that represents the thumbnail.</returns>
		// Token: 0x060003F3 RID: 1011 RVA: 0x00004667 File Offset: 0x00002867
		public Image GetThumbnailImage(int thumbWidth, int thumbHeight, Image.GetThumbnailImageAbort callback, IntPtr callbackData)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns a value that indicates whether the pixel format for this <see cref="T:System.Drawing.Image" /> contains alpha information.</summary>
		/// <param name="pixfmt">The <see cref="T:System.Drawing.Imaging.PixelFormat" /> to test. </param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="pixfmt" /> contains alpha information; otherwise, <see langword="false" />.</returns>
		// Token: 0x060003F4 RID: 1012 RVA: 0x000055A4 File Offset: 0x000037A4
		public static bool IsAlphaPixelFormat(PixelFormat pixfmt)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a value that indicates whether the pixel format is 32 bits per pixel.</summary>
		/// <param name="pixfmt">The <see cref="T:System.Drawing.Imaging.PixelFormat" /> to test. </param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="pixfmt" /> is canonical; otherwise, <see langword="false" />.</returns>
		// Token: 0x060003F5 RID: 1013 RVA: 0x000055C0 File Offset: 0x000037C0
		public static bool IsCanonicalPixelFormat(PixelFormat pixfmt)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a value that indicates whether the pixel format is 64 bits per pixel.</summary>
		/// <param name="pixfmt">The <see cref="T:System.Drawing.Imaging.PixelFormat" /> enumeration to test. </param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="pixfmt" /> is extended; otherwise, <see langword="false" />.</returns>
		// Token: 0x060003F6 RID: 1014 RVA: 0x000055DC File Offset: 0x000037DC
		public static bool IsExtendedPixelFormat(PixelFormat pixfmt)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Removes the specified property item from this <see cref="T:System.Drawing.Image" />.</summary>
		/// <param name="propid">The ID of the property item to remove. </param>
		/// <exception cref="T:System.ArgumentException">The image does not contain the requested property item.-or-The image format for this image does not support property items.</exception>
		// Token: 0x060003F7 RID: 1015 RVA: 0x00004644 File Offset: 0x00002844
		public void RemovePropertyItem(int propid)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Rotates, flips, or rotates and flips the <see cref="T:System.Drawing.Image" />.</summary>
		/// <param name="rotateFlipType">A <see cref="T:System.Drawing.RotateFlipType" /> member that specifies the type of rotation and flip to apply to the image. </param>
		// Token: 0x060003F8 RID: 1016 RVA: 0x00004644 File Offset: 0x00002844
		public void RotateFlip(RotateFlipType rotateFlipType)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Saves this image to the specified stream, with the specified encoder and image encoder parameters.</summary>
		/// <param name="stream">The <see cref="T:System.IO.Stream" /> where the image will be saved. </param>
		/// <param name="encoder">The <see cref="T:System.Drawing.Imaging.ImageCodecInfo" /> for this <see cref="T:System.Drawing.Image" />.</param>
		/// <param name="encoderParams">An <see cref="T:System.Drawing.Imaging.EncoderParameters" /> that specifies parameters used by the image encoder. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="stream" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.Runtime.InteropServices.ExternalException">The image was saved with the wrong image format.</exception>
		// Token: 0x060003F9 RID: 1017 RVA: 0x00004644 File Offset: 0x00002844
		public void Save(Stream stream, ImageCodecInfo encoder, EncoderParameters encoderParams)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Saves this image to the specified stream in the specified format.</summary>
		/// <param name="stream">The <see cref="T:System.IO.Stream" /> where the image will be saved. </param>
		/// <param name="format">An <see cref="T:System.Drawing.Imaging.ImageFormat" /> that specifies the format of the saved image. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="stream" /> or <paramref name="format" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.Runtime.InteropServices.ExternalException">The image was saved with the wrong image format</exception>
		// Token: 0x060003FA RID: 1018 RVA: 0x00004644 File Offset: 0x00002844
		public void Save(Stream stream, ImageFormat format)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Saves this <see cref="T:System.Drawing.Image" /> to the specified file or stream.</summary>
		/// <param name="filename">A string that contains the name of the file to which to save this <see cref="T:System.Drawing.Image" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="filename" /> is <see langword="null." /></exception>
		/// <exception cref="T:System.Runtime.InteropServices.ExternalException">The image was saved with the wrong image format.-or- The image was saved to the same file it was created from.</exception>
		// Token: 0x060003FB RID: 1019 RVA: 0x00004644 File Offset: 0x00002844
		public void Save(string filename)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Saves this <see cref="T:System.Drawing.Image" /> to the specified file, with the specified encoder and image-encoder parameters.</summary>
		/// <param name="filename">A string that contains the name of the file to which to save this <see cref="T:System.Drawing.Image" />. </param>
		/// <param name="encoder">The <see cref="T:System.Drawing.Imaging.ImageCodecInfo" /> for this <see cref="T:System.Drawing.Image" />. </param>
		/// <param name="encoderParams">An <see cref="T:System.Drawing.Imaging.EncoderParameters" /> to use for this <see cref="T:System.Drawing.Image" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="filename" /> or <paramref name="encoder" /> is <see langword="null." /></exception>
		/// <exception cref="T:System.Runtime.InteropServices.ExternalException">The image was saved with the wrong image format.-or- The image was saved to the same file it was created from.</exception>
		// Token: 0x060003FC RID: 1020 RVA: 0x00004644 File Offset: 0x00002844
		public void Save(string filename, ImageCodecInfo encoder, EncoderParameters encoderParams)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Saves this <see cref="T:System.Drawing.Image" /> to the specified file in the specified format.</summary>
		/// <param name="filename">A string that contains the name of the file to which to save this <see cref="T:System.Drawing.Image" />. </param>
		/// <param name="format">The <see cref="T:System.Drawing.Imaging.ImageFormat" /> for this <see cref="T:System.Drawing.Image" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="filename" /> or <paramref name="format" /> is <see langword="null." /></exception>
		/// <exception cref="T:System.Runtime.InteropServices.ExternalException">The image was saved with the wrong image format.-or- The image was saved to the same file it was created from.</exception>
		// Token: 0x060003FD RID: 1021 RVA: 0x00004644 File Offset: 0x00002844
		public void Save(string filename, ImageFormat format)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a frame to the file or stream specified in a previous call to the <see cref="Overload:System.Drawing.Image.Save" /> method.</summary>
		/// <param name="image">An <see cref="T:System.Drawing.Image" /> that contains the frame to add. </param>
		/// <param name="encoderParams">An <see cref="T:System.Drawing.Imaging.EncoderParameters" /> that holds parameters required by the image encoder that is used by the save-add operation. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060003FE RID: 1022 RVA: 0x00004644 File Offset: 0x00002844
		public void SaveAdd(Image image, EncoderParameters encoderParams)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a frame to the file or stream specified in a previous call to the <see cref="Overload:System.Drawing.Image.Save" /> method. Use this method to save selected frames from a multiple-frame image to another multiple-frame image.</summary>
		/// <param name="encoderParams">An <see cref="T:System.Drawing.Imaging.EncoderParameters" /> that holds parameters required by the image encoder that is used by the save-add operation. </param>
		// Token: 0x060003FF RID: 1023 RVA: 0x00004644 File Offset: 0x00002844
		public void SaveAdd(EncoderParameters encoderParams)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Selects the frame specified by the dimension and index.</summary>
		/// <param name="dimension">A <see cref="T:System.Drawing.Imaging.FrameDimension" /> that specifies the identity of the dimension type. </param>
		/// <param name="frameIndex">The index of the active frame. </param>
		/// <returns>Always returns 0.</returns>
		// Token: 0x06000400 RID: 1024 RVA: 0x000055F8 File Offset: 0x000037F8
		public int SelectActiveFrame(FrameDimension dimension, int frameIndex)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Stores a property item (piece of metadata) in this <see cref="T:System.Drawing.Image" />.</summary>
		/// <param name="propitem">The <see cref="T:System.Drawing.Imaging.PropertyItem" /> to be stored. </param>
		/// <exception cref="T:System.ArgumentException">The image format of this image does not support property items.</exception>
		// Token: 0x06000401 RID: 1025 RVA: 0x00004644 File Offset: 0x00002844
		public void SetPropertyItem(PropertyItem propitem)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with the data needed to serialize the target object.</summary>
		/// <param name="si">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data.</param>
		/// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this serialization.</param>
		// Token: 0x06000402 RID: 1026 RVA: 0x00004644 File Offset: 0x00002844
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
		void ISerializable.GetObjectData(SerializationInfo si, StreamingContext context)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Provides a callback method for determining when the <see cref="M:System.Drawing.Image.GetThumbnailImage(System.Int32,System.Int32,System.Drawing.Image.GetThumbnailImageAbort,System.IntPtr)" /> method should prematurely cancel execution.</summary>
		/// <returns>This method returns <see langword="true" /> if it decides that the <see cref="M:System.Drawing.Image.GetThumbnailImage(System.Int32,System.Int32,System.Drawing.Image.GetThumbnailImageAbort,System.IntPtr)" /> method should prematurely stop execution; otherwise, it returns <see langword="false" />.</returns>
		// Token: 0x02000040 RID: 64
		public sealed class GetThumbnailImageAbort : MulticastDelegate
		{
			// Token: 0x06000403 RID: 1027 RVA: 0x00004644 File Offset: 0x00002844
			public GetThumbnailImageAbort(object @object, IntPtr method)
			{
				ThrowStub.ThrowNotSupportedException();
			}

			// Token: 0x06000404 RID: 1028 RVA: 0x00005614 File Offset: 0x00003814
			public bool Invoke()
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}

			// Token: 0x06000405 RID: 1029 RVA: 0x00004667 File Offset: 0x00002867
			public IAsyncResult BeginInvoke(AsyncCallback callback, object @object)
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}

			// Token: 0x06000406 RID: 1030 RVA: 0x00005630 File Offset: 0x00003830
			public bool EndInvoke(IAsyncResult result)
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}
	}
}
