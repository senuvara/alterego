﻿using System;
using Unity;

namespace System.Drawing.Printing
{
	/// <summary>Represents the method that will handle the <see cref="E:System.Drawing.Printing.PrintDocument.BeginPrint" /> or <see cref="E:System.Drawing.Printing.PrintDocument.EndPrint" /> event of a <see cref="T:System.Drawing.Printing.PrintDocument" />.</summary>
	/// <param name="sender">The source of the event. </param>
	/// <param name="e">A <see cref="T:System.Drawing.Printing.PrintEventArgs" /> that contains the event data. </param>
	// Token: 0x020000A1 RID: 161
	public sealed class PrintEventHandler : MulticastDelegate
	{
		// Token: 0x0600087C RID: 2172 RVA: 0x00004644 File Offset: 0x00002844
		public PrintEventHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0600087D RID: 2173 RVA: 0x00004644 File Offset: 0x00002844
		public void Invoke(object sender, PrintEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0600087E RID: 2174 RVA: 0x00004667 File Offset: 0x00002867
		public IAsyncResult BeginInvoke(object sender, PrintEventArgs e, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x0600087F RID: 2175 RVA: 0x00004644 File Offset: 0x00002844
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
