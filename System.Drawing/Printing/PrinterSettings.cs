﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing.Imaging;
using Unity;

namespace System.Drawing.Printing
{
	/// <summary>Specifies information about how a document is printed, including the printer that prints it, when printing from a Windows Forms application.</summary>
	// Token: 0x0200008E RID: 142
	[Serializable]
	public class PrinterSettings : ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.PrinterSettings" /> class.</summary>
		// Token: 0x060007BA RID: 1978 RVA: 0x00004644 File Offset: 0x00002844
		public PrinterSettings()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a value indicating whether the printer supports double-sided printing.</summary>
		/// <returns>
		///     <see langword="true" /> if the printer supports double-sided printing; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000332 RID: 818
		// (get) Token: 0x060007BB RID: 1979 RVA: 0x00006644 File Offset: 0x00004844
		public bool CanDuplex
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets or sets a value indicating whether the printed document is collated.</summary>
		/// <returns>
		///     <see langword="true" /> if the printed document is collated; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000333 RID: 819
		// (get) Token: 0x060007BC RID: 1980 RVA: 0x00006660 File Offset: 0x00004860
		// (set) Token: 0x060007BD RID: 1981 RVA: 0x00004644 File Offset: 0x00002844
		public bool Collate
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the number of copies of the document to print.</summary>
		/// <returns>The number of copies to print. The default is 1.</returns>
		/// <exception cref="T:System.ArgumentException">The value of the <see cref="P:System.Drawing.Printing.PrinterSettings.Copies" /> property is less than zero. </exception>
		// Token: 0x17000334 RID: 820
		// (get) Token: 0x060007BE RID: 1982 RVA: 0x0000667C File Offset: 0x0000487C
		// (set) Token: 0x060007BF RID: 1983 RVA: 0x00004644 File Offset: 0x00002844
		public short Copies
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the default page settings for this printer.</summary>
		/// <returns>A <see cref="T:System.Drawing.Printing.PageSettings" /> that represents the default page settings for this printer.</returns>
		// Token: 0x17000335 RID: 821
		// (get) Token: 0x060007C0 RID: 1984 RVA: 0x00004667 File Offset: 0x00002867
		public PageSettings DefaultPageSettings
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the printer setting for double-sided printing.</summary>
		/// <returns>One of the <see cref="T:System.Drawing.Printing.Duplex" /> values. The default is determined by the printer.</returns>
		/// <exception cref="T:System.ComponentModel.InvalidEnumArgumentException">The value of the <see cref="P:System.Drawing.Printing.PrinterSettings.Duplex" /> property is not one of the <see cref="T:System.Drawing.Printing.Duplex" /> values. </exception>
		// Token: 0x17000336 RID: 822
		// (get) Token: 0x060007C1 RID: 1985 RVA: 0x00006698 File Offset: 0x00004898
		// (set) Token: 0x060007C2 RID: 1986 RVA: 0x00004644 File Offset: 0x00002844
		public Duplex Duplex
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (Duplex)0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the page number of the first page to print.</summary>
		/// <returns>The page number of the first page to print.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Printing.PrinterSettings.FromPage" /> property's value is less than zero. </exception>
		// Token: 0x17000337 RID: 823
		// (get) Token: 0x060007C3 RID: 1987 RVA: 0x000066B4 File Offset: 0x000048B4
		// (set) Token: 0x060007C4 RID: 1988 RVA: 0x00004644 File Offset: 0x00002844
		public int FromPage
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the names of all printers installed on the computer.</summary>
		/// <returns>A <see cref="T:System.Drawing.Printing.PrinterSettings.StringCollection" /> that represents the names of all printers installed on the computer.</returns>
		/// <exception cref="T:System.ComponentModel.Win32Exception">The available printers could not be enumerated. </exception>
		// Token: 0x17000338 RID: 824
		// (get) Token: 0x060007C5 RID: 1989 RVA: 0x00004667 File Offset: 0x00002867
		public static PrinterSettings.StringCollection InstalledPrinters
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a value indicating whether the <see cref="P:System.Drawing.Printing.PrinterSettings.PrinterName" /> property designates the default printer, except when the user explicitly sets <see cref="P:System.Drawing.Printing.PrinterSettings.PrinterName" />.</summary>
		/// <returns>
		///     <see langword="true" /> if <see cref="P:System.Drawing.Printing.PrinterSettings.PrinterName" /> designates the default printer; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000339 RID: 825
		// (get) Token: 0x060007C6 RID: 1990 RVA: 0x000066D0 File Offset: 0x000048D0
		public bool IsDefaultPrinter
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value indicating whether the printer is a plotter.</summary>
		/// <returns>
		///     <see langword="true" /> if the printer is a plotter; <see langword="false" /> if the printer is a raster.</returns>
		// Token: 0x1700033A RID: 826
		// (get) Token: 0x060007C7 RID: 1991 RVA: 0x000066EC File Offset: 0x000048EC
		public bool IsPlotter
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value indicating whether the <see cref="P:System.Drawing.Printing.PrinterSettings.PrinterName" /> property designates a valid printer.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="P:System.Drawing.Printing.PrinterSettings.PrinterName" /> property designates a valid printer; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700033B RID: 827
		// (get) Token: 0x060007C8 RID: 1992 RVA: 0x00006708 File Offset: 0x00004908
		public bool IsValid
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets the angle, in degrees, that the portrait orientation is rotated to produce the landscape orientation.</summary>
		/// <returns>The angle, in degrees, that the portrait orientation is rotated to produce the landscape orientation.</returns>
		// Token: 0x1700033C RID: 828
		// (get) Token: 0x060007C9 RID: 1993 RVA: 0x00006724 File Offset: 0x00004924
		public int LandscapeAngle
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the maximum number of copies that the printer enables the user to print at a time.</summary>
		/// <returns>The maximum number of copies that the printer enables the user to print at a time.</returns>
		// Token: 0x1700033D RID: 829
		// (get) Token: 0x060007CA RID: 1994 RVA: 0x00006740 File Offset: 0x00004940
		public int MaximumCopies
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets or sets the maximum <see cref="P:System.Drawing.Printing.PrinterSettings.FromPage" /> or <see cref="P:System.Drawing.Printing.PrinterSettings.ToPage" /> that can be selected in a <see cref="T:System.Windows.Forms.PrintDialog" />.</summary>
		/// <returns>The maximum <see cref="P:System.Drawing.Printing.PrinterSettings.FromPage" /> or <see cref="P:System.Drawing.Printing.PrinterSettings.ToPage" /> that can be selected in a <see cref="T:System.Windows.Forms.PrintDialog" />.</returns>
		/// <exception cref="T:System.ArgumentException">The value of the <see cref="P:System.Drawing.Printing.PrinterSettings.MaximumPage" /> property is less than zero. </exception>
		// Token: 0x1700033E RID: 830
		// (get) Token: 0x060007CB RID: 1995 RVA: 0x0000675C File Offset: 0x0000495C
		// (set) Token: 0x060007CC RID: 1996 RVA: 0x00004644 File Offset: 0x00002844
		public int MaximumPage
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the minimum <see cref="P:System.Drawing.Printing.PrinterSettings.FromPage" /> or <see cref="P:System.Drawing.Printing.PrinterSettings.ToPage" /> that can be selected in a <see cref="T:System.Windows.Forms.PrintDialog" />.</summary>
		/// <returns>The minimum <see cref="P:System.Drawing.Printing.PrinterSettings.FromPage" /> or <see cref="P:System.Drawing.Printing.PrinterSettings.ToPage" /> that can be selected in a <see cref="T:System.Windows.Forms.PrintDialog" />.</returns>
		/// <exception cref="T:System.ArgumentException">The value of the <see cref="P:System.Drawing.Printing.PrinterSettings.MinimumPage" /> property is less than zero. </exception>
		// Token: 0x1700033F RID: 831
		// (get) Token: 0x060007CD RID: 1997 RVA: 0x00006778 File Offset: 0x00004978
		// (set) Token: 0x060007CE RID: 1998 RVA: 0x00004644 File Offset: 0x00002844
		public int MinimumPage
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the paper sizes that are supported by this printer.</summary>
		/// <returns>A <see cref="T:System.Drawing.Printing.PrinterSettings.PaperSizeCollection" /> that represents the paper sizes that are supported by this printer.</returns>
		// Token: 0x17000340 RID: 832
		// (get) Token: 0x060007CF RID: 1999 RVA: 0x00004667 File Offset: 0x00002867
		public PrinterSettings.PaperSizeCollection PaperSizes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the paper source trays that are available on the printer.</summary>
		/// <returns>A <see cref="T:System.Drawing.Printing.PrinterSettings.PaperSourceCollection" /> that represents the paper source trays that are available on this printer.</returns>
		// Token: 0x17000341 RID: 833
		// (get) Token: 0x060007D0 RID: 2000 RVA: 0x00004667 File Offset: 0x00002867
		public PrinterSettings.PaperSourceCollection PaperSources
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the name of the printer to use.</summary>
		/// <returns>The name of the printer to use.</returns>
		// Token: 0x17000342 RID: 834
		// (get) Token: 0x060007D1 RID: 2001 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x060007D2 RID: 2002 RVA: 0x00004644 File Offset: 0x00002844
		public string PrinterName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets all the resolutions that are supported by this printer.</summary>
		/// <returns>A <see cref="T:System.Drawing.Printing.PrinterSettings.PrinterResolutionCollection" /> that represents the resolutions that are supported by this printer.</returns>
		// Token: 0x17000343 RID: 835
		// (get) Token: 0x060007D3 RID: 2003 RVA: 0x00004667 File Offset: 0x00002867
		public PrinterSettings.PrinterResolutionCollection PrinterResolutions
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the file name, when printing to a file.</summary>
		/// <returns>The file name, when printing to a file.</returns>
		// Token: 0x17000344 RID: 836
		// (get) Token: 0x060007D4 RID: 2004 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x060007D5 RID: 2005 RVA: 0x00004644 File Offset: 0x00002844
		public string PrintFileName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the page numbers that the user has specified to be printed.</summary>
		/// <returns>One of the <see cref="T:System.Drawing.Printing.PrintRange" /> values.</returns>
		/// <exception cref="T:System.ComponentModel.InvalidEnumArgumentException">The value of the <see cref="P:System.Drawing.Printing.PrinterSettings.PrintRange" /> property is not one of the <see cref="T:System.Drawing.Printing.PrintRange" /> values. </exception>
		// Token: 0x17000345 RID: 837
		// (get) Token: 0x060007D6 RID: 2006 RVA: 0x00006794 File Offset: 0x00004994
		// (set) Token: 0x060007D7 RID: 2007 RVA: 0x00004644 File Offset: 0x00002844
		public PrintRange PrintRange
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return PrintRange.AllPages;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether the printing output is sent to a file instead of a port.</summary>
		/// <returns>
		///     <see langword="true" /> if the printing output is sent to a file; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000346 RID: 838
		// (get) Token: 0x060007D8 RID: 2008 RVA: 0x000067B0 File Offset: 0x000049B0
		// (set) Token: 0x060007D9 RID: 2009 RVA: 0x00004644 File Offset: 0x00002844
		public bool PrintToFile
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a value indicating whether this printer supports color printing.</summary>
		/// <returns>
		///     <see langword="true" /> if this printer supports color; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000347 RID: 839
		// (get) Token: 0x060007DA RID: 2010 RVA: 0x000067CC File Offset: 0x000049CC
		public bool SupportsColor
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets or sets the number of the last page to print.</summary>
		/// <returns>The number of the last page to print.</returns>
		/// <exception cref="T:System.ArgumentException">The value of the <see cref="P:System.Drawing.Printing.PrinterSettings.ToPage" /> property is less than zero. </exception>
		// Token: 0x17000348 RID: 840
		// (get) Token: 0x060007DB RID: 2011 RVA: 0x000067E8 File Offset: 0x000049E8
		// (set) Token: 0x060007DC RID: 2012 RVA: 0x00004644 File Offset: 0x00002844
		public int ToPage
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Creates a copy of this <see cref="T:System.Drawing.Printing.PrinterSettings" />.</summary>
		/// <returns>A copy of this object.</returns>
		// Token: 0x060007DD RID: 2013 RVA: 0x00004667 File Offset: 0x00002867
		public object Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns a <see cref="T:System.Drawing.Graphics" /> that contains printer information that is useful when creating a <see cref="T:System.Drawing.Printing.PrintDocument" />. </summary>
		/// <returns>A <see cref="T:System.Drawing.Graphics" /> that contains information from a printer.</returns>
		/// <exception cref="T:System.Drawing.Printing.InvalidPrinterException">The printer named in the <see cref="P:System.Drawing.Printing.PrinterSettings.PrinterName" /> property does not exist. </exception>
		// Token: 0x060007DE RID: 2014 RVA: 0x00004667 File Offset: 0x00002867
		public Graphics CreateMeasurementGraphics()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns a <see cref="T:System.Drawing.Graphics" /> that contains printer information, optionally specifying the origin at the margins.</summary>
		/// <param name="honorOriginAtMargins">
		///       <see langword="true" /> to indicate the origin at the margins; otherwise, <see langword="false" />. </param>
		/// <returns>A <see cref="T:System.Drawing.Graphics" /> that contains printer information from the <see cref="T:System.Drawing.Printing.PageSettings" />.</returns>
		// Token: 0x060007DF RID: 2015 RVA: 0x00004667 File Offset: 0x00002867
		public Graphics CreateMeasurementGraphics(bool honorOriginAtMargins)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns a <see cref="T:System.Drawing.Graphics" /> that contains printer information associated with the specified <see cref="T:System.Drawing.Printing.PageSettings" />.</summary>
		/// <param name="pageSettings">The <see cref="T:System.Drawing.Printing.PageSettings" /> to retrieve a graphics object for.</param>
		/// <returns>A <see cref="T:System.Drawing.Graphics" /> that contains printer information from the <see cref="T:System.Drawing.Printing.PageSettings" />.</returns>
		// Token: 0x060007E0 RID: 2016 RVA: 0x00004667 File Offset: 0x00002867
		public Graphics CreateMeasurementGraphics(PageSettings pageSettings)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates a <see cref="T:System.Drawing.Graphics" /> associated with the specified page settings and optionally specifying the origin at the margins.</summary>
		/// <param name="pageSettings">The <see cref="T:System.Drawing.Printing.PageSettings" /> to retrieve a <see cref="T:System.Drawing.Graphics" /> object for.</param>
		/// <param name="honorOriginAtMargins">
		///       <see langword="true" /> to specify the origin at the margins; otherwise, <see langword="false" />. </param>
		/// <returns>A <see cref="T:System.Drawing.Graphics" /> that contains printer information from the <see cref="T:System.Drawing.Printing.PageSettings" />.</returns>
		// Token: 0x060007E1 RID: 2017 RVA: 0x00004667 File Offset: 0x00002867
		public Graphics CreateMeasurementGraphics(PageSettings pageSettings, bool honorOriginAtMargins)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates a handle to a <see langword="DEVMODE" /> structure that corresponds to the printer settings.</summary>
		/// <returns>A handle to a <see langword="DEVMODE" /> structure.</returns>
		/// <exception cref="T:System.Drawing.Printing.InvalidPrinterException">The printer named in the <see cref="P:System.Drawing.Printing.PrinterSettings.PrinterName" /> property does not exist. </exception>
		/// <exception cref="T:System.ComponentModel.Win32Exception">The printer's initialization information could not be retrieved. </exception>
		// Token: 0x060007E2 RID: 2018 RVA: 0x00006804 File Offset: 0x00004A04
		public IntPtr GetHdevmode()
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Creates a handle to a <see langword="DEVMODE" /> structure that corresponds to the printer and the page settings specified through the <paramref name="pageSettings" /> parameter.</summary>
		/// <param name="pageSettings">The <see cref="T:System.Drawing.Printing.PageSettings" /> object that the <see langword="DEVMODE" /> structure's handle corresponds to. </param>
		/// <returns>A handle to a <see langword="DEVMODE" /> structure.</returns>
		/// <exception cref="T:System.Drawing.Printing.InvalidPrinterException">The printer named in the <see cref="P:System.Drawing.Printing.PrinterSettings.PrinterName" /> property does not exist. </exception>
		/// <exception cref="T:System.ComponentModel.Win32Exception">The printer's initialization information could not be retrieved. </exception>
		// Token: 0x060007E3 RID: 2019 RVA: 0x00006820 File Offset: 0x00004A20
		public IntPtr GetHdevmode(PageSettings pageSettings)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Creates a handle to a <see langword="DEVNAMES" /> structure that corresponds to the printer settings.</summary>
		/// <returns>A handle to a <see langword="DEVNAMES" /> structure.</returns>
		// Token: 0x060007E4 RID: 2020 RVA: 0x0000683C File Offset: 0x00004A3C
		public IntPtr GetHdevnames()
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Gets a value indicating whether the printer supports printing the specified image file.</summary>
		/// <param name="image">The image to print.</param>
		/// <returns>
		///     <see langword="true" /> if the printer supports printing the specified image; otherwise, <see langword="false" />.</returns>
		// Token: 0x060007E5 RID: 2021 RVA: 0x00006858 File Offset: 0x00004A58
		public bool IsDirectPrintingSupported(Image image)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a value indicating whether the printer supports printing the specified image format.</summary>
		/// <param name="imageFormat">An <see cref="T:System.Drawing.Imaging.ImageFormat" /> to print.</param>
		/// <returns>
		///     <see langword="true" /> if the printer supports printing the specified image format; otherwise, <see langword="false" />.</returns>
		// Token: 0x060007E6 RID: 2022 RVA: 0x00006874 File Offset: 0x00004A74
		public bool IsDirectPrintingSupported(ImageFormat imageFormat)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Copies the relevant information out of the given handle and into the <see cref="T:System.Drawing.Printing.PrinterSettings" />.</summary>
		/// <param name="hdevmode">The handle to a Win32 <see langword="DEVMODE" /> structure. </param>
		/// <exception cref="T:System.ArgumentException">The printer handle is not valid. </exception>
		// Token: 0x060007E7 RID: 2023 RVA: 0x00004644 File Offset: 0x00002844
		public void SetHdevmode(IntPtr hdevmode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Copies the relevant information out of the given handle and into the <see cref="T:System.Drawing.Printing.PrinterSettings" />.</summary>
		/// <param name="hdevnames">The handle to a Win32 <see langword="DEVNAMES" /> structure. </param>
		/// <exception cref="T:System.ArgumentException">The printer handle is invalid. </exception>
		// Token: 0x060007E8 RID: 2024 RVA: 0x00004644 File Offset: 0x00002844
		public void SetHdevnames(IntPtr hdevnames)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Contains a collection of <see cref="T:System.Drawing.Printing.PaperSize" /> objects.</summary>
		// Token: 0x0200008F RID: 143
		public class PaperSizeCollection : ICollection, IEnumerable
		{
			/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.PrinterSettings.PaperSizeCollection" /> class.</summary>
			/// <param name="array">An array of type <see cref="T:System.Drawing.Printing.PaperSize" />. </param>
			// Token: 0x060007E9 RID: 2025 RVA: 0x00004644 File Offset: 0x00002844
			public PaperSizeCollection(PaperSize[] array)
			{
				ThrowStub.ThrowNotSupportedException();
			}

			/// <summary>Gets the number of different paper sizes in the collection.</summary>
			/// <returns>The number of different paper sizes in the collection.</returns>
			// Token: 0x17000349 RID: 841
			// (get) Token: 0x060007EA RID: 2026 RVA: 0x00006890 File Offset: 0x00004A90
			public int Count
			{
				get
				{
					ThrowStub.ThrowNotSupportedException();
					return 0;
				}
			}

			/// <summary>Gets the <see cref="T:System.Drawing.Printing.PaperSize" /> at a specified index.</summary>
			/// <param name="index">The index of the <see cref="T:System.Drawing.Printing.PaperSize" /> to get. </param>
			/// <returns>The <see cref="T:System.Drawing.Printing.PaperSize" /> at the specified index.</returns>
			// Token: 0x1700034A RID: 842
			public virtual PaperSize this[int index]
			{
				get
				{
					ThrowStub.ThrowNotSupportedException();
					return null;
				}
			}

			// Token: 0x060007EC RID: 2028 RVA: 0x000068AC File Offset: 0x00004AAC
			int ICollection.get_Count()
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}

			// Token: 0x060007ED RID: 2029 RVA: 0x000068C8 File Offset: 0x00004AC8
			bool ICollection.get_IsSynchronized()
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}

			// Token: 0x060007EE RID: 2030 RVA: 0x00004667 File Offset: 0x00002867
			object ICollection.get_SyncRoot()
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}

			/// <summary>Adds a <see cref="T:System.Drawing.Printing.PrinterResolution" /> to the end of the collection.</summary>
			/// <param name="paperSize">The <see cref="T:System.Drawing.Printing.PaperSize" /> to add to the collection.</param>
			/// <returns>The zero-based index of the newly added item.</returns>
			// Token: 0x060007EF RID: 2031 RVA: 0x000068E4 File Offset: 0x00004AE4
			[EditorBrowsable(EditorBrowsableState.Never)]
			public int Add(PaperSize paperSize)
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}

			/// <summary>Copies the contents of the current <see cref="T:System.Drawing.Printing.PrinterSettings.PaperSizeCollection" /> to the specified array, starting at the specified index.</summary>
			/// <param name="paperSizes">A zero-based array that receives the items copied from the <see cref="T:System.Drawing.Printing.PrinterSettings.PaperSizeCollection" />.</param>
			/// <param name="index">The index at which to start copying items.</param>
			// Token: 0x060007F0 RID: 2032 RVA: 0x00004644 File Offset: 0x00002844
			public void CopyTo(PaperSize[] paperSizes, int index)
			{
				ThrowStub.ThrowNotSupportedException();
			}

			/// <summary>Returns an enumerator that can iterate through the collection.</summary>
			/// <returns>An <see cref="T:System.Collections.IEnumerator" /> for the <see cref="T:System.Drawing.Printing.PrinterSettings.PaperSizeCollection" />.</returns>
			// Token: 0x060007F1 RID: 2033 RVA: 0x00004667 File Offset: 0x00002867
			public IEnumerator GetEnumerator()
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}

			/// <summary>For a description of this member, see <see cref="M:System.Collections.ICollection.CopyTo(System.Array,System.Int32)" />.</summary>
			/// <param name="array">A zero-based array that receives the items copied from the collection.</param>
			/// <param name="index">The index at which to start copying items.</param>
			// Token: 0x060007F2 RID: 2034 RVA: 0x00004644 File Offset: 0x00002844
			void ICollection.CopyTo(Array array, int index)
			{
				ThrowStub.ThrowNotSupportedException();
			}

			/// <summary>For a description of this member, see <see cref="M:System.Collections.IEnumerable.GetEnumerator" />.</summary>
			/// <returns>An enumerator associated with the collection.</returns>
			// Token: 0x060007F3 RID: 2035 RVA: 0x00004667 File Offset: 0x00002867
			IEnumerator IEnumerable.GetEnumerator()
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Contains a collection of <see cref="T:System.Drawing.Printing.PaperSource" /> objects.</summary>
		// Token: 0x02000090 RID: 144
		public class PaperSourceCollection : ICollection, IEnumerable
		{
			/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.PrinterSettings.PaperSourceCollection" /> class.</summary>
			/// <param name="array">An array of type <see cref="T:System.Drawing.Printing.PaperSource" />. </param>
			// Token: 0x060007F4 RID: 2036 RVA: 0x00004644 File Offset: 0x00002844
			public PaperSourceCollection(PaperSource[] array)
			{
				ThrowStub.ThrowNotSupportedException();
			}

			/// <summary>Gets the number of different paper sources in the collection.</summary>
			/// <returns>The number of different paper sources in the collection.</returns>
			// Token: 0x1700034B RID: 843
			// (get) Token: 0x060007F5 RID: 2037 RVA: 0x00006900 File Offset: 0x00004B00
			public int Count
			{
				get
				{
					ThrowStub.ThrowNotSupportedException();
					return 0;
				}
			}

			/// <summary>Gets the <see cref="T:System.Drawing.Printing.PaperSource" /> at a specified index.</summary>
			/// <param name="index">The index of the <see cref="T:System.Drawing.Printing.PaperSource" /> to get. </param>
			/// <returns>The <see cref="T:System.Drawing.Printing.PaperSource" /> at the specified index.</returns>
			// Token: 0x1700034C RID: 844
			public virtual PaperSource this[int index]
			{
				get
				{
					ThrowStub.ThrowNotSupportedException();
					return null;
				}
			}

			// Token: 0x060007F7 RID: 2039 RVA: 0x0000691C File Offset: 0x00004B1C
			int ICollection.get_Count()
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}

			// Token: 0x060007F8 RID: 2040 RVA: 0x00006938 File Offset: 0x00004B38
			bool ICollection.get_IsSynchronized()
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}

			// Token: 0x060007F9 RID: 2041 RVA: 0x00004667 File Offset: 0x00002867
			object ICollection.get_SyncRoot()
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}

			/// <summary>Adds the specified <see cref="T:System.Drawing.Printing.PaperSource" /> to end of the <see cref="T:System.Drawing.Printing.PrinterSettings.PaperSourceCollection" />.</summary>
			/// <param name="paperSource">The <see cref="T:System.Drawing.Printing.PaperSource" /> to add to the collection.</param>
			/// <returns>The zero-based index where the <see cref="T:System.Drawing.Printing.PaperSource" /> was added.</returns>
			// Token: 0x060007FA RID: 2042 RVA: 0x00006954 File Offset: 0x00004B54
			[EditorBrowsable(EditorBrowsableState.Never)]
			public int Add(PaperSource paperSource)
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}

			/// <summary>Copies the contents of the current <see cref="T:System.Drawing.Printing.PrinterSettings.PaperSourceCollection" /> to the specified array, starting at the specified index.</summary>
			/// <param name="paperSources">A zero-based array that receives the items copied from the <see cref="T:System.Drawing.Printing.PrinterSettings.PaperSourceCollection" />.</param>
			/// <param name="index">The index at which to start copying items.</param>
			// Token: 0x060007FB RID: 2043 RVA: 0x00004644 File Offset: 0x00002844
			public void CopyTo(PaperSource[] paperSources, int index)
			{
				ThrowStub.ThrowNotSupportedException();
			}

			/// <summary>Returns an enumerator that can iterate through the collection.</summary>
			/// <returns>An <see cref="T:System.Collections.IEnumerator" /> for the <see cref="T:System.Drawing.Printing.PrinterSettings.PaperSourceCollection" />.</returns>
			// Token: 0x060007FC RID: 2044 RVA: 0x00004667 File Offset: 0x00002867
			public IEnumerator GetEnumerator()
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}

			/// <summary>For a description of this member, see <see cref="M:System.Collections.ICollection.CopyTo(System.Array,System.Int32)" />.</summary>
			/// <param name="array">The destination array for the contents of the collection.</param>
			/// <param name="index">The index at which to start the copy operation.</param>
			// Token: 0x060007FD RID: 2045 RVA: 0x00004644 File Offset: 0x00002844
			void ICollection.CopyTo(Array array, int index)
			{
				ThrowStub.ThrowNotSupportedException();
			}

			/// <summary>For a description of this member, see <see cref="M:System.Collections.IEnumerable.GetEnumerator" />.</summary>
			// Token: 0x060007FE RID: 2046 RVA: 0x00004667 File Offset: 0x00002867
			IEnumerator IEnumerable.GetEnumerator()
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Contains a collection of <see cref="T:System.Drawing.Printing.PrinterResolution" /> objects.</summary>
		// Token: 0x02000091 RID: 145
		public class PrinterResolutionCollection : ICollection, IEnumerable
		{
			/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.PrinterSettings.PrinterResolutionCollection" /> class.</summary>
			/// <param name="array">An array of type <see cref="T:System.Drawing.Printing.PrinterResolution" />. </param>
			// Token: 0x060007FF RID: 2047 RVA: 0x00004644 File Offset: 0x00002844
			public PrinterResolutionCollection(PrinterResolution[] array)
			{
				ThrowStub.ThrowNotSupportedException();
			}

			/// <summary>Gets the number of available printer resolutions in the collection.</summary>
			/// <returns>The number of available printer resolutions in the collection.</returns>
			// Token: 0x1700034D RID: 845
			// (get) Token: 0x06000800 RID: 2048 RVA: 0x00006970 File Offset: 0x00004B70
			public int Count
			{
				get
				{
					ThrowStub.ThrowNotSupportedException();
					return 0;
				}
			}

			/// <summary>Gets the <see cref="T:System.Drawing.Printing.PrinterResolution" /> at a specified index.</summary>
			/// <param name="index">The index of the <see cref="T:System.Drawing.Printing.PrinterResolution" /> to get. </param>
			/// <returns>The <see cref="T:System.Drawing.Printing.PrinterResolution" /> at the specified index.</returns>
			// Token: 0x1700034E RID: 846
			public virtual PrinterResolution this[int index]
			{
				get
				{
					ThrowStub.ThrowNotSupportedException();
					return null;
				}
			}

			// Token: 0x06000802 RID: 2050 RVA: 0x0000698C File Offset: 0x00004B8C
			int ICollection.get_Count()
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}

			// Token: 0x06000803 RID: 2051 RVA: 0x000069A8 File Offset: 0x00004BA8
			bool ICollection.get_IsSynchronized()
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}

			// Token: 0x06000804 RID: 2052 RVA: 0x00004667 File Offset: 0x00002867
			object ICollection.get_SyncRoot()
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}

			/// <summary>Adds a <see cref="T:System.Drawing.Printing.PrinterResolution" /> to the end of the collection.</summary>
			/// <param name="printerResolution">The <see cref="T:System.Drawing.Printing.PrinterResolution" /> to add to the collection.</param>
			/// <returns>The zero-based index of the newly added item.</returns>
			// Token: 0x06000805 RID: 2053 RVA: 0x000069C4 File Offset: 0x00004BC4
			[EditorBrowsable(EditorBrowsableState.Never)]
			public int Add(PrinterResolution printerResolution)
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}

			/// <summary>Copies the contents of the current <see cref="T:System.Drawing.Printing.PrinterSettings.PrinterResolutionCollection" /> to the specified array, starting at the specified index.</summary>
			/// <param name="printerResolutions">A zero-based array that receives the items copied from the <see cref="T:System.Drawing.Printing.PrinterSettings.PrinterResolutionCollection" />.</param>
			/// <param name="index">The index at which to start copying items.</param>
			// Token: 0x06000806 RID: 2054 RVA: 0x00004644 File Offset: 0x00002844
			public void CopyTo(PrinterResolution[] printerResolutions, int index)
			{
				ThrowStub.ThrowNotSupportedException();
			}

			/// <summary>Returns an enumerator that can iterate through the collection.</summary>
			/// <returns>An <see cref="T:System.Collections.IEnumerator" /> for the <see cref="T:System.Drawing.Printing.PrinterSettings.PrinterResolutionCollection" />.</returns>
			// Token: 0x06000807 RID: 2055 RVA: 0x00004667 File Offset: 0x00002867
			public IEnumerator GetEnumerator()
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}

			/// <summary>For a description of this member, see <see cref="M:System.Collections.ICollection.CopyTo(System.Array,System.Int32)" />.</summary>
			/// <param name="array">The destination array.</param>
			/// <param name="index">The index at which to start the copy operation.</param>
			// Token: 0x06000808 RID: 2056 RVA: 0x00004644 File Offset: 0x00002844
			void ICollection.CopyTo(Array array, int index)
			{
				ThrowStub.ThrowNotSupportedException();
			}

			/// <summary>For a description of this member, see <see cref="M:System.Collections.IEnumerable.GetEnumerator" />.</summary>
			// Token: 0x06000809 RID: 2057 RVA: 0x00004667 File Offset: 0x00002867
			IEnumerator IEnumerable.GetEnumerator()
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Contains a collection of <see cref="T:System.String" /> objects.</summary>
		// Token: 0x02000092 RID: 146
		public class StringCollection : ICollection, IEnumerable
		{
			/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.PrinterSettings.StringCollection" /> class.</summary>
			/// <param name="array">An array of type <see cref="T:System.String" />. </param>
			// Token: 0x0600080A RID: 2058 RVA: 0x00004644 File Offset: 0x00002844
			public StringCollection(string[] array)
			{
				ThrowStub.ThrowNotSupportedException();
			}

			/// <summary>Gets the number of strings in the collection.</summary>
			/// <returns>The number of strings in the collection.</returns>
			// Token: 0x1700034F RID: 847
			// (get) Token: 0x0600080B RID: 2059 RVA: 0x000069E0 File Offset: 0x00004BE0
			public int Count
			{
				get
				{
					ThrowStub.ThrowNotSupportedException();
					return 0;
				}
			}

			/// <summary>Gets the <see cref="T:System.String" /> at a specified index.</summary>
			/// <param name="index">The index of the <see cref="T:System.String" /> to get. </param>
			/// <returns>The <see cref="T:System.String" /> at the specified index.</returns>
			// Token: 0x17000350 RID: 848
			public virtual string this[int index]
			{
				get
				{
					ThrowStub.ThrowNotSupportedException();
					return null;
				}
			}

			// Token: 0x0600080D RID: 2061 RVA: 0x000069FC File Offset: 0x00004BFC
			int ICollection.get_Count()
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}

			// Token: 0x0600080E RID: 2062 RVA: 0x00006A18 File Offset: 0x00004C18
			bool ICollection.get_IsSynchronized()
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}

			// Token: 0x0600080F RID: 2063 RVA: 0x00004667 File Offset: 0x00002867
			object ICollection.get_SyncRoot()
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}

			/// <summary>Adds a string to the end of the collection.</summary>
			/// <param name="value">The string to add to the collection.</param>
			/// <returns>The zero-based index of the newly added item.</returns>
			// Token: 0x06000810 RID: 2064 RVA: 0x00006A34 File Offset: 0x00004C34
			[EditorBrowsable(EditorBrowsableState.Never)]
			public int Add(string value)
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}

			/// <summary>Copies the contents of the current <see cref="T:System.Drawing.Printing.PrinterSettings.PrinterResolutionCollection" /> to the specified array, starting at the specified index</summary>
			/// <param name="strings">A zero-based array that receives the items copied from the <see cref="T:System.Drawing.Printing.PrinterSettings.StringCollection" />.</param>
			/// <param name="index">The index at which to start copying items.</param>
			// Token: 0x06000811 RID: 2065 RVA: 0x00004644 File Offset: 0x00002844
			public void CopyTo(string[] strings, int index)
			{
				ThrowStub.ThrowNotSupportedException();
			}

			/// <summary>Returns an enumerator that can iterate through the collection.</summary>
			/// <returns>An <see cref="T:System.Collections.IEnumerator" /> for the <see cref="T:System.Drawing.Printing.PrinterSettings.StringCollection" />.</returns>
			// Token: 0x06000812 RID: 2066 RVA: 0x00004667 File Offset: 0x00002867
			public IEnumerator GetEnumerator()
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}

			/// <summary>For a description of this member, see <see cref="M:System.Collections.ICollection.CopyTo(System.Array,System.Int32)" />.</summary>
			/// <param name="array">The array for items to be copied to.</param>
			/// <param name="index">The starting index.</param>
			// Token: 0x06000813 RID: 2067 RVA: 0x00004644 File Offset: 0x00002844
			void ICollection.CopyTo(Array array, int index)
			{
				ThrowStub.ThrowNotSupportedException();
			}

			/// <summary>For a description of this member, see <see cref="M:System.Collections.IEnumerable.GetEnumerator" />.</summary>
			// Token: 0x06000814 RID: 2068 RVA: 0x00004667 File Offset: 0x00002867
			IEnumerator IEnumerable.GetEnumerator()
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
