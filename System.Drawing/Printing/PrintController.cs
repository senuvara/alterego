﻿using System;
using Unity;

namespace System.Drawing.Printing
{
	/// <summary>Controls how a document is printed, when printing from a Windows Forms application.</summary>
	// Token: 0x0200009F RID: 159
	public abstract class PrintController
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.PrintController" /> class.</summary>
		// Token: 0x0600085E RID: 2142 RVA: 0x00004644 File Offset: 0x00002844
		protected PrintController()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a value indicating whether the <see cref="T:System.Drawing.Printing.PrintController" /> is used for print preview.</summary>
		/// <returns>
		///     <see langword="false" /> in all cases.</returns>
		// Token: 0x1700036F RID: 879
		// (get) Token: 0x0600085F RID: 2143 RVA: 0x00006CF0 File Offset: 0x00004EF0
		public virtual bool IsPreview
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>When overridden in a derived class, completes the control sequence that determines when and how to print a page of a document.</summary>
		/// <param name="document">A <see cref="T:System.Drawing.Printing.PrintDocument" /> that represents the document currently being printed. </param>
		/// <param name="e">A <see cref="T:System.Drawing.Printing.PrintPageEventArgs" /> that contains the event data. </param>
		// Token: 0x06000860 RID: 2144 RVA: 0x00004644 File Offset: 0x00002844
		public virtual void OnEndPage(PrintDocument document, PrintPageEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>When overridden in a derived class, completes the control sequence that determines when and how to print a document.</summary>
		/// <param name="document">A <see cref="T:System.Drawing.Printing.PrintDocument" /> that represents the document currently being printed. </param>
		/// <param name="e">A <see cref="T:System.Drawing.Printing.PrintEventArgs" /> that contains the event data. </param>
		// Token: 0x06000861 RID: 2145 RVA: 0x00004644 File Offset: 0x00002844
		public virtual void OnEndPrint(PrintDocument document, PrintEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>When overridden in a derived class, begins the control sequence that determines when and how to print a page of a document.</summary>
		/// <param name="document">A <see cref="T:System.Drawing.Printing.PrintDocument" /> that represents the document currently being printed. </param>
		/// <param name="e">A <see cref="T:System.Drawing.Printing.PrintPageEventArgs" /> that contains the event data. </param>
		/// <returns>A <see cref="T:System.Drawing.Graphics" /> that represents a page from a <see cref="T:System.Drawing.Printing.PrintDocument" />.</returns>
		// Token: 0x06000862 RID: 2146 RVA: 0x00004667 File Offset: 0x00002867
		public virtual Graphics OnStartPage(PrintDocument document, PrintPageEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>When overridden in a derived class, begins the control sequence that determines when and how to print a document.</summary>
		/// <param name="document">A <see cref="T:System.Drawing.Printing.PrintDocument" /> that represents the document currently being printed. </param>
		/// <param name="e">A <see cref="T:System.Drawing.Printing.PrintEventArgs" /> that contains the event data. </param>
		// Token: 0x06000863 RID: 2147 RVA: 0x00004644 File Offset: 0x00002844
		public virtual void OnStartPrint(PrintDocument document, PrintEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
