﻿using System;
using Unity;

namespace System.Drawing.Printing
{
	/// <summary>Specifies a print controller that sends information to a printer.</summary>
	// Token: 0x020000AD RID: 173
	public class StandardPrintController : PrintController
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.StandardPrintController" /> class. </summary>
		// Token: 0x060008AB RID: 2219 RVA: 0x00004644 File Offset: 0x00002844
		public StandardPrintController()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Completes the control sequence that determines when and how to print a page of a document.</summary>
		/// <param name="document">A <see cref="T:System.Drawing.Printing.PrintDocument" /> that represents the document being printed. </param>
		/// <param name="e">A <see cref="T:System.Drawing.Printing.PrintPageEventArgs" /> that contains data about how to print a page in the document. </param>
		/// <exception cref="T:System.ComponentModel.Win32Exception">The native Win32 Application Programming Interface (API) could not finish writing to a page. </exception>
		// Token: 0x060008AC RID: 2220 RVA: 0x00004644 File Offset: 0x00002844
		public override void OnEndPage(PrintDocument document, PrintPageEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Completes the control sequence that determines when and how to print a document.</summary>
		/// <param name="document">A <see cref="T:System.Drawing.Printing.PrintDocument" /> that represents the document being printed. </param>
		/// <param name="e">A <see cref="T:System.Drawing.Printing.PrintEventArgs" /> that contains data about how to print the document. </param>
		/// <exception cref="T:System.ComponentModel.Win32Exception">The native Win32 Application Programming Interface (API) could not complete the print job.-or- The native Win32 API could not delete the specified device context (DC). </exception>
		// Token: 0x060008AD RID: 2221 RVA: 0x00004644 File Offset: 0x00002844
		public override void OnEndPrint(PrintDocument document, PrintEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Begins the control sequence that determines when and how to print a page in a document.</summary>
		/// <param name="document">A <see cref="T:System.Drawing.Printing.PrintDocument" /> that represents the document being printed. </param>
		/// <param name="e">A <see cref="T:System.Drawing.Printing.PrintPageEventArgs" /> that contains data about how to print a page in the document. Initially, the <see cref="P:System.Drawing.Printing.PrintPageEventArgs.Graphics" /> property of this parameter will be <see langword="null" />. The value returned from the <see cref="M:System.Drawing.Printing.StandardPrintController.OnStartPage(System.Drawing.Printing.PrintDocument,System.Drawing.Printing.PrintPageEventArgs)" /> method will be used to set this property. </param>
		/// <returns>A <see cref="T:System.Drawing.Graphics" /> object that represents a page from a <see cref="T:System.Drawing.Printing.PrintDocument" />.</returns>
		/// <exception cref="T:System.ComponentModel.Win32Exception">The native Win32 Application Programming Interface (API) could not prepare the printer driver to accept data.-or- The native Win32 API could not update the specified printer or plotter device context (DC) using the specified information. </exception>
		// Token: 0x060008AE RID: 2222 RVA: 0x00004667 File Offset: 0x00002867
		public override Graphics OnStartPage(PrintDocument document, PrintPageEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Begins the control sequence that determines when and how to print a document.</summary>
		/// <param name="document">A <see cref="T:System.Drawing.Printing.PrintDocument" /> that represents the document being printed. </param>
		/// <param name="e">A <see cref="T:System.Drawing.Printing.PrintEventArgs" /> that contains data about how to print the document. </param>
		/// <exception cref="T:System.Drawing.Printing.InvalidPrinterException">The printer settings are not valid. </exception>
		/// <exception cref="T:System.ComponentModel.Win32Exception">The native Win32 Application Programming Interface (API) could not start a print job. </exception>
		// Token: 0x060008AF RID: 2223 RVA: 0x00004644 File Offset: 0x00002844
		public override void OnStartPrint(PrintDocument document, PrintEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
