﻿using System;

namespace System.Drawing.Printing
{
	/// <summary>Standard paper sources.</summary>
	// Token: 0x02000096 RID: 150
	[Serializable]
	public enum PaperSourceKind
	{
		/// <summary>Automatically fed paper.</summary>
		// Token: 0x040003AE RID: 942
		AutomaticFeed = 7,
		/// <summary>A paper cassette.</summary>
		// Token: 0x040003AF RID: 943
		Cassette = 14,
		/// <summary>A printer-specific paper source.</summary>
		// Token: 0x040003B0 RID: 944
		Custom = 257,
		/// <summary>An envelope.</summary>
		// Token: 0x040003B1 RID: 945
		Envelope = 5,
		/// <summary>The printer's default input bin.</summary>
		// Token: 0x040003B2 RID: 946
		FormSource = 15,
		/// <summary>The printer's large-capacity bin.</summary>
		// Token: 0x040003B3 RID: 947
		LargeCapacity = 11,
		/// <summary>Large-format paper.</summary>
		// Token: 0x040003B4 RID: 948
		LargeFormat = 10,
		/// <summary>The lower bin of a printer.</summary>
		// Token: 0x040003B5 RID: 949
		Lower = 2,
		/// <summary>Manually fed paper.</summary>
		// Token: 0x040003B6 RID: 950
		Manual = 4,
		/// <summary>Manually fed envelope.</summary>
		// Token: 0x040003B7 RID: 951
		ManualFeed = 6,
		/// <summary>The middle bin of a printer.</summary>
		// Token: 0x040003B8 RID: 952
		Middle = 3,
		/// <summary>Small-format paper.</summary>
		// Token: 0x040003B9 RID: 953
		SmallFormat = 9,
		/// <summary>A tractor feed.</summary>
		// Token: 0x040003BA RID: 954
		TractorFeed = 8,
		/// <summary>The upper bin of a printer (or the default bin, if the printer only has one bin).</summary>
		// Token: 0x040003BB RID: 955
		Upper = 1
	}
}
