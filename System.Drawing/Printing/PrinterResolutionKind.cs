﻿using System;

namespace System.Drawing.Printing
{
	/// <summary>Specifies a printer resolution.</summary>
	// Token: 0x02000098 RID: 152
	[Serializable]
	public enum PrinterResolutionKind
	{
		/// <summary>Custom resolution.</summary>
		// Token: 0x040003BD RID: 957
		Custom,
		/// <summary>Draft-quality resolution.</summary>
		// Token: 0x040003BE RID: 958
		Draft = -1,
		/// <summary>High resolution.</summary>
		// Token: 0x040003BF RID: 959
		High = -4,
		/// <summary>Low resolution.</summary>
		// Token: 0x040003C0 RID: 960
		Low = -2,
		/// <summary>Medium resolution.</summary>
		// Token: 0x040003C1 RID: 961
		Medium = -3
	}
}
