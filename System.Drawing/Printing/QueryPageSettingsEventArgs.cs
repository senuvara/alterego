﻿using System;
using Unity;

namespace System.Drawing.Printing
{
	/// <summary>Provides data for the <see cref="E:System.Drawing.Printing.PrintDocument.QueryPageSettings" /> event.</summary>
	// Token: 0x020000A7 RID: 167
	public class QueryPageSettingsEventArgs : PrintEventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.QueryPageSettingsEventArgs" /> class.</summary>
		/// <param name="pageSettings">The page settings for the page to be printed. </param>
		// Token: 0x06000893 RID: 2195 RVA: 0x00004644 File Offset: 0x00002844
		public QueryPageSettingsEventArgs(PageSettings pageSettings)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the page settings for the page to be printed.</summary>
		/// <returns>The page settings for the page to be printed.</returns>
		// Token: 0x1700037C RID: 892
		// (get) Token: 0x06000894 RID: 2196 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000895 RID: 2197 RVA: 0x00004644 File Offset: 0x00002844
		public PageSettings PageSettings
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
