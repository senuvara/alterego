﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Drawing.Printing
{
	/// <summary>Provides data for the <see cref="E:System.Drawing.Printing.PrintDocument.BeginPrint" /> and <see cref="E:System.Drawing.Printing.PrintDocument.EndPrint" /> events.</summary>
	// Token: 0x020000A2 RID: 162
	public class PrintEventArgs : CancelEventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.PrintEventArgs" /> class.</summary>
		// Token: 0x06000880 RID: 2176 RVA: 0x00004644 File Offset: 0x00002844
		public PrintEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns <see cref="F:System.Drawing.Printing.PrintAction.PrintToFile" /> in all cases.</summary>
		/// <returns>
		///     <see cref="F:System.Drawing.Printing.PrintAction.PrintToFile" /> in all cases.</returns>
		// Token: 0x17000375 RID: 885
		// (get) Token: 0x06000881 RID: 2177 RVA: 0x00006D28 File Offset: 0x00004F28
		public PrintAction PrintAction
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return PrintAction.PrintToFile;
			}
		}
	}
}
