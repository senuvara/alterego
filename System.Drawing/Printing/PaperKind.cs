﻿using System;

namespace System.Drawing.Printing
{
	/// <summary>Specifies the standard paper sizes.</summary>
	// Token: 0x02000094 RID: 148
	[Serializable]
	public enum PaperKind
	{
		/// <summary>A2 paper (420 mm by 594 mm).</summary>
		// Token: 0x04000338 RID: 824
		A2 = 66,
		/// <summary>A3 paper (297 mm by 420 mm).</summary>
		// Token: 0x04000339 RID: 825
		A3 = 8,
		/// <summary>A3 extra paper (322 mm by 445 mm).</summary>
		// Token: 0x0400033A RID: 826
		A3Extra = 63,
		/// <summary>A3 extra transverse paper (322 mm by 445 mm).</summary>
		// Token: 0x0400033B RID: 827
		A3ExtraTransverse = 68,
		/// <summary>A3 rotated paper (420 mm by 297 mm).</summary>
		// Token: 0x0400033C RID: 828
		A3Rotated = 76,
		/// <summary>A3 transverse paper (297 mm by 420 mm).</summary>
		// Token: 0x0400033D RID: 829
		A3Transverse = 67,
		/// <summary>A4 paper (210 mm by 297 mm).</summary>
		// Token: 0x0400033E RID: 830
		A4 = 9,
		/// <summary>A4 extra paper (236 mm by 322 mm). This value is specific to the PostScript driver and is used only by Linotronic printers to help save paper.</summary>
		// Token: 0x0400033F RID: 831
		A4Extra = 53,
		/// <summary>A4 plus paper (210 mm by 330 mm).</summary>
		// Token: 0x04000340 RID: 832
		A4Plus = 60,
		/// <summary>A4 rotated paper (297 mm by 210 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000341 RID: 833
		A4Rotated = 77,
		/// <summary>A4 small paper (210 mm by 297 mm).</summary>
		// Token: 0x04000342 RID: 834
		A4Small = 10,
		/// <summary>A4 transverse paper (210 mm by 297 mm).</summary>
		// Token: 0x04000343 RID: 835
		A4Transverse = 55,
		/// <summary>A5 paper (148 mm by 210 mm).</summary>
		// Token: 0x04000344 RID: 836
		A5 = 11,
		/// <summary>A5 extra paper (174 mm by 235 mm).</summary>
		// Token: 0x04000345 RID: 837
		A5Extra = 64,
		/// <summary>A5 rotated paper (210 mm by 148 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000346 RID: 838
		A5Rotated = 78,
		/// <summary>A5 transverse paper (148 mm by 210 mm).</summary>
		// Token: 0x04000347 RID: 839
		A5Transverse = 61,
		/// <summary>A6 paper (105 mm by 148 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000348 RID: 840
		A6 = 70,
		/// <summary>A6 rotated paper (148 mm by 105 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000349 RID: 841
		A6Rotated = 83,
		/// <summary>SuperA/SuperA/A4 paper (227 mm by 356 mm).</summary>
		// Token: 0x0400034A RID: 842
		APlus = 57,
		/// <summary>B4 paper (250 mm by 353 mm).</summary>
		// Token: 0x0400034B RID: 843
		B4 = 12,
		/// <summary>B4 envelope (250 mm by 353 mm).</summary>
		// Token: 0x0400034C RID: 844
		B4Envelope = 33,
		/// <summary>JIS B4 rotated paper (364 mm by 257 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x0400034D RID: 845
		B4JisRotated = 79,
		/// <summary>B5 paper (176 mm by 250 mm).</summary>
		// Token: 0x0400034E RID: 846
		B5 = 13,
		/// <summary>B5 envelope (176 mm by 250 mm).</summary>
		// Token: 0x0400034F RID: 847
		B5Envelope = 34,
		/// <summary>ISO B5 extra paper (201 mm by 276 mm).</summary>
		// Token: 0x04000350 RID: 848
		B5Extra = 65,
		/// <summary>JIS B5 rotated paper (257 mm by 182 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000351 RID: 849
		B5JisRotated = 80,
		/// <summary>JIS B5 transverse paper (182 mm by 257 mm).</summary>
		// Token: 0x04000352 RID: 850
		B5Transverse = 62,
		/// <summary>B6 envelope (176 mm by 125 mm).</summary>
		// Token: 0x04000353 RID: 851
		B6Envelope = 35,
		/// <summary>JIS B6 paper (128 mm by 182 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000354 RID: 852
		B6Jis = 88,
		/// <summary>JIS B6 rotated paper (182 mm by 128 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000355 RID: 853
		B6JisRotated,
		/// <summary>SuperB/SuperB/A3 paper (305 mm by 487 mm).</summary>
		// Token: 0x04000356 RID: 854
		BPlus = 58,
		/// <summary>C3 envelope (324 mm by 458 mm).</summary>
		// Token: 0x04000357 RID: 855
		C3Envelope = 29,
		/// <summary>C4 envelope (229 mm by 324 mm).</summary>
		// Token: 0x04000358 RID: 856
		C4Envelope,
		/// <summary>C5 envelope (162 mm by 229 mm).</summary>
		// Token: 0x04000359 RID: 857
		C5Envelope = 28,
		/// <summary>C65 envelope (114 mm by 229 mm).</summary>
		// Token: 0x0400035A RID: 858
		C65Envelope = 32,
		/// <summary>C6 envelope (114 mm by 162 mm).</summary>
		// Token: 0x0400035B RID: 859
		C6Envelope = 31,
		/// <summary>C paper (17 in. by 22 in.).</summary>
		// Token: 0x0400035C RID: 860
		CSheet = 24,
		/// <summary>The paper size is defined by the user.</summary>
		// Token: 0x0400035D RID: 861
		Custom = 0,
		/// <summary>DL envelope (110 mm by 220 mm).</summary>
		// Token: 0x0400035E RID: 862
		DLEnvelope = 27,
		/// <summary>D paper (22 in. by 34 in.).</summary>
		// Token: 0x0400035F RID: 863
		DSheet = 25,
		/// <summary>E paper (34 in. by 44 in.).</summary>
		// Token: 0x04000360 RID: 864
		ESheet,
		/// <summary>Executive paper (7.25 in. by 10.5 in.).</summary>
		// Token: 0x04000361 RID: 865
		Executive = 7,
		/// <summary>Folio paper (8.5 in. by 13 in.).</summary>
		// Token: 0x04000362 RID: 866
		Folio = 14,
		/// <summary>German legal fanfold (8.5 in. by 13 in.).</summary>
		// Token: 0x04000363 RID: 867
		GermanLegalFanfold = 41,
		/// <summary>German standard fanfold (8.5 in. by 12 in.).</summary>
		// Token: 0x04000364 RID: 868
		GermanStandardFanfold = 40,
		/// <summary>Invitation envelope (220 mm by 220 mm).</summary>
		// Token: 0x04000365 RID: 869
		InviteEnvelope = 47,
		/// <summary>ISO B4 (250 mm by 353 mm).</summary>
		// Token: 0x04000366 RID: 870
		IsoB4 = 42,
		/// <summary>Italy envelope (110 mm by 230 mm).</summary>
		// Token: 0x04000367 RID: 871
		ItalyEnvelope = 36,
		/// <summary>Japanese double postcard (200 mm by 148 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000368 RID: 872
		JapaneseDoublePostcard = 69,
		/// <summary>Japanese rotated double postcard (148 mm by 200 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000369 RID: 873
		JapaneseDoublePostcardRotated = 82,
		/// <summary>Japanese Chou #3 envelope. Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x0400036A RID: 874
		JapaneseEnvelopeChouNumber3 = 73,
		/// <summary>Japanese rotated Chou #3 envelope. Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x0400036B RID: 875
		JapaneseEnvelopeChouNumber3Rotated = 86,
		/// <summary>Japanese Chou #4 envelope. Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x0400036C RID: 876
		JapaneseEnvelopeChouNumber4 = 74,
		/// <summary>Japanese rotated Chou #4 envelope. Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x0400036D RID: 877
		JapaneseEnvelopeChouNumber4Rotated = 87,
		/// <summary>Japanese Kaku #2 envelope. Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x0400036E RID: 878
		JapaneseEnvelopeKakuNumber2 = 71,
		/// <summary>Japanese rotated Kaku #2 envelope. Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x0400036F RID: 879
		JapaneseEnvelopeKakuNumber2Rotated = 84,
		/// <summary>Japanese Kaku #3 envelope. Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000370 RID: 880
		JapaneseEnvelopeKakuNumber3 = 72,
		/// <summary>Japanese rotated Kaku #3 envelope. Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000371 RID: 881
		JapaneseEnvelopeKakuNumber3Rotated = 85,
		/// <summary>Japanese You #4 envelope. Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000372 RID: 882
		JapaneseEnvelopeYouNumber4 = 91,
		/// <summary>Japanese You #4 rotated envelope. Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000373 RID: 883
		JapaneseEnvelopeYouNumber4Rotated,
		/// <summary>Japanese postcard (100 mm by 148 mm).</summary>
		// Token: 0x04000374 RID: 884
		JapanesePostcard = 43,
		/// <summary>Japanese rotated postcard (148 mm by 100 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000375 RID: 885
		JapanesePostcardRotated = 81,
		/// <summary>Ledger paper (17 in. by 11 in.).</summary>
		// Token: 0x04000376 RID: 886
		Ledger = 4,
		/// <summary>Legal paper (8.5 in. by 14 in.).</summary>
		// Token: 0x04000377 RID: 887
		Legal,
		/// <summary>Legal extra paper (9.275 in. by 15 in.). This value is specific to the PostScript driver and is used only by Linotronic printers in order to conserve paper.</summary>
		// Token: 0x04000378 RID: 888
		LegalExtra = 51,
		/// <summary>Letter paper (8.5 in. by 11 in.).</summary>
		// Token: 0x04000379 RID: 889
		Letter = 1,
		/// <summary>Letter extra paper (9.275 in. by 12 in.). This value is specific to the PostScript driver and is used only by Linotronic printers in order to conserve paper.</summary>
		// Token: 0x0400037A RID: 890
		LetterExtra = 50,
		/// <summary>Letter extra transverse paper (9.275 in. by 12 in.).</summary>
		// Token: 0x0400037B RID: 891
		LetterExtraTransverse = 56,
		/// <summary>Letter plus paper (8.5 in. by 12.69 in.).</summary>
		// Token: 0x0400037C RID: 892
		LetterPlus = 59,
		/// <summary>Letter rotated paper (11 in. by 8.5 in.).</summary>
		// Token: 0x0400037D RID: 893
		LetterRotated = 75,
		/// <summary>Letter small paper (8.5 in. by 11 in.).</summary>
		// Token: 0x0400037E RID: 894
		LetterSmall = 2,
		/// <summary>Letter transverse paper (8.275 in. by 11 in.).</summary>
		// Token: 0x0400037F RID: 895
		LetterTransverse = 54,
		/// <summary>Monarch envelope (3.875 in. by 7.5 in.).</summary>
		// Token: 0x04000380 RID: 896
		MonarchEnvelope = 37,
		/// <summary>Note paper (8.5 in. by 11 in.).</summary>
		// Token: 0x04000381 RID: 897
		Note = 18,
		/// <summary>#10 envelope (4.125 in. by 9.5 in.).</summary>
		// Token: 0x04000382 RID: 898
		Number10Envelope = 20,
		/// <summary>#11 envelope (4.5 in. by 10.375 in.).</summary>
		// Token: 0x04000383 RID: 899
		Number11Envelope,
		/// <summary>#12 envelope (4.75 in. by 11 in.).</summary>
		// Token: 0x04000384 RID: 900
		Number12Envelope,
		/// <summary>#14 envelope (5 in. by 11.5 in.).</summary>
		// Token: 0x04000385 RID: 901
		Number14Envelope,
		/// <summary>#9 envelope (3.875 in. by 8.875 in.).</summary>
		// Token: 0x04000386 RID: 902
		Number9Envelope = 19,
		/// <summary>6 3/4 envelope (3.625 in. by 6.5 in.).</summary>
		// Token: 0x04000387 RID: 903
		PersonalEnvelope = 38,
		/// <summary> 16K paper (146 mm by 215 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000388 RID: 904
		Prc16K = 93,
		/// <summary> 16K rotated paper (146 mm by 215 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000389 RID: 905
		Prc16KRotated = 106,
		/// <summary> 32K paper (97 mm by 151 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x0400038A RID: 906
		Prc32K = 94,
		/// <summary> 32K big paper (97 mm by 151 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x0400038B RID: 907
		Prc32KBig,
		/// <summary> 32K big rotated paper (97 mm by 151 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x0400038C RID: 908
		Prc32KBigRotated = 108,
		/// <summary> 32K rotated paper (97 mm by 151 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x0400038D RID: 909
		Prc32KRotated = 107,
		/// <summary> #1 envelope (102 mm by 165 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x0400038E RID: 910
		PrcEnvelopeNumber1 = 96,
		/// <summary> #10 envelope (324 mm by 458 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x0400038F RID: 911
		PrcEnvelopeNumber10 = 105,
		/// <summary> #10 rotated envelope (458 mm by 324 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000390 RID: 912
		PrcEnvelopeNumber10Rotated = 118,
		/// <summary> #1 rotated envelope (165 mm by 102 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000391 RID: 913
		PrcEnvelopeNumber1Rotated = 109,
		/// <summary> #2 envelope (102 mm by 176 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000392 RID: 914
		PrcEnvelopeNumber2 = 97,
		/// <summary> #2 rotated envelope (176 mm by 102 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000393 RID: 915
		PrcEnvelopeNumber2Rotated = 110,
		/// <summary> #3 envelope (125 mm by 176 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000394 RID: 916
		PrcEnvelopeNumber3 = 98,
		/// <summary> #3 rotated envelope (176 mm by 125 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000395 RID: 917
		PrcEnvelopeNumber3Rotated = 111,
		/// <summary> #4 envelope (110 mm by 208 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000396 RID: 918
		PrcEnvelopeNumber4 = 99,
		/// <summary> #4 rotated envelope (208 mm by 110 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000397 RID: 919
		PrcEnvelopeNumber4Rotated = 112,
		/// <summary> #5 envelope (110 mm by 220 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000398 RID: 920
		PrcEnvelopeNumber5 = 100,
		/// <summary> Envelope #5 rotated envelope (220 mm by 110 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x04000399 RID: 921
		PrcEnvelopeNumber5Rotated = 113,
		/// <summary> #6 envelope (120 mm by 230 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x0400039A RID: 922
		PrcEnvelopeNumber6 = 101,
		/// <summary> #6 rotated envelope (230 mm by 120 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x0400039B RID: 923
		PrcEnvelopeNumber6Rotated = 114,
		/// <summary> #7 envelope (160 mm by 230 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x0400039C RID: 924
		PrcEnvelopeNumber7 = 102,
		/// <summary> #7 rotated envelope (230 mm by 160 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x0400039D RID: 925
		PrcEnvelopeNumber7Rotated = 115,
		/// <summary> #8 envelope (120 mm by 309 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x0400039E RID: 926
		PrcEnvelopeNumber8 = 103,
		/// <summary> #8 rotated envelope (309 mm by 120 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x0400039F RID: 927
		PrcEnvelopeNumber8Rotated = 116,
		/// <summary> #9 envelope (229 mm by 324 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x040003A0 RID: 928
		PrcEnvelopeNumber9 = 104,
		/// <summary> #9 rotated envelope (324 mm by 229 mm). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x040003A1 RID: 929
		PrcEnvelopeNumber9Rotated = 117,
		/// <summary>Quarto paper (215 mm by 275 mm).</summary>
		// Token: 0x040003A2 RID: 930
		Quarto = 15,
		/// <summary>Standard paper (10 in. by 11 in.).</summary>
		// Token: 0x040003A3 RID: 931
		Standard10x11 = 45,
		/// <summary>Standard paper (10 in. by 14 in.).</summary>
		// Token: 0x040003A4 RID: 932
		Standard10x14 = 16,
		/// <summary>Standard paper (11 in. by 17 in.).</summary>
		// Token: 0x040003A5 RID: 933
		Standard11x17,
		/// <summary>Standard paper (12 in. by 11 in.). Requires Windows 98, Windows NT 4.0, or later.</summary>
		// Token: 0x040003A6 RID: 934
		Standard12x11 = 90,
		/// <summary>Standard paper (15 in. by 11 in.).</summary>
		// Token: 0x040003A7 RID: 935
		Standard15x11 = 46,
		/// <summary>Standard paper (9 in. by 11 in.).</summary>
		// Token: 0x040003A8 RID: 936
		Standard9x11 = 44,
		/// <summary>Statement paper (5.5 in. by 8.5 in.).</summary>
		// Token: 0x040003A9 RID: 937
		Statement = 6,
		/// <summary>Tabloid paper (11 in. by 17 in.).</summary>
		// Token: 0x040003AA RID: 938
		Tabloid = 3,
		/// <summary>Tabloid extra paper (11.69 in. by 18 in.). This value is specific to the PostScript driver and is used only by Linotronic printers in order to conserve paper.</summary>
		// Token: 0x040003AB RID: 939
		TabloidExtra = 52,
		/// <summary>US standard fanfold (14.875 in. by 11 in.).</summary>
		// Token: 0x040003AC RID: 940
		USStandardFanfold = 39
	}
}
