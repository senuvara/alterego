﻿using System;
using System.Security;
using System.Security.Permissions;
using Unity;

namespace System.Drawing.Printing
{
	/// <summary>Controls access to printers. This class cannot be inherited.</summary>
	// Token: 0x020000AA RID: 170
	[Serializable]
	public sealed class PrintingPermission : CodeAccessPermission, IUnrestrictedPermission
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.PrintingPermission" /> class with the level of printing access specified.</summary>
		/// <param name="printingLevel">One of the <see cref="T:System.Drawing.Printing.PrintingPermissionLevel" /> values. </param>
		// Token: 0x0600089D RID: 2205 RVA: 0x00004644 File Offset: 0x00002844
		public PrintingPermission(PrintingPermissionLevel printingLevel)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.PrintingPermission" /> class with either fully restricted or unrestricted access, as specified.</summary>
		/// <param name="state">One of the <see cref="T:System.Security.Permissions.PermissionState" /> values. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="state" /> is not a valid <see cref="T:System.Security.Permissions.PermissionState" />. </exception>
		// Token: 0x0600089E RID: 2206 RVA: 0x00004644 File Offset: 0x00002844
		public PrintingPermission(PermissionState state)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the code's level of printing access.</summary>
		/// <returns>One of the <see cref="T:System.Drawing.Printing.PrintingPermissionLevel" /> values.</returns>
		// Token: 0x1700037D RID: 893
		// (get) Token: 0x0600089F RID: 2207 RVA: 0x00006E40 File Offset: 0x00005040
		// (set) Token: 0x060008A0 RID: 2208 RVA: 0x00004644 File Offset: 0x00002844
		public PrintingPermissionLevel Level
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return PrintingPermissionLevel.NoPrinting;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Creates and returns an identical copy of the current permission object.</summary>
		/// <returns>A copy of the current permission object.</returns>
		// Token: 0x060008A1 RID: 2209 RVA: 0x00004667 File Offset: 0x00002867
		public override IPermission Copy()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Reconstructs a security object with a specified state from an XML encoding.</summary>
		/// <param name="esd">The XML encoding to use to reconstruct the security object. </param>
		// Token: 0x060008A2 RID: 2210 RVA: 0x00004644 File Offset: 0x00002844
		public override void FromXml(SecurityElement esd)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates and returns a permission that is the intersection of the current permission object and a target permission object.</summary>
		/// <param name="target">A permission object of the same type as the current permission object. </param>
		/// <returns>A new permission object that represents the intersection of the current object and the specified target. This object is <see langword="null" /> if the intersection is empty.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="target" /> is an object that is not of the same type as the current permission object. </exception>
		// Token: 0x060008A3 RID: 2211 RVA: 0x00004667 File Offset: 0x00002867
		public override IPermission Intersect(IPermission target)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Determines whether the current permission object is a subset of the specified permission.</summary>
		/// <param name="target">A permission object that is to be tested for the subset relationship. This object must be of the same type as the current permission object. </param>
		/// <returns>
		///     <see langword="true" /> if the current permission object is a subset of <paramref name="target" />; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="target" /> is an object that is not of the same type as the current permission object. </exception>
		// Token: 0x060008A4 RID: 2212 RVA: 0x00006E5C File Offset: 0x0000505C
		public override bool IsSubsetOf(IPermission target)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Gets a value indicating whether the permission is unrestricted.</summary>
		/// <returns>
		///     <see langword="true" /> if permission is unrestricted; otherwise, <see langword="false" />.</returns>
		// Token: 0x060008A5 RID: 2213 RVA: 0x00006E78 File Offset: 0x00005078
		public bool IsUnrestricted()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Creates an XML encoding of the security object and its current state.</summary>
		/// <returns>An XML encoding of the security object, including any state information.</returns>
		// Token: 0x060008A6 RID: 2214 RVA: 0x00004667 File Offset: 0x00002867
		public override SecurityElement ToXml()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
