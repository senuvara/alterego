﻿using System;

namespace System.Drawing.Printing
{
	/// <summary>Specifies several of the units of measure used for printing.</summary>
	// Token: 0x020000A8 RID: 168
	public enum PrinterUnit
	{
		/// <summary>The default unit (0.01 in.).</summary>
		// Token: 0x040003CC RID: 972
		Display,
		/// <summary>One-hundredth of a millimeter (0.01 mm).</summary>
		// Token: 0x040003CD RID: 973
		HundredthsOfAMillimeter = 2,
		/// <summary>One-tenth of a millimeter (0.1 mm).</summary>
		// Token: 0x040003CE RID: 974
		TenthsOfAMillimeter,
		/// <summary>One-thousandth of an inch (0.001 in.).</summary>
		// Token: 0x040003CF RID: 975
		ThousandthsOfAnInch = 1
	}
}
