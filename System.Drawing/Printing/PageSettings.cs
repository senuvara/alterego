﻿using System;
using Unity;

namespace System.Drawing.Printing
{
	/// <summary>Specifies settings that apply to a single, printed page.</summary>
	// Token: 0x02000099 RID: 153
	[Serializable]
	public class PageSettings : ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.PageSettings" /> class using the default printer.</summary>
		// Token: 0x0600082D RID: 2093 RVA: 0x00004644 File Offset: 0x00002844
		public PageSettings()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.PageSettings" /> class using a specified printer.</summary>
		/// <param name="printerSettings">The <see cref="T:System.Drawing.Printing.PrinterSettings" /> that describes the printer to use. </param>
		// Token: 0x0600082E RID: 2094 RVA: 0x00004644 File Offset: 0x00002844
		public PageSettings(PrinterSettings printerSettings)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the size of the page, taking into account the page orientation specified by the <see cref="P:System.Drawing.Printing.PageSettings.Landscape" /> property.</summary>
		/// <returns>A <see cref="T:System.Drawing.Rectangle" /> that represents the length and width, in hundredths of an inch, of the page.</returns>
		/// <exception cref="T:System.Drawing.Printing.InvalidPrinterException">The printer named in the <see cref="P:System.Drawing.Printing.PrinterSettings.PrinterName" /> property does not exist. </exception>
		// Token: 0x1700035C RID: 860
		// (get) Token: 0x0600082F RID: 2095 RVA: 0x00006B4C File Offset: 0x00004D4C
		public Rectangle Bounds
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Rectangle);
			}
		}

		/// <summary>Gets or sets a value indicating whether the page should be printed in color.</summary>
		/// <returns>
		///     <see langword="true" /> if the page should be printed in color; otherwise, <see langword="false" />. The default is determined by the printer.</returns>
		/// <exception cref="T:System.Drawing.Printing.InvalidPrinterException">The printer named in the <see cref="P:System.Drawing.Printing.PrinterSettings.PrinterName" /> property does not exist. </exception>
		// Token: 0x1700035D RID: 861
		// (get) Token: 0x06000830 RID: 2096 RVA: 0x00006B68 File Offset: 0x00004D68
		// (set) Token: 0x06000831 RID: 2097 RVA: 0x00004644 File Offset: 0x00002844
		public bool Color
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the x-coordinate, in hundredths of an inch, of the hard margin at the left of the page.</summary>
		/// <returns>The x-coordinate, in hundredths of an inch, of the left-hand hard margin.</returns>
		// Token: 0x1700035E RID: 862
		// (get) Token: 0x06000832 RID: 2098 RVA: 0x00006B84 File Offset: 0x00004D84
		public float HardMarginX
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
		}

		/// <summary>Gets the y-coordinate, in hundredths of an inch, of the hard margin at the top of the page.</summary>
		/// <returns>The y-coordinate, in hundredths of an inch, of the hard margin at the top of the page.</returns>
		// Token: 0x1700035F RID: 863
		// (get) Token: 0x06000833 RID: 2099 RVA: 0x00006BA0 File Offset: 0x00004DA0
		public float HardMarginY
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
		}

		/// <summary>Gets or sets a value indicating whether the page is printed in landscape or portrait orientation.</summary>
		/// <returns>
		///     <see langword="true" /> if the page should be printed in landscape orientation; otherwise, <see langword="false" />. The default is determined by the printer.</returns>
		/// <exception cref="T:System.Drawing.Printing.InvalidPrinterException">The printer named in the <see cref="P:System.Drawing.Printing.PrinterSettings.PrinterName" /> property does not exist. </exception>
		// Token: 0x17000360 RID: 864
		// (get) Token: 0x06000834 RID: 2100 RVA: 0x00006BBC File Offset: 0x00004DBC
		// (set) Token: 0x06000835 RID: 2101 RVA: 0x00004644 File Offset: 0x00002844
		public bool Landscape
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the margins for this page.</summary>
		/// <returns>A <see cref="T:System.Drawing.Printing.Margins" /> that represents the margins, in hundredths of an inch, for the page. The default is 1-inch margins on all sides.</returns>
		/// <exception cref="T:System.Drawing.Printing.InvalidPrinterException">The printer named in the <see cref="P:System.Drawing.Printing.PrinterSettings.PrinterName" /> property does not exist. </exception>
		// Token: 0x17000361 RID: 865
		// (get) Token: 0x06000836 RID: 2102 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000837 RID: 2103 RVA: 0x00004644 File Offset: 0x00002844
		public Margins Margins
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the paper size for the page.</summary>
		/// <returns>A <see cref="T:System.Drawing.Printing.PaperSize" /> that represents the size of the paper. The default is the printer's default paper size.</returns>
		/// <exception cref="T:System.Drawing.Printing.InvalidPrinterException">The printer named in the <see cref="P:System.Drawing.Printing.PrinterSettings.PrinterName" /> property does not exist or there is no default printer installed. </exception>
		// Token: 0x17000362 RID: 866
		// (get) Token: 0x06000838 RID: 2104 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000839 RID: 2105 RVA: 0x00004644 File Offset: 0x00002844
		public PaperSize PaperSize
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the page's paper source; for example, the printer's upper tray.</summary>
		/// <returns>A <see cref="T:System.Drawing.Printing.PaperSource" /> that specifies the source of the paper. The default is the printer's default paper source.</returns>
		/// <exception cref="T:System.Drawing.Printing.InvalidPrinterException">The printer named in the <see cref="P:System.Drawing.Printing.PrinterSettings.PrinterName" /> property does not exist or there is no default printer installed. </exception>
		// Token: 0x17000363 RID: 867
		// (get) Token: 0x0600083A RID: 2106 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x0600083B RID: 2107 RVA: 0x00004644 File Offset: 0x00002844
		public PaperSource PaperSource
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the bounds of the printable area of the page for the printer.</summary>
		/// <returns>A <see cref="T:System.Drawing.RectangleF" /> representing the length and width, in hundredths of an inch, of the area the printer is capable of printing in.</returns>
		// Token: 0x17000364 RID: 868
		// (get) Token: 0x0600083C RID: 2108 RVA: 0x00006BD8 File Offset: 0x00004DD8
		public RectangleF PrintableArea
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(RectangleF);
			}
		}

		/// <summary>Gets or sets the printer resolution for the page.</summary>
		/// <returns>A <see cref="T:System.Drawing.Printing.PrinterResolution" /> that specifies the printer resolution for the page. The default is the printer's default resolution.</returns>
		/// <exception cref="T:System.Drawing.Printing.InvalidPrinterException">The printer named in the <see cref="P:System.Drawing.Printing.PrinterSettings.PrinterName" /> property does not exist or there is no default printer installed. </exception>
		// Token: 0x17000365 RID: 869
		// (get) Token: 0x0600083D RID: 2109 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x0600083E RID: 2110 RVA: 0x00004644 File Offset: 0x00002844
		public PrinterResolution PrinterResolution
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the printer settings associated with the page.</summary>
		/// <returns>A <see cref="T:System.Drawing.Printing.PrinterSettings" /> that represents the printer settings associated with the page.</returns>
		// Token: 0x17000366 RID: 870
		// (get) Token: 0x0600083F RID: 2111 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000840 RID: 2112 RVA: 0x00004644 File Offset: 0x00002844
		public PrinterSettings PrinterSettings
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Creates a copy of this <see cref="T:System.Drawing.Printing.PageSettings" />.</summary>
		/// <returns>A copy of this object.</returns>
		// Token: 0x06000841 RID: 2113 RVA: 0x00004667 File Offset: 0x00002867
		public object Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Copies the relevant information from the <see cref="T:System.Drawing.Printing.PageSettings" /> to the specified <see langword="DEVMODE " />structure.</summary>
		/// <param name="hdevmode">The handle to a Win32 <see langword="DEVMODE" /> structure. </param>
		/// <exception cref="T:System.Drawing.Printing.InvalidPrinterException">The printer named in the <see cref="P:System.Drawing.Printing.PrinterSettings.PrinterName" /> property does not exist or there is no default printer installed. </exception>
		// Token: 0x06000842 RID: 2114 RVA: 0x00004644 File Offset: 0x00002844
		public void CopyToHdevmode(IntPtr hdevmode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Copies relevant information to the <see cref="T:System.Drawing.Printing.PageSettings" /> from the specified <see langword="DEVMODE" /> structure.</summary>
		/// <param name="hdevmode">The handle to a Win32 <see langword="DEVMODE" /> structure. </param>
		/// <exception cref="T:System.ArgumentException">The printer handle is not valid. </exception>
		/// <exception cref="T:System.Drawing.Printing.InvalidPrinterException">The printer named in the <see cref="P:System.Drawing.Printing.PrinterSettings.PrinterName" /> property does not exist or there is no default printer installed. </exception>
		// Token: 0x06000843 RID: 2115 RVA: 0x00004644 File Offset: 0x00002844
		public void SetHdevmode(IntPtr hdevmode)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
