﻿using System;
using Unity;

namespace System.Drawing.Printing
{
	/// <summary>Specifies the paper tray from which the printer gets paper.</summary>
	// Token: 0x02000095 RID: 149
	[Serializable]
	public class PaperSource
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.PaperSource" /> class. </summary>
		// Token: 0x06000820 RID: 2080 RVA: 0x00004644 File Offset: 0x00002844
		public PaperSource()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the paper source.</summary>
		/// <returns>One of the <see cref="T:System.Drawing.Printing.PaperSourceKind" /> values.</returns>
		// Token: 0x17000356 RID: 854
		// (get) Token: 0x06000821 RID: 2081 RVA: 0x00006AC0 File Offset: 0x00004CC0
		public PaperSourceKind Kind
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (PaperSourceKind)0;
			}
		}

		/// <summary>Gets or sets the integer representing one of the <see cref="T:System.Drawing.Printing.PaperSourceKind" /> values or a custom value.</summary>
		/// <returns>The integer value representing one of the <see cref="T:System.Drawing.Printing.PaperSourceKind" /> values or a custom value. </returns>
		// Token: 0x17000357 RID: 855
		// (get) Token: 0x06000822 RID: 2082 RVA: 0x00006ADC File Offset: 0x00004CDC
		// (set) Token: 0x06000823 RID: 2083 RVA: 0x00004644 File Offset: 0x00002844
		public int RawKind
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the paper source.</summary>
		/// <returns>The name of the paper source.</returns>
		// Token: 0x17000358 RID: 856
		// (get) Token: 0x06000824 RID: 2084 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000825 RID: 2085 RVA: 0x00004644 File Offset: 0x00002844
		public string SourceName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
