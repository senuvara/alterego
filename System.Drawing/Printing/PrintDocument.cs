﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Drawing.Printing
{
	/// <summary>Defines a reusable object that sends output to a printer, when printing from a Windows Forms application.</summary>
	// Token: 0x020000A0 RID: 160
	[DefaultProperty("DocumentName")]
	[DefaultEvent("PrintPage")]
	[ToolboxItemFilter("System.Drawing.Printing")]
	public class PrintDocument : Component
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.PrintDocument" /> class.</summary>
		// Token: 0x06000864 RID: 2148 RVA: 0x00004644 File Offset: 0x00002844
		public PrintDocument()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets page settings that are used as defaults for all pages to be printed.</summary>
		/// <returns>A <see cref="T:System.Drawing.Printing.PageSettings" /> that specifies the default page settings for the document.</returns>
		// Token: 0x17000370 RID: 880
		// (get) Token: 0x06000865 RID: 2149 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000866 RID: 2150 RVA: 0x00004644 File Offset: 0x00002844
		public PageSettings DefaultPageSettings
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the document name to display (for example, in a print status dialog box or printer queue) while printing the document.</summary>
		/// <returns>The document name to display while printing the document. The default is "document".</returns>
		// Token: 0x17000371 RID: 881
		// (get) Token: 0x06000867 RID: 2151 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000868 RID: 2152 RVA: 0x00004644 File Offset: 0x00002844
		public string DocumentName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether the position of a graphics object associated with a page is located just inside the user-specified margins or at the top-left corner of the printable area of the page.</summary>
		/// <returns>
		///     <see langword="true" /> if the graphics origin starts at the page margins; <see langword="false" /> if the graphics origin is at the top-left corner of the printable page. The default is <see langword="false" />.</returns>
		// Token: 0x17000372 RID: 882
		// (get) Token: 0x06000869 RID: 2153 RVA: 0x00006D0C File Offset: 0x00004F0C
		// (set) Token: 0x0600086A RID: 2154 RVA: 0x00004644 File Offset: 0x00002844
		public bool OriginAtMargins
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the print controller that guides the printing process.</summary>
		/// <returns>The <see cref="T:System.Drawing.Printing.PrintController" /> that guides the printing process. The default is a new instance of the <see cref="T:System.Windows.Forms.PrintControllerWithStatusDialog" /> class.</returns>
		// Token: 0x17000373 RID: 883
		// (get) Token: 0x0600086B RID: 2155 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x0600086C RID: 2156 RVA: 0x00004644 File Offset: 0x00002844
		public PrintController PrintController
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the printer that prints the document.</summary>
		/// <returns>A <see cref="T:System.Drawing.Printing.PrinterSettings" /> that specifies where and how the document is printed. The default is a <see cref="T:System.Drawing.Printing.PrinterSettings" /> with its properties set to their default values.</returns>
		// Token: 0x17000374 RID: 884
		// (get) Token: 0x0600086D RID: 2157 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x0600086E RID: 2158 RVA: 0x00004644 File Offset: 0x00002844
		public PrinterSettings PrinterSettings
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs when the <see cref="M:System.Drawing.Printing.PrintDocument.Print" /> method is called and before the first page of the document prints.</summary>
		// Token: 0x14000003 RID: 3
		// (add) Token: 0x0600086F RID: 2159 RVA: 0x00004644 File Offset: 0x00002844
		// (remove) Token: 0x06000870 RID: 2160 RVA: 0x00004644 File Offset: 0x00002844
		public event PrintEventHandler BeginPrint
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs when the last page of the document has printed.</summary>
		// Token: 0x14000004 RID: 4
		// (add) Token: 0x06000871 RID: 2161 RVA: 0x00004644 File Offset: 0x00002844
		// (remove) Token: 0x06000872 RID: 2162 RVA: 0x00004644 File Offset: 0x00002844
		public event PrintEventHandler EndPrint
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs when the output to print for the current page is needed.</summary>
		// Token: 0x14000005 RID: 5
		// (add) Token: 0x06000873 RID: 2163 RVA: 0x00004644 File Offset: 0x00002844
		// (remove) Token: 0x06000874 RID: 2164 RVA: 0x00004644 File Offset: 0x00002844
		public event PrintPageEventHandler PrintPage
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs immediately before each <see cref="E:System.Drawing.Printing.PrintDocument.PrintPage" /> event.</summary>
		// Token: 0x14000006 RID: 6
		// (add) Token: 0x06000875 RID: 2165 RVA: 0x00004644 File Offset: 0x00002844
		// (remove) Token: 0x06000876 RID: 2166 RVA: 0x00004644 File Offset: 0x00002844
		public event QueryPageSettingsEventHandler QueryPageSettings
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Raises the <see cref="E:System.Drawing.Printing.PrintDocument.BeginPrint" /> event. It is called after the <see cref="M:System.Drawing.Printing.PrintDocument.Print" /> method is called and before the first page of the document prints.</summary>
		/// <param name="e">A <see cref="T:System.Drawing.Printing.PrintEventArgs" /> that contains the event data. </param>
		// Token: 0x06000877 RID: 2167 RVA: 0x00004644 File Offset: 0x00002844
		protected virtual void OnBeginPrint(PrintEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Raises the <see cref="E:System.Drawing.Printing.PrintDocument.EndPrint" /> event. It is called when the last page of the document has printed.</summary>
		/// <param name="e">A <see cref="T:System.Drawing.Printing.PrintEventArgs" /> that contains the event data. </param>
		// Token: 0x06000878 RID: 2168 RVA: 0x00004644 File Offset: 0x00002844
		protected virtual void OnEndPrint(PrintEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Raises the <see cref="E:System.Drawing.Printing.PrintDocument.PrintPage" /> event. It is called before a page prints.</summary>
		/// <param name="e">A <see cref="T:System.Drawing.Printing.PrintPageEventArgs" /> that contains the event data. </param>
		// Token: 0x06000879 RID: 2169 RVA: 0x00004644 File Offset: 0x00002844
		protected virtual void OnPrintPage(PrintPageEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Raises the <see cref="E:System.Drawing.Printing.PrintDocument.QueryPageSettings" /> event. It is called immediately before each <see cref="E:System.Drawing.Printing.PrintDocument.PrintPage" /> event.</summary>
		/// <param name="e">A <see cref="T:System.Drawing.Printing.QueryPageSettingsEventArgs" /> that contains the event data. </param>
		// Token: 0x0600087A RID: 2170 RVA: 0x00004644 File Offset: 0x00002844
		protected virtual void OnQueryPageSettings(QueryPageSettingsEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Starts the document's printing process.</summary>
		/// <exception cref="T:System.Drawing.Printing.InvalidPrinterException">The printer named in the <see cref="P:System.Drawing.Printing.PrinterSettings.PrinterName" /> property does not exist. </exception>
		// Token: 0x0600087B RID: 2171 RVA: 0x00004644 File Offset: 0x00002844
		public void Print()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
