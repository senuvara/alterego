﻿using System;
using Unity;

namespace System.Drawing.Printing
{
	/// <summary>Specifies the size of a piece of paper.</summary>
	// Token: 0x02000093 RID: 147
	[Serializable]
	public class PaperSize
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.PaperSize" /> class.</summary>
		// Token: 0x06000815 RID: 2069 RVA: 0x00004644 File Offset: 0x00002844
		public PaperSize()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.PaperSize" /> class.</summary>
		/// <param name="name">The name of the paper. </param>
		/// <param name="width">The width of the paper, in hundredths of an inch. </param>
		/// <param name="height">The height of the paper, in hundredths of an inch. </param>
		// Token: 0x06000816 RID: 2070 RVA: 0x00004644 File Offset: 0x00002844
		public PaperSize(string name, int width, int height)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the height of the paper, in hundredths of an inch.</summary>
		/// <returns>The height of the paper, in hundredths of an inch.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Printing.PaperSize.Kind" /> property is not set to <see cref="F:System.Drawing.Printing.PaperKind.Custom" />. </exception>
		// Token: 0x17000351 RID: 849
		// (get) Token: 0x06000817 RID: 2071 RVA: 0x00006A50 File Offset: 0x00004C50
		// (set) Token: 0x06000818 RID: 2072 RVA: 0x00004644 File Offset: 0x00002844
		public int Height
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the type of paper.</summary>
		/// <returns>One of the <see cref="T:System.Drawing.Printing.PaperKind" /> values.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Printing.PaperSize.Kind" /> property is not set to <see cref="F:System.Drawing.Printing.PaperKind.Custom" />. </exception>
		// Token: 0x17000352 RID: 850
		// (get) Token: 0x06000819 RID: 2073 RVA: 0x00006A6C File Offset: 0x00004C6C
		public PaperKind Kind
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return PaperKind.Custom;
			}
		}

		/// <summary>Gets or sets the name of the type of paper.</summary>
		/// <returns>The name of the type of paper.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Printing.PaperSize.Kind" /> property is not set to <see cref="F:System.Drawing.Printing.PaperKind.Custom" />. </exception>
		// Token: 0x17000353 RID: 851
		// (get) Token: 0x0600081A RID: 2074 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x0600081B RID: 2075 RVA: 0x00004644 File Offset: 0x00002844
		public string PaperName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets an integer representing one of the <see cref="T:System.Drawing.Printing.PaperSize" /> values or a custom value.</summary>
		/// <returns>An integer representing one of the <see cref="T:System.Drawing.Printing.PaperSize" /> values, or a custom value.</returns>
		// Token: 0x17000354 RID: 852
		// (get) Token: 0x0600081C RID: 2076 RVA: 0x00006A88 File Offset: 0x00004C88
		// (set) Token: 0x0600081D RID: 2077 RVA: 0x00004644 File Offset: 0x00002844
		public int RawKind
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the width of the paper, in hundredths of an inch.</summary>
		/// <returns>The width of the paper, in hundredths of an inch.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Printing.PaperSize.Kind" /> property is not set to <see cref="F:System.Drawing.Printing.PaperKind.Custom" />. </exception>
		// Token: 0x17000355 RID: 853
		// (get) Token: 0x0600081E RID: 2078 RVA: 0x00006AA4 File Offset: 0x00004CA4
		// (set) Token: 0x0600081F RID: 2079 RVA: 0x00004644 File Offset: 0x00002844
		public int Width
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
