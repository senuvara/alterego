﻿using System;
using Unity;

namespace System.Drawing.Printing
{
	/// <summary>Represents the resolution supported by a printer.</summary>
	// Token: 0x02000097 RID: 151
	[Serializable]
	public class PrinterResolution
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.PrinterResolution" /> class. </summary>
		// Token: 0x06000826 RID: 2086 RVA: 0x00004644 File Offset: 0x00002844
		public PrinterResolution()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the printer resolution.</summary>
		/// <returns>One of the <see cref="T:System.Drawing.Printing.PrinterResolutionKind" /> values.</returns>
		/// <exception cref="T:System.ComponentModel.InvalidEnumArgumentException">The value assigned is not a member of the <see cref="T:System.Drawing.Printing.PrinterResolutionKind" /> enumeration.</exception>
		// Token: 0x17000359 RID: 857
		// (get) Token: 0x06000827 RID: 2087 RVA: 0x00006AF8 File Offset: 0x00004CF8
		// (set) Token: 0x06000828 RID: 2088 RVA: 0x00004644 File Offset: 0x00002844
		public PrinterResolutionKind Kind
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return PrinterResolutionKind.Custom;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the horizontal printer resolution, in dots per inch.</summary>
		/// <returns>The horizontal printer resolution, in dots per inch, if <see cref="P:System.Drawing.Printing.PrinterResolution.Kind" /> is set to <see cref="F:System.Drawing.Printing.PrinterResolutionKind.Custom" />; otherwise, a <see langword="dmPrintQuality" /> value.</returns>
		// Token: 0x1700035A RID: 858
		// (get) Token: 0x06000829 RID: 2089 RVA: 0x00006B14 File Offset: 0x00004D14
		// (set) Token: 0x0600082A RID: 2090 RVA: 0x00004644 File Offset: 0x00002844
		public int X
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the vertical printer resolution, in dots per inch.</summary>
		/// <returns>The vertical printer resolution, in dots per inch.</returns>
		// Token: 0x1700035B RID: 859
		// (get) Token: 0x0600082B RID: 2091 RVA: 0x00006B30 File Offset: 0x00004D30
		// (set) Token: 0x0600082C RID: 2092 RVA: 0x00004644 File Offset: 0x00002844
		public int Y
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
