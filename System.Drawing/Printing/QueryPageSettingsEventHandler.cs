﻿using System;
using Unity;

namespace System.Drawing.Printing
{
	/// <summary>Represents the method that handles the <see cref="E:System.Drawing.Printing.PrintDocument.QueryPageSettings" /> event of a <see cref="T:System.Drawing.Printing.PrintDocument" />.</summary>
	/// <param name="sender">The source of the event. </param>
	/// <param name="e">A <see cref="T:System.Drawing.Printing.QueryPageSettingsEventArgs" /> that contains the event data. </param>
	// Token: 0x020000A6 RID: 166
	public sealed class QueryPageSettingsEventHandler : MulticastDelegate
	{
		// Token: 0x0600088F RID: 2191 RVA: 0x00004644 File Offset: 0x00002844
		public QueryPageSettingsEventHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06000890 RID: 2192 RVA: 0x00004644 File Offset: 0x00002844
		public void Invoke(object sender, QueryPageSettingsEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06000891 RID: 2193 RVA: 0x00004667 File Offset: 0x00002867
		public IAsyncResult BeginInvoke(object sender, QueryPageSettingsEventArgs e, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06000892 RID: 2194 RVA: 0x00004644 File Offset: 0x00002844
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
