﻿using System;
using Unity;

namespace System.Drawing.Printing
{
	/// <summary>Represents the method that will handle the <see cref="E:System.Drawing.Printing.PrintDocument.PrintPage" /> event of a <see cref="T:System.Drawing.Printing.PrintDocument" />.</summary>
	/// <param name="sender">The source of the event. </param>
	/// <param name="e">A <see cref="T:System.Drawing.Printing.PrintPageEventArgs" /> that contains the event data. </param>
	// Token: 0x020000A4 RID: 164
	public sealed class PrintPageEventHandler : MulticastDelegate
	{
		// Token: 0x06000882 RID: 2178 RVA: 0x00004644 File Offset: 0x00002844
		public PrintPageEventHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06000883 RID: 2179 RVA: 0x00004644 File Offset: 0x00002844
		public void Invoke(object sender, PrintPageEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06000884 RID: 2180 RVA: 0x00004667 File Offset: 0x00002867
		public IAsyncResult BeginInvoke(object sender, PrintPageEventArgs e, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06000885 RID: 2181 RVA: 0x00004644 File Offset: 0x00002844
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
