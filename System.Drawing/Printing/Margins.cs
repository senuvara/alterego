﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Drawing.Printing
{
	/// <summary>Specifies the dimensions of the margins of a printed page.</summary>
	// Token: 0x0200009A RID: 154
	[TypeConverter(typeof(MarginsConverter))]
	[Serializable]
	public class Margins : ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.Margins" /> class with 1-inch wide margins.</summary>
		// Token: 0x06000844 RID: 2116 RVA: 0x00004644 File Offset: 0x00002844
		public Margins()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.Margins" /> class with the specified left, right, top, and bottom margins.</summary>
		/// <param name="left">The left margin, in hundredths of an inch. </param>
		/// <param name="right">The right margin, in hundredths of an inch. </param>
		/// <param name="top">The top margin, in hundredths of an inch. </param>
		/// <param name="bottom">The bottom margin, in hundredths of an inch. </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="left" /> parameter value is less than 0.-or- The <paramref name="right" /> parameter value is less than 0.-or- The <paramref name="top" /> parameter value is less than 0.-or- The <paramref name="bottom" /> parameter value is less than 0. </exception>
		// Token: 0x06000845 RID: 2117 RVA: 0x00004644 File Offset: 0x00002844
		public Margins(int left, int right, int top, int bottom)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the bottom margin, in hundredths of an inch.</summary>
		/// <returns>The bottom margin, in hundredths of an inch.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Printing.Margins.Bottom" /> property is set to a value that is less than 0. </exception>
		// Token: 0x17000367 RID: 871
		// (get) Token: 0x06000846 RID: 2118 RVA: 0x00006BF4 File Offset: 0x00004DF4
		// (set) Token: 0x06000847 RID: 2119 RVA: 0x00004644 File Offset: 0x00002844
		public int Bottom
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the left margin width, in hundredths of an inch.</summary>
		/// <returns>The left margin width, in hundredths of an inch.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Printing.Margins.Left" /> property is set to a value that is less than 0. </exception>
		// Token: 0x17000368 RID: 872
		// (get) Token: 0x06000848 RID: 2120 RVA: 0x00006C10 File Offset: 0x00004E10
		// (set) Token: 0x06000849 RID: 2121 RVA: 0x00004644 File Offset: 0x00002844
		public int Left
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the right margin width, in hundredths of an inch.</summary>
		/// <returns>The right margin width, in hundredths of an inch.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Printing.Margins.Right" /> property is set to a value that is less than 0. </exception>
		// Token: 0x17000369 RID: 873
		// (get) Token: 0x0600084A RID: 2122 RVA: 0x00006C2C File Offset: 0x00004E2C
		// (set) Token: 0x0600084B RID: 2123 RVA: 0x00004644 File Offset: 0x00002844
		public int Right
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the top margin width, in hundredths of an inch.</summary>
		/// <returns>The top margin width, in hundredths of an inch.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.Printing.Margins.Top" /> property is set to a value that is less than 0. </exception>
		// Token: 0x1700036A RID: 874
		// (get) Token: 0x0600084C RID: 2124 RVA: 0x00006C48 File Offset: 0x00004E48
		// (set) Token: 0x0600084D RID: 2125 RVA: 0x00004644 File Offset: 0x00002844
		public int Top
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Retrieves a duplicate of this object, member by member.</summary>
		/// <returns>A duplicate of this object.</returns>
		// Token: 0x0600084E RID: 2126 RVA: 0x00004667 File Offset: 0x00002867
		public object Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Compares two <see cref="T:System.Drawing.Printing.Margins" /> to determine if they have the same dimensions.</summary>
		/// <param name="m1">The first <see cref="T:System.Drawing.Printing.Margins" /> to compare for equality.</param>
		/// <param name="m2">The second <see cref="T:System.Drawing.Printing.Margins" /> to compare for equality.</param>
		/// <returns>
		///     <see langword="true" /> to indicate the <see cref="P:System.Drawing.Printing.Margins.Left" />, <see cref="P:System.Drawing.Printing.Margins.Right" />, <see cref="P:System.Drawing.Printing.Margins.Top" />, and <see cref="P:System.Drawing.Printing.Margins.Bottom" /> properties of both margins have the same value; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600084F RID: 2127 RVA: 0x00006C64 File Offset: 0x00004E64
		public static bool operator ==(Margins m1, Margins m2)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Compares two <see cref="T:System.Drawing.Printing.Margins" /> to determine whether they are of unequal width.</summary>
		/// <param name="m1">The first <see cref="T:System.Drawing.Printing.Margins" /> to compare for inequality.</param>
		/// <param name="m2">The second <see cref="T:System.Drawing.Printing.Margins" /> to compare for inequality.</param>
		/// <returns>
		///     <see langword="true" /> to indicate if the <see cref="P:System.Drawing.Printing.Margins.Left" />, <see cref="P:System.Drawing.Printing.Margins.Right" />, <see cref="P:System.Drawing.Printing.Margins.Top" />, or <see cref="P:System.Drawing.Printing.Margins.Bottom" /> properties of both margins are not equal; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000850 RID: 2128 RVA: 0x00006C80 File Offset: 0x00004E80
		public static bool operator !=(Margins m1, Margins m2)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}
	}
}
