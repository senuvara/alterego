﻿using System;
using Unity;

namespace System.Drawing.Printing
{
	/// <summary>Provides data for the <see cref="E:System.Drawing.Printing.PrintDocument.PrintPage" /> event.</summary>
	// Token: 0x020000A5 RID: 165
	public class PrintPageEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.PrintPageEventArgs" /> class.</summary>
		/// <param name="graphics">The <see cref="T:System.Drawing.Graphics" /> used to paint the item. </param>
		/// <param name="marginBounds">The area between the margins. </param>
		/// <param name="pageBounds">The total area of the paper. </param>
		/// <param name="pageSettings">The <see cref="T:System.Drawing.Printing.PageSettings" /> for the page. </param>
		// Token: 0x06000886 RID: 2182 RVA: 0x00004644 File Offset: 0x00002844
		public PrintPageEventArgs(Graphics graphics, Rectangle marginBounds, Rectangle pageBounds, PageSettings pageSettings)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets a value indicating whether the print job should be canceled.</summary>
		/// <returns>
		///     <see langword="true" /> if the print job should be canceled; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000376 RID: 886
		// (get) Token: 0x06000887 RID: 2183 RVA: 0x00006D44 File Offset: 0x00004F44
		// (set) Token: 0x06000888 RID: 2184 RVA: 0x00004644 File Offset: 0x00002844
		public bool Cancel
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the <see cref="T:System.Drawing.Graphics" /> used to paint the page.</summary>
		/// <returns>The <see cref="T:System.Drawing.Graphics" /> used to paint the page.</returns>
		// Token: 0x17000377 RID: 887
		// (get) Token: 0x06000889 RID: 2185 RVA: 0x00004667 File Offset: 0x00002867
		public Graphics Graphics
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets a value indicating whether an additional page should be printed.</summary>
		/// <returns>
		///     <see langword="true" /> if an additional page should be printed; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000378 RID: 888
		// (get) Token: 0x0600088A RID: 2186 RVA: 0x00006D60 File Offset: 0x00004F60
		// (set) Token: 0x0600088B RID: 2187 RVA: 0x00004644 File Offset: 0x00002844
		public bool HasMorePages
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the rectangular area that represents the portion of the page inside the margins.</summary>
		/// <returns>The rectangular area, measured in hundredths of an inch, that represents the portion of the page inside the margins. </returns>
		// Token: 0x17000379 RID: 889
		// (get) Token: 0x0600088C RID: 2188 RVA: 0x00006D7C File Offset: 0x00004F7C
		public Rectangle MarginBounds
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Rectangle);
			}
		}

		/// <summary>Gets the rectangular area that represents the total area of the page.</summary>
		/// <returns>The rectangular area that represents the total area of the page.</returns>
		// Token: 0x1700037A RID: 890
		// (get) Token: 0x0600088D RID: 2189 RVA: 0x00006D98 File Offset: 0x00004F98
		public Rectangle PageBounds
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Rectangle);
			}
		}

		/// <summary>Gets the page settings for the current page.</summary>
		/// <returns>The page settings for the current page.</returns>
		// Token: 0x1700037B RID: 891
		// (get) Token: 0x0600088E RID: 2190 RVA: 0x00004667 File Offset: 0x00002867
		public PageSettings PageSettings
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
