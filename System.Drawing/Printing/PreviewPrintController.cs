﻿using System;
using Unity;

namespace System.Drawing.Printing
{
	/// <summary>Specifies a print controller that displays a document on a screen as a series of images.</summary>
	// Token: 0x0200009E RID: 158
	public class PreviewPrintController : PrintController
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.PreviewPrintController" /> class.</summary>
		// Token: 0x06000855 RID: 2133 RVA: 0x00004644 File Offset: 0x00002844
		public PreviewPrintController()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a value indicating whether this controller is used for print preview. </summary>
		/// <returns>
		///     <see langword="true" /> in all cases.</returns>
		// Token: 0x1700036D RID: 877
		// (get) Token: 0x06000856 RID: 2134 RVA: 0x00006CB8 File Offset: 0x00004EB8
		public override bool IsPreview
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets or sets a value indicating whether to use anti-aliasing when displaying the print preview.</summary>
		/// <returns>
		///     <see langword="true" /> if the print preview uses anti-aliasing; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x1700036E RID: 878
		// (get) Token: 0x06000857 RID: 2135 RVA: 0x00006CD4 File Offset: 0x00004ED4
		// (set) Token: 0x06000858 RID: 2136 RVA: 0x00004644 File Offset: 0x00002844
		public virtual bool UseAntiAlias
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Captures the pages of a document as a series of images.</summary>
		/// <returns>An array of type <see cref="T:System.Drawing.Printing.PreviewPageInfo" /> that contains the pages of a <see cref="T:System.Drawing.Printing.PrintDocument" /> as a series of images.</returns>
		// Token: 0x06000859 RID: 2137 RVA: 0x00004667 File Offset: 0x00002867
		public PreviewPageInfo[] GetPreviewPageInfo()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Completes the control sequence that determines when and how to preview a page in a print document.</summary>
		/// <param name="document">A <see cref="T:System.Drawing.Printing.PrintDocument" /> that represents the document being previewed. </param>
		/// <param name="e">A <see cref="T:System.Drawing.Printing.PrintPageEventArgs" /> that contains data about how to preview a page in the print document. </param>
		// Token: 0x0600085A RID: 2138 RVA: 0x00004644 File Offset: 0x00002844
		public override void OnEndPage(PrintDocument document, PrintPageEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Completes the control sequence that determines when and how to preview a print document.</summary>
		/// <param name="document">A <see cref="T:System.Drawing.Printing.PrintDocument" /> that represents the document being previewed. </param>
		/// <param name="e">A <see cref="T:System.Drawing.Printing.PrintEventArgs" /> that contains data about how to preview the print document. </param>
		// Token: 0x0600085B RID: 2139 RVA: 0x00004644 File Offset: 0x00002844
		public override void OnEndPrint(PrintDocument document, PrintEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Begins the control sequence that determines when and how to preview a page in a print document.</summary>
		/// <param name="document">A <see cref="T:System.Drawing.Printing.PrintDocument" /> that represents the document being previewed. </param>
		/// <param name="e">A <see cref="T:System.Drawing.Printing.PrintPageEventArgs" /> that contains data about how to preview a page in the print document. Initially, the <see cref="P:System.Drawing.Printing.PrintPageEventArgs.Graphics" /> property of this parameter will be <see langword="null" />. The value returned from this method will be used to set this property. </param>
		/// <returns>A <see cref="T:System.Drawing.Graphics" /> that represents a page from a <see cref="T:System.Drawing.Printing.PrintDocument" />.</returns>
		// Token: 0x0600085C RID: 2140 RVA: 0x00004667 File Offset: 0x00002867
		public override Graphics OnStartPage(PrintDocument document, PrintPageEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Begins the control sequence that determines when and how to preview a print document.</summary>
		/// <param name="document">A <see cref="T:System.Drawing.Printing.PrintDocument" /> that represents the document being previewed. </param>
		/// <param name="e">A <see cref="T:System.Drawing.Printing.PrintEventArgs" /> that contains data about how to print the document. </param>
		/// <exception cref="T:System.Drawing.Printing.InvalidPrinterException">The printer named in the <see cref="P:System.Drawing.Printing.PrinterSettings.PrinterName" /> property does not exist. </exception>
		// Token: 0x0600085D RID: 2141 RVA: 0x00004644 File Offset: 0x00002844
		public override void OnStartPrint(PrintDocument document, PrintEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
