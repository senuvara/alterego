﻿using System;

namespace System.Drawing.Printing
{
	/// <summary>Specifies the printer's duplex setting.</summary>
	// Token: 0x0200008C RID: 140
	[Serializable]
	public enum Duplex
	{
		/// <summary>The printer's default duplex setting.</summary>
		// Token: 0x04000333 RID: 819
		Default = -1,
		/// <summary>Double-sided, horizontal printing.</summary>
		// Token: 0x04000334 RID: 820
		Horizontal = 3,
		/// <summary>Single-sided printing.</summary>
		// Token: 0x04000335 RID: 821
		Simplex = 1,
		/// <summary>Double-sided, vertical printing.</summary>
		// Token: 0x04000336 RID: 822
		Vertical
	}
}
