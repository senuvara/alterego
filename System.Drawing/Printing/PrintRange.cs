﻿using System;

namespace System.Drawing.Printing
{
	/// <summary>Specifies the part of the document to print.</summary>
	// Token: 0x0200009C RID: 156
	[Serializable]
	public enum PrintRange
	{
		/// <summary>All pages are printed.</summary>
		// Token: 0x040003C3 RID: 963
		AllPages,
		/// <summary>The currently displayed page is printed</summary>
		// Token: 0x040003C4 RID: 964
		CurrentPage = 4194304,
		/// <summary>The selected pages are printed.</summary>
		// Token: 0x040003C5 RID: 965
		Selection = 1,
		/// <summary>The pages between <see cref="P:System.Drawing.Printing.PrinterSettings.FromPage" /> and <see cref="P:System.Drawing.Printing.PrinterSettings.ToPage" /> are printed.</summary>
		// Token: 0x040003C6 RID: 966
		SomePages
	}
}
