﻿using System;

namespace System.Drawing.Printing
{
	/// <summary>Specifies the type of print operation occurring.</summary>
	// Token: 0x020000A3 RID: 163
	public enum PrintAction
	{
		/// <summary>The print operation is printing to a file.</summary>
		// Token: 0x040003C8 RID: 968
		PrintToFile,
		/// <summary>The print operation is a print preview.</summary>
		// Token: 0x040003C9 RID: 969
		PrintToPreview,
		/// <summary>The print operation is printing to a printer.</summary>
		// Token: 0x040003CA RID: 970
		PrintToPrinter
	}
}
