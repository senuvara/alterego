﻿using System;
using Unity;

namespace System.Drawing.Printing
{
	/// <summary>Specifies a series of conversion methods that are useful when interoperating with the Win32 printing API. This class cannot be inherited.</summary>
	// Token: 0x020000A9 RID: 169
	public sealed class PrinterUnitConvert
	{
		// Token: 0x06000896 RID: 2198 RVA: 0x00004644 File Offset: 0x00002844
		internal PrinterUnitConvert()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Converts a double-precision floating-point number from one <see cref="T:System.Drawing.Printing.PrinterUnit" /> type to another <see cref="T:System.Drawing.Printing.PrinterUnit" /> type.</summary>
		/// <param name="value">The <see cref="T:System.Drawing.Point" /> being converted. </param>
		/// <param name="fromUnit">The unit to convert from. </param>
		/// <param name="toUnit">The unit to convert to. </param>
		/// <returns>A double-precision floating-point number that represents the converted <see cref="T:System.Drawing.Printing.PrinterUnit" />.</returns>
		// Token: 0x06000897 RID: 2199 RVA: 0x00006DB4 File Offset: 0x00004FB4
		public static double Convert(double value, PrinterUnit fromUnit, PrinterUnit toUnit)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0.0;
		}

		/// <summary>Converts a <see cref="T:System.Drawing.Point" /> from one <see cref="T:System.Drawing.Printing.PrinterUnit" /> type to another <see cref="T:System.Drawing.Printing.PrinterUnit" /> type.</summary>
		/// <param name="value">The <see cref="T:System.Drawing.Point" /> being converted. </param>
		/// <param name="fromUnit">The unit to convert from. </param>
		/// <param name="toUnit">The unit to convert to. </param>
		/// <returns>A <see cref="T:System.Drawing.Point" /> that represents the converted <see cref="T:System.Drawing.Printing.PrinterUnit" />.</returns>
		// Token: 0x06000898 RID: 2200 RVA: 0x00006DD0 File Offset: 0x00004FD0
		public static Point Convert(Point value, PrinterUnit fromUnit, PrinterUnit toUnit)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(Point);
		}

		/// <summary>Converts a <see cref="T:System.Drawing.Printing.Margins" /> from one <see cref="T:System.Drawing.Printing.PrinterUnit" /> type to another <see cref="T:System.Drawing.Printing.PrinterUnit" /> type.</summary>
		/// <param name="value">The <see cref="T:System.Drawing.Printing.Margins" /> being converted. </param>
		/// <param name="fromUnit">The unit to convert from. </param>
		/// <param name="toUnit">The unit to convert to. </param>
		/// <returns>A <see cref="T:System.Drawing.Printing.Margins" /> that represents the converted <see cref="T:System.Drawing.Printing.PrinterUnit" />.</returns>
		// Token: 0x06000899 RID: 2201 RVA: 0x00004667 File Offset: 0x00002867
		public static Margins Convert(Margins value, PrinterUnit fromUnit, PrinterUnit toUnit)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Converts a <see cref="T:System.Drawing.Rectangle" /> from one <see cref="T:System.Drawing.Printing.PrinterUnit" /> type to another <see cref="T:System.Drawing.Printing.PrinterUnit" /> type.</summary>
		/// <param name="value">The <see cref="T:System.Drawing.Rectangle" /> being converted. </param>
		/// <param name="fromUnit">The unit to convert from. </param>
		/// <param name="toUnit">The unit to convert to. </param>
		/// <returns>A <see cref="T:System.Drawing.Rectangle" /> that represents the converted <see cref="T:System.Drawing.Printing.PrinterUnit" />.</returns>
		// Token: 0x0600089A RID: 2202 RVA: 0x00006DEC File Offset: 0x00004FEC
		public static Rectangle Convert(Rectangle value, PrinterUnit fromUnit, PrinterUnit toUnit)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(Rectangle);
		}

		/// <summary>Converts a <see cref="T:System.Drawing.Size" /> from one <see cref="T:System.Drawing.Printing.PrinterUnit" /> type to another <see cref="T:System.Drawing.Printing.PrinterUnit" /> type.</summary>
		/// <param name="value">The <see cref="T:System.Drawing.Size" /> being converted. </param>
		/// <param name="fromUnit">The unit to convert from. </param>
		/// <param name="toUnit">The unit to convert to. </param>
		/// <returns>A <see cref="T:System.Drawing.Size" /> that represents the converted <see cref="T:System.Drawing.Printing.PrinterUnit" />.</returns>
		// Token: 0x0600089B RID: 2203 RVA: 0x00006E08 File Offset: 0x00005008
		public static Size Convert(Size value, PrinterUnit fromUnit, PrinterUnit toUnit)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(Size);
		}

		/// <summary>Converts a 32-bit signed integer from one <see cref="T:System.Drawing.Printing.PrinterUnit" /> type to another <see cref="T:System.Drawing.Printing.PrinterUnit" /> type.</summary>
		/// <param name="value">The value being converted. </param>
		/// <param name="fromUnit">The unit to convert from. </param>
		/// <param name="toUnit">The unit to convert to. </param>
		/// <returns>A 32-bit signed integer that represents the converted <see cref="T:System.Drawing.Printing.PrinterUnit" />.</returns>
		// Token: 0x0600089C RID: 2204 RVA: 0x00006E24 File Offset: 0x00005024
		public static int Convert(int value, PrinterUnit fromUnit, PrinterUnit toUnit)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}
	}
}
