﻿using System;
using Unity;

namespace System.Drawing.Printing
{
	/// <summary>Specifies print preview information for a single page. This class cannot be inherited.</summary>
	// Token: 0x0200009D RID: 157
	public sealed class PreviewPageInfo
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.PreviewPageInfo" /> class.</summary>
		/// <param name="image">The image of the printed page. </param>
		/// <param name="physicalSize">The size of the printed page, in hundredths of an inch. </param>
		// Token: 0x06000852 RID: 2130 RVA: 0x00004644 File Offset: 0x00002844
		public PreviewPageInfo(Image image, Size physicalSize)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the image of the printed page.</summary>
		/// <returns>An <see cref="T:System.Drawing.Image" /> representing the printed page.</returns>
		// Token: 0x1700036B RID: 875
		// (get) Token: 0x06000853 RID: 2131 RVA: 0x00004667 File Offset: 0x00002867
		public Image Image
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the size of the printed page, in hundredths of an inch.</summary>
		/// <returns>A <see cref="T:System.Drawing.Size" /> that specifies the size of the printed page, in hundredths of an inch.</returns>
		// Token: 0x1700036C RID: 876
		// (get) Token: 0x06000854 RID: 2132 RVA: 0x00006C9C File Offset: 0x00004E9C
		public Size PhysicalSize
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Size);
			}
		}
	}
}
