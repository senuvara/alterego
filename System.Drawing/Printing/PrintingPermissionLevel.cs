﻿using System;

namespace System.Drawing.Printing
{
	/// <summary>Specifies the type of printing that code is allowed to do.</summary>
	// Token: 0x020000AB RID: 171
	[Serializable]
	public enum PrintingPermissionLevel
	{
		/// <summary>Provides full access to all printers.</summary>
		// Token: 0x040003D1 RID: 977
		AllPrinting = 3,
		/// <summary>Provides printing programmatically to the default printer, along with safe printing through semirestricted dialog box. <see cref="F:System.Drawing.Printing.PrintingPermissionLevel.DefaultPrinting" /> is a subset of <see cref="F:System.Drawing.Printing.PrintingPermissionLevel.AllPrinting" />.</summary>
		// Token: 0x040003D2 RID: 978
		DefaultPrinting = 2,
		/// <summary>Prevents access to printers. <see cref="F:System.Drawing.Printing.PrintingPermissionLevel.NoPrinting" /> is a subset of <see cref="F:System.Drawing.Printing.PrintingPermissionLevel.SafePrinting" />.</summary>
		// Token: 0x040003D3 RID: 979
		NoPrinting = 0,
		/// <summary>Provides printing only from a restricted dialog box. <see cref="F:System.Drawing.Printing.PrintingPermissionLevel.SafePrinting" /> is a subset of <see cref="F:System.Drawing.Printing.PrintingPermissionLevel.DefaultPrinting" />.</summary>
		// Token: 0x040003D4 RID: 980
		SafePrinting
	}
}
