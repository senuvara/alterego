﻿using System;
using System.Runtime.Serialization;
using Unity;

namespace System.Drawing.Printing
{
	/// <summary>Represents the exception that is thrown when you try to access a printer using printer settings that are not valid.</summary>
	// Token: 0x0200008D RID: 141
	[Serializable]
	public class InvalidPrinterException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.InvalidPrinterException" /> class.</summary>
		/// <param name="settings">A <see cref="T:System.Drawing.Printing.PrinterSettings" /> that specifies the settings for a printer. </param>
		// Token: 0x060007B8 RID: 1976 RVA: 0x00004644 File Offset: 0x00002844
		public InvalidPrinterException(PrinterSettings settings)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.InvalidPrinterException" /> class with serialized data.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown. </param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="info" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">The class name is <see langword="null" /> or <see cref="P:System.Exception.HResult" /> is 0. </exception>
		// Token: 0x060007B9 RID: 1977 RVA: 0x00004644 File Offset: 0x00002844
		protected InvalidPrinterException(SerializationInfo info, StreamingContext context)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
