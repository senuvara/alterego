﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Drawing.Printing
{
	/// <summary>Provides a <see cref="T:System.Drawing.Printing.MarginsConverter" /> for <see cref="T:System.Drawing.Printing.Margins" />.</summary>
	// Token: 0x0200009B RID: 155
	public class MarginsConverter : ExpandableObjectConverter
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.MarginsConverter" /> class. </summary>
		// Token: 0x06000851 RID: 2129 RVA: 0x00004644 File Offset: 0x00002844
		public MarginsConverter()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
