﻿using System;
using System.Security;
using System.Security.Permissions;
using Unity;

namespace System.Drawing.Printing
{
	/// <summary>Allows declarative printing permission checks.</summary>
	// Token: 0x020000AC RID: 172
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
	public sealed class PrintingPermissionAttribute : CodeAccessSecurityAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Printing.PrintingPermissionAttribute" /> class.</summary>
		/// <param name="action">One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values. </param>
		// Token: 0x060008A7 RID: 2215 RVA: 0x00004642 File Offset: 0x00002842
		public PrintingPermissionAttribute(SecurityAction action)
		{
		}

		/// <summary>Gets or sets the type of printing allowed.</summary>
		/// <returns>One of the <see cref="T:System.Drawing.Printing.PrintingPermissionLevel" /> values.</returns>
		/// <exception cref="T:System.ArgumentException">The value is not one of the <see cref="T:System.Drawing.Printing.PrintingPermissionLevel" /> values. </exception>
		// Token: 0x1700037E RID: 894
		// (get) Token: 0x060008A8 RID: 2216 RVA: 0x00006E94 File Offset: 0x00005094
		// (set) Token: 0x060008A9 RID: 2217 RVA: 0x00004644 File Offset: 0x00002844
		public PrintingPermissionLevel Level
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return PrintingPermissionLevel.NoPrinting;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Creates the permission based on the requested access levels, which are set through the <see cref="P:System.Drawing.Printing.PrintingPermissionAttribute.Level" /> property on the attribute.</summary>
		/// <returns>An <see cref="T:System.Security.IPermission" /> that represents the created permission.</returns>
		// Token: 0x060008AA RID: 2218 RVA: 0x00004667 File Offset: 0x00002867
		public override IPermission CreatePermission()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
