﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace System.Drawing
{
	/// <summary>Stores a set of four integers that represent the location and size of a rectangle</summary>
	// Token: 0x02000009 RID: 9
	[ComVisible(true)]
	[Serializable]
	public struct Rectangle
	{
		/// <summary>Converts the specified <see cref="T:System.Drawing.RectangleF" /> structure to a <see cref="T:System.Drawing.Rectangle" /> structure by rounding the <see cref="T:System.Drawing.RectangleF" /> values to the next higher integer values.</summary>
		/// <param name="value">The <see cref="T:System.Drawing.RectangleF" /> structure to be converted. </param>
		/// <returns>Returns a <see cref="T:System.Drawing.Rectangle" />.</returns>
		// Token: 0x060000DD RID: 221 RVA: 0x00003720 File Offset: 0x00001920
		public static Rectangle Ceiling(RectangleF value)
		{
			checked
			{
				int num = (int)Math.Ceiling((double)value.X);
				int num2 = (int)Math.Ceiling((double)value.Y);
				int num3 = (int)Math.Ceiling((double)value.Width);
				int num4 = (int)Math.Ceiling((double)value.Height);
				return new Rectangle(num, num2, num3, num4);
			}
		}

		/// <summary>Creates a <see cref="T:System.Drawing.Rectangle" /> structure with the specified edge locations.</summary>
		/// <param name="left">The x-coordinate of the upper-left corner of this <see cref="T:System.Drawing.Rectangle" /> structure. </param>
		/// <param name="top">The y-coordinate of the upper-left corner of this <see cref="T:System.Drawing.Rectangle" /> structure. </param>
		/// <param name="right">The x-coordinate of the lower-right corner of this <see cref="T:System.Drawing.Rectangle" /> structure. </param>
		/// <param name="bottom">The y-coordinate of the lower-right corner of this <see cref="T:System.Drawing.Rectangle" /> structure. </param>
		/// <returns>The new <see cref="T:System.Drawing.Rectangle" /> that this method creates.</returns>
		// Token: 0x060000DE RID: 222 RVA: 0x00003770 File Offset: 0x00001970
		public static Rectangle FromLTRB(int left, int top, int right, int bottom)
		{
			return new Rectangle(left, top, right - left, bottom - top);
		}

		/// <summary>Creates and returns an enlarged copy of the specified <see cref="T:System.Drawing.Rectangle" /> structure. The copy is enlarged by the specified amount. The original <see cref="T:System.Drawing.Rectangle" /> structure remains unmodified.</summary>
		/// <param name="rect">The <see cref="T:System.Drawing.Rectangle" /> with which to start. This rectangle is not modified. </param>
		/// <param name="x">The amount to inflate this <see cref="T:System.Drawing.Rectangle" /> horizontally. </param>
		/// <param name="y">The amount to inflate this <see cref="T:System.Drawing.Rectangle" /> vertically. </param>
		/// <returns>The enlarged <see cref="T:System.Drawing.Rectangle" />.</returns>
		// Token: 0x060000DF RID: 223 RVA: 0x00003780 File Offset: 0x00001980
		public static Rectangle Inflate(Rectangle rect, int x, int y)
		{
			Rectangle result = new Rectangle(rect.Location, rect.Size);
			result.Inflate(x, y);
			return result;
		}

		/// <summary>Enlarges this <see cref="T:System.Drawing.Rectangle" /> by the specified amount.</summary>
		/// <param name="width">The amount to inflate this <see cref="T:System.Drawing.Rectangle" /> horizontally. </param>
		/// <param name="height">The amount to inflate this <see cref="T:System.Drawing.Rectangle" /> vertically. </param>
		// Token: 0x060000E0 RID: 224 RVA: 0x000037AC File Offset: 0x000019AC
		public void Inflate(int width, int height)
		{
			this.Inflate(new Size(width, height));
		}

		/// <summary>Enlarges this <see cref="T:System.Drawing.Rectangle" /> by the specified amount.</summary>
		/// <param name="size">The amount to inflate this rectangle. </param>
		// Token: 0x060000E1 RID: 225 RVA: 0x000037BC File Offset: 0x000019BC
		public void Inflate(Size size)
		{
			this.x -= size.Width;
			this.y -= size.Height;
			this.Width += size.Width * 2;
			this.Height += size.Height * 2;
		}

		/// <summary>Returns a third <see cref="T:System.Drawing.Rectangle" /> structure that represents the intersection of two other <see cref="T:System.Drawing.Rectangle" /> structures. If there is no intersection, an empty <see cref="T:System.Drawing.Rectangle" /> is returned.</summary>
		/// <param name="a">A rectangle to intersect. </param>
		/// <param name="b">A rectangle to intersect. </param>
		/// <returns>A <see cref="T:System.Drawing.Rectangle" /> that represents the intersection of <paramref name="a" /> and <paramref name="b" />.</returns>
		// Token: 0x060000E2 RID: 226 RVA: 0x00003820 File Offset: 0x00001A20
		public static Rectangle Intersect(Rectangle a, Rectangle b)
		{
			if (!a.IntersectsWithInclusive(b))
			{
				return Rectangle.Empty;
			}
			return Rectangle.FromLTRB(Math.Max(a.Left, b.Left), Math.Max(a.Top, b.Top), Math.Min(a.Right, b.Right), Math.Min(a.Bottom, b.Bottom));
		}

		/// <summary>Replaces this <see cref="T:System.Drawing.Rectangle" /> with the intersection of itself and the specified <see cref="T:System.Drawing.Rectangle" />.</summary>
		/// <param name="rect">The <see cref="T:System.Drawing.Rectangle" /> with which to intersect. </param>
		// Token: 0x060000E3 RID: 227 RVA: 0x0000388E File Offset: 0x00001A8E
		public void Intersect(Rectangle rect)
		{
			this = Rectangle.Intersect(this, rect);
		}

		/// <summary>Converts the specified <see cref="T:System.Drawing.RectangleF" /> to a <see cref="T:System.Drawing.Rectangle" /> by rounding the <see cref="T:System.Drawing.RectangleF" /> values to the nearest integer values.</summary>
		/// <param name="value">The <see cref="T:System.Drawing.RectangleF" /> to be converted. </param>
		/// <returns>The rounded interger value of the <see cref="T:System.Drawing.Rectangle" />.</returns>
		// Token: 0x060000E4 RID: 228 RVA: 0x000038A4 File Offset: 0x00001AA4
		public static Rectangle Round(RectangleF value)
		{
			checked
			{
				int num = (int)Math.Round((double)value.X);
				int num2 = (int)Math.Round((double)value.Y);
				int num3 = (int)Math.Round((double)value.Width);
				int num4 = (int)Math.Round((double)value.Height);
				return new Rectangle(num, num2, num3, num4);
			}
		}

		/// <summary>Converts the specified <see cref="T:System.Drawing.RectangleF" /> to a <see cref="T:System.Drawing.Rectangle" /> by truncating the <see cref="T:System.Drawing.RectangleF" /> values.</summary>
		/// <param name="value">The <see cref="T:System.Drawing.RectangleF" /> to be converted. </param>
		/// <returns>The truncated value of the  <see cref="T:System.Drawing.Rectangle" />.</returns>
		// Token: 0x060000E5 RID: 229 RVA: 0x000038F4 File Offset: 0x00001AF4
		public static Rectangle Truncate(RectangleF value)
		{
			checked
			{
				int num = (int)value.X;
				int num2 = (int)value.Y;
				int num3 = (int)value.Width;
				int num4 = (int)value.Height;
				return new Rectangle(num, num2, num3, num4);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Rectangle" /> structure that contains the union of two <see cref="T:System.Drawing.Rectangle" /> structures.</summary>
		/// <param name="a">A rectangle to union. </param>
		/// <param name="b">A rectangle to union. </param>
		/// <returns>A <see cref="T:System.Drawing.Rectangle" /> structure that bounds the union of the two <see cref="T:System.Drawing.Rectangle" /> structures.</returns>
		// Token: 0x060000E6 RID: 230 RVA: 0x0000392C File Offset: 0x00001B2C
		public static Rectangle Union(Rectangle a, Rectangle b)
		{
			return Rectangle.FromLTRB(Math.Min(a.Left, b.Left), Math.Min(a.Top, b.Top), Math.Max(a.Right, b.Right), Math.Max(a.Bottom, b.Bottom));
		}

		/// <summary>Tests whether two <see cref="T:System.Drawing.Rectangle" /> structures have equal location and size.</summary>
		/// <param name="left">The <see cref="T:System.Drawing.Rectangle" /> structure that is to the left of the equality operator. </param>
		/// <param name="right">The <see cref="T:System.Drawing.Rectangle" /> structure that is to the right of the equality operator. </param>
		/// <returns>This operator returns <see langword="true" /> if the two <see cref="T:System.Drawing.Rectangle" /> structures have equal <see cref="P:System.Drawing.Rectangle.X" />, <see cref="P:System.Drawing.Rectangle.Y" />, <see cref="P:System.Drawing.Rectangle.Width" />, and <see cref="P:System.Drawing.Rectangle.Height" /> properties.</returns>
		// Token: 0x060000E7 RID: 231 RVA: 0x0000398A File Offset: 0x00001B8A
		public static bool operator ==(Rectangle left, Rectangle right)
		{
			return left.Location == right.Location && left.Size == right.Size;
		}

		/// <summary>Tests whether two <see cref="T:System.Drawing.Rectangle" /> structures differ in location or size.</summary>
		/// <param name="left">The <see cref="T:System.Drawing.Rectangle" /> structure that is to the left of the inequality operator. </param>
		/// <param name="right">The <see cref="T:System.Drawing.Rectangle" /> structure that is to the right of the inequality operator. </param>
		/// <returns>This operator returns <see langword="true" /> if any of the <see cref="P:System.Drawing.Rectangle.X" />, <see cref="P:System.Drawing.Rectangle.Y" />, <see cref="P:System.Drawing.Rectangle.Width" /> or <see cref="P:System.Drawing.Rectangle.Height" /> properties of the two <see cref="T:System.Drawing.Rectangle" /> structures are unequal; otherwise <see langword="false" />.</returns>
		// Token: 0x060000E8 RID: 232 RVA: 0x000039B6 File Offset: 0x00001BB6
		public static bool operator !=(Rectangle left, Rectangle right)
		{
			return left.Location != right.Location || left.Size != right.Size;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Rectangle" /> class with the specified location and size.</summary>
		/// <param name="location">A <see cref="T:System.Drawing.Point" /> that represents the upper-left corner of the rectangular region. </param>
		/// <param name="size">A <see cref="T:System.Drawing.Size" /> that represents the width and height of the rectangular region. </param>
		// Token: 0x060000E9 RID: 233 RVA: 0x000039E2 File Offset: 0x00001BE2
		public Rectangle(Point location, Size size)
		{
			this.x = location.X;
			this.y = location.Y;
			this.width = size.Width;
			this.height = size.Height;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Rectangle" /> class with the specified location and size.</summary>
		/// <param name="x">The x-coordinate of the upper-left corner of the rectangle. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the rectangle. </param>
		/// <param name="width">The width of the rectangle. </param>
		/// <param name="height">The height of the rectangle. </param>
		// Token: 0x060000EA RID: 234 RVA: 0x00003A18 File Offset: 0x00001C18
		public Rectangle(int x, int y, int width, int height)
		{
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
		}

		/// <summary>Gets the y-coordinate that is the sum of the <see cref="P:System.Drawing.Rectangle.Y" /> and <see cref="P:System.Drawing.Rectangle.Height" /> property values of this <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <returns>The y-coordinate that is the sum of <see cref="P:System.Drawing.Rectangle.Y" /> and <see cref="P:System.Drawing.Rectangle.Height" /> of this <see cref="T:System.Drawing.Rectangle" />.</returns>
		// Token: 0x1700009E RID: 158
		// (get) Token: 0x060000EB RID: 235 RVA: 0x00003A37 File Offset: 0x00001C37
		[Browsable(false)]
		public int Bottom
		{
			get
			{
				return this.y + this.height;
			}
		}

		/// <summary>Gets or sets the height of this <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <returns>The height of this <see cref="T:System.Drawing.Rectangle" /> structure. The default is 0.</returns>
		// Token: 0x1700009F RID: 159
		// (get) Token: 0x060000EC RID: 236 RVA: 0x00003A46 File Offset: 0x00001C46
		// (set) Token: 0x060000ED RID: 237 RVA: 0x00003A4E File Offset: 0x00001C4E
		public int Height
		{
			get
			{
				return this.height;
			}
			set
			{
				this.height = value;
			}
		}

		/// <summary>Tests whether all numeric properties of this <see cref="T:System.Drawing.Rectangle" /> have values of zero.</summary>
		/// <returns>This property returns <see langword="true" /> if the <see cref="P:System.Drawing.Rectangle.Width" />, <see cref="P:System.Drawing.Rectangle.Height" />, <see cref="P:System.Drawing.Rectangle.X" />, and <see cref="P:System.Drawing.Rectangle.Y" /> properties of this <see cref="T:System.Drawing.Rectangle" /> all have values of zero; otherwise, <see langword="false" />.</returns>
		// Token: 0x170000A0 RID: 160
		// (get) Token: 0x060000EE RID: 238 RVA: 0x00003A57 File Offset: 0x00001C57
		[Browsable(false)]
		public bool IsEmpty
		{
			get
			{
				return this.x == 0 && this.y == 0 && this.width == 0 && this.height == 0;
			}
		}

		/// <summary>Gets the x-coordinate of the left edge of this <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <returns>The x-coordinate of the left edge of this <see cref="T:System.Drawing.Rectangle" /> structure.</returns>
		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x060000EF RID: 239 RVA: 0x00003A7C File Offset: 0x00001C7C
		[Browsable(false)]
		public int Left
		{
			get
			{
				return this.X;
			}
		}

		/// <summary>Gets or sets the coordinates of the upper-left corner of this <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <returns>A <see cref="T:System.Drawing.Point" /> that represents the upper-left corner of this <see cref="T:System.Drawing.Rectangle" /> structure.</returns>
		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x060000F0 RID: 240 RVA: 0x00003A84 File Offset: 0x00001C84
		// (set) Token: 0x060000F1 RID: 241 RVA: 0x00003A97 File Offset: 0x00001C97
		[Browsable(false)]
		public Point Location
		{
			get
			{
				return new Point(this.x, this.y);
			}
			set
			{
				this.x = value.X;
				this.y = value.Y;
			}
		}

		/// <summary>Gets the x-coordinate that is the sum of <see cref="P:System.Drawing.Rectangle.X" /> and <see cref="P:System.Drawing.Rectangle.Width" /> property values of this <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <returns>The x-coordinate that is the sum of <see cref="P:System.Drawing.Rectangle.X" /> and <see cref="P:System.Drawing.Rectangle.Width" /> of this <see cref="T:System.Drawing.Rectangle" />.</returns>
		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x060000F2 RID: 242 RVA: 0x00003AB3 File Offset: 0x00001CB3
		[Browsable(false)]
		public int Right
		{
			get
			{
				return this.X + this.Width;
			}
		}

		/// <summary>Gets or sets the size of this <see cref="T:System.Drawing.Rectangle" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.Size" /> that represents the width and height of this <see cref="T:System.Drawing.Rectangle" /> structure.</returns>
		// Token: 0x170000A4 RID: 164
		// (get) Token: 0x060000F3 RID: 243 RVA: 0x00003AC2 File Offset: 0x00001CC2
		// (set) Token: 0x060000F4 RID: 244 RVA: 0x00003AD5 File Offset: 0x00001CD5
		[Browsable(false)]
		public Size Size
		{
			get
			{
				return new Size(this.Width, this.Height);
			}
			set
			{
				this.Width = value.Width;
				this.Height = value.Height;
			}
		}

		/// <summary>Gets the y-coordinate of the top edge of this <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <returns>The y-coordinate of the top edge of this <see cref="T:System.Drawing.Rectangle" /> structure.</returns>
		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x060000F5 RID: 245 RVA: 0x00003AF1 File Offset: 0x00001CF1
		[Browsable(false)]
		public int Top
		{
			get
			{
				return this.y;
			}
		}

		/// <summary>Gets or sets the width of this <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <returns>The width of this <see cref="T:System.Drawing.Rectangle" /> structure. The default is 0.</returns>
		// Token: 0x170000A6 RID: 166
		// (get) Token: 0x060000F6 RID: 246 RVA: 0x00003AF9 File Offset: 0x00001CF9
		// (set) Token: 0x060000F7 RID: 247 RVA: 0x00003B01 File Offset: 0x00001D01
		public int Width
		{
			get
			{
				return this.width;
			}
			set
			{
				this.width = value;
			}
		}

		/// <summary>Gets or sets the x-coordinate of the upper-left corner of this <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <returns>The x-coordinate of the upper-left corner of this <see cref="T:System.Drawing.Rectangle" /> structure. The default is 0.</returns>
		// Token: 0x170000A7 RID: 167
		// (get) Token: 0x060000F8 RID: 248 RVA: 0x00003B0A File Offset: 0x00001D0A
		// (set) Token: 0x060000F9 RID: 249 RVA: 0x00003B12 File Offset: 0x00001D12
		public int X
		{
			get
			{
				return this.x;
			}
			set
			{
				this.x = value;
			}
		}

		/// <summary>Gets or sets the y-coordinate of the upper-left corner of this <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <returns>The y-coordinate of the upper-left corner of this <see cref="T:System.Drawing.Rectangle" /> structure. The default is 0.</returns>
		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x060000FA RID: 250 RVA: 0x00003AF1 File Offset: 0x00001CF1
		// (set) Token: 0x060000FB RID: 251 RVA: 0x00003B1B File Offset: 0x00001D1B
		public int Y
		{
			get
			{
				return this.y;
			}
			set
			{
				this.y = value;
			}
		}

		/// <summary>Determines if the specified point is contained within this <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <param name="x">The x-coordinate of the point to test. </param>
		/// <param name="y">The y-coordinate of the point to test. </param>
		/// <returns>This method returns <see langword="true" /> if the point defined by <paramref name="x" /> and <paramref name="y" /> is contained within this <see cref="T:System.Drawing.Rectangle" /> structure; otherwise <see langword="false" />.</returns>
		// Token: 0x060000FC RID: 252 RVA: 0x00003B24 File Offset: 0x00001D24
		public bool Contains(int x, int y)
		{
			return x >= this.Left && x < this.Right && y >= this.Top && y < this.Bottom;
		}

		/// <summary>Determines if the specified point is contained within this <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <param name="pt">The <see cref="T:System.Drawing.Point" /> to test. </param>
		/// <returns>This method returns <see langword="true" /> if the point represented by <paramref name="pt" /> is contained within this <see cref="T:System.Drawing.Rectangle" /> structure; otherwise <see langword="false" />.</returns>
		// Token: 0x060000FD RID: 253 RVA: 0x00003B4C File Offset: 0x00001D4C
		public bool Contains(Point pt)
		{
			return this.Contains(pt.X, pt.Y);
		}

		/// <summary>Determines if the rectangular region represented by <paramref name="rect" /> is entirely contained within this <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <param name="rect">The <see cref="T:System.Drawing.Rectangle" /> to test. </param>
		/// <returns>This method returns <see langword="true" /> if the rectangular region represented by <paramref name="rect" /> is entirely contained within this <see cref="T:System.Drawing.Rectangle" /> structure; otherwise <see langword="false" />.</returns>
		// Token: 0x060000FE RID: 254 RVA: 0x00003B62 File Offset: 0x00001D62
		public bool Contains(Rectangle rect)
		{
			return rect == Rectangle.Intersect(this, rect);
		}

		/// <summary>Tests whether <paramref name="obj" /> is a <see cref="T:System.Drawing.Rectangle" /> structure with the same location and size of this <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <param name="obj">The <see cref="T:System.Object" /> to test. </param>
		/// <returns>This method returns <see langword="true" /> if <paramref name="obj" /> is a <see cref="T:System.Drawing.Rectangle" /> structure and its <see cref="P:System.Drawing.Rectangle.X" />, <see cref="P:System.Drawing.Rectangle.Y" />, <see cref="P:System.Drawing.Rectangle.Width" />, and <see cref="P:System.Drawing.Rectangle.Height" /> properties are equal to the corresponding properties of this <see cref="T:System.Drawing.Rectangle" /> structure; otherwise, <see langword="false" />.</returns>
		// Token: 0x060000FF RID: 255 RVA: 0x00003B76 File Offset: 0x00001D76
		public override bool Equals(object obj)
		{
			return obj is Rectangle && this == (Rectangle)obj;
		}

		/// <summary>Returns the hash code for this <see cref="T:System.Drawing.Rectangle" /> structure. For information about the use of hash codes, see <see cref="M:System.Object.GetHashCode" /> .</summary>
		/// <returns>An integer that represents the hash code for this rectangle.</returns>
		// Token: 0x06000100 RID: 256 RVA: 0x00003B93 File Offset: 0x00001D93
		public override int GetHashCode()
		{
			return this.height + this.width ^ this.x + this.y;
		}

		/// <summary>Determines if this rectangle intersects with <paramref name="rect" />.</summary>
		/// <param name="rect">The rectangle to test. </param>
		/// <returns>This method returns <see langword="true" /> if there is any intersection, otherwise <see langword="false" />.</returns>
		// Token: 0x06000101 RID: 257 RVA: 0x00003BB0 File Offset: 0x00001DB0
		public bool IntersectsWith(Rectangle rect)
		{
			return this.Left < rect.Right && this.Right > rect.Left && this.Top < rect.Bottom && this.Bottom > rect.Top;
		}

		// Token: 0x06000102 RID: 258 RVA: 0x00003BF0 File Offset: 0x00001DF0
		private bool IntersectsWithInclusive(Rectangle r)
		{
			return this.Left <= r.Right && this.Right >= r.Left && this.Top <= r.Bottom && this.Bottom >= r.Top;
		}

		/// <summary>Adjusts the location of this rectangle by the specified amount.</summary>
		/// <param name="x">The horizontal offset. </param>
		/// <param name="y">The vertical offset. </param>
		// Token: 0x06000103 RID: 259 RVA: 0x00003C3E File Offset: 0x00001E3E
		public void Offset(int x, int y)
		{
			this.x += x;
			this.y += y;
		}

		/// <summary>Adjusts the location of this rectangle by the specified amount.</summary>
		/// <param name="pos">Amount to offset the location. </param>
		// Token: 0x06000104 RID: 260 RVA: 0x00003C5C File Offset: 0x00001E5C
		public void Offset(Point pos)
		{
			this.x += pos.X;
			this.y += pos.Y;
		}

		/// <summary>Converts the attributes of this <see cref="T:System.Drawing.Rectangle" /> to a human-readable string.</summary>
		/// <returns>A string that contains the position, width, and height of this <see cref="T:System.Drawing.Rectangle" /> structure ¾ for example, {X=20, Y=20, Width=100, Height=50} </returns>
		// Token: 0x06000105 RID: 261 RVA: 0x00003C88 File Offset: 0x00001E88
		public override string ToString()
		{
			return string.Format("{{X={0},Y={1},Width={2},Height={3}}}", new object[]
			{
				this.x,
				this.y,
				this.width,
				this.height
			});
		}

		// Token: 0x040000EB RID: 235
		private int x;

		// Token: 0x040000EC RID: 236
		private int y;

		// Token: 0x040000ED RID: 237
		private int width;

		// Token: 0x040000EE RID: 238
		private int height;

		/// <summary>Represents a <see cref="T:System.Drawing.Rectangle" /> structure with its properties left uninitialized.</summary>
		// Token: 0x040000EF RID: 239
		public static readonly Rectangle Empty;
	}
}
