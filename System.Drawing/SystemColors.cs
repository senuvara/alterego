﻿using System;
using Unity;

namespace System.Drawing
{
	/// <summary>Each property of the <see cref="T:System.Drawing.SystemColors" /> class is a <see cref="T:System.Drawing.Color" /> structure that is the color of a Windows display element.</summary>
	// Token: 0x02000085 RID: 133
	public sealed class SystemColors
	{
		// Token: 0x06000742 RID: 1858 RVA: 0x00004644 File Offset: 0x00002844
		internal SystemColors()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the color of the active window's border.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the color of the active window's border.</returns>
		// Token: 0x170002DB RID: 731
		// (get) Token: 0x06000743 RID: 1859 RVA: 0x0000628C File Offset: 0x0000448C
		public static Color ActiveBorder
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the color of the background of the active window's title bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the color of the active window's title bar.</returns>
		// Token: 0x170002DC RID: 732
		// (get) Token: 0x06000744 RID: 1860 RVA: 0x000062A8 File Offset: 0x000044A8
		public static Color ActiveCaption
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the color of the text in the active window's title bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the color of the text in the active window's title bar.</returns>
		// Token: 0x170002DD RID: 733
		// (get) Token: 0x06000745 RID: 1861 RVA: 0x000062C4 File Offset: 0x000044C4
		public static Color ActiveCaptionText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the color of the application workspace. </summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the color of the application workspace.</returns>
		// Token: 0x170002DE RID: 734
		// (get) Token: 0x06000746 RID: 1862 RVA: 0x000062E0 File Offset: 0x000044E0
		public static Color AppWorkspace
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the face color of a 3-D element.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the face color of a 3-D element.</returns>
		// Token: 0x170002DF RID: 735
		// (get) Token: 0x06000747 RID: 1863 RVA: 0x000062FC File Offset: 0x000044FC
		public static Color ButtonFace
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the highlight color of a 3-D element. </summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the highlight color of a 3-D element.</returns>
		// Token: 0x170002E0 RID: 736
		// (get) Token: 0x06000748 RID: 1864 RVA: 0x00006318 File Offset: 0x00004518
		public static Color ButtonHighlight
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the shadow color of a 3-D element. </summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the shadow color of a 3-D element.</returns>
		// Token: 0x170002E1 RID: 737
		// (get) Token: 0x06000749 RID: 1865 RVA: 0x00006334 File Offset: 0x00004534
		public static Color ButtonShadow
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the face color of a 3-D element.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the face color of a 3-D element.</returns>
		// Token: 0x170002E2 RID: 738
		// (get) Token: 0x0600074A RID: 1866 RVA: 0x00006350 File Offset: 0x00004550
		public static Color Control
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the shadow color of a 3-D element. </summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the shadow color of a 3-D element.</returns>
		// Token: 0x170002E3 RID: 739
		// (get) Token: 0x0600074B RID: 1867 RVA: 0x0000636C File Offset: 0x0000456C
		public static Color ControlDark
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the dark shadow color of a 3-D element. </summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the dark shadow color of a 3-D element.</returns>
		// Token: 0x170002E4 RID: 740
		// (get) Token: 0x0600074C RID: 1868 RVA: 0x00006388 File Offset: 0x00004588
		public static Color ControlDarkDark
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the light color of a 3-D element. </summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the light color of a 3-D element.</returns>
		// Token: 0x170002E5 RID: 741
		// (get) Token: 0x0600074D RID: 1869 RVA: 0x000063A4 File Offset: 0x000045A4
		public static Color ControlLight
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the highlight color of a 3-D element. </summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the highlight color of a 3-D element.</returns>
		// Token: 0x170002E6 RID: 742
		// (get) Token: 0x0600074E RID: 1870 RVA: 0x000063C0 File Offset: 0x000045C0
		public static Color ControlLightLight
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the color of text in a 3-D element.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the color of text in a 3-D element.</returns>
		// Token: 0x170002E7 RID: 743
		// (get) Token: 0x0600074F RID: 1871 RVA: 0x000063DC File Offset: 0x000045DC
		public static Color ControlText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the color of the desktop.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the color of the desktop.</returns>
		// Token: 0x170002E8 RID: 744
		// (get) Token: 0x06000750 RID: 1872 RVA: 0x000063F8 File Offset: 0x000045F8
		public static Color Desktop
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the lightest color in the color gradient of an active window's title bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the lightest color in the color gradient of an active window's title bar.</returns>
		// Token: 0x170002E9 RID: 745
		// (get) Token: 0x06000751 RID: 1873 RVA: 0x00006414 File Offset: 0x00004614
		public static Color GradientActiveCaption
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the lightest color in the color gradient of an inactive window's title bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the lightest color in the color gradient of an inactive window's title bar.</returns>
		// Token: 0x170002EA RID: 746
		// (get) Token: 0x06000752 RID: 1874 RVA: 0x00006430 File Offset: 0x00004630
		public static Color GradientInactiveCaption
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the color of dimmed text. </summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the color of dimmed text.</returns>
		// Token: 0x170002EB RID: 747
		// (get) Token: 0x06000753 RID: 1875 RVA: 0x0000644C File Offset: 0x0000464C
		public static Color GrayText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the color of the background of selected items.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the color of the background of selected items.</returns>
		// Token: 0x170002EC RID: 748
		// (get) Token: 0x06000754 RID: 1876 RVA: 0x00006468 File Offset: 0x00004668
		public static Color Highlight
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the color of the text of selected items.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the color of the text of selected items.</returns>
		// Token: 0x170002ED RID: 749
		// (get) Token: 0x06000755 RID: 1877 RVA: 0x00006484 File Offset: 0x00004684
		public static Color HighlightText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the color used to designate a hot-tracked item. </summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the color used to designate a hot-tracked item.</returns>
		// Token: 0x170002EE RID: 750
		// (get) Token: 0x06000756 RID: 1878 RVA: 0x000064A0 File Offset: 0x000046A0
		public static Color HotTrack
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the color of an inactive window's border.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the color of an inactive window's border.</returns>
		// Token: 0x170002EF RID: 751
		// (get) Token: 0x06000757 RID: 1879 RVA: 0x000064BC File Offset: 0x000046BC
		public static Color InactiveBorder
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the color of the background of an inactive window's title bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the color of the background of an inactive window's title bar.</returns>
		// Token: 0x170002F0 RID: 752
		// (get) Token: 0x06000758 RID: 1880 RVA: 0x000064D8 File Offset: 0x000046D8
		public static Color InactiveCaption
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the color of the text in an inactive window's title bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the color of the text in an inactive window's title bar.</returns>
		// Token: 0x170002F1 RID: 753
		// (get) Token: 0x06000759 RID: 1881 RVA: 0x000064F4 File Offset: 0x000046F4
		public static Color InactiveCaptionText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the color of the background of a ToolTip.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the color of the background of a ToolTip.</returns>
		// Token: 0x170002F2 RID: 754
		// (get) Token: 0x0600075A RID: 1882 RVA: 0x00006510 File Offset: 0x00004710
		public static Color Info
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the color of the text of a ToolTip.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the color of the text of a ToolTip.</returns>
		// Token: 0x170002F3 RID: 755
		// (get) Token: 0x0600075B RID: 1883 RVA: 0x0000652C File Offset: 0x0000472C
		public static Color InfoText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the color of a menu's background.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the color of a menu's background.</returns>
		// Token: 0x170002F4 RID: 756
		// (get) Token: 0x0600075C RID: 1884 RVA: 0x00006548 File Offset: 0x00004748
		public static Color Menu
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the color of the background of a menu bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the color of the background of a menu bar.</returns>
		// Token: 0x170002F5 RID: 757
		// (get) Token: 0x0600075D RID: 1885 RVA: 0x00006564 File Offset: 0x00004764
		public static Color MenuBar
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the color used to highlight menu items when the menu appears as a flat menu.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the color used to highlight menu items when the menu appears as a flat menu.</returns>
		// Token: 0x170002F6 RID: 758
		// (get) Token: 0x0600075E RID: 1886 RVA: 0x00006580 File Offset: 0x00004780
		public static Color MenuHighlight
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the color of a menu's text.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the color of a menu's text.</returns>
		// Token: 0x170002F7 RID: 759
		// (get) Token: 0x0600075F RID: 1887 RVA: 0x0000659C File Offset: 0x0000479C
		public static Color MenuText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the color of the background of a scroll bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the color of the background of a scroll bar.</returns>
		// Token: 0x170002F8 RID: 760
		// (get) Token: 0x06000760 RID: 1888 RVA: 0x000065B8 File Offset: 0x000047B8
		public static Color ScrollBar
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the color of the background in the client area of a window.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the color of the background in the client area of a window.</returns>
		// Token: 0x170002F9 RID: 761
		// (get) Token: 0x06000761 RID: 1889 RVA: 0x000065D4 File Offset: 0x000047D4
		public static Color Window
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the color of a window frame.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the color of a window frame.</returns>
		// Token: 0x170002FA RID: 762
		// (get) Token: 0x06000762 RID: 1890 RVA: 0x000065F0 File Offset: 0x000047F0
		public static Color WindowFrame
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Color" /> structure that is the color of the text in the client area of a window.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> that is the color of the text in the client area of a window.</returns>
		// Token: 0x170002FB RID: 763
		// (get) Token: 0x06000763 RID: 1891 RVA: 0x0000660C File Offset: 0x0000480C
		public static Color WindowText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
		}
	}
}
