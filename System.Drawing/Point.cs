﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System.Drawing
{
	/// <summary>Represents an ordered pair of integer x- and y-coordinates that defines a point in a two-dimensional plane.</summary>
	// Token: 0x02000007 RID: 7
	[ComVisible(true)]
	[Serializable]
	public struct Point
	{
		/// <summary>Converts the specified <see cref="T:System.Drawing.PointF" /> to a <see cref="T:System.Drawing.Point" /> by rounding the values of the <see cref="T:System.Drawing.PointF" /> to the next higher integer values.</summary>
		/// <param name="value">The <see cref="T:System.Drawing.PointF" /> to convert. </param>
		/// <returns>The <see cref="T:System.Drawing.Point" /> this method converts to.</returns>
		// Token: 0x060000B2 RID: 178 RVA: 0x00003338 File Offset: 0x00001538
		public static Point Ceiling(PointF value)
		{
			checked
			{
				int num = (int)Math.Ceiling((double)value.X);
				int num2 = (int)Math.Ceiling((double)value.Y);
				return new Point(num, num2);
			}
		}

		/// <summary>Converts the specified <see cref="T:System.Drawing.PointF" /> to a <see cref="T:System.Drawing.Point" /> object by rounding the <see cref="T:System.Drawing.Point" /> values to the nearest integer.</summary>
		/// <param name="value">The <see cref="T:System.Drawing.PointF" /> to convert. </param>
		/// <returns>The <see cref="T:System.Drawing.Point" /> this method converts to.</returns>
		// Token: 0x060000B3 RID: 179 RVA: 0x00003368 File Offset: 0x00001568
		public static Point Round(PointF value)
		{
			checked
			{
				int num = (int)Math.Round((double)value.X);
				int num2 = (int)Math.Round((double)value.Y);
				return new Point(num, num2);
			}
		}

		/// <summary>Converts the specified <see cref="T:System.Drawing.PointF" /> to a <see cref="T:System.Drawing.Point" /> by truncating the values of the <see cref="T:System.Drawing.Point" />.</summary>
		/// <param name="value">The <see cref="T:System.Drawing.PointF" /> to convert. </param>
		/// <returns>The <see cref="T:System.Drawing.Point" /> this method converts to.</returns>
		// Token: 0x060000B4 RID: 180 RVA: 0x00003398 File Offset: 0x00001598
		public static Point Truncate(PointF value)
		{
			checked
			{
				int num = (int)value.X;
				int num2 = (int)value.Y;
				return new Point(num, num2);
			}
		}

		/// <summary>Translates a <see cref="T:System.Drawing.Point" /> by a given <see cref="T:System.Drawing.Size" />.</summary>
		/// <param name="pt">The <see cref="T:System.Drawing.Point" /> to translate. </param>
		/// <param name="sz">A <see cref="T:System.Drawing.Size" /> that specifies the pair of numbers to add to the coordinates of <paramref name="pt" />. </param>
		/// <returns>The translated <see cref="T:System.Drawing.Point" />.</returns>
		// Token: 0x060000B5 RID: 181 RVA: 0x000033BC File Offset: 0x000015BC
		public static Point operator +(Point pt, Size sz)
		{
			return new Point(pt.X + sz.Width, pt.Y + sz.Height);
		}

		/// <summary>Compares two <see cref="T:System.Drawing.Point" /> objects. The result specifies whether the values of the <see cref="P:System.Drawing.Point.X" /> and <see cref="P:System.Drawing.Point.Y" /> properties of the two <see cref="T:System.Drawing.Point" /> objects are equal.</summary>
		/// <param name="left">A <see cref="T:System.Drawing.Point" /> to compare. </param>
		/// <param name="right">A <see cref="T:System.Drawing.Point" /> to compare. </param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="P:System.Drawing.Point.X" /> and <see cref="P:System.Drawing.Point.Y" /> values of <paramref name="left" /> and <paramref name="right" /> are equal; otherwise, <see langword="false" />.</returns>
		// Token: 0x060000B6 RID: 182 RVA: 0x000033E1 File Offset: 0x000015E1
		public static bool operator ==(Point left, Point right)
		{
			return left.X == right.X && left.Y == right.Y;
		}

		/// <summary>Compares two <see cref="T:System.Drawing.Point" /> objects. The result specifies whether the values of the <see cref="P:System.Drawing.Point.X" /> or <see cref="P:System.Drawing.Point.Y" /> properties of the two <see cref="T:System.Drawing.Point" /> objects are unequal.</summary>
		/// <param name="left">A <see cref="T:System.Drawing.Point" /> to compare. </param>
		/// <param name="right">A <see cref="T:System.Drawing.Point" /> to compare. </param>
		/// <returns>
		///     <see langword="true" /> if the values of either the <see cref="P:System.Drawing.Point.X" /> properties or the <see cref="P:System.Drawing.Point.Y" /> properties of <paramref name="left" /> and <paramref name="right" /> differ; otherwise, <see langword="false" />.</returns>
		// Token: 0x060000B7 RID: 183 RVA: 0x00003405 File Offset: 0x00001605
		public static bool operator !=(Point left, Point right)
		{
			return left.X != right.X || left.Y != right.Y;
		}

		/// <summary>Translates a <see cref="T:System.Drawing.Point" /> by the negative of a given <see cref="T:System.Drawing.Size" />.</summary>
		/// <param name="pt">The <see cref="T:System.Drawing.Point" /> to translate. </param>
		/// <param name="sz">A <see cref="T:System.Drawing.Size" /> that specifies the pair of numbers to subtract from the coordinates of <paramref name="pt" />. </param>
		/// <returns>A <see cref="T:System.Drawing.Point" /> structure that is translated by the negative of a given <see cref="T:System.Drawing.Size" /> structure.</returns>
		// Token: 0x060000B8 RID: 184 RVA: 0x0000342C File Offset: 0x0000162C
		public static Point operator -(Point pt, Size sz)
		{
			return new Point(pt.X - sz.Width, pt.Y - sz.Height);
		}

		/// <summary>Converts the specified <see cref="T:System.Drawing.Point" /> structure to a <see cref="T:System.Drawing.Size" /> structure.</summary>
		/// <param name="p">The <see cref="T:System.Drawing.Point" /> to be converted.</param>
		/// <returns>The <see cref="T:System.Drawing.Size" /> that results from the conversion.</returns>
		// Token: 0x060000B9 RID: 185 RVA: 0x00003451 File Offset: 0x00001651
		public static explicit operator Size(Point p)
		{
			return new Size(p.X, p.Y);
		}

		/// <summary>Converts the specified <see cref="T:System.Drawing.Point" /> structure to a <see cref="T:System.Drawing.PointF" /> structure.</summary>
		/// <param name="p">The <see cref="T:System.Drawing.Point" /> to be converted.</param>
		/// <returns>The <see cref="T:System.Drawing.PointF" /> that results from the conversion.</returns>
		// Token: 0x060000BA RID: 186 RVA: 0x00003466 File Offset: 0x00001666
		public static implicit operator PointF(Point p)
		{
			return new PointF((float)p.X, (float)p.Y);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Point" /> class using coordinates specified by an integer value.</summary>
		/// <param name="dw">A 32-bit integer that specifies the coordinates for the new <see cref="T:System.Drawing.Point" />. </param>
		// Token: 0x060000BB RID: 187 RVA: 0x0000347D File Offset: 0x0000167D
		public Point(int dw)
		{
			this.y = dw >> 16;
			this.x = (int)((short)(dw & 65535));
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Point" /> class from a <see cref="T:System.Drawing.Size" />.</summary>
		/// <param name="sz">A <see cref="T:System.Drawing.Size" /> that specifies the coordinates for the new <see cref="T:System.Drawing.Point" />. </param>
		// Token: 0x060000BC RID: 188 RVA: 0x00003497 File Offset: 0x00001697
		public Point(Size sz)
		{
			this.x = sz.Width;
			this.y = sz.Height;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Point" /> class with the specified coordinates.</summary>
		/// <param name="x">The horizontal position of the point. </param>
		/// <param name="y">The vertical position of the point. </param>
		// Token: 0x060000BD RID: 189 RVA: 0x000034B3 File Offset: 0x000016B3
		public Point(int x, int y)
		{
			this.x = x;
			this.y = y;
		}

		/// <summary>Gets a value indicating whether this <see cref="T:System.Drawing.Point" /> is empty.</summary>
		/// <returns>
		///     <see langword="true" /> if both <see cref="P:System.Drawing.Point.X" /> and <see cref="P:System.Drawing.Point.Y" /> are 0; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000098 RID: 152
		// (get) Token: 0x060000BE RID: 190 RVA: 0x000034C3 File Offset: 0x000016C3
		[Browsable(false)]
		public bool IsEmpty
		{
			get
			{
				return this.x == 0 && this.y == 0;
			}
		}

		/// <summary>Gets or sets the x-coordinate of this <see cref="T:System.Drawing.Point" />.</summary>
		/// <returns>The x-coordinate of this <see cref="T:System.Drawing.Point" />.</returns>
		// Token: 0x17000099 RID: 153
		// (get) Token: 0x060000BF RID: 191 RVA: 0x000034D8 File Offset: 0x000016D8
		// (set) Token: 0x060000C0 RID: 192 RVA: 0x000034E0 File Offset: 0x000016E0
		public int X
		{
			get
			{
				return this.x;
			}
			set
			{
				this.x = value;
			}
		}

		/// <summary>Gets or sets the y-coordinate of this <see cref="T:System.Drawing.Point" />.</summary>
		/// <returns>The y-coordinate of this <see cref="T:System.Drawing.Point" />.</returns>
		// Token: 0x1700009A RID: 154
		// (get) Token: 0x060000C1 RID: 193 RVA: 0x000034E9 File Offset: 0x000016E9
		// (set) Token: 0x060000C2 RID: 194 RVA: 0x000034F1 File Offset: 0x000016F1
		public int Y
		{
			get
			{
				return this.y;
			}
			set
			{
				this.y = value;
			}
		}

		/// <summary>Specifies whether this <see cref="T:System.Drawing.Point" /> contains the same coordinates as the specified <see cref="T:System.Object" />.</summary>
		/// <param name="obj">The <see cref="T:System.Object" /> to test. </param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="obj" /> is a <see cref="T:System.Drawing.Point" /> and has the same coordinates as this <see cref="T:System.Drawing.Point" />.</returns>
		// Token: 0x060000C3 RID: 195 RVA: 0x000034FA File Offset: 0x000016FA
		public override bool Equals(object obj)
		{
			return obj is Point && this == (Point)obj;
		}

		/// <summary>Returns a hash code for this <see cref="T:System.Drawing.Point" />.</summary>
		/// <returns>An integer value that specifies a hash value for this <see cref="T:System.Drawing.Point" />.</returns>
		// Token: 0x060000C4 RID: 196 RVA: 0x00003517 File Offset: 0x00001717
		public override int GetHashCode()
		{
			return this.x ^ this.y;
		}

		/// <summary>Translates this <see cref="T:System.Drawing.Point" /> by the specified amount.</summary>
		/// <param name="dx">The amount to offset the x-coordinate. </param>
		/// <param name="dy">The amount to offset the y-coordinate. </param>
		// Token: 0x060000C5 RID: 197 RVA: 0x00003526 File Offset: 0x00001726
		public void Offset(int dx, int dy)
		{
			this.x += dx;
			this.y += dy;
		}

		/// <summary>Converts this <see cref="T:System.Drawing.Point" /> to a human-readable string.</summary>
		/// <returns>A string that represents this <see cref="T:System.Drawing.Point" />.</returns>
		// Token: 0x060000C6 RID: 198 RVA: 0x00003544 File Offset: 0x00001744
		public override string ToString()
		{
			return string.Format("{{X={0},Y={1}}}", this.x.ToString(CultureInfo.InvariantCulture), this.y.ToString(CultureInfo.InvariantCulture));
		}

		/// <summary>Adds the specified <see cref="T:System.Drawing.Size" /> to the specified <see cref="T:System.Drawing.Point" />.</summary>
		/// <param name="pt">The <see cref="T:System.Drawing.Point" /> to add.</param>
		/// <param name="sz">The <see cref="T:System.Drawing.Size" /> to add</param>
		/// <returns>The <see cref="T:System.Drawing.Point" /> that is the result of the addition operation.</returns>
		// Token: 0x060000C7 RID: 199 RVA: 0x000033BC File Offset: 0x000015BC
		public static Point Add(Point pt, Size sz)
		{
			return new Point(pt.X + sz.Width, pt.Y + sz.Height);
		}

		/// <summary>Translates this <see cref="T:System.Drawing.Point" /> by the specified <see cref="T:System.Drawing.Point" />.</summary>
		/// <param name="p">The <see cref="T:System.Drawing.Point" /> used offset this <see cref="T:System.Drawing.Point" />.</param>
		// Token: 0x060000C8 RID: 200 RVA: 0x00003570 File Offset: 0x00001770
		public void Offset(Point p)
		{
			this.Offset(p.X, p.Y);
		}

		/// <summary>Returns the result of subtracting specified <see cref="T:System.Drawing.Size" /> from the specified <see cref="T:System.Drawing.Point" />.</summary>
		/// <param name="pt">The <see cref="T:System.Drawing.Point" /> to be subtracted from. </param>
		/// <param name="sz">The <see cref="T:System.Drawing.Size" /> to subtract from the <see cref="T:System.Drawing.Point" />.</param>
		/// <returns>The <see cref="T:System.Drawing.Point" /> that is the result of the subtraction operation.</returns>
		// Token: 0x060000C9 RID: 201 RVA: 0x0000342C File Offset: 0x0000162C
		public static Point Subtract(Point pt, Size sz)
		{
			return new Point(pt.X - sz.Width, pt.Y - sz.Height);
		}

		// Token: 0x040000E5 RID: 229
		private int x;

		// Token: 0x040000E6 RID: 230
		private int y;

		/// <summary>Represents a <see cref="T:System.Drawing.Point" /> that has <see cref="P:System.Drawing.Point.X" /> and <see cref="P:System.Drawing.Point.Y" /> values set to zero. </summary>
		// Token: 0x040000E7 RID: 231
		public static readonly Point Empty;
	}
}
