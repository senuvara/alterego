﻿using System;
using Unity;

namespace System.Drawing
{
	/// <summary>Provides access to the main buffered graphics context object for the application domain.</summary>
	// Token: 0x02000079 RID: 121
	public sealed class BufferedGraphicsManager
	{
		// Token: 0x06000678 RID: 1656 RVA: 0x00004644 File Offset: 0x00002844
		internal BufferedGraphicsManager()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the <see cref="T:System.Drawing.BufferedGraphicsContext" /> for the current application domain.</summary>
		/// <returns>The <see cref="T:System.Drawing.BufferedGraphicsContext" /> for the current application domain.</returns>
		// Token: 0x1700022B RID: 555
		// (get) Token: 0x06000679 RID: 1657 RVA: 0x00004667 File Offset: 0x00002867
		public static BufferedGraphicsContext Current
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
