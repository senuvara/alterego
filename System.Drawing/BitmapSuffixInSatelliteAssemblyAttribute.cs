﻿using System;

namespace System.Drawing
{
	/// <summary>Specifies that, when interpreting <see cref="T:System.Drawing.ToolboxBitmapAttribute" /> declarations, the assembly should look for the indicated resources in a satellite assembly, but with the <see cref="P:System.Drawing.Configuration.SystemDrawingSection.BitmapSuffix" /> configuration value appended to the declared file name.</summary>
	// Token: 0x0200000F RID: 15
	[AttributeUsage(AttributeTargets.Assembly)]
	public class BitmapSuffixInSatelliteAssemblyAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.BitmapSuffixInSatelliteAssemblyAttribute" /> class.</summary>
		// Token: 0x06000156 RID: 342 RVA: 0x00004642 File Offset: 0x00002842
		public BitmapSuffixInSatelliteAssemblyAttribute()
		{
		}
	}
}
