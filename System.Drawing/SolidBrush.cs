﻿using System;
using Unity;

namespace System.Drawing
{
	/// <summary>Defines a brush of a single color. Brushes are used to fill graphics shapes, such as rectangles, ellipses, pies, polygons, and paths. This class cannot be inherited.</summary>
	// Token: 0x02000082 RID: 130
	public sealed class SolidBrush : Brush
	{
		/// <summary>Initializes a new <see cref="T:System.Drawing.SolidBrush" /> object of the specified color.</summary>
		/// <param name="color">A <see cref="T:System.Drawing.Color" /> structure that represents the color of this brush. </param>
		// Token: 0x0600071A RID: 1818 RVA: 0x00004644 File Offset: 0x00002844
		public SolidBrush(Color color)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the color of this <see cref="T:System.Drawing.SolidBrush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> structure that represents the color of this brush.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Drawing.SolidBrush.Color" /> property is set on an immutable <see cref="T:System.Drawing.SolidBrush" />.</exception>
		// Token: 0x170002B9 RID: 697
		// (get) Token: 0x0600071B RID: 1819 RVA: 0x00006270 File Offset: 0x00004470
		// (set) Token: 0x0600071C RID: 1820 RVA: 0x00004644 File Offset: 0x00002844
		public Color Color
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Creates an exact copy of this <see cref="T:System.Drawing.SolidBrush" /> object.</summary>
		/// <returns>The <see cref="T:System.Drawing.SolidBrush" /> object that this method creates.</returns>
		// Token: 0x0600071D RID: 1821 RVA: 0x00004667 File Offset: 0x00002867
		public override object Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x0600071E RID: 1822 RVA: 0x00004644 File Offset: 0x00002844
		protected override void Dispose(bool disposing)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
