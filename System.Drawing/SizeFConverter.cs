﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Drawing
{
	/// <summary>Converts <see cref="T:System.Drawing.SizeF" /> objects from one type to another.</summary>
	// Token: 0x02000081 RID: 129
	public class SizeFConverter : TypeConverter
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.SizeFConverter" /> class.</summary>
		// Token: 0x06000719 RID: 1817 RVA: 0x00004644 File Offset: 0x00002844
		public SizeFConverter()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
