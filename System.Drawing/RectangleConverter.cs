﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Drawing
{
	/// <summary>Converts rectangles from one data type to another. Access this class through the <see cref="T:System.ComponentModel.TypeDescriptor" />.</summary>
	// Token: 0x0200007F RID: 127
	public class RectangleConverter : TypeConverter
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.RectangleConverter" /> class.</summary>
		// Token: 0x06000717 RID: 1815 RVA: 0x00004644 File Offset: 0x00002844
		public RectangleConverter()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
