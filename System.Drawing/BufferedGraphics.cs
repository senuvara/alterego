﻿using System;
using Unity;

namespace System.Drawing
{
	/// <summary>Provides a graphics buffer for double buffering.</summary>
	// Token: 0x02000077 RID: 119
	public sealed class BufferedGraphics : IDisposable
	{
		// Token: 0x0600066B RID: 1643 RVA: 0x00004644 File Offset: 0x00002844
		internal BufferedGraphics()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Graphics" /> object that outputs to the graphics buffer.</summary>
		/// <returns>A <see cref="T:System.Drawing.Graphics" /> object that outputs to the graphics buffer.</returns>
		// Token: 0x17000229 RID: 553
		// (get) Token: 0x0600066C RID: 1644 RVA: 0x00004667 File Offset: 0x00002867
		public Graphics Graphics
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Releases all resources used by the <see cref="T:System.Drawing.BufferedGraphics" /> object.</summary>
		// Token: 0x0600066D RID: 1645 RVA: 0x00004644 File Offset: 0x00002844
		public void Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Writes the contents of the graphics buffer to the default device.</summary>
		// Token: 0x0600066E RID: 1646 RVA: 0x00004644 File Offset: 0x00002844
		public void Render()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Writes the contents of the graphics buffer to the specified <see cref="T:System.Drawing.Graphics" /> object.</summary>
		/// <param name="target">A <see cref="T:System.Drawing.Graphics" /> object to which to write the contents of the graphics buffer. </param>
		// Token: 0x0600066F RID: 1647 RVA: 0x00004644 File Offset: 0x00002844
		public void Render(Graphics target)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Writes the contents of the graphics buffer to the device context associated with the specified <see cref="T:System.IntPtr" /> handle.</summary>
		/// <param name="targetDC">An <see cref="T:System.IntPtr" /> that points to the device context to which to write the contents of the graphics buffer. </param>
		// Token: 0x06000670 RID: 1648 RVA: 0x00004644 File Offset: 0x00002844
		public void Render(IntPtr targetDC)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
