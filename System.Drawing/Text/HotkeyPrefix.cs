﻿using System;

namespace System.Drawing.Text
{
	/// <summary>Specifies the type of display for hot-key prefixes that relate to text.</summary>
	// Token: 0x02000024 RID: 36
	public enum HotkeyPrefix
	{
		/// <summary>Do not display the hot-key prefix.</summary>
		// Token: 0x0400021F RID: 543
		Hide = 2,
		/// <summary>No hot-key prefix.</summary>
		// Token: 0x04000220 RID: 544
		None = 0,
		/// <summary>Display the hot-key prefix.</summary>
		// Token: 0x04000221 RID: 545
		Show
	}
}
