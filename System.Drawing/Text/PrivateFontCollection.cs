﻿using System;
using Unity;

namespace System.Drawing.Text
{
	/// <summary>Provides a collection of font families built from font files that are provided by the client application.</summary>
	// Token: 0x0200008B RID: 139
	public sealed class PrivateFontCollection : FontCollection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Text.PrivateFontCollection" /> class. </summary>
		// Token: 0x060007B4 RID: 1972 RVA: 0x00004644 File Offset: 0x00002844
		public PrivateFontCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a font from the specified file to this <see cref="T:System.Drawing.Text.PrivateFontCollection" />. </summary>
		/// <param name="filename">A <see cref="T:System.String" /> that contains the file name of the font to add. </param>
		/// <exception cref="T:System.IO.FileNotFoundException">The specified font is not supported or the font file cannot be found.</exception>
		// Token: 0x060007B5 RID: 1973 RVA: 0x00004644 File Offset: 0x00002844
		public void AddFontFile(string filename)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a font contained in system memory to this <see cref="T:System.Drawing.Text.PrivateFontCollection" />.</summary>
		/// <param name="memory">The memory address of the font to add. </param>
		/// <param name="length">The memory length of the font to add. </param>
		// Token: 0x060007B6 RID: 1974 RVA: 0x00004644 File Offset: 0x00002844
		public void AddMemoryFont(IntPtr memory, int length)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060007B7 RID: 1975 RVA: 0x00004644 File Offset: 0x00002844
		protected override void Dispose(bool disposing)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
