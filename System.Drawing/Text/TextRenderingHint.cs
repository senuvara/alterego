﻿using System;

namespace System.Drawing.Text
{
	/// <summary>Specifies the quality of text rendering.</summary>
	// Token: 0x0200003A RID: 58
	public enum TextRenderingHint
	{
		/// <summary>Each character is drawn using its antialiased glyph bitmap without hinting. Better quality due to antialiasing. Stem width differences may be noticeable because hinting is turned off.</summary>
		// Token: 0x04000283 RID: 643
		AntiAlias = 4,
		/// <summary>Each character is drawn using its antialiased glyph bitmap with hinting. Much better quality due to antialiasing, but at a higher performance cost.</summary>
		// Token: 0x04000284 RID: 644
		AntiAliasGridFit = 3,
		/// <summary>Each character is drawn using its glyph ClearType bitmap with hinting. The highest quality setting. Used to take advantage of ClearType font features.</summary>
		// Token: 0x04000285 RID: 645
		ClearTypeGridFit = 5,
		/// <summary>Each character is drawn using its glyph bitmap. Hinting is not used.</summary>
		// Token: 0x04000286 RID: 646
		SingleBitPerPixel = 2,
		/// <summary>Each character is drawn using its glyph bitmap. Hinting is used to improve character appearance on stems and curvature.</summary>
		// Token: 0x04000287 RID: 647
		SingleBitPerPixelGridFit = 1,
		/// <summary>Each character is drawn using its glyph bitmap, with the system default rendering hint. The text will be drawn using whatever font-smoothing settings the user has selected for the system.</summary>
		// Token: 0x04000288 RID: 648
		SystemDefault = 0
	}
}
