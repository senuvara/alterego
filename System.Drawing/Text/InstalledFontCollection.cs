﻿using System;
using Unity;

namespace System.Drawing.Text
{
	/// <summary>Represents the fonts installed on the system. This class cannot be inherited. </summary>
	// Token: 0x0200008A RID: 138
	public sealed class InstalledFontCollection : FontCollection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Text.InstalledFontCollection" /> class. </summary>
		// Token: 0x060007B3 RID: 1971 RVA: 0x00004644 File Offset: 0x00002844
		public InstalledFontCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
