﻿using System;
using Unity;

namespace System.Drawing.Text
{
	/// <summary>Provides a base class for installed and private font collections. </summary>
	// Token: 0x0200001E RID: 30
	public abstract class FontCollection : IDisposable
	{
		// Token: 0x06000319 RID: 793 RVA: 0x00004644 File Offset: 0x00002844
		internal FontCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the array of <see cref="T:System.Drawing.FontFamily" /> objects associated with this <see cref="T:System.Drawing.Text.FontCollection" />. </summary>
		/// <returns>An array of <see cref="T:System.Drawing.FontFamily" /> objects.</returns>
		// Token: 0x170000DD RID: 221
		// (get) Token: 0x0600031A RID: 794 RVA: 0x00004667 File Offset: 0x00002867
		public FontFamily[] Families
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Releases all resources used by this <see cref="T:System.Drawing.Text.FontCollection" />.</summary>
		// Token: 0x0600031B RID: 795 RVA: 0x00004644 File Offset: 0x00002844
		public void Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Drawing.Text.FontCollection" /> and optionally releases the managed resources. </summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x0600031C RID: 796 RVA: 0x00004644 File Offset: 0x00002844
		protected virtual void Dispose(bool disposing)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
