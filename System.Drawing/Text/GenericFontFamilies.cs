﻿using System;

namespace System.Drawing.Text
{
	/// <summary>Specifies a generic <see cref="T:System.Drawing.FontFamily" /> object.</summary>
	// Token: 0x0200001D RID: 29
	public enum GenericFontFamilies
	{
		/// <summary>A generic Monospace <see cref="T:System.Drawing.FontFamily" /> object.</summary>
		// Token: 0x04000202 RID: 514
		Monospace = 2,
		/// <summary>A generic Sans Serif <see cref="T:System.Drawing.FontFamily" /> object.</summary>
		// Token: 0x04000203 RID: 515
		SansSerif = 1,
		/// <summary>A generic Serif <see cref="T:System.Drawing.FontFamily" /> object.</summary>
		// Token: 0x04000204 RID: 516
		Serif = 0
	}
}
