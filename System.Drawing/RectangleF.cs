﻿using System;
using System.ComponentModel;

namespace System.Drawing
{
	/// <summary>Stores a set of four floating-point numbers that represent the location and size of a rectangle. For more advanced region functions, use a <see cref="T:System.Drawing.Region" /> object.</summary>
	// Token: 0x0200000A RID: 10
	[Serializable]
	public struct RectangleF
	{
		/// <summary>Creates a <see cref="T:System.Drawing.RectangleF" /> structure with upper-left corner and lower-right corner at the specified locations.</summary>
		/// <param name="left">The x-coordinate of the upper-left corner of the rectangular region. </param>
		/// <param name="top">The y-coordinate of the upper-left corner of the rectangular region. </param>
		/// <param name="right">The x-coordinate of the lower-right corner of the rectangular region. </param>
		/// <param name="bottom">The y-coordinate of the lower-right corner of the rectangular region. </param>
		/// <returns>The new <see cref="T:System.Drawing.RectangleF" /> that this method creates.</returns>
		// Token: 0x06000106 RID: 262 RVA: 0x00003CDD File Offset: 0x00001EDD
		public static RectangleF FromLTRB(float left, float top, float right, float bottom)
		{
			return new RectangleF(left, top, right - left, bottom - top);
		}

		/// <summary>Creates and returns an enlarged copy of the specified <see cref="T:System.Drawing.RectangleF" /> structure. The copy is enlarged by the specified amount and the original rectangle remains unmodified.</summary>
		/// <param name="rect">The <see cref="T:System.Drawing.RectangleF" /> to be copied. This rectangle is not modified. </param>
		/// <param name="x">The amount to enlarge the copy of the rectangle horizontally. </param>
		/// <param name="y">The amount to enlarge the copy of the rectangle vertically. </param>
		/// <returns>The enlarged <see cref="T:System.Drawing.RectangleF" />.</returns>
		// Token: 0x06000107 RID: 263 RVA: 0x00003CEC File Offset: 0x00001EEC
		public static RectangleF Inflate(RectangleF rect, float x, float y)
		{
			RectangleF result = new RectangleF(rect.X, rect.Y, rect.Width, rect.Height);
			result.Inflate(x, y);
			return result;
		}

		/// <summary>Enlarges this <see cref="T:System.Drawing.RectangleF" /> structure by the specified amount.</summary>
		/// <param name="x">The amount to inflate this <see cref="T:System.Drawing.RectangleF" /> structure horizontally. </param>
		/// <param name="y">The amount to inflate this <see cref="T:System.Drawing.RectangleF" /> structure vertically. </param>
		// Token: 0x06000108 RID: 264 RVA: 0x00003D26 File Offset: 0x00001F26
		public void Inflate(float x, float y)
		{
			this.Inflate(new SizeF(x, y));
		}

		/// <summary>Enlarges this <see cref="T:System.Drawing.RectangleF" /> by the specified amount.</summary>
		/// <param name="size">The amount to inflate this rectangle. </param>
		// Token: 0x06000109 RID: 265 RVA: 0x00003D38 File Offset: 0x00001F38
		public void Inflate(SizeF size)
		{
			this.x -= size.Width;
			this.y -= size.Height;
			this.width += size.Width * 2f;
			this.height += size.Height * 2f;
		}

		/// <summary>Returns a <see cref="T:System.Drawing.RectangleF" /> structure that represents the intersection of two rectangles. If there is no intersection, and empty <see cref="T:System.Drawing.RectangleF" /> is returned.</summary>
		/// <param name="a">A rectangle to intersect. </param>
		/// <param name="b">A rectangle to intersect. </param>
		/// <returns>A third <see cref="T:System.Drawing.RectangleF" /> structure the size of which represents the overlapped area of the two specified rectangles.</returns>
		// Token: 0x0600010A RID: 266 RVA: 0x00003DA4 File Offset: 0x00001FA4
		public static RectangleF Intersect(RectangleF a, RectangleF b)
		{
			if (!a.IntersectsWithInclusive(b))
			{
				return RectangleF.Empty;
			}
			return RectangleF.FromLTRB(Math.Max(a.Left, b.Left), Math.Max(a.Top, b.Top), Math.Min(a.Right, b.Right), Math.Min(a.Bottom, b.Bottom));
		}

		/// <summary>Replaces this <see cref="T:System.Drawing.RectangleF" /> structure with the intersection of itself and the specified <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <param name="rect">The rectangle to intersect. </param>
		// Token: 0x0600010B RID: 267 RVA: 0x00003E12 File Offset: 0x00002012
		public void Intersect(RectangleF rect)
		{
			this = RectangleF.Intersect(this, rect);
		}

		/// <summary>Creates the smallest possible third rectangle that can contain both of two rectangles that form a union.</summary>
		/// <param name="a">A rectangle to union. </param>
		/// <param name="b">A rectangle to union. </param>
		/// <returns>A third <see cref="T:System.Drawing.RectangleF" /> structure that contains both of the two rectangles that form the union.</returns>
		// Token: 0x0600010C RID: 268 RVA: 0x00003E28 File Offset: 0x00002028
		public static RectangleF Union(RectangleF a, RectangleF b)
		{
			return RectangleF.FromLTRB(Math.Min(a.Left, b.Left), Math.Min(a.Top, b.Top), Math.Max(a.Right, b.Right), Math.Max(a.Bottom, b.Bottom));
		}

		/// <summary>Tests whether two <see cref="T:System.Drawing.RectangleF" /> structures have equal location and size.</summary>
		/// <param name="left">The <see cref="T:System.Drawing.RectangleF" /> structure that is to the left of the equality operator. </param>
		/// <param name="right">The <see cref="T:System.Drawing.RectangleF" /> structure that is to the right of the equality operator. </param>
		/// <returns>This operator returns <see langword="true" /> if the two specified <see cref="T:System.Drawing.RectangleF" /> structures have equal <see cref="P:System.Drawing.RectangleF.X" />, <see cref="P:System.Drawing.RectangleF.Y" />, <see cref="P:System.Drawing.RectangleF.Width" />, and <see cref="P:System.Drawing.RectangleF.Height" /> properties.</returns>
		// Token: 0x0600010D RID: 269 RVA: 0x00003E88 File Offset: 0x00002088
		public static bool operator ==(RectangleF left, RectangleF right)
		{
			return left.X == right.X && left.Y == right.Y && left.Width == right.Width && left.Height == right.Height;
		}

		/// <summary>Tests whether two <see cref="T:System.Drawing.RectangleF" /> structures differ in location or size.</summary>
		/// <param name="left">The <see cref="T:System.Drawing.RectangleF" /> structure that is to the left of the inequality operator. </param>
		/// <param name="right">The <see cref="T:System.Drawing.RectangleF" /> structure that is to the right of the inequality operator. </param>
		/// <returns>This operator returns <see langword="true" /> if any of the <see cref="P:System.Drawing.RectangleF.X" /> , <see cref="P:System.Drawing.RectangleF.Y" />, <see cref="P:System.Drawing.RectangleF.Width" />, or <see cref="P:System.Drawing.RectangleF.Height" /> properties of the two <see cref="T:System.Drawing.Rectangle" /> structures are unequal; otherwise <see langword="false" />.</returns>
		// Token: 0x0600010E RID: 270 RVA: 0x00003ED8 File Offset: 0x000020D8
		public static bool operator !=(RectangleF left, RectangleF right)
		{
			return left.X != right.X || left.Y != right.Y || left.Width != right.Width || left.Height != right.Height;
		}

		/// <summary>Converts the specified <see cref="T:System.Drawing.Rectangle" /> structure to a <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <param name="r">The <see cref="T:System.Drawing.Rectangle" /> structure to convert. </param>
		/// <returns>The <see cref="T:System.Drawing.RectangleF" /> structure that is converted from the specified <see cref="T:System.Drawing.Rectangle" /> structure.</returns>
		// Token: 0x0600010F RID: 271 RVA: 0x00003F2A File Offset: 0x0000212A
		public static implicit operator RectangleF(Rectangle r)
		{
			return new RectangleF((float)r.X, (float)r.Y, (float)r.Width, (float)r.Height);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.RectangleF" /> class with the specified location and size.</summary>
		/// <param name="location">A <see cref="T:System.Drawing.PointF" /> that represents the upper-left corner of the rectangular region. </param>
		/// <param name="size">A <see cref="T:System.Drawing.SizeF" /> that represents the width and height of the rectangular region. </param>
		// Token: 0x06000110 RID: 272 RVA: 0x00003F51 File Offset: 0x00002151
		public RectangleF(PointF location, SizeF size)
		{
			this.x = location.X;
			this.y = location.Y;
			this.width = size.Width;
			this.height = size.Height;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.RectangleF" /> class with the specified location and size.</summary>
		/// <param name="x">The x-coordinate of the upper-left corner of the rectangle. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the rectangle. </param>
		/// <param name="width">The width of the rectangle. </param>
		/// <param name="height">The height of the rectangle. </param>
		// Token: 0x06000111 RID: 273 RVA: 0x00003F87 File Offset: 0x00002187
		public RectangleF(float x, float y, float width, float height)
		{
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
		}

		/// <summary>Gets the y-coordinate that is the sum of <see cref="P:System.Drawing.RectangleF.Y" /> and <see cref="P:System.Drawing.RectangleF.Height" /> of this <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <returns>The y-coordinate that is the sum of <see cref="P:System.Drawing.RectangleF.Y" /> and <see cref="P:System.Drawing.RectangleF.Height" /> of this <see cref="T:System.Drawing.RectangleF" /> structure.</returns>
		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x06000112 RID: 274 RVA: 0x00003FA6 File Offset: 0x000021A6
		[Browsable(false)]
		public float Bottom
		{
			get
			{
				return this.Y + this.Height;
			}
		}

		/// <summary>Gets or sets the height of this <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <returns>The height of this <see cref="T:System.Drawing.RectangleF" /> structure. The default is 0.</returns>
		// Token: 0x170000AA RID: 170
		// (get) Token: 0x06000113 RID: 275 RVA: 0x00003FB5 File Offset: 0x000021B5
		// (set) Token: 0x06000114 RID: 276 RVA: 0x00003FBD File Offset: 0x000021BD
		public float Height
		{
			get
			{
				return this.height;
			}
			set
			{
				this.height = value;
			}
		}

		/// <summary>Tests whether the <see cref="P:System.Drawing.RectangleF.Width" /> or <see cref="P:System.Drawing.RectangleF.Height" /> property of this <see cref="T:System.Drawing.RectangleF" /> has a value of zero.</summary>
		/// <returns>This property returns <see langword="true" /> if the <see cref="P:System.Drawing.RectangleF.Width" /> or <see cref="P:System.Drawing.RectangleF.Height" /> property of this <see cref="T:System.Drawing.RectangleF" /> has a value of zero; otherwise, <see langword="false" />.</returns>
		// Token: 0x170000AB RID: 171
		// (get) Token: 0x06000115 RID: 277 RVA: 0x00003FC6 File Offset: 0x000021C6
		[Browsable(false)]
		public bool IsEmpty
		{
			get
			{
				return this.width <= 0f || this.height <= 0f;
			}
		}

		/// <summary>Gets the x-coordinate of the left edge of this <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <returns>The x-coordinate of the left edge of this <see cref="T:System.Drawing.RectangleF" /> structure. </returns>
		// Token: 0x170000AC RID: 172
		// (get) Token: 0x06000116 RID: 278 RVA: 0x00003FE7 File Offset: 0x000021E7
		[Browsable(false)]
		public float Left
		{
			get
			{
				return this.X;
			}
		}

		/// <summary>Gets or sets the coordinates of the upper-left corner of this <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <returns>A <see cref="T:System.Drawing.PointF" /> that represents the upper-left corner of this <see cref="T:System.Drawing.RectangleF" /> structure.</returns>
		// Token: 0x170000AD RID: 173
		// (get) Token: 0x06000117 RID: 279 RVA: 0x00003FEF File Offset: 0x000021EF
		// (set) Token: 0x06000118 RID: 280 RVA: 0x00004002 File Offset: 0x00002202
		[Browsable(false)]
		public PointF Location
		{
			get
			{
				return new PointF(this.x, this.y);
			}
			set
			{
				this.x = value.X;
				this.y = value.Y;
			}
		}

		/// <summary>Gets the x-coordinate that is the sum of <see cref="P:System.Drawing.RectangleF.X" /> and <see cref="P:System.Drawing.RectangleF.Width" /> of this <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <returns>The x-coordinate that is the sum of <see cref="P:System.Drawing.RectangleF.X" /> and <see cref="P:System.Drawing.RectangleF.Width" /> of this <see cref="T:System.Drawing.RectangleF" /> structure. </returns>
		// Token: 0x170000AE RID: 174
		// (get) Token: 0x06000119 RID: 281 RVA: 0x0000401E File Offset: 0x0000221E
		[Browsable(false)]
		public float Right
		{
			get
			{
				return this.X + this.Width;
			}
		}

		/// <summary>Gets or sets the size of this <see cref="T:System.Drawing.RectangleF" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.SizeF" /> that represents the width and height of this <see cref="T:System.Drawing.RectangleF" /> structure.</returns>
		// Token: 0x170000AF RID: 175
		// (get) Token: 0x0600011A RID: 282 RVA: 0x0000402D File Offset: 0x0000222D
		// (set) Token: 0x0600011B RID: 283 RVA: 0x00004040 File Offset: 0x00002240
		[Browsable(false)]
		public SizeF Size
		{
			get
			{
				return new SizeF(this.width, this.height);
			}
			set
			{
				this.width = value.Width;
				this.height = value.Height;
			}
		}

		/// <summary>Gets the y-coordinate of the top edge of this <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <returns>The y-coordinate of the top edge of this <see cref="T:System.Drawing.RectangleF" /> structure.</returns>
		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x0600011C RID: 284 RVA: 0x0000405C File Offset: 0x0000225C
		[Browsable(false)]
		public float Top
		{
			get
			{
				return this.Y;
			}
		}

		/// <summary>Gets or sets the width of this <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <returns>The width of this <see cref="T:System.Drawing.RectangleF" /> structure. The default is 0.</returns>
		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x0600011D RID: 285 RVA: 0x00004064 File Offset: 0x00002264
		// (set) Token: 0x0600011E RID: 286 RVA: 0x0000406C File Offset: 0x0000226C
		public float Width
		{
			get
			{
				return this.width;
			}
			set
			{
				this.width = value;
			}
		}

		/// <summary>Gets or sets the x-coordinate of the upper-left corner of this <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <returns>The x-coordinate of the upper-left corner of this <see cref="T:System.Drawing.RectangleF" /> structure. The default is 0.</returns>
		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x0600011F RID: 287 RVA: 0x00004075 File Offset: 0x00002275
		// (set) Token: 0x06000120 RID: 288 RVA: 0x0000407D File Offset: 0x0000227D
		public float X
		{
			get
			{
				return this.x;
			}
			set
			{
				this.x = value;
			}
		}

		/// <summary>Gets or sets the y-coordinate of the upper-left corner of this <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <returns>The y-coordinate of the upper-left corner of this <see cref="T:System.Drawing.RectangleF" /> structure. The default is 0.</returns>
		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x06000121 RID: 289 RVA: 0x00004086 File Offset: 0x00002286
		// (set) Token: 0x06000122 RID: 290 RVA: 0x0000408E File Offset: 0x0000228E
		public float Y
		{
			get
			{
				return this.y;
			}
			set
			{
				this.y = value;
			}
		}

		/// <summary>Determines if the specified point is contained within this <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <param name="x">The x-coordinate of the point to test. </param>
		/// <param name="y">The y-coordinate of the point to test. </param>
		/// <returns>This method returns <see langword="true" /> if the point defined by <paramref name="x" /> and <paramref name="y" /> is contained within this <see cref="T:System.Drawing.RectangleF" /> structure; otherwise <see langword="false" />.</returns>
		// Token: 0x06000123 RID: 291 RVA: 0x00004097 File Offset: 0x00002297
		public bool Contains(float x, float y)
		{
			return x >= this.Left && x < this.Right && y >= this.Top && y < this.Bottom;
		}

		/// <summary>Determines if the specified point is contained within this <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <param name="pt">The <see cref="T:System.Drawing.PointF" /> to test. </param>
		/// <returns>This method returns <see langword="true" /> if the point represented by the <paramref name="pt" /> parameter is contained within this <see cref="T:System.Drawing.RectangleF" /> structure; otherwise <see langword="false" />.</returns>
		// Token: 0x06000124 RID: 292 RVA: 0x000040BF File Offset: 0x000022BF
		public bool Contains(PointF pt)
		{
			return this.Contains(pt.X, pt.Y);
		}

		/// <summary>Determines if the rectangular region represented by <paramref name="rect" /> is entirely contained within this <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <param name="rect">The <see cref="T:System.Drawing.RectangleF" /> to test. </param>
		/// <returns>This method returns <see langword="true" /> if the rectangular region represented by <paramref name="rect" /> is entirely contained within the rectangular region represented by this <see cref="T:System.Drawing.RectangleF" />; otherwise <see langword="false" />.</returns>
		// Token: 0x06000125 RID: 293 RVA: 0x000040D8 File Offset: 0x000022D8
		public bool Contains(RectangleF rect)
		{
			return this.X <= rect.X && this.Right >= rect.Right && this.Y <= rect.Y && this.Bottom >= rect.Bottom;
		}

		/// <summary>Tests whether <paramref name="obj" /> is a <see cref="T:System.Drawing.RectangleF" /> with the same location and size of this <see cref="T:System.Drawing.RectangleF" />.</summary>
		/// <param name="obj">The <see cref="T:System.Object" /> to test. </param>
		/// <returns>This method returns <see langword="true" /> if <paramref name="obj" /> is a <see cref="T:System.Drawing.RectangleF" /> and its <see langword="X" />, <see langword="Y" />, <see langword="Width" />, and <see langword="Height" /> properties are equal to the corresponding properties of this <see cref="T:System.Drawing.RectangleF" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000126 RID: 294 RVA: 0x00004126 File Offset: 0x00002326
		public override bool Equals(object obj)
		{
			return obj is RectangleF && this == (RectangleF)obj;
		}

		/// <summary>Gets the hash code for this <see cref="T:System.Drawing.RectangleF" /> structure. For information about the use of hash codes, see <see langword="Object.GetHashCode" />.</summary>
		/// <returns>The hash code for this <see cref="T:System.Drawing.RectangleF" />.</returns>
		// Token: 0x06000127 RID: 295 RVA: 0x00004143 File Offset: 0x00002343
		public override int GetHashCode()
		{
			return (int)(this.x + this.y + this.width + this.height);
		}

		/// <summary>Determines if this rectangle intersects with <paramref name="rect" />.</summary>
		/// <param name="rect">The rectangle to test. </param>
		/// <returns>This method returns <see langword="true" /> if there is any intersection.</returns>
		// Token: 0x06000128 RID: 296 RVA: 0x00004161 File Offset: 0x00002361
		public bool IntersectsWith(RectangleF rect)
		{
			return this.Left < rect.Right && this.Right > rect.Left && this.Top < rect.Bottom && this.Bottom > rect.Top;
		}

		// Token: 0x06000129 RID: 297 RVA: 0x000041A4 File Offset: 0x000023A4
		private bool IntersectsWithInclusive(RectangleF r)
		{
			return this.Left <= r.Right && this.Right >= r.Left && this.Top <= r.Bottom && this.Bottom >= r.Top;
		}

		/// <summary>Adjusts the location of this rectangle by the specified amount.</summary>
		/// <param name="x">The amount to offset the location horizontally. </param>
		/// <param name="y">The amount to offset the location vertically. </param>
		// Token: 0x0600012A RID: 298 RVA: 0x000041F2 File Offset: 0x000023F2
		public void Offset(float x, float y)
		{
			this.X += x;
			this.Y += y;
		}

		/// <summary>Adjusts the location of this rectangle by the specified amount.</summary>
		/// <param name="pos">The amount to offset the location. </param>
		// Token: 0x0600012B RID: 299 RVA: 0x00004210 File Offset: 0x00002410
		public void Offset(PointF pos)
		{
			this.Offset(pos.X, pos.Y);
		}

		/// <summary>Converts the <see langword="Location" /> and <see cref="T:System.Drawing.Size" /> of this <see cref="T:System.Drawing.RectangleF" /> to a human-readable string.</summary>
		/// <returns>A string that contains the position, width, and height of this <see cref="T:System.Drawing.RectangleF" /> structure. For example, "{X=20, Y=20, Width=100, Height=50}".</returns>
		// Token: 0x0600012C RID: 300 RVA: 0x00004228 File Offset: 0x00002428
		public override string ToString()
		{
			return string.Format("{{X={0},Y={1},Width={2},Height={3}}}", new object[]
			{
				this.x,
				this.y,
				this.width,
				this.height
			});
		}

		// Token: 0x040000F0 RID: 240
		private float x;

		// Token: 0x040000F1 RID: 241
		private float y;

		// Token: 0x040000F2 RID: 242
		private float width;

		// Token: 0x040000F3 RID: 243
		private float height;

		/// <summary>Represents an instance of the <see cref="T:System.Drawing.RectangleF" /> class with its members uninitialized.</summary>
		// Token: 0x040000F4 RID: 244
		public static readonly RectangleF Empty;
	}
}
