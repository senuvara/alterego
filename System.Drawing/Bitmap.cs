﻿using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Unity;

namespace System.Drawing
{
	/// <summary>Encapsulates a GDI+ bitmap, which consists of the pixel data for a graphics image and its attributes. A <see cref="T:System.Drawing.Bitmap" /> is an object used to work with images defined by pixel data.</summary>
	// Token: 0x0200003E RID: 62
	[ComVisible(true)]
	[Editor("System.Drawing.Design.BitmapEditor, System.Drawing.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
	[Serializable]
	public sealed class Bitmap : Image
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Bitmap" /> class from the specified existing image.</summary>
		/// <param name="original">The <see cref="T:System.Drawing.Image" /> from which to create the new <see cref="T:System.Drawing.Bitmap" />. </param>
		// Token: 0x060003B8 RID: 952 RVA: 0x00004644 File Offset: 0x00002844
		public Bitmap(Image original)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Bitmap" /> class from the specified existing image, scaled to the specified size.</summary>
		/// <param name="original">The <see cref="T:System.Drawing.Image" /> from which to create the new <see cref="T:System.Drawing.Bitmap" />. </param>
		/// <param name="newSize">The <see cref="T:System.Drawing.Size" /> structure that represent the size of the new <see cref="T:System.Drawing.Bitmap" />. </param>
		/// <exception cref="T:System.Exception">The operation failed.</exception>
		// Token: 0x060003B9 RID: 953 RVA: 0x00004644 File Offset: 0x00002844
		public Bitmap(Image original, Size newSize)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Bitmap" /> class from the specified existing image, scaled to the specified size.</summary>
		/// <param name="original">The <see cref="T:System.Drawing.Image" /> from which to create the new <see cref="T:System.Drawing.Bitmap" />. </param>
		/// <param name="width">The width, in pixels, of the new <see cref="T:System.Drawing.Bitmap" />. </param>
		/// <param name="height">The height, in pixels, of the new <see cref="T:System.Drawing.Bitmap" />. </param>
		/// <exception cref="T:System.Exception">The operation failed.</exception>
		// Token: 0x060003BA RID: 954 RVA: 0x00004644 File Offset: 0x00002844
		public Bitmap(Image original, int width, int height)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Bitmap" /> class with the specified size.</summary>
		/// <param name="width">The width, in pixels, of the new <see cref="T:System.Drawing.Bitmap" />. </param>
		/// <param name="height">The height, in pixels, of the new <see cref="T:System.Drawing.Bitmap" />. </param>
		/// <exception cref="T:System.Exception">The operation failed.</exception>
		// Token: 0x060003BB RID: 955 RVA: 0x00004644 File Offset: 0x00002844
		public Bitmap(int width, int height)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Bitmap" /> class with the specified size and with the resolution of the specified <see cref="T:System.Drawing.Graphics" /> object.</summary>
		/// <param name="width">The width, in pixels, of the new <see cref="T:System.Drawing.Bitmap" />. </param>
		/// <param name="height">The height, in pixels, of the new <see cref="T:System.Drawing.Bitmap" />. </param>
		/// <param name="g">The <see cref="T:System.Drawing.Graphics" /> object that specifies the resolution for the new <see cref="T:System.Drawing.Bitmap" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="g" /> is <see langword="null" />.</exception>
		// Token: 0x060003BC RID: 956 RVA: 0x00004644 File Offset: 0x00002844
		public Bitmap(int width, int height, Graphics g)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Bitmap" /> class with the specified size and format.</summary>
		/// <param name="width">The width, in pixels, of the new <see cref="T:System.Drawing.Bitmap" />. </param>
		/// <param name="height">The height, in pixels, of the new <see cref="T:System.Drawing.Bitmap" />. </param>
		/// <param name="format">The pixel format for the new <see cref="T:System.Drawing.Bitmap" />. This must specify a value that begins with <paramref name="Format" />.</param>
		/// <exception cref="T:System.ArgumentException">A <see cref="T:System.Drawing.Imaging.PixelFormat" /> value is specified whose name does not start with Format. For example, specifying <see cref="F:System.Drawing.Imaging.PixelFormat.Gdi" /> will cause an <see cref="T:System.ArgumentException" />, but <see cref="F:System.Drawing.Imaging.PixelFormat.Format48bppRgb" /> will not.</exception>
		// Token: 0x060003BD RID: 957 RVA: 0x00004644 File Offset: 0x00002844
		public Bitmap(int width, int height, PixelFormat format)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Bitmap" /> class with the specified size, pixel format, and pixel data.</summary>
		/// <param name="width">The width, in pixels, of the new <see cref="T:System.Drawing.Bitmap" />. </param>
		/// <param name="height">The height, in pixels, of the new <see cref="T:System.Drawing.Bitmap" />. </param>
		/// <param name="stride">Integer that specifies the byte offset between the beginning of one scan line and the next. This is usually (but not necessarily) the number of bytes in the pixel format (for example, 2 for 16 bits per pixel) multiplied by the width of the bitmap. The value passed to this parameter must be a multiple of four.. </param>
		/// <param name="format">The pixel format for the new <see cref="T:System.Drawing.Bitmap" />. This must specify a value that begins with <paramref name="Format" />.</param>
		/// <param name="scan0">Pointer to an array of bytes that contains the pixel data.</param>
		/// <exception cref="T:System.ArgumentException">A <see cref="T:System.Drawing.Imaging.PixelFormat" /> value is specified whose name does not start with Format. For example, specifying <see cref="F:System.Drawing.Imaging.PixelFormat.Gdi" /> will cause an <see cref="T:System.ArgumentException" />, but <see cref="F:System.Drawing.Imaging.PixelFormat.Format48bppRgb" /> will not.</exception>
		// Token: 0x060003BE RID: 958 RVA: 0x00004644 File Offset: 0x00002844
		public Bitmap(int width, int height, int stride, PixelFormat format, IntPtr scan0)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Bitmap" /> class from the specified data stream.</summary>
		/// <param name="stream">The data stream used to load the image. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="stream" /> does not contain image data or is <see langword="null" />.-or-
		///         <paramref name="stream" /> contains a PNG image file with a single dimension greater than 65,535 pixels.</exception>
		// Token: 0x060003BF RID: 959 RVA: 0x00004644 File Offset: 0x00002844
		public Bitmap(Stream stream)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Bitmap" /> class from the specified data stream.</summary>
		/// <param name="stream">The data stream used to load the image. </param>
		/// <param name="useIcm">
		///       <see langword="true" /> to use color correction for this <see cref="T:System.Drawing.Bitmap" />; otherwise, <see langword="false" />. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="stream" /> does not contain image data or is <see langword="null" />.-or-
		///         <paramref name="stream" /> contains a PNG image file with a single dimension greater than 65,535 pixels.</exception>
		// Token: 0x060003C0 RID: 960 RVA: 0x00004644 File Offset: 0x00002844
		public Bitmap(Stream stream, bool useIcm)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Bitmap" /> class from the specified file.</summary>
		/// <param name="filename">The bitmap file name and path. </param>
		/// <exception cref="T:System.IO.FileNotFoundException">The specified file is not found.</exception>
		// Token: 0x060003C1 RID: 961 RVA: 0x00004644 File Offset: 0x00002844
		public Bitmap(string filename)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Bitmap" /> class from the specified file.</summary>
		/// <param name="filename">The name of the bitmap file. </param>
		/// <param name="useIcm">
		///       <see langword="true" /> to use color correction for this <see cref="T:System.Drawing.Bitmap" />; otherwise, <see langword="false" />. </param>
		// Token: 0x060003C2 RID: 962 RVA: 0x00004644 File Offset: 0x00002844
		public Bitmap(string filename, bool useIcm)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Bitmap" /> class from a specified resource.</summary>
		/// <param name="type">The class used to extract the resource. </param>
		/// <param name="resource">The name of the resource. </param>
		// Token: 0x060003C3 RID: 963 RVA: 0x00004644 File Offset: 0x00002844
		public Bitmap(Type type, string resource)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a copy of the section of this <see cref="T:System.Drawing.Bitmap" /> defined by <see cref="T:System.Drawing.Rectangle" /> structure and with a specified <see cref="T:System.Drawing.Imaging.PixelFormat" /> enumeration.</summary>
		/// <param name="rect">Defines the portion of this <see cref="T:System.Drawing.Bitmap" /> to copy. Coordinates are relative to this <see cref="T:System.Drawing.Bitmap" />. </param>
		/// <param name="format">The pixel format for the new <see cref="T:System.Drawing.Bitmap" />. This must specify a value that begins with <paramref name="Format" />.</param>
		/// <returns>The new <see cref="T:System.Drawing.Bitmap" /> that this method creates.</returns>
		/// <exception cref="T:System.OutOfMemoryException">
		///         <paramref name="rect" /> is outside of the source bitmap bounds.</exception>
		/// <exception cref="T:System.ArgumentException">The height or width of <paramref name="rect" /> is 0. -or-A <see cref="T:System.Drawing.Imaging.PixelFormat" /> value is specified whose name does not start with Format. For example, specifying <see cref="F:System.Drawing.Imaging.PixelFormat.Gdi" /> will cause an <see cref="T:System.ArgumentException" />, but <see cref="F:System.Drawing.Imaging.PixelFormat.Format48bppRgb" /> will not.</exception>
		// Token: 0x060003C4 RID: 964 RVA: 0x00004667 File Offset: 0x00002867
		public Bitmap Clone(Rectangle rect, PixelFormat format)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates a copy of the section of this <see cref="T:System.Drawing.Bitmap" /> defined with a specified <see cref="T:System.Drawing.Imaging.PixelFormat" /> enumeration.</summary>
		/// <param name="rect">Defines the portion of this <see cref="T:System.Drawing.Bitmap" /> to copy. </param>
		/// <param name="format">Specifies the <see cref="T:System.Drawing.Imaging.PixelFormat" /> enumeration for the destination <see cref="T:System.Drawing.Bitmap" />. </param>
		/// <returns>The <see cref="T:System.Drawing.Bitmap" /> that this method creates.</returns>
		/// <exception cref="T:System.OutOfMemoryException">
		///         <paramref name="rect" /> is outside of the source bitmap bounds.</exception>
		/// <exception cref="T:System.ArgumentException">The height or width of <paramref name="rect" /> is 0. </exception>
		// Token: 0x060003C5 RID: 965 RVA: 0x00004667 File Offset: 0x00002867
		public Bitmap Clone(RectangleF rect, PixelFormat format)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates a <see cref="T:System.Drawing.Bitmap" /> from a Windows handle to an icon.</summary>
		/// <param name="hicon">A handle to an icon. </param>
		/// <returns>The <see cref="T:System.Drawing.Bitmap" /> that this method creates.</returns>
		// Token: 0x060003C6 RID: 966 RVA: 0x00004667 File Offset: 0x00002867
		public static Bitmap FromHicon(IntPtr hicon)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates a <see cref="T:System.Drawing.Bitmap" /> from the specified Windows resource.</summary>
		/// <param name="hinstance">A handle to an instance of the executable file that contains the resource. </param>
		/// <param name="bitmapName">A string that contains the name of the resource bitmap. </param>
		/// <returns>The <see cref="T:System.Drawing.Bitmap" /> that this method creates.</returns>
		// Token: 0x060003C7 RID: 967 RVA: 0x00004667 File Offset: 0x00002867
		public static Bitmap FromResource(IntPtr hinstance, string bitmapName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates a GDI bitmap object from this <see cref="T:System.Drawing.Bitmap" />.</summary>
		/// <returns>A handle to the GDI bitmap object that this method creates.</returns>
		/// <exception cref="T:System.ArgumentException">The height or width of the bitmap is greater than <see cref="F:System.Int16.MaxValue" />.</exception>
		/// <exception cref="T:System.Exception">The operation failed.</exception>
		// Token: 0x060003C8 RID: 968 RVA: 0x00005400 File Offset: 0x00003600
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public IntPtr GetHbitmap()
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Creates a GDI bitmap object from this <see cref="T:System.Drawing.Bitmap" />.</summary>
		/// <param name="background">A <see cref="T:System.Drawing.Color" /> structure that specifies the background color. This parameter is ignored if the bitmap is totally opaque. </param>
		/// <returns>A handle to the GDI bitmap object that this method creates.</returns>
		/// <exception cref="T:System.ArgumentException">The height or width of the bitmap is greater than <see cref="F:System.Int16.MaxValue" />.</exception>
		/// <exception cref="T:System.Exception">The operation failed.</exception>
		// Token: 0x060003C9 RID: 969 RVA: 0x0000541C File Offset: 0x0000361C
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public IntPtr GetHbitmap(Color background)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Returns the handle to an icon.</summary>
		/// <returns>A Windows handle to an icon with the same image as the <see cref="T:System.Drawing.Bitmap" />.</returns>
		/// <exception cref="T:System.Exception">The operation failed.</exception>
		// Token: 0x060003CA RID: 970 RVA: 0x00005438 File Offset: 0x00003638
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public IntPtr GetHicon()
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Gets the color of the specified pixel in this <see cref="T:System.Drawing.Bitmap" />.</summary>
		/// <param name="x">The x-coordinate of the pixel to retrieve. </param>
		/// <param name="y">The y-coordinate of the pixel to retrieve. </param>
		/// <returns>A <see cref="T:System.Drawing.Color" /> structure that represents the color of the specified pixel.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="x" /> is less than 0, or greater than or equal to <see cref="P:System.Drawing.Image.Width" />. -or-
		///         <paramref name="y" /> is less than 0, or greater than or equal to <see cref="P:System.Drawing.Image.Height" />.</exception>
		/// <exception cref="T:System.Exception">The operation failed.</exception>
		// Token: 0x060003CB RID: 971 RVA: 0x00005454 File Offset: 0x00003654
		public Color GetPixel(int x, int y)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(Color);
		}

		/// <summary>Locks a <see cref="T:System.Drawing.Bitmap" /> into system memory.</summary>
		/// <param name="rect">A <see cref="T:System.Drawing.Rectangle" /> structure that specifies the portion of the <see cref="T:System.Drawing.Bitmap" /> to lock. </param>
		/// <param name="flags">An <see cref="T:System.Drawing.Imaging.ImageLockMode" /> enumeration that specifies the access level (read/write) for the <see cref="T:System.Drawing.Bitmap" />. </param>
		/// <param name="format">A <see cref="T:System.Drawing.Imaging.PixelFormat" /> enumeration that specifies the data format of this <see cref="T:System.Drawing.Bitmap" />. </param>
		/// <returns>A <see cref="T:System.Drawing.Imaging.BitmapData" /> that contains information about this lock operation.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="T:System.Drawing.Imaging.PixelFormat" /> is not a specific bits-per-pixel value.-or-The incorrect <see cref="T:System.Drawing.Imaging.PixelFormat" /> is passed in for a bitmap.</exception>
		/// <exception cref="T:System.Exception">The operation failed.</exception>
		// Token: 0x060003CC RID: 972 RVA: 0x00004667 File Offset: 0x00002867
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public BitmapData LockBits(Rectangle rect, ImageLockMode flags, PixelFormat format)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Locks a <see cref="T:System.Drawing.Bitmap" /> into system memory </summary>
		/// <param name="rect">A rectangle structure that specifies the portion of the <see cref="T:System.Drawing.Bitmap" /> to lock.</param>
		/// <param name="flags">One of the <see cref="T:System.Drawing.Imaging.ImageLockMode" /> values that specifies the access level (read/write) for the <see cref="T:System.Drawing.Bitmap" />.</param>
		/// <param name="format">One of the <see cref="T:System.Drawing.Imaging.PixelFormat" /> values that specifies the data format of the <see cref="T:System.Drawing.Bitmap" />.</param>
		/// <param name="bitmapData">A <see cref="T:System.Drawing.Imaging.BitmapData" /> that contains information about the lock operation.</param>
		/// <returns>A <see cref="T:System.Drawing.Imaging.BitmapData" /> that contains information about the lock operation.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <see cref="T:System.Drawing.Imaging.PixelFormat" /> value is not a specific bits-per-pixel value.-or-The incorrect <see cref="T:System.Drawing.Imaging.PixelFormat" /> is passed in for a bitmap.</exception>
		/// <exception cref="T:System.Exception">The operation failed.</exception>
		// Token: 0x060003CD RID: 973 RVA: 0x00004667 File Offset: 0x00002867
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public BitmapData LockBits(Rectangle rect, ImageLockMode flags, PixelFormat format, BitmapData bitmapData)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Makes the default transparent color transparent for this <see cref="T:System.Drawing.Bitmap" />.</summary>
		/// <exception cref="T:System.InvalidOperationException">The image format of the <see cref="T:System.Drawing.Bitmap" /> is an icon format.</exception>
		/// <exception cref="T:System.Exception">The operation failed.</exception>
		// Token: 0x060003CE RID: 974 RVA: 0x00004644 File Offset: 0x00002844
		public void MakeTransparent()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Makes the specified color transparent for this <see cref="T:System.Drawing.Bitmap" />.</summary>
		/// <param name="transparentColor">The <see cref="T:System.Drawing.Color" /> structure that represents the color to make transparent. </param>
		/// <exception cref="T:System.InvalidOperationException">The image format of the <see cref="T:System.Drawing.Bitmap" /> is an icon format.</exception>
		/// <exception cref="T:System.Exception">The operation failed.</exception>
		// Token: 0x060003CF RID: 975 RVA: 0x00004644 File Offset: 0x00002844
		public void MakeTransparent(Color transparentColor)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the color of the specified pixel in this <see cref="T:System.Drawing.Bitmap" />.</summary>
		/// <param name="x">The x-coordinate of the pixel to set. </param>
		/// <param name="y">The y-coordinate of the pixel to set. </param>
		/// <param name="color">A <see cref="T:System.Drawing.Color" /> structure that represents the color to assign to the specified pixel. </param>
		/// <exception cref="T:System.Exception">The operation failed.</exception>
		// Token: 0x060003D0 RID: 976 RVA: 0x00004644 File Offset: 0x00002844
		public void SetPixel(int x, int y, Color color)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the resolution for this <see cref="T:System.Drawing.Bitmap" />.</summary>
		/// <param name="xDpi">The horizontal resolution, in dots per inch, of the <see cref="T:System.Drawing.Bitmap" />. </param>
		/// <param name="yDpi">The vertical resolution, in dots per inch, of the <see cref="T:System.Drawing.Bitmap" />. </param>
		/// <exception cref="T:System.Exception">The operation failed.</exception>
		// Token: 0x060003D1 RID: 977 RVA: 0x00004644 File Offset: 0x00002844
		public void SetResolution(float xDpi, float yDpi)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Unlocks this <see cref="T:System.Drawing.Bitmap" /> from system memory.</summary>
		/// <param name="bitmapdata">A <see cref="T:System.Drawing.Imaging.BitmapData" /> that specifies information about the lock operation. </param>
		/// <exception cref="T:System.Exception">The operation failed.</exception>
		// Token: 0x060003D2 RID: 978 RVA: 0x00004644 File Offset: 0x00002844
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public void UnlockBits(BitmapData bitmapdata)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
