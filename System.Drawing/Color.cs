﻿using System;

namespace System.Drawing
{
	/// <summary>Represents an ARGB (alpha, red, green, blue) color.</summary>
	// Token: 0x02000003 RID: 3
	[Serializable]
	public struct Color
	{
		/// <summary>Gets the name of this <see cref="T:System.Drawing.Color" />.</summary>
		/// <returns>The name of this <see cref="T:System.Drawing.Color" />.</returns>
		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public string Name
		{
			get
			{
				if (this.name == null)
				{
					if (this.IsNamedColor)
					{
						this.name = KnownColors.GetName(this.knownColor);
					}
					else
					{
						this.name = string.Format("{0:x}", this.ToArgb());
					}
				}
				return this.name;
			}
		}

		/// <summary>Gets a value indicating whether this <see cref="T:System.Drawing.Color" /> structure is a predefined color. Predefined colors are represented by the elements of the <see cref="T:System.Drawing.KnownColor" /> enumeration.</summary>
		/// <returns>
		///     <see langword="true" /> if this <see cref="T:System.Drawing.Color" /> was created from a predefined color by using either the <see cref="M:System.Drawing.Color.FromName(System.String)" /> method or the <see cref="M:System.Drawing.Color.FromKnownColor(System.Drawing.KnownColor)" /> method; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000002 RID: 2 RVA: 0x000020A1 File Offset: 0x000002A1
		public bool IsKnownColor
		{
			get
			{
				return (this.state & 1) != 0;
			}
		}

		/// <summary>Gets a value indicating whether this <see cref="T:System.Drawing.Color" /> structure is a system color. A system color is a color that is used in a Windows display element. System colors are represented by elements of the <see cref="T:System.Drawing.KnownColor" /> enumeration.</summary>
		/// <returns>
		///     <see langword="true" /> if this <see cref="T:System.Drawing.Color" /> was created from a system color by using either the <see cref="M:System.Drawing.Color.FromName(System.String)" /> method or the <see cref="M:System.Drawing.Color.FromKnownColor(System.Drawing.KnownColor)" /> method; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000003 RID: 3 RVA: 0x000020AE File Offset: 0x000002AE
		public bool IsSystemColor
		{
			get
			{
				return (this.state & 8) != 0;
			}
		}

		/// <summary>Gets a value indicating whether this <see cref="T:System.Drawing.Color" /> structure is a named color or a member of the <see cref="T:System.Drawing.KnownColor" /> enumeration.</summary>
		/// <returns>
		///     <see langword="true" /> if this <see cref="T:System.Drawing.Color" /> was created by using either the <see cref="M:System.Drawing.Color.FromName(System.String)" /> method or the <see cref="M:System.Drawing.Color.FromKnownColor(System.Drawing.KnownColor)" /> method; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000004 RID: 4 RVA: 0x000020BB File Offset: 0x000002BB
		public bool IsNamedColor
		{
			get
			{
				return (this.state & 5) != 0;
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000005 RID: 5 RVA: 0x000020C8 File Offset: 0x000002C8
		// (set) Token: 0x06000006 RID: 6 RVA: 0x00002108 File Offset: 0x00000308
		internal long Value
		{
			get
			{
				if (this.value == 0L && this.IsKnownColor)
				{
					this.value = ((long)Color.FromKnownColor((KnownColor)this.knownColor).ToArgb() & (long)((ulong)-1));
				}
				return this.value;
			}
			set
			{
				this.value = value;
			}
		}

		/// <summary>Creates a <see cref="T:System.Drawing.Color" /> structure from the specified 8-bit color values (red, green, and blue). The alpha value is implicitly 255 (fully opaque). Although this method allows a 32-bit value to be passed for each color component, the value of each component is limited to 8 bits.</summary>
		/// <param name="red">The red component value for the new <see cref="T:System.Drawing.Color" />. Valid values are 0 through 255. </param>
		/// <param name="green">The green component value for the new <see cref="T:System.Drawing.Color" />. Valid values are 0 through 255. </param>
		/// <param name="blue">The blue component value for the new <see cref="T:System.Drawing.Color" />. Valid values are 0 through 255. </param>
		/// <returns>The <see cref="T:System.Drawing.Color" /> that this method creates.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="red" />, <paramref name="green" />, or <paramref name="blue" /> is less than 0 or greater than 255.</exception>
		// Token: 0x06000007 RID: 7 RVA: 0x00002111 File Offset: 0x00000311
		public static Color FromArgb(int red, int green, int blue)
		{
			return Color.FromArgb(255, red, green, blue);
		}

		/// <summary>Creates a <see cref="T:System.Drawing.Color" /> structure from the four ARGB component (alpha, red, green, and blue) values. Although this method allows a 32-bit value to be passed for each component, the value of each component is limited to 8 bits.</summary>
		/// <param name="alpha">The alpha component. Valid values are 0 through 255. </param>
		/// <param name="red">The red component. Valid values are 0 through 255. </param>
		/// <param name="green">The green component. Valid values are 0 through 255. </param>
		/// <param name="blue">The blue component. Valid values are 0 through 255. </param>
		/// <returns>The <see cref="T:System.Drawing.Color" /> that this method creates.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="alpha" />, <paramref name="red" />, <paramref name="green" />, or <paramref name="blue" /> is less than 0 or greater than 255.</exception>
		// Token: 0x06000008 RID: 8 RVA: 0x00002120 File Offset: 0x00000320
		public static Color FromArgb(int alpha, int red, int green, int blue)
		{
			Color.CheckARGBValues(alpha, red, green, blue);
			return new Color
			{
				state = 2,
				Value = (long)((alpha << 24) + (red << 16) + (green << 8) + blue)
			};
		}

		/// <summary>Gets the 32-bit ARGB value of this <see cref="T:System.Drawing.Color" /> structure.</summary>
		/// <returns>The 32-bit ARGB value of this <see cref="T:System.Drawing.Color" />.</returns>
		// Token: 0x06000009 RID: 9 RVA: 0x0000215E File Offset: 0x0000035E
		public int ToArgb()
		{
			return (int)this.Value;
		}

		/// <summary>Creates a <see cref="T:System.Drawing.Color" /> structure from the specified <see cref="T:System.Drawing.Color" /> structure, but with the new specified alpha value. Although this method allows a 32-bit value to be passed for the alpha value, the value is limited to 8 bits.</summary>
		/// <param name="alpha">The alpha value for the new <see cref="T:System.Drawing.Color" />. Valid values are 0 through 255. </param>
		/// <param name="baseColor">The <see cref="T:System.Drawing.Color" /> from which to create the new <see cref="T:System.Drawing.Color" />. </param>
		/// <returns>The <see cref="T:System.Drawing.Color" /> that this method creates.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="alpha" /> is less than 0 or greater than 255.</exception>
		// Token: 0x0600000A RID: 10 RVA: 0x00002167 File Offset: 0x00000367
		public static Color FromArgb(int alpha, Color baseColor)
		{
			return Color.FromArgb(alpha, (int)baseColor.R, (int)baseColor.G, (int)baseColor.B);
		}

		/// <summary>Creates a <see cref="T:System.Drawing.Color" /> structure from a 32-bit ARGB value.</summary>
		/// <param name="argb">A value specifying the 32-bit ARGB value. </param>
		/// <returns>The <see cref="T:System.Drawing.Color" /> structure that this method creates.</returns>
		// Token: 0x0600000B RID: 11 RVA: 0x00002184 File Offset: 0x00000384
		public static Color FromArgb(int argb)
		{
			return Color.FromArgb(argb >> 24 & 255, argb >> 16 & 255, argb >> 8 & 255, argb & 255);
		}

		/// <summary>Creates a <see cref="T:System.Drawing.Color" /> structure from the specified predefined color.</summary>
		/// <param name="color">An element of the <see cref="T:System.Drawing.KnownColor" /> enumeration. </param>
		/// <returns>The <see cref="T:System.Drawing.Color" /> that this method creates.</returns>
		// Token: 0x0600000C RID: 12 RVA: 0x000021B0 File Offset: 0x000003B0
		public static Color FromKnownColor(KnownColor color)
		{
			short num = (short)color;
			Color result;
			if (num <= 0 || (int)num >= KnownColors.ArgbValues.Length)
			{
				result = Color.FromArgb(0, 0, 0, 0);
				result.state |= 4;
			}
			else
			{
				result = default(Color);
				result.state = 7;
				if (num < 27 || num > 169)
				{
					result.state |= 8;
				}
				result.Value = (long)((ulong)KnownColors.ArgbValues[(int)num]);
			}
			result.knownColor = num;
			return result;
		}

		/// <summary>Creates a <see cref="T:System.Drawing.Color" /> structure from the specified name of a predefined color.</summary>
		/// <param name="name">A string that is the name of a predefined color. Valid names are the same as the names of the elements of the <see cref="T:System.Drawing.KnownColor" /> enumeration. </param>
		/// <returns>The <see cref="T:System.Drawing.Color" /> that this method creates.</returns>
		// Token: 0x0600000D RID: 13 RVA: 0x0000222C File Offset: 0x0000042C
		public static Color FromName(string name)
		{
			Color result;
			try
			{
				result = Color.FromKnownColor((KnownColor)Enum.Parse(typeof(KnownColor), name, true));
			}
			catch
			{
				Color color = Color.FromArgb(0, 0, 0, 0);
				color.name = name;
				color.state |= 4;
				result = color;
			}
			return result;
		}

		/// <summary>Tests whether two specified <see cref="T:System.Drawing.Color" /> structures are equivalent.</summary>
		/// <param name="left">The <see cref="T:System.Drawing.Color" /> that is to the left of the equality operator. </param>
		/// <param name="right">The <see cref="T:System.Drawing.Color" /> that is to the right of the equality operator. </param>
		/// <returns>
		///     <see langword="true" /> if the two <see cref="T:System.Drawing.Color" /> structures are equal; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600000E RID: 14 RVA: 0x0000228C File Offset: 0x0000048C
		public static bool operator ==(Color left, Color right)
		{
			return left.Value == right.Value && left.IsNamedColor == right.IsNamedColor && left.IsSystemColor == right.IsSystemColor && left.IsEmpty == right.IsEmpty && (!left.IsNamedColor || !(left.Name != right.Name));
		}

		/// <summary>Tests whether two specified <see cref="T:System.Drawing.Color" /> structures are different.</summary>
		/// <param name="left">The <see cref="T:System.Drawing.Color" /> that is to the left of the inequality operator. </param>
		/// <param name="right">The <see cref="T:System.Drawing.Color" /> that is to the right of the inequality operator. </param>
		/// <returns>
		///     <see langword="true" /> if the two <see cref="T:System.Drawing.Color" /> structures are different; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600000F RID: 15 RVA: 0x00002302 File Offset: 0x00000502
		public static bool operator !=(Color left, Color right)
		{
			return !(left == right);
		}

		/// <summary>Gets the hue-saturation-brightness (HSB) brightness value for this <see cref="T:System.Drawing.Color" /> structure.</summary>
		/// <returns>The brightness of this <see cref="T:System.Drawing.Color" />. The brightness ranges from 0.0 through 1.0, where 0.0 represents black and 1.0 represents white.</returns>
		// Token: 0x06000010 RID: 16 RVA: 0x00002310 File Offset: 0x00000510
		public float GetBrightness()
		{
			byte b = Math.Min(this.R, Math.Min(this.G, this.B));
			return (float)(Math.Max(this.R, Math.Max(this.G, this.B)) + b) / 510f;
		}

		/// <summary>Gets the hue-saturation-brightness (HSB) saturation value for this <see cref="T:System.Drawing.Color" /> structure.</summary>
		/// <returns>The saturation of this <see cref="T:System.Drawing.Color" />. The saturation ranges from 0.0 through 1.0, where 0.0 is grayscale and 1.0 is the most saturated.</returns>
		// Token: 0x06000011 RID: 17 RVA: 0x00002360 File Offset: 0x00000560
		public float GetSaturation()
		{
			byte b = Math.Min(this.R, Math.Min(this.G, this.B));
			byte b2 = Math.Max(this.R, Math.Max(this.G, this.B));
			if (b2 == b)
			{
				return 0f;
			}
			int num = (int)(b2 + b);
			if (num > 255)
			{
				num = 510 - num;
			}
			return (float)(b2 - b) / (float)num;
		}

		/// <summary>Gets the hue-saturation-brightness (HSB) hue value, in degrees, for this <see cref="T:System.Drawing.Color" /> structure.</summary>
		/// <returns>The hue, in degrees, of this <see cref="T:System.Drawing.Color" />. The hue is measured in degrees, ranging from 0.0 through 360.0, in HSB color space.</returns>
		// Token: 0x06000012 RID: 18 RVA: 0x000023CC File Offset: 0x000005CC
		public float GetHue()
		{
			int r = (int)this.R;
			int g = (int)this.G;
			int b = (int)this.B;
			byte b2 = (byte)Math.Min(r, Math.Min(g, b));
			byte b3 = (byte)Math.Max(r, Math.Max(g, b));
			if (b3 == b2)
			{
				return 0f;
			}
			float num = (float)(b3 - b2);
			float num2 = (float)((int)b3 - r) / num;
			float num3 = (float)((int)b3 - g) / num;
			float num4 = (float)((int)b3 - b) / num;
			float num5 = 0f;
			if (r == (int)b3)
			{
				num5 = 60f * (6f + num4 - num3);
			}
			if (g == (int)b3)
			{
				num5 = 60f * (2f + num2 - num4);
			}
			if (b == (int)b3)
			{
				num5 = 60f * (4f + num3 - num2);
			}
			if (num5 > 360f)
			{
				num5 -= 360f;
			}
			return num5;
		}

		/// <summary>Gets the <see cref="T:System.Drawing.KnownColor" /> value of this <see cref="T:System.Drawing.Color" /> structure.</summary>
		/// <returns>An element of the <see cref="T:System.Drawing.KnownColor" /> enumeration, if the <see cref="T:System.Drawing.Color" /> is created from a predefined color by using either the <see cref="M:System.Drawing.Color.FromName(System.String)" /> method or the <see cref="M:System.Drawing.Color.FromKnownColor(System.Drawing.KnownColor)" /> method; otherwise, 0.</returns>
		// Token: 0x06000013 RID: 19 RVA: 0x000024A1 File Offset: 0x000006A1
		public KnownColor ToKnownColor()
		{
			return (KnownColor)this.knownColor;
		}

		/// <summary>Specifies whether this <see cref="T:System.Drawing.Color" /> structure is uninitialized.</summary>
		/// <returns>This property returns <see langword="true" /> if this color is uninitialized; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000014 RID: 20 RVA: 0x000024A9 File Offset: 0x000006A9
		public bool IsEmpty
		{
			get
			{
				return this.state == 0;
			}
		}

		/// <summary>Gets the alpha component value of this <see cref="T:System.Drawing.Color" /> structure.</summary>
		/// <returns>The alpha component value of this <see cref="T:System.Drawing.Color" />.</returns>
		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000015 RID: 21 RVA: 0x000024B4 File Offset: 0x000006B4
		public byte A
		{
			get
			{
				return (byte)(this.Value >> 24);
			}
		}

		/// <summary>Gets the red component value of this <see cref="T:System.Drawing.Color" /> structure.</summary>
		/// <returns>The red component value of this <see cref="T:System.Drawing.Color" />.</returns>
		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000016 RID: 22 RVA: 0x000024C0 File Offset: 0x000006C0
		public byte R
		{
			get
			{
				return (byte)(this.Value >> 16);
			}
		}

		/// <summary>Gets the green component value of this <see cref="T:System.Drawing.Color" /> structure.</summary>
		/// <returns>The green component value of this <see cref="T:System.Drawing.Color" />.</returns>
		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000017 RID: 23 RVA: 0x000024CC File Offset: 0x000006CC
		public byte G
		{
			get
			{
				return (byte)(this.Value >> 8);
			}
		}

		/// <summary>Gets the blue component value of this <see cref="T:System.Drawing.Color" /> structure.</summary>
		/// <returns>The blue component value of this <see cref="T:System.Drawing.Color" />.</returns>
		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000018 RID: 24 RVA: 0x000024D7 File Offset: 0x000006D7
		public byte B
		{
			get
			{
				return (byte)this.Value;
			}
		}

		/// <summary>Tests whether the specified object is a <see cref="T:System.Drawing.Color" /> structure and is equivalent to this <see cref="T:System.Drawing.Color" /> structure.</summary>
		/// <param name="obj">The object to test. </param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="obj" /> is a <see cref="T:System.Drawing.Color" /> structure equivalent to this <see cref="T:System.Drawing.Color" /> structure; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000019 RID: 25 RVA: 0x000024E0 File Offset: 0x000006E0
		public override bool Equals(object obj)
		{
			if (!(obj is Color))
			{
				return false;
			}
			Color right = (Color)obj;
			return this == right;
		}

		/// <summary>Returns a hash code for this <see cref="T:System.Drawing.Color" /> structure.</summary>
		/// <returns>An integer value that specifies the hash code for this <see cref="T:System.Drawing.Color" />.</returns>
		// Token: 0x0600001A RID: 26 RVA: 0x0000250C File Offset: 0x0000070C
		public override int GetHashCode()
		{
			int num = (int)(this.Value ^ this.Value >> 32 ^ (long)this.state ^ (long)(this.knownColor >> 16));
			if (this.IsNamedColor)
			{
				num ^= this.Name.GetHashCode();
			}
			return num;
		}

		/// <summary>Converts this <see cref="T:System.Drawing.Color" /> structure to a human-readable string.</summary>
		/// <returns>A string that is the name of this <see cref="T:System.Drawing.Color" />, if the <see cref="T:System.Drawing.Color" /> is created from a predefined color by using either the <see cref="M:System.Drawing.Color.FromName(System.String)" /> method or the <see cref="M:System.Drawing.Color.FromKnownColor(System.Drawing.KnownColor)" /> method; otherwise, a string that consists of the ARGB component names and their values.</returns>
		// Token: 0x0600001B RID: 27 RVA: 0x00002558 File Offset: 0x00000758
		public override string ToString()
		{
			if (this.IsEmpty)
			{
				return "Color [Empty]";
			}
			if (this.IsNamedColor)
			{
				return "Color [" + this.Name + "]";
			}
			return string.Format("Color [A={0}, R={1}, G={2}, B={3}]", new object[]
			{
				this.A,
				this.R,
				this.G,
				this.B
			});
		}

		// Token: 0x0600001C RID: 28 RVA: 0x000025DC File Offset: 0x000007DC
		private static void CheckRGBValues(int red, int green, int blue)
		{
			if (red > 255 || red < 0)
			{
				throw Color.CreateColorArgumentException(red, "red");
			}
			if (green > 255 || green < 0)
			{
				throw Color.CreateColorArgumentException(green, "green");
			}
			if (blue > 255 || blue < 0)
			{
				throw Color.CreateColorArgumentException(blue, "blue");
			}
		}

		// Token: 0x0600001D RID: 29 RVA: 0x00002631 File Offset: 0x00000831
		private static ArgumentException CreateColorArgumentException(int value, string color)
		{
			return new ArgumentException(string.Format("'{0}' is not a valid value for '{1}'. '{1}' should be greater or equal to 0 and less than or equal to 255.", value, color));
		}

		// Token: 0x0600001E RID: 30 RVA: 0x00002649 File Offset: 0x00000849
		private static void CheckARGBValues(int alpha, int red, int green, int blue)
		{
			if (alpha > 255 || alpha < 0)
			{
				throw Color.CreateColorArgumentException(alpha, "alpha");
			}
			Color.CheckRGBValues(red, green, blue);
		}

		/// <summary>Gets a system-defined color.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700000B RID: 11
		// (get) Token: 0x0600001F RID: 31 RVA: 0x0000266B File Offset: 0x0000086B
		public static Color Transparent
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Transparent);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFF0F8FF.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000020 RID: 32 RVA: 0x00002674 File Offset: 0x00000874
		public static Color AliceBlue
		{
			get
			{
				return Color.FromKnownColor(KnownColor.AliceBlue);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFAEBD7.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000021 RID: 33 RVA: 0x0000267D File Offset: 0x0000087D
		public static Color AntiqueWhite
		{
			get
			{
				return Color.FromKnownColor(KnownColor.AntiqueWhite);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF00FFFF.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700000E RID: 14
		// (get) Token: 0x06000022 RID: 34 RVA: 0x00002686 File Offset: 0x00000886
		public static Color Aqua
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Aqua);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF7FFFD4.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000023 RID: 35 RVA: 0x0000268F File Offset: 0x0000088F
		public static Color Aquamarine
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Aquamarine);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFF0FFFF.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000024 RID: 36 RVA: 0x00002698 File Offset: 0x00000898
		public static Color Azure
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Azure);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFF5F5DC.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000025 RID: 37 RVA: 0x000026A1 File Offset: 0x000008A1
		public static Color Beige
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Beige);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFFE4C4.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000026 RID: 38 RVA: 0x000026AA File Offset: 0x000008AA
		public static Color Bisque
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Bisque);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF000000.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000027 RID: 39 RVA: 0x000026B3 File Offset: 0x000008B3
		public static Color Black
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Black);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFFEBCD.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000028 RID: 40 RVA: 0x000026BC File Offset: 0x000008BC
		public static Color BlanchedAlmond
		{
			get
			{
				return Color.FromKnownColor(KnownColor.BlanchedAlmond);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF0000FF.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000029 RID: 41 RVA: 0x000026C5 File Offset: 0x000008C5
		public static Color Blue
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Blue);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF8A2BE2.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000016 RID: 22
		// (get) Token: 0x0600002A RID: 42 RVA: 0x000026CE File Offset: 0x000008CE
		public static Color BlueViolet
		{
			get
			{
				return Color.FromKnownColor(KnownColor.BlueViolet);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFA52A2A.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000017 RID: 23
		// (get) Token: 0x0600002B RID: 43 RVA: 0x000026D7 File Offset: 0x000008D7
		public static Color Brown
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Brown);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFDEB887.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000018 RID: 24
		// (get) Token: 0x0600002C RID: 44 RVA: 0x000026E0 File Offset: 0x000008E0
		public static Color BurlyWood
		{
			get
			{
				return Color.FromKnownColor(KnownColor.BurlyWood);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF5F9EA0.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000019 RID: 25
		// (get) Token: 0x0600002D RID: 45 RVA: 0x000026E9 File Offset: 0x000008E9
		public static Color CadetBlue
		{
			get
			{
				return Color.FromKnownColor(KnownColor.CadetBlue);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF7FFF00.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700001A RID: 26
		// (get) Token: 0x0600002E RID: 46 RVA: 0x000026F2 File Offset: 0x000008F2
		public static Color Chartreuse
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Chartreuse);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFD2691E.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700001B RID: 27
		// (get) Token: 0x0600002F RID: 47 RVA: 0x000026FB File Offset: 0x000008FB
		public static Color Chocolate
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Chocolate);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFF7F50.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000030 RID: 48 RVA: 0x00002704 File Offset: 0x00000904
		public static Color Coral
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Coral);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF6495ED.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700001D RID: 29
		// (get) Token: 0x06000031 RID: 49 RVA: 0x0000270D File Offset: 0x0000090D
		public static Color CornflowerBlue
		{
			get
			{
				return Color.FromKnownColor(KnownColor.CornflowerBlue);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFFF8DC.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700001E RID: 30
		// (get) Token: 0x06000032 RID: 50 RVA: 0x00002716 File Offset: 0x00000916
		public static Color Cornsilk
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Cornsilk);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFDC143C.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000033 RID: 51 RVA: 0x0000271F File Offset: 0x0000091F
		public static Color Crimson
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Crimson);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF00FFFF.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000020 RID: 32
		// (get) Token: 0x06000034 RID: 52 RVA: 0x00002728 File Offset: 0x00000928
		public static Color Cyan
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Cyan);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF00008B.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000021 RID: 33
		// (get) Token: 0x06000035 RID: 53 RVA: 0x00002731 File Offset: 0x00000931
		public static Color DarkBlue
		{
			get
			{
				return Color.FromKnownColor(KnownColor.DarkBlue);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF008B8B.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000036 RID: 54 RVA: 0x0000273A File Offset: 0x0000093A
		public static Color DarkCyan
		{
			get
			{
				return Color.FromKnownColor(KnownColor.DarkCyan);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFB8860B.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000023 RID: 35
		// (get) Token: 0x06000037 RID: 55 RVA: 0x00002743 File Offset: 0x00000943
		public static Color DarkGoldenrod
		{
			get
			{
				return Color.FromKnownColor(KnownColor.DarkGoldenrod);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFA9A9A9.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000024 RID: 36
		// (get) Token: 0x06000038 RID: 56 RVA: 0x0000274C File Offset: 0x0000094C
		public static Color DarkGray
		{
			get
			{
				return Color.FromKnownColor(KnownColor.DarkGray);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF006400.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000025 RID: 37
		// (get) Token: 0x06000039 RID: 57 RVA: 0x00002755 File Offset: 0x00000955
		public static Color DarkGreen
		{
			get
			{
				return Color.FromKnownColor(KnownColor.DarkGreen);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFBDB76B.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000026 RID: 38
		// (get) Token: 0x0600003A RID: 58 RVA: 0x0000275E File Offset: 0x0000095E
		public static Color DarkKhaki
		{
			get
			{
				return Color.FromKnownColor(KnownColor.DarkKhaki);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF8B008B.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000027 RID: 39
		// (get) Token: 0x0600003B RID: 59 RVA: 0x00002767 File Offset: 0x00000967
		public static Color DarkMagenta
		{
			get
			{
				return Color.FromKnownColor(KnownColor.DarkMagenta);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF556B2F.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000028 RID: 40
		// (get) Token: 0x0600003C RID: 60 RVA: 0x00002770 File Offset: 0x00000970
		public static Color DarkOliveGreen
		{
			get
			{
				return Color.FromKnownColor(KnownColor.DarkOliveGreen);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFF8C00.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000029 RID: 41
		// (get) Token: 0x0600003D RID: 61 RVA: 0x00002779 File Offset: 0x00000979
		public static Color DarkOrange
		{
			get
			{
				return Color.FromKnownColor(KnownColor.DarkOrange);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF9932CC.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700002A RID: 42
		// (get) Token: 0x0600003E RID: 62 RVA: 0x00002782 File Offset: 0x00000982
		public static Color DarkOrchid
		{
			get
			{
				return Color.FromKnownColor(KnownColor.DarkOrchid);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF8B0000.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700002B RID: 43
		// (get) Token: 0x0600003F RID: 63 RVA: 0x0000278B File Offset: 0x0000098B
		public static Color DarkRed
		{
			get
			{
				return Color.FromKnownColor(KnownColor.DarkRed);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFE9967A.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700002C RID: 44
		// (get) Token: 0x06000040 RID: 64 RVA: 0x00002794 File Offset: 0x00000994
		public static Color DarkSalmon
		{
			get
			{
				return Color.FromKnownColor(KnownColor.DarkSalmon);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF8FBC8F.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700002D RID: 45
		// (get) Token: 0x06000041 RID: 65 RVA: 0x0000279D File Offset: 0x0000099D
		public static Color DarkSeaGreen
		{
			get
			{
				return Color.FromKnownColor(KnownColor.DarkSeaGreen);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF483D8B.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700002E RID: 46
		// (get) Token: 0x06000042 RID: 66 RVA: 0x000027A6 File Offset: 0x000009A6
		public static Color DarkSlateBlue
		{
			get
			{
				return Color.FromKnownColor(KnownColor.DarkSlateBlue);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF2F4F4F.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700002F RID: 47
		// (get) Token: 0x06000043 RID: 67 RVA: 0x000027AF File Offset: 0x000009AF
		public static Color DarkSlateGray
		{
			get
			{
				return Color.FromKnownColor(KnownColor.DarkSlateGray);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF00CED1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000030 RID: 48
		// (get) Token: 0x06000044 RID: 68 RVA: 0x000027B8 File Offset: 0x000009B8
		public static Color DarkTurquoise
		{
			get
			{
				return Color.FromKnownColor(KnownColor.DarkTurquoise);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF9400D3.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000031 RID: 49
		// (get) Token: 0x06000045 RID: 69 RVA: 0x000027C1 File Offset: 0x000009C1
		public static Color DarkViolet
		{
			get
			{
				return Color.FromKnownColor(KnownColor.DarkViolet);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFF1493.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000032 RID: 50
		// (get) Token: 0x06000046 RID: 70 RVA: 0x000027CA File Offset: 0x000009CA
		public static Color DeepPink
		{
			get
			{
				return Color.FromKnownColor(KnownColor.DeepPink);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF00BFFF.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000033 RID: 51
		// (get) Token: 0x06000047 RID: 71 RVA: 0x000027D3 File Offset: 0x000009D3
		public static Color DeepSkyBlue
		{
			get
			{
				return Color.FromKnownColor(KnownColor.DeepSkyBlue);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF696969.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000034 RID: 52
		// (get) Token: 0x06000048 RID: 72 RVA: 0x000027DC File Offset: 0x000009DC
		public static Color DimGray
		{
			get
			{
				return Color.FromKnownColor(KnownColor.DimGray);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF1E90FF.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000035 RID: 53
		// (get) Token: 0x06000049 RID: 73 RVA: 0x000027E5 File Offset: 0x000009E5
		public static Color DodgerBlue
		{
			get
			{
				return Color.FromKnownColor(KnownColor.DodgerBlue);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFB22222.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000036 RID: 54
		// (get) Token: 0x0600004A RID: 74 RVA: 0x000027EE File Offset: 0x000009EE
		public static Color Firebrick
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Firebrick);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFFFAF0.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000037 RID: 55
		// (get) Token: 0x0600004B RID: 75 RVA: 0x000027F7 File Offset: 0x000009F7
		public static Color FloralWhite
		{
			get
			{
				return Color.FromKnownColor(KnownColor.FloralWhite);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF228B22.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000038 RID: 56
		// (get) Token: 0x0600004C RID: 76 RVA: 0x00002800 File Offset: 0x00000A00
		public static Color ForestGreen
		{
			get
			{
				return Color.FromKnownColor(KnownColor.ForestGreen);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFF00FF.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000039 RID: 57
		// (get) Token: 0x0600004D RID: 77 RVA: 0x00002809 File Offset: 0x00000A09
		public static Color Fuchsia
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Fuchsia);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFDCDCDC.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700003A RID: 58
		// (get) Token: 0x0600004E RID: 78 RVA: 0x00002812 File Offset: 0x00000A12
		public static Color Gainsboro
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Gainsboro);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFF8F8FF.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700003B RID: 59
		// (get) Token: 0x0600004F RID: 79 RVA: 0x0000281B File Offset: 0x00000A1B
		public static Color GhostWhite
		{
			get
			{
				return Color.FromKnownColor(KnownColor.GhostWhite);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFFD700.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700003C RID: 60
		// (get) Token: 0x06000050 RID: 80 RVA: 0x00002824 File Offset: 0x00000A24
		public static Color Gold
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Gold);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFDAA520.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700003D RID: 61
		// (get) Token: 0x06000051 RID: 81 RVA: 0x0000282D File Offset: 0x00000A2D
		public static Color Goldenrod
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Goldenrod);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF808080.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> strcture representing a system-defined color.</returns>
		// Token: 0x1700003E RID: 62
		// (get) Token: 0x06000052 RID: 82 RVA: 0x00002836 File Offset: 0x00000A36
		public static Color Gray
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Gray);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF008000.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700003F RID: 63
		// (get) Token: 0x06000053 RID: 83 RVA: 0x0000283F File Offset: 0x00000A3F
		public static Color Green
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Green);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFADFF2F.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000040 RID: 64
		// (get) Token: 0x06000054 RID: 84 RVA: 0x00002848 File Offset: 0x00000A48
		public static Color GreenYellow
		{
			get
			{
				return Color.FromKnownColor(KnownColor.GreenYellow);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFF0FFF0.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000041 RID: 65
		// (get) Token: 0x06000055 RID: 85 RVA: 0x00002851 File Offset: 0x00000A51
		public static Color Honeydew
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Honeydew);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFF69B4.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000042 RID: 66
		// (get) Token: 0x06000056 RID: 86 RVA: 0x0000285A File Offset: 0x00000A5A
		public static Color HotPink
		{
			get
			{
				return Color.FromKnownColor(KnownColor.HotPink);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFCD5C5C.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000043 RID: 67
		// (get) Token: 0x06000057 RID: 87 RVA: 0x00002863 File Offset: 0x00000A63
		public static Color IndianRed
		{
			get
			{
				return Color.FromKnownColor(KnownColor.IndianRed);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF4B0082.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000044 RID: 68
		// (get) Token: 0x06000058 RID: 88 RVA: 0x0000286C File Offset: 0x00000A6C
		public static Color Indigo
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Indigo);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFFFFF0.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000045 RID: 69
		// (get) Token: 0x06000059 RID: 89 RVA: 0x00002875 File Offset: 0x00000A75
		public static Color Ivory
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Ivory);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFF0E68C.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000046 RID: 70
		// (get) Token: 0x0600005A RID: 90 RVA: 0x0000287E File Offset: 0x00000A7E
		public static Color Khaki
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Khaki);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFE6E6FA.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000047 RID: 71
		// (get) Token: 0x0600005B RID: 91 RVA: 0x00002887 File Offset: 0x00000A87
		public static Color Lavender
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Lavender);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFFF0F5.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000048 RID: 72
		// (get) Token: 0x0600005C RID: 92 RVA: 0x00002890 File Offset: 0x00000A90
		public static Color LavenderBlush
		{
			get
			{
				return Color.FromKnownColor(KnownColor.LavenderBlush);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF7CFC00.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000049 RID: 73
		// (get) Token: 0x0600005D RID: 93 RVA: 0x00002899 File Offset: 0x00000A99
		public static Color LawnGreen
		{
			get
			{
				return Color.FromKnownColor(KnownColor.LawnGreen);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFFFACD.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700004A RID: 74
		// (get) Token: 0x0600005E RID: 94 RVA: 0x000028A2 File Offset: 0x00000AA2
		public static Color LemonChiffon
		{
			get
			{
				return Color.FromKnownColor(KnownColor.LemonChiffon);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFADD8E6.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700004B RID: 75
		// (get) Token: 0x0600005F RID: 95 RVA: 0x000028AB File Offset: 0x00000AAB
		public static Color LightBlue
		{
			get
			{
				return Color.FromKnownColor(KnownColor.LightBlue);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFF08080.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700004C RID: 76
		// (get) Token: 0x06000060 RID: 96 RVA: 0x000028B4 File Offset: 0x00000AB4
		public static Color LightCoral
		{
			get
			{
				return Color.FromKnownColor(KnownColor.LightCoral);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFE0FFFF.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700004D RID: 77
		// (get) Token: 0x06000061 RID: 97 RVA: 0x000028BD File Offset: 0x00000ABD
		public static Color LightCyan
		{
			get
			{
				return Color.FromKnownColor(KnownColor.LightCyan);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFAFAD2.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700004E RID: 78
		// (get) Token: 0x06000062 RID: 98 RVA: 0x000028C6 File Offset: 0x00000AC6
		public static Color LightGoldenrodYellow
		{
			get
			{
				return Color.FromKnownColor(KnownColor.LightGoldenrodYellow);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF90EE90.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700004F RID: 79
		// (get) Token: 0x06000063 RID: 99 RVA: 0x000028CF File Offset: 0x00000ACF
		public static Color LightGreen
		{
			get
			{
				return Color.FromKnownColor(KnownColor.LightGreen);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFD3D3D3.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000050 RID: 80
		// (get) Token: 0x06000064 RID: 100 RVA: 0x000028D8 File Offset: 0x00000AD8
		public static Color LightGray
		{
			get
			{
				return Color.FromKnownColor(KnownColor.LightGray);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFFB6C1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000051 RID: 81
		// (get) Token: 0x06000065 RID: 101 RVA: 0x000028E1 File Offset: 0x00000AE1
		public static Color LightPink
		{
			get
			{
				return Color.FromKnownColor(KnownColor.LightPink);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFFA07A.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000052 RID: 82
		// (get) Token: 0x06000066 RID: 102 RVA: 0x000028EA File Offset: 0x00000AEA
		public static Color LightSalmon
		{
			get
			{
				return Color.FromKnownColor(KnownColor.LightSalmon);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF20B2AA.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000053 RID: 83
		// (get) Token: 0x06000067 RID: 103 RVA: 0x000028F3 File Offset: 0x00000AF3
		public static Color LightSeaGreen
		{
			get
			{
				return Color.FromKnownColor(KnownColor.LightSeaGreen);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF87CEFA.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000054 RID: 84
		// (get) Token: 0x06000068 RID: 104 RVA: 0x000028FC File Offset: 0x00000AFC
		public static Color LightSkyBlue
		{
			get
			{
				return Color.FromKnownColor(KnownColor.LightSkyBlue);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF778899.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000055 RID: 85
		// (get) Token: 0x06000069 RID: 105 RVA: 0x00002905 File Offset: 0x00000B05
		public static Color LightSlateGray
		{
			get
			{
				return Color.FromKnownColor(KnownColor.LightSlateGray);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFB0C4DE.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000056 RID: 86
		// (get) Token: 0x0600006A RID: 106 RVA: 0x0000290E File Offset: 0x00000B0E
		public static Color LightSteelBlue
		{
			get
			{
				return Color.FromKnownColor(KnownColor.LightSteelBlue);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFFFFE0.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000057 RID: 87
		// (get) Token: 0x0600006B RID: 107 RVA: 0x00002917 File Offset: 0x00000B17
		public static Color LightYellow
		{
			get
			{
				return Color.FromKnownColor(KnownColor.LightYellow);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF00FF00.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000058 RID: 88
		// (get) Token: 0x0600006C RID: 108 RVA: 0x00002920 File Offset: 0x00000B20
		public static Color Lime
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Lime);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF32CD32.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000059 RID: 89
		// (get) Token: 0x0600006D RID: 109 RVA: 0x00002929 File Offset: 0x00000B29
		public static Color LimeGreen
		{
			get
			{
				return Color.FromKnownColor(KnownColor.LimeGreen);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFAF0E6.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700005A RID: 90
		// (get) Token: 0x0600006E RID: 110 RVA: 0x00002932 File Offset: 0x00000B32
		public static Color Linen
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Linen);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFF00FF.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700005B RID: 91
		// (get) Token: 0x0600006F RID: 111 RVA: 0x0000293B File Offset: 0x00000B3B
		public static Color Magenta
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Magenta);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF800000.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700005C RID: 92
		// (get) Token: 0x06000070 RID: 112 RVA: 0x00002944 File Offset: 0x00000B44
		public static Color Maroon
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Maroon);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF66CDAA.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700005D RID: 93
		// (get) Token: 0x06000071 RID: 113 RVA: 0x0000294D File Offset: 0x00000B4D
		public static Color MediumAquamarine
		{
			get
			{
				return Color.FromKnownColor(KnownColor.MediumAquamarine);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF0000CD.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700005E RID: 94
		// (get) Token: 0x06000072 RID: 114 RVA: 0x00002956 File Offset: 0x00000B56
		public static Color MediumBlue
		{
			get
			{
				return Color.FromKnownColor(KnownColor.MediumBlue);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFBA55D3.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700005F RID: 95
		// (get) Token: 0x06000073 RID: 115 RVA: 0x0000295F File Offset: 0x00000B5F
		public static Color MediumOrchid
		{
			get
			{
				return Color.FromKnownColor(KnownColor.MediumOrchid);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF9370DB.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000060 RID: 96
		// (get) Token: 0x06000074 RID: 116 RVA: 0x00002968 File Offset: 0x00000B68
		public static Color MediumPurple
		{
			get
			{
				return Color.FromKnownColor(KnownColor.MediumPurple);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF3CB371.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000061 RID: 97
		// (get) Token: 0x06000075 RID: 117 RVA: 0x00002971 File Offset: 0x00000B71
		public static Color MediumSeaGreen
		{
			get
			{
				return Color.FromKnownColor(KnownColor.MediumSeaGreen);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF7B68EE.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000062 RID: 98
		// (get) Token: 0x06000076 RID: 118 RVA: 0x0000297A File Offset: 0x00000B7A
		public static Color MediumSlateBlue
		{
			get
			{
				return Color.FromKnownColor(KnownColor.MediumSlateBlue);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF00FA9A.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000063 RID: 99
		// (get) Token: 0x06000077 RID: 119 RVA: 0x00002983 File Offset: 0x00000B83
		public static Color MediumSpringGreen
		{
			get
			{
				return Color.FromKnownColor(KnownColor.MediumSpringGreen);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF48D1CC.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000064 RID: 100
		// (get) Token: 0x06000078 RID: 120 RVA: 0x0000298C File Offset: 0x00000B8C
		public static Color MediumTurquoise
		{
			get
			{
				return Color.FromKnownColor(KnownColor.MediumTurquoise);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFC71585.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000065 RID: 101
		// (get) Token: 0x06000079 RID: 121 RVA: 0x00002995 File Offset: 0x00000B95
		public static Color MediumVioletRed
		{
			get
			{
				return Color.FromKnownColor(KnownColor.MediumVioletRed);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF191970.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000066 RID: 102
		// (get) Token: 0x0600007A RID: 122 RVA: 0x0000299E File Offset: 0x00000B9E
		public static Color MidnightBlue
		{
			get
			{
				return Color.FromKnownColor(KnownColor.MidnightBlue);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFF5FFFA.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000067 RID: 103
		// (get) Token: 0x0600007B RID: 123 RVA: 0x000029A7 File Offset: 0x00000BA7
		public static Color MintCream
		{
			get
			{
				return Color.FromKnownColor(KnownColor.MintCream);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFFE4E1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000068 RID: 104
		// (get) Token: 0x0600007C RID: 124 RVA: 0x000029B0 File Offset: 0x00000BB0
		public static Color MistyRose
		{
			get
			{
				return Color.FromKnownColor(KnownColor.MistyRose);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFFE4B5.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000069 RID: 105
		// (get) Token: 0x0600007D RID: 125 RVA: 0x000029B9 File Offset: 0x00000BB9
		public static Color Moccasin
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Moccasin);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFFDEAD.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700006A RID: 106
		// (get) Token: 0x0600007E RID: 126 RVA: 0x000029C2 File Offset: 0x00000BC2
		public static Color NavajoWhite
		{
			get
			{
				return Color.FromKnownColor(KnownColor.NavajoWhite);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF000080.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700006B RID: 107
		// (get) Token: 0x0600007F RID: 127 RVA: 0x000029CB File Offset: 0x00000BCB
		public static Color Navy
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Navy);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFDF5E6.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700006C RID: 108
		// (get) Token: 0x06000080 RID: 128 RVA: 0x000029D4 File Offset: 0x00000BD4
		public static Color OldLace
		{
			get
			{
				return Color.FromKnownColor(KnownColor.OldLace);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF808000.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700006D RID: 109
		// (get) Token: 0x06000081 RID: 129 RVA: 0x000029DD File Offset: 0x00000BDD
		public static Color Olive
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Olive);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF6B8E23.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700006E RID: 110
		// (get) Token: 0x06000082 RID: 130 RVA: 0x000029E6 File Offset: 0x00000BE6
		public static Color OliveDrab
		{
			get
			{
				return Color.FromKnownColor(KnownColor.OliveDrab);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFFA500.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700006F RID: 111
		// (get) Token: 0x06000083 RID: 131 RVA: 0x000029EF File Offset: 0x00000BEF
		public static Color Orange
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Orange);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFF4500.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000070 RID: 112
		// (get) Token: 0x06000084 RID: 132 RVA: 0x000029F8 File Offset: 0x00000BF8
		public static Color OrangeRed
		{
			get
			{
				return Color.FromKnownColor(KnownColor.OrangeRed);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFDA70D6.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000071 RID: 113
		// (get) Token: 0x06000085 RID: 133 RVA: 0x00002A04 File Offset: 0x00000C04
		public static Color Orchid
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Orchid);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFEEE8AA.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000072 RID: 114
		// (get) Token: 0x06000086 RID: 134 RVA: 0x00002A10 File Offset: 0x00000C10
		public static Color PaleGoldenrod
		{
			get
			{
				return Color.FromKnownColor(KnownColor.PaleGoldenrod);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF98FB98.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000073 RID: 115
		// (get) Token: 0x06000087 RID: 135 RVA: 0x00002A1C File Offset: 0x00000C1C
		public static Color PaleGreen
		{
			get
			{
				return Color.FromKnownColor(KnownColor.PaleGreen);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFAFEEEE.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000074 RID: 116
		// (get) Token: 0x06000088 RID: 136 RVA: 0x00002A28 File Offset: 0x00000C28
		public static Color PaleTurquoise
		{
			get
			{
				return Color.FromKnownColor(KnownColor.PaleTurquoise);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFDB7093.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000075 RID: 117
		// (get) Token: 0x06000089 RID: 137 RVA: 0x00002A34 File Offset: 0x00000C34
		public static Color PaleVioletRed
		{
			get
			{
				return Color.FromKnownColor(KnownColor.PaleVioletRed);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFFEFD5.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000076 RID: 118
		// (get) Token: 0x0600008A RID: 138 RVA: 0x00002A40 File Offset: 0x00000C40
		public static Color PapayaWhip
		{
			get
			{
				return Color.FromKnownColor(KnownColor.PapayaWhip);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFFDAB9.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000077 RID: 119
		// (get) Token: 0x0600008B RID: 139 RVA: 0x00002A4C File Offset: 0x00000C4C
		public static Color PeachPuff
		{
			get
			{
				return Color.FromKnownColor(KnownColor.PeachPuff);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFCD853F.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000078 RID: 120
		// (get) Token: 0x0600008C RID: 140 RVA: 0x00002A58 File Offset: 0x00000C58
		public static Color Peru
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Peru);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFFC0CB.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000079 RID: 121
		// (get) Token: 0x0600008D RID: 141 RVA: 0x00002A64 File Offset: 0x00000C64
		public static Color Pink
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Pink);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFDDA0DD.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700007A RID: 122
		// (get) Token: 0x0600008E RID: 142 RVA: 0x00002A70 File Offset: 0x00000C70
		public static Color Plum
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Plum);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFB0E0E6.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700007B RID: 123
		// (get) Token: 0x0600008F RID: 143 RVA: 0x00002A7C File Offset: 0x00000C7C
		public static Color PowderBlue
		{
			get
			{
				return Color.FromKnownColor(KnownColor.PowderBlue);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF800080.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700007C RID: 124
		// (get) Token: 0x06000090 RID: 144 RVA: 0x00002A88 File Offset: 0x00000C88
		public static Color Purple
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Purple);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFF0000.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700007D RID: 125
		// (get) Token: 0x06000091 RID: 145 RVA: 0x00002A94 File Offset: 0x00000C94
		public static Color Red
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Red);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFBC8F8F.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700007E RID: 126
		// (get) Token: 0x06000092 RID: 146 RVA: 0x00002AA0 File Offset: 0x00000CA0
		public static Color RosyBrown
		{
			get
			{
				return Color.FromKnownColor(KnownColor.RosyBrown);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF4169E1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700007F RID: 127
		// (get) Token: 0x06000093 RID: 147 RVA: 0x00002AAC File Offset: 0x00000CAC
		public static Color RoyalBlue
		{
			get
			{
				return Color.FromKnownColor(KnownColor.RoyalBlue);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF8B4513.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000080 RID: 128
		// (get) Token: 0x06000094 RID: 148 RVA: 0x00002AB8 File Offset: 0x00000CB8
		public static Color SaddleBrown
		{
			get
			{
				return Color.FromKnownColor(KnownColor.SaddleBrown);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFA8072.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000081 RID: 129
		// (get) Token: 0x06000095 RID: 149 RVA: 0x00002AC4 File Offset: 0x00000CC4
		public static Color Salmon
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Salmon);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFF4A460.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000082 RID: 130
		// (get) Token: 0x06000096 RID: 150 RVA: 0x00002AD0 File Offset: 0x00000CD0
		public static Color SandyBrown
		{
			get
			{
				return Color.FromKnownColor(KnownColor.SandyBrown);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF2E8B57.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000083 RID: 131
		// (get) Token: 0x06000097 RID: 151 RVA: 0x00002ADC File Offset: 0x00000CDC
		public static Color SeaGreen
		{
			get
			{
				return Color.FromKnownColor(KnownColor.SeaGreen);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFFF5EE.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000084 RID: 132
		// (get) Token: 0x06000098 RID: 152 RVA: 0x00002AE8 File Offset: 0x00000CE8
		public static Color SeaShell
		{
			get
			{
				return Color.FromKnownColor(KnownColor.SeaShell);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFA0522D.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000085 RID: 133
		// (get) Token: 0x06000099 RID: 153 RVA: 0x00002AF4 File Offset: 0x00000CF4
		public static Color Sienna
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Sienna);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFC0C0C0.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000086 RID: 134
		// (get) Token: 0x0600009A RID: 154 RVA: 0x00002B00 File Offset: 0x00000D00
		public static Color Silver
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Silver);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF87CEEB.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000087 RID: 135
		// (get) Token: 0x0600009B RID: 155 RVA: 0x00002B0C File Offset: 0x00000D0C
		public static Color SkyBlue
		{
			get
			{
				return Color.FromKnownColor(KnownColor.SkyBlue);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF6A5ACD.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000088 RID: 136
		// (get) Token: 0x0600009C RID: 156 RVA: 0x00002B18 File Offset: 0x00000D18
		public static Color SlateBlue
		{
			get
			{
				return Color.FromKnownColor(KnownColor.SlateBlue);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF708090.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000089 RID: 137
		// (get) Token: 0x0600009D RID: 157 RVA: 0x00002B24 File Offset: 0x00000D24
		public static Color SlateGray
		{
			get
			{
				return Color.FromKnownColor(KnownColor.SlateGray);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFFFAFA.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700008A RID: 138
		// (get) Token: 0x0600009E RID: 158 RVA: 0x00002B30 File Offset: 0x00000D30
		public static Color Snow
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Snow);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF00FF7F.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700008B RID: 139
		// (get) Token: 0x0600009F RID: 159 RVA: 0x00002B3C File Offset: 0x00000D3C
		public static Color SpringGreen
		{
			get
			{
				return Color.FromKnownColor(KnownColor.SpringGreen);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF4682B4.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700008C RID: 140
		// (get) Token: 0x060000A0 RID: 160 RVA: 0x00002B48 File Offset: 0x00000D48
		public static Color SteelBlue
		{
			get
			{
				return Color.FromKnownColor(KnownColor.SteelBlue);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFD2B48C.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700008D RID: 141
		// (get) Token: 0x060000A1 RID: 161 RVA: 0x00002B54 File Offset: 0x00000D54
		public static Color Tan
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Tan);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF008080.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700008E RID: 142
		// (get) Token: 0x060000A2 RID: 162 RVA: 0x00002B60 File Offset: 0x00000D60
		public static Color Teal
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Teal);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFD8BFD8.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x1700008F RID: 143
		// (get) Token: 0x060000A3 RID: 163 RVA: 0x00002B6C File Offset: 0x00000D6C
		public static Color Thistle
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Thistle);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFF6347.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000090 RID: 144
		// (get) Token: 0x060000A4 RID: 164 RVA: 0x00002B78 File Offset: 0x00000D78
		public static Color Tomato
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Tomato);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF40E0D0.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000091 RID: 145
		// (get) Token: 0x060000A5 RID: 165 RVA: 0x00002B84 File Offset: 0x00000D84
		public static Color Turquoise
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Turquoise);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFEE82EE.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000092 RID: 146
		// (get) Token: 0x060000A6 RID: 166 RVA: 0x00002B90 File Offset: 0x00000D90
		public static Color Violet
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Violet);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFF5DEB3.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000093 RID: 147
		// (get) Token: 0x060000A7 RID: 167 RVA: 0x00002B9C File Offset: 0x00000D9C
		public static Color Wheat
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Wheat);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFFFFFF.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000094 RID: 148
		// (get) Token: 0x060000A8 RID: 168 RVA: 0x00002BA8 File Offset: 0x00000DA8
		public static Color White
		{
			get
			{
				return Color.FromKnownColor(KnownColor.White);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFF5F5F5.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000095 RID: 149
		// (get) Token: 0x060000A9 RID: 169 RVA: 0x00002BB4 File Offset: 0x00000DB4
		public static Color WhiteSmoke
		{
			get
			{
				return Color.FromKnownColor(KnownColor.WhiteSmoke);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FFFFFF00.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000096 RID: 150
		// (get) Token: 0x060000AA RID: 170 RVA: 0x00002BC0 File Offset: 0x00000DC0
		public static Color Yellow
		{
			get
			{
				return Color.FromKnownColor(KnownColor.Yellow);
			}
		}

		/// <summary>Gets a system-defined color that has an ARGB value of #FF9ACD32.</summary>
		/// <returns>A <see cref="T:System.Drawing.Color" /> representing a system-defined color.</returns>
		// Token: 0x17000097 RID: 151
		// (get) Token: 0x060000AB RID: 171 RVA: 0x00002BCC File Offset: 0x00000DCC
		public static Color YellowGreen
		{
			get
			{
				return Color.FromKnownColor(KnownColor.YellowGreen);
			}
		}

		// Token: 0x0400002A RID: 42
		private long value;

		// Token: 0x0400002B RID: 43
		internal short state;

		// Token: 0x0400002C RID: 44
		internal short knownColor;

		// Token: 0x0400002D RID: 45
		internal string name;

		/// <summary>Represents a color that is <see langword="null" />.</summary>
		// Token: 0x0400002E RID: 46
		public static readonly Color Empty;

		// Token: 0x02000004 RID: 4
		[Flags]
		internal enum ColorType : short
		{
			// Token: 0x04000030 RID: 48
			Empty = 0,
			// Token: 0x04000031 RID: 49
			Known = 1,
			// Token: 0x04000032 RID: 50
			ARGB = 2,
			// Token: 0x04000033 RID: 51
			Named = 4,
			// Token: 0x04000034 RID: 52
			System = 8
		}
	}
}
