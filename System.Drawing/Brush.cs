﻿using System;
using Unity;

namespace System.Drawing
{
	/// <summary>Defines objects used to fill the interiors of graphical shapes such as rectangles, ellipses, pies, polygons, and paths.</summary>
	// Token: 0x0200002A RID: 42
	public abstract class Brush : MarshalByRefObject, ICloneable, IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Brush" /> class. </summary>
		// Token: 0x0600038B RID: 907 RVA: 0x00004644 File Offset: 0x00002844
		protected Brush()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>When overridden in a derived class, creates an exact copy of this <see cref="T:System.Drawing.Brush" />.</summary>
		/// <returns>The new <see cref="T:System.Drawing.Brush" /> that this method creates.</returns>
		// Token: 0x0600038C RID: 908
		public abstract object Clone();

		/// <summary>Releases all resources used by this <see cref="T:System.Drawing.Brush" /> object.</summary>
		// Token: 0x0600038D RID: 909 RVA: 0x00004644 File Offset: 0x00002844
		public void Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Drawing.Brush" /> and optionally releases the managed resources. </summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources.</param>
		// Token: 0x0600038E RID: 910 RVA: 0x00004644 File Offset: 0x00002844
		protected virtual void Dispose(bool disposing)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>In a derived class, sets a reference to a GDI+ brush object. </summary>
		/// <param name="brush">A pointer to the GDI+ brush object.</param>
		// Token: 0x0600038F RID: 911 RVA: 0x00004644 File Offset: 0x00002844
		protected internal void SetNativeBrush(IntPtr brush)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
