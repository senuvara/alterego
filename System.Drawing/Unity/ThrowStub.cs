﻿using System;

namespace Unity
{
	// Token: 0x020000C3 RID: 195
	internal sealed class ThrowStub : ObjectDisposedException
	{
		// Token: 0x06000934 RID: 2356 RVA: 0x0000716B File Offset: 0x0000536B
		public static void ThrowNotSupportedException()
		{
			throw new PlatformNotSupportedException();
		}
	}
}
