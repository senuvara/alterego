﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Drawing
{
	/// <summary>The <see cref="T:System.Drawing.SizeConverter" /> class is used to convert from one data type to another. Access this class through the <see cref="T:System.ComponentModel.TypeDescriptor" /> object.</summary>
	// Token: 0x02000080 RID: 128
	public class SizeConverter : TypeConverter
	{
		/// <summary>Initializes a new <see cref="T:System.Drawing.SizeConverter" /> object.</summary>
		// Token: 0x06000718 RID: 1816 RVA: 0x00004644 File Offset: 0x00002844
		public SizeConverter()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
