﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Drawing
{
	/// <summary>
	///     <see cref="T:System.Drawing.ImageFormatConverter" /> is a class that can be used to convert <see cref="T:System.Drawing.Imaging.ImageFormat" /> objects from one data type to another. Access this class through the <see cref="T:System.ComponentModel.TypeDescriptor" /> object.</summary>
	// Token: 0x02000045 RID: 69
	public class ImageFormatConverter : TypeConverter
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.ImageFormatConverter" /> class.</summary>
		// Token: 0x0600041F RID: 1055 RVA: 0x00004644 File Offset: 0x00002844
		public ImageFormatConverter()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
