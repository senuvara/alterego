﻿using System;

namespace System.Drawing
{
	/// <summary>Specifies the alignment of a text string relative to its layout rectangle.</summary>
	// Token: 0x02000022 RID: 34
	public enum StringAlignment
	{
		/// <summary>Specifies that text is aligned in the center of the layout rectangle.</summary>
		// Token: 0x04000216 RID: 534
		Center = 1,
		/// <summary>Specifies that text is aligned far from the origin position of the layout rectangle. In a left-to-right layout, the far position is right. In a right-to-left layout, the far position is left.</summary>
		// Token: 0x04000217 RID: 535
		Far,
		/// <summary>Specifies the text be aligned near the layout. In a left-to-right layout, the near position is left. In a right-to-left layout, the near position is right.</summary>
		// Token: 0x04000218 RID: 536
		Near = 0
	}
}
