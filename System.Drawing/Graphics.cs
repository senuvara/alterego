﻿using System;
using System.ComponentModel;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Security.Permissions;
using Unity;

namespace System.Drawing
{
	/// <summary>Encapsulates a GDI+ drawing surface. This class cannot be inherited.</summary>
	// Token: 0x02000013 RID: 19
	public sealed class Graphics : MarshalByRefObject, IDeviceContext, IDisposable
	{
		// Token: 0x06000166 RID: 358 RVA: 0x00004644 File Offset: 0x00002844
		internal Graphics()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets a <see cref="T:System.Drawing.Region" /> that limits the drawing region of this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.Region" /> that limits the portion of this <see cref="T:System.Drawing.Graphics" /> that is currently available for drawing.</returns>
		// Token: 0x170000BF RID: 191
		// (get) Token: 0x06000167 RID: 359 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000168 RID: 360 RVA: 0x00004644 File Offset: 0x00002844
		public Region Clip
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.RectangleF" /> structure that bounds the clipping region of this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.RectangleF" /> structure that represents a bounding rectangle for the clipping region of this <see cref="T:System.Drawing.Graphics" />.</returns>
		// Token: 0x170000C0 RID: 192
		// (get) Token: 0x06000169 RID: 361 RVA: 0x000046FC File Offset: 0x000028FC
		public RectangleF ClipBounds
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(RectangleF);
			}
		}

		/// <summary>Gets a value that specifies how composited images are drawn to this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <returns>This property specifies a member of the <see cref="T:System.Drawing.Drawing2D.CompositingMode" /> enumeration. The default is <see cref="F:System.Drawing.Drawing2D.CompositingMode.SourceOver" />.</returns>
		// Token: 0x170000C1 RID: 193
		// (get) Token: 0x0600016A RID: 362 RVA: 0x00004718 File Offset: 0x00002918
		// (set) Token: 0x0600016B RID: 363 RVA: 0x00004644 File Offset: 0x00002844
		public CompositingMode CompositingMode
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return CompositingMode.SourceOver;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the rendering quality of composited images drawn to this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <returns>This property specifies a member of the <see cref="T:System.Drawing.Drawing2D.CompositingQuality" /> enumeration. The default is <see cref="F:System.Drawing.Drawing2D.CompositingQuality.Default" />.</returns>
		// Token: 0x170000C2 RID: 194
		// (get) Token: 0x0600016C RID: 364 RVA: 0x00004734 File Offset: 0x00002934
		// (set) Token: 0x0600016D RID: 365 RVA: 0x00004644 File Offset: 0x00002844
		public CompositingQuality CompositingQuality
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return CompositingQuality.Default;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the horizontal resolution of this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <returns>The value, in dots per inch, for the horizontal resolution supported by this <see cref="T:System.Drawing.Graphics" />.</returns>
		// Token: 0x170000C3 RID: 195
		// (get) Token: 0x0600016E RID: 366 RVA: 0x00004750 File Offset: 0x00002950
		public float DpiX
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
		}

		/// <summary>Gets the vertical resolution of this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <returns>The value, in dots per inch, for the vertical resolution supported by this <see cref="T:System.Drawing.Graphics" />.</returns>
		// Token: 0x170000C4 RID: 196
		// (get) Token: 0x0600016F RID: 367 RVA: 0x0000476C File Offset: 0x0000296C
		public float DpiY
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
		}

		/// <summary>Gets or sets the interpolation mode associated with this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <returns>One of the <see cref="T:System.Drawing.Drawing2D.InterpolationMode" /> values.</returns>
		// Token: 0x170000C5 RID: 197
		// (get) Token: 0x06000170 RID: 368 RVA: 0x00004788 File Offset: 0x00002988
		// (set) Token: 0x06000171 RID: 369 RVA: 0x00004644 File Offset: 0x00002844
		public InterpolationMode InterpolationMode
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return InterpolationMode.Default;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a value indicating whether the clipping region of this <see cref="T:System.Drawing.Graphics" /> is empty.</summary>
		/// <returns>
		///     <see langword="true" /> if the clipping region of this <see cref="T:System.Drawing.Graphics" /> is empty; otherwise, <see langword="false" />.</returns>
		// Token: 0x170000C6 RID: 198
		// (get) Token: 0x06000172 RID: 370 RVA: 0x000047A4 File Offset: 0x000029A4
		public bool IsClipEmpty
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value indicating whether the visible clipping region of this <see cref="T:System.Drawing.Graphics" /> is empty.</summary>
		/// <returns>
		///     <see langword="true" /> if the visible portion of the clipping region of this <see cref="T:System.Drawing.Graphics" /> is empty; otherwise, <see langword="false" />.</returns>
		// Token: 0x170000C7 RID: 199
		// (get) Token: 0x06000173 RID: 371 RVA: 0x000047C0 File Offset: 0x000029C0
		public bool IsVisibleClipEmpty
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets or sets the scaling between world units and page units for this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <returns>This property specifies a value for the scaling between world units and page units for this <see cref="T:System.Drawing.Graphics" />.</returns>
		// Token: 0x170000C8 RID: 200
		// (get) Token: 0x06000174 RID: 372 RVA: 0x000047DC File Offset: 0x000029DC
		// (set) Token: 0x06000175 RID: 373 RVA: 0x00004644 File Offset: 0x00002844
		public float PageScale
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the unit of measure used for page coordinates in this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <returns>One of the <see cref="T:System.Drawing.GraphicsUnit" /> values other than <see cref="F:System.Drawing.GraphicsUnit.World" />.</returns>
		/// <exception cref="T:System.ComponentModel.InvalidEnumArgumentException">
		///         <see cref="P:System.Drawing.Graphics.PageUnit" /> is set to <see cref="F:System.Drawing.GraphicsUnit.World" />, which is not a physical unit. </exception>
		// Token: 0x170000C9 RID: 201
		// (get) Token: 0x06000176 RID: 374 RVA: 0x000047F8 File Offset: 0x000029F8
		// (set) Token: 0x06000177 RID: 375 RVA: 0x00004644 File Offset: 0x00002844
		public GraphicsUnit PageUnit
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return GraphicsUnit.World;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or set a value specifying how pixels are offset during rendering of this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <returns>This property specifies a member of the <see cref="T:System.Drawing.Drawing2D.PixelOffsetMode" /> enumeration </returns>
		// Token: 0x170000CA RID: 202
		// (get) Token: 0x06000178 RID: 376 RVA: 0x00004814 File Offset: 0x00002A14
		// (set) Token: 0x06000179 RID: 377 RVA: 0x00004644 File Offset: 0x00002844
		public PixelOffsetMode PixelOffsetMode
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return PixelOffsetMode.Default;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the rendering origin of this <see cref="T:System.Drawing.Graphics" /> for dithering and for hatch brushes.</summary>
		/// <returns>A <see cref="T:System.Drawing.Point" /> structure that represents the dither origin for 8-bits-per-pixel and 16-bits-per-pixel dithering and is also used to set the origin for hatch brushes.</returns>
		// Token: 0x170000CB RID: 203
		// (get) Token: 0x0600017A RID: 378 RVA: 0x00004830 File Offset: 0x00002A30
		// (set) Token: 0x0600017B RID: 379 RVA: 0x00004644 File Offset: 0x00002844
		public Point RenderingOrigin
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Point);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the rendering quality for this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <returns>One of the <see cref="T:System.Drawing.Drawing2D.SmoothingMode" /> values.</returns>
		// Token: 0x170000CC RID: 204
		// (get) Token: 0x0600017C RID: 380 RVA: 0x0000484C File Offset: 0x00002A4C
		// (set) Token: 0x0600017D RID: 381 RVA: 0x00004644 File Offset: 0x00002844
		public SmoothingMode SmoothingMode
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return SmoothingMode.Default;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the gamma correction value for rendering text.</summary>
		/// <returns>The gamma correction value used for rendering antialiased and ClearType text.</returns>
		// Token: 0x170000CD RID: 205
		// (get) Token: 0x0600017E RID: 382 RVA: 0x00004868 File Offset: 0x00002A68
		// (set) Token: 0x0600017F RID: 383 RVA: 0x00004644 File Offset: 0x00002844
		public int TextContrast
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the rendering mode for text associated with this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <returns>One of the <see cref="T:System.Drawing.Text.TextRenderingHint" /> values.</returns>
		// Token: 0x170000CE RID: 206
		// (get) Token: 0x06000180 RID: 384 RVA: 0x00004884 File Offset: 0x00002A84
		// (set) Token: 0x06000181 RID: 385 RVA: 0x00004644 File Offset: 0x00002844
		public TextRenderingHint TextRenderingHint
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return TextRenderingHint.SystemDefault;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a copy of the geometric world transformation for this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <returns>A copy of the <see cref="T:System.Drawing.Drawing2D.Matrix" /> that represents the geometric world transformation for this <see cref="T:System.Drawing.Graphics" />.</returns>
		// Token: 0x170000CF RID: 207
		// (get) Token: 0x06000182 RID: 386 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000183 RID: 387 RVA: 0x00004644 File Offset: 0x00002844
		public Matrix Transform
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the bounding rectangle of the visible clipping region of this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.RectangleF" /> structure that represents a bounding rectangle for the visible clipping region of this <see cref="T:System.Drawing.Graphics" />.</returns>
		// Token: 0x170000D0 RID: 208
		// (get) Token: 0x06000184 RID: 388 RVA: 0x000048A0 File Offset: 0x00002AA0
		public RectangleF VisibleClipBounds
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(RectangleF);
			}
		}

		/// <summary>Adds a comment to the current <see cref="T:System.Drawing.Imaging.Metafile" />.</summary>
		/// <param name="data">Array of bytes that contains the comment. </param>
		// Token: 0x06000185 RID: 389 RVA: 0x00004644 File Offset: 0x00002844
		public void AddMetafileComment(byte[] data)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Saves a graphics container with the current state of this <see cref="T:System.Drawing.Graphics" /> and opens and uses a new graphics container.</summary>
		/// <returns>This method returns a <see cref="T:System.Drawing.Drawing2D.GraphicsContainer" /> that represents the state of this <see cref="T:System.Drawing.Graphics" /> at the time of the method call.</returns>
		// Token: 0x06000186 RID: 390 RVA: 0x00004667 File Offset: 0x00002867
		public GraphicsContainer BeginContainer()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Saves a graphics container with the current state of this <see cref="T:System.Drawing.Graphics" /> and opens and uses a new graphics container with the specified scale transformation.</summary>
		/// <param name="dstrect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that, together with the <paramref name="srcrect" /> parameter, specifies a scale transformation for the container. </param>
		/// <param name="srcrect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that, together with the <paramref name="dstrect" /> parameter, specifies a scale transformation for the container. </param>
		/// <param name="unit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the unit of measure for the container. </param>
		/// <returns>This method returns a <see cref="T:System.Drawing.Drawing2D.GraphicsContainer" /> that represents the state of this <see cref="T:System.Drawing.Graphics" /> at the time of the method call.</returns>
		// Token: 0x06000187 RID: 391 RVA: 0x00004667 File Offset: 0x00002867
		public GraphicsContainer BeginContainer(Rectangle dstrect, Rectangle srcrect, GraphicsUnit unit)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Saves a graphics container with the current state of this <see cref="T:System.Drawing.Graphics" /> and opens and uses a new graphics container with the specified scale transformation.</summary>
		/// <param name="dstrect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that, together with the <paramref name="srcrect" /> parameter, specifies a scale transformation for the new graphics container. </param>
		/// <param name="srcrect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that, together with the <paramref name="dstrect" /> parameter, specifies a scale transformation for the new graphics container. </param>
		/// <param name="unit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the unit of measure for the container. </param>
		/// <returns>This method returns a <see cref="T:System.Drawing.Drawing2D.GraphicsContainer" /> that represents the state of this <see cref="T:System.Drawing.Graphics" /> at the time of the method call.</returns>
		// Token: 0x06000188 RID: 392 RVA: 0x00004667 File Offset: 0x00002867
		public GraphicsContainer BeginContainer(RectangleF dstrect, RectangleF srcrect, GraphicsUnit unit)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Clears the entire drawing surface and fills it with the specified background color.</summary>
		/// <param name="color">
		///       <see cref="T:System.Drawing.Color" /> structure that represents the background color of the drawing surface. </param>
		// Token: 0x06000189 RID: 393 RVA: 0x00004644 File Offset: 0x00002844
		public void Clear(Color color)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Performs a bit-block transfer of color data, corresponding to a rectangle of pixels, from the screen to the drawing surface of the <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="upperLeftSource">The point at the upper-left corner of the source rectangle.</param>
		/// <param name="upperLeftDestination">The point at the upper-left corner of the destination rectangle.</param>
		/// <param name="blockRegionSize">The size of the area to be transferred.</param>
		/// <exception cref="T:System.ComponentModel.Win32Exception">The operation failed.</exception>
		// Token: 0x0600018A RID: 394 RVA: 0x00004644 File Offset: 0x00002844
		public void CopyFromScreen(Point upperLeftSource, Point upperLeftDestination, Size blockRegionSize)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Performs a bit-block transfer of color data, corresponding to a rectangle of pixels, from the screen to the drawing surface of the <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="upperLeftSource">The point at the upper-left corner of the source rectangle.</param>
		/// <param name="upperLeftDestination">The point at the upper-left corner of the destination rectangle.</param>
		/// <param name="blockRegionSize">The size of the area to be transferred.</param>
		/// <param name="copyPixelOperation">One of the <see cref="T:System.Drawing.CopyPixelOperation" /> values.</param>
		/// <exception cref="T:System.ComponentModel.InvalidEnumArgumentException">
		///         <paramref name="copyPixelOperation" /> is not a member of <see cref="T:System.Drawing.CopyPixelOperation" />.</exception>
		/// <exception cref="T:System.ComponentModel.Win32Exception">The operation failed.</exception>
		// Token: 0x0600018B RID: 395 RVA: 0x00004644 File Offset: 0x00002844
		public void CopyFromScreen(Point upperLeftSource, Point upperLeftDestination, Size blockRegionSize, CopyPixelOperation copyPixelOperation)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Performs a bit-block transfer of the color data, corresponding to a rectangle of pixels, from the screen to the drawing surface of the <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="sourceX">The x-coordinate of the point at the upper-left corner of the source rectangle.</param>
		/// <param name="sourceY">The y-coordinate of the point at the upper-left corner of the source rectangle.</param>
		/// <param name="destinationX">The x-coordinate of the point at the upper-left corner of the destination rectangle.</param>
		/// <param name="destinationY">The y-coordinate of the point at the upper-left corner of the destination rectangle.</param>
		/// <param name="blockRegionSize">The size of the area to be transferred.</param>
		/// <exception cref="T:System.ComponentModel.Win32Exception">The operation failed.</exception>
		// Token: 0x0600018C RID: 396 RVA: 0x00004644 File Offset: 0x00002844
		public void CopyFromScreen(int sourceX, int sourceY, int destinationX, int destinationY, Size blockRegionSize)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Performs a bit-block transfer of the color data, corresponding to a rectangle of pixels, from the screen to the drawing surface of the <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="sourceX">The x-coordinate of the point at the upper-left corner of the source rectangle.</param>
		/// <param name="sourceY">The y-coordinate of the point at the upper-left corner of the source rectangle</param>
		/// <param name="destinationX">The x-coordinate of the point at the upper-left corner of the destination rectangle.</param>
		/// <param name="destinationY">The y-coordinate of the point at the upper-left corner of the destination rectangle.</param>
		/// <param name="blockRegionSize">The size of the area to be transferred.</param>
		/// <param name="copyPixelOperation">One of the <see cref="T:System.Drawing.CopyPixelOperation" /> values.</param>
		/// <exception cref="T:System.ComponentModel.InvalidEnumArgumentException">
		///         <paramref name="copyPixelOperation" /> is not a member of <see cref="T:System.Drawing.CopyPixelOperation" />.</exception>
		/// <exception cref="T:System.ComponentModel.Win32Exception">The operation failed.</exception>
		// Token: 0x0600018D RID: 397 RVA: 0x00004644 File Offset: 0x00002844
		public void CopyFromScreen(int sourceX, int sourceY, int destinationX, int destinationY, Size blockRegionSize, CopyPixelOperation copyPixelOperation)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Releases all resources used by this <see cref="T:System.Drawing.Graphics" />.</summary>
		// Token: 0x0600018E RID: 398 RVA: 0x00004644 File Offset: 0x00002844
		public void Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws an arc representing a portion of an ellipse specified by a <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the arc. </param>
		/// <param name="rect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that defines the boundaries of the ellipse. </param>
		/// <param name="startAngle">Angle in degrees measured clockwise from the x-axis to the starting point of the arc. </param>
		/// <param name="sweepAngle">Angle in degrees measured clockwise from the <paramref name="startAngle" /> parameter to ending point of the arc. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.</exception>
		// Token: 0x0600018F RID: 399 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawArc(Pen pen, Rectangle rect, float startAngle, float sweepAngle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws an arc representing a portion of an ellipse specified by a <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the arc. </param>
		/// <param name="rect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that defines the boundaries of the ellipse. </param>
		/// <param name="startAngle">Angle in degrees measured clockwise from the x-axis to the starting point of the arc. </param>
		/// <param name="sweepAngle">Angle in degrees measured clockwise from the <paramref name="startAngle" /> parameter to ending point of the arc. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" /></exception>
		// Token: 0x06000190 RID: 400 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawArc(Pen pen, RectangleF rect, float startAngle, float sweepAngle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws an arc representing a portion of an ellipse specified by a pair of coordinates, a width, and a height.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the arc. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the rectangle that defines the ellipse. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the rectangle that defines the ellipse. </param>
		/// <param name="width">Width of the rectangle that defines the ellipse. </param>
		/// <param name="height">Height of the rectangle that defines the ellipse. </param>
		/// <param name="startAngle">Angle in degrees measured clockwise from the x-axis to the starting point of the arc. </param>
		/// <param name="sweepAngle">Angle in degrees measured clockwise from the <paramref name="startAngle" /> parameter to ending point of the arc. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.-or-
		///         <paramref name="rects" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="rects" /> is a zero-length array.</exception>
		// Token: 0x06000191 RID: 401 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawArc(Pen pen, int x, int y, int width, int height, int startAngle, int sweepAngle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws an arc representing a portion of an ellipse specified by a pair of coordinates, a width, and a height.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the arc. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the rectangle that defines the ellipse. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the rectangle that defines the ellipse. </param>
		/// <param name="width">Width of the rectangle that defines the ellipse. </param>
		/// <param name="height">Height of the rectangle that defines the ellipse. </param>
		/// <param name="startAngle">Angle in degrees measured clockwise from the x-axis to the starting point of the arc. </param>
		/// <param name="sweepAngle">Angle in degrees measured clockwise from the <paramref name="startAngle" /> parameter to ending point of the arc. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.</exception>
		// Token: 0x06000192 RID: 402 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawArc(Pen pen, float x, float y, float width, float height, float startAngle, float sweepAngle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a Bézier spline defined by four <see cref="T:System.Drawing.Point" /> structures.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> structure that determines the color, width, and style of the curve. </param>
		/// <param name="pt1">
		///       <see cref="T:System.Drawing.Point" /> structure that represents the starting point of the curve. </param>
		/// <param name="pt2">
		///       <see cref="T:System.Drawing.Point" /> structure that represents the first control point for the curve. </param>
		/// <param name="pt3">
		///       <see cref="T:System.Drawing.Point" /> structure that represents the second control point for the curve. </param>
		/// <param name="pt4">
		///       <see cref="T:System.Drawing.Point" /> structure that represents the ending point of the curve. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.</exception>
		// Token: 0x06000193 RID: 403 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawBezier(Pen pen, Point pt1, Point pt2, Point pt3, Point pt4)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a Bézier spline defined by four <see cref="T:System.Drawing.PointF" /> structures.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the curve. </param>
		/// <param name="pt1">
		///       <see cref="T:System.Drawing.PointF" /> structure that represents the starting point of the curve. </param>
		/// <param name="pt2">
		///       <see cref="T:System.Drawing.PointF" /> structure that represents the first control point for the curve. </param>
		/// <param name="pt3">
		///       <see cref="T:System.Drawing.PointF" /> structure that represents the second control point for the curve. </param>
		/// <param name="pt4">
		///       <see cref="T:System.Drawing.PointF" /> structure that represents the ending point of the curve. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.</exception>
		// Token: 0x06000194 RID: 404 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawBezier(Pen pen, PointF pt1, PointF pt2, PointF pt3, PointF pt4)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a Bézier spline defined by four ordered pairs of coordinates that represent points.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the curve. </param>
		/// <param name="x1">The x-coordinate of the starting point of the curve. </param>
		/// <param name="y1">The y-coordinate of the starting point of the curve. </param>
		/// <param name="x2">The x-coordinate of the first control point of the curve. </param>
		/// <param name="y2">The y-coordinate of the first control point of the curve. </param>
		/// <param name="x3">The x-coordinate of the second control point of the curve. </param>
		/// <param name="y3">The y-coordinate of the second control point of the curve. </param>
		/// <param name="x4">The x-coordinate of the ending point of the curve. </param>
		/// <param name="y4">The y-coordinate of the ending point of the curve. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.</exception>
		// Token: 0x06000195 RID: 405 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawBezier(Pen pen, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a series of Bézier splines from an array of <see cref="T:System.Drawing.PointF" /> structures.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the curve. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.PointF" /> structures that represent the points that determine the curve. The number of points in the array should be a multiple of 3 plus 1, such as 4, 7, or 10.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x06000196 RID: 406 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawBeziers(Pen pen, PointF[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a series of Bézier splines from an array of <see cref="T:System.Drawing.Point" /> structures.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the curve. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.Point" /> structures that represent the points that determine the curve. The number of points in the array should be a multiple of 3 plus 1, such as 4, 7, or 10.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x06000197 RID: 407 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawBeziers(Pen pen, Point[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a closed cardinal spline defined by an array of <see cref="T:System.Drawing.PointF" /> structures.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and height of the curve. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.PointF" /> structures that define the spline. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x06000198 RID: 408 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawClosedCurve(Pen pen, PointF[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a closed cardinal spline defined by an array of <see cref="T:System.Drawing.PointF" /> structures using a specified tension.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and height of the curve. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.PointF" /> structures that define the spline. </param>
		/// <param name="tension">Value greater than or equal to 0.0F that specifies the tension of the curve. </param>
		/// <param name="fillmode">Member of the <see cref="T:System.Drawing.Drawing2D.FillMode" /> enumeration that determines how the curve is filled. This parameter is required but is ignored. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x06000199 RID: 409 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawClosedCurve(Pen pen, PointF[] points, float tension, FillMode fillmode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a closed cardinal spline defined by an array of <see cref="T:System.Drawing.Point" /> structures.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and height of the curve. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.Point" /> structures that define the spline. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x0600019A RID: 410 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawClosedCurve(Pen pen, Point[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a closed cardinal spline defined by an array of <see cref="T:System.Drawing.Point" /> structures using a specified tension.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and height of the curve. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.Point" /> structures that define the spline. </param>
		/// <param name="tension">Value greater than or equal to 0.0F that specifies the tension of the curve. </param>
		/// <param name="fillmode">Member of the <see cref="T:System.Drawing.Drawing2D.FillMode" /> enumeration that determines how the curve is filled. This parameter is required but ignored. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x0600019B RID: 411 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawClosedCurve(Pen pen, Point[] points, float tension, FillMode fillmode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a cardinal spline through a specified array of <see cref="T:System.Drawing.PointF" /> structures.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the curve. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.PointF" /> structures that define the spline. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x0600019C RID: 412 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawCurve(Pen pen, PointF[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a cardinal spline through a specified array of <see cref="T:System.Drawing.PointF" /> structures. The drawing begins offset from the beginning of the array.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the curve. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.PointF" /> structures that define the spline. </param>
		/// <param name="offset">Offset from the first element in the array of the <paramref name="points" /> parameter to the starting point in the curve. </param>
		/// <param name="numberOfSegments">Number of segments after the starting point to include in the curve. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x0600019D RID: 413 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawCurve(Pen pen, PointF[] points, int offset, int numberOfSegments)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a cardinal spline through a specified array of <see cref="T:System.Drawing.PointF" /> structures using a specified tension. The drawing begins offset from the beginning of the array.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the curve. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.PointF" /> structures that define the spline. </param>
		/// <param name="offset">Offset from the first element in the array of the <paramref name="points" /> parameter to the starting point in the curve. </param>
		/// <param name="numberOfSegments">Number of segments after the starting point to include in the curve. </param>
		/// <param name="tension">Value greater than or equal to 0.0F that specifies the tension of the curve. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x0600019E RID: 414 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawCurve(Pen pen, PointF[] points, int offset, int numberOfSegments, float tension)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a cardinal spline through a specified array of <see cref="T:System.Drawing.PointF" /> structures using a specified tension.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the curve. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.PointF" /> structures that represent the points that define the curve. </param>
		/// <param name="tension">Value greater than or equal to 0.0F that specifies the tension of the curve. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x0600019F RID: 415 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawCurve(Pen pen, PointF[] points, float tension)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a cardinal spline through a specified array of <see cref="T:System.Drawing.Point" /> structures.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and height of the curve. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.Point" /> structures that define the spline. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x060001A0 RID: 416 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawCurve(Pen pen, Point[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a cardinal spline through a specified array of <see cref="T:System.Drawing.Point" /> structures using a specified tension.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the curve. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.Point" /> structures that define the spline. </param>
		/// <param name="offset">Offset from the first element in the array of the <paramref name="points" /> parameter to the starting point in the curve. </param>
		/// <param name="numberOfSegments">Number of segments after the starting point to include in the curve. </param>
		/// <param name="tension">Value greater than or equal to 0.0F that specifies the tension of the curve. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x060001A1 RID: 417 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawCurve(Pen pen, Point[] points, int offset, int numberOfSegments, float tension)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a cardinal spline through a specified array of <see cref="T:System.Drawing.Point" /> structures using a specified tension.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the curve. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.Point" /> structures that define the spline. </param>
		/// <param name="tension">Value greater than or equal to 0.0F that specifies the tension of the curve. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x060001A2 RID: 418 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawCurve(Pen pen, Point[] points, float tension)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws an ellipse specified by a bounding <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the ellipse. </param>
		/// <param name="rect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that defines the boundaries of the ellipse. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.</exception>
		// Token: 0x060001A3 RID: 419 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawEllipse(Pen pen, Rectangle rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws an ellipse defined by a bounding <see cref="T:System.Drawing.RectangleF" />.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the ellipse. </param>
		/// <param name="rect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that defines the boundaries of the ellipse. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.</exception>
		// Token: 0x060001A4 RID: 420 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawEllipse(Pen pen, RectangleF rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws an ellipse defined by a bounding rectangle specified by coordinates for the upper-left corner of the rectangle, a height, and a width.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the ellipse. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the bounding rectangle that defines the ellipse. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the bounding rectangle that defines the ellipse. </param>
		/// <param name="width">Width of the bounding rectangle that defines the ellipse. </param>
		/// <param name="height">Height of the bounding rectangle that defines the ellipse. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.</exception>
		// Token: 0x060001A5 RID: 421 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawEllipse(Pen pen, int x, int y, int width, int height)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws an ellipse defined by a bounding rectangle specified by a pair of coordinates, a height, and a width.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the ellipse. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the bounding rectangle that defines the ellipse. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the bounding rectangle that defines the ellipse. </param>
		/// <param name="width">Width of the bounding rectangle that defines the ellipse. </param>
		/// <param name="height">Height of the bounding rectangle that defines the ellipse. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.</exception>
		// Token: 0x060001A6 RID: 422 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawEllipse(Pen pen, float x, float y, float width, float height)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the image represented by the specified <see cref="T:System.Drawing.Icon" /> within the area specified by a <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <param name="icon">
		///       <see cref="T:System.Drawing.Icon" /> to draw. </param>
		/// <param name="targetRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the location and size of the resulting image on the display surface. The image contained in the <paramref name="icon" /> parameter is scaled to the dimensions of this rectangular area. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="icon" /> is <see langword="null" />.</exception>
		// Token: 0x060001A7 RID: 423 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawIcon(Icon icon, Rectangle targetRect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the image represented by the specified <see cref="T:System.Drawing.Icon" /> at the specified coordinates.</summary>
		/// <param name="icon">
		///       <see cref="T:System.Drawing.Icon" /> to draw. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the drawn image. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the drawn image. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="icon" /> is <see langword="null" />.</exception>
		// Token: 0x060001A8 RID: 424 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawIcon(Icon icon, int x, int y)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the image represented by the specified <see cref="T:System.Drawing.Icon" /> without scaling the image.</summary>
		/// <param name="icon">
		///       <see cref="T:System.Drawing.Icon" /> to draw. </param>
		/// <param name="targetRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the location and size of the resulting image. The image is not scaled to fit this rectangle, but retains its original size. If the image is larger than the rectangle, it is clipped to fit inside it. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="icon" /> is <see langword="null" />.</exception>
		// Token: 0x060001A9 RID: 425 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawIconUnstretched(Icon icon, Rectangle targetRect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified <see cref="T:System.Drawing.Image" />, using its original physical size, at the specified location.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="point">
		///       <see cref="T:System.Drawing.Point" /> structure that represents the location of the upper-left corner of the drawn image. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001AA RID: 426 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, Point point)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified <see cref="T:System.Drawing.Image" />, using its original physical size, at the specified location.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="point">
		///       <see cref="T:System.Drawing.PointF" /> structure that represents the upper-left corner of the drawn image. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001AB RID: 427 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, PointF point)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified <see cref="T:System.Drawing.Image" /> at the specified location and with the specified shape and size.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="destPoints">Array of three <see cref="T:System.Drawing.PointF" /> structures that define a parallelogram. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001AC RID: 428 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, PointF[] destPoints)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified portion of the specified <see cref="T:System.Drawing.Image" /> at the specified location and with the specified size.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="destPoints">Array of three <see cref="T:System.Drawing.PointF" /> structures that define a parallelogram. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the portion of the <paramref name="image" /> object to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the units of measure used by the <paramref name="srcRect" /> parameter. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001AD RID: 429 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, PointF[] destPoints, RectangleF srcRect, GraphicsUnit srcUnit)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified portion of the specified <see cref="T:System.Drawing.Image" /> at the specified location and with the specified size.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="destPoints">Array of three <see cref="T:System.Drawing.PointF" /> structures that define a parallelogram. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the portion of the <paramref name="image" /> object to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the units of measure used by the <paramref name="srcRect" /> parameter. </param>
		/// <param name="imageAttr">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies recoloring and gamma information for the <paramref name="image" /> object. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001AE RID: 430 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, PointF[] destPoints, RectangleF srcRect, GraphicsUnit srcUnit, ImageAttributes imageAttr)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified portion of the specified <see cref="T:System.Drawing.Image" /> at the specified location and with the specified size.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="destPoints">Array of three <see cref="T:System.Drawing.PointF" /> structures that define a parallelogram. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the portion of the <paramref name="image" /> object to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the units of measure used by the <paramref name="srcRect" /> parameter. </param>
		/// <param name="imageAttr">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies recoloring and gamma information for the <paramref name="image" /> object. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.DrawImageAbort" /> delegate that specifies a method to call during the drawing of the image. This method is called frequently to check whether to stop execution of the <see cref="M:System.Drawing.Graphics.DrawImage(System.Drawing.Image,System.Drawing.PointF[],System.Drawing.RectangleF,System.Drawing.GraphicsUnit,System.Drawing.Imaging.ImageAttributes,System.Drawing.Graphics.DrawImageAbort)" />  method according to application-determined criteria. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001AF RID: 431 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, PointF[] destPoints, RectangleF srcRect, GraphicsUnit srcUnit, ImageAttributes imageAttr, Graphics.DrawImageAbort callback)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified portion of the specified <see cref="T:System.Drawing.Image" /> at the specified location and with the specified size.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="destPoints">Array of three <see cref="T:System.Drawing.PointF" /> structures that define a parallelogram. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the portion of the <paramref name="image" /> object to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the units of measure used by the <paramref name="srcRect" /> parameter. </param>
		/// <param name="imageAttr">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies recoloring and gamma information for the <paramref name="image" /> object. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.DrawImageAbort" /> delegate that specifies a method to call during the drawing of the image. This method is called frequently to check whether to stop execution of the <see cref="M:System.Drawing.Graphics.DrawImage(System.Drawing.Image,System.Drawing.PointF[],System.Drawing.RectangleF,System.Drawing.GraphicsUnit,System.Drawing.Imaging.ImageAttributes,System.Drawing.Graphics.DrawImageAbort,System.Int32)" /> method according to application-determined criteria. </param>
		/// <param name="callbackData">Value specifying additional data for the <see cref="T:System.Drawing.Graphics.DrawImageAbort" /> delegate to use when checking whether to stop execution of the <see cref="M:System.Drawing.Graphics.DrawImage(System.Drawing.Image,System.Drawing.PointF[],System.Drawing.RectangleF,System.Drawing.GraphicsUnit,System.Drawing.Imaging.ImageAttributes,System.Drawing.Graphics.DrawImageAbort,System.Int32)" /> method. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001B0 RID: 432 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, PointF[] destPoints, RectangleF srcRect, GraphicsUnit srcUnit, ImageAttributes imageAttr, Graphics.DrawImageAbort callback, int callbackData)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified <see cref="T:System.Drawing.Image" /> at the specified location and with the specified shape and size.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="destPoints">Array of three <see cref="T:System.Drawing.Point" /> structures that define a parallelogram. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001B1 RID: 433 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, Point[] destPoints)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified portion of the specified <see cref="T:System.Drawing.Image" /> at the specified location and with the specified size.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="destPoints">Array of three <see cref="T:System.Drawing.Point" /> structures that define a parallelogram. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the portion of the <paramref name="image" /> object to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the units of measure used by the <paramref name="srcRect" /> parameter. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001B2 RID: 434 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, Point[] destPoints, Rectangle srcRect, GraphicsUnit srcUnit)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified portion of the specified <see cref="T:System.Drawing.Image" /> at the specified location.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="destPoints">Array of three <see cref="T:System.Drawing.Point" /> structures that define a parallelogram. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the portion of the <paramref name="image" /> object to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the units of measure used by the <paramref name="srcRect" /> parameter. </param>
		/// <param name="imageAttr">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies recoloring and gamma information for the <paramref name="image" /> object. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001B3 RID: 435 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, Point[] destPoints, Rectangle srcRect, GraphicsUnit srcUnit, ImageAttributes imageAttr)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified portion of the specified <see cref="T:System.Drawing.Image" /> at the specified location and with the specified size.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="destPoints">Array of three <see cref="T:System.Drawing.PointF" /> structures that define a parallelogram. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the portion of the <paramref name="image" /> object to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the units of measure used by the <paramref name="srcRect" /> parameter. </param>
		/// <param name="imageAttr">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies recoloring and gamma information for the <paramref name="image" /> object. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.DrawImageAbort" /> delegate that specifies a method to call during the drawing of the image. This method is called frequently to check whether to stop execution of the <see cref="M:System.Drawing.Graphics.DrawImage(System.Drawing.Image,System.Drawing.Point[],System.Drawing.Rectangle,System.Drawing.GraphicsUnit,System.Drawing.Imaging.ImageAttributes,System.Drawing.Graphics.DrawImageAbort)" /> method according to application-determined criteria. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001B4 RID: 436 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, Point[] destPoints, Rectangle srcRect, GraphicsUnit srcUnit, ImageAttributes imageAttr, Graphics.DrawImageAbort callback)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified portion of the specified <see cref="T:System.Drawing.Image" /> at the specified location and with the specified size.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="destPoints">Array of three <see cref="T:System.Drawing.PointF" /> structures that define a parallelogram. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the portion of the <paramref name="image" /> object to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the units of measure used by the <paramref name="srcRect" /> parameter. </param>
		/// <param name="imageAttr">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies recoloring and gamma information for the <paramref name="image" /> object. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.DrawImageAbort" /> delegate that specifies a method to call during the drawing of the image. This method is called frequently to check whether to stop execution of the <see cref="M:System.Drawing.Graphics.DrawImage(System.Drawing.Image,System.Drawing.Point[],System.Drawing.Rectangle,System.Drawing.GraphicsUnit,System.Drawing.Imaging.ImageAttributes,System.Drawing.Graphics.DrawImageAbort,System.Int32)" /> method according to application-determined criteria. </param>
		/// <param name="callbackData">Value specifying additional data for the <see cref="T:System.Drawing.Graphics.DrawImageAbort" /> delegate to use when checking whether to stop execution of the <see cref="M:System.Drawing.Graphics.DrawImage(System.Drawing.Image,System.Drawing.Point[],System.Drawing.Rectangle,System.Drawing.GraphicsUnit,System.Drawing.Imaging.ImageAttributes,System.Drawing.Graphics.DrawImageAbort,System.Int32)" /> method. </param>
		// Token: 0x060001B5 RID: 437 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, Point[] destPoints, Rectangle srcRect, GraphicsUnit srcUnit, ImageAttributes imageAttr, Graphics.DrawImageAbort callback, int callbackData)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified <see cref="T:System.Drawing.Image" /> at the specified location and with the specified size.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="rect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the location and size of the drawn image. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001B6 RID: 438 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, Rectangle rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified portion of the specified <see cref="T:System.Drawing.Image" /> at the specified location and with the specified size.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="destRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the location and size of the drawn image. The image is scaled to fit the rectangle. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the portion of the <paramref name="image" /> object to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the units of measure used by the <paramref name="srcRect" /> parameter. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001B7 RID: 439 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, Rectangle destRect, Rectangle srcRect, GraphicsUnit srcUnit)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified portion of the specified <see cref="T:System.Drawing.Image" /> at the specified location and with the specified size.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="destRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the location and size of the drawn image. The image is scaled to fit the rectangle. </param>
		/// <param name="srcX">The x-coordinate of the upper-left corner of the portion of the source image to draw. </param>
		/// <param name="srcY">The y-coordinate of the upper-left corner of the portion of the source image to draw. </param>
		/// <param name="srcWidth">Width of the portion of the source image to draw. </param>
		/// <param name="srcHeight">Height of the portion of the source image to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the units of measure used to determine the source rectangle. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001B8 RID: 440 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, Rectangle destRect, int srcX, int srcY, int srcWidth, int srcHeight, GraphicsUnit srcUnit)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified portion of the specified <see cref="T:System.Drawing.Image" /> at the specified location and with the specified size.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="destRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the location and size of the drawn image. The image is scaled to fit the rectangle. </param>
		/// <param name="srcX">The x-coordinate of the upper-left corner of the portion of the source image to draw. </param>
		/// <param name="srcY">The y-coordinate of the upper-left corner of the portion of the source image to draw. </param>
		/// <param name="srcWidth">Width of the portion of the source image to draw. </param>
		/// <param name="srcHeight">Height of the portion of the source image to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the units of measure used to determine the source rectangle. </param>
		/// <param name="imageAttr">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies recoloring and gamma information for the <paramref name="image" /> object. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001B9 RID: 441 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, Rectangle destRect, int srcX, int srcY, int srcWidth, int srcHeight, GraphicsUnit srcUnit, ImageAttributes imageAttr)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified portion of the specified <see cref="T:System.Drawing.Image" /> at the specified location and with the specified size.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="destRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the location and size of the drawn image. The image is scaled to fit the rectangle. </param>
		/// <param name="srcX">The x-coordinate of the upper-left corner of the portion of the source image to draw. </param>
		/// <param name="srcY">The y-coordinate of the upper-left corner of the portion of the source image to draw. </param>
		/// <param name="srcWidth">Width of the portion of the source image to draw. </param>
		/// <param name="srcHeight">Height of the portion of the source image to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the units of measure used to determine the source rectangle. </param>
		/// <param name="imageAttr">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies recoloring and gamma information for <paramref name="image" />. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.DrawImageAbort" /> delegate that specifies a method to call during the drawing of the image. This method is called frequently to check whether to stop execution of the <see cref="M:System.Drawing.Graphics.DrawImage(System.Drawing.Image,System.Drawing.Rectangle,System.Int32,System.Int32,System.Int32,System.Int32,System.Drawing.GraphicsUnit,System.Drawing.Imaging.ImageAttributes,System.Drawing.Graphics.DrawImageAbort)" /> method according to application-determined criteria. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001BA RID: 442 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, Rectangle destRect, int srcX, int srcY, int srcWidth, int srcHeight, GraphicsUnit srcUnit, ImageAttributes imageAttr, Graphics.DrawImageAbort callback)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified portion of the specified <see cref="T:System.Drawing.Image" /> at the specified location and with the specified size.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="destRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the location and size of the drawn image. The image is scaled to fit the rectangle. </param>
		/// <param name="srcX">The x-coordinate of the upper-left corner of the portion of the source image to draw. </param>
		/// <param name="srcY">The y-coordinate of the upper-left corner of the portion of the source image to draw. </param>
		/// <param name="srcWidth">Width of the portion of the source image to draw. </param>
		/// <param name="srcHeight">Height of the portion of the source image to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the units of measure used to determine the source rectangle. </param>
		/// <param name="imageAttrs">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies recoloring and gamma information for the <paramref name="image" /> object. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.DrawImageAbort" /> delegate that specifies a method to call during the drawing of the image. This method is called frequently to check whether to stop execution of the <see cref="M:System.Drawing.Graphics.DrawImage(System.Drawing.Image,System.Drawing.Rectangle,System.Int32,System.Int32,System.Int32,System.Int32,System.Drawing.GraphicsUnit,System.Drawing.Imaging.ImageAttributes,System.Drawing.Graphics.DrawImageAbort,System.IntPtr)" /> method according to application-determined criteria. </param>
		/// <param name="callbackData">Value specifying additional data for the <see cref="T:System.Drawing.Graphics.DrawImageAbort" /> delegate to use when checking whether to stop execution of the <see langword="DrawImage" /> method. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001BB RID: 443 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, Rectangle destRect, int srcX, int srcY, int srcWidth, int srcHeight, GraphicsUnit srcUnit, ImageAttributes imageAttrs, Graphics.DrawImageAbort callback, IntPtr callbackData)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified portion of the specified <see cref="T:System.Drawing.Image" /> at the specified location and with the specified size.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="destRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the location and size of the drawn image. The image is scaled to fit the rectangle. </param>
		/// <param name="srcX">The x-coordinate of the upper-left corner of the portion of the source image to draw. </param>
		/// <param name="srcY">The y-coordinate of the upper-left corner of the portion of the source image to draw. </param>
		/// <param name="srcWidth">Width of the portion of the source image to draw. </param>
		/// <param name="srcHeight">Height of the portion of the source image to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the units of measure used to determine the source rectangle. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001BC RID: 444 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, Rectangle destRect, float srcX, float srcY, float srcWidth, float srcHeight, GraphicsUnit srcUnit)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified portion of the specified <see cref="T:System.Drawing.Image" /> at the specified location and with the specified size.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="destRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the location and size of the drawn image. The image is scaled to fit the rectangle. </param>
		/// <param name="srcX">The x-coordinate of the upper-left corner of the portion of the source image to draw. </param>
		/// <param name="srcY">The y-coordinate of the upper-left corner of the portion of the source image to draw. </param>
		/// <param name="srcWidth">Width of the portion of the source image to draw. </param>
		/// <param name="srcHeight">Height of the portion of the source image to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the units of measure used to determine the source rectangle. </param>
		/// <param name="imageAttrs">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies recoloring and gamma information for the <paramref name="image" /> object. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001BD RID: 445 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, Rectangle destRect, float srcX, float srcY, float srcWidth, float srcHeight, GraphicsUnit srcUnit, ImageAttributes imageAttrs)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified portion of the specified <see cref="T:System.Drawing.Image" /> at the specified location and with the specified size.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="destRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the location and size of the drawn image. The image is scaled to fit the rectangle. </param>
		/// <param name="srcX">The x-coordinate of the upper-left corner of the portion of the source image to draw. </param>
		/// <param name="srcY">The y-coordinate of the upper-left corner of the portion of the source image to draw. </param>
		/// <param name="srcWidth">Width of the portion of the source image to draw. </param>
		/// <param name="srcHeight">Height of the portion of the source image to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the units of measure used to determine the source rectangle. </param>
		/// <param name="imageAttrs">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies recoloring and gamma information for the <paramref name="image" /> object. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.DrawImageAbort" /> delegate that specifies a method to call during the drawing of the image. This method is called frequently to check whether to stop execution of the <see cref="M:System.Drawing.Graphics.DrawImage(System.Drawing.Image,System.Drawing.Rectangle,System.Single,System.Single,System.Single,System.Single,System.Drawing.GraphicsUnit,System.Drawing.Imaging.ImageAttributes,System.Drawing.Graphics.DrawImageAbort)" /> method according to application-determined criteria. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001BE RID: 446 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, Rectangle destRect, float srcX, float srcY, float srcWidth, float srcHeight, GraphicsUnit srcUnit, ImageAttributes imageAttrs, Graphics.DrawImageAbort callback)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified portion of the specified <see cref="T:System.Drawing.Image" /> at the specified location and with the specified size.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="destRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the location and size of the drawn image. The image is scaled to fit the rectangle. </param>
		/// <param name="srcX">The x-coordinate of the upper-left corner of the portion of the source image to draw. </param>
		/// <param name="srcY">The y-coordinate of the upper-left corner of the portion of the source image to draw. </param>
		/// <param name="srcWidth">Width of the portion of the source image to draw. </param>
		/// <param name="srcHeight">Height of the portion of the source image to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the units of measure used to determine the source rectangle. </param>
		/// <param name="imageAttrs">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies recoloring and gamma information for the <paramref name="image" /> object. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.DrawImageAbort" /> delegate that specifies a method to call during the drawing of the image. This method is called frequently to check whether to stop execution of the <see cref="M:System.Drawing.Graphics.DrawImage(System.Drawing.Image,System.Drawing.Rectangle,System.Single,System.Single,System.Single,System.Single,System.Drawing.GraphicsUnit,System.Drawing.Imaging.ImageAttributes,System.Drawing.Graphics.DrawImageAbort,System.IntPtr)" /> method according to application-determined criteria. </param>
		/// <param name="callbackData">Value specifying additional data for the <see cref="T:System.Drawing.Graphics.DrawImageAbort" /> delegate to use when checking whether to stop execution of the <see langword="DrawImage" /> method. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001BF RID: 447 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, Rectangle destRect, float srcX, float srcY, float srcWidth, float srcHeight, GraphicsUnit srcUnit, ImageAttributes imageAttrs, Graphics.DrawImageAbort callback, IntPtr callbackData)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified <see cref="T:System.Drawing.Image" /> at the specified location and with the specified size.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="rect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the location and size of the drawn image. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001C0 RID: 448 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, RectangleF rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified portion of the specified <see cref="T:System.Drawing.Image" /> at the specified location and with the specified size.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="destRect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the location and size of the drawn image. The image is scaled to fit the rectangle. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the portion of the <paramref name="image" /> object to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the units of measure used by the <paramref name="srcRect" /> parameter. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001C1 RID: 449 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, RectangleF destRect, RectangleF srcRect, GraphicsUnit srcUnit)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified image, using its original physical size, at the location specified by a coordinate pair.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the drawn image. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the drawn image. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001C2 RID: 450 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, int x, int y)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a portion of an image at a specified location.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the drawn image. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the drawn image. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the portion of the <paramref name="image" /> object to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the units of measure used by the <paramref name="srcRect" /> parameter. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001C3 RID: 451 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, int x, int y, Rectangle srcRect, GraphicsUnit srcUnit)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified <see cref="T:System.Drawing.Image" /> at the specified location and with the specified size.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the drawn image. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the drawn image. </param>
		/// <param name="width">Width of the drawn image. </param>
		/// <param name="height">Height of the drawn image. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001C4 RID: 452 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, int x, int y, int width, int height)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified <see cref="T:System.Drawing.Image" />, using its original physical size, at the specified location.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the drawn image. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the drawn image. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001C5 RID: 453 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, float x, float y)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a portion of an image at a specified location.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the drawn image. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the drawn image. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the portion of the <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the units of measure used by the <paramref name="srcRect" /> parameter. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001C6 RID: 454 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, float x, float y, RectangleF srcRect, GraphicsUnit srcUnit)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified <see cref="T:System.Drawing.Image" /> at the specified location and with the specified size.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the drawn image. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the drawn image. </param>
		/// <param name="width">Width of the drawn image. </param>
		/// <param name="height">Height of the drawn image. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001C7 RID: 455 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImage(Image image, float x, float y, float width, float height)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a specified image using its original physical size at a specified location.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="point">
		///       <see cref="T:System.Drawing.Point" /> structure that specifies the upper-left corner of the drawn image. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001C8 RID: 456 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImageUnscaled(Image image, Point point)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a specified image using its original physical size at a specified location.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="rect">
		///       <see cref="T:System.Drawing.Rectangle" /> that specifies the upper-left corner of the drawn image. The X and Y properties of the rectangle specify the upper-left corner. The Width and Height properties are ignored. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001C9 RID: 457 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImageUnscaled(Image image, Rectangle rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified image using its original physical size at the location specified by a coordinate pair.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the drawn image. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the drawn image. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001CA RID: 458 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImageUnscaled(Image image, int x, int y)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a specified image using its original physical size at a specified location.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> to draw. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the drawn image. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the drawn image. </param>
		/// <param name="width">Not used. </param>
		/// <param name="height">Not used. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001CB RID: 459 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImageUnscaled(Image image, int x, int y, int width, int height)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified image without scaling and clips it, if necessary, to fit in the specified rectangle.</summary>
		/// <param name="image">The <see cref="T:System.Drawing.Image" /> to draw.</param>
		/// <param name="rect">The <see cref="T:System.Drawing.Rectangle" /> in which to draw the image.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		// Token: 0x060001CC RID: 460 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawImageUnscaledAndClipped(Image image, Rectangle rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a line connecting two <see cref="T:System.Drawing.Point" /> structures.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the line. </param>
		/// <param name="pt1">
		///       <see cref="T:System.Drawing.Point" /> structure that represents the first point to connect. </param>
		/// <param name="pt2">
		///       <see cref="T:System.Drawing.Point" /> structure that represents the second point to connect. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.</exception>
		// Token: 0x060001CD RID: 461 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawLine(Pen pen, Point pt1, Point pt2)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a line connecting two <see cref="T:System.Drawing.PointF" /> structures.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the line. </param>
		/// <param name="pt1">
		///       <see cref="T:System.Drawing.PointF" /> structure that represents the first point to connect. </param>
		/// <param name="pt2">
		///       <see cref="T:System.Drawing.PointF" /> structure that represents the second point to connect. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.</exception>
		// Token: 0x060001CE RID: 462 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawLine(Pen pen, PointF pt1, PointF pt2)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a line connecting the two points specified by the coordinate pairs.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the line. </param>
		/// <param name="x1">The x-coordinate of the first point. </param>
		/// <param name="y1">The y-coordinate of the first point. </param>
		/// <param name="x2">The x-coordinate of the second point. </param>
		/// <param name="y2">The y-coordinate of the second point. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.</exception>
		// Token: 0x060001CF RID: 463 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawLine(Pen pen, int x1, int y1, int x2, int y2)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a line connecting the two points specified by the coordinate pairs.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the line. </param>
		/// <param name="x1">The x-coordinate of the first point. </param>
		/// <param name="y1">The y-coordinate of the first point. </param>
		/// <param name="x2">The x-coordinate of the second point. </param>
		/// <param name="y2">The y-coordinate of the second point. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.</exception>
		// Token: 0x060001D0 RID: 464 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawLine(Pen pen, float x1, float y1, float x2, float y2)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a series of line segments that connect an array of <see cref="T:System.Drawing.PointF" /> structures.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the line segments. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.PointF" /> structures that represent the points to connect. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x060001D1 RID: 465 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawLines(Pen pen, PointF[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a series of line segments that connect an array of <see cref="T:System.Drawing.Point" /> structures.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the line segments. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.Point" /> structures that represent the points to connect. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x060001D2 RID: 466 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawLines(Pen pen, Point[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the path. </param>
		/// <param name="path">
		///       <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> to draw. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.-or-
		///         <paramref name="path" /> is <see langword="null" />.</exception>
		// Token: 0x060001D3 RID: 467 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawPath(Pen pen, GraphicsPath path)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a pie shape defined by an ellipse specified by a <see cref="T:System.Drawing.Rectangle" /> structure and two radial lines.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the pie shape. </param>
		/// <param name="rect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that represents the bounding rectangle that defines the ellipse from which the pie shape comes. </param>
		/// <param name="startAngle">Angle measured in degrees clockwise from the x-axis to the first side of the pie shape. </param>
		/// <param name="sweepAngle">Angle measured in degrees clockwise from the <paramref name="startAngle" /> parameter to the second side of the pie shape. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.</exception>
		// Token: 0x060001D4 RID: 468 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawPie(Pen pen, Rectangle rect, float startAngle, float sweepAngle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a pie shape defined by an ellipse specified by a <see cref="T:System.Drawing.RectangleF" /> structure and two radial lines.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the pie shape. </param>
		/// <param name="rect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that represents the bounding rectangle that defines the ellipse from which the pie shape comes. </param>
		/// <param name="startAngle">Angle measured in degrees clockwise from the x-axis to the first side of the pie shape. </param>
		/// <param name="sweepAngle">Angle measured in degrees clockwise from the <paramref name="startAngle" /> parameter to the second side of the pie shape. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.</exception>
		// Token: 0x060001D5 RID: 469 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawPie(Pen pen, RectangleF rect, float startAngle, float sweepAngle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a pie shape defined by an ellipse specified by a coordinate pair, a width, a height, and two radial lines.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the pie shape. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the bounding rectangle that defines the ellipse from which the pie shape comes. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the bounding rectangle that defines the ellipse from which the pie shape comes. </param>
		/// <param name="width">Width of the bounding rectangle that defines the ellipse from which the pie shape comes. </param>
		/// <param name="height">Height of the bounding rectangle that defines the ellipse from which the pie shape comes. </param>
		/// <param name="startAngle">Angle measured in degrees clockwise from the x-axis to the first side of the pie shape. </param>
		/// <param name="sweepAngle">Angle measured in degrees clockwise from the <paramref name="startAngle" /> parameter to the second side of the pie shape. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.</exception>
		// Token: 0x060001D6 RID: 470 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawPie(Pen pen, int x, int y, int width, int height, int startAngle, int sweepAngle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a pie shape defined by an ellipse specified by a coordinate pair, a width, a height, and two radial lines.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the pie shape. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the bounding rectangle that defines the ellipse from which the pie shape comes. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the bounding rectangle that defines the ellipse from which the pie shape comes. </param>
		/// <param name="width">Width of the bounding rectangle that defines the ellipse from which the pie shape comes. </param>
		/// <param name="height">Height of the bounding rectangle that defines the ellipse from which the pie shape comes. </param>
		/// <param name="startAngle">Angle measured in degrees clockwise from the x-axis to the first side of the pie shape. </param>
		/// <param name="sweepAngle">Angle measured in degrees clockwise from the <paramref name="startAngle" /> parameter to the second side of the pie shape. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.</exception>
		// Token: 0x060001D7 RID: 471 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawPie(Pen pen, float x, float y, float width, float height, float startAngle, float sweepAngle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a polygon defined by an array of <see cref="T:System.Drawing.PointF" /> structures.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the polygon. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.PointF" /> structures that represent the vertices of the polygon. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x060001D8 RID: 472 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawPolygon(Pen pen, PointF[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a polygon defined by an array of <see cref="T:System.Drawing.Point" /> structures.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the polygon. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.Point" /> structures that represent the vertices of the polygon. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.</exception>
		// Token: 0x060001D9 RID: 473 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawPolygon(Pen pen, Point[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a rectangle specified by a <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <param name="pen">A <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the rectangle. </param>
		/// <param name="rect">A <see cref="T:System.Drawing.Rectangle" /> structure that represents the rectangle to draw. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.</exception>
		// Token: 0x060001DA RID: 474 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawRectangle(Pen pen, Rectangle rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a rectangle specified by a coordinate pair, a width, and a height.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the rectangle. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the rectangle to draw. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the rectangle to draw. </param>
		/// <param name="width">Width of the rectangle to draw. </param>
		/// <param name="height">Height of the rectangle to draw. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.</exception>
		// Token: 0x060001DB RID: 475 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawRectangle(Pen pen, int x, int y, int width, int height)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a rectangle specified by a coordinate pair, a width, and a height.</summary>
		/// <param name="pen">A <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the rectangle. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the rectangle to draw. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the rectangle to draw. </param>
		/// <param name="width">The width of the rectangle to draw. </param>
		/// <param name="height">The height of the rectangle to draw. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.</exception>
		// Token: 0x060001DC RID: 476 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawRectangle(Pen pen, float x, float y, float width, float height)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a series of rectangles specified by <see cref="T:System.Drawing.RectangleF" /> structures.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the outlines of the rectangles. </param>
		/// <param name="rects">Array of <see cref="T:System.Drawing.RectangleF" /> structures that represent the rectangles to draw. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.-or-
		///         <paramref name="rects" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="rects" /> is a zero-length array.</exception>
		// Token: 0x060001DD RID: 477 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawRectangles(Pen pen, RectangleF[] rects)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws a series of rectangles specified by <see cref="T:System.Drawing.Rectangle" /> structures.</summary>
		/// <param name="pen">
		///       <see cref="T:System.Drawing.Pen" /> that determines the color, width, and style of the outlines of the rectangles. </param>
		/// <param name="rects">Array of <see cref="T:System.Drawing.Rectangle" /> structures that represent the rectangles to draw. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pen" /> is <see langword="null" />.-or-
		///         <paramref name="rects" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="rects" /> is a zero-length array.</exception>
		// Token: 0x060001DE RID: 478 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawRectangles(Pen pen, Rectangle[] rects)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified text string at the specified location with the specified <see cref="T:System.Drawing.Brush" /> and <see cref="T:System.Drawing.Font" /> objects.</summary>
		/// <param name="s">String to draw. </param>
		/// <param name="font">
		///       <see cref="T:System.Drawing.Font" /> that defines the text format of the string. </param>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the color and texture of the drawn text. </param>
		/// <param name="point">
		///       <see cref="T:System.Drawing.PointF" /> structure that specifies the upper-left corner of the drawn text. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.-or-
		///         <paramref name="s" /> is <see langword="null" />.</exception>
		// Token: 0x060001DF RID: 479 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawString(string s, Font font, Brush brush, PointF point)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified text string at the specified location with the specified <see cref="T:System.Drawing.Brush" /> and <see cref="T:System.Drawing.Font" /> objects using the formatting attributes of the specified <see cref="T:System.Drawing.StringFormat" />.</summary>
		/// <param name="s">String to draw. </param>
		/// <param name="font">
		///       <see cref="T:System.Drawing.Font" /> that defines the text format of the string. </param>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the color and texture of the drawn text. </param>
		/// <param name="point">
		///       <see cref="T:System.Drawing.PointF" /> structure that specifies the upper-left corner of the drawn text. </param>
		/// <param name="format">
		///       <see cref="T:System.Drawing.StringFormat" /> that specifies formatting attributes, such as line spacing and alignment, that are applied to the drawn text. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.-or-
		///         <paramref name="s" /> is <see langword="null" />.</exception>
		// Token: 0x060001E0 RID: 480 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawString(string s, Font font, Brush brush, PointF point, StringFormat format)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified text string in the specified rectangle with the specified <see cref="T:System.Drawing.Brush" /> and <see cref="T:System.Drawing.Font" /> objects.</summary>
		/// <param name="s">String to draw. </param>
		/// <param name="font">
		///       <see cref="T:System.Drawing.Font" /> that defines the text format of the string. </param>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the color and texture of the drawn text. </param>
		/// <param name="layoutRectangle">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the location of the drawn text. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.-or-
		///         <paramref name="s" /> is <see langword="null" />.</exception>
		// Token: 0x060001E1 RID: 481 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawString(string s, Font font, Brush brush, RectangleF layoutRectangle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified text string in the specified rectangle with the specified <see cref="T:System.Drawing.Brush" /> and <see cref="T:System.Drawing.Font" /> objects using the formatting attributes of the specified <see cref="T:System.Drawing.StringFormat" />.</summary>
		/// <param name="s">String to draw. </param>
		/// <param name="font">
		///       <see cref="T:System.Drawing.Font" /> that defines the text format of the string. </param>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the color and texture of the drawn text. </param>
		/// <param name="layoutRectangle">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the location of the drawn text. </param>
		/// <param name="format">
		///       <see cref="T:System.Drawing.StringFormat" /> that specifies formatting attributes, such as line spacing and alignment, that are applied to the drawn text. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.-or-
		///         <paramref name="s" /> is <see langword="null" />.</exception>
		// Token: 0x060001E2 RID: 482 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawString(string s, Font font, Brush brush, RectangleF layoutRectangle, StringFormat format)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified text string at the specified location with the specified <see cref="T:System.Drawing.Brush" /> and <see cref="T:System.Drawing.Font" /> objects.</summary>
		/// <param name="s">String to draw. </param>
		/// <param name="font">
		///       <see cref="T:System.Drawing.Font" /> that defines the text format of the string. </param>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the color and texture of the drawn text. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the drawn text. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the drawn text. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.-or-
		///         <paramref name="s" /> is <see langword="null" />.</exception>
		// Token: 0x060001E3 RID: 483 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawString(string s, Font font, Brush brush, float x, float y)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Draws the specified text string at the specified location with the specified <see cref="T:System.Drawing.Brush" /> and <see cref="T:System.Drawing.Font" /> objects using the formatting attributes of the specified <see cref="T:System.Drawing.StringFormat" />.</summary>
		/// <param name="s">String to draw. </param>
		/// <param name="font">
		///       <see cref="T:System.Drawing.Font" /> that defines the text format of the string. </param>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the color and texture of the drawn text. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the drawn text. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the drawn text. </param>
		/// <param name="format">
		///       <see cref="T:System.Drawing.StringFormat" /> that specifies formatting attributes, such as line spacing and alignment, that are applied to the drawn text. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.-or-
		///         <paramref name="s" /> is <see langword="null" />.</exception>
		// Token: 0x060001E4 RID: 484 RVA: 0x00004644 File Offset: 0x00002844
		public void DrawString(string s, Font font, Brush brush, float x, float y, StringFormat format)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Closes the current graphics container and restores the state of this <see cref="T:System.Drawing.Graphics" /> to the state saved by a call to the <see cref="M:System.Drawing.Graphics.BeginContainer" /> method.</summary>
		/// <param name="container">
		///       <see cref="T:System.Drawing.Drawing2D.GraphicsContainer" /> that represents the container this method restores. </param>
		// Token: 0x060001E5 RID: 485 RVA: 0x00004644 File Offset: 0x00002844
		public void EndContainer(GraphicsContainer container)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in the specified <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display at a specified point.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoint">
		///       <see cref="T:System.Drawing.Point" /> structure that specifies the location of the upper-left corner of the drawn metafile. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		// Token: 0x060001E6 RID: 486 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, Point destPoint, Graphics.EnumerateMetafileProc callback)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in the specified <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display at a specified point.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoint">
		///       <see cref="T:System.Drawing.Point" /> structure that specifies the location of the upper-left corner of the drawn metafile. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		// Token: 0x060001E7 RID: 487 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, Point destPoint, Graphics.EnumerateMetafileProc callback, IntPtr callbackData)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in the specified <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display at a specified point using specified image attributes.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoint">
		///       <see cref="T:System.Drawing.Point" /> structure that specifies the location of the upper-left corner of the drawn metafile. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		/// <param name="imageAttr">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies image attribute information for the drawn image. </param>
		// Token: 0x060001E8 RID: 488 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, Point destPoint, Graphics.EnumerateMetafileProc callback, IntPtr callbackData, ImageAttributes imageAttr)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in a selected rectangle from a <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display at a specified point.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoint">
		///       <see cref="T:System.Drawing.Point" /> structure that specifies the location of the upper-left corner of the drawn metafile. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the portion of the metafile, relative to its upper-left corner, to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the unit of measure used to determine the portion of the metafile that the rectangle specified by the <paramref name="srcRect" /> parameter contains. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		// Token: 0x060001E9 RID: 489 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, Point destPoint, Rectangle srcRect, GraphicsUnit srcUnit, Graphics.EnumerateMetafileProc callback)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in a selected rectangle from a <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display at a specified point.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoint">
		///       <see cref="T:System.Drawing.Point" /> structure that specifies the location of the upper-left corner of the drawn metafile. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the portion of the metafile, relative to its upper-left corner, to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the unit of measure used to determine the portion of the metafile that the rectangle specified by the <paramref name="srcRect" /> parameter contains. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		// Token: 0x060001EA RID: 490 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, Point destPoint, Rectangle srcRect, GraphicsUnit srcUnit, Graphics.EnumerateMetafileProc callback, IntPtr callbackData)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in a selected rectangle from a <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display at a specified point using specified image attributes.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoint">
		///       <see cref="T:System.Drawing.Point" /> structure that specifies the location of the upper-left corner of the drawn metafile. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the portion of the metafile, relative to its upper-left corner, to draw. </param>
		/// <param name="unit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the unit of measure used to determine the portion of the metafile that the rectangle specified by the <paramref name="srcRect" /> parameter contains. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		/// <param name="imageAttr">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies image attribute information for the drawn image. </param>
		// Token: 0x060001EB RID: 491 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, Point destPoint, Rectangle srcRect, GraphicsUnit unit, Graphics.EnumerateMetafileProc callback, IntPtr callbackData, ImageAttributes imageAttr)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in the specified <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display at a specified point.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoint">
		///       <see cref="T:System.Drawing.PointF" /> structure that specifies the location of the upper-left corner of the drawn metafile. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		// Token: 0x060001EC RID: 492 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, PointF destPoint, Graphics.EnumerateMetafileProc callback)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in the specified <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display at a specified point.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoint">
		///       <see cref="T:System.Drawing.PointF" /> structure that specifies the location of the upper-left corner of the drawn metafile. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		// Token: 0x060001ED RID: 493 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, PointF destPoint, Graphics.EnumerateMetafileProc callback, IntPtr callbackData)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in the specified <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display at a specified point using specified image attributes.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoint">
		///       <see cref="T:System.Drawing.PointF" /> structure that specifies the location of the upper-left corner of the drawn metafile. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		/// <param name="imageAttr">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies image attribute information for the drawn image. </param>
		// Token: 0x060001EE RID: 494 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, PointF destPoint, Graphics.EnumerateMetafileProc callback, IntPtr callbackData, ImageAttributes imageAttr)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in a selected rectangle from a <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display at a specified point.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoint">
		///       <see cref="T:System.Drawing.PointF" /> structure that specifies the location of the upper-left corner of the drawn metafile. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the portion of the metafile, relative to its upper-left corner, to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the unit of measure used to determine the portion of the metafile that the rectangle specified by the <paramref name="srcRect" /> parameter contains. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		// Token: 0x060001EF RID: 495 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, PointF destPoint, RectangleF srcRect, GraphicsUnit srcUnit, Graphics.EnumerateMetafileProc callback)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in a selected rectangle from a <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display at a specified point.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoint">
		///       <see cref="T:System.Drawing.PointF" /> structure that specifies the location of the upper-left corner of the drawn metafile. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the portion of the metafile, relative to its upper-left corner, to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the unit of measure used to determine the portion of the metafile that the rectangle specified by the <paramref name="srcRect" /> parameter contains. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		// Token: 0x060001F0 RID: 496 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, PointF destPoint, RectangleF srcRect, GraphicsUnit srcUnit, Graphics.EnumerateMetafileProc callback, IntPtr callbackData)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in a selected rectangle from a <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display at a specified point using specified image attributes.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoint">
		///       <see cref="T:System.Drawing.PointF" /> structure that specifies the location of the upper-left corner of the drawn metafile. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the portion of the metafile, relative to its upper-left corner, to draw. </param>
		/// <param name="unit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the unit of measure used to determine the portion of the metafile that the rectangle specified by the <paramref name="srcRect" /> parameter contains. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		/// <param name="imageAttr">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies image attribute information for the drawn image. </param>
		// Token: 0x060001F1 RID: 497 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, PointF destPoint, RectangleF srcRect, GraphicsUnit unit, Graphics.EnumerateMetafileProc callback, IntPtr callbackData, ImageAttributes imageAttr)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in the specified <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified parallelogram.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoints">Array of three <see cref="T:System.Drawing.PointF" /> structures that define a parallelogram that determines the size and location of the drawn metafile. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		// Token: 0x060001F2 RID: 498 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, PointF[] destPoints, Graphics.EnumerateMetafileProc callback)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in the specified <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified parallelogram.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoints">Array of three <see cref="T:System.Drawing.PointF" /> structures that define a parallelogram that determines the size and location of the drawn metafile. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		// Token: 0x060001F3 RID: 499 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, PointF[] destPoints, Graphics.EnumerateMetafileProc callback, IntPtr callbackData)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in the specified <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified parallelogram using specified image attributes.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoints">Array of three <see cref="T:System.Drawing.PointF" /> structures that define a parallelogram that determines the size and location of the drawn metafile. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		/// <param name="imageAttr">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies image attribute information for the drawn image. </param>
		// Token: 0x060001F4 RID: 500 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, PointF[] destPoints, Graphics.EnumerateMetafileProc callback, IntPtr callbackData, ImageAttributes imageAttr)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in a selected rectangle from a <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified parallelogram.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoints">Array of three <see cref="T:System.Drawing.PointF" /> structures that define a parallelogram that determines the size and location of the drawn metafile. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.RectangleF" /> structures that specifies the portion of the metafile, relative to its upper-left corner, to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the unit of measure used to determine the portion of the metafile that the rectangle specified by the <paramref name="srcRect" /> parameter contains. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		// Token: 0x060001F5 RID: 501 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, PointF[] destPoints, RectangleF srcRect, GraphicsUnit srcUnit, Graphics.EnumerateMetafileProc callback)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in a selected rectangle from a <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified parallelogram.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoints">Array of three <see cref="T:System.Drawing.PointF" /> structures that define a parallelogram that determines the size and location of the drawn metafile. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the portion of the metafile, relative to its upper-left corner, to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the unit of measure used to determine the portion of the metafile that the rectangle specified by the <paramref name="srcRect" /> parameter contains. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		// Token: 0x060001F6 RID: 502 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, PointF[] destPoints, RectangleF srcRect, GraphicsUnit srcUnit, Graphics.EnumerateMetafileProc callback, IntPtr callbackData)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in a selected rectangle from a <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified parallelogram using specified image attributes.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoints">Array of three <see cref="T:System.Drawing.PointF" /> structures that define a parallelogram that determines the size and location of the drawn metafile. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the portion of the metafile, relative to its upper-left corner, to draw. </param>
		/// <param name="unit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the unit of measure used to determine the portion of the metafile that the rectangle specified by the <paramref name="srcRect" /> parameter contains. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		/// <param name="imageAttr">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies image attribute information for the drawn image. </param>
		// Token: 0x060001F7 RID: 503 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, PointF[] destPoints, RectangleF srcRect, GraphicsUnit unit, Graphics.EnumerateMetafileProc callback, IntPtr callbackData, ImageAttributes imageAttr)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in the specified <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified parallelogram.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoints">Array of three <see cref="T:System.Drawing.Point" /> structures that define a parallelogram that determines the size and location of the drawn metafile. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		// Token: 0x060001F8 RID: 504 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, Point[] destPoints, Graphics.EnumerateMetafileProc callback)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in the specified <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified parallelogram.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoints">Array of three <see cref="T:System.Drawing.Point" /> structures that define a parallelogram that determines the size and location of the drawn metafile. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		// Token: 0x060001F9 RID: 505 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, Point[] destPoints, Graphics.EnumerateMetafileProc callback, IntPtr callbackData)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in the specified <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified parallelogram using specified image attributes.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoints">Array of three <see cref="T:System.Drawing.Point" /> structures that define a parallelogram that determines the size and location of the drawn metafile. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		/// <param name="imageAttr">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies image attribute information for the drawn image. </param>
		// Token: 0x060001FA RID: 506 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, Point[] destPoints, Graphics.EnumerateMetafileProc callback, IntPtr callbackData, ImageAttributes imageAttr)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in a selected rectangle from a <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified parallelogram.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoints">Array of three <see cref="T:System.Drawing.Point" /> structures that define a parallelogram that determines the size and location of the drawn metafile. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the portion of the metafile, relative to its upper-left corner, to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the unit of measure used to determine the portion of the metafile that the rectangle specified by the <paramref name="srcRect" /> parameter contains. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		// Token: 0x060001FB RID: 507 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, Point[] destPoints, Rectangle srcRect, GraphicsUnit srcUnit, Graphics.EnumerateMetafileProc callback)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in a selected rectangle from a <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified parallelogram.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoints">Array of three <see cref="T:System.Drawing.Point" /> structures that define a parallelogram that determines the size and location of the drawn metafile. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the portion of the metafile, relative to its upper-left corner, to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the unit of measure used to determine the portion of the metafile that the rectangle specified by the <paramref name="srcRect" /> parameter contains. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		// Token: 0x060001FC RID: 508 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, Point[] destPoints, Rectangle srcRect, GraphicsUnit srcUnit, Graphics.EnumerateMetafileProc callback, IntPtr callbackData)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records in a selected rectangle from a <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified parallelogram using specified image attributes.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destPoints">Array of three <see cref="T:System.Drawing.Point" /> structures that define a parallelogram that determines the size and location of the drawn metafile. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the portion of the metafile, relative to its upper-left corner, to draw. </param>
		/// <param name="unit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the unit of measure used to determine the portion of the metafile that the rectangle specified by the <paramref name="srcRect" /> parameter contains. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		/// <param name="imageAttr">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies image attribute information for the drawn image. </param>
		// Token: 0x060001FD RID: 509 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, Point[] destPoints, Rectangle srcRect, GraphicsUnit unit, Graphics.EnumerateMetafileProc callback, IntPtr callbackData, ImageAttributes imageAttr)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records of the specified <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified rectangle.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the location and size of the drawn metafile. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		// Token: 0x060001FE RID: 510 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, Rectangle destRect, Graphics.EnumerateMetafileProc callback)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records of the specified <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified rectangle.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the location and size of the drawn metafile. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		// Token: 0x060001FF RID: 511 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, Rectangle destRect, Graphics.EnumerateMetafileProc callback, IntPtr callbackData)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records of the specified <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified rectangle using specified image attributes.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the location and size of the drawn metafile. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		/// <param name="imageAttr">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies image attribute information for the drawn image. </param>
		// Token: 0x06000200 RID: 512 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, Rectangle destRect, Graphics.EnumerateMetafileProc callback, IntPtr callbackData, ImageAttributes imageAttr)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records of a selected rectangle from a <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified rectangle.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the location and size of the drawn metafile. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the portion of the metafile, relative to its upper-left corner, to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the unit of measure used to determine the portion of the metafile that the rectangle specified by the <paramref name="srcRect" /> parameter contains. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		// Token: 0x06000201 RID: 513 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, Rectangle destRect, Rectangle srcRect, GraphicsUnit srcUnit, Graphics.EnumerateMetafileProc callback)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records of a selected rectangle from a <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified rectangle.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the location and size of the drawn metafile. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the portion of the metafile, relative to its upper-left corner, to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the unit of measure used to determine the portion of the metafile that the rectangle specified by the <paramref name="srcRect" /> parameter contains. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		// Token: 0x06000202 RID: 514 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, Rectangle destRect, Rectangle srcRect, GraphicsUnit srcUnit, Graphics.EnumerateMetafileProc callback, IntPtr callbackData)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records of a selected rectangle from a <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified rectangle using specified image attributes.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the location and size of the drawn metafile. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the portion of the metafile, relative to its upper-left corner, to draw. </param>
		/// <param name="unit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the unit of measure used to determine the portion of the metafile that the rectangle specified by the <paramref name="srcRect" /> parameter contains. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		/// <param name="imageAttr">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies image attribute information for the drawn image. </param>
		// Token: 0x06000203 RID: 515 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, Rectangle destRect, Rectangle srcRect, GraphicsUnit unit, Graphics.EnumerateMetafileProc callback, IntPtr callbackData, ImageAttributes imageAttr)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records of the specified <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified rectangle.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destRect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the location and size of the drawn metafile. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		// Token: 0x06000204 RID: 516 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, RectangleF destRect, Graphics.EnumerateMetafileProc callback)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records of the specified <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified rectangle.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destRect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the location and size of the drawn metafile. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		// Token: 0x06000205 RID: 517 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, RectangleF destRect, Graphics.EnumerateMetafileProc callback, IntPtr callbackData)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records of the specified <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified rectangle using specified image attributes.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destRect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the location and size of the drawn metafile. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		/// <param name="imageAttr">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies image attribute information for the drawn image. </param>
		// Token: 0x06000206 RID: 518 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, RectangleF destRect, Graphics.EnumerateMetafileProc callback, IntPtr callbackData, ImageAttributes imageAttr)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records of a selected rectangle from a <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified rectangle.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destRect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the location and size of the drawn metafile. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the portion of the metafile, relative to its upper-left corner, to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the unit of measure used to determine the portion of the metafile that the rectangle specified by the <paramref name="srcRect" /> parameter contains. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		// Token: 0x06000207 RID: 519 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, RectangleF destRect, RectangleF srcRect, GraphicsUnit srcUnit, Graphics.EnumerateMetafileProc callback)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records of a selected rectangle from a <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified rectangle.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destRect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the location and size of the drawn metafile. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the portion of the metafile, relative to its upper-left corner, to draw. </param>
		/// <param name="srcUnit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the unit of measure used to determine the portion of the metafile that the rectangle specified by the <paramref name="srcRect" /> parameter contains. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		// Token: 0x06000208 RID: 520 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, RectangleF destRect, RectangleF srcRect, GraphicsUnit srcUnit, Graphics.EnumerateMetafileProc callback, IntPtr callbackData)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sends the records of a selected rectangle from a <see cref="T:System.Drawing.Imaging.Metafile" />, one at a time, to a callback method for display in a specified rectangle using specified image attributes.</summary>
		/// <param name="metafile">
		///       <see cref="T:System.Drawing.Imaging.Metafile" /> to enumerate. </param>
		/// <param name="destRect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the location and size of the drawn metafile. </param>
		/// <param name="srcRect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the portion of the metafile, relative to its upper-left corner, to draw. </param>
		/// <param name="unit">Member of the <see cref="T:System.Drawing.GraphicsUnit" /> enumeration that specifies the unit of measure used to determine the portion of the metafile that the rectangle specified by the <paramref name="srcRect" /> parameter contains. </param>
		/// <param name="callback">
		///       <see cref="T:System.Drawing.Graphics.EnumerateMetafileProc" /> delegate that specifies the method to which the metafile records are sent. </param>
		/// <param name="callbackData">Internal pointer that is required, but ignored. You can pass <see cref="F:System.IntPtr.Zero" /> for this parameter. </param>
		/// <param name="imageAttr">
		///       <see cref="T:System.Drawing.Imaging.ImageAttributes" /> that specifies image attribute information for the drawn image. </param>
		// Token: 0x06000209 RID: 521 RVA: 0x00004644 File Offset: 0x00002844
		public void EnumerateMetafile(Metafile metafile, RectangleF destRect, RectangleF srcRect, GraphicsUnit unit, Graphics.EnumerateMetafileProc callback, IntPtr callbackData, ImageAttributes imageAttr)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates the clip region of this <see cref="T:System.Drawing.Graphics" /> to exclude the area specified by a <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <param name="rect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that specifies the rectangle to exclude from the clip region. </param>
		// Token: 0x0600020A RID: 522 RVA: 0x00004644 File Offset: 0x00002844
		public void ExcludeClip(Rectangle rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates the clip region of this <see cref="T:System.Drawing.Graphics" /> to exclude the area specified by a <see cref="T:System.Drawing.Region" />.</summary>
		/// <param name="region">
		///       <see cref="T:System.Drawing.Region" /> that specifies the region to exclude from the clip region. </param>
		// Token: 0x0600020B RID: 523 RVA: 0x00004644 File Offset: 0x00002844
		public void ExcludeClip(Region region)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interior of a closed cardinal spline curve defined by an array of <see cref="T:System.Drawing.PointF" /> structures.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.PointF" /> structures that define the spline. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x0600020C RID: 524 RVA: 0x00004644 File Offset: 0x00002844
		public void FillClosedCurve(Brush brush, PointF[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interior of a closed cardinal spline curve defined by an array of <see cref="T:System.Drawing.PointF" /> structures using the specified fill mode.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.PointF" /> structures that define the spline. </param>
		/// <param name="fillmode">Member of the <see cref="T:System.Drawing.Drawing2D.FillMode" /> enumeration that determines how the curve is filled.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x0600020D RID: 525 RVA: 0x00004644 File Offset: 0x00002844
		public void FillClosedCurve(Brush brush, PointF[] points, FillMode fillmode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interior of a closed cardinal spline curve defined by an array of <see cref="T:System.Drawing.PointF" /> structures using the specified fill mode and tension.</summary>
		/// <param name="brush">A <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.PointF" /> structures that define the spline. </param>
		/// <param name="fillmode">Member of the <see cref="T:System.Drawing.Drawing2D.FillMode" /> enumeration that determines how the curve is filled. </param>
		/// <param name="tension">Value greater than or equal to 0.0F that specifies the tension of the curve. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x0600020E RID: 526 RVA: 0x00004644 File Offset: 0x00002844
		public void FillClosedCurve(Brush brush, PointF[] points, FillMode fillmode, float tension)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interior of a closed cardinal spline curve defined by an array of <see cref="T:System.Drawing.Point" /> structures.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.Point" /> structures that define the spline. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x0600020F RID: 527 RVA: 0x00004644 File Offset: 0x00002844
		public void FillClosedCurve(Brush brush, Point[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interior of a closed cardinal spline curve defined by an array of <see cref="T:System.Drawing.Point" /> structures using the specified fill mode.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.Point" /> structures that define the spline. </param>
		/// <param name="fillmode">Member of the <see cref="T:System.Drawing.Drawing2D.FillMode" /> enumeration that determines how the curve is filled.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x06000210 RID: 528 RVA: 0x00004644 File Offset: 0x00002844
		public void FillClosedCurve(Brush brush, Point[] points, FillMode fillmode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interior of a closed cardinal spline curve defined by an array of <see cref="T:System.Drawing.Point" /> structures using the specified fill mode and tension.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.Point" /> structures that define the spline. </param>
		/// <param name="fillmode">Member of the <see cref="T:System.Drawing.Drawing2D.FillMode" /> enumeration that determines how the curve is filled. </param>
		/// <param name="tension">Value greater than or equal to 0.0F that specifies the tension of the curve. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x06000211 RID: 529 RVA: 0x00004644 File Offset: 0x00002844
		public void FillClosedCurve(Brush brush, Point[] points, FillMode fillmode, float tension)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interior of an ellipse defined by a bounding rectangle specified by a <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="rect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that represents the bounding rectangle that defines the ellipse. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.</exception>
		// Token: 0x06000212 RID: 530 RVA: 0x00004644 File Offset: 0x00002844
		public void FillEllipse(Brush brush, Rectangle rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interior of an ellipse defined by a bounding rectangle specified by a <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="rect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that represents the bounding rectangle that defines the ellipse. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.</exception>
		// Token: 0x06000213 RID: 531 RVA: 0x00004644 File Offset: 0x00002844
		public void FillEllipse(Brush brush, RectangleF rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interior of an ellipse defined by a bounding rectangle specified by a pair of coordinates, a width, and a height.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the bounding rectangle that defines the ellipse. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the bounding rectangle that defines the ellipse. </param>
		/// <param name="width">Width of the bounding rectangle that defines the ellipse. </param>
		/// <param name="height">Height of the bounding rectangle that defines the ellipse. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.</exception>
		// Token: 0x06000214 RID: 532 RVA: 0x00004644 File Offset: 0x00002844
		public void FillEllipse(Brush brush, int x, int y, int width, int height)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interior of an ellipse defined by a bounding rectangle specified by a pair of coordinates, a width, and a height.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the bounding rectangle that defines the ellipse. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the bounding rectangle that defines the ellipse. </param>
		/// <param name="width">Width of the bounding rectangle that defines the ellipse. </param>
		/// <param name="height">Height of the bounding rectangle that defines the ellipse. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.</exception>
		// Token: 0x06000215 RID: 533 RVA: 0x00004644 File Offset: 0x00002844
		public void FillEllipse(Brush brush, float x, float y, float width, float height)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interior of a <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="path">
		///       <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> that represents the path to fill. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.-or-
		///         <paramref name="path" /> is <see langword="null" />.</exception>
		// Token: 0x06000216 RID: 534 RVA: 0x00004644 File Offset: 0x00002844
		public void FillPath(Brush brush, GraphicsPath path)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interior of a pie section defined by an ellipse specified by a <see cref="T:System.Drawing.RectangleF" /> structure and two radial lines.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="rect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that represents the bounding rectangle that defines the ellipse from which the pie section comes. </param>
		/// <param name="startAngle">Angle in degrees measured clockwise from the x-axis to the first side of the pie section. </param>
		/// <param name="sweepAngle">Angle in degrees measured clockwise from the <paramref name="startAngle" /> parameter to the second side of the pie section. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.</exception>
		// Token: 0x06000217 RID: 535 RVA: 0x00004644 File Offset: 0x00002844
		public void FillPie(Brush brush, Rectangle rect, float startAngle, float sweepAngle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interior of a pie section defined by an ellipse specified by a pair of coordinates, a width, a height, and two radial lines.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the bounding rectangle that defines the ellipse from which the pie section comes. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the bounding rectangle that defines the ellipse from which the pie section comes. </param>
		/// <param name="width">Width of the bounding rectangle that defines the ellipse from which the pie section comes. </param>
		/// <param name="height">Height of the bounding rectangle that defines the ellipse from which the pie section comes. </param>
		/// <param name="startAngle">Angle in degrees measured clockwise from the x-axis to the first side of the pie section. </param>
		/// <param name="sweepAngle">Angle in degrees measured clockwise from the <paramref name="startAngle" /> parameter to the second side of the pie section. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.</exception>
		// Token: 0x06000218 RID: 536 RVA: 0x00004644 File Offset: 0x00002844
		public void FillPie(Brush brush, int x, int y, int width, int height, int startAngle, int sweepAngle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interior of a pie section defined by an ellipse specified by a pair of coordinates, a width, a height, and two radial lines.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the bounding rectangle that defines the ellipse from which the pie section comes. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the bounding rectangle that defines the ellipse from which the pie section comes. </param>
		/// <param name="width">Width of the bounding rectangle that defines the ellipse from which the pie section comes. </param>
		/// <param name="height">Height of the bounding rectangle that defines the ellipse from which the pie section comes. </param>
		/// <param name="startAngle">Angle in degrees measured clockwise from the x-axis to the first side of the pie section. </param>
		/// <param name="sweepAngle">Angle in degrees measured clockwise from the <paramref name="startAngle" /> parameter to the second side of the pie section. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.</exception>
		// Token: 0x06000219 RID: 537 RVA: 0x00004644 File Offset: 0x00002844
		public void FillPie(Brush brush, float x, float y, float width, float height, float startAngle, float sweepAngle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interior of a polygon defined by an array of points specified by <see cref="T:System.Drawing.PointF" /> structures.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.PointF" /> structures that represent the vertices of the polygon to fill. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x0600021A RID: 538 RVA: 0x00004644 File Offset: 0x00002844
		public void FillPolygon(Brush brush, PointF[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interior of a polygon defined by an array of points specified by <see cref="T:System.Drawing.PointF" /> structures using the specified fill mode.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.PointF" /> structures that represent the vertices of the polygon to fill. </param>
		/// <param name="fillMode">Member of the <see cref="T:System.Drawing.Drawing2D.FillMode" /> enumeration that determines the style of the fill. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x0600021B RID: 539 RVA: 0x00004644 File Offset: 0x00002844
		public void FillPolygon(Brush brush, PointF[] points, FillMode fillMode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interior of a polygon defined by an array of points specified by <see cref="T:System.Drawing.Point" /> structures.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.Point" /> structures that represent the vertices of the polygon to fill. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x0600021C RID: 540 RVA: 0x00004644 File Offset: 0x00002844
		public void FillPolygon(Brush brush, Point[] points)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interior of a polygon defined by an array of points specified by <see cref="T:System.Drawing.Point" /> structures using the specified fill mode.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="points">Array of <see cref="T:System.Drawing.Point" /> structures that represent the vertices of the polygon to fill. </param>
		/// <param name="fillMode">Member of the <see cref="T:System.Drawing.Drawing2D.FillMode" /> enumeration that determines the style of the fill. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.-or-
		///         <paramref name="points" /> is <see langword="null" />.</exception>
		// Token: 0x0600021D RID: 541 RVA: 0x00004644 File Offset: 0x00002844
		public void FillPolygon(Brush brush, Point[] points, FillMode fillMode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interior of a rectangle specified by a <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="rect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that represents the rectangle to fill. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.</exception>
		// Token: 0x0600021E RID: 542 RVA: 0x00004644 File Offset: 0x00002844
		public void FillRectangle(Brush brush, Rectangle rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interior of a rectangle specified by a <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="rect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that represents the rectangle to fill. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.</exception>
		// Token: 0x0600021F RID: 543 RVA: 0x00004644 File Offset: 0x00002844
		public void FillRectangle(Brush brush, RectangleF rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interior of a rectangle specified by a pair of coordinates, a width, and a height.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the rectangle to fill. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the rectangle to fill. </param>
		/// <param name="width">Width of the rectangle to fill. </param>
		/// <param name="height">Height of the rectangle to fill. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.</exception>
		// Token: 0x06000220 RID: 544 RVA: 0x00004644 File Offset: 0x00002844
		public void FillRectangle(Brush brush, int x, int y, int width, int height)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interior of a rectangle specified by a pair of coordinates, a width, and a height.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="x">The x-coordinate of the upper-left corner of the rectangle to fill. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the rectangle to fill. </param>
		/// <param name="width">Width of the rectangle to fill. </param>
		/// <param name="height">Height of the rectangle to fill. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.</exception>
		// Token: 0x06000221 RID: 545 RVA: 0x00004644 File Offset: 0x00002844
		public void FillRectangle(Brush brush, float x, float y, float width, float height)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interiors of a series of rectangles specified by <see cref="T:System.Drawing.RectangleF" /> structures.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="rects">Array of <see cref="T:System.Drawing.RectangleF" /> structures that represent the rectangles to fill. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.-or-
		///         <paramref name="rects" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="Rects" /> is a zero-length array.</exception>
		// Token: 0x06000222 RID: 546 RVA: 0x00004644 File Offset: 0x00002844
		public void FillRectangles(Brush brush, RectangleF[] rects)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interiors of a series of rectangles specified by <see cref="T:System.Drawing.Rectangle" /> structures.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="rects">Array of <see cref="T:System.Drawing.Rectangle" /> structures that represent the rectangles to fill. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.-or-
		///         <paramref name="rects" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="rects" /> is a zero-length array.</exception>
		// Token: 0x06000223 RID: 547 RVA: 0x00004644 File Offset: 0x00002844
		public void FillRectangles(Brush brush, Rectangle[] rects)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Fills the interior of a <see cref="T:System.Drawing.Region" />.</summary>
		/// <param name="brush">
		///       <see cref="T:System.Drawing.Brush" /> that determines the characteristics of the fill. </param>
		/// <param name="region">
		///       <see cref="T:System.Drawing.Region" /> that represents the area to fill. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="brush" /> is <see langword="null" />.-or-
		///         <paramref name="region" /> is <see langword="null" />.</exception>
		// Token: 0x06000224 RID: 548 RVA: 0x00004644 File Offset: 0x00002844
		public void FillRegion(Brush brush, Region region)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Forces execution of all pending graphics operations and returns immediately without waiting for the operations to finish.</summary>
		// Token: 0x06000225 RID: 549 RVA: 0x00004644 File Offset: 0x00002844
		public void Flush()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Forces execution of all pending graphics operations with the method waiting or not waiting, as specified, to return before the operations finish.</summary>
		/// <param name="intention">Member of the <see cref="T:System.Drawing.Drawing2D.FlushIntention" /> enumeration that specifies whether the method returns immediately or waits for any existing operations to finish. </param>
		// Token: 0x06000226 RID: 550 RVA: 0x00004644 File Offset: 0x00002844
		public void Flush(FlushIntention intention)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a new <see cref="T:System.Drawing.Graphics" /> from the specified handle to a device context.</summary>
		/// <param name="hdc">Handle to a device context. </param>
		/// <returns>This method returns a new <see cref="T:System.Drawing.Graphics" /> for the specified device context.</returns>
		// Token: 0x06000227 RID: 551 RVA: 0x00004667 File Offset: 0x00002867
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public static Graphics FromHdc(IntPtr hdc)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates a new <see cref="T:System.Drawing.Graphics" /> from the specified handle to a device context and handle to a device.</summary>
		/// <param name="hdc">Handle to a device context. </param>
		/// <param name="hdevice">Handle to a device. </param>
		/// <returns>This method returns a new <see cref="T:System.Drawing.Graphics" /> for the specified device context and device.</returns>
		// Token: 0x06000228 RID: 552 RVA: 0x00004667 File Offset: 0x00002867
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public static Graphics FromHdc(IntPtr hdc, IntPtr hdevice)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns a <see cref="T:System.Drawing.Graphics" /> for the specified device context.</summary>
		/// <param name="hdc">Handle to a device context. </param>
		/// <returns>A <see cref="T:System.Drawing.Graphics" /> for the specified device context.</returns>
		// Token: 0x06000229 RID: 553 RVA: 0x00004667 File Offset: 0x00002867
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public static Graphics FromHdcInternal(IntPtr hdc)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates a new <see cref="T:System.Drawing.Graphics" /> from the specified handle to a window.</summary>
		/// <param name="hwnd">Handle to a window. </param>
		/// <returns>This method returns a new <see cref="T:System.Drawing.Graphics" /> for the specified window handle.</returns>
		// Token: 0x0600022A RID: 554 RVA: 0x00004667 File Offset: 0x00002867
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public static Graphics FromHwnd(IntPtr hwnd)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates a new <see cref="T:System.Drawing.Graphics" /> for the specified windows handle.</summary>
		/// <param name="hwnd">Handle to a window. </param>
		/// <returns>A <see cref="T:System.Drawing.Graphics" /> for the specified window handle.</returns>
		// Token: 0x0600022B RID: 555 RVA: 0x00004667 File Offset: 0x00002867
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public static Graphics FromHwndInternal(IntPtr hwnd)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates a new <see cref="T:System.Drawing.Graphics" /> from the specified <see cref="T:System.Drawing.Image" />.</summary>
		/// <param name="image">
		///       <see cref="T:System.Drawing.Image" /> from which to create the new <see cref="T:System.Drawing.Graphics" />. </param>
		/// <returns>This method returns a new <see cref="T:System.Drawing.Graphics" /> for the specified <see cref="T:System.Drawing.Image" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="image" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.Exception">
		///         <paramref name="image" /> has an indexed pixel format or its format is undefined.</exception>
		// Token: 0x0600022C RID: 556 RVA: 0x00004667 File Offset: 0x00002867
		public static Graphics FromImage(Image image)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the cumulative graphics context.</summary>
		/// <returns>An <see cref="T:System.Object" /> representing the cumulative graphics context.</returns>
		// Token: 0x0600022D RID: 557 RVA: 0x00004667 File Offset: 0x00002867
		[EditorBrowsable(EditorBrowsableState.Never)]
		[StrongNameIdentityPermission(SecurityAction.LinkDemand, Name = "System.Windows.Forms", PublicKey = "0x00000000000000000400000000000000")]
		public object GetContextInfo()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets a handle to the current Windows halftone palette.</summary>
		/// <returns>Internal pointer that specifies the handle to the palette.</returns>
		// Token: 0x0600022E RID: 558 RVA: 0x000048BC File Offset: 0x00002ABC
		public static IntPtr GetHalftonePalette()
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Gets the handle to the device context associated with this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <returns>Handle to the device context associated with this <see cref="T:System.Drawing.Graphics" />.</returns>
		// Token: 0x0600022F RID: 559 RVA: 0x000048D8 File Offset: 0x00002AD8
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public IntPtr GetHdc()
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Gets the nearest color to the specified <see cref="T:System.Drawing.Color" /> structure.</summary>
		/// <param name="color">
		///       <see cref="T:System.Drawing.Color" /> structure for which to find a match. </param>
		/// <returns>A <see cref="T:System.Drawing.Color" /> structure that represents the nearest color to the one specified with the <paramref name="color" /> parameter.</returns>
		// Token: 0x06000230 RID: 560 RVA: 0x000048F4 File Offset: 0x00002AF4
		public Color GetNearestColor(Color color)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(Color);
		}

		/// <summary>Updates the clip region of this <see cref="T:System.Drawing.Graphics" /> to the intersection of the current clip region and the specified <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <param name="rect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure to intersect with the current clip region. </param>
		// Token: 0x06000231 RID: 561 RVA: 0x00004644 File Offset: 0x00002844
		public void IntersectClip(Rectangle rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates the clip region of this <see cref="T:System.Drawing.Graphics" /> to the intersection of the current clip region and the specified <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <param name="rect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure to intersect with the current clip region. </param>
		// Token: 0x06000232 RID: 562 RVA: 0x00004644 File Offset: 0x00002844
		public void IntersectClip(RectangleF rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Updates the clip region of this <see cref="T:System.Drawing.Graphics" /> to the intersection of the current clip region and the specified <see cref="T:System.Drawing.Region" />.</summary>
		/// <param name="region">
		///       <see cref="T:System.Drawing.Region" /> to intersect with the current region. </param>
		// Token: 0x06000233 RID: 563 RVA: 0x00004644 File Offset: 0x00002844
		public void IntersectClip(Region region)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Indicates whether the specified <see cref="T:System.Drawing.Point" /> structure is contained within the visible clip region of this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="point">
		///       <see cref="T:System.Drawing.Point" /> structure to test for visibility. </param>
		/// <returns>
		///     <see langword="true" /> if the point specified by the <paramref name="point" /> parameter is contained within the visible clip region of this <see cref="T:System.Drawing.Graphics" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000234 RID: 564 RVA: 0x00004910 File Offset: 0x00002B10
		public bool IsVisible(Point point)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the specified <see cref="T:System.Drawing.PointF" /> structure is contained within the visible clip region of this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="point">
		///       <see cref="T:System.Drawing.PointF" /> structure to test for visibility. </param>
		/// <returns>
		///     <see langword="true" /> if the point specified by the <paramref name="point" /> parameter is contained within the visible clip region of this <see cref="T:System.Drawing.Graphics" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000235 RID: 565 RVA: 0x0000492C File Offset: 0x00002B2C
		public bool IsVisible(PointF point)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the rectangle specified by a <see cref="T:System.Drawing.Rectangle" /> structure is contained within the visible clip region of this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="rect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure to test for visibility. </param>
		/// <returns>
		///     <see langword="true" /> if the rectangle specified by the <paramref name="rect" /> parameter is contained within the visible clip region of this <see cref="T:System.Drawing.Graphics" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000236 RID: 566 RVA: 0x00004948 File Offset: 0x00002B48
		public bool IsVisible(Rectangle rect)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the rectangle specified by a <see cref="T:System.Drawing.RectangleF" /> structure is contained within the visible clip region of this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="rect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure to test for visibility. </param>
		/// <returns>
		///     <see langword="true" /> if the rectangle specified by the <paramref name="rect" /> parameter is contained within the visible clip region of this <see cref="T:System.Drawing.Graphics" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000237 RID: 567 RVA: 0x00004964 File Offset: 0x00002B64
		public bool IsVisible(RectangleF rect)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the point specified by a pair of coordinates is contained within the visible clip region of this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="x">The x-coordinate of the point to test for visibility. </param>
		/// <param name="y">The y-coordinate of the point to test for visibility. </param>
		/// <returns>
		///     <see langword="true" /> if the point defined by the <paramref name="x" /> and <paramref name="y" /> parameters is contained within the visible clip region of this <see cref="T:System.Drawing.Graphics" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000238 RID: 568 RVA: 0x00004980 File Offset: 0x00002B80
		public bool IsVisible(int x, int y)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the rectangle specified by a pair of coordinates, a width, and a height is contained within the visible clip region of this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="x">The x-coordinate of the upper-left corner of the rectangle to test for visibility. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the rectangle to test for visibility.</param>
		/// <param name="width">Width of the rectangle to test for visibility. </param>
		/// <param name="height">Height of the rectangle to test for visibility. </param>
		/// <returns>
		///     <see langword="true" /> if the rectangle defined by the <paramref name="x" />, <paramref name="y" />, <paramref name="width" />, and <paramref name="height" /> parameters is contained within the visible clip region of this <see cref="T:System.Drawing.Graphics" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000239 RID: 569 RVA: 0x0000499C File Offset: 0x00002B9C
		public bool IsVisible(int x, int y, int width, int height)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the point specified by a pair of coordinates is contained within the visible clip region of this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="x">The x-coordinate of the point to test for visibility. </param>
		/// <param name="y">The y-coordinate of the point to test for visibility. </param>
		/// <returns>
		///     <see langword="true" /> if the point defined by the <paramref name="x" /> and <paramref name="y" /> parameters is contained within the visible clip region of this <see cref="T:System.Drawing.Graphics" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600023A RID: 570 RVA: 0x000049B8 File Offset: 0x00002BB8
		public bool IsVisible(float x, float y)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the rectangle specified by a pair of coordinates, a width, and a height is contained within the visible clip region of this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="x">The x-coordinate of the upper-left corner of the rectangle to test for visibility. </param>
		/// <param name="y">The y-coordinate of the upper-left corner of the rectangle to test for visibility. </param>
		/// <param name="width">Width of the rectangle to test for visibility. </param>
		/// <param name="height">Height of the rectangle to test for visibility. </param>
		/// <returns>
		///     <see langword="true" /> if the rectangle defined by the <paramref name="x" />, <paramref name="y" />, <paramref name="width" />, and <paramref name="height" /> parameters is contained within the visible clip region of this <see cref="T:System.Drawing.Graphics" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600023B RID: 571 RVA: 0x000049D4 File Offset: 0x00002BD4
		public bool IsVisible(float x, float y, float width, float height)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Gets an array of <see cref="T:System.Drawing.Region" /> objects, each of which bounds a range of character positions within the specified string.</summary>
		/// <param name="text">String to measure. </param>
		/// <param name="font">
		///       <see cref="T:System.Drawing.Font" /> that defines the text format of the string. </param>
		/// <param name="layoutRect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that specifies the layout rectangle for the string. </param>
		/// <param name="stringFormat">
		///       <see cref="T:System.Drawing.StringFormat" /> that represents formatting information, such as line spacing, for the string. </param>
		/// <returns>This method returns an array of <see cref="T:System.Drawing.Region" /> objects, each of which bounds a range of character positions within the specified string.</returns>
		// Token: 0x0600023C RID: 572 RVA: 0x00004667 File Offset: 0x00002867
		public Region[] MeasureCharacterRanges(string text, Font font, RectangleF layoutRect, StringFormat stringFormat)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Measures the specified string when drawn with the specified <see cref="T:System.Drawing.Font" />.</summary>
		/// <param name="text">String to measure. </param>
		/// <param name="font">
		///       <see cref="T:System.Drawing.Font" /> that defines the text format of the string. </param>
		/// <returns>This method returns a <see cref="T:System.Drawing.SizeF" /> structure that represents the size, in the units specified by the <see cref="P:System.Drawing.Graphics.PageUnit" /> property, of the string specified by the <paramref name="text" /> parameter as drawn with the <paramref name="font" /> parameter.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="font" /> is <see langword="null" />.</exception>
		// Token: 0x0600023D RID: 573 RVA: 0x000049F0 File Offset: 0x00002BF0
		public SizeF MeasureString(string text, Font font)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(SizeF);
		}

		/// <summary>Measures the specified string when drawn with the specified <see cref="T:System.Drawing.Font" /> and formatted with the specified <see cref="T:System.Drawing.StringFormat" />.</summary>
		/// <param name="text">String to measure. </param>
		/// <param name="font">
		///       <see cref="T:System.Drawing.Font" /> defines the text format of the string. </param>
		/// <param name="origin">
		///       <see cref="T:System.Drawing.PointF" /> structure that represents the upper-left corner of the string. </param>
		/// <param name="stringFormat">
		///       <see cref="T:System.Drawing.StringFormat" /> that represents formatting information, such as line spacing, for the string. </param>
		/// <returns>This method returns a <see cref="T:System.Drawing.SizeF" /> structure that represents the size, in the units specified by the <see cref="P:System.Drawing.Graphics.PageUnit" /> property, of the string specified by the <paramref name="text" /> parameter as drawn with the <paramref name="font" /> parameter and the <paramref name="stringFormat" /> parameter.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="font" /> is <see langword="null" />.</exception>
		// Token: 0x0600023E RID: 574 RVA: 0x00004A0C File Offset: 0x00002C0C
		public SizeF MeasureString(string text, Font font, PointF origin, StringFormat stringFormat)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(SizeF);
		}

		/// <summary>Measures the specified string when drawn with the specified <see cref="T:System.Drawing.Font" /> within the specified layout area.</summary>
		/// <param name="text">String to measure. </param>
		/// <param name="font">
		///       <see cref="T:System.Drawing.Font" /> defines the text format of the string. </param>
		/// <param name="layoutArea">
		///       <see cref="T:System.Drawing.SizeF" /> structure that specifies the maximum layout area for the text. </param>
		/// <returns>This method returns a <see cref="T:System.Drawing.SizeF" /> structure that represents the size, in the units specified by the <see cref="P:System.Drawing.Graphics.PageUnit" /> property, of the string specified by the <paramref name="text" /> parameter as drawn with the <paramref name="font" /> parameter.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="font" /> is <see langword="null" />.</exception>
		// Token: 0x0600023F RID: 575 RVA: 0x00004A28 File Offset: 0x00002C28
		public SizeF MeasureString(string text, Font font, SizeF layoutArea)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(SizeF);
		}

		/// <summary>Measures the specified string when drawn with the specified <see cref="T:System.Drawing.Font" /> and formatted with the specified <see cref="T:System.Drawing.StringFormat" />.</summary>
		/// <param name="text">String to measure. </param>
		/// <param name="font">
		///       <see cref="T:System.Drawing.Font" /> defines the text format of the string. </param>
		/// <param name="layoutArea">
		///       <see cref="T:System.Drawing.SizeF" /> structure that specifies the maximum layout area for the text. </param>
		/// <param name="stringFormat">
		///       <see cref="T:System.Drawing.StringFormat" /> that represents formatting information, such as line spacing, for the string. </param>
		/// <returns>This method returns a <see cref="T:System.Drawing.SizeF" /> structure that represents the size, in the units specified by the <see cref="P:System.Drawing.Graphics.PageUnit" /> property, of the string specified in the <paramref name="text" /> parameter as drawn with the <paramref name="font" /> parameter and the <paramref name="stringFormat" /> parameter.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="font" /> is <see langword="null" />.</exception>
		// Token: 0x06000240 RID: 576 RVA: 0x00004A44 File Offset: 0x00002C44
		public SizeF MeasureString(string text, Font font, SizeF layoutArea, StringFormat stringFormat)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(SizeF);
		}

		/// <summary>Measures the specified string when drawn with the specified <see cref="T:System.Drawing.Font" /> and formatted with the specified <see cref="T:System.Drawing.StringFormat" />.</summary>
		/// <param name="text">String to measure. </param>
		/// <param name="font">
		///       <see cref="T:System.Drawing.Font" /> that defines the text format of the string. </param>
		/// <param name="layoutArea">
		///       <see cref="T:System.Drawing.SizeF" /> structure that specifies the maximum layout area for the text. </param>
		/// <param name="stringFormat">
		///       <see cref="T:System.Drawing.StringFormat" /> that represents formatting information, such as line spacing, for the string. </param>
		/// <param name="charactersFitted">Number of characters in the string. </param>
		/// <param name="linesFilled">Number of text lines in the string. </param>
		/// <returns>This method returns a <see cref="T:System.Drawing.SizeF" /> structure that represents the size of the string, in the units specified by the <see cref="P:System.Drawing.Graphics.PageUnit" /> property, of the <paramref name="text" /> parameter as drawn with the <paramref name="font" /> parameter and the <paramref name="stringFormat" /> parameter.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="font" /> is <see langword="null" />.</exception>
		// Token: 0x06000241 RID: 577 RVA: 0x00004A60 File Offset: 0x00002C60
		public SizeF MeasureString(string text, Font font, SizeF layoutArea, StringFormat stringFormat, out int charactersFitted, out int linesFilled)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(SizeF);
		}

		/// <summary>Measures the specified string when drawn with the specified <see cref="T:System.Drawing.Font" />.</summary>
		/// <param name="text">String to measure. </param>
		/// <param name="font">
		///       <see cref="T:System.Drawing.Font" /> that defines the format of the string. </param>
		/// <param name="width">Maximum width of the string in pixels. </param>
		/// <returns>This method returns a <see cref="T:System.Drawing.SizeF" /> structure that represents the size, in the units specified by the <see cref="P:System.Drawing.Graphics.PageUnit" /> property, of the string specified in the <paramref name="text" /> parameter as drawn with the <paramref name="font" /> parameter.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="font" /> is <see langword="null" />.</exception>
		// Token: 0x06000242 RID: 578 RVA: 0x00004A7C File Offset: 0x00002C7C
		public SizeF MeasureString(string text, Font font, int width)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(SizeF);
		}

		/// <summary>Measures the specified string when drawn with the specified <see cref="T:System.Drawing.Font" /> and formatted with the specified <see cref="T:System.Drawing.StringFormat" />.</summary>
		/// <param name="text">String to measure. </param>
		/// <param name="font">
		///       <see cref="T:System.Drawing.Font" /> that defines the text format of the string. </param>
		/// <param name="width">Maximum width of the string. </param>
		/// <param name="format">
		///       <see cref="T:System.Drawing.StringFormat" /> that represents formatting information, such as line spacing, for the string. </param>
		/// <returns>This method returns a <see cref="T:System.Drawing.SizeF" /> structure that represents the size, in the units specified by the <see cref="P:System.Drawing.Graphics.PageUnit" /> property, of the string specified in the <paramref name="text" /> parameter as drawn with the <paramref name="font" /> parameter and the <paramref name="stringFormat" /> parameter.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="font" /> is <see langword="null" />.</exception>
		// Token: 0x06000243 RID: 579 RVA: 0x00004A98 File Offset: 0x00002C98
		public SizeF MeasureString(string text, Font font, int width, StringFormat format)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(SizeF);
		}

		/// <summary>Multiplies the world transformation of this <see cref="T:System.Drawing.Graphics" /> and specified the <see cref="T:System.Drawing.Drawing2D.Matrix" />.</summary>
		/// <param name="matrix">4x4 <see cref="T:System.Drawing.Drawing2D.Matrix" /> that multiplies the world transformation. </param>
		// Token: 0x06000244 RID: 580 RVA: 0x00004644 File Offset: 0x00002844
		public void MultiplyTransform(Matrix matrix)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Multiplies the world transformation of this <see cref="T:System.Drawing.Graphics" /> and specified the <see cref="T:System.Drawing.Drawing2D.Matrix" /> in the specified order.</summary>
		/// <param name="matrix">4x4 <see cref="T:System.Drawing.Drawing2D.Matrix" /> that multiplies the world transformation. </param>
		/// <param name="order">Member of the <see cref="T:System.Drawing.Drawing2D.MatrixOrder" /> enumeration that determines the order of the multiplication. </param>
		// Token: 0x06000245 RID: 581 RVA: 0x00004644 File Offset: 0x00002844
		public void MultiplyTransform(Matrix matrix, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Releases a device context handle obtained by a previous call to the <see cref="M:System.Drawing.Graphics.GetHdc" /> method of this <see cref="T:System.Drawing.Graphics" />.</summary>
		// Token: 0x06000246 RID: 582 RVA: 0x00004644 File Offset: 0x00002844
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public void ReleaseHdc()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Releases a device context handle obtained by a previous call to the <see cref="M:System.Drawing.Graphics.GetHdc" /> method of this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="hdc">Handle to a device context obtained by a previous call to the <see cref="M:System.Drawing.Graphics.GetHdc" /> method of this <see cref="T:System.Drawing.Graphics" />. </param>
		// Token: 0x06000247 RID: 583 RVA: 0x00004644 File Offset: 0x00002844
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ReleaseHdc(IntPtr hdc)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Releases a handle to a device context.</summary>
		/// <param name="hdc">Handle to a device context. </param>
		// Token: 0x06000248 RID: 584 RVA: 0x00004644 File Offset: 0x00002844
		[EditorBrowsable(EditorBrowsableState.Never)]
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public void ReleaseHdcInternal(IntPtr hdc)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Resets the clip region of this <see cref="T:System.Drawing.Graphics" /> to an infinite region.</summary>
		// Token: 0x06000249 RID: 585 RVA: 0x00004644 File Offset: 0x00002844
		public void ResetClip()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Resets the world transformation matrix of this <see cref="T:System.Drawing.Graphics" /> to the identity matrix.</summary>
		// Token: 0x0600024A RID: 586 RVA: 0x00004644 File Offset: 0x00002844
		public void ResetTransform()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Restores the state of this <see cref="T:System.Drawing.Graphics" /> to the state represented by a <see cref="T:System.Drawing.Drawing2D.GraphicsState" />.</summary>
		/// <param name="gstate">
		///       <see cref="T:System.Drawing.Drawing2D.GraphicsState" /> that represents the state to which to restore this <see cref="T:System.Drawing.Graphics" />. </param>
		// Token: 0x0600024B RID: 587 RVA: 0x00004644 File Offset: 0x00002844
		public void Restore(GraphicsState gstate)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Applies the specified rotation to the transformation matrix of this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="angle">Angle of rotation in degrees. </param>
		// Token: 0x0600024C RID: 588 RVA: 0x00004644 File Offset: 0x00002844
		public void RotateTransform(float angle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Applies the specified rotation to the transformation matrix of this <see cref="T:System.Drawing.Graphics" /> in the specified order.</summary>
		/// <param name="angle">Angle of rotation in degrees. </param>
		/// <param name="order">Member of the <see cref="T:System.Drawing.Drawing2D.MatrixOrder" /> enumeration that specifies whether the rotation is appended or prepended to the matrix transformation. </param>
		// Token: 0x0600024D RID: 589 RVA: 0x00004644 File Offset: 0x00002844
		public void RotateTransform(float angle, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Saves the current state of this <see cref="T:System.Drawing.Graphics" /> and identifies the saved state with a <see cref="T:System.Drawing.Drawing2D.GraphicsState" />.</summary>
		/// <returns>This method returns a <see cref="T:System.Drawing.Drawing2D.GraphicsState" /> that represents the saved state of this <see cref="T:System.Drawing.Graphics" />.</returns>
		// Token: 0x0600024E RID: 590 RVA: 0x00004667 File Offset: 0x00002867
		public GraphicsState Save()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Applies the specified scaling operation to the transformation matrix of this <see cref="T:System.Drawing.Graphics" /> by prepending it to the object's transformation matrix.</summary>
		/// <param name="sx">Scale factor in the x direction. </param>
		/// <param name="sy">Scale factor in the y direction. </param>
		// Token: 0x0600024F RID: 591 RVA: 0x00004644 File Offset: 0x00002844
		public void ScaleTransform(float sx, float sy)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Applies the specified scaling operation to the transformation matrix of this <see cref="T:System.Drawing.Graphics" /> in the specified order.</summary>
		/// <param name="sx">Scale factor in the x direction. </param>
		/// <param name="sy">Scale factor in the y direction. </param>
		/// <param name="order">Member of the <see cref="T:System.Drawing.Drawing2D.MatrixOrder" /> enumeration that specifies whether the scaling operation is prepended or appended to the transformation matrix. </param>
		// Token: 0x06000250 RID: 592 RVA: 0x00004644 File Offset: 0x00002844
		public void ScaleTransform(float sx, float sy, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the clipping region of this <see cref="T:System.Drawing.Graphics" /> to the specified <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="path">
		///       <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> that represents the new clip region. </param>
		// Token: 0x06000251 RID: 593 RVA: 0x00004644 File Offset: 0x00002844
		public void SetClip(GraphicsPath path)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the clipping region of this <see cref="T:System.Drawing.Graphics" /> to the result of the specified operation combining the current clip region and the specified <see cref="T:System.Drawing.Drawing2D.GraphicsPath" />.</summary>
		/// <param name="path">
		///       <see cref="T:System.Drawing.Drawing2D.GraphicsPath" /> to combine. </param>
		/// <param name="combineMode">Member of the <see cref="T:System.Drawing.Drawing2D.CombineMode" /> enumeration that specifies the combining operation to use. </param>
		// Token: 0x06000252 RID: 594 RVA: 0x00004644 File Offset: 0x00002844
		public void SetClip(GraphicsPath path, CombineMode combineMode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the clipping region of this <see cref="T:System.Drawing.Graphics" /> to the <see langword="Clip" /> property of the specified <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="g">
		///       <see cref="T:System.Drawing.Graphics" /> from which to take the new clip region. </param>
		// Token: 0x06000253 RID: 595 RVA: 0x00004644 File Offset: 0x00002844
		public void SetClip(Graphics g)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the clipping region of this <see cref="T:System.Drawing.Graphics" /> to the result of the specified combining operation of the current clip region and the <see cref="P:System.Drawing.Graphics.Clip" /> property of the specified <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="g">
		///       <see cref="T:System.Drawing.Graphics" /> that specifies the clip region to combine. </param>
		/// <param name="combineMode">Member of the <see cref="T:System.Drawing.Drawing2D.CombineMode" /> enumeration that specifies the combining operation to use. </param>
		// Token: 0x06000254 RID: 596 RVA: 0x00004644 File Offset: 0x00002844
		public void SetClip(Graphics g, CombineMode combineMode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the clipping region of this <see cref="T:System.Drawing.Graphics" /> to the rectangle specified by a <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <param name="rect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure that represents the new clip region. </param>
		// Token: 0x06000255 RID: 597 RVA: 0x00004644 File Offset: 0x00002844
		public void SetClip(Rectangle rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the clipping region of this <see cref="T:System.Drawing.Graphics" /> to the result of the specified operation combining the current clip region and the rectangle specified by a <see cref="T:System.Drawing.Rectangle" /> structure.</summary>
		/// <param name="rect">
		///       <see cref="T:System.Drawing.Rectangle" /> structure to combine. </param>
		/// <param name="combineMode">Member of the <see cref="T:System.Drawing.Drawing2D.CombineMode" /> enumeration that specifies the combining operation to use. </param>
		// Token: 0x06000256 RID: 598 RVA: 0x00004644 File Offset: 0x00002844
		public void SetClip(Rectangle rect, CombineMode combineMode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the clipping region of this <see cref="T:System.Drawing.Graphics" /> to the rectangle specified by a <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <param name="rect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure that represents the new clip region. </param>
		// Token: 0x06000257 RID: 599 RVA: 0x00004644 File Offset: 0x00002844
		public void SetClip(RectangleF rect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the clipping region of this <see cref="T:System.Drawing.Graphics" /> to the result of the specified operation combining the current clip region and the rectangle specified by a <see cref="T:System.Drawing.RectangleF" /> structure.</summary>
		/// <param name="rect">
		///       <see cref="T:System.Drawing.RectangleF" /> structure to combine. </param>
		/// <param name="combineMode">Member of the <see cref="T:System.Drawing.Drawing2D.CombineMode" /> enumeration that specifies the combining operation to use. </param>
		// Token: 0x06000258 RID: 600 RVA: 0x00004644 File Offset: 0x00002844
		public void SetClip(RectangleF rect, CombineMode combineMode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the clipping region of this <see cref="T:System.Drawing.Graphics" /> to the result of the specified operation combining the current clip region and the specified <see cref="T:System.Drawing.Region" />.</summary>
		/// <param name="region">
		///       <see cref="T:System.Drawing.Region" /> to combine. </param>
		/// <param name="combineMode">Member from the <see cref="T:System.Drawing.Drawing2D.CombineMode" /> enumeration that specifies the combining operation to use. </param>
		// Token: 0x06000259 RID: 601 RVA: 0x00004644 File Offset: 0x00002844
		public void SetClip(Region region, CombineMode combineMode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Transforms an array of points from one coordinate space to another using the current world and page transformations of this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="destSpace">Member of the <see cref="T:System.Drawing.Drawing2D.CoordinateSpace" /> enumeration that specifies the destination coordinate space. </param>
		/// <param name="srcSpace">Member of the <see cref="T:System.Drawing.Drawing2D.CoordinateSpace" /> enumeration that specifies the source coordinate space. </param>
		/// <param name="pts">Array of <see cref="T:System.Drawing.PointF" /> structures that represent the points to transform. </param>
		// Token: 0x0600025A RID: 602 RVA: 0x00004644 File Offset: 0x00002844
		public void TransformPoints(CoordinateSpace destSpace, CoordinateSpace srcSpace, PointF[] pts)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Transforms an array of points from one coordinate space to another using the current world and page transformations of this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="destSpace">Member of the <see cref="T:System.Drawing.Drawing2D.CoordinateSpace" /> enumeration that specifies the destination coordinate space. </param>
		/// <param name="srcSpace">Member of the <see cref="T:System.Drawing.Drawing2D.CoordinateSpace" /> enumeration that specifies the source coordinate space. </param>
		/// <param name="pts">Array of <see cref="T:System.Drawing.Point" /> structures that represents the points to transformation. </param>
		// Token: 0x0600025B RID: 603 RVA: 0x00004644 File Offset: 0x00002844
		public void TransformPoints(CoordinateSpace destSpace, CoordinateSpace srcSpace, Point[] pts)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Translates the clipping region of this <see cref="T:System.Drawing.Graphics" /> by specified amounts in the horizontal and vertical directions.</summary>
		/// <param name="dx">The x-coordinate of the translation. </param>
		/// <param name="dy">The y-coordinate of the translation. </param>
		// Token: 0x0600025C RID: 604 RVA: 0x00004644 File Offset: 0x00002844
		public void TranslateClip(int dx, int dy)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Translates the clipping region of this <see cref="T:System.Drawing.Graphics" /> by specified amounts in the horizontal and vertical directions.</summary>
		/// <param name="dx">The x-coordinate of the translation. </param>
		/// <param name="dy">The y-coordinate of the translation. </param>
		// Token: 0x0600025D RID: 605 RVA: 0x00004644 File Offset: 0x00002844
		public void TranslateClip(float dx, float dy)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Changes the origin of the coordinate system by prepending the specified translation to the transformation matrix of this <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="dx">The x-coordinate of the translation. </param>
		/// <param name="dy">The y-coordinate of the translation. </param>
		// Token: 0x0600025E RID: 606 RVA: 0x00004644 File Offset: 0x00002844
		public void TranslateTransform(float dx, float dy)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Changes the origin of the coordinate system by applying the specified translation to the transformation matrix of this <see cref="T:System.Drawing.Graphics" /> in the specified order.</summary>
		/// <param name="dx">The x-coordinate of the translation. </param>
		/// <param name="dy">The y-coordinate of the translation. </param>
		/// <param name="order">Member of the <see cref="T:System.Drawing.Drawing2D.MatrixOrder" /> enumeration that specifies whether the translation is prepended or appended to the transformation matrix. </param>
		// Token: 0x0600025F RID: 607 RVA: 0x00004644 File Offset: 0x00002844
		public void TranslateTransform(float dx, float dy, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Provides a callback method for deciding when the <see cref="Overload:System.Drawing.Graphics.DrawImage" /> method should prematurely cancel execution and stop drawing an image.</summary>
		/// <param name="callbackdata">Internal pointer that specifies data for the callback method. This parameter is not passed by all <see cref="Overload:System.Drawing.Graphics.DrawImage" /> overloads. You can test for its absence by checking for the value <see cref="F:System.IntPtr.Zero" />. </param>
		/// <returns>This method returns <see langword="true" /> if it decides that the <see cref="Overload:System.Drawing.Graphics.DrawImage" /> method should prematurely stop execution. Otherwise it returns <see langword="false" /> to indicate that the <see cref="Overload:System.Drawing.Graphics.DrawImage" /> method should continue execution.</returns>
		// Token: 0x02000014 RID: 20
		public sealed class DrawImageAbort : MulticastDelegate
		{
			// Token: 0x06000260 RID: 608 RVA: 0x00004644 File Offset: 0x00002844
			public DrawImageAbort(object @object, IntPtr method)
			{
				ThrowStub.ThrowNotSupportedException();
			}

			// Token: 0x06000261 RID: 609 RVA: 0x00004AB4 File Offset: 0x00002CB4
			public bool Invoke(IntPtr callbackdata)
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}

			// Token: 0x06000262 RID: 610 RVA: 0x00004667 File Offset: 0x00002867
			public IAsyncResult BeginInvoke(IntPtr callbackdata, AsyncCallback callback, object @object)
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}

			// Token: 0x06000263 RID: 611 RVA: 0x00004AD0 File Offset: 0x00002CD0
			public bool EndInvoke(IAsyncResult result)
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Provides a callback method for the <see cref="Overload:System.Drawing.Graphics.EnumerateMetafile" /> method.</summary>
		/// <param name="recordType">Member of the <see cref="T:System.Drawing.Imaging.EmfPlusRecordType" /> enumeration that specifies the type of metafile record. </param>
		/// <param name="flags">Set of flags that specify attributes of the record. </param>
		/// <param name="dataSize">Number of bytes in the record data. </param>
		/// <param name="data">Pointer to a buffer that contains the record data. </param>
		/// <param name="callbackData">Not used. </param>
		/// <returns>Return <see langword="true" /> if you want to continue enumerating records; otherwise, <see langword="false" />.</returns>
		// Token: 0x02000015 RID: 21
		public sealed class EnumerateMetafileProc : MulticastDelegate
		{
			// Token: 0x06000264 RID: 612 RVA: 0x00004644 File Offset: 0x00002844
			public EnumerateMetafileProc(object @object, IntPtr method)
			{
				ThrowStub.ThrowNotSupportedException();
			}

			// Token: 0x06000265 RID: 613 RVA: 0x00004AEC File Offset: 0x00002CEC
			public bool Invoke(EmfPlusRecordType recordType, int flags, int dataSize, IntPtr data, PlayRecordCallback callbackData)
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}

			// Token: 0x06000266 RID: 614 RVA: 0x00004667 File Offset: 0x00002867
			public IAsyncResult BeginInvoke(EmfPlusRecordType recordType, int flags, int dataSize, IntPtr data, PlayRecordCallback callbackData, AsyncCallback callback, object @object)
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}

			// Token: 0x06000267 RID: 615 RVA: 0x00004B08 File Offset: 0x00002D08
			public bool EndInvoke(IAsyncResult result)
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}
	}
}
