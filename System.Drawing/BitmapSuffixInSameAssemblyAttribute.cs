﻿using System;

namespace System.Drawing
{
	/// <summary>Specifies that, when interpreting <see cref="T:System.Drawing.ToolboxBitmapAttribute" /> declarations, the assembly should look for the indicated resources in the same assembly, but with the <see cref="P:System.Drawing.Configuration.SystemDrawingSection.BitmapSuffix" /> configuration value appended to the declared file name.</summary>
	// Token: 0x02000075 RID: 117
	[AttributeUsage(AttributeTargets.Assembly)]
	public class BitmapSuffixInSameAssemblyAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.BitmapSuffixInSameAssemblyAttribute" /> class.</summary>
		// Token: 0x060005DC RID: 1500 RVA: 0x00004642 File Offset: 0x00002842
		public BitmapSuffixInSameAssemblyAttribute()
		{
		}
	}
}
