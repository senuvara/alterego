﻿using System;

namespace System.Drawing
{
	/// <summary>Specifies how to trim characters from a string that does not completely fit into a layout shape.</summary>
	// Token: 0x02000025 RID: 37
	public enum StringTrimming
	{
		/// <summary>Specifies that the text is trimmed to the nearest character.</summary>
		// Token: 0x04000223 RID: 547
		Character = 1,
		/// <summary>Specifies that the text is trimmed to the nearest character, and an ellipsis is inserted at the end of a trimmed line.</summary>
		// Token: 0x04000224 RID: 548
		EllipsisCharacter = 3,
		/// <summary>The center is removed from trimmed lines and replaced by an ellipsis. The algorithm keeps as much of the last slash-delimited segment of the line as possible.</summary>
		// Token: 0x04000225 RID: 549
		EllipsisPath = 5,
		/// <summary>Specifies that text is trimmed to the nearest word, and an ellipsis is inserted at the end of a trimmed line.</summary>
		// Token: 0x04000226 RID: 550
		EllipsisWord = 4,
		/// <summary>Specifies no trimming.</summary>
		// Token: 0x04000227 RID: 551
		None = 0,
		/// <summary>Specifies that text is trimmed to the nearest word.</summary>
		// Token: 0x04000228 RID: 552
		Word = 2
	}
}
