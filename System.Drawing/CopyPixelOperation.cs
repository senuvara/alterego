﻿using System;
using System.Runtime.InteropServices;

namespace System.Drawing
{
	/// <summary>Determines how the source color in a copy pixel operation is combined with the destination color to result in a final color.</summary>
	// Token: 0x0200003C RID: 60
	[ComVisible(true)]
	public enum CopyPixelOperation
	{
		/// <summary>The destination area is filled by using the color associated with index 0 in the physical palette. (This color is black for the default physical palette.)</summary>
		// Token: 0x0400028A RID: 650
		Blackness = 66,
		/// <summary>Windows that are layered on top of your window are included in the resulting image. By default, the image contains only your window. Note that this generally cannot be used for printing device contexts.</summary>
		// Token: 0x0400028B RID: 651
		CaptureBlt = 1073741824,
		/// <summary>The destination area is inverted.</summary>
		// Token: 0x0400028C RID: 652
		DestinationInvert = 5570569,
		/// <summary>The colors of the source area are merged with the colors of the selected brush of the destination device context using the Boolean <see langword="AND" /> operator.</summary>
		// Token: 0x0400028D RID: 653
		MergeCopy = 12583114,
		/// <summary>The colors of the inverted source area are merged with the colors of the destination area by using the Boolean <see langword="OR" /> operator.</summary>
		// Token: 0x0400028E RID: 654
		MergePaint = 12255782,
		/// <summary>The bitmap is not mirrored.</summary>
		// Token: 0x0400028F RID: 655
		NoMirrorBitmap = -2147483648,
		/// <summary>The inverted source area is copied to the destination.</summary>
		// Token: 0x04000290 RID: 656
		NotSourceCopy = 3342344,
		/// <summary>The source and destination colors are combined using the Boolean <see langword="OR" /> operator, and then resultant color is then inverted.</summary>
		// Token: 0x04000291 RID: 657
		NotSourceErase = 1114278,
		/// <summary>The brush currently selected in the destination device context is copied to the destination bitmap.</summary>
		// Token: 0x04000292 RID: 658
		PatCopy = 15728673,
		/// <summary>The colors of the brush currently selected in the destination device context are combined with the colors of the destination are using the Boolean <see langword="XOR" /> operator.</summary>
		// Token: 0x04000293 RID: 659
		PatInvert = 5898313,
		/// <summary>The colors of the brush currently selected in the destination device context are combined with the colors of the inverted source area using the Boolean <see langword="OR" /> operator. The result of this operation is combined with the colors of the destination area using the Boolean <see langword="OR" /> operator.</summary>
		// Token: 0x04000294 RID: 660
		PatPaint = 16452105,
		/// <summary>The colors of the source and destination areas are combined using the Boolean <see langword="AND" /> operator.</summary>
		// Token: 0x04000295 RID: 661
		SourceAnd = 8913094,
		/// <summary>The source area is copied directly to the destination area.</summary>
		// Token: 0x04000296 RID: 662
		SourceCopy = 13369376,
		/// <summary>The inverted colors of the destination area are combined with the colors of the source area using the Boolean <see langword="AND" /> operator.</summary>
		// Token: 0x04000297 RID: 663
		SourceErase = 4457256,
		/// <summary>The colors of the source and destination areas are combined using the Boolean <see langword="XOR" /> operator.</summary>
		// Token: 0x04000298 RID: 664
		SourceInvert = 6684742,
		/// <summary>The colors of the source and destination areas are combined using the Boolean <see langword="OR" /> operator.</summary>
		// Token: 0x04000299 RID: 665
		SourcePaint = 15597702,
		/// <summary>The destination area is filled by using the color associated with index 1 in the physical palette. (This color is white for the default physical palette.)</summary>
		// Token: 0x0400029A RID: 666
		Whiteness = 16711778
	}
}
