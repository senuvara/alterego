﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Drawing
{
	/// <summary>Converts an <see cref="T:System.Drawing.Icon" /> object from one data type to another. Access this class through the <see cref="T:System.ComponentModel.TypeDescriptor" /> object.</summary>
	// Token: 0x02000051 RID: 81
	public class IconConverter : ExpandableObjectConverter
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.IconConverter" /> class.</summary>
		// Token: 0x0600046A RID: 1130 RVA: 0x00004644 File Offset: 0x00002844
		public IconConverter()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
