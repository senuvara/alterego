﻿using System;
using Unity;

namespace System.Drawing
{
	/// <summary>Specifies a range of character positions within a string.</summary>
	// Token: 0x02000026 RID: 38
	public struct CharacterRange
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.CharacterRange" /> structure, specifying a range of character positions within a string.</summary>
		/// <param name="First">The position of the first character in the range. For example, if <paramref name="First" /> is set to 0, the first position of the range is position 0 in the string. </param>
		/// <param name="Length">The number of positions in the range. </param>
		// Token: 0x06000335 RID: 821 RVA: 0x00004644 File Offset: 0x00002844
		public CharacterRange(int First, int Length)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the position in the string of the first character of this <see cref="T:System.Drawing.CharacterRange" />.</summary>
		/// <returns>The first position of this <see cref="T:System.Drawing.CharacterRange" />.</returns>
		// Token: 0x170000E7 RID: 231
		// (get) Token: 0x06000336 RID: 822 RVA: 0x0000510C File Offset: 0x0000330C
		// (set) Token: 0x06000337 RID: 823 RVA: 0x00004644 File Offset: 0x00002844
		public int First
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the number of positions in this <see cref="T:System.Drawing.CharacterRange" />.</summary>
		/// <returns>The number of positions in this <see cref="T:System.Drawing.CharacterRange" />.</returns>
		// Token: 0x170000E8 RID: 232
		// (get) Token: 0x06000338 RID: 824 RVA: 0x00005128 File Offset: 0x00003328
		// (set) Token: 0x06000339 RID: 825 RVA: 0x00004644 File Offset: 0x00002844
		public int Length
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Compares two <see cref="T:System.Drawing.CharacterRange" /> objects. Gets a value indicating whether the <see cref="P:System.Drawing.CharacterRange.First" /> and <see cref="P:System.Drawing.CharacterRange.Length" /> values of the two <see cref="T:System.Drawing.CharacterRange" /> objects are equal.</summary>
		/// <param name="cr1">A <see cref="T:System.Drawing.CharacterRange" /> to compare for equality.</param>
		/// <param name="cr2">A <see cref="T:System.Drawing.CharacterRange" /> to compare for equality.</param>
		/// <returns>
		///     <see langword="true" /> to indicate the two <see cref="T:System.Drawing.CharacterRange" /> objects have the same <see cref="P:System.Drawing.CharacterRange.First" /> and <see cref="P:System.Drawing.CharacterRange.Length" /> values; otherwise, <see langword="false" />. </returns>
		// Token: 0x0600033A RID: 826 RVA: 0x00005144 File Offset: 0x00003344
		public static bool operator ==(CharacterRange cr1, CharacterRange cr2)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Compares two <see cref="T:System.Drawing.CharacterRange" /> objects. Gets a value indicating whether the <see cref="P:System.Drawing.CharacterRange.First" /> or <see cref="P:System.Drawing.CharacterRange.Length" /> values of the two <see cref="T:System.Drawing.CharacterRange" /> objects are not equal.</summary>
		/// <param name="cr1">A <see cref="T:System.Drawing.CharacterRange" /> to compare for inequality.</param>
		/// <param name="cr2">A <see cref="T:System.Drawing.CharacterRange" /> to compare for inequality.</param>
		/// <returns>
		///     <see langword="true" /> to indicate the either the <see cref="P:System.Drawing.CharacterRange.First" /> or <see cref="P:System.Drawing.CharacterRange.Length" /> values of the two <see cref="T:System.Drawing.CharacterRange" /> objects differ; otherwise, <see langword="false" />. </returns>
		// Token: 0x0600033B RID: 827 RVA: 0x00005160 File Offset: 0x00003360
		public static bool operator !=(CharacterRange cr1, CharacterRange cr2)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x04000229 RID: 553
		private int DummyAddedByUnityProfileStubber;
	}
}
