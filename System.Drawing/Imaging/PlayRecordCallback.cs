﻿using System;
using Unity;

namespace System.Drawing.Imaging
{
	/// <summary>This delegate is not used. For an example of enumerating the records of a metafile, see <see cref="M:System.Drawing.Graphics.EnumerateMetafile(System.Drawing.Imaging.Metafile,System.Drawing.Point,System.Drawing.Graphics.EnumerateMetafileProc)" />.</summary>
	/// <param name="recordType">Not used. </param>
	/// <param name="flags">Not used. </param>
	/// <param name="dataSize">Not used. </param>
	/// <param name="recordData">Not used. </param>
	// Token: 0x02000017 RID: 23
	public sealed class PlayRecordCallback : MulticastDelegate
	{
		// Token: 0x06000268 RID: 616 RVA: 0x00004644 File Offset: 0x00002844
		public PlayRecordCallback(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06000269 RID: 617 RVA: 0x00004644 File Offset: 0x00002844
		public void Invoke(EmfPlusRecordType recordType, int flags, int dataSize, IntPtr recordData)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0600026A RID: 618 RVA: 0x00004667 File Offset: 0x00002867
		public IAsyncResult BeginInvoke(EmfPlusRecordType recordType, int flags, int dataSize, IntPtr recordData, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x0600026B RID: 619 RVA: 0x00004644 File Offset: 0x00002844
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
