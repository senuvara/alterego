﻿using System;

namespace System.Drawing.Imaging
{
	/// <summary>Specifies the type of color data in the system palette. The data can be color data with alpha, grayscale data only, or halftone data.</summary>
	// Token: 0x020000B2 RID: 178
	[Flags]
	public enum PaletteFlags
	{
		/// <summary>Grayscale data.</summary>
		// Token: 0x04000404 RID: 1028
		GrayScale = 2,
		/// <summary>Halftone data.</summary>
		// Token: 0x04000405 RID: 1029
		Halftone = 4,
		/// <summary>Alpha data.</summary>
		// Token: 0x04000406 RID: 1030
		HasAlpha = 1
	}
}
