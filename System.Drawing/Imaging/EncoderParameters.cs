﻿using System;
using Unity;

namespace System.Drawing.Imaging
{
	/// <summary>Encapsulates an array of <see cref="T:System.Drawing.Imaging.EncoderParameter" /> objects.</summary>
	// Token: 0x02000046 RID: 70
	public sealed class EncoderParameters : IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Imaging.EncoderParameters" /> class that can contain one <see cref="T:System.Drawing.Imaging.EncoderParameter" /> object.</summary>
		// Token: 0x06000420 RID: 1056 RVA: 0x00004644 File Offset: 0x00002844
		public EncoderParameters()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Imaging.EncoderParameters" /> class that can contain the specified number of <see cref="T:System.Drawing.Imaging.EncoderParameter" /> objects.</summary>
		/// <param name="count">An integer that specifies the number of <see cref="T:System.Drawing.Imaging.EncoderParameter" /> objects that the <see cref="T:System.Drawing.Imaging.EncoderParameters" /> object can contain. </param>
		// Token: 0x06000421 RID: 1057 RVA: 0x00004644 File Offset: 0x00002844
		public EncoderParameters(int count)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets an array of <see cref="T:System.Drawing.Imaging.EncoderParameter" /> objects.</summary>
		/// <returns>The array of <see cref="T:System.Drawing.Imaging.EncoderParameter" /> objects.</returns>
		// Token: 0x17000127 RID: 295
		// (get) Token: 0x06000422 RID: 1058 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000423 RID: 1059 RVA: 0x00004644 File Offset: 0x00002844
		public EncoderParameter[] Param
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Releases all resources used by this <see cref="T:System.Drawing.Imaging.EncoderParameters" /> object.</summary>
		// Token: 0x06000424 RID: 1060 RVA: 0x00004644 File Offset: 0x00002844
		public void Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
