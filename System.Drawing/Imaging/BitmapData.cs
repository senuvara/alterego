﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.Drawing.Imaging
{
	/// <summary>Specifies the attributes of a bitmap image. The <see cref="T:System.Drawing.Imaging.BitmapData" /> class is used by the <see cref="Overload:System.Drawing.Bitmap.LockBits" /> and <see cref="M:System.Drawing.Bitmap.UnlockBits(System.Drawing.Imaging.BitmapData)" /> methods of the <see cref="T:System.Drawing.Bitmap" /> class. Not inheritable. </summary>
	// Token: 0x02000050 RID: 80
	[StructLayout(LayoutKind.Sequential)]
	public sealed class BitmapData
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Imaging.BitmapData" /> class.</summary>
		// Token: 0x0600045D RID: 1117 RVA: 0x00004644 File Offset: 0x00002844
		public BitmapData()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the pixel height of the <see cref="T:System.Drawing.Bitmap" /> object. Also sometimes referred to as the number of scan lines.</summary>
		/// <returns>The pixel height of the <see cref="T:System.Drawing.Bitmap" /> object.</returns>
		// Token: 0x1700013C RID: 316
		// (get) Token: 0x0600045E RID: 1118 RVA: 0x000057D4 File Offset: 0x000039D4
		// (set) Token: 0x0600045F RID: 1119 RVA: 0x00004644 File Offset: 0x00002844
		public int Height
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the format of the pixel information in the <see cref="T:System.Drawing.Bitmap" /> object that returned this <see cref="T:System.Drawing.Imaging.BitmapData" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Imaging.PixelFormat" /> that specifies the format of the pixel information in the associated <see cref="T:System.Drawing.Bitmap" /> object.</returns>
		// Token: 0x1700013D RID: 317
		// (get) Token: 0x06000460 RID: 1120 RVA: 0x000057F0 File Offset: 0x000039F0
		// (set) Token: 0x06000461 RID: 1121 RVA: 0x00004644 File Offset: 0x00002844
		public PixelFormat PixelFormat
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return PixelFormat.DontCare;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Reserved. Do not use.</summary>
		/// <returns>Reserved. Do not use.</returns>
		// Token: 0x1700013E RID: 318
		// (get) Token: 0x06000462 RID: 1122 RVA: 0x0000580C File Offset: 0x00003A0C
		// (set) Token: 0x06000463 RID: 1123 RVA: 0x00004644 File Offset: 0x00002844
		public int Reserved
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the address of the first pixel data in the bitmap. This can also be thought of as the first scan line in the bitmap.</summary>
		/// <returns>The address of the first pixel data in the bitmap.</returns>
		// Token: 0x1700013F RID: 319
		// (get) Token: 0x06000464 RID: 1124 RVA: 0x00005828 File Offset: 0x00003A28
		// (set) Token: 0x06000465 RID: 1125 RVA: 0x00004644 File Offset: 0x00002844
		public IntPtr Scan0
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the stride width (also called scan width) of the <see cref="T:System.Drawing.Bitmap" /> object.</summary>
		/// <returns>The stride width, in bytes, of the <see cref="T:System.Drawing.Bitmap" /> object.</returns>
		// Token: 0x17000140 RID: 320
		// (get) Token: 0x06000466 RID: 1126 RVA: 0x00005844 File Offset: 0x00003A44
		// (set) Token: 0x06000467 RID: 1127 RVA: 0x00004644 File Offset: 0x00002844
		public int Stride
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the pixel width of the <see cref="T:System.Drawing.Bitmap" /> object. This can also be thought of as the number of pixels in one scan line.</summary>
		/// <returns>The pixel width of the <see cref="T:System.Drawing.Bitmap" /> object.</returns>
		// Token: 0x17000141 RID: 321
		// (get) Token: 0x06000468 RID: 1128 RVA: 0x00005860 File Offset: 0x00003A60
		// (set) Token: 0x06000469 RID: 1129 RVA: 0x00004644 File Offset: 0x00002844
		public int Width
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
