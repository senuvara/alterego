﻿using System;

namespace System.Drawing.Imaging
{
	/// <summary>Specifies two modes for color component values.</summary>
	// Token: 0x020000AF RID: 175
	public enum ColorMode
	{
		/// <summary>The integer values supplied are 32-bit values.</summary>
		// Token: 0x040003D9 RID: 985
		Argb32Mode,
		/// <summary>The integer values supplied are 64-bit values.</summary>
		// Token: 0x040003DA RID: 986
		Argb64Mode
	}
}
