﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Drawing.Imaging
{
	/// <summary>Specifies the file format of the image. Not inheritable.</summary>
	// Token: 0x02000044 RID: 68
	[TypeConverter(typeof(ImageFormatConverter))]
	public sealed class ImageFormat
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Imaging.ImageFormat" /> class by using the specified <see cref="T:System.Guid" /> structure.</summary>
		/// <param name="guid">The <see cref="T:System.Guid" /> structure that specifies a particular image format. </param>
		// Token: 0x06000413 RID: 1043 RVA: 0x00004644 File Offset: 0x00002844
		public ImageFormat(Guid guid)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the bitmap (BMP) image format.</summary>
		/// <returns>An <see cref="T:System.Drawing.Imaging.ImageFormat" /> object that indicates the bitmap image format.</returns>
		// Token: 0x1700011C RID: 284
		// (get) Token: 0x06000414 RID: 1044 RVA: 0x00004667 File Offset: 0x00002867
		public static ImageFormat Bmp
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the enhanced metafile (EMF) image format.</summary>
		/// <returns>An <see cref="T:System.Drawing.Imaging.ImageFormat" /> object that indicates the enhanced metafile image format.</returns>
		// Token: 0x1700011D RID: 285
		// (get) Token: 0x06000415 RID: 1045 RVA: 0x00004667 File Offset: 0x00002867
		public static ImageFormat Emf
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the Exchangeable Image File (Exif) format.</summary>
		/// <returns>An <see cref="T:System.Drawing.Imaging.ImageFormat" /> object that indicates the Exif format.</returns>
		// Token: 0x1700011E RID: 286
		// (get) Token: 0x06000416 RID: 1046 RVA: 0x00004667 File Offset: 0x00002867
		public static ImageFormat Exif
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the Graphics Interchange Format (GIF) image format.</summary>
		/// <returns>An <see cref="T:System.Drawing.Imaging.ImageFormat" /> object that indicates the GIF image format.</returns>
		// Token: 0x1700011F RID: 287
		// (get) Token: 0x06000417 RID: 1047 RVA: 0x00004667 File Offset: 0x00002867
		public static ImageFormat Gif
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Guid" /> structure that represents this <see cref="T:System.Drawing.Imaging.ImageFormat" /> object.</summary>
		/// <returns>A <see cref="T:System.Guid" /> structure that represents this <see cref="T:System.Drawing.Imaging.ImageFormat" /> object.</returns>
		// Token: 0x17000120 RID: 288
		// (get) Token: 0x06000418 RID: 1048 RVA: 0x000056BC File Offset: 0x000038BC
		public Guid Guid
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Guid);
			}
		}

		/// <summary>Gets the Windows icon image format.</summary>
		/// <returns>An <see cref="T:System.Drawing.Imaging.ImageFormat" /> object that indicates the Windows icon image format.</returns>
		// Token: 0x17000121 RID: 289
		// (get) Token: 0x06000419 RID: 1049 RVA: 0x00004667 File Offset: 0x00002867
		public static ImageFormat Icon
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the Joint Photographic Experts Group (JPEG) image format.</summary>
		/// <returns>An <see cref="T:System.Drawing.Imaging.ImageFormat" /> object that indicates the JPEG image format.</returns>
		// Token: 0x17000122 RID: 290
		// (get) Token: 0x0600041A RID: 1050 RVA: 0x00004667 File Offset: 0x00002867
		public static ImageFormat Jpeg
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the format of a bitmap in memory.</summary>
		/// <returns>An <see cref="T:System.Drawing.Imaging.ImageFormat" /> object that indicates the format of a bitmap in memory.</returns>
		// Token: 0x17000123 RID: 291
		// (get) Token: 0x0600041B RID: 1051 RVA: 0x00004667 File Offset: 0x00002867
		public static ImageFormat MemoryBmp
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the W3C Portable Network Graphics (PNG) image format.</summary>
		/// <returns>An <see cref="T:System.Drawing.Imaging.ImageFormat" /> object that indicates the PNG image format.</returns>
		// Token: 0x17000124 RID: 292
		// (get) Token: 0x0600041C RID: 1052 RVA: 0x00004667 File Offset: 0x00002867
		public static ImageFormat Png
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the Tagged Image File Format (TIFF) image format.</summary>
		/// <returns>An <see cref="T:System.Drawing.Imaging.ImageFormat" /> object that indicates the TIFF image format.</returns>
		// Token: 0x17000125 RID: 293
		// (get) Token: 0x0600041D RID: 1053 RVA: 0x00004667 File Offset: 0x00002867
		public static ImageFormat Tiff
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the Windows metafile (WMF) image format.</summary>
		/// <returns>An <see cref="T:System.Drawing.Imaging.ImageFormat" /> object that indicates the Windows metafile image format.</returns>
		// Token: 0x17000126 RID: 294
		// (get) Token: 0x0600041E RID: 1054 RVA: 0x00004667 File Offset: 0x00002867
		public static ImageFormat Wmf
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
