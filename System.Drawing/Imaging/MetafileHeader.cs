﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.Drawing.Imaging
{
	/// <summary>Contains attributes of an associated <see cref="T:System.Drawing.Imaging.Metafile" />. Not inheritable.</summary>
	// Token: 0x02000061 RID: 97
	[StructLayout(LayoutKind.Sequential)]
	public sealed class MetafileHeader
	{
		// Token: 0x06000542 RID: 1346 RVA: 0x00004644 File Offset: 0x00002844
		internal MetafileHeader()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a <see cref="T:System.Drawing.Rectangle" /> that bounds the associated <see cref="T:System.Drawing.Imaging.Metafile" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.Rectangle" /> that bounds the associated <see cref="T:System.Drawing.Imaging.Metafile" />.</returns>
		// Token: 0x17000177 RID: 375
		// (get) Token: 0x06000543 RID: 1347 RVA: 0x00005E64 File Offset: 0x00004064
		public Rectangle Bounds
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Rectangle);
			}
		}

		/// <summary>Gets the horizontal resolution, in dots per inch, of the associated <see cref="T:System.Drawing.Imaging.Metafile" />.</summary>
		/// <returns>The horizontal resolution, in dots per inch, of the associated <see cref="T:System.Drawing.Imaging.Metafile" />.</returns>
		// Token: 0x17000178 RID: 376
		// (get) Token: 0x06000544 RID: 1348 RVA: 0x00005E80 File Offset: 0x00004080
		public float DpiX
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
		}

		/// <summary>Gets the vertical resolution, in dots per inch, of the associated <see cref="T:System.Drawing.Imaging.Metafile" />.</summary>
		/// <returns>The vertical resolution, in dots per inch, of the associated <see cref="T:System.Drawing.Imaging.Metafile" />.</returns>
		// Token: 0x17000179 RID: 377
		// (get) Token: 0x06000545 RID: 1349 RVA: 0x00005E9C File Offset: 0x0000409C
		public float DpiY
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
		}

		/// <summary>Gets the size, in bytes, of the enhanced metafile plus header file.</summary>
		/// <returns>The size, in bytes, of the enhanced metafile plus header file.</returns>
		// Token: 0x1700017A RID: 378
		// (get) Token: 0x06000546 RID: 1350 RVA: 0x00005EB8 File Offset: 0x000040B8
		public int EmfPlusHeaderSize
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the logical horizontal resolution, in dots per inch, of the associated <see cref="T:System.Drawing.Imaging.Metafile" />.</summary>
		/// <returns>The logical horizontal resolution, in dots per inch, of the associated <see cref="T:System.Drawing.Imaging.Metafile" />.</returns>
		// Token: 0x1700017B RID: 379
		// (get) Token: 0x06000547 RID: 1351 RVA: 0x00005ED4 File Offset: 0x000040D4
		public int LogicalDpiX
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the logical vertical resolution, in dots per inch, of the associated <see cref="T:System.Drawing.Imaging.Metafile" />.</summary>
		/// <returns>The logical vertical resolution, in dots per inch, of the associated <see cref="T:System.Drawing.Imaging.Metafile" />.</returns>
		// Token: 0x1700017C RID: 380
		// (get) Token: 0x06000548 RID: 1352 RVA: 0x00005EF0 File Offset: 0x000040F0
		public int LogicalDpiY
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the size, in bytes, of the associated <see cref="T:System.Drawing.Imaging.Metafile" />.</summary>
		/// <returns>The size, in bytes, of the associated <see cref="T:System.Drawing.Imaging.Metafile" />.</returns>
		// Token: 0x1700017D RID: 381
		// (get) Token: 0x06000549 RID: 1353 RVA: 0x00005F0C File Offset: 0x0000410C
		public int MetafileSize
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the type of the associated <see cref="T:System.Drawing.Imaging.Metafile" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.Imaging.MetafileType" /> enumeration that represents the type of the associated <see cref="T:System.Drawing.Imaging.Metafile" />.</returns>
		// Token: 0x1700017E RID: 382
		// (get) Token: 0x0600054A RID: 1354 RVA: 0x00005F28 File Offset: 0x00004128
		public MetafileType Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return MetafileType.Invalid;
			}
		}

		/// <summary>Gets the version number of the associated <see cref="T:System.Drawing.Imaging.Metafile" />.</summary>
		/// <returns>The version number of the associated <see cref="T:System.Drawing.Imaging.Metafile" />.</returns>
		// Token: 0x1700017F RID: 383
		// (get) Token: 0x0600054B RID: 1355 RVA: 0x00005F44 File Offset: 0x00004144
		public int Version
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the Windows metafile (WMF) header file for the associated <see cref="T:System.Drawing.Imaging.Metafile" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.Imaging.MetaHeader" /> that contains the WMF header file for the associated <see cref="T:System.Drawing.Imaging.Metafile" />.</returns>
		// Token: 0x17000180 RID: 384
		// (get) Token: 0x0600054C RID: 1356 RVA: 0x00004667 File Offset: 0x00002867
		public MetaHeader WmfHeader
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Returns a value that indicates whether the associated <see cref="T:System.Drawing.Imaging.Metafile" /> is device dependent.</summary>
		/// <returns>
		///     <see langword="true" /> if the associated <see cref="T:System.Drawing.Imaging.Metafile" /> is device dependent; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600054D RID: 1357 RVA: 0x00005F60 File Offset: 0x00004160
		public bool IsDisplay()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a value that indicates whether the associated <see cref="T:System.Drawing.Imaging.Metafile" /> is in the Windows enhanced metafile format.</summary>
		/// <returns>
		///     <see langword="true" /> if the associated <see cref="T:System.Drawing.Imaging.Metafile" /> is in the Windows enhanced metafile format; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600054E RID: 1358 RVA: 0x00005F7C File Offset: 0x0000417C
		public bool IsEmf()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a value that indicates whether the associated <see cref="T:System.Drawing.Imaging.Metafile" /> is in the Windows enhanced metafile format or the Windows enhanced metafile plus format.</summary>
		/// <returns>
		///     <see langword="true" /> if the associated <see cref="T:System.Drawing.Imaging.Metafile" /> is in the Windows enhanced metafile format or the Windows enhanced metafile plus format; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600054F RID: 1359 RVA: 0x00005F98 File Offset: 0x00004198
		public bool IsEmfOrEmfPlus()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a value that indicates whether the associated <see cref="T:System.Drawing.Imaging.Metafile" /> is in the Windows enhanced metafile plus format.</summary>
		/// <returns>
		///     <see langword="true" /> if the associated <see cref="T:System.Drawing.Imaging.Metafile" /> is in the Windows enhanced metafile plus format; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000550 RID: 1360 RVA: 0x00005FB4 File Offset: 0x000041B4
		public bool IsEmfPlus()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a value that indicates whether the associated <see cref="T:System.Drawing.Imaging.Metafile" /> is in the Dual enhanced metafile format. This format supports both the enhanced and the enhanced plus format.</summary>
		/// <returns>
		///     <see langword="true" /> if the associated <see cref="T:System.Drawing.Imaging.Metafile" /> is in the Dual enhanced metafile format; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000551 RID: 1361 RVA: 0x00005FD0 File Offset: 0x000041D0
		public bool IsEmfPlusDual()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a value that indicates whether the associated <see cref="T:System.Drawing.Imaging.Metafile" /> supports only the Windows enhanced metafile plus format.</summary>
		/// <returns>
		///     <see langword="true" /> if the associated <see cref="T:System.Drawing.Imaging.Metafile" /> supports only the Windows enhanced metafile plus format; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000552 RID: 1362 RVA: 0x00005FEC File Offset: 0x000041EC
		public bool IsEmfPlusOnly()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a value that indicates whether the associated <see cref="T:System.Drawing.Imaging.Metafile" /> is in the Windows metafile format.</summary>
		/// <returns>
		///     <see langword="true" /> if the associated <see cref="T:System.Drawing.Imaging.Metafile" /> is in the Windows metafile format; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000553 RID: 1363 RVA: 0x00006008 File Offset: 0x00004208
		public bool IsWmf()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a value that indicates whether the associated <see cref="T:System.Drawing.Imaging.Metafile" /> is in the Windows placeable metafile format.</summary>
		/// <returns>
		///     <see langword="true" /> if the associated <see cref="T:System.Drawing.Imaging.Metafile" /> is in the Windows placeable metafile format; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000554 RID: 1364 RVA: 0x00006024 File Offset: 0x00004224
		public bool IsWmfPlaceable()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}
	}
}
