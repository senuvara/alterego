﻿using System;

namespace System.Drawing.Imaging
{
	/// <summary>Specifies the types of images and colors that will be affected by the color and grayscale adjustment settings of an <see cref="T:System.Drawing.Imaging.ImageAttributes" />.</summary>
	// Token: 0x02000056 RID: 86
	public enum ColorMatrixFlag
	{
		/// <summary>Only gray shades are adjusted.</summary>
		// Token: 0x040002EF RID: 751
		AltGrays = 2,
		/// <summary>All color values, including gray shades, are adjusted by the same color-adjustment matrix.</summary>
		// Token: 0x040002F0 RID: 752
		Default = 0,
		/// <summary>All colors are adjusted, but gray shades are not adjusted. A gray shade is any color that has the same value for its red, green, and blue components.</summary>
		// Token: 0x040002F1 RID: 753
		SkipGrays
	}
}
