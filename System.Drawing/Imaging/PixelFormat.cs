﻿using System;

namespace System.Drawing.Imaging
{
	/// <summary>Specifies the format of the color data for each pixel in the image.</summary>
	// Token: 0x02000042 RID: 66
	public enum PixelFormat
	{
		/// <summary>The pixel data contains alpha values that are not premultiplied.</summary>
		// Token: 0x0400029C RID: 668
		Alpha = 262144,
		/// <summary>The default pixel format of 32 bits per pixel. The format specifies 24-bit color depth and an 8-bit alpha channel.</summary>
		// Token: 0x0400029D RID: 669
		Canonical = 2097152,
		/// <summary>No pixel format is specified.</summary>
		// Token: 0x0400029E RID: 670
		DontCare = 0,
		/// <summary>Reserved.</summary>
		// Token: 0x0400029F RID: 671
		Extended = 1048576,
		/// <summary>The pixel format is 16 bits per pixel. The color information specifies 32,768 shades of color, of which 5 bits are red, 5 bits are green, 5 bits are blue, and 1 bit is alpha.</summary>
		// Token: 0x040002A0 RID: 672
		Format16bppArgb1555 = 397319,
		/// <summary>The pixel format is 16 bits per pixel. The color information specifies 65536 shades of gray.</summary>
		// Token: 0x040002A1 RID: 673
		Format16bppGrayScale = 1052676,
		/// <summary>Specifies that the format is 16 bits per pixel; 5 bits each are used for the red, green, and blue components. The remaining bit is not used.</summary>
		// Token: 0x040002A2 RID: 674
		Format16bppRgb555 = 135173,
		/// <summary>Specifies that the format is 16 bits per pixel; 5 bits are used for the red component, 6 bits are used for the green component, and 5 bits are used for the blue component.</summary>
		// Token: 0x040002A3 RID: 675
		Format16bppRgb565,
		/// <summary>Specifies that the pixel format is 1 bit per pixel and that it uses indexed color. The color table therefore has two colors in it.</summary>
		// Token: 0x040002A4 RID: 676
		Format1bppIndexed = 196865,
		/// <summary>Specifies that the format is 24 bits per pixel; 8 bits each are used for the red, green, and blue components.</summary>
		// Token: 0x040002A5 RID: 677
		Format24bppRgb = 137224,
		/// <summary>Specifies that the format is 32 bits per pixel; 8 bits each are used for the alpha, red, green, and blue components.</summary>
		// Token: 0x040002A6 RID: 678
		Format32bppArgb = 2498570,
		/// <summary>Specifies that the format is 32 bits per pixel; 8 bits each are used for the alpha, red, green, and blue components. The red, green, and blue components are premultiplied, according to the alpha component.</summary>
		// Token: 0x040002A7 RID: 679
		Format32bppPArgb = 925707,
		/// <summary>Specifies that the format is 32 bits per pixel; 8 bits each are used for the red, green, and blue components. The remaining 8 bits are not used.</summary>
		// Token: 0x040002A8 RID: 680
		Format32bppRgb = 139273,
		/// <summary>Specifies that the format is 48 bits per pixel; 16 bits each are used for the red, green, and blue components.</summary>
		// Token: 0x040002A9 RID: 681
		Format48bppRgb = 1060876,
		/// <summary>Specifies that the format is 4 bits per pixel, indexed.</summary>
		// Token: 0x040002AA RID: 682
		Format4bppIndexed = 197634,
		/// <summary>Specifies that the format is 64 bits per pixel; 16 bits each are used for the alpha, red, green, and blue components.</summary>
		// Token: 0x040002AB RID: 683
		Format64bppArgb = 3424269,
		/// <summary>Specifies that the format is 64 bits per pixel; 16 bits each are used for the alpha, red, green, and blue components. The red, green, and blue components are premultiplied according to the alpha component.</summary>
		// Token: 0x040002AC RID: 684
		Format64bppPArgb = 1851406,
		/// <summary>Specifies that the format is 8 bits per pixel, indexed. The color table therefore has 256 colors in it.</summary>
		// Token: 0x040002AD RID: 685
		Format8bppIndexed = 198659,
		/// <summary>The pixel data contains GDI colors.</summary>
		// Token: 0x040002AE RID: 686
		Gdi = 131072,
		/// <summary>The pixel data contains color-indexed values, which means the values are an index to colors in the system color table, as opposed to individual color values.</summary>
		// Token: 0x040002AF RID: 687
		Indexed = 65536,
		/// <summary>The maximum value for this enumeration.</summary>
		// Token: 0x040002B0 RID: 688
		Max = 15,
		/// <summary>The pixel format contains premultiplied alpha values.</summary>
		// Token: 0x040002B1 RID: 689
		PAlpha = 524288,
		/// <summary>The pixel format is undefined.</summary>
		// Token: 0x040002B2 RID: 690
		Undefined = 0
	}
}
