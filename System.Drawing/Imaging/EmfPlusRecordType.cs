﻿using System;

namespace System.Drawing.Imaging
{
	/// <summary>Specifies the methods available for use with a metafile to read and write graphic commands. </summary>
	// Token: 0x02000016 RID: 22
	public enum EmfPlusRecordType
	{
		/// <summary>See <see cref="M:System.Drawing.Graphics.BeginContainer" /> methods.</summary>
		// Token: 0x04000101 RID: 257
		BeginContainer = 16423,
		/// <summary>See <see cref="M:System.Drawing.Graphics.BeginContainer" /> methods.</summary>
		// Token: 0x04000102 RID: 258
		BeginContainerNoParams,
		/// <summary>See <see cref="M:System.Drawing.Graphics.Clear(System.Drawing.Color)" />.</summary>
		// Token: 0x04000103 RID: 259
		Clear = 16393,
		/// <summary>See <see cref="M:System.Drawing.Graphics.AddMetafileComment(System.Byte[])" />.</summary>
		// Token: 0x04000104 RID: 260
		Comment = 16387,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.DrawArc" /> methods.</summary>
		// Token: 0x04000105 RID: 261
		DrawArc = 16402,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.DrawBeziers" /> methods.</summary>
		// Token: 0x04000106 RID: 262
		DrawBeziers = 16409,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.DrawClosedCurve" /> methods.</summary>
		// Token: 0x04000107 RID: 263
		DrawClosedCurve = 16407,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.DrawCurve" /> methods.</summary>
		// Token: 0x04000108 RID: 264
		DrawCurve,
		/// <summary>Specifies a character string, a location, and formatting information.</summary>
		// Token: 0x04000109 RID: 265
		DrawDriverString = 16438,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.DrawEllipse" /> methods.</summary>
		// Token: 0x0400010A RID: 266
		DrawEllipse = 16399,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.DrawImage" /> methods.</summary>
		// Token: 0x0400010B RID: 267
		DrawImage = 16410,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.DrawImage" /> methods.</summary>
		// Token: 0x0400010C RID: 268
		DrawImagePoints,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.DrawLines" /> methods.</summary>
		// Token: 0x0400010D RID: 269
		DrawLines = 16397,
		/// <summary>See <see cref="M:System.Drawing.Graphics.DrawPath(System.Drawing.Pen,System.Drawing.Drawing2D.GraphicsPath)" />.</summary>
		// Token: 0x0400010E RID: 270
		DrawPath = 16405,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.DrawPie" /> methods.</summary>
		// Token: 0x0400010F RID: 271
		DrawPie = 16401,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.DrawRectangles" /> methods.</summary>
		// Token: 0x04000110 RID: 272
		DrawRects = 16395,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.DrawString" /> methods.</summary>
		// Token: 0x04000111 RID: 273
		DrawString = 16412,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000112 RID: 274
		EmfAbortPath = 68,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000113 RID: 275
		EmfAlphaBlend = 114,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000114 RID: 276
		EmfAngleArc = 41,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000115 RID: 277
		EmfArcTo = 55,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000116 RID: 278
		EmfBeginPath = 59,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000117 RID: 279
		EmfBitBlt = 76,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000118 RID: 280
		EmfChord = 46,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000119 RID: 281
		EmfCloseFigure = 61,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400011A RID: 282
		EmfColorCorrectPalette = 111,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400011B RID: 283
		EmfColorMatchToTargetW = 121,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400011C RID: 284
		EmfCreateBrushIndirect = 39,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400011D RID: 285
		EmfCreateColorSpace = 99,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400011E RID: 286
		EmfCreateColorSpaceW = 122,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400011F RID: 287
		EmfCreateDibPatternBrushPt = 94,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000120 RID: 288
		EmfCreateMonoBrush = 93,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000121 RID: 289
		EmfCreatePalette = 49,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000122 RID: 290
		EmfCreatePen = 38,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000123 RID: 291
		EmfDeleteColorSpace = 101,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000124 RID: 292
		EmfDeleteObject = 40,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000125 RID: 293
		EmfDrawEscape = 105,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000126 RID: 294
		EmfEllipse = 42,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000127 RID: 295
		EmfEndPath = 60,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000128 RID: 296
		EmfEof = 14,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000129 RID: 297
		EmfExcludeClipRect = 29,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400012A RID: 298
		EmfExtCreateFontIndirect = 82,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400012B RID: 299
		EmfExtCreatePen = 95,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400012C RID: 300
		EmfExtEscape = 106,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400012D RID: 301
		EmfExtFloodFill = 53,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400012E RID: 302
		EmfExtSelectClipRgn = 75,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400012F RID: 303
		EmfExtTextOutA = 83,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000130 RID: 304
		EmfExtTextOutW,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000131 RID: 305
		EmfFillPath = 62,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000132 RID: 306
		EmfFillRgn = 71,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000133 RID: 307
		EmfFlattenPath = 65,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000134 RID: 308
		EmfForceUfiMapping = 109,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000135 RID: 309
		EmfFrameRgn = 72,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000136 RID: 310
		EmfGdiComment = 70,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000137 RID: 311
		EmfGlsBoundedRecord = 103,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000138 RID: 312
		EmfGlsRecord = 102,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000139 RID: 313
		EmfGradientFill = 118,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400013A RID: 314
		EmfHeader = 1,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400013B RID: 315
		EmfIntersectClipRect = 30,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400013C RID: 316
		EmfInvertRgn = 73,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400013D RID: 317
		EmfLineTo = 54,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400013E RID: 318
		EmfMaskBlt = 78,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400013F RID: 319
		EmfMax = 122,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000140 RID: 320
		EmfMin = 1,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000141 RID: 321
		EmfModifyWorldTransform = 36,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000142 RID: 322
		EmfMoveToEx = 27,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000143 RID: 323
		EmfNamedEscpae = 110,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000144 RID: 324
		EmfOffsetClipRgn = 26,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000145 RID: 325
		EmfPaintRgn = 74,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000146 RID: 326
		EmfPie = 47,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000147 RID: 327
		EmfPixelFormat = 104,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000148 RID: 328
		EmfPlgBlt = 79,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000149 RID: 329
		EmfPlusRecordBase = 16384,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400014A RID: 330
		EmfPolyBezier = 2,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400014B RID: 331
		EmfPolyBezier16 = 85,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400014C RID: 332
		EmfPolyBezierTo = 5,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400014D RID: 333
		EmfPolyBezierTo16 = 88,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400014E RID: 334
		EmfPolyDraw = 56,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400014F RID: 335
		EmfPolyDraw16 = 92,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000150 RID: 336
		EmfPolygon = 3,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000151 RID: 337
		EmfPolygon16 = 86,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000152 RID: 338
		EmfPolyline = 4,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000153 RID: 339
		EmfPolyline16 = 87,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000154 RID: 340
		EmfPolyLineTo = 6,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000155 RID: 341
		EmfPolylineTo16 = 89,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000156 RID: 342
		EmfPolyPolygon = 8,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000157 RID: 343
		EmfPolyPolygon16 = 91,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000158 RID: 344
		EmfPolyPolyline = 7,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000159 RID: 345
		EmfPolyPolyline16 = 90,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400015A RID: 346
		EmfPolyTextOutA = 96,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400015B RID: 347
		EmfPolyTextOutW,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400015C RID: 348
		EmfRealizePalette = 52,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400015D RID: 349
		EmfRectangle = 43,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400015E RID: 350
		EmfReserved069 = 69,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400015F RID: 351
		EmfReserved117 = 117,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000160 RID: 352
		EmfResizePalette = 51,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000161 RID: 353
		EmfRestoreDC = 34,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000162 RID: 354
		EmfRoundArc = 45,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000163 RID: 355
		EmfRoundRect = 44,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000164 RID: 356
		EmfSaveDC = 33,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000165 RID: 357
		EmfScaleViewportExtEx = 31,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000166 RID: 358
		EmfScaleWindowExtEx,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000167 RID: 359
		EmfSelectClipPath = 67,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000168 RID: 360
		EmfSelectObject = 37,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000169 RID: 361
		EmfSelectPalette = 48,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400016A RID: 362
		EmfSetArcDirection = 57,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400016B RID: 363
		EmfSetBkColor = 25,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400016C RID: 364
		EmfSetBkMode = 18,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400016D RID: 365
		EmfSetBrushOrgEx = 13,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400016E RID: 366
		EmfSetColorAdjustment = 23,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400016F RID: 367
		EmfSetColorSpace = 100,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000170 RID: 368
		EmfSetDIBitsToDevice = 80,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000171 RID: 369
		EmfSetIcmMode = 98,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000172 RID: 370
		EmfSetIcmProfileA = 112,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000173 RID: 371
		EmfSetIcmProfileW,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000174 RID: 372
		EmfSetLayout = 115,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000175 RID: 373
		EmfSetLinkedUfis = 119,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000176 RID: 374
		EmfSetMapMode = 17,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000177 RID: 375
		EmfSetMapperFlags = 16,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000178 RID: 376
		EmfSetMetaRgn = 28,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000179 RID: 377
		EmfSetMiterLimit = 58,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400017A RID: 378
		EmfSetPaletteEntries = 50,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400017B RID: 379
		EmfSetPixelV = 15,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400017C RID: 380
		EmfSetPolyFillMode = 19,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400017D RID: 381
		EmfSetROP2,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400017E RID: 382
		EmfSetStretchBltMode,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400017F RID: 383
		EmfSetTextAlign,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000180 RID: 384
		EmfSetTextColor = 24,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000181 RID: 385
		EmfSetTextJustification = 120,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000182 RID: 386
		EmfSetViewportExtEx = 11,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000183 RID: 387
		EmfSetViewportOrgEx,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000184 RID: 388
		EmfSetWindowExtEx = 9,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000185 RID: 389
		EmfSetWindowOrgEx,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000186 RID: 390
		EmfSetWorldTransform = 35,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000187 RID: 391
		EmfSmallTextOut = 108,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000188 RID: 392
		EmfStartDoc = 107,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x04000189 RID: 393
		EmfStretchBlt = 77,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400018A RID: 394
		EmfStretchDIBits = 81,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400018B RID: 395
		EmfStrokeAndFillPath = 63,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400018C RID: 396
		EmfStrokePath,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400018D RID: 397
		EmfTransparentBlt = 116,
		/// <summary>See "Enhanced-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x0400018E RID: 398
		EmfWidenPath = 66,
		/// <summary>See <see cref="M:System.Drawing.Graphics.EndContainer(System.Drawing.Drawing2D.GraphicsContainer)" />.</summary>
		// Token: 0x0400018F RID: 399
		EndContainer = 16425,
		/// <summary>Identifies a record that marks the last EMF+ record of a metafile.</summary>
		// Token: 0x04000190 RID: 400
		EndOfFile = 16386,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.FillClosedCurve" /> methods.</summary>
		// Token: 0x04000191 RID: 401
		FillClosedCurve = 16406,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.FillEllipse" /> methods.</summary>
		// Token: 0x04000192 RID: 402
		FillEllipse = 16398,
		/// <summary>See <see cref="M:System.Drawing.Graphics.FillPath(System.Drawing.Brush,System.Drawing.Drawing2D.GraphicsPath)" />.</summary>
		// Token: 0x04000193 RID: 403
		FillPath = 16404,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.FillPie" /> methods.</summary>
		// Token: 0x04000194 RID: 404
		FillPie = 16400,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.FillPolygon" /> methods.</summary>
		// Token: 0x04000195 RID: 405
		FillPolygon = 16396,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.FillRectangles" /> methods.</summary>
		// Token: 0x04000196 RID: 406
		FillRects = 16394,
		/// <summary>See <see cref="M:System.Drawing.Graphics.FillRegion(System.Drawing.Brush,System.Drawing.Region)" />.</summary>
		// Token: 0x04000197 RID: 407
		FillRegion = 16403,
		/// <summary>See <see cref="M:System.Drawing.Graphics.GetHdc" />.</summary>
		// Token: 0x04000198 RID: 408
		GetDC = 16388,
		/// <summary>Identifies a record that is the EMF+ header.</summary>
		// Token: 0x04000199 RID: 409
		Header = 16385,
		/// <summary>Indicates invalid data.</summary>
		// Token: 0x0400019A RID: 410
		Invalid = 16384,
		/// <summary>The maximum value for this enumeration.</summary>
		// Token: 0x0400019B RID: 411
		Max = 16438,
		/// <summary>The minimum value for this enumeration.</summary>
		// Token: 0x0400019C RID: 412
		Min = 16385,
		/// <summary>Marks the end of a multiple-format section.</summary>
		// Token: 0x0400019D RID: 413
		MultiFormatEnd = 16391,
		/// <summary>Marks a multiple-format section.</summary>
		// Token: 0x0400019E RID: 414
		MultiFormatSection = 16390,
		/// <summary>Marks the start of a multiple-format section.</summary>
		// Token: 0x0400019F RID: 415
		MultiFormatStart = 16389,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.MultiplyTransform" /> methods.</summary>
		// Token: 0x040001A0 RID: 416
		MultiplyWorldTransform = 16428,
		/// <summary>Marks an object.</summary>
		// Token: 0x040001A1 RID: 417
		Object = 16392,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.TranslateClip" /> methods.</summary>
		// Token: 0x040001A2 RID: 418
		OffsetClip = 16437,
		/// <summary>See <see cref="M:System.Drawing.Graphics.ResetClip" />.</summary>
		// Token: 0x040001A3 RID: 419
		ResetClip = 16433,
		/// <summary>See <see cref="M:System.Drawing.Graphics.ResetTransform" />.</summary>
		// Token: 0x040001A4 RID: 420
		ResetWorldTransform = 16427,
		/// <summary>See <see cref="M:System.Drawing.Graphics.Restore(System.Drawing.Drawing2D.GraphicsState)" />.</summary>
		// Token: 0x040001A5 RID: 421
		Restore = 16422,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.RotateTransform" /> methods.</summary>
		// Token: 0x040001A6 RID: 422
		RotateWorldTransform = 16431,
		/// <summary>See <see cref="M:System.Drawing.Graphics.Save" />.</summary>
		// Token: 0x040001A7 RID: 423
		Save = 16421,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.ScaleTransform" /> methods.</summary>
		// Token: 0x040001A8 RID: 424
		ScaleWorldTransform = 16430,
		/// <summary>See <see cref="P:System.Drawing.Graphics.SmoothingMode" />.</summary>
		// Token: 0x040001A9 RID: 425
		SetAntiAliasMode = 16414,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.SetClip" /> methods.</summary>
		// Token: 0x040001AA RID: 426
		SetClipPath = 16435,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.SetClip" /> methods.</summary>
		// Token: 0x040001AB RID: 427
		SetClipRect = 16434,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.SetClip" /> methods.</summary>
		// Token: 0x040001AC RID: 428
		SetClipRegion = 16436,
		/// <summary>See <see cref="P:System.Drawing.Graphics.CompositingMode" />.</summary>
		// Token: 0x040001AD RID: 429
		SetCompositingMode = 16419,
		/// <summary>See <see cref="P:System.Drawing.Graphics.CompositingQuality" />.</summary>
		// Token: 0x040001AE RID: 430
		SetCompositingQuality,
		/// <summary>See <see cref="P:System.Drawing.Graphics.InterpolationMode" />.</summary>
		// Token: 0x040001AF RID: 431
		SetInterpolationMode = 16417,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.TransformPoints" /> methods.</summary>
		// Token: 0x040001B0 RID: 432
		SetPageTransform = 16432,
		/// <summary>See <see cref="P:System.Drawing.Graphics.PixelOffsetMode" />.</summary>
		// Token: 0x040001B1 RID: 433
		SetPixelOffsetMode = 16418,
		/// <summary>See <see cref="P:System.Drawing.Graphics.RenderingOrigin" />.</summary>
		// Token: 0x040001B2 RID: 434
		SetRenderingOrigin = 16413,
		/// <summary>See <see cref="P:System.Drawing.Graphics.TextContrast" />.</summary>
		// Token: 0x040001B3 RID: 435
		SetTextContrast = 16416,
		/// <summary>See <see cref="P:System.Drawing.Graphics.TextRenderingHint" />.</summary>
		// Token: 0x040001B4 RID: 436
		SetTextRenderingHint = 16415,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.TransformPoints" /> methods.</summary>
		// Token: 0x040001B5 RID: 437
		SetWorldTransform = 16426,
		/// <summary>Used internally.</summary>
		// Token: 0x040001B6 RID: 438
		Total = 16439,
		/// <summary>See <see cref="Overload:System.Drawing.Graphics.TransformPoints" /> methods.</summary>
		// Token: 0x040001B7 RID: 439
		TranslateWorldTransform = 16429,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001B8 RID: 440
		WmfAnimatePalette = 66614,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001B9 RID: 441
		WmfArc = 67607,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001BA RID: 442
		WmfBitBlt = 67874,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001BB RID: 443
		WmfChord = 67632,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001BC RID: 444
		WmfCreateBrushIndirect = 66300,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001BD RID: 445
		WmfCreateFontIndirect = 66299,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001BE RID: 446
		WmfCreatePalette = 65783,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001BF RID: 447
		WmfCreatePatternBrush = 66041,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001C0 RID: 448
		WmfCreatePenIndirect = 66298,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001C1 RID: 449
		WmfCreateRegion = 67327,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001C2 RID: 450
		WmfDeleteObject = 66032,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001C3 RID: 451
		WmfDibBitBlt = 67904,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001C4 RID: 452
		WmfDibCreatePatternBrush = 65858,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001C5 RID: 453
		WmfDibStretchBlt = 68417,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001C6 RID: 454
		WmfEllipse = 66584,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001C7 RID: 455
		WmfEscape = 67110,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001C8 RID: 456
		WmfExcludeClipRect = 66581,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001C9 RID: 457
		WmfExtFloodFill = 66888,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001CA RID: 458
		WmfExtTextOut = 68146,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001CB RID: 459
		WmfFillRegion = 66088,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001CC RID: 460
		WmfFloodFill = 66585,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001CD RID: 461
		WmfFrameRegion = 66601,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001CE RID: 462
		WmfIntersectClipRect = 66582,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001CF RID: 463
		WmfInvertRegion = 65834,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001D0 RID: 464
		WmfLineTo = 66067,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001D1 RID: 465
		WmfMoveTo,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001D2 RID: 466
		WmfOffsetCilpRgn = 66080,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001D3 RID: 467
		WmfOffsetViewportOrg = 66065,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001D4 RID: 468
		WmfOffsetWindowOrg = 66063,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001D5 RID: 469
		WmfPaintRegion = 65835,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001D6 RID: 470
		WmfPatBlt = 67101,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001D7 RID: 471
		WmfPie = 67610,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001D8 RID: 472
		WmfPolygon = 66340,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001D9 RID: 473
		WmfPolyline,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001DA RID: 474
		WmfPolyPolygon = 66872,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001DB RID: 475
		WmfRealizePalette = 65589,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001DC RID: 476
		WmfRecordBase = 65536,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001DD RID: 477
		WmfRectangle = 66587,
		/// <summary>Increases or decreases the size of a logical palette based on the specified value.</summary>
		// Token: 0x040001DE RID: 478
		WmfResizePalette = 65849,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001DF RID: 479
		WmfRestoreDC = 65831,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001E0 RID: 480
		WmfRoundRect = 67100,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001E1 RID: 481
		WmfSaveDC = 65566,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001E2 RID: 482
		WmfScaleViewportExt = 66578,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001E3 RID: 483
		WmfScaleWindowExt = 66576,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001E4 RID: 484
		WmfSelectClipRegion = 65836,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001E5 RID: 485
		WmfSelectObject,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001E6 RID: 486
		WmfSelectPalette = 66100,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001E7 RID: 487
		WmfSetBkColor = 66049,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001E8 RID: 488
		WmfSetBkMode = 65794,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001E9 RID: 489
		WmfSetDibToDev = 68915,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001EA RID: 490
		WmfSetLayout = 65865,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001EB RID: 491
		WmfSetMapMode = 65795,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001EC RID: 492
		WmfSetMapperFlags = 66097,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001ED RID: 493
		WmfSetPalEntries = 65591,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001EE RID: 494
		WmfSetPixel = 66591,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001EF RID: 495
		WmfSetPolyFillMode = 65798,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001F0 RID: 496
		WmfSetRelAbs = 65797,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001F1 RID: 497
		WmfSetROP2 = 65796,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001F2 RID: 498
		WmfSetStretchBltMode = 65799,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001F3 RID: 499
		WmfSetTextAlign = 65838,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001F4 RID: 500
		WmfSetTextCharExtra = 65800,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001F5 RID: 501
		WmfSetTextColor = 66057,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001F6 RID: 502
		WmfSetTextJustification,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001F7 RID: 503
		WmfSetViewportExt = 66062,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001F8 RID: 504
		WmfSetViewportOrg = 66061,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001F9 RID: 505
		WmfSetWindowExt = 66060,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001FA RID: 506
		WmfSetWindowOrg = 66059,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001FB RID: 507
		WmfStretchBlt = 68387,
		/// <summary>Copies the color data for a rectangle of pixels in a DIB to the specified destination rectangle.</summary>
		// Token: 0x040001FC RID: 508
		WmfStretchDib = 69443,
		/// <summary>See "Windows-Format Metafiles" in the GDI section of the MSDN Library.</summary>
		// Token: 0x040001FD RID: 509
		WmfTextOut = 66849
	}
}
