﻿using System;

namespace System.Drawing.Imaging
{
	/// <summary>Specifies the types of color maps.</summary>
	// Token: 0x020000AE RID: 174
	public enum ColorMapType
	{
		/// <summary>Specifies a color map for a <see cref="T:System.Drawing.Brush" />.</summary>
		// Token: 0x040003D6 RID: 982
		Brush = 1,
		/// <summary>A default color map.</summary>
		// Token: 0x040003D7 RID: 983
		Default = 0
	}
}
