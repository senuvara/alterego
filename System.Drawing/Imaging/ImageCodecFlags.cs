﻿using System;

namespace System.Drawing.Imaging
{
	/// <summary>Provides attributes of an image encoder/decoder (codec).</summary>
	// Token: 0x0200004D RID: 77
	[Flags]
	public enum ImageCodecFlags
	{
		/// <summary>The decoder has blocking behavior during the decoding process.</summary>
		// Token: 0x040002D8 RID: 728
		BlockingDecode = 32,
		/// <summary>The codec is built into GDI+.</summary>
		// Token: 0x040002D9 RID: 729
		Builtin = 65536,
		/// <summary>The codec supports decoding (reading).</summary>
		// Token: 0x040002DA RID: 730
		Decoder = 2,
		/// <summary>The codec supports encoding (saving).</summary>
		// Token: 0x040002DB RID: 731
		Encoder = 1,
		/// <summary>The encoder requires a seekable output stream.</summary>
		// Token: 0x040002DC RID: 732
		SeekableEncode = 16,
		/// <summary>The codec supports raster images (bitmaps).</summary>
		// Token: 0x040002DD RID: 733
		SupportBitmap = 4,
		/// <summary>The codec supports vector images (metafiles).</summary>
		// Token: 0x040002DE RID: 734
		SupportVector = 8,
		/// <summary>Not used.</summary>
		// Token: 0x040002DF RID: 735
		System = 131072,
		/// <summary>Not used.</summary>
		// Token: 0x040002E0 RID: 736
		User = 262144
	}
}
