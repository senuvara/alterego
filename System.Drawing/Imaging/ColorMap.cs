﻿using System;
using Unity;

namespace System.Drawing.Imaging
{
	/// <summary>Defines a map for converting colors. Several methods of the <see cref="T:System.Drawing.Imaging.ImageAttributes" /> class adjust image colors by using a color-remap table, which is an array of <see cref="T:System.Drawing.Imaging.ColorMap" /> structures. Not inheritable.</summary>
	// Token: 0x02000054 RID: 84
	public sealed class ColorMap
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Imaging.ColorMap" /> class.</summary>
		// Token: 0x06000498 RID: 1176 RVA: 0x00004644 File Offset: 0x00002844
		public ColorMap()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the new <see cref="T:System.Drawing.Color" /> structure to which to convert.</summary>
		/// <returns>The new <see cref="T:System.Drawing.Color" /> structure to which to convert.</returns>
		// Token: 0x17000142 RID: 322
		// (get) Token: 0x06000499 RID: 1177 RVA: 0x0000587C File Offset: 0x00003A7C
		// (set) Token: 0x0600049A RID: 1178 RVA: 0x00004644 File Offset: 0x00002844
		public Color NewColor
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the existing <see cref="T:System.Drawing.Color" /> structure to be converted.</summary>
		/// <returns>The existing <see cref="T:System.Drawing.Color" /> structure to be converted.</returns>
		// Token: 0x17000143 RID: 323
		// (get) Token: 0x0600049B RID: 1179 RVA: 0x00005898 File Offset: 0x00003A98
		// (set) Token: 0x0600049C RID: 1180 RVA: 0x00004644 File Offset: 0x00002844
		public Color OldColor
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Color);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
