﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.Drawing.Imaging
{
	/// <summary>Contains information about a windows-format (WMF) metafile.</summary>
	// Token: 0x02000063 RID: 99
	[StructLayout(LayoutKind.Sequential, Pack = 2)]
	public sealed class MetaHeader
	{
		/// <summary>Initializes a new instance of the <see langword="MetaHeader" /> class.</summary>
		// Token: 0x06000555 RID: 1365 RVA: 0x00004644 File Offset: 0x00002844
		public MetaHeader()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the size, in bytes, of the header file.</summary>
		/// <returns>The size, in bytes, of the header file.</returns>
		// Token: 0x17000181 RID: 385
		// (get) Token: 0x06000556 RID: 1366 RVA: 0x00006040 File Offset: 0x00004240
		// (set) Token: 0x06000557 RID: 1367 RVA: 0x00004644 File Offset: 0x00002844
		public short HeaderSize
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the size, in bytes, of the largest record in the associated <see cref="T:System.Drawing.Imaging.Metafile" /> object.</summary>
		/// <returns>The size, in bytes, of the largest record in the associated <see cref="T:System.Drawing.Imaging.Metafile" /> object.</returns>
		// Token: 0x17000182 RID: 386
		// (get) Token: 0x06000558 RID: 1368 RVA: 0x0000605C File Offset: 0x0000425C
		// (set) Token: 0x06000559 RID: 1369 RVA: 0x00004644 File Offset: 0x00002844
		public int MaxRecord
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the maximum number of objects that exist in the <see cref="T:System.Drawing.Imaging.Metafile" /> object at the same time.</summary>
		/// <returns>The maximum number of objects that exist in the <see cref="T:System.Drawing.Imaging.Metafile" /> object at the same time.</returns>
		// Token: 0x17000183 RID: 387
		// (get) Token: 0x0600055A RID: 1370 RVA: 0x00006078 File Offset: 0x00004278
		// (set) Token: 0x0600055B RID: 1371 RVA: 0x00004644 File Offset: 0x00002844
		public short NoObjects
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Not used. Always returns 0.</summary>
		/// <returns>Always 0.</returns>
		// Token: 0x17000184 RID: 388
		// (get) Token: 0x0600055C RID: 1372 RVA: 0x00006094 File Offset: 0x00004294
		// (set) Token: 0x0600055D RID: 1373 RVA: 0x00004644 File Offset: 0x00002844
		public short NoParameters
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the size, in bytes, of the associated <see cref="T:System.Drawing.Imaging.Metafile" /> object.</summary>
		/// <returns>The size, in bytes, of the associated <see cref="T:System.Drawing.Imaging.Metafile" /> object.</returns>
		// Token: 0x17000185 RID: 389
		// (get) Token: 0x0600055E RID: 1374 RVA: 0x000060B0 File Offset: 0x000042B0
		// (set) Token: 0x0600055F RID: 1375 RVA: 0x00004644 File Offset: 0x00002844
		public int Size
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the type of the associated <see cref="T:System.Drawing.Imaging.Metafile" /> object.</summary>
		/// <returns>The type of the associated <see cref="T:System.Drawing.Imaging.Metafile" /> object.</returns>
		// Token: 0x17000186 RID: 390
		// (get) Token: 0x06000560 RID: 1376 RVA: 0x000060CC File Offset: 0x000042CC
		// (set) Token: 0x06000561 RID: 1377 RVA: 0x00004644 File Offset: 0x00002844
		public short Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the version number of the header format.</summary>
		/// <returns>The version number of the header format.</returns>
		// Token: 0x17000187 RID: 391
		// (get) Token: 0x06000562 RID: 1378 RVA: 0x000060E8 File Offset: 0x000042E8
		// (set) Token: 0x06000563 RID: 1379 RVA: 0x00004644 File Offset: 0x00002844
		public short Version
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
