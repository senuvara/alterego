﻿using System;
using Unity;

namespace System.Drawing.Imaging
{
	/// <summary>Defines an array of colors that make up a color palette. The colors are 32-bit ARGB colors. Not inheritable.</summary>
	// Token: 0x02000041 RID: 65
	public sealed class ColorPalette
	{
		// Token: 0x06000407 RID: 1031 RVA: 0x00004644 File Offset: 0x00002844
		internal ColorPalette()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets an array of <see cref="T:System.Drawing.Color" /> structures.</summary>
		/// <returns>The array of <see cref="T:System.Drawing.Color" /> structure that make up this <see cref="T:System.Drawing.Imaging.ColorPalette" />.</returns>
		// Token: 0x17000116 RID: 278
		// (get) Token: 0x06000408 RID: 1032 RVA: 0x00004667 File Offset: 0x00002867
		public Color[] Entries
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a value that specifies how to interpret the color information in the array of colors.</summary>
		/// <returns>The following flag values are valid: 0x00000001The color values in the array contain alpha information. 0x00000002The colors in the array are grayscale values. 0x00000004The colors in the array are halftone values. </returns>
		// Token: 0x17000117 RID: 279
		// (get) Token: 0x06000409 RID: 1033 RVA: 0x0000564C File Offset: 0x0000384C
		public int Flags
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}
	}
}
