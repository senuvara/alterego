﻿using System;
using Unity;

namespace System.Drawing.Imaging
{
	/// <summary>An <see cref="T:System.Drawing.Imaging.Encoder" /> object encapsulates a globally unique identifier (GUID) that identifies the category of an image encoder parameter.</summary>
	// Token: 0x02000048 RID: 72
	public sealed class Encoder
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Imaging.Encoder" /> class from the specified globally unique identifier (GUID). The GUID specifies an image encoder parameter category.</summary>
		/// <param name="guid">A globally unique identifier that identifies an image encoder parameter category. </param>
		// Token: 0x0600043C RID: 1084 RVA: 0x00004644 File Offset: 0x00002844
		public Encoder(Guid guid)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a globally unique identifier (GUID) that identifies an image encoder parameter category.</summary>
		/// <returns>The GUID that identifies an image encoder parameter category.</returns>
		// Token: 0x1700012C RID: 300
		// (get) Token: 0x0600043D RID: 1085 RVA: 0x0000572C File Offset: 0x0000392C
		public Guid Guid
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Guid);
			}
		}

		/// <summary>An <see cref="T:System.Drawing.Imaging.Encoder" /> object that is initialized with the globally unique identifier for the chrominance table parameter category.</summary>
		// Token: 0x040002B3 RID: 691
		public static readonly Encoder ChrominanceTable;

		/// <summary>An <see cref="T:System.Drawing.Imaging.Encoder" /> object that is initialized with the globally unique identifier for the color depth parameter category.</summary>
		// Token: 0x040002B4 RID: 692
		public static readonly Encoder ColorDepth;

		/// <summary>An <see cref="T:System.Drawing.Imaging.Encoder" /> object that is initialized with the globally unique identifier for the compression parameter category.</summary>
		// Token: 0x040002B5 RID: 693
		public static readonly Encoder Compression;

		/// <summary>Represents an <see cref="T:System.Drawing.Imaging.Encoder" /> object that is initialized with the globally unique identifier for the luminance table parameter category.</summary>
		// Token: 0x040002B6 RID: 694
		public static readonly Encoder LuminanceTable;

		/// <summary>Gets an <see cref="T:System.Drawing.Imaging.Encoder" /> object that is initialized with the globally unique identifier for the quality parameter category.</summary>
		// Token: 0x040002B7 RID: 695
		public static readonly Encoder Quality;

		/// <summary>Represents an <see cref="T:System.Drawing.Imaging.Encoder" /> object that is initialized with the globally unique identifier for the render method parameter category.</summary>
		// Token: 0x040002B8 RID: 696
		public static readonly Encoder RenderMethod;

		/// <summary>Represents an <see cref="T:System.Drawing.Imaging.Encoder" /> object that is initialized with the globally unique identifier for the save flag parameter category.</summary>
		// Token: 0x040002B9 RID: 697
		public static readonly Encoder SaveFlag;

		/// <summary>Represents an <see cref="T:System.Drawing.Imaging.Encoder" /> object that is initialized with the globally unique identifier for the scan method parameter category.</summary>
		// Token: 0x040002BA RID: 698
		public static readonly Encoder ScanMethod;

		/// <summary>Represents an <see cref="T:System.Drawing.Imaging.Encoder" /> object that is initialized with the globally unique identifier for the transformation parameter category.</summary>
		// Token: 0x040002BB RID: 699
		public static readonly Encoder Transformation;

		/// <summary>Represents an <see cref="T:System.Drawing.Imaging.Encoder" /> object that is initialized with the globally unique identifier for the version parameter category.</summary>
		// Token: 0x040002BC RID: 700
		public static readonly Encoder Version;
	}
}
