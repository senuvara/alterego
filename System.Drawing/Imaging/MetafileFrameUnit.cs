﻿using System;

namespace System.Drawing.Imaging
{
	/// <summary>Specifies the unit of measurement for the rectangle used to size and position a metafile. This is specified during the creation of the <see cref="T:System.Drawing.Imaging.Metafile" /> object.</summary>
	// Token: 0x02000060 RID: 96
	public enum MetafileFrameUnit
	{
		/// <summary>The unit of measurement is 1/300 of an inch.</summary>
		// Token: 0x04000303 RID: 771
		Document = 5,
		/// <summary>The unit of measurement is 0.01 millimeter. Provided for compatibility with GDI.</summary>
		// Token: 0x04000304 RID: 772
		GdiCompatible = 7,
		/// <summary>The unit of measurement is 1 inch.</summary>
		// Token: 0x04000305 RID: 773
		Inch = 4,
		/// <summary>The unit of measurement is 1 millimeter.</summary>
		// Token: 0x04000306 RID: 774
		Millimeter = 6,
		/// <summary>The unit of measurement is 1 pixel.</summary>
		// Token: 0x04000307 RID: 775
		Pixel = 2,
		/// <summary>The unit of measurement is 1 printer's point.</summary>
		// Token: 0x04000308 RID: 776
		Point
	}
}
