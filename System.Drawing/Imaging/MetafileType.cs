﻿using System;

namespace System.Drawing.Imaging
{
	/// <summary>Specifies types of metafiles. The <see cref="P:System.Drawing.Imaging.MetafileHeader.Type" /> property returns a member of this enumeration.</summary>
	// Token: 0x02000062 RID: 98
	public enum MetafileType
	{
		/// <summary>Specifies an Enhanced Metafile (EMF) file. Such a file contains only GDI records.</summary>
		// Token: 0x0400030A RID: 778
		Emf = 3,
		/// <summary>Specifies an EMF+ Dual file. Such a file contains GDI+ records along with alternative GDI records and can be displayed by using either GDI or GDI+. Displaying the records using GDI may cause some quality degradation.</summary>
		// Token: 0x0400030B RID: 779
		EmfPlusDual = 5,
		/// <summary>Specifies an EMF+ file. Such a file contains only GDI+ records and must be displayed by using GDI+. Displaying the records using GDI may cause unpredictable results.</summary>
		// Token: 0x0400030C RID: 780
		EmfPlusOnly = 4,
		/// <summary>Specifies a metafile format that is not recognized in GDI+.</summary>
		// Token: 0x0400030D RID: 781
		Invalid = 0,
		/// <summary>Specifies a WMF (Windows Metafile) file. Such a file contains only GDI records.</summary>
		// Token: 0x0400030E RID: 782
		Wmf,
		/// <summary>Specifies a WMF (Windows Metafile) file that has a placeable metafile header in front of it.</summary>
		// Token: 0x0400030F RID: 783
		WmfPlaceable
	}
}
