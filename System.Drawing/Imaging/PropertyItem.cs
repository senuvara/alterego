﻿using System;
using Unity;

namespace System.Drawing.Imaging
{
	/// <summary>Encapsulates a metadata property to be included in an image file. Not inheritable.</summary>
	// Token: 0x02000043 RID: 67
	public sealed class PropertyItem
	{
		// Token: 0x0600040A RID: 1034 RVA: 0x00004644 File Offset: 0x00002844
		internal PropertyItem()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the ID of the property.</summary>
		/// <returns>The integer that represents the ID of the property.</returns>
		// Token: 0x17000118 RID: 280
		// (get) Token: 0x0600040B RID: 1035 RVA: 0x00005668 File Offset: 0x00003868
		// (set) Token: 0x0600040C RID: 1036 RVA: 0x00004644 File Offset: 0x00002844
		public int Id
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the length (in bytes) of the <see cref="P:System.Drawing.Imaging.PropertyItem.Value" /> property.</summary>
		/// <returns>An integer that represents the length (in bytes) of the <see cref="P:System.Drawing.Imaging.PropertyItem.Value" /> byte array.</returns>
		// Token: 0x17000119 RID: 281
		// (get) Token: 0x0600040D RID: 1037 RVA: 0x00005684 File Offset: 0x00003884
		// (set) Token: 0x0600040E RID: 1038 RVA: 0x00004644 File Offset: 0x00002844
		public int Len
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets an integer that defines the type of data contained in the <see cref="P:System.Drawing.Imaging.PropertyItem.Value" /> property.</summary>
		/// <returns>An integer that defines the type of data contained in <see cref="P:System.Drawing.Imaging.PropertyItem.Value" />.</returns>
		// Token: 0x1700011A RID: 282
		// (get) Token: 0x0600040F RID: 1039 RVA: 0x000056A0 File Offset: 0x000038A0
		// (set) Token: 0x06000410 RID: 1040 RVA: 0x00004644 File Offset: 0x00002844
		public short Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the value of the property item.</summary>
		/// <returns>A byte array that represents the value of the property item.</returns>
		// Token: 0x1700011B RID: 283
		// (get) Token: 0x06000411 RID: 1041 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000412 RID: 1042 RVA: 0x00004644 File Offset: 0x00002844
		public byte[] Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
