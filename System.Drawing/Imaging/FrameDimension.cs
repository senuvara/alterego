﻿using System;
using Unity;

namespace System.Drawing.Imaging
{
	/// <summary>Provides properties that get the frame dimensions of an image. Not inheritable.</summary>
	// Token: 0x0200004A RID: 74
	public sealed class FrameDimension
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Imaging.FrameDimension" /> class using the specified <see langword="Guid" /> structure.</summary>
		/// <param name="guid">A <see langword="Guid" /> structure that contains a GUID for this <see cref="T:System.Drawing.Imaging.FrameDimension" /> object. </param>
		// Token: 0x0600043E RID: 1086 RVA: 0x00004644 File Offset: 0x00002844
		public FrameDimension(Guid guid)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a globally unique identifier (GUID) that represents this <see cref="T:System.Drawing.Imaging.FrameDimension" /> object.</summary>
		/// <returns>A <see langword="Guid" /> structure that contains a GUID that represents this <see cref="T:System.Drawing.Imaging.FrameDimension" /> object.</returns>
		// Token: 0x1700012D RID: 301
		// (get) Token: 0x0600043F RID: 1087 RVA: 0x00005748 File Offset: 0x00003948
		public Guid Guid
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Guid);
			}
		}

		/// <summary>Gets the page dimension.</summary>
		/// <returns>The page dimension.</returns>
		// Token: 0x1700012E RID: 302
		// (get) Token: 0x06000440 RID: 1088 RVA: 0x00004667 File Offset: 0x00002867
		public static FrameDimension Page
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the resolution dimension.</summary>
		/// <returns>The resolution dimension.</returns>
		// Token: 0x1700012F RID: 303
		// (get) Token: 0x06000441 RID: 1089 RVA: 0x00004667 File Offset: 0x00002867
		public static FrameDimension Resolution
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the time dimension.</summary>
		/// <returns>The time dimension.</returns>
		// Token: 0x17000130 RID: 304
		// (get) Token: 0x06000442 RID: 1090 RVA: 0x00004667 File Offset: 0x00002867
		public static FrameDimension Time
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
