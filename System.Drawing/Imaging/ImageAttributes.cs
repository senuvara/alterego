﻿using System;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using Unity;

namespace System.Drawing.Imaging
{
	/// <summary>Contains information about how bitmap and metafile colors are manipulated during rendering. </summary>
	// Token: 0x02000052 RID: 82
	[StructLayout(LayoutKind.Sequential)]
	public sealed class ImageAttributes : ICloneable, IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Imaging.ImageAttributes" /> class.</summary>
		// Token: 0x0600046B RID: 1131 RVA: 0x00004644 File Offset: 0x00002844
		public ImageAttributes()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Clears the brush color-remap table of this <see cref="T:System.Drawing.Imaging.ImageAttributes" /> object.</summary>
		// Token: 0x0600046C RID: 1132 RVA: 0x00004644 File Offset: 0x00002844
		public void ClearBrushRemapTable()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Clears the color key (transparency range) for the default category.</summary>
		// Token: 0x0600046D RID: 1133 RVA: 0x00004644 File Offset: 0x00002844
		public void ClearColorKey()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Clears the color key (transparency range) for a specified category.</summary>
		/// <param name="type">An element of <see cref="T:System.Drawing.Imaging.ColorAdjustType" /> that specifies the category for which the color key is cleared. </param>
		// Token: 0x0600046E RID: 1134 RVA: 0x00004644 File Offset: 0x00002844
		public void ClearColorKey(ColorAdjustType type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Clears the color-adjustment matrix for the default category.</summary>
		// Token: 0x0600046F RID: 1135 RVA: 0x00004644 File Offset: 0x00002844
		public void ClearColorMatrix()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Clears the color-adjustment matrix for a specified category.</summary>
		/// <param name="type">An element of <see cref="T:System.Drawing.Imaging.ColorAdjustType" /> that specifies the category for which the color-adjustment matrix is cleared. </param>
		// Token: 0x06000470 RID: 1136 RVA: 0x00004644 File Offset: 0x00002844
		public void ClearColorMatrix(ColorAdjustType type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Disables gamma correction for the default category.</summary>
		// Token: 0x06000471 RID: 1137 RVA: 0x00004644 File Offset: 0x00002844
		public void ClearGamma()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Disables gamma correction for a specified category.</summary>
		/// <param name="type">An element of <see cref="T:System.Drawing.Imaging.ColorAdjustType" /> that specifies the category for which gamma correction is disabled. </param>
		// Token: 0x06000472 RID: 1138 RVA: 0x00004644 File Offset: 0x00002844
		public void ClearGamma(ColorAdjustType type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Clears the <see langword="NoOp" /> setting for the default category.</summary>
		// Token: 0x06000473 RID: 1139 RVA: 0x00004644 File Offset: 0x00002844
		public void ClearNoOp()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Clears the <see langword="NoOp" /> setting for a specified category.</summary>
		/// <param name="type">An element of <see cref="T:System.Drawing.Imaging.ColorAdjustType" /> that specifies the category for which the <see langword="NoOp" /> setting is cleared. </param>
		// Token: 0x06000474 RID: 1140 RVA: 0x00004644 File Offset: 0x00002844
		public void ClearNoOp(ColorAdjustType type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Clears the CMYK (cyan-magenta-yellow-black) output channel setting for the default category.</summary>
		// Token: 0x06000475 RID: 1141 RVA: 0x00004644 File Offset: 0x00002844
		public void ClearOutputChannel()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Clears the (cyan-magenta-yellow-black) output channel setting for a specified category.</summary>
		/// <param name="type">An element of <see cref="T:System.Drawing.Imaging.ColorAdjustType" /> that specifies the category for which the output channel setting is cleared. </param>
		// Token: 0x06000476 RID: 1142 RVA: 0x00004644 File Offset: 0x00002844
		public void ClearOutputChannel(ColorAdjustType type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Clears the output channel color profile setting for the default category.</summary>
		// Token: 0x06000477 RID: 1143 RVA: 0x00004644 File Offset: 0x00002844
		public void ClearOutputChannelColorProfile()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Clears the output channel color profile setting for a specified category.</summary>
		/// <param name="type">An element of <see cref="T:System.Drawing.Imaging.ColorAdjustType" /> that specifies the category for which the output channel profile setting is cleared. </param>
		// Token: 0x06000478 RID: 1144 RVA: 0x00004644 File Offset: 0x00002844
		public void ClearOutputChannelColorProfile(ColorAdjustType type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Clears the color-remap table for the default category.</summary>
		// Token: 0x06000479 RID: 1145 RVA: 0x00004644 File Offset: 0x00002844
		public void ClearRemapTable()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Clears the color-remap table for a specified category.</summary>
		/// <param name="type">An element of <see cref="T:System.Drawing.Imaging.ColorAdjustType" /> that specifies the category for which the remap table is cleared. </param>
		// Token: 0x0600047A RID: 1146 RVA: 0x00004644 File Offset: 0x00002844
		public void ClearRemapTable(ColorAdjustType type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Clears the threshold value for the default category.</summary>
		// Token: 0x0600047B RID: 1147 RVA: 0x00004644 File Offset: 0x00002844
		public void ClearThreshold()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Clears the threshold value for a specified category.</summary>
		/// <param name="type">An element of <see cref="T:System.Drawing.Imaging.ColorAdjustType" /> that specifies the category for which the threshold is cleared. </param>
		// Token: 0x0600047C RID: 1148 RVA: 0x00004644 File Offset: 0x00002844
		public void ClearThreshold(ColorAdjustType type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates an exact copy of this <see cref="T:System.Drawing.Imaging.ImageAttributes" /> object.</summary>
		/// <returns>The <see cref="T:System.Drawing.Imaging.ImageAttributes" /> object this class creates, cast as an object.</returns>
		// Token: 0x0600047D RID: 1149 RVA: 0x00004667 File Offset: 0x00002867
		public object Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Releases all resources used by this <see cref="T:System.Drawing.Imaging.ImageAttributes" /> object.</summary>
		// Token: 0x0600047E RID: 1150 RVA: 0x00004644 File Offset: 0x00002844
		public void Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adjusts the colors in a palette according to the adjustment settings of a specified category.</summary>
		/// <param name="palette">A <see cref="T:System.Drawing.Imaging.ColorPalette" /> that on input contains the palette to be adjusted, and on output contains the adjusted palette. </param>
		/// <param name="type">An element of <see cref="T:System.Drawing.Imaging.ColorAdjustType" /> that specifies the category whose adjustment settings will be applied to the palette. </param>
		// Token: 0x0600047F RID: 1151 RVA: 0x00004644 File Offset: 0x00002844
		public void GetAdjustedPalette(ColorPalette palette, ColorAdjustType type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the color-remap table for the brush category.</summary>
		/// <param name="map">An array of <see cref="T:System.Drawing.Imaging.ColorMap" /> objects. </param>
		// Token: 0x06000480 RID: 1152 RVA: 0x00004644 File Offset: 0x00002844
		public void SetBrushRemapTable(ColorMap[] map)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the color key for the default category.</summary>
		/// <param name="colorLow">The low color-key value. </param>
		/// <param name="colorHigh">The high color-key value. </param>
		// Token: 0x06000481 RID: 1153 RVA: 0x00004644 File Offset: 0x00002844
		public void SetColorKey(Color colorLow, Color colorHigh)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the color key (transparency range) for a specified category.</summary>
		/// <param name="colorLow">The low color-key value. </param>
		/// <param name="colorHigh">The high color-key value. </param>
		/// <param name="type">An element of <see cref="T:System.Drawing.Imaging.ColorAdjustType" /> that specifies the category for which the color key is set. </param>
		// Token: 0x06000482 RID: 1154 RVA: 0x00004644 File Offset: 0x00002844
		public void SetColorKey(Color colorLow, Color colorHigh, ColorAdjustType type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the color-adjustment matrix and the grayscale-adjustment matrix for the default category.</summary>
		/// <param name="newColorMatrix">The color-adjustment matrix. </param>
		/// <param name="grayMatrix">The grayscale-adjustment matrix. </param>
		// Token: 0x06000483 RID: 1155 RVA: 0x00004644 File Offset: 0x00002844
		public void SetColorMatrices(ColorMatrix newColorMatrix, ColorMatrix grayMatrix)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the color-adjustment matrix and the grayscale-adjustment matrix for the default category.</summary>
		/// <param name="newColorMatrix">The color-adjustment matrix. </param>
		/// <param name="grayMatrix">The grayscale-adjustment matrix. </param>
		/// <param name="flags">An element of <see cref="T:System.Drawing.Imaging.ColorMatrixFlag" /> that specifies the type of image and color that will be affected by the color-adjustment and grayscale-adjustment matrices. </param>
		// Token: 0x06000484 RID: 1156 RVA: 0x00004644 File Offset: 0x00002844
		public void SetColorMatrices(ColorMatrix newColorMatrix, ColorMatrix grayMatrix, ColorMatrixFlag flags)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the color-adjustment matrix and the grayscale-adjustment matrix for a specified category.</summary>
		/// <param name="newColorMatrix">The color-adjustment matrix. </param>
		/// <param name="grayMatrix">The grayscale-adjustment matrix. </param>
		/// <param name="mode">An element of <see cref="T:System.Drawing.Imaging.ColorMatrixFlag" /> that specifies the type of image and color that will be affected by the color-adjustment and grayscale-adjustment matrices. </param>
		/// <param name="type">An element of <see cref="T:System.Drawing.Imaging.ColorAdjustType" /> that specifies the category for which the color-adjustment and grayscale-adjustment matrices are set. </param>
		// Token: 0x06000485 RID: 1157 RVA: 0x00004644 File Offset: 0x00002844
		public void SetColorMatrices(ColorMatrix newColorMatrix, ColorMatrix grayMatrix, ColorMatrixFlag mode, ColorAdjustType type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the color-adjustment matrix for the default category.</summary>
		/// <param name="newColorMatrix">The color-adjustment matrix. </param>
		// Token: 0x06000486 RID: 1158 RVA: 0x00004644 File Offset: 0x00002844
		public void SetColorMatrix(ColorMatrix newColorMatrix)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the color-adjustment matrix for the default category.</summary>
		/// <param name="newColorMatrix">The color-adjustment matrix. </param>
		/// <param name="flags">An element of <see cref="T:System.Drawing.Imaging.ColorMatrixFlag" /> that specifies the type of image and color that will be affected by the color-adjustment matrix. </param>
		// Token: 0x06000487 RID: 1159 RVA: 0x00004644 File Offset: 0x00002844
		public void SetColorMatrix(ColorMatrix newColorMatrix, ColorMatrixFlag flags)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the color-adjustment matrix for a specified category.</summary>
		/// <param name="newColorMatrix">The color-adjustment matrix. </param>
		/// <param name="mode">An element of <see cref="T:System.Drawing.Imaging.ColorMatrixFlag" /> that specifies the type of image and color that will be affected by the color-adjustment matrix. </param>
		/// <param name="type">An element of <see cref="T:System.Drawing.Imaging.ColorAdjustType" /> that specifies the category for which the color-adjustment matrix is set. </param>
		// Token: 0x06000488 RID: 1160 RVA: 0x00004644 File Offset: 0x00002844
		public void SetColorMatrix(ColorMatrix newColorMatrix, ColorMatrixFlag mode, ColorAdjustType type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the gamma value for the default category.</summary>
		/// <param name="gamma">The gamma correction value. </param>
		// Token: 0x06000489 RID: 1161 RVA: 0x00004644 File Offset: 0x00002844
		public void SetGamma(float gamma)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the gamma value for a specified category.</summary>
		/// <param name="gamma">The gamma correction value. </param>
		/// <param name="type">An element of the <see cref="T:System.Drawing.Imaging.ColorAdjustType" /> enumeration that specifies the category for which the gamma value is set. </param>
		// Token: 0x0600048A RID: 1162 RVA: 0x00004644 File Offset: 0x00002844
		public void SetGamma(float gamma, ColorAdjustType type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Turns off color adjustment for the default category. You can call the <see cref="Overload:System.Drawing.Imaging.ImageAttributes.ClearNoOp" /> method to reinstate the color-adjustment settings that were in place before the call to the <see cref="Overload:System.Drawing.Imaging.ImageAttributes.SetNoOp" /> method.</summary>
		// Token: 0x0600048B RID: 1163 RVA: 0x00004644 File Offset: 0x00002844
		public void SetNoOp()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Turns off color adjustment for a specified category. You can call the <see cref="Overload:System.Drawing.Imaging.ImageAttributes.ClearNoOp" /> method to reinstate the color-adjustment settings that were in place before the call to the <see cref="Overload:System.Drawing.Imaging.ImageAttributes.SetNoOp" /> method.</summary>
		/// <param name="type">An element of <see cref="T:System.Drawing.Imaging.ColorAdjustType" /> that specifies the category for which color correction is turned off. </param>
		// Token: 0x0600048C RID: 1164 RVA: 0x00004644 File Offset: 0x00002844
		public void SetNoOp(ColorAdjustType type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the CMYK (cyan-magenta-yellow-black) output channel for the default category.</summary>
		/// <param name="flags">An element of <see cref="T:System.Drawing.Imaging.ColorChannelFlag" /> that specifies the output channel. </param>
		// Token: 0x0600048D RID: 1165 RVA: 0x00004644 File Offset: 0x00002844
		public void SetOutputChannel(ColorChannelFlag flags)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the CMYK (cyan-magenta-yellow-black) output channel for a specified category.</summary>
		/// <param name="flags">An element of <see cref="T:System.Drawing.Imaging.ColorChannelFlag" /> that specifies the output channel. </param>
		/// <param name="type">An element of <see cref="T:System.Drawing.Imaging.ColorAdjustType" /> that specifies the category for which the output channel is set. </param>
		// Token: 0x0600048E RID: 1166 RVA: 0x00004644 File Offset: 0x00002844
		public void SetOutputChannel(ColorChannelFlag flags, ColorAdjustType type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the output channel color-profile file for the default category.</summary>
		/// <param name="colorProfileFilename">The path name of a color-profile file. If the color-profile file is in the %SystemRoot%\System32\Spool\Drivers\Color directory, this parameter can be the file name. Otherwise, this parameter must be the fully qualified path name. </param>
		// Token: 0x0600048F RID: 1167 RVA: 0x00004644 File Offset: 0x00002844
		public void SetOutputChannelColorProfile(string colorProfileFilename)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the output channel color-profile file for a specified category.</summary>
		/// <param name="colorProfileFilename">The path name of a color-profile file. If the color-profile file is in the %SystemRoot%\System32\Spool\Drivers\Color directory, this parameter can be the file name. Otherwise, this parameter must be the fully qualified path name. </param>
		/// <param name="type">An element of <see cref="T:System.Drawing.Imaging.ColorAdjustType" /> that specifies the category for which the output channel color-profile file is set. </param>
		// Token: 0x06000490 RID: 1168 RVA: 0x00004644 File Offset: 0x00002844
		public void SetOutputChannelColorProfile(string colorProfileFilename, ColorAdjustType type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the color-remap table for the default category.</summary>
		/// <param name="map">An array of color pairs of type <see cref="T:System.Drawing.Imaging.ColorMap" />. Each color pair contains an existing color (the first value) and the color that it will be mapped to (the second value). </param>
		// Token: 0x06000491 RID: 1169 RVA: 0x00004644 File Offset: 0x00002844
		public void SetRemapTable(ColorMap[] map)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the color-remap table for a specified category.</summary>
		/// <param name="map">An array of color pairs of type <see cref="T:System.Drawing.Imaging.ColorMap" />. Each color pair contains an existing color (the first value) and the color that it will be mapped to (the second value). </param>
		/// <param name="type">An element of <see cref="T:System.Drawing.Imaging.ColorAdjustType" /> that specifies the category for which the color-remap table is set. </param>
		// Token: 0x06000492 RID: 1170 RVA: 0x00004644 File Offset: 0x00002844
		public void SetRemapTable(ColorMap[] map, ColorAdjustType type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the threshold (transparency range) for the default category.</summary>
		/// <param name="threshold">A real number that specifies the threshold value. </param>
		// Token: 0x06000493 RID: 1171 RVA: 0x00004644 File Offset: 0x00002844
		public void SetThreshold(float threshold)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the threshold (transparency range) for a specified category.</summary>
		/// <param name="threshold">A threshold value from 0.0 to 1.0 that is used as a breakpoint to sort colors that will be mapped to either a maximum or a minimum value. </param>
		/// <param name="type">An element of <see cref="T:System.Drawing.Imaging.ColorAdjustType" /> that specifies the category for which the color threshold is set. </param>
		// Token: 0x06000494 RID: 1172 RVA: 0x00004644 File Offset: 0x00002844
		public void SetThreshold(float threshold, ColorAdjustType type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the wrap mode that is used to decide how to tile a texture across a shape, or at shape boundaries. A texture is tiled across a shape to fill it in when the texture is smaller than the shape it is filling.</summary>
		/// <param name="mode">An element of <see cref="T:System.Drawing.Drawing2D.WrapMode" /> that specifies how repeated copies of an image are used to tile an area. </param>
		// Token: 0x06000495 RID: 1173 RVA: 0x00004644 File Offset: 0x00002844
		public void SetWrapMode(WrapMode mode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the wrap mode and color used to decide how to tile a texture across a shape, or at shape boundaries. A texture is tiled across a shape to fill it in when the texture is smaller than the shape it is filling.</summary>
		/// <param name="mode">An element of <see cref="T:System.Drawing.Drawing2D.WrapMode" /> that specifies how repeated copies of an image are used to tile an area. </param>
		/// <param name="color">An <see cref="T:System.Drawing.Imaging.ImageAttributes" /> object that specifies the color of pixels outside of a rendered image. This color is visible if the mode parameter is set to <see cref="F:System.Drawing.Drawing2D.WrapMode.Clamp" /> and the source rectangle passed to <see cref="Overload:System.Drawing.Graphics.DrawImage" /> is larger than the image itself. </param>
		// Token: 0x06000496 RID: 1174 RVA: 0x00004644 File Offset: 0x00002844
		public void SetWrapMode(WrapMode mode, Color color)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the wrap mode and color used to decide how to tile a texture across a shape, or at shape boundaries. A texture is tiled across a shape to fill it in when the texture is smaller than the shape it is filling.</summary>
		/// <param name="mode">An element of <see cref="T:System.Drawing.Drawing2D.WrapMode" /> that specifies how repeated copies of an image are used to tile an area. </param>
		/// <param name="color">A color object that specifies the color of pixels outside of a rendered image. This color is visible if the mode parameter is set to <see cref="F:System.Drawing.Drawing2D.WrapMode.Clamp" /> and the source rectangle passed to <see cref="Overload:System.Drawing.Graphics.DrawImage" /> is larger than the image itself. </param>
		/// <param name="clamp">This parameter has no effect. Set it to <see langword="false" />. </param>
		// Token: 0x06000497 RID: 1175 RVA: 0x00004644 File Offset: 0x00002844
		public void SetWrapMode(WrapMode mode, Color color, bool clamp)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
