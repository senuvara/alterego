﻿using System;

namespace System.Drawing.Imaging
{
	/// <summary>Specifies the attributes of the pixel data contained in an <see cref="T:System.Drawing.Image" /> object. The <see cref="P:System.Drawing.Image.Flags" /> property returns a member of this enumeration.</summary>
	// Token: 0x020000B1 RID: 177
	[Flags]
	public enum ImageFlags
	{
		/// <summary>The pixel data can be cached for faster access.</summary>
		// Token: 0x040003F5 RID: 1013
		Caching = 131072,
		/// <summary>The pixel data uses a CMYK color space.</summary>
		// Token: 0x040003F6 RID: 1014
		ColorSpaceCmyk = 32,
		/// <summary>The pixel data is grayscale.</summary>
		// Token: 0x040003F7 RID: 1015
		ColorSpaceGray = 64,
		/// <summary>The pixel data uses an RGB color space.</summary>
		// Token: 0x040003F8 RID: 1016
		ColorSpaceRgb = 16,
		/// <summary>Specifies that the image is stored using a YCBCR color space.</summary>
		// Token: 0x040003F9 RID: 1017
		ColorSpaceYcbcr = 128,
		/// <summary>Specifies that the image is stored using a YCCK color space.</summary>
		// Token: 0x040003FA RID: 1018
		ColorSpaceYcck = 256,
		/// <summary>The pixel data contains alpha information.</summary>
		// Token: 0x040003FB RID: 1019
		HasAlpha = 2,
		/// <summary>Specifies that dots per inch information is stored in the image.</summary>
		// Token: 0x040003FC RID: 1020
		HasRealDpi = 4096,
		/// <summary>Specifies that the pixel size is stored in the image.</summary>
		// Token: 0x040003FD RID: 1021
		HasRealPixelSize = 8192,
		/// <summary>Specifies that the pixel data has alpha values other than 0 (transparent) and 255 (opaque).</summary>
		// Token: 0x040003FE RID: 1022
		HasTranslucent = 4,
		/// <summary>There is no format information.</summary>
		// Token: 0x040003FF RID: 1023
		None = 0,
		/// <summary>The pixel data is partially scalable, but there are some limitations.</summary>
		// Token: 0x04000400 RID: 1024
		PartiallyScalable = 8,
		/// <summary>The pixel data is read-only.</summary>
		// Token: 0x04000401 RID: 1025
		ReadOnly = 65536,
		/// <summary>The pixel data is scalable.</summary>
		// Token: 0x04000402 RID: 1026
		Scalable = 1
	}
}
