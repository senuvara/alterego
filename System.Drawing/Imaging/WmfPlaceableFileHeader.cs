﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.Drawing.Imaging
{
	/// <summary>Defines a placeable metafile. Not inheritable.</summary>
	// Token: 0x0200005F RID: 95
	[StructLayout(LayoutKind.Sequential)]
	public sealed class WmfPlaceableFileHeader
	{
		/// <summary>Initializes a new instance of the <see langword="WmfPlaceableFileHeader" /> class.</summary>
		// Token: 0x0600052F RID: 1327 RVA: 0x00004644 File Offset: 0x00002844
		public WmfPlaceableFileHeader()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the y-coordinate of the lower-right corner of the bounding rectangle of the metafile image on the output device.</summary>
		/// <returns>The y-coordinate of the lower-right corner of the bounding rectangle of the metafile image on the output device.</returns>
		// Token: 0x1700016E RID: 366
		// (get) Token: 0x06000530 RID: 1328 RVA: 0x00005D68 File Offset: 0x00003F68
		// (set) Token: 0x06000531 RID: 1329 RVA: 0x00004644 File Offset: 0x00002844
		public short BboxBottom
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the x-coordinate of the upper-left corner of the bounding rectangle of the metafile image on the output device.</summary>
		/// <returns>The x-coordinate of the upper-left corner of the bounding rectangle of the metafile image on the output device.</returns>
		// Token: 0x1700016F RID: 367
		// (get) Token: 0x06000532 RID: 1330 RVA: 0x00005D84 File Offset: 0x00003F84
		// (set) Token: 0x06000533 RID: 1331 RVA: 0x00004644 File Offset: 0x00002844
		public short BboxLeft
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the x-coordinate of the lower-right corner of the bounding rectangle of the metafile image on the output device.</summary>
		/// <returns>The x-coordinate of the lower-right corner of the bounding rectangle of the metafile image on the output device.</returns>
		// Token: 0x17000170 RID: 368
		// (get) Token: 0x06000534 RID: 1332 RVA: 0x00005DA0 File Offset: 0x00003FA0
		// (set) Token: 0x06000535 RID: 1333 RVA: 0x00004644 File Offset: 0x00002844
		public short BboxRight
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the y-coordinate of the upper-left corner of the bounding rectangle of the metafile image on the output device.</summary>
		/// <returns>The y-coordinate of the upper-left corner of the bounding rectangle of the metafile image on the output device.</returns>
		// Token: 0x17000171 RID: 369
		// (get) Token: 0x06000536 RID: 1334 RVA: 0x00005DBC File Offset: 0x00003FBC
		// (set) Token: 0x06000537 RID: 1335 RVA: 0x00004644 File Offset: 0x00002844
		public short BboxTop
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the checksum value for the previous ten <see langword="WORD" /> s in the header.</summary>
		/// <returns>The checksum value for the previous ten <see langword="WORD" /> s in the header.</returns>
		// Token: 0x17000172 RID: 370
		// (get) Token: 0x06000538 RID: 1336 RVA: 0x00005DD8 File Offset: 0x00003FD8
		// (set) Token: 0x06000539 RID: 1337 RVA: 0x00004644 File Offset: 0x00002844
		public short Checksum
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the handle of the metafile in memory.</summary>
		/// <returns>The handle of the metafile in memory.</returns>
		// Token: 0x17000173 RID: 371
		// (get) Token: 0x0600053A RID: 1338 RVA: 0x00005DF4 File Offset: 0x00003FF4
		// (set) Token: 0x0600053B RID: 1339 RVA: 0x00004644 File Offset: 0x00002844
		public short Hmf
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the number of twips per inch.</summary>
		/// <returns>The number of twips per inch.</returns>
		// Token: 0x17000174 RID: 372
		// (get) Token: 0x0600053C RID: 1340 RVA: 0x00005E10 File Offset: 0x00004010
		// (set) Token: 0x0600053D RID: 1341 RVA: 0x00004644 File Offset: 0x00002844
		public short Inch
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating the presence of a placeable metafile header.</summary>
		/// <returns>A value indicating presence of a placeable metafile header.</returns>
		// Token: 0x17000175 RID: 373
		// (get) Token: 0x0600053E RID: 1342 RVA: 0x00005E2C File Offset: 0x0000402C
		// (set) Token: 0x0600053F RID: 1343 RVA: 0x00004644 File Offset: 0x00002844
		public int Key
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Reserved. Do not use.</summary>
		/// <returns>Reserved. Do not use.</returns>
		// Token: 0x17000176 RID: 374
		// (get) Token: 0x06000540 RID: 1344 RVA: 0x00005E48 File Offset: 0x00004048
		// (set) Token: 0x06000541 RID: 1345 RVA: 0x00004644 File Offset: 0x00002844
		public int Reserved
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
