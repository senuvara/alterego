﻿using System;

namespace System.Drawing.Imaging
{
	/// <summary>Specifies individual channels in the CMYK (cyan, magenta, yellow, black) color space. This enumeration is used by the <see cref="Overload:System.Drawing.Imaging.ImageAttributes.SetOutputChannel" /> methods.</summary>
	// Token: 0x02000057 RID: 87
	public enum ColorChannelFlag
	{
		/// <summary>The cyan color channel.</summary>
		// Token: 0x040002F3 RID: 755
		ColorChannelC,
		/// <summary>The black color channel.</summary>
		// Token: 0x040002F4 RID: 756
		ColorChannelK = 3,
		/// <summary>The last selected channel should be used.</summary>
		// Token: 0x040002F5 RID: 757
		ColorChannelLast,
		/// <summary>The magenta color channel.</summary>
		// Token: 0x040002F6 RID: 758
		ColorChannelM = 1,
		/// <summary>The yellow color channel.</summary>
		// Token: 0x040002F7 RID: 759
		ColorChannelY
	}
}
