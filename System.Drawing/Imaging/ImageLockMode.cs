﻿using System;

namespace System.Drawing.Imaging
{
	/// <summary>Specifies flags that are passed to the flags parameter of the <see cref="Overload:System.Drawing.Bitmap.LockBits" /> method. The <see cref="Overload:System.Drawing.Bitmap.LockBits" /> method locks a portion of an image so that you can read or write the pixel data.</summary>
	// Token: 0x0200004F RID: 79
	public enum ImageLockMode
	{
		/// <summary>Specifies that a portion of the image is locked for reading.</summary>
		// Token: 0x040002E2 RID: 738
		ReadOnly = 1,
		/// <summary>Specifies that a portion of the image is locked for reading or writing.</summary>
		// Token: 0x040002E3 RID: 739
		ReadWrite = 3,
		/// <summary>Specifies that the buffer used for reading or writing pixel data is allocated by the user. If this flag is set, the <paramref name="flags" /> parameter of the <see cref="Overload:System.Drawing.Bitmap.LockBits" /> method serves as an input parameter (and possibly as an output parameter). If this flag is cleared, then the <paramref name="flags" /> parameter serves only as an output parameter.</summary>
		// Token: 0x040002E4 RID: 740
		UserInputBuffer,
		/// <summary>Specifies that a portion of the image is locked for writing.</summary>
		// Token: 0x040002E5 RID: 741
		WriteOnly = 2
	}
}
