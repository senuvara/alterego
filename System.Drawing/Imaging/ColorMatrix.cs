﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.Drawing.Imaging
{
	/// <summary>Defines a 5 x 5 matrix that contains the coordinates for the RGBAW space. Several methods of the <see cref="T:System.Drawing.Imaging.ImageAttributes" /> class adjust image colors by using a color matrix. This class cannot be inherited.</summary>
	// Token: 0x02000055 RID: 85
	[StructLayout(LayoutKind.Sequential)]
	public sealed class ColorMatrix
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Imaging.ColorMatrix" /> class.</summary>
		// Token: 0x0600049D RID: 1181 RVA: 0x00004644 File Offset: 0x00002844
		public ColorMatrix()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Imaging.ColorMatrix" /> class using the elements in the specified matrix <paramref name="newColorMatrix" />.</summary>
		/// <param name="newColorMatrix">The values of the elements for the new <see cref="T:System.Drawing.Imaging.ColorMatrix" />. </param>
		// Token: 0x0600049E RID: 1182 RVA: 0x00004644 File Offset: 0x00002844
		[CLSCompliant(false)]
		public ColorMatrix(float[][] newColorMatrix)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the element at the specified row and column in the <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</summary>
		/// <param name="row">The row of the element.</param>
		/// <param name="column">The column of the element.</param>
		/// <returns>The element at the specified row and column.</returns>
		// Token: 0x17000144 RID: 324
		public float this[int row, int column]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the 0 (zero) row and 0 column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</summary>
		/// <returns>The element at the 0 row and 0 column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x17000145 RID: 325
		// (get) Token: 0x060004A1 RID: 1185 RVA: 0x000058D0 File Offset: 0x00003AD0
		// (set) Token: 0x060004A2 RID: 1186 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix00
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the 0 (zero) row and first column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</summary>
		/// <returns>The element at the 0 row and first column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" /> .</returns>
		// Token: 0x17000146 RID: 326
		// (get) Token: 0x060004A3 RID: 1187 RVA: 0x000058EC File Offset: 0x00003AEC
		// (set) Token: 0x060004A4 RID: 1188 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix01
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the 0 (zero) row and second column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</summary>
		/// <returns>The element at the 0 row and second column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x17000147 RID: 327
		// (get) Token: 0x060004A5 RID: 1189 RVA: 0x00005908 File Offset: 0x00003B08
		// (set) Token: 0x060004A6 RID: 1190 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix02
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the 0 (zero) row and third column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />. Represents the alpha component.</summary>
		/// <returns>The element at the 0 row and third column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x17000148 RID: 328
		// (get) Token: 0x060004A7 RID: 1191 RVA: 0x00005924 File Offset: 0x00003B24
		// (set) Token: 0x060004A8 RID: 1192 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix03
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the 0 (zero) row and fourth column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</summary>
		/// <returns>The element at the 0 row and fourth column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x17000149 RID: 329
		// (get) Token: 0x060004A9 RID: 1193 RVA: 0x00005940 File Offset: 0x00003B40
		// (set) Token: 0x060004AA RID: 1194 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix04
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the first row and 0 (zero) column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</summary>
		/// <returns>The element at the first row and 0 column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x1700014A RID: 330
		// (get) Token: 0x060004AB RID: 1195 RVA: 0x0000595C File Offset: 0x00003B5C
		// (set) Token: 0x060004AC RID: 1196 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix10
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the first row and first column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</summary>
		/// <returns>The element at the first row and first column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x1700014B RID: 331
		// (get) Token: 0x060004AD RID: 1197 RVA: 0x00005978 File Offset: 0x00003B78
		// (set) Token: 0x060004AE RID: 1198 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix11
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the first row and second column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</summary>
		/// <returns>The element at the first row and second column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x1700014C RID: 332
		// (get) Token: 0x060004AF RID: 1199 RVA: 0x00005994 File Offset: 0x00003B94
		// (set) Token: 0x060004B0 RID: 1200 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix12
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the first row and third column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />. Represents the alpha component.</summary>
		/// <returns>The element at the first row and third column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x1700014D RID: 333
		// (get) Token: 0x060004B1 RID: 1201 RVA: 0x000059B0 File Offset: 0x00003BB0
		// (set) Token: 0x060004B2 RID: 1202 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix13
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the first row and fourth column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</summary>
		/// <returns>The element at the first row and fourth column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x1700014E RID: 334
		// (get) Token: 0x060004B3 RID: 1203 RVA: 0x000059CC File Offset: 0x00003BCC
		// (set) Token: 0x060004B4 RID: 1204 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix14
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the second row and 0 (zero) column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</summary>
		/// <returns>The element at the second row and 0 column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x1700014F RID: 335
		// (get) Token: 0x060004B5 RID: 1205 RVA: 0x000059E8 File Offset: 0x00003BE8
		// (set) Token: 0x060004B6 RID: 1206 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix20
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the second row and first column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</summary>
		/// <returns>The element at the second row and first column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x17000150 RID: 336
		// (get) Token: 0x060004B7 RID: 1207 RVA: 0x00005A04 File Offset: 0x00003C04
		// (set) Token: 0x060004B8 RID: 1208 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix21
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the second row and second column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</summary>
		/// <returns>The element at the second row and second column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x17000151 RID: 337
		// (get) Token: 0x060004B9 RID: 1209 RVA: 0x00005A20 File Offset: 0x00003C20
		// (set) Token: 0x060004BA RID: 1210 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix22
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the second row and third column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</summary>
		/// <returns>The element at the second row and third column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x17000152 RID: 338
		// (get) Token: 0x060004BB RID: 1211 RVA: 0x00005A3C File Offset: 0x00003C3C
		// (set) Token: 0x060004BC RID: 1212 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix23
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the second row and fourth column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</summary>
		/// <returns>The element at the second row and fourth column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x17000153 RID: 339
		// (get) Token: 0x060004BD RID: 1213 RVA: 0x00005A58 File Offset: 0x00003C58
		// (set) Token: 0x060004BE RID: 1214 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix24
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the third row and 0 (zero) column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</summary>
		/// <returns>The element at the third row and 0 column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x17000154 RID: 340
		// (get) Token: 0x060004BF RID: 1215 RVA: 0x00005A74 File Offset: 0x00003C74
		// (set) Token: 0x060004C0 RID: 1216 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix30
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the third row and first column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</summary>
		/// <returns>The element at the third row and first column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x17000155 RID: 341
		// (get) Token: 0x060004C1 RID: 1217 RVA: 0x00005A90 File Offset: 0x00003C90
		// (set) Token: 0x060004C2 RID: 1218 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix31
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the third row and second column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</summary>
		/// <returns>The element at the third row and second column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x17000156 RID: 342
		// (get) Token: 0x060004C3 RID: 1219 RVA: 0x00005AAC File Offset: 0x00003CAC
		// (set) Token: 0x060004C4 RID: 1220 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix32
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the third row and third column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />. Represents the alpha component.</summary>
		/// <returns>The element at the third row and third column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x17000157 RID: 343
		// (get) Token: 0x060004C5 RID: 1221 RVA: 0x00005AC8 File Offset: 0x00003CC8
		// (set) Token: 0x060004C6 RID: 1222 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix33
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the third row and fourth column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</summary>
		/// <returns>The element at the third row and fourth column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x17000158 RID: 344
		// (get) Token: 0x060004C7 RID: 1223 RVA: 0x00005AE4 File Offset: 0x00003CE4
		// (set) Token: 0x060004C8 RID: 1224 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix34
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the fourth row and 0 (zero) column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</summary>
		/// <returns>The element at the fourth row and 0 column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x17000159 RID: 345
		// (get) Token: 0x060004C9 RID: 1225 RVA: 0x00005B00 File Offset: 0x00003D00
		// (set) Token: 0x060004CA RID: 1226 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix40
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the fourth row and first column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</summary>
		/// <returns>The element at the fourth row and first column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x1700015A RID: 346
		// (get) Token: 0x060004CB RID: 1227 RVA: 0x00005B1C File Offset: 0x00003D1C
		// (set) Token: 0x060004CC RID: 1228 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix41
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the fourth row and second column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</summary>
		/// <returns>The element at the fourth row and second column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x1700015B RID: 347
		// (get) Token: 0x060004CD RID: 1229 RVA: 0x00005B38 File Offset: 0x00003D38
		// (set) Token: 0x060004CE RID: 1230 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix42
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the fourth row and third column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />. Represents the alpha component.</summary>
		/// <returns>The element at the fourth row and third column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x1700015C RID: 348
		// (get) Token: 0x060004CF RID: 1231 RVA: 0x00005B54 File Offset: 0x00003D54
		// (set) Token: 0x060004D0 RID: 1232 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix43
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the element at the fourth row and fourth column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</summary>
		/// <returns>The element at the fourth row and fourth column of this <see cref="T:System.Drawing.Imaging.ColorMatrix" />.</returns>
		// Token: 0x1700015D RID: 349
		// (get) Token: 0x060004D1 RID: 1233 RVA: 0x00005B70 File Offset: 0x00003D70
		// (set) Token: 0x060004D2 RID: 1234 RVA: 0x00004644 File Offset: 0x00002844
		public float Matrix44
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
