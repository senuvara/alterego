﻿using System;

namespace System.Drawing.Imaging
{
	/// <summary>Specifies which GDI+ objects use color adjustment information.</summary>
	// Token: 0x02000053 RID: 83
	public enum ColorAdjustType
	{
		/// <summary>The number of types specified.</summary>
		// Token: 0x040002E7 RID: 743
		Any = 6,
		/// <summary>Color adjustment information for <see cref="T:System.Drawing.Bitmap" /> objects.</summary>
		// Token: 0x040002E8 RID: 744
		Bitmap = 1,
		/// <summary>Color adjustment information for <see cref="T:System.Drawing.Brush" /> objects.</summary>
		// Token: 0x040002E9 RID: 745
		Brush,
		/// <summary>The number of types specified.</summary>
		// Token: 0x040002EA RID: 746
		Count = 5,
		/// <summary>Color adjustment information that is used by all GDI+ objects that do not have their own color adjustment information.</summary>
		// Token: 0x040002EB RID: 747
		Default = 0,
		/// <summary>Color adjustment information for <see cref="T:System.Drawing.Pen" /> objects.</summary>
		// Token: 0x040002EC RID: 748
		Pen = 3,
		/// <summary>Color adjustment information for text.</summary>
		// Token: 0x040002ED RID: 749
		Text
	}
}
