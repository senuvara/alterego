﻿using System;

namespace System.Drawing.Imaging
{
	/// <summary>Used to specify the data type of the <see cref="T:System.Drawing.Imaging.EncoderParameter" /> used with the <see cref="Overload:System.Drawing.Image.Save" /> or <see cref="Overload:System.Drawing.Image.SaveAdd" /> method of an image. </summary>
	// Token: 0x02000049 RID: 73
	public enum EncoderParameterValueType
	{
		/// <summary>Specifies that the array of values is a null-terminated ASCII character string. Note that the <see langword="NumberOfValues" /> data member of the <see cref="T:System.Drawing.Imaging.EncoderParameter" /> object indicates the length of the character string including the NULL terminator.</summary>
		// Token: 0x040002BE RID: 702
		ValueTypeAscii = 2,
		/// <summary>Specifies that each value in the array is an 8-bit unsigned integer.</summary>
		// Token: 0x040002BF RID: 703
		ValueTypeByte = 1,
		/// <summary>Specifies that each value in the array is a 32-bit unsigned integer.</summary>
		// Token: 0x040002C0 RID: 704
		ValueTypeLong = 4,
		/// <summary>Specifies that each value in the array is a pair of 32-bit unsigned integers. Each pair represents a range of numbers.</summary>
		// Token: 0x040002C1 RID: 705
		ValueTypeLongRange = 6,
		/// <summary>Specifies that each value in the array is a pair of 32-bit unsigned integers. Each pair represents a fraction, the first integer being the numerator and the second integer being the denominator.</summary>
		// Token: 0x040002C2 RID: 706
		ValueTypeRational = 5,
		/// <summary>Specifies that each value in the array is a set of four, 32-bit unsigned integers. The first two integers represent one fraction, and the second two integers represent a second fraction. The two fractions represent a range of rational numbers. The first fraction is the smallest rational number in the range, and the second fraction is the largest rational number in the range.</summary>
		// Token: 0x040002C3 RID: 707
		ValueTypeRationalRange = 8,
		/// <summary>Specifies that each value in the array is a 16-bit, unsigned integer.</summary>
		// Token: 0x040002C4 RID: 708
		ValueTypeShort = 3,
		/// <summary>Specifies that the array of values is an array of bytes that has no data type defined.</summary>
		// Token: 0x040002C5 RID: 709
		ValueTypeUndefined = 7
	}
}
