﻿using System;

namespace System.Drawing.Imaging
{
	/// <summary>Used to specify the parameter value passed to a JPEG or TIFF image encoder when using the <see cref="M:System.Drawing.Image.Save(System.String,System.Drawing.Imaging.ImageCodecInfo,System.Drawing.Imaging.EncoderParameters)" /> or <see cref="M:System.Drawing.Image.SaveAdd(System.Drawing.Imaging.EncoderParameters)" /> methods.</summary>
	// Token: 0x020000B0 RID: 176
	public enum EncoderValue
	{
		/// <summary>Not used in GDI+ version 1.0.</summary>
		// Token: 0x040003DC RID: 988
		ColorTypeCMYK,
		/// <summary>Not used in GDI+ version 1.0.</summary>
		// Token: 0x040003DD RID: 989
		ColorTypeYCCK,
		/// <summary>Specifies the CCITT3 compression scheme. Can be passed to the TIFF encoder as a parameter that belongs to the compression category.</summary>
		// Token: 0x040003DE RID: 990
		CompressionCCITT3 = 3,
		/// <summary>Specifies the CCITT4 compression scheme. Can be passed to the TIFF encoder as a parameter that belongs to the compression category.</summary>
		// Token: 0x040003DF RID: 991
		CompressionCCITT4,
		/// <summary>Specifies the LZW compression scheme. Can be passed to the TIFF encoder as a parameter that belongs to the Compression category.</summary>
		// Token: 0x040003E0 RID: 992
		CompressionLZW = 2,
		/// <summary>Specifies no compression. Can be passed to the TIFF encoder as a parameter that belongs to the compression category.</summary>
		// Token: 0x040003E1 RID: 993
		CompressionNone = 6,
		/// <summary>Specifies the RLE compression scheme. Can be passed to the TIFF encoder as a parameter that belongs to the compression category.</summary>
		// Token: 0x040003E2 RID: 994
		CompressionRle = 5,
		/// <summary>Specifies that a multiple-frame file or stream should be closed. Can be passed to the TIFF encoder as a parameter that belongs to the save flag category.</summary>
		// Token: 0x040003E3 RID: 995
		Flush = 20,
		/// <summary>Specifies that a frame is to be added to the page dimension of an image. Can be passed to the TIFF encoder as a parameter that belongs to the save flag category.</summary>
		// Token: 0x040003E4 RID: 996
		FrameDimensionPage = 23,
		/// <summary>Not used in GDI+ version 1.0.</summary>
		// Token: 0x040003E5 RID: 997
		FrameDimensionResolution = 22,
		/// <summary>Not used in GDI+ version 1.0.</summary>
		// Token: 0x040003E6 RID: 998
		FrameDimensionTime = 21,
		/// <summary>Specifies the last frame in a multiple-frame image. Can be passed to the TIFF encoder as a parameter that belongs to the save flag category.</summary>
		// Token: 0x040003E7 RID: 999
		LastFrame = 19,
		/// <summary>Specifies that the image has more than one frame (page). Can be passed to the TIFF encoder as a parameter that belongs to the save flag category.</summary>
		// Token: 0x040003E8 RID: 1000
		MultiFrame = 18,
		/// <summary>Not used in GDI+ version 1.0.</summary>
		// Token: 0x040003E9 RID: 1001
		RenderNonProgressive = 12,
		/// <summary>Not used in GDI+ version 1.0.</summary>
		// Token: 0x040003EA RID: 1002
		RenderProgressive = 11,
		/// <summary>Not used in GDI+ version 1.0.</summary>
		// Token: 0x040003EB RID: 1003
		ScanMethodInterlaced = 7,
		/// <summary>Not used in GDI+ version 1.0.</summary>
		// Token: 0x040003EC RID: 1004
		ScanMethodNonInterlaced,
		/// <summary>Specifies that the image is to be flipped horizontally (about the vertical axis). Can be passed to the JPEG encoder as a parameter that belongs to the transformation category.</summary>
		// Token: 0x040003ED RID: 1005
		TransformFlipHorizontal = 16,
		/// <summary>Specifies that the image is to be flipped vertically (about the horizontal axis). Can be passed to the JPEG encoder as a parameter that belongs to the transformation category.</summary>
		// Token: 0x040003EE RID: 1006
		TransformFlipVertical,
		/// <summary>Specifies that the image is to be rotated 180 degrees about its center. Can be passed to the JPEG encoder as a parameter that belongs to the transformation category.</summary>
		// Token: 0x040003EF RID: 1007
		TransformRotate180 = 14,
		/// <summary>Specifies that the image is to be rotated clockwise 270 degrees about its center. Can be passed to the JPEG encoder as a parameter that belongs to the transformation category.</summary>
		// Token: 0x040003F0 RID: 1008
		TransformRotate270,
		/// <summary>Specifies that the image is to be rotated clockwise 90 degrees about its center. Can be passed to the JPEG encoder as a parameter that belongs to the transformation category.</summary>
		// Token: 0x040003F1 RID: 1009
		TransformRotate90 = 13,
		/// <summary>Not used in GDI+ version 1.0.</summary>
		// Token: 0x040003F2 RID: 1010
		VersionGif87 = 9,
		/// <summary>Not used in GDI+ version 1.0.</summary>
		// Token: 0x040003F3 RID: 1011
		VersionGif89
	}
}
