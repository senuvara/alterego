﻿using System;
using Unity;

namespace System.Drawing.Imaging
{
	/// <summary>The <see cref="T:System.Drawing.Imaging.ImageCodecInfo" /> class provides the necessary storage members and methods to retrieve all pertinent information about the installed image encoders and decoders (called codecs). Not inheritable. </summary>
	// Token: 0x0200004C RID: 76
	public sealed class ImageCodecInfo
	{
		// Token: 0x06000443 RID: 1091 RVA: 0x00004644 File Offset: 0x00002844
		internal ImageCodecInfo()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets a <see cref="T:System.Guid" /> structure that contains a GUID that identifies a specific codec.</summary>
		/// <returns>A <see cref="T:System.Guid" /> structure that contains a GUID that identifies a specific codec.</returns>
		// Token: 0x17000131 RID: 305
		// (get) Token: 0x06000444 RID: 1092 RVA: 0x00005764 File Offset: 0x00003964
		// (set) Token: 0x06000445 RID: 1093 RVA: 0x00004644 File Offset: 0x00002844
		public Guid Clsid
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Guid);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a string that contains the name of the codec.</summary>
		/// <returns>A string that contains the name of the codec.</returns>
		// Token: 0x17000132 RID: 306
		// (get) Token: 0x06000446 RID: 1094 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000447 RID: 1095 RVA: 0x00004644 File Offset: 0x00002844
		public string CodecName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets string that contains the path name of the DLL that holds the codec. If the codec is not in a DLL, this pointer is <see langword="null" />.</summary>
		/// <returns>A string that contains the path name of the DLL that holds the codec.</returns>
		// Token: 0x17000133 RID: 307
		// (get) Token: 0x06000448 RID: 1096 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000449 RID: 1097 RVA: 0x00004644 File Offset: 0x00002844
		public string DllName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets string that contains the file name extension(s) used in the codec. The extensions are separated by semicolons.</summary>
		/// <returns>A string that contains the file name extension(s) used in the codec.</returns>
		// Token: 0x17000134 RID: 308
		// (get) Token: 0x0600044A RID: 1098 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x0600044B RID: 1099 RVA: 0x00004644 File Offset: 0x00002844
		public string FilenameExtension
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets 32-bit value used to store additional information about the codec. This property returns a combination of flags from the <see cref="T:System.Drawing.Imaging.ImageCodecFlags" /> enumeration.</summary>
		/// <returns>A 32-bit value used to store additional information about the codec.</returns>
		// Token: 0x17000135 RID: 309
		// (get) Token: 0x0600044C RID: 1100 RVA: 0x00005780 File Offset: 0x00003980
		// (set) Token: 0x0600044D RID: 1101 RVA: 0x00004644 File Offset: 0x00002844
		public ImageCodecFlags Flags
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (ImageCodecFlags)0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a string that describes the codec's file format.</summary>
		/// <returns>A string that describes the codec's file format.</returns>
		// Token: 0x17000136 RID: 310
		// (get) Token: 0x0600044E RID: 1102 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x0600044F RID: 1103 RVA: 0x00004644 File Offset: 0x00002844
		public string FormatDescription
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Guid" /> structure that contains a GUID that identifies the codec's format.</summary>
		/// <returns>A <see cref="T:System.Guid" /> structure that contains a GUID that identifies the codec's format.</returns>
		// Token: 0x17000137 RID: 311
		// (get) Token: 0x06000450 RID: 1104 RVA: 0x0000579C File Offset: 0x0000399C
		// (set) Token: 0x06000451 RID: 1105 RVA: 0x00004644 File Offset: 0x00002844
		public Guid FormatID
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Guid);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a string that contains the codec's Multipurpose Internet Mail Extensions (MIME) type.</summary>
		/// <returns>A string that contains the codec's Multipurpose Internet Mail Extensions (MIME) type.</returns>
		// Token: 0x17000138 RID: 312
		// (get) Token: 0x06000452 RID: 1106 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000453 RID: 1107 RVA: 0x00004644 File Offset: 0x00002844
		public string MimeType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a two dimensional array of bytes that can be used as a filter.</summary>
		/// <returns>A two dimensional array of bytes that can be used as a filter.</returns>
		// Token: 0x17000139 RID: 313
		// (get) Token: 0x06000454 RID: 1108 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000455 RID: 1109 RVA: 0x00004644 File Offset: 0x00002844
		public byte[][] SignatureMasks
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a two dimensional array of bytes that represents the signature of the codec.</summary>
		/// <returns>A two dimensional array of bytes that represents the signature of the codec.</returns>
		// Token: 0x1700013A RID: 314
		// (get) Token: 0x06000456 RID: 1110 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000457 RID: 1111 RVA: 0x00004644 File Offset: 0x00002844
		public byte[][] SignaturePatterns
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the version number of the codec.</summary>
		/// <returns>The version number of the codec.</returns>
		// Token: 0x1700013B RID: 315
		// (get) Token: 0x06000458 RID: 1112 RVA: 0x000057B8 File Offset: 0x000039B8
		// (set) Token: 0x06000459 RID: 1113 RVA: 0x00004644 File Offset: 0x00002844
		public int Version
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Returns an array of <see cref="T:System.Drawing.Imaging.ImageCodecInfo" /> objects that contain information about the image decoders built into GDI+.</summary>
		/// <returns>An array of <see cref="T:System.Drawing.Imaging.ImageCodecInfo" /> objects. Each <see cref="T:System.Drawing.Imaging.ImageCodecInfo" /> object in the array contains information about one of the built-in image decoders.</returns>
		// Token: 0x0600045A RID: 1114 RVA: 0x00004667 File Offset: 0x00002867
		public static ImageCodecInfo[] GetImageDecoders()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns an array of <see cref="T:System.Drawing.Imaging.ImageCodecInfo" /> objects that contain information about the image encoders built into GDI+.</summary>
		/// <returns>An array of <see cref="T:System.Drawing.Imaging.ImageCodecInfo" /> objects. Each <see cref="T:System.Drawing.Imaging.ImageCodecInfo" /> object in the array contains information about one of the built-in image encoders.</returns>
		// Token: 0x0600045B RID: 1115 RVA: 0x00004667 File Offset: 0x00002867
		public static ImageCodecInfo[] GetImageEncoders()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
