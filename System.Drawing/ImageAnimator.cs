﻿using System;
using Unity;

namespace System.Drawing
{
	/// <summary>Animates an image that has time-based frames.</summary>
	// Token: 0x0200007C RID: 124
	public sealed class ImageAnimator
	{
		// Token: 0x06000682 RID: 1666 RVA: 0x00004644 File Offset: 0x00002844
		internal ImageAnimator()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Displays a multiple-frame image as an animation.</summary>
		/// <param name="image">The <see cref="T:System.Drawing.Image" /> object to animate. </param>
		/// <param name="onFrameChangedHandler">An <see langword="EventHandler" /> object that specifies the method that is called when the animation frame changes. </param>
		// Token: 0x06000683 RID: 1667 RVA: 0x00004644 File Offset: 0x00002844
		public static void Animate(Image image, EventHandler onFrameChangedHandler)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns a Boolean value indicating whether the specified image contains time-based frames.</summary>
		/// <param name="image">The <see cref="T:System.Drawing.Image" /> object to test. </param>
		/// <returns>This method returns <see langword="true" /> if the specified image contains time-based frames; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000684 RID: 1668 RVA: 0x00006254 File Offset: 0x00004454
		public static bool CanAnimate(Image image)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Terminates a running animation.</summary>
		/// <param name="image">The <see cref="T:System.Drawing.Image" /> object to stop animating. </param>
		/// <param name="onFrameChangedHandler">An <see langword="EventHandler" /> object that specifies the method that is called when the animation frame changes. </param>
		// Token: 0x06000685 RID: 1669 RVA: 0x00004644 File Offset: 0x00002844
		public static void StopAnimate(Image image, EventHandler onFrameChangedHandler)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Advances the frame in all images currently being animated. The new frame is drawn the next time the image is rendered.</summary>
		// Token: 0x06000686 RID: 1670 RVA: 0x00004644 File Offset: 0x00002844
		public static void UpdateFrames()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Advances the frame in the specified image. The new frame is drawn the next time the image is rendered. This method applies only to images with time-based frames.</summary>
		/// <param name="image">The <see cref="T:System.Drawing.Image" /> object for which to update frames. </param>
		// Token: 0x06000687 RID: 1671 RVA: 0x00004644 File Offset: 0x00002844
		public static void UpdateFrames(Image image)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
