﻿using System;
using System.Drawing.Text;
using Unity;

namespace System.Drawing
{
	/// <summary>Defines a group of type faces having a similar basic design and certain variations in styles. This class cannot be inherited.</summary>
	// Token: 0x0200001C RID: 28
	public sealed class FontFamily : MarshalByRefObject, IDisposable
	{
		/// <summary>Initializes a new <see cref="T:System.Drawing.FontFamily" /> from the specified generic font family.</summary>
		/// <param name="genericFamily">The <see cref="T:System.Drawing.Text.GenericFontFamilies" /> from which to create the new <see cref="T:System.Drawing.FontFamily" />. </param>
		// Token: 0x06000309 RID: 777 RVA: 0x00004644 File Offset: 0x00002844
		public FontFamily(GenericFontFamilies genericFamily)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.FontFamily" /> with the specified name.</summary>
		/// <param name="name">The name of the new <see cref="T:System.Drawing.FontFamily" />. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="name" /> is an empty string ("").-or-
		///         <paramref name="name" /> specifies a font that is not installed on the computer running the application.-or-
		///         <paramref name="name" /> specifies a font that is not a TrueType font.</exception>
		// Token: 0x0600030A RID: 778 RVA: 0x00004644 File Offset: 0x00002844
		public FontFamily(string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.FontFamily" /> in the specified <see cref="T:System.Drawing.Text.FontCollection" /> with the specified name.</summary>
		/// <param name="name">A <see cref="T:System.String" /> that represents the name of the new <see cref="T:System.Drawing.FontFamily" />. </param>
		/// <param name="fontCollection">The <see cref="T:System.Drawing.Text.FontCollection" /> that contains this <see cref="T:System.Drawing.FontFamily" />. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="name" /> is an empty string ("").-or-
		///         <paramref name="name" /> specifies a font that is not installed on the computer running the application.-or-
		///         <paramref name="name" /> specifies a font that is not a TrueType font.</exception>
		// Token: 0x0600030B RID: 779 RVA: 0x00004644 File Offset: 0x00002844
		public FontFamily(string name, FontCollection fontCollection)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns an array that contains all the <see cref="T:System.Drawing.FontFamily" /> objects associated with the current graphics context.</summary>
		/// <returns>An array of <see cref="T:System.Drawing.FontFamily" /> objects associated with the current graphics context.</returns>
		// Token: 0x170000D8 RID: 216
		// (get) Token: 0x0600030C RID: 780 RVA: 0x00004667 File Offset: 0x00002867
		public static FontFamily[] Families
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a generic monospace <see cref="T:System.Drawing.FontFamily" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.FontFamily" /> that represents a generic monospace font.</returns>
		// Token: 0x170000D9 RID: 217
		// (get) Token: 0x0600030D RID: 781 RVA: 0x00004667 File Offset: 0x00002867
		public static FontFamily GenericMonospace
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a generic sans serif <see cref="T:System.Drawing.FontFamily" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.FontFamily" /> object that represents a generic sans serif font.</returns>
		// Token: 0x170000DA RID: 218
		// (get) Token: 0x0600030E RID: 782 RVA: 0x00004667 File Offset: 0x00002867
		public static FontFamily GenericSansSerif
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a generic serif <see cref="T:System.Drawing.FontFamily" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.FontFamily" /> that represents a generic serif font.</returns>
		// Token: 0x170000DB RID: 219
		// (get) Token: 0x0600030F RID: 783 RVA: 0x00004667 File Offset: 0x00002867
		public static FontFamily GenericSerif
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the name of this <see cref="T:System.Drawing.FontFamily" />.</summary>
		/// <returns>A <see cref="T:System.String" /> that represents the name of this <see cref="T:System.Drawing.FontFamily" />.</returns>
		// Token: 0x170000DC RID: 220
		// (get) Token: 0x06000310 RID: 784 RVA: 0x00004667 File Offset: 0x00002867
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Releases all resources used by this <see cref="T:System.Drawing.FontFamily" />.</summary>
		// Token: 0x06000311 RID: 785 RVA: 0x00004644 File Offset: 0x00002844
		public void Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns the cell ascent, in design units, of the <see cref="T:System.Drawing.FontFamily" /> of the specified style.</summary>
		/// <param name="style">A <see cref="T:System.Drawing.FontStyle" /> that contains style information for the font. </param>
		/// <returns>The cell ascent for this <see cref="T:System.Drawing.FontFamily" /> that uses the specified <see cref="T:System.Drawing.FontStyle" />.</returns>
		// Token: 0x06000312 RID: 786 RVA: 0x00004FBC File Offset: 0x000031BC
		public int GetCellAscent(FontStyle style)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Returns the cell descent, in design units, of the <see cref="T:System.Drawing.FontFamily" /> of the specified style. </summary>
		/// <param name="style">A <see cref="T:System.Drawing.FontStyle" /> that contains style information for the font. </param>
		/// <returns>The cell descent metric for this <see cref="T:System.Drawing.FontFamily" /> that uses the specified <see cref="T:System.Drawing.FontStyle" />.</returns>
		// Token: 0x06000313 RID: 787 RVA: 0x00004FD8 File Offset: 0x000031D8
		public int GetCellDescent(FontStyle style)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Gets the height, in font design units, of the em square for the specified style.</summary>
		/// <param name="style">The <see cref="T:System.Drawing.FontStyle" /> for which to get the em height. </param>
		/// <returns>The height of the em square.</returns>
		// Token: 0x06000314 RID: 788 RVA: 0x00004FF4 File Offset: 0x000031F4
		public int GetEmHeight(FontStyle style)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Returns an array that contains all the <see cref="T:System.Drawing.FontFamily" /> objects available for the specified graphics context.</summary>
		/// <param name="graphics">The <see cref="T:System.Drawing.Graphics" /> object from which to return <see cref="T:System.Drawing.FontFamily" /> objects. </param>
		/// <returns>An array of <see cref="T:System.Drawing.FontFamily" /> objects available for the specified <see cref="T:System.Drawing.Graphics" /> object.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="graphics " /> is <see langword="null" />.</exception>
		// Token: 0x06000315 RID: 789 RVA: 0x00004667 File Offset: 0x00002867
		[Obsolete("Do not use method GetFamilies, use property Families instead")]
		public static FontFamily[] GetFamilies(Graphics graphics)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the line spacing, in design units, of the <see cref="T:System.Drawing.FontFamily" /> of the specified style. The line spacing is the vertical distance between the base lines of two consecutive lines of text. </summary>
		/// <param name="style">The <see cref="T:System.Drawing.FontStyle" /> to apply. </param>
		/// <returns>The distance between two consecutive lines of text.</returns>
		// Token: 0x06000316 RID: 790 RVA: 0x00005010 File Offset: 0x00003210
		public int GetLineSpacing(FontStyle style)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Returns the name, in the specified language, of this <see cref="T:System.Drawing.FontFamily" />.</summary>
		/// <param name="language">The language in which the name is returned. </param>
		/// <returns>A <see cref="T:System.String" /> that represents the name, in the specified language, of this <see cref="T:System.Drawing.FontFamily" />. </returns>
		// Token: 0x06000317 RID: 791 RVA: 0x00004667 File Offset: 0x00002867
		public string GetName(int language)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Indicates whether the specified <see cref="T:System.Drawing.FontStyle" /> enumeration is available.</summary>
		/// <param name="style">The <see cref="T:System.Drawing.FontStyle" /> to test. </param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.Drawing.FontStyle" /> is available; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000318 RID: 792 RVA: 0x0000502C File Offset: 0x0000322C
		public bool IsStyleAvailable(FontStyle style)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}
	}
}
