﻿using System;
using Unity;

namespace System.Drawing
{
	/// <summary>Pens for all the standard colors. This class cannot be inherited.</summary>
	// Token: 0x0200007D RID: 125
	public sealed class Pens
	{
		// Token: 0x06000688 RID: 1672 RVA: 0x00004644 File Offset: 0x00002844
		internal Pens()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700022C RID: 556
		// (get) Token: 0x06000689 RID: 1673 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen AliceBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700022D RID: 557
		// (get) Token: 0x0600068A RID: 1674 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen AntiqueWhite
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700022E RID: 558
		// (get) Token: 0x0600068B RID: 1675 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Aqua
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700022F RID: 559
		// (get) Token: 0x0600068C RID: 1676 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Aquamarine
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000230 RID: 560
		// (get) Token: 0x0600068D RID: 1677 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Azure
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000231 RID: 561
		// (get) Token: 0x0600068E RID: 1678 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Beige
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000232 RID: 562
		// (get) Token: 0x0600068F RID: 1679 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Bisque
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000233 RID: 563
		// (get) Token: 0x06000690 RID: 1680 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Black
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000234 RID: 564
		// (get) Token: 0x06000691 RID: 1681 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen BlanchedAlmond
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000235 RID: 565
		// (get) Token: 0x06000692 RID: 1682 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Blue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000236 RID: 566
		// (get) Token: 0x06000693 RID: 1683 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen BlueViolet
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000237 RID: 567
		// (get) Token: 0x06000694 RID: 1684 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Brown
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000238 RID: 568
		// (get) Token: 0x06000695 RID: 1685 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen BurlyWood
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000239 RID: 569
		// (get) Token: 0x06000696 RID: 1686 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen CadetBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700023A RID: 570
		// (get) Token: 0x06000697 RID: 1687 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Chartreuse
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700023B RID: 571
		// (get) Token: 0x06000698 RID: 1688 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Chocolate
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700023C RID: 572
		// (get) Token: 0x06000699 RID: 1689 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Coral
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700023D RID: 573
		// (get) Token: 0x0600069A RID: 1690 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen CornflowerBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700023E RID: 574
		// (get) Token: 0x0600069B RID: 1691 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Cornsilk
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700023F RID: 575
		// (get) Token: 0x0600069C RID: 1692 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Crimson
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000240 RID: 576
		// (get) Token: 0x0600069D RID: 1693 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Cyan
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000241 RID: 577
		// (get) Token: 0x0600069E RID: 1694 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen DarkBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000242 RID: 578
		// (get) Token: 0x0600069F RID: 1695 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen DarkCyan
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000243 RID: 579
		// (get) Token: 0x060006A0 RID: 1696 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen DarkGoldenrod
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000244 RID: 580
		// (get) Token: 0x060006A1 RID: 1697 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen DarkGray
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000245 RID: 581
		// (get) Token: 0x060006A2 RID: 1698 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen DarkGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000246 RID: 582
		// (get) Token: 0x060006A3 RID: 1699 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen DarkKhaki
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000247 RID: 583
		// (get) Token: 0x060006A4 RID: 1700 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen DarkMagenta
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000248 RID: 584
		// (get) Token: 0x060006A5 RID: 1701 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen DarkOliveGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000249 RID: 585
		// (get) Token: 0x060006A6 RID: 1702 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen DarkOrange
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700024A RID: 586
		// (get) Token: 0x060006A7 RID: 1703 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen DarkOrchid
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700024B RID: 587
		// (get) Token: 0x060006A8 RID: 1704 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen DarkRed
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700024C RID: 588
		// (get) Token: 0x060006A9 RID: 1705 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen DarkSalmon
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700024D RID: 589
		// (get) Token: 0x060006AA RID: 1706 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen DarkSeaGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700024E RID: 590
		// (get) Token: 0x060006AB RID: 1707 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen DarkSlateBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700024F RID: 591
		// (get) Token: 0x060006AC RID: 1708 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen DarkSlateGray
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000250 RID: 592
		// (get) Token: 0x060006AD RID: 1709 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen DarkTurquoise
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000251 RID: 593
		// (get) Token: 0x060006AE RID: 1710 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen DarkViolet
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000252 RID: 594
		// (get) Token: 0x060006AF RID: 1711 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen DeepPink
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000253 RID: 595
		// (get) Token: 0x060006B0 RID: 1712 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen DeepSkyBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000254 RID: 596
		// (get) Token: 0x060006B1 RID: 1713 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen DimGray
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000255 RID: 597
		// (get) Token: 0x060006B2 RID: 1714 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen DodgerBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000256 RID: 598
		// (get) Token: 0x060006B3 RID: 1715 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Firebrick
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000257 RID: 599
		// (get) Token: 0x060006B4 RID: 1716 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen FloralWhite
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000258 RID: 600
		// (get) Token: 0x060006B5 RID: 1717 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen ForestGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000259 RID: 601
		// (get) Token: 0x060006B6 RID: 1718 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Fuchsia
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700025A RID: 602
		// (get) Token: 0x060006B7 RID: 1719 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Gainsboro
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700025B RID: 603
		// (get) Token: 0x060006B8 RID: 1720 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen GhostWhite
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700025C RID: 604
		// (get) Token: 0x060006B9 RID: 1721 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Gold
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700025D RID: 605
		// (get) Token: 0x060006BA RID: 1722 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Goldenrod
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700025E RID: 606
		// (get) Token: 0x060006BB RID: 1723 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Gray
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700025F RID: 607
		// (get) Token: 0x060006BC RID: 1724 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Green
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000260 RID: 608
		// (get) Token: 0x060006BD RID: 1725 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen GreenYellow
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000261 RID: 609
		// (get) Token: 0x060006BE RID: 1726 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Honeydew
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000262 RID: 610
		// (get) Token: 0x060006BF RID: 1727 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen HotPink
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000263 RID: 611
		// (get) Token: 0x060006C0 RID: 1728 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen IndianRed
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000264 RID: 612
		// (get) Token: 0x060006C1 RID: 1729 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Indigo
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000265 RID: 613
		// (get) Token: 0x060006C2 RID: 1730 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Ivory
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000266 RID: 614
		// (get) Token: 0x060006C3 RID: 1731 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Khaki
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000267 RID: 615
		// (get) Token: 0x060006C4 RID: 1732 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Lavender
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000268 RID: 616
		// (get) Token: 0x060006C5 RID: 1733 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen LavenderBlush
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000269 RID: 617
		// (get) Token: 0x060006C6 RID: 1734 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen LawnGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700026A RID: 618
		// (get) Token: 0x060006C7 RID: 1735 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen LemonChiffon
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700026B RID: 619
		// (get) Token: 0x060006C8 RID: 1736 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen LightBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700026C RID: 620
		// (get) Token: 0x060006C9 RID: 1737 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen LightCoral
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700026D RID: 621
		// (get) Token: 0x060006CA RID: 1738 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen LightCyan
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700026E RID: 622
		// (get) Token: 0x060006CB RID: 1739 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen LightGoldenrodYellow
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700026F RID: 623
		// (get) Token: 0x060006CC RID: 1740 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen LightGray
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000270 RID: 624
		// (get) Token: 0x060006CD RID: 1741 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen LightGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000271 RID: 625
		// (get) Token: 0x060006CE RID: 1742 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen LightPink
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000272 RID: 626
		// (get) Token: 0x060006CF RID: 1743 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen LightSalmon
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000273 RID: 627
		// (get) Token: 0x060006D0 RID: 1744 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen LightSeaGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000274 RID: 628
		// (get) Token: 0x060006D1 RID: 1745 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen LightSkyBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000275 RID: 629
		// (get) Token: 0x060006D2 RID: 1746 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen LightSlateGray
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000276 RID: 630
		// (get) Token: 0x060006D3 RID: 1747 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen LightSteelBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000277 RID: 631
		// (get) Token: 0x060006D4 RID: 1748 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen LightYellow
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000278 RID: 632
		// (get) Token: 0x060006D5 RID: 1749 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Lime
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000279 RID: 633
		// (get) Token: 0x060006D6 RID: 1750 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen LimeGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700027A RID: 634
		// (get) Token: 0x060006D7 RID: 1751 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Linen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700027B RID: 635
		// (get) Token: 0x060006D8 RID: 1752 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Magenta
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700027C RID: 636
		// (get) Token: 0x060006D9 RID: 1753 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Maroon
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700027D RID: 637
		// (get) Token: 0x060006DA RID: 1754 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen MediumAquamarine
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700027E RID: 638
		// (get) Token: 0x060006DB RID: 1755 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen MediumBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700027F RID: 639
		// (get) Token: 0x060006DC RID: 1756 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen MediumOrchid
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000280 RID: 640
		// (get) Token: 0x060006DD RID: 1757 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen MediumPurple
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000281 RID: 641
		// (get) Token: 0x060006DE RID: 1758 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen MediumSeaGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000282 RID: 642
		// (get) Token: 0x060006DF RID: 1759 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen MediumSlateBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000283 RID: 643
		// (get) Token: 0x060006E0 RID: 1760 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen MediumSpringGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000284 RID: 644
		// (get) Token: 0x060006E1 RID: 1761 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen MediumTurquoise
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000285 RID: 645
		// (get) Token: 0x060006E2 RID: 1762 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen MediumVioletRed
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000286 RID: 646
		// (get) Token: 0x060006E3 RID: 1763 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen MidnightBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000287 RID: 647
		// (get) Token: 0x060006E4 RID: 1764 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen MintCream
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000288 RID: 648
		// (get) Token: 0x060006E5 RID: 1765 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen MistyRose
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000289 RID: 649
		// (get) Token: 0x060006E6 RID: 1766 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Moccasin
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700028A RID: 650
		// (get) Token: 0x060006E7 RID: 1767 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen NavajoWhite
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700028B RID: 651
		// (get) Token: 0x060006E8 RID: 1768 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Navy
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700028C RID: 652
		// (get) Token: 0x060006E9 RID: 1769 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen OldLace
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700028D RID: 653
		// (get) Token: 0x060006EA RID: 1770 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Olive
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700028E RID: 654
		// (get) Token: 0x060006EB RID: 1771 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen OliveDrab
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700028F RID: 655
		// (get) Token: 0x060006EC RID: 1772 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Orange
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000290 RID: 656
		// (get) Token: 0x060006ED RID: 1773 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen OrangeRed
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000291 RID: 657
		// (get) Token: 0x060006EE RID: 1774 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Orchid
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000292 RID: 658
		// (get) Token: 0x060006EF RID: 1775 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen PaleGoldenrod
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000293 RID: 659
		// (get) Token: 0x060006F0 RID: 1776 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen PaleGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000294 RID: 660
		// (get) Token: 0x060006F1 RID: 1777 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen PaleTurquoise
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000295 RID: 661
		// (get) Token: 0x060006F2 RID: 1778 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen PaleVioletRed
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000296 RID: 662
		// (get) Token: 0x060006F3 RID: 1779 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen PapayaWhip
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000297 RID: 663
		// (get) Token: 0x060006F4 RID: 1780 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen PeachPuff
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000298 RID: 664
		// (get) Token: 0x060006F5 RID: 1781 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Peru
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x17000299 RID: 665
		// (get) Token: 0x060006F6 RID: 1782 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Pink
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700029A RID: 666
		// (get) Token: 0x060006F7 RID: 1783 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Plum
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700029B RID: 667
		// (get) Token: 0x060006F8 RID: 1784 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen PowderBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700029C RID: 668
		// (get) Token: 0x060006F9 RID: 1785 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Purple
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700029D RID: 669
		// (get) Token: 0x060006FA RID: 1786 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Red
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700029E RID: 670
		// (get) Token: 0x060006FB RID: 1787 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen RosyBrown
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x1700029F RID: 671
		// (get) Token: 0x060006FC RID: 1788 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen RoyalBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002A0 RID: 672
		// (get) Token: 0x060006FD RID: 1789 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen SaddleBrown
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002A1 RID: 673
		// (get) Token: 0x060006FE RID: 1790 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Salmon
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002A2 RID: 674
		// (get) Token: 0x060006FF RID: 1791 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen SandyBrown
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002A3 RID: 675
		// (get) Token: 0x06000700 RID: 1792 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen SeaGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002A4 RID: 676
		// (get) Token: 0x06000701 RID: 1793 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen SeaShell
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002A5 RID: 677
		// (get) Token: 0x06000702 RID: 1794 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Sienna
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002A6 RID: 678
		// (get) Token: 0x06000703 RID: 1795 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Silver
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002A7 RID: 679
		// (get) Token: 0x06000704 RID: 1796 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen SkyBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002A8 RID: 680
		// (get) Token: 0x06000705 RID: 1797 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen SlateBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002A9 RID: 681
		// (get) Token: 0x06000706 RID: 1798 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen SlateGray
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002AA RID: 682
		// (get) Token: 0x06000707 RID: 1799 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Snow
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002AB RID: 683
		// (get) Token: 0x06000708 RID: 1800 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen SpringGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002AC RID: 684
		// (get) Token: 0x06000709 RID: 1801 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen SteelBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002AD RID: 685
		// (get) Token: 0x0600070A RID: 1802 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Tan
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002AE RID: 686
		// (get) Token: 0x0600070B RID: 1803 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Teal
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002AF RID: 687
		// (get) Token: 0x0600070C RID: 1804 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Thistle
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002B0 RID: 688
		// (get) Token: 0x0600070D RID: 1805 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Tomato
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002B1 RID: 689
		// (get) Token: 0x0600070E RID: 1806 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Transparent
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002B2 RID: 690
		// (get) Token: 0x0600070F RID: 1807 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Turquoise
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002B3 RID: 691
		// (get) Token: 0x06000710 RID: 1808 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Violet
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002B4 RID: 692
		// (get) Token: 0x06000711 RID: 1809 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Wheat
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002B5 RID: 693
		// (get) Token: 0x06000712 RID: 1810 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen White
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002B6 RID: 694
		// (get) Token: 0x06000713 RID: 1811 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen WhiteSmoke
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002B7 RID: 695
		// (get) Token: 0x06000714 RID: 1812 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen Yellow
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>A system-defined <see cref="T:System.Drawing.Pen" /> object with a width of 1.</summary>
		/// <returns>A <see cref="T:System.Drawing.Pen" /> object set to a system-defined color.</returns>
		// Token: 0x170002B8 RID: 696
		// (get) Token: 0x06000715 RID: 1813 RVA: 0x00004667 File Offset: 0x00002867
		public static Pen YellowGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
