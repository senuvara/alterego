﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Drawing
{
	/// <summary>Converts a <see cref="T:System.Drawing.Point" /> object from one data type to another. </summary>
	// Token: 0x0200007E RID: 126
	public class PointConverter : TypeConverter
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.PointConverter" /> class.</summary>
		// Token: 0x06000716 RID: 1814 RVA: 0x00004644 File Offset: 0x00002844
		public PointConverter()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
