﻿using System;
using Unity;

namespace System.Drawing
{
	/// <summary>Brushes for all the standard colors. This class cannot be inherited.</summary>
	// Token: 0x02000076 RID: 118
	public sealed class Brushes
	{
		// Token: 0x060005DD RID: 1501 RVA: 0x00004644 File Offset: 0x00002844
		internal Brushes()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x1700019C RID: 412
		// (get) Token: 0x060005DE RID: 1502 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush AliceBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x1700019D RID: 413
		// (get) Token: 0x060005DF RID: 1503 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush AntiqueWhite
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x1700019E RID: 414
		// (get) Token: 0x060005E0 RID: 1504 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Aqua
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x1700019F RID: 415
		// (get) Token: 0x060005E1 RID: 1505 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Aquamarine
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001A0 RID: 416
		// (get) Token: 0x060005E2 RID: 1506 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Azure
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001A1 RID: 417
		// (get) Token: 0x060005E3 RID: 1507 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Beige
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001A2 RID: 418
		// (get) Token: 0x060005E4 RID: 1508 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Bisque
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001A3 RID: 419
		// (get) Token: 0x060005E5 RID: 1509 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Black
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001A4 RID: 420
		// (get) Token: 0x060005E6 RID: 1510 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush BlanchedAlmond
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001A5 RID: 421
		// (get) Token: 0x060005E7 RID: 1511 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Blue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001A6 RID: 422
		// (get) Token: 0x060005E8 RID: 1512 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush BlueViolet
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001A7 RID: 423
		// (get) Token: 0x060005E9 RID: 1513 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Brown
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001A8 RID: 424
		// (get) Token: 0x060005EA RID: 1514 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush BurlyWood
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001A9 RID: 425
		// (get) Token: 0x060005EB RID: 1515 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush CadetBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001AA RID: 426
		// (get) Token: 0x060005EC RID: 1516 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Chartreuse
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001AB RID: 427
		// (get) Token: 0x060005ED RID: 1517 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Chocolate
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001AC RID: 428
		// (get) Token: 0x060005EE RID: 1518 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Coral
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001AD RID: 429
		// (get) Token: 0x060005EF RID: 1519 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush CornflowerBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001AE RID: 430
		// (get) Token: 0x060005F0 RID: 1520 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Cornsilk
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001AF RID: 431
		// (get) Token: 0x060005F1 RID: 1521 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Crimson
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001B0 RID: 432
		// (get) Token: 0x060005F2 RID: 1522 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Cyan
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001B1 RID: 433
		// (get) Token: 0x060005F3 RID: 1523 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush DarkBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001B2 RID: 434
		// (get) Token: 0x060005F4 RID: 1524 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush DarkCyan
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001B3 RID: 435
		// (get) Token: 0x060005F5 RID: 1525 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush DarkGoldenrod
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001B4 RID: 436
		// (get) Token: 0x060005F6 RID: 1526 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush DarkGray
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001B5 RID: 437
		// (get) Token: 0x060005F7 RID: 1527 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush DarkGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001B6 RID: 438
		// (get) Token: 0x060005F8 RID: 1528 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush DarkKhaki
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001B7 RID: 439
		// (get) Token: 0x060005F9 RID: 1529 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush DarkMagenta
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001B8 RID: 440
		// (get) Token: 0x060005FA RID: 1530 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush DarkOliveGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001B9 RID: 441
		// (get) Token: 0x060005FB RID: 1531 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush DarkOrange
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001BA RID: 442
		// (get) Token: 0x060005FC RID: 1532 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush DarkOrchid
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001BB RID: 443
		// (get) Token: 0x060005FD RID: 1533 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush DarkRed
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001BC RID: 444
		// (get) Token: 0x060005FE RID: 1534 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush DarkSalmon
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001BD RID: 445
		// (get) Token: 0x060005FF RID: 1535 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush DarkSeaGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001BE RID: 446
		// (get) Token: 0x06000600 RID: 1536 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush DarkSlateBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001BF RID: 447
		// (get) Token: 0x06000601 RID: 1537 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush DarkSlateGray
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001C0 RID: 448
		// (get) Token: 0x06000602 RID: 1538 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush DarkTurquoise
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001C1 RID: 449
		// (get) Token: 0x06000603 RID: 1539 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush DarkViolet
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001C2 RID: 450
		// (get) Token: 0x06000604 RID: 1540 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush DeepPink
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001C3 RID: 451
		// (get) Token: 0x06000605 RID: 1541 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush DeepSkyBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001C4 RID: 452
		// (get) Token: 0x06000606 RID: 1542 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush DimGray
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001C5 RID: 453
		// (get) Token: 0x06000607 RID: 1543 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush DodgerBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001C6 RID: 454
		// (get) Token: 0x06000608 RID: 1544 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Firebrick
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001C7 RID: 455
		// (get) Token: 0x06000609 RID: 1545 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush FloralWhite
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001C8 RID: 456
		// (get) Token: 0x0600060A RID: 1546 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush ForestGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001C9 RID: 457
		// (get) Token: 0x0600060B RID: 1547 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Fuchsia
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001CA RID: 458
		// (get) Token: 0x0600060C RID: 1548 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Gainsboro
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001CB RID: 459
		// (get) Token: 0x0600060D RID: 1549 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush GhostWhite
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001CC RID: 460
		// (get) Token: 0x0600060E RID: 1550 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Gold
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001CD RID: 461
		// (get) Token: 0x0600060F RID: 1551 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Goldenrod
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001CE RID: 462
		// (get) Token: 0x06000610 RID: 1552 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Gray
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001CF RID: 463
		// (get) Token: 0x06000611 RID: 1553 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Green
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001D0 RID: 464
		// (get) Token: 0x06000612 RID: 1554 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush GreenYellow
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001D1 RID: 465
		// (get) Token: 0x06000613 RID: 1555 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Honeydew
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001D2 RID: 466
		// (get) Token: 0x06000614 RID: 1556 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush HotPink
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001D3 RID: 467
		// (get) Token: 0x06000615 RID: 1557 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush IndianRed
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001D4 RID: 468
		// (get) Token: 0x06000616 RID: 1558 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Indigo
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001D5 RID: 469
		// (get) Token: 0x06000617 RID: 1559 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Ivory
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001D6 RID: 470
		// (get) Token: 0x06000618 RID: 1560 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Khaki
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001D7 RID: 471
		// (get) Token: 0x06000619 RID: 1561 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Lavender
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001D8 RID: 472
		// (get) Token: 0x0600061A RID: 1562 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush LavenderBlush
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001D9 RID: 473
		// (get) Token: 0x0600061B RID: 1563 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush LawnGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001DA RID: 474
		// (get) Token: 0x0600061C RID: 1564 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush LemonChiffon
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001DB RID: 475
		// (get) Token: 0x0600061D RID: 1565 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush LightBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001DC RID: 476
		// (get) Token: 0x0600061E RID: 1566 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush LightCoral
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001DD RID: 477
		// (get) Token: 0x0600061F RID: 1567 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush LightCyan
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001DE RID: 478
		// (get) Token: 0x06000620 RID: 1568 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush LightGoldenrodYellow
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001DF RID: 479
		// (get) Token: 0x06000621 RID: 1569 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush LightGray
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001E0 RID: 480
		// (get) Token: 0x06000622 RID: 1570 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush LightGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001E1 RID: 481
		// (get) Token: 0x06000623 RID: 1571 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush LightPink
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001E2 RID: 482
		// (get) Token: 0x06000624 RID: 1572 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush LightSalmon
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001E3 RID: 483
		// (get) Token: 0x06000625 RID: 1573 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush LightSeaGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001E4 RID: 484
		// (get) Token: 0x06000626 RID: 1574 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush LightSkyBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001E5 RID: 485
		// (get) Token: 0x06000627 RID: 1575 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush LightSlateGray
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001E6 RID: 486
		// (get) Token: 0x06000628 RID: 1576 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush LightSteelBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001E7 RID: 487
		// (get) Token: 0x06000629 RID: 1577 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush LightYellow
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001E8 RID: 488
		// (get) Token: 0x0600062A RID: 1578 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Lime
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001E9 RID: 489
		// (get) Token: 0x0600062B RID: 1579 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush LimeGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001EA RID: 490
		// (get) Token: 0x0600062C RID: 1580 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Linen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001EB RID: 491
		// (get) Token: 0x0600062D RID: 1581 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Magenta
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001EC RID: 492
		// (get) Token: 0x0600062E RID: 1582 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Maroon
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001ED RID: 493
		// (get) Token: 0x0600062F RID: 1583 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush MediumAquamarine
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001EE RID: 494
		// (get) Token: 0x06000630 RID: 1584 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush MediumBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001EF RID: 495
		// (get) Token: 0x06000631 RID: 1585 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush MediumOrchid
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001F0 RID: 496
		// (get) Token: 0x06000632 RID: 1586 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush MediumPurple
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001F1 RID: 497
		// (get) Token: 0x06000633 RID: 1587 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush MediumSeaGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001F2 RID: 498
		// (get) Token: 0x06000634 RID: 1588 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush MediumSlateBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001F3 RID: 499
		// (get) Token: 0x06000635 RID: 1589 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush MediumSpringGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001F4 RID: 500
		// (get) Token: 0x06000636 RID: 1590 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush MediumTurquoise
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001F5 RID: 501
		// (get) Token: 0x06000637 RID: 1591 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush MediumVioletRed
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001F6 RID: 502
		// (get) Token: 0x06000638 RID: 1592 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush MidnightBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001F7 RID: 503
		// (get) Token: 0x06000639 RID: 1593 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush MintCream
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001F8 RID: 504
		// (get) Token: 0x0600063A RID: 1594 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush MistyRose
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001F9 RID: 505
		// (get) Token: 0x0600063B RID: 1595 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Moccasin
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001FA RID: 506
		// (get) Token: 0x0600063C RID: 1596 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush NavajoWhite
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001FB RID: 507
		// (get) Token: 0x0600063D RID: 1597 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Navy
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001FC RID: 508
		// (get) Token: 0x0600063E RID: 1598 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush OldLace
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001FD RID: 509
		// (get) Token: 0x0600063F RID: 1599 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Olive
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001FE RID: 510
		// (get) Token: 0x06000640 RID: 1600 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush OliveDrab
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x170001FF RID: 511
		// (get) Token: 0x06000641 RID: 1601 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Orange
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000200 RID: 512
		// (get) Token: 0x06000642 RID: 1602 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush OrangeRed
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000201 RID: 513
		// (get) Token: 0x06000643 RID: 1603 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Orchid
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000202 RID: 514
		// (get) Token: 0x06000644 RID: 1604 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush PaleGoldenrod
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000203 RID: 515
		// (get) Token: 0x06000645 RID: 1605 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush PaleGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000204 RID: 516
		// (get) Token: 0x06000646 RID: 1606 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush PaleTurquoise
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000205 RID: 517
		// (get) Token: 0x06000647 RID: 1607 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush PaleVioletRed
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000206 RID: 518
		// (get) Token: 0x06000648 RID: 1608 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush PapayaWhip
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000207 RID: 519
		// (get) Token: 0x06000649 RID: 1609 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush PeachPuff
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000208 RID: 520
		// (get) Token: 0x0600064A RID: 1610 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Peru
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000209 RID: 521
		// (get) Token: 0x0600064B RID: 1611 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Pink
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x1700020A RID: 522
		// (get) Token: 0x0600064C RID: 1612 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Plum
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x1700020B RID: 523
		// (get) Token: 0x0600064D RID: 1613 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush PowderBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x1700020C RID: 524
		// (get) Token: 0x0600064E RID: 1614 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Purple
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x1700020D RID: 525
		// (get) Token: 0x0600064F RID: 1615 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Red
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x1700020E RID: 526
		// (get) Token: 0x06000650 RID: 1616 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush RosyBrown
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x1700020F RID: 527
		// (get) Token: 0x06000651 RID: 1617 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush RoyalBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000210 RID: 528
		// (get) Token: 0x06000652 RID: 1618 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush SaddleBrown
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000211 RID: 529
		// (get) Token: 0x06000653 RID: 1619 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Salmon
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000212 RID: 530
		// (get) Token: 0x06000654 RID: 1620 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush SandyBrown
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000213 RID: 531
		// (get) Token: 0x06000655 RID: 1621 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush SeaGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000214 RID: 532
		// (get) Token: 0x06000656 RID: 1622 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush SeaShell
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000215 RID: 533
		// (get) Token: 0x06000657 RID: 1623 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Sienna
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000216 RID: 534
		// (get) Token: 0x06000658 RID: 1624 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Silver
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000217 RID: 535
		// (get) Token: 0x06000659 RID: 1625 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush SkyBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000218 RID: 536
		// (get) Token: 0x0600065A RID: 1626 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush SlateBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000219 RID: 537
		// (get) Token: 0x0600065B RID: 1627 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush SlateGray
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x1700021A RID: 538
		// (get) Token: 0x0600065C RID: 1628 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Snow
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x1700021B RID: 539
		// (get) Token: 0x0600065D RID: 1629 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush SpringGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x1700021C RID: 540
		// (get) Token: 0x0600065E RID: 1630 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush SteelBlue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x1700021D RID: 541
		// (get) Token: 0x0600065F RID: 1631 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Tan
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x1700021E RID: 542
		// (get) Token: 0x06000660 RID: 1632 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Teal
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x1700021F RID: 543
		// (get) Token: 0x06000661 RID: 1633 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Thistle
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000220 RID: 544
		// (get) Token: 0x06000662 RID: 1634 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Tomato
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000221 RID: 545
		// (get) Token: 0x06000663 RID: 1635 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Transparent
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000222 RID: 546
		// (get) Token: 0x06000664 RID: 1636 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Turquoise
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000223 RID: 547
		// (get) Token: 0x06000665 RID: 1637 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Violet
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000224 RID: 548
		// (get) Token: 0x06000666 RID: 1638 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Wheat
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000225 RID: 549
		// (get) Token: 0x06000667 RID: 1639 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush White
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000226 RID: 550
		// (get) Token: 0x06000668 RID: 1640 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush WhiteSmoke
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000227 RID: 551
		// (get) Token: 0x06000669 RID: 1641 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Yellow
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a system-defined <see cref="T:System.Drawing.Brush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Brush" /> object set to a system-defined color.</returns>
		// Token: 0x17000228 RID: 552
		// (get) Token: 0x0600066A RID: 1642 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush YellowGreen
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
