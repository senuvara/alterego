﻿using System;
using Unity;

namespace System.Drawing
{
	/// <summary>Each property of the <see cref="T:System.Drawing.SystemBrushes" /> class is a <see cref="T:System.Drawing.SolidBrush" /> that is the color of a Windows display element.</summary>
	// Token: 0x02000084 RID: 132
	public sealed class SystemBrushes
	{
		// Token: 0x0600071F RID: 1823 RVA: 0x00004644 File Offset: 0x00002844
		internal SystemBrushes()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the color of the active window's border.</summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the color of the active window's border.</returns>
		// Token: 0x170002BA RID: 698
		// (get) Token: 0x06000720 RID: 1824 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush ActiveBorder
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the color of the background of the active window's title bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the color of the background of the active window's title bar.</returns>
		// Token: 0x170002BB RID: 699
		// (get) Token: 0x06000721 RID: 1825 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush ActiveCaption
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the color of the text in the active window's title bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the color of the background of the active window's title bar.</returns>
		// Token: 0x170002BC RID: 700
		// (get) Token: 0x06000722 RID: 1826 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush ActiveCaptionText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the color of the application workspace. </summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the color of the application workspace.</returns>
		// Token: 0x170002BD RID: 701
		// (get) Token: 0x06000723 RID: 1827 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush AppWorkspace
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the face color of a 3-D element.</summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the face color of a 3-D element.</returns>
		// Token: 0x170002BE RID: 702
		// (get) Token: 0x06000724 RID: 1828 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush ButtonFace
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the highlight color of a 3-D element. </summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the highlight color of a 3-D element.</returns>
		// Token: 0x170002BF RID: 703
		// (get) Token: 0x06000725 RID: 1829 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush ButtonHighlight
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the shadow color of a 3-D element. </summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the shadow color of a 3-D element.</returns>
		// Token: 0x170002C0 RID: 704
		// (get) Token: 0x06000726 RID: 1830 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush ButtonShadow
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the face color of a 3-D element.</summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the face color of a 3-D element.</returns>
		// Token: 0x170002C1 RID: 705
		// (get) Token: 0x06000727 RID: 1831 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Control
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the shadow color of a 3-D element. </summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the shadow color of a 3-D element.</returns>
		// Token: 0x170002C2 RID: 706
		// (get) Token: 0x06000728 RID: 1832 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush ControlDark
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the dark shadow color of a 3-D element. </summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the dark shadow color of a 3-D element.</returns>
		// Token: 0x170002C3 RID: 707
		// (get) Token: 0x06000729 RID: 1833 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush ControlDarkDark
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the light color of a 3-D element. </summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the light color of a 3-D element.</returns>
		// Token: 0x170002C4 RID: 708
		// (get) Token: 0x0600072A RID: 1834 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush ControlLight
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the highlight color of a 3-D element. </summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the highlight color of a 3-D element.</returns>
		// Token: 0x170002C5 RID: 709
		// (get) Token: 0x0600072B RID: 1835 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush ControlLightLight
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the color of text in a 3-D element.</summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the color of text in a 3-D element.</returns>
		// Token: 0x170002C6 RID: 710
		// (get) Token: 0x0600072C RID: 1836 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush ControlText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the color of the desktop.</summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the color of the desktop.</returns>
		// Token: 0x170002C7 RID: 711
		// (get) Token: 0x0600072D RID: 1837 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Desktop
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the lightest color in the color gradient of an active window's title bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the lightest color in the color gradient of an active window's title bar.</returns>
		// Token: 0x170002C8 RID: 712
		// (get) Token: 0x0600072E RID: 1838 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush GradientActiveCaption
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the lightest color in the color gradient of an inactive window's title bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the lightest color in the color gradient of an inactive window's title bar.</returns>
		// Token: 0x170002C9 RID: 713
		// (get) Token: 0x0600072F RID: 1839 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush GradientInactiveCaption
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the color of dimmed text.</summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the color of dimmed text.</returns>
		// Token: 0x170002CA RID: 714
		// (get) Token: 0x06000730 RID: 1840 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush GrayText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the color of the background of selected items. </summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the color of the background of selected items.</returns>
		// Token: 0x170002CB RID: 715
		// (get) Token: 0x06000731 RID: 1841 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Highlight
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the color of the text of selected items. </summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the color of the text of selected items.</returns>
		// Token: 0x170002CC RID: 716
		// (get) Token: 0x06000732 RID: 1842 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush HighlightText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the color used to designate a hot-tracked item. </summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the color used to designate a hot-tracked item.</returns>
		// Token: 0x170002CD RID: 717
		// (get) Token: 0x06000733 RID: 1843 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush HotTrack
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the color of an inactive window's border.</summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the color of an inactive window's border.</returns>
		// Token: 0x170002CE RID: 718
		// (get) Token: 0x06000734 RID: 1844 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush InactiveBorder
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the color of the background of an inactive window's title bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the color of the background of an inactive window's title bar.</returns>
		// Token: 0x170002CF RID: 719
		// (get) Token: 0x06000735 RID: 1845 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush InactiveCaption
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the color of the text in an inactive window's title bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the color of the text in an inactive window's title bar.</returns>
		// Token: 0x170002D0 RID: 720
		// (get) Token: 0x06000736 RID: 1846 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush InactiveCaptionText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the color of the background of a ToolTip.</summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the color of the background of a ToolTip.</returns>
		// Token: 0x170002D1 RID: 721
		// (get) Token: 0x06000737 RID: 1847 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Info
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the color of the text of a ToolTip.</summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> is the color of the text of a ToolTip.</returns>
		// Token: 0x170002D2 RID: 722
		// (get) Token: 0x06000738 RID: 1848 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush InfoText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the color of a menu's background.</summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the color of a menu's background.</returns>
		// Token: 0x170002D3 RID: 723
		// (get) Token: 0x06000739 RID: 1849 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Menu
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the color of the background of a menu bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the color of the background of a menu bar.</returns>
		// Token: 0x170002D4 RID: 724
		// (get) Token: 0x0600073A RID: 1850 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush MenuBar
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the color used to highlight menu items when the menu appears as a flat menu.</summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the color used to highlight menu items when the menu appears as a flat menu.</returns>
		// Token: 0x170002D5 RID: 725
		// (get) Token: 0x0600073B RID: 1851 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush MenuHighlight
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the color of a menu's text.</summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the color of a menu's text.</returns>
		// Token: 0x170002D6 RID: 726
		// (get) Token: 0x0600073C RID: 1852 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush MenuText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the color of the background of a scroll bar.</summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the color of the background of a scroll bar.</returns>
		// Token: 0x170002D7 RID: 727
		// (get) Token: 0x0600073D RID: 1853 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush ScrollBar
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the color of the background in the client area of a window.</summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the color of the background in the client area of a window.</returns>
		// Token: 0x170002D8 RID: 728
		// (get) Token: 0x0600073E RID: 1854 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush Window
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the color of a window frame.</summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the color of a window frame.</returns>
		// Token: 0x170002D9 RID: 729
		// (get) Token: 0x0600073F RID: 1855 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush WindowFrame
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Drawing.SolidBrush" /> that is the color of the text in the client area of a window.</summary>
		/// <returns>A <see cref="T:System.Drawing.SolidBrush" /> that is the color of the text in the client area of a window.</returns>
		// Token: 0x170002DA RID: 730
		// (get) Token: 0x06000740 RID: 1856 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush WindowText
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Creates a <see cref="T:System.Drawing.Brush" /> from the specified <see cref="T:System.Drawing.Color" /> structure.</summary>
		/// <param name="c">The <see cref="T:System.Drawing.Color" /> structure from which to create the <see cref="T:System.Drawing.Brush" />. </param>
		/// <returns>The <see cref="T:System.Drawing.Brush" /> this method creates.</returns>
		// Token: 0x06000741 RID: 1857 RVA: 0x00004667 File Offset: 0x00002867
		public static Brush FromSystemColor(Color c)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
