﻿using System;
using Unity;

namespace System.Drawing.Design
{
	/// <summary>Represents the method that handles the <see cref="E:System.Drawing.Design.ToolboxItem.ComponentsCreating" /> event.</summary>
	/// <param name="sender">The source of the event. </param>
	/// <param name="e">A <see cref="T:System.Drawing.Design.ToolboxComponentsCreatingEventArgs" /> that provides data for the event. </param>
	// Token: 0x0200006E RID: 110
	public sealed class ToolboxComponentsCreatingEventHandler : MulticastDelegate
	{
		// Token: 0x060005A6 RID: 1446 RVA: 0x00004644 File Offset: 0x00002844
		public ToolboxComponentsCreatingEventHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060005A7 RID: 1447 RVA: 0x00004644 File Offset: 0x00002844
		public void Invoke(object sender, ToolboxComponentsCreatingEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060005A8 RID: 1448 RVA: 0x00004667 File Offset: 0x00002867
		public IAsyncResult BeginInvoke(object sender, ToolboxComponentsCreatingEventArgs e, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060005A9 RID: 1449 RVA: 0x00004644 File Offset: 0x00002844
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
