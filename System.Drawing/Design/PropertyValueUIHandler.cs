﻿using System;
using System.Collections;
using System.ComponentModel;
using Unity;

namespace System.Drawing.Design
{
	/// <summary>Represents the method that adds a delegate to an implementation of <see cref="T:System.Drawing.Design.IPropertyValueUIService" />.</summary>
	/// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that can be used to obtain context information. </param>
	/// <param name="propDesc">A <see cref="T:System.ComponentModel.PropertyDescriptor" /> that represents the property being queried. </param>
	/// <param name="valueUIItemList">An <see cref="T:System.Collections.ArrayList" /> of <see cref="T:System.Drawing.Design.PropertyValueUIItem" /> objects containing the UI items associated with the property. </param>
	// Token: 0x020000BF RID: 191
	public sealed class PropertyValueUIHandler : MulticastDelegate
	{
		// Token: 0x06000926 RID: 2342 RVA: 0x00004644 File Offset: 0x00002844
		public PropertyValueUIHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06000927 RID: 2343 RVA: 0x00004644 File Offset: 0x00002844
		public void Invoke(ITypeDescriptorContext context, PropertyDescriptor propDesc, ArrayList valueUIItemList)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06000928 RID: 2344 RVA: 0x00004667 File Offset: 0x00002867
		public IAsyncResult BeginInvoke(ITypeDescriptorContext context, PropertyDescriptor propDesc, ArrayList valueUIItemList, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06000929 RID: 2345 RVA: 0x00004644 File Offset: 0x00002844
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
