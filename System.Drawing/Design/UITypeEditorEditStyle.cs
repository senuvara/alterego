﻿using System;

namespace System.Drawing.Design
{
	/// <summary>Specifies identifiers that indicate the value editing style of a <see cref="T:System.Drawing.Design.UITypeEditor" />.</summary>
	// Token: 0x02000011 RID: 17
	public enum UITypeEditorEditStyle
	{
		/// <summary>Displays a drop-down arrow button and hosts the user interface (UI) in a drop-down dialog box.</summary>
		// Token: 0x040000FD RID: 253
		DropDown = 3,
		/// <summary>Displays an ellipsis (...) button to start a modal dialog box, which requires user input before continuing a program, or a modeless dialog box, which stays on the screen and is available for use at any time but permits other user activities.</summary>
		// Token: 0x040000FE RID: 254
		Modal = 2,
		/// <summary>Provides no interactive user interface (UI) component.</summary>
		// Token: 0x040000FF RID: 255
		None = 1
	}
}
