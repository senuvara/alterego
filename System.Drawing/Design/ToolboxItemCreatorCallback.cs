﻿using System;
using Unity;

namespace System.Drawing.Design
{
	/// <summary>Provides a callback mechanism that can create a <see cref="T:System.Drawing.Design.ToolboxItem" />.</summary>
	/// <param name="serializedObject">The object which contains the data to create a <see cref="T:System.Drawing.Design.ToolboxItem" /> for. </param>
	/// <param name="format">The name of the clipboard data format to create a <see cref="T:System.Drawing.Design.ToolboxItem" /> for. </param>
	/// <returns>The deserialized <see cref="T:System.Drawing.Design.ToolboxItem" /> object specified by <paramref name="serializedObject" />.</returns>
	// Token: 0x02000073 RID: 115
	public sealed class ToolboxItemCreatorCallback : MulticastDelegate
	{
		// Token: 0x060005D2 RID: 1490 RVA: 0x00004644 File Offset: 0x00002844
		public ToolboxItemCreatorCallback(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060005D3 RID: 1491 RVA: 0x00004667 File Offset: 0x00002867
		public ToolboxItem Invoke(object serializedObject, string format)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060005D4 RID: 1492 RVA: 0x00004667 File Offset: 0x00002867
		public IAsyncResult BeginInvoke(object serializedObject, string format, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060005D5 RID: 1493 RVA: 0x00004667 File Offset: 0x00002867
		public ToolboxItem EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
