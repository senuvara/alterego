﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Drawing.Design
{
	/// <summary>Represents the method that will handle the <see cref="P:System.Drawing.Design.PropertyValueUIItem.InvokeHandler" /> event of a <see cref="T:System.Drawing.Design.PropertyValueUIItem" />.</summary>
	/// <param name="context">The <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> for the property associated with the icon that was double-clicked. </param>
	/// <param name="descriptor">The property associated with the icon that was double-clicked. </param>
	/// <param name="invokedItem">The <see cref="T:System.Drawing.Design.PropertyValueUIItem" /> associated with the icon that was double-clicked. </param>
	// Token: 0x020000C1 RID: 193
	public sealed class PropertyValueUIItemInvokeHandler : MulticastDelegate
	{
		// Token: 0x0600092F RID: 2351 RVA: 0x00004644 File Offset: 0x00002844
		public PropertyValueUIItemInvokeHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06000930 RID: 2352 RVA: 0x00004644 File Offset: 0x00002844
		public void Invoke(ITypeDescriptorContext context, PropertyDescriptor descriptor, PropertyValueUIItem invokedItem)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06000931 RID: 2353 RVA: 0x00004667 File Offset: 0x00002867
		public IAsyncResult BeginInvoke(ITypeDescriptorContext context, PropertyDescriptor descriptor, PropertyValueUIItem invokedItem, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06000932 RID: 2354 RVA: 0x00004644 File Offset: 0x00002844
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
