﻿using System;
using System.Collections;
using System.Security.Permissions;
using Unity;

namespace System.Drawing.Design
{
	/// <summary>Represents a collection of category name strings.</summary>
	// Token: 0x02000071 RID: 113
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public sealed class CategoryNameCollection : ReadOnlyCollectionBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Design.CategoryNameCollection" /> class using the specified collection.</summary>
		/// <param name="value">A <see cref="T:System.Drawing.Design.CategoryNameCollection" /> that contains the names to initialize the collection values to. </param>
		// Token: 0x060005AE RID: 1454 RVA: 0x00004644 File Offset: 0x00002844
		public CategoryNameCollection(CategoryNameCollection value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Design.CategoryNameCollection" /> class using the specified array of names.</summary>
		/// <param name="value">An array of strings that contains the names of the categories to initialize the collection values to. </param>
		// Token: 0x060005AF RID: 1455 RVA: 0x00004644 File Offset: 0x00002844
		public CategoryNameCollection(string[] value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the category name at the specified index.</summary>
		/// <param name="index">The index of the collection element to access. </param>
		/// <returns>The category name at the specified index.</returns>
		// Token: 0x17000198 RID: 408
		public string this[int index]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Indicates whether the specified category is contained in the collection.</summary>
		/// <param name="value">The string to check for in the collection. </param>
		/// <returns>
		///     <see langword="true" /> if the specified category is contained in the collection; otherwise, <see langword="false" />.</returns>
		// Token: 0x060005B1 RID: 1457 RVA: 0x0000613C File Offset: 0x0000433C
		public bool Contains(string value)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Copies the collection elements to the specified array at the specified index.</summary>
		/// <param name="array">The array to copy to. </param>
		/// <param name="index">The index of the destination array at which to begin copying. </param>
		// Token: 0x060005B2 RID: 1458 RVA: 0x00004644 File Offset: 0x00002844
		public void CopyTo(string[] array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the index of the specified value.</summary>
		/// <param name="value">The category name to retrieve the index of in the collection. </param>
		/// <returns>The index in the collection, or <see langword="null" /> if the string does not exist in the collection.</returns>
		// Token: 0x060005B3 RID: 1459 RVA: 0x00006158 File Offset: 0x00004358
		public int IndexOf(string value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}
	}
}
