﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Drawing.Design
{
	/// <summary>Provides information about a property displayed in the Properties window, including the associated event handler, pop-up information string, and the icon to display for the property.</summary>
	// Token: 0x020000C0 RID: 192
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public class PropertyValueUIItem
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Design.PropertyValueUIItem" /> class.</summary>
		/// <param name="uiItemImage">The icon to display. The image must be 8 x 8 pixels. </param>
		/// <param name="handler">The handler to invoke when the image is double-clicked. </param>
		/// <param name="tooltip">The <see cref="P:System.Drawing.Design.PropertyValueUIItem.ToolTip" /> to display for the property that this <see cref="T:System.Drawing.Design.PropertyValueUIItem" /> is associated with. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="uiItemImage" /> or <paramref name="handler" /> is <see langword="null" />.</exception>
		// Token: 0x0600092A RID: 2346 RVA: 0x00004644 File Offset: 0x00002844
		public PropertyValueUIItem(Image uiItemImage, PropertyValueUIItemInvokeHandler handler, string tooltip)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the 8 x 8 pixel image that will be drawn in the Properties window.</summary>
		/// <returns>The image to use for the property icon.</returns>
		// Token: 0x1700039C RID: 924
		// (get) Token: 0x0600092B RID: 2347 RVA: 0x00004667 File Offset: 0x00002867
		public virtual Image Image
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the handler that is raised when a user double-clicks this item.</summary>
		/// <returns>A <see cref="T:System.Drawing.Design.PropertyValueUIItemInvokeHandler" /> indicating the event handler for this user interface (UI) item.</returns>
		// Token: 0x1700039D RID: 925
		// (get) Token: 0x0600092C RID: 2348 RVA: 0x00004667 File Offset: 0x00002867
		public virtual PropertyValueUIItemInvokeHandler InvokeHandler
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the information string to display for this item.</summary>
		/// <returns>A string containing the information string to display for this item.</returns>
		// Token: 0x1700039E RID: 926
		// (get) Token: 0x0600092D RID: 2349 RVA: 0x00004667 File Offset: 0x00002867
		public virtual string ToolTip
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Resets the user interface (UI) item.</summary>
		// Token: 0x0600092E RID: 2350 RVA: 0x00004644 File Offset: 0x00002844
		public virtual void Reset()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
