﻿using System;
using System.ComponentModel;
using System.Security.Permissions;
using Unity;

namespace System.Drawing.Design
{
	/// <summary>Provides data for the <see cref="E:System.Drawing.Design.ToolboxItem.ComponentsCreated" /> event that occurs when components are added to the toolbox.</summary>
	// Token: 0x0200006D RID: 109
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public class ToolboxComponentsCreatedEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Design.ToolboxComponentsCreatedEventArgs" /> class.</summary>
		/// <param name="components">The components to include in the toolbox. </param>
		// Token: 0x060005A4 RID: 1444 RVA: 0x00004644 File Offset: 0x00002844
		public ToolboxComponentsCreatedEventArgs(IComponent[] components)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets an array containing the components to add to the toolbox.</summary>
		/// <returns>An array of type <see cref="T:System.ComponentModel.IComponent" /> indicating the components to add to the toolbox.</returns>
		// Token: 0x17000196 RID: 406
		// (get) Token: 0x060005A5 RID: 1445 RVA: 0x00004667 File Offset: 0x00002867
		public IComponent[] Components
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
