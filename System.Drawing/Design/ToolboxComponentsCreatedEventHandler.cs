﻿using System;
using Unity;

namespace System.Drawing.Design
{
	/// <summary>Represents the method that handles the <see cref="E:System.Drawing.Design.ToolboxItem.ComponentsCreated" /> event.</summary>
	/// <param name="sender">The source of the event. </param>
	/// <param name="e">A <see cref="T:System.Drawing.Design.ToolboxComponentsCreatedEventArgs" /> that provides data for the event. </param>
	// Token: 0x0200006C RID: 108
	public sealed class ToolboxComponentsCreatedEventHandler : MulticastDelegate
	{
		// Token: 0x060005A0 RID: 1440 RVA: 0x00004644 File Offset: 0x00002844
		public ToolboxComponentsCreatedEventHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060005A1 RID: 1441 RVA: 0x00004644 File Offset: 0x00002844
		public void Invoke(object sender, ToolboxComponentsCreatedEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060005A2 RID: 1442 RVA: 0x00004667 File Offset: 0x00002867
		public IAsyncResult BeginInvoke(object sender, ToolboxComponentsCreatedEventArgs e, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060005A3 RID: 1443 RVA: 0x00004644 File Offset: 0x00002844
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
