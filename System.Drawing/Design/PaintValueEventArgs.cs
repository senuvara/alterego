﻿using System;
using System.ComponentModel;
using System.Security.Permissions;
using Unity;

namespace System.Drawing.Design
{
	/// <summary>Provides data for the <see cref="M:System.Drawing.Design.UITypeEditor.PaintValue(System.Object,System.Drawing.Graphics,System.Drawing.Rectangle)" /> method.</summary>
	// Token: 0x02000012 RID: 18
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public class PaintValueEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Design.PaintValueEventArgs" /> class using the specified values.</summary>
		/// <param name="context">The context in which the value appears. </param>
		/// <param name="value">The value to paint. </param>
		/// <param name="graphics">The <see cref="T:System.Drawing.Graphics" /> object with which drawing is to be done. </param>
		/// <param name="bounds">The <see cref="T:System.Drawing.Rectangle" /> in which drawing is to be done. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="graphics" /> is <see langword="null" />.</exception>
		// Token: 0x06000161 RID: 353 RVA: 0x00004644 File Offset: 0x00002844
		public PaintValueEventArgs(ITypeDescriptorContext context, object value, Graphics graphics, Rectangle bounds)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the rectangle that indicates the area in which the painting should be done.</summary>
		/// <returns>The rectangle that indicates the area in which the painting should be done.</returns>
		// Token: 0x170000BB RID: 187
		// (get) Token: 0x06000162 RID: 354 RVA: 0x000046E0 File Offset: 0x000028E0
		public Rectangle Bounds
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Rectangle);
			}
		}

		/// <summary>Gets the <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> interface to be used to gain additional information about the context this value appears in.</summary>
		/// <returns>An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that indicates the context of the event.</returns>
		// Token: 0x170000BC RID: 188
		// (get) Token: 0x06000163 RID: 355 RVA: 0x00004667 File Offset: 0x00002867
		public ITypeDescriptorContext Context
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the <see cref="T:System.Drawing.Graphics" /> object with which painting should be done.</summary>
		/// <returns>A <see cref="T:System.Drawing.Graphics" /> object to use for painting.</returns>
		// Token: 0x170000BD RID: 189
		// (get) Token: 0x06000164 RID: 356 RVA: 0x00004667 File Offset: 0x00002867
		public Graphics Graphics
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the value to paint.</summary>
		/// <returns>An object indicating what to paint.</returns>
		// Token: 0x170000BE RID: 190
		// (get) Token: 0x06000165 RID: 357 RVA: 0x00004667 File Offset: 0x00002867
		public object Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
