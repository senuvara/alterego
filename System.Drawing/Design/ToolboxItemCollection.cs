﻿using System;
using System.Collections;
using System.Security.Permissions;
using Unity;

namespace System.Drawing.Design
{
	/// <summary>Represents a collection of toolbox items.</summary>
	// Token: 0x02000074 RID: 116
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public sealed class ToolboxItemCollection : ReadOnlyCollectionBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Design.ToolboxItemCollection" /> class using the specified collection.</summary>
		/// <param name="value">A <see cref="T:System.Drawing.Design.ToolboxItemCollection" /> to fill the new collection with. </param>
		// Token: 0x060005D6 RID: 1494 RVA: 0x00004644 File Offset: 0x00002844
		public ToolboxItemCollection(ToolboxItemCollection value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Design.ToolboxItemCollection" /> class using the specified array of toolbox items.</summary>
		/// <param name="value">An array of type <see cref="T:System.Drawing.Design.ToolboxItem" /> containing the toolbox items to fill the collection with. </param>
		// Token: 0x060005D7 RID: 1495 RVA: 0x00004644 File Offset: 0x00002844
		public ToolboxItemCollection(ToolboxItem[] value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the <see cref="T:System.Drawing.Design.ToolboxItem" /> at the specified index.</summary>
		/// <param name="index">The index of the object to get or set. </param>
		/// <returns>A <see cref="T:System.Drawing.Design.ToolboxItem" /> at each valid index in the collection.</returns>
		// Token: 0x1700019B RID: 411
		public ToolboxItem this[int index]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Indicates whether the collection contains the specified <see cref="T:System.Drawing.Design.ToolboxItem" />.</summary>
		/// <param name="value">A <see cref="T:System.Drawing.Design.ToolboxItem" /> to search the collection for. </param>
		/// <returns>
		///     <see langword="true" /> if the collection contains the specified object; otherwise, <see langword="false" />.</returns>
		// Token: 0x060005D9 RID: 1497 RVA: 0x00006174 File Offset: 0x00004374
		public bool Contains(ToolboxItem value)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Copies the collection to the specified array beginning with the specified destination index.</summary>
		/// <param name="array">The array to copy to. </param>
		/// <param name="index">The index to begin copying to. </param>
		// Token: 0x060005DA RID: 1498 RVA: 0x00004644 File Offset: 0x00002844
		public void CopyTo(ToolboxItem[] array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the index of the specified <see cref="T:System.Drawing.Design.ToolboxItem" />, if it exists in the collection.</summary>
		/// <param name="value">A <see cref="T:System.Drawing.Design.ToolboxItem" /> to get the index of in the collection. </param>
		/// <returns>The index of the specified <see cref="T:System.Drawing.Design.ToolboxItem" />.</returns>
		// Token: 0x060005DB RID: 1499 RVA: 0x00006190 File Offset: 0x00004390
		public int IndexOf(ToolboxItem value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}
	}
}
