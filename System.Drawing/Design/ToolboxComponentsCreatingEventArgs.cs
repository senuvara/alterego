﻿using System;
using System.ComponentModel.Design;
using System.Security.Permissions;
using Unity;

namespace System.Drawing.Design
{
	/// <summary>Provides data for the <see cref="E:System.Drawing.Design.ToolboxItem.ComponentsCreating" /> event that occurs when components are added to the toolbox.</summary>
	// Token: 0x0200006F RID: 111
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	public class ToolboxComponentsCreatingEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Design.ToolboxComponentsCreatingEventArgs" /> class.</summary>
		/// <param name="host">The designer host that is making the request. </param>
		// Token: 0x060005AA RID: 1450 RVA: 0x00004644 File Offset: 0x00002844
		public ToolboxComponentsCreatingEventArgs(IDesignerHost host)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets an instance of the <see cref="T:System.ComponentModel.Design.IDesignerHost" /> that made the request to create toolbox components.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.IDesignerHost" /> that made the request to create toolbox components, or <see langword="null" /> if no designer host was provided to the toolbox item.</returns>
		// Token: 0x17000197 RID: 407
		// (get) Token: 0x060005AB RID: 1451 RVA: 0x00004667 File Offset: 0x00002867
		public IDesignerHost DesignerHost
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
