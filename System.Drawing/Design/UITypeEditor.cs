﻿using System;
using System.ComponentModel;
using System.Security.Permissions;
using Unity;

namespace System.Drawing.Design
{
	/// <summary>Provides a base class that can be used to design value editors that can provide a user interface (UI) for representing and editing the values of objects of the supported data types.</summary>
	// Token: 0x02000010 RID: 16
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public class UITypeEditor
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Design.UITypeEditor" /> class.</summary>
		// Token: 0x06000157 RID: 343 RVA: 0x00004644 File Offset: 0x00002844
		public UITypeEditor()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a value indicating whether drop-down editors should be resizable by the user.</summary>
		/// <returns>
		///     <see langword="true" /> if drop-down editors are resizable; otherwise, <see langword="false" />. </returns>
		// Token: 0x170000BA RID: 186
		// (get) Token: 0x06000158 RID: 344 RVA: 0x0000464C File Offset: 0x0000284C
		public virtual bool IsDropDownResizable
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Edits the specified object's value using the editor style indicated by the <see cref="M:System.Drawing.Design.UITypeEditor.GetEditStyle" /> method.</summary>
		/// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that can be used to gain additional context information. </param>
		/// <param name="provider">An <see cref="T:System.IServiceProvider" /> that this editor can use to obtain services. </param>
		/// <param name="value">The object to edit. </param>
		/// <returns>The new value of the object. If the value of the object has not changed, this should return the same object it was passed.</returns>
		// Token: 0x06000159 RID: 345 RVA: 0x00004667 File Offset: 0x00002867
		public virtual object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Edits the value of the specified object using the editor style indicated by the <see cref="M:System.Drawing.Design.UITypeEditor.GetEditStyle" /> method.</summary>
		/// <param name="provider">An <see cref="T:System.IServiceProvider" /> that this editor can use to obtain services. </param>
		/// <param name="value">The object to edit. </param>
		/// <returns>The new value of the object.</returns>
		// Token: 0x0600015A RID: 346 RVA: 0x00004667 File Offset: 0x00002867
		public object EditValue(IServiceProvider provider, object value)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the editor style used by the <see cref="M:System.Drawing.Design.UITypeEditor.EditValue(System.IServiceProvider,System.Object)" /> method.</summary>
		/// <returns>A <see cref="T:System.Drawing.Design.UITypeEditorEditStyle" /> enumeration value that indicates the style of editor used by the current <see cref="T:System.Drawing.Design.UITypeEditor" />. By default, this method will return <see cref="F:System.Drawing.Design.UITypeEditorEditStyle.None" />.</returns>
		// Token: 0x0600015B RID: 347 RVA: 0x00004670 File Offset: 0x00002870
		public UITypeEditorEditStyle GetEditStyle()
		{
			ThrowStub.ThrowNotSupportedException();
			return (UITypeEditorEditStyle)0;
		}

		/// <summary>Gets the editor style used by the <see cref="M:System.Drawing.Design.UITypeEditor.EditValue(System.IServiceProvider,System.Object)" /> method.</summary>
		/// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that can be used to gain additional context information. </param>
		/// <returns>A <see cref="T:System.Drawing.Design.UITypeEditorEditStyle" /> value that indicates the style of editor used by the <see cref="M:System.Drawing.Design.UITypeEditor.EditValue(System.IServiceProvider,System.Object)" /> method. If the <see cref="T:System.Drawing.Design.UITypeEditor" /> does not support this method, then <see cref="M:System.Drawing.Design.UITypeEditor.GetEditStyle" /> will return <see cref="F:System.Drawing.Design.UITypeEditorEditStyle.None" />.</returns>
		// Token: 0x0600015C RID: 348 RVA: 0x0000468C File Offset: 0x0000288C
		public virtual UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			ThrowStub.ThrowNotSupportedException();
			return (UITypeEditorEditStyle)0;
		}

		/// <summary>Indicates whether this editor supports painting a representation of an object's value.</summary>
		/// <returns>
		///     <see langword="true" /> if <see cref="M:System.Drawing.Design.UITypeEditor.PaintValue(System.Object,System.Drawing.Graphics,System.Drawing.Rectangle)" /> is implemented; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600015D RID: 349 RVA: 0x000046A8 File Offset: 0x000028A8
		public bool GetPaintValueSupported()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the specified context supports painting a representation of an object's value within the specified context.</summary>
		/// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that can be used to gain additional context information. </param>
		/// <returns>
		///     <see langword="true" /> if <see cref="M:System.Drawing.Design.UITypeEditor.PaintValue(System.Object,System.Drawing.Graphics,System.Drawing.Rectangle)" /> is implemented; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600015E RID: 350 RVA: 0x000046C4 File Offset: 0x000028C4
		public virtual bool GetPaintValueSupported(ITypeDescriptorContext context)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Paints a representation of the value of an object using the specified <see cref="T:System.Drawing.Design.PaintValueEventArgs" />.</summary>
		/// <param name="e">A <see cref="T:System.Drawing.Design.PaintValueEventArgs" /> that indicates what to paint and where to paint it. </param>
		// Token: 0x0600015F RID: 351 RVA: 0x00004644 File Offset: 0x00002844
		public virtual void PaintValue(PaintValueEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Paints a representation of the value of the specified object to the specified canvas.</summary>
		/// <param name="value">The object whose value this type editor will display. </param>
		/// <param name="canvas">A drawing canvas on which to paint the representation of the object's value. </param>
		/// <param name="rectangle">A <see cref="T:System.Drawing.Rectangle" /> within whose boundaries to paint the value. </param>
		// Token: 0x06000160 RID: 352 RVA: 0x00004644 File Offset: 0x00002844
		public void PaintValue(object value, Graphics canvas, Rectangle rectangle)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
