﻿using System;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Unity;

namespace System.Drawing.Design
{
	/// <summary>Provides a base implementation of a toolbox item.</summary>
	// Token: 0x0200006B RID: 107
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	[Serializable]
	public class ToolboxItem : ISerializable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Design.ToolboxItem" /> class.</summary>
		// Token: 0x06000570 RID: 1392 RVA: 0x00004644 File Offset: 0x00002844
		public ToolboxItem()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.Design.ToolboxItem" /> class that creates the specified type of component.</summary>
		/// <param name="toolType">The type of <see cref="T:System.ComponentModel.IComponent" /> that the toolbox item creates. </param>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Drawing.Design.ToolboxItem" /> was locked. </exception>
		// Token: 0x06000571 RID: 1393 RVA: 0x00004644 File Offset: 0x00002844
		public ToolboxItem(Type toolType)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the name of the assembly that contains the type or types that the toolbox item creates.</summary>
		/// <returns>An <see cref="T:System.Reflection.AssemblyName" /> that indicates the assembly containing the type or types to create.</returns>
		// Token: 0x17000188 RID: 392
		// (get) Token: 0x06000572 RID: 1394 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000573 RID: 1395 RVA: 0x00004644 File Offset: 0x00002844
		public AssemblyName AssemblyName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a bitmap to represent the toolbox item in the toolbox.</summary>
		/// <returns>A <see cref="T:System.Drawing.Bitmap" /> that represents the toolbox item in the toolbox.</returns>
		// Token: 0x17000189 RID: 393
		// (get) Token: 0x06000574 RID: 1396 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000575 RID: 1397 RVA: 0x00004644 File Offset: 0x00002844
		public Bitmap Bitmap
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the company name for this <see cref="T:System.Drawing.Design.ToolboxItem" />.</summary>
		/// <returns>A <see cref="T:System.String" /> that specifies the company for this <see cref="T:System.Drawing.Design.ToolboxItem" />.</returns>
		// Token: 0x1700018A RID: 394
		// (get) Token: 0x06000576 RID: 1398 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000577 RID: 1399 RVA: 0x00004644 File Offset: 0x00002844
		public string Company
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the component type for this <see cref="T:System.Drawing.Design.ToolboxItem" />.</summary>
		/// <returns>A <see cref="T:System.String" /> that specifies the component type for this <see cref="T:System.Drawing.Design.ToolboxItem" />.</returns>
		// Token: 0x1700018B RID: 395
		// (get) Token: 0x06000578 RID: 1400 RVA: 0x00004667 File Offset: 0x00002867
		public virtual string ComponentType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Reflection.AssemblyName" /> for the toolbox item.</summary>
		/// <returns>An array of <see cref="T:System.Reflection.AssemblyName" /> objects.</returns>
		// Token: 0x1700018C RID: 396
		// (get) Token: 0x06000579 RID: 1401 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x0600057A RID: 1402 RVA: 0x00004644 File Offset: 0x00002844
		public AssemblyName[] DependentAssemblies
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the description for this <see cref="T:System.Drawing.Design.ToolboxItem" />.</summary>
		/// <returns>A <see cref="T:System.String" /> that specifies the description for this <see cref="T:System.Drawing.Design.ToolboxItem" />.</returns>
		// Token: 0x1700018D RID: 397
		// (get) Token: 0x0600057B RID: 1403 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x0600057C RID: 1404 RVA: 0x00004644 File Offset: 0x00002844
		public string Description
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the display name for the toolbox item.</summary>
		/// <returns>The display name for the toolbox item.</returns>
		// Token: 0x1700018E RID: 398
		// (get) Token: 0x0600057D RID: 1405 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x0600057E RID: 1406 RVA: 0x00004644 File Offset: 0x00002844
		public string DisplayName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the filter that determines whether the toolbox item can be used on a destination component.</summary>
		/// <returns>An <see cref="T:System.Collections.ICollection" /> of <see cref="T:System.ComponentModel.ToolboxItemFilterAttribute" /> objects.</returns>
		// Token: 0x1700018F RID: 399
		// (get) Token: 0x0600057F RID: 1407 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000580 RID: 1408 RVA: 0x00004644 File Offset: 0x00002844
		public ICollection Filter
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a value indicating whether the toolbox item is transient.</summary>
		/// <returns>
		///     <see langword="true" />, if this toolbox item should not be stored in any toolbox database when an application that is providing a toolbox closes; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000190 RID: 400
		// (get) Token: 0x06000581 RID: 1409 RVA: 0x00006104 File Offset: 0x00004304
		// (set) Token: 0x06000582 RID: 1410 RVA: 0x00004644 File Offset: 0x00002844
		public bool IsTransient
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a value indicating whether the <see cref="T:System.Drawing.Design.ToolboxItem" /> is currently locked.</summary>
		/// <returns>
		///     <see langword="true" /> if the toolbox item is locked; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000191 RID: 401
		// (get) Token: 0x06000583 RID: 1411 RVA: 0x00006120 File Offset: 0x00004320
		public virtual bool Locked
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets or sets the original bitmap that will be used in the toolbox for this item.</summary>
		/// <returns>A <see cref="T:System.Drawing.Bitmap" /> that represents the toolbox item in the toolbox.</returns>
		// Token: 0x17000192 RID: 402
		// (get) Token: 0x06000584 RID: 1412 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000585 RID: 1413 RVA: 0x00004644 File Offset: 0x00002844
		public Bitmap OriginalBitmap
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a dictionary of properties.</summary>
		/// <returns>A dictionary of name/value pairs (the names are property names and the values are property values).</returns>
		// Token: 0x17000193 RID: 403
		// (get) Token: 0x06000586 RID: 1414 RVA: 0x00004667 File Offset: 0x00002867
		public IDictionary Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the fully qualified name of the type of <see cref="T:System.ComponentModel.IComponent" /> that the toolbox item creates when invoked.</summary>
		/// <returns>The fully qualified type name of the type of component that this toolbox item creates.</returns>
		// Token: 0x17000194 RID: 404
		// (get) Token: 0x06000587 RID: 1415 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x06000588 RID: 1416 RVA: 0x00004644 File Offset: 0x00002844
		public string TypeName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the version for this <see cref="T:System.Drawing.Design.ToolboxItem" />.</summary>
		/// <returns>A <see cref="T:System.String" /> that specifies the version for this <see cref="T:System.Drawing.Design.ToolboxItem" />.</returns>
		// Token: 0x17000195 RID: 405
		// (get) Token: 0x06000589 RID: 1417 RVA: 0x00004667 File Offset: 0x00002867
		public virtual string Version
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Occurs immediately after components are created.</summary>
		// Token: 0x14000001 RID: 1
		// (add) Token: 0x0600058A RID: 1418 RVA: 0x00004644 File Offset: 0x00002844
		// (remove) Token: 0x0600058B RID: 1419 RVA: 0x00004644 File Offset: 0x00002844
		public event ToolboxComponentsCreatedEventHandler ComponentsCreated
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs when components are about to be created.</summary>
		// Token: 0x14000002 RID: 2
		// (add) Token: 0x0600058C RID: 1420 RVA: 0x00004644 File Offset: 0x00002844
		// (remove) Token: 0x0600058D RID: 1421 RVA: 0x00004644 File Offset: 0x00002844
		public event ToolboxComponentsCreatingEventHandler ComponentsCreating
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Throws an exception if the toolbox item is currently locked.</summary>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Drawing.Design.ToolboxItem" /> is locked. </exception>
		// Token: 0x0600058E RID: 1422 RVA: 0x00004644 File Offset: 0x00002844
		protected void CheckUnlocked()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates the components that the toolbox item is configured to create.</summary>
		/// <returns>An array of created <see cref="T:System.ComponentModel.IComponent" /> objects.</returns>
		// Token: 0x0600058F RID: 1423 RVA: 0x00004667 File Offset: 0x00002867
		public IComponent[] CreateComponents()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates the components that the toolbox item is configured to create, using the specified designer host.</summary>
		/// <param name="host">The <see cref="T:System.ComponentModel.Design.IDesignerHost" /> to use when creating the components. </param>
		/// <returns>An array of created <see cref="T:System.ComponentModel.IComponent" /> objects.</returns>
		// Token: 0x06000590 RID: 1424 RVA: 0x00004667 File Offset: 0x00002867
		public IComponent[] CreateComponents(IDesignerHost host)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates the components that the toolbox item is configured to create, using the specified designer host and default values.</summary>
		/// <param name="host">The <see cref="T:System.ComponentModel.Design.IDesignerHost" /> to use when creating the components.</param>
		/// <param name="defaultValues">A dictionary of property name/value pairs of default values with which to initialize the component.</param>
		/// <returns>An array of created <see cref="T:System.ComponentModel.IComponent" /> objects.</returns>
		// Token: 0x06000591 RID: 1425 RVA: 0x00004667 File Offset: 0x00002867
		public IComponent[] CreateComponents(IDesignerHost host, IDictionary defaultValues)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates a component or an array of components when the toolbox item is invoked.</summary>
		/// <param name="host">The <see cref="T:System.ComponentModel.Design.IDesignerHost" /> to host the toolbox item. </param>
		/// <returns>An array of created <see cref="T:System.ComponentModel.IComponent" /> objects.</returns>
		// Token: 0x06000592 RID: 1426 RVA: 0x00004667 File Offset: 0x00002867
		protected virtual IComponent[] CreateComponentsCore(IDesignerHost host)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates an array of components when the toolbox item is invoked.</summary>
		/// <param name="host">The designer host to use when creating components.</param>
		/// <param name="defaultValues">A dictionary of property name/value pairs of default values with which to initialize the component.</param>
		/// <returns>An array of created <see cref="T:System.ComponentModel.IComponent" /> objects.</returns>
		// Token: 0x06000593 RID: 1427 RVA: 0x00004667 File Offset: 0x00002867
		protected virtual IComponent[] CreateComponentsCore(IDesignerHost host, IDictionary defaultValues)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Loads the state of the toolbox item from the specified serialization information object.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to load from. </param>
		/// <param name="context">A <see cref="T:System.Runtime.Serialization.StreamingContext" /> that indicates the stream characteristics. </param>
		// Token: 0x06000594 RID: 1428 RVA: 0x00004644 File Offset: 0x00002844
		protected virtual void Deserialize(SerializationInfo info, StreamingContext context)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Filters a property value before returning it.</summary>
		/// <param name="propertyName">The name of the property to filter.</param>
		/// <param name="value">The value against which to filter the property.</param>
		/// <returns>A filtered property value.</returns>
		// Token: 0x06000595 RID: 1429 RVA: 0x00004667 File Offset: 0x00002867
		protected virtual object FilterPropertyValue(string propertyName, object value)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Enables access to the type associated with the toolbox item.</summary>
		/// <param name="host">The designer host to query for <see cref="T:System.ComponentModel.Design.ITypeResolutionService" />.</param>
		/// <returns>The type associated with the toolbox item.</returns>
		// Token: 0x06000596 RID: 1430 RVA: 0x00004667 File Offset: 0x00002867
		public Type GetType(IDesignerHost host)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates an instance of the specified type, optionally using a specified designer host and assembly name.</summary>
		/// <param name="host">The <see cref="T:System.ComponentModel.Design.IDesignerHost" /> for the current document. This can be <see langword="null" />. </param>
		/// <param name="assemblyName">An <see cref="T:System.Reflection.AssemblyName" /> that indicates the assembly that contains the type to load. This can be <see langword="null" />. </param>
		/// <param name="typeName">The name of the type to create an instance of. </param>
		/// <param name="reference">A value indicating whether or not to add a reference to the assembly that contains the specified type to the designer host's set of references. </param>
		/// <returns>An instance of the specified type, if it can be located.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="typeName" /> is not specified. </exception>
		// Token: 0x06000597 RID: 1431 RVA: 0x00004667 File Offset: 0x00002867
		protected virtual Type GetType(IDesignerHost host, AssemblyName assemblyName, string typeName, bool reference)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Initializes the current toolbox item with the specified type to create.</summary>
		/// <param name="type">The <see cref="T:System.Type" /> that the toolbox item creates. </param>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Drawing.Design.ToolboxItem" /> was locked. </exception>
		// Token: 0x06000598 RID: 1432 RVA: 0x00004644 File Offset: 0x00002844
		public virtual void Initialize(Type type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Locks the toolbox item and prevents changes to its properties.</summary>
		// Token: 0x06000599 RID: 1433 RVA: 0x00004644 File Offset: 0x00002844
		public virtual void Lock()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Raises the <see cref="E:System.Drawing.Design.ToolboxItem.ComponentsCreated" /> event.</summary>
		/// <param name="args">A <see cref="T:System.Drawing.Design.ToolboxComponentsCreatedEventArgs" /> that provides data for the event. </param>
		// Token: 0x0600059A RID: 1434 RVA: 0x00004644 File Offset: 0x00002844
		protected virtual void OnComponentsCreated(ToolboxComponentsCreatedEventArgs args)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Raises the <see cref="E:System.Drawing.Design.ToolboxItem.ComponentsCreating" /> event.</summary>
		/// <param name="args">A <see cref="T:System.Drawing.Design.ToolboxComponentsCreatingEventArgs" /> that provides data for the event. </param>
		// Token: 0x0600059B RID: 1435 RVA: 0x00004644 File Offset: 0x00002844
		protected virtual void OnComponentsCreating(ToolboxComponentsCreatingEventArgs args)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Saves the state of the toolbox item to the specified serialization information object.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to save to. </param>
		/// <param name="context">A <see cref="T:System.Runtime.Serialization.StreamingContext" /> that indicates the stream characteristics. </param>
		// Token: 0x0600059C RID: 1436 RVA: 0x00004644 File Offset: 0x00002844
		protected virtual void Serialize(SerializationInfo info, StreamingContext context)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>For a description of this member, see the <see cref="M:System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)" /> method.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data.</param>
		/// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this serialization.</param>
		// Token: 0x0600059D RID: 1437 RVA: 0x00004644 File Offset: 0x00002844
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Validates that an object is of a given type.</summary>
		/// <param name="propertyName">The name of the property to validate.</param>
		/// <param name="value">Optional value against which to validate.</param>
		/// <param name="expectedType">The expected type of the property.</param>
		/// <param name="allowNull">
		///       <see langword="true" /> to allow <see langword="null" />; otherwise, <see langword="false" />.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />, and <paramref name="allowNull" /> is <see langword="false" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="value" /> is not the type specified by <paramref name="expectedType" />.</exception>
		// Token: 0x0600059E RID: 1438 RVA: 0x00004644 File Offset: 0x00002844
		protected void ValidatePropertyType(string propertyName, object value, Type expectedType, bool allowNull)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Validates a property before it is assigned to the property dictionary.</summary>
		/// <param name="propertyName">The name of the property to validate.</param>
		/// <param name="value">The value against which to validate.</param>
		/// <returns>The value used to perform validation.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />, and <paramref name="propertyName" /> is "IsTransient".</exception>
		// Token: 0x0600059F RID: 1439 RVA: 0x00004667 File Offset: 0x00002867
		protected virtual object ValidatePropertyValue(string propertyName, object value)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
