﻿using System;

namespace System.Drawing
{
	/// <summary>Specifies style information applied to text.</summary>
	// Token: 0x0200001F RID: 31
	[Flags]
	public enum FontStyle
	{
		/// <summary>Bold text.</summary>
		// Token: 0x04000206 RID: 518
		Bold = 1,
		/// <summary>Italic text.</summary>
		// Token: 0x04000207 RID: 519
		Italic = 2,
		/// <summary>Normal text.</summary>
		// Token: 0x04000208 RID: 520
		Regular = 0,
		/// <summary>Text with a line through the middle.</summary>
		// Token: 0x04000209 RID: 521
		Strikeout = 8,
		/// <summary>Underlined text.</summary>
		// Token: 0x0400020A RID: 522
		Underline = 4
	}
}
