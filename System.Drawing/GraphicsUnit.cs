﻿using System;

namespace System.Drawing
{
	/// <summary>Specifies the unit of measure for the given data.</summary>
	// Token: 0x02000037 RID: 55
	public enum GraphicsUnit
	{
		/// <summary>Specifies the unit of measure of the display device. Typically pixels for video displays, and 1/100 inch for printers.</summary>
		// Token: 0x0400026D RID: 621
		Display = 1,
		/// <summary>Specifies the document unit (1/300 inch) as the unit of measure.</summary>
		// Token: 0x0400026E RID: 622
		Document = 5,
		/// <summary>Specifies the inch as the unit of measure.</summary>
		// Token: 0x0400026F RID: 623
		Inch = 4,
		/// <summary>Specifies the millimeter as the unit of measure.</summary>
		// Token: 0x04000270 RID: 624
		Millimeter = 6,
		/// <summary>Specifies a device pixel as the unit of measure.</summary>
		// Token: 0x04000271 RID: 625
		Pixel = 2,
		/// <summary>Specifies a printer's point (1/72 inch) as the unit of measure.</summary>
		// Token: 0x04000272 RID: 626
		Point,
		/// <summary>Specifies the world coordinate system unit as the unit of measure.</summary>
		// Token: 0x04000273 RID: 627
		World = 0
	}
}
