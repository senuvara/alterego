﻿using System;
using System.Drawing.Text;
using Unity;

namespace System.Drawing
{
	/// <summary>Encapsulates text layout information (such as alignment, orientation and tab stops) display manipulations (such as ellipsis insertion and national digit substitution) and OpenType features. This class cannot be inherited.</summary>
	// Token: 0x02000020 RID: 32
	public sealed class StringFormat : MarshalByRefObject, ICloneable, IDisposable
	{
		/// <summary>Initializes a new <see cref="T:System.Drawing.StringFormat" /> object.</summary>
		// Token: 0x0600031D RID: 797 RVA: 0x00004644 File Offset: 0x00002844
		public StringFormat()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.StringFormat" /> object from the specified existing <see cref="T:System.Drawing.StringFormat" /> object.</summary>
		/// <param name="format">The <see cref="T:System.Drawing.StringFormat" /> object from which to initialize the new <see cref="T:System.Drawing.StringFormat" /> object. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="format" /> is <see langword="null" />.</exception>
		// Token: 0x0600031E RID: 798 RVA: 0x00004644 File Offset: 0x00002844
		public StringFormat(StringFormat format)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.StringFormat" /> object with the specified <see cref="T:System.Drawing.StringFormatFlags" /> enumeration.</summary>
		/// <param name="options">The <see cref="T:System.Drawing.StringFormatFlags" /> enumeration for the new <see cref="T:System.Drawing.StringFormat" /> object. </param>
		// Token: 0x0600031F RID: 799 RVA: 0x00004644 File Offset: 0x00002844
		public StringFormat(StringFormatFlags options)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.StringFormat" /> object with the specified <see cref="T:System.Drawing.StringFormatFlags" /> enumeration and language.</summary>
		/// <param name="options">The <see cref="T:System.Drawing.StringFormatFlags" /> enumeration for the new <see cref="T:System.Drawing.StringFormat" /> object. </param>
		/// <param name="language">A value that indicates the language of the text. </param>
		// Token: 0x06000320 RID: 800 RVA: 0x00004644 File Offset: 0x00002844
		public StringFormat(StringFormatFlags options, int language)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets horizontal alignment of the string.</summary>
		/// <returns>A <see cref="T:System.Drawing.StringAlignment" /> enumeration that specifies the horizontal  alignment of the string.</returns>
		// Token: 0x170000DE RID: 222
		// (get) Token: 0x06000321 RID: 801 RVA: 0x00005048 File Offset: 0x00003248
		// (set) Token: 0x06000322 RID: 802 RVA: 0x00004644 File Offset: 0x00002844
		public StringAlignment Alignment
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return StringAlignment.Near;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the language that is used when local digits are substituted for western digits.</summary>
		/// <returns>A National Language Support (NLS) language identifier that identifies the language that will be used when local digits are substituted for western digits. You can pass the <see cref="P:System.Globalization.CultureInfo.LCID" /> property of a <see cref="T:System.Globalization.CultureInfo" /> object as the NLS language identifier. For example, suppose you create a <see cref="T:System.Globalization.CultureInfo" /> object by passing the string "ar-EG" to a <see cref="T:System.Globalization.CultureInfo" /> constructor. If you pass the <see cref="P:System.Globalization.CultureInfo.LCID" /> property of that <see cref="T:System.Globalization.CultureInfo" /> object along with.<see cref="F:System.Drawing.StringDigitSubstitute.Traditional" /> to the <see cref="M:System.Drawing.StringFormat.SetDigitSubstitution(System.Int32,System.Drawing.StringDigitSubstitute)" /> method, then Arabic-Indic digits will be substituted for western digits at display time.</returns>
		// Token: 0x170000DF RID: 223
		// (get) Token: 0x06000323 RID: 803 RVA: 0x00005064 File Offset: 0x00003264
		public int DigitSubstitutionLanguage
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the method to be used for digit substitution.</summary>
		/// <returns>A <see cref="T:System.Drawing.StringDigitSubstitute" /> enumeration value that specifies how to substitute characters in a string that cannot be displayed because they are not supported by the current font.</returns>
		// Token: 0x170000E0 RID: 224
		// (get) Token: 0x06000324 RID: 804 RVA: 0x00005080 File Offset: 0x00003280
		public StringDigitSubstitute DigitSubstitutionMethod
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return StringDigitSubstitute.User;
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Drawing.StringFormatFlags" /> enumeration that contains formatting information.</summary>
		/// <returns>A <see cref="T:System.Drawing.StringFormatFlags" /> enumeration that contains formatting information.</returns>
		// Token: 0x170000E1 RID: 225
		// (get) Token: 0x06000325 RID: 805 RVA: 0x0000509C File Offset: 0x0000329C
		// (set) Token: 0x06000326 RID: 806 RVA: 0x00004644 File Offset: 0x00002844
		public StringFormatFlags FormatFlags
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (StringFormatFlags)0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a generic default <see cref="T:System.Drawing.StringFormat" /> object.</summary>
		/// <returns>The generic default <see cref="T:System.Drawing.StringFormat" /> object.</returns>
		// Token: 0x170000E2 RID: 226
		// (get) Token: 0x06000327 RID: 807 RVA: 0x00004667 File Offset: 0x00002867
		public static StringFormat GenericDefault
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a generic typographic <see cref="T:System.Drawing.StringFormat" /> object.</summary>
		/// <returns>A generic typographic <see cref="T:System.Drawing.StringFormat" /> object.</returns>
		// Token: 0x170000E3 RID: 227
		// (get) Token: 0x06000328 RID: 808 RVA: 0x00004667 File Offset: 0x00002867
		public static StringFormat GenericTypographic
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Drawing.Text.HotkeyPrefix" /> object for this <see cref="T:System.Drawing.StringFormat" /> object.</summary>
		/// <returns>The <see cref="T:System.Drawing.Text.HotkeyPrefix" /> object for this <see cref="T:System.Drawing.StringFormat" /> object, the default is <see cref="F:System.Drawing.Text.HotkeyPrefix.None" />.</returns>
		// Token: 0x170000E4 RID: 228
		// (get) Token: 0x06000329 RID: 809 RVA: 0x000050B8 File Offset: 0x000032B8
		// (set) Token: 0x0600032A RID: 810 RVA: 0x00004644 File Offset: 0x00002844
		public HotkeyPrefix HotkeyPrefix
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return HotkeyPrefix.None;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the vertical alignment of the string.</summary>
		/// <returns>A <see cref="T:System.Drawing.StringAlignment" /> enumeration that represents the vertical line alignment.</returns>
		// Token: 0x170000E5 RID: 229
		// (get) Token: 0x0600032B RID: 811 RVA: 0x000050D4 File Offset: 0x000032D4
		// (set) Token: 0x0600032C RID: 812 RVA: 0x00004644 File Offset: 0x00002844
		public StringAlignment LineAlignment
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return StringAlignment.Near;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Drawing.StringTrimming" /> enumeration for this <see cref="T:System.Drawing.StringFormat" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.StringTrimming" /> enumeration that indicates how text drawn with this <see cref="T:System.Drawing.StringFormat" /> object is trimmed when it exceeds the edges of the layout rectangle.</returns>
		// Token: 0x170000E6 RID: 230
		// (get) Token: 0x0600032D RID: 813 RVA: 0x000050F0 File Offset: 0x000032F0
		// (set) Token: 0x0600032E RID: 814 RVA: 0x00004644 File Offset: 0x00002844
		public StringTrimming Trimming
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return StringTrimming.None;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Creates an exact copy of this <see cref="T:System.Drawing.StringFormat" /> object.</summary>
		/// <returns>The <see cref="T:System.Drawing.StringFormat" /> object this method creates.</returns>
		// Token: 0x0600032F RID: 815 RVA: 0x00004667 File Offset: 0x00002867
		public object Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Releases all resources used by this <see cref="T:System.Drawing.StringFormat" /> object.</summary>
		// Token: 0x06000330 RID: 816 RVA: 0x00004644 File Offset: 0x00002844
		public void Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the tab stops for this <see cref="T:System.Drawing.StringFormat" /> object.</summary>
		/// <param name="firstTabOffset">The number of spaces between the beginning of a text line and the first tab stop. </param>
		/// <returns>An array of distances (in number of spaces) between tab stops.</returns>
		// Token: 0x06000331 RID: 817 RVA: 0x00004667 File Offset: 0x00002867
		public float[] GetTabStops(out float firstTabOffset)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Specifies the language and method to be used when local digits are substituted for western digits.</summary>
		/// <param name="language">A National Language Support (NLS) language identifier that identifies the language that will be used when local digits are substituted for western digits. You can pass the <see cref="P:System.Globalization.CultureInfo.LCID" /> property of a <see cref="T:System.Globalization.CultureInfo" /> object as the NLS language identifier. For example, suppose you create a <see cref="T:System.Globalization.CultureInfo" /> object by passing the string "ar-EG" to a <see cref="T:System.Globalization.CultureInfo" /> constructor. If you pass the <see cref="P:System.Globalization.CultureInfo.LCID" /> property of that <see cref="T:System.Globalization.CultureInfo" /> object along with <see cref="F:System.Drawing.StringDigitSubstitute.Traditional" /> to the <see cref="M:System.Drawing.StringFormat.SetDigitSubstitution(System.Int32,System.Drawing.StringDigitSubstitute)" /> method, then Arabic-Indic digits will be substituted for western digits at display time. </param>
		/// <param name="substitute">An element of the <see cref="T:System.Drawing.StringDigitSubstitute" /> enumeration that specifies how digits are displayed. </param>
		// Token: 0x06000332 RID: 818 RVA: 0x00004644 File Offset: 0x00002844
		public void SetDigitSubstitution(int language, StringDigitSubstitute substitute)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Specifies an array of <see cref="T:System.Drawing.CharacterRange" /> structures that represent the ranges of characters measured by a call to the <see cref="M:System.Drawing.Graphics.MeasureCharacterRanges(System.String,System.Drawing.Font,System.Drawing.RectangleF,System.Drawing.StringFormat)" /> method.</summary>
		/// <param name="ranges">An array of <see cref="T:System.Drawing.CharacterRange" /> structures that specifies the ranges of characters measured by a call to the <see cref="M:System.Drawing.Graphics.MeasureCharacterRanges(System.String,System.Drawing.Font,System.Drawing.RectangleF,System.Drawing.StringFormat)" /> method. </param>
		/// <exception cref="T:System.OverflowException">More than 32 character ranges are set.</exception>
		// Token: 0x06000333 RID: 819 RVA: 0x00004644 File Offset: 0x00002844
		public void SetMeasurableCharacterRanges(CharacterRange[] ranges)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets tab stops for this <see cref="T:System.Drawing.StringFormat" /> object.</summary>
		/// <param name="firstTabOffset">The number of spaces between the beginning of a line of text and the first tab stop. </param>
		/// <param name="tabStops">An array of distances between tab stops in the units specified by the <see cref="P:System.Drawing.Graphics.PageUnit" /> property. </param>
		// Token: 0x06000334 RID: 820 RVA: 0x00004644 File Offset: 0x00002844
		public void SetTabStops(float firstTabOffset, float[] tabStops)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
