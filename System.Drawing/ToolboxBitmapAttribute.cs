﻿using System;
using Unity;

namespace System.Drawing
{
	/// <summary>Allows you to specify an icon to represent a control in a container, such as the Microsoft Visual Studio Form Designer.</summary>
	// Token: 0x0200006A RID: 106
	[AttributeUsage(AttributeTargets.Class)]
	public class ToolboxBitmapAttribute : Attribute
	{
		/// <summary>Initializes a new <see cref="T:System.Drawing.ToolboxBitmapAttribute" /> object with an image from a specified file.</summary>
		/// <param name="imageFile">The name of a file that contains a 16 by 16 bitmap. </param>
		// Token: 0x06000567 RID: 1383 RVA: 0x00004642 File Offset: 0x00002842
		public ToolboxBitmapAttribute(string imageFile)
		{
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.ToolboxBitmapAttribute" /> object based on a 16 x 16 bitmap that is embedded as a resource in a specified assembly.</summary>
		/// <param name="t">A <see cref="T:System.Type" /> whose defining assembly is searched for the bitmap resource. </param>
		// Token: 0x06000568 RID: 1384 RVA: 0x00004642 File Offset: 0x00002842
		public ToolboxBitmapAttribute(Type t)
		{
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.ToolboxBitmapAttribute" /> object based on a 16 by 16 bitmap that is embedded as a resource in a specified assembly.</summary>
		/// <param name="t">A <see cref="T:System.Type" /> whose defining assembly is searched for the bitmap resource. </param>
		/// <param name="name">The name of the embedded bitmap resource. </param>
		// Token: 0x06000569 RID: 1385 RVA: 0x00004642 File Offset: 0x00002842
		public ToolboxBitmapAttribute(Type t, string name)
		{
		}

		/// <summary>Gets the small <see cref="T:System.Drawing.Image" /> associated with this <see cref="T:System.Drawing.ToolboxBitmapAttribute" /> object.</summary>
		/// <param name="component">If this <see cref="T:System.Drawing.ToolboxBitmapAttribute" /> object does not already have a small image, this method searches for a bitmap resource in the assembly that defines the type of the object specified by the component parameter. For example, if you pass an object of type ControlA to the component parameter, then this method searches the assembly that defines ControlA. </param>
		/// <returns>The small <see cref="T:System.Drawing.Image" /> associated with this <see cref="T:System.Drawing.ToolboxBitmapAttribute" /> object.</returns>
		// Token: 0x0600056A RID: 1386 RVA: 0x00004667 File Offset: 0x00002867
		public Image GetImage(object component)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the small or large <see cref="T:System.Drawing.Image" /> associated with this <see cref="T:System.Drawing.ToolboxBitmapAttribute" /> object.</summary>
		/// <param name="component">If this <see cref="T:System.Drawing.ToolboxBitmapAttribute" /> object does not already have a small image, this method searches for a bitmap resource in the assembly that defines the type of the object specified by the component parameter. For example, if you pass an object of type ControlA to the component parameter, then this method searches the assembly that defines ControlA. </param>
		/// <param name="large">Specifies whether this method returns a large image (<see langword="true" />) or a small image (<see langword="false" />). The small image is 16 by 16, and the large image is 32 by 32. </param>
		/// <returns>An <see cref="T:System.Drawing.Image" /> object associated with this <see cref="T:System.Drawing.ToolboxBitmapAttribute" /> object.</returns>
		// Token: 0x0600056B RID: 1387 RVA: 0x00004667 File Offset: 0x00002867
		public Image GetImage(object component, bool large)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the small <see cref="T:System.Drawing.Image" /> associated with this <see cref="T:System.Drawing.ToolboxBitmapAttribute" /> object.</summary>
		/// <param name="type">If this <see cref="T:System.Drawing.ToolboxBitmapAttribute" /> object does not already have a small image, this method searches for a bitmap resource in the assembly that defines the type specified by the type parameter. For example, if you pass typeof(ControlA) to the type parameter, then this method searches the assembly that defines ControlA. </param>
		/// <returns>The small <see cref="T:System.Drawing.Image" /> associated with this <see cref="T:System.Drawing.ToolboxBitmapAttribute" /> object.</returns>
		// Token: 0x0600056C RID: 1388 RVA: 0x00004667 File Offset: 0x00002867
		public Image GetImage(Type type)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the small or large <see cref="T:System.Drawing.Image" /> associated with this <see cref="T:System.Drawing.ToolboxBitmapAttribute" /> object.</summary>
		/// <param name="type">If this <see cref="T:System.Drawing.ToolboxBitmapAttribute" /> object does not already have a small image, this method searches for a bitmap resource in the assembly that defines the type specified by the component type. For example, if you pass typeof(ControlA) to the type parameter, then this method searches the assembly that defines ControlA. </param>
		/// <param name="large">Specifies whether this method returns a large image (<see langword="true" />) or a small image (<see langword="false" />). The small image is 16 by 16, and the large image is 32 by 32. </param>
		/// <returns>An <see cref="T:System.Drawing.Image" /> associated with this <see cref="T:System.Drawing.ToolboxBitmapAttribute" /> object.</returns>
		// Token: 0x0600056D RID: 1389 RVA: 0x00004667 File Offset: 0x00002867
		public Image GetImage(Type type, bool large)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the small or large <see cref="T:System.Drawing.Image" /> associated with this <see cref="T:System.Drawing.ToolboxBitmapAttribute" /> object.</summary>
		/// <param name="type">If this <see cref="T:System.Drawing.ToolboxBitmapAttribute" /> object does not already have a small image, this method searches for an embedded bitmap resource in the assembly that defines the type specified by the component type. For example, if you pass typeof(ControlA) to the type parameter, then this method searches the assembly that defines ControlA. </param>
		/// <param name="imgName">The name of the embedded bitmap resource. </param>
		/// <param name="large">Specifies whether this method returns a large image (<see langword="true" />) or a small image (<see langword="false" />). The small image is 16 by 16, and the large image is 32 by 32. </param>
		/// <returns>An <see cref="T:System.Drawing.Image" /> associated with this <see cref="T:System.Drawing.ToolboxBitmapAttribute" /> object.</returns>
		// Token: 0x0600056E RID: 1390 RVA: 0x00004667 File Offset: 0x00002867
		public Image GetImage(Type type, string imgName, bool large)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns an <see cref="T:System.Drawing.Image" /> object based on a bitmap resource that is embedded in an assembly.</summary>
		/// <param name="t">This method searches for an embedded bitmap resource in the assembly that defines the type specified by the t parameter. For example, if you pass typeof(ControlA) to the t parameter, then this method searches the assembly that defines ControlA. </param>
		/// <param name="imageName">The name of the embedded bitmap resource. </param>
		/// <param name="large">Specifies whether this method returns a large image (true)or a small image (false). The small image is 16 by 16, and the large image is 32 x 32. </param>
		/// <returns>An <see cref="T:System.Drawing.Image" /> object based on the retrieved bitmap.</returns>
		// Token: 0x0600056F RID: 1391 RVA: 0x00004667 File Offset: 0x00002867
		public static Image GetImageFromResource(Type t, string imageName, bool large)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>A <see cref="T:System.Drawing.ToolboxBitmapAttribute" /> object that has its small image and its large image set to <see langword="null" />.</summary>
		// Token: 0x04000328 RID: 808
		public static readonly ToolboxBitmapAttribute Default;
	}
}
