﻿using System;
using System.Security.Permissions;

namespace System.Drawing
{
	/// <summary>Defines methods for obtaining and releasing an existing handle to a Windows device context.</summary>
	// Token: 0x02000068 RID: 104
	public interface IDeviceContext : IDisposable
	{
		/// <summary>Returns the handle to a Windows device context.</summary>
		/// <returns>An <see cref="T:System.IntPtr" /> representing the handle of a device context.</returns>
		// Token: 0x06000565 RID: 1381
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		[SecurityPermission(SecurityAction.InheritanceDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		IntPtr GetHdc();

		/// <summary>Releases the handle of a Windows device context.</summary>
		// Token: 0x06000566 RID: 1382
		[SecurityPermission(SecurityAction.InheritanceDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		void ReleaseHdc();
	}
}
