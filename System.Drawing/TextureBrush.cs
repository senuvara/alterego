﻿using System;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Unity;

namespace System.Drawing
{
	/// <summary>Each property of the <see cref="T:System.Drawing.TextureBrush" /> class is a <see cref="T:System.Drawing.Brush" /> object that uses an image to fill the interior of a shape. This class cannot be inherited.</summary>
	// Token: 0x02000089 RID: 137
	public sealed class TextureBrush : Brush
	{
		/// <summary>Initializes a new <see cref="T:System.Drawing.TextureBrush" /> object that uses the specified image.</summary>
		/// <param name="bitmap">The <see cref="T:System.Drawing.Image" /> object with which this <see cref="T:System.Drawing.TextureBrush" /> object fills interiors. </param>
		// Token: 0x0600079C RID: 1948 RVA: 0x00004644 File Offset: 0x00002844
		public TextureBrush(Image bitmap)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.TextureBrush" /> object that uses the specified image and wrap mode.</summary>
		/// <param name="image">The <see cref="T:System.Drawing.Image" /> object with which this <see cref="T:System.Drawing.TextureBrush" /> object fills interiors. </param>
		/// <param name="wrapMode">A <see cref="T:System.Drawing.Drawing2D.WrapMode" /> enumeration that specifies how this <see cref="T:System.Drawing.TextureBrush" /> object is tiled. </param>
		// Token: 0x0600079D RID: 1949 RVA: 0x00004644 File Offset: 0x00002844
		public TextureBrush(Image image, WrapMode wrapMode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.TextureBrush" /> object that uses the specified image, wrap mode, and bounding rectangle.</summary>
		/// <param name="image">The <see cref="T:System.Drawing.Image" /> object with which this <see cref="T:System.Drawing.TextureBrush" /> object fills interiors. </param>
		/// <param name="wrapMode">A <see cref="T:System.Drawing.Drawing2D.WrapMode" /> enumeration that specifies how this <see cref="T:System.Drawing.TextureBrush" /> object is tiled. </param>
		/// <param name="dstRect">A <see cref="T:System.Drawing.Rectangle" /> structure that represents the bounding rectangle for this <see cref="T:System.Drawing.TextureBrush" /> object. </param>
		// Token: 0x0600079E RID: 1950 RVA: 0x00004644 File Offset: 0x00002844
		public TextureBrush(Image image, WrapMode wrapMode, Rectangle dstRect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.TextureBrush" /> object that uses the specified image, wrap mode, and bounding rectangle.</summary>
		/// <param name="image">The <see cref="T:System.Drawing.Image" /> object with which this <see cref="T:System.Drawing.TextureBrush" /> object fills interiors. </param>
		/// <param name="wrapMode">A <see cref="T:System.Drawing.Drawing2D.WrapMode" /> enumeration that specifies how this <see cref="T:System.Drawing.TextureBrush" /> object is tiled. </param>
		/// <param name="dstRect">A <see cref="T:System.Drawing.RectangleF" /> structure that represents the bounding rectangle for this <see cref="T:System.Drawing.TextureBrush" /> object. </param>
		// Token: 0x0600079F RID: 1951 RVA: 0x00004644 File Offset: 0x00002844
		public TextureBrush(Image image, WrapMode wrapMode, RectangleF dstRect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.TextureBrush" /> object that uses the specified image and bounding rectangle.</summary>
		/// <param name="image">The <see cref="T:System.Drawing.Image" /> object with which this <see cref="T:System.Drawing.TextureBrush" /> object fills interiors. </param>
		/// <param name="dstRect">A <see cref="T:System.Drawing.Rectangle" /> structure that represents the bounding rectangle for this <see cref="T:System.Drawing.TextureBrush" /> object. </param>
		// Token: 0x060007A0 RID: 1952 RVA: 0x00004644 File Offset: 0x00002844
		public TextureBrush(Image image, Rectangle dstRect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.TextureBrush" /> object that uses the specified image, bounding rectangle, and image attributes.</summary>
		/// <param name="image">The <see cref="T:System.Drawing.Image" /> object with which this <see cref="T:System.Drawing.TextureBrush" /> object fills interiors. </param>
		/// <param name="dstRect">A <see cref="T:System.Drawing.Rectangle" /> structure that represents the bounding rectangle for this <see cref="T:System.Drawing.TextureBrush" /> object. </param>
		/// <param name="imageAttr">An <see cref="T:System.Drawing.Imaging.ImageAttributes" /> object that contains additional information about the image used by this <see cref="T:System.Drawing.TextureBrush" /> object. </param>
		// Token: 0x060007A1 RID: 1953 RVA: 0x00004644 File Offset: 0x00002844
		public TextureBrush(Image image, Rectangle dstRect, ImageAttributes imageAttr)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.TextureBrush" /> object that uses the specified image and bounding rectangle.</summary>
		/// <param name="image">The <see cref="T:System.Drawing.Image" /> object with which this <see cref="T:System.Drawing.TextureBrush" /> object fills interiors. </param>
		/// <param name="dstRect">A <see cref="T:System.Drawing.RectangleF" /> structure that represents the bounding rectangle for this <see cref="T:System.Drawing.TextureBrush" /> object. </param>
		// Token: 0x060007A2 RID: 1954 RVA: 0x00004644 File Offset: 0x00002844
		public TextureBrush(Image image, RectangleF dstRect)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.TextureBrush" /> object that uses the specified image, bounding rectangle, and image attributes.</summary>
		/// <param name="image">The <see cref="T:System.Drawing.Image" /> object with which this <see cref="T:System.Drawing.TextureBrush" /> object fills interiors. </param>
		/// <param name="dstRect">A <see cref="T:System.Drawing.RectangleF" /> structure that represents the bounding rectangle for this <see cref="T:System.Drawing.TextureBrush" /> object. </param>
		/// <param name="imageAttr">An <see cref="T:System.Drawing.Imaging.ImageAttributes" /> object that contains additional information about the image used by this <see cref="T:System.Drawing.TextureBrush" /> object. </param>
		// Token: 0x060007A3 RID: 1955 RVA: 0x00004644 File Offset: 0x00002844
		public TextureBrush(Image image, RectangleF dstRect, ImageAttributes imageAttr)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the <see cref="T:System.Drawing.Image" /> object associated with this <see cref="T:System.Drawing.TextureBrush" /> object.</summary>
		/// <returns>An <see cref="T:System.Drawing.Image" /> object that represents the image with which this <see cref="T:System.Drawing.TextureBrush" /> object fills shapes.</returns>
		// Token: 0x1700032F RID: 815
		// (get) Token: 0x060007A4 RID: 1956 RVA: 0x00004667 File Offset: 0x00002867
		public Image Image
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets a copy of the <see cref="T:System.Drawing.Drawing2D.Matrix" /> object that defines a local geometric transformation for the image associated with this <see cref="T:System.Drawing.TextureBrush" /> object.</summary>
		/// <returns>A copy of the <see cref="T:System.Drawing.Drawing2D.Matrix" /> object that defines a geometric transformation that applies only to fills drawn by using this <see cref="T:System.Drawing.TextureBrush" /> object.</returns>
		// Token: 0x17000330 RID: 816
		// (get) Token: 0x060007A5 RID: 1957 RVA: 0x00004667 File Offset: 0x00002867
		// (set) Token: 0x060007A6 RID: 1958 RVA: 0x00004644 File Offset: 0x00002844
		public Matrix Transform
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Drawing.Drawing2D.WrapMode" /> enumeration that indicates the wrap mode for this <see cref="T:System.Drawing.TextureBrush" /> object.</summary>
		/// <returns>A <see cref="T:System.Drawing.Drawing2D.WrapMode" /> enumeration that specifies how fills drawn by using this <see cref="T:System.Drawing.Drawing2D.LinearGradientBrush" /> object are tiled.</returns>
		// Token: 0x17000331 RID: 817
		// (get) Token: 0x060007A7 RID: 1959 RVA: 0x00006628 File Offset: 0x00004828
		// (set) Token: 0x060007A8 RID: 1960 RVA: 0x00004644 File Offset: 0x00002844
		public WrapMode WrapMode
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return WrapMode.Tile;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Creates an exact copy of this <see cref="T:System.Drawing.TextureBrush" /> object.</summary>
		/// <returns>The <see cref="T:System.Drawing.TextureBrush" /> object this method creates, cast as an <see cref="T:System.Object" /> object.</returns>
		// Token: 0x060007A9 RID: 1961 RVA: 0x00004667 File Offset: 0x00002867
		public override object Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Multiplies the <see cref="T:System.Drawing.Drawing2D.Matrix" /> object that represents the local geometric transformation of this <see cref="T:System.Drawing.TextureBrush" /> object by the specified <see cref="T:System.Drawing.Drawing2D.Matrix" /> object by prepending the specified <see cref="T:System.Drawing.Drawing2D.Matrix" /> object.</summary>
		/// <param name="matrix">The <see cref="T:System.Drawing.Drawing2D.Matrix" /> object by which to multiply the geometric transformation. </param>
		// Token: 0x060007AA RID: 1962 RVA: 0x00004644 File Offset: 0x00002844
		public void MultiplyTransform(Matrix matrix)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Multiplies the <see cref="T:System.Drawing.Drawing2D.Matrix" /> object that represents the local geometric transformation of this <see cref="T:System.Drawing.TextureBrush" /> object by the specified <see cref="T:System.Drawing.Drawing2D.Matrix" /> object in the specified order.</summary>
		/// <param name="matrix">The <see cref="T:System.Drawing.Drawing2D.Matrix" /> object by which to multiply the geometric transformation. </param>
		/// <param name="order">A <see cref="T:System.Drawing.Drawing2D.MatrixOrder" /> enumeration that specifies the order in which to multiply the two matrices. </param>
		// Token: 0x060007AB RID: 1963 RVA: 0x00004644 File Offset: 0x00002844
		public void MultiplyTransform(Matrix matrix, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Resets the <see langword="Transform" /> property of this <see cref="T:System.Drawing.TextureBrush" /> object to identity.</summary>
		// Token: 0x060007AC RID: 1964 RVA: 0x00004644 File Offset: 0x00002844
		public void ResetTransform()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Rotates the local geometric transformation of this <see cref="T:System.Drawing.TextureBrush" /> object by the specified amount. This method prepends the rotation to the transformation.</summary>
		/// <param name="angle">The angle of rotation. </param>
		// Token: 0x060007AD RID: 1965 RVA: 0x00004644 File Offset: 0x00002844
		public void RotateTransform(float angle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Rotates the local geometric transformation of this <see cref="T:System.Drawing.TextureBrush" /> object by the specified amount in the specified order.</summary>
		/// <param name="angle">The angle of rotation. </param>
		/// <param name="order">A <see cref="T:System.Drawing.Drawing2D.MatrixOrder" /> enumeration that specifies whether to append or prepend the rotation matrix. </param>
		// Token: 0x060007AE RID: 1966 RVA: 0x00004644 File Offset: 0x00002844
		public void RotateTransform(float angle, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Scales the local geometric transformation of this <see cref="T:System.Drawing.TextureBrush" /> object by the specified amounts. This method prepends the scaling matrix to the transformation.</summary>
		/// <param name="sx">The amount by which to scale the transformation in the x direction. </param>
		/// <param name="sy">The amount by which to scale the transformation in the y direction. </param>
		// Token: 0x060007AF RID: 1967 RVA: 0x00004644 File Offset: 0x00002844
		public void ScaleTransform(float sx, float sy)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Scales the local geometric transformation of this <see cref="T:System.Drawing.TextureBrush" /> object by the specified amounts in the specified order.</summary>
		/// <param name="sx">The amount by which to scale the transformation in the x direction. </param>
		/// <param name="sy">The amount by which to scale the transformation in the y direction. </param>
		/// <param name="order">A <see cref="T:System.Drawing.Drawing2D.MatrixOrder" /> enumeration that specifies whether to append or prepend the scaling matrix. </param>
		// Token: 0x060007B0 RID: 1968 RVA: 0x00004644 File Offset: 0x00002844
		public void ScaleTransform(float sx, float sy, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Translates the local geometric transformation of this <see cref="T:System.Drawing.TextureBrush" /> object by the specified dimensions. This method prepends the translation to the transformation.</summary>
		/// <param name="dx">The dimension by which to translate the transformation in the x direction. </param>
		/// <param name="dy">The dimension by which to translate the transformation in the y direction. </param>
		// Token: 0x060007B1 RID: 1969 RVA: 0x00004644 File Offset: 0x00002844
		public void TranslateTransform(float dx, float dy)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Translates the local geometric transformation of this <see cref="T:System.Drawing.TextureBrush" /> object by the specified dimensions in the specified order.</summary>
		/// <param name="dx">The dimension by which to translate the transformation in the x direction. </param>
		/// <param name="dy">The dimension by which to translate the transformation in the y direction. </param>
		/// <param name="order">The order (prepend or append) in which to apply the translation. </param>
		// Token: 0x060007B2 RID: 1970 RVA: 0x00004644 File Offset: 0x00002844
		public void TranslateTransform(float dx, float dy, MatrixOrder order)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
