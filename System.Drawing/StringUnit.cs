﻿using System;

namespace System.Drawing
{
	/// <summary>Specifies the units of measure for a text string.</summary>
	// Token: 0x02000083 RID: 131
	public enum StringUnit
	{
		/// <summary>Specifies the device unit as the unit of measure.</summary>
		// Token: 0x0400032A RID: 810
		Display = 1,
		/// <summary>Specifies 1/300 of an inch as the unit of measure.</summary>
		// Token: 0x0400032B RID: 811
		Document = 5,
		/// <summary>Specifies a printer's em size of 32 as the unit of measure.</summary>
		// Token: 0x0400032C RID: 812
		Em = 32,
		/// <summary>Specifies an inch as the unit of measure.</summary>
		// Token: 0x0400032D RID: 813
		Inch = 4,
		/// <summary>Specifies a millimeter as the unit of measure </summary>
		// Token: 0x0400032E RID: 814
		Millimeter = 6,
		/// <summary>Specifies a pixel as the unit of measure.</summary>
		// Token: 0x0400032F RID: 815
		Pixel = 2,
		/// <summary>Specifies a printer's point (1/72 inch) as the unit of measure.</summary>
		// Token: 0x04000330 RID: 816
		Point,
		/// <summary>Specifies world units as the unit of measure.</summary>
		// Token: 0x04000331 RID: 817
		World = 0
	}
}
