﻿using System;
using Unity;

namespace System.Drawing
{
	/// <summary>Translates colors to and from GDI+ <see cref="T:System.Drawing.Color" /> structures. This class cannot be inherited.</summary>
	// Token: 0x0200007B RID: 123
	public sealed class ColorTranslator
	{
		// Token: 0x0600067B RID: 1659 RVA: 0x00004644 File Offset: 0x00002844
		internal ColorTranslator()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Translates an HTML color representation to a GDI+ <see cref="T:System.Drawing.Color" /> structure.</summary>
		/// <param name="htmlColor">The string representation of the Html color to translate. </param>
		/// <returns>The <see cref="T:System.Drawing.Color" /> structure that represents the translated HTML color or <see cref="F:System.Drawing.Color.Empty" /> if <paramref name="htmlColor" /> is <see langword="null" />.</returns>
		/// <exception cref="T:System.Exception">
		///         <paramref name="htmlColor" /> is not a valid HTML color name.</exception>
		// Token: 0x0600067C RID: 1660 RVA: 0x000061C8 File Offset: 0x000043C8
		public static Color FromHtml(string htmlColor)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(Color);
		}

		/// <summary>Translates an OLE color value to a GDI+ <see cref="T:System.Drawing.Color" /> structure.</summary>
		/// <param name="oleColor">The OLE color to translate. </param>
		/// <returns>The <see cref="T:System.Drawing.Color" /> structure that represents the translated OLE color.</returns>
		// Token: 0x0600067D RID: 1661 RVA: 0x000061E4 File Offset: 0x000043E4
		public static Color FromOle(int oleColor)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(Color);
		}

		/// <summary>Translates a Windows color value to a GDI+ <see cref="T:System.Drawing.Color" /> structure.</summary>
		/// <param name="win32Color">The Windows color to translate. </param>
		/// <returns>The <see cref="T:System.Drawing.Color" /> structure that represents the translated Windows color.</returns>
		// Token: 0x0600067E RID: 1662 RVA: 0x00006200 File Offset: 0x00004400
		public static Color FromWin32(int win32Color)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(Color);
		}

		/// <summary>Translates the specified <see cref="T:System.Drawing.Color" /> structure to an HTML string color representation.</summary>
		/// <param name="c">The <see cref="T:System.Drawing.Color" /> structure to translate. </param>
		/// <returns>The string that represents the HTML color.</returns>
		// Token: 0x0600067F RID: 1663 RVA: 0x00004667 File Offset: 0x00002867
		public static string ToHtml(Color c)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Translates the specified <see cref="T:System.Drawing.Color" /> structure to an OLE color.</summary>
		/// <param name="c">The <see cref="T:System.Drawing.Color" /> structure to translate. </param>
		/// <returns>The OLE color value.</returns>
		// Token: 0x06000680 RID: 1664 RVA: 0x0000621C File Offset: 0x0000441C
		public static int ToOle(Color c)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Translates the specified <see cref="T:System.Drawing.Color" /> structure to a Windows color.</summary>
		/// <param name="c">The <see cref="T:System.Drawing.Color" /> structure to translate. </param>
		/// <returns>The Windows color value.</returns>
		// Token: 0x06000681 RID: 1665 RVA: 0x00006238 File Offset: 0x00004438
		public static int ToWin32(Color c)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}
	}
}
