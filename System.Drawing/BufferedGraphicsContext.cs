﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Drawing
{
	/// <summary>Provides methods for creating graphics buffers that can be used for double buffering.</summary>
	// Token: 0x02000078 RID: 120
	public sealed class BufferedGraphicsContext : IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.BufferedGraphicsContext" /> class.</summary>
		// Token: 0x06000671 RID: 1649 RVA: 0x00004644 File Offset: 0x00002844
		public BufferedGraphicsContext()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the maximum size of the buffer to use.</summary>
		/// <returns>A <see cref="T:System.Drawing.Size" /> indicating the maximum size of the buffer dimensions.</returns>
		/// <exception cref="T:System.ArgumentException">The height or width of the size is less than or equal to zero. </exception>
		// Token: 0x1700022A RID: 554
		// (get) Token: 0x06000672 RID: 1650 RVA: 0x000061AC File Offset: 0x000043AC
		// (set) Token: 0x06000673 RID: 1651 RVA: 0x00004644 File Offset: 0x00002844
		public Size MaximumBuffer
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Size);
			}
			[UIPermission(SecurityAction.Demand, Window = UIPermissionWindow.AllWindows)]
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Creates a graphics buffer of the specified size using the pixel format of the specified <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="targetGraphics">The <see cref="T:System.Drawing.Graphics" /> to match the pixel format for the new buffer to. </param>
		/// <param name="targetRectangle">A <see cref="T:System.Drawing.Rectangle" /> indicating the size of the buffer to create. </param>
		/// <returns>A <see cref="T:System.Drawing.BufferedGraphics" /> that can be used to draw to a buffer of the specified dimensions.</returns>
		// Token: 0x06000674 RID: 1652 RVA: 0x00004667 File Offset: 0x00002867
		public BufferedGraphics Allocate(Graphics targetGraphics, Rectangle targetRectangle)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates a graphics buffer of the specified size using the pixel format of the specified <see cref="T:System.Drawing.Graphics" />.</summary>
		/// <param name="targetDC">An <see cref="T:System.IntPtr" /> to a device context to match the pixel format of the new buffer to. </param>
		/// <param name="targetRectangle">A <see cref="T:System.Drawing.Rectangle" /> indicating the size of the buffer to create. </param>
		/// <returns>A <see cref="T:System.Drawing.BufferedGraphics" /> that can be used to draw to a buffer of the specified dimensions.</returns>
		// Token: 0x06000675 RID: 1653 RVA: 0x00004667 File Offset: 0x00002867
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public BufferedGraphics Allocate(IntPtr targetDC, Rectangle targetRectangle)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Releases all resources used by the <see cref="T:System.Drawing.BufferedGraphicsContext" />.</summary>
		// Token: 0x06000676 RID: 1654 RVA: 0x00004644 File Offset: 0x00002844
		public void Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Disposes of the current graphics buffer, if a buffer has been allocated and has not yet been disposed.</summary>
		// Token: 0x06000677 RID: 1655 RVA: 0x00004644 File Offset: 0x00002844
		public void Invalidate()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
