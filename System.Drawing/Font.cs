﻿using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Unity;

namespace System.Drawing
{
	/// <summary>Defines a particular format for text, including font face, size, and style attributes. This class cannot be inherited.</summary>
	// Token: 0x02000059 RID: 89
	[Editor("System.Drawing.Design.FontEditor, System.Drawing.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
	[TypeConverter(typeof(FontConverter))]
	[ComVisible(true)]
	[Serializable]
	public sealed class Font : MarshalByRefObject, ICloneable, IDisposable, ISerializable
	{
		/// <summary>Initializes a new <see cref="T:System.Drawing.Font" /> that uses the specified existing <see cref="T:System.Drawing.Font" /> and <see cref="T:System.Drawing.FontStyle" /> enumeration.</summary>
		/// <param name="prototype">The existing <see cref="T:System.Drawing.Font" /> from which to create the new <see cref="T:System.Drawing.Font" />. </param>
		/// <param name="newStyle">The <see cref="T:System.Drawing.FontStyle" /> to apply to the new <see cref="T:System.Drawing.Font" />. Multiple values of the <see cref="T:System.Drawing.FontStyle" /> enumeration can be combined with the <see langword="OR" /> operator. </param>
		// Token: 0x060004D3 RID: 1235 RVA: 0x00004644 File Offset: 0x00002844
		public Font(Font prototype, FontStyle newStyle)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.Font" /> using a specified size. </summary>
		/// <param name="family">The <see cref="T:System.Drawing.FontFamily" /> of the new <see cref="T:System.Drawing.Font" />. </param>
		/// <param name="emSize">The em-size, in points, of the new font. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="emSize" /> is less than or equal to 0, evaluates to infinity, or is not a valid number. </exception>
		// Token: 0x060004D4 RID: 1236 RVA: 0x00004644 File Offset: 0x00002844
		public Font(FontFamily family, float emSize)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.Font" /> using a specified size and style. </summary>
		/// <param name="family">The <see cref="T:System.Drawing.FontFamily" /> of the new <see cref="T:System.Drawing.Font" />. </param>
		/// <param name="emSize">The em-size, in points, of the new font. </param>
		/// <param name="style">The <see cref="T:System.Drawing.FontStyle" /> of the new font. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="emSize" /> is less than or equal to 0, evaluates to infinity, or is not a valid number. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="family" /> is <see langword="null" />.</exception>
		// Token: 0x060004D5 RID: 1237 RVA: 0x00004644 File Offset: 0x00002844
		public Font(FontFamily family, float emSize, FontStyle style)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.Font" /> using a specified size, style, and unit.</summary>
		/// <param name="family">The <see cref="T:System.Drawing.FontFamily" /> of the new <see cref="T:System.Drawing.Font" />. </param>
		/// <param name="emSize">The em-size of the new font in the units specified by the <paramref name="unit" /> parameter. </param>
		/// <param name="style">The <see cref="T:System.Drawing.FontStyle" /> of the new font. </param>
		/// <param name="unit">The <see cref="T:System.Drawing.GraphicsUnit" /> of the new font. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="emSize" /> is less than or equal to 0, evaluates to infinity, or is not a valid number. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="family" /> is <see langword="null" />.</exception>
		// Token: 0x060004D6 RID: 1238 RVA: 0x00004644 File Offset: 0x00002844
		public Font(FontFamily family, float emSize, FontStyle style, GraphicsUnit unit)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.Font" /> using a specified size, style, unit, and character set.</summary>
		/// <param name="family">The <see cref="T:System.Drawing.FontFamily" /> of the new <see cref="T:System.Drawing.Font" />. </param>
		/// <param name="emSize">The em-size of the new font in the units specified by the <paramref name="unit" /> parameter. </param>
		/// <param name="style">The <see cref="T:System.Drawing.FontStyle" /> of the new font. </param>
		/// <param name="unit">The <see cref="T:System.Drawing.GraphicsUnit" /> of the new font. </param>
		/// <param name="gdiCharSet">A <see cref="T:System.Byte" /> that specifies a 
		///       GDI character set to use for the new font. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="emSize" /> is less than or equal to 0, evaluates to infinity, or is not a valid number. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="family" /> is <see langword="null" />.</exception>
		// Token: 0x060004D7 RID: 1239 RVA: 0x00004644 File Offset: 0x00002844
		public Font(FontFamily family, float emSize, FontStyle style, GraphicsUnit unit, byte gdiCharSet)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.Font" /> using a specified size, style, unit, and character set.</summary>
		/// <param name="family">The <see cref="T:System.Drawing.FontFamily" /> of the new <see cref="T:System.Drawing.Font" />. </param>
		/// <param name="emSize">The em-size of the new font in the units specified by the <paramref name="unit" /> parameter. </param>
		/// <param name="style">The <see cref="T:System.Drawing.FontStyle" /> of the new font. </param>
		/// <param name="unit">The <see cref="T:System.Drawing.GraphicsUnit" /> of the new font. </param>
		/// <param name="gdiCharSet">A <see cref="T:System.Byte" /> that specifies a 
		///       GDI character set to use for this font. </param>
		/// <param name="gdiVerticalFont">A Boolean value indicating whether the new font is derived from a GDI vertical font. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="emSize" /> is less than or equal to 0, evaluates to infinity, or is not a valid number. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="family" /> is <see langword="null " /></exception>
		// Token: 0x060004D8 RID: 1240 RVA: 0x00004644 File Offset: 0x00002844
		public Font(FontFamily family, float emSize, FontStyle style, GraphicsUnit unit, byte gdiCharSet, bool gdiVerticalFont)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.Font" /> using a specified size and unit. Sets the style to <see cref="F:System.Drawing.FontStyle.Regular" />.</summary>
		/// <param name="family">The <see cref="T:System.Drawing.FontFamily" /> of the new <see cref="T:System.Drawing.Font" />. </param>
		/// <param name="emSize">The em-size of the new font in the units specified by the <paramref name="unit" /> parameter. </param>
		/// <param name="unit">The <see cref="T:System.Drawing.GraphicsUnit" /> of the new font. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="family" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="emSize" /> is less than or equal to 0, evaluates to infinity, or is not a valid number. </exception>
		// Token: 0x060004D9 RID: 1241 RVA: 0x00004644 File Offset: 0x00002844
		public Font(FontFamily family, float emSize, GraphicsUnit unit)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.Font" /> using a specified size. </summary>
		/// <param name="familyName">A string representation of the <see cref="T:System.Drawing.FontFamily" /> for the new <see cref="T:System.Drawing.Font" />. </param>
		/// <param name="emSize">The em-size, in points, of the new font. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="emSize" /> is less than or equal to 0, evaluates to infinity or is not a valid number. </exception>
		// Token: 0x060004DA RID: 1242 RVA: 0x00004644 File Offset: 0x00002844
		public Font(string familyName, float emSize)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.Font" /> using a specified size and style. </summary>
		/// <param name="familyName">A string representation of the <see cref="T:System.Drawing.FontFamily" /> for the new <see cref="T:System.Drawing.Font" />. </param>
		/// <param name="emSize">The em-size, in points, of the new font. </param>
		/// <param name="style">The <see cref="T:System.Drawing.FontStyle" /> of the new font. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="emSize" /> is less than or equal to 0, evaluates to infinity, or is not a valid number. </exception>
		// Token: 0x060004DB RID: 1243 RVA: 0x00004644 File Offset: 0x00002844
		public Font(string familyName, float emSize, FontStyle style)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.Font" /> using a specified size, style, and unit.</summary>
		/// <param name="familyName">A string representation of the <see cref="T:System.Drawing.FontFamily" /> for the new <see cref="T:System.Drawing.Font" />. </param>
		/// <param name="emSize">The em-size of the new font in the units specified by the <paramref name="unit" /> parameter. </param>
		/// <param name="style">The <see cref="T:System.Drawing.FontStyle" /> of the new font. </param>
		/// <param name="unit">The <see cref="T:System.Drawing.GraphicsUnit" /> of the new font. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="emSize" /> is less than or equal to 0, evaluates to infinity or is not a valid number. </exception>
		// Token: 0x060004DC RID: 1244 RVA: 0x00004644 File Offset: 0x00002844
		public Font(string familyName, float emSize, FontStyle style, GraphicsUnit unit)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.Font" /> using a specified size, style, unit, and character set.</summary>
		/// <param name="familyName">A string representation of the <see cref="T:System.Drawing.FontFamily" /> for the new <see cref="T:System.Drawing.Font" />. </param>
		/// <param name="emSize">The em-size of the new font in the units specified by the <paramref name="unit" /> parameter. </param>
		/// <param name="style">The <see cref="T:System.Drawing.FontStyle" /> of the new font. </param>
		/// <param name="unit">The <see cref="T:System.Drawing.GraphicsUnit" /> of the new font. </param>
		/// <param name="gdiCharSet">A <see cref="T:System.Byte" /> that specifies a GDI character set to use for this font. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="emSize" /> is less than or equal to 0, evaluates to infinity, or is not a valid number. </exception>
		// Token: 0x060004DD RID: 1245 RVA: 0x00004644 File Offset: 0x00002844
		public Font(string familyName, float emSize, FontStyle style, GraphicsUnit unit, byte gdiCharSet)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.Font" /> using the specified size, style, unit, and character set.</summary>
		/// <param name="familyName">A string representation of the <see cref="T:System.Drawing.FontFamily" /> for the new <see cref="T:System.Drawing.Font" />. </param>
		/// <param name="emSize">The em-size of the new font in the units specified by the <paramref name="unit" /> parameter. </param>
		/// <param name="style">The <see cref="T:System.Drawing.FontStyle" /> of the new font. </param>
		/// <param name="unit">The <see cref="T:System.Drawing.GraphicsUnit" /> of the new font. </param>
		/// <param name="gdiCharSet">A <see cref="T:System.Byte" /> that specifies a GDI character set to use for this font. </param>
		/// <param name="gdiVerticalFont">A Boolean value indicating whether the new <see cref="T:System.Drawing.Font" /> is derived from a GDI vertical font. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="emSize" /> is less than or equal to 0, evaluates to infinity, or is not a valid number. </exception>
		// Token: 0x060004DE RID: 1246 RVA: 0x00004644 File Offset: 0x00002844
		public Font(string familyName, float emSize, FontStyle style, GraphicsUnit unit, byte gdiCharSet, bool gdiVerticalFont)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Drawing.Font" /> using a specified size and unit. The style is set to <see cref="F:System.Drawing.FontStyle.Regular" />.</summary>
		/// <param name="familyName">A string representation of the <see cref="T:System.Drawing.FontFamily" /> for the new <see cref="T:System.Drawing.Font" />. </param>
		/// <param name="emSize">The em-size of the new font in the units specified by the <paramref name="unit" /> parameter. </param>
		/// <param name="unit">The <see cref="T:System.Drawing.GraphicsUnit" /> of the new font. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="emSize" /> is less than or equal to 0, evaluates to infinity, or is not a valid number. </exception>
		// Token: 0x060004DF RID: 1247 RVA: 0x00004644 File Offset: 0x00002844
		public Font(string familyName, float emSize, GraphicsUnit unit)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a value that indicates whether this <see cref="T:System.Drawing.Font" /> is bold.</summary>
		/// <returns>
		///     <see langword="true" /> if this <see cref="T:System.Drawing.Font" /> is bold; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700015E RID: 350
		// (get) Token: 0x060004E0 RID: 1248 RVA: 0x00005B8C File Offset: 0x00003D8C
		public bool Bold
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets the <see cref="T:System.Drawing.FontFamily" /> associated with this <see cref="T:System.Drawing.Font" />.</summary>
		/// <returns>The <see cref="T:System.Drawing.FontFamily" /> associated with this <see cref="T:System.Drawing.Font" />.</returns>
		// Token: 0x1700015F RID: 351
		// (get) Token: 0x060004E1 RID: 1249 RVA: 0x00004667 File Offset: 0x00002867
		public FontFamily FontFamily
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a byte value that specifies the GDI character set that this <see cref="T:System.Drawing.Font" /> uses.</summary>
		/// <returns>A byte value that specifies the GDI character set that this <see cref="T:System.Drawing.Font" /> uses. The default is 1.</returns>
		// Token: 0x17000160 RID: 352
		// (get) Token: 0x060004E2 RID: 1250 RVA: 0x00005BA8 File Offset: 0x00003DA8
		public byte GdiCharSet
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets a Boolean value that indicates whether this <see cref="T:System.Drawing.Font" /> is derived from a GDI vertical font.</summary>
		/// <returns>
		///     <see langword="true" /> if this <see cref="T:System.Drawing.Font" /> is derived from a GDI vertical font; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000161 RID: 353
		// (get) Token: 0x060004E3 RID: 1251 RVA: 0x00005BC4 File Offset: 0x00003DC4
		public bool GdiVerticalFont
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets the line spacing of this font.</summary>
		/// <returns>The line spacing, in pixels, of this font. </returns>
		// Token: 0x17000162 RID: 354
		// (get) Token: 0x060004E4 RID: 1252 RVA: 0x00005BE0 File Offset: 0x00003DE0
		public int Height
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets a value indicating whether the font is a member of <see cref="T:System.Drawing.SystemFonts" />. </summary>
		/// <returns>
		///     <see langword="true" /> if the font is a member of <see cref="T:System.Drawing.SystemFonts" />; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000163 RID: 355
		// (get) Token: 0x060004E5 RID: 1253 RVA: 0x00005BFC File Offset: 0x00003DFC
		public bool IsSystemFont
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value that indicates whether this font has the italic style applied.</summary>
		/// <returns>
		///     <see langword="true" /> to indicate this font has the italic style applied; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000164 RID: 356
		// (get) Token: 0x060004E6 RID: 1254 RVA: 0x00005C18 File Offset: 0x00003E18
		public bool Italic
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets the face name of this <see cref="T:System.Drawing.Font" />.</summary>
		/// <returns>A string representation of the face name of this <see cref="T:System.Drawing.Font" />.</returns>
		// Token: 0x17000165 RID: 357
		// (get) Token: 0x060004E7 RID: 1255 RVA: 0x00004667 File Offset: 0x00002867
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the name of the font originally specified.</summary>
		/// <returns>The string representing the name of the font originally specified.</returns>
		// Token: 0x17000166 RID: 358
		// (get) Token: 0x060004E8 RID: 1256 RVA: 0x00004667 File Offset: 0x00002867
		public string OriginalFontName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the em-size of this <see cref="T:System.Drawing.Font" /> measured in the units specified by the <see cref="P:System.Drawing.Font.Unit" /> property.</summary>
		/// <returns>The em-size of this <see cref="T:System.Drawing.Font" />.</returns>
		// Token: 0x17000167 RID: 359
		// (get) Token: 0x060004E9 RID: 1257 RVA: 0x00005C34 File Offset: 0x00003E34
		public float Size
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
		}

		/// <summary>Gets the em-size, in points, of this <see cref="T:System.Drawing.Font" />.</summary>
		/// <returns>The em-size, in points, of this <see cref="T:System.Drawing.Font" />.</returns>
		// Token: 0x17000168 RID: 360
		// (get) Token: 0x060004EA RID: 1258 RVA: 0x00005C50 File Offset: 0x00003E50
		public float SizeInPoints
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0f;
			}
		}

		/// <summary>Gets a value that indicates whether this <see cref="T:System.Drawing.Font" /> specifies a horizontal line through the font.</summary>
		/// <returns>
		///     <see langword="true" /> if this <see cref="T:System.Drawing.Font" /> has a horizontal line through it; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000169 RID: 361
		// (get) Token: 0x060004EB RID: 1259 RVA: 0x00005C6C File Offset: 0x00003E6C
		public bool Strikeout
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets style information for this <see cref="T:System.Drawing.Font" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.FontStyle" /> enumeration that contains style information for this <see cref="T:System.Drawing.Font" />.</returns>
		// Token: 0x1700016A RID: 362
		// (get) Token: 0x060004EC RID: 1260 RVA: 0x00005C88 File Offset: 0x00003E88
		public FontStyle Style
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return FontStyle.Regular;
			}
		}

		/// <summary>Gets the name of the system font if the <see cref="P:System.Drawing.Font.IsSystemFont" /> property returns <see langword="true" />.</summary>
		/// <returns>The name of the system font, if <see cref="P:System.Drawing.Font.IsSystemFont" /> returns <see langword="true" />; otherwise, an empty string ("").</returns>
		// Token: 0x1700016B RID: 363
		// (get) Token: 0x060004ED RID: 1261 RVA: 0x00004667 File Offset: 0x00002867
		public string SystemFontName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a value that indicates whether this <see cref="T:System.Drawing.Font" /> is underlined.</summary>
		/// <returns>
		///     <see langword="true" /> if this <see cref="T:System.Drawing.Font" /> is underlined; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700016C RID: 364
		// (get) Token: 0x060004EE RID: 1262 RVA: 0x00005CA4 File Offset: 0x00003EA4
		public bool Underline
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets the unit of measure for this <see cref="T:System.Drawing.Font" />.</summary>
		/// <returns>A <see cref="T:System.Drawing.GraphicsUnit" /> that represents the unit of measure for this <see cref="T:System.Drawing.Font" />.</returns>
		// Token: 0x1700016D RID: 365
		// (get) Token: 0x060004EF RID: 1263 RVA: 0x00005CC0 File Offset: 0x00003EC0
		public GraphicsUnit Unit
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return GraphicsUnit.World;
			}
		}

		/// <summary>Creates an exact copy of this <see cref="T:System.Drawing.Font" />.</summary>
		/// <returns>The <see cref="T:System.Drawing.Font" /> this method creates, cast as an <see cref="T:System.Object" />.</returns>
		// Token: 0x060004F0 RID: 1264 RVA: 0x00004667 File Offset: 0x00002867
		public object Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Releases all resources used by this <see cref="T:System.Drawing.Font" />.</summary>
		// Token: 0x060004F1 RID: 1265 RVA: 0x00004644 File Offset: 0x00002844
		public void Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a <see cref="T:System.Drawing.Font" /> from the specified Windows handle to a device context.</summary>
		/// <param name="hdc">A handle to a device context. </param>
		/// <returns>The <see cref="T:System.Drawing.Font" /> this method creates.</returns>
		/// <exception cref="T:System.ArgumentException">The font for the specified device context is not a TrueType font.</exception>
		// Token: 0x060004F2 RID: 1266 RVA: 0x00004667 File Offset: 0x00002867
		public static Font FromHdc(IntPtr hdc)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates a <see cref="T:System.Drawing.Font" /> from the specified Windows handle.</summary>
		/// <param name="hfont">A Windows handle to a GDI font. </param>
		/// <returns>The <see cref="T:System.Drawing.Font" /> this method creates.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="hfont" /> points to an object that is not a TrueType font.</exception>
		// Token: 0x060004F3 RID: 1267 RVA: 0x00004667 File Offset: 0x00002867
		public static Font FromHfont(IntPtr hfont)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates a <see cref="T:System.Drawing.Font" /> from the specified GDI logical font (LOGFONT) structure.</summary>
		/// <param name="lf">An <see cref="T:System.Object" /> that represents the GDI <see langword="LOGFONT" /> structure from which to create the <see cref="T:System.Drawing.Font" />. </param>
		/// <returns>The <see cref="T:System.Drawing.Font" /> that this method creates.</returns>
		// Token: 0x060004F4 RID: 1268 RVA: 0x00004667 File Offset: 0x00002867
		public static Font FromLogFont(object lf)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates a <see cref="T:System.Drawing.Font" /> from the specified GDI logical font (LOGFONT) structure.</summary>
		/// <param name="lf">An <see cref="T:System.Object" /> that represents the GDI <see langword="LOGFONT" /> structure from which to create the <see cref="T:System.Drawing.Font" />. </param>
		/// <param name="hdc">A handle to a device context that contains additional information about the <paramref name="lf" /> structure. </param>
		/// <returns>The <see cref="T:System.Drawing.Font" /> that this method creates.</returns>
		/// <exception cref="T:System.ArgumentException">The font is not a TrueType font.</exception>
		// Token: 0x060004F5 RID: 1269 RVA: 0x00004667 File Offset: 0x00002867
		public static Font FromLogFont(object lf, IntPtr hdc)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the line spacing, in pixels, of this font. </summary>
		/// <returns>The line spacing, in pixels, of this font.</returns>
		// Token: 0x060004F6 RID: 1270 RVA: 0x00005CDC File Offset: 0x00003EDC
		public float GetHeight()
		{
			ThrowStub.ThrowNotSupportedException();
			return 0f;
		}

		/// <summary>Returns the line spacing, in the current unit of a specified <see cref="T:System.Drawing.Graphics" />, of this font. </summary>
		/// <param name="graphics">A <see cref="T:System.Drawing.Graphics" /> that holds the vertical resolution, in dots per inch, of the display device as well as settings for page unit and page scale. </param>
		/// <returns>The line spacing, in pixels, of this font.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="graphics" /> is <see langword="null" />.</exception>
		// Token: 0x060004F7 RID: 1271 RVA: 0x00005CF8 File Offset: 0x00003EF8
		public float GetHeight(Graphics graphics)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0f;
		}

		/// <summary>Returns the height, in pixels, of this <see cref="T:System.Drawing.Font" /> when drawn to a device with the specified vertical resolution.</summary>
		/// <param name="dpi">The vertical resolution, in dots per inch, used to calculate the height of the font. </param>
		/// <returns>The height, in pixels, of this <see cref="T:System.Drawing.Font" />.</returns>
		// Token: 0x060004F8 RID: 1272 RVA: 0x00005D14 File Offset: 0x00003F14
		public float GetHeight(float dpi)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0f;
		}

		/// <summary>Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with the data needed to serialize the target object.</summary>
		/// <param name="si">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data.</param>
		/// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this serialization.</param>
		// Token: 0x060004F9 RID: 1273 RVA: 0x00004644 File Offset: 0x00002844
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
		void ISerializable.GetObjectData(SerializationInfo si, StreamingContext context)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns a handle to this <see cref="T:System.Drawing.Font" />.</summary>
		/// <returns>A Windows handle to this <see cref="T:System.Drawing.Font" />.</returns>
		/// <exception cref="T:System.ComponentModel.Win32Exception">The operation was unsuccessful.</exception>
		// Token: 0x060004FA RID: 1274 RVA: 0x00005D30 File Offset: 0x00003F30
		public IntPtr ToHfont()
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Creates a GDI logical font (LOGFONT) structure from this <see cref="T:System.Drawing.Font" />.</summary>
		/// <param name="logFont">An <see cref="T:System.Object" /> to represent the <see langword="LOGFONT" /> structure that this method creates. </param>
		// Token: 0x060004FB RID: 1275 RVA: 0x00004644 File Offset: 0x00002844
		public void ToLogFont(object logFont)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a GDI logical font (LOGFONT) structure from this <see cref="T:System.Drawing.Font" />.</summary>
		/// <param name="logFont">An <see cref="T:System.Object" /> to represent the <see langword="LOGFONT" /> structure that this method creates. </param>
		/// <param name="graphics">A <see cref="T:System.Drawing.Graphics" /> that provides additional information for the <see langword="LOGFONT" /> structure. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="graphics" /> is <see langword="null" />.</exception>
		// Token: 0x060004FC RID: 1276 RVA: 0x00004644 File Offset: 0x00002844
		public void ToLogFont(object logFont, Graphics graphics)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
