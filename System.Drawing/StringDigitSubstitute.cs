﻿using System;

namespace System.Drawing
{
	/// <summary>The <see cref="T:System.Drawing.StringDigitSubstitute" /> enumeration specifies how to substitute digits in a string according to a user's locale or language.</summary>
	// Token: 0x02000023 RID: 35
	public enum StringDigitSubstitute
	{
		/// <summary>Specifies substitution digits that correspond with the official national language of the user's locale.</summary>
		// Token: 0x0400021A RID: 538
		National = 2,
		/// <summary>Specifies to disable substitutions.</summary>
		// Token: 0x0400021B RID: 539
		None = 1,
		/// <summary>Specifies substitution digits that correspond with the user's native script or language, which may be different from the official national language of the user's locale.</summary>
		// Token: 0x0400021C RID: 540
		Traditional = 3,
		/// <summary>Specifies a user-defined substitution scheme.</summary>
		// Token: 0x0400021D RID: 541
		User = 0
	}
}
