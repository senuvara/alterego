﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Drawing
{
	/// <summary>Converts <see cref="T:System.Drawing.Font" /> objects from one data type to another. </summary>
	// Token: 0x0200005A RID: 90
	public class FontConverter : TypeConverter
	{
		/// <summary>Initializes a new <see cref="T:System.Drawing.FontConverter" /> object.</summary>
		// Token: 0x060004FD RID: 1277 RVA: 0x00004644 File Offset: 0x00002844
		public FontConverter()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>
		///     <see cref="T:System.Drawing.FontConverter.FontNameConverter" /> is a type converter that is used to convert a font name to and from various other representations.</summary>
		// Token: 0x0200005B RID: 91
		public sealed class FontNameConverter : TypeConverter, IDisposable
		{
			/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.FontConverter.FontNameConverter" /> class. </summary>
			// Token: 0x060004FE RID: 1278 RVA: 0x00004644 File Offset: 0x00002844
			public FontNameConverter()
			{
				ThrowStub.ThrowNotSupportedException();
			}

			/// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
			// Token: 0x060004FF RID: 1279 RVA: 0x00004644 File Offset: 0x00002844
			void IDisposable.Dispose()
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Converts font units to and from other unit types.</summary>
		// Token: 0x0200005C RID: 92
		public class FontUnitConverter : EnumConverter
		{
			/// <summary>Initializes a new instance of the <see cref="T:System.Drawing.FontConverter.FontUnitConverter" /> class.</summary>
			// Token: 0x06000500 RID: 1280 RVA: 0x00004644 File Offset: 0x00002844
			public FontUnitConverter()
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
