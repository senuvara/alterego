﻿using System;
using Unity;

namespace System.Drawing
{
	/// <summary>Each property of the <see cref="T:System.Drawing.SystemIcons" /> class is an <see cref="T:System.Drawing.Icon" /> object for Windows system-wide icons. This class cannot be inherited.</summary>
	// Token: 0x02000087 RID: 135
	public sealed class SystemIcons
	{
		// Token: 0x0600076E RID: 1902 RVA: 0x00004644 File Offset: 0x00002844
		internal SystemIcons()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets an <see cref="T:System.Drawing.Icon" /> object that contains the default application icon (WIN32: IDI_APPLICATION).</summary>
		/// <returns>An <see cref="T:System.Drawing.Icon" /> object that contains the default application icon.</returns>
		// Token: 0x17000304 RID: 772
		// (get) Token: 0x0600076F RID: 1903 RVA: 0x00004667 File Offset: 0x00002867
		public static Icon Application
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets an <see cref="T:System.Drawing.Icon" /> object that contains the system asterisk icon (WIN32: IDI_ASTERISK).</summary>
		/// <returns>An <see cref="T:System.Drawing.Icon" /> object that contains the system asterisk icon.</returns>
		// Token: 0x17000305 RID: 773
		// (get) Token: 0x06000770 RID: 1904 RVA: 0x00004667 File Offset: 0x00002867
		public static Icon Asterisk
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets an <see cref="T:System.Drawing.Icon" /> object that contains the system error icon (WIN32: IDI_ERROR).</summary>
		/// <returns>An <see cref="T:System.Drawing.Icon" /> object that contains the system error icon.</returns>
		// Token: 0x17000306 RID: 774
		// (get) Token: 0x06000771 RID: 1905 RVA: 0x00004667 File Offset: 0x00002867
		public static Icon Error
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets an <see cref="T:System.Drawing.Icon" /> object that contains the system exclamation icon (WIN32: IDI_EXCLAMATION).</summary>
		/// <returns>An <see cref="T:System.Drawing.Icon" /> object that contains the system exclamation icon.</returns>
		// Token: 0x17000307 RID: 775
		// (get) Token: 0x06000772 RID: 1906 RVA: 0x00004667 File Offset: 0x00002867
		public static Icon Exclamation
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets an <see cref="T:System.Drawing.Icon" /> object that contains the system hand icon (WIN32: IDI_HAND).</summary>
		/// <returns>An <see cref="T:System.Drawing.Icon" /> object that contains the system hand icon.</returns>
		// Token: 0x17000308 RID: 776
		// (get) Token: 0x06000773 RID: 1907 RVA: 0x00004667 File Offset: 0x00002867
		public static Icon Hand
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets an <see cref="T:System.Drawing.Icon" /> object that contains the system information icon (WIN32: IDI_INFORMATION).</summary>
		/// <returns>An <see cref="T:System.Drawing.Icon" /> object that contains the system information icon.</returns>
		// Token: 0x17000309 RID: 777
		// (get) Token: 0x06000774 RID: 1908 RVA: 0x00004667 File Offset: 0x00002867
		public static Icon Information
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets an <see cref="T:System.Drawing.Icon" /> object that contains the system question icon (WIN32: IDI_QUESTION).</summary>
		/// <returns>An <see cref="T:System.Drawing.Icon" /> object that contains the system question icon.</returns>
		// Token: 0x1700030A RID: 778
		// (get) Token: 0x06000775 RID: 1909 RVA: 0x00004667 File Offset: 0x00002867
		public static Icon Question
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets an <see cref="T:System.Drawing.Icon" /> object that contains the shield icon.</summary>
		/// <returns>An <see cref="T:System.Drawing.Icon" /> object that contains the shield icon.</returns>
		// Token: 0x1700030B RID: 779
		// (get) Token: 0x06000776 RID: 1910 RVA: 0x00004667 File Offset: 0x00002867
		public static Icon Shield
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets an <see cref="T:System.Drawing.Icon" /> object that contains the system warning icon (WIN32: IDI_WARNING).</summary>
		/// <returns>An <see cref="T:System.Drawing.Icon" /> object that contains the system warning icon.</returns>
		// Token: 0x1700030C RID: 780
		// (get) Token: 0x06000777 RID: 1911 RVA: 0x00004667 File Offset: 0x00002867
		public static Icon Warning
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets an <see cref="T:System.Drawing.Icon" /> object that contains the Windows logo icon (WIN32: IDI_WINLOGO).</summary>
		/// <returns>An <see cref="T:System.Drawing.Icon" /> object that contains the Windows logo icon.</returns>
		// Token: 0x1700030D RID: 781
		// (get) Token: 0x06000778 RID: 1912 RVA: 0x00004667 File Offset: 0x00002867
		public static Icon WinLogo
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
