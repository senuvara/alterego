﻿using System;

namespace UnityEngine
{
	// Token: 0x02000008 RID: 8
	public interface ICanvasRaycastFilter
	{
		// Token: 0x06000034 RID: 52
		bool IsRaycastLocationValid(Vector2 sp, Camera eventCamera);
	}
}
