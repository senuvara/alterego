﻿using System;

namespace UnityEngine
{
	// Token: 0x02000003 RID: 3
	[Flags]
	public enum AdditionalCanvasShaderChannels
	{
		// Token: 0x04000006 RID: 6
		None = 0,
		// Token: 0x04000007 RID: 7
		TexCoord1 = 1,
		// Token: 0x04000008 RID: 8
		TexCoord2 = 2,
		// Token: 0x04000009 RID: 9
		TexCoord3 = 4,
		// Token: 0x0400000A RID: 10
		Normal = 8,
		// Token: 0x0400000B RID: 11
		Tangent = 16
	}
}
