﻿using System;

namespace UnityEngine
{
	// Token: 0x02000002 RID: 2
	public enum RenderMode
	{
		// Token: 0x04000002 RID: 2
		ScreenSpaceOverlay,
		// Token: 0x04000003 RID: 3
		ScreenSpaceCamera,
		// Token: 0x04000004 RID: 4
		WorldSpace
	}
}
