﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000009 RID: 9
	[NativeClass("UI::CanvasGroup")]
	[NativeHeader("Runtime/UI/CanvasGroup.h")]
	public sealed class CanvasGroup : Behaviour, ICanvasRaycastFilter
	{
		// Token: 0x06000035 RID: 53 RVA: 0x00002050 File Offset: 0x00000250
		public CanvasGroup()
		{
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000036 RID: 54
		// (set) Token: 0x06000037 RID: 55
		[NativeProperty("Alpha", false, TargetType.Function)]
		public extern float alpha { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000038 RID: 56
		// (set) Token: 0x06000039 RID: 57
		[NativeProperty("Interactable", false, TargetType.Function)]
		public extern bool interactable { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x0600003A RID: 58
		// (set) Token: 0x0600003B RID: 59
		[NativeProperty("BlocksRaycasts", false, TargetType.Function)]
		public extern bool blocksRaycasts { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x0600003C RID: 60
		// (set) Token: 0x0600003D RID: 61
		[NativeProperty("IgnoreParentGroups", false, TargetType.Function)]
		public extern bool ignoreParentGroups { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600003E RID: 62 RVA: 0x000020F8 File Offset: 0x000002F8
		public bool IsRaycastLocationValid(Vector2 sp, Camera eventCamera)
		{
			return this.blocksRaycasts;
		}
	}
}
