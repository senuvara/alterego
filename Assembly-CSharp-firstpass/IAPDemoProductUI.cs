﻿using System;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

// Token: 0x02000003 RID: 3
public class IAPDemoProductUI : MonoBehaviour
{
	// Token: 0x06000019 RID: 25 RVA: 0x00002E34 File Offset: 0x00001034
	public void SetProduct(Product p, Action<string> purchaseCallback)
	{
		this.titleText.text = p.metadata.localizedTitle;
		this.descriptionText.text = p.metadata.localizedDescription;
		this.priceText.text = p.metadata.localizedPriceString;
		this.receiptButton.interactable = p.hasReceipt;
		this.m_Receipt = p.receipt;
		this.m_ProductID = p.definition.id;
		this.m_PurchaseCallback = purchaseCallback;
		this.statusText.text = (p.availableToPurchase ? "Available" : "Unavailable");
	}

	// Token: 0x0600001A RID: 26 RVA: 0x00002ED7 File Offset: 0x000010D7
	public void SetPendingTime(int secondsRemaining)
	{
		this.statusText.text = "Pending " + secondsRemaining.ToString();
	}

	// Token: 0x0600001B RID: 27 RVA: 0x00002EF5 File Offset: 0x000010F5
	public void PurchaseButtonClick()
	{
		if (this.m_PurchaseCallback != null && !string.IsNullOrEmpty(this.m_ProductID))
		{
			this.m_PurchaseCallback(this.m_ProductID);
		}
	}

	// Token: 0x0600001C RID: 28 RVA: 0x00002F1D File Offset: 0x0000111D
	public void ReceiptButtonClick()
	{
		if (!string.IsNullOrEmpty(this.m_Receipt))
		{
			Debug.Log("Receipt for " + this.m_ProductID + ": " + this.m_Receipt);
		}
	}

	// Token: 0x0600001D RID: 29 RVA: 0x00002F4C File Offset: 0x0000114C
	public IAPDemoProductUI()
	{
	}

	// Token: 0x04000019 RID: 25
	public Button purchaseButton;

	// Token: 0x0400001A RID: 26
	public Button receiptButton;

	// Token: 0x0400001B RID: 27
	public Text titleText;

	// Token: 0x0400001C RID: 28
	public Text descriptionText;

	// Token: 0x0400001D RID: 29
	public Text priceText;

	// Token: 0x0400001E RID: 30
	public Text statusText;

	// Token: 0x0400001F RID: 31
	private string m_ProductID;

	// Token: 0x04000020 RID: 32
	private Action<string> m_PurchaseCallback;

	// Token: 0x04000021 RID: 33
	private string m_Receipt;
}
