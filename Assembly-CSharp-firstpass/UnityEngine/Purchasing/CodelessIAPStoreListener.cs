﻿using System;
using System.Collections.Generic;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000004 RID: 4
	public class CodelessIAPStoreListener : IStoreListener
	{
		// Token: 0x0600001E RID: 30 RVA: 0x00002F54 File Offset: 0x00001154
		[RuntimeInitializeOnLoadMethod]
		private static void InitializeCodelessPurchasingOnLoad()
		{
			ProductCatalog productCatalog = ProductCatalog.LoadDefaultCatalog();
			if (productCatalog.enableCodelessAutoInitialization && !productCatalog.IsEmpty() && CodelessIAPStoreListener.instance == null)
			{
				CodelessIAPStoreListener.CreateCodelessIAPStoreListenerInstance();
			}
		}

		// Token: 0x0600001F RID: 31 RVA: 0x00002F84 File Offset: 0x00001184
		private static void InitializePurchasing()
		{
			StandardPurchasingModule standardPurchasingModule = StandardPurchasingModule.Instance();
			standardPurchasingModule.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;
			ConfigurationBuilder builder = ConfigurationBuilder.Instance(standardPurchasingModule, Array.Empty<IPurchasingModule>());
			IAPConfigurationHelper.PopulateConfigurationBuilder(ref builder, CodelessIAPStoreListener.instance.catalog);
			UnityPurchasing.Initialize(CodelessIAPStoreListener.instance, builder);
			CodelessIAPStoreListener.unityPurchasingInitialized = true;
		}

		// Token: 0x06000020 RID: 32 RVA: 0x00002FCA File Offset: 0x000011CA
		private CodelessIAPStoreListener()
		{
			this.catalog = ProductCatalog.LoadDefaultCatalog();
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000021 RID: 33 RVA: 0x00002FF3 File Offset: 0x000011F3
		public static CodelessIAPStoreListener Instance
		{
			get
			{
				if (CodelessIAPStoreListener.instance == null)
				{
					CodelessIAPStoreListener.CreateCodelessIAPStoreListenerInstance();
				}
				return CodelessIAPStoreListener.instance;
			}
		}

		// Token: 0x06000022 RID: 34 RVA: 0x00003006 File Offset: 0x00001206
		private static void CreateCodelessIAPStoreListenerInstance()
		{
			CodelessIAPStoreListener.instance = new CodelessIAPStoreListener();
			if (!CodelessIAPStoreListener.unityPurchasingInitialized)
			{
				Debug.Log("Initializing UnityPurchasing via Codeless IAP");
				CodelessIAPStoreListener.InitializePurchasing();
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000023 RID: 35 RVA: 0x00003028 File Offset: 0x00001228
		public IStoreController StoreController
		{
			get
			{
				return this.controller;
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000024 RID: 36 RVA: 0x00003030 File Offset: 0x00001230
		public IExtensionProvider ExtensionProvider
		{
			get
			{
				return this.extensions;
			}
		}

		// Token: 0x06000025 RID: 37 RVA: 0x00003038 File Offset: 0x00001238
		public bool HasProductInCatalog(string productID)
		{
			using (IEnumerator<ProductCatalogItem> enumerator = this.catalog.allProducts.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					if (enumerator.Current.id == productID)
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x06000026 RID: 38 RVA: 0x00003098 File Offset: 0x00001298
		public Product GetProduct(string productID)
		{
			if (this.controller != null && this.controller.products != null && !string.IsNullOrEmpty(productID))
			{
				return this.controller.products.WithID(productID);
			}
			Debug.LogError("CodelessIAPStoreListener attempted to get unknown product " + productID);
			return null;
		}

		// Token: 0x06000027 RID: 39 RVA: 0x000030E5 File Offset: 0x000012E5
		public void AddButton(IAPButton button)
		{
			this.activeButtons.Add(button);
		}

		// Token: 0x06000028 RID: 40 RVA: 0x000030F3 File Offset: 0x000012F3
		public void RemoveButton(IAPButton button)
		{
			this.activeButtons.Remove(button);
		}

		// Token: 0x06000029 RID: 41 RVA: 0x00003102 File Offset: 0x00001302
		public void AddListener(IAPListener listener)
		{
			this.activeListeners.Add(listener);
		}

		// Token: 0x0600002A RID: 42 RVA: 0x00003110 File Offset: 0x00001310
		public void RemoveListener(IAPListener listener)
		{
			this.activeListeners.Remove(listener);
		}

		// Token: 0x0600002B RID: 43 RVA: 0x00003120 File Offset: 0x00001320
		public void InitiatePurchase(string productID)
		{
			if (this.controller == null)
			{
				Debug.LogError("Purchase failed because Purchasing was not initialized correctly");
				foreach (IAPButton iapbutton in this.activeButtons)
				{
					if (iapbutton.productId == productID)
					{
						iapbutton.OnPurchaseFailed(null, PurchaseFailureReason.PurchasingUnavailable);
					}
				}
				return;
			}
			this.controller.InitiatePurchase(productID);
		}

		// Token: 0x0600002C RID: 44 RVA: 0x000031A4 File Offset: 0x000013A4
		public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
		{
			CodelessIAPStoreListener.initializationComplete = true;
			this.controller = controller;
			this.extensions = extensions;
			foreach (IAPButton iapbutton in this.activeButtons)
			{
				iapbutton.UpdateText();
			}
		}

		// Token: 0x0600002D RID: 45 RVA: 0x00003208 File Offset: 0x00001408
		public void OnInitializeFailed(InitializationFailureReason error)
		{
			Debug.LogError(string.Format("Purchasing failed to initialize. Reason: {0}", error.ToString()));
		}

		// Token: 0x0600002E RID: 46 RVA: 0x00003228 File Offset: 0x00001428
		public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
		{
			bool flag = false;
			bool flag2 = false;
			foreach (IAPButton iapbutton in this.activeButtons)
			{
				if (iapbutton.productId == e.purchasedProduct.definition.id)
				{
					if (iapbutton.ProcessPurchase(e) == PurchaseProcessingResult.Complete)
					{
						flag = true;
					}
					flag2 = true;
				}
			}
			using (List<IAPListener>.Enumerator enumerator2 = this.activeListeners.GetEnumerator())
			{
				while (enumerator2.MoveNext())
				{
					if (enumerator2.Current.ProcessPurchase(e) == PurchaseProcessingResult.Complete)
					{
						flag = true;
					}
					flag2 = true;
				}
			}
			if (!flag2)
			{
				Debug.LogError("Purchase not correctly processed for product \"" + e.purchasedProduct.definition.id + "\". Add an active IAPButton to process this purchase, or add an IAPListener to receive any unhandled purchase events.");
			}
			if (!flag)
			{
				return PurchaseProcessingResult.Pending;
			}
			return PurchaseProcessingResult.Complete;
		}

		// Token: 0x0600002F RID: 47 RVA: 0x00003318 File Offset: 0x00001518
		public void OnPurchaseFailed(Product product, PurchaseFailureReason reason)
		{
			bool flag = false;
			foreach (IAPButton iapbutton in this.activeButtons)
			{
				if (iapbutton.productId == product.definition.id)
				{
					iapbutton.OnPurchaseFailed(product, reason);
					flag = true;
				}
			}
			foreach (IAPListener iaplistener in this.activeListeners)
			{
				iaplistener.OnPurchaseFailed(product, reason);
				flag = true;
			}
			if (!flag)
			{
				Debug.LogError("Failed purchase not correctly handled for product \"" + product.definition.id + "\". Add an active IAPButton to handle this failure, or add an IAPListener to receive any unhandled purchase failures.");
			}
		}

		// Token: 0x04000022 RID: 34
		private static CodelessIAPStoreListener instance;

		// Token: 0x04000023 RID: 35
		private List<IAPButton> activeButtons = new List<IAPButton>();

		// Token: 0x04000024 RID: 36
		private List<IAPListener> activeListeners = new List<IAPListener>();

		// Token: 0x04000025 RID: 37
		private static bool unityPurchasingInitialized;

		// Token: 0x04000026 RID: 38
		protected IStoreController controller;

		// Token: 0x04000027 RID: 39
		protected IExtensionProvider extensions;

		// Token: 0x04000028 RID: 40
		protected ProductCatalog catalog;

		// Token: 0x04000029 RID: 41
		public static bool initializationComplete;
	}
}
