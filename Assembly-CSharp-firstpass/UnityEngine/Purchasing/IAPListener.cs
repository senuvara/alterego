﻿using System;
using UnityEngine.Events;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000008 RID: 8
	[AddComponentMenu("Unity IAP/IAP Listener")]
	[HelpURL("https://docs.unity3d.com/Manual/UnityIAP.html")]
	public class IAPListener : MonoBehaviour
	{
		// Token: 0x0600003E RID: 62 RVA: 0x000038B0 File Offset: 0x00001AB0
		private void OnEnable()
		{
			if (this.dontDestroyOnLoad)
			{
				Object.DontDestroyOnLoad(base.gameObject);
			}
			CodelessIAPStoreListener.Instance.AddListener(this);
		}

		// Token: 0x0600003F RID: 63 RVA: 0x000038D0 File Offset: 0x00001AD0
		private void OnDisable()
		{
			CodelessIAPStoreListener.Instance.RemoveListener(this);
		}

		// Token: 0x06000040 RID: 64 RVA: 0x000038DD File Offset: 0x00001ADD
		public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
		{
			Debug.Log(string.Format("IAPListener.ProcessPurchase(PurchaseEventArgs {0} - {1})", e, e.purchasedProduct.definition.id));
			this.onPurchaseComplete.Invoke(e.purchasedProduct);
			if (!this.consumePurchase)
			{
				return PurchaseProcessingResult.Pending;
			}
			return PurchaseProcessingResult.Complete;
		}

		// Token: 0x06000041 RID: 65 RVA: 0x0000391B File Offset: 0x00001B1B
		public void OnPurchaseFailed(Product product, PurchaseFailureReason reason)
		{
			Debug.Log(string.Format("IAPListener.OnPurchaseFailed(Product {0}, PurchaseFailureReason {1})", product, reason));
			this.onPurchaseFailed.Invoke(product, reason);
		}

		// Token: 0x06000042 RID: 66 RVA: 0x00003940 File Offset: 0x00001B40
		public IAPListener()
		{
		}

		// Token: 0x04000032 RID: 50
		[Tooltip("Consume successful purchases immediately")]
		public bool consumePurchase = true;

		// Token: 0x04000033 RID: 51
		[Tooltip("Preserve this GameObject when a new scene is loaded")]
		public bool dontDestroyOnLoad = true;

		// Token: 0x04000034 RID: 52
		[Tooltip("Event fired after a successful purchase of this product")]
		public IAPListener.OnPurchaseCompletedEvent onPurchaseComplete;

		// Token: 0x04000035 RID: 53
		[Tooltip("Event fired after a failed purchase of this product")]
		public IAPListener.OnPurchaseFailedEvent onPurchaseFailed;

		// Token: 0x02000014 RID: 20
		[Serializable]
		public class OnPurchaseCompletedEvent : UnityEvent<Product>
		{
			// Token: 0x06000057 RID: 87 RVA: 0x00003AAE File Offset: 0x00001CAE
			public OnPurchaseCompletedEvent()
			{
			}
		}

		// Token: 0x02000015 RID: 21
		[Serializable]
		public class OnPurchaseFailedEvent : UnityEvent<Product, PurchaseFailureReason>
		{
			// Token: 0x06000058 RID: 88 RVA: 0x00003AB6 File Offset: 0x00001CB6
			public OnPurchaseFailedEvent()
			{
			}
		}
	}
}
