﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000006 RID: 6
	[RequireComponent(typeof(Button))]
	[AddComponentMenu("Unity IAP/IAP Button")]
	[HelpURL("https://docs.unity3d.com/Manual/UnityIAP.html")]
	public class IAPButton : MonoBehaviour
	{
		// Token: 0x06000032 RID: 50 RVA: 0x0000341C File Offset: 0x0000161C
		private void Start()
		{
			Button component = base.GetComponent<Button>();
			if (this.buttonType == IAPButton.ButtonType.Purchase)
			{
				if (component)
				{
					component.onClick.AddListener(new UnityAction(this.PurchaseProduct));
				}
				if (string.IsNullOrEmpty(this.productId))
				{
					Debug.LogError("IAPButton productId is empty");
				}
				if (!CodelessIAPStoreListener.Instance.HasProductInCatalog(this.productId))
				{
					Debug.LogWarning("The product catalog has no product with the ID \"" + this.productId + "\"");
					return;
				}
			}
			else if (this.buttonType == IAPButton.ButtonType.Restore && component)
			{
				component.onClick.AddListener(new UnityAction(this.Restore));
			}
		}

		// Token: 0x06000033 RID: 51 RVA: 0x000034C3 File Offset: 0x000016C3
		private void OnEnable()
		{
			if (this.buttonType == IAPButton.ButtonType.Purchase)
			{
				CodelessIAPStoreListener.Instance.AddButton(this);
				if (CodelessIAPStoreListener.initializationComplete)
				{
					this.UpdateText();
				}
			}
		}

		// Token: 0x06000034 RID: 52 RVA: 0x000034E5 File Offset: 0x000016E5
		private void OnDisable()
		{
			if (this.buttonType == IAPButton.ButtonType.Purchase)
			{
				CodelessIAPStoreListener.Instance.RemoveButton(this);
			}
		}

		// Token: 0x06000035 RID: 53 RVA: 0x000034FA File Offset: 0x000016FA
		private void PurchaseProduct()
		{
			if (this.buttonType == IAPButton.ButtonType.Purchase)
			{
				Debug.Log("IAPButton.PurchaseProduct() with product ID: " + this.productId);
				CodelessIAPStoreListener.Instance.InitiatePurchase(this.productId);
			}
		}

		// Token: 0x06000036 RID: 54 RVA: 0x0000352C File Offset: 0x0000172C
		private void Restore()
		{
			if (this.buttonType == IAPButton.ButtonType.Restore)
			{
				if (Application.platform == RuntimePlatform.MetroPlayerX86 || Application.platform == RuntimePlatform.MetroPlayerX64 || Application.platform == RuntimePlatform.MetroPlayerARM)
				{
					CodelessIAPStoreListener.Instance.ExtensionProvider.GetExtension<IMicrosoftExtensions>().RestoreTransactions();
					return;
				}
				if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.tvOS)
				{
					CodelessIAPStoreListener.Instance.ExtensionProvider.GetExtension<IAppleExtensions>().RestoreTransactions(new Action<bool>(this.OnTransactionsRestored));
					return;
				}
				if (Application.platform == RuntimePlatform.Android && StandardPurchasingModule.Instance().appStore == AppStore.SamsungApps)
				{
					CodelessIAPStoreListener.Instance.ExtensionProvider.GetExtension<ISamsungAppsExtensions>().RestoreTransactions(new Action<bool>(this.OnTransactionsRestored));
					return;
				}
				if (Application.platform == RuntimePlatform.Android && StandardPurchasingModule.Instance().appStore == AppStore.CloudMoolah)
				{
					CodelessIAPStoreListener.Instance.ExtensionProvider.GetExtension<IMoolahExtension>().RestoreTransactionID(delegate(RestoreTransactionIDState restoreTransactionIDState)
					{
						this.OnTransactionsRestored(restoreTransactionIDState != RestoreTransactionIDState.RestoreFailed && restoreTransactionIDState != RestoreTransactionIDState.NotKnown);
					});
					return;
				}
				Debug.LogWarning(Application.platform.ToString() + " is not a supported platform for the Codeless IAP restore button");
			}
		}

		// Token: 0x06000037 RID: 55 RVA: 0x0000363F File Offset: 0x0000183F
		private void OnTransactionsRestored(bool success)
		{
			Debug.Log("Transactions restored: " + success.ToString());
		}

		// Token: 0x06000038 RID: 56 RVA: 0x00003657 File Offset: 0x00001857
		public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
		{
			Debug.Log(string.Format("IAPButton.ProcessPurchase(PurchaseEventArgs {0} - {1})", e, e.purchasedProduct.definition.id));
			this.onPurchaseComplete.Invoke(e.purchasedProduct);
			if (!this.consumePurchase)
			{
				return PurchaseProcessingResult.Pending;
			}
			return PurchaseProcessingResult.Complete;
		}

		// Token: 0x06000039 RID: 57 RVA: 0x00003695 File Offset: 0x00001895
		public void OnPurchaseFailed(Product product, PurchaseFailureReason reason)
		{
			Debug.Log(string.Format("IAPButton.OnPurchaseFailed(Product {0}, PurchaseFailureReason {1})", product, reason));
			this.onPurchaseFailed.Invoke(product, reason);
		}

		// Token: 0x0600003A RID: 58 RVA: 0x000036BC File Offset: 0x000018BC
		internal void UpdateText()
		{
			Product product = CodelessIAPStoreListener.Instance.GetProduct(this.productId);
			if (product != null)
			{
				if (this.titleText != null)
				{
					this.titleText.text = product.metadata.localizedTitle;
				}
				if (this.descriptionText != null)
				{
					this.descriptionText.text = product.metadata.localizedDescription;
				}
				if (this.priceText != null)
				{
					this.priceText.text = product.metadata.localizedPriceString;
				}
			}
		}

		// Token: 0x0600003B RID: 59 RVA: 0x00003749 File Offset: 0x00001949
		public IAPButton()
		{
		}

		// Token: 0x0600003C RID: 60 RVA: 0x00003758 File Offset: 0x00001958
		[CompilerGenerated]
		private void <Restore>b__15_0(RestoreTransactionIDState restoreTransactionIDState)
		{
			this.OnTransactionsRestored(restoreTransactionIDState != RestoreTransactionIDState.RestoreFailed && restoreTransactionIDState != RestoreTransactionIDState.NotKnown);
		}

		// Token: 0x0400002A RID: 42
		[HideInInspector]
		public string productId;

		// Token: 0x0400002B RID: 43
		[Tooltip("The type of this button, can be either a purchase or a restore button")]
		public IAPButton.ButtonType buttonType;

		// Token: 0x0400002C RID: 44
		[Tooltip("Consume the product immediately after a successful purchase")]
		public bool consumePurchase = true;

		// Token: 0x0400002D RID: 45
		[Tooltip("Event fired after a successful purchase of this product")]
		public IAPButton.OnPurchaseCompletedEvent onPurchaseComplete;

		// Token: 0x0400002E RID: 46
		[Tooltip("Event fired after a failed purchase of this product")]
		public IAPButton.OnPurchaseFailedEvent onPurchaseFailed;

		// Token: 0x0400002F RID: 47
		[Tooltip("[Optional] Displays the localized title from the app store")]
		public Text titleText;

		// Token: 0x04000030 RID: 48
		[Tooltip("[Optional] Displays the localized description from the app store")]
		public Text descriptionText;

		// Token: 0x04000031 RID: 49
		[Tooltip("[Optional] Displays the localized price from the app store")]
		public Text priceText;

		// Token: 0x02000011 RID: 17
		public enum ButtonType
		{
			// Token: 0x0400004D RID: 77
			Purchase,
			// Token: 0x0400004E RID: 78
			Restore
		}

		// Token: 0x02000012 RID: 18
		[Serializable]
		public class OnPurchaseCompletedEvent : UnityEvent<Product>
		{
			// Token: 0x06000055 RID: 85 RVA: 0x00003AAE File Offset: 0x00001CAE
			public OnPurchaseCompletedEvent()
			{
			}
		}

		// Token: 0x02000013 RID: 19
		[Serializable]
		public class OnPurchaseFailedEvent : UnityEvent<Product, PurchaseFailureReason>
		{
			// Token: 0x06000056 RID: 86 RVA: 0x00003AB6 File Offset: 0x00001CB6
			public OnPurchaseFailedEvent()
			{
			}
		}
	}
}
