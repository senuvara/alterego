﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000005 RID: 5
	[AddComponentMenu("")]
	public class DemoInventory : MonoBehaviour
	{
		// Token: 0x06000030 RID: 48 RVA: 0x000033F0 File Offset: 0x000015F0
		public void Fulfill(string productId)
		{
			if (productId == "100.gold.coins")
			{
				Debug.Log("You Got Money!");
				return;
			}
			Debug.Log(string.Format("Unrecognized productId \"{0}\"", productId));
		}

		// Token: 0x06000031 RID: 49 RVA: 0x00002F4C File Offset: 0x0000114C
		public DemoInventory()
		{
		}
	}
}
