﻿using System;
using UnityEngine;
using UnityEngine.Store;

namespace AppStoresSupport
{
	// Token: 0x0200000A RID: 10
	[Serializable]
	public class AppStoreSettings : ScriptableObject
	{
		// Token: 0x06000044 RID: 68 RVA: 0x00003974 File Offset: 0x00001B74
		public AppInfo getAppInfo()
		{
			return new AppInfo
			{
				clientId = this.UnityClientID,
				clientKey = this.UnityClientKey,
				appId = this.XiaomiAppStoreSetting.AppID,
				appKey = this.XiaomiAppStoreSetting.AppKey,
				debug = this.XiaomiAppStoreSetting.IsTestMode
			};
		}

		// Token: 0x06000045 RID: 69 RVA: 0x000039D1 File Offset: 0x00001BD1
		public AppStoreSettings()
		{
		}

		// Token: 0x04000039 RID: 57
		public string UnityClientID = "";

		// Token: 0x0400003A RID: 58
		public string UnityClientKey = "";

		// Token: 0x0400003B RID: 59
		public string UnityClientRSAPublicKey = "";

		// Token: 0x0400003C RID: 60
		public AppStoreSetting XiaomiAppStoreSetting = new AppStoreSetting();
	}
}
