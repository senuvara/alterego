﻿using System;

namespace AppStoresSupport
{
	// Token: 0x02000009 RID: 9
	[Serializable]
	public class AppStoreSetting
	{
		// Token: 0x06000043 RID: 67 RVA: 0x00003956 File Offset: 0x00001B56
		public AppStoreSetting()
		{
		}

		// Token: 0x04000036 RID: 54
		public string AppID = "";

		// Token: 0x04000037 RID: 55
		public string AppKey = "";

		// Token: 0x04000038 RID: 56
		public bool IsTestMode;
	}
}
