﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Extension;
using UnityEngine.Store;
using UnityEngine.UI;

// Token: 0x02000002 RID: 2
[AddComponentMenu("Unity IAP/Demo")]
public class IAPDemo : MonoBehaviour, IStoreListener
{
	// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
	public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
	{
		this.m_Controller = controller;
		this.m_AppleExtensions = extensions.GetExtension<IAppleExtensions>();
		this.m_SamsungExtensions = extensions.GetExtension<ISamsungAppsExtensions>();
		this.m_MoolahExtensions = extensions.GetExtension<IMoolahExtension>();
		this.m_MicrosoftExtensions = extensions.GetExtension<IMicrosoftExtensions>();
		this.m_UnityChannelExtensions = extensions.GetExtension<IUnityChannelExtensions>();
		this.m_TransactionHistoryExtensions = extensions.GetExtension<ITransactionHistoryExtensions>();
		this.m_GooglePlayStoreExtensions = extensions.GetExtension<IGooglePlayStoreExtensions>();
		this.InitUI(controller.products.all);
		this.m_AppleExtensions.RegisterPurchaseDeferredListener(new Action<Product>(this.OnDeferred));
		Debug.Log("Available items:");
		foreach (Product product in controller.products.all)
		{
			if (product.availableToPurchase)
			{
				Debug.Log(string.Join(" - ", new string[]
				{
					product.metadata.localizedTitle,
					product.metadata.localizedDescription,
					product.metadata.isoCurrencyCode,
					product.metadata.localizedPrice.ToString(),
					product.metadata.localizedPriceString,
					product.transactionID,
					product.receipt
				}));
			}
		}
		this.AddProductUIs(this.m_Controller.products.all);
		this.LogProductDefinitions();
	}

	// Token: 0x06000002 RID: 2 RVA: 0x000021A8 File Offset: 0x000003A8
	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
	{
		Debug.Log("Purchase OK: " + e.purchasedProduct.definition.id);
		Debug.Log("Receipt: " + e.purchasedProduct.receipt);
		this.m_LastTransactionID = e.purchasedProduct.transactionID;
		this.m_PurchaseInProgress = false;
		if (this.m_IsUnityChannelSelected)
		{
			UnifiedReceipt unifiedReceipt = JsonUtility.FromJson<UnifiedReceipt>(e.purchasedProduct.receipt);
			if (unifiedReceipt != null && !string.IsNullOrEmpty(unifiedReceipt.Payload))
			{
				UnityChannelPurchaseReceipt unityChannelPurchaseReceipt = JsonUtility.FromJson<UnityChannelPurchaseReceipt>(unifiedReceipt.Payload);
				Debug.LogFormat("UnityChannel receipt: storeSpecificId = {0}, transactionId = {1}, orderQueryToken = {2}", new object[]
				{
					unityChannelPurchaseReceipt.storeSpecificId,
					unityChannelPurchaseReceipt.transactionId,
					unityChannelPurchaseReceipt.orderQueryToken
				});
			}
		}
		this.UpdateProductUI(e.purchasedProduct);
		return PurchaseProcessingResult.Complete;
	}

	// Token: 0x06000003 RID: 3 RVA: 0x00002274 File Offset: 0x00000474
	public void OnPurchaseFailed(Product item, PurchaseFailureReason r)
	{
		Debug.Log("Purchase failed: " + item.definition.id);
		Debug.Log(r);
		Debug.Log("Store specific error code: " + this.m_TransactionHistoryExtensions.GetLastStoreSpecificPurchaseErrorCode());
		if (this.m_TransactionHistoryExtensions.GetLastPurchaseFailureDescription() != null)
		{
			Debug.Log("Purchase failure description message: " + this.m_TransactionHistoryExtensions.GetLastPurchaseFailureDescription().message);
		}
		if (this.m_IsUnityChannelSelected)
		{
			IAPDemo.UnityChannelPurchaseError unityChannelPurchaseError = JsonUtility.FromJson<IAPDemo.UnityChannelPurchaseError>(this.m_UnityChannelExtensions.GetLastPurchaseError());
			if (unityChannelPurchaseError != null && unityChannelPurchaseError.purchaseInfo != null)
			{
				IAPDemo.UnityChannelPurchaseInfo purchaseInfo = unityChannelPurchaseError.purchaseInfo;
				Debug.LogFormat("UnityChannel purchaseInfo: productCode = {0}, gameOrderId = {1}, orderQueryToken = {2}", new object[]
				{
					purchaseInfo.productCode,
					purchaseInfo.gameOrderId,
					purchaseInfo.orderQueryToken
				});
			}
			if (r == PurchaseFailureReason.DuplicateTransaction)
			{
				Debug.Log("Duplicate transaction detected, unlock this item");
			}
		}
		this.m_PurchaseInProgress = false;
	}

	// Token: 0x06000004 RID: 4 RVA: 0x0000235C File Offset: 0x0000055C
	public void OnInitializeFailed(InitializationFailureReason error)
	{
		Debug.Log("Billing failed to initialize!");
		switch (error)
		{
		case InitializationFailureReason.PurchasingUnavailable:
			Debug.Log("Billing disabled!");
			return;
		case InitializationFailureReason.NoProductsAvailable:
			Debug.Log("No products available for purchase!");
			return;
		case InitializationFailureReason.AppNotKnown:
			Debug.LogError("Is your App correctly uploaded on the relevant publisher console?");
			return;
		default:
			return;
		}
	}

	// Token: 0x06000005 RID: 5 RVA: 0x0000239C File Offset: 0x0000059C
	public void Awake()
	{
		StandardPurchasingModule standardPurchasingModule = StandardPurchasingModule.Instance();
		standardPurchasingModule.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;
		ConfigurationBuilder builder = ConfigurationBuilder.Instance(standardPurchasingModule, Array.Empty<IPurchasingModule>());
		builder.Configure<IMicrosoftConfiguration>().useMockBillingSystem = false;
		this.m_IsGooglePlayStoreSelected = (Application.platform == RuntimePlatform.Android && standardPurchasingModule.appStore == AppStore.GooglePlay);
		builder.Configure<IMoolahConfiguration>().appKey = "d93f4564c41d463ed3d3cd207594ee1b";
		builder.Configure<IMoolahConfiguration>().hashKey = "cc";
		builder.Configure<IMoolahConfiguration>().SetMode(CloudMoolahMode.AlwaysSucceed);
		this.m_IsCloudMoolahStoreSelected = (Application.platform == RuntimePlatform.Android && standardPurchasingModule.appStore == AppStore.CloudMoolah);
		this.m_IsUnityChannelSelected = (Application.platform == RuntimePlatform.Android && standardPurchasingModule.appStore == AppStore.XiaomiMiPay);
		builder.Configure<IUnityChannelConfiguration>().fetchReceiptPayloadOnPurchase = this.m_FetchReceiptPayloadOnPurchase;
		foreach (ProductCatalogItem productCatalogItem in ProductCatalog.LoadDefaultCatalog().allValidProducts)
		{
			if (productCatalogItem.allStoreIDs.Count > 0)
			{
				IDs ds = new IDs();
				foreach (StoreID storeID in productCatalogItem.allStoreIDs)
				{
					ds.Add(storeID.id, new string[]
					{
						storeID.store
					});
				}
				builder.AddProduct(productCatalogItem.id, productCatalogItem.type, ds);
			}
			else
			{
				builder.AddProduct(productCatalogItem.id, productCatalogItem.type);
			}
		}
		builder.AddProduct("100.gold.coins", ProductType.Consumable, new IDs
		{
			{
				"com.unity3d.unityiap.unityiapdemo.100goldcoins.7",
				new string[]
				{
					"MacAppStore"
				}
			},
			{
				"000000596586",
				new string[]
				{
					"TizenStore"
				}
			},
			{
				"com.ff",
				new string[]
				{
					"MoolahAppStore"
				}
			},
			{
				"100.gold.coins",
				new string[]
				{
					"AmazonApps"
				}
			},
			{
				"100.gold.coins",
				new string[]
				{
					"AppleAppStore"
				}
			}
		});
		builder.AddProduct("500.gold.coins", ProductType.Consumable, new IDs
		{
			{
				"com.unity3d.unityiap.unityiapdemo.500goldcoins.7",
				new string[]
				{
					"MacAppStore"
				}
			},
			{
				"000000596581",
				new string[]
				{
					"TizenStore"
				}
			},
			{
				"com.ee",
				new string[]
				{
					"MoolahAppStore"
				}
			},
			{
				"500.gold.coins",
				new string[]
				{
					"AmazonApps"
				}
			}
		});
		builder.AddProduct("300.gold.coins", ProductType.Consumable, new IDs());
		builder.AddProduct("sub1", ProductType.Subscription, new IDs());
		builder.AddProduct("sub2", ProductType.Subscription, new IDs());
		builder.Configure<ISamsungAppsConfiguration>().SetMode(SamsungAppsMode.AlwaysSucceed);
		this.m_IsSamsungAppsStoreSelected = (Application.platform == RuntimePlatform.Android && standardPurchasingModule.appStore == AppStore.SamsungApps);
		builder.Configure<ITizenStoreConfiguration>().SetGroupId("100000085616");
		Action initializeUnityIap = delegate()
		{
			UnityPurchasing.Initialize(this, builder);
		};
		if (!this.m_IsUnityChannelSelected)
		{
			initializeUnityIap();
			return;
		}
		AppInfo appInfo = new AppInfo();
		appInfo.appId = "abc123appId";
		appInfo.appKey = "efg456appKey";
		appInfo.clientId = "hij789clientId";
		appInfo.clientKey = "klm012clientKey";
		appInfo.debug = false;
		this.unityChannelLoginHandler = new IAPDemo.UnityChannelLoginHandler();
		this.unityChannelLoginHandler.initializeFailedAction = delegate(string message)
		{
			Debug.LogError("Failed to initialize and login to UnityChannel: " + message);
		};
		this.unityChannelLoginHandler.initializeSucceededAction = delegate()
		{
			initializeUnityIap();
		};
		StoreService.Initialize(appInfo, this.unityChannelLoginHandler);
	}

	// Token: 0x06000006 RID: 6 RVA: 0x000027C8 File Offset: 0x000009C8
	private void OnTransactionsRestored(bool success)
	{
		Debug.Log("Transactions restored." + success.ToString());
	}

	// Token: 0x06000007 RID: 7 RVA: 0x000027E0 File Offset: 0x000009E0
	private void OnDeferred(Product item)
	{
		Debug.Log("Purchase deferred: " + item.definition.id);
	}

	// Token: 0x06000008 RID: 8 RVA: 0x000027FC File Offset: 0x000009FC
	private void InitUI(IEnumerable<Product> items)
	{
		this.restoreButton.gameObject.SetActive(true);
		this.loginButton.gameObject.SetActive(this.NeedLoginButton());
		this.validateButton.gameObject.SetActive(this.NeedValidateButton());
		this.ClearProductUIs();
		this.restoreButton.onClick.AddListener(new UnityAction(this.RestoreButtonClick));
		this.loginButton.onClick.AddListener(new UnityAction(this.LoginButtonClick));
		this.validateButton.onClick.AddListener(new UnityAction(this.ValidateButtonClick));
		this.versionText.text = "Unity version: " + Application.unityVersion + "\nIAP version: 1.22.0";
	}

	// Token: 0x06000009 RID: 9 RVA: 0x000028C0 File Offset: 0x00000AC0
	public void PurchaseButtonClick(string productID)
	{
		if (this.m_PurchaseInProgress)
		{
			Debug.Log("Please wait, purchase in progress");
			return;
		}
		if (this.m_Controller == null)
		{
			Debug.LogError("Purchasing is not initialized");
			return;
		}
		if (this.m_Controller.products.WithID(productID) == null)
		{
			Debug.LogError("No product has id " + productID);
			return;
		}
		if (this.NeedLoginButton() && !this.m_IsLoggedIn)
		{
			Debug.LogWarning("Purchase notifications will not be forwarded server-to-server. Login incomplete.");
		}
		this.m_PurchaseInProgress = true;
		this.m_Controller.InitiatePurchase(this.m_Controller.products.WithID(productID), "developerPayload");
	}

	// Token: 0x0600000A RID: 10 RVA: 0x0000295C File Offset: 0x00000B5C
	public void RestoreButtonClick()
	{
		if (this.m_IsCloudMoolahStoreSelected)
		{
			if (!this.m_IsLoggedIn)
			{
				Debug.LogError("CloudMoolah purchase restoration aborted. Login incomplete.");
				return;
			}
			this.m_MoolahExtensions.RestoreTransactionID(delegate(RestoreTransactionIDState restoreTransactionIDState)
			{
				Debug.Log("restoreTransactionIDState = " + restoreTransactionIDState.ToString());
				bool success = restoreTransactionIDState != RestoreTransactionIDState.RestoreFailed && restoreTransactionIDState != RestoreTransactionIDState.NotKnown;
				this.OnTransactionsRestored(success);
			});
			return;
		}
		else
		{
			if (this.m_IsSamsungAppsStoreSelected)
			{
				this.m_SamsungExtensions.RestoreTransactions(new Action<bool>(this.OnTransactionsRestored));
				return;
			}
			if (Application.platform == RuntimePlatform.MetroPlayerX86 || Application.platform == RuntimePlatform.MetroPlayerX64 || Application.platform == RuntimePlatform.MetroPlayerARM)
			{
				this.m_MicrosoftExtensions.RestoreTransactions();
				return;
			}
			if (this.m_IsGooglePlayStoreSelected)
			{
				this.m_GooglePlayStoreExtensions.RestoreTransactions(new Action<bool>(this.OnTransactionsRestored));
				return;
			}
			this.m_AppleExtensions.RestoreTransactions(new Action<bool>(this.OnTransactionsRestored));
			return;
		}
	}

	// Token: 0x0600000B RID: 11 RVA: 0x00002A1C File Offset: 0x00000C1C
	public void LoginButtonClick()
	{
		if (!this.m_IsUnityChannelSelected)
		{
			Debug.Log("Login is only required for the Xiaomi store");
			return;
		}
		this.unityChannelLoginHandler.loginSucceededAction = delegate(UserInfo userInfo)
		{
			this.m_IsLoggedIn = true;
			Debug.LogFormat("Succeeded logging into UnityChannel. channel {0}, userId {1}, userLoginToken {2} ", new object[]
			{
				userInfo.channel,
				userInfo.userId,
				userInfo.userLoginToken
			});
		};
		this.unityChannelLoginHandler.loginFailedAction = delegate(string message)
		{
			this.m_IsLoggedIn = false;
			Debug.LogError("Failed logging into UnityChannel. " + message);
		};
		StoreService.Login(this.unityChannelLoginHandler);
	}

	// Token: 0x0600000C RID: 12 RVA: 0x00002A78 File Offset: 0x00000C78
	public void ValidateButtonClick()
	{
		if (!this.m_IsUnityChannelSelected)
		{
			Debug.Log("Remote purchase validation is only supported for the Xiaomi store");
			return;
		}
		string txId = this.m_LastTransactionID;
		this.m_UnityChannelExtensions.ValidateReceipt(txId, delegate(bool success, string signData, string signature)
		{
			Debug.LogFormat("ValidateReceipt transactionId {0}, success {1}, signData {2}, signature {3}", new object[]
			{
				txId,
				success,
				signData,
				signature
			});
		});
	}

	// Token: 0x0600000D RID: 13 RVA: 0x00002AC8 File Offset: 0x00000CC8
	private void ClearProductUIs()
	{
		foreach (KeyValuePair<string, IAPDemoProductUI> keyValuePair in this.m_ProductUIs)
		{
			Object.Destroy(keyValuePair.Value.gameObject);
		}
		this.m_ProductUIs.Clear();
	}

	// Token: 0x0600000E RID: 14 RVA: 0x00002B30 File Offset: 0x00000D30
	private void AddProductUIs(Product[] products)
	{
		this.ClearProductUIs();
		RectTransform component = this.productUITemplate.GetComponent<RectTransform>();
		float height = component.rect.height;
		Vector3 vector = component.localPosition;
		this.contentRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (float)products.Length * height);
		foreach (Product product in products)
		{
			GameObject gameObject = Object.Instantiate<GameObject>(this.productUITemplate.gameObject);
			gameObject.transform.SetParent(this.productUITemplate.transform.parent, false);
			gameObject.GetComponent<RectTransform>().localPosition = vector;
			vector += Vector3.down * height;
			gameObject.SetActive(true);
			IAPDemoProductUI component2 = gameObject.GetComponent<IAPDemoProductUI>();
			component2.SetProduct(product, new Action<string>(this.PurchaseButtonClick));
			this.m_ProductUIs[product.definition.id] = component2;
		}
	}

	// Token: 0x0600000F RID: 15 RVA: 0x00002C1C File Offset: 0x00000E1C
	private void UpdateProductUI(Product p)
	{
		if (this.m_ProductUIs.ContainsKey(p.definition.id))
		{
			this.m_ProductUIs[p.definition.id].SetProduct(p, new Action<string>(this.PurchaseButtonClick));
		}
	}

	// Token: 0x06000010 RID: 16 RVA: 0x00002C69 File Offset: 0x00000E69
	private void UpdateProductPendingUI(Product p, int secondsRemaining)
	{
		if (this.m_ProductUIs.ContainsKey(p.definition.id))
		{
			this.m_ProductUIs[p.definition.id].SetPendingTime(secondsRemaining);
		}
	}

	// Token: 0x06000011 RID: 17 RVA: 0x00002CA0 File Offset: 0x00000EA0
	private bool NeedRestoreButton()
	{
		return Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.tvOS || Application.platform == RuntimePlatform.MetroPlayerX86 || Application.platform == RuntimePlatform.MetroPlayerX64 || Application.platform == RuntimePlatform.MetroPlayerARM || this.m_IsSamsungAppsStoreSelected || this.m_IsCloudMoolahStoreSelected;
	}

	// Token: 0x06000012 RID: 18 RVA: 0x00002CF1 File Offset: 0x00000EF1
	private bool NeedLoginButton()
	{
		return this.m_IsUnityChannelSelected;
	}

	// Token: 0x06000013 RID: 19 RVA: 0x00002CF1 File Offset: 0x00000EF1
	private bool NeedValidateButton()
	{
		return this.m_IsUnityChannelSelected;
	}

	// Token: 0x06000014 RID: 20 RVA: 0x00002CFC File Offset: 0x00000EFC
	private void LogProductDefinitions()
	{
		foreach (Product product in this.m_Controller.products.all)
		{
			Debug.Log(string.Format("id: {0}\nstore-specific id: {1}\ntype: {2}\nenabled: {3}\n", new object[]
			{
				product.definition.id,
				product.definition.storeSpecificId,
				product.definition.type.ToString(),
				product.definition.enabled ? "enabled" : "disabled"
			}));
		}
	}

	// Token: 0x06000015 RID: 21 RVA: 0x00002D95 File Offset: 0x00000F95
	public IAPDemo()
	{
	}

	// Token: 0x06000016 RID: 22 RVA: 0x00002DA8 File Offset: 0x00000FA8
	[CompilerGenerated]
	private void <RestoreButtonClick>b__36_0(RestoreTransactionIDState restoreTransactionIDState)
	{
		Debug.Log("restoreTransactionIDState = " + restoreTransactionIDState.ToString());
		bool success = restoreTransactionIDState != RestoreTransactionIDState.RestoreFailed && restoreTransactionIDState != RestoreTransactionIDState.NotKnown;
		this.OnTransactionsRestored(success);
	}

	// Token: 0x06000017 RID: 23 RVA: 0x00002DE7 File Offset: 0x00000FE7
	[CompilerGenerated]
	private void <LoginButtonClick>b__37_0(UserInfo userInfo)
	{
		this.m_IsLoggedIn = true;
		Debug.LogFormat("Succeeded logging into UnityChannel. channel {0}, userId {1}, userLoginToken {2} ", new object[]
		{
			userInfo.channel,
			userInfo.userId,
			userInfo.userLoginToken
		});
	}

	// Token: 0x06000018 RID: 24 RVA: 0x00002E1B File Offset: 0x0000101B
	[CompilerGenerated]
	private void <LoginButtonClick>b__37_1(string message)
	{
		this.m_IsLoggedIn = false;
		Debug.LogError("Failed logging into UnityChannel. " + message);
	}

	// Token: 0x04000001 RID: 1
	private IStoreController m_Controller;

	// Token: 0x04000002 RID: 2
	private IAppleExtensions m_AppleExtensions;

	// Token: 0x04000003 RID: 3
	private IMoolahExtension m_MoolahExtensions;

	// Token: 0x04000004 RID: 4
	private ISamsungAppsExtensions m_SamsungExtensions;

	// Token: 0x04000005 RID: 5
	private IMicrosoftExtensions m_MicrosoftExtensions;

	// Token: 0x04000006 RID: 6
	private IUnityChannelExtensions m_UnityChannelExtensions;

	// Token: 0x04000007 RID: 7
	private ITransactionHistoryExtensions m_TransactionHistoryExtensions;

	// Token: 0x04000008 RID: 8
	private IGooglePlayStoreExtensions m_GooglePlayStoreExtensions;

	// Token: 0x04000009 RID: 9
	private bool m_IsGooglePlayStoreSelected;

	// Token: 0x0400000A RID: 10
	private bool m_IsSamsungAppsStoreSelected;

	// Token: 0x0400000B RID: 11
	private bool m_IsCloudMoolahStoreSelected;

	// Token: 0x0400000C RID: 12
	private bool m_IsUnityChannelSelected;

	// Token: 0x0400000D RID: 13
	private string m_LastTransactionID;

	// Token: 0x0400000E RID: 14
	private bool m_IsLoggedIn;

	// Token: 0x0400000F RID: 15
	private IAPDemo.UnityChannelLoginHandler unityChannelLoginHandler;

	// Token: 0x04000010 RID: 16
	private bool m_FetchReceiptPayloadOnPurchase;

	// Token: 0x04000011 RID: 17
	private bool m_PurchaseInProgress;

	// Token: 0x04000012 RID: 18
	private Dictionary<string, IAPDemoProductUI> m_ProductUIs = new Dictionary<string, IAPDemoProductUI>();

	// Token: 0x04000013 RID: 19
	public GameObject productUITemplate;

	// Token: 0x04000014 RID: 20
	public RectTransform contentRect;

	// Token: 0x04000015 RID: 21
	public Button restoreButton;

	// Token: 0x04000016 RID: 22
	public Button loginButton;

	// Token: 0x04000017 RID: 23
	public Button validateButton;

	// Token: 0x04000018 RID: 24
	public Text versionText;

	// Token: 0x0200000B RID: 11
	[Serializable]
	public class UnityChannelPurchaseError
	{
		// Token: 0x06000046 RID: 70 RVA: 0x00003A05 File Offset: 0x00001C05
		public UnityChannelPurchaseError()
		{
		}

		// Token: 0x0400003D RID: 61
		public string error;

		// Token: 0x0400003E RID: 62
		public IAPDemo.UnityChannelPurchaseInfo purchaseInfo;
	}

	// Token: 0x0200000C RID: 12
	[Serializable]
	public class UnityChannelPurchaseInfo
	{
		// Token: 0x06000047 RID: 71 RVA: 0x00003A05 File Offset: 0x00001C05
		public UnityChannelPurchaseInfo()
		{
		}

		// Token: 0x0400003F RID: 63
		public string productCode;

		// Token: 0x04000040 RID: 64
		public string gameOrderId;

		// Token: 0x04000041 RID: 65
		public string orderQueryToken;
	}

	// Token: 0x0200000D RID: 13
	private class UnityChannelLoginHandler : ILoginListener
	{
		// Token: 0x06000048 RID: 72 RVA: 0x00003A0D File Offset: 0x00001C0D
		public void OnInitialized()
		{
			this.initializeSucceededAction();
		}

		// Token: 0x06000049 RID: 73 RVA: 0x00003A1A File Offset: 0x00001C1A
		public void OnInitializeFailed(string message)
		{
			this.initializeFailedAction(message);
		}

		// Token: 0x0600004A RID: 74 RVA: 0x00003A28 File Offset: 0x00001C28
		public void OnLogin(UserInfo userInfo)
		{
			this.loginSucceededAction(userInfo);
		}

		// Token: 0x0600004B RID: 75 RVA: 0x00003A36 File Offset: 0x00001C36
		public void OnLoginFailed(string message)
		{
			this.loginFailedAction(message);
		}

		// Token: 0x0600004C RID: 76 RVA: 0x00003A05 File Offset: 0x00001C05
		public UnityChannelLoginHandler()
		{
		}

		// Token: 0x04000042 RID: 66
		internal Action initializeSucceededAction;

		// Token: 0x04000043 RID: 67
		internal Action<string> initializeFailedAction;

		// Token: 0x04000044 RID: 68
		internal Action<UserInfo> loginSucceededAction;

		// Token: 0x04000045 RID: 69
		internal Action<string> loginFailedAction;
	}

	// Token: 0x0200000E RID: 14
	[CompilerGenerated]
	private sealed class <>c__DisplayClass30_0
	{
		// Token: 0x0600004D RID: 77 RVA: 0x00003A05 File Offset: 0x00001C05
		public <>c__DisplayClass30_0()
		{
		}

		// Token: 0x0600004E RID: 78 RVA: 0x00003A44 File Offset: 0x00001C44
		internal void <Awake>b__0()
		{
			UnityPurchasing.Initialize(this.<>4__this, this.builder);
		}

		// Token: 0x0600004F RID: 79 RVA: 0x00003A57 File Offset: 0x00001C57
		internal void <Awake>b__2()
		{
			this.initializeUnityIap();
		}

		// Token: 0x04000046 RID: 70
		public IAPDemo <>4__this;

		// Token: 0x04000047 RID: 71
		public ConfigurationBuilder builder;

		// Token: 0x04000048 RID: 72
		public Action initializeUnityIap;
	}

	// Token: 0x0200000F RID: 15
	[CompilerGenerated]
	[Serializable]
	private sealed class <>c
	{
		// Token: 0x06000050 RID: 80 RVA: 0x00003A64 File Offset: 0x00001C64
		// Note: this type is marked as 'beforefieldinit'.
		static <>c()
		{
		}

		// Token: 0x06000051 RID: 81 RVA: 0x00003A05 File Offset: 0x00001C05
		public <>c()
		{
		}

		// Token: 0x06000052 RID: 82 RVA: 0x00003A70 File Offset: 0x00001C70
		internal void <Awake>b__30_1(string message)
		{
			Debug.LogError("Failed to initialize and login to UnityChannel: " + message);
		}

		// Token: 0x04000049 RID: 73
		public static readonly IAPDemo.<>c <>9 = new IAPDemo.<>c();

		// Token: 0x0400004A RID: 74
		public static Action<string> <>9__30_1;
	}

	// Token: 0x02000010 RID: 16
	[CompilerGenerated]
	private sealed class <>c__DisplayClass38_0
	{
		// Token: 0x06000053 RID: 83 RVA: 0x00003A05 File Offset: 0x00001C05
		public <>c__DisplayClass38_0()
		{
		}

		// Token: 0x06000054 RID: 84 RVA: 0x00003A82 File Offset: 0x00001C82
		internal void <ValidateButtonClick>b__0(bool success, string signData, string signature)
		{
			Debug.LogFormat("ValidateReceipt transactionId {0}, success {1}, signData {2}, signature {3}", new object[]
			{
				this.txId,
				success,
				signData,
				signature
			});
		}

		// Token: 0x0400004B RID: 75
		public string txId;
	}
}
