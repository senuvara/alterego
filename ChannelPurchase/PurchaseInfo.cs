﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.ChannelPurchase
{
	// Token: 0x02000004 RID: 4
	public class PurchaseInfo
	{
		// Token: 0x17000001 RID: 1
		// (get) Token: 0x0600000D RID: 13 RVA: 0x0000214D File Offset: 0x0000034D
		// (set) Token: 0x0600000E RID: 14 RVA: 0x00002155 File Offset: 0x00000355
		public string productCode
		{
			[CompilerGenerated]
			get
			{
				return this.<productCode>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<productCode>k__BackingField = value;
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x0600000F RID: 15 RVA: 0x0000215E File Offset: 0x0000035E
		// (set) Token: 0x06000010 RID: 16 RVA: 0x00002166 File Offset: 0x00000366
		public string gameOrderId
		{
			[CompilerGenerated]
			get
			{
				return this.<gameOrderId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<gameOrderId>k__BackingField = value;
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000011 RID: 17 RVA: 0x0000216F File Offset: 0x0000036F
		// (set) Token: 0x06000012 RID: 18 RVA: 0x00002177 File Offset: 0x00000377
		public string orderQueryToken
		{
			[CompilerGenerated]
			get
			{
				return this.<orderQueryToken>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<orderQueryToken>k__BackingField = value;
			}
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000013 RID: 19 RVA: 0x00002180 File Offset: 0x00000380
		// (set) Token: 0x06000014 RID: 20 RVA: 0x00002188 File Offset: 0x00000388
		public string developerPayload
		{
			[CompilerGenerated]
			get
			{
				return this.<developerPayload>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<developerPayload>k__BackingField = value;
			}
		}

		// Token: 0x06000015 RID: 21 RVA: 0x00002133 File Offset: 0x00000333
		public PurchaseInfo()
		{
		}

		// Token: 0x04000002 RID: 2
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <productCode>k__BackingField;

		// Token: 0x04000003 RID: 3
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <gameOrderId>k__BackingField;

		// Token: 0x04000004 RID: 4
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <orderQueryToken>k__BackingField;

		// Token: 0x04000005 RID: 5
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <developerPayload>k__BackingField;
	}
}
