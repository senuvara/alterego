﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.ChannelPurchase
{
	// Token: 0x02000005 RID: 5
	public class ReceiptInfo
	{
		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000016 RID: 22 RVA: 0x00002191 File Offset: 0x00000391
		// (set) Token: 0x06000017 RID: 23 RVA: 0x00002199 File Offset: 0x00000399
		public string gameOrderId
		{
			[CompilerGenerated]
			get
			{
				return this.<gameOrderId>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<gameOrderId>k__BackingField = value;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000018 RID: 24 RVA: 0x000021A2 File Offset: 0x000003A2
		// (set) Token: 0x06000019 RID: 25 RVA: 0x000021AA File Offset: 0x000003AA
		public string signData
		{
			[CompilerGenerated]
			get
			{
				return this.<signData>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<signData>k__BackingField = value;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600001A RID: 26 RVA: 0x000021B3 File Offset: 0x000003B3
		// (set) Token: 0x0600001B RID: 27 RVA: 0x000021BB File Offset: 0x000003BB
		public string signature
		{
			[CompilerGenerated]
			get
			{
				return this.<signature>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<signature>k__BackingField = value;
			}
		}

		// Token: 0x0600001C RID: 28 RVA: 0x00002133 File Offset: 0x00000333
		public ReceiptInfo()
		{
		}

		// Token: 0x04000006 RID: 6
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <gameOrderId>k__BackingField;

		// Token: 0x04000007 RID: 7
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <signData>k__BackingField;

		// Token: 0x04000008 RID: 8
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private string <signature>k__BackingField;
	}
}
