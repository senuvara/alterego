﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Store;

namespace UnityEngine.ChannelPurchase
{
	// Token: 0x02000006 RID: 6
	public class PurchaseForwardCallback : AndroidJavaProxy
	{
		// Token: 0x0600001D RID: 29 RVA: 0x000021C4 File Offset: 0x000003C4
		public PurchaseForwardCallback(IPurchaseListener purchaseListener) : base("com.unity.channel.sdk.PurchaseCallback")
		{
			this.purchaseListener = purchaseListener;
		}

		// Token: 0x0600001E RID: 30 RVA: 0x000021DC File Offset: 0x000003DC
		public void onPurchaseFinished(int resultCode, AndroidJavaObject jo)
		{
			PurchaseInfo purchaseInfo = new PurchaseInfo();
			bool flag = jo == null;
			if (flag)
			{
				purchaseInfo = null;
			}
			else
			{
				purchaseInfo.productCode = jo.Call<string>("getProductCode", new object[0]);
				purchaseInfo.gameOrderId = jo.Call<string>("getGameOrderId", new object[0]);
				purchaseInfo.orderQueryToken = jo.Call<string>("getOrderQueryToken", new object[0]);
				purchaseInfo.developerPayload = jo.Call<string>("getDeveloperPayload", new object[0]);
			}
			bool flag2 = this.purchaseListener == null;
			if (!flag2)
			{
				bool flag3 = resultCode == ResultCode.SDK_PURCHASE_SUCCESS;
				if (flag3)
				{
					MainThreadDispatcher.RunOnMainThread(delegate
					{
						this.purchaseListener.OnPurchase(purchaseInfo);
					});
				}
				else
				{
					bool flag4 = resultCode == ResultCode.SDK_PURCHASE_REPEAT;
					if (flag4)
					{
						string productCode = jo.Call<string>("getProductCode", new object[0]);
						MainThreadDispatcher.RunOnMainThread(delegate
						{
							this.purchaseListener.OnPurchaseRepeated(productCode);
						});
					}
					else
					{
						MainThreadDispatcher.RunOnMainThread(delegate
						{
							this.purchaseListener.OnPurchaseFailed("Purchase Failed: " + resultCode.ToString(), purchaseInfo);
						});
					}
				}
			}
		}

		// Token: 0x0600001F RID: 31 RVA: 0x00002334 File Offset: 0x00000534
		public void onReceiptValidated(int resultCode, AndroidJavaObject jo)
		{
			bool flag = this.purchaseListener == null;
			if (!flag)
			{
				bool flag2 = resultCode == ResultCode.SDK_RECEIPT_VALIDATE_SUCCESS;
				if (flag2)
				{
					ReceiptInfo receiptInfo = new ReceiptInfo();
					receiptInfo.gameOrderId = jo.Call<string>("getGameOrderId", new object[0]);
					receiptInfo.signData = jo.Call<string>("getSignData", new object[0]);
					receiptInfo.signature = jo.Call<string>("getSignature", new object[0]);
					MainThreadDispatcher.RunOnMainThread(delegate
					{
						this.purchaseListener.OnReceiptValidate(receiptInfo);
					});
				}
				else
				{
					string orderId = null;
					bool flag3 = jo != null;
					if (flag3)
					{
						orderId = jo.Call<string>("getGameOrderId", new object[0]);
					}
					MainThreadDispatcher.RunOnMainThread(delegate
					{
						this.purchaseListener.OnReceiptValidateFailed(orderId, "Receipt Validate Failed: " + resultCode.ToString());
					});
				}
			}
		}

		// Token: 0x06000020 RID: 32 RVA: 0x00002454 File Offset: 0x00000654
		public void onPurchaseConfirmed(int resultCode, string gameOrderId)
		{
			bool flag = this.purchaseListener == null;
			if (!flag)
			{
				bool flag2 = resultCode == ResultCode.SDK_CONFIRM_PURCHASE_SUCCESS;
				if (flag2)
				{
					MainThreadDispatcher.RunOnMainThread(delegate
					{
						this.purchaseListener.OnPurchaseConfirm(gameOrderId);
					});
				}
				else
				{
					MainThreadDispatcher.RunOnMainThread(delegate
					{
						this.purchaseListener.OnPurchaseConfirmFailed(gameOrderId, "Purchase Confirm Failed: " + resultCode.ToString());
					});
				}
			}
		}

		// Token: 0x04000009 RID: 9
		private IPurchaseListener purchaseListener;

		// Token: 0x02000007 RID: 7
		[CompilerGenerated]
		private sealed class <>c__DisplayClass2_0
		{
			// Token: 0x06000021 RID: 33 RVA: 0x00002133 File Offset: 0x00000333
			public <>c__DisplayClass2_0()
			{
			}

			// Token: 0x06000022 RID: 34 RVA: 0x000024C8 File Offset: 0x000006C8
			internal void <onPurchaseFinished>b__0()
			{
				this.<>4__this.purchaseListener.OnPurchase(this.purchaseInfo);
			}

			// Token: 0x06000023 RID: 35 RVA: 0x000024E2 File Offset: 0x000006E2
			internal void <onPurchaseFinished>b__2()
			{
				this.<>4__this.purchaseListener.OnPurchaseFailed("Purchase Failed: " + this.resultCode.ToString(), this.purchaseInfo);
			}

			// Token: 0x0400000A RID: 10
			public PurchaseInfo purchaseInfo;

			// Token: 0x0400000B RID: 11
			public int resultCode;

			// Token: 0x0400000C RID: 12
			public PurchaseForwardCallback <>4__this;
		}

		// Token: 0x02000008 RID: 8
		[CompilerGenerated]
		private sealed class <>c__DisplayClass2_1
		{
			// Token: 0x06000024 RID: 36 RVA: 0x00002133 File Offset: 0x00000333
			public <>c__DisplayClass2_1()
			{
			}

			// Token: 0x06000025 RID: 37 RVA: 0x00002511 File Offset: 0x00000711
			internal void <onPurchaseFinished>b__1()
			{
				this.CS$<>8__locals1.<>4__this.purchaseListener.OnPurchaseRepeated(this.productCode);
			}

			// Token: 0x0400000D RID: 13
			public string productCode;

			// Token: 0x0400000E RID: 14
			public PurchaseForwardCallback.<>c__DisplayClass2_0 CS$<>8__locals1;
		}

		// Token: 0x02000009 RID: 9
		[CompilerGenerated]
		private sealed class <>c__DisplayClass3_0
		{
			// Token: 0x06000026 RID: 38 RVA: 0x00002133 File Offset: 0x00000333
			public <>c__DisplayClass3_0()
			{
			}

			// Token: 0x06000027 RID: 39 RVA: 0x00002530 File Offset: 0x00000730
			internal void <onReceiptValidated>b__0()
			{
				this.CS$<>8__locals1.<>4__this.purchaseListener.OnReceiptValidate(this.receiptInfo);
			}

			// Token: 0x0400000F RID: 15
			public ReceiptInfo receiptInfo;

			// Token: 0x04000010 RID: 16
			public PurchaseForwardCallback.<>c__DisplayClass3_2 CS$<>8__locals1;
		}

		// Token: 0x0200000A RID: 10
		[CompilerGenerated]
		private sealed class <>c__DisplayClass3_1
		{
			// Token: 0x06000028 RID: 40 RVA: 0x00002133 File Offset: 0x00000333
			public <>c__DisplayClass3_1()
			{
			}

			// Token: 0x06000029 RID: 41 RVA: 0x0000254F File Offset: 0x0000074F
			internal void <onReceiptValidated>b__1()
			{
				this.CS$<>8__locals2.<>4__this.purchaseListener.OnReceiptValidateFailed(this.orderId, "Receipt Validate Failed: " + this.CS$<>8__locals2.resultCode.ToString());
			}

			// Token: 0x04000011 RID: 17
			public string orderId;

			// Token: 0x04000012 RID: 18
			public PurchaseForwardCallback.<>c__DisplayClass3_2 CS$<>8__locals2;
		}

		// Token: 0x0200000B RID: 11
		[CompilerGenerated]
		private sealed class <>c__DisplayClass3_2
		{
			// Token: 0x0600002A RID: 42 RVA: 0x00002133 File Offset: 0x00000333
			public <>c__DisplayClass3_2()
			{
			}

			// Token: 0x04000013 RID: 19
			public int resultCode;

			// Token: 0x04000014 RID: 20
			public PurchaseForwardCallback <>4__this;
		}

		// Token: 0x0200000C RID: 12
		[CompilerGenerated]
		private sealed class <>c__DisplayClass4_0
		{
			// Token: 0x0600002B RID: 43 RVA: 0x00002133 File Offset: 0x00000333
			public <>c__DisplayClass4_0()
			{
			}

			// Token: 0x0600002C RID: 44 RVA: 0x00002588 File Offset: 0x00000788
			internal void <onPurchaseConfirmed>b__0()
			{
				this.<>4__this.purchaseListener.OnPurchaseConfirm(this.gameOrderId);
			}

			// Token: 0x0600002D RID: 45 RVA: 0x000025A2 File Offset: 0x000007A2
			internal void <onPurchaseConfirmed>b__1()
			{
				this.<>4__this.purchaseListener.OnPurchaseConfirmFailed(this.gameOrderId, "Purchase Confirm Failed: " + this.resultCode.ToString());
			}

			// Token: 0x04000015 RID: 21
			public string gameOrderId;

			// Token: 0x04000016 RID: 22
			public int resultCode;

			// Token: 0x04000017 RID: 23
			public PurchaseForwardCallback <>4__this;
		}
	}
}
