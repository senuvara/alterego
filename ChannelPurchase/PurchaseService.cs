﻿using System;

namespace UnityEngine.ChannelPurchase
{
	// Token: 0x02000002 RID: 2
	public class PurchaseService
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public static void Purchase(string productCode, string gameOrderId, IPurchaseListener listener, string developerPayload = null)
		{
			PurchaseForwardCallback purchaseForwardCallback = new PurchaseForwardCallback(listener);
			AndroidJavaObject androidJavaObject = new AndroidJavaObject("com.unity.channel.sdk.PurchaseInfo", new object[0]);
			androidJavaObject.Set<string>("productCode", productCode);
			androidJavaObject.Set<string>("gameOrderId", gameOrderId);
			bool flag = !string.IsNullOrEmpty(developerPayload);
			if (flag)
			{
				androidJavaObject.Set<string>("developerPayload", developerPayload);
			}
			PurchaseService.serviceClass.CallStatic("purchase", new object[]
			{
				androidJavaObject,
				purchaseForwardCallback
			});
		}

		// Token: 0x06000002 RID: 2 RVA: 0x000020CC File Offset: 0x000002CC
		public static void ValidateReceipt(string gameOrderId, IPurchaseListener listener)
		{
			PurchaseForwardCallback purchaseForwardCallback = new PurchaseForwardCallback(listener);
			PurchaseService.serviceClass.CallStatic("validateReceipt", new object[]
			{
				gameOrderId,
				purchaseForwardCallback
			});
		}

		// Token: 0x06000003 RID: 3 RVA: 0x00002100 File Offset: 0x00000300
		public static void ConfirmPurchase(string gameOrderId, IPurchaseListener listener)
		{
			PurchaseForwardCallback purchaseForwardCallback = new PurchaseForwardCallback(listener);
			PurchaseService.serviceClass.CallStatic("confirmPurchase", new object[]
			{
				gameOrderId,
				purchaseForwardCallback
			});
		}

		// Token: 0x06000004 RID: 4 RVA: 0x00002133 File Offset: 0x00000333
		public PurchaseService()
		{
		}

		// Token: 0x06000005 RID: 5 RVA: 0x0000213C File Offset: 0x0000033C
		// Note: this type is marked as 'beforefieldinit'.
		static PurchaseService()
		{
		}

		// Token: 0x04000001 RID: 1
		private static AndroidJavaClass serviceClass = new AndroidJavaClass("com.unity.channel.sdk.ChannelService");
	}
}
