﻿using System;

namespace UnityEngine.ChannelPurchase
{
	// Token: 0x02000003 RID: 3
	public interface IPurchaseListener
	{
		// Token: 0x06000006 RID: 6
		void OnPurchase(PurchaseInfo purchaseInfo);

		// Token: 0x06000007 RID: 7
		void OnPurchaseFailed(string message, PurchaseInfo purchaseInfo);

		// Token: 0x06000008 RID: 8
		void OnPurchaseRepeated(string productCode);

		// Token: 0x06000009 RID: 9
		void OnReceiptValidate(ReceiptInfo receiptInfo);

		// Token: 0x0600000A RID: 10
		void OnReceiptValidateFailed(string gameOrderId, string message);

		// Token: 0x0600000B RID: 11
		void OnPurchaseConfirm(string gameOrderId);

		// Token: 0x0600000C RID: 12
		void OnPurchaseConfirmFailed(string gameOrderId, string message);
	}
}
