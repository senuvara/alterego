﻿using System;

namespace Unity
{
	// Token: 0x0200003F RID: 63
	internal sealed class ThrowStub : ObjectDisposedException
	{
		// Token: 0x0600018F RID: 399 RVA: 0x000089CA File Offset: 0x00006BCA
		public static void ThrowNotSupportedException()
		{
			throw new PlatformNotSupportedException();
		}
	}
}
