﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000004 RID: 4
	internal class FriendAccessAllowedAttribute : Attribute
	{
		// Token: 0x06000009 RID: 9 RVA: 0x000020AE File Offset: 0x000002AE
		public FriendAccessAllowedAttribute()
		{
		}
	}
}
