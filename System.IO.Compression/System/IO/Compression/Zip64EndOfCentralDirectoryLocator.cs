﻿using System;

namespace System.IO.Compression
{
	// Token: 0x02000026 RID: 38
	internal struct Zip64EndOfCentralDirectoryLocator
	{
		// Token: 0x06000140 RID: 320 RVA: 0x00007817 File Offset: 0x00005A17
		public static bool TryReadBlock(BinaryReader reader, out Zip64EndOfCentralDirectoryLocator zip64EOCDLocator)
		{
			zip64EOCDLocator = default(Zip64EndOfCentralDirectoryLocator);
			if (reader.ReadUInt32() != 117853008U)
			{
				return false;
			}
			zip64EOCDLocator.NumberOfDiskWithZip64EOCD = reader.ReadUInt32();
			zip64EOCDLocator.OffsetOfZip64EOCD = reader.ReadUInt64();
			zip64EOCDLocator.TotalNumberOfDisks = reader.ReadUInt32();
			return true;
		}

		// Token: 0x06000141 RID: 321 RVA: 0x00007854 File Offset: 0x00005A54
		public static void WriteBlock(Stream stream, long zip64EOCDRecordStart)
		{
			BinaryWriter binaryWriter = new BinaryWriter(stream);
			binaryWriter.Write(117853008U);
			binaryWriter.Write(0U);
			binaryWriter.Write(zip64EOCDRecordStart);
			binaryWriter.Write(1U);
		}

		// Token: 0x0400017C RID: 380
		public const uint SignatureConstant = 117853008U;

		// Token: 0x0400017D RID: 381
		public const int SizeOfBlockWithoutSignature = 16;

		// Token: 0x0400017E RID: 382
		public uint NumberOfDiskWithZip64EOCD;

		// Token: 0x0400017F RID: 383
		public ulong OffsetOfZip64EOCD;

		// Token: 0x04000180 RID: 384
		public uint TotalNumberOfDisks;
	}
}
