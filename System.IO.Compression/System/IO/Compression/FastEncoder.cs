﻿using System;

namespace System.IO.Compression
{
	// Token: 0x0200000E RID: 14
	internal sealed class FastEncoder
	{
		// Token: 0x06000048 RID: 72 RVA: 0x00002F4F File Offset: 0x0000114F
		public FastEncoder()
		{
			this._inputWindow = new FastEncoderWindow();
			this._currentMatch = new Match();
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000049 RID: 73 RVA: 0x00002F6D File Offset: 0x0000116D
		internal int BytesInHistory
		{
			get
			{
				return this._inputWindow.BytesAvailable;
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x0600004A RID: 74 RVA: 0x00002F7A File Offset: 0x0000117A
		internal DeflateInput UnprocessedInput
		{
			get
			{
				return this._inputWindow.UnprocessedInput;
			}
		}

		// Token: 0x0600004B RID: 75 RVA: 0x00002F87 File Offset: 0x00001187
		internal void FlushInput()
		{
			this._inputWindow.FlushWindow();
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x0600004C RID: 76 RVA: 0x00002F94 File Offset: 0x00001194
		internal double LastCompressionRatio
		{
			get
			{
				return this._lastCompressionRatio;
			}
		}

		// Token: 0x0600004D RID: 77 RVA: 0x00002F9C File Offset: 0x0000119C
		internal void GetBlock(DeflateInput input, OutputBuffer output, int maxBytesToCopy)
		{
			FastEncoder.WriteDeflatePreamble(output);
			this.GetCompressedOutput(input, output, maxBytesToCopy);
			this.WriteEndOfBlock(output);
		}

		// Token: 0x0600004E RID: 78 RVA: 0x00002FB4 File Offset: 0x000011B4
		internal void GetCompressedData(DeflateInput input, OutputBuffer output)
		{
			this.GetCompressedOutput(input, output, -1);
		}

		// Token: 0x0600004F RID: 79 RVA: 0x00002FBF File Offset: 0x000011BF
		internal void GetBlockHeader(OutputBuffer output)
		{
			FastEncoder.WriteDeflatePreamble(output);
		}

		// Token: 0x06000050 RID: 80 RVA: 0x00002FC7 File Offset: 0x000011C7
		internal void GetBlockFooter(OutputBuffer output)
		{
			this.WriteEndOfBlock(output);
		}

		// Token: 0x06000051 RID: 81 RVA: 0x00002FD0 File Offset: 0x000011D0
		private void GetCompressedOutput(DeflateInput input, OutputBuffer output, int maxBytesToCopy)
		{
			int bytesWritten = output.BytesWritten;
			int num = 0;
			int num2 = this.BytesInHistory + input.Count;
			do
			{
				int num3 = (input.Count < this._inputWindow.FreeWindowSpace) ? input.Count : this._inputWindow.FreeWindowSpace;
				if (maxBytesToCopy >= 1)
				{
					num3 = Math.Min(num3, maxBytesToCopy - num);
				}
				if (num3 > 0)
				{
					this._inputWindow.CopyBytes(input.Buffer, input.StartIndex, num3);
					input.ConsumeBytes(num3);
					num += num3;
				}
				this.GetCompressedOutput(output);
			}
			while (this.SafeToWriteTo(output) && this.InputAvailable(input) && (maxBytesToCopy < 1 || num < maxBytesToCopy));
			int num4 = output.BytesWritten - bytesWritten;
			int num5 = this.BytesInHistory + input.Count;
			int num6 = num2 - num5;
			if (num4 != 0)
			{
				this._lastCompressionRatio = (double)num4 / (double)num6;
			}
		}

		// Token: 0x06000052 RID: 82 RVA: 0x000030A8 File Offset: 0x000012A8
		private void GetCompressedOutput(OutputBuffer output)
		{
			while (this._inputWindow.BytesAvailable > 0 && this.SafeToWriteTo(output))
			{
				this._inputWindow.GetNextSymbolOrMatch(this._currentMatch);
				if (this._currentMatch.State == MatchState.HasSymbol)
				{
					FastEncoder.WriteChar(this._currentMatch.Symbol, output);
				}
				else if (this._currentMatch.State == MatchState.HasMatch)
				{
					FastEncoder.WriteMatch(this._currentMatch.Length, this._currentMatch.Position, output);
				}
				else
				{
					FastEncoder.WriteChar(this._currentMatch.Symbol, output);
					FastEncoder.WriteMatch(this._currentMatch.Length, this._currentMatch.Position, output);
				}
			}
		}

		// Token: 0x06000053 RID: 83 RVA: 0x00003160 File Offset: 0x00001360
		private bool InputAvailable(DeflateInput input)
		{
			return input.Count > 0 || this.BytesInHistory > 0;
		}

		// Token: 0x06000054 RID: 84 RVA: 0x00003176 File Offset: 0x00001376
		private bool SafeToWriteTo(OutputBuffer output)
		{
			return output.FreeBytes > 16;
		}

		// Token: 0x06000055 RID: 85 RVA: 0x00003184 File Offset: 0x00001384
		private void WriteEndOfBlock(OutputBuffer output)
		{
			uint num = FastEncoderStatics.FastEncoderLiteralCodeInfo[256];
			int n = (int)(num & 31U);
			output.WriteBits(n, num >> 5);
		}

		// Token: 0x06000056 RID: 86 RVA: 0x000031AC File Offset: 0x000013AC
		internal static void WriteMatch(int matchLen, int matchPos, OutputBuffer output)
		{
			uint num = FastEncoderStatics.FastEncoderLiteralCodeInfo[254 + matchLen];
			int num2 = (int)(num & 31U);
			if (num2 <= 16)
			{
				output.WriteBits(num2, num >> 5);
			}
			else
			{
				output.WriteBits(16, num >> 5 & 65535U);
				output.WriteBits(num2 - 16, num >> 21);
			}
			num = FastEncoderStatics.FastEncoderDistanceCodeInfo[FastEncoderStatics.GetSlot(matchPos)];
			output.WriteBits((int)(num & 15U), num >> 8);
			int num3 = (int)(num >> 4 & 15U);
			if (num3 != 0)
			{
				output.WriteBits(num3, (uint)(matchPos & (int)FastEncoderStatics.BitMask[num3]));
			}
		}

		// Token: 0x06000057 RID: 87 RVA: 0x00003230 File Offset: 0x00001430
		internal static void WriteChar(byte b, OutputBuffer output)
		{
			uint num = FastEncoderStatics.FastEncoderLiteralCodeInfo[(int)b];
			output.WriteBits((int)(num & 31U), num >> 5);
		}

		// Token: 0x06000058 RID: 88 RVA: 0x00003252 File Offset: 0x00001452
		internal static void WriteDeflatePreamble(OutputBuffer output)
		{
			output.WriteBytes(FastEncoderStatics.FastEncoderTreeStructureData, 0, FastEncoderStatics.FastEncoderTreeStructureData.Length);
			output.WriteBits(9, 34U);
		}

		// Token: 0x040000A9 RID: 169
		private readonly FastEncoderWindow _inputWindow;

		// Token: 0x040000AA RID: 170
		private readonly Match _currentMatch;

		// Token: 0x040000AB RID: 171
		private double _lastCompressionRatio;
	}
}
