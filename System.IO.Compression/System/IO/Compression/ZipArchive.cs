﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;

namespace System.IO.Compression
{
	/// <summary>Represents a package of compressed files in the zip archive format.</summary>
	// Token: 0x0200001D RID: 29
	public class ZipArchive : IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.IO.Compression.ZipArchive" /> class from the specified stream.</summary>
		/// <param name="stream">The stream that contains the archive to be read.</param>
		/// <exception cref="T:System.ArgumentException">The stream is already closed or does not support reading.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="stream" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.IO.InvalidDataException">The contents of the stream are not in the zip archive format.</exception>
		// Token: 0x060000CA RID: 202 RVA: 0x00005143 File Offset: 0x00003343
		public ZipArchive(Stream stream) : this(stream, ZipArchiveMode.Read, false, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.Compression.ZipArchive" /> class from the specified stream and with the specified mode.</summary>
		/// <param name="stream">The input or output stream.</param>
		/// <param name="mode">One of the enumeration values that indicates whether the zip archive is used to read, create, or update entries.</param>
		/// <exception cref="T:System.ArgumentException">The stream is already closed, or the capabilities of the stream do not match the mode.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="stream" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="mode" /> is an invalid value.</exception>
		/// <exception cref="T:System.IO.InvalidDataException">The contents of the stream could not be interpreted as a zip archive.-or-
		///         <paramref name="mode" /> is <see cref="F:System.IO.Compression.ZipArchiveMode.Update" /> and an entry is missing from the archive or is corrupt and cannot be read.-or-
		///         <paramref name="mode" /> is <see cref="F:System.IO.Compression.ZipArchiveMode.Update" /> and an entry is too large to fit into memory.</exception>
		// Token: 0x060000CB RID: 203 RVA: 0x0000514F File Offset: 0x0000334F
		public ZipArchive(Stream stream, ZipArchiveMode mode) : this(stream, mode, false, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.Compression.ZipArchive" /> class on the specified stream for the specified mode, and optionally leaves the stream open.</summary>
		/// <param name="stream">The input or output stream.</param>
		/// <param name="mode">One of the enumeration values that indicates whether the zip archive is used to read, create, or update entries.</param>
		/// <param name="leaveOpen">
		///       <see langword="true" /> to leave the stream open after the <see cref="T:System.IO.Compression.ZipArchive" /> object is disposed; otherwise, <see langword="false" />.</param>
		/// <exception cref="T:System.ArgumentException">The stream is already closed, or the capabilities of the stream do not match the mode.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="stream" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="mode" /> is an invalid value.</exception>
		/// <exception cref="T:System.IO.InvalidDataException">The contents of the stream could not be interpreted as a zip archive.-or-
		///         <paramref name="mode" /> is <see cref="F:System.IO.Compression.ZipArchiveMode.Update" /> and an entry is missing from the archive or is corrupt and cannot be read.-or-
		///         <paramref name="mode" /> is <see cref="F:System.IO.Compression.ZipArchiveMode.Update" /> and an entry is too large to fit into memory.</exception>
		// Token: 0x060000CC RID: 204 RVA: 0x0000515B File Offset: 0x0000335B
		public ZipArchive(Stream stream, ZipArchiveMode mode, bool leaveOpen) : this(stream, mode, leaveOpen, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.Compression.ZipArchive" /> class on the specified stream for the specified mode, uses the specified encoding for entry names, and optionally leaves the stream open.</summary>
		/// <param name="stream">The input or output stream.</param>
		/// <param name="mode">One of the enumeration values that indicates whether the zip archive is used to read, create, or update entries.</param>
		/// <param name="leaveOpen">
		///       <see langword="true" /> to leave the stream open after the <see cref="T:System.IO.Compression.ZipArchive" /> object is disposed; otherwise, <see langword="false" />.</param>
		/// <param name="entryNameEncoding">The encoding to use when reading or writing entry names in this archive. Specify a value for this parameter only when an encoding is required for interoperability with zip archive tools and libraries that do not support UTF-8 encoding for entry names.</param>
		/// <exception cref="T:System.ArgumentException">The stream is already closed, or the capabilities of the stream do not match the mode.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="stream" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="mode" /> is an invalid value.</exception>
		/// <exception cref="T:System.IO.InvalidDataException">The contents of the stream could not be interpreted as a zip archive.-or-
		///         <paramref name="mode" /> is <see cref="F:System.IO.Compression.ZipArchiveMode.Update" /> and an entry is missing from the archive or is corrupt and cannot be read.-or-
		///         <paramref name="mode" /> is <see cref="F:System.IO.Compression.ZipArchiveMode.Update" /> and an entry is too large to fit into memory.</exception>
		// Token: 0x060000CD RID: 205 RVA: 0x00005167 File Offset: 0x00003367
		public ZipArchive(Stream stream, ZipArchiveMode mode, bool leaveOpen, Encoding entryNameEncoding)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			this.EntryNameEncoding = entryNameEncoding;
			this.Init(stream, mode, leaveOpen);
		}

		/// <summary>Gets the collection of entries that are currently in the zip archive.</summary>
		/// <returns>The collection of entries that are currently in the zip archive.</returns>
		/// <exception cref="T:System.NotSupportedException">The zip archive does not support reading.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The zip archive has been disposed.</exception>
		/// <exception cref="T:System.IO.InvalidDataException">The zip archive is corrupt, and its entries cannot be retrieved.</exception>
		// Token: 0x17000025 RID: 37
		// (get) Token: 0x060000CE RID: 206 RVA: 0x0000518E File Offset: 0x0000338E
		public ReadOnlyCollection<ZipArchiveEntry> Entries
		{
			get
			{
				if (this._mode == ZipArchiveMode.Create)
				{
					throw new NotSupportedException("Cannot access entries in Create mode.");
				}
				this.ThrowIfDisposed();
				this.EnsureCentralDirectoryRead();
				return this._entriesCollection;
			}
		}

		/// <summary>Gets a value that describes the type of action the zip archive can perform on entries.</summary>
		/// <returns>One of the enumeration values that describes the type of action (read, create, or update) the zip archive can perform on entries.</returns>
		// Token: 0x17000026 RID: 38
		// (get) Token: 0x060000CF RID: 207 RVA: 0x000051B6 File Offset: 0x000033B6
		public ZipArchiveMode Mode
		{
			get
			{
				return this._mode;
			}
		}

		/// <summary>Creates an empty entry that has the specified path and entry name in the zip archive.</summary>
		/// <param name="entryName">A path, relative to the root of the archive, that specifies the name of the entry to be created.</param>
		/// <returns>An empty entry in the zip archive.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="entryName" /> is <see cref="F:System.String.Empty" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="entryName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.NotSupportedException">The zip archive does not support writing.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The zip archive has been disposed.</exception>
		// Token: 0x060000D0 RID: 208 RVA: 0x000051C0 File Offset: 0x000033C0
		public ZipArchiveEntry CreateEntry(string entryName)
		{
			return this.DoCreateEntry(entryName, null);
		}

		/// <summary>Creates an empty entry that has the specified entry name and compression level in the zip archive.</summary>
		/// <param name="entryName">A path, relative to the root of the archive, that specifies the name of the entry to be created.</param>
		/// <param name="compressionLevel">One of the enumeration values that indicates whether to emphasize speed or compression effectiveness when creating the entry.</param>
		/// <returns>An empty entry in the zip archive.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="entryName" /> is <see cref="F:System.String.Empty" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="entryName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.NotSupportedException">The zip archive does not support writing.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The zip archive has been disposed.</exception>
		// Token: 0x060000D1 RID: 209 RVA: 0x000051DD File Offset: 0x000033DD
		public ZipArchiveEntry CreateEntry(string entryName, CompressionLevel compressionLevel)
		{
			return this.DoCreateEntry(entryName, new CompressionLevel?(compressionLevel));
		}

		/// <summary>Called by the <see cref="M:System.IO.Compression.ZipArchive.Dispose" /> and <see cref="M:System.Object.Finalize" /> methods to release the unmanaged resources used by the current instance of the <see cref="T:System.IO.Compression.ZipArchive" /> class, and optionally finishes writing the archive and releases the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to finish writing the archive and release unmanaged and managed resources; <see langword="false" /> to release only unmanaged resources.</param>
		// Token: 0x060000D2 RID: 210 RVA: 0x000051EC File Offset: 0x000033EC
		protected virtual void Dispose(bool disposing)
		{
			if (disposing && !this._isDisposed)
			{
				try
				{
					ZipArchiveMode mode = this._mode;
					if (mode != ZipArchiveMode.Read)
					{
						int num = mode - ZipArchiveMode.Create;
						this.WriteFile();
					}
				}
				finally
				{
					this.CloseStreams();
					this._isDisposed = true;
				}
			}
		}

		/// <summary>Releases the resources used by the current instance of the <see cref="T:System.IO.Compression.ZipArchive" /> class.</summary>
		// Token: 0x060000D3 RID: 211 RVA: 0x0000523C File Offset: 0x0000343C
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>Retrieves a wrapper for the specified entry in the zip archive.</summary>
		/// <param name="entryName">A path, relative to the root of the archive, that identifies the entry to retrieve.</param>
		/// <returns>A wrapper for the specified entry in the archive; <see langword="null" /> if the entry does not exist in the archive.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="entryName" /> is <see cref="F:System.String.Empty" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="entryName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.NotSupportedException">The zip archive does not support reading.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The zip archive has been disposed.</exception>
		/// <exception cref="T:System.IO.InvalidDataException">The zip archive is corrupt, and its entries cannot be retrieved.</exception>
		// Token: 0x060000D4 RID: 212 RVA: 0x0000524C File Offset: 0x0000344C
		public ZipArchiveEntry GetEntry(string entryName)
		{
			if (entryName == null)
			{
				throw new ArgumentNullException("entryName");
			}
			if (this._mode == ZipArchiveMode.Create)
			{
				throw new NotSupportedException("Cannot access entries in Create mode.");
			}
			this.EnsureCentralDirectoryRead();
			ZipArchiveEntry result;
			this._entriesDictionary.TryGetValue(entryName, out result);
			return result;
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x060000D5 RID: 213 RVA: 0x00005291 File Offset: 0x00003491
		internal BinaryReader ArchiveReader
		{
			get
			{
				return this._archiveReader;
			}
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x060000D6 RID: 214 RVA: 0x00005299 File Offset: 0x00003499
		internal Stream ArchiveStream
		{
			get
			{
				return this._archiveStream;
			}
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x060000D7 RID: 215 RVA: 0x000052A1 File Offset: 0x000034A1
		internal uint NumberOfThisDisk
		{
			get
			{
				return this._numberOfThisDisk;
			}
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x060000D8 RID: 216 RVA: 0x000052A9 File Offset: 0x000034A9
		// (set) Token: 0x060000D9 RID: 217 RVA: 0x000052B1 File Offset: 0x000034B1
		internal Encoding EntryNameEncoding
		{
			get
			{
				return this._entryNameEncoding;
			}
			private set
			{
				if (value != null && (value.Equals(Encoding.BigEndianUnicode) || value.Equals(Encoding.Unicode)))
				{
					throw new ArgumentException("The specified entry name encoding is not supported.", "EntryNameEncoding");
				}
				this._entryNameEncoding = value;
			}
		}

		// Token: 0x060000DA RID: 218 RVA: 0x000052E8 File Offset: 0x000034E8
		private ZipArchiveEntry DoCreateEntry(string entryName, CompressionLevel? compressionLevel)
		{
			if (entryName == null)
			{
				throw new ArgumentNullException("entryName");
			}
			if (string.IsNullOrEmpty(entryName))
			{
				throw new ArgumentException("String cannot be empty.", "entryName");
			}
			if (this._mode == ZipArchiveMode.Read)
			{
				throw new NotSupportedException("Cannot create entries on an archive opened in read mode.");
			}
			this.ThrowIfDisposed();
			ZipArchiveEntry zipArchiveEntry = (compressionLevel != null) ? new ZipArchiveEntry(this, entryName, compressionLevel.Value) : new ZipArchiveEntry(this, entryName);
			this.AddEntry(zipArchiveEntry);
			return zipArchiveEntry;
		}

		// Token: 0x060000DB RID: 219 RVA: 0x0000535D File Offset: 0x0000355D
		internal void AcquireArchiveStream(ZipArchiveEntry entry)
		{
			if (this._archiveStreamOwner != null)
			{
				if (this._archiveStreamOwner.EverOpenedForWrite)
				{
					throw new IOException("Entries cannot be created while previously created entries are still open.");
				}
				this._archiveStreamOwner.WriteAndFinishLocalEntry();
			}
			this._archiveStreamOwner = entry;
		}

		// Token: 0x060000DC RID: 220 RVA: 0x00005394 File Offset: 0x00003594
		private void AddEntry(ZipArchiveEntry entry)
		{
			this._entries.Add(entry);
			string fullName = entry.FullName;
			if (!this._entriesDictionary.ContainsKey(fullName))
			{
				this._entriesDictionary.Add(fullName, entry);
			}
		}

		// Token: 0x060000DD RID: 221 RVA: 0x00002F4D File Offset: 0x0000114D
		[Conditional("DEBUG")]
		internal void DebugAssertIsStillArchiveStreamOwner(ZipArchiveEntry entry)
		{
		}

		// Token: 0x060000DE RID: 222 RVA: 0x000053CF File Offset: 0x000035CF
		internal void ReleaseArchiveStream(ZipArchiveEntry entry)
		{
			this._archiveStreamOwner = null;
		}

		// Token: 0x060000DF RID: 223 RVA: 0x000053D8 File Offset: 0x000035D8
		internal void RemoveEntry(ZipArchiveEntry entry)
		{
			this._entries.Remove(entry);
			this._entriesDictionary.Remove(entry.FullName);
		}

		// Token: 0x060000E0 RID: 224 RVA: 0x000053F9 File Offset: 0x000035F9
		internal void ThrowIfDisposed()
		{
			if (this._isDisposed)
			{
				throw new ObjectDisposedException(base.GetType().ToString());
			}
		}

		// Token: 0x060000E1 RID: 225 RVA: 0x00005414 File Offset: 0x00003614
		private void CloseStreams()
		{
			if (this._leaveOpen)
			{
				if (this._backingStream != null)
				{
					this._archiveStream.Dispose();
				}
				return;
			}
			this._archiveStream.Dispose();
			Stream backingStream = this._backingStream;
			if (backingStream != null)
			{
				backingStream.Dispose();
			}
			BinaryReader archiveReader = this._archiveReader;
			if (archiveReader == null)
			{
				return;
			}
			archiveReader.Dispose();
		}

		// Token: 0x060000E2 RID: 226 RVA: 0x00005469 File Offset: 0x00003669
		private void EnsureCentralDirectoryRead()
		{
			if (!this._readEntries)
			{
				this.ReadCentralDirectory();
				this._readEntries = true;
			}
		}

		// Token: 0x060000E3 RID: 227 RVA: 0x00005480 File Offset: 0x00003680
		private void Init(Stream stream, ZipArchiveMode mode, bool leaveOpen)
		{
			Stream stream2 = null;
			try
			{
				this._backingStream = null;
				switch (mode)
				{
				case ZipArchiveMode.Read:
					if (!stream.CanRead)
					{
						throw new ArgumentException("Cannot use read mode on a non-readable stream.");
					}
					if (!stream.CanSeek)
					{
						this._backingStream = stream;
						stream = (stream2 = new MemoryStream());
						this._backingStream.CopyTo(stream);
						stream.Seek(0L, SeekOrigin.Begin);
					}
					break;
				case ZipArchiveMode.Create:
					if (!stream.CanWrite)
					{
						throw new ArgumentException("Cannot use create mode on a non-writable stream.");
					}
					break;
				case ZipArchiveMode.Update:
					if (!stream.CanRead || !stream.CanWrite || !stream.CanSeek)
					{
						throw new ArgumentException("Update mode requires a stream with read, write, and seek capabilities.");
					}
					break;
				default:
					throw new ArgumentOutOfRangeException("mode");
				}
				this._mode = mode;
				if (mode == ZipArchiveMode.Create && !stream.CanSeek)
				{
					this._archiveStream = new PositionPreservingWriteOnlyStreamWrapper(stream);
				}
				else
				{
					this._archiveStream = stream;
				}
				this._archiveStreamOwner = null;
				if (mode == ZipArchiveMode.Create)
				{
					this._archiveReader = null;
				}
				else
				{
					this._archiveReader = new BinaryReader(this._archiveStream);
				}
				this._entries = new List<ZipArchiveEntry>();
				this._entriesCollection = new ReadOnlyCollection<ZipArchiveEntry>(this._entries);
				this._entriesDictionary = new Dictionary<string, ZipArchiveEntry>();
				this._readEntries = false;
				this._leaveOpen = leaveOpen;
				this._centralDirectoryStart = 0L;
				this._isDisposed = false;
				this._numberOfThisDisk = 0U;
				this._archiveComment = null;
				switch (mode)
				{
				case ZipArchiveMode.Read:
					this.ReadEndOfCentralDirectory();
					goto IL_1BC;
				case ZipArchiveMode.Create:
					this._readEntries = true;
					goto IL_1BC;
				}
				if (this._archiveStream.Length == 0L)
				{
					this._readEntries = true;
				}
				else
				{
					this.ReadEndOfCentralDirectory();
					this.EnsureCentralDirectoryRead();
					foreach (ZipArchiveEntry zipArchiveEntry in this._entries)
					{
						zipArchiveEntry.ThrowIfNotOpenable(false, true);
					}
				}
				IL_1BC:;
			}
			catch
			{
				if (stream2 != null)
				{
					stream2.Dispose();
				}
				throw;
			}
		}

		// Token: 0x060000E4 RID: 228 RVA: 0x0000568C File Offset: 0x0000388C
		private void ReadCentralDirectory()
		{
			try
			{
				this._archiveStream.Seek(this._centralDirectoryStart, SeekOrigin.Begin);
				long num = 0L;
				bool saveExtraFieldsAndComments = this.Mode == ZipArchiveMode.Update;
				ZipCentralDirectoryFileHeader cd;
				while (ZipCentralDirectoryFileHeader.TryReadBlock(this._archiveReader, saveExtraFieldsAndComments, out cd))
				{
					this.AddEntry(new ZipArchiveEntry(this, cd));
					num += 1L;
				}
				if (num != this._expectedNumberOfEntries)
				{
					throw new InvalidDataException("Number of entries expected in End Of Central Directory does not correspond to number of entries in Central Directory.");
				}
			}
			catch (EndOfStreamException p)
			{
				throw new InvalidDataException(SR.Format("Central Directory is invalid.", p));
			}
		}

		// Token: 0x060000E5 RID: 229 RVA: 0x00005718 File Offset: 0x00003918
		private void ReadEndOfCentralDirectory()
		{
			try
			{
				this._archiveStream.Seek(-18L, SeekOrigin.End);
				if (!ZipHelper.SeekBackwardsToSignature(this._archiveStream, 101010256U))
				{
					throw new InvalidDataException("End of Central Directory record could not be found.");
				}
				long position = this._archiveStream.Position;
				ZipEndOfCentralDirectoryBlock zipEndOfCentralDirectoryBlock;
				ZipEndOfCentralDirectoryBlock.TryReadBlock(this._archiveReader, out zipEndOfCentralDirectoryBlock);
				if (zipEndOfCentralDirectoryBlock.NumberOfThisDisk != zipEndOfCentralDirectoryBlock.NumberOfTheDiskWithTheStartOfTheCentralDirectory)
				{
					throw new InvalidDataException("Split or spanned archives are not supported.");
				}
				this._numberOfThisDisk = (uint)zipEndOfCentralDirectoryBlock.NumberOfThisDisk;
				this._centralDirectoryStart = (long)((ulong)zipEndOfCentralDirectoryBlock.OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber);
				if (zipEndOfCentralDirectoryBlock.NumberOfEntriesInTheCentralDirectory != zipEndOfCentralDirectoryBlock.NumberOfEntriesInTheCentralDirectoryOnThisDisk)
				{
					throw new InvalidDataException("Split or spanned archives are not supported.");
				}
				this._expectedNumberOfEntries = (long)((ulong)zipEndOfCentralDirectoryBlock.NumberOfEntriesInTheCentralDirectory);
				if (this._mode == ZipArchiveMode.Update)
				{
					this._archiveComment = zipEndOfCentralDirectoryBlock.ArchiveComment;
				}
				if (zipEndOfCentralDirectoryBlock.NumberOfThisDisk == 65535 || zipEndOfCentralDirectoryBlock.OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber == 4294967295U || zipEndOfCentralDirectoryBlock.NumberOfEntriesInTheCentralDirectory == 65535)
				{
					this._archiveStream.Seek(position - 16L, SeekOrigin.Begin);
					if (ZipHelper.SeekBackwardsToSignature(this._archiveStream, 117853008U))
					{
						Zip64EndOfCentralDirectoryLocator zip64EndOfCentralDirectoryLocator;
						Zip64EndOfCentralDirectoryLocator.TryReadBlock(this._archiveReader, out zip64EndOfCentralDirectoryLocator);
						if (zip64EndOfCentralDirectoryLocator.OffsetOfZip64EOCD > 9223372036854775807UL)
						{
							throw new InvalidDataException("Offset to Zip64 End Of Central Directory record cannot be held in an Int64.");
						}
						long offsetOfZip64EOCD = (long)zip64EndOfCentralDirectoryLocator.OffsetOfZip64EOCD;
						this._archiveStream.Seek(offsetOfZip64EOCD, SeekOrigin.Begin);
						Zip64EndOfCentralDirectoryRecord zip64EndOfCentralDirectoryRecord;
						if (!Zip64EndOfCentralDirectoryRecord.TryReadBlock(this._archiveReader, out zip64EndOfCentralDirectoryRecord))
						{
							throw new InvalidDataException("Zip 64 End of Central Directory Record not where indicated.");
						}
						this._numberOfThisDisk = zip64EndOfCentralDirectoryRecord.NumberOfThisDisk;
						if (zip64EndOfCentralDirectoryRecord.NumberOfEntriesTotal > 9223372036854775807UL)
						{
							throw new InvalidDataException("Number of Entries cannot be held in an Int64.");
						}
						if (zip64EndOfCentralDirectoryRecord.OffsetOfCentralDirectory > 9223372036854775807UL)
						{
							throw new InvalidDataException("Offset to Central Directory cannot be held in an Int64.");
						}
						if (zip64EndOfCentralDirectoryRecord.NumberOfEntriesTotal != zip64EndOfCentralDirectoryRecord.NumberOfEntriesOnThisDisk)
						{
							throw new InvalidDataException("Split or spanned archives are not supported.");
						}
						this._expectedNumberOfEntries = (long)zip64EndOfCentralDirectoryRecord.NumberOfEntriesTotal;
						this._centralDirectoryStart = (long)zip64EndOfCentralDirectoryRecord.OffsetOfCentralDirectory;
					}
				}
				if (this._centralDirectoryStart > this._archiveStream.Length)
				{
					throw new InvalidDataException("Offset to Central Directory cannot be held in an Int64.");
				}
			}
			catch (EndOfStreamException innerException)
			{
				throw new InvalidDataException("Central Directory corrupt.", innerException);
			}
			catch (IOException innerException2)
			{
				throw new InvalidDataException("Central Directory corrupt.", innerException2);
			}
		}

		// Token: 0x060000E6 RID: 230 RVA: 0x00005970 File Offset: 0x00003B70
		private void WriteFile()
		{
			if (this._mode == ZipArchiveMode.Update)
			{
				List<ZipArchiveEntry> list = new List<ZipArchiveEntry>();
				foreach (ZipArchiveEntry zipArchiveEntry in this._entries)
				{
					if (!zipArchiveEntry.LoadLocalHeaderExtraFieldAndCompressedBytesIfNeeded())
					{
						list.Add(zipArchiveEntry);
					}
				}
				foreach (ZipArchiveEntry zipArchiveEntry2 in list)
				{
					zipArchiveEntry2.Delete();
				}
				this._archiveStream.Seek(0L, SeekOrigin.Begin);
				this._archiveStream.SetLength(0L);
			}
			foreach (ZipArchiveEntry zipArchiveEntry3 in this._entries)
			{
				zipArchiveEntry3.WriteAndFinishLocalEntry();
			}
			long position = this._archiveStream.Position;
			foreach (ZipArchiveEntry zipArchiveEntry4 in this._entries)
			{
				zipArchiveEntry4.WriteCentralDirectoryFileHeader();
			}
			long sizeOfCentralDirectory = this._archiveStream.Position - position;
			this.WriteArchiveEpilogue(position, sizeOfCentralDirectory);
		}

		// Token: 0x060000E7 RID: 231 RVA: 0x00005AD8 File Offset: 0x00003CD8
		private void WriteArchiveEpilogue(long startOfCentralDirectory, long sizeOfCentralDirectory)
		{
			if (startOfCentralDirectory >= (long)((ulong)-1) || sizeOfCentralDirectory >= (long)((ulong)-1) || this._entries.Count >= 65535)
			{
				long position = this._archiveStream.Position;
				Zip64EndOfCentralDirectoryRecord.WriteBlock(this._archiveStream, (long)this._entries.Count, startOfCentralDirectory, sizeOfCentralDirectory);
				Zip64EndOfCentralDirectoryLocator.WriteBlock(this._archiveStream, position);
			}
			ZipEndOfCentralDirectoryBlock.WriteBlock(this._archiveStream, (long)this._entries.Count, startOfCentralDirectory, sizeOfCentralDirectory, this._archiveComment);
		}

		// Token: 0x0400012B RID: 299
		private Stream _archiveStream;

		// Token: 0x0400012C RID: 300
		private ZipArchiveEntry _archiveStreamOwner;

		// Token: 0x0400012D RID: 301
		private BinaryReader _archiveReader;

		// Token: 0x0400012E RID: 302
		private ZipArchiveMode _mode;

		// Token: 0x0400012F RID: 303
		private List<ZipArchiveEntry> _entries;

		// Token: 0x04000130 RID: 304
		private ReadOnlyCollection<ZipArchiveEntry> _entriesCollection;

		// Token: 0x04000131 RID: 305
		private Dictionary<string, ZipArchiveEntry> _entriesDictionary;

		// Token: 0x04000132 RID: 306
		private bool _readEntries;

		// Token: 0x04000133 RID: 307
		private bool _leaveOpen;

		// Token: 0x04000134 RID: 308
		private long _centralDirectoryStart;

		// Token: 0x04000135 RID: 309
		private bool _isDisposed;

		// Token: 0x04000136 RID: 310
		private uint _numberOfThisDisk;

		// Token: 0x04000137 RID: 311
		private long _expectedNumberOfEntries;

		// Token: 0x04000138 RID: 312
		private Stream _backingStream;

		// Token: 0x04000139 RID: 313
		private byte[] _archiveComment;

		// Token: 0x0400013A RID: 314
		private Encoding _entryNameEncoding;
	}
}
