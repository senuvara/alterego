﻿using System;

namespace System.IO.Compression
{
	// Token: 0x0200000C RID: 12
	internal sealed class DeflaterManaged : IDisposable
	{
		// Token: 0x0600003F RID: 63 RVA: 0x00002BA6 File Offset: 0x00000DA6
		internal DeflaterManaged()
		{
			this._deflateEncoder = new FastEncoder();
			this._copyEncoder = new CopyEncoder();
			this._input = new DeflateInput();
			this._output = new OutputBuffer();
			this._processingState = DeflaterManaged.DeflaterState.NotStarted;
		}

		// Token: 0x06000040 RID: 64 RVA: 0x00002BE1 File Offset: 0x00000DE1
		internal bool NeedsInput()
		{
			return this._input.Count == 0 && this._deflateEncoder.BytesInHistory == 0;
		}

		// Token: 0x06000041 RID: 65 RVA: 0x00002C00 File Offset: 0x00000E00
		internal void SetInput(byte[] inputBuffer, int startIndex, int count)
		{
			this._input.Buffer = inputBuffer;
			this._input.Count = count;
			this._input.StartIndex = startIndex;
			if (count > 0 && count < 256)
			{
				DeflaterManaged.DeflaterState processingState = this._processingState;
				if (processingState != DeflaterManaged.DeflaterState.NotStarted)
				{
					if (processingState == DeflaterManaged.DeflaterState.CompressThenCheck)
					{
						this._processingState = DeflaterManaged.DeflaterState.HandlingSmallData;
						return;
					}
					if (processingState != DeflaterManaged.DeflaterState.CheckingForIncompressible)
					{
						return;
					}
				}
				this._processingState = DeflaterManaged.DeflaterState.StartingSmallData;
				return;
			}
		}

		// Token: 0x06000042 RID: 66 RVA: 0x00002C60 File Offset: 0x00000E60
		internal int GetDeflateOutput(byte[] outputBuffer)
		{
			this._output.UpdateBuffer(outputBuffer);
			switch (this._processingState)
			{
			case DeflaterManaged.DeflaterState.NotStarted:
			{
				DeflateInput.InputState state = this._input.DumpState();
				OutputBuffer.BufferState state2 = this._output.DumpState();
				this._deflateEncoder.GetBlockHeader(this._output);
				this._deflateEncoder.GetCompressedData(this._input, this._output);
				if (!this.UseCompressed(this._deflateEncoder.LastCompressionRatio))
				{
					this._input.RestoreState(state);
					this._output.RestoreState(state2);
					this._copyEncoder.GetBlock(this._input, this._output, false);
					this.FlushInputWindows();
					this._processingState = DeflaterManaged.DeflaterState.CheckingForIncompressible;
					goto IL_23A;
				}
				this._processingState = DeflaterManaged.DeflaterState.CompressThenCheck;
				goto IL_23A;
			}
			case DeflaterManaged.DeflaterState.SlowDownForIncompressible1:
				this._deflateEncoder.GetBlockFooter(this._output);
				this._processingState = DeflaterManaged.DeflaterState.SlowDownForIncompressible2;
				break;
			case DeflaterManaged.DeflaterState.SlowDownForIncompressible2:
				break;
			case DeflaterManaged.DeflaterState.StartingSmallData:
				this._deflateEncoder.GetBlockHeader(this._output);
				this._processingState = DeflaterManaged.DeflaterState.HandlingSmallData;
				goto IL_223;
			case DeflaterManaged.DeflaterState.CompressThenCheck:
				this._deflateEncoder.GetCompressedData(this._input, this._output);
				if (!this.UseCompressed(this._deflateEncoder.LastCompressionRatio))
				{
					this._processingState = DeflaterManaged.DeflaterState.SlowDownForIncompressible1;
					this._inputFromHistory = this._deflateEncoder.UnprocessedInput;
					goto IL_23A;
				}
				goto IL_23A;
			case DeflaterManaged.DeflaterState.CheckingForIncompressible:
			{
				DeflateInput.InputState state3 = this._input.DumpState();
				OutputBuffer.BufferState state4 = this._output.DumpState();
				this._deflateEncoder.GetBlock(this._input, this._output, 8072);
				if (!this.UseCompressed(this._deflateEncoder.LastCompressionRatio))
				{
					this._input.RestoreState(state3);
					this._output.RestoreState(state4);
					this._copyEncoder.GetBlock(this._input, this._output, false);
					this.FlushInputWindows();
					goto IL_23A;
				}
				goto IL_23A;
			}
			case DeflaterManaged.DeflaterState.HandlingSmallData:
				goto IL_223;
			default:
				goto IL_23A;
			}
			if (this._inputFromHistory.Count > 0)
			{
				this._copyEncoder.GetBlock(this._inputFromHistory, this._output, false);
			}
			if (this._inputFromHistory.Count == 0)
			{
				this._deflateEncoder.FlushInput();
				this._processingState = DeflaterManaged.DeflaterState.CheckingForIncompressible;
				goto IL_23A;
			}
			goto IL_23A;
			IL_223:
			this._deflateEncoder.GetCompressedData(this._input, this._output);
			IL_23A:
			return this._output.BytesWritten;
		}

		// Token: 0x06000043 RID: 67 RVA: 0x00002EB4 File Offset: 0x000010B4
		internal bool Finish(byte[] outputBuffer, out int bytesRead)
		{
			if (this._processingState == DeflaterManaged.DeflaterState.NotStarted)
			{
				bytesRead = 0;
				return true;
			}
			this._output.UpdateBuffer(outputBuffer);
			if (this._processingState == DeflaterManaged.DeflaterState.CompressThenCheck || this._processingState == DeflaterManaged.DeflaterState.HandlingSmallData || this._processingState == DeflaterManaged.DeflaterState.SlowDownForIncompressible1)
			{
				this._deflateEncoder.GetBlockFooter(this._output);
			}
			this.WriteFinal();
			bytesRead = this._output.BytesWritten;
			return true;
		}

		// Token: 0x06000044 RID: 68 RVA: 0x00002F1A File Offset: 0x0000111A
		private bool UseCompressed(double ratio)
		{
			return ratio <= 1.0;
		}

		// Token: 0x06000045 RID: 69 RVA: 0x00002F2B File Offset: 0x0000112B
		private void FlushInputWindows()
		{
			this._deflateEncoder.FlushInput();
		}

		// Token: 0x06000046 RID: 70 RVA: 0x00002F38 File Offset: 0x00001138
		private void WriteFinal()
		{
			this._copyEncoder.GetBlock(null, this._output, true);
		}

		// Token: 0x06000047 RID: 71 RVA: 0x00002F4D File Offset: 0x0000114D
		public void Dispose()
		{
		}

		// Token: 0x04000097 RID: 151
		private const int MinBlockSize = 256;

		// Token: 0x04000098 RID: 152
		private const int MaxHeaderFooterGoo = 120;

		// Token: 0x04000099 RID: 153
		private const int CleanCopySize = 8072;

		// Token: 0x0400009A RID: 154
		private const double BadCompressionThreshold = 1.0;

		// Token: 0x0400009B RID: 155
		private readonly FastEncoder _deflateEncoder;

		// Token: 0x0400009C RID: 156
		private readonly CopyEncoder _copyEncoder;

		// Token: 0x0400009D RID: 157
		private readonly DeflateInput _input;

		// Token: 0x0400009E RID: 158
		private readonly OutputBuffer _output;

		// Token: 0x0400009F RID: 159
		private DeflaterManaged.DeflaterState _processingState;

		// Token: 0x040000A0 RID: 160
		private DeflateInput _inputFromHistory;

		// Token: 0x0200000D RID: 13
		private enum DeflaterState
		{
			// Token: 0x040000A2 RID: 162
			NotStarted,
			// Token: 0x040000A3 RID: 163
			SlowDownForIncompressible1,
			// Token: 0x040000A4 RID: 164
			SlowDownForIncompressible2,
			// Token: 0x040000A5 RID: 165
			StartingSmallData,
			// Token: 0x040000A6 RID: 166
			CompressThenCheck,
			// Token: 0x040000A7 RID: 167
			CheckingForIncompressible,
			// Token: 0x040000A8 RID: 168
			HandlingSmallData
		}
	}
}
