﻿using System;
using System.Diagnostics;

namespace System.IO.Compression
{
	// Token: 0x02000010 RID: 16
	internal sealed class FastEncoderWindow
	{
		// Token: 0x0600005D RID: 93 RVA: 0x000033FB File Offset: 0x000015FB
		public FastEncoderWindow()
		{
			this.ResetWindow();
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x0600005E RID: 94 RVA: 0x00003409 File Offset: 0x00001609
		public int BytesAvailable
		{
			get
			{
				return this._bufEnd - this._bufPos;
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600005F RID: 95 RVA: 0x00003418 File Offset: 0x00001618
		public DeflateInput UnprocessedInput
		{
			get
			{
				return new DeflateInput
				{
					Buffer = this._window,
					StartIndex = this._bufPos,
					Count = this._bufEnd - this._bufPos
				};
			}
		}

		// Token: 0x06000060 RID: 96 RVA: 0x0000344A File Offset: 0x0000164A
		public void FlushWindow()
		{
			this.ResetWindow();
		}

		// Token: 0x06000061 RID: 97 RVA: 0x00003454 File Offset: 0x00001654
		private void ResetWindow()
		{
			this._window = new byte[16646];
			this._prev = new ushort[8450];
			this._lookup = new ushort[2048];
			this._bufPos = 8192;
			this._bufEnd = this._bufPos;
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x06000062 RID: 98 RVA: 0x000034A8 File Offset: 0x000016A8
		public int FreeWindowSpace
		{
			get
			{
				return 16384 - this._bufEnd;
			}
		}

		// Token: 0x06000063 RID: 99 RVA: 0x000034B6 File Offset: 0x000016B6
		public void CopyBytes(byte[] inputBuffer, int startIndex, int count)
		{
			Array.Copy(inputBuffer, startIndex, this._window, this._bufEnd, count);
			this._bufEnd += count;
		}

		// Token: 0x06000064 RID: 100 RVA: 0x000034DC File Offset: 0x000016DC
		public void MoveWindows()
		{
			Array.Copy(this._window, this._bufPos - 8192, this._window, 0, 8192);
			for (int i = 0; i < 2048; i++)
			{
				int num = (int)(this._lookup[i] - 8192);
				if (num <= 0)
				{
					this._lookup[i] = 0;
				}
				else
				{
					this._lookup[i] = (ushort)num;
				}
			}
			for (int i = 0; i < 8192; i++)
			{
				long num2 = (long)((ulong)this._prev[i] - 8192UL);
				if (num2 <= 0L)
				{
					this._prev[i] = 0;
				}
				else
				{
					this._prev[i] = (ushort)num2;
				}
			}
			this._bufPos = 8192;
			this._bufEnd = this._bufPos;
		}

		// Token: 0x06000065 RID: 101 RVA: 0x00003596 File Offset: 0x00001796
		private uint HashValue(uint hash, byte b)
		{
			return hash << 4 ^ (uint)b;
		}

		// Token: 0x06000066 RID: 102 RVA: 0x000035A0 File Offset: 0x000017A0
		private uint InsertString(ref uint hash)
		{
			hash = this.HashValue(hash, this._window[this._bufPos + 2]);
			uint num = (uint)this._lookup[(int)(hash & 2047U)];
			this._lookup[(int)(hash & 2047U)] = (ushort)this._bufPos;
			this._prev[this._bufPos & 8191] = (ushort)num;
			return num;
		}

		// Token: 0x06000067 RID: 103 RVA: 0x00003604 File Offset: 0x00001804
		private void InsertStrings(ref uint hash, int matchLen)
		{
			if (this._bufEnd - this._bufPos <= matchLen)
			{
				this._bufPos += matchLen - 1;
				return;
			}
			while (--matchLen > 0)
			{
				this.InsertString(ref hash);
				this._bufPos++;
			}
		}

		// Token: 0x06000068 RID: 104 RVA: 0x00003654 File Offset: 0x00001854
		internal bool GetNextSymbolOrMatch(Match match)
		{
			uint hash = this.HashValue(0U, this._window[this._bufPos]);
			hash = this.HashValue(hash, this._window[this._bufPos + 1]);
			int position = 0;
			int num;
			if (this._bufEnd - this._bufPos <= 3)
			{
				num = 0;
			}
			else
			{
				int num2 = (int)this.InsertString(ref hash);
				if (num2 != 0)
				{
					num = this.FindMatch(num2, out position, 32, 32);
					if (this._bufPos + num > this._bufEnd)
					{
						num = this._bufEnd - this._bufPos;
					}
				}
				else
				{
					num = 0;
				}
			}
			if (num < 3)
			{
				match.State = MatchState.HasSymbol;
				match.Symbol = this._window[this._bufPos];
				this._bufPos++;
			}
			else
			{
				this._bufPos++;
				if (num <= 6)
				{
					int position2 = 0;
					int num3 = (int)this.InsertString(ref hash);
					int num4;
					if (num3 != 0)
					{
						num4 = this.FindMatch(num3, out position2, (num < 4) ? 32 : 8, 32);
						if (this._bufPos + num4 > this._bufEnd)
						{
							num4 = this._bufEnd - this._bufPos;
						}
					}
					else
					{
						num4 = 0;
					}
					if (num4 > num)
					{
						match.State = MatchState.HasSymbolAndMatch;
						match.Symbol = this._window[this._bufPos - 1];
						match.Position = position2;
						match.Length = num4;
						this._bufPos++;
						num = num4;
						this.InsertStrings(ref hash, num);
					}
					else
					{
						match.State = MatchState.HasMatch;
						match.Position = position;
						match.Length = num;
						num--;
						this._bufPos++;
						this.InsertStrings(ref hash, num);
					}
				}
				else
				{
					match.State = MatchState.HasMatch;
					match.Position = position;
					match.Length = num;
					this.InsertStrings(ref hash, num);
				}
			}
			if (this._bufPos == 16384)
			{
				this.MoveWindows();
			}
			return true;
		}

		// Token: 0x06000069 RID: 105 RVA: 0x00003824 File Offset: 0x00001A24
		private int FindMatch(int search, out int matchPos, int searchDepth, int niceLength)
		{
			int num = 0;
			int num2 = 0;
			int num3 = this._bufPos - 8192;
			byte b = this._window[this._bufPos];
			while (search > num3)
			{
				if (this._window[search + num] == b)
				{
					int num4 = 0;
					while (num4 < 258 && this._window[this._bufPos + num4] == this._window[search + num4])
					{
						num4++;
					}
					if (num4 > num)
					{
						num = num4;
						num2 = search;
						if (num4 > 32)
						{
							break;
						}
						b = this._window[this._bufPos + num4];
					}
				}
				if (--searchDepth == 0)
				{
					break;
				}
				search = (int)this._prev[search & 8191];
			}
			matchPos = this._bufPos - num2 - 1;
			if (num == 3 && matchPos >= 16384)
			{
				return 0;
			}
			return num;
		}

		// Token: 0x0600006A RID: 106 RVA: 0x00002F4D File Offset: 0x0000114D
		[Conditional("DEBUG")]
		private void DebugAssertVerifyHashes()
		{
		}

		// Token: 0x0600006B RID: 107 RVA: 0x00002F4D File Offset: 0x0000114D
		[Conditional("DEBUG")]
		private void DebugAssertRecalculatedHashesAreEqual(int position1, int position2, string message = "")
		{
		}

		// Token: 0x040000BE RID: 190
		private byte[] _window;

		// Token: 0x040000BF RID: 191
		private int _bufPos;

		// Token: 0x040000C0 RID: 192
		private int _bufEnd;

		// Token: 0x040000C1 RID: 193
		private const int FastEncoderHashShift = 4;

		// Token: 0x040000C2 RID: 194
		private const int FastEncoderHashtableSize = 2048;

		// Token: 0x040000C3 RID: 195
		private const int FastEncoderHashMask = 2047;

		// Token: 0x040000C4 RID: 196
		private const int FastEncoderWindowSize = 8192;

		// Token: 0x040000C5 RID: 197
		private const int FastEncoderWindowMask = 8191;

		// Token: 0x040000C6 RID: 198
		private const int FastEncoderMatch3DistThreshold = 16384;

		// Token: 0x040000C7 RID: 199
		internal const int MaxMatch = 258;

		// Token: 0x040000C8 RID: 200
		internal const int MinMatch = 3;

		// Token: 0x040000C9 RID: 201
		private const int SearchDepth = 32;

		// Token: 0x040000CA RID: 202
		private const int GoodLength = 4;

		// Token: 0x040000CB RID: 203
		private const int NiceLength = 32;

		// Token: 0x040000CC RID: 204
		private const int LazyMatchThreshold = 6;

		// Token: 0x040000CD RID: 205
		private ushort[] _prev;

		// Token: 0x040000CE RID: 206
		private ushort[] _lookup;
	}
}
