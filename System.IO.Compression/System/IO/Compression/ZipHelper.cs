﻿using System;

namespace System.IO.Compression
{
	// Token: 0x0200002E RID: 46
	internal static class ZipHelper
	{
		// Token: 0x06000179 RID: 377 RVA: 0x000083AC File Offset: 0x000065AC
		internal static bool RequiresUnicode(string test)
		{
			foreach (char c in test)
			{
				if (c > '~' || c < ' ')
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x0600017A RID: 378 RVA: 0x000083E4 File Offset: 0x000065E4
		internal static void ReadBytes(Stream stream, byte[] buffer, int bytesToRead)
		{
			int i = bytesToRead;
			int num = 0;
			while (i > 0)
			{
				int num2 = stream.Read(buffer, num, i);
				if (num2 == 0)
				{
					throw new IOException("Zip file corrupt: unexpected end of stream reached.");
				}
				num += num2;
				i -= num2;
			}
		}

		// Token: 0x0600017B RID: 379 RVA: 0x0000841C File Offset: 0x0000661C
		internal static DateTime DosTimeToDateTime(uint dateTime)
		{
			int year = (int)(1980U + (dateTime >> 25));
			int month = (int)(dateTime >> 21 & 15U);
			int day = (int)(dateTime >> 16 & 31U);
			int hour = (int)(dateTime >> 11 & 31U);
			int minute = (int)(dateTime >> 5 & 63U);
			int second = (int)((dateTime & 31U) * 2U);
			DateTime result;
			try
			{
				result = new DateTime(year, month, day, hour, minute, second, 0);
			}
			catch (ArgumentOutOfRangeException)
			{
				result = ZipHelper.s_invalidDateIndicator;
			}
			catch (ArgumentException)
			{
				result = ZipHelper.s_invalidDateIndicator;
			}
			return result;
		}

		// Token: 0x0600017C RID: 380 RVA: 0x000084A0 File Offset: 0x000066A0
		internal static uint DateTimeToDosTime(DateTime dateTime)
		{
			return (uint)(((((((dateTime.Year - 1980 & 127) << 4) + dateTime.Month << 5) + dateTime.Day << 5) + dateTime.Hour << 6) + dateTime.Minute << 5) + dateTime.Second / 2);
		}

		// Token: 0x0600017D RID: 381 RVA: 0x000084F4 File Offset: 0x000066F4
		internal static bool SeekBackwardsToSignature(Stream stream, uint signatureToFind)
		{
			int num = 0;
			uint num2 = 0U;
			byte[] array = new byte[32];
			bool flag = false;
			bool flag2 = false;
			while (!flag2 && !flag)
			{
				flag = ZipHelper.SeekBackwardsAndRead(stream, array, out num);
				while (num >= 0 && !flag2)
				{
					num2 = (num2 << 8 | (uint)array[num]);
					if (num2 == signatureToFind)
					{
						flag2 = true;
					}
					else
					{
						num--;
					}
				}
			}
			if (!flag2)
			{
				return false;
			}
			stream.Seek((long)num, SeekOrigin.Current);
			return true;
		}

		// Token: 0x0600017E RID: 382 RVA: 0x00008558 File Offset: 0x00006758
		internal static void AdvanceToPosition(this Stream stream, long position)
		{
			int num2;
			for (long num = position - stream.Position; num != 0L; num -= (long)num2)
			{
				int count = (num > 64L) ? 64 : ((int)num);
				num2 = stream.Read(new byte[64], 0, count);
				if (num2 == 0)
				{
					throw new IOException("Zip file corrupt: unexpected end of stream reached.");
				}
			}
		}

		// Token: 0x0600017F RID: 383 RVA: 0x000085A4 File Offset: 0x000067A4
		private static bool SeekBackwardsAndRead(Stream stream, byte[] buffer, out int bufferPointer)
		{
			if (stream.Position >= (long)buffer.Length)
			{
				stream.Seek((long)(-(long)buffer.Length), SeekOrigin.Current);
				ZipHelper.ReadBytes(stream, buffer, buffer.Length);
				stream.Seek((long)(-(long)buffer.Length), SeekOrigin.Current);
				bufferPointer = buffer.Length - 1;
				return false;
			}
			int num = (int)stream.Position;
			stream.Seek(0L, SeekOrigin.Begin);
			ZipHelper.ReadBytes(stream, buffer, num);
			stream.Seek(0L, SeekOrigin.Begin);
			bufferPointer = num - 1;
			return true;
		}

		// Token: 0x06000180 RID: 384 RVA: 0x00008614 File Offset: 0x00006814
		// Note: this type is marked as 'beforefieldinit'.
		static ZipHelper()
		{
		}

		// Token: 0x040001C6 RID: 454
		internal const uint Mask32Bit = 4294967295U;

		// Token: 0x040001C7 RID: 455
		internal const ushort Mask16Bit = 65535;

		// Token: 0x040001C8 RID: 456
		private const int BackwardsSeekingBufferSize = 32;

		// Token: 0x040001C9 RID: 457
		internal const int ValidZipDate_YearMin = 1980;

		// Token: 0x040001CA RID: 458
		internal const int ValidZipDate_YearMax = 2107;

		// Token: 0x040001CB RID: 459
		private static readonly DateTime s_invalidDateIndicator = new DateTime(1980, 1, 1, 0, 0, 0);
	}
}
