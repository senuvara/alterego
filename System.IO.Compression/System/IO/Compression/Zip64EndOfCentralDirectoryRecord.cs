﻿using System;

namespace System.IO.Compression
{
	// Token: 0x02000027 RID: 39
	internal struct Zip64EndOfCentralDirectoryRecord
	{
		// Token: 0x06000142 RID: 322 RVA: 0x0000787C File Offset: 0x00005A7C
		public static bool TryReadBlock(BinaryReader reader, out Zip64EndOfCentralDirectoryRecord zip64EOCDRecord)
		{
			zip64EOCDRecord = default(Zip64EndOfCentralDirectoryRecord);
			if (reader.ReadUInt32() != 101075792U)
			{
				return false;
			}
			zip64EOCDRecord.SizeOfThisRecord = reader.ReadUInt64();
			zip64EOCDRecord.VersionMadeBy = reader.ReadUInt16();
			zip64EOCDRecord.VersionNeededToExtract = reader.ReadUInt16();
			zip64EOCDRecord.NumberOfThisDisk = reader.ReadUInt32();
			zip64EOCDRecord.NumberOfDiskWithStartOfCD = reader.ReadUInt32();
			zip64EOCDRecord.NumberOfEntriesOnThisDisk = reader.ReadUInt64();
			zip64EOCDRecord.NumberOfEntriesTotal = reader.ReadUInt64();
			zip64EOCDRecord.SizeOfCentralDirectory = reader.ReadUInt64();
			zip64EOCDRecord.OffsetOfCentralDirectory = reader.ReadUInt64();
			return true;
		}

		// Token: 0x06000143 RID: 323 RVA: 0x0000790C File Offset: 0x00005B0C
		public static void WriteBlock(Stream stream, long numberOfEntries, long startOfCentralDirectory, long sizeOfCentralDirectory)
		{
			BinaryWriter binaryWriter = new BinaryWriter(stream);
			binaryWriter.Write(101075792U);
			binaryWriter.Write(44UL);
			binaryWriter.Write(45);
			binaryWriter.Write(45);
			binaryWriter.Write(0U);
			binaryWriter.Write(0U);
			binaryWriter.Write(numberOfEntries);
			binaryWriter.Write(numberOfEntries);
			binaryWriter.Write(sizeOfCentralDirectory);
			binaryWriter.Write(startOfCentralDirectory);
		}

		// Token: 0x04000181 RID: 385
		private const uint SignatureConstant = 101075792U;

		// Token: 0x04000182 RID: 386
		private const ulong NormalSize = 44UL;

		// Token: 0x04000183 RID: 387
		public ulong SizeOfThisRecord;

		// Token: 0x04000184 RID: 388
		public ushort VersionMadeBy;

		// Token: 0x04000185 RID: 389
		public ushort VersionNeededToExtract;

		// Token: 0x04000186 RID: 390
		public uint NumberOfThisDisk;

		// Token: 0x04000187 RID: 391
		public uint NumberOfDiskWithStartOfCD;

		// Token: 0x04000188 RID: 392
		public ulong NumberOfEntriesOnThisDisk;

		// Token: 0x04000189 RID: 393
		public ulong NumberOfEntriesTotal;

		// Token: 0x0400018A RID: 394
		public ulong SizeOfCentralDirectory;

		// Token: 0x0400018B RID: 395
		public ulong OffsetOfCentralDirectory;
	}
}
