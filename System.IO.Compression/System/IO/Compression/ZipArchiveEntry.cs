﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using Unity;

namespace System.IO.Compression
{
	/// <summary>Represents a compressed file within a zip archive.</summary>
	// Token: 0x0200001E RID: 30
	public class ZipArchiveEntry
	{
		// Token: 0x060000E8 RID: 232 RVA: 0x00005B54 File Offset: 0x00003D54
		internal ZipArchiveEntry(ZipArchive archive, ZipCentralDirectoryFileHeader cd)
		{
			this._archive = archive;
			this._originallyInArchive = true;
			this._diskNumberStart = cd.DiskNumberStart;
			this._versionMadeByPlatform = (ZipVersionMadeByPlatform)cd.VersionMadeByCompatibility;
			this._versionMadeBySpecification = (ZipVersionNeededValues)cd.VersionMadeBySpecification;
			this._versionToExtract = (ZipVersionNeededValues)cd.VersionNeededToExtract;
			this._generalPurposeBitFlag = (ZipArchiveEntry.BitFlagValues)cd.GeneralPurposeBitFlag;
			this.CompressionMethod = (ZipArchiveEntry.CompressionMethodValues)cd.CompressionMethod;
			this._lastModified = new DateTimeOffset(ZipHelper.DosTimeToDateTime(cd.LastModified));
			this._compressedSize = cd.CompressedSize;
			this._uncompressedSize = cd.UncompressedSize;
			this._externalFileAttr = cd.ExternalFileAttributes;
			this._offsetOfLocalHeader = cd.RelativeOffsetOfLocalHeader;
			this._storedOffsetOfCompressedData = null;
			this._crc32 = cd.Crc32;
			this._compressedBytes = null;
			this._storedUncompressedData = null;
			this._currentlyOpenForWrite = false;
			this._everOpenedForWrite = false;
			this._outstandingWriteStream = null;
			this.FullName = this.DecodeEntryName(cd.Filename);
			this._lhUnknownExtraFields = null;
			this._cdUnknownExtraFields = cd.ExtraFields;
			this._fileComment = cd.FileComment;
			this._compressionLevel = null;
		}

		// Token: 0x060000E9 RID: 233 RVA: 0x00005C7B File Offset: 0x00003E7B
		internal ZipArchiveEntry(ZipArchive archive, string entryName, CompressionLevel compressionLevel) : this(archive, entryName)
		{
			this._compressionLevel = new CompressionLevel?(compressionLevel);
		}

		// Token: 0x060000EA RID: 234 RVA: 0x00005C94 File Offset: 0x00003E94
		internal ZipArchiveEntry(ZipArchive archive, string entryName)
		{
			this._archive = archive;
			this._originallyInArchive = false;
			this._diskNumberStart = 0;
			this._versionMadeByPlatform = ZipArchiveEntry.CurrentZipPlatform;
			this._versionMadeBySpecification = ZipVersionNeededValues.Default;
			this._versionToExtract = ZipVersionNeededValues.Default;
			this._generalPurposeBitFlag = (ZipArchiveEntry.BitFlagValues)0;
			this.CompressionMethod = ZipArchiveEntry.CompressionMethodValues.Deflate;
			this._lastModified = DateTimeOffset.Now;
			this._compressedSize = 0L;
			this._uncompressedSize = 0L;
			this._externalFileAttr = 0U;
			this._offsetOfLocalHeader = 0L;
			this._storedOffsetOfCompressedData = null;
			this._crc32 = 0U;
			this._compressedBytes = null;
			this._storedUncompressedData = null;
			this._currentlyOpenForWrite = false;
			this._everOpenedForWrite = false;
			this._outstandingWriteStream = null;
			this.FullName = entryName;
			this._cdUnknownExtraFields = null;
			this._lhUnknownExtraFields = null;
			this._fileComment = null;
			this._compressionLevel = null;
			if (this._storedEntryNameBytes.Length > 65535)
			{
				throw new ArgumentException("Entry names cannot require more than 2^16 bits.");
			}
			if (this._archive.Mode == ZipArchiveMode.Create)
			{
				this._archive.AcquireArchiveStream(this);
			}
		}

		/// <summary>Gets the zip archive that the entry belongs to.</summary>
		/// <returns>The zip archive that the entry belongs to, or <see langword="null" /> if the entry has been deleted.</returns>
		// Token: 0x1700002B RID: 43
		// (get) Token: 0x060000EB RID: 235 RVA: 0x00005DA1 File Offset: 0x00003FA1
		public ZipArchive Archive
		{
			get
			{
				return this._archive;
			}
		}

		/// <summary>Gets the compressed size of the entry in the zip archive.</summary>
		/// <returns>The compressed size of the entry in the zip archive.</returns>
		/// <exception cref="T:System.InvalidOperationException">The value of the property is not available because the entry has been modified.</exception>
		// Token: 0x1700002C RID: 44
		// (get) Token: 0x060000EC RID: 236 RVA: 0x00005DA9 File Offset: 0x00003FA9
		public long CompressedLength
		{
			get
			{
				if (this._everOpenedForWrite)
				{
					throw new InvalidOperationException("Length properties are unavailable once an entry has been opened for writing.");
				}
				return this._compressedSize;
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x060000ED RID: 237 RVA: 0x00005DC4 File Offset: 0x00003FC4
		// (set) Token: 0x060000EE RID: 238 RVA: 0x00005DCC File Offset: 0x00003FCC
		public int ExternalAttributes
		{
			get
			{
				return (int)this._externalFileAttr;
			}
			set
			{
				this.ThrowIfInvalidArchive();
				this._externalFileAttr = (uint)value;
			}
		}

		/// <summary>Gets the relative path of the entry in the zip archive.</summary>
		/// <returns>The relative path of the entry in the zip archive.</returns>
		// Token: 0x1700002E RID: 46
		// (get) Token: 0x060000EF RID: 239 RVA: 0x00005DDB File Offset: 0x00003FDB
		// (set) Token: 0x060000F0 RID: 240 RVA: 0x00005DE4 File Offset: 0x00003FE4
		public string FullName
		{
			get
			{
				return this._storedEntryName;
			}
			private set
			{
				if (value == null)
				{
					throw new ArgumentNullException("FullName");
				}
				bool flag;
				this._storedEntryNameBytes = this.EncodeEntryName(value, out flag);
				this._storedEntryName = value;
				if (flag)
				{
					this._generalPurposeBitFlag |= ZipArchiveEntry.BitFlagValues.UnicodeFileName;
				}
				else
				{
					this._generalPurposeBitFlag &= ~ZipArchiveEntry.BitFlagValues.UnicodeFileName;
				}
				if (ZipArchiveEntry.ParseFileName(value, this._versionMadeByPlatform) == "")
				{
					this.VersionToExtractAtLeast(ZipVersionNeededValues.ExplicitDirectory);
				}
			}
		}

		/// <summary>Gets or sets the last time the entry in the zip archive was changed.</summary>
		/// <returns>The last time the entry in the zip archive was changed.</returns>
		/// <exception cref="T:System.NotSupportedException">The attempt to set this property failed, because the zip archive for the entry is in <see cref="F:System.IO.Compression.ZipArchiveMode.Read" /> mode.</exception>
		/// <exception cref="T:System.IO.IOException">The archive mode is set to <see cref="F:System.IO.Compression.ZipArchiveMode.Create" />.- or -The archive mode is set to <see cref="F:System.IO.Compression.ZipArchiveMode.Update" /> and the entry has been opened.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">An attempt was made to set this property to a value that is either earlier than 1980 January 1 0:00:00 (midnight) or later than 2107 December 31 23:59:58 (one second before midnight).</exception>
		// Token: 0x1700002F RID: 47
		// (get) Token: 0x060000F1 RID: 241 RVA: 0x00005E5E File Offset: 0x0000405E
		// (set) Token: 0x060000F2 RID: 242 RVA: 0x00005E68 File Offset: 0x00004068
		public DateTimeOffset LastWriteTime
		{
			get
			{
				return this._lastModified;
			}
			set
			{
				this.ThrowIfInvalidArchive();
				if (this._archive.Mode == ZipArchiveMode.Read)
				{
					throw new NotSupportedException("Cannot modify read-only archive.");
				}
				if (this._archive.Mode == ZipArchiveMode.Create && this._everOpenedForWrite)
				{
					throw new IOException("Cannot modify entry in Create mode after entry has been opened for writing.");
				}
				if (value.DateTime.Year < 1980 || value.DateTime.Year > 2107)
				{
					throw new ArgumentOutOfRangeException("value", "The DateTimeOffset specified cannot be converted into a Zip file timestamp.");
				}
				this._lastModified = value;
			}
		}

		/// <summary>Gets the uncompressed size of the entry in the zip archive.</summary>
		/// <returns>The uncompressed size of the entry in the zip archive.</returns>
		/// <exception cref="T:System.InvalidOperationException">The value of the property is not available because the entry has been modified.</exception>
		// Token: 0x17000030 RID: 48
		// (get) Token: 0x060000F3 RID: 243 RVA: 0x00005EF7 File Offset: 0x000040F7
		public long Length
		{
			get
			{
				if (this._everOpenedForWrite)
				{
					throw new InvalidOperationException("Length properties are unavailable once an entry has been opened for writing.");
				}
				return this._uncompressedSize;
			}
		}

		/// <summary>Gets the file name of the entry in the zip archive.</summary>
		/// <returns>The file name of the entry in the zip archive.</returns>
		// Token: 0x17000031 RID: 49
		// (get) Token: 0x060000F4 RID: 244 RVA: 0x00005F12 File Offset: 0x00004112
		public string Name
		{
			get
			{
				return ZipArchiveEntry.ParseFileName(this.FullName, this._versionMadeByPlatform);
			}
		}

		/// <summary>Deletes the entry from the zip archive.</summary>
		/// <exception cref="T:System.IO.IOException">The entry is already open for reading or writing.</exception>
		/// <exception cref="T:System.NotSupportedException">The zip archive for this entry was opened in a mode other than <see cref="F:System.IO.Compression.ZipArchiveMode.Update" />. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The zip archive for this entry has been disposed.</exception>
		// Token: 0x060000F5 RID: 245 RVA: 0x00005F28 File Offset: 0x00004128
		public void Delete()
		{
			if (this._archive == null)
			{
				return;
			}
			if (this._currentlyOpenForWrite)
			{
				throw new IOException("Cannot delete an entry currently open for writing.");
			}
			if (this._archive.Mode != ZipArchiveMode.Update)
			{
				throw new NotSupportedException("Delete can only be used when the archive is in Update mode.");
			}
			this._archive.ThrowIfDisposed();
			this._archive.RemoveEntry(this);
			this._archive = null;
			this.UnloadStreams();
		}

		/// <summary>Opens the entry from the zip archive.</summary>
		/// <returns>The stream that represents the contents of the entry.</returns>
		/// <exception cref="T:System.IO.IOException">The entry is already currently open for writing.-or-The entry has been deleted from the archive.-or-The archive for this entry was opened with the <see cref="F:System.IO.Compression.ZipArchiveMode.Create" /> mode, and this entry has already been written to. </exception>
		/// <exception cref="T:System.IO.InvalidDataException">The entry is either missing from the archive or is corrupt and cannot be read. -or-The entry has been compressed by using a compression method that is not supported.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The zip archive for this entry has been disposed.</exception>
		// Token: 0x060000F6 RID: 246 RVA: 0x00005F90 File Offset: 0x00004190
		public Stream Open()
		{
			this.ThrowIfInvalidArchive();
			switch (this._archive.Mode)
			{
			case ZipArchiveMode.Read:
				return this.OpenInReadMode(true);
			case ZipArchiveMode.Create:
				return this.OpenInWriteMode();
			}
			return this.OpenInUpdateMode();
		}

		/// <summary>Retrieves the relative path of the entry in the zip archive.</summary>
		/// <returns>The relative path of the entry, which is the value stored in the <see cref="P:System.IO.Compression.ZipArchiveEntry.FullName" /> property.</returns>
		// Token: 0x060000F7 RID: 247 RVA: 0x00005FD8 File Offset: 0x000041D8
		public override string ToString()
		{
			return this.FullName;
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x060000F8 RID: 248 RVA: 0x00005FE0 File Offset: 0x000041E0
		internal bool EverOpenedForWrite
		{
			get
			{
				return this._everOpenedForWrite;
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x060000F9 RID: 249 RVA: 0x00005FE8 File Offset: 0x000041E8
		private long OffsetOfCompressedData
		{
			get
			{
				if (this._storedOffsetOfCompressedData == null)
				{
					this._archive.ArchiveStream.Seek(this._offsetOfLocalHeader, SeekOrigin.Begin);
					if (!ZipLocalFileHeader.TrySkipBlock(this._archive.ArchiveReader))
					{
						throw new InvalidDataException("A local file header is corrupt.");
					}
					this._storedOffsetOfCompressedData = new long?(this._archive.ArchiveStream.Position);
				}
				return this._storedOffsetOfCompressedData.Value;
			}
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x060000FA RID: 250 RVA: 0x00006060 File Offset: 0x00004260
		private MemoryStream UncompressedData
		{
			get
			{
				if (this._storedUncompressedData == null)
				{
					this._storedUncompressedData = new MemoryStream((int)this._uncompressedSize);
					if (this._originallyInArchive)
					{
						using (Stream stream = this.OpenInReadMode(false))
						{
							try
							{
								stream.CopyTo(this._storedUncompressedData);
							}
							catch (InvalidDataException)
							{
								this._storedUncompressedData.Dispose();
								this._storedUncompressedData = null;
								this._currentlyOpenForWrite = false;
								this._everOpenedForWrite = false;
								throw;
							}
						}
					}
					this.CompressionMethod = ZipArchiveEntry.CompressionMethodValues.Deflate;
				}
				return this._storedUncompressedData;
			}
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x060000FB RID: 251 RVA: 0x000060FC File Offset: 0x000042FC
		// (set) Token: 0x060000FC RID: 252 RVA: 0x00006104 File Offset: 0x00004304
		private ZipArchiveEntry.CompressionMethodValues CompressionMethod
		{
			get
			{
				return this._storedCompressionMethod;
			}
			set
			{
				if (value == ZipArchiveEntry.CompressionMethodValues.Deflate)
				{
					this.VersionToExtractAtLeast(ZipVersionNeededValues.ExplicitDirectory);
				}
				else if (value == ZipArchiveEntry.CompressionMethodValues.Deflate64)
				{
					this.VersionToExtractAtLeast(ZipVersionNeededValues.Deflate64);
				}
				this._storedCompressionMethod = value;
			}
		}

		// Token: 0x060000FD RID: 253 RVA: 0x00006128 File Offset: 0x00004328
		private string DecodeEntryName(byte[] entryNameBytes)
		{
			Encoding encoding;
			if ((this._generalPurposeBitFlag & ZipArchiveEntry.BitFlagValues.UnicodeFileName) == (ZipArchiveEntry.BitFlagValues)0)
			{
				encoding = ((this._archive == null) ? Encoding.UTF8 : (this._archive.EntryNameEncoding ?? Encoding.UTF8));
			}
			else
			{
				encoding = Encoding.UTF8;
			}
			return encoding.GetString(entryNameBytes);
		}

		// Token: 0x060000FE RID: 254 RVA: 0x00006178 File Offset: 0x00004378
		private byte[] EncodeEntryName(string entryName, out bool isUTF8)
		{
			Encoding encoding;
			if (this._archive != null && this._archive.EntryNameEncoding != null)
			{
				encoding = this._archive.EntryNameEncoding;
			}
			else
			{
				encoding = (ZipHelper.RequiresUnicode(entryName) ? Encoding.UTF8 : Encoding.ASCII);
			}
			isUTF8 = encoding.Equals(Encoding.UTF8);
			return encoding.GetBytes(entryName);
		}

		// Token: 0x060000FF RID: 255 RVA: 0x000061D1 File Offset: 0x000043D1
		internal void WriteAndFinishLocalEntry()
		{
			this.CloseStreams();
			this.WriteLocalFileHeaderAndDataIfNeeded();
			this.UnloadStreams();
		}

		// Token: 0x06000100 RID: 256 RVA: 0x000061E8 File Offset: 0x000043E8
		internal void WriteCentralDirectoryFileHeader()
		{
			BinaryWriter binaryWriter = new BinaryWriter(this._archive.ArchiveStream);
			Zip64ExtraField zip64ExtraField = default(Zip64ExtraField);
			bool flag = false;
			uint value;
			uint value2;
			if (this.SizesTooLarge())
			{
				flag = true;
				value = uint.MaxValue;
				value2 = uint.MaxValue;
				zip64ExtraField.CompressedSize = new long?(this._compressedSize);
				zip64ExtraField.UncompressedSize = new long?(this._uncompressedSize);
			}
			else
			{
				value = (uint)this._compressedSize;
				value2 = (uint)this._uncompressedSize;
			}
			uint value3;
			if (this._offsetOfLocalHeader > (long)((ulong)-1))
			{
				flag = true;
				value3 = uint.MaxValue;
				zip64ExtraField.LocalHeaderOffset = new long?(this._offsetOfLocalHeader);
			}
			else
			{
				value3 = (uint)this._offsetOfLocalHeader;
			}
			if (flag)
			{
				this.VersionToExtractAtLeast(ZipVersionNeededValues.Zip64);
			}
			int num = (int)(flag ? zip64ExtraField.TotalSize : 0) + ((this._cdUnknownExtraFields != null) ? ZipGenericExtraField.TotalSize(this._cdUnknownExtraFields) : 0);
			ushort value4;
			if (num > 65535)
			{
				value4 = (flag ? zip64ExtraField.TotalSize : 0);
				this._cdUnknownExtraFields = null;
			}
			else
			{
				value4 = (ushort)num;
			}
			binaryWriter.Write(33639248U);
			binaryWriter.Write((byte)this._versionMadeBySpecification);
			binaryWriter.Write((byte)ZipArchiveEntry.CurrentZipPlatform);
			binaryWriter.Write((ushort)this._versionToExtract);
			binaryWriter.Write((ushort)this._generalPurposeBitFlag);
			binaryWriter.Write((ushort)this.CompressionMethod);
			binaryWriter.Write(ZipHelper.DateTimeToDosTime(this._lastModified.DateTime));
			binaryWriter.Write(this._crc32);
			binaryWriter.Write(value);
			binaryWriter.Write(value2);
			binaryWriter.Write((ushort)this._storedEntryNameBytes.Length);
			binaryWriter.Write(value4);
			binaryWriter.Write((this._fileComment != null) ? ((ushort)this._fileComment.Length) : 0);
			binaryWriter.Write(0);
			binaryWriter.Write(0);
			binaryWriter.Write(this._externalFileAttr);
			binaryWriter.Write(value3);
			binaryWriter.Write(this._storedEntryNameBytes);
			if (flag)
			{
				zip64ExtraField.WriteBlock(this._archive.ArchiveStream);
			}
			if (this._cdUnknownExtraFields != null)
			{
				ZipGenericExtraField.WriteAllBlocks(this._cdUnknownExtraFields, this._archive.ArchiveStream);
			}
			if (this._fileComment != null)
			{
				binaryWriter.Write(this._fileComment);
			}
		}

		// Token: 0x06000101 RID: 257 RVA: 0x000063FC File Offset: 0x000045FC
		internal bool LoadLocalHeaderExtraFieldAndCompressedBytesIfNeeded()
		{
			if (this._originallyInArchive)
			{
				this._archive.ArchiveStream.Seek(this._offsetOfLocalHeader, SeekOrigin.Begin);
				this._lhUnknownExtraFields = ZipLocalFileHeader.GetExtraFields(this._archive.ArchiveReader);
			}
			if (!this._everOpenedForWrite && this._originallyInArchive)
			{
				this._compressedBytes = new byte[this._compressedSize / 2147483591L + 1L][];
				for (int i = 0; i < this._compressedBytes.Length - 1; i++)
				{
					this._compressedBytes[i] = new byte[2147483591];
				}
				this._compressedBytes[this._compressedBytes.Length - 1] = new byte[this._compressedSize % 2147483591L];
				this._archive.ArchiveStream.Seek(this.OffsetOfCompressedData, SeekOrigin.Begin);
				for (int j = 0; j < this._compressedBytes.Length - 1; j++)
				{
					ZipHelper.ReadBytes(this._archive.ArchiveStream, this._compressedBytes[j], 2147483591);
				}
				ZipHelper.ReadBytes(this._archive.ArchiveStream, this._compressedBytes[this._compressedBytes.Length - 1], (int)(this._compressedSize % 2147483591L));
			}
			return true;
		}

		// Token: 0x06000102 RID: 258 RVA: 0x00006538 File Offset: 0x00004738
		internal void ThrowIfNotOpenable(bool needToUncompress, bool needToLoadIntoMemory)
		{
			string message;
			if (!this.IsOpenable(needToUncompress, needToLoadIntoMemory, out message))
			{
				throw new InvalidDataException(message);
			}
		}

		// Token: 0x06000103 RID: 259 RVA: 0x00006558 File Offset: 0x00004758
		private CheckSumAndSizeWriteStream GetDataCompressor(Stream backingStream, bool leaveBackingStreamOpen, EventHandler onClose)
		{
			Stream baseStream = (this._compressionLevel != null) ? new DeflateStream(backingStream, this._compressionLevel.Value, leaveBackingStreamOpen) : new DeflateStream(backingStream, CompressionMode.Compress, leaveBackingStreamOpen);
			bool flag = true;
			bool leaveOpenOnClose = leaveBackingStreamOpen && !flag;
			return new CheckSumAndSizeWriteStream(baseStream, backingStream, leaveOpenOnClose, this, onClose, delegate(long initialPosition, long currentPosition, uint checkSum, Stream backing, ZipArchiveEntry thisRef, EventHandler closeHandler)
			{
				thisRef._crc32 = checkSum;
				thisRef._uncompressedSize = currentPosition;
				thisRef._compressedSize = backing.Position - initialPosition;
				if (closeHandler != null)
				{
					closeHandler(thisRef, EventArgs.Empty);
				}
			});
		}

		// Token: 0x06000104 RID: 260 RVA: 0x000065C4 File Offset: 0x000047C4
		private Stream GetDataDecompressor(Stream compressedStreamToRead)
		{
			ZipArchiveEntry.CompressionMethodValues compressionMethod = this.CompressionMethod;
			if (compressionMethod != ZipArchiveEntry.CompressionMethodValues.Stored)
			{
				if (compressionMethod == ZipArchiveEntry.CompressionMethodValues.Deflate)
				{
					return new DeflateStream(compressedStreamToRead, CompressionMode.Decompress);
				}
				if (compressionMethod == ZipArchiveEntry.CompressionMethodValues.Deflate64)
				{
					return new DeflateManagedStream(compressedStreamToRead, ZipArchiveEntry.CompressionMethodValues.Deflate64);
				}
			}
			return compressedStreamToRead;
		}

		// Token: 0x06000105 RID: 261 RVA: 0x00006600 File Offset: 0x00004800
		private Stream OpenInReadMode(bool checkOpenable)
		{
			if (checkOpenable)
			{
				this.ThrowIfNotOpenable(true, false);
			}
			Stream compressedStreamToRead = new SubReadStream(this._archive.ArchiveStream, this.OffsetOfCompressedData, this._compressedSize);
			return this.GetDataDecompressor(compressedStreamToRead);
		}

		// Token: 0x06000106 RID: 262 RVA: 0x0000663C File Offset: 0x0000483C
		private Stream OpenInWriteMode()
		{
			if (this._everOpenedForWrite)
			{
				throw new IOException("Entries in create mode may only be written to once, and only one entry may be held open at a time.");
			}
			this._everOpenedForWrite = true;
			CheckSumAndSizeWriteStream dataCompressor = this.GetDataCompressor(this._archive.ArchiveStream, true, delegate(object o, EventArgs e)
			{
				ZipArchiveEntry zipArchiveEntry = (ZipArchiveEntry)o;
				zipArchiveEntry._archive.ReleaseArchiveStream(zipArchiveEntry);
				zipArchiveEntry._outstandingWriteStream = null;
			});
			this._outstandingWriteStream = new ZipArchiveEntry.DirectToArchiveWriterStream(dataCompressor, this);
			return new WrappedStream(this._outstandingWriteStream, true);
		}

		// Token: 0x06000107 RID: 263 RVA: 0x000066B0 File Offset: 0x000048B0
		private Stream OpenInUpdateMode()
		{
			if (this._currentlyOpenForWrite)
			{
				throw new IOException("Entries cannot be opened multiple times in Update mode.");
			}
			this.ThrowIfNotOpenable(true, true);
			this._everOpenedForWrite = true;
			this._currentlyOpenForWrite = true;
			this.UncompressedData.Seek(0L, SeekOrigin.Begin);
			return new WrappedStream(this.UncompressedData, this, delegate(ZipArchiveEntry thisRef)
			{
				thisRef._currentlyOpenForWrite = false;
			});
		}

		// Token: 0x06000108 RID: 264 RVA: 0x00006720 File Offset: 0x00004920
		private bool IsOpenable(bool needToUncompress, bool needToLoadIntoMemory, out string message)
		{
			message = null;
			if (this._originallyInArchive)
			{
				if (needToUncompress && this.CompressionMethod != ZipArchiveEntry.CompressionMethodValues.Stored && this.CompressionMethod != ZipArchiveEntry.CompressionMethodValues.Deflate && this.CompressionMethod != ZipArchiveEntry.CompressionMethodValues.Deflate64)
				{
					ZipArchiveEntry.CompressionMethodValues compressionMethod = this.CompressionMethod;
					if (compressionMethod == ZipArchiveEntry.CompressionMethodValues.BZip2 || compressionMethod == ZipArchiveEntry.CompressionMethodValues.LZMA)
					{
						message = SR.Format("The archive entry was compressed using {0} and is not supported.", this.CompressionMethod.ToString());
					}
					else
					{
						message = "The archive entry was compressed using an unsupported compression method.";
					}
					return false;
				}
				if ((long)this._diskNumberStart != (long)((ulong)this._archive.NumberOfThisDisk))
				{
					message = "Split or spanned archives are not supported.";
					return false;
				}
				if (this._offsetOfLocalHeader > this._archive.ArchiveStream.Length)
				{
					message = "A local file header is corrupt.";
					return false;
				}
				this._archive.ArchiveStream.Seek(this._offsetOfLocalHeader, SeekOrigin.Begin);
				if (!ZipLocalFileHeader.TrySkipBlock(this._archive.ArchiveReader))
				{
					message = "A local file header is corrupt.";
					return false;
				}
				if (this.OffsetOfCompressedData + this._compressedSize > this._archive.ArchiveStream.Length)
				{
					message = "A local file header is corrupt.";
					return false;
				}
				if (needToLoadIntoMemory && this._compressedSize > 2147483647L && !ZipArchiveEntry.s_allowLargeZipArchiveEntriesInUpdateMode)
				{
					message = "Entries larger than 4GB are not supported in Update mode.";
					return false;
				}
			}
			return true;
		}

		// Token: 0x06000109 RID: 265 RVA: 0x00006851 File Offset: 0x00004A51
		private bool SizesTooLarge()
		{
			return this._compressedSize > (long)((ulong)-1) || this._uncompressedSize > (long)((ulong)-1);
		}

		// Token: 0x0600010A RID: 266 RVA: 0x0000686C File Offset: 0x00004A6C
		private bool WriteLocalFileHeader(bool isEmptyFile)
		{
			BinaryWriter binaryWriter = new BinaryWriter(this._archive.ArchiveStream);
			Zip64ExtraField zip64ExtraField = default(Zip64ExtraField);
			bool flag = false;
			uint value;
			uint value2;
			if (isEmptyFile)
			{
				this.CompressionMethod = ZipArchiveEntry.CompressionMethodValues.Stored;
				value = 0U;
				value2 = 0U;
			}
			else if (this._archive.Mode == ZipArchiveMode.Create && !this._archive.ArchiveStream.CanSeek && !isEmptyFile)
			{
				this._generalPurposeBitFlag |= ZipArchiveEntry.BitFlagValues.DataDescriptor;
				flag = false;
				value = 0U;
				value2 = 0U;
			}
			else if (this.SizesTooLarge())
			{
				flag = true;
				value = uint.MaxValue;
				value2 = uint.MaxValue;
				zip64ExtraField.CompressedSize = new long?(this._compressedSize);
				zip64ExtraField.UncompressedSize = new long?(this._uncompressedSize);
				this.VersionToExtractAtLeast(ZipVersionNeededValues.Zip64);
			}
			else
			{
				flag = false;
				value = (uint)this._compressedSize;
				value2 = (uint)this._uncompressedSize;
			}
			this._offsetOfLocalHeader = binaryWriter.BaseStream.Position;
			int num = (int)(flag ? zip64ExtraField.TotalSize : 0) + ((this._lhUnknownExtraFields != null) ? ZipGenericExtraField.TotalSize(this._lhUnknownExtraFields) : 0);
			ushort value3;
			if (num > 65535)
			{
				value3 = (flag ? zip64ExtraField.TotalSize : 0);
				this._lhUnknownExtraFields = null;
			}
			else
			{
				value3 = (ushort)num;
			}
			binaryWriter.Write(67324752U);
			binaryWriter.Write((ushort)this._versionToExtract);
			binaryWriter.Write((ushort)this._generalPurposeBitFlag);
			binaryWriter.Write((ushort)this.CompressionMethod);
			binaryWriter.Write(ZipHelper.DateTimeToDosTime(this._lastModified.DateTime));
			binaryWriter.Write(this._crc32);
			binaryWriter.Write(value);
			binaryWriter.Write(value2);
			binaryWriter.Write((ushort)this._storedEntryNameBytes.Length);
			binaryWriter.Write(value3);
			binaryWriter.Write(this._storedEntryNameBytes);
			if (flag)
			{
				zip64ExtraField.WriteBlock(this._archive.ArchiveStream);
			}
			if (this._lhUnknownExtraFields != null)
			{
				ZipGenericExtraField.WriteAllBlocks(this._lhUnknownExtraFields, this._archive.ArchiveStream);
			}
			return flag;
		}

		// Token: 0x0600010B RID: 267 RVA: 0x00006A48 File Offset: 0x00004C48
		private void WriteLocalFileHeaderAndDataIfNeeded()
		{
			if (this._storedUncompressedData != null || this._compressedBytes != null)
			{
				if (this._storedUncompressedData != null)
				{
					this._uncompressedSize = this._storedUncompressedData.Length;
					using (Stream stream = new ZipArchiveEntry.DirectToArchiveWriterStream(this.GetDataCompressor(this._archive.ArchiveStream, true, null), this))
					{
						this._storedUncompressedData.Seek(0L, SeekOrigin.Begin);
						this._storedUncompressedData.CopyTo(stream);
						this._storedUncompressedData.Dispose();
						this._storedUncompressedData = null;
						return;
					}
				}
				if (this._uncompressedSize == 0L)
				{
					this.CompressionMethod = ZipArchiveEntry.CompressionMethodValues.Stored;
				}
				this.WriteLocalFileHeader(false);
				foreach (byte[] array in this._compressedBytes)
				{
					this._archive.ArchiveStream.Write(array, 0, array.Length);
				}
				return;
			}
			if (this._archive.Mode == ZipArchiveMode.Update || !this._everOpenedForWrite)
			{
				this._everOpenedForWrite = true;
				this.WriteLocalFileHeader(true);
			}
		}

		// Token: 0x0600010C RID: 268 RVA: 0x00006B50 File Offset: 0x00004D50
		private void WriteCrcAndSizesInLocalHeader(bool zip64HeaderUsed)
		{
			long position = this._archive.ArchiveStream.Position;
			BinaryWriter binaryWriter = new BinaryWriter(this._archive.ArchiveStream);
			bool flag = this.SizesTooLarge();
			bool flag2 = flag && !zip64HeaderUsed;
			uint value = flag ? uint.MaxValue : ((uint)this._compressedSize);
			uint value2 = flag ? uint.MaxValue : ((uint)this._uncompressedSize);
			if (flag2)
			{
				this._generalPurposeBitFlag |= ZipArchiveEntry.BitFlagValues.DataDescriptor;
				this._archive.ArchiveStream.Seek(this._offsetOfLocalHeader + 6L, SeekOrigin.Begin);
				binaryWriter.Write((ushort)this._generalPurposeBitFlag);
			}
			this._archive.ArchiveStream.Seek(this._offsetOfLocalHeader + 14L, SeekOrigin.Begin);
			if (!flag2)
			{
				binaryWriter.Write(this._crc32);
				binaryWriter.Write(value);
				binaryWriter.Write(value2);
			}
			else
			{
				binaryWriter.Write(0U);
				binaryWriter.Write(0U);
				binaryWriter.Write(0U);
			}
			if (zip64HeaderUsed)
			{
				this._archive.ArchiveStream.Seek(this._offsetOfLocalHeader + 30L + (long)this._storedEntryNameBytes.Length + 4L, SeekOrigin.Begin);
				binaryWriter.Write(this._uncompressedSize);
				binaryWriter.Write(this._compressedSize);
				this._archive.ArchiveStream.Seek(position, SeekOrigin.Begin);
			}
			this._archive.ArchiveStream.Seek(position, SeekOrigin.Begin);
			if (flag2)
			{
				binaryWriter.Write(this._crc32);
				binaryWriter.Write(this._compressedSize);
				binaryWriter.Write(this._uncompressedSize);
			}
		}

		// Token: 0x0600010D RID: 269 RVA: 0x00006CC8 File Offset: 0x00004EC8
		private void WriteDataDescriptor()
		{
			BinaryWriter binaryWriter = new BinaryWriter(this._archive.ArchiveStream);
			binaryWriter.Write(134695760U);
			binaryWriter.Write(this._crc32);
			if (this.SizesTooLarge())
			{
				binaryWriter.Write(this._compressedSize);
				binaryWriter.Write(this._uncompressedSize);
				return;
			}
			binaryWriter.Write((uint)this._compressedSize);
			binaryWriter.Write((uint)this._uncompressedSize);
		}

		// Token: 0x0600010E RID: 270 RVA: 0x00006D38 File Offset: 0x00004F38
		private void UnloadStreams()
		{
			if (this._storedUncompressedData != null)
			{
				this._storedUncompressedData.Dispose();
			}
			this._compressedBytes = null;
			this._outstandingWriteStream = null;
		}

		// Token: 0x0600010F RID: 271 RVA: 0x00006D5B File Offset: 0x00004F5B
		private void CloseStreams()
		{
			if (this._outstandingWriteStream != null)
			{
				this._outstandingWriteStream.Dispose();
			}
		}

		// Token: 0x06000110 RID: 272 RVA: 0x00006D70 File Offset: 0x00004F70
		private void VersionToExtractAtLeast(ZipVersionNeededValues value)
		{
			if (this._versionToExtract < value)
			{
				this._versionToExtract = value;
			}
			if (this._versionMadeBySpecification < value)
			{
				this._versionMadeBySpecification = value;
			}
		}

		// Token: 0x06000111 RID: 273 RVA: 0x00006D92 File Offset: 0x00004F92
		private void ThrowIfInvalidArchive()
		{
			if (this._archive == null)
			{
				throw new InvalidOperationException("Cannot modify deleted entry.");
			}
			this._archive.ThrowIfDisposed();
		}

		// Token: 0x06000112 RID: 274 RVA: 0x00006DB4 File Offset: 0x00004FB4
		private static string GetFileName_Windows(string path)
		{
			int num = path.Length;
			while (--num >= 0)
			{
				char c = path[num];
				if (c == '\\' || c == '/' || c == ':')
				{
					return path.Substring(num + 1);
				}
			}
			return path;
		}

		// Token: 0x06000113 RID: 275 RVA: 0x00006DF4 File Offset: 0x00004FF4
		private static string GetFileName_Unix(string path)
		{
			int num = path.Length;
			while (--num >= 0)
			{
				if (path[num] == '/')
				{
					return path.Substring(num + 1);
				}
			}
			return path;
		}

		// Token: 0x06000114 RID: 276 RVA: 0x00006E28 File Offset: 0x00005028
		internal static string ParseFileName(string path, ZipVersionMadeByPlatform madeByPlatform)
		{
			if (madeByPlatform == ZipVersionMadeByPlatform.Windows)
			{
				return ZipArchiveEntry.GetFileName_Windows(path);
			}
			if (madeByPlatform != ZipVersionMadeByPlatform.Unix)
			{
				return ZipArchiveEntry.ParseFileName(path, ZipArchiveEntry.CurrentZipPlatform);
			}
			return ZipArchiveEntry.GetFileName_Unix(path);
		}

		// Token: 0x06000115 RID: 277 RVA: 0x00006E4C File Offset: 0x0000504C
		// Note: this type is marked as 'beforefieldinit'.
		static ZipArchiveEntry()
		{
		}

		// Token: 0x06000116 RID: 278 RVA: 0x00006E6D File Offset: 0x0000506D
		internal ZipArchiveEntry()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0400013B RID: 315
		private const ushort DefaultVersionToExtract = 10;

		// Token: 0x0400013C RID: 316
		private const int MaxSingleBufferSize = 2147483591;

		// Token: 0x0400013D RID: 317
		private ZipArchive _archive;

		// Token: 0x0400013E RID: 318
		private readonly bool _originallyInArchive;

		// Token: 0x0400013F RID: 319
		private readonly int _diskNumberStart;

		// Token: 0x04000140 RID: 320
		private readonly ZipVersionMadeByPlatform _versionMadeByPlatform;

		// Token: 0x04000141 RID: 321
		private ZipVersionNeededValues _versionMadeBySpecification;

		// Token: 0x04000142 RID: 322
		private ZipVersionNeededValues _versionToExtract;

		// Token: 0x04000143 RID: 323
		private ZipArchiveEntry.BitFlagValues _generalPurposeBitFlag;

		// Token: 0x04000144 RID: 324
		private ZipArchiveEntry.CompressionMethodValues _storedCompressionMethod;

		// Token: 0x04000145 RID: 325
		private DateTimeOffset _lastModified;

		// Token: 0x04000146 RID: 326
		private long _compressedSize;

		// Token: 0x04000147 RID: 327
		private long _uncompressedSize;

		// Token: 0x04000148 RID: 328
		private long _offsetOfLocalHeader;

		// Token: 0x04000149 RID: 329
		private long? _storedOffsetOfCompressedData;

		// Token: 0x0400014A RID: 330
		private uint _crc32;

		// Token: 0x0400014B RID: 331
		private byte[][] _compressedBytes;

		// Token: 0x0400014C RID: 332
		private MemoryStream _storedUncompressedData;

		// Token: 0x0400014D RID: 333
		private bool _currentlyOpenForWrite;

		// Token: 0x0400014E RID: 334
		private bool _everOpenedForWrite;

		// Token: 0x0400014F RID: 335
		private Stream _outstandingWriteStream;

		// Token: 0x04000150 RID: 336
		private uint _externalFileAttr;

		// Token: 0x04000151 RID: 337
		private string _storedEntryName;

		// Token: 0x04000152 RID: 338
		private byte[] _storedEntryNameBytes;

		// Token: 0x04000153 RID: 339
		private List<ZipGenericExtraField> _cdUnknownExtraFields;

		// Token: 0x04000154 RID: 340
		private List<ZipGenericExtraField> _lhUnknownExtraFields;

		// Token: 0x04000155 RID: 341
		private byte[] _fileComment;

		// Token: 0x04000156 RID: 342
		private CompressionLevel? _compressionLevel;

		// Token: 0x04000157 RID: 343
		private static readonly bool s_allowLargeZipArchiveEntriesInUpdateMode = IntPtr.Size > 4;

		// Token: 0x04000158 RID: 344
		internal static readonly ZipVersionMadeByPlatform CurrentZipPlatform = (Path.PathSeparator == '/') ? ZipVersionMadeByPlatform.Unix : ZipVersionMadeByPlatform.Windows;

		// Token: 0x0200001F RID: 31
		private sealed class DirectToArchiveWriterStream : Stream
		{
			// Token: 0x06000117 RID: 279 RVA: 0x00006E74 File Offset: 0x00005074
			public DirectToArchiveWriterStream(CheckSumAndSizeWriteStream crcSizeStream, ZipArchiveEntry entry)
			{
				this._position = 0L;
				this._crcSizeStream = crcSizeStream;
				this._everWritten = false;
				this._isDisposed = false;
				this._entry = entry;
				this._usedZip64inLH = false;
				this._canWrite = true;
			}

			// Token: 0x17000036 RID: 54
			// (get) Token: 0x06000118 RID: 280 RVA: 0x00006EAE File Offset: 0x000050AE
			public override long Length
			{
				get
				{
					this.ThrowIfDisposed();
					throw new NotSupportedException("This stream from ZipArchiveEntry does not support seeking.");
				}
			}

			// Token: 0x17000037 RID: 55
			// (get) Token: 0x06000119 RID: 281 RVA: 0x00006EC0 File Offset: 0x000050C0
			// (set) Token: 0x0600011A RID: 282 RVA: 0x00006EAE File Offset: 0x000050AE
			public override long Position
			{
				get
				{
					this.ThrowIfDisposed();
					return this._position;
				}
				set
				{
					this.ThrowIfDisposed();
					throw new NotSupportedException("This stream from ZipArchiveEntry does not support seeking.");
				}
			}

			// Token: 0x17000038 RID: 56
			// (get) Token: 0x0600011B RID: 283 RVA: 0x000022D7 File Offset: 0x000004D7
			public override bool CanRead
			{
				get
				{
					return false;
				}
			}

			// Token: 0x17000039 RID: 57
			// (get) Token: 0x0600011C RID: 284 RVA: 0x000022D7 File Offset: 0x000004D7
			public override bool CanSeek
			{
				get
				{
					return false;
				}
			}

			// Token: 0x1700003A RID: 58
			// (get) Token: 0x0600011D RID: 285 RVA: 0x00006ECE File Offset: 0x000050CE
			public override bool CanWrite
			{
				get
				{
					return this._canWrite;
				}
			}

			// Token: 0x0600011E RID: 286 RVA: 0x00006ED6 File Offset: 0x000050D6
			private void ThrowIfDisposed()
			{
				if (this._isDisposed)
				{
					throw new ObjectDisposedException(base.GetType().ToString(), "A stream from ZipArchiveEntry has been disposed.");
				}
			}

			// Token: 0x0600011F RID: 287 RVA: 0x00006EF6 File Offset: 0x000050F6
			public override int Read(byte[] buffer, int offset, int count)
			{
				this.ThrowIfDisposed();
				throw new NotSupportedException("This stream from ZipArchiveEntry does not support reading.");
			}

			// Token: 0x06000120 RID: 288 RVA: 0x00006EAE File Offset: 0x000050AE
			public override long Seek(long offset, SeekOrigin origin)
			{
				this.ThrowIfDisposed();
				throw new NotSupportedException("This stream from ZipArchiveEntry does not support seeking.");
			}

			// Token: 0x06000121 RID: 289 RVA: 0x00006F08 File Offset: 0x00005108
			public override void SetLength(long value)
			{
				this.ThrowIfDisposed();
				throw new NotSupportedException("SetLength requires a stream that supports seeking and writing.");
			}

			// Token: 0x06000122 RID: 290 RVA: 0x00006F1C File Offset: 0x0000511C
			public override void Write(byte[] buffer, int offset, int count)
			{
				if (buffer == null)
				{
					throw new ArgumentNullException("buffer");
				}
				if (offset < 0)
				{
					throw new ArgumentOutOfRangeException("offset", "The argument must be non-negative.");
				}
				if (count < 0)
				{
					throw new ArgumentOutOfRangeException("count", "The argument must be non-negative.");
				}
				if (buffer.Length - offset < count)
				{
					throw new ArgumentException("The offset and length parameters are not valid for the array that was given.");
				}
				this.ThrowIfDisposed();
				if (count == 0)
				{
					return;
				}
				if (!this._everWritten)
				{
					this._everWritten = true;
					this._usedZip64inLH = this._entry.WriteLocalFileHeader(false);
				}
				this._crcSizeStream.Write(buffer, offset, count);
				this._position += (long)count;
			}

			// Token: 0x06000123 RID: 291 RVA: 0x00006FBA File Offset: 0x000051BA
			public override void Flush()
			{
				this.ThrowIfDisposed();
				this._crcSizeStream.Flush();
			}

			// Token: 0x06000124 RID: 292 RVA: 0x00006FD0 File Offset: 0x000051D0
			protected override void Dispose(bool disposing)
			{
				if (disposing && !this._isDisposed)
				{
					this._crcSizeStream.Dispose();
					if (!this._everWritten)
					{
						this._entry.WriteLocalFileHeader(true);
					}
					else if (this._entry._archive.ArchiveStream.CanSeek)
					{
						this._entry.WriteCrcAndSizesInLocalHeader(this._usedZip64inLH);
					}
					else
					{
						this._entry.WriteDataDescriptor();
					}
					this._canWrite = false;
					this._isDisposed = true;
				}
				base.Dispose(disposing);
			}

			// Token: 0x04000159 RID: 345
			private long _position;

			// Token: 0x0400015A RID: 346
			private CheckSumAndSizeWriteStream _crcSizeStream;

			// Token: 0x0400015B RID: 347
			private bool _everWritten;

			// Token: 0x0400015C RID: 348
			private bool _isDisposed;

			// Token: 0x0400015D RID: 349
			private ZipArchiveEntry _entry;

			// Token: 0x0400015E RID: 350
			private bool _usedZip64inLH;

			// Token: 0x0400015F RID: 351
			private bool _canWrite;
		}

		// Token: 0x02000020 RID: 32
		[Flags]
		private enum BitFlagValues : ushort
		{
			// Token: 0x04000161 RID: 353
			DataDescriptor = 8,
			// Token: 0x04000162 RID: 354
			UnicodeFileName = 2048
		}

		// Token: 0x02000021 RID: 33
		internal enum CompressionMethodValues : ushort
		{
			// Token: 0x04000164 RID: 356
			Stored,
			// Token: 0x04000165 RID: 357
			Deflate = 8,
			// Token: 0x04000166 RID: 358
			Deflate64,
			// Token: 0x04000167 RID: 359
			BZip2 = 12,
			// Token: 0x04000168 RID: 360
			LZMA = 14
		}

		// Token: 0x02000022 RID: 34
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000125 RID: 293 RVA: 0x00007054 File Offset: 0x00005254
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000126 RID: 294 RVA: 0x00002157 File Offset: 0x00000357
			public <>c()
			{
			}

			// Token: 0x06000127 RID: 295 RVA: 0x00007060 File Offset: 0x00005260
			internal void <GetDataCompressor>b__67_0(long initialPosition, long currentPosition, uint checkSum, Stream backing, ZipArchiveEntry thisRef, EventHandler closeHandler)
			{
				thisRef._crc32 = checkSum;
				thisRef._uncompressedSize = currentPosition;
				thisRef._compressedSize = backing.Position - initialPosition;
				if (closeHandler != null)
				{
					closeHandler(thisRef, EventArgs.Empty);
				}
			}

			// Token: 0x06000128 RID: 296 RVA: 0x00007094 File Offset: 0x00005294
			internal void <OpenInWriteMode>b__70_0(object o, EventArgs e)
			{
				ZipArchiveEntry zipArchiveEntry = (ZipArchiveEntry)o;
				zipArchiveEntry._archive.ReleaseArchiveStream(zipArchiveEntry);
				zipArchiveEntry._outstandingWriteStream = null;
			}

			// Token: 0x06000129 RID: 297 RVA: 0x000070BB File Offset: 0x000052BB
			internal void <OpenInUpdateMode>b__71_0(ZipArchiveEntry thisRef)
			{
				thisRef._currentlyOpenForWrite = false;
			}

			// Token: 0x04000169 RID: 361
			public static readonly ZipArchiveEntry.<>c <>9 = new ZipArchiveEntry.<>c();

			// Token: 0x0400016A RID: 362
			public static Action<long, long, uint, Stream, ZipArchiveEntry, EventHandler> <>9__67_0;

			// Token: 0x0400016B RID: 363
			public static EventHandler <>9__70_0;

			// Token: 0x0400016C RID: 364
			public static Action<ZipArchiveEntry> <>9__71_0;
		}
	}
}
