﻿using System;

namespace System.IO.Compression
{
	// Token: 0x02000011 RID: 17
	internal interface IFileFormatWriter
	{
		// Token: 0x0600006C RID: 108
		byte[] GetHeader();

		// Token: 0x0600006D RID: 109
		void UpdateWithBytesRead(byte[] buffer, int offset, int bytesToCopy);

		// Token: 0x0600006E RID: 110
		byte[] GetFooter();
	}
}
