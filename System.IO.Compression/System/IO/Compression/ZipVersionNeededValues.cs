﻿using System;

namespace System.IO.Compression
{
	// Token: 0x0200002F RID: 47
	internal enum ZipVersionNeededValues : ushort
	{
		// Token: 0x040001CD RID: 461
		Default = 10,
		// Token: 0x040001CE RID: 462
		ExplicitDirectory = 20,
		// Token: 0x040001CF RID: 463
		Deflate = 20,
		// Token: 0x040001D0 RID: 464
		Deflate64,
		// Token: 0x040001D1 RID: 465
		Zip64 = 45
	}
}
