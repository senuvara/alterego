﻿using System;

namespace System.IO.Compression
{
	// Token: 0x02000016 RID: 22
	internal sealed class InputBuffer
	{
		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000088 RID: 136 RVA: 0x000047C8 File Offset: 0x000029C8
		public int AvailableBits
		{
			get
			{
				return this._bitsInBuffer;
			}
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000089 RID: 137 RVA: 0x000047D0 File Offset: 0x000029D0
		public int AvailableBytes
		{
			get
			{
				return this._end - this._start + this._bitsInBuffer / 8;
			}
		}

		// Token: 0x0600008A RID: 138 RVA: 0x000047E8 File Offset: 0x000029E8
		public bool EnsureBitsAvailable(int count)
		{
			if (this._bitsInBuffer < count)
			{
				if (this.NeedsInput())
				{
					return false;
				}
				uint bitBuffer = this._bitBuffer;
				byte[] buffer = this._buffer;
				int start = this._start;
				this._start = start + 1;
				this._bitBuffer = (bitBuffer | buffer[start] << (this._bitsInBuffer & 31));
				this._bitsInBuffer += 8;
				if (this._bitsInBuffer < count)
				{
					if (this.NeedsInput())
					{
						return false;
					}
					uint bitBuffer2 = this._bitBuffer;
					byte[] buffer2 = this._buffer;
					start = this._start;
					this._start = start + 1;
					this._bitBuffer = (bitBuffer2 | buffer2[start] << (this._bitsInBuffer & 31));
					this._bitsInBuffer += 8;
				}
			}
			return true;
		}

		// Token: 0x0600008B RID: 139 RVA: 0x0000489C File Offset: 0x00002A9C
		public uint TryLoad16Bits()
		{
			if (this._bitsInBuffer < 8)
			{
				if (this._start < this._end)
				{
					uint bitBuffer = this._bitBuffer;
					byte[] buffer = this._buffer;
					int start = this._start;
					this._start = start + 1;
					this._bitBuffer = (bitBuffer | buffer[start] << (this._bitsInBuffer & 31));
					this._bitsInBuffer += 8;
				}
				if (this._start < this._end)
				{
					uint bitBuffer2 = this._bitBuffer;
					byte[] buffer2 = this._buffer;
					int start = this._start;
					this._start = start + 1;
					this._bitBuffer = (bitBuffer2 | buffer2[start] << (this._bitsInBuffer & 31));
					this._bitsInBuffer += 8;
				}
			}
			else if (this._bitsInBuffer < 16 && this._start < this._end)
			{
				uint bitBuffer3 = this._bitBuffer;
				byte[] buffer3 = this._buffer;
				int start = this._start;
				this._start = start + 1;
				this._bitBuffer = (bitBuffer3 | buffer3[start] << (this._bitsInBuffer & 31));
				this._bitsInBuffer += 8;
			}
			return this._bitBuffer;
		}

		// Token: 0x0600008C RID: 140 RVA: 0x000049AB File Offset: 0x00002BAB
		private uint GetBitMask(int count)
		{
			return (1U << count) - 1U;
		}

		// Token: 0x0600008D RID: 141 RVA: 0x000049B5 File Offset: 0x00002BB5
		public int GetBits(int count)
		{
			if (!this.EnsureBitsAvailable(count))
			{
				return -1;
			}
			int result = (int)(this._bitBuffer & this.GetBitMask(count));
			this._bitBuffer >>= count;
			this._bitsInBuffer -= count;
			return result;
		}

		// Token: 0x0600008E RID: 142 RVA: 0x000049F0 File Offset: 0x00002BF0
		public int CopyTo(byte[] output, int offset, int length)
		{
			int num = 0;
			while (this._bitsInBuffer > 0 && length > 0)
			{
				output[offset++] = (byte)this._bitBuffer;
				this._bitBuffer >>= 8;
				this._bitsInBuffer -= 8;
				length--;
				num++;
			}
			if (length == 0)
			{
				return num;
			}
			int num2 = this._end - this._start;
			if (length > num2)
			{
				length = num2;
			}
			Array.Copy(this._buffer, this._start, output, offset, length);
			this._start += length;
			return num + length;
		}

		// Token: 0x0600008F RID: 143 RVA: 0x00004A81 File Offset: 0x00002C81
		public bool NeedsInput()
		{
			return this._start == this._end;
		}

		// Token: 0x06000090 RID: 144 RVA: 0x00004A91 File Offset: 0x00002C91
		public void SetInput(byte[] buffer, int offset, int length)
		{
			this._buffer = buffer;
			this._start = offset;
			this._end = offset + length;
		}

		// Token: 0x06000091 RID: 145 RVA: 0x00004AAA File Offset: 0x00002CAA
		public void SkipBits(int n)
		{
			this._bitBuffer >>= n;
			this._bitsInBuffer -= n;
		}

		// Token: 0x06000092 RID: 146 RVA: 0x00004ACB File Offset: 0x00002CCB
		public void SkipToByteBoundary()
		{
			this._bitBuffer >>= this._bitsInBuffer % 8;
			this._bitsInBuffer -= this._bitsInBuffer % 8;
		}

		// Token: 0x06000093 RID: 147 RVA: 0x00002157 File Offset: 0x00000357
		public InputBuffer()
		{
		}

		// Token: 0x04000110 RID: 272
		private byte[] _buffer;

		// Token: 0x04000111 RID: 273
		private int _start;

		// Token: 0x04000112 RID: 274
		private int _end;

		// Token: 0x04000113 RID: 275
		private uint _bitBuffer;

		// Token: 0x04000114 RID: 276
		private int _bitsInBuffer;
	}
}
