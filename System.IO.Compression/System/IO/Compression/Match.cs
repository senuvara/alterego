﻿using System;
using System.Runtime.CompilerServices;

namespace System.IO.Compression
{
	// Token: 0x02000017 RID: 23
	internal sealed class Match
	{
		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000094 RID: 148 RVA: 0x00004AFA File Offset: 0x00002CFA
		// (set) Token: 0x06000095 RID: 149 RVA: 0x00004B02 File Offset: 0x00002D02
		internal MatchState State
		{
			[CompilerGenerated]
			get
			{
				return this.<State>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<State>k__BackingField = value;
			}
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000096 RID: 150 RVA: 0x00004B0B File Offset: 0x00002D0B
		// (set) Token: 0x06000097 RID: 151 RVA: 0x00004B13 File Offset: 0x00002D13
		internal int Position
		{
			[CompilerGenerated]
			get
			{
				return this.<Position>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Position>k__BackingField = value;
			}
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000098 RID: 152 RVA: 0x00004B1C File Offset: 0x00002D1C
		// (set) Token: 0x06000099 RID: 153 RVA: 0x00004B24 File Offset: 0x00002D24
		internal int Length
		{
			[CompilerGenerated]
			get
			{
				return this.<Length>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Length>k__BackingField = value;
			}
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x0600009A RID: 154 RVA: 0x00004B2D File Offset: 0x00002D2D
		// (set) Token: 0x0600009B RID: 155 RVA: 0x00004B35 File Offset: 0x00002D35
		internal byte Symbol
		{
			[CompilerGenerated]
			get
			{
				return this.<Symbol>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Symbol>k__BackingField = value;
			}
		}

		// Token: 0x0600009C RID: 156 RVA: 0x00002157 File Offset: 0x00000357
		public Match()
		{
		}

		// Token: 0x04000115 RID: 277
		[CompilerGenerated]
		private MatchState <State>k__BackingField;

		// Token: 0x04000116 RID: 278
		[CompilerGenerated]
		private int <Position>k__BackingField;

		// Token: 0x04000117 RID: 279
		[CompilerGenerated]
		private int <Length>k__BackingField;

		// Token: 0x04000118 RID: 280
		[CompilerGenerated]
		private byte <Symbol>k__BackingField;
	}
}
