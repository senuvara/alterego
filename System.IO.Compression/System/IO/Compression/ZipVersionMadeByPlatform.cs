﻿using System;

namespace System.IO.Compression
{
	// Token: 0x02000030 RID: 48
	internal enum ZipVersionMadeByPlatform : byte
	{
		// Token: 0x040001D3 RID: 467
		Windows,
		// Token: 0x040001D4 RID: 468
		Unix = 3
	}
}
