﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace System.IO.Compression
{
	// Token: 0x0200001C RID: 28
	internal sealed class PositionPreservingWriteOnlyStreamWrapper : Stream
	{
		// Token: 0x060000B2 RID: 178 RVA: 0x00005020 File Offset: 0x00003220
		public PositionPreservingWriteOnlyStreamWrapper(Stream stream)
		{
			this._stream = stream;
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x060000B3 RID: 179 RVA: 0x000022D7 File Offset: 0x000004D7
		public override bool CanRead
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x060000B4 RID: 180 RVA: 0x000022D7 File Offset: 0x000004D7
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x060000B5 RID: 181 RVA: 0x0000502F File Offset: 0x0000322F
		public override bool CanWrite
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x060000B6 RID: 182 RVA: 0x00005032 File Offset: 0x00003232
		// (set) Token: 0x060000B7 RID: 183 RVA: 0x000022DA File Offset: 0x000004DA
		public override long Position
		{
			get
			{
				return this._position;
			}
			set
			{
				throw new NotSupportedException("This operation is not supported.");
			}
		}

		// Token: 0x060000B8 RID: 184 RVA: 0x0000503A File Offset: 0x0000323A
		public override void Write(byte[] buffer, int offset, int count)
		{
			this._position += (long)count;
			this._stream.Write(buffer, offset, count);
		}

		// Token: 0x060000B9 RID: 185 RVA: 0x00005059 File Offset: 0x00003259
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			this._position += (long)count;
			return this._stream.BeginWrite(buffer, offset, count, callback, state);
		}

		// Token: 0x060000BA RID: 186 RVA: 0x0000507C File Offset: 0x0000327C
		public override void EndWrite(IAsyncResult asyncResult)
		{
			this._stream.EndWrite(asyncResult);
		}

		// Token: 0x060000BB RID: 187 RVA: 0x0000508A File Offset: 0x0000328A
		public override void WriteByte(byte value)
		{
			this._position += 1L;
			this._stream.WriteByte(value);
		}

		// Token: 0x060000BC RID: 188 RVA: 0x000050A7 File Offset: 0x000032A7
		public override Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			this._position += (long)count;
			return this._stream.WriteAsync(buffer, offset, count, cancellationToken);
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x060000BD RID: 189 RVA: 0x000050C8 File Offset: 0x000032C8
		public override bool CanTimeout
		{
			get
			{
				return this._stream.CanTimeout;
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x060000BE RID: 190 RVA: 0x000050D5 File Offset: 0x000032D5
		// (set) Token: 0x060000BF RID: 191 RVA: 0x000050E2 File Offset: 0x000032E2
		public override int ReadTimeout
		{
			get
			{
				return this._stream.ReadTimeout;
			}
			set
			{
				this._stream.ReadTimeout = value;
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x060000C0 RID: 192 RVA: 0x000050F0 File Offset: 0x000032F0
		// (set) Token: 0x060000C1 RID: 193 RVA: 0x000050FD File Offset: 0x000032FD
		public override int WriteTimeout
		{
			get
			{
				return this._stream.WriteTimeout;
			}
			set
			{
				this._stream.WriteTimeout = value;
			}
		}

		// Token: 0x060000C2 RID: 194 RVA: 0x0000510B File Offset: 0x0000330B
		public override void Flush()
		{
			this._stream.Flush();
		}

		// Token: 0x060000C3 RID: 195 RVA: 0x00005118 File Offset: 0x00003318
		public override Task FlushAsync(CancellationToken cancellationToken)
		{
			return this._stream.FlushAsync(cancellationToken);
		}

		// Token: 0x060000C4 RID: 196 RVA: 0x00005126 File Offset: 0x00003326
		public override void Close()
		{
			this._stream.Close();
		}

		// Token: 0x060000C5 RID: 197 RVA: 0x00005133 File Offset: 0x00003333
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				this._stream.Dispose();
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x060000C6 RID: 198 RVA: 0x000022DA File Offset: 0x000004DA
		public override long Length
		{
			get
			{
				throw new NotSupportedException("This operation is not supported.");
			}
		}

		// Token: 0x060000C7 RID: 199 RVA: 0x000022DA File Offset: 0x000004DA
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException("This operation is not supported.");
		}

		// Token: 0x060000C8 RID: 200 RVA: 0x000022DA File Offset: 0x000004DA
		public override void SetLength(long value)
		{
			throw new NotSupportedException("This operation is not supported.");
		}

		// Token: 0x060000C9 RID: 201 RVA: 0x000022DA File Offset: 0x000004DA
		public override int Read(byte[] buffer, int offset, int count)
		{
			throw new NotSupportedException("This operation is not supported.");
		}

		// Token: 0x04000129 RID: 297
		private readonly Stream _stream;

		// Token: 0x0400012A RID: 298
		private long _position;
	}
}
