﻿using System;
using System.Collections.Generic;

namespace System.IO.Compression
{
	// Token: 0x02000025 RID: 37
	internal struct Zip64ExtraField
	{
		// Token: 0x1700003E RID: 62
		// (get) Token: 0x06000132 RID: 306 RVA: 0x0000726C File Offset: 0x0000546C
		public ushort TotalSize
		{
			get
			{
				return this._size + 4;
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x06000133 RID: 307 RVA: 0x00007277 File Offset: 0x00005477
		// (set) Token: 0x06000134 RID: 308 RVA: 0x0000727F File Offset: 0x0000547F
		public long? UncompressedSize
		{
			get
			{
				return this._uncompressedSize;
			}
			set
			{
				this._uncompressedSize = value;
				this.UpdateSize();
			}
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x06000135 RID: 309 RVA: 0x0000728E File Offset: 0x0000548E
		// (set) Token: 0x06000136 RID: 310 RVA: 0x00007296 File Offset: 0x00005496
		public long? CompressedSize
		{
			get
			{
				return this._compressedSize;
			}
			set
			{
				this._compressedSize = value;
				this.UpdateSize();
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x06000137 RID: 311 RVA: 0x000072A5 File Offset: 0x000054A5
		// (set) Token: 0x06000138 RID: 312 RVA: 0x000072AD File Offset: 0x000054AD
		public long? LocalHeaderOffset
		{
			get
			{
				return this._localHeaderOffset;
			}
			set
			{
				this._localHeaderOffset = value;
				this.UpdateSize();
			}
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x06000139 RID: 313 RVA: 0x000072BC File Offset: 0x000054BC
		public int? StartDiskNumber
		{
			get
			{
				return this._startDiskNumber;
			}
		}

		// Token: 0x0600013A RID: 314 RVA: 0x000072C4 File Offset: 0x000054C4
		private void UpdateSize()
		{
			this._size = 0;
			if (this._uncompressedSize != null)
			{
				this._size += 8;
			}
			if (this._compressedSize != null)
			{
				this._size += 8;
			}
			if (this._localHeaderOffset != null)
			{
				this._size += 8;
			}
			if (this._startDiskNumber != null)
			{
				this._size += 4;
			}
		}

		// Token: 0x0600013B RID: 315 RVA: 0x00007348 File Offset: 0x00005548
		public static Zip64ExtraField GetJustZip64Block(Stream extraFieldStream, bool readUncompressedSize, bool readCompressedSize, bool readLocalHeaderOffset, bool readStartDiskNumber)
		{
			Zip64ExtraField result;
			using (BinaryReader binaryReader = new BinaryReader(extraFieldStream))
			{
				ZipGenericExtraField extraField;
				while (ZipGenericExtraField.TryReadBlock(binaryReader, extraFieldStream.Length, out extraField))
				{
					if (Zip64ExtraField.TryGetZip64BlockFromGenericExtraField(extraField, readUncompressedSize, readCompressedSize, readLocalHeaderOffset, readStartDiskNumber, out result))
					{
						return result;
					}
				}
			}
			result = new Zip64ExtraField
			{
				_compressedSize = null,
				_uncompressedSize = null,
				_localHeaderOffset = null,
				_startDiskNumber = null
			};
			return result;
		}

		// Token: 0x0600013C RID: 316 RVA: 0x000073DC File Offset: 0x000055DC
		private static bool TryGetZip64BlockFromGenericExtraField(ZipGenericExtraField extraField, bool readUncompressedSize, bool readCompressedSize, bool readLocalHeaderOffset, bool readStartDiskNumber, out Zip64ExtraField zip64Block)
		{
			zip64Block = default(Zip64ExtraField);
			zip64Block._compressedSize = null;
			zip64Block._uncompressedSize = null;
			zip64Block._localHeaderOffset = null;
			zip64Block._startDiskNumber = null;
			if (extraField.Tag != 1)
			{
				return false;
			}
			MemoryStream memoryStream = null;
			bool result;
			try
			{
				memoryStream = new MemoryStream(extraField.Data);
				using (BinaryReader binaryReader = new BinaryReader(memoryStream))
				{
					memoryStream = null;
					zip64Block._size = extraField.Size;
					ushort num = 0;
					if (readUncompressedSize)
					{
						num += 8;
					}
					if (readCompressedSize)
					{
						num += 8;
					}
					if (readLocalHeaderOffset)
					{
						num += 8;
					}
					if (readStartDiskNumber)
					{
						num += 4;
					}
					if (num != zip64Block._size)
					{
						result = false;
					}
					else
					{
						if (readUncompressedSize)
						{
							zip64Block._uncompressedSize = new long?(binaryReader.ReadInt64());
						}
						if (readCompressedSize)
						{
							zip64Block._compressedSize = new long?(binaryReader.ReadInt64());
						}
						if (readLocalHeaderOffset)
						{
							zip64Block._localHeaderOffset = new long?(binaryReader.ReadInt64());
						}
						if (readStartDiskNumber)
						{
							zip64Block._startDiskNumber = new int?(binaryReader.ReadInt32());
						}
						if (zip64Block._uncompressedSize < 0L)
						{
							throw new InvalidDataException("Uncompressed Size cannot be held in an Int64.");
						}
						if (zip64Block._compressedSize < 0L)
						{
							throw new InvalidDataException("Compressed Size cannot be held in an Int64.");
						}
						if (zip64Block._localHeaderOffset < 0L)
						{
							throw new InvalidDataException("Local Header Offset cannot be held in an Int64.");
						}
						if (zip64Block._startDiskNumber < 0)
						{
							throw new InvalidDataException("Start Disk Number cannot be held in an Int64.");
						}
						result = true;
					}
				}
			}
			finally
			{
				if (memoryStream != null)
				{
					memoryStream.Dispose();
				}
			}
			return result;
		}

		// Token: 0x0600013D RID: 317 RVA: 0x000075E8 File Offset: 0x000057E8
		public static Zip64ExtraField GetAndRemoveZip64Block(List<ZipGenericExtraField> extraFields, bool readUncompressedSize, bool readCompressedSize, bool readLocalHeaderOffset, bool readStartDiskNumber)
		{
			Zip64ExtraField result = default(Zip64ExtraField);
			result._compressedSize = null;
			result._uncompressedSize = null;
			result._localHeaderOffset = null;
			result._startDiskNumber = null;
			List<ZipGenericExtraField> list = new List<ZipGenericExtraField>();
			bool flag = false;
			foreach (ZipGenericExtraField zipGenericExtraField in extraFields)
			{
				if (zipGenericExtraField.Tag == 1)
				{
					list.Add(zipGenericExtraField);
					if (!flag && Zip64ExtraField.TryGetZip64BlockFromGenericExtraField(zipGenericExtraField, readUncompressedSize, readCompressedSize, readLocalHeaderOffset, readStartDiskNumber, out result))
					{
						flag = true;
					}
				}
			}
			foreach (ZipGenericExtraField item in list)
			{
				extraFields.Remove(item);
			}
			return result;
		}

		// Token: 0x0600013E RID: 318 RVA: 0x000076DC File Offset: 0x000058DC
		public static void RemoveZip64Blocks(List<ZipGenericExtraField> extraFields)
		{
			List<ZipGenericExtraField> list = new List<ZipGenericExtraField>();
			foreach (ZipGenericExtraField item in extraFields)
			{
				if (item.Tag == 1)
				{
					list.Add(item);
				}
			}
			foreach (ZipGenericExtraField item2 in list)
			{
				extraFields.Remove(item2);
			}
		}

		// Token: 0x0600013F RID: 319 RVA: 0x00007778 File Offset: 0x00005978
		public void WriteBlock(Stream stream)
		{
			BinaryWriter binaryWriter = new BinaryWriter(stream);
			binaryWriter.Write(1);
			binaryWriter.Write(this._size);
			if (this._uncompressedSize != null)
			{
				binaryWriter.Write(this._uncompressedSize.Value);
			}
			if (this._compressedSize != null)
			{
				binaryWriter.Write(this._compressedSize.Value);
			}
			if (this._localHeaderOffset != null)
			{
				binaryWriter.Write(this._localHeaderOffset.Value);
			}
			if (this._startDiskNumber != null)
			{
				binaryWriter.Write(this._startDiskNumber.Value);
			}
		}

		// Token: 0x04000175 RID: 373
		public const int OffsetToFirstField = 4;

		// Token: 0x04000176 RID: 374
		private const ushort TagConstant = 1;

		// Token: 0x04000177 RID: 375
		private ushort _size;

		// Token: 0x04000178 RID: 376
		private long? _uncompressedSize;

		// Token: 0x04000179 RID: 377
		private long? _compressedSize;

		// Token: 0x0400017A RID: 378
		private long? _localHeaderOffset;

		// Token: 0x0400017B RID: 379
		private int? _startDiskNumber;
	}
}
