﻿using System;
using System.Collections.Generic;

namespace System.IO.Compression
{
	// Token: 0x02000028 RID: 40
	internal struct ZipLocalFileHeader
	{
		// Token: 0x06000144 RID: 324 RVA: 0x0000796C File Offset: 0x00005B6C
		public static List<ZipGenericExtraField> GetExtraFields(BinaryReader reader)
		{
			reader.BaseStream.Seek(26L, SeekOrigin.Current);
			ushort num = reader.ReadUInt16();
			ushort num2 = reader.ReadUInt16();
			reader.BaseStream.Seek((long)((ulong)num), SeekOrigin.Current);
			List<ZipGenericExtraField> list;
			using (Stream stream = new SubReadStream(reader.BaseStream, reader.BaseStream.Position, (long)((ulong)num2)))
			{
				list = ZipGenericExtraField.ParseExtraField(stream);
			}
			Zip64ExtraField.RemoveZip64Blocks(list);
			return list;
		}

		// Token: 0x06000145 RID: 325 RVA: 0x000079EC File Offset: 0x00005BEC
		public static bool TrySkipBlock(BinaryReader reader)
		{
			if (reader.ReadUInt32() != 67324752U)
			{
				return false;
			}
			if (reader.BaseStream.Length < reader.BaseStream.Position + 22L)
			{
				return false;
			}
			reader.BaseStream.Seek(22L, SeekOrigin.Current);
			ushort num = reader.ReadUInt16();
			ushort num2 = reader.ReadUInt16();
			if (reader.BaseStream.Length < reader.BaseStream.Position + (long)((ulong)num) + (long)((ulong)num2))
			{
				return false;
			}
			reader.BaseStream.Seek((long)(num + num2), SeekOrigin.Current);
			return true;
		}

		// Token: 0x0400018C RID: 396
		public const uint DataDescriptorSignature = 134695760U;

		// Token: 0x0400018D RID: 397
		public const uint SignatureConstant = 67324752U;

		// Token: 0x0400018E RID: 398
		public const int OffsetToCrcFromHeaderStart = 14;

		// Token: 0x0400018F RID: 399
		public const int OffsetToBitFlagFromHeaderStart = 6;

		// Token: 0x04000190 RID: 400
		public const int SizeOfLocalHeader = 30;
	}
}
