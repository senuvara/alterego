﻿using System;

namespace System.IO.Compression
{
	// Token: 0x0200002A RID: 42
	internal struct ZipEndOfCentralDirectoryBlock
	{
		// Token: 0x06000147 RID: 327 RVA: 0x00007CFC File Offset: 0x00005EFC
		public static void WriteBlock(Stream stream, long numberOfEntries, long startOfCentralDirectory, long sizeOfCentralDirectory, byte[] archiveComment)
		{
			BinaryWriter binaryWriter = new BinaryWriter(stream);
			ushort value = (numberOfEntries > 65535L) ? ushort.MaxValue : ((ushort)numberOfEntries);
			uint value2 = (startOfCentralDirectory > (long)((ulong)-1)) ? uint.MaxValue : ((uint)startOfCentralDirectory);
			uint value3 = (sizeOfCentralDirectory > (long)((ulong)-1)) ? uint.MaxValue : ((uint)sizeOfCentralDirectory);
			binaryWriter.Write(101010256U);
			binaryWriter.Write(0);
			binaryWriter.Write(0);
			binaryWriter.Write(value);
			binaryWriter.Write(value);
			binaryWriter.Write(value3);
			binaryWriter.Write(value2);
			binaryWriter.Write((archiveComment != null) ? ((ushort)archiveComment.Length) : 0);
			if (archiveComment != null)
			{
				binaryWriter.Write(archiveComment);
			}
		}

		// Token: 0x06000148 RID: 328 RVA: 0x00007D8C File Offset: 0x00005F8C
		public static bool TryReadBlock(BinaryReader reader, out ZipEndOfCentralDirectoryBlock eocdBlock)
		{
			eocdBlock = default(ZipEndOfCentralDirectoryBlock);
			if (reader.ReadUInt32() != 101010256U)
			{
				return false;
			}
			eocdBlock.Signature = 101010256U;
			eocdBlock.NumberOfThisDisk = reader.ReadUInt16();
			eocdBlock.NumberOfTheDiskWithTheStartOfTheCentralDirectory = reader.ReadUInt16();
			eocdBlock.NumberOfEntriesInTheCentralDirectoryOnThisDisk = reader.ReadUInt16();
			eocdBlock.NumberOfEntriesInTheCentralDirectory = reader.ReadUInt16();
			eocdBlock.SizeOfCentralDirectory = reader.ReadUInt32();
			eocdBlock.OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber = reader.ReadUInt32();
			ushort count = reader.ReadUInt16();
			eocdBlock.ArchiveComment = reader.ReadBytes((int)count);
			return true;
		}

		// Token: 0x040001A5 RID: 421
		public const uint SignatureConstant = 101010256U;

		// Token: 0x040001A6 RID: 422
		public const int SizeOfBlockWithoutSignature = 18;

		// Token: 0x040001A7 RID: 423
		public uint Signature;

		// Token: 0x040001A8 RID: 424
		public ushort NumberOfThisDisk;

		// Token: 0x040001A9 RID: 425
		public ushort NumberOfTheDiskWithTheStartOfTheCentralDirectory;

		// Token: 0x040001AA RID: 426
		public ushort NumberOfEntriesInTheCentralDirectoryOnThisDisk;

		// Token: 0x040001AB RID: 427
		public ushort NumberOfEntriesInTheCentralDirectory;

		// Token: 0x040001AC RID: 428
		public uint SizeOfCentralDirectory;

		// Token: 0x040001AD RID: 429
		public uint OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber;

		// Token: 0x040001AE RID: 430
		public byte[] ArchiveComment;
	}
}
