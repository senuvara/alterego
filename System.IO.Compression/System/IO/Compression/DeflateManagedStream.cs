﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace System.IO.Compression
{
	// Token: 0x02000009 RID: 9
	internal sealed class DeflateManagedStream : Stream
	{
		// Token: 0x06000018 RID: 24 RVA: 0x000021ED File Offset: 0x000003ED
		internal DeflateManagedStream(Stream stream, ZipArchiveEntry.CompressionMethodValues method)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			if (!stream.CanRead)
			{
				throw new ArgumentException("Stream does not support reading.", "stream");
			}
			this.InitializeInflater(stream, false, null, method);
		}

		// Token: 0x06000019 RID: 25 RVA: 0x00002228 File Offset: 0x00000428
		internal void InitializeInflater(Stream stream, bool leaveOpen, IFileFormatReader reader = null, ZipArchiveEntry.CompressionMethodValues method = ZipArchiveEntry.CompressionMethodValues.Deflate)
		{
			if (!stream.CanRead)
			{
				throw new ArgumentException("Stream does not support reading.", "stream");
			}
			this._inflater = new InflaterManaged(reader, method == ZipArchiveEntry.CompressionMethodValues.Deflate64);
			this._stream = stream;
			this._mode = CompressionMode.Decompress;
			this._leaveOpen = leaveOpen;
			this._buffer = new byte[8192];
		}

		// Token: 0x0600001A RID: 26 RVA: 0x00002288 File Offset: 0x00000488
		internal void SetFileFormatWriter(IFileFormatWriter writer)
		{
			if (writer != null)
			{
				this._formatWriter = writer;
			}
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x0600001B RID: 27 RVA: 0x00002294 File Offset: 0x00000494
		public override bool CanRead
		{
			get
			{
				return this._stream != null && this._mode == CompressionMode.Decompress && this._stream.CanRead;
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x0600001C RID: 28 RVA: 0x000022B5 File Offset: 0x000004B5
		public override bool CanWrite
		{
			get
			{
				return this._stream != null && this._mode == CompressionMode.Compress && this._stream.CanWrite;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x0600001D RID: 29 RVA: 0x000022D7 File Offset: 0x000004D7
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600001E RID: 30 RVA: 0x000022DA File Offset: 0x000004DA
		public override long Length
		{
			get
			{
				throw new NotSupportedException("This operation is not supported.");
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x0600001F RID: 31 RVA: 0x000022DA File Offset: 0x000004DA
		// (set) Token: 0x06000020 RID: 32 RVA: 0x000022DA File Offset: 0x000004DA
		public override long Position
		{
			get
			{
				throw new NotSupportedException("This operation is not supported.");
			}
			set
			{
				throw new NotSupportedException("This operation is not supported.");
			}
		}

		// Token: 0x06000021 RID: 33 RVA: 0x000022E6 File Offset: 0x000004E6
		public override void Flush()
		{
			this.EnsureNotDisposed();
		}

		// Token: 0x06000022 RID: 34 RVA: 0x000022EE File Offset: 0x000004EE
		public override Task FlushAsync(CancellationToken cancellationToken)
		{
			this.EnsureNotDisposed();
			if (!cancellationToken.IsCancellationRequested)
			{
				return Task.CompletedTask;
			}
			return Task.FromCanceled(cancellationToken);
		}

		// Token: 0x06000023 RID: 35 RVA: 0x000022DA File Offset: 0x000004DA
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException("This operation is not supported.");
		}

		// Token: 0x06000024 RID: 36 RVA: 0x000022DA File Offset: 0x000004DA
		public override void SetLength(long value)
		{
			throw new NotSupportedException("This operation is not supported.");
		}

		// Token: 0x06000025 RID: 37 RVA: 0x0000230C File Offset: 0x0000050C
		public override int Read(byte[] array, int offset, int count)
		{
			this.EnsureDecompressionMode();
			this.ValidateParameters(array, offset, count);
			this.EnsureNotDisposed();
			int num = offset;
			int num2 = count;
			for (;;)
			{
				int num3 = this._inflater.Inflate(array, num, num2);
				num += num3;
				num2 -= num3;
				if (num2 == 0 || this._inflater.Finished())
				{
					goto IL_8A;
				}
				int num4 = this._stream.Read(this._buffer, 0, this._buffer.Length);
				if (num4 <= 0)
				{
					goto IL_8A;
				}
				if (num4 > this._buffer.Length)
				{
					break;
				}
				this._inflater.SetInput(this._buffer, 0, num4);
			}
			throw new InvalidDataException("Found invalid data while decoding.");
			IL_8A:
			return count - num2;
		}

		// Token: 0x06000026 RID: 38 RVA: 0x000023A8 File Offset: 0x000005A8
		private void ValidateParameters(byte[] array, int offset, int count)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (array.Length - offset < count)
			{
				throw new ArgumentException("Offset plus count is larger than the length of target array.");
			}
		}

		// Token: 0x06000027 RID: 39 RVA: 0x000023F4 File Offset: 0x000005F4
		private void EnsureNotDisposed()
		{
			if (this._stream == null)
			{
				DeflateManagedStream.ThrowStreamClosedException();
			}
		}

		// Token: 0x06000028 RID: 40 RVA: 0x00002403 File Offset: 0x00000603
		[MethodImpl(MethodImplOptions.NoInlining)]
		private static void ThrowStreamClosedException()
		{
			throw new ObjectDisposedException(null, "Can not access a closed Stream.");
		}

		// Token: 0x06000029 RID: 41 RVA: 0x00002410 File Offset: 0x00000610
		private void EnsureDecompressionMode()
		{
			if (this._mode != CompressionMode.Decompress)
			{
				DeflateManagedStream.ThrowCannotReadFromDeflateManagedStreamException();
			}
		}

		// Token: 0x0600002A RID: 42 RVA: 0x0000241F File Offset: 0x0000061F
		[MethodImpl(MethodImplOptions.NoInlining)]
		private static void ThrowCannotReadFromDeflateManagedStreamException()
		{
			throw new InvalidOperationException("Reading from the compression stream is not supported.");
		}

		// Token: 0x0600002B RID: 43 RVA: 0x0000242B File Offset: 0x0000062B
		private void EnsureCompressionMode()
		{
			if (this._mode != CompressionMode.Compress)
			{
				DeflateManagedStream.ThrowCannotWriteToDeflateManagedStreamException();
			}
		}

		// Token: 0x0600002C RID: 44 RVA: 0x0000243B File Offset: 0x0000063B
		[MethodImpl(MethodImplOptions.NoInlining)]
		private static void ThrowCannotWriteToDeflateManagedStreamException()
		{
			throw new InvalidOperationException("Writing to the compression stream is not supported.");
		}

		// Token: 0x0600002D RID: 45 RVA: 0x00002447 File Offset: 0x00000647
		public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback asyncCallback, object asyncState)
		{
			return TaskToApm.Begin(this.ReadAsync(buffer, offset, count, CancellationToken.None), asyncCallback, asyncState);
		}

		// Token: 0x0600002E RID: 46 RVA: 0x00002460 File Offset: 0x00000660
		public override int EndRead(IAsyncResult asyncResult)
		{
			return TaskToApm.End<int>(asyncResult);
		}

		// Token: 0x0600002F RID: 47 RVA: 0x00002468 File Offset: 0x00000668
		public override Task<int> ReadAsync(byte[] array, int offset, int count, CancellationToken cancellationToken)
		{
			this.EnsureDecompressionMode();
			if (this._asyncOperations != 0)
			{
				throw new InvalidOperationException("Only one asynchronous reader or writer is allowed time at one time.");
			}
			this.ValidateParameters(array, offset, count);
			this.EnsureNotDisposed();
			if (cancellationToken.IsCancellationRequested)
			{
				return Task.FromCanceled<int>(cancellationToken);
			}
			Interlocked.Increment(ref this._asyncOperations);
			Task<int> task = null;
			Task<int> result;
			try
			{
				int num = this._inflater.Inflate(array, offset, count);
				if (num != 0)
				{
					result = Task.FromResult<int>(num);
				}
				else if (this._inflater.Finished())
				{
					result = Task.FromResult<int>(0);
				}
				else
				{
					task = this._stream.ReadAsync(this._buffer, 0, this._buffer.Length, cancellationToken);
					if (task == null)
					{
						throw new InvalidOperationException("Stream does not support reading.");
					}
					result = this.ReadAsyncCore(task, array, offset, count, cancellationToken);
				}
			}
			finally
			{
				if (task == null)
				{
					Interlocked.Decrement(ref this._asyncOperations);
				}
			}
			return result;
		}

		// Token: 0x06000030 RID: 48 RVA: 0x00002548 File Offset: 0x00000748
		private async Task<int> ReadAsyncCore(Task<int> readTask, byte[] array, int offset, int count, CancellationToken cancellationToken)
		{
			int result;
			try
			{
				int num;
				for (;;)
				{
					num = await readTask.ConfigureAwait(false);
					this.EnsureNotDisposed();
					if (num <= 0)
					{
						break;
					}
					if (num > this._buffer.Length)
					{
						goto Block_2;
					}
					cancellationToken.ThrowIfCancellationRequested();
					this._inflater.SetInput(this._buffer, 0, num);
					num = this._inflater.Inflate(array, offset, count);
					if (num != 0 || this._inflater.Finished())
					{
						goto IL_12C;
					}
					readTask = this._stream.ReadAsync(this._buffer, 0, this._buffer.Length, cancellationToken);
					if (readTask == null)
					{
						goto Block_5;
					}
				}
				return 0;
				Block_2:
				throw new InvalidDataException("Found invalid data while decoding.");
				Block_5:
				throw new InvalidOperationException("Stream does not support reading.");
				IL_12C:
				result = num;
			}
			finally
			{
				Interlocked.Decrement(ref this._asyncOperations);
			}
			return result;
		}

		// Token: 0x06000031 RID: 49 RVA: 0x000025B7 File Offset: 0x000007B7
		public override void Write(byte[] array, int offset, int count)
		{
			this.EnsureCompressionMode();
			this.ValidateParameters(array, offset, count);
			this.EnsureNotDisposed();
			this.DoMaintenance(array, offset, count);
			this.WriteDeflaterOutput();
			this._deflater.SetInput(array, offset, count);
			this.WriteDeflaterOutput();
		}

		// Token: 0x06000032 RID: 50 RVA: 0x000025F4 File Offset: 0x000007F4
		private void WriteDeflaterOutput()
		{
			while (!this._deflater.NeedsInput())
			{
				int deflateOutput = this._deflater.GetDeflateOutput(this._buffer);
				if (deflateOutput > 0)
				{
					this._stream.Write(this._buffer, 0, deflateOutput);
				}
			}
		}

		// Token: 0x06000033 RID: 51 RVA: 0x0000263C File Offset: 0x0000083C
		private void DoMaintenance(byte[] array, int offset, int count)
		{
			if (count <= 0)
			{
				return;
			}
			this._wroteBytes = true;
			if (this._formatWriter == null)
			{
				return;
			}
			if (!this._wroteHeader)
			{
				byte[] header = this._formatWriter.GetHeader();
				this._stream.Write(header, 0, header.Length);
				this._wroteHeader = true;
			}
			this._formatWriter.UpdateWithBytesRead(array, offset, count);
		}

		// Token: 0x06000034 RID: 52 RVA: 0x00002698 File Offset: 0x00000898
		private void PurgeBuffers(bool disposing)
		{
			if (!disposing)
			{
				return;
			}
			if (this._stream == null)
			{
				return;
			}
			this.Flush();
			if (this._mode != CompressionMode.Compress)
			{
				return;
			}
			if (this._wroteBytes)
			{
				this.WriteDeflaterOutput();
				bool flag;
				do
				{
					int num;
					flag = this._deflater.Finish(this._buffer, out num);
					if (num > 0)
					{
						this._stream.Write(this._buffer, 0, num);
					}
				}
				while (!flag);
			}
			else
			{
				bool flag2;
				do
				{
					int num2;
					flag2 = this._deflater.Finish(this._buffer, out num2);
				}
				while (!flag2);
			}
			if (this._formatWriter != null && this._wroteHeader)
			{
				byte[] footer = this._formatWriter.GetFooter();
				this._stream.Write(footer, 0, footer.Length);
			}
		}

		// Token: 0x06000035 RID: 53 RVA: 0x00002748 File Offset: 0x00000948
		protected override void Dispose(bool disposing)
		{
			try
			{
				this.PurgeBuffers(disposing);
			}
			finally
			{
				try
				{
					if (disposing && !this._leaveOpen && this._stream != null)
					{
						this._stream.Dispose();
					}
				}
				finally
				{
					this._stream = null;
					try
					{
						DeflaterManaged deflater = this._deflater;
						if (deflater != null)
						{
							deflater.Dispose();
						}
						InflaterManaged inflater = this._inflater;
						if (inflater != null)
						{
							inflater.Dispose();
						}
					}
					finally
					{
						this._deflater = null;
						this._inflater = null;
						base.Dispose(disposing);
					}
				}
			}
		}

		// Token: 0x06000036 RID: 54 RVA: 0x000027EC File Offset: 0x000009EC
		public override Task WriteAsync(byte[] array, int offset, int count, CancellationToken cancellationToken)
		{
			this.EnsureCompressionMode();
			if (this._asyncOperations != 0)
			{
				throw new InvalidOperationException("Only one asynchronous reader or writer is allowed time at one time.");
			}
			this.ValidateParameters(array, offset, count);
			this.EnsureNotDisposed();
			if (cancellationToken.IsCancellationRequested)
			{
				return Task.FromCanceled<int>(cancellationToken);
			}
			return this.WriteAsyncCore(array, offset, count, cancellationToken);
		}

		// Token: 0x06000037 RID: 55 RVA: 0x00002840 File Offset: 0x00000A40
		private async Task WriteAsyncCore(byte[] array, int offset, int count, CancellationToken cancellationToken)
		{
			Interlocked.Increment(ref this._asyncOperations);
			try
			{
				await base.WriteAsync(array, offset, count, cancellationToken).ConfigureAwait(false);
			}
			finally
			{
				Interlocked.Decrement(ref this._asyncOperations);
			}
		}

		// Token: 0x06000038 RID: 56 RVA: 0x000028A6 File Offset: 0x00000AA6
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback asyncCallback, object asyncState)
		{
			return TaskToApm.Begin(this.WriteAsync(buffer, offset, count, CancellationToken.None), asyncCallback, asyncState);
		}

		// Token: 0x06000039 RID: 57 RVA: 0x000028BF File Offset: 0x00000ABF
		public override void EndWrite(IAsyncResult asyncResult)
		{
			TaskToApm.End(asyncResult);
		}

		// Token: 0x0600003A RID: 58 RVA: 0x000028C7 File Offset: 0x00000AC7
		[CompilerGenerated]
		[DebuggerHidden]
		private Task <>n__0(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			return base.WriteAsync(buffer, offset, count, cancellationToken);
		}

		// Token: 0x0400007B RID: 123
		internal const int DefaultBufferSize = 8192;

		// Token: 0x0400007C RID: 124
		private Stream _stream;

		// Token: 0x0400007D RID: 125
		private CompressionMode _mode;

		// Token: 0x0400007E RID: 126
		private bool _leaveOpen;

		// Token: 0x0400007F RID: 127
		private InflaterManaged _inflater;

		// Token: 0x04000080 RID: 128
		private DeflaterManaged _deflater;

		// Token: 0x04000081 RID: 129
		private byte[] _buffer;

		// Token: 0x04000082 RID: 130
		private int _asyncOperations;

		// Token: 0x04000083 RID: 131
		private IFileFormatWriter _formatWriter;

		// Token: 0x04000084 RID: 132
		private bool _wroteHeader;

		// Token: 0x04000085 RID: 133
		private bool _wroteBytes;

		// Token: 0x0200000A RID: 10
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadAsyncCore>d__40 : IAsyncStateMachine
		{
			// Token: 0x0600003B RID: 59 RVA: 0x000028D4 File Offset: 0x00000AD4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DeflateManagedStream deflateManagedStream = this;
				int result;
				try
				{
					try
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num == 0)
						{
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
							num = (num2 = -1);
							goto IL_75;
						}
						IL_14:
						configuredTaskAwaiter = readTask.ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num = (num2 = 0);
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, DeflateManagedStream.<ReadAsyncCore>d__40>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_75:
						int num3 = configuredTaskAwaiter.GetResult();
						deflateManagedStream.EnsureNotDisposed();
						if (num3 <= 0)
						{
							result = 0;
						}
						else
						{
							if (num3 > deflateManagedStream._buffer.Length)
							{
								throw new InvalidDataException("Found invalid data while decoding.");
							}
							cancellationToken.ThrowIfCancellationRequested();
							deflateManagedStream._inflater.SetInput(deflateManagedStream._buffer, 0, num3);
							num3 = deflateManagedStream._inflater.Inflate(array, offset, count);
							if (num3 == 0 && !deflateManagedStream._inflater.Finished())
							{
								readTask = deflateManagedStream._stream.ReadAsync(deflateManagedStream._buffer, 0, deflateManagedStream._buffer.Length, cancellationToken);
								if (readTask == null)
								{
									throw new InvalidOperationException("Stream does not support reading.");
								}
								goto IL_14;
							}
							else
							{
								result = num3;
							}
						}
					}
					finally
					{
						if (num < 0)
						{
							Interlocked.Decrement(ref deflateManagedStream._asyncOperations);
						}
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x0600003C RID: 60 RVA: 0x00002A84 File Offset: 0x00000C84
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000086 RID: 134
			public int <>1__state;

			// Token: 0x04000087 RID: 135
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04000088 RID: 136
			public Task<int> readTask;

			// Token: 0x04000089 RID: 137
			public DeflateManagedStream <>4__this;

			// Token: 0x0400008A RID: 138
			public CancellationToken cancellationToken;

			// Token: 0x0400008B RID: 139
			public byte[] array;

			// Token: 0x0400008C RID: 140
			public int offset;

			// Token: 0x0400008D RID: 141
			public int count;

			// Token: 0x0400008E RID: 142
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200000B RID: 11
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteAsyncCore>d__47 : IAsyncStateMachine
		{
			// Token: 0x0600003D RID: 61 RVA: 0x00002A94 File Offset: 0x00000C94
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DeflateManagedStream deflateManagedStream = this;
				try
				{
					if (num != 0)
					{
						Interlocked.Increment(ref deflateManagedStream._asyncOperations);
					}
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							configuredTaskAwaiter = deflateManagedStream.<>n__0(array, offset, count, cancellationToken).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num = (num2 = 0);
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DeflateManagedStream.<WriteAsyncCore>d__47>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num = (num2 = -1);
						}
						configuredTaskAwaiter.GetResult();
					}
					finally
					{
						if (num < 0)
						{
							Interlocked.Decrement(ref deflateManagedStream._asyncOperations);
						}
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600003E RID: 62 RVA: 0x00002B98 File Offset: 0x00000D98
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400008F RID: 143
			public int <>1__state;

			// Token: 0x04000090 RID: 144
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000091 RID: 145
			public DeflateManagedStream <>4__this;

			// Token: 0x04000092 RID: 146
			public byte[] array;

			// Token: 0x04000093 RID: 147
			public int offset;

			// Token: 0x04000094 RID: 148
			public int count;

			// Token: 0x04000095 RID: 149
			public CancellationToken cancellationToken;

			// Token: 0x04000096 RID: 150
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
