﻿using System;

namespace System.IO.Compression
{
	/// <summary>Specifies values for interacting with zip archive entries.</summary>
	// Token: 0x02000023 RID: 35
	public enum ZipArchiveMode
	{
		/// <summary>Only reading archive entries is permitted.</summary>
		// Token: 0x0400016E RID: 366
		Read,
		/// <summary>Only creating new archive entries is permitted.</summary>
		// Token: 0x0400016F RID: 367
		Create,
		/// <summary>Both read and write operations are permitted for archive entries.</summary>
		// Token: 0x04000170 RID: 368
		Update
	}
}
