﻿using System;

namespace System.IO.Compression
{
	// Token: 0x02000006 RID: 6
	internal sealed class CopyEncoder
	{
		// Token: 0x0600000A RID: 10 RVA: 0x000020B8 File Offset: 0x000002B8
		public void GetBlock(DeflateInput input, OutputBuffer output, bool isFinal)
		{
			int num = 0;
			if (input != null)
			{
				num = Math.Min(input.Count, output.FreeBytes - 5 - output.BitsInBuffer);
				if (num > 65531)
				{
					num = 65531;
				}
			}
			if (isFinal)
			{
				output.WriteBits(3, 1U);
			}
			else
			{
				output.WriteBits(3, 0U);
			}
			output.FlushBits();
			this.WriteLenNLen((ushort)num, output);
			if (input != null && num > 0)
			{
				output.WriteBytes(input.Buffer, input.StartIndex, num);
				input.ConsumeBytes(num);
			}
		}

		// Token: 0x0600000B RID: 11 RVA: 0x00002138 File Offset: 0x00000338
		private void WriteLenNLen(ushort len, OutputBuffer output)
		{
			output.WriteUInt16(len);
			ushort value = ~len;
			output.WriteUInt16(value);
		}

		// Token: 0x0600000C RID: 12 RVA: 0x00002157 File Offset: 0x00000357
		public CopyEncoder()
		{
		}

		// Token: 0x04000074 RID: 116
		private const int PaddingSize = 5;

		// Token: 0x04000075 RID: 117
		private const int MaxUncompressedBlockSize = 65536;
	}
}
