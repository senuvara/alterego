﻿using System;

namespace System.IO.Compression
{
	// Token: 0x02000018 RID: 24
	internal enum MatchState
	{
		// Token: 0x0400011A RID: 282
		HasSymbol = 1,
		// Token: 0x0400011B RID: 283
		HasMatch,
		// Token: 0x0400011C RID: 284
		HasSymbolAndMatch
	}
}
