﻿using System;

namespace System.IO.Compression
{
	// Token: 0x0200002B RID: 43
	internal sealed class WrappedStream : Stream
	{
		// Token: 0x06000149 RID: 329 RVA: 0x00007E17 File Offset: 0x00006017
		internal WrappedStream(Stream baseStream, bool closeBaseStream) : this(baseStream, closeBaseStream, null, null)
		{
		}

		// Token: 0x0600014A RID: 330 RVA: 0x00007E23 File Offset: 0x00006023
		private WrappedStream(Stream baseStream, bool closeBaseStream, ZipArchiveEntry entry, Action<ZipArchiveEntry> onClosed)
		{
			this._baseStream = baseStream;
			this._closeBaseStream = closeBaseStream;
			this._onClosed = onClosed;
			this._zipArchiveEntry = entry;
			this._isDisposed = false;
		}

		// Token: 0x0600014B RID: 331 RVA: 0x00007E4F File Offset: 0x0000604F
		internal WrappedStream(Stream baseStream, ZipArchiveEntry entry, Action<ZipArchiveEntry> onClosed) : this(baseStream, false, entry, onClosed)
		{
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x0600014C RID: 332 RVA: 0x00007E5B File Offset: 0x0000605B
		public override long Length
		{
			get
			{
				this.ThrowIfDisposed();
				return this._baseStream.Length;
			}
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x0600014D RID: 333 RVA: 0x00007E6E File Offset: 0x0000606E
		// (set) Token: 0x0600014E RID: 334 RVA: 0x00007E81 File Offset: 0x00006081
		public override long Position
		{
			get
			{
				this.ThrowIfDisposed();
				return this._baseStream.Position;
			}
			set
			{
				this.ThrowIfDisposed();
				this.ThrowIfCantSeek();
				this._baseStream.Position = value;
			}
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x0600014F RID: 335 RVA: 0x00007E9B File Offset: 0x0000609B
		public override bool CanRead
		{
			get
			{
				return !this._isDisposed && this._baseStream.CanRead;
			}
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x06000150 RID: 336 RVA: 0x00007EB2 File Offset: 0x000060B2
		public override bool CanSeek
		{
			get
			{
				return !this._isDisposed && this._baseStream.CanSeek;
			}
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x06000151 RID: 337 RVA: 0x00007EC9 File Offset: 0x000060C9
		public override bool CanWrite
		{
			get
			{
				return !this._isDisposed && this._baseStream.CanWrite;
			}
		}

		// Token: 0x06000152 RID: 338 RVA: 0x00007EE0 File Offset: 0x000060E0
		private void ThrowIfDisposed()
		{
			if (this._isDisposed)
			{
				throw new ObjectDisposedException(base.GetType().ToString(), "A stream from ZipArchiveEntry has been disposed.");
			}
		}

		// Token: 0x06000153 RID: 339 RVA: 0x00007F00 File Offset: 0x00006100
		private void ThrowIfCantRead()
		{
			if (!this.CanRead)
			{
				throw new NotSupportedException("This stream from ZipArchiveEntry does not support reading.");
			}
		}

		// Token: 0x06000154 RID: 340 RVA: 0x00007F15 File Offset: 0x00006115
		private void ThrowIfCantWrite()
		{
			if (!this.CanWrite)
			{
				throw new NotSupportedException("This stream from ZipArchiveEntry does not support writing.");
			}
		}

		// Token: 0x06000155 RID: 341 RVA: 0x00007F2A File Offset: 0x0000612A
		private void ThrowIfCantSeek()
		{
			if (!this.CanSeek)
			{
				throw new NotSupportedException("This stream from ZipArchiveEntry does not support seeking.");
			}
		}

		// Token: 0x06000156 RID: 342 RVA: 0x00007F3F File Offset: 0x0000613F
		public override int Read(byte[] buffer, int offset, int count)
		{
			this.ThrowIfDisposed();
			this.ThrowIfCantRead();
			return this._baseStream.Read(buffer, offset, count);
		}

		// Token: 0x06000157 RID: 343 RVA: 0x00007F5B File Offset: 0x0000615B
		public override long Seek(long offset, SeekOrigin origin)
		{
			this.ThrowIfDisposed();
			this.ThrowIfCantSeek();
			return this._baseStream.Seek(offset, origin);
		}

		// Token: 0x06000158 RID: 344 RVA: 0x00007F76 File Offset: 0x00006176
		public override void SetLength(long value)
		{
			this.ThrowIfDisposed();
			this.ThrowIfCantSeek();
			this.ThrowIfCantWrite();
			this._baseStream.SetLength(value);
		}

		// Token: 0x06000159 RID: 345 RVA: 0x00007F96 File Offset: 0x00006196
		public override void Write(byte[] buffer, int offset, int count)
		{
			this.ThrowIfDisposed();
			this.ThrowIfCantWrite();
			this._baseStream.Write(buffer, offset, count);
		}

		// Token: 0x0600015A RID: 346 RVA: 0x00007FB2 File Offset: 0x000061B2
		public override void Flush()
		{
			this.ThrowIfDisposed();
			this.ThrowIfCantWrite();
			this._baseStream.Flush();
		}

		// Token: 0x0600015B RID: 347 RVA: 0x00007FCC File Offset: 0x000061CC
		protected override void Dispose(bool disposing)
		{
			if (disposing && !this._isDisposed)
			{
				Action<ZipArchiveEntry> onClosed = this._onClosed;
				if (onClosed != null)
				{
					onClosed(this._zipArchiveEntry);
				}
				if (this._closeBaseStream)
				{
					this._baseStream.Dispose();
				}
				this._isDisposed = true;
			}
			base.Dispose(disposing);
		}

		// Token: 0x040001AF RID: 431
		private readonly Stream _baseStream;

		// Token: 0x040001B0 RID: 432
		private readonly bool _closeBaseStream;

		// Token: 0x040001B1 RID: 433
		private readonly Action<ZipArchiveEntry> _onClosed;

		// Token: 0x040001B2 RID: 434
		private readonly ZipArchiveEntry _zipArchiveEntry;

		// Token: 0x040001B3 RID: 435
		private bool _isDisposed;
	}
}
