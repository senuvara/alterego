﻿using System;
using System.Collections.Generic;

namespace System.IO.Compression
{
	// Token: 0x02000024 RID: 36
	internal struct ZipGenericExtraField
	{
		// Token: 0x1700003B RID: 59
		// (get) Token: 0x0600012A RID: 298 RVA: 0x000070C4 File Offset: 0x000052C4
		public ushort Tag
		{
			get
			{
				return this._tag;
			}
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x0600012B RID: 299 RVA: 0x000070CC File Offset: 0x000052CC
		public ushort Size
		{
			get
			{
				return this._size;
			}
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x0600012C RID: 300 RVA: 0x000070D4 File Offset: 0x000052D4
		public byte[] Data
		{
			get
			{
				return this._data;
			}
		}

		// Token: 0x0600012D RID: 301 RVA: 0x000070DC File Offset: 0x000052DC
		public void WriteBlock(Stream stream)
		{
			BinaryWriter binaryWriter = new BinaryWriter(stream);
			binaryWriter.Write(this.Tag);
			binaryWriter.Write(this.Size);
			binaryWriter.Write(this.Data);
		}

		// Token: 0x0600012E RID: 302 RVA: 0x00007108 File Offset: 0x00005308
		public static bool TryReadBlock(BinaryReader reader, long endExtraField, out ZipGenericExtraField field)
		{
			field = default(ZipGenericExtraField);
			if (endExtraField - reader.BaseStream.Position < 4L)
			{
				return false;
			}
			field._tag = reader.ReadUInt16();
			field._size = reader.ReadUInt16();
			if (endExtraField - reader.BaseStream.Position < (long)((ulong)field._size))
			{
				return false;
			}
			field._data = reader.ReadBytes((int)field._size);
			return true;
		}

		// Token: 0x0600012F RID: 303 RVA: 0x00007174 File Offset: 0x00005374
		public static List<ZipGenericExtraField> ParseExtraField(Stream extraFieldData)
		{
			List<ZipGenericExtraField> list = new List<ZipGenericExtraField>();
			using (BinaryReader binaryReader = new BinaryReader(extraFieldData))
			{
				ZipGenericExtraField item;
				while (ZipGenericExtraField.TryReadBlock(binaryReader, extraFieldData.Length, out item))
				{
					list.Add(item);
				}
			}
			return list;
		}

		// Token: 0x06000130 RID: 304 RVA: 0x000071C4 File Offset: 0x000053C4
		public static int TotalSize(List<ZipGenericExtraField> fields)
		{
			int num = 0;
			foreach (ZipGenericExtraField zipGenericExtraField in fields)
			{
				num += (int)(zipGenericExtraField.Size + 4);
			}
			return num;
		}

		// Token: 0x06000131 RID: 305 RVA: 0x0000721C File Offset: 0x0000541C
		public static void WriteAllBlocks(List<ZipGenericExtraField> fields, Stream stream)
		{
			foreach (ZipGenericExtraField zipGenericExtraField in fields)
			{
				zipGenericExtraField.WriteBlock(stream);
			}
		}

		// Token: 0x04000171 RID: 369
		private const int SizeOfHeader = 4;

		// Token: 0x04000172 RID: 370
		private ushort _tag;

		// Token: 0x04000173 RID: 371
		private ushort _size;

		// Token: 0x04000174 RID: 372
		private byte[] _data;
	}
}
