﻿using System;

namespace System.IO.Compression
{
	// Token: 0x02000012 RID: 18
	internal interface IFileFormatReader
	{
		// Token: 0x0600006F RID: 111
		bool ReadHeader(InputBuffer input);

		// Token: 0x06000070 RID: 112
		bool ReadFooter(InputBuffer input);

		// Token: 0x06000071 RID: 113
		void UpdateWithBytesRead(byte[] buffer, int offset, int bytesToCopy);

		// Token: 0x06000072 RID: 114
		void Validate();
	}
}
