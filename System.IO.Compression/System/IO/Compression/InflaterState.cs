﻿using System;

namespace System.IO.Compression
{
	// Token: 0x02000015 RID: 21
	internal enum InflaterState
	{
		// Token: 0x040000F9 RID: 249
		ReadingHeader,
		// Token: 0x040000FA RID: 250
		ReadingBFinal = 2,
		// Token: 0x040000FB RID: 251
		ReadingBType,
		// Token: 0x040000FC RID: 252
		ReadingNumLitCodes,
		// Token: 0x040000FD RID: 253
		ReadingNumDistCodes,
		// Token: 0x040000FE RID: 254
		ReadingNumCodeLengthCodes,
		// Token: 0x040000FF RID: 255
		ReadingCodeLengthCodes,
		// Token: 0x04000100 RID: 256
		ReadingTreeCodesBefore,
		// Token: 0x04000101 RID: 257
		ReadingTreeCodesAfter,
		// Token: 0x04000102 RID: 258
		DecodeTop,
		// Token: 0x04000103 RID: 259
		HaveInitialLength,
		// Token: 0x04000104 RID: 260
		HaveFullLength,
		// Token: 0x04000105 RID: 261
		HaveDistCode,
		// Token: 0x04000106 RID: 262
		UncompressedAligning = 15,
		// Token: 0x04000107 RID: 263
		UncompressedByte1,
		// Token: 0x04000108 RID: 264
		UncompressedByte2,
		// Token: 0x04000109 RID: 265
		UncompressedByte3,
		// Token: 0x0400010A RID: 266
		UncompressedByte4,
		// Token: 0x0400010B RID: 267
		DecodingUncompressed,
		// Token: 0x0400010C RID: 268
		StartReadingFooter,
		// Token: 0x0400010D RID: 269
		ReadingFooter,
		// Token: 0x0400010E RID: 270
		VerifyingFooter,
		// Token: 0x0400010F RID: 271
		Done
	}
}
