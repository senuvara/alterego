﻿using System;
using System.Runtime.CompilerServices;

namespace System.IO.Compression
{
	// Token: 0x02000007 RID: 7
	internal sealed class DeflateInput
	{
		// Token: 0x17000001 RID: 1
		// (get) Token: 0x0600000D RID: 13 RVA: 0x0000215F File Offset: 0x0000035F
		// (set) Token: 0x0600000E RID: 14 RVA: 0x00002167 File Offset: 0x00000367
		internal byte[] Buffer
		{
			[CompilerGenerated]
			get
			{
				return this.<Buffer>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Buffer>k__BackingField = value;
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x0600000F RID: 15 RVA: 0x00002170 File Offset: 0x00000370
		// (set) Token: 0x06000010 RID: 16 RVA: 0x00002178 File Offset: 0x00000378
		internal int Count
		{
			[CompilerGenerated]
			get
			{
				return this.<Count>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Count>k__BackingField = value;
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000011 RID: 17 RVA: 0x00002181 File Offset: 0x00000381
		// (set) Token: 0x06000012 RID: 18 RVA: 0x00002189 File Offset: 0x00000389
		internal int StartIndex
		{
			[CompilerGenerated]
			get
			{
				return this.<StartIndex>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<StartIndex>k__BackingField = value;
			}
		}

		// Token: 0x06000013 RID: 19 RVA: 0x00002192 File Offset: 0x00000392
		internal void ConsumeBytes(int n)
		{
			this.StartIndex += n;
			this.Count -= n;
		}

		// Token: 0x06000014 RID: 20 RVA: 0x000021B0 File Offset: 0x000003B0
		internal DeflateInput.InputState DumpState()
		{
			return new DeflateInput.InputState(this.Count, this.StartIndex);
		}

		// Token: 0x06000015 RID: 21 RVA: 0x000021C3 File Offset: 0x000003C3
		internal void RestoreState(DeflateInput.InputState state)
		{
			this.Count = state._count;
			this.StartIndex = state._startIndex;
		}

		// Token: 0x06000016 RID: 22 RVA: 0x00002157 File Offset: 0x00000357
		public DeflateInput()
		{
		}

		// Token: 0x04000076 RID: 118
		[CompilerGenerated]
		private byte[] <Buffer>k__BackingField;

		// Token: 0x04000077 RID: 119
		[CompilerGenerated]
		private int <Count>k__BackingField;

		// Token: 0x04000078 RID: 120
		[CompilerGenerated]
		private int <StartIndex>k__BackingField;

		// Token: 0x02000008 RID: 8
		internal struct InputState
		{
			// Token: 0x06000017 RID: 23 RVA: 0x000021DD File Offset: 0x000003DD
			internal InputState(int count, int startIndex)
			{
				this._count = count;
				this._startIndex = startIndex;
			}

			// Token: 0x04000079 RID: 121
			internal readonly int _count;

			// Token: 0x0400007A RID: 122
			internal readonly int _startIndex;
		}
	}
}
