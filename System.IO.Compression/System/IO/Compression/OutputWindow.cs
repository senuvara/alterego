﻿using System;

namespace System.IO.Compression
{
	// Token: 0x0200001B RID: 27
	internal sealed class OutputWindow
	{
		// Token: 0x060000AB RID: 171 RVA: 0x00004D9C File Offset: 0x00002F9C
		public void Write(byte b)
		{
			byte[] window = this._window;
			int end = this._end;
			this._end = end + 1;
			window[end] = b;
			this._end &= 262143;
			this._bytesUsed++;
		}

		// Token: 0x060000AC RID: 172 RVA: 0x00004DE4 File Offset: 0x00002FE4
		public void WriteLengthDistance(int length, int distance)
		{
			this._bytesUsed += length;
			int num = this._end - distance & 262143;
			int num2 = 262144 - length;
			if (num > num2 || this._end >= num2)
			{
				while (length-- > 0)
				{
					byte[] window = this._window;
					int end = this._end;
					this._end = end + 1;
					window[end] = this._window[num++];
					this._end &= 262143;
					num &= 262143;
				}
				return;
			}
			if (length <= distance)
			{
				Array.Copy(this._window, num, this._window, this._end, length);
				this._end += length;
				return;
			}
			while (length-- > 0)
			{
				byte[] window2 = this._window;
				int end = this._end;
				this._end = end + 1;
				window2[end] = this._window[num++];
			}
		}

		// Token: 0x060000AD RID: 173 RVA: 0x00004ECC File Offset: 0x000030CC
		public int CopyFrom(InputBuffer input, int length)
		{
			length = Math.Min(Math.Min(length, 262144 - this._bytesUsed), input.AvailableBytes);
			int num = 262144 - this._end;
			int num2;
			if (length > num)
			{
				num2 = input.CopyTo(this._window, this._end, num);
				if (num2 == num)
				{
					num2 += input.CopyTo(this._window, 0, length - num);
				}
			}
			else
			{
				num2 = input.CopyTo(this._window, this._end, length);
			}
			this._end = (this._end + num2 & 262143);
			this._bytesUsed += num2;
			return num2;
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x060000AE RID: 174 RVA: 0x00004F6D File Offset: 0x0000316D
		public int FreeBytes
		{
			get
			{
				return 262144 - this._bytesUsed;
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x060000AF RID: 175 RVA: 0x00004F7B File Offset: 0x0000317B
		public int AvailableBytes
		{
			get
			{
				return this._bytesUsed;
			}
		}

		// Token: 0x060000B0 RID: 176 RVA: 0x00004F84 File Offset: 0x00003184
		public int CopyTo(byte[] output, int offset, int length)
		{
			int num;
			if (length > this._bytesUsed)
			{
				num = this._end;
				length = this._bytesUsed;
			}
			else
			{
				num = (this._end - this._bytesUsed + length & 262143);
			}
			int num2 = length;
			int num3 = length - num;
			if (num3 > 0)
			{
				Array.Copy(this._window, 262144 - num3, output, offset, num3);
				offset += num3;
				length = num;
			}
			Array.Copy(this._window, num - length, output, offset, length);
			this._bytesUsed -= num2;
			return num2;
		}

		// Token: 0x060000B1 RID: 177 RVA: 0x00005008 File Offset: 0x00003208
		public OutputWindow()
		{
		}

		// Token: 0x04000124 RID: 292
		private const int WindowSize = 262144;

		// Token: 0x04000125 RID: 293
		private const int WindowMask = 262143;

		// Token: 0x04000126 RID: 294
		private readonly byte[] _window = new byte[262144];

		// Token: 0x04000127 RID: 295
		private int _end;

		// Token: 0x04000128 RID: 296
		private int _bytesUsed;
	}
}
