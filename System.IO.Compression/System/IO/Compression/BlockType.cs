﻿using System;

namespace System.IO.Compression
{
	// Token: 0x02000005 RID: 5
	internal enum BlockType
	{
		// Token: 0x04000071 RID: 113
		Uncompressed,
		// Token: 0x04000072 RID: 114
		Static,
		// Token: 0x04000073 RID: 115
		Dynamic
	}
}
