﻿using System;

namespace System.IO.Compression
{
	// Token: 0x0200002C RID: 44
	internal sealed class SubReadStream : Stream
	{
		// Token: 0x0600015C RID: 348 RVA: 0x0000801C File Offset: 0x0000621C
		public SubReadStream(Stream superStream, long startPosition, long maxLength)
		{
			this._startInSuperStream = startPosition;
			this._positionInSuperStream = startPosition;
			this._endInSuperStream = startPosition + maxLength;
			this._superStream = superStream;
			this._canRead = true;
			this._isDisposed = false;
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x0600015D RID: 349 RVA: 0x00008050 File Offset: 0x00006250
		public override long Length
		{
			get
			{
				this.ThrowIfDisposed();
				return this._endInSuperStream - this._startInSuperStream;
			}
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x0600015E RID: 350 RVA: 0x00008065 File Offset: 0x00006265
		// (set) Token: 0x0600015F RID: 351 RVA: 0x0000807A File Offset: 0x0000627A
		public override long Position
		{
			get
			{
				this.ThrowIfDisposed();
				return this._positionInSuperStream - this._startInSuperStream;
			}
			set
			{
				this.ThrowIfDisposed();
				throw new NotSupportedException("This stream from ZipArchiveEntry does not support seeking.");
			}
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x06000160 RID: 352 RVA: 0x0000808C File Offset: 0x0000628C
		public override bool CanRead
		{
			get
			{
				return this._superStream.CanRead && this._canRead;
			}
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x06000161 RID: 353 RVA: 0x000022D7 File Offset: 0x000004D7
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x06000162 RID: 354 RVA: 0x000022D7 File Offset: 0x000004D7
		public override bool CanWrite
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06000163 RID: 355 RVA: 0x000080A3 File Offset: 0x000062A3
		private void ThrowIfDisposed()
		{
			if (this._isDisposed)
			{
				throw new ObjectDisposedException(base.GetType().ToString(), "A stream from ZipArchiveEntry has been disposed.");
			}
		}

		// Token: 0x06000164 RID: 356 RVA: 0x00007F00 File Offset: 0x00006100
		private void ThrowIfCantRead()
		{
			if (!this.CanRead)
			{
				throw new NotSupportedException("This stream from ZipArchiveEntry does not support reading.");
			}
		}

		// Token: 0x06000165 RID: 357 RVA: 0x000080C4 File Offset: 0x000062C4
		public override int Read(byte[] buffer, int offset, int count)
		{
			this.ThrowIfDisposed();
			this.ThrowIfCantRead();
			if (this._superStream.Position != this._positionInSuperStream)
			{
				this._superStream.Seek(this._positionInSuperStream, SeekOrigin.Begin);
			}
			if (this._positionInSuperStream + (long)count > this._endInSuperStream)
			{
				count = (int)(this._endInSuperStream - this._positionInSuperStream);
			}
			int num = this._superStream.Read(buffer, offset, count);
			this._positionInSuperStream += (long)num;
			return num;
		}

		// Token: 0x06000166 RID: 358 RVA: 0x0000807A File Offset: 0x0000627A
		public override long Seek(long offset, SeekOrigin origin)
		{
			this.ThrowIfDisposed();
			throw new NotSupportedException("This stream from ZipArchiveEntry does not support seeking.");
		}

		// Token: 0x06000167 RID: 359 RVA: 0x00008143 File Offset: 0x00006343
		public override void SetLength(long value)
		{
			this.ThrowIfDisposed();
			throw new NotSupportedException("SetLength requires a stream that supports seeking and writing.");
		}

		// Token: 0x06000168 RID: 360 RVA: 0x00008155 File Offset: 0x00006355
		public override void Write(byte[] buffer, int offset, int count)
		{
			this.ThrowIfDisposed();
			throw new NotSupportedException("This stream from ZipArchiveEntry does not support writing.");
		}

		// Token: 0x06000169 RID: 361 RVA: 0x00008155 File Offset: 0x00006355
		public override void Flush()
		{
			this.ThrowIfDisposed();
			throw new NotSupportedException("This stream from ZipArchiveEntry does not support writing.");
		}

		// Token: 0x0600016A RID: 362 RVA: 0x00008167 File Offset: 0x00006367
		protected override void Dispose(bool disposing)
		{
			if (disposing && !this._isDisposed)
			{
				this._canRead = false;
				this._isDisposed = true;
			}
			base.Dispose(disposing);
		}

		// Token: 0x040001B4 RID: 436
		private readonly long _startInSuperStream;

		// Token: 0x040001B5 RID: 437
		private long _positionInSuperStream;

		// Token: 0x040001B6 RID: 438
		private readonly long _endInSuperStream;

		// Token: 0x040001B7 RID: 439
		private readonly Stream _superStream;

		// Token: 0x040001B8 RID: 440
		private bool _canRead;

		// Token: 0x040001B9 RID: 441
		private bool _isDisposed;
	}
}
