﻿using System;

namespace System.IO.Compression
{
	// Token: 0x02000019 RID: 25
	internal sealed class OutputBuffer
	{
		// Token: 0x0600009D RID: 157 RVA: 0x00004B3E File Offset: 0x00002D3E
		internal void UpdateBuffer(byte[] output)
		{
			this._byteBuffer = output;
			this._pos = 0;
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x0600009E RID: 158 RVA: 0x00004B4E File Offset: 0x00002D4E
		internal int BytesWritten
		{
			get
			{
				return this._pos;
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x0600009F RID: 159 RVA: 0x00004B56 File Offset: 0x00002D56
		internal int FreeBytes
		{
			get
			{
				return this._byteBuffer.Length - this._pos;
			}
		}

		// Token: 0x060000A0 RID: 160 RVA: 0x00004B68 File Offset: 0x00002D68
		internal void WriteUInt16(ushort value)
		{
			byte[] byteBuffer = this._byteBuffer;
			int pos = this._pos;
			this._pos = pos + 1;
			byteBuffer[pos] = (byte)value;
			byte[] byteBuffer2 = this._byteBuffer;
			pos = this._pos;
			this._pos = pos + 1;
			byteBuffer2[pos] = (byte)(value >> 8);
		}

		// Token: 0x060000A1 RID: 161 RVA: 0x00004BAC File Offset: 0x00002DAC
		internal void WriteBits(int n, uint bits)
		{
			this._bitBuf |= bits << this._bitCount;
			this._bitCount += n;
			if (this._bitCount >= 16)
			{
				byte[] byteBuffer = this._byteBuffer;
				int pos = this._pos;
				this._pos = pos + 1;
				byteBuffer[pos] = (byte)this._bitBuf;
				byte[] byteBuffer2 = this._byteBuffer;
				pos = this._pos;
				this._pos = pos + 1;
				byteBuffer2[pos] = (byte)(this._bitBuf >> 8);
				this._bitCount -= 16;
				this._bitBuf >>= 16;
			}
		}

		// Token: 0x060000A2 RID: 162 RVA: 0x00004C48 File Offset: 0x00002E48
		internal void FlushBits()
		{
			while (this._bitCount >= 8)
			{
				byte[] byteBuffer = this._byteBuffer;
				int pos = this._pos;
				this._pos = pos + 1;
				byteBuffer[pos] = (byte)this._bitBuf;
				this._bitCount -= 8;
				this._bitBuf >>= 8;
			}
			if (this._bitCount > 0)
			{
				byte[] byteBuffer2 = this._byteBuffer;
				int pos = this._pos;
				this._pos = pos + 1;
				byteBuffer2[pos] = (byte)this._bitBuf;
				this._bitBuf = 0U;
				this._bitCount = 0;
			}
		}

		// Token: 0x060000A3 RID: 163 RVA: 0x00004CD1 File Offset: 0x00002ED1
		internal void WriteBytes(byte[] byteArray, int offset, int count)
		{
			if (this._bitCount == 0)
			{
				Array.Copy(byteArray, offset, this._byteBuffer, this._pos, count);
				this._pos += count;
				return;
			}
			this.WriteBytesUnaligned(byteArray, offset, count);
		}

		// Token: 0x060000A4 RID: 164 RVA: 0x00004D08 File Offset: 0x00002F08
		private void WriteBytesUnaligned(byte[] byteArray, int offset, int count)
		{
			for (int i = 0; i < count; i++)
			{
				byte b = byteArray[offset + i];
				this.WriteByteUnaligned(b);
			}
		}

		// Token: 0x060000A5 RID: 165 RVA: 0x00004D2E File Offset: 0x00002F2E
		private void WriteByteUnaligned(byte b)
		{
			this.WriteBits(8, (uint)b);
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x060000A6 RID: 166 RVA: 0x00004D38 File Offset: 0x00002F38
		internal int BitsInBuffer
		{
			get
			{
				return this._bitCount / 8 + 1;
			}
		}

		// Token: 0x060000A7 RID: 167 RVA: 0x00004D44 File Offset: 0x00002F44
		internal OutputBuffer.BufferState DumpState()
		{
			return new OutputBuffer.BufferState(this._pos, this._bitBuf, this._bitCount);
		}

		// Token: 0x060000A8 RID: 168 RVA: 0x00004D5D File Offset: 0x00002F5D
		internal void RestoreState(OutputBuffer.BufferState state)
		{
			this._pos = state._pos;
			this._bitBuf = state._bitBuf;
			this._bitCount = state._bitCount;
		}

		// Token: 0x060000A9 RID: 169 RVA: 0x00002157 File Offset: 0x00000357
		public OutputBuffer()
		{
		}

		// Token: 0x0400011D RID: 285
		private byte[] _byteBuffer;

		// Token: 0x0400011E RID: 286
		private int _pos;

		// Token: 0x0400011F RID: 287
		private uint _bitBuf;

		// Token: 0x04000120 RID: 288
		private int _bitCount;

		// Token: 0x0200001A RID: 26
		internal struct BufferState
		{
			// Token: 0x060000AA RID: 170 RVA: 0x00004D83 File Offset: 0x00002F83
			internal BufferState(int pos, uint bitBuf, int bitCount)
			{
				this._pos = pos;
				this._bitBuf = bitBuf;
				this._bitCount = bitCount;
			}

			// Token: 0x04000121 RID: 289
			internal readonly int _pos;

			// Token: 0x04000122 RID: 290
			internal readonly uint _bitBuf;

			// Token: 0x04000123 RID: 291
			internal readonly int _bitCount;
		}
	}
}
