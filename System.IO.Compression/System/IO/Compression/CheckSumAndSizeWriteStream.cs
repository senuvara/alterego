﻿using System;

namespace System.IO.Compression
{
	// Token: 0x0200002D RID: 45
	internal sealed class CheckSumAndSizeWriteStream : Stream
	{
		// Token: 0x0600016B RID: 363 RVA: 0x0000818C File Offset: 0x0000638C
		public CheckSumAndSizeWriteStream(Stream baseStream, Stream baseBaseStream, bool leaveOpenOnClose, ZipArchiveEntry entry, EventHandler onClose, Action<long, long, uint, Stream, ZipArchiveEntry, EventHandler> saveCrcAndSizes)
		{
			this._baseStream = baseStream;
			this._baseBaseStream = baseBaseStream;
			this._position = 0L;
			this._checksum = 0U;
			this._leaveOpenOnClose = leaveOpenOnClose;
			this._canWrite = true;
			this._isDisposed = false;
			this._initialPosition = 0L;
			this._zipArchiveEntry = entry;
			this._onClose = onClose;
			this._saveCrcAndSizes = saveCrcAndSizes;
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x0600016C RID: 364 RVA: 0x000081F1 File Offset: 0x000063F1
		public override long Length
		{
			get
			{
				this.ThrowIfDisposed();
				throw new NotSupportedException("This stream from ZipArchiveEntry does not support seeking.");
			}
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x0600016D RID: 365 RVA: 0x00008203 File Offset: 0x00006403
		// (set) Token: 0x0600016E RID: 366 RVA: 0x000081F1 File Offset: 0x000063F1
		public override long Position
		{
			get
			{
				this.ThrowIfDisposed();
				return this._position;
			}
			set
			{
				this.ThrowIfDisposed();
				throw new NotSupportedException("This stream from ZipArchiveEntry does not support seeking.");
			}
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x0600016F RID: 367 RVA: 0x000022D7 File Offset: 0x000004D7
		public override bool CanRead
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x06000170 RID: 368 RVA: 0x000022D7 File Offset: 0x000004D7
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x06000171 RID: 369 RVA: 0x00008211 File Offset: 0x00006411
		public override bool CanWrite
		{
			get
			{
				return this._canWrite;
			}
		}

		// Token: 0x06000172 RID: 370 RVA: 0x00008219 File Offset: 0x00006419
		private void ThrowIfDisposed()
		{
			if (this._isDisposed)
			{
				throw new ObjectDisposedException(base.GetType().ToString(), "A stream from ZipArchiveEntry has been disposed.");
			}
		}

		// Token: 0x06000173 RID: 371 RVA: 0x00008239 File Offset: 0x00006439
		public override int Read(byte[] buffer, int offset, int count)
		{
			this.ThrowIfDisposed();
			throw new NotSupportedException("This stream from ZipArchiveEntry does not support reading.");
		}

		// Token: 0x06000174 RID: 372 RVA: 0x000081F1 File Offset: 0x000063F1
		public override long Seek(long offset, SeekOrigin origin)
		{
			this.ThrowIfDisposed();
			throw new NotSupportedException("This stream from ZipArchiveEntry does not support seeking.");
		}

		// Token: 0x06000175 RID: 373 RVA: 0x0000824B File Offset: 0x0000644B
		public override void SetLength(long value)
		{
			this.ThrowIfDisposed();
			throw new NotSupportedException("SetLength requires a stream that supports seeking and writing.");
		}

		// Token: 0x06000176 RID: 374 RVA: 0x00008260 File Offset: 0x00006460
		public override void Write(byte[] buffer, int offset, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", "The argument must be non-negative.");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", "The argument must be non-negative.");
			}
			if (buffer.Length - offset < count)
			{
				throw new ArgumentException("The offset and length parameters are not valid for the array that was given.");
			}
			this.ThrowIfDisposed();
			if (count == 0)
			{
				return;
			}
			if (!this._everWritten)
			{
				this._initialPosition = this._baseBaseStream.Position;
				this._everWritten = true;
			}
			this._checksum = Crc32Helper.UpdateCrc32(this._checksum, buffer, offset, count);
			this._baseStream.Write(buffer, offset, count);
			this._position += (long)count;
		}

		// Token: 0x06000177 RID: 375 RVA: 0x00008311 File Offset: 0x00006511
		public override void Flush()
		{
			this.ThrowIfDisposed();
			this._baseStream.Flush();
		}

		// Token: 0x06000178 RID: 376 RVA: 0x00008324 File Offset: 0x00006524
		protected override void Dispose(bool disposing)
		{
			if (disposing && !this._isDisposed)
			{
				if (!this._everWritten)
				{
					this._initialPosition = this._baseBaseStream.Position;
				}
				if (!this._leaveOpenOnClose)
				{
					this._baseStream.Dispose();
				}
				Action<long, long, uint, Stream, ZipArchiveEntry, EventHandler> saveCrcAndSizes = this._saveCrcAndSizes;
				if (saveCrcAndSizes != null)
				{
					saveCrcAndSizes(this._initialPosition, this.Position, this._checksum, this._baseBaseStream, this._zipArchiveEntry, this._onClose);
				}
				this._isDisposed = true;
			}
			base.Dispose(disposing);
		}

		// Token: 0x040001BA RID: 442
		private readonly Stream _baseStream;

		// Token: 0x040001BB RID: 443
		private readonly Stream _baseBaseStream;

		// Token: 0x040001BC RID: 444
		private long _position;

		// Token: 0x040001BD RID: 445
		private uint _checksum;

		// Token: 0x040001BE RID: 446
		private readonly bool _leaveOpenOnClose;

		// Token: 0x040001BF RID: 447
		private bool _canWrite;

		// Token: 0x040001C0 RID: 448
		private bool _isDisposed;

		// Token: 0x040001C1 RID: 449
		private bool _everWritten;

		// Token: 0x040001C2 RID: 450
		private long _initialPosition;

		// Token: 0x040001C3 RID: 451
		private readonly ZipArchiveEntry _zipArchiveEntry;

		// Token: 0x040001C4 RID: 452
		private readonly EventHandler _onClose;

		// Token: 0x040001C5 RID: 453
		private readonly Action<long, long, uint, Stream, ZipArchiveEntry, EventHandler> _saveCrcAndSizes;
	}
}
