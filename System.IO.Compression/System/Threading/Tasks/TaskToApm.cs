﻿using System;
using System.Runtime.CompilerServices;

namespace System.Threading.Tasks
{
	// Token: 0x02000032 RID: 50
	internal static class TaskToApm
	{
		// Token: 0x06000184 RID: 388 RVA: 0x00008858 File Offset: 0x00006A58
		public static IAsyncResult Begin(Task task, AsyncCallback callback, object state)
		{
			IAsyncResult asyncResult;
			if (task.IsCompleted)
			{
				asyncResult = new TaskToApm.TaskWrapperAsyncResult(task, state, true);
				if (callback != null)
				{
					callback(asyncResult);
				}
			}
			else
			{
				IAsyncResult asyncResult3;
				if (task.AsyncState != state)
				{
					IAsyncResult asyncResult2 = new TaskToApm.TaskWrapperAsyncResult(task, state, false);
					asyncResult3 = asyncResult2;
				}
				else
				{
					asyncResult3 = task;
				}
				asyncResult = asyncResult3;
				if (callback != null)
				{
					TaskToApm.InvokeCallbackWhenTaskCompletes(task, callback, asyncResult);
				}
			}
			return asyncResult;
		}

		// Token: 0x06000185 RID: 389 RVA: 0x000088A8 File Offset: 0x00006AA8
		public static void End(IAsyncResult asyncResult)
		{
			TaskToApm.TaskWrapperAsyncResult taskWrapperAsyncResult = asyncResult as TaskToApm.TaskWrapperAsyncResult;
			Task task;
			if (taskWrapperAsyncResult != null)
			{
				task = taskWrapperAsyncResult.Task;
			}
			else
			{
				task = (asyncResult as Task);
			}
			if (task == null)
			{
				throw new ArgumentNullException();
			}
			task.GetAwaiter().GetResult();
		}

		// Token: 0x06000186 RID: 390 RVA: 0x000088E8 File Offset: 0x00006AE8
		public static TResult End<TResult>(IAsyncResult asyncResult)
		{
			TaskToApm.TaskWrapperAsyncResult taskWrapperAsyncResult = asyncResult as TaskToApm.TaskWrapperAsyncResult;
			Task<TResult> task;
			if (taskWrapperAsyncResult != null)
			{
				task = (taskWrapperAsyncResult.Task as Task<TResult>);
			}
			else
			{
				task = (asyncResult as Task<TResult>);
			}
			if (task == null)
			{
				throw new ArgumentNullException();
			}
			return task.GetAwaiter().GetResult();
		}

		// Token: 0x06000187 RID: 391 RVA: 0x0000892C File Offset: 0x00006B2C
		private static void InvokeCallbackWhenTaskCompletes(Task antecedent, AsyncCallback callback, IAsyncResult asyncResult)
		{
			antecedent.ConfigureAwait(false).GetAwaiter().OnCompleted(delegate
			{
				callback(asyncResult);
			});
		}

		// Token: 0x02000033 RID: 51
		private sealed class TaskWrapperAsyncResult : IAsyncResult
		{
			// Token: 0x06000188 RID: 392 RVA: 0x00008970 File Offset: 0x00006B70
			internal TaskWrapperAsyncResult(Task task, object state, bool completedSynchronously)
			{
				this.Task = task;
				this._state = state;
				this._completedSynchronously = completedSynchronously;
			}

			// Token: 0x17000052 RID: 82
			// (get) Token: 0x06000189 RID: 393 RVA: 0x0000898D File Offset: 0x00006B8D
			object IAsyncResult.AsyncState
			{
				get
				{
					return this._state;
				}
			}

			// Token: 0x17000053 RID: 83
			// (get) Token: 0x0600018A RID: 394 RVA: 0x00008995 File Offset: 0x00006B95
			bool IAsyncResult.CompletedSynchronously
			{
				get
				{
					return this._completedSynchronously;
				}
			}

			// Token: 0x17000054 RID: 84
			// (get) Token: 0x0600018B RID: 395 RVA: 0x0000899D File Offset: 0x00006B9D
			bool IAsyncResult.IsCompleted
			{
				get
				{
					return this.Task.IsCompleted;
				}
			}

			// Token: 0x17000055 RID: 85
			// (get) Token: 0x0600018C RID: 396 RVA: 0x000089AA File Offset: 0x00006BAA
			WaitHandle IAsyncResult.AsyncWaitHandle
			{
				get
				{
					return ((IAsyncResult)this.Task).AsyncWaitHandle;
				}
			}

			// Token: 0x040001DD RID: 477
			internal readonly Task Task;

			// Token: 0x040001DE RID: 478
			private readonly object _state;

			// Token: 0x040001DF RID: 479
			private readonly bool _completedSynchronously;
		}

		// Token: 0x02000034 RID: 52
		[CompilerGenerated]
		private sealed class <>c__DisplayClass3_0
		{
			// Token: 0x0600018D RID: 397 RVA: 0x00002157 File Offset: 0x00000357
			public <>c__DisplayClass3_0()
			{
			}

			// Token: 0x0600018E RID: 398 RVA: 0x000089B7 File Offset: 0x00006BB7
			internal void <InvokeCallbackWhenTaskCompletes>b__0()
			{
				this.callback(this.asyncResult);
			}

			// Token: 0x040001E0 RID: 480
			public AsyncCallback callback;

			// Token: 0x040001E1 RID: 481
			public IAsyncResult asyncResult;
		}
	}
}
