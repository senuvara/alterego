﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Token: 0x02000035 RID: 53
[CompilerGenerated]
internal sealed class <PrivateImplementationDetails>
{
	// Token: 0x040001E2 RID: 482 RVA: 0x000089D4 File Offset: 0x00006BD4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1024 0B7FD85925E9433BF3A6037BE741E8204DE4C7BE;

	// Token: 0x040001E3 RID: 483 RVA: 0x00008DD4 File Offset: 0x00006FD4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1024 0FD10D7F8CC523A1965CE894696E9F57630FD38D;

	// Token: 0x040001E4 RID: 484 RVA: 0x000091D4 File Offset: 0x000073D4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1024 246C5401780254CAC9EB6FDBEA931DAA6878E793;

	// Token: 0x040001E5 RID: 485 RVA: 0x000095D4 File Offset: 0x000077D4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=32 2492606636F4A4666E0D617B51116A5A68539881;

	// Token: 0x040001E6 RID: 486 RVA: 0x000095F4 File Offset: 0x000077F4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=29 2DAF59AD82CBE58E893B0C025E68180B6C4037B4;

	// Token: 0x040001E7 RID: 487 RVA: 0x00009611 File Offset: 0x00007811
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=98 345AD00C4B31ADE2765BCF63BC137F7F7E6819B8;

	// Token: 0x040001E8 RID: 488 RVA: 0x00009673 File Offset: 0x00007873
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1024 373B494F210C656134C5728D551D4C97B013EB33;

	// Token: 0x040001E9 RID: 489 RVA: 0x00009A73 File Offset: 0x00007C73
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=98 5001D1286D6CE39FFA3E67C66D5D68A487F49D0D;

	// Token: 0x040001EA RID: 490 RVA: 0x00009AD5 File Offset: 0x00007CD5
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1024 5AA7D0C2D138A84EFE90031D571726896428A268;

	// Token: 0x040001EB RID: 491 RVA: 0x00009ED5 File Offset: 0x000080D5
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1024 6A7F0402FAAF3E739B9773CFA1EE25B1B5F88F7F;

	// Token: 0x040001EC RID: 492 RVA: 0x0000A2D5 File Offset: 0x000084D5
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=128 6A94F0C3DCA389344CEDB99F171FA1E092E842E6;

	// Token: 0x040001ED RID: 493 RVA: 0x0000A355 File Offset: 0x00008555
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=32 794CB2CC08B7EC30CA04323EFAC1B017E5B5D2C6;

	// Token: 0x040001EE RID: 494 RVA: 0x0000A375 File Offset: 0x00008575
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=64 8ED4E99B936B26A09EDFAB9E336CF75F4913B454;

	// Token: 0x040001EF RID: 495 RVA: 0x0000A3B5 File Offset: 0x000085B5
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=19 9F8365E9D6C62D3B47026EC465B05A7B5526B5CD;

	// Token: 0x040001F0 RID: 496 RVA: 0x0000A3C8 File Offset: 0x000085C8
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1024 A001BF98C82457C80B4E4E5A12AC22AAE18DFA1F;

	// Token: 0x040001F1 RID: 497 RVA: 0x0000A7C8 File Offset: 0x000089C8
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=128 A207CFAA460B8D3D4A2F0CECE2F3F5AB742798AB;

	// Token: 0x040001F2 RID: 498 RVA: 0x0000A848 File Offset: 0x00008A48
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=2052 C0C10EC6AF4A4101F894B153E1CD493ADC01A67F;

	// Token: 0x040001F3 RID: 499 RVA: 0x0000B04C File Offset: 0x0000924C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=29 D8FC711F1CC83D7118A9B118E950C05928A2FAB3;

	// Token: 0x040001F4 RID: 500 RVA: 0x0000B069 File Offset: 0x00009269
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=116 E9AB5B250BCB4314314008E0E9FE1B3E3C11C861;

	// Token: 0x040001F5 RID: 501 RVA: 0x0000B0DD File Offset: 0x000092DD
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1024 EE129F04713DB8E02DCAA1833E24967BC2D09DFC;

	// Token: 0x02000036 RID: 54
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 19)]
	private struct __StaticArrayInitTypeSize=19
	{
	}

	// Token: 0x02000037 RID: 55
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 29)]
	private struct __StaticArrayInitTypeSize=29
	{
	}

	// Token: 0x02000038 RID: 56
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 32)]
	private struct __StaticArrayInitTypeSize=32
	{
	}

	// Token: 0x02000039 RID: 57
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 64)]
	private struct __StaticArrayInitTypeSize=64
	{
	}

	// Token: 0x0200003A RID: 58
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 98)]
	private struct __StaticArrayInitTypeSize=98
	{
	}

	// Token: 0x0200003B RID: 59
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 116)]
	private struct __StaticArrayInitTypeSize=116
	{
	}

	// Token: 0x0200003C RID: 60
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 128)]
	private struct __StaticArrayInitTypeSize=128
	{
	}

	// Token: 0x0200003D RID: 61
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 1024)]
	private struct __StaticArrayInitTypeSize=1024
	{
	}

	// Token: 0x0200003E RID: 62
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 2052)]
	private struct __StaticArrayInitTypeSize=2052
	{
	}
}
