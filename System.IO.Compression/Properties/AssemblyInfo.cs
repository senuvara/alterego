﻿using System;
using System.Diagnostics;
using System.IO.Compression;
using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyVersion("4.0.0.0")]
[assembly: AssemblyDelaySign(true)]
[assembly: AssemblyInformationalVersion("4.0.30319.17020")]
[assembly: AssemblyCopyright("(c) Various Mono authors")]
[assembly: AssemblyProduct("Mono Common Language Infrastructure")]
[assembly: AssemblyCompany("Mono development team")]
[assembly: AssemblyDefaultAlias("System.IO.Compression.dll")]
[assembly: AssemblyDescription("System.IO.Compression.dll")]
[assembly: AssemblyTitle("System.IO.Compression.dll")]
[assembly: AssemblyFileVersion("4.0.30319.17020")]
// System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e
[assembly: TypeForwardedTo(typeof(CompressionLevel))]
[assembly: TypeForwardedTo(typeof(CompressionMode))]
[assembly: TypeForwardedTo(typeof(DeflateStream))]
[assembly: TypeForwardedTo(typeof(GZipStream))]
