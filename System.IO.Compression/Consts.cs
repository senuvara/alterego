﻿using System;

// Token: 0x02000002 RID: 2
internal static class Consts
{
	// Token: 0x04000001 RID: 1
	public const string MonoVersion = "5.11.0.0";

	// Token: 0x04000002 RID: 2
	public const string MonoCompany = "Mono development team";

	// Token: 0x04000003 RID: 3
	public const string MonoProduct = "Mono Common Language Infrastructure";

	// Token: 0x04000004 RID: 4
	public const string MonoCopyright = "(c) Various Mono authors";

	// Token: 0x04000005 RID: 5
	public const int MonoCorlibVersion = 1051100001;

	// Token: 0x04000006 RID: 6
	public const string FxVersion = "4.0.0.0";

	// Token: 0x04000007 RID: 7
	public const string FxFileVersion = "4.0.30319.17020";

	// Token: 0x04000008 RID: 8
	public const string EnvironmentVersion = "4.0.30319.17020";

	// Token: 0x04000009 RID: 9
	public const string VsVersion = "0.0.0.0";

	// Token: 0x0400000A RID: 10
	public const string VsFileVersion = "11.0.0.0";

	// Token: 0x0400000B RID: 11
	private const string PublicKeyToken = "b77a5c561934e089";

	// Token: 0x0400000C RID: 12
	public const string AssemblyI18N = "I18N, Version=4.0.0.0, Culture=neutral, PublicKeyToken=0738eb9f132ed756";

	// Token: 0x0400000D RID: 13
	public const string AssemblyMicrosoft_JScript = "Microsoft.JScript, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x0400000E RID: 14
	public const string AssemblyMicrosoft_VisualStudio = "Microsoft.VisualStudio, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x0400000F RID: 15
	public const string AssemblyMicrosoft_VisualStudio_Web = "Microsoft.VisualStudio.Web, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x04000010 RID: 16
	public const string AssemblyMicrosoft_VSDesigner = "Microsoft.VSDesigner, Version=0.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x04000011 RID: 17
	public const string AssemblyMono_Http = "Mono.Http, Version=4.0.0.0, Culture=neutral, PublicKeyToken=0738eb9f132ed756";

	// Token: 0x04000012 RID: 18
	public const string AssemblyMono_Posix = "Mono.Posix, Version=4.0.0.0, Culture=neutral, PublicKeyToken=0738eb9f132ed756";

	// Token: 0x04000013 RID: 19
	public const string AssemblyMono_Security = "Mono.Security, Version=4.0.0.0, Culture=neutral, PublicKeyToken=0738eb9f132ed756";

	// Token: 0x04000014 RID: 20
	public const string AssemblyMono_Messaging_RabbitMQ = "Mono.Messaging.RabbitMQ, Version=4.0.0.0, Culture=neutral, PublicKeyToken=0738eb9f132ed756";

	// Token: 0x04000015 RID: 21
	public const string AssemblyCorlib = "mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";

	// Token: 0x04000016 RID: 22
	public const string AssemblySystem = "System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";

	// Token: 0x04000017 RID: 23
	public const string AssemblySystem_Data = "System.Data, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";

	// Token: 0x04000018 RID: 24
	public const string AssemblySystem_Design = "System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x04000019 RID: 25
	public const string AssemblySystem_DirectoryServices = "System.DirectoryServices, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x0400001A RID: 26
	public const string AssemblySystem_Drawing = "System.Drawing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x0400001B RID: 27
	public const string AssemblySystem_Drawing_Design = "System.Drawing.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x0400001C RID: 28
	public const string AssemblySystem_Messaging = "System.Messaging, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x0400001D RID: 29
	public const string AssemblySystem_Security = "System.Security, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x0400001E RID: 30
	public const string AssemblySystem_ServiceProcess = "System.ServiceProcess, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x0400001F RID: 31
	public const string AssemblySystem_Web = "System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

	// Token: 0x04000020 RID: 32
	public const string AssemblySystem_Windows_Forms = "System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";

	// Token: 0x04000021 RID: 33
	public const string AssemblySystem_2_0 = "System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";

	// Token: 0x04000022 RID: 34
	public const string AssemblySystemCore_3_5 = "System.Core, Version=3.5.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";

	// Token: 0x04000023 RID: 35
	public const string AssemblySystem_Core = "System.Core, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";

	// Token: 0x04000024 RID: 36
	public const string WindowsBase_3_0 = "WindowsBase, Version=3.0.0.0, PublicKeyToken=31bf3856ad364e35";

	// Token: 0x04000025 RID: 37
	public const string AssemblyWindowsBase = "WindowsBase, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35";

	// Token: 0x04000026 RID: 38
	public const string AssemblyPresentationCore_3_5 = "PresentationCore, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35";

	// Token: 0x04000027 RID: 39
	public const string AssemblyPresentationCore_4_0 = "PresentationCore, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35";

	// Token: 0x04000028 RID: 40
	public const string AssemblyPresentationFramework_3_5 = "PresentationFramework, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35";

	// Token: 0x04000029 RID: 41
	public const string AssemblySystemServiceModel_3_0 = "System.ServiceModel, Version=3.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";
}
