﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x0200003C RID: 60
public class AdfurikunMovieNativeAdViewUtility : MonoBehaviour
{
	// Token: 0x0600026B RID: 619 RVA: 0x0001370C File Offset: 0x0001190C
	public void Awake()
	{
		if (AdfurikunMovieNativeAdViewUtility.mInstance == null)
		{
			AdfurikunMovieNativeAdViewUtility.mInstance = this;
			return;
		}
		Object.Destroy(base.gameObject);
	}

	// Token: 0x0600026C RID: 620 RVA: 0x00013730 File Offset: 0x00011930
	private void Start()
	{
		if (Application.isEditor)
		{
			return;
		}
		if (Application.platform == RuntimePlatform.Android)
		{
			AndroidJavaObject androidJavaObject = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity").Call<AndroidJavaObject>("getWindowManager", Array.Empty<object>());
			if (androidJavaObject != null)
			{
				AndroidJavaObject androidJavaObject2 = androidJavaObject.Call<AndroidJavaObject>("getDefaultDisplay", Array.Empty<object>());
				if (androidJavaObject2 != null)
				{
					AndroidJavaObject androidJavaObject3 = new AndroidJavaObject("android.util.DisplayMetrics", Array.Empty<object>());
					androidJavaObject2.Call("getMetrics", new object[]
					{
						androidJavaObject3
					});
				}
			}
		}
		this.initializeMovieNativeAdView();
	}

	// Token: 0x0600026D RID: 621 RVA: 0x000137B4 File Offset: 0x000119B4
	public void OnApplicationPause(bool pause)
	{
		if (Application.isEditor)
		{
			return;
		}
		if (Application.platform == RuntimePlatform.Android)
		{
			if (pause)
			{
				this.callAndroidMovieNativeAdViewMethod("onPause");
				return;
			}
			this.callAndroidMovieNativeAdViewMethod("onResume");
		}
	}

	// Token: 0x0600026E RID: 622 RVA: 0x000137E1 File Offset: 0x000119E1
	public void initializeMovieNativeAdView()
	{
		this.initializeMovieNativeAdView(this.getAppID());
	}

	// Token: 0x0600026F RID: 623 RVA: 0x000137F0 File Offset: 0x000119F0
	public void initializeMovieNativeAdView(string appId)
	{
		if (Application.platform == RuntimePlatform.Android)
		{
			AndroidJavaObject @static = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
			new AndroidJavaClass("jp.tjkapp.adfurikunsdk.moviereward.unityplugin.AdfurikunUnityManager").CallStatic("initialize", new object[]
			{
				@static,
				this.unityPluginVersion
			});
			this.makeInstance_AdfurikunMovieNativeAdViewController().CallStatic("initialize", new object[]
			{
				@static,
				appId
			});
		}
	}

	// Token: 0x06000270 RID: 624 RVA: 0x00013860 File Offset: 0x00011A60
	public void loadMovieNativeAdView()
	{
		this.loadMovieNativeAdView(this.getAppID());
	}

	// Token: 0x06000271 RID: 625 RVA: 0x0001386E File Offset: 0x00011A6E
	public void loadMovieNativeAdView(string appId)
	{
		if (Application.platform == RuntimePlatform.Android)
		{
			this.makeInstance_AdfurikunMovieNativeAdViewController().CallStatic("load", new object[]
			{
				appId
			});
		}
	}

	// Token: 0x06000272 RID: 626 RVA: 0x00013893 File Offset: 0x00011A93
	public void setMovieNativeAdView(float x, float y, float width, float height)
	{
		this.setMovieNativeAdView(this.getAppID(), x, y, width, height);
	}

	// Token: 0x06000273 RID: 627 RVA: 0x000138A8 File Offset: 0x00011AA8
	public void setMovieNativeAdView(string appId, float x, float y, float width, float height)
	{
		if (Application.platform == RuntimePlatform.Android)
		{
			AndroidJavaObject androidJavaObject = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity").Call<AndroidJavaObject>("getWindow", Array.Empty<object>()).Call<AndroidJavaObject>("getDecorView", Array.Empty<object>());
			int num = androidJavaObject.Call<int>("getWidth", Array.Empty<object>());
			int num2 = androidJavaObject.Call<int>("getHeight", Array.Empty<object>());
			float num3 = (float)num * (width / (float)Screen.width);
			float num4 = (float)num2 * (height / (float)Screen.height);
			float num5 = (float)num * (x / (float)Screen.width);
			float num6 = (float)num2 * (y / (float)Screen.height);
			this.makeInstance_AdfurikunMovieNativeAdViewController().CallStatic("show", new object[]
			{
				appId,
				num5,
				num6,
				num3,
				num4
			});
		}
	}

	// Token: 0x06000274 RID: 628 RVA: 0x00013984 File Offset: 0x00011B84
	public void setMovieNativeAdViewFrame(float x, float y, float width, float height)
	{
		this.setMovieNativeAdViewFrame(this.getAppID(), x, y, width, height);
	}

	// Token: 0x06000275 RID: 629 RVA: 0x00013997 File Offset: 0x00011B97
	public void setMovieNativeAdViewFrame(string appId, float x, float y, float width, float height)
	{
		if (Application.platform == RuntimePlatform.Android)
		{
			this.setMovieNativeAdView(appId, x, y, width, height);
		}
	}

	// Token: 0x06000276 RID: 630 RVA: 0x000139AF File Offset: 0x00011BAF
	public void setFrameGravity(float displaySizeW, float displaySizeH, float width, float height, int horizontalGravity, int verticalGravity)
	{
		this.setFrameGravity(this.getAppID(), displaySizeW, displaySizeH, width, height, horizontalGravity, verticalGravity);
	}

	// Token: 0x06000277 RID: 631 RVA: 0x000139C8 File Offset: 0x00011BC8
	public void setFrameGravity(string appId, float displaySizeW, float displaySizeH, float width, float height, int horizontalGravity, int verticalGravity)
	{
		float num = (float)Screen.width / displaySizeW;
		float num2 = width * num;
		float num3 = height * num;
		float screenPositionByGravity = this.getScreenPositionByGravity(horizontalGravity, (float)Screen.width, num2);
		float screenPositionByGravity2 = this.getScreenPositionByGravity(verticalGravity, (float)Screen.height, num3);
		this.setMovieNativeAdViewFrame(appId, screenPositionByGravity, screenPositionByGravity2, num2, num3);
	}

	// Token: 0x06000278 RID: 632 RVA: 0x00013A15 File Offset: 0x00011C15
	public void setFitWidthFrame(float displaySizeH, float height, int verticalGravity)
	{
		this.setFitWidthFrame(this.getAppID(), displaySizeH, height, verticalGravity);
	}

	// Token: 0x06000279 RID: 633 RVA: 0x00013A28 File Offset: 0x00011C28
	public void setFitWidthFrame(string appId, float displaySizeH, float height, int verticalGravity)
	{
		float num = (float)Screen.height / displaySizeH;
		float num2 = height * num;
		float width = (float)Screen.width;
		float screenPositionByGravity = this.getScreenPositionByGravity(verticalGravity, (float)Screen.height, num2);
		this.setMovieNativeAdViewFrame(appId, 0f, screenPositionByGravity, width, num2);
	}

	// Token: 0x0600027A RID: 634 RVA: 0x00013A68 File Offset: 0x00011C68
	public void playMovieNativeAdView()
	{
		this.playMovieNativeAdViewNative(this.getAppID());
	}

	// Token: 0x0600027B RID: 635 RVA: 0x00013A76 File Offset: 0x00011C76
	public void playMovieNativeAdViewNative(string appId)
	{
		if (Application.platform == RuntimePlatform.Android)
		{
			this.makeInstance_AdfurikunMovieNativeAdViewController().CallStatic("play", new object[]
			{
				appId
			});
		}
	}

	// Token: 0x0600027C RID: 636 RVA: 0x00013A9B File Offset: 0x00011C9B
	public void hideMovieNativeAdView()
	{
		this.hideMovieNativeAdView(this.getAppID());
	}

	// Token: 0x0600027D RID: 637 RVA: 0x00013AA9 File Offset: 0x00011CA9
	public void hideMovieNativeAdView(string appId)
	{
		if (Application.platform == RuntimePlatform.Android)
		{
			this.makeInstance_AdfurikunMovieNativeAdViewController().CallStatic("hide", new object[]
			{
				appId
			});
		}
	}

	// Token: 0x0600027E RID: 638 RVA: 0x00013ACE File Offset: 0x00011CCE
	public void dispose()
	{
		this.disposeResource();
	}

	// Token: 0x0600027F RID: 639 RVA: 0x00013AD6 File Offset: 0x00011CD6
	public void disposeResource()
	{
		if (Application.platform == RuntimePlatform.Android)
		{
			this.callAndroidMovieNativeAdViewMethod("onDestroy");
		}
	}

	// Token: 0x06000280 RID: 640 RVA: 0x00013AEC File Offset: 0x00011CEC
	[Obsolete("Use Action delegate instead.")]
	public void setMovieNativeAdViewSrcObject(GameObject movieNativeAdViewSrcObject)
	{
		this.setMovieNativeAdViewSrcObject(movieNativeAdViewSrcObject, this.getAppID());
	}

	// Token: 0x06000281 RID: 641 RVA: 0x00013AFB File Offset: 0x00011CFB
	public void setMovieNativeAdViewSrcObject(GameObject movieNativeAdViewSrcObject, string appId)
	{
		this.mMovieNativeAdViewSrcObject = movieNativeAdViewSrcObject;
	}

	// Token: 0x06000282 RID: 642 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
	private void Update()
	{
	}

	// Token: 0x06000283 RID: 643 RVA: 0x00013B04 File Offset: 0x00011D04
	private string getAppID()
	{
		string result = "";
		if (Application.platform == RuntimePlatform.Android)
		{
			result = this.config.androidAppID;
		}
		return result;
	}

	// Token: 0x06000284 RID: 644 RVA: 0x00013B30 File Offset: 0x00011D30
	private float getScreenPositionByGravity(int gravity, float screenSize, float contentSize)
	{
		float result = 0f;
		switch (gravity)
		{
		case 0:
			result = 0f;
			break;
		case 1:
			result = (screenSize - contentSize) / 2f;
			break;
		case 2:
			result = screenSize - contentSize;
			break;
		}
		return result;
	}

	// Token: 0x06000285 RID: 645 RVA: 0x00013B70 File Offset: 0x00011D70
	public void MovieNativeAdViewCallback(string param_str)
	{
		string a = param_str.Split(new char[]
		{
			';'
		})[0].Split(new char[]
		{
			':'
		})[1];
		if (a == "LoadFinish")
		{
			this.MovieNativeAdViewLoadFinish(param_str);
			return;
		}
		if (a == "LoadError")
		{
			this.MovieNativeAdViewLoadError(param_str);
			return;
		}
		if (a == "PlayStart")
		{
			this.MovieNativeAdViewPlayStart(param_str);
			return;
		}
		if (a == "PlayFinish")
		{
			this.MovieNativeAdViewPlayFinish(param_str);
			return;
		}
		if (!(a == "PlayFail"))
		{
			return;
		}
		this.MovieNativeAdViewPlayError(param_str);
	}

	// Token: 0x06000286 RID: 646 RVA: 0x00013C0C File Offset: 0x00011E0C
	private void MovieNativeAdViewLoadFinish(string param_str)
	{
		string text = param_str.Split(new char[]
		{
			';'
		})[1].Split(new char[]
		{
			':'
		})[1];
		string value = "";
		this.onLoadFinish.NullSafe(text);
		AdfurikunMovieNativeAdViewUtility.ADF_MovieStatus adf_MovieStatus = AdfurikunMovieNativeAdViewUtility.ADF_MovieStatus.LoadFinish;
		ArrayList arrayList = new ArrayList();
		arrayList.Add((int)adf_MovieStatus);
		arrayList.Add(text);
		arrayList.Add(value);
		if (this.mMovieNativeAdViewSrcObject != null)
		{
			this.mMovieNativeAdViewSrcObject.SendMessage("MovieNativeAdViewCallback", arrayList);
		}
	}

	// Token: 0x06000287 RID: 647 RVA: 0x00013C98 File Offset: 0x00011E98
	private void MovieNativeAdViewLoadError(string param_str)
	{
		string[] array = param_str.Split(new char[]
		{
			';'
		});
		string text = array[1].Split(new char[]
		{
			':'
		})[1];
		string text2 = "";
		if (array.Length > 2)
		{
			text2 = array[2].Split(new char[]
			{
				':'
			})[1];
		}
		this.onLoadError.NullSafe(text, text2);
		AdfurikunMovieNativeAdViewUtility.ADF_MovieStatus adf_MovieStatus = AdfurikunMovieNativeAdViewUtility.ADF_MovieStatus.LoadError;
		ArrayList arrayList = new ArrayList();
		arrayList.Add((int)adf_MovieStatus);
		arrayList.Add(text);
		arrayList.Add(text2);
		if (this.mMovieNativeAdViewSrcObject != null)
		{
			this.mMovieNativeAdViewSrcObject.SendMessage("MovieNativeAdViewCallback", arrayList);
		}
	}

	// Token: 0x06000288 RID: 648 RVA: 0x00013D48 File Offset: 0x00011F48
	private void MovieNativeAdViewPlayStart(string param_str)
	{
		string arg = param_str.Split(new char[]
		{
			';'
		})[1].Split(new char[]
		{
			':'
		})[1];
		this.onPlayStart.NullSafe(arg);
	}

	// Token: 0x06000289 RID: 649 RVA: 0x00013D88 File Offset: 0x00011F88
	private void MovieNativeAdViewPlayFinish(string param_str)
	{
		string[] array = param_str.Split(new char[]
		{
			';'
		});
		string arg = array[1].Split(new char[]
		{
			':'
		})[1];
		bool arg2 = true;
		if (array.Length > 2 && array[2].Split(new char[]
		{
			':'
		})[1] == "false")
		{
			arg2 = false;
		}
		this.onPlayFinish.NullSafe(arg, arg2);
	}

	// Token: 0x0600028A RID: 650 RVA: 0x00013DF8 File Offset: 0x00011FF8
	private void MovieNativeAdViewPlayError(string param_str)
	{
		string[] array = param_str.Split(new char[]
		{
			';'
		});
		string arg = array[1].Split(new char[]
		{
			':'
		})[1];
		string arg2 = "";
		if (array.Length > 2)
		{
			arg2 = array[2].Split(new char[]
			{
				':'
			})[1];
		}
		this.onPlayError.NullSafe(arg, arg2);
	}

	// Token: 0x0600028B RID: 651 RVA: 0x00013E5C File Offset: 0x0001205C
	private AndroidJavaClass makeInstance_AdfurikunMovieNativeAdViewController()
	{
		return new AndroidJavaClass("jp.tjkapp.adfurikunsdk.moviereward.unityplugin.AdfurikunMovieNativeAdViewController");
	}

	// Token: 0x0600028C RID: 652 RVA: 0x00013E68 File Offset: 0x00012068
	private void callAndroidMovieNativeAdViewMethod(string methodName)
	{
		this.makeInstance_AdfurikunMovieNativeAdViewController().CallStatic(methodName, Array.Empty<object>());
	}

	// Token: 0x0600028D RID: 653 RVA: 0x00013E7B File Offset: 0x0001207B
	public AdfurikunMovieNativeAdViewUtility()
	{
	}

	// Token: 0x0600028E RID: 654 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
	// Note: this type is marked as 'beforefieldinit'.
	static AdfurikunMovieNativeAdViewUtility()
	{
	}

	// Token: 0x040000D6 RID: 214
	public Action<string> onLoadFinish;

	// Token: 0x040000D7 RID: 215
	public Action<string, string> onLoadError;

	// Token: 0x040000D8 RID: 216
	public Action<string> onPlayStart;

	// Token: 0x040000D9 RID: 217
	public Action<string, bool> onPlayFinish;

	// Token: 0x040000DA RID: 218
	public Action<string, string> onPlayError;

	// Token: 0x040000DB RID: 219
	public AdfurikunMovieNativeAdViewAdConfig config;

	// Token: 0x040000DC RID: 220
	private static AdfurikunMovieNativeAdViewUtility mInstance;

	// Token: 0x040000DD RID: 221
	private GameObject mMovieNativeAdViewSrcObject;

	// Token: 0x040000DE RID: 222
	private string unityPluginVersion = "2.20.0";

	// Token: 0x02000134 RID: 308
	public enum ADF_MovieStatus
	{
		// Token: 0x040004C7 RID: 1223
		LoadFinish,
		// Token: 0x040004C8 RID: 1224
		LoadError,
		// Token: 0x040004C9 RID: 1225
		PlayStart,
		// Token: 0x040004CA RID: 1226
		PlayFinish,
		// Token: 0x040004CB RID: 1227
		PlayError
	}
}
