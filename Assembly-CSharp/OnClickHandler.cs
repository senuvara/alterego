﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// Token: 0x02000031 RID: 49
public class OnClickHandler : MonoBehaviour, IPointerDownHandler, IEventSystemHandler, IPointerUpHandler, IPointerExitHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	// Token: 0x06000204 RID: 516 RVA: 0x00011AB4 File Offset: 0x0000FCB4
	private void Start()
	{
		this.IsUI = (base.GetComponentsInChildren<Graphic>().Length != 0);
		if (this.IsUI)
		{
			this.Scroll = base.GetComponentInParent<ScrollRect>();
			return;
		}
		if (base.GetComponent<Rigidbody2D>() == null)
		{
			base.gameObject.AddComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
		}
	}

	// Token: 0x06000205 RID: 517 RVA: 0x00011B05 File Offset: 0x0000FD05
	public void OnMouseDown()
	{
		this.OnPointerDown(null);
	}

	// Token: 0x06000206 RID: 518 RVA: 0x00011B0E File Offset: 0x0000FD0E
	public void OnMouseUp()
	{
		this.OnPointerExit(null);
	}

	// Token: 0x06000207 RID: 519 RVA: 0x00011B17 File Offset: 0x0000FD17
	public void OnMouseUpAsButton()
	{
		this.OnPointerUp(null);
	}

	// Token: 0x06000208 RID: 520 RVA: 0x00011B20 File Offset: 0x0000FD20
	public void OnPointerDown(PointerEventData eventData)
	{
		if (!this.IsEnable())
		{
			this.State = OnClickHandler.STATE.NONE;
			return;
		}
		if (this.EffectOn)
		{
			this.EndRepeat();
		}
		this.EffectOn = true;
		OnClickHandler.EFFECT effect = this.Effect;
		if (effect != OnClickHandler.EFFECT.DOWN)
		{
			if (effect == OnClickHandler.EFFECT.SHRINK)
			{
				base.transform.localScale *= 0.95f;
			}
		}
		else
		{
			base.GetComponent<RectTransform>().anchoredPosition -= new Vector2(0f, 5f);
		}
		this.State = OnClickHandler.STATE.DOWN;
		if (this.RepeatWait >= 0f)
		{
			base.StartCoroutine("HandleClickRepeat");
		}
	}

	// Token: 0x06000209 RID: 521 RVA: 0x00011BC8 File Offset: 0x0000FDC8
	public void OnPointerExit(PointerEventData eventData)
	{
		OnClickHandler.STATE state = this.State;
		if (state - OnClickHandler.STATE.DOWN <= 3)
		{
			this.EndRepeat();
		}
	}

	// Token: 0x0600020A RID: 522 RVA: 0x00011BE8 File Offset: 0x0000FDE8
	public void OnDrag(PointerEventData eventData)
	{
		if (this.State == OnClickHandler.STATE.DRAG)
		{
			this.EndRepeat();
			this.State = OnClickHandler.STATE.SCROLL;
		}
		if (eventData != null && this.State == OnClickHandler.STATE.SCROLL && this.Scroll != null)
		{
			this.Scroll.OnDrag(eventData);
		}
	}

	// Token: 0x0600020B RID: 523 RVA: 0x00011C28 File Offset: 0x0000FE28
	public void OnBeginDrag(PointerEventData eventData)
	{
		OnClickHandler.STATE state = this.State;
		if (state > OnClickHandler.STATE.DOWN)
		{
			if (state != OnClickHandler.STATE.LONGTAP)
			{
				return;
			}
			this.State = OnClickHandler.STATE.LONGDRAG;
		}
		else
		{
			this.State = OnClickHandler.STATE.DRAG;
			if (eventData != null && this.Scroll != null)
			{
				this.Scroll.StopMovement();
				this.Scroll.OnBeginDrag(eventData);
				return;
			}
		}
	}

	// Token: 0x0600020C RID: 524 RVA: 0x00011C7C File Offset: 0x0000FE7C
	public void OnEndDrag(PointerEventData eventData)
	{
		switch (this.State)
		{
		case OnClickHandler.STATE.DRAG:
		case OnClickHandler.STATE.SCROLL:
			if (eventData != null && this.Scroll != null)
			{
				this.Scroll.OnEndDrag(eventData);
				return;
			}
			using (List<GameObject>.Enumerator enumerator = eventData.hovered.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					GameObject gameObject = enumerator.Current;
					if (base.gameObject.name == gameObject.name)
					{
						this.Click();
						this.EndRepeat();
						break;
					}
				}
				return;
			}
			break;
		case OnClickHandler.STATE.LONGTAP:
			return;
		case OnClickHandler.STATE.LONGDRAG:
			break;
		default:
			return;
		}
		this.EndRepeat();
	}

	// Token: 0x0600020D RID: 525 RVA: 0x00011D34 File Offset: 0x0000FF34
	public void OnPointerUp(PointerEventData eventData)
	{
		OnClickHandler.STATE state = this.State;
		if (state == OnClickHandler.STATE.DOWN)
		{
			this.Click();
			this.EndRepeat();
			return;
		}
		if (state != OnClickHandler.STATE.LONGTAP)
		{
			return;
		}
		this.EndRepeat();
	}

	// Token: 0x0600020E RID: 526 RVA: 0x00011D64 File Offset: 0x0000FF64
	public void Click()
	{
		if (this.IsEnable())
		{
			AudioManager.PlaySound(new string[]
			{
				"Click",
				base.transform.parent.name,
				base.gameObject.name
			});
			base.gameObject.SendMessageUpwards("OnClick", base.gameObject, SendMessageOptions.DontRequireReceiver);
		}
	}

	// Token: 0x0600020F RID: 527 RVA: 0x00011DC4 File Offset: 0x0000FFC4
	private void EndRepeat()
	{
		if (this.State == OnClickHandler.STATE.NONE)
		{
			return;
		}
		if (this.EffectOn)
		{
			OnClickHandler.EFFECT effect = this.Effect;
			if (effect != OnClickHandler.EFFECT.DOWN)
			{
				if (effect == OnClickHandler.EFFECT.SHRINK)
				{
					base.transform.localScale /= 0.95f;
				}
			}
			else
			{
				base.GetComponent<RectTransform>().anchoredPosition += new Vector2(0f, 5f);
			}
			this.EffectOn = false;
		}
		base.StopCoroutine("HandleClickRepeat");
		this.State = OnClickHandler.STATE.NONE;
	}

	// Token: 0x06000210 RID: 528 RVA: 0x00011E4E File Offset: 0x0001004E
	private IEnumerator HandleClickRepeat()
	{
		yield return AppUtil.Wait(0.5f);
		OnClickHandler.STATE state = this.State;
		if (state != OnClickHandler.STATE.DOWN)
		{
			if (state == OnClickHandler.STATE.DRAG)
			{
				this.State = OnClickHandler.STATE.LONGDRAG;
			}
		}
		else
		{
			this.State = OnClickHandler.STATE.LONGTAP;
		}
		this.Click();
		while (this.State == OnClickHandler.STATE.LONGTAP || this.State == OnClickHandler.STATE.LONGDRAG)
		{
			yield return AppUtil.Wait(this.RepeatWait);
			this.Click();
		}
		yield break;
	}

	// Token: 0x06000211 RID: 529 RVA: 0x00011E60 File Offset: 0x00010060
	private bool IsEnable()
	{
		if (this.IsUI)
		{
			Button component = base.GetComponent<Button>();
			if (component == null)
			{
				return true;
			}
			if (!component.gameObject.activeInHierarchy || !component.enabled || !component.interactable)
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x06000212 RID: 530 RVA: 0x00011EA8 File Offset: 0x000100A8
	public static void SendEventOperationLog(string eventData, Transform target)
	{
		List<string> list = new List<string>();
		list.Add(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
		list.Add(SceneManager.GetActiveScene().name);
		list.Add(eventData);
		list.Add(target.name);
		list.Add(StringStatusConverter.GetHierarchyPath(target));
		string[] statusStringArray = StringStatusConverter.GetStatusStringArray(null, true, true);
		list.Add(statusStringArray[1]);
		GssDataHelper.PostData("1olHdM1G5zhODy6IbT6ii-Xj2JdEH4oGYyigzAor2zJY", "操作ログ", string.Join("\t", list.ToArray()), "", "");
	}

	// Token: 0x06000213 RID: 531 RVA: 0x00011F40 File Offset: 0x00010140
	public OnClickHandler()
	{
	}

	// Token: 0x040000AD RID: 173
	public float RepeatWait = -1f;

	// Token: 0x040000AE RID: 174
	public OnClickHandler.EFFECT Effect;

	// Token: 0x040000AF RID: 175
	private bool IsUI;

	// Token: 0x040000B0 RID: 176
	private OnClickHandler.STATE State;

	// Token: 0x040000B1 RID: 177
	private ScrollRect Scroll;

	// Token: 0x040000B2 RID: 178
	private bool EffectOn;

	// Token: 0x02000129 RID: 297
	private enum STATE
	{
		// Token: 0x04000498 RID: 1176
		NONE,
		// Token: 0x04000499 RID: 1177
		DOWN,
		// Token: 0x0400049A RID: 1178
		DRAG,
		// Token: 0x0400049B RID: 1179
		LONGTAP,
		// Token: 0x0400049C RID: 1180
		LONGDRAG,
		// Token: 0x0400049D RID: 1181
		SCROLL
	}

	// Token: 0x0200012A RID: 298
	public enum EFFECT
	{
		// Token: 0x0400049F RID: 1183
		NONE,
		// Token: 0x040004A0 RID: 1184
		DOWN,
		// Token: 0x040004A1 RID: 1185
		SHRINK
	}

	// Token: 0x0200012B RID: 299
	[CompilerGenerated]
	private sealed class <HandleClickRepeat>d__20 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x0600098E RID: 2446 RVA: 0x0002EF51 File Offset: 0x0002D151
		[DebuggerHidden]
		public <HandleClickRepeat>d__20(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x0600098F RID: 2447 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000990 RID: 2448 RVA: 0x0002EF60 File Offset: 0x0002D160
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			OnClickHandler onClickHandler = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				this.<>2__current = AppUtil.Wait(0.5f);
				this.<>1__state = 1;
				return true;
			case 1:
			{
				this.<>1__state = -1;
				OnClickHandler.STATE state = onClickHandler.State;
				if (state != OnClickHandler.STATE.DOWN)
				{
					if (state == OnClickHandler.STATE.DRAG)
					{
						onClickHandler.State = OnClickHandler.STATE.LONGDRAG;
					}
				}
				else
				{
					onClickHandler.State = OnClickHandler.STATE.LONGTAP;
				}
				onClickHandler.Click();
				break;
			}
			case 2:
				this.<>1__state = -1;
				onClickHandler.Click();
				break;
			default:
				return false;
			}
			if (onClickHandler.State != OnClickHandler.STATE.LONGTAP && onClickHandler.State != OnClickHandler.STATE.LONGDRAG)
			{
				return false;
			}
			this.<>2__current = AppUtil.Wait(onClickHandler.RepeatWait);
			this.<>1__state = 2;
			return true;
		}

		// Token: 0x17000148 RID: 328
		// (get) Token: 0x06000991 RID: 2449 RVA: 0x0002F019 File Offset: 0x0002D219
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000992 RID: 2450 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000149 RID: 329
		// (get) Token: 0x06000993 RID: 2451 RVA: 0x0002F019 File Offset: 0x0002D219
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040004A2 RID: 1186
		private int <>1__state;

		// Token: 0x040004A3 RID: 1187
		private object <>2__current;

		// Token: 0x040004A4 RID: 1188
		public OnClickHandler <>4__this;
	}
}
