﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using App;

// Token: 0x0200000A RID: 10
public class Counseling
{
	// Token: 0x0600002C RID: 44 RVA: 0x00003B6C File Offset: 0x00001D6C
	public static string[] GetCounselingType(string scenarioType, string data)
	{
		Counseling.ScenarioType = scenarioType;
		Debug.Log("GetCounselingType scenarioType=" + Counseling.ScenarioType);
		if (Counseling.ScenarioType.Contains("診断1"))
		{
			return Counseling.GetCounselingType1(data);
		}
		if (Counseling.ScenarioType.Contains("診断2"))
		{
			return Counseling.GetCounselingType2(data);
		}
		if (Counseling.ScenarioType.Contains("診断3"))
		{
			return Counseling.GetCounselingType3(data);
		}
		if (Counseling.ScenarioType.Contains("診断4"))
		{
			return Counseling.GetCounselingType4(data);
		}
		return null;
	}

	// Token: 0x0600002D RID: 45 RVA: 0x00003BF4 File Offset: 0x00001DF4
	private static string[] GetCounselingType1(string data)
	{
		string[] array = data.Split(new char[]
		{
			','
		});
		int[] array2 = new int[Data.GET_COUNSELING_TYPE(Counseling.ScenarioType).Length];
		for (int i = 0; i < array.Length - 1; i++)
		{
			int[] array3 = Data.GET_COUNSELING_TABLE(Counseling.ScenarioType, i + 1);
			string a = array[i];
			if (!(a == "Yes"))
			{
				if (!(a == "No"))
				{
					if (a == "N/A")
					{
						array2[array3[2] - 1] += 5;
					}
				}
				else
				{
					array2[array3[1] - 1] += 10;
				}
			}
			else
			{
				array2[array3[0] - 1] += 10;
			}
		}
		int num = Array.IndexOf<int>(array2, new List<int>(array2).Max());
		return new string[]
		{
			Data.GET_COUNSELING_TYPE(Counseling.ScenarioType)[num]
		};
	}

	// Token: 0x0600002E RID: 46 RVA: 0x00003CD8 File Offset: 0x00001ED8
	private static string[] GetCounselingType2(string data)
	{
		string text = data.Substring(0, data.Length - 2);
		string str = data.Substring(data.Length - 2, 1);
		string str2 = "N";
		if (text.Length - text.Replace("L", "").Length >= 2)
		{
			str2 = "L";
		}
		else if (text.Length - text.Replace("C", "").Length >= 2)
		{
			str2 = "C";
		}
		return new string[]
		{
			str2 + str
		};
	}

	// Token: 0x0600002F RID: 47 RVA: 0x00003D68 File Offset: 0x00001F68
	private static string[] GetCounselingType4(string data)
	{
		Dictionary<string, int> dictionary = new Dictionary<string, int>();
		List<string> list = new List<string>();
		list.Add("");
		foreach (string text in Data.GET_COUNSELING_TYPE(Counseling.ScenarioType))
		{
			string scenarioKey = Data.GetScenarioKey(Counseling.ScenarioType, "問", text, 0);
			if (scenarioKey != null)
			{
				string[] array2 = LanguageManager.Get(scenarioKey).Split(new char[]
				{
					'、'
				});
				dictionary.Add(text, 0);
				foreach (string text2 in array2)
				{
					int length = text2.Length;
					int length2 = data.Replace(text2, "").Length;
					int num = (data.Length - length2) / length;
					Dictionary<string, int> dictionary2 = dictionary;
					string key = text;
					dictionary2[key] += num;
					if (num > 0)
					{
						list.Add(text2);
					}
				}
			}
		}
		List<KeyValuePair<string, int>> list2 = AppUtil.ReverseByValueS(dictionary);
		List<string> list3 = new List<string>();
		List<string> list4 = new List<string>();
		int value = list2[0].Value;
		int num2 = -1;
		foreach (KeyValuePair<string, int> keyValuePair in list2)
		{
			if (value == 0)
			{
				break;
			}
			if (keyValuePair.Value == value)
			{
				list3.Add(keyValuePair.Key);
			}
			else
			{
				if (num2 < 0)
				{
					num2 = keyValuePair.Value;
				}
				if (num2 == 0)
				{
					break;
				}
				if (keyValuePair.Value != num2)
				{
					break;
				}
				list4.Add(keyValuePair.Key);
			}
		}
		string text3 = null;
		string text4;
		if (list3.Count >= 2)
		{
			string[] array4 = (from x in list3
			orderby Counseling.rnd.Next()
			select x).Take(2).ToArray<string>();
			text4 = array4[0];
			text3 = array4[1];
		}
		else if (list3.Count == 1)
		{
			text4 = list3[0];
			if (list4.Count >= 1)
			{
				text3 = (from x in list4
				orderby Counseling.rnd.Next()
				select x).Take(1).ToArray<string>()[0];
			}
		}
		else
		{
			text4 = "その他";
		}
		if (text4 == "メンヘラ" || text3 == "メンヘラ")
		{
			text4 = "メンヘラ";
			text3 = null;
		}
		List<string> list5 = new List<string>();
		if (text3 == null)
		{
			if (text4 == "その他")
			{
				list5.Add("その他1");
				list5.Add("その他2");
				list5.Add("その他3");
			}
			else
			{
				list5.Add(text4);
			}
		}
		else
		{
			string text5 = text4 + "+" + text3;
			string text6 = text3 + "+" + text4;
			if (Data.GetScenarioKey(Counseling.ScenarioType, "型", text5, 0) != null)
			{
				list5.Add(text5);
			}
			else if (Data.GetScenarioKey(Counseling.ScenarioType, "型", text6, 0) != null)
			{
				list5.Add(text6);
			}
			else
			{
				list5.Add(text4);
				list5.Add(text3);
			}
		}
		string[] array5 = list5.ToArray();
		list[0] = array5[Counseling.rnd.Next(array5.Length)];
		return list.ToArray();
	}

	// Token: 0x06000030 RID: 48 RVA: 0x000040C0 File Offset: 0x000022C0
	private static string[] GetCounselingType3(string data)
	{
		string[] typeString = Counseling.TypeString3;
		int[] array = new int[typeString.Length];
		for (int i = 0; i < typeString.Length; i++)
		{
			array[i] += (data.Length - data.Replace(typeString[i], "").Length) / typeString[i].Length;
		}
		int num = Array.IndexOf<int>(array, new List<int>(array).Max());
		int num2 = Array.LastIndexOf<int>(array, new List<int>(array).Min());
		return new string[]
		{
			typeString[num] + "/" + typeString[num2]
		};
	}

	// Token: 0x06000031 RID: 49 RVA: 0x0000415C File Offset: 0x0000235C
	public static List<List<string>> GetFukidasiKeyList(string scenarioType)
	{
		Counseling.ScenarioType = scenarioType;
		Counseling.TypeString3 = Data.GET_COUNSELING_TYPE(Counseling.ScenarioType);
		List<List<string>> list = new List<List<string>>();
		foreach (string text in Counseling.TypeString3)
		{
			List<string> list2 = new List<string>(new string[]
			{
				text
			});
			string[] collection = LanguageManager.Get(Data.GetScenarioKey(Counseling.ScenarioType, "問", text, 0)).Split(new char[]
			{
				'、'
			});
			list2.AddRange((from x in new List<string>(collection)
			orderby Counseling.rnd.Next()
			select x).ToArray<string>());
			list.Add(list2);
		}
		return list;
	}

	// Token: 0x06000032 RID: 50 RVA: 0x00004220 File Offset: 0x00002420
	public static List<string> GetFukidasiKey(List<List<string>> allKeyList)
	{
		List<string> list = new List<string>();
		for (int i = 0; i < allKeyList.Count; i++)
		{
			int num = 2;
			if (allKeyList.Count == 3 && i != 1)
			{
				num = 3;
			}
			for (int j = 0; j < num; j++)
			{
				list.Add(allKeyList[i][0] + ":" + AppUtil.Pop<string>(allKeyList[i]));
			}
		}
		return new List<string>((from x in list
		orderby Counseling.rnd.Next()
		select x).ToArray<string>());
	}

	// Token: 0x06000033 RID: 51 RVA: 0x000042BC File Offset: 0x000024BC
	public static List<List<string>> CustomFukidasiKeyList(List<List<string>> allKeyList, string data)
	{
		Dictionary<int, int> dictionary = new Dictionary<int, int>();
		for (int i = 0; i < allKeyList.Count; i++)
		{
			dictionary.Add(i, (data.Length - data.Replace(allKeyList[i][0], "").Length) / allKeyList[i][0].Length);
		}
		List<KeyValuePair<int, int>> list = AppUtil.ReverseByValue(dictionary);
		List<List<string>> list2 = new List<List<string>>();
		list2.Add(allKeyList[list[0].Key]);
		list2.Add(allKeyList[list[1].Key]);
		list2.Add(allKeyList[list[3].Key]);
		Counseling.TypeString3 = new string[]
		{
			"",
			"",
			""
		};
		for (int j = 0; j < list2.Count; j++)
		{
			Counseling.TypeString3[j] = list2[j][0];
		}
		return list2;
	}

	// Token: 0x06000034 RID: 52 RVA: 0x000043CE File Offset: 0x000025CE
	public Counseling()
	{
	}

	// Token: 0x06000035 RID: 53 RVA: 0x000043D6 File Offset: 0x000025D6
	// Note: this type is marked as 'beforefieldinit'.
	static Counseling()
	{
	}

	// Token: 0x04000012 RID: 18
	private static Random rnd = new Random();

	// Token: 0x04000013 RID: 19
	private static string[] TypeString3;

	// Token: 0x04000014 RID: 20
	private static string ScenarioType;

	// Token: 0x020000BA RID: 186
	[CompilerGenerated]
	[Serializable]
	private sealed class <>c
	{
		// Token: 0x060007A8 RID: 1960 RVA: 0x00026C2D File Offset: 0x00024E2D
		// Note: this type is marked as 'beforefieldinit'.
		static <>c()
		{
		}

		// Token: 0x060007A9 RID: 1961 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c()
		{
		}

		// Token: 0x060007AA RID: 1962 RVA: 0x00026C39 File Offset: 0x00024E39
		internal int <GetCounselingType4>b__6_0(string x)
		{
			return Counseling.rnd.Next();
		}

		// Token: 0x060007AB RID: 1963 RVA: 0x00026C39 File Offset: 0x00024E39
		internal int <GetCounselingType4>b__6_1(string x)
		{
			return Counseling.rnd.Next();
		}

		// Token: 0x060007AC RID: 1964 RVA: 0x00026C39 File Offset: 0x00024E39
		internal int <GetFukidasiKeyList>b__8_0(string x)
		{
			return Counseling.rnd.Next();
		}

		// Token: 0x060007AD RID: 1965 RVA: 0x00026C39 File Offset: 0x00024E39
		internal int <GetFukidasiKey>b__9_0(string x)
		{
			return Counseling.rnd.Next();
		}

		// Token: 0x040002B2 RID: 690
		public static readonly Counseling.<>c <>9 = new Counseling.<>c();

		// Token: 0x040002B3 RID: 691
		public static Func<string, int> <>9__6_0;

		// Token: 0x040002B4 RID: 692
		public static Func<string, int> <>9__6_1;

		// Token: 0x040002B5 RID: 693
		public static Func<string, int> <>9__8_0;

		// Token: 0x040002B6 RID: 694
		public static Func<string, int> <>9__9_0;
	}
}
