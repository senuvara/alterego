﻿using System;
using System.Collections.Generic;
using App;
using UnityEngine;

// Token: 0x02000027 RID: 39
public class AnalyticsManager
{
	// Token: 0x06000153 RID: 339 RVA: 0x0000E98C File Offset: 0x0000CB8C
	private static string GetDeviceInfo()
	{
		if (!PlayerPrefs.HasKey(AnalyticsManager.DeviceInfoKey))
		{
			List<string> list = new List<string>();
			string item = Guid.NewGuid().ToString().ToUpper();
			list.Add(item);
			list.Add(SystemInfo.operatingSystem);
			list.Add(SystemInfo.deviceModel);
			list.Add(Screen.height + "x" + Screen.width);
			list.Add(BuildInfo.APP_VERSION);
			PlayerPrefs.SetString(AnalyticsManager.DeviceInfoKey, string.Join("\t", list.ToArray()));
			PlayerPrefs.Save();
		}
		return PlayerPrefs.GetString(AnalyticsManager.DeviceInfoKey);
	}

	// Token: 0x06000154 RID: 340 RVA: 0x0000EA3C File Offset: 0x0000CC3C
	public static string GetUserID()
	{
		return AnalyticsManager.GetDeviceInfo().Split(new char[]
		{
			'\t'
		})[0].Replace("/editor", "").Replace("/debug", "");
	}

	// Token: 0x06000155 RID: 341 RVA: 0x0000EA74 File Offset: 0x0000CC74
	public static void SendEvent(string[] eventDataList, string ssid = "1DLWkIkaBFBnrV9VoNi09qwvOhocUADC8BWVHfNzyjfA")
	{
		if (!PlayerPrefs.HasKey(AnalyticsManager.DeviceInfoKey))
		{
			AnalyticsManager.GetDeviceInfo();
		}
		string text = "\t";
		text = text + DateTime.Now.ToString() + "\t";
		text = text + AnalyticsManager.GetDeviceInfo() + "\t";
		text = text + BuildInfo.APP_VERSION + "\t";
		text += string.Join("\t", eventDataList);
		text += "\t";
		string[] statusStringArray = StringStatusConverter.GetStatusStringArray(null, false, true);
		text += statusStringArray[1];
		text = text + SafeAreaAdjuster.GetScreenSize().ToString() + "\t";
		text = text + Screen.safeArea.ToString() + "\t";
		text += SafeAreaAdjuster.SafeAreaHeight;
		GssDataHelper.PostData(ssid, "EGO_005", text, "", "");
	}

	// Token: 0x06000156 RID: 342 RVA: 0x000043CE File Offset: 0x000025CE
	public AnalyticsManager()
	{
	}

	// Token: 0x06000157 RID: 343 RVA: 0x0000EB6B File Offset: 0x0000CD6B
	// Note: this type is marked as 'beforefieldinit'.
	static AnalyticsManager()
	{
	}

	// Token: 0x04000099 RID: 153
	private static readonly string DeviceInfoKey = "DeviceInfo";
}
