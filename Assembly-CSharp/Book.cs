﻿using System;
using System.Runtime.CompilerServices;
using App;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000002 RID: 2
public class Book : MonoBehaviour
{
	// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
	private void Awake()
	{
		this.BookNo = base.gameObject.name.Replace("BookItem", "");
	}

	// Token: 0x06000002 RID: 2 RVA: 0x00002072 File Offset: 0x00000272
	private void OnEnable()
	{
		this.UpdateButton();
	}

	// Token: 0x06000003 RID: 3 RVA: 0x0000207A File Offset: 0x0000027A
	private void FixedUpdate()
	{
		this.UpdateStatus();
	}

	// Token: 0x06000004 RID: 4 RVA: 0x00002084 File Offset: 0x00000284
	public void OnClick(GameObject clickObject)
	{
		global::Debug.Log("Book:OnClick " + clickObject.name);
		if (!clickObject.name.Contains("ReadButton"))
		{
			if (clickObject.name == "AuthorImage")
			{
				DialogManager.ShowDialog("BookDialog", new object[]
				{
					this.BookNo,
					2
				});
			}
			return;
		}
		if (PlayerStatus.EgoPoint < this.BookPrice)
		{
			return;
		}
		PlayerStatus.EgoPoint -= this.BookPrice;
		BookLevel.Add(this.BookNo.ToString());
		int num = BookLevel.Get(this.BookNo);
		if (num == 0)
		{
			DialogManager.ShowDialog("BookDialog", new object[]
			{
				this.BookNo,
				1
			});
		}
		else
		{
			GameObject flipAnimation = base.transform.Find("ReadButton2/FlipPage").gameObject;
			if (!flipAnimation.activeSelf)
			{
				flipAnimation.SetActive(true);
				base.StartCoroutine(AppUtil.WaitAnimation(flipAnimation, delegate()
				{
					flipAnimation.SetActive(false);
				}));
			}
			if (BookEgoPoint.IsRankUp(this.BookNo, num))
			{
				base.transform.Find("BookStatus/ProgressBar").GetComponent<Image>().fillAmount = 1f;
				DialogManager.ShowDialog("RankupDialog", new object[]
				{
					this.BookNo
				});
			}
		}
		this.UpdateButton();
	}

	// Token: 0x06000005 RID: 5 RVA: 0x00002204 File Offset: 0x00000404
	private void UpdateButton()
	{
		if (!base.gameObject.activeSelf)
		{
			return;
		}
		if (this.BookNo == null || this.BookNo == "")
		{
			return;
		}
		int num = BookLevel.Get(this.BookNo);
		int rank = BookLevel.GetRank(this.BookNo);
		EgoPoint bookEgo = BookEgoPoint.GetBookEgo(this.BookNo, "per_second", (num >= 0) ? num : 0);
		base.transform.Find("BookStatus/EgoPerSecond").GetComponent<TextLocalization>().SetText(bookEgo.ToString(LanguageManager.Get("[UI]BookStatus/EgoPerSecond")).Replace(" ", ""));
		base.transform.Find("BookStatus/EgoPerSecond/EgoPerSecondUnit").GetComponent<RectTransform>().anchoredPosition = new Vector2(base.transform.Find("BookStatus/EgoPerSecond").GetComponent<Text>().preferredWidth, 0f);
		if (rank >= 5)
		{
			base.transform.Find("ReadButton1").gameObject.SetActive(false);
			base.transform.Find("ReadButton2").gameObject.SetActive(false);
			base.transform.Find("Complete").gameObject.SetActive(true);
			base.transform.Find("BookStatus/ProgressBar").GetComponent<Image>().fillAmount = 1f;
			this.BookPrice = null;
			return;
		}
		base.transform.Find("ReadButton1").gameObject.SetActive(num == -1);
		base.transform.Find("ReadButton2").gameObject.SetActive(num >= 0);
		base.transform.Find("InActiveFilter").gameObject.SetActive(num == -1);
		float num2 = (float)BookLevel.GetCurrentInRank(this.BookNo);
		float num3 = (float)BookLevel.GetMaxPageInRank(this.BookNo);
		base.transform.Find("BookStatus/ProgressBar").GetComponent<Image>().fillAmount = num2 / num3;
		this.BookPrice = BookEgoPoint.GetBookEgo(this.BookNo, "price", num);
		if (base.transform.Find("ReadButton1").gameObject.activeSelf)
		{
			base.transform.Find("ReadButton1/PriceText").GetComponent<TextLocalization>().SetText(this.BookPrice.ToString(LanguageManager.Get("[UI]ReadButton1/PriceText")));
		}
		if (base.transform.Find("ReadButton2").gameObject.activeSelf)
		{
			base.transform.Find("ReadButton2/ReadCountText").GetComponent<TextLocalization>().SetText((num + 1).ToString(LanguageManager.Get("[UI]ReadButton2/ReadCountText")));
			EgoPoint bookEgo2 = BookEgoPoint.GetBookEgo(this.BookNo, "add_per_second");
			base.transform.Find("ReadButton2/PerSecondText").GetComponent<TextLocalization>().SetText(bookEgo2.ToString(LanguageManager.Get("[UI]ReadButton2/PerSecondText")));
			base.transform.Find("ReadButton2/PriceText").GetComponent<TextLocalization>().SetText(this.BookPrice.ToString(LanguageManager.Get("[UI]ReadButton2/PriceText")));
			for (int i = 1; i <= rank; i++)
			{
				base.transform.Find("ReadButton2/Marker" + i).gameObject.SetActive(true);
			}
		}
		this.UpdateStatus();
	}

	// Token: 0x06000006 RID: 6 RVA: 0x00002550 File Offset: 0x00000750
	private void UpdateStatus()
	{
		if (this.BookPrice == null)
		{
			return;
		}
		bool interactable = PlayerStatus.EgoPoint >= this.BookPrice;
		base.transform.Find("ReadButton1").GetComponent<Button>().interactable = interactable;
		base.transform.Find("ReadButton2").GetComponent<Button>().interactable = interactable;
	}

	// Token: 0x06000007 RID: 7 RVA: 0x000025AD File Offset: 0x000007AD
	public Book()
	{
	}

	// Token: 0x04000001 RID: 1
	[SerializeField]
	private string BookNo;

	// Token: 0x04000002 RID: 2
	private EgoPoint BookPrice;

	// Token: 0x020000B2 RID: 178
	[CompilerGenerated]
	private sealed class <>c__DisplayClass5_0
	{
		// Token: 0x0600078E RID: 1934 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass5_0()
		{
		}

		// Token: 0x0600078F RID: 1935 RVA: 0x00026654 File Offset: 0x00024854
		internal void <OnClick>b__0()
		{
			this.flipAnimation.SetActive(false);
		}

		// Token: 0x04000299 RID: 665
		public GameObject flipAnimation;
	}
}
