﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000016 RID: 22
public class PrizeView : MonoBehaviour
{
	// Token: 0x06000096 RID: 150 RVA: 0x000087FD File Offset: 0x000069FD
	private void OnEnable()
	{
		this.ItemNum = int.Parse(base.gameObject.name.Replace("PrizeItem", ""));
		this.Prize = new PrizeItem(this.ItemNum);
		this.SetItem();
	}

	// Token: 0x06000097 RID: 151 RVA: 0x0000883C File Offset: 0x00006A3C
	private void OnClick(GameObject clickObject)
	{
		global::Debug.Log(base.gameObject.name + ":OnClick " + clickObject.name);
		string name = clickObject.name;
		if (name == "GetPrizeButton")
		{
			DialogManager.ShowDialog("GetPrizeDialog", new object[]
			{
				this.Prize.GetGift(),
				this.Prize.GetGiftLocalizeText(),
				this.Icon
			});
			this.Prize.AddLevel();
			PrizeView[] componentsInChildren = base.transform.parent.parent.GetComponentsInChildren<PrizeView>();
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				componentsInChildren[i].SetItem();
			}
		}
	}

	// Token: 0x06000098 RID: 152 RVA: 0x000088EC File Offset: 0x00006AEC
	public void SetItem()
	{
		int itemNum = this.ItemNum;
		Image component = base.transform.Find("PrizeDetail/Icon").GetComponent<Image>();
		component.sprite = this.Icon;
		component.SetNativeSize();
		string text = string.Format(LanguageManager.Get("[Prize]Condition" + itemNum), this.Prize.GetNextCondition() + this.Prize.GetUnit());
		text = text.Replace("広", "<size=27>広</size>");
		base.transform.Find("PrizeDetail/TitleText").GetComponent<TextLocalization>().SetText(text);
		base.transform.Find("PrizeDetail/EffectText").GetComponent<TextLocalization>().SetText(LanguageManager.Get("[UI]PrizeDetail/EffectText") + "\n" + this.Prize.GetGiftLocalizeText());
		float currentValueRate = this.Prize.GetCurrentValueRate();
		bool flag = this.Prize.IsEnableButton();
		base.transform.Find("PrizeDetail/GetPrizeButton").GetComponent<Button>().interactable = flag;
		base.transform.Find("PrizeDetail/GetPrizeButton/Notice").gameObject.SetActive(flag);
		base.transform.Find("PrizeDetail/Bar/BarCurrent").GetComponent<Image>().fillAmount = currentValueRate;
		string currentValue = this.Prize.GetCurrentValue();
		string nextCondition = this.Prize.GetNextCondition();
		base.transform.Find("PrizeDetail/Bar/BarText").GetComponent<TextLocalization>().SetText(string.Concat(new string[]
		{
			currentValue,
			this.Prize.GetUnit(),
			"/",
			nextCondition,
			this.Prize.GetUnit()
		}));
		if (this.Prize.IsMax)
		{
			base.transform.Find("PrizeDetail/GetPrizeButton").GetComponentInChildren<TextLocalization>().SetKey("[UI]GetPrizeButton/TextMax");
			base.transform.Find("PrizeDetail/EffectText").gameObject.SetActive(false);
		}
	}

	// Token: 0x06000099 RID: 153 RVA: 0x000025AD File Offset: 0x000007AD
	public PrizeView()
	{
	}

	// Token: 0x04000027 RID: 39
	[SerializeField]
	private Sprite Icon;

	// Token: 0x04000028 RID: 40
	private int ItemNum;

	// Token: 0x04000029 RID: 41
	private PrizeItem Prize;
}
