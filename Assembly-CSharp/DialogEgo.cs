﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text;
using App;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

// Token: 0x0200000B RID: 11
public class DialogEgo : Dialog
{
	// Token: 0x06000036 RID: 54 RVA: 0x000043E4 File Offset: 0x000025E4
	protected override bool OnClick(GameObject clickObject)
	{
		global::Debug.Log("DialogEgo:OnClick " + clickObject.name);
		if (base.OnClick(clickObject))
		{
			return true;
		}
		string name = clickObject.name;
		uint num = <PrivateImplementationDetails>.ComputeStringHash(name);
		if (num > 2795540305U)
		{
			if (num <= 3669600488U)
			{
				if (num <= 3171997475U)
				{
					if (num <= 2829095543U)
					{
						if (num != 2825569960U)
						{
							if (num != 2829095543U)
							{
								return true;
							}
							if (!(name == "Bookmark4"))
							{
								return true;
							}
							goto IL_94A;
						}
						else
						{
							if (!(name == "AppButton"))
							{
								return true;
							}
							Application.OpenURL("http://caracolu.com/app/escape/");
							return true;
						}
					}
					else if (num != 2933263904U)
					{
						if (num != 3013031537U)
						{
							if (num != 3171997475U)
							{
								return true;
							}
							if (!(name == "NoPlayButton1"))
							{
								return true;
							}
							TimeManager.Reset(TimeManager.TYPE.NEXT_BONUS_BOOK, TimeSpan.FromMinutes(15.0));
							this.Close();
							return true;
						}
						else
						{
							if (!(name == "ReadButton"))
							{
								return true;
							}
							goto IL_65F;
						}
					}
					else
					{
						if (!(name == "OpenSaveDataButton"))
						{
							return true;
						}
						base.StartCoroutine(this.Save());
						return true;
					}
				}
				else if (num <= 3174415438U)
				{
					if (num != 3173878329U)
					{
						if (num != 3174415438U)
						{
							return true;
						}
						if (!(name == "DailyMovieButton"))
						{
							return true;
						}
						AdManager.Show("Reward", delegate(bool reward)
						{
							if (reward)
							{
								DialogManager.ShowDialog("GetDailyBonusDialog", new object[]
								{
									this.args[0]
								});
							}
						});
						this.Close();
						return true;
					}
					else
					{
						if (!(name == "GameStartButton"))
						{
							return true;
						}
						SceneTransition.LoadScene("探求", null, 0.5f);
						return true;
					}
				}
				else if (num != 3188775094U)
				{
					if (num != 3297657084U)
					{
						if (num != 3669600488U)
						{
							return true;
						}
						if (!(name == "ShopItemButton2"))
						{
							return true;
						}
					}
					else
					{
						if (!(name == "EsBonusButton"))
						{
							return true;
						}
						if (AdInfo.ENABLE)
						{
							AdManager.Show("Reward", delegate(bool reward)
							{
								this.RewardForBonus("Temp", reward, true);
							});
						}
						else
						{
							this.RewardForBonus("Temp", true, false);
						}
						this.Close();
						return true;
					}
				}
				else
				{
					if (!(name == "NoPlayButton2"))
					{
						return true;
					}
					this.Close();
					return true;
				}
			}
			else if (num <= 3770266202U)
			{
				if (num <= 3719933345U)
				{
					if (num != 3686378107U)
					{
						if (num != 3719933345U)
						{
							return true;
						}
						if (!(name == "ShopItemButton1"))
						{
							return true;
						}
					}
					else if (!(name == "ShopItemButton3"))
					{
						return true;
					}
				}
				else if (num != 3736710964U)
				{
					if (num != 3747971189U)
					{
						if (num != 3770266202U)
						{
							return true;
						}
						if (!(name == "ShopItemButton4"))
						{
							return true;
						}
					}
					else
					{
						if (!(name == "Shop2Button"))
						{
							return true;
						}
						DialogManager.ShowDialog("ShopDialog", new object[]
						{
							2
						});
						return true;
					}
				}
				else if (!(name == "ShopItemButton6"))
				{
					return true;
				}
			}
			else if (num <= 3853923206U)
			{
				if (num != 3787043821U)
				{
					if (num != 3795929794U)
					{
						if (num != 3853923206U)
						{
							return true;
						}
						if (!(name == "RejectButton"))
						{
							return true;
						}
						goto IL_65F;
					}
					else
					{
						if (!(name == "ReferenceButton"))
						{
							return true;
						}
						Application.OpenURL("http://alterego.caracolu.com/references");
						return true;
					}
				}
				else if (!(name == "ShopItemButton5"))
				{
					return true;
				}
			}
			else if (num != 3886701167U)
			{
				if (num != 4006744183U)
				{
					if (num != 4106898081U)
					{
						return true;
					}
					if (!(name == "CreditsButton"))
					{
						return true;
					}
					DialogManager.ShowDialog("CreditsDialog", Array.Empty<object>());
					return true;
				}
				else
				{
					if (!(name == "IDText"))
					{
						return true;
					}
					AppUtil.SetClipBoard(Settings.SaveDataID);
					DialogManager.ShowDialog("CopyToast", Array.Empty<object>());
					return true;
				}
			}
			else
			{
				if (!(name == "BookBonusButton"))
				{
					return true;
				}
				AdManager.Show("Reward", delegate(bool reward)
				{
					this.RewardForBonus("Time", reward, false);
				});
				this.Close();
				return true;
			}
			string str = clickObject.name.Replace("ShopItemButton", "");
			base.StartCoroutine(InAppPurchaser.BuyProductID("item" + str, new Action(this.CallbackFromPuchaser)));
			return true;
		}
		if (num <= 1412828986U)
		{
			if (num <= 543985802U)
			{
				if (num <= 290868480U)
				{
					if (num != 257794567U)
					{
						if (num != 290868480U)
						{
							return true;
						}
						if (!(name == "GetEgoButton"))
						{
							return true;
						}
						string text = base.transform.Find("EgoBanner/EgoText").GetComponent<Text>().text;
						PlayerStatus.EgoPoint += new EgoPoint(text);
						this.Close();
						PlayerStatus.EnableDailyBonus = false;
						return true;
					}
					else
					{
						if (!(name == "BookPageRight"))
						{
							return true;
						}
						this.args[1] = (int)this.args[1] - 1;
						this.OnShown(Array.Empty<object>());
						return true;
					}
				}
				else if (num != 456477288U)
				{
					if (num != 507760969U)
					{
						if (num != 543985802U)
						{
							return true;
						}
						if (!(name == "SkipButton"))
						{
							return true;
						}
					}
					else
					{
						if (!(name == "StartChapterEndButton"))
						{
							return true;
						}
						base.transform.Find("Icon1").gameObject.SetActive(false);
						base.transform.Find("Icon2").gameObject.SetActive(true);
						base.GetComponent<CanvasGroup>().blocksRaycasts = false;
						AppUtil.DelayAction(this, 0.5f, delegate()
						{
							this.Close();
						}, true);
						return true;
					}
				}
				else
				{
					if (!(name == "RestartButton"))
					{
						return true;
					}
					base.transform.Find("Icon1").gameObject.SetActive(false);
					base.transform.Find("Icon2").gameObject.SetActive(true);
					base.GetComponent<CanvasGroup>().blocksRaycasts = false;
					AppUtil.DelayAction(this, 0.5f, delegate()
					{
						Data.Restart();
					}, true);
					return true;
				}
			}
			else if (num <= 1116130609U)
			{
				if (num != 648655509U)
				{
					if (num != 1116130609U)
					{
						return true;
					}
					if (!(name == "OpenLoadDataButton"))
					{
						return true;
					}
					DialogManager.ShowDialog("LoadDataDialog", Array.Empty<object>());
					return true;
				}
				else
				{
					if (!(name == "ReviewButton"))
					{
						return true;
					}
					AppUtil.OpenReviewURL();
					if (base.gameObject.name == "ReviewDialog")
					{
						this.Close();
						return true;
					}
					return true;
				}
			}
			else if (num != 1263210103U)
			{
				if (num != 1296674175U)
				{
					if (num != 1412828986U)
					{
						return true;
					}
					if (!(name == "BookPageLeft"))
					{
						return true;
					}
					this.args[1] = (int)this.args[1] + 1;
					this.OnShown(Array.Empty<object>());
					return true;
				}
				else
				{
					if (!(name == "TutorialButton"))
					{
						return true;
					}
					DialogManager.ShowDialog("TutorialDialog", Array.Empty<object>());
					return true;
				}
			}
			else
			{
				if (!(name == "AppShareButton"))
				{
					return true;
				}
				clickObject.GetComponent<Button>().interactable = false;
				base.StartCoroutine(AppUtil.DelayAction(1f, delegate()
				{
					clickObject.GetComponent<Button>().interactable = true;
				}, true));
				StringBuilder stringBuilder = new StringBuilder();
				name = base.gameObject.name;
				if (name == "BookDialog")
				{
					string str2 = (string)this.args[0];
					if (GameObject.Find("BookPageDialog") != null)
					{
						string text2 = GameObject.Find("BookPageDialog").transform.Find("FukidasiText").GetComponent<VerticalText>().text;
						stringBuilder.Append("「").Append(text2.Replace("\n", "")).Append("」\n");
					}
					else
					{
						string value = LanguageManager.Get("[Book]Description" + str2).Split(new char[]
						{
							'\n'
						})[0];
						stringBuilder.Append(value).Append("\n");
					}
					string value2 = LanguageManager.Get("[Book]Author" + str2);
					string value3 = LanguageManager.Get("[Book]Title" + str2);
					stringBuilder.Append(value2).Append("『").Append(value3).Append("』");
				}
				stringBuilder.Append("\n").Append("自分探しタップゲーム『ALTER EGO』 caracolu.com/app/alterego/ #ALTEREGO");
				AppUtil.ShareLocalImage(stringBuilder.ToString(), null, "SNSシェア画像.jpg");
				return true;
			}
		}
		else if (num <= 1921566945U)
		{
			if (num <= 1575583836U)
			{
				if (num != 1474678599U)
				{
					if (num != 1575583836U)
					{
						return true;
					}
					if (!(name == "CounselingButton"))
					{
						return true;
					}
				}
				else if (!(name == "GetButton"))
				{
					return true;
				}
			}
			else if (num != 1741692803U)
			{
				if (num != 1752725837U)
				{
					if (num != 1921566945U)
					{
						return true;
					}
					if (!(name == "RetryButtonFromDialog"))
					{
						return true;
					}
				}
				else
				{
					if (!(name == "LoadDataButton"))
					{
						return true;
					}
					base.StartCoroutine(this.Load());
					return true;
				}
			}
			else
			{
				if (!(name == "BookCover"))
				{
					return true;
				}
				DialogManager.ShowDialog("BookPageDialog", new object[]
				{
					this.args[0],
					1
				});
				return true;
			}
		}
		else
		{
			if (num <= 2738712957U)
			{
				if (num != 2328861692U)
				{
					if (num != 2587041957U)
					{
						if (num != 2738712957U)
						{
							return true;
						}
						if (!(name == "ShopButton"))
						{
							return true;
						}
					}
					else
					{
						if (!(name == "RestoreButton"))
						{
							return true;
						}
						base.StartCoroutine(InAppPurchaser.RestorePurchases(new Action(this.CallbackFromPuchaser)));
						return true;
					}
				}
				else if (!(name == "Shop1Button"))
				{
					return true;
				}
				DialogManager.ShowDialog("ShopDialog", new object[]
				{
					1
				});
				return true;
			}
			if (num != 2745207448U)
			{
				if (num != 2778762686U)
				{
					if (num != 2795540305U)
					{
						return true;
					}
					if (!(name == "Bookmark2"))
					{
						return true;
					}
					goto IL_94A;
				}
				else
				{
					if (!(name == "Bookmark3"))
					{
						return true;
					}
					goto IL_94A;
				}
			}
			else
			{
				if (!(name == "Bookmark1"))
				{
					return true;
				}
				goto IL_94A;
			}
		}
		IL_65F:
		this.Close();
		return true;
		IL_94A:
		this.args[1] = int.Parse(clickObject.name.Replace("Bookmark", ""));
		this.OnShown(Array.Empty<object>());
		return true;
	}

	// Token: 0x06000037 RID: 55 RVA: 0x00004FBC File Offset: 0x000031BC
	public void RewardForBonus(string type, bool reward, bool dialog)
	{
		if (reward)
		{
			PlayerResult.AdMovieCount++;
			if (type == "Time")
			{
				BonusTime.SetBonus();
				TimeManager.Reset(TimeManager.TYPE.NEXT_BONUS_BOOK, TimeSpan.FromMinutes(15.0));
				return;
			}
			if (type == "Temp")
			{
				EgoPoint egoPoint = new EgoPoint(base.transform.Find("EgoBanner/EgoText").GetComponent<Text>().text);
				TimeManager.Reset(TimeManager.TYPE.NEXT_BONUS_ES, TimeSpan.FromMinutes(15.0));
				if (dialog)
				{
					DialogManager.ShowDialog("GetEgoDialog", new object[]
					{
						egoPoint
					});
				}
				PlayerStatus.EgoPoint += egoPoint;
				return;
			}
			if (!(type == "DailyBonus"))
			{
				return;
			}
			EgoPoint egoPoint2 = new EgoPoint(base.transform.Find("EgoBanner/EgoText").GetComponent<Text>().text);
			PlayerStatus.EgoPoint += egoPoint2;
			PlayerStatus.EnableDailyBonus = false;
			global::Debug.Log("RewardForBonus " + egoPoint2.ToString());
		}
	}

	// Token: 0x06000038 RID: 56 RVA: 0x000050C4 File Offset: 0x000032C4
	private void CallbackFromPuchaser()
	{
		EventSystem.current.SetSelectedGameObject(null, null);
		if (base.gameObject.activeSelf)
		{
			this.OnShown(Array.Empty<object>());
		}
	}

	// Token: 0x06000039 RID: 57 RVA: 0x000050EC File Offset: 0x000032EC
	protected override void Render()
	{
		string name = base.gameObject.name;
		uint num = <PrivateImplementationDetails>.ComputeStringHash(name);
		if (num <= 1627802666U)
		{
			if (num <= 1138431753U)
			{
				if (num != 410271955U)
				{
					if (num != 1138431753U)
					{
						goto IL_46E;
					}
					if (!(name == "CreditsDialog"))
					{
						goto IL_46E;
					}
				}
				else
				{
					if (!(name == "CopyToast"))
					{
						goto IL_46E;
					}
					base.StartCoroutine(AppUtil.FadeIn(base.gameObject, 0.25f, null));
					AppUtil.DelayAction(this, 1f, delegate()
					{
						this.Close();
					}, true);
					goto IL_486;
				}
			}
			else
			{
				if (num != 1212909418U)
				{
					if (num != 1579043927U)
					{
						if (num != 1627802666U)
						{
							goto IL_46E;
						}
						if (!(name == "EndingScreenSE"))
						{
							goto IL_46E;
						}
					}
					else if (!(name == "EndingScreenID"))
					{
						goto IL_46E;
					}
					base.StartCoroutine(AppUtil.FadeIn(base.gameObject, 3f, null));
					base.GetComponentInParent<DialogManager>().transform.Find("FilterGroup").GetComponentInChildren<Image>().enabled = false;
					goto IL_486;
				}
				if (!(name == "MenuDialog"))
				{
					goto IL_46E;
				}
				base.StartCoroutine(AppUtil.FadeIn(base.gameObject, 0.25f, null));
				this.SlideDialog(true);
				goto IL_486;
			}
		}
		else if (num <= 1873997423U)
		{
			if (num != 1862661981U)
			{
				if (num != 1873997423U)
				{
					goto IL_46E;
				}
				if (!(name == "GetDailyBonusDialog"))
				{
					goto IL_46E;
				}
				base.StartCoroutine(AppUtil.FadeIn(base.gameObject, 0.25f, null));
				int intParameter = Utility.GetIntParameter("日替わり動画ボーナス");
				base.transform.Find("EgoBanner/EgoText").GetComponent<Text>().text = ((EgoPoint)this.args[0] * (float)intParameter).ToString();
				this.RewardForBonus("DailyBonus", true, false);
				goto IL_486;
			}
			else if (!(name == "ShopDialog"))
			{
				goto IL_46E;
			}
		}
		else
		{
			if (num != 2586654130U)
			{
				if (num != 3598380987U)
				{
					if (num != 3970019489U)
					{
						goto IL_46E;
					}
					if (!(name == "GetPrizeDialog"))
					{
						goto IL_46E;
					}
				}
				else
				{
					if (!(name == "TutorialDialog"))
					{
						goto IL_46E;
					}
					goto IL_19A;
				}
			}
			else if (!(name == "RankupDialog"))
			{
				goto IL_46E;
			}
			base.StartCoroutine(AppUtil.FadeIn(base.gameObject, 0.25f, null));
			AppUtil.SetAlpha(base.transform.Find("AdFrame"), 0f);
			base.transform.Find("CloseButton").gameObject.SetActive(false);
			if (AdInfo.ENABLE)
			{
				AppUtil.DelayAction(this, 1.25f, AppUtil.FadeIn(base.transform.Find("AdFrame"), 0.25f, null), true);
				AppUtil.DelayAction(this, 1.25f, delegate()
				{
					AdManager.Show("Rectangle", null);
				}, true);
				AppUtil.DelayAction(this, 2f, delegate()
				{
					base.transform.Find("CloseButton").gameObject.SetActive(true);
					base.StartCoroutine(AppUtil.FadeIn(base.transform.Find("CloseButton"), 0.25f, null));
				}, true);
				goto IL_486;
			}
			base.transform.Find("AdFrame").gameObject.SetActive(false);
			string text = "RankupLabel";
			if (base.gameObject.name == "GetPrizeDialog")
			{
				text = "PrizeLabel";
			}
			base.transform.Find(text).GetComponent<RectTransform>().anchoredPosition = base.transform.Find(text + "PosNoAd").GetComponent<RectTransform>().anchoredPosition;
			base.transform.Find("CloseButton").GetComponent<RectTransform>().anchoredPosition = base.transform.Find("CloseButtonPosNoAd").GetComponent<RectTransform>().anchoredPosition;
			AppUtil.DelayAction(this, 1.5f, delegate()
			{
				base.transform.Find("CloseButton").gameObject.SetActive(true);
				base.StartCoroutine(AppUtil.FadeIn(base.transform.Find("CloseButton"), 0.25f, null));
			}, true);
			goto IL_486;
		}
		IL_19A:
		base.StartCoroutine(AppUtil.FadeIn(base.gameObject, 0.25f, null));
		if (GameObject.Find("MenuDialog") != null)
		{
			base.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, base.GetComponent<RectTransform>().anchoredPosition.y);
			base.StartCoroutine(this.ShowMenuPage(GameObject.Find("MenuDialog")));
			goto IL_486;
		}
		this.SlideDialog(true);
		goto IL_486;
		IL_46E:
		base.StartCoroutine(AppUtil.FadeIn(base.gameObject, 0.25f, null));
		IL_486:
		if (base.gameObject.name == "ShopDialog")
		{
			base.StartCoroutine(AppUtil.FadeIn(base.transform.Find("MainPage/Shop1/Shop1Button"), 0.25f, null));
			base.StartCoroutine(AppUtil.FadeIn(base.transform.Find("MainPage/Shop1/Shop2Button"), 0.25f, null));
			base.StartCoroutine(AppUtil.FadeIn(base.transform.Find("MainPage/Shop2/Shop1Button"), 0.25f, null));
			base.StartCoroutine(AppUtil.FadeIn(base.transform.Find("MainPage/Shop2/Shop2Button"), 0.25f, null));
		}
	}

	// Token: 0x0600003A RID: 58 RVA: 0x00005621 File Offset: 0x00003821
	protected override void OnDisable()
	{
		base.OnDisable();
		base.CancelInvoke();
	}

	// Token: 0x0600003B RID: 59 RVA: 0x00005630 File Offset: 0x00003830
	private void SlideDialog(bool show)
	{
		Transform transform = base.transform.Find("Background");
		foreach (string str in new string[]
		{
			"MainPage",
			"MainPage/Shop1",
			"MainPage/Shop2"
		})
		{
			if (transform != null)
			{
				break;
			}
			transform = base.transform.Find(str + "/Background");
		}
		float x = transform.GetComponent<RectTransform>().sizeDelta.x;
		float y = base.GetComponent<RectTransform>().anchoredPosition.y;
		AppUtil.SetAlpha(transform.parent, 1f);
		if (show)
		{
			base.StartCoroutine(AppUtil.MoveEasingFloat(x, 0f, delegate(float tmp)
			{
				this.GetComponent<RectTransform>().anchoredPosition = new Vector2(tmp, y);
			}, true, 0.4f, EasingFunction.Ease.EaseOutQuint, null));
			return;
		}
		base.StartCoroutine(AppUtil.MoveEasingFloat(0f, x, delegate(float tmp)
		{
			this.GetComponent<RectTransform>().anchoredPosition = new Vector2(tmp, y);
		}, true, 0.3f, EasingFunction.Ease.EaseOutQuint, null));
	}

	// Token: 0x0600003C RID: 60 RVA: 0x0000573D File Offset: 0x0000393D
	private IEnumerator CloseMenuPage(GameObject menu)
	{
		menu.transform.SetAsLastSibling();
		AppUtil.SetAlpha(menu.transform.Find("InnerPage2"), 0f);
		yield return null;
		AppUtil.SetAlpha(menu.transform.Find("InnerPage2"), this.WAIT_PAGE_ALPHA);
		yield return AppUtil.WaitRealtime(this.WAIT_PAGE_TIME);
		AppUtil.SetAlpha(menu.transform.Find("InnerPage2"), 0f);
		AppUtil.SetAlpha(menu.transform.Find("InnerPage3"), this.WAIT_PAGE_ALPHA);
		yield return AppUtil.WaitRealtime(this.WAIT_PAGE_TIME);
		AppUtil.SetAlpha(menu.transform.Find("InnerPage3"), 0f);
		menu.transform.Find("MainPage").gameObject.SetActive(true);
		yield break;
	}

	// Token: 0x0600003D RID: 61 RVA: 0x00005753 File Offset: 0x00003953
	private IEnumerator ShowMenuPage(GameObject menu)
	{
		menu.transform.SetAsLastSibling();
		yield return null;
		menu.transform.Find("MainPage").gameObject.SetActive(false);
		AppUtil.SetAlpha(menu.transform.Find("InnerPage3"), this.WAIT_PAGE_ALPHA);
		yield return AppUtil.WaitRealtime(this.WAIT_PAGE_TIME);
		AppUtil.SetAlpha(menu.transform.Find("InnerPage3"), 0f);
		AppUtil.SetAlpha(menu.transform.Find("InnerPage2"), 1f);
		yield return AppUtil.WaitRealtime(this.WAIT_PAGE_TIME);
		AppUtil.SetAlpha(menu.transform.Find("InnerPage2"), 0f);
		yield break;
	}

	// Token: 0x0600003E RID: 62 RVA: 0x0000576C File Offset: 0x0000396C
	public override void OnShown(params object[] args)
	{
		base.OnShown(args);
		Time.timeScale = Settings.GAME_SPEED;
		GameObject[] array = GameObject.FindGameObjectsWithTag("Beta");
		for (int i = 0; i < array.Length; i++)
		{
			array[i].SetActive(false);
		}
		string name = base.gameObject.name;
		uint num = <PrivateImplementationDetails>.ComputeStringHash(name);
		if (num <= 1629034328U)
		{
			if (num <= 1240870616U)
			{
				if (num <= 406125217U)
				{
					if (num != 179114240U)
					{
						if (num != 406125217U)
						{
							return;
						}
						if (!(name == "RestartDialogID"))
						{
							return;
						}
						goto IL_ACE;
					}
					else
					{
						if (!(name == "SaveDataDialog"))
						{
							return;
						}
						string saveDataID = Settings.SaveDataID;
						string text = "";
						string text2 = "";
						if (!string.IsNullOrEmpty(saveDataID))
						{
							DateTime dateTime = SaveDataManager.SaveDataTime.ToLocalTime();
							CultureInfo provider = CultureInfo.CreateSpecificCulture("ja");
							text = dateTime.ToString(provider);
							text2 = dateTime.AddDays(30.0).ToString(provider);
						}
						base.transform.Find("SaveData/IDText").GetComponent<Text>().text = saveDataID;
						base.transform.Find("SaveData/DateText").GetComponent<TextLocalization>().SetParameters(new object[]
						{
							text,
							text2
						});
						return;
					}
				}
				else if (num != 701148747U)
				{
					if (num != 1212909418U)
					{
						if (num != 1240870616U)
						{
							return;
						}
						if (!(name == "BookDialog"))
						{
							return;
						}
						bool is1st = (int)this.args[1] == 1;
						string text3 = (string)this.args[0];
						bool flag = text3 == "11";
						base.transform.Find("TitleText").gameObject.SetActive(!flag);
						base.transform.Find("TitleText1").gameObject.SetActive(flag);
						base.transform.Find("TitleText2").gameObject.SetActive(flag);
						base.transform.Find("AuthorText").gameObject.SetActive(!flag);
						base.transform.Find("AuthorText2").gameObject.SetActive(flag);
						base.transform.Find("TitleText").GetComponent<Text>().text = LanguageManager.Get("[Book]Title" + text3);
						base.transform.Find("AuthorText").GetComponent<Text>().text = LanguageManager.Get("[Book]Author" + text3);
						if (flag)
						{
							base.transform.Find("TitleText1").GetComponent<Text>().text = "フランケン";
							base.transform.Find("TitleText2").GetComponent<Text>().text = "シュタイン";
							base.transform.Find("AuthorText2").GetComponent<Text>().text = LanguageManager.Get("[Book]Author" + text3);
						}
						string text4 = LanguageManager.Get("[Book]Description" + text3);
						string text5 = text4.Split(new char[]
						{
							'\n'
						})[0];
						base.transform.Find("DescriptionTextHead").GetComponent<Text>().text = text5;
						base.transform.Find("DescriptionText").GetComponent<Text>().text = text4.Replace(text5 + "\n\n", "");
						base.transform.Find("BookCover").GetComponent<Button>().enabled = !is1st;
						base.transform.Find("ButterflyParticle").gameObject.SetActive(false);
						if (is1st)
						{
							base.StartCoroutine(AppUtil.DelayAction(0.2f, delegate()
							{
								this.transform.Find("ButterflyParticle").gameObject.SetActive(is1st);
							}, true));
						}
						base.transform.Find("CloseAllButton").gameObject.SetActive(!is1st);
						base.transform.Find("ReadButton").gameObject.SetActive(is1st);
						return;
					}
					else
					{
						if (!(name == "MenuDialog"))
						{
							return;
						}
						base.transform.Find("MainPage/SwitchBGM").GetComponent<Toggle>().isOn = Settings.BGM;
						base.transform.Find("MainPage/SwitchSE").GetComponent<Toggle>().isOn = Settings.SE;
						Text component = base.transform.Find("MainPage/ReferenceButton/TitleText").GetComponent<Text>();
						component.text = string.Concat(new object[]
						{
							"<size=",
							(float)component.fontSize * 1.2f,
							">",
							component.text.Substring(0, 1),
							"</size>",
							component.text.Substring(1, component.text.Length - 1)
						});
						return;
					}
				}
				else
				{
					if (!(name == "ChapterEndDialog"))
					{
						return;
					}
					goto IL_ACE;
				}
			}
			else
			{
				if (num > 1578922526U)
				{
					if (num != 1579043927U)
					{
						if (num != 1627802666U)
						{
							if (num != 1629034328U)
							{
								return;
							}
							if (!(name == "ShareDialog"))
							{
								return;
							}
							bool flag2 = Utility.EsExists();
							if (PlayerStatus.ScenarioNo.Contains("4章ID"))
							{
								flag2 = false;
							}
							base.transform.Find("Es").gameObject.SetActive(flag2);
							base.transform.Find("Background").GetComponent<Image>().fillAmount = (flag2 ? 1f : 0.8f);
							Vector2 anchoredPosition = base.transform.Find("ShareButton").GetComponent<RectTransform>().anchoredPosition;
							anchoredPosition.y = -454f;
							if (flag2)
							{
								anchoredPosition.y -= 200f;
								int num2 = Random.Range(1, 11);
								base.transform.Find("Es/Fukidasi/Text").GetComponent<TextLocalization>().SetKey("[EsTalk]SNSシェア-" + num2);
							}
							base.transform.Find("ShareButton").GetComponent<RectTransform>().anchoredPosition = anchoredPosition;
							return;
						}
						else if (!(name == "EndingScreenSE"))
						{
							return;
						}
					}
					else if (!(name == "EndingScreenID"))
					{
						return;
					}
					AppUtil.DelayAction(this, 5f, delegate()
					{
						base.transform.Find("Mirror1").gameObject.SetActive(true);
						base.StartCoroutine(AppUtil.FadeIn(base.transform.Find("Mirror1"), 0.5f, null));
					}, true);
					AppUtil.DelayAction(this, 8f, delegate()
					{
						base.StartCoroutine(AppUtil.FadeOut(base.transform.Find("Mirror1"), 0.5f, null));
						base.transform.Find("Mirror2").gameObject.SetActive(true);
						base.StartCoroutine(AppUtil.FadeIn(base.transform.Find("Mirror2"), 0.5f, null));
						AudioManager.PlaySound(new string[]
						{
							"Click",
							"",
							"ShowResultButton"
						});
					}, true);
					AppUtil.DelayAction(this, 11f, delegate()
					{
						base.transform.Find("EndingText").gameObject.SetActive(true);
					}, true);
					AppUtil.DelayAction(this, 16f, delegate()
					{
						SceneTransition.LoadScene("タイトル", new Color?(Color.black), 3f);
					}, true);
					return;
				}
				if (num != 1386389485U)
				{
					if (num != 1494424848U)
					{
						if (num != 1578922526U)
						{
							return;
						}
						if (!(name == "ComebackDialog"))
						{
							return;
						}
					}
					else
					{
						if (!(name == "BonusMovieDialog"))
						{
							return;
						}
						if (base.transform.Find("EgoBanner/EgoText") != null)
						{
							base.transform.Find("EgoBanner/EgoText").GetComponent<Text>().text = (PlayerStatus.EgoPerSecond * 3f * 60f * 60f).ToString();
						}
						TextLocalization component2;
						if (base.transform.Find("BookBonusButton/Text") != null)
						{
							component2 = base.transform.Find("BookBonusButton/Text").GetComponent<TextLocalization>();
						}
						else
						{
							component2 = base.transform.Find("EsBonusButton/Text").GetComponent<TextLocalization>();
						}
						if (!AdInfo.ENABLE)
						{
							component2.SetKey("[UI]GetButton/Text");
							foreach (string text6 in new string[]
							{
								"Text2-1",
								"Text2-2",
								"Text3-1",
								"Text3-2",
								"EgoBanner/BonusText"
							})
							{
								if (base.transform.Find(text6) != null)
								{
									base.transform.Find(text6).GetComponent<TextLocalization>().SetKey("[UI]BonusMovieDialog/" + text6 + "NoAd");
								}
							}
							return;
						}
						component2.transform.parent.GetComponent<Button>().interactable = AdManager.IsReady("Reward");
						if (AdManager.IsReady("Reward"))
						{
							component2.SetKey("[UI]PlayButton/Text");
							return;
						}
						component2.SetKey("[UI]NoAdMovieMessage");
						return;
					}
				}
				else
				{
					if (!(name == "RetryDialog"))
					{
						return;
					}
					if (!AdInfo.ENABLE)
					{
						base.transform.Find("AttentionText1").gameObject.SetActive(false);
						base.transform.Find("RetryButtonFromDialog/Text").GetComponent<TextLocalization>().SetKey("[UI]RetryButtonFromResult/TextNoAd");
						return;
					}
					if (AdManager.IsReady("Reward"))
					{
						base.transform.Find("RetryButtonFromDialog").GetComponent<Button>().interactable = true;
						base.transform.Find("RetryButtonFromDialog/Text").GetComponent<TextLocalization>().SetKey("[UI]RetryButtonFromResult/Text");
						return;
					}
					base.transform.Find("RetryButtonFromDialog").GetComponent<Button>().interactable = false;
					base.transform.Find("RetryButtonFromDialog/Text").GetComponent<TextLocalization>().SetKey("[UI]NoAdMovieMessage");
					return;
				}
			}
		}
		else if (num <= 2865910535U)
		{
			if (num <= 1862661981U)
			{
				if (num != 1721499631U)
				{
					if (num != 1862661981U)
					{
						return;
					}
					if (!(name == "ShopDialog"))
					{
						return;
					}
					base.transform.Find("MainPage/RestoreButton").gameObject.SetActive(Application.platform == RuntimePlatform.IPhonePlayer);
					int num3 = (int)this.args[0];
					base.transform.Find("MainPage/Shop1").gameObject.SetActive(num3 == 1);
					base.transform.Find("MainPage/Shop2").gameObject.SetActive(num3 == 2);
					int num4 = (num3 == 1) ? 1 : 4;
					for (int j = num4; j < num4 + 3; j++)
					{
						bool flag3 = !PurchasingItem.Get(PurchasingItem.ITEM_LIST[j - 1]);
						base.transform.Find(string.Concat(new object[]
						{
							"MainPage/Shop",
							num3,
							"/ShopItemButton",
							j,
							"/DisableFilter"
						})).gameObject.SetActive(!flag3);
						base.transform.Find(string.Concat(new object[]
						{
							"MainPage/Shop",
							num3,
							"/ShopItemButton",
							j
						})).GetComponent<Button>().enabled = flag3;
					}
					base.transform.Find("MainPage/CloseButton").gameObject.SetActive(GameObject.Find("MenuDialog") == null);
					base.transform.Find("MainPage/BackButton").gameObject.SetActive(GameObject.Find("MenuDialog") != null);
					return;
				}
				else
				{
					if (!(name == "BookPageDialog"))
					{
						return;
					}
					string text7 = (string)this.args[0];
					int num5 = (int)this.args[1];
					int rank = BookLevel.GetRank(text7);
					if (num5 <= 0 || num5 > rank || num5 >= 5)
					{
						this.Close();
						return;
					}
					string text8 = LanguageManager.Get("[Book]Title" + text7);
					base.transform.Find("TitleText").GetComponent<Text>().text = text8;
					int num6 = 100;
					if (text8.Length > 8)
					{
						num6 = 40;
					}
					base.transform.Find("TitleText").GetComponent<LetterSpacing>().spacing = (float)num6;
					for (int k = 1; k < 5; k++)
					{
						base.transform.Find("Bookmark" + k + "/Text").GetComponent<Text>().text = LanguageManager.Get(string.Concat(new object[]
						{
							"[Book]Page",
							text7,
							"Rank",
							k
						}));
						base.transform.Find("Bookmark" + k + "/Active/Text").GetComponent<Text>().text = LanguageManager.Get(string.Concat(new object[]
						{
							"[Book]Page",
							text7,
							"Rank",
							k
						}));
						bool flag4 = num5 == k;
						bool flag5 = rank >= k;
						base.transform.Find("Bookmark" + k).GetComponent<Image>().enabled = !flag4;
						base.transform.Find("Bookmark" + k + "/Active").gameObject.SetActive(flag4);
						base.transform.Find("Bookmark" + k + "/Text").GetComponent<Text>().color = (flag5 ? Color.black : new Color(0.75f, 0.75f, 0.75f));
						base.transform.Find("Bookmark" + k).GetComponent<Button>().interactable = flag5;
					}
					string text9 = LanguageManager.Get("[Book]OriginL" + text7);
					text9 = text9.Replace(",", "").Replace(".", "");
					if (text9.Length >= 25)
					{
						text9 = text9.Insert(text9.IndexOf("『"), "\n");
						base.transform.Find("OriginText").GetComponent<Text>().alignment = TextAnchor.UpperCenter;
					}
					else
					{
						base.transform.Find("OriginText").GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
					}
					base.transform.Find("OriginText").GetComponent<VerticalText>().SetText(text9);
					base.transform.Find("FukidasiText").GetComponent<VerticalText>().SetText(LanguageManager.Get(string.Concat(new object[]
					{
						"[Fukidasi]Book",
						text7,
						"Rank",
						num5
					})));
					base.transform.Find("QuoteText").GetComponent<VerticalText>().SetText(LanguageManager.Get(string.Concat(new object[]
					{
						"[Book]QuoteL",
						text7,
						"Rank",
						num5
					})));
					base.transform.Find("PageText").GetComponent<Text>().text = LanguageManager.Get(string.Concat(new object[]
					{
						"[Book]Page",
						text7,
						"Rank",
						num5
					}));
					return;
				}
			}
			else if (num != 2586654130U)
			{
				if (num != 2749870933U)
				{
					if (num != 2865910535U)
					{
						return;
					}
					if (!(name == "SkipCounselingDialog"))
					{
						return;
					}
					string text10 = (string)this.args[0];
					string text11 = LanguageManager.Get(Data.GetScenarioKey(text10, "型", CounselingResult.Get(text10), 0), false);
					base.transform.Find("ResultText").GetComponent<TextLocalization>().SetParameters(new object[]
					{
						text11
					});
					return;
				}
				else
				{
					if (!(name == "DailyBonusDialog"))
					{
						return;
					}
					int intParameter = Utility.GetIntParameter("日替わり動画ボーナス");
					base.transform.Find("DailyMovieButton/Text2").GetComponent<TextLocalization>().SetParameters(new object[]
					{
						intParameter
					});
					base.transform.Find("EgoBanner/EgoText").GetComponent<Text>().text = this.args[0].ToString();
					return;
				}
			}
			else
			{
				if (!(name == "RankupDialog"))
				{
					return;
				}
				string text12 = (string)args[0];
				int rank2 = BookLevel.GetRank(text12);
				bool flag6 = rank2 == 5;
				string format = LanguageManager.Get("[UI]RankupLabel/TitleText" + (flag6 ? "Max" : ""));
				base.transform.Find("RankupLabel/TitleText").GetComponent<TextLocalization>().SetText(string.Format(format, LanguageManager.Get("[Book]Title" + text12)));
				int num7 = BookLevel.Get(text12);
				format = LanguageManager.Get("[UI]RankupLabel/EgoText");
				base.transform.Find("RankupLabel/ValueLayout/EgoTextBefore").GetComponent<Text>().text = string.Format(format, BookEgoPoint.GetBookEgo(text12, "per_second", num7 - 1).ToString());
				base.transform.Find("RankupLabel/ValueLayout/EgoTextAfter").GetComponent<Text>().text = string.Format(format, BookEgoPoint.GetBookEgo(text12, "per_second", num7).ToString());
				float num8 = 0f;
				foreach (RectTransform rectTransform in base.transform.Find("RankupLabel/ValueLayout").GetComponentsInChildren<RectTransform>(true))
				{
					if (!(rectTransform.name == "ValueLayout"))
					{
						rectTransform.anchoredPosition = new Vector2(num8, rectTransform.anchoredPosition.y);
						float preferredWidth = rectTransform.GetComponent<Text>().preferredWidth;
						rectTransform.sizeDelta = new Vector2(preferredWidth, rectTransform.sizeDelta.y);
						num8 += preferredWidth + 5f;
					}
				}
				num8 -= 5f;
				RectTransform componentInChildren = base.transform.Find("RankupLabel/ValueLayout").GetComponentInChildren<RectTransform>();
				componentInChildren.anchoredPosition = new Vector2((componentInChildren.parent.GetComponent<RectTransform>().rect.width - num8) / 2f, 0f);
				base.transform.Find("RankupLabel/QuoteText").GetComponent<HyphenationJpn>().SetText(LanguageManager.Get(string.Concat(new object[]
				{
					"[Book]QuoteS",
					text12,
					"Rank",
					rank2
				})));
				format = LanguageManager.Get("[UI]RankupLabel/OriginText");
				base.transform.Find("RankupLabel/OriginText").GetComponent<Text>().text = string.Format(format, LanguageManager.Get("[Book]Author" + text12), LanguageManager.Get("[Book]Title" + text12));
				RectTransform component3 = base.transform.Find("RankupLabel/OriginText").GetComponent<RectTransform>();
				float num9 = -220f - base.transform.Find("RankupLabel/QuoteText").GetComponent<Text>().preferredHeight;
				num9 = Mathf.Max(-340f, num9);
				component3.anchoredPosition = new Vector2(component3.anchoredPosition.x, num9);
				return;
			}
		}
		else if (num <= 3896356444U)
		{
			if (num != 2983740493U)
			{
				if (num != 3598380987U)
				{
					if (num != 3896356444U)
					{
						return;
					}
					if (!(name == "MoveDataDialog"))
					{
						return;
					}
					if (!PlayerPrefs.HasKey("ScenarioNo"))
					{
						base.transform.Find("OpenSaveDataButton").GetComponent<Button>().interactable = false;
						base.transform.Find("OpenSaveDataButton/Text").GetComponent<Text>().color = new Color(0.7f, 0.7f, 0.7f);
					}
					base.transform.Find("OpenSaveDataButton/Text").GetComponent<TextLocalization>().SetKey("[UI]OpenSaveDataButton/Text");
					return;
				}
				else
				{
					if (!(name == "TutorialDialog"))
					{
						return;
					}
					base.transform.Find("MainPage/CloseButton").gameObject.SetActive(GameObject.Find("MenuDialog") == null);
					base.transform.Find("MainPage/BackButton").gameObject.SetActive(GameObject.Find("MenuDialog") != null);
					return;
				}
			}
			else
			{
				if (!(name == "LoadDataDialog"))
				{
					return;
				}
				base.transform.Find("LoadData/ErrorText").GetComponent<TextLocalization>().SetKey("[UI]LoadDataDialog/ErrorText");
				base.transform.Find("LoadData/InputField/Text").GetComponent<Text>().horizontalOverflow = HorizontalWrapMode.Wrap;
				return;
			}
		}
		else if (num != 3970019489U)
		{
			if (num != 4084557752U)
			{
				if (num != 4096312756U)
				{
					return;
				}
				if (!(name == "GetEgoDialog"))
				{
					return;
				}
			}
			else
			{
				if (!(name == "RestartDialogSE"))
				{
					return;
				}
				goto IL_ACE;
			}
		}
		else
		{
			if (!(name == "GetPrizeDialog"))
			{
				return;
			}
			string text13 = (string)this.args[0];
			base.transform.Find("PrizeLabel/PrizeGroup1").gameObject.SetActive(false);
			base.transform.Find("PrizeLabel/PrizeGroup2").gameObject.SetActive(false);
			base.transform.Find("PrizeLabel/PrizeGroup3").gameObject.SetActive(false);
			string text14 = "";
			string text15 = "PrizeLabel/";
			EgoPoint egoPoint = null;
			float value = 1f;
			if (text13.Contains("分のEGO"))
			{
				string pointInString = text13.Replace("1時間分のEGO", "").Replace("3時間分のEGO", "").Replace("<br>を獲得", "");
				PlayerStatus.EgoPoint += new EgoPoint(pointInString);
				text15 += "PrizeGroup1";
			}
			else if (text13.Contains("本の獲得EGO"))
			{
				text14 = LanguageManager.Get("[UI]RankupLabel/EgoText");
				text15 += "PrizeGroup2";
				egoPoint = PlayerStatus.EgoPerSecond;
				value = float.Parse(text13.Replace("本の獲得EGO", "").Replace("倍", ""));
			}
			else
			{
				text14 = "{0}";
				text15 += "PrizeGroup3";
				egoPoint = Data.TapPower(false);
				value = float.Parse(text13.Replace("タップ獲得EGO", "").Replace("倍", ""));
			}
			base.transform.Find(text15).gameObject.SetActive(true);
			Image component4 = base.transform.Find(text15 + "/Icon").GetComponent<Image>();
			component4.sprite = (Sprite)this.args[2];
			component4.SetNativeSize();
			base.transform.Find(text15 + "/PrizeText").GetComponent<TextLocalization>().SetText((string)this.args[1]);
			if (text14 != "")
			{
				EgoPoint arg = egoPoint * value;
				base.transform.Find(text15 + "/ValueLayout/EgoTextBefore").GetComponent<Text>().text = string.Format(text14, egoPoint);
				base.transform.Find(text15 + "/ValueLayout/EgoTextAfter").GetComponent<Text>().text = string.Format(text14, arg);
			}
			if (text15.Contains("Group2"))
			{
				float num10 = 0f;
				foreach (RectTransform rectTransform2 in base.transform.Find(text15 + "/ValueLayout").GetComponentsInChildren<RectTransform>(true))
				{
					if (!(rectTransform2.name == "ValueLayout"))
					{
						rectTransform2.anchoredPosition = new Vector2(num10, rectTransform2.anchoredPosition.y);
						float preferredWidth2 = rectTransform2.GetComponent<Text>().preferredWidth;
						rectTransform2.sizeDelta = new Vector2(preferredWidth2, rectTransform2.sizeDelta.y);
						num10 += preferredWidth2 + 5f;
					}
				}
				num10 -= 5f;
				RectTransform componentInChildren2 = base.transform.Find(text15 + "/ValueLayout").GetComponentInChildren<RectTransform>();
				componentInChildren2.anchoredPosition = new Vector2((componentInChildren2.parent.GetComponent<RectTransform>().rect.width - num10) / 2f, -200f);
				return;
			}
			return;
		}
		if (this.args.Length != 0)
		{
			base.transform.Find("EgoBanner/Text").GetComponent<Text>().text = this.args[0].ToString();
			return;
		}
		return;
		IL_ACE:
		base.transform.Find("Icon1").gameObject.SetActive(true);
		base.transform.Find("Icon2").gameObject.SetActive(false);
		if (base.gameObject.name == "RestartDialogID")
		{
			base.InvokeRepeating("ArrangeText", 0f, 0.84f);
			return;
		}
	}

	// Token: 0x0600003F RID: 63 RVA: 0x00007017 File Offset: 0x00005217
	public override IEnumerator OnClosed()
	{
		AdManager.Hide("Rectangle");
		string name = base.gameObject.name;
		if (!(name == "MenuDialog"))
		{
			if (name == "ShopDialog" || name == "CreditsDialog" || name == "TutorialDialog")
			{
				if (GameObject.Find("MenuDialog") != null)
				{
					yield return this.CloseMenuPage(GameObject.Find("MenuDialog"));
				}
				else
				{
					this.SlideDialog(false);
				}
			}
		}
		else
		{
			this.SlideDialog(false);
		}
		if (base.gameObject.name == "ShopDialog")
		{
			base.StartCoroutine(AppUtil.FadeOut(base.transform.Find("MainPage/Shop1/Shop1Button"), 0.25f, null));
			base.StartCoroutine(AppUtil.FadeOut(base.transform.Find("MainPage/Shop1/Shop2Button"), 0.25f, null));
			base.StartCoroutine(AppUtil.FadeOut(base.transform.Find("MainPage/Shop2/Shop1Button"), 0.25f, null));
			base.StartCoroutine(AppUtil.FadeOut(base.transform.Find("MainPage/Shop2/Shop2Button"), 0.25f, null));
		}
		yield return base.OnClosed();
		yield break;
	}

	// Token: 0x06000040 RID: 64 RVA: 0x00007028 File Offset: 0x00005228
	public void OnValueChanged(string args)
	{
		Transform transform = base.transform.Find("LoadData/LoadDataButton");
		if (transform != null)
		{
			bool flag = args != "" && args.Length >= 1;
			transform.GetComponent<Button>().interactable = flag;
			Color white = Color.white;
			white.a = (flag ? 1f : 0.5f);
			transform.GetComponentInChildren<Text>().color = white;
		}
	}

	// Token: 0x06000041 RID: 65 RVA: 0x000070A0 File Offset: 0x000052A0
	public void OnValueChanged(string[] args)
	{
		bool flag = bool.Parse(args[1]);
		string a = args[0];
		if (a == "SwitchBGM")
		{
			Settings.BGM = flag;
			return;
		}
		if (!(a == "SwitchSE"))
		{
			return;
		}
		Settings.SE = flag;
	}

	// Token: 0x06000042 RID: 66 RVA: 0x000070E4 File Offset: 0x000052E4
	private void ArrangeText()
	{
		base.transform.Find("FlavorText").localRotation = Quaternion.identity;
		base.GetComponentInChildren<CommentUI>().ArrangeText();
		base.transform.Find("FlavorText").Rotate(new Vector3(0f, 0f, 4.6f));
	}

	// Token: 0x06000043 RID: 67 RVA: 0x0000713F File Offset: 0x0000533F
	private IEnumerator Save()
	{
		base.GetComponent<CanvasGroup>().blocksRaycasts = false;
		base.transform.Find("OpenSaveDataButton/Text").GetComponent<TextLocalization>().SetKey("[UI]MoveDataDialog/Processing");
		string errorMsg = "";
		Action<string> <>9__0;
		int num;
		for (int retryCount = 0; retryCount < 5; retryCount = num + 1)
		{
			Action<string> callback;
			if ((callback = <>9__0) == null)
			{
				callback = (<>9__0 = delegate(string ret)
				{
					errorMsg = ret;
				});
			}
			yield return SaveDataManager.Save(callback);
			if (errorMsg == "SUCCESS")
			{
				break;
			}
			num = retryCount;
		}
		base.GetComponent<CanvasGroup>().blocksRaycasts = true;
		if (errorMsg == "SUCCESS")
		{
			DialogManager.ShowDialog("SaveDataDialog", Array.Empty<object>());
		}
		else
		{
			DialogManager.ShowDialog("MoveDataErrorDialog", Array.Empty<object>());
		}
		yield break;
	}

	// Token: 0x06000044 RID: 68 RVA: 0x0000714E File Offset: 0x0000534E
	private IEnumerator Load()
	{
		TextLocalization errorText = base.transform.Find("LoadData/ErrorText").GetComponent<TextLocalization>();
		errorText.SetKey("[UI]LoadDataDialog/ErrorText");
		string loadID = base.transform.Find("LoadData/InputField/Text").GetComponent<Text>().text;
		if (Settings.SaveDataID == loadID)
		{
			errorText.SetKey("[UI]LoadDataDialog/ErrorText2");
			yield break;
		}
		base.GetComponent<CanvasGroup>().blocksRaycasts = false;
		base.transform.Find("LoadData/LoadDataButton/Text").GetComponent<TextLocalization>().SetKey("[UI]MoveDataDialog/Processing");
		bool ret = false;
		Action<bool> <>9__0;
		int num;
		for (int retryCount = 0; retryCount < 5; retryCount = num + 1)
		{
			string loadID2 = loadID;
			Action<bool> callback;
			if ((callback = <>9__0) == null)
			{
				callback = (<>9__0 = delegate(bool result)
				{
					ret = result;
				});
			}
			yield return SaveDataManager.Load(loadID2, callback);
			if (ret)
			{
				break;
			}
			num = retryCount;
		}
		if (ret)
		{
			DialogManager.CloseAllDialog();
			DialogManager.ShowDialog("MoveDataSuccessDialog", Array.Empty<object>());
		}
		else
		{
			base.transform.Find("LoadData/LoadDataButton/Text").GetComponent<TextLocalization>().SetKey("[UI]LoadDataDialog/ButtonText");
			errorText.SetKey("[UI]LoadDataDialog/ErrorText1");
		}
		base.GetComponent<CanvasGroup>().blocksRaycasts = true;
		yield break;
	}

	// Token: 0x06000045 RID: 69 RVA: 0x0000715D File Offset: 0x0000535D
	public DialogEgo()
	{
	}

	// Token: 0x06000046 RID: 70 RVA: 0x0000717B File Offset: 0x0000537B
	[CompilerGenerated]
	private void <Render>b__3_0()
	{
		this.Close();
	}

	// Token: 0x06000047 RID: 71 RVA: 0x00007183 File Offset: 0x00005383
	[CompilerGenerated]
	private void <Render>b__3_2()
	{
		base.transform.Find("CloseButton").gameObject.SetActive(true);
		base.StartCoroutine(AppUtil.FadeIn(base.transform.Find("CloseButton"), 0.25f, null));
	}

	// Token: 0x06000048 RID: 72 RVA: 0x00007183 File Offset: 0x00005383
	[CompilerGenerated]
	private void <Render>b__3_3()
	{
		base.transform.Find("CloseButton").gameObject.SetActive(true);
		base.StartCoroutine(AppUtil.FadeIn(base.transform.Find("CloseButton"), 0.25f, null));
	}

	// Token: 0x06000049 RID: 73 RVA: 0x000071C2 File Offset: 0x000053C2
	[CompilerGenerated]
	private void <OnShown>b__10_0()
	{
		base.transform.Find("Mirror1").gameObject.SetActive(true);
		base.StartCoroutine(AppUtil.FadeIn(base.transform.Find("Mirror1"), 0.5f, null));
	}

	// Token: 0x0600004A RID: 74 RVA: 0x00007204 File Offset: 0x00005404
	[CompilerGenerated]
	private void <OnShown>b__10_1()
	{
		base.StartCoroutine(AppUtil.FadeOut(base.transform.Find("Mirror1"), 0.5f, null));
		base.transform.Find("Mirror2").gameObject.SetActive(true);
		base.StartCoroutine(AppUtil.FadeIn(base.transform.Find("Mirror2"), 0.5f, null));
		AudioManager.PlaySound(new string[]
		{
			"Click",
			"",
			"ShowResultButton"
		});
	}

	// Token: 0x0600004B RID: 75 RVA: 0x00007293 File Offset: 0x00005493
	[CompilerGenerated]
	private void <OnShown>b__10_2()
	{
		base.transform.Find("EndingText").gameObject.SetActive(true);
	}

	// Token: 0x0600004C RID: 76 RVA: 0x000072B0 File Offset: 0x000054B0
	[CompilerGenerated]
	[DebuggerHidden]
	private IEnumerator <>n__0()
	{
		return base.OnClosed();
	}

	// Token: 0x04000015 RID: 21
	private float WAIT_PAGE_TIME = 0.08f;

	// Token: 0x04000016 RID: 22
	private float WAIT_PAGE_ALPHA = 0.9f;

	// Token: 0x020000BB RID: 187
	[CompilerGenerated]
	private sealed class <>c__DisplayClass0_0
	{
		// Token: 0x060007AE RID: 1966 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass0_0()
		{
		}

		// Token: 0x060007AF RID: 1967 RVA: 0x00026C45 File Offset: 0x00024E45
		internal void <OnClick>b__0()
		{
			this.<>4__this.Close();
		}

		// Token: 0x060007B0 RID: 1968 RVA: 0x00026C52 File Offset: 0x00024E52
		internal void <OnClick>b__2(bool reward)
		{
			this.<>4__this.RewardForBonus("Time", reward, false);
		}

		// Token: 0x060007B1 RID: 1969 RVA: 0x00026C66 File Offset: 0x00024E66
		internal void <OnClick>b__3(bool reward)
		{
			this.<>4__this.RewardForBonus("Temp", reward, true);
		}

		// Token: 0x060007B2 RID: 1970 RVA: 0x00026C7A File Offset: 0x00024E7A
		internal void <OnClick>b__4()
		{
			this.clickObject.GetComponent<Button>().interactable = true;
		}

		// Token: 0x060007B3 RID: 1971 RVA: 0x00026C8D File Offset: 0x00024E8D
		internal void <OnClick>b__5(bool reward)
		{
			if (reward)
			{
				DialogManager.ShowDialog("GetDailyBonusDialog", new object[]
				{
					this.<>4__this.args[0]
				});
			}
		}

		// Token: 0x040002B7 RID: 695
		public DialogEgo <>4__this;

		// Token: 0x040002B8 RID: 696
		public GameObject clickObject;
	}

	// Token: 0x020000BC RID: 188
	[CompilerGenerated]
	[Serializable]
	private sealed class <>c
	{
		// Token: 0x060007B4 RID: 1972 RVA: 0x00026CB2 File Offset: 0x00024EB2
		// Note: this type is marked as 'beforefieldinit'.
		static <>c()
		{
		}

		// Token: 0x060007B5 RID: 1973 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c()
		{
		}

		// Token: 0x060007B6 RID: 1974 RVA: 0x00026CBE File Offset: 0x00024EBE
		internal void <OnClick>b__0_1()
		{
			Data.Restart();
		}

		// Token: 0x060007B7 RID: 1975 RVA: 0x00026CC5 File Offset: 0x00024EC5
		internal void <Render>b__3_1()
		{
			AdManager.Show("Rectangle", null);
		}

		// Token: 0x060007B8 RID: 1976 RVA: 0x00026CD2 File Offset: 0x00024ED2
		internal void <OnShown>b__10_3()
		{
			SceneTransition.LoadScene("タイトル", new Color?(Color.black), 3f);
		}

		// Token: 0x040002B9 RID: 697
		public static readonly DialogEgo.<>c <>9 = new DialogEgo.<>c();

		// Token: 0x040002BA RID: 698
		public static Action <>9__0_1;

		// Token: 0x040002BB RID: 699
		public static Action <>9__3_1;

		// Token: 0x040002BC RID: 700
		public static Action <>9__10_3;
	}

	// Token: 0x020000BD RID: 189
	[CompilerGenerated]
	private sealed class <>c__DisplayClass5_0
	{
		// Token: 0x060007B9 RID: 1977 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass5_0()
		{
		}

		// Token: 0x060007BA RID: 1978 RVA: 0x00026CED File Offset: 0x00024EED
		internal void <SlideDialog>b__0(float tmp)
		{
			this.<>4__this.GetComponent<RectTransform>().anchoredPosition = new Vector2(tmp, this.y);
		}

		// Token: 0x060007BB RID: 1979 RVA: 0x00026CED File Offset: 0x00024EED
		internal void <SlideDialog>b__1(float tmp)
		{
			this.<>4__this.GetComponent<RectTransform>().anchoredPosition = new Vector2(tmp, this.y);
		}

		// Token: 0x040002BD RID: 701
		public DialogEgo <>4__this;

		// Token: 0x040002BE RID: 702
		public float y;
	}

	// Token: 0x020000BE RID: 190
	[CompilerGenerated]
	private sealed class <CloseMenuPage>d__8 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060007BC RID: 1980 RVA: 0x00026D0B File Offset: 0x00024F0B
		[DebuggerHidden]
		public <CloseMenuPage>d__8(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060007BD RID: 1981 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060007BE RID: 1982 RVA: 0x00026D1C File Offset: 0x00024F1C
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			DialogEgo dialogEgo = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				menu.transform.SetAsLastSibling();
				AppUtil.SetAlpha(menu.transform.Find("InnerPage2"), 0f);
				this.<>2__current = null;
				this.<>1__state = 1;
				return true;
			case 1:
				this.<>1__state = -1;
				AppUtil.SetAlpha(menu.transform.Find("InnerPage2"), dialogEgo.WAIT_PAGE_ALPHA);
				this.<>2__current = AppUtil.WaitRealtime(dialogEgo.WAIT_PAGE_TIME);
				this.<>1__state = 2;
				return true;
			case 2:
				this.<>1__state = -1;
				AppUtil.SetAlpha(menu.transform.Find("InnerPage2"), 0f);
				AppUtil.SetAlpha(menu.transform.Find("InnerPage3"), dialogEgo.WAIT_PAGE_ALPHA);
				this.<>2__current = AppUtil.WaitRealtime(dialogEgo.WAIT_PAGE_TIME);
				this.<>1__state = 3;
				return true;
			case 3:
				this.<>1__state = -1;
				AppUtil.SetAlpha(menu.transform.Find("InnerPage3"), 0f);
				menu.transform.Find("MainPage").gameObject.SetActive(true);
				return false;
			default:
				return false;
			}
		}

		// Token: 0x170000D0 RID: 208
		// (get) Token: 0x060007BF RID: 1983 RVA: 0x00026E82 File Offset: 0x00025082
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060007C0 RID: 1984 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000D1 RID: 209
		// (get) Token: 0x060007C1 RID: 1985 RVA: 0x00026E82 File Offset: 0x00025082
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040002BF RID: 703
		private int <>1__state;

		// Token: 0x040002C0 RID: 704
		private object <>2__current;

		// Token: 0x040002C1 RID: 705
		public GameObject menu;

		// Token: 0x040002C2 RID: 706
		public DialogEgo <>4__this;
	}

	// Token: 0x020000BF RID: 191
	[CompilerGenerated]
	private sealed class <ShowMenuPage>d__9 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060007C2 RID: 1986 RVA: 0x00026E8A File Offset: 0x0002508A
		[DebuggerHidden]
		public <ShowMenuPage>d__9(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060007C3 RID: 1987 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060007C4 RID: 1988 RVA: 0x00026E9C File Offset: 0x0002509C
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			DialogEgo dialogEgo = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				menu.transform.SetAsLastSibling();
				this.<>2__current = null;
				this.<>1__state = 1;
				return true;
			case 1:
				this.<>1__state = -1;
				menu.transform.Find("MainPage").gameObject.SetActive(false);
				AppUtil.SetAlpha(menu.transform.Find("InnerPage3"), dialogEgo.WAIT_PAGE_ALPHA);
				this.<>2__current = AppUtil.WaitRealtime(dialogEgo.WAIT_PAGE_TIME);
				this.<>1__state = 2;
				return true;
			case 2:
				this.<>1__state = -1;
				AppUtil.SetAlpha(menu.transform.Find("InnerPage3"), 0f);
				AppUtil.SetAlpha(menu.transform.Find("InnerPage2"), 1f);
				this.<>2__current = AppUtil.WaitRealtime(dialogEgo.WAIT_PAGE_TIME);
				this.<>1__state = 3;
				return true;
			case 3:
				this.<>1__state = -1;
				AppUtil.SetAlpha(menu.transform.Find("InnerPage2"), 0f);
				return false;
			default:
				return false;
			}
		}

		// Token: 0x170000D2 RID: 210
		// (get) Token: 0x060007C5 RID: 1989 RVA: 0x00026FE1 File Offset: 0x000251E1
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060007C6 RID: 1990 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000D3 RID: 211
		// (get) Token: 0x060007C7 RID: 1991 RVA: 0x00026FE1 File Offset: 0x000251E1
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040002C3 RID: 707
		private int <>1__state;

		// Token: 0x040002C4 RID: 708
		private object <>2__current;

		// Token: 0x040002C5 RID: 709
		public GameObject menu;

		// Token: 0x040002C6 RID: 710
		public DialogEgo <>4__this;
	}

	// Token: 0x020000C0 RID: 192
	[CompilerGenerated]
	private sealed class <>c__DisplayClass10_0
	{
		// Token: 0x060007C8 RID: 1992 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass10_0()
		{
		}

		// Token: 0x060007C9 RID: 1993 RVA: 0x00026FE9 File Offset: 0x000251E9
		internal void <OnShown>b__4()
		{
			this.<>4__this.transform.Find("ButterflyParticle").gameObject.SetActive(this.is1st);
		}

		// Token: 0x040002C7 RID: 711
		public bool is1st;

		// Token: 0x040002C8 RID: 712
		public DialogEgo <>4__this;
	}

	// Token: 0x020000C1 RID: 193
	[CompilerGenerated]
	private sealed class <OnClosed>d__11 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060007CA RID: 1994 RVA: 0x00027010 File Offset: 0x00025210
		[DebuggerHidden]
		public <OnClosed>d__11(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060007CB RID: 1995 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060007CC RID: 1996 RVA: 0x00027020 File Offset: 0x00025220
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			DialogEgo dialogEgo = this;
			switch (num)
			{
			case 0:
			{
				this.<>1__state = -1;
				AdManager.Hide("Rectangle");
				string name = dialogEgo.gameObject.name;
				if (!(name == "MenuDialog"))
				{
					if (name == "ShopDialog" || name == "CreditsDialog" || name == "TutorialDialog")
					{
						if (GameObject.Find("MenuDialog") != null)
						{
							this.<>2__current = dialogEgo.CloseMenuPage(GameObject.Find("MenuDialog"));
							this.<>1__state = 1;
							return true;
						}
						dialogEgo.SlideDialog(false);
					}
				}
				else
				{
					dialogEgo.SlideDialog(false);
				}
				break;
			}
			case 1:
				this.<>1__state = -1;
				break;
			case 2:
				this.<>1__state = -1;
				return false;
			default:
				return false;
			}
			if (dialogEgo.gameObject.name == "ShopDialog")
			{
				dialogEgo.StartCoroutine(AppUtil.FadeOut(dialogEgo.transform.Find("MainPage/Shop1/Shop1Button"), 0.25f, null));
				dialogEgo.StartCoroutine(AppUtil.FadeOut(dialogEgo.transform.Find("MainPage/Shop1/Shop2Button"), 0.25f, null));
				dialogEgo.StartCoroutine(AppUtil.FadeOut(dialogEgo.transform.Find("MainPage/Shop2/Shop1Button"), 0.25f, null));
				dialogEgo.StartCoroutine(AppUtil.FadeOut(dialogEgo.transform.Find("MainPage/Shop2/Shop2Button"), 0.25f, null));
			}
			this.<>2__current = dialogEgo.<>n__0();
			this.<>1__state = 2;
			return true;
		}

		// Token: 0x170000D4 RID: 212
		// (get) Token: 0x060007CD RID: 1997 RVA: 0x000271AB File Offset: 0x000253AB
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060007CE RID: 1998 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000D5 RID: 213
		// (get) Token: 0x060007CF RID: 1999 RVA: 0x000271AB File Offset: 0x000253AB
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040002C9 RID: 713
		private int <>1__state;

		// Token: 0x040002CA RID: 714
		private object <>2__current;

		// Token: 0x040002CB RID: 715
		public DialogEgo <>4__this;
	}

	// Token: 0x020000C2 RID: 194
	[CompilerGenerated]
	private sealed class <>c__DisplayClass15_0
	{
		// Token: 0x060007D0 RID: 2000 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass15_0()
		{
		}

		// Token: 0x060007D1 RID: 2001 RVA: 0x000271B3 File Offset: 0x000253B3
		internal void <Save>b__0(string ret)
		{
			this.errorMsg = ret;
		}

		// Token: 0x040002CC RID: 716
		public string errorMsg;

		// Token: 0x040002CD RID: 717
		public Action<string> <>9__0;
	}

	// Token: 0x020000C3 RID: 195
	[CompilerGenerated]
	private sealed class <Save>d__15 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060007D2 RID: 2002 RVA: 0x000271BC File Offset: 0x000253BC
		[DebuggerHidden]
		public <Save>d__15(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060007D3 RID: 2003 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060007D4 RID: 2004 RVA: 0x000271CC File Offset: 0x000253CC
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			DialogEgo dialogEgo = this;
			if (num != 0)
			{
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				if (CS$<>8__locals1.errorMsg == "SUCCESS")
				{
					goto IL_E9;
				}
				int num2 = retryCount;
				retryCount = num2 + 1;
			}
			else
			{
				this.<>1__state = -1;
				CS$<>8__locals1 = new DialogEgo.<>c__DisplayClass15_0();
				dialogEgo.GetComponent<CanvasGroup>().blocksRaycasts = false;
				dialogEgo.transform.Find("OpenSaveDataButton/Text").GetComponent<TextLocalization>().SetKey("[UI]MoveDataDialog/Processing");
				CS$<>8__locals1.errorMsg = "";
				retryCount = 0;
			}
			if (retryCount < 5)
			{
				Action<string> callback;
				if ((callback = CS$<>8__locals1.<>9__0) == null)
				{
					callback = (CS$<>8__locals1.<>9__0 = new Action<string>(CS$<>8__locals1.<Save>b__0));
				}
				this.<>2__current = SaveDataManager.Save(callback);
				this.<>1__state = 1;
				return true;
			}
			IL_E9:
			dialogEgo.GetComponent<CanvasGroup>().blocksRaycasts = true;
			if (CS$<>8__locals1.errorMsg == "SUCCESS")
			{
				DialogManager.ShowDialog("SaveDataDialog", Array.Empty<object>());
			}
			else
			{
				DialogManager.ShowDialog("MoveDataErrorDialog", Array.Empty<object>());
			}
			return false;
		}

		// Token: 0x170000D6 RID: 214
		// (get) Token: 0x060007D5 RID: 2005 RVA: 0x00027306 File Offset: 0x00025506
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060007D6 RID: 2006 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000D7 RID: 215
		// (get) Token: 0x060007D7 RID: 2007 RVA: 0x00027306 File Offset: 0x00025506
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040002CE RID: 718
		private int <>1__state;

		// Token: 0x040002CF RID: 719
		private object <>2__current;

		// Token: 0x040002D0 RID: 720
		public DialogEgo <>4__this;

		// Token: 0x040002D1 RID: 721
		private DialogEgo.<>c__DisplayClass15_0 <>8__1;

		// Token: 0x040002D2 RID: 722
		private int <retryCount>5__2;
	}

	// Token: 0x020000C4 RID: 196
	[CompilerGenerated]
	private sealed class <>c__DisplayClass16_0
	{
		// Token: 0x060007D8 RID: 2008 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass16_0()
		{
		}

		// Token: 0x060007D9 RID: 2009 RVA: 0x0002730E File Offset: 0x0002550E
		internal void <Load>b__0(bool result)
		{
			this.ret = result;
		}

		// Token: 0x040002D3 RID: 723
		public bool ret;

		// Token: 0x040002D4 RID: 724
		public Action<bool> <>9__0;
	}

	// Token: 0x020000C5 RID: 197
	[CompilerGenerated]
	private sealed class <Load>d__16 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060007DA RID: 2010 RVA: 0x00027317 File Offset: 0x00025517
		[DebuggerHidden]
		public <Load>d__16(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060007DB RID: 2011 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060007DC RID: 2012 RVA: 0x00027328 File Offset: 0x00025528
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			DialogEgo dialogEgo = this;
			if (num != 0)
			{
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				if (CS$<>8__locals1.ret)
				{
					goto IL_150;
				}
				int num2 = retryCount;
				retryCount = num2 + 1;
			}
			else
			{
				this.<>1__state = -1;
				CS$<>8__locals1 = new DialogEgo.<>c__DisplayClass16_0();
				errorText = dialogEgo.transform.Find("LoadData/ErrorText").GetComponent<TextLocalization>();
				errorText.SetKey("[UI]LoadDataDialog/ErrorText");
				loadID = dialogEgo.transform.Find("LoadData/InputField/Text").GetComponent<Text>().text;
				if (Settings.SaveDataID == loadID)
				{
					errorText.SetKey("[UI]LoadDataDialog/ErrorText2");
					return false;
				}
				dialogEgo.GetComponent<CanvasGroup>().blocksRaycasts = false;
				dialogEgo.transform.Find("LoadData/LoadDataButton/Text").GetComponent<TextLocalization>().SetKey("[UI]MoveDataDialog/Processing");
				CS$<>8__locals1.ret = false;
				retryCount = 0;
			}
			if (retryCount < 5)
			{
				string loadID2 = loadID;
				Action<bool> callback;
				if ((callback = CS$<>8__locals1.<>9__0) == null)
				{
					callback = (CS$<>8__locals1.<>9__0 = new Action<bool>(CS$<>8__locals1.<Load>b__0));
				}
				this.<>2__current = SaveDataManager.Load(loadID2, callback);
				this.<>1__state = 1;
				return true;
			}
			IL_150:
			if (CS$<>8__locals1.ret)
			{
				DialogManager.CloseAllDialog();
				DialogManager.ShowDialog("MoveDataSuccessDialog", Array.Empty<object>());
			}
			else
			{
				dialogEgo.transform.Find("LoadData/LoadDataButton/Text").GetComponent<TextLocalization>().SetKey("[UI]LoadDataDialog/ButtonText");
				errorText.SetKey("[UI]LoadDataDialog/ErrorText1");
			}
			dialogEgo.GetComponent<CanvasGroup>().blocksRaycasts = true;
			return false;
		}

		// Token: 0x170000D8 RID: 216
		// (get) Token: 0x060007DD RID: 2013 RVA: 0x000274E4 File Offset: 0x000256E4
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060007DE RID: 2014 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000D9 RID: 217
		// (get) Token: 0x060007DF RID: 2015 RVA: 0x000274E4 File Offset: 0x000256E4
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040002D5 RID: 725
		private int <>1__state;

		// Token: 0x040002D6 RID: 726
		private object <>2__current;

		// Token: 0x040002D7 RID: 727
		public DialogEgo <>4__this;

		// Token: 0x040002D8 RID: 728
		private DialogEgo.<>c__DisplayClass16_0 <>8__1;

		// Token: 0x040002D9 RID: 729
		private TextLocalization <errorText>5__2;

		// Token: 0x040002DA RID: 730
		private string <loadID>5__3;

		// Token: 0x040002DB RID: 731
		private int <retryCount>5__4;
	}
}
