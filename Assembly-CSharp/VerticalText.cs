﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000024 RID: 36
[RequireComponent(typeof(Text))]
public class VerticalText : BaseMeshEffect
{
	// Token: 0x17000012 RID: 18
	// (get) Token: 0x06000131 RID: 305 RVA: 0x0000CFF1 File Offset: 0x0000B1F1
	// (set) Token: 0x06000132 RID: 306 RVA: 0x0000CFF9 File Offset: 0x0000B1F9
	public string text
	{
		[CompilerGenerated]
		get
		{
			return this.<text>k__BackingField;
		}
		[CompilerGenerated]
		private set
		{
			this.<text>k__BackingField = value;
		}
	}

	// Token: 0x17000013 RID: 19
	// (get) Token: 0x06000133 RID: 307 RVA: 0x0000D002 File Offset: 0x0000B202
	// (set) Token: 0x06000134 RID: 308 RVA: 0x0000D00A File Offset: 0x0000B20A
	public float spacing
	{
		get
		{
			return this.m_spacing;
		}
		set
		{
			if (this.m_spacing == value)
			{
				return;
			}
			this.m_spacing = value;
			if (base.graphic != null)
			{
				base.graphic.SetVerticesDirty();
			}
		}
	}

	// Token: 0x06000135 RID: 309 RVA: 0x0000D038 File Offset: 0x0000B238
	public override void ModifyMesh(VertexHelper vertexList)
	{
		if (!this.IsActive())
		{
			return;
		}
		List<UIVertex> list = new List<UIVertex>();
		vertexList.GetUIVertexStream(list);
		this.ModifyVertices(list);
		vertexList.Clear();
		vertexList.AddUIVertexTriangleStream(list);
	}

	// Token: 0x06000136 RID: 310 RVA: 0x0000D070 File Offset: 0x0000B270
	public void ModifyVertices(List<UIVertex> vertexList)
	{
		Text component = base.GetComponent<Text>();
		if (component == null || component.text == null)
		{
			return;
		}
		if (component.text.ToCharArray().Length == 0)
		{
			return;
		}
		string text = component.text;
		int fontSize = component.fontSize;
		float num = this.spacing * (float)fontSize / 100f;
		float num2 = 0f;
		int num3 = 0;
		bool supportRichText = component.supportRichText;
		IEnumerator enumerator = null;
		Match match = null;
		switch (component.alignment)
		{
		case TextAnchor.UpperLeft:
		case TextAnchor.MiddleLeft:
		case TextAnchor.LowerLeft:
			num2 = 0f;
			break;
		case TextAnchor.UpperCenter:
		case TextAnchor.MiddleCenter:
		case TextAnchor.LowerCenter:
			num2 = 0.5f;
			break;
		case TextAnchor.UpperRight:
		case TextAnchor.MiddleRight:
		case TextAnchor.LowerRight:
			num2 = 1f;
			break;
		}
		component.font.RequestCharactersInTexture(text, fontSize);
		string[] array = text.Split(new char[]
		{
			'\n'
		});
		float[] array2 = new float[array[0].Length];
		foreach (string text2 in array)
		{
			int length = text2.Length;
			if (supportRichText)
			{
				enumerator = LetterSpacing.GetRegexMatchedTagCollection(text2, out length);
				match = null;
				if (enumerator.MoveNext())
				{
					match = (Match)enumerator.Current;
				}
			}
			float num4 = (float)(length - 1) * num * num2;
			int j = 0;
			int num5 = 0;
			while (j < text2.Length)
			{
				if (supportRichText && match != null && match.Index == j)
				{
					j += match.Length - 1;
					num5--;
					num3 += match.Length;
					match = null;
					if (enumerator.MoveNext())
					{
						match = (Match)enumerator.Current;
					}
				}
				else
				{
					int num6 = num3 * 6;
					if (num6 + 5 > vertexList.Count - 1)
					{
						return;
					}
					CharacterInfo characterInfo;
					if (j - 1 >= 0)
					{
						component.font.GetCharacterInfo(text2[j - 1], out characterInfo, fontSize);
						if (num2 == 0f)
						{
							num4 -= (float)(fontSize - characterInfo.advance);
						}
						else if (num2 == 0.5f)
						{
							num4 -= (float)(fontSize - characterInfo.advance) * 0.5f;
						}
					}
					Vector3 vector = Vector3.right * (num * (float)num5 - num4);
					component.font.GetCharacterInfo(text2[j], out characterInfo, fontSize);
					if (num2 == 0f)
					{
						vector += Vector3.right * (float)(fontSize - characterInfo.advance) * 0.5f;
					}
					else if (num2 == 1f)
					{
						vector += Vector3.left * (float)(fontSize - characterInfo.advance) * 0.5f;
					}
					if ("っッぁぃぅぇぉァィゥェォゃゅょャュョゎヮ".Contains(text2[j]) || "()（）「」『』【】<>＜＞".Contains(text2[j]) || "()<>0123456789,".Contains(text2[j]) || "、。.".Contains(text2[j]))
					{
						vector += Vector3.up * ((float)(fontSize - characterInfo.minY - characterInfo.maxY) / 2f - (float)fontSize * 0.2f);
					}
					if ("っッぁぃぅぇぉァィゥェォゃゅょャュョゎヮ".Contains(text2[j]) || "、。.".Contains(text2[j]))
					{
						vector += Vector3.right * (float)(fontSize - characterInfo.minX - characterInfo.maxX) / 2f;
					}
					if ("（）".Contains(text2[j]))
					{
						vector += Vector3.right * (float)(characterInfo.glyphWidth - characterInfo.minX - characterInfo.maxX) / 2f;
					}
					if ("っッぁぃぅぇぉァィゥェォゃゅょャュョゎヮ".Contains(text2[j]))
					{
						vector += Vector3.right * (float)(fontSize - characterInfo.glyphWidth) / 2f / 2f;
					}
					if ("、。.".Contains(text2[j]))
					{
						vector += new Vector3((float)characterInfo.glyphWidth, (float)characterInfo.glyphHeight, 0f);
					}
					if (",.".Contains(text2[j]))
					{
						vector += new Vector3((float)(-(float)characterInfo.glyphWidth) / 2f, 0f, 0f);
					}
					if ("()<>0123456789,".Contains(text2[j]))
					{
						array2[j] += (float)(fontSize - characterInfo.glyphWidth) * 0.8f;
					}
					char c = text2[j];
					if (c <= '>')
					{
						if (c != ' ')
						{
							if (c != '<')
							{
								if (c == '>')
								{
									vector += Vector3.down * ((float)characterInfo.glyphWidth / 4f);
								}
							}
							else
							{
								vector += Vector3.down * ((float)characterInfo.glyphWidth / 8f);
							}
						}
						else
						{
							array2[j] += (float)fontSize / 2f;
						}
					}
					else
					{
						switch (c)
						{
						case '「':
						case '『':
							vector += Vector3.right * ((float)characterInfo.glyphHeight / 8f);
							vector += Vector3.down * ((float)characterInfo.glyphWidth / 4f);
							break;
						case '」':
						case '』':
							vector += Vector3.left * ((float)characterInfo.glyphHeight / 8f);
							vector += Vector3.up * ((float)characterInfo.glyphWidth / 4f);
							break;
						default:
							if (c == '（' || c == '）')
							{
								vector += Vector3.right * ((float)characterInfo.glyphHeight / 10f);
							}
							break;
						}
					}
					vector += Vector3.up * array2[j];
					for (int k = 0; k < 6; k++)
					{
						UIVertex value = vertexList[num6 + k];
						value.position += vector;
						vertexList[num6 + k] = value;
					}
					if ("…‥‐゠＝=―–-〜ー()（）「」『』【】<>＜＞0123456789,".Contains(text2[j]))
					{
						Vector2 vector2 = Vector2.Lerp(vertexList[num6].position, vertexList[num6 + 3].position, 0.5f);
						for (int l = 0; l < 6; l++)
						{
							UIVertex uivertex = vertexList[num6 + l];
							Vector3 vector3 = uivertex.position - vector2;
							Vector2 a = new Vector2(vector3.x * Mathf.Cos(-1.5707964f) - vector3.y * Mathf.Sin(-1.5707964f), vector3.x * Mathf.Sin(-1.5707964f) + vector3.y * Mathf.Cos(-1.5707964f));
							uivertex.position = a + vector2;
							vertexList[num6 + l] = uivertex;
						}
					}
					num3++;
				}
				j++;
				num5++;
			}
			num4 = 0f;
			int m = text2.Length - 1;
			int num7 = text2.Length - 1;
			while (m >= 0)
			{
				num3--;
				if (num3 * 6 + 5 > vertexList.Count - 1)
				{
					return;
				}
				if (m + 1 < text2.Length)
				{
					CharacterInfo characterInfo2;
					component.font.GetCharacterInfo(text2[m + 1], out characterInfo2, fontSize);
					if (num2 == 1f)
					{
						num4 += (float)(fontSize - characterInfo2.advance);
					}
					else if (num2 == 0.5f)
					{
						num4 += (float)(fontSize - characterInfo2.advance) * 0.5f;
					}
				}
				Vector3 b = Vector3.left * num4;
				int num8 = num3 * 6;
				for (int n = 0; n < 6; n++)
				{
					UIVertex value2 = vertexList[num8 + n];
					value2.position += b;
					vertexList[num8 + n] = value2;
				}
				m--;
				num7--;
			}
			num3 += text2.Length;
			num3++;
		}
	}

	// Token: 0x06000137 RID: 311 RVA: 0x0000D920 File Offset: 0x0000BB20
	public void SetText(string text)
	{
		if (this.text == text)
		{
			return;
		}
		this.text = text;
		text = text.Replace("！？", "!?").Replace("！！", "!!");
		string[] array = text.Split(new char[]
		{
			'\n'
		});
		List<string> list = new List<string>();
		int num = 0;
		for (int i = 0; i < array.Length; i++)
		{
			while (this.MaxLengthPerLine > 0 && array[i].Length > this.MaxLengthPerLine)
			{
				int num2 = this.MaxLengthPerLine;
				if (",)]｝、。）〕〉》」』】〙〗〟’”｠»‐゠–〜ー?!！？‼⁇⁈⁉・:;。.".Contains(array[i][num2]))
				{
					num2++;
				}
				num = Mathf.Max(num, num2);
				list.Add(array[i].Substring(0, num2));
				array[i] = array[i].Substring(num2, array[i].Length - num2);
			}
			if (array[i] != "")
			{
				num = Mathf.Max(num, array[i].Length);
				list.Add(array[i]);
			}
		}
		string[] array2 = new string[num];
		for (int j = 0; j < num; j++)
		{
			string text2 = "";
			for (int k = 0; k < list.Count; k++)
			{
				if (list[k].Length > j)
				{
					if (list[k].Length > j + 1 && list[k][j] == '!')
					{
						text2 = list[k][j].ToString() + list[k][j + 1].ToString() + text2;
						list[k] = list[k].Substring(0, j) + "\u3000";
					}
					else
					{
						text2 = list[k][j].ToString() + text2;
					}
				}
				else
				{
					text2 = "\u3000" + text2;
				}
			}
			array2[j] = text2;
		}
		text = string.Join("\n", array2);
		base.GetComponent<Text>().text = text;
		base.transform.localScale = base.transform.parent.localScale;
		Graphic graphic = base.GetComponent<Graphic>();
		if (graphic != null)
		{
			AppUtil.DelayAction(this, 0f, delegate()
			{
				graphic.SetVerticesDirty();
			}, true);
		}
	}

	// Token: 0x06000138 RID: 312 RVA: 0x0000DBB0 File Offset: 0x0000BDB0
	public VerticalText()
	{
	}

	// Token: 0x04000057 RID: 87
	private const string SupportedTagRegexPattersn = "<b>|</b>|<i>|</i>|<size=.*?>|</size>|<color=.*?>|</color>|<material=.*?>|</material>";

	// Token: 0x04000058 RID: 88
	private const string RotatableCharacters = "…‥‐゠＝=―–-〜ー()（）「」『』【】<>＜＞0123456789,";

	// Token: 0x04000059 RID: 89
	private const string HalfCharacters = "()<>0123456789,";

	// Token: 0x0400005A RID: 90
	private const string VCenterCharacters = "()（）「」『』【】<>＜＞";

	// Token: 0x0400005B RID: 91
	private const string HCenterCharacters = "（）";

	// Token: 0x0400005C RID: 92
	private const string RightTopCharacters = "、。.";

	// Token: 0x0400005D RID: 93
	private const string LeftTopCharacters = ",.";

	// Token: 0x0400005E RID: 94
	private const string SmallCharacters = "っッぁぃぅぇぉァィゥェォゃゅょャュョゎヮ";

	// Token: 0x0400005F RID: 95
	private const string HYP_FRONT = ",)]｝、。）〕〉》」』】〙〗〟’”｠»‐゠–〜ー?!！？‼⁇⁈⁉・:;。.";

	// Token: 0x04000060 RID: 96
	[CompilerGenerated]
	private string <text>k__BackingField;

	// Token: 0x04000061 RID: 97
	[SerializeField]
	private int MaxLengthPerLine;

	// Token: 0x04000062 RID: 98
	[SerializeField]
	private float m_spacing;

	// Token: 0x020000FE RID: 254
	[CompilerGenerated]
	private sealed class <>c__DisplayClass20_0
	{
		// Token: 0x060008D2 RID: 2258 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass20_0()
		{
		}

		// Token: 0x060008D3 RID: 2259 RVA: 0x0002D39E File Offset: 0x0002B59E
		internal void <SetText>b__0()
		{
			this.graphic.SetVerticesDirty();
		}

		// Token: 0x040003D0 RID: 976
		public Graphic graphic;
	}
}
