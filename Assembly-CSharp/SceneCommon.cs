﻿using System;
using System.Collections.Generic;
using System.Linq;
using App;
using UnityEngine;

// Token: 0x02000034 RID: 52
public class SceneCommon : MonoBehaviour
{
	// Token: 0x0600022C RID: 556 RVA: 0x0001243C File Offset: 0x0001063C
	protected virtual void Awake()
	{
		if (base.gameObject.scene.name != "タイトル" && !SceneCommon.IsInitialized)
		{
			this.Initialize();
		}
		SceneCommon.BackButtonList.Clear();
	}

	// Token: 0x0600022D RID: 557 RVA: 0x00012480 File Offset: 0x00010680
	protected virtual void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (SceneCommon.BackButtonList.Count > 0)
			{
				SceneCommon.BackButtonList.Last<OnClickHandler>().Click();
				return;
			}
			new AndroidJavaObject("com.caracolu.appcommon.Util", Array.Empty<object>()).Call("CallHomeScreen", Array.Empty<object>());
		}
	}

	// Token: 0x0600022E RID: 558 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
	protected virtual void FixedUpdate()
	{
	}

	// Token: 0x0600022F RID: 559 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
	protected virtual void LateUpdate()
	{
	}

	// Token: 0x06000230 RID: 560 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
	protected virtual void OnDestroy()
	{
	}

	// Token: 0x06000231 RID: 561 RVA: 0x000124D4 File Offset: 0x000106D4
	protected virtual void Start()
	{
		if (base.gameObject.scene.name == "タイトル" && !SceneCommon.IsInitialized)
		{
			base.Invoke("Initialize", 2f);
		}
	}

	// Token: 0x06000232 RID: 562 RVA: 0x00012518 File Offset: 0x00010718
	protected virtual void Initialize()
	{
		Time.timeScale = Settings.GAME_SPEED;
		GameObject gameObject = new GameObject("AppCommon");
		Object.DontDestroyOnLoad(gameObject);
		foreach (Type componentType in AppInfo.DefaultComponent)
		{
			gameObject.AddComponent(componentType);
		}
		gameObject.AddComponent<GssDataHelper>();
		SceneCommon.IsInitialized = true;
	}

	// Token: 0x06000233 RID: 563 RVA: 0x000025AD File Offset: 0x000007AD
	public SceneCommon()
	{
	}

	// Token: 0x06000234 RID: 564 RVA: 0x0001256D File Offset: 0x0001076D
	// Note: this type is marked as 'beforefieldinit'.
	static SceneCommon()
	{
	}

	// Token: 0x040000BC RID: 188
	protected static bool IsInitialized = false;

	// Token: 0x040000BD RID: 189
	public static List<OnClickHandler> BackButtonList = new List<OnClickHandler>();
}
