﻿using System;
using System.Collections.Generic;
using App;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x0200000F RID: 15
public class GlobalButton : MonoBehaviour
{
	// Token: 0x0600006A RID: 106 RVA: 0x00007ADC File Offset: 0x00005CDC
	private void OnEnable()
	{
		for (int i = 1; i <= Data.PRIZE_DATA.Count; i++)
		{
			this.PrizeList.Add(new PrizeItem(i));
		}
		this.UpdateButtonStatus();
	}

	// Token: 0x0600006B RID: 107 RVA: 0x00007B15 File Offset: 0x00005D15
	private void FixedUpdate()
	{
		this.UpdateNotice();
	}

	// Token: 0x0600006C RID: 108 RVA: 0x00007B20 File Offset: 0x00005D20
	public void UpdateButtonStatus()
	{
		foreach (Button button in base.GetComponentsInChildren<Button>())
		{
			bool flag = button.name.Contains(base.gameObject.scene.name);
			bool flag2 = false;
			string name = button.name;
			if (name == "GlobalButton記録室" || name == "GlobalButton目標達成")
			{
				flag2 = (PlayerStatus.TutorialLv <= PlayerStatus.TUTORIAL_MAX);
			}
			if (flag)
			{
				button.transform.Find("Icon").gameObject.SetActive(true);
				button.transform.Find("Text").gameObject.SetActive(true);
				button.transform.Find("Text").GetComponent<Text>().color = GlobalButton.COLOR_ON;
				button.transform.Find("Light").gameObject.SetActive(true);
				button.GetComponent<Button>().enabled = false;
				button.GetComponent<Image>().color = GlobalButton.COLOR_ON;
			}
			else if (flag2)
			{
				button.transform.Find("Icon").gameObject.SetActive(false);
				button.transform.Find("Text").gameObject.SetActive(false);
				button.transform.Find("Light").gameObject.SetActive(false);
				button.GetComponent<Button>().enabled = false;
				button.GetComponent<Image>().color = GlobalButton.COLOR_LOCK;
			}
			else
			{
				button.transform.Find("Icon").gameObject.SetActive(true);
				button.transform.Find("Text").gameObject.SetActive(true);
				button.transform.Find("Text").GetComponent<Text>().color = GlobalButton.COLOR_OFF;
				button.transform.Find("Light").gameObject.SetActive(false);
				button.GetComponent<Button>().enabled = true;
				button.GetComponent<Image>().color = GlobalButton.COLOR_OFF;
			}
		}
	}

	// Token: 0x0600006D RID: 109 RVA: 0x00007D40 File Offset: 0x00005F40
	private void UpdateNotice()
	{
		GameObject gameObject = GameObject.Find("GlobalButtonエスの部屋");
		bool flag;
		EgoPoint egoPoint;
		if (PlayerStatus.ScenarioNo.Contains("4章AE"))
		{
			flag = PlayerStatus.EnableDailyBonus;
			if (!flag)
			{
				bool flag2 = TimeManager.IsInTime(TimeManager.TYPE.END_BOOK);
				egoPoint = Data.NEXT_HAYAKAWA_BOOK_PRICE;
				flag = (!flag2 && PlayerStatus.EgoPoint >= egoPoint);
			}
		}
		else if (PlayerStatus.ScenarioNo.Contains("4章"))
		{
			flag = true;
		}
		else
		{
			egoPoint = Data.NEXT_SCENARIO_PRICE;
			flag = (egoPoint != null && PlayerStatus.EgoPoint >= egoPoint && Utility.EsExists());
		}
		gameObject.transform.Find("Notice").gameObject.SetActive(flag);
		if (PlayerStatus.TutorialLv <= PlayerStatus.TUTORIAL_MAX)
		{
			return;
		}
		gameObject = GameObject.Find("GlobalButton探求");
		egoPoint = Data.NEXT_BOOK_PRICE;
		gameObject.transform.Find("Notice").gameObject.SetActive(egoPoint != null && PlayerStatus.EgoPoint >= egoPoint);
		gameObject = GameObject.Find("GlobalButton目標達成");
		bool active = false;
		using (List<PrizeItem>.Enumerator enumerator = this.PrizeList.GetEnumerator())
		{
			while (enumerator.MoveNext())
			{
				if (enumerator.Current.IsEnableButton())
				{
					active = true;
					break;
				}
			}
		}
		gameObject.transform.Find("Notice").gameObject.SetActive(active);
		gameObject = GameObject.Find("GlobalButton記録室");
		bool active2 = CounselingResult.IsNew("ゲームクリア");
		gameObject.transform.Find("Notice").gameObject.SetActive(active2);
	}

	// Token: 0x0600006E RID: 110 RVA: 0x00007ED0 File Offset: 0x000060D0
	public PrizeItem GetPrizeItem(int num)
	{
		return this.PrizeList[num - 1];
	}

	// Token: 0x0600006F RID: 111 RVA: 0x00007EE0 File Offset: 0x000060E0
	public GlobalButton()
	{
	}

	// Token: 0x06000070 RID: 112 RVA: 0x00007EF3 File Offset: 0x000060F3
	// Note: this type is marked as 'beforefieldinit'.
	static GlobalButton()
	{
	}

	// Token: 0x04000019 RID: 25
	private static readonly Color COLOR_ON = Color.white;

	// Token: 0x0400001A RID: 26
	private static readonly Color COLOR_OFF = new Color(0.627451f, 0.627451f, 0.627451f);

	// Token: 0x0400001B RID: 27
	private static readonly Color COLOR_LOCK = new Color(0.3137255f, 0.3137255f, 0.3137255f);

	// Token: 0x0400001C RID: 28
	private List<PrizeItem> PrizeList = new List<PrizeItem>();
}
