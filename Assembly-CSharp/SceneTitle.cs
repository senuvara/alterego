﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using App;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000020 RID: 32
public class SceneTitle : SceneBase
{
	// Token: 0x060000FF RID: 255 RVA: 0x0000BE1C File Offset: 0x0000A01C
	protected override void Awake()
	{
		base.Awake();
		AppUtil.SetAlpha(this.StartMessage, 0f);
		AppUtil.SetAlpha(base.transform.Find("Header/VersionText"), 0f);
		AppUtil.SetAlpha(base.transform.Find("Header/UserIDText"), 0f);
		this.StartScreen.enabled = false;
		this.LoadingMessage.SetActive(false);
		this.MoveDataButton.SetActive(false);
	}

	// Token: 0x06000100 RID: 256 RVA: 0x0000BE9C File Offset: 0x0000A09C
	protected override void Start()
	{
		base.Start();
		base.StartCoroutine(this.RenderTitle());
		base.transform.Find("Header/VersionText").GetComponent<Text>().text = "ver. " + BuildInfo.APP_VERSION;
		if (!this.SetUserID())
		{
			base.StartCoroutine(this.UpdateUserID());
		}
		base.StartCoroutine(AppUtil.FadeIn(base.transform.Find("Header/VersionText"), 0.5f, null));
		base.StartCoroutine(AppUtil.FadeIn(base.transform.Find("Header/UserIDText").GetComponent<Text>(), 0.5f, null));
	}

	// Token: 0x06000101 RID: 257 RVA: 0x0000BF43 File Offset: 0x0000A143
	private IEnumerator UpdateUserID()
	{
		yield return SaveDataManager.SetUserID();
		this.SetUserID();
		yield break;
	}

	// Token: 0x06000102 RID: 258 RVA: 0x0000BF54 File Offset: 0x0000A154
	private bool SetUserID()
	{
		string text = Settings.SaveDataID;
		bool result = true;
		if (string.IsNullOrEmpty(text))
		{
			text = "";
			result = false;
		}
		base.transform.Find("Header/UserIDText").GetComponent<Text>().text = SceneTitle.USERID_HEADER + text;
		return result;
	}

	// Token: 0x06000103 RID: 259 RVA: 0x0000BF9F File Offset: 0x0000A19F
	private IEnumerator RenderTitle()
	{
		this.TitleLogo.GetComponent<Animator>().enabled = true;
		float length = this.TitleLogo.GetComponent<Animator>().GetCurrentAnimatorClipInfo(0)[0].clip.length;
		yield return AppUtil.Wait(length * 0.8f);
		this.LoadingMessage.SetActive(true);
		while (!SceneCommon.IsInitialized)
		{
			yield return null;
		}
		if (PlayerResult.Ending.Contains("AE"))
		{
			Utility.PromptReview("AEクリア");
		}
		this.LoadingMessage.SetActive(false);
		this.MoveDataButton.SetActive(true);
		yield return AppUtil.FadeIn(this.StartMessage, 0.5f, null);
		global::Debug.Log("AutoTestEvent:Screenshot");
		this.StartScreen.enabled = true;
		for (;;)
		{
			yield return AppUtil.Wait(1.2f);
			yield return AppUtil.FadeOut(this.StartMessage, 0.3f, null);
			yield return AppUtil.Wait(0.6f);
			yield return AppUtil.FadeIn(this.StartMessage, 0.3f, null);
		}
		yield break;
	}

	// Token: 0x06000104 RID: 260 RVA: 0x0000BFB0 File Offset: 0x0000A1B0
	public new void OnClick(GameObject clickObject)
	{
		if (base.OnClick(clickObject))
		{
			return;
		}
		global::Debug.Log("OnClick " + clickObject.name);
		string name = clickObject.name;
		if (!(name == "StartScreen"))
		{
			if (name == "UserIDText")
			{
				AnalyticsManager.SendEvent(new string[]
				{
					"",
					"",
					""
				}, "1Jyb1Gc-XDgXGFebWkWco-tO56PVYTeS9GJ24x8ONO8Q");
				if (AppUtil.SetClipBoard(base.transform.Find("Header/UserIDText").GetComponent<Text>().text.Replace(SceneTitle.USERID_HEADER, "")))
				{
					base.transform.Find("Header/CopyToast/CopyText").GetComponent<TextLocalization>().SetKey("[UI]CopyToast/CopyText");
				}
				else
				{
					base.transform.Find("Header/CopyToast/CopyText").GetComponent<TextLocalization>().SetKey("[UI]CopyToast/CopyTextFailed");
				}
				base.StopCoroutine("DisplayToast");
				base.StartCoroutine("DisplayToast");
				return;
			}
			if (!(name == "MoveDataButton"))
			{
				return;
			}
			DialogManager.ShowDialog("MoveDataDialog", Array.Empty<object>());
		}
		else
		{
			SceneTransition.LoadScene("探求", null, 0.5f);
			if (!PlayerPrefs.HasKey("ScenarioNo"))
			{
				PlayerStatus.TutorialLv = 0;
				PlayerStatus.ScenarioNo = PlayerStatus.SCENARIO_NO_LIST[0];
				return;
			}
		}
	}

	// Token: 0x06000105 RID: 261 RVA: 0x0000C107 File Offset: 0x0000A307
	private IEnumerator DisplayToast()
	{
		yield return AppUtil.FadeIn(base.transform.Find("Header/CopyToast"), 0.5f, null);
		yield return AppUtil.Wait(2f);
		yield return AppUtil.FadeOut(base.transform.Find("Header/CopyToast"), 0.5f, null);
		yield break;
	}

	// Token: 0x06000106 RID: 262 RVA: 0x00009F09 File Offset: 0x00008109
	public SceneTitle()
	{
	}

	// Token: 0x06000107 RID: 263 RVA: 0x0000C116 File Offset: 0x0000A316
	// Note: this type is marked as 'beforefieldinit'.
	static SceneTitle()
	{
	}

	// Token: 0x0400004B RID: 75
	private static string USERID_HEADER = "User ID : ";

	// Token: 0x0400004C RID: 76
	public GameObject TitleLogo;

	// Token: 0x0400004D RID: 77
	public GameObject LoadingMessage;

	// Token: 0x0400004E RID: 78
	public GameObject StartMessage;

	// Token: 0x0400004F RID: 79
	public GameObject MoveDataButton;

	// Token: 0x04000050 RID: 80
	public Button StartScreen;

	// Token: 0x020000F9 RID: 249
	[CompilerGenerated]
	private sealed class <UpdateUserID>d__8 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060008BA RID: 2234 RVA: 0x0002CD5A File Offset: 0x0002AF5A
		[DebuggerHidden]
		public <UpdateUserID>d__8(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060008BB RID: 2235 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060008BC RID: 2236 RVA: 0x0002CD6C File Offset: 0x0002AF6C
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			SceneTitle sceneTitle = this;
			if (num == 0)
			{
				this.<>1__state = -1;
				this.<>2__current = SaveDataManager.SetUserID();
				this.<>1__state = 1;
				return true;
			}
			if (num != 1)
			{
				return false;
			}
			this.<>1__state = -1;
			sceneTitle.SetUserID();
			return false;
		}

		// Token: 0x1700010E RID: 270
		// (get) Token: 0x060008BD RID: 2237 RVA: 0x0002CDBA File Offset: 0x0002AFBA
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060008BE RID: 2238 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x1700010F RID: 271
		// (get) Token: 0x060008BF RID: 2239 RVA: 0x0002CDBA File Offset: 0x0002AFBA
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040003B8 RID: 952
		private int <>1__state;

		// Token: 0x040003B9 RID: 953
		private object <>2__current;

		// Token: 0x040003BA RID: 954
		public SceneTitle <>4__this;
	}

	// Token: 0x020000FA RID: 250
	[CompilerGenerated]
	private sealed class <RenderTitle>d__10 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060008C0 RID: 2240 RVA: 0x0002CDC2 File Offset: 0x0002AFC2
		[DebuggerHidden]
		public <RenderTitle>d__10(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060008C1 RID: 2241 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060008C2 RID: 2242 RVA: 0x0002CDD4 File Offset: 0x0002AFD4
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			SceneTitle sceneTitle = this;
			switch (num)
			{
			case 0:
			{
				this.<>1__state = -1;
				sceneTitle.TitleLogo.GetComponent<Animator>().enabled = true;
				float length = sceneTitle.TitleLogo.GetComponent<Animator>().GetCurrentAnimatorClipInfo(0)[0].clip.length;
				this.<>2__current = AppUtil.Wait(length * 0.8f);
				this.<>1__state = 1;
				return true;
			}
			case 1:
				this.<>1__state = -1;
				sceneTitle.LoadingMessage.SetActive(true);
				break;
			case 2:
				this.<>1__state = -1;
				break;
			case 3:
				this.<>1__state = -1;
				global::Debug.Log("AutoTestEvent:Screenshot");
				sceneTitle.StartScreen.enabled = true;
				goto IL_12E;
			case 4:
				this.<>1__state = -1;
				this.<>2__current = AppUtil.FadeOut(sceneTitle.StartMessage, 0.3f, null);
				this.<>1__state = 5;
				return true;
			case 5:
				this.<>1__state = -1;
				this.<>2__current = AppUtil.Wait(0.6f);
				this.<>1__state = 6;
				return true;
			case 6:
				this.<>1__state = -1;
				this.<>2__current = AppUtil.FadeIn(sceneTitle.StartMessage, 0.3f, null);
				this.<>1__state = 7;
				return true;
			case 7:
				this.<>1__state = -1;
				goto IL_12E;
			default:
				return false;
			}
			if (SceneCommon.IsInitialized)
			{
				if (PlayerResult.Ending.Contains("AE"))
				{
					Utility.PromptReview("AEクリア");
				}
				sceneTitle.LoadingMessage.SetActive(false);
				sceneTitle.MoveDataButton.SetActive(true);
				this.<>2__current = AppUtil.FadeIn(sceneTitle.StartMessage, 0.5f, null);
				this.<>1__state = 3;
				return true;
			}
			this.<>2__current = null;
			this.<>1__state = 2;
			return true;
			IL_12E:
			this.<>2__current = AppUtil.Wait(1.2f);
			this.<>1__state = 4;
			return true;
		}

		// Token: 0x17000110 RID: 272
		// (get) Token: 0x060008C3 RID: 2243 RVA: 0x0002CFA1 File Offset: 0x0002B1A1
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060008C4 RID: 2244 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000111 RID: 273
		// (get) Token: 0x060008C5 RID: 2245 RVA: 0x0002CFA1 File Offset: 0x0002B1A1
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040003BB RID: 955
		private int <>1__state;

		// Token: 0x040003BC RID: 956
		private object <>2__current;

		// Token: 0x040003BD RID: 957
		public SceneTitle <>4__this;
	}

	// Token: 0x020000FB RID: 251
	[CompilerGenerated]
	private sealed class <DisplayToast>d__12 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060008C6 RID: 2246 RVA: 0x0002CFA9 File Offset: 0x0002B1A9
		[DebuggerHidden]
		public <DisplayToast>d__12(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060008C7 RID: 2247 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060008C8 RID: 2248 RVA: 0x0002CFB8 File Offset: 0x0002B1B8
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			SceneTitle sceneTitle = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				this.<>2__current = AppUtil.FadeIn(sceneTitle.transform.Find("Header/CopyToast"), 0.5f, null);
				this.<>1__state = 1;
				return true;
			case 1:
				this.<>1__state = -1;
				this.<>2__current = AppUtil.Wait(2f);
				this.<>1__state = 2;
				return true;
			case 2:
				this.<>1__state = -1;
				this.<>2__current = AppUtil.FadeOut(sceneTitle.transform.Find("Header/CopyToast"), 0.5f, null);
				this.<>1__state = 3;
				return true;
			case 3:
				this.<>1__state = -1;
				return false;
			default:
				return false;
			}
		}

		// Token: 0x17000112 RID: 274
		// (get) Token: 0x060008C9 RID: 2249 RVA: 0x0002D075 File Offset: 0x0002B275
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060008CA RID: 2250 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000113 RID: 275
		// (get) Token: 0x060008CB RID: 2251 RVA: 0x0002D075 File Offset: 0x0002B275
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040003BE RID: 958
		private int <>1__state;

		// Token: 0x040003BF RID: 959
		private object <>2__current;

		// Token: 0x040003C0 RID: 960
		public SceneTitle <>4__this;
	}
}
