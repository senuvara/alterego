﻿using System;
using UnityEngine;

// Token: 0x02000033 RID: 51
public class SafeAreaAdjuster : MonoBehaviour
{
	// Token: 0x1700001F RID: 31
	// (get) Token: 0x06000218 RID: 536 RVA: 0x00011F7D File Offset: 0x0001017D
	// (set) Token: 0x06000219 RID: 537 RVA: 0x00011F84 File Offset: 0x00010184
	public static int SafeAreaHeight
	{
		get
		{
			return SafeAreaAdjuster._SafeAreaHeight;
		}
		set
		{
			SafeAreaAdjuster._SafeAreaHeight = value;
			global::Debug.Log("SafeAreaHeight=" + value);
		}
	}

	// Token: 0x17000020 RID: 32
	// (get) Token: 0x0600021A RID: 538 RVA: 0x00011FA1 File Offset: 0x000101A1
	// (set) Token: 0x0600021B RID: 539 RVA: 0x00011FA8 File Offset: 0x000101A8
	public static Rect BottomBannerRect
	{
		get
		{
			return SafeAreaAdjuster._BottomBannerRect;
		}
		set
		{
			SafeAreaAdjuster._BottomBannerRect = value;
			SafeAreaAdjuster.UpdateLayout();
		}
	}

	// Token: 0x0600021C RID: 540 RVA: 0x00011FB8 File Offset: 0x000101B8
	private void Start()
	{
		this.SizeOrg = base.GetComponent<RectTransform>().sizeDelta;
		this.PositionOrg = base.GetComponent<RectTransform>().anchoredPosition;
		SafeAreaAdjuster.OnSizeChanged = (SafeAreaAdjuster.OnAction)Delegate.Combine(SafeAreaAdjuster.OnSizeChanged, new SafeAreaAdjuster.OnAction(this.AdjustLayout));
		SafeAreaAdjuster.UpdateLayout();
	}

	// Token: 0x0600021D RID: 541 RVA: 0x0001200C File Offset: 0x0001020C
	private void OnDestroy()
	{
		SafeAreaAdjuster.OnSizeChanged = (SafeAreaAdjuster.OnAction)Delegate.Remove(SafeAreaAdjuster.OnSizeChanged, new SafeAreaAdjuster.OnAction(this.AdjustLayout));
	}

	// Token: 0x0600021E RID: 542 RVA: 0x00012030 File Offset: 0x00010230
	private void AdjustLayout(float bannerHeight, float navBarHeight)
	{
		Vector2 sizeOrg = this.SizeOrg;
		Vector2 positionOrg = this.PositionOrg;
		float num = SafeAreaAdjuster.PixelToCanvas(SafeAreaAdjuster.GetSafeAreaTop(), true);
		num = Mathf.Max(0f, num);
		float num2 = SafeAreaAdjuster.PixelToCanvas(SafeAreaAdjuster.GetSafeAreaBottom(), true) + navBarHeight;
		num2 = Mathf.Max(0f, num2);
		if (this.ShowAd)
		{
			num2 += bannerHeight;
		}
		SafeAreaAdjuster.FooterSize = num2;
		switch (this.Type)
		{
		case SafeAreaAdjuster.TYPE.EXPAND_TOP:
			sizeOrg.y += num;
			base.GetComponent<RectTransform>().sizeDelta = sizeOrg;
			return;
		case SafeAreaAdjuster.TYPE.SHRINK_Y:
			sizeOrg.y -= num + num2;
			positionOrg.y -= (num - num2) / 2f;
			base.GetComponent<RectTransform>().sizeDelta = sizeOrg;
			base.GetComponent<RectTransform>().anchoredPosition = positionOrg;
			return;
		case SafeAreaAdjuster.TYPE.MOVE_DOWN:
			positionOrg.y -= num;
			base.GetComponent<RectTransform>().anchoredPosition = positionOrg;
			return;
		case SafeAreaAdjuster.TYPE.MOVE_UP:
			positionOrg.y += num2;
			base.GetComponent<RectTransform>().anchoredPosition = positionOrg;
			return;
		case SafeAreaAdjuster.TYPE.EXPAND_BOTTOM:
			sizeOrg.y += num2;
			base.GetComponent<RectTransform>().sizeDelta = sizeOrg;
			return;
		case SafeAreaAdjuster.TYPE.SHRINK_TOP:
			sizeOrg.y -= num;
			positionOrg.y -= num / 2f;
			base.GetComponent<RectTransform>().sizeDelta = sizeOrg;
			base.GetComponent<RectTransform>().anchoredPosition = positionOrg;
			return;
		case SafeAreaAdjuster.TYPE.SHRINK_BOTTOM:
			sizeOrg.y -= num2;
			positionOrg.y += num2 / 2f;
			base.GetComponent<RectTransform>().sizeDelta = sizeOrg;
			base.GetComponent<RectTransform>().anchoredPosition = positionOrg;
			return;
		default:
			return;
		}
	}

	// Token: 0x0600021F RID: 543 RVA: 0x000121CC File Offset: 0x000103CC
	public static Rect GetSafeArea()
	{
		Rect safeArea = Screen.safeArea;
		if (SafeAreaAdjuster.SafeArea != safeArea)
		{
			SafeAreaAdjuster.SafeArea = safeArea;
			global::Debug.Log(string.Concat(new string[]
			{
				"SafeArea: \nScreenSize=",
				SafeAreaAdjuster.GetScreenSize().ToString(),
				"\nUnity SafeArea=",
				Screen.safeArea.ToString(),
				"\nEGO SafeArea=",
				safeArea.ToString()
			}));
		}
		return SafeAreaAdjuster.SafeArea;
	}

	// Token: 0x06000220 RID: 544 RVA: 0x0001225C File Offset: 0x0001045C
	public static float GetSafeAreaTop()
	{
		return SafeAreaAdjuster.GetSafeArea().y;
	}

	// Token: 0x06000221 RID: 545 RVA: 0x00012278 File Offset: 0x00010478
	public static float GetSafeAreaBottom()
	{
		Rect safeArea = SafeAreaAdjuster.GetSafeArea();
		return SafeAreaAdjuster.GetScreenSize().y - safeArea.y - safeArea.height;
	}

	// Token: 0x06000222 RID: 546 RVA: 0x000122A5 File Offset: 0x000104A5
	public static float GetFooterSize()
	{
		return SafeAreaAdjuster.FooterSize;
	}

	// Token: 0x06000223 RID: 547 RVA: 0x000122AC File Offset: 0x000104AC
	public static void UpdateLayout()
	{
		if (SafeAreaAdjuster.OnSizeChanged != null)
		{
			float size = new AndroidJavaObject("com.caracolu.appcommon.Util", Array.Empty<object>()).Call<float>("GetNavigationBarSize", Array.Empty<object>());
			SafeAreaAdjuster.OnSizeChanged(SafeAreaAdjuster.BottomBannerRect.height, size);
		}
	}

	// Token: 0x06000224 RID: 548 RVA: 0x000122F7 File Offset: 0x000104F7
	public static Vector2 PixelToCanvas(Vector2 pixel)
	{
		pixel.x /= SafeAreaAdjuster.GetPixelRate().x;
		pixel.y /= SafeAreaAdjuster.GetPixelRate().y;
		return pixel;
	}

	// Token: 0x06000225 RID: 549 RVA: 0x00012324 File Offset: 0x00010524
	public static float PixelToCanvas(float pixel, bool height = true)
	{
		if (height)
		{
			return pixel / SafeAreaAdjuster.GetPixelRate().y;
		}
		return pixel / SafeAreaAdjuster.GetPixelRate().x;
	}

	// Token: 0x06000226 RID: 550 RVA: 0x00012342 File Offset: 0x00010542
	public static Vector2 CanvasToPixel(Vector2 canvas)
	{
		canvas.x *= SafeAreaAdjuster.GetPixelRate().x;
		canvas.y *= SafeAreaAdjuster.GetPixelRate().y;
		return canvas;
	}

	// Token: 0x06000227 RID: 551 RVA: 0x0001236F File Offset: 0x0001056F
	public static int CanvasToPixel(float canvas, bool height = true)
	{
		if (height)
		{
			return (int)(canvas * SafeAreaAdjuster.GetPixelRate().y);
		}
		return (int)(canvas * SafeAreaAdjuster.GetPixelRate().x);
	}

	// Token: 0x06000228 RID: 552 RVA: 0x0001238F File Offset: 0x0001058F
	public static Vector2 GetScreenSize()
	{
		return new Vector2((float)Screen.width, (float)Screen.height);
	}

	// Token: 0x06000229 RID: 553 RVA: 0x000123A4 File Offset: 0x000105A4
	private static Vector2 GetPixelRate()
	{
		float num = 0.5625f;
		float x = SafeAreaAdjuster.GetScreenSize().x;
		float y = SafeAreaAdjuster.GetScreenSize().y;
		float num2 = x / y;
		Vector2 one = Vector2.one;
		if (num2 < num)
		{
			one.y = x / 1080f;
			one.x = x / 1080f;
		}
		else
		{
			one.x = y / 1920f;
			one.y = y / 1920f;
		}
		return one;
	}

	// Token: 0x0600022A RID: 554 RVA: 0x00012414 File Offset: 0x00010614
	public SafeAreaAdjuster()
	{
	}

	// Token: 0x0600022B RID: 555 RVA: 0x00012423 File Offset: 0x00010623
	// Note: this type is marked as 'beforefieldinit'.
	static SafeAreaAdjuster()
	{
	}

	// Token: 0x040000B3 RID: 179
	private Vector2 SizeOrg;

	// Token: 0x040000B4 RID: 180
	private Vector2 PositionOrg;

	// Token: 0x040000B5 RID: 181
	[SerializeField]
	private bool ShowAd = true;

	// Token: 0x040000B6 RID: 182
	private static int _SafeAreaHeight;

	// Token: 0x040000B7 RID: 183
	private static float FooterSize;

	// Token: 0x040000B8 RID: 184
	private static Rect SafeArea = Rect.zero;

	// Token: 0x040000B9 RID: 185
	public static Rect _BottomBannerRect = default(Rect);

	// Token: 0x040000BA RID: 186
	public static SafeAreaAdjuster.OnAction OnSizeChanged;

	// Token: 0x040000BB RID: 187
	[SerializeField]
	private SafeAreaAdjuster.TYPE Type;

	// Token: 0x0200012D RID: 301
	private enum TYPE
	{
		// Token: 0x040004AA RID: 1194
		NONE,
		// Token: 0x040004AB RID: 1195
		EXPAND_TOP,
		// Token: 0x040004AC RID: 1196
		SHRINK_Y,
		// Token: 0x040004AD RID: 1197
		MOVE_DOWN,
		// Token: 0x040004AE RID: 1198
		MOVE_UP,
		// Token: 0x040004AF RID: 1199
		EXPAND_BOTTOM,
		// Token: 0x040004B0 RID: 1200
		SHRINK_TOP,
		// Token: 0x040004B1 RID: 1201
		SHRINK_BOTTOM
	}

	// Token: 0x0200012E RID: 302
	// (Invoke) Token: 0x0600099B RID: 2459
	public delegate void OnAction(float size1, float size2);
}
