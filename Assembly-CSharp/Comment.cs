﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using App;
using UnityEngine;

// Token: 0x02000007 RID: 7
public class Comment : MonoBehaviour
{
	// Token: 0x06000018 RID: 24 RVA: 0x00002C0A File Offset: 0x00000E0A
	private void Awake()
	{
		this.gameManager = base.GetComponentInParent<SceneBook>();
	}

	// Token: 0x06000019 RID: 25 RVA: 0x00002C18 File Offset: 0x00000E18
	private void Start()
	{
		base.GetComponent<Collider2D>().enabled = false;
		base.StartCoroutine(AppUtil.FadeIn(base.gameObject, 0.5f, null));
		if (this.Type == "Butterfly")
		{
			return;
		}
		string text = LanguageManager.Get(this.CommentKey);
		if (this.Type != "ID")
		{
			text = string.Concat(new object[]
			{
				"<size=",
				(float)base.GetComponentInChildren<TextMesh>().fontSize * 1.3f,
				">",
				text.Substring(0, 1),
				"</size>",
				text.Substring(1, text.Length - 1)
			});
		}
		TextLocalization[] componentsInChildren = base.GetComponentsInChildren<TextLocalization>(true);
		for (int i = 0; i < componentsInChildren.Length; i++)
		{
			componentsInChildren[i].SetText(text);
		}
		base.transform.Find("Filter").gameObject.SetActive(true);
		if (!ReadFukidasiList.Contains(this.CommentKey))
		{
			this.NewEffect = base.transform.Find("NewEffect").gameObject;
		}
		if (this.Type == "ID")
		{
			base.InvokeRepeating("ArrangeText", 0f, 0.84f);
		}
	}

	// Token: 0x0600001A RID: 26 RVA: 0x00002D64 File Offset: 0x00000F64
	public bool UpdateStatus(bool isFirst, bool noWalking)
	{
		int num = (int)base.transform.position.z;
		int num2 = (int)(-base.transform.position.z * 10f) + 100;
		base.GetComponent<SpriteRenderer>().sortingOrder = num2;
		MeshRenderer[] componentsInChildren = base.GetComponentsInChildren<MeshRenderer>();
		for (int i = 0; i < componentsInChildren.Length; i++)
		{
			componentsInChildren[i].sortingOrder = num2 + 2;
		}
		if (this.Type == "SE")
		{
			base.transform.Find("MessageTextShadow").GetComponent<MeshRenderer>().sortingOrder = num2 + 1;
		}
		base.transform.Find("Filter").GetComponent<SpriteRenderer>().sortingOrder = num2 + 3;
		base.GetComponent<SpriteMask>().frontSortingOrder = num2 + 5;
		base.GetComponent<SpriteMask>().backSortingOrder = num2;
		if (noWalking && (float)num <= -2f)
		{
			base.StartCoroutine("DestroyAfterFadeOut");
			return false;
		}
		if (!isFirst)
		{
			return true;
		}
		if (num < 7)
		{
			base.GetComponent<Collider2D>().enabled = true;
			base.transform.Find("Filter").gameObject.SetActive(false);
			this.gameManager.SetTutorial(2, false);
			if (this.NewEffect != null)
			{
				bool active = !ReadFukidasiList.Contains(this.CommentKey);
				this.NewEffect.SetActive(active);
				this.NewEffect.GetComponent<SpriteRenderer>().sortingOrder = num2 + 4;
			}
		}
		if (noWalking && (float)num <= 1f && this.Type != "Butterfly")
		{
			base.gameObject.name = "CloseComment" + this.Type;
		}
		return true;
	}

	// Token: 0x0600001B RID: 27 RVA: 0x00002F00 File Offset: 0x00001100
	private void OnMouseUpAsButton()
	{
		this.gameManager.OnClick(base.gameObject);
		base.GetComponent<Collider2D>().enabled = false;
		AudioManager.PlaySound(new string[]
		{
			"Click",
			"",
			base.gameObject.name
		});
		base.StopCoroutine("DestroyAfterFadeOut");
		base.transform.SetParent(base.transform.parent.parent);
		if (this.Type != "Butterfly")
		{
			base.StartCoroutine("DestroyAfterCrash");
			return;
		}
		base.StartCoroutine("DestroyAfterFadeOut");
	}

	// Token: 0x0600001C RID: 28 RVA: 0x00002FA4 File Offset: 0x000011A4
	private IEnumerator DestroyAfterCrash()
	{
		base.GetComponent<SpriteMask>().enabled = false;
		base.GetComponent<Animator>().enabled = true;
		base.transform.localScale = Vector3.one * 0.975f;
		if (this.Type == "ID")
		{
			base.StartCoroutine(AppUtil.MoveEasingFloat(1f, 0f, delegate(float value)
			{
				AppUtil.SetAlphaChildren(base.transform.Find("MessageGroup").gameObject, value);
			}, true, 0.3f, EasingFunction.Ease.EaseInSine, null));
			base.StartCoroutine(AppUtil.ShakeObject(base.gameObject, 0.25f));
		}
		yield return AppUtil.WaitAnimation(base.gameObject, null);
		Object.Destroy(base.gameObject);
		yield break;
	}

	// Token: 0x0600001D RID: 29 RVA: 0x00002FB3 File Offset: 0x000011B3
	public IEnumerator DestroyAfterFadeOut()
	{
		base.GetComponent<SpriteMask>().enabled = false;
		for (float f = 1f; f >= 0f; f -= 0.1f)
		{
			if (f <= 0.3f)
			{
				base.GetComponent<Collider2D>().enabled = false;
			}
			AppUtil.SetAlphaChildren(base.gameObject, f);
			yield return AppUtil.Wait(0.01f);
		}
		Object.Destroy(base.gameObject);
		yield break;
	}

	// Token: 0x0600001E RID: 30 RVA: 0x00002FC4 File Offset: 0x000011C4
	public void ArrangeText()
	{
		if (base.transform.Find("MessageGroup") != null)
		{
			Object.Destroy(base.transform.Find("MessageGroup").gameObject);
		}
		GameObject gameObject = new GameObject("MessageGroup");
		gameObject.transform.parent = base.transform;
		GameObject gameObject2 = base.transform.Find("MessageText").gameObject;
		string text = gameObject2.GetComponent<TextMesh>().text;
		gameObject2.GetComponent<MeshRenderer>().sortingOrder = base.GetComponent<SpriteRenderer>().sortingOrder + 1;
		gameObject2.SetActive(true);
		Vector3 zero = Vector3.zero;
		Vector3 vector = zero;
		float num = 0f;
		float y = 0.1f;
		foreach (char c in text.ToCharArray())
		{
			if (c == '\n')
			{
				num = vector.x;
				y = 0.35f;
				vector = zero - new Vector3(0f, 0.57199997f, 0f);
			}
			else
			{
				GameObject gameObject3 = Object.Instantiate<GameObject>(gameObject2, gameObject.transform);
				gameObject3.transform.localPosition = vector;
				gameObject3.transform.localPosition += new Vector3(0f, Random.Range(-0.05f, 0.05f), 0f);
				gameObject3.transform.localScale = Vector3.one * Random.Range(0.9f, 1.1f);
				gameObject3.transform.Rotate(new Vector3(0f, 0f, (float)Random.Range(-10, 10)));
				string font = LanguageManager.FONTDATA_TABLE.Keys.ToArray<string>()[Random.Range(0, LanguageManager.FONTDATA_TABLE.Keys.Count)];
				if (Random.Range(0f, 1f) >= 0.5f)
				{
					font = "kokumin";
				}
				float num2 = 0.42f;
				if (!Comment.IsKanji(c))
				{
					num2 *= 0.75f;
				}
				if (vector == zero)
				{
					font = "kokumin";
					gameObject3.GetComponent<TextMesh>().fontSize = (int)((float)gameObject3.GetComponent<TextMesh>().fontSize * 1.3f);
					vector += new Vector3(num2 * 1.3f, 0f, 0f);
				}
				else
				{
					vector += new Vector3(num2, 0f, 0f);
				}
				gameObject3.GetComponent<TextLocalization>().SetText(c.ToString());
				gameObject3.GetComponent<TextLocalization>().SetFont(font);
			}
		}
		num = Mathf.Max(vector.x, num);
		gameObject.transform.localPosition = new Vector3(-num / 2f, y, 0f);
		gameObject2.SetActive(false);
	}

	// Token: 0x0600001F RID: 31 RVA: 0x00003294 File Offset: 0x00001494
	public static bool IsKanji(char c)
	{
		return ('一' <= c && c <= '鿏') || ('豈' <= c && c <= '﫿') || ('㐀' <= c && c <= '䶿');
	}

	// Token: 0x06000020 RID: 32 RVA: 0x000025AD File Offset: 0x000007AD
	public Comment()
	{
	}

	// Token: 0x06000021 RID: 33 RVA: 0x000032CD File Offset: 0x000014CD
	[CompilerGenerated]
	private void <DestroyAfterCrash>b__9_0(float value)
	{
		AppUtil.SetAlphaChildren(base.transform.Find("MessageGroup").gameObject, value);
	}

	// Token: 0x04000009 RID: 9
	public string CommentKey;

	// Token: 0x0400000A RID: 10
	public string Type;

	// Token: 0x0400000B RID: 11
	public bool IsHyperActive;

	// Token: 0x0400000C RID: 12
	private SceneBook gameManager;

	// Token: 0x0400000D RID: 13
	private GameObject NewEffect;

	// Token: 0x020000B7 RID: 183
	[CompilerGenerated]
	private sealed class <DestroyAfterCrash>d__9 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x0600079C RID: 1948 RVA: 0x00026A61 File Offset: 0x00024C61
		[DebuggerHidden]
		public <DestroyAfterCrash>d__9(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x0600079D RID: 1949 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x0600079E RID: 1950 RVA: 0x00026A70 File Offset: 0x00024C70
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			Comment comment = this;
			if (num == 0)
			{
				this.<>1__state = -1;
				comment.GetComponent<SpriteMask>().enabled = false;
				comment.GetComponent<Animator>().enabled = true;
				comment.transform.localScale = Vector3.one * 0.975f;
				if (comment.Type == "ID")
				{
					comment.StartCoroutine(AppUtil.MoveEasingFloat(1f, 0f, delegate(float value)
					{
						AppUtil.SetAlphaChildren(base.transform.Find("MessageGroup").gameObject, value);
					}, true, 0.3f, EasingFunction.Ease.EaseInSine, null));
					comment.StartCoroutine(AppUtil.ShakeObject(comment.gameObject, 0.25f));
				}
				this.<>2__current = AppUtil.WaitAnimation(comment.gameObject, null);
				this.<>1__state = 1;
				return true;
			}
			if (num != 1)
			{
				return false;
			}
			this.<>1__state = -1;
			Object.Destroy(comment.gameObject);
			return false;
		}

		// Token: 0x170000CC RID: 204
		// (get) Token: 0x0600079F RID: 1951 RVA: 0x00026B52 File Offset: 0x00024D52
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060007A0 RID: 1952 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000CD RID: 205
		// (get) Token: 0x060007A1 RID: 1953 RVA: 0x00026B52 File Offset: 0x00024D52
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040002A7 RID: 679
		private int <>1__state;

		// Token: 0x040002A8 RID: 680
		private object <>2__current;

		// Token: 0x040002A9 RID: 681
		public Comment <>4__this;
	}

	// Token: 0x020000B8 RID: 184
	[CompilerGenerated]
	private sealed class <DestroyAfterFadeOut>d__10 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060007A2 RID: 1954 RVA: 0x00026B5A File Offset: 0x00024D5A
		[DebuggerHidden]
		public <DestroyAfterFadeOut>d__10(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060007A3 RID: 1955 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060007A4 RID: 1956 RVA: 0x00026B6C File Offset: 0x00024D6C
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			Comment comment = this;
			if (num != 0)
			{
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				f -= 0.1f;
			}
			else
			{
				this.<>1__state = -1;
				comment.GetComponent<SpriteMask>().enabled = false;
				f = 1f;
			}
			if (f < 0f)
			{
				Object.Destroy(comment.gameObject);
				return false;
			}
			if (f <= 0.3f)
			{
				comment.GetComponent<Collider2D>().enabled = false;
			}
			AppUtil.SetAlphaChildren(comment.gameObject, f);
			this.<>2__current = AppUtil.Wait(0.01f);
			this.<>1__state = 1;
			return true;
		}

		// Token: 0x170000CE RID: 206
		// (get) Token: 0x060007A5 RID: 1957 RVA: 0x00026C25 File Offset: 0x00024E25
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060007A6 RID: 1958 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000CF RID: 207
		// (get) Token: 0x060007A7 RID: 1959 RVA: 0x00026C25 File Offset: 0x00024E25
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040002AA RID: 682
		private int <>1__state;

		// Token: 0x040002AB RID: 683
		private object <>2__current;

		// Token: 0x040002AC RID: 684
		public Comment <>4__this;

		// Token: 0x040002AD RID: 685
		private float <f>5__2;
	}
}
