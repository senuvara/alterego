﻿using System;
using System.Collections.Generic;
using GoogleMobileAds.Common;
using UnityEngine;

namespace GoogleMobileAds.Api
{
	// Token: 0x02000091 RID: 145
	public class CustomNativeTemplateAd
	{
		// Token: 0x06000660 RID: 1632 RVA: 0x000228E9 File Offset: 0x00020AE9
		internal CustomNativeTemplateAd(ICustomNativeTemplateClient client)
		{
			this.client = client;
		}

		// Token: 0x06000661 RID: 1633 RVA: 0x000228F8 File Offset: 0x00020AF8
		public List<string> GetAvailableAssetNames()
		{
			return this.client.GetAvailableAssetNames();
		}

		// Token: 0x06000662 RID: 1634 RVA: 0x00022905 File Offset: 0x00020B05
		public string GetCustomTemplateId()
		{
			return this.client.GetTemplateId();
		}

		// Token: 0x06000663 RID: 1635 RVA: 0x00022914 File Offset: 0x00020B14
		public Texture2D GetTexture2D(string key)
		{
			byte[] imageByteArray = this.client.GetImageByteArray(key);
			if (imageByteArray == null)
			{
				return null;
			}
			return Utils.GetTexture2DFromByteArray(imageByteArray);
		}

		// Token: 0x06000664 RID: 1636 RVA: 0x00022939 File Offset: 0x00020B39
		public string GetText(string key)
		{
			return this.client.GetText(key);
		}

		// Token: 0x06000665 RID: 1637 RVA: 0x00022947 File Offset: 0x00020B47
		public void PerformClick(string assetName)
		{
			this.client.PerformClick(assetName);
		}

		// Token: 0x06000666 RID: 1638 RVA: 0x00022955 File Offset: 0x00020B55
		public void RecordImpression()
		{
			this.client.RecordImpression();
		}

		// Token: 0x04000214 RID: 532
		private ICustomNativeTemplateClient client;
	}
}
