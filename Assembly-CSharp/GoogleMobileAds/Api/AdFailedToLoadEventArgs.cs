﻿using System;
using System.Runtime.CompilerServices;

namespace GoogleMobileAds.Api
{
	// Token: 0x02000089 RID: 137
	public class AdFailedToLoadEventArgs : EventArgs
	{
		// Token: 0x17000074 RID: 116
		// (get) Token: 0x06000616 RID: 1558 RVA: 0x00022023 File Offset: 0x00020223
		// (set) Token: 0x06000617 RID: 1559 RVA: 0x0002202B File Offset: 0x0002022B
		public string Message
		{
			[CompilerGenerated]
			get
			{
				return this.<Message>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Message>k__BackingField = value;
			}
		}

		// Token: 0x06000618 RID: 1560 RVA: 0x0002201B File Offset: 0x0002021B
		public AdFailedToLoadEventArgs()
		{
		}

		// Token: 0x040001E9 RID: 489
		[CompilerGenerated]
		private string <Message>k__BackingField;
	}
}
