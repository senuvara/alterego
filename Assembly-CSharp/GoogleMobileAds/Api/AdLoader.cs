﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using GoogleMobileAds.Common;

namespace GoogleMobileAds.Api
{
	// Token: 0x0200008B RID: 139
	public class AdLoader
	{
		// Token: 0x06000619 RID: 1561 RVA: 0x00022034 File Offset: 0x00020234
		private AdLoader(AdLoader.Builder builder)
		{
			this.AdUnitId = string.Copy(builder.AdUnitId);
			this.CustomNativeTemplateClickHandlers = new Dictionary<string, Action<CustomNativeTemplateAd, string>>(builder.CustomNativeTemplateClickHandlers);
			this.TemplateIds = new HashSet<string>(builder.TemplateIds);
			this.AdTypes = new HashSet<NativeAdType>(builder.AdTypes);
			MethodInfo method = Type.GetType("GoogleMobileAds.GoogleMobileAdsClientFactory,Assembly-CSharp").GetMethod("BuildAdLoaderClient", BindingFlags.Static | BindingFlags.Public);
			this.adLoaderClient = (IAdLoaderClient)method.Invoke(null, new object[]
			{
				this
			});
			Utils.CheckInitialization();
			this.adLoaderClient.OnCustomNativeTemplateAdLoaded += delegate(object sender, CustomNativeEventArgs args)
			{
				this.OnCustomNativeTemplateAdLoaded(this, args);
			};
			this.adLoaderClient.OnAdFailedToLoad += delegate(object sender, AdFailedToLoadEventArgs args)
			{
				if (this.OnAdFailedToLoad != null)
				{
					this.OnAdFailedToLoad(this, args);
				}
			};
		}

		// Token: 0x14000045 RID: 69
		// (add) Token: 0x0600061A RID: 1562 RVA: 0x000220F4 File Offset: 0x000202F4
		// (remove) Token: 0x0600061B RID: 1563 RVA: 0x0002212C File Offset: 0x0002032C
		public event EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad
		{
			[CompilerGenerated]
			add
			{
				EventHandler<AdFailedToLoadEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdFailedToLoadEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdFailedToLoadEventArgs> value2 = (EventHandler<AdFailedToLoadEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdFailedToLoadEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<AdFailedToLoadEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdFailedToLoadEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdFailedToLoadEventArgs> value2 = (EventHandler<AdFailedToLoadEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdFailedToLoadEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000046 RID: 70
		// (add) Token: 0x0600061C RID: 1564 RVA: 0x00022164 File Offset: 0x00020364
		// (remove) Token: 0x0600061D RID: 1565 RVA: 0x0002219C File Offset: 0x0002039C
		public event EventHandler<CustomNativeEventArgs> OnCustomNativeTemplateAdLoaded
		{
			[CompilerGenerated]
			add
			{
				EventHandler<CustomNativeEventArgs> eventHandler = this.OnCustomNativeTemplateAdLoaded;
				EventHandler<CustomNativeEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<CustomNativeEventArgs> value2 = (EventHandler<CustomNativeEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<CustomNativeEventArgs>>(ref this.OnCustomNativeTemplateAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<CustomNativeEventArgs> eventHandler = this.OnCustomNativeTemplateAdLoaded;
				EventHandler<CustomNativeEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<CustomNativeEventArgs> value2 = (EventHandler<CustomNativeEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<CustomNativeEventArgs>>(ref this.OnCustomNativeTemplateAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x0600061E RID: 1566 RVA: 0x000221D1 File Offset: 0x000203D1
		// (set) Token: 0x0600061F RID: 1567 RVA: 0x000221D9 File Offset: 0x000203D9
		public Dictionary<string, Action<CustomNativeTemplateAd, string>> CustomNativeTemplateClickHandlers
		{
			[CompilerGenerated]
			get
			{
				return this.<CustomNativeTemplateClickHandlers>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<CustomNativeTemplateClickHandlers>k__BackingField = value;
			}
		}

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x06000620 RID: 1568 RVA: 0x000221E2 File Offset: 0x000203E2
		// (set) Token: 0x06000621 RID: 1569 RVA: 0x000221EA File Offset: 0x000203EA
		public string AdUnitId
		{
			[CompilerGenerated]
			get
			{
				return this.<AdUnitId>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<AdUnitId>k__BackingField = value;
			}
		}

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x06000622 RID: 1570 RVA: 0x000221F3 File Offset: 0x000203F3
		// (set) Token: 0x06000623 RID: 1571 RVA: 0x000221FB File Offset: 0x000203FB
		public HashSet<NativeAdType> AdTypes
		{
			[CompilerGenerated]
			get
			{
				return this.<AdTypes>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<AdTypes>k__BackingField = value;
			}
		}

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x06000624 RID: 1572 RVA: 0x00022204 File Offset: 0x00020404
		// (set) Token: 0x06000625 RID: 1573 RVA: 0x0002220C File Offset: 0x0002040C
		public HashSet<string> TemplateIds
		{
			[CompilerGenerated]
			get
			{
				return this.<TemplateIds>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<TemplateIds>k__BackingField = value;
			}
		}

		// Token: 0x06000626 RID: 1574 RVA: 0x00022215 File Offset: 0x00020415
		public void LoadAd(AdRequest request)
		{
			this.adLoaderClient.LoadAd(request);
		}

		// Token: 0x06000627 RID: 1575 RVA: 0x00022223 File Offset: 0x00020423
		[CompilerGenerated]
		private void <.ctor>b__1_0(object sender, CustomNativeEventArgs args)
		{
			this.OnCustomNativeTemplateAdLoaded(this, args);
		}

		// Token: 0x06000628 RID: 1576 RVA: 0x00022232 File Offset: 0x00020432
		[CompilerGenerated]
		private void <.ctor>b__1_1(object sender, AdFailedToLoadEventArgs args)
		{
			if (this.OnAdFailedToLoad != null)
			{
				this.OnAdFailedToLoad(this, args);
			}
		}

		// Token: 0x040001EC RID: 492
		private IAdLoaderClient adLoaderClient;

		// Token: 0x040001ED RID: 493
		[CompilerGenerated]
		private EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad;

		// Token: 0x040001EE RID: 494
		[CompilerGenerated]
		private EventHandler<CustomNativeEventArgs> OnCustomNativeTemplateAdLoaded;

		// Token: 0x040001EF RID: 495
		[CompilerGenerated]
		private Dictionary<string, Action<CustomNativeTemplateAd, string>> <CustomNativeTemplateClickHandlers>k__BackingField;

		// Token: 0x040001F0 RID: 496
		[CompilerGenerated]
		private string <AdUnitId>k__BackingField;

		// Token: 0x040001F1 RID: 497
		[CompilerGenerated]
		private HashSet<NativeAdType> <AdTypes>k__BackingField;

		// Token: 0x040001F2 RID: 498
		[CompilerGenerated]
		private HashSet<string> <TemplateIds>k__BackingField;

		// Token: 0x0200015B RID: 347
		public class Builder
		{
			// Token: 0x06000A41 RID: 2625 RVA: 0x00031188 File Offset: 0x0002F388
			public Builder(string adUnitId)
			{
				this.AdUnitId = adUnitId;
				this.AdTypes = new HashSet<NativeAdType>();
				this.TemplateIds = new HashSet<string>();
				this.CustomNativeTemplateClickHandlers = new Dictionary<string, Action<CustomNativeTemplateAd, string>>();
			}

			// Token: 0x17000160 RID: 352
			// (get) Token: 0x06000A42 RID: 2626 RVA: 0x000311B8 File Offset: 0x0002F3B8
			// (set) Token: 0x06000A43 RID: 2627 RVA: 0x000311C0 File Offset: 0x0002F3C0
			internal string AdUnitId
			{
				[CompilerGenerated]
				get
				{
					return this.<AdUnitId>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<AdUnitId>k__BackingField = value;
				}
			}

			// Token: 0x17000161 RID: 353
			// (get) Token: 0x06000A44 RID: 2628 RVA: 0x000311C9 File Offset: 0x0002F3C9
			// (set) Token: 0x06000A45 RID: 2629 RVA: 0x000311D1 File Offset: 0x0002F3D1
			internal HashSet<NativeAdType> AdTypes
			{
				[CompilerGenerated]
				get
				{
					return this.<AdTypes>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<AdTypes>k__BackingField = value;
				}
			}

			// Token: 0x17000162 RID: 354
			// (get) Token: 0x06000A46 RID: 2630 RVA: 0x000311DA File Offset: 0x0002F3DA
			// (set) Token: 0x06000A47 RID: 2631 RVA: 0x000311E2 File Offset: 0x0002F3E2
			internal HashSet<string> TemplateIds
			{
				[CompilerGenerated]
				get
				{
					return this.<TemplateIds>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<TemplateIds>k__BackingField = value;
				}
			}

			// Token: 0x17000163 RID: 355
			// (get) Token: 0x06000A48 RID: 2632 RVA: 0x000311EB File Offset: 0x0002F3EB
			// (set) Token: 0x06000A49 RID: 2633 RVA: 0x000311F3 File Offset: 0x0002F3F3
			internal Dictionary<string, Action<CustomNativeTemplateAd, string>> CustomNativeTemplateClickHandlers
			{
				[CompilerGenerated]
				get
				{
					return this.<CustomNativeTemplateClickHandlers>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<CustomNativeTemplateClickHandlers>k__BackingField = value;
				}
			}

			// Token: 0x06000A4A RID: 2634 RVA: 0x000311FC File Offset: 0x0002F3FC
			public AdLoader.Builder ForCustomNativeAd(string templateId)
			{
				this.TemplateIds.Add(templateId);
				this.AdTypes.Add(NativeAdType.CustomTemplate);
				return this;
			}

			// Token: 0x06000A4B RID: 2635 RVA: 0x00031219 File Offset: 0x0002F419
			public AdLoader.Builder ForCustomNativeAd(string templateId, Action<CustomNativeTemplateAd, string> callback)
			{
				this.TemplateIds.Add(templateId);
				this.CustomNativeTemplateClickHandlers[templateId] = callback;
				this.AdTypes.Add(NativeAdType.CustomTemplate);
				return this;
			}

			// Token: 0x06000A4C RID: 2636 RVA: 0x00031243 File Offset: 0x0002F443
			public AdLoader Build()
			{
				return new AdLoader(this);
			}

			// Token: 0x04000534 RID: 1332
			[CompilerGenerated]
			private string <AdUnitId>k__BackingField;

			// Token: 0x04000535 RID: 1333
			[CompilerGenerated]
			private HashSet<NativeAdType> <AdTypes>k__BackingField;

			// Token: 0x04000536 RID: 1334
			[CompilerGenerated]
			private HashSet<string> <TemplateIds>k__BackingField;

			// Token: 0x04000537 RID: 1335
			[CompilerGenerated]
			private Dictionary<string, Action<CustomNativeTemplateAd, string>> <CustomNativeTemplateClickHandlers>k__BackingField;
		}
	}
}
