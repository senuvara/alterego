﻿using System;
using System.Runtime.CompilerServices;

namespace GoogleMobileAds.Api
{
	// Token: 0x02000090 RID: 144
	public class CustomNativeEventArgs : EventArgs
	{
		// Token: 0x17000083 RID: 131
		// (get) Token: 0x0600065D RID: 1629 RVA: 0x000228D8 File Offset: 0x00020AD8
		// (set) Token: 0x0600065E RID: 1630 RVA: 0x000228E0 File Offset: 0x00020AE0
		public CustomNativeTemplateAd nativeAd
		{
			[CompilerGenerated]
			get
			{
				return this.<nativeAd>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<nativeAd>k__BackingField = value;
			}
		}

		// Token: 0x0600065F RID: 1631 RVA: 0x0002201B File Offset: 0x0002021B
		public CustomNativeEventArgs()
		{
		}

		// Token: 0x04000213 RID: 531
		[CompilerGenerated]
		private CustomNativeTemplateAd <nativeAd>k__BackingField;
	}
}
