﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace GoogleMobileAds.Api.Mediation
{
	// Token: 0x02000098 RID: 152
	public abstract class MediationExtras
	{
		// Token: 0x17000087 RID: 135
		// (get) Token: 0x060006BF RID: 1727 RVA: 0x0002374C File Offset: 0x0002194C
		// (set) Token: 0x060006C0 RID: 1728 RVA: 0x00023754 File Offset: 0x00021954
		public Dictionary<string, string> Extras
		{
			[CompilerGenerated]
			get
			{
				return this.<Extras>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<Extras>k__BackingField = value;
			}
		}

		// Token: 0x060006C1 RID: 1729 RVA: 0x0002375D File Offset: 0x0002195D
		public MediationExtras()
		{
			this.Extras = new Dictionary<string, string>();
		}

		// Token: 0x17000088 RID: 136
		// (get) Token: 0x060006C2 RID: 1730
		public abstract string AndroidMediationExtraBuilderClassName { get; }

		// Token: 0x17000089 RID: 137
		// (get) Token: 0x060006C3 RID: 1731
		public abstract string IOSMediationExtraBuilderClassName { get; }

		// Token: 0x04000233 RID: 563
		[CompilerGenerated]
		private Dictionary<string, string> <Extras>k__BackingField;
	}
}
