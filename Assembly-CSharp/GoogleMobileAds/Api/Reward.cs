﻿using System;
using System.Runtime.CompilerServices;

namespace GoogleMobileAds.Api
{
	// Token: 0x02000095 RID: 149
	public class Reward : EventArgs
	{
		// Token: 0x17000084 RID: 132
		// (get) Token: 0x06000683 RID: 1667 RVA: 0x00022D6B File Offset: 0x00020F6B
		// (set) Token: 0x06000684 RID: 1668 RVA: 0x00022D73 File Offset: 0x00020F73
		public string Type
		{
			[CompilerGenerated]
			get
			{
				return this.<Type>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Type>k__BackingField = value;
			}
		}

		// Token: 0x17000085 RID: 133
		// (get) Token: 0x06000685 RID: 1669 RVA: 0x00022D7C File Offset: 0x00020F7C
		// (set) Token: 0x06000686 RID: 1670 RVA: 0x00022D84 File Offset: 0x00020F84
		public double Amount
		{
			[CompilerGenerated]
			get
			{
				return this.<Amount>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Amount>k__BackingField = value;
			}
		}

		// Token: 0x06000687 RID: 1671 RVA: 0x0002201B File Offset: 0x0002021B
		public Reward()
		{
		}

		// Token: 0x04000220 RID: 544
		[CompilerGenerated]
		private string <Type>k__BackingField;

		// Token: 0x04000221 RID: 545
		[CompilerGenerated]
		private double <Amount>k__BackingField;
	}
}
