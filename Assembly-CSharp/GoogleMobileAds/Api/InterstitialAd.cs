﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using GoogleMobileAds.Common;

namespace GoogleMobileAds.Api
{
	// Token: 0x02000093 RID: 147
	public class InterstitialAd
	{
		// Token: 0x06000667 RID: 1639 RVA: 0x00022964 File Offset: 0x00020B64
		public InterstitialAd(string adUnitId)
		{
			MethodInfo method = Type.GetType("GoogleMobileAds.GoogleMobileAdsClientFactory,Assembly-CSharp").GetMethod("BuildInterstitialClient", BindingFlags.Static | BindingFlags.Public);
			this.client = (IInterstitialClient)method.Invoke(null, null);
			this.client.CreateInterstitialAd(adUnitId);
			this.client.OnAdLoaded += delegate(object sender, EventArgs args)
			{
				if (this.OnAdLoaded != null)
				{
					this.OnAdLoaded(this, args);
				}
			};
			this.client.OnAdFailedToLoad += delegate(object sender, AdFailedToLoadEventArgs args)
			{
				if (this.OnAdFailedToLoad != null)
				{
					this.OnAdFailedToLoad(this, args);
				}
			};
			this.client.OnAdOpening += delegate(object sender, EventArgs args)
			{
				if (this.OnAdOpening != null)
				{
					this.OnAdOpening(this, args);
				}
			};
			this.client.OnAdClosed += delegate(object sender, EventArgs args)
			{
				if (this.OnAdClosed != null)
				{
					this.OnAdClosed(this, args);
				}
			};
			this.client.OnAdLeavingApplication += delegate(object sender, EventArgs args)
			{
				if (this.OnAdLeavingApplication != null)
				{
					this.OnAdLeavingApplication(this, args);
				}
			};
		}

		// Token: 0x1400004C RID: 76
		// (add) Token: 0x06000668 RID: 1640 RVA: 0x00022A20 File Offset: 0x00020C20
		// (remove) Token: 0x06000669 RID: 1641 RVA: 0x00022A58 File Offset: 0x00020C58
		public event EventHandler<EventArgs> OnAdLoaded
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLoaded;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLoaded;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x1400004D RID: 77
		// (add) Token: 0x0600066A RID: 1642 RVA: 0x00022A90 File Offset: 0x00020C90
		// (remove) Token: 0x0600066B RID: 1643 RVA: 0x00022AC8 File Offset: 0x00020CC8
		public event EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad
		{
			[CompilerGenerated]
			add
			{
				EventHandler<AdFailedToLoadEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdFailedToLoadEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdFailedToLoadEventArgs> value2 = (EventHandler<AdFailedToLoadEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdFailedToLoadEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<AdFailedToLoadEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdFailedToLoadEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdFailedToLoadEventArgs> value2 = (EventHandler<AdFailedToLoadEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdFailedToLoadEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x1400004E RID: 78
		// (add) Token: 0x0600066C RID: 1644 RVA: 0x00022B00 File Offset: 0x00020D00
		// (remove) Token: 0x0600066D RID: 1645 RVA: 0x00022B38 File Offset: 0x00020D38
		public event EventHandler<EventArgs> OnAdOpening
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdOpening;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdOpening, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdOpening;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdOpening, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x1400004F RID: 79
		// (add) Token: 0x0600066E RID: 1646 RVA: 0x00022B70 File Offset: 0x00020D70
		// (remove) Token: 0x0600066F RID: 1647 RVA: 0x00022BA8 File Offset: 0x00020DA8
		public event EventHandler<EventArgs> OnAdClosed
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdClosed;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdClosed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdClosed;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdClosed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000050 RID: 80
		// (add) Token: 0x06000670 RID: 1648 RVA: 0x00022BE0 File Offset: 0x00020DE0
		// (remove) Token: 0x06000671 RID: 1649 RVA: 0x00022C18 File Offset: 0x00020E18
		public event EventHandler<EventArgs> OnAdLeavingApplication
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLeavingApplication;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLeavingApplication, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLeavingApplication;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLeavingApplication, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x06000672 RID: 1650 RVA: 0x00022C4D File Offset: 0x00020E4D
		public void LoadAd(AdRequest request)
		{
			this.client.LoadAd(request);
		}

		// Token: 0x06000673 RID: 1651 RVA: 0x00022C5B File Offset: 0x00020E5B
		public bool IsLoaded()
		{
			return this.client.IsLoaded();
		}

		// Token: 0x06000674 RID: 1652 RVA: 0x00022C68 File Offset: 0x00020E68
		public void Show()
		{
			this.client.ShowInterstitial();
		}

		// Token: 0x06000675 RID: 1653 RVA: 0x00022C75 File Offset: 0x00020E75
		public void Destroy()
		{
			this.client.DestroyInterstitial();
		}

		// Token: 0x06000676 RID: 1654 RVA: 0x00022C82 File Offset: 0x00020E82
		public string MediationAdapterClassName()
		{
			return this.client.MediationAdapterClassName();
		}

		// Token: 0x06000677 RID: 1655 RVA: 0x00022C8F File Offset: 0x00020E8F
		[CompilerGenerated]
		private void <.ctor>b__1_0(object sender, EventArgs args)
		{
			if (this.OnAdLoaded != null)
			{
				this.OnAdLoaded(this, args);
			}
		}

		// Token: 0x06000678 RID: 1656 RVA: 0x00022CA6 File Offset: 0x00020EA6
		[CompilerGenerated]
		private void <.ctor>b__1_1(object sender, AdFailedToLoadEventArgs args)
		{
			if (this.OnAdFailedToLoad != null)
			{
				this.OnAdFailedToLoad(this, args);
			}
		}

		// Token: 0x06000679 RID: 1657 RVA: 0x00022CBD File Offset: 0x00020EBD
		[CompilerGenerated]
		private void <.ctor>b__1_2(object sender, EventArgs args)
		{
			if (this.OnAdOpening != null)
			{
				this.OnAdOpening(this, args);
			}
		}

		// Token: 0x0600067A RID: 1658 RVA: 0x00022CD4 File Offset: 0x00020ED4
		[CompilerGenerated]
		private void <.ctor>b__1_3(object sender, EventArgs args)
		{
			if (this.OnAdClosed != null)
			{
				this.OnAdClosed(this, args);
			}
		}

		// Token: 0x0600067B RID: 1659 RVA: 0x00022CEB File Offset: 0x00020EEB
		[CompilerGenerated]
		private void <.ctor>b__1_4(object sender, EventArgs args)
		{
			if (this.OnAdLeavingApplication != null)
			{
				this.OnAdLeavingApplication(this, args);
			}
		}

		// Token: 0x04000219 RID: 537
		private IInterstitialClient client;

		// Token: 0x0400021A RID: 538
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdLoaded;

		// Token: 0x0400021B RID: 539
		[CompilerGenerated]
		private EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad;

		// Token: 0x0400021C RID: 540
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdOpening;

		// Token: 0x0400021D RID: 541
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdClosed;

		// Token: 0x0400021E RID: 542
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdLeavingApplication;
	}
}
