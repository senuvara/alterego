﻿using System;
using System.Reflection;
using GoogleMobileAds.Common;

namespace GoogleMobileAds.Api
{
	// Token: 0x02000094 RID: 148
	public class MobileAds
	{
		// Token: 0x0600067C RID: 1660 RVA: 0x00022D02 File Offset: 0x00020F02
		public static void Initialize(string appId)
		{
			MobileAds.client.Initialize(appId);
			MobileAdsEventExecutor.Initialize();
		}

		// Token: 0x0600067D RID: 1661 RVA: 0x00022D14 File Offset: 0x00020F14
		public static void SetApplicationMuted(bool muted)
		{
			MobileAds.client.SetApplicationMuted(muted);
		}

		// Token: 0x0600067E RID: 1662 RVA: 0x00022D21 File Offset: 0x00020F21
		public static void SetApplicationVolume(float volume)
		{
			MobileAds.client.SetApplicationVolume(volume);
		}

		// Token: 0x0600067F RID: 1663 RVA: 0x00022D2E File Offset: 0x00020F2E
		public static void SetiOSAppPauseOnBackground(bool pause)
		{
			MobileAds.client.SetiOSAppPauseOnBackground(pause);
		}

		// Token: 0x06000680 RID: 1664 RVA: 0x00022D3B File Offset: 0x00020F3B
		private static IMobileAdsClient GetMobileAdsClient()
		{
			return (IMobileAdsClient)Type.GetType("GoogleMobileAds.GoogleMobileAdsClientFactory,Assembly-CSharp").GetMethod("MobileAdsInstance", BindingFlags.Static | BindingFlags.Public).Invoke(null, null);
		}

		// Token: 0x06000681 RID: 1665 RVA: 0x000043CE File Offset: 0x000025CE
		public MobileAds()
		{
		}

		// Token: 0x06000682 RID: 1666 RVA: 0x00022D5F File Offset: 0x00020F5F
		// Note: this type is marked as 'beforefieldinit'.
		static MobileAds()
		{
		}

		// Token: 0x0400021F RID: 543
		private static readonly IMobileAdsClient client = MobileAds.GetMobileAdsClient();
	}
}
