﻿using System;

namespace GoogleMobileAds.Api
{
	// Token: 0x0200008C RID: 140
	public enum AdPosition
	{
		// Token: 0x040001F4 RID: 500
		Top,
		// Token: 0x040001F5 RID: 501
		Bottom,
		// Token: 0x040001F6 RID: 502
		TopLeft,
		// Token: 0x040001F7 RID: 503
		TopRight,
		// Token: 0x040001F8 RID: 504
		BottomLeft,
		// Token: 0x040001F9 RID: 505
		BottomRight,
		// Token: 0x040001FA RID: 506
		Center
	}
}
