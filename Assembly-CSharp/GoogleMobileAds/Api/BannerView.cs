﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using GoogleMobileAds.Common;

namespace GoogleMobileAds.Api
{
	// Token: 0x0200008F RID: 143
	public class BannerView
	{
		// Token: 0x06000642 RID: 1602 RVA: 0x00022494 File Offset: 0x00020694
		public BannerView(string adUnitId, AdSize adSize, AdPosition position)
		{
			MethodInfo method = Type.GetType("GoogleMobileAds.GoogleMobileAdsClientFactory,Assembly-CSharp").GetMethod("BuildBannerClient", BindingFlags.Static | BindingFlags.Public);
			this.client = (IBannerClient)method.Invoke(null, null);
			this.client.CreateBannerView(adUnitId, adSize, position);
			this.ConfigureBannerEvents();
		}

		// Token: 0x06000643 RID: 1603 RVA: 0x000224E8 File Offset: 0x000206E8
		public BannerView(string adUnitId, AdSize adSize, int x, int y)
		{
			MethodInfo method = Type.GetType("GoogleMobileAds.GoogleMobileAdsClientFactory,Assembly-CSharp").GetMethod("BuildBannerClient", BindingFlags.Static | BindingFlags.Public);
			this.client = (IBannerClient)method.Invoke(null, null);
			this.client.CreateBannerView(adUnitId, adSize, x, y);
			this.ConfigureBannerEvents();
		}

		// Token: 0x14000047 RID: 71
		// (add) Token: 0x06000644 RID: 1604 RVA: 0x0002253C File Offset: 0x0002073C
		// (remove) Token: 0x06000645 RID: 1605 RVA: 0x00022574 File Offset: 0x00020774
		public event EventHandler<EventArgs> OnAdLoaded
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLoaded;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLoaded;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000048 RID: 72
		// (add) Token: 0x06000646 RID: 1606 RVA: 0x000225AC File Offset: 0x000207AC
		// (remove) Token: 0x06000647 RID: 1607 RVA: 0x000225E4 File Offset: 0x000207E4
		public event EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad
		{
			[CompilerGenerated]
			add
			{
				EventHandler<AdFailedToLoadEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdFailedToLoadEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdFailedToLoadEventArgs> value2 = (EventHandler<AdFailedToLoadEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdFailedToLoadEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<AdFailedToLoadEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdFailedToLoadEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdFailedToLoadEventArgs> value2 = (EventHandler<AdFailedToLoadEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdFailedToLoadEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000049 RID: 73
		// (add) Token: 0x06000648 RID: 1608 RVA: 0x0002261C File Offset: 0x0002081C
		// (remove) Token: 0x06000649 RID: 1609 RVA: 0x00022654 File Offset: 0x00020854
		public event EventHandler<EventArgs> OnAdOpening
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdOpening;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdOpening, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdOpening;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdOpening, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x1400004A RID: 74
		// (add) Token: 0x0600064A RID: 1610 RVA: 0x0002268C File Offset: 0x0002088C
		// (remove) Token: 0x0600064B RID: 1611 RVA: 0x000226C4 File Offset: 0x000208C4
		public event EventHandler<EventArgs> OnAdClosed
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdClosed;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdClosed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdClosed;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdClosed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x1400004B RID: 75
		// (add) Token: 0x0600064C RID: 1612 RVA: 0x000226FC File Offset: 0x000208FC
		// (remove) Token: 0x0600064D RID: 1613 RVA: 0x00022734 File Offset: 0x00020934
		public event EventHandler<EventArgs> OnAdLeavingApplication
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLeavingApplication;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLeavingApplication, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLeavingApplication;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLeavingApplication, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x0600064E RID: 1614 RVA: 0x00022769 File Offset: 0x00020969
		public void LoadAd(AdRequest request)
		{
			this.client.LoadAd(request);
		}

		// Token: 0x0600064F RID: 1615 RVA: 0x00022777 File Offset: 0x00020977
		public void Hide()
		{
			this.client.HideBannerView();
		}

		// Token: 0x06000650 RID: 1616 RVA: 0x00022784 File Offset: 0x00020984
		public void Show()
		{
			this.client.ShowBannerView();
		}

		// Token: 0x06000651 RID: 1617 RVA: 0x00022791 File Offset: 0x00020991
		public void Destroy()
		{
			this.client.DestroyBannerView();
		}

		// Token: 0x06000652 RID: 1618 RVA: 0x0002279E File Offset: 0x0002099E
		public float GetHeightInPixels()
		{
			return this.client.GetHeightInPixels();
		}

		// Token: 0x06000653 RID: 1619 RVA: 0x000227AB File Offset: 0x000209AB
		public float GetWidthInPixels()
		{
			return this.client.GetWidthInPixels();
		}

		// Token: 0x06000654 RID: 1620 RVA: 0x000227B8 File Offset: 0x000209B8
		public void SetPosition(AdPosition adPosition)
		{
			this.client.SetPosition(adPosition);
		}

		// Token: 0x06000655 RID: 1621 RVA: 0x000227C6 File Offset: 0x000209C6
		public void SetPosition(int x, int y)
		{
			this.client.SetPosition(x, y);
		}

		// Token: 0x06000656 RID: 1622 RVA: 0x000227D8 File Offset: 0x000209D8
		private void ConfigureBannerEvents()
		{
			this.client.OnAdLoaded += delegate(object sender, EventArgs args)
			{
				if (this.OnAdLoaded != null)
				{
					this.OnAdLoaded(this, args);
				}
			};
			this.client.OnAdFailedToLoad += delegate(object sender, AdFailedToLoadEventArgs args)
			{
				if (this.OnAdFailedToLoad != null)
				{
					this.OnAdFailedToLoad(this, args);
				}
			};
			this.client.OnAdOpening += delegate(object sender, EventArgs args)
			{
				if (this.OnAdOpening != null)
				{
					this.OnAdOpening(this, args);
				}
			};
			this.client.OnAdClosed += delegate(object sender, EventArgs args)
			{
				if (this.OnAdClosed != null)
				{
					this.OnAdClosed(this, args);
				}
			};
			this.client.OnAdLeavingApplication += delegate(object sender, EventArgs args)
			{
				if (this.OnAdLeavingApplication != null)
				{
					this.OnAdLeavingApplication(this, args);
				}
			};
		}

		// Token: 0x06000657 RID: 1623 RVA: 0x00022858 File Offset: 0x00020A58
		public string MediationAdapterClassName()
		{
			return this.client.MediationAdapterClassName();
		}

		// Token: 0x06000658 RID: 1624 RVA: 0x00022865 File Offset: 0x00020A65
		[CompilerGenerated]
		private void <ConfigureBannerEvents>b__26_0(object sender, EventArgs args)
		{
			if (this.OnAdLoaded != null)
			{
				this.OnAdLoaded(this, args);
			}
		}

		// Token: 0x06000659 RID: 1625 RVA: 0x0002287C File Offset: 0x00020A7C
		[CompilerGenerated]
		private void <ConfigureBannerEvents>b__26_1(object sender, AdFailedToLoadEventArgs args)
		{
			if (this.OnAdFailedToLoad != null)
			{
				this.OnAdFailedToLoad(this, args);
			}
		}

		// Token: 0x0600065A RID: 1626 RVA: 0x00022893 File Offset: 0x00020A93
		[CompilerGenerated]
		private void <ConfigureBannerEvents>b__26_2(object sender, EventArgs args)
		{
			if (this.OnAdOpening != null)
			{
				this.OnAdOpening(this, args);
			}
		}

		// Token: 0x0600065B RID: 1627 RVA: 0x000228AA File Offset: 0x00020AAA
		[CompilerGenerated]
		private void <ConfigureBannerEvents>b__26_3(object sender, EventArgs args)
		{
			if (this.OnAdClosed != null)
			{
				this.OnAdClosed(this, args);
			}
		}

		// Token: 0x0600065C RID: 1628 RVA: 0x000228C1 File Offset: 0x00020AC1
		[CompilerGenerated]
		private void <ConfigureBannerEvents>b__26_4(object sender, EventArgs args)
		{
			if (this.OnAdLeavingApplication != null)
			{
				this.OnAdLeavingApplication(this, args);
			}
		}

		// Token: 0x0400020D RID: 525
		private IBannerClient client;

		// Token: 0x0400020E RID: 526
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdLoaded;

		// Token: 0x0400020F RID: 527
		[CompilerGenerated]
		private EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad;

		// Token: 0x04000210 RID: 528
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdOpening;

		// Token: 0x04000211 RID: 529
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdClosed;

		// Token: 0x04000212 RID: 530
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdLeavingApplication;
	}
}
