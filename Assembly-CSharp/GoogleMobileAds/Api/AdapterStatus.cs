﻿using System;
using System.Runtime.CompilerServices;

namespace GoogleMobileAds.Api
{
	// Token: 0x02000086 RID: 134
	public class AdapterStatus
	{
		// Token: 0x17000070 RID: 112
		// (get) Token: 0x0600060C RID: 1548 RVA: 0x00021FBA File Offset: 0x000201BA
		// (set) Token: 0x0600060D RID: 1549 RVA: 0x00021FC2 File Offset: 0x000201C2
		public AdapterState InitializationState
		{
			[CompilerGenerated]
			get
			{
				return this.<InitializationState>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<InitializationState>k__BackingField = value;
			}
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x0600060E RID: 1550 RVA: 0x00021FCB File Offset: 0x000201CB
		// (set) Token: 0x0600060F RID: 1551 RVA: 0x00021FD3 File Offset: 0x000201D3
		public string Description
		{
			[CompilerGenerated]
			get
			{
				return this.<Description>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Description>k__BackingField = value;
			}
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x06000610 RID: 1552 RVA: 0x00021FDC File Offset: 0x000201DC
		// (set) Token: 0x06000611 RID: 1553 RVA: 0x00021FE4 File Offset: 0x000201E4
		public int Latency
		{
			[CompilerGenerated]
			get
			{
				return this.<Latency>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Latency>k__BackingField = value;
			}
		}

		// Token: 0x06000612 RID: 1554 RVA: 0x00021FED File Offset: 0x000201ED
		internal AdapterStatus(AdapterState state, string description, int latency)
		{
			this.InitializationState = state;
			this.Description = description;
			this.Latency = latency;
		}

		// Token: 0x040001E2 RID: 482
		[CompilerGenerated]
		private AdapterState <InitializationState>k__BackingField;

		// Token: 0x040001E3 RID: 483
		[CompilerGenerated]
		private string <Description>k__BackingField;

		// Token: 0x040001E4 RID: 484
		[CompilerGenerated]
		private int <Latency>k__BackingField;
	}
}
