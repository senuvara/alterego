﻿using System;

namespace GoogleMobileAds.Api
{
	// Token: 0x0200008E RID: 142
	public class AdSize
	{
		// Token: 0x06000638 RID: 1592 RVA: 0x00022339 File Offset: 0x00020539
		public AdSize(int width, int height)
		{
			this.isSmartBanner = false;
			this.width = width;
			this.height = height;
		}

		// Token: 0x06000639 RID: 1593 RVA: 0x00022356 File Offset: 0x00020556
		private AdSize(bool isSmartBanner) : this(0, 0)
		{
			this.isSmartBanner = isSmartBanner;
		}

		// Token: 0x17000080 RID: 128
		// (get) Token: 0x0600063A RID: 1594 RVA: 0x00022367 File Offset: 0x00020567
		public int Width
		{
			get
			{
				return this.width;
			}
		}

		// Token: 0x17000081 RID: 129
		// (get) Token: 0x0600063B RID: 1595 RVA: 0x0002236F File Offset: 0x0002056F
		public int Height
		{
			get
			{
				return this.height;
			}
		}

		// Token: 0x17000082 RID: 130
		// (get) Token: 0x0600063C RID: 1596 RVA: 0x00022377 File Offset: 0x00020577
		public bool IsSmartBanner
		{
			get
			{
				return this.isSmartBanner;
			}
		}

		// Token: 0x0600063D RID: 1597 RVA: 0x00022380 File Offset: 0x00020580
		public override bool Equals(object obj)
		{
			if (obj == null || base.GetType() != obj.GetType())
			{
				return false;
			}
			AdSize adSize = (AdSize)obj;
			return this.width == adSize.width && this.height == adSize.height && this.isSmartBanner == adSize.isSmartBanner;
		}

		// Token: 0x0600063E RID: 1598 RVA: 0x000223D8 File Offset: 0x000205D8
		public static bool operator ==(AdSize a, AdSize b)
		{
			return a.Equals(b);
		}

		// Token: 0x0600063F RID: 1599 RVA: 0x000223E1 File Offset: 0x000205E1
		public static bool operator !=(AdSize a, AdSize b)
		{
			return !a.Equals(b);
		}

		// Token: 0x06000640 RID: 1600 RVA: 0x000223F0 File Offset: 0x000205F0
		public override int GetHashCode()
		{
			int num = 71;
			int num2 = 11;
			return ((num * num2 ^ this.width.GetHashCode()) * num2 ^ this.height.GetHashCode()) * num2 ^ this.isSmartBanner.GetHashCode();
		}

		// Token: 0x06000641 RID: 1601 RVA: 0x0002242C File Offset: 0x0002062C
		// Note: this type is marked as 'beforefieldinit'.
		static AdSize()
		{
		}

		// Token: 0x04000204 RID: 516
		private bool isSmartBanner;

		// Token: 0x04000205 RID: 517
		private int width;

		// Token: 0x04000206 RID: 518
		private int height;

		// Token: 0x04000207 RID: 519
		public static readonly AdSize Banner = new AdSize(320, 50);

		// Token: 0x04000208 RID: 520
		public static readonly AdSize MediumRectangle = new AdSize(300, 250);

		// Token: 0x04000209 RID: 521
		public static readonly AdSize IABBanner = new AdSize(468, 60);

		// Token: 0x0400020A RID: 522
		public static readonly AdSize Leaderboard = new AdSize(728, 90);

		// Token: 0x0400020B RID: 523
		public static readonly AdSize SmartBanner = new AdSize(true);

		// Token: 0x0400020C RID: 524
		public static readonly int FullWidth = -1;
	}
}
