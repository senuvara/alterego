﻿using System;

namespace GoogleMobileAds.Api
{
	// Token: 0x02000092 RID: 146
	public enum Gender
	{
		// Token: 0x04000216 RID: 534
		Unknown,
		// Token: 0x04000217 RID: 535
		Male,
		// Token: 0x04000218 RID: 536
		Female
	}
}
