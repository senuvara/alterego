﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using GoogleMobileAds.Api.Mediation;

namespace GoogleMobileAds.Api
{
	// Token: 0x0200008D RID: 141
	public class AdRequest
	{
		// Token: 0x06000629 RID: 1577 RVA: 0x0002224C File Offset: 0x0002044C
		private AdRequest(AdRequest.Builder builder)
		{
			this.TestDevices = new List<string>(builder.TestDevices);
			this.Keywords = new HashSet<string>(builder.Keywords);
			this.Birthday = builder.Birthday;
			this.Gender = builder.Gender;
			this.TagForChildDirectedTreatment = builder.ChildDirectedTreatmentTag;
			this.Extras = new Dictionary<string, string>(builder.Extras);
			this.MediationExtras = builder.MediationExtras;
		}

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x0600062A RID: 1578 RVA: 0x000222C2 File Offset: 0x000204C2
		// (set) Token: 0x0600062B RID: 1579 RVA: 0x000222CA File Offset: 0x000204CA
		public List<string> TestDevices
		{
			[CompilerGenerated]
			get
			{
				return this.<TestDevices>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<TestDevices>k__BackingField = value;
			}
		}

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x0600062C RID: 1580 RVA: 0x000222D3 File Offset: 0x000204D3
		// (set) Token: 0x0600062D RID: 1581 RVA: 0x000222DB File Offset: 0x000204DB
		public HashSet<string> Keywords
		{
			[CompilerGenerated]
			get
			{
				return this.<Keywords>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Keywords>k__BackingField = value;
			}
		}

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x0600062E RID: 1582 RVA: 0x000222E4 File Offset: 0x000204E4
		// (set) Token: 0x0600062F RID: 1583 RVA: 0x000222EC File Offset: 0x000204EC
		public DateTime? Birthday
		{
			[CompilerGenerated]
			get
			{
				return this.<Birthday>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Birthday>k__BackingField = value;
			}
		}

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x06000630 RID: 1584 RVA: 0x000222F5 File Offset: 0x000204F5
		// (set) Token: 0x06000631 RID: 1585 RVA: 0x000222FD File Offset: 0x000204FD
		public Gender? Gender
		{
			[CompilerGenerated]
			get
			{
				return this.<Gender>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Gender>k__BackingField = value;
			}
		}

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x06000632 RID: 1586 RVA: 0x00022306 File Offset: 0x00020506
		// (set) Token: 0x06000633 RID: 1587 RVA: 0x0002230E File Offset: 0x0002050E
		public bool? TagForChildDirectedTreatment
		{
			[CompilerGenerated]
			get
			{
				return this.<TagForChildDirectedTreatment>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<TagForChildDirectedTreatment>k__BackingField = value;
			}
		}

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x06000634 RID: 1588 RVA: 0x00022317 File Offset: 0x00020517
		// (set) Token: 0x06000635 RID: 1589 RVA: 0x0002231F File Offset: 0x0002051F
		public Dictionary<string, string> Extras
		{
			[CompilerGenerated]
			get
			{
				return this.<Extras>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Extras>k__BackingField = value;
			}
		}

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x06000636 RID: 1590 RVA: 0x00022328 File Offset: 0x00020528
		// (set) Token: 0x06000637 RID: 1591 RVA: 0x00022330 File Offset: 0x00020530
		public List<MediationExtras> MediationExtras
		{
			[CompilerGenerated]
			get
			{
				return this.<MediationExtras>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<MediationExtras>k__BackingField = value;
			}
		}

		// Token: 0x040001FB RID: 507
		public const string Version = "3.16.0";

		// Token: 0x040001FC RID: 508
		public const string TestDeviceSimulator = "SIMULATOR";

		// Token: 0x040001FD RID: 509
		[CompilerGenerated]
		private List<string> <TestDevices>k__BackingField;

		// Token: 0x040001FE RID: 510
		[CompilerGenerated]
		private HashSet<string> <Keywords>k__BackingField;

		// Token: 0x040001FF RID: 511
		[CompilerGenerated]
		private DateTime? <Birthday>k__BackingField;

		// Token: 0x04000200 RID: 512
		[CompilerGenerated]
		private Gender? <Gender>k__BackingField;

		// Token: 0x04000201 RID: 513
		[CompilerGenerated]
		private bool? <TagForChildDirectedTreatment>k__BackingField;

		// Token: 0x04000202 RID: 514
		[CompilerGenerated]
		private Dictionary<string, string> <Extras>k__BackingField;

		// Token: 0x04000203 RID: 515
		[CompilerGenerated]
		private List<MediationExtras> <MediationExtras>k__BackingField;

		// Token: 0x0200015C RID: 348
		public class Builder
		{
			// Token: 0x06000A4D RID: 2637 RVA: 0x0003124C File Offset: 0x0002F44C
			public Builder()
			{
				this.TestDevices = new List<string>();
				this.Keywords = new HashSet<string>();
				this.Birthday = null;
				this.Gender = null;
				this.ChildDirectedTreatmentTag = null;
				this.Extras = new Dictionary<string, string>();
				this.MediationExtras = new List<MediationExtras>();
			}

			// Token: 0x17000164 RID: 356
			// (get) Token: 0x06000A4E RID: 2638 RVA: 0x000312B8 File Offset: 0x0002F4B8
			// (set) Token: 0x06000A4F RID: 2639 RVA: 0x000312C0 File Offset: 0x0002F4C0
			internal List<string> TestDevices
			{
				[CompilerGenerated]
				get
				{
					return this.<TestDevices>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<TestDevices>k__BackingField = value;
				}
			}

			// Token: 0x17000165 RID: 357
			// (get) Token: 0x06000A50 RID: 2640 RVA: 0x000312C9 File Offset: 0x0002F4C9
			// (set) Token: 0x06000A51 RID: 2641 RVA: 0x000312D1 File Offset: 0x0002F4D1
			internal HashSet<string> Keywords
			{
				[CompilerGenerated]
				get
				{
					return this.<Keywords>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<Keywords>k__BackingField = value;
				}
			}

			// Token: 0x17000166 RID: 358
			// (get) Token: 0x06000A52 RID: 2642 RVA: 0x000312DA File Offset: 0x0002F4DA
			// (set) Token: 0x06000A53 RID: 2643 RVA: 0x000312E2 File Offset: 0x0002F4E2
			internal DateTime? Birthday
			{
				[CompilerGenerated]
				get
				{
					return this.<Birthday>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<Birthday>k__BackingField = value;
				}
			}

			// Token: 0x17000167 RID: 359
			// (get) Token: 0x06000A54 RID: 2644 RVA: 0x000312EB File Offset: 0x0002F4EB
			// (set) Token: 0x06000A55 RID: 2645 RVA: 0x000312F3 File Offset: 0x0002F4F3
			internal Gender? Gender
			{
				[CompilerGenerated]
				get
				{
					return this.<Gender>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<Gender>k__BackingField = value;
				}
			}

			// Token: 0x17000168 RID: 360
			// (get) Token: 0x06000A56 RID: 2646 RVA: 0x000312FC File Offset: 0x0002F4FC
			// (set) Token: 0x06000A57 RID: 2647 RVA: 0x00031304 File Offset: 0x0002F504
			internal bool? ChildDirectedTreatmentTag
			{
				[CompilerGenerated]
				get
				{
					return this.<ChildDirectedTreatmentTag>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<ChildDirectedTreatmentTag>k__BackingField = value;
				}
			}

			// Token: 0x17000169 RID: 361
			// (get) Token: 0x06000A58 RID: 2648 RVA: 0x0003130D File Offset: 0x0002F50D
			// (set) Token: 0x06000A59 RID: 2649 RVA: 0x00031315 File Offset: 0x0002F515
			internal Dictionary<string, string> Extras
			{
				[CompilerGenerated]
				get
				{
					return this.<Extras>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<Extras>k__BackingField = value;
				}
			}

			// Token: 0x1700016A RID: 362
			// (get) Token: 0x06000A5A RID: 2650 RVA: 0x0003131E File Offset: 0x0002F51E
			// (set) Token: 0x06000A5B RID: 2651 RVA: 0x00031326 File Offset: 0x0002F526
			internal List<MediationExtras> MediationExtras
			{
				[CompilerGenerated]
				get
				{
					return this.<MediationExtras>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<MediationExtras>k__BackingField = value;
				}
			}

			// Token: 0x06000A5C RID: 2652 RVA: 0x0003132F File Offset: 0x0002F52F
			public AdRequest.Builder AddKeyword(string keyword)
			{
				this.Keywords.Add(keyword);
				return this;
			}

			// Token: 0x06000A5D RID: 2653 RVA: 0x0003133F File Offset: 0x0002F53F
			public AdRequest.Builder AddTestDevice(string deviceId)
			{
				this.TestDevices.Add(deviceId);
				return this;
			}

			// Token: 0x06000A5E RID: 2654 RVA: 0x0003134E File Offset: 0x0002F54E
			public AdRequest Build()
			{
				return new AdRequest(this);
			}

			// Token: 0x06000A5F RID: 2655 RVA: 0x00031356 File Offset: 0x0002F556
			public AdRequest.Builder SetBirthday(DateTime birthday)
			{
				this.Birthday = new DateTime?(birthday);
				return this;
			}

			// Token: 0x06000A60 RID: 2656 RVA: 0x00031365 File Offset: 0x0002F565
			public AdRequest.Builder SetGender(Gender gender)
			{
				this.Gender = new Gender?(gender);
				return this;
			}

			// Token: 0x06000A61 RID: 2657 RVA: 0x00031374 File Offset: 0x0002F574
			public AdRequest.Builder AddMediationExtras(MediationExtras extras)
			{
				this.MediationExtras.Add(extras);
				return this;
			}

			// Token: 0x06000A62 RID: 2658 RVA: 0x00031383 File Offset: 0x0002F583
			public AdRequest.Builder TagForChildDirectedTreatment(bool tagForChildDirectedTreatment)
			{
				this.ChildDirectedTreatmentTag = new bool?(tagForChildDirectedTreatment);
				return this;
			}

			// Token: 0x06000A63 RID: 2659 RVA: 0x00031392 File Offset: 0x0002F592
			public AdRequest.Builder AddExtra(string key, string value)
			{
				this.Extras.Add(key, value);
				return this;
			}

			// Token: 0x04000538 RID: 1336
			[CompilerGenerated]
			private List<string> <TestDevices>k__BackingField;

			// Token: 0x04000539 RID: 1337
			[CompilerGenerated]
			private HashSet<string> <Keywords>k__BackingField;

			// Token: 0x0400053A RID: 1338
			[CompilerGenerated]
			private DateTime? <Birthday>k__BackingField;

			// Token: 0x0400053B RID: 1339
			[CompilerGenerated]
			private Gender? <Gender>k__BackingField;

			// Token: 0x0400053C RID: 1340
			[CompilerGenerated]
			private bool? <ChildDirectedTreatmentTag>k__BackingField;

			// Token: 0x0400053D RID: 1341
			[CompilerGenerated]
			private Dictionary<string, string> <Extras>k__BackingField;

			// Token: 0x0400053E RID: 1342
			[CompilerGenerated]
			private List<MediationExtras> <MediationExtras>k__BackingField;
		}
	}
}
