﻿using System;
using System.Runtime.CompilerServices;

namespace GoogleMobileAds.Api
{
	// Token: 0x02000088 RID: 136
	public class AdErrorEventArgs : EventArgs
	{
		// Token: 0x17000073 RID: 115
		// (get) Token: 0x06000613 RID: 1555 RVA: 0x0002200A File Offset: 0x0002020A
		// (set) Token: 0x06000614 RID: 1556 RVA: 0x00022012 File Offset: 0x00020212
		public string Message
		{
			[CompilerGenerated]
			get
			{
				return this.<Message>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Message>k__BackingField = value;
			}
		}

		// Token: 0x06000615 RID: 1557 RVA: 0x0002201B File Offset: 0x0002021B
		public AdErrorEventArgs()
		{
		}

		// Token: 0x040001E8 RID: 488
		[CompilerGenerated]
		private string <Message>k__BackingField;
	}
}
