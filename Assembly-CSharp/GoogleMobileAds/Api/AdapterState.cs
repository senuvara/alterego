﻿using System;

namespace GoogleMobileAds.Api
{
	// Token: 0x02000087 RID: 135
	public enum AdapterState
	{
		// Token: 0x040001E6 RID: 486
		NotReady,
		// Token: 0x040001E7 RID: 487
		Ready
	}
}
