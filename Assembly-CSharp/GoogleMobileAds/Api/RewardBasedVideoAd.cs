﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using GoogleMobileAds.Common;

namespace GoogleMobileAds.Api
{
	// Token: 0x02000096 RID: 150
	public class RewardBasedVideoAd
	{
		// Token: 0x17000086 RID: 134
		// (get) Token: 0x06000688 RID: 1672 RVA: 0x00022D8D File Offset: 0x00020F8D
		public static RewardBasedVideoAd Instance
		{
			get
			{
				return RewardBasedVideoAd.instance;
			}
		}

		// Token: 0x06000689 RID: 1673 RVA: 0x00022D94 File Offset: 0x00020F94
		private RewardBasedVideoAd()
		{
			MethodInfo method = Type.GetType("GoogleMobileAds.GoogleMobileAdsClientFactory,Assembly-CSharp").GetMethod("BuildRewardBasedVideoAdClient", BindingFlags.Static | BindingFlags.Public);
			this.client = (IRewardBasedVideoAdClient)method.Invoke(null, null);
			this.client.CreateRewardBasedVideoAd();
			this.client.OnAdLoaded += delegate(object sender, EventArgs args)
			{
				if (this.OnAdLoaded != null)
				{
					this.OnAdLoaded(this, args);
				}
			};
			this.client.OnAdFailedToLoad += delegate(object sender, AdFailedToLoadEventArgs args)
			{
				if (this.OnAdFailedToLoad != null)
				{
					this.OnAdFailedToLoad(this, args);
				}
			};
			this.client.OnAdOpening += delegate(object sender, EventArgs args)
			{
				if (this.OnAdOpening != null)
				{
					this.OnAdOpening(this, args);
				}
			};
			this.client.OnAdStarted += delegate(object sender, EventArgs args)
			{
				if (this.OnAdStarted != null)
				{
					this.OnAdStarted(this, args);
				}
			};
			this.client.OnAdClosed += delegate(object sender, EventArgs args)
			{
				if (this.OnAdClosed != null)
				{
					this.OnAdClosed(this, args);
				}
			};
			this.client.OnAdLeavingApplication += delegate(object sender, EventArgs args)
			{
				if (this.OnAdLeavingApplication != null)
				{
					this.OnAdLeavingApplication(this, args);
				}
			};
			this.client.OnAdRewarded += delegate(object sender, Reward args)
			{
				if (this.OnAdRewarded != null)
				{
					this.OnAdRewarded(this, args);
				}
			};
			this.client.OnAdCompleted += delegate(object sender, EventArgs args)
			{
				if (this.OnAdCompleted != null)
				{
					this.OnAdCompleted(this, args);
				}
			};
		}

		// Token: 0x14000051 RID: 81
		// (add) Token: 0x0600068A RID: 1674 RVA: 0x00022E94 File Offset: 0x00021094
		// (remove) Token: 0x0600068B RID: 1675 RVA: 0x00022ECC File Offset: 0x000210CC
		public event EventHandler<EventArgs> OnAdLoaded
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLoaded;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLoaded;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000052 RID: 82
		// (add) Token: 0x0600068C RID: 1676 RVA: 0x00022F04 File Offset: 0x00021104
		// (remove) Token: 0x0600068D RID: 1677 RVA: 0x00022F3C File Offset: 0x0002113C
		public event EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad
		{
			[CompilerGenerated]
			add
			{
				EventHandler<AdFailedToLoadEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdFailedToLoadEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdFailedToLoadEventArgs> value2 = (EventHandler<AdFailedToLoadEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdFailedToLoadEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<AdFailedToLoadEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdFailedToLoadEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdFailedToLoadEventArgs> value2 = (EventHandler<AdFailedToLoadEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdFailedToLoadEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000053 RID: 83
		// (add) Token: 0x0600068E RID: 1678 RVA: 0x00022F74 File Offset: 0x00021174
		// (remove) Token: 0x0600068F RID: 1679 RVA: 0x00022FAC File Offset: 0x000211AC
		public event EventHandler<EventArgs> OnAdOpening
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdOpening;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdOpening, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdOpening;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdOpening, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000054 RID: 84
		// (add) Token: 0x06000690 RID: 1680 RVA: 0x00022FE4 File Offset: 0x000211E4
		// (remove) Token: 0x06000691 RID: 1681 RVA: 0x0002301C File Offset: 0x0002121C
		public event EventHandler<EventArgs> OnAdStarted
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdStarted;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdStarted, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdStarted;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdStarted, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000055 RID: 85
		// (add) Token: 0x06000692 RID: 1682 RVA: 0x00023054 File Offset: 0x00021254
		// (remove) Token: 0x06000693 RID: 1683 RVA: 0x0002308C File Offset: 0x0002128C
		public event EventHandler<EventArgs> OnAdClosed
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdClosed;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdClosed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdClosed;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdClosed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000056 RID: 86
		// (add) Token: 0x06000694 RID: 1684 RVA: 0x000230C4 File Offset: 0x000212C4
		// (remove) Token: 0x06000695 RID: 1685 RVA: 0x000230FC File Offset: 0x000212FC
		public event EventHandler<Reward> OnAdRewarded
		{
			[CompilerGenerated]
			add
			{
				EventHandler<Reward> eventHandler = this.OnAdRewarded;
				EventHandler<Reward> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<Reward> value2 = (EventHandler<Reward>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<Reward>>(ref this.OnAdRewarded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<Reward> eventHandler = this.OnAdRewarded;
				EventHandler<Reward> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<Reward> value2 = (EventHandler<Reward>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<Reward>>(ref this.OnAdRewarded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000057 RID: 87
		// (add) Token: 0x06000696 RID: 1686 RVA: 0x00023134 File Offset: 0x00021334
		// (remove) Token: 0x06000697 RID: 1687 RVA: 0x0002316C File Offset: 0x0002136C
		public event EventHandler<EventArgs> OnAdLeavingApplication
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLeavingApplication;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLeavingApplication, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLeavingApplication;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLeavingApplication, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000058 RID: 88
		// (add) Token: 0x06000698 RID: 1688 RVA: 0x000231A4 File Offset: 0x000213A4
		// (remove) Token: 0x06000699 RID: 1689 RVA: 0x000231DC File Offset: 0x000213DC
		public event EventHandler<EventArgs> OnAdCompleted
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdCompleted;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdCompleted, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdCompleted;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdCompleted, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x0600069A RID: 1690 RVA: 0x00023211 File Offset: 0x00021411
		public void LoadAd(AdRequest request, string adUnitId)
		{
			this.client.LoadAd(request, adUnitId);
		}

		// Token: 0x0600069B RID: 1691 RVA: 0x00023220 File Offset: 0x00021420
		public bool IsLoaded()
		{
			return this.client.IsLoaded();
		}

		// Token: 0x0600069C RID: 1692 RVA: 0x0002322D File Offset: 0x0002142D
		public void Show()
		{
			this.client.ShowRewardBasedVideoAd();
		}

		// Token: 0x0600069D RID: 1693 RVA: 0x0002323A File Offset: 0x0002143A
		public void SetUserId(string userId)
		{
			this.client.SetUserId(userId);
		}

		// Token: 0x0600069E RID: 1694 RVA: 0x00023248 File Offset: 0x00021448
		public string MediationAdapterClassName()
		{
			return this.client.MediationAdapterClassName();
		}

		// Token: 0x0600069F RID: 1695 RVA: 0x00023255 File Offset: 0x00021455
		// Note: this type is marked as 'beforefieldinit'.
		static RewardBasedVideoAd()
		{
		}

		// Token: 0x060006A0 RID: 1696 RVA: 0x00023261 File Offset: 0x00021461
		[CompilerGenerated]
		private void <.ctor>b__4_0(object sender, EventArgs args)
		{
			if (this.OnAdLoaded != null)
			{
				this.OnAdLoaded(this, args);
			}
		}

		// Token: 0x060006A1 RID: 1697 RVA: 0x00023278 File Offset: 0x00021478
		[CompilerGenerated]
		private void <.ctor>b__4_1(object sender, AdFailedToLoadEventArgs args)
		{
			if (this.OnAdFailedToLoad != null)
			{
				this.OnAdFailedToLoad(this, args);
			}
		}

		// Token: 0x060006A2 RID: 1698 RVA: 0x0002328F File Offset: 0x0002148F
		[CompilerGenerated]
		private void <.ctor>b__4_2(object sender, EventArgs args)
		{
			if (this.OnAdOpening != null)
			{
				this.OnAdOpening(this, args);
			}
		}

		// Token: 0x060006A3 RID: 1699 RVA: 0x000232A6 File Offset: 0x000214A6
		[CompilerGenerated]
		private void <.ctor>b__4_3(object sender, EventArgs args)
		{
			if (this.OnAdStarted != null)
			{
				this.OnAdStarted(this, args);
			}
		}

		// Token: 0x060006A4 RID: 1700 RVA: 0x000232BD File Offset: 0x000214BD
		[CompilerGenerated]
		private void <.ctor>b__4_4(object sender, EventArgs args)
		{
			if (this.OnAdClosed != null)
			{
				this.OnAdClosed(this, args);
			}
		}

		// Token: 0x060006A5 RID: 1701 RVA: 0x000232D4 File Offset: 0x000214D4
		[CompilerGenerated]
		private void <.ctor>b__4_5(object sender, EventArgs args)
		{
			if (this.OnAdLeavingApplication != null)
			{
				this.OnAdLeavingApplication(this, args);
			}
		}

		// Token: 0x060006A6 RID: 1702 RVA: 0x000232EB File Offset: 0x000214EB
		[CompilerGenerated]
		private void <.ctor>b__4_6(object sender, Reward args)
		{
			if (this.OnAdRewarded != null)
			{
				this.OnAdRewarded(this, args);
			}
		}

		// Token: 0x060006A7 RID: 1703 RVA: 0x00023302 File Offset: 0x00021502
		[CompilerGenerated]
		private void <.ctor>b__4_7(object sender, EventArgs args)
		{
			if (this.OnAdCompleted != null)
			{
				this.OnAdCompleted(this, args);
			}
		}

		// Token: 0x04000222 RID: 546
		private IRewardBasedVideoAdClient client;

		// Token: 0x04000223 RID: 547
		private static readonly RewardBasedVideoAd instance = new RewardBasedVideoAd();

		// Token: 0x04000224 RID: 548
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdLoaded;

		// Token: 0x04000225 RID: 549
		[CompilerGenerated]
		private EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad;

		// Token: 0x04000226 RID: 550
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdOpening;

		// Token: 0x04000227 RID: 551
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdStarted;

		// Token: 0x04000228 RID: 552
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdClosed;

		// Token: 0x04000229 RID: 553
		[CompilerGenerated]
		private EventHandler<Reward> OnAdRewarded;

		// Token: 0x0400022A RID: 554
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdLeavingApplication;

		// Token: 0x0400022B RID: 555
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdCompleted;
	}
}
