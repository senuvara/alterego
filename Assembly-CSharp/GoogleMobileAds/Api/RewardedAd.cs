﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using GoogleMobileAds.Common;

namespace GoogleMobileAds.Api
{
	// Token: 0x02000097 RID: 151
	public class RewardedAd
	{
		// Token: 0x060006A8 RID: 1704 RVA: 0x0002331C File Offset: 0x0002151C
		public RewardedAd(string adUnitId)
		{
			MethodInfo method = Type.GetType("GoogleMobileAds.GoogleMobileAdsClientFactory,Assembly-CSharp").GetMethod("BuildRewardedAdClient", BindingFlags.Static | BindingFlags.Public);
			this.client = (IRewardedAdClient)method.Invoke(null, null);
			this.client.CreateRewardedAd(adUnitId);
			this.client.OnAdLoaded += delegate(object sender, EventArgs args)
			{
				if (this.OnAdLoaded != null)
				{
					this.OnAdLoaded(this, args);
				}
			};
			this.client.OnAdFailedToLoad += delegate(object sender, AdErrorEventArgs args)
			{
				if (this.OnAdFailedToLoad != null)
				{
					this.OnAdFailedToLoad(this, args);
				}
			};
			this.client.OnAdFailedToShow += delegate(object sender, AdErrorEventArgs args)
			{
				if (this.OnAdFailedToShow != null)
				{
					this.OnAdFailedToShow(this, args);
				}
			};
			this.client.OnAdOpening += delegate(object sender, EventArgs args)
			{
				if (this.OnAdOpening != null)
				{
					this.OnAdOpening(this, args);
				}
			};
			this.client.OnAdClosed += delegate(object sender, EventArgs args)
			{
				if (this.OnAdClosed != null)
				{
					this.OnAdClosed(this, args);
				}
			};
			this.client.OnUserEarnedReward += delegate(object sender, Reward args)
			{
				if (this.OnUserEarnedReward != null)
				{
					this.OnUserEarnedReward(this, args);
				}
			};
		}

		// Token: 0x14000059 RID: 89
		// (add) Token: 0x060006A9 RID: 1705 RVA: 0x000233F0 File Offset: 0x000215F0
		// (remove) Token: 0x060006AA RID: 1706 RVA: 0x00023428 File Offset: 0x00021628
		public event EventHandler<EventArgs> OnAdLoaded
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLoaded;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLoaded;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x1400005A RID: 90
		// (add) Token: 0x060006AB RID: 1707 RVA: 0x00023460 File Offset: 0x00021660
		// (remove) Token: 0x060006AC RID: 1708 RVA: 0x00023498 File Offset: 0x00021698
		public event EventHandler<AdErrorEventArgs> OnAdFailedToLoad
		{
			[CompilerGenerated]
			add
			{
				EventHandler<AdErrorEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdErrorEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdErrorEventArgs> value2 = (EventHandler<AdErrorEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdErrorEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<AdErrorEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdErrorEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdErrorEventArgs> value2 = (EventHandler<AdErrorEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdErrorEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x1400005B RID: 91
		// (add) Token: 0x060006AD RID: 1709 RVA: 0x000234D0 File Offset: 0x000216D0
		// (remove) Token: 0x060006AE RID: 1710 RVA: 0x00023508 File Offset: 0x00021708
		public event EventHandler<AdErrorEventArgs> OnAdFailedToShow
		{
			[CompilerGenerated]
			add
			{
				EventHandler<AdErrorEventArgs> eventHandler = this.OnAdFailedToShow;
				EventHandler<AdErrorEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdErrorEventArgs> value2 = (EventHandler<AdErrorEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdErrorEventArgs>>(ref this.OnAdFailedToShow, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<AdErrorEventArgs> eventHandler = this.OnAdFailedToShow;
				EventHandler<AdErrorEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdErrorEventArgs> value2 = (EventHandler<AdErrorEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdErrorEventArgs>>(ref this.OnAdFailedToShow, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x1400005C RID: 92
		// (add) Token: 0x060006AF RID: 1711 RVA: 0x00023540 File Offset: 0x00021740
		// (remove) Token: 0x060006B0 RID: 1712 RVA: 0x00023578 File Offset: 0x00021778
		public event EventHandler<EventArgs> OnAdOpening
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdOpening;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdOpening, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdOpening;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdOpening, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x1400005D RID: 93
		// (add) Token: 0x060006B1 RID: 1713 RVA: 0x000235B0 File Offset: 0x000217B0
		// (remove) Token: 0x060006B2 RID: 1714 RVA: 0x000235E8 File Offset: 0x000217E8
		public event EventHandler<EventArgs> OnAdClosed
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdClosed;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdClosed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdClosed;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdClosed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x1400005E RID: 94
		// (add) Token: 0x060006B3 RID: 1715 RVA: 0x00023620 File Offset: 0x00021820
		// (remove) Token: 0x060006B4 RID: 1716 RVA: 0x00023658 File Offset: 0x00021858
		public event EventHandler<Reward> OnUserEarnedReward
		{
			[CompilerGenerated]
			add
			{
				EventHandler<Reward> eventHandler = this.OnUserEarnedReward;
				EventHandler<Reward> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<Reward> value2 = (EventHandler<Reward>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<Reward>>(ref this.OnUserEarnedReward, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<Reward> eventHandler = this.OnUserEarnedReward;
				EventHandler<Reward> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<Reward> value2 = (EventHandler<Reward>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<Reward>>(ref this.OnUserEarnedReward, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x060006B5 RID: 1717 RVA: 0x0002368D File Offset: 0x0002188D
		public void LoadAd(AdRequest request)
		{
			this.client.LoadAd(request);
		}

		// Token: 0x060006B6 RID: 1718 RVA: 0x0002369B File Offset: 0x0002189B
		public bool IsLoaded()
		{
			return this.client.IsLoaded();
		}

		// Token: 0x060006B7 RID: 1719 RVA: 0x000236A8 File Offset: 0x000218A8
		public void Show()
		{
			this.client.Show();
		}

		// Token: 0x060006B8 RID: 1720 RVA: 0x000236B5 File Offset: 0x000218B5
		public string MediationAdapterClassName()
		{
			return this.client.MediationAdapterClassName();
		}

		// Token: 0x060006B9 RID: 1721 RVA: 0x000236C2 File Offset: 0x000218C2
		[CompilerGenerated]
		private void <.ctor>b__1_0(object sender, EventArgs args)
		{
			if (this.OnAdLoaded != null)
			{
				this.OnAdLoaded(this, args);
			}
		}

		// Token: 0x060006BA RID: 1722 RVA: 0x000236D9 File Offset: 0x000218D9
		[CompilerGenerated]
		private void <.ctor>b__1_1(object sender, AdErrorEventArgs args)
		{
			if (this.OnAdFailedToLoad != null)
			{
				this.OnAdFailedToLoad(this, args);
			}
		}

		// Token: 0x060006BB RID: 1723 RVA: 0x000236F0 File Offset: 0x000218F0
		[CompilerGenerated]
		private void <.ctor>b__1_2(object sender, AdErrorEventArgs args)
		{
			if (this.OnAdFailedToShow != null)
			{
				this.OnAdFailedToShow(this, args);
			}
		}

		// Token: 0x060006BC RID: 1724 RVA: 0x00023707 File Offset: 0x00021907
		[CompilerGenerated]
		private void <.ctor>b__1_3(object sender, EventArgs args)
		{
			if (this.OnAdOpening != null)
			{
				this.OnAdOpening(this, args);
			}
		}

		// Token: 0x060006BD RID: 1725 RVA: 0x0002371E File Offset: 0x0002191E
		[CompilerGenerated]
		private void <.ctor>b__1_4(object sender, EventArgs args)
		{
			if (this.OnAdClosed != null)
			{
				this.OnAdClosed(this, args);
			}
		}

		// Token: 0x060006BE RID: 1726 RVA: 0x00023735 File Offset: 0x00021935
		[CompilerGenerated]
		private void <.ctor>b__1_5(object sender, Reward args)
		{
			if (this.OnUserEarnedReward != null)
			{
				this.OnUserEarnedReward(this, args);
			}
		}

		// Token: 0x0400022C RID: 556
		private IRewardedAdClient client;

		// Token: 0x0400022D RID: 557
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdLoaded;

		// Token: 0x0400022E RID: 558
		[CompilerGenerated]
		private EventHandler<AdErrorEventArgs> OnAdFailedToLoad;

		// Token: 0x0400022F RID: 559
		[CompilerGenerated]
		private EventHandler<AdErrorEventArgs> OnAdFailedToShow;

		// Token: 0x04000230 RID: 560
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdOpening;

		// Token: 0x04000231 RID: 561
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdClosed;

		// Token: 0x04000232 RID: 562
		[CompilerGenerated]
		private EventHandler<Reward> OnUserEarnedReward;
	}
}
