﻿using System;
using GoogleMobileAds.Api;

namespace GoogleMobileAds.Common
{
	// Token: 0x0200007F RID: 127
	public interface IInterstitialClient
	{
		// Token: 0x1400002C RID: 44
		// (add) Token: 0x060005B4 RID: 1460
		// (remove) Token: 0x060005B5 RID: 1461
		event EventHandler<EventArgs> OnAdLoaded;

		// Token: 0x1400002D RID: 45
		// (add) Token: 0x060005B6 RID: 1462
		// (remove) Token: 0x060005B7 RID: 1463
		event EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad;

		// Token: 0x1400002E RID: 46
		// (add) Token: 0x060005B8 RID: 1464
		// (remove) Token: 0x060005B9 RID: 1465
		event EventHandler<EventArgs> OnAdOpening;

		// Token: 0x1400002F RID: 47
		// (add) Token: 0x060005BA RID: 1466
		// (remove) Token: 0x060005BB RID: 1467
		event EventHandler<EventArgs> OnAdClosed;

		// Token: 0x14000030 RID: 48
		// (add) Token: 0x060005BC RID: 1468
		// (remove) Token: 0x060005BD RID: 1469
		event EventHandler<EventArgs> OnAdLeavingApplication;

		// Token: 0x060005BE RID: 1470
		void CreateInterstitialAd(string adUnitId);

		// Token: 0x060005BF RID: 1471
		void LoadAd(AdRequest request);

		// Token: 0x060005C0 RID: 1472
		bool IsLoaded();

		// Token: 0x060005C1 RID: 1473
		void ShowInterstitial();

		// Token: 0x060005C2 RID: 1474
		void DestroyInterstitial();

		// Token: 0x060005C3 RID: 1475
		string MediationAdapterClassName();
	}
}
