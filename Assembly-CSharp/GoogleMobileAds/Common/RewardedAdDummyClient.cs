﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;
using GoogleMobileAds.Api;

namespace GoogleMobileAds.Common
{
	// Token: 0x02000084 RID: 132
	public class RewardedAdDummyClient : IRewardedAdClient
	{
		// Token: 0x060005F7 RID: 1527 RVA: 0x000043CE File Offset: 0x000025CE
		public RewardedAdDummyClient()
		{
		}

		// Token: 0x1400003F RID: 63
		// (add) Token: 0x060005F8 RID: 1528 RVA: 0x00021CE8 File Offset: 0x0001FEE8
		// (remove) Token: 0x060005F9 RID: 1529 RVA: 0x00021D20 File Offset: 0x0001FF20
		public event EventHandler<EventArgs> OnAdLoaded
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLoaded;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLoaded;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000040 RID: 64
		// (add) Token: 0x060005FA RID: 1530 RVA: 0x00021D58 File Offset: 0x0001FF58
		// (remove) Token: 0x060005FB RID: 1531 RVA: 0x00021D90 File Offset: 0x0001FF90
		public event EventHandler<AdErrorEventArgs> OnAdFailedToLoad
		{
			[CompilerGenerated]
			add
			{
				EventHandler<AdErrorEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdErrorEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdErrorEventArgs> value2 = (EventHandler<AdErrorEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdErrorEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<AdErrorEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdErrorEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdErrorEventArgs> value2 = (EventHandler<AdErrorEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdErrorEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000041 RID: 65
		// (add) Token: 0x060005FC RID: 1532 RVA: 0x00021DC8 File Offset: 0x0001FFC8
		// (remove) Token: 0x060005FD RID: 1533 RVA: 0x00021E00 File Offset: 0x00020000
		public event EventHandler<AdErrorEventArgs> OnAdFailedToShow
		{
			[CompilerGenerated]
			add
			{
				EventHandler<AdErrorEventArgs> eventHandler = this.OnAdFailedToShow;
				EventHandler<AdErrorEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdErrorEventArgs> value2 = (EventHandler<AdErrorEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdErrorEventArgs>>(ref this.OnAdFailedToShow, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<AdErrorEventArgs> eventHandler = this.OnAdFailedToShow;
				EventHandler<AdErrorEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdErrorEventArgs> value2 = (EventHandler<AdErrorEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdErrorEventArgs>>(ref this.OnAdFailedToShow, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000042 RID: 66
		// (add) Token: 0x060005FE RID: 1534 RVA: 0x00021E38 File Offset: 0x00020038
		// (remove) Token: 0x060005FF RID: 1535 RVA: 0x00021E70 File Offset: 0x00020070
		public event EventHandler<EventArgs> OnAdOpening
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdOpening;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdOpening, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdOpening;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdOpening, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000043 RID: 67
		// (add) Token: 0x06000600 RID: 1536 RVA: 0x00021EA8 File Offset: 0x000200A8
		// (remove) Token: 0x06000601 RID: 1537 RVA: 0x00021EE0 File Offset: 0x000200E0
		public event EventHandler<EventArgs> OnAdClosed
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdClosed;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdClosed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdClosed;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdClosed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000044 RID: 68
		// (add) Token: 0x06000602 RID: 1538 RVA: 0x00021F18 File Offset: 0x00020118
		// (remove) Token: 0x06000603 RID: 1539 RVA: 0x00021F50 File Offset: 0x00020150
		public event EventHandler<Reward> OnUserEarnedReward
		{
			[CompilerGenerated]
			add
			{
				EventHandler<Reward> eventHandler = this.OnUserEarnedReward;
				EventHandler<Reward> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<Reward> value2 = (EventHandler<Reward>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<Reward>>(ref this.OnUserEarnedReward, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<Reward> eventHandler = this.OnUserEarnedReward;
				EventHandler<Reward> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<Reward> value2 = (EventHandler<Reward>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<Reward>>(ref this.OnUserEarnedReward, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x06000604 RID: 1540 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void CreateRewardedAd(string adUnitId)
		{
		}

		// Token: 0x06000605 RID: 1541 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void LoadAd(AdRequest request)
		{
		}

		// Token: 0x06000606 RID: 1542 RVA: 0x00021B8F File Offset: 0x0001FD8F
		public bool IsLoaded()
		{
			return true;
		}

		// Token: 0x06000607 RID: 1543 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void Show()
		{
		}

		// Token: 0x06000608 RID: 1544 RVA: 0x0001E9D4 File Offset: 0x0001CBD4
		public string MediationAdapterClassName()
		{
			return null;
		}

		// Token: 0x040001DC RID: 476
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdLoaded;

		// Token: 0x040001DD RID: 477
		[CompilerGenerated]
		private EventHandler<AdErrorEventArgs> OnAdFailedToLoad;

		// Token: 0x040001DE RID: 478
		[CompilerGenerated]
		private EventHandler<AdErrorEventArgs> OnAdFailedToShow;

		// Token: 0x040001DF RID: 479
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdOpening;

		// Token: 0x040001E0 RID: 480
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdClosed;

		// Token: 0x040001E1 RID: 481
		[CompilerGenerated]
		private EventHandler<Reward> OnUserEarnedReward;
	}
}
