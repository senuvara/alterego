﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;
using GoogleMobileAds.Api;

namespace GoogleMobileAds.Common
{
	// Token: 0x0200007B RID: 123
	public class DummyClient : IBannerClient, IInterstitialClient, IRewardBasedVideoAdClient, IAdLoaderClient, IMobileAdsClient
	{
		// Token: 0x06000564 RID: 1380 RVA: 0x000043CE File Offset: 0x000025CE
		public DummyClient()
		{
		}

		// Token: 0x1400001C RID: 28
		// (add) Token: 0x06000565 RID: 1381 RVA: 0x00021794 File Offset: 0x0001F994
		// (remove) Token: 0x06000566 RID: 1382 RVA: 0x000217CC File Offset: 0x0001F9CC
		public event EventHandler<EventArgs> OnAdLoaded
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLoaded;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLoaded;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x1400001D RID: 29
		// (add) Token: 0x06000567 RID: 1383 RVA: 0x00021804 File Offset: 0x0001FA04
		// (remove) Token: 0x06000568 RID: 1384 RVA: 0x0002183C File Offset: 0x0001FA3C
		public event EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad
		{
			[CompilerGenerated]
			add
			{
				EventHandler<AdFailedToLoadEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdFailedToLoadEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdFailedToLoadEventArgs> value2 = (EventHandler<AdFailedToLoadEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdFailedToLoadEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<AdFailedToLoadEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdFailedToLoadEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdFailedToLoadEventArgs> value2 = (EventHandler<AdFailedToLoadEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdFailedToLoadEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x1400001E RID: 30
		// (add) Token: 0x06000569 RID: 1385 RVA: 0x00021874 File Offset: 0x0001FA74
		// (remove) Token: 0x0600056A RID: 1386 RVA: 0x000218AC File Offset: 0x0001FAAC
		public event EventHandler<EventArgs> OnAdOpening
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdOpening;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdOpening, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdOpening;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdOpening, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x1400001F RID: 31
		// (add) Token: 0x0600056B RID: 1387 RVA: 0x000218E4 File Offset: 0x0001FAE4
		// (remove) Token: 0x0600056C RID: 1388 RVA: 0x0002191C File Offset: 0x0001FB1C
		public event EventHandler<EventArgs> OnAdStarted
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdStarted;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdStarted, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdStarted;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdStarted, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000020 RID: 32
		// (add) Token: 0x0600056D RID: 1389 RVA: 0x00021954 File Offset: 0x0001FB54
		// (remove) Token: 0x0600056E RID: 1390 RVA: 0x0002198C File Offset: 0x0001FB8C
		public event EventHandler<EventArgs> OnAdClosed
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdClosed;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdClosed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdClosed;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdClosed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000021 RID: 33
		// (add) Token: 0x0600056F RID: 1391 RVA: 0x000219C4 File Offset: 0x0001FBC4
		// (remove) Token: 0x06000570 RID: 1392 RVA: 0x000219FC File Offset: 0x0001FBFC
		public event EventHandler<Reward> OnAdRewarded
		{
			[CompilerGenerated]
			add
			{
				EventHandler<Reward> eventHandler = this.OnAdRewarded;
				EventHandler<Reward> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<Reward> value2 = (EventHandler<Reward>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<Reward>>(ref this.OnAdRewarded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<Reward> eventHandler = this.OnAdRewarded;
				EventHandler<Reward> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<Reward> value2 = (EventHandler<Reward>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<Reward>>(ref this.OnAdRewarded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000022 RID: 34
		// (add) Token: 0x06000571 RID: 1393 RVA: 0x00021A34 File Offset: 0x0001FC34
		// (remove) Token: 0x06000572 RID: 1394 RVA: 0x00021A6C File Offset: 0x0001FC6C
		public event EventHandler<EventArgs> OnAdLeavingApplication
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLeavingApplication;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLeavingApplication, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLeavingApplication;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLeavingApplication, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000023 RID: 35
		// (add) Token: 0x06000573 RID: 1395 RVA: 0x00021AA4 File Offset: 0x0001FCA4
		// (remove) Token: 0x06000574 RID: 1396 RVA: 0x00021ADC File Offset: 0x0001FCDC
		public event EventHandler<EventArgs> OnAdCompleted
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdCompleted;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdCompleted, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdCompleted;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdCompleted, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000024 RID: 36
		// (add) Token: 0x06000575 RID: 1397 RVA: 0x00021B14 File Offset: 0x0001FD14
		// (remove) Token: 0x06000576 RID: 1398 RVA: 0x00021B4C File Offset: 0x0001FD4C
		public event EventHandler<CustomNativeEventArgs> OnCustomNativeTemplateAdLoaded
		{
			[CompilerGenerated]
			add
			{
				EventHandler<CustomNativeEventArgs> eventHandler = this.OnCustomNativeTemplateAdLoaded;
				EventHandler<CustomNativeEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<CustomNativeEventArgs> value2 = (EventHandler<CustomNativeEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<CustomNativeEventArgs>>(ref this.OnCustomNativeTemplateAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<CustomNativeEventArgs> eventHandler = this.OnCustomNativeTemplateAdLoaded;
				EventHandler<CustomNativeEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<CustomNativeEventArgs> value2 = (EventHandler<CustomNativeEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<CustomNativeEventArgs>>(ref this.OnCustomNativeTemplateAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x06000577 RID: 1399 RVA: 0x00021B81 File Offset: 0x0001FD81
		// (set) Token: 0x06000578 RID: 1400 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public string UserId
		{
			get
			{
				return "UserId";
			}
			set
			{
			}
		}

		// Token: 0x06000579 RID: 1401 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void Initialize(string appId)
		{
		}

		// Token: 0x0600057A RID: 1402 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void SetApplicationMuted(bool muted)
		{
		}

		// Token: 0x0600057B RID: 1403 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void SetApplicationVolume(float volume)
		{
		}

		// Token: 0x0600057C RID: 1404 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void SetiOSAppPauseOnBackground(bool pause)
		{
		}

		// Token: 0x0600057D RID: 1405 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void CreateBannerView(string adUnitId, AdSize adSize, AdPosition position)
		{
		}

		// Token: 0x0600057E RID: 1406 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void CreateBannerView(string adUnitId, AdSize adSize, int positionX, int positionY)
		{
		}

		// Token: 0x0600057F RID: 1407 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void LoadAd(AdRequest request)
		{
		}

		// Token: 0x06000580 RID: 1408 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void ShowBannerView()
		{
		}

		// Token: 0x06000581 RID: 1409 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void HideBannerView()
		{
		}

		// Token: 0x06000582 RID: 1410 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void DestroyBannerView()
		{
		}

		// Token: 0x06000583 RID: 1411 RVA: 0x00021B88 File Offset: 0x0001FD88
		public float GetHeightInPixels()
		{
			return 0f;
		}

		// Token: 0x06000584 RID: 1412 RVA: 0x00021B88 File Offset: 0x0001FD88
		public float GetWidthInPixels()
		{
			return 0f;
		}

		// Token: 0x06000585 RID: 1413 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void SetPosition(AdPosition adPosition)
		{
		}

		// Token: 0x06000586 RID: 1414 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void SetPosition(int x, int y)
		{
		}

		// Token: 0x06000587 RID: 1415 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void CreateInterstitialAd(string adUnitId)
		{
		}

		// Token: 0x06000588 RID: 1416 RVA: 0x00021B8F File Offset: 0x0001FD8F
		public bool IsLoaded()
		{
			return true;
		}

		// Token: 0x06000589 RID: 1417 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void ShowInterstitial()
		{
		}

		// Token: 0x0600058A RID: 1418 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void DestroyInterstitial()
		{
		}

		// Token: 0x0600058B RID: 1419 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void CreateRewardBasedVideoAd()
		{
		}

		// Token: 0x0600058C RID: 1420 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void SetUserId(string userId)
		{
		}

		// Token: 0x0600058D RID: 1421 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void LoadAd(AdRequest request, string adUnitId)
		{
		}

		// Token: 0x0600058E RID: 1422 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void DestroyRewardBasedVideoAd()
		{
		}

		// Token: 0x0600058F RID: 1423 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void ShowRewardBasedVideoAd()
		{
		}

		// Token: 0x06000590 RID: 1424 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void CreateAdLoader(AdLoader.Builder builder)
		{
		}

		// Token: 0x06000591 RID: 1425 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void Load(AdRequest request)
		{
		}

		// Token: 0x06000592 RID: 1426 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void SetAdSize(AdSize adSize)
		{
		}

		// Token: 0x06000593 RID: 1427 RVA: 0x0001E9D4 File Offset: 0x0001CBD4
		public string MediationAdapterClassName()
		{
			return null;
		}

		// Token: 0x040001D0 RID: 464
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdLoaded;

		// Token: 0x040001D1 RID: 465
		[CompilerGenerated]
		private EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad;

		// Token: 0x040001D2 RID: 466
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdOpening;

		// Token: 0x040001D3 RID: 467
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdStarted;

		// Token: 0x040001D4 RID: 468
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdClosed;

		// Token: 0x040001D5 RID: 469
		[CompilerGenerated]
		private EventHandler<Reward> OnAdRewarded;

		// Token: 0x040001D6 RID: 470
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdLeavingApplication;

		// Token: 0x040001D7 RID: 471
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdCompleted;

		// Token: 0x040001D8 RID: 472
		[CompilerGenerated]
		private EventHandler<CustomNativeEventArgs> OnCustomNativeTemplateAdLoaded;
	}
}
