﻿using System;

namespace GoogleMobileAds.Common
{
	// Token: 0x02000080 RID: 128
	public interface IMobileAdsClient
	{
		// Token: 0x060005C4 RID: 1476
		void Initialize(string appId);

		// Token: 0x060005C5 RID: 1477
		void SetApplicationVolume(float volume);

		// Token: 0x060005C6 RID: 1478
		void SetApplicationMuted(bool muted);

		// Token: 0x060005C7 RID: 1479
		void SetiOSAppPauseOnBackground(bool pause);
	}
}
