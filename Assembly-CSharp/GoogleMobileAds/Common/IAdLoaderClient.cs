﻿using System;
using GoogleMobileAds.Api;

namespace GoogleMobileAds.Common
{
	// Token: 0x0200007C RID: 124
	public interface IAdLoaderClient
	{
		// Token: 0x14000025 RID: 37
		// (add) Token: 0x06000594 RID: 1428
		// (remove) Token: 0x06000595 RID: 1429
		event EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad;

		// Token: 0x14000026 RID: 38
		// (add) Token: 0x06000596 RID: 1430
		// (remove) Token: 0x06000597 RID: 1431
		event EventHandler<CustomNativeEventArgs> OnCustomNativeTemplateAdLoaded;

		// Token: 0x06000598 RID: 1432
		void LoadAd(AdRequest request);
	}
}
