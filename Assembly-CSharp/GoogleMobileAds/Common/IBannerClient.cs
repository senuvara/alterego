﻿using System;
using GoogleMobileAds.Api;

namespace GoogleMobileAds.Common
{
	// Token: 0x0200007D RID: 125
	public interface IBannerClient
	{
		// Token: 0x14000027 RID: 39
		// (add) Token: 0x06000599 RID: 1433
		// (remove) Token: 0x0600059A RID: 1434
		event EventHandler<EventArgs> OnAdLoaded;

		// Token: 0x14000028 RID: 40
		// (add) Token: 0x0600059B RID: 1435
		// (remove) Token: 0x0600059C RID: 1436
		event EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad;

		// Token: 0x14000029 RID: 41
		// (add) Token: 0x0600059D RID: 1437
		// (remove) Token: 0x0600059E RID: 1438
		event EventHandler<EventArgs> OnAdOpening;

		// Token: 0x1400002A RID: 42
		// (add) Token: 0x0600059F RID: 1439
		// (remove) Token: 0x060005A0 RID: 1440
		event EventHandler<EventArgs> OnAdClosed;

		// Token: 0x1400002B RID: 43
		// (add) Token: 0x060005A1 RID: 1441
		// (remove) Token: 0x060005A2 RID: 1442
		event EventHandler<EventArgs> OnAdLeavingApplication;

		// Token: 0x060005A3 RID: 1443
		void CreateBannerView(string adUnitId, AdSize adSize, AdPosition position);

		// Token: 0x060005A4 RID: 1444
		void CreateBannerView(string adUnitId, AdSize adSize, int x, int y);

		// Token: 0x060005A5 RID: 1445
		void LoadAd(AdRequest request);

		// Token: 0x060005A6 RID: 1446
		void ShowBannerView();

		// Token: 0x060005A7 RID: 1447
		void HideBannerView();

		// Token: 0x060005A8 RID: 1448
		void DestroyBannerView();

		// Token: 0x060005A9 RID: 1449
		float GetHeightInPixels();

		// Token: 0x060005AA RID: 1450
		float GetWidthInPixels();

		// Token: 0x060005AB RID: 1451
		void SetPosition(AdPosition adPosition);

		// Token: 0x060005AC RID: 1452
		void SetPosition(int x, int y);

		// Token: 0x060005AD RID: 1453
		string MediationAdapterClassName();
	}
}
