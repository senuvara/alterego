﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GoogleMobileAds.Common
{
	// Token: 0x02000083 RID: 131
	public class MobileAdsEventExecutor : MonoBehaviour
	{
		// Token: 0x060005EF RID: 1519 RVA: 0x00021B92 File Offset: 0x0001FD92
		public static void Initialize()
		{
			if (MobileAdsEventExecutor.IsActive())
			{
				return;
			}
			GameObject gameObject = new GameObject("MobileAdsMainThreadExecuter");
			gameObject.hideFlags = HideFlags.HideAndDontSave;
			Object.DontDestroyOnLoad(gameObject);
			MobileAdsEventExecutor.instance = gameObject.AddComponent<MobileAdsEventExecutor>();
		}

		// Token: 0x060005F0 RID: 1520 RVA: 0x00021BBE File Offset: 0x0001FDBE
		public static bool IsActive()
		{
			return MobileAdsEventExecutor.instance != null;
		}

		// Token: 0x060005F1 RID: 1521 RVA: 0x00021BCB File Offset: 0x0001FDCB
		public void Awake()
		{
			Object.DontDestroyOnLoad(base.gameObject);
		}

		// Token: 0x060005F2 RID: 1522 RVA: 0x00021BD8 File Offset: 0x0001FDD8
		public static void ExecuteInUpdate(Action action)
		{
			List<Action> obj = MobileAdsEventExecutor.adEventsQueue;
			lock (obj)
			{
				MobileAdsEventExecutor.adEventsQueue.Add(action);
				MobileAdsEventExecutor.adEventsQueueEmpty = false;
			}
		}

		// Token: 0x060005F3 RID: 1523 RVA: 0x00021C24 File Offset: 0x0001FE24
		public void Update()
		{
			if (MobileAdsEventExecutor.adEventsQueueEmpty)
			{
				return;
			}
			List<Action> list = new List<Action>();
			List<Action> obj = MobileAdsEventExecutor.adEventsQueue;
			lock (obj)
			{
				list.AddRange(MobileAdsEventExecutor.adEventsQueue);
				MobileAdsEventExecutor.adEventsQueue.Clear();
				MobileAdsEventExecutor.adEventsQueueEmpty = true;
			}
			foreach (Action action in list)
			{
				action();
			}
		}

		// Token: 0x060005F4 RID: 1524 RVA: 0x00021CC4 File Offset: 0x0001FEC4
		public void OnDisable()
		{
			MobileAdsEventExecutor.instance = null;
		}

		// Token: 0x060005F5 RID: 1525 RVA: 0x000025AD File Offset: 0x000007AD
		public MobileAdsEventExecutor()
		{
		}

		// Token: 0x060005F6 RID: 1526 RVA: 0x00021CCC File Offset: 0x0001FECC
		// Note: this type is marked as 'beforefieldinit'.
		static MobileAdsEventExecutor()
		{
		}

		// Token: 0x040001D9 RID: 473
		public static MobileAdsEventExecutor instance = null;

		// Token: 0x040001DA RID: 474
		private static List<Action> adEventsQueue = new List<Action>();

		// Token: 0x040001DB RID: 475
		private static volatile bool adEventsQueueEmpty = true;
	}
}
