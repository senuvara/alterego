﻿using System;
using GoogleMobileAds.Api;

namespace GoogleMobileAds.Common
{
	// Token: 0x02000082 RID: 130
	public interface IRewardedAdClient
	{
		// Token: 0x14000039 RID: 57
		// (add) Token: 0x060005DE RID: 1502
		// (remove) Token: 0x060005DF RID: 1503
		event EventHandler<EventArgs> OnAdLoaded;

		// Token: 0x1400003A RID: 58
		// (add) Token: 0x060005E0 RID: 1504
		// (remove) Token: 0x060005E1 RID: 1505
		event EventHandler<AdErrorEventArgs> OnAdFailedToLoad;

		// Token: 0x1400003B RID: 59
		// (add) Token: 0x060005E2 RID: 1506
		// (remove) Token: 0x060005E3 RID: 1507
		event EventHandler<AdErrorEventArgs> OnAdFailedToShow;

		// Token: 0x1400003C RID: 60
		// (add) Token: 0x060005E4 RID: 1508
		// (remove) Token: 0x060005E5 RID: 1509
		event EventHandler<EventArgs> OnAdOpening;

		// Token: 0x1400003D RID: 61
		// (add) Token: 0x060005E6 RID: 1510
		// (remove) Token: 0x060005E7 RID: 1511
		event EventHandler<Reward> OnUserEarnedReward;

		// Token: 0x1400003E RID: 62
		// (add) Token: 0x060005E8 RID: 1512
		// (remove) Token: 0x060005E9 RID: 1513
		event EventHandler<EventArgs> OnAdClosed;

		// Token: 0x060005EA RID: 1514
		void CreateRewardedAd(string adUnitId);

		// Token: 0x060005EB RID: 1515
		void LoadAd(AdRequest request);

		// Token: 0x060005EC RID: 1516
		bool IsLoaded();

		// Token: 0x060005ED RID: 1517
		string MediationAdapterClassName();

		// Token: 0x060005EE RID: 1518
		void Show();
	}
}
