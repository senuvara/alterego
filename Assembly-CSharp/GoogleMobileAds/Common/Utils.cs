﻿using System;
using UnityEngine;

namespace GoogleMobileAds.Common
{
	// Token: 0x02000085 RID: 133
	internal class Utils
	{
		// Token: 0x06000609 RID: 1545 RVA: 0x00021F85 File Offset: 0x00020185
		public static void CheckInitialization()
		{
			if (!MobileAdsEventExecutor.IsActive())
			{
				global::Debug.Log("You intitialized an ad object but have not yet called MobileAds.Initialize(). We highly recommend you call MobileAds.Initialize() before interacting with the Google Mobile Ads SDK.");
			}
			MobileAdsEventExecutor.Initialize();
		}

		// Token: 0x0600060A RID: 1546 RVA: 0x00021F9D File Offset: 0x0002019D
		public static Texture2D GetTexture2DFromByteArray(byte[] img)
		{
			Texture2D texture2D = new Texture2D(1, 1);
			if (!texture2D.LoadImage(img))
			{
				throw new InvalidOperationException("Could not load custom native template\n                        image asset as texture");
			}
			return texture2D;
		}

		// Token: 0x0600060B RID: 1547 RVA: 0x000043CE File Offset: 0x000025CE
		public Utils()
		{
		}
	}
}
