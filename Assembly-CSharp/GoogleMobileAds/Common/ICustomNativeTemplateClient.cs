﻿using System;
using System.Collections.Generic;

namespace GoogleMobileAds.Common
{
	// Token: 0x0200007E RID: 126
	public interface ICustomNativeTemplateClient
	{
		// Token: 0x060005AE RID: 1454
		string GetTemplateId();

		// Token: 0x060005AF RID: 1455
		byte[] GetImageByteArray(string key);

		// Token: 0x060005B0 RID: 1456
		List<string> GetAvailableAssetNames();

		// Token: 0x060005B1 RID: 1457
		string GetText(string key);

		// Token: 0x060005B2 RID: 1458
		void PerformClick(string assetName);

		// Token: 0x060005B3 RID: 1459
		void RecordImpression();
	}
}
