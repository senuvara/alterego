﻿using System;
using GoogleMobileAds.Api;

namespace GoogleMobileAds.Common
{
	// Token: 0x02000081 RID: 129
	public interface IRewardBasedVideoAdClient
	{
		// Token: 0x14000031 RID: 49
		// (add) Token: 0x060005C8 RID: 1480
		// (remove) Token: 0x060005C9 RID: 1481
		event EventHandler<EventArgs> OnAdLoaded;

		// Token: 0x14000032 RID: 50
		// (add) Token: 0x060005CA RID: 1482
		// (remove) Token: 0x060005CB RID: 1483
		event EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad;

		// Token: 0x14000033 RID: 51
		// (add) Token: 0x060005CC RID: 1484
		// (remove) Token: 0x060005CD RID: 1485
		event EventHandler<EventArgs> OnAdOpening;

		// Token: 0x14000034 RID: 52
		// (add) Token: 0x060005CE RID: 1486
		// (remove) Token: 0x060005CF RID: 1487
		event EventHandler<EventArgs> OnAdStarted;

		// Token: 0x14000035 RID: 53
		// (add) Token: 0x060005D0 RID: 1488
		// (remove) Token: 0x060005D1 RID: 1489
		event EventHandler<Reward> OnAdRewarded;

		// Token: 0x14000036 RID: 54
		// (add) Token: 0x060005D2 RID: 1490
		// (remove) Token: 0x060005D3 RID: 1491
		event EventHandler<EventArgs> OnAdClosed;

		// Token: 0x14000037 RID: 55
		// (add) Token: 0x060005D4 RID: 1492
		// (remove) Token: 0x060005D5 RID: 1493
		event EventHandler<EventArgs> OnAdLeavingApplication;

		// Token: 0x14000038 RID: 56
		// (add) Token: 0x060005D6 RID: 1494
		// (remove) Token: 0x060005D7 RID: 1495
		event EventHandler<EventArgs> OnAdCompleted;

		// Token: 0x060005D8 RID: 1496
		void CreateRewardBasedVideoAd();

		// Token: 0x060005D9 RID: 1497
		void LoadAd(AdRequest request, string adUnitId);

		// Token: 0x060005DA RID: 1498
		bool IsLoaded();

		// Token: 0x060005DB RID: 1499
		string MediationAdapterClassName();

		// Token: 0x060005DC RID: 1500
		void ShowRewardBasedVideoAd();

		// Token: 0x060005DD RID: 1501
		void SetUserId(string userId);
	}
}
