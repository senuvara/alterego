﻿using System;
using GoogleMobileAds.Android;
using GoogleMobileAds.Api;
using GoogleMobileAds.Common;

namespace GoogleMobileAds
{
	// Token: 0x02000072 RID: 114
	public class GoogleMobileAdsClientFactory
	{
		// Token: 0x060004D7 RID: 1239 RVA: 0x0001FB3E File Offset: 0x0001DD3E
		public static IBannerClient BuildBannerClient()
		{
			return new BannerClient();
		}

		// Token: 0x060004D8 RID: 1240 RVA: 0x0001FB45 File Offset: 0x0001DD45
		public static IInterstitialClient BuildInterstitialClient()
		{
			return new InterstitialClient();
		}

		// Token: 0x060004D9 RID: 1241 RVA: 0x0001FB4C File Offset: 0x0001DD4C
		public static IRewardBasedVideoAdClient BuildRewardBasedVideoAdClient()
		{
			return new RewardBasedVideoAdClient();
		}

		// Token: 0x060004DA RID: 1242 RVA: 0x0001FB53 File Offset: 0x0001DD53
		public static IRewardedAdClient BuildRewardedAdClient()
		{
			return new RewardedAdClient();
		}

		// Token: 0x060004DB RID: 1243 RVA: 0x0001FB5A File Offset: 0x0001DD5A
		public static IAdLoaderClient BuildAdLoaderClient(AdLoader adLoader)
		{
			return new AdLoaderClient(adLoader);
		}

		// Token: 0x060004DC RID: 1244 RVA: 0x0001FB62 File Offset: 0x0001DD62
		public static IMobileAdsClient MobileAdsInstance()
		{
			return MobileAdsClient.Instance;
		}

		// Token: 0x060004DD RID: 1245 RVA: 0x000043CE File Offset: 0x000025CE
		public GoogleMobileAdsClientFactory()
		{
		}
	}
}
