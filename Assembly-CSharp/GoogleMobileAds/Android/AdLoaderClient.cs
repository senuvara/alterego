﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using GoogleMobileAds.Api;
using GoogleMobileAds.Common;
using UnityEngine;

namespace GoogleMobileAds.Android
{
	// Token: 0x02000073 RID: 115
	public class AdLoaderClient : AndroidJavaProxy, IAdLoaderClient
	{
		// Token: 0x1700006D RID: 109
		// (get) Token: 0x060004DE RID: 1246 RVA: 0x0001FB69 File Offset: 0x0001DD69
		// (set) Token: 0x060004DF RID: 1247 RVA: 0x0001FB71 File Offset: 0x0001DD71
		private Dictionary<string, Action<CustomNativeTemplateAd, string>> CustomNativeTemplateCallbacks
		{
			[CompilerGenerated]
			get
			{
				return this.<CustomNativeTemplateCallbacks>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<CustomNativeTemplateCallbacks>k__BackingField = value;
			}
		}

		// Token: 0x14000002 RID: 2
		// (add) Token: 0x060004E0 RID: 1248 RVA: 0x0001FB7C File Offset: 0x0001DD7C
		// (remove) Token: 0x060004E1 RID: 1249 RVA: 0x0001FBB4 File Offset: 0x0001DDB4
		public event EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad
		{
			[CompilerGenerated]
			add
			{
				EventHandler<AdFailedToLoadEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdFailedToLoadEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdFailedToLoadEventArgs> value2 = (EventHandler<AdFailedToLoadEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdFailedToLoadEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<AdFailedToLoadEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdFailedToLoadEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdFailedToLoadEventArgs> value2 = (EventHandler<AdFailedToLoadEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdFailedToLoadEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000003 RID: 3
		// (add) Token: 0x060004E2 RID: 1250 RVA: 0x0001FBEC File Offset: 0x0001DDEC
		// (remove) Token: 0x060004E3 RID: 1251 RVA: 0x0001FC24 File Offset: 0x0001DE24
		public event EventHandler<CustomNativeEventArgs> OnCustomNativeTemplateAdLoaded
		{
			[CompilerGenerated]
			add
			{
				EventHandler<CustomNativeEventArgs> eventHandler = this.OnCustomNativeTemplateAdLoaded;
				EventHandler<CustomNativeEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<CustomNativeEventArgs> value2 = (EventHandler<CustomNativeEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<CustomNativeEventArgs>>(ref this.OnCustomNativeTemplateAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<CustomNativeEventArgs> eventHandler = this.OnCustomNativeTemplateAdLoaded;
				EventHandler<CustomNativeEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<CustomNativeEventArgs> value2 = (EventHandler<CustomNativeEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<CustomNativeEventArgs>>(ref this.OnCustomNativeTemplateAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x060004E4 RID: 1252 RVA: 0x0001FC5C File Offset: 0x0001DE5C
		public AdLoaderClient(AdLoader unityAdLoader) : base("com.google.unity.ads.UnityAdLoaderListener")
		{
			AndroidJavaObject @static = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
			this.adLoader = new AndroidJavaObject("com.google.unity.ads.NativeAdLoader", new object[]
			{
				@static,
				unityAdLoader.AdUnitId,
				this
			});
			this.CustomNativeTemplateCallbacks = unityAdLoader.CustomNativeTemplateClickHandlers;
			if (unityAdLoader.AdTypes.Contains(NativeAdType.CustomTemplate))
			{
				foreach (string text in unityAdLoader.TemplateIds)
				{
					this.adLoader.Call("configureCustomNativeTemplateAd", new object[]
					{
						text,
						this.CustomNativeTemplateCallbacks.ContainsKey(text)
					});
				}
			}
			this.adLoader.Call("create", Array.Empty<object>());
		}

		// Token: 0x060004E5 RID: 1253 RVA: 0x0001FD4C File Offset: 0x0001DF4C
		public void LoadAd(AdRequest request)
		{
			this.adLoader.Call("loadAd", new object[]
			{
				Utils.GetAdRequestJavaObject(request)
			});
		}

		// Token: 0x060004E6 RID: 1254 RVA: 0x0001FD70 File Offset: 0x0001DF70
		public void onCustomTemplateAdLoaded(AndroidJavaObject ad)
		{
			if (this.OnCustomNativeTemplateAdLoaded != null)
			{
				CustomNativeEventArgs e = new CustomNativeEventArgs
				{
					nativeAd = new CustomNativeTemplateAd(new CustomNativeTemplateClient(ad))
				};
				this.OnCustomNativeTemplateAdLoaded(this, e);
			}
		}

		// Token: 0x060004E7 RID: 1255 RVA: 0x0001FDAC File Offset: 0x0001DFAC
		private void onAdFailedToLoad(string errorReason)
		{
			AdFailedToLoadEventArgs e = new AdFailedToLoadEventArgs
			{
				Message = errorReason
			};
			this.OnAdFailedToLoad(this, e);
		}

		// Token: 0x060004E8 RID: 1256 RVA: 0x0001FDD4 File Offset: 0x0001DFD4
		public void onCustomClick(AndroidJavaObject ad, string assetName)
		{
			CustomNativeTemplateAd customNativeTemplateAd = new CustomNativeTemplateAd(new CustomNativeTemplateClient(ad));
			this.CustomNativeTemplateCallbacks[customNativeTemplateAd.GetCustomTemplateId()](customNativeTemplateAd, assetName);
		}

		// Token: 0x04000199 RID: 409
		private AndroidJavaObject adLoader;

		// Token: 0x0400019A RID: 410
		[CompilerGenerated]
		private Dictionary<string, Action<CustomNativeTemplateAd, string>> <CustomNativeTemplateCallbacks>k__BackingField;

		// Token: 0x0400019B RID: 411
		[CompilerGenerated]
		private EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad;

		// Token: 0x0400019C RID: 412
		[CompilerGenerated]
		private EventHandler<CustomNativeEventArgs> OnCustomNativeTemplateAdLoaded;
	}
}
