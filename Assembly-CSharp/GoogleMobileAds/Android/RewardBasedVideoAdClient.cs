﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;
using GoogleMobileAds.Api;
using GoogleMobileAds.Common;
using UnityEngine;

namespace GoogleMobileAds.Android
{
	// Token: 0x02000078 RID: 120
	public class RewardBasedVideoAdClient : AndroidJavaProxy, IRewardBasedVideoAdClient
	{
		// Token: 0x1400000E RID: 14
		// (add) Token: 0x06000528 RID: 1320 RVA: 0x000207AC File Offset: 0x0001E9AC
		// (remove) Token: 0x06000529 RID: 1321 RVA: 0x000207E4 File Offset: 0x0001E9E4
		public event EventHandler<EventArgs> OnAdLoaded
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLoaded;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLoaded;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x1400000F RID: 15
		// (add) Token: 0x0600052A RID: 1322 RVA: 0x0002081C File Offset: 0x0001EA1C
		// (remove) Token: 0x0600052B RID: 1323 RVA: 0x00020854 File Offset: 0x0001EA54
		public event EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad
		{
			[CompilerGenerated]
			add
			{
				EventHandler<AdFailedToLoadEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdFailedToLoadEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdFailedToLoadEventArgs> value2 = (EventHandler<AdFailedToLoadEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdFailedToLoadEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<AdFailedToLoadEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdFailedToLoadEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdFailedToLoadEventArgs> value2 = (EventHandler<AdFailedToLoadEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdFailedToLoadEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000010 RID: 16
		// (add) Token: 0x0600052C RID: 1324 RVA: 0x0002088C File Offset: 0x0001EA8C
		// (remove) Token: 0x0600052D RID: 1325 RVA: 0x000208C4 File Offset: 0x0001EAC4
		public event EventHandler<EventArgs> OnAdOpening
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdOpening;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdOpening, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdOpening;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdOpening, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000011 RID: 17
		// (add) Token: 0x0600052E RID: 1326 RVA: 0x000208FC File Offset: 0x0001EAFC
		// (remove) Token: 0x0600052F RID: 1327 RVA: 0x00020934 File Offset: 0x0001EB34
		public event EventHandler<EventArgs> OnAdStarted
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdStarted;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdStarted, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdStarted;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdStarted, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000012 RID: 18
		// (add) Token: 0x06000530 RID: 1328 RVA: 0x0002096C File Offset: 0x0001EB6C
		// (remove) Token: 0x06000531 RID: 1329 RVA: 0x000209A4 File Offset: 0x0001EBA4
		public event EventHandler<EventArgs> OnAdClosed
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdClosed;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdClosed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdClosed;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdClosed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000013 RID: 19
		// (add) Token: 0x06000532 RID: 1330 RVA: 0x000209DC File Offset: 0x0001EBDC
		// (remove) Token: 0x06000533 RID: 1331 RVA: 0x00020A14 File Offset: 0x0001EC14
		public event EventHandler<Reward> OnAdRewarded
		{
			[CompilerGenerated]
			add
			{
				EventHandler<Reward> eventHandler = this.OnAdRewarded;
				EventHandler<Reward> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<Reward> value2 = (EventHandler<Reward>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<Reward>>(ref this.OnAdRewarded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<Reward> eventHandler = this.OnAdRewarded;
				EventHandler<Reward> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<Reward> value2 = (EventHandler<Reward>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<Reward>>(ref this.OnAdRewarded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000014 RID: 20
		// (add) Token: 0x06000534 RID: 1332 RVA: 0x00020A4C File Offset: 0x0001EC4C
		// (remove) Token: 0x06000535 RID: 1333 RVA: 0x00020A84 File Offset: 0x0001EC84
		public event EventHandler<EventArgs> OnAdLeavingApplication
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLeavingApplication;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLeavingApplication, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLeavingApplication;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLeavingApplication, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000015 RID: 21
		// (add) Token: 0x06000536 RID: 1334 RVA: 0x00020ABC File Offset: 0x0001ECBC
		// (remove) Token: 0x06000537 RID: 1335 RVA: 0x00020AF4 File Offset: 0x0001ECF4
		public event EventHandler<EventArgs> OnAdCompleted
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdCompleted;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdCompleted, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdCompleted;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdCompleted, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x06000538 RID: 1336 RVA: 0x00020B2C File Offset: 0x0001ED2C
		public RewardBasedVideoAdClient() : base("com.google.unity.ads.UnityRewardBasedVideoAdListener")
		{
			AndroidJavaObject @static = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
			this.androidRewardBasedVideo = new AndroidJavaObject("com.google.unity.ads.RewardBasedVideo", new object[]
			{
				@static,
				this
			});
		}

		// Token: 0x06000539 RID: 1337 RVA: 0x00020C9F File Offset: 0x0001EE9F
		public void CreateRewardBasedVideoAd()
		{
			this.androidRewardBasedVideo.Call("create", Array.Empty<object>());
		}

		// Token: 0x0600053A RID: 1338 RVA: 0x00020CB6 File Offset: 0x0001EEB6
		public void LoadAd(AdRequest request, string adUnitId)
		{
			this.androidRewardBasedVideo.Call("loadAd", new object[]
			{
				Utils.GetAdRequestJavaObject(request),
				adUnitId
			});
		}

		// Token: 0x0600053B RID: 1339 RVA: 0x00020CDB File Offset: 0x0001EEDB
		public bool IsLoaded()
		{
			return this.androidRewardBasedVideo.Call<bool>("isLoaded", Array.Empty<object>());
		}

		// Token: 0x0600053C RID: 1340 RVA: 0x00020CF2 File Offset: 0x0001EEF2
		public void ShowRewardBasedVideoAd()
		{
			this.androidRewardBasedVideo.Call("show", Array.Empty<object>());
		}

		// Token: 0x0600053D RID: 1341 RVA: 0x00020D09 File Offset: 0x0001EF09
		public void SetUserId(string userId)
		{
			this.androidRewardBasedVideo.Call("setUserId", new object[]
			{
				userId
			});
		}

		// Token: 0x0600053E RID: 1342 RVA: 0x00020D25 File Offset: 0x0001EF25
		public void DestroyRewardBasedVideoAd()
		{
			this.androidRewardBasedVideo.Call("destroy", Array.Empty<object>());
		}

		// Token: 0x0600053F RID: 1343 RVA: 0x00020D3C File Offset: 0x0001EF3C
		public string MediationAdapterClassName()
		{
			return this.androidRewardBasedVideo.Call<string>("getMediationAdapterClassName", Array.Empty<object>());
		}

		// Token: 0x06000540 RID: 1344 RVA: 0x00020D53 File Offset: 0x0001EF53
		private void onAdLoaded()
		{
			if (this.OnAdLoaded != null)
			{
				this.OnAdLoaded(this, EventArgs.Empty);
			}
		}

		// Token: 0x06000541 RID: 1345 RVA: 0x00020D70 File Offset: 0x0001EF70
		private void onAdFailedToLoad(string errorReason)
		{
			if (this.OnAdFailedToLoad != null)
			{
				AdFailedToLoadEventArgs e = new AdFailedToLoadEventArgs
				{
					Message = errorReason
				};
				this.OnAdFailedToLoad(this, e);
			}
		}

		// Token: 0x06000542 RID: 1346 RVA: 0x00020D9F File Offset: 0x0001EF9F
		private void onAdOpened()
		{
			if (this.OnAdOpening != null)
			{
				this.OnAdOpening(this, EventArgs.Empty);
			}
		}

		// Token: 0x06000543 RID: 1347 RVA: 0x00020DBA File Offset: 0x0001EFBA
		private void onAdStarted()
		{
			if (this.OnAdStarted != null)
			{
				this.OnAdStarted(this, EventArgs.Empty);
			}
		}

		// Token: 0x06000544 RID: 1348 RVA: 0x00020DD5 File Offset: 0x0001EFD5
		private void onAdClosed()
		{
			if (this.OnAdClosed != null)
			{
				this.OnAdClosed(this, EventArgs.Empty);
			}
		}

		// Token: 0x06000545 RID: 1349 RVA: 0x00020DF0 File Offset: 0x0001EFF0
		private void onAdRewarded(string type, float amount)
		{
			if (this.OnAdRewarded != null)
			{
				Reward e = new Reward
				{
					Type = type,
					Amount = (double)amount
				};
				this.OnAdRewarded(this, e);
			}
		}

		// Token: 0x06000546 RID: 1350 RVA: 0x00020E27 File Offset: 0x0001F027
		private void onAdLeftApplication()
		{
			if (this.OnAdLeavingApplication != null)
			{
				this.OnAdLeavingApplication(this, EventArgs.Empty);
			}
		}

		// Token: 0x06000547 RID: 1351 RVA: 0x00020E42 File Offset: 0x0001F042
		private void onAdCompleted()
		{
			if (this.OnAdCompleted != null)
			{
				this.OnAdCompleted(this, EventArgs.Empty);
			}
		}

		// Token: 0x040001AB RID: 427
		private AndroidJavaObject androidRewardBasedVideo;

		// Token: 0x040001AC RID: 428
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdLoaded = delegate(object <p0>, EventArgs <p1>)
		{
		};

		// Token: 0x040001AD RID: 429
		[CompilerGenerated]
		private EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad = delegate(object <p0>, AdFailedToLoadEventArgs <p1>)
		{
		};

		// Token: 0x040001AE RID: 430
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdOpening = delegate(object <p0>, EventArgs <p1>)
		{
		};

		// Token: 0x040001AF RID: 431
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdStarted = delegate(object <p0>, EventArgs <p1>)
		{
		};

		// Token: 0x040001B0 RID: 432
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdClosed = delegate(object <p0>, EventArgs <p1>)
		{
		};

		// Token: 0x040001B1 RID: 433
		[CompilerGenerated]
		private EventHandler<Reward> OnAdRewarded = delegate(object <p0>, Reward <p1>)
		{
		};

		// Token: 0x040001B2 RID: 434
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdLeavingApplication = delegate(object <p0>, EventArgs <p1>)
		{
		};

		// Token: 0x040001B3 RID: 435
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdCompleted = delegate(object <p0>, EventArgs <p1>)
		{
		};

		// Token: 0x0200015A RID: 346
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000A37 RID: 2615 RVA: 0x0003117C File Offset: 0x0002F37C
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000A38 RID: 2616 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c()
			{
			}

			// Token: 0x06000A39 RID: 2617 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
			internal void <.ctor>b__25_0(object <p0>, EventArgs <p1>)
			{
			}

			// Token: 0x06000A3A RID: 2618 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
			internal void <.ctor>b__25_1(object <p0>, AdFailedToLoadEventArgs <p1>)
			{
			}

			// Token: 0x06000A3B RID: 2619 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
			internal void <.ctor>b__25_2(object <p0>, EventArgs <p1>)
			{
			}

			// Token: 0x06000A3C RID: 2620 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
			internal void <.ctor>b__25_3(object <p0>, EventArgs <p1>)
			{
			}

			// Token: 0x06000A3D RID: 2621 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
			internal void <.ctor>b__25_4(object <p0>, EventArgs <p1>)
			{
			}

			// Token: 0x06000A3E RID: 2622 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
			internal void <.ctor>b__25_5(object <p0>, Reward <p1>)
			{
			}

			// Token: 0x06000A3F RID: 2623 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
			internal void <.ctor>b__25_6(object <p0>, EventArgs <p1>)
			{
			}

			// Token: 0x06000A40 RID: 2624 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
			internal void <.ctor>b__25_7(object <p0>, EventArgs <p1>)
			{
			}

			// Token: 0x0400052B RID: 1323
			public static readonly RewardBasedVideoAdClient.<>c <>9 = new RewardBasedVideoAdClient.<>c();

			// Token: 0x0400052C RID: 1324
			public static EventHandler<EventArgs> <>9__25_0;

			// Token: 0x0400052D RID: 1325
			public static EventHandler<AdFailedToLoadEventArgs> <>9__25_1;

			// Token: 0x0400052E RID: 1326
			public static EventHandler<EventArgs> <>9__25_2;

			// Token: 0x0400052F RID: 1327
			public static EventHandler<EventArgs> <>9__25_3;

			// Token: 0x04000530 RID: 1328
			public static EventHandler<EventArgs> <>9__25_4;

			// Token: 0x04000531 RID: 1329
			public static EventHandler<Reward> <>9__25_5;

			// Token: 0x04000532 RID: 1330
			public static EventHandler<EventArgs> <>9__25_6;

			// Token: 0x04000533 RID: 1331
			public static EventHandler<EventArgs> <>9__25_7;
		}
	}
}
