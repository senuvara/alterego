﻿using System;
using System.Collections.Generic;
using GoogleMobileAds.Common;
using UnityEngine;

namespace GoogleMobileAds.Android
{
	// Token: 0x02000075 RID: 117
	internal class CustomNativeTemplateClient : ICustomNativeTemplateClient
	{
		// Token: 0x06000504 RID: 1284 RVA: 0x00020278 File Offset: 0x0001E478
		public CustomNativeTemplateClient(AndroidJavaObject customNativeAd)
		{
			this.customNativeAd = customNativeAd;
		}

		// Token: 0x06000505 RID: 1285 RVA: 0x00020287 File Offset: 0x0001E487
		public List<string> GetAvailableAssetNames()
		{
			return new List<string>(this.customNativeAd.Call<string[]>("getAvailableAssetNames", Array.Empty<object>()));
		}

		// Token: 0x06000506 RID: 1286 RVA: 0x000202A3 File Offset: 0x0001E4A3
		public string GetTemplateId()
		{
			return this.customNativeAd.Call<string>("getTemplateId", Array.Empty<object>());
		}

		// Token: 0x06000507 RID: 1287 RVA: 0x000202BC File Offset: 0x0001E4BC
		public byte[] GetImageByteArray(string key)
		{
			byte[] array = this.customNativeAd.Call<byte[]>("getImage", new object[]
			{
				key
			});
			if (array.Length == 0)
			{
				return null;
			}
			return array;
		}

		// Token: 0x06000508 RID: 1288 RVA: 0x000202EC File Offset: 0x0001E4EC
		public string GetText(string key)
		{
			string text = this.customNativeAd.Call<string>("getText", new object[]
			{
				key
			});
			if (text.Equals(string.Empty))
			{
				return null;
			}
			return text;
		}

		// Token: 0x06000509 RID: 1289 RVA: 0x00020324 File Offset: 0x0001E524
		public void PerformClick(string assetName)
		{
			this.customNativeAd.Call("performClick", new object[]
			{
				assetName
			});
		}

		// Token: 0x0600050A RID: 1290 RVA: 0x00020340 File Offset: 0x0001E540
		public void RecordImpression()
		{
			this.customNativeAd.Call("recordImpression", Array.Empty<object>());
		}

		// Token: 0x040001A3 RID: 419
		private AndroidJavaObject customNativeAd;
	}
}
