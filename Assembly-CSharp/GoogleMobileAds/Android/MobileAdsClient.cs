﻿using System;
using GoogleMobileAds.Common;
using UnityEngine;

namespace GoogleMobileAds.Android
{
	// Token: 0x02000077 RID: 119
	public class MobileAdsClient : IMobileAdsClient
	{
		// Token: 0x1700006E RID: 110
		// (get) Token: 0x06000521 RID: 1313 RVA: 0x00020708 File Offset: 0x0001E908
		public static MobileAdsClient Instance
		{
			get
			{
				return MobileAdsClient.instance;
			}
		}

		// Token: 0x06000522 RID: 1314 RVA: 0x00020710 File Offset: 0x0001E910
		public void Initialize(string appId)
		{
			AndroidJavaObject @static = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
			new AndroidJavaClass("com.google.android.gms.ads.MobileAds").CallStatic("initialize", new object[]
			{
				@static,
				appId
			});
		}

		// Token: 0x06000523 RID: 1315 RVA: 0x00020754 File Offset: 0x0001E954
		public void SetApplicationVolume(float volume)
		{
			new AndroidJavaClass("com.google.android.gms.ads.MobileAds").CallStatic("setAppVolume", new object[]
			{
				volume
			});
		}

		// Token: 0x06000524 RID: 1316 RVA: 0x00020779 File Offset: 0x0001E979
		public void SetApplicationMuted(bool muted)
		{
			new AndroidJavaClass("com.google.android.gms.ads.MobileAds").CallStatic("setAppMuted", new object[]
			{
				muted
			});
		}

		// Token: 0x06000525 RID: 1317 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		public void SetiOSAppPauseOnBackground(bool pause)
		{
		}

		// Token: 0x06000526 RID: 1318 RVA: 0x000043CE File Offset: 0x000025CE
		public MobileAdsClient()
		{
		}

		// Token: 0x06000527 RID: 1319 RVA: 0x0002079E File Offset: 0x0001E99E
		// Note: this type is marked as 'beforefieldinit'.
		static MobileAdsClient()
		{
		}

		// Token: 0x040001AA RID: 426
		private static MobileAdsClient instance = new MobileAdsClient();
	}
}
