﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;
using GoogleMobileAds.Api;
using GoogleMobileAds.Common;
using UnityEngine;

namespace GoogleMobileAds.Android
{
	// Token: 0x02000079 RID: 121
	public class RewardedAdClient : AndroidJavaProxy, IRewardedAdClient
	{
		// Token: 0x06000548 RID: 1352 RVA: 0x00020E60 File Offset: 0x0001F060
		public RewardedAdClient() : base("com.google.unity.ads.UnityRewardedAdCallback")
		{
			AndroidJavaObject @static = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
			this.androidRewardedAd = new AndroidJavaObject("com.google.unity.ads.UnityRewardedAd", new object[]
			{
				@static,
				this
			});
		}

		// Token: 0x14000016 RID: 22
		// (add) Token: 0x06000549 RID: 1353 RVA: 0x00020EAC File Offset: 0x0001F0AC
		// (remove) Token: 0x0600054A RID: 1354 RVA: 0x00020EE4 File Offset: 0x0001F0E4
		public event EventHandler<EventArgs> OnAdLoaded
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLoaded;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLoaded;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000017 RID: 23
		// (add) Token: 0x0600054B RID: 1355 RVA: 0x00020F1C File Offset: 0x0001F11C
		// (remove) Token: 0x0600054C RID: 1356 RVA: 0x00020F54 File Offset: 0x0001F154
		public event EventHandler<AdErrorEventArgs> OnAdFailedToLoad
		{
			[CompilerGenerated]
			add
			{
				EventHandler<AdErrorEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdErrorEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdErrorEventArgs> value2 = (EventHandler<AdErrorEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdErrorEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<AdErrorEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdErrorEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdErrorEventArgs> value2 = (EventHandler<AdErrorEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdErrorEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000018 RID: 24
		// (add) Token: 0x0600054D RID: 1357 RVA: 0x00020F8C File Offset: 0x0001F18C
		// (remove) Token: 0x0600054E RID: 1358 RVA: 0x00020FC4 File Offset: 0x0001F1C4
		public event EventHandler<AdErrorEventArgs> OnAdFailedToShow
		{
			[CompilerGenerated]
			add
			{
				EventHandler<AdErrorEventArgs> eventHandler = this.OnAdFailedToShow;
				EventHandler<AdErrorEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdErrorEventArgs> value2 = (EventHandler<AdErrorEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdErrorEventArgs>>(ref this.OnAdFailedToShow, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<AdErrorEventArgs> eventHandler = this.OnAdFailedToShow;
				EventHandler<AdErrorEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdErrorEventArgs> value2 = (EventHandler<AdErrorEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdErrorEventArgs>>(ref this.OnAdFailedToShow, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000019 RID: 25
		// (add) Token: 0x0600054F RID: 1359 RVA: 0x00020FFC File Offset: 0x0001F1FC
		// (remove) Token: 0x06000550 RID: 1360 RVA: 0x00021034 File Offset: 0x0001F234
		public event EventHandler<EventArgs> OnAdOpening
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdOpening;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdOpening, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdOpening;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdOpening, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x1400001A RID: 26
		// (add) Token: 0x06000551 RID: 1361 RVA: 0x0002106C File Offset: 0x0001F26C
		// (remove) Token: 0x06000552 RID: 1362 RVA: 0x000210A4 File Offset: 0x0001F2A4
		public event EventHandler<Reward> OnUserEarnedReward
		{
			[CompilerGenerated]
			add
			{
				EventHandler<Reward> eventHandler = this.OnUserEarnedReward;
				EventHandler<Reward> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<Reward> value2 = (EventHandler<Reward>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<Reward>>(ref this.OnUserEarnedReward, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<Reward> eventHandler = this.OnUserEarnedReward;
				EventHandler<Reward> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<Reward> value2 = (EventHandler<Reward>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<Reward>>(ref this.OnUserEarnedReward, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x1400001B RID: 27
		// (add) Token: 0x06000553 RID: 1363 RVA: 0x000210DC File Offset: 0x0001F2DC
		// (remove) Token: 0x06000554 RID: 1364 RVA: 0x00021114 File Offset: 0x0001F314
		public event EventHandler<EventArgs> OnAdClosed
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdClosed;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdClosed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdClosed;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdClosed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x06000555 RID: 1365 RVA: 0x00021149 File Offset: 0x0001F349
		public void CreateRewardedAd(string adUnitId)
		{
			this.androidRewardedAd.Call("create", new object[]
			{
				adUnitId
			});
		}

		// Token: 0x06000556 RID: 1366 RVA: 0x00021165 File Offset: 0x0001F365
		public void LoadAd(AdRequest request)
		{
			this.androidRewardedAd.Call("loadAd", new object[]
			{
				Utils.GetAdRequestJavaObject(request)
			});
		}

		// Token: 0x06000557 RID: 1367 RVA: 0x00021186 File Offset: 0x0001F386
		public bool IsLoaded()
		{
			return this.androidRewardedAd.Call<bool>("isLoaded", Array.Empty<object>());
		}

		// Token: 0x06000558 RID: 1368 RVA: 0x0002119D File Offset: 0x0001F39D
		public void Show()
		{
			this.androidRewardedAd.Call("show", Array.Empty<object>());
		}

		// Token: 0x06000559 RID: 1369 RVA: 0x000211B4 File Offset: 0x0001F3B4
		public void DestroyRewardBasedVideoAd()
		{
			this.androidRewardedAd.Call("destroy", Array.Empty<object>());
		}

		// Token: 0x0600055A RID: 1370 RVA: 0x000211CB File Offset: 0x0001F3CB
		public string MediationAdapterClassName()
		{
			return this.androidRewardedAd.Call<string>("getMediationAdapterClassName", Array.Empty<object>());
		}

		// Token: 0x0600055B RID: 1371 RVA: 0x000211E2 File Offset: 0x0001F3E2
		private void onRewardedAdLoaded()
		{
			if (this.OnAdLoaded != null)
			{
				this.OnAdLoaded(this, EventArgs.Empty);
			}
		}

		// Token: 0x0600055C RID: 1372 RVA: 0x00021200 File Offset: 0x0001F400
		private void onRewardedAdFailedToLoad(string errorReason)
		{
			if (this.OnAdFailedToLoad != null)
			{
				AdErrorEventArgs e = new AdErrorEventArgs
				{
					Message = errorReason
				};
				this.OnAdFailedToLoad(this, e);
			}
		}

		// Token: 0x0600055D RID: 1373 RVA: 0x00021230 File Offset: 0x0001F430
		private void onRewardedAdFailedToShow(string errorReason)
		{
			if (this.OnAdFailedToLoad != null)
			{
				AdErrorEventArgs e = new AdErrorEventArgs
				{
					Message = errorReason
				};
				this.OnAdFailedToShow(this, e);
			}
		}

		// Token: 0x0600055E RID: 1374 RVA: 0x0002125F File Offset: 0x0001F45F
		private void onRewardedAdOpened()
		{
			if (this.OnAdOpening != null)
			{
				this.OnAdOpening(this, EventArgs.Empty);
			}
		}

		// Token: 0x0600055F RID: 1375 RVA: 0x0002127A File Offset: 0x0001F47A
		private void onRewardedAdClosed()
		{
			if (this.OnAdClosed != null)
			{
				this.OnAdClosed(this, EventArgs.Empty);
			}
		}

		// Token: 0x06000560 RID: 1376 RVA: 0x00021298 File Offset: 0x0001F498
		private void onUserEarnedReward(string type, float amount)
		{
			if (this.OnUserEarnedReward != null)
			{
				Reward e = new Reward
				{
					Type = type,
					Amount = (double)amount
				};
				this.OnUserEarnedReward(this, e);
			}
		}

		// Token: 0x040001B4 RID: 436
		private AndroidJavaObject androidRewardedAd;

		// Token: 0x040001B5 RID: 437
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdLoaded;

		// Token: 0x040001B6 RID: 438
		[CompilerGenerated]
		private EventHandler<AdErrorEventArgs> OnAdFailedToLoad;

		// Token: 0x040001B7 RID: 439
		[CompilerGenerated]
		private EventHandler<AdErrorEventArgs> OnAdFailedToShow;

		// Token: 0x040001B8 RID: 440
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdOpening;

		// Token: 0x040001B9 RID: 441
		[CompilerGenerated]
		private EventHandler<Reward> OnUserEarnedReward;

		// Token: 0x040001BA RID: 442
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdClosed;
	}
}
