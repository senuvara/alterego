﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;
using GoogleMobileAds.Api;
using GoogleMobileAds.Common;
using UnityEngine;

namespace GoogleMobileAds.Android
{
	// Token: 0x02000074 RID: 116
	public class BannerClient : AndroidJavaProxy, IBannerClient
	{
		// Token: 0x060004E9 RID: 1257 RVA: 0x0001FE08 File Offset: 0x0001E008
		public BannerClient() : base("com.google.unity.ads.UnityAdListener")
		{
			AndroidJavaObject @static = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
			this.bannerView = new AndroidJavaObject("com.google.unity.ads.Banner", new object[]
			{
				@static,
				this
			});
		}

		// Token: 0x14000004 RID: 4
		// (add) Token: 0x060004EA RID: 1258 RVA: 0x0001FE54 File Offset: 0x0001E054
		// (remove) Token: 0x060004EB RID: 1259 RVA: 0x0001FE8C File Offset: 0x0001E08C
		public event EventHandler<EventArgs> OnAdLoaded
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLoaded;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLoaded;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000005 RID: 5
		// (add) Token: 0x060004EC RID: 1260 RVA: 0x0001FEC4 File Offset: 0x0001E0C4
		// (remove) Token: 0x060004ED RID: 1261 RVA: 0x0001FEFC File Offset: 0x0001E0FC
		public event EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad
		{
			[CompilerGenerated]
			add
			{
				EventHandler<AdFailedToLoadEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdFailedToLoadEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdFailedToLoadEventArgs> value2 = (EventHandler<AdFailedToLoadEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdFailedToLoadEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<AdFailedToLoadEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdFailedToLoadEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdFailedToLoadEventArgs> value2 = (EventHandler<AdFailedToLoadEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdFailedToLoadEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000006 RID: 6
		// (add) Token: 0x060004EE RID: 1262 RVA: 0x0001FF34 File Offset: 0x0001E134
		// (remove) Token: 0x060004EF RID: 1263 RVA: 0x0001FF6C File Offset: 0x0001E16C
		public event EventHandler<EventArgs> OnAdOpening
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdOpening;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdOpening, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdOpening;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdOpening, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000007 RID: 7
		// (add) Token: 0x060004F0 RID: 1264 RVA: 0x0001FFA4 File Offset: 0x0001E1A4
		// (remove) Token: 0x060004F1 RID: 1265 RVA: 0x0001FFDC File Offset: 0x0001E1DC
		public event EventHandler<EventArgs> OnAdClosed
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdClosed;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdClosed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdClosed;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdClosed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000008 RID: 8
		// (add) Token: 0x060004F2 RID: 1266 RVA: 0x00020014 File Offset: 0x0001E214
		// (remove) Token: 0x060004F3 RID: 1267 RVA: 0x0002004C File Offset: 0x0001E24C
		public event EventHandler<EventArgs> OnAdLeavingApplication
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLeavingApplication;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLeavingApplication, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLeavingApplication;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLeavingApplication, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x060004F4 RID: 1268 RVA: 0x00020081 File Offset: 0x0001E281
		public void CreateBannerView(string adUnitId, AdSize adSize, AdPosition position)
		{
			this.bannerView.Call("create", new object[]
			{
				adUnitId,
				Utils.GetAdSizeJavaObject(adSize),
				(int)position
			});
		}

		// Token: 0x060004F5 RID: 1269 RVA: 0x000200AF File Offset: 0x0001E2AF
		public void CreateBannerView(string adUnitId, AdSize adSize, int x, int y)
		{
			this.bannerView.Call("create", new object[]
			{
				adUnitId,
				Utils.GetAdSizeJavaObject(adSize),
				x,
				y
			});
		}

		// Token: 0x060004F6 RID: 1270 RVA: 0x000200E7 File Offset: 0x0001E2E7
		public void LoadAd(AdRequest request)
		{
			this.bannerView.Call("loadAd", new object[]
			{
				Utils.GetAdRequestJavaObject(request)
			});
		}

		// Token: 0x060004F7 RID: 1271 RVA: 0x00020108 File Offset: 0x0001E308
		public void ShowBannerView()
		{
			this.bannerView.Call("show", Array.Empty<object>());
		}

		// Token: 0x060004F8 RID: 1272 RVA: 0x0002011F File Offset: 0x0001E31F
		public void HideBannerView()
		{
			this.bannerView.Call("hide", Array.Empty<object>());
		}

		// Token: 0x060004F9 RID: 1273 RVA: 0x00020136 File Offset: 0x0001E336
		public void DestroyBannerView()
		{
			this.bannerView.Call("destroy", Array.Empty<object>());
		}

		// Token: 0x060004FA RID: 1274 RVA: 0x0002014D File Offset: 0x0001E34D
		public float GetHeightInPixels()
		{
			return this.bannerView.Call<float>("getHeightInPixels", Array.Empty<object>());
		}

		// Token: 0x060004FB RID: 1275 RVA: 0x00020164 File Offset: 0x0001E364
		public float GetWidthInPixels()
		{
			return this.bannerView.Call<float>("getWidthInPixels", Array.Empty<object>());
		}

		// Token: 0x060004FC RID: 1276 RVA: 0x0002017B File Offset: 0x0001E37B
		public void SetPosition(AdPosition adPosition)
		{
			this.bannerView.Call("setPosition", new object[]
			{
				(int)adPosition
			});
		}

		// Token: 0x060004FD RID: 1277 RVA: 0x0002019C File Offset: 0x0001E39C
		public void SetPosition(int x, int y)
		{
			this.bannerView.Call("setPosition", new object[]
			{
				x,
				y
			});
		}

		// Token: 0x060004FE RID: 1278 RVA: 0x000201C6 File Offset: 0x0001E3C6
		public string MediationAdapterClassName()
		{
			return this.bannerView.Call<string>("getMediationAdapterClassName", Array.Empty<object>());
		}

		// Token: 0x060004FF RID: 1279 RVA: 0x000201DD File Offset: 0x0001E3DD
		public void onAdLoaded()
		{
			if (this.OnAdLoaded != null)
			{
				this.OnAdLoaded(this, EventArgs.Empty);
			}
		}

		// Token: 0x06000500 RID: 1280 RVA: 0x000201F8 File Offset: 0x0001E3F8
		public void onAdFailedToLoad(string errorReason)
		{
			if (this.OnAdFailedToLoad != null)
			{
				AdFailedToLoadEventArgs e = new AdFailedToLoadEventArgs
				{
					Message = errorReason
				};
				this.OnAdFailedToLoad(this, e);
			}
		}

		// Token: 0x06000501 RID: 1281 RVA: 0x00020227 File Offset: 0x0001E427
		public void onAdOpened()
		{
			if (this.OnAdOpening != null)
			{
				this.OnAdOpening(this, EventArgs.Empty);
			}
		}

		// Token: 0x06000502 RID: 1282 RVA: 0x00020242 File Offset: 0x0001E442
		public void onAdClosed()
		{
			if (this.OnAdClosed != null)
			{
				this.OnAdClosed(this, EventArgs.Empty);
			}
		}

		// Token: 0x06000503 RID: 1283 RVA: 0x0002025D File Offset: 0x0001E45D
		public void onAdLeftApplication()
		{
			if (this.OnAdLeavingApplication != null)
			{
				this.OnAdLeavingApplication(this, EventArgs.Empty);
			}
		}

		// Token: 0x0400019D RID: 413
		private AndroidJavaObject bannerView;

		// Token: 0x0400019E RID: 414
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdLoaded;

		// Token: 0x0400019F RID: 415
		[CompilerGenerated]
		private EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad;

		// Token: 0x040001A0 RID: 416
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdOpening;

		// Token: 0x040001A1 RID: 417
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdClosed;

		// Token: 0x040001A2 RID: 418
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdLeavingApplication;
	}
}
