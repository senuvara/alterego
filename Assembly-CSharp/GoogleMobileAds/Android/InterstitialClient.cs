﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;
using GoogleMobileAds.Api;
using GoogleMobileAds.Common;
using UnityEngine;

namespace GoogleMobileAds.Android
{
	// Token: 0x02000076 RID: 118
	public class InterstitialClient : AndroidJavaProxy, IInterstitialClient
	{
		// Token: 0x0600050B RID: 1291 RVA: 0x00020358 File Offset: 0x0001E558
		public InterstitialClient() : base("com.google.unity.ads.UnityAdListener")
		{
			AndroidJavaObject @static = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
			this.interstitial = new AndroidJavaObject("com.google.unity.ads.Interstitial", new object[]
			{
				@static,
				this
			});
		}

		// Token: 0x14000009 RID: 9
		// (add) Token: 0x0600050C RID: 1292 RVA: 0x000203A4 File Offset: 0x0001E5A4
		// (remove) Token: 0x0600050D RID: 1293 RVA: 0x000203DC File Offset: 0x0001E5DC
		public event EventHandler<EventArgs> OnAdLoaded
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLoaded;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLoaded;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLoaded, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x1400000A RID: 10
		// (add) Token: 0x0600050E RID: 1294 RVA: 0x00020414 File Offset: 0x0001E614
		// (remove) Token: 0x0600050F RID: 1295 RVA: 0x0002044C File Offset: 0x0001E64C
		public event EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad
		{
			[CompilerGenerated]
			add
			{
				EventHandler<AdFailedToLoadEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdFailedToLoadEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdFailedToLoadEventArgs> value2 = (EventHandler<AdFailedToLoadEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdFailedToLoadEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<AdFailedToLoadEventArgs> eventHandler = this.OnAdFailedToLoad;
				EventHandler<AdFailedToLoadEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<AdFailedToLoadEventArgs> value2 = (EventHandler<AdFailedToLoadEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<AdFailedToLoadEventArgs>>(ref this.OnAdFailedToLoad, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x1400000B RID: 11
		// (add) Token: 0x06000510 RID: 1296 RVA: 0x00020484 File Offset: 0x0001E684
		// (remove) Token: 0x06000511 RID: 1297 RVA: 0x000204BC File Offset: 0x0001E6BC
		public event EventHandler<EventArgs> OnAdOpening
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdOpening;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdOpening, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdOpening;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdOpening, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x1400000C RID: 12
		// (add) Token: 0x06000512 RID: 1298 RVA: 0x000204F4 File Offset: 0x0001E6F4
		// (remove) Token: 0x06000513 RID: 1299 RVA: 0x0002052C File Offset: 0x0001E72C
		public event EventHandler<EventArgs> OnAdClosed
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdClosed;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdClosed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdClosed;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdClosed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x1400000D RID: 13
		// (add) Token: 0x06000514 RID: 1300 RVA: 0x00020564 File Offset: 0x0001E764
		// (remove) Token: 0x06000515 RID: 1301 RVA: 0x0002059C File Offset: 0x0001E79C
		public event EventHandler<EventArgs> OnAdLeavingApplication
		{
			[CompilerGenerated]
			add
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLeavingApplication;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLeavingApplication, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<EventArgs> eventHandler = this.OnAdLeavingApplication;
				EventHandler<EventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<EventArgs> value2 = (EventHandler<EventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<EventArgs>>(ref this.OnAdLeavingApplication, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x06000516 RID: 1302 RVA: 0x000205D1 File Offset: 0x0001E7D1
		public void CreateInterstitialAd(string adUnitId)
		{
			this.interstitial.Call("create", new object[]
			{
				adUnitId
			});
		}

		// Token: 0x06000517 RID: 1303 RVA: 0x000205ED File Offset: 0x0001E7ED
		public void LoadAd(AdRequest request)
		{
			this.interstitial.Call("loadAd", new object[]
			{
				Utils.GetAdRequestJavaObject(request)
			});
		}

		// Token: 0x06000518 RID: 1304 RVA: 0x0002060E File Offset: 0x0001E80E
		public bool IsLoaded()
		{
			return this.interstitial.Call<bool>("isLoaded", Array.Empty<object>());
		}

		// Token: 0x06000519 RID: 1305 RVA: 0x00020625 File Offset: 0x0001E825
		public void ShowInterstitial()
		{
			this.interstitial.Call("show", Array.Empty<object>());
		}

		// Token: 0x0600051A RID: 1306 RVA: 0x0002063C File Offset: 0x0001E83C
		public void DestroyInterstitial()
		{
			this.interstitial.Call("destroy", Array.Empty<object>());
		}

		// Token: 0x0600051B RID: 1307 RVA: 0x00020653 File Offset: 0x0001E853
		public string MediationAdapterClassName()
		{
			return this.interstitial.Call<string>("getMediationAdapterClassName", Array.Empty<object>());
		}

		// Token: 0x0600051C RID: 1308 RVA: 0x0002066A File Offset: 0x0001E86A
		public void onAdLoaded()
		{
			if (this.OnAdLoaded != null)
			{
				this.OnAdLoaded(this, EventArgs.Empty);
			}
		}

		// Token: 0x0600051D RID: 1309 RVA: 0x00020688 File Offset: 0x0001E888
		public void onAdFailedToLoad(string errorReason)
		{
			if (this.OnAdFailedToLoad != null)
			{
				AdFailedToLoadEventArgs e = new AdFailedToLoadEventArgs
				{
					Message = errorReason
				};
				this.OnAdFailedToLoad(this, e);
			}
		}

		// Token: 0x0600051E RID: 1310 RVA: 0x000206B7 File Offset: 0x0001E8B7
		public void onAdOpened()
		{
			if (this.OnAdOpening != null)
			{
				this.OnAdOpening(this, EventArgs.Empty);
			}
		}

		// Token: 0x0600051F RID: 1311 RVA: 0x000206D2 File Offset: 0x0001E8D2
		public void onAdClosed()
		{
			if (this.OnAdClosed != null)
			{
				this.OnAdClosed(this, EventArgs.Empty);
			}
		}

		// Token: 0x06000520 RID: 1312 RVA: 0x000206ED File Offset: 0x0001E8ED
		public void onAdLeftApplication()
		{
			if (this.OnAdLeavingApplication != null)
			{
				this.OnAdLeavingApplication(this, EventArgs.Empty);
			}
		}

		// Token: 0x040001A4 RID: 420
		private AndroidJavaObject interstitial;

		// Token: 0x040001A5 RID: 421
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdLoaded;

		// Token: 0x040001A6 RID: 422
		[CompilerGenerated]
		private EventHandler<AdFailedToLoadEventArgs> OnAdFailedToLoad;

		// Token: 0x040001A7 RID: 423
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdOpening;

		// Token: 0x040001A8 RID: 424
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdClosed;

		// Token: 0x040001A9 RID: 425
		[CompilerGenerated]
		private EventHandler<EventArgs> OnAdLeavingApplication;
	}
}
