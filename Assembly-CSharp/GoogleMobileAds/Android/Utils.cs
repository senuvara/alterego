﻿using System;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using GoogleMobileAds.Api.Mediation;
using UnityEngine;

namespace GoogleMobileAds.Android
{
	// Token: 0x0200007A RID: 122
	internal class Utils
	{
		// Token: 0x06000561 RID: 1377 RVA: 0x000212D0 File Offset: 0x0001F4D0
		public static AndroidJavaObject GetAdSizeJavaObject(AdSize adSize)
		{
			if (adSize.IsSmartBanner)
			{
				return new AndroidJavaClass("com.google.android.gms.ads.AdSize").GetStatic<AndroidJavaObject>("SMART_BANNER");
			}
			return new AndroidJavaObject("com.google.android.gms.ads.AdSize", new object[]
			{
				adSize.Width,
				adSize.Height
			});
		}

		// Token: 0x06000562 RID: 1378 RVA: 0x00021328 File Offset: 0x0001F528
		public static AndroidJavaObject GetAdRequestJavaObject(AdRequest request)
		{
			AndroidJavaObject androidJavaObject = new AndroidJavaObject("com.google.android.gms.ads.AdRequest$Builder", Array.Empty<object>());
			foreach (string text in request.Keywords)
			{
				androidJavaObject.Call<AndroidJavaObject>("addKeyword", new object[]
				{
					text
				});
			}
			foreach (string text2 in request.TestDevices)
			{
				if (text2 == "SIMULATOR")
				{
					string @static = new AndroidJavaClass("com.google.android.gms.ads.AdRequest").GetStatic<string>("DEVICE_ID_EMULATOR");
					androidJavaObject.Call<AndroidJavaObject>("addTestDevice", new object[]
					{
						@static
					});
				}
				else
				{
					androidJavaObject.Call<AndroidJavaObject>("addTestDevice", new object[]
					{
						text2
					});
				}
			}
			if (request.Birthday != null)
			{
				DateTime valueOrDefault = request.Birthday.GetValueOrDefault();
				AndroidJavaObject androidJavaObject2 = new AndroidJavaObject("java.util.Date", new object[]
				{
					valueOrDefault.Year,
					valueOrDefault.Month,
					valueOrDefault.Day
				});
				androidJavaObject.Call<AndroidJavaObject>("setBirthday", new object[]
				{
					androidJavaObject2
				});
			}
			if (request.Gender != null)
			{
				int? num = null;
				switch (request.Gender.GetValueOrDefault())
				{
				case Gender.Unknown:
					num = new int?(new AndroidJavaClass("com.google.android.gms.ads.AdRequest").GetStatic<int>("GENDER_UNKNOWN"));
					break;
				case Gender.Male:
					num = new int?(new AndroidJavaClass("com.google.android.gms.ads.AdRequest").GetStatic<int>("GENDER_MALE"));
					break;
				case Gender.Female:
					num = new int?(new AndroidJavaClass("com.google.android.gms.ads.AdRequest").GetStatic<int>("GENDER_FEMALE"));
					break;
				}
				if (num != null)
				{
					androidJavaObject.Call<AndroidJavaObject>("setGender", new object[]
					{
						num
					});
				}
			}
			if (request.TagForChildDirectedTreatment != null)
			{
				androidJavaObject.Call<AndroidJavaObject>("tagForChildDirectedTreatment", new object[]
				{
					request.TagForChildDirectedTreatment.GetValueOrDefault()
				});
			}
			androidJavaObject.Call<AndroidJavaObject>("setRequestAgent", new object[]
			{
				"unity-3.16.0"
			});
			AndroidJavaObject androidJavaObject3 = new AndroidJavaObject("android.os.Bundle", Array.Empty<object>());
			foreach (KeyValuePair<string, string> keyValuePair in request.Extras)
			{
				androidJavaObject3.Call("putString", new object[]
				{
					keyValuePair.Key,
					keyValuePair.Value
				});
			}
			androidJavaObject3.Call("putString", new object[]
			{
				"is_unity",
				"1"
			});
			AndroidJavaObject androidJavaObject4 = new AndroidJavaObject("com.google.android.gms.ads.mediation.admob.AdMobExtras", new object[]
			{
				androidJavaObject3
			});
			androidJavaObject.Call<AndroidJavaObject>("addNetworkExtras", new object[]
			{
				androidJavaObject4
			});
			foreach (MediationExtras mediationExtras in request.MediationExtras)
			{
				AndroidJavaObject androidJavaObject5 = new AndroidJavaObject(mediationExtras.AndroidMediationExtraBuilderClassName, Array.Empty<object>());
				AndroidJavaObject androidJavaObject6 = new AndroidJavaObject("java.util.HashMap", Array.Empty<object>());
				foreach (KeyValuePair<string, string> keyValuePair2 in mediationExtras.Extras)
				{
					androidJavaObject6.Call<AndroidJavaObject>("put", new object[]
					{
						keyValuePair2.Key,
						keyValuePair2.Value
					});
				}
				AndroidJavaObject androidJavaObject7 = androidJavaObject5.Call<AndroidJavaObject>("buildExtras", new object[]
				{
					androidJavaObject6
				});
				if (androidJavaObject7 != null)
				{
					androidJavaObject.Call<AndroidJavaObject>("addNetworkExtrasBundle", new object[]
					{
						androidJavaObject5.Call<AndroidJavaClass>("getAdapterClass", Array.Empty<object>()),
						androidJavaObject7
					});
				}
			}
			return androidJavaObject.Call<AndroidJavaObject>("build", Array.Empty<object>());
		}

		// Token: 0x06000563 RID: 1379 RVA: 0x000043CE File Offset: 0x000025CE
		public Utils()
		{
		}

		// Token: 0x040001BB RID: 443
		public const string AdListenerClassName = "com.google.android.gms.ads.AdListener";

		// Token: 0x040001BC RID: 444
		public const string AdRequestClassName = "com.google.android.gms.ads.AdRequest";

		// Token: 0x040001BD RID: 445
		public const string AdRequestBuilderClassName = "com.google.android.gms.ads.AdRequest$Builder";

		// Token: 0x040001BE RID: 446
		public const string AdSizeClassName = "com.google.android.gms.ads.AdSize";

		// Token: 0x040001BF RID: 447
		public const string AdMobExtrasClassName = "com.google.android.gms.ads.mediation.admob.AdMobExtras";

		// Token: 0x040001C0 RID: 448
		public const string PlayStorePurchaseListenerClassName = "com.google.android.gms.ads.purchase.PlayStorePurchaseListener";

		// Token: 0x040001C1 RID: 449
		public const string MobileAdsClassName = "com.google.android.gms.ads.MobileAds";

		// Token: 0x040001C2 RID: 450
		public const string BannerViewClassName = "com.google.unity.ads.Banner";

		// Token: 0x040001C3 RID: 451
		public const string InterstitialClassName = "com.google.unity.ads.Interstitial";

		// Token: 0x040001C4 RID: 452
		public const string RewardBasedVideoClassName = "com.google.unity.ads.RewardBasedVideo";

		// Token: 0x040001C5 RID: 453
		public const string UnityRewardedAdClassName = "com.google.unity.ads.UnityRewardedAd";

		// Token: 0x040001C6 RID: 454
		public const string NativeAdLoaderClassName = "com.google.unity.ads.NativeAdLoader";

		// Token: 0x040001C7 RID: 455
		public const string UnityAdListenerClassName = "com.google.unity.ads.UnityAdListener";

		// Token: 0x040001C8 RID: 456
		public const string UnityRewardBasedVideoAdListenerClassName = "com.google.unity.ads.UnityRewardBasedVideoAdListener";

		// Token: 0x040001C9 RID: 457
		public const string UnityRewardedAdCallbackClassName = "com.google.unity.ads.UnityRewardedAdCallback";

		// Token: 0x040001CA RID: 458
		public const string UnityAdapterStatusEnumName = "com.google.android.gms.ads.initialization.AdapterStatus$State";

		// Token: 0x040001CB RID: 459
		public const string UnityAdLoaderListenerClassName = "com.google.unity.ads.UnityAdLoaderListener";

		// Token: 0x040001CC RID: 460
		public const string PluginUtilsClassName = "com.google.unity.ads.PluginUtils";

		// Token: 0x040001CD RID: 461
		public const string UnityActivityClassName = "com.unity3d.player.UnityPlayer";

		// Token: 0x040001CE RID: 462
		public const string BundleClassName = "android.os.Bundle";

		// Token: 0x040001CF RID: 463
		public const string DateClassName = "java.util.Date";
	}
}
