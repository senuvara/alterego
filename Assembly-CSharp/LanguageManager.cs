﻿using System;
using System.Collections.Generic;
using App;
using UnityEngine;

// Token: 0x02000030 RID: 48
public static class LanguageManager
{
	// Token: 0x060001F8 RID: 504 RVA: 0x00011840 File Offset: 0x0000FA40
	static LanguageManager()
	{
		Data.Init();
		LanguageManager.SetLanguage();
		LanguageManager.FONTDATA_TABLE.Add("Default", Resources.GetBuiltinResource<Font>("Arial.ttf"));
		foreach (Font font in Resources.LoadAll<Font>("Font"))
		{
			LanguageManager.FONTDATA_TABLE.Add(font.name, font);
		}
	}

	// Token: 0x060001F9 RID: 505 RVA: 0x000118BC File Offset: 0x0000FABC
	public static void SetLanguage()
	{
		string language = BuildInfo.LANG_DEFAULT;
		SystemLanguage systemLanguage = Application.systemLanguage;
		if (systemLanguage <= SystemLanguage.English)
		{
			if (systemLanguage != SystemLanguage.Chinese)
			{
				if (systemLanguage != SystemLanguage.English)
				{
					goto IL_4D;
				}
				language = "英語";
				goto IL_4D;
			}
		}
		else
		{
			if (systemLanguage == SystemLanguage.Japanese)
			{
				language = "日本語";
				goto IL_4D;
			}
			if (systemLanguage == SystemLanguage.Spanish)
			{
				language = "スペイン語";
				goto IL_4D;
			}
			if (systemLanguage - SystemLanguage.ChineseSimplified > 1)
			{
				goto IL_4D;
			}
		}
		language = "中国語";
		IL_4D:
		Settings.LANGUAGE = language;
		if (LanguageManager.GetIndex() < 0)
		{
			Settings.LANGUAGE = BuildInfo.LANG_DEFAULT;
		}
	}

	// Token: 0x060001FA RID: 506 RVA: 0x00011930 File Offset: 0x0000FB30
	public static string GetCode(string type, string os)
	{
		if (type == "日本語")
		{
			return "ja";
		}
		if (!(type == "中国語"))
		{
			if (type == "英語")
			{
				return "en";
			}
			if (!(type == "スペイン語"))
			{
				return "";
			}
			return "es";
		}
		else
		{
			if (!(os == "iOS"))
			{
				return "zh";
			}
			return "zh-Hans";
		}
	}

	// Token: 0x060001FB RID: 507 RVA: 0x000119A3 File Offset: 0x0000FBA3
	public static int GetIndex()
	{
		return Array.IndexOf<string>(BuildInfo.LANG_TYPE, Settings.LANGUAGE);
	}

	// Token: 0x060001FC RID: 508 RVA: 0x000119B4 File Offset: 0x0000FBB4
	public static bool IsDefault()
	{
		return Settings.LANGUAGE == BuildInfo.LANG_DEFAULT;
	}

	// Token: 0x060001FD RID: 509 RVA: 0x000119C5 File Offset: 0x0000FBC5
	public static bool IsJapanese()
	{
		return Settings.LANGUAGE == "日本語";
	}

	// Token: 0x060001FE RID: 510 RVA: 0x000119D6 File Offset: 0x0000FBD6
	public static bool IsChinese()
	{
		return Settings.LANGUAGE == "中国語";
	}

	// Token: 0x060001FF RID: 511 RVA: 0x000119E7 File Offset: 0x0000FBE7
	public static bool AllowVertical()
	{
		return LanguageManager.IsJapanese() || LanguageManager.IsChinese();
	}

	// Token: 0x06000200 RID: 512 RVA: 0x000119F7 File Offset: 0x0000FBF7
	public static bool Contains(string id)
	{
		return LanguageManager.STRINGS_TABLE.ContainsKey(id);
	}

	// Token: 0x06000201 RID: 513 RVA: 0x00011A04 File Offset: 0x0000FC04
	public static string Get(string id)
	{
		return LanguageManager.Get(id, true);
	}

	// Token: 0x06000202 RID: 514 RVA: 0x00011A10 File Offset: 0x0000FC10
	public static string Get(string id, bool required)
	{
		if (id != null && LanguageManager.STRINGS_TABLE.ContainsKey(id))
		{
			int index = LanguageManager.GetIndex();
			return LanguageManager.STRINGS_TABLE[id][index].Replace("<br>", "\n");
		}
		if (required)
		{
			global::Debug.LogError("LanguageManager string id=" + id);
		}
		return "";
	}

	// Token: 0x06000203 RID: 515 RVA: 0x00011A68 File Offset: 0x0000FC68
	public static Font GetFont(string id)
	{
		if (!LanguageManager.FONT_TABLE.ContainsKey(id))
		{
			return null;
		}
		int index = LanguageManager.GetIndex();
		string key = LanguageManager.FONT_TABLE[id][index];
		if (LanguageManager.FONTDATA_TABLE.ContainsKey(key))
		{
			return LanguageManager.FONTDATA_TABLE[key];
		}
		return null;
	}

	// Token: 0x040000A9 RID: 169
	public static Font[] FontTable;

	// Token: 0x040000AA RID: 170
	public static readonly IDictionary<string, Font> FONTDATA_TABLE = new Dictionary<string, Font>();

	// Token: 0x040000AB RID: 171
	public static IDictionary<string, string[]> STRINGS_TABLE = new Dictionary<string, string[]>();

	// Token: 0x040000AC RID: 172
	public static IDictionary<string, string[]> FONT_TABLE = new Dictionary<string, string[]>();
}
