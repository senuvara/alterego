﻿using System;

// Token: 0x02000039 RID: 57
[Serializable]
public class AdfurikunMovieInterstitialAdConfig
{
	// Token: 0x06000252 RID: 594 RVA: 0x000043CE File Offset: 0x000025CE
	public AdfurikunMovieInterstitialAdConfig()
	{
	}

	// Token: 0x040000C7 RID: 199
	public string iPhoneAppID;

	// Token: 0x040000C8 RID: 200
	public string androidAppID;
}
