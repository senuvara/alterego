﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Token: 0x020000B1 RID: 177
[CompilerGenerated]
internal sealed class <PrivateImplementationDetails>
{
	// Token: 0x0600078D RID: 1933 RVA: 0x0002661C File Offset: 0x0002481C
	internal static uint ComputeStringHash(string s)
	{
		uint num;
		if (s != null)
		{
			num = 2166136261U;
			for (int i = 0; i < s.Length; i++)
			{
				num = ((uint)s[i] ^ num) * 16777619U;
			}
		}
		return num;
	}

	// Token: 0x04000298 RID: 664 RVA: 0x0006A59C File Offset: 0x0006879C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=12 0FA69D09E275A14E5756435450655AB6B5B229F2;

	// Token: 0x02000164 RID: 356
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 12)]
	private struct __StaticArrayInitTypeSize=12
	{
	}
}
