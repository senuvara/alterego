﻿using System;
using UnityEngine;

// Token: 0x0200000C RID: 12
public class EffectController : MonoBehaviour
{
	// Token: 0x0600004D RID: 77 RVA: 0x000072B8 File Offset: 0x000054B8
	public void OnEndAnimation()
	{
		Object.Destroy(base.transform.parent.gameObject);
	}

	// Token: 0x0600004E RID: 78 RVA: 0x000072CF File Offset: 0x000054CF
	public void PlaySound(string soundName)
	{
		if (this.SoundOn)
		{
			AudioManager.PlaySound(new string[]
			{
				"SE",
				base.gameObject.name,
				soundName
			});
		}
	}

	// Token: 0x0600004F RID: 79 RVA: 0x000025AD File Offset: 0x000007AD
	public EffectController()
	{
	}

	// Token: 0x04000017 RID: 23
	public bool SoundOn;
}
