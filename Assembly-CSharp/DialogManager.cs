﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using App;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x0200002D RID: 45
public class DialogManager : MonoBehaviour
{
	// Token: 0x060001A1 RID: 417 RVA: 0x0000FC20 File Offset: 0x0000DE20
	private void Awake()
	{
		DialogManager.Instance = this;
		this.FilterGroup = base.transform.Find("FilterGroup").gameObject;
		this.FilterGroup.SetActive(false);
		this.CanvasDialog = base.transform.Find("DialogCanvas").gameObject;
		this.CanvasDialog.SetActive(false);
		GameObject gameObject = Object.Instantiate<GameObject>(Resources.Load<GameObject>("FakeMovieAdScreen"), base.transform.Find("DialogCanvas"));
		Sprite sprite = Resources.Load<Sprite>("FakeAdImage");
		if (sprite != null)
		{
			gameObject.transform.Find("Rectangle").GetComponent<Image>().sprite = sprite;
		}
		foreach (Dialog dialog in base.transform.Find("DialogCanvas").GetComponentsInChildren<Dialog>(true))
		{
			dialog.gameObject.SetActive(false);
			dialog.gameObject.name = dialog.gameObject.name.Replace("(Clone)", "");
		}
	}

	// Token: 0x060001A2 RID: 418 RVA: 0x0000FD2E File Offset: 0x0000DF2E
	public void OnClick(GameObject clickObject)
	{
		global::Debug.Log("DialogManager:OnClick " + clickObject.name);
	}

	// Token: 0x060001A3 RID: 419 RVA: 0x0000FD45 File Offset: 0x0000DF45
	public static bool IsShowing()
	{
		return !(DialogManager.Instance == null) && DialogManager.GetShowingDialogCnt() > 0;
	}

	// Token: 0x060001A4 RID: 420 RVA: 0x0000FD60 File Offset: 0x0000DF60
	public static void ShowDialog(string dialogName, params object[] args)
	{
		if (DialogManager.Instance == null)
		{
			return;
		}
		global::Debug.Log("DialogManager args=" + ((args != null) ? args.Length : 0));
		if (DialogManager.GetShowingDialogCnt() == 0)
		{
			DialogManager.Instance.FilterGroup.SetActive(true);
			DialogManager.Instance.StartCoroutine(AppUtil.FadeIn(DialogManager.Instance.FilterGroup.GetComponentInChildren<CanvasGroup>(), 0.25f, null));
			DialogManager.Instance.CanvasDialog.SetActive(true);
			Time.timeScale = 0f;
		}
		Dialog component = DialogManager.Instance.transform.Find("DialogCanvas/" + dialogName).GetComponent<Dialog>();
		if (DialogManager.Instance.ClosingDialog.ContainsKey(component.name))
		{
			DialogManager.Instance.StopCoroutine(DialogManager.Instance.ClosingDialog[component.name]);
			DialogManager.Instance.ClosingDialog.Remove(component.name);
			component.gameObject.SetActive(false);
		}
		component.OnShown(args);
	}

	// Token: 0x060001A5 RID: 421 RVA: 0x0000FE70 File Offset: 0x0000E070
	public static void CloseAllDialog()
	{
		foreach (Dialog dialog in DialogManager.Instance.transform.Find("DialogCanvas").GetComponentsInChildren<Dialog>())
		{
			DialogManager.Instance.StartCoroutine(DialogManager.Instance.CloseDialogAsync(dialog.name));
		}
		if (DialogManager.GetShowingDialogCnt() > 1)
		{
			DialogManager.Instance.StartCoroutine(AppUtil.FadeOut(DialogManager.Instance.FilterGroup.GetComponentInChildren<CanvasGroup>(), 0.25f, null));
		}
	}

	// Token: 0x060001A6 RID: 422 RVA: 0x0000FEF4 File Offset: 0x0000E0F4
	public static void CloseDialog(string dialogName)
	{
		if (DialogManager.Instance == null)
		{
			return;
		}
		foreach (Dialog dialog in DialogManager.Instance.transform.Find("DialogCanvas").GetComponentsInChildren<Dialog>())
		{
			if (dialog.name != dialogName)
			{
				dialog.OnShown(Array.Empty<object>());
			}
		}
		DialogManager.Instance.StartCoroutine(DialogManager.Instance.CloseDialogAsync(dialogName));
	}

	// Token: 0x060001A7 RID: 423 RVA: 0x0000FF6A File Offset: 0x0000E16A
	private IEnumerator CloseDialogAsync(string dialogName)
	{
		Dialog dialog = base.transform.Find("DialogCanvas/" + dialogName).GetComponent<Dialog>();
		if (DialogManager.GetShowingDialogCnt() == 1)
		{
			base.StartCoroutine(AppUtil.FadeOut(this.FilterGroup.GetComponentInChildren<CanvasGroup>(), 0.25f, null));
		}
		IEnumerator enumerator = dialog.OnClosed();
		this.ClosingDialog.Add(dialog.name, enumerator);
		yield return enumerator;
		this.ClosingDialog.Remove(dialog.name);
		if (DialogManager.GetShowingDialogCnt() == 0)
		{
			this.FilterGroup.SetActive(false);
			this.CanvasDialog.SetActive(false);
			Time.timeScale = Settings.GAME_SPEED;
		}
		yield break;
	}

	// Token: 0x060001A8 RID: 424 RVA: 0x0000FF80 File Offset: 0x0000E180
	private static int GetShowingDialogCnt()
	{
		return DialogManager.Instance.transform.Find("DialogCanvas").GetComponentsInChildren<Dialog>().Length;
	}

	// Token: 0x060001A9 RID: 425 RVA: 0x0000FF9D File Offset: 0x0000E19D
	public DialogManager()
	{
	}

	// Token: 0x040000A1 RID: 161
	private static DialogManager Instance;

	// Token: 0x040000A2 RID: 162
	private GameObject FilterGroup;

	// Token: 0x040000A3 RID: 163
	private GameObject CanvasDialogBG;

	// Token: 0x040000A4 RID: 164
	private GameObject CanvasDialog;

	// Token: 0x040000A5 RID: 165
	private IDictionary<string, IEnumerator> ClosingDialog = new Dictionary<string, IEnumerator>();

	// Token: 0x0200011F RID: 287
	[CompilerGenerated]
	private sealed class <CloseDialogAsync>d__11 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000968 RID: 2408 RVA: 0x0002EB1C File Offset: 0x0002CD1C
		[DebuggerHidden]
		public <CloseDialogAsync>d__11(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000969 RID: 2409 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x0600096A RID: 2410 RVA: 0x0002EB2C File Offset: 0x0002CD2C
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			DialogManager dialogManager = this;
			if (num == 0)
			{
				this.<>1__state = -1;
				dialog = dialogManager.transform.Find("DialogCanvas/" + dialogName).GetComponent<Dialog>();
				if (DialogManager.GetShowingDialogCnt() == 1)
				{
					dialogManager.StartCoroutine(AppUtil.FadeOut(dialogManager.FilterGroup.GetComponentInChildren<CanvasGroup>(), 0.25f, null));
				}
				IEnumerator value = dialog.OnClosed();
				dialogManager.ClosingDialog.Add(dialog.name, value);
				this.<>2__current = value;
				this.<>1__state = 1;
				return true;
			}
			if (num != 1)
			{
				return false;
			}
			this.<>1__state = -1;
			dialogManager.ClosingDialog.Remove(dialog.name);
			if (DialogManager.GetShowingDialogCnt() == 0)
			{
				dialogManager.FilterGroup.SetActive(false);
				dialogManager.CanvasDialog.SetActive(false);
				Time.timeScale = Settings.GAME_SPEED;
			}
			return false;
		}

		// Token: 0x17000140 RID: 320
		// (get) Token: 0x0600096B RID: 2411 RVA: 0x0002EC20 File Offset: 0x0002CE20
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0600096C RID: 2412 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000141 RID: 321
		// (get) Token: 0x0600096D RID: 2413 RVA: 0x0002EC20 File Offset: 0x0002CE20
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x04000456 RID: 1110
		private int <>1__state;

		// Token: 0x04000457 RID: 1111
		private object <>2__current;

		// Token: 0x04000458 RID: 1112
		public DialogManager <>4__this;

		// Token: 0x04000459 RID: 1113
		public string dialogName;

		// Token: 0x0400045A RID: 1114
		private Dialog <dialog>5__2;
	}
}
