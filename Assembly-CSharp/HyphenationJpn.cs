﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

// Token: 0x02000010 RID: 16
[RequireComponent(typeof(Text))]
[ExecuteInEditMode]
public class HyphenationJpn : UIBehaviour
{
	// Token: 0x17000001 RID: 1
	// (get) Token: 0x06000071 RID: 113 RVA: 0x00007F31 File Offset: 0x00006131
	private RectTransform _RectTransform
	{
		get
		{
			if (this._rectTransform == null)
			{
				this._rectTransform = base.GetComponent<RectTransform>();
			}
			return this._rectTransform;
		}
	}

	// Token: 0x17000002 RID: 2
	// (get) Token: 0x06000072 RID: 114 RVA: 0x00007F53 File Offset: 0x00006153
	private Text _Text
	{
		get
		{
			if (this._text == null)
			{
				this._text = base.GetComponent<Text>();
			}
			return this._text;
		}
	}

	// Token: 0x06000073 RID: 115 RVA: 0x00007F75 File Offset: 0x00006175
	protected override void OnRectTransformDimensionsChange()
	{
		base.OnRectTransformDimensionsChange();
		this.UpdateText(this.text);
	}

	// Token: 0x06000074 RID: 116 RVA: 0x00007F89 File Offset: 0x00006189
	private void UpdateText(string str)
	{
		this._Text.text = this.GetFormatedText(this._Text, str);
	}

	// Token: 0x06000075 RID: 117 RVA: 0x00007FA3 File Offset: 0x000061A3
	public void SetText(string str)
	{
		this.text = str;
		this.UpdateText(this.text);
	}

	// Token: 0x06000076 RID: 118 RVA: 0x00007FB8 File Offset: 0x000061B8
	private float GetSpaceWidth(Text textComp)
	{
		float textWidth = this.GetTextWidth(textComp, "m m");
		float textWidth2 = this.GetTextWidth(textComp, "mm");
		return textWidth - textWidth2;
	}

	// Token: 0x06000077 RID: 119 RVA: 0x00007FE0 File Offset: 0x000061E0
	private float GetTextWidth(Text textComp, string message)
	{
		if (this._text.supportRichText)
		{
			message = Regex.Replace(message, HyphenationJpn.RITCH_TEXT_REPLACE, string.Empty);
		}
		textComp.text = message;
		return textComp.preferredWidth;
	}

	// Token: 0x06000078 RID: 120 RVA: 0x00008010 File Offset: 0x00006210
	private string GetFormatedText(Text textComp, string msg)
	{
		if (string.IsNullOrEmpty(msg))
		{
			return string.Empty;
		}
		float width = this._RectTransform.rect.width;
		float spaceWidth = this.GetSpaceWidth(textComp);
		textComp.horizontalOverflow = HorizontalWrapMode.Overflow;
		StringBuilder stringBuilder = new StringBuilder();
		float num = 0f;
		foreach (string text in this.GetWordList(msg))
		{
			num += this.GetTextWidth(textComp, text);
			if (text == Environment.NewLine)
			{
				num = 0f;
			}
			else
			{
				if (text == " ")
				{
					num += spaceWidth;
				}
				if (num > width)
				{
					stringBuilder.Append(Environment.NewLine);
					num = this.GetTextWidth(textComp, text);
				}
			}
			stringBuilder.Append(text);
		}
		return stringBuilder.ToString();
	}

	// Token: 0x06000079 RID: 121 RVA: 0x000080FC File Offset: 0x000062FC
	private List<string> GetWordList(string tmpText)
	{
		List<string> list = new List<string>();
		StringBuilder stringBuilder = new StringBuilder();
		char c = '\0';
		for (int i = 0; i < tmpText.Length; i++)
		{
			char c2 = tmpText[i];
			char c3 = (i < tmpText.Length - 1) ? tmpText[i + 1] : c;
			char c4 = (i > 0) ? tmpText[i - 1] : c;
			stringBuilder.Append(c2);
			if ((HyphenationJpn.IsLatin(c2) && HyphenationJpn.IsLatin(c4) && HyphenationJpn.IsLatin(c2) && !HyphenationJpn.IsLatin(c4)) || (!HyphenationJpn.IsLatin(c2) && HyphenationJpn.CHECK_HYP_BACK(c4)) || (!HyphenationJpn.IsLatin(c3) && !HyphenationJpn.CHECK_HYP_FRONT(c3) && !HyphenationJpn.CHECK_HYP_BACK(c2)) || i == tmpText.Length - 1)
			{
				list.Add(stringBuilder.ToString());
				stringBuilder = new StringBuilder();
			}
		}
		return list;
	}

	// Token: 0x17000003 RID: 3
	// (get) Token: 0x0600007B RID: 123 RVA: 0x000081EC File Offset: 0x000063EC
	// (set) Token: 0x0600007A RID: 122 RVA: 0x000081DD File Offset: 0x000063DD
	public float textWidth
	{
		get
		{
			return this._RectTransform.rect.width;
		}
		set
		{
			this._RectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, value);
		}
	}

	// Token: 0x17000004 RID: 4
	// (get) Token: 0x0600007D RID: 125 RVA: 0x0000821A File Offset: 0x0000641A
	// (set) Token: 0x0600007C RID: 124 RVA: 0x0000820C File Offset: 0x0000640C
	public int fontSize
	{
		get
		{
			return this._Text.fontSize;
		}
		set
		{
			this._Text.fontSize = value;
		}
	}

	// Token: 0x0600007E RID: 126 RVA: 0x00008228 File Offset: 0x00006428
	private static bool CHECK_HYP_FRONT(char str)
	{
		return Array.Exists<char>(HyphenationJpn.HYP_FRONT, (char item) => item == str);
	}

	// Token: 0x0600007F RID: 127 RVA: 0x00008258 File Offset: 0x00006458
	private static bool CHECK_HYP_BACK(char str)
	{
		return Array.Exists<char>(HyphenationJpn.HYP_BACK, (char item) => item == str);
	}

	// Token: 0x06000080 RID: 128 RVA: 0x00008288 File Offset: 0x00006488
	private static bool IsLatin(char s)
	{
		return Array.Exists<char>(HyphenationJpn.HYP_LATIN, (char item) => item == s);
	}

	// Token: 0x06000081 RID: 129 RVA: 0x000082B8 File Offset: 0x000064B8
	public HyphenationJpn()
	{
	}

	// Token: 0x06000082 RID: 130 RVA: 0x000082C0 File Offset: 0x000064C0
	// Note: this type is marked as 'beforefieldinit'.
	static HyphenationJpn()
	{
	}

	// Token: 0x0400001D RID: 29
	[TextArea(3, 10)]
	[SerializeField]
	private string text;

	// Token: 0x0400001E RID: 30
	private RectTransform _rectTransform;

	// Token: 0x0400001F RID: 31
	private Text _text;

	// Token: 0x04000020 RID: 32
	private static readonly string RITCH_TEXT_REPLACE = "(\\<color=.*\\>|</color>|\\<size=.n\\>|</size>|<b>|</b>|<i>|</i>)";

	// Token: 0x04000021 RID: 33
	private static readonly char[] HYP_FRONT = ",)]｝、。）〕〉》」』】〙〗〟’”｠»ァィゥェォッャュョヮヵヶっぁぃぅぇぉっゃゅょゎ‐゠–〜ー?!！？‼⁇⁈⁉・:;。.".ToCharArray();

	// Token: 0x04000022 RID: 34
	private static readonly char[] HYP_BACK = "(（[｛〔〈《「『【〘〖〝‘“｟«".ToCharArray();

	// Token: 0x04000023 RID: 35
	private static readonly char[] HYP_LATIN = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789<>=/().,".ToCharArray();

	// Token: 0x020000C6 RID: 198
	[CompilerGenerated]
	private sealed class <>c__DisplayClass24_0
	{
		// Token: 0x060007E0 RID: 2016 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass24_0()
		{
		}

		// Token: 0x060007E1 RID: 2017 RVA: 0x000274EC File Offset: 0x000256EC
		internal bool <CHECK_HYP_FRONT>b__0(char item)
		{
			return item == this.str;
		}

		// Token: 0x040002DC RID: 732
		public char str;
	}

	// Token: 0x020000C7 RID: 199
	[CompilerGenerated]
	private sealed class <>c__DisplayClass25_0
	{
		// Token: 0x060007E2 RID: 2018 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass25_0()
		{
		}

		// Token: 0x060007E3 RID: 2019 RVA: 0x000274F7 File Offset: 0x000256F7
		internal bool <CHECK_HYP_BACK>b__0(char item)
		{
			return item == this.str;
		}

		// Token: 0x040002DD RID: 733
		public char str;
	}

	// Token: 0x020000C8 RID: 200
	[CompilerGenerated]
	private sealed class <>c__DisplayClass26_0
	{
		// Token: 0x060007E4 RID: 2020 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass26_0()
		{
		}

		// Token: 0x060007E5 RID: 2021 RVA: 0x00027502 File Offset: 0x00025702
		internal bool <IsLatin>b__0(char item)
		{
			return item == this.s;
		}

		// Token: 0x040002DE RID: 734
		public char s;
	}
}
