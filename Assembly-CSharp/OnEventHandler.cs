﻿using System;
using UnityEngine;

// Token: 0x02000014 RID: 20
public class OnEventHandler : MonoBehaviour
{
	// Token: 0x06000087 RID: 135 RVA: 0x00008355 File Offset: 0x00006555
	public void OnEvent()
	{
		base.gameObject.SendMessageUpwards("OnAnimationEvent", base.gameObject);
	}

	// Token: 0x06000088 RID: 136 RVA: 0x0000836D File Offset: 0x0000656D
	public void OnChanged(bool on)
	{
		base.gameObject.SendMessageUpwards("OnValueChanged", new string[]
		{
			base.gameObject.name,
			on.ToString()
		}, SendMessageOptions.DontRequireReceiver);
	}

	// Token: 0x06000089 RID: 137 RVA: 0x000025AD File Offset: 0x000007AD
	public OnEventHandler()
	{
	}
}
