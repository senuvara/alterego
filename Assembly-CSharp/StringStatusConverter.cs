﻿using System;
using System.Reflection;
using System.Text;
using App;
using UnityEngine;
using UnityEngine.SceneManagement;

// Token: 0x02000036 RID: 54
public class StringStatusConverter
{
	// Token: 0x0600023E RID: 574 RVA: 0x00012690 File Offset: 0x00010890
	public static void SetPlayerStatus(string userData)
	{
		if (userData == "None" || !userData.Contains("="))
		{
			return;
		}
		foreach (string text in userData.Split(new char[]
		{
			','
		}))
		{
			if (text[0] == ' ')
			{
				text = text.Substring(1, text.Length - 1);
			}
			if (text[text.Length - 1] == ' ')
			{
				text = text.Substring(0, text.Length - 1);
			}
			StringStatusConverter.SetEachProperty(text);
		}
	}

	// Token: 0x0600023F RID: 575 RVA: 0x0001271F File Offset: 0x0001091F
	public static void SetEachProperty(string key, string value)
	{
		StringStatusConverter.SetEachProperty(key + "=" + value);
	}

	// Token: 0x06000240 RID: 576 RVA: 0x00012734 File Offset: 0x00010934
	private static void SetEachProperty(string propertyPair)
	{
		string[] array = propertyPair.Split(new char[]
		{
			'='
		});
		string text = array[0];
		string text2 = array[1];
		PropertyInfo propertyInfo = null;
		Type[] playerDataList = AppInfo.PlayerDataList;
		if (playerDataList == null)
		{
			return;
		}
		foreach (Type type in playerDataList)
		{
			if (AppUtil.HasAttribute(type, typeof(AppUtil.DataRangeAttribute)))
			{
				if (text.Contains(type.Name))
				{
					int num = int.Parse(text.Replace(type.Name, ""));
					string text3 = num.ToString();
					MethodInfo method = type.GetMethod("GetTitle", new Type[]
					{
						typeof(int)
					});
					if (method != null)
					{
						text3 = method.Invoke(null, new object[]
						{
							num
						}).ToString();
					}
					method = type.GetMethod("Set", new Type[]
					{
						typeof(string),
						typeof(int)
					});
					if (method != null)
					{
						method.Invoke(null, new object[]
						{
							text3,
							int.Parse(text2)
						});
					}
					method = type.GetMethod("Set", new Type[]
					{
						typeof(string),
						typeof(string)
					});
					if (method != null)
					{
						method.Invoke(null, new object[]
						{
							text3,
							text2
						});
					}
					method = type.GetMethod("Set", new Type[]
					{
						typeof(string),
						typeof(bool)
					});
					if (method != null)
					{
						method.Invoke(null, new object[]
						{
							text3,
							bool.Parse(text2)
						});
					}
					method = type.GetMethod("Set", new Type[]
					{
						typeof(string),
						typeof(TimeSpan)
					});
					if (method != null)
					{
						method.Invoke(null, new object[]
						{
							text3,
							TimeSpan.Parse(text2)
						});
					}
					return;
				}
			}
			else
			{
				foreach (PropertyInfo propertyInfo2 in type.GetProperties())
				{
					if (text == AppUtil.GetTitle(propertyInfo2))
					{
						propertyInfo = propertyInfo2;
						break;
					}
				}
				if (propertyInfo == null)
				{
					propertyInfo = type.GetProperty(text);
				}
				if (propertyInfo != null)
				{
					break;
				}
			}
		}
		if (propertyInfo == null)
		{
			return;
		}
		if (propertyInfo.PropertyType == typeof(float))
		{
			propertyInfo.SetValue(null, float.Parse(text2), null);
			return;
		}
		if (propertyInfo.PropertyType == typeof(int))
		{
			propertyInfo.SetValue(null, int.Parse(text2), null);
			return;
		}
		if (propertyInfo.PropertyType == typeof(bool))
		{
			propertyInfo.SetValue(null, bool.Parse(text2), null);
			return;
		}
		if (propertyInfo.PropertyType == typeof(string))
		{
			propertyInfo.SetValue(null, text2, null);
			return;
		}
		MethodInfo method2 = propertyInfo.PropertyType.GetMethod("CreateInstance");
		if (method2 != null)
		{
			propertyInfo.SetValue(null, method2.Invoke(null, new object[]
			{
				text2
			}), null);
		}
	}

	// Token: 0x06000241 RID: 577 RVA: 0x00012AB0 File Offset: 0x00010CB0
	public static string GetStatusString()
	{
		string text = "\n";
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append("[シーン]" + SceneManager.GetActiveScene().name);
		stringBuilder.Append(text);
		Type[] playerDataList = AppInfo.PlayerDataList;
		if (playerDataList == null)
		{
			return stringBuilder.ToString();
		}
		foreach (Type type in playerDataList)
		{
			stringBuilder.Append(string.Concat(new string[]
			{
				text,
				"[",
				AppUtil.GetTitle(type),
				"]",
				text
			}));
			if (AppUtil.HasAttribute(type, typeof(AppUtil.DataRangeAttribute)))
			{
				int num = (int)type.GetProperty("Min").GetValue(null, null);
				int num2 = (int)type.GetProperty("Max").GetValue(null, null);
				MethodInfo method = type.GetMethod("GetTitle");
				MethodInfo method2 = type.GetMethod("GetLabel");
				MethodInfo method3 = type.GetMethod("Get", new Type[]
				{
					typeof(string)
				});
				for (int j = num; j <= num2; j++)
				{
					string text2 = method.Invoke(null, new object[]
					{
						j
					}).ToString();
					string value = text2;
					if (method2 != null)
					{
						value = method2.Invoke(null, new object[]
						{
							j
						}).ToString();
					}
					string value2 = method3.Invoke(null, new object[]
					{
						text2
					}).ToString();
					stringBuilder.Append(value);
					stringBuilder.Append("=");
					stringBuilder.Append(value2);
					stringBuilder.Append(text);
				}
			}
			else
			{
				foreach (PropertyInfo propertyInfo in type.GetProperties())
				{
					stringBuilder.Append(AppUtil.GetTitle(propertyInfo));
					stringBuilder.Append("=");
					stringBuilder.Append(propertyInfo.GetValue(null, null).ToString());
					stringBuilder.Append(text);
				}
			}
		}
		return stringBuilder.ToString();
	}

	// Token: 0x06000242 RID: 578 RVA: 0x00012CE4 File Offset: 0x00010EE4
	public static string[] GetStatusStringArray(Type[] typeList = null, bool showHideItem = true, bool showReadOnlyItem = true)
	{
		string value = "\t";
		StringBuilder stringBuilder = new StringBuilder();
		StringBuilder stringBuilder2 = new StringBuilder();
		StringBuilder stringBuilder3 = new StringBuilder();
		if (typeList == null)
		{
			stringBuilder2.Append(SceneManager.GetActiveScene().name);
			stringBuilder2.Append(value);
			stringBuilder3.Append("Scene");
			stringBuilder3.Append(value);
			typeList = AppInfo.PlayerDataList;
		}
		if (typeList == null)
		{
			return new string[]
			{
				stringBuilder.ToString(),
				stringBuilder2.ToString(),
				stringBuilder3.ToString()
			};
		}
		foreach (Type type in typeList)
		{
			if (showHideItem || !AppUtil.HasAttribute(type, typeof(AppUtil.HideAttribute)))
			{
				if (AppUtil.HasAttribute(type, typeof(AppUtil.DataRangeAttribute)))
				{
					int num = (int)type.GetProperty("Min").GetValue(null, null);
					int num2 = (int)type.GetProperty("Max").GetValue(null, null);
					MethodInfo method = type.GetMethod("GetTitle");
					MethodInfo method2 = type.GetMethod("Get", new Type[]
					{
						typeof(string)
					});
					for (int j = num; j <= num2; j++)
					{
						string value2 = type.Name + j;
						string text = method.Invoke(null, new object[]
						{
							j
						}).ToString();
						string value3 = method2.Invoke(null, new object[]
						{
							text
						}).ToString();
						stringBuilder2.Append(value3);
						stringBuilder2.Append(value);
						stringBuilder3.Append(value2);
						stringBuilder3.Append(value);
					}
				}
				else
				{
					foreach (PropertyInfo propertyInfo in type.GetProperties())
					{
						if ((showReadOnlyItem || propertyInfo.CanWrite) && (showHideItem || !AppUtil.HasAttribute(propertyInfo, typeof(AppUtil.HideAttribute))))
						{
							string name = propertyInfo.Name;
							stringBuilder2.Append(propertyInfo.GetValue(null, null).ToString());
							stringBuilder2.Append(value);
							stringBuilder3.Append(name);
							stringBuilder3.Append(value);
						}
					}
				}
			}
		}
		return new string[]
		{
			stringBuilder.ToString(),
			stringBuilder2.ToString(),
			stringBuilder3.ToString()
		};
	}

	// Token: 0x06000243 RID: 579 RVA: 0x00012F40 File Offset: 0x00011140
	public static string GetHierarchyPath(Transform main)
	{
		string text = main.name;
		Transform parent = main.parent;
		while (parent != null)
		{
			text = parent.name + "/" + text;
			parent = parent.parent;
		}
		return text;
	}

	// Token: 0x06000244 RID: 580 RVA: 0x000043CE File Offset: 0x000025CE
	public StringStatusConverter()
	{
	}
}
