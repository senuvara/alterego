﻿using System;
using App;
using UnityEngine;

// Token: 0x02000008 RID: 8
public class CommentManager : MonoBehaviour
{
	// Token: 0x06000022 RID: 34 RVA: 0x000032EA File Offset: 0x000014EA
	private void FixedUpdate()
	{
		this.UpdateComment(false);
	}

	// Token: 0x06000023 RID: 35 RVA: 0x000032F3 File Offset: 0x000014F3
	public void UpdateCommentWithDestroy()
	{
		this.UpdateComment(true);
	}

	// Token: 0x06000024 RID: 36 RVA: 0x000032FC File Offset: 0x000014FC
	private void UpdateComment(bool destroy)
	{
		bool isFirst = true;
		Comment[] componentsInChildren = base.GetComponentsInChildren<Comment>();
		for (int i = 0; i < componentsInChildren.Length; i++)
		{
			if (componentsInChildren[i].UpdateStatus(isFirst, destroy))
			{
				isFirst = false;
			}
		}
	}

	// Token: 0x06000025 RID: 37 RVA: 0x00003330 File Offset: 0x00001530
	public void Generate(bool full, string mode, float rate)
	{
		bool flag = mode.Contains("HyperActive");
		bool flag2 = !full && rate == 2f && AdManager.IsReady("Reward") && TimeManager.IsOverTime(TimeManager.TYPE.NEXT_BONUS_BOOK) && !BonusTime.IsActive && GameObject.Find("BonusButterfly") == null && !PlayerStatus.EgoPerSecond.IsZero();
		string a = "Comment";
		if (flag2)
		{
			TimeManager.Reset(TimeManager.TYPE.NEXT_BONUS_BOOK, TimeSpan.FromMinutes(0.5));
			a = "Butterfly";
		}
		for (int i = ((rate == 0f) ? 0 : (full ? ((int)(12f / rate)) : 1)) - 1; i >= 0; i--)
		{
			Vector3 zero = Vector3.zero;
			zero.z = 12f + (float)i * -1.5f * 2f * rate;
			if (zero.z > -3f)
			{
				Vector2[][] array = flag ? this.POSITION2 : this.POSITION1;
				int num = this.PreColumnIndex;
				if (mode == "HyperActiveLast")
				{
					num = 2;
				}
				else
				{
					while (this.PreColumnIndex == num)
					{
						num = Random.Range(0, array.Length);
					}
				}
				this.PreColumnIndex = num;
				int num2 = Random.Range(0, array[num].Length);
				zero.x = array[num][num2].x;
				zero.y = array[num][num2].y;
				string text = Data.COMMENT_KEY_LIST[Random.Range(0, Data.COMMENT_KEY_LIST.Length)];
				string text2;
				if (a == "Comment")
				{
					text2 = "";
					if (text.Contains("[Es]"))
					{
						text2 = PlayerStatus.Route;
					}
				}
				else
				{
					text2 = "Butterfly";
				}
				GameObject gameObject = Object.Instantiate<GameObject>(Resources.Load<GameObject>("Comment" + text2), base.transform);
				if (a == "Comment")
				{
					gameObject.name = "FarComment" + text2;
				}
				else
				{
					gameObject.name = "ButterflyBonus";
				}
				gameObject.transform.position = zero;
				Comment component = gameObject.GetComponent<Comment>();
				component.Type = text2;
				component.CommentKey = text;
				component.IsHyperActive = flag;
			}
		}
		this.UpdateComment(full);
	}

	// Token: 0x06000026 RID: 38 RVA: 0x00003568 File Offset: 0x00001768
	public bool KeepHyperActive()
	{
		Comment[] componentsInChildren = base.GetComponentsInChildren<Comment>();
		for (int i = 0; i < componentsInChildren.Length; i++)
		{
			if (componentsInChildren[i].IsHyperActive)
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x06000027 RID: 39 RVA: 0x00003598 File Offset: 0x00001798
	public CommentManager()
	{
	}

	// Token: 0x0400000E RID: 14
	private int PreColumnIndex;

	// Token: 0x0400000F RID: 15
	private readonly Vector2[][] POSITION1 = new Vector2[][]
	{
		new Vector2[]
		{
			new Vector2(-2f, 2f),
			new Vector2(-2f, 0f),
			new Vector2(-2f, -2f)
		},
		new Vector2[]
		{
			new Vector2(0f, 1f),
			new Vector2(0f, -1f)
		},
		new Vector2[]
		{
			new Vector2(2f, 2f),
			new Vector2(2f, 0f),
			new Vector2(2f, -2f)
		}
	};

	// Token: 0x04000010 RID: 16
	private readonly Vector2[][] POSITION2 = new Vector2[][]
	{
		new Vector2[]
		{
			new Vector2(-3f, 3.5f),
			new Vector2(-3f, 0f),
			new Vector2(-3f, -2.5f)
		},
		new Vector2[]
		{
			new Vector2(-1.5f, 2f),
			new Vector2(-1.5f, 0f),
			new Vector2(-1.5f, -2f)
		},
		new Vector2[]
		{
			new Vector2(0f, 1f),
			new Vector2(0f, -1f)
		},
		new Vector2[]
		{
			new Vector2(1.5f, 2f),
			new Vector2(1.5f, 0f),
			new Vector2(1.5f, -2f)
		},
		new Vector2[]
		{
			new Vector2(3f, 3.5f),
			new Vector2(3f, 0f),
			new Vector2(3f, -2.5f)
		}
	};
}
