﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000005 RID: 5
public class ChoiceGroup : MonoBehaviour
{
	// Token: 0x0600000F RID: 15 RVA: 0x00002988 File Offset: 0x00000B88
	private void OnEnable()
	{
		foreach (GameObject gameObject in this.ButtonList)
		{
			gameObject.SetActive(false);
			gameObject.GetComponent<Button>().enabled = false;
		}
		base.transform.Find("ProgressBarSet").gameObject.SetActive(false);
	}

	// Token: 0x06000010 RID: 16 RVA: 0x000029DC File Offset: 0x00000BDC
	public void SelectItem(GameObject selectObject)
	{
		if (Array.IndexOf<GameObject>(this.ButtonList, selectObject) < 0)
		{
			return;
		}
		foreach (GameObject gameObject in this.ButtonList)
		{
			if (gameObject.activeSelf)
			{
				gameObject.SetActive(gameObject == selectObject);
			}
			gameObject.GetComponent<Button>().enabled = false;
		}
	}

	// Token: 0x06000011 RID: 17 RVA: 0x00002A33 File Offset: 0x00000C33
	public IEnumerator SetButton(string[] nameList, string[] textKeyList, bool isRandom = false)
	{
		if (nameList == null)
		{
			foreach (GameObject gameObject in this.ButtonList)
			{
				gameObject.SetActive(false);
				gameObject.GetComponent<Button>().enabled = false;
			}
			yield break;
		}
		nameList = AppUtil.GetChildArray(nameList, 0, true, 0);
		textKeyList = AppUtil.GetChildArray(textKeyList, 0, true, 0);
		int[] array = Enumerable.Range(0, textKeyList.Length).ToArray<int>();
		if (isRandom)
		{
			array = AppUtil.RandomArray(textKeyList.Length);
		}
		for (int j = 0; j < textKeyList.Length; j++)
		{
			this.ButtonList[j].name = nameList[array[j]];
			if (textKeyList != null)
			{
				string text = LanguageManager.Get(textKeyList[array[j]]);
				this.ButtonList[j].GetComponentInChildren<TextLocalization>(true).SetText(text);
			}
		}
		this.ButtonList[0].SetActive(true);
		float anchorY1 = this.ButtonList[0].GetComponent<RectTransform>().anchoredPosition.y;
		base.StartCoroutine(AppUtil.MoveEasingFloat((float)Screen.width, 0f, delegate(float tmp)
		{
			this.ButtonList[0].GetComponent<RectTransform>().anchoredPosition = new Vector2(tmp, anchorY1);
		}, false, 0.2f, EasingFunction.Ease.EaseOutCubic, null));
		yield return AppUtil.Wait(0.1f);
		if (textKeyList.Length >= 2 && !string.IsNullOrEmpty(textKeyList[1]))
		{
			this.ButtonList[1].SetActive(true);
			float anchorY2 = this.ButtonList[1].GetComponent<RectTransform>().anchoredPosition.y;
			base.StartCoroutine(AppUtil.MoveEasingFloat((float)Screen.width, 0f, delegate(float tmp)
			{
				this.ButtonList[1].GetComponent<RectTransform>().anchoredPosition = new Vector2(tmp, anchorY2);
			}, false, 0.2f, EasingFunction.Ease.EaseOutCubic, null));
			yield return AppUtil.Wait(0.1f);
		}
		if (textKeyList.Length >= 3 && !string.IsNullOrEmpty(textKeyList[2]))
		{
			this.ButtonList[2].SetActive(true);
			float anchorY3 = this.ButtonList[2].GetComponent<RectTransform>().anchoredPosition.y;
			base.StartCoroutine(AppUtil.MoveEasingFloat((float)Screen.width, 0f, delegate(float tmp)
			{
				this.ButtonList[2].GetComponent<RectTransform>().anchoredPosition = new Vector2(tmp, anchorY3);
			}, false, 0.2f, EasingFunction.Ease.EaseOutCubic, null));
			yield return AppUtil.Wait(0.1f);
		}
		yield return AppUtil.Wait(0.1f);
		GameObject[] buttonList = this.ButtonList;
		for (int i = 0; i < buttonList.Length; i++)
		{
			buttonList[i].GetComponent<Button>().enabled = true;
		}
		yield break;
	}

	// Token: 0x06000012 RID: 18 RVA: 0x00002A58 File Offset: 0x00000C58
	public void DismissButton()
	{
		GameObject[] buttonList = this.ButtonList;
		for (int i = 0; i < buttonList.Length; i++)
		{
			buttonList[i].SetActive(false);
		}
	}

	// Token: 0x06000013 RID: 19 RVA: 0x000025AD File Offset: 0x000007AD
	public ChoiceGroup()
	{
	}

	// Token: 0x04000004 RID: 4
	public GameObject[] ButtonList;

	// Token: 0x020000B3 RID: 179
	[CompilerGenerated]
	private sealed class <>c__DisplayClass3_0
	{
		// Token: 0x06000790 RID: 1936 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass3_0()
		{
		}

		// Token: 0x06000791 RID: 1937 RVA: 0x00026662 File Offset: 0x00024862
		internal void <SetButton>b__0(float tmp)
		{
			this.<>4__this.ButtonList[0].GetComponent<RectTransform>().anchoredPosition = new Vector2(tmp, this.anchorY1);
		}

		// Token: 0x0400029A RID: 666
		public ChoiceGroup <>4__this;

		// Token: 0x0400029B RID: 667
		public float anchorY1;
	}

	// Token: 0x020000B4 RID: 180
	[CompilerGenerated]
	private sealed class <>c__DisplayClass3_1
	{
		// Token: 0x06000792 RID: 1938 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass3_1()
		{
		}

		// Token: 0x06000793 RID: 1939 RVA: 0x00026687 File Offset: 0x00024887
		internal void <SetButton>b__1(float tmp)
		{
			this.CS$<>8__locals1.<>4__this.ButtonList[1].GetComponent<RectTransform>().anchoredPosition = new Vector2(tmp, this.anchorY2);
		}

		// Token: 0x0400029C RID: 668
		public float anchorY2;

		// Token: 0x0400029D RID: 669
		public ChoiceGroup.<>c__DisplayClass3_0 CS$<>8__locals1;
	}

	// Token: 0x020000B5 RID: 181
	[CompilerGenerated]
	private sealed class <>c__DisplayClass3_2
	{
		// Token: 0x06000794 RID: 1940 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass3_2()
		{
		}

		// Token: 0x06000795 RID: 1941 RVA: 0x000266B1 File Offset: 0x000248B1
		internal void <SetButton>b__2(float tmp)
		{
			this.CS$<>8__locals2.<>4__this.ButtonList[2].GetComponent<RectTransform>().anchoredPosition = new Vector2(tmp, this.anchorY3);
		}

		// Token: 0x0400029E RID: 670
		public float anchorY3;

		// Token: 0x0400029F RID: 671
		public ChoiceGroup.<>c__DisplayClass3_0 CS$<>8__locals2;
	}

	// Token: 0x020000B6 RID: 182
	[CompilerGenerated]
	private sealed class <SetButton>d__3 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000796 RID: 1942 RVA: 0x000266DB File Offset: 0x000248DB
		[DebuggerHidden]
		public <SetButton>d__3(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000797 RID: 1943 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000798 RID: 1944 RVA: 0x000266EC File Offset: 0x000248EC
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			ChoiceGroup choiceGroup = this;
			switch (num)
			{
			case 0:
			{
				this.<>1__state = -1;
				CS$<>8__locals1 = new ChoiceGroup.<>c__DisplayClass3_0();
				CS$<>8__locals1.<>4__this = this;
				if (nameList == null)
				{
					foreach (GameObject gameObject in choiceGroup.ButtonList)
					{
						gameObject.SetActive(false);
						gameObject.GetComponent<Button>().enabled = false;
					}
					return false;
				}
				nameList = AppUtil.GetChildArray(nameList, 0, true, 0);
				textKeyList = AppUtil.GetChildArray(textKeyList, 0, true, 0);
				int[] array = Enumerable.Range(0, textKeyList.Length).ToArray<int>();
				if (isRandom)
				{
					array = AppUtil.RandomArray(textKeyList.Length);
				}
				for (int j = 0; j < textKeyList.Length; j++)
				{
					choiceGroup.ButtonList[j].name = nameList[array[j]];
					if (textKeyList != null)
					{
						string text = LanguageManager.Get(textKeyList[array[j]]);
						choiceGroup.ButtonList[j].GetComponentInChildren<TextLocalization>(true).SetText(text);
					}
				}
				choiceGroup.ButtonList[0].SetActive(true);
				CS$<>8__locals1.anchorY1 = choiceGroup.ButtonList[0].GetComponent<RectTransform>().anchoredPosition.y;
				choiceGroup.StartCoroutine(AppUtil.MoveEasingFloat((float)Screen.width, 0f, new Action<float>(CS$<>8__locals1.<SetButton>b__0), false, 0.2f, EasingFunction.Ease.EaseOutCubic, null));
				this.<>2__current = AppUtil.Wait(0.1f);
				this.<>1__state = 1;
				return true;
			}
			case 1:
				this.<>1__state = -1;
				if (textKeyList.Length >= 2 && !string.IsNullOrEmpty(textKeyList[1]))
				{
					ChoiceGroup.<>c__DisplayClass3_1 CS$<>8__locals2 = new ChoiceGroup.<>c__DisplayClass3_1();
					CS$<>8__locals2.CS$<>8__locals1 = CS$<>8__locals1;
					choiceGroup.ButtonList[1].SetActive(true);
					CS$<>8__locals2.anchorY2 = choiceGroup.ButtonList[1].GetComponent<RectTransform>().anchoredPosition.y;
					choiceGroup.StartCoroutine(AppUtil.MoveEasingFloat((float)Screen.width, 0f, new Action<float>(CS$<>8__locals2.<SetButton>b__1), false, 0.2f, EasingFunction.Ease.EaseOutCubic, null));
					this.<>2__current = AppUtil.Wait(0.1f);
					this.<>1__state = 2;
					return true;
				}
				break;
			case 2:
				this.<>1__state = -1;
				break;
			case 3:
				this.<>1__state = -1;
				goto IL_310;
			case 4:
			{
				this.<>1__state = -1;
				GameObject[] buttonList = choiceGroup.ButtonList;
				for (int i = 0; i < buttonList.Length; i++)
				{
					buttonList[i].GetComponent<Button>().enabled = true;
				}
				return false;
			}
			default:
				return false;
			}
			if (textKeyList.Length >= 3 && !string.IsNullOrEmpty(textKeyList[2]))
			{
				ChoiceGroup.<>c__DisplayClass3_2 CS$<>8__locals3 = new ChoiceGroup.<>c__DisplayClass3_2();
				CS$<>8__locals3.CS$<>8__locals2 = CS$<>8__locals1;
				choiceGroup.ButtonList[2].SetActive(true);
				CS$<>8__locals3.anchorY3 = choiceGroup.ButtonList[2].GetComponent<RectTransform>().anchoredPosition.y;
				choiceGroup.StartCoroutine(AppUtil.MoveEasingFloat((float)Screen.width, 0f, new Action<float>(CS$<>8__locals3.<SetButton>b__2), false, 0.2f, EasingFunction.Ease.EaseOutCubic, null));
				this.<>2__current = AppUtil.Wait(0.1f);
				this.<>1__state = 3;
				return true;
			}
			IL_310:
			this.<>2__current = AppUtil.Wait(0.1f);
			this.<>1__state = 4;
			return true;
		}

		// Token: 0x170000CA RID: 202
		// (get) Token: 0x06000799 RID: 1945 RVA: 0x00026A52 File Offset: 0x00024C52
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0600079A RID: 1946 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000CB RID: 203
		// (get) Token: 0x0600079B RID: 1947 RVA: 0x00026A52 File Offset: 0x00024C52
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040002A0 RID: 672
		private int <>1__state;

		// Token: 0x040002A1 RID: 673
		private object <>2__current;

		// Token: 0x040002A2 RID: 674
		public ChoiceGroup <>4__this;

		// Token: 0x040002A3 RID: 675
		public string[] nameList;

		// Token: 0x040002A4 RID: 676
		public string[] textKeyList;

		// Token: 0x040002A5 RID: 677
		public bool isRandom;

		// Token: 0x040002A6 RID: 678
		private ChoiceGroup.<>c__DisplayClass3_0 <>8__1;
	}
}
