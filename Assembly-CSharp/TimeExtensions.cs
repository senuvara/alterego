﻿using System;

// Token: 0x02000023 RID: 35
internal static class TimeExtensions
{
	// Token: 0x0600012F RID: 303 RVA: 0x0000CFC8 File Offset: 0x0000B1C8
	public static string ToBinaryString(this DateTime dt)
	{
		return dt.ToBinary().ToString();
	}

	// Token: 0x06000130 RID: 304 RVA: 0x0000CFE4 File Offset: 0x0000B1E4
	public static DateTime Convert2DateTime(this string dt)
	{
		return DateTime.FromBinary(Convert.ToInt64(dt));
	}
}
