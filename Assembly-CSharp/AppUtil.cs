﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using App;
using SocialConnector;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000028 RID: 40
public class AppUtil
{
	// Token: 0x06000158 RID: 344 RVA: 0x0000EB77 File Offset: 0x0000CD77
	public static IEnumerator FadeIn(Object target, float time = 0.5f, Action<Object> callback = null)
	{
		yield return AppUtil.FadeInOut(target, true, time, callback);
		yield break;
	}

	// Token: 0x06000159 RID: 345 RVA: 0x0000EB94 File Offset: 0x0000CD94
	public static IEnumerator FadeOut(Object target, float time = 0.5f, Action<Object> callback = null)
	{
		yield return AppUtil.FadeInOut(target, false, time, callback);
		yield break;
	}

	// Token: 0x0600015A RID: 346 RVA: 0x0000EBB1 File Offset: 0x0000CDB1
	private static IEnumerator FadeInOut(Object target, bool fadein, float totalTime, Action<Object> callback)
	{
		if (target == null)
		{
			yield break;
		}
		float start = (float)(fadein ? 0 : 1);
		float end = (float)(fadein ? 1 : 0);
		float gap = end - start;
		AppUtil.SetAlpha(target, start);
		float time = 0f;
		for (;;)
		{
			time += Time.unscaledDeltaTime * Settings.GAME_SPEED;
			float num = start + gap * time / totalTime;
			if ((end - num) * gap < 0f)
			{
				break;
			}
			AppUtil.SetAlpha(target, num);
			yield return null;
		}
		AppUtil.SetAlpha(target, end);
		if (callback != null)
		{
			callback(target);
		}
		yield break;
	}

	// Token: 0x0600015B RID: 347 RVA: 0x0000EBD8 File Offset: 0x0000CDD8
	public static void SetAlphaChildren(GameObject target, float alpha)
	{
		Transform[] componentsInChildren = target.GetComponentsInChildren<Transform>(true);
		for (int i = 0; i < componentsInChildren.Length; i++)
		{
			AppUtil.SetAlpha(componentsInChildren[i], alpha);
		}
	}

	// Token: 0x0600015C RID: 348 RVA: 0x0000EC08 File Offset: 0x0000CE08
	public static bool SetAlpha(Object target, float alpha)
	{
		if (target == null)
		{
			return false;
		}
		if (target is GameObject)
		{
			GameObject gameObject = (GameObject)target;
			if (AppUtil.SetAlpha(gameObject.GetComponentInChildren<CanvasGroup>(), alpha))
			{
				return true;
			}
			bool flag = AppUtil.SetAlpha(gameObject.GetComponentInChildren<Image>(), alpha);
			bool flag2 = AppUtil.SetAlpha(gameObject.GetComponentInChildren<SpriteRenderer>(), alpha);
			bool flag3 = AppUtil.SetAlpha(gameObject.GetComponentInChildren<Text>(), alpha);
			bool flag4 = AppUtil.SetAlpha(gameObject.GetComponentInChildren<TextMesh>(), alpha);
			return flag || flag2 || flag3 || flag4;
		}
		else if (target is Transform)
		{
			Transform transform = (Transform)target;
			if (AppUtil.SetAlpha(transform.GetComponentInChildren<CanvasGroup>(), alpha))
			{
				return true;
			}
			bool flag5 = AppUtil.SetAlpha(transform.GetComponentInChildren<Image>(), alpha);
			bool flag6 = AppUtil.SetAlpha(transform.GetComponentInChildren<SpriteRenderer>(), alpha);
			bool flag7 = AppUtil.SetAlpha(transform.GetComponentInChildren<Text>(), alpha);
			bool flag8 = AppUtil.SetAlpha(transform.GetComponentInChildren<TextMesh>(), alpha);
			return flag5 || flag6 || flag7 || flag8;
		}
		else
		{
			if (target is Image)
			{
				Image image = (Image)target;
				Color color = image.color;
				color.a = alpha;
				image.color = color;
				return true;
			}
			if (target is Text)
			{
				Text text = (Text)target;
				Color color2 = text.color;
				color2.a = alpha;
				text.color = color2;
				return true;
			}
			if (target is SpriteRenderer)
			{
				SpriteRenderer spriteRenderer = (SpriteRenderer)target;
				Color color3 = spriteRenderer.color;
				color3.a = alpha;
				spriteRenderer.color = color3;
				return true;
			}
			if (target is CanvasGroup)
			{
				((CanvasGroup)target).alpha = alpha;
				return true;
			}
			if (target is TextMesh)
			{
				TextMesh textMesh = (TextMesh)target;
				Color color4 = textMesh.color;
				color4.a = alpha;
				textMesh.color = color4;
				return true;
			}
			return false;
		}
	}

	// Token: 0x0600015D RID: 349 RVA: 0x0000ED99 File Offset: 0x0000CF99
	public static IEnumerator TypeText(string text, Object target)
	{
		float time = 0f;
		int length = 0;
		string orgText = "";
		if (target is Text)
		{
			orgText = ((Text)target).text;
		}
		else if (target is TextMesh)
		{
			orgText = ((TextMesh)target).text;
		}
		while (length <= text.Length)
		{
			time += Time.deltaTime;
			length = (int)(time * 20f);
			int length2 = Mathf.Min(length, text.Length);
			if (target is Text)
			{
				((Text)target).text = orgText + text.Substring(0, length2);
			}
			else if (target is TextMesh)
			{
				((TextMesh)target).text = orgText + text.Substring(0, length2);
			}
			yield return null;
		}
		yield break;
	}

	// Token: 0x0600015E RID: 350 RVA: 0x0000EDAF File Offset: 0x0000CFAF
	public static IEnumerator ShakeObject(GameObject target, float totalTime = 0.25f)
	{
		float strength = (target.GetComponentInChildren<MaskableGraphic>() == null) ? 0.2f : 20f;
		Vector3 orgPosition = target.transform.position;
		int max = (int)(totalTime / 0.05f);
		int num;
		for (int i = 0; i < max; i = num + 1)
		{
			target.transform.position = new Vector3(orgPosition.x + Random.Range(-strength, strength), orgPosition.y + Random.Range(-strength, strength), orgPosition.z);
			yield return AppUtil.Wait(0.05f);
			num = i;
		}
		target.transform.position = orgPosition;
		yield break;
	}

	// Token: 0x0600015F RID: 351 RVA: 0x0000EDC8 File Offset: 0x0000CFC8
	public static Vector3 GetMousePosition()
	{
		Vector3 position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.z);
		return Camera.main.ScreenToWorldPoint(position);
	}

	// Token: 0x06000160 RID: 352 RVA: 0x0000EE0F File Offset: 0x0000D00F
	public static IEnumerator Wait(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		yield break;
	}

	// Token: 0x06000161 RID: 353 RVA: 0x0000EE1E File Offset: 0x0000D01E
	public static IEnumerator WaitRealtime(float waitTime)
	{
		yield return new WaitForSecondsRealtime(waitTime / Settings.GAME_SPEED);
		yield break;
	}

	// Token: 0x06000162 RID: 354 RVA: 0x0000EE2D File Offset: 0x0000D02D
	public static string ConvertToJson(byte[] data)
	{
		return JsonUtility.ToJson(new AppUtil.JsonString(AppUtil.ReadDataSJIS(data, false)), true);
	}

	// Token: 0x06000163 RID: 355 RVA: 0x0000EE41 File Offset: 0x0000D041
	public static string ConvertSJISToJson(byte[] data)
	{
		return JsonUtility.ToJson(new AppUtil.JsonString(AppUtil.ReadDataSJIS(data, true)), true);
	}

	// Token: 0x06000164 RID: 356 RVA: 0x0000EE58 File Offset: 0x0000D058
	public static string[] ReadDataSJIS(byte[] data, bool sjis = true)
	{
		string path = Application.temporaryCachePath + "/sjisdata.txt";
		if (File.Exists(path))
		{
			File.Delete(path);
		}
		File.WriteAllBytes(path, data);
		FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
		StreamReader streamReader = new StreamReader(fileStream, sjis ? Encoding.GetEncoding(932) : Encoding.UTF8);
		List<string> list = new List<string>();
		while (!streamReader.EndOfStream)
		{
			string text = streamReader.ReadLine();
			if (sjis)
			{
				text = text.Replace("(?；ω；｀)", "(´；ω；｀)").Replace("(｀；ω；?)", "(｀；ω；´)");
			}
			list.Add(text);
		}
		streamReader.Close();
		fileStream.Close();
		return list.ToArray();
	}

	// Token: 0x06000165 RID: 357 RVA: 0x0000EF05 File Offset: 0x0000D105
	public static IEnumerator MoveEasingFloat(float startValue, float endValue, Action<float> callback, bool unscaled = true, float time = 0.75f, EasingFunction.Ease type = EasingFunction.Ease.EaseInOutQuint, Action callback2 = null)
	{
		callback(startValue);
		EasingFunction.Function easingFunction = EasingFunction.GetEasingFunction(type);
		float t = 0f;
		while (t <= time)
		{
			yield return new WaitForEndOfFrame();
			if (unscaled)
			{
				t += Time.unscaledDeltaTime * Settings.GAME_SPEED;
			}
			else
			{
				t += Time.deltaTime * Settings.GAME_SPEED;
			}
			callback(easingFunction(startValue, endValue, t / time));
		}
		callback(endValue);
		if (callback2 != null)
		{
			callback2();
		}
		yield return new WaitForEndOfFrame();
		yield break;
	}

	// Token: 0x06000166 RID: 358 RVA: 0x0000EF41 File Offset: 0x0000D141
	public static IEnumerator MoveEasingVector3(Vector3 startValue, Vector3 endValue, Action<Vector3> callback, bool unscaled = true, float time = 0.75f, EasingFunction.Ease type = EasingFunction.Ease.EaseInOutQuint, Action callback2 = null)
	{
		callback(startValue);
		EasingFunction.Function easingFunction = EasingFunction.GetEasingFunction(type);
		float t = 0f;
		while (t <= time)
		{
			yield return new WaitForEndOfFrame();
			if (unscaled)
			{
				t += Time.unscaledDeltaTime * Settings.GAME_SPEED;
			}
			else
			{
				t += Time.deltaTime * Settings.GAME_SPEED;
			}
			Vector3 obj = new Vector3(easingFunction(startValue.x, endValue.x, t / time), easingFunction(startValue.y, endValue.y, t / time), easingFunction(startValue.z, endValue.z, t / time));
			callback(obj);
		}
		callback(endValue);
		if (callback2 != null)
		{
			callback2();
		}
		yield return new WaitForEndOfFrame();
		yield break;
	}

	// Token: 0x06000167 RID: 359 RVA: 0x0000EF7D File Offset: 0x0000D17D
	public static void DelayAction(MonoBehaviour caller, float waitTime, Action waitAction, bool unscaled = true)
	{
		caller.StartCoroutine(AppUtil.DelayAction(waitTime, waitAction, unscaled));
	}

	// Token: 0x06000168 RID: 360 RVA: 0x0000EF8E File Offset: 0x0000D18E
	public static IEnumerator DelayAction(float waitTime, Action waitAction, bool unscaled = true)
	{
		if (unscaled)
		{
			yield return AppUtil.WaitRealtime(waitTime);
		}
		else
		{
			yield return AppUtil.Wait(waitTime);
		}
		waitAction();
		yield break;
	}

	// Token: 0x06000169 RID: 361 RVA: 0x0000EFAB File Offset: 0x0000D1AB
	public static void DelayAction(MonoBehaviour caller, float waitTime, IEnumerator waitAction, bool unscaled = true)
	{
		caller.StartCoroutine(AppUtil.DelayAction(waitTime, waitAction, unscaled));
	}

	// Token: 0x0600016A RID: 362 RVA: 0x0000EFBC File Offset: 0x0000D1BC
	public static IEnumerator DelayAction(float waitTime, IEnumerator waitAction, bool unscaled = true)
	{
		if (unscaled)
		{
			yield return AppUtil.WaitRealtime(waitTime);
		}
		else
		{
			yield return AppUtil.Wait(waitTime);
		}
		yield return waitAction;
		yield break;
	}

	// Token: 0x0600016B RID: 363 RVA: 0x0000EFD9 File Offset: 0x0000D1D9
	public static bool HasAttribute(MemberInfo info, Type checkType)
	{
		return Attribute.GetCustomAttributes(info, checkType).Length != 0;
	}

	// Token: 0x0600016C RID: 364 RVA: 0x0000EFE8 File Offset: 0x0000D1E8
	public static string GetTitle(MemberInfo info)
	{
		Attribute customAttribute = Attribute.GetCustomAttribute(info, typeof(AppUtil.TitleAttribute));
		if (customAttribute == null)
		{
			return info.Name;
		}
		return ((AppUtil.TitleAttribute)customAttribute).Title;
	}

	// Token: 0x0600016D RID: 365 RVA: 0x0000F01C File Offset: 0x0000D21C
	public static string[] GetValueArray(MemberInfo info)
	{
		Attribute customAttribute = Attribute.GetCustomAttribute(info, typeof(AppUtil.ArrayValueAttribute));
		if (customAttribute == null)
		{
			return null;
		}
		FieldInfo field = info.DeclaringType.GetField(((AppUtil.ArrayValueAttribute)customAttribute).Data);
		if (field != null)
		{
			return (string[])field.GetValue(null);
		}
		return (string[])info.DeclaringType.GetProperty(((AppUtil.ArrayValueAttribute)customAttribute).Data).GetValue(null, null);
	}

	// Token: 0x0600016E RID: 366 RVA: 0x0000F08E File Offset: 0x0000D28E
	public static List<KeyValuePair<int, int>> ReverseByValue(Dictionary<int, int> dict)
	{
		List<KeyValuePair<int, int>> list = new List<KeyValuePair<int, int>>(dict);
		list.Sort(delegate(KeyValuePair<int, int> kvp1, KeyValuePair<int, int> kvp2)
		{
			if (kvp2.Value == kvp1.Value)
			{
				return kvp1.Key - kvp2.Key;
			}
			return kvp2.Value - kvp1.Value;
		});
		return list;
	}

	// Token: 0x0600016F RID: 367 RVA: 0x0000F0BB File Offset: 0x0000D2BB
	public static List<KeyValuePair<string, int>> ReverseByValueS(Dictionary<string, int> dict)
	{
		List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>(dict);
		list.Sort((KeyValuePair<string, int> kvp1, KeyValuePair<string, int> kvp2) => kvp2.Value - kvp1.Value);
		return list;
	}

	// Token: 0x06000170 RID: 368 RVA: 0x0000F0E8 File Offset: 0x0000D2E8
	public static T Shift<T>(IList<T> self)
	{
		T result = self[0];
		self.RemoveAt(0);
		return result;
	}

	// Token: 0x06000171 RID: 369 RVA: 0x0000F0F8 File Offset: 0x0000D2F8
	public static T Pop<T>(IList<T> self)
	{
		T result = self[self.Count - 1];
		self.RemoveAt(self.Count - 1);
		return result;
	}

	// Token: 0x06000172 RID: 370 RVA: 0x0000F118 File Offset: 0x0000D318
	public static T GetAt<T>(IList<T> self, T main, int index = 1)
	{
		int num = self.IndexOf(main);
		num += index;
		if (num < 0)
		{
			num = self.Count - 1;
		}
		else if (num >= self.Count)
		{
			num = 0;
		}
		return self[num];
	}

	// Token: 0x06000173 RID: 371 RVA: 0x0000F152 File Offset: 0x0000D352
	public static string ToString(int[] array)
	{
		return string.Join(",", (from x in array
		select x.ToString()).ToArray<string>());
	}

	// Token: 0x06000174 RID: 372 RVA: 0x0000F188 File Offset: 0x0000D388
	public static int SetBitFlag(int baseNum, int flagNum, bool flagOn)
	{
		int num = (int)Mathf.Log(2.1474836E+09f, 2f) + 1;
		if (flagNum >= num)
		{
			global::Debug.LogError(string.Concat(new object[]
			{
				"SetBitFlag() 桁あふれエラー flagNum(",
				flagNum,
				")>=maxDigit(",
				num,
				")",
				Convert.ToString(int.MaxValue, 2)
			}));
		}
		int num2 = 1 << flagNum;
		if (flagOn)
		{
			baseNum |= num2;
		}
		else
		{
			baseNum &= ~num2;
		}
		return baseNum;
	}

	// Token: 0x06000175 RID: 373 RVA: 0x0000F20C File Offset: 0x0000D40C
	public static bool BitFlagIsSet(int baseNum, int flagNum)
	{
		int num = 1 << flagNum;
		return (baseNum & num) != 0;
	}

	// Token: 0x06000176 RID: 374 RVA: 0x0000F228 File Offset: 0x0000D428
	public static Vector2 Vector2(string data, float factor = 1f)
	{
		char[] separator = new char[]
		{
			','
		};
		string[] array = data.Split(separator);
		return new Vector2(float.Parse(array[0]) / factor, float.Parse(array[1]) / factor);
	}

	// Token: 0x06000177 RID: 375 RVA: 0x0000F262 File Offset: 0x0000D462
	public static IEnumerator WaitAnimation(GameObject animObject, Action callback = null)
	{
		if (animObject == null)
		{
			yield break;
		}
		yield return AppUtil.WaitAnimation(animObject.GetComponent<Animator>(), callback);
		yield break;
	}

	// Token: 0x06000178 RID: 376 RVA: 0x0000F278 File Offset: 0x0000D478
	public static IEnumerator WaitAnimation(Animator anim, Action callback = null)
	{
		if (anim == null)
		{
			yield break;
		}
		if (anim.updateMode == AnimatorUpdateMode.UnscaledTime)
		{
			yield return AppUtil.WaitRealtime(anim.GetCurrentAnimatorClipInfo(0)[0].clip.length);
		}
		else
		{
			yield return AppUtil.Wait(anim.GetCurrentAnimatorClipInfo(0)[0].clip.length);
		}
		if (callback != null)
		{
			callback();
		}
		yield break;
	}

	// Token: 0x06000179 RID: 377 RVA: 0x0000F290 File Offset: 0x0000D490
	public static Color CreateColor(string htmlColor)
	{
		Color result;
		ColorUtility.TryParseHtmlString(htmlColor, out result);
		return result;
	}

	// Token: 0x0600017A RID: 378 RVA: 0x0000F2A7 File Offset: 0x0000D4A7
	public static int[] RandomArray(int count)
	{
		return (from tmp in Enumerable.Range(0, count)
		orderby Guid.NewGuid()
		select tmp).ToArray<int>();
	}

	// Token: 0x0600017B RID: 379 RVA: 0x0000F2D9 File Offset: 0x0000D4D9
	public static int[] RandomArray(int start, int count)
	{
		return (from tmp in Enumerable.Range(start, count)
		orderby Guid.NewGuid()
		select tmp).ToArray<int>();
	}

	// Token: 0x0600017C RID: 380 RVA: 0x0000F30B File Offset: 0x0000D50B
	public static IEnumerator RectTransformCapture(RectTransform captureTarget)
	{
		yield return new WaitForEndOfFrame();
		string path = "Externals/Log/Temp/" + "Screenshot" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".png";
		Vector3[] array = new Vector3[4];
		captureTarget.GetWorldCorners(array);
		Rect source = new Rect(array[0].x, array[0].y, array[2].x - array[0].x, array[2].y - array[0].y);
		if (captureTarget.GetComponentInParent<Canvas>().renderMode == RenderMode.ScreenSpaceCamera)
		{
			Vector2 vector = Camera.main.WorldToScreenPoint(array[0]);
			Vector2 vector2 = Camera.main.WorldToScreenPoint(array[2]);
			source = new Rect(vector.x, vector.y, vector2.x - vector.x, vector2.y - vector.y);
		}
		Texture2D texture2D = new Texture2D((int)source.width, (int)source.height);
		texture2D.ReadPixels(source, 0, 0);
		texture2D.Apply();
		byte[] bytes = texture2D.EncodeToPNG();
		File.WriteAllBytes(path, bytes);
		yield break;
	}

	// Token: 0x0600017D RID: 381 RVA: 0x0000F31C File Offset: 0x0000D51C
	public static void ShareLocalImage(string msg, string url, string resourcePath)
	{
		byte[] bytes = Resources.Load<TextAsset>(resourcePath).bytes;
		string text = Application.persistentDataPath + "/shareimage.jpg";
		File.WriteAllBytes(text, bytes);
		SocialConnector.Share(msg, url, text);
	}

	// Token: 0x0600017E RID: 382 RVA: 0x0000F354 File Offset: 0x0000D554
	public static IEnumerator Share(bool showDialog, string msg, string url, RectTransform captureTarget = null)
	{
		yield return new WaitForEndOfFrame();
		Rect source = new Rect(0f, 0f, (float)Screen.width, (float)Screen.height);
		if (captureTarget != null)
		{
			Vector3[] array = new Vector3[4];
			captureTarget.GetWorldCorners(array);
			source = new Rect(array[0].x, array[0].y, array[2].x - array[0].x, array[2].y - array[0].y);
			if (captureTarget.GetComponentInParent<Canvas>().renderMode == RenderMode.ScreenSpaceCamera)
			{
				Vector2 vector = Camera.main.WorldToScreenPoint(array[0]);
				Vector2 vector2 = Camera.main.WorldToScreenPoint(array[2]);
				source = new Rect(vector.x, vector.y, vector2.x - vector.x, vector2.y - vector.y);
				if (source.x < 0f)
				{
					source.width += source.x;
					source.x = 0f;
				}
				if (source.width + source.x > (float)Screen.width)
				{
					source.width = (float)Screen.width - source.x;
				}
				if (source.y < 0f)
				{
					source.height += source.y;
					source.y = 0f;
				}
				if (source.height + source.y > (float)Screen.height)
				{
					source.height = (float)Screen.height - source.y;
				}
			}
		}
		Texture2D texture2D = new Texture2D((int)source.width, (int)source.height);
		texture2D.ReadPixels(source, 0, 0);
		texture2D.Apply();
		byte[] bytes = texture2D.EncodeToJPG();
		string text = Application.persistentDataPath + "/shareimage.jpg";
		File.WriteAllBytes(text, bytes);
		if (showDialog)
		{
			DialogManager.ShowDialog("ShareDialog", new object[]
			{
				msg,
				url,
				texture2D,
				text
			});
		}
		else
		{
			SocialConnector.Share(msg, url, text);
		}
		yield break;
	}

	// Token: 0x0600017F RID: 383 RVA: 0x0000F378 File Offset: 0x0000D578
	public static string[] GetChildArray(string[] parent, int start, bool trim = true, int childCount = 0)
	{
		if (childCount == 0)
		{
			childCount = parent.Length - start;
		}
		for (int i = start + childCount - 1; i >= start; i--)
		{
			if (trim && (parent[i] == null || parent[i] == ""))
			{
				childCount--;
			}
		}
		string[] array = new string[childCount];
		Array.Copy(parent, start, array, 0, childCount);
		return array;
	}

	// Token: 0x06000180 RID: 384 RVA: 0x0000F3CD File Offset: 0x0000D5CD
	public static void SetFalse(GameObject target)
	{
		if (target != null)
		{
			target.SetActive(false);
		}
	}

	// Token: 0x06000181 RID: 385 RVA: 0x0000F3DF File Offset: 0x0000D5DF
	public static void SetFalse(string name)
	{
		AppUtil.SetFalse(GameObject.Find(name));
	}

	// Token: 0x06000182 RID: 386 RVA: 0x0000F3EC File Offset: 0x0000D5EC
	public static float GetOSVersion()
	{
		return new AndroidJavaClass("android.os.Build$VERSION").GetStatic<float>("SDK_INT");
	}

	// Token: 0x06000183 RID: 387 RVA: 0x0000F402 File Offset: 0x0000D602
	public static void OpenReviewURL()
	{
		Application.OpenURL("https://play.google.com/store/apps/details?id=" + BuildInfo.APP_BUNDLE_ID);
	}

	// Token: 0x06000184 RID: 388 RVA: 0x00007AD6 File Offset: 0x00005CD6
	public static bool CallReviewView()
	{
		return false;
	}

	// Token: 0x06000185 RID: 389 RVA: 0x0000F418 File Offset: 0x0000D618
	public static bool SetClipBoard(string copyText)
	{
		return new AndroidJavaObject("com.caracolu.appcommon.Util", Array.Empty<object>()).Call<bool>("SetClipData", new object[]
		{
			copyText
		});
	}

	// Token: 0x06000186 RID: 390 RVA: 0x000043CE File Offset: 0x000025CE
	public AppUtil()
	{
	}

	// Token: 0x06000187 RID: 391 RVA: 0x0000F440 File Offset: 0x0000D640
	// Note: this type is marked as 'beforefieldinit'.
	static AppUtil()
	{
	}

	// Token: 0x0400009A RID: 154
	public static string[] ToFullWidth = new string[]
	{
		"０",
		"１",
		"２",
		"３",
		"４",
		"５",
		"６",
		"７",
		"８",
		"９"
	};

	// Token: 0x02000104 RID: 260
	public class JsonString
	{
		// Token: 0x060008E6 RID: 2278 RVA: 0x0002D6D9 File Offset: 0x0002B8D9
		public JsonString()
		{
			this.string_array = new string[0];
		}

		// Token: 0x060008E7 RID: 2279 RVA: 0x0002D6ED File Offset: 0x0002B8ED
		public JsonString(string[] data)
		{
			this.string_array = data;
		}

		// Token: 0x040003E2 RID: 994
		public string[] string_array;
	}

	// Token: 0x02000105 RID: 261
	// (Invoke) Token: 0x060008E9 RID: 2281
	public delegate IEnumerator Coroutine();

	// Token: 0x02000106 RID: 262
	[AttributeUsage(AttributeTargets.Class)]
	public class DataRangeAttribute : Attribute
	{
		// Token: 0x060008EC RID: 2284 RVA: 0x000158E0 File Offset: 0x00013AE0
		public DataRangeAttribute()
		{
		}
	}

	// Token: 0x02000107 RID: 263
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Property | AttributeTargets.Field)]
	public class HideAttribute : Attribute
	{
		// Token: 0x060008ED RID: 2285 RVA: 0x000158E0 File Offset: 0x00013AE0
		public HideAttribute()
		{
		}
	}

	// Token: 0x02000108 RID: 264
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Property | AttributeTargets.Field)]
	public class TitleAttribute : Attribute
	{
		// Token: 0x060008EE RID: 2286 RVA: 0x0002D6FC File Offset: 0x0002B8FC
		public TitleAttribute(string title)
		{
			this.Title = title;
		}

		// Token: 0x040003E3 RID: 995
		public string Title;
	}

	// Token: 0x02000109 RID: 265
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Property | AttributeTargets.Field)]
	public class ArrayValueAttribute : Attribute
	{
		// Token: 0x060008EF RID: 2287 RVA: 0x0002D70B File Offset: 0x0002B90B
		public ArrayValueAttribute(string data)
		{
			this.Data = data;
		}

		// Token: 0x040003E4 RID: 996
		public string Data;
	}

	// Token: 0x0200010A RID: 266
	[CompilerGenerated]
	private sealed class <FadeIn>d__3 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060008F0 RID: 2288 RVA: 0x0002D71A File Offset: 0x0002B91A
		[DebuggerHidden]
		public <FadeIn>d__3(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060008F1 RID: 2289 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060008F2 RID: 2290 RVA: 0x0002D72C File Offset: 0x0002B92C
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			if (num == 0)
			{
				this.<>1__state = -1;
				this.<>2__current = AppUtil.FadeInOut(target, true, time, callback);
				this.<>1__state = 1;
				return true;
			}
			if (num != 1)
			{
				return false;
			}
			this.<>1__state = -1;
			return false;
		}

		// Token: 0x1700011C RID: 284
		// (get) Token: 0x060008F3 RID: 2291 RVA: 0x0002D77F File Offset: 0x0002B97F
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060008F4 RID: 2292 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x1700011D RID: 285
		// (get) Token: 0x060008F5 RID: 2293 RVA: 0x0002D77F File Offset: 0x0002B97F
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040003E5 RID: 997
		private int <>1__state;

		// Token: 0x040003E6 RID: 998
		private object <>2__current;

		// Token: 0x040003E7 RID: 999
		public Object target;

		// Token: 0x040003E8 RID: 1000
		public float time;

		// Token: 0x040003E9 RID: 1001
		public Action<Object> callback;
	}

	// Token: 0x0200010B RID: 267
	[CompilerGenerated]
	private sealed class <FadeOut>d__4 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060008F6 RID: 2294 RVA: 0x0002D787 File Offset: 0x0002B987
		[DebuggerHidden]
		public <FadeOut>d__4(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060008F7 RID: 2295 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060008F8 RID: 2296 RVA: 0x0002D798 File Offset: 0x0002B998
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			if (num == 0)
			{
				this.<>1__state = -1;
				this.<>2__current = AppUtil.FadeInOut(target, false, time, callback);
				this.<>1__state = 1;
				return true;
			}
			if (num != 1)
			{
				return false;
			}
			this.<>1__state = -1;
			return false;
		}

		// Token: 0x1700011E RID: 286
		// (get) Token: 0x060008F9 RID: 2297 RVA: 0x0002D7EB File Offset: 0x0002B9EB
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060008FA RID: 2298 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x1700011F RID: 287
		// (get) Token: 0x060008FB RID: 2299 RVA: 0x0002D7EB File Offset: 0x0002B9EB
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040003EA RID: 1002
		private int <>1__state;

		// Token: 0x040003EB RID: 1003
		private object <>2__current;

		// Token: 0x040003EC RID: 1004
		public Object target;

		// Token: 0x040003ED RID: 1005
		public float time;

		// Token: 0x040003EE RID: 1006
		public Action<Object> callback;
	}

	// Token: 0x0200010C RID: 268
	[CompilerGenerated]
	private sealed class <FadeInOut>d__5 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060008FC RID: 2300 RVA: 0x0002D7F3 File Offset: 0x0002B9F3
		[DebuggerHidden]
		public <FadeInOut>d__5(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060008FD RID: 2301 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060008FE RID: 2302 RVA: 0x0002D804 File Offset: 0x0002BA04
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			if (num != 0)
			{
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
			}
			else
			{
				this.<>1__state = -1;
				if (target == null)
				{
					return false;
				}
				start = (float)(fadein ? 0 : 1);
				end = (float)(fadein ? 1 : 0);
				gap = end - start;
				AppUtil.SetAlpha(target, start);
				time = 0f;
			}
			time += Time.unscaledDeltaTime * Settings.GAME_SPEED;
			float num2 = start + gap * time / totalTime;
			if ((end - num2) * gap >= 0f)
			{
				AppUtil.SetAlpha(target, num2);
				this.<>2__current = null;
				this.<>1__state = 1;
				return true;
			}
			AppUtil.SetAlpha(target, end);
			if (callback != null)
			{
				callback(target);
			}
			return false;
		}

		// Token: 0x17000120 RID: 288
		// (get) Token: 0x060008FF RID: 2303 RVA: 0x0002D92D File Offset: 0x0002BB2D
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000900 RID: 2304 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000121 RID: 289
		// (get) Token: 0x06000901 RID: 2305 RVA: 0x0002D92D File Offset: 0x0002BB2D
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040003EF RID: 1007
		private int <>1__state;

		// Token: 0x040003F0 RID: 1008
		private object <>2__current;

		// Token: 0x040003F1 RID: 1009
		public Object target;

		// Token: 0x040003F2 RID: 1010
		public bool fadein;

		// Token: 0x040003F3 RID: 1011
		public float totalTime;

		// Token: 0x040003F4 RID: 1012
		public Action<Object> callback;

		// Token: 0x040003F5 RID: 1013
		private float <start>5__2;

		// Token: 0x040003F6 RID: 1014
		private float <end>5__3;

		// Token: 0x040003F7 RID: 1015
		private float <gap>5__4;

		// Token: 0x040003F8 RID: 1016
		private float <time>5__5;
	}

	// Token: 0x0200010D RID: 269
	[CompilerGenerated]
	private sealed class <TypeText>d__8 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000902 RID: 2306 RVA: 0x0002D935 File Offset: 0x0002BB35
		[DebuggerHidden]
		public <TypeText>d__8(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000903 RID: 2307 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000904 RID: 2308 RVA: 0x0002D944 File Offset: 0x0002BB44
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			if (num != 0)
			{
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
			}
			else
			{
				this.<>1__state = -1;
				time = 0f;
				length = 0;
				orgText = "";
				if (target is Text)
				{
					orgText = ((Text)target).text;
				}
				else if (target is TextMesh)
				{
					orgText = ((TextMesh)target).text;
				}
			}
			if (length > text.Length)
			{
				return false;
			}
			time += Time.deltaTime;
			length = (int)(time * 20f);
			int length2 = Mathf.Min(length, text.Length);
			if (target is Text)
			{
				((Text)target).text = orgText + text.Substring(0, length2);
			}
			else if (target is TextMesh)
			{
				((TextMesh)target).text = orgText + text.Substring(0, length2);
			}
			this.<>2__current = null;
			this.<>1__state = 1;
			return true;
		}

		// Token: 0x17000122 RID: 290
		// (get) Token: 0x06000905 RID: 2309 RVA: 0x0002DAB1 File Offset: 0x0002BCB1
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000906 RID: 2310 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000123 RID: 291
		// (get) Token: 0x06000907 RID: 2311 RVA: 0x0002DAB1 File Offset: 0x0002BCB1
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040003F9 RID: 1017
		private int <>1__state;

		// Token: 0x040003FA RID: 1018
		private object <>2__current;

		// Token: 0x040003FB RID: 1019
		public Object target;

		// Token: 0x040003FC RID: 1020
		public string text;

		// Token: 0x040003FD RID: 1021
		private float <time>5__2;

		// Token: 0x040003FE RID: 1022
		private int <length>5__3;

		// Token: 0x040003FF RID: 1023
		private string <orgText>5__4;
	}

	// Token: 0x0200010E RID: 270
	[CompilerGenerated]
	private sealed class <ShakeObject>d__9 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000908 RID: 2312 RVA: 0x0002DAB9 File Offset: 0x0002BCB9
		[DebuggerHidden]
		public <ShakeObject>d__9(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000909 RID: 2313 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x0600090A RID: 2314 RVA: 0x0002DAC8 File Offset: 0x0002BCC8
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			if (num != 0)
			{
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				int num2 = i;
				i = num2 + 1;
			}
			else
			{
				this.<>1__state = -1;
				strength = ((target.GetComponentInChildren<MaskableGraphic>() == null) ? 0.2f : 20f);
				orgPosition = target.transform.position;
				max = (int)(totalTime / 0.05f);
				i = 0;
			}
			if (i >= max)
			{
				target.transform.position = orgPosition;
				return false;
			}
			target.transform.position = new Vector3(orgPosition.x + Random.Range(-strength, strength), orgPosition.y + Random.Range(-strength, strength), orgPosition.z);
			this.<>2__current = AppUtil.Wait(0.05f);
			this.<>1__state = 1;
			return true;
		}

		// Token: 0x17000124 RID: 292
		// (get) Token: 0x0600090B RID: 2315 RVA: 0x0002DBFD File Offset: 0x0002BDFD
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0600090C RID: 2316 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000125 RID: 293
		// (get) Token: 0x0600090D RID: 2317 RVA: 0x0002DBFD File Offset: 0x0002BDFD
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x04000400 RID: 1024
		private int <>1__state;

		// Token: 0x04000401 RID: 1025
		private object <>2__current;

		// Token: 0x04000402 RID: 1026
		public GameObject target;

		// Token: 0x04000403 RID: 1027
		public float totalTime;

		// Token: 0x04000404 RID: 1028
		private float <strength>5__2;

		// Token: 0x04000405 RID: 1029
		private Vector3 <orgPosition>5__3;

		// Token: 0x04000406 RID: 1030
		private int <max>5__4;

		// Token: 0x04000407 RID: 1031
		private int <i>5__5;
	}

	// Token: 0x0200010F RID: 271
	[CompilerGenerated]
	private sealed class <Wait>d__11 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x0600090E RID: 2318 RVA: 0x0002DC05 File Offset: 0x0002BE05
		[DebuggerHidden]
		public <Wait>d__11(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x0600090F RID: 2319 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000910 RID: 2320 RVA: 0x0002DC14 File Offset: 0x0002BE14
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			if (num == 0)
			{
				this.<>1__state = -1;
				this.<>2__current = new WaitForSeconds(waitTime);
				this.<>1__state = 1;
				return true;
			}
			if (num != 1)
			{
				return false;
			}
			this.<>1__state = -1;
			return false;
		}

		// Token: 0x17000126 RID: 294
		// (get) Token: 0x06000911 RID: 2321 RVA: 0x0002DC5A File Offset: 0x0002BE5A
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000912 RID: 2322 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000127 RID: 295
		// (get) Token: 0x06000913 RID: 2323 RVA: 0x0002DC5A File Offset: 0x0002BE5A
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x04000408 RID: 1032
		private int <>1__state;

		// Token: 0x04000409 RID: 1033
		private object <>2__current;

		// Token: 0x0400040A RID: 1034
		public float waitTime;
	}

	// Token: 0x02000110 RID: 272
	[CompilerGenerated]
	private sealed class <WaitRealtime>d__12 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000914 RID: 2324 RVA: 0x0002DC62 File Offset: 0x0002BE62
		[DebuggerHidden]
		public <WaitRealtime>d__12(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000915 RID: 2325 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000916 RID: 2326 RVA: 0x0002DC74 File Offset: 0x0002BE74
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			if (num == 0)
			{
				this.<>1__state = -1;
				this.<>2__current = new WaitForSecondsRealtime(waitTime / Settings.GAME_SPEED);
				this.<>1__state = 1;
				return true;
			}
			if (num != 1)
			{
				return false;
			}
			this.<>1__state = -1;
			return false;
		}

		// Token: 0x17000128 RID: 296
		// (get) Token: 0x06000917 RID: 2327 RVA: 0x0002DCC0 File Offset: 0x0002BEC0
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000918 RID: 2328 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000129 RID: 297
		// (get) Token: 0x06000919 RID: 2329 RVA: 0x0002DCC0 File Offset: 0x0002BEC0
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0400040B RID: 1035
		private int <>1__state;

		// Token: 0x0400040C RID: 1036
		private object <>2__current;

		// Token: 0x0400040D RID: 1037
		public float waitTime;
	}

	// Token: 0x02000111 RID: 273
	[CompilerGenerated]
	private sealed class <MoveEasingFloat>d__16 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x0600091A RID: 2330 RVA: 0x0002DCC8 File Offset: 0x0002BEC8
		[DebuggerHidden]
		public <MoveEasingFloat>d__16(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x0600091B RID: 2331 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x0600091C RID: 2332 RVA: 0x0002DCD8 File Offset: 0x0002BED8
		bool IEnumerator.MoveNext()
		{
			switch (this.<>1__state)
			{
			case 0:
				this.<>1__state = -1;
				callback(startValue);
				easingFunction = EasingFunction.GetEasingFunction(type);
				t = 0f;
				break;
			case 1:
				this.<>1__state = -1;
				if (unscaled)
				{
					t += Time.unscaledDeltaTime * Settings.GAME_SPEED;
				}
				else
				{
					t += Time.deltaTime * Settings.GAME_SPEED;
				}
				callback(easingFunction(startValue, endValue, t / time));
				break;
			case 2:
				this.<>1__state = -1;
				return false;
			default:
				return false;
			}
			if (t > time)
			{
				callback(endValue);
				if (callback2 != null)
				{
					callback2();
				}
				this.<>2__current = new WaitForEndOfFrame();
				this.<>1__state = 2;
				return true;
			}
			this.<>2__current = new WaitForEndOfFrame();
			this.<>1__state = 1;
			return true;
		}

		// Token: 0x1700012A RID: 298
		// (get) Token: 0x0600091D RID: 2333 RVA: 0x0002DE0E File Offset: 0x0002C00E
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0600091E RID: 2334 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x1700012B RID: 299
		// (get) Token: 0x0600091F RID: 2335 RVA: 0x0002DE0E File Offset: 0x0002C00E
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0400040E RID: 1038
		private int <>1__state;

		// Token: 0x0400040F RID: 1039
		private object <>2__current;

		// Token: 0x04000410 RID: 1040
		public Action<float> callback;

		// Token: 0x04000411 RID: 1041
		public float startValue;

		// Token: 0x04000412 RID: 1042
		public EasingFunction.Ease type;

		// Token: 0x04000413 RID: 1043
		public bool unscaled;

		// Token: 0x04000414 RID: 1044
		public float endValue;

		// Token: 0x04000415 RID: 1045
		public float time;

		// Token: 0x04000416 RID: 1046
		public Action callback2;

		// Token: 0x04000417 RID: 1047
		private EasingFunction.Function <easingFunction>5__2;

		// Token: 0x04000418 RID: 1048
		private float <t>5__3;
	}

	// Token: 0x02000112 RID: 274
	[CompilerGenerated]
	private sealed class <MoveEasingVector3>d__17 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000920 RID: 2336 RVA: 0x0002DE16 File Offset: 0x0002C016
		[DebuggerHidden]
		public <MoveEasingVector3>d__17(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000921 RID: 2337 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000922 RID: 2338 RVA: 0x0002DE28 File Offset: 0x0002C028
		bool IEnumerator.MoveNext()
		{
			switch (this.<>1__state)
			{
			case 0:
				this.<>1__state = -1;
				callback(startValue);
				easingFunction = EasingFunction.GetEasingFunction(type);
				t = 0f;
				break;
			case 1:
			{
				this.<>1__state = -1;
				if (unscaled)
				{
					t += Time.unscaledDeltaTime * Settings.GAME_SPEED;
				}
				else
				{
					t += Time.deltaTime * Settings.GAME_SPEED;
				}
				Vector3 obj = new Vector3(easingFunction(startValue.x, endValue.x, t / time), easingFunction(startValue.y, endValue.y, t / time), easingFunction(startValue.z, endValue.z, t / time));
				callback(obj);
				break;
			}
			case 2:
				this.<>1__state = -1;
				return false;
			default:
				return false;
			}
			if (t > time)
			{
				callback(endValue);
				if (callback2 != null)
				{
					callback2();
				}
				this.<>2__current = new WaitForEndOfFrame();
				this.<>1__state = 2;
				return true;
			}
			this.<>2__current = new WaitForEndOfFrame();
			this.<>1__state = 1;
			return true;
		}

		// Token: 0x1700012C RID: 300
		// (get) Token: 0x06000923 RID: 2339 RVA: 0x0002DFCC File Offset: 0x0002C1CC
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000924 RID: 2340 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x1700012D RID: 301
		// (get) Token: 0x06000925 RID: 2341 RVA: 0x0002DFCC File Offset: 0x0002C1CC
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x04000419 RID: 1049
		private int <>1__state;

		// Token: 0x0400041A RID: 1050
		private object <>2__current;

		// Token: 0x0400041B RID: 1051
		public Action<Vector3> callback;

		// Token: 0x0400041C RID: 1052
		public Vector3 startValue;

		// Token: 0x0400041D RID: 1053
		public EasingFunction.Ease type;

		// Token: 0x0400041E RID: 1054
		public bool unscaled;

		// Token: 0x0400041F RID: 1055
		public Vector3 endValue;

		// Token: 0x04000420 RID: 1056
		public float time;

		// Token: 0x04000421 RID: 1057
		public Action callback2;

		// Token: 0x04000422 RID: 1058
		private EasingFunction.Function <easingFunction>5__2;

		// Token: 0x04000423 RID: 1059
		private float <t>5__3;
	}

	// Token: 0x02000113 RID: 275
	[CompilerGenerated]
	private sealed class <DelayAction>d__19 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000926 RID: 2342 RVA: 0x0002DFD4 File Offset: 0x0002C1D4
		[DebuggerHidden]
		public <DelayAction>d__19(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000927 RID: 2343 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000928 RID: 2344 RVA: 0x0002DFE4 File Offset: 0x0002C1E4
		bool IEnumerator.MoveNext()
		{
			switch (this.<>1__state)
			{
			case 0:
				this.<>1__state = -1;
				if (unscaled)
				{
					this.<>2__current = AppUtil.WaitRealtime(waitTime);
					this.<>1__state = 1;
					return true;
				}
				this.<>2__current = AppUtil.Wait(waitTime);
				this.<>1__state = 2;
				return true;
			case 1:
				this.<>1__state = -1;
				break;
			case 2:
				this.<>1__state = -1;
				break;
			default:
				return false;
			}
			waitAction();
			return false;
		}

		// Token: 0x1700012E RID: 302
		// (get) Token: 0x06000929 RID: 2345 RVA: 0x0002E06B File Offset: 0x0002C26B
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0600092A RID: 2346 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x1700012F RID: 303
		// (get) Token: 0x0600092B RID: 2347 RVA: 0x0002E06B File Offset: 0x0002C26B
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x04000424 RID: 1060
		private int <>1__state;

		// Token: 0x04000425 RID: 1061
		private object <>2__current;

		// Token: 0x04000426 RID: 1062
		public bool unscaled;

		// Token: 0x04000427 RID: 1063
		public float waitTime;

		// Token: 0x04000428 RID: 1064
		public Action waitAction;
	}

	// Token: 0x02000114 RID: 276
	[CompilerGenerated]
	private sealed class <DelayAction>d__21 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x0600092C RID: 2348 RVA: 0x0002E073 File Offset: 0x0002C273
		[DebuggerHidden]
		public <DelayAction>d__21(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x0600092D RID: 2349 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x0600092E RID: 2350 RVA: 0x0002E084 File Offset: 0x0002C284
		bool IEnumerator.MoveNext()
		{
			switch (this.<>1__state)
			{
			case 0:
				this.<>1__state = -1;
				if (unscaled)
				{
					this.<>2__current = AppUtil.WaitRealtime(waitTime);
					this.<>1__state = 1;
					return true;
				}
				this.<>2__current = AppUtil.Wait(waitTime);
				this.<>1__state = 2;
				return true;
			case 1:
				this.<>1__state = -1;
				break;
			case 2:
				this.<>1__state = -1;
				break;
			case 3:
				this.<>1__state = -1;
				return false;
			default:
				return false;
			}
			this.<>2__current = waitAction;
			this.<>1__state = 3;
			return true;
		}

		// Token: 0x17000130 RID: 304
		// (get) Token: 0x0600092F RID: 2351 RVA: 0x0002E120 File Offset: 0x0002C320
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000930 RID: 2352 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000131 RID: 305
		// (get) Token: 0x06000931 RID: 2353 RVA: 0x0002E120 File Offset: 0x0002C320
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x04000429 RID: 1065
		private int <>1__state;

		// Token: 0x0400042A RID: 1066
		private object <>2__current;

		// Token: 0x0400042B RID: 1067
		public bool unscaled;

		// Token: 0x0400042C RID: 1068
		public float waitTime;

		// Token: 0x0400042D RID: 1069
		public IEnumerator waitAction;
	}

	// Token: 0x02000115 RID: 277
	[CompilerGenerated]
	[Serializable]
	private sealed class <>c
	{
		// Token: 0x06000932 RID: 2354 RVA: 0x0002E128 File Offset: 0x0002C328
		// Note: this type is marked as 'beforefieldinit'.
		static <>c()
		{
		}

		// Token: 0x06000933 RID: 2355 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c()
		{
		}

		// Token: 0x06000934 RID: 2356 RVA: 0x0002E134 File Offset: 0x0002C334
		internal int <ReverseByValue>b__29_0(KeyValuePair<int, int> kvp1, KeyValuePair<int, int> kvp2)
		{
			if (kvp2.Value == kvp1.Value)
			{
				return kvp1.Key - kvp2.Key;
			}
			return kvp2.Value - kvp1.Value;
		}

		// Token: 0x06000935 RID: 2357 RVA: 0x0002E165 File Offset: 0x0002C365
		internal int <ReverseByValueS>b__30_0(KeyValuePair<string, int> kvp1, KeyValuePair<string, int> kvp2)
		{
			return kvp2.Value - kvp1.Value;
		}

		// Token: 0x06000936 RID: 2358 RVA: 0x0002E176 File Offset: 0x0002C376
		internal string <ToString>b__34_0(int x)
		{
			return x.ToString();
		}

		// Token: 0x06000937 RID: 2359 RVA: 0x00027665 File Offset: 0x00025865
		internal Guid <RandomArray>b__41_0(int tmp)
		{
			return Guid.NewGuid();
		}

		// Token: 0x06000938 RID: 2360 RVA: 0x00027665 File Offset: 0x00025865
		internal Guid <RandomArray>b__42_0(int tmp)
		{
			return Guid.NewGuid();
		}

		// Token: 0x0400042E RID: 1070
		public static readonly AppUtil.<>c <>9 = new AppUtil.<>c();

		// Token: 0x0400042F RID: 1071
		public static Comparison<KeyValuePair<int, int>> <>9__29_0;

		// Token: 0x04000430 RID: 1072
		public static Comparison<KeyValuePair<string, int>> <>9__30_0;

		// Token: 0x04000431 RID: 1073
		public static Func<int, string> <>9__34_0;

		// Token: 0x04000432 RID: 1074
		public static Func<int, Guid> <>9__41_0;

		// Token: 0x04000433 RID: 1075
		public static Func<int, Guid> <>9__42_0;
	}

	// Token: 0x02000116 RID: 278
	[CompilerGenerated]
	private sealed class <WaitAnimation>d__38 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000939 RID: 2361 RVA: 0x0002E17F File Offset: 0x0002C37F
		[DebuggerHidden]
		public <WaitAnimation>d__38(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x0600093A RID: 2362 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x0600093B RID: 2363 RVA: 0x0002E190 File Offset: 0x0002C390
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			if (num != 0)
			{
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				return false;
			}
			else
			{
				this.<>1__state = -1;
				if (animObject == null)
				{
					return false;
				}
				this.<>2__current = AppUtil.WaitAnimation(animObject.GetComponent<Animator>(), callback);
				this.<>1__state = 1;
				return true;
			}
		}

		// Token: 0x17000132 RID: 306
		// (get) Token: 0x0600093C RID: 2364 RVA: 0x0002E1F1 File Offset: 0x0002C3F1
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0600093D RID: 2365 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000133 RID: 307
		// (get) Token: 0x0600093E RID: 2366 RVA: 0x0002E1F1 File Offset: 0x0002C3F1
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x04000434 RID: 1076
		private int <>1__state;

		// Token: 0x04000435 RID: 1077
		private object <>2__current;

		// Token: 0x04000436 RID: 1078
		public GameObject animObject;

		// Token: 0x04000437 RID: 1079
		public Action callback;
	}

	// Token: 0x02000117 RID: 279
	[CompilerGenerated]
	private sealed class <WaitAnimation>d__39 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x0600093F RID: 2367 RVA: 0x0002E1F9 File Offset: 0x0002C3F9
		[DebuggerHidden]
		public <WaitAnimation>d__39(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000940 RID: 2368 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000941 RID: 2369 RVA: 0x0002E208 File Offset: 0x0002C408
		bool IEnumerator.MoveNext()
		{
			switch (this.<>1__state)
			{
			case 0:
				this.<>1__state = -1;
				if (anim == null)
				{
					return false;
				}
				if (anim.updateMode == AnimatorUpdateMode.UnscaledTime)
				{
					this.<>2__current = AppUtil.WaitRealtime(anim.GetCurrentAnimatorClipInfo(0)[0].clip.length);
					this.<>1__state = 1;
					return true;
				}
				this.<>2__current = AppUtil.Wait(anim.GetCurrentAnimatorClipInfo(0)[0].clip.length);
				this.<>1__state = 2;
				return true;
			case 1:
				this.<>1__state = -1;
				break;
			case 2:
				this.<>1__state = -1;
				break;
			default:
				return false;
			}
			if (callback != null)
			{
				callback();
			}
			return false;
		}

		// Token: 0x17000134 RID: 308
		// (get) Token: 0x06000942 RID: 2370 RVA: 0x0002E2D9 File Offset: 0x0002C4D9
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000943 RID: 2371 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000135 RID: 309
		// (get) Token: 0x06000944 RID: 2372 RVA: 0x0002E2D9 File Offset: 0x0002C4D9
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x04000438 RID: 1080
		private int <>1__state;

		// Token: 0x04000439 RID: 1081
		private object <>2__current;

		// Token: 0x0400043A RID: 1082
		public Animator anim;

		// Token: 0x0400043B RID: 1083
		public Action callback;
	}

	// Token: 0x02000118 RID: 280
	[CompilerGenerated]
	private sealed class <RectTransformCapture>d__43 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000945 RID: 2373 RVA: 0x0002E2E1 File Offset: 0x0002C4E1
		[DebuggerHidden]
		public <RectTransformCapture>d__43(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000946 RID: 2374 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000947 RID: 2375 RVA: 0x0002E2F0 File Offset: 0x0002C4F0
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			if (num == 0)
			{
				this.<>1__state = -1;
				this.<>2__current = new WaitForEndOfFrame();
				this.<>1__state = 1;
				return true;
			}
			if (num != 1)
			{
				return false;
			}
			this.<>1__state = -1;
			string path = "Externals/Log/Temp/" + "Screenshot" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".png";
			Vector3[] array = new Vector3[4];
			captureTarget.GetWorldCorners(array);
			Rect source = new Rect(array[0].x, array[0].y, array[2].x - array[0].x, array[2].y - array[0].y);
			if (captureTarget.GetComponentInParent<Canvas>().renderMode == RenderMode.ScreenSpaceCamera)
			{
				Vector2 vector = Camera.main.WorldToScreenPoint(array[0]);
				Vector2 vector2 = Camera.main.WorldToScreenPoint(array[2]);
				source = new Rect(vector.x, vector.y, vector2.x - vector.x, vector2.y - vector.y);
			}
			Texture2D texture2D = new Texture2D((int)source.width, (int)source.height);
			texture2D.ReadPixels(source, 0, 0);
			texture2D.Apply();
			byte[] bytes = texture2D.EncodeToPNG();
			File.WriteAllBytes(path, bytes);
			return false;
		}

		// Token: 0x17000136 RID: 310
		// (get) Token: 0x06000948 RID: 2376 RVA: 0x0002E461 File Offset: 0x0002C661
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000949 RID: 2377 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000137 RID: 311
		// (get) Token: 0x0600094A RID: 2378 RVA: 0x0002E461 File Offset: 0x0002C661
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0400043C RID: 1084
		private int <>1__state;

		// Token: 0x0400043D RID: 1085
		private object <>2__current;

		// Token: 0x0400043E RID: 1086
		public RectTransform captureTarget;
	}

	// Token: 0x02000119 RID: 281
	[CompilerGenerated]
	private sealed class <Share>d__45 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x0600094B RID: 2379 RVA: 0x0002E469 File Offset: 0x0002C669
		[DebuggerHidden]
		public <Share>d__45(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x0600094C RID: 2380 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x0600094D RID: 2381 RVA: 0x0002E478 File Offset: 0x0002C678
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			if (num == 0)
			{
				this.<>1__state = -1;
				this.<>2__current = new WaitForEndOfFrame();
				this.<>1__state = 1;
				return true;
			}
			if (num != 1)
			{
				return false;
			}
			this.<>1__state = -1;
			Rect source = new Rect(0f, 0f, (float)Screen.width, (float)Screen.height);
			if (captureTarget != null)
			{
				Vector3[] array = new Vector3[4];
				captureTarget.GetWorldCorners(array);
				source = new Rect(array[0].x, array[0].y, array[2].x - array[0].x, array[2].y - array[0].y);
				if (captureTarget.GetComponentInParent<Canvas>().renderMode == RenderMode.ScreenSpaceCamera)
				{
					Vector2 vector = Camera.main.WorldToScreenPoint(array[0]);
					Vector2 vector2 = Camera.main.WorldToScreenPoint(array[2]);
					source = new Rect(vector.x, vector.y, vector2.x - vector.x, vector2.y - vector.y);
					if (source.x < 0f)
					{
						source.width += source.x;
						source.x = 0f;
					}
					if (source.width + source.x > (float)Screen.width)
					{
						source.width = (float)Screen.width - source.x;
					}
					if (source.y < 0f)
					{
						source.height += source.y;
						source.y = 0f;
					}
					if (source.height + source.y > (float)Screen.height)
					{
						source.height = (float)Screen.height - source.y;
					}
				}
			}
			Texture2D texture2D = new Texture2D((int)source.width, (int)source.height);
			texture2D.ReadPixels(source, 0, 0);
			texture2D.Apply();
			byte[] bytes = texture2D.EncodeToJPG();
			string text = Application.persistentDataPath + "/shareimage.jpg";
			File.WriteAllBytes(text, bytes);
			if (showDialog)
			{
				DialogManager.ShowDialog("ShareDialog", new object[]
				{
					msg,
					url,
					texture2D,
					text
				});
			}
			else
			{
				SocialConnector.Share(msg, url, text);
			}
			return false;
		}

		// Token: 0x17000138 RID: 312
		// (get) Token: 0x0600094E RID: 2382 RVA: 0x0002E710 File Offset: 0x0002C910
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0600094F RID: 2383 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000139 RID: 313
		// (get) Token: 0x06000950 RID: 2384 RVA: 0x0002E710 File Offset: 0x0002C910
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0400043F RID: 1087
		private int <>1__state;

		// Token: 0x04000440 RID: 1088
		private object <>2__current;

		// Token: 0x04000441 RID: 1089
		public RectTransform captureTarget;

		// Token: 0x04000442 RID: 1090
		public bool showDialog;

		// Token: 0x04000443 RID: 1091
		public string msg;

		// Token: 0x04000444 RID: 1092
		public string url;
	}
}
