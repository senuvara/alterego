﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using App;
using UnityEngine;
using UnityEngine.SceneManagement;

// Token: 0x02000029 RID: 41
public class AudioManager : MonoBehaviour
{
	// Token: 0x06000188 RID: 392 RVA: 0x0000F4AC File Offset: 0x0000D6AC
	private void Awake()
	{
		AudioManager.Instance = this;
		foreach (string text in Settings.AUDIO_LIST.Values)
		{
			if (!(text == ""))
			{
				AudioSource audioSource = base.gameObject.AddComponent<AudioSource>();
				audioSource.loop = true;
				audioSource.clip = Resources.Load<AudioClip>(text);
				this.AudioList.Add(audioSource);
			}
		}
		this.AudioSE = base.gameObject.AddComponent<AudioSource>();
		this.AudioSE.loop = false;
		base.StartCoroutine(this.PlayBGM(Settings.AUDIO_LIST["Default"]));
		SceneManager.sceneLoaded += this.OnSceneLoaded;
		AudioManager.ChangeSettings();
	}

	// Token: 0x06000189 RID: 393 RVA: 0x0000F584 File Offset: 0x0000D784
	private void OnDestroy()
	{
		SceneManager.sceneLoaded -= this.OnSceneLoaded;
	}

	// Token: 0x0600018A RID: 394 RVA: 0x0000F598 File Offset: 0x0000D798
	public static void PlaySound(params string[] eventList)
	{
		if (AudioManager.Instance == null)
		{
			return;
		}
		string soundName = Utility.GetSoundName(eventList);
		if (soundName == null || soundName == "")
		{
			return;
		}
		AudioClip audioClip = null;
		foreach (AudioClip audioClip2 in AudioManager.Instance.ClipList)
		{
			if (audioClip2.name == soundName)
			{
				audioClip = audioClip2;
				break;
			}
		}
		if (audioClip == null)
		{
			audioClip = Resources.Load<AudioClip>("SE/" + soundName);
			if (audioClip == null)
			{
				global::Debug.LogWarning("音声ファイル「" + soundName + "」は存在しません");
				return;
			}
			AudioManager.Instance.ClipList.Add(audioClip);
		}
		AudioManager.Instance.AudioSE.PlayOneShot(audioClip);
	}

	// Token: 0x0600018B RID: 395 RVA: 0x0000F67C File Offset: 0x0000D87C
	public static void ChangeSettings()
	{
		foreach (AudioSource audioSource in AudioManager.Instance.AudioList)
		{
			audioSource.mute = !Settings.BGM;
		}
		AudioManager.Instance.AudioSE.mute = !Settings.SE;
	}

	// Token: 0x0600018C RID: 396 RVA: 0x0000F6F0 File Offset: 0x0000D8F0
	public void OnSceneLoaded(Scene scene, LoadSceneMode SceneMode)
	{
		string key = "Default";
		if (Settings.AUDIO_LIST.ContainsKey(scene.name))
		{
			key = scene.name;
		}
		base.StartCoroutine(this.PlayBGM(Settings.AUDIO_LIST[key]));
	}

	// Token: 0x0600018D RID: 397 RVA: 0x0000F738 File Offset: 0x0000D938
	public void OnPreSceneLoaded(string scenename)
	{
		string key = "Default";
		if (Settings.AUDIO_LIST.ContainsKey(scenename))
		{
			key = scenename;
		}
		this.StopBGM(Settings.AUDIO_LIST[key]);
	}

	// Token: 0x0600018E RID: 398 RVA: 0x0000F76C File Offset: 0x0000D96C
	public static void ChangeBGM(string bgm)
	{
		AudioManager.Instance.StartCoroutine(AudioManager.Instance.PlayBGM(bgm));
	}

	// Token: 0x0600018F RID: 399 RVA: 0x0000F784 File Offset: 0x0000D984
	private bool StopBGM(string nextbgm)
	{
		if (this.CurrentBGM != null && this.CurrentBGM == nextbgm)
		{
			return false;
		}
		using (List<AudioSource>.Enumerator enumerator = this.AudioList.GetEnumerator())
		{
			while (enumerator.MoveNext())
			{
				AudioSource audio = enumerator.Current;
				if (this.CurrentBGM == audio.clip.name)
				{
					base.StartCoroutine(AppUtil.MoveEasingFloat(1f, 0f, delegate(float tmp)
					{
						audio.volume = tmp;
					}, true, 3f, EasingFunction.Ease.EaseOutQuint, null));
					this.CurrentBGM = null;
					return true;
				}
			}
		}
		return false;
	}

	// Token: 0x06000190 RID: 400 RVA: 0x0000F84C File Offset: 0x0000DA4C
	private IEnumerator PlayBGM(string bgm)
	{
		if (this.CurrentBGM != null && this.CurrentBGM == bgm)
		{
			yield break;
		}
		if (bgm == "")
		{
			this.StopBGM(bgm);
			yield break;
		}
		if (this.StopBGM(bgm))
		{
			yield return AppUtil.WaitRealtime(1f);
		}
		this.CurrentBGM = bgm;
		yield return AppUtil.WaitRealtime(1f);
		using (List<AudioSource>.Enumerator enumerator = this.AudioList.GetEnumerator())
		{
			while (enumerator.MoveNext())
			{
				AudioSource audioSource = enumerator.Current;
				if (audioSource.clip.name == bgm)
				{
					audioSource.Play();
					audioSource.volume = 1f;
				}
				else
				{
					audioSource.Stop();
				}
			}
			yield break;
		}
		yield break;
	}

	// Token: 0x06000191 RID: 401 RVA: 0x0000F862 File Offset: 0x0000DA62
	public AudioManager()
	{
	}

	// Token: 0x0400009B RID: 155
	private static AudioManager Instance;

	// Token: 0x0400009C RID: 156
	private string CurrentBGM;

	// Token: 0x0400009D RID: 157
	private List<AudioSource> AudioList = new List<AudioSource>();

	// Token: 0x0400009E RID: 158
	private AudioSource AudioSE;

	// Token: 0x0400009F RID: 159
	private List<AudioClip> ClipList = new List<AudioClip>();

	// Token: 0x0200011A RID: 282
	[CompilerGenerated]
	private sealed class <>c__DisplayClass12_0
	{
		// Token: 0x06000951 RID: 2385 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass12_0()
		{
		}

		// Token: 0x06000952 RID: 2386 RVA: 0x0002E718 File Offset: 0x0002C918
		internal void <StopBGM>b__0(float tmp)
		{
			this.audio.volume = tmp;
		}

		// Token: 0x04000445 RID: 1093
		public AudioSource audio;
	}

	// Token: 0x0200011B RID: 283
	[CompilerGenerated]
	private sealed class <PlayBGM>d__13 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000953 RID: 2387 RVA: 0x0002E726 File Offset: 0x0002C926
		[DebuggerHidden]
		public <PlayBGM>d__13(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000954 RID: 2388 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000955 RID: 2389 RVA: 0x0002E738 File Offset: 0x0002C938
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			AudioManager audioManager = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				if (audioManager.CurrentBGM != null && audioManager.CurrentBGM == bgm)
				{
					return false;
				}
				if (bgm == "")
				{
					audioManager.StopBGM(bgm);
					return false;
				}
				if (audioManager.StopBGM(bgm))
				{
					this.<>2__current = AppUtil.WaitRealtime(1f);
					this.<>1__state = 1;
					return true;
				}
				break;
			case 1:
				this.<>1__state = -1;
				break;
			case 2:
				this.<>1__state = -1;
				foreach (AudioSource audioSource in audioManager.AudioList)
				{
					if (audioSource.clip.name == bgm)
					{
						audioSource.Play();
						audioSource.volume = 1f;
					}
					else
					{
						audioSource.Stop();
					}
				}
				return false;
			default:
				return false;
			}
			audioManager.CurrentBGM = bgm;
			this.<>2__current = AppUtil.WaitRealtime(1f);
			this.<>1__state = 2;
			return true;
		}

		// Token: 0x1700013A RID: 314
		// (get) Token: 0x06000956 RID: 2390 RVA: 0x0002E878 File Offset: 0x0002CA78
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000957 RID: 2391 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x1700013B RID: 315
		// (get) Token: 0x06000958 RID: 2392 RVA: 0x0002E878 File Offset: 0x0002CA78
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x04000446 RID: 1094
		private int <>1__state;

		// Token: 0x04000447 RID: 1095
		private object <>2__current;

		// Token: 0x04000448 RID: 1096
		public AudioManager <>4__this;

		// Token: 0x04000449 RID: 1097
		public string bgm;
	}
}
