﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

// Token: 0x02000032 RID: 50
public class OnLongClickHandler : MonoBehaviour, IPointerDownHandler, IEventSystemHandler, IPointerUpHandler
{
	// Token: 0x06000214 RID: 532 RVA: 0x00011F53 File Offset: 0x00010153
	public void OnPointerDown(PointerEventData eventData)
	{
		base.StartCoroutine("HandleLongClick");
	}

	// Token: 0x06000215 RID: 533 RVA: 0x00011F61 File Offset: 0x00010161
	public void OnPointerUp(PointerEventData eventData)
	{
		base.StopCoroutine("HandleLongClick");
	}

	// Token: 0x06000216 RID: 534 RVA: 0x00011F6E File Offset: 0x0001016E
	private IEnumerator HandleLongClick()
	{
		float totalTime = 0f;
		while (totalTime < 1f)
		{
			totalTime += Time.unscaledDeltaTime;
			yield return null;
		}
		base.gameObject.SendMessageUpwards("OnLongClick", base.gameObject.name, SendMessageOptions.DontRequireReceiver);
		yield break;
	}

	// Token: 0x06000217 RID: 535 RVA: 0x000025AD File Offset: 0x000007AD
	public OnLongClickHandler()
	{
	}

	// Token: 0x0200012C RID: 300
	[CompilerGenerated]
	private sealed class <HandleLongClick>d__2 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000994 RID: 2452 RVA: 0x0002F021 File Offset: 0x0002D221
		[DebuggerHidden]
		public <HandleLongClick>d__2(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000995 RID: 2453 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000996 RID: 2454 RVA: 0x0002F030 File Offset: 0x0002D230
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			OnLongClickHandler onLongClickHandler = this;
			if (num != 0)
			{
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
			}
			else
			{
				this.<>1__state = -1;
				totalTime = 0f;
			}
			if (totalTime >= 1f)
			{
				onLongClickHandler.gameObject.SendMessageUpwards("OnLongClick", onLongClickHandler.gameObject.name, SendMessageOptions.DontRequireReceiver);
				return false;
			}
			totalTime += Time.unscaledDeltaTime;
			this.<>2__current = null;
			this.<>1__state = 1;
			return true;
		}

		// Token: 0x1700014A RID: 330
		// (get) Token: 0x06000997 RID: 2455 RVA: 0x0002F0BB File Offset: 0x0002D2BB
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000998 RID: 2456 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x1700014B RID: 331
		// (get) Token: 0x06000999 RID: 2457 RVA: 0x0002F0BB File Offset: 0x0002D2BB
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040004A5 RID: 1189
		private int <>1__state;

		// Token: 0x040004A6 RID: 1190
		private object <>2__current;

		// Token: 0x040004A7 RID: 1191
		public OnLongClickHandler <>4__this;

		// Token: 0x040004A8 RID: 1192
		private float <totalTime>5__2;
	}
}
