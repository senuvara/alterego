﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x0200000D RID: 13
public class EgoPoint : ICloneable
{
	// Token: 0x06000050 RID: 80 RVA: 0x000072FE File Offset: 0x000054FE
	public EgoPoint()
	{
	}

	// Token: 0x06000051 RID: 81 RVA: 0x00007321 File Offset: 0x00005521
	public EgoPoint(float pointFloat)
	{
		this.SetFloat(pointFloat, 1);
		this.FormatEgoPoint();
	}

	// Token: 0x06000052 RID: 82 RVA: 0x00007354 File Offset: 0x00005554
	public EgoPoint(string pointInString)
	{
		if (pointInString == "")
		{
			return;
		}
		char c = pointInString[pointInString.Length - 1];
		int num = 0;
		if (c >= 'a' && c <= 'z')
		{
			c = Convert.ToChar((int)('A' + c - 'a'));
		}
		if (c >= 'A')
		{
			pointInString = pointInString.Substring(0, pointInString.Length - 1);
			num += (int)(c - 'A' + '\u0001');
			for (int i = 0; i < num; i++)
			{
				this.Point.Add(0L);
			}
		}
		this.SetFloat(float.Parse(pointInString), num + 1);
	}

	// Token: 0x06000053 RID: 83 RVA: 0x00007400 File Offset: 0x00005600
	private void SetFloat(float value, int digit)
	{
		string[] array = value.ToString("0.000").Split(new char[]
		{
			'.'
		});
		this.Point[digit] = long.Parse(array[0]);
		if (digit > 0 && array.Length > 1)
		{
			List<long> point = this.Point;
			int index = digit - 1;
			point[index] += long.Parse(array[1]);
		}
	}

	// Token: 0x06000054 RID: 84 RVA: 0x0000746A File Offset: 0x0000566A
	public override string ToString()
	{
		return this.ToString("0.00");
	}

	// Token: 0x06000055 RID: 85 RVA: 0x00007477 File Offset: 0x00005677
	public bool IsZero()
	{
		return this.ToString() == "0.00";
	}

	// Token: 0x06000056 RID: 86 RVA: 0x0000748C File Offset: 0x0000568C
	public void debug()
	{
		string text = "";
		for (int i = this.Point.Count - 1; i >= 0; i--)
		{
			text = text + this.Point[i] + ", ";
		}
		global::Debug.Log("this.Point=" + text);
	}

	// Token: 0x06000057 RID: 87 RVA: 0x000074E4 File Offset: 0x000056E4
	private void FormatEgoPoint()
	{
		for (int i = 0; i < this.Point.Count; i++)
		{
			long num = (long)((float)this.Point[i] / 1000f);
			if (num > 0L)
			{
				List<long> point = this.Point;
				int index = i;
				point[index] -= num * 1000L;
				if (i == this.Point.Count - 1)
				{
					this.Point.Add(num);
				}
				else
				{
					point = this.Point;
					index = i + 1;
					point[index] += num;
				}
			}
			if (i < this.Point.Count - 1)
			{
				while (this.Point[i] < 0L)
				{
					List<long> point2 = this.Point;
					int index = i + 1;
					long num2 = point2[index];
					point2[index] = num2 - 1L;
					List<long> point = this.Point;
					index = i;
					point[index] += 1000L;
				}
			}
		}
		int num3 = this.Point.Count - 1;
		while (num3 > 1 && this.Point[num3] <= 0L)
		{
			this.Point.RemoveAt(num3);
			num3--;
		}
		if (this >= new EgoPoint("1["))
		{
			this.Point = new EgoPoint("999.99Z").Point;
		}
	}

	// Token: 0x06000058 RID: 88 RVA: 0x00007640 File Offset: 0x00005840
	public string ToString(string format)
	{
		int num = this.Point.Count - 1;
		float num2 = (float)this.Point[num] + (float)this.Point[num - 1] / 1000f;
		if (num2 == 0f)
		{
			return num2.ToString(format);
		}
		if (format.Contains("0.000"))
		{
			format = format.Replace("0.000", "0.000" + this.GetUnit(num));
			num2 -= 0.0005f;
		}
		else if (format.Contains("0.00"))
		{
			format = format.Replace("0.00", "0.00" + this.GetUnit(num));
			num2 -= 0.005f;
		}
		else if (format.Contains("0.0"))
		{
			format = format.Replace("0.0", "0.0" + this.GetUnit(num));
			num2 -= 0.05f;
		}
		else
		{
			format = format.Replace("0", "0" + this.GetUnit(num));
			num2 -= 0.5f;
		}
		return num2.ToString(format);
	}

	// Token: 0x06000059 RID: 89 RVA: 0x00007760 File Offset: 0x00005960
	public int GetDigit()
	{
		return this.Point.Count - 1;
	}

	// Token: 0x0600005A RID: 90 RVA: 0x00007770 File Offset: 0x00005970
	private string GetUnit(int index)
	{
		if (index < 2)
		{
			return "";
		}
		index -= 2;
		return " " + Convert.ToChar(65 + index).ToString();
	}

	// Token: 0x0600005B RID: 91 RVA: 0x000077A7 File Offset: 0x000059A7
	public object Clone()
	{
		return new EgoPoint
		{
			Point = new List<long>(this.Point)
		};
	}

	// Token: 0x0600005C RID: 92 RVA: 0x000077C0 File Offset: 0x000059C0
	public static EgoPoint operator +(EgoPoint org, EgoPoint value)
	{
		EgoPoint egoPoint = (EgoPoint)org.Clone();
		for (int i = egoPoint.Point.Count; i < value.Point.Count; i++)
		{
			egoPoint.Point.Add(0L);
		}
		for (int j = 0; j < value.Point.Count; j++)
		{
			List<long> point = egoPoint.Point;
			int index = j;
			point[index] += value.Point[j];
		}
		egoPoint.FormatEgoPoint();
		return egoPoint;
	}

	// Token: 0x0600005D RID: 93 RVA: 0x0000784C File Offset: 0x00005A4C
	public static EgoPoint operator *(EgoPoint org, float value)
	{
		EgoPoint egoPoint = (EgoPoint)org.Clone();
		for (int i = 0; i < egoPoint.Point.Count; i++)
		{
			egoPoint.SetFloat((float)org.Point[i] * value, i);
		}
		egoPoint.FormatEgoPoint();
		return egoPoint;
	}

	// Token: 0x0600005E RID: 94 RVA: 0x00007898 File Offset: 0x00005A98
	public static EgoPoint operator /(EgoPoint org, float value)
	{
		EgoPoint egoPoint = (EgoPoint)org.Clone();
		for (int i = 0; i < egoPoint.Point.Count; i++)
		{
			egoPoint.SetFloat((float)org.Point[i] / value, i);
		}
		egoPoint.FormatEgoPoint();
		return egoPoint;
	}

	// Token: 0x0600005F RID: 95 RVA: 0x000078E4 File Offset: 0x00005AE4
	public static EgoPoint operator /(EgoPoint org, long value)
	{
		EgoPoint egoPoint = (EgoPoint)org.Clone();
		while (value > 1000L)
		{
			value /= 1000L;
			egoPoint /= 1000L;
		}
		egoPoint /= (float)value;
		egoPoint.FormatEgoPoint();
		return egoPoint;
	}

	// Token: 0x06000060 RID: 96 RVA: 0x00007930 File Offset: 0x00005B30
	public static float operator /(EgoPoint org, EgoPoint value)
	{
		float result = 1f;
		EgoPoint org2 = value / 100f;
		for (int i = 0; i < 100; i++)
		{
			if (org2 * (float)i <= org && org < org2 * (float)(i + 1))
			{
				result = 0.01f * (float)i;
				break;
			}
		}
		return result;
	}

	// Token: 0x06000061 RID: 97 RVA: 0x0000798C File Offset: 0x00005B8C
	public static EgoPoint operator -(EgoPoint org, EgoPoint value)
	{
		if (org <= value)
		{
			return new EgoPoint(0f);
		}
		EgoPoint egoPoint = (EgoPoint)org.Clone();
		int num = Mathf.Min(org.Point.Count, value.Point.Count);
		for (int i = 0; i < num; i++)
		{
			List<long> point = egoPoint.Point;
			int index = i;
			point[index] -= value.Point[i];
		}
		egoPoint.FormatEgoPoint();
		return egoPoint;
	}

	// Token: 0x06000062 RID: 98 RVA: 0x00007A0F File Offset: 0x00005C0F
	public static bool operator >=(EgoPoint a, EgoPoint b)
	{
		return EgoPoint.IsBigger(a, b, true);
	}

	// Token: 0x06000063 RID: 99 RVA: 0x00007A19 File Offset: 0x00005C19
	public static bool operator <=(EgoPoint a, EgoPoint b)
	{
		return EgoPoint.IsBigger(b, a, true);
	}

	// Token: 0x06000064 RID: 100 RVA: 0x00007A23 File Offset: 0x00005C23
	public static bool operator >(EgoPoint a, EgoPoint b)
	{
		return EgoPoint.IsBigger(a, b, false);
	}

	// Token: 0x06000065 RID: 101 RVA: 0x00007A2D File Offset: 0x00005C2D
	public static bool operator <(EgoPoint a, EgoPoint b)
	{
		return EgoPoint.IsBigger(b, a, false);
	}

	// Token: 0x06000066 RID: 102 RVA: 0x00007A38 File Offset: 0x00005C38
	public static bool IsBigger(EgoPoint a, EgoPoint b, bool equal)
	{
		if (a == null)
		{
			a = new EgoPoint();
		}
		if (b == null)
		{
			b = new EgoPoint();
		}
		int count = a.Point.Count;
		int count2 = b.Point.Count;
		if (count > count2)
		{
			return true;
		}
		if (count < count2)
		{
			return false;
		}
		for (int i = a.Point.Count - 1; i >= 0; i--)
		{
			if (a.Point[i] > b.Point[i])
			{
				return true;
			}
			if (a.Point[i] < b.Point[i])
			{
				return false;
			}
		}
		return equal;
	}

	// Token: 0x06000067 RID: 103 RVA: 0x00007ACE File Offset: 0x00005CCE
	public static EgoPoint CreateInstance(string value)
	{
		return new EgoPoint(value);
	}

	// Token: 0x04000018 RID: 24
	public List<long> Point = new List<long>
	{
		0L,
		0L
	};
}
