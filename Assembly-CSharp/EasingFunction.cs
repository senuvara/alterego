﻿using System;
using UnityEngine;

// Token: 0x0200002E RID: 46
public class EasingFunction
{
	// Token: 0x060001AA RID: 426 RVA: 0x0000FFB0 File Offset: 0x0000E1B0
	public static float Linear(float start, float end, float value)
	{
		return Mathf.Lerp(start, end, value);
	}

	// Token: 0x060001AB RID: 427 RVA: 0x0000FFBC File Offset: 0x0000E1BC
	public static float Spring(float start, float end, float value)
	{
		value = Mathf.Clamp01(value);
		value = (Mathf.Sin(value * 3.1415927f * (0.2f + 2.5f * value * value * value)) * Mathf.Pow(1f - value, 2.2f) + value) * (1f + 1.2f * (1f - value));
		return start + (end - start) * value;
	}

	// Token: 0x060001AC RID: 428 RVA: 0x00010020 File Offset: 0x0000E220
	public static float EaseInQuad(float start, float end, float value)
	{
		end -= start;
		return end * value * value + start;
	}

	// Token: 0x060001AD RID: 429 RVA: 0x0001002E File Offset: 0x0000E22E
	public static float EaseOutQuad(float start, float end, float value)
	{
		end -= start;
		return -end * value * (value - 2f) + start;
	}

	// Token: 0x060001AE RID: 430 RVA: 0x00010044 File Offset: 0x0000E244
	public static float EaseInOutQuad(float start, float end, float value)
	{
		value /= 0.5f;
		end -= start;
		if (value < 1f)
		{
			return end * 0.5f * value * value + start;
		}
		value -= 1f;
		return -end * 0.5f * (value * (value - 2f) - 1f) + start;
	}

	// Token: 0x060001AF RID: 431 RVA: 0x00010098 File Offset: 0x0000E298
	public static float EaseInCubic(float start, float end, float value)
	{
		end -= start;
		return end * value * value * value + start;
	}

	// Token: 0x060001B0 RID: 432 RVA: 0x000100A8 File Offset: 0x0000E2A8
	public static float EaseOutCubic(float start, float end, float value)
	{
		value -= 1f;
		end -= start;
		return end * (value * value * value + 1f) + start;
	}

	// Token: 0x060001B1 RID: 433 RVA: 0x000100C8 File Offset: 0x0000E2C8
	public static float EaseInOutCubic(float start, float end, float value)
	{
		value /= 0.5f;
		end -= start;
		if (value < 1f)
		{
			return end * 0.5f * value * value * value + start;
		}
		value -= 2f;
		return end * 0.5f * (value * value * value + 2f) + start;
	}

	// Token: 0x060001B2 RID: 434 RVA: 0x00010119 File Offset: 0x0000E319
	public static float EaseInQuart(float start, float end, float value)
	{
		end -= start;
		return end * value * value * value * value + start;
	}

	// Token: 0x060001B3 RID: 435 RVA: 0x0001012B File Offset: 0x0000E32B
	public static float EaseOutQuart(float start, float end, float value)
	{
		value -= 1f;
		end -= start;
		return -end * (value * value * value * value - 1f) + start;
	}

	// Token: 0x060001B4 RID: 436 RVA: 0x00010150 File Offset: 0x0000E350
	public static float EaseInOutQuart(float start, float end, float value)
	{
		value /= 0.5f;
		end -= start;
		if (value < 1f)
		{
			return end * 0.5f * value * value * value * value + start;
		}
		value -= 2f;
		return -end * 0.5f * (value * value * value * value - 2f) + start;
	}

	// Token: 0x060001B5 RID: 437 RVA: 0x000101A6 File Offset: 0x0000E3A6
	public static float EaseInQuint(float start, float end, float value)
	{
		end -= start;
		return end * value * value * value * value * value + start;
	}

	// Token: 0x060001B6 RID: 438 RVA: 0x000101BA File Offset: 0x0000E3BA
	public static float EaseOutQuint(float start, float end, float value)
	{
		value -= 1f;
		end -= start;
		return end * (value * value * value * value * value + 1f) + start;
	}

	// Token: 0x060001B7 RID: 439 RVA: 0x000101E0 File Offset: 0x0000E3E0
	public static float EaseInOutQuint(float start, float end, float value)
	{
		value /= 0.5f;
		end -= start;
		if (value < 1f)
		{
			return end * 0.5f * value * value * value * value * value + start;
		}
		value -= 2f;
		return end * 0.5f * (value * value * value * value * value + 2f) + start;
	}

	// Token: 0x060001B8 RID: 440 RVA: 0x00010239 File Offset: 0x0000E439
	public static float EaseInSine(float start, float end, float value)
	{
		end -= start;
		return -end * Mathf.Cos(value * 1.5707964f) + end + start;
	}

	// Token: 0x060001B9 RID: 441 RVA: 0x00010253 File Offset: 0x0000E453
	public static float EaseOutSine(float start, float end, float value)
	{
		end -= start;
		return end * Mathf.Sin(value * 1.5707964f) + start;
	}

	// Token: 0x060001BA RID: 442 RVA: 0x0001026A File Offset: 0x0000E46A
	public static float EaseInOutSine(float start, float end, float value)
	{
		end -= start;
		return -end * 0.5f * (Mathf.Cos(3.1415927f * value) - 1f) + start;
	}

	// Token: 0x060001BB RID: 443 RVA: 0x0001028E File Offset: 0x0000E48E
	public static float EaseInExpo(float start, float end, float value)
	{
		end -= start;
		return end * Mathf.Pow(2f, 10f * (value - 1f)) + start;
	}

	// Token: 0x060001BC RID: 444 RVA: 0x000102B0 File Offset: 0x0000E4B0
	public static float EaseOutExpo(float start, float end, float value)
	{
		end -= start;
		return end * (-Mathf.Pow(2f, -10f * value) + 1f) + start;
	}

	// Token: 0x060001BD RID: 445 RVA: 0x000102D4 File Offset: 0x0000E4D4
	public static float EaseInOutExpo(float start, float end, float value)
	{
		value /= 0.5f;
		end -= start;
		if (value < 1f)
		{
			return end * 0.5f * Mathf.Pow(2f, 10f * (value - 1f)) + start;
		}
		value -= 1f;
		return end * 0.5f * (-Mathf.Pow(2f, -10f * value) + 2f) + start;
	}

	// Token: 0x060001BE RID: 446 RVA: 0x00010344 File Offset: 0x0000E544
	public static float EaseInCirc(float start, float end, float value)
	{
		end -= start;
		return -end * (Mathf.Sqrt(1f - value * value) - 1f) + start;
	}

	// Token: 0x060001BF RID: 447 RVA: 0x00010364 File Offset: 0x0000E564
	public static float EaseOutCirc(float start, float end, float value)
	{
		value -= 1f;
		end -= start;
		return end * Mathf.Sqrt(1f - value * value) + start;
	}

	// Token: 0x060001C0 RID: 448 RVA: 0x00010388 File Offset: 0x0000E588
	public static float EaseInOutCirc(float start, float end, float value)
	{
		value /= 0.5f;
		end -= start;
		if (value < 1f)
		{
			return -end * 0.5f * (Mathf.Sqrt(1f - value * value) - 1f) + start;
		}
		value -= 2f;
		return end * 0.5f * (Mathf.Sqrt(1f - value * value) + 1f) + start;
	}

	// Token: 0x060001C1 RID: 449 RVA: 0x000103F4 File Offset: 0x0000E5F4
	public static float EaseInBounce(float start, float end, float value)
	{
		end -= start;
		float num = 1f;
		return end - EasingFunction.EaseOutBounce(0f, end, num - value) + start;
	}

	// Token: 0x060001C2 RID: 450 RVA: 0x00010420 File Offset: 0x0000E620
	public static float EaseOutBounce(float start, float end, float value)
	{
		value /= 1f;
		end -= start;
		if (value < 0.36363637f)
		{
			return end * (7.5625f * value * value) + start;
		}
		if (value < 0.72727275f)
		{
			value -= 0.54545456f;
			return end * (7.5625f * value * value + 0.75f) + start;
		}
		if ((double)value < 0.9090909090909091)
		{
			value -= 0.8181818f;
			return end * (7.5625f * value * value + 0.9375f) + start;
		}
		value -= 0.95454544f;
		return end * (7.5625f * value * value + 0.984375f) + start;
	}

	// Token: 0x060001C3 RID: 451 RVA: 0x000104BC File Offset: 0x0000E6BC
	public static float EaseInOutBounce(float start, float end, float value)
	{
		end -= start;
		float num = 1f;
		if (value < num * 0.5f)
		{
			return EasingFunction.EaseInBounce(0f, end, value * 2f) * 0.5f + start;
		}
		return EasingFunction.EaseOutBounce(0f, end, value * 2f - num) * 0.5f + end * 0.5f + start;
	}

	// Token: 0x060001C4 RID: 452 RVA: 0x00010520 File Offset: 0x0000E720
	public static float EaseInBack(float start, float end, float value)
	{
		end -= start;
		value /= 1f;
		float num = 1.70158f;
		return end * value * value * ((num + 1f) * value - num) + start;
	}

	// Token: 0x060001C5 RID: 453 RVA: 0x00010554 File Offset: 0x0000E754
	public static float EaseOutBack(float start, float end, float value)
	{
		float num = 1.70158f;
		end -= start;
		value -= 1f;
		return end * (value * value * ((num + 1f) * value + num) + 1f) + start;
	}

	// Token: 0x060001C6 RID: 454 RVA: 0x00010590 File Offset: 0x0000E790
	public static float EaseInOutBack(float start, float end, float value)
	{
		float num = 1.70158f;
		end -= start;
		value /= 0.5f;
		if (value < 1f)
		{
			num *= 1.525f;
			return end * 0.5f * (value * value * ((num + 1f) * value - num)) + start;
		}
		value -= 2f;
		num *= 1.525f;
		return end * 0.5f * (value * value * ((num + 1f) * value + num) + 2f) + start;
	}

	// Token: 0x060001C7 RID: 455 RVA: 0x0001060C File Offset: 0x0000E80C
	public static float EaseInElastic(float start, float end, float value)
	{
		end -= start;
		float num = 1f;
		float num2 = num * 0.3f;
		float num3 = 0f;
		if (value == 0f)
		{
			return start;
		}
		if ((value /= num) == 1f)
		{
			return start + end;
		}
		float num4;
		if (num3 == 0f || num3 < Mathf.Abs(end))
		{
			num3 = end;
			num4 = num2 / 4f;
		}
		else
		{
			num4 = num2 / 6.2831855f * Mathf.Asin(end / num3);
		}
		return -(num3 * Mathf.Pow(2f, 10f * (value -= 1f)) * Mathf.Sin((value * num - num4) * 6.2831855f / num2)) + start;
	}

	// Token: 0x060001C8 RID: 456 RVA: 0x000106B0 File Offset: 0x0000E8B0
	public static float EaseOutElastic(float start, float end, float value)
	{
		end -= start;
		float num = 1f;
		float num2 = num * 0.3f;
		float num3 = 0f;
		if (value == 0f)
		{
			return start;
		}
		if ((value /= num) == 1f)
		{
			return start + end;
		}
		float num4;
		if (num3 == 0f || num3 < Mathf.Abs(end))
		{
			num3 = end;
			num4 = num2 * 0.25f;
		}
		else
		{
			num4 = num2 / 6.2831855f * Mathf.Asin(end / num3);
		}
		return num3 * Mathf.Pow(2f, -10f * value) * Mathf.Sin((value * num - num4) * 6.2831855f / num2) + end + start;
	}

	// Token: 0x060001C9 RID: 457 RVA: 0x0001074C File Offset: 0x0000E94C
	public static float EaseInOutElastic(float start, float end, float value)
	{
		end -= start;
		float num = 1f;
		float num2 = num * 0.3f;
		float num3 = 0f;
		if (value == 0f)
		{
			return start;
		}
		if ((value /= num * 0.5f) == 2f)
		{
			return start + end;
		}
		float num4;
		if (num3 == 0f || num3 < Mathf.Abs(end))
		{
			num3 = end;
			num4 = num2 / 4f;
		}
		else
		{
			num4 = num2 / 6.2831855f * Mathf.Asin(end / num3);
		}
		if (value < 1f)
		{
			return -0.5f * (num3 * Mathf.Pow(2f, 10f * (value -= 1f)) * Mathf.Sin((value * num - num4) * 6.2831855f / num2)) + start;
		}
		return num3 * Mathf.Pow(2f, -10f * (value -= 1f)) * Mathf.Sin((value * num - num4) * 6.2831855f / num2) * 0.5f + end + start;
	}

	// Token: 0x060001CA RID: 458 RVA: 0x0001083A File Offset: 0x0000EA3A
	public static float LinearD(float start, float end, float value)
	{
		return end - start;
	}

	// Token: 0x060001CB RID: 459 RVA: 0x0001083F File Offset: 0x0000EA3F
	public static float EaseInQuadD(float start, float end, float value)
	{
		return 2f * (end - start) * value;
	}

	// Token: 0x060001CC RID: 460 RVA: 0x0001084C File Offset: 0x0000EA4C
	public static float EaseOutQuadD(float start, float end, float value)
	{
		end -= start;
		return -end * value - end * (value - 2f);
	}

	// Token: 0x060001CD RID: 461 RVA: 0x00010861 File Offset: 0x0000EA61
	public static float EaseInOutQuadD(float start, float end, float value)
	{
		value /= 0.5f;
		end -= start;
		if (value < 1f)
		{
			return end * value;
		}
		value -= 1f;
		return end * (1f - value);
	}

	// Token: 0x060001CE RID: 462 RVA: 0x0001088F File Offset: 0x0000EA8F
	public static float EaseInCubicD(float start, float end, float value)
	{
		return 3f * (end - start) * value * value;
	}

	// Token: 0x060001CF RID: 463 RVA: 0x0001089E File Offset: 0x0000EA9E
	public static float EaseOutCubicD(float start, float end, float value)
	{
		value -= 1f;
		end -= start;
		return 3f * end * value * value;
	}

	// Token: 0x060001D0 RID: 464 RVA: 0x000108B9 File Offset: 0x0000EAB9
	public static float EaseInOutCubicD(float start, float end, float value)
	{
		value /= 0.5f;
		end -= start;
		if (value < 1f)
		{
			return 1.5f * end * value * value;
		}
		value -= 2f;
		return 1.5f * end * value * value;
	}

	// Token: 0x060001D1 RID: 465 RVA: 0x000108F1 File Offset: 0x0000EAF1
	public static float EaseInQuartD(float start, float end, float value)
	{
		return 4f * (end - start) * value * value * value;
	}

	// Token: 0x060001D2 RID: 466 RVA: 0x00010902 File Offset: 0x0000EB02
	public static float EaseOutQuartD(float start, float end, float value)
	{
		value -= 1f;
		end -= start;
		return -4f * end * value * value * value;
	}

	// Token: 0x060001D3 RID: 467 RVA: 0x0001091F File Offset: 0x0000EB1F
	public static float EaseInOutQuartD(float start, float end, float value)
	{
		value /= 0.5f;
		end -= start;
		if (value < 1f)
		{
			return 2f * end * value * value * value;
		}
		value -= 2f;
		return -2f * end * value * value * value;
	}

	// Token: 0x060001D4 RID: 468 RVA: 0x0001095B File Offset: 0x0000EB5B
	public static float EaseInQuintD(float start, float end, float value)
	{
		return 5f * (end - start) * value * value * value * value;
	}

	// Token: 0x060001D5 RID: 469 RVA: 0x0001096E File Offset: 0x0000EB6E
	public static float EaseOutQuintD(float start, float end, float value)
	{
		value -= 1f;
		end -= start;
		return 5f * end * value * value * value * value;
	}

	// Token: 0x060001D6 RID: 470 RVA: 0x0001098D File Offset: 0x0000EB8D
	public static float EaseInOutQuintD(float start, float end, float value)
	{
		value /= 0.5f;
		end -= start;
		if (value < 1f)
		{
			return 2.5f * end * value * value * value * value;
		}
		value -= 2f;
		return 2.5f * end * value * value * value * value;
	}

	// Token: 0x060001D7 RID: 471 RVA: 0x000109CD File Offset: 0x0000EBCD
	public static float EaseInSineD(float start, float end, float value)
	{
		return (end - start) * 0.5f * 3.1415927f * Mathf.Sin(1.5707964f * value);
	}

	// Token: 0x060001D8 RID: 472 RVA: 0x000109EB File Offset: 0x0000EBEB
	public static float EaseOutSineD(float start, float end, float value)
	{
		end -= start;
		return 1.5707964f * end * Mathf.Cos(value * 1.5707964f);
	}

	// Token: 0x060001D9 RID: 473 RVA: 0x00010A06 File Offset: 0x0000EC06
	public static float EaseInOutSineD(float start, float end, float value)
	{
		end -= start;
		return end * 0.5f * 3.1415927f * Mathf.Cos(3.1415927f * value);
	}

	// Token: 0x060001DA RID: 474 RVA: 0x00010A27 File Offset: 0x0000EC27
	public static float EaseInExpoD(float start, float end, float value)
	{
		return 6.931472f * (end - start) * Mathf.Pow(2f, 10f * (value - 1f));
	}

	// Token: 0x060001DB RID: 475 RVA: 0x00010A4A File Offset: 0x0000EC4A
	public static float EaseOutExpoD(float start, float end, float value)
	{
		end -= start;
		return 3.465736f * end * Mathf.Pow(2f, 1f - 10f * value);
	}

	// Token: 0x060001DC RID: 476 RVA: 0x00010A70 File Offset: 0x0000EC70
	public static float EaseInOutExpoD(float start, float end, float value)
	{
		value /= 0.5f;
		end -= start;
		if (value < 1f)
		{
			return 3.465736f * end * Mathf.Pow(2f, 10f * (value - 1f));
		}
		value -= 1f;
		return 3.465736f * end / Mathf.Pow(2f, 10f * value);
	}

	// Token: 0x060001DD RID: 477 RVA: 0x00010AD5 File Offset: 0x0000ECD5
	public static float EaseInCircD(float start, float end, float value)
	{
		return (end - start) * value / Mathf.Sqrt(1f - value * value);
	}

	// Token: 0x060001DE RID: 478 RVA: 0x00010AEB File Offset: 0x0000ECEB
	public static float EaseOutCircD(float start, float end, float value)
	{
		value -= 1f;
		end -= start;
		return -end * value / Mathf.Sqrt(1f - value * value);
	}

	// Token: 0x060001DF RID: 479 RVA: 0x00010B10 File Offset: 0x0000ED10
	public static float EaseInOutCircD(float start, float end, float value)
	{
		value /= 0.5f;
		end -= start;
		if (value < 1f)
		{
			return end * value / (2f * Mathf.Sqrt(1f - value * value));
		}
		value -= 2f;
		return -end * value / (2f * Mathf.Sqrt(1f - value * value));
	}

	// Token: 0x060001E0 RID: 480 RVA: 0x00010B70 File Offset: 0x0000ED70
	public static float EaseInBounceD(float start, float end, float value)
	{
		end -= start;
		float num = 1f;
		return EasingFunction.EaseOutBounceD(0f, end, num - value);
	}

	// Token: 0x060001E1 RID: 481 RVA: 0x00010B98 File Offset: 0x0000ED98
	public static float EaseOutBounceD(float start, float end, float value)
	{
		value /= 1f;
		end -= start;
		if (value < 0.36363637f)
		{
			return 2f * end * 7.5625f * value;
		}
		if (value < 0.72727275f)
		{
			value -= 0.54545456f;
			return 2f * end * 7.5625f * value;
		}
		if ((double)value < 0.9090909090909091)
		{
			value -= 0.8181818f;
			return 2f * end * 7.5625f * value;
		}
		value -= 0.95454544f;
		return 2f * end * 7.5625f * value;
	}

	// Token: 0x060001E2 RID: 482 RVA: 0x00010C2C File Offset: 0x0000EE2C
	public static float EaseInOutBounceD(float start, float end, float value)
	{
		end -= start;
		float num = 1f;
		if (value < num * 0.5f)
		{
			return EasingFunction.EaseInBounceD(0f, end, value * 2f) * 0.5f;
		}
		return EasingFunction.EaseOutBounceD(0f, end, value * 2f - num) * 0.5f;
	}

	// Token: 0x060001E3 RID: 483 RVA: 0x00010C84 File Offset: 0x0000EE84
	public static float EaseInBackD(float start, float end, float value)
	{
		float num = 1.70158f;
		return 3f * (num + 1f) * (end - start) * value * value - 2f * num * (end - start) * value;
	}

	// Token: 0x060001E4 RID: 484 RVA: 0x00010CBC File Offset: 0x0000EEBC
	public static float EaseOutBackD(float start, float end, float value)
	{
		float num = 1.70158f;
		end -= start;
		value -= 1f;
		return end * ((num + 1f) * value * value + 2f * value * ((num + 1f) * value + num));
	}

	// Token: 0x060001E5 RID: 485 RVA: 0x00010D00 File Offset: 0x0000EF00
	public static float EaseInOutBackD(float start, float end, float value)
	{
		float num = 1.70158f;
		end -= start;
		value /= 0.5f;
		if (value < 1f)
		{
			num *= 1.525f;
			return 0.5f * end * (num + 1f) * value * value + end * value * ((num + 1f) * value - num);
		}
		value -= 2f;
		num *= 1.525f;
		return 0.5f * end * ((num + 1f) * value * value + 2f * value * ((num + 1f) * value + num));
	}

	// Token: 0x060001E6 RID: 486 RVA: 0x00010D90 File Offset: 0x0000EF90
	public static float EaseInElasticD(float start, float end, float value)
	{
		end -= start;
		float num = 1f;
		float num2 = num * 0.3f;
		float num3 = 0f;
		float num4;
		if (num3 == 0f || num3 < Mathf.Abs(end))
		{
			num3 = end;
			num4 = num2 / 4f;
		}
		else
		{
			num4 = num2 / 6.2831855f * Mathf.Asin(end / num3);
		}
		float num5 = 6.2831855f;
		return -num3 * num * num5 * Mathf.Cos(num5 * (num * (value - 1f) - num4) / num2) / num2 - 3.465736f * num3 * Mathf.Sin(num5 * (num * (value - 1f) - num4) / num2) * Mathf.Pow(2f, 10f * (value - 1f) + 1f);
	}

	// Token: 0x060001E7 RID: 487 RVA: 0x00010E48 File Offset: 0x0000F048
	public static float EaseOutElasticD(float start, float end, float value)
	{
		end -= start;
		float num = 1f;
		float num2 = num * 0.3f;
		float num3 = 0f;
		float num4;
		if (num3 == 0f || num3 < Mathf.Abs(end))
		{
			num3 = end;
			num4 = num2 * 0.25f;
		}
		else
		{
			num4 = num2 / 6.2831855f * Mathf.Asin(end / num3);
		}
		return num3 * 3.1415927f * num * Mathf.Pow(2f, 1f - 10f * value) * Mathf.Cos(6.2831855f * (num * value - num4) / num2) / num2 - 3.465736f * num3 * Mathf.Pow(2f, 1f - 10f * value) * Mathf.Sin(6.2831855f * (num * value - num4) / num2);
	}

	// Token: 0x060001E8 RID: 488 RVA: 0x00010F08 File Offset: 0x0000F108
	public static float EaseInOutElasticD(float start, float end, float value)
	{
		end -= start;
		float num = 1f;
		float num2 = num * 0.3f;
		float num3 = 0f;
		float num4;
		if (num3 == 0f || num3 < Mathf.Abs(end))
		{
			num3 = end;
			num4 = num2 / 4f;
		}
		else
		{
			num4 = num2 / 6.2831855f * Mathf.Asin(end / num3);
		}
		if (value < 1f)
		{
			value -= 1f;
			return -3.465736f * num3 * Mathf.Pow(2f, 10f * value) * Mathf.Sin(6.2831855f * (num * value - 2f) / num2) - num3 * 3.1415927f * num * Mathf.Pow(2f, 10f * value) * Mathf.Cos(6.2831855f * (num * value - num4) / num2) / num2;
		}
		value -= 1f;
		return num3 * 3.1415927f * num * Mathf.Cos(6.2831855f * (num * value - num4) / num2) / (num2 * Mathf.Pow(2f, 10f * value)) - 3.465736f * num3 * Mathf.Sin(6.2831855f * (num * value - num4) / num2) / Mathf.Pow(2f, 10f * value);
	}

	// Token: 0x060001E9 RID: 489 RVA: 0x00011038 File Offset: 0x0000F238
	public static float SpringD(float start, float end, float value)
	{
		value = Mathf.Clamp01(value);
		end -= start;
		return end * (6f * (1f - value) / 5f + 1f) * (-2.2f * Mathf.Pow(1f - value, 1.2f) * Mathf.Sin(3.1415927f * value * (2.5f * value * value * value + 0.2f)) + Mathf.Pow(1f - value, 2.2f) * (3.1415927f * (2.5f * value * value * value + 0.2f) + 23.561945f * value * value * value) * Mathf.Cos(3.1415927f * value * (2.5f * value * value * value + 0.2f)) + 1f) - 6f * end * (Mathf.Pow(1f - value, 2.2f) * Mathf.Sin(3.1415927f * value * (2.5f * value * value * value + 0.2f)) + value / 5f);
	}

	// Token: 0x060001EA RID: 490 RVA: 0x00011140 File Offset: 0x0000F340
	public static EasingFunction.Function GetEasingFunction(EasingFunction.Ease easingFunction)
	{
		if (easingFunction == EasingFunction.Ease.EaseInQuad)
		{
			return new EasingFunction.Function(EasingFunction.EaseInQuad);
		}
		if (easingFunction == EasingFunction.Ease.EaseOutQuad)
		{
			return new EasingFunction.Function(EasingFunction.EaseOutQuad);
		}
		if (easingFunction == EasingFunction.Ease.EaseInOutQuad)
		{
			return new EasingFunction.Function(EasingFunction.EaseInOutQuad);
		}
		if (easingFunction == EasingFunction.Ease.EaseInCubic)
		{
			return new EasingFunction.Function(EasingFunction.EaseInCubic);
		}
		if (easingFunction == EasingFunction.Ease.EaseOutCubic)
		{
			return new EasingFunction.Function(EasingFunction.EaseOutCubic);
		}
		if (easingFunction == EasingFunction.Ease.EaseInOutCubic)
		{
			return new EasingFunction.Function(EasingFunction.EaseInOutCubic);
		}
		if (easingFunction == EasingFunction.Ease.EaseInQuart)
		{
			return new EasingFunction.Function(EasingFunction.EaseInQuart);
		}
		if (easingFunction == EasingFunction.Ease.EaseOutQuart)
		{
			return new EasingFunction.Function(EasingFunction.EaseOutQuart);
		}
		if (easingFunction == EasingFunction.Ease.EaseInOutQuart)
		{
			return new EasingFunction.Function(EasingFunction.EaseInOutQuart);
		}
		if (easingFunction == EasingFunction.Ease.EaseInQuint)
		{
			return new EasingFunction.Function(EasingFunction.EaseInQuint);
		}
		if (easingFunction == EasingFunction.Ease.EaseOutQuint)
		{
			return new EasingFunction.Function(EasingFunction.EaseOutQuint);
		}
		if (easingFunction == EasingFunction.Ease.EaseInOutQuint)
		{
			return new EasingFunction.Function(EasingFunction.EaseInOutQuint);
		}
		if (easingFunction == EasingFunction.Ease.EaseInSine)
		{
			return new EasingFunction.Function(EasingFunction.EaseInSine);
		}
		if (easingFunction == EasingFunction.Ease.EaseOutSine)
		{
			return new EasingFunction.Function(EasingFunction.EaseOutSine);
		}
		if (easingFunction == EasingFunction.Ease.EaseInOutSine)
		{
			return new EasingFunction.Function(EasingFunction.EaseInOutSine);
		}
		if (easingFunction == EasingFunction.Ease.EaseInExpo)
		{
			return new EasingFunction.Function(EasingFunction.EaseInExpo);
		}
		if (easingFunction == EasingFunction.Ease.EaseOutExpo)
		{
			return new EasingFunction.Function(EasingFunction.EaseOutExpo);
		}
		if (easingFunction == EasingFunction.Ease.EaseInOutExpo)
		{
			return new EasingFunction.Function(EasingFunction.EaseInOutExpo);
		}
		if (easingFunction == EasingFunction.Ease.EaseInCirc)
		{
			return new EasingFunction.Function(EasingFunction.EaseInCirc);
		}
		if (easingFunction == EasingFunction.Ease.EaseOutCirc)
		{
			return new EasingFunction.Function(EasingFunction.EaseOutCirc);
		}
		if (easingFunction == EasingFunction.Ease.EaseInOutCirc)
		{
			return new EasingFunction.Function(EasingFunction.EaseInOutCirc);
		}
		if (easingFunction == EasingFunction.Ease.Linear)
		{
			return new EasingFunction.Function(EasingFunction.Linear);
		}
		if (easingFunction == EasingFunction.Ease.Spring)
		{
			return new EasingFunction.Function(EasingFunction.Spring);
		}
		if (easingFunction == EasingFunction.Ease.EaseInBounce)
		{
			return new EasingFunction.Function(EasingFunction.EaseInBounce);
		}
		if (easingFunction == EasingFunction.Ease.EaseOutBounce)
		{
			return new EasingFunction.Function(EasingFunction.EaseOutBounce);
		}
		if (easingFunction == EasingFunction.Ease.EaseInOutBounce)
		{
			return new EasingFunction.Function(EasingFunction.EaseInOutBounce);
		}
		if (easingFunction == EasingFunction.Ease.EaseInBack)
		{
			return new EasingFunction.Function(EasingFunction.EaseInBack);
		}
		if (easingFunction == EasingFunction.Ease.EaseOutBack)
		{
			return new EasingFunction.Function(EasingFunction.EaseOutBack);
		}
		if (easingFunction == EasingFunction.Ease.EaseInOutBack)
		{
			return new EasingFunction.Function(EasingFunction.EaseInOutBack);
		}
		if (easingFunction == EasingFunction.Ease.EaseInElastic)
		{
			return new EasingFunction.Function(EasingFunction.EaseInElastic);
		}
		if (easingFunction == EasingFunction.Ease.EaseOutElastic)
		{
			return new EasingFunction.Function(EasingFunction.EaseOutElastic);
		}
		if (easingFunction == EasingFunction.Ease.EaseInOutElastic)
		{
			return new EasingFunction.Function(EasingFunction.EaseInOutElastic);
		}
		return null;
	}

	// Token: 0x060001EB RID: 491 RVA: 0x00011384 File Offset: 0x0000F584
	public static EasingFunction.Function GetEasingFunctionDerivative(EasingFunction.Ease easingFunction)
	{
		if (easingFunction == EasingFunction.Ease.EaseInQuad)
		{
			return new EasingFunction.Function(EasingFunction.EaseInQuadD);
		}
		if (easingFunction == EasingFunction.Ease.EaseOutQuad)
		{
			return new EasingFunction.Function(EasingFunction.EaseOutQuadD);
		}
		if (easingFunction == EasingFunction.Ease.EaseInOutQuad)
		{
			return new EasingFunction.Function(EasingFunction.EaseInOutQuadD);
		}
		if (easingFunction == EasingFunction.Ease.EaseInCubic)
		{
			return new EasingFunction.Function(EasingFunction.EaseInCubicD);
		}
		if (easingFunction == EasingFunction.Ease.EaseOutCubic)
		{
			return new EasingFunction.Function(EasingFunction.EaseOutCubicD);
		}
		if (easingFunction == EasingFunction.Ease.EaseInOutCubic)
		{
			return new EasingFunction.Function(EasingFunction.EaseInOutCubicD);
		}
		if (easingFunction == EasingFunction.Ease.EaseInQuart)
		{
			return new EasingFunction.Function(EasingFunction.EaseInQuartD);
		}
		if (easingFunction == EasingFunction.Ease.EaseOutQuart)
		{
			return new EasingFunction.Function(EasingFunction.EaseOutQuartD);
		}
		if (easingFunction == EasingFunction.Ease.EaseInOutQuart)
		{
			return new EasingFunction.Function(EasingFunction.EaseInOutQuartD);
		}
		if (easingFunction == EasingFunction.Ease.EaseInQuint)
		{
			return new EasingFunction.Function(EasingFunction.EaseInQuintD);
		}
		if (easingFunction == EasingFunction.Ease.EaseOutQuint)
		{
			return new EasingFunction.Function(EasingFunction.EaseOutQuintD);
		}
		if (easingFunction == EasingFunction.Ease.EaseInOutQuint)
		{
			return new EasingFunction.Function(EasingFunction.EaseInOutQuintD);
		}
		if (easingFunction == EasingFunction.Ease.EaseInSine)
		{
			return new EasingFunction.Function(EasingFunction.EaseInSineD);
		}
		if (easingFunction == EasingFunction.Ease.EaseOutSine)
		{
			return new EasingFunction.Function(EasingFunction.EaseOutSineD);
		}
		if (easingFunction == EasingFunction.Ease.EaseInOutSine)
		{
			return new EasingFunction.Function(EasingFunction.EaseInOutSineD);
		}
		if (easingFunction == EasingFunction.Ease.EaseInExpo)
		{
			return new EasingFunction.Function(EasingFunction.EaseInExpoD);
		}
		if (easingFunction == EasingFunction.Ease.EaseOutExpo)
		{
			return new EasingFunction.Function(EasingFunction.EaseOutExpoD);
		}
		if (easingFunction == EasingFunction.Ease.EaseInOutExpo)
		{
			return new EasingFunction.Function(EasingFunction.EaseInOutExpoD);
		}
		if (easingFunction == EasingFunction.Ease.EaseInCirc)
		{
			return new EasingFunction.Function(EasingFunction.EaseInCircD);
		}
		if (easingFunction == EasingFunction.Ease.EaseOutCirc)
		{
			return new EasingFunction.Function(EasingFunction.EaseOutCircD);
		}
		if (easingFunction == EasingFunction.Ease.EaseInOutCirc)
		{
			return new EasingFunction.Function(EasingFunction.EaseInOutCircD);
		}
		if (easingFunction == EasingFunction.Ease.Linear)
		{
			return new EasingFunction.Function(EasingFunction.LinearD);
		}
		if (easingFunction == EasingFunction.Ease.Spring)
		{
			return new EasingFunction.Function(EasingFunction.SpringD);
		}
		if (easingFunction == EasingFunction.Ease.EaseInBounce)
		{
			return new EasingFunction.Function(EasingFunction.EaseInBounceD);
		}
		if (easingFunction == EasingFunction.Ease.EaseOutBounce)
		{
			return new EasingFunction.Function(EasingFunction.EaseOutBounceD);
		}
		if (easingFunction == EasingFunction.Ease.EaseInOutBounce)
		{
			return new EasingFunction.Function(EasingFunction.EaseInOutBounceD);
		}
		if (easingFunction == EasingFunction.Ease.EaseInBack)
		{
			return new EasingFunction.Function(EasingFunction.EaseInBackD);
		}
		if (easingFunction == EasingFunction.Ease.EaseOutBack)
		{
			return new EasingFunction.Function(EasingFunction.EaseOutBackD);
		}
		if (easingFunction == EasingFunction.Ease.EaseInOutBack)
		{
			return new EasingFunction.Function(EasingFunction.EaseInOutBackD);
		}
		if (easingFunction == EasingFunction.Ease.EaseInElastic)
		{
			return new EasingFunction.Function(EasingFunction.EaseInElasticD);
		}
		if (easingFunction == EasingFunction.Ease.EaseOutElastic)
		{
			return new EasingFunction.Function(EasingFunction.EaseOutElasticD);
		}
		if (easingFunction == EasingFunction.Ease.EaseInOutElastic)
		{
			return new EasingFunction.Function(EasingFunction.EaseInOutElasticD);
		}
		return null;
	}

	// Token: 0x060001EC RID: 492 RVA: 0x000043CE File Offset: 0x000025CE
	public EasingFunction()
	{
	}

	// Token: 0x040000A6 RID: 166
	private const float NATURAL_LOG_OF_2 = 0.6931472f;

	// Token: 0x02000120 RID: 288
	public enum Ease
	{
		// Token: 0x0400045C RID: 1116
		EaseInQuad,
		// Token: 0x0400045D RID: 1117
		EaseOutQuad,
		// Token: 0x0400045E RID: 1118
		EaseInOutQuad,
		// Token: 0x0400045F RID: 1119
		EaseInCubic,
		// Token: 0x04000460 RID: 1120
		EaseOutCubic,
		// Token: 0x04000461 RID: 1121
		EaseInOutCubic,
		// Token: 0x04000462 RID: 1122
		EaseInQuart,
		// Token: 0x04000463 RID: 1123
		EaseOutQuart,
		// Token: 0x04000464 RID: 1124
		EaseInOutQuart,
		// Token: 0x04000465 RID: 1125
		EaseInQuint,
		// Token: 0x04000466 RID: 1126
		EaseOutQuint,
		// Token: 0x04000467 RID: 1127
		EaseInOutQuint,
		// Token: 0x04000468 RID: 1128
		EaseInSine,
		// Token: 0x04000469 RID: 1129
		EaseOutSine,
		// Token: 0x0400046A RID: 1130
		EaseInOutSine,
		// Token: 0x0400046B RID: 1131
		EaseInExpo,
		// Token: 0x0400046C RID: 1132
		EaseOutExpo,
		// Token: 0x0400046D RID: 1133
		EaseInOutExpo,
		// Token: 0x0400046E RID: 1134
		EaseInCirc,
		// Token: 0x0400046F RID: 1135
		EaseOutCirc,
		// Token: 0x04000470 RID: 1136
		EaseInOutCirc,
		// Token: 0x04000471 RID: 1137
		Linear,
		// Token: 0x04000472 RID: 1138
		Spring,
		// Token: 0x04000473 RID: 1139
		EaseInBounce,
		// Token: 0x04000474 RID: 1140
		EaseOutBounce,
		// Token: 0x04000475 RID: 1141
		EaseInOutBounce,
		// Token: 0x04000476 RID: 1142
		EaseInBack,
		// Token: 0x04000477 RID: 1143
		EaseOutBack,
		// Token: 0x04000478 RID: 1144
		EaseInOutBack,
		// Token: 0x04000479 RID: 1145
		EaseInElastic,
		// Token: 0x0400047A RID: 1146
		EaseOutElastic,
		// Token: 0x0400047B RID: 1147
		EaseInOutElastic
	}

	// Token: 0x02000121 RID: 289
	// (Invoke) Token: 0x0600096F RID: 2415
	public delegate float Function(float s, float e, float v);
}
