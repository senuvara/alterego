﻿using System;

// Token: 0x0200003B RID: 59
[Serializable]
public class AdfurikunMovieNativeAdViewAdConfig
{
	// Token: 0x0600026A RID: 618 RVA: 0x000043CE File Offset: 0x000025CE
	public AdfurikunMovieNativeAdViewAdConfig()
	{
	}

	// Token: 0x040000D4 RID: 212
	public string iPhoneAppID;

	// Token: 0x040000D5 RID: 213
	public string androidAppID;
}
