﻿using System;

// Token: 0x0200003D RID: 61
[Serializable]
public class AdfurikunMovieRewardAdConfig
{
	// Token: 0x0600028F RID: 655 RVA: 0x000043CE File Offset: 0x000025CE
	public AdfurikunMovieRewardAdConfig()
	{
	}

	// Token: 0x040000DF RID: 223
	public string iPhoneAppID;

	// Token: 0x040000E0 RID: 224
	public string androidAppID;
}
