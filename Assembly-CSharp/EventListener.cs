﻿using System;
using UnityEngine;

// Token: 0x0200000E RID: 14
public class EventListener : MonoBehaviour
{
	// Token: 0x06000068 RID: 104 RVA: 0x00007AD6 File Offset: 0x00005CD6
	public virtual bool OnEvent(string[] eventList)
	{
		return false;
	}

	// Token: 0x06000069 RID: 105 RVA: 0x000025AD File Offset: 0x000007AD
	public EventListener()
	{
	}
}
