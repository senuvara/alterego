﻿using System;
using System.Runtime.CompilerServices;
using App;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x0200001E RID: 30
public class SceneLibrary : SceneBase
{
	// Token: 0x060000DD RID: 221 RVA: 0x0000A01C File Offset: 0x0000821C
	public new void OnClick(GameObject clickObject)
	{
		global::Debug.Log("OnClick " + clickObject.name);
		string name = clickObject.name;
		if (name == "BookLibraryButton")
		{
			base.transform.Find("BookCanvas").gameObject.SetActive(true);
			base.StartCoroutine(AppUtil.FadeIn(base.transform.Find("BookCanvas"), 0.25f, null));
			this.SetButtonList();
			SceneCommon.BackButtonList.Add(base.transform.Find("BookCanvas/BackButton").GetComponent<OnClickHandler>());
			return;
		}
		if (name == "CounselingLibraryButton")
		{
			base.transform.Find("CounselingCanvas").gameObject.SetActive(true);
			base.StartCoroutine(AppUtil.FadeIn(base.transform.Find("CounselingCanvas"), 0.25f, null));
			this.SetCounselingList();
			SceneCommon.BackButtonList.Add(base.transform.Find("CounselingCanvas/BackButton").GetComponent<OnClickHandler>());
			return;
		}
		if (name == "BackButton")
		{
			this.OnClickBackKey(clickObject);
			SceneCommon.BackButtonList.Remove(clickObject.GetComponent<OnClickHandler>());
			return;
		}
		if (name == "ResultShareButton")
		{
			return;
		}
		if (name == "ShareButton")
		{
			if (base.transform.Find("CounselingCanvas").gameObject.activeSelf)
			{
				RectTransform mirrorFrame = base.transform.Find("CounselingCanvas/Frame").GetComponent<RectTransform>();
				Action<bool> SetUIActive = delegate(bool active)
				{
					clickObject.GetComponent<Button>().interactable = active;
					this.transform.Find("UICanvas").gameObject.SetActive(active);
					this.transform.Find("CounselingCanvas/BackButton").gameObject.SetActive(active);
					Transform transform = GameObject.Find("AppCommon").transform.Find("AdCanvas(Clone)");
					if (transform != null)
					{
						transform.gameObject.SetActive(active);
					}
					if (active)
					{
						mirrorFrame.localScale = Vector2.one;
						return;
					}
					float x = mirrorFrame.sizeDelta.x;
					if (x > (float)Screen.width)
					{
						mirrorFrame.localScale = Vector2.one * ((float)Screen.width / x);
					}
				};
				SetUIActive(false);
				base.StartCoroutine(AppUtil.Share(true, "自分探しタップゲーム『ALTER EGO』 caracolu.com/app/alterego/ #ALTEREGO", null, mirrorFrame));
				base.StartCoroutine(AppUtil.DelayAction(0.25f, delegate()
				{
					SetUIActive(true);
				}, true));
				return;
			}
		}
		if (clickObject.name.Contains("Book ("))
		{
			DialogManager.ShowDialog("BookDialog", new object[]
			{
				int.Parse(clickObject.name.Replace("Book (", "").Replace(")", "")).ToString(),
				2
			});
			return;
		}
		if (clickObject.name.Contains("Result"))
		{
			int no = int.Parse(clickObject.name.Replace("Result", ""));
			base.transform.Find("ResultCanvas").GetComponent<ResultCanvas>().RequestResult(CounselingResult.GetTitle(no), CounselingResult.Get(no), null);
			return;
		}
		base.OnClick(clickObject);
	}

	// Token: 0x060000DE RID: 222 RVA: 0x0000A314 File Offset: 0x00008514
	private void OnClickBackKey(GameObject backButton = null)
	{
		if (backButton == null)
		{
			backButton = GameObject.Find("BackButton");
		}
		if (DialogManager.IsShowing())
		{
			return;
		}
		if (backButton == null)
		{
			return;
		}
		base.StartCoroutine(AppUtil.FadeOut(backButton.transform.parent, 0.25f, delegate(Object target)
		{
			backButton.transform.parent.gameObject.SetActive(false);
		}));
	}

	// Token: 0x060000DF RID: 223 RVA: 0x0000A390 File Offset: 0x00008590
	private void SetButtonList()
	{
		for (int i = 1; i <= BookLevel.Max; i++)
		{
			base.transform.Find("BookCanvas/List/BookList/Book (" + i + ")").gameObject.SetActive(BookLevel.Get(i.ToString()) >= 0);
		}
	}

	// Token: 0x060000E0 RID: 224 RVA: 0x0000A3EC File Offset: 0x000085EC
	private void SetCounselingList()
	{
		Transform transform = base.transform.Find("CounselingCanvas/Frame/");
		for (int i = 1; i <= CounselingResult.Max; i++)
		{
			bool flag = CounselingResult.Get(i) != "";
			transform.Find("Result" + i).gameObject.SetActive(flag);
			base.transform.Find("CounselingCanvas/Frame/ShadowGroup/Shadow" + i).gameObject.SetActive(flag);
			if (flag)
			{
				string text = LanguageManager.Get(Data.GetScenarioKey(CounselingResult.GetTitle(i), "型", CounselingResult.Get(i), 0));
				switch (i)
				{
				case 1:
				case 6:
					foreach (int num in new int[]
					{
						2,
						4,
						5
					})
					{
						if (transform.Find(string.Concat(new object[]
						{
							"Result",
							i,
							"/Text",
							num
						})) != null)
						{
							transform.Find(string.Concat(new object[]
							{
								"Result",
								i,
								"/Text",
								num
							})).gameObject.SetActive(false);
						}
					}
					transform.Find(string.Concat(new object[]
					{
						"Result",
						i,
						"/Text",
						text.Length
					})).gameObject.SetActive(true);
					for (int k = 1; k <= text.Length; k++)
					{
						base.transform.Find(string.Concat(new object[]
						{
							"CounselingCanvas/Frame/Result",
							i,
							"/Text",
							text.Length,
							"/",
							k
						})).GetComponent<Text>().text = text[k - 1].ToString();
					}
					break;
				case 2:
					transform.Find("Result" + i + "/Text1").GetComponent<Text>().text = text.Substring(0, 5);
					transform.Find("Result" + i + "/Text2").GetComponent<Text>().text = text.Substring(5, text.Length - 5);
					break;
				case 3:
					for (int l = 1; l <= text.Length; l++)
					{
						transform.Find(string.Concat(new object[]
						{
							"Result",
							i,
							"/Text",
							l
						})).GetComponent<Text>().text = text[l - 1].ToString();
					}
					break;
				case 4:
					transform.Find("Result" + i + "/Text").GetComponent<Text>().text = text;
					break;
				case 5:
					text = text.Insert(4, "\n");
					transform.Find("Result" + i + "/Text").GetComponent<Text>().text = text;
					break;
				case 7:
					foreach (object obj in transform.Find("Result" + i))
					{
						Transform transform2 = (Transform)obj;
						transform2.gameObject.SetActive(transform2.name == "Fragment" || transform2.name == CounselingResult.Get(i));
					}
					break;
				}
			}
		}
	}

	// Token: 0x060000E1 RID: 225 RVA: 0x00009F09 File Offset: 0x00008109
	public SceneLibrary()
	{
	}

	// Token: 0x020000E1 RID: 225
	[CompilerGenerated]
	private sealed class <>c__DisplayClass0_0
	{
		// Token: 0x06000852 RID: 2130 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass0_0()
		{
		}

		// Token: 0x0400034D RID: 845
		public GameObject clickObject;

		// Token: 0x0400034E RID: 846
		public SceneLibrary <>4__this;
	}

	// Token: 0x020000E2 RID: 226
	[CompilerGenerated]
	private sealed class <>c__DisplayClass0_1
	{
		// Token: 0x06000853 RID: 2131 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass0_1()
		{
		}

		// Token: 0x06000854 RID: 2132 RVA: 0x00029BDC File Offset: 0x00027DDC
		internal void <OnClick>b__0(bool active)
		{
			this.CS$<>8__locals1.clickObject.GetComponent<Button>().interactable = active;
			this.CS$<>8__locals1.<>4__this.transform.Find("UICanvas").gameObject.SetActive(active);
			this.CS$<>8__locals1.<>4__this.transform.Find("CounselingCanvas/BackButton").gameObject.SetActive(active);
			Transform transform = GameObject.Find("AppCommon").transform.Find("AdCanvas(Clone)");
			if (transform != null)
			{
				transform.gameObject.SetActive(active);
			}
			if (active)
			{
				this.mirrorFrame.localScale = Vector2.one;
				return;
			}
			float x = this.mirrorFrame.sizeDelta.x;
			if (x > (float)Screen.width)
			{
				this.mirrorFrame.localScale = Vector2.one * ((float)Screen.width / x);
			}
		}

		// Token: 0x06000855 RID: 2133 RVA: 0x00029CCD File Offset: 0x00027ECD
		internal void <OnClick>b__1()
		{
			this.SetUIActive(true);
		}

		// Token: 0x0400034F RID: 847
		public RectTransform mirrorFrame;

		// Token: 0x04000350 RID: 848
		public Action<bool> SetUIActive;

		// Token: 0x04000351 RID: 849
		public SceneLibrary.<>c__DisplayClass0_0 CS$<>8__locals1;
	}

	// Token: 0x020000E3 RID: 227
	[CompilerGenerated]
	private sealed class <>c__DisplayClass1_0
	{
		// Token: 0x06000856 RID: 2134 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass1_0()
		{
		}

		// Token: 0x06000857 RID: 2135 RVA: 0x00029CDB File Offset: 0x00027EDB
		internal void <OnClickBackKey>b__0(Object target)
		{
			this.backButton.transform.parent.gameObject.SetActive(false);
		}

		// Token: 0x04000352 RID: 850
		public GameObject backButton;
	}
}
