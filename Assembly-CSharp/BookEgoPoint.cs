﻿using System;
using System.Collections.Generic;
using System.Text;
using App;

// Token: 0x02000003 RID: 3
public static class BookEgoPoint
{
	// Token: 0x06000008 RID: 8 RVA: 0x000025B5 File Offset: 0x000007B5
	public static EgoPoint GetBookEgo(string book, string type)
	{
		return BookEgoPoint.GetBookEgo(book, type, BookLevel.Get(book));
	}

	// Token: 0x06000009 RID: 9 RVA: 0x000025C4 File Offset: 0x000007C4
	public static EgoPoint GetBookEgo(string book, string type, int page)
	{
		int num = int.Parse(book) - 1;
		if (type == "price")
		{
			return new EgoPoint(Data.BOOKEGO_DATA[num * 2][page + 1]);
		}
		if (!(type == "per_second"))
		{
			if (!(type == "add_per_second"))
			{
				return null;
			}
			return BookEgoPoint.GetBookEgo(book, "per_second", page + 1) - BookEgoPoint.GetBookEgo(book, "per_second", page);
		}
		else
		{
			if (page < 0)
			{
				return new EgoPoint(0f);
			}
			return Data.GetEgoBookPower(new EgoPoint(Data.BOOKEGO_DATA[num * 2 + 1][page]), true);
		}
	}

	// Token: 0x0600000A RID: 10 RVA: 0x00002668 File Offset: 0x00000868
	public static string[] MakeBookEgoListAll()
	{
		List<string> list = new List<string>();
		for (int i = 1; i <= BookLevel.Max; i++)
		{
			string text = i.ToString();
			EgoPoint firstPrice = Data.BOOK_PARAMETER(int.Parse(text))[0];
			EgoPoint firstPerSecond = Data.BOOK_PARAMETER(int.Parse(text))[1];
			string item = BookEgoPoint.MakeBookEgoListEach(text, firstPrice, firstPerSecond);
			list.Add(item);
		}
		return list.ToArray();
	}

	// Token: 0x0600000B RID: 11 RVA: 0x000026C8 File Offset: 0x000008C8
	public static string MakeBookEgoListEach(string book, EgoPoint firstPrice, EgoPoint firstPerSecond)
	{
		int page = BookLevel.GetPage(book, 5);
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = -1; i < page; i++)
		{
			EgoPoint egoPoint = firstPrice;
			int rank = BookLevel.GetRank(book, i);
			for (int j = 2; j <= rank; j++)
			{
				float value = float.Parse(Data.DIC["BOOK_PRICE_RANK" + j][0]);
				egoPoint *= value;
			}
			int currentInRank = BookLevel.GetCurrentInRank(book, i);
			float value2 = float.Parse(Data.DIC["BOOK_PRICE_GAIN"][0]);
			for (int k = 0; k <= currentInRank; k++)
			{
				egoPoint *= value2;
			}
			if (stringBuilder.Length > 0)
			{
				stringBuilder.Append("\t");
			}
			stringBuilder.Append(egoPoint.ToString());
		}
		StringBuilder stringBuilder2 = new StringBuilder();
		for (int l = 0; l <= page; l++)
		{
			EgoPoint egoPoint2 = firstPerSecond;
			float value3 = float.Parse(Data.DIC["BOOK_EARN_GAIN"][0]);
			for (int m = 0; m <= l; m++)
			{
				if (BookEgoPoint.IsRankUp(book, m))
				{
					int rank2 = BookLevel.GetRank(book, m);
					egoPoint2 *= float.Parse(Data.DIC["BOOK_EARN_RANK" + rank2][0]);
				}
				else
				{
					egoPoint2 *= value3;
				}
			}
			if (stringBuilder2.Length > 0)
			{
				stringBuilder2.Append("\t");
			}
			stringBuilder2.Append(egoPoint2.ToString());
		}
		return stringBuilder.ToString() + "\n" + stringBuilder2.ToString();
	}

	// Token: 0x0600000C RID: 12 RVA: 0x00002870 File Offset: 0x00000A70
	public static bool IsRankUp(string book, int page)
	{
		for (int i = 1; i <= 5; i++)
		{
			if (BookLevel.GetPage(book, i) == page)
			{
				return true;
			}
		}
		return false;
	}
}
