﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using App;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// Token: 0x02000035 RID: 53
public class SceneTransition : MonoBehaviour
{
	// Token: 0x06000235 RID: 565 RVA: 0x0001257F File Offset: 0x0001077F
	public static void Transit(Action process, Color? filterColor = null)
	{
		SceneTransition.SetActive(true);
		SceneTransition.Instance.StartCoroutine(SceneTransition.Instance.TransitFromStatic(process, filterColor));
	}

	// Token: 0x06000236 RID: 566 RVA: 0x0001259E File Offset: 0x0001079E
	public static void LoadScene(string scenename, Color? filterColor = null, float time = 0.5f)
	{
		SceneTransition.SetActive(true);
		SceneTransition.Instance.StartCoroutine(SceneTransition.Instance.LoadSceneFromStatic(scenename, filterColor, time));
		GameObject.Find("AppCommon").GetComponent<AudioManager>().OnPreSceneLoaded(scenename);
	}

	// Token: 0x06000237 RID: 567 RVA: 0x000125D3 File Offset: 0x000107D3
	public static void SetActive(bool value)
	{
		SceneTransition.TransitionScreen.SetActive(value);
	}

	// Token: 0x06000238 RID: 568 RVA: 0x000125E0 File Offset: 0x000107E0
	static SceneTransition()
	{
		global::Debug.Log("static SceneTransition");
		SceneTransition.TransitionScreen = Object.Instantiate<GameObject>(Resources.Load<GameObject>("TransitionScreen"));
		SceneTransition.TransitionScreen.name = "TransitionScreen";
		SceneTransition.TransitionScreen.AddComponent<SceneTransition>();
		SceneTransition.Filter = SceneTransition.TransitionScreen.GetComponentInChildren<Image>();
		Object.DontDestroyOnLoad(SceneTransition.TransitionScreen);
		SceneTransition.SetActive(false);
	}

	// Token: 0x06000239 RID: 569 RVA: 0x00012644 File Offset: 0x00010844
	private void Awake()
	{
		SceneTransition.Instance = this;
	}

	// Token: 0x0600023A RID: 570 RVA: 0x0001264C File Offset: 0x0001084C
	private void OnDestroy()
	{
		base.StopAllCoroutines();
	}

	// Token: 0x0600023B RID: 571 RVA: 0x00012654 File Offset: 0x00010854
	private IEnumerator LoadSceneFromStatic(string scenename, Color? filterColor, float time)
	{
		SceneTransition.Filter.color = (filterColor ?? Settings.TRANSITION_COLOR);
		AsyncOperation async = SceneManager.LoadSceneAsync(scenename);
		async.allowSceneActivation = false;
		base.StartCoroutine(AppUtil.FadeIn(SceneTransition.Filter, time, delegate(Object param)
		{
			async.allowSceneActivation = true;
		}));
		float passedTime = 0f;
		while (!async.isDone)
		{
			passedTime += Time.unscaledDeltaTime * Settings.GAME_SPEED;
			yield return new WaitForEndOfFrame();
		}
		yield return null;
		float waitTime = Mathf.Max(1f - passedTime, 0.1f);
		yield return AppUtil.WaitRealtime(waitTime);
		yield return null;
		yield return AppUtil.FadeOut(SceneTransition.Filter, 0.5f, null);
		SceneTransition.SetActive(false);
		global::Debug.Log("AutoTestEvent:スクリーンショット");
		yield break;
	}

	// Token: 0x0600023C RID: 572 RVA: 0x00012678 File Offset: 0x00010878
	private IEnumerator TransitFromStatic(Action process, Color? filterColor)
	{
		SceneTransition.Filter.color = (filterColor ?? Settings.TRANSITION_COLOR);
		yield return AppUtil.FadeIn(SceneTransition.Filter, 0.5f, null);
		process();
		yield return AppUtil.WaitRealtime(0.5f);
		yield return AppUtil.FadeOut(SceneTransition.Filter, 0.5f, null);
		SceneTransition.SetActive(false);
		global::Debug.Log("AutoTestEvent:スクリーンショット");
		yield break;
	}

	// Token: 0x0600023D RID: 573 RVA: 0x000025AD File Offset: 0x000007AD
	public SceneTransition()
	{
	}

	// Token: 0x040000BE RID: 190
	private static SceneTransition Instance;

	// Token: 0x040000BF RID: 191
	private static GameObject TransitionScreen;

	// Token: 0x040000C0 RID: 192
	public static Image Filter;

	// Token: 0x0200012F RID: 303
	[CompilerGenerated]
	private sealed class <>c__DisplayClass9_0
	{
		// Token: 0x0600099E RID: 2462 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass9_0()
		{
		}

		// Token: 0x0600099F RID: 2463 RVA: 0x0002F0C3 File Offset: 0x0002D2C3
		internal void <LoadSceneFromStatic>b__0(Object param)
		{
			this.async.allowSceneActivation = true;
		}

		// Token: 0x040004B2 RID: 1202
		public AsyncOperation async;
	}

	// Token: 0x02000130 RID: 304
	[CompilerGenerated]
	private sealed class <LoadSceneFromStatic>d__9 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060009A0 RID: 2464 RVA: 0x0002F0D1 File Offset: 0x0002D2D1
		[DebuggerHidden]
		public <LoadSceneFromStatic>d__9(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060009A1 RID: 2465 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060009A2 RID: 2466 RVA: 0x0002F0E0 File Offset: 0x0002D2E0
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			SceneTransition sceneTransition = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				CS$<>8__locals1 = new SceneTransition.<>c__DisplayClass9_0();
				SceneTransition.Filter.color = (filterColor ?? Settings.TRANSITION_COLOR);
				CS$<>8__locals1.async = SceneManager.LoadSceneAsync(scenename);
				CS$<>8__locals1.async.allowSceneActivation = false;
				sceneTransition.StartCoroutine(AppUtil.FadeIn(SceneTransition.Filter, time, new Action<Object>(CS$<>8__locals1.<LoadSceneFromStatic>b__0)));
				passedTime = 0f;
				break;
			case 1:
				this.<>1__state = -1;
				break;
			case 2:
			{
				this.<>1__state = -1;
				float waitTime = Mathf.Max(1f - passedTime, 0.1f);
				this.<>2__current = AppUtil.WaitRealtime(waitTime);
				this.<>1__state = 3;
				return true;
			}
			case 3:
				this.<>1__state = -1;
				this.<>2__current = null;
				this.<>1__state = 4;
				return true;
			case 4:
				this.<>1__state = -1;
				this.<>2__current = AppUtil.FadeOut(SceneTransition.Filter, 0.5f, null);
				this.<>1__state = 5;
				return true;
			case 5:
				this.<>1__state = -1;
				SceneTransition.SetActive(false);
				global::Debug.Log("AutoTestEvent:スクリーンショット");
				return false;
			default:
				return false;
			}
			if (CS$<>8__locals1.async.isDone)
			{
				this.<>2__current = null;
				this.<>1__state = 2;
				return true;
			}
			passedTime += Time.unscaledDeltaTime * Settings.GAME_SPEED;
			this.<>2__current = new WaitForEndOfFrame();
			this.<>1__state = 1;
			return true;
		}

		// Token: 0x1700014C RID: 332
		// (get) Token: 0x060009A3 RID: 2467 RVA: 0x0002F28E File Offset: 0x0002D48E
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060009A4 RID: 2468 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x1700014D RID: 333
		// (get) Token: 0x060009A5 RID: 2469 RVA: 0x0002F28E File Offset: 0x0002D48E
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040004B3 RID: 1203
		private int <>1__state;

		// Token: 0x040004B4 RID: 1204
		private object <>2__current;

		// Token: 0x040004B5 RID: 1205
		public Color? filterColor;

		// Token: 0x040004B6 RID: 1206
		public string scenename;

		// Token: 0x040004B7 RID: 1207
		public SceneTransition <>4__this;

		// Token: 0x040004B8 RID: 1208
		public float time;

		// Token: 0x040004B9 RID: 1209
		private SceneTransition.<>c__DisplayClass9_0 <>8__1;

		// Token: 0x040004BA RID: 1210
		private float <passedTime>5__2;
	}

	// Token: 0x02000131 RID: 305
	[CompilerGenerated]
	private sealed class <TransitFromStatic>d__10 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060009A6 RID: 2470 RVA: 0x0002F296 File Offset: 0x0002D496
		[DebuggerHidden]
		public <TransitFromStatic>d__10(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060009A7 RID: 2471 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060009A8 RID: 2472 RVA: 0x0002F2A8 File Offset: 0x0002D4A8
		bool IEnumerator.MoveNext()
		{
			switch (this.<>1__state)
			{
			case 0:
				this.<>1__state = -1;
				SceneTransition.Filter.color = (filterColor ?? Settings.TRANSITION_COLOR);
				this.<>2__current = AppUtil.FadeIn(SceneTransition.Filter, 0.5f, null);
				this.<>1__state = 1;
				return true;
			case 1:
				this.<>1__state = -1;
				process();
				this.<>2__current = AppUtil.WaitRealtime(0.5f);
				this.<>1__state = 2;
				return true;
			case 2:
				this.<>1__state = -1;
				this.<>2__current = AppUtil.FadeOut(SceneTransition.Filter, 0.5f, null);
				this.<>1__state = 3;
				return true;
			case 3:
				this.<>1__state = -1;
				SceneTransition.SetActive(false);
				global::Debug.Log("AutoTestEvent:スクリーンショット");
				return false;
			default:
				return false;
			}
		}

		// Token: 0x1700014E RID: 334
		// (get) Token: 0x060009A9 RID: 2473 RVA: 0x0002F38B File Offset: 0x0002D58B
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060009AA RID: 2474 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x1700014F RID: 335
		// (get) Token: 0x060009AB RID: 2475 RVA: 0x0002F38B File Offset: 0x0002D58B
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040004BB RID: 1211
		private int <>1__state;

		// Token: 0x040004BC RID: 1212
		private object <>2__current;

		// Token: 0x040004BD RID: 1213
		public Color? filterColor;

		// Token: 0x040004BE RID: 1214
		public Action process;
	}
}
