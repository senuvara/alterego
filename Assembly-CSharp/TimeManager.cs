﻿using System;
using App;
using UnityEngine;

// Token: 0x02000022 RID: 34
[AppUtil.DataRangeAttribute]
[AppUtil.TitleAttribute("時間リスト")]
public class TimeManager : MonoBehaviour
{
	// Token: 0x1700000D RID: 13
	// (get) Token: 0x06000115 RID: 277 RVA: 0x0000CC4C File Offset: 0x0000AE4C
	// (set) Token: 0x06000116 RID: 278 RVA: 0x0000CC67 File Offset: 0x0000AE67
	private static DateTime LastUpdate
	{
		get
		{
			return PlayerPrefs.GetString("LastUpdate", DateTime.UtcNow.ToBinaryString()).Convert2DateTime();
		}
		set
		{
			PlayerPrefs.SetString("LastUpdate", value.ToBinaryString());
		}
	}

	// Token: 0x1700000E RID: 14
	// (get) Token: 0x06000117 RID: 279 RVA: 0x0000CC7C File Offset: 0x0000AE7C
	// (set) Token: 0x06000118 RID: 280 RVA: 0x0000CCA6 File Offset: 0x0000AEA6
	private static DateTime LastLogin
	{
		get
		{
			return PlayerPrefs.GetString("LastLogin", default(DateTime).ToBinaryString()).Convert2DateTime();
		}
		set
		{
			PlayerPrefs.SetString("LastLogin", value.ToBinaryString());
		}
	}

	// Token: 0x1700000F RID: 15
	// (get) Token: 0x06000119 RID: 281 RVA: 0x00007AD6 File Offset: 0x00005CD6
	public static int Min
	{
		get
		{
			return 0;
		}
	}

	// Token: 0x17000010 RID: 16
	// (get) Token: 0x0600011A RID: 282 RVA: 0x0000CCB8 File Offset: 0x0000AEB8
	public static int Max
	{
		get
		{
			return Enum.GetNames(typeof(TimeManager.TYPE)).Length - 1;
		}
	}

	// Token: 0x0600011B RID: 283 RVA: 0x0000CCCD File Offset: 0x0000AECD
	public static TimeManager.TYPE GetType(string type)
	{
		if (type == "")
		{
			return TimeManager.TYPE.REAL_TOTAL;
		}
		return (TimeManager.TYPE)Enum.Parse(typeof(TimeManager.TYPE), type);
	}

	// Token: 0x0600011C RID: 284 RVA: 0x0000CCF4 File Offset: 0x0000AEF4
	public static string GetTitle(int index)
	{
		return ((TimeManager.TYPE)Enum.GetValues(typeof(TimeManager.TYPE)).GetValue(index)).ToString();
	}

	// Token: 0x0600011D RID: 285 RVA: 0x0000CD29 File Offset: 0x0000AF29
	public static string GetKey(TimeManager.TYPE type)
	{
		return "Time:" + type.ToString();
	}

	// Token: 0x0600011E RID: 286 RVA: 0x0000CD42 File Offset: 0x0000AF42
	public static TimeSpan Get(string type)
	{
		return TimeManager.Get(TimeManager.GetType(type));
	}

	// Token: 0x0600011F RID: 287 RVA: 0x0000CD50 File Offset: 0x0000AF50
	public static TimeSpan Get(TimeManager.TYPE type)
	{
		TimeSpan timeSpan = TimeSpan.Zero;
		if (type.ToString().Contains("NEXT_"))
		{
			timeSpan = TimeSpan.MaxValue;
		}
		return TimeSpan.Parse(PlayerPrefs.GetString(TimeManager.GetKey(type), timeSpan.ToString()));
	}

	// Token: 0x06000120 RID: 288 RVA: 0x0000CD9F File Offset: 0x0000AF9F
	public static void Set(string type, TimeSpan value)
	{
		TimeManager.Set(TimeManager.GetType(type), value);
	}

	// Token: 0x06000121 RID: 289 RVA: 0x0000CDAD File Offset: 0x0000AFAD
	public static void Reset(TimeManager.TYPE type)
	{
		TimeManager.Reset(type, TimeSpan.Zero);
	}

	// Token: 0x06000122 RID: 290 RVA: 0x0000CDBA File Offset: 0x0000AFBA
	public static void Reset(TimeManager.TYPE type, TimeSpan offset)
	{
		TimeManager.Rebase();
		TimeManager.Set(type, TimeManager.Now + offset);
	}

	// Token: 0x06000123 RID: 291 RVA: 0x0000CDD2 File Offset: 0x0000AFD2
	public static void Set(TimeManager.TYPE type, TimeSpan value)
	{
		if (Settings.GAME_SPEED != 1f)
		{
			value = new TimeSpan(value.Ticks / (long)Settings.GAME_SPEED);
		}
		PlayerPrefs.SetString(TimeManager.GetKey(type), value.ToString());
	}

	// Token: 0x06000124 RID: 292 RVA: 0x0000CE0D File Offset: 0x0000B00D
	public static void Add(TimeManager.TYPE type, TimeSpan add)
	{
		TimeManager.Set(type, TimeManager.Get(type) + add);
	}

	// Token: 0x06000125 RID: 293 RVA: 0x0000CE24 File Offset: 0x0000B024
	private void Awake()
	{
		if (PlayerPrefs.HasKey("LastEgoTime"))
		{
			TimeManager.LastUpdate = PlayerPrefs.GetString("LastEgoTime").Convert2DateTime().ToUniversalTime();
			PlayerPrefs.DeleteKey("LastEgoTime");
		}
		DateTime utcNow = DateTime.UtcNow;
		TimeManager.Add(TimeManager.TYPE.REAL_TOTAL, utcNow - TimeManager.LastUpdate);
		TimeManager.LastUpdate = utcNow;
		TimeManager.UpdateLoginDate();
	}

	// Token: 0x06000126 RID: 294 RVA: 0x0000CE85 File Offset: 0x0000B085
	private void FixedUpdate()
	{
		TimeManager.Rebase();
	}

	// Token: 0x06000127 RID: 295 RVA: 0x0000CE8C File Offset: 0x0000B08C
	private static void Rebase()
	{
		DateTime utcNow = DateTime.UtcNow;
		TimeSpan add = utcNow - TimeManager.LastUpdate;
		TimeManager.Add(TimeManager.TYPE.GAME_TOTAL, add);
		TimeManager.Add(TimeManager.TYPE.REAL_TOTAL, add);
		TimeManager.LastUpdate = utcNow;
	}

	// Token: 0x06000128 RID: 296 RVA: 0x0000CEBD File Offset: 0x0000B0BD
	public static TimeSpan GetGapTime(TimeManager.TYPE type)
	{
		return TimeManager.Get(type) - TimeManager.Now;
	}

	// Token: 0x06000129 RID: 297 RVA: 0x0000CED0 File Offset: 0x0000B0D0
	public static float SetLastTime(TimeManager.TYPE type)
	{
		float result = (float)(TimeManager.Now - TimeManager.Get(type)).TotalSeconds;
		TimeManager.Set(type, TimeManager.Now);
		return result;
	}

	// Token: 0x0600012A RID: 298 RVA: 0x0000CF01 File Offset: 0x0000B101
	public static bool IsOverTime(TimeManager.TYPE type)
	{
		return TimeManager.Now > TimeManager.Get(type);
	}

	// Token: 0x0600012B RID: 299 RVA: 0x0000CF13 File Offset: 0x0000B113
	public static bool IsInTime(TimeManager.TYPE type)
	{
		return TimeManager.Now < TimeManager.Get(type);
	}

	// Token: 0x17000011 RID: 17
	// (get) Token: 0x0600012C RID: 300 RVA: 0x0000CF25 File Offset: 0x0000B125
	public static TimeSpan Now
	{
		get
		{
			return TimeManager.Get(TimeManager.TYPE.REAL_TOTAL);
		}
	}

	// Token: 0x0600012D RID: 301 RVA: 0x0000CF30 File Offset: 0x0000B130
	public static void UpdateLoginDate()
	{
		DateTime now = DateTime.Now;
		if ((now.Date - TimeManager.LastLogin.Date).Days > 0)
		{
			PlayerResult.LoginCount++;
			if (PlayerStatus.ScenarioNo.Contains("4章AE"))
			{
				PlayerStatus.EnableDailyBonus = true;
			}
			AnalyticsManager.SendEvent(new string[]
			{
				"ログイン",
				PlayerResult.LoginCount + "日目",
				""
			}, "1DLWkIkaBFBnrV9VoNi09qwvOhocUADC8BWVHfNzyjfA");
		}
		TimeManager.LastLogin = now;
	}

	// Token: 0x0600012E RID: 302 RVA: 0x000025AD File Offset: 0x000007AD
	public TimeManager()
	{
	}

	// Token: 0x020000FD RID: 253
	public enum TYPE
	{
		// Token: 0x040003C8 RID: 968
		GAME_TOTAL,
		// Token: 0x040003C9 RID: 969
		REAL_TOTAL,
		// Token: 0x040003CA RID: 970
		LAST_LOGIN,
		// Token: 0x040003CB RID: 971
		LAST_EGO,
		// Token: 0x040003CC RID: 972
		NEXT_BONUS_BOOK,
		// Token: 0x040003CD RID: 973
		NEXT_BONUS_ES,
		// Token: 0x040003CE RID: 974
		END_BONUS,
		// Token: 0x040003CF RID: 975
		END_BOOK
	}
}
