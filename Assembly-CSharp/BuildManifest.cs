﻿using System;
using UnityEngine;

// Token: 0x02000026 RID: 38
[Serializable]
public class BuildManifest
{
	// Token: 0x06000148 RID: 328 RVA: 0x0000E8C0 File Offset: 0x0000CAC0
	public static BuildManifest Load()
	{
		TextAsset textAsset = Resources.Load<TextAsset>("UnityCloudBuildManifest.json");
		if (!(textAsset == null))
		{
			return JsonUtility.FromJson<BuildManifest>(textAsset.text);
		}
		textAsset = Resources.Load<TextAsset>("UnityCloudBuildManifestLocal.json");
		if (textAsset == null)
		{
			return new BuildManifest();
		}
		return JsonUtility.FromJson<BuildManifest>(textAsset.text);
	}

	// Token: 0x17000016 RID: 22
	// (get) Token: 0x06000149 RID: 329 RVA: 0x0000E912 File Offset: 0x0000CB12
	public string ScmCommitId
	{
		get
		{
			return this.scmCommitId;
		}
	}

	// Token: 0x17000017 RID: 23
	// (get) Token: 0x0600014A RID: 330 RVA: 0x0000E91A File Offset: 0x0000CB1A
	public string ScmBranch
	{
		get
		{
			return this.scmBranch;
		}
	}

	// Token: 0x17000018 RID: 24
	// (get) Token: 0x0600014B RID: 331 RVA: 0x0000E922 File Offset: 0x0000CB22
	public string BuildNumber
	{
		get
		{
			return this.buildNumber;
		}
	}

	// Token: 0x17000019 RID: 25
	// (get) Token: 0x0600014C RID: 332 RVA: 0x0000E92A File Offset: 0x0000CB2A
	public string BuildStartTime
	{
		get
		{
			return this.buildStartTime;
		}
	}

	// Token: 0x1700001A RID: 26
	// (get) Token: 0x0600014D RID: 333 RVA: 0x0000E932 File Offset: 0x0000CB32
	public string ProjectId
	{
		get
		{
			return this.projectId;
		}
	}

	// Token: 0x1700001B RID: 27
	// (get) Token: 0x0600014E RID: 334 RVA: 0x0000E93A File Offset: 0x0000CB3A
	public string BundleId
	{
		get
		{
			return this.bundleId;
		}
	}

	// Token: 0x1700001C RID: 28
	// (get) Token: 0x0600014F RID: 335 RVA: 0x0000E942 File Offset: 0x0000CB42
	public string UnityVersion
	{
		get
		{
			return this.unityVersion;
		}
	}

	// Token: 0x1700001D RID: 29
	// (get) Token: 0x06000150 RID: 336 RVA: 0x0000E94A File Offset: 0x0000CB4A
	public string XCodeVersion
	{
		get
		{
			return this.xcodeVersion;
		}
	}

	// Token: 0x1700001E RID: 30
	// (get) Token: 0x06000151 RID: 337 RVA: 0x0000E952 File Offset: 0x0000CB52
	public string CloudBuildTargetName
	{
		get
		{
			return this.cloudBuildTargetName;
		}
	}

	// Token: 0x06000152 RID: 338 RVA: 0x0000E95C File Offset: 0x0000CB5C
	public BuildManifest()
	{
	}

	// Token: 0x04000090 RID: 144
	[SerializeField]
	private string scmCommitId;

	// Token: 0x04000091 RID: 145
	[SerializeField]
	private string scmBranch;

	// Token: 0x04000092 RID: 146
	[SerializeField]
	private string buildNumber;

	// Token: 0x04000093 RID: 147
	[SerializeField]
	private string buildStartTime = DateTime.Now.ToUniversalTime().ToString();

	// Token: 0x04000094 RID: 148
	[SerializeField]
	private string projectId;

	// Token: 0x04000095 RID: 149
	[SerializeField]
	private string bundleId;

	// Token: 0x04000096 RID: 150
	[SerializeField]
	private string unityVersion;

	// Token: 0x04000097 RID: 151
	[SerializeField]
	private string xcodeVersion;

	// Token: 0x04000098 RID: 152
	[SerializeField]
	private string cloudBuildTargetName;
}
