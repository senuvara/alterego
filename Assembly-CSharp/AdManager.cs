﻿using System;
using System.Collections.Generic;
using System.Linq;
using App;
using UnityEngine;

// Token: 0x0200003F RID: 63
public class AdManager : MonoBehaviour
{
	// Token: 0x060002A7 RID: 679 RVA: 0x00014288 File Offset: 0x00012488
	private void Awake()
	{
		AdManager.Instance = this;
		this.IsActive = true;
		AdManager.SetActive(AdInfo.ENABLE);
		this.AndroidUtil = new AndroidJavaObject("com.caracolu.appcommon.Util", Array.Empty<object>());
		this.AndroidUtil.Call("SetUIChangeListener", Array.Empty<object>());
	}

	// Token: 0x060002A8 RID: 680 RVA: 0x000142D8 File Offset: 0x000124D8
	public static void SetActive(bool active)
	{
		global::Debug.Log("AdManager#SetActive " + active.ToString());
		foreach (AdModule adModule in AdManager.Instance.AdList.Values)
		{
			adModule.Destroy();
		}
		AdManager.Instance.AdList.Clear();
		Object.Destroy(AdManager.Instance.BannerBG);
		AdManager.Instance.BannerBG = null;
		if (active)
		{
			AdManager.Instance.RestoreModule();
		}
		else
		{
			AdManager.Instance.AddModule("SkipReward", null);
		}
		SafeAreaAdjuster.UpdateLayout();
	}

	// Token: 0x060002A9 RID: 681 RVA: 0x00014390 File Offset: 0x00012590
	private void RestoreModule()
	{
		foreach (string key in AdInfo.AD_ID.Keys)
		{
			this.AddModule(key, AdInfo.AD_ID[key]);
		}
		GameObject gameObject = Resources.Load<GameObject>("AdCanvas");
		if (gameObject != null)
		{
			this.BannerBG = Object.Instantiate<GameObject>(gameObject, base.transform);
			this.BannerBG.SetActive(false);
		}
	}

	// Token: 0x060002AA RID: 682 RVA: 0x00014420 File Offset: 0x00012620
	private void AddModule(string key, string[] values)
	{
		if (this.AdList.ContainsKey(key))
		{
			this.AdList[key].Destroy();
			this.AdList[key].OnAdEvent -= this.OnAdEvent;
			this.AdList.Remove(key);
		}
		AdModule adModule;
		if (key.Contains("UnityAds"))
		{
			adModule = new AdModuleUnityAds();
		}
		else
		{
			adModule = new AdModule(this, key, values);
		}
		this.AdList.Add(key, adModule);
		adModule.OnAdEvent += this.OnAdEvent;
	}

	// Token: 0x060002AB RID: 683 RVA: 0x000144B4 File Offset: 0x000126B4
	private void OnAdEvent(object sender, AdModule.AdEventArgs args)
	{
		global::Debug.Log(string.Concat(new string[]
		{
			"AdManager#OnEvent(",
			args.Type,
			" ",
			args.Message,
			")"
		}));
		if (!this.IsActive)
		{
			this.SuspendEvent.Add(args);
			return;
		}
		string message = args.Message;
		uint num = <PrivateImplementationDetails>.ComputeStringHash(message);
		if (num <= 1215464327U)
		{
			if (num != 287701475U)
			{
				if (num == 489681732U)
				{
					message == "OnFailed(Play)";
					return;
				}
				if (num != 1215464327U)
				{
					return;
				}
				if (!(message == "OnChangedSafeInsetArea"))
				{
					return;
				}
				SafeAreaAdjuster.SafeAreaHeight = this.AndroidUtil.GetStatic<int>("sSafeInsetHeight");
				foreach (string text in this.AdList.Keys.ToArray<string>())
				{
					if (text.Contains("Rectangle"))
					{
						this.AddModule(text, AdInfo.AD_ID[text]);
					}
				}
				return;
			}
			else if (!(message == "NavigationBar:NotVisible"))
			{
				return;
			}
		}
		else if (num <= 1300762695U)
		{
			if (num == 1299922320U)
			{
				message == "OnClosed";
				return;
			}
			if (num != 1300762695U)
			{
				return;
			}
			if (!(message == "OnReward"))
			{
				return;
			}
			this.AppCallback(true);
			return;
		}
		else if (num != 1600958328U)
		{
			if (num != 4059217573U)
			{
				return;
			}
			if (!(message == "NoReward"))
			{
				return;
			}
			this.AppCallback(false);
			return;
		}
		else if (!(message == "NavigationBar:IsVisible"))
		{
			return;
		}
		if (this.AdList.ContainsKey("AdMobBanner") && this.AdList["AdMobBanner"].IsShowing)
		{
			this.AdList["AdMobBanner"].Hide();
			this.AdList["AdMobBanner"].Show();
			SafeAreaAdjuster.UpdateLayout();
			return;
		}
	}

	// Token: 0x060002AC RID: 684 RVA: 0x000146A0 File Offset: 0x000128A0
	private void OnApplicationPause(bool pause)
	{
		global::Debug.Log("AdManager#OnApplicationPause " + pause.ToString());
		this.IsActive = !pause;
		this.AndroidUtil.SetStatic<bool>("sIsActive", this.IsActive);
		if (this.IsActive)
		{
			foreach (AdModule.AdEventArgs args in this.SuspendEvent)
			{
				this.OnAdEvent(null, args);
			}
			this.SuspendEvent.Clear();
			SafeAreaAdjuster.UpdateLayout();
		}
	}

	// Token: 0x060002AD RID: 685 RVA: 0x00014744 File Offset: 0x00012944
	private void OnDestroy()
	{
		foreach (AdModule adModule in this.AdList.Values)
		{
			adModule.OnAdEvent -= this.OnAdEvent;
			adModule.Destroy();
		}
		AdManager.Instance = null;
		this.AndroidUtil.Dispose();
	}

	// Token: 0x060002AE RID: 686 RVA: 0x000147B8 File Offset: 0x000129B8
	public static bool IsReady(string type)
	{
		if (AdManager.Instance == null)
		{
			return false;
		}
		foreach (string text in AdManager.Instance.AdList.Keys)
		{
			if (text.Contains(type) && AdManager.Instance.AdList[text].IsReady())
			{
				return true;
			}
		}
		return false;
	}

	// Token: 0x060002AF RID: 687 RVA: 0x00014840 File Offset: 0x00012A40
	public static void Show(string type, Action<bool> callback = null)
	{
		if (AdManager.Instance == null)
		{
			return;
		}
		AdManager.Instance.AppCallback = callback;
		foreach (string text in AdManager.Instance.AdList.Keys)
		{
			if (text.Contains(type))
			{
				AdModule adModule = AdManager.Instance.AdList[text];
				if (!type.Contains("Reward") || adModule.IsReady())
				{
					adModule.Show();
					break;
				}
			}
		}
		if (AdManager.Instance.BannerBG == null)
		{
			return;
		}
		if (type.Contains("Banner"))
		{
			AdManager.Instance.BannerBG.SetActive(true);
		}
	}

	// Token: 0x060002B0 RID: 688 RVA: 0x00014910 File Offset: 0x00012B10
	public static void Hide(string type)
	{
		if (AdManager.Instance == null)
		{
			return;
		}
		foreach (string text in AdManager.Instance.AdList.Keys)
		{
			if (text.Contains(type))
			{
				AdManager.Instance.AdList[text].Hide();
				break;
			}
		}
		if (AdManager.Instance.BannerBG == null)
		{
			return;
		}
		if (type.Contains("Banner"))
		{
			AdManager.Instance.BannerBG.SetActive(false);
		}
	}

	// Token: 0x060002B1 RID: 689 RVA: 0x000149C0 File Offset: 0x00012BC0
	public void OnEventFromAndroid(string message)
	{
		this.OnAdEvent(null, new AdModule.AdEventArgs
		{
			Type = "AndroidEvent",
			Message = message
		});
	}

	// Token: 0x060002B2 RID: 690 RVA: 0x000149ED File Offset: 0x00012BED
	public AdManager()
	{
	}

	// Token: 0x040000EC RID: 236
	private static AdManager Instance;

	// Token: 0x040000ED RID: 237
	private bool IsActive;

	// Token: 0x040000EE RID: 238
	private GameObject BannerBG;

	// Token: 0x040000EF RID: 239
	private AndroidJavaObject AndroidUtil;

	// Token: 0x040000F0 RID: 240
	private List<AdModule.AdEventArgs> SuspendEvent = new List<AdModule.AdEventArgs>();

	// Token: 0x040000F1 RID: 241
	private IDictionary<string, AdModule> AdList = new Dictionary<string, AdModule>();

	// Token: 0x040000F2 RID: 242
	private Action<bool> AppCallback;
}
