﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using App;
using NCMB;
using UnityEngine;

// Token: 0x0200001A RID: 26
public class SaveDataManager
{
	// Token: 0x17000006 RID: 6
	// (get) Token: 0x060000AF RID: 175 RVA: 0x0000936D File Offset: 0x0000756D
	// (set) Token: 0x060000B0 RID: 176 RVA: 0x00009398 File Offset: 0x00007598
	public static DateTime SaveDataTime
	{
		get
		{
			if (PlayerPrefs.HasKey("SaveDataTime"))
			{
				return DateTime.FromBinary(Convert.ToInt64(PlayerPrefs.GetString("SaveDataTime")));
			}
			return DateTime.MinValue;
		}
		set
		{
			PlayerPrefs.SetString("SaveDataTime", value.ToBinary().ToString());
		}
	}

	// Token: 0x060000B1 RID: 177 RVA: 0x000093BE File Offset: 0x000075BE
	public static IEnumerator SetUserID()
	{
		NCMBObject ncmbData = new NCMBObject("SaveData");
		string resultMessage = "";
		try
		{
			ncmbData.SaveAsync(delegate(NCMBException e)
			{
				if (e == null)
				{
					Settings.SaveDataID = ncmbData.ObjectId;
					resultMessage = "SUCCESS";
					return;
				}
				resultMessage = "ERROR";
			});
		}
		catch (Exception ex)
		{
			global::Debug.Log("NCMB:SaveAsync " + ex.ToString());
			resultMessage = "ERROR";
		}
		yield return new WaitUntil(() => resultMessage != "");
		yield break;
	}

	// Token: 0x060000B2 RID: 178 RVA: 0x000093C6 File Offset: 0x000075C6
	public static IEnumerator Save(Action<string> callback)
	{
		string resultMessage = "";
		NCMBObject saveData = new NCMBObject("SaveData");
		if (string.IsNullOrEmpty(Settings.SaveDataID))
		{
			resultMessage = "SUCCESS";
		}
		else
		{
			saveData.ObjectId = Settings.SaveDataID;
			try
			{
				saveData.FetchAsync(delegate(NCMBException e)
				{
					resultMessage = "SUCCESS";
					if (e != null)
					{
						saveData.ObjectId = null;
					}
				});
			}
			catch (Exception ex)
			{
				global::Debug.LogError("NCMB:FetchAsync " + ex.ToString());
				resultMessage = "ERROR";
			}
		}
		yield return new WaitUntil(() => resultMessage != "");
		if (resultMessage == "SUCCESS")
		{
			yield return SaveDataManager.SaveToNCMB(saveData, delegate(string ret)
			{
				resultMessage = ret;
			});
		}
		callback(resultMessage);
		yield break;
	}

	// Token: 0x060000B3 RID: 179 RVA: 0x000093D5 File Offset: 0x000075D5
	private static IEnumerator SaveToNCMB(NCMBObject target, Action<string> callback)
	{
		string resultMessage = "";
		string[] statusStringArray = StringStatusConverter.GetStatusStringArray(SaveDataInfo.TypeList, false, false);
		string text = "UserID\t" + statusStringArray[2];
		string text2 = AnalyticsManager.GetUserID() + "\t" + statusStringArray[1];
		string[] array = text.Split(new char[]
		{
			'\t'
		});
		string[] array2 = text2.Split(new char[]
		{
			'\t'
		});
		for (int i = 0; i < array.Length - 1; i++)
		{
			target[array[i]] = array2[i];
		}
		target["LoadEnable"] = true;
		try
		{
			target.SaveAsync(delegate(NCMBException e)
			{
				if (e == null)
				{
					Settings.SaveDataID = target.ObjectId;
					SaveDataManager.SaveDataTime = (target.UpdateDate ?? DateTime.MinValue);
					resultMessage = "SUCCESS";
					return;
				}
				resultMessage = "ERROR";
			});
		}
		catch (Exception ex)
		{
			global::Debug.LogError("NCMB:SaveAsync " + ex.ToString());
			resultMessage = "ERROR";
		}
		yield return new WaitUntil(() => resultMessage != "");
		callback(resultMessage);
		yield break;
	}

	// Token: 0x060000B4 RID: 180 RVA: 0x000093EB File Offset: 0x000075EB
	public static IEnumerator Load(string loadID, Action<bool> callback)
	{
		string resultMessage = "";
		NCMBObject loadData = new NCMBObject("SaveData");
		loadData.ObjectId = loadID;
		try
		{
			loadData.FetchAsync(delegate(NCMBException e)
			{
				if (e != null)
				{
					resultMessage = "ERROR";
					return;
				}
				if (loadData.ContainsKey("LoadEnable") && (bool)loadData["LoadEnable"])
				{
					resultMessage = "SUCCESS";
					return;
				}
				resultMessage = "ERROR";
			});
		}
		catch (Exception ex)
		{
			global::Debug.LogError("NCMB:FetchAsync " + ex.ToString());
			resultMessage = "ERROR";
		}
		yield return new WaitUntil(() => resultMessage != "");
		if (resultMessage == "ERROR")
		{
			callback(false);
			yield break;
		}
		foreach (string text in loadData.Keys)
		{
			if (Array.IndexOf<string>(SaveDataManager.EXCEPTION_KEY, text) < 0)
			{
				StringStatusConverter.SetEachProperty(text, (string)loadData[text]);
			}
		}
		if (loadData.ContainsKey("IsInfinity") && bool.Parse((string)loadData["IsInfinity"]))
		{
			callback(true);
			yield break;
		}
		loadData["LoadEnable"] = false;
		try
		{
			loadData.SaveAsync();
		}
		catch (Exception ex2)
		{
			global::Debug.LogError("NCMB:SaveAsync " + ex2.ToString());
		}
		callback(true);
		yield break;
	}

	// Token: 0x060000B5 RID: 181 RVA: 0x000043CE File Offset: 0x000025CE
	public SaveDataManager()
	{
	}

	// Token: 0x060000B6 RID: 182 RVA: 0x00009401 File Offset: 0x00007601
	// Note: this type is marked as 'beforefieldinit'.
	static SaveDataManager()
	{
	}

	// Token: 0x04000030 RID: 48
	private const string CLASS_NAME = "SaveData";

	// Token: 0x04000031 RID: 49
	private static readonly string[] EXCEPTION_KEY = new string[]
	{
		"acl",
		"objectId",
		"createDate",
		"updateDate",
		"LoadEnable",
		"IsInfinity"
	};

	// Token: 0x020000CF RID: 207
	[CompilerGenerated]
	private sealed class <>c__DisplayClass5_0
	{
		// Token: 0x06000803 RID: 2051 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass5_0()
		{
		}

		// Token: 0x06000804 RID: 2052 RVA: 0x00027E84 File Offset: 0x00026084
		internal void <SetUserID>b__0(NCMBException e)
		{
			if (e == null)
			{
				Settings.SaveDataID = this.ncmbData.ObjectId;
				this.resultMessage = "SUCCESS";
				return;
			}
			this.resultMessage = "ERROR";
		}

		// Token: 0x06000805 RID: 2053 RVA: 0x00027EB0 File Offset: 0x000260B0
		internal bool <SetUserID>b__1()
		{
			return this.resultMessage != "";
		}

		// Token: 0x040002F7 RID: 759
		public NCMBObject ncmbData;

		// Token: 0x040002F8 RID: 760
		public string resultMessage;
	}

	// Token: 0x020000D0 RID: 208
	[CompilerGenerated]
	private sealed class <SetUserID>d__5 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000806 RID: 2054 RVA: 0x00027EC2 File Offset: 0x000260C2
		[DebuggerHidden]
		public <SetUserID>d__5(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000807 RID: 2055 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000808 RID: 2056 RVA: 0x00027ED4 File Offset: 0x000260D4
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			if (num == 0)
			{
				this.<>1__state = -1;
				SaveDataManager.<>c__DisplayClass5_0 CS$<>8__locals1 = new SaveDataManager.<>c__DisplayClass5_0();
				CS$<>8__locals1.ncmbData = new NCMBObject("SaveData");
				CS$<>8__locals1.resultMessage = "";
				try
				{
					CS$<>8__locals1.ncmbData.SaveAsync(new NCMBCallback(CS$<>8__locals1.<SetUserID>b__0));
				}
				catch (Exception ex)
				{
					global::Debug.Log("NCMB:SaveAsync " + ex.ToString());
					CS$<>8__locals1.resultMessage = "ERROR";
				}
				this.<>2__current = new WaitUntil(new Func<bool>(CS$<>8__locals1.<SetUserID>b__1));
				this.<>1__state = 1;
				return true;
			}
			if (num != 1)
			{
				return false;
			}
			this.<>1__state = -1;
			return false;
		}

		// Token: 0x170000E2 RID: 226
		// (get) Token: 0x06000809 RID: 2057 RVA: 0x00027F90 File Offset: 0x00026190
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0600080A RID: 2058 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000E3 RID: 227
		// (get) Token: 0x0600080B RID: 2059 RVA: 0x00027F90 File Offset: 0x00026190
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040002F9 RID: 761
		private int <>1__state;

		// Token: 0x040002FA RID: 762
		private object <>2__current;
	}

	// Token: 0x020000D1 RID: 209
	[CompilerGenerated]
	private sealed class <>c__DisplayClass6_0
	{
		// Token: 0x0600080C RID: 2060 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass6_0()
		{
		}

		// Token: 0x0600080D RID: 2061 RVA: 0x00027F98 File Offset: 0x00026198
		internal void <Save>b__0(NCMBException e)
		{
			this.resultMessage = "SUCCESS";
			if (e != null)
			{
				this.saveData.ObjectId = null;
			}
		}

		// Token: 0x0600080E RID: 2062 RVA: 0x00027FB4 File Offset: 0x000261B4
		internal bool <Save>b__1()
		{
			return this.resultMessage != "";
		}

		// Token: 0x0600080F RID: 2063 RVA: 0x00027FC6 File Offset: 0x000261C6
		internal void <Save>b__2(string ret)
		{
			this.resultMessage = ret;
		}

		// Token: 0x040002FB RID: 763
		public string resultMessage;

		// Token: 0x040002FC RID: 764
		public NCMBObject saveData;
	}

	// Token: 0x020000D2 RID: 210
	[CompilerGenerated]
	private sealed class <Save>d__6 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000810 RID: 2064 RVA: 0x00027FCF File Offset: 0x000261CF
		[DebuggerHidden]
		public <Save>d__6(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000811 RID: 2065 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000812 RID: 2066 RVA: 0x00027FE0 File Offset: 0x000261E0
		bool IEnumerator.MoveNext()
		{
			switch (this.<>1__state)
			{
			case 0:
				this.<>1__state = -1;
				CS$<>8__locals1 = new SaveDataManager.<>c__DisplayClass6_0();
				CS$<>8__locals1.resultMessage = "";
				CS$<>8__locals1.saveData = new NCMBObject("SaveData");
				if (string.IsNullOrEmpty(Settings.SaveDataID))
				{
					CS$<>8__locals1.resultMessage = "SUCCESS";
				}
				else
				{
					CS$<>8__locals1.saveData.ObjectId = Settings.SaveDataID;
					try
					{
						CS$<>8__locals1.saveData.FetchAsync(new NCMBCallback(CS$<>8__locals1.<Save>b__0));
					}
					catch (Exception ex)
					{
						global::Debug.LogError("NCMB:FetchAsync " + ex.ToString());
						CS$<>8__locals1.resultMessage = "ERROR";
					}
				}
				this.<>2__current = new WaitUntil(new Func<bool>(CS$<>8__locals1.<Save>b__1));
				this.<>1__state = 1;
				return true;
			case 1:
				this.<>1__state = -1;
				if (CS$<>8__locals1.resultMessage == "SUCCESS")
				{
					this.<>2__current = SaveDataManager.SaveToNCMB(CS$<>8__locals1.saveData, new Action<string>(CS$<>8__locals1.<Save>b__2));
					this.<>1__state = 2;
					return true;
				}
				break;
			case 2:
				this.<>1__state = -1;
				break;
			default:
				return false;
			}
			callback(CS$<>8__locals1.resultMessage);
			return false;
		}

		// Token: 0x170000E4 RID: 228
		// (get) Token: 0x06000813 RID: 2067 RVA: 0x00028160 File Offset: 0x00026360
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000814 RID: 2068 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000E5 RID: 229
		// (get) Token: 0x06000815 RID: 2069 RVA: 0x00028160 File Offset: 0x00026360
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040002FD RID: 765
		private int <>1__state;

		// Token: 0x040002FE RID: 766
		private object <>2__current;

		// Token: 0x040002FF RID: 767
		private SaveDataManager.<>c__DisplayClass6_0 <>8__1;

		// Token: 0x04000300 RID: 768
		public Action<string> callback;
	}

	// Token: 0x020000D3 RID: 211
	[CompilerGenerated]
	private sealed class <>c__DisplayClass7_0
	{
		// Token: 0x06000816 RID: 2070 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass7_0()
		{
		}

		// Token: 0x06000817 RID: 2071 RVA: 0x00028168 File Offset: 0x00026368
		internal void <SaveToNCMB>b__0(NCMBException e)
		{
			if (e == null)
			{
				Settings.SaveDataID = this.target.ObjectId;
				SaveDataManager.SaveDataTime = (this.target.UpdateDate ?? DateTime.MinValue);
				this.resultMessage = "SUCCESS";
				return;
			}
			this.resultMessage = "ERROR";
		}

		// Token: 0x06000818 RID: 2072 RVA: 0x000281C7 File Offset: 0x000263C7
		internal bool <SaveToNCMB>b__1()
		{
			return this.resultMessage != "";
		}

		// Token: 0x04000301 RID: 769
		public NCMBObject target;

		// Token: 0x04000302 RID: 770
		public string resultMessage;
	}

	// Token: 0x020000D4 RID: 212
	[CompilerGenerated]
	private sealed class <SaveToNCMB>d__7 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000819 RID: 2073 RVA: 0x000281D9 File Offset: 0x000263D9
		[DebuggerHidden]
		public <SaveToNCMB>d__7(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x0600081A RID: 2074 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x0600081B RID: 2075 RVA: 0x000281E8 File Offset: 0x000263E8
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			if (num == 0)
			{
				this.<>1__state = -1;
				CS$<>8__locals1 = new SaveDataManager.<>c__DisplayClass7_0();
				CS$<>8__locals1.target = target;
				CS$<>8__locals1.resultMessage = "";
				string[] statusStringArray = StringStatusConverter.GetStatusStringArray(SaveDataInfo.TypeList, false, false);
				string text = "UserID\t" + statusStringArray[2];
				string text2 = AnalyticsManager.GetUserID() + "\t" + statusStringArray[1];
				string[] array = text.Split(new char[]
				{
					'\t'
				});
				string[] array2 = text2.Split(new char[]
				{
					'\t'
				});
				for (int i = 0; i < array.Length - 1; i++)
				{
					CS$<>8__locals1.target[array[i]] = array2[i];
				}
				CS$<>8__locals1.target["LoadEnable"] = true;
				try
				{
					CS$<>8__locals1.target.SaveAsync(new NCMBCallback(CS$<>8__locals1.<SaveToNCMB>b__0));
				}
				catch (Exception ex)
				{
					global::Debug.LogError("NCMB:SaveAsync " + ex.ToString());
					CS$<>8__locals1.resultMessage = "ERROR";
				}
				this.<>2__current = new WaitUntil(new Func<bool>(CS$<>8__locals1.<SaveToNCMB>b__1));
				this.<>1__state = 1;
				return true;
			}
			if (num != 1)
			{
				return false;
			}
			this.<>1__state = -1;
			callback(CS$<>8__locals1.resultMessage);
			return false;
		}

		// Token: 0x170000E6 RID: 230
		// (get) Token: 0x0600081C RID: 2076 RVA: 0x00028374 File Offset: 0x00026574
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0600081D RID: 2077 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000E7 RID: 231
		// (get) Token: 0x0600081E RID: 2078 RVA: 0x00028374 File Offset: 0x00026574
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x04000303 RID: 771
		private int <>1__state;

		// Token: 0x04000304 RID: 772
		private object <>2__current;

		// Token: 0x04000305 RID: 773
		public NCMBObject target;

		// Token: 0x04000306 RID: 774
		public Action<string> callback;

		// Token: 0x04000307 RID: 775
		private SaveDataManager.<>c__DisplayClass7_0 <>8__1;
	}

	// Token: 0x020000D5 RID: 213
	[CompilerGenerated]
	private sealed class <>c__DisplayClass8_0
	{
		// Token: 0x0600081F RID: 2079 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass8_0()
		{
		}

		// Token: 0x06000820 RID: 2080 RVA: 0x0002837C File Offset: 0x0002657C
		internal void <Load>b__0(NCMBException e)
		{
			if (e != null)
			{
				this.resultMessage = "ERROR";
				return;
			}
			if (this.loadData.ContainsKey("LoadEnable") && (bool)this.loadData["LoadEnable"])
			{
				this.resultMessage = "SUCCESS";
				return;
			}
			this.resultMessage = "ERROR";
		}

		// Token: 0x06000821 RID: 2081 RVA: 0x000283D8 File Offset: 0x000265D8
		internal bool <Load>b__1()
		{
			return this.resultMessage != "";
		}

		// Token: 0x04000308 RID: 776
		public NCMBObject loadData;

		// Token: 0x04000309 RID: 777
		public string resultMessage;
	}

	// Token: 0x020000D6 RID: 214
	[CompilerGenerated]
	private sealed class <Load>d__8 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000822 RID: 2082 RVA: 0x000283EA File Offset: 0x000265EA
		[DebuggerHidden]
		public <Load>d__8(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000823 RID: 2083 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000824 RID: 2084 RVA: 0x000283FC File Offset: 0x000265FC
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			if (num == 0)
			{
				this.<>1__state = -1;
				CS$<>8__locals1 = new SaveDataManager.<>c__DisplayClass8_0();
				CS$<>8__locals1.resultMessage = "";
				CS$<>8__locals1.loadData = new NCMBObject("SaveData");
				CS$<>8__locals1.loadData.ObjectId = loadID;
				try
				{
					CS$<>8__locals1.loadData.FetchAsync(new NCMBCallback(CS$<>8__locals1.<Load>b__0));
				}
				catch (Exception ex)
				{
					global::Debug.LogError("NCMB:FetchAsync " + ex.ToString());
					CS$<>8__locals1.resultMessage = "ERROR";
				}
				this.<>2__current = new WaitUntil(new Func<bool>(CS$<>8__locals1.<Load>b__1));
				this.<>1__state = 1;
				return true;
			}
			if (num != 1)
			{
				return false;
			}
			this.<>1__state = -1;
			if (CS$<>8__locals1.resultMessage == "ERROR")
			{
				callback(false);
				return false;
			}
			foreach (string text in CS$<>8__locals1.loadData.Keys)
			{
				if (Array.IndexOf<string>(SaveDataManager.EXCEPTION_KEY, text) < 0)
				{
					StringStatusConverter.SetEachProperty(text, (string)CS$<>8__locals1.loadData[text]);
				}
			}
			if (CS$<>8__locals1.loadData.ContainsKey("IsInfinity") && bool.Parse((string)CS$<>8__locals1.loadData["IsInfinity"]))
			{
				callback(true);
				return false;
			}
			CS$<>8__locals1.loadData["LoadEnable"] = false;
			try
			{
				CS$<>8__locals1.loadData.SaveAsync();
			}
			catch (Exception ex2)
			{
				global::Debug.LogError("NCMB:SaveAsync " + ex2.ToString());
			}
			callback(true);
			return false;
		}

		// Token: 0x170000E8 RID: 232
		// (get) Token: 0x06000825 RID: 2085 RVA: 0x00028624 File Offset: 0x00026824
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000826 RID: 2086 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000E9 RID: 233
		// (get) Token: 0x06000827 RID: 2087 RVA: 0x00028624 File Offset: 0x00026824
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0400030A RID: 778
		private int <>1__state;

		// Token: 0x0400030B RID: 779
		private object <>2__current;

		// Token: 0x0400030C RID: 780
		public string loadID;

		// Token: 0x0400030D RID: 781
		private SaveDataManager.<>c__DisplayClass8_0 <>8__1;

		// Token: 0x0400030E RID: 782
		public Action<bool> callback;
	}
}
