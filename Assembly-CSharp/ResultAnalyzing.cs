﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000018 RID: 24
public class ResultAnalyzing : MonoBehaviour
{
	// Token: 0x0600009F RID: 159 RVA: 0x00008B62 File Offset: 0x00006D62
	private void Awake()
	{
		this.AnalyzingText = base.transform.Find("AnalyzingTextGroup/BaseText").GetComponent<Text>();
		this.ArrangeText(0);
	}

	// Token: 0x060000A0 RID: 160 RVA: 0x00008B88 File Offset: 0x00006D88
	public void StartAnalyzing(string[] wordList)
	{
		base.transform.Find("AnalyzingTextGroup").gameObject.SetActive(true);
		AppUtil.SetAlpha(base.transform.Find("AnalyzingTextGroup"), 1f);
		base.transform.Find("AnalyzeComplete").gameObject.SetActive(false);
		base.transform.Find("ShowResultButton").gameObject.SetActive(false);
		base.GetComponent<Animator>().enabled = true;
		this.DefaultEs = base.transform.Find("Es").GetComponent<Image>().sprite;
		base.transform.Find("Es").GetComponent<Animator>().enabled = true;
		this.WordDisplaying = this.DisplayWord(wordList);
		base.StartCoroutine(this.WordDisplaying);
	}

	// Token: 0x060000A1 RID: 161 RVA: 0x00008C62 File Offset: 0x00006E62
	public IEnumerator StopAnalyzing()
	{
		base.StopCoroutine(this.WordDisplaying);
		base.GetComponent<Animator>().enabled = false;
		base.transform.Find("Es").GetComponent<Animator>().enabled = false;
		yield return AppUtil.Wait(1f);
		yield return AppUtil.FadeOut(base.transform.Find("AnalyzingTextGroup"), 0.25f, null);
		base.transform.Find("AnalyzeComplete").gameObject.SetActive(true);
		base.transform.Find("Es").GetComponent<Image>().sprite = this.DefaultEs;
		yield return AppUtil.Wait(0.5f);
		base.transform.Find("ShowResultButton").gameObject.SetActive(true);
		yield break;
	}

	// Token: 0x060000A2 RID: 162 RVA: 0x00008C71 File Offset: 0x00006E71
	public void UpdateAnimation(int timing)
	{
		this.ArrangeText(timing);
	}

	// Token: 0x060000A3 RID: 163 RVA: 0x00008C7C File Offset: 0x00006E7C
	private void ArrangeText(int timing)
	{
		Transform parent = this.AnalyzingText.transform.parent;
		if (parent.Find("MessageGroup") != null)
		{
			Object.Destroy(parent.Find("MessageGroup").gameObject);
		}
		GameObject gameObject = new GameObject("MessageGroup");
		gameObject.AddComponent<RectTransform>();
		gameObject.transform.SetParent(parent, false);
		GameObject gameObject2 = this.AnalyzingText.gameObject;
		gameObject2.SetActive(true);
		string text = gameObject2.GetComponent<Text>().text;
		for (int i = 0; i < text.Length; i++)
		{
			GameObject gameObject3 = Object.Instantiate<GameObject>(gameObject2, gameObject.transform);
			gameObject3.GetComponent<Text>().text = text[i].ToString();
			gameObject3.GetComponent<RectTransform>().anchoredPosition = new Vector2((float)(280 + (text.Length - i + 1) * -100), 0f);
			gameObject3.GetComponent<RectTransform>().sizeDelta = Vector2.one * 100f;
			float d;
			if ((i + timing) % 2 == 0)
			{
				d = Random.Range(0.7f, 1f);
			}
			else
			{
				d = Random.Range(0.9f, 1.2f);
			}
			gameObject3.transform.localScale = Vector3.one * d;
			gameObject3.transform.Rotate(new Vector3(0f, 0f, (float)Random.Range(-15, 15)));
		}
		gameObject2.SetActive(false);
	}

	// Token: 0x060000A4 RID: 164 RVA: 0x00008DF4 File Offset: 0x00006FF4
	private IEnumerator DisplayWord(string[] wordList)
	{
		if (base.transform.Find("WordGroup") != null)
		{
			Object.Destroy(base.transform.Find("WordGroup").gameObject);
		}
		yield return AppUtil.Wait(1f);
		Rect[] area = new Rect[]
		{
			new Rect(-337f, 126f, 674f, 242f),
			new Rect(-337f, -376f, 674f, 300f)
		};
		GameObject baseObject = base.transform.Find("Word").gameObject;
		GameObject group = new GameObject("WordGroup");
		group.AddComponent<RectTransform>();
		group.transform.SetParent(base.transform, false);
		int num2;
		for (int i = 1; i <= 2; i = num2 + 1)
		{
			string[] array = (from tmp in wordList
			orderby Guid.NewGuid()
			select tmp).ToArray<string>();
			foreach (string text in array)
			{
				GameObject gameObject = Object.Instantiate<GameObject>(baseObject, group.transform);
				gameObject.GetComponent<Text>().text = text;
				gameObject.SetActive(true);
				float num = Random.Range(0.5f, 1.2f);
				if (Random.Range(0, 10) <= 6)
				{
					gameObject.transform.localScale = Vector3.one * num;
				}
				else
				{
					gameObject.transform.localScale = new Vector3(-num, num, num);
				}
				Rect rect = area[Random.Range(0, area.Length)];
				float x = rect.x + Random.Range(0f, rect.width);
				float y = rect.y + Random.Range(0f, rect.height);
				gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(x, y);
				base.StartCoroutine(AppUtil.FadeIn(gameObject, 0.5f, null));
				AppUtil.DelayAction(this, Random.Range(0.5f, 1.5f), AppUtil.FadeOut(gameObject, 0.5f, null), true);
				yield return AppUtil.Wait(Random.Range(0.2f, 0.55f));
			}
			string[] array2 = null;
			num2 = i;
		}
		yield break;
	}

	// Token: 0x060000A5 RID: 165 RVA: 0x000025AD File Offset: 0x000007AD
	public ResultAnalyzing()
	{
	}

	// Token: 0x0400002A RID: 42
	private Sprite DefaultEs;

	// Token: 0x0400002B RID: 43
	private Text AnalyzingText;

	// Token: 0x0400002C RID: 44
	private IEnumerator WordDisplaying;

	// Token: 0x020000C9 RID: 201
	[CompilerGenerated]
	private sealed class <StopAnalyzing>d__5 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060007E6 RID: 2022 RVA: 0x0002750D File Offset: 0x0002570D
		[DebuggerHidden]
		public <StopAnalyzing>d__5(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060007E7 RID: 2023 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060007E8 RID: 2024 RVA: 0x0002751C File Offset: 0x0002571C
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			ResultAnalyzing resultAnalyzing = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				resultAnalyzing.StopCoroutine(resultAnalyzing.WordDisplaying);
				resultAnalyzing.GetComponent<Animator>().enabled = false;
				resultAnalyzing.transform.Find("Es").GetComponent<Animator>().enabled = false;
				this.<>2__current = AppUtil.Wait(1f);
				this.<>1__state = 1;
				return true;
			case 1:
				this.<>1__state = -1;
				this.<>2__current = AppUtil.FadeOut(resultAnalyzing.transform.Find("AnalyzingTextGroup"), 0.25f, null);
				this.<>1__state = 2;
				return true;
			case 2:
				this.<>1__state = -1;
				resultAnalyzing.transform.Find("AnalyzeComplete").gameObject.SetActive(true);
				resultAnalyzing.transform.Find("Es").GetComponent<Image>().sprite = resultAnalyzing.DefaultEs;
				this.<>2__current = AppUtil.Wait(0.5f);
				this.<>1__state = 3;
				return true;
			case 3:
				this.<>1__state = -1;
				resultAnalyzing.transform.Find("ShowResultButton").gameObject.SetActive(true);
				return false;
			default:
				return false;
			}
		}

		// Token: 0x170000DA RID: 218
		// (get) Token: 0x060007E9 RID: 2025 RVA: 0x00027651 File Offset: 0x00025851
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060007EA RID: 2026 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000DB RID: 219
		// (get) Token: 0x060007EB RID: 2027 RVA: 0x00027651 File Offset: 0x00025851
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040002DF RID: 735
		private int <>1__state;

		// Token: 0x040002E0 RID: 736
		private object <>2__current;

		// Token: 0x040002E1 RID: 737
		public ResultAnalyzing <>4__this;
	}

	// Token: 0x020000CA RID: 202
	[CompilerGenerated]
	[Serializable]
	private sealed class <>c
	{
		// Token: 0x060007EC RID: 2028 RVA: 0x00027659 File Offset: 0x00025859
		// Note: this type is marked as 'beforefieldinit'.
		static <>c()
		{
		}

		// Token: 0x060007ED RID: 2029 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c()
		{
		}

		// Token: 0x060007EE RID: 2030 RVA: 0x00027665 File Offset: 0x00025865
		internal Guid <DisplayWord>b__8_0(string tmp)
		{
			return Guid.NewGuid();
		}

		// Token: 0x040002E2 RID: 738
		public static readonly ResultAnalyzing.<>c <>9 = new ResultAnalyzing.<>c();

		// Token: 0x040002E3 RID: 739
		public static Func<string, Guid> <>9__8_0;
	}

	// Token: 0x020000CB RID: 203
	[CompilerGenerated]
	private sealed class <DisplayWord>d__8 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060007EF RID: 2031 RVA: 0x0002766C File Offset: 0x0002586C
		[DebuggerHidden]
		public <DisplayWord>d__8(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060007F0 RID: 2032 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060007F1 RID: 2033 RVA: 0x0002767C File Offset: 0x0002587C
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			ResultAnalyzing resultAnalyzing = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				if (resultAnalyzing.transform.Find("WordGroup") != null)
				{
					Object.Destroy(resultAnalyzing.transform.Find("WordGroup").gameObject);
				}
				this.<>2__current = AppUtil.Wait(1f);
				this.<>1__state = 1;
				return true;
			case 1:
				this.<>1__state = -1;
				area = new Rect[]
				{
					new Rect(-337f, 126f, 674f, 242f),
					new Rect(-337f, -376f, 674f, 300f)
				};
				baseObject = resultAnalyzing.transform.Find("Word").gameObject;
				group = new GameObject("WordGroup");
				group.AddComponent<RectTransform>();
				group.transform.SetParent(resultAnalyzing.transform, false);
				i = 1;
				goto IL_2EE;
			case 2:
				this.<>1__state = -1;
				j++;
				break;
			default:
				return false;
			}
			IL_2C2:
			if (j < array2.Length)
			{
				string text = array2[j];
				GameObject gameObject = Object.Instantiate<GameObject>(baseObject, group.transform);
				gameObject.GetComponent<Text>().text = text;
				gameObject.SetActive(true);
				float num2 = Random.Range(0.5f, 1.2f);
				if (Random.Range(0, 10) <= 6)
				{
					gameObject.transform.localScale = Vector3.one * num2;
				}
				else
				{
					gameObject.transform.localScale = new Vector3(-num2, num2, num2);
				}
				Rect rect = area[Random.Range(0, area.Length)];
				float x = rect.x + Random.Range(0f, rect.width);
				float y = rect.y + Random.Range(0f, rect.height);
				gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(x, y);
				resultAnalyzing.StartCoroutine(AppUtil.FadeIn(gameObject, 0.5f, null));
				AppUtil.DelayAction(resultAnalyzing, Random.Range(0.5f, 1.5f), AppUtil.FadeOut(gameObject, 0.5f, null), true);
				this.<>2__current = AppUtil.Wait(Random.Range(0.2f, 0.55f));
				this.<>1__state = 2;
				return true;
			}
			array2 = null;
			int num3 = i;
			i = num3 + 1;
			IL_2EE:
			if (i > 2)
			{
				return false;
			}
			string[] array3 = wordList.OrderBy(new Func<string, Guid>(ResultAnalyzing.<>c.<>9.<DisplayWord>b__8_0)).ToArray<string>();
			array2 = array3;
			j = 0;
			goto IL_2C2;
		}

		// Token: 0x170000DC RID: 220
		// (get) Token: 0x060007F2 RID: 2034 RVA: 0x00027984 File Offset: 0x00025B84
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060007F3 RID: 2035 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000DD RID: 221
		// (get) Token: 0x060007F4 RID: 2036 RVA: 0x00027984 File Offset: 0x00025B84
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040002E4 RID: 740
		private int <>1__state;

		// Token: 0x040002E5 RID: 741
		private object <>2__current;

		// Token: 0x040002E6 RID: 742
		public ResultAnalyzing <>4__this;

		// Token: 0x040002E7 RID: 743
		public string[] wordList;

		// Token: 0x040002E8 RID: 744
		private Rect[] <area>5__2;

		// Token: 0x040002E9 RID: 745
		private GameObject <baseObject>5__3;

		// Token: 0x040002EA RID: 746
		private GameObject <group>5__4;

		// Token: 0x040002EB RID: 747
		private int <i>5__5;

		// Token: 0x040002EC RID: 748
		private string[] <>7__wrap5;

		// Token: 0x040002ED RID: 749
		private int <>7__wrap6;
	}
}
