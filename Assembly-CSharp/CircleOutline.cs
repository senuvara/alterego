﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000006 RID: 6
public class CircleOutline : BaseMeshEffect
{
	// Token: 0x06000014 RID: 20 RVA: 0x00002A84 File Offset: 0x00000C84
	public override void ModifyMesh(VertexHelper vh)
	{
		if (!this.IsActive())
		{
			return;
		}
		List<UIVertex> list = new List<UIVertex>();
		vh.GetUIVertexStream(list);
		this.ModifyVertices(list);
		vh.Clear();
		vh.AddUIVertexTriangleStream(list);
	}

	// Token: 0x06000015 RID: 21 RVA: 0x00002ABC File Offset: 0x00000CBC
	private void ModifyVertices(List<UIVertex> verts)
	{
		int start = 0;
		int count = verts.Count;
		for (int i = 0; i < this.m_nEffectNumber; i++)
		{
			float f = 6.2831855f * (float)i / (float)this.m_nEffectNumber;
			float x = this.m_EffectDistance * Mathf.Cos(f);
			float y = this.m_EffectDistance * Mathf.Sin(f);
			this.ApplyShadow(verts, start, count, x, y);
			start = count;
			count = verts.Count;
		}
	}

	// Token: 0x06000016 RID: 22 RVA: 0x00002B28 File Offset: 0x00000D28
	private void ApplyShadow(List<UIVertex> verts, int start, int end, float x, float y)
	{
		for (int i = start; i < end; i++)
		{
			UIVertex uivertex = verts[i];
			verts.Add(uivertex);
			Vector3 position = uivertex.position;
			position.x += x;
			position.y += y;
			uivertex.position = position;
			Color32 color = this.m_EffectColor;
			if (this.m_UseGraphicAlpha)
			{
				color.a = (byte)((float)(color.a * verts[i].color.a) / 255f);
			}
			uivertex.color = color;
			verts[i] = uivertex;
		}
	}

	// Token: 0x06000017 RID: 23 RVA: 0x00002BCA File Offset: 0x00000DCA
	public CircleOutline()
	{
	}

	// Token: 0x04000005 RID: 5
	[SerializeField]
	private Color m_EffectColor = new Color(0f, 0f, 0f, 0.5f);

	// Token: 0x04000006 RID: 6
	[SerializeField]
	private float m_EffectDistance = 1f;

	// Token: 0x04000007 RID: 7
	[SerializeField]
	private int m_nEffectNumber = 4;

	// Token: 0x04000008 RID: 8
	[SerializeField]
	private bool m_UseGraphicAlpha = true;
}
