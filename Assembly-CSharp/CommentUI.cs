﻿using System;
using System.Linq;
using App;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000009 RID: 9
public class CommentUI : MonoBehaviour
{
	// Token: 0x06000028 RID: 40 RVA: 0x000037EF File Offset: 0x000019EF
	private void OnEnable()
	{
		if (this.Type == CommentUI.TYPE.ID || PlayerStatus.ScenarioNo.Contains("4章ID"))
		{
			this.ArrangeText();
		}
	}

	// Token: 0x06000029 RID: 41 RVA: 0x00003814 File Offset: 0x00001A14
	public void ArrangeText()
	{
		if (base.transform.Find("MessageGroup") != null)
		{
			Object.Destroy(base.transform.Find("MessageGroup").gameObject);
		}
		GameObject gameObject = new GameObject("MessageGroup");
		gameObject.transform.parent = base.transform;
		gameObject.transform.localPosition = Vector3.zero;
		gameObject.transform.localScale = Vector3.one;
		RectTransform rectTransform = gameObject.AddComponent<RectTransform>();
		rectTransform.anchorMin = Vector2.one * 0.5f;
		rectTransform.anchorMax = Vector2.one * 0.5f;
		rectTransform.sizeDelta = Vector2.zero;
		GameObject gameObject2 = base.GetComponentInChildren<Text>(true).gameObject;
		gameObject2.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
		string text = gameObject2.GetComponent<Text>().text;
		gameObject2.SetActive(true);
		Object.Destroy(gameObject2.GetComponent<CenterLeftTextEgo>());
		Vector3 zero = Vector3.zero;
		Vector3 vector = zero;
		float num = 0f;
		float y = 30f;
		foreach (char c in text.ToCharArray())
		{
			if (c == '\n')
			{
				num = vector.x;
				y = 60f;
				vector = zero - new Vector3(0f, 52.8f, 0f);
			}
			else
			{
				GameObject gameObject3 = Object.Instantiate<GameObject>(gameObject2, gameObject.transform);
				RectTransform component = gameObject3.GetComponent<RectTransform>();
				component.anchorMin = Vector2.one * 0.5f;
				component.anchorMax = Vector2.one * 0.5f;
				component.sizeDelta = Vector2.one * 100f;
				component.anchoredPosition = vector;
				component.anchoredPosition += new Vector2(0f, Random.Range(-5f, 5f));
				gameObject3.transform.localScale = Vector3.one * Random.Range(0.9f, 1.1f);
				gameObject3.transform.Rotate(new Vector3(0f, 0f, (float)Random.Range(-10, 10)));
				string font = LanguageManager.FONTDATA_TABLE.Keys.ToArray<string>()[Random.Range(0, LanguageManager.FONTDATA_TABLE.Keys.Count)];
				if (Random.Range(0f, 1f) >= 0.5f)
				{
					font = "kokumin";
				}
				float num2 = 42f;
				if (!CommentUI.IsKanji(c))
				{
					num2 *= 0.75f;
				}
				if (vector == zero)
				{
					font = "kokumin";
					gameObject3.GetComponent<Text>().fontSize = (int)((float)gameObject3.GetComponent<Text>().fontSize * 1.3f);
					vector += new Vector3(num2 * 1.3f, 0f, 0f);
				}
				else
				{
					vector += new Vector3(num2, 0f, 0f);
				}
				gameObject3.GetComponent<TextLocalization>().SetText(c.ToString());
				gameObject3.GetComponent<TextLocalization>().SetFont(font);
			}
		}
		num = Mathf.Max(vector.x, num);
		rectTransform.anchoredPosition = new Vector2(-num / 3f, y);
		gameObject2.SetActive(false);
	}

	// Token: 0x0600002A RID: 42 RVA: 0x00003294 File Offset: 0x00001494
	public static bool IsKanji(char c)
	{
		return ('一' <= c && c <= '鿏') || ('豈' <= c && c <= '﫿') || ('㐀' <= c && c <= '䶿');
	}

	// Token: 0x0600002B RID: 43 RVA: 0x000025AD File Offset: 0x000007AD
	public CommentUI()
	{
	}

	// Token: 0x04000011 RID: 17
	public CommentUI.TYPE Type;

	// Token: 0x020000B9 RID: 185
	public enum TYPE
	{
		// Token: 0x040002AF RID: 687
		NONE,
		// Token: 0x040002B0 RID: 688
		ID,
		// Token: 0x040002B1 RID: 689
		SE
	}
}
