﻿using System;
using App;

// Token: 0x02000015 RID: 21
public class PrizeItem
{
	// Token: 0x17000005 RID: 5
	// (get) Token: 0x0600008A RID: 138 RVA: 0x000083A0 File Offset: 0x000065A0
	public bool IsMax
	{
		get
		{
			int num = int.Parse(Data.PRIZE_DATA[this.ItemNum - 1][5]);
			int num2 = int.Parse(Data.PRIZE_DATA[this.ItemNum - 1][4]);
			return PrizeLevel.Get(this.Condition) >= num / num2;
		}
	}

	// Token: 0x0600008B RID: 139 RVA: 0x000083F4 File Offset: 0x000065F4
	public PrizeItem(int num)
	{
		this.ItemNum = num;
		this.Condition = Data.PRIZE_DATA[this.ItemNum - 1][1];
	}

	// Token: 0x0600008C RID: 140 RVA: 0x00008420 File Offset: 0x00006620
	public string GetGift()
	{
		string baseText = Data.PRIZE_DATA[this.ItemNum - 1][3];
		return this.GetGiftText(baseText);
	}

	// Token: 0x0600008D RID: 141 RVA: 0x0000844C File Offset: 0x0000664C
	public string GetGiftLocalizeText()
	{
		string baseText = LanguageManager.Get("[Prize]Gift" + this.ItemNum);
		return this.GetGiftText(baseText);
	}

	// Token: 0x0600008E RID: 142 RVA: 0x0000847C File Offset: 0x0000667C
	private string GetGiftText(string baseText)
	{
		if (baseText.Contains("{0}"))
		{
			EgoPoint egoPoint = PlayerStatus.EgoPerSecond * 60f * 60f;
			if (baseText.Contains("3時間分"))
			{
				egoPoint *= 3f;
			}
			return string.Format(baseText, egoPoint.ToString());
		}
		return baseText;
	}

	// Token: 0x0600008F RID: 143 RVA: 0x000084D8 File Offset: 0x000066D8
	public float GetCurrentValueRate()
	{
		if (this.Condition.Contains("本の獲得EGO"))
		{
			EgoPoint org = new EgoPoint(this.GetCurrentValue());
			EgoPoint value = new EgoPoint(this.GetNextCondition());
			return org / value;
		}
		float num = (float)int.Parse(this.GetCurrentValue());
		float num2 = (float)int.Parse(this.GetNextCondition());
		return num / num2;
	}

	// Token: 0x06000090 RID: 144 RVA: 0x00008530 File Offset: 0x00006730
	public string GetCurrentValue()
	{
		string condition = this.Condition;
		uint num = <PrivateImplementationDetails>.ComputeStringHash(condition);
		if (num <= 1878490183U)
		{
			if (num <= 509253879U)
			{
				if (num != 388766957U)
				{
					if (num == 509253879U)
					{
						if (condition == "本ページ合計")
						{
							return PlayerResult.BookPageSum.ToString();
						}
					}
				}
				else if (condition == "吹き出しタップ回数")
				{
					return PlayerResult.TapCount.ToString();
				}
			}
			else if (num != 1437233026U)
			{
				if (num == 1878490183U)
				{
					if (condition == "獲得EGO/タップ")
					{
						return PlayerResult.TapMax.ToString();
					}
				}
			}
			else if (condition == "目標達成回数")
			{
				return PlayerResult.PrizeCount.ToString();
			}
		}
		else if (num <= 2860442795U)
		{
			if (num != 1990224568U)
			{
				if (num == 2860442795U)
				{
					if (condition == "エンディング回数")
					{
						return PlayerResult.EndingCount.ToString();
					}
				}
			}
			else if (condition == "動画広告視聴回数")
			{
				return PlayerResult.AdMovieCount.ToString();
			}
		}
		else if (num != 3273228950U)
		{
			if (num == 3552439344U)
			{
				if (condition == "ログイン日数")
				{
					return PlayerResult.LoginCount.ToString();
				}
			}
		}
		else if (condition == "獲得EGO/秒")
		{
			return PlayerStatus.EgoPerSecond.ToString();
		}
		return "";
	}

	// Token: 0x06000091 RID: 145 RVA: 0x000086BC File Offset: 0x000068BC
	public string GetNextCondition()
	{
		string s = Data.PRIZE_DATA[this.ItemNum - 1][4];
		if (this.Condition.Contains("本の獲得EGO"))
		{
			int num = int.Parse(s);
			EgoPoint egoPoint = new EgoPoint((float)num);
			for (int i = 0; i < PrizeLevel.Get(this.Condition); i++)
			{
				egoPoint *= (float)num;
			}
			return egoPoint.ToString();
		}
		if (this.IsMax)
		{
			return Data.PRIZE_DATA[this.ItemNum - 1][5];
		}
		return (int.Parse(s) * (PrizeLevel.Get(this.Condition) + 1)).ToString();
	}

	// Token: 0x06000092 RID: 146 RVA: 0x0000875F File Offset: 0x0000695F
	public void AddLevel()
	{
		PrizeLevel.Add(this.Condition, 1);
	}

	// Token: 0x06000093 RID: 147 RVA: 0x0000876D File Offset: 0x0000696D
	public int GetLevel()
	{
		return PrizeLevel.Get(this.Condition);
	}

	// Token: 0x06000094 RID: 148 RVA: 0x0000877A File Offset: 0x0000697A
	public string GetUnit()
	{
		if (this.Condition.Contains("回数"))
		{
			return "回";
		}
		if (this.Condition.Contains("日数"))
		{
			return "日";
		}
		return "";
	}

	// Token: 0x06000095 RID: 149 RVA: 0x000087B4 File Offset: 0x000069B4
	public bool IsEnableButton()
	{
		if (this.IsMax)
		{
			return false;
		}
		bool result = this.GetCurrentValueRate() >= 1f;
		if (PlayerStatus.EgoPerSecond.IsZero() && this.GetGift().Contains("時間分の"))
		{
			result = false;
		}
		return result;
	}

	// Token: 0x04000025 RID: 37
	private int ItemNum;

	// Token: 0x04000026 RID: 38
	private string Condition;
}
