﻿using System;
using System.Runtime.CompilerServices;
using App;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

// Token: 0x0200001D RID: 29
public class SceneEnding : SceneBase
{
	// Token: 0x060000D9 RID: 217 RVA: 0x00009F24 File Offset: 0x00008124
	protected void OnEnable()
	{
		AdManager.Hide("Banner");
		this.EndingDirector.stopped += this.OnPlayableDirectorStopped;
		this.EndingDirector.GetComponent<AudioSource>().mute = !Settings.BGM;
		this.audioListener.enabled = (SceneManager.sceneCount == 1);
	}

	// Token: 0x060000DA RID: 218 RVA: 0x00009F80 File Offset: 0x00008180
	private void OnPlayableDirectorStopped(PlayableDirector aDirector)
	{
		if (this.EndingDirector == aDirector)
		{
			global::Debug.Log("PlayableDirector#OnPlayableDirectorStopped " + SceneManager.sceneCount);
			if (SceneManager.sceneCount > 1)
			{
				SceneTransition.Transit(delegate
				{
					SceneManager.UnloadSceneAsync("Ending");
					AudioManager.ChangeBGM("Silence_OffV");
					AdManager.Show("Banner", null);
				}, null);
				return;
			}
			SceneTransition.LoadScene("タイトル", new Color?(Color.black), 3f);
		}
	}

	// Token: 0x060000DB RID: 219 RVA: 0x0000A003 File Offset: 0x00008203
	protected void OnDisable()
	{
		this.EndingDirector.stopped -= this.OnPlayableDirectorStopped;
	}

	// Token: 0x060000DC RID: 220 RVA: 0x00009F09 File Offset: 0x00008109
	public SceneEnding()
	{
	}

	// Token: 0x04000039 RID: 57
	public PlayableDirector EndingDirector;

	// Token: 0x0400003A RID: 58
	public AudioListener audioListener;

	// Token: 0x020000E0 RID: 224
	[CompilerGenerated]
	[Serializable]
	private sealed class <>c
	{
		// Token: 0x0600084F RID: 2127 RVA: 0x00029BAD File Offset: 0x00027DAD
		// Note: this type is marked as 'beforefieldinit'.
		static <>c()
		{
		}

		// Token: 0x06000850 RID: 2128 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c()
		{
		}

		// Token: 0x06000851 RID: 2129 RVA: 0x00029BB9 File Offset: 0x00027DB9
		internal void <OnPlayableDirectorStopped>b__3_0()
		{
			SceneManager.UnloadSceneAsync("Ending");
			AudioManager.ChangeBGM("Silence_OffV");
			AdManager.Show("Banner", null);
		}

		// Token: 0x0400034B RID: 843
		public static readonly SceneEnding.<>c <>9 = new SceneEnding.<>c();

		// Token: 0x0400034C RID: 844
		public static Action <>9__3_0;
	}
}
