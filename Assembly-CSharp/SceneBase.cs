﻿using System;
using System.Runtime.CompilerServices;
using App;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// Token: 0x0200001B RID: 27
public class SceneBase : SceneCommon
{
	// Token: 0x060000B7 RID: 183 RVA: 0x00009440 File Offset: 0x00007640
	protected override void Awake()
	{
		base.Awake();
		if (GameObject.Find("EgoCount") == null)
		{
			return;
		}
		TimeManager.UpdateLoginDate();
		this.EgoCountText = GameObject.Find("UI_Header/Header1/EgoCount").GetComponent<Text>();
		this.EgoPerSecondText = GameObject.Find("UI_Header/Header1/EgoPerSecond").GetComponent<Text>();
		float num = TimeManager.SetLastTime(TimeManager.TYPE.LAST_EGO);
		if (num > 21600f)
		{
			num = 21600f;
		}
		EgoPoint egoPoint = PlayerStatus.EgoPerSecond * num;
		if (egoPoint.ToString() != "0.00")
		{
			PlayerStatus.EgoPoint += egoPoint;
			if (num >= 60f)
			{
				DialogManager.ShowDialog("ComebackDialog", new object[]
				{
					egoPoint.ToString()
				});
			}
		}
		this.UpdateHeader();
	}

	// Token: 0x060000B8 RID: 184 RVA: 0x00009500 File Offset: 0x00007700
	protected override void Start()
	{
		base.Start();
		EventSystem.current.pixelDragThreshold = 50;
		string name = base.gameObject.scene.name;
		if (name == "タイトル" || name == "探求" || name == "Ending")
		{
			AdManager.Hide("Banner");
			return;
		}
		AdManager.Show("Banner", null);
	}

	// Token: 0x060000B9 RID: 185 RVA: 0x00009570 File Offset: 0x00007770
	protected override void FixedUpdate()
	{
		base.FixedUpdate();
		if (this.EgoCountText == null)
		{
			return;
		}
		float num = TimeManager.SetLastTime(TimeManager.TYPE.LAST_EGO);
		if (num > 21600f)
		{
			num = 21600f;
		}
		EgoPoint value = PlayerStatus.EgoPerSecond * num * Settings.GAME_SPEED;
		PlayerStatus.EgoPoint += value;
		this.UpdateHeader();
	}

	// Token: 0x060000BA RID: 186 RVA: 0x000095D4 File Offset: 0x000077D4
	protected void UpdateHeader()
	{
		if (this.EgoCountText == null)
		{
			return;
		}
		if (base.transform.Find("UICanvas/UI_Header/Header1/NormalLabel") == null)
		{
			return;
		}
		bool inBonus = this.InBonus;
		this.InBonus = BonusTime.IsActive;
		if (inBonus != this.InBonus)
		{
			Data.UpdateEgoPerSecond();
		}
		base.transform.Find("UICanvas/UI_Header/Header1/NormalLabel").gameObject.SetActive(!this.InBonus);
		base.transform.Find("UICanvas/UI_Header/Header1/BonusLabel").gameObject.SetActive(this.InBonus);
		if (this.InBonus)
		{
			string text = BonusTime.TimeLeft.ToString("mm\\:ss");
			base.transform.Find("UICanvas/UI_Header/Header1/BonusLabel/EgoUnit/BonusTimeText").GetComponent<Text>().text = text;
		}
		this.EgoCountText.text = PlayerStatus.EgoPoint.ToString(LanguageManager.Get("[UI]UI_Header/EgoCount"));
		string text2 = PlayerStatus.EgoPerSecond.ToString(LanguageManager.Get("[UI]UI_Header/EgoPerSecond"));
		this.EgoPerSecondText.text = text2;
	}

	// Token: 0x060000BB RID: 187 RVA: 0x000096E0 File Offset: 0x000078E0
	public bool OnClick(GameObject clickObject)
	{
		global::Debug.Log("SceneBase:OnClick " + clickObject.name);
		if (clickObject.name.Contains("GlobalButton"))
		{
			string name = SceneManager.GetActiveScene().name;
			string text = clickObject.name.Replace("GlobalButton", "");
			if (text == "アンケート")
			{
				Application.OpenURL("https://goo.gl/forms/8xsZWW9lvcY17Q9G3");
			}
			else if (text != name)
			{
				SceneTransition.LoadScene(text, null, 0.5f);
			}
			return true;
		}
		string name2 = clickObject.name;
		if (name2 == "MenuButton")
		{
			DialogManager.ShowDialog("MenuDialog", Array.Empty<object>());
			return true;
		}
		if (name2 == "ButterflyBonus")
		{
			Object.Destroy(clickObject);
			DialogManager.ShowDialog("BonusMovieDialog", Array.Empty<object>());
			return true;
		}
		if (!(name2 == "ShareButton"))
		{
			return false;
		}
		clickObject.GetComponent<Button>().interactable = false;
		base.StartCoroutine(AppUtil.DelayAction(1f, delegate()
		{
			clickObject.GetComponent<Button>().interactable = true;
		}, true));
		base.StartCoroutine(AppUtil.Share(true, "自分探しタップゲーム『ALTER EGO』 caracolu.com/app/alterego/ #ALTEREGO", null, null));
		return true;
	}

	// Token: 0x060000BC RID: 188 RVA: 0x00009839 File Offset: 0x00007A39
	public SceneBase()
	{
	}

	// Token: 0x04000032 RID: 50
	private Text EgoCountText;

	// Token: 0x04000033 RID: 51
	private Text EgoPerSecondText;

	// Token: 0x04000034 RID: 52
	private bool InBonus;

	// Token: 0x020000D7 RID: 215
	[CompilerGenerated]
	private sealed class <>c__DisplayClass7_0
	{
		// Token: 0x06000828 RID: 2088 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass7_0()
		{
		}

		// Token: 0x06000829 RID: 2089 RVA: 0x0002862C File Offset: 0x0002682C
		internal void <OnClick>b__0()
		{
			this.clickObject.GetComponent<Button>().interactable = true;
		}

		// Token: 0x0400030F RID: 783
		public GameObject clickObject;
	}
}
