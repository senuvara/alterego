﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

namespace App
{
	// Token: 0x020000A4 RID: 164
	[AppUtil.TitleAttribute("ステータス")]
	public static class PlayerStatus
	{
		// Token: 0x1700009B RID: 155
		// (get) Token: 0x060006FD RID: 1789 RVA: 0x000244B7 File Offset: 0x000226B7
		// (set) Token: 0x060006FE RID: 1790 RVA: 0x000244C8 File Offset: 0x000226C8
		[AppUtil.TitleAttribute("シナリオ")]
		[AppUtil.ArrayValueAttribute("SCENARIO_NO_LIST")]
		public static string ScenarioNo
		{
			get
			{
				return PlayerPrefs.GetString("ScenarioNo", "1章-1");
			}
			set
			{
				if (PlayerStatus.ScenarioNo == value)
				{
					return;
				}
				string scenarioNo = PlayerStatus.ScenarioNo;
				PlayerPrefs.SetString("ScenarioNo", value);
				PlayerStatus.Route = Data.GetRoute(value);
				Data.UpdateClearScenarioList();
				Data.UpdateCommentList();
				Data.UpdateEgoBookPower();
				AnalyticsEvent.Custom("scenario_lv", new Dictionary<string, object>
				{
					{
						"scenario_no",
						value
					}
				});
				if (value.Contains("-1"))
				{
					string text = scenarioNo.Split(new char[]
					{
						'-'
					})[0];
					AnalyticsManager.SendEvent(new string[]
					{
						"シナリオ読了",
						text,
						"退行" + PlayerResult.EndingCount
					}, "1DLWkIkaBFBnrV9VoNi09qwvOhocUADC8BWVHfNzyjfA");
				}
			}
		}

		// Token: 0x1700009C RID: 156
		// (get) Token: 0x060006FF RID: 1791 RVA: 0x0002457E File Offset: 0x0002277E
		// (set) Token: 0x06000700 RID: 1792 RVA: 0x00024599 File Offset: 0x00022799
		[AppUtil.TitleAttribute("EGO")]
		public static EgoPoint EgoPoint
		{
			get
			{
				return JsonUtility.FromJson<EgoPoint>(PlayerPrefs.GetString("EgoPoint", JsonUtility.ToJson(new EgoPoint())));
			}
			set
			{
				PlayerPrefs.SetString("EgoPoint", JsonUtility.ToJson(value));
			}
		}

		// Token: 0x1700009D RID: 157
		// (get) Token: 0x06000701 RID: 1793 RVA: 0x000245AB File Offset: 0x000227AB
		[AppUtil.TitleAttribute("EGO(毎秒)")]
		public static EgoPoint EgoPerSecond
		{
			get
			{
				return Data.EgoPerSecond;
			}
		}

		// Token: 0x1700009E RID: 158
		// (get) Token: 0x06000702 RID: 1794 RVA: 0x000245B2 File Offset: 0x000227B2
		[AppUtil.TitleAttribute("EGO(タップx倍)")]
		public static EgoPoint TapPower
		{
			get
			{
				return Data.GetEgoTapPower();
			}
		}

		// Token: 0x1700009F RID: 159
		// (get) Token: 0x06000703 RID: 1795 RVA: 0x000245B9 File Offset: 0x000227B9
		[AppUtil.HideAttribute]
		public static string[] SCENARIO_NO_LIST
		{
			get
			{
				return Data.SCENARIO_DATA_KEY.ToArray();
			}
		}

		// Token: 0x170000A0 RID: 160
		// (get) Token: 0x06000704 RID: 1796 RVA: 0x000245C8 File Offset: 0x000227C8
		// (set) Token: 0x06000705 RID: 1797 RVA: 0x000245FF File Offset: 0x000227FF
		[AppUtil.TitleAttribute("チュートリアル")]
		public static int TutorialLv
		{
			get
			{
				int num = PlayerPrefs.GetInt("TutorialLv", PlayerStatus.TUTORIAL_MAX + 1);
				if (PlayerStatus.ScenarioNo == "1章-1" && num <= 0)
				{
					num = 1;
				}
				return num;
			}
			set
			{
				PlayerPrefs.SetInt("TutorialLv", value);
			}
		}

		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x06000706 RID: 1798 RVA: 0x0002460C File Offset: 0x0002280C
		[AppUtil.TitleAttribute("属性(L/C)")]
		public static int LCType
		{
			get
			{
				int num = 0;
				for (int i = UserChoice.Min; i <= UserChoice.Max; i++)
				{
					string title = UserChoice.GetTitle(i);
					if (title.Contains("対話"))
					{
						string a = UserChoice.Get(title);
						if (!(a == "Law"))
						{
							if (a == "Chaos")
							{
								num--;
							}
						}
						else
						{
							num++;
						}
					}
				}
				return num;
			}
		}

		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x06000707 RID: 1799 RVA: 0x00024671 File Offset: 0x00022871
		[AppUtil.TitleAttribute("属性傾向")]
		public static string LCTypeTrend
		{
			get
			{
				if (Mathf.Abs(PlayerStatus.LCType) <= 3)
				{
					return "Neutral";
				}
				if (PlayerStatus.LCType >= 0)
				{
					return "Law";
				}
				return "Chaos";
			}
		}

		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x06000708 RID: 1800 RVA: 0x00024699 File Offset: 0x00022899
		// (set) Token: 0x06000709 RID: 1801 RVA: 0x000246A5 File Offset: 0x000228A5
		[AppUtil.TitleAttribute("ルート")]
		public static string Route
		{
			get
			{
				return PlayerPrefs.GetString("Route");
			}
			set
			{
				PlayerPrefs.SetString("Route", value);
			}
		}

		// Token: 0x170000A4 RID: 164
		// (get) Token: 0x0600070A RID: 1802 RVA: 0x000246B4 File Offset: 0x000228B4
		[AppUtil.TitleAttribute("ルート傾向")]
		public static string RouteTrend
		{
			get
			{
				if (PlayerStatus.Route != "")
				{
					return PlayerStatus.Route;
				}
				if (PlayerResult.Ending.Contains("SE") && PlayerResult.Ending.Contains("ID") && -3 <= PlayerStatus.LCType && PlayerStatus.LCType <= 3)
				{
					return "AE";
				}
				if (PlayerStatus.LCType >= 0)
				{
					return "SE";
				}
				return "ID";
			}
		}

		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x0600070B RID: 1803 RVA: 0x0002472C File Offset: 0x0002292C
		// (set) Token: 0x0600070C RID: 1804 RVA: 0x00024762 File Offset: 0x00022962
		[AppUtil.TitleAttribute("３章ルート")]
		public static string Route3
		{
			get
			{
				string text = PlayerPrefs.GetString("Route3");
				if (text == "")
				{
					text = PlayerStatus.Route;
					PlayerStatus.Route3 = PlayerStatus.Route;
				}
				return text;
			}
			set
			{
				PlayerPrefs.SetString("Route3", value);
			}
		}

		// Token: 0x170000A6 RID: 166
		// (get) Token: 0x0600070D RID: 1805 RVA: 0x0002476F File Offset: 0x0002296F
		// (set) Token: 0x0600070E RID: 1806 RVA: 0x0002477B File Offset: 0x0002297B
		[AppUtil.TitleAttribute("レビュー促進状況")]
		public static string PromptReview
		{
			get
			{
				return PlayerPrefs.GetString("PromptReview");
			}
			set
			{
				PlayerPrefs.SetString("PromptReview", value);
			}
		}

		// Token: 0x170000A7 RID: 167
		// (get) Token: 0x0600070F RID: 1807 RVA: 0x00024788 File Offset: 0x00022988
		// (set) Token: 0x06000710 RID: 1808 RVA: 0x00024798 File Offset: 0x00022998
		[AppUtil.TitleAttribute("日替わり会話")]
		public static bool EnableDailyBonus
		{
			get
			{
				return PlayerPrefs.GetInt("EnableDailyBonus", 1) == 1;
			}
			set
			{
				PlayerPrefs.SetInt("EnableDailyBonus", value ? 1 : 0);
			}
		}

		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x06000711 RID: 1809 RVA: 0x000247AB File Offset: 0x000229AB
		// (set) Token: 0x06000712 RID: 1810 RVA: 0x000247B7 File Offset: 0x000229B7
		[AppUtil.TitleAttribute("読書中の本")]
		public static int ReadingBook
		{
			get
			{
				return PlayerPrefs.GetInt("ReadingBookHayakawa");
			}
			set
			{
				PlayerPrefs.SetInt("ReadingBookHayakawa", value);
			}
		}

		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x06000713 RID: 1811 RVA: 0x000247C4 File Offset: 0x000229C4
		// (set) Token: 0x06000714 RID: 1812 RVA: 0x000247D0 File Offset: 0x000229D0
		[AppUtil.TitleAttribute("借りた本")]
		public static string ReadingBookList
		{
			get
			{
				return PlayerPrefs.GetString("ReadingBookHayakawaList");
			}
			set
			{
				PlayerPrefs.SetString("ReadingBookHayakawaList", value);
				Data.UpdateEgoBookPower();
			}
		}

		// Token: 0x06000715 RID: 1813 RVA: 0x000247E2 File Offset: 0x000229E2
		// Note: this type is marked as 'beforefieldinit'.
		static PlayerStatus()
		{
		}

		// Token: 0x04000265 RID: 613
		public static readonly int TUTORIAL_MAX = 7;
	}
}
