﻿using System;
using UnityEngine;

namespace App
{
	// Token: 0x020000A2 RID: 162
	[AppUtil.DataRangeAttribute]
	[AppUtil.TitleAttribute("アイテム課金")]
	public static class PurchasingItem
	{
		// Token: 0x17000095 RID: 149
		// (get) Token: 0x060006EC RID: 1772 RVA: 0x00021B8F File Offset: 0x0001FD8F
		public static int Min
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000096 RID: 150
		// (get) Token: 0x060006ED RID: 1773 RVA: 0x0002429F File Offset: 0x0002249F
		public static int Max
		{
			get
			{
				return PurchasingItem.ITEM_LIST.Length;
			}
		}

		// Token: 0x060006EE RID: 1774 RVA: 0x000242A8 File Offset: 0x000224A8
		public static string GetTitle(int no)
		{
			return PurchasingItem.ITEM_LIST[no - 1];
		}

		// Token: 0x060006EF RID: 1775 RVA: 0x000242B3 File Offset: 0x000224B3
		public static string GetLabel(int no)
		{
			return PurchasingItem.LABEL_LIST[no - 1];
		}

		// Token: 0x060006F0 RID: 1776 RVA: 0x000242BE File Offset: 0x000224BE
		public static bool GetByLabel(string itemLabel)
		{
			return PurchasingItem.Get(PurchasingItem.ITEM_LIST[Array.IndexOf<string>(PurchasingItem.LABEL_LIST, itemLabel)]);
		}

		// Token: 0x060006F1 RID: 1777 RVA: 0x000242D6 File Offset: 0x000224D6
		public static bool Get(string name)
		{
			return PlayerPrefs.GetInt(name + PurchasingItem.label, 0) > 0;
		}

		// Token: 0x060006F2 RID: 1778 RVA: 0x000242EC File Offset: 0x000224EC
		public static void Set(string name, bool value)
		{
			if (PurchasingItem.Get(name) == value)
			{
				return;
			}
			PlayerPrefs.SetInt(name + PurchasingItem.label, value ? 1 : 0);
			string text = PurchasingItem.LABEL_LIST[Array.IndexOf<string>(PurchasingItem.ITEM_LIST, name)];
			if (!(text == "広告削除"))
			{
				if (text == "本の獲得EGO10倍")
				{
					Data.UpdateEgoBookPower();
				}
			}
			else
			{
				AdManager.SetActive(!value);
			}
			if (text.Contains("自我とエス"))
			{
				GameObject gameObject = GameObject.Find("TalkManager");
				if (gameObject != null)
				{
					gameObject.GetComponent<SceneTalk>().ResetButtonList();
				}
			}
		}

		// Token: 0x060006F3 RID: 1779 RVA: 0x00024384 File Offset: 0x00022584
		// Note: this type is marked as 'beforefieldinit'.
		static PurchasingItem()
		{
		}

		// Token: 0x0400025E RID: 606
		public static readonly string[] ITEM_LIST = new string[]
		{
			"item1",
			"item2",
			"item3",
			"item4",
			"item5",
			"item6"
		};

		// Token: 0x0400025F RID: 607
		public static readonly string[] LABEL_LIST = new string[]
		{
			"タップ獲得EGO1000倍",
			"本の獲得EGO10倍",
			"広告削除",
			"自我とエス(SE)",
			"自我とエス(ID)",
			"自我とエス(AE)"
		};

		// Token: 0x04000260 RID: 608
		private static string label = "PurchasingItem";
	}
}
