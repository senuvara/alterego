﻿using System;

namespace App
{
	// Token: 0x020000A3 RID: 163
	[AppUtil.TitleAttribute("多動モード")]
	public static class HyperActive
	{
		// Token: 0x060006F4 RID: 1780 RVA: 0x00024411 File Offset: 0x00022611
		static HyperActive()
		{
			HyperActive.Init();
		}

		// Token: 0x060006F5 RID: 1781 RVA: 0x00024438 File Offset: 0x00022638
		public static void Init()
		{
			HyperActive.Phase = 1;
		}

		// Token: 0x17000097 RID: 151
		// (get) Token: 0x060006F6 RID: 1782 RVA: 0x00024440 File Offset: 0x00022640
		// (set) Token: 0x060006F7 RID: 1783 RVA: 0x00024447 File Offset: 0x00022647
		[AppUtil.TitleAttribute("多動フェーズ")]
		public static int Phase
		{
			get
			{
				return HyperActive._Phase;
			}
			set
			{
				HyperActive.LastTime = TimeManager.Now;
				HyperActive.Time = 0f;
				HyperActive.Point = 0;
				HyperActive._Phase = value;
			}
		}

		// Token: 0x17000098 RID: 152
		// (get) Token: 0x060006F8 RID: 1784 RVA: 0x00024469 File Offset: 0x00022669
		// (set) Token: 0x060006F9 RID: 1785 RVA: 0x00024470 File Offset: 0x00022670
		[AppUtil.TitleAttribute("多動ポイント")]
		public static int Point
		{
			get
			{
				return HyperActive._Point;
			}
			set
			{
				HyperActive._Point = value;
			}
		}

		// Token: 0x17000099 RID: 153
		// (get) Token: 0x060006FA RID: 1786 RVA: 0x00024478 File Offset: 0x00022678
		// (set) Token: 0x060006FB RID: 1787 RVA: 0x0002447F File Offset: 0x0002267F
		[AppUtil.TitleAttribute("多動経過時間")]
		public static float Time
		{
			get
			{
				return HyperActive._Time;
			}
			set
			{
				HyperActive._Time = value;
			}
		}

		// Token: 0x1700009A RID: 154
		// (get) Token: 0x060006FC RID: 1788 RVA: 0x00024488 File Offset: 0x00022688
		[AppUtil.HideAttribute]
		public static float PassedTime
		{
			get
			{
				float result = (float)(TimeManager.Now - HyperActive.LastTime).TotalSeconds;
				HyperActive.LastTime = TimeManager.Now;
				return result;
			}
		}

		// Token: 0x04000261 RID: 609
		private static int _Phase = 0;

		// Token: 0x04000262 RID: 610
		private static int _Point = 0;

		// Token: 0x04000263 RID: 611
		private static float _Time = 0f;

		// Token: 0x04000264 RID: 612
		public static TimeSpan LastTime = TimeManager.Now;
	}
}
