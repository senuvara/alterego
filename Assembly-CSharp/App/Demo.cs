﻿using System;

namespace App
{
	// Token: 0x020000AD RID: 173
	public static class Demo
	{
		// Token: 0x06000787 RID: 1927 RVA: 0x0002641C File Offset: 0x0002461C
		// Note: this type is marked as 'beforefieldinit'.
		static Demo()
		{
		}

		// Token: 0x0400027B RID: 635
		public static readonly string StartScene = "タイトル";

		// Token: 0x0400027C RID: 636
		public static readonly string PlayTestCase = "Demo1";
	}
}
