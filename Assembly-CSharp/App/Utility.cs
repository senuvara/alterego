﻿using System;

namespace App
{
	// Token: 0x020000AE RID: 174
	public static class Utility
	{
		// Token: 0x06000788 RID: 1928 RVA: 0x00026434 File Offset: 0x00024634
		public static string GetSoundName(params string[] eventList)
		{
			string text = "";
			foreach (string[] array in Data.SOUND_DATA)
			{
				bool flag = array[2] == "" || array[2] == eventList[0];
				bool flag2 = array[3] == "" || array[3] == eventList[1];
				bool flag3 = array[4] == "" || array[4] == eventList[2];
				if (flag && flag2 && flag3)
				{
					text = array[1];
					break;
				}
			}
			Debug.Log("GetSoundName " + string.Join(",", eventList) + "=" + text);
			return text;
		}

		// Token: 0x06000789 RID: 1929 RVA: 0x00026510 File Offset: 0x00024710
		public static bool EsExists()
		{
			return PlayerStatus.TutorialLv >= 7 && !Data.GetScenarioSpecific("").Contains("エス不在");
		}

		// Token: 0x0600078A RID: 1930 RVA: 0x00026535 File Offset: 0x00024735
		public static void PromptReview(string type)
		{
			if (!PlayerStatus.PromptReview.Contains(type))
			{
				if (!AppUtil.CallReviewView())
				{
					DialogManager.ShowDialog("ReviewDialog", Array.Empty<object>());
				}
				PlayerStatus.PromptReview = PlayerStatus.PromptReview + type + ",";
			}
		}

		// Token: 0x0600078B RID: 1931 RVA: 0x0002656F File Offset: 0x0002476F
		public static int GetIntParameter(string name)
		{
			return int.Parse(GssDataHelper.GetData("その他", name)[1]);
		}
	}
}
