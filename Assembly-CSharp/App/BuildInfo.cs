﻿using System;

namespace App
{
	// Token: 0x0200009B RID: 155
	public static class BuildInfo
	{
		// Token: 0x060006D0 RID: 1744 RVA: 0x00023E00 File Offset: 0x00022000
		// Note: this type is marked as 'beforefieldinit'.
		static BuildInfo()
		{
		}

		// Token: 0x04000238 RID: 568
		public static readonly string[] LANG_TYPE = new string[]
		{
			"日本語"
		};

		// Token: 0x04000239 RID: 569
		public static readonly string LANG_DEFAULT = "日本語";

		// Token: 0x0400023A RID: 570
		public static readonly string APP_CODENAME = "EGO";

		// Token: 0x0400023B RID: 571
		public static readonly string APP_NAME = "ALTER EGO";

		// Token: 0x0400023C RID: 572
		public static readonly string APP_VERSION_MAIN = "2.4";

		// Token: 0x0400023D RID: 573
		public static readonly string APP_VERSION_SUB = "2";

		// Token: 0x0400023E RID: 574
		public static readonly int APP_VERSION_CODE = 26;

		// Token: 0x0400023F RID: 575
		public static readonly string APP_BUNDLE_ID_BASE = "com.caracolu.alterego";

		// Token: 0x04000240 RID: 576
		public static bool IsRelease = true;

		// Token: 0x04000241 RID: 577
		public static readonly string APP_VERSION = BuildInfo.APP_VERSION_MAIN + "." + BuildInfo.APP_VERSION_SUB;

		// Token: 0x04000242 RID: 578
		public static readonly string APP_BUNDLE_ID = BuildInfo.APP_BUNDLE_ID_BASE;
	}
}
