﻿using System;
using UnityEngine;

namespace App
{
	// Token: 0x020000A7 RID: 167
	[AppUtil.DataRangeAttribute]
	[AppUtil.TitleAttribute("診断結果")]
	public static class CounselingResult
	{
		// Token: 0x170000AF RID: 175
		// (get) Token: 0x06000720 RID: 1824 RVA: 0x00021B8F File Offset: 0x0001FD8F
		public static int Min
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x06000721 RID: 1825 RVA: 0x000249C3 File Offset: 0x00022BC3
		public static int Max
		{
			get
			{
				return Data.COUNSELING_TYPE_LIST.Count;
			}
		}

		// Token: 0x06000722 RID: 1826 RVA: 0x000249CF File Offset: 0x00022BCF
		public static string GetTitle(int no)
		{
			return Data.COUNSELING_TYPE_LIST[no - 1];
		}

		// Token: 0x06000723 RID: 1827 RVA: 0x000249DE File Offset: 0x00022BDE
		public static string Get(int no)
		{
			return CounselingResult.Get(CounselingResult.GetTitle(no));
		}

		// Token: 0x06000724 RID: 1828 RVA: 0x000249EB File Offset: 0x00022BEB
		public static string Get(string scenarioID)
		{
			return PlayerPrefs.GetString(CounselingResult.label + scenarioID + "Result");
		}

		// Token: 0x06000725 RID: 1829 RVA: 0x00024A02 File Offset: 0x00022C02
		public static void Set(string scenarioID, string value)
		{
			PlayerPrefs.SetString(CounselingResult.label + scenarioID + "Result", value);
			Data.UpdateCommentList();
		}

		// Token: 0x06000726 RID: 1830 RVA: 0x00024A1F File Offset: 0x00022C1F
		public static void Read(string scenarioID)
		{
			PlayerPrefs.SetInt(CounselingResult.label + scenarioID + "New", 1);
		}

		// Token: 0x06000727 RID: 1831 RVA: 0x00024A37 File Offset: 0x00022C37
		public static bool IsNew(string scenarioID)
		{
			return CounselingResult.Get(scenarioID) != "" && PlayerPrefs.GetInt(CounselingResult.label + scenarioID + "New", 0) == 0;
		}

		// Token: 0x06000728 RID: 1832 RVA: 0x00024A66 File Offset: 0x00022C66
		// Note: this type is marked as 'beforefieldinit'.
		static CounselingResult()
		{
		}

		// Token: 0x04000267 RID: 615
		private static string label = "Counseling";
	}
}
