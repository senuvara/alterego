﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.SceneManagement;

namespace App
{
	// Token: 0x0200009F RID: 159
	[AppUtil.TitleAttribute("デバッグ機能")]
	public static class DebugFunctions
	{
		// Token: 0x1700008C RID: 140
		// (get) Token: 0x060006D5 RID: 1749 RVA: 0x000240B0 File Offset: 0x000222B0
		// (set) Token: 0x060006D6 RID: 1750 RVA: 0x000240CA File Offset: 0x000222CA
		[AppUtil.TitleAttribute("シーン切替")]
		[AppUtil.ArrayValueAttribute("SCENE_LIST")]
		public static string SCENE
		{
			get
			{
				return SceneManager.GetActiveScene().name;
			}
			set
			{
				Debug.Log("set SCENE " + value);
				SceneManager.LoadScene(value);
			}
		}

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x060006D7 RID: 1751 RVA: 0x000240E4 File Offset: 0x000222E4
		[AppUtil.HideAttribute]
		public static string[] SCENE_LIST
		{
			get
			{
				int sceneCountInBuildSettings = SceneManager.sceneCountInBuildSettings;
				string[] array = new string[sceneCountInBuildSettings];
				for (int i = 0; i < sceneCountInBuildSettings; i++)
				{
					string text = SceneUtility.GetScenePathByBuildIndex(i).Replace("Assets/AppData/Scenes/", "").Replace(".unity", "");
					array[i] = text;
				}
				return array;
			}
		}

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x060006D8 RID: 1752 RVA: 0x00024134 File Offset: 0x00022334
		// (set) Token: 0x060006D9 RID: 1753 RVA: 0x0002413B File Offset: 0x0002233B
		[AppUtil.TitleAttribute("所持EGO")]
		public static EgoPoint EgoPoint2
		{
			get
			{
				return PlayerStatus.EgoPoint;
			}
			set
			{
				PlayerStatus.EgoPoint = value;
			}
		}

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x060006DA RID: 1754 RVA: 0x00021B88 File Offset: 0x0001FD88
		// (set) Token: 0x060006DB RID: 1755 RVA: 0x00024143 File Offset: 0x00022343
		[AppUtil.TitleAttribute("時間を進める(h)")]
		public static float TIME_FORWARD
		{
			get
			{
				return 0f;
			}
			set
			{
				TimeManager.Add(TimeManager.TYPE.REAL_TOTAL, TimeSpan.FromHours((double)(value * Settings.GAME_SPEED)));
				PlayerStatus.EnableDailyBonus = true;
			}
		}

		// Token: 0x060006DC RID: 1756 RVA: 0x0002415E File Offset: 0x0002235E
		private static IEnumerator LOAD_GSSDATA_METHOD()
		{
			string savedata = null;
			yield return GssDataHelper.GetText("1csBkwtdEPOM-dk0kNER5lvOsPDmWs2Gft9y_CGyr898", "SaveData_v2", delegate(string result)
			{
				savedata = result;
			}, null);
			string[] array = savedata.Split(new char[]
			{
				'\n'
			})[2].Split(new char[]
			{
				'\t'
			});
			string[] array2 = savedata.Split(new char[]
			{
				'\n'
			})[3].Split(new char[]
			{
				'\t'
			});
			for (int i = 2; i < array.Length; i++)
			{
				StringStatusConverter.SetEachProperty(array[i], array2[i]);
			}
			yield break;
		}

		// Token: 0x060006DD RID: 1757 RVA: 0x00024166 File Offset: 0x00022366
		// Note: this type is marked as 'beforefieldinit'.
		static DebugFunctions()
		{
		}

		// Token: 0x0400024F RID: 591
		[AppUtil.TitleAttribute("Analyticsデータ送信")]
		public static Action SEND_ANALYTICS = delegate()
		{
			AnalyticsManager.SendEvent(new string[]
			{
				"デバッグ",
				"",
				""
			}, "1DLWkIkaBFBnrV9VoNi09qwvOhocUADC8BWVHfNzyjfA");
		};

		// Token: 0x04000250 RID: 592
		[AppUtil.TitleAttribute("ロード")]
		public static AppUtil.Coroutine LOAD_GSSDATA = new AppUtil.Coroutine(DebugFunctions.LOAD_GSSDATA_METHOD);

		// Token: 0x0200015D RID: 349
		[CompilerGenerated]
		private sealed class <>c__DisplayClass13_0
		{
			// Token: 0x06000A64 RID: 2660 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c__DisplayClass13_0()
			{
			}

			// Token: 0x06000A65 RID: 2661 RVA: 0x000313A2 File Offset: 0x0002F5A2
			internal void <LOAD_GSSDATA_METHOD>b__0(string result)
			{
				this.savedata = result;
			}

			// Token: 0x0400053F RID: 1343
			public string savedata;
		}

		// Token: 0x0200015E RID: 350
		[CompilerGenerated]
		private sealed class <LOAD_GSSDATA_METHOD>d__13 : IEnumerator<object>, IEnumerator, IDisposable
		{
			// Token: 0x06000A66 RID: 2662 RVA: 0x000313AB File Offset: 0x0002F5AB
			[DebuggerHidden]
			public <LOAD_GSSDATA_METHOD>d__13(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x06000A67 RID: 2663 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06000A68 RID: 2664 RVA: 0x000313BC File Offset: 0x0002F5BC
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				if (num == 0)
				{
					this.<>1__state = -1;
					CS$<>8__locals1 = new DebugFunctions.<>c__DisplayClass13_0();
					CS$<>8__locals1.savedata = null;
					this.<>2__current = GssDataHelper.GetText("1csBkwtdEPOM-dk0kNER5lvOsPDmWs2Gft9y_CGyr898", "SaveData_v2", new Action<string>(CS$<>8__locals1.<LOAD_GSSDATA_METHOD>b__0), null);
					this.<>1__state = 1;
					return true;
				}
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				string[] array = CS$<>8__locals1.savedata.Split(new char[]
				{
					'\n'
				})[2].Split(new char[]
				{
					'\t'
				});
				string[] array2 = CS$<>8__locals1.savedata.Split(new char[]
				{
					'\n'
				})[3].Split(new char[]
				{
					'\t'
				});
				for (int i = 2; i < array.Length; i++)
				{
					StringStatusConverter.SetEachProperty(array[i], array2[i]);
				}
				return false;
			}

			// Token: 0x1700016B RID: 363
			// (get) Token: 0x06000A69 RID: 2665 RVA: 0x000314A4 File Offset: 0x0002F6A4
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000A6A RID: 2666 RVA: 0x00026A5A File Offset: 0x00024C5A
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700016C RID: 364
			// (get) Token: 0x06000A6B RID: 2667 RVA: 0x000314A4 File Offset: 0x0002F6A4
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x04000540 RID: 1344
			private int <>1__state;

			// Token: 0x04000541 RID: 1345
			private object <>2__current;

			// Token: 0x04000542 RID: 1346
			private DebugFunctions.<>c__DisplayClass13_0 <>8__1;
		}

		// Token: 0x0200015F RID: 351
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000A6C RID: 2668 RVA: 0x000314AC File Offset: 0x0002F6AC
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000A6D RID: 2669 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c()
			{
			}

			// Token: 0x06000A6E RID: 2670 RVA: 0x000314B8 File Offset: 0x0002F6B8
			internal void <.cctor>b__14_0()
			{
				AnalyticsManager.SendEvent(new string[]
				{
					"デバッグ",
					"",
					""
				}, "1DLWkIkaBFBnrV9VoNi09qwvOhocUADC8BWVHfNzyjfA");
			}

			// Token: 0x04000543 RID: 1347
			public static readonly DebugFunctions.<>c <>9 = new DebugFunctions.<>c();
		}
	}
}
