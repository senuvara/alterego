﻿using System;
using UnityEngine;

namespace App
{
	// Token: 0x020000AA RID: 170
	[AppUtil.DataRangeAttribute]
	[AppUtil.TitleAttribute("目標達成")]
	public static class PrizeLevel
	{
		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x06000742 RID: 1858 RVA: 0x00021B8F File Offset: 0x0001FD8F
		public static int Min
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170000B6 RID: 182
		// (get) Token: 0x06000743 RID: 1859 RVA: 0x00024CF6 File Offset: 0x00022EF6
		public static int Max
		{
			get
			{
				return Data.PRIZE_DATA.Count;
			}
		}

		// Token: 0x06000744 RID: 1860 RVA: 0x00024D02 File Offset: 0x00022F02
		public static string GetTitle(int no)
		{
			return Data.PRIZE_DATA[no - 1][1];
		}

		// Token: 0x06000745 RID: 1861 RVA: 0x00024D13 File Offset: 0x00022F13
		public static int Get(string name)
		{
			return PlayerPrefs.GetInt(name + PrizeLevel.label, 0);
		}

		// Token: 0x06000746 RID: 1862 RVA: 0x00024D26 File Offset: 0x00022F26
		public static void Set(string name, int value)
		{
			PlayerPrefs.SetInt(name + PrizeLevel.label, value);
			Data.UpdateEgoBookPower();
		}

		// Token: 0x06000747 RID: 1863 RVA: 0x00024D3E File Offset: 0x00022F3E
		public static void Add(string name, int add = 1)
		{
			PrizeLevel.Set(name, PrizeLevel.Get(name) + add);
		}

		// Token: 0x06000748 RID: 1864 RVA: 0x00024D4E File Offset: 0x00022F4E
		// Note: this type is marked as 'beforefieldinit'.
		static PrizeLevel()
		{
		}

		// Token: 0x0400026B RID: 619
		private static string label = "目標達成";
	}
}
