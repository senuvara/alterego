﻿using System;
using UnityEngine;

namespace App
{
	// Token: 0x020000AB RID: 171
	[AppUtil.TitleAttribute("実績")]
	public static class PlayerResult
	{
		// Token: 0x170000B7 RID: 183
		// (get) Token: 0x06000749 RID: 1865 RVA: 0x00024D5A File Offset: 0x00022F5A
		// (set) Token: 0x0600074A RID: 1866 RVA: 0x00024D67 File Offset: 0x00022F67
		[AppUtil.TitleAttribute("ページ数合計")]
		public static int BookPageSum
		{
			get
			{
				return PlayerPrefs.GetInt("ResultBookLevel", 0);
			}
			set
			{
				PlayerPrefs.SetInt("ResultBookLevel", value);
			}
		}

		// Token: 0x170000B8 RID: 184
		// (get) Token: 0x0600074B RID: 1867 RVA: 0x00024D74 File Offset: 0x00022F74
		// (set) Token: 0x0600074C RID: 1868 RVA: 0x00024D8A File Offset: 0x00022F8A
		[AppUtil.TitleAttribute("1タップあたりの獲得EGO")]
		public static EgoPoint TapMax
		{
			get
			{
				return new EgoPoint(PlayerPrefs.GetString("ResultTapMax", "0"));
			}
			set
			{
				if (value > PlayerResult.TapMax)
				{
					PlayerPrefs.SetString("ResultTapMax", value.ToString());
				}
			}
		}

		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x0600074D RID: 1869 RVA: 0x00024DA9 File Offset: 0x00022FA9
		// (set) Token: 0x0600074E RID: 1870 RVA: 0x00024DB6 File Offset: 0x00022FB6
		[AppUtil.TitleAttribute("吹き出しタップ回数")]
		public static int TapCount
		{
			get
			{
				return PlayerPrefs.GetInt("ResultTapCount", 0);
			}
			set
			{
				PlayerPrefs.SetInt("ResultTapCount", value);
			}
		}

		// Token: 0x170000BA RID: 186
		// (get) Token: 0x0600074F RID: 1871 RVA: 0x00024DC3 File Offset: 0x00022FC3
		// (set) Token: 0x06000750 RID: 1872 RVA: 0x00024DD0 File Offset: 0x00022FD0
		[AppUtil.TitleAttribute("動画広告視聴回数")]
		public static int AdMovieCount
		{
			get
			{
				return PlayerPrefs.GetInt("ResultAdMovieCount", 0);
			}
			set
			{
				PlayerPrefs.SetInt("ResultAdMovieCount", value);
			}
		}

		// Token: 0x170000BB RID: 187
		// (get) Token: 0x06000751 RID: 1873 RVA: 0x00024DE0 File Offset: 0x00022FE0
		[AppUtil.TitleAttribute("目標達成回数")]
		public static int PrizeCount
		{
			get
			{
				int num = 0;
				for (int i = PrizeLevel.Min; i <= PrizeLevel.Max; i++)
				{
					num += PrizeLevel.Get(PrizeLevel.GetTitle(i));
				}
				return num;
			}
		}

		// Token: 0x170000BC RID: 188
		// (get) Token: 0x06000752 RID: 1874 RVA: 0x00024E12 File Offset: 0x00023012
		// (set) Token: 0x06000753 RID: 1875 RVA: 0x00024E23 File Offset: 0x00023023
		[AppUtil.TitleAttribute("エンディング到達")]
		public static string Ending
		{
			get
			{
				return PlayerPrefs.GetString("ResultEnding", "");
			}
			set
			{
				PlayerPrefs.SetString("ResultEnding", value);
			}
		}

		// Token: 0x170000BD RID: 189
		// (get) Token: 0x06000754 RID: 1876 RVA: 0x00024E30 File Offset: 0x00023030
		[AppUtil.HideAttribute]
		public static int EndingCount
		{
			get
			{
				return PlayerResult.Ending.Length / 2;
			}
		}

		// Token: 0x170000BE RID: 190
		// (get) Token: 0x06000755 RID: 1877 RVA: 0x00024E3E File Offset: 0x0002303E
		// (set) Token: 0x06000756 RID: 1878 RVA: 0x00024E4B File Offset: 0x0002304B
		[AppUtil.TitleAttribute("ログイン日数")]
		public static int LoginCount
		{
			get
			{
				return PlayerPrefs.GetInt("ResultLoginCount", 0);
			}
			set
			{
				PlayerPrefs.SetInt("ResultLoginCount", value);
			}
		}

		// Token: 0x170000BF RID: 191
		// (get) Token: 0x06000757 RID: 1879 RVA: 0x00024E58 File Offset: 0x00023058
		// (set) Token: 0x06000758 RID: 1880 RVA: 0x00024E65 File Offset: 0x00023065
		[AppUtil.TitleAttribute("エス反応")]
		public static int EsTapCount
		{
			get
			{
				return PlayerPrefs.GetInt("EsTapCount", 0);
			}
			set
			{
				PlayerPrefs.SetInt("EsTapCount", value);
			}
		}

		// Token: 0x170000C0 RID: 192
		// (get) Token: 0x06000759 RID: 1881 RVA: 0x00024E72 File Offset: 0x00023072
		// (set) Token: 0x0600075A RID: 1882 RVA: 0x00024E7F File Offset: 0x0002307F
		[AppUtil.TitleAttribute("雑談回数")]
		public static int EsTalkCount
		{
			get
			{
				return PlayerPrefs.GetInt("EsTalkCount", 0);
			}
			set
			{
				PlayerPrefs.SetInt("EsTalkCount", value);
			}
		}

		// Token: 0x170000C1 RID: 193
		// (get) Token: 0x0600075B RID: 1883 RVA: 0x00024E8C File Offset: 0x0002308C
		// (set) Token: 0x0600075C RID: 1884 RVA: 0x00024E99 File Offset: 0x00023099
		[AppUtil.TitleAttribute("カタルシス文字数")]
		public static int CatharsisLength
		{
			get
			{
				return PlayerPrefs.GetInt("CatharsisLength", 0);
			}
			set
			{
				PlayerPrefs.SetInt("CatharsisLength", value);
			}
		}
	}
}
