﻿using System;
using UnityEngine;

namespace App
{
	// Token: 0x020000A5 RID: 165
	public static class ReadFukidasiList
	{
		// Token: 0x170000AA RID: 170
		// (get) Token: 0x06000716 RID: 1814 RVA: 0x000247EC File Offset: 0x000229EC
		private static string[] FukidasiList
		{
			get
			{
				if (ReadFukidasiList._FukidasiList == null)
				{
					ReadFukidasiList._FukidasiList = Resources.Load<TextAsset>("Scenario/FukidasiList").text.Split(new char[]
					{
						'\n'
					});
					if (PlayerPrefs.HasKey("ReadFukidasiList"))
					{
						string[] string_array = JsonUtility.FromJson<AppUtil.JsonString>(PlayerPrefs.GetString("ReadFukidasiList")).string_array;
						for (int i = 0; i < string_array.Length; i++)
						{
							ReadFukidasiList.Add(string_array[i]);
						}
						PlayerPrefs.DeleteKey("ReadFukidasiList");
					}
				}
				return ReadFukidasiList._FukidasiList;
			}
		}

		// Token: 0x170000AB RID: 171
		// (get) Token: 0x06000717 RID: 1815 RVA: 0x0002486B File Offset: 0x00022A6B
		// (set) Token: 0x06000718 RID: 1816 RVA: 0x00024877 File Offset: 0x00022A77
		public static string fukidasiListFlg
		{
			get
			{
				return PlayerPrefs.GetString("ReadFukidasiFlg");
			}
			set
			{
				PlayerPrefs.SetString("ReadFukidasiFlg", value);
			}
		}

		// Token: 0x06000719 RID: 1817 RVA: 0x00024884 File Offset: 0x00022A84
		public static bool Contains(string key)
		{
			int num = Array.IndexOf<string>(ReadFukidasiList.FukidasiList, key);
			return ReadFukidasiList.ConvertBitFlg(ReadFukidasiList.fukidasiListFlg, num + 1, true) == "OK";
		}

		// Token: 0x0600071A RID: 1818 RVA: 0x000248B8 File Offset: 0x00022AB8
		public static bool Add(string key)
		{
			int num = Array.IndexOf<string>(ReadFukidasiList.FukidasiList, key);
			string text = ReadFukidasiList.ConvertBitFlg(ReadFukidasiList.fukidasiListFlg, num + 1, false);
			if (text != ReadFukidasiList.fukidasiListFlg)
			{
				ReadFukidasiList.fukidasiListFlg = text;
				return true;
			}
			return false;
		}

		// Token: 0x0600071B RID: 1819 RVA: 0x000248F8 File Offset: 0x00022AF8
		private static string ConvertBitFlg(string baseBit, int target, bool contains)
		{
			int num = (target - 1) / 4;
			int flagNum = (target - 1) % 4;
			while (baseBit.Length < num + 1)
			{
				baseBit += "0";
			}
			int baseNum = Convert.ToInt32(baseBit[num].ToString(), 16);
			if (!contains)
			{
				int num2 = AppUtil.SetBitFlag(baseNum, flagNum, true);
				baseBit = baseBit.Remove(num, 1);
				baseBit = baseBit.Insert(num, num2.ToString("X"));
				return baseBit;
			}
			if (!AppUtil.BitFlagIsSet(baseNum, flagNum))
			{
				return "NG";
			}
			return "OK";
		}

		// Token: 0x04000266 RID: 614
		private static string[] _FukidasiList;
	}
}
