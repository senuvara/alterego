﻿using System;

namespace App
{
	// Token: 0x020000A6 RID: 166
	public static class BonusTime
	{
		// Token: 0x0600071C RID: 1820 RVA: 0x00024984 File Offset: 0x00022B84
		public static void SetBonus()
		{
			TimeSpan offset = TimeSpan.FromSeconds(600.9000244140625);
			TimeManager.Reset(TimeManager.TYPE.END_BONUS, offset);
		}

		// Token: 0x170000AC RID: 172
		// (get) Token: 0x0600071D RID: 1821 RVA: 0x000249A7 File Offset: 0x00022BA7
		public static bool IsActive
		{
			get
			{
				return TimeManager.IsInTime(TimeManager.TYPE.END_BONUS);
			}
		}

		// Token: 0x170000AD RID: 173
		// (get) Token: 0x0600071E RID: 1822 RVA: 0x000249AF File Offset: 0x00022BAF
		public static int BonusValue
		{
			get
			{
				if (!BonusTime.IsActive)
				{
					return 1;
				}
				return 3;
			}
		}

		// Token: 0x170000AE RID: 174
		// (get) Token: 0x0600071F RID: 1823 RVA: 0x000249BB File Offset: 0x00022BBB
		public static TimeSpan TimeLeft
		{
			get
			{
				return TimeManager.GetGapTime(TimeManager.TYPE.END_BONUS);
			}
		}
	}
}
