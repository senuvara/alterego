﻿using System;

namespace App
{
	// Token: 0x0200009C RID: 156
	public static class AppInfo
	{
		// Token: 0x060006D1 RID: 1745 RVA: 0x00023E8C File Offset: 0x0002208C
		// Note: this type is marked as 'beforefieldinit'.
		static AppInfo()
		{
		}

		// Token: 0x04000243 RID: 579
		public const string ANALYTICS_VERSION = "EGO_005";

		// Token: 0x04000244 RID: 580
		public const string RESOURCES_FOLDER = "Assets/AppData/Resources";

		// Token: 0x04000245 RID: 581
		public const string APPSTORE_ID = "1447605099";

		// Token: 0x04000246 RID: 582
		public const string SHARE_TAG = "#ALTEREGO";

		// Token: 0x04000247 RID: 583
		public const string SHARE_URL = "caracolu.com/app/alterego/";

		// Token: 0x04000248 RID: 584
		public const string SHARE_DEFAULT = "自分探しタップゲーム『ALTER EGO』 caracolu.com/app/alterego/ #ALTEREGO";

		// Token: 0x04000249 RID: 585
		public static readonly Type[] DefaultComponent = new Type[]
		{
			typeof(RemoteSettingsReceiver),
			typeof(AudioManager),
			typeof(AdManager),
			typeof(InAppPurchaser),
			typeof(TimeManager)
		};

		// Token: 0x0400024A RID: 586
		public static readonly Type[] PlayerDataList = new Type[]
		{
			typeof(PurchasingItem),
			typeof(PlayerStatus),
			typeof(BookLevel),
			typeof(PrizeLevel),
			typeof(PlayerResult),
			typeof(CounselingResult),
			typeof(UserChoice),
			typeof(TimeManager),
			typeof(ReadFukidasiList),
			typeof(Settings),
			typeof(DebugFunctions)
		};
	}
}
