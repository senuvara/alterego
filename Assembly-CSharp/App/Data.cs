﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace App
{
	// Token: 0x020000AC RID: 172
	public static class Data
	{
		// Token: 0x0600075D RID: 1885 RVA: 0x00024EA6 File Offset: 0x000230A6
		static Data()
		{
			Data.Init();
			Data.ReadData();
		}

		// Token: 0x0600075E RID: 1886 RVA: 0x00024EC8 File Offset: 0x000230C8
		public static void Init()
		{
			Data.comment_list = null;
			Data._clear_scenario_list = null;
			Data._EgoPerSecond = null;
			Data._EgoBookPower = null;
			Data._NEXT_PRICE_LIST = null;
		}

		// Token: 0x0600075F RID: 1887 RVA: 0x00024EE8 File Offset: 0x000230E8
		public static void ReadData()
		{
			Data.DIC = new Dictionary<string, string[]>();
			Data.SCENARIO_DATA = new List<string[]>();
			Data.SCENARIO_DATA_KEY = new List<string>();
			Data.COUNSELING_TYPE_LIST = new List<string>();
			Data.CHOICE_TYPE_LIST = new List<string>();
			Data.PRIZE_DATA = new List<string[]>();
			Data.SOUND_DATA = new List<string[]>();
			Data.BOOKEGO_DATA = new List<string[]>();
			LanguageManager.STRINGS_TABLE.Clear();
			LanguageManager.FONT_TABLE.Clear();
			Data.LastUpdateTime = DateTime.Parse("1900/1/1");
			foreach (string str in new string[]
			{
				"scenario",
				"data"
			})
			{
				Data.UpdateGameData(Resources.Load<TextAsset>("Scenario/" + str + "-UTF8.txt").text.Replace("\t", "\\t"), true);
			}
			foreach (string[] array2 in Data.SCENARIO_DATA)
			{
				if (array2[1].Contains("診断") && !array2[2].Contains("ルート分岐"))
				{
					Data.COUNSELING_TYPE_LIST.Add(array2[1]);
				}
				if (!array2[0].Contains("4章") && (array2[1].Contains("診断") || array2[1].Contains("対話")) && !Data.CHOICE_TYPE_LIST.Contains(array2[1]))
				{
					Data.CHOICE_TYPE_LIST.Add(array2[1]);
				}
			}
			Data.COUNSELING_TYPE_LIST.Add("ゲームクリア");
			foreach (string text in Resources.Load<TextAsset>("Scenario/BookEgoList.txt").text.Split(new char[]
			{
				'\n'
			}))
			{
				Data.BOOKEGO_DATA.Add(text.Split(new char[]
				{
					'\t'
				}));
			}
		}

		// Token: 0x06000760 RID: 1888 RVA: 0x000250E0 File Offset: 0x000232E0
		public static int[] GET_COUNSELING_TABLE(string scenarioType, int index)
		{
			return Array.ConvertAll<string, int>(Data.DIC[scenarioType + "_CDATA_" + index], new Converter<string, int>(int.Parse));
		}

		// Token: 0x06000761 RID: 1889 RVA: 0x00025110 File Offset: 0x00023310
		public static string[] GET_COUNSELING_TYPE(string scenarioType)
		{
			string key = scenarioType + "_CTYPE";
			if (Data.DIC.ContainsKey(key))
			{
				return Data.DIC[key];
			}
			return null;
		}

		// Token: 0x06000762 RID: 1890 RVA: 0x00025143 File Offset: 0x00023343
		public static EgoPoint[] BOOK_PARAMETER(int index)
		{
			return Array.ConvertAll<string, EgoPoint>(Data.DIC["BOOK_PARAMETER" + index], (string s) => new EgoPoint(s));
		}

		// Token: 0x170000C2 RID: 194
		// (get) Token: 0x06000763 RID: 1891 RVA: 0x00025183 File Offset: 0x00023383
		public static string[] COMMENT_KEY_LIST
		{
			get
			{
				if (Data.comment_list == null)
				{
					Data.UpdateCommentList();
				}
				return Data.comment_list.ToArray();
			}
		}

		// Token: 0x06000764 RID: 1892 RVA: 0x0002519C File Offset: 0x0002339C
		public static void UpdateCommentList()
		{
			Data.comment_list = new List<string>();
			bool flag = false;
			string scenarioNo = PlayerStatus.ScenarioNo;
			if (!(scenarioNo == "3章ID-6") && !(scenarioNo == "4章ID-1") && !(scenarioNo == "4章ID-2"))
			{
				if (scenarioNo == "3章SE-6" || scenarioNo == "4章SE-1" || scenarioNo == "4章SE-2")
				{
					flag = true;
				}
			}
			else
			{
				flag = true;
			}
			if (flag)
			{
				foreach (string text in PlayerStatus.SCENARIO_NO_LIST)
				{
					if (text.Contains(PlayerStatus.Route))
					{
						string scenarioType = Data.GetScenarioType(text);
						string arg = "[Es]" + scenarioType + "_吹出";
						int num = 1;
						while (num <= 8 && LanguageManager.Contains(arg + num))
						{
							Data.comment_list.Add(arg + num);
							num++;
						}
					}
				}
				return;
			}
			Data.comment_list.Add("[Fukidasi]Base");
			for (int j = CounselingResult.Min; j <= CounselingResult.Max; j++)
			{
				for (int k = 1; k <= 10; k++)
				{
					string title = CounselingResult.GetTitle(j);
					string text2 = string.Concat(new object[]
					{
						"[Fukidasi]CR",
						title,
						"_",
						CounselingResult.Get(title),
						"-",
						k
					});
					if (!LanguageManager.Contains(text2))
					{
						break;
					}
					Data.comment_list.Add(text2);
				}
			}
			for (int l = 1; l <= BookLevel.Max; l++)
			{
				int rank = BookLevel.GetRank(l.ToString());
				for (int m = 1; m <= 5; m++)
				{
					if (rank >= m)
					{
						string text3 = string.Concat(new object[]
						{
							"[Fukidasi]Book",
							l,
							"Rank",
							m
						});
						if (LanguageManager.Contains(text3) && LanguageManager.Get(text3) != "0")
						{
							Data.comment_list.Add(text3);
						}
					}
				}
			}
			if (PlayerStatus.Route != "ID" && PlayerStatus.Route != "SE")
			{
				return;
			}
			for (int n = UserChoice.Min; n <= UserChoice.Max; n++)
			{
				string title2 = UserChoice.GetTitle(n);
				string arg2 = "[Es]" + title2 + "_吹出";
				if (LanguageManager.Contains(arg2 + 1))
				{
					int num2 = 0;
					if (PlayerStatus.Route == "ID")
					{
						if (UserChoice.Get(title2) == "Chaos")
						{
							num2 = 4;
						}
						else if (UserChoice.Get(title2) == "Neutral")
						{
							num2 = 2;
						}
					}
					else if (PlayerStatus.Route == "SE")
					{
						if (UserChoice.Get(title2) == "Law")
						{
							num2 = 4;
						}
						else if (UserChoice.Get(title2) == "Neutral")
						{
							num2 = 2;
						}
					}
					int num3 = 1;
					while (num3 <= num2 && LanguageManager.Contains(arg2 + num3))
					{
						Data.comment_list.Add(arg2 + num3);
						num3++;
					}
				}
			}
		}

		// Token: 0x170000C3 RID: 195
		// (get) Token: 0x06000765 RID: 1893 RVA: 0x000254F8 File Offset: 0x000236F8
		// (set) Token: 0x06000766 RID: 1894 RVA: 0x0002550B File Offset: 0x0002370B
		public static EgoPoint EgoPerSecond
		{
			get
			{
				if (Data._EgoPerSecond == null)
				{
					Data.UpdateEgoPerSecond();
				}
				return Data._EgoPerSecond;
			}
			set
			{
				Data._EgoPerSecond = value;
			}
		}

		// Token: 0x06000767 RID: 1895 RVA: 0x00025514 File Offset: 0x00023714
		public static void UpdateEgoPerSecond()
		{
			EgoPoint egoPoint = new EgoPoint(0f);
			for (int i = 1; i <= BookLevel.Max; i++)
			{
				egoPoint += BookEgoPoint.GetBookEgo(i.ToString(), "per_second", BookLevel.Get(i.ToString()));
			}
			Data._EgoPerSecond = egoPoint;
			if (GameObject.Find("BookListView") != null)
			{
				GameObject.Find("BookListView").BroadcastMessage("UpdateButton");
			}
		}

		// Token: 0x170000C4 RID: 196
		// (get) Token: 0x06000768 RID: 1896 RVA: 0x0002558C File Offset: 0x0002378C
		public static List<string> CLEAR_SCENARIO_LIST
		{
			get
			{
				if (Data._clear_scenario_list == null)
				{
					Data.UpdateClearScenarioList();
				}
				return Data._clear_scenario_list;
			}
		}

		// Token: 0x06000769 RID: 1897 RVA: 0x000255A0 File Offset: 0x000237A0
		public static void UpdateClearScenarioList()
		{
			Data._clear_scenario_list = new List<string>();
			if (PlayerStatus.ScenarioNo == "退行")
			{
				return;
			}
			string route = PlayerStatus.Route;
			foreach (string text in PlayerStatus.SCENARIO_NO_LIST)
			{
				if (text == PlayerStatus.ScenarioNo)
				{
					break;
				}
				if (PlayerStatus.ScenarioNo.Contains("4章AE"))
				{
					if (!text.Contains("ID") && !text.Contains("SE"))
					{
						Data._clear_scenario_list.Add(text);
					}
				}
				else if (!text.Contains("3章") || !(PlayerStatus.Route != PlayerStatus.Route3))
				{
					if (text.Contains("ID") || text.Contains("SE") || text.Contains("AE"))
					{
						if (text.Contains(route))
						{
							Data._clear_scenario_list.Add(text);
						}
					}
					else
					{
						Data._clear_scenario_list.Add(text);
					}
				}
			}
			if (Data._clear_scenario_list.Contains("1章-3") && TimeManager.Get(TimeManager.TYPE.NEXT_BONUS_ES) == TimeSpan.MaxValue)
			{
				TimeManager.Reset(TimeManager.TYPE.NEXT_BONUS_ES, TimeSpan.FromMinutes(0.20000000298023224));
				TimeManager.Reset(TimeManager.TYPE.NEXT_BONUS_BOOK);
			}
			if (!PlayerStatus.ScenarioNo.Contains("4章AE") && PlayerStatus.Route != PlayerStatus.Route3)
			{
				Data._clear_scenario_list.AddRange(new string[]
				{
					"3章AE-1",
					"3章AE-2",
					"3章AE-3",
					"3章AE-4"
				});
				Data._clear_scenario_list.Add("3章" + PlayerStatus.Route + "-5");
				if (PlayerStatus.ScenarioNo.Contains("4章"))
				{
					Data._clear_scenario_list.Add("3章" + PlayerStatus.Route + "-6");
				}
			}
		}

		// Token: 0x0600076A RID: 1898 RVA: 0x0002577D File Offset: 0x0002397D
		private static EgoPoint NEXT_PRICE_LIST(Data.PRICE index)
		{
			if (Data._NEXT_PRICE_LIST == null)
			{
				Data.UpdatePriceList();
			}
			return Data._NEXT_PRICE_LIST[(int)index];
		}

		// Token: 0x0600076B RID: 1899 RVA: 0x00025794 File Offset: 0x00023994
		public static void UpdatePriceList()
		{
			Data._NEXT_PRICE_LIST = new EgoPoint[3];
			EgoPoint egoPoint = null;
			for (int i = 1; i <= BookLevel.Max; i++)
			{
				if (BookLevel.Get(i.ToString()) < 0)
				{
					egoPoint = BookEgoPoint.GetBookEgo(i.ToString(), "price", -1);
					break;
				}
			}
			Data._NEXT_PRICE_LIST[0] = egoPoint;
			EgoPoint egoPoint2 = null;
			string[] scenarioItemData = Data.GetScenarioItemData(PlayerStatus.ScenarioNo);
			if (scenarioItemData != null && !string.IsNullOrEmpty(scenarioItemData[0]))
			{
				egoPoint2 = new EgoPoint(scenarioItemData[0]);
			}
			Data._NEXT_PRICE_LIST[1] = egoPoint2;
			EgoPoint egoPoint3 = new EgoPoint(0f);
			if (!Data.ReadingBookIsOver)
			{
				egoPoint3 = new EgoPoint(GssDataHelper.GetData("その他", "読書消費EGO_A")[1]);
				int intParameter = Utility.GetIntParameter("読書消費EGO_B");
				for (int j = 0; j < PlayerStatus.ReadingBookList.Length; j++)
				{
					egoPoint3 *= (float)intParameter;
				}
			}
			Data._NEXT_PRICE_LIST[2] = egoPoint3;
		}

		// Token: 0x170000C5 RID: 197
		// (get) Token: 0x0600076C RID: 1900 RVA: 0x00025879 File Offset: 0x00023A79
		public static EgoPoint NEXT_BOOK_PRICE
		{
			get
			{
				return Data.NEXT_PRICE_LIST(Data.PRICE.BOOK);
			}
		}

		// Token: 0x170000C6 RID: 198
		// (get) Token: 0x0600076D RID: 1901 RVA: 0x00025881 File Offset: 0x00023A81
		public static EgoPoint NEXT_SCENARIO_PRICE
		{
			get
			{
				return Data.NEXT_PRICE_LIST(Data.PRICE.SCENARIO);
			}
		}

		// Token: 0x170000C7 RID: 199
		// (get) Token: 0x0600076E RID: 1902 RVA: 0x00025889 File Offset: 0x00023A89
		public static EgoPoint NEXT_HAYAKAWA_BOOK_PRICE
		{
			get
			{
				return Data.NEXT_PRICE_LIST(Data.PRICE.HAYAKAWA);
			}
		}

		// Token: 0x170000C8 RID: 200
		// (get) Token: 0x0600076F RID: 1903 RVA: 0x00025891 File Offset: 0x00023A91
		public static bool ReadingBookIsOver
		{
			get
			{
				return PlayerStatus.ReadingBookList.Length >= 30;
			}
		}

		// Token: 0x06000770 RID: 1904 RVA: 0x000258A4 File Offset: 0x00023AA4
		public static void GoToNextScenario()
		{
			PlayerStatus.ScenarioNo = Data.GetNextScenarioNo();
		}

		// Token: 0x06000771 RID: 1905 RVA: 0x000258B0 File Offset: 0x00023AB0
		public static string GetNextScenarioNo()
		{
			string scenarioNo = PlayerStatus.ScenarioNo;
			if (scenarioNo == "3章ID-6")
			{
				return "4章ID-1";
			}
			if (scenarioNo == "3章SE-6")
			{
				return "4章SE-1";
			}
			if (scenarioNo == "3章AE-6")
			{
				return "4章AE-1";
			}
			int num = Data.GetScenarioIndex(PlayerStatus.ScenarioNo);
			if (num < 0)
			{
				return "";
			}
			num++;
			if (num >= Data.SCENARIO_DATA_KEY.Count)
			{
				return "";
			}
			return Data.SCENARIO_DATA_KEY[num];
		}

		// Token: 0x06000772 RID: 1906 RVA: 0x00025935 File Offset: 0x00023B35
		public static string GetNextScenarioSpecific()
		{
			return Data.GetScenarioSpecific(Data.GetNextScenarioNo());
		}

		// Token: 0x06000773 RID: 1907 RVA: 0x00025941 File Offset: 0x00023B41
		public static int GetScenarioIndex(string no)
		{
			return Data.SCENARIO_DATA_KEY.IndexOf(no);
		}

		// Token: 0x06000774 RID: 1908 RVA: 0x00025950 File Offset: 0x00023B50
		public static string GetScenarioType(string no)
		{
			int scenarioIndex = Data.GetScenarioIndex(no);
			if (scenarioIndex < 0)
			{
				return "";
			}
			return Data.SCENARIO_DATA[scenarioIndex][1];
		}

		// Token: 0x06000775 RID: 1909 RVA: 0x0002597C File Offset: 0x00023B7C
		public static string GetScenarioSpecific(string no = "")
		{
			if (no == "")
			{
				no = PlayerStatus.ScenarioNo;
			}
			int scenarioIndex = Data.GetScenarioIndex(no);
			if (scenarioIndex < 0)
			{
				return "";
			}
			if (scenarioIndex >= Data.SCENARIO_DATA.Count)
			{
				return "";
			}
			return Data.SCENARIO_DATA[scenarioIndex][2];
		}

		// Token: 0x06000776 RID: 1910 RVA: 0x000259D0 File Offset: 0x00023BD0
		public static string[] GetScenarioItemData(string no)
		{
			int scenarioIndex = Data.GetScenarioIndex(no);
			if (scenarioIndex < 0)
			{
				return null;
			}
			return new List<string>
			{
				Data.SCENARIO_DATA[scenarioIndex][3],
				Data.SCENARIO_DATA[scenarioIndex][4],
				Data.SCENARIO_DATA[scenarioIndex][5]
			}.ToArray();
		}

		// Token: 0x06000777 RID: 1911 RVA: 0x00025A30 File Offset: 0x00023C30
		public static string GetScenarioKey(string scenarioType, string type1, string type2 = null, int index = 0)
		{
			string text = "_" + type1;
			if (type2 != null)
			{
				text = text + "-" + type2;
			}
			if (index > 0)
			{
				text = text + "-" + index;
			}
			string text2 = "[Es]" + scenarioType + text;
			if (LanguageManager.Contains(text2))
			{
				return text2;
			}
			return null;
		}

		// Token: 0x06000778 RID: 1912 RVA: 0x00025A87 File Offset: 0x00023C87
		public static bool UpdateGameData(byte[] data)
		{
			return Data.UpdateGameData(AppUtil.ConvertSJISToJson(data), false);
		}

		// Token: 0x06000779 RID: 1913 RVA: 0x00025A98 File Offset: 0x00023C98
		public static bool UpdateGameData(string jsondata, bool init)
		{
			string[] string_array = JsonUtility.FromJson<AppUtil.JsonString>(jsondata).string_array;
			DateTime dateTime = DateTime.Parse(string_array[0]);
			if (dateTime > Data.LastUpdateTime)
			{
				Data.LastUpdateTime = dateTime;
			}
			else if (!init)
			{
				return false;
			}
			for (int i = 1; i < string_array.Length; i++)
			{
				if (string_array[i].StartsWith("[[SheetName]]"))
				{
					string text = string_array[i].Replace("[[SheetName]]", "");
					uint num = <PrivateImplementationDetails>.ComputeStringHash(text);
					if (num <= 1204600759U)
					{
						if (num != 47750331U)
						{
							if (num != 613888204U)
							{
								if (num == 1204600759U)
								{
									if (text == "ScenarioList")
									{
										i = Data.SetScenarioData(string_array, i + 1);
									}
								}
							}
							else if (text == "Translation")
							{
								i = Data.SetLocalizeData(string_array, i + 1, false);
							}
						}
						else if (text == "MasterData")
						{
							i = Data.OverwriteEgoData(string_array, i + 1);
						}
					}
					else if (num <= 2079568635U)
					{
						if (num != 1546649404U)
						{
							if (num == 2079568635U)
							{
								if (text == "UI")
								{
									i = Data.SetLocalizeData(string_array, i + 1, true);
								}
							}
						}
						else if (text == "目標達成")
						{
							i = Data.SetDataArray(string_array, i + 1, Data.PRIZE_DATA);
						}
					}
					else if (num != 2136295023U)
					{
						if (num == 3016399412U)
						{
							if (text == "Sound")
							{
								i = Data.SetDataArray(string_array, i + 1, Data.SOUND_DATA);
							}
						}
					}
					else if (text == "GameData")
					{
						i = Data.SetGameData(string_array, i + 1);
					}
				}
			}
			return true;
		}

		// Token: 0x0600077A RID: 1914 RVA: 0x00025C4C File Offset: 0x00023E4C
		private static int SetGameData(string[] allData, int start)
		{
			int i;
			for (i = start + 1; i < allData.Length; i++)
			{
				string[] array = allData[i].Split(new char[]
				{
					'\t'
				});
				if (array[0].Contains("[[SheetName]]"))
				{
					break;
				}
				if (!(array[1] == ""))
				{
					int num = 0;
					for (int j = 2; j < array.Length; j++)
					{
						if (array[j] != "")
						{
							num++;
						}
					}
					string[] array2 = new string[num];
					Array.Copy(array, 2, array2, 0, num);
					try
					{
						Data.DIC.Add(array[1], array2);
					}
					catch (Exception ex)
					{
						global::Debug.LogError(ex.ToString() + array[1] + ":" + string.Join(",", array2));
					}
				}
			}
			return i - 1;
		}

		// Token: 0x0600077B RID: 1915 RVA: 0x00025D24 File Offset: 0x00023F24
		private static int SetLocalizeData(string[] allData, int start, bool UI)
		{
			int i;
			for (i = start + 1; i < allData.Length; i++)
			{
				string[] array = allData[i].Split(new char[]
				{
					'\t'
				});
				if (array[0].Contains("[[SheetName]]"))
				{
					break;
				}
				string text = array[2];
				if (UI)
				{
					text = array[0];
					LanguageManager.FONT_TABLE.Add(text, new string[]
					{
						array[5],
						array[6]
					});
				}
				if (!(text == ""))
				{
					if (LanguageManager.STRINGS_TABLE.ContainsKey(text))
					{
						global::Debug.LogError("SetLocalizeData key=" + text);
					}
					else
					{
						LanguageManager.STRINGS_TABLE.Add(text, new string[]
						{
							array[3],
							array[4]
						});
					}
				}
			}
			return i - 1;
		}

		// Token: 0x0600077C RID: 1916 RVA: 0x00025DE0 File Offset: 0x00023FE0
		private static int SetScenarioData(string[] allData, int start)
		{
			int i;
			for (i = start + 1; i < allData.Length; i++)
			{
				string[] array = allData[i].Split(new char[]
				{
					'\t'
				});
				if (array[0].Contains("[[SheetName]]"))
				{
					break;
				}
				Data.SCENARIO_DATA_KEY.Add(array[0]);
				Data.SCENARIO_DATA.Add(array);
			}
			return i - 1;
		}

		// Token: 0x0600077D RID: 1917 RVA: 0x00025E3C File Offset: 0x0002403C
		public static int OverwriteEgoData(string[] allData, int start)
		{
			int i;
			for (i = start + 1; i < allData.Length; i++)
			{
				string[] array = allData[i].Split(new char[]
				{
					'\t'
				});
				if (array[0].Contains("[[SheetName]]"))
				{
					break;
				}
				string text = array[0].Replace("[MasterData]", "");
				if (text.Contains("BOOK_"))
				{
					if (Data.DIC.ContainsKey(text))
					{
						Data.DIC[text][0] = array[1];
						Data.DIC[text][1] = array[2];
						Data.DIC[text][2] = array[3];
					}
					else
					{
						Data.DIC.Add(text, AppUtil.GetChildArray(array, 1, false, 0));
					}
				}
				else if (text.Contains("章"))
				{
					int scenarioIndex = Data.GetScenarioIndex(text);
					Data.SCENARIO_DATA[scenarioIndex][3] = array[1];
					Data.SCENARIO_DATA[scenarioIndex][4] = array[2];
					Data.SCENARIO_DATA[scenarioIndex][5] = array[3];
				}
				else if (text.Contains("目標"))
				{
					int index = int.Parse(text.Replace("目標", "")) - 1;
					Data.PRIZE_DATA[index][4] = array[2];
					Data.PRIZE_DATA[index][5] = array[3];
				}
			}
			return i - 1;
		}

		// Token: 0x0600077E RID: 1918 RVA: 0x00025F94 File Offset: 0x00024194
		private static int SetDataArray(string[] allData, int start, List<string[]> outputList)
		{
			int i;
			for (i = start + 1; i < allData.Length; i++)
			{
				string[] array = allData[i].Split(new char[]
				{
					'\t'
				});
				if (array[0].Contains("[[SheetName]]"))
				{
					break;
				}
				outputList.Add(array);
			}
			return i - 1;
		}

		// Token: 0x0600077F RID: 1919 RVA: 0x00025FE0 File Offset: 0x000241E0
		public static void Restart()
		{
			SceneTransition.LoadScene("探求", null, 0.5f);
			BookLevel.Init();
			UserChoice.Init();
			PlayerStatus.Route = "";
			PlayerStatus.Route3 = "";
			PlayerStatus.ScenarioNo = "退行";
			Data.UpdateClearScenarioList();
			Data.UpdateEgoBookPower();
			PlayerStatus.EgoPoint = new EgoPoint(0f);
		}

		// Token: 0x06000780 RID: 1920 RVA: 0x00026048 File Offset: 0x00024248
		public static string GetRoute(string scenarioNo)
		{
			foreach (string text in new string[]
			{
				"ID",
				"SE",
				"AE"
			})
			{
				if (scenarioNo.Contains(text))
				{
					return text;
				}
			}
			return "";
		}

		// Token: 0x06000781 RID: 1921 RVA: 0x00026098 File Offset: 0x00024298
		public static EgoPoint GetEgoTapPower()
		{
			EgoPoint egoPoint = new EgoPoint(1f);
			foreach (string no in Data.CLEAR_SCENARIO_LIST)
			{
				string[] scenarioItemData = Data.GetScenarioItemData(no);
				if (scenarioItemData != null && scenarioItemData[1] == "タップ")
				{
					int num = scenarioItemData[2].Length - 1;
					for (int i = 0; i < num; i++)
					{
						egoPoint *= 10f;
					}
				}
			}
			foreach (string[] array in Data.PRIZE_DATA)
			{
				string text = array[3];
				if (text.Contains("タップ"))
				{
					for (int j = 0; j < PrizeLevel.Get(array[1]); j++)
					{
						string s = text.Replace("タップ獲得EGO ", "").Replace("倍", "");
						egoPoint *= (float)int.Parse(s);
					}
				}
			}
			if (PurchasingItem.GetByLabel("タップ獲得EGO1000倍"))
			{
				egoPoint *= 1000f;
			}
			egoPoint *= (float)BonusTime.BonusValue;
			return egoPoint;
		}

		// Token: 0x170000C9 RID: 201
		// (get) Token: 0x06000782 RID: 1922 RVA: 0x000261EC File Offset: 0x000243EC
		private static List<int> EgoBookPower
		{
			get
			{
				if (Data._EgoBookPower == null)
				{
					Data.UpdateEgoBookPower();
				}
				return Data._EgoBookPower;
			}
		}

		// Token: 0x06000783 RID: 1923 RVA: 0x00026200 File Offset: 0x00024400
		public static void UpdateEgoBookPower()
		{
			Data._EgoBookPower = new List<int>();
			long num = 1L;
			foreach (string no in Data.CLEAR_SCENARIO_LIST)
			{
				string[] scenarioItemData = Data.GetScenarioItemData(no);
				if (scenarioItemData != null && scenarioItemData[1] == "時間")
				{
					int effect = int.Parse(scenarioItemData[2]);
					num = Data.AddEgoPerSecond(num, effect);
				}
			}
			foreach (string[] array in Data.PRIZE_DATA)
			{
				if (array[3] == "本の獲得EGO 2倍")
				{
					int num2 = PrizeLevel.Get(array[1]);
					int effect2 = 2;
					for (int i = 0; i < num2; i++)
					{
						num = Data.AddEgoPerSecond(num, effect2);
					}
				}
			}
			if (PurchasingItem.GetByLabel("本の獲得EGO10倍"))
			{
				int effect3 = 10;
				num = Data.AddEgoPerSecond(num, effect3);
			}
			if (PlayerStatus.ReadingBookList.Length > 0)
			{
				double num3 = (double)2;
				int num4 = Math.Min(PlayerStatus.ReadingBookList.Length, 30);
				int effect4 = (int)Math.Pow(num3, (double)num4);
				num = Data.AddEgoPerSecond(num, effect4);
			}
			Data._EgoBookPower.Add((int)num);
			Data.UpdateEgoPerSecond();
			Data.UpdatePriceList();
		}

		// Token: 0x06000784 RID: 1924 RVA: 0x0002635C File Offset: 0x0002455C
		private static long AddEgoPerSecond(long sum, int effect)
		{
			if (sum * (long)effect > 2147483647L)
			{
				Data._EgoBookPower.Add((int)sum);
				return (long)effect;
			}
			return sum * (long)effect;
		}

		// Token: 0x06000785 RID: 1925 RVA: 0x00026380 File Offset: 0x00024580
		public static EgoPoint GetEgoBookPower(EgoPoint baseEgo, bool multiple)
		{
			foreach (int num in Data.EgoBookPower)
			{
				if (multiple)
				{
					baseEgo *= (float)num;
				}
				else
				{
					baseEgo /= (long)num;
				}
			}
			if (multiple)
			{
				baseEgo *= (float)BonusTime.BonusValue;
			}
			else
			{
				baseEgo /= (long)BonusTime.BonusValue;
			}
			return baseEgo;
		}

		// Token: 0x06000786 RID: 1926 RVA: 0x00026408 File Offset: 0x00024608
		public static EgoPoint TapPower(bool close)
		{
			return Data.GetEgoTapPower() * (float)(close ? 5 : 3);
		}

		// Token: 0x0400026C RID: 620
		public static readonly string DATA_FILE_PATH = "Ego/scenario-SJIS.txt";

		// Token: 0x0400026D RID: 621
		public static DateTime LastUpdateTime;

		// Token: 0x0400026E RID: 622
		public static IDictionary<string, string[]> DIC;

		// Token: 0x0400026F RID: 623
		public static List<string[]> SCENARIO_DATA;

		// Token: 0x04000270 RID: 624
		public static List<string> SCENARIO_DATA_KEY;

		// Token: 0x04000271 RID: 625
		public static List<string> COUNSELING_TYPE_LIST;

		// Token: 0x04000272 RID: 626
		public static List<string> CHOICE_TYPE_LIST;

		// Token: 0x04000273 RID: 627
		public static List<string[]> PRIZE_DATA;

		// Token: 0x04000274 RID: 628
		public static List<string[]> SOUND_DATA;

		// Token: 0x04000275 RID: 629
		public static List<string[]> BOOKEGO_DATA;

		// Token: 0x04000276 RID: 630
		private static List<string> comment_list;

		// Token: 0x04000277 RID: 631
		private static EgoPoint _EgoPerSecond = null;

		// Token: 0x04000278 RID: 632
		private static List<string> _clear_scenario_list;

		// Token: 0x04000279 RID: 633
		private static EgoPoint[] _NEXT_PRICE_LIST;

		// Token: 0x0400027A RID: 634
		private static List<int> _EgoBookPower = null;

		// Token: 0x02000161 RID: 353
		public enum BOOK
		{
			// Token: 0x04000547 RID: 1351
			PRICE,
			// Token: 0x04000548 RID: 1352
			PERSECOND
		}

		// Token: 0x02000162 RID: 354
		private enum PRICE
		{
			// Token: 0x0400054A RID: 1354
			BOOK,
			// Token: 0x0400054B RID: 1355
			SCENARIO,
			// Token: 0x0400054C RID: 1356
			HAYAKAWA
		}

		// Token: 0x02000163 RID: 355
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000A75 RID: 2677 RVA: 0x0003153C File Offset: 0x0002F73C
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000A76 RID: 2678 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c()
			{
			}

			// Token: 0x06000A77 RID: 2679 RVA: 0x00031548 File Offset: 0x0002F748
			internal EgoPoint <BOOK_PARAMETER>b__15_0(string s)
			{
				return new EgoPoint(s);
			}

			// Token: 0x0400054D RID: 1357
			public static readonly Data.<>c <>9 = new Data.<>c();

			// Token: 0x0400054E RID: 1358
			public static Converter<string, EgoPoint> <>9__15_0;
		}
	}
}
