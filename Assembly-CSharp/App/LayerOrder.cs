﻿using System;

namespace App
{
	// Token: 0x020000AF RID: 175
	public static class LayerOrder
	{
		// Token: 0x0600078C RID: 1932 RVA: 0x00026584 File Offset: 0x00024784
		// Note: this type is marked as 'beforefieldinit'.
		static LayerOrder()
		{
		}

		// Token: 0x0400027D RID: 637
		public static int WALL = 10;

		// Token: 0x0400027E RID: 638
		public static int COMMENT_MIN = 40;

		// Token: 0x0400027F RID: 639
		public static int EGO_TAP_AREA = 91;

		// Token: 0x04000280 RID: 640
		public static int COMMENT_MAX = 104;

		// Token: 0x04000281 RID: 641
		public static int EGO_EFFECT = 120;

		// Token: 0x04000282 RID: 642
		public static int OBJECT_FILTER = 1000;

		// Token: 0x04000283 RID: 643
		public static int BG_CANVAS = -1;

		// Token: 0x04000284 RID: 644
		public static int DEFAULT_CANVAS = 0;

		// Token: 0x04000285 RID: 645
		public static int UI_HEADER = 2;

		// Token: 0x04000286 RID: 646
		public static int FULL_SCREEN = 10;

		// Token: 0x04000287 RID: 647
		public static int DIALOG_FILTER = 20;

		// Token: 0x04000288 RID: 648
		public static int DIALOG = 21;

		// Token: 0x04000289 RID: 649
		public static int DEBUG_BUTTON = 1;

		// Token: 0x0400028A RID: 650
		public static int HEADER = 2;

		// Token: 0x0400028B RID: 651
		public static int AD_CANVAS = 500;

		// Token: 0x0400028C RID: 652
		public static int OPENING_CANVAS = 1000;

		// Token: 0x0400028D RID: 653
		public static int DEBUG_MENU = 1000;

		// Token: 0x0400028E RID: 654
		public static int SCENE_TRANSITION = 200;
	}
}
