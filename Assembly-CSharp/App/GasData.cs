﻿using System;

namespace App
{
	// Token: 0x020000B0 RID: 176
	public static class GasData
	{
		// Token: 0x0400028F RID: 655
		public const string GAS_URL = "https://script.google.com/macros/s/AKfycbx0OgWjabmLpq7UnE3HxZIRjAxlZs1-GMbJX5c9VWFZV0JDsmM/exec";

		// Token: 0x04000290 RID: 656
		public const string SSID_ANALYTICS = "1DLWkIkaBFBnrV9VoNi09qwvOhocUADC8BWVHfNzyjfA";

		// Token: 0x04000291 RID: 657
		public const string SSID_SUPPORT = "1Jyb1Gc-XDgXGFebWkWco-tO56PVYTeS9GJ24x8ONO8Q";

		// Token: 0x04000292 RID: 658
		public const string SSID_AUTOTEST = "1olHdM1G5zhODy6IbT6ii-Xj2JdEH4oGYyigzAor2zJY";

		// Token: 0x04000293 RID: 659
		public const string SSID_SAVEDATA = "1csBkwtdEPOM-dk0kNER5lvOsPDmWs2Gft9y_CGyr898";

		// Token: 0x04000294 RID: 660
		public const string AUTOTEST_SHEET = "UI,SS,DemoS,DemoJ,RR,AE,Demo";

		// Token: 0x04000295 RID: 661
		public const string SSID_TEXTDATA = "1x4btaRfP3ubB7XPYYE3TiqEV_OihpZjGo81DhM4okWM";

		// Token: 0x04000296 RID: 662
		public const string TEXTDATA_SHEET = "多動モード,その他";

		// Token: 0x04000297 RID: 663
		public const string TEXTDATA_FILENAME = "parameters";
	}
}
