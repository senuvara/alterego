﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace App
{
	// Token: 0x020000A0 RID: 160
	[AppUtil.TitleAttribute("デバッグ機能")]
	public static class DebugFunctionsA
	{
		// Token: 0x060006DE RID: 1758 RVA: 0x0002418E File Offset: 0x0002238E
		private static IEnumerator UpdateParams()
		{
			yield return GssDataHelper.Overwrite();
			yield break;
		}

		// Token: 0x060006DF RID: 1759 RVA: 0x00024196 File Offset: 0x00022396
		// Note: this type is marked as 'beforefieldinit'.
		static DebugFunctionsA()
		{
		}

		// Token: 0x04000251 RID: 593
		[AppUtil.TitleAttribute("パラメータ更新")]
		public static AppUtil.Coroutine UpdateParameter = new AppUtil.Coroutine(DebugFunctionsA.UpdateParams);

		// Token: 0x02000160 RID: 352
		[CompilerGenerated]
		private sealed class <UpdateParams>d__1 : IEnumerator<object>, IEnumerator, IDisposable
		{
			// Token: 0x06000A6F RID: 2671 RVA: 0x000314E2 File Offset: 0x0002F6E2
			[DebuggerHidden]
			public <UpdateParams>d__1(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x06000A70 RID: 2672 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06000A71 RID: 2673 RVA: 0x000314F4 File Offset: 0x0002F6F4
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				if (num == 0)
				{
					this.<>1__state = -1;
					this.<>2__current = GssDataHelper.Overwrite();
					this.<>1__state = 1;
					return true;
				}
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				return false;
			}

			// Token: 0x1700016D RID: 365
			// (get) Token: 0x06000A72 RID: 2674 RVA: 0x00031534 File Offset: 0x0002F734
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000A73 RID: 2675 RVA: 0x00026A5A File Offset: 0x00024C5A
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700016E RID: 366
			// (get) Token: 0x06000A74 RID: 2676 RVA: 0x00031534 File Offset: 0x0002F734
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x04000544 RID: 1348
			private int <>1__state;

			// Token: 0x04000545 RID: 1349
			private object <>2__current;
		}
	}
}
