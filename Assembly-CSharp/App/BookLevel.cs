﻿using System;
using UnityEngine;

namespace App
{
	// Token: 0x020000A9 RID: 169
	[AppUtil.DataRangeAttribute]
	[AppUtil.TitleAttribute("読破ページ数")]
	public static class BookLevel
	{
		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x06000730 RID: 1840 RVA: 0x00021B8F File Offset: 0x0001FD8F
		public static int Min
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170000B4 RID: 180
		// (get) Token: 0x06000731 RID: 1841 RVA: 0x00024AFA File Offset: 0x00022CFA
		public static int Max
		{
			get
			{
				return 15;
			}
		}

		// Token: 0x06000732 RID: 1842 RVA: 0x00024AFE File Offset: 0x00022CFE
		public static string GetTitle(int bookNo)
		{
			return bookNo.ToString();
		}

		// Token: 0x06000733 RID: 1843 RVA: 0x00024B07 File Offset: 0x00022D07
		public static string GetLabel(int bookNo)
		{
			return bookNo + "." + LanguageManager.Get("[Book]Title" + bookNo);
		}

		// Token: 0x06000734 RID: 1844 RVA: 0x00024B2E File Offset: 0x00022D2E
		public static int Get(string bookNo)
		{
			return PlayerPrefs.GetInt(BookLevel.label + bookNo, -1);
		}

		// Token: 0x06000735 RID: 1845 RVA: 0x00024B44 File Offset: 0x00022D44
		public static void Set(string bookNo, int value)
		{
			if (value > BookLevel.GetPage(bookNo, 5))
			{
				value = BookLevel.GetPage(bookNo, 5);
			}
			int rank = BookLevel.GetRank(bookNo);
			PlayerPrefs.SetInt(BookLevel.label + bookNo, value);
			int rank2 = BookLevel.GetRank(bookNo);
			if (rank != rank2)
			{
				Data.UpdateCommentList();
				Data.UpdatePriceList();
			}
			Data.EgoPerSecond += BookEgoPoint.GetBookEgo(bookNo, "add_per_second", value - 1);
		}

		// Token: 0x06000736 RID: 1846 RVA: 0x00024BAC File Offset: 0x00022DAC
		public static void Add(string bookNo)
		{
			PlayerResult.BookPageSum++;
			BookLevel.Set(bookNo, BookLevel.Get(bookNo) + 1);
		}

		// Token: 0x06000737 RID: 1847 RVA: 0x00024BC8 File Offset: 0x00022DC8
		public static bool IsMax(string bookNo)
		{
			return BookLevel.Get(bookNo) >= BookLevel.GetPage(bookNo, 5);
		}

		// Token: 0x06000738 RID: 1848 RVA: 0x00024BDC File Offset: 0x00022DDC
		public static void Init()
		{
			for (int i = BookLevel.Min; i <= BookLevel.Max; i++)
			{
				PlayerPrefs.DeleteKey(BookLevel.label + BookLevel.GetTitle(i));
			}
		}

		// Token: 0x06000739 RID: 1849 RVA: 0x00024C12 File Offset: 0x00022E12
		public static int GetPage(string bookNo, int rank = 5)
		{
			return BookLevel.GetPage(int.Parse(bookNo), rank);
		}

		// Token: 0x0600073A RID: 1850 RVA: 0x00024C20 File Offset: 0x00022E20
		public static int GetPage(int bookNo, int rank = 5)
		{
			if (rank <= 1)
			{
				return 0;
			}
			return int.Parse(LanguageManager.Get(string.Concat(new object[]
			{
				"[Book]Page",
				bookNo,
				"Rank",
				rank
			})));
		}

		// Token: 0x0600073B RID: 1851 RVA: 0x00024C5F File Offset: 0x00022E5F
		public static int GetRank(string bookNo)
		{
			return BookLevel.GetRank(bookNo, BookLevel.Get(bookNo));
		}

		// Token: 0x0600073C RID: 1852 RVA: 0x00024C70 File Offset: 0x00022E70
		public static int GetRank(string bookNo, int page)
		{
			for (int i = 5; i > 0; i--)
			{
				if (page >= BookLevel.GetPage(bookNo, i))
				{
					return i;
				}
			}
			return 0;
		}

		// Token: 0x0600073D RID: 1853 RVA: 0x00024C96 File Offset: 0x00022E96
		public static int GetCurrentInRank(string bookNo)
		{
			return BookLevel.GetCurrentInRank(bookNo, BookLevel.Get(bookNo));
		}

		// Token: 0x0600073E RID: 1854 RVA: 0x00024CA4 File Offset: 0x00022EA4
		public static int GetCurrentInRank(string bookNo, int page)
		{
			return page - BookLevel.GetPage(bookNo, BookLevel.GetRank(bookNo, page));
		}

		// Token: 0x0600073F RID: 1855 RVA: 0x00024CB5 File Offset: 0x00022EB5
		public static int GetMaxPageInRank(string bookNo)
		{
			return BookLevel.GetMaxPageInRank(bookNo, BookLevel.Get(bookNo));
		}

		// Token: 0x06000740 RID: 1856 RVA: 0x00024CC4 File Offset: 0x00022EC4
		public static int GetMaxPageInRank(string bookNo, int page)
		{
			int rank = BookLevel.GetRank(bookNo, page);
			return BookLevel.GetPage(bookNo, rank + 1) - BookLevel.GetPage(bookNo, rank);
		}

		// Token: 0x06000741 RID: 1857 RVA: 0x00024CEA File Offset: 0x00022EEA
		// Note: this type is marked as 'beforefieldinit'.
		static BookLevel()
		{
		}

		// Token: 0x04000269 RID: 617
		public const int RankMax = 5;

		// Token: 0x0400026A RID: 618
		private static readonly string label = "BookLevel";
	}
}
