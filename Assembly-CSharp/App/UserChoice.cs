﻿using System;
using UnityEngine;

namespace App
{
	// Token: 0x020000A8 RID: 168
	[AppUtil.DataRangeAttribute]
	[AppUtil.TitleAttribute("選択肢")]
	public static class UserChoice
	{
		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x06000729 RID: 1833 RVA: 0x00021B8F File Offset: 0x0001FD8F
		public static int Min
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x0600072A RID: 1834 RVA: 0x00024A72 File Offset: 0x00022C72
		public static int Max
		{
			get
			{
				return Data.CHOICE_TYPE_LIST.Count;
			}
		}

		// Token: 0x0600072B RID: 1835 RVA: 0x00024A7E File Offset: 0x00022C7E
		public static string GetTitle(int no)
		{
			return Data.CHOICE_TYPE_LIST[no - 1];
		}

		// Token: 0x0600072C RID: 1836 RVA: 0x00024A8D File Offset: 0x00022C8D
		public static string Get(string title)
		{
			return PlayerPrefs.GetString(UserChoice.label + title);
		}

		// Token: 0x0600072D RID: 1837 RVA: 0x00024A9F File Offset: 0x00022C9F
		public static void Set(string title, string value)
		{
			PlayerPrefs.SetString(UserChoice.label + title, value);
			Data.UpdateCommentList();
		}

		// Token: 0x0600072E RID: 1838 RVA: 0x00024AB8 File Offset: 0x00022CB8
		public static void Init()
		{
			for (int i = UserChoice.Min; i <= UserChoice.Max; i++)
			{
				PlayerPrefs.DeleteKey(UserChoice.label + UserChoice.GetTitle(i));
			}
		}

		// Token: 0x0600072F RID: 1839 RVA: 0x00024AEE File Offset: 0x00022CEE
		// Note: this type is marked as 'beforefieldinit'.
		static UserChoice()
		{
		}

		// Token: 0x04000268 RID: 616
		private static string label = "UserChoice";
	}
}
