﻿using System;
using System.Collections.Generic;

namespace App
{
	// Token: 0x0200009E RID: 158
	public static class AdInfo
	{
		// Token: 0x1700008B RID: 139
		// (get) Token: 0x060006D3 RID: 1747 RVA: 0x00023FF7 File Offset: 0x000221F7
		public static bool ENABLE
		{
			get
			{
				return !PurchasingItem.GetByLabel("広告削除");
			}
		}

		// Token: 0x060006D4 RID: 1748 RVA: 0x00024008 File Offset: 0x00022208
		// Note: this type is marked as 'beforefieldinit'.
		static AdInfo()
		{
		}

		// Token: 0x0400024D RID: 589
		private static readonly string ADMOB_APP_ID = "ca-app-pub-4024401978273999~8378034079";

		// Token: 0x0400024E RID: 590
		public static readonly IDictionary<string, string[]> AD_ID = new Dictionary<string, string[]>
		{
			{
				"AdMobBanner",
				new string[]
				{
					AdInfo.ADMOB_APP_ID,
					"ca-app-pub-4024401978273999/4437406177"
				}
			},
			{
				"AdMobRectangle",
				new string[]
				{
					AdInfo.ADMOB_APP_ID,
					"ca-app-pub-4024401978273999/2702201417",
					"900,750",
					"0,-90"
				}
			},
			{
				"AdfuriNative",
				new string[]
				{
					"5ae8316a6a77f0634c00001a"
				}
			},
			{
				"AdfuriReward",
				new string[]
				{
					"5ae832b321eaef8c13000025"
				}
			}
		};
	}
}
