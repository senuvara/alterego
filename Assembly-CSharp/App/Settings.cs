﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace App
{
	// Token: 0x020000A1 RID: 161
	[AppUtil.TitleAttribute("設定")]
	public static class Settings
	{
		// Token: 0x17000090 RID: 144
		// (get) Token: 0x060006E0 RID: 1760 RVA: 0x000241A9 File Offset: 0x000223A9
		// (set) Token: 0x060006E1 RID: 1761 RVA: 0x000241B0 File Offset: 0x000223B0
		[AppUtil.TitleAttribute("言語")]
		[AppUtil.ArrayValueAttribute("LANG_LIST")]
		public static string LANGUAGE
		{
			[CompilerGenerated]
			get
			{
				return Settings.<LANGUAGE>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				Settings.<LANGUAGE>k__BackingField = value;
			}
		}

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x060006E2 RID: 1762 RVA: 0x000241B8 File Offset: 0x000223B8
		// (set) Token: 0x060006E3 RID: 1763 RVA: 0x000241BF File Offset: 0x000223BF
		[AppUtil.TitleAttribute("ゲームスピード")]
		public static float GAME_SPEED
		{
			[CompilerGenerated]
			get
			{
				return Settings.<GAME_SPEED>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				Settings.<GAME_SPEED>k__BackingField = value;
			}
		}

		// Token: 0x060006E4 RID: 1764 RVA: 0x000241C7 File Offset: 0x000223C7
		public static void Init()
		{
			Settings.GAME_SPEED = 1f;
			Settings.TRANSITION_COLOR = Color.black;
		}

		// Token: 0x060006E5 RID: 1765 RVA: 0x000241E0 File Offset: 0x000223E0
		static Settings()
		{
			Settings.Init();
		}

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x060006E6 RID: 1766 RVA: 0x00024236 File Offset: 0x00022436
		// (set) Token: 0x060006E7 RID: 1767 RVA: 0x00024246 File Offset: 0x00022446
		[AppUtil.TitleAttribute("BGM ON/OFF")]
		public static bool BGM
		{
			get
			{
				return PlayerPrefs.GetInt("BGM", 1) == 1;
			}
			set
			{
				PlayerPrefs.SetInt("BGM", value ? 1 : 0);
				AudioManager.ChangeSettings();
			}
		}

		// Token: 0x17000093 RID: 147
		// (get) Token: 0x060006E8 RID: 1768 RVA: 0x0002425E File Offset: 0x0002245E
		// (set) Token: 0x060006E9 RID: 1769 RVA: 0x0002426E File Offset: 0x0002246E
		[AppUtil.TitleAttribute("SE ON/OFF")]
		public static bool SE
		{
			get
			{
				return PlayerPrefs.GetInt("SE", 1) == 1;
			}
			set
			{
				PlayerPrefs.SetInt("SE", value ? 1 : 0);
				AudioManager.ChangeSettings();
			}
		}

		// Token: 0x17000094 RID: 148
		// (get) Token: 0x060006EA RID: 1770 RVA: 0x00024286 File Offset: 0x00022486
		// (set) Token: 0x060006EB RID: 1771 RVA: 0x00024292 File Offset: 0x00022492
		[AppUtil.TitleAttribute("引き継ぎID")]
		public static string SaveDataID
		{
			get
			{
				return PlayerPrefs.GetString("SaveDataID");
			}
			set
			{
				PlayerPrefs.SetString("SaveDataID", value);
			}
		}

		// Token: 0x04000252 RID: 594
		[CompilerGenerated]
		private static string <LANGUAGE>k__BackingField;

		// Token: 0x04000253 RID: 595
		public static readonly string[] LANG_LIST = BuildInfo.LANG_TYPE;

		// Token: 0x04000254 RID: 596
		[CompilerGenerated]
		private static float <GAME_SPEED>k__BackingField;

		// Token: 0x04000255 RID: 597
		public const float BUTTERFLY_WAIT_BOOK = 15f;

		// Token: 0x04000256 RID: 598
		public const float BUTTERFLY_WAIT_ES = 15f;

		// Token: 0x04000257 RID: 599
		public const float WALK_DISTANCE = -1.5f;

		// Token: 0x04000258 RID: 600
		public const float WALK_SPEED = 0.42f;

		// Token: 0x04000259 RID: 601
		public const float BONUS_SECONDS = 600f;

		// Token: 0x0400025A RID: 602
		public const int BOOK_HAYAKAWA_MAX = 30;

		// Token: 0x0400025B RID: 603
		public const int BOOK_HAYAKAWA_BONUS = 2;

		// Token: 0x0400025C RID: 604
		public static Color TRANSITION_COLOR;

		// Token: 0x0400025D RID: 605
		public static IDictionary<string, string> AUDIO_LIST = new Dictionary<string, string>
		{
			{
				"Default",
				"Alterego"
			},
			{
				"エスの部屋",
				"Silence_OffV"
			},
			{
				"Ending",
				""
			}
		};
	}
}
