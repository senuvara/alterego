﻿using System;

namespace App
{
	// Token: 0x0200009D RID: 157
	public static class SaveDataInfo
	{
		// Token: 0x060006D2 RID: 1746 RVA: 0x00023F84 File Offset: 0x00022184
		// Note: this type is marked as 'beforefieldinit'.
		static SaveDataInfo()
		{
		}

		// Token: 0x0400024B RID: 587
		public const string VERSION = "EGO_001";

		// Token: 0x0400024C RID: 588
		public static readonly Type[] TypeList = new Type[]
		{
			typeof(PlayerStatus),
			typeof(BookLevel),
			typeof(PrizeLevel),
			typeof(PlayerResult),
			typeof(CounselingResult),
			typeof(UserChoice),
			typeof(ReadFukidasiList)
		};
	}
}
