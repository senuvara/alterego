﻿using System;
using UnityEngine;

namespace SocialConnector
{
	// Token: 0x02000099 RID: 153
	public class SocialConnector
	{
		// Token: 0x060006C4 RID: 1732 RVA: 0x00023770 File Offset: 0x00021970
		private static void _Share(string text, string url, string textureUrl)
		{
			using (AndroidJavaObject androidJavaObject = new AndroidJavaObject("android.content.Intent", Array.Empty<object>()))
			{
				androidJavaObject.Call<AndroidJavaObject>("setAction", new object[]
				{
					"android.intent.action.SEND"
				});
				androidJavaObject.Call<AndroidJavaObject>("setType", new object[]
				{
					string.IsNullOrEmpty(textureUrl) ? "text/plain" : "image/png"
				});
				if (!string.IsNullOrEmpty(url))
				{
					text = text + "\t" + url;
				}
				if (!string.IsNullOrEmpty(text))
				{
					androidJavaObject.Call<AndroidJavaObject>("putExtra", new object[]
					{
						"android.intent.extra.TEXT",
						text
					});
				}
				if (!string.IsNullOrEmpty(textureUrl))
				{
					int @static = new AndroidJavaClass("android.os.Build$VERSION").GetStatic<int>("SDK_INT");
					AndroidJavaObject androidJavaObject4;
					if (24 <= @static)
					{
						AndroidJavaObject androidJavaObject2 = SocialConnector.activity.Call<AndroidJavaObject>("getApplicationContext", Array.Empty<object>());
						AndroidJavaClass androidJavaClass = new AndroidJavaClass("android.support.v4.content.FileProvider");
						AndroidJavaObject androidJavaObject3 = new AndroidJavaObject("java.io.File", new object[]
						{
							textureUrl
						});
						androidJavaObject4 = androidJavaClass.CallStatic<AndroidJavaObject>("getUriForFile", new object[]
						{
							androidJavaObject2,
							Application.identifier + ".fileprovider",
							androidJavaObject3
						});
					}
					else
					{
						AndroidJavaClass androidJavaClass2 = new AndroidJavaClass("android.net.Uri");
						AndroidJavaObject androidJavaObject5 = new AndroidJavaObject("java.io.File", new object[]
						{
							textureUrl
						});
						androidJavaObject4 = androidJavaClass2.CallStatic<AndroidJavaObject>("fromFile", new object[]
						{
							androidJavaObject5
						});
					}
					androidJavaObject.Call<AndroidJavaObject>("putExtra", new object[]
					{
						"android.intent.extra.STREAM",
						androidJavaObject4
					});
				}
				AndroidJavaObject androidJavaObject6 = androidJavaObject.CallStatic<AndroidJavaObject>("createChooser", new object[]
				{
					androidJavaObject,
					""
				});
				androidJavaObject6.Call<AndroidJavaObject>("putExtra", new object[]
				{
					"android.intent.extra.EXTRA_INITIAL_INTENTS",
					androidJavaObject
				});
				SocialConnector.activity.Call("startActivity", new object[]
				{
					androidJavaObject6
				});
			}
		}

		// Token: 0x060006C5 RID: 1733 RVA: 0x00023970 File Offset: 0x00021B70
		public static void Share(string text)
		{
			SocialConnector.Share(text, null, null);
		}

		// Token: 0x060006C6 RID: 1734 RVA: 0x0002397A File Offset: 0x00021B7A
		public static void Share(string text, string url)
		{
			SocialConnector.Share(text, url, null);
		}

		// Token: 0x060006C7 RID: 1735 RVA: 0x00023984 File Offset: 0x00021B84
		public static void Share(string text, string url, string textureUrl)
		{
			SocialConnector._Share(text, url, textureUrl);
		}

		// Token: 0x060006C8 RID: 1736 RVA: 0x000043CE File Offset: 0x000025CE
		public SocialConnector()
		{
		}

		// Token: 0x060006C9 RID: 1737 RVA: 0x0002398E File Offset: 0x00021B8E
		// Note: this type is marked as 'beforefieldinit'.
		static SocialConnector()
		{
		}

		// Token: 0x04000234 RID: 564
		private static AndroidJavaObject clazz = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

		// Token: 0x04000235 RID: 565
		private static AndroidJavaObject activity = SocialConnector.clazz.GetStatic<AndroidJavaObject>("currentActivity");
	}
}
