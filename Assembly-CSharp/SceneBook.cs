﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using App;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x0200001C RID: 28
public class SceneBook : SceneBase
{
	// Token: 0x060000BD RID: 189 RVA: 0x00009844 File Offset: 0x00007A44
	protected override void Start()
	{
		base.Start();
		global::Debug.Log("SceneBook:Start()");
		this.WallGroup = base.transform.Find("WallGroup/Wall").gameObject;
		if (PlayerStatus.ScenarioNo.Contains("4章SE"))
		{
			base.transform.Find("WallGroup/Wall/LineGroup").gameObject.SetActive(false);
			base.transform.Find("WallGroup/Wall/Ego").gameObject.SetActive(true);
			base.transform.Find("WallGroup/Shadow1").gameObject.SetActive(false);
		}
		this.CommentGroup = base.transform.Find("WallGroup/Wall/CommentGroup").gameObject;
		base.transform.Find("UICanvas/BookListView/Viewport").gameObject.SetActive(false);
		string text = Data.GetScenarioType(PlayerStatus.ScenarioNo);
		if (PlayerStatus.TutorialLv == PlayerStatus.TUTORIAL_MAX)
		{
			int scenarioIndex = Data.GetScenarioIndex(PlayerStatus.ScenarioNo);
			int scenarioIndex2 = Data.GetScenarioIndex("1章-1");
			if (scenarioIndex > scenarioIndex2)
			{
				text = "壁会話3";
			}
		}
		else if (PlayerStatus.ScenarioNo == "退行")
		{
			text = "壁会話9";
		}
		if (text == "オープニング")
		{
			this.OpeningGroup.SetActive(true);
			this.UICanvas.SetActive(false);
			base.StartCoroutine(this.Opening());
			AdManager.Hide("Banner");
			return;
		}
		if (text.Contains("壁会話"))
		{
			this.OpeningGroup.SetActive(false);
			this.UICanvas.SetActive(false);
			base.StartCoroutine(this.KingEgo(text));
			AdManager.Show("Banner", null);
			return;
		}
		this.OpeningGroup.SetActive(false);
		this.UICanvas.SetActive(true);
		float haParameters = this.GetHaParameters(this.HyperActivePhase, SceneBook.HA_INDEX.RATE);
		string mode = this.IsHAMode ? "HyperActive" : "None";
		base.GetComponentInChildren<CommentManager>().Generate(true, mode, haParameters);
		base.transform.Find("UICanvas/BookListView/Viewport").gameObject.SetActive(true);
		AppUtil.DelayAction(this, 1f, this.Walking(true), true);
		AdManager.Show("Banner", null);
	}

	// Token: 0x060000BE RID: 190 RVA: 0x00009A60 File Offset: 0x00007C60
	public void SetTutorial(int lv, bool updateMessage = false)
	{
		if (PlayerStatus.TutorialLv >= PlayerStatus.TUTORIAL_MAX)
		{
			return;
		}
		if (PlayerStatus.TutorialLv + 1 == lv)
		{
			PlayerStatus.TutorialLv = lv;
			updateMessage = true;
		}
		if (!updateMessage)
		{
			return;
		}
		string key = "壁会話2_" + PlayerStatus.TutorialLv;
		base.StartCoroutine(this.SetEgoMessage(key, true));
	}

	// Token: 0x060000BF RID: 191 RVA: 0x00009AB8 File Offset: 0x00007CB8
	protected override void FixedUpdate()
	{
		base.FixedUpdate();
		if (3 <= PlayerStatus.TutorialLv && PlayerStatus.TutorialLv <= 6)
		{
			if (PlayerStatus.EgoPoint >= Data.BOOK_PARAMETER(1)[0])
			{
				this.SetTutorial(4, false);
			}
			if (PlayerResult.BookPageSum > 0)
			{
				this.SetTutorial(5, false);
			}
			EgoPoint next_SCENARIO_PRICE = Data.NEXT_SCENARIO_PRICE;
			if (PlayerStatus.EgoPoint * 2f >= next_SCENARIO_PRICE)
			{
				this.SetTutorial(6, false);
			}
			if (PlayerStatus.EgoPoint >= next_SCENARIO_PRICE)
			{
				this.SetTutorial(7, false);
			}
		}
	}

	// Token: 0x060000C0 RID: 192 RVA: 0x00009B41 File Offset: 0x00007D41
	private IEnumerator Opening()
	{
		Text egomessage = this.OpeningGroup.transform.Find("OpeningCanvas/Message1").GetComponent<Text>();
		Text egomessage2 = this.OpeningGroup.transform.Find("OpeningCanvas/Message2").GetComponent<Text>();
		Text author = this.OpeningGroup.transform.Find("OpeningCanvas/Author").GetComponent<Text>();
		AppUtil.SetAlpha(egomessage, 0f);
		AppUtil.SetAlpha(egomessage2, 0f);
		AppUtil.SetAlpha(author, 0f);
		yield return AppUtil.Wait(1f);
		yield return AppUtil.FadeIn(egomessage, 0.5f, null);
		yield return AppUtil.Wait(1f);
		yield return AppUtil.FadeIn(egomessage2, 0.5f, null);
		yield return AppUtil.Wait(1f);
		yield return AppUtil.FadeIn(author, 0.5f, null);
		global::Debug.Log("AutoTestEvent:Screenshot");
		yield return AppUtil.Wait(2.5f);
		base.StartCoroutine(AppUtil.FadeOut(egomessage, 0.5f, null));
		base.StartCoroutine(AppUtil.FadeOut(egomessage2, 0.5f, null));
		yield return AppUtil.FadeOut(author, 0.5f, null);
		yield return AppUtil.Wait(0.5f);
		yield return AppUtil.FadeOut(this.OpeningGroup.transform.Find("OpeningCanvas/Background").GetComponent<Image>(), 1f, null);
		yield return AppUtil.Wait(0.5f);
		this.SetTutorial(0, false);
		Data.GoToNextScenario();
		base.StartCoroutine(this.KingEgo(Data.GetScenarioType(PlayerStatus.ScenarioNo)));
		AdManager.Show("Banner", null);
		yield break;
	}

	// Token: 0x060000C1 RID: 193 RVA: 0x00009B50 File Offset: 0x00007D50
	private IEnumerator KingEgo(string scenarioType)
	{
		this.OpeningGroup.transform.Find("OpeningCanvas").gameObject.SetActive(false);
		IEnumerator walking = this.Walking(false);
		yield return AppUtil.Wait(1f);
		base.StartCoroutine(walking);
		yield return AppUtil.Wait(4.5f);
		base.StopCoroutine(walking);
		yield return AppUtil.Wait(0.5f);
		base.transform.Find("KingEgo").gameObject.SetActive(true);
		Animator kingEgoAnim = base.transform.Find("KingEgo").GetComponent<Animator>();
		yield return AppUtil.WaitAnimation(kingEgoAnim, null);
		kingEgoAnim.SetFloat("Speed", 0f);
		int no = 1;
		string[] keyTypeList = new string[]
		{
			"",
			PlayerStatus.LCTypeTrend,
			PlayerStatus.RouteTrend
		};
		while (no <= 50)
		{
			string text = "";
			foreach (string text2 in keyTypeList)
			{
				string str = scenarioType + "_" + no;
				string text3 = str + text2;
				if (LanguageManager.Contains(text3 + "-L"))
				{
					text = text3;
					break;
				}
				text3 = str + "同" + text2;
				if (LanguageManager.Contains(text3 + "-L"))
				{
					text = text3;
					break;
				}
			}
			if (text == "")
			{
				yield return this.SetEgoMessage(text, false);
				break;
			}
			if (LanguageManager.Get(text + "-L") == "[[操作説明]]")
			{
				yield return AppUtil.Wait(0.25f);
				DialogManager.ShowDialog("TutorialDialog", Array.Empty<object>());
				while (DialogManager.IsShowing())
				{
					yield return null;
				}
				yield return AppUtil.Wait(0.25f);
			}
			else
			{
				yield return this.SetEgoMessage(text, false);
			}
			int i = no;
			no = i + 1;
		}
		base.transform.Find("MessageCanvas/MessageWindow/MessageL").gameObject.SetActive(false);
		base.transform.Find("MessageCanvas/MessageWindow/MessageR").gameObject.SetActive(false);
		base.transform.Find("MessageCanvas/TutorialWindow/MessageL").gameObject.SetActive(false);
		base.transform.Find("MessageCanvas/TutorialWindow/MessageR").gameObject.SetActive(false);
		yield return AppUtil.Wait(0.5f);
		kingEgoAnim.SetFloat("Speed", -1f);
		yield return AppUtil.WaitAnimation(kingEgoAnim, null);
		yield return base.StartCoroutine(AppUtil.FadeOut(this.OpeningGroup, 0.5f, null));
		base.transform.Find("KingEgo").gameObject.SetActive(false);
		AppUtil.SetAlpha(this.UICanvas, 0f);
		AppUtil.SetAlpha(base.transform.Find("UICanvas/UI_Header/Header2"), 0f);
		if (scenarioType == "壁会話3")
		{
			PlayerStatus.TutorialLv = PlayerStatus.TUTORIAL_MAX + 1;
		}
		else if (scenarioType == "壁会話9")
		{
			PlayerStatus.ScenarioNo = "1章-1";
		}
		else
		{
			Data.GoToNextScenario();
			this.SetTutorial(1, true);
		}
		yield return AppUtil.Wait(0.5f);
		this.UICanvas.SetActive(true);
		base.StartCoroutine(AppUtil.FadeIn(this.UICanvas, 0.5f, null));
		yield return AppUtil.Wait(0.25f);
		yield return base.StartCoroutine(AppUtil.FadeIn(base.transform.Find("UICanvas/UI_Header/Header2"), 0.5f, null));
		base.transform.Find("UICanvas/BookListView/Viewport").gameObject.SetActive(true);
		yield return AppUtil.Wait(1f);
		base.StartCoroutine(this.Walking(true));
		yield break;
	}

	// Token: 0x060000C2 RID: 194 RVA: 0x00009B66 File Offset: 0x00007D66
	private IEnumerator SetEgoMessage(string key, bool tutorial)
	{
		while (DialogManager.IsShowing())
		{
			yield return null;
		}
		string window = "MessageWindow";
		if (tutorial)
		{
			window = "TutorialWindow";
		}
		else
		{
			yield return AppUtil.Wait(0.25f);
		}
		GameObject messageL = base.transform.Find("MessageCanvas/" + window + "/MessageL").gameObject;
		GameObject messageR = base.transform.Find("MessageCanvas/" + window + "/MessageR").gameObject;
		if (base.transform.Find("MessageCanvas/" + window + "/MessageL").gameObject.activeSelf)
		{
			messageL.GetComponent<Animator>().PlayInFixedTime("MessageL", -1, 0.2f);
			messageR.GetComponent<Animator>().PlayInFixedTime("MessageR", -1, 0.2f);
			messageL.GetComponent<Animator>().SetFloat("Speed", -1f);
			messageR.GetComponent<Animator>().SetFloat("Speed", -1f);
			yield return AppUtil.FadeOut(base.transform.Find("MessageCanvas/" + window), 0.15f, null);
			messageL.SetActive(false);
			messageR.SetActive(false);
			AppUtil.SetAlpha(base.transform.Find("MessageCanvas/" + window), 1f);
		}
		if (key == "")
		{
			yield break;
		}
		yield return AppUtil.Wait(0.25f);
		messageL.GetComponentInChildren<TextLocalization>().SetKey(key + "-L");
		messageL.GetComponent<Animator>().SetFloat("Speed", 1f);
		messageL.SetActive(true);
		Image touch = GameObject.Find("MessageScreen").GetComponent<Image>();
		if (!tutorial && !key.Contains("同"))
		{
			yield return AppUtil.Wait(0.5f);
		}
		messageR.GetComponentInChildren<TextLocalization>().SetKey(key + "-R");
		messageR.GetComponent<Animator>().SetFloat("Speed", 1f);
		messageR.SetActive(true);
		global::Debug.Log("AutoTestEvent:Screenshot");
		if (!tutorial)
		{
			touch.raycastTarget = true;
			while (touch.raycastTarget)
			{
				yield return null;
			}
		}
		yield break;
	}

	// Token: 0x17000007 RID: 7
	// (get) Token: 0x060000C3 RID: 195 RVA: 0x00009B83 File Offset: 0x00007D83
	// (set) Token: 0x060000C4 RID: 196 RVA: 0x00009B8A File Offset: 0x00007D8A
	private int HyperActivePoint
	{
		get
		{
			return HyperActive.Point;
		}
		set
		{
			HyperActive.Point = value;
		}
	}

	// Token: 0x17000008 RID: 8
	// (get) Token: 0x060000C5 RID: 197 RVA: 0x00009B92 File Offset: 0x00007D92
	// (set) Token: 0x060000C6 RID: 198 RVA: 0x00009B99 File Offset: 0x00007D99
	private int HyperActivePhase
	{
		get
		{
			return HyperActive.Phase;
		}
		set
		{
			HyperActive.Phase = value;
		}
	}

	// Token: 0x17000009 RID: 9
	// (get) Token: 0x060000C7 RID: 199 RVA: 0x00009BA1 File Offset: 0x00007DA1
	// (set) Token: 0x060000C8 RID: 200 RVA: 0x00009BA8 File Offset: 0x00007DA8
	private float HyperActivePhaseSeconds
	{
		get
		{
			return HyperActive.Time;
		}
		set
		{
			HyperActive.Time = value;
		}
	}

	// Token: 0x060000C9 RID: 201 RVA: 0x00009BB0 File Offset: 0x00007DB0
	private float GetHaParameters(int phase, SceneBook.HA_INDEX index)
	{
		return float.Parse(GssDataHelper.GetSheet("多動モード")[phase][(int)index]);
	}

	// Token: 0x1700000A RID: 10
	// (get) Token: 0x060000CA RID: 202 RVA: 0x00009BC5 File Offset: 0x00007DC5
	private int HaMaxPhase
	{
		get
		{
			return GssDataHelper.GetSheet("多動モード").Length - 1;
		}
	}

	// Token: 0x060000CB RID: 203 RVA: 0x00009BD5 File Offset: 0x00007DD5
	private string GetHaMode()
	{
		return GssDataHelper.GetSheet("多動モード")[this.HyperActivePhase][5];
	}

	// Token: 0x1700000B RID: 11
	// (get) Token: 0x060000CC RID: 204 RVA: 0x00009BEA File Offset: 0x00007DEA
	private bool IsHAMode
	{
		get
		{
			return this.GetHaMode().Contains("多動");
		}
	}

	// Token: 0x1700000C RID: 12
	// (get) Token: 0x060000CD RID: 205 RVA: 0x00009BFC File Offset: 0x00007DFC
	private bool IsHALastMode
	{
		get
		{
			return this.GetHaMode() == "多動ラスト";
		}
	}

	// Token: 0x060000CE RID: 206 RVA: 0x00009C10 File Offset: 0x00007E10
	private void UpdateHYPhase(float passedTime)
	{
		this.HyperActivePhaseSeconds += passedTime;
		int num = (int)this.GetHaParameters(this.HyperActivePhase, SceneBook.HA_INDEX.POINT);
		int num2 = (int)this.GetHaParameters(this.HyperActivePhase, SceneBook.HA_INDEX.SECONDS);
		if (num > 0)
		{
			if (this.HyperActivePoint >= num)
			{
				this.MoveNextPhase(true);
			}
			if (num2 > 0 && this.HyperActivePhaseSeconds >= (float)num2)
			{
				this.MoveNextPhase(false);
				return;
			}
		}
		else if (this.HyperActivePhaseSeconds >= (float)num2)
		{
			this.MoveNextPhase(true);
		}
	}

	// Token: 0x060000CF RID: 207 RVA: 0x00009C84 File Offset: 0x00007E84
	private void MoveNextPhase(bool next = true)
	{
		this.HyperActivePhase += (next ? 1 : -1);
		if (this.HyperActivePhase > this.HaMaxPhase)
		{
			this.HyperActivePhase = 1;
		}
		if (this.HyperActivePhase <= 0)
		{
			this.HyperActivePhase = this.HaMaxPhase;
		}
	}

	// Token: 0x060000D0 RID: 208 RVA: 0x00009CC4 File Offset: 0x00007EC4
	private IEnumerator Walking(bool withComment)
	{
		int i = 0;
		CommentManager mgr = base.GetComponentInChildren<CommentManager>();
		float speed = 1f;
		EasingFunction.Ease easeIn = EasingFunction.Ease.EaseInSine;
		EasingFunction.Ease easeOut = EasingFunction.Ease.EaseOutSine;
		for (;;)
		{
			SceneBook.<>c__DisplayClass30_0 CS$<>8__locals1 = new SceneBook.<>c__DisplayClass30_0();
			float passedTime = HyperActive.PassedTime;
			this.UpdateHYPhase(passedTime);
			float fukidasiRate = this.GetHaParameters(this.HyperActivePhase, SceneBook.HA_INDEX.RATE);
			string commentMode = this.IsHALastMode ? "HyperActiveLast" : (this.IsHAMode ? "HyperActive" : "None");
			if (withComment && 0f < fukidasiRate && fukidasiRate < 1f)
			{
				break;
			}
			if (this.IsHAMode || !mgr.KeepHyperActive())
			{
				speed = this.GetHaParameters(this.HyperActivePhase, SceneBook.HA_INDEX.SPEED);
			}
			float moveTime = 0.42f / speed;
			float distance = -1.5f;
			Vector3 start = this.WallGroup.transform.position;
			Vector3 end = start + new Vector3(0f, 0f, distance);
			CS$<>8__locals1.cameraT = GameObject.Find("Main Camera").transform;
			Vector3 startCamera = CS$<>8__locals1.cameraT.position;
			Vector3 endCamera = startCamera + new Vector3(0f, (speed <= 1.2f) ? -0.04f : -0.02f, 0f);
			while (DialogManager.IsShowing())
			{
				yield return null;
			}
			base.StartCoroutine(AppUtil.MoveEasingVector3(start, end, delegate(Vector3 ret)
			{
				this.WallGroup.transform.position = ret;
			}, false, moveTime, easeIn, null));
			yield return AppUtil.MoveEasingVector3(startCamera, endCamera, delegate(Vector3 ret)
			{
				CS$<>8__locals1.cameraT.position = ret;
			}, false, moveTime, easeIn, null);
			start = end;
			end = start + new Vector3(0f, 0f, distance);
			while (DialogManager.IsShowing())
			{
				yield return null;
			}
			base.StartCoroutine(AppUtil.MoveEasingVector3(start, end, delegate(Vector3 ret)
			{
				this.WallGroup.transform.position = ret;
			}, false, moveTime, easeOut, null));
			yield return AppUtil.MoveEasingVector3(endCamera, startCamera, delegate(Vector3 ret)
			{
				CS$<>8__locals1.cameraT.position = ret;
			}, false, moveTime, easeOut, null);
			mgr.UpdateCommentWithDestroy();
			float num = 40f;
			if (base.transform.Find("WallGroup/Wall/Ego").gameObject.activeSelf)
			{
				num = 38.475f;
			}
			while (this.WallGroup.transform.localPosition.z <= -num)
			{
				this.WallGroup.transform.Translate(new Vector3(0f, 0f, num));
				Comment[] componentsInChildren = base.GetComponentsInChildren<Comment>();
				for (int j = 0; j < componentsInChildren.Length; j++)
				{
					componentsInChildren[j].transform.Translate(new Vector3(0f, 0f, -num));
				}
			}
			if (withComment && fukidasiRate >= 1f && (float)i >= fukidasiRate - 1f)
			{
				mgr.Generate(false, commentMode, fukidasiRate);
				i = 0;
			}
			else
			{
				int j = i;
				i = j + 1;
			}
			CS$<>8__locals1 = null;
			commentMode = null;
			start = default(Vector3);
			end = default(Vector3);
			startCamera = default(Vector3);
			endCamera = default(Vector3);
		}
		base.StartCoroutine(this.WalkingHAMode());
		yield break;
		yield break;
	}

	// Token: 0x060000D1 RID: 209 RVA: 0x00009CDA File Offset: 0x00007EDA
	private IEnumerator WalkingHAMode()
	{
		float haParameters = this.GetHaParameters(this.HyperActivePhase, SceneBook.HA_INDEX.SPEED);
		float moveTime = 0.42f / haParameters * 2f;
		float distance = -1.5f;
		float fukidasiRate = this.GetHaParameters(this.HyperActivePhase, SceneBook.HA_INDEX.RATE);
		CommentManager mgr = base.GetComponentInChildren<CommentManager>();
		float num = moveTime * fukidasiRate;
		base.InvokeRepeating("GenerateComment", num, num);
		base.InvokeRepeating("DestroyComment", num, num);
		Action<Vector3> <>9__1;
		for (;;)
		{
			float passedTime = HyperActive.PassedTime;
			this.UpdateHYPhase(passedTime);
			float haParameters2 = this.GetHaParameters(this.HyperActivePhase, SceneBook.HA_INDEX.RATE);
			if (haParameters2 == 0f || haParameters2 >= 1f)
			{
				break;
			}
			Vector3 position = this.WallGroup.transform.position;
			Vector3 vector = position + new Vector3(0f, 0f, distance * 2f);
			Vector3 startValue = position;
			Vector3 endValue = vector;
			Action<Vector3> callback;
			if ((callback = <>9__1) == null)
			{
				callback = (<>9__1 = delegate(Vector3 ret)
				{
					this.WallGroup.transform.position = ret;
				});
			}
			yield return base.StartCoroutine(AppUtil.MoveEasingVector3(startValue, endValue, callback, false, moveTime * 0.95f, EasingFunction.Ease.Linear, null));
			yield return AppUtil.Wait(moveTime * 0.05f);
			float num2 = 40f;
			if (base.transform.Find("WallGroup/Wall/Ego").gameObject.activeSelf)
			{
				num2 = 38.475f;
			}
			while (this.WallGroup.transform.localPosition.z <= -num2)
			{
				this.WallGroup.transform.Translate(new Vector3(0f, 0f, num2));
				Comment[] componentsInChildren = base.GetComponentsInChildren<Comment>();
				for (int i = 0; i < componentsInChildren.Length; i++)
				{
					componentsInChildren[i].transform.Translate(new Vector3(0f, 0f, -num2));
				}
			}
		}
		base.CancelInvoke("GenerateComment");
		AppUtil.DelayAction(this, moveTime, delegate()
		{
			mgr.Generate(false, "HyperActiveLast", fukidasiRate);
		}, true);
		Action<Vector3> <>9__2;
		while (base.GetComponentsInChildren<Comment>().Length != 0)
		{
			Vector3 position2 = this.WallGroup.transform.position;
			Vector3 vector2 = position2 + new Vector3(0f, 0f, distance * 2f);
			Vector3 startValue2 = position2;
			Vector3 endValue2 = vector2;
			Action<Vector3> callback2;
			if ((callback2 = <>9__2) == null)
			{
				callback2 = (<>9__2 = delegate(Vector3 ret)
				{
					this.WallGroup.transform.position = ret;
				});
			}
			yield return base.StartCoroutine(AppUtil.MoveEasingVector3(startValue2, endValue2, callback2, false, moveTime * 0.95f, EasingFunction.Ease.Linear, null));
			yield return AppUtil.Wait(moveTime * 0.05f);
			float num3 = 40f;
			if (base.transform.Find("WallGroup/Wall/Ego").gameObject.activeSelf)
			{
				num3 = 38.475f;
			}
			while (this.WallGroup.transform.localPosition.z <= -num3)
			{
				this.WallGroup.transform.Translate(new Vector3(0f, 0f, num3));
				Comment[] componentsInChildren = base.GetComponentsInChildren<Comment>();
				for (int i = 0; i < componentsInChildren.Length; i++)
				{
					componentsInChildren[i].transform.Translate(new Vector3(0f, 0f, -num3));
				}
			}
		}
		base.CancelInvoke("DestroyComment");
		base.StartCoroutine(this.Walking(true));
		yield break;
	}

	// Token: 0x060000D2 RID: 210 RVA: 0x00009CEC File Offset: 0x00007EEC
	private void GenerateComment()
	{
		CommentManager componentInChildren = base.GetComponentInChildren<CommentManager>();
		float haParameters = this.GetHaParameters(this.HyperActivePhase, SceneBook.HA_INDEX.RATE);
		string mode = this.IsHALastMode ? "HyperActiveLast" : (this.IsHAMode ? "HyperActive" : "None");
		componentInChildren.Generate(false, mode, haParameters);
	}

	// Token: 0x060000D3 RID: 211 RVA: 0x00009D39 File Offset: 0x00007F39
	private void DestroyComment()
	{
		base.GetComponentInChildren<CommentManager>().UpdateCommentWithDestroy();
	}

	// Token: 0x060000D4 RID: 212 RVA: 0x00009D48 File Offset: 0x00007F48
	public void TapEgo(GameObject tapFukidasi)
	{
		Comment component = tapFukidasi.GetComponent<Comment>();
		if (ReadFukidasiList.Add(component.CommentKey))
		{
			this.HyperActivePoint += 10;
		}
		else
		{
			this.HyperActivePoint++;
		}
		this.SetTutorial(3, false);
		bool flag = tapFukidasi.name.Contains("CloseComment");
		EgoPoint egoPoint = Data.TapPower(flag);
		if (component.Type != "")
		{
			egoPoint *= 3f;
		}
		string text = "FarEgoEffect";
		if (flag)
		{
			text = "CloseEgoEffect";
		}
		GameObject gameObject = Object.Instantiate<GameObject>(this.CommentGroup.transform.Find(text).gameObject, this.CommentGroup.transform);
		gameObject.name = text;
		gameObject.GetComponentInChildren<TextMeshPro>().text = egoPoint.ToString("+0");
		gameObject.transform.position = tapFukidasi.transform.position + new Vector3(0f, -1.5f, 0f);
		gameObject.transform.SetParent(null);
		gameObject.GetComponentInChildren<Animator>(true).enabled = true;
		PlayerStatus.EgoPoint += egoPoint;
		PlayerResult.TapMax = egoPoint;
		PlayerResult.TapCount++;
		base.UpdateHeader();
	}

	// Token: 0x060000D5 RID: 213 RVA: 0x00009E88 File Offset: 0x00008088
	public new void OnClick(GameObject clickObject)
	{
		if (base.OnClick(clickObject))
		{
			return;
		}
		clickObject.name = clickObject.name.Replace("(Clone)", "");
		global::Debug.Log("SceneBook:OnClick " + clickObject.name);
		string name = clickObject.name;
		if (name == "MessageScreen")
		{
			clickObject.GetComponent<Image>().raycastTarget = false;
			return;
		}
		if (clickObject.name.Contains("Comment"))
		{
			this.TapEgo(clickObject);
		}
	}

	// Token: 0x060000D6 RID: 214 RVA: 0x00009F09 File Offset: 0x00008109
	public SceneBook()
	{
	}

	// Token: 0x060000D7 RID: 215 RVA: 0x00009F11 File Offset: 0x00008111
	[CompilerGenerated]
	private void <Walking>b__30_0(Vector3 ret)
	{
		this.WallGroup.transform.position = ret;
	}

	// Token: 0x060000D8 RID: 216 RVA: 0x00009F11 File Offset: 0x00008111
	[CompilerGenerated]
	private void <Walking>b__30_2(Vector3 ret)
	{
		this.WallGroup.transform.position = ret;
	}

	// Token: 0x04000035 RID: 53
	public GameObject OpeningGroup;

	// Token: 0x04000036 RID: 54
	public GameObject UICanvas;

	// Token: 0x04000037 RID: 55
	private GameObject WallGroup;

	// Token: 0x04000038 RID: 56
	private GameObject CommentGroup;

	// Token: 0x020000D8 RID: 216
	private enum HA_INDEX
	{
		// Token: 0x04000311 RID: 785
		SPEED = 1,
		// Token: 0x04000312 RID: 786
		POINT,
		// Token: 0x04000313 RID: 787
		SECONDS,
		// Token: 0x04000314 RID: 788
		RATE,
		// Token: 0x04000315 RID: 789
		MODE
	}

	// Token: 0x020000D9 RID: 217
	[CompilerGenerated]
	private sealed class <Opening>d__7 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x0600082A RID: 2090 RVA: 0x0002863F File Offset: 0x0002683F
		[DebuggerHidden]
		public <Opening>d__7(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x0600082B RID: 2091 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x0600082C RID: 2092 RVA: 0x00028650 File Offset: 0x00026850
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			SceneBook sceneBook = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				egomessage = sceneBook.OpeningGroup.transform.Find("OpeningCanvas/Message1").GetComponent<Text>();
				egomessage2 = sceneBook.OpeningGroup.transform.Find("OpeningCanvas/Message2").GetComponent<Text>();
				author = sceneBook.OpeningGroup.transform.Find("OpeningCanvas/Author").GetComponent<Text>();
				AppUtil.SetAlpha(egomessage, 0f);
				AppUtil.SetAlpha(egomessage2, 0f);
				AppUtil.SetAlpha(author, 0f);
				this.<>2__current = AppUtil.Wait(1f);
				this.<>1__state = 1;
				return true;
			case 1:
				this.<>1__state = -1;
				this.<>2__current = AppUtil.FadeIn(egomessage, 0.5f, null);
				this.<>1__state = 2;
				return true;
			case 2:
				this.<>1__state = -1;
				this.<>2__current = AppUtil.Wait(1f);
				this.<>1__state = 3;
				return true;
			case 3:
				this.<>1__state = -1;
				this.<>2__current = AppUtil.FadeIn(egomessage2, 0.5f, null);
				this.<>1__state = 4;
				return true;
			case 4:
				this.<>1__state = -1;
				this.<>2__current = AppUtil.Wait(1f);
				this.<>1__state = 5;
				return true;
			case 5:
				this.<>1__state = -1;
				this.<>2__current = AppUtil.FadeIn(author, 0.5f, null);
				this.<>1__state = 6;
				return true;
			case 6:
				this.<>1__state = -1;
				global::Debug.Log("AutoTestEvent:Screenshot");
				this.<>2__current = AppUtil.Wait(2.5f);
				this.<>1__state = 7;
				return true;
			case 7:
				this.<>1__state = -1;
				sceneBook.StartCoroutine(AppUtil.FadeOut(egomessage, 0.5f, null));
				sceneBook.StartCoroutine(AppUtil.FadeOut(egomessage2, 0.5f, null));
				this.<>2__current = AppUtil.FadeOut(author, 0.5f, null);
				this.<>1__state = 8;
				return true;
			case 8:
				this.<>1__state = -1;
				this.<>2__current = AppUtil.Wait(0.5f);
				this.<>1__state = 9;
				return true;
			case 9:
				this.<>1__state = -1;
				this.<>2__current = AppUtil.FadeOut(sceneBook.OpeningGroup.transform.Find("OpeningCanvas/Background").GetComponent<Image>(), 1f, null);
				this.<>1__state = 10;
				return true;
			case 10:
				this.<>1__state = -1;
				this.<>2__current = AppUtil.Wait(0.5f);
				this.<>1__state = 11;
				return true;
			case 11:
				this.<>1__state = -1;
				sceneBook.SetTutorial(0, false);
				Data.GoToNextScenario();
				sceneBook.StartCoroutine(sceneBook.KingEgo(Data.GetScenarioType(PlayerStatus.ScenarioNo)));
				AdManager.Show("Banner", null);
				return false;
			default:
				return false;
			}
		}

		// Token: 0x170000EA RID: 234
		// (get) Token: 0x0600082D RID: 2093 RVA: 0x00028941 File Offset: 0x00026B41
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0600082E RID: 2094 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000EB RID: 235
		// (get) Token: 0x0600082F RID: 2095 RVA: 0x00028941 File Offset: 0x00026B41
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x04000316 RID: 790
		private int <>1__state;

		// Token: 0x04000317 RID: 791
		private object <>2__current;

		// Token: 0x04000318 RID: 792
		public SceneBook <>4__this;

		// Token: 0x04000319 RID: 793
		private Text <egomessage1>5__2;

		// Token: 0x0400031A RID: 794
		private Text <egomessage2>5__3;

		// Token: 0x0400031B RID: 795
		private Text <author>5__4;
	}

	// Token: 0x020000DA RID: 218
	[CompilerGenerated]
	private sealed class <KingEgo>d__8 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000830 RID: 2096 RVA: 0x00028949 File Offset: 0x00026B49
		[DebuggerHidden]
		public <KingEgo>d__8(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000831 RID: 2097 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000832 RID: 2098 RVA: 0x00028958 File Offset: 0x00026B58
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			SceneBook sceneBook = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				sceneBook.OpeningGroup.transform.Find("OpeningCanvas").gameObject.SetActive(false);
				walking = sceneBook.Walking(false);
				this.<>2__current = AppUtil.Wait(1f);
				this.<>1__state = 1;
				return true;
			case 1:
				this.<>1__state = -1;
				sceneBook.StartCoroutine(walking);
				this.<>2__current = AppUtil.Wait(4.5f);
				this.<>1__state = 2;
				return true;
			case 2:
				this.<>1__state = -1;
				sceneBook.StopCoroutine(walking);
				this.<>2__current = AppUtil.Wait(0.5f);
				this.<>1__state = 3;
				return true;
			case 3:
				this.<>1__state = -1;
				sceneBook.transform.Find("KingEgo").gameObject.SetActive(true);
				kingEgoAnim = sceneBook.transform.Find("KingEgo").GetComponent<Animator>();
				this.<>2__current = AppUtil.WaitAnimation(kingEgoAnim, null);
				this.<>1__state = 4;
				return true;
			case 4:
				this.<>1__state = -1;
				kingEgoAnim.SetFloat("Speed", 0f);
				no = 1;
				keyTypeList = new string[]
				{
					"",
					PlayerStatus.LCTypeTrend,
					PlayerStatus.RouteTrend
				};
				goto IL_31F;
			case 5:
				this.<>1__state = -1;
				goto IL_32C;
			case 6:
				this.<>1__state = -1;
				DialogManager.ShowDialog("TutorialDialog", Array.Empty<object>());
				break;
			case 7:
				this.<>1__state = -1;
				break;
			case 8:
				this.<>1__state = -1;
				goto IL_30D;
			case 9:
				this.<>1__state = -1;
				goto IL_30D;
			case 10:
				this.<>1__state = -1;
				kingEgoAnim.SetFloat("Speed", -1f);
				this.<>2__current = AppUtil.WaitAnimation(kingEgoAnim, null);
				this.<>1__state = 11;
				return true;
			case 11:
				this.<>1__state = -1;
				this.<>2__current = sceneBook.StartCoroutine(AppUtil.FadeOut(sceneBook.OpeningGroup, 0.5f, null));
				this.<>1__state = 12;
				return true;
			case 12:
				this.<>1__state = -1;
				sceneBook.transform.Find("KingEgo").gameObject.SetActive(false);
				AppUtil.SetAlpha(sceneBook.UICanvas, 0f);
				AppUtil.SetAlpha(sceneBook.transform.Find("UICanvas/UI_Header/Header2"), 0f);
				if (scenarioType == "壁会話3")
				{
					PlayerStatus.TutorialLv = PlayerStatus.TUTORIAL_MAX + 1;
				}
				else if (scenarioType == "壁会話9")
				{
					PlayerStatus.ScenarioNo = "1章-1";
				}
				else
				{
					Data.GoToNextScenario();
					sceneBook.SetTutorial(1, true);
				}
				this.<>2__current = AppUtil.Wait(0.5f);
				this.<>1__state = 13;
				return true;
			case 13:
				this.<>1__state = -1;
				sceneBook.UICanvas.SetActive(true);
				sceneBook.StartCoroutine(AppUtil.FadeIn(sceneBook.UICanvas, 0.5f, null));
				this.<>2__current = AppUtil.Wait(0.25f);
				this.<>1__state = 14;
				return true;
			case 14:
				this.<>1__state = -1;
				this.<>2__current = sceneBook.StartCoroutine(AppUtil.FadeIn(sceneBook.transform.Find("UICanvas/UI_Header/Header2"), 0.5f, null));
				this.<>1__state = 15;
				return true;
			case 15:
				this.<>1__state = -1;
				sceneBook.transform.Find("UICanvas/BookListView/Viewport").gameObject.SetActive(true);
				this.<>2__current = AppUtil.Wait(1f);
				this.<>1__state = 16;
				return true;
			case 16:
				this.<>1__state = -1;
				sceneBook.StartCoroutine(sceneBook.Walking(true));
				return false;
			default:
				return false;
			}
			if (!DialogManager.IsShowing())
			{
				this.<>2__current = AppUtil.Wait(0.25f);
				this.<>1__state = 8;
				return true;
			}
			this.<>2__current = null;
			this.<>1__state = 7;
			return true;
			IL_30D:
			int i = no;
			no = i + 1;
			IL_31F:
			if (no <= 50)
			{
				string text = "";
				foreach (string text2 in keyTypeList)
				{
					string str = scenarioType + "_" + no;
					string text3 = str + text2;
					if (LanguageManager.Contains(text3 + "-L"))
					{
						text = text3;
						break;
					}
					text3 = str + "同" + text2;
					if (LanguageManager.Contains(text3 + "-L"))
					{
						text = text3;
						break;
					}
				}
				if (text == "")
				{
					this.<>2__current = sceneBook.SetEgoMessage(text, false);
					this.<>1__state = 5;
					return true;
				}
				if (LanguageManager.Get(text + "-L") == "[[操作説明]]")
				{
					this.<>2__current = AppUtil.Wait(0.25f);
					this.<>1__state = 6;
					return true;
				}
				this.<>2__current = sceneBook.SetEgoMessage(text, false);
				this.<>1__state = 9;
				return true;
			}
			IL_32C:
			sceneBook.transform.Find("MessageCanvas/MessageWindow/MessageL").gameObject.SetActive(false);
			sceneBook.transform.Find("MessageCanvas/MessageWindow/MessageR").gameObject.SetActive(false);
			sceneBook.transform.Find("MessageCanvas/TutorialWindow/MessageL").gameObject.SetActive(false);
			sceneBook.transform.Find("MessageCanvas/TutorialWindow/MessageR").gameObject.SetActive(false);
			this.<>2__current = AppUtil.Wait(0.5f);
			this.<>1__state = 10;
			return true;
		}

		// Token: 0x170000EC RID: 236
		// (get) Token: 0x06000833 RID: 2099 RVA: 0x00028EFF File Offset: 0x000270FF
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000834 RID: 2100 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000ED RID: 237
		// (get) Token: 0x06000835 RID: 2101 RVA: 0x00028EFF File Offset: 0x000270FF
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0400031C RID: 796
		private int <>1__state;

		// Token: 0x0400031D RID: 797
		private object <>2__current;

		// Token: 0x0400031E RID: 798
		public SceneBook <>4__this;

		// Token: 0x0400031F RID: 799
		public string scenarioType;

		// Token: 0x04000320 RID: 800
		private IEnumerator <walking>5__2;

		// Token: 0x04000321 RID: 801
		private Animator <kingEgoAnim>5__3;

		// Token: 0x04000322 RID: 802
		private int <no>5__4;

		// Token: 0x04000323 RID: 803
		private string[] <keyTypeList>5__5;
	}

	// Token: 0x020000DB RID: 219
	[CompilerGenerated]
	private sealed class <SetEgoMessage>d__9 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000836 RID: 2102 RVA: 0x00028F07 File Offset: 0x00027107
		[DebuggerHidden]
		public <SetEgoMessage>d__9(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000837 RID: 2103 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000838 RID: 2104 RVA: 0x00028F18 File Offset: 0x00027118
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			SceneBook sceneBook = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				break;
			case 1:
				this.<>1__state = -1;
				break;
			case 2:
				this.<>1__state = -1;
				goto IL_99;
			case 3:
				this.<>1__state = -1;
				messageL.SetActive(false);
				messageR.SetActive(false);
				AppUtil.SetAlpha(sceneBook.transform.Find("MessageCanvas/" + window), 1f);
				goto IL_202;
			case 4:
				this.<>1__state = -1;
				messageL.GetComponentInChildren<TextLocalization>().SetKey(key + "-L");
				messageL.GetComponent<Animator>().SetFloat("Speed", 1f);
				messageL.SetActive(true);
				touch = GameObject.Find("MessageScreen").GetComponent<Image>();
				if (!tutorial && !key.Contains("同"))
				{
					this.<>2__current = AppUtil.Wait(0.5f);
					this.<>1__state = 5;
					return true;
				}
				goto IL_2CB;
			case 5:
				this.<>1__state = -1;
				goto IL_2CB;
			case 6:
				this.<>1__state = -1;
				goto IL_348;
			default:
				return false;
			}
			if (DialogManager.IsShowing())
			{
				this.<>2__current = null;
				this.<>1__state = 1;
				return true;
			}
			window = "MessageWindow";
			if (!tutorial)
			{
				this.<>2__current = AppUtil.Wait(0.25f);
				this.<>1__state = 2;
				return true;
			}
			window = "TutorialWindow";
			IL_99:
			messageL = sceneBook.transform.Find("MessageCanvas/" + window + "/MessageL").gameObject;
			messageR = sceneBook.transform.Find("MessageCanvas/" + window + "/MessageR").gameObject;
			if (sceneBook.transform.Find("MessageCanvas/" + window + "/MessageL").gameObject.activeSelf)
			{
				messageL.GetComponent<Animator>().PlayInFixedTime("MessageL", -1, 0.2f);
				messageR.GetComponent<Animator>().PlayInFixedTime("MessageR", -1, 0.2f);
				messageL.GetComponent<Animator>().SetFloat("Speed", -1f);
				messageR.GetComponent<Animator>().SetFloat("Speed", -1f);
				this.<>2__current = AppUtil.FadeOut(sceneBook.transform.Find("MessageCanvas/" + window), 0.15f, null);
				this.<>1__state = 3;
				return true;
			}
			IL_202:
			if (key == "")
			{
				return false;
			}
			this.<>2__current = AppUtil.Wait(0.25f);
			this.<>1__state = 4;
			return true;
			IL_2CB:
			messageR.GetComponentInChildren<TextLocalization>().SetKey(key + "-R");
			messageR.GetComponent<Animator>().SetFloat("Speed", 1f);
			messageR.SetActive(true);
			global::Debug.Log("AutoTestEvent:Screenshot");
			if (tutorial)
			{
				return false;
			}
			touch.raycastTarget = true;
			IL_348:
			if (touch.raycastTarget)
			{
				this.<>2__current = null;
				this.<>1__state = 6;
				return true;
			}
			return false;
		}

		// Token: 0x170000EE RID: 238
		// (get) Token: 0x06000839 RID: 2105 RVA: 0x0002927B File Offset: 0x0002747B
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0600083A RID: 2106 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000EF RID: 239
		// (get) Token: 0x0600083B RID: 2107 RVA: 0x0002927B File Offset: 0x0002747B
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x04000324 RID: 804
		private int <>1__state;

		// Token: 0x04000325 RID: 805
		private object <>2__current;

		// Token: 0x04000326 RID: 806
		public bool tutorial;

		// Token: 0x04000327 RID: 807
		public SceneBook <>4__this;

		// Token: 0x04000328 RID: 808
		public string key;

		// Token: 0x04000329 RID: 809
		private string <window>5__2;

		// Token: 0x0400032A RID: 810
		private GameObject <messageL>5__3;

		// Token: 0x0400032B RID: 811
		private GameObject <messageR>5__4;

		// Token: 0x0400032C RID: 812
		private Image <touch>5__5;
	}

	// Token: 0x020000DC RID: 220
	[CompilerGenerated]
	private sealed class <>c__DisplayClass30_0
	{
		// Token: 0x0600083C RID: 2108 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass30_0()
		{
		}

		// Token: 0x0600083D RID: 2109 RVA: 0x00029283 File Offset: 0x00027483
		internal void <Walking>b__1(Vector3 ret)
		{
			this.cameraT.position = ret;
		}

		// Token: 0x0600083E RID: 2110 RVA: 0x00029283 File Offset: 0x00027483
		internal void <Walking>b__3(Vector3 ret)
		{
			this.cameraT.position = ret;
		}

		// Token: 0x0400032D RID: 813
		public Transform cameraT;
	}

	// Token: 0x020000DD RID: 221
	[CompilerGenerated]
	private sealed class <Walking>d__30 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x0600083F RID: 2111 RVA: 0x00029291 File Offset: 0x00027491
		[DebuggerHidden]
		public <Walking>d__30(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000840 RID: 2112 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000841 RID: 2113 RVA: 0x000292A0 File Offset: 0x000274A0
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			SceneBook sceneBook = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				i = 0;
				mgr = sceneBook.GetComponentInChildren<CommentManager>();
				speed = 1f;
				easeIn = EasingFunction.Ease.EaseInSine;
				easeOut = EasingFunction.Ease.EaseOutSine;
				break;
			case 1:
				this.<>1__state = -1;
				goto IL_1E7;
			case 2:
				this.<>1__state = -1;
				start = end;
				end = start + new Vector3(0f, 0f, distance);
				goto IL_2B1;
			case 3:
				this.<>1__state = -1;
				goto IL_2B1;
			case 4:
			{
				this.<>1__state = -1;
				mgr.UpdateCommentWithDestroy();
				float num2 = 40f;
				if (sceneBook.transform.Find("WallGroup/Wall/Ego").gameObject.activeSelf)
				{
					num2 = 38.475f;
				}
				while (sceneBook.WallGroup.transform.localPosition.z <= -num2)
				{
					sceneBook.WallGroup.transform.Translate(new Vector3(0f, 0f, num2));
					Comment[] componentsInChildren = sceneBook.GetComponentsInChildren<Comment>();
					for (int j = 0; j < componentsInChildren.Length; j++)
					{
						componentsInChildren[j].transform.Translate(new Vector3(0f, 0f, -num2));
					}
				}
				if (withComment && fukidasiRate >= 1f && (float)i >= fukidasiRate - 1f)
				{
					mgr.Generate(false, commentMode, fukidasiRate);
					i = 0;
				}
				else
				{
					int j = i;
					i = j + 1;
				}
				CS$<>8__locals1 = null;
				commentMode = null;
				start = default(Vector3);
				end = default(Vector3);
				startCamera = default(Vector3);
				endCamera = default(Vector3);
				break;
			}
			default:
				return false;
			}
			CS$<>8__locals1 = new SceneBook.<>c__DisplayClass30_0();
			float passedTime = HyperActive.PassedTime;
			sceneBook.UpdateHYPhase(passedTime);
			fukidasiRate = sceneBook.GetHaParameters(sceneBook.HyperActivePhase, SceneBook.HA_INDEX.RATE);
			commentMode = (sceneBook.IsHALastMode ? "HyperActiveLast" : (sceneBook.IsHAMode ? "HyperActive" : "None"));
			if (withComment && 0f < fukidasiRate && fukidasiRate < 1f)
			{
				sceneBook.StartCoroutine(sceneBook.WalkingHAMode());
				return false;
			}
			if (sceneBook.IsHAMode || !mgr.KeepHyperActive())
			{
				speed = sceneBook.GetHaParameters(sceneBook.HyperActivePhase, SceneBook.HA_INDEX.SPEED);
			}
			moveTime = 0.42f / speed;
			distance = -1.5f;
			start = sceneBook.WallGroup.transform.position;
			end = start + new Vector3(0f, 0f, distance);
			CS$<>8__locals1.cameraT = GameObject.Find("Main Camera").transform;
			startCamera = CS$<>8__locals1.cameraT.position;
			endCamera = startCamera + new Vector3(0f, (speed <= 1.2f) ? -0.04f : -0.02f, 0f);
			IL_1E7:
			if (!DialogManager.IsShowing())
			{
				sceneBook.StartCoroutine(AppUtil.MoveEasingVector3(start, end, delegate(Vector3 ret)
				{
					sceneBook.WallGroup.transform.position = ret;
				}, false, moveTime, easeIn, null));
				this.<>2__current = AppUtil.MoveEasingVector3(startCamera, endCamera, new Action<Vector3>(CS$<>8__locals1.<Walking>b__1), false, moveTime, easeIn, null);
				this.<>1__state = 2;
				return true;
			}
			this.<>2__current = null;
			this.<>1__state = 1;
			return true;
			IL_2B1:
			if (!DialogManager.IsShowing())
			{
				sceneBook.StartCoroutine(AppUtil.MoveEasingVector3(start, end, delegate(Vector3 ret)
				{
					sceneBook.WallGroup.transform.position = ret;
				}, false, moveTime, easeOut, null));
				this.<>2__current = AppUtil.MoveEasingVector3(endCamera, startCamera, new Action<Vector3>(CS$<>8__locals1.<Walking>b__3), false, moveTime, easeOut, null);
				this.<>1__state = 4;
				return true;
			}
			this.<>2__current = null;
			this.<>1__state = 3;
			return true;
		}

		// Token: 0x170000F0 RID: 240
		// (get) Token: 0x06000842 RID: 2114 RVA: 0x00029725 File Offset: 0x00027925
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000843 RID: 2115 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000F1 RID: 241
		// (get) Token: 0x06000844 RID: 2116 RVA: 0x00029725 File Offset: 0x00027925
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0400032E RID: 814
		private int <>1__state;

		// Token: 0x0400032F RID: 815
		private object <>2__current;

		// Token: 0x04000330 RID: 816
		public SceneBook <>4__this;

		// Token: 0x04000331 RID: 817
		public bool withComment;

		// Token: 0x04000332 RID: 818
		private SceneBook.<>c__DisplayClass30_0 <>8__1;

		// Token: 0x04000333 RID: 819
		private int <i>5__2;

		// Token: 0x04000334 RID: 820
		private CommentManager <mgr>5__3;

		// Token: 0x04000335 RID: 821
		private float <speed>5__4;

		// Token: 0x04000336 RID: 822
		private EasingFunction.Ease <easeIn>5__5;

		// Token: 0x04000337 RID: 823
		private EasingFunction.Ease <easeOut>5__6;

		// Token: 0x04000338 RID: 824
		private float <fukidasiRate>5__7;

		// Token: 0x04000339 RID: 825
		private string <commentMode>5__8;

		// Token: 0x0400033A RID: 826
		private float <moveTime>5__9;

		// Token: 0x0400033B RID: 827
		private float <distance>5__10;

		// Token: 0x0400033C RID: 828
		private Vector3 <start>5__11;

		// Token: 0x0400033D RID: 829
		private Vector3 <end>5__12;

		// Token: 0x0400033E RID: 830
		private Vector3 <startCamera>5__13;

		// Token: 0x0400033F RID: 831
		private Vector3 <endCamera>5__14;
	}

	// Token: 0x020000DE RID: 222
	[CompilerGenerated]
	private sealed class <>c__DisplayClass31_0
	{
		// Token: 0x06000845 RID: 2117 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass31_0()
		{
		}

		// Token: 0x06000846 RID: 2118 RVA: 0x0002972D File Offset: 0x0002792D
		internal void <WalkingHAMode>b__1(Vector3 ret)
		{
			this.<>4__this.WallGroup.transform.position = ret;
		}

		// Token: 0x06000847 RID: 2119 RVA: 0x00029745 File Offset: 0x00027945
		internal void <WalkingHAMode>b__0()
		{
			this.mgr.Generate(false, "HyperActiveLast", this.fukidasiRate);
		}

		// Token: 0x06000848 RID: 2120 RVA: 0x0002972D File Offset: 0x0002792D
		internal void <WalkingHAMode>b__2(Vector3 ret)
		{
			this.<>4__this.WallGroup.transform.position = ret;
		}

		// Token: 0x04000340 RID: 832
		public SceneBook <>4__this;

		// Token: 0x04000341 RID: 833
		public CommentManager mgr;

		// Token: 0x04000342 RID: 834
		public float fukidasiRate;

		// Token: 0x04000343 RID: 835
		public Action<Vector3> <>9__1;

		// Token: 0x04000344 RID: 836
		public Action<Vector3> <>9__2;
	}

	// Token: 0x020000DF RID: 223
	[CompilerGenerated]
	private sealed class <WalkingHAMode>d__31 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000849 RID: 2121 RVA: 0x0002975E File Offset: 0x0002795E
		[DebuggerHidden]
		public <WalkingHAMode>d__31(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x0600084A RID: 2122 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x0600084B RID: 2123 RVA: 0x00029770 File Offset: 0x00027970
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			SceneBook sceneBook = this;
			switch (num)
			{
			case 0:
			{
				this.<>1__state = -1;
				CS$<>8__locals1 = new SceneBook.<>c__DisplayClass31_0();
				CS$<>8__locals1.<>4__this = this;
				float haParameters = sceneBook.GetHaParameters(sceneBook.HyperActivePhase, SceneBook.HA_INDEX.SPEED);
				moveTime = 0.42f / haParameters * 2f;
				distance = -1.5f;
				CS$<>8__locals1.fukidasiRate = sceneBook.GetHaParameters(sceneBook.HyperActivePhase, SceneBook.HA_INDEX.RATE);
				CS$<>8__locals1.mgr = sceneBook.GetComponentInChildren<CommentManager>();
				float num2 = moveTime * CS$<>8__locals1.fukidasiRate;
				sceneBook.InvokeRepeating("GenerateComment", num2, num2);
				sceneBook.InvokeRepeating("DestroyComment", num2, num2);
				break;
			}
			case 1:
				this.<>1__state = -1;
				this.<>2__current = AppUtil.Wait(moveTime * 0.05f);
				this.<>1__state = 2;
				return true;
			case 2:
			{
				this.<>1__state = -1;
				float num3 = 40f;
				if (sceneBook.transform.Find("WallGroup/Wall/Ego").gameObject.activeSelf)
				{
					num3 = 38.475f;
				}
				while (sceneBook.WallGroup.transform.localPosition.z <= -num3)
				{
					sceneBook.WallGroup.transform.Translate(new Vector3(0f, 0f, num3));
					Comment[] componentsInChildren = sceneBook.GetComponentsInChildren<Comment>();
					for (int i = 0; i < componentsInChildren.Length; i++)
					{
						componentsInChildren[i].transform.Translate(new Vector3(0f, 0f, -num3));
					}
				}
				break;
			}
			case 3:
				this.<>1__state = -1;
				this.<>2__current = AppUtil.Wait(moveTime * 0.05f);
				this.<>1__state = 4;
				return true;
			case 4:
			{
				this.<>1__state = -1;
				float num4 = 40f;
				if (sceneBook.transform.Find("WallGroup/Wall/Ego").gameObject.activeSelf)
				{
					num4 = 38.475f;
				}
				while (sceneBook.WallGroup.transform.localPosition.z <= -num4)
				{
					sceneBook.WallGroup.transform.Translate(new Vector3(0f, 0f, num4));
					Comment[] componentsInChildren = sceneBook.GetComponentsInChildren<Comment>();
					for (int i = 0; i < componentsInChildren.Length; i++)
					{
						componentsInChildren[i].transform.Translate(new Vector3(0f, 0f, -num4));
					}
				}
				goto IL_402;
			}
			default:
				return false;
			}
			float passedTime = HyperActive.PassedTime;
			sceneBook.UpdateHYPhase(passedTime);
			float haParameters2 = sceneBook.GetHaParameters(sceneBook.HyperActivePhase, SceneBook.HA_INDEX.RATE);
			if (haParameters2 != 0f && haParameters2 < 1f)
			{
				Vector3 position = sceneBook.WallGroup.transform.position;
				Vector3 vector = position + new Vector3(0f, 0f, distance * 2f);
				MonoBehaviour monoBehaviour = sceneBook;
				Vector3 startValue = position;
				Vector3 endValue = vector;
				Action<Vector3> callback;
				if ((callback = CS$<>8__locals1.<>9__1) == null)
				{
					callback = (CS$<>8__locals1.<>9__1 = new Action<Vector3>(CS$<>8__locals1.<WalkingHAMode>b__1));
				}
				this.<>2__current = monoBehaviour.StartCoroutine(AppUtil.MoveEasingVector3(startValue, endValue, callback, false, moveTime * 0.95f, EasingFunction.Ease.Linear, null));
				this.<>1__state = 1;
				return true;
			}
			sceneBook.CancelInvoke("GenerateComment");
			AppUtil.DelayAction(sceneBook, moveTime, new Action(CS$<>8__locals1.<WalkingHAMode>b__0), true);
			IL_402:
			if (sceneBook.GetComponentsInChildren<Comment>().Length == 0)
			{
				sceneBook.CancelInvoke("DestroyComment");
				sceneBook.StartCoroutine(sceneBook.Walking(true));
				return false;
			}
			Vector3 position2 = sceneBook.WallGroup.transform.position;
			Vector3 vector2 = position2 + new Vector3(0f, 0f, distance * 2f);
			MonoBehaviour monoBehaviour2 = sceneBook;
			Vector3 startValue2 = position2;
			Vector3 endValue2 = vector2;
			Action<Vector3> callback2;
			if ((callback2 = CS$<>8__locals1.<>9__2) == null)
			{
				callback2 = (CS$<>8__locals1.<>9__2 = new Action<Vector3>(CS$<>8__locals1.<WalkingHAMode>b__2));
			}
			this.<>2__current = monoBehaviour2.StartCoroutine(AppUtil.MoveEasingVector3(startValue2, endValue2, callback2, false, moveTime * 0.95f, EasingFunction.Ease.Linear, null));
			this.<>1__state = 3;
			return true;
		}

		// Token: 0x170000F2 RID: 242
		// (get) Token: 0x0600084C RID: 2124 RVA: 0x00029BA5 File Offset: 0x00027DA5
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0600084D RID: 2125 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000F3 RID: 243
		// (get) Token: 0x0600084E RID: 2126 RVA: 0x00029BA5 File Offset: 0x00027DA5
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x04000345 RID: 837
		private int <>1__state;

		// Token: 0x04000346 RID: 838
		private object <>2__current;

		// Token: 0x04000347 RID: 839
		public SceneBook <>4__this;

		// Token: 0x04000348 RID: 840
		private SceneBook.<>c__DisplayClass31_0 <>8__1;

		// Token: 0x04000349 RID: 841
		private float <moveTime>5__2;

		// Token: 0x0400034A RID: 842
		private float <distance>5__3;
	}
}
