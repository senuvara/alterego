﻿using System;
using App;
using UnityEngine;

// Token: 0x02000017 RID: 23
public class RemoteSettingsReceiver : MonoBehaviour
{
	// Token: 0x0600009A RID: 154 RVA: 0x00008ADE File Offset: 0x00006CDE
	private void Start()
	{
		this.ReadRemoteSettings();
		RemoteSettings.Completed += this.ReadRemoteSettings;
	}

	// Token: 0x0600009B RID: 155 RVA: 0x00008AF7 File Offset: 0x00006CF7
	private void OnDestroy()
	{
		RemoteSettings.Completed -= this.ReadRemoteSettings;
	}

	// Token: 0x0600009C RID: 156 RVA: 0x00008B0A File Offset: 0x00006D0A
	private void ReadRemoteSettings(bool wasUpdatedFromServer, bool settingsChanged, int serverResponse)
	{
		this.ReadRemoteSettings();
	}

	// Token: 0x0600009D RID: 157 RVA: 0x00008B14 File Offset: 0x00006D14
	private void ReadRemoteSettings()
	{
		string saveDataID = Settings.SaveDataID;
		if (string.IsNullOrEmpty(saveDataID))
		{
			return;
		}
		string @string = RemoteSettings.GetString(saveDataID);
		foreach (string text in PurchasingItem.ITEM_LIST)
		{
			if (@string.Contains(text))
			{
				PurchasingItem.Set(text, true);
			}
		}
	}

	// Token: 0x0600009E RID: 158 RVA: 0x000025AD File Offset: 0x000007AD
	public RemoteSettingsReceiver()
	{
	}
}
