﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using App;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000021 RID: 33
public class TalkItem : MonoBehaviour
{
	// Token: 0x06000108 RID: 264 RVA: 0x0000C122 File Offset: 0x0000A322
	private void Awake()
	{
		this.MainButton = base.transform.Find("ScenarioButton");
	}

	// Token: 0x06000109 RID: 265 RVA: 0x0000C13A File Offset: 0x0000A33A
	private void FixedUpdate()
	{
		this.UpdateStatus();
	}

	// Token: 0x0600010A RID: 266 RVA: 0x0000C142 File Offset: 0x0000A342
	private void OnEnable()
	{
		base.transform.Find("RankUpEffect").gameObject.SetActive(false);
	}

	// Token: 0x0600010B RID: 267 RVA: 0x0000C15F File Offset: 0x0000A35F
	public void OnClick(GameObject clickObject)
	{
		global::Debug.Log("TalkItem:OnClick " + clickObject.name);
	}

	// Token: 0x0600010C RID: 268 RVA: 0x0000C176 File Offset: 0x0000A376
	public void OnLevelUp(string before, string after)
	{
		base.StartCoroutine(this.LevelUpEffect(before, after));
	}

	// Token: 0x0600010D RID: 269 RVA: 0x0000C187 File Offset: 0x0000A387
	private IEnumerator LevelUpEffect(string before, string after)
	{
		string text = base.transform.Find("EffectText").GetComponent<Text>().text;
		text = text.Replace("タップ獲得EGO", "タップ獲得EGO\n");
		after = after.Replace("/秒", "<size=38>/秒</size>");
		string str = "診断";
		if (Data.GetScenarioType(this.ScenarioNo).Contains("対話"))
		{
			str = "対話";
		}
		if (Data.GetScenarioSpecific(this.ScenarioNo) == "書評")
		{
			str = "本";
		}
		GameObject effect = base.transform.Find("RankUpEffect").gameObject;
		base.transform.Find("RankUpEffect/TitleText").GetComponent<TextLocalization>().SetKey("[UI]RankUpEffect/TitleText" + str);
		base.transform.Find("RankUpEffect/ValueLayout/EffectText").GetComponent<TextLocalization>().SetText(text);
		base.transform.Find("RankUpEffect/ValueLayout/BeforeText").GetComponent<TextLocalization>().SetText(before);
		base.transform.Find("RankUpEffect/ValueLayout/AfterText").GetComponent<TextLocalization>().SetText(after);
		float num = 0f;
		foreach (RectTransform rectTransform in base.transform.Find("RankUpEffect/ValueLayout").GetComponentsInChildren<RectTransform>())
		{
			if (!(rectTransform.name == "ValueLayout"))
			{
				rectTransform.anchoredPosition = new Vector2(num, rectTransform.anchoredPosition.y);
				float preferredWidth = rectTransform.GetComponent<Text>().preferredWidth;
				rectTransform.sizeDelta = new Vector2(preferredWidth, 100f);
				num += preferredWidth + 20f;
				if (rectTransform.name == "Arrow" || rectTransform.name == "AfterText")
				{
					num += 10f;
				}
			}
		}
		RectTransform componentInChildren = base.transform.Find("RankUpEffect/ValueLayout").GetComponentInChildren<RectTransform>();
		componentInChildren.anchoredPosition = new Vector2((componentInChildren.parent.GetComponent<RectTransform>().rect.width - num) / 2f, -30f);
		effect.SetActive(true);
		yield return AppUtil.FadeIn(effect, 0.5f, null);
		global::Debug.Log("AutoTestEvent:Screenshot");
		this.SetItem(this.ScenarioNo, 0, false);
		yield return AppUtil.Wait(2f);
		yield return AppUtil.FadeOut(effect, 0.5f, null);
		effect.SetActive(false);
		yield break;
	}

	// Token: 0x0600010E RID: 270 RVA: 0x0000C1A4 File Offset: 0x0000A3A4
	public void SetItem()
	{
		if (base.transform.Find("RetryNoAdButton") != null || base.transform.Find("RestartButtonAE") != null)
		{
			return;
		}
		this.SetItem(this.ScenarioNo, 0, false);
	}

	// Token: 0x0600010F RID: 271 RVA: 0x0000C1F0 File Offset: 0x0000A3F0
	public void SetItem(string scenarioNo, int count = 0, bool rememberMode = false)
	{
		string scenarioSpecific = Data.GetScenarioSpecific(scenarioNo);
		if (scenarioNo.Contains("3章") && (scenarioSpecific.Contains("ルート分岐") || scenarioSpecific.Contains("章END")))
		{
			scenarioNo = scenarioNo.Replace(PlayerStatus.Route3, PlayerStatus.Route);
		}
		this.ScenarioNo = scenarioNo;
		if (!Utility.EsExists() && !scenarioSpecific.Contains("退行"))
		{
			base.transform.Find("ScenarioRetryButton").gameObject.SetActive(true);
			base.transform.Find("InActiveScreen").gameObject.SetActive(true);
			this.MainButton.gameObject.SetActive(false);
			base.transform.Find("EffectText").gameObject.SetActive(false);
			this.MainButton.Find("ButterflyNotice").gameObject.SetActive(false);
			this.MainButton.Find("Light").gameObject.SetActive(false);
			return;
		}
		base.gameObject.name = "ScenarioItem" + scenarioNo;
		string[] scenarioItemData = Data.GetScenarioItemData(scenarioNo);
		string scenarioType = Data.GetScenarioType(scenarioNo);
		if (count != 0)
		{
			this.Title1 = string.Concat(new object[]
			{
				scenarioNo[0].ToString(),
				"-",
				count,
				" ",
				LanguageManager.Get("[Es]" + scenarioType + "_タイトル")
			});
			if (rememberMode)
			{
				string route = Data.GetRoute(scenarioNo);
				if (route != "")
				{
					this.Title1 = this.Title1 + " (" + route + ")";
				}
			}
		}
		if (scenarioNo == "エンドロール")
		{
			this.Title1 = "3-X " + LanguageManager.Get("[Es]対話23_タイトル");
		}
		base.transform.Find("TitleText").GetComponent<TextLocalization>().SetText(this.Title1);
		base.transform.Find("SubtitleText").GetComponent<TextLocalization>().SetKey("[Es]" + scenarioType + "_サブタイトル");
		if (scenarioType.Contains("診断"))
		{
			float preferredWidth = base.transform.Find("TitleText").GetComponent<Text>().preferredWidth;
			base.transform.Find("MirrorIcon").gameObject.SetActive(true);
			base.transform.Find("MirrorIcon").GetComponent<RectTransform>().anchoredPosition = new Vector2(75.3f + preferredWidth, 55.5f);
		}
		Color white = Color.white;
		if (scenarioItemData[0] == "" || scenarioItemData[0] == "0")
		{
			base.transform.Find("EffectText").gameObject.SetActive(false);
			bool flag = false;
			string text = null;
			string name = null;
			if (scenarioSpecific.Contains("退行"))
			{
				text = "[UI]RestartButton/Text";
				name = "RestartButtonAE";
				if (PlayerStatus.Route != "AE")
				{
					flag = true;
					name = "RestartButton";
				}
			}
			else if (scenarioSpecific.Contains("雑談"))
			{
				flag = true;
				text = "[UI]TalkButton/Text";
				name = "雑談Button";
				if (this.MainButton.Find("Light") != null)
				{
					Object.Destroy(this.MainButton.Find("Light").gameObject);
				}
			}
			else if (scenarioSpecific.Contains("日替"))
			{
				flag = true;
				if (!PlayerStatus.EnableDailyBonus)
				{
					this.SetButtonActive(false);
				}
				text = "[UI]GreetingButton/Text";
				name = "日替わりButton";
			}
			else if (scenarioSpecific.Contains("書評"))
			{
				flag = true;
				name = "本を返すButton";
				this.TimeCounter = null;
				this.MainButton.Find("PriceText").GetComponent<LetterSpacing>().spacing = 0f;
				if (TimeManager.IsInTime(TimeManager.TYPE.END_BOOK))
				{
					this.SetButtonActive(false);
					text = "[UI]ReadingButton/Text";
					this.TimeCounter = this.MainButton.Find("PriceText").GetComponent<TextLocalization>();
					this.MainButton.Find("PriceText").GetComponent<TextLocalization>().SetText("");
				}
				else if (PlayerStatus.ReadingBook > 0)
				{
					text = "[UI]GetBackButton/Text";
					this.PrizeType = "時間";
					string id = "[UI]ScenarioItem/EffectTextTime";
					int num = 2;
					base.transform.Find("EffectText").GetComponent<TextLocalization>().SetText(num.ToString(LanguageManager.Get(id)));
				}
				else
				{
					name = "本を借りるButton";
					if (!Data.ReadingBookIsOver)
					{
						this.Price = Data.NEXT_HAYAKAWA_BOOK_PRICE;
						text = null;
						this.MainButton.Find("PriceText").GetComponent<TextLocalization>().SetText(this.Price.ToString(LanguageManager.Get("[UI]ScenarioButton/PriceText")));
					}
					else
					{
						this.Price = null;
						text = "[UI]TakeBookButton/Text";
						this.MainButton.Find("PriceText").GetComponent<LetterSpacing>().spacing = -20f;
					}
				}
				GameObject gameObject = base.transform.Find("BookText").gameObject;
				string text2 = Data.GetScenarioKey("本を借りる1", "9選択肢", PlayerStatus.ReadingBook.ToString(), 0);
				if (text2 == null)
				{
					text2 = "[UI]BookInfo/TitleText";
				}
				gameObject.GetComponent<TextLocalization>().SetKey(text2);
			}
			else if (scenarioSpecific.Contains("自我とエス"))
			{
				this.MainButton.gameObject.SetActive(false);
				flag = true;
				if (PurchasingItem.GetByLabel(scenarioSpecific))
				{
					if (base.transform.Find("IchEsButton") != null)
					{
						this.MainButton = base.transform.Find("IchEsButton");
					}
					name = scenarioSpecific + "Button";
				}
				else
				{
					this.MainButton = base.transform.Find("IchEsButtonUnlocked");
					name = "IchEsButtonUnlocked";
				}
			}
			this.MainButton.gameObject.SetActive(flag);
			base.transform.Find("ScenarioRetryButton").gameObject.SetActive(!flag);
			if (flag)
			{
				if (text != null)
				{
					this.MainButton.Find("PriceText").GetComponent<TextLocalization>().SetKey(text);
				}
				this.MainButton.gameObject.name = name;
			}
			else
			{
				base.transform.Find("ScenarioRetryButton/Text").GetComponent<TextLocalization>().SetKey(text);
				base.transform.Find("ScenarioRetryButton").gameObject.name = name;
			}
		}
		else if (rememberMode)
		{
			base.transform.Find("ScenarioRetryButton").gameObject.SetActive(true);
			this.MainButton.gameObject.SetActive(false);
			string key = "[UI]RememberButton/Text";
			base.transform.Find("ScenarioRetryButton/Text").GetComponent<TextLocalization>().SetKey(key);
			base.transform.Find("ScenarioRetryButton").gameObject.name = "RetryNoAdButton";
			base.transform.Find("EffectText").gameObject.SetActive(false);
		}
		else if (!Data.CLEAR_SCENARIO_LIST.Contains(scenarioNo))
		{
			this.Price = new EgoPoint(scenarioItemData[0]);
			this.MainButton.Find("PriceText").GetComponent<TextLocalization>().SetText(this.Price.ToString(LanguageManager.Get("[UI]ScenarioButton/PriceText")));
			string id2 = "[UI]ScenarioItem/EffectTextTap";
			this.PrizeType = "タップ";
			if (scenarioItemData[1] == "時間")
			{
				this.PrizeType = "時間";
				id2 = "[UI]ScenarioItem/EffectTextTime";
			}
			base.transform.Find("EffectText").GetComponent<TextLocalization>().SetText(int.Parse(scenarioItemData[2]).ToString(LanguageManager.Get(id2)));
			base.transform.Find("ScenarioRetryButton").gameObject.SetActive(false);
			if (scenarioNo != PlayerStatus.ScenarioNo)
			{
				base.transform.Find("InActiveScreen").gameObject.SetActive(true);
				this.SetButtonActive(false);
			}
			else
			{
				base.transform.Find("InActiveScreen").gameObject.SetActive(false);
			}
		}
		else
		{
			base.transform.Find("ScenarioRetryButton").gameObject.SetActive(true);
			if (!AdInfo.ENABLE)
			{
				base.transform.Find("ScenarioRetryButton/Text").GetComponent<TextLocalization>().SetKey("[UI]ScenarioRetryButton/TextNoAd");
			}
			this.MainButton.gameObject.SetActive(false);
			base.transform.Find("EffectText").gameObject.SetActive(false);
			white = new Color(0.54f, 0.54f, 0.54f);
		}
		base.transform.Find("TitleText").GetComponent<Text>().color = white;
		base.transform.Find("SubtitleText").GetComponent<Text>().color = white;
		base.transform.Find("MirrorIcon").GetComponent<Image>().color = white;
		this.UpdateStatus();
	}

	// Token: 0x06000110 RID: 272 RVA: 0x0000CB08 File Offset: 0x0000AD08
	private void UpdateStatus()
	{
		if (this.TimeCounter != null)
		{
			if (TimeManager.IsInTime(TimeManager.TYPE.END_BOOK))
			{
				TimeSpan gapTime = TimeManager.GetGapTime(TimeManager.TYPE.END_BOOK);
				this.TimeCounter.SetParameters(new object[]
				{
					gapTime.ToString("h\\:mm\\:ss")
				});
				return;
			}
			this.SetItem();
			this.SetButtonActive(true);
			return;
		}
		else
		{
			if (!this.MainButton.gameObject.activeSelf)
			{
				return;
			}
			if (this.Price == null)
			{
				return;
			}
			if (!this.IsActive())
			{
				return;
			}
			bool buttonActive = PlayerStatus.EgoPoint >= this.Price;
			this.SetButtonActive(buttonActive);
			return;
		}
	}

	// Token: 0x06000111 RID: 273 RVA: 0x0000CB9E File Offset: 0x0000AD9E
	public bool IsActive()
	{
		return !base.transform.Find("InActiveScreen").gameObject.activeSelf;
	}

	// Token: 0x06000112 RID: 274 RVA: 0x0000CBC0 File Offset: 0x0000ADC0
	public void SetButtonActive(bool buttonEnable)
	{
		this.MainButton.GetComponent<Button>().interactable = buttonEnable;
		if (this.MainButton.Find("ButterflyNotice") != null)
		{
			this.MainButton.Find("ButterflyNotice").gameObject.SetActive(buttonEnable);
		}
		if (this.MainButton.Find("Light") != null)
		{
			this.MainButton.Find("Light").gameObject.SetActive(buttonEnable);
		}
	}

	// Token: 0x06000113 RID: 275 RVA: 0x0000CC44 File Offset: 0x0000AE44
	public new string GetType()
	{
		return this.PrizeType;
	}

	// Token: 0x06000114 RID: 276 RVA: 0x000025AD File Offset: 0x000007AD
	public TalkItem()
	{
	}

	// Token: 0x04000051 RID: 81
	public string ScenarioNo;

	// Token: 0x04000052 RID: 82
	public EgoPoint Price;

	// Token: 0x04000053 RID: 83
	private string Title1;

	// Token: 0x04000054 RID: 84
	private TextLocalization TimeCounter;

	// Token: 0x04000055 RID: 85
	private string PrizeType;

	// Token: 0x04000056 RID: 86
	private Transform MainButton;

	// Token: 0x020000FC RID: 252
	[CompilerGenerated]
	private sealed class <LevelUpEffect>d__11 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060008CC RID: 2252 RVA: 0x0002D07D File Offset: 0x0002B27D
		[DebuggerHidden]
		public <LevelUpEffect>d__11(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060008CD RID: 2253 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060008CE RID: 2254 RVA: 0x0002D08C File Offset: 0x0002B28C
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			TalkItem talkItem = this;
			switch (num)
			{
			case 0:
			{
				this.<>1__state = -1;
				string text = talkItem.transform.Find("EffectText").GetComponent<Text>().text;
				text = text.Replace("タップ獲得EGO", "タップ獲得EGO\n");
				after = after.Replace("/秒", "<size=38>/秒</size>");
				string str = "診断";
				if (Data.GetScenarioType(talkItem.ScenarioNo).Contains("対話"))
				{
					str = "対話";
				}
				if (Data.GetScenarioSpecific(talkItem.ScenarioNo) == "書評")
				{
					str = "本";
				}
				effect = talkItem.transform.Find("RankUpEffect").gameObject;
				talkItem.transform.Find("RankUpEffect/TitleText").GetComponent<TextLocalization>().SetKey("[UI]RankUpEffect/TitleText" + str);
				talkItem.transform.Find("RankUpEffect/ValueLayout/EffectText").GetComponent<TextLocalization>().SetText(text);
				talkItem.transform.Find("RankUpEffect/ValueLayout/BeforeText").GetComponent<TextLocalization>().SetText(before);
				talkItem.transform.Find("RankUpEffect/ValueLayout/AfterText").GetComponent<TextLocalization>().SetText(after);
				float num2 = 0f;
				foreach (RectTransform rectTransform in talkItem.transform.Find("RankUpEffect/ValueLayout").GetComponentsInChildren<RectTransform>())
				{
					if (!(rectTransform.name == "ValueLayout"))
					{
						rectTransform.anchoredPosition = new Vector2(num2, rectTransform.anchoredPosition.y);
						float preferredWidth = rectTransform.GetComponent<Text>().preferredWidth;
						rectTransform.sizeDelta = new Vector2(preferredWidth, 100f);
						num2 += preferredWidth + 20f;
						if (rectTransform.name == "Arrow" || rectTransform.name == "AfterText")
						{
							num2 += 10f;
						}
					}
				}
				RectTransform componentInChildren = talkItem.transform.Find("RankUpEffect/ValueLayout").GetComponentInChildren<RectTransform>();
				componentInChildren.anchoredPosition = new Vector2((componentInChildren.parent.GetComponent<RectTransform>().rect.width - num2) / 2f, -30f);
				effect.SetActive(true);
				this.<>2__current = AppUtil.FadeIn(effect, 0.5f, null);
				this.<>1__state = 1;
				return true;
			}
			case 1:
				this.<>1__state = -1;
				global::Debug.Log("AutoTestEvent:Screenshot");
				talkItem.SetItem(talkItem.ScenarioNo, 0, false);
				this.<>2__current = AppUtil.Wait(2f);
				this.<>1__state = 2;
				return true;
			case 2:
				this.<>1__state = -1;
				this.<>2__current = AppUtil.FadeOut(effect, 0.5f, null);
				this.<>1__state = 3;
				return true;
			case 3:
				this.<>1__state = -1;
				effect.SetActive(false);
				return false;
			default:
				return false;
			}
		}

		// Token: 0x17000114 RID: 276
		// (get) Token: 0x060008CF RID: 2255 RVA: 0x0002D396 File Offset: 0x0002B596
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060008D0 RID: 2256 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000115 RID: 277
		// (get) Token: 0x060008D1 RID: 2257 RVA: 0x0002D396 File Offset: 0x0002B596
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040003C1 RID: 961
		private int <>1__state;

		// Token: 0x040003C2 RID: 962
		private object <>2__current;

		// Token: 0x040003C3 RID: 963
		public TalkItem <>4__this;

		// Token: 0x040003C4 RID: 964
		public string after;

		// Token: 0x040003C5 RID: 965
		public string before;

		// Token: 0x040003C6 RID: 966
		private GameObject <effect>5__2;
	}
}
