﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

// Token: 0x02000025 RID: 37
[RequireComponent(typeof(Text))]
[AddComponentMenu("UI/Effects/TypefaceAnimator")]
public class TypefaceAnimator : BaseMeshEffect
{
	// Token: 0x17000014 RID: 20
	// (get) Token: 0x06000139 RID: 313 RVA: 0x0000DBB8 File Offset: 0x0000BDB8
	// (set) Token: 0x0600013A RID: 314 RVA: 0x0000DBC0 File Offset: 0x0000BDC0
	public float progress
	{
		get
		{
			return this.m_progress;
		}
		set
		{
			this.m_progress = value;
			if (base.graphic != null)
			{
				base.graphic.SetVerticesDirty();
			}
		}
	}

	// Token: 0x17000015 RID: 21
	// (get) Token: 0x0600013B RID: 315 RVA: 0x0000DBE2 File Offset: 0x0000BDE2
	public bool isPlaying
	{
		get
		{
			return this.m_isPlaying;
		}
	}

	// Token: 0x0600013C RID: 316 RVA: 0x0000DBEA File Offset: 0x0000BDEA
	protected override void OnEnable()
	{
		if (this.playOnAwake)
		{
			this.Play();
		}
		base.OnEnable();
	}

	// Token: 0x0600013D RID: 317 RVA: 0x0000DC00 File Offset: 0x0000BE00
	protected override void OnDisable()
	{
		this.Stop();
		base.OnDisable();
	}

	// Token: 0x0600013E RID: 318 RVA: 0x0000DC10 File Offset: 0x0000BE10
	public void Play()
	{
		this.progress = 0f;
		TypefaceAnimator.TimeMode timeMode = this.timeMode;
		if (timeMode != TypefaceAnimator.TimeMode.Time)
		{
			if (timeMode == TypefaceAnimator.TimeMode.Speed)
			{
				this.animationTime = (float)this.characterNumber / 10f / this.speed;
			}
		}
		else
		{
			this.animationTime = this.duration;
		}
		switch (this.style)
		{
		case TypefaceAnimator.Style.Once:
			this.playCoroutine = base.StartCoroutine(this.PlayOnceCoroutine());
			return;
		case TypefaceAnimator.Style.Loop:
			this.playCoroutine = base.StartCoroutine(this.PlayLoopCoroutine());
			return;
		case TypefaceAnimator.Style.PingPong:
			this.playCoroutine = base.StartCoroutine(this.PlayPingPongCoroutine());
			return;
		default:
			return;
		}
	}

	// Token: 0x0600013F RID: 319 RVA: 0x0000DCB2 File Offset: 0x0000BEB2
	public void Stop()
	{
		if (this.playCoroutine != null)
		{
			base.StopCoroutine(this.playCoroutine);
		}
		this.m_isPlaying = false;
		this.playCoroutine = null;
	}

	// Token: 0x06000140 RID: 320 RVA: 0x0000DCD6 File Offset: 0x0000BED6
	private IEnumerator PlayOnceCoroutine()
	{
		if (this.delay > 0f)
		{
			yield return new WaitForSeconds(this.delay);
		}
		if (this.m_isPlaying)
		{
			yield break;
		}
		this.m_isPlaying = true;
		if (this.onStart != null)
		{
			this.onStart.Invoke();
		}
		while (this.progress < 1f)
		{
			this.progress += Time.deltaTime / this.animationTime;
			yield return null;
		}
		this.m_isPlaying = false;
		this.progress = 1f;
		if (this.onComplete != null)
		{
			this.onComplete.Invoke();
		}
		yield break;
	}

	// Token: 0x06000141 RID: 321 RVA: 0x0000DCE5 File Offset: 0x0000BEE5
	private IEnumerator PlayLoopCoroutine()
	{
		if (this.delay > 0f)
		{
			yield return new WaitForSeconds(this.delay);
		}
		if (this.m_isPlaying)
		{
			yield break;
		}
		this.m_isPlaying = true;
		if (this.onStart != null)
		{
			this.onStart.Invoke();
		}
		for (;;)
		{
			this.progress += Time.deltaTime / this.animationTime;
			if (this.progress > 1f)
			{
				this.progress -= 1f;
			}
			yield return null;
		}
		yield break;
	}

	// Token: 0x06000142 RID: 322 RVA: 0x0000DCF4 File Offset: 0x0000BEF4
	private IEnumerator PlayPingPongCoroutine()
	{
		if (this.delay > 0f)
		{
			yield return new WaitForSeconds(this.delay);
		}
		if (this.m_isPlaying)
		{
			yield break;
		}
		this.m_isPlaying = true;
		if (this.onStart != null)
		{
			this.onStart.Invoke();
		}
		bool isPositive = true;
		for (;;)
		{
			float num = Time.deltaTime / this.animationTime;
			if (isPositive)
			{
				this.progress += num;
				if (this.progress > 1f)
				{
					isPositive = false;
					this.progress -= num;
				}
			}
			else
			{
				this.progress -= num;
				if (this.progress < 0f)
				{
					isPositive = true;
					this.progress += num;
				}
			}
			yield return null;
		}
		yield break;
	}

	// Token: 0x06000143 RID: 323 RVA: 0x0000DD04 File Offset: 0x0000BF04
	public override void ModifyMesh(VertexHelper vertexHelper)
	{
		if (!this.IsActive() || vertexHelper.currentVertCount == 0)
		{
			return;
		}
		List<UIVertex> list = new List<UIVertex>();
		vertexHelper.GetUIVertexStream(list);
		List<UIVertex> list2 = new List<UIVertex>();
		for (int i = 0; i < list.Count; i++)
		{
			int num = i % 6;
			if (num == 0 || num == 1 || num == 2 || num == 4)
			{
				list2.Add(list[i]);
			}
		}
		this.ModifyVertices(list2);
		List<UIVertex> list3 = new List<UIVertex>(list.Count);
		for (int j = 0; j < list.Count / 6; j++)
		{
			int num2 = j * 4;
			list3.Add(list2[num2]);
			list3.Add(list2[num2 + 1]);
			list3.Add(list2[num2 + 2]);
			list3.Add(list2[num2 + 2]);
			list3.Add(list2[num2 + 3]);
			list3.Add(list2[num2]);
		}
		vertexHelper.Clear();
		vertexHelper.AddUIVertexTriangleStream(list3);
	}

	// Token: 0x06000144 RID: 324 RVA: 0x0000DE04 File Offset: 0x0000C004
	public void ModifyVertices(List<UIVertex> verts)
	{
		if (!this.IsActive())
		{
			return;
		}
		this.Modify(verts);
	}

	// Token: 0x06000145 RID: 325 RVA: 0x0000DE18 File Offset: 0x0000C018
	private void Modify(List<UIVertex> verts)
	{
		this.characterNumber = verts.Count / 4;
		for (int i = 0; i < verts.Count; i++)
		{
			if (i % 4 == 0)
			{
				int currentCharacterNumber = i / 4;
				UIVertex uivertex = verts[i];
				UIVertex uivertex2 = verts[i + 1];
				UIVertex uivertex3 = verts[i + 2];
				UIVertex uivertex4 = verts[i + 3];
				if (this.usePosition)
				{
					float d = this.positionAnimationCurve.Evaluate(TypefaceAnimator.SeparationRate(this.progress, currentCharacterNumber, this.characterNumber, this.positionSeparation));
					Vector3 b = (this.positionTo - this.positionFrom) * d + this.positionFrom;
					uivertex.position += b;
					uivertex2.position += b;
					uivertex3.position += b;
					uivertex4.position += b;
				}
				if (this.useScale)
				{
					if (this.scaleSyncXY)
					{
						float num = this.scaleAnimationCurve.Evaluate(TypefaceAnimator.SeparationRate(this.progress, currentCharacterNumber, this.characterNumber, this.scaleSeparation));
						float d2 = (this.scaleTo - this.scaleFrom) * num + this.scaleFrom;
						float x = (uivertex2.position.x - uivertex4.position.x) * this.scalePivot.x + uivertex4.position.x;
						float y = (uivertex2.position.y - uivertex4.position.y) * this.scalePivot.y + uivertex4.position.y;
						Vector3 b2 = new Vector3(x, y, 0f);
						uivertex.position = (uivertex.position - b2) * d2 + b2;
						uivertex2.position = (uivertex2.position - b2) * d2 + b2;
						uivertex3.position = (uivertex3.position - b2) * d2 + b2;
						uivertex4.position = (uivertex4.position - b2) * d2 + b2;
					}
					else
					{
						float num2 = this.scaleAnimationCurve.Evaluate(TypefaceAnimator.SeparationRate(this.progress, currentCharacterNumber, this.characterNumber, this.scaleSeparation));
						float d3 = (this.scaleTo - this.scaleFrom) * num2 + this.scaleFrom;
						float x2 = (uivertex2.position.x - uivertex4.position.x) * this.scalePivot.x + uivertex4.position.x;
						float y2 = (uivertex2.position.y - uivertex4.position.y) * this.scalePivot.y + uivertex4.position.y;
						Vector3 b3 = new Vector3(x2, y2, 0f);
						uivertex.position = new Vector3(((uivertex.position - b3) * d3 + b3).x, uivertex.position.y, uivertex.position.z);
						uivertex2.position = new Vector3(((uivertex2.position - b3) * d3 + b3).x, uivertex2.position.y, uivertex2.position.z);
						uivertex3.position = new Vector3(((uivertex3.position - b3) * d3 + b3).x, uivertex3.position.y, uivertex3.position.z);
						uivertex4.position = new Vector3(((uivertex4.position - b3) * d3 + b3).x, uivertex4.position.y, uivertex4.position.z);
						num2 = this.scaleAnimationCurveY.Evaluate(TypefaceAnimator.SeparationRate(this.progress, currentCharacterNumber, this.characterNumber, this.scaleSeparation));
						d3 = (this.scaleToY - this.scaleFromY) * num2 + this.scaleFromY;
						x2 = (uivertex2.position.x - uivertex4.position.x) * this.scalePivotY.x + uivertex4.position.x;
						y2 = (uivertex2.position.y - uivertex4.position.y) * this.scalePivotY.y + uivertex4.position.y;
						b3 = new Vector3(x2, y2, 0f);
						uivertex.position = new Vector3(uivertex.position.x, ((uivertex.position - b3) * d3 + b3).y, uivertex.position.z);
						uivertex2.position = new Vector3(uivertex2.position.x, ((uivertex2.position - b3) * d3 + b3).y, uivertex2.position.z);
						uivertex3.position = new Vector3(uivertex3.position.x, ((uivertex3.position - b3) * d3 + b3).y, uivertex3.position.z);
						uivertex4.position = new Vector3(uivertex4.position.x, ((uivertex4.position - b3) * d3 + b3).y, uivertex4.position.z);
					}
				}
				if (this.useRotation)
				{
					float num3 = this.rotationAnimationCurve.Evaluate(TypefaceAnimator.SeparationRate(this.progress, currentCharacterNumber, this.characterNumber, this.rotationSeparation));
					float angle = (this.rotationTo - this.rotationFrom) * num3 + this.rotationFrom;
					float x3 = (uivertex2.position.x - uivertex4.position.x) * this.rotationPivot.x + uivertex4.position.x;
					float y3 = (uivertex2.position.y - uivertex4.position.y) * this.rotationPivot.y + uivertex4.position.y;
					Vector3 b4 = new Vector3(x3, y3, 0f);
					uivertex.position = Quaternion.AngleAxis(angle, Vector3.forward) * (uivertex.position - b4) + b4;
					uivertex2.position = Quaternion.AngleAxis(angle, Vector3.forward) * (uivertex2.position - b4) + b4;
					uivertex3.position = Quaternion.AngleAxis(angle, Vector3.forward) * (uivertex3.position - b4) + b4;
					uivertex4.position = Quaternion.AngleAxis(angle, Vector3.forward) * (uivertex4.position - b4) + b4;
				}
				Color color = uivertex.color;
				if (this.useColor)
				{
					float b5 = this.colorAnimationCurve.Evaluate(TypefaceAnimator.SeparationRate(this.progress, currentCharacterNumber, this.characterNumber, this.colorSeparation));
					color = (this.colorTo - this.colorFrom) * b5 + this.colorFrom;
					uivertex.color = (uivertex2.color = (uivertex3.color = (uivertex4.color = color)));
				}
				if (this.useAlpha)
				{
					float num4 = this.alphaAnimationCurve.Evaluate(TypefaceAnimator.SeparationRate(this.progress, currentCharacterNumber, this.characterNumber, this.alphaSeparation));
					float num5 = (this.alphaTo - this.alphaFrom) * num4 + this.alphaFrom;
					color = new Color(color.r, color.g, color.b, color.a * num5);
					uivertex.color = (uivertex2.color = (uivertex3.color = (uivertex4.color = color)));
				}
				verts[i] = uivertex;
				verts[i + 1] = uivertex2;
				verts[i + 2] = uivertex3;
				verts[i + 3] = uivertex4;
			}
		}
	}

	// Token: 0x06000146 RID: 326 RVA: 0x0000E6E1 File Offset: 0x0000C8E1
	private static float SeparationRate(float progress, int currentCharacterNumber, int characterNumber, float separation)
	{
		return Mathf.Clamp01((progress - (float)currentCharacterNumber * separation / (float)characterNumber) / (separation / (float)characterNumber + 1f - separation));
	}

	// Token: 0x06000147 RID: 327 RVA: 0x0000E700 File Offset: 0x0000C900
	public TypefaceAnimator()
	{
	}

	// Token: 0x04000063 RID: 99
	public TypefaceAnimator.TimeMode timeMode;

	// Token: 0x04000064 RID: 100
	public float duration = 1f;

	// Token: 0x04000065 RID: 101
	public float speed = 5f;

	// Token: 0x04000066 RID: 102
	public float delay;

	// Token: 0x04000067 RID: 103
	public TypefaceAnimator.Style style;

	// Token: 0x04000068 RID: 104
	public bool playOnAwake = true;

	// Token: 0x04000069 RID: 105
	[SerializeField]
	private float m_progress = 1f;

	// Token: 0x0400006A RID: 106
	public bool usePosition;

	// Token: 0x0400006B RID: 107
	public bool useRotation;

	// Token: 0x0400006C RID: 108
	public bool useScale;

	// Token: 0x0400006D RID: 109
	public bool useAlpha;

	// Token: 0x0400006E RID: 110
	public bool useColor;

	// Token: 0x0400006F RID: 111
	public UnityEvent onStart;

	// Token: 0x04000070 RID: 112
	public UnityEvent onComplete;

	// Token: 0x04000071 RID: 113
	[SerializeField]
	private int characterNumber;

	// Token: 0x04000072 RID: 114
	private float animationTime;

	// Token: 0x04000073 RID: 115
	private Coroutine playCoroutine;

	// Token: 0x04000074 RID: 116
	private bool m_isPlaying;

	// Token: 0x04000075 RID: 117
	public Vector3 positionFrom = Vector3.zero;

	// Token: 0x04000076 RID: 118
	public Vector3 positionTo = Vector3.zero;

	// Token: 0x04000077 RID: 119
	public AnimationCurve positionAnimationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);

	// Token: 0x04000078 RID: 120
	public float positionSeparation = 0.5f;

	// Token: 0x04000079 RID: 121
	public float rotationFrom;

	// Token: 0x0400007A RID: 122
	public float rotationTo;

	// Token: 0x0400007B RID: 123
	public Vector2 rotationPivot = new Vector2(0.5f, 0.5f);

	// Token: 0x0400007C RID: 124
	public AnimationCurve rotationAnimationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);

	// Token: 0x0400007D RID: 125
	public float rotationSeparation = 0.5f;

	// Token: 0x0400007E RID: 126
	public bool scaleSyncXY = true;

	// Token: 0x0400007F RID: 127
	public float scaleFrom;

	// Token: 0x04000080 RID: 128
	public float scaleTo = 1f;

	// Token: 0x04000081 RID: 129
	public Vector2 scalePivot = new Vector2(0.5f, 0.5f);

	// Token: 0x04000082 RID: 130
	public AnimationCurve scaleAnimationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);

	// Token: 0x04000083 RID: 131
	public float scaleFromY;

	// Token: 0x04000084 RID: 132
	public float scaleToY = 1f;

	// Token: 0x04000085 RID: 133
	public Vector2 scalePivotY = new Vector2(0.5f, 0.5f);

	// Token: 0x04000086 RID: 134
	public AnimationCurve scaleAnimationCurveY = AnimationCurve.Linear(0f, 0f, 1f, 1f);

	// Token: 0x04000087 RID: 135
	public float scaleSeparation = 0.5f;

	// Token: 0x04000088 RID: 136
	public float alphaFrom;

	// Token: 0x04000089 RID: 137
	public float alphaTo = 1f;

	// Token: 0x0400008A RID: 138
	public AnimationCurve alphaAnimationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);

	// Token: 0x0400008B RID: 139
	public float alphaSeparation = 0.5f;

	// Token: 0x0400008C RID: 140
	public Color colorFrom = Color.white;

	// Token: 0x0400008D RID: 141
	public Color colorTo = Color.white;

	// Token: 0x0400008E RID: 142
	public AnimationCurve colorAnimationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);

	// Token: 0x0400008F RID: 143
	public float colorSeparation = 0.5f;

	// Token: 0x020000FF RID: 255
	public enum TimeMode
	{
		// Token: 0x040003D2 RID: 978
		Time,
		// Token: 0x040003D3 RID: 979
		Speed
	}

	// Token: 0x02000100 RID: 256
	public enum Style
	{
		// Token: 0x040003D5 RID: 981
		Once,
		// Token: 0x040003D6 RID: 982
		Loop,
		// Token: 0x040003D7 RID: 983
		PingPong
	}

	// Token: 0x02000101 RID: 257
	[CompilerGenerated]
	private sealed class <PlayOnceCoroutine>d__56 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060008D4 RID: 2260 RVA: 0x0002D3AB File Offset: 0x0002B5AB
		[DebuggerHidden]
		public <PlayOnceCoroutine>d__56(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060008D5 RID: 2261 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060008D6 RID: 2262 RVA: 0x0002D3BC File Offset: 0x0002B5BC
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			TypefaceAnimator typefaceAnimator = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				if (typefaceAnimator.delay > 0f)
				{
					this.<>2__current = new WaitForSeconds(typefaceAnimator.delay);
					this.<>1__state = 1;
					return true;
				}
				break;
			case 1:
				this.<>1__state = -1;
				break;
			case 2:
				this.<>1__state = -1;
				goto IL_AD;
			default:
				return false;
			}
			if (typefaceAnimator.m_isPlaying)
			{
				return false;
			}
			typefaceAnimator.m_isPlaying = true;
			if (typefaceAnimator.onStart != null)
			{
				typefaceAnimator.onStart.Invoke();
			}
			IL_AD:
			if (typefaceAnimator.progress >= 1f)
			{
				typefaceAnimator.m_isPlaying = false;
				typefaceAnimator.progress = 1f;
				if (typefaceAnimator.onComplete != null)
				{
					typefaceAnimator.onComplete.Invoke();
				}
				return false;
			}
			typefaceAnimator.progress += Time.deltaTime / typefaceAnimator.animationTime;
			this.<>2__current = null;
			this.<>1__state = 2;
			return true;
		}

		// Token: 0x17000116 RID: 278
		// (get) Token: 0x060008D7 RID: 2263 RVA: 0x0002D4A9 File Offset: 0x0002B6A9
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060008D8 RID: 2264 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000117 RID: 279
		// (get) Token: 0x060008D9 RID: 2265 RVA: 0x0002D4A9 File Offset: 0x0002B6A9
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040003D8 RID: 984
		private int <>1__state;

		// Token: 0x040003D9 RID: 985
		private object <>2__current;

		// Token: 0x040003DA RID: 986
		public TypefaceAnimator <>4__this;
	}

	// Token: 0x02000102 RID: 258
	[CompilerGenerated]
	private sealed class <PlayLoopCoroutine>d__57 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060008DA RID: 2266 RVA: 0x0002D4B1 File Offset: 0x0002B6B1
		[DebuggerHidden]
		public <PlayLoopCoroutine>d__57(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060008DB RID: 2267 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060008DC RID: 2268 RVA: 0x0002D4C0 File Offset: 0x0002B6C0
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			TypefaceAnimator typefaceAnimator = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				if (typefaceAnimator.delay > 0f)
				{
					this.<>2__current = new WaitForSeconds(typefaceAnimator.delay);
					this.<>1__state = 1;
					return true;
				}
				break;
			case 1:
				this.<>1__state = -1;
				break;
			case 2:
				this.<>1__state = -1;
				goto IL_7B;
			default:
				return false;
			}
			if (typefaceAnimator.m_isPlaying)
			{
				return false;
			}
			typefaceAnimator.m_isPlaying = true;
			if (typefaceAnimator.onStart != null)
			{
				typefaceAnimator.onStart.Invoke();
			}
			IL_7B:
			typefaceAnimator.progress += Time.deltaTime / typefaceAnimator.animationTime;
			if (typefaceAnimator.progress > 1f)
			{
				typefaceAnimator.progress -= 1f;
			}
			this.<>2__current = null;
			this.<>1__state = 2;
			return true;
		}

		// Token: 0x17000118 RID: 280
		// (get) Token: 0x060008DD RID: 2269 RVA: 0x0002D598 File Offset: 0x0002B798
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060008DE RID: 2270 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000119 RID: 281
		// (get) Token: 0x060008DF RID: 2271 RVA: 0x0002D598 File Offset: 0x0002B798
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040003DB RID: 987
		private int <>1__state;

		// Token: 0x040003DC RID: 988
		private object <>2__current;

		// Token: 0x040003DD RID: 989
		public TypefaceAnimator <>4__this;
	}

	// Token: 0x02000103 RID: 259
	[CompilerGenerated]
	private sealed class <PlayPingPongCoroutine>d__58 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060008E0 RID: 2272 RVA: 0x0002D5A0 File Offset: 0x0002B7A0
		[DebuggerHidden]
		public <PlayPingPongCoroutine>d__58(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060008E1 RID: 2273 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060008E2 RID: 2274 RVA: 0x0002D5B0 File Offset: 0x0002B7B0
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			TypefaceAnimator typefaceAnimator = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				if (typefaceAnimator.delay > 0f)
				{
					this.<>2__current = new WaitForSeconds(typefaceAnimator.delay);
					this.<>1__state = 1;
					return true;
				}
				break;
			case 1:
				this.<>1__state = -1;
				break;
			case 2:
				this.<>1__state = -1;
				goto IL_82;
			default:
				return false;
			}
			if (typefaceAnimator.m_isPlaying)
			{
				return false;
			}
			typefaceAnimator.m_isPlaying = true;
			if (typefaceAnimator.onStart != null)
			{
				typefaceAnimator.onStart.Invoke();
			}
			isPositive = true;
			IL_82:
			float num2 = Time.deltaTime / typefaceAnimator.animationTime;
			if (isPositive)
			{
				typefaceAnimator.progress += num2;
				if (typefaceAnimator.progress > 1f)
				{
					isPositive = false;
					typefaceAnimator.progress -= num2;
				}
			}
			else
			{
				typefaceAnimator.progress -= num2;
				if (typefaceAnimator.progress < 0f)
				{
					isPositive = true;
					typefaceAnimator.progress += num2;
				}
			}
			this.<>2__current = null;
			this.<>1__state = 2;
			return true;
		}

		// Token: 0x1700011A RID: 282
		// (get) Token: 0x060008E3 RID: 2275 RVA: 0x0002D6D1 File Offset: 0x0002B8D1
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060008E4 RID: 2276 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x1700011B RID: 283
		// (get) Token: 0x060008E5 RID: 2277 RVA: 0x0002D6D1 File Offset: 0x0002B8D1
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040003DE RID: 990
		private int <>1__state;

		// Token: 0x040003DF RID: 991
		private object <>2__current;

		// Token: 0x040003E0 RID: 992
		public TypefaceAnimator <>4__this;

		// Token: 0x040003E1 RID: 993
		private bool <isPositive>5__2;
	}
}
