﻿using System;
using System.Collections;
using System.Text.RegularExpressions;
using UnityEngine;

// Token: 0x0200003E RID: 62
public class AdfurikunMovieRewardUtility : MonoBehaviour
{
	// Token: 0x17000022 RID: 34
	// (get) Token: 0x06000290 RID: 656 RVA: 0x00013E8E File Offset: 0x0001208E
	public static AdfurikunMovieRewardUtility instance
	{
		get
		{
			return AdfurikunMovieRewardUtility.mInstance;
		}
	}

	// Token: 0x06000291 RID: 657 RVA: 0x00013E95 File Offset: 0x00012095
	public void Awake()
	{
		if (AdfurikunMovieRewardUtility.mInstance == null)
		{
			AdfurikunMovieRewardUtility.mInstance = this;
			return;
		}
		Object.Destroy(base.gameObject);
	}

	// Token: 0x06000292 RID: 658 RVA: 0x00013EB6 File Offset: 0x000120B6
	public void OnDestroy()
	{
		if (Application.isEditor)
		{
			return;
		}
		AdfurikunMovieRewardUtility.mInstance == this;
	}

	// Token: 0x06000293 RID: 659 RVA: 0x00013ECC File Offset: 0x000120CC
	public void OnApplicationPause(bool pause)
	{
		if (Application.isEditor)
		{
			return;
		}
		if (Application.platform == RuntimePlatform.Android)
		{
			if (pause)
			{
				this.callAndroidMovieRewardMethod("onPause");
				return;
			}
			this.callAndroidMovieRewardMethod("onResume");
		}
	}

	// Token: 0x06000294 RID: 660 RVA: 0x00013EF9 File Offset: 0x000120F9
	public void Start()
	{
		if (Application.isEditor)
		{
			return;
		}
		this.initializeMovieReward();
	}

	// Token: 0x06000295 RID: 661 RVA: 0x00013F09 File Offset: 0x00012109
	public void initializeMovieReward()
	{
		this.initializeMovieReward(this.getAppID());
	}

	// Token: 0x06000296 RID: 662 RVA: 0x00013F18 File Offset: 0x00012118
	public void initializeMovieReward(string appId)
	{
		if (!this.isValidAppID(appId))
		{
			return;
		}
		if (Application.platform == RuntimePlatform.Android)
		{
			AndroidJavaObject @static = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
			if (this.mAdfurikunUnityListener == null)
			{
				this.mAdfurikunUnityListener = new AdfurikunMovieRewardUtility.AdfurikunUnityListener();
			}
			new AndroidJavaClass("jp.tjkapp.adfurikunsdk.moviereward.unityplugin.AdfurikunUnityManager").CallStatic("initialize", new object[]
			{
				@static,
				this.unityPluginVersion
			});
			this.makeInstance_AdfurikunMovieRewardController().CallStatic("initialize", new object[]
			{
				@static,
				appId,
				this.mAdfurikunUnityListener
			});
		}
	}

	// Token: 0x06000297 RID: 663 RVA: 0x00013FAE File Offset: 0x000121AE
	public bool isPreparedMovieReward()
	{
		return this.isPreparedMovieReward(this.getAppID());
	}

	// Token: 0x06000298 RID: 664 RVA: 0x00013FBC File Offset: 0x000121BC
	public bool isPreparedMovieReward(string appId)
	{
		return this.isValidAppID(appId) && Application.platform == RuntimePlatform.Android && this.makeInstance_AdfurikunMovieRewardController().CallStatic<bool>("isPrepared", new object[]
		{
			appId
		});
	}

	// Token: 0x06000299 RID: 665 RVA: 0x00013FEE File Offset: 0x000121EE
	[Obsolete("Use Action delegate instead.")]
	public void setMovieRewardSrcObject(GameObject movieRewardSrcObject)
	{
		this.setMovieRewardSrcObject(movieRewardSrcObject, this.getAppID());
	}

	// Token: 0x0600029A RID: 666 RVA: 0x00013FFD File Offset: 0x000121FD
	public void setMovieRewardSrcObject(GameObject movieRewardSrcObject, string appId)
	{
		if (!this.isValidAppID(appId))
		{
			return;
		}
		this.mMovieRewardSrcObject = movieRewardSrcObject;
		if (this.isPreparedMovieReward(appId))
		{
			this.onPrepareSuccess.NullSafe(appId);
			this.sendMessage(AdfurikunMovieRewardUtility.ADF_MovieStatus.PrepareSuccess, appId, "");
		}
	}

	// Token: 0x0600029B RID: 667 RVA: 0x00014032 File Offset: 0x00012232
	public void playMovieReward()
	{
		this.playMovieReward(this.getAppID());
	}

	// Token: 0x0600029C RID: 668 RVA: 0x00014040 File Offset: 0x00012240
	public void playMovieReward(string appId)
	{
		if (!this.isValidAppID(appId))
		{
			return;
		}
		if (Application.platform == RuntimePlatform.Android)
		{
			if (!this.isPreparedMovieReward(appId))
			{
				this.onNotPrepared.NullSafe(appId);
				this.sendMessage(AdfurikunMovieRewardUtility.ADF_MovieStatus.NotPrepared, appId, "");
				return;
			}
			this.makeInstance_AdfurikunMovieRewardController().CallStatic("play", new object[]
			{
				appId
			});
		}
	}

	// Token: 0x0600029D RID: 669 RVA: 0x000140A0 File Offset: 0x000122A0
	public void MovieRewardCallback(string param_str)
	{
		string[] array = param_str.Split(new char[]
		{
			';'
		});
		string a = array[0].Split(new char[]
		{
			':'
		})[1];
		string text = array[1].Split(new char[]
		{
			':'
		})[1];
		string text2 = "";
		if (array.Length > 2)
		{
			text2 = array[2].Split(new char[]
			{
				':'
			})[1];
		}
		AdfurikunMovieRewardUtility.ADF_MovieStatus status;
		if (!(a == "PrepareSuccess"))
		{
			if (!(a == "StartPlaying"))
			{
				if (!(a == "FinishedPlaying"))
				{
					if (!(a == "FailedPlaying"))
					{
						if (!(a == "AdClose"))
						{
							return;
						}
						status = AdfurikunMovieRewardUtility.ADF_MovieStatus.AdClose;
						this.onCloseAd.NullSafe(text, text2);
					}
					else
					{
						status = AdfurikunMovieRewardUtility.ADF_MovieStatus.FailedPlaying;
						this.onFailedPlaying.NullSafe(text, text2);
					}
				}
				else
				{
					status = AdfurikunMovieRewardUtility.ADF_MovieStatus.FinishedPlaying;
					this.onFinishPlaying.NullSafe(text, text2);
				}
			}
			else
			{
				status = AdfurikunMovieRewardUtility.ADF_MovieStatus.StartPlaying;
				this.onStartPlaying.NullSafe(text, text2);
			}
		}
		else
		{
			status = AdfurikunMovieRewardUtility.ADF_MovieStatus.PrepareSuccess;
			this.onPrepareSuccess.NullSafe(text);
		}
		this.sendMessage(status, text, text2);
	}

	// Token: 0x0600029E RID: 670 RVA: 0x000141B0 File Offset: 0x000123B0
	public void sendMessage(AdfurikunMovieRewardUtility.ADF_MovieStatus status, string appId, string adnetworkKey)
	{
		if (this.mMovieRewardSrcObject != null)
		{
			ArrayList arrayList = new ArrayList();
			arrayList.Add((int)status);
			arrayList.Add(appId);
			arrayList.Add(adnetworkKey);
			this.mMovieRewardSrcObject.SendMessage("MovieRewardCallback", arrayList);
		}
	}

	// Token: 0x0600029F RID: 671 RVA: 0x000141FF File Offset: 0x000123FF
	public void dispose()
	{
		this.disposeResource();
	}

	// Token: 0x060002A0 RID: 672 RVA: 0x00014207 File Offset: 0x00012407
	public void disposeResource()
	{
		if (Application.platform == RuntimePlatform.Android)
		{
			this.callAndroidMovieRewardMethod("onDestroy");
		}
	}

	// Token: 0x060002A1 RID: 673 RVA: 0x00014220 File Offset: 0x00012420
	private string getAppID()
	{
		string result = "";
		if (Application.platform == RuntimePlatform.Android)
		{
			result = this.config.androidAppID;
		}
		return result;
	}

	// Token: 0x060002A2 RID: 674 RVA: 0x00014249 File Offset: 0x00012449
	private bool isValidAppID(string appId)
	{
		return Regex.IsMatch(appId, "^[a-f0-9]{24}$");
	}

	// Token: 0x060002A3 RID: 675 RVA: 0x00014256 File Offset: 0x00012456
	private AndroidJavaClass makeInstance_AdfurikunMovieRewardController()
	{
		return new AndroidJavaClass("jp.tjkapp.adfurikunsdk.moviereward.unityplugin.AdfurikunMovieRewardController");
	}

	// Token: 0x060002A4 RID: 676 RVA: 0x00014262 File Offset: 0x00012462
	private void callAndroidMovieRewardMethod(string methodName)
	{
		this.makeInstance_AdfurikunMovieRewardController().CallStatic(methodName, Array.Empty<object>());
	}

	// Token: 0x060002A5 RID: 677 RVA: 0x00014275 File Offset: 0x00012475
	public AdfurikunMovieRewardUtility()
	{
	}

	// Token: 0x060002A6 RID: 678 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
	// Note: this type is marked as 'beforefieldinit'.
	static AdfurikunMovieRewardUtility()
	{
	}

	// Token: 0x040000E1 RID: 225
	public Action<string> onNotPrepared;

	// Token: 0x040000E2 RID: 226
	public Action<string> onPrepareSuccess;

	// Token: 0x040000E3 RID: 227
	public Action<string, string> onStartPlaying;

	// Token: 0x040000E4 RID: 228
	public Action<string, string> onFinishPlaying;

	// Token: 0x040000E5 RID: 229
	public Action<string, string> onFailedPlaying;

	// Token: 0x040000E6 RID: 230
	public Action<string, string> onCloseAd;

	// Token: 0x040000E7 RID: 231
	public AdfurikunMovieRewardAdConfig config;

	// Token: 0x040000E8 RID: 232
	private static AdfurikunMovieRewardUtility mInstance;

	// Token: 0x040000E9 RID: 233
	private GameObject mMovieRewardSrcObject;

	// Token: 0x040000EA RID: 234
	private AdfurikunMovieRewardUtility.AdfurikunUnityListener mAdfurikunUnityListener;

	// Token: 0x040000EB RID: 235
	private string unityPluginVersion = "2.20.0";

	// Token: 0x02000135 RID: 309
	public enum ADF_MovieStatus
	{
		// Token: 0x040004CD RID: 1229
		NotPrepared,
		// Token: 0x040004CE RID: 1230
		PrepareSuccess,
		// Token: 0x040004CF RID: 1231
		StartPlaying,
		// Token: 0x040004D0 RID: 1232
		FinishedPlaying,
		// Token: 0x040004D1 RID: 1233
		FailedPlaying,
		// Token: 0x040004D2 RID: 1234
		AdClose
	}

	// Token: 0x02000136 RID: 310
	public class AdfurikunUnityListener : AndroidJavaProxy
	{
		// Token: 0x060009B2 RID: 2482 RVA: 0x0002F393 File Offset: 0x0002D593
		public AdfurikunUnityListener() : base("jp.tjkapp.adfurikunsdk.moviereward.unityplugin.UnityMovieListener")
		{
		}

		// Token: 0x060009B3 RID: 2483 RVA: 0x0002F443 File Offset: 0x0002D643
		public void onPrepareSuccess(string appId)
		{
			AdfurikunMovieRewardUtility.mInstance.onPrepareSuccess.NullSafe(appId);
			AdfurikunMovieRewardUtility.mInstance.sendMessage(AdfurikunMovieRewardUtility.ADF_MovieStatus.PrepareSuccess, appId, "");
		}

		// Token: 0x060009B4 RID: 2484 RVA: 0x0002F466 File Offset: 0x0002D666
		public void onStartPlaying(string appId, string adnetworkKey)
		{
			AdfurikunMovieRewardUtility.mInstance.onStartPlaying.NullSafe(appId, adnetworkKey);
			AdfurikunMovieRewardUtility.mInstance.sendMessage(AdfurikunMovieRewardUtility.ADF_MovieStatus.StartPlaying, appId, adnetworkKey);
		}

		// Token: 0x060009B5 RID: 2485 RVA: 0x0002F486 File Offset: 0x0002D686
		public void onFinishedPlaying(string appId, string adnetworkKey)
		{
			AdfurikunMovieRewardUtility.mInstance.onFinishPlaying.NullSafe(appId, adnetworkKey);
			AdfurikunMovieRewardUtility.mInstance.sendMessage(AdfurikunMovieRewardUtility.ADF_MovieStatus.FinishedPlaying, appId, adnetworkKey);
		}

		// Token: 0x060009B6 RID: 2486 RVA: 0x0002F4A6 File Offset: 0x0002D6A6
		public void onFailedPlaying(string appId, string adnetworkKey)
		{
			AdfurikunMovieRewardUtility.mInstance.onFailedPlaying.NullSafe(appId, adnetworkKey);
			AdfurikunMovieRewardUtility.mInstance.sendMessage(AdfurikunMovieRewardUtility.ADF_MovieStatus.FailedPlaying, appId, adnetworkKey);
		}

		// Token: 0x060009B7 RID: 2487 RVA: 0x0002F4C6 File Offset: 0x0002D6C6
		public void onAdClose(string appId, string adnetworkKey)
		{
			AdfurikunMovieRewardUtility.mInstance.onCloseAd.NullSafe(appId, adnetworkKey);
			AdfurikunMovieRewardUtility.mInstance.sendMessage(AdfurikunMovieRewardUtility.ADF_MovieStatus.AdClose, appId, adnetworkKey);
		}
	}
}
