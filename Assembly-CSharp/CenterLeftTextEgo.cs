﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000004 RID: 4
public class CenterLeftTextEgo : CenterLeftText
{
	// Token: 0x0600000D RID: 13 RVA: 0x00002898 File Offset: 0x00000A98
	public override void OnEnable()
	{
		if (!(base.gameObject.name == "TalkText"))
		{
			base.OnEnable();
			return;
		}
		base.GetComponent<Text>().fontSize = 44;
		float width = base.GetComponent<RectTransform>().rect.width;
		float preferredWidth = base.GetComponent<Text>().preferredWidth;
		if (preferredWidth > width * 0.95f)
		{
			base.GetComponent<Text>().fontSize = 40;
		}
		preferredWidth = base.GetComponent<Text>().preferredWidth;
		if (preferredWidth <= width * 0.8f)
		{
			base.GetComponent<RectTransform>().anchoredPosition = new Vector2(this.LeftPadding, base.GetComponent<RectTransform>().anchoredPosition.y);
			return;
		}
		base.GetComponent<RectTransform>().anchoredPosition = new Vector2(this.LeftPadding - 45f + (width - preferredWidth) / 2f, base.GetComponent<RectTransform>().anchoredPosition.y);
	}

	// Token: 0x0600000E RID: 14 RVA: 0x0000297D File Offset: 0x00000B7D
	public CenterLeftTextEgo()
	{
	}

	// Token: 0x04000003 RID: 3
	public float LeftPadding;
}
