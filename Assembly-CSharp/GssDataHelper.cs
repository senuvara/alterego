﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

// Token: 0x0200002F RID: 47
public class GssDataHelper : MonoBehaviour
{
	// Token: 0x060001ED RID: 493 RVA: 0x000115C8 File Offset: 0x0000F7C8
	public void Awake()
	{
		if (GssDataHelper.Instance != null)
		{
			return;
		}
		GssDataHelper.Instance = this;
		GssDataHelper.Init();
	}

	// Token: 0x060001EE RID: 494 RVA: 0x000115E3 File Offset: 0x0000F7E3
	public void OnDestroy()
	{
		GssDataHelper.Instance = null;
	}

	// Token: 0x060001EF RID: 495 RVA: 0x000115EC File Offset: 0x0000F7EC
	public static void Init()
	{
		TextAsset textAsset = Resources.Load<TextAsset>("parameters");
		if (textAsset != null)
		{
			GssDataHelper.GssTextData = textAsset.text;
		}
	}

	// Token: 0x060001F0 RID: 496 RVA: 0x00011618 File Offset: 0x0000F818
	public static IEnumerator GetText(string spreadSheetID, string sheet, Action<string> callback, string option = null)
	{
		string text = "https://script.google.com/macros/s/AKfycbx0OgWjabmLpq7UnE3HxZIRjAxlZs1-GMbJX5c9VWFZV0JDsmM/exec";
		text = text + "?ssID=" + spreadSheetID;
		text = text + "&sheet=" + sheet;
		text = text + "&option=" + option;
		using (UnityWebRequest www = UnityWebRequest.Get(text))
		{
			www.SendWebRequest();
			while (!www.isDone)
			{
				yield return null;
			}
			callback(www.downloadHandler.text);
		}
		UnityWebRequest www = null;
		yield break;
		yield break;
	}

	// Token: 0x060001F1 RID: 497 RVA: 0x0001163C File Offset: 0x0000F83C
	public static void PostData(string spreadSheetID, string sheet, string data, string cell = "", string option = "")
	{
		if (GssDataHelper.Instance != null)
		{
			GssDataHelper.Instance.StartCoroutine(GssDataHelper.PostDataCoroutine(spreadSheetID, sheet, data, cell, option, null));
			return;
		}
		GameObject caller = new GameObject("GssDataHelperObject");
		caller.AddComponent<GssDataHelper>().StartCoroutine(GssDataHelper.PostDataCoroutine(spreadSheetID, sheet, data, cell, option, delegate
		{
			Object.Destroy(caller);
		}));
	}

	// Token: 0x060001F2 RID: 498 RVA: 0x000116AC File Offset: 0x0000F8AC
	public static IEnumerator PostDataCoroutine(string spreadSheetID, string sheet, string data, string cell = "", string option = "", Action callback = null)
	{
		UnityWebRequest unityWebRequest = GssDataHelper.MakeRequestPostData(spreadSheetID, sheet, data, cell, option);
		UnityWebRequestAsyncOperation async = unityWebRequest.SendWebRequest();
		while (!async.isDone)
		{
			yield return null;
		}
		if (callback != null)
		{
			callback();
		}
		yield break;
	}

	// Token: 0x060001F3 RID: 499 RVA: 0x000116E0 File Offset: 0x0000F8E0
	private static UnityWebRequest MakeRequestPostData(string spreadSheetID, string sheet, string data, string cell = "", string option = "")
	{
		UnityWebRequest unityWebRequest = new UnityWebRequest("https://script.google.com/macros/s/AKfycbx0OgWjabmLpq7UnE3HxZIRjAxlZs1-GMbJX5c9VWFZV0JDsmM/exec", "POST");
		GssDataHelper.TableData obj = new GssDataHelper.TableData(spreadSheetID, sheet, data, cell, option);
		byte[] bytes = Encoding.UTF8.GetBytes(JsonUtility.ToJson(obj));
		unityWebRequest.uploadHandler = new UploadHandlerRaw(bytes);
		unityWebRequest.SetRequestHeader("Content-Type", "application/json");
		return unityWebRequest;
	}

	// Token: 0x060001F4 RID: 500 RVA: 0x00011738 File Offset: 0x0000F938
	public static string[][] GetSheet(string sheet)
	{
		if (GssDataHelper.GssTextData == null)
		{
			GssDataHelper.Init();
		}
		return (from x in GssDataHelper.GssTextData.Split(new char[]
		{
			'\n'
		})
		where x.StartsWith(sheet + "\t")
		select x.Replace(sheet + "\t", "").Replace("<br>", "\n").Split(new char[]
		{
			'\t'
		})).ToArray<string[]>();
	}

	// Token: 0x060001F5 RID: 501 RVA: 0x0001179C File Offset: 0x0000F99C
	public static string[] GetData(string sheet, string key)
	{
		if (GssDataHelper.GssTextData == null)
		{
			GssDataHelper.Init();
		}
		IEnumerable<string[]> source = from x in GssDataHelper.GssTextData.Split(new char[]
		{
			'\n'
		})
		where x.StartsWith(sheet + "\t" + key + "\t")
		select x.Replace(sheet + "\t", "").Replace("<br>", "\n").Split(new char[]
		{
			'\t'
		});
		if (source.ToArray<string[]>().Length != 1)
		{
			global::Debug.LogError("Error! - GssDataHelper#GetDataByKey sheet=" + sheet + " key= " + key);
			return null;
		}
		return source.ToArray<string[]>()[0];
	}

	// Token: 0x060001F6 RID: 502 RVA: 0x00011836 File Offset: 0x0000FA36
	public static IEnumerator Overwrite()
	{
		yield break;
	}

	// Token: 0x060001F7 RID: 503 RVA: 0x000025AD File Offset: 0x000007AD
	public GssDataHelper()
	{
	}

	// Token: 0x040000A7 RID: 167
	private static GssDataHelper Instance;

	// Token: 0x040000A8 RID: 168
	private static string GssTextData;

	// Token: 0x02000122 RID: 290
	public class TableData
	{
		// Token: 0x06000972 RID: 2418 RVA: 0x0002EC28 File Offset: 0x0002CE28
		public TableData(string ssID, string sheet, string data, string cell, string option = "")
		{
			this.ssID = ssID;
			this.sheet = sheet;
			this.data = data;
			this.cell = cell;
			this.option = option;
		}

		// Token: 0x0400047C RID: 1148
		public string ssID;

		// Token: 0x0400047D RID: 1149
		public string sheet;

		// Token: 0x0400047E RID: 1150
		public string data;

		// Token: 0x0400047F RID: 1151
		public string cell;

		// Token: 0x04000480 RID: 1152
		public string option;
	}

	// Token: 0x02000123 RID: 291
	[CompilerGenerated]
	private sealed class <GetText>d__5 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000973 RID: 2419 RVA: 0x0002EC55 File Offset: 0x0002CE55
		[DebuggerHidden]
		public <GetText>d__5(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000974 RID: 2420 RVA: 0x0002EC64 File Offset: 0x0002CE64
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
			int num = this.<>1__state;
			if (num == -3 || num == 1)
			{
				try
				{
				}
				finally
				{
					this.<>m__Finally1();
				}
			}
		}

		// Token: 0x06000975 RID: 2421 RVA: 0x0002EC9C File Offset: 0x0002CE9C
		bool IEnumerator.MoveNext()
		{
			bool result;
			try
			{
				int num = this.<>1__state;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -3;
				}
				else
				{
					this.<>1__state = -1;
					string text = "https://script.google.com/macros/s/AKfycbx0OgWjabmLpq7UnE3HxZIRjAxlZs1-GMbJX5c9VWFZV0JDsmM/exec";
					text = text + "?ssID=" + spreadSheetID;
					text = text + "&sheet=" + sheet;
					text = text + "&option=" + option;
					www = UnityWebRequest.Get(text);
					this.<>1__state = -3;
					www.SendWebRequest();
				}
				if (www.isDone)
				{
					callback(www.downloadHandler.text);
					this.<>m__Finally1();
					www = null;
					result = false;
				}
				else
				{
					this.<>2__current = null;
					this.<>1__state = 1;
					result = true;
				}
			}
			catch
			{
				this.System.IDisposable.Dispose();
				throw;
			}
			return result;
		}

		// Token: 0x06000976 RID: 2422 RVA: 0x0002ED90 File Offset: 0x0002CF90
		private void <>m__Finally1()
		{
			this.<>1__state = -1;
			if (www != null)
			{
				((IDisposable)www).Dispose();
			}
		}

		// Token: 0x17000142 RID: 322
		// (get) Token: 0x06000977 RID: 2423 RVA: 0x0002EDAC File Offset: 0x0002CFAC
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000978 RID: 2424 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000143 RID: 323
		// (get) Token: 0x06000979 RID: 2425 RVA: 0x0002EDAC File Offset: 0x0002CFAC
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x04000481 RID: 1153
		private int <>1__state;

		// Token: 0x04000482 RID: 1154
		private object <>2__current;

		// Token: 0x04000483 RID: 1155
		public string spreadSheetID;

		// Token: 0x04000484 RID: 1156
		public string sheet;

		// Token: 0x04000485 RID: 1157
		public string option;

		// Token: 0x04000486 RID: 1158
		public Action<string> callback;

		// Token: 0x04000487 RID: 1159
		private UnityWebRequest <www>5__2;
	}

	// Token: 0x02000124 RID: 292
	[CompilerGenerated]
	private sealed class <>c__DisplayClass6_0
	{
		// Token: 0x0600097A RID: 2426 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass6_0()
		{
		}

		// Token: 0x0600097B RID: 2427 RVA: 0x0002EDB4 File Offset: 0x0002CFB4
		internal void <PostData>b__0()
		{
			Object.Destroy(this.caller);
		}

		// Token: 0x04000488 RID: 1160
		public GameObject caller;
	}

	// Token: 0x02000125 RID: 293
	[CompilerGenerated]
	private sealed class <PostDataCoroutine>d__7 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x0600097C RID: 2428 RVA: 0x0002EDC1 File Offset: 0x0002CFC1
		[DebuggerHidden]
		public <PostDataCoroutine>d__7(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x0600097D RID: 2429 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x0600097E RID: 2430 RVA: 0x0002EDD0 File Offset: 0x0002CFD0
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			if (num != 0)
			{
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
			}
			else
			{
				this.<>1__state = -1;
				UnityWebRequest unityWebRequest = GssDataHelper.MakeRequestPostData(spreadSheetID, sheet, data, cell, option);
				async = unityWebRequest.SendWebRequest();
			}
			if (async.isDone)
			{
				if (callback != null)
				{
					callback();
				}
				return false;
			}
			this.<>2__current = null;
			this.<>1__state = 1;
			return true;
		}

		// Token: 0x17000144 RID: 324
		// (get) Token: 0x0600097F RID: 2431 RVA: 0x0002EE5E File Offset: 0x0002D05E
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000980 RID: 2432 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000145 RID: 325
		// (get) Token: 0x06000981 RID: 2433 RVA: 0x0002EE5E File Offset: 0x0002D05E
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x04000489 RID: 1161
		private int <>1__state;

		// Token: 0x0400048A RID: 1162
		private object <>2__current;

		// Token: 0x0400048B RID: 1163
		public string spreadSheetID;

		// Token: 0x0400048C RID: 1164
		public string sheet;

		// Token: 0x0400048D RID: 1165
		public string data;

		// Token: 0x0400048E RID: 1166
		public string cell;

		// Token: 0x0400048F RID: 1167
		public string option;

		// Token: 0x04000490 RID: 1168
		public Action callback;

		// Token: 0x04000491 RID: 1169
		private UnityWebRequestAsyncOperation <async>5__2;
	}

	// Token: 0x02000126 RID: 294
	[CompilerGenerated]
	private sealed class <>c__DisplayClass10_0
	{
		// Token: 0x06000982 RID: 2434 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass10_0()
		{
		}

		// Token: 0x06000983 RID: 2435 RVA: 0x0002EE66 File Offset: 0x0002D066
		internal bool <GetSheet>b__0(string x)
		{
			return x.StartsWith(this.sheet + "\t");
		}

		// Token: 0x06000984 RID: 2436 RVA: 0x0002EE7E File Offset: 0x0002D07E
		internal string[] <GetSheet>b__1(string x)
		{
			return x.Replace(this.sheet + "\t", "").Replace("<br>", "\n").Split(new char[]
			{
				'\t'
			});
		}

		// Token: 0x04000492 RID: 1170
		public string sheet;
	}

	// Token: 0x02000127 RID: 295
	[CompilerGenerated]
	private sealed class <>c__DisplayClass11_0
	{
		// Token: 0x06000985 RID: 2437 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass11_0()
		{
		}

		// Token: 0x06000986 RID: 2438 RVA: 0x0002EEBA File Offset: 0x0002D0BA
		internal bool <GetData>b__0(string x)
		{
			return x.StartsWith(this.sheet + "\t" + this.key + "\t");
		}

		// Token: 0x06000987 RID: 2439 RVA: 0x0002EEDD File Offset: 0x0002D0DD
		internal string[] <GetData>b__1(string x)
		{
			return x.Replace(this.sheet + "\t", "").Replace("<br>", "\n").Split(new char[]
			{
				'\t'
			});
		}

		// Token: 0x04000493 RID: 1171
		public string sheet;

		// Token: 0x04000494 RID: 1172
		public string key;
	}

	// Token: 0x02000128 RID: 296
	[CompilerGenerated]
	private sealed class <Overwrite>d__12 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000988 RID: 2440 RVA: 0x0002EF19 File Offset: 0x0002D119
		[DebuggerHidden]
		public <Overwrite>d__12(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000989 RID: 2441 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x0600098A RID: 2442 RVA: 0x0002EF28 File Offset: 0x0002D128
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			if (num != 0)
			{
				return false;
			}
			this.<>1__state = -1;
			return false;
		}

		// Token: 0x17000146 RID: 326
		// (get) Token: 0x0600098B RID: 2443 RVA: 0x0002EF49 File Offset: 0x0002D149
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0600098C RID: 2444 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000147 RID: 327
		// (get) Token: 0x0600098D RID: 2445 RVA: 0x0002EF49 File Offset: 0x0002D149
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x04000495 RID: 1173
		private int <>1__state;

		// Token: 0x04000496 RID: 1174
		private object <>2__current;
	}
}
