﻿using System;

// Token: 0x02000038 RID: 56
public static class ActionExtensions
{
	// Token: 0x06000250 RID: 592 RVA: 0x000132F8 File Offset: 0x000114F8
	public static void NullSafe<T>(this Action<T> action, T arg)
	{
		if (action != null)
		{
			action(arg);
		}
	}

	// Token: 0x06000251 RID: 593 RVA: 0x00013304 File Offset: 0x00011504
	public static void NullSafe<T1, T2>(this Action<T1, T2> action, T1 arg1, T2 arg2)
	{
		if (action != null)
		{
			action(arg1, arg2);
		}
	}
}
