﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using App;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000019 RID: 25
public class ResultCanvas : MonoBehaviour
{
	// Token: 0x060000A6 RID: 166 RVA: 0x00008E0A File Offset: 0x0000700A
	private void OnEnable()
	{
		SceneCommon.BackButtonList.Add(this.CloseButton);
	}

	// Token: 0x060000A7 RID: 167 RVA: 0x00008E1C File Offset: 0x0000701C
	private void OnDisable()
	{
		SceneCommon.BackButtonList.Remove(this.CloseButton);
	}

	// Token: 0x060000A8 RID: 168 RVA: 0x00008E30 File Offset: 0x00007030
	public void SetActive(bool active)
	{
		if (active)
		{
			base.gameObject.SetActive(active);
			Sprite sprite = this.ResultFrame[Random.Range(0, this.ResultFrame.Length)];
			base.transform.Find("Result").GetComponent<Image>().sprite = sprite;
			base.StartCoroutine(AppUtil.FadeIn(base.gameObject, 0.5f, null));
			return;
		}
		if (base.gameObject.activeSelf)
		{
			base.StartCoroutine(AppUtil.FadeOut(base.gameObject, 0.5f, delegate(Object ret)
			{
				base.gameObject.SetActive(false);
			}));
		}
	}

	// Token: 0x060000A9 RID: 169 RVA: 0x00008EC8 File Offset: 0x000070C8
	private void OnClick(GameObject clickObject)
	{
		string name = clickObject.name;
		if (name == "CloseButton" || name == "RetryButtonFromResult")
		{
			this.SetActive(false);
			return;
		}
		if (name == "ShowResultButton")
		{
			base.StartCoroutine(this.ShowResult());
			return;
		}
		if (name == "ResultShareButton")
		{
			clickObject.GetComponent<Button>().interactable = false;
			base.StartCoroutine(AppUtil.DelayAction(1f, delegate()
			{
				clickObject.GetComponent<Button>().interactable = true;
			}, true));
			RectTransform captureTarget = (RectTransform)base.transform.Find("Result/ShareArea");
			string text = base.transform.Find("Result/TextGroup/StatusText1").GetComponent<Text>().text;
			string text2 = base.transform.Find("Result/TextGroup/StatusText2").GetComponent<Text>().text;
			string msg = text + "「" + text2 + "」\n自分探しタップゲーム『ALTER EGO』 caracolu.com/app/alterego/ #ALTEREGO";
			base.StartCoroutine(AppUtil.Share(false, msg, null, captureTarget));
			return;
		}
		if (!(name == "Filter") && !(name == "ChangeButton"))
		{
			return;
		}
		Transform transform = base.transform.Find("Result/Filter");
		float alpha = transform.GetComponent<CanvasGroup>().alpha;
		AppUtil.SetAlpha(transform, (float)((alpha > 0f) ? 0 : 1));
	}

	// Token: 0x060000AA RID: 170 RVA: 0x00009028 File Offset: 0x00007228
	public void RequestResult(string scenarioType, string type, List<string> displayWordList)
	{
		this.SetActive(true);
		this.ScenarioType = scenarioType;
		base.transform.Find("ButtonGroup").gameObject.SetActive(false);
		base.transform.Find("Result/Filter/FukidasiGroup").gameObject.SetActive(false);
		base.transform.Find("Result/Filter").GetComponent<CanvasGroup>().blocksRaycasts = false;
		AppUtil.SetAlpha(base.transform.Find("Result/Filter"), 1f);
		base.transform.Find("MirrorFragments").gameObject.SetActive(false);
		AppUtil.SetAlpha(base.transform.Find("Result/TextGroup"), 0f);
		if (AdInfo.ENABLE)
		{
			if (AdManager.IsReady("Reward"))
			{
				base.transform.Find("ButtonGroup/RetryButtonFromResult/").GetComponent<Button>().interactable = true;
				base.transform.Find("ButtonGroup/RetryButtonFromResult/Text").GetComponent<TextLocalization>().SetKey("[UI]RetryButtonFromResult/Text");
			}
			else
			{
				base.transform.Find("ButtonGroup/RetryButtonFromResult/").GetComponent<Button>().interactable = false;
				base.transform.Find("ButtonGroup/RetryButtonFromResult/Text").GetComponent<TextLocalization>().SetKey("[UI]NoAdMovieMessage");
			}
		}
		else
		{
			base.transform.Find("ButtonGroup/RetryButtonFromResult/Text").GetComponent<TextLocalization>().SetKey("[UI]RetryButtonFromResult/TextNoAd");
		}
		if (displayWordList != null)
		{
			AdManager.Show("Native", null);
			AdManager.Hide("Banner");
			base.transform.Find("Analyzing").gameObject.SetActive(true);
			base.transform.Find("Analyzing").GetComponent<ResultAnalyzing>().StartAnalyzing(displayWordList.ToArray());
			AppUtil.DelayAction(this, 8f, base.transform.Find("Analyzing").GetComponent<ResultAnalyzing>().StopAnalyzing(), true);
		}
		else
		{
			base.StartCoroutine(this.ShowResultInLibrary());
		}
		foreach (TextLocalization textLocalization in base.transform.Find("Result/TextGroup").GetComponentsInChildren<TextLocalization>())
		{
			string name = textLocalization.name;
			if (!(name == "結果解説"))
			{
				if (!(name == "Title"))
				{
					if (!(name == "StatusText1"))
					{
						if (name == "StatusText2")
						{
							string text = LanguageManager.Get(Data.GetScenarioKey(scenarioType, "型", type, 0));
							text = LanguageManager.Get("TypePrefix") + text + LanguageManager.Get("TypePostfix");
							textLocalization.SetText(text);
						}
					}
					else
					{
						textLocalization.SetText(LanguageManager.Get(Data.GetScenarioKey(scenarioType, "型コピー", type, 0)));
					}
				}
				else
				{
					string str = LanguageManager.Get("TitlePrefix");
					if (scenarioType == "ゲームクリア")
					{
						str = "";
					}
					textLocalization.SetText(str + LanguageManager.Get(Data.GetScenarioKey(scenarioType, "結果タイトル", null, 0)) + LanguageManager.Get("TitlePostfix"));
				}
			}
			else
			{
				textLocalization.SetKey(Data.GetScenarioKey(scenarioType, "解説", type, 0));
			}
		}
	}

	// Token: 0x060000AB RID: 171 RVA: 0x00009341 File Offset: 0x00007541
	private IEnumerator ShowResultInLibrary()
	{
		CounselingResult.Read(this.ScenarioType);
		base.transform.Find("ButtonGroupLibrary").gameObject.SetActive(true);
		base.transform.Find("ButtonGroupLibrary/ChangeButton").gameObject.SetActive(false);
		base.transform.Find("ButtonGroupLibrary/ResultShareButton").gameObject.SetActive(false);
		for (int j = 1; j <= 4; j++)
		{
			base.transform.Find("Result/Filter/FukidasiGroup/Fukidasi" + j).gameObject.SetActive(false);
		}
		base.transform.Find("Result/Filter/FukidasiGroup").gameObject.SetActive(true);
		base.transform.Find("Result/Filter").GetComponent<Image>().raycastTarget = true;
		base.transform.Find("ButtonGroupLibrary/ChangeButton").GetComponent<Button>().interactable = true;
		int num;
		for (int i = 1; i <= 4; i = num + 1)
		{
			string scenarioType = this.ScenarioType;
			string key = string.Concat(new object[]
			{
				"[Fukidasi]CR",
				scenarioType,
				"_",
				CounselingResult.Get(scenarioType),
				"-",
				i
			});
			if (!LanguageManager.Contains(key))
			{
				base.transform.Find("Result/Filter").GetComponent<Image>().raycastTarget = false;
				base.transform.Find("ButtonGroupLibrary/ChangeButton").GetComponent<Button>().interactable = false;
				break;
			}
			yield return AppUtil.Wait(0.4f);
			base.transform.Find("Result/Filter/FukidasiGroup/Fukidasi" + i).GetComponent<TextLocalization>().SetKey(key);
			base.transform.Find("Result/Filter/FukidasiGroup/Fukidasi" + i).gameObject.SetActive(true);
			key = null;
			num = i;
		}
		yield return AppUtil.Wait(1f);
		base.StartCoroutine(AppUtil.FadeOut(base.transform.Find("Result/Filter"), 0.25f, null));
		base.transform.Find("MirrorFragments").gameObject.SetActive(true);
		AudioManager.PlaySound(new string[]
		{
			"Click",
			"",
			"ShowResultButton"
		});
		yield return AppUtil.Wait(1f);
		yield return AppUtil.FadeIn(base.transform.Find("Result/TextGroup"), 0.5f, null);
		base.transform.Find("ButtonGroupLibrary/ChangeButton").gameObject.SetActive(true);
		base.transform.Find("ButtonGroupLibrary/ResultShareButton").gameObject.SetActive(true);
		base.transform.Find("Result/Filter").GetComponent<CanvasGroup>().blocksRaycasts = true;
		yield break;
	}

	// Token: 0x060000AC RID: 172 RVA: 0x00009350 File Offset: 0x00007550
	private IEnumerator ShowResult()
	{
		base.transform.Find("Analyzing").gameObject.SetActive(false);
		base.StartCoroutine(AppUtil.FadeOut(base.transform.Find("Result/Filter"), 0.25f, null));
		base.transform.Find("MirrorFragments").gameObject.SetActive(true);
		yield return AppUtil.Wait(1f);
		yield return AppUtil.FadeIn(base.transform.Find("Result/TextGroup"), 0.5f, null);
		AdManager.Hide("Native");
		AdManager.Show("Banner", null);
		yield return AppUtil.Wait(0.5f);
		base.transform.Find("ButtonGroup").gameObject.SetActive(true);
		yield break;
	}

	// Token: 0x060000AD RID: 173 RVA: 0x000025AD File Offset: 0x000007AD
	public ResultCanvas()
	{
	}

	// Token: 0x060000AE RID: 174 RVA: 0x0000935F File Offset: 0x0000755F
	[CompilerGenerated]
	private void <SetActive>b__5_0(Object ret)
	{
		base.gameObject.SetActive(false);
	}

	// Token: 0x0400002D RID: 45
	public Sprite[] ResultFrame;

	// Token: 0x0400002E RID: 46
	private string ScenarioType;

	// Token: 0x0400002F RID: 47
	[SerializeField]
	private OnClickHandler CloseButton;

	// Token: 0x020000CC RID: 204
	[CompilerGenerated]
	private sealed class <>c__DisplayClass6_0
	{
		// Token: 0x060007F5 RID: 2037 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass6_0()
		{
		}

		// Token: 0x060007F6 RID: 2038 RVA: 0x0002798C File Offset: 0x00025B8C
		internal void <OnClick>b__0()
		{
			this.clickObject.GetComponent<Button>().interactable = true;
		}

		// Token: 0x040002EE RID: 750
		public GameObject clickObject;
	}

	// Token: 0x020000CD RID: 205
	[CompilerGenerated]
	private sealed class <ShowResultInLibrary>d__8 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060007F7 RID: 2039 RVA: 0x0002799F File Offset: 0x00025B9F
		[DebuggerHidden]
		public <ShowResultInLibrary>d__8(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060007F8 RID: 2040 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060007F9 RID: 2041 RVA: 0x000279B0 File Offset: 0x00025BB0
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			ResultCanvas resultCanvas = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				CounselingResult.Read(resultCanvas.ScenarioType);
				resultCanvas.transform.Find("ButtonGroupLibrary").gameObject.SetActive(true);
				resultCanvas.transform.Find("ButtonGroupLibrary/ChangeButton").gameObject.SetActive(false);
				resultCanvas.transform.Find("ButtonGroupLibrary/ResultShareButton").gameObject.SetActive(false);
				for (int j = 1; j <= 4; j++)
				{
					resultCanvas.transform.Find("Result/Filter/FukidasiGroup/Fukidasi" + j).gameObject.SetActive(false);
				}
				resultCanvas.transform.Find("Result/Filter/FukidasiGroup").gameObject.SetActive(true);
				resultCanvas.transform.Find("Result/Filter").GetComponent<Image>().raycastTarget = true;
				resultCanvas.transform.Find("ButtonGroupLibrary/ChangeButton").GetComponent<Button>().interactable = true;
				i = 1;
				break;
			case 1:
			{
				this.<>1__state = -1;
				resultCanvas.transform.Find("Result/Filter/FukidasiGroup/Fukidasi" + i).GetComponent<TextLocalization>().SetKey(key);
				resultCanvas.transform.Find("Result/Filter/FukidasiGroup/Fukidasi" + i).gameObject.SetActive(true);
				key = null;
				int num2 = i;
				i = num2 + 1;
				break;
			}
			case 2:
				this.<>1__state = -1;
				resultCanvas.StartCoroutine(AppUtil.FadeOut(resultCanvas.transform.Find("Result/Filter"), 0.25f, null));
				resultCanvas.transform.Find("MirrorFragments").gameObject.SetActive(true);
				AudioManager.PlaySound(new string[]
				{
					"Click",
					"",
					"ShowResultButton"
				});
				this.<>2__current = AppUtil.Wait(1f);
				this.<>1__state = 3;
				return true;
			case 3:
				this.<>1__state = -1;
				this.<>2__current = AppUtil.FadeIn(resultCanvas.transform.Find("Result/TextGroup"), 0.5f, null);
				this.<>1__state = 4;
				return true;
			case 4:
				this.<>1__state = -1;
				resultCanvas.transform.Find("ButtonGroupLibrary/ChangeButton").gameObject.SetActive(true);
				resultCanvas.transform.Find("ButtonGroupLibrary/ResultShareButton").gameObject.SetActive(true);
				resultCanvas.transform.Find("Result/Filter").GetComponent<CanvasGroup>().blocksRaycasts = true;
				return false;
			default:
				return false;
			}
			if (i <= 4)
			{
				string scenarioType = resultCanvas.ScenarioType;
				key = string.Concat(new object[]
				{
					"[Fukidasi]CR",
					scenarioType,
					"_",
					CounselingResult.Get(scenarioType),
					"-",
					i
				});
				if (LanguageManager.Contains(key))
				{
					this.<>2__current = AppUtil.Wait(0.4f);
					this.<>1__state = 1;
					return true;
				}
				resultCanvas.transform.Find("Result/Filter").GetComponent<Image>().raycastTarget = false;
				resultCanvas.transform.Find("ButtonGroupLibrary/ChangeButton").GetComponent<Button>().interactable = false;
			}
			this.<>2__current = AppUtil.Wait(1f);
			this.<>1__state = 2;
			return true;
		}

		// Token: 0x170000DE RID: 222
		// (get) Token: 0x060007FA RID: 2042 RVA: 0x00027D2E File Offset: 0x00025F2E
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060007FB RID: 2043 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000DF RID: 223
		// (get) Token: 0x060007FC RID: 2044 RVA: 0x00027D2E File Offset: 0x00025F2E
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040002EF RID: 751
		private int <>1__state;

		// Token: 0x040002F0 RID: 752
		private object <>2__current;

		// Token: 0x040002F1 RID: 753
		public ResultCanvas <>4__this;

		// Token: 0x040002F2 RID: 754
		private int <j>5__2;

		// Token: 0x040002F3 RID: 755
		private string <key>5__3;
	}

	// Token: 0x020000CE RID: 206
	[CompilerGenerated]
	private sealed class <ShowResult>d__9 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060007FD RID: 2045 RVA: 0x00027D36 File Offset: 0x00025F36
		[DebuggerHidden]
		public <ShowResult>d__9(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060007FE RID: 2046 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060007FF RID: 2047 RVA: 0x00027D48 File Offset: 0x00025F48
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			ResultCanvas resultCanvas = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				resultCanvas.transform.Find("Analyzing").gameObject.SetActive(false);
				resultCanvas.StartCoroutine(AppUtil.FadeOut(resultCanvas.transform.Find("Result/Filter"), 0.25f, null));
				resultCanvas.transform.Find("MirrorFragments").gameObject.SetActive(true);
				this.<>2__current = AppUtil.Wait(1f);
				this.<>1__state = 1;
				return true;
			case 1:
				this.<>1__state = -1;
				this.<>2__current = AppUtil.FadeIn(resultCanvas.transform.Find("Result/TextGroup"), 0.5f, null);
				this.<>1__state = 2;
				return true;
			case 2:
				this.<>1__state = -1;
				AdManager.Hide("Native");
				AdManager.Show("Banner", null);
				this.<>2__current = AppUtil.Wait(0.5f);
				this.<>1__state = 3;
				return true;
			case 3:
				this.<>1__state = -1;
				resultCanvas.transform.Find("ButtonGroup").gameObject.SetActive(true);
				return false;
			default:
				return false;
			}
		}

		// Token: 0x170000E0 RID: 224
		// (get) Token: 0x06000800 RID: 2048 RVA: 0x00027E7C File Offset: 0x0002607C
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000801 RID: 2049 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000E1 RID: 225
		// (get) Token: 0x06000802 RID: 2050 RVA: 0x00027E7C File Offset: 0x0002607C
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040002F4 RID: 756
		private int <>1__state;

		// Token: 0x040002F5 RID: 757
		private object <>2__current;

		// Token: 0x040002F6 RID: 758
		public ResultCanvas <>4__this;
	}
}
