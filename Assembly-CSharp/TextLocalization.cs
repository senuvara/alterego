﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000037 RID: 55
public class TextLocalization : MonoBehaviour
{
	// Token: 0x06000245 RID: 581 RVA: 0x00012F80 File Offset: 0x00011180
	private void Awake()
	{
		this.DefaultRotation = base.transform.localRotation;
	}

	// Token: 0x06000246 RID: 582 RVA: 0x00012F93 File Offset: 0x00011193
	private void OnEnable()
	{
		this.UpdateText();
	}

	// Token: 0x06000247 RID: 583 RVA: 0x00012F9B File Offset: 0x0001119B
	public void SetKey(string key)
	{
		this.PriorKey = key;
		this.UpdateText();
	}

	// Token: 0x06000248 RID: 584 RVA: 0x00012FAA File Offset: 0x000111AA
	public void SetText(string input)
	{
		this.PriorText = input;
		this.UpdateText();
	}

	// Token: 0x06000249 RID: 585 RVA: 0x00012FBC File Offset: 0x000111BC
	public string GetKey()
	{
		if (this.PriorKey != "")
		{
			return this.PriorKey;
		}
		return ("[UI]" + base.transform.parent.name + "/" + base.gameObject.name).Replace("(Clone)", "");
	}

	// Token: 0x0600024A RID: 586 RVA: 0x0001301B File Offset: 0x0001121B
	public void SetParameters(params object[] parameters)
	{
		this.Parameters = parameters;
		this.UpdateText();
	}

	// Token: 0x0600024B RID: 587 RVA: 0x0001302C File Offset: 0x0001122C
	private void UpdateText()
	{
		string text;
		if (this.PriorText == "")
		{
			text = LanguageManager.Get(this.GetKey());
		}
		else
		{
			text = this.PriorText;
		}
		if (this.Parameters != null)
		{
			text = string.Format(text, this.Parameters);
		}
		if (this.IsVertical)
		{
			if (LanguageManager.AllowVertical())
			{
				if (base.transform.localRotation == this.DefaultRotation)
				{
					base.transform.Rotate(new Vector3(0f, 0f, 90f));
				}
				char[] array = text.ToCharArray();
				text = "";
				foreach (char c in array)
				{
					text = text + c.ToString() + "\n";
				}
				text = text.Substring(0, text.Length - 1);
			}
			else
			{
				base.transform.localRotation = this.DefaultRotation;
			}
		}
		text = text.Replace("<br>", "\n").Replace("、", "､").Replace("「", "｢").Replace("」", "｣");
		Type[] typeList = TextLocalization.TypeList;
		int i = 0;
		while (i < typeList.Length)
		{
			Type type = typeList[i];
			object component = base.GetComponent(type);
			if (component != null)
			{
				if (component is TextMeshProUGUI)
				{
					((TextMeshProUGUI)component).text = text;
					break;
				}
				type.GetProperty("text").SetValue(component, text, null);
				break;
			}
			else
			{
				i++;
			}
		}
		Font font = LanguageManager.GetFont(this.GetKey());
		this.SetFont(font);
		if (base.GetComponent<CenterLeftText>() != null)
		{
			base.GetComponent<CenterLeftText>().OnEnable();
		}
	}

	// Token: 0x0600024C RID: 588 RVA: 0x000131D8 File Offset: 0x000113D8
	public void SetFont(string fontName)
	{
		if (!LanguageManager.FONTDATA_TABLE.ContainsKey(fontName))
		{
			return;
		}
		Font font = LanguageManager.FONTDATA_TABLE[fontName];
		this.SetFont(font);
	}

	// Token: 0x0600024D RID: 589 RVA: 0x00013208 File Offset: 0x00011408
	public void SetFont(Font font)
	{
		if (font == null)
		{
			return;
		}
		Type[] typeList = TextLocalization.TypeList;
		int i = 0;
		while (i < typeList.Length)
		{
			Type type = typeList[i];
			if (base.GetComponent(type) != null)
			{
				if (type == typeof(Text))
				{
					base.GetComponent<Text>().font = font;
				}
				if (type == typeof(TextMesh))
				{
					base.GetComponent<TextMesh>().font = font;
					base.GetComponent<Renderer>().sharedMaterial = font.material;
					return;
				}
				break;
			}
			else
			{
				i++;
			}
		}
	}

	// Token: 0x0600024E RID: 590 RVA: 0x0001328E File Offset: 0x0001148E
	public TextLocalization()
	{
	}

	// Token: 0x0600024F RID: 591 RVA: 0x000132AC File Offset: 0x000114AC
	// Note: this type is marked as 'beforefieldinit'.
	static TextLocalization()
	{
	}

	// Token: 0x040000C1 RID: 193
	public string PriorKey = "";

	// Token: 0x040000C2 RID: 194
	[SerializeField]
	private bool IsVertical;

	// Token: 0x040000C3 RID: 195
	private string PriorText = "";

	// Token: 0x040000C4 RID: 196
	private object[] Parameters;

	// Token: 0x040000C5 RID: 197
	private Quaternion DefaultRotation;

	// Token: 0x040000C6 RID: 198
	private static Type[] TypeList = new Type[]
	{
		typeof(TextMeshPro),
		typeof(TextMeshProUGUI),
		typeof(Text),
		typeof(TextMesh)
	};
}
