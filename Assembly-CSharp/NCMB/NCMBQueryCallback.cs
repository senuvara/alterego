﻿using System;
using System.Collections.Generic;

namespace NCMB
{
	// Token: 0x0200004E RID: 78
	// (Invoke) Token: 0x06000340 RID: 832
	public delegate void NCMBQueryCallback<T>(List<T> objects, NCMBException error);
}
