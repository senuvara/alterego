﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;

namespace NCMB
{
	// Token: 0x02000048 RID: 72
	public class NCMBPushPayload
	{
		// Token: 0x17000025 RID: 37
		// (get) Token: 0x060002FC RID: 764 RVA: 0x00018704 File Offset: 0x00016904
		// (set) Token: 0x060002FD RID: 765 RVA: 0x0001870C File Offset: 0x0001690C
		public string PushId
		{
			[CompilerGenerated]
			get
			{
				return this.<PushId>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<PushId>k__BackingField = value;
			}
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x060002FE RID: 766 RVA: 0x00018715 File Offset: 0x00016915
		// (set) Token: 0x060002FF RID: 767 RVA: 0x0001871D File Offset: 0x0001691D
		public string Data
		{
			[CompilerGenerated]
			get
			{
				return this.<Data>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<Data>k__BackingField = value;
			}
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000300 RID: 768 RVA: 0x00018726 File Offset: 0x00016926
		// (set) Token: 0x06000301 RID: 769 RVA: 0x0001872E File Offset: 0x0001692E
		public string Title
		{
			[CompilerGenerated]
			get
			{
				return this.<Title>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<Title>k__BackingField = value;
			}
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x06000302 RID: 770 RVA: 0x00018737 File Offset: 0x00016937
		// (set) Token: 0x06000303 RID: 771 RVA: 0x0001873F File Offset: 0x0001693F
		public string Message
		{
			[CompilerGenerated]
			get
			{
				return this.<Message>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<Message>k__BackingField = value;
			}
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x06000304 RID: 772 RVA: 0x00018748 File Offset: 0x00016948
		// (set) Token: 0x06000305 RID: 773 RVA: 0x00018750 File Offset: 0x00016950
		public string Channel
		{
			[CompilerGenerated]
			get
			{
				return this.<Channel>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<Channel>k__BackingField = value;
			}
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x06000306 RID: 774 RVA: 0x00018759 File Offset: 0x00016959
		// (set) Token: 0x06000307 RID: 775 RVA: 0x00018761 File Offset: 0x00016961
		public bool Dialog
		{
			[CompilerGenerated]
			get
			{
				return this.<Dialog>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<Dialog>k__BackingField = value;
			}
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x06000308 RID: 776 RVA: 0x0001876A File Offset: 0x0001696A
		// (set) Token: 0x06000309 RID: 777 RVA: 0x00018772 File Offset: 0x00016972
		public string RichUrl
		{
			[CompilerGenerated]
			get
			{
				return this.<RichUrl>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<RichUrl>k__BackingField = value;
			}
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x0600030A RID: 778 RVA: 0x0001877B File Offset: 0x0001697B
		// (set) Token: 0x0600030B RID: 779 RVA: 0x00018783 File Offset: 0x00016983
		public IDictionary UserInfo
		{
			[CompilerGenerated]
			get
			{
				return this.<UserInfo>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<UserInfo>k__BackingField = value;
			}
		}

		// Token: 0x0600030C RID: 780 RVA: 0x0001878C File Offset: 0x0001698C
		internal NCMBPushPayload(string pushId, string data, string title, string message, string channel, string dialog, string richUrl, IDictionary userInfo = null)
		{
			this.PushId = pushId;
			this.Data = data;
			this.Title = title;
			this.Message = message;
			this.Channel = channel;
			this.Dialog = (dialog == "true" || dialog == "TRUE" || dialog == "True" || dialog == "1");
			this.RichUrl = richUrl;
			this.UserInfo = userInfo;
		}

		// Token: 0x0400010E RID: 270
		[CompilerGenerated]
		private string <PushId>k__BackingField;

		// Token: 0x0400010F RID: 271
		[CompilerGenerated]
		private string <Data>k__BackingField;

		// Token: 0x04000110 RID: 272
		[CompilerGenerated]
		private string <Title>k__BackingField;

		// Token: 0x04000111 RID: 273
		[CompilerGenerated]
		private string <Message>k__BackingField;

		// Token: 0x04000112 RID: 274
		[CompilerGenerated]
		private string <Channel>k__BackingField;

		// Token: 0x04000113 RID: 275
		[CompilerGenerated]
		private bool <Dialog>k__BackingField;

		// Token: 0x04000114 RID: 276
		[CompilerGenerated]
		private string <RichUrl>k__BackingField;

		// Token: 0x04000115 RID: 277
		[CompilerGenerated]
		private IDictionary <UserInfo>k__BackingField;
	}
}
