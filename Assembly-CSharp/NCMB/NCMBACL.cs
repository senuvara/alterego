﻿using System;
using System.Collections.Generic;

namespace NCMB
{
	// Token: 0x0200004B RID: 75
	public class NCMBACL
	{
		// Token: 0x17000034 RID: 52
		// (get) Token: 0x06000322 RID: 802 RVA: 0x00018A11 File Offset: 0x00016C11
		// (set) Token: 0x06000321 RID: 801 RVA: 0x00018A03 File Offset: 0x00016C03
		public bool PublicReadAccess
		{
			get
			{
				return this.GetReadAccess("*");
			}
			set
			{
				this.SetReadAccess("*", value);
			}
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x06000324 RID: 804 RVA: 0x00018A2C File Offset: 0x00016C2C
		// (set) Token: 0x06000323 RID: 803 RVA: 0x00018A1E File Offset: 0x00016C1E
		public bool PublicWriteAccess
		{
			get
			{
				return this.GetWriteAccess("*");
			}
			set
			{
				this.SetWriteAccess("*", value);
			}
		}

		// Token: 0x06000325 RID: 805 RVA: 0x00018A39 File Offset: 0x00016C39
		public NCMBACL()
		{
			this.permissionsById = new Dictionary<string, object>();
		}

		// Token: 0x06000326 RID: 806 RVA: 0x00018A4C File Offset: 0x00016C4C
		public NCMBACL(string objectId) : this()
		{
			if (objectId == null)
			{
				throw new NCMBException(new ArgumentException("objectId may not be null "));
			}
			this.SetWriteAccess(objectId, true);
			this.SetReadAccess(objectId, true);
		}

		// Token: 0x06000327 RID: 807 RVA: 0x00018A77 File Offset: 0x00016C77
		internal bool _isShared()
		{
			return this.shared;
		}

		// Token: 0x06000328 RID: 808 RVA: 0x00018A7F File Offset: 0x00016C7F
		internal void _setShared(bool shared)
		{
			this.shared = shared;
		}

		// Token: 0x06000329 RID: 809 RVA: 0x00018A88 File Offset: 0x00016C88
		internal NCMBACL _copy()
		{
			NCMBACL ncmbacl = new NCMBACL();
			try
			{
				ncmbacl.permissionsById = new Dictionary<string, object>(this.permissionsById);
			}
			catch (NCMBException error)
			{
				throw new NCMBException(error);
			}
			return ncmbacl;
		}

		// Token: 0x0600032A RID: 810 RVA: 0x00018AC8 File Offset: 0x00016CC8
		public void SetReadAccess(string objectId, bool allowed)
		{
			if (objectId == null)
			{
				throw new NCMBException(new ArgumentException("cannot SetReadAccess for null objectId "));
			}
			this._setAccess("read", objectId, allowed);
		}

		// Token: 0x0600032B RID: 811 RVA: 0x00018AEA File Offset: 0x00016CEA
		public void SetWriteAccess(string objectId, bool allowed)
		{
			if (objectId == null)
			{
				throw new NCMBException(new ArgumentException("cannot SetWriteAccess for null objectId "));
			}
			this._setAccess("write", objectId, allowed);
		}

		// Token: 0x0600032C RID: 812 RVA: 0x00018B0C File Offset: 0x00016D0C
		public void SetRoleReadAccess(string roleName, bool allowed)
		{
			this.SetReadAccess("role:" + roleName, allowed);
		}

		// Token: 0x0600032D RID: 813 RVA: 0x00018B20 File Offset: 0x00016D20
		public void SetRoleWriteAccess(string roleName, bool allowed)
		{
			this.SetWriteAccess("role:" + roleName, allowed);
		}

		// Token: 0x0600032E RID: 814 RVA: 0x00018B34 File Offset: 0x00016D34
		public static void SetDefaultACL(NCMBACL acl, bool withAccessForCurrentUser)
		{
			NCMBACL.defaultACLWithCurrentUser = null;
			if (acl != null)
			{
				NCMBACL.defaultACL = acl._copy();
				NCMBACL.defaultACL._setShared(true);
				NCMBACL.defaultACLUsesCurrentUser = withAccessForCurrentUser;
				return;
			}
			NCMBACL.defaultACL = null;
		}

		// Token: 0x0600032F RID: 815 RVA: 0x00018B64 File Offset: 0x00016D64
		private void _setAccess(string accessType, string objectId, bool allowed)
		{
			try
			{
				Dictionary<string, object> dictionary = null;
				object obj;
				if (this.permissionsById.TryGetValue(objectId, out obj))
				{
					dictionary = (Dictionary<string, object>)obj;
				}
				if (dictionary == null)
				{
					if (!allowed)
					{
						return;
					}
					dictionary = new Dictionary<string, object>();
					this.permissionsById[objectId] = dictionary;
				}
				if (allowed)
				{
					dictionary[accessType] = true;
				}
				else
				{
					dictionary.Remove(accessType);
					if (dictionary.Count == 0)
					{
						this.permissionsById.Remove(objectId);
					}
				}
			}
			catch (NCMBException ex)
			{
				throw new NCMBException(new ArgumentException("JSON failure with ACL: " + ex.GetType().ToString()));
			}
		}

		// Token: 0x06000330 RID: 816 RVA: 0x00018C08 File Offset: 0x00016E08
		public bool GetReadAccess(string objectId)
		{
			if (objectId == null)
			{
				throw new NCMBException(new ArgumentException("cannot GetReadAccess for null objectId "));
			}
			return this._getAccess("read", objectId);
		}

		// Token: 0x06000331 RID: 817 RVA: 0x00018C29 File Offset: 0x00016E29
		public bool GetWriteAccess(string objectId)
		{
			if (objectId == null)
			{
				throw new NCMBException(new ArgumentException("cannot GetWriteAccess for null objectId "));
			}
			return this._getAccess("write", objectId);
		}

		// Token: 0x06000332 RID: 818 RVA: 0x00018C4A File Offset: 0x00016E4A
		public bool GetRoleReadAccess(string roleName)
		{
			return this.GetReadAccess("role:" + roleName);
		}

		// Token: 0x06000333 RID: 819 RVA: 0x00018C5D File Offset: 0x00016E5D
		public bool GetRoleWriteAccess(string roleName)
		{
			return this.GetWriteAccess("role:" + roleName);
		}

		// Token: 0x06000334 RID: 820 RVA: 0x00018C70 File Offset: 0x00016E70
		internal static NCMBACL _getDefaultACL()
		{
			if (!NCMBACL.defaultACLUsesCurrentUser || NCMBACL.defaultACL == null)
			{
				return NCMBACL.defaultACL;
			}
			if (NCMBUser.CurrentUser == null || NCMBUser.CurrentUser.ObjectId == null)
			{
				return NCMBACL.defaultACL;
			}
			NCMBACL.defaultACLWithCurrentUser = NCMBACL.defaultACL._copy();
			NCMBACL.defaultACLWithCurrentUser._setShared(true);
			NCMBACL.defaultACLWithCurrentUser.SetReadAccess(NCMBUser.CurrentUser.ObjectId, true);
			NCMBACL.defaultACLWithCurrentUser.SetWriteAccess(NCMBUser.CurrentUser.ObjectId, true);
			return NCMBACL.defaultACLWithCurrentUser;
		}

		// Token: 0x06000335 RID: 821 RVA: 0x00018CF4 File Offset: 0x00016EF4
		private bool _getAccess(string accessType, string objectId)
		{
			bool result;
			try
			{
				Dictionary<string, object> dictionary = null;
				object obj;
				if (this.permissionsById.TryGetValue(objectId, out obj))
				{
					dictionary = (Dictionary<string, object>)obj;
				}
				if (dictionary == null)
				{
					result = false;
				}
				else if (!dictionary.TryGetValue(accessType, out obj))
				{
					result = false;
				}
				else
				{
					result = (bool)obj;
				}
			}
			catch (NCMBException ex)
			{
				throw new NCMBException(new ArgumentException("JSON failure with ACL: " + ex.GetType().ToString()));
			}
			return result;
		}

		// Token: 0x06000336 RID: 822 RVA: 0x00018D6C File Offset: 0x00016F6C
		internal IDictionary<string, object> _toJSONObject()
		{
			return this.permissionsById;
		}

		// Token: 0x06000337 RID: 823 RVA: 0x00018D74 File Offset: 0x00016F74
		internal static NCMBACL _createACLFromJSONObject(Dictionary<string, object> aclValue)
		{
			NCMBACL ncmbacl = new NCMBACL();
			if (aclValue != null)
			{
				foreach (KeyValuePair<string, object> keyValuePair in aclValue)
				{
					foreach (KeyValuePair<string, object> keyValuePair2 in ((Dictionary<string, object>)keyValuePair.Value))
					{
						ncmbacl._setAccess(keyValuePair2.Key, keyValuePair.Key, true);
					}
				}
			}
			return ncmbacl;
		}

		// Token: 0x04000128 RID: 296
		private static NCMBACL defaultACL;

		// Token: 0x04000129 RID: 297
		private Dictionary<string, object> permissionsById;

		// Token: 0x0400012A RID: 298
		private bool shared;

		// Token: 0x0400012B RID: 299
		private static NCMBACL defaultACLWithCurrentUser;

		// Token: 0x0400012C RID: 300
		private static bool defaultACLUsesCurrentUser;
	}
}
