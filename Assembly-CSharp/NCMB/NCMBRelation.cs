﻿using System;
using System.Collections.Generic;
using NCMB.Internal;

namespace NCMB
{
	// Token: 0x0200005E RID: 94
	public class NCMBRelation<T> where T : NCMBObject
	{
		// Token: 0x1700005E RID: 94
		// (get) Token: 0x0600042E RID: 1070 RVA: 0x0001C7F5 File Offset: 0x0001A9F5
		// (set) Token: 0x0600042F RID: 1071 RVA: 0x0001C7FD File Offset: 0x0001A9FD
		internal string TargetClass
		{
			get
			{
				return this._targetClass;
			}
			set
			{
				this._targetClass = value;
			}
		}

		// Token: 0x06000430 RID: 1072 RVA: 0x0001C806 File Offset: 0x0001AA06
		internal NCMBRelation(NCMBObject parent, string key)
		{
			this._parent = parent;
			this._key = key;
			this._targetClass = null;
		}

		// Token: 0x06000431 RID: 1073 RVA: 0x0001C823 File Offset: 0x0001AA23
		internal NCMBRelation(string targetClass)
		{
			this._parent = null;
			this._key = null;
			this._targetClass = targetClass;
		}

		// Token: 0x06000432 RID: 1074 RVA: 0x0001C840 File Offset: 0x0001AA40
		public void Add(T obj)
		{
			this._addDuplicationCheck(obj);
			NCMBRelationOperation<T> ncmbrelationOperation = new NCMBRelationOperation<T>(new HashSet<T>
			{
				obj
			}, null);
			this._targetClass = ncmbrelationOperation.TargetClass;
			this._parent._performOperation(this._key, ncmbrelationOperation);
		}

		// Token: 0x06000433 RID: 1075 RVA: 0x0001C888 File Offset: 0x0001AA88
		public void Remove(T obj)
		{
			this._removeDuplicationCheck(obj);
			NCMBRelationOperation<T> ncmbrelationOperation = new NCMBRelationOperation<T>(null, new HashSet<T>
			{
				obj
			});
			this._targetClass = ncmbrelationOperation.TargetClass;
			this._parent._performOperation(this._key, ncmbrelationOperation);
		}

		// Token: 0x06000434 RID: 1076 RVA: 0x0001C8D0 File Offset: 0x0001AAD0
		private void _removeDuplicationCheck(T obj)
		{
			if (this._parent._currentOperations.ContainsKey(this._key) && this._parent._currentOperations[this._key] is NCMBRelationOperation<T>)
			{
				NCMBRelationOperation<T> ncmbrelationOperation = (NCMBRelationOperation<T>)this._parent._currentOperations[this._key];
				if (ncmbrelationOperation._relationsToAdd.Count > 0)
				{
					bool flag = false;
					using (HashSet<string>.Enumerator enumerator = ncmbrelationOperation._relationsToAdd.GetEnumerator())
					{
						while (enumerator.MoveNext())
						{
							if (enumerator.Current == obj.ObjectId)
							{
								flag = true;
							}
						}
					}
					if (!flag)
					{
						throw new NCMBException(new ArgumentException("Remove objects in a Add Must be the same. Call SaveAsync() to send the data."));
					}
				}
			}
		}

		// Token: 0x06000435 RID: 1077 RVA: 0x0001C9AC File Offset: 0x0001ABAC
		private void _addDuplicationCheck(T obj)
		{
			if (this._parent._currentOperations.ContainsKey(this._key) && this._parent._currentOperations[this._key] is NCMBRelationOperation<T>)
			{
				NCMBRelationOperation<T> ncmbrelationOperation = (NCMBRelationOperation<T>)this._parent._currentOperations[this._key];
				if (ncmbrelationOperation._relationsToRemove.Count > 0)
				{
					bool flag = false;
					using (HashSet<string>.Enumerator enumerator = ncmbrelationOperation._relationsToRemove.GetEnumerator())
					{
						while (enumerator.MoveNext())
						{
							if (enumerator.Current == obj.ObjectId)
							{
								flag = true;
							}
						}
					}
					if (!flag)
					{
						throw new NCMBException(new ArgumentException("Add objects in a Remove Must be the same. Call SaveAsync() to send the data."));
					}
				}
			}
		}

		// Token: 0x06000436 RID: 1078 RVA: 0x0001CA88 File Offset: 0x0001AC88
		internal void _ensureParentAndKey(NCMBObject someParent, string someKey)
		{
			if (this._parent == null)
			{
				this._parent = someParent;
			}
			if (this._key == null)
			{
				this._key = someKey;
			}
			if (this._parent != someParent)
			{
				throw new NCMBException(new ArgumentException("IInternal error. One NCMBRelation retrieved from two different NCMBObjects."));
			}
			if (!this._key.Equals(someKey))
			{
				throw new NCMBException(new ArgumentException("Internal error. One NCMBRelation retrieved from two different keys."));
			}
		}

		// Token: 0x06000437 RID: 1079 RVA: 0x0001CAEA File Offset: 0x0001ACEA
		internal Dictionary<string, object> _encodeToJSON()
		{
			return new Dictionary<string, object>
			{
				{
					"__type",
					"Relation"
				},
				{
					"className",
					this._targetClass
				}
			};
		}

		// Token: 0x06000438 RID: 1080 RVA: 0x0001CB14 File Offset: 0x0001AD14
		public NCMBQuery<T> GetQuery()
		{
			NCMBQuery<T> query;
			if (this._targetClass == null)
			{
				query = NCMBQuery<T>.GetQuery(this._parent.ClassName);
			}
			else
			{
				query = NCMBQuery<T>.GetQuery(this._targetClass);
			}
			query._whereRelatedTo(this._parent, this._key);
			return query;
		}

		// Token: 0x04000163 RID: 355
		private NCMBObject _parent;

		// Token: 0x04000164 RID: 356
		private string _key;

		// Token: 0x04000165 RID: 357
		private string _targetClass;
	}
}
