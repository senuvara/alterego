﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using MiniJSON;
using NCMB.Internal;

namespace NCMB
{
	// Token: 0x0200005A RID: 90
	[NCMBClassName("installation")]
	public class NCMBInstallation : NCMBObject
	{
		// Token: 0x0600037C RID: 892 RVA: 0x00019498 File Offset: 0x00017698
		private void setDefaultProperty()
		{
			IDictionary<string, object> installationDefaultProperty = NCMBManager.installationDefaultProperty;
			object obj;
			if (installationDefaultProperty.TryGetValue("applicationName", out obj))
			{
				this.ApplicationName = (string)obj;
			}
			if (installationDefaultProperty.TryGetValue("appVersion", out obj))
			{
				this.AppVersion = (string)obj;
			}
			if (installationDefaultProperty.TryGetValue("deviceType", out obj))
			{
				this.DeviceType = (string)obj;
			}
			if (installationDefaultProperty.TryGetValue("timeZone", out obj))
			{
				this.TimeZone = (string)obj;
			}
			this["pushType"] = "fcm";
			this.SdkVersion = CommonConstant.SDK_VERSION;
		}

		// Token: 0x0600037D RID: 893 RVA: 0x00019530 File Offset: 0x00017730
		public NCMBInstallation() : this("")
		{
		}

		// Token: 0x0600037E RID: 894 RVA: 0x00019540 File Offset: 0x00017740
		internal NCMBInstallation(string jsonText)
		{
			if (jsonText != null && jsonText != "")
			{
				Dictionary<string, object> dictionary = Json.Deserialize(jsonText) as Dictionary<string, object>;
				object obj;
				if (dictionary.TryGetValue("data", out obj))
				{
					dictionary = (Dictionary<string, object>)obj;
				}
				this._mergeFromServer(dictionary, false);
			}
			this.DeviceToken = NCMBManager._token;
			this.setDefaultProperty();
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x0600037F RID: 895 RVA: 0x0001959E File Offset: 0x0001779E
		// (set) Token: 0x06000380 RID: 896 RVA: 0x000195B0 File Offset: 0x000177B0
		public string ApplicationName
		{
			get
			{
				return (string)this["applicationName"];
			}
			internal set
			{
				this["applicationName"] = value;
			}
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x06000381 RID: 897 RVA: 0x000195BE File Offset: 0x000177BE
		// (set) Token: 0x06000382 RID: 898 RVA: 0x000195D0 File Offset: 0x000177D0
		public string AppVersion
		{
			get
			{
				return (string)this["appVersion"];
			}
			internal set
			{
				this["appVersion"] = value;
			}
		}

		// Token: 0x1700003F RID: 63
		// (set) Token: 0x06000383 RID: 899 RVA: 0x000195DE File Offset: 0x000177DE
		public string DeviceToken
		{
			set
			{
				this["deviceToken"] = value;
			}
		}

		// Token: 0x06000384 RID: 900 RVA: 0x000195EC File Offset: 0x000177EC
		public void GetDeviceToken(NCMBGetCallback<string> callback)
		{
			if (base.ContainsKey("deviceToken") && this["deviceToken"] != null)
			{
				callback((string)this["deviceToken"], null);
				return;
			}
			new Thread(delegate()
			{
				for (int i = 0; i < 10; i++)
				{
					if (NCMBManager._token != null)
					{
						this["deviceToken"] = NCMBManager._token;
						break;
					}
					Thread.Sleep(500);
				}
				if (callback != null)
				{
					if (this.ContainsKey("deviceToken") && this["deviceToken"] != null)
					{
						callback((string)this["deviceToken"], null);
						return;
					}
					callback(null, new NCMBException("Can not get device token"));
				}
			}).Start();
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x06000385 RID: 901 RVA: 0x0001965A File Offset: 0x0001785A
		// (set) Token: 0x06000386 RID: 902 RVA: 0x0001966C File Offset: 0x0001786C
		public string DeviceType
		{
			get
			{
				return (string)this["deviceType"];
			}
			internal set
			{
				this["deviceType"] = value;
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x06000387 RID: 903 RVA: 0x0001967A File Offset: 0x0001787A
		// (set) Token: 0x06000388 RID: 904 RVA: 0x0001968C File Offset: 0x0001788C
		public string SdkVersion
		{
			get
			{
				return (string)this["sdkVersion"];
			}
			internal set
			{
				this["sdkVersion"] = value;
			}
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x06000389 RID: 905 RVA: 0x0001969A File Offset: 0x0001789A
		// (set) Token: 0x0600038A RID: 906 RVA: 0x000196AC File Offset: 0x000178AC
		public string TimeZone
		{
			get
			{
				return (string)this["timeZone"];
			}
			internal set
			{
				this["timeZone"] = value;
			}
		}

		// Token: 0x0600038B RID: 907 RVA: 0x000196BC File Offset: 0x000178BC
		public static NCMBInstallation getCurrentInstallation()
		{
			NCMBInstallation result = null;
			try
			{
				string currentInstallation = NCMBManager.GetCurrentInstallation();
				if (currentInstallation != "")
				{
					result = new NCMBInstallation(currentInstallation);
				}
				else
				{
					result = new NCMBInstallation();
				}
			}
			catch (SystemException error)
			{
				throw new NCMBException(error);
			}
			return result;
		}

		// Token: 0x0600038C RID: 908 RVA: 0x00019708 File Offset: 0x00017908
		public static NCMBQuery<NCMBInstallation> GetQuery()
		{
			return NCMBQuery<NCMBInstallation>.GetQuery("installation");
		}

		// Token: 0x0600038D RID: 909 RVA: 0x00019714 File Offset: 0x00017914
		internal override string _getBaseUrl()
		{
			return NCMBSettings.DomainURL + "/" + NCMBSettings.APIVersion + "/installations";
		}

		// Token: 0x0600038E RID: 910 RVA: 0x00019730 File Offset: 0x00017930
		internal override void _afterSave(int statusCode, NCMBException error)
		{
			if (error != null)
			{
				if (error.ErrorCode == "E404001")
				{
					NCMBManager.DeleteCurrentInstallation(NCMBManager.SearchPath());
					return;
				}
			}
			else if (statusCode == 201 || statusCode == 200)
			{
				string text = NCMBManager.SearchPath();
				if (text != NCMBSettings.currentInstallationPath)
				{
					NCMBManager.DeleteCurrentInstallation(text);
				}
				this._saveInstallationToDisk("currentInstallation");
			}
		}

		// Token: 0x0600038F RID: 911 RVA: 0x00019794 File Offset: 0x00017994
		internal void _saveInstallationToDisk(string fileName)
		{
			string path = NCMBSettings.filePath + "/" + fileName;
			lock (this.mutex)
			{
				try
				{
					string value = base._toJsonDataForDataFile();
					using (StreamWriter streamWriter = new StreamWriter(path, false, Encoding.UTF8))
					{
						streamWriter.Write(value);
						streamWriter.Close();
					}
				}
				catch (Exception error)
				{
					throw new NCMBException(error);
				}
			}
		}

		// Token: 0x02000147 RID: 327
		[CompilerGenerated]
		private sealed class <>c__DisplayClass11_0
		{
			// Token: 0x06000A0B RID: 2571 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c__DisplayClass11_0()
			{
			}

			// Token: 0x06000A0C RID: 2572 RVA: 0x00030844 File Offset: 0x0002EA44
			internal void <GetDeviceToken>b__0()
			{
				for (int i = 0; i < 10; i++)
				{
					if (NCMBManager._token != null)
					{
						this.<>4__this["deviceToken"] = NCMBManager._token;
						break;
					}
					Thread.Sleep(500);
				}
				if (this.callback != null)
				{
					if (this.<>4__this.ContainsKey("deviceToken") && this.<>4__this["deviceToken"] != null)
					{
						this.callback((string)this.<>4__this["deviceToken"], null);
						return;
					}
					this.callback(null, new NCMBException("Can not get device token"));
				}
			}

			// Token: 0x040004F9 RID: 1273
			public NCMBInstallation <>4__this;

			// Token: 0x040004FA RID: 1274
			public NCMBGetCallback<string> callback;
		}
	}
}
