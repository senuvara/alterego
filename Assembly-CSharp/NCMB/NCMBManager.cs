﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using MiniJSON;
using UnityEngine;

namespace NCMB
{
	// Token: 0x02000047 RID: 71
	public class NCMBManager : MonoBehaviour
	{
		// Token: 0x060002EC RID: 748 RVA: 0x0001838C File Offset: 0x0001658C
		public virtual void Awake()
		{
			if (!NCMBSettings._isInitialized)
			{
				Object.DontDestroyOnLoad(base.gameObject);
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x060002ED RID: 749 RVA: 0x000183A0 File Offset: 0x000165A0
		// (set) Token: 0x060002EE RID: 750 RVA: 0x000183A7 File Offset: 0x000165A7
		internal static bool Inited
		{
			[CompilerGenerated]
			get
			{
				return NCMBManager.<Inited>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				NCMBManager.<Inited>k__BackingField = value;
			}
		}

		// Token: 0x060002EF RID: 751 RVA: 0x000183AF File Offset: 0x000165AF
		private void OnRegistration(string message)
		{
			NCMBManager.Inited = true;
			if (NCMBManager.onRegistration != null)
			{
				if (message == "")
				{
					message = null;
				}
				NCMBManager.onRegistration(message);
			}
		}

		// Token: 0x060002F0 RID: 752 RVA: 0x000183DC File Offset: 0x000165DC
		private void OnNotificationReceived(string message)
		{
			if (NCMBManager.onNotificationReceived != null)
			{
				string[] array = message.Split(new string[]
				{
					"NCMB_SPLITTER"
				}, StringSplitOptions.None);
				NCMBPushPayload payload = new NCMBPushPayload(array[0], array[1], array[2], array[3], array[4], array[5], array[6], null);
				NCMBManager.onNotificationReceived(payload);
			}
		}

		// Token: 0x060002F1 RID: 753 RVA: 0x00018430 File Offset: 0x00016630
		internal static string SearchPath()
		{
			string result;
			try
			{
				string text = NCMBSettings.currentInstallationPath;
				text = NCMBSettings.filePath;
				text = text.Replace("files", "");
				text += "app_NCMB/currentInstallation";
				if (!File.Exists(text))
				{
					text = NCMBSettings.currentInstallationPath;
				}
				result = text;
			}
			catch (FileNotFoundException ex)
			{
				throw ex;
			}
			return result;
		}

		// Token: 0x060002F2 RID: 754 RVA: 0x0001848C File Offset: 0x0001668C
		internal void onTokenReceived(string token)
		{
			NCMBManager._token = token;
			string path = NCMBManager.SearchPath();
			NCMBInstallation installation = null;
			string jsonText;
			if ((jsonText = NCMBManager.ReadFile(path)) != "")
			{
				installation = new NCMBInstallation(jsonText);
			}
			else
			{
				installation = new NCMBInstallation();
			}
			installation.DeviceToken = NCMBManager._token;
			NCMBCallback <>9__1;
			installation.SaveAsync(delegate(NCMBException saveError)
			{
				if (saveError == null)
				{
					this.OnRegistration("");
					return;
				}
				NCMBObject installation;
				if (saveError.ErrorCode.Equals(NCMBException.DUPPLICATION_ERROR))
				{
					this.updateExistedInstallation(installation, path);
					return;
				}
				if (saveError.ErrorCode.Equals(NCMBException.DATA_NOT_FOUND))
				{
					installation.ObjectId = null;
					installation = installation;
					NCMBCallback callback;
					if ((callback = <>9__1) == null)
					{
						callback = (<>9__1 = delegate(NCMBException updateError)
						{
							if (updateError != null)
							{
								this.OnRegistration(updateError.ErrorMessage);
								return;
							}
							this.OnRegistration("");
						});
					}
					installation.SaveAsync(callback);
					return;
				}
				this.OnRegistration(saveError.ErrorMessage);
			});
		}

		// Token: 0x060002F3 RID: 755 RVA: 0x00018520 File Offset: 0x00016720
		private void updateExistedInstallation(NCMBInstallation installation, string path)
		{
			NCMBQuery<NCMBInstallation> query = NCMBInstallation.GetQuery();
			NCMBCallback <>9__2;
			NCMBQueryCallback<NCMBInstallation> <>9__1;
			installation.GetDeviceToken(delegate(string token, NCMBException error)
			{
				NCMBQuery<NCMBInstallation> query;
				query.WhereEqualTo("deviceToken", token);
				query = query;
				NCMBQueryCallback<NCMBInstallation> callback;
				if ((callback = <>9__1) == null)
				{
					callback = (<>9__1 = delegate(List<NCMBInstallation> objList, NCMBException findError)
					{
						if (findError != null)
						{
							this.OnRegistration(findError.ErrorMessage);
							return;
						}
						if (objList.Count != 0)
						{
							installation.ObjectId = objList[0].ObjectId;
							NCMBObject installation2 = installation;
							NCMBCallback callback2;
							if ((callback2 = <>9__2) == null)
							{
								callback2 = (<>9__2 = delegate(NCMBException installationUpdateError)
								{
									if (installationUpdateError != null)
									{
										this.OnRegistration(installationUpdateError.ErrorMessage);
										return;
									}
									this.OnRegistration("");
								});
							}
							installation2.SaveAsync(callback2);
						}
					});
				}
				query.FindAsync(callback);
			});
		}

		// Token: 0x060002F4 RID: 756 RVA: 0x00018564 File Offset: 0x00016764
		private void SaveFile(string path, string text)
		{
			try
			{
				Encoding encoding = Encoding.GetEncoding("UTF-8");
				StreamWriter streamWriter = new StreamWriter(path, false, encoding);
				streamWriter.WriteLine(text);
				streamWriter.Close();
			}
			catch (Exception ex)
			{
				if (ex != null)
				{
					path = NCMBSettings.currentInstallationPath;
					try
					{
						Encoding encoding2 = Encoding.GetEncoding("UTF-8");
						StreamWriter streamWriter2 = new StreamWriter(path, false, encoding2);
						streamWriter2.WriteLine(text);
						streamWriter2.Close();
					}
					catch (IOException ex2)
					{
						throw new IOException("File save error" + ex2.Message);
					}
				}
			}
		}

		// Token: 0x060002F5 RID: 757 RVA: 0x000185F4 File Offset: 0x000167F4
		private static string ReadFile(string path)
		{
			string result = "";
			if (File.Exists(path))
			{
				try
				{
					StreamReader streamReader = new StreamReader(path, Encoding.GetEncoding("UTF-8"));
					result = streamReader.ReadToEnd();
					streamReader.Close();
				}
				catch (Exception ex)
				{
					if (ex != null)
					{
						path = NCMBSettings.currentInstallationPath;
						try
						{
							StreamReader streamReader2 = new StreamReader(path, Encoding.GetEncoding("UTF-8"));
							result = streamReader2.ReadToEnd();
							streamReader2.Close();
						}
						catch (FileNotFoundException ex2)
						{
							throw ex2;
						}
					}
				}
			}
			return result;
		}

		// Token: 0x060002F6 RID: 758 RVA: 0x00018674 File Offset: 0x00016874
		private void onAnalyticsReceived(string _pushId)
		{
			NCMBAnalytics.TrackAppOpened(_pushId);
		}

		// Token: 0x060002F7 RID: 759 RVA: 0x0001867C File Offset: 0x0001687C
		internal static void DeleteCurrentInstallation(string path)
		{
			try
			{
				File.Delete(path);
			}
			catch (IOException innerException)
			{
				throw new IOException("Delete currentInstallation failed.", innerException);
			}
		}

		// Token: 0x060002F8 RID: 760 RVA: 0x000186B0 File Offset: 0x000168B0
		internal static string GetCurrentInstallation()
		{
			return NCMBManager.ReadFile(NCMBManager.SearchPath());
		}

		// Token: 0x060002F9 RID: 761 RVA: 0x000186BC File Offset: 0x000168BC
		internal static void CreateInstallationProperty()
		{
			string text = new AndroidJavaClass("com.nifcloud.mbaas.ncmbfcmplugin.FCMInit").CallStatic<string>("getInstallationProperty", Array.Empty<object>());
			if (text != null)
			{
				NCMBManager.installationDefaultProperty = (Json.Deserialize(text) as Dictionary<string, object>);
			}
		}

		// Token: 0x060002FA RID: 762 RVA: 0x000025AD File Offset: 0x000007AD
		public NCMBManager()
		{
		}

		// Token: 0x060002FB RID: 763 RVA: 0x000186F8 File Offset: 0x000168F8
		// Note: this type is marked as 'beforefieldinit'.
		static NCMBManager()
		{
		}

		// Token: 0x04000108 RID: 264
		private const string NS = "NCMB_SPLITTER";

		// Token: 0x04000109 RID: 265
		[CompilerGenerated]
		private static bool <Inited>k__BackingField;

		// Token: 0x0400010A RID: 266
		internal static string _token;

		// Token: 0x0400010B RID: 267
		internal static IDictionary<string, object> installationDefaultProperty = new Dictionary<string, object>();

		// Token: 0x0400010C RID: 268
		public static NCMBManager.OnRegistrationDelegate onRegistration;

		// Token: 0x0400010D RID: 269
		public static NCMBManager.OnNotificationReceivedDelegate onNotificationReceived;

		// Token: 0x02000140 RID: 320
		// (Invoke) Token: 0x060009F6 RID: 2550
		public delegate void OnRegistrationDelegate(string errorMessage);

		// Token: 0x02000141 RID: 321
		// (Invoke) Token: 0x060009FA RID: 2554
		public delegate void OnNotificationReceivedDelegate(NCMBPushPayload payload);

		// Token: 0x02000142 RID: 322
		[CompilerGenerated]
		private sealed class <>c__DisplayClass15_0
		{
			// Token: 0x060009FD RID: 2557 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c__DisplayClass15_0()
			{
			}

			// Token: 0x060009FE RID: 2558 RVA: 0x000305BC File Offset: 0x0002E7BC
			internal void <onTokenReceived>b__0(NCMBException saveError)
			{
				if (saveError == null)
				{
					this.<>4__this.OnRegistration("");
					return;
				}
				if (saveError.ErrorCode.Equals(NCMBException.DUPPLICATION_ERROR))
				{
					this.<>4__this.updateExistedInstallation(this.installation, this.path);
					return;
				}
				if (saveError.ErrorCode.Equals(NCMBException.DATA_NOT_FOUND))
				{
					this.installation.ObjectId = null;
					NCMBObject ncmbobject = this.installation;
					NCMBCallback callback;
					if ((callback = this.<>9__1) == null)
					{
						callback = (this.<>9__1 = delegate(NCMBException updateError)
						{
							if (updateError != null)
							{
								this.<>4__this.OnRegistration(updateError.ErrorMessage);
								return;
							}
							this.<>4__this.OnRegistration("");
						});
					}
					ncmbobject.SaveAsync(callback);
					return;
				}
				this.<>4__this.OnRegistration(saveError.ErrorMessage);
			}

			// Token: 0x060009FF RID: 2559 RVA: 0x00030664 File Offset: 0x0002E864
			internal void <onTokenReceived>b__1(NCMBException updateError)
			{
				if (updateError != null)
				{
					this.<>4__this.OnRegistration(updateError.ErrorMessage);
					return;
				}
				this.<>4__this.OnRegistration("");
			}

			// Token: 0x040004E9 RID: 1257
			public NCMBManager <>4__this;

			// Token: 0x040004EA RID: 1258
			public NCMBInstallation installation;

			// Token: 0x040004EB RID: 1259
			public string path;

			// Token: 0x040004EC RID: 1260
			public NCMBCallback <>9__1;
		}

		// Token: 0x02000143 RID: 323
		[CompilerGenerated]
		private sealed class <>c__DisplayClass16_0
		{
			// Token: 0x06000A00 RID: 2560 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c__DisplayClass16_0()
			{
			}

			// Token: 0x06000A01 RID: 2561 RVA: 0x0003068C File Offset: 0x0002E88C
			internal void <updateExistedInstallation>b__0(string token, NCMBException error)
			{
				this.query.WhereEqualTo("deviceToken", token);
				NCMBQuery<NCMBInstallation> ncmbquery = this.query;
				NCMBQueryCallback<NCMBInstallation> callback;
				if ((callback = this.<>9__1) == null)
				{
					callback = (this.<>9__1 = delegate(List<NCMBInstallation> objList, NCMBException findError)
					{
						if (findError != null)
						{
							this.<>4__this.OnRegistration(findError.ErrorMessage);
							return;
						}
						if (objList.Count != 0)
						{
							this.installation.ObjectId = objList[0].ObjectId;
							NCMBObject ncmbobject = this.installation;
							NCMBCallback callback2;
							if ((callback2 = this.<>9__2) == null)
							{
								callback2 = (this.<>9__2 = delegate(NCMBException installationUpdateError)
								{
									if (installationUpdateError != null)
									{
										this.<>4__this.OnRegistration(installationUpdateError.ErrorMessage);
										return;
									}
									this.<>4__this.OnRegistration("");
								});
							}
							ncmbobject.SaveAsync(callback2);
						}
					});
				}
				ncmbquery.FindAsync(callback);
			}

			// Token: 0x06000A02 RID: 2562 RVA: 0x000306D8 File Offset: 0x0002E8D8
			internal void <updateExistedInstallation>b__1(List<NCMBInstallation> objList, NCMBException findError)
			{
				if (findError != null)
				{
					this.<>4__this.OnRegistration(findError.ErrorMessage);
					return;
				}
				if (objList.Count != 0)
				{
					this.installation.ObjectId = objList[0].ObjectId;
					NCMBObject ncmbobject = this.installation;
					NCMBCallback callback;
					if ((callback = this.<>9__2) == null)
					{
						callback = (this.<>9__2 = delegate(NCMBException installationUpdateError)
						{
							if (installationUpdateError != null)
							{
								this.<>4__this.OnRegistration(installationUpdateError.ErrorMessage);
								return;
							}
							this.<>4__this.OnRegistration("");
						});
					}
					ncmbobject.SaveAsync(callback);
				}
			}

			// Token: 0x06000A03 RID: 2563 RVA: 0x00030743 File Offset: 0x0002E943
			internal void <updateExistedInstallation>b__2(NCMBException installationUpdateError)
			{
				if (installationUpdateError != null)
				{
					this.<>4__this.OnRegistration(installationUpdateError.ErrorMessage);
					return;
				}
				this.<>4__this.OnRegistration("");
			}

			// Token: 0x040004ED RID: 1261
			public NCMBQuery<NCMBInstallation> query;

			// Token: 0x040004EE RID: 1262
			public NCMBManager <>4__this;

			// Token: 0x040004EF RID: 1263
			public NCMBInstallation installation;

			// Token: 0x040004F0 RID: 1264
			public NCMBCallback <>9__2;

			// Token: 0x040004F1 RID: 1265
			public NCMBQueryCallback<NCMBInstallation> <>9__1;
		}
	}
}
