﻿using System;

namespace NCMB
{
	// Token: 0x02000050 RID: 80
	// (Invoke) Token: 0x06000348 RID: 840
	public delegate void NCMBCountCallback(int count, NCMBException error);
}
