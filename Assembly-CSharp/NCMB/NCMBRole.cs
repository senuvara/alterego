﻿using System;
using System.Text.RegularExpressions;
using NCMB.Internal;

namespace NCMB
{
	// Token: 0x0200005F RID: 95
	[NCMBClassName("role")]
	public class NCMBRole : NCMBObject
	{
		// Token: 0x06000439 RID: 1081 RVA: 0x0001B39B File Offset: 0x0001959B
		internal NCMBRole()
		{
		}

		// Token: 0x0600043A RID: 1082 RVA: 0x0001CB5C File Offset: 0x0001AD5C
		public NCMBRole(string roleName)
		{
			this.Name = roleName;
		}

		// Token: 0x0600043B RID: 1083 RVA: 0x0001CB6B File Offset: 0x0001AD6B
		public NCMBRole(string roleName, NCMBACL acl)
		{
			this.Name = roleName;
			base.ACL = acl;
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x0600043D RID: 1085 RVA: 0x0001CB8F File Offset: 0x0001AD8F
		// (set) Token: 0x0600043C RID: 1084 RVA: 0x0001CB81 File Offset: 0x0001AD81
		public string Name
		{
			get
			{
				return (string)this["roleName"];
			}
			set
			{
				this["roleName"] = value;
			}
		}

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x0600043E RID: 1086 RVA: 0x0001CBA1 File Offset: 0x0001ADA1
		public NCMBRelation<NCMBUser> Users
		{
			get
			{
				return base.GetRelation<NCMBUser>("belongUser");
			}
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x0600043F RID: 1087 RVA: 0x0001CBAE File Offset: 0x0001ADAE
		public NCMBRelation<NCMBRole> Roles
		{
			get
			{
				return base.GetRelation<NCMBRole>("belongRole");
			}
		}

		// Token: 0x06000440 RID: 1088 RVA: 0x0001CBBB File Offset: 0x0001ADBB
		public static NCMBQuery<NCMBRole> GetQuery()
		{
			return NCMBQuery<NCMBRole>.GetQuery("role");
		}

		// Token: 0x06000441 RID: 1089 RVA: 0x0001CBC8 File Offset: 0x0001ADC8
		internal override void _onSettingValue(string key, object value)
		{
			base._onSettingValue(key, value);
			if ("roleName".Equals(key))
			{
				if (base.ObjectId != null)
				{
					throw new NCMBException(new ArgumentException("A role's name can only be set before it has been saved."));
				}
				if (!(value is string))
				{
					throw new NCMBException(new ArgumentException("A role's name must be a String."));
				}
				if (!NCMBRole.namePattern.IsMatch((string)value))
				{
					throw new NCMBException(new ArgumentException("A role's name can only contain alphanumeric characters, _, -, and spaces."));
				}
			}
			if ("belongUser".Equals(key))
			{
				throw new NCMBException("belongUser key is already exist. Use this.Users to set it");
			}
			if ("belongRole".Equals(key))
			{
				throw new NCMBException("belongRole key is already exist. Use this.Roles to set it");
			}
		}

		// Token: 0x06000442 RID: 1090 RVA: 0x0001CC6C File Offset: 0x0001AE6C
		internal override void _beforeSave()
		{
			if (base.ObjectId == null && this.Name == null)
			{
				throw new NCMBException(new ArgumentException("New roles must specify a name."));
			}
		}

		// Token: 0x06000443 RID: 1091 RVA: 0x0001CC8E File Offset: 0x0001AE8E
		internal override string _getBaseUrl()
		{
			return NCMBSettings.DomainURL + "/" + NCMBSettings.APIVersion + "/roles";
		}

		// Token: 0x06000444 RID: 1092 RVA: 0x0001CCA9 File Offset: 0x0001AEA9
		// Note: this type is marked as 'beforefieldinit'.
		static NCMBRole()
		{
		}

		// Token: 0x04000166 RID: 358
		private static readonly Regex namePattern = new Regex("^[0-9a-zA-Z_\\- ]+$");
	}
}
