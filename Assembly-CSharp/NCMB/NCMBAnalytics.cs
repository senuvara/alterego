﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using MiniJSON;
using NCMB.Internal;

namespace NCMB
{
	// Token: 0x0200004C RID: 76
	[NCMBClassName("analytics")]
	internal class NCMBAnalytics
	{
		// Token: 0x06000338 RID: 824 RVA: 0x00018E1C File Offset: 0x0001701C
		internal static void TrackAppOpened(string _pushId)
		{
			if (_pushId != null && NCMBManager._token != null && NCMBSettings.UseAnalytics)
			{
				string value = "android";
				object obj = Json.Serialize(new Dictionary<string, object>
				{
					{
						"pushId",
						_pushId
					},
					{
						"deviceToken",
						NCMBManager._token
					},
					{
						"deviceType",
						value
					}
				});
				string url = NCMBAnalytics._getBaseUrl(_pushId);
				ConnectType method = ConnectType.POST;
				string content = obj.ToString();
				new NCMBConnection(url, method, content, NCMBUser._getCurrentSessionToken()).Connect(delegate(int statusCode, string responseData, NCMBException error)
				{
					try
					{
					}
					catch (Exception error2)
					{
						error = new NCMBException(error2);
					}
				});
			}
		}

		// Token: 0x06000339 RID: 825 RVA: 0x000043CE File Offset: 0x000025CE
		internal NCMBAnalytics()
		{
		}

		// Token: 0x0600033A RID: 826 RVA: 0x00018EBE File Offset: 0x000170BE
		internal static string _getBaseUrl(string _pushId)
		{
			return string.Concat(new string[]
			{
				NCMBSettings.DomainURL,
				"/",
				NCMBSettings.APIVersion,
				"/push/",
				_pushId,
				"/openNumber"
			});
		}

		// Token: 0x02000144 RID: 324
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000A04 RID: 2564 RVA: 0x0003076A File Offset: 0x0002E96A
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000A05 RID: 2565 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c()
			{
			}

			// Token: 0x06000A06 RID: 2566 RVA: 0x00030778 File Offset: 0x0002E978
			internal void <TrackAppOpened>b__0_0(int statusCode, string responseData, NCMBException error)
			{
				try
				{
				}
				catch (Exception error2)
				{
					error = new NCMBException(error2);
				}
			}

			// Token: 0x040004F2 RID: 1266
			public static readonly NCMBAnalytics.<>c <>9 = new NCMBAnalytics.<>c();

			// Token: 0x040004F3 RID: 1267
			public static HttpClientCallback <>9__0_0;
		}
	}
}
