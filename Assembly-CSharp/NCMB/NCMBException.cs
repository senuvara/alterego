﻿using System;

namespace NCMB
{
	// Token: 0x02000056 RID: 86
	public class NCMBException : Exception
	{
		// Token: 0x0600035F RID: 863 RVA: 0x00018EF7 File Offset: 0x000170F7
		public NCMBException()
		{
			this._errorCode = "";
			this.ErrorMessage = "";
		}

		// Token: 0x06000360 RID: 864 RVA: 0x00018F18 File Offset: 0x00017118
		public NCMBException(Exception error)
		{
			this._errorCode = "";
			this.ErrorMessage = error.Message;
			Debug.Log(string.Concat(new object[]
			{
				"Error occurred: ",
				error.Message,
				" \n with: ",
				error.Data,
				" ; \n ",
				error.StackTrace
			}));
		}

		// Token: 0x06000361 RID: 865 RVA: 0x00018F85 File Offset: 0x00017185
		public NCMBException(string message)
		{
			this._errorCode = "";
			this.ErrorMessage = message;
			Debug.Log("Error occurred: " + message);
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x06000362 RID: 866 RVA: 0x00018FAF File Offset: 0x000171AF
		// (set) Token: 0x06000363 RID: 867 RVA: 0x00018FB7 File Offset: 0x000171B7
		public string ErrorCode
		{
			get
			{
				return this._errorCode;
			}
			set
			{
				this._errorCode = value;
			}
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x06000364 RID: 868 RVA: 0x00018FC0 File Offset: 0x000171C0
		// (set) Token: 0x06000365 RID: 869 RVA: 0x00018FE9 File Offset: 0x000171E9
		public string ErrorMessage
		{
			get
			{
				if (this._errorMessage != null && this._errorMessage != "")
				{
					return this._errorMessage;
				}
				return this.Message;
			}
			set
			{
				this._errorMessage = value;
			}
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x06000366 RID: 870 RVA: 0x00018FF2 File Offset: 0x000171F2
		public override string Message
		{
			get
			{
				return this._errorMessage;
			}
		}

		// Token: 0x06000367 RID: 871 RVA: 0x00018FFC File Offset: 0x000171FC
		// Note: this type is marked as 'beforefieldinit'.
		static NCMBException()
		{
		}

		// Token: 0x0400012D RID: 301
		private string _errorCode;

		// Token: 0x0400012E RID: 302
		private string _errorMessage;

		// Token: 0x0400012F RID: 303
		public static readonly string BAD_REQUEST = "E400000";

		// Token: 0x04000130 RID: 304
		public static readonly string INVALID_JSON = "E400001";

		// Token: 0x04000131 RID: 305
		public static readonly string INVALID_TYPE = "E400002";

		// Token: 0x04000132 RID: 306
		public static readonly string REQUIRED = "E400003";

		// Token: 0x04000133 RID: 307
		public static readonly string INVALID_FORMAT = "E400004";

		// Token: 0x04000134 RID: 308
		public static readonly string INVALID_VALUE = "E400005";

		// Token: 0x04000135 RID: 309
		public static readonly string NOT_EXIST = "E400006";

		// Token: 0x04000136 RID: 310
		public static readonly string RELATION_ERROR = "E400008";

		// Token: 0x04000137 RID: 311
		public static readonly string INVALID_SIZE = "E400009";

		// Token: 0x04000138 RID: 312
		public static readonly string INCORRECT_HEADER = "E401001";

		// Token: 0x04000139 RID: 313
		public static readonly string INCORRECT_PASSWORD = "E401002";

		// Token: 0x0400013A RID: 314
		public static readonly string OAUTH_ERROR = "E401003";

		// Token: 0x0400013B RID: 315
		public static readonly string INVALID_ACL = "E403001";

		// Token: 0x0400013C RID: 316
		public static readonly string INVALID_OPERATION = "E403002";

		// Token: 0x0400013D RID: 317
		public static readonly string FORBIDDEN_OPERATION = "E403003";

		// Token: 0x0400013E RID: 318
		public static readonly string INVALID_SETTING = "E403005";

		// Token: 0x0400013F RID: 319
		public static readonly string INVALID_GEOPOINT = "E403006";

		// Token: 0x04000140 RID: 320
		public static readonly string INVALID_METHOD = "E405001";

		// Token: 0x04000141 RID: 321
		public static readonly string DUPPLICATION_ERROR = "E409001";

		// Token: 0x04000142 RID: 322
		public static readonly string FILE_SIZE_ERROR = "E413001";

		// Token: 0x04000143 RID: 323
		public static readonly string DOCUMENT_SIZE_ERROR = "E413002";

		// Token: 0x04000144 RID: 324
		public static readonly string REQUEST_LIMIT_ERROR = "E413003";

		// Token: 0x04000145 RID: 325
		public static readonly string UNSUPPORT_MEDIA = "E415001";

		// Token: 0x04000146 RID: 326
		public static readonly string REQUEST_OVERLOAD = "E429001";

		// Token: 0x04000147 RID: 327
		public static readonly string SYSTEM_ERROR = "E500001";

		// Token: 0x04000148 RID: 328
		public static readonly string STORAGE_ERROR = "E502001";

		// Token: 0x04000149 RID: 329
		public static readonly string MAIL_ERROR = "E502002";

		// Token: 0x0400014A RID: 330
		public static readonly string DATABASE_ERROR = "E502003";

		// Token: 0x0400014B RID: 331
		public static readonly string DATA_NOT_FOUND = "E404001";
	}
}
