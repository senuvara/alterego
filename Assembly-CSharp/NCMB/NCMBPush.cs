﻿using System;
using NCMB.Internal;
using UnityEngine;

namespace NCMB
{
	// Token: 0x0200005C RID: 92
	[NCMBClassName("push")]
	public class NCMBPush : NCMBObject
	{
		// Token: 0x060003DA RID: 986 RVA: 0x0001B38A File Offset: 0x0001958A
		static NCMBPush()
		{
		}

		// Token: 0x060003DB RID: 987 RVA: 0x0001B39B File Offset: 0x0001959B
		public NCMBPush()
		{
		}

		// Token: 0x060003DC RID: 988 RVA: 0x0001B3A3 File Offset: 0x000195A3
		public static void Register()
		{
			NCMBPush.m_AJClass.CallStatic("Init", Array.Empty<object>());
		}

		// Token: 0x060003DD RID: 989 RVA: 0x0001B3A3 File Offset: 0x000195A3
		public static void RegisterWithLocation()
		{
			NCMBPush.m_AJClass.CallStatic("Init", Array.Empty<object>());
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x060003DE RID: 990 RVA: 0x0001B3B9 File Offset: 0x000195B9
		// (set) Token: 0x060003DF RID: 991 RVA: 0x0001B3CB File Offset: 0x000195CB
		public string Message
		{
			get
			{
				return (string)this["message"];
			}
			set
			{
				this["message"] = value;
			}
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x060003E0 RID: 992 RVA: 0x0001B3D9 File Offset: 0x000195D9
		// (set) Token: 0x060003E1 RID: 993 RVA: 0x0001B3E6 File Offset: 0x000195E6
		public object SearchCondition
		{
			get
			{
				return this["searchCondition"];
			}
			set
			{
				this["searchCondition"] = value;
			}
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x060003E2 RID: 994 RVA: 0x0001B3F4 File Offset: 0x000195F4
		// (set) Token: 0x060003E3 RID: 995 RVA: 0x0001B410 File Offset: 0x00019610
		public DateTime DeliveryTime
		{
			get
			{
				return TimeZoneInfo.ConvertTimeFromUtc((DateTime)this["deliveryTime"], TimeZoneInfo.Local);
			}
			set
			{
				this["deliveryTime"] = DateTime.Parse(TimeZoneInfo.ConvertTimeToUtc(value).ToString());
			}
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x060003E4 RID: 996 RVA: 0x0001B440 File Offset: 0x00019640
		// (set) Token: 0x060003E5 RID: 997 RVA: 0x0001B452 File Offset: 0x00019652
		public bool ImmediateDeliveryFlag
		{
			get
			{
				return (bool)this["immediateDeliveryFlag"];
			}
			set
			{
				this["immediateDeliveryFlag"] = value;
			}
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x060003E6 RID: 998 RVA: 0x0001B465 File Offset: 0x00019665
		// (set) Token: 0x060003E7 RID: 999 RVA: 0x0001B477 File Offset: 0x00019677
		public string Title
		{
			get
			{
				return (string)this["title"];
			}
			set
			{
				this["title"] = value;
			}
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x060003E8 RID: 1000 RVA: 0x0001B488 File Offset: 0x00019688
		// (set) Token: 0x060003E9 RID: 1001 RVA: 0x0001B4D8 File Offset: 0x000196D8
		public bool PushToIOS
		{
			get
			{
				bool result = false;
				if (base.ContainsKey("target"))
				{
					string[] array = (string[])this["target"];
					for (int i = 0; i < array.Length; i++)
					{
						if (array[i] == "ios")
						{
							result = true;
						}
					}
				}
				return result;
			}
			set
			{
				bool pushToAndroid = this.PushToAndroid;
				if (value && !pushToAndroid)
				{
					this["target"] = new string[]
					{
						"ios"
					};
					return;
				}
				if (!value && pushToAndroid)
				{
					this["target"] = new string[]
					{
						"android"
					};
					return;
				}
				if (base.ContainsKey("target"))
				{
					this.Remove("target");
				}
			}
		}

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x060003EA RID: 1002 RVA: 0x0001B544 File Offset: 0x00019744
		// (set) Token: 0x060003EB RID: 1003 RVA: 0x0001B594 File Offset: 0x00019794
		public bool PushToAndroid
		{
			get
			{
				bool result = false;
				if (base.ContainsKey("target"))
				{
					string[] array = (string[])this["target"];
					for (int i = 0; i < array.Length; i++)
					{
						if (array[i] == "android")
						{
							result = true;
						}
					}
				}
				return result;
			}
			set
			{
				bool pushToIOS = this.PushToIOS;
				if (value && !pushToIOS)
				{
					this["target"] = new string[]
					{
						"android"
					};
					return;
				}
				if (!value && pushToIOS)
				{
					this["target"] = new string[]
					{
						"ios"
					};
					return;
				}
				if (base.ContainsKey("target"))
				{
					this.Remove("target");
				}
			}
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x060003EC RID: 1004 RVA: 0x0001B600 File Offset: 0x00019800
		// (set) Token: 0x060003ED RID: 1005 RVA: 0x0001B617 File Offset: 0x00019817
		public int? Badge
		{
			get
			{
				return new int?((int)this["badgeSetting"]);
			}
			set
			{
				this["badgeSetting"] = value;
			}
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x060003EE RID: 1006 RVA: 0x0001B62A File Offset: 0x0001982A
		// (set) Token: 0x060003EF RID: 1007 RVA: 0x0001B63C File Offset: 0x0001983C
		public bool BadgeIncrementFlag
		{
			get
			{
				return (bool)this["badgeIncrementFlag"];
			}
			set
			{
				this["badgeIncrementFlag"] = value;
			}
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x060003F0 RID: 1008 RVA: 0x0001B64F File Offset: 0x0001984F
		// (set) Token: 0x060003F1 RID: 1009 RVA: 0x0001B661 File Offset: 0x00019861
		public string RichUrl
		{
			get
			{
				return (string)this["richUrl"];
			}
			set
			{
				this["richUrl"] = value;
			}
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x060003F2 RID: 1010 RVA: 0x0001B66F File Offset: 0x0001986F
		// (set) Token: 0x060003F3 RID: 1011 RVA: 0x0001B681 File Offset: 0x00019881
		public bool Dialog
		{
			get
			{
				return (bool)this["dialog"];
			}
			set
			{
				this["dialog"] = value;
			}
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x060003F4 RID: 1012 RVA: 0x0001B694 File Offset: 0x00019894
		// (set) Token: 0x060003F5 RID: 1013 RVA: 0x0001B6A6 File Offset: 0x000198A6
		public bool ContentAvailable
		{
			get
			{
				return (bool)this["contentAvailable"];
			}
			set
			{
				this["contentAvailable"] = value;
			}
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x060003F6 RID: 1014 RVA: 0x0001B6B9 File Offset: 0x000198B9
		// (set) Token: 0x060003F7 RID: 1015 RVA: 0x0001B6CB File Offset: 0x000198CB
		public string Category
		{
			get
			{
				return (string)this["category"];
			}
			set
			{
				this["category"] = value;
			}
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x060003F8 RID: 1016 RVA: 0x0001B6DC File Offset: 0x000198DC
		// (set) Token: 0x060003F9 RID: 1017 RVA: 0x0001B708 File Offset: 0x00019908
		public DateTime? DeliveryExpirationDate
		{
			get
			{
				DateTime? dateTime = new DateTime?((DateTime)this["deliveryExpirationDate"]);
				this.DeliveryExpirationDate = dateTime;
				return dateTime;
			}
			set
			{
				this["deliveryExpirationDate"] = value;
			}
		}

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x060003FA RID: 1018 RVA: 0x0001B71B File Offset: 0x0001991B
		// (set) Token: 0x060003FB RID: 1019 RVA: 0x0001B72D File Offset: 0x0001992D
		public string DeliveryExpirationTime
		{
			get
			{
				return (string)this["deliveryExpirationTime"];
			}
			set
			{
				this["deliveryExpirationTime"] = value;
			}
		}

		// Token: 0x060003FC RID: 1020 RVA: 0x0001B73B File Offset: 0x0001993B
		public void SendPush()
		{
			this.SendPush(null);
		}

		// Token: 0x060003FD RID: 1021 RVA: 0x0001B744 File Offset: 0x00019944
		public void SendPush(NCMBCallback callback)
		{
			if (base.ContainsKey("deliveryExpirationDate") && base.ContainsKey("deliveryExpirationTime"))
			{
				throw new ArgumentException("DeliveryExpirationDate and DeliveryExpirationTime can not be set at the same time.Please set only one.");
			}
			if (base.ContainsKey("deliveryTime") && base.ContainsKey("immediateDeliveryFlag") && this.ImmediateDeliveryFlag)
			{
				throw new ArgumentException("deliveryTime and immediateDeliveryFlag can not be set at the same time.Please set only one.");
			}
			if (!base.ContainsKey("deliveryTime"))
			{
				this.ImmediateDeliveryFlag = true;
			}
			base.SaveAsync(callback);
		}

		// Token: 0x060003FE RID: 1022 RVA: 0x0001B7BE File Offset: 0x000199BE
		public static NCMBQuery<NCMBPush> GetQuery()
		{
			return NCMBQuery<NCMBPush>.GetQuery("push");
		}

		// Token: 0x060003FF RID: 1023 RVA: 0x0001B7CA File Offset: 0x000199CA
		internal override string _getBaseUrl()
		{
			return NCMBSettings.DomainURL + "/" + NCMBSettings.APIVersion + "/push";
		}

		// Token: 0x0400015B RID: 347
		private static AndroidJavaClass m_AJClass = new AndroidJavaClass("com.nifcloud.mbaas.ncmbfcmplugin.FCMInit");
	}
}
