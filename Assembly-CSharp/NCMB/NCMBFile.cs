﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using MiniJSON;
using NCMB.Internal;

namespace NCMB
{
	// Token: 0x02000058 RID: 88
	[NCMBClassName("files")]
	public class NCMBFile : NCMBObject
	{
		// Token: 0x17000039 RID: 57
		// (get) Token: 0x06000369 RID: 873 RVA: 0x000191DC File Offset: 0x000173DC
		// (set) Token: 0x0600036A RID: 874 RVA: 0x00019213 File Offset: 0x00017413
		public string FileName
		{
			get
			{
				object obj = null;
				this.estimatedData.TryGetValue("fileName", out obj);
				if (obj == null)
				{
					return null;
				}
				return (string)this["fileName"];
			}
			set
			{
				this["fileName"] = value;
			}
		}

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x0600036B RID: 875 RVA: 0x00019224 File Offset: 0x00017424
		// (set) Token: 0x0600036C RID: 876 RVA: 0x0001925B File Offset: 0x0001745B
		public byte[] FileData
		{
			get
			{
				object obj = null;
				this.estimatedData.TryGetValue("fileData", out obj);
				if (obj == null)
				{
					return null;
				}
				return (byte[])this["fileData"];
			}
			set
			{
				this["fileData"] = value;
			}
		}

		// Token: 0x0600036D RID: 877 RVA: 0x00019269 File Offset: 0x00017469
		public NCMBFile() : this(null)
		{
		}

		// Token: 0x0600036E RID: 878 RVA: 0x00019272 File Offset: 0x00017472
		public NCMBFile(string fileName) : this(fileName, null)
		{
		}

		// Token: 0x0600036F RID: 879 RVA: 0x0001927C File Offset: 0x0001747C
		public NCMBFile(string fileName, byte[] fileData) : this(fileName, fileData, null)
		{
		}

		// Token: 0x06000370 RID: 880 RVA: 0x00019287 File Offset: 0x00017487
		public NCMBFile(string fileName, byte[] fileData, NCMBACL acl)
		{
			this.FileName = fileName;
			this.FileData = fileData;
			base.ACL = acl;
		}

		// Token: 0x06000371 RID: 881 RVA: 0x000192A4 File Offset: 0x000174A4
		public override void SaveAsync(NCMBCallback callback)
		{
			if (this.FileName == null)
			{
				throw new NCMBException("fileName must not be null.");
			}
			ConnectType method;
			if (base.CreateDate != null)
			{
				method = ConnectType.PUT;
			}
			else
			{
				method = ConnectType.POST;
			}
			IDictionary<string, INCMBFieldOperation> currentOperations = null;
			currentOperations = base.StartSave();
			string content = base._toJSONObjectForSaving(currentOperations);
			new NCMBConnection(this._getBaseUrl(), method, content, NCMBUser._getCurrentSessionToken(), this).Connect(delegate(int statusCode, string responseData, NCMBException error)
			{
				try
				{
					if (error != null)
					{
						this._handleSaveResult(false, null, currentOperations);
					}
					else
					{
						Dictionary<string, object> responseDic = Json.Deserialize(responseData) as Dictionary<string, object>;
						this._handleSaveResult(true, responseDic, currentOperations);
					}
				}
				catch (Exception error2)
				{
					error = new NCMBException(error2);
				}
				if (callback != null)
				{
					callback(error);
				}
			});
		}

		// Token: 0x06000372 RID: 882 RVA: 0x00019332 File Offset: 0x00017532
		public override void SaveAsync()
		{
			this.SaveAsync(null);
		}

		// Token: 0x06000373 RID: 883 RVA: 0x0001933C File Offset: 0x0001753C
		public void FetchAsync(NCMBGetFileCallback callback)
		{
			if (this.FileName == null)
			{
				throw new NCMBException("fileName must not be null.");
			}
			new NCMBConnection(this._getBaseUrl(), ConnectType.GET, null, NCMBUser._getCurrentSessionToken(), this).Connect(delegate(int statusCode, byte[] responseData, NCMBException error)
			{
				this.estimatedData["fileData"] = responseData;
				if (callback != null)
				{
					callback(responseData, error);
				}
			});
		}

		// Token: 0x06000374 RID: 884 RVA: 0x00019394 File Offset: 0x00017594
		public override void FetchAsync()
		{
			this.FetchAsync(null);
		}

		// Token: 0x06000375 RID: 885 RVA: 0x0001939D File Offset: 0x0001759D
		public static NCMBQuery<NCMBFile> GetQuery()
		{
			return NCMBQuery<NCMBFile>.GetQuery("file");
		}

		// Token: 0x06000376 RID: 886 RVA: 0x000193AC File Offset: 0x000175AC
		internal override string _getBaseUrl()
		{
			if (this.FileName != null)
			{
				return string.Concat(new string[]
				{
					NCMBSettings.DomainURL,
					"/",
					NCMBSettings.APIVersion,
					"/files/",
					this.FileName
				});
			}
			return NCMBSettings.DomainURL + "/" + NCMBSettings.APIVersion + "/files";
		}

		// Token: 0x02000145 RID: 325
		[CompilerGenerated]
		private sealed class <>c__DisplayClass10_0
		{
			// Token: 0x06000A07 RID: 2567 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c__DisplayClass10_0()
			{
			}

			// Token: 0x06000A08 RID: 2568 RVA: 0x000307A0 File Offset: 0x0002E9A0
			internal void <SaveAsync>b__0(int statusCode, string responseData, NCMBException error)
			{
				try
				{
					if (error != null)
					{
						this.<>4__this._handleSaveResult(false, null, this.currentOperations);
					}
					else
					{
						Dictionary<string, object> responseDic = Json.Deserialize(responseData) as Dictionary<string, object>;
						this.<>4__this._handleSaveResult(true, responseDic, this.currentOperations);
					}
				}
				catch (Exception error2)
				{
					error = new NCMBException(error2);
				}
				if (this.callback != null)
				{
					this.callback(error);
				}
			}

			// Token: 0x040004F4 RID: 1268
			public NCMBFile <>4__this;

			// Token: 0x040004F5 RID: 1269
			public IDictionary<string, INCMBFieldOperation> currentOperations;

			// Token: 0x040004F6 RID: 1270
			public NCMBCallback callback;
		}

		// Token: 0x02000146 RID: 326
		[CompilerGenerated]
		private sealed class <>c__DisplayClass12_0
		{
			// Token: 0x06000A09 RID: 2569 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c__DisplayClass12_0()
			{
			}

			// Token: 0x06000A0A RID: 2570 RVA: 0x00030814 File Offset: 0x0002EA14
			internal void <FetchAsync>b__0(int statusCode, byte[] responseData, NCMBException error)
			{
				this.<>4__this.estimatedData["fileData"] = responseData;
				if (this.callback != null)
				{
					this.callback(responseData, error);
				}
			}

			// Token: 0x040004F7 RID: 1271
			public NCMBFile <>4__this;

			// Token: 0x040004F8 RID: 1272
			public NCMBGetFileCallback callback;
		}
	}
}
