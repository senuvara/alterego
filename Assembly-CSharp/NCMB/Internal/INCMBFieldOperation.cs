﻿using System;

namespace NCMB.Internal
{
	// Token: 0x0200006C RID: 108
	internal interface INCMBFieldOperation
	{
		// Token: 0x060004B5 RID: 1205
		object Encode();

		// Token: 0x060004B6 RID: 1206
		INCMBFieldOperation MergeWithPrevious(INCMBFieldOperation previous);

		// Token: 0x060004B7 RID: 1207
		object Apply(object oldValue, NCMBObject obj, string key);
	}
}
