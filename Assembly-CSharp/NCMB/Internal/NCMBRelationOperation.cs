﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace NCMB.Internal
{
	// Token: 0x0200006E RID: 110
	internal class NCMBRelationOperation<T> : INCMBFieldOperation where T : NCMBObject
	{
		// Token: 0x1700006C RID: 108
		// (get) Token: 0x060004BC RID: 1212 RVA: 0x0001EACE File Offset: 0x0001CCCE
		internal string TargetClass
		{
			get
			{
				return this._targetClass;
			}
		}

		// Token: 0x060004BD RID: 1213 RVA: 0x0001EAD8 File Offset: 0x0001CCD8
		internal NCMBRelationOperation(HashSet<T> newRelationsToAdd, HashSet<T> newRelationsToRemove)
		{
			this._targetClass = null;
			this._relationsToAdd = new HashSet<string>();
			this._relationsToRemove = new HashSet<string>();
			if (newRelationsToAdd != null)
			{
				foreach (T t in newRelationsToAdd)
				{
					NCMBObject ncmbobject = t;
					if (ncmbobject.ObjectId == null)
					{
						throw new NCMBException(new ArgumentException("All objects in a relation must have object ids."));
					}
					this._relationsToAdd.Add(ncmbobject.ObjectId);
					if (this._targetClass == null)
					{
						this._targetClass = ncmbobject.ClassName;
					}
					else if (!this._targetClass.Equals(ncmbobject.ClassName))
					{
						throw new NCMBException(new ArgumentException("All objects in a relation must be of the same class."));
					}
				}
			}
			if (newRelationsToRemove != null)
			{
				foreach (T t2 in newRelationsToRemove)
				{
					NCMBObject ncmbobject2 = t2;
					if (ncmbobject2.ObjectId == null)
					{
						throw new NCMBException(new ArgumentException("All objects in a relation must have object ids."));
					}
					this._relationsToRemove.Add(ncmbobject2.ObjectId);
					if (this._targetClass == null)
					{
						this._targetClass = ncmbobject2.ClassName;
					}
					else if (!this._targetClass.Equals(ncmbobject2.ClassName))
					{
						throw new NCMBException(new ArgumentException("All objects in a relation must be of the same class."));
					}
				}
			}
			if (this._targetClass == null)
			{
				throw new NCMBException(new ArgumentException("Cannot create a NCMBRelationOperation with no objects."));
			}
		}

		// Token: 0x060004BE RID: 1214 RVA: 0x0001EC6C File Offset: 0x0001CE6C
		private NCMBRelationOperation(string newTargetClass, HashSet<string> newRelationsToAdd, HashSet<string> newRelationsToRemove)
		{
			this._targetClass = newTargetClass;
			this._relationsToAdd = new HashSet<string>(newRelationsToAdd);
			this._relationsToRemove = new HashSet<string>(newRelationsToRemove);
		}

		// Token: 0x060004BF RID: 1215 RVA: 0x0001EC94 File Offset: 0x0001CE94
		public object Encode()
		{
			Dictionary<string, object> dictionary = null;
			Dictionary<string, object> dictionary2 = null;
			if (this._relationsToAdd.Count > 0)
			{
				dictionary = new Dictionary<string, object>();
				dictionary.Add("__op", "AddRelation");
				dictionary.Add("objects", this._convertSetToArray(this._relationsToAdd));
			}
			if (this._relationsToRemove.Count > 0)
			{
				dictionary2 = new Dictionary<string, object>();
				dictionary2.Add("__op", "RemoveRelation");
				dictionary2.Add("objects", this._convertSetToArray(this._relationsToRemove));
			}
			if (dictionary != null)
			{
				return dictionary;
			}
			if (dictionary2 != null)
			{
				return dictionary2;
			}
			return null;
		}

		// Token: 0x060004C0 RID: 1216 RVA: 0x0001ED28 File Offset: 0x0001CF28
		private ArrayList _convertSetToArray(HashSet<string> set)
		{
			ArrayList arrayList = new ArrayList();
			foreach (string value in set)
			{
				arrayList.Add(new Dictionary<string, object>
				{
					{
						"__type",
						"Pointer"
					},
					{
						"className",
						this._targetClass
					},
					{
						"objectId",
						value
					}
				});
			}
			return arrayList;
		}

		// Token: 0x060004C1 RID: 1217 RVA: 0x0001EDB4 File Offset: 0x0001CFB4
		public INCMBFieldOperation MergeWithPrevious(INCMBFieldOperation previous)
		{
			if (previous == null)
			{
				return this;
			}
			if (previous is NCMBDeleteOperation)
			{
				throw new NCMBException(new ArgumentException("You can't modify a relation after deleting it."));
			}
			if (!(previous is NCMBRelationOperation<T>))
			{
				throw new NCMBException(new ArgumentException("Operation is invalid after previous operation."));
			}
			NCMBRelationOperation<T> ncmbrelationOperation = (NCMBRelationOperation<T>)previous;
			if (ncmbrelationOperation._targetClass != null && !ncmbrelationOperation._targetClass.Equals(this._targetClass))
			{
				throw new NCMBException(new ArgumentException(string.Concat(new string[]
				{
					"Related object object must be of class ",
					ncmbrelationOperation._targetClass,
					", but ",
					this._targetClass,
					" was passed in."
				})));
			}
			HashSet<string> hashSet = new HashSet<string>(ncmbrelationOperation._relationsToAdd);
			HashSet<string> hashSet2 = new HashSet<string>(ncmbrelationOperation._relationsToRemove);
			if (this._relationsToAdd.Count > 0)
			{
				if (hashSet2.Count == 0)
				{
					using (HashSet<string>.Enumerator enumerator = this._relationsToAdd.GetEnumerator())
					{
						while (enumerator.MoveNext())
						{
							string item = enumerator.Current;
							hashSet.Add(item);
						}
						goto IL_12A;
					}
				}
				foreach (string item2 in this._relationsToAdd)
				{
					hashSet2.Remove(item2);
				}
			}
			IL_12A:
			if (this._relationsToRemove.Count > 0)
			{
				if (hashSet.Count == 0)
				{
					using (HashSet<string>.Enumerator enumerator = this._relationsToRemove.GetEnumerator())
					{
						while (enumerator.MoveNext())
						{
							string item3 = enumerator.Current;
							hashSet2.Add(item3);
						}
						goto IL_1B2;
					}
				}
				foreach (string item4 in this._relationsToRemove)
				{
					hashSet.Remove(item4);
				}
			}
			IL_1B2:
			return new NCMBRelationOperation<T>(this._targetClass, hashSet, hashSet2);
		}

		// Token: 0x060004C2 RID: 1218 RVA: 0x0001EFC4 File Offset: 0x0001D1C4
		public object Apply(object oldValue, NCMBObject obj, string key)
		{
			if (oldValue == null || (oldValue is IList && ((IList)oldValue).Count == 0))
			{
				return new NCMBRelation<T>(obj, key)
				{
					TargetClass = this._targetClass
				};
			}
			if (oldValue is NCMBRelation<T>)
			{
				NCMBRelation<T> ncmbrelation = (NCMBRelation<T>)oldValue;
				if (this._targetClass != null && ncmbrelation.TargetClass != null)
				{
					if (!ncmbrelation.TargetClass.Equals(this._targetClass))
					{
						throw new ArgumentException(string.Concat(new string[]
						{
							"Related object object must be of class ",
							ncmbrelation.TargetClass,
							", but ",
							this._targetClass,
							" was passed in."
						}));
					}
					ncmbrelation.TargetClass = this._targetClass;
				}
				return ncmbrelation;
			}
			throw new NCMBException(new ArgumentException("Operation is invalid after previous operation."));
		}

		// Token: 0x04000194 RID: 404
		private string _targetClass;

		// Token: 0x04000195 RID: 405
		internal HashSet<string> _relationsToAdd;

		// Token: 0x04000196 RID: 406
		internal HashSet<string> _relationsToRemove;
	}
}
