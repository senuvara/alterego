﻿using System;
using System.Runtime.CompilerServices;

namespace NCMB.Internal
{
	// Token: 0x02000068 RID: 104
	[AttributeUsage(AttributeTargets.All)]
	internal class NCMBClassNameAttribute : Attribute
	{
		// Token: 0x1700006B RID: 107
		// (get) Token: 0x06000495 RID: 1173 RVA: 0x0001DEE4 File Offset: 0x0001C0E4
		// (set) Token: 0x06000496 RID: 1174 RVA: 0x0001DEEC File Offset: 0x0001C0EC
		public string ClassName
		{
			[CompilerGenerated]
			get
			{
				return this.<ClassName>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ClassName>k__BackingField = value;
			}
		}

		// Token: 0x06000497 RID: 1175 RVA: 0x0001DEF5 File Offset: 0x0001C0F5
		internal NCMBClassNameAttribute(string className)
		{
			this.ClassName = className;
		}

		// Token: 0x0400017B RID: 379
		[CompilerGenerated]
		private string <ClassName>k__BackingField;
	}
}
