﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace NCMB.Internal
{
	// Token: 0x02000067 RID: 103
	internal class NCMBAddUniqueOperation : INCMBFieldOperation
	{
		// Token: 0x06000491 RID: 1169 RVA: 0x0001DC98 File Offset: 0x0001BE98
		public NCMBAddUniqueOperation(object values)
		{
			if (values is IEnumerable)
			{
				foreach (object value in ((IEnumerable)values))
				{
					this.objects.Add(value);
				}
				return;
			}
			this.objects.Add(values);
		}

		// Token: 0x06000492 RID: 1170 RVA: 0x0001DCF6 File Offset: 0x0001BEF6
		public object Encode()
		{
			return new Dictionary<string, object>
			{
				{
					"__op",
					"AddUnique"
				},
				{
					"objects",
					NCMBUtility._maybeEncodeJSONObject(this.objects, true)
				}
			};
		}

		// Token: 0x06000493 RID: 1171 RVA: 0x0001DD24 File Offset: 0x0001BF24
		public INCMBFieldOperation MergeWithPrevious(INCMBFieldOperation previous)
		{
			if (previous == null)
			{
				return this;
			}
			if (previous is NCMBDeleteOperation)
			{
				return new NCMBSetOperation(this.objects);
			}
			if (previous is NCMBSetOperation)
			{
				object value = ((NCMBSetOperation)previous).getValue();
				if (value is IList)
				{
					return new NCMBSetOperation(this.Apply(value, null, null));
				}
				throw new InvalidOperationException("You can only add an item to a List.");
			}
			else
			{
				if (previous is NCMBAddUniqueOperation)
				{
					IList oldValue = new ArrayList(((NCMBAddUniqueOperation)previous).objects);
					return new NCMBAddUniqueOperation((IList)this.Apply(oldValue, null, null));
				}
				throw new InvalidOperationException("Operation is invalid after previous operation.");
			}
		}

		// Token: 0x06000494 RID: 1172 RVA: 0x0001DDB8 File Offset: 0x0001BFB8
		public object Apply(object oldValue, NCMBObject obj, string key)
		{
			if (oldValue == null)
			{
				return new ArrayList(this.objects);
			}
			if (oldValue is IList)
			{
				ArrayList arrayList = new ArrayList((IList)oldValue);
				Hashtable hashtable = new Hashtable();
				foreach (object obj2 in arrayList)
				{
					int num = 0;
					if (obj2 is NCMBObject)
					{
						NCMBObject ncmbobject = (NCMBObject)obj2;
						hashtable.Add(ncmbobject.ObjectId, num);
					}
				}
				foreach (object obj3 in this.objects)
				{
					if (obj3 is NCMBObject)
					{
						NCMBObject ncmbobject2 = (NCMBObject)obj3;
						if (hashtable.ContainsKey(ncmbobject2.ObjectId))
						{
							int index = Convert.ToInt32(hashtable[ncmbobject2.ObjectId]);
							arrayList.Insert(index, obj3);
						}
						else
						{
							arrayList.Add(obj3);
						}
					}
					else if (!arrayList.Contains(obj3))
					{
						arrayList.Add(obj3);
					}
				}
				return arrayList;
			}
			throw new InvalidOperationException("Operation is invalid after previous operation.");
		}

		// Token: 0x0400017A RID: 378
		private ArrayList objects = new ArrayList();
	}
}
