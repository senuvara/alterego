﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace NCMB.Internal
{
	// Token: 0x02000066 RID: 102
	internal class NCMBAddOperation : INCMBFieldOperation
	{
		// Token: 0x0600048D RID: 1165 RVA: 0x0001DB38 File Offset: 0x0001BD38
		public NCMBAddOperation(object values)
		{
			if (values is IEnumerable)
			{
				foreach (object value in ((IEnumerable)values))
				{
					this.objects.Add(value);
				}
				return;
			}
			this.objects.Add(values);
		}

		// Token: 0x0600048E RID: 1166 RVA: 0x0001DB96 File Offset: 0x0001BD96
		public object Encode()
		{
			return new Dictionary<string, object>
			{
				{
					"__op",
					"Add"
				},
				{
					"objects",
					NCMBUtility._maybeEncodeJSONObject(this.objects, true)
				}
			};
		}

		// Token: 0x0600048F RID: 1167 RVA: 0x0001DBC4 File Offset: 0x0001BDC4
		public INCMBFieldOperation MergeWithPrevious(INCMBFieldOperation previous)
		{
			if (previous == null)
			{
				return this;
			}
			if (previous is NCMBDeleteOperation)
			{
				return new NCMBSetOperation(this.objects);
			}
			if (previous is NCMBSetOperation)
			{
				object value = ((NCMBSetOperation)previous).getValue();
				if (value is IList)
				{
					ArrayList arrayList = new ArrayList((IList)value);
					arrayList.AddRange(this.objects);
					return new NCMBSetOperation(arrayList);
				}
				throw new InvalidOperationException("You can only add an item to a List.");
			}
			else
			{
				if (previous is NCMBAddOperation)
				{
					ArrayList arrayList2 = new ArrayList(((NCMBAddOperation)previous).objects);
					arrayList2.AddRange(this.objects);
					return new NCMBAddOperation(arrayList2);
				}
				throw new InvalidOperationException("Operation is invalid after previous operation.");
			}
		}

		// Token: 0x06000490 RID: 1168 RVA: 0x0001DC62 File Offset: 0x0001BE62
		public object Apply(object oldValue, NCMBObject obj, string key)
		{
			if (oldValue == null)
			{
				return this.objects;
			}
			if (oldValue is IList)
			{
				ArrayList arrayList = new ArrayList((IList)oldValue);
				arrayList.AddRange(this.objects);
				return arrayList;
			}
			throw new InvalidOperationException("Operation is invalid after previous operation.");
		}

		// Token: 0x04000179 RID: 377
		private ArrayList objects = new ArrayList();
	}
}
