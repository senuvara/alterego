﻿using System;

namespace NCMB.Internal
{
	// Token: 0x02000064 RID: 100
	internal static class CommonConstant
	{
		// Token: 0x0600048A RID: 1162 RVA: 0x0001DAFD File Offset: 0x0001BCFD
		// Note: this type is marked as 'beforefieldinit'.
		static CommonConstant()
		{
		}

		// Token: 0x04000175 RID: 373
		public static readonly string DOMAIN = "mbaas.api.nifcloud.com";

		// Token: 0x04000176 RID: 374
		public static readonly string DOMAIN_URL = "https://mbaas.api.nifcloud.com";

		// Token: 0x04000177 RID: 375
		public static readonly string API_VERSION = "2013-09-01";

		// Token: 0x04000178 RID: 376
		public static readonly string SDK_VERSION = "4.0.0";
	}
}
