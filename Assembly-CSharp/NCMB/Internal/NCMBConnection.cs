﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using MimeTypes;
using MiniJSON;
using UnityEngine;
using UnityEngine.Networking;

namespace NCMB.Internal
{
	// Token: 0x02000069 RID: 105
	public class NCMBConnection
	{
		// Token: 0x06000498 RID: 1176 RVA: 0x0001DF04 File Offset: 0x0001C104
		internal NCMBConnection(string url, ConnectType method, string content, string sessionToken) : this(url, method, content, sessionToken, null, NCMBSettings.DomainURL)
		{
		}

		// Token: 0x06000499 RID: 1177 RVA: 0x0001DF17 File Offset: 0x0001C117
		internal NCMBConnection(string url, ConnectType method, string content, string sessionToken, NCMBFile file) : this(url, method, content, sessionToken, file, NCMBSettings.DomainURL)
		{
		}

		// Token: 0x0600049A RID: 1178 RVA: 0x0001DF2C File Offset: 0x0001C12C
		internal NCMBConnection(string url, ConnectType method, string content, string sessionToken, NCMBFile file, string domain)
		{
			this._method = method;
			this._content = content;
			this._url = url;
			this._sessionToken = sessionToken;
			this._applicationKey = NCMBSettings.ApplicationKey;
			this._clientKey = NCMBSettings.ClientKey;
			this._domainUri = new Uri(domain);
			this._file = file;
			this._request = this._returnRequest();
		}

		// Token: 0x0600049B RID: 1179 RVA: 0x0001DFD5 File Offset: 0x0001C1D5
		internal void Connect(HttpClientFileDataCallback callback)
		{
			this._Connection(callback);
		}

		// Token: 0x0600049C RID: 1180 RVA: 0x0001DFD5 File Offset: 0x0001C1D5
		internal void Connect(HttpClientCallback callback)
		{
			this._Connection(callback);
		}

		// Token: 0x0600049D RID: 1181 RVA: 0x0001DFDE File Offset: 0x0001C1DE
		private void _Connection(object callback)
		{
			GameObject.Find("NCMBSettings").GetComponent<NCMBSettings>().Connection(this, callback);
		}

		// Token: 0x0600049E RID: 1182 RVA: 0x0001DFF8 File Offset: 0x0001C1F8
		private void _signatureCheck(string responseSignature, string statusCode, string responseData, byte[] responseByte, ref NCMBException error)
		{
			StringBuilder stringBuilder = this._makeSignatureHashData();
			if (responseByte.Any<byte>() && responseData != "")
			{
				stringBuilder.Append("\n" + responseData);
			}
			else if (responseByte.Any<byte>())
			{
				stringBuilder.Append("\n" + NCMBConnection.AsHex(responseByte));
			}
			string b = this._makeSignature(stringBuilder.ToString());
			if (responseSignature != b)
			{
				responseData = "{}";
				error = new NCMBException();
				error.ErrorCode = "E100001";
				error.ErrorMessage = "Authentication error by response signature incorrect.";
			}
		}

		// Token: 0x0600049F RID: 1183 RVA: 0x0001E0A0 File Offset: 0x0001C2A0
		public static string AsHex(byte[] bytes)
		{
			StringBuilder stringBuilder = new StringBuilder(bytes.Length * 2);
			foreach (byte b in bytes)
			{
				if (b < 16)
				{
					stringBuilder.Append('0');
				}
				stringBuilder.Append(Convert.ToString(b, 16));
			}
			return stringBuilder.ToString();
		}

		// Token: 0x060004A0 RID: 1184 RVA: 0x0001E0F0 File Offset: 0x0001C2F0
		private UnityWebRequest _setUploadHandlerForFile(UnityWebRequest req)
		{
			string text = "\r\n";
			string text2 = "_NCMBBoundary";
			string text3 = "--" + text2 + text;
			byte[] bytes = Encoding.Default.GetBytes(text + "--" + text2 + "--");
			text3 = text3 + "Content-Disposition: form-data; name=\"file\"; filename=" + Uri.EscapeUriString(this._file.FileName) + text;
			text3 = string.Concat(new string[]
			{
				text3,
				"Content-Type: ",
				MimeTypeMap.GetMimeType(Path.GetExtension(this._file.FileName)),
				text,
				text
			});
			byte[] array = Encoding.Default.GetBytes(text3);
			if (this._file.FileData != null)
			{
				array = array.Concat(this._file.FileData).ToArray<byte>();
			}
			if (this._file.ACL != null && this._file.ACL._toJSONObject().Count > 0)
			{
				string str = Json.Serialize(this._file.ACL._toJSONObject());
				text3 = text + "--" + text2 + text;
				text3 = text3 + "Content-Disposition: form-data; name=acl; filename=acl" + text + text;
				text3 += str;
				byte[] bytes2 = Encoding.Default.GetBytes(text3);
				array = array.Concat(bytes2).ToArray<byte>();
			}
			array = array.Concat(bytes).ToArray<byte>();
			req.uploadHandler = new UploadHandlerRaw(array);
			return req;
		}

		// Token: 0x060004A1 RID: 1185 RVA: 0x0001E258 File Offset: 0x0001C458
		internal UnityWebRequest _returnRequest()
		{
			Uri uri = new Uri(this._url);
			this._url = uri.AbsoluteUri;
			string method = "";
			switch (this._method)
			{
			case ConnectType.GET:
				method = "GET";
				break;
			case ConnectType.POST:
				method = "POST";
				break;
			case ConnectType.PUT:
				method = "PUT";
				break;
			case ConnectType.DELETE:
				method = "DELETE";
				break;
			}
			UnityWebRequest unityWebRequest = new UnityWebRequest(this._url, method);
			this._makeTimeStamp();
			StringBuilder stringBuilder = this._makeSignatureHashData();
			string value = this._makeSignature(stringBuilder.ToString());
			if ((unityWebRequest.method.Equals("POST") && this._file != null) || (unityWebRequest.method.Equals("PUT") && this._file != null))
			{
				unityWebRequest.SetRequestHeader("Content-Type", "multipart/form-data; boundary=_NCMBBoundary");
			}
			else
			{
				unityWebRequest.SetRequestHeader("Content-Type", "application/json");
			}
			unityWebRequest.SetRequestHeader(NCMBConnection.HEADER_APPLICATION_KEY, this._applicationKey);
			unityWebRequest.SetRequestHeader(NCMBConnection.HEADER_SIGNATURE, value);
			unityWebRequest.SetRequestHeader(NCMBConnection.HEADER_TIMESTAMP_KEY, this._headerTimestamp);
			unityWebRequest.SetRequestHeader(NCMBConnection.HEADER_USER_AGENT_KEY, NCMBConnection.HEADER_USER_AGENT_VALUE);
			if (this._sessionToken != null && this._sessionToken != "")
			{
				unityWebRequest.SetRequestHeader(NCMBConnection.HEADER_SESSION_TOKEN, this._sessionToken);
			}
			if (unityWebRequest.GetRequestHeader("Content-Type").Equals("multipart/form-data; boundary=_NCMBBoundary"))
			{
				this._setUploadHandlerForFile(unityWebRequest);
			}
			else if ((unityWebRequest.method.Equals("POST") || unityWebRequest.method.Equals("PUT")) && this._content != null)
			{
				byte[] bytes = Encoding.UTF8.GetBytes(this._content);
				unityWebRequest.uploadHandler = new UploadHandlerRaw(bytes);
			}
			unityWebRequest.downloadHandler = new DownloadHandlerBuffer();
			return unityWebRequest;
		}

		// Token: 0x060004A2 RID: 1186 RVA: 0x0001E424 File Offset: 0x0001C624
		private StringBuilder _makeSignatureHashData()
		{
			StringBuilder stringBuilder = new StringBuilder();
			string text = this._url.Substring(this._domainUri.OriginalString.Length);
			string[] array = text.Split(new char[]
			{
				'?'
			});
			text = array[0];
			string text2 = null;
			if (array.Length > 1)
			{
				text2 = array[1];
			}
			Hashtable hashtable = new Hashtable();
			hashtable[NCMBConnection.SIGNATURE_METHOD_KEY] = NCMBConnection.SIGNATURE_METHOD_VALUE;
			hashtable[NCMBConnection.SIGNATURE_VERSION_KEY] = NCMBConnection.SIGNATURE_VERSION_VALUE;
			hashtable[NCMBConnection.HEADER_APPLICATION_KEY] = this._applicationKey;
			hashtable[NCMBConnection.HEADER_TIMESTAMP_KEY] = this._headerTimestamp;
			if (text2 != null && this._method == ConnectType.GET)
			{
				string[] array2 = text2.Split(new char[]
				{
					'&'
				});
				for (int i = 0; i < array2.Length; i++)
				{
					string[] array3 = array2[i].Split(new char[]
					{
						'='
					});
					hashtable[array3[0]] = array3[1];
				}
			}
			List<string> list = new List<string>();
			foreach (object obj in hashtable)
			{
				list.Add(((DictionaryEntry)obj).Key.ToString());
			}
			StringComparer ordinal = StringComparer.Ordinal;
			list.Sort(ordinal);
			stringBuilder.Append(this._method);
			stringBuilder.Append("\n");
			stringBuilder.Append(this._domainUri.Host);
			stringBuilder.Append("\n");
			stringBuilder.Append(text);
			stringBuilder.Append("\n");
			foreach (string text3 in list)
			{
				stringBuilder.Append(string.Concat(new object[]
				{
					text3,
					"=",
					hashtable[text3],
					"&"
				}));
			}
			stringBuilder.Remove(stringBuilder.Length - 1, 1);
			return stringBuilder;
		}

		// Token: 0x060004A3 RID: 1187 RVA: 0x0001E65C File Offset: 0x0001C85C
		private string _makeSignature(string stringData)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(this._clientKey);
			byte[] bytes2 = Encoding.UTF8.GetBytes(stringData);
			return Convert.ToBase64String(new HMACSHA256
			{
				Key = bytes
			}.ComputeHash(bytes2));
		}

		// Token: 0x060004A4 RID: 1188 RVA: 0x0001E6A0 File Offset: 0x0001C8A0
		private void _makeTimeStamp()
		{
			string text = DateTime.UtcNow.ToString("yyyy-MM-dd'T'HH:mm:ss.fff'Z'");
			this._headerTimestamp = text.Replace(":", "%3A");
		}

		// Token: 0x060004A5 RID: 1189 RVA: 0x0001E6D6 File Offset: 0x0001C8D6
		internal void _checkInvalidSessionToken(string code)
		{
			if (NCMBException.INCORRECT_HEADER.Equals(code) && this._sessionToken != null && this._sessionToken.Equals(NCMBUser._getCurrentSessionToken()))
			{
				NCMBUser._logOutEvent();
			}
		}

		// Token: 0x060004A6 RID: 1190 RVA: 0x0001E704 File Offset: 0x0001C904
		internal void _checkResponseSignature(string code, string responseData, UnityWebRequest req, ref NCMBException error)
		{
			if (NCMBSettings._responseValidationFlag && req.error == null && error == null && req.GetResponseHeader(NCMBConnection.RESPONSE_SIGNATURE) != null)
			{
				string responseSignature = req.GetResponseHeader(NCMBConnection.RESPONSE_SIGNATURE).ToString();
				string text = responseData;
				if (text != null)
				{
					text = NCMBUtility.unicodeUnescape(text);
				}
				this._signatureCheck(responseSignature, code, text, req.downloadHandler.data, ref error);
			}
		}

		// Token: 0x060004A7 RID: 1191 RVA: 0x0001E765 File Offset: 0x0001C965
		internal static IEnumerator SendRequest(NCMBConnection connection, UnityWebRequest req, object callback)
		{
			NCMBException error = null;
			byte[] byteData = new byte[32768];
			string json = "";
			req.SendWebRequest();
			float elapsedTime = 0f;
			float waitTime = 0.2f;
			while (!req.isDone)
			{
				elapsedTime += waitTime;
				if (elapsedTime >= (float)NCMBConnection.REQUEST_TIME_OUT)
				{
					req.Abort();
					error = new NCMBException();
					break;
				}
				yield return new WaitForSeconds(waitTime);
			}
			if (error != null)
			{
				error.ErrorCode = "408";
				error.ErrorMessage = "Request Timeout.";
			}
			else if (req.isNetworkError)
			{
				error = new NCMBException();
				error.ErrorCode = req.responseCode.ToString();
				error.ErrorMessage = req.error;
			}
			else if (req.responseCode != 200L && req.responseCode != 201L)
			{
				error = new NCMBException();
				Dictionary<string, object> dictionary = Json.Deserialize(req.downloadHandler.text) as Dictionary<string, object>;
				error.ErrorCode = dictionary["code"].ToString();
				error.ErrorMessage = dictionary["error"].ToString();
			}
			else
			{
				byteData = req.downloadHandler.data;
				json = req.downloadHandler.text;
			}
			if (error != null)
			{
				connection._checkInvalidSessionToken(error.ErrorCode);
			}
			if (callback != null && !(callback is NCMBExecuteScriptCallback))
			{
				string code = req.responseCode.ToString();
				string responseData = req.downloadHandler.text;
				if (callback is HttpClientFileDataCallback)
				{
					responseData = "";
				}
				connection._checkResponseSignature(code, responseData, req, ref error);
			}
			if (callback != null)
			{
				if (callback is NCMBExecuteScriptCallback)
				{
					((NCMBExecuteScriptCallback)callback)(byteData, error);
				}
				else if (callback is HttpClientCallback)
				{
					((HttpClientCallback)callback)((int)req.responseCode, json, error);
				}
				else if (callback is HttpClientFileDataCallback)
				{
					((HttpClientFileDataCallback)callback)((int)req.responseCode, byteData, error);
				}
			}
			yield break;
		}

		// Token: 0x060004A8 RID: 1192 RVA: 0x0001E784 File Offset: 0x0001C984
		// Note: this type is marked as 'beforefieldinit'.
		static NCMBConnection()
		{
		}

		// Token: 0x0400017C RID: 380
		private static readonly string RESPONSE_SIGNATURE = "X-NCMB-Response-Signature";

		// Token: 0x0400017D RID: 381
		private static readonly string SIGNATURE_METHOD_KEY = "SignatureMethod";

		// Token: 0x0400017E RID: 382
		private static readonly string SIGNATURE_METHOD_VALUE = "HmacSHA256";

		// Token: 0x0400017F RID: 383
		private static readonly string SIGNATURE_VERSION_KEY = "SignatureVersion";

		// Token: 0x04000180 RID: 384
		private static readonly string SIGNATURE_VERSION_VALUE = "2";

		// Token: 0x04000181 RID: 385
		private static readonly string HEADER_SIGNATURE = "X-NCMB-Signature";

		// Token: 0x04000182 RID: 386
		private static readonly string HEADER_APPLICATION_KEY = "X-NCMB-Application-Key";

		// Token: 0x04000183 RID: 387
		private static readonly string HEADER_TIMESTAMP_KEY = "X-NCMB-Timestamp";

		// Token: 0x04000184 RID: 388
		private static readonly string HEADER_SESSION_TOKEN = "X-NCMB-Apps-Session-Token";

		// Token: 0x04000185 RID: 389
		private static readonly string HEADER_USER_AGENT_KEY = "X-NCMB-SDK-Version";

		// Token: 0x04000186 RID: 390
		private static readonly string HEADER_USER_AGENT_VALUE = "unity-" + CommonConstant.SDK_VERSION;

		// Token: 0x04000187 RID: 391
		private static readonly int REQUEST_TIME_OUT = 10;

		// Token: 0x04000188 RID: 392
		private string _applicationKey = "";

		// Token: 0x04000189 RID: 393
		private string _clientKey = "";

		// Token: 0x0400018A RID: 394
		private string _headerTimestamp = "";

		// Token: 0x0400018B RID: 395
		private ConnectType _method;

		// Token: 0x0400018C RID: 396
		private string _url = "";

		// Token: 0x0400018D RID: 397
		private string _content = "";

		// Token: 0x0400018E RID: 398
		private string _sessionToken = "";

		// Token: 0x0400018F RID: 399
		private Uri _domainUri;

		// Token: 0x04000190 RID: 400
		private NCMBFile _file;

		// Token: 0x04000191 RID: 401
		internal UnityWebRequest _request;

		// Token: 0x02000158 RID: 344
		[CompilerGenerated]
		private sealed class <SendRequest>d__37 : IEnumerator<object>, IEnumerator, IDisposable
		{
			// Token: 0x06000A2E RID: 2606 RVA: 0x00030DF2 File Offset: 0x0002EFF2
			[DebuggerHidden]
			public <SendRequest>d__37(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x06000A2F RID: 2607 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06000A30 RID: 2608 RVA: 0x00030E04 File Offset: 0x0002F004
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
				}
				else
				{
					this.<>1__state = -1;
					error = null;
					byteData = new byte[32768];
					json = "";
					req.SendWebRequest();
					elapsedTime = 0f;
					waitTime = 0.2f;
				}
				if (!req.isDone)
				{
					elapsedTime += waitTime;
					if (elapsedTime < (float)NCMBConnection.REQUEST_TIME_OUT)
					{
						this.<>2__current = new WaitForSeconds(waitTime);
						this.<>1__state = 1;
						return true;
					}
					req.Abort();
					error = new NCMBException();
				}
				if (error != null)
				{
					error.ErrorCode = "408";
					error.ErrorMessage = "Request Timeout.";
				}
				else if (req.isNetworkError)
				{
					error = new NCMBException();
					error.ErrorCode = req.responseCode.ToString();
					error.ErrorMessage = req.error;
				}
				else if (req.responseCode != 200L && req.responseCode != 201L)
				{
					error = new NCMBException();
					Dictionary<string, object> dictionary = Json.Deserialize(req.downloadHandler.text) as Dictionary<string, object>;
					error.ErrorCode = dictionary["code"].ToString();
					error.ErrorMessage = dictionary["error"].ToString();
				}
				else
				{
					byteData = req.downloadHandler.data;
					json = req.downloadHandler.text;
				}
				if (error != null)
				{
					connection._checkInvalidSessionToken(error.ErrorCode);
				}
				if (callback != null && !(callback is NCMBExecuteScriptCallback))
				{
					string code = req.responseCode.ToString();
					string responseData = req.downloadHandler.text;
					if (callback is HttpClientFileDataCallback)
					{
						responseData = "";
					}
					connection._checkResponseSignature(code, responseData, req, ref error);
				}
				if (callback != null)
				{
					if (callback is NCMBExecuteScriptCallback)
					{
						((NCMBExecuteScriptCallback)callback)(byteData, error);
					}
					else if (callback is HttpClientCallback)
					{
						((HttpClientCallback)callback)((int)req.responseCode, json, error);
					}
					else if (callback is HttpClientFileDataCallback)
					{
						((HttpClientFileDataCallback)callback)((int)req.responseCode, byteData, error);
					}
				}
				return false;
			}

			// Token: 0x1700015E RID: 350
			// (get) Token: 0x06000A31 RID: 2609 RVA: 0x00031136 File Offset: 0x0002F336
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000A32 RID: 2610 RVA: 0x00026A5A File Offset: 0x00024C5A
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700015F RID: 351
			// (get) Token: 0x06000A33 RID: 2611 RVA: 0x00031136 File Offset: 0x0002F336
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0400051F RID: 1311
			private int <>1__state;

			// Token: 0x04000520 RID: 1312
			private object <>2__current;

			// Token: 0x04000521 RID: 1313
			public UnityWebRequest req;

			// Token: 0x04000522 RID: 1314
			public NCMBConnection connection;

			// Token: 0x04000523 RID: 1315
			public object callback;

			// Token: 0x04000524 RID: 1316
			private NCMBException <error>5__2;

			// Token: 0x04000525 RID: 1317
			private byte[] <byteData>5__3;

			// Token: 0x04000526 RID: 1318
			private string <json>5__4;

			// Token: 0x04000527 RID: 1319
			private float <elapsedTime>5__5;

			// Token: 0x04000528 RID: 1320
			private float <waitTime>5__6;
		}
	}
}
