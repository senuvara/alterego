﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace NCMB.Internal
{
	// Token: 0x0200006F RID: 111
	internal class NCMBRemoveOperation : INCMBFieldOperation
	{
		// Token: 0x060004C3 RID: 1219 RVA: 0x0001F088 File Offset: 0x0001D288
		public NCMBRemoveOperation(object values)
		{
			if (values is IEnumerable)
			{
				foreach (object value in ((IEnumerable)values))
				{
					this.objects.Add(value);
				}
				return;
			}
			this.objects.Add(values);
		}

		// Token: 0x060004C4 RID: 1220 RVA: 0x0001F0E6 File Offset: 0x0001D2E6
		public object Encode()
		{
			return new Dictionary<string, object>
			{
				{
					"__op",
					"Remove"
				},
				{
					"objects",
					NCMBUtility._maybeEncodeJSONObject(new ArrayList(this.objects), true)
				}
			};
		}

		// Token: 0x060004C5 RID: 1221 RVA: 0x0001F11C File Offset: 0x0001D31C
		public INCMBFieldOperation MergeWithPrevious(INCMBFieldOperation previous)
		{
			if (previous == null)
			{
				return this;
			}
			if (previous is NCMBDeleteOperation)
			{
				return new NCMBSetOperation(new ArrayList());
			}
			if (previous is NCMBSetOperation)
			{
				object value = ((NCMBSetOperation)previous).getValue();
				if (value is IList)
				{
					return new NCMBSetOperation(this.Apply(value, null, null));
				}
				throw new InvalidOperationException("You can only add an item to a List.");
			}
			else
			{
				if (previous is NCMBRemoveOperation)
				{
					ArrayList arrayList = new ArrayList(((NCMBRemoveOperation)previous).objects);
					foreach (object value2 in this.objects)
					{
						arrayList.Add(value2);
					}
					return new NCMBRemoveOperation(arrayList);
				}
				throw new InvalidOperationException("Operation is invalid after previous operation.");
			}
		}

		// Token: 0x060004C6 RID: 1222 RVA: 0x0001F1EC File Offset: 0x0001D3EC
		public object Apply(object oldValue, NCMBObject obj, string key)
		{
			if (oldValue == null)
			{
				return new ArrayList();
			}
			if (oldValue is IList)
			{
				ArrayList arrayList = new ArrayList((IList)oldValue);
				foreach (object obj2 in this.objects)
				{
					while (arrayList.Contains(obj2))
					{
						arrayList.Remove(obj2);
					}
				}
				ArrayList arrayList2 = new ArrayList(this.objects);
				foreach (object obj3 in arrayList)
				{
					while (arrayList2.Contains(obj3))
					{
						arrayList2.Remove(obj3);
					}
				}
				HashSet<object> hashSet = new HashSet<object>();
				foreach (object obj4 in arrayList2)
				{
					if (obj4 is NCMBObject)
					{
						NCMBObject ncmbobject = (NCMBObject)obj4;
						hashSet.Add(ncmbobject.ObjectId);
					}
					for (int i = 0; i < arrayList.Count; i++)
					{
						object obj5 = arrayList[i];
						if (obj5 is NCMBObject)
						{
							NCMBObject ncmbobject2 = (NCMBObject)obj5;
							if (hashSet.Contains(ncmbobject2.ObjectId))
							{
								arrayList.RemoveAt(i);
							}
						}
					}
				}
				return arrayList;
			}
			throw new InvalidOperationException("Operation is invalid after previous operation.");
		}

		// Token: 0x04000197 RID: 407
		private ArrayList objects = new ArrayList();
	}
}
