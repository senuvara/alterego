﻿using System;

namespace NCMB.Internal
{
	// Token: 0x0200006B RID: 107
	internal class NCMBDeleteOperation : INCMBFieldOperation
	{
		// Token: 0x060004B0 RID: 1200 RVA: 0x000043CE File Offset: 0x000025CE
		public NCMBDeleteOperation()
		{
		}

		// Token: 0x060004B1 RID: 1201 RVA: 0x0001E9D4 File Offset: 0x0001CBD4
		public object getValue()
		{
			return null;
		}

		// Token: 0x060004B2 RID: 1202 RVA: 0x0001E9D4 File Offset: 0x0001CBD4
		public object Encode()
		{
			return null;
		}

		// Token: 0x060004B3 RID: 1203 RVA: 0x0001DB27 File Offset: 0x0001BD27
		public INCMBFieldOperation MergeWithPrevious(INCMBFieldOperation previous)
		{
			return this;
		}

		// Token: 0x060004B4 RID: 1204 RVA: 0x0001E9D4 File Offset: 0x0001CBD4
		public object Apply(object oldValue, NCMBObject obj, string key)
		{
			return null;
		}
	}
}
