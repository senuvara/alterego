﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace NCMB.Internal
{
	// Token: 0x0200006A RID: 106
	internal static class NCMBDebug
	{
		// Token: 0x060004A9 RID: 1193 RVA: 0x0001E810 File Offset: 0x0001CA10
		[Conditional("DEBUG")]
		public static void Log(string message)
		{
			UnityEngine.Debug.Log(message);
		}

		// Token: 0x060004AA RID: 1194 RVA: 0x0001E818 File Offset: 0x0001CA18
		[Conditional("DEBUG")]
		public static void LogWarning(string message)
		{
			UnityEngine.Debug.LogWarning(message);
		}

		// Token: 0x060004AB RID: 1195 RVA: 0x0001E820 File Offset: 0x0001CA20
		[Conditional("DEBUG")]
		public static void LogError(string message)
		{
			UnityEngine.Debug.LogError(message);
		}

		// Token: 0x060004AC RID: 1196 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[Conditional("DEBUG")]
		public static void LogError(object message, object context)
		{
		}

		// Token: 0x060004AD RID: 1197 RVA: 0x0001E828 File Offset: 0x0001CA28
		[Conditional("DEBUG")]
		public static void List(string title, IList list)
		{
			string text = null;
			text += string.Format(title + NCMBDebug.newLine, Array.Empty<object>());
			for (int i = 0; i < list.Count; i++)
			{
				text += string.Format(string.Concat(new object[]
				{
					"【",
					i,
					"】",
					list[i].ToString(),
					"{0}"
				}), (i < list.Count - 1) ? NCMBDebug.newLine : "");
			}
			UnityEngine.Debug.Log(text);
		}

		// Token: 0x060004AE RID: 1198 RVA: 0x0001E8C8 File Offset: 0x0001CAC8
		[Conditional("DEBUG")]
		public static void Dictionary<T, K>(string title, Dictionary<T, K> dictionary)
		{
			int num = 0;
			string text = null;
			text += string.Format(title + NCMBDebug.newLine, Array.Empty<object>());
			foreach (KeyValuePair<T, K> keyValuePair in dictionary)
			{
				string str = text;
				object[] array = new object[7];
				array[0] = "【";
				array[1] = num;
				array[2] = "】 Key : ";
				int num2 = 3;
				T key = keyValuePair.Key;
				array[num2] = key.ToString();
				array[4] = " Value : ";
				int num3 = 5;
				K value = keyValuePair.Value;
				array[num3] = value.ToString();
				array[6] = "{0}";
				text = str + string.Format(string.Concat(array), (num < dictionary.Count - 1) ? NCMBDebug.newLine : "");
				num++;
			}
			UnityEngine.Debug.Log(text);
		}

		// Token: 0x060004AF RID: 1199 RVA: 0x0001E9C8 File Offset: 0x0001CBC8
		// Note: this type is marked as 'beforefieldinit'.
		static NCMBDebug()
		{
		}

		// Token: 0x04000192 RID: 402
		private static string newLine = "\n";
	}
}
