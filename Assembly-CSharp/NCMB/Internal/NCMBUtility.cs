﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;

namespace NCMB.Internal
{
	// Token: 0x02000071 RID: 113
	internal static class NCMBUtility
	{
		// Token: 0x060004CC RID: 1228 RVA: 0x0001F3AC File Offset: 0x0001D5AC
		internal static string GetClassName(object t)
		{
			string className;
			try
			{
				NCMBClassNameAttribute ncmbclassNameAttribute = (NCMBClassNameAttribute)t.GetType().GetCustomAttributes(true)[0];
				if (ncmbclassNameAttribute == null || ncmbclassNameAttribute.ClassName == null)
				{
					throw new NCMBException(new ArgumentException("No ClassName attribute specified on the given subclass."));
				}
				className = ncmbclassNameAttribute.ClassName;
			}
			catch (Exception error)
			{
				throw new NCMBException(error);
			}
			return className;
		}

		// Token: 0x060004CD RID: 1229 RVA: 0x0001F408 File Offset: 0x0001D608
		internal static void CopyDictionary(IDictionary<string, object> listSouce, IDictionary<string, object> listDestination)
		{
			try
			{
				foreach (KeyValuePair<string, object> keyValuePair in listSouce)
				{
					listDestination[keyValuePair.Key] = keyValuePair.Value;
				}
			}
			catch (Exception error)
			{
				throw new NCMBException(error);
			}
		}

		// Token: 0x060004CE RID: 1230 RVA: 0x0001F474 File Offset: 0x0001D674
		private static IDictionary<string, object> _encodeJSONObject(object value, bool allowNCMBObjects)
		{
			if (value is DateTime)
			{
				DateTime dateTime = (DateTime)value;
				Dictionary<string, object> dictionary = new Dictionary<string, object>();
				string value2 = dateTime.ToString("yyyy-MM-dd'T'HH:mm:ss.fff'Z'");
				dictionary.Add("__type", "Date");
				dictionary.Add("iso", value2);
				return dictionary;
			}
			if (value is NCMBObject)
			{
				NCMBObject ncmbobject = (NCMBObject)value;
				if (!allowNCMBObjects)
				{
					throw new ArgumentException("NCMBObjects not allowed here.");
				}
				if (ncmbobject.ObjectId == null)
				{
					throw new ArgumentException("Cannot create a pointer to an object without an objectId.");
				}
				return new Dictionary<string, object>
				{
					{
						"__type",
						"Pointer"
					},
					{
						"className",
						ncmbobject.ClassName
					},
					{
						"objectId",
						ncmbobject.ObjectId
					}
				};
			}
			else
			{
				if (value is NCMBGeoPoint)
				{
					NCMBGeoPoint ncmbgeoPoint = (NCMBGeoPoint)value;
					return new Dictionary<string, object>
					{
						{
							"__type",
							"GeoPoint"
						},
						{
							"latitude",
							ncmbgeoPoint.Latitude
						},
						{
							"longitude",
							ncmbgeoPoint.Longitude
						}
					};
				}
				if (value is IDictionary)
				{
					Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
					IDictionary dictionary3 = (IDictionary)value;
					foreach (object obj in dictionary3.Keys)
					{
						if (!(obj is string))
						{
							throw new NCMBException(new ArgumentException("Invalid type for key: " + obj.GetType().ToString() + ".key type string only."));
						}
						dictionary2[(string)obj] = NCMBUtility._maybeEncodeJSONObject(dictionary3[obj], allowNCMBObjects);
					}
					return dictionary2;
				}
				if (value is NCMBRelation<NCMBObject>)
				{
					return ((NCMBRelation<NCMBObject>)value)._encodeToJSON();
				}
				if (value is NCMBACL)
				{
					return ((NCMBACL)value)._toJSONObject();
				}
				return null;
			}
		}

		// Token: 0x060004CF RID: 1231 RVA: 0x0001F658 File Offset: 0x0001D858
		internal static List<object> _encodeAsJSONArray(IList list, bool allowNCMBObjects)
		{
			List<object> list2 = new List<object>();
			foreach (object obj in list)
			{
				if (!NCMBObject._isValidType(obj))
				{
					throw new NCMBException(new ArgumentException("invalid type for value in array: " + obj.GetType().ToString()));
				}
				list2.Add(NCMBUtility._maybeEncodeJSONObject(obj, allowNCMBObjects));
			}
			return list2;
		}

		// Token: 0x060004D0 RID: 1232 RVA: 0x0001F6DC File Offset: 0x0001D8DC
		internal static object _maybeEncodeJSONObject(object value, bool allowNCMBObjects)
		{
			if (value is INCMBFieldOperation)
			{
				return ((INCMBFieldOperation)value).Encode();
			}
			if (value is IList)
			{
				return NCMBUtility._encodeAsJSONArray((IList)value, allowNCMBObjects);
			}
			IDictionary<string, object> dictionary = NCMBUtility._encodeJSONObject(value, allowNCMBObjects);
			if (dictionary != null)
			{
				return dictionary;
			}
			return value;
		}

		// Token: 0x060004D1 RID: 1233 RVA: 0x0001F720 File Offset: 0x0001D920
		internal static object decodeJSONObject(object jsonDicParameter)
		{
			if (jsonDicParameter is IList)
			{
				ArrayList arrayList = new ArrayList();
				List<object> list = new List<object>();
				list = (List<object>)jsonDicParameter;
				for (int i = 0; i < list.Count; i++)
				{
					if (NCMBUtility.decodeJSONObject(list[i]) != null)
					{
						arrayList.Add(NCMBUtility.decodeJSONObject(list[i]));
					}
					else
					{
						arrayList.Add(list[i]);
					}
				}
				return arrayList;
			}
			if (!(jsonDicParameter is IDictionary))
			{
				return null;
			}
			Dictionary<string, object> dictionary = (Dictionary<string, object>)jsonDicParameter;
			object obj;
			dictionary.TryGetValue("__type", out obj);
			if (obj == null)
			{
				Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
				foreach (KeyValuePair<string, object> keyValuePair in dictionary)
				{
					object obj2 = NCMBUtility.decodeJSONObject(keyValuePair.Value);
					if (obj2 != null)
					{
						dictionary2.Add(keyValuePair.Key, obj2);
					}
					else
					{
						dictionary2.Add(keyValuePair.Key, keyValuePair.Value);
					}
				}
				return dictionary2;
			}
			if (obj.Equals("Date"))
			{
				object obj3;
				dictionary.TryGetValue("iso", out obj3);
				return NCMBUtility.parseDate((string)obj3);
			}
			if (obj.Equals("Pointer"))
			{
				object obj4;
				dictionary.TryGetValue("className", out obj4);
				object obj5;
				dictionary.TryGetValue("objectId", out obj5);
				return NCMBObject.CreateWithoutData((string)obj4, (string)obj5);
			}
			if (obj.Equals("GeoPoint"))
			{
				double latitude = 0.0;
				double longitude = 0.0;
				try
				{
					object value;
					dictionary.TryGetValue("latitude", out value);
					latitude = Convert.ToDouble(value);
					object value2;
					dictionary.TryGetValue("longitude", out value2);
					longitude = Convert.ToDouble(value2);
				}
				catch (Exception error)
				{
					throw new NCMBException(error);
				}
				return new NCMBGeoPoint(latitude, longitude);
			}
			if (obj.Equals("Object"))
			{
				object obj6;
				dictionary.TryGetValue("className", out obj6);
				NCMBObject ncmbobject = NCMBObject.CreateWithoutData((string)obj6, null);
				ncmbobject._handleFetchResult(true, dictionary);
				return ncmbobject;
			}
			if (!obj.Equals("Relation"))
			{
				return null;
			}
			if (dictionary["className"].Equals("user"))
			{
				return new NCMBRelation<NCMBUser>((string)dictionary["className"]);
			}
			if (dictionary["className"].Equals("role"))
			{
				return new NCMBRelation<NCMBRole>((string)dictionary["className"]);
			}
			return new NCMBRelation<NCMBObject>((string)dictionary["className"]);
		}

		// Token: 0x060004D2 RID: 1234 RVA: 0x0001F9C4 File Offset: 0x0001DBC4
		internal static DateTime parseDate(string dateString)
		{
			string format = "yyyy-MM-dd'T'HH:mm:ss.fff'Z'";
			return DateTime.ParseExact(dateString, format, null);
		}

		// Token: 0x060004D3 RID: 1235 RVA: 0x0001F9DF File Offset: 0x0001DBDF
		internal static string encodeDate(DateTime dateObject)
		{
			return dateObject.ToString("yyyy-MM-dd'T'HH:mm:ss.fff'Z'");
		}

		// Token: 0x060004D4 RID: 1236 RVA: 0x0001F9ED File Offset: 0x0001DBED
		private static bool isContainerObject(object object1)
		{
			return object1 is NCMBGeoPoint || object1 is IDictionary;
		}

		// Token: 0x060004D5 RID: 1237 RVA: 0x0001FA04 File Offset: 0x0001DC04
		internal static string _encodeString(string str)
		{
			StringBuilder stringBuilder = new StringBuilder();
			char[] array = str.ToCharArray();
			int i = 0;
			while (i < array.Length)
			{
				char c = array[i];
				switch (c)
				{
				case '\b':
					stringBuilder.Append("\\b");
					break;
				case '\t':
					stringBuilder.Append("\\t");
					break;
				case '\n':
					stringBuilder.Append("\\n");
					break;
				case '\v':
					goto IL_AC;
				case '\f':
					stringBuilder.Append("\\f");
					break;
				case '\r':
					stringBuilder.Append("\\r");
					break;
				default:
					if (c != '"')
					{
						if (c != '\\')
						{
							goto IL_AC;
						}
						stringBuilder.Append("\\\\");
					}
					else
					{
						stringBuilder.Append("\\\"");
					}
					break;
				}
				IL_E9:
				i++;
				continue;
				IL_AC:
				int num = Convert.ToInt32(c);
				if (num >= 32 && num <= 126)
				{
					stringBuilder.Append(c);
					goto IL_E9;
				}
				stringBuilder.Append("\\u");
				stringBuilder.Append(num.ToString("x4"));
				goto IL_E9;
			}
			return stringBuilder.ToString();
		}

		// Token: 0x060004D6 RID: 1238 RVA: 0x0001FB0D File Offset: 0x0001DD0D
		internal static string unicodeUnescape(string targetText)
		{
			return Regex.Replace(targetText, "\\\\[Uu]([0-9A-Fa-f]{4})", (Match x) => ((char)ushort.Parse(x.Groups[1].Value, NumberStyles.AllowHexSpecifier)).ToString()).ToString();
		}

		// Token: 0x02000159 RID: 345
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000A34 RID: 2612 RVA: 0x0003113E File Offset: 0x0002F33E
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000A35 RID: 2613 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c()
			{
			}

			// Token: 0x06000A36 RID: 2614 RVA: 0x0003114C File Offset: 0x0002F34C
			internal string <unicodeUnescape>b__10_0(Match x)
			{
				return ((char)ushort.Parse(x.Groups[1].Value, NumberStyles.AllowHexSpecifier)).ToString();
			}

			// Token: 0x04000529 RID: 1321
			public static readonly NCMBUtility.<>c <>9 = new NCMBUtility.<>c();

			// Token: 0x0400052A RID: 1322
			public static MatchEvaluator <>9__10_0;
		}
	}
}
