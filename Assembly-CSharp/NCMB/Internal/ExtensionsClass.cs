﻿using System;

namespace NCMB.Internal
{
	// Token: 0x02000065 RID: 101
	internal static class ExtensionsClass
	{
		// Token: 0x0600048B RID: 1163 RVA: 0x0001DB27 File Offset: 0x0001BD27
		public static Type GetTypeInfo(this Type t)
		{
			return t;
		}

		// Token: 0x0600048C RID: 1164 RVA: 0x0001DB2A File Offset: 0x0001BD2A
		internal static bool IsPrimitive(this Type type)
		{
			return type.GetTypeInfo().IsPrimitive;
		}
	}
}
