﻿using System;

namespace NCMB.Internal
{
	// Token: 0x02000070 RID: 112
	internal class NCMBSetOperation : INCMBFieldOperation
	{
		// Token: 0x060004C7 RID: 1223 RVA: 0x0001F384 File Offset: 0x0001D584
		public NCMBSetOperation(object value)
		{
			this.Value = value;
		}

		// Token: 0x060004C8 RID: 1224 RVA: 0x0001F393 File Offset: 0x0001D593
		public object getValue()
		{
			return this.Value;
		}

		// Token: 0x060004C9 RID: 1225 RVA: 0x0001F39B File Offset: 0x0001D59B
		public object Encode()
		{
			return NCMBUtility._maybeEncodeJSONObject(this.Value, true);
		}

		// Token: 0x060004CA RID: 1226 RVA: 0x0001DB27 File Offset: 0x0001BD27
		public INCMBFieldOperation MergeWithPrevious(INCMBFieldOperation previous)
		{
			return this;
		}

		// Token: 0x060004CB RID: 1227 RVA: 0x0001F393 File Offset: 0x0001D593
		public object Apply(object oldValue, NCMBObject obj, string key)
		{
			return this.Value;
		}

		// Token: 0x04000198 RID: 408
		public object Value;
	}
}
