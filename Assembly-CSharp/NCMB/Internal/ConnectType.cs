﻿using System;

namespace NCMB.Internal
{
	// Token: 0x02000063 RID: 99
	internal enum ConnectType
	{
		// Token: 0x04000171 RID: 369
		GET,
		// Token: 0x04000172 RID: 370
		POST,
		// Token: 0x04000173 RID: 371
		PUT,
		// Token: 0x04000174 RID: 372
		DELETE
	}
}
