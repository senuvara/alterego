﻿using System;
using System.Collections.Generic;

namespace NCMB.Internal
{
	// Token: 0x0200006D RID: 109
	internal class NCMBIncrementOperation : INCMBFieldOperation
	{
		// Token: 0x060004B8 RID: 1208 RVA: 0x0001E9D7 File Offset: 0x0001CBD7
		public NCMBIncrementOperation(object amount)
		{
			this.amount = amount;
		}

		// Token: 0x060004B9 RID: 1209 RVA: 0x0001E9E6 File Offset: 0x0001CBE6
		public object Encode()
		{
			return new Dictionary<string, object>
			{
				{
					"__op",
					"Increment"
				},
				{
					"amount",
					this.amount
				}
			};
		}

		// Token: 0x060004BA RID: 1210 RVA: 0x0001EA10 File Offset: 0x0001CC10
		public INCMBFieldOperation MergeWithPrevious(INCMBFieldOperation previous)
		{
			if (previous == null)
			{
				return this;
			}
			if (previous is NCMBDeleteOperation)
			{
				return new NCMBSetOperation(this.amount);
			}
			if (previous is NCMBSetOperation)
			{
				object value = ((NCMBSetOperation)previous).getValue();
				if (value is string || value == null)
				{
					throw new InvalidOperationException("You cannot increment a non-number.");
				}
				return new NCMBSetOperation(NCMBObject._addNumbers(value, this.amount));
			}
			else
			{
				if (previous is NCMBIncrementOperation)
				{
					return new NCMBIncrementOperation(NCMBObject._addNumbers(((NCMBIncrementOperation)previous).amount, this.amount));
				}
				throw new InvalidOperationException("Operation is invalid after previous operation.");
			}
		}

		// Token: 0x060004BB RID: 1211 RVA: 0x0001EAA0 File Offset: 0x0001CCA0
		public object Apply(object oldValue, NCMBObject obj, string key)
		{
			if (oldValue == null)
			{
				return this.amount;
			}
			if (oldValue is string || oldValue == null)
			{
				throw new InvalidOperationException("You cannot increment a non-number.");
			}
			return NCMBObject._addNumbers(oldValue, this.amount);
		}

		// Token: 0x04000193 RID: 403
		private object amount;
	}
}
