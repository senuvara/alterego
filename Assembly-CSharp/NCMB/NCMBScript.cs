﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Security;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using MiniJSON;
using NCMB.Internal;
using SimpleJSON;
using UnityEngine;

namespace NCMB
{
	// Token: 0x02000060 RID: 96
	public class NCMBScript
	{
		// Token: 0x17000062 RID: 98
		// (get) Token: 0x06000445 RID: 1093 RVA: 0x0001CCBA File Offset: 0x0001AEBA
		// (set) Token: 0x06000446 RID: 1094 RVA: 0x0001CCC2 File Offset: 0x0001AEC2
		public string ScriptName
		{
			get
			{
				return this._scriptName;
			}
			set
			{
				this._scriptName = value;
			}
		}

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x06000447 RID: 1095 RVA: 0x0001CCCB File Offset: 0x0001AECB
		// (set) Token: 0x06000448 RID: 1096 RVA: 0x0001CCD3 File Offset: 0x0001AED3
		public NCMBScript.MethodType Method
		{
			get
			{
				return this._method;
			}
			set
			{
				this._method = value;
			}
		}

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x06000449 RID: 1097 RVA: 0x0001CCDC File Offset: 0x0001AEDC
		// (set) Token: 0x0600044A RID: 1098 RVA: 0x0001CCE4 File Offset: 0x0001AEE4
		public string BaseUrl
		{
			get
			{
				return this._baseUrl;
			}
			set
			{
				this._baseUrl = value;
			}
		}

		// Token: 0x0600044B RID: 1099 RVA: 0x0001CCED File Offset: 0x0001AEED
		public NCMBScript(string scriptName, NCMBScript.MethodType method) : this(scriptName, method, NCMBScript.DEFAULT_SCRIPT_ENDPOINT)
		{
		}

		// Token: 0x0600044C RID: 1100 RVA: 0x0001CCFC File Offset: 0x0001AEFC
		public NCMBScript(string scriptName, NCMBScript.MethodType method, string baseUrl)
		{
			this._scriptName = scriptName;
			this._method = method;
			this._baseUrl = baseUrl;
		}

		// Token: 0x0600044D RID: 1101 RVA: 0x0001CD1C File Offset: 0x0001AF1C
		public void ExecuteAsync(IDictionary<string, object> header, IDictionary<string, object> body, IDictionary<string, object> query, NCMBExecuteScriptCallback callback)
		{
			string domain = NCMBScript.DEFAULT_SCRIPT_ENDPOINT;
			string text = string.Concat(new string[]
			{
				NCMBScript.DEFAULT_SCRIPT_ENDPOINT,
				"/",
				NCMBScript.DEFAULT_SCRIPT_API_VERSION,
				"/",
				NCMBScript.SERVICE_PATH,
				"/",
				this._scriptName
			});
			if (this._baseUrl == null || this._baseUrl.Length == 0)
			{
				throw new ArgumentException("Invalid baseUrl.");
			}
			if (!this._baseUrl.Equals(NCMBScript.DEFAULT_SCRIPT_ENDPOINT))
			{
				domain = this._baseUrl;
				text = this._baseUrl + "/" + this._scriptName;
			}
			ConnectType method;
			switch (this._method)
			{
			case NCMBScript.MethodType.POST:
				method = ConnectType.POST;
				break;
			case NCMBScript.MethodType.PUT:
				method = ConnectType.PUT;
				break;
			case NCMBScript.MethodType.GET:
				method = ConnectType.GET;
				break;
			case NCMBScript.MethodType.DELETE:
				method = ConnectType.DELETE;
				break;
			default:
				throw new ArgumentException("Invalid methodType.");
			}
			string content = null;
			if (body != null)
			{
				content = MiniJSON.Json.Serialize(body);
			}
			string text2 = "?";
			if (query != null && query.Count > 0)
			{
				int num = query.Count;
				foreach (KeyValuePair<string, object> keyValuePair in query)
				{
					string stringToEscape;
					if (keyValuePair.Value is IList || keyValuePair.Value is IDictionary)
					{
						stringToEscape = SimpleJSON.Json.Serialize(keyValuePair.Value);
					}
					else if (keyValuePair.Value is DateTime)
					{
						stringToEscape = NCMBUtility.encodeDate((DateTime)keyValuePair.Value);
					}
					else
					{
						stringToEscape = keyValuePair.Value.ToString();
					}
					text2 = text2 + keyValuePair.Key + "=" + Uri.EscapeDataString(stringToEscape);
					if (num > 1)
					{
						text2 += "&";
						num--;
					}
				}
				text += text2;
			}
			ServicePointManager.ServerCertificateValidationCallback = ((object <p0>, X509Certificate <p1>, X509Chain <p2>, SslPolicyErrors <p3>) => true);
			NCMBConnection ncmbconnection = new NCMBConnection(text, method, content, NCMBUser._getCurrentSessionToken(), null, domain);
			if (header != null && header.Count > 0)
			{
				foreach (KeyValuePair<string, object> keyValuePair2 in header)
				{
					ncmbconnection._request.SetRequestHeader(keyValuePair2.Key, keyValuePair2.Value.ToString());
				}
			}
			this.Connect(ncmbconnection, callback);
		}

		// Token: 0x0600044E RID: 1102 RVA: 0x0001CFB0 File Offset: 0x0001B1B0
		internal void Connect(NCMBConnection connection, NCMBExecuteScriptCallback callback)
		{
			GameObject.Find("NCMBSettings").GetComponent<NCMBSettings>().Connection(connection, callback);
		}

		// Token: 0x0600044F RID: 1103 RVA: 0x0001CFC8 File Offset: 0x0001B1C8
		// Note: this type is marked as 'beforefieldinit'.
		static NCMBScript()
		{
		}

		// Token: 0x04000167 RID: 359
		private static readonly string SERVICE_PATH = "script";

		// Token: 0x04000168 RID: 360
		private static readonly string DEFAULT_SCRIPT_ENDPOINT = "https://script.mbaas.api.nifcloud.com";

		// Token: 0x04000169 RID: 361
		private static readonly string DEFAULT_SCRIPT_API_VERSION = "2015-09-01";

		// Token: 0x0400016A RID: 362
		private string _scriptName;

		// Token: 0x0400016B RID: 363
		private NCMBScript.MethodType _method;

		// Token: 0x0400016C RID: 364
		private string _baseUrl;

		// Token: 0x0200014E RID: 334
		public enum MethodType
		{
			// Token: 0x04000508 RID: 1288
			POST,
			// Token: 0x04000509 RID: 1289
			PUT,
			// Token: 0x0400050A RID: 1290
			GET,
			// Token: 0x0400050B RID: 1291
			DELETE
		}

		// Token: 0x0200014F RID: 335
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000A19 RID: 2585 RVA: 0x00030B88 File Offset: 0x0002ED88
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000A1A RID: 2586 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c()
			{
			}

			// Token: 0x06000A1B RID: 2587 RVA: 0x00021B8F File Offset: 0x0001FD8F
			internal bool <ExecuteAsync>b__18_0(object <p0>, X509Certificate <p1>, X509Chain <p2>, SslPolicyErrors <p3>)
			{
				return true;
			}

			// Token: 0x0400050C RID: 1292
			public static readonly NCMBScript.<>c <>9 = new NCMBScript.<>c();

			// Token: 0x0400050D RID: 1293
			public static RemoteCertificateValidationCallback <>9__18_0;
		}
	}
}
