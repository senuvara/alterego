﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using MiniJSON;
using NCMB.Internal;

namespace NCMB
{
	// Token: 0x0200005B RID: 91
	public class NCMBObject
	{
		// Token: 0x17000043 RID: 67
		public virtual object this[string key]
		{
			get
			{
				object result;
				lock (this.mutex)
				{
					this._checkGetAccess(key);
					if (!this.estimatedData.ContainsKey(key))
					{
						throw new NCMBException(new ArgumentException("The given key was not present in the dictionary."));
					}
					object obj2 = this.estimatedData[key];
					if (obj2 is NCMBRelation<NCMBObject>)
					{
						((NCMBRelation<NCMBObject>)obj2)._ensureParentAndKey(this, key);
					}
					else if (obj2 is NCMBRelation<NCMBUser>)
					{
						((NCMBRelation<NCMBUser>)obj2)._ensureParentAndKey(this, key);
					}
					else if (obj2 is NCMBRelation<NCMBRole>)
					{
						((NCMBRelation<NCMBRole>)obj2)._ensureParentAndKey(this, key);
					}
					result = obj2;
				}
				return result;
			}
			set
			{
				lock (this.mutex)
				{
					this._onSettingValue(key, value);
					if (!NCMBObject._isValidType(value))
					{
						throw new NCMBException(new ArgumentException("Invalid type for value: " + value.GetType().ToString()));
					}
					this.estimatedData[key] = value;
					this._performOperation(key, new NCMBSetOperation(value));
				}
			}
		}

		// Token: 0x06000392 RID: 914 RVA: 0x00019950 File Offset: 0x00017B50
		internal virtual void _onSettingValue(string key, object value)
		{
			if (key == null)
			{
				throw new NCMBException(new ArgumentNullException("key"));
			}
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x06000393 RID: 915 RVA: 0x00019965 File Offset: 0x00017B65
		// (set) Token: 0x06000394 RID: 916 RVA: 0x0001996D File Offset: 0x00017B6D
		public string ClassName
		{
			get
			{
				return this._className;
			}
			private set
			{
				this._className = value;
			}
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x06000395 RID: 917 RVA: 0x00019976 File Offset: 0x00017B76
		// (set) Token: 0x06000396 RID: 918 RVA: 0x00019980 File Offset: 0x00017B80
		public string ObjectId
		{
			get
			{
				return this._objectId;
			}
			set
			{
				lock (this.mutex)
				{
					this._dirty = true;
					this._objectId = value;
				}
			}
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x06000397 RID: 919 RVA: 0x000199C4 File Offset: 0x00017BC4
		// (set) Token: 0x06000398 RID: 920 RVA: 0x000199CC File Offset: 0x00017BCC
		public DateTime? UpdateDate
		{
			get
			{
				return this._updateDate;
			}
			private set
			{
				this._updateDate = value;
			}
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x06000399 RID: 921 RVA: 0x000199D5 File Offset: 0x00017BD5
		// (set) Token: 0x0600039A RID: 922 RVA: 0x000199DD File Offset: 0x00017BDD
		public DateTime? CreateDate
		{
			get
			{
				return this._createDate;
			}
			set
			{
				this._createDate = value;
			}
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x0600039C RID: 924 RVA: 0x000199F4 File Offset: 0x00017BF4
		// (set) Token: 0x0600039B RID: 923 RVA: 0x000199E6 File Offset: 0x00017BE6
		public NCMBACL ACL
		{
			get
			{
				object obj = null;
				this.estimatedData.TryGetValue("acl", out obj);
				if (obj == null)
				{
					return null;
				}
				if (!(obj is NCMBACL))
				{
					throw new NCMBException(new ArgumentException("only ACLs can be stored in the ACL key"));
				}
				NCMBACL ncmbacl = (NCMBACL)obj;
				if (ncmbacl._isShared())
				{
					NCMBACL ncmbacl2 = ncmbacl._copy();
					this.estimatedData["acl"] = ncmbacl2;
					return ncmbacl2;
				}
				return ncmbacl;
			}
			set
			{
				this["acl"] = value;
			}
		}

		// Token: 0x0600039D RID: 925 RVA: 0x00019A5D File Offset: 0x00017C5D
		private bool _checkIsDataAvailable(string key)
		{
			return this.dataAvailability.ContainsKey(key) && this.dataAvailability[key];
		}

		// Token: 0x0600039E RID: 926 RVA: 0x00019A7B File Offset: 0x00017C7B
		private void _checkGetAccess(string key)
		{
			if (!this._checkIsDataAvailable(key))
			{
				throw new NCMBException(new InvalidOperationException("NCMBObject has no data for this key.  Call FetchAsync() to get the data."));
			}
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x0600039F RID: 927 RVA: 0x00019A98 File Offset: 0x00017C98
		// (set) Token: 0x060003A0 RID: 928 RVA: 0x00019AD4 File Offset: 0x00017CD4
		public bool IsDirty
		{
			get
			{
				bool result;
				lock (this.mutex)
				{
					result = this._checkIsDirty(true);
				}
				return result;
			}
			internal set
			{
				lock (this.mutex)
				{
					this._dirty = value;
				}
			}
		}

		// Token: 0x060003A1 RID: 929 RVA: 0x00019B10 File Offset: 0x00017D10
		private bool _checkIsDirty(bool considerChildren)
		{
			return this._dirty || this._currentOperations.Count > 0 || (considerChildren && this._hasDirtyChildren());
		}

		// Token: 0x060003A2 RID: 930 RVA: 0x00019B38 File Offset: 0x00017D38
		private bool _hasDirtyChildren()
		{
			bool result;
			lock (this.mutex)
			{
				List<NCMBObject> list = new List<NCMBObject>();
				NCMBObject._findUnsavedChildren(this.estimatedData, list);
				result = (list.Count > 0);
			}
			return result;
		}

		// Token: 0x060003A3 RID: 931 RVA: 0x00019B88 File Offset: 0x00017D88
		private static void _findUnsavedChildren(object data, List<NCMBObject> unsaved)
		{
			if (data is IList)
			{
				using (IEnumerator enumerator = ((IList)data).GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						object data2 = enumerator.Current;
						NCMBObject._findUnsavedChildren(data2, unsaved);
					}
					return;
				}
			}
			if (data is IDictionary)
			{
				using (IEnumerator enumerator = ((IDictionary)data).Values.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						object data3 = enumerator.Current;
						NCMBObject._findUnsavedChildren(data3, unsaved);
					}
					return;
				}
			}
			if (data is NCMBObject)
			{
				NCMBObject ncmbobject = (NCMBObject)data;
				if (ncmbobject.IsDirty)
				{
					unsaved.Add(ncmbobject);
				}
			}
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x060003A4 RID: 932 RVA: 0x00019C50 File Offset: 0x00017E50
		public ICollection<string> Keys
		{
			get
			{
				ICollection<string> keys;
				lock (this.mutex)
				{
					keys = this.estimatedData.Keys;
				}
				return keys;
			}
		}

		// Token: 0x060003A5 RID: 933 RVA: 0x00019C90 File Offset: 0x00017E90
		internal NCMBObject() : this(NCMBObject.AUTO_CLASS_NAME)
		{
		}

		// Token: 0x060003A6 RID: 934 RVA: 0x00019CA0 File Offset: 0x00017EA0
		public NCMBRelation<T> GetRelation<T>(string key) where T : NCMBObject
		{
			NCMBRelation<T> ncmbrelation = new NCMBRelation<T>(this, key);
			object obj = null;
			this.estimatedData.TryGetValue(key, out obj);
			if (obj is NCMBRelation<T>)
			{
				ncmbrelation.TargetClass = ((NCMBRelation<T>)obj).TargetClass;
			}
			return ncmbrelation;
		}

		// Token: 0x060003A7 RID: 935 RVA: 0x00019CE0 File Offset: 0x00017EE0
		public NCMBObject(string className)
		{
			if (className == "" || className == null)
			{
				throw new NCMBException("You must specify class name or invalid classname");
			}
			if (className == NCMBObject.AUTO_CLASS_NAME)
			{
				this.ClassName = NCMBUtility.GetClassName(this);
			}
			else
			{
				this.ClassName = className;
			}
			this.operationSetQueue.AddLast(new Dictionary<string, INCMBFieldOperation>());
			this.estimatedData = new Dictionary<string, object>();
			this.IsDirty = true;
			this.dataAvailability = new Dictionary<string, bool>();
			this._setDefaultValues();
		}

		// Token: 0x060003A8 RID: 936 RVA: 0x00019D9C File Offset: 0x00017F9C
		internal void _performOperation(string key, INCMBFieldOperation operation)
		{
			lock (this.mutex)
			{
				try
				{
					object oldValue;
					this.estimatedData.TryGetValue(key, out oldValue);
					object obj2 = operation.Apply(oldValue, this, key);
					if (obj2 != null)
					{
						this.estimatedData[key] = obj2;
					}
					else
					{
						this.estimatedData.Remove(key);
					}
					INCMBFieldOperation previous;
					this._currentOperations.TryGetValue(key, out previous);
					INCMBFieldOperation value = operation.MergeWithPrevious(previous);
					this._currentOperations[key] = value;
					this.dataAvailability[key] = true;
				}
				catch (Exception error)
				{
					throw new NCMBException(error);
				}
			}
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x060003A9 RID: 937 RVA: 0x00019E4C File Offset: 0x0001804C
		internal IDictionary<string, INCMBFieldOperation> _currentOperations
		{
			get
			{
				return this.operationSetQueue.Last.Value;
			}
		}

		// Token: 0x060003AA RID: 938 RVA: 0x00019E60 File Offset: 0x00018060
		internal IDictionary<string, INCMBFieldOperation> StartSave()
		{
			object obj;
			Monitor.Enter(obj = this.mutex);
			IDictionary<string, INCMBFieldOperation> result = null;
			try
			{
				IDictionary<string, INCMBFieldOperation> currentOperations = this._currentOperations;
				this.operationSetQueue.AddLast(new Dictionary<string, INCMBFieldOperation>());
				result = currentOperations;
			}
			finally
			{
				Monitor.Exit(obj);
			}
			return result;
		}

		// Token: 0x060003AB RID: 939 RVA: 0x00019EB0 File Offset: 0x000180B0
		internal static bool _isValidType(object value)
		{
			return value == null || value is string || value is NCMBObject || value is NCMBGeoPoint || value is DateTime || value is IDictionary || value is IList || value is NCMBACL || value.GetType().IsPrimitive() || value is NCMBRelation<NCMBObject>;
		}

		// Token: 0x060003AC RID: 940 RVA: 0x00019F10 File Offset: 0x00018110
		internal void _listIsValidType(IEnumerable values)
		{
			foreach (object obj in values)
			{
				if (!NCMBObject._isValidType(obj))
				{
					throw new NCMBException(new ArgumentException("invalid type for value: " + obj.GetType().ToString()));
				}
			}
		}

		// Token: 0x060003AD RID: 941 RVA: 0x00019F60 File Offset: 0x00018160
		public void Revert()
		{
			if (this._currentOperations.Count > 0)
			{
				this._currentOperations.Clear();
				this.operationSetQueue.Clear();
				this.operationSetQueue.AddLast(new Dictionary<string, INCMBFieldOperation>());
				this._rebuildEstimatedData();
				this._dirty = false;
			}
		}

		// Token: 0x060003AE RID: 942 RVA: 0x00019FB0 File Offset: 0x000181B0
		private void _rebuildEstimatedData()
		{
			lock (this.mutex)
			{
				this.estimatedData.Clear();
				foreach (KeyValuePair<string, object> item in this.serverData)
				{
					this.estimatedData.Add(item);
				}
			}
		}

		// Token: 0x060003AF RID: 943 RVA: 0x0001A030 File Offset: 0x00018230
		private void _updateLatestEstimatedData()
		{
			lock (this.mutex)
			{
				if (this.operationSetQueue.Count > 0)
				{
					foreach (IDictionary<string, INCMBFieldOperation> operations in this.operationSetQueue)
					{
						this._applyOperations(operations, this.estimatedData);
					}
				}
			}
		}

		// Token: 0x060003B0 RID: 944 RVA: 0x0001A0BC File Offset: 0x000182BC
		private void _applyOperations(IDictionary<string, INCMBFieldOperation> operations, IDictionary<string, object> map)
		{
			lock (this.mutex)
			{
				foreach (KeyValuePair<string, INCMBFieldOperation> keyValuePair in operations)
				{
					object oldValue;
					map.TryGetValue(keyValuePair.Key, out oldValue);
					object obj2 = keyValuePair.Value.Apply(oldValue, this, keyValuePair.Key);
					if (obj2 != null)
					{
						map[keyValuePair.Key] = obj2;
					}
					else
					{
						map.Remove(keyValuePair.Key);
					}
				}
			}
		}

		// Token: 0x060003B1 RID: 945 RVA: 0x0001A168 File Offset: 0x00018368
		public virtual void Add(string key, object value)
		{
			lock (this.mutex)
			{
				if (key == null)
				{
					throw new NCMBException(new ArgumentException("key may not be null."));
				}
				if (value == null)
				{
					throw new NCMBException(new ArgumentException("value may not be null."));
				}
				if (this.estimatedData.ContainsKey(key))
				{
					throw new NCMBException(new ArgumentException("Key already exists", key));
				}
				this[key] = value;
			}
		}

		// Token: 0x060003B2 RID: 946 RVA: 0x0001A1E8 File Offset: 0x000183E8
		public virtual void Remove(string key)
		{
			lock (this.mutex)
			{
				object obj2;
				if (this.estimatedData.TryGetValue(key, out obj2))
				{
					this.estimatedData.Remove(key);
					this._currentOperations[key] = new NCMBDeleteOperation();
				}
			}
		}

		// Token: 0x060003B3 RID: 947 RVA: 0x0001A24C File Offset: 0x0001844C
		public void RemoveRangeFromList(string key, IEnumerable values)
		{
			this._listIsValidType(values);
			if (this._objectId != null && !this._objectId.Equals(""))
			{
				NCMBRemoveOperation operation = new NCMBRemoveOperation(values);
				this._performOperation(key, operation);
				return;
			}
			object obj;
			if (!this.estimatedData.TryGetValue(key, out obj))
			{
				throw new NCMBException(new ArgumentException("Does not have a value."));
			}
			if (obj is IList)
			{
				IList c = new ArrayList((IList)obj);
				ArrayList arrayList = new ArrayList(c);
				foreach (object obj2 in values)
				{
					while (arrayList.Contains(obj2))
					{
						arrayList.Remove(obj2);
					}
				}
				ArrayList arrayList2 = new ArrayList((IList)values);
				foreach (object obj3 in arrayList)
				{
					while (arrayList2.Contains(obj3))
					{
						arrayList2.Remove(obj3);
					}
				}
				HashSet<object> hashSet = new HashSet<object>();
				foreach (object obj4 in arrayList2)
				{
					if (obj4 is NCMBObject)
					{
						NCMBObject ncmbobject = (NCMBObject)obj4;
						hashSet.Add(ncmbobject.ObjectId);
					}
					for (int i = 0; i < arrayList.Count; i++)
					{
						object obj5 = arrayList[i];
						if (obj5 is NCMBObject)
						{
							NCMBObject ncmbobject2 = (NCMBObject)obj5;
							if (hashSet.Contains(ncmbobject2.ObjectId))
							{
								arrayList.RemoveAt(i);
							}
						}
					}
				}
				NCMBSetOperation operation2 = new NCMBSetOperation(arrayList);
				this._performOperation(key, operation2);
				return;
			}
			throw new NCMBException(new ArgumentException("Old value is not an array."));
		}

		// Token: 0x060003B4 RID: 948 RVA: 0x0001A458 File Offset: 0x00018658
		public void AddToList(string key, object value)
		{
			this.AddRangeToList(key, new ArrayList
			{
				value
			});
		}

		// Token: 0x060003B5 RID: 949 RVA: 0x0001A470 File Offset: 0x00018670
		public void AddRangeToList(string key, IEnumerable values)
		{
			this._listIsValidType(values);
			if (this._objectId != null && !this._objectId.Equals(""))
			{
				NCMBAddOperation operation = new NCMBAddOperation(values);
				this._performOperation(key, operation);
				return;
			}
			object obj;
			if (!this.estimatedData.TryGetValue(key, out obj))
			{
				NCMBSetOperation operation2 = new NCMBSetOperation(values);
				this._performOperation(key, operation2);
				return;
			}
			if (obj is IList)
			{
				ArrayList arrayList = new ArrayList((IList)obj);
				foreach (object value in values)
				{
					arrayList.Add(value);
				}
				NCMBSetOperation operation3 = new NCMBSetOperation(arrayList);
				this._performOperation(key, operation3);
				return;
			}
			throw new NCMBException(new ArgumentException("Old value is not an array."));
		}

		// Token: 0x060003B6 RID: 950 RVA: 0x0001A52B File Offset: 0x0001872B
		public void AddUniqueToList(string key, object value)
		{
			this.AddRangeUniqueToList(key, new ArrayList
			{
				value
			});
		}

		// Token: 0x060003B7 RID: 951 RVA: 0x0001A544 File Offset: 0x00018744
		public void AddRangeUniqueToList(string key, IEnumerable values)
		{
			this._listIsValidType(values);
			if (this._objectId != null && !this._objectId.Equals(""))
			{
				NCMBAddUniqueOperation operation = new NCMBAddUniqueOperation(values);
				this._performOperation(key, operation);
				return;
			}
			ArrayList arrayList = null;
			object obj;
			if (!this.estimatedData.TryGetValue(key, out obj))
			{
				NCMBSetOperation operation2 = new NCMBSetOperation(values);
				this._performOperation(key, operation2);
				return;
			}
			if (obj is IList)
			{
				arrayList = new ArrayList((IList)obj);
				Hashtable hashtable = new Hashtable();
				foreach (object obj2 in arrayList)
				{
					int num = 0;
					if (obj2 is NCMBObject)
					{
						NCMBObject ncmbobject = (NCMBObject)obj2;
						hashtable.Add(ncmbobject.ObjectId, num);
					}
				}
				foreach (object obj3 in values)
				{
					if (obj3 is NCMBObject)
					{
						NCMBObject ncmbobject2 = (NCMBObject)obj3;
						if (hashtable.ContainsKey(ncmbobject2.ObjectId))
						{
							int index = Convert.ToInt32(hashtable[ncmbobject2.ObjectId]);
							arrayList.Insert(index, obj3);
						}
						else
						{
							arrayList.Add(obj3);
						}
					}
					else if (!arrayList.Contains(obj3))
					{
						arrayList.Add(obj3);
					}
				}
				NCMBSetOperation operation3 = new NCMBSetOperation(arrayList);
				this._performOperation(key, operation3);
				return;
			}
			throw new NCMBException(new ArgumentException("Old value is not an array."));
		}

		// Token: 0x060003B8 RID: 952 RVA: 0x0001A6D0 File Offset: 0x000188D0
		public void Increment(string key)
		{
			this.Increment(key, 1L);
		}

		// Token: 0x060003B9 RID: 953 RVA: 0x0001A6DB File Offset: 0x000188DB
		public void Increment(string key, long amount)
		{
			this._incrementMerge(key, amount);
		}

		// Token: 0x060003BA RID: 954 RVA: 0x0001A6EA File Offset: 0x000188EA
		public void Increment(string key, double amount)
		{
			this._incrementMerge(key, amount);
		}

		// Token: 0x060003BB RID: 955 RVA: 0x0001A6FC File Offset: 0x000188FC
		private void _incrementMerge(string key, object amount)
		{
			if (this._objectId == null || this._objectId.Equals(""))
			{
				object obj;
				NCMBSetOperation operation;
				if (this.estimatedData.TryGetValue(key, out obj))
				{
					if (obj is string || obj == null)
					{
						throw new NCMBException(new InvalidOperationException("Old value is not an number."));
					}
					operation = new NCMBSetOperation(NCMBObject._addNumbers(obj, amount));
				}
				else
				{
					operation = new NCMBSetOperation(amount);
				}
				this._performOperation(key, operation);
				return;
			}
			NCMBIncrementOperation operation2 = new NCMBIncrementOperation(amount);
			this._performOperation(key, operation2);
		}

		// Token: 0x060003BC RID: 956 RVA: 0x0001A77C File Offset: 0x0001897C
		internal static object _addNumbers(object first, object second)
		{
			object result;
			try
			{
				object obj;
				if (second is long)
				{
					if (first is double)
					{
						first = Math.Truncate(Convert.ToDouble(first));
					}
					obj = Convert.ToInt64(first) + (long)second;
				}
				else
				{
					obj = Convert.ToDouble(first) + (double)second;
				}
				result = obj;
			}
			catch
			{
				throw new NCMBException(new InvalidOperationException("Value is invalid."));
			}
			return result;
		}

		// Token: 0x060003BD RID: 957 RVA: 0x0001A7FC File Offset: 0x000189FC
		internal virtual string _getBaseUrl()
		{
			return string.Concat(new string[]
			{
				NCMBSettings.DomainURL,
				"/",
				NCMBSettings.APIVersion,
				"/classes/",
				this.ClassName
			});
		}

		// Token: 0x060003BE RID: 958 RVA: 0x0001A834 File Offset: 0x00018A34
		public virtual void DeleteAsync(NCMBCallback callback)
		{
			string url = this._getBaseUrl() + "/" + this._objectId;
			ConnectType method = ConnectType.DELETE;
			new NCMBConnection(url, method, null, NCMBUser._getCurrentSessionToken()).Connect(delegate(int statusCode, string responseData, NCMBException error)
			{
				try
				{
					if (error != null)
					{
						this._handleDeleteResult(false);
					}
					else
					{
						this._handleDeleteResult(true);
					}
				}
				catch (Exception error2)
				{
					error = new NCMBException(error2);
				}
				this._afterDelete(error);
				if (callback != null)
				{
					callback(error);
				}
			});
		}

		// Token: 0x060003BF RID: 959 RVA: 0x0001A88A File Offset: 0x00018A8A
		public virtual void DeleteAsync()
		{
			this.DeleteAsync(null);
		}

		// Token: 0x060003C0 RID: 960 RVA: 0x0001A893 File Offset: 0x00018A93
		public virtual void SaveAsync(NCMBCallback callback)
		{
			this.Save(callback);
		}

		// Token: 0x060003C1 RID: 961 RVA: 0x00019332 File Offset: 0x00017532
		public virtual void SaveAsync()
		{
			this.SaveAsync(null);
		}

		// Token: 0x060003C2 RID: 962 RVA: 0x0001A89C File Offset: 0x00018A9C
		internal void Save()
		{
			this.Save(null);
		}

		// Token: 0x060003C3 RID: 963 RVA: 0x0001A8A8 File Offset: 0x00018AA8
		internal void Save(NCMBCallback callback)
		{
			if (!this.IsDirty)
			{
				if (callback != null)
				{
					callback(null);
				}
				return;
			}
			List<NCMBObject> list = new List<NCMBObject>();
			NCMBObject._findUnsavedChildren(this.estimatedData, list);
			if (list.Count > 0)
			{
				foreach (NCMBObject ncmbobject in list)
				{
					try
					{
						ncmbobject.Save();
					}
					catch (NCMBException error)
					{
						if (callback != null)
						{
							NCMBException error2;
							callback(error2);
						}
						return;
					}
				}
			}
			this._beforeSave();
			string text = this._getBaseUrl();
			ConnectType method;
			if (this._objectId != null)
			{
				text = text + "/" + this._objectId;
				method = ConnectType.PUT;
			}
			else
			{
				method = ConnectType.POST;
			}
			IDictionary<string, INCMBFieldOperation> currentOperations = null;
			currentOperations = this.StartSave();
			string content = this._toJSONObjectForSaving(currentOperations);
			new NCMBConnection(text, method, content, NCMBUser._getCurrentSessionToken()).Connect(delegate(int statusCode, string responseData, NCMBException error)
			{
				try
				{
					if (error != null)
					{
						this._handleSaveResult(false, null, currentOperations);
					}
					else
					{
						Dictionary<string, object> responseDic = Json.Deserialize(responseData) as Dictionary<string, object>;
						this._handleSaveResult(true, responseDic, currentOperations);
					}
				}
				catch (Exception error3)
				{
					error = new NCMBException(error3);
				}
				this._afterSave(statusCode, error);
				if (callback != null)
				{
					callback(error);
				}
			});
		}

		// Token: 0x060003C4 RID: 964 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		internal virtual void _beforeSave()
		{
		}

		// Token: 0x060003C5 RID: 965 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		internal virtual void _afterSave(int statusCode, NCMBException error)
		{
		}

		// Token: 0x060003C6 RID: 966 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		internal virtual void _afterDelete(NCMBException error)
		{
		}

		// Token: 0x060003C7 RID: 967 RVA: 0x0001A9DC File Offset: 0x00018BDC
		internal string _toJSONObjectForSaving(IDictionary<string, INCMBFieldOperation> operations)
		{
			string result = "";
			lock (this.mutex)
			{
				Dictionary<string, object> dictionary = new Dictionary<string, object>();
				foreach (KeyValuePair<string, INCMBFieldOperation> keyValuePair in operations)
				{
					INCMBFieldOperation value = keyValuePair.Value;
					if (value is NCMBRelationOperation<NCMBObject>)
					{
						NCMBRelationOperation<NCMBObject> ncmbrelationOperation = (NCMBRelationOperation<NCMBObject>)value;
						if (ncmbrelationOperation._relationsToAdd.Count == 0 && ncmbrelationOperation._relationsToRemove.Count == 0)
						{
							continue;
						}
					}
					else if (value is NCMBRelationOperation<NCMBUser>)
					{
						NCMBRelationOperation<NCMBUser> ncmbrelationOperation2 = (NCMBRelationOperation<NCMBUser>)value;
						if (ncmbrelationOperation2._relationsToAdd.Count == 0 && ncmbrelationOperation2._relationsToRemove.Count == 0)
						{
							continue;
						}
					}
					else if (value is NCMBRelationOperation<NCMBRole>)
					{
						NCMBRelationOperation<NCMBRole> ncmbrelationOperation3 = (NCMBRelationOperation<NCMBRole>)value;
						if (ncmbrelationOperation3._relationsToAdd.Count == 0 && ncmbrelationOperation3._relationsToRemove.Count == 0)
						{
							continue;
						}
					}
					dictionary[keyValuePair.Key] = NCMBUtility._maybeEncodeJSONObject(value, true);
				}
				result = Json.Serialize(dictionary);
			}
			return result;
		}

		// Token: 0x060003C8 RID: 968 RVA: 0x0001AB08 File Offset: 0x00018D08
		public virtual void FetchAsync(NCMBCallback callback)
		{
			if (this._objectId == null || this._objectId == "")
			{
				throw new NCMBException("Object ID must be set to be fetched.");
			}
			string url = this._getBaseUrl() + "/" + this._objectId;
			ConnectType method = ConnectType.GET;
			new NCMBConnection(url, method, null, NCMBUser._getCurrentSessionToken()).Connect(delegate(int statusCode, string responseData, NCMBException error)
			{
				try
				{
					if (error != null)
					{
						this._handleFetchResult(false, null);
					}
					else
					{
						Dictionary<string, object> responseDic = Json.Deserialize(responseData) as Dictionary<string, object>;
						this._handleFetchResult(true, responseDic);
					}
				}
				catch (Exception error2)
				{
					throw new NCMBException(error2);
				}
				if (callback != null)
				{
					callback(error);
				}
			});
		}

		// Token: 0x060003C9 RID: 969 RVA: 0x0001AB83 File Offset: 0x00018D83
		public virtual void FetchAsync()
		{
			this.FetchAsync(null);
		}

		// Token: 0x060003CA RID: 970 RVA: 0x0001AB8C File Offset: 0x00018D8C
		public bool ContainsKey(string key)
		{
			object obj = this.mutex;
			bool result;
			lock (obj)
			{
				result = this.estimatedData.ContainsKey(key);
			}
			return result;
		}

		// Token: 0x060003CB RID: 971 RVA: 0x0001ABD4 File Offset: 0x00018DD4
		public static NCMBObject CreateWithoutData(string className, string objectId)
		{
			NCMBObject result = null;
			try
			{
				if (className == "user")
				{
					result = new NCMBUser
					{
						ObjectId = objectId
					};
				}
				else
				{
					result = new NCMBObject(className)
					{
						ObjectId = objectId
					};
				}
			}
			catch (Exception error)
			{
				throw new NCMBException(error);
			}
			return result;
		}

		// Token: 0x060003CC RID: 972 RVA: 0x0001AC28 File Offset: 0x00018E28
		internal virtual void _mergeFromServer(Dictionary<string, object> responseDic, bool completeData)
		{
			lock (this.mutex)
			{
				this.IsDirty = false;
				object obj2;
				if (responseDic.TryGetValue("objectId", out obj2))
				{
					this._objectId = (string)obj2;
					responseDic.Remove("objectId");
				}
				if (responseDic.TryGetValue("createDate", out obj2))
				{
					this._setCreateDate((string)obj2);
					responseDic.Remove("createDate");
				}
				if (responseDic.TryGetValue("updateDate", out obj2))
				{
					this._setUpdateDate((string)obj2);
					responseDic.Remove("updateDate");
				}
				if (this._updateDate == null && this._createDate != null)
				{
					this._updateDate = this._createDate;
				}
				foreach (KeyValuePair<string, object> keyValuePair in responseDic)
				{
					this.dataAvailability[keyValuePair.Key] = true;
					object value = keyValuePair.Value;
					object obj3 = NCMBUtility.decodeJSONObject(value);
					if (obj3 != null)
					{
						this.serverData[keyValuePair.Key] = obj3;
					}
					else
					{
						this.serverData[keyValuePair.Key] = value;
					}
				}
				if (responseDic.TryGetValue("acl", out obj2))
				{
					NCMBACL value2 = NCMBACL._createACLFromJSONObject((Dictionary<string, object>)obj2);
					this.serverData["acl"] = value2;
					responseDic.Remove("acl");
				}
				this._rebuildEstimatedData();
			}
		}

		// Token: 0x060003CD RID: 973 RVA: 0x0001ADE0 File Offset: 0x00018FE0
		internal void _handleSaveResult(bool success, Dictionary<string, object> responseDic, IDictionary<string, INCMBFieldOperation> operationBeforeSave)
		{
			lock (this.mutex)
			{
				if (success)
				{
					this._applyOperations(operationBeforeSave, this.serverData);
					this.operationSetQueue.Remove(operationBeforeSave);
					this._mergeFromServer(responseDic, success);
					this._rebuildEstimatedData();
					this._updateLatestEstimatedData();
				}
				else
				{
					LinkedListNode<IDictionary<string, INCMBFieldOperation>> linkedListNode = this.operationSetQueue.Find(operationBeforeSave);
					IDictionary<string, INCMBFieldOperation> value = linkedListNode.Next.Value;
					this.operationSetQueue.Remove(linkedListNode);
					foreach (KeyValuePair<string, INCMBFieldOperation> keyValuePair in operationBeforeSave)
					{
						INCMBFieldOperation value2 = keyValuePair.Value;
						INCMBFieldOperation incmbfieldOperation = null;
						value.TryGetValue(keyValuePair.Key, out incmbfieldOperation);
						if (incmbfieldOperation != null)
						{
							incmbfieldOperation = incmbfieldOperation.MergeWithPrevious(value2);
						}
						else
						{
							incmbfieldOperation = value2;
						}
						value[keyValuePair.Key] = incmbfieldOperation;
					}
				}
			}
		}

		// Token: 0x060003CE RID: 974 RVA: 0x0001AEE0 File Offset: 0x000190E0
		internal void _handleFetchResult(bool success, Dictionary<string, object> responseDic)
		{
			lock (this.mutex)
			{
				if (success)
				{
					this._mergeFromServer(responseDic, success);
					this._rebuildEstimatedData();
				}
			}
		}

		// Token: 0x060003CF RID: 975 RVA: 0x0001AF24 File Offset: 0x00019124
		private void _handleDeleteResult(bool success)
		{
			lock (this.mutex)
			{
				if (success)
				{
					this.estimatedData["objectId"] = null;
					this.operationSetQueue.Clear();
					this.operationSetQueue.AddLast(new Dictionary<string, INCMBFieldOperation>());
					this.serverData.Clear();
					this.estimatedData.Clear();
					this._dirty = true;
				}
			}
		}

		// Token: 0x060003D0 RID: 976 RVA: 0x0001AFA4 File Offset: 0x000191A4
		private void _setCreateDate(string resultDate)
		{
			string format = "yyyy-MM-dd'T'HH:mm:ss.fff'Z'";
			this._createDate = new DateTime?(DateTime.ParseExact(resultDate, format, null));
		}

		// Token: 0x060003D1 RID: 977 RVA: 0x0001AFCC File Offset: 0x000191CC
		private void _setUpdateDate(string resultDate)
		{
			string format = "yyyy-MM-dd'T'HH:mm:ss.fff'Z'";
			this._updateDate = new DateTime?(DateTime.ParseExact(resultDate, format, null));
		}

		// Token: 0x060003D2 RID: 978 RVA: 0x0001AFF4 File Offset: 0x000191F4
		internal void _saveToVariable()
		{
			lock (this.mutex)
			{
				try
				{
					NCMBSettings.CurrentUser = this._toJsonDataForDataFile();
				}
				catch (Exception error)
				{
					throw new NCMBException(error);
				}
			}
		}

		// Token: 0x060003D3 RID: 979 RVA: 0x0001B044 File Offset: 0x00019244
		internal static NCMBObject _getFromVariable()
		{
			try
			{
				string currentUser = NCMBSettings.CurrentUser;
				Dictionary<string, object> dictionary = new Dictionary<string, object>();
				if (currentUser != null && currentUser != "")
				{
					dictionary = (Json.Deserialize(currentUser) as Dictionary<string, object>);
					NCMBObject ncmbobject = NCMBObject.CreateWithoutData((string)dictionary["className"], null);
					ncmbobject._mergeFromServer(dictionary, true);
					return ncmbobject;
				}
			}
			catch (Exception error)
			{
				throw new NCMBException(error);
			}
			return null;
		}

		// Token: 0x060003D4 RID: 980 RVA: 0x0001B0B8 File Offset: 0x000192B8
		internal void _saveToDisk(string fileName)
		{
			string path = NCMBSettings.filePath + "/" + fileName;
			lock (this.mutex)
			{
				try
				{
					string value = this._toJsonDataForDataFile();
					using (StreamWriter streamWriter = new StreamWriter(path, false, Encoding.UTF8))
					{
						streamWriter.Write(value);
						streamWriter.Close();
					}
				}
				catch (Exception error)
				{
					throw new NCMBException(error);
				}
			}
		}

		// Token: 0x060003D5 RID: 981 RVA: 0x0001B148 File Offset: 0x00019348
		internal static NCMBObject _getFromDisk(string fileName)
		{
			try
			{
				string text = NCMBObject._getDiskData(NCMBSettings.filePath + "/" + fileName);
				Dictionary<string, object> dictionary = new Dictionary<string, object>();
				if (text != null && text != "")
				{
					dictionary = (Json.Deserialize(text) as Dictionary<string, object>);
					NCMBObject ncmbobject = NCMBObject.CreateWithoutData((string)dictionary["className"], null);
					ncmbobject._mergeFromServer(dictionary, true);
					return ncmbobject;
				}
			}
			catch (Exception error)
			{
				throw new NCMBException(error);
			}
			return null;
		}

		// Token: 0x060003D6 RID: 982 RVA: 0x0001B1CC File Offset: 0x000193CC
		internal static string _getDiskData(string fileName)
		{
			string result;
			try
			{
				string text = "";
				if (File.Exists(fileName))
				{
					StreamReader streamReader = new StreamReader(fileName, Encoding.UTF8);
					using (streamReader)
					{
						string text2;
						do
						{
							text2 = streamReader.ReadLine();
							if (text2 != null)
							{
								text += text2;
							}
						}
						while (text2 != null);
						streamReader.Close();
					}
				}
				result = text;
			}
			catch (Exception error)
			{
				throw new NCMBException(error);
			}
			return result;
		}

		// Token: 0x060003D7 RID: 983 RVA: 0x0001B248 File Offset: 0x00019448
		internal string _toJsonDataForDataFile()
		{
			string result = "";
			lock (this.mutex)
			{
				Dictionary<string, object> dictionary = new Dictionary<string, object>();
				foreach (KeyValuePair<string, object> keyValuePair in this.serverData)
				{
					object value = keyValuePair.Value;
					dictionary[keyValuePair.Key] = NCMBUtility._maybeEncodeJSONObject(value, true);
				}
				if (this._createDate != null)
				{
					dictionary["createDate"] = NCMBUtility.encodeDate(this._createDate.Value);
				}
				if (this._updateDate != null)
				{
					dictionary["updateDate"] = NCMBUtility.encodeDate(this._updateDate.Value);
				}
				if (this._objectId != null)
				{
					dictionary["objectId"] = this._objectId;
				}
				dictionary["className"] = this._className;
				result = Json.Serialize(dictionary);
			}
			return result;
		}

		// Token: 0x060003D8 RID: 984 RVA: 0x0001B360 File Offset: 0x00019560
		private void _setDefaultValues()
		{
			if (NCMBACL._getDefaultACL() != null)
			{
				this.ACL = NCMBACL._getDefaultACL();
			}
		}

		// Token: 0x060003D9 RID: 985 RVA: 0x0001B374 File Offset: 0x00019574
		// Note: this type is marked as 'beforefieldinit'.
		static NCMBObject()
		{
		}

		// Token: 0x0400014F RID: 335
		private static readonly string AUTO_CLASS_NAME = "_NewAutoClass";

		// Token: 0x04000150 RID: 336
		private bool _dirty;

		// Token: 0x04000151 RID: 337
		private readonly IDictionary<string, bool> dataAvailability = new Dictionary<string, bool>();

		// Token: 0x04000152 RID: 338
		internal IDictionary<string, object> estimatedData = new Dictionary<string, object>();

		// Token: 0x04000153 RID: 339
		internal IDictionary<string, object> serverData = new Dictionary<string, object>();

		// Token: 0x04000154 RID: 340
		internal readonly object mutex = new object();

		// Token: 0x04000155 RID: 341
		internal static readonly object fileMutex = new object();

		// Token: 0x04000156 RID: 342
		private readonly LinkedList<IDictionary<string, INCMBFieldOperation>> operationSetQueue = new LinkedList<IDictionary<string, INCMBFieldOperation>>();

		// Token: 0x04000157 RID: 343
		private string _className;

		// Token: 0x04000158 RID: 344
		private string _objectId;

		// Token: 0x04000159 RID: 345
		private DateTime? _updateDate;

		// Token: 0x0400015A RID: 346
		private DateTime? _createDate;

		// Token: 0x02000148 RID: 328
		[CompilerGenerated]
		private sealed class <>c__DisplayClass67_0
		{
			// Token: 0x06000A0D RID: 2573 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c__DisplayClass67_0()
			{
			}

			// Token: 0x06000A0E RID: 2574 RVA: 0x000308EC File Offset: 0x0002EAEC
			internal void <DeleteAsync>b__0(int statusCode, string responseData, NCMBException error)
			{
				try
				{
					if (error != null)
					{
						this.<>4__this._handleDeleteResult(false);
					}
					else
					{
						this.<>4__this._handleDeleteResult(true);
					}
				}
				catch (Exception error2)
				{
					error = new NCMBException(error2);
				}
				this.<>4__this._afterDelete(error);
				if (this.callback != null)
				{
					this.callback(error);
				}
			}

			// Token: 0x040004FB RID: 1275
			public NCMBObject <>4__this;

			// Token: 0x040004FC RID: 1276
			public NCMBCallback callback;
		}

		// Token: 0x02000149 RID: 329
		[CompilerGenerated]
		private sealed class <>c__DisplayClass72_0
		{
			// Token: 0x06000A0F RID: 2575 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c__DisplayClass72_0()
			{
			}

			// Token: 0x06000A10 RID: 2576 RVA: 0x00030954 File Offset: 0x0002EB54
			internal void <Save>b__0(int statusCode, string responseData, NCMBException error)
			{
				try
				{
					if (error != null)
					{
						this.<>4__this._handleSaveResult(false, null, this.currentOperations);
					}
					else
					{
						Dictionary<string, object> responseDic = Json.Deserialize(responseData) as Dictionary<string, object>;
						this.<>4__this._handleSaveResult(true, responseDic, this.currentOperations);
					}
				}
				catch (Exception error2)
				{
					error = new NCMBException(error2);
				}
				this.<>4__this._afterSave(statusCode, error);
				if (this.callback != null)
				{
					this.callback(error);
				}
			}

			// Token: 0x040004FD RID: 1277
			public NCMBObject <>4__this;

			// Token: 0x040004FE RID: 1278
			public IDictionary<string, INCMBFieldOperation> currentOperations;

			// Token: 0x040004FF RID: 1279
			public NCMBCallback callback;
		}

		// Token: 0x0200014A RID: 330
		[CompilerGenerated]
		private sealed class <>c__DisplayClass77_0
		{
			// Token: 0x06000A11 RID: 2577 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c__DisplayClass77_0()
			{
			}

			// Token: 0x06000A12 RID: 2578 RVA: 0x000309D4 File Offset: 0x0002EBD4
			internal void <FetchAsync>b__0(int statusCode, string responseData, NCMBException error)
			{
				try
				{
					if (error != null)
					{
						this.<>4__this._handleFetchResult(false, null);
					}
					else
					{
						Dictionary<string, object> responseDic = Json.Deserialize(responseData) as Dictionary<string, object>;
						this.<>4__this._handleFetchResult(true, responseDic);
					}
				}
				catch (Exception error2)
				{
					throw new NCMBException(error2);
				}
				if (this.callback != null)
				{
					this.callback(error);
				}
			}

			// Token: 0x04000500 RID: 1280
			public NCMBObject <>4__this;

			// Token: 0x04000501 RID: 1281
			public NCMBCallback callback;
		}
	}
}
