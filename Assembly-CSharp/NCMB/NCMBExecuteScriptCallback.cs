﻿using System;

namespace NCMB
{
	// Token: 0x02000053 RID: 83
	// (Invoke) Token: 0x06000354 RID: 852
	public delegate void NCMBExecuteScriptCallback(byte[] data, NCMBException error);
}
