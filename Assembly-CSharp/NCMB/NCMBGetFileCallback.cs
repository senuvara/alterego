﻿using System;

namespace NCMB
{
	// Token: 0x02000054 RID: 84
	// (Invoke) Token: 0x06000358 RID: 856
	public delegate void NCMBGetFileCallback(byte[] data, NCMBException error);
}
