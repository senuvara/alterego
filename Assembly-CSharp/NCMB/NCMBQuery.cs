﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using MiniJSON;
using NCMB.Internal;

namespace NCMB
{
	// Token: 0x0200005D RID: 93
	public class NCMBQuery<T> where T : NCMBObject
	{
		// Token: 0x06000400 RID: 1024 RVA: 0x0001B7E8 File Offset: 0x000199E8
		public NCMBQuery(string theClassName)
		{
			this._className = theClassName;
			this._limit = -1;
			this._skip = 0;
			this._order = new List<string>();
			this._include = new List<string>();
			this._where = new Dictionary<string, object>();
		}

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x06000401 RID: 1025 RVA: 0x0001B83C File Offset: 0x00019A3C
		// (set) Token: 0x06000402 RID: 1026 RVA: 0x0001B844 File Offset: 0x00019A44
		public int Skip
		{
			get
			{
				return this._skip;
			}
			set
			{
				this._skip = value;
			}
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x06000403 RID: 1027 RVA: 0x0001B84D File Offset: 0x00019A4D
		// (set) Token: 0x06000404 RID: 1028 RVA: 0x0001B855 File Offset: 0x00019A55
		public int Limit
		{
			get
			{
				return this._limit;
			}
			set
			{
				this._limit = value;
			}
		}

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x06000405 RID: 1029 RVA: 0x0001B85E File Offset: 0x00019A5E
		public string ClassName
		{
			get
			{
				return this._className;
			}
		}

		// Token: 0x06000406 RID: 1030 RVA: 0x0001B868 File Offset: 0x00019A68
		public static NCMBQuery<T> Or(List<NCMBQuery<T>> queries)
		{
			List<NCMBQuery<T>> list = new List<NCMBQuery<T>>();
			string text = null;
			if (queries == null)
			{
				throw new NCMBException(new ArgumentException("queries may not be null."));
			}
			if (queries.Count == 0)
			{
				throw new NCMBException(new ArgumentException("Can't take an or of an empty list of queries"));
			}
			for (int i = 0; i < queries.Count; i++)
			{
				if (text != null && !queries[i]._className.Equals(text))
				{
					throw new NCMBException(new ArgumentException("All of the queries in an or query must be on the same class "));
				}
				text = queries[i]._className;
				list.Add(queries[i]);
			}
			return new NCMBQuery<T>(text)._whereSatifiesAnyOf(list);
		}

		// Token: 0x06000407 RID: 1031 RVA: 0x0001B906 File Offset: 0x00019B06
		private NCMBQuery<T> _whereSatifiesAnyOf(List<NCMBQuery<T>> queries)
		{
			this._where["$or"] = queries;
			return this;
		}

		// Token: 0x06000408 RID: 1032 RVA: 0x0001B91A File Offset: 0x00019B1A
		public NCMBQuery<T> OrderByAscending(string key)
		{
			this._order.Clear();
			this._order.Add(key);
			return this;
		}

		// Token: 0x06000409 RID: 1033 RVA: 0x0001B934 File Offset: 0x00019B34
		public NCMBQuery<T> OrderByDescending(string key)
		{
			this._order.Clear();
			this._order.Add("-" + key);
			return this;
		}

		// Token: 0x0600040A RID: 1034 RVA: 0x0001B958 File Offset: 0x00019B58
		public NCMBQuery<T> AddAscendingOrder(string key)
		{
			if (this._order.Count == 0 || this._order[0].Equals(""))
			{
				this._order.Clear();
			}
			this._order.Add(key);
			return this;
		}

		// Token: 0x0600040B RID: 1035 RVA: 0x0001B998 File Offset: 0x00019B98
		public NCMBQuery<T> AddDescendingOrder(string key)
		{
			if (this._order.Count == 0 || this._order[0].Equals(""))
			{
				this._order.Clear();
			}
			this._order.Add("-" + key);
			return this;
		}

		// Token: 0x0600040C RID: 1036 RVA: 0x0001B9EC File Offset: 0x00019BEC
		public NCMBQuery<T> WhereEqualTo(string key, object value)
		{
			value = NCMBUtility._maybeEncodeJSONObject(value, true);
			this._where[key] = value;
			return this;
		}

		// Token: 0x0600040D RID: 1037 RVA: 0x0001BA05 File Offset: 0x00019C05
		public NCMBQuery<T> WhereNotEqualTo(string key, object value)
		{
			this._addCondition(key, "$ne", value);
			return this;
		}

		// Token: 0x0600040E RID: 1038 RVA: 0x0001BA15 File Offset: 0x00019C15
		public NCMBQuery<T> WhereGreaterThan(string key, object value)
		{
			this._addCondition(key, "$gt", value);
			return this;
		}

		// Token: 0x0600040F RID: 1039 RVA: 0x0001BA25 File Offset: 0x00019C25
		public NCMBQuery<T> WhereGreaterThanOrEqualTo(string key, object value)
		{
			this._addCondition(key, "$gte", value);
			return this;
		}

		// Token: 0x06000410 RID: 1040 RVA: 0x0001BA35 File Offset: 0x00019C35
		public NCMBQuery<T> WhereLessThan(string key, object value)
		{
			this._addCondition(key, "$lt", value);
			return this;
		}

		// Token: 0x06000411 RID: 1041 RVA: 0x0001BA45 File Offset: 0x00019C45
		public NCMBQuery<T> WhereLessThanOrEqualTo(string key, object value)
		{
			this._addCondition(key, "$lte", value);
			return this;
		}

		// Token: 0x06000412 RID: 1042 RVA: 0x0001BA58 File Offset: 0x00019C58
		public NCMBQuery<T> WhereContainedIn(string key, IEnumerable values)
		{
			List<object> list = new List<object>();
			foreach (object item in values)
			{
				list.Add(item);
			}
			this._addCondition(key, "$in", list);
			return this;
		}

		// Token: 0x06000413 RID: 1043 RVA: 0x0001BABC File Offset: 0x00019CBC
		public NCMBQuery<T> WhereNotContainedIn(string key, IEnumerable values)
		{
			List<object> list = new List<object>();
			foreach (object item in values)
			{
				list.Add(item);
			}
			this._addCondition(key, "$nin", list);
			return this;
		}

		// Token: 0x06000414 RID: 1044 RVA: 0x0001BB20 File Offset: 0x00019D20
		public NCMBQuery<T> WhereContainedInArray(string key, IEnumerable values)
		{
			List<object> list = new List<object>();
			foreach (object item in values)
			{
				list.Add(item);
			}
			this._addCondition(key, "$inArray", list);
			return this;
		}

		// Token: 0x06000415 RID: 1045 RVA: 0x0001BB84 File Offset: 0x00019D84
		public NCMBQuery<T> WhereContainsAll(string key, IEnumerable values)
		{
			List<object> list = new List<object>();
			foreach (object item in values)
			{
				list.Add(item);
			}
			this._addCondition(key, "$all", list);
			return this;
		}

		// Token: 0x06000416 RID: 1046 RVA: 0x0001BBE8 File Offset: 0x00019DE8
		public NCMBQuery<T> WhereMatchesQuery<TOther>(string key, NCMBQuery<TOther> query) where TOther : NCMBObject
		{
			this._addCondition(key, "$inQuery", query);
			return this;
		}

		// Token: 0x06000417 RID: 1047 RVA: 0x0001BBF8 File Offset: 0x00019DF8
		public NCMBQuery<T> WhereMatchesKeyInQuery<TOther>(string mainKey, string subKey, NCMBQuery<TOther> query) where TOther : NCMBObject
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary["query"] = query;
			dictionary["key"] = subKey;
			this._addCondition(mainKey, "$select", dictionary);
			return this;
		}

		// Token: 0x06000418 RID: 1048 RVA: 0x0001BC31 File Offset: 0x00019E31
		public void Include(string key)
		{
			this._include.Add(key);
		}

		// Token: 0x06000419 RID: 1049 RVA: 0x0001BC40 File Offset: 0x00019E40
		public NCMBQuery<T> WhereNearGeoPoint(string key, NCMBGeoPoint point)
		{
			object value = this._geoPointToObject(point);
			this._addCondition(key, "$nearSphere", value);
			return this;
		}

		// Token: 0x0600041A RID: 1050 RVA: 0x0001BC64 File Offset: 0x00019E64
		public NCMBQuery<T> WhereGeoPointWithinKilometers(string key, NCMBGeoPoint point, double maxDistance)
		{
			Dictionary<string, object> value = this._geoPointToObject(point);
			this._addCondition(key, "$nearSphere", value);
			this._addCondition(key, "$maxDistanceInKilometers", maxDistance);
			return this;
		}

		// Token: 0x0600041B RID: 1051 RVA: 0x0001BC9C File Offset: 0x00019E9C
		public NCMBQuery<T> WhereGeoPointWithinMiles(string key, NCMBGeoPoint point, double maxDistance)
		{
			Dictionary<string, object> value = this._geoPointToObject(point);
			this._addCondition(key, "$nearSphere", value);
			this._addCondition(key, "$maxDistanceInMiles", maxDistance);
			return this;
		}

		// Token: 0x0600041C RID: 1052 RVA: 0x0001BCD4 File Offset: 0x00019ED4
		public NCMBQuery<T> WhereGeoPointWithinRadians(string key, NCMBGeoPoint point, double maxDistance)
		{
			Dictionary<string, object> value = this._geoPointToObject(point);
			this._addCondition(key, "$nearSphere", value);
			this._addCondition(key, "$maxDistanceInRadians", maxDistance);
			return this;
		}

		// Token: 0x0600041D RID: 1053 RVA: 0x0001BD0C File Offset: 0x00019F0C
		public NCMBQuery<T> WhereWithinGeoBox(string key, NCMBGeoPoint southwest, NCMBGeoPoint northeast)
		{
			Dictionary<string, object> value = this._geoPointToObjectWithinBox(southwest, northeast);
			this._addCondition(key, "$within", value);
			return this;
		}

		// Token: 0x0600041E RID: 1054 RVA: 0x0001BD30 File Offset: 0x00019F30
		private Dictionary<string, object> _geoPointToObjectWithinBox(NCMBGeoPoint southwest, NCMBGeoPoint northeast)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			Dictionary<string, object> item = this._geoPointToObject(southwest);
			Dictionary<string, object> item2 = this._geoPointToObject(northeast);
			dictionary["$box"] = new List<object>
			{
				item,
				item2
			};
			return dictionary;
		}

		// Token: 0x0600041F RID: 1055 RVA: 0x0001BD74 File Offset: 0x00019F74
		private Dictionary<string, object> _geoPointToObject(NCMBGeoPoint point)
		{
			return new Dictionary<string, object>
			{
				{
					"__type",
					"GeoPoint"
				},
				{
					"longitude",
					point.Longitude
				},
				{
					"latitude",
					point.Latitude
				}
			};
		}

		// Token: 0x06000420 RID: 1056 RVA: 0x0001BDC4 File Offset: 0x00019FC4
		private void _addCondition(string key, string condition, object value)
		{
			Dictionary<string, object> dictionary = null;
			value = NCMBUtility._maybeEncodeJSONObject(value, true);
			if (this._where.ContainsKey(key))
			{
				object obj = this._where[key];
				if (obj is IDictionary)
				{
					dictionary = (Dictionary<string, object>)obj;
				}
			}
			if (dictionary == null)
			{
				dictionary = new Dictionary<string, object>();
			}
			dictionary[condition] = value;
			this._where[key] = dictionary;
		}

		// Token: 0x06000421 RID: 1057 RVA: 0x0001BE24 File Offset: 0x0001A024
		public void FindAsync(NCMBQueryCallback<T> callback)
		{
			if (callback == null)
			{
				throw new ArgumentException("It is necessary to always set a callback.");
			}
			this.Find(callback);
		}

		// Token: 0x06000422 RID: 1058 RVA: 0x0001BE3C File Offset: 0x0001A03C
		internal void Find(NCMBQueryCallback<T> callback)
		{
			string text = this._getSearchUrl(this._className);
			text += this.WHERE_URL;
			Dictionary<string, object> beforejsonData = this._getFindParams();
			text = this._makeWhereUrl(text, beforejsonData);
			ConnectType method = ConnectType.GET;
			new NCMBConnection(text, method, null, NCMBUser._getCurrentSessionToken()).Connect(delegate(int statusCode, string responseData, NCMBException error)
			{
				List<T> list = new List<T>();
				try
				{
					if (error == null)
					{
						Dictionary<string, object> response = Json.Deserialize(responseData) as Dictionary<string, object>;
						foreach (object obj in this._convertFindResponse(response))
						{
							T item = (T)((object)obj);
							list.Add(item);
						}
					}
				}
				catch (Exception error2)
				{
					error = new NCMBException(error2);
				}
				callback(list, error);
			});
		}

		// Token: 0x06000423 RID: 1059 RVA: 0x0001BEA8 File Offset: 0x0001A0A8
		private ArrayList _convertFindResponse(Dictionary<string, object> response)
		{
			ArrayList arrayList = new ArrayList();
			List<object> list = (List<object>)response["results"];
			if (list == null)
			{
				Debug.Log("null results in find response");
			}
			else
			{
				object obj = null;
				string text = null;
				if (response.TryGetValue("className", out obj))
				{
					text = (string)obj;
				}
				if (text == null)
				{
					text = this._className;
				}
				for (int i = 0; i < list.Count; i++)
				{
					NCMBObject ncmbobject;
					if (text.Equals("user"))
					{
						ncmbobject = new NCMBUser();
					}
					else if (text.Equals("role"))
					{
						ncmbobject = new NCMBRole();
					}
					else if (text.Equals("installation"))
					{
						ncmbobject = new NCMBInstallation();
					}
					else if (text.Equals("push"))
					{
						ncmbobject = new NCMBPush();
					}
					else if (text.Equals("file"))
					{
						ncmbobject = new NCMBFile();
					}
					else
					{
						ncmbobject = new NCMBObject(text);
					}
					ncmbobject._mergeFromServer((Dictionary<string, object>)list[i], true);
					arrayList.Add(ncmbobject);
				}
			}
			return arrayList;
		}

		// Token: 0x06000424 RID: 1060 RVA: 0x0001BFB8 File Offset: 0x0001A1B8
		public void GetAsync(string objectId, NCMBGetCallback<T> callback)
		{
			if (callback == null)
			{
				throw new ArgumentException("It is necessary to always set a callback.");
			}
			string url = this._getSearchUrl(this._className) + "/" + objectId;
			ConnectType method = ConnectType.GET;
			new NCMBConnection(url, method, null, NCMBUser._getCurrentSessionToken()).Connect(delegate(int statusCode, string responseData, NCMBException error)
			{
				NCMBObject ncmbobject = null;
				try
				{
					if (error == null)
					{
						Dictionary<string, object> response = Json.Deserialize(responseData) as Dictionary<string, object>;
						ncmbobject = this._convertGetResponse(response);
					}
				}
				catch (Exception error2)
				{
					error = new NCMBException(error2);
				}
				callback((T)((object)ncmbobject), error);
			});
		}

		// Token: 0x06000425 RID: 1061 RVA: 0x0001C024 File Offset: 0x0001A224
		private NCMBObject _convertGetResponse(Dictionary<string, object> response)
		{
			NCMBObject result = null;
			if (response == null)
			{
				Debug.Log("null results in get response");
			}
			else
			{
				string className = this._className;
				NCMBObject ncmbobject;
				if (className.Equals("user"))
				{
					ncmbobject = new NCMBUser();
				}
				else
				{
					ncmbobject = new NCMBObject(className);
				}
				ncmbobject._mergeFromServer(response, true);
				result = ncmbobject;
			}
			return result;
		}

		// Token: 0x06000426 RID: 1062 RVA: 0x0001C074 File Offset: 0x0001A274
		public void CountAsync(NCMBCountCallback callback)
		{
			if (callback == null)
			{
				throw new ArgumentException("It is necessary to always set a callback.");
			}
			string text = this._getSearchUrl(this._className);
			text += this.WHERE_URL;
			Dictionary<string, object> dictionary = this._getFindParams();
			dictionary["count"] = 1;
			text = this._makeWhereUrl(text, dictionary);
			ConnectType method = ConnectType.GET;
			new NCMBConnection(text, method, null, NCMBUser._getCurrentSessionToken()).Connect(delegate(int statusCode, string responseData, NCMBException error)
			{
				int count = 0;
				if (error == null)
				{
					try
					{
						Dictionary<string, object> dictionary2 = Json.Deserialize(responseData) as Dictionary<string, object>;
						object value = null;
						if (dictionary2.TryGetValue("count", out value))
						{
							count = Convert.ToInt32(value);
						}
					}
					catch (Exception error2)
					{
						error = new NCMBException(error2);
					}
				}
				callback(count, error);
			});
		}

		// Token: 0x06000427 RID: 1063 RVA: 0x0001C0FC File Offset: 0x0001A2FC
		private string _makeWhereUrl(string url, Dictionary<string, object> beforejsonData)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(url);
			foreach (string text in beforejsonData.Keys)
			{
				if (!text.Equals("className"))
				{
					string stringToEscape;
					if (beforejsonData[text] is IDictionary)
					{
						stringToEscape = Json.Serialize((Dictionary<string, object>)beforejsonData[text]);
					}
					else if (beforejsonData[text] is int)
					{
						stringToEscape = Json.Serialize((int)beforejsonData[text]);
					}
					else
					{
						stringToEscape = (string)beforejsonData[text];
					}
					string text2 = Uri.EscapeUriString(stringToEscape);
					text2 = text2.Replace(":", "%3A");
					stringBuilder.Append(text).Append("=").Append(text2).Append("&");
				}
			}
			if (beforejsonData.Count > 0)
			{
				stringBuilder.Remove(stringBuilder.Length - 1, 1);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000428 RID: 1064 RVA: 0x0001C220 File Offset: 0x0001A420
		private Dictionary<string, object> _getFindParams()
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
			dictionary["className"] = this._className;
			foreach (string text in this._where.Keys)
			{
				if (text.Equals("$or"))
				{
					List<NCMBQuery<T>> list = (List<NCMBQuery<T>>)this._where[text];
					List<object> list2 = new List<object>();
					foreach (NCMBQuery<T> ncmbquery in list)
					{
						if (ncmbquery._limit >= 0)
						{
							throw new ArgumentException("Cannot have limits in sub queries of an 'OR' query");
						}
						if (ncmbquery._skip > 0)
						{
							throw new ArgumentException("Cannot have skips in sub queries of an 'OR' query");
						}
						if (ncmbquery._order.Count > 0)
						{
							throw new ArgumentException("Cannot have an order in sub queries of an 'OR' query");
						}
						if (ncmbquery._include.Count > 0)
						{
							throw new ArgumentException("Cannot have an include in sub queries of an 'OR' query");
						}
						Dictionary<string, object> dictionary3 = ncmbquery._getFindParams();
						if (dictionary3["where"] != null)
						{
							list2.Add(dictionary3["where"]);
						}
						else
						{
							list2.Add(new Dictionary<string, object>());
						}
					}
					dictionary2[text] = list2;
				}
				else
				{
					object value = this._encodeSubQueries(this._where[text]);
					dictionary2[text] = NCMBUtility._maybeEncodeJSONObject(value, true);
				}
			}
			dictionary["where"] = dictionary2;
			if (this._limit >= 0)
			{
				dictionary["limit"] = this._limit;
			}
			if (this._skip > 0)
			{
				dictionary["skip"] = this._skip;
			}
			if (this._order.Count > 0)
			{
				dictionary["order"] = this._join(this._order, ",");
			}
			if (this._include.Count > 0)
			{
				dictionary["include"] = this._join(this._include, ",");
			}
			return dictionary;
		}

		// Token: 0x06000429 RID: 1065 RVA: 0x0001C46C File Offset: 0x0001A66C
		private object _encodeSubQueries(object value)
		{
			if (!(value is IDictionary))
			{
				return value;
			}
			Dictionary<string, object> dictionary = (Dictionary<string, object>)value;
			Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
			foreach (KeyValuePair<string, object> keyValuePair in dictionary)
			{
				if (keyValuePair.Value is NCMBQuery<NCMBObject>)
				{
					Dictionary<string, object> dictionary3 = ((NCMBQuery<NCMBObject>)keyValuePair.Value)._getFindParams();
					dictionary3["where"] = dictionary3["where"];
					dictionary2[keyValuePair.Key] = dictionary3;
				}
				else if (keyValuePair.Value is NCMBQuery<NCMBUser>)
				{
					Dictionary<string, object> dictionary4 = ((NCMBQuery<NCMBUser>)keyValuePair.Value)._getFindParams();
					dictionary4["where"] = dictionary4["where"];
					dictionary2[keyValuePair.Key] = dictionary4;
				}
				else if (keyValuePair.Value is NCMBQuery<NCMBRole>)
				{
					Dictionary<string, object> dictionary5 = ((NCMBQuery<NCMBRole>)keyValuePair.Value)._getFindParams();
					dictionary5["where"] = dictionary5["where"];
					dictionary2[keyValuePair.Key] = dictionary5;
				}
				else if (keyValuePair.Value is NCMBQuery<NCMBInstallation>)
				{
					Dictionary<string, object> dictionary6 = ((NCMBQuery<NCMBInstallation>)keyValuePair.Value)._getFindParams();
					dictionary6["where"] = dictionary6["where"];
					dictionary2[keyValuePair.Key] = dictionary6;
				}
				else if (keyValuePair.Value is NCMBQuery<NCMBPush>)
				{
					Dictionary<string, object> dictionary7 = ((NCMBQuery<NCMBPush>)keyValuePair.Value)._getFindParams();
					dictionary7["where"] = dictionary7["where"];
					dictionary2[keyValuePair.Key] = dictionary7;
				}
				else if (keyValuePair.Value is IDictionary)
				{
					dictionary2[keyValuePair.Key] = this._encodeSubQueries(keyValuePair.Value);
				}
				else
				{
					dictionary2[keyValuePair.Key] = keyValuePair.Value;
				}
			}
			return dictionary2;
		}

		// Token: 0x0600042A RID: 1066 RVA: 0x0001C694 File Offset: 0x0001A894
		private string _join(List<string> items, string delimiter)
		{
			StringBuilder stringBuilder = new StringBuilder();
			foreach (string value in items)
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(delimiter);
				}
				stringBuilder.Append(value);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x0600042B RID: 1067 RVA: 0x0001C700 File Offset: 0x0001A900
		private string _getSearchUrl(string className)
		{
			if (className == null || className.Equals(""))
			{
				throw new ArgumentException("Not class name error. Please be sure to specify the class name.");
			}
			string result;
			if (className.Equals("push"))
			{
				result = new NCMBPush()._getBaseUrl();
			}
			else if (className.Equals("installation"))
			{
				result = new NCMBInstallation()._getBaseUrl();
			}
			else if (className.Equals("file"))
			{
				result = new NCMBFile()._getBaseUrl();
			}
			else if (className.Equals("user"))
			{
				result = new NCMBUser()._getBaseUrl();
			}
			else if (className.Equals("role"))
			{
				result = new NCMBRole()._getBaseUrl();
			}
			else
			{
				result = new NCMBObject(this._className)._getBaseUrl();
			}
			return result;
		}

		// Token: 0x0600042C RID: 1068 RVA: 0x0001C7C2 File Offset: 0x0001A9C2
		public static NCMBQuery<T> GetQuery(string className)
		{
			return new NCMBQuery<T>(className);
		}

		// Token: 0x0600042D RID: 1069 RVA: 0x0001C7CA File Offset: 0x0001A9CA
		internal NCMBQuery<T> _whereRelatedTo(NCMBObject parent, string key)
		{
			this._addCondition("$relatedTo", "object", NCMBUtility._maybeEncodeJSONObject(parent, true));
			this._addCondition("$relatedTo", "key", key);
			return this;
		}

		// Token: 0x0400015C RID: 348
		private readonly Dictionary<string, object> _where;

		// Token: 0x0400015D RID: 349
		private readonly string WHERE_URL = "?";

		// Token: 0x0400015E RID: 350
		private string _className;

		// Token: 0x0400015F RID: 351
		private int _limit;

		// Token: 0x04000160 RID: 352
		private int _skip;

		// Token: 0x04000161 RID: 353
		private List<string> _order;

		// Token: 0x04000162 RID: 354
		private List<string> _include;

		// Token: 0x0200014B RID: 331
		[CompilerGenerated]
		private sealed class <>c__DisplayClass44_0
		{
			// Token: 0x06000A13 RID: 2579 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c__DisplayClass44_0()
			{
			}

			// Token: 0x06000A14 RID: 2580 RVA: 0x00030A38 File Offset: 0x0002EC38
			internal void <Find>b__0(int statusCode, string responseData, NCMBException error)
			{
				List<T> list = new List<T>();
				try
				{
					if (error == null)
					{
						Dictionary<string, object> response = Json.Deserialize(responseData) as Dictionary<string, object>;
						foreach (object obj in this.<>4__this._convertFindResponse(response))
						{
							T item = (T)((object)obj);
							list.Add(item);
						}
					}
				}
				catch (Exception error2)
				{
					error = new NCMBException(error2);
				}
				this.callback(list, error);
			}

			// Token: 0x04000502 RID: 1282
			public NCMBQuery<T> <>4__this;

			// Token: 0x04000503 RID: 1283
			public NCMBQueryCallback<T> callback;
		}

		// Token: 0x0200014C RID: 332
		[CompilerGenerated]
		private sealed class <>c__DisplayClass46_0
		{
			// Token: 0x06000A15 RID: 2581 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c__DisplayClass46_0()
			{
			}

			// Token: 0x06000A16 RID: 2582 RVA: 0x00030AD4 File Offset: 0x0002ECD4
			internal void <GetAsync>b__0(int statusCode, string responseData, NCMBException error)
			{
				NCMBObject ncmbobject = null;
				try
				{
					if (error == null)
					{
						Dictionary<string, object> response = Json.Deserialize(responseData) as Dictionary<string, object>;
						ncmbobject = this.<>4__this._convertGetResponse(response);
					}
				}
				catch (Exception error2)
				{
					error = new NCMBException(error2);
				}
				this.callback((T)((object)ncmbobject), error);
			}

			// Token: 0x04000504 RID: 1284
			public NCMBQuery<T> <>4__this;

			// Token: 0x04000505 RID: 1285
			public NCMBGetCallback<T> callback;
		}

		// Token: 0x0200014D RID: 333
		[CompilerGenerated]
		private sealed class <>c__DisplayClass48_0
		{
			// Token: 0x06000A17 RID: 2583 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c__DisplayClass48_0()
			{
			}

			// Token: 0x06000A18 RID: 2584 RVA: 0x00030B2C File Offset: 0x0002ED2C
			internal void <CountAsync>b__0(int statusCode, string responseData, NCMBException error)
			{
				int count = 0;
				if (error == null)
				{
					try
					{
						Dictionary<string, object> dictionary = Json.Deserialize(responseData) as Dictionary<string, object>;
						object value = null;
						if (dictionary.TryGetValue("count", out value))
						{
							count = Convert.ToInt32(value);
						}
					}
					catch (Exception error2)
					{
						error = new NCMBException(error2);
					}
				}
				this.callback(count, error);
			}

			// Token: 0x04000506 RID: 1286
			public NCMBCountCallback callback;
		}
	}
}
