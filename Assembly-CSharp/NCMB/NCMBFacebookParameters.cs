﻿using System;
using System.Collections.Generic;
using NCMB.Internal;

namespace NCMB
{
	// Token: 0x02000057 RID: 87
	[NCMBClassName("facebookParameters")]
	public class NCMBFacebookParameters
	{
		// Token: 0x06000368 RID: 872 RVA: 0x0001912C File Offset: 0x0001732C
		public NCMBFacebookParameters(string userId, string accessToken, DateTime expirationDate)
		{
			if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(accessToken) || string.IsNullOrEmpty(NCMBUtility.encodeDate(expirationDate)))
			{
				throw new NCMBException(new ArgumentException("userId or accessToken or expirationDate must not be null."));
			}
			Dictionary<string, object> value = new Dictionary<string, object>
			{
				{
					"__type",
					"Date"
				},
				{
					"iso",
					NCMBUtility.encodeDate(expirationDate)
				}
			};
			Dictionary<string, object> value2 = new Dictionary<string, object>
			{
				{
					"id",
					userId
				},
				{
					"access_token",
					accessToken
				},
				{
					"expiration_date",
					value
				}
			};
			this.param.Add("facebook", value2);
		}

		// Token: 0x0400014C RID: 332
		internal Dictionary<string, object> param = new Dictionary<string, object>();
	}
}
