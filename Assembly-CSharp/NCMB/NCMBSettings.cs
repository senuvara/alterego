﻿using System;
using NCMB.Internal;
using UnityEngine;

namespace NCMB
{
	// Token: 0x02000049 RID: 73
	public class NCMBSettings : MonoBehaviour
	{
		// Token: 0x1700002D RID: 45
		// (get) Token: 0x0600030D RID: 781 RVA: 0x00018816 File Offset: 0x00016A16
		// (set) Token: 0x0600030E RID: 782 RVA: 0x0001881D File Offset: 0x00016A1D
		internal static string CurrentUser
		{
			get
			{
				return NCMBSettings._currentUser;
			}
			set
			{
				NCMBSettings._currentUser = value;
			}
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x0600030F RID: 783 RVA: 0x00018825 File Offset: 0x00016A25
		// (set) Token: 0x06000310 RID: 784 RVA: 0x0001882C File Offset: 0x00016A2C
		public static string ApplicationKey
		{
			get
			{
				return NCMBSettings._applicationKey;
			}
			set
			{
				NCMBSettings._applicationKey = value;
			}
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x06000311 RID: 785 RVA: 0x00018834 File Offset: 0x00016A34
		// (set) Token: 0x06000312 RID: 786 RVA: 0x0001883B File Offset: 0x00016A3B
		public static string ClientKey
		{
			get
			{
				return NCMBSettings._clientKey;
			}
			set
			{
				NCMBSettings._clientKey = value;
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x06000313 RID: 787 RVA: 0x00018843 File Offset: 0x00016A43
		public static bool UsePush
		{
			get
			{
				return NCMBSettings._usePush;
			}
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x06000314 RID: 788 RVA: 0x0001884A File Offset: 0x00016A4A
		public static bool UseAnalytics
		{
			get
			{
				return NCMBSettings._useAnalytics;
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x06000315 RID: 789 RVA: 0x00018851 File Offset: 0x00016A51
		// (set) Token: 0x06000316 RID: 790 RVA: 0x00018858 File Offset: 0x00016A58
		internal static string DomainURL
		{
			get
			{
				return NCMBSettings._domainURL;
			}
			set
			{
				NCMBSettings._domainURL = value;
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x06000317 RID: 791 RVA: 0x00018860 File Offset: 0x00016A60
		// (set) Token: 0x06000318 RID: 792 RVA: 0x00018867 File Offset: 0x00016A67
		internal static string APIVersion
		{
			get
			{
				return NCMBSettings._apiVersion;
			}
			set
			{
				NCMBSettings._apiVersion = value;
			}
		}

		// Token: 0x06000319 RID: 793 RVA: 0x0001886F File Offset: 0x00016A6F
		public NCMBSettings()
		{
		}

		// Token: 0x0600031A RID: 794 RVA: 0x000188A3 File Offset: 0x00016AA3
		public static void Initialize(string applicationKey, string clientKey, string domainURL, string apiVersion)
		{
			NCMBSettings._applicationKey = applicationKey;
			NCMBSettings._clientKey = clientKey;
			NCMBSettings._domainURL = (string.IsNullOrEmpty(domainURL) ? CommonConstant.DOMAIN_URL : domainURL);
			NCMBSettings._apiVersion = (string.IsNullOrEmpty(apiVersion) ? CommonConstant.API_VERSION : apiVersion);
		}

		// Token: 0x0600031B RID: 795 RVA: 0x000188DB File Offset: 0x00016ADB
		private static void RegisterPush(bool usePush, bool useAnalytics, bool getLocation = false)
		{
			NCMBSettings._usePush = usePush;
			NCMBSettings._useAnalytics = useAnalytics;
			if (usePush)
			{
				NCMBManager.CreateInstallationProperty();
				if (!getLocation)
				{
					NCMBPush.Register();
					return;
				}
				NCMBPush.RegisterWithLocation();
			}
		}

		// Token: 0x0600031C RID: 796 RVA: 0x000188FF File Offset: 0x00016AFF
		public static void EnableResponseValidation(bool checkFlag)
		{
			NCMBSettings._responseValidationFlag = checkFlag;
		}

		// Token: 0x0600031D RID: 797 RVA: 0x00018908 File Offset: 0x00016B08
		public virtual void Awake()
		{
			if (!NCMBSettings._isInitialized)
			{
				NCMBSettings._isInitialized = true;
				NCMBSettings._responseValidationFlag = this.responseValidation;
				Object.DontDestroyOnLoad(base.gameObject);
				NCMBSettings.Initialize(this.applicationKey, this.clientKey, this.domainURL, this.apiVersion);
				NCMBSettings.filePath = Application.persistentDataPath;
				NCMBSettings.currentInstallationPath = NCMBSettings.filePath + "/currentInstallation";
				NCMBSettings.RegisterPush(this.usePush, this.useAnalytics, false);
			}
		}

		// Token: 0x0600031E RID: 798 RVA: 0x00018985 File Offset: 0x00016B85
		internal void Connection(NCMBConnection connection, object callback)
		{
			base.StartCoroutine(NCMBConnection.SendRequest(connection, connection._request, callback));
		}

		// Token: 0x0600031F RID: 799 RVA: 0x0001899C File Offset: 0x00016B9C
		// Note: this type is marked as 'beforefieldinit'.
		static NCMBSettings()
		{
		}

		// Token: 0x04000116 RID: 278
		private static string _applicationKey = "";

		// Token: 0x04000117 RID: 279
		private static string _clientKey = "";

		// Token: 0x04000118 RID: 280
		internal static bool _responseValidationFlag = false;

		// Token: 0x04000119 RID: 281
		internal static bool _isInitialized = false;

		// Token: 0x0400011A RID: 282
		private static bool _usePush = false;

		// Token: 0x0400011B RID: 283
		private static bool _useAnalytics = false;

		// Token: 0x0400011C RID: 284
		private static string _domainURL = "";

		// Token: 0x0400011D RID: 285
		private static string _apiVersion = "";

		// Token: 0x0400011E RID: 286
		internal string applicationKey = "a416aff3f1a784b55ff9638f243d78dc962b1da30b8eabf072d1cf0c7009e5cc";

		// Token: 0x0400011F RID: 287
		internal string clientKey = "deb34de45084b86952d73d2a999e8ac291a0bef464804a3ee43b0edb4bb47722";

		// Token: 0x04000120 RID: 288
		[SerializeField]
		internal bool usePush;

		// Token: 0x04000121 RID: 289
		[SerializeField]
		internal bool useAnalytics;

		// Token: 0x04000122 RID: 290
		[SerializeField]
		internal bool responseValidation;

		// Token: 0x04000123 RID: 291
		internal string domainURL = "";

		// Token: 0x04000124 RID: 292
		internal string apiVersion = "";

		// Token: 0x04000125 RID: 293
		private static string _currentUser = null;

		// Token: 0x04000126 RID: 294
		internal static string filePath = "";

		// Token: 0x04000127 RID: 295
		internal static string currentInstallationPath = "";
	}
}
