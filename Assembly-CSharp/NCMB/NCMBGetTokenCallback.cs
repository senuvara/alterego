﻿using System;

namespace NCMB
{
	// Token: 0x02000055 RID: 85
	// (Invoke) Token: 0x0600035C RID: 860
	public delegate void NCMBGetTokenCallback(string token, NCMBException error);
}
