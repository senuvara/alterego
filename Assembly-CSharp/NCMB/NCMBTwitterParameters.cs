﻿using System;
using System.Collections.Generic;
using NCMB.Internal;

namespace NCMB
{
	// Token: 0x02000061 RID: 97
	[NCMBClassName("twitterParameters")]
	public class NCMBTwitterParameters
	{
		// Token: 0x06000450 RID: 1104 RVA: 0x0001CFE8 File Offset: 0x0001B1E8
		public NCMBTwitterParameters(string userId, string screenName, string consumerKey, string consumerSecret, string accessToken, string accessTokenSecret)
		{
			if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(screenName) || string.IsNullOrEmpty(consumerKey) || string.IsNullOrEmpty(consumerSecret) || string.IsNullOrEmpty(accessToken) || string.IsNullOrEmpty(accessTokenSecret))
			{
				throw new NCMBException(new ArgumentException("constructor parameters must not be null."));
			}
			Dictionary<string, object> value = new Dictionary<string, object>
			{
				{
					"id",
					userId
				},
				{
					"screen_name",
					screenName
				},
				{
					"oauth_consumer_key",
					consumerKey
				},
				{
					"consumer_secret",
					consumerSecret
				},
				{
					"oauth_token",
					accessToken
				},
				{
					"oauth_token_secret",
					accessTokenSecret
				}
			};
			this.param.Add("twitter", value);
		}

		// Token: 0x0400016D RID: 365
		internal Dictionary<string, object> param = new Dictionary<string, object>();
	}
}
