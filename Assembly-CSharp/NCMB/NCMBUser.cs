﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using MiniJSON;
using NCMB.Internal;
using UnityEngine.Networking;

namespace NCMB
{
	// Token: 0x02000062 RID: 98
	[NCMBClassName("user")]
	public class NCMBUser : NCMBObject
	{
		// Token: 0x17000065 RID: 101
		// (get) Token: 0x06000451 RID: 1105 RVA: 0x0001D0AB File Offset: 0x0001B2AB
		// (set) Token: 0x06000452 RID: 1106 RVA: 0x0001D0BD File Offset: 0x0001B2BD
		public string UserName
		{
			get
			{
				return (string)this["userName"];
			}
			set
			{
				this["userName"] = value;
			}
		}

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x06000453 RID: 1107 RVA: 0x0001D0CB File Offset: 0x0001B2CB
		// (set) Token: 0x06000454 RID: 1108 RVA: 0x0001D0DD File Offset: 0x0001B2DD
		public string Email
		{
			get
			{
				return (string)this["mailAddress"];
			}
			set
			{
				this["mailAddress"] = value;
			}
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x06000455 RID: 1109 RVA: 0x0001D0EB File Offset: 0x0001B2EB
		// (set) Token: 0x06000456 RID: 1110 RVA: 0x0001D100 File Offset: 0x0001B300
		public string Password
		{
			private get
			{
				return (string)this["password"];
			}
			set
			{
				lock (this.mutex)
				{
					this["password"] = value;
					base.IsDirty = true;
				}
			}
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x06000457 RID: 1111 RVA: 0x0001D148 File Offset: 0x0001B348
		// (set) Token: 0x06000458 RID: 1112 RVA: 0x0001D15A File Offset: 0x0001B35A
		public Dictionary<string, object> AuthData
		{
			get
			{
				return (Dictionary<string, object>)this["authData"];
			}
			internal set
			{
				this["authData"] = value;
			}
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x06000459 RID: 1113 RVA: 0x0001D168 File Offset: 0x0001B368
		// (set) Token: 0x0600045A RID: 1114 RVA: 0x0001D17A File Offset: 0x0001B37A
		public string SessionToken
		{
			get
			{
				return (string)this["sessionToken"];
			}
			internal set
			{
				this["sessionToken"] = value;
			}
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x0600045B RID: 1115 RVA: 0x0001D188 File Offset: 0x0001B388
		public static NCMBUser CurrentUser
		{
			get
			{
				if (NCMBUser._currentUser != null)
				{
					return NCMBUser._currentUser;
				}
				NCMBUser ncmbuser = (NCMBUser)NCMBObject._getFromVariable();
				if (ncmbuser == null)
				{
					ncmbuser = (NCMBUser)NCMBObject._getFromDisk("currentUser");
				}
				if (ncmbuser != null)
				{
					NCMBUser._currentUser = ncmbuser;
					NCMBUser._currentUser._isCurrentUser = true;
					return NCMBUser._currentUser;
				}
				return null;
			}
		}

		// Token: 0x0600045C RID: 1116 RVA: 0x0001D1DD File Offset: 0x0001B3DD
		public NCMBUser()
		{
			this._isCurrentUser = false;
		}

		// Token: 0x0600045D RID: 1117 RVA: 0x0001D1EC File Offset: 0x0001B3EC
		internal override void _onSettingValue(string key, object value)
		{
			base._onSettingValue(key, value);
		}

		// Token: 0x0600045E RID: 1118 RVA: 0x0001D1F8 File Offset: 0x0001B3F8
		public override void Add(string key, object value)
		{
			if ("userName".Equals(key))
			{
				throw new NCMBException("userName key is already exist. Use this.UserName to set it");
			}
			if ("password".Equals(key))
			{
				throw new NCMBException("password key is already exist. Use this.Password to set it");
			}
			if ("mailAddress".Equals(key))
			{
				throw new NCMBException("mailAdress key is already exist. Use this.Email to set it");
			}
			base.Add(key, value);
		}

		// Token: 0x0600045F RID: 1119 RVA: 0x0001D255 File Offset: 0x0001B455
		public override void Remove(string key)
		{
			if ("userName".Equals(key))
			{
				throw new NCMBException("Can not remove the userName key");
			}
			if ("password".Equals(key))
			{
				throw new NCMBException("Can not remove the Password key");
			}
			base.Remove(key);
		}

		// Token: 0x06000460 RID: 1120 RVA: 0x0001D28E File Offset: 0x0001B48E
		public static NCMBQuery<NCMBUser> GetQuery()
		{
			return NCMBQuery<NCMBUser>.GetQuery("user");
		}

		// Token: 0x06000461 RID: 1121 RVA: 0x0001D29A File Offset: 0x0001B49A
		internal override string _getBaseUrl()
		{
			return NCMBSettings.DomainURL + "/" + NCMBSettings.APIVersion + "/users";
		}

		// Token: 0x06000462 RID: 1122 RVA: 0x0001D2B5 File Offset: 0x0001B4B5
		internal static string _getLogInUrl()
		{
			return NCMBSettings.DomainURL + "/" + NCMBSettings.APIVersion + "/login";
		}

		// Token: 0x06000463 RID: 1123 RVA: 0x0001D2D0 File Offset: 0x0001B4D0
		internal static string _getLogOutUrl()
		{
			return NCMBSettings.DomainURL + "/" + NCMBSettings.APIVersion + "/logout";
		}

		// Token: 0x06000464 RID: 1124 RVA: 0x0001D2EB File Offset: 0x0001B4EB
		internal static string _getRequestPasswordResetUrl()
		{
			return NCMBSettings.DomainURL + "/" + NCMBSettings.APIVersion + "/requestPasswordReset";
		}

		// Token: 0x06000465 RID: 1125 RVA: 0x0001D306 File Offset: 0x0001B506
		private static string _getmailAddressUserEntryUrl()
		{
			return NCMBSettings.DomainURL + "/" + NCMBSettings.APIVersion + "/requestMailAddressUserEntry";
		}

		// Token: 0x06000466 RID: 1126 RVA: 0x0001D321 File Offset: 0x0001B521
		internal override void _afterSave(int statusCode, NCMBException error)
		{
			if ((statusCode == 201 || statusCode == 200) && error == null)
			{
				NCMBUser._saveCurrentUser(this);
			}
		}

		// Token: 0x06000467 RID: 1127 RVA: 0x0001D33C File Offset: 0x0001B53C
		internal override void _afterDelete(NCMBException error)
		{
			if (error == null)
			{
				NCMBUser._logOutEvent();
			}
		}

		// Token: 0x06000468 RID: 1128 RVA: 0x0001A88A File Offset: 0x00018A8A
		public override void DeleteAsync()
		{
			this.DeleteAsync(null);
		}

		// Token: 0x06000469 RID: 1129 RVA: 0x0001D346 File Offset: 0x0001B546
		public override void DeleteAsync(NCMBCallback callback)
		{
			base.DeleteAsync(callback);
		}

		// Token: 0x0600046A RID: 1130 RVA: 0x0001D34F File Offset: 0x0001B54F
		public void SignUpAsync(NCMBCallback callback)
		{
			base.SaveAsync(callback);
		}

		// Token: 0x0600046B RID: 1131 RVA: 0x0001D358 File Offset: 0x0001B558
		public void SignUpAsync()
		{
			this.SignUpAsync(null);
		}

		// Token: 0x0600046C RID: 1132 RVA: 0x00019332 File Offset: 0x00017532
		public override void SaveAsync()
		{
			this.SaveAsync(null);
		}

		// Token: 0x0600046D RID: 1133 RVA: 0x0001D34F File Offset: 0x0001B54F
		public override void SaveAsync(NCMBCallback callback)
		{
			base.SaveAsync(callback);
		}

		// Token: 0x0600046E RID: 1134 RVA: 0x0001D364 File Offset: 0x0001B564
		internal static void _saveCurrentUser(NCMBUser user)
		{
			if (NCMBUser._currentUser != null)
			{
				lock (NCMBUser._currentUser.mutex)
				{
					if (NCMBUser._currentUser != null && NCMBUser._currentUser != user)
					{
						NCMBUser._logOutEvent();
					}
				}
			}
			lock (user.mutex)
			{
				user._isCurrentUser = true;
				user._saveToDisk("currentUser");
				user._saveToVariable();
				NCMBUser._currentUser = user;
			}
		}

		// Token: 0x0600046F RID: 1135 RVA: 0x0001D3F8 File Offset: 0x0001B5F8
		internal static void _logOutEvent()
		{
			string path = NCMBSettings.filePath + "/currentUser";
			if (NCMBUser._currentUser != null)
			{
				NCMBUser._currentUser._isCurrentUser = false;
			}
			NCMBUser._currentUser = null;
			try
			{
				if (File.Exists(path))
				{
					File.Delete(path);
				}
				NCMBSettings.CurrentUser = "";
			}
			catch (Exception error)
			{
				throw new NCMBException(error);
			}
		}

		// Token: 0x06000470 RID: 1136 RVA: 0x0001D460 File Offset: 0x0001B660
		internal static string _getCurrentSessionToken()
		{
			if (NCMBUser.CurrentUser != null)
			{
				return NCMBUser.CurrentUser.SessionToken;
			}
			return "";
		}

		// Token: 0x06000471 RID: 1137 RVA: 0x0001D479 File Offset: 0x0001B679
		public bool IsAuthenticated()
		{
			return this.SessionToken != null && NCMBUser.CurrentUser != null && NCMBUser.CurrentUser.ObjectId.Equals(base.ObjectId);
		}

		// Token: 0x06000472 RID: 1138 RVA: 0x0001D4A1 File Offset: 0x0001B6A1
		public static void RequestPasswordResetAsync(string email)
		{
			NCMBUser.RequestPasswordResetAsync(email, null);
		}

		// Token: 0x06000473 RID: 1139 RVA: 0x0001D4AA File Offset: 0x0001B6AA
		public static void RequestPasswordResetAsync(string email, NCMBCallback callback)
		{
			NCMBUser._requestPasswordReset(email, callback);
		}

		// Token: 0x06000474 RID: 1140 RVA: 0x0001D4B4 File Offset: 0x0001B6B4
		internal static void _requestPasswordReset(string email, NCMBCallback callback)
		{
			string url = NCMBUser._getRequestPasswordResetUrl();
			ConnectType method = ConnectType.POST;
			NCMBUser ncmbuser = new NCMBUser();
			ncmbuser.Email = email;
			string content = ncmbuser._toJSONObjectForSaving(ncmbuser.StartSave());
			new NCMBConnection(url, method, content, NCMBUser._getCurrentSessionToken()).Connect(delegate(int statusCode, string responseData, NCMBException error)
			{
				try
				{
				}
				catch (Exception error2)
				{
					error = new NCMBException(error2);
				}
				if (callback != null)
				{
					callback(error);
				}
			});
		}

		// Token: 0x06000475 RID: 1141 RVA: 0x0001D50A File Offset: 0x0001B70A
		public static void LogInAsync(string name, string password)
		{
			NCMBUser.LogInAsync(name, password, null);
		}

		// Token: 0x06000476 RID: 1142 RVA: 0x0001D514 File Offset: 0x0001B714
		public static void LogInAsync(string name, string password, NCMBCallback callback)
		{
			NCMBUser._ncmbLogIn(name, password, null, callback);
		}

		// Token: 0x06000477 RID: 1143 RVA: 0x0001D520 File Offset: 0x0001B720
		private static void _ncmbLogIn(string name, string password, string email, NCMBCallback callback)
		{
			string str = NCMBUser._getLogInUrl();
			ConnectType method = ConnectType.GET;
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary["password"] = password;
			if (name != null)
			{
				dictionary["userName"] = name;
			}
			else
			{
				if (email == null)
				{
					throw new NCMBException(new ArgumentException("UserName or Email can not be null."));
				}
				dictionary["mailAddress"] = email;
			}
			new NCMBConnection(NCMBUser._makeParamUrl(str + "?", dictionary), method, null, null).Connect(delegate(int statusCode, string responseData, NCMBException error)
			{
				try
				{
					if (error == null)
					{
						Dictionary<string, object> responseDic = Json.Deserialize(responseData) as Dictionary<string, object>;
						NCMBUser ncmbuser = new NCMBUser();
						ncmbuser._handleFetchResult(true, responseDic);
						NCMBUser._saveCurrentUser(ncmbuser);
					}
				}
				catch (Exception error2)
				{
					error = new NCMBException(error2);
				}
				if (callback != null)
				{
					callback(error);
				}
			});
		}

		// Token: 0x06000478 RID: 1144 RVA: 0x0001D5B0 File Offset: 0x0001B7B0
		private static string _makeParamUrl(string url, Dictionary<string, object> parameter)
		{
			string text = url;
			foreach (KeyValuePair<string, object> keyValuePair in parameter)
			{
				text = string.Concat(new string[]
				{
					text,
					keyValuePair.Key,
					"=",
					UnityWebRequest.EscapeURL((string)keyValuePair.Value),
					"&"
				});
			}
			if (parameter.Count > 0)
			{
				text = text.Remove(text.Length - 1);
			}
			return text;
		}

		// Token: 0x06000479 RID: 1145 RVA: 0x0001D650 File Offset: 0x0001B850
		public static void LogInWithMailAddressAsync(string email, string password, NCMBCallback callback)
		{
			NCMBUser._ncmbLogIn(null, password, email, callback);
		}

		// Token: 0x0600047A RID: 1146 RVA: 0x0001D65B File Offset: 0x0001B85B
		public static void LogInWithMailAddressAsync(string email, string password)
		{
			NCMBUser._ncmbLogIn(null, password, email, null);
		}

		// Token: 0x0600047B RID: 1147 RVA: 0x0001D666 File Offset: 0x0001B866
		public static void RequestAuthenticationMailAsync(string email)
		{
			NCMBUser.RequestAuthenticationMailAsync(email, null);
		}

		// Token: 0x0600047C RID: 1148 RVA: 0x0001D670 File Offset: 0x0001B870
		public static void RequestAuthenticationMailAsync(string email, NCMBCallback callback)
		{
			string url = NCMBUser._getmailAddressUserEntryUrl();
			NCMBUser ncmbuser = new NCMBUser();
			ncmbuser.Email = email;
			string content = ncmbuser._toJSONObjectForSaving(ncmbuser.StartSave());
			ConnectType method = ConnectType.POST;
			new NCMBConnection(url, method, content, NCMBUser._getCurrentSessionToken()).Connect(delegate(int statusCode, string responseData, NCMBException error)
			{
				if (callback != null)
				{
					callback(error);
				}
			});
		}

		// Token: 0x0600047D RID: 1149 RVA: 0x0001D6C6 File Offset: 0x0001B8C6
		public static void LogOutAsync()
		{
			NCMBUser.LogOutAsync(null);
		}

		// Token: 0x0600047E RID: 1150 RVA: 0x0001D6D0 File Offset: 0x0001B8D0
		public static void LogOutAsync(NCMBCallback callback)
		{
			if (NCMBUser._currentUser != null)
			{
				NCMBUser._logOut(callback);
				return;
			}
			try
			{
				NCMBUser._logOutEvent();
			}
			catch (NCMBException error)
			{
				if (callback != null)
				{
					callback(error);
				}
				return;
			}
			if (callback != null)
			{
				callback(null);
			}
		}

		// Token: 0x0600047F RID: 1151 RVA: 0x0001D71C File Offset: 0x0001B91C
		internal static void _logOut(NCMBCallback callback)
		{
			string url = NCMBUser._getLogOutUrl();
			ConnectType method = ConnectType.GET;
			string content = null;
			new NCMBConnection(url, method, content, NCMBUser._getCurrentSessionToken()).Connect(delegate(int statusCode, string responseData, NCMBException error)
			{
				try
				{
					if (error == null)
					{
						NCMBUser._logOutEvent();
					}
				}
				catch (Exception error2)
				{
					error = new NCMBException(error2);
				}
				if (callback != null)
				{
					if (error != null && NCMBException.INCORRECT_HEADER.Equals(error.ErrorCode))
					{
						callback(null);
						return;
					}
					callback(error);
				}
			});
		}

		// Token: 0x06000480 RID: 1152 RVA: 0x0001D75C File Offset: 0x0001B95C
		internal override void _mergeFromServer(Dictionary<string, object> responseDic, bool completeData)
		{
			base._mergeFromServer(responseDic, completeData);
		}

		// Token: 0x06000481 RID: 1153 RVA: 0x0001D768 File Offset: 0x0001B968
		public void LogInWithAuthDataAsync(NCMBCallback callback)
		{
			if (this.AuthData == null)
			{
				throw new NCMBException(new ArgumentException("Post authData not exist"));
			}
			this.SignUpAsync(delegate(NCMBException error)
			{
				if (error != null)
				{
					this.AuthData.Clear();
				}
				if (callback != null)
				{
					callback(error);
				}
			});
		}

		// Token: 0x06000482 RID: 1154 RVA: 0x0001D7B3 File Offset: 0x0001B9B3
		public void LogInWithAuthDataAsync()
		{
			this.LogInWithAuthDataAsync(null);
		}

		// Token: 0x06000483 RID: 1155 RVA: 0x0001D7BC File Offset: 0x0001B9BC
		public void LinkWithAuthDataAsync(Dictionary<string, object> linkParam, NCMBCallback callback)
		{
			if (this.AuthData == null)
			{
				this.AuthData = linkParam;
				this.LogInWithAuthDataAsync(callback);
			}
			Dictionary<string, object> currentParam = new Dictionary<string, object>();
			currentParam = this.AuthData;
			this.AuthData = linkParam;
			this.SignUpAsync(delegate(NCMBException error)
			{
				if (error == null)
				{
					Dictionary<string, object> authData = linkParam.Concat(currentParam).ToDictionary((KeyValuePair<string, object> x) => x.Key, (KeyValuePair<string, object> x) => x.Value);
					this.AuthData = authData;
				}
				else
				{
					this.AuthData = currentParam;
				}
				if (callback != null)
				{
					callback(error);
				}
			});
		}

		// Token: 0x06000484 RID: 1156 RVA: 0x0001D839 File Offset: 0x0001BA39
		public void LinkWithAuthDataAsync(Dictionary<string, object> linkParam)
		{
			this.LinkWithAuthDataAsync(linkParam, null);
		}

		// Token: 0x06000485 RID: 1157 RVA: 0x0001D844 File Offset: 0x0001BA44
		public void UnLinkWithAuthDataAsync(string provider, NCMBCallback callback)
		{
			if (this.AuthData == null)
			{
				throw new NCMBException(new ArgumentException("Current User authData not exist"));
			}
			List<string> list = new List<string>
			{
				"facebook",
				"twitter"
			};
			if (string.IsNullOrEmpty(provider) || !list.Contains(provider))
			{
				throw new NCMBException(new ArgumentException("Provider must be facebook or twitter"));
			}
			Dictionary<string, object> currentParam = new Dictionary<string, object>();
			currentParam = this.AuthData;
			Dictionary<string, object> authData = new Dictionary<string, object>
			{
				{
					provider,
					null
				}
			};
			this.AuthData = authData;
			this.SignUpAsync(delegate(NCMBException error)
			{
				if (error == null)
				{
					currentParam.Remove(provider);
					this.AuthData = currentParam;
				}
				else
				{
					this.AuthData = currentParam;
				}
				if (callback != null)
				{
					callback(error);
				}
			});
		}

		// Token: 0x06000486 RID: 1158 RVA: 0x0001D90E File Offset: 0x0001BB0E
		public void UnLinkWithAuthDataAsync(string provider)
		{
			this.UnLinkWithAuthDataAsync(provider, null);
		}

		// Token: 0x06000487 RID: 1159 RVA: 0x0001D918 File Offset: 0x0001BB18
		public bool IsLinkWith(string provider)
		{
			List<string> list = new List<string>
			{
				"facebook",
				"twitter"
			};
			if (string.IsNullOrEmpty(provider) || !list.Contains(provider))
			{
				throw new NCMBException(new ArgumentException("Provider must be facebook or twitter"));
			}
			return this.AuthData != null && this.AuthData.ContainsKey(provider);
		}

		// Token: 0x06000488 RID: 1160 RVA: 0x0001D978 File Offset: 0x0001BB78
		public Dictionary<string, object> GetAuthDataForProvider(string provider)
		{
			List<string> list = new List<string>
			{
				"facebook",
				"twitter"
			};
			if (string.IsNullOrEmpty(provider) || !list.Contains(provider))
			{
				throw new NCMBException(new ArgumentException("Provider must be facebook or twitter"));
			}
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			if (!(provider == "facebook"))
			{
				if (provider == "twitter")
				{
					Dictionary<string, object> dictionary2 = (Dictionary<string, object>)((Dictionary<string, object>)this["authData"])["twitter"];
					dictionary.Add("id", dictionary2["id"]);
					dictionary.Add("screen_name", dictionary2["screen_name"]);
					dictionary.Add("oauth_consumer_key", dictionary2["oauth_consumer_key"]);
					dictionary.Add("consumer_secret", dictionary2["consumer_secret"]);
					dictionary.Add("oauth_token", dictionary2["oauth_token"]);
					dictionary.Add("oauth_token_secret", dictionary2["oauth_token_secret"]);
				}
			}
			else
			{
				Dictionary<string, object> dictionary3 = (Dictionary<string, object>)((Dictionary<string, object>)this["authData"])["facebook"];
				dictionary.Add("id", dictionary3["id"]);
				dictionary.Add("access_token", dictionary3["access_token"]);
				dictionary.Add("expiration_date", NCMBUtility.encodeDate((DateTime)dictionary3["expiration_date"]));
			}
			return dictionary;
		}

		// Token: 0x06000489 RID: 1161 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		// Note: this type is marked as 'beforefieldinit'.
		static NCMBUser()
		{
		}

		// Token: 0x0400016E RID: 366
		private static NCMBUser _currentUser;

		// Token: 0x0400016F RID: 367
		internal bool _isCurrentUser;

		// Token: 0x02000150 RID: 336
		[CompilerGenerated]
		private sealed class <>c__DisplayClass43_0
		{
			// Token: 0x06000A1C RID: 2588 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c__DisplayClass43_0()
			{
			}

			// Token: 0x06000A1D RID: 2589 RVA: 0x00030B94 File Offset: 0x0002ED94
			internal void <_requestPasswordReset>b__0(int statusCode, string responseData, NCMBException error)
			{
				try
				{
				}
				catch (Exception error2)
				{
					error = new NCMBException(error2);
				}
				if (this.callback != null)
				{
					this.callback(error);
				}
			}

			// Token: 0x0400050E RID: 1294
			public NCMBCallback callback;
		}

		// Token: 0x02000151 RID: 337
		[CompilerGenerated]
		private sealed class <>c__DisplayClass46_0
		{
			// Token: 0x06000A1E RID: 2590 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c__DisplayClass46_0()
			{
			}

			// Token: 0x06000A1F RID: 2591 RVA: 0x00030BD4 File Offset: 0x0002EDD4
			internal void <_ncmbLogIn>b__0(int statusCode, string responseData, NCMBException error)
			{
				try
				{
					if (error == null)
					{
						Dictionary<string, object> responseDic = Json.Deserialize(responseData) as Dictionary<string, object>;
						NCMBUser ncmbuser = new NCMBUser();
						ncmbuser._handleFetchResult(true, responseDic);
						NCMBUser._saveCurrentUser(ncmbuser);
					}
				}
				catch (Exception error2)
				{
					error = new NCMBException(error2);
				}
				if (this.callback != null)
				{
					this.callback(error);
				}
			}

			// Token: 0x0400050F RID: 1295
			public NCMBCallback callback;
		}

		// Token: 0x02000152 RID: 338
		[CompilerGenerated]
		private sealed class <>c__DisplayClass51_0
		{
			// Token: 0x06000A20 RID: 2592 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c__DisplayClass51_0()
			{
			}

			// Token: 0x06000A21 RID: 2593 RVA: 0x00030C34 File Offset: 0x0002EE34
			internal void <RequestAuthenticationMailAsync>b__0(int statusCode, string responseData, NCMBException error)
			{
				if (this.callback != null)
				{
					this.callback(error);
				}
			}

			// Token: 0x04000510 RID: 1296
			public NCMBCallback callback;
		}

		// Token: 0x02000153 RID: 339
		[CompilerGenerated]
		private sealed class <>c__DisplayClass54_0
		{
			// Token: 0x06000A22 RID: 2594 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c__DisplayClass54_0()
			{
			}

			// Token: 0x06000A23 RID: 2595 RVA: 0x00030C4C File Offset: 0x0002EE4C
			internal void <_logOut>b__0(int statusCode, string responseData, NCMBException error)
			{
				try
				{
					if (error == null)
					{
						NCMBUser._logOutEvent();
					}
				}
				catch (Exception error2)
				{
					error = new NCMBException(error2);
				}
				if (this.callback != null)
				{
					if (error != null && NCMBException.INCORRECT_HEADER.Equals(error.ErrorCode))
					{
						this.callback(null);
						return;
					}
					this.callback(error);
				}
			}

			// Token: 0x04000511 RID: 1297
			public NCMBCallback callback;
		}

		// Token: 0x02000154 RID: 340
		[CompilerGenerated]
		private sealed class <>c__DisplayClass56_0
		{
			// Token: 0x06000A24 RID: 2596 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c__DisplayClass56_0()
			{
			}

			// Token: 0x06000A25 RID: 2597 RVA: 0x00030CB4 File Offset: 0x0002EEB4
			internal void <LogInWithAuthDataAsync>b__0(NCMBException error)
			{
				if (error != null)
				{
					this.<>4__this.AuthData.Clear();
				}
				if (this.callback != null)
				{
					this.callback(error);
				}
			}

			// Token: 0x04000512 RID: 1298
			public NCMBUser <>4__this;

			// Token: 0x04000513 RID: 1299
			public NCMBCallback callback;
		}

		// Token: 0x02000155 RID: 341
		[CompilerGenerated]
		private sealed class <>c__DisplayClass58_0
		{
			// Token: 0x06000A26 RID: 2598 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c__DisplayClass58_0()
			{
			}

			// Token: 0x06000A27 RID: 2599 RVA: 0x00030CE0 File Offset: 0x0002EEE0
			internal void <LinkWithAuthDataAsync>b__0(NCMBException error)
			{
				if (error == null)
				{
					Dictionary<string, object> authData = this.linkParam.Concat(this.currentParam).ToDictionary(new Func<KeyValuePair<string, object>, string>(NCMBUser.<>c.<>9.<LinkWithAuthDataAsync>b__58_1), new Func<KeyValuePair<string, object>, object>(NCMBUser.<>c.<>9.<LinkWithAuthDataAsync>b__58_2));
					this.<>4__this.AuthData = authData;
				}
				else
				{
					this.<>4__this.AuthData = this.currentParam;
				}
				if (this.callback != null)
				{
					this.callback(error);
				}
			}

			// Token: 0x04000514 RID: 1300
			public Dictionary<string, object> linkParam;

			// Token: 0x04000515 RID: 1301
			public Dictionary<string, object> currentParam;

			// Token: 0x04000516 RID: 1302
			public NCMBUser <>4__this;

			// Token: 0x04000517 RID: 1303
			public NCMBCallback callback;
		}

		// Token: 0x02000156 RID: 342
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000A28 RID: 2600 RVA: 0x00030D78 File Offset: 0x0002EF78
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000A29 RID: 2601 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c()
			{
			}

			// Token: 0x06000A2A RID: 2602 RVA: 0x00030D84 File Offset: 0x0002EF84
			internal string <LinkWithAuthDataAsync>b__58_1(KeyValuePair<string, object> x)
			{
				return x.Key;
			}

			// Token: 0x06000A2B RID: 2603 RVA: 0x00030D8D File Offset: 0x0002EF8D
			internal object <LinkWithAuthDataAsync>b__58_2(KeyValuePair<string, object> x)
			{
				return x.Value;
			}

			// Token: 0x04000518 RID: 1304
			public static readonly NCMBUser.<>c <>9 = new NCMBUser.<>c();

			// Token: 0x04000519 RID: 1305
			public static Func<KeyValuePair<string, object>, string> <>9__58_1;

			// Token: 0x0400051A RID: 1306
			public static Func<KeyValuePair<string, object>, object> <>9__58_2;
		}

		// Token: 0x02000157 RID: 343
		[CompilerGenerated]
		private sealed class <>c__DisplayClass60_0
		{
			// Token: 0x06000A2C RID: 2604 RVA: 0x000043CE File Offset: 0x000025CE
			public <>c__DisplayClass60_0()
			{
			}

			// Token: 0x06000A2D RID: 2605 RVA: 0x00030D98 File Offset: 0x0002EF98
			internal void <UnLinkWithAuthDataAsync>b__0(NCMBException error)
			{
				if (error == null)
				{
					this.currentParam.Remove(this.provider);
					this.<>4__this.AuthData = this.currentParam;
				}
				else
				{
					this.<>4__this.AuthData = this.currentParam;
				}
				if (this.callback != null)
				{
					this.callback(error);
				}
			}

			// Token: 0x0400051B RID: 1307
			public Dictionary<string, object> currentParam;

			// Token: 0x0400051C RID: 1308
			public string provider;

			// Token: 0x0400051D RID: 1309
			public NCMBUser <>4__this;

			// Token: 0x0400051E RID: 1310
			public NCMBCallback callback;
		}
	}
}
