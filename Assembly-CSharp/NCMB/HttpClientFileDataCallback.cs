﻿using System;

namespace NCMB
{
	// Token: 0x02000052 RID: 82
	// (Invoke) Token: 0x06000350 RID: 848
	internal delegate void HttpClientFileDataCallback(int statusCode, byte[] responseData, NCMBException e);
}
