﻿using System;

namespace NCMB
{
	// Token: 0x02000051 RID: 81
	// (Invoke) Token: 0x0600034C RID: 844
	internal delegate void HttpClientCallback(int statusCode, string responseData, NCMBException e);
}
