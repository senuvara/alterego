﻿using System;

namespace NCMB
{
	// Token: 0x02000059 RID: 89
	public struct NCMBGeoPoint
	{
		// Token: 0x1700003B RID: 59
		// (get) Token: 0x06000377 RID: 887 RVA: 0x0001940F File Offset: 0x0001760F
		// (set) Token: 0x06000378 RID: 888 RVA: 0x00019417 File Offset: 0x00017617
		public double Latitude
		{
			get
			{
				return this.latitude;
			}
			set
			{
				if (value > 90.0 || value < -90.0)
				{
					throw new NCMBException(new ArgumentException("Latitude must be within the range -90.0~90.0"));
				}
				this.latitude = value;
			}
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x06000379 RID: 889 RVA: 0x00019448 File Offset: 0x00017648
		// (set) Token: 0x0600037A RID: 890 RVA: 0x00019450 File Offset: 0x00017650
		public double Longitude
		{
			get
			{
				return this.longitude;
			}
			set
			{
				if (value > 180.0 || value < -180.0)
				{
					throw new NCMBException(new ArgumentException("Longitude must be within the range -180~180"));
				}
				this.longitude = value;
			}
		}

		// Token: 0x0600037B RID: 891 RVA: 0x00019481 File Offset: 0x00017681
		public NCMBGeoPoint(double latitude, double longitude)
		{
			this = default(NCMBGeoPoint);
			this.Latitude = latitude;
			this.Longitude = longitude;
		}

		// Token: 0x0400014D RID: 333
		private double latitude;

		// Token: 0x0400014E RID: 334
		private double longitude;
	}
}
