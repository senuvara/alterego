﻿using System;

namespace NCMB
{
	// Token: 0x0200004F RID: 79
	// (Invoke) Token: 0x06000344 RID: 836
	public delegate void NCMBGetCallback<T>(T obj, NCMBException error);
}
