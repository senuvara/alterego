﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x0200002A RID: 42
public class CenterLeftText : MonoBehaviour
{
	// Token: 0x06000192 RID: 402 RVA: 0x0000F880 File Offset: 0x0000DA80
	public virtual void OnEnable()
	{
		float width = base.GetComponent<RectTransform>().rect.width;
		float preferredWidth = base.GetComponent<Text>().preferredWidth;
		base.GetComponent<RectTransform>().anchoredPosition = new Vector2((width - preferredWidth) / 2f, base.GetComponent<RectTransform>().anchoredPosition.y);
	}

	// Token: 0x06000193 RID: 403 RVA: 0x000025AD File Offset: 0x000007AD
	public CenterLeftText()
	{
	}
}
