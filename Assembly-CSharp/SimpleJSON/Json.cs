﻿using System;
using System.Collections;
using System.Text;

namespace SimpleJSON
{
	// Token: 0x02000044 RID: 68
	internal static class Json
	{
		// Token: 0x060002E6 RID: 742 RVA: 0x00015AAA File Offset: 0x00013CAA
		public static string Serialize(object obj)
		{
			return Json.Serializer.Serialize(obj);
		}

		// Token: 0x0200013D RID: 317
		private sealed class Serializer
		{
			// Token: 0x060009D8 RID: 2520 RVA: 0x0002F8E2 File Offset: 0x0002DAE2
			private Serializer()
			{
				this.builder = new StringBuilder();
			}

			// Token: 0x060009D9 RID: 2521 RVA: 0x0002F8F5 File Offset: 0x0002DAF5
			public static string Serialize(object obj)
			{
				Json.Serializer serializer = new Json.Serializer();
				serializer.SerializeValue(obj);
				return serializer.builder.ToString();
			}

			// Token: 0x060009DA RID: 2522 RVA: 0x0002F910 File Offset: 0x0002DB10
			private void SerializeValue(object value)
			{
				if (value == null)
				{
					this.builder.Append("null");
					return;
				}
				string str;
				if ((str = (value as string)) != null)
				{
					this.SerializeString(str);
					return;
				}
				if (value is bool)
				{
					this.builder.Append(((bool)value) ? "true" : "false");
					return;
				}
				IList anArray;
				if ((anArray = (value as IList)) != null)
				{
					this.SerializeArray(anArray);
					return;
				}
				IDictionary obj;
				if ((obj = (value as IDictionary)) != null)
				{
					this.SerializeObject(obj);
					return;
				}
				if (value is char)
				{
					this.SerializeString(new string((char)value, 1));
					return;
				}
				this.SerializeOther(value);
			}

			// Token: 0x060009DB RID: 2523 RVA: 0x0002F9B4 File Offset: 0x0002DBB4
			private void SerializeObject(IDictionary obj)
			{
				bool flag = true;
				this.builder.Append('{');
				foreach (object obj2 in obj.Keys)
				{
					if (!flag)
					{
						this.builder.Append(',');
					}
					this.SerializeString(obj2.ToString());
					this.builder.Append(':');
					this.SerializeValue(obj[obj2]);
					flag = false;
				}
				this.builder.Append('}');
			}

			// Token: 0x060009DC RID: 2524 RVA: 0x0002FA5C File Offset: 0x0002DC5C
			private void SerializeArray(IList anArray)
			{
				this.builder.Append('[');
				bool flag = true;
				foreach (object value in anArray)
				{
					if (!flag)
					{
						this.builder.Append(',');
					}
					this.SerializeValue(value);
					flag = false;
				}
				this.builder.Append(']');
			}

			// Token: 0x060009DD RID: 2525 RVA: 0x0002FADC File Offset: 0x0002DCDC
			private void SerializeString(string str)
			{
				this.builder.Append('"');
				char[] array = str.ToCharArray();
				int i = 0;
				while (i < array.Length)
				{
					char c = array[i];
					switch (c)
					{
					case '\b':
						this.builder.Append("\\b");
						break;
					case '\t':
						this.builder.Append("\\t");
						break;
					case '\n':
						this.builder.Append("\\n");
						break;
					case '\v':
						goto IL_D4;
					case '\f':
						this.builder.Append("\\f");
						break;
					case '\r':
						this.builder.Append("\\r");
						break;
					default:
						if (c != '"')
						{
							if (c != '\\')
							{
								goto IL_D4;
							}
							this.builder.Append("\\\\");
						}
						else
						{
							this.builder.Append("\\\"");
						}
						break;
					}
					IL_E1:
					i++;
					continue;
					IL_D4:
					this.builder.Append(c);
					goto IL_E1;
				}
				this.builder.Append('"');
			}

			// Token: 0x060009DE RID: 2526 RVA: 0x0002FBE8 File Offset: 0x0002DDE8
			private void SerializeOther(object value)
			{
				if (value is float)
				{
					this.builder.Append(((float)value).ToString("R"));
					return;
				}
				if (value is int || value is uint || value is long || value is sbyte || value is byte || value is short || value is ushort || value is ulong)
				{
					this.builder.Append(value);
					return;
				}
				if (value is double || value is decimal)
				{
					this.builder.Append(Convert.ToDouble(value).ToString("R"));
					return;
				}
				this.SerializeString(value.ToString());
			}

			// Token: 0x040004E5 RID: 1253
			private StringBuilder builder;
		}
	}
}
