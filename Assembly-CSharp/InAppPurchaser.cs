﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using App;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Extension;

// Token: 0x02000043 RID: 67
public class InAppPurchaser : MonoBehaviour, IStoreListener
{
	// Token: 0x060002DA RID: 730 RVA: 0x000158E8 File Offset: 0x00013AE8
	public void Awake()
	{
		if (InAppPurchaser.Instance == null)
		{
			InAppPurchaser.Instance = this;
			base.StartCoroutine(this.InitializePurchasing());
		}
	}

	// Token: 0x060002DB RID: 731 RVA: 0x0001590A File Offset: 0x00013B0A
	public IEnumerator InitializePurchasing()
	{
		if (this.IsInitialized())
		{
			yield break;
		}
		if (!this.IsWaiting)
		{
			StandardPurchasingModule standardPurchasingModule = StandardPurchasingModule.Instance();
			standardPurchasingModule.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;
			ConfigurationBuilder configurationBuilder = ConfigurationBuilder.Instance(standardPurchasingModule, Array.Empty<IPurchasingModule>());
			string app_BUNDLE_ID = BuildInfo.APP_BUNDLE_ID;
			foreach (string text in InAppPurchaser.ProductIDList)
			{
				configurationBuilder.AddProduct(text, ProductType.NonConsumable, new IDs
				{
					{
						app_BUNDLE_ID + "." + text,
						new string[]
						{
							"AppleAppStore"
						}
					},
					{
						app_BUNDLE_ID + "." + text,
						new string[]
						{
							"GooglePlay"
						}
					}
				});
			}
			UnityPurchasing.Initialize(this, configurationBuilder);
			this.IsWaiting = true;
		}
		yield return null;
		float waitTime = 0f;
		while (!this.IsInitialized())
		{
			waitTime += Time.unscaledDeltaTime;
			if (waitTime > 60f)
			{
				yield break;
			}
			yield return null;
		}
		yield break;
	}

	// Token: 0x060002DC RID: 732 RVA: 0x00015919 File Offset: 0x00013B19
	private bool IsInitialized()
	{
		return this.m_StoreController != null && this.m_StoreExtensionProvider != null;
	}

	// Token: 0x060002DD RID: 733 RVA: 0x0001592E File Offset: 0x00013B2E
	public static IEnumerator BuyProductID(string productId, Action callback)
	{
		if (InAppPurchaser.Instance == null)
		{
			yield break;
		}
		InAppPurchaser.Instance.CallbackMethod = callback;
		yield return InAppPurchaser.Instance.InitializePurchasing();
		Product product = InAppPurchaser.Instance.m_StoreController.products.WithID(productId);
		if (InAppPurchaser.Instance.IsInitialized() && product != null && product.availableToPurchase)
		{
			InAppPurchaser.Instance.m_StoreController.InitiatePurchase(product);
		}
		else
		{
			global::Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
		}
		yield break;
	}

	// Token: 0x060002DE RID: 734 RVA: 0x00015944 File Offset: 0x00013B44
	public static IEnumerator RestorePurchases(Action callback)
	{
		if (InAppPurchaser.Instance == null)
		{
			yield break;
		}
		InAppPurchaser.Instance.CallbackMethod = callback;
		yield return InAppPurchaser.Instance.InitializePurchasing();
		if (!InAppPurchaser.Instance.IsInitialized())
		{
			global::Debug.Log("RestorePurchases FAIL. Not initialized.");
			yield break;
		}
		if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer)
		{
			InAppPurchaser.Instance.m_StoreExtensionProvider.GetExtension<IAppleExtensions>().RestoreTransactions(delegate(bool result)
			{
				global::Debug.Log("RestorePurchases continuing: " + result.ToString() + ". If no further messages, no purchases available to restore.");
			});
		}
		else
		{
			global::Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
		}
		yield break;
	}

	// Token: 0x060002DF RID: 735 RVA: 0x00015954 File Offset: 0x00013B54
	public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
	{
		global::Debug.Log("OnInitialized: PASS");
		this.m_StoreController = controller;
		this.m_StoreExtensionProvider = extensions;
		foreach (Product product in controller.products.all)
		{
			global::Debug.Log(string.Concat(new string[]
			{
				"product.id=",
				product.definition.id,
				" has=",
				product.hasReceipt.ToString(),
				" available=",
				product.availableToPurchase.ToString(),
				" receipt=",
				product.receipt
			}));
			if (product.hasReceipt)
			{
				PurchasingItem.Set(product.definition.id, true);
			}
		}
		this.Callback();
	}

	// Token: 0x060002E0 RID: 736 RVA: 0x00015A25 File Offset: 0x00013C25
	public void OnInitializeFailed(InitializationFailureReason error)
	{
		global::Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
		this.Callback();
	}

	// Token: 0x060002E1 RID: 737 RVA: 0x00015A42 File Offset: 0x00013C42
	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
	{
		PurchasingItem.Set(args.purchasedProduct.definition.id, true);
		this.Callback();
		return PurchaseProcessingResult.Complete;
	}

	// Token: 0x060002E2 RID: 738 RVA: 0x00015A61 File Offset: 0x00013C61
	public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
	{
		global::Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
		this.Callback();
	}

	// Token: 0x060002E3 RID: 739 RVA: 0x00015A89 File Offset: 0x00013C89
	private void Callback()
	{
		if (this.CallbackMethod != null)
		{
			this.CallbackMethod();
		}
	}

	// Token: 0x060002E4 RID: 740 RVA: 0x000025AD File Offset: 0x000007AD
	public InAppPurchaser()
	{
	}

	// Token: 0x060002E5 RID: 741 RVA: 0x00015A9E File Offset: 0x00013C9E
	// Note: this type is marked as 'beforefieldinit'.
	static InAppPurchaser()
	{
	}

	// Token: 0x04000101 RID: 257
	private static InAppPurchaser Instance;

	// Token: 0x04000102 RID: 258
	private IStoreController m_StoreController;

	// Token: 0x04000103 RID: 259
	private IExtensionProvider m_StoreExtensionProvider;

	// Token: 0x04000104 RID: 260
	private bool IsWaiting;

	// Token: 0x04000105 RID: 261
	private Action CallbackMethod;

	// Token: 0x04000106 RID: 262
	public static readonly string[] ProductIDList = PurchasingItem.ITEM_LIST;

	// Token: 0x02000139 RID: 313
	[CompilerGenerated]
	private sealed class <InitializePurchasing>d__7 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060009C3 RID: 2499 RVA: 0x0002F591 File Offset: 0x0002D791
		[DebuggerHidden]
		public <InitializePurchasing>d__7(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060009C4 RID: 2500 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060009C5 RID: 2501 RVA: 0x0002F5A0 File Offset: 0x0002D7A0
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			InAppPurchaser inAppPurchaser = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				if (inAppPurchaser.IsInitialized())
				{
					return false;
				}
				if (!inAppPurchaser.IsWaiting)
				{
					StandardPurchasingModule standardPurchasingModule = StandardPurchasingModule.Instance();
					standardPurchasingModule.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;
					ConfigurationBuilder configurationBuilder = ConfigurationBuilder.Instance(standardPurchasingModule, Array.Empty<IPurchasingModule>());
					string app_BUNDLE_ID = BuildInfo.APP_BUNDLE_ID;
					foreach (string text in InAppPurchaser.ProductIDList)
					{
						configurationBuilder.AddProduct(text, ProductType.NonConsumable, new IDs
						{
							{
								app_BUNDLE_ID + "." + text,
								new string[]
								{
									"AppleAppStore"
								}
							},
							{
								app_BUNDLE_ID + "." + text,
								new string[]
								{
									"GooglePlay"
								}
							}
						});
					}
					UnityPurchasing.Initialize(inAppPurchaser, configurationBuilder);
					inAppPurchaser.IsWaiting = true;
				}
				this.<>2__current = null;
				this.<>1__state = 1;
				return true;
			case 1:
				this.<>1__state = -1;
				waitTime = 0f;
				break;
			case 2:
				this.<>1__state = -1;
				break;
			default:
				return false;
			}
			if (inAppPurchaser.IsInitialized())
			{
				return false;
			}
			waitTime += Time.unscaledDeltaTime;
			if (waitTime > 60f)
			{
				return false;
			}
			this.<>2__current = null;
			this.<>1__state = 2;
			return true;
		}

		// Token: 0x17000154 RID: 340
		// (get) Token: 0x060009C6 RID: 2502 RVA: 0x0002F6F3 File Offset: 0x0002D8F3
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060009C7 RID: 2503 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000155 RID: 341
		// (get) Token: 0x060009C8 RID: 2504 RVA: 0x0002F6F3 File Offset: 0x0002D8F3
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040004D8 RID: 1240
		private int <>1__state;

		// Token: 0x040004D9 RID: 1241
		private object <>2__current;

		// Token: 0x040004DA RID: 1242
		public InAppPurchaser <>4__this;

		// Token: 0x040004DB RID: 1243
		private float <waitTime>5__2;
	}

	// Token: 0x0200013A RID: 314
	[CompilerGenerated]
	private sealed class <BuyProductID>d__9 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060009C9 RID: 2505 RVA: 0x0002F6FB File Offset: 0x0002D8FB
		[DebuggerHidden]
		public <BuyProductID>d__9(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060009CA RID: 2506 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060009CB RID: 2507 RVA: 0x0002F70C File Offset: 0x0002D90C
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			if (num != 0)
			{
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				Product product = InAppPurchaser.Instance.m_StoreController.products.WithID(productId);
				if (InAppPurchaser.Instance.IsInitialized() && product != null && product.availableToPurchase)
				{
					InAppPurchaser.Instance.m_StoreController.InitiatePurchase(product);
				}
				else
				{
					global::Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
				}
				return false;
			}
			else
			{
				this.<>1__state = -1;
				if (InAppPurchaser.Instance == null)
				{
					return false;
				}
				InAppPurchaser.Instance.CallbackMethod = callback;
				this.<>2__current = InAppPurchaser.Instance.InitializePurchasing();
				this.<>1__state = 1;
				return true;
			}
		}

		// Token: 0x17000156 RID: 342
		// (get) Token: 0x060009CC RID: 2508 RVA: 0x0002F7BE File Offset: 0x0002D9BE
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060009CD RID: 2509 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000157 RID: 343
		// (get) Token: 0x060009CE RID: 2510 RVA: 0x0002F7BE File Offset: 0x0002D9BE
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040004DC RID: 1244
		private int <>1__state;

		// Token: 0x040004DD RID: 1245
		private object <>2__current;

		// Token: 0x040004DE RID: 1246
		public Action callback;

		// Token: 0x040004DF RID: 1247
		public string productId;
	}

	// Token: 0x0200013B RID: 315
	[CompilerGenerated]
	[Serializable]
	private sealed class <>c
	{
		// Token: 0x060009CF RID: 2511 RVA: 0x0002F7C6 File Offset: 0x0002D9C6
		// Note: this type is marked as 'beforefieldinit'.
		static <>c()
		{
		}

		// Token: 0x060009D0 RID: 2512 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c()
		{
		}

		// Token: 0x060009D1 RID: 2513 RVA: 0x0002F7D2 File Offset: 0x0002D9D2
		internal void <RestorePurchases>b__10_0(bool result)
		{
			global::Debug.Log("RestorePurchases continuing: " + result.ToString() + ". If no further messages, no purchases available to restore.");
		}

		// Token: 0x040004E0 RID: 1248
		public static readonly InAppPurchaser.<>c <>9 = new InAppPurchaser.<>c();

		// Token: 0x040004E1 RID: 1249
		public static Action<bool> <>9__10_0;
	}

	// Token: 0x0200013C RID: 316
	[CompilerGenerated]
	private sealed class <RestorePurchases>d__10 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060009D2 RID: 2514 RVA: 0x0002F7EF File Offset: 0x0002D9EF
		[DebuggerHidden]
		public <RestorePurchases>d__10(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060009D3 RID: 2515 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060009D4 RID: 2516 RVA: 0x0002F800 File Offset: 0x0002DA00
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			if (num != 0)
			{
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				if (!InAppPurchaser.Instance.IsInitialized())
				{
					global::Debug.Log("RestorePurchases FAIL. Not initialized.");
					return false;
				}
				if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer)
				{
					InAppPurchaser.Instance.m_StoreExtensionProvider.GetExtension<IAppleExtensions>().RestoreTransactions(new Action<bool>(InAppPurchaser.<>c.<>9.<RestorePurchases>b__10_0));
				}
				else
				{
					global::Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
				}
				return false;
			}
			else
			{
				this.<>1__state = -1;
				if (InAppPurchaser.Instance == null)
				{
					return false;
				}
				InAppPurchaser.Instance.CallbackMethod = callback;
				this.<>2__current = InAppPurchaser.Instance.InitializePurchasing();
				this.<>1__state = 1;
				return true;
			}
		}

		// Token: 0x17000158 RID: 344
		// (get) Token: 0x060009D5 RID: 2517 RVA: 0x0002F8DA File Offset: 0x0002DADA
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060009D6 RID: 2518 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000159 RID: 345
		// (get) Token: 0x060009D7 RID: 2519 RVA: 0x0002F8DA File Offset: 0x0002DADA
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040004E2 RID: 1250
		private int <>1__state;

		// Token: 0x040004E3 RID: 1251
		private object <>2__current;

		// Token: 0x040004E4 RID: 1252
		public Action callback;
	}
}
