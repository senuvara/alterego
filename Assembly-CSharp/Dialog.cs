﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using SocialConnector;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x0200002C RID: 44
public class Dialog : MonoBehaviour
{
	// Token: 0x06000197 RID: 407 RVA: 0x0000F8D8 File Offset: 0x0000DAD8
	protected virtual void Awake()
	{
		if (base.GetComponent<CanvasGroup>() == null)
		{
			base.gameObject.AddComponent<CanvasGroup>();
		}
		AppUtil.SetAlpha(base.gameObject, 0f);
	}

	// Token: 0x06000198 RID: 408 RVA: 0x0000F908 File Offset: 0x0000DB08
	protected virtual bool OnClick(GameObject clickObject)
	{
		global::Debug.Log("Dialog:OnClick " + clickObject.name);
		string name = clickObject.name;
		if (!(name == "CloseButton") && !(name == "BackButton") && !(name == "CancelButton"))
		{
			if (!(name == "CloseAllButton"))
			{
				if (name == "ShareButton")
				{
					SocialConnector.Share((string)this.args[0], (string)this.args[1], (string)this.args[3]);
					this.Close();
					return true;
				}
			}
			else
			{
				DialogManager.CloseAllDialog();
			}
			return false;
		}
		this.Close();
		return true;
	}

	// Token: 0x06000199 RID: 409 RVA: 0x0000F9B8 File Offset: 0x0000DBB8
	protected virtual void Close()
	{
		DialogManager.CloseDialog(base.gameObject.name);
	}

	// Token: 0x0600019A RID: 410 RVA: 0x0000F9CC File Offset: 0x0000DBCC
	protected virtual void OnEnable()
	{
		base.transform.SetAsLastSibling();
		base.GetComponent<CanvasGroup>().blocksRaycasts = true;
		this.Render();
		foreach (OnClickHandler onClickHandler in base.GetComponentsInChildren<OnClickHandler>(true))
		{
			string name = onClickHandler.name;
			if (name == "CloseButton" || name == "BackButton" || name == "CancelButton" || name == "CloseAllButton")
			{
				SceneCommon.BackButtonList.Add(onClickHandler);
			}
		}
	}

	// Token: 0x0600019B RID: 411 RVA: 0x0000FA56 File Offset: 0x0000DC56
	protected virtual void Render()
	{
		base.StartCoroutine(AppUtil.FadeIn(base.gameObject, 0.25f, delegate(Object ret)
		{
			global::Debug.Log("AutoTestEvent:スクリーンショット");
		}));
	}

	// Token: 0x0600019C RID: 412 RVA: 0x0000FA90 File Offset: 0x0000DC90
	protected virtual void OnDisable()
	{
		foreach (OnClickHandler onClickHandler in base.GetComponentsInChildren<OnClickHandler>(true))
		{
			string name = onClickHandler.name;
			if (name == "CloseButton" || name == "BackButton" || name == "CancelButton" || name == "CloseAllButton")
			{
				SceneCommon.BackButtonList.Remove(onClickHandler);
			}
		}
	}

	// Token: 0x0600019D RID: 413 RVA: 0x0000FB00 File Offset: 0x0000DD00
	public virtual void OnShown(params object[] args)
	{
		if (args != null && args.Length != 0)
		{
			this.args = args;
		}
		bool activeSelf = base.gameObject.activeSelf;
		base.gameObject.SetActive(true);
		if (!activeSelf)
		{
			string name = base.gameObject.name;
			if (name == "FakeMovieAdScreen")
			{
				base.transform.Find("CloseButton").gameObject.SetActive(false);
				base.transform.Find("CountDownText").GetComponent<Text>().text = "10";
				base.StartCoroutine("FakeMovie");
				return;
			}
			if (!(name == "ShareDialog"))
			{
				return;
			}
			Texture2D texture2D = (Texture2D)this.args[2];
			Sprite sprite = Sprite.Create(texture2D, new Rect(0f, 0f, (float)texture2D.width, (float)texture2D.height), Vector2.one * 0.5f);
			base.transform.Find("ShareImage").GetComponent<Image>().sprite = sprite;
		}
	}

	// Token: 0x0600019E RID: 414 RVA: 0x0000FC01 File Offset: 0x0000DE01
	public virtual IEnumerator OnClosed()
	{
		base.GetComponent<CanvasGroup>().blocksRaycasts = false;
		yield return AppUtil.FadeOut(base.gameObject, 0.25f, null);
		base.gameObject.SetActive(false);
		yield break;
	}

	// Token: 0x0600019F RID: 415 RVA: 0x0000FC10 File Offset: 0x0000DE10
	private IEnumerator FakeMovie()
	{
		int unit = 10;
		Transform text = base.transform.Find("CountDownText");
		int num;
		for (int i = 9; i >= 0; i = num - 1)
		{
			yield return AppUtil.WaitRealtime(1f - (float)(90 / unit * 2) * 0.01f);
			for (int angle = 0; angle < 90; angle += unit)
			{
				yield return AppUtil.WaitRealtime(0.01f);
				text.Rotate(new Vector3(0f, (float)unit, 0f));
			}
			text.GetComponent<Text>().text = i.ToString();
			text.Rotate(new Vector3(0f, 180f, 0f));
			for (int angle = 0; angle < 90; angle += unit)
			{
				yield return AppUtil.WaitRealtime(0.01f);
				text.Rotate(new Vector3(0f, (float)unit, 0f));
			}
			text.localRotation = Quaternion.identity;
			num = i;
		}
		base.transform.Find("CloseButton").gameObject.SetActive(true);
		yield break;
	}

	// Token: 0x060001A0 RID: 416 RVA: 0x000025AD File Offset: 0x000007AD
	public Dialog()
	{
	}

	// Token: 0x040000A0 RID: 160
	protected object[] args;

	// Token: 0x0200011C RID: 284
	[CompilerGenerated]
	[Serializable]
	private sealed class <>c
	{
		// Token: 0x06000959 RID: 2393 RVA: 0x0002E880 File Offset: 0x0002CA80
		// Note: this type is marked as 'beforefieldinit'.
		static <>c()
		{
		}

		// Token: 0x0600095A RID: 2394 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c()
		{
		}

		// Token: 0x0600095B RID: 2395 RVA: 0x0002E88C File Offset: 0x0002CA8C
		internal void <Render>b__5_0(Object ret)
		{
			global::Debug.Log("AutoTestEvent:スクリーンショット");
		}

		// Token: 0x0400044A RID: 1098
		public static readonly Dialog.<>c <>9 = new Dialog.<>c();

		// Token: 0x0400044B RID: 1099
		public static Action<Object> <>9__5_0;
	}

	// Token: 0x0200011D RID: 285
	[CompilerGenerated]
	private sealed class <OnClosed>d__8 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x0600095C RID: 2396 RVA: 0x0002E898 File Offset: 0x0002CA98
		[DebuggerHidden]
		public <OnClosed>d__8(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x0600095D RID: 2397 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x0600095E RID: 2398 RVA: 0x0002E8A8 File Offset: 0x0002CAA8
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			Dialog dialog = this;
			if (num == 0)
			{
				this.<>1__state = -1;
				dialog.GetComponent<CanvasGroup>().blocksRaycasts = false;
				this.<>2__current = AppUtil.FadeOut(dialog.gameObject, 0.25f, null);
				this.<>1__state = 1;
				return true;
			}
			if (num != 1)
			{
				return false;
			}
			this.<>1__state = -1;
			dialog.gameObject.SetActive(false);
			return false;
		}

		// Token: 0x1700013C RID: 316
		// (get) Token: 0x0600095F RID: 2399 RVA: 0x0002E913 File Offset: 0x0002CB13
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000960 RID: 2400 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x1700013D RID: 317
		// (get) Token: 0x06000961 RID: 2401 RVA: 0x0002E913 File Offset: 0x0002CB13
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0400044C RID: 1100
		private int <>1__state;

		// Token: 0x0400044D RID: 1101
		private object <>2__current;

		// Token: 0x0400044E RID: 1102
		public Dialog <>4__this;
	}

	// Token: 0x0200011E RID: 286
	[CompilerGenerated]
	private sealed class <FakeMovie>d__9 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000962 RID: 2402 RVA: 0x0002E91B File Offset: 0x0002CB1B
		[DebuggerHidden]
		public <FakeMovie>d__9(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000963 RID: 2403 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000964 RID: 2404 RVA: 0x0002E92C File Offset: 0x0002CB2C
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			Dialog dialog = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				unit = 10;
				text = dialog.transform.Find("CountDownText");
				i = 9;
				goto IL_1B3;
			case 1:
				this.<>1__state = -1;
				angle = 0;
				break;
			case 2:
				this.<>1__state = -1;
				text.Rotate(new Vector3(0f, (float)unit, 0f));
				angle += unit;
				break;
			case 3:
				this.<>1__state = -1;
				text.Rotate(new Vector3(0f, (float)unit, 0f));
				angle += unit;
				goto IL_189;
			default:
				return false;
			}
			if (angle < 90)
			{
				this.<>2__current = AppUtil.WaitRealtime(0.01f);
				this.<>1__state = 2;
				return true;
			}
			text.GetComponent<Text>().text = i.ToString();
			text.Rotate(new Vector3(0f, 180f, 0f));
			angle = 0;
			IL_189:
			if (angle < 90)
			{
				this.<>2__current = AppUtil.WaitRealtime(0.01f);
				this.<>1__state = 3;
				return true;
			}
			text.localRotation = Quaternion.identity;
			int num2 = i;
			i = num2 - 1;
			IL_1B3:
			if (i < 0)
			{
				dialog.transform.Find("CloseButton").gameObject.SetActive(true);
				return false;
			}
			this.<>2__current = AppUtil.WaitRealtime(1f - (float)(90 / unit * 2) * 0.01f);
			this.<>1__state = 1;
			return true;
		}

		// Token: 0x1700013E RID: 318
		// (get) Token: 0x06000965 RID: 2405 RVA: 0x0002EB14 File Offset: 0x0002CD14
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000966 RID: 2406 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x1700013F RID: 319
		// (get) Token: 0x06000967 RID: 2407 RVA: 0x0002EB14 File Offset: 0x0002CD14
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0400044F RID: 1103
		private int <>1__state;

		// Token: 0x04000450 RID: 1104
		private object <>2__current;

		// Token: 0x04000451 RID: 1105
		public Dialog <>4__this;

		// Token: 0x04000452 RID: 1106
		private int <unit>5__2;

		// Token: 0x04000453 RID: 1107
		private Transform <text>5__3;

		// Token: 0x04000454 RID: 1108
		private int <i>5__4;

		// Token: 0x04000455 RID: 1109
		private int <angle>5__5;
	}
}
