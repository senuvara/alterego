﻿using System;
using System.Collections;
using System.Text.RegularExpressions;
using UnityEngine;

// Token: 0x0200003A RID: 58
public class AdfurikunMovieInterstitialUtility : MonoBehaviour
{
	// Token: 0x17000021 RID: 33
	// (get) Token: 0x06000253 RID: 595 RVA: 0x00013311 File Offset: 0x00011511
	public static AdfurikunMovieInterstitialUtility instance
	{
		get
		{
			return AdfurikunMovieInterstitialUtility.mInstance;
		}
	}

	// Token: 0x06000254 RID: 596 RVA: 0x00013318 File Offset: 0x00011518
	public void Awake()
	{
		if (AdfurikunMovieInterstitialUtility.mInstance == null)
		{
			AdfurikunMovieInterstitialUtility.mInstance = this;
			return;
		}
		Object.Destroy(base.gameObject);
	}

	// Token: 0x06000255 RID: 597 RVA: 0x00013339 File Offset: 0x00011539
	public void OnDestroy()
	{
		if (Application.isEditor)
		{
			return;
		}
		AdfurikunMovieInterstitialUtility.mInstance == this;
	}

	// Token: 0x06000256 RID: 598 RVA: 0x0001334F File Offset: 0x0001154F
	public void OnApplicationPause(bool pause)
	{
		if (Application.isEditor)
		{
			return;
		}
		if (Application.platform == RuntimePlatform.Android)
		{
			if (pause)
			{
				this.callAndroidMovieInterstitialMethod("onPause");
				return;
			}
			this.callAndroidMovieInterstitialMethod("onResume");
		}
	}

	// Token: 0x06000257 RID: 599 RVA: 0x0001337C File Offset: 0x0001157C
	public void Start()
	{
		if (Application.isEditor)
		{
			return;
		}
		this.initializeMovieInterstitial();
	}

	// Token: 0x06000258 RID: 600 RVA: 0x0001338C File Offset: 0x0001158C
	public void initializeMovieInterstitial()
	{
		this.initializeMovieInterstitial(this.getAppID());
	}

	// Token: 0x06000259 RID: 601 RVA: 0x0001339C File Offset: 0x0001159C
	public void initializeMovieInterstitial(string appId)
	{
		if (!this.isValidAppID(appId))
		{
			return;
		}
		if (Application.platform == RuntimePlatform.Android)
		{
			AndroidJavaObject @static = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
			if (this.mAdfurikunUnityListener == null)
			{
				this.mAdfurikunUnityListener = new AdfurikunMovieInterstitialUtility.AdfurikunUnityListener();
			}
			new AndroidJavaClass("jp.tjkapp.adfurikunsdk.moviereward.unityplugin.AdfurikunUnityManager").CallStatic("initialize", new object[]
			{
				@static,
				this.unityPluginVersion
			});
			this.makeInstance_AdfurikunMovieInterstitialController().CallStatic("initialize", new object[]
			{
				@static,
				appId,
				this.mAdfurikunUnityListener
			});
		}
	}

	// Token: 0x0600025A RID: 602 RVA: 0x00013432 File Offset: 0x00011632
	public bool isPreparedMovieInterstitial()
	{
		return this.isPreparedMovieInterstitial(this.getAppID());
	}

	// Token: 0x0600025B RID: 603 RVA: 0x00013440 File Offset: 0x00011640
	public bool isPreparedMovieInterstitial(string appId)
	{
		return this.isValidAppID(appId) && Application.platform == RuntimePlatform.Android && this.makeInstance_AdfurikunMovieInterstitialController().CallStatic<bool>("isPrepared", new object[]
		{
			appId
		});
	}

	// Token: 0x0600025C RID: 604 RVA: 0x00013472 File Offset: 0x00011672
	[Obsolete("Use Action delegate instead.")]
	public void setMovieInterstitialSrcObject(GameObject movieInterstitialSrcObject)
	{
		this.setMovieInterstitialSrcObject(movieInterstitialSrcObject, this.getAppID());
	}

	// Token: 0x0600025D RID: 605 RVA: 0x00013481 File Offset: 0x00011681
	public void setMovieInterstitialSrcObject(GameObject movieInterstitialSrcObject, string appId)
	{
		if (!this.isValidAppID(appId))
		{
			return;
		}
		this.mMovieInterstitialSrcObject = movieInterstitialSrcObject;
		if (this.isPreparedMovieInterstitial(appId))
		{
			this.onPrepareSuccess.NullSafe(appId);
			this.sendMessage(AdfurikunMovieInterstitialUtility.ADF_MovieStatus.PrepareSuccess, appId, "");
		}
	}

	// Token: 0x0600025E RID: 606 RVA: 0x000134B6 File Offset: 0x000116B6
	public void playMovieInterstitial()
	{
		this.playMovieInterstitial(this.getAppID());
	}

	// Token: 0x0600025F RID: 607 RVA: 0x000134C4 File Offset: 0x000116C4
	public void playMovieInterstitial(string appId)
	{
		if (!this.isValidAppID(appId))
		{
			return;
		}
		if (Application.platform == RuntimePlatform.Android)
		{
			if (!this.isPreparedMovieInterstitial(appId))
			{
				this.onNotPrepared.NullSafe(appId);
				this.sendMessage(AdfurikunMovieInterstitialUtility.ADF_MovieStatus.NotPrepared, appId, "");
				return;
			}
			this.makeInstance_AdfurikunMovieInterstitialController().CallStatic("play", new object[]
			{
				appId
			});
		}
	}

	// Token: 0x06000260 RID: 608 RVA: 0x00013524 File Offset: 0x00011724
	public void MovieInterstitialCallback(string param)
	{
		string[] array = param.Split(new char[]
		{
			';'
		});
		string a = array[0].Split(new char[]
		{
			':'
		})[1];
		string text = array[1].Split(new char[]
		{
			':'
		})[1];
		string text2 = "";
		if (array.Length > 2)
		{
			text2 = array[2].Split(new char[]
			{
				':'
			})[1];
		}
		AdfurikunMovieInterstitialUtility.ADF_MovieStatus status;
		if (!(a == "PrepareSuccess"))
		{
			if (!(a == "StartPlaying"))
			{
				if (!(a == "FinishedPlaying"))
				{
					if (!(a == "FailedPlaying"))
					{
						if (!(a == "AdClose"))
						{
							return;
						}
						status = AdfurikunMovieInterstitialUtility.ADF_MovieStatus.AdClose;
						this.onCloseAd.NullSafe(text, text2);
					}
					else
					{
						status = AdfurikunMovieInterstitialUtility.ADF_MovieStatus.FailedPlaying;
						this.onFailedPlaying.NullSafe(text, text2);
					}
				}
				else
				{
					status = AdfurikunMovieInterstitialUtility.ADF_MovieStatus.FinishedPlaying;
					this.onFinishPlaying.NullSafe(text, text2);
				}
			}
			else
			{
				status = AdfurikunMovieInterstitialUtility.ADF_MovieStatus.StartPlaying;
				this.onStartPlaying.NullSafe(text, text2);
			}
		}
		else
		{
			status = AdfurikunMovieInterstitialUtility.ADF_MovieStatus.PrepareSuccess;
			this.onPrepareSuccess.NullSafe(text);
		}
		this.sendMessage(status, text, text2);
	}

	// Token: 0x06000261 RID: 609 RVA: 0x00013634 File Offset: 0x00011834
	public void sendMessage(AdfurikunMovieInterstitialUtility.ADF_MovieStatus status, string appId, string adnetworkKey)
	{
		if (this.mMovieInterstitialSrcObject != null)
		{
			ArrayList arrayList = new ArrayList();
			arrayList.Add((int)status);
			arrayList.Add(appId);
			arrayList.Add(adnetworkKey);
			this.mMovieInterstitialSrcObject.SendMessage("MovieInterstitialCallback", arrayList);
		}
	}

	// Token: 0x06000262 RID: 610 RVA: 0x00013683 File Offset: 0x00011883
	public void dispose()
	{
		this.disposeResource();
	}

	// Token: 0x06000263 RID: 611 RVA: 0x0001368B File Offset: 0x0001188B
	public void disposeResource()
	{
		if (Application.platform == RuntimePlatform.Android)
		{
			this.callAndroidMovieInterstitialMethod("onDestroy");
		}
	}

	// Token: 0x06000264 RID: 612 RVA: 0x000136A4 File Offset: 0x000118A4
	private string getAppID()
	{
		string result = "";
		if (Application.platform == RuntimePlatform.Android)
		{
			result = this.config.androidAppID;
		}
		return result;
	}

	// Token: 0x06000265 RID: 613 RVA: 0x000136CD File Offset: 0x000118CD
	private bool isValidAppID(string appId)
	{
		return Regex.IsMatch(appId, "^[a-f0-9]{24}$");
	}

	// Token: 0x06000266 RID: 614 RVA: 0x000136DA File Offset: 0x000118DA
	private AndroidJavaClass makeInstance_AdfurikunMovieInterstitialController()
	{
		return new AndroidJavaClass("jp.tjkapp.adfurikunsdk.moviereward.unityplugin.AdfurikunMovieInterController");
	}

	// Token: 0x06000267 RID: 615 RVA: 0x000136E6 File Offset: 0x000118E6
	private void callAndroidMovieInterstitialMethod(string methodName)
	{
		this.makeInstance_AdfurikunMovieInterstitialController().CallStatic(methodName, Array.Empty<object>());
	}

	// Token: 0x06000268 RID: 616 RVA: 0x000136F9 File Offset: 0x000118F9
	public AdfurikunMovieInterstitialUtility()
	{
	}

	// Token: 0x06000269 RID: 617 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
	// Note: this type is marked as 'beforefieldinit'.
	static AdfurikunMovieInterstitialUtility()
	{
	}

	// Token: 0x040000C9 RID: 201
	public Action<string> onNotPrepared;

	// Token: 0x040000CA RID: 202
	public Action<string> onPrepareSuccess;

	// Token: 0x040000CB RID: 203
	public Action<string, string> onStartPlaying;

	// Token: 0x040000CC RID: 204
	public Action<string, string> onFinishPlaying;

	// Token: 0x040000CD RID: 205
	public Action<string, string> onFailedPlaying;

	// Token: 0x040000CE RID: 206
	public Action<string, string> onCloseAd;

	// Token: 0x040000CF RID: 207
	public AdfurikunMovieInterstitialAdConfig config;

	// Token: 0x040000D0 RID: 208
	private static AdfurikunMovieInterstitialUtility mInstance;

	// Token: 0x040000D1 RID: 209
	private GameObject mMovieInterstitialSrcObject;

	// Token: 0x040000D2 RID: 210
	private AdfurikunMovieInterstitialUtility.AdfurikunUnityListener mAdfurikunUnityListener;

	// Token: 0x040000D3 RID: 211
	private string unityPluginVersion = "2.20.0";

	// Token: 0x02000132 RID: 306
	public enum ADF_MovieStatus
	{
		// Token: 0x040004C0 RID: 1216
		NotPrepared,
		// Token: 0x040004C1 RID: 1217
		PrepareSuccess,
		// Token: 0x040004C2 RID: 1218
		StartPlaying,
		// Token: 0x040004C3 RID: 1219
		FinishedPlaying,
		// Token: 0x040004C4 RID: 1220
		FailedPlaying,
		// Token: 0x040004C5 RID: 1221
		AdClose
	}

	// Token: 0x02000133 RID: 307
	public class AdfurikunUnityListener : AndroidJavaProxy
	{
		// Token: 0x060009AC RID: 2476 RVA: 0x0002F393 File Offset: 0x0002D593
		public AdfurikunUnityListener() : base("jp.tjkapp.adfurikunsdk.moviereward.unityplugin.UnityMovieListener")
		{
		}

		// Token: 0x060009AD RID: 2477 RVA: 0x0002F3A0 File Offset: 0x0002D5A0
		public void onPrepareSuccess(string appId)
		{
			AdfurikunMovieInterstitialUtility.mInstance.onPrepareSuccess.NullSafe(appId);
			AdfurikunMovieInterstitialUtility.mInstance.sendMessage(AdfurikunMovieInterstitialUtility.ADF_MovieStatus.PrepareSuccess, appId, "");
		}

		// Token: 0x060009AE RID: 2478 RVA: 0x0002F3C3 File Offset: 0x0002D5C3
		public void onStartPlaying(string appId, string adnetworkKey)
		{
			AdfurikunMovieInterstitialUtility.mInstance.onStartPlaying.NullSafe(appId, adnetworkKey);
			AdfurikunMovieInterstitialUtility.mInstance.sendMessage(AdfurikunMovieInterstitialUtility.ADF_MovieStatus.StartPlaying, appId, adnetworkKey);
		}

		// Token: 0x060009AF RID: 2479 RVA: 0x0002F3E3 File Offset: 0x0002D5E3
		public void onFinishedPlaying(string appId, string adnetworkKey)
		{
			AdfurikunMovieInterstitialUtility.mInstance.onFinishPlaying.NullSafe(appId, adnetworkKey);
			AdfurikunMovieInterstitialUtility.mInstance.sendMessage(AdfurikunMovieInterstitialUtility.ADF_MovieStatus.FinishedPlaying, appId, adnetworkKey);
		}

		// Token: 0x060009B0 RID: 2480 RVA: 0x0002F403 File Offset: 0x0002D603
		public void onFailedPlaying(string appId, string adnetworkKey)
		{
			AdfurikunMovieInterstitialUtility.mInstance.onFailedPlaying.NullSafe(appId, adnetworkKey);
			AdfurikunMovieInterstitialUtility.mInstance.sendMessage(AdfurikunMovieInterstitialUtility.ADF_MovieStatus.FailedPlaying, appId, adnetworkKey);
		}

		// Token: 0x060009B1 RID: 2481 RVA: 0x0002F423 File Offset: 0x0002D623
		public void onAdClose(string appId, string adnetworkKey)
		{
			AdfurikunMovieInterstitialUtility.mInstance.onCloseAd.NullSafe(appId, adnetworkKey);
			AdfurikunMovieInterstitialUtility.mInstance.sendMessage(AdfurikunMovieInterstitialUtility.ADF_MovieStatus.AdClose, appId, adnetworkKey);
		}
	}
}
