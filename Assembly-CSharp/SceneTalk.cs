﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using App;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// Token: 0x0200001F RID: 31
public class SceneTalk : SceneBase
{
	// Token: 0x060000E2 RID: 226 RVA: 0x0000A800 File Offset: 0x00008A00
	protected override void Start()
	{
		base.Start();
		if (PlayerStatus.ScenarioNo == "3章AE-5")
		{
			PlayerStatus.Route = "AE";
		}
		bool flag = Utility.EsExists();
		if (flag)
		{
			base.StartCoroutine(this.SetEsStatus("待機", null));
			this.SetButtonList();
		}
		else
		{
			base.StartCoroutine(this.SetEsStatus("不在", null));
			if (Data.GetScenarioSpecific("").Contains("退行"))
			{
				this.SetButtonList();
			}
		}
		this.EsComment.transform.parent.gameObject.SetActive(false);
		GameObject.Find("MessageScreen").GetComponent<Image>().raycastTarget = false;
		Color color;
		ColorUtility.TryParseHtmlString("#646464E4", out color);
		base.transform.Find("UICanvas/UI_Footer").GetComponent<Image>().color = color;
		base.transform.Find("UICanvas/UI_Footer/ScrollShade1").GetComponent<Image>().color = color;
		base.transform.Find("UICanvas/UI_Footer/ScrollShade2").GetComponent<Image>().color = color;
		if (flag && PlayerStatus.ScenarioNo.Contains("4章ID"))
		{
			base.StartCoroutine(this.SetEsCrazy());
		}
	}

	// Token: 0x060000E3 RID: 227 RVA: 0x0000A930 File Offset: 0x00008B30
	protected override void FixedUpdate()
	{
		base.FixedUpdate();
		if (base.transform.Find("UICanvas/UI1/ButterflyBonus") != null && !base.transform.Find("UICanvas/UI1/ButterflyBonus").gameObject.activeSelf && base.transform.Find("UICanvas/UI2/TalkItemGroup").gameObject.activeSelf && AdManager.IsReady("Reward") && TimeManager.IsOverTime(TimeManager.TYPE.NEXT_BONUS_ES) && !PlayerStatus.EgoPerSecond.IsZero())
		{
			base.transform.Find("UICanvas/UI1/ButterflyBonus").gameObject.SetActive(true);
			Animator butterfly = base.transform.Find("UICanvas/UI1/ButterflyBonus").GetComponent<Animator>();
			butterfly.enabled = false;
			base.StartCoroutine(AppUtil.DelayAction(1f, delegate()
			{
				if (butterfly != null)
				{
					butterfly.enabled = true;
				}
			}, true));
		}
	}

	// Token: 0x060000E4 RID: 228 RVA: 0x0000AA24 File Offset: 0x00008C24
	public void ResetButtonList()
	{
		TalkItem[] componentsInChildren = base.transform.Find("UICanvas/UI2/TalkItemGroup/Viewport/Content").GetComponentsInChildren<TalkItem>();
		for (int i = 0; i < componentsInChildren.Length; i++)
		{
			Object.Destroy(componentsInChildren[i].gameObject);
		}
		this.SetButtonList();
	}

	// Token: 0x060000E5 RID: 229 RVA: 0x0000AA68 File Offset: 0x00008C68
	private void SetButtonList()
	{
		if (PlayerStatus.ScenarioNo == "3章AE-6" && Data.GetRoute(PlayerStatus.ScenarioNo) != PlayerStatus.Route)
		{
			PlayerStatus.ScenarioNo = "3章" + PlayerStatus.Route + "-6";
		}
		string text = PlayerStatus.ScenarioNo.Split(new char[]
		{
			'-'
		})[0];
		if (text.Contains("3章"))
		{
			text = "3章" + PlayerStatus.Route3;
		}
		string start = text + "-1";
		string text2 = "";
		foreach (string text3 in Data.SCENARIO_DATA_KEY)
		{
			if (text3.Contains(text))
			{
				text2 = text3;
			}
			else if (text2 != "")
			{
				break;
			}
		}
		this.SetButtonList(start, text2);
	}

	// Token: 0x060000E6 RID: 230 RVA: 0x0000AB64 File Offset: 0x00008D64
	private void SetButtonList(string start, string end)
	{
		Transform transform = base.transform.Find("UICanvas/UI2/TalkItemGroup/Viewport/Content");
		GameObject gameObject = base.transform.Find("UICanvas/UI2/TalkItemGroup/Viewport/Content/ScenarioItem").gameObject;
		gameObject.SetActive(true);
		int num = 1;
		int num2 = 0;
		string value = "";
		bool flag = false;
		float num3 = 0f;
		for (int i = 1; i <= 2; i++)
		{
			if (i == 2)
			{
				if (!PlayerStatus.ScenarioNo.Contains("4章AE"))
				{
					break;
				}
				start = "1章-1";
				end = "3章AE-6";
				flag = true;
				num3 = 1570f;
				transform.Find("RememberScroll").gameObject.SetActive(true);
			}
			int scenarioIndex = Data.GetScenarioIndex(start);
			int scenarioIndex2 = Data.GetScenarioIndex(end);
			for (int j = scenarioIndex; j <= scenarioIndex2; j++)
			{
				string text = Data.SCENARIO_DATA_KEY[j];
				string[] scenarioItemData = Data.GetScenarioItemData(text);
				if (scenarioItemData == null)
				{
					break;
				}
				if (!(scenarioItemData[0] == ""))
				{
					if (text.Contains(value))
					{
						num2++;
					}
					else
					{
						num2 = 1;
					}
					value = text.Split(new char[]
					{
						'-'
					})[0];
					GameObject gameObject2 = Object.Instantiate<GameObject>(gameObject, transform);
					gameObject2.GetComponent<TalkItem>().SetItem(text, num2, flag);
					gameObject2.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, -((float)(232 * (num - 1) + 10 + 20) + num3), 0f);
					num++;
				}
			}
		}
		if (flag)
		{
			GameObject gameObject3 = Object.Instantiate<GameObject>(gameObject, transform);
			gameObject3.GetComponent<TalkItem>().SetItem("エンドロール", 0, flag);
			gameObject3.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, -((float)(232 * (num - 1) + 10 + 20) + num3), 0f);
			num++;
		}
		gameObject.SetActive(false);
		transform.GetComponent<RectTransform>().sizeDelta = new Vector2(0f, (float)(232 * (num - 1) + 120 + 20) + num3 + 160f);
		if (!flag)
		{
			this.SetFilter(false);
			return;
		}
		base.transform.Find("UICanvas/UI2/TalkItemGroup/Viewport/Content/ScenarioFilter").gameObject.SetActive(false);
	}

	// Token: 0x060000E7 RID: 231 RVA: 0x0000AD8C File Offset: 0x00008F8C
	private void SetFilter(bool move)
	{
		Transform filter = base.transform.Find("UICanvas/UI2/TalkItemGroup/Viewport/Content/ScenarioFilter");
		filter.gameObject.SetActive(true);
		filter.SetAsLastSibling();
		int num = 0;
		TalkItem[] componentsInChildren = base.GetComponentsInChildren<TalkItem>();
		for (int i = 0; i < componentsInChildren.Length; i++)
		{
			if (componentsInChildren[i].IsActive())
			{
				num++;
			}
		}
		float num2 = (float)(-(float)num * 232 - 20);
		if (num == 0)
		{
			num2 = 0f;
		}
		if (move)
		{
			float y = filter.GetComponent<RectTransform>().anchoredPosition.y;
			base.StartCoroutine(AppUtil.MoveEasingFloat(y, num2, delegate(float tmp)
			{
				filter.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, tmp);
			}, false, 3f, EasingFunction.Ease.EaseInOutQuint, null));
			return;
		}
		filter.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, num2);
	}

	// Token: 0x060000E8 RID: 232 RVA: 0x0000AE6C File Offset: 0x0000906C
	public new void OnClick(GameObject clickObject)
	{
		if (base.OnClick(clickObject))
		{
			return;
		}
		global::Debug.Log("SceneTalk:OnClick " + clickObject.name);
		if (this.PreComment != null)
		{
			base.StopCoroutine(this.PreComment);
			this.PreComment = null;
		}
		TalkItem componentInParent = clickObject.GetComponentInParent<TalkItem>();
		if (componentInParent != null)
		{
			this.ClickedTalkItem = componentInParent;
		}
		ChoiceGroup componentInParent2 = clickObject.GetComponentInParent<ChoiceGroup>();
		if (componentInParent2 != null)
		{
			componentInParent2.SelectItem(clickObject);
			this.EsComment.transform.parent.gameObject.SetActive(false);
			this.WaitingChoice = false;
			string text = clickObject.name.Replace("Button", "").Replace("Choice", "");
			if (!(text == "Law"))
			{
				if (!(text == "Chaos"))
				{
					if (!(text == "Neutral"))
					{
						if (text == "NA")
						{
							text = "N/A";
						}
					}
					else
					{
						text = "N";
					}
				}
				else
				{
					text = "C";
				}
			}
			else
			{
				text = "L";
			}
			this.CounselingData = this.CounselingData + text + ",";
			return;
		}
		string name = clickObject.name;
		uint num = <PrivateImplementationDetails>.ComputeStringHash(name);
		if (num <= 2172580610U)
		{
			if (num <= 543985802U)
			{
				if (num <= 365988264U)
				{
					if (num != 278287555U)
					{
						if (num != 365988264U)
						{
							goto IL_8E2;
						}
						if (!(name == "本を返すButton"))
						{
							goto IL_8E2;
						}
						this.ScenarioNo = "本を返す";
						this.ScenarioType = "本を返す共通";
						base.StartCoroutine(this.StartCounseling());
						goto IL_8E2;
					}
					else
					{
						if (!(name == "InputButton"))
						{
							goto IL_8E2;
						}
						InputField component = base.transform.Find("UICanvas/UI2/InputGroup/ThinkingArea").GetComponent<InputField>();
						this.CounselingData = component.text;
						PlayerResult.CatharsisLength = this.CounselingData.Length;
						this.WaitingChoice = false;
						return;
					}
				}
				else if (num != 456477288U)
				{
					if (num != 507760969U)
					{
						if (num != 543985802U)
						{
							goto IL_8E2;
						}
						if (!(name == "SkipButton"))
						{
							goto IL_8E2;
						}
						this.WaitingChoice = false;
						return;
					}
					else
					{
						if (!(name == "StartChapterEndButton"))
						{
							goto IL_8E2;
						}
						base.StartCoroutine(this.StartCounseling());
						return;
					}
				}
				else
				{
					if (!(name == "RestartButton"))
					{
						goto IL_8E2;
					}
					DialogManager.ShowDialog("RestartDialog" + PlayerStatus.Route, Array.Empty<object>());
					goto IL_8E2;
				}
			}
			else if (num <= 1843024091U)
			{
				if (num != 1058369205U)
				{
					if (num != 1575583836U)
					{
						if (num != 1843024091U)
						{
							goto IL_8E2;
						}
						if (!(name == "自我とエス(ID)Button"))
						{
							goto IL_8E2;
						}
					}
					else
					{
						if (!(name == "CounselingButton"))
						{
							goto IL_8E2;
						}
						this.WaitingChoice = true;
						return;
					}
				}
				else if (!(name == "雑談Button"))
				{
					goto IL_8E2;
				}
			}
			else if (num != 1921566945U)
			{
				if (num != 2072337994U)
				{
					if (num != 2172580610U)
					{
						goto IL_8E2;
					}
					if (!(name == "TalkArea"))
					{
						goto IL_8E2;
					}
					this.ScenarioNo = null;
					this.ScenarioType = null;
					this.TalkCount++;
					if (!PlayerStatus.ScenarioNo.Contains("4章ID"))
					{
						PlayerResult.EsTapCount++;
					}
					int scenarioIndex = Data.GetScenarioIndex(PlayerStatus.ScenarioNo);
					int scenarioIndex2 = Data.GetScenarioIndex("1章-2");
					string key = "[EsTalk]1章-通常-9";
					string text2 = "通常";
					if (scenarioIndex >= scenarioIndex2)
					{
						if (this.TalkCount > 10 && !PlayerStatus.ScenarioNo.Contains("4章ID"))
						{
							text2 = "怒り";
						}
						int num2 = (text2 == "怒り") ? 10 : 20;
						string text3 = PlayerStatus.ScenarioNo.Split(new char[]
						{
							'-'
						})[0];
						key = string.Concat(new object[]
						{
							"[EsTalk]",
							text3,
							"-",
							text2,
							"-",
							Random.Range(1, num2 + 1)
						});
					}
					if (this.PreComment != null)
					{
						base.StopCoroutine(this.PreComment);
					}
					this.PreComment = this.SetComment(text2, key);
					base.StartCoroutine(this.PreComment);
					return;
				}
				else
				{
					if (!(name == "RestartButtonAE"))
					{
						goto IL_8E2;
					}
					this.ScenarioNo = clickObject.GetComponentInParent<TalkItem>().ScenarioNo;
					this.ScenarioType = Data.GetScenarioType(this.ScenarioNo);
					base.StartCoroutine(this.StartCounseling());
					goto IL_8E2;
				}
			}
			else
			{
				if (!(name == "RetryButtonFromDialog"))
				{
					goto IL_8E2;
				}
				AdManager.Show("Reward", delegate(bool ret)
				{
					this.RetryWithReward(ret);
				});
				return;
			}
		}
		else if (num <= 2567189190U)
		{
			if (num <= 2250790435U)
			{
				if (num != 2208031522U)
				{
					if (num != 2250790435U)
					{
						goto IL_8E2;
					}
					if (!(name == "本を借りるButton"))
					{
						goto IL_8E2;
					}
				}
				else
				{
					if (!(name == "ApproveButton"))
					{
						goto IL_8E2;
					}
					base.StartCoroutine(this.Closing());
					return;
				}
			}
			else if (num != 2290307011U)
			{
				if (num != 2401406720U)
				{
					if (num != 2567189190U)
					{
						goto IL_8E2;
					}
					if (!(name == "IchEsButtonUnlocked"))
					{
						goto IL_8E2;
					}
					DialogManager.ShowDialog("ShopDialog", new object[]
					{
						2
					});
					goto IL_8E2;
				}
				else if (!(name == "日替わりButton"))
				{
					goto IL_8E2;
				}
			}
			else
			{
				if (!(name == "RetryNoAdButton"))
				{
					goto IL_8E2;
				}
				this.ScenarioNo = clickObject.GetComponentInParent<TalkItem>().ScenarioNo;
				this.ScenarioType = Data.GetScenarioType(this.ScenarioNo);
				if (this.ScenarioNo == "エンドロール")
				{
					SceneTransition.Transit(delegate
					{
						SceneManager.LoadScene("Ending", LoadSceneMode.Additive);
					}, null);
					goto IL_8E2;
				}
				base.StartCoroutine(this.StartCounseling());
				goto IL_8E2;
			}
		}
		else if (num <= 3250750992U)
		{
			if (num != 2689065599U)
			{
				if (num != 2804332844U)
				{
					if (num != 3250750992U)
					{
						goto IL_8E2;
					}
					if (!(name == "MessageScreen"))
					{
						goto IL_8E2;
					}
					GameObject.Find("MessageScreen").GetComponent<Image>().raycastTarget = false;
					return;
				}
				else
				{
					if (!(name == "RetryButtonFromResult"))
					{
						goto IL_8E2;
					}
					AdManager.Show("Reward", delegate(bool ret)
					{
						this.RetryWithReward(ret);
					});
					return;
				}
			}
			else
			{
				if (!(name == "ScenarioButton"))
				{
					goto IL_8E2;
				}
				this.ScenarioNo = clickObject.GetComponentInParent<TalkItem>().ScenarioNo;
				this.ScenarioType = Data.GetScenarioType(this.ScenarioNo);
				if (Data.GetScenarioSpecific(this.ScenarioNo).Contains("章END"))
				{
					DialogManager.ShowDialog("ChapterEndDialog", Array.Empty<object>());
					return;
				}
				base.StartCoroutine(this.StartCounseling());
				return;
			}
		}
		else if (num != 3697598008U)
		{
			if (num != 3738829183U)
			{
				if (num != 3936806110U)
				{
					goto IL_8E2;
				}
				if (!(name == "自我とエス(AE)Button"))
				{
					goto IL_8E2;
				}
			}
			else
			{
				if (!(name == "ScenarioRetryButton"))
				{
					goto IL_8E2;
				}
				this.ScenarioNo = clickObject.GetComponentInParent<TalkItem>().ScenarioNo;
				this.ScenarioType = Data.GetScenarioType(this.ScenarioNo);
				DialogManager.ShowDialog("RetryDialog", Array.Empty<object>());
				return;
			}
		}
		else if (!(name == "自我とエス(SE)Button"))
		{
			goto IL_8E2;
		}
		string text4 = clickObject.name.Replace("Button", "");
		int count = int.Parse(Data.DIC[text4 + "COUNT"][0]);
		int[] array = AppUtil.RandomArray(1, count);
		int num3 = array[0];
		if (text4 == "本を借りる")
		{
			if (PlayerStatus.ReadingBookList.Length == 0)
			{
				num3 = 1;
			}
		}
		else if (num3 == PlayerPrefs.GetInt("Last" + text4 + "Index"))
		{
			num3 = array[1];
		}
		PlayerPrefs.SetInt("Last" + text4 + "Index", num3);
		this.ScenarioNo = text4;
		this.ScenarioType = text4 + num3;
		base.StartCoroutine(this.StartCounseling());
		if (text4 == "日替わり")
		{
			clickObject.transform.parent.GetComponent<TalkItem>().SetButtonActive(false);
		}
		IL_8E2:
		string[] array2 = Data.GET_COUNSELING_TYPE(this.ScenarioType);
		if (array2 != null && Array.IndexOf<string>(array2, clickObject.name) >= 0)
		{
			this.CounselingData = this.CounselingData + clickObject.name + ",";
			this.WordListForDisplay.Add(clickObject.GetComponentInChildren<Text>().text);
			this.WaitingChoice = false;
			foreach (Button button in base.transform.Find("UICanvas/WordGroup/WordItemGroup").GetComponentsInChildren<Button>())
			{
				button.enabled = false;
				if (button.gameObject == clickObject)
				{
					button.transform.Rotate(new Vector3(0f, 0f, -6f));
				}
				else
				{
					button.GetComponent<Image>().sprite = this.PaperSprite[this.PaperSprite.Length - 1];
					button.GetComponent<Image>().SetNativeSize();
					button.GetComponentInChildren<Text>().enabled = false;
					button.transform.Rotate(new Vector3(0f, 0f, 360f * Random.Range(0f, 1f)));
				}
			}
			return;
		}
	}

	// Token: 0x060000E9 RID: 233 RVA: 0x0000B88F File Offset: 0x00009A8F
	private void RetryWithReward(bool reward)
	{
		if (reward)
		{
			base.StartCoroutine(this.StartCounseling());
			PlayerResult.AdMovieCount++;
		}
	}

	// Token: 0x060000EA RID: 234 RVA: 0x0000B8AD File Offset: 0x00009AAD
	private IEnumerator SetComment(string state, string key)
	{
		GameObject commentFrame = this.EsComment.transform.parent.gameObject;
		if (commentFrame.activeSelf)
		{
			commentFrame.SetActive(false);
			yield return AppUtil.Wait(0.2f);
		}
		if (state == "怒り")
		{
			yield return this.SetEsStatus("対話", null);
			base.transform.Find("UICanvas/UI1/TalkArea").gameObject.SetActive(true);
		}
		this.EsComment.SetKey(key);
		commentFrame.SetActive(true);
		yield return AppUtil.Wait(2f);
		commentFrame.SetActive(false);
		if (state == "怒り")
		{
			yield return this.SetEsStatus("待機", null);
		}
		this.PreComment = null;
		yield break;
	}

	// Token: 0x060000EB RID: 235 RVA: 0x0000B8CA File Offset: 0x00009ACA
	private IEnumerator SetEsCrazy()
	{
		base.transform.Find("BGCanvas/Es正面/EsMain").GetComponent<Image>().sprite = this.EsCrazy;
		base.transform.Find("BGCanvas/Es正面/Background").GetComponent<Image>().sprite = this.EsCrazyBG;
		this.SetEsFace("Main");
		GameObject talkArea = base.transform.Find("UICanvas/UI1/TalkArea").gameObject;
		if (!PlayerStatus.ScenarioNo.Contains("4章ID"))
		{
			yield break;
		}
		for (;;)
		{
			yield return AppUtil.Wait(Random.Range(0.5f, 2.5f));
			this.OnClick(talkArea);
		}
	}

	// Token: 0x060000EC RID: 236 RVA: 0x0000B8DC File Offset: 0x00009ADC
	private void SetEsFace(string target)
	{
		target = target.Replace("<", "").Replace(">", "");
		if (target == "ほほえみ")
		{
			target = "Smile";
		}
		Animator component = base.transform.Find("BGCanvas/Es正面").GetComponent<Animator>();
		component.ResetTrigger("Wait");
		component.SetTrigger(target);
	}

	// Token: 0x060000ED RID: 237 RVA: 0x0000B944 File Offset: 0x00009B44
	private IEnumerator SetEsStatus(string state, Action changeScene = null)
	{
		global::Debug.Log("SetEsStatus " + state);
		base.transform.Find("BGCanvas/Es正面/EsMain").GetComponent<Image>().sprite = this.EsStandard;
		base.transform.Find("BGCanvas/Es正面/Background").GetComponent<Image>().sprite = this.EsStandardBG;
		base.transform.Find("UICanvas/UI1/TalkArea").gameObject.SetActive(false);
		string direction = "正面";
		if (state.Contains("対話") || state.Contains("診断1") || state.Contains("雑談") || state.Contains("日替わり") || state.Contains("本を") || state.Contains("自我とエス"))
		{
			state = "対話";
		}
		if (state.Contains("診断2"))
		{
			direction = "横向き";
			state = "横向き";
			base.transform.Find("BGCanvas/Es横向き/Background/MoveEye").gameObject.SetActive(false);
		}
		else if (state.Contains("診断3") || state.Contains("診断4"))
		{
			direction = "アオリ";
			state = "アオリ";
		}
		if (!base.transform.Find("BGCanvas/Es" + direction).gameObject.activeSelf)
		{
			SceneTransition.Transit(delegate
			{
				foreach (string text in new string[]
				{
					"正面",
					"横向き",
					"アオリ"
				})
				{
					this.transform.Find("BGCanvas/Es" + text).gameObject.SetActive(text == direction);
				}
				if (changeScene != null)
				{
					changeScene();
				}
			}, null);
			yield return AppUtil.WaitRealtime(0.75f);
		}
		else if (changeScene != null)
		{
			changeScene();
		}
		this.EsComment = base.transform.Find("BGCanvas/Es" + direction).GetComponentInChildren<TextLocalization>(true);
		this.EsComment.transform.parent.gameObject.SetActive(false);
		string a = state;
		if (!(a == "不在"))
		{
			if (!(a == "退室"))
			{
				if (!(a == "待機"))
				{
					if (a == "対話")
					{
						base.transform.Find("BGCanvas/Es正面/EsMain").gameObject.SetActive(true);
						string esFace = "Main";
						if (PlayerStatus.ScenarioNo.Contains("4章AE"))
						{
							if (Data.GetScenarioSpecific(this.ScenarioNo).Contains("退行"))
							{
								esFace = "Smile";
							}
							else
							{
								string scenarioKey = Data.GetScenarioKey(this.ScenarioType, "立ち絵", null, 0);
								if (scenarioKey != null)
								{
									esFace = LanguageManager.Get(scenarioKey);
								}
								else
								{
									a = this.ScenarioNo;
									if (!(a == "雑談") && !(a == "本を借りる") && !(a == "本を返す"))
									{
										if (!(a == "自我とエス(ID)"))
										{
											if (!(a == "自我とエス(SE)"))
											{
												if (a == "自我とエス(AE)")
												{
													esFace = "ほほえみ";
												}
											}
											else
											{
												esFace = "SE";
											}
										}
										else
										{
											esFace = "ID";
										}
									}
									else
									{
										esFace = "Smile";
									}
								}
							}
						}
						this.SetEsFace(esFace);
					}
				}
				else
				{
					base.transform.Find("BGCanvas/Es正面/EsMain").gameObject.SetActive(true);
					base.transform.Find("UICanvas/UI1/TalkArea").gameObject.SetActive(true);
					this.SetEsFace("Wait");
				}
			}
			else
			{
				base.StartCoroutine(AppUtil.FadeOut(base.transform.Find("BGCanvas/Es正面/EsMain"), 0.5f, null));
			}
		}
		else
		{
			base.transform.Find("BGCanvas/Es正面/EsMain").gameObject.SetActive(false);
		}
		yield break;
	}

	// Token: 0x060000EE RID: 238 RVA: 0x0000B961 File Offset: 0x00009B61
	private IEnumerator Closing()
	{
		this.ResultCanvas.SetActive(false);
		if (Data.GetScenarioKey(this.ScenarioType, "結び", null, 1) != null)
		{
			yield return this.EsTalk("結び", true);
		}
		string scenarioNo = this.ScenarioNo;
		EgoPoint bonusEgo;
		if (!(scenarioNo == "日替わり"))
		{
			if (scenarioNo == "本を借りる")
			{
				int intParameter = Utility.GetIntParameter("読書時間");
				TimeManager.Reset(TimeManager.TYPE.END_BOOK, TimeSpan.FromHours((double)intParameter) + TimeSpan.FromSeconds(0.8999999761581421));
				if (this.ClickedTalkItem.Price != null)
				{
					PlayerStatus.EgoPoint -= this.ClickedTalkItem.Price;
				}
				this.ClickedTalkItem.SetItem();
			}
		}
		else
		{
			int intParameter2 = Utility.GetIntParameter(this.ScenarioNo + this.CounselingData.Replace(",", ""));
			bonusEgo = PlayerStatus.EgoPerSecond * (float)intParameter2 * 60f * 60f * Settings.GAME_SPEED;
			this.EsComment.transform.parent.gameObject.SetActive(false);
			yield return AppUtil.Wait(0.2f);
			if (AdInfo.ENABLE)
			{
				DialogManager.ShowDialog("DailyBonusDialog", new object[]
				{
					bonusEgo
				});
			}
			else
			{
				DialogManager.ShowDialog("GetDailyBonusDialog", new object[]
				{
					bonusEgo
				});
			}
			yield return AppUtil.Wait(0.2f);
		}
		bonusEgo = null;
		string state = "待機";
		bool levelUp = this.ScenarioNo == PlayerStatus.ScenarioNo || this.ScenarioNo == "本を返す";
		if (levelUp)
		{
			if (Data.GetScenarioSpecific("").Contains("エンディング"))
			{
				PlayerResult.Ending += PlayerStatus.Route;
				AdManager.Hide("Banner");
				if (PlayerStatus.Route == "AE")
				{
					SceneTransition.LoadScene("Ending", new Color?(Color.black), 3f);
					PlayerStatus.ReadingBook = 0;
					PlayerStatus.ReadingBookList = "";
					string scenarioID = "ゲームクリア";
					string value;
					if (PlayerResult.EsTapCount >= 500)
					{
						value = "エス崇拝者";
					}
					else if (PlayerResult.TapCount >= 10000)
					{
						value = "自問の達人";
					}
					else if (PlayerResult.Ending.StartsWith("SE"))
					{
						value = "管理者";
					}
					else
					{
						value = "解放者";
					}
					CounselingResult.Set(scenarioID, value);
				}
				else
				{
					DialogManager.ShowDialog("EndingScreen" + PlayerStatus.Route, Array.Empty<object>());
				}
				PlayerStatus.EgoPoint -= this.ClickedTalkItem.Price;
				Data.GoToNextScenario();
				yield break;
			}
			if (Data.GetNextScenarioSpecific().Contains("エス不在"))
			{
				state = "退室";
			}
		}
		base.StartCoroutine(this.SetEsStatus(state, delegate
		{
			this.EsComment.transform.parent.gameObject.SetActive(false);
			this.transform.Find("UICanvas/UI2/TalkItemGroup").gameObject.SetActive(true);
			this.transform.Find("UICanvas/UI_Footer").gameObject.SetActive(true);
			this.transform.Find("UICanvas/UI2/ChoiceGroup").gameObject.SetActive(false);
			this.transform.Find("UICanvas/UI2/EhonGroup").gameObject.SetActive(false);
			this.transform.Find("BGCanvas/Es横向き/Background/MoveEye").gameObject.SetActive(false);
			if (levelUp)
			{
				this.ScenarioLevelUp();
				return;
			}
			if (this.ScenarioNo == "3章AE-5")
			{
				this.ResetButtonList();
			}
		}));
		yield break;
	}

	// Token: 0x060000EF RID: 239 RVA: 0x0000B970 File Offset: 0x00009B70
	private void ScenarioLevelUp()
	{
		TalkItem clickedTalkItem = this.ClickedTalkItem;
		string type = clickedTalkItem.GetType();
		string format = LanguageManager.Get("[UI]RankUpEffect/EgoText");
		string before;
		if (type == "タップ")
		{
			before = Data.TapPower(false).ToString();
		}
		else
		{
			before = string.Format(format, PlayerStatus.EgoPerSecond);
		}
		string scenarioSpecific = Data.GetScenarioSpecific(this.ScenarioNo);
		if (this.ScenarioNo == "本を返す")
		{
			bool readingBookIsOver = Data.ReadingBookIsOver;
			PlayerStatus.ReadingBookList += PlayerStatus.ReadingBook.ToString("X");
			PlayerStatus.ReadingBook = 0;
			if (readingBookIsOver)
			{
				this.ClickedTalkItem.SetItem();
				return;
			}
		}
		else
		{
			PlayerStatus.EgoPoint -= this.ClickedTalkItem.Price;
			if (scenarioSpecific.Contains("ルート分岐"))
			{
				if (this.ScenarioNo.Contains("2章"))
				{
					PlayerStatus.Route = PlayerStatus.RouteTrend;
					PlayerStatus.ScenarioNo = "3章" + PlayerStatus.Route + "-1";
					PlayerStatus.Route3 = PlayerStatus.Route;
				}
				else
				{
					PlayerStatus.ScenarioNo = "3章" + PlayerStatus.Route + "-6";
				}
			}
			else
			{
				Data.GoToNextScenario();
			}
		}
		string after;
		if (type == "タップ")
		{
			after = Data.TapPower(false).ToString();
		}
		else
		{
			after = string.Format(format, PlayerStatus.EgoPerSecond);
		}
		clickedTalkItem.OnLevelUp(before, after);
		float waitTime = 0f;
		if (Data.GetScenarioSpecific("").Contains("エス不在"))
		{
			waitTime = 0.5f;
		}
		TalkItem[] componentsInChildren = base.GetComponentsInChildren<TalkItem>();
		for (int i = 0; i < componentsInChildren.Length; i++)
		{
			componentsInChildren[i].SetItem();
		}
		base.StartCoroutine(AppUtil.DelayAction(waitTime, delegate()
		{
			this.SetFilter(this.ScenarioNo != "本を返す");
		}, true));
		if (scenarioSpecific.Contains("エンディング"))
		{
			base.transform.Find("UICanvas/UI2/TalkItemGroup").gameObject.SetActive(false);
			base.transform.Find("UICanvas/UI_Footer").gameObject.SetActive(false);
		}
	}

	// Token: 0x060000F0 RID: 240 RVA: 0x0000BB7D File Offset: 0x00009D7D
	private IEnumerator EsTalk(string keyHeader, bool wait = true)
	{
		GameObject commentFrame = this.EsComment.transform.parent.gameObject;
		Image touch = GameObject.Find("MessageScreen").GetComponent<Image>();
		if (wait)
		{
			commentFrame.SetActive(false);
			yield return AppUtil.Wait(1f);
		}
		int num;
		for (int i = 1; i <= 100; i = num + 1)
		{
			string key = Data.GetScenarioKey(this.ScenarioType, keyHeader, null, i);
			if (key == null)
			{
				break;
			}
			commentFrame.SetActive(false);
			yield return AppUtil.Wait(0.2f);
			this.EsComment.SetKey(key);
			commentFrame.SetActive(true);
			touch.raycastTarget = true;
			while (touch.raycastTarget)
			{
				yield return null;
			}
			key = null;
			num = i;
		}
		yield break;
	}

	// Token: 0x060000F1 RID: 241 RVA: 0x0000BB9A File Offset: 0x00009D9A
	private IEnumerator StartCounseling()
	{
		HyperActive.Init();
		this.CounselingData = "";
		base.transform.Find("UICanvas/UI2/InputGroup/ThinkingArea").GetComponent<InputField>().text = "";
		base.transform.Find("UICanvas/UI2/TalkItemGroup").gameObject.SetActive(false);
		base.transform.Find("UICanvas/UI_Footer").gameObject.SetActive(false);
		if (base.transform.Find("UICanvas/UI1/ButterflyBonus") != null)
		{
			base.transform.Find("UICanvas/UI1/ButterflyBonus").gameObject.SetActive(false);
		}
		yield return this.SetEsStatus(this.ScenarioType, null);
		yield return this.EsTalk("導入", true);
		string scenarioSpecific = Data.GetScenarioSpecific(this.ScenarioNo);
		if (this.ScenarioType.Contains("診断"))
		{
			this.WaitingChoice = true;
			if (PlayerResult.EndingCount > 0 && !scenarioSpecific.Contains("ルート分岐"))
			{
				DialogManager.ShowDialog("SkipCounselingDialog", new object[]
				{
					this.ScenarioType
				});
				while (DialogManager.IsShowing())
				{
					yield return null;
				}
			}
			if (this.WaitingChoice)
			{
				yield return base.StartCoroutine(this.StartCounselingType());
				yield break;
			}
		}
		else
		{
			yield return this.StartDialogue();
			if (this.ScenarioNo == "退行")
			{
				Data.Restart();
				yield break;
			}
			if (this.ScenarioType.Contains("雑談"))
			{
				PlayerResult.EsTalkCount++;
				if (PlayerResult.EsTalkCount >= 100)
				{
					Utility.PromptReview("雑談100回");
				}
			}
		}
		yield return this.Closing();
		yield break;
	}

	// Token: 0x060000F2 RID: 242 RVA: 0x0000BBA9 File Offset: 0x00009DA9
	private IEnumerator StartCounselingType()
	{
		this.WordListForDisplay = new List<string>();
		TextLocalization preEsComment = this.EsComment;
		string closingKey = "結果導入";
		if (this.ScenarioType.Contains("診断1"))
		{
			yield return this.StartCounselingType1();
		}
		else if (this.ScenarioType.Contains("診断2"))
		{
			yield return this.StartCounselingType2();
		}
		else if (this.ScenarioType.Contains("診断3"))
		{
			yield return this.StartCounselingType3();
		}
		else if (this.ScenarioType.Contains("診断4"))
		{
			yield return this.StartCounselingType4();
			if (this.CounselingData.Length <= 10)
			{
				closingKey += "10";
			}
			else if (this.CounselingData.Length >= 100)
			{
				closingKey += "100";
			}
		}
		yield return this.EsTalk(closingKey, true);
		this.EsComment = preEsComment;
		this.EsComment.transform.parent.gameObject.SetActive(false);
		string scenarioType = this.ScenarioType;
		if (scenarioType == "診断2-3" || scenarioType == "診断2-4" || scenarioType == "診断2-5")
		{
			yield return AppUtil.FadeOut(base.transform.Find("UICanvas/UI2/EhonGroup"), 1f, null);
			base.StartCoroutine(this.Closing());
		}
		else
		{
			string[] counselingType = Counseling.GetCounselingType(this.ScenarioType, this.CounselingData);
			string text = counselingType[0];
			CounselingResult.Set(this.ScenarioType, text);
			if (this.ScenarioType.Contains("診断4"))
			{
				this.WordListForDisplay.AddRange(AppUtil.GetChildArray(counselingType, 1, true, 0));
				string text2 = text;
				if (text.Contains("その他"))
				{
					text2 = "ノンジャンル";
				}
				foreach (string type in text2.Split(new char[]
				{
					'+'
				}))
				{
					string scenarioKey = Data.GetScenarioKey(this.ScenarioType, "解析中表示単語", type, 0);
					this.WordListForDisplay.AddRange(LanguageManager.Get(scenarioKey).Split(new char[]
					{
						'、'
					}));
				}
			}
			this.ResultCanvas.RequestResult(this.ScenarioType, text, this.WordListForDisplay);
			AppUtil.SetFalse("UICanvas/UI2/EhonGroup");
			AppUtil.SetFalse("BGCanvas/Es横向き/Background/MoveEye");
		}
		yield break;
	}

	// Token: 0x060000F3 RID: 243 RVA: 0x0000BBB8 File Offset: 0x00009DB8
	private IEnumerator StartCounselingType1()
	{
		int max;
		int j;
		for (max = 1; max <= 20; max = j + 1)
		{
			if (Data.GetScenarioKey(this.ScenarioType, "問", null, max) == null)
			{
				j = max;
				max = j - 1;
				break;
			}
			j = max;
		}
		ChoiceGroup choice = base.GetComponentInChildren<ChoiceGroup>(true);
		choice.gameObject.SetActive(true);
		this.SetProgressState(base.transform.Find("UICanvas/UI2/ChoiceGroup"), 0f, (float)max);
		GameObject commentFrame = this.EsComment.transform.parent.gameObject;
		string[] buttonNameList = new string[]
		{
			"YesButton",
			"NoButton",
			"NAButton"
		};
		for (int i = 1; i <= max; i = j + 1)
		{
			yield return choice.SetButton(null, null, false);
			string key = Data.GetScenarioKey(this.ScenarioType, "問", null, i);
			commentFrame.SetActive(false);
			yield return AppUtil.Wait(0.2f);
			this.EsComment.SetKey(key);
			commentFrame.SetActive(true);
			yield return choice.SetButton(buttonNameList, new string[]
			{
				"[UI]YesButton/Text",
				"[UI]NoButton/Text",
				"[UI]NAButton/Text"
			}, false);
			this.WaitingChoice = true;
			while (this.WaitingChoice)
			{
				yield return null;
			}
			string str = "";
			foreach (string text in buttonNameList)
			{
				if (base.transform.Find("UICanvas/UI2/ChoiceGroup/" + text).gameObject.activeSelf)
				{
					str = text.Replace("Button", "");
				}
			}
			string id = key.Replace("問", "解析中表示単語") + "-" + str;
			this.WordListForDisplay.AddRange(LanguageManager.Get(id).Split(new char[]
			{
				'、'
			}));
			this.SetProgressState(base.transform.Find("UICanvas/UI2/ChoiceGroup"), (float)i, (float)max);
			yield return AppUtil.Wait(0.5f);
			key = null;
			j = i;
		}
		choice.gameObject.SetActive(false);
		yield break;
	}

	// Token: 0x060000F4 RID: 244 RVA: 0x0000BBC7 File Offset: 0x00009DC7
	private IEnumerator StartCounselingType2()
	{
		this.EsComment.transform.parent.gameObject.SetActive(false);
		base.transform.Find("UICanvas/UI2/EhonGroup").gameObject.SetActive(true);
		base.transform.Find("UICanvas/UI2/EhonGroup/" + this.ScenarioType + "/Q0").gameObject.SetActive(true);
		base.transform.Find("UICanvas/UI2/EhonGroup/診断2-1").gameObject.SetActive(false);
		base.transform.Find("UICanvas/UI2/EhonGroup/診断2-2").gameObject.SetActive(false);
		base.transform.Find("UICanvas/UI2/EhonGroup/診断2-3").gameObject.SetActive(false);
		base.transform.Find("UICanvas/UI2/EhonGroup/診断2-4").gameObject.SetActive(false);
		base.transform.Find("UICanvas/UI2/EhonGroup/診断2-5").gameObject.SetActive(false);
		base.transform.Find("UICanvas/UI2/EhonGroup/" + this.ScenarioType + "/Ending1").gameObject.SetActive(false);
		base.transform.Find("UICanvas/UI2/EhonGroup/" + this.ScenarioType + "/Ending2").gameObject.SetActive(false);
		base.transform.Find("UICanvas/UI2/EhonGroup/" + this.ScenarioType + "/Ending3").gameObject.SetActive(false);
		base.transform.Find("UICanvas/UI2/EhonGroup/" + this.ScenarioType).gameObject.SetActive(true);
		base.transform.Find("UICanvas/UI2/EhonGroup/EsTalk").gameObject.SetActive(false);
		base.transform.Find("UICanvas/UI2/EhonGroup/" + this.ScenarioType + "/Background").gameObject.SetActive(true);
		base.transform.Find("UICanvas/UI2/EhonGroup/Background").GetComponent<Image>().color = Color.white;
		base.StartCoroutine(AppUtil.FadeIn(base.transform.Find("UICanvas/UI2/EhonGroup"), 0.5f, null));
		this.EsComment = base.transform.Find("UICanvas/UI2/EhonGroup/EsTalk").GetComponentInChildren<TextLocalization>(true);
		yield return this.EsTalk("診断導入", true);
		if (Data.GetScenarioSpecific(this.ScenarioNo).Contains("ルート分岐"))
		{
			SceneTalk.<>c__DisplayClass34_0 CS$<>8__locals1 = new SceneTalk.<>c__DisplayClass34_0();
			CS$<>8__locals1.Q01 = base.transform.Find("UICanvas/UI2/EhonGroup/" + this.ScenarioType + "/Q0").gameObject;
			base.StartCoroutine(AppUtil.FadeOut(CS$<>8__locals1.Q01, 0.5f, delegate(Object target)
			{
				CS$<>8__locals1.Q01.SetActive(false);
			}));
			GameObject gameObject = base.transform.Find("UICanvas/UI2/EhonGroup/" + this.ScenarioType + "/Q1").gameObject;
			gameObject.SetActive(true);
			base.StartCoroutine(AppUtil.FadeIn(gameObject, 0.5f, null));
			yield return this.EsTalk("診断導入2", false);
			AppUtil.SetAlpha(CS$<>8__locals1.Q01, 1f);
			CS$<>8__locals1 = null;
		}
		int max;
		int num;
		for (max = 1; max <= 20; max = num + 1)
		{
			if (Data.GetScenarioKey(this.ScenarioType, max + "問", null, 0) == null)
			{
				num = max;
				max = num - 1;
				break;
			}
			num = max;
		}
		ChoiceGroup choice = base.GetComponentInChildren<ChoiceGroup>(true);
		choice.gameObject.SetActive(true);
		for (int i = 1; i <= max; i = num + 1)
		{
			yield return this.StartDialogueQuestion(i, max);
			num = i;
		}
		choice.gameObject.SetActive(false);
		yield break;
	}

	// Token: 0x060000F5 RID: 245 RVA: 0x0000BBD6 File Offset: 0x00009DD6
	private IEnumerator StartCounselingType4()
	{
		for (;;)
		{
			base.transform.Find("UICanvas/UI2/InputGroup").gameObject.SetActive(true);
			this.WaitingChoice = true;
			while (this.WaitingChoice)
			{
				yield return null;
			}
			base.transform.Find("UICanvas/UI2/InputGroup").gameObject.SetActive(false);
			base.transform.Find("BGCanvas/Es横向き/Background/MoveEye").gameObject.SetActive(true);
			if (this.CounselingData.Length > 10)
			{
				break;
			}
			string data = this.CounselingData;
			yield return this.EsTalk("10文字以内確認", true);
			ChoiceGroup choice = base.GetComponentInChildren<ChoiceGroup>(true);
			choice.gameObject.SetActive(true);
			yield return choice.SetButton(new string[]
			{
				"YesButton",
				"NoButton"
			}, new string[]
			{
				"[UI]YesButton/Text",
				"[UI]NoButton/Text"
			}, false);
			this.WaitingChoice = true;
			while (this.WaitingChoice)
			{
				yield return null;
			}
			yield return AppUtil.Wait(0.5f);
			choice.gameObject.SetActive(false);
			this.CounselingData = data;
			if (base.transform.Find("UICanvas/UI2/ChoiceGroup/YesButton").gameObject.activeSelf)
			{
				break;
			}
			yield return this.EsTalk("やり直し", true);
			base.transform.Find("BGCanvas/Es横向き/Background/MoveEye").gameObject.SetActive(false);
			data = null;
			choice = null;
		}
		yield break;
	}

	// Token: 0x060000F6 RID: 246 RVA: 0x0000BBE8 File Offset: 0x00009DE8
	public void OnInputChanged(string value)
	{
		bool flag = value.Length > 0;
		base.transform.Find("UICanvas/UI2/InputGroup/InputButton").GetComponent<Button>().interactable = flag;
		base.transform.Find("UICanvas/UI2/InputGroup/InputButton/InactiveFilter").gameObject.SetActive(!flag);
	}

	// Token: 0x060000F7 RID: 247 RVA: 0x0000BC38 File Offset: 0x00009E38
	private IEnumerator StartCounselingType3()
	{
		int fukidasiMax = 20;
		GameObject group = base.transform.Find("UICanvas/WordGroup").gameObject;
		TextLocalization[] fukidasiList = group.transform.Find("WordItemGroup").GetComponentsInChildren<TextLocalization>();
		TextLocalization[] array = fukidasiList;
		int j;
		for (j = 0; j < array.Length; j++)
		{
			array[j].transform.parent.gameObject.SetActive(false);
		}
		group.transform.Find("EsMessageFrame/EsMessage").GetComponent<TextLocalization>().SetText(LanguageManager.Get(Data.GetScenarioKey(this.ScenarioType, "診断中説明", null, 0)));
		this.SetProgressState(base.transform.Find("UICanvas/WordGroup"), 0f, (float)fukidasiMax);
		SceneTransition.Transit(delegate
		{
			group.SetActive(true);
		}, null);
		while (!group.activeSelf)
		{
			yield return null;
		}
		yield return AppUtil.Wait(1.5f);
		List<List<string>> fukidasiKeyList = Counseling.GetFukidasiKeyList(this.ScenarioType);
		Func<TextLocalization, int> <>9__2;
		for (int i = 1; i <= fukidasiMax; i = j + 1)
		{
			this.WaitingChoice = true;
			List<string> fukidasiKey = Counseling.GetFukidasiKey(fukidasiKeyList);
			IEnumerable<TextLocalization> source = new List<TextLocalization>(fukidasiList);
			Func<TextLocalization, int> keySelector;
			if ((keySelector = <>9__2) == null)
			{
				keySelector = (<>9__2 = ((TextLocalization x) => this.rnd.Next()));
			}
			foreach (TextLocalization textLocalization in source.OrderBy(keySelector))
			{
				textLocalization.transform.parent.GetComponent<Image>().sprite = this.PaperSprite[this.rnd.Next(0, this.PaperSprite.Length - 2)];
				textLocalization.transform.parent.GetComponent<Image>().SetNativeSize();
				base.StartCoroutine(AppUtil.FadeIn(textLocalization, 0.3f, null));
				base.StartCoroutine(AppUtil.FadeIn(textLocalization.transform.parent, 0.3f, null));
				string[] array2 = AppUtil.Shift<string>(fukidasiKey).Split(new char[]
				{
					':'
				});
				textLocalization.GetComponent<Text>().enabled = true;
				textLocalization.transform.parent.localRotation = Quaternion.identity;
				textLocalization.transform.parent.GetComponent<Button>().enabled = true;
				textLocalization.transform.parent.name = array2[0];
				textLocalization.SetText(array2[1]);
				textLocalization.transform.parent.gameObject.SetActive(true);
			}
			yield return AppUtil.Wait(0.3f);
			global::Debug.Log("AutoTestEvent:Screenshot");
			while (this.WaitingChoice)
			{
				yield return null;
			}
			this.SetProgressState(base.transform.Find("UICanvas/WordGroup"), (float)i, (float)fukidasiMax);
			yield return AppUtil.Wait(0.25f);
			foreach (TextLocalization textLocalization2 in fukidasiList)
			{
				base.StartCoroutine(AppUtil.FadeOut(textLocalization2.transform.parent, 0.3f, null));
			}
			yield return AppUtil.Wait(0.1f);
			if (i == 10)
			{
				fukidasiKeyList = Counseling.CustomFukidasiKeyList(fukidasiKeyList, this.CounselingData);
				this.CounselingData = "";
			}
			j = i;
		}
		array = fukidasiList;
		for (j = 0; j < array.Length; j++)
		{
			array[j].transform.parent.gameObject.SetActive(true);
		}
		SceneTransition.Transit(delegate
		{
			group.SetActive(false);
		}, null);
		while (!group.activeSelf)
		{
			yield return null;
		}
		yield break;
	}

	// Token: 0x060000F8 RID: 248 RVA: 0x0000BC48 File Offset: 0x00009E48
	private void SetProgressState(Transform parent, float current, float max)
	{
		Transform progressSet = parent.Find("ProgressBarSet");
		progressSet.gameObject.SetActive(max > 1f);
		if (max <= 1f)
		{
			return;
		}
		progressSet.Find("Text").GetComponent<Text>().text = current + "/" + max;
		if (current == 0f)
		{
			progressSet.Find("Bar/Current").GetComponent<Image>().fillAmount = 0f;
			progressSet.Find("Bar/Icon").GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 0f);
			return;
		}
		float fillAmount = progressSet.Find("Bar/Current").GetComponent<Image>().fillAmount;
		float num = current / max;
		float x = progressSet.Find("Bar/Icon").GetComponent<RectTransform>().anchoredPosition.x;
		float endValue = progressSet.Find("Bar").GetComponent<RectTransform>().rect.width * num;
		base.StartCoroutine(AppUtil.MoveEasingFloat(fillAmount, num, delegate(float tmp)
		{
			progressSet.Find("Bar/Current").GetComponent<Image>().fillAmount = tmp;
		}, true, 0.25f, EasingFunction.Ease.EaseOutCubic, null));
		base.StartCoroutine(AppUtil.MoveEasingFloat(x, endValue, delegate(float tmp)
		{
			progressSet.Find("Bar/Icon").GetComponent<RectTransform>().anchoredPosition = new Vector2(tmp, 0f);
		}, true, 0.25f, EasingFunction.Ease.EaseOutCubic, null));
	}

	// Token: 0x060000F9 RID: 249 RVA: 0x0000BDB9 File Offset: 0x00009FB9
	private IEnumerator StartDialogue()
	{
		GameObject commentFrame = this.EsComment.transform.parent.gameObject;
		Image touch = GameObject.Find("MessageScreen").GetComponent<Image>();
		string scenarioType = this.ScenarioType;
		int num;
		for (int i = 1; i <= 100; i = num + 1)
		{
			string scenarioKey = Data.GetScenarioKey(this.ScenarioType, i.ToString(), null, 0);
			string scenarioKey2 = Data.GetScenarioKey(this.ScenarioType, i.ToString() + "問", null, 0);
			string scenarioKey3 = Data.GetScenarioKey(this.ScenarioType, i.ToString() + "分岐", "Law", 0);
			string scenarioKey4 = Data.GetScenarioKey(this.ScenarioType, i.ToString() + "分岐", "SE", 0);
			string scenarioKey5 = Data.GetScenarioKey(this.ScenarioType, i.ToString() + "変化-Law", null, 0);
			string scenarioKey6 = Data.GetScenarioKey(this.ScenarioType, i.ToString() + "変化-立ち絵", null, 0);
			if (scenarioKey != null || scenarioKey3 != null || scenarioKey4 != null)
			{
				string key = scenarioKey;
				if (scenarioKey3 != null)
				{
					key = Data.GetScenarioKey(this.ScenarioType, i.ToString() + "分岐", PlayerStatus.LCTypeTrend, 0);
				}
				if (scenarioKey4 != null)
				{
					string routeTrend = PlayerStatus.RouteTrend;
					key = Data.GetScenarioKey(this.ScenarioType, i.ToString() + "分岐", routeTrend, 0);
				}
				if (scenarioKey5 != null)
				{
					string a = LanguageManager.Get(scenarioKey5);
					if (!(a == "<狂気>"))
					{
						if (a == "<ほほえみ>")
						{
							this.SetEsFace("Smile");
						}
					}
					else
					{
						base.StartCoroutine(this.SetEsCrazy());
					}
				}
				if (scenarioKey6 != null)
				{
					string esFace = LanguageManager.Get(scenarioKey6);
					this.SetEsFace(esFace);
				}
				commentFrame.SetActive(false);
				yield return AppUtil.Wait(0.2f);
				this.EsComment.SetKey(key);
				commentFrame.SetActive(true);
				touch.raycastTarget = true;
				while (touch.raycastTarget)
				{
					yield return null;
				}
				key = null;
			}
			else
			{
				if (scenarioKey2 == null)
				{
					break;
				}
				yield return this.StartDialogueQuestion(i, 0);
			}
			if (scenarioType != this.ScenarioType)
			{
				i = 0;
				scenarioType = this.ScenarioType;
			}
			num = i;
		}
		yield break;
	}

	// Token: 0x060000FA RID: 250 RVA: 0x0000BDC8 File Offset: 0x00009FC8
	private IEnumerator StartDialogueQuestion(int no, int max = 0)
	{
		GameObject commentFrame = this.EsComment.transform.parent.gameObject;
		commentFrame.SetActive(false);
		if (this.ScenarioType.Contains("診断"))
		{
			this.SetProgressState(base.transform.Find("UICanvas/UI2/ChoiceGroup"), (float)(no - 1), (float)max);
			for (int i = 0; i <= max; i++)
			{
				base.transform.Find(string.Concat(new object[]
				{
					"UICanvas/UI2/EhonGroup/",
					this.ScenarioType,
					"/Q",
					i
				})).gameObject.SetActive(i == no);
			}
		}
		if (this.ScenarioType == "診断2-3" || this.ScenarioType == "診断2-4")
		{
			base.transform.Find("UICanvas/FukidasiGroup/" + this.ScenarioType).gameObject.SetActive(true);
		}
		string type = this.ScenarioType;
		if (this.ScenarioNo == "本を借りる")
		{
			this.ScenarioType = "本を借りる1";
			no = 9;
		}
		yield return AppUtil.Wait(0.2f);
		string scenarioKey = Data.GetScenarioKey(this.ScenarioType, no + "問", null, 0);
		this.EsComment.SetKey(scenarioKey);
		commentFrame.SetActive(true);
		ChoiceGroup choice = base.GetComponentInChildren<ChoiceGroup>(true);
		choice.gameObject.SetActive(true);
		string scenarioNo = this.ScenarioNo;
		string[] array;
		if (!(scenarioNo == "日替わり"))
		{
			if (!(scenarioNo == "本を借りる"))
			{
				if (!(scenarioNo == "本を返す"))
				{
					array = new string[]
					{
						"Law",
						"Chaos",
						"Neutral"
					};
				}
				else
				{
					array = new string[]
					{
						"1",
						"2",
						"3"
					};
				}
			}
			else
			{
				int[] array2 = AppUtil.RandomArray(1, 10);
				int length = PlayerStatus.ReadingBookList.Length;
				int num = Mathf.Min(3, length);
				string text = PlayerStatus.ReadingBookList.Substring(length - num, num);
				List<string> list = new List<string>();
				foreach (int num2 in array2)
				{
					string value = num2.ToString("X");
					if (!text.Contains(value))
					{
						list.Add(num2.ToString());
					}
					if (list.Count >= 3)
					{
						break;
					}
				}
				array = list.ToArray();
			}
		}
		else
		{
			array = new string[]
			{
				"Perfect",
				"Good",
				"Normal"
			};
		}
		if (this.ScenarioType == "本を返す共通")
		{
			array = new string[]
			{
				"既読",
				"未読",
				""
			};
		}
		string[] choiceList = new string[]
		{
			"Choice" + array[0],
			"Choice" + array[1],
			"Choice" + array[2]
		};
		string[] choiceKeyList = new string[]
		{
			Data.GetScenarioKey(this.ScenarioType, no + "選択肢", array[0], 0),
			Data.GetScenarioKey(this.ScenarioType, no + "選択肢", array[1], 0),
			Data.GetScenarioKey(this.ScenarioType, no + "選択肢", array[2], 0)
		};
		bool restartChoice = Data.GetScenarioSpecific(this.ScenarioNo).Contains("退行");
		bool isRandom = !restartChoice && this.ScenarioType != "本を返す共通";
		yield return choice.SetButton(choiceList, choiceKeyList, isRandom);
		this.WaitingChoice = true;
		while (this.WaitingChoice)
		{
			yield return null;
		}
		yield return AppUtil.Wait(0.5f);
		string choosenItem = "";
		int num3 = 0;
		for (int k = 0; k < choiceList.Length; k++)
		{
			if (base.transform.Find("UICanvas/UI2/ChoiceGroup/" + choiceList[k]).gameObject.activeSelf)
			{
				choosenItem = choiceList[k].Replace("Choice", "");
				num3 = k;
				break;
			}
		}
		global::Debug.Log("choosenItem=" + choosenItem);
		if (!this.ScenarioNo.Contains("4章"))
		{
			UserChoice.Set(this.ScenarioType, choosenItem);
		}
		if (restartChoice)
		{
			if (choosenItem == "Law")
			{
				this.ScenarioNo = "退行";
			}
			else
			{
				this.ScenarioNo = "退行中止";
			}
		}
		commentFrame.SetActive(false);
		if (choiceKeyList[0].Contains("診断"))
		{
			string scenarioKey2 = Data.GetScenarioKey(this.ScenarioType, no + "解析中表示単語", choosenItem, 0);
			if (scenarioKey2 != null)
			{
				this.WordListForDisplay.AddRange(LanguageManager.Get(scenarioKey2).Split(new char[]
				{
					'、'
				}));
			}
			choice.DismissButton();
			this.SetProgressState(base.transform.Find("UICanvas/UI2/ChoiceGroup"), (float)no, (float)max);
			if (no == max)
			{
				base.transform.Find(string.Concat(new object[]
				{
					"UICanvas/UI2/EhonGroup/",
					this.ScenarioType,
					"/Q",
					no
				})).gameObject.SetActive(false);
				Transform transform = base.transform.Find(string.Concat(new object[]
				{
					"UICanvas/UI2/EhonGroup/",
					this.ScenarioType,
					"/Ending",
					num3 + 1
				}));
				transform.gameObject.SetActive(true);
				base.StartCoroutine(AppUtil.FadeIn(transform, 0.5f, null));
				if (this.ScenarioType == "診断2-5")
				{
					if (num3 == 0)
					{
						this.ScenarioType = "診断2-4";
						if (PlayerStatus.ScenarioNo.Contains("3章"))
						{
							PlayerStatus.Route = "SE";
						}
					}
					else if (num3 == 1)
					{
						this.ScenarioType = "診断2-3";
						if (PlayerStatus.ScenarioNo.Contains("3章"))
						{
							PlayerStatus.Route = "ID";
						}
					}
				}
				if (this.ScenarioType == "診断2-3")
				{
					base.transform.Find("UICanvas/UI2/EhonGroup/Background").GetComponent<Image>().color = Color.black;
					base.transform.Find("UICanvas/UI2/EhonGroup/診断2-5/Background").gameObject.SetActive(false);
					base.transform.Find("UICanvas/UI2/EhonGroup/" + this.ScenarioType + "/Background").gameObject.SetActive(false);
					base.transform.Find("UICanvas/FukidasiGroup/" + this.ScenarioType).gameObject.SetActive(false);
				}
				if (this.ScenarioType == "診断2-4")
				{
					base.transform.Find("UICanvas/FukidasiGroup/" + this.ScenarioType).gameObject.SetActive(false);
				}
			}
		}
		else
		{
			choice.gameObject.SetActive(false);
		}
		yield return AppUtil.Wait(0.5f);
		yield return this.EsTalk(no + "回答-" + choosenItem, false);
		if (this.ScenarioNo == "本を借りる")
		{
			PlayerStatus.ReadingBook = int.Parse(choosenItem);
			this.ScenarioType = type;
		}
		if (this.ScenarioType == "本を返す共通")
		{
			int readingBook = PlayerStatus.ReadingBook;
			this.ScenarioType = string.Concat(new object[]
			{
				"本を返す",
				readingBook,
				"-",
				choosenItem
			});
			if (Data.GetScenarioKey(this.ScenarioType, "1", null, 0) == null)
			{
				this.ScenarioType = string.Concat(new object[]
				{
					"本を返す",
					7,
					"-",
					choosenItem
				});
			}
		}
		yield break;
	}

	// Token: 0x060000FB RID: 251 RVA: 0x0000BDE5 File Offset: 0x00009FE5
	public SceneTalk()
	{
	}

	// Token: 0x060000FC RID: 252 RVA: 0x0000BDF8 File Offset: 0x00009FF8
	[CompilerGenerated]
	private void <OnClick>b__22_1(bool ret)
	{
		this.RetryWithReward(ret);
	}

	// Token: 0x060000FD RID: 253 RVA: 0x0000BDF8 File Offset: 0x00009FF8
	[CompilerGenerated]
	private void <OnClick>b__22_2(bool ret)
	{
		this.RetryWithReward(ret);
	}

	// Token: 0x060000FE RID: 254 RVA: 0x0000BE01 File Offset: 0x0000A001
	[CompilerGenerated]
	private void <ScenarioLevelUp>b__29_0()
	{
		this.SetFilter(this.ScenarioNo != "本を返す");
	}

	// Token: 0x0400003B RID: 59
	public TextLocalization EsComment;

	// Token: 0x0400003C RID: 60
	public ResultCanvas ResultCanvas;

	// Token: 0x0400003D RID: 61
	private bool WaitingChoice;

	// Token: 0x0400003E RID: 62
	private string CounselingData;

	// Token: 0x0400003F RID: 63
	private int TalkCount;

	// Token: 0x04000040 RID: 64
	private IEnumerator PreComment;

	// Token: 0x04000041 RID: 65
	private Random rnd = new Random();

	// Token: 0x04000042 RID: 66
	private string ScenarioNo;

	// Token: 0x04000043 RID: 67
	private string ScenarioType;

	// Token: 0x04000044 RID: 68
	private List<string> WordListForDisplay;

	// Token: 0x04000045 RID: 69
	private TalkItem ClickedTalkItem;

	// Token: 0x04000046 RID: 70
	[SerializeField]
	private Sprite EsStandard;

	// Token: 0x04000047 RID: 71
	[SerializeField]
	private Sprite EsStandardBG;

	// Token: 0x04000048 RID: 72
	[SerializeField]
	private Sprite EsCrazy;

	// Token: 0x04000049 RID: 73
	[SerializeField]
	private Sprite EsCrazyBG;

	// Token: 0x0400004A RID: 74
	[SerializeField]
	private Sprite[] PaperSprite;

	// Token: 0x020000E4 RID: 228
	[CompilerGenerated]
	private sealed class <>c__DisplayClass17_0
	{
		// Token: 0x06000858 RID: 2136 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass17_0()
		{
		}

		// Token: 0x06000859 RID: 2137 RVA: 0x00029CF8 File Offset: 0x00027EF8
		internal void <FixedUpdate>b__0()
		{
			if (this.butterfly != null)
			{
				this.butterfly.enabled = true;
			}
		}

		// Token: 0x04000353 RID: 851
		public Animator butterfly;
	}

	// Token: 0x020000E5 RID: 229
	[CompilerGenerated]
	private sealed class <>c__DisplayClass21_0
	{
		// Token: 0x0600085A RID: 2138 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass21_0()
		{
		}

		// Token: 0x0600085B RID: 2139 RVA: 0x00029D14 File Offset: 0x00027F14
		internal void <SetFilter>b__0(float tmp)
		{
			this.filter.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, tmp);
		}

		// Token: 0x04000354 RID: 852
		public Transform filter;
	}

	// Token: 0x020000E6 RID: 230
	[CompilerGenerated]
	[Serializable]
	private sealed class <>c
	{
		// Token: 0x0600085C RID: 2140 RVA: 0x00029D31 File Offset: 0x00027F31
		// Note: this type is marked as 'beforefieldinit'.
		static <>c()
		{
		}

		// Token: 0x0600085D RID: 2141 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c()
		{
		}

		// Token: 0x0600085E RID: 2142 RVA: 0x00029D3D File Offset: 0x00027F3D
		internal void <OnClick>b__22_0()
		{
			SceneManager.LoadScene("Ending", LoadSceneMode.Additive);
		}

		// Token: 0x04000355 RID: 853
		public static readonly SceneTalk.<>c <>9 = new SceneTalk.<>c();

		// Token: 0x04000356 RID: 854
		public static Action <>9__22_0;
	}

	// Token: 0x020000E7 RID: 231
	[CompilerGenerated]
	private sealed class <SetComment>d__24 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x0600085F RID: 2143 RVA: 0x00029D4A File Offset: 0x00027F4A
		[DebuggerHidden]
		public <SetComment>d__24(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000860 RID: 2144 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000861 RID: 2145 RVA: 0x00029D5C File Offset: 0x00027F5C
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			SceneTalk sceneTalk = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				commentFrame = sceneTalk.EsComment.transform.parent.gameObject;
				if (commentFrame.activeSelf)
				{
					commentFrame.SetActive(false);
					this.<>2__current = AppUtil.Wait(0.2f);
					this.<>1__state = 1;
					return true;
				}
				break;
			case 1:
				this.<>1__state = -1;
				break;
			case 2:
				this.<>1__state = -1;
				sceneTalk.transform.Find("UICanvas/UI1/TalkArea").gameObject.SetActive(true);
				goto IL_D4;
			case 3:
				this.<>1__state = -1;
				commentFrame.SetActive(false);
				if (state == "怒り")
				{
					this.<>2__current = sceneTalk.SetEsStatus("待機", null);
					this.<>1__state = 4;
					return true;
				}
				goto IL_151;
			case 4:
				this.<>1__state = -1;
				goto IL_151;
			default:
				return false;
			}
			if (state == "怒り")
			{
				this.<>2__current = sceneTalk.SetEsStatus("対話", null);
				this.<>1__state = 2;
				return true;
			}
			IL_D4:
			sceneTalk.EsComment.SetKey(key);
			commentFrame.SetActive(true);
			this.<>2__current = AppUtil.Wait(2f);
			this.<>1__state = 3;
			return true;
			IL_151:
			sceneTalk.PreComment = null;
			return false;
		}

		// Token: 0x170000F4 RID: 244
		// (get) Token: 0x06000862 RID: 2146 RVA: 0x00029EC2 File Offset: 0x000280C2
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000863 RID: 2147 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000F5 RID: 245
		// (get) Token: 0x06000864 RID: 2148 RVA: 0x00029EC2 File Offset: 0x000280C2
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x04000357 RID: 855
		private int <>1__state;

		// Token: 0x04000358 RID: 856
		private object <>2__current;

		// Token: 0x04000359 RID: 857
		public SceneTalk <>4__this;

		// Token: 0x0400035A RID: 858
		public string state;

		// Token: 0x0400035B RID: 859
		public string key;

		// Token: 0x0400035C RID: 860
		private GameObject <commentFrame>5__2;
	}

	// Token: 0x020000E8 RID: 232
	[CompilerGenerated]
	private sealed class <SetEsCrazy>d__25 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000865 RID: 2149 RVA: 0x00029ECA File Offset: 0x000280CA
		[DebuggerHidden]
		public <SetEsCrazy>d__25(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000866 RID: 2150 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000867 RID: 2151 RVA: 0x00029EDC File Offset: 0x000280DC
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			SceneTalk sceneTalk = this;
			if (num != 0)
			{
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				sceneTalk.OnClick(talkArea);
			}
			else
			{
				this.<>1__state = -1;
				sceneTalk.transform.Find("BGCanvas/Es正面/EsMain").GetComponent<Image>().sprite = sceneTalk.EsCrazy;
				sceneTalk.transform.Find("BGCanvas/Es正面/Background").GetComponent<Image>().sprite = sceneTalk.EsCrazyBG;
				sceneTalk.SetEsFace("Main");
				talkArea = sceneTalk.transform.Find("UICanvas/UI1/TalkArea").gameObject;
				if (!PlayerStatus.ScenarioNo.Contains("4章ID"))
				{
					return false;
				}
			}
			this.<>2__current = AppUtil.Wait(Random.Range(0.5f, 2.5f));
			this.<>1__state = 1;
			return true;
		}

		// Token: 0x170000F6 RID: 246
		// (get) Token: 0x06000868 RID: 2152 RVA: 0x00029FBA File Offset: 0x000281BA
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000869 RID: 2153 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000F7 RID: 247
		// (get) Token: 0x0600086A RID: 2154 RVA: 0x00029FBA File Offset: 0x000281BA
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0400035D RID: 861
		private int <>1__state;

		// Token: 0x0400035E RID: 862
		private object <>2__current;

		// Token: 0x0400035F RID: 863
		public SceneTalk <>4__this;

		// Token: 0x04000360 RID: 864
		private GameObject <talkArea>5__2;
	}

	// Token: 0x020000E9 RID: 233
	[CompilerGenerated]
	private sealed class <>c__DisplayClass27_0
	{
		// Token: 0x0600086B RID: 2155 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass27_0()
		{
		}

		// Token: 0x0600086C RID: 2156 RVA: 0x00029FC4 File Offset: 0x000281C4
		internal void <SetEsStatus>b__0()
		{
			foreach (string text in new string[]
			{
				"正面",
				"横向き",
				"アオリ"
			})
			{
				this.<>4__this.transform.Find("BGCanvas/Es" + text).gameObject.SetActive(text == this.direction);
			}
			if (this.changeScene != null)
			{
				this.changeScene();
			}
		}

		// Token: 0x04000361 RID: 865
		public SceneTalk <>4__this;

		// Token: 0x04000362 RID: 866
		public string direction;

		// Token: 0x04000363 RID: 867
		public Action changeScene;
	}

	// Token: 0x020000EA RID: 234
	[CompilerGenerated]
	private sealed class <SetEsStatus>d__27 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x0600086D RID: 2157 RVA: 0x0002A046 File Offset: 0x00028246
		[DebuggerHidden]
		public <SetEsStatus>d__27(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x0600086E RID: 2158 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x0600086F RID: 2159 RVA: 0x0002A058 File Offset: 0x00028258
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			SceneTalk sceneTalk = this;
			if (num != 0)
			{
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
			}
			else
			{
				this.<>1__state = -1;
				CS$<>8__locals1 = new SceneTalk.<>c__DisplayClass27_0();
				CS$<>8__locals1.<>4__this = this;
				CS$<>8__locals1.changeScene = changeScene;
				global::Debug.Log("SetEsStatus " + state);
				sceneTalk.transform.Find("BGCanvas/Es正面/EsMain").GetComponent<Image>().sprite = sceneTalk.EsStandard;
				sceneTalk.transform.Find("BGCanvas/Es正面/Background").GetComponent<Image>().sprite = sceneTalk.EsStandardBG;
				sceneTalk.transform.Find("UICanvas/UI1/TalkArea").gameObject.SetActive(false);
				CS$<>8__locals1.direction = "正面";
				if (state.Contains("対話") || state.Contains("診断1") || state.Contains("雑談") || state.Contains("日替わり") || state.Contains("本を") || state.Contains("自我とエス"))
				{
					state = "対話";
				}
				if (state.Contains("診断2"))
				{
					CS$<>8__locals1.direction = "横向き";
					state = "横向き";
					sceneTalk.transform.Find("BGCanvas/Es横向き/Background/MoveEye").gameObject.SetActive(false);
				}
				else if (state.Contains("診断3") || state.Contains("診断4"))
				{
					CS$<>8__locals1.direction = "アオリ";
					state = "アオリ";
				}
				if (!sceneTalk.transform.Find("BGCanvas/Es" + CS$<>8__locals1.direction).gameObject.activeSelf)
				{
					SceneTransition.Transit(new Action(CS$<>8__locals1.<SetEsStatus>b__0), null);
					this.<>2__current = AppUtil.WaitRealtime(0.75f);
					this.<>1__state = 1;
					return true;
				}
				if (CS$<>8__locals1.changeScene != null)
				{
					CS$<>8__locals1.changeScene();
				}
			}
			sceneTalk.EsComment = sceneTalk.transform.Find("BGCanvas/Es" + CS$<>8__locals1.direction).GetComponentInChildren<TextLocalization>(true);
			sceneTalk.EsComment.transform.parent.gameObject.SetActive(false);
			string a = state;
			if (!(a == "不在"))
			{
				if (!(a == "退室"))
				{
					if (!(a == "待機"))
					{
						if (a == "対話")
						{
							sceneTalk.transform.Find("BGCanvas/Es正面/EsMain").gameObject.SetActive(true);
							string esFace = "Main";
							if (PlayerStatus.ScenarioNo.Contains("4章AE"))
							{
								if (Data.GetScenarioSpecific(sceneTalk.ScenarioNo).Contains("退行"))
								{
									esFace = "Smile";
								}
								else
								{
									string scenarioKey = Data.GetScenarioKey(sceneTalk.ScenarioType, "立ち絵", null, 0);
									if (scenarioKey != null)
									{
										esFace = LanguageManager.Get(scenarioKey);
									}
									else
									{
										a = sceneTalk.ScenarioNo;
										if (!(a == "雑談") && !(a == "本を借りる") && !(a == "本を返す"))
										{
											if (!(a == "自我とエス(ID)"))
											{
												if (!(a == "自我とエス(SE)"))
												{
													if (a == "自我とエス(AE)")
													{
														esFace = "ほほえみ";
													}
												}
												else
												{
													esFace = "SE";
												}
											}
											else
											{
												esFace = "ID";
											}
										}
										else
										{
											esFace = "Smile";
										}
									}
								}
							}
							sceneTalk.SetEsFace(esFace);
						}
					}
					else
					{
						sceneTalk.transform.Find("BGCanvas/Es正面/EsMain").gameObject.SetActive(true);
						sceneTalk.transform.Find("UICanvas/UI1/TalkArea").gameObject.SetActive(true);
						sceneTalk.SetEsFace("Wait");
					}
				}
				else
				{
					sceneTalk.StartCoroutine(AppUtil.FadeOut(sceneTalk.transform.Find("BGCanvas/Es正面/EsMain"), 0.5f, null));
				}
			}
			else
			{
				sceneTalk.transform.Find("BGCanvas/Es正面/EsMain").gameObject.SetActive(false);
			}
			return false;
		}

		// Token: 0x170000F8 RID: 248
		// (get) Token: 0x06000870 RID: 2160 RVA: 0x0002A4D5 File Offset: 0x000286D5
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000871 RID: 2161 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000F9 RID: 249
		// (get) Token: 0x06000872 RID: 2162 RVA: 0x0002A4D5 File Offset: 0x000286D5
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x04000364 RID: 868
		private int <>1__state;

		// Token: 0x04000365 RID: 869
		private object <>2__current;

		// Token: 0x04000366 RID: 870
		public SceneTalk <>4__this;

		// Token: 0x04000367 RID: 871
		public Action changeScene;

		// Token: 0x04000368 RID: 872
		public string state;

		// Token: 0x04000369 RID: 873
		private SceneTalk.<>c__DisplayClass27_0 <>8__1;
	}

	// Token: 0x020000EB RID: 235
	[CompilerGenerated]
	private sealed class <>c__DisplayClass28_0
	{
		// Token: 0x06000873 RID: 2163 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass28_0()
		{
		}

		// Token: 0x06000874 RID: 2164 RVA: 0x0002A4E0 File Offset: 0x000286E0
		internal void <Closing>b__0()
		{
			this.<>4__this.EsComment.transform.parent.gameObject.SetActive(false);
			this.<>4__this.transform.Find("UICanvas/UI2/TalkItemGroup").gameObject.SetActive(true);
			this.<>4__this.transform.Find("UICanvas/UI_Footer").gameObject.SetActive(true);
			this.<>4__this.transform.Find("UICanvas/UI2/ChoiceGroup").gameObject.SetActive(false);
			this.<>4__this.transform.Find("UICanvas/UI2/EhonGroup").gameObject.SetActive(false);
			this.<>4__this.transform.Find("BGCanvas/Es横向き/Background/MoveEye").gameObject.SetActive(false);
			if (this.levelUp)
			{
				this.<>4__this.ScenarioLevelUp();
				return;
			}
			if (this.<>4__this.ScenarioNo == "3章AE-5")
			{
				this.<>4__this.ResetButtonList();
			}
		}

		// Token: 0x0400036A RID: 874
		public SceneTalk <>4__this;

		// Token: 0x0400036B RID: 875
		public bool levelUp;
	}

	// Token: 0x020000EC RID: 236
	[CompilerGenerated]
	private sealed class <Closing>d__28 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000875 RID: 2165 RVA: 0x0002A5E3 File Offset: 0x000287E3
		[DebuggerHidden]
		public <Closing>d__28(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000876 RID: 2166 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000877 RID: 2167 RVA: 0x0002A5F4 File Offset: 0x000287F4
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			SceneTalk sceneTalk = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				CS$<>8__locals1 = new SceneTalk.<>c__DisplayClass28_0();
				CS$<>8__locals1.<>4__this = this;
				sceneTalk.ResultCanvas.SetActive(false);
				if (Data.GetScenarioKey(sceneTalk.ScenarioType, "結び", null, 1) != null)
				{
					this.<>2__current = sceneTalk.EsTalk("結び", true);
					this.<>1__state = 1;
					return true;
				}
				break;
			case 1:
				this.<>1__state = -1;
				break;
			case 2:
				this.<>1__state = -1;
				if (AdInfo.ENABLE)
				{
					DialogManager.ShowDialog("DailyBonusDialog", new object[]
					{
						bonusEgo
					});
				}
				else
				{
					DialogManager.ShowDialog("GetDailyBonusDialog", new object[]
					{
						bonusEgo
					});
				}
				this.<>2__current = AppUtil.Wait(0.2f);
				this.<>1__state = 3;
				return true;
			case 3:
				this.<>1__state = -1;
				goto IL_203;
			default:
				return false;
			}
			string scenarioNo = sceneTalk.ScenarioNo;
			if (scenarioNo == "日替わり")
			{
				int intParameter = Utility.GetIntParameter(sceneTalk.ScenarioNo + sceneTalk.CounselingData.Replace(",", ""));
				bonusEgo = PlayerStatus.EgoPerSecond * (float)intParameter * 60f * 60f * Settings.GAME_SPEED;
				sceneTalk.EsComment.transform.parent.gameObject.SetActive(false);
				this.<>2__current = AppUtil.Wait(0.2f);
				this.<>1__state = 2;
				return true;
			}
			if (scenarioNo == "本を借りる")
			{
				int intParameter2 = Utility.GetIntParameter("読書時間");
				TimeManager.Reset(TimeManager.TYPE.END_BOOK, TimeSpan.FromHours((double)intParameter2) + TimeSpan.FromSeconds(0.8999999761581421));
				if (sceneTalk.ClickedTalkItem.Price != null)
				{
					PlayerStatus.EgoPoint -= sceneTalk.ClickedTalkItem.Price;
				}
				sceneTalk.ClickedTalkItem.SetItem();
			}
			IL_203:
			bonusEgo = null;
			string state = "待機";
			CS$<>8__locals1.levelUp = (sceneTalk.ScenarioNo == PlayerStatus.ScenarioNo || sceneTalk.ScenarioNo == "本を返す");
			if (CS$<>8__locals1.levelUp)
			{
				if (Data.GetScenarioSpecific("").Contains("エンディング"))
				{
					PlayerResult.Ending += PlayerStatus.Route;
					AdManager.Hide("Banner");
					if (PlayerStatus.Route == "AE")
					{
						SceneTransition.LoadScene("Ending", new Color?(Color.black), 3f);
						PlayerStatus.ReadingBook = 0;
						PlayerStatus.ReadingBookList = "";
						string scenarioID = "ゲームクリア";
						string value;
						if (PlayerResult.EsTapCount >= 500)
						{
							value = "エス崇拝者";
						}
						else if (PlayerResult.TapCount >= 10000)
						{
							value = "自問の達人";
						}
						else if (PlayerResult.Ending.StartsWith("SE"))
						{
							value = "管理者";
						}
						else
						{
							value = "解放者";
						}
						CounselingResult.Set(scenarioID, value);
					}
					else
					{
						DialogManager.ShowDialog("EndingScreen" + PlayerStatus.Route, Array.Empty<object>());
					}
					PlayerStatus.EgoPoint -= sceneTalk.ClickedTalkItem.Price;
					Data.GoToNextScenario();
					return false;
				}
				if (Data.GetNextScenarioSpecific().Contains("エス不在"))
				{
					state = "退室";
				}
			}
			sceneTalk.StartCoroutine(sceneTalk.SetEsStatus(state, new Action(CS$<>8__locals1.<Closing>b__0)));
			return false;
		}

		// Token: 0x170000FA RID: 250
		// (get) Token: 0x06000878 RID: 2168 RVA: 0x0002A98F File Offset: 0x00028B8F
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000879 RID: 2169 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000FB RID: 251
		// (get) Token: 0x0600087A RID: 2170 RVA: 0x0002A98F File Offset: 0x00028B8F
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0400036C RID: 876
		private int <>1__state;

		// Token: 0x0400036D RID: 877
		private object <>2__current;

		// Token: 0x0400036E RID: 878
		public SceneTalk <>4__this;

		// Token: 0x0400036F RID: 879
		private SceneTalk.<>c__DisplayClass28_0 <>8__1;

		// Token: 0x04000370 RID: 880
		private EgoPoint <bonusEgo>5__2;
	}

	// Token: 0x020000ED RID: 237
	[CompilerGenerated]
	private sealed class <EsTalk>d__30 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x0600087B RID: 2171 RVA: 0x0002A997 File Offset: 0x00028B97
		[DebuggerHidden]
		public <EsTalk>d__30(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x0600087C RID: 2172 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x0600087D RID: 2173 RVA: 0x0002A9A8 File Offset: 0x00028BA8
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			SceneTalk sceneTalk = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				commentFrame = sceneTalk.EsComment.transform.parent.gameObject;
				touch = GameObject.Find("MessageScreen").GetComponent<Image>();
				if (wait)
				{
					commentFrame.SetActive(false);
					this.<>2__current = AppUtil.Wait(1f);
					this.<>1__state = 1;
					return true;
				}
				break;
			case 1:
				this.<>1__state = -1;
				break;
			case 2:
				this.<>1__state = -1;
				sceneTalk.EsComment.SetKey(key);
				commentFrame.SetActive(true);
				touch.raycastTarget = true;
				goto IL_134;
			case 3:
				this.<>1__state = -1;
				goto IL_134;
			default:
				return false;
			}
			i = 1;
			goto IL_158;
			IL_134:
			if (touch.raycastTarget)
			{
				this.<>2__current = null;
				this.<>1__state = 3;
				return true;
			}
			key = null;
			int num2 = i;
			i = num2 + 1;
			IL_158:
			if (i <= 100)
			{
				key = Data.GetScenarioKey(sceneTalk.ScenarioType, keyHeader, null, i);
				if (key != null)
				{
					commentFrame.SetActive(false);
					this.<>2__current = AppUtil.Wait(0.2f);
					this.<>1__state = 2;
					return true;
				}
			}
			return false;
		}

		// Token: 0x170000FC RID: 252
		// (get) Token: 0x0600087E RID: 2174 RVA: 0x0002AB1B File Offset: 0x00028D1B
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0600087F RID: 2175 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000FD RID: 253
		// (get) Token: 0x06000880 RID: 2176 RVA: 0x0002AB1B File Offset: 0x00028D1B
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x04000371 RID: 881
		private int <>1__state;

		// Token: 0x04000372 RID: 882
		private object <>2__current;

		// Token: 0x04000373 RID: 883
		public SceneTalk <>4__this;

		// Token: 0x04000374 RID: 884
		public bool wait;

		// Token: 0x04000375 RID: 885
		public string keyHeader;

		// Token: 0x04000376 RID: 886
		private GameObject <commentFrame>5__2;

		// Token: 0x04000377 RID: 887
		private Image <touch>5__3;

		// Token: 0x04000378 RID: 888
		private int <i>5__4;

		// Token: 0x04000379 RID: 889
		private string <key>5__5;
	}

	// Token: 0x020000EE RID: 238
	[CompilerGenerated]
	private sealed class <StartCounseling>d__31 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000881 RID: 2177 RVA: 0x0002AB23 File Offset: 0x00028D23
		[DebuggerHidden]
		public <StartCounseling>d__31(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000882 RID: 2178 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000883 RID: 2179 RVA: 0x0002AB34 File Offset: 0x00028D34
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			SceneTalk sceneTalk = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				HyperActive.Init();
				sceneTalk.CounselingData = "";
				sceneTalk.transform.Find("UICanvas/UI2/InputGroup/ThinkingArea").GetComponent<InputField>().text = "";
				sceneTalk.transform.Find("UICanvas/UI2/TalkItemGroup").gameObject.SetActive(false);
				sceneTalk.transform.Find("UICanvas/UI_Footer").gameObject.SetActive(false);
				if (sceneTalk.transform.Find("UICanvas/UI1/ButterflyBonus") != null)
				{
					sceneTalk.transform.Find("UICanvas/UI1/ButterflyBonus").gameObject.SetActive(false);
				}
				this.<>2__current = sceneTalk.SetEsStatus(sceneTalk.ScenarioType, null);
				this.<>1__state = 1;
				return true;
			case 1:
				this.<>1__state = -1;
				this.<>2__current = sceneTalk.EsTalk("導入", true);
				this.<>1__state = 2;
				return true;
			case 2:
			{
				this.<>1__state = -1;
				string scenarioSpecific = Data.GetScenarioSpecific(sceneTalk.ScenarioNo);
				if (!sceneTalk.ScenarioType.Contains("診断"))
				{
					this.<>2__current = sceneTalk.StartDialogue();
					this.<>1__state = 5;
					return true;
				}
				sceneTalk.WaitingChoice = true;
				if (PlayerResult.EndingCount <= 0 || scenarioSpecific.Contains("ルート分岐"))
				{
					goto IL_18C;
				}
				DialogManager.ShowDialog("SkipCounselingDialog", new object[]
				{
					sceneTalk.ScenarioType
				});
				break;
			}
			case 3:
				this.<>1__state = -1;
				break;
			case 4:
				this.<>1__state = -1;
				return false;
			case 5:
				this.<>1__state = -1;
				if (sceneTalk.ScenarioNo == "退行")
				{
					Data.Restart();
					return false;
				}
				if (!sceneTalk.ScenarioType.Contains("雑談"))
				{
					goto IL_221;
				}
				PlayerResult.EsTalkCount++;
				if (PlayerResult.EsTalkCount >= 100)
				{
					Utility.PromptReview("雑談100回");
					goto IL_221;
				}
				goto IL_221;
			case 6:
				this.<>1__state = -1;
				return false;
			default:
				return false;
			}
			if (DialogManager.IsShowing())
			{
				this.<>2__current = null;
				this.<>1__state = 3;
				return true;
			}
			IL_18C:
			if (sceneTalk.WaitingChoice)
			{
				this.<>2__current = sceneTalk.StartCoroutine(sceneTalk.StartCounselingType());
				this.<>1__state = 4;
				return true;
			}
			IL_221:
			this.<>2__current = sceneTalk.Closing();
			this.<>1__state = 6;
			return true;
		}

		// Token: 0x170000FE RID: 254
		// (get) Token: 0x06000884 RID: 2180 RVA: 0x0002AD7F File Offset: 0x00028F7F
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000885 RID: 2181 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170000FF RID: 255
		// (get) Token: 0x06000886 RID: 2182 RVA: 0x0002AD7F File Offset: 0x00028F7F
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0400037A RID: 890
		private int <>1__state;

		// Token: 0x0400037B RID: 891
		private object <>2__current;

		// Token: 0x0400037C RID: 892
		public SceneTalk <>4__this;
	}

	// Token: 0x020000EF RID: 239
	[CompilerGenerated]
	private sealed class <StartCounselingType>d__32 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000887 RID: 2183 RVA: 0x0002AD87 File Offset: 0x00028F87
		[DebuggerHidden]
		public <StartCounselingType>d__32(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000888 RID: 2184 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000889 RID: 2185 RVA: 0x0002AD98 File Offset: 0x00028F98
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			SceneTalk sceneTalk = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				sceneTalk.WordListForDisplay = new List<string>();
				preEsComment = sceneTalk.EsComment;
				closingKey = "結果導入";
				if (sceneTalk.ScenarioType.Contains("診断1"))
				{
					this.<>2__current = sceneTalk.StartCounselingType1();
					this.<>1__state = 1;
					return true;
				}
				if (sceneTalk.ScenarioType.Contains("診断2"))
				{
					this.<>2__current = sceneTalk.StartCounselingType2();
					this.<>1__state = 2;
					return true;
				}
				if (sceneTalk.ScenarioType.Contains("診断3"))
				{
					this.<>2__current = sceneTalk.StartCounselingType3();
					this.<>1__state = 3;
					return true;
				}
				if (sceneTalk.ScenarioType.Contains("診断4"))
				{
					this.<>2__current = sceneTalk.StartCounselingType4();
					this.<>1__state = 4;
					return true;
				}
				break;
			case 1:
				this.<>1__state = -1;
				break;
			case 2:
				this.<>1__state = -1;
				break;
			case 3:
				this.<>1__state = -1;
				break;
			case 4:
				this.<>1__state = -1;
				if (sceneTalk.CounselingData.Length <= 10)
				{
					closingKey += "10";
				}
				else if (sceneTalk.CounselingData.Length >= 100)
				{
					closingKey += "100";
				}
				break;
			case 5:
			{
				this.<>1__state = -1;
				sceneTalk.EsComment = preEsComment;
				sceneTalk.EsComment.transform.parent.gameObject.SetActive(false);
				string scenarioType = sceneTalk.ScenarioType;
				if (scenarioType == "診断2-3" || scenarioType == "診断2-4" || scenarioType == "診断2-5")
				{
					this.<>2__current = AppUtil.FadeOut(sceneTalk.transform.Find("UICanvas/UI2/EhonGroup"), 1f, null);
					this.<>1__state = 6;
					return true;
				}
				string[] counselingType = Counseling.GetCounselingType(sceneTalk.ScenarioType, sceneTalk.CounselingData);
				string text = counselingType[0];
				CounselingResult.Set(sceneTalk.ScenarioType, text);
				if (sceneTalk.ScenarioType.Contains("診断4"))
				{
					sceneTalk.WordListForDisplay.AddRange(AppUtil.GetChildArray(counselingType, 1, true, 0));
					string text2 = text;
					if (text.Contains("その他"))
					{
						text2 = "ノンジャンル";
					}
					foreach (string type in text2.Split(new char[]
					{
						'+'
					}))
					{
						string scenarioKey = Data.GetScenarioKey(sceneTalk.ScenarioType, "解析中表示単語", type, 0);
						sceneTalk.WordListForDisplay.AddRange(LanguageManager.Get(scenarioKey).Split(new char[]
						{
							'、'
						}));
					}
				}
				sceneTalk.ResultCanvas.RequestResult(sceneTalk.ScenarioType, text, sceneTalk.WordListForDisplay);
				AppUtil.SetFalse("UICanvas/UI2/EhonGroup");
				AppUtil.SetFalse("BGCanvas/Es横向き/Background/MoveEye");
				return false;
			}
			case 6:
				this.<>1__state = -1;
				sceneTalk.StartCoroutine(sceneTalk.Closing());
				return false;
			default:
				return false;
			}
			this.<>2__current = sceneTalk.EsTalk(closingKey, true);
			this.<>1__state = 5;
			return true;
		}

		// Token: 0x17000100 RID: 256
		// (get) Token: 0x0600088A RID: 2186 RVA: 0x0002B0C7 File Offset: 0x000292C7
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0600088B RID: 2187 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000101 RID: 257
		// (get) Token: 0x0600088C RID: 2188 RVA: 0x0002B0C7 File Offset: 0x000292C7
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0400037D RID: 893
		private int <>1__state;

		// Token: 0x0400037E RID: 894
		private object <>2__current;

		// Token: 0x0400037F RID: 895
		public SceneTalk <>4__this;

		// Token: 0x04000380 RID: 896
		private TextLocalization <preEsComment>5__2;

		// Token: 0x04000381 RID: 897
		private string <closingKey>5__3;
	}

	// Token: 0x020000F0 RID: 240
	[CompilerGenerated]
	private sealed class <StartCounselingType1>d__33 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x0600088D RID: 2189 RVA: 0x0002B0CF File Offset: 0x000292CF
		[DebuggerHidden]
		public <StartCounselingType1>d__33(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x0600088E RID: 2190 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x0600088F RID: 2191 RVA: 0x0002B0E0 File Offset: 0x000292E0
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			SceneTalk sceneTalk = this;
			switch (num)
			{
			case 0:
			{
				this.<>1__state = -1;
				int j;
				for (max = 1; max <= 20; max = j + 1)
				{
					if (Data.GetScenarioKey(sceneTalk.ScenarioType, "問", null, max) == null)
					{
						j = max;
						max = j - 1;
						break;
					}
					j = max;
				}
				choice = sceneTalk.GetComponentInChildren<ChoiceGroup>(true);
				choice.gameObject.SetActive(true);
				sceneTalk.SetProgressState(sceneTalk.transform.Find("UICanvas/UI2/ChoiceGroup"), 0f, (float)max);
				commentFrame = sceneTalk.EsComment.transform.parent.gameObject;
				buttonNameList = new string[]
				{
					"YesButton",
					"NoButton",
					"NAButton"
				};
				i = 1;
				goto IL_300;
			}
			case 1:
				this.<>1__state = -1;
				key = Data.GetScenarioKey(sceneTalk.ScenarioType, "問", null, i);
				commentFrame.SetActive(false);
				this.<>2__current = AppUtil.Wait(0.2f);
				this.<>1__state = 2;
				return true;
			case 2:
				this.<>1__state = -1;
				sceneTalk.EsComment.SetKey(key);
				commentFrame.SetActive(true);
				this.<>2__current = choice.SetButton(buttonNameList, new string[]
				{
					"[UI]YesButton/Text",
					"[UI]NoButton/Text",
					"[UI]NAButton/Text"
				}, false);
				this.<>1__state = 3;
				return true;
			case 3:
				this.<>1__state = -1;
				sceneTalk.WaitingChoice = true;
				break;
			case 4:
				this.<>1__state = -1;
				break;
			case 5:
			{
				this.<>1__state = -1;
				key = null;
				int j = i;
				i = j + 1;
				goto IL_300;
			}
			default:
				return false;
			}
			if (!sceneTalk.WaitingChoice)
			{
				string str = "";
				foreach (string text in buttonNameList)
				{
					if (sceneTalk.transform.Find("UICanvas/UI2/ChoiceGroup/" + text).gameObject.activeSelf)
					{
						str = text.Replace("Button", "");
					}
				}
				string id = key.Replace("問", "解析中表示単語") + "-" + str;
				sceneTalk.WordListForDisplay.AddRange(LanguageManager.Get(id).Split(new char[]
				{
					'、'
				}));
				sceneTalk.SetProgressState(sceneTalk.transform.Find("UICanvas/UI2/ChoiceGroup"), (float)i, (float)max);
				this.<>2__current = AppUtil.Wait(0.5f);
				this.<>1__state = 5;
				return true;
			}
			this.<>2__current = null;
			this.<>1__state = 4;
			return true;
			IL_300:
			if (i > max)
			{
				choice.gameObject.SetActive(false);
				return false;
			}
			this.<>2__current = choice.SetButton(null, null, false);
			this.<>1__state = 1;
			return true;
		}

		// Token: 0x17000102 RID: 258
		// (get) Token: 0x06000890 RID: 2192 RVA: 0x0002B410 File Offset: 0x00029610
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000891 RID: 2193 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000103 RID: 259
		// (get) Token: 0x06000892 RID: 2194 RVA: 0x0002B410 File Offset: 0x00029610
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x04000382 RID: 898
		private int <>1__state;

		// Token: 0x04000383 RID: 899
		private object <>2__current;

		// Token: 0x04000384 RID: 900
		public SceneTalk <>4__this;

		// Token: 0x04000385 RID: 901
		private int <max>5__2;

		// Token: 0x04000386 RID: 902
		private ChoiceGroup <choice>5__3;

		// Token: 0x04000387 RID: 903
		private GameObject <commentFrame>5__4;

		// Token: 0x04000388 RID: 904
		private string[] <buttonNameList>5__5;

		// Token: 0x04000389 RID: 905
		private int <i>5__6;

		// Token: 0x0400038A RID: 906
		private string <key>5__7;
	}

	// Token: 0x020000F1 RID: 241
	[CompilerGenerated]
	private sealed class <>c__DisplayClass34_0
	{
		// Token: 0x06000893 RID: 2195 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass34_0()
		{
		}

		// Token: 0x06000894 RID: 2196 RVA: 0x0002B418 File Offset: 0x00029618
		internal void <StartCounselingType2>b__0(Object target)
		{
			this.Q01.SetActive(false);
		}

		// Token: 0x0400038B RID: 907
		public GameObject Q01;
	}

	// Token: 0x020000F2 RID: 242
	[CompilerGenerated]
	private sealed class <StartCounselingType2>d__34 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x06000895 RID: 2197 RVA: 0x0002B426 File Offset: 0x00029626
		[DebuggerHidden]
		public <StartCounselingType2>d__34(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x06000896 RID: 2198 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x06000897 RID: 2199 RVA: 0x0002B438 File Offset: 0x00029638
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			SceneTalk sceneTalk = this;
			int num2;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				sceneTalk.EsComment.transform.parent.gameObject.SetActive(false);
				sceneTalk.transform.Find("UICanvas/UI2/EhonGroup").gameObject.SetActive(true);
				sceneTalk.transform.Find("UICanvas/UI2/EhonGroup/" + sceneTalk.ScenarioType + "/Q0").gameObject.SetActive(true);
				sceneTalk.transform.Find("UICanvas/UI2/EhonGroup/診断2-1").gameObject.SetActive(false);
				sceneTalk.transform.Find("UICanvas/UI2/EhonGroup/診断2-2").gameObject.SetActive(false);
				sceneTalk.transform.Find("UICanvas/UI2/EhonGroup/診断2-3").gameObject.SetActive(false);
				sceneTalk.transform.Find("UICanvas/UI2/EhonGroup/診断2-4").gameObject.SetActive(false);
				sceneTalk.transform.Find("UICanvas/UI2/EhonGroup/診断2-5").gameObject.SetActive(false);
				sceneTalk.transform.Find("UICanvas/UI2/EhonGroup/" + sceneTalk.ScenarioType + "/Ending1").gameObject.SetActive(false);
				sceneTalk.transform.Find("UICanvas/UI2/EhonGroup/" + sceneTalk.ScenarioType + "/Ending2").gameObject.SetActive(false);
				sceneTalk.transform.Find("UICanvas/UI2/EhonGroup/" + sceneTalk.ScenarioType + "/Ending3").gameObject.SetActive(false);
				sceneTalk.transform.Find("UICanvas/UI2/EhonGroup/" + sceneTalk.ScenarioType).gameObject.SetActive(true);
				sceneTalk.transform.Find("UICanvas/UI2/EhonGroup/EsTalk").gameObject.SetActive(false);
				sceneTalk.transform.Find("UICanvas/UI2/EhonGroup/" + sceneTalk.ScenarioType + "/Background").gameObject.SetActive(true);
				sceneTalk.transform.Find("UICanvas/UI2/EhonGroup/Background").GetComponent<Image>().color = Color.white;
				sceneTalk.StartCoroutine(AppUtil.FadeIn(sceneTalk.transform.Find("UICanvas/UI2/EhonGroup"), 0.5f, null));
				sceneTalk.EsComment = sceneTalk.transform.Find("UICanvas/UI2/EhonGroup/EsTalk").GetComponentInChildren<TextLocalization>(true);
				this.<>2__current = sceneTalk.EsTalk("診断導入", true);
				this.<>1__state = 1;
				return true;
			case 1:
				this.<>1__state = -1;
				if (Data.GetScenarioSpecific(sceneTalk.ScenarioNo).Contains("ルート分岐"))
				{
					CS$<>8__locals1 = new SceneTalk.<>c__DisplayClass34_0();
					CS$<>8__locals1.Q01 = sceneTalk.transform.Find("UICanvas/UI2/EhonGroup/" + sceneTalk.ScenarioType + "/Q0").gameObject;
					sceneTalk.StartCoroutine(AppUtil.FadeOut(CS$<>8__locals1.Q01, 0.5f, new Action<Object>(CS$<>8__locals1.<StartCounselingType2>b__0)));
					GameObject gameObject = sceneTalk.transform.Find("UICanvas/UI2/EhonGroup/" + sceneTalk.ScenarioType + "/Q1").gameObject;
					gameObject.SetActive(true);
					sceneTalk.StartCoroutine(AppUtil.FadeIn(gameObject, 0.5f, null));
					this.<>2__current = sceneTalk.EsTalk("診断導入2", false);
					this.<>1__state = 2;
					return true;
				}
				break;
			case 2:
				this.<>1__state = -1;
				AppUtil.SetAlpha(CS$<>8__locals1.Q01, 1f);
				CS$<>8__locals1 = null;
				break;
			case 3:
				this.<>1__state = -1;
				num2 = i;
				i = num2 + 1;
				goto IL_43A;
			default:
				return false;
			}
			for (max = 1; max <= 20; max = num2 + 1)
			{
				if (Data.GetScenarioKey(sceneTalk.ScenarioType, max + "問", null, 0) == null)
				{
					num2 = max;
					max = num2 - 1;
					break;
				}
				num2 = max;
			}
			choice = sceneTalk.GetComponentInChildren<ChoiceGroup>(true);
			choice.gameObject.SetActive(true);
			i = 1;
			IL_43A:
			if (i > max)
			{
				choice.gameObject.SetActive(false);
				return false;
			}
			this.<>2__current = sceneTalk.StartDialogueQuestion(i, max);
			this.<>1__state = 3;
			return true;
		}

		// Token: 0x17000104 RID: 260
		// (get) Token: 0x06000898 RID: 2200 RVA: 0x0002B89F File Offset: 0x00029A9F
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x06000899 RID: 2201 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000105 RID: 261
		// (get) Token: 0x0600089A RID: 2202 RVA: 0x0002B89F File Offset: 0x00029A9F
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0400038C RID: 908
		private int <>1__state;

		// Token: 0x0400038D RID: 909
		private object <>2__current;

		// Token: 0x0400038E RID: 910
		public SceneTalk <>4__this;

		// Token: 0x0400038F RID: 911
		private SceneTalk.<>c__DisplayClass34_0 <>8__1;

		// Token: 0x04000390 RID: 912
		private int <max>5__2;

		// Token: 0x04000391 RID: 913
		private ChoiceGroup <choice>5__3;

		// Token: 0x04000392 RID: 914
		private int <i>5__4;
	}

	// Token: 0x020000F3 RID: 243
	[CompilerGenerated]
	private sealed class <StartCounselingType4>d__35 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x0600089B RID: 2203 RVA: 0x0002B8A7 File Offset: 0x00029AA7
		[DebuggerHidden]
		public <StartCounselingType4>d__35(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x0600089C RID: 2204 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x0600089D RID: 2205 RVA: 0x0002B8B8 File Offset: 0x00029AB8
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			SceneTalk sceneTalk = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				break;
			case 1:
				this.<>1__state = -1;
				goto IL_74;
			case 2:
				this.<>1__state = -1;
				choice = sceneTalk.GetComponentInChildren<ChoiceGroup>(true);
				choice.gameObject.SetActive(true);
				this.<>2__current = choice.SetButton(new string[]
				{
					"YesButton",
					"NoButton"
				}, new string[]
				{
					"[UI]YesButton/Text",
					"[UI]NoButton/Text"
				}, false);
				this.<>1__state = 3;
				return true;
			case 3:
				this.<>1__state = -1;
				sceneTalk.WaitingChoice = true;
				goto IL_17E;
			case 4:
				this.<>1__state = -1;
				goto IL_17E;
			case 5:
				this.<>1__state = -1;
				choice.gameObject.SetActive(false);
				sceneTalk.CounselingData = data;
				if (!sceneTalk.transform.Find("UICanvas/UI2/ChoiceGroup/YesButton").gameObject.activeSelf)
				{
					this.<>2__current = sceneTalk.EsTalk("やり直し", true);
					this.<>1__state = 6;
					return true;
				}
				return false;
			case 6:
				this.<>1__state = -1;
				sceneTalk.transform.Find("BGCanvas/Es横向き/Background/MoveEye").gameObject.SetActive(false);
				data = null;
				choice = null;
				break;
			default:
				return false;
			}
			sceneTalk.transform.Find("UICanvas/UI2/InputGroup").gameObject.SetActive(true);
			sceneTalk.WaitingChoice = true;
			IL_74:
			if (sceneTalk.WaitingChoice)
			{
				this.<>2__current = null;
				this.<>1__state = 1;
				return true;
			}
			sceneTalk.transform.Find("UICanvas/UI2/InputGroup").gameObject.SetActive(false);
			sceneTalk.transform.Find("BGCanvas/Es横向き/Background/MoveEye").gameObject.SetActive(true);
			if (sceneTalk.CounselingData.Length <= 10)
			{
				data = sceneTalk.CounselingData;
				this.<>2__current = sceneTalk.EsTalk("10文字以内確認", true);
				this.<>1__state = 2;
				return true;
			}
			return false;
			IL_17E:
			if (!sceneTalk.WaitingChoice)
			{
				this.<>2__current = AppUtil.Wait(0.5f);
				this.<>1__state = 5;
				return true;
			}
			this.<>2__current = null;
			this.<>1__state = 4;
			return true;
		}

		// Token: 0x17000106 RID: 262
		// (get) Token: 0x0600089E RID: 2206 RVA: 0x0002BAF5 File Offset: 0x00029CF5
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0600089F RID: 2207 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000107 RID: 263
		// (get) Token: 0x060008A0 RID: 2208 RVA: 0x0002BAF5 File Offset: 0x00029CF5
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x04000393 RID: 915
		private int <>1__state;

		// Token: 0x04000394 RID: 916
		private object <>2__current;

		// Token: 0x04000395 RID: 917
		public SceneTalk <>4__this;

		// Token: 0x04000396 RID: 918
		private string <data>5__2;

		// Token: 0x04000397 RID: 919
		private ChoiceGroup <choice>5__3;
	}

	// Token: 0x020000F4 RID: 244
	[CompilerGenerated]
	private sealed class <>c__DisplayClass37_0
	{
		// Token: 0x060008A1 RID: 2209 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass37_0()
		{
		}

		// Token: 0x060008A2 RID: 2210 RVA: 0x0002BAFD File Offset: 0x00029CFD
		internal void <StartCounselingType3>b__0()
		{
			this.group.SetActive(true);
		}

		// Token: 0x060008A3 RID: 2211 RVA: 0x0002BB0B File Offset: 0x00029D0B
		internal int <StartCounselingType3>b__2(TextLocalization x)
		{
			return this.<>4__this.rnd.Next();
		}

		// Token: 0x060008A4 RID: 2212 RVA: 0x0002BB1D File Offset: 0x00029D1D
		internal void <StartCounselingType3>b__1()
		{
			this.group.SetActive(false);
		}

		// Token: 0x04000398 RID: 920
		public GameObject group;

		// Token: 0x04000399 RID: 921
		public SceneTalk <>4__this;

		// Token: 0x0400039A RID: 922
		public Func<TextLocalization, int> <>9__2;
	}

	// Token: 0x020000F5 RID: 245
	[CompilerGenerated]
	private sealed class <StartCounselingType3>d__37 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060008A5 RID: 2213 RVA: 0x0002BB2B File Offset: 0x00029D2B
		[DebuggerHidden]
		public <StartCounselingType3>d__37(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060008A6 RID: 2214 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060008A7 RID: 2215 RVA: 0x0002BB3C File Offset: 0x00029D3C
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			SceneTalk sceneTalk = this;
			TextLocalization[] array;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				CS$<>8__locals1 = new SceneTalk.<>c__DisplayClass37_0();
				CS$<>8__locals1.<>4__this = this;
				fukidasiMax = 20;
				CS$<>8__locals1.group = sceneTalk.transform.Find("UICanvas/WordGroup").gameObject;
				fukidasiList = CS$<>8__locals1.group.transform.Find("WordItemGroup").GetComponentsInChildren<TextLocalization>();
				array = fukidasiList;
				for (int j = 0; j < array.Length; j++)
				{
					array[j].transform.parent.gameObject.SetActive(false);
				}
				CS$<>8__locals1.group.transform.Find("EsMessageFrame/EsMessage").GetComponent<TextLocalization>().SetText(LanguageManager.Get(Data.GetScenarioKey(sceneTalk.ScenarioType, "診断中説明", null, 0)));
				sceneTalk.SetProgressState(sceneTalk.transform.Find("UICanvas/WordGroup"), 0f, (float)fukidasiMax);
				SceneTransition.Transit(new Action(CS$<>8__locals1.<StartCounselingType3>b__0), null);
				break;
			case 1:
				this.<>1__state = -1;
				break;
			case 2:
				this.<>1__state = -1;
				fukidasiKeyList = Counseling.GetFukidasiKeyList(sceneTalk.ScenarioType);
				i = 1;
				goto IL_463;
			case 3:
				this.<>1__state = -1;
				global::Debug.Log("AutoTestEvent:Screenshot");
				goto IL_383;
			case 4:
				this.<>1__state = -1;
				goto IL_383;
			case 5:
				this.<>1__state = -1;
				foreach (TextLocalization textLocalization in fukidasiList)
				{
					sceneTalk.StartCoroutine(AppUtil.FadeOut(textLocalization.transform.parent, 0.3f, null));
				}
				this.<>2__current = AppUtil.Wait(0.1f);
				this.<>1__state = 6;
				return true;
			case 6:
			{
				this.<>1__state = -1;
				if (i == 10)
				{
					fukidasiKeyList = Counseling.CustomFukidasiKeyList(fukidasiKeyList, sceneTalk.CounselingData);
					sceneTalk.CounselingData = "";
				}
				int j = i;
				i = j + 1;
				goto IL_463;
			}
			case 7:
				this.<>1__state = -1;
				goto IL_4DA;
			default:
				return false;
			}
			if (CS$<>8__locals1.group.activeSelf)
			{
				this.<>2__current = AppUtil.Wait(1.5f);
				this.<>1__state = 2;
				return true;
			}
			this.<>2__current = null;
			this.<>1__state = 1;
			return true;
			IL_383:
			if (!sceneTalk.WaitingChoice)
			{
				sceneTalk.SetProgressState(sceneTalk.transform.Find("UICanvas/WordGroup"), (float)i, (float)fukidasiMax);
				this.<>2__current = AppUtil.Wait(0.25f);
				this.<>1__state = 5;
				return true;
			}
			this.<>2__current = null;
			this.<>1__state = 4;
			return true;
			IL_463:
			if (i <= fukidasiMax)
			{
				sceneTalk.WaitingChoice = true;
				List<string> fukidasiKey = Counseling.GetFukidasiKey(fukidasiKeyList);
				IEnumerable<TextLocalization> source = new List<TextLocalization>(fukidasiList);
				Func<TextLocalization, int> keySelector;
				if ((keySelector = CS$<>8__locals1.<>9__2) == null)
				{
					keySelector = (CS$<>8__locals1.<>9__2 = new Func<TextLocalization, int>(CS$<>8__locals1.<StartCounselingType3>b__2));
				}
				foreach (TextLocalization textLocalization2 in source.OrderBy(keySelector))
				{
					textLocalization2.transform.parent.GetComponent<Image>().sprite = sceneTalk.PaperSprite[sceneTalk.rnd.Next(0, sceneTalk.PaperSprite.Length - 2)];
					textLocalization2.transform.parent.GetComponent<Image>().SetNativeSize();
					sceneTalk.StartCoroutine(AppUtil.FadeIn(textLocalization2, 0.3f, null));
					sceneTalk.StartCoroutine(AppUtil.FadeIn(textLocalization2.transform.parent, 0.3f, null));
					string[] array2 = AppUtil.Shift<string>(fukidasiKey).Split(new char[]
					{
						':'
					});
					textLocalization2.GetComponent<Text>().enabled = true;
					textLocalization2.transform.parent.localRotation = Quaternion.identity;
					textLocalization2.transform.parent.GetComponent<Button>().enabled = true;
					textLocalization2.transform.parent.name = array2[0];
					textLocalization2.SetText(array2[1]);
					textLocalization2.transform.parent.gameObject.SetActive(true);
				}
				this.<>2__current = AppUtil.Wait(0.3f);
				this.<>1__state = 3;
				return true;
			}
			array = fukidasiList;
			for (int j = 0; j < array.Length; j++)
			{
				array[j].transform.parent.gameObject.SetActive(true);
			}
			SceneTransition.Transit(new Action(CS$<>8__locals1.<StartCounselingType3>b__1), null);
			IL_4DA:
			if (CS$<>8__locals1.group.activeSelf)
			{
				return false;
			}
			this.<>2__current = null;
			this.<>1__state = 7;
			return true;
		}

		// Token: 0x17000108 RID: 264
		// (get) Token: 0x060008A8 RID: 2216 RVA: 0x0002C054 File Offset: 0x0002A254
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060008A9 RID: 2217 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000109 RID: 265
		// (get) Token: 0x060008AA RID: 2218 RVA: 0x0002C054 File Offset: 0x0002A254
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x0400039B RID: 923
		private int <>1__state;

		// Token: 0x0400039C RID: 924
		private object <>2__current;

		// Token: 0x0400039D RID: 925
		public SceneTalk <>4__this;

		// Token: 0x0400039E RID: 926
		private SceneTalk.<>c__DisplayClass37_0 <>8__1;

		// Token: 0x0400039F RID: 927
		private int <fukidasiMax>5__2;

		// Token: 0x040003A0 RID: 928
		private TextLocalization[] <fukidasiList>5__3;

		// Token: 0x040003A1 RID: 929
		private List<List<string>> <fukidasiKeyList>5__4;

		// Token: 0x040003A2 RID: 930
		private int <i>5__5;
	}

	// Token: 0x020000F6 RID: 246
	[CompilerGenerated]
	private sealed class <>c__DisplayClass38_0
	{
		// Token: 0x060008AB RID: 2219 RVA: 0x000043CE File Offset: 0x000025CE
		public <>c__DisplayClass38_0()
		{
		}

		// Token: 0x060008AC RID: 2220 RVA: 0x0002C05C File Offset: 0x0002A25C
		internal void <SetProgressState>b__0(float tmp)
		{
			this.progressSet.Find("Bar/Current").GetComponent<Image>().fillAmount = tmp;
		}

		// Token: 0x060008AD RID: 2221 RVA: 0x0002C079 File Offset: 0x0002A279
		internal void <SetProgressState>b__1(float tmp)
		{
			this.progressSet.Find("Bar/Icon").GetComponent<RectTransform>().anchoredPosition = new Vector2(tmp, 0f);
		}

		// Token: 0x040003A3 RID: 931
		public Transform progressSet;
	}

	// Token: 0x020000F7 RID: 247
	[CompilerGenerated]
	private sealed class <StartDialogue>d__39 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060008AE RID: 2222 RVA: 0x0002C0A0 File Offset: 0x0002A2A0
		[DebuggerHidden]
		public <StartDialogue>d__39(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060008AF RID: 2223 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060008B0 RID: 2224 RVA: 0x0002C0B0 File Offset: 0x0002A2B0
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			SceneTalk sceneTalk = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				commentFrame = sceneTalk.EsComment.transform.parent.gameObject;
				touch = GameObject.Find("MessageScreen").GetComponent<Image>();
				scenarioType = sceneTalk.ScenarioType;
				i = 1;
				goto IL_2FE;
			case 1:
				this.<>1__state = -1;
				sceneTalk.EsComment.SetKey(key);
				commentFrame.SetActive(true);
				touch.raycastTarget = true;
				break;
			case 2:
				this.<>1__state = -1;
				break;
			case 3:
				this.<>1__state = -1;
				goto IL_2C6;
			default:
				return false;
			}
			if (touch.raycastTarget)
			{
				this.<>2__current = null;
				this.<>1__state = 2;
				return true;
			}
			key = null;
			IL_2C6:
			if (scenarioType != sceneTalk.ScenarioType)
			{
				i = 0;
				scenarioType = sceneTalk.ScenarioType;
			}
			int num2 = i;
			i = num2 + 1;
			IL_2FE:
			if (i <= 100)
			{
				string scenarioKey = Data.GetScenarioKey(sceneTalk.ScenarioType, i.ToString(), null, 0);
				string scenarioKey2 = Data.GetScenarioKey(sceneTalk.ScenarioType, i.ToString() + "問", null, 0);
				string scenarioKey3 = Data.GetScenarioKey(sceneTalk.ScenarioType, i.ToString() + "分岐", "Law", 0);
				string scenarioKey4 = Data.GetScenarioKey(sceneTalk.ScenarioType, i.ToString() + "分岐", "SE", 0);
				string scenarioKey5 = Data.GetScenarioKey(sceneTalk.ScenarioType, i.ToString() + "変化-Law", null, 0);
				string scenarioKey6 = Data.GetScenarioKey(sceneTalk.ScenarioType, i.ToString() + "変化-立ち絵", null, 0);
				if (scenarioKey != null || scenarioKey3 != null || scenarioKey4 != null)
				{
					key = scenarioKey;
					if (scenarioKey3 != null)
					{
						key = Data.GetScenarioKey(sceneTalk.ScenarioType, i.ToString() + "分岐", PlayerStatus.LCTypeTrend, 0);
					}
					if (scenarioKey4 != null)
					{
						string routeTrend = PlayerStatus.RouteTrend;
						key = Data.GetScenarioKey(sceneTalk.ScenarioType, i.ToString() + "分岐", routeTrend, 0);
					}
					if (scenarioKey5 != null)
					{
						string a = LanguageManager.Get(scenarioKey5);
						if (!(a == "<狂気>"))
						{
							if (a == "<ほほえみ>")
							{
								sceneTalk.SetEsFace("Smile");
							}
						}
						else
						{
							sceneTalk.StartCoroutine(sceneTalk.SetEsCrazy());
						}
					}
					if (scenarioKey6 != null)
					{
						string esFace = LanguageManager.Get(scenarioKey6);
						sceneTalk.SetEsFace(esFace);
					}
					commentFrame.SetActive(false);
					this.<>2__current = AppUtil.Wait(0.2f);
					this.<>1__state = 1;
					return true;
				}
				if (scenarioKey2 != null)
				{
					this.<>2__current = sceneTalk.StartDialogueQuestion(i, 0);
					this.<>1__state = 3;
					return true;
				}
			}
			return false;
		}

		// Token: 0x1700010A RID: 266
		// (get) Token: 0x060008B1 RID: 2225 RVA: 0x0002C3C9 File Offset: 0x0002A5C9
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060008B2 RID: 2226 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x1700010B RID: 267
		// (get) Token: 0x060008B3 RID: 2227 RVA: 0x0002C3C9 File Offset: 0x0002A5C9
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040003A4 RID: 932
		private int <>1__state;

		// Token: 0x040003A5 RID: 933
		private object <>2__current;

		// Token: 0x040003A6 RID: 934
		public SceneTalk <>4__this;

		// Token: 0x040003A7 RID: 935
		private GameObject <commentFrame>5__2;

		// Token: 0x040003A8 RID: 936
		private Image <touch>5__3;

		// Token: 0x040003A9 RID: 937
		private string <scenarioType>5__4;

		// Token: 0x040003AA RID: 938
		private int <i>5__5;

		// Token: 0x040003AB RID: 939
		private string <key>5__6;
	}

	// Token: 0x020000F8 RID: 248
	[CompilerGenerated]
	private sealed class <StartDialogueQuestion>d__40 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060008B4 RID: 2228 RVA: 0x0002C3D1 File Offset: 0x0002A5D1
		[DebuggerHidden]
		public <StartDialogueQuestion>d__40(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060008B5 RID: 2229 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060008B6 RID: 2230 RVA: 0x0002C3E0 File Offset: 0x0002A5E0
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			SceneTalk sceneTalk = this;
			switch (num)
			{
			case 0:
				this.<>1__state = -1;
				commentFrame = sceneTalk.EsComment.transform.parent.gameObject;
				commentFrame.SetActive(false);
				if (sceneTalk.ScenarioType.Contains("診断"))
				{
					sceneTalk.SetProgressState(sceneTalk.transform.Find("UICanvas/UI2/ChoiceGroup"), (float)(no - 1), (float)max);
					for (int i = 0; i <= max; i++)
					{
						sceneTalk.transform.Find(string.Concat(new object[]
						{
							"UICanvas/UI2/EhonGroup/",
							sceneTalk.ScenarioType,
							"/Q",
							i
						})).gameObject.SetActive(i == no);
					}
				}
				if (sceneTalk.ScenarioType == "診断2-3" || sceneTalk.ScenarioType == "診断2-4")
				{
					sceneTalk.transform.Find("UICanvas/FukidasiGroup/" + sceneTalk.ScenarioType).gameObject.SetActive(true);
				}
				type = sceneTalk.ScenarioType;
				if (sceneTalk.ScenarioNo == "本を借りる")
				{
					sceneTalk.ScenarioType = "本を借りる1";
					no = 9;
				}
				this.<>2__current = AppUtil.Wait(0.2f);
				this.<>1__state = 1;
				return true;
			case 1:
			{
				this.<>1__state = -1;
				string scenarioKey = Data.GetScenarioKey(sceneTalk.ScenarioType, no + "問", null, 0);
				sceneTalk.EsComment.SetKey(scenarioKey);
				commentFrame.SetActive(true);
				choice = sceneTalk.GetComponentInChildren<ChoiceGroup>(true);
				choice.gameObject.SetActive(true);
				string scenarioNo = sceneTalk.ScenarioNo;
				string[] array;
				if (!(scenarioNo == "日替わり"))
				{
					if (!(scenarioNo == "本を借りる"))
					{
						if (!(scenarioNo == "本を返す"))
						{
							array = new string[]
							{
								"Law",
								"Chaos",
								"Neutral"
							};
						}
						else
						{
							array = new string[]
							{
								"1",
								"2",
								"3"
							};
						}
					}
					else
					{
						int[] array2 = AppUtil.RandomArray(1, 10);
						int length = PlayerStatus.ReadingBookList.Length;
						int num2 = Mathf.Min(3, length);
						string text = PlayerStatus.ReadingBookList.Substring(length - num2, num2);
						List<string> list = new List<string>();
						foreach (int num3 in array2)
						{
							string value = num3.ToString("X");
							if (!text.Contains(value))
							{
								list.Add(num3.ToString());
							}
							if (list.Count >= 3)
							{
								break;
							}
						}
						array = list.ToArray();
					}
				}
				else
				{
					array = new string[]
					{
						"Perfect",
						"Good",
						"Normal"
					};
				}
				if (sceneTalk.ScenarioType == "本を返す共通")
				{
					array = new string[]
					{
						"既読",
						"未読",
						""
					};
				}
				choiceList = new string[]
				{
					"Choice" + array[0],
					"Choice" + array[1],
					"Choice" + array[2]
				};
				choiceKeyList = new string[]
				{
					Data.GetScenarioKey(sceneTalk.ScenarioType, no + "選択肢", array[0], 0),
					Data.GetScenarioKey(sceneTalk.ScenarioType, no + "選択肢", array[1], 0),
					Data.GetScenarioKey(sceneTalk.ScenarioType, no + "選択肢", array[2], 0)
				};
				restartChoice = Data.GetScenarioSpecific(sceneTalk.ScenarioNo).Contains("退行");
				bool isRandom = !restartChoice && sceneTalk.ScenarioType != "本を返す共通";
				this.<>2__current = choice.SetButton(choiceList, choiceKeyList, isRandom);
				this.<>1__state = 2;
				return true;
			}
			case 2:
				this.<>1__state = -1;
				sceneTalk.WaitingChoice = true;
				break;
			case 3:
				this.<>1__state = -1;
				break;
			case 4:
			{
				this.<>1__state = -1;
				choosenItem = "";
				int num4 = 0;
				for (int k = 0; k < choiceList.Length; k++)
				{
					if (sceneTalk.transform.Find("UICanvas/UI2/ChoiceGroup/" + choiceList[k]).gameObject.activeSelf)
					{
						choosenItem = choiceList[k].Replace("Choice", "");
						num4 = k;
						break;
					}
				}
				global::Debug.Log("choosenItem=" + choosenItem);
				if (!sceneTalk.ScenarioNo.Contains("4章"))
				{
					UserChoice.Set(sceneTalk.ScenarioType, choosenItem);
				}
				if (restartChoice)
				{
					if (choosenItem == "Law")
					{
						sceneTalk.ScenarioNo = "退行";
					}
					else
					{
						sceneTalk.ScenarioNo = "退行中止";
					}
				}
				commentFrame.SetActive(false);
				if (choiceKeyList[0].Contains("診断"))
				{
					string scenarioKey2 = Data.GetScenarioKey(sceneTalk.ScenarioType, no + "解析中表示単語", choosenItem, 0);
					if (scenarioKey2 != null)
					{
						sceneTalk.WordListForDisplay.AddRange(LanguageManager.Get(scenarioKey2).Split(new char[]
						{
							'、'
						}));
					}
					choice.DismissButton();
					sceneTalk.SetProgressState(sceneTalk.transform.Find("UICanvas/UI2/ChoiceGroup"), (float)no, (float)max);
					if (no == max)
					{
						sceneTalk.transform.Find(string.Concat(new object[]
						{
							"UICanvas/UI2/EhonGroup/",
							sceneTalk.ScenarioType,
							"/Q",
							no
						})).gameObject.SetActive(false);
						Transform transform = sceneTalk.transform.Find(string.Concat(new object[]
						{
							"UICanvas/UI2/EhonGroup/",
							sceneTalk.ScenarioType,
							"/Ending",
							num4 + 1
						}));
						transform.gameObject.SetActive(true);
						sceneTalk.StartCoroutine(AppUtil.FadeIn(transform, 0.5f, null));
						if (sceneTalk.ScenarioType == "診断2-5")
						{
							if (num4 == 0)
							{
								sceneTalk.ScenarioType = "診断2-4";
								if (PlayerStatus.ScenarioNo.Contains("3章"))
								{
									PlayerStatus.Route = "SE";
								}
							}
							else if (num4 == 1)
							{
								sceneTalk.ScenarioType = "診断2-3";
								if (PlayerStatus.ScenarioNo.Contains("3章"))
								{
									PlayerStatus.Route = "ID";
								}
							}
						}
						if (sceneTalk.ScenarioType == "診断2-3")
						{
							sceneTalk.transform.Find("UICanvas/UI2/EhonGroup/Background").GetComponent<Image>().color = Color.black;
							sceneTalk.transform.Find("UICanvas/UI2/EhonGroup/診断2-5/Background").gameObject.SetActive(false);
							sceneTalk.transform.Find("UICanvas/UI2/EhonGroup/" + sceneTalk.ScenarioType + "/Background").gameObject.SetActive(false);
							sceneTalk.transform.Find("UICanvas/FukidasiGroup/" + sceneTalk.ScenarioType).gameObject.SetActive(false);
						}
						if (sceneTalk.ScenarioType == "診断2-4")
						{
							sceneTalk.transform.Find("UICanvas/FukidasiGroup/" + sceneTalk.ScenarioType).gameObject.SetActive(false);
						}
					}
				}
				else
				{
					choice.gameObject.SetActive(false);
				}
				this.<>2__current = AppUtil.Wait(0.5f);
				this.<>1__state = 5;
				return true;
			}
			case 5:
				this.<>1__state = -1;
				this.<>2__current = sceneTalk.EsTalk(no + "回答-" + choosenItem, false);
				this.<>1__state = 6;
				return true;
			case 6:
				this.<>1__state = -1;
				if (sceneTalk.ScenarioNo == "本を借りる")
				{
					PlayerStatus.ReadingBook = int.Parse(choosenItem);
					sceneTalk.ScenarioType = type;
				}
				if (sceneTalk.ScenarioType == "本を返す共通")
				{
					int readingBook = PlayerStatus.ReadingBook;
					sceneTalk.ScenarioType = string.Concat(new object[]
					{
						"本を返す",
						readingBook,
						"-",
						choosenItem
					});
					if (Data.GetScenarioKey(sceneTalk.ScenarioType, "1", null, 0) == null)
					{
						sceneTalk.ScenarioType = string.Concat(new object[]
						{
							"本を返す",
							7,
							"-",
							choosenItem
						});
					}
				}
				return false;
			default:
				return false;
			}
			if (!sceneTalk.WaitingChoice)
			{
				this.<>2__current = AppUtil.Wait(0.5f);
				this.<>1__state = 4;
				return true;
			}
			this.<>2__current = null;
			this.<>1__state = 3;
			return true;
		}

		// Token: 0x1700010C RID: 268
		// (get) Token: 0x060008B7 RID: 2231 RVA: 0x0002CD52 File Offset: 0x0002AF52
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060008B8 RID: 2232 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x1700010D RID: 269
		// (get) Token: 0x060008B9 RID: 2233 RVA: 0x0002CD52 File Offset: 0x0002AF52
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040003AC RID: 940
		private int <>1__state;

		// Token: 0x040003AD RID: 941
		private object <>2__current;

		// Token: 0x040003AE RID: 942
		public SceneTalk <>4__this;

		// Token: 0x040003AF RID: 943
		public int no;

		// Token: 0x040003B0 RID: 944
		public int max;

		// Token: 0x040003B1 RID: 945
		private GameObject <commentFrame>5__2;

		// Token: 0x040003B2 RID: 946
		private string <type>5__3;

		// Token: 0x040003B3 RID: 947
		private ChoiceGroup <choice>5__4;

		// Token: 0x040003B4 RID: 948
		private string[] <choiceList>5__5;

		// Token: 0x040003B5 RID: 949
		private string[] <choiceKeyList>5__6;

		// Token: 0x040003B6 RID: 950
		private bool <restartChoice>5__7;

		// Token: 0x040003B7 RID: 951
		private string <choosenItem>5__8;
	}
}
