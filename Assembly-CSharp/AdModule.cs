﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using App;
using GoogleMobileAds.Api;
using UnityEngine;

// Token: 0x02000040 RID: 64
public class AdModule
{
	// Token: 0x17000023 RID: 35
	// (get) Token: 0x060002B3 RID: 691 RVA: 0x00014A0C File Offset: 0x00012C0C
	private static float Density
	{
		get
		{
			if (AdModule._Density == 0f)
			{
				BannerView bannerView = new BannerView("", AdSize.Banner, AdPosition.Center);
				AdModule._Density = bannerView.GetHeightInPixels() / 50f;
				global::Debug.Log(string.Concat(new object[]
				{
					"AdModule:_Density=",
					AdModule._Density,
					"\nScreenSize=",
					AdModule.GetScreenSize().ToString()
				}));
				bannerView.Destroy();
			}
			return AdModule._Density;
		}
	}

	// Token: 0x14000001 RID: 1
	// (add) Token: 0x060002B4 RID: 692 RVA: 0x00014A98 File Offset: 0x00012C98
	// (remove) Token: 0x060002B5 RID: 693 RVA: 0x00014AD0 File Offset: 0x00012CD0
	public event EventHandler<AdModule.AdEventArgs> OnAdEvent
	{
		[CompilerGenerated]
		add
		{
			EventHandler<AdModule.AdEventArgs> eventHandler = this.OnAdEvent;
			EventHandler<AdModule.AdEventArgs> eventHandler2;
			do
			{
				eventHandler2 = eventHandler;
				EventHandler<AdModule.AdEventArgs> value2 = (EventHandler<AdModule.AdEventArgs>)Delegate.Combine(eventHandler2, value);
				eventHandler = Interlocked.CompareExchange<EventHandler<AdModule.AdEventArgs>>(ref this.OnAdEvent, value2, eventHandler2);
			}
			while (eventHandler != eventHandler2);
		}
		[CompilerGenerated]
		remove
		{
			EventHandler<AdModule.AdEventArgs> eventHandler = this.OnAdEvent;
			EventHandler<AdModule.AdEventArgs> eventHandler2;
			do
			{
				eventHandler2 = eventHandler;
				EventHandler<AdModule.AdEventArgs> value2 = (EventHandler<AdModule.AdEventArgs>)Delegate.Remove(eventHandler2, value);
				eventHandler = Interlocked.CompareExchange<EventHandler<AdModule.AdEventArgs>>(ref this.OnAdEvent, value2, eventHandler2);
			}
			while (eventHandler != eventHandler2);
		}
	}

	// Token: 0x060002B6 RID: 694 RVA: 0x00014B05 File Offset: 0x00012D05
	public AdModule()
	{
	}

	// Token: 0x060002B7 RID: 695 RVA: 0x00014B20 File Offset: 0x00012D20
	public AdModule(AdManager mgr, string type, string[] args)
	{
		this.Type = type;
		if (!(type == "AdMobBanner"))
		{
			if (!(type == "AdMobRectangle"))
			{
				if (!(type == "AdMobReward"))
				{
					if (!(type == "AdfuriNative"))
					{
						if (type == "AdfuriReward")
						{
							GameObject gameObject = new GameObject("AdfurikunMovieRewardUtility");
							gameObject.transform.SetParent(mgr.transform);
							this.AdfuriReward = gameObject.AddComponent<AdfurikunMovieRewardUtility>();
							this.AdfuriReward.config = new AdfurikunMovieRewardAdConfig();
							this.AdfuriReward.config.androidAppID = args[0];
							this.AdfuriReward.config.iPhoneAppID = args[0];
							this.AdfuriReward.initializeMovieReward();
							this.AdfuriReward.onPrepareSuccess = new Action<string>(this.OnLoadAdfuri);
							this.AdfuriReward.onStartPlaying = new Action<string, string>(this.OnStartAdfuriReward);
							this.AdfuriReward.onFinishPlaying = new Action<string, string>(this.OnFinishedAdfuriReward);
							this.AdfuriReward.onFailedPlaying = new Action<string, string>(this.OnPlayFailedAdfuriReward);
							this.AdfuriReward.onCloseAd = new Action<string, string>(this.OnClosedAdfuriReward);
						}
					}
					else
					{
						GameObject gameObject2 = new GameObject("AdfurikunMovieNativeAdViewUtility");
						gameObject2.transform.SetParent(mgr.transform);
						this.AdfuriNative = gameObject2.AddComponent<AdfurikunMovieNativeAdViewUtility>();
						this.AdfuriNative.config = new AdfurikunMovieNativeAdViewAdConfig();
						this.AdfuriNative.config.androidAppID = args[0];
						this.AdfuriNative.config.iPhoneAppID = args[0];
						this.AdfuriNative.onLoadFinish = new Action<string>(this.OnLoadAdfuri);
						this.AdfuriNative.onLoadError = new Action<string, string>(this.OnLoadFailedAdfuri);
						this.AdfuriNative.onPlayStart = new Action<string>(this.OnStartAdfuri);
						this.AdfuriNative.onPlayFinish = new Action<string, bool>(this.OnFinishedAdfuri);
						this.AdfuriNative.onPlayError = new Action<string, string>(this.OnPlayFailedAdfuri);
					}
				}
				else
				{
					MobileAds.Initialize(args[0]);
					this.UnitId = args[1];
					this.AdMobVideo = RewardBasedVideoAd.Instance;
					this.AdMobVideo.OnAdLoaded += this.OnLoadedAdMob;
					this.AdMobVideo.OnAdFailedToLoad += this.OnFailedAdMob;
					this.AdMobVideo.OnAdRewarded += this.OnRewardAdMob;
					this.AdMobVideo.OnAdClosed += this.OnClosedAdMob;
				}
			}
			else
			{
				MobileAds.Initialize(args[0]);
				this.UnitId = args[1];
				AdSize adSize = AdSize.MediumRectangle;
				Vector2 adjustPos = Vector2.zero;
				if (args.Length > 3)
				{
					adjustPos = AppUtil.Vector2(args[3], 1f);
				}
				Vector2Int positionCenter = this.GetPositionCenter(adSize, adjustPos);
				this.AdMobView = new BannerView(this.UnitId, adSize, positionCenter.x, positionCenter.y);
				if (args.Length > 2)
				{
					Vector2 canvas = AppUtil.Vector2(args[2], 1f);
					Vector2 vector = SafeAreaAdjuster.CanvasToPixel(canvas);
					if (this.AdMobView.GetWidthInPixels() > vector.x || this.AdMobView.GetHeightInPixels() > vector.y)
					{
						Vector2 vector2 = this.CanvasToPoint(canvas);
						adSize = new AdSize((int)vector2.x, (int)vector2.y);
						positionCenter = this.GetPositionCenter(adSize, adjustPos);
						this.AdMobView.Destroy();
						this.AdMobView = new BannerView(this.UnitId, adSize, positionCenter.x, positionCenter.y);
					}
				}
				this.AdMobView.OnAdLoaded += this.OnLoadedAdMob;
				this.AdMobView.OnAdFailedToLoad += this.OnFailedAdMob;
			}
		}
		else
		{
			MobileAds.Initialize(args[0]);
			this.UnitId = args[1];
			AdSize smartBanner = AdSize.SmartBanner;
			this.AdMobView = new BannerView(this.UnitId, smartBanner, AdPosition.Bottom);
			this.AdMobView.OnAdLoaded += this.OnLoadedAdMob;
			this.AdMobView.OnAdFailedToLoad += this.OnFailedAdMob;
			this.OnLoadedAdMob(null, null);
		}
		this.Load(false);
	}

	// Token: 0x060002B8 RID: 696 RVA: 0x00014F6C File Offset: 0x0001316C
	public void Load(bool waiting = false)
	{
		this.IsLoaded = false;
		this.IsWaiting = waiting;
		string type = this.Type;
		if (!(type == "AdMobBanner") && !(type == "AdMobRectangle"))
		{
			if (type == "AdMobReward")
			{
				AdRequest adMobRequest = this.GetAdMobRequest();
				this.AdMobVideo.LoadAd(adMobRequest, this.UnitId);
				return;
			}
			if (type == "SkipReward" || type == "FakeReward")
			{
				this.IsLoaded = true;
				return;
			}
			if (!(type == "AdfuriNative"))
			{
				return;
			}
			this.AdfuriNative.loadMovieNativeAdView();
		}
		else
		{
			AdRequest adMobRequest2 = this.GetAdMobRequest();
			this.AdMobView.LoadAd(adMobRequest2);
			global::Debug.Log(string.Concat(new object[]
			{
				"Load AdMobView pixelSize=",
				this.AdMobView.GetWidthInPixels(),
				",",
				this.AdMobView.GetHeightInPixels()
			}));
			if (!waiting)
			{
				this.AdMobView.Hide();
				return;
			}
		}
	}

	// Token: 0x060002B9 RID: 697 RVA: 0x00015080 File Offset: 0x00013280
	public void Hide()
	{
		string type = this.Type;
		if (!(type == "AdMobBanner"))
		{
			if (!(type == "AdMobRectangle"))
			{
				if (type == "AdfuriNative")
				{
					this.AdfuriNative.hideMovieNativeAdView();
					this.Load(false);
				}
			}
			else
			{
				this.AdMobView.Hide();
				this.Load(false);
			}
		}
		else
		{
			this.AdMobView.Hide();
		}
		this.IsWaiting = false;
		this.IsShowing = false;
	}

	// Token: 0x060002BA RID: 698 RVA: 0x00015100 File Offset: 0x00013300
	public virtual void Show()
	{
		this.IsRewarded = false;
		string type = this.Type;
		uint num = <PrivateImplementationDetails>.ComputeStringHash(type);
		if (num <= 1775578499U)
		{
			if (num != 1343160461U)
			{
				if (num != 1604465227U)
				{
					if (num == 1775578499U)
					{
						if (type == "FakeReward")
						{
							GameObject.Find("AppCommon").GetComponent<AdManager>().StartCoroutine(this.StartFakeAdMovie());
						}
					}
				}
				else if (type == "AdfuriReward")
				{
					this.AdfuriReward.playMovieReward();
				}
			}
			else if (type == "SkipReward")
			{
				this.OnEvent("OnFinished");
				this.OnEvent("OnClosed");
			}
		}
		else if (num <= 2767357316U)
		{
			if (num != 2666580215U)
			{
				if (num == 2767357316U)
				{
					if (type == "AdMobBanner")
					{
						if (this.IsLoaded)
						{
							this.AdMobView.Show();
						}
						else
						{
							this.RetryCount = 1;
							this.Load(true);
						}
					}
				}
			}
			else if (type == "AdfuriNative")
			{
				if (this.IsLoaded)
				{
					float height = Mathf.Min((float)Screen.width * 0.5625f, (float)Screen.height * 0.375f);
					this.AdfuriNative.setFitWidthFrame((float)Screen.height, height, 2);
					this.AdfuriNative.playMovieNativeAdView();
				}
				else
				{
					this.Load(true);
				}
			}
		}
		else if (num != 2942754799U)
		{
			if (num == 2987584937U)
			{
				if (type == "AdMobReward")
				{
					if (this.IsLoaded)
					{
						this.AdMobVideo.Show();
					}
				}
			}
		}
		else if (type == "AdMobRectangle")
		{
			this.AdMobView.Show();
		}
		this.IsShowing = true;
	}

	// Token: 0x060002BB RID: 699 RVA: 0x00015304 File Offset: 0x00013504
	public void Destroy()
	{
		string type = this.Type;
		if (type == "AdMobBanner")
		{
			this.AdMobView.Destroy();
			SafeAreaAdjuster.BottomBannerRect = default(Rect);
			return;
		}
		if (type == "AdMobRectangle")
		{
			this.AdMobView.Destroy();
			return;
		}
		if (type == "AdfuriNative")
		{
			this.AdfuriNative.disposeResource();
			Object.Destroy(this.AdfuriNative.gameObject);
			return;
		}
		if (!(type == "AdfuriReward"))
		{
			return;
		}
		this.AdfuriReward.disposeResource();
		Object.Destroy(this.AdfuriReward.gameObject);
	}

	// Token: 0x060002BC RID: 700 RVA: 0x000153AA File Offset: 0x000135AA
	public virtual bool IsReady()
	{
		if (this.Type == "AdfuriReward")
		{
			return this.AdfuriReward.isPreparedMovieReward();
		}
		bool isLoaded = this.IsLoaded;
		if (!isLoaded)
		{
			this.RetryCount = 5;
			this.Load(false);
		}
		return isLoaded;
	}

	// Token: 0x060002BD RID: 701 RVA: 0x000153E4 File Offset: 0x000135E4
	public Vector2 GetSize()
	{
		string type = this.Type;
		if (type == "AdMobBanner" || type == "AdMobRectangle")
		{
			return SafeAreaAdjuster.PixelToCanvas(this.PixelSize);
		}
		return Vector2.one * -1f;
	}

	// Token: 0x060002BE RID: 702 RVA: 0x00015430 File Offset: 0x00013630
	protected void OnEvent(string message)
	{
		global::Debug.Log("AdModule#OnEvent " + this.Type + " " + message);
		if (this.OnAdEvent == null)
		{
			return;
		}
		AdModule.AdEventArgs adEventArgs = new AdModule.AdEventArgs();
		adEventArgs.Type = this.Type;
		adEventArgs.Message = message;
		this.OnAdEvent(null, adEventArgs);
		if (!(message == "OnLoaded"))
		{
			if (message == "OnFinished")
			{
				this.IsRewarded = true;
				return;
			}
			if (!(message == "OnClosed"))
			{
				return;
			}
			AdModule.AdEventArgs adEventArgs2 = new AdModule.AdEventArgs();
			adEventArgs2.Type = this.Type;
			if (this.IsRewarded)
			{
				adEventArgs2.Message = "OnReward";
			}
			else
			{
				adEventArgs2.Message = "NoReward";
			}
			this.OnAdEvent(null, adEventArgs2);
		}
		else
		{
			this.IsLoaded = true;
			if (this.IsWaiting)
			{
				this.Show();
				return;
			}
		}
	}

	// Token: 0x060002BF RID: 703 RVA: 0x0001550C File Offset: 0x0001370C
	private IEnumerator StartFakeAdMovie()
	{
		DialogManager.ShowDialog("FakeMovieAdScreen", Array.Empty<object>());
		while (DialogManager.IsShowing())
		{
			yield return null;
		}
		this.OnEvent("OnFinished");
		this.OnEvent("OnClosed");
		yield break;
	}

	// Token: 0x060002C0 RID: 704 RVA: 0x0001551C File Offset: 0x0001371C
	private AdRequest GetAdMobRequest()
	{
		return new AdRequest.Builder().AddTestDevice("SIMULATOR").AddTestDevice("2B3B9715CF716A80B923B0A8FDEC4C6E").AddTestDevice("603FCAEF19A587D0DE46759DA4E29B05").AddTestDevice("65C19A2EEDACE8CFE707F06CE8ADCBB9").AddTestDevice("c279074adac7c39288a6b9cfb587d913").AddTestDevice("6423d13eae0bdae39e50249c3f682d87").AddTestDevice("DDA86E100D011A140E624641373F350D").Build();
	}

	// Token: 0x060002C1 RID: 705 RVA: 0x0001557C File Offset: 0x0001377C
	private Vector2Int GetPositionCenter(AdSize size, Vector2 adjustPos)
	{
		Vector2Int one = Vector2Int.one;
		one.x = this.PixelToPoint((AdModule.GetScreenSize().x - (float)this.PointToPixel((float)size.Width)) / 2f - (float)AdModule.CanvasToPixel(adjustPos.x, false));
		one.y = this.PixelToPoint((AdModule.GetScreenSize().y - (float)this.PointToPixel((float)size.Height)) / 2f - (float)AdModule.CanvasToPixel(adjustPos.y, true));
		float safeAreaTop = AdModule.GetSafeAreaTop();
		one.y -= this.PixelToPoint(safeAreaTop);
		return one;
	}

	// Token: 0x060002C2 RID: 706 RVA: 0x0001238F File Offset: 0x0001058F
	public static Vector2 GetScreenSize()
	{
		return new Vector2((float)Screen.width, (float)Screen.height);
	}

	// Token: 0x060002C3 RID: 707 RVA: 0x00015620 File Offset: 0x00013820
	public static float GetSafeAreaTop()
	{
		Rect safeArea = SafeAreaAdjuster.GetSafeArea();
		AdModule.GetScreenSize();
		return AdModule.GetScreenSize().y - safeArea.y - safeArea.height;
	}

	// Token: 0x060002C4 RID: 708 RVA: 0x00015654 File Offset: 0x00013854
	private static Vector2 GetPixelRate()
	{
		float num = 0.5625f;
		float x = AdModule.GetScreenSize().x;
		float y = AdModule.GetScreenSize().y;
		float num2 = x / y;
		Vector2 one = Vector2.one;
		if (num2 < num)
		{
			one.y = x / 1080f;
			one.x = x / 1080f;
		}
		else
		{
			one.x = y / 1920f;
			one.y = y / 1920f;
		}
		return one;
	}

	// Token: 0x060002C5 RID: 709 RVA: 0x000156C4 File Offset: 0x000138C4
	public static int CanvasToPixel(float canvas, bool height = true)
	{
		if (height)
		{
			return (int)(canvas * AdModule.GetPixelRate().y);
		}
		return (int)(canvas * AdModule.GetPixelRate().x);
	}

	// Token: 0x060002C6 RID: 710 RVA: 0x000156E4 File Offset: 0x000138E4
	private int PixelToPoint(float pixel)
	{
		return (int)(pixel / AdModule.Density);
	}

	// Token: 0x060002C7 RID: 711 RVA: 0x000156EE File Offset: 0x000138EE
	private int PointToPixel(float point)
	{
		return (int)(point * AdModule.Density);
	}

	// Token: 0x060002C8 RID: 712 RVA: 0x000156F8 File Offset: 0x000138F8
	private Vector2 PointToCanvas(Vector2 point)
	{
		return SafeAreaAdjuster.PixelToCanvas(point) * AdModule.Density;
	}

	// Token: 0x060002C9 RID: 713 RVA: 0x0001570A File Offset: 0x0001390A
	private Vector2 CanvasToPoint(Vector2 canvas)
	{
		return SafeAreaAdjuster.CanvasToPixel(canvas) / AdModule.Density;
	}

	// Token: 0x060002CA RID: 714 RVA: 0x0001571C File Offset: 0x0001391C
	public void OnLoadedAdMob(object sender, EventArgs args)
	{
		if (!AdInfo.ENABLE)
		{
			return;
		}
		this.OnEvent("OnLoaded");
		this.PixelSize.x = this.AdMobView.GetWidthInPixels();
		this.PixelSize.y = this.AdMobView.GetHeightInPixels();
		if (this.Type != "AdMobBanner")
		{
			return;
		}
		Rect bottomBannerRect = default(Rect);
		bottomBannerRect.width = this.GetSize().x;
		bottomBannerRect.height = this.GetSize().y;
		if (bottomBannerRect.height < 50f)
		{
			bottomBannerRect.height = 150f;
		}
		SafeAreaAdjuster.BottomBannerRect = bottomBannerRect;
	}

	// Token: 0x060002CB RID: 715 RVA: 0x000157C7 File Offset: 0x000139C7
	public void OnFailedAdMob(object sender, AdFailedToLoadEventArgs args)
	{
		this.OnEvent("OnFailed " + args.Message);
		if (this.RetryCount > 0)
		{
			this.Load(false);
			this.RetryCount--;
		}
	}

	// Token: 0x060002CC RID: 716 RVA: 0x000157FD File Offset: 0x000139FD
	public void OnRewardAdMob(object sender, Reward reward)
	{
		this.OnEvent("OnFinished");
	}

	// Token: 0x060002CD RID: 717 RVA: 0x0001580A File Offset: 0x00013A0A
	public void OnClosedAdMob(object sender, EventArgs args)
	{
		this.OnEvent("OnClosed");
		this.Load(false);
	}

	// Token: 0x060002CE RID: 718 RVA: 0x0001581E File Offset: 0x00013A1E
	public void OnLoadAdfuri(string appID)
	{
		global::Debug.Log("AdModule#OnLoadAdfuri");
		this.OnEvent("OnLoaded");
	}

	// Token: 0x060002CF RID: 719 RVA: 0x00015835 File Offset: 0x00013A35
	public void OnStartAdfuri(string appID)
	{
		global::Debug.Log("AdModule#OnStartAdfuri");
		this.OnEvent("OnStarted");
	}

	// Token: 0x060002D0 RID: 720 RVA: 0x0001584C File Offset: 0x00013A4C
	public void OnFinishedAdfuri(string appID, bool isVideo)
	{
		global::Debug.Log("AdModule#OnFinishedAdfuri");
		this.OnEvent("OnFinished " + (isVideo ? "Video" : "NoVideo"));
	}

	// Token: 0x060002D1 RID: 721 RVA: 0x00015877 File Offset: 0x00013A77
	public void OnLoadFailedAdfuri(string appID, string errorCode)
	{
		global::Debug.Log("AdModule#OnLoadFailedAdfuri");
		this.OnEvent("OnFailed(Load) " + errorCode);
	}

	// Token: 0x060002D2 RID: 722 RVA: 0x00015894 File Offset: 0x00013A94
	public void OnPlayFailedAdfuri(string appID, string errorCode)
	{
		global::Debug.Log("AdModule#OnPlayFailedAdfuri");
		this.OnEvent("OnFailed(Play)");
	}

	// Token: 0x060002D3 RID: 723 RVA: 0x000158AB File Offset: 0x00013AAB
	public void OnStartAdfuriReward(string appID, string adnetworkKey)
	{
		this.OnEvent("OnStarted " + adnetworkKey);
	}

	// Token: 0x060002D4 RID: 724 RVA: 0x000157FD File Offset: 0x000139FD
	public void OnFinishedAdfuriReward(string appID, string adnetworkKey)
	{
		this.OnEvent("OnFinished");
	}

	// Token: 0x060002D5 RID: 725 RVA: 0x000158BE File Offset: 0x00013ABE
	public void OnPlayFailedAdfuriReward(string appID, string adnetworkKey)
	{
		this.OnEvent("OnFailed(Play)");
	}

	// Token: 0x060002D6 RID: 726 RVA: 0x000158CB File Offset: 0x00013ACB
	public void OnClosedAdfuriReward(string appID, string adnetworkKey)
	{
		this.OnEvent("OnClosed");
	}

	// Token: 0x060002D7 RID: 727 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
	// Note: this type is marked as 'beforefieldinit'.
	static AdModule()
	{
	}

	// Token: 0x040000F3 RID: 243
	public bool IsLoaded;

	// Token: 0x040000F4 RID: 244
	public bool IsShowing;

	// Token: 0x040000F5 RID: 245
	protected string Type;

	// Token: 0x040000F6 RID: 246
	private string UnitId;

	// Token: 0x040000F7 RID: 247
	protected bool IsRewarded;

	// Token: 0x040000F8 RID: 248
	private bool IsWaiting;

	// Token: 0x040000F9 RID: 249
	private static float _Density;

	// Token: 0x040000FA RID: 250
	private int RetryCount = 5;

	// Token: 0x040000FB RID: 251
	private Vector2 PixelSize = Vector2.zero;

	// Token: 0x040000FC RID: 252
	private BannerView AdMobView;

	// Token: 0x040000FD RID: 253
	private RewardBasedVideoAd AdMobVideo;

	// Token: 0x040000FE RID: 254
	private AdfurikunMovieNativeAdViewUtility AdfuriNative;

	// Token: 0x040000FF RID: 255
	private AdfurikunMovieRewardUtility AdfuriReward;

	// Token: 0x04000100 RID: 256
	[CompilerGenerated]
	private EventHandler<AdModule.AdEventArgs> OnAdEvent;

	// Token: 0x02000137 RID: 311
	public class AdEventArgs : EventArgs
	{
		// Token: 0x17000150 RID: 336
		// (get) Token: 0x060009B8 RID: 2488 RVA: 0x0002F4E6 File Offset: 0x0002D6E6
		// (set) Token: 0x060009B9 RID: 2489 RVA: 0x0002F4EE File Offset: 0x0002D6EE
		public string Type
		{
			[CompilerGenerated]
			get
			{
				return this.<Type>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Type>k__BackingField = value;
			}
		}

		// Token: 0x17000151 RID: 337
		// (get) Token: 0x060009BA RID: 2490 RVA: 0x0002F4F7 File Offset: 0x0002D6F7
		// (set) Token: 0x060009BB RID: 2491 RVA: 0x0002F4FF File Offset: 0x0002D6FF
		public string Message
		{
			[CompilerGenerated]
			get
			{
				return this.<Message>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Message>k__BackingField = value;
			}
		}

		// Token: 0x060009BC RID: 2492 RVA: 0x0002201B File Offset: 0x0002021B
		public AdEventArgs()
		{
		}

		// Token: 0x040004D3 RID: 1235
		[CompilerGenerated]
		private string <Type>k__BackingField;

		// Token: 0x040004D4 RID: 1236
		[CompilerGenerated]
		private string <Message>k__BackingField;
	}

	// Token: 0x02000138 RID: 312
	[CompilerGenerated]
	private sealed class <StartFakeAdMovie>d__28 : IEnumerator<object>, IEnumerator, IDisposable
	{
		// Token: 0x060009BD RID: 2493 RVA: 0x0002F508 File Offset: 0x0002D708
		[DebuggerHidden]
		public <StartFakeAdMovie>d__28(int <>1__state)
		{
			this.<>1__state = <>1__state;
		}

		// Token: 0x060009BE RID: 2494 RVA: 0x0000F8D6 File Offset: 0x0000DAD6
		[DebuggerHidden]
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060009BF RID: 2495 RVA: 0x0002F518 File Offset: 0x0002D718
		bool IEnumerator.MoveNext()
		{
			int num = this.<>1__state;
			AdModule adModule = this;
			if (num != 0)
			{
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
			}
			else
			{
				this.<>1__state = -1;
				DialogManager.ShowDialog("FakeMovieAdScreen", Array.Empty<object>());
			}
			if (!DialogManager.IsShowing())
			{
				adModule.OnEvent("OnFinished");
				adModule.OnEvent("OnClosed");
				return false;
			}
			this.<>2__current = null;
			this.<>1__state = 1;
			return true;
		}

		// Token: 0x17000152 RID: 338
		// (get) Token: 0x060009C0 RID: 2496 RVA: 0x0002F589 File Offset: 0x0002D789
		object IEnumerator<object>.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x060009C1 RID: 2497 RVA: 0x00026A5A File Offset: 0x00024C5A
		[DebuggerHidden]
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000153 RID: 339
		// (get) Token: 0x060009C2 RID: 2498 RVA: 0x0002F589 File Offset: 0x0002D789
		object IEnumerator.Current
		{
			[DebuggerHidden]
			get
			{
				return this.<>2__current;
			}
		}

		// Token: 0x040004D5 RID: 1237
		private int <>1__state;

		// Token: 0x040004D6 RID: 1238
		private object <>2__current;

		// Token: 0x040004D7 RID: 1239
		public AdModule <>4__this;
	}
}
