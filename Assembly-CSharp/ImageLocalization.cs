﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000011 RID: 17
public class ImageLocalization : MonoBehaviour
{
	// Token: 0x06000083 RID: 131 RVA: 0x000082FC File Offset: 0x000064FC
	private void OnEnable()
	{
		Image component = base.GetComponent<Image>();
		if (component != null)
		{
			component.sprite = this.LocalizedSprite[LanguageManager.GetIndex()];
		}
		SpriteRenderer component2 = base.GetComponent<SpriteRenderer>();
		if (component2 != null)
		{
			component2.sprite = this.LocalizedSprite[LanguageManager.GetIndex()];
		}
	}

	// Token: 0x06000084 RID: 132 RVA: 0x000025AD File Offset: 0x000007AD
	public ImageLocalization()
	{
	}

	// Token: 0x04000024 RID: 36
	[SerializeField]
	private Sprite[] LocalizedSprite;
}
