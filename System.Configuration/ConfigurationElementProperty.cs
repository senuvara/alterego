﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Specifies the property of a configuration element. This class cannot be inherited.</summary>
	// Token: 0x0200001B RID: 27
	public sealed class ConfigurationElementProperty
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ConfigurationElementProperty" /> class, based on a supplied parameter.</summary>
		/// <param name="validator">A <see cref="T:System.Configuration.ConfigurationValidatorBase" /> object.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="validator" /> is <see langword="null" />.</exception>
		// Token: 0x0600010E RID: 270 RVA: 0x00002050 File Offset: 0x00000250
		public ConfigurationElementProperty(ConfigurationValidatorBase validator)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a <see cref="T:System.Configuration.ConfigurationValidatorBase" /> object used to validate the <see cref="T:System.Configuration.ConfigurationElementProperty" /> object.</summary>
		/// <returns>A <see cref="T:System.Configuration.ConfigurationValidatorBase" /> object.</returns>
		// Token: 0x1700006A RID: 106
		// (get) Token: 0x0600010F RID: 271 RVA: 0x00002057 File Offset: 0x00000257
		public ConfigurationValidatorBase Validator
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
