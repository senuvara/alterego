﻿using System;
using System.Collections.Specialized;
using System.Security.Cryptography;
using System.Security.Permissions;
using System.Xml;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides a <see cref="T:System.Configuration.ProtectedConfigurationProvider" /> instance that uses RSA encryption to encrypt and decrypt configuration data.</summary>
	// Token: 0x02000051 RID: 81
	[PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
	public sealed class RsaProtectedConfigurationProvider : ProtectedConfigurationProvider
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.RsaProtectedConfigurationProvider" /> class. </summary>
		// Token: 0x06000245 RID: 581 RVA: 0x00002050 File Offset: 0x00000250
		public RsaProtectedConfigurationProvider()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the name of the Windows cryptography API (crypto API) cryptographic service provider (CSP).</summary>
		/// <returns>The name of the CryptoAPI cryptographic service provider.</returns>
		// Token: 0x170000D2 RID: 210
		// (get) Token: 0x06000246 RID: 582 RVA: 0x00002057 File Offset: 0x00000257
		public string CspProviderName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the name of the key container.</summary>
		/// <returns>The name of the key container.</returns>
		// Token: 0x170000D3 RID: 211
		// (get) Token: 0x06000247 RID: 583 RVA: 0x00002057 File Offset: 0x00000257
		public string KeyContainerName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the public key used by the provider.</summary>
		/// <returns>An <see cref="T:System.Security.Cryptography.RSAParameters" /> object that contains the public key used by the provider.</returns>
		// Token: 0x170000D4 RID: 212
		// (get) Token: 0x06000248 RID: 584 RVA: 0x00002C20 File Offset: 0x00000E20
		public RSAParameters RsaPublicKey
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(RSAParameters);
			}
		}

		/// <summary>Gets a value indicating whether the provider uses FIPS.</summary>
		/// <returns>
		///     <see langword="true" /> if the provider uses FIPS; otherwise, <see langword="false" />.</returns>
		// Token: 0x170000D5 RID: 213
		// (get) Token: 0x06000249 RID: 585 RVA: 0x00002C3C File Offset: 0x00000E3C
		public bool UseFIPS
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value that indicates whether the <see cref="T:System.Configuration.RsaProtectedConfigurationProvider" /> object is using the machine key container.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Configuration.RsaProtectedConfigurationProvider" /> object is using the machine key container; otherwise, <see langword="false" />.</returns>
		// Token: 0x170000D6 RID: 214
		// (get) Token: 0x0600024A RID: 586 RVA: 0x00002C58 File Offset: 0x00000E58
		public bool UseMachineContainer
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value that indicates whether the provider is using Optimal Asymmetric Encryption Padding (OAEP) key exchange data.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Configuration.RsaProtectedConfigurationProvider" /> object is using Optimal Asymmetric Encryption Padding (OAEP) key exchange data; otherwise, <see langword="false" />.</returns>
		// Token: 0x170000D7 RID: 215
		// (get) Token: 0x0600024B RID: 587 RVA: 0x00002C74 File Offset: 0x00000E74
		public bool UseOAEP
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Adds a key to the RSA key container.</summary>
		/// <param name="keySize">The size of the key to add.</param>
		/// <param name="exportable">
		///       <see langword="true" /> to indicate that the key is exportable; otherwise, <see langword="false" />.</param>
		// Token: 0x0600024C RID: 588 RVA: 0x00002050 File Offset: 0x00000250
		public void AddKey(int keySize, bool exportable)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Decrypts the XML node passed to it.</summary>
		/// <param name="encryptedNode">The <see cref="T:System.Xml.XmlNode" /> to decrypt.</param>
		/// <returns>The decrypted XML node.</returns>
		// Token: 0x0600024D RID: 589 RVA: 0x00002057 File Offset: 0x00000257
		public override XmlNode Decrypt(XmlNode encryptedNode)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Removes a key from the RSA key container.</summary>
		// Token: 0x0600024E RID: 590 RVA: 0x00002050 File Offset: 0x00000250
		public void DeleteKey()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Encrypts the XML node passed to it.</summary>
		/// <param name="node">The <see cref="T:System.Xml.XmlNode" /> to encrypt.</param>
		/// <returns>An encrypted <see cref="T:System.Xml.XmlNode" /> object.</returns>
		// Token: 0x0600024F RID: 591 RVA: 0x00002057 File Offset: 0x00000257
		public override XmlNode Encrypt(XmlNode node)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Exports an RSA key from the key container.</summary>
		/// <param name="xmlFileName">The file name and path to export the key to.</param>
		/// <param name="includePrivateParameters">
		///       <see langword="true" /> to indicate that private parameters are exported; otherwise, <see langword="false" />.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="path" /> is a zero-length string, contains only white space, or contains one or more invalid characters as defined by <see cref="F:System.IO.Path.InvalidPathChars" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.IO.PathTooLongException">The specified path, file name, or both exceed the system-defined maximum length. For example, on Windows-based platforms, paths must be less than 248 characters, and file names must be less than 260 characters. </exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified path is invalid, such as being on an unmapped drive. </exception>
		/// <exception cref="T:System.IO.IOException">An error occurred while opening the file. </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">
		///         <paramref name="path" /> specified a file that is read-only.-or- This operation is not supported on the current platform.-or- 
		///         <paramref name="path" /> specified a directory.-or- The caller does not have the required permission. </exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The file specified in <paramref name="path" /> was not found. </exception>
		/// <exception cref="T:System.NotSupportedException">
		///         <paramref name="path" /> is in an invalid format. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x06000250 RID: 592 RVA: 0x00002050 File Offset: 0x00000250
		public void ExportKey(string xmlFileName, bool includePrivateParameters)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Imports an RSA key into the key container.</summary>
		/// <param name="xmlFileName">The file name and path to import the key from.</param>
		/// <param name="exportable">
		///       <see langword="true" /> to indicate that the key is exportable; otherwise, <see langword="false" />.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="path" /> is a zero-length string, contains only white space, or contains one or more invalid characters as defined by <see cref="F:System.IO.Path.InvalidPathChars" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.IO.PathTooLongException">The specified path, file name, or both exceed the system-defined maximum length. For example, on Windows-based platforms, paths must be less than 248 characters, and file names must be less than 260 characters. </exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified path is invalid, such as being on an unmapped drive. </exception>
		/// <exception cref="T:System.IO.IOException">An error occurred while opening the file. </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">
		///         <paramref name="path" /> specified a file that is write-only.-or- This operation is not supported on the current platform.-or- 
		///         <paramref name="path" /> specified a directory.-or- The caller does not have the required permission. </exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The file specified in <paramref name="path" /> was not found. </exception>
		/// <exception cref="T:System.NotSupportedException">
		///         <paramref name="path" /> is in an invalid format. </exception>
		// Token: 0x06000251 RID: 593 RVA: 0x00002050 File Offset: 0x00000250
		public void ImportKey(string xmlFileName, bool exportable)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes the provider with default settings.</summary>
		/// <param name="name">The provider name to use for the object.</param>
		/// <param name="configurationValues">A <see cref="T:System.Collections.Specialized.NameValueCollection" /> collection of values to use when initializing the object.</param>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">
		///         <paramref name="configurationValues" /> includes one or more unrecognized values.</exception>
		// Token: 0x06000252 RID: 594 RVA: 0x00002050 File Offset: 0x00000250
		public override void Initialize(string name, NameValueCollection configurationValues)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
