﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Declaratively instructs the .NET Framework to perform string validation on a configuration property. This class cannot be inherited.</summary>
	// Token: 0x02000053 RID: 83
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class StringValidatorAttribute : ConfigurationValidatorAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.StringValidatorAttribute" /> class.</summary>
		// Token: 0x06000258 RID: 600 RVA: 0x000024C7 File Offset: 0x000006C7
		public StringValidatorAttribute()
		{
		}

		/// <summary>Gets or sets the invalid characters for the property.</summary>
		/// <returns>The string that contains the set of characters that are not allowed for the property.</returns>
		// Token: 0x170000D8 RID: 216
		// (get) Token: 0x06000259 RID: 601 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x0600025A RID: 602 RVA: 0x00002050 File Offset: 0x00000250
		public string InvalidCharacters
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the maximum length allowed for the string to assign to the property.</summary>
		/// <returns>An integer that indicates the maximum allowed length for the string to assign to the property.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The selected value is less than <see cref="P:System.Configuration.StringValidatorAttribute.MinLength" />.</exception>
		// Token: 0x170000D9 RID: 217
		// (get) Token: 0x0600025B RID: 603 RVA: 0x00002CAC File Offset: 0x00000EAC
		// (set) Token: 0x0600025C RID: 604 RVA: 0x00002050 File Offset: 0x00000250
		public int MaxLength
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the minimum allowed value for the string to assign to the property.</summary>
		/// <returns>An integer that indicates the allowed minimum length for the string to assign to the property.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The selected value is greater than <see cref="P:System.Configuration.StringValidatorAttribute.MaxLength" />.</exception>
		// Token: 0x170000DA RID: 218
		// (get) Token: 0x0600025D RID: 605 RVA: 0x00002CC8 File Offset: 0x00000EC8
		// (set) Token: 0x0600025E RID: 606 RVA: 0x00002050 File Offset: 0x00000250
		public int MinLength
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets an instance of the <see cref="T:System.Configuration.StringValidator" /> class.</summary>
		/// <returns>A current <see cref="T:System.Configuration.StringValidator" /> settings in a <see cref="T:System.Configuration.ConfigurationValidatorBase" /> validator instance.</returns>
		// Token: 0x170000DB RID: 219
		// (get) Token: 0x0600025F RID: 607 RVA: 0x00002057 File Offset: 0x00000257
		public override ConfigurationValidatorBase ValidatorInstance
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
