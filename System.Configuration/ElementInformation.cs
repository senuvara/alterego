﻿using System;
using System.Collections;
using Unity;

namespace System.Configuration
{
	/// <summary>Contains meta-information about an individual element within the configuration. This class cannot be inherited.</summary>
	// Token: 0x02000017 RID: 23
	public sealed class ElementInformation
	{
		// Token: 0x060000F1 RID: 241 RVA: 0x00002050 File Offset: 0x00000250
		internal ElementInformation()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the errors for the associated element and subelements</summary>
		/// <returns>The collection containing the errors for the associated element and subelements</returns>
		// Token: 0x17000052 RID: 82
		// (get) Token: 0x060000F2 RID: 242 RVA: 0x00002057 File Offset: 0x00000257
		public ICollection Errors
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a value indicating whether the associated <see cref="T:System.Configuration.ConfigurationElement" /> object is a <see cref="T:System.Configuration.ConfigurationElementCollection" /> collection.</summary>
		/// <returns>
		///     <see langword="true" /> if the associated <see cref="T:System.Configuration.ConfigurationElement" /> object is a <see cref="T:System.Configuration.ConfigurationElementCollection" /> collection; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000053 RID: 83
		// (get) Token: 0x060000F3 RID: 243 RVA: 0x00002558 File Offset: 0x00000758
		public bool IsCollection
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value that indicates whether the associated <see cref="T:System.Configuration.ConfigurationElement" /> object cannot be modified.</summary>
		/// <returns>
		///     <see langword="true" /> if the associated <see cref="T:System.Configuration.ConfigurationElement" /> object cannot be modified; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000054 RID: 84
		// (get) Token: 0x060000F4 RID: 244 RVA: 0x00002574 File Offset: 0x00000774
		public bool IsLocked
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value indicating whether the associated <see cref="T:System.Configuration.ConfigurationElement" /> object is in the configuration file.</summary>
		/// <returns>
		///     <see langword="true" /> if the associated <see cref="T:System.Configuration.ConfigurationElement" /> object is in the configuration file; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000055 RID: 85
		// (get) Token: 0x060000F5 RID: 245 RVA: 0x00002590 File Offset: 0x00000790
		public bool IsPresent
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets the line number in the configuration file where the associated <see cref="T:System.Configuration.ConfigurationElement" /> object is defined.</summary>
		/// <returns>The line number in the configuration file where the associated <see cref="T:System.Configuration.ConfigurationElement" /> object is defined.</returns>
		// Token: 0x17000056 RID: 86
		// (get) Token: 0x060000F6 RID: 246 RVA: 0x000025AC File Offset: 0x000007AC
		public int LineNumber
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets a <see cref="T:System.Configuration.PropertyInformationCollection" /> collection of the properties in the associated <see cref="T:System.Configuration.ConfigurationElement" /> object.</summary>
		/// <returns>A <see cref="T:System.Configuration.PropertyInformationCollection" /> collection of the properties in the associated <see cref="T:System.Configuration.ConfigurationElement" /> object.</returns>
		// Token: 0x17000057 RID: 87
		// (get) Token: 0x060000F7 RID: 247 RVA: 0x00002057 File Offset: 0x00000257
		public PropertyInformationCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the source file where the associated <see cref="T:System.Configuration.ConfigurationElement" /> object originated.</summary>
		/// <returns>The source file where the associated <see cref="T:System.Configuration.ConfigurationElement" /> object originated.</returns>
		// Token: 0x17000058 RID: 88
		// (get) Token: 0x060000F8 RID: 248 RVA: 0x00002057 File Offset: 0x00000257
		public string Source
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the type of the associated <see cref="T:System.Configuration.ConfigurationElement" /> object.</summary>
		/// <returns>The type of the associated <see cref="T:System.Configuration.ConfigurationElement" /> object.</returns>
		// Token: 0x17000059 RID: 89
		// (get) Token: 0x060000F9 RID: 249 RVA: 0x00002057 File Offset: 0x00000257
		public Type Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the object used to validate the associated <see cref="T:System.Configuration.ConfigurationElement" /> object.</summary>
		/// <returns>The object used to validate the associated <see cref="T:System.Configuration.ConfigurationElement" /> object.</returns>
		// Token: 0x1700005A RID: 90
		// (get) Token: 0x060000FA RID: 250 RVA: 0x00002057 File Offset: 0x00000257
		public ConfigurationValidatorBase Validator
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
