﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Converts between type and string values. This class cannot be inherited.</summary>
	// Token: 0x0200005C RID: 92
	public sealed class TypeNameConverter : ConfigurationConverterBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.TypeNameConverter" /> class.</summary>
		// Token: 0x06000279 RID: 633 RVA: 0x00002050 File Offset: 0x00000250
		public TypeNameConverter()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
