﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Declaratively instructs the .NET Framework to instantiate a configuration property. This class cannot be inherited.</summary>
	// Token: 0x0200003A RID: 58
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class ConfigurationPropertyAttribute : Attribute
	{
		/// <summary>Initializes a new instance of <see cref="T:System.Configuration.ConfigurationPropertyAttribute" /> class.</summary>
		/// <param name="name">Name of the <see cref="T:System.Configuration.ConfigurationProperty" /> object defined.</param>
		// Token: 0x060001DB RID: 475 RVA: 0x000024C7 File Offset: 0x000006C7
		public ConfigurationPropertyAttribute(string name)
		{
		}

		/// <summary>Gets or sets the default value for the decorated property.</summary>
		/// <returns>The object representing the default value of the decorated configuration-element property.</returns>
		// Token: 0x170000AE RID: 174
		// (get) Token: 0x060001DC RID: 476 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x060001DD RID: 477 RVA: 0x00002050 File Offset: 0x00000250
		public object DefaultValue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether this is the default property collection for the decorated configuration property. </summary>
		/// <returns>
		///     <see langword="true" /> if the property represents the default collection of an element; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170000AF RID: 175
		// (get) Token: 0x060001DE RID: 478 RVA: 0x00002A0C File Offset: 0x00000C0C
		// (set) Token: 0x060001DF RID: 479 RVA: 0x00002050 File Offset: 0x00000250
		public bool IsDefaultCollection
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether this is a key property for the decorated element property.</summary>
		/// <returns>
		///     <see langword="true" /> if the property is a key property for an element of the collection; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x060001E0 RID: 480 RVA: 0x00002A28 File Offset: 0x00000C28
		// (set) Token: 0x060001E1 RID: 481 RVA: 0x00002050 File Offset: 0x00000250
		public bool IsKey
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether the decorated element property is required.</summary>
		/// <returns>
		///     <see langword="true" /> if the property is required; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x060001E2 RID: 482 RVA: 0x00002A44 File Offset: 0x00000C44
		// (set) Token: 0x060001E3 RID: 483 RVA: 0x00002050 File Offset: 0x00000250
		public bool IsRequired
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the name of the decorated configuration-element property.</summary>
		/// <returns>The name of the decorated configuration-element property.</returns>
		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x060001E4 RID: 484 RVA: 0x00002057 File Offset: 0x00000257
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Configuration.ConfigurationPropertyOptions" /> for the decorated configuration-element property.</summary>
		/// <returns>One of the <see cref="T:System.Configuration.ConfigurationPropertyOptions" /> enumeration values associated with the property.</returns>
		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x060001E5 RID: 485 RVA: 0x00002A60 File Offset: 0x00000C60
		// (set) Token: 0x060001E6 RID: 486 RVA: 0x00002050 File Offset: 0x00000250
		public ConfigurationPropertyOptions Options
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ConfigurationPropertyOptions.None;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
