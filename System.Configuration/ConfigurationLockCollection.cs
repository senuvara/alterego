﻿using System;
using System.Collections;
using Unity;

namespace System.Configuration
{
	/// <summary>Contains a collection of locked configuration objects. This class cannot be inherited.</summary>
	// Token: 0x0200001C RID: 28
	public sealed class ConfigurationLockCollection : ICollection, IEnumerable
	{
		// Token: 0x06000110 RID: 272 RVA: 0x00002050 File Offset: 0x00000250
		internal ConfigurationLockCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a list of configuration objects contained in the collection.</summary>
		/// <returns>A comma-delimited string that lists the lock configuration objects in the collection.</returns>
		// Token: 0x1700006B RID: 107
		// (get) Token: 0x06000111 RID: 273 RVA: 0x00002057 File Offset: 0x00000257
		public string AttributeList
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the number of locked configuration objects contained in the collection.</summary>
		/// <returns>The number of locked configuration objects contained in the collection.</returns>
		// Token: 0x1700006C RID: 108
		// (get) Token: 0x06000112 RID: 274 RVA: 0x00002670 File Offset: 0x00000870
		public int Count
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets a value specifying whether the collection of locked objects has parent elements.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Configuration.ConfigurationLockCollection" /> collection has parent elements; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700006D RID: 109
		// (get) Token: 0x06000113 RID: 275 RVA: 0x0000268C File Offset: 0x0000088C
		public bool HasParentElements
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value specifying whether the collection has been modified.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Configuration.ConfigurationLockCollection" /> collection has been modified; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700006E RID: 110
		// (get) Token: 0x06000114 RID: 276 RVA: 0x000026A8 File Offset: 0x000008A8
		public bool IsModified
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value specifying whether the collection is synchronized.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Configuration.ConfigurationLockCollection" /> collection is synchronized; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700006F RID: 111
		// (get) Token: 0x06000115 RID: 277 RVA: 0x000026C4 File Offset: 0x000008C4
		public bool IsSynchronized
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets an object used to synchronize access to this <see cref="T:System.Configuration.ConfigurationLockCollection" /> collection.</summary>
		/// <returns>An object used to synchronize access to this <see cref="T:System.Configuration.ConfigurationLockCollection" /> collection.</returns>
		// Token: 0x17000070 RID: 112
		// (get) Token: 0x06000116 RID: 278 RVA: 0x00002057 File Offset: 0x00000257
		public object SyncRoot
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Locks a configuration object by adding it to the collection.</summary>
		/// <param name="name">The name of the configuration object.</param>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">Occurs when the <paramref name="name" /> does not match an existing configuration object within the collection.</exception>
		// Token: 0x06000117 RID: 279 RVA: 0x00002050 File Offset: 0x00000250
		public void Add(string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Clears all configuration objects from the collection.</summary>
		// Token: 0x06000118 RID: 280 RVA: 0x00002050 File Offset: 0x00000250
		public void Clear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Verifies whether a specific configuration object is locked.</summary>
		/// <param name="name">The name of the configuration object to verify.</param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Configuration.ConfigurationLockCollection" /> contains the specified configuration object; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000119 RID: 281 RVA: 0x000026E0 File Offset: 0x000008E0
		public bool Contains(string name)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Copies the entire <see cref="T:System.Configuration.ConfigurationLockCollection" /> collection to a compatible one-dimensional <see cref="T:System.Array" />, starting at the specified index of the target array.</summary>
		/// <param name="array">A one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from the <see cref="T:System.Configuration.ConfigurationLockCollection" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
		/// <param name="index">The zero-based index in <paramref name="array" /> at which copying begins.</param>
		// Token: 0x0600011A RID: 282 RVA: 0x00002050 File Offset: 0x00000250
		public void CopyTo(string[] array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets an <see cref="T:System.Collections.IEnumerator" /> object, which is used to iterate through this <see cref="T:System.Configuration.ConfigurationLockCollection" /> collection.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> object.</returns>
		// Token: 0x0600011B RID: 283 RVA: 0x00002057 File Offset: 0x00000257
		public IEnumerator GetEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Verifies whether a specific configuration object is read-only.</summary>
		/// <param name="name">The name of the configuration object to verify.</param>
		/// <returns>
		///     <see langword="true" /> if the specified configuration object in the <see cref="T:System.Configuration.ConfigurationLockCollection" /> collection is read-only; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The specified configuration object is not in the collection.</exception>
		// Token: 0x0600011C RID: 284 RVA: 0x000026FC File Offset: 0x000008FC
		public bool IsReadOnly(string name)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Removes a configuration object from the collection.</summary>
		/// <param name="name">The name of the configuration object.</param>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">Occurs when the <paramref name="name" /> does not match an existing configuration object within the collection.</exception>
		// Token: 0x0600011D RID: 285 RVA: 0x00002050 File Offset: 0x00000250
		public void Remove(string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Locks a set of configuration objects based on the supplied list.</summary>
		/// <param name="attributeList">A comma-delimited string.</param>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">Occurs when an item in the <paramref name="attributeList" /> parameter is not a valid lockable configuration attribute.</exception>
		// Token: 0x0600011E RID: 286 RVA: 0x00002050 File Offset: 0x00000250
		public void SetFromList(string attributeList)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Copies the entire <see cref="T:System.Configuration.ConfigurationLockCollection" /> collection to a compatible one-dimensional <see cref="T:System.Array" />, starting at the specified index of the target array.</summary>
		/// <param name="array">A one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from the <see cref="T:System.Configuration.ConfigurationLockCollection" /> collection. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
		/// <param name="index">The zero-based index in <paramref name="array" /> at which copying begins.</param>
		// Token: 0x0600011F RID: 287 RVA: 0x00002050 File Offset: 0x00000250
		void ICollection.CopyTo(Array array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
