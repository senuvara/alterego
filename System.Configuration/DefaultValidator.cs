﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides validation of an object. This class cannot be inherited.</summary>
	// Token: 0x0200003C RID: 60
	public sealed class DefaultValidator : ConfigurationValidatorBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.DefaultValidator" /> class. </summary>
		// Token: 0x060001EE RID: 494 RVA: 0x00002050 File Offset: 0x00000250
		public DefaultValidator()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Determines whether an object can be validated, based on type.</summary>
		/// <param name="type">The object type.</param>
		/// <returns>
		///     <see langword="true" /> for all types being validated. </returns>
		// Token: 0x060001EF RID: 495 RVA: 0x00002A98 File Offset: 0x00000C98
		public override bool CanValidate(Type type)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Determines whether the value of an object is valid. </summary>
		/// <param name="value">The object value.</param>
		// Token: 0x060001F0 RID: 496 RVA: 0x00002050 File Offset: 0x00000250
		public override void Validate(object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
