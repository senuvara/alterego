﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Declaratively instructs the .NET Framework to perform long-integer validation on a configuration property. This class cannot be inherited.</summary>
	// Token: 0x02000046 RID: 70
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class LongValidatorAttribute : ConfigurationValidatorAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.LongValidatorAttribute" /> class.</summary>
		// Token: 0x06000215 RID: 533 RVA: 0x000024C7 File Offset: 0x000006C7
		public LongValidatorAttribute()
		{
		}

		/// <summary>Gets or sets a value that indicates whether to include or exclude the integers in the range defined by the <see cref="P:System.Configuration.LongValidatorAttribute.MinValue" /> and <see cref="P:System.Configuration.LongValidatorAttribute.MaxValue" /> property values.</summary>
		/// <returns>
		///     <see langword="true" /> if the value must be excluded; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170000BD RID: 189
		// (get) Token: 0x06000216 RID: 534 RVA: 0x00002B94 File Offset: 0x00000D94
		// (set) Token: 0x06000217 RID: 535 RVA: 0x00002050 File Offset: 0x00000250
		public bool ExcludeRange
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the maximum value allowed for the property.</summary>
		/// <returns>A long integer that indicates the allowed maximum value.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The selected value is less than <see cref="P:System.Configuration.LongValidatorAttribute.MinValue" />.</exception>
		// Token: 0x170000BE RID: 190
		// (get) Token: 0x06000218 RID: 536 RVA: 0x00002BB0 File Offset: 0x00000DB0
		// (set) Token: 0x06000219 RID: 537 RVA: 0x00002050 File Offset: 0x00000250
		public long MaxValue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0L;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the minimum value allowed for the property.</summary>
		/// <returns>An integer that indicates the allowed minimum value.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The selected value is greater than <see cref="P:System.Configuration.LongValidatorAttribute.MaxValue" />.</exception>
		// Token: 0x170000BF RID: 191
		// (get) Token: 0x0600021A RID: 538 RVA: 0x00002BCC File Offset: 0x00000DCC
		// (set) Token: 0x0600021B RID: 539 RVA: 0x00002050 File Offset: 0x00000250
		public long MinValue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0L;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets an instance of the <see cref="T:System.Configuration.LongValidator" /> class.</summary>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationValidatorBase" /> validator instance.</returns>
		// Token: 0x170000C0 RID: 192
		// (get) Token: 0x0600021C RID: 540 RVA: 0x00002057 File Offset: 0x00000257
		public override ConfigurationValidatorBase ValidatorInstance
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
