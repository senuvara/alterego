﻿using System;

namespace System.Configuration
{
	/// <summary>Determines which properties are written out to a configuration file.</summary>
	// Token: 0x0200000A RID: 10
	public enum ConfigurationSaveMode
	{
		/// <summary>Causes all properties to be written to the configuration file. This is useful mostly for creating information configuration files or moving configuration values from one machine to another.</summary>
		// Token: 0x04000007 RID: 7
		Full = 2,
		/// <summary>Causes only properties that differ from inherited values to be written to the configuration file.</summary>
		// Token: 0x04000008 RID: 8
		Minimal = 1,
		/// <summary>Causes only modified properties to be written to the configuration file, even when the value is the same as the inherited value.</summary>
		// Token: 0x04000009 RID: 9
		Modified = 0
	}
}
