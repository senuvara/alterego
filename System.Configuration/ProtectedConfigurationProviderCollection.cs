﻿using System;
using System.Configuration.Provider;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides a collection of <see cref="T:System.Configuration.ProtectedConfigurationProvider" /> objects.</summary>
	// Token: 0x0200004C RID: 76
	public class ProtectedConfigurationProviderCollection : ProviderCollection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ProtectedConfigurationProviderCollection" /> class using default settings.</summary>
		// Token: 0x06000234 RID: 564 RVA: 0x00002050 File Offset: 0x00000250
		public ProtectedConfigurationProviderCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a <see cref="T:System.Configuration.ProtectedConfigurationProvider" /> object in the collection with the specified name.</summary>
		/// <param name="name">The name of a <see cref="T:System.Configuration.ProtectedConfigurationProvider" /> object in the collection.</param>
		/// <returns>The <see cref="T:System.Configuration.ProtectedConfigurationProvider" /> object with the specified name, or <see langword="null" /> if there is no object with that name.</returns>
		// Token: 0x170000CA RID: 202
		public ProtectedConfigurationProvider this[string name]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Adds a <see cref="T:System.Configuration.ProtectedConfigurationProvider" /> object to the collection.</summary>
		/// <param name="provider">A <see cref="T:System.Configuration.ProtectedConfigurationProvider" /> object to add to the collection.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="provider" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="provider" /> is not a <see cref="T:System.Configuration.ProtectedConfigurationProvider" /> object.</exception>
		/// <exception cref="T:System.Configuration.ConfigurationException">The <see cref="T:System.Configuration.ProtectedConfigurationProvider" /> object to add already exists in the collection.- or -The collection is read-only.</exception>
		// Token: 0x06000236 RID: 566 RVA: 0x00002050 File Offset: 0x00000250
		public override void Add(ProviderBase provider)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
