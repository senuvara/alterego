﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Converts a time span expressed in minutes. </summary>
	// Token: 0x02000056 RID: 86
	public class TimeSpanMinutesConverter : ConfigurationConverterBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.TimeSpanMinutesConverter" /> class.</summary>
		// Token: 0x06000266 RID: 614 RVA: 0x00002050 File Offset: 0x00000250
		public TimeSpanMinutesConverter()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
