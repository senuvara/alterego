﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Converts between a string and the standard infinite <see cref="T:System.TimeSpan" /> value.</summary>
	// Token: 0x02000042 RID: 66
	public sealed class InfiniteTimeSpanConverter : ConfigurationConverterBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.InfiniteTimeSpanConverter" /> class.</summary>
		// Token: 0x06000202 RID: 514 RVA: 0x00002050 File Offset: 0x00000250
		public InfiniteTimeSpanConverter()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
