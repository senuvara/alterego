﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Specifies a <see cref="T:System.Configuration.CallbackValidator" /> object to use for code validation. This class cannot be inherited.</summary>
	// Token: 0x02000028 RID: 40
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class CallbackValidatorAttribute : ConfigurationValidatorAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.CallbackValidatorAttribute" /> class. </summary>
		// Token: 0x0600016C RID: 364 RVA: 0x000024C7 File Offset: 0x000006C7
		public CallbackValidatorAttribute()
		{
		}

		/// <summary>Gets or sets the name of the callback method.</summary>
		/// <returns>The name of the method to call.</returns>
		// Token: 0x1700008F RID: 143
		// (get) Token: 0x0600016D RID: 365 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x0600016E RID: 366 RVA: 0x00002050 File Offset: 0x00000250
		public string CallbackMethodName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the type of the validator.</summary>
		/// <returns>The <see cref="T:System.Type" /> of the current validator attribute instance.</returns>
		// Token: 0x17000090 RID: 144
		// (get) Token: 0x0600016F RID: 367 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x06000170 RID: 368 RVA: 0x00002050 File Offset: 0x00000250
		public Type Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the validator instance.</summary>
		/// <returns>The current <see cref="T:System.Configuration.ConfigurationValidatorBase" /> instance.</returns>
		/// <exception cref="T:System.ArgumentNullException">The value of the <see cref="P:System.Configuration.CallbackValidatorAttribute.Type" /> property is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Configuration.CallbackValidatorAttribute.CallbackMethodName" /> property is not set to a public static void method with one object parameter.</exception>
		// Token: 0x17000091 RID: 145
		// (get) Token: 0x06000171 RID: 369 RVA: 0x00002057 File Offset: 0x00000257
		public override ConfigurationValidatorBase ValidatorInstance
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
