﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Contains a collection of <see cref="T:System.Configuration.KeyValueConfigurationElement" /> objects. </summary>
	// Token: 0x02000024 RID: 36
	[ConfigurationCollection(typeof(KeyValueConfigurationElement))]
	public class KeyValueConfigurationCollection : ConfigurationElementCollection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.KeyValueConfigurationCollection" /> class.</summary>
		// Token: 0x06000154 RID: 340 RVA: 0x00002050 File Offset: 0x00000250
		public KeyValueConfigurationCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the keys to all items contained in the <see cref="T:System.Configuration.KeyValueConfigurationCollection" /> collection.</summary>
		/// <returns>A string array.</returns>
		// Token: 0x17000088 RID: 136
		// (get) Token: 0x06000155 RID: 341 RVA: 0x00002057 File Offset: 0x00000257
		public string[] AllKeys
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the <see cref="T:System.Configuration.KeyValueConfigurationElement" /> object based on the supplied parameter.</summary>
		/// <param name="key">The key of the <see cref="T:System.Configuration.KeyValueConfigurationElement" /> contained in the collection.</param>
		/// <returns>A configuration element, or <see langword="null" /> if the key does not exist in the collection.</returns>
		// Token: 0x17000089 RID: 137
		public KeyValueConfigurationElement this[string key]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a collection of configuration properties.</summary>
		/// <returns>A collection of configuration properties.</returns>
		// Token: 0x1700008A RID: 138
		// (get) Token: 0x06000157 RID: 343 RVA: 0x00002057 File Offset: 0x00000257
		protected internal override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a value indicating whether an attempt to add a duplicate <see cref="T:System.Configuration.KeyValueConfigurationElement" /> object to the <see cref="T:System.Configuration.KeyValueConfigurationCollection" /> collection will cause an exception to be thrown.</summary>
		/// <returns>
		///     <see langword="true" /> if an attempt to add a duplicate <see cref="T:System.Configuration.KeyValueConfigurationElement" /> to the <see cref="T:System.Configuration.KeyValueConfigurationCollection" /> will cause an exception to be thrown; otherwise, <see langword="false" />. </returns>
		// Token: 0x1700008B RID: 139
		// (get) Token: 0x06000158 RID: 344 RVA: 0x000028BC File Offset: 0x00000ABC
		protected override bool ThrowOnDuplicate
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Adds a <see cref="T:System.Configuration.KeyValueConfigurationElement" /> object to the collection based on the supplied parameters.</summary>
		/// <param name="keyValue">A <see cref="T:System.Configuration.KeyValueConfigurationElement" />.</param>
		// Token: 0x06000159 RID: 345 RVA: 0x00002050 File Offset: 0x00000250
		public void Add(KeyValueConfigurationElement keyValue)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a <see cref="T:System.Configuration.KeyValueConfigurationElement" /> object to the collection based on the supplied parameters.</summary>
		/// <param name="key">A string specifying the key.</param>
		/// <param name="value">A string specifying the value.</param>
		// Token: 0x0600015A RID: 346 RVA: 0x00002050 File Offset: 0x00000250
		public void Add(string key, string value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Clears the <see cref="T:System.Configuration.KeyValueConfigurationCollection" /> collection.</summary>
		// Token: 0x0600015B RID: 347 RVA: 0x00002050 File Offset: 0x00000250
		public void Clear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>When overridden in a derived class, the <see cref="M:System.Configuration.KeyValueConfigurationCollection.CreateNewElement" /> method creates a new <see cref="T:System.Configuration.KeyValueConfigurationElement" /> object.</summary>
		/// <returns>A newly created <see cref="T:System.Configuration.KeyValueConfigurationElement" />.</returns>
		// Token: 0x0600015C RID: 348 RVA: 0x00002057 File Offset: 0x00000257
		protected override ConfigurationElement CreateNewElement()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the element key for a specified configuration element when overridden in a derived class.</summary>
		/// <param name="element">The <see cref="T:System.Configuration.KeyValueConfigurationElement" /> to which the key should be returned.</param>
		/// <returns>An object that acts as the key for the specified <see cref="T:System.Configuration.KeyValueConfigurationElement" />.</returns>
		// Token: 0x0600015D RID: 349 RVA: 0x00002057 File Offset: 0x00000257
		protected override object GetElementKey(ConfigurationElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Removes a <see cref="T:System.Configuration.KeyValueConfigurationElement" /> object from the collection.</summary>
		/// <param name="key">A string specifying the <paramref name="key" />.</param>
		// Token: 0x0600015E RID: 350 RVA: 0x00002050 File Offset: 0x00000250
		public void Remove(string key)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
