﻿using System;

namespace System.Configuration
{
	/// <summary>Specifies the level in the configuration hierarchy where a configuration property value originated.</summary>
	// Token: 0x0200001A RID: 26
	public enum PropertyValueOrigin
	{
		/// <summary>The configuration property value originates from the <see cref="P:System.Configuration.ConfigurationProperty.DefaultValue" /> property.</summary>
		// Token: 0x04000013 RID: 19
		Default,
		/// <summary>The configuration property value is inherited from a parent level in the configuration.</summary>
		// Token: 0x04000014 RID: 20
		Inherited,
		/// <summary>The configuration property value is defined at the current level of the hierarchy.</summary>
		// Token: 0x04000015 RID: 21
		SetHere
	}
}
