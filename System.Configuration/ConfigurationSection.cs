﻿using System;
using System.Runtime.Versioning;
using System.Xml;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents a section within a configuration file.</summary>
	// Token: 0x02000003 RID: 3
	public abstract class ConfigurationSection : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ConfigurationSection" /> class. </summary>
		// Token: 0x0600000B RID: 11 RVA: 0x00002050 File Offset: 0x00000250
		protected ConfigurationSection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a <see cref="T:System.Configuration.SectionInformation" /> object that contains the non-customizable information and functionality of the <see cref="T:System.Configuration.ConfigurationSection" /> object. </summary>
		/// <returns>A <see cref="T:System.Configuration.SectionInformation" /> that contains the non-customizable information and functionality of the <see cref="T:System.Configuration.ConfigurationSection" />.</returns>
		// Token: 0x17000004 RID: 4
		// (get) Token: 0x0600000C RID: 12 RVA: 0x00002057 File Offset: 0x00000257
		public SectionInformation SectionInformation
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Reads XML from the configuration file.</summary>
		/// <param name="reader">The <see cref="T:System.Xml.XmlReader" /> object, which reads from the configuration file. </param>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">
		///         <paramref name="reader" /> found no elements in the configuration file.</exception>
		// Token: 0x0600000D RID: 13 RVA: 0x00002050 File Offset: 0x00000250
		protected internal virtual void DeserializeSection(XmlReader reader)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns a custom object when overridden in a derived class.</summary>
		/// <returns>The object representing the section.</returns>
		// Token: 0x0600000E RID: 14 RVA: 0x00002057 File Offset: 0x00000257
		protected internal virtual object GetRuntimeObject()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Indicates whether this configuration element has been modified since it was last saved or loaded when implemented in a derived class.</summary>
		/// <returns>
		///     <see langword="true" /> if the element has been modified; otherwise, <see langword="false" />. </returns>
		// Token: 0x0600000F RID: 15 RVA: 0x0000207C File Offset: 0x0000027C
		protected internal override bool IsModified()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Resets the value of the <see cref="M:System.Configuration.ConfigurationElement.IsModified" /> method to <see langword="false" /> when implemented in a derived class.</summary>
		// Token: 0x06000010 RID: 16 RVA: 0x00002050 File Offset: 0x00000250
		protected internal override void ResetModified()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates an XML string containing an unmerged view of the <see cref="T:System.Configuration.ConfigurationSection" /> object as a single section to write to a file.</summary>
		/// <param name="parentElement">The <see cref="T:System.Configuration.ConfigurationElement" /> instance to use as the parent when performing the un-merge.</param>
		/// <param name="name">The name of the section to create.</param>
		/// <param name="saveMode">The <see cref="T:System.Configuration.ConfigurationSaveMode" /> instance to use when writing to a string.</param>
		/// <returns>An XML string containing an unmerged view of the <see cref="T:System.Configuration.ConfigurationSection" /> object.</returns>
		// Token: 0x06000011 RID: 17 RVA: 0x00002057 File Offset: 0x00000257
		protected internal virtual string SerializeSection(ConfigurationElement parentElement, string name, ConfigurationSaveMode saveMode)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Indicates whether the specified element should be serialized when the configuration object hierarchy is serialized for the specified target version of the .NET Framework.</summary>
		/// <param name="element">The <see cref="T:System.Configuration.ConfigurationElement" /> object that is a candidate for serialization.</param>
		/// <param name="elementName">The name of the <see cref="T:System.Configuration.ConfigurationElement" /> object as it occurs in XML.</param>
		/// <param name="targetFramework">The target version of the .NET Framework.</param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="element" /> should be serialized; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000012 RID: 18 RVA: 0x00002098 File Offset: 0x00000298
		protected internal virtual bool ShouldSerializeElementInTargetVersion(ConfigurationElement element, string elementName, FrameworkName targetFramework)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the specified property should be serialized when the configuration object hierarchy is serialized for the specified target version of the .NET Framework.</summary>
		/// <param name="property">The <see cref="T:System.Configuration.ConfigurationProperty" /> object that is a candidate for serialization.</param>
		/// <param name="propertyName">The name of the <see cref="T:System.Configuration.ConfigurationProperty" /> object as it occurs in XML.</param>
		/// <param name="targetFramework">The target version of the .NET Framework.</param>
		/// <param name="parentConfigurationElement">The parent element of the property.</param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="property" /> should be serialized; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000013 RID: 19 RVA: 0x000020B4 File Offset: 0x000002B4
		protected internal virtual bool ShouldSerializePropertyInTargetVersion(ConfigurationProperty property, string propertyName, FrameworkName targetFramework, ConfigurationElement parentConfigurationElement)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the current <see cref="T:System.Configuration.ConfigurationSection" /> instance should be serialized when the configuration object hierarchy is serialized for the specified target version of the .NET Framework.</summary>
		/// <param name="targetFramework">The target version of the .NET Framework.</param>
		/// <returns>
		///     <see langword="true" /> if the current section should be serialized; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000014 RID: 20 RVA: 0x000020D0 File Offset: 0x000002D0
		protected internal virtual bool ShouldSerializeSectionInTargetVersion(FrameworkName targetFramework)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}
	}
}
