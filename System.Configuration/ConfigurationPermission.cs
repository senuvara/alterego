﻿using System;
using System.Security;
using System.Security.Permissions;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides a permission structure that allows methods or classes to access configuration files. </summary>
	// Token: 0x02000038 RID: 56
	[Serializable]
	public sealed class ConfigurationPermission : CodeAccessPermission, IUnrestrictedPermission
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ConfigurationPermission" /> class. </summary>
		/// <param name="state">The permission level to grant.</param>
		/// <exception cref="T:System.ArgumentException">The value of <paramref name="state" /> is neither <see cref="F:System.Security.Permissions.PermissionState.Unrestricted" /> nor <see cref="F:System.Security.Permissions.PermissionState.None" />.</exception>
		// Token: 0x060001D2 RID: 466 RVA: 0x00002050 File Offset: 0x00000250
		public ConfigurationPermission(PermissionState state)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns a new <see cref="T:System.Configuration.ConfigurationPermission" /> object with the same permission level.</summary>
		/// <returns>A new <see cref="T:System.Configuration.ConfigurationPermission" /> with the same permission level.</returns>
		// Token: 0x060001D3 RID: 467 RVA: 0x00002057 File Offset: 0x00000257
		public override IPermission Copy()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Reads the value of the permission state from XML.</summary>
		/// <param name="securityElement">The configuration element from which the permission state is read.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="securityElement" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Security.SecurityElement.Tag" /> for the given <paramref name="securityElement" /> does not equal "IPermission".</exception>
		/// <exception cref="T:System.ArgumentException">The <see langword="class" /> attribute of the given <paramref name="securityElement " />equals <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">The <see langword="class" /> attribute of the given <paramref name="securityElement" /> is not the type name for <see cref="T:System.Configuration.ConfigurationPermission" />.</exception>
		/// <exception cref="T:System.ArgumentException">The <see langword="version" /> attribute for the given <paramref name="securityElement" /> does not equal 1.</exception>
		/// <exception cref="T:System.ArgumentException">The <see langword="unrestricted" /> attribute for the given <paramref name="securityElement" /> is neither <see langword="true" /> nor <see langword="false" />.</exception>
		// Token: 0x060001D4 RID: 468 RVA: 0x00002050 File Offset: 0x00000250
		public override void FromXml(SecurityElement securityElement)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns the logical intersection between the <see cref="T:System.Configuration.ConfigurationPermission" /> object and a given object that implements the <see cref="T:System.Security.IPermission" /> interface.</summary>
		/// <param name="target">The object containing the permissions to perform the intersection with.</param>
		/// <returns>The logical intersection between the <see cref="T:System.Configuration.ConfigurationPermission" /> and a given object that implements <see cref="T:System.Security.IPermission" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="target" /> is not typed as <see cref="T:System.Configuration.ConfigurationPermission" />.</exception>
		// Token: 0x060001D5 RID: 469 RVA: 0x00002057 File Offset: 0x00000257
		public override IPermission Intersect(IPermission target)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Compares the <see cref="T:System.Configuration.ConfigurationPermission" /> object with an object implementing the <see cref="T:System.Security.IPermission" /> interface.</summary>
		/// <param name="target">The object to compare to.</param>
		/// <returns>
		///     <see langword="true" /> if the permission state is equal; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="target" /> is not typed as <see cref="T:System.Configuration.ConfigurationPermission" />.</exception>
		// Token: 0x060001D6 RID: 470 RVA: 0x000029D4 File Offset: 0x00000BD4
		public override bool IsSubsetOf(IPermission target)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the permission state for the <see cref="T:System.Configuration.ConfigurationPermission" /> object is the <see cref="F:System.Security.Permissions.PermissionState.Unrestricted" /> value of the <see cref="T:System.Security.Permissions.PermissionState" /> enumeration.</summary>
		/// <returns>
		///     <see langword="true" /> if the permission state for the <see cref="T:System.Configuration.ConfigurationPermission" /> is the <see cref="F:System.Security.Permissions.PermissionState.Unrestricted" /> value of <see cref="T:System.Security.Permissions.PermissionState" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060001D7 RID: 471 RVA: 0x000029F0 File Offset: 0x00000BF0
		public bool IsUnrestricted()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a <see cref="T:System.Security.SecurityElement" /> object with attribute values based on the current <see cref="T:System.Configuration.ConfigurationPermission" /> object.</summary>
		/// <returns>A <see cref="T:System.Security.SecurityElement" /> with attribute values based on the current <see cref="T:System.Configuration.ConfigurationPermission" />.</returns>
		// Token: 0x060001D8 RID: 472 RVA: 0x00002057 File Offset: 0x00000257
		public override SecurityElement ToXml()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
