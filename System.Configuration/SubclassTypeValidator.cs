﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Validates that an object is a derived class of a specified type.</summary>
	// Token: 0x02000054 RID: 84
	public sealed class SubclassTypeValidator : ConfigurationValidatorBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SubclassTypeValidator" /> class. </summary>
		/// <param name="baseClass">The base class to validate against.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="baseClass" /> is <see langword="null" />.</exception>
		// Token: 0x06000260 RID: 608 RVA: 0x00002050 File Offset: 0x00000250
		public SubclassTypeValidator(Type baseClass)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Determines whether an object can be validated based on type.</summary>
		/// <param name="type">The object type.</param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="type" /> parameter matches a type that can be validated; otherwise, <see langword="false" />. </returns>
		// Token: 0x06000261 RID: 609 RVA: 0x00002CE4 File Offset: 0x00000EE4
		public override bool CanValidate(Type type)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Determines whether the value of an object is valid. </summary>
		/// <param name="value">The object value.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="value" /> is not of a <see cref="T:System.Type" /> that can be derived from <paramref name="baseClass" /> as defined in the constructor.</exception>
		// Token: 0x06000262 RID: 610 RVA: 0x00002050 File Offset: 0x00000250
		public override void Validate(object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
