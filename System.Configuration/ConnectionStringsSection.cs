﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides programmatic access to the connection strings configuration-file section. </summary>
	// Token: 0x02000006 RID: 6
	public sealed class ConnectionStringsSection : ConfigurationSection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ConnectionStringsSection" /> class.</summary>
		// Token: 0x06000054 RID: 84 RVA: 0x00002050 File Offset: 0x00000250
		public ConnectionStringsSection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a <see cref="T:System.Configuration.ConnectionStringSettingsCollection" /> collection of <see cref="T:System.Configuration.ConnectionStringSettings" /> objects.</summary>
		/// <returns>A <see cref="T:System.Configuration.ConnectionStringSettingsCollection" /> collection of <see cref="T:System.Configuration.ConnectionStringSettings" /> objects.</returns>
		// Token: 0x1700001E RID: 30
		// (get) Token: 0x06000055 RID: 85 RVA: 0x00002057 File Offset: 0x00000257
		public ConnectionStringSettingsCollection ConnectionStrings
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000056 RID: 86 RVA: 0x00002057 File Offset: 0x00000257
		protected internal override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x06000057 RID: 87 RVA: 0x00002057 File Offset: 0x00000257
		protected internal override object GetRuntimeObject()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
