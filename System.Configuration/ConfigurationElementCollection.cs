﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Xml;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents a configuration element containing a collection of child elements.</summary>
	// Token: 0x02000008 RID: 8
	[DebuggerDisplay("Count = {Count}")]
	public abstract class ConfigurationElementCollection : ConfigurationElement, ICollection, IEnumerable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ConfigurationElementCollection" /> class.</summary>
		// Token: 0x06000066 RID: 102 RVA: 0x00002050 File Offset: 0x00000250
		protected ConfigurationElementCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Configuration.ConfigurationElementCollection" /> class.</summary>
		/// <param name="comparer">The <see cref="T:System.Collections.IComparer" /> comparer to use.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="comparer" /> is <see langword="null" />.</exception>
		// Token: 0x06000067 RID: 103 RVA: 0x00002050 File Offset: 0x00000250
		protected ConfigurationElementCollection(IComparer comparer)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the name of the <see cref="T:System.Configuration.ConfigurationElement" /> to associate with the add operation in the <see cref="T:System.Configuration.ConfigurationElementCollection" /> when overridden in a derived class. </summary>
		/// <returns>The name of the element.</returns>
		/// <exception cref="T:System.ArgumentException">The selected value starts with the reserved prefix "config" or "lock".</exception>
		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000068 RID: 104 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x06000069 RID: 105 RVA: 0x00002050 File Offset: 0x00000250
		protected internal string AddElementName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name for the <see cref="T:System.Configuration.ConfigurationElement" /> to associate with the clear operation in the <see cref="T:System.Configuration.ConfigurationElementCollection" /> when overridden in a derived class. </summary>
		/// <returns>The name of the element.</returns>
		/// <exception cref="T:System.ArgumentException">The selected value starts with the reserved prefix "config" or "lock".</exception>
		// Token: 0x17000023 RID: 35
		// (get) Token: 0x0600006A RID: 106 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x0600006B RID: 107 RVA: 0x00002050 File Offset: 0x00000250
		protected internal string ClearElementName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the type of the <see cref="T:System.Configuration.ConfigurationElementCollection" />.</summary>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationElementCollectionType" /> of this collection.</returns>
		// Token: 0x17000024 RID: 36
		// (get) Token: 0x0600006C RID: 108 RVA: 0x00002228 File Offset: 0x00000428
		public virtual ConfigurationElementCollectionType CollectionType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ConfigurationElementCollectionType.BasicMap;
			}
		}

		/// <summary>Gets the number of elements in the collection.</summary>
		/// <returns>The number of elements in the collection.</returns>
		// Token: 0x17000025 RID: 37
		// (get) Token: 0x0600006D RID: 109 RVA: 0x00002244 File Offset: 0x00000444
		public int Count
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the name used to identify this collection of elements in the configuration file when overridden in a derived class.</summary>
		/// <returns>The name of the collection; otherwise, an empty string. The default is an empty string.</returns>
		// Token: 0x17000026 RID: 38
		// (get) Token: 0x0600006E RID: 110 RVA: 0x00002057 File Offset: 0x00000257
		protected virtual string ElementName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets a value that specifies whether the collection has been cleared.</summary>
		/// <returns>
		///     <see langword="true" /> if the collection has been cleared; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The configuration is read-only.</exception>
		// Token: 0x17000027 RID: 39
		// (get) Token: 0x0600006F RID: 111 RVA: 0x00002260 File Offset: 0x00000460
		// (set) Token: 0x06000070 RID: 112 RVA: 0x00002050 File Offset: 0x00000250
		public bool EmitClear
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a value indicating whether access to the collection is synchronized.</summary>
		/// <returns>
		///     <see langword="true" /> if access to the <see cref="T:System.Configuration.ConfigurationElementCollection" /> is synchronized; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000028 RID: 40
		// (get) Token: 0x06000071 RID: 113 RVA: 0x0000227C File Offset: 0x0000047C
		public bool IsSynchronized
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets or sets the name of the <see cref="T:System.Configuration.ConfigurationElement" /> to associate with the remove operation in the <see cref="T:System.Configuration.ConfigurationElementCollection" /> when overridden in a derived class. </summary>
		/// <returns>The name of the element.</returns>
		/// <exception cref="T:System.ArgumentException">The selected value starts with the reserved prefix "config" or "lock".</exception>
		// Token: 0x17000029 RID: 41
		// (get) Token: 0x06000072 RID: 114 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x06000073 RID: 115 RVA: 0x00002050 File Offset: 0x00000250
		protected internal string RemoveElementName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets an object used to synchronize access to the <see cref="T:System.Configuration.ConfigurationElementCollection" />.</summary>
		/// <returns>An object used to synchronize access to the <see cref="T:System.Configuration.ConfigurationElementCollection" />.</returns>
		// Token: 0x1700002A RID: 42
		// (get) Token: 0x06000074 RID: 116 RVA: 0x00002057 File Offset: 0x00000257
		public object SyncRoot
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a value indicating whether an attempt to add a duplicate <see cref="T:System.Configuration.ConfigurationElement" /> to the <see cref="T:System.Configuration.ConfigurationElementCollection" /> will cause an exception to be thrown.</summary>
		/// <returns>
		///     <see langword="true" /> if an attempt to add a duplicate <see cref="T:System.Configuration.ConfigurationElement" /> to this <see cref="T:System.Configuration.ConfigurationElementCollection" /> will cause an exception to be thrown; otherwise, <see langword="false" />. </returns>
		// Token: 0x1700002B RID: 43
		// (get) Token: 0x06000075 RID: 117 RVA: 0x00002298 File Offset: 0x00000498
		protected virtual bool ThrowOnDuplicate
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Adds a configuration element to the <see cref="T:System.Configuration.ConfigurationElementCollection" />.</summary>
		/// <param name="element">The <see cref="T:System.Configuration.ConfigurationElement" /> to add.</param>
		// Token: 0x06000076 RID: 118 RVA: 0x00002050 File Offset: 0x00000250
		protected virtual void BaseAdd(ConfigurationElement element)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a configuration element to the configuration element collection.</summary>
		/// <param name="element">The <see cref="T:System.Configuration.ConfigurationElement" /> to add.</param>
		/// <param name="throwIfExists">
		///       <see langword="true" /> to throw an exception if the <see cref="T:System.Configuration.ConfigurationElement" /> specified is already contained in the <see cref="T:System.Configuration.ConfigurationElementCollection" />; otherwise, <see langword="false" />. </param>
		/// <exception cref="T:System.Exception">The <see cref="T:System.Configuration.ConfigurationElement" /> to add already exists in the <see cref="T:System.Configuration.ConfigurationElementCollection" /> and the <paramref name="throwIfExists" /> parameter is <see langword="true" />. </exception>
		// Token: 0x06000077 RID: 119 RVA: 0x00002050 File Offset: 0x00000250
		protected internal void BaseAdd(ConfigurationElement element, bool throwIfExists)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds a configuration element to the configuration element collection.</summary>
		/// <param name="index">The index location at which to add the specified <see cref="T:System.Configuration.ConfigurationElement" />. </param>
		/// <param name="element">The <see cref="T:System.Configuration.ConfigurationElement" /> to add. </param>
		// Token: 0x06000078 RID: 120 RVA: 0x00002050 File Offset: 0x00000250
		protected virtual void BaseAdd(int index, ConfigurationElement element)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes all configuration element objects from the collection.</summary>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The configuration is read-only.- or -A collection item has been locked in a higher-level configuration.</exception>
		// Token: 0x06000079 RID: 121 RVA: 0x00002050 File Offset: 0x00000250
		protected internal void BaseClear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the configuration element at the specified index location.</summary>
		/// <param name="index">The index location of the <see cref="T:System.Configuration.ConfigurationElement" /> to return. </param>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationElement" /> at the specified index.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">
		///         <paramref name="index" /> is less than <see langword="0" />.- or -There is no <see cref="T:System.Configuration.ConfigurationElement" /> at the specified <paramref name="index" />.</exception>
		// Token: 0x0600007A RID: 122 RVA: 0x00002057 File Offset: 0x00000257
		protected internal ConfigurationElement BaseGet(int index)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the configuration element with the specified key.</summary>
		/// <param name="key">The key of the element to return. </param>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationElement" /> with the specified key; otherwise, <see langword="null" />.</returns>
		// Token: 0x0600007B RID: 123 RVA: 0x00002057 File Offset: 0x00000257
		protected internal ConfigurationElement BaseGet(object key)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns an array of the keys for all of the configuration elements contained in the <see cref="T:System.Configuration.ConfigurationElementCollection" />.</summary>
		/// <returns>An array that contains the keys for all of the <see cref="T:System.Configuration.ConfigurationElement" /> objects contained in the <see cref="T:System.Configuration.ConfigurationElementCollection" />.</returns>
		// Token: 0x0600007C RID: 124 RVA: 0x00002057 File Offset: 0x00000257
		protected internal object[] BaseGetAllKeys()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the key for the <see cref="T:System.Configuration.ConfigurationElement" /> at the specified index location.</summary>
		/// <param name="index">The index location for the <see cref="T:System.Configuration.ConfigurationElement" />.</param>
		/// <returns>The key for the specified <see cref="T:System.Configuration.ConfigurationElement" />.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">
		///         <paramref name="index" /> is less than <see langword="0" />.- or -There is no <see cref="T:System.Configuration.ConfigurationElement" /> at the specified <paramref name="index" />.</exception>
		// Token: 0x0600007D RID: 125 RVA: 0x00002057 File Offset: 0x00000257
		protected internal object BaseGetKey(int index)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Indicates the index of the specified <see cref="T:System.Configuration.ConfigurationElement" />.</summary>
		/// <param name="element">The <see cref="T:System.Configuration.ConfigurationElement" /> for the specified index location.</param>
		/// <returns>The index of the specified <see cref="T:System.Configuration.ConfigurationElement" />; otherwise, -1.</returns>
		// Token: 0x0600007E RID: 126 RVA: 0x000022B4 File Offset: 0x000004B4
		protected int BaseIndexOf(ConfigurationElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Indicates whether the <see cref="T:System.Configuration.ConfigurationElement" /> with the specified key has been removed from the <see cref="T:System.Configuration.ConfigurationElementCollection" />.</summary>
		/// <param name="key">The key of the element to check.</param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Configuration.ConfigurationElement" /> with the specified key has been removed; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x0600007F RID: 127 RVA: 0x000022D0 File Offset: 0x000004D0
		protected internal bool BaseIsRemoved(object key)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Removes a <see cref="T:System.Configuration.ConfigurationElement" /> from the collection.</summary>
		/// <param name="key">The key of the <see cref="T:System.Configuration.ConfigurationElement" /> to remove.</param>
		/// <exception cref="T:System.Exception">No <see cref="T:System.Configuration.ConfigurationElement" /> with the specified key exists in the collection, the element has already been removed, or the element cannot be removed because the value of its <see cref="P:System.Configuration.ConfigurationProperty.Type" /> is not <see cref="F:System.Configuration.ConfigurationElementCollectionType.AddRemoveClearMap" />. </exception>
		// Token: 0x06000080 RID: 128 RVA: 0x00002050 File Offset: 0x00000250
		protected internal void BaseRemove(object key)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the <see cref="T:System.Configuration.ConfigurationElement" /> at the specified index location.</summary>
		/// <param name="index">The index location of the <see cref="T:System.Configuration.ConfigurationElement" /> to remove.</param>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The configuration is read-only.- or -
		///         <paramref name="index" /> is less than <see langword="0" /> or greater than the number of <see cref="T:System.Configuration.ConfigurationElement" /> objects in the collection.- or -The <see cref="T:System.Configuration.ConfigurationElement" /> object has already been removed.- or -The value of the <see cref="T:System.Configuration.ConfigurationElement" /> object has been locked at a higher level.- or -The <see cref="T:System.Configuration.ConfigurationElement" /> object was inherited.- or -The value of the <see cref="T:System.Configuration.ConfigurationElement" /> object's <see cref="P:System.Configuration.ConfigurationProperty.Type" /> is not <see cref="F:System.Configuration.ConfigurationElementCollectionType.AddRemoveClearMap" /> or <see cref="F:System.Configuration.ConfigurationElementCollectionType.AddRemoveClearMapAlternate" />.</exception>
		// Token: 0x06000081 RID: 129 RVA: 0x00002050 File Offset: 0x00000250
		protected internal void BaseRemoveAt(int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Copies the contents of the <see cref="T:System.Configuration.ConfigurationElementCollection" /> to an array.</summary>
		/// <param name="array">Array to which to copy the contents of the <see cref="T:System.Configuration.ConfigurationElementCollection" />.</param>
		/// <param name="index">Index location at which to begin copying.</param>
		// Token: 0x06000082 RID: 130 RVA: 0x00002050 File Offset: 0x00000250
		public void CopyTo(ConfigurationElement[] array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>When overridden in a derived class, creates a new <see cref="T:System.Configuration.ConfigurationElement" />.</summary>
		/// <returns>A newly created <see cref="T:System.Configuration.ConfigurationElement" />.</returns>
		// Token: 0x06000083 RID: 131
		protected abstract ConfigurationElement CreateNewElement();

		/// <summary>Creates a new <see cref="T:System.Configuration.ConfigurationElement" /> when overridden in a derived class.</summary>
		/// <param name="elementName">The name of the <see cref="T:System.Configuration.ConfigurationElement" /> to create. </param>
		/// <returns>A new <see cref="T:System.Configuration.ConfigurationElement" /> with a specified name.</returns>
		// Token: 0x06000084 RID: 132 RVA: 0x00002057 File Offset: 0x00000257
		protected virtual ConfigurationElement CreateNewElement(string elementName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the element key for a specified configuration element when overridden in a derived class.</summary>
		/// <param name="element">The <see cref="T:System.Configuration.ConfigurationElement" /> to return the key for. </param>
		/// <returns>An <see cref="T:System.Object" /> that acts as the key for the specified <see cref="T:System.Configuration.ConfigurationElement" />.</returns>
		// Token: 0x06000085 RID: 133
		protected abstract object GetElementKey(ConfigurationElement element);

		/// <summary>Gets an <see cref="T:System.Collections.IEnumerator" /> which is used to iterate through the <see cref="T:System.Configuration.ConfigurationElementCollection" />.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> which is used to iterate through the <see cref="T:System.Configuration.ConfigurationElementCollection" />.</returns>
		// Token: 0x06000086 RID: 134 RVA: 0x00002057 File Offset: 0x00000257
		public IEnumerator GetEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Indicates whether the specified <see cref="T:System.Configuration.ConfigurationElement" /> exists in the <see cref="T:System.Configuration.ConfigurationElementCollection" />.</summary>
		/// <param name="elementName">The name of the element to verify. </param>
		/// <returns>
		///     <see langword="true" /> if the element exists in the collection; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x06000087 RID: 135 RVA: 0x000022EC File Offset: 0x000004EC
		protected virtual bool IsElementName(string elementName)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the specified <see cref="T:System.Configuration.ConfigurationElement" /> can be removed from the <see cref="T:System.Configuration.ConfigurationElementCollection" />.</summary>
		/// <param name="element">The element to check.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.Configuration.ConfigurationElement" /> can be removed from this <see cref="T:System.Configuration.ConfigurationElementCollection" />; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x06000088 RID: 136 RVA: 0x00002308 File Offset: 0x00000508
		protected virtual bool IsElementRemovable(ConfigurationElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether this <see cref="T:System.Configuration.ConfigurationElementCollection" /> has been modified since it was last saved or loaded when overridden in a derived class.</summary>
		/// <returns>
		///     <see langword="true" /> if any contained element has been modified; otherwise, <see langword="false" /></returns>
		// Token: 0x06000089 RID: 137 RVA: 0x00002324 File Offset: 0x00000524
		protected internal override bool IsModified()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the <see cref="T:System.Configuration.ConfigurationElementCollection" /> object is read only.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Configuration.ConfigurationElementCollection" /> object is read only; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600008A RID: 138 RVA: 0x00002340 File Offset: 0x00000540
		public override bool IsReadOnly()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Causes the configuration system to throw an exception.</summary>
		/// <param name="elementName">The name of the unrecognized element.</param>
		/// <param name="reader">An input stream that reads XML from the configuration file. </param>
		/// <returns>
		///     <see langword="true" /> if the unrecognized element was deserialized successfully; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The element specified in <paramref name="elementName" /> is the <see langword="&lt;clear&gt;" /> element.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="elementName" /> starts with the reserved prefix "config" or "lock".</exception>
		// Token: 0x0600008B RID: 139 RVA: 0x0000235C File Offset: 0x0000055C
		protected override bool OnDeserializeUnrecognizedElement(string elementName, XmlReader reader)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Resets the <see cref="T:System.Configuration.ConfigurationElementCollection" /> to its unmodified state when overridden in a derived class.</summary>
		/// <param name="parentElement">The <see cref="T:System.Configuration.ConfigurationElement" /> representing the collection parent element, if any; otherwise, <see langword="null" />. </param>
		// Token: 0x0600008C RID: 140 RVA: 0x00002050 File Offset: 0x00000250
		protected internal override void Reset(ConfigurationElement parentElement)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Resets the value of the <see cref="M:System.Configuration.ConfigurationElementCollection.IsModified" /> property to <see langword="false" /> when overridden in a derived class.</summary>
		// Token: 0x0600008D RID: 141 RVA: 0x00002050 File Offset: 0x00000250
		protected internal override void ResetModified()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Writes the configuration data to an XML element in the configuration file when overridden in a derived class.</summary>
		/// <param name="writer">Output stream that writes XML to the configuration file.</param>
		/// <param name="serializeCollectionKey">
		///       <see langword="true" /> to serialize the collection key; otherwise, <see langword="false" />.</param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Configuration.ConfigurationElementCollection" /> was written to the configuration file successfully.</returns>
		/// <exception cref="T:System.ArgumentException">One of the elements in the collection was added or replaced and starts with the reserved prefix "config" or "lock".</exception>
		// Token: 0x0600008E RID: 142 RVA: 0x00002378 File Offset: 0x00000578
		protected internal override bool SerializeElement(XmlWriter writer, bool serializeCollectionKey)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Sets the <see cref="M:System.Configuration.ConfigurationElementCollection.IsReadOnly" /> property for the <see cref="T:System.Configuration.ConfigurationElementCollection" /> object and for all sub-elements.</summary>
		// Token: 0x0600008F RID: 143 RVA: 0x00002050 File Offset: 0x00000250
		protected internal override void SetReadOnly()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Copies the <see cref="T:System.Configuration.ConfigurationElementCollection" /> to an array.</summary>
		/// <param name="arr">Array to which to copy this <see cref="T:System.Configuration.ConfigurationElementCollection" />.</param>
		/// <param name="index">Index location at which to begin copying.</param>
		// Token: 0x06000090 RID: 144 RVA: 0x00002050 File Offset: 0x00000250
		void ICollection.CopyTo(Array arr, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Reverses the effect of merging configuration information from different levels of the configuration hierarchy </summary>
		/// <param name="sourceElement">A <see cref="T:System.Configuration.ConfigurationElement" /> object at the current level containing a merged view of the properties.</param>
		/// <param name="parentElement">The parent <see cref="T:System.Configuration.ConfigurationElement" /> object of the current element, or <see langword="null" /> if this is the top level.</param>
		/// <param name="saveMode">A <see cref="T:System.Configuration.ConfigurationSaveMode" /> enumerated value that determines which property values to include.</param>
		// Token: 0x06000091 RID: 145 RVA: 0x00002050 File Offset: 0x00000250
		protected internal override void Unmerge(ConfigurationElement sourceElement, ConfigurationElement parentElement, ConfigurationSaveMode saveMode)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
