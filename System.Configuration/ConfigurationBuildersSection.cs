﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides programmatic access to the <see langword="&lt;configBuilders&gt;" /> section. This class can't be inherited.</summary>
	// Token: 0x02000032 RID: 50
	public sealed class ConfigurationBuildersSection : ConfigurationSection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ConfigurationBuildersSection" /> class.</summary>
		// Token: 0x060001A4 RID: 420 RVA: 0x00002050 File Offset: 0x00000250
		public ConfigurationBuildersSection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a <see cref="T:System.Configuration.ConfigurationBuilderCollection" /> of all <see cref="T:System.Configuration.ConfigurationBuilder" /> objects in all participating configuration files.</summary>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationBuilder" /> objects in all participating configuration files.</returns>
		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x060001A5 RID: 421 RVA: 0x00002057 File Offset: 0x00000257
		public ProviderSettingsCollection Builders
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x060001A6 RID: 422 RVA: 0x00002057 File Offset: 0x00000257
		protected internal override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Returns a <see cref="T:System.Configuration.ConfigurationBuilder" /> object that has the provided configuration builder name.</summary>
		/// <param name="builderName">A configuration builder name or a comma-separated list of names. If <paramref name="builderName" /> is a comma-separated list of <see cref="T:System.Configuration.ConfigurationBuilder" /> names, a special aggregate <see cref="T:System.Configuration.ConfigurationBuilder" /> object that references and applies all named configuration builders is returned.</param>
		/// <returns>A <see cref="T:System.Configuration.ConfigurationBuilder" /> object that has the provided configuration <paramref name="builderName" />.</returns>
		/// <exception cref="T:System.Exception">The <see cref="T:System.Configuration.ConfigurationBuilder" /> object from <paramref name="builderName" /> wasn't found.</exception>
		/// <exception cref="T:System.Exception">The type specified doesn't extend the <see cref="T:System.Configuration.ConfigurationBuilder" /> class.</exception>
		/// <exception cref="T:System.Exception">A configuration provider type can't be instantiated under a partially trusted security policy (<see cref="T:System.Security.AllowPartiallyTrustedCallersAttribute" /> is not present on the target assembly).</exception>
		/// <exception cref="T:System.IO.FileNotFoundException">ConfigurationBuilders.IgnoreLoadFailure is disabled by default. If a bin-deployed configuration builder can't be found or instantiated for one of the sections read from the configuration file, a <see cref="T:System.IO.FileNotFoundException" /> is trapped and reported. If you wish to ignore load failures, enable ConfigurationBuilders.IgnoreLoadFailure.</exception>
		/// <exception cref="T:System.TypeLoadException">ConfigurationBuilders.IgnoreLoadFailure is disabled by default. While loading a configuration builder if a <see cref="T:System.TypeLoadException" /> occurs for one of the sections read from the configuration file, a <see cref="T:System.TypeLoadException" /> is trapped and reported. If you wish to ignore load failures, enable ConfigurationBuilders.IgnoreLoadFailure.</exception>
		// Token: 0x060001A7 RID: 423 RVA: 0x00002057 File Offset: 0x00000257
		public ConfigurationBuilder GetBuilderFromName(string builderName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
