﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Declaratively instructs the .NET Framework to perform integer validation on a configuration property. This class cannot be inherited.</summary>
	// Token: 0x02000044 RID: 68
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class IntegerValidatorAttribute : ConfigurationValidatorAttribute
	{
		/// <summary>Creates a new instance of the <see cref="T:System.Configuration.IntegerValidatorAttribute" /> class.</summary>
		// Token: 0x06000208 RID: 520 RVA: 0x000024C7 File Offset: 0x000006C7
		public IntegerValidatorAttribute()
		{
		}

		/// <summary>Gets or sets a value that indicates whether to include or exclude the integers in the range defined by the <see cref="P:System.Configuration.IntegerValidatorAttribute.MinValue" /> and <see cref="P:System.Configuration.IntegerValidatorAttribute.MaxValue" /> property values.</summary>
		/// <returns>
		///     <see langword="true" /> if the value must be excluded; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x06000209 RID: 521 RVA: 0x00002B24 File Offset: 0x00000D24
		// (set) Token: 0x0600020A RID: 522 RVA: 0x00002050 File Offset: 0x00000250
		public bool ExcludeRange
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the maximum value allowed for the property.</summary>
		/// <returns>An integer that indicates the allowed maximum value.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The selected value is less than <see cref="P:System.Configuration.IntegerValidatorAttribute.MinValue" />.</exception>
		// Token: 0x170000BA RID: 186
		// (get) Token: 0x0600020B RID: 523 RVA: 0x00002B40 File Offset: 0x00000D40
		// (set) Token: 0x0600020C RID: 524 RVA: 0x00002050 File Offset: 0x00000250
		public int MaxValue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the minimum value allowed for the property.</summary>
		/// <returns>An integer that indicates the allowed minimum value.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The selected value is greater than <see cref="P:System.Configuration.IntegerValidatorAttribute.MaxValue" />.</exception>
		// Token: 0x170000BB RID: 187
		// (get) Token: 0x0600020D RID: 525 RVA: 0x00002B5C File Offset: 0x00000D5C
		// (set) Token: 0x0600020E RID: 526 RVA: 0x00002050 File Offset: 0x00000250
		public int MinValue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets an instance of the <see cref="T:System.Configuration.IntegerValidator" /> class.</summary>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationValidatorBase" /> validator instance.</returns>
		// Token: 0x170000BC RID: 188
		// (get) Token: 0x0600020F RID: 527 RVA: 0x00002057 File Offset: 0x00000257
		public override ConfigurationValidatorBase ValidatorInstance
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
