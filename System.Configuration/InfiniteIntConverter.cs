﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Converts between a string and the standard infinite or integer value.</summary>
	// Token: 0x02000041 RID: 65
	public sealed class InfiniteIntConverter : ConfigurationConverterBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.InfiniteIntConverter" /> class.</summary>
		// Token: 0x06000201 RID: 513 RVA: 0x00002050 File Offset: 0x00000250
		public InfiniteIntConverter()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
