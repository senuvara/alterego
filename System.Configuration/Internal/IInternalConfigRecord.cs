﻿using System;
using System.Runtime.InteropServices;

namespace System.Configuration.Internal
{
	/// <summary>Defines interfaces used by internal .NET structures to support creation of new configuration records.</summary>
	// Token: 0x02000062 RID: 98
	[ComVisible(false)]
	public interface IInternalConfigRecord
	{
		/// <summary>Gets a string representing a configuration file path.</summary>
		/// <returns>A string representing a configuration file path.</returns>
		// Token: 0x170000F0 RID: 240
		// (get) Token: 0x060002D9 RID: 729
		string ConfigPath { get; }

		/// <summary>Returns a value indicating whether an error occurred during initialization of a configuration object.</summary>
		/// <returns>
		///     <see langword="true" /> if an error occurred during initialization of a configuration object; otherwise, <see langword="false" />.</returns>
		// Token: 0x170000F1 RID: 241
		// (get) Token: 0x060002DA RID: 730
		bool HasInitErrors { get; }

		/// <summary>Returns the name of a <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file.</summary>
		/// <returns>A string representing the name of a <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file.</returns>
		// Token: 0x170000F2 RID: 242
		// (get) Token: 0x060002DB RID: 731
		string StreamName { get; }

		/// <summary>Returns an object representing a section of a configuration from the last-known-good (LKG) configuration.</summary>
		/// <param name="configKey">A string representing a key to a configuration section.</param>
		/// <returns>An <see cref="T:System.Object" /> instance representing the section of the last-known-good configuration specified by <paramref name="configKey" />.</returns>
		// Token: 0x060002DC RID: 732
		object GetLkgSection(string configKey);

		/// <summary>Returns an <see cref="T:System.Object" /> instance representing a section of a configuration file.</summary>
		/// <param name="configKey">A string representing a key to a configuration section.</param>
		/// <returns>An <see cref="T:System.Object" /> instance representing a section of a configuration file.</returns>
		// Token: 0x060002DD RID: 733
		object GetSection(string configKey);

		/// <summary>Causes a specified section of the configuration object to be reinitialized.</summary>
		/// <param name="configKey">A string representing a key to a configuration section that is to be refreshed.</param>
		// Token: 0x060002DE RID: 734
		void RefreshSection(string configKey);

		/// <summary>Removes a configuration record.</summary>
		// Token: 0x060002DF RID: 735
		void Remove();

		/// <summary>Grants the configuration object the permission to throw an exception if an error occurs during initialization.</summary>
		// Token: 0x060002E0 RID: 736
		void ThrowIfInitErrors();
	}
}
