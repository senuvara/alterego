﻿using System;

namespace System.Configuration.Internal
{
	/// <summary>Defines an interface used by the .NET Framework to support creating error configuration records.</summary>
	// Token: 0x02000067 RID: 103
	public interface IConfigErrorInfo
	{
		/// <summary>Gets a string specifying the file name related to the configuration details.</summary>
		/// <returns>A string specifying a filename.</returns>
		// Token: 0x170000F5 RID: 245
		// (get) Token: 0x060002F7 RID: 759
		string Filename { get; }

		/// <summary>Gets an integer specifying the line number related to the configuration details.</summary>
		/// <returns>An integer specifying a line number.</returns>
		// Token: 0x170000F6 RID: 246
		// (get) Token: 0x060002F8 RID: 760
		int LineNumber { get; }
	}
}
