﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;

namespace System.Configuration.Internal
{
	/// <summary>Defines interfaces used by internal .NET structures to initialize application configuration properties.</summary>
	// Token: 0x02000061 RID: 97
	[ComVisible(false)]
	public interface IInternalConfigHost
	{
		/// <summary>Returns a value indicating whether the configuration is remote.</summary>
		/// <returns>
		///     <see langword="true" /> if the configuration is remote; otherwise, <see langword="false" />.</returns>
		// Token: 0x170000EB RID: 235
		// (get) Token: 0x060002B0 RID: 688
		bool IsRemote { get; }

		/// <summary>Returns a value indicating whether the host configuration supports change notification.</summary>
		/// <returns>
		///     <see langword="true" /> if the configuration supports change notification; otherwise, <see langword="false" />.</returns>
		// Token: 0x170000EC RID: 236
		// (get) Token: 0x060002B1 RID: 689
		bool SupportsChangeNotifications { get; }

		/// <summary>Returns a value indicating whether the host configuration supports location tags.</summary>
		/// <returns>
		///     <see langword="true" /> if the configuration supports location tags; otherwise, <see langword="false" />.</returns>
		// Token: 0x170000ED RID: 237
		// (get) Token: 0x060002B2 RID: 690
		bool SupportsLocation { get; }

		/// <summary>Returns a value indicating whether the host configuration supports path tags.</summary>
		/// <returns>
		///     <see langword="true" /> if the configuration supports path tags; otherwise, <see langword="false" />.</returns>
		// Token: 0x170000EE RID: 238
		// (get) Token: 0x060002B3 RID: 691
		bool SupportsPath { get; }

		/// <summary>Returns a value indicating whether the host configuration supports configuration refresh.</summary>
		/// <returns>
		///     <see langword="true" /> if the configuration supports configuration refresh; otherwise, <see langword="false" />.</returns>
		// Token: 0x170000EF RID: 239
		// (get) Token: 0x060002B4 RID: 692
		bool SupportsRefresh { get; }

		/// <summary>Creates and returns a context object for a <see cref="T:System.Configuration.ConfigurationElement" /> of an application configuration.</summary>
		/// <param name="configPath">A string representing the path of the application configuration file.</param>
		/// <param name="locationSubPath">A string representing a subpath location of the configuration element.</param>
		/// <returns>A context object for a <see cref="T:System.Configuration.ConfigurationElement" /> object of an application configuration.</returns>
		// Token: 0x060002B5 RID: 693
		object CreateConfigurationContext(string configPath, string locationSubPath);

		/// <summary>Creates and returns a deprecated context object of the application configuration.</summary>
		/// <param name="configPath">A string representing a path to an application configuration file.</param>
		/// <returns>A deprecated context object of the application configuration.</returns>
		// Token: 0x060002B6 RID: 694
		object CreateDeprecatedConfigContext(string configPath);

		/// <summary>Decrypts an encrypted configuration section and returns it as a string.</summary>
		/// <param name="encryptedXml">An encrypted XML string representing a configuration section.</param>
		/// <param name="protectionProvider">The <see cref="T:System.Configuration.ProtectedConfigurationProvider" /> object.</param>
		/// <param name="protectedConfigSection">The <see cref="T:System.Configuration.ProtectedConfigurationSection" /> object.</param>
		/// <returns>A decrypted configuration section as a string.</returns>
		// Token: 0x060002B7 RID: 695
		string DecryptSection(string encryptedXml, ProtectedConfigurationProvider protectionProvider, ProtectedConfigurationSection protectedConfigSection);

		/// <summary>Deletes the <see cref="T:System.IO.Stream" /> object performing I/O tasks on the application configuration file.</summary>
		/// <param name="streamName">A string representing the name of the <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file.</param>
		// Token: 0x060002B8 RID: 696
		void DeleteStream(string streamName);

		/// <summary>Encrypts a configuration section and returns it as a string.</summary>
		/// <param name="clearTextXml">An XML string representing a configuration section to encrypt.</param>
		/// <param name="protectionProvider">The <see cref="T:System.Configuration.ProtectedConfigurationProvider" /> object.</param>
		/// <param name="protectedConfigSection">The <see cref="T:System.Configuration.ProtectedConfigurationSection" /> object.</param>
		/// <returns>An encrypted configuration section represented as a string.</returns>
		// Token: 0x060002B9 RID: 697
		string EncryptSection(string clearTextXml, ProtectedConfigurationProvider protectionProvider, ProtectedConfigurationSection protectedConfigSection);

		/// <summary>Returns the complete path to an application configuration file based on the location subpath.</summary>
		/// <param name="configPath">A string representing the path of the application configuration file.</param>
		/// <param name="locationSubPath">The subpath location of the configuration file.</param>
		/// <returns>A string representing the complete path to an application configuration file.</returns>
		// Token: 0x060002BA RID: 698
		string GetConfigPathFromLocationSubPath(string configPath, string locationSubPath);

		/// <summary>Returns a <see cref="T:System.Type" /> object representing the type of the configuration object.</summary>
		/// <param name="typeName">The type name</param>
		/// <param name="throwOnError">
		///       <see langword="true" /> to throw an exception if an error occurs; otherwise, <see langword="false" /></param>
		/// <returns>A <see cref="T:System.Type" /> object representing the type of the configuration object.</returns>
		// Token: 0x060002BB RID: 699
		Type GetConfigType(string typeName, bool throwOnError);

		/// <summary>Returns a string representing a type name from the <see cref="T:System.Type" /> object representing the type of the configuration.</summary>
		/// <param name="t">A <see cref="T:System.Type" /> object.</param>
		/// <returns>A string representing the type name from a <see cref="T:System.Type" /> object representing the type of the configuration.</returns>
		// Token: 0x060002BC RID: 700
		string GetConfigTypeName(Type t);

		/// <summary>Associates the configuration with a <see cref="T:System.Security.PermissionSet" /> object.</summary>
		/// <param name="configRecord">An <see cref="T:System.Configuration.Internal.IInternalConfigRecord" /> object.</param>
		/// <param name="permissionSet">The <see cref="T:System.Security.PermissionSet" /> object to associate with the configuration.</param>
		/// <param name="isHostReady">
		///       <see langword="true" /> to indicate the configuration host is has completed building associated permissions; otherwise, <see langword="false" />.</param>
		// Token: 0x060002BD RID: 701
		void GetRestrictedPermissions(IInternalConfigRecord configRecord, out PermissionSet permissionSet, out bool isHostReady);

		/// <summary>Returns a string representing the configuration file name associated with the <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file.</summary>
		/// <param name="configPath">A string representing the path of the application configuration file.</param>
		/// <returns>A string representing the configuration file name associated with the <see cref="T:System.IO.Stream" /> I/O tasks on the configuration file.</returns>
		// Token: 0x060002BE RID: 702
		string GetStreamName(string configPath);

		/// <summary>Returns a string representing the configuration file name associated with the <see cref="T:System.IO.Stream" /> object performing I/O tasks on a remote configuration file.</summary>
		/// <param name="streamName">A string representing the configuration file name associated with the <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file.</param>
		/// <param name="configSource">A string representing a path to a remote configuration file.</param>
		/// <returns>A string representing the configuration file name associated with the <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file.</returns>
		// Token: 0x060002BF RID: 703
		string GetStreamNameForConfigSource(string streamName, string configSource);

		/// <summary>Returns the version of the <see cref="T:System.IO.Stream" /> object associated with configuration file.</summary>
		/// <param name="streamName">A string representing the name of the <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file.</param>
		/// <returns>The version of the <see cref="T:System.IO.Stream" /> object associated with configuration file.</returns>
		// Token: 0x060002C0 RID: 704
		object GetStreamVersion(string streamName);

		/// <summary>Instructs the host to impersonate and returns an <see cref="T:System.IDisposable" /> object required by the internal .NET structure.</summary>
		/// <returns>An <see cref="T:System.IDisposable" /> value.</returns>
		// Token: 0x060002C1 RID: 705
		IDisposable Impersonate();

		/// <summary>Initializes a configuration host.</summary>
		/// <param name="configRoot">The configuration root object.</param>
		/// <param name="hostInitParams">The parameter object containing the values used for initializing the configuration host.</param>
		// Token: 0x060002C2 RID: 706
		void Init(IInternalConfigRoot configRoot, object[] hostInitParams);

		/// <summary>Initializes a configuration object.</summary>
		/// <param name="locationSubPath">The subpath location of the configuration file.</param>
		/// <param name="configPath">A string representing the path of the application configuration file.</param>
		/// <param name="locationConfigPath">A string representing the location of a configuration path.</param>
		/// <param name="configRoot">The <see cref="T:System.Configuration.Internal.IInternalConfigRoot" /> object.</param>
		/// <param name="hostInitConfigurationParams">The parameter object containing the values used for initializing the configuration host.</param>
		// Token: 0x060002C3 RID: 707
		void InitForConfiguration(ref string locationSubPath, out string configPath, out string locationConfigPath, IInternalConfigRoot configRoot, object[] hostInitConfigurationParams);

		/// <summary>Returns a value indicating whether the configuration file is located at a higher level in the configuration hierarchy than the application configuration.</summary>
		/// <param name="configPath">A string representing the path of the application configuration file.</param>
		/// <returns>
		///     <see langword="true" /> the configuration file is located at a higher level in the configuration hierarchy than the application configuration; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002C4 RID: 708
		bool IsAboveApplication(string configPath);

		/// <summary>Returns a value indicating whether a child record is required for a child configuration path.</summary>
		/// <param name="configPath">A string representing the path of the application configuration file.</param>
		/// <returns>
		///     <see langword="true" /> if child record is required for a child configuration path; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002C5 RID: 709
		bool IsConfigRecordRequired(string configPath);

		/// <summary>Determines if a different <see cref="T:System.Type" /> definition is allowable for an application configuration object.</summary>
		/// <param name="configPath">A string representing the path of the application configuration file.</param>
		/// <param name="allowDefinition">A <see cref="T:System.Configuration.ConfigurationAllowDefinition" /> object.</param>
		/// <param name="allowExeDefinition">A <see cref="T:System.Configuration.ConfigurationAllowExeDefinition" /> object.</param>
		/// <returns>
		///     <see langword="true" /> if a different <see cref="T:System.Type" /> definition is allowable for an application configuration object; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002C6 RID: 710
		bool IsDefinitionAllowed(string configPath, ConfigurationAllowDefinition allowDefinition, ConfigurationAllowExeDefinition allowExeDefinition);

		/// <summary>Returns a value indicating whether the file path used by a <see cref="T:System.IO.Stream" /> object to read a configuration file is a valid path.</summary>
		/// <param name="streamName">A string representing the name of the <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file.</param>
		/// <returns>
		///     <see langword="true" /> if the path used by a <see cref="T:System.IO.Stream" /> object to read a configuration file is a valid path; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002C7 RID: 711
		bool IsFile(string streamName);

		/// <summary>Returns a value indicating whether a configuration section requires a fully trusted code access security level and does not allow the <see cref="T:System.Security.AllowPartiallyTrustedCallersAttribute" /> attribute to disable implicit link demands.</summary>
		/// <param name="configRecord">The <see cref="T:System.Configuration.Internal.IInternalConfigRecord" /> object.</param>
		/// <returns>
		///     <see langword="true" /> if the configuration section requires a fully trusted code access security level and does not allow the <see cref="T:System.Security.AllowPartiallyTrustedCallersAttribute" /> attribute to disable implicit link demands; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002C8 RID: 712
		bool IsFullTrustSectionWithoutAptcaAllowed(IInternalConfigRecord configRecord);

		/// <summary>Returns a value indicating whether the initialization of a configuration object is considered delayed.</summary>
		/// <param name="configRecord">The <see cref="T:System.Configuration.Internal.IInternalConfigRecord" /> object.</param>
		/// <returns>
		///     <see langword="true" /> if the initialization of a configuration object is considered delayed; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002C9 RID: 713
		bool IsInitDelayed(IInternalConfigRecord configRecord);

		/// <summary>Returns a value indicating whether the configuration object supports a location tag.</summary>
		/// <param name="configPath">A string representing the path of the application configuration file.</param>
		/// <returns>
		///     <see langword="true" /> if the configuration object supports a location tag; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002CA RID: 714
		bool IsLocationApplicable(string configPath);

		/// <summary>Returns a value indicating whether a configuration path is to a configuration node whose contents should be treated as a root.</summary>
		/// <param name="configPath">A string representing the path of the application configuration file.</param>
		/// <returns>
		///     <see langword="true" /> if the configuration path is to a configuration node whose contents should be treated as a root; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002CB RID: 715
		bool IsSecondaryRoot(string configPath);

		/// <summary>Returns a value indicating whether the configuration path is trusted.</summary>
		/// <param name="configPath">A string representing the path of the application configuration file.</param>
		/// <returns>
		///     <see langword="true" /> if the configuration path is trusted; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002CC RID: 716
		bool IsTrustedConfigPath(string configPath);

		/// <summary>Opens a <see cref="T:System.IO.Stream" /> to read a configuration file.</summary>
		/// <param name="streamName">A string representing the name of the <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file.</param>
		/// <returns>A <see cref="T:System.IO.Stream" /> object.</returns>
		// Token: 0x060002CD RID: 717
		Stream OpenStreamForRead(string streamName);

		/// <summary>Opens a <see cref="T:System.IO.Stream" /> object to read a configuration file.</summary>
		/// <param name="streamName">A string representing the name of the <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file.</param>
		/// <param name="assertPermissions">
		///       <see langword="true" /> to assert permissions; otherwise, <see langword="false" />.</param>
		/// <returns>Returns the <see cref="T:System.IO.Stream" /> object specified by <paramref name="streamName" />.</returns>
		// Token: 0x060002CE RID: 718
		Stream OpenStreamForRead(string streamName, bool assertPermissions);

		/// <summary>Opens a <see cref="T:System.IO.Stream" /> object for writing to a configuration file or for writing to a temporary file used to build a configuration file. Allows a <see cref="T:System.IO.Stream" /> object to be designated as a template for copying file attributes.</summary>
		/// <param name="streamName">A string representing the name of the <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file.</param>
		/// <param name="templateStreamName">The name of a <see cref="T:System.IO.Stream" /> object from which file attributes are to be copied as a template.</param>
		/// <param name="writeContext">The write context of the <see cref="T:System.IO.Stream" /> object.</param>
		/// <returns>A <see cref="T:System.IO.Stream" /> object.</returns>
		// Token: 0x060002CF RID: 719
		Stream OpenStreamForWrite(string streamName, string templateStreamName, ref object writeContext);

		/// <summary>Opens a <see cref="T:System.IO.Stream" /> object for writing to a configuration file. Allows a <see cref="T:System.IO.Stream" /> object to be designated as a template for copying file attributes.</summary>
		/// <param name="streamName">A string representing the name of the <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file.</param>
		/// <param name="templateStreamName">The name of a <see cref="T:System.IO.Stream" /> from which file attributes are to be copied as a template.</param>
		/// <param name="writeContext">The write context of the <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file.</param>
		/// <param name="assertPermissions">
		///       <see langword="true" /> to assert permissions; otherwise, <see langword="false" />.</param>
		/// <returns>Returns the <see cref="T:System.IO.Stream" /> object specified by <paramref name="streamName" />.</returns>
		// Token: 0x060002D0 RID: 720
		Stream OpenStreamForWrite(string streamName, string templateStreamName, ref object writeContext, bool assertPermissions);

		/// <summary>Returns a value indicating whether the entire configuration file could be read by a designated <see cref="T:System.IO.Stream" /> object.</summary>
		/// <param name="configPath">A string representing the path of the application configuration file.</param>
		/// <param name="streamName">A string representing the name of the <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file.</param>
		/// <returns>
		///     <see langword="true" /> if the entire configuration file could be read by the <see cref="T:System.IO.Stream" /> object designated by <paramref name="streamName" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002D1 RID: 721
		bool PrefetchAll(string configPath, string streamName);

		/// <summary>Instructs the <see cref="T:System.Configuration.Internal.IInternalConfigHost" /> object to read a designated section of its associated configuration file.</summary>
		/// <param name="sectionGroupName">A string representing the identifying name of a configuration file section group.</param>
		/// <param name="sectionName">A string representing the identifying name of a configuration file section.</param>
		/// <returns>
		///     <see langword="true" /> if a section of the configuration file designated by <paramref name="sectionGroupName" /> and <paramref name="sectionName" /> could be read by a <see cref="T:System.IO.Stream" /> object; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002D2 RID: 722
		bool PrefetchSection(string sectionGroupName, string sectionName);

		/// <summary>Indicates a new configuration record requires a complete initialization.</summary>
		/// <param name="configRecord">An <see cref="T:System.Configuration.Internal.IInternalConfigRecord" /> object.</param>
		// Token: 0x060002D3 RID: 723
		void RequireCompleteInit(IInternalConfigRecord configRecord);

		/// <summary>Instructs the <see cref="T:System.Configuration.Internal.IInternalConfigHost" /> object to monitor an associated <see cref="T:System.IO.Stream" /> object for changes in a configuration file.</summary>
		/// <param name="streamName">A string representing the name of the <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file.</param>
		/// <param name="callback">A <see cref="T:System.Configuration.Internal.StreamChangeCallback" /> object to receive the returned data representing the changes in the configuration file.</param>
		/// <returns>An <see cref="T:System.Object" /> containing changed configuration settings.</returns>
		// Token: 0x060002D4 RID: 724
		object StartMonitoringStreamForChanges(string streamName, StreamChangeCallback callback);

		/// <summary>Instructs the  <see cref="T:System.Configuration.Internal.IInternalConfigHost" /> object to stop monitoring an associated <see cref="T:System.IO.Stream" /> object for changes in a configuration file.</summary>
		/// <param name="streamName">A string representing the name of the <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file.</param>
		/// <param name="callback">A <see cref="T:System.Configuration.Internal.StreamChangeCallback" /> object.</param>
		// Token: 0x060002D5 RID: 725
		void StopMonitoringStreamForChanges(string streamName, StreamChangeCallback callback);

		/// <summary>Verifies that a configuration definition is allowed for a configuration record.</summary>
		/// <param name="configPath">A string representing the path of the application configuration file.</param>
		/// <param name="allowDefinition">A <see cref="P:System.Configuration.SectionInformation.AllowDefinition" /> object.</param>
		/// <param name="allowExeDefinition">A <see cref="T:System.Configuration.ConfigurationAllowExeDefinition" /> object</param>
		/// <param name="errorInfo">An <see cref="T:System.Configuration.Internal.IConfigErrorInfo" /> object.</param>
		// Token: 0x060002D6 RID: 726
		void VerifyDefinitionAllowed(string configPath, ConfigurationAllowDefinition allowDefinition, ConfigurationAllowExeDefinition allowExeDefinition, IConfigErrorInfo errorInfo);

		/// <summary>Indicates that all writing to the configuration file has completed.</summary>
		/// <param name="streamName">A string representing the name of the <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file.</param>
		/// <param name="success">
		///       <see langword="true" /> if the write to the configuration file was completed successfully; otherwise, <see langword="false" />.</param>
		/// <param name="writeContext">The write context of the <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file.</param>
		// Token: 0x060002D7 RID: 727
		void WriteCompleted(string streamName, bool success, object writeContext);

		/// <summary>Indicates that all writing to the configuration file has completed and specifies whether permissions should be asserted.</summary>
		/// <param name="streamName">A string representing the name of the <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file.</param>
		/// <param name="success">
		///       <see langword="true" /> to indicate the write was completed successfully; otherwise, <see langword="false" />.</param>
		/// <param name="writeContext">The write context of the <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file.</param>
		/// <param name="assertPermissions">
		///       <see langword="true" /> to assert permissions; otherwise, <see langword="false" />.</param>
		// Token: 0x060002D8 RID: 728
		void WriteCompleted(string streamName, bool success, object writeContext, bool assertPermissions);
	}
}
