﻿using System;
using Unity;

namespace System.Configuration.Internal
{
	/// <summary>Defines a class used by the .NET Framework infrastructure to support configuration events.</summary>
	/// <param name="sender">The source object of the event.</param>
	/// <param name="e">A configuration event argument.</param>
	// Token: 0x02000064 RID: 100
	public sealed class InternalConfigEventHandler : MulticastDelegate
	{
		// Token: 0x060002EC RID: 748 RVA: 0x00002050 File Offset: 0x00000250
		public InternalConfigEventHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060002ED RID: 749 RVA: 0x00002050 File Offset: 0x00000250
		public void Invoke(object sender, InternalConfigEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060002EE RID: 750 RVA: 0x00002057 File Offset: 0x00000257
		public IAsyncResult BeginInvoke(object sender, InternalConfigEventArgs e, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060002EF RID: 751 RVA: 0x00002050 File Offset: 0x00000250
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
