﻿using System;
using System.Runtime.InteropServices;

namespace System.Configuration.Internal
{
	/// <summary>Defines interfaces used by internal .NET structures to support a configuration root object.</summary>
	// Token: 0x02000063 RID: 99
	[ComVisible(false)]
	public interface IInternalConfigRoot
	{
		/// <summary>Returns a value indicating whether the configuration is a design-time configuration.</summary>
		/// <returns>
		///     <see langword="true" /> if the configuration is a design-time configuration; <see langword="false" /> if the configuration is not a design-time configuration.</returns>
		// Token: 0x170000F3 RID: 243
		// (get) Token: 0x060002E1 RID: 737
		bool IsDesignTime { get; }

		/// <summary>Represents the method that handles the <see cref="E:System.Configuration.Internal.IInternalConfigRoot.ConfigChanged" /> event of an <see cref="T:System.Configuration.Internal.IInternalConfigRoot" /> object.</summary>
		// Token: 0x14000001 RID: 1
		// (add) Token: 0x060002E2 RID: 738
		// (remove) Token: 0x060002E3 RID: 739
		event InternalConfigEventHandler ConfigChanged;

		/// <summary>Represents the method that handles the <see cref="E:System.Configuration.Internal.IInternalConfigRoot.ConfigRemoved" /> event of a <see cref="T:System.Configuration.Internal.IInternalConfigRoot" /> object.</summary>
		// Token: 0x14000002 RID: 2
		// (add) Token: 0x060002E4 RID: 740
		// (remove) Token: 0x060002E5 RID: 741
		event InternalConfigEventHandler ConfigRemoved;

		/// <summary>Returns an <see cref="T:System.Configuration.Internal.IInternalConfigRecord" /> object representing a configuration specified by a configuration path.</summary>
		/// <param name="configPath">A string representing the path to a configuration file.</param>
		/// <returns>An <see cref="T:System.Configuration.Internal.IInternalConfigRecord" /> object representing a configuration specified by <paramref name="configPath" />.</returns>
		// Token: 0x060002E6 RID: 742
		IInternalConfigRecord GetConfigRecord(string configPath);

		/// <summary>Returns an <see cref="T:System.Object" /> representing the data in a section of a configuration file.</summary>
		/// <param name="section">A string representing a section of a configuration file.</param>
		/// <param name="configPath">A string representing the path to a configuration file.</param>
		/// <returns>An <see cref="T:System.Object" /> representing the data in a section of a configuration file.</returns>
		// Token: 0x060002E7 RID: 743
		object GetSection(string section, string configPath);

		/// <summary>Returns a value representing the file path of the nearest configuration ancestor that has configuration data.</summary>
		/// <param name="configPath">The path of configuration file.</param>
		/// <returns>Returns a string representing the file path of the nearest configuration ancestor that has configuration data.</returns>
		// Token: 0x060002E8 RID: 744
		string GetUniqueConfigPath(string configPath);

		/// <summary>Returns an <see cref="T:System.Configuration.Internal.IInternalConfigRecord" /> object representing a unique configuration record for given configuration path.</summary>
		/// <param name="configPath">The path of the configuration file.</param>
		/// <returns>An <see cref="T:System.Configuration.Internal.IInternalConfigRecord" /> object representing a unique configuration record for a given configuration path.</returns>
		// Token: 0x060002E9 RID: 745
		IInternalConfigRecord GetUniqueConfigRecord(string configPath);

		/// <summary>Initializes a configuration object.</summary>
		/// <param name="host">An <see cref="T:System.Configuration.Internal.IInternalConfigHost" /> object.</param>
		/// <param name="isDesignTime">
		///       <see langword="true" /> if design time; <see langword="false" /> if run time.</param>
		// Token: 0x060002EA RID: 746
		void Init(IInternalConfigHost host, bool isDesignTime);

		/// <summary>Finds and removes a configuration record and all its children for a given configuration path.</summary>
		/// <param name="configPath">The path of the configuration file.</param>
		// Token: 0x060002EB RID: 747
		void RemoveConfig(string configPath);
	}
}
