﻿using System;
using System.IO;
using System.Security;
using System.Xml;
using Unity;

namespace System.Configuration.Internal
{
	/// <summary>Delegates all members of the <see cref="T:System.Configuration.Internal.IInternalConfigHost" /> interface to another instance of a host.</summary>
	// Token: 0x0200005F RID: 95
	public class DelegatingConfigHost : IInternalConfigHost, IInternalConfigurationBuilderHost
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.Internal.DelegatingConfigHost" /> class.</summary>
		// Token: 0x0600027F RID: 639 RVA: 0x00002050 File Offset: 0x00000250
		protected DelegatingConfigHost()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the <see cref="T:System.Configuration.Internal.IInternalConfigurationBuilderHost" /> object if the delegated host provides the functionality required by that interface.</summary>
		/// <returns>An <see cref="T:System.Configuration.Internal.IInternalConfigurationBuilderHost" /> object.</returns>
		// Token: 0x170000E4 RID: 228
		// (get) Token: 0x06000280 RID: 640 RVA: 0x00002057 File Offset: 0x00000257
		protected IInternalConfigurationBuilderHost ConfigBuilderHost
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Configuration.Internal.IInternalConfigHost" /> object.</summary>
		/// <returns>A <see cref="T:System.Configuration.Internal.IInternalConfigHost" /> object.</returns>
		// Token: 0x170000E5 RID: 229
		// (get) Token: 0x06000281 RID: 641 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x06000282 RID: 642 RVA: 0x00002050 File Offset: 0x00000250
		protected IInternalConfigHost Host
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a value indicating whether the configuration is remote.</summary>
		/// <returns>
		///     <see langword="true" /> if the configuration is remote; otherwise, <see langword="false" />.</returns>
		// Token: 0x170000E6 RID: 230
		// (get) Token: 0x06000283 RID: 643 RVA: 0x00002D70 File Offset: 0x00000F70
		public virtual bool IsRemote
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value indicating whether the host configuration supports change notifications.</summary>
		/// <returns>
		///     <see langword="true" /> if the host supports change notifications; otherwise, <see langword="false" />. </returns>
		// Token: 0x170000E7 RID: 231
		// (get) Token: 0x06000284 RID: 644 RVA: 0x00002D8C File Offset: 0x00000F8C
		public virtual bool SupportsChangeNotifications
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value indicating whether the host configuration supports location tags.</summary>
		/// <returns>
		///     <see langword="true" /> if the host supports location tags; otherwise, <see langword="false" />.</returns>
		// Token: 0x170000E8 RID: 232
		// (get) Token: 0x06000285 RID: 645 RVA: 0x00002DA8 File Offset: 0x00000FA8
		public virtual bool SupportsLocation
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value indicating whether the host configuration has path support.</summary>
		/// <returns>
		///     <see langword="true" /> if the host configuration has path support; otherwise, <see langword="false" />.</returns>
		// Token: 0x170000E9 RID: 233
		// (get) Token: 0x06000286 RID: 646 RVA: 0x00002DC4 File Offset: 0x00000FC4
		public virtual bool SupportsPath
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value indicating whether the host configuration supports refresh.</summary>
		/// <returns>
		///     <see langword="true" /> if the host configuration supports refresh; otherwise, <see langword="false" />.</returns>
		// Token: 0x170000EA RID: 234
		// (get) Token: 0x06000287 RID: 647 RVA: 0x00002DE0 File Offset: 0x00000FE0
		public virtual bool SupportsRefresh
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Creates a new configuration context.</summary>
		/// <param name="configPath">A string representing the path to a configuration file.</param>
		/// <param name="locationSubPath">A string representing a location subpath.</param>
		/// <returns>A <see cref="T:System.Object" /> representing a new configuration context.</returns>
		// Token: 0x06000288 RID: 648 RVA: 0x00002057 File Offset: 0x00000257
		public virtual object CreateConfigurationContext(string configPath, string locationSubPath)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates a deprecated configuration context.</summary>
		/// <param name="configPath">A string representing the path to a configuration file.</param>
		/// <returns>A <see cref="T:System.Object" /> representing a deprecated configuration context.</returns>
		// Token: 0x06000289 RID: 649 RVA: 0x00002057 File Offset: 0x00000257
		public virtual object CreateDeprecatedConfigContext(string configPath)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Decrypts an encrypted configuration section.</summary>
		/// <param name="encryptedXml">An encrypted section of a configuration file.</param>
		/// <param name="protectionProvider">A <see cref="T:System.Configuration.ProtectedConfigurationProvider" /> object.</param>
		/// <param name="protectedConfigSection">A <see cref="T:System.Configuration.ProtectedConfigurationSection" /> object.</param>
		/// <returns>A string representing a decrypted configuration section.</returns>
		// Token: 0x0600028A RID: 650 RVA: 0x00002057 File Offset: 0x00000257
		public virtual string DecryptSection(string encryptedXml, ProtectedConfigurationProvider protectionProvider, ProtectedConfigurationSection protectedConfigSection)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Deletes the <see cref="T:System.IO.Stream" /> object performing I/O tasks on a configuration file.</summary>
		/// <param name="streamName">The name of a <see cref="T:System.IO.Stream" /> object performing I/O tasks on a configuration file.</param>
		// Token: 0x0600028B RID: 651 RVA: 0x00002050 File Offset: 0x00000250
		public virtual void DeleteStream(string streamName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Encrypts a section of a configuration object.</summary>
		/// <param name="clearTextXml">A section of the configuration that is not encrypted.</param>
		/// <param name="protectionProvider">A <see cref="T:System.Configuration.ProtectedConfigurationProvider" /> object.</param>
		/// <param name="protectedConfigSection">A <see cref="T:System.Configuration.ProtectedConfigurationSection" /> object.</param>
		/// <returns>A string representing an encrypted section of the configuration object.</returns>
		// Token: 0x0600028C RID: 652 RVA: 0x00002057 File Offset: 0x00000257
		public virtual string EncryptSection(string clearTextXml, ProtectedConfigurationProvider protectionProvider, ProtectedConfigurationSection protectedConfigSection)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns a configuration path based on a location subpath.</summary>
		/// <param name="configPath">A string representing the path to a configuration file.</param>
		/// <param name="locationSubPath">A string representing a location subpath.</param>
		/// <returns>A string representing a configuration path.</returns>
		// Token: 0x0600028D RID: 653 RVA: 0x00002057 File Offset: 0x00000257
		public virtual string GetConfigPathFromLocationSubPath(string configPath, string locationSubPath)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns a <see cref="T:System.Type" /> representing the type of the configuration.</summary>
		/// <param name="typeName">A string representing the configuration type.</param>
		/// <param name="throwOnError">
		///       <see langword="true" /> if an exception should be thrown if an error is encountered; <see langword="false" /> if an exception should not be thrown if an error is encountered.</param>
		/// <returns>A <see cref="T:System.Type" /> representing the type of the configuration.</returns>
		// Token: 0x0600028E RID: 654 RVA: 0x00002057 File Offset: 0x00000257
		public virtual Type GetConfigType(string typeName, bool throwOnError)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns a string representing the type name of the configuration object.</summary>
		/// <param name="t">A <see cref="T:System.Type" /> object.</param>
		/// <returns>A string representing the type name of the configuration object.</returns>
		// Token: 0x0600028F RID: 655 RVA: 0x00002057 File Offset: 0x00000257
		public virtual string GetConfigTypeName(Type t)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Sets the specified permission set if available within the host object.</summary>
		/// <param name="configRecord">An <see cref="T:System.Configuration.Internal.IInternalConfigRecord" /> object.</param>
		/// <param name="permissionSet">A <see cref="T:System.Security.PermissionSet" /> object.</param>
		/// <param name="isHostReady">
		///       <see langword="true" /> if the host has finished initialization; otherwise, <see langword="false" />.</param>
		// Token: 0x06000290 RID: 656 RVA: 0x00002050 File Offset: 0x00000250
		public virtual void GetRestrictedPermissions(IInternalConfigRecord configRecord, out PermissionSet permissionSet, out bool isHostReady)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns the name of a <see cref="T:System.IO.Stream" /> object performing I/O tasks on a configuration file.</summary>
		/// <param name="configPath">A string representing the path to a configuration file.</param>
		/// <returns>A string representing the name of a <see cref="T:System.IO.Stream" /> object performing I/O tasks on a configuration file.</returns>
		// Token: 0x06000291 RID: 657 RVA: 0x00002057 File Offset: 0x00000257
		public virtual string GetStreamName(string configPath)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the name of a <see cref="T:System.IO.Stream" /> object performing I/O tasks on a configuration source.</summary>
		/// <param name="streamName">The name of a <see cref="T:System.IO.Stream" /> object performing I/O tasks on a configuration file.</param>
		/// <param name="configSource">A string representing the configuration source.</param>
		/// <returns>A string representing the name of a <see cref="T:System.IO.Stream" /> object performing I/O tasks on a configuration source.</returns>
		// Token: 0x06000292 RID: 658 RVA: 0x00002057 File Offset: 0x00000257
		public virtual string GetStreamNameForConfigSource(string streamName, string configSource)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns a <see cref="P:System.Diagnostics.FileVersionInfo.FileVersion" /> object representing the version of a <see cref="T:System.IO.Stream" /> object performing I/O tasks on a configuration file.</summary>
		/// <param name="streamName">The name of a <see cref="T:System.IO.Stream" /> object performing I/O tasks on a configuration file.</param>
		/// <returns>A <see cref="P:System.Diagnostics.FileVersionInfo.FileVersion" /> object representing the version of a <see cref="T:System.IO.Stream" /> object performing I/O tasks on a configuration file.</returns>
		// Token: 0x06000293 RID: 659 RVA: 0x00002057 File Offset: 0x00000257
		public virtual object GetStreamVersion(string streamName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Instructs the host to impersonate and returns an <see cref="T:System.IDisposable" /> object required internally by the .NET Framework.</summary>
		/// <returns>An <see cref="T:System.IDisposable" /> value.</returns>
		// Token: 0x06000294 RID: 660 RVA: 0x00002057 File Offset: 0x00000257
		public virtual IDisposable Impersonate()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Initializes the configuration host.</summary>
		/// <param name="configRoot">An <see cref="T:System.Configuration.Internal.IInternalConfigRoot" /> object.</param>
		/// <param name="hostInitParams">A parameter object containing the values used for initializing the configuration host.</param>
		// Token: 0x06000295 RID: 661 RVA: 0x00002050 File Offset: 0x00000250
		public virtual void Init(IInternalConfigRoot configRoot, object[] hostInitParams)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes the host for configuration.</summary>
		/// <param name="locationSubPath">A string representing a location subpath (passed by reference).</param>
		/// <param name="configPath">A string representing the path to a configuration file.</param>
		/// <param name="locationConfigPath">The location configuration path.</param>
		/// <param name="configRoot">The configuration root element.</param>
		/// <param name="hostInitConfigurationParams">A parameter object representing the parameters used to initialize the host.</param>
		// Token: 0x06000296 RID: 662 RVA: 0x00002050 File Offset: 0x00000250
		public virtual void InitForConfiguration(ref string locationSubPath, out string configPath, out string locationConfigPath, IInternalConfigRoot configRoot, object[] hostInitConfigurationParams)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns a value indicating whether the configuration is above the application configuration in the configuration hierarchy.</summary>
		/// <param name="configPath">A string representing the path to a configuration file.</param>
		/// <returns>
		///     <see langword="true" /> if the configuration is above the application configuration in the configuration hierarchy; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000297 RID: 663 RVA: 0x00002DFC File Offset: 0x00000FFC
		public virtual bool IsAboveApplication(string configPath)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a value indicating whether a configuration record is required for the host configuration initialization.</summary>
		/// <param name="configPath">A string representing the path to a configuration file.</param>
		/// <returns>
		///     <see langword="true" /> if a configuration record is required for the host configuration initialization; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000298 RID: 664 RVA: 0x00002E18 File Offset: 0x00001018
		public virtual bool IsConfigRecordRequired(string configPath)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Restricts or allows definitions in the host configuration. </summary>
		/// <param name="configPath">A string representing the path to a configuration file.</param>
		/// <param name="allowDefinition">The <see cref="T:System.Configuration.ConfigurationAllowDefinition" /> object.</param>
		/// <param name="allowExeDefinition">The <see cref="T:System.Configuration.ConfigurationAllowExeDefinition" /> object.</param>
		/// <returns>
		///     <see langword="true" /> if the grant or restriction of definitions in the host configuration was successful; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000299 RID: 665 RVA: 0x00002E34 File Offset: 0x00001034
		public virtual bool IsDefinitionAllowed(string configPath, ConfigurationAllowDefinition allowDefinition, ConfigurationAllowExeDefinition allowExeDefinition)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a value indicating whether the file path used by a <see cref="T:System.IO.Stream" /> object to read a configuration file is a valid path.</summary>
		/// <param name="streamName">The name of a <see cref="T:System.IO.Stream" /> object performing I/O tasks on a configuration file.</param>
		/// <returns>
		///     <see langword="true" /> if the path used by a <see cref="T:System.IO.Stream" /> object to read a configuration file is a valid path; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600029A RID: 666 RVA: 0x00002E50 File Offset: 0x00001050
		public virtual bool IsFile(string streamName)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a value indicating whether a configuration section requires a fully trusted code access security level and does not allow the <see cref="T:System.Security.AllowPartiallyTrustedCallersAttribute" /> attribute to disable implicit link demands.</summary>
		/// <param name="configRecord">The <see cref="T:System.Configuration.Internal.IInternalConfigRecord" /> object.</param>
		/// <returns>
		///     <see langword="true" /> if the configuration section requires a fully trusted code access security level and does not allow the <see cref="T:System.Security.AllowPartiallyTrustedCallersAttribute" /> attribute to disable implicit link demands; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600029B RID: 667 RVA: 0x00002E6C File Offset: 0x0000106C
		public virtual bool IsFullTrustSectionWithoutAptcaAllowed(IInternalConfigRecord configRecord)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a value indicating whether the initialization of a configuration object is considered delayed.</summary>
		/// <param name="configRecord">The <see cref="T:System.Configuration.Internal.IInternalConfigRecord" /> object.</param>
		/// <returns>
		///     <see langword="true" /> if the initialization of a configuration object is considered delayed; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600029C RID: 668 RVA: 0x00002E88 File Offset: 0x00001088
		public virtual bool IsInitDelayed(IInternalConfigRecord configRecord)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a value indicating whether the configuration object supports a location tag.</summary>
		/// <param name="configPath">A string representing the path to a configuration file.</param>
		/// <returns>
		///     <see langword="true" /> if the configuration object supports a location tag; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600029D RID: 669 RVA: 0x00002EA4 File Offset: 0x000010A4
		public virtual bool IsLocationApplicable(string configPath)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a value indicating whether a configuration path is to a configuration node whose contents should be treated as a root.</summary>
		/// <param name="configPath">A string representing the path to a configuration file.</param>
		/// <returns>
		///     <see langword="true" /> if the configuration path is to a configuration node whose contents should be treated as a root; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600029E RID: 670 RVA: 0x00002EC0 File Offset: 0x000010C0
		public virtual bool IsSecondaryRoot(string configPath)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a value indicating whether the configuration path is trusted.</summary>
		/// <param name="configPath">A string representing the path to a configuration file.</param>
		/// <returns>
		///     <see langword="true" /> if the configuration path is trusted; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600029F RID: 671 RVA: 0x00002EDC File Offset: 0x000010DC
		public virtual bool IsTrustedConfigPath(string configPath)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Opens a <see cref="T:System.IO.Stream" /> object to read a configuration file.</summary>
		/// <param name="streamName">The name of a <see cref="T:System.IO.Stream" /> object performing I/O tasks on a configuration file.</param>
		/// <returns>Returns the <see cref="T:System.IO.Stream" /> object specified by <paramref name="streamName" />.</returns>
		// Token: 0x060002A0 RID: 672 RVA: 0x00002057 File Offset: 0x00000257
		public virtual Stream OpenStreamForRead(string streamName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Opens a <see cref="T:System.IO.Stream" /> object to read a configuration file.</summary>
		/// <param name="streamName">The name of a <see cref="T:System.IO.Stream" /> object performing I/O tasks on a configuration file.</param>
		/// <param name="assertPermissions">
		///       <see langword="true" /> to assert permissions; otherwise, <see langword="false" />.</param>
		/// <returns>Returns the <see cref="T:System.IO.Stream" /> object specified by <paramref name="streamName" />.</returns>
		// Token: 0x060002A1 RID: 673 RVA: 0x00002057 File Offset: 0x00000257
		public virtual Stream OpenStreamForRead(string streamName, bool assertPermissions)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Opens a <see cref="T:System.IO.Stream" /> object for writing to a configuration file or for writing to a temporary file used to build a configuration file. Allows a <see cref="T:System.IO.Stream" /> object to be designated as a template for copying file attributes.</summary>
		/// <param name="streamName">The name of a <see cref="T:System.IO.Stream" /> object performing I/O tasks on a configuration file.</param>
		/// <param name="templateStreamName">The name of a <see cref="T:System.IO.Stream" /> object from which file attributes are to be copied as a template.</param>
		/// <param name="writeContext">The write context of the <see cref="T:System.IO.Stream" /> object (passed by reference).</param>
		/// <returns>A <see cref="T:System.IO.Stream" /> object.</returns>
		// Token: 0x060002A2 RID: 674 RVA: 0x00002057 File Offset: 0x00000257
		public virtual Stream OpenStreamForWrite(string streamName, string templateStreamName, ref object writeContext)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Opens a <see cref="T:System.IO.Stream" /> object for writing to a configuration file. Allows a <see cref="T:System.IO.Stream" /> object to be designated as a template for copying file attributes.</summary>
		/// <param name="streamName">The name of a <see cref="T:System.IO.Stream" /> object performing I/O tasks on a configuration file.</param>
		/// <param name="templateStreamName">The name of a <see cref="T:System.IO.Stream" /> object from which file attributes are to be copied as a template.</param>
		/// <param name="writeContext">The write context of the <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file (passed by reference).</param>
		/// <param name="assertPermissions">
		///       <see langword="true" /> to assert permissions; otherwise, <see langword="false" />.</param>
		/// <returns>Returns the <see cref="T:System.IO.Stream" /> object specified by the <paramref name="streamName" /> parameter.</returns>
		// Token: 0x060002A3 RID: 675 RVA: 0x00002057 File Offset: 0x00000257
		public virtual Stream OpenStreamForWrite(string streamName, string templateStreamName, ref object writeContext, bool assertPermissions)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns a value indicating whether the entire configuration file could be read by a designated <see cref="T:System.IO.Stream" /> object.</summary>
		/// <param name="configPath">A string representing the path to a configuration file.</param>
		/// <param name="streamName">The name of a <see cref="T:System.IO.Stream" /> object performing I/O tasks on a configuration file.</param>
		/// <returns>
		///     <see langword="true" /> if the entire configuration file could be read by the <see cref="T:System.IO.Stream" /> object designated by <paramref name="streamName" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002A4 RID: 676 RVA: 0x00002EF8 File Offset: 0x000010F8
		public virtual bool PrefetchAll(string configPath, string streamName)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Instructs the <see cref="T:System.Configuration.Internal.IInternalConfigHost" /> object to read a designated section of its associated configuration file.</summary>
		/// <param name="sectionGroupName">A string representing the name of a section group in the configuration file.</param>
		/// <param name="sectionName">A string representing the name of a section in the configuration file.</param>
		/// <returns>
		///     <see langword="true" /> if a section of the configuration file designated by the <paramref name="sectionGroupName" /> and <paramref name="sectionName" /> parameters can be read by a <see cref="T:System.IO.Stream" /> object; otherwise, <see langword="false" />.</returns>
		// Token: 0x060002A5 RID: 677 RVA: 0x00002F14 File Offset: 0x00001114
		public virtual bool PrefetchSection(string sectionGroupName, string sectionName)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Processes a <see cref="T:System.Configuration.ConfigurationSection" /> object using the provided <see cref="T:System.Configuration.ConfigurationBuilder" />.</summary>
		/// <param name="configSection">The <see cref="T:System.Configuration.ConfigurationSection" /> to process.</param>
		/// <param name="builder">
		///   <see cref="T:System.Configuration.ConfigurationBuilder" /> to use to process the <paramref name="configSection" />.</param>
		/// <returns>The processed <see cref="T:System.Configuration.ConfigurationSection" />.</returns>
		// Token: 0x060002A6 RID: 678 RVA: 0x00002057 File Offset: 0x00000257
		public virtual ConfigurationSection ProcessConfigurationSection(ConfigurationSection configSection, ConfigurationBuilder builder)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Processes the markup of a configuration section using the provided <see cref="T:System.Configuration.ConfigurationBuilder" />.</summary>
		/// <param name="rawXml">The <see cref="T:System.Xml.XmlNode" /> to process.</param>
		/// <param name="builder">
		///   <see cref="T:System.Configuration.ConfigurationBuilder" /> to use to process the <paramref name="rawXml" />.</param>
		/// <returns>The processed <see cref="T:System.Xml.XmlNode" />.</returns>
		// Token: 0x060002A7 RID: 679 RVA: 0x00002057 File Offset: 0x00000257
		public virtual XmlNode ProcessRawXml(XmlNode rawXml, ConfigurationBuilder builder)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Indicates that a new configuration record requires a complete initialization.</summary>
		/// <param name="configRecord">An <see cref="T:System.Configuration.Internal.IInternalConfigRecord" /> object.</param>
		// Token: 0x060002A8 RID: 680 RVA: 0x00002050 File Offset: 0x00000250
		public virtual void RequireCompleteInit(IInternalConfigRecord configRecord)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Instructs the host to monitor an associated <see cref="T:System.IO.Stream" /> object for changes in a configuration file.</summary>
		/// <param name="streamName">The name of a <see cref="T:System.IO.Stream" /> object performing I/O tasks on a configuration file.</param>
		/// <param name="callback">A <see cref="T:System.Configuration.Internal.StreamChangeCallback" /> object to receive the returned data representing the changes in the configuration file.</param>
		/// <returns>An <see cref="T:System.Object" /> instance containing changed configuration settings.</returns>
		// Token: 0x060002A9 RID: 681 RVA: 0x00002057 File Offset: 0x00000257
		public virtual object StartMonitoringStreamForChanges(string streamName, StreamChangeCallback callback)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Instructs the host object to stop monitoring an associated <see cref="T:System.IO.Stream" /> object for changes in a configuration file.</summary>
		/// <param name="streamName">The name of a <see cref="T:System.IO.Stream" /> object performing I/O tasks on a configuration file.</param>
		/// <param name="callback">A <see cref="T:System.Configuration.Internal.StreamChangeCallback" /> object.</param>
		// Token: 0x060002AA RID: 682 RVA: 0x00002050 File Offset: 0x00000250
		public virtual void StopMonitoringStreamForChanges(string streamName, StreamChangeCallback callback)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Verifies that a configuration definition is allowed for a configuration record.</summary>
		/// <param name="configPath">A string representing the path to a configuration file.</param>
		/// <param name="allowDefinition">An <see cref="P:System.Configuration.SectionInformation.AllowDefinition" /> object.</param>
		/// <param name="allowExeDefinition">A <see cref="T:System.Configuration.ConfigurationAllowExeDefinition" /> object</param>
		/// <param name="errorInfo">An <see cref="T:System.Configuration.Internal.IConfigErrorInfo" /> object.</param>
		// Token: 0x060002AB RID: 683 RVA: 0x00002050 File Offset: 0x00000250
		public virtual void VerifyDefinitionAllowed(string configPath, ConfigurationAllowDefinition allowDefinition, ConfigurationAllowExeDefinition allowExeDefinition, IConfigErrorInfo errorInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Indicates that all writing to the configuration file has completed.</summary>
		/// <param name="streamName">The name of a <see cref="T:System.IO.Stream" /> object performing I/O tasks on a configuration file.</param>
		/// <param name="success">
		///       <see langword="true" /> if writing to the configuration file completed successfully; otherwise, <see langword="false" />.</param>
		/// <param name="writeContext">The write context of the <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file.</param>
		// Token: 0x060002AC RID: 684 RVA: 0x00002050 File Offset: 0x00000250
		public virtual void WriteCompleted(string streamName, bool success, object writeContext)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Indicates that all writing to the configuration file has completed and specifies whether permissions should be asserted.</summary>
		/// <param name="streamName">The name of a <see cref="T:System.IO.Stream" /> object performing I/O tasks on a configuration file.</param>
		/// <param name="success">
		///       <see langword="true" /> to indicate that writing was completed successfully; otherwise, <see langword="false" />.</param>
		/// <param name="writeContext">The write context of the <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file.</param>
		/// <param name="assertPermissions">
		///       <see langword="true" /> to assert permissions; otherwise, <see langword="false" />.</param>
		// Token: 0x060002AD RID: 685 RVA: 0x00002050 File Offset: 0x00000250
		public virtual void WriteCompleted(string streamName, bool success, object writeContext, bool assertPermissions)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
