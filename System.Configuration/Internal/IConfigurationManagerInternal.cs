﻿using System;
using System.Runtime.InteropServices;

namespace System.Configuration.Internal
{
	/// <summary>Defines an interface used by the .NET Framework to initialize configuration properties.</summary>
	// Token: 0x0200006A RID: 106
	[ComVisible(false)]
	public interface IConfigurationManagerInternal
	{
		/// <summary>Gets the configuration file name related to the application path.</summary>
		/// <returns>A string value representing a configuration file name.</returns>
		// Token: 0x170000F9 RID: 249
		// (get) Token: 0x060002FD RID: 765
		string ApplicationConfigUri { get; }

		/// <summary>Gets the local configuration directory of the application based on the entry assembly.</summary>
		/// <returns>A string representing the local configuration directory.</returns>
		// Token: 0x170000FA RID: 250
		// (get) Token: 0x060002FE RID: 766
		string ExeLocalConfigDirectory { get; }

		/// <summary>Gets the local configuration path of the application based on the entry assembly.</summary>
		/// <returns>A string value representing the local configuration path of the application.</returns>
		// Token: 0x170000FB RID: 251
		// (get) Token: 0x060002FF RID: 767
		string ExeLocalConfigPath { get; }

		/// <summary>Gets the product name of the application based on the entry assembly.</summary>
		/// <returns>A string value representing the product name of the application.</returns>
		// Token: 0x170000FC RID: 252
		// (get) Token: 0x06000300 RID: 768
		string ExeProductName { get; }

		/// <summary>Gets the product version of the application based on the entry assembly.</summary>
		/// <returns>A string value representing the product version of the application.</returns>
		// Token: 0x170000FD RID: 253
		// (get) Token: 0x06000301 RID: 769
		string ExeProductVersion { get; }

		/// <summary>Gets the roaming configuration directory of the application based on the entry assembly.</summary>
		/// <returns>A string value representing the roaming configuration directory of the application.</returns>
		// Token: 0x170000FE RID: 254
		// (get) Token: 0x06000302 RID: 770
		string ExeRoamingConfigDirectory { get; }

		/// <summary>Gets the roaming user's configuration path based on the application's entry assembly.</summary>
		/// <returns>A string value representing the roaming user's configuration path.</returns>
		// Token: 0x170000FF RID: 255
		// (get) Token: 0x06000303 RID: 771
		string ExeRoamingConfigPath { get; }

		/// <summary>Gets the configuration path for the Machine.config file.</summary>
		/// <returns>A string value representing the path of the Machine.config file.</returns>
		// Token: 0x17000100 RID: 256
		// (get) Token: 0x06000304 RID: 772
		string MachineConfigPath { get; }

		/// <summary>Gets a value representing the configuration system's status.</summary>
		/// <returns>
		///     <see langword="true" /> if the configuration system is in the process of being initialized; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000101 RID: 257
		// (get) Token: 0x06000305 RID: 773
		bool SetConfigurationSystemInProgress { get; }

		/// <summary>Gets a value that specifies whether user configuration settings are supported.</summary>
		/// <returns>
		///     <see langword="true" /> if the configuration system supports user configuration settings; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000102 RID: 258
		// (get) Token: 0x06000306 RID: 774
		bool SupportsUserConfig { get; }

		/// <summary>Gets the name of the file used to store user configuration settings.</summary>
		/// <returns>A string specifying the name of the file used to store user configuration.</returns>
		// Token: 0x17000103 RID: 259
		// (get) Token: 0x06000307 RID: 775
		string UserConfigFilename { get; }
	}
}
