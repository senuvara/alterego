﻿using System;
using System.Runtime.InteropServices;

namespace System.Configuration.Internal
{
	/// <summary>Defines an interface used by the .NET Framework to support configuration management.</summary>
	// Token: 0x02000069 RID: 105
	[ComVisible(false)]
	public interface IConfigurationManagerHelper
	{
		/// <summary>Ensures that the networking configuration is loaded.</summary>
		// Token: 0x060002FC RID: 764
		void EnsureNetConfigLoaded();
	}
}
