﻿using System;
using Unity;

namespace System.Configuration.Internal
{
	/// <summary>Represents a method for hosts to call when a monitored stream has changed.</summary>
	/// <param name="streamName">The name of the <see cref="T:System.IO.Stream" /> object performing I/O tasks on the configuration file.</param>
	// Token: 0x02000066 RID: 102
	public sealed class StreamChangeCallback : MulticastDelegate
	{
		// Token: 0x060002F3 RID: 755 RVA: 0x00002050 File Offset: 0x00000250
		public StreamChangeCallback(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060002F4 RID: 756 RVA: 0x00002050 File Offset: 0x00000250
		public void Invoke(string streamName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060002F5 RID: 757 RVA: 0x00002057 File Offset: 0x00000257
		public IAsyncResult BeginInvoke(string streamName, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060002F6 RID: 758 RVA: 0x00002050 File Offset: 0x00000250
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
