﻿using System;
using Unity;

namespace System.Configuration.Internal
{
	/// <summary>Defines a class that allows the .NET Framework infrastructure to specify event arguments for configuration events.</summary>
	// Token: 0x02000065 RID: 101
	public sealed class InternalConfigEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.Internal.InternalConfigEventArgs" /> class.</summary>
		/// <param name="configPath">A configuration path.</param>
		// Token: 0x060002F0 RID: 752 RVA: 0x00002050 File Offset: 0x00000250
		public InternalConfigEventArgs(string configPath)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the configuration path related to the <see cref="T:System.Configuration.Internal.InternalConfigEventArgs" /> object.</summary>
		/// <returns>A string value specifying the configuration path.</returns>
		// Token: 0x170000F4 RID: 244
		// (get) Token: 0x060002F1 RID: 753 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x060002F2 RID: 754 RVA: 0x00002050 File Offset: 0x00000250
		public string ConfigPath
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
