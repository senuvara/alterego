﻿using System;
using System.Runtime.InteropServices;

namespace System.Configuration.Internal
{
	/// <summary>Defines an interface used by the configuration system to set the <see cref="T:System.Configuration.ConfigurationSettings" /> class.</summary>
	// Token: 0x0200006D RID: 109
	[ComVisible(false)]
	public interface IInternalConfigSettingsFactory
	{
		/// <summary>Indicates that initialization of the configuration system has completed. </summary>
		// Token: 0x06000310 RID: 784
		void CompleteInit();

		/// <summary>Provides hierarchical configuration settings and extensions specific to ASP.NET to the configuration system. </summary>
		/// <param name="internalConfigSystem">An <see cref="T:System.Configuration.Internal.IInternalConfigSystem" /> object used by the <see cref="T:System.Configuration.ConfigurationSettings" /> class.</param>
		/// <param name="initComplete">
		///       <see langword="true" /> if the initialization process of the configuration system is complete; otherwise, <see langword="false" />.</param>
		// Token: 0x06000311 RID: 785
		void SetConfigurationSystem(IInternalConfigSystem internalConfigSystem, bool initComplete);
	}
}
