﻿using System;

namespace System.Configuration.Internal
{
	/// <summary>Defines an interface used by the .NET Framework to support the initialization of configuration properties.</summary>
	// Token: 0x02000068 RID: 104
	public interface IConfigSystem
	{
		/// <summary>Gets the configuration host.</summary>
		/// <returns>An <see cref="T:System.Configuration.Internal.IInternalConfigHost" /> object that is used by the .NET Framework to initialize application configuration properties.</returns>
		// Token: 0x170000F7 RID: 247
		// (get) Token: 0x060002F9 RID: 761
		IInternalConfigHost Host { get; }

		/// <summary>Gets the root of the configuration hierarchy.</summary>
		/// <returns>An <see cref="T:System.Configuration.Internal.IInternalConfigRoot" /> object.</returns>
		// Token: 0x170000F8 RID: 248
		// (get) Token: 0x060002FA RID: 762
		IInternalConfigRoot Root { get; }

		/// <summary>Initializes a configuration object.</summary>
		/// <param name="typeConfigHost">The type of configuration host.</param>
		/// <param name="hostInitParams">An array of configuration host parameters.</param>
		// Token: 0x060002FB RID: 763
		void Init(Type typeConfigHost, object[] hostInitParams);
	}
}
