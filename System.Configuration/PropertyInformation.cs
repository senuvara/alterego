﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Configuration
{
	/// <summary>Contains meta-information on an individual property within the configuration. This type cannot be inherited.</summary>
	// Token: 0x02000019 RID: 25
	public sealed class PropertyInformation
	{
		// Token: 0x060000FE RID: 254 RVA: 0x00002050 File Offset: 0x00000250
		internal PropertyInformation()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the <see cref="T:System.ComponentModel.TypeConverter" /> object related to the configuration attribute.</summary>
		/// <returns>A <see cref="T:System.ComponentModel.TypeConverter" /> object.</returns>
		// Token: 0x1700005C RID: 92
		// (get) Token: 0x060000FF RID: 255 RVA: 0x00002057 File Offset: 0x00000257
		public TypeConverter Converter
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets an object containing the default value related to a configuration attribute.</summary>
		/// <returns>An object containing the default value of the configuration attribute.</returns>
		// Token: 0x1700005D RID: 93
		// (get) Token: 0x06000100 RID: 256 RVA: 0x00002057 File Offset: 0x00000257
		public object DefaultValue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the description of the object that corresponds to a configuration attribute.</summary>
		/// <returns>The description of the configuration attribute.</returns>
		// Token: 0x1700005E RID: 94
		// (get) Token: 0x06000101 RID: 257 RVA: 0x00002057 File Offset: 0x00000257
		public string Description
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a value specifying whether the configuration attribute is a key.</summary>
		/// <returns>
		///     <see langword="true" /> if the configuration attribute is a key; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700005F RID: 95
		// (get) Token: 0x06000102 RID: 258 RVA: 0x000025C8 File Offset: 0x000007C8
		public bool IsKey
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value specifying whether the configuration attribute is locked.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Configuration.PropertyInformation" /> object is locked; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000060 RID: 96
		// (get) Token: 0x06000103 RID: 259 RVA: 0x000025E4 File Offset: 0x000007E4
		public bool IsLocked
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value specifying whether the configuration attribute has been modified.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Configuration.PropertyInformation" /> object has been modified; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000061 RID: 97
		// (get) Token: 0x06000104 RID: 260 RVA: 0x00002600 File Offset: 0x00000800
		public bool IsModified
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value specifying whether the configuration attribute is required.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Configuration.PropertyInformation" /> object is required; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000062 RID: 98
		// (get) Token: 0x06000105 RID: 261 RVA: 0x0000261C File Offset: 0x0000081C
		public bool IsRequired
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets the line number in the configuration file related to the configuration attribute.</summary>
		/// <returns>A line number of the configuration file.</returns>
		// Token: 0x17000063 RID: 99
		// (get) Token: 0x06000106 RID: 262 RVA: 0x00002638 File Offset: 0x00000838
		public int LineNumber
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the name of the object that corresponds to a configuration attribute.</summary>
		/// <returns>The name of the <see cref="T:System.Configuration.PropertyInformation" /> object.</returns>
		// Token: 0x17000064 RID: 100
		// (get) Token: 0x06000107 RID: 263 RVA: 0x00002057 File Offset: 0x00000257
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the source file that corresponds to a configuration attribute.</summary>
		/// <returns>The source file of the <see cref="T:System.Configuration.PropertyInformation" /> object.</returns>
		// Token: 0x17000065 RID: 101
		// (get) Token: 0x06000108 RID: 264 RVA: 0x00002057 File Offset: 0x00000257
		public string Source
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the <see cref="T:System.Type" /> of the object that corresponds to a configuration attribute.</summary>
		/// <returns>The <see cref="T:System.Type" /> of the <see cref="T:System.Configuration.PropertyInformation" /> object.</returns>
		// Token: 0x17000066 RID: 102
		// (get) Token: 0x06000109 RID: 265 RVA: 0x00002057 File Offset: 0x00000257
		public Type Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Configuration.ConfigurationValidatorBase" /> object related to the configuration attribute.</summary>
		/// <returns>A <see cref="T:System.Configuration.ConfigurationValidatorBase" /> object.</returns>
		// Token: 0x17000067 RID: 103
		// (get) Token: 0x0600010A RID: 266 RVA: 0x00002057 File Offset: 0x00000257
		public ConfigurationValidatorBase Validator
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets an object containing the value related to a configuration attribute.</summary>
		/// <returns>An object containing the value for the <see cref="T:System.Configuration.PropertyInformation" /> object.</returns>
		// Token: 0x17000068 RID: 104
		// (get) Token: 0x0600010B RID: 267 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x0600010C RID: 268 RVA: 0x00002050 File Offset: 0x00000250
		public object Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a <see cref="T:System.Configuration.PropertyValueOrigin" /> object related to the configuration attribute. </summary>
		/// <returns>A <see cref="T:System.Configuration.PropertyValueOrigin" /> object.</returns>
		// Token: 0x17000069 RID: 105
		// (get) Token: 0x0600010D RID: 269 RVA: 0x00002654 File Offset: 0x00000854
		public PropertyValueOrigin ValueOrigin
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return PropertyValueOrigin.Default;
			}
		}
	}
}
