﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides validation of an <see cref="T:System.Int64" /> value.</summary>
	// Token: 0x02000045 RID: 69
	public class LongValidator : ConfigurationValidatorBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.LongValidator" /> class. </summary>
		/// <param name="minValue">An <see cref="T:System.Int64" /> value that specifies the minimum length of the <see langword="long" /> value.</param>
		/// <param name="maxValue">An <see cref="T:System.Int64" /> value that specifies the maximum length of the <see langword="long" /> value.</param>
		// Token: 0x06000210 RID: 528 RVA: 0x00002050 File Offset: 0x00000250
		public LongValidator(long minValue, long maxValue)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.LongValidator" /> class. </summary>
		/// <param name="minValue">An <see cref="T:System.Int64" /> value that specifies the minimum length of the <see langword="long" /> value.</param>
		/// <param name="maxValue">An <see cref="T:System.Int64" /> value that specifies the maximum length of the <see langword="long" /> value.</param>
		/// <param name="rangeIsExclusive">A <see cref="T:System.Boolean" /> value that specifies whether the validation range is exclusive.</param>
		// Token: 0x06000211 RID: 529 RVA: 0x00002050 File Offset: 0x00000250
		public LongValidator(long minValue, long maxValue, bool rangeIsExclusive)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.LongValidator" /> class. </summary>
		/// <param name="minValue">An <see cref="T:System.Int64" /> value that specifies the minimum length of the <see langword="long" /> value.</param>
		/// <param name="maxValue">An <see cref="T:System.Int64" /> value that specifies the maximum length of the <see langword="long" /> value.</param>
		/// <param name="rangeIsExclusive">A <see cref="T:System.Boolean" /> value that specifies whether the validation range is exclusive.</param>
		/// <param name="resolution">An <see cref="T:System.Int64" /> value that specifies a specific value that must be matched.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="resolution" /> is equal to or less than <see langword="0" />.- or -
		///         <paramref name="maxValue" /> is less than <paramref name="minValue" />.</exception>
		// Token: 0x06000212 RID: 530 RVA: 0x00002050 File Offset: 0x00000250
		public LongValidator(long minValue, long maxValue, bool rangeIsExclusive, long resolution)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Determines whether the type of the object can be validated.</summary>
		/// <param name="type">The type of object.</param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="type" /> parameter matches an <see cref="T:System.Int64" /> value; otherwise, <see langword="false" />. </returns>
		// Token: 0x06000213 RID: 531 RVA: 0x00002B78 File Offset: 0x00000D78
		public override bool CanValidate(Type type)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Determines whether the value of an object is valid.</summary>
		/// <param name="value">The value of an object.</param>
		// Token: 0x06000214 RID: 532 RVA: 0x00002050 File Offset: 0x00000250
		public override void Validate(object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
