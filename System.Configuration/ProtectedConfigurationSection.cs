﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides programmatic access to the <see langword="configProtectedData" /> configuration section. This class cannot be inherited.</summary>
	// Token: 0x0200004D RID: 77
	public sealed class ProtectedConfigurationSection : ConfigurationSection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ProtectedConfigurationSection" /> class using default settings.</summary>
		// Token: 0x06000237 RID: 567 RVA: 0x00002050 File Offset: 0x00000250
		public ProtectedConfigurationSection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the name of the default <see cref="T:System.Configuration.ProtectedConfigurationProvider" /> object in the <see cref="P:System.Configuration.ProtectedConfigurationSection.Providers" /> collection property.</summary>
		/// <returns>The name of the default <see cref="T:System.Configuration.ProtectedConfigurationProvider" /> object in the <see cref="P:System.Configuration.ProtectedConfigurationSection.Providers" /> collection property. </returns>
		// Token: 0x170000CB RID: 203
		// (get) Token: 0x06000238 RID: 568 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x06000239 RID: 569 RVA: 0x00002050 File Offset: 0x00000250
		public string DefaultProvider
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x170000CC RID: 204
		// (get) Token: 0x0600023A RID: 570 RVA: 0x00002057 File Offset: 0x00000257
		protected internal override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Configuration.ProviderSettingsCollection" /> collection of all the <see cref="T:System.Configuration.ProtectedConfigurationProvider" /> objects in all participating configuration files.</summary>
		/// <returns>A <see cref="T:System.Configuration.ProviderSettingsCollection" /> collection of all the <see cref="T:System.Configuration.ProtectedConfigurationProvider" /> objects in all participating configuration files. </returns>
		// Token: 0x170000CD RID: 205
		// (get) Token: 0x0600023B RID: 571 RVA: 0x00002057 File Offset: 0x00000257
		public ProviderSettingsCollection Providers
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
