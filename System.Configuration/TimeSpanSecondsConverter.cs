﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Converts a time span expressed in seconds. </summary>
	// Token: 0x02000058 RID: 88
	public class TimeSpanSecondsConverter : ConfigurationConverterBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.TimeSpanSecondsConverter" /> class.</summary>
		// Token: 0x06000268 RID: 616 RVA: 0x00002050 File Offset: 0x00000250
		public TimeSpanSecondsConverter()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
