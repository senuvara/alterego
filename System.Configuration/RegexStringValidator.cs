﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides validation of a string based on the rules provided by a regular expression.</summary>
	// Token: 0x0200004F RID: 79
	public class RegexStringValidator : ConfigurationValidatorBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.RegexStringValidator" /> class. </summary>
		/// <param name="regex">A string that specifies a regular expression.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="regex" /> is null or an empty string ("").</exception>
		// Token: 0x0600023F RID: 575 RVA: 0x00002050 File Offset: 0x00000250
		public RegexStringValidator(string regex)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Determines whether the type of the object can be validated.</summary>
		/// <param name="type">The type of object.</param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="type" /> parameter matches a string; otherwise, <see langword="false" />. </returns>
		// Token: 0x06000240 RID: 576 RVA: 0x00002C04 File Offset: 0x00000E04
		public override bool CanValidate(Type type)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Determines whether the value of an object is valid.</summary>
		/// <param name="value">The value of an object.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="value" /> does not conform to the parameters of the <see cref="T:System.Text.RegularExpressions.Regex" /> class.</exception>
		// Token: 0x06000241 RID: 577 RVA: 0x00002050 File Offset: 0x00000250
		public override void Validate(object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
