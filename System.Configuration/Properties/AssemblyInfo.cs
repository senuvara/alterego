﻿using System;
using System.Configuration.Assemblies;
using System.Diagnostics;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

[assembly: AssemblyAlgorithmId(AssemblyHashAlgorithm.None)]
[assembly: AssemblyVersion("4.0.0.0")]
[assembly: SecurityRules(SecurityRuleSet.Level1, SkipVerificationInFullTrust = true)]
[assembly: AssemblyCompany("Mono development team")]
[assembly: AssemblyCopyright("(c) Various Mono authors")]
[assembly: AssemblyDefaultAlias("System.Configuration.dll")]
[assembly: AssemblyDescription("System.Configuration.dll")]
[assembly: AssemblyFileVersion("4.7.2558.0")]
[assembly: AssemblyInformationalVersion("4.7.2558.0")]
[assembly: CLSCompliant(true)]
[assembly: AssemblyProduct("Mono Common Language Infrastructure")]
[assembly: NeutralResourcesLanguage("en-US")]
[assembly: SatelliteContractVersion("4.0.0.0")]
[assembly: ComCompatibleVersion(1, 0, 3300, 0)]
[assembly: ComVisible(false)]
[assembly: DefaultDllImportSearchPaths(DllImportSearchPath.System32 | DllImportSearchPath.AssemblyDirectory)]
[assembly: AssemblyTitle("System.Configuration.dll")]
[assembly: AllowPartiallyTrustedCallers]
