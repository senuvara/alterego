﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents a group of configuration elements that configure the providers for the <see langword="&lt;configProtectedData&gt;" /> configuration section.</summary>
	// Token: 0x0200004E RID: 78
	public class ProtectedProviderSettings : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ProtectedProviderSettings" /> class. </summary>
		// Token: 0x0600023C RID: 572 RVA: 0x00002050 File Offset: 0x00000250
		public ProtectedProviderSettings()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a <see cref="T:System.Configuration.ConfigurationPropertyCollection" /> collection that represents the properties of the providers for the protected configuration data.</summary>
		/// <returns>A <see cref="T:System.Configuration.ConfigurationPropertyCollection" /> that represents the properties of the providers for the protected configuration data.</returns>
		// Token: 0x170000CE RID: 206
		// (get) Token: 0x0600023D RID: 573 RVA: 0x00002057 File Offset: 0x00000257
		protected internal override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a collection of <see cref="T:System.Configuration.ProviderSettings" /> objects that represent the properties of the providers for the protected configuration data.</summary>
		/// <returns>A collection of <see cref="T:System.Configuration.ProviderSettings" /> objects that represent the properties of the providers for the protected configuration data.</returns>
		// Token: 0x170000CF RID: 207
		// (get) Token: 0x0600023E RID: 574 RVA: 0x00002057 File Offset: 0x00000257
		public ProviderSettingsCollection Providers
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
