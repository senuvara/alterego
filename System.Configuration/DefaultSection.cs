﻿using System;
using System.Xml;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents a basic configuration-section handler that exposes the configuration section's XML for both read and write access.</summary>
	// Token: 0x0200003B RID: 59
	public sealed class DefaultSection : ConfigurationSection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.DefaultSection" /> class. </summary>
		// Token: 0x060001E7 RID: 487 RVA: 0x00002050 File Offset: 0x00000250
		public DefaultSection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x170000B4 RID: 180
		// (get) Token: 0x060001E8 RID: 488 RVA: 0x00002057 File Offset: 0x00000257
		protected internal override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x060001E9 RID: 489 RVA: 0x00002050 File Offset: 0x00000250
		protected internal override void DeserializeSection(XmlReader xmlReader)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060001EA RID: 490 RVA: 0x00002A7C File Offset: 0x00000C7C
		protected internal override bool IsModified()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x060001EB RID: 491 RVA: 0x00002050 File Offset: 0x00000250
		protected internal override void Reset(ConfigurationElement parentSection)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060001EC RID: 492 RVA: 0x00002050 File Offset: 0x00000250
		protected internal override void ResetModified()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060001ED RID: 493 RVA: 0x00002057 File Offset: 0x00000257
		protected internal override string SerializeSection(ConfigurationElement parentSection, string name, ConfigurationSaveMode saveMode)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
