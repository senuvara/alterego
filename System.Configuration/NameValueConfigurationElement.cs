﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>A configuration element that contains a <see cref="T:System.String" /> name and <see cref="T:System.String" /> value. This class cannot be inherited.</summary>
	// Token: 0x02000048 RID: 72
	public sealed class NameValueConfigurationElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.NameValueConfigurationElement" /> class based on supplied parameters.</summary>
		/// <param name="name">The name of the <see cref="T:System.Configuration.NameValueConfigurationElement" /> object.</param>
		/// <param name="value">The value of the <see cref="T:System.Configuration.NameValueConfigurationElement" /> object.</param>
		// Token: 0x06000228 RID: 552 RVA: 0x00002050 File Offset: 0x00000250
		public NameValueConfigurationElement(string name, string value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the name of the <see cref="T:System.Configuration.NameValueConfigurationElement" /> object.</summary>
		/// <returns>The name of the <see cref="T:System.Configuration.NameValueConfigurationElement" /> object.</returns>
		// Token: 0x170000C4 RID: 196
		// (get) Token: 0x06000229 RID: 553 RVA: 0x00002057 File Offset: 0x00000257
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x170000C5 RID: 197
		// (get) Token: 0x0600022A RID: 554 RVA: 0x00002057 File Offset: 0x00000257
		protected internal override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the value of the <see cref="T:System.Configuration.NameValueConfigurationElement" /> object.</summary>
		/// <returns>The value of the <see cref="T:System.Configuration.NameValueConfigurationElement" /> object.</returns>
		// Token: 0x170000C6 RID: 198
		// (get) Token: 0x0600022B RID: 555 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x0600022C RID: 556 RVA: 0x00002050 File Offset: 0x00000250
		public string Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
