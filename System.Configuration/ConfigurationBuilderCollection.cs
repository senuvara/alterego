﻿using System;
using System.Configuration.Provider;
using Unity;

namespace System.Configuration
{
	/// <summary>Maintains a collection of <see cref="T:System.Configuration.ConfigurationBuilder" /> objects by name.</summary>
	// Token: 0x0200002D RID: 45
	public class ConfigurationBuilderCollection : ProviderCollection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ConfigurationBuilderCollection" /> class.</summary>
		// Token: 0x0600017C RID: 380 RVA: 0x00002050 File Offset: 0x00000250
		public ConfigurationBuilderCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the <see cref="T:System.Configuration.ConfigurationBuilder" /> object from the <see cref="T:System.Configuration.ConfigurationBuilderCollection" /> that is configured with the provided name.</summary>
		/// <param name="name">A configuration builder name.</param>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationBuilder" /> object that is configured with the provided <paramref name="name" />.</returns>
		// Token: 0x17000095 RID: 149
		public ConfigurationBuilder this[string name]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Adds a <see cref="T:System.Configuration.ConfigurationBuilder" /> object to the <see cref="T:System.Configuration.ConfigurationBuilderCollection" /> object.</summary>
		/// <param name="builder">The <see cref="T:System.Configuration.ConfigurationBuilder" /> object to add to the collection.</param>
		/// <exception cref="T:System.ArgumentNullException">
		/// 			  <paramref name="builder" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         The configuration provider in <paramref name="builder" /> must implement the class <see cref="T:System.Configuration.ConfigurationBuilder" />.</exception>
		// Token: 0x0600017E RID: 382 RVA: 0x00002050 File Offset: 0x00000250
		public override void Add(ProviderBase builder)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
