﻿using System;
using System.Security;
using System.Security.Permissions;
using Unity;

namespace System.Configuration
{
	/// <summary>Creates a <see cref="T:System.Configuration.ConfigurationPermission" /> object that either grants or denies the marked target permission to access sections of configuration files.</summary>
	// Token: 0x02000039 RID: 57
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class ConfigurationPermissionAttribute : CodeAccessSecurityAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ConfigurationPermissionAttribute" /> class.</summary>
		/// <param name="action">The security action represented by an enumeration member of <see cref="T:System.Security.Permissions.SecurityAction" />. Determines the permission state of the attribute.</param>
		// Token: 0x060001D9 RID: 473 RVA: 0x000024C7 File Offset: 0x000006C7
		public ConfigurationPermissionAttribute(SecurityAction action)
		{
		}

		/// <summary>Creates and returns an object that implements the <see cref="T:System.Security.IPermission" /> interface.</summary>
		/// <returns>Returns an object that implements <see cref="T:System.Security.IPermission" />.</returns>
		// Token: 0x060001DA RID: 474 RVA: 0x00002057 File Offset: 0x00000257
		public override IPermission CreatePermission()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
