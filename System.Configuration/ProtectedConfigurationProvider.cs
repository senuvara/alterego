﻿using System;
using System.Configuration.Provider;
using System.Xml;
using Unity;

namespace System.Configuration
{
	/// <summary>Is the base class to create providers for encrypting and decrypting protected-configuration data.</summary>
	// Token: 0x02000023 RID: 35
	public abstract class ProtectedConfigurationProvider : ProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ProtectedConfigurationProvider" /> class using default settings.</summary>
		// Token: 0x06000151 RID: 337 RVA: 0x00002050 File Offset: 0x00000250
		protected ProtectedConfigurationProvider()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Decrypts the passed <see cref="T:System.Xml.XmlNode" /> object from a configuration file.</summary>
		/// <param name="encryptedNode">The <see cref="T:System.Xml.XmlNode" /> object to decrypt.</param>
		/// <returns>The <see cref="T:System.Xml.XmlNode" /> object containing decrypted data.</returns>
		// Token: 0x06000152 RID: 338
		public abstract XmlNode Decrypt(XmlNode encryptedNode);

		/// <summary>Encrypts the passed <see cref="T:System.Xml.XmlNode" /> object from a configuration file.</summary>
		/// <param name="node">The <see cref="T:System.Xml.XmlNode" /> object to encrypt.</param>
		/// <returns>The <see cref="T:System.Xml.XmlNode" /> object containing encrypted data.</returns>
		// Token: 0x06000153 RID: 339
		public abstract XmlNode Encrypt(XmlNode node);
	}
}
