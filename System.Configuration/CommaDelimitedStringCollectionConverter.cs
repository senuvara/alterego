﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Converts a comma-delimited string value to and from a <see cref="T:System.Configuration.CommaDelimitedStringCollection" /> object. This class cannot be inherited.</summary>
	// Token: 0x0200002B RID: 43
	public sealed class CommaDelimitedStringCollectionConverter : ConfigurationConverterBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.CommaDelimitedStringCollectionConverter" /> class. </summary>
		// Token: 0x0600017A RID: 378 RVA: 0x00002050 File Offset: 0x00000250
		public CommaDelimitedStringCollectionConverter()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
