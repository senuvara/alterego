﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides dynamic validation of an object.</summary>
	// Token: 0x02000026 RID: 38
	public sealed class CallbackValidator : ConfigurationValidatorBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.CallbackValidator" /> class.</summary>
		/// <param name="type">The type of object that will be validated.</param>
		/// <param name="callback">The <see cref="T:System.Configuration.ValidatorCallback" /> used as the delegate.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="type " />is <see langword="null" />.</exception>
		// Token: 0x06000165 RID: 357 RVA: 0x00002050 File Offset: 0x00000250
		public CallbackValidator(Type type, ValidatorCallback callback)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Determines whether the type of the object can be validated.</summary>
		/// <param name="type">The type of object.</param>
		/// <returns>
		///     <see langword="true" /> if the <see langword="type" /> parameter matches the type used as the first parameter when creating an instance of <see cref="T:System.Configuration.CallbackValidator" />; otherwise, <see langword="false" />. </returns>
		// Token: 0x06000166 RID: 358 RVA: 0x000028D8 File Offset: 0x00000AD8
		public override bool CanValidate(Type type)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Determines whether the value of an object is valid.</summary>
		/// <param name="value">The value of an object.</param>
		// Token: 0x06000167 RID: 359 RVA: 0x00002050 File Offset: 0x00000250
		public override void Validate(object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
