﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Manages the path context for the current application. This class cannot be inherited.</summary>
	// Token: 0x0200003E RID: 62
	public sealed class ExeContext
	{
		// Token: 0x060001F6 RID: 502 RVA: 0x00002050 File Offset: 0x00000250
		internal ExeContext()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the current path for the application.</summary>
		/// <returns>A string value containing the current path.</returns>
		// Token: 0x170000B6 RID: 182
		// (get) Token: 0x060001F7 RID: 503 RVA: 0x00002057 File Offset: 0x00000257
		public string ExePath
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets an object representing the path level of the current application.</summary>
		/// <returns>A <see cref="T:System.Configuration.ConfigurationUserLevel" /> object representing the path level of the current application.</returns>
		// Token: 0x170000B7 RID: 183
		// (get) Token: 0x060001F8 RID: 504 RVA: 0x00002AD0 File Offset: 0x00000CD0
		public ConfigurationUserLevel UserLevel
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ConfigurationUserLevel.None;
			}
		}
	}
}
