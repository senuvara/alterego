﻿using System;
using System.Collections.Specialized;
using System.Reflection;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents a collection of string elements separated by commas. This class cannot be inherited.</summary>
	// Token: 0x0200002A RID: 42
	[DefaultMember("Item")]
	public sealed class CommaDelimitedStringCollection : StringCollection
	{
		/// <summary>Creates a new instance of the <see cref="T:System.Configuration.CommaDelimitedStringCollection" /> class.</summary>
		// Token: 0x06000176 RID: 374 RVA: 0x00002050 File Offset: 0x00000250
		public CommaDelimitedStringCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a value that specifies whether the collection has been modified.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Configuration.CommaDelimitedStringCollection" /> has been modified; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000094 RID: 148
		// (get) Token: 0x06000177 RID: 375 RVA: 0x000028F4 File Offset: 0x00000AF4
		public bool IsModified
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Creates a copy of the collection.</summary>
		/// <returns>A copy of the <see cref="T:System.Configuration.CommaDelimitedStringCollection" />.</returns>
		// Token: 0x06000178 RID: 376 RVA: 0x00002057 File Offset: 0x00000257
		public CommaDelimitedStringCollection Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Sets the collection object to read-only.</summary>
		// Token: 0x06000179 RID: 377 RVA: 0x00002050 File Offset: 0x00000250
		public void SetReadOnly()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
