﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Declaratively instructs the .NET Framework to create an instance of a configuration element collection. This class cannot be inherited.</summary>
	// Token: 0x02000010 RID: 16
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Property)]
	public sealed class ConfigurationCollectionAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ConfigurationCollectionAttribute" /> class.</summary>
		/// <param name="itemType">The type of the property collection to create.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="itemType" /> is <see langword="null" />.</exception>
		// Token: 0x060000BC RID: 188 RVA: 0x000024C7 File Offset: 0x000006C7
		public ConfigurationCollectionAttribute(Type itemType)
		{
		}

		/// <summary>Gets or sets the name of the <see langword="&lt;add&gt;" /> configuration element.</summary>
		/// <returns>The name that substitutes the standard name "add" for the configuration item.</returns>
		// Token: 0x17000040 RID: 64
		// (get) Token: 0x060000BD RID: 189 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x060000BE RID: 190 RVA: 0x00002050 File Offset: 0x00000250
		public string AddItemName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name for the <see langword="&lt;clear&gt;" /> configuration element.</summary>
		/// <returns>The name that replaces the standard name "clear" for the configuration item.</returns>
		// Token: 0x17000041 RID: 65
		// (get) Token: 0x060000BF RID: 191 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x060000C0 RID: 192 RVA: 0x00002050 File Offset: 0x00000250
		public string ClearItemsName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the type of the <see cref="T:System.Configuration.ConfigurationCollectionAttribute" /> attribute.</summary>
		/// <returns>The type of the <see cref="T:System.Configuration.ConfigurationCollectionAttribute" />.</returns>
		// Token: 0x17000042 RID: 66
		// (get) Token: 0x060000C1 RID: 193 RVA: 0x000024CC File Offset: 0x000006CC
		// (set) Token: 0x060000C2 RID: 194 RVA: 0x00002050 File Offset: 0x00000250
		public ConfigurationElementCollectionType CollectionType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ConfigurationElementCollectionType.BasicMap;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the type of the collection element.</summary>
		/// <returns>The type of the collection element.</returns>
		// Token: 0x17000043 RID: 67
		// (get) Token: 0x060000C3 RID: 195 RVA: 0x00002057 File Offset: 0x00000257
		public Type ItemType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the name for the <see langword="&lt;remove&gt;" /> configuration element.</summary>
		/// <returns>The name that replaces the standard name "remove" for the configuration element.</returns>
		// Token: 0x17000044 RID: 68
		// (get) Token: 0x060000C4 RID: 196 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x060000C5 RID: 197 RVA: 0x00002050 File Offset: 0x00000250
		public string RemoveItemName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
