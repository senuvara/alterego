﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Defines the configuration file mapping for an .exe application. This class cannot be inherited.</summary>
	// Token: 0x02000037 RID: 55
	public sealed class ExeConfigurationFileMap : ConfigurationFileMap
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ExeConfigurationFileMap" /> class.</summary>
		// Token: 0x060001C9 RID: 457 RVA: 0x00002050 File Offset: 0x00000250
		public ExeConfigurationFileMap()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ExeConfigurationFileMap" /> class by using the specified machine configuration file name.</summary>
		/// <param name="machineConfigFileName">The name of the machine configuration file that includes the complete physical path (for example, c:\Windows\Microsoft.NET\Framework\v2.0.50727\CONFIG\machine.config).</param>
		// Token: 0x060001CA RID: 458 RVA: 0x00002050 File Offset: 0x00000250
		public ExeConfigurationFileMap(string machineConfigFileName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the name of the configuration file.</summary>
		/// <returns>The configuration file name.</returns>
		// Token: 0x170000AB RID: 171
		// (get) Token: 0x060001CB RID: 459 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x060001CC RID: 460 RVA: 0x00002050 File Offset: 0x00000250
		public string ExeConfigFilename
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the configuration file for the local user.</summary>
		/// <returns>The configuration file name.</returns>
		// Token: 0x170000AC RID: 172
		// (get) Token: 0x060001CD RID: 461 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x060001CE RID: 462 RVA: 0x00002050 File Offset: 0x00000250
		public string LocalUserConfigFilename
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the configuration file for the roaming user.</summary>
		/// <returns>The configuration file name.</returns>
		// Token: 0x170000AD RID: 173
		// (get) Token: 0x060001CF RID: 463 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x060001D0 RID: 464 RVA: 0x00002050 File Offset: 0x00000250
		public string RoamingUserConfigFilename
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Creates a copy of the existing <see cref="T:System.Configuration.ExeConfigurationFileMap" /> object.</summary>
		/// <returns>An <see cref="T:System.Configuration.ExeConfigurationFileMap" /> object.</returns>
		// Token: 0x060001D1 RID: 465 RVA: 0x00002057 File Offset: 0x00000257
		public override object Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
