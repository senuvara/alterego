﻿using System;
using System.Collections;
using Unity;

namespace System.Configuration
{
	/// <summary>Contains a collection of <see cref="T:System.Configuration.ConfigurationLocationCollection" /> objects.</summary>
	// Token: 0x02000012 RID: 18
	public class ConfigurationLocationCollection : ReadOnlyCollectionBase
	{
		// Token: 0x060000CA RID: 202 RVA: 0x00002050 File Offset: 0x00000250
		internal ConfigurationLocationCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the <see cref="T:System.Configuration.ConfigurationLocationCollection" /> object at the specified index.</summary>
		/// <param name="index">The index location of the <see cref="T:System.Configuration.ConfigurationLocationCollection" /> to return.</param>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationLocationCollection" /> at the specified index.</returns>
		// Token: 0x17000047 RID: 71
		public ConfigurationLocation this[int index]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
