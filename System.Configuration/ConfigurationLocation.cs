﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents a <see langword="location" /> element within a configuration file.</summary>
	// Token: 0x02000013 RID: 19
	public class ConfigurationLocation
	{
		// Token: 0x060000CC RID: 204 RVA: 0x00002050 File Offset: 0x00000250
		internal ConfigurationLocation()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the relative path to the resource whose configuration settings are represented by this <see cref="T:System.Configuration.ConfigurationLocation" /> object.</summary>
		/// <returns>The relative path to the resource whose configuration settings are represented by this <see cref="T:System.Configuration.ConfigurationLocation" />.</returns>
		// Token: 0x17000048 RID: 72
		// (get) Token: 0x060000CD RID: 205 RVA: 0x00002057 File Offset: 0x00000257
		public string Path
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Creates an instance of a Configuration object.</summary>
		/// <returns>A Configuration object.</returns>
		// Token: 0x060000CE RID: 206 RVA: 0x00002057 File Offset: 0x00000257
		public Configuration OpenConfiguration()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
