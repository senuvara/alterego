﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents a method to be called after the validation of an object.</summary>
	/// <param name="value">The callback method.</param>
	// Token: 0x02000027 RID: 39
	public sealed class ValidatorCallback : MulticastDelegate
	{
		// Token: 0x06000168 RID: 360 RVA: 0x00002050 File Offset: 0x00000250
		public ValidatorCallback(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06000169 RID: 361 RVA: 0x00002050 File Offset: 0x00000250
		public void Invoke(object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0600016A RID: 362 RVA: 0x00002057 File Offset: 0x00000257
		public IAsyncResult BeginInvoke(object value, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x0600016B RID: 363 RVA: 0x00002050 File Offset: 0x00000250
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
