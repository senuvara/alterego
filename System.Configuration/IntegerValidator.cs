﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides validation of an <see cref="T:System.Int32" /> value.</summary>
	// Token: 0x02000043 RID: 67
	public class IntegerValidator : ConfigurationValidatorBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.IntegerValidator" /> class. </summary>
		/// <param name="minValue">An <see cref="T:System.Int32" /> object that specifies the minimum value.</param>
		/// <param name="maxValue">An <see cref="T:System.Int32" /> object that specifies the maximum value.</param>
		// Token: 0x06000203 RID: 515 RVA: 0x00002050 File Offset: 0x00000250
		public IntegerValidator(int minValue, int maxValue)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.IntegerValidator" /> class. </summary>
		/// <param name="minValue">An <see cref="T:System.Int32" /> object that specifies the minimum value.</param>
		/// <param name="maxValue">An <see cref="T:System.Int32" /> object that specifies the maximum value.</param>
		/// <param name="rangeIsExclusive">
		///       <see langword="true" /> to specify that the validation range is exclusive. Inclusive means the value to be validated must be within the specified range; exclusive means that it must be below the minimum or above the maximum.</param>
		// Token: 0x06000204 RID: 516 RVA: 0x00002050 File Offset: 0x00000250
		public IntegerValidator(int minValue, int maxValue, bool rangeIsExclusive)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.IntegerValidator" /> class. </summary>
		/// <param name="minValue">An <see cref="T:System.Int32" /> object that specifies the minimum length of the integer value.</param>
		/// <param name="maxValue">An <see cref="T:System.Int32" /> object that specifies the maximum length of the integer value.</param>
		/// <param name="rangeIsExclusive">A <see cref="T:System.Boolean" /> value that specifies whether the validation range is exclusive.</param>
		/// <param name="resolution">An <see cref="T:System.Int32" /> object that specifies a value that must be matched.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="resolution" /> is less than <see langword="0" />.- or -
		///         <paramref name="minValue" /> is greater than <paramref name="maxValue" />.</exception>
		// Token: 0x06000205 RID: 517 RVA: 0x00002050 File Offset: 0x00000250
		public IntegerValidator(int minValue, int maxValue, bool rangeIsExclusive, int resolution)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Determines whether the type of the object can be validated.</summary>
		/// <param name="type">The type of the object.</param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="type" /> parameter matches an <see cref="T:System.Int32" /> value; otherwise, <see langword="false" />. </returns>
		// Token: 0x06000206 RID: 518 RVA: 0x00002B08 File Offset: 0x00000D08
		public override bool CanValidate(Type type)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Determines whether the value of an object is valid.</summary>
		/// <param name="value">The value to be validated.</param>
		// Token: 0x06000207 RID: 519 RVA: 0x00002050 File Offset: 0x00000250
		public override void Validate(object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
