﻿using System;
using System.Xml;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides configuration system support for the <see langword="appSettings" /> configuration section. This class cannot be inherited.</summary>
	// Token: 0x02000002 RID: 2
	public sealed class AppSettingsSection : ConfigurationSection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.AppSettingsSection" /> class.</summary>
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public AppSettingsSection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets a configuration file that provides additional settings or overrides the settings specified in the <see langword="appSettings" /> element.</summary>
		/// <returns>A configuration file that provides additional settings or overrides the settings specified in the <see langword="appSettings" /> element.</returns>
		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000002 RID: 2 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x06000003 RID: 3 RVA: 0x00002050 File Offset: 0x00000250
		public string File
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000004 RID: 4 RVA: 0x00002057 File Offset: 0x00000257
		protected internal override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a collection of key/value pairs that contains application settings.</summary>
		/// <returns>A collection of key/value pairs that contains the application settings from the configuration file.</returns>
		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000005 RID: 5 RVA: 0x00002057 File Offset: 0x00000257
		public KeyValueConfigurationCollection Settings
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x06000006 RID: 6 RVA: 0x00002050 File Offset: 0x00000250
		protected internal override void DeserializeElement(XmlReader reader, bool serializeCollectionKey)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06000007 RID: 7 RVA: 0x00002057 File Offset: 0x00000257
		protected internal override object GetRuntimeObject()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06000008 RID: 8 RVA: 0x00002060 File Offset: 0x00000260
		protected internal override bool IsModified()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x06000009 RID: 9 RVA: 0x00002050 File Offset: 0x00000250
		protected internal override void Reset(ConfigurationElement parentSection)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0600000A RID: 10 RVA: 0x00002057 File Offset: 0x00000257
		protected internal override string SerializeSection(ConfigurationElement parentElement, string name, ConfigurationSaveMode saveMode)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
