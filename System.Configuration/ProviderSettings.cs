﻿using System;
using System.Collections.Specialized;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents the configuration elements associated with a provider.</summary>
	// Token: 0x02000031 RID: 49
	public sealed class ProviderSettings : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ProviderSettings" /> class. </summary>
		// Token: 0x06000198 RID: 408 RVA: 0x00002050 File Offset: 0x00000250
		public ProviderSettings()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ProviderSettings" /> class. </summary>
		/// <param name="name">The name of the provider to configure settings for.</param>
		/// <param name="type">The type of the provider to configure settings for.</param>
		// Token: 0x06000199 RID: 409 RVA: 0x00002050 File Offset: 0x00000250
		public ProviderSettings(string name, string type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the name of the provider configured by this class.</summary>
		/// <returns>The name of the provider.</returns>
		// Token: 0x1700009E RID: 158
		// (get) Token: 0x0600019A RID: 410 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x0600019B RID: 411 RVA: 0x00002050 File Offset: 0x00000250
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a collection of user-defined parameters for the provider.</summary>
		/// <returns>A <see cref="T:System.Collections.Specialized.NameValueCollection" /> of parameters for the provider.</returns>
		// Token: 0x1700009F RID: 159
		// (get) Token: 0x0600019C RID: 412 RVA: 0x00002057 File Offset: 0x00000257
		public NameValueCollection Parameters
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x170000A0 RID: 160
		// (get) Token: 0x0600019D RID: 413 RVA: 0x00002057 File Offset: 0x00000257
		protected internal override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the type of the provider configured by this class.</summary>
		/// <returns>The fully qualified namespace and class name for the type of provider configured by this <see cref="T:System.Configuration.ProviderSettings" /> instance.</returns>
		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x0600019E RID: 414 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x0600019F RID: 415 RVA: 0x00002050 File Offset: 0x00000250
		public string Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x060001A0 RID: 416 RVA: 0x00002948 File Offset: 0x00000B48
		protected internal override bool IsModified()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x060001A1 RID: 417 RVA: 0x00002964 File Offset: 0x00000B64
		protected override bool OnDeserializeUnrecognizedAttribute(string name, string value)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x060001A2 RID: 418 RVA: 0x00002050 File Offset: 0x00000250
		protected internal override void Reset(ConfigurationElement parentElement)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060001A3 RID: 419 RVA: 0x00002050 File Offset: 0x00000250
		protected internal override void Unmerge(ConfigurationElement sourceElement, ConfigurationElement parentElement, ConfigurationSaveMode saveMode)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
