﻿using System;
using System.Runtime.Versioning;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents a group of related sections within a configuration file.</summary>
	// Token: 0x02000014 RID: 20
	public class ConfigurationSectionGroup
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ConfigurationSectionGroup" /> class. </summary>
		// Token: 0x060000CF RID: 207 RVA: 0x00002050 File Offset: 0x00000250
		public ConfigurationSectionGroup()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a value that indicates whether this <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object declaration is required. </summary>
		/// <returns>
		///     <see langword="true" /> if this <see cref="T:System.Configuration.ConfigurationSectionGroup" /> declaration is required; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000049 RID: 73
		// (get) Token: 0x060000D0 RID: 208 RVA: 0x00002504 File Offset: 0x00000704
		public bool IsDeclarationRequired
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value that indicates whether this <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object is declared.</summary>
		/// <returns>
		///     <see langword="true" /> if this <see cref="T:System.Configuration.ConfigurationSectionGroup" /> is declared; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x1700004A RID: 74
		// (get) Token: 0x060000D1 RID: 209 RVA: 0x00002520 File Offset: 0x00000720
		public bool IsDeclared
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets the name property of this <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object.</summary>
		/// <returns>The name property of this <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object.</returns>
		// Token: 0x1700004B RID: 75
		// (get) Token: 0x060000D2 RID: 210 RVA: 0x00002057 File Offset: 0x00000257
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the section group name associated with this <see cref="T:System.Configuration.ConfigurationSectionGroup" />.</summary>
		/// <returns>The section group name of this <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object.</returns>
		// Token: 0x1700004C RID: 76
		// (get) Token: 0x060000D3 RID: 211 RVA: 0x00002057 File Offset: 0x00000257
		public string SectionGroupName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Configuration.ConfigurationSectionGroupCollection" /> object that contains all the <see cref="T:System.Configuration.ConfigurationSectionGroup" /> objects that are children of this <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object.</summary>
		/// <returns>A <see cref="T:System.Configuration.ConfigurationSectionGroupCollection" /> object that contains all the <see cref="T:System.Configuration.ConfigurationSectionGroup" /> objects that are children of this <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object.</returns>
		// Token: 0x1700004D RID: 77
		// (get) Token: 0x060000D4 RID: 212 RVA: 0x00002057 File Offset: 0x00000257
		public ConfigurationSectionGroupCollection SectionGroups
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.Configuration.ConfigurationSectionCollection" /> object that contains all of <see cref="T:System.Configuration.ConfigurationSection" /> objects within this <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object.</summary>
		/// <returns>A <see cref="T:System.Configuration.ConfigurationSectionCollection" /> object that contains all the <see cref="T:System.Configuration.ConfigurationSection" /> objects within this <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object.</returns>
		// Token: 0x1700004E RID: 78
		// (get) Token: 0x060000D5 RID: 213 RVA: 0x00002057 File Offset: 0x00000257
		public ConfigurationSectionCollection Sections
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the type for this <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object.</summary>
		/// <returns>The type of this <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object is the root section group.- or -The <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object has a location.</exception>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The section or group is already defined at another level.</exception>
		// Token: 0x1700004F RID: 79
		// (get) Token: 0x060000D6 RID: 214 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x060000D7 RID: 215 RVA: 0x00002050 File Offset: 0x00000250
		public string Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Forces the declaration for this <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object.</summary>
		// Token: 0x060000D8 RID: 216 RVA: 0x00002050 File Offset: 0x00000250
		public void ForceDeclaration()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Forces the declaration for this <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object.</summary>
		/// <param name="force">
		///       <see langword="true" /> if the <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object must be written to the file; otherwise, <see langword="false" />.</param>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object is the root section group.- or -The <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object has a location.</exception>
		// Token: 0x060000D9 RID: 217 RVA: 0x00002050 File Offset: 0x00000250
		public void ForceDeclaration(bool force)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Indicates whether the current <see cref="T:System.Configuration.ConfigurationSectionGroup" /> instance should be serialized when the configuration object hierarchy is serialized for the specified target version of the .NET Framework.</summary>
		/// <param name="targetFramework">The target version of the .NET Framework.</param>
		/// <returns>
		///     <see langword="true" /> if the current section group should be serialized; otherwise, <see langword="false" />.</returns>
		// Token: 0x060000DA RID: 218 RVA: 0x0000253C File Offset: 0x0000073C
		protected internal virtual bool ShouldSerializeSectionGroupInTargetVersion(FrameworkName targetFramework)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}
	}
}
