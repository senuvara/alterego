﻿using System;
using System.Collections;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents a collection of configuration-element properties.</summary>
	// Token: 0x0200000C RID: 12
	public class ConfigurationPropertyCollection : ICollection, IEnumerable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ConfigurationPropertyCollection" /> class. </summary>
		// Token: 0x0600009C RID: 156 RVA: 0x00002050 File Offset: 0x00000250
		public ConfigurationPropertyCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the number of properties in the collection.</summary>
		/// <returns>The number of properties in the collection.</returns>
		// Token: 0x17000030 RID: 48
		// (get) Token: 0x0600009D RID: 157 RVA: 0x00002394 File Offset: 0x00000594
		public int Count
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets a value indicating whether access to the collection is synchronized (thread safe).</summary>
		/// <returns>
		///     <see langword="true" /> if access to the <see cref="T:System.Configuration.ConfigurationPropertyCollection" /> is synchronized; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000031 RID: 49
		// (get) Token: 0x0600009E RID: 158 RVA: 0x000023B0 File Offset: 0x000005B0
		public bool IsSynchronized
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets the collection item with the specified name.</summary>
		/// <param name="name">The <see cref="T:System.Configuration.ConfigurationProperty" /> to return. </param>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationProperty" /> with the specified <paramref name="name" />.</returns>
		// Token: 0x17000032 RID: 50
		public ConfigurationProperty this[string name]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the object to synchronize access to the collection.</summary>
		/// <returns>The object to synchronize access to the collection.</returns>
		// Token: 0x17000033 RID: 51
		// (get) Token: 0x060000A0 RID: 160 RVA: 0x00002057 File Offset: 0x00000257
		public object SyncRoot
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Adds a configuration property to the collection.</summary>
		/// <param name="property">The <see cref="T:System.Configuration.ConfigurationProperty" />  to add. </param>
		// Token: 0x060000A1 RID: 161 RVA: 0x00002050 File Offset: 0x00000250
		public void Add(ConfigurationProperty property)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes all configuration property objects from the collection.</summary>
		// Token: 0x060000A2 RID: 162 RVA: 0x00002050 File Offset: 0x00000250
		public void Clear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Specifies whether the configuration property is contained in this collection.</summary>
		/// <param name="name">An identifier for the <see cref="T:System.Configuration.ConfigurationProperty" /> to verify. </param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.Configuration.ConfigurationProperty" /> is contained in the collection; otherwise, <see langword="false" />.</returns>
		// Token: 0x060000A3 RID: 163 RVA: 0x000023CC File Offset: 0x000005CC
		public bool Contains(string name)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Copies this ConfigurationPropertyCollection to an array.</summary>
		/// <param name="array">Array to which to copy.</param>
		/// <param name="index">Index at which to begin copying.</param>
		// Token: 0x060000A4 RID: 164 RVA: 0x00002050 File Offset: 0x00000250
		public void CopyTo(ConfigurationProperty[] array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the <see cref="T:System.Collections.IEnumerator" /> object as it applies to the collection.</summary>
		/// <returns>The <see cref="T:System.Collections.IEnumerator" /> object as it applies to the collection</returns>
		// Token: 0x060000A5 RID: 165 RVA: 0x00002057 File Offset: 0x00000257
		public IEnumerator GetEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Removes a configuration property from the collection.</summary>
		/// <param name="name">The <see cref="T:System.Configuration.ConfigurationProperty" /> to remove. </param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.Configuration.ConfigurationProperty" /> was removed; otherwise, <see langword="false" />.</returns>
		// Token: 0x060000A6 RID: 166 RVA: 0x000023E8 File Offset: 0x000005E8
		public bool Remove(string name)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Copies this collection to an array.</summary>
		/// <param name="array">The array to which to copy.</param>
		/// <param name="index">The index location at which to begin copying.</param>
		// Token: 0x060000A7 RID: 167 RVA: 0x00002050 File Offset: 0x00000250
		void ICollection.CopyTo(Array array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
