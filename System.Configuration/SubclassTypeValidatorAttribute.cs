﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Declaratively instructs the .NET Framework to perform validation on a configuration property. This class cannot be inherited.</summary>
	// Token: 0x02000055 RID: 85
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class SubclassTypeValidatorAttribute : ConfigurationValidatorAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.SubclassTypeValidatorAttribute" /> class. </summary>
		/// <param name="baseClass">The base class type.</param>
		// Token: 0x06000263 RID: 611 RVA: 0x000024C7 File Offset: 0x000006C7
		public SubclassTypeValidatorAttribute(Type baseClass)
		{
		}

		/// <summary>Gets the base type of the object being validated.</summary>
		/// <returns>The base type of the object being validated.</returns>
		// Token: 0x170000DC RID: 220
		// (get) Token: 0x06000264 RID: 612 RVA: 0x00002057 File Offset: 0x00000257
		public Type BaseClass
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the validator attribute instance.</summary>
		/// <returns>The current <see cref="T:System.Configuration.ConfigurationValidatorBase" /> instance.</returns>
		// Token: 0x170000DD RID: 221
		// (get) Token: 0x06000265 RID: 613 RVA: 0x00002057 File Offset: 0x00000257
		public override ConfigurationValidatorBase ValidatorInstance
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
