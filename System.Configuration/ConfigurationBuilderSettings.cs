﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents a group of configuration elements that configure the providers for the <see langword="&lt;configBuilders&gt;" /> configuration section.</summary>
	// Token: 0x0200002F RID: 47
	public class ConfigurationBuilderSettings : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ConfigurationBuilderSettings" /> class.</summary>
		// Token: 0x0600018B RID: 395 RVA: 0x00002050 File Offset: 0x00000250
		public ConfigurationBuilderSettings()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a collection of <see cref="T:System.Configuration.ConfigurationBuilderSettings" /> objects that represent the properties of configuration builders.</summary>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationBuilder" /> objects.</returns>
		// Token: 0x1700009A RID: 154
		// (get) Token: 0x0600018C RID: 396 RVA: 0x00002057 File Offset: 0x00000257
		public ProviderSettingsCollection Builders
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the <see cref="T:System.Configuration.ConfigurationPropertyCollection" /> of a <see cref="T:System.Configuration.ConfigurationElement" />.</summary>
		/// <returns>A <see cref="T:System.Configuration.ConfigurationPropertyCollection" /> of a <see cref="T:System.Configuration.ConfigurationElement" />.</returns>
		// Token: 0x1700009B RID: 155
		// (get) Token: 0x0600018D RID: 397 RVA: 0x00002057 File Offset: 0x00000257
		protected internal override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
