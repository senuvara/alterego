﻿using System;

namespace Unity
{
	// Token: 0x0200006F RID: 111
	internal sealed class ThrowStub : ObjectDisposedException
	{
		// Token: 0x06000315 RID: 789 RVA: 0x00002F2F File Offset: 0x0000112F
		public static void ThrowNotSupportedException()
		{
			throw new PlatformNotSupportedException();
		}
	}
}
