﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Encapsulates the context information that is associated with a <see cref="T:System.Configuration.ConfigurationElement" /> object. This class cannot be inherited.</summary>
	// Token: 0x02000011 RID: 17
	public sealed class ContextInformation
	{
		// Token: 0x060000C6 RID: 198 RVA: 0x00002050 File Offset: 0x00000250
		internal ContextInformation()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the context of the environment where the configuration property is being evaluated.</summary>
		/// <returns>An object specifying the environment where the configuration property is being evaluated.</returns>
		// Token: 0x17000045 RID: 69
		// (get) Token: 0x060000C7 RID: 199 RVA: 0x00002057 File Offset: 0x00000257
		public object HostingContext
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a value specifying whether the configuration property is being evaluated at the machine configuration level.</summary>
		/// <returns>
		///     <see langword="true" /> if the configuration property is being evaluated at the machine configuration level; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000046 RID: 70
		// (get) Token: 0x060000C8 RID: 200 RVA: 0x000024E8 File Offset: 0x000006E8
		public bool IsMachineLevel
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Provides an object containing configuration-section information based on the specified section name.</summary>
		/// <param name="sectionName">The name of the configuration section.</param>
		/// <returns>An object containing the specified section within the configuration.</returns>
		// Token: 0x060000C9 RID: 201 RVA: 0x00002057 File Offset: 0x00000257
		public object GetSection(string sectionName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
