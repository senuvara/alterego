﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Converts a <see cref="T:System.TimeSpan" /> expressed in seconds or as a standard infinite time span.</summary>
	// Token: 0x02000059 RID: 89
	public sealed class TimeSpanSecondsOrInfiniteConverter : TimeSpanSecondsConverter
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.TimeSpanSecondsOrInfiniteConverter" /> class.</summary>
		// Token: 0x06000269 RID: 617 RVA: 0x00002050 File Offset: 0x00000250
		public TimeSpanSecondsOrInfiniteConverter()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
