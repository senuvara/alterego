﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides validation of a string.</summary>
	// Token: 0x02000052 RID: 82
	public class StringValidator : ConfigurationValidatorBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.StringValidator" /> class, based on a supplied parameter.</summary>
		/// <param name="minLength">An integer that specifies the minimum length of the string value.</param>
		// Token: 0x06000253 RID: 595 RVA: 0x00002050 File Offset: 0x00000250
		public StringValidator(int minLength)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.StringValidator" /> class, based on supplied parameters.</summary>
		/// <param name="minLength">An integer that specifies the minimum length of the string value.</param>
		/// <param name="maxLength">An integer that specifies the maximum length of the string value.</param>
		// Token: 0x06000254 RID: 596 RVA: 0x00002050 File Offset: 0x00000250
		public StringValidator(int minLength, int maxLength)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.StringValidator" /> class, based on supplied parameters.</summary>
		/// <param name="minLength">An integer that specifies the minimum length of the string value.</param>
		/// <param name="maxLength">An integer that specifies the maximum length of the string value.</param>
		/// <param name="invalidCharacters">A string that represents invalid characters. </param>
		// Token: 0x06000255 RID: 597 RVA: 0x00002050 File Offset: 0x00000250
		public StringValidator(int minLength, int maxLength, string invalidCharacters)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Determines whether an object can be validated based on type.</summary>
		/// <param name="type">The object type.</param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="type" /> parameter matches a string; otherwise, <see langword="false" />. </returns>
		// Token: 0x06000256 RID: 598 RVA: 0x00002C90 File Offset: 0x00000E90
		public override bool CanValidate(Type type)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Determines whether the value of an object is valid. </summary>
		/// <param name="value">The object value.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="value" /> is less than <paramref name="minValue" /> or greater than <paramref name="maxValue" /> as defined in the constructor.- or -
		///         <paramref name="value" /> contains invalid characters.</exception>
		// Token: 0x06000257 RID: 599 RVA: 0x00002050 File Offset: 0x00000250
		public override void Validate(object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
