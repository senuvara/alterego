﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Contains metadata about an individual section within the configuration hierarchy. This class cannot be inherited.</summary>
	// Token: 0x0200001D RID: 29
	public sealed class SectionInformation
	{
		// Token: 0x06000120 RID: 288 RVA: 0x00002050 File Offset: 0x00000250
		internal SectionInformation()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets a value that indicates where in the configuration file hierarchy the associated configuration section can be defined. </summary>
		/// <returns>A value that indicates where in the configuration file hierarchy the associated <see cref="T:System.Configuration.ConfigurationSection" /> object can be declared.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The selected value conflicts with a value that is already defined.</exception>
		// Token: 0x17000071 RID: 113
		// (get) Token: 0x06000121 RID: 289 RVA: 0x00002718 File Offset: 0x00000918
		// (set) Token: 0x06000122 RID: 290 RVA: 0x00002050 File Offset: 0x00000250
		public ConfigurationAllowDefinition AllowDefinition
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ConfigurationAllowDefinition.MachineOnly;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates where in the configuration file hierarchy the associated configuration section can be declared.</summary>
		/// <returns>A value that indicates where in the configuration file hierarchy the associated <see cref="T:System.Configuration.ConfigurationSection" /> object can be declared for .exe files.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The selected value conflicts with a value that is already defined.</exception>
		// Token: 0x17000072 RID: 114
		// (get) Token: 0x06000123 RID: 291 RVA: 0x00002734 File Offset: 0x00000934
		// (set) Token: 0x06000124 RID: 292 RVA: 0x00002050 File Offset: 0x00000250
		public ConfigurationAllowExeDefinition AllowExeDefinition
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ConfigurationAllowExeDefinition.MachineOnly;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates whether the configuration section allows the <see langword="location" /> attribute.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see langword="location" /> attribute is allowed; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The selected value conflicts with a value that is already defined.</exception>
		// Token: 0x17000073 RID: 115
		// (get) Token: 0x06000125 RID: 293 RVA: 0x00002750 File Offset: 0x00000950
		// (set) Token: 0x06000126 RID: 294 RVA: 0x00002050 File Offset: 0x00000250
		public bool AllowLocation
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates whether the associated configuration section can be overridden by lower-level configuration files.</summary>
		/// <returns>
		///     <see langword="true" /> if the section can be overridden; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000074 RID: 116
		// (get) Token: 0x06000127 RID: 295 RVA: 0x0000276C File Offset: 0x0000096C
		// (set) Token: 0x06000128 RID: 296 RVA: 0x00002050 File Offset: 0x00000250
		public bool AllowOverride
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the include file in which the associated configuration section is defined, if such a file exists.</summary>
		/// <returns>The name of the include file in which the associated <see cref="T:System.Configuration.ConfigurationSection" /> is defined, if such a file exists; otherwise, an empty string ("").</returns>
		// Token: 0x17000075 RID: 117
		// (get) Token: 0x06000129 RID: 297 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x0600012A RID: 298 RVA: 0x00002050 File Offset: 0x00000250
		public string ConfigSource
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the <see cref="T:System.Configuration.ConfigurationBuilder" /> object for this configuration section.</summary>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationBuilder" /> object for this configuration section.</returns>
		// Token: 0x17000076 RID: 118
		// (get) Token: 0x0600012B RID: 299 RVA: 0x00002057 File Offset: 0x00000257
		public ConfigurationBuilder ConfigurationBuilder
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets a value that indicates whether the associated configuration section will be saved even if it has not been modified.</summary>
		/// <returns>
		///     <see langword="true" /> if the associated <see cref="T:System.Configuration.ConfigurationSection" /> object will be saved even if it has not been modified; otherwise, <see langword="false" />. The default is <see langword="false" />.If the configuration file is saved (even if there are no modifications), ASP.NET restarts the application.</returns>
		// Token: 0x17000077 RID: 119
		// (get) Token: 0x0600012C RID: 300 RVA: 0x00002788 File Offset: 0x00000988
		// (set) Token: 0x0600012D RID: 301 RVA: 0x00002050 File Offset: 0x00000250
		public bool ForceSave
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates whether the settings that are specified in the associated configuration section are inherited by applications that reside in a subdirectory of the relevant application.</summary>
		/// <returns>
		///     <see langword="true" /> if the settings specified in this <see cref="T:System.Configuration.ConfigurationSection" /> object are inherited by child applications; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x17000078 RID: 120
		// (get) Token: 0x0600012E RID: 302 RVA: 0x000027A4 File Offset: 0x000009A4
		// (set) Token: 0x0600012F RID: 303 RVA: 0x00002050 File Offset: 0x00000250
		public bool InheritInChildApplications
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a value that indicates whether the configuration section must be declared in the configuration file.</summary>
		/// <returns>
		///     <see langword="true" /> if the associated <see cref="T:System.Configuration.ConfigurationSection" /> object must be declared in the configuration file; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000079 RID: 121
		// (get) Token: 0x06000130 RID: 304 RVA: 0x000027C0 File Offset: 0x000009C0
		public bool IsDeclarationRequired
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value that indicates whether the associated configuration section is declared in the configuration file.</summary>
		/// <returns>
		///     <see langword="true" /> if this <see cref="T:System.Configuration.ConfigurationSection" /> is declared in the configuration file; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x1700007A RID: 122
		// (get) Token: 0x06000131 RID: 305 RVA: 0x000027DC File Offset: 0x000009DC
		public bool IsDeclared
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value that indicates whether the associated configuration section is locked.</summary>
		/// <returns>
		///     <see langword="true" /> if the section is locked; otherwise, <see langword="false" />. </returns>
		// Token: 0x1700007B RID: 123
		// (get) Token: 0x06000132 RID: 306 RVA: 0x000027F8 File Offset: 0x000009F8
		public bool IsLocked
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value that indicates whether the associated configuration section is protected.</summary>
		/// <returns>
		///     <see langword="true" /> if this <see cref="T:System.Configuration.ConfigurationSection" /> is protected; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x1700007C RID: 124
		// (get) Token: 0x06000133 RID: 307 RVA: 0x00002814 File Offset: 0x00000A14
		public bool IsProtected
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets the name of the associated configuration section.</summary>
		/// <returns>The complete name of the configuration section.</returns>
		// Token: 0x1700007D RID: 125
		// (get) Token: 0x06000134 RID: 308 RVA: 0x00002057 File Offset: 0x00000257
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Configuration.OverrideMode" /> enumeration value that specifies whether the associated configuration section can be overridden by child configuration files.</summary>
		/// <returns>One of the <see cref="T:System.Configuration.OverrideMode" /> enumeration values.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">An attempt was made to change both the <see cref="P:System.Configuration.SectionInformation.AllowOverride" /> and <see cref="P:System.Configuration.SectionInformation.OverrideMode" /> properties, which is not supported for compatibility reasons. </exception>
		// Token: 0x1700007E RID: 126
		// (get) Token: 0x06000135 RID: 309 RVA: 0x00002830 File Offset: 0x00000A30
		// (set) Token: 0x06000136 RID: 310 RVA: 0x00002050 File Offset: 0x00000250
		public OverrideMode OverrideMode
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return OverrideMode.Inherit;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that specifies the default override behavior of a configuration section by child configuration files.</summary>
		/// <returns>One of the <see cref="T:System.Configuration.OverrideMode" /> enumeration values.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The override behavior is specified in a parent configuration section.</exception>
		// Token: 0x1700007F RID: 127
		// (get) Token: 0x06000137 RID: 311 RVA: 0x0000284C File Offset: 0x00000A4C
		// (set) Token: 0x06000138 RID: 312 RVA: 0x00002050 File Offset: 0x00000250
		public OverrideMode OverrideModeDefault
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return OverrideMode.Inherit;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the override behavior of a configuration section that is in turn based on whether child configuration files can lock the configuration section. </summary>
		/// <returns>One of the <see cref="T:System.Configuration.OverrideMode" /> enumeration values.</returns>
		// Token: 0x17000080 RID: 128
		// (get) Token: 0x06000139 RID: 313 RVA: 0x00002868 File Offset: 0x00000A68
		public OverrideMode OverrideModeEffective
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return OverrideMode.Inherit;
			}
		}

		/// <summary>Gets the protected configuration provider for the associated configuration section.</summary>
		/// <returns>The protected configuration provider for this <see cref="T:System.Configuration.ConfigurationSection" /> object.</returns>
		// Token: 0x17000081 RID: 129
		// (get) Token: 0x0600013A RID: 314 RVA: 0x00002057 File Offset: 0x00000257
		public ProtectedConfigurationProvider ProtectionProvider
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a value that indicates whether the associated configuration section requires access permissions.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see langword="requirePermission" /> attribute is set to <see langword="true" />; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The selected value conflicts with a value that is already defined.</exception>
		// Token: 0x17000082 RID: 130
		// (get) Token: 0x0600013B RID: 315 RVA: 0x00002884 File Offset: 0x00000A84
		// (set) Token: 0x0600013C RID: 316 RVA: 0x00002050 File Offset: 0x00000250
		public bool RequirePermission
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that specifies whether a change in an external configuration include file requires an application restart.</summary>
		/// <returns>
		///     <see langword="true" /> if a change in an external configuration include file requires an application restart; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The selected value conflicts with a value that is already defined.</exception>
		// Token: 0x17000083 RID: 131
		// (get) Token: 0x0600013D RID: 317 RVA: 0x000028A0 File Offset: 0x00000AA0
		// (set) Token: 0x0600013E RID: 318 RVA: 0x00002050 File Offset: 0x00000250
		public bool RestartOnExternalChanges
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the name of the associated configuration section.</summary>
		/// <returns>The name of the associated <see cref="T:System.Configuration.ConfigurationSection" /> object.</returns>
		// Token: 0x17000084 RID: 132
		// (get) Token: 0x0600013F RID: 319 RVA: 0x00002057 File Offset: 0x00000257
		public string SectionName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the section class name.</summary>
		/// <returns>The name of the class that is associated with this <see cref="T:System.Configuration.ConfigurationSection" /> section.</returns>
		/// <exception cref="T:System.ArgumentException">The selected value is <see langword="null" /> or an empty string ("").</exception>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The selected value conflicts with a value that is already defined.</exception>
		// Token: 0x17000085 RID: 133
		// (get) Token: 0x06000140 RID: 320 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x06000141 RID: 321 RVA: 0x00002050 File Offset: 0x00000250
		public string Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Forces the associated configuration section to appear in the configuration file.</summary>
		// Token: 0x06000142 RID: 322 RVA: 0x00002050 File Offset: 0x00000250
		public void ForceDeclaration()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Forces the associated configuration section to appear in the configuration file, or removes an existing section from the configuration file.</summary>
		/// <param name="force">
		///       <see langword="true" /> if the associated section should be written in the configuration file; otherwise, <see langword="false" />.</param>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">
		///         <paramref name="force" /> is <see langword="true" /> and the associated section cannot be exported to the child configuration file, or it is undeclared.</exception>
		// Token: 0x06000143 RID: 323 RVA: 0x00002050 File Offset: 0x00000250
		public void ForceDeclaration(bool force)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the configuration section that contains the configuration section associated with this object.</summary>
		/// <returns>The configuration section that contains the <see cref="T:System.Configuration.ConfigurationSection" /> that is associated with this <see cref="T:System.Configuration.SectionInformation" /> object.</returns>
		/// <exception cref="T:System.InvalidOperationException">The method is invoked from a parent section.</exception>
		// Token: 0x06000144 RID: 324 RVA: 0x00002057 File Offset: 0x00000257
		public ConfigurationSection GetParentSection()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns an XML node object that represents the associated configuration-section object.</summary>
		/// <returns>The XML representation for this configuration section.</returns>
		/// <exception cref="T:System.InvalidOperationException">This configuration object is locked and cannot be edited.</exception>
		// Token: 0x06000145 RID: 325 RVA: 0x00002057 File Offset: 0x00000257
		public string GetRawXml()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Marks a configuration section for protection. </summary>
		/// <param name="protectionProvider">The name of the protection provider to use.</param>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.Configuration.SectionInformation.AllowLocation" /> property is set to <see langword="false" />.- or -The target section is already a protected data section.</exception>
		// Token: 0x06000146 RID: 326 RVA: 0x00002050 File Offset: 0x00000250
		public void ProtectSection(string protectionProvider)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Causes the associated configuration section to inherit all its values from the parent section.</summary>
		/// <exception cref="T:System.InvalidOperationException">This method cannot be called outside editing mode.</exception>
		// Token: 0x06000147 RID: 327 RVA: 0x00002050 File Offset: 0x00000250
		public void RevertToParent()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the object to an XML representation of the associated configuration section within the configuration file.</summary>
		/// <param name="rawXml">The XML to use.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="rawXml" /> is <see langword="null" />.</exception>
		// Token: 0x06000148 RID: 328 RVA: 0x00002050 File Offset: 0x00000250
		public void SetRawXml(string rawXml)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the protected configuration encryption from the associated configuration section.</summary>
		// Token: 0x06000149 RID: 329 RVA: 0x00002050 File Offset: 0x00000250
		public void UnprotectSection()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
