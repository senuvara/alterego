﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Converts between a string and an enumeration type. </summary>
	// Token: 0x0200003F RID: 63
	public sealed class GenericEnumConverter : ConfigurationConverterBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.GenericEnumConverter" /> class.</summary>
		/// <param name="typeEnum">The enumeration type to convert.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="typeEnum" /> is <see langword="null" />.</exception>
		// Token: 0x060001F9 RID: 505 RVA: 0x00002050 File Offset: 0x00000250
		public GenericEnumConverter(Type typeEnum)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
