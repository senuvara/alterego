﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Acts as a base class for deriving a validation class so that a value of an object can be verified.</summary>
	// Token: 0x0200000E RID: 14
	public abstract class ConfigurationValidatorBase
	{
		/// <summary>Initializes an instance of the <see cref="T:System.Configuration.ConfigurationValidatorBase" /> class.</summary>
		// Token: 0x060000B9 RID: 185 RVA: 0x00002050 File Offset: 0x00000250
		protected ConfigurationValidatorBase()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Determines whether an object can be validated based on type.</summary>
		/// <param name="type">The object type.</param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="type" /> parameter value matches the expected <see langword="type" />; otherwise, <see langword="false" />. </returns>
		// Token: 0x060000BA RID: 186 RVA: 0x000024AC File Offset: 0x000006AC
		public virtual bool CanValidate(Type type)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Determines whether the value of an object is valid. </summary>
		/// <param name="value">The object value.</param>
		// Token: 0x060000BB RID: 187
		public abstract void Validate(object value);
	}
}
