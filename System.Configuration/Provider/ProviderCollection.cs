﻿using System;
using System.Collections;
using Unity;

namespace System.Configuration.Provider
{
	/// <summary>Represents a collection of provider objects that inherit from <see cref="T:System.Configuration.Provider.ProviderBase" />.</summary>
	// Token: 0x0200002E RID: 46
	public class ProviderCollection : ICollection, IEnumerable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.Provider.ProviderCollection" /> class. </summary>
		// Token: 0x0600017F RID: 383 RVA: 0x00002050 File Offset: 0x00000250
		public ProviderCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the number of providers in the collection.</summary>
		/// <returns>The number of providers in the collection.</returns>
		// Token: 0x17000096 RID: 150
		// (get) Token: 0x06000180 RID: 384 RVA: 0x00002910 File Offset: 0x00000B10
		public int Count
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets a value indicating whether access to the collection is synchronized (thread safe).</summary>
		/// <returns>
		///     <see langword="false" /> in all cases.</returns>
		// Token: 0x17000097 RID: 151
		// (get) Token: 0x06000181 RID: 385 RVA: 0x0000292C File Offset: 0x00000B2C
		public bool IsSynchronized
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets the provider with the specified name.</summary>
		/// <param name="name">The key by which the provider is identified.</param>
		/// <returns>The provider with the specified name.</returns>
		// Token: 0x17000098 RID: 152
		public ProviderBase this[string name]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the current object.</summary>
		/// <returns>The current object.</returns>
		// Token: 0x17000099 RID: 153
		// (get) Token: 0x06000183 RID: 387 RVA: 0x00002057 File Offset: 0x00000257
		public object SyncRoot
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Adds a provider to the collection.</summary>
		/// <param name="provider">The provider to be added.</param>
		/// <exception cref="T:System.NotSupportedException">The collection is read-only.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="provider" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Configuration.Provider.ProviderBase.Name" /> of <paramref name="provider" /> is <see langword="null" />.- or -The length of the <see cref="P:System.Configuration.Provider.ProviderBase.Name" /> of <paramref name="provider" /> is less than 1.</exception>
		// Token: 0x06000184 RID: 388 RVA: 0x00002050 File Offset: 0x00000250
		public virtual void Add(ProviderBase provider)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes all items from the collection.</summary>
		/// <exception cref="T:System.NotSupportedException">The collection is set to read-only.</exception>
		// Token: 0x06000185 RID: 389 RVA: 0x00002050 File Offset: 0x00000250
		public void Clear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Copies the contents of the collection to the given array starting at the specified index.</summary>
		/// <param name="array">The array to copy the elements of the collection to.</param>
		/// <param name="index">The index of the collection item at which to start the copying process.</param>
		// Token: 0x06000186 RID: 390 RVA: 0x00002050 File Offset: 0x00000250
		public void CopyTo(ProviderBase[] array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns an object that implements the <see cref="T:System.Collections.IEnumerator" /> interface to iterate through the collection.</summary>
		/// <returns>An object that implements <see cref="T:System.Collections.IEnumerator" /> to iterate through the collection.</returns>
		// Token: 0x06000187 RID: 391 RVA: 0x00002057 File Offset: 0x00000257
		public IEnumerator GetEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Removes a provider from the collection.</summary>
		/// <param name="name">The name of the provider to be removed.</param>
		/// <exception cref="T:System.NotSupportedException">The collection has been set to read-only.</exception>
		// Token: 0x06000188 RID: 392 RVA: 0x00002050 File Offset: 0x00000250
		public void Remove(string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the collection to be read-only.</summary>
		// Token: 0x06000189 RID: 393 RVA: 0x00002050 File Offset: 0x00000250
		public void SetReadOnly()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Copies the elements of the <see cref="T:System.Configuration.Provider.ProviderCollection" /> to an array, starting at a particular array index.</summary>
		/// <param name="array">The array to copy the elements of the collection to.</param>
		/// <param name="index">The index of the array at which to start copying provider instances from the collection.</param>
		// Token: 0x0600018A RID: 394 RVA: 0x00002050 File Offset: 0x00000250
		void ICollection.CopyTo(Array array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
