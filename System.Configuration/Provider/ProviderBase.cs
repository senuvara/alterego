﻿using System;
using System.Collections.Specialized;
using Unity;

namespace System.Configuration.Provider
{
	/// <summary>Provides a base implementation for the extensible provider model.</summary>
	// Token: 0x02000021 RID: 33
	public abstract class ProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.Provider.ProviderBase" /> class. </summary>
		// Token: 0x0600014D RID: 333 RVA: 0x00002050 File Offset: 0x00000250
		protected ProviderBase()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a brief, friendly description suitable for display in administrative tools or other user interfaces (UIs).</summary>
		/// <returns>A brief, friendly description suitable for display in administrative tools or other UIs.</returns>
		// Token: 0x17000086 RID: 134
		// (get) Token: 0x0600014E RID: 334 RVA: 0x00002057 File Offset: 0x00000257
		public virtual string Description
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the friendly name used to refer to the provider during configuration.</summary>
		/// <returns>The friendly name used to refer to the provider during configuration.</returns>
		// Token: 0x17000087 RID: 135
		// (get) Token: 0x0600014F RID: 335 RVA: 0x00002057 File Offset: 0x00000257
		public virtual string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Initializes the configuration builder.</summary>
		/// <param name="name">The friendly name of the provider.</param>
		/// <param name="config">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
		/// <exception cref="T:System.ArgumentNullException">The name of the provider is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">The name of the provider has a length of zero.</exception>
		/// <exception cref="T:System.InvalidOperationException">An attempt is made to call <see cref="M:System.Configuration.Provider.ProviderBase.Initialize(System.String,System.Collections.Specialized.NameValueCollection)" /> on a provider after the provider has already been initialized.</exception>
		// Token: 0x06000150 RID: 336 RVA: 0x00002050 File Offset: 0x00000250
		public virtual void Initialize(string name, NameValueCollection config)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
