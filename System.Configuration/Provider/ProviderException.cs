﻿using System;
using System.Runtime.Serialization;
using Unity;

namespace System.Configuration.Provider
{
	/// <summary>The exception that is thrown when a configuration provider error has occurred. This exception class is also used by providers to throw exceptions when internal errors occur within the provider that do not map to other pre-existing exception classes.</summary>
	// Token: 0x0200005E RID: 94
	[Serializable]
	public class ProviderException : Exception
	{
		/// <summary>Creates a new instance of the <see cref="T:System.Configuration.Provider.ProviderException" /> class.</summary>
		// Token: 0x0600027B RID: 635 RVA: 0x00002050 File Offset: 0x00000250
		public ProviderException()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Configuration.Provider.ProviderException" /> class.</summary>
		/// <param name="info">The object that holds the information to deserialize.</param>
		/// <param name="context">Contextual information about the source or destination.</param>
		// Token: 0x0600027C RID: 636 RVA: 0x00002050 File Offset: 0x00000250
		protected ProviderException(SerializationInfo info, StreamingContext context)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Configuration.Provider.ProviderException" /> class.</summary>
		/// <param name="message">A message describing why this <see cref="T:System.Configuration.Provider.ProviderException" /> was thrown.</param>
		// Token: 0x0600027D RID: 637 RVA: 0x00002050 File Offset: 0x00000250
		public ProviderException(string message)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Configuration.Provider.ProviderException" /> class.</summary>
		/// <param name="message">A message describing why this <see cref="T:System.Configuration.Provider.ProviderException" /> was thrown.</param>
		/// <param name="innerException">The exception that caused this <see cref="T:System.Configuration.Provider.ProviderException" /> to be thrown.</param>
		// Token: 0x0600027E RID: 638 RVA: 0x00002050 File Offset: 0x00000250
		public ProviderException(string message, Exception innerException)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
