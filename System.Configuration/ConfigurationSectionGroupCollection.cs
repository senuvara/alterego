﻿using System;
using System.Collections.Specialized;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents a collection of <see cref="T:System.Configuration.ConfigurationSectionGroup" /> objects.</summary>
	// Token: 0x02000015 RID: 21
	[Serializable]
	public sealed class ConfigurationSectionGroupCollection : NameObjectCollectionBase
	{
		// Token: 0x060000DB RID: 219 RVA: 0x00002050 File Offset: 0x00000250
		internal ConfigurationSectionGroupCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060000DC RID: 220 RVA: 0x00002057 File Offset: 0x00000257
		public ConfigurationSectionGroup get_Item(int index)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object whose name is specified from the collection.</summary>
		/// <param name="name">The name of the <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object to be returned. </param>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object with the specified name.In C#, this property is the indexer for the <see cref="T:System.Configuration.ConfigurationSectionCollection" /> class. </returns>
		// Token: 0x17000050 RID: 80
		public ConfigurationSectionGroup this[string name]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Adds a <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object to this <see cref="T:System.Configuration.ConfigurationSectionGroupCollection" /> object.</summary>
		/// <param name="name">The name of the <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object to be added.</param>
		/// <param name="sectionGroup">The <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object to be added.</param>
		// Token: 0x060000DE RID: 222 RVA: 0x00002050 File Offset: 0x00000250
		public void Add(string name, ConfigurationSectionGroup sectionGroup)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Clears the collection.</summary>
		// Token: 0x060000DF RID: 223 RVA: 0x00002050 File Offset: 0x00000250
		public void Clear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Copies this <see cref="T:System.Configuration.ConfigurationSectionGroupCollection" /> object to an array.</summary>
		/// <param name="array">The array to copy the object to.</param>
		/// <param name="index">The index location at which to begin copying.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The length of <paramref name="array" /> is less than the value of <see cref="P:System.Configuration.ConfigurationSectionGroupCollection.Count" /> plus <paramref name="index" />.</exception>
		// Token: 0x060000E0 RID: 224 RVA: 0x00002050 File Offset: 0x00000250
		public void CopyTo(ConfigurationSectionGroup[] array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the specified <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object contained in the collection.</summary>
		/// <param name="index">The index of the <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object to be returned. </param>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object at the specified index.</returns>
		// Token: 0x060000E1 RID: 225 RVA: 0x00002057 File Offset: 0x00000257
		public ConfigurationSectionGroup Get(int index)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the specified <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object from the collection.</summary>
		/// <param name="name">The name of the <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object to be returned. </param>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object with the specified name.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="name" /> is null or an empty string ("").</exception>
		// Token: 0x060000E2 RID: 226 RVA: 0x00002057 File Offset: 0x00000257
		public ConfigurationSectionGroup Get(string name)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the key of the specified <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object contained in this <see cref="T:System.Configuration.ConfigurationSectionGroupCollection" /> object.</summary>
		/// <param name="index">The index of the section group whose key is to be returned. </param>
		/// <returns>The key of the <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object at the specified index.</returns>
		// Token: 0x060000E3 RID: 227 RVA: 0x00002057 File Offset: 0x00000257
		public string GetKey(int index)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Removes the <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object whose name is specified from this <see cref="T:System.Configuration.ConfigurationSectionGroupCollection" /> object.</summary>
		/// <param name="name">The name of the section group to be removed. </param>
		// Token: 0x060000E4 RID: 228 RVA: 0x00002050 File Offset: 0x00000250
		public void Remove(string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object whose index is specified from this <see cref="T:System.Configuration.ConfigurationSectionGroupCollection" /> object.</summary>
		/// <param name="index">The index of the section group to be removed. </param>
		// Token: 0x060000E5 RID: 229 RVA: 0x00002050 File Offset: 0x00000250
		public void RemoveAt(int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
