﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides validation of a <see cref="T:System.TimeSpan" /> object. This class cannot be inherited.</summary>
	// Token: 0x02000049 RID: 73
	public class PositiveTimeSpanValidator : ConfigurationValidatorBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.PositiveTimeSpanValidator" /> class. </summary>
		// Token: 0x0600022D RID: 557 RVA: 0x00002050 File Offset: 0x00000250
		public PositiveTimeSpanValidator()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Determines whether the object type can be validated.</summary>
		/// <param name="type">The object type.</param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="type" /> parameter matches a <see cref="T:System.TimeSpan" /> object; otherwise, <see langword="false" />. </returns>
		// Token: 0x0600022E RID: 558 RVA: 0x00002BE8 File Offset: 0x00000DE8
		public override bool CanValidate(Type type)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Determines whether the value of an object is valid.</summary>
		/// <param name="value">The value of an object.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="value" /> cannot be resolved as a positive <see cref="T:System.TimeSpan" /> value.</exception>
		// Token: 0x0600022F RID: 559 RVA: 0x00002050 File Offset: 0x00000250
		public override void Validate(object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
