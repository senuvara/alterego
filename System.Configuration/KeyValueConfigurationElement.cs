﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents a configuration element that contains a key/value pair. </summary>
	// Token: 0x02000025 RID: 37
	public class KeyValueConfigurationElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.KeyValueConfigurationElement" /> class based on the supplied parameters.</summary>
		/// <param name="key">The key of the <see cref="T:System.Configuration.KeyValueConfigurationElement" />.</param>
		/// <param name="value">The value of the <see cref="T:System.Configuration.KeyValueConfigurationElement" />.</param>
		// Token: 0x0600015F RID: 351 RVA: 0x00002050 File Offset: 0x00000250
		public KeyValueConfigurationElement(string key, string value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the key of the <see cref="T:System.Configuration.KeyValueConfigurationElement" /> object.</summary>
		/// <returns>The key of the <see cref="T:System.Configuration.KeyValueConfigurationElement" />.</returns>
		// Token: 0x1700008C RID: 140
		// (get) Token: 0x06000160 RID: 352 RVA: 0x00002057 File Offset: 0x00000257
		public string Key
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the collection of properties. </summary>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationPropertyCollection" /> of properties for the element.</returns>
		// Token: 0x1700008D RID: 141
		// (get) Token: 0x06000161 RID: 353 RVA: 0x00002057 File Offset: 0x00000257
		protected internal override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the value of the <see cref="T:System.Configuration.KeyValueConfigurationElement" /> object.</summary>
		/// <returns>The value of the <see cref="T:System.Configuration.KeyValueConfigurationElement" />.</returns>
		// Token: 0x1700008E RID: 142
		// (get) Token: 0x06000162 RID: 354 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x06000163 RID: 355 RVA: 0x00002050 File Offset: 0x00000250
		public string Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Sets the <see cref="T:System.Configuration.KeyValueConfigurationElement" /> object to its initial state.</summary>
		// Token: 0x06000164 RID: 356 RVA: 0x00002050 File Offset: 0x00000250
		protected internal override void Init()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
