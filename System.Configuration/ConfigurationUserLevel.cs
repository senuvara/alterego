﻿using System;

namespace System.Configuration
{
	/// <summary>Used to specify which configuration file is to be represented by the Configuration object.</summary>
	// Token: 0x02000036 RID: 54
	public enum ConfigurationUserLevel
	{
		/// <summary>Get the <see cref="T:System.Configuration.Configuration" /> that applies to all users.</summary>
		// Token: 0x04000025 RID: 37
		None,
		/// <summary>Get the roaming <see cref="T:System.Configuration.Configuration" /> that applies to the current user.</summary>
		// Token: 0x04000026 RID: 38
		PerUserRoaming = 10,
		/// <summary>Get the local <see cref="T:System.Configuration.Configuration" /> that applies to the current user.</summary>
		// Token: 0x04000027 RID: 39
		PerUserRoamingAndLocal = 20
	}
}
