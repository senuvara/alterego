﻿using System;

namespace System.Configuration
{
	/// <summary>Specifies the options to apply to a property.</summary>
	// Token: 0x0200000F RID: 15
	[Flags]
	public enum ConfigurationPropertyOptions
	{
		/// <summary>Indicates whether the assembly name for the configuration property requires transformation when it is serialized for an earlier version of the .NET Framework.</summary>
		// Token: 0x0400000B RID: 11
		IsAssemblyStringTransformationRequired = 16,
		/// <summary>Indicates that the property is a default collection. </summary>
		// Token: 0x0400000C RID: 12
		IsDefaultCollection = 1,
		/// <summary>Indicates that the property is a collection key.</summary>
		// Token: 0x0400000D RID: 13
		IsKey = 4,
		/// <summary>Indicates that the property is required. </summary>
		// Token: 0x0400000E RID: 14
		IsRequired = 2,
		/// <summary>Indicates whether the type name for the configuration property requires transformation when it is serialized for an earlier version of the .NET Framework.</summary>
		// Token: 0x0400000F RID: 15
		IsTypeStringTransformationRequired = 8,
		/// <summary>Indicates whether the configuration property's parent configuration section should be queried at serialization time to determine whether the configuration property should be serialized into XML.</summary>
		// Token: 0x04000010 RID: 16
		IsVersionCheckRequired = 32,
		/// <summary>Indicates that no option applies to the property.</summary>
		// Token: 0x04000011 RID: 17
		None = 0
	}
}
