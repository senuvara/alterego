﻿using System;
using System.Collections.Specialized;
using System.Security.Permissions;
using System.Xml;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides a <see cref="T:System.Configuration.ProtectedConfigurationProvider" /> object that uses the Windows data protection API (DPAPI) to encrypt and decrypt configuration data.</summary>
	// Token: 0x0200003D RID: 61
	[PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
	public sealed class DpapiProtectedConfigurationProvider : ProtectedConfigurationProvider
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.DpapiProtectedConfigurationProvider" /> class using default settings.</summary>
		// Token: 0x060001F1 RID: 497 RVA: 0x00002050 File Offset: 0x00000250
		public DpapiProtectedConfigurationProvider()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a value that indicates whether the <see cref="T:System.Configuration.DpapiProtectedConfigurationProvider" /> object is using machine-specific or user-account-specific protection.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Configuration.DpapiProtectedConfigurationProvider" /> is using machine-specific protection; <see langword="false" /> if it is using user-account-specific protection.</returns>
		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x060001F2 RID: 498 RVA: 0x00002AB4 File Offset: 0x00000CB4
		public bool UseMachineProtection
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Decrypts the passed <see cref="T:System.Xml.XmlNode" /> object.</summary>
		/// <param name="encryptedNode">The <see cref="T:System.Xml.XmlNode" /> object to decrypt. </param>
		/// <returns>A decrypted <see cref="T:System.Xml.XmlNode" /> object.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">
		///         <paramref name="encryptedNode" /> does not have <see cref="P:System.Xml.XmlNode.Name" /> set to "EncryptedData" and <see cref="T:System.Xml.XmlNodeType" /> set to <see cref="F:System.Xml.XmlNodeType.Element" />.- or -
		///         <paramref name="encryptedNode" /> does not have a child node named "CipherData" with a child node named "CipherValue".- or -The child node named "CipherData" is an empty node.</exception>
		// Token: 0x060001F3 RID: 499 RVA: 0x00002057 File Offset: 0x00000257
		public override XmlNode Decrypt(XmlNode encryptedNode)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Encrypts the passed <see cref="T:System.Xml.XmlNode" /> object.</summary>
		/// <param name="node">The <see cref="T:System.Xml.XmlNode" /> object to encrypt. </param>
		/// <returns>An encrypted <see cref="T:System.Xml.XmlNode" /> object.</returns>
		// Token: 0x060001F4 RID: 500 RVA: 0x00002057 File Offset: 0x00000257
		public override XmlNode Encrypt(XmlNode node)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Initializes the provider with default settings.</summary>
		/// <param name="name">The provider name to use for the object.</param>
		/// <param name="configurationValues">A <see cref="T:System.Collections.Specialized.NameValueCollection" /> collection of values to use when initializing the object.</param>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">
		///         <paramref name="configurationValues" /> contains an unrecognized configuration setting.</exception>
		// Token: 0x060001F5 RID: 501 RVA: 0x00002050 File Offset: 0x00000250
		public override void Initialize(string name, NameValueCollection configurationValues)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
