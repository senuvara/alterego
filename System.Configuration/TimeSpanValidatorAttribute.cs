﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Declaratively instructs the .NET Framework to perform time validation on a configuration property. This class cannot be inherited.</summary>
	// Token: 0x0200005B RID: 91
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class TimeSpanValidatorAttribute : ConfigurationValidatorAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.TimeSpanValidatorAttribute" /> class.</summary>
		// Token: 0x0600026F RID: 623 RVA: 0x000024C7 File Offset: 0x000006C7
		public TimeSpanValidatorAttribute()
		{
		}

		/// <summary>Gets or sets a value that indicates whether to include or exclude the integers in the range as defined by <see cref="P:System.Configuration.TimeSpanValidatorAttribute.MinValueString" /> and <see cref="P:System.Configuration.TimeSpanValidatorAttribute.MaxValueString" />.</summary>
		/// <returns>
		///     <see langword="true" /> if the value must be excluded; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170000DE RID: 222
		// (get) Token: 0x06000270 RID: 624 RVA: 0x00002D1C File Offset: 0x00000F1C
		// (set) Token: 0x06000271 RID: 625 RVA: 0x00002050 File Offset: 0x00000250
		public bool ExcludeRange
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the absolute maximum <see cref="T:System.TimeSpan" /> value.</summary>
		/// <returns>The allowed maximum <see cref="T:System.TimeSpan" /> value. </returns>
		// Token: 0x170000DF RID: 223
		// (get) Token: 0x06000272 RID: 626 RVA: 0x00002D38 File Offset: 0x00000F38
		public TimeSpan MaxValue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(TimeSpan);
			}
		}

		/// <summary>Gets or sets the relative maximum <see cref="T:System.TimeSpan" /> value.</summary>
		/// <returns>The allowed maximum <see cref="T:System.TimeSpan" /> value. </returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The selected value represents less than <see cref="P:System.Configuration.TimeSpanValidatorAttribute.MinValue" />.</exception>
		// Token: 0x170000E0 RID: 224
		// (get) Token: 0x06000273 RID: 627 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x06000274 RID: 628 RVA: 0x00002050 File Offset: 0x00000250
		public string MaxValueString
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the absolute minimum <see cref="T:System.TimeSpan" /> value.</summary>
		/// <returns>The allowed minimum <see cref="T:System.TimeSpan" /> value. </returns>
		// Token: 0x170000E1 RID: 225
		// (get) Token: 0x06000275 RID: 629 RVA: 0x00002D54 File Offset: 0x00000F54
		public TimeSpan MinValue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(TimeSpan);
			}
		}

		/// <summary>Gets or sets the relative minimum <see cref="T:System.TimeSpan" /> value.</summary>
		/// <returns>The minimum allowed <see cref="T:System.TimeSpan" /> value. </returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The selected value represents more than <see cref="P:System.Configuration.TimeSpanValidatorAttribute.MaxValue" />.</exception>
		// Token: 0x170000E2 RID: 226
		// (get) Token: 0x06000276 RID: 630 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x06000277 RID: 631 RVA: 0x00002050 File Offset: 0x00000250
		public string MinValueString
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets an instance of the <see cref="T:System.Configuration.TimeSpanValidator" /> class.</summary>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationValidatorBase" /> validator instance. </returns>
		// Token: 0x170000E3 RID: 227
		// (get) Token: 0x06000278 RID: 632 RVA: 0x00002057 File Offset: 0x00000257
		public override ConfigurationValidatorBase ValidatorInstance
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the absolute maximum value allowed.</summary>
		// Token: 0x0400002B RID: 43
		public const string TimeSpanMaxValue = "10675199.02:48:05.4775807";

		/// <summary>Gets the absolute minimum value allowed.</summary>
		// Token: 0x0400002C RID: 44
		public const string TimeSpanMinValue = "-10675199.02:48:05.4775808";
	}
}
