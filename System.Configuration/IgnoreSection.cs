﻿using System;
using System.Xml;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides a wrapper type definition for configuration sections that are not handled by the <see cref="N:System.Configuration" /> types.</summary>
	// Token: 0x02000040 RID: 64
	public sealed class IgnoreSection : ConfigurationSection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.IgnoreSection" /> class.</summary>
		// Token: 0x060001FA RID: 506 RVA: 0x00002050 File Offset: 0x00000250
		public IgnoreSection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x170000B8 RID: 184
		// (get) Token: 0x060001FB RID: 507 RVA: 0x00002057 File Offset: 0x00000257
		protected internal override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x060001FC RID: 508 RVA: 0x00002050 File Offset: 0x00000250
		protected internal override void DeserializeSection(XmlReader xmlReader)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060001FD RID: 509 RVA: 0x00002AEC File Offset: 0x00000CEC
		protected internal override bool IsModified()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x060001FE RID: 510 RVA: 0x00002050 File Offset: 0x00000250
		protected internal override void Reset(ConfigurationElement parentSection)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x060001FF RID: 511 RVA: 0x00002050 File Offset: 0x00000250
		protected internal override void ResetModified()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06000200 RID: 512 RVA: 0x00002057 File Offset: 0x00000257
		protected internal override string SerializeSection(ConfigurationElement parentSection, string name, ConfigurationSaveMode saveMode)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
