﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Serves as the base class for the <see cref="N:System.Configuration" /> validator attribute types.</summary>
	// Token: 0x02000029 RID: 41
	[AttributeUsage(AttributeTargets.Property)]
	public class ConfigurationValidatorAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ConfigurationValidatorAttribute" /> class.</summary>
		// Token: 0x06000172 RID: 370 RVA: 0x000024C7 File Offset: 0x000006C7
		protected ConfigurationValidatorAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ConfigurationValidatorAttribute" /> class using the specified validator type.</summary>
		/// <param name="validator">The validator type to use when creating an instance of <see cref="T:System.Configuration.ConfigurationValidatorAttribute" />.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="validator" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="validator" /> is not derived from <see cref="T:System.Configuration.ConfigurationValidatorBase" />.</exception>
		// Token: 0x06000173 RID: 371 RVA: 0x000024C7 File Offset: 0x000006C7
		public ConfigurationValidatorAttribute(Type validator)
		{
		}

		/// <summary>Gets the validator attribute instance.</summary>
		/// <returns>The current <see cref="T:System.Configuration.ConfigurationValidatorBase" />.</returns>
		// Token: 0x17000092 RID: 146
		// (get) Token: 0x06000174 RID: 372 RVA: 0x00002057 File Offset: 0x00000257
		public virtual ConfigurationValidatorBase ValidatorInstance
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the type of the validator attribute.</summary>
		/// <returns>The <see cref="T:System.Type" /> of the current validator attribute instance.</returns>
		// Token: 0x17000093 RID: 147
		// (get) Token: 0x06000175 RID: 373 RVA: 0x00002057 File Offset: 0x00000257
		public Type ValidatorType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
