﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides access to the protected-configuration providers for the current application's configuration file. </summary>
	// Token: 0x0200004B RID: 75
	[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
	public static class ProtectedConfiguration
	{
		/// <summary>Gets the name of the default protected-configuration provider.</summary>
		/// <returns>The name of the default protected-configuration provider.</returns>
		// Token: 0x170000C8 RID: 200
		// (get) Token: 0x06000232 RID: 562 RVA: 0x00002057 File Offset: 0x00000257
		public static string DefaultProvider
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a collection of the installed protected-configuration providers.</summary>
		/// <returns>A <see cref="T:System.Configuration.ProtectedConfigurationProviderCollection" /> collection of installed <see cref="T:System.Configuration.ProtectedConfigurationProvider" /> objects.</returns>
		// Token: 0x170000C9 RID: 201
		// (get) Token: 0x06000233 RID: 563 RVA: 0x00002057 File Offset: 0x00000257
		public static ProtectedConfigurationProviderCollection Providers
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>The name of the data protection provider.</summary>
		// Token: 0x04000028 RID: 40
		public const string DataProtectionProviderName = "DataProtectionConfigurationProvider";

		/// <summary>The name of the protected data section.</summary>
		// Token: 0x04000029 RID: 41
		public const string ProtectedDataSectionName = "configProtectedData";

		/// <summary>The name of the RSA provider.</summary>
		// Token: 0x0400002A RID: 42
		public const string RsaProviderName = "RsaProtectedConfigurationProvider";
	}
}
