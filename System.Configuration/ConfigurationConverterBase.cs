﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Configuration
{
	/// <summary>The base class for the configuration converter types.</summary>
	// Token: 0x0200002C RID: 44
	public abstract class ConfigurationConverterBase : TypeConverter
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ConfigurationConverterBase" /> class.</summary>
		// Token: 0x0600017B RID: 379 RVA: 0x00002050 File Offset: 0x00000250
		protected ConfigurationConverterBase()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
