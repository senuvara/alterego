﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents a collection of <see cref="T:System.Configuration.ProviderSettings" /> objects.</summary>
	// Token: 0x02000030 RID: 48
	[ConfigurationCollection(typeof(ProviderSettings))]
	public sealed class ProviderSettingsCollection : ConfigurationElementCollection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ProviderSettingsCollection" /> class. </summary>
		// Token: 0x0600018E RID: 398 RVA: 0x00002050 File Offset: 0x00000250
		public ProviderSettingsCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0600018F RID: 399 RVA: 0x00002057 File Offset: 0x00000257
		public ProviderSettings get_Item(int index)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets an item from the collection. </summary>
		/// <param name="key">A string reference to the <see cref="T:System.Configuration.ProviderSettings" /> object within the collection.</param>
		/// <returns>A <see cref="T:System.Configuration.ProviderSettings" /> object contained in the collection.</returns>
		// Token: 0x1700009C RID: 156
		public ProviderSettings this[string key]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x1700009D RID: 157
		// (get) Token: 0x06000192 RID: 402 RVA: 0x00002057 File Offset: 0x00000257
		protected internal override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Adds a <see cref="T:System.Configuration.ProviderSettings" /> object to the collection.</summary>
		/// <param name="provider">The <see cref="T:System.Configuration.ProviderSettings" /> object to add.</param>
		// Token: 0x06000193 RID: 403 RVA: 0x00002050 File Offset: 0x00000250
		public void Add(ProviderSettings provider)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Clears the collection.</summary>
		// Token: 0x06000194 RID: 404 RVA: 0x00002050 File Offset: 0x00000250
		public void Clear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06000195 RID: 405 RVA: 0x00002057 File Offset: 0x00000257
		protected override ConfigurationElement CreateNewElement()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06000196 RID: 406 RVA: 0x00002057 File Offset: 0x00000257
		protected override object GetElementKey(ConfigurationElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Removes an element from the collection.</summary>
		/// <param name="name">The name of the <see cref="T:System.Configuration.ProviderSettings" /> object to remove.</param>
		// Token: 0x06000197 RID: 407 RVA: 0x00002050 File Offset: 0x00000250
		public void Remove(string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
