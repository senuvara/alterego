﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Declaratively instructs the .NET Framework to perform string validation on a configuration property using a regular expression. This class cannot be inherited.</summary>
	// Token: 0x02000050 RID: 80
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class RegexStringValidatorAttribute : ConfigurationValidatorAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.RegexStringValidatorAttribute" /> object.</summary>
		/// <param name="regex">The string to use for regular expression validation.</param>
		// Token: 0x06000242 RID: 578 RVA: 0x000024C7 File Offset: 0x000006C7
		public RegexStringValidatorAttribute(string regex)
		{
		}

		/// <summary>Gets the string used to perform regular-expression validation.</summary>
		/// <returns>The string containing the regular expression used to filter the string assigned to the decorated configuration-element property.</returns>
		// Token: 0x170000D0 RID: 208
		// (get) Token: 0x06000243 RID: 579 RVA: 0x00002057 File Offset: 0x00000257
		public string Regex
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets an instance of the <see cref="T:System.Configuration.RegexStringValidator" /> class.</summary>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationValidatorBase" /> validator instance.</returns>
		// Token: 0x170000D1 RID: 209
		// (get) Token: 0x06000244 RID: 580 RVA: 0x00002057 File Offset: 0x00000257
		public override ConfigurationValidatorBase ValidatorInstance
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
