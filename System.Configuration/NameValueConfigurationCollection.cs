﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Contains a collection of <see cref="T:System.Configuration.NameValueConfigurationElement" /> objects. This class cannot be inherited.</summary>
	// Token: 0x02000047 RID: 71
	[ConfigurationCollection(typeof(NameValueConfigurationElement))]
	public sealed class NameValueConfigurationCollection : ConfigurationElementCollection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.NameValueConfigurationCollection" /> class.</summary>
		// Token: 0x0600021D RID: 541 RVA: 0x00002050 File Offset: 0x00000250
		public NameValueConfigurationCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the keys to all items contained in the <see cref="T:System.Configuration.NameValueConfigurationCollection" />.</summary>
		/// <returns>A string array.</returns>
		// Token: 0x170000C1 RID: 193
		// (get) Token: 0x0600021E RID: 542 RVA: 0x00002057 File Offset: 0x00000257
		public string[] AllKeys
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Configuration.NameValueConfigurationElement" /> object based on the supplied parameter.</summary>
		/// <param name="name">The name of the <see cref="T:System.Configuration.NameValueConfigurationElement" /> contained in the collection.</param>
		/// <returns>A <see cref="T:System.Configuration.NameValueConfigurationElement" /> object.</returns>
		// Token: 0x170000C2 RID: 194
		public NameValueConfigurationElement this[string name]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x170000C3 RID: 195
		// (get) Token: 0x06000221 RID: 545 RVA: 0x00002057 File Offset: 0x00000257
		protected internal override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Adds a <see cref="T:System.Configuration.NameValueConfigurationElement" /> object to the collection.</summary>
		/// <param name="nameValue">A  <see cref="T:System.Configuration.NameValueConfigurationElement" /> object.</param>
		// Token: 0x06000222 RID: 546 RVA: 0x00002050 File Offset: 0x00000250
		public void Add(NameValueConfigurationElement nameValue)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Clears the <see cref="T:System.Configuration.NameValueConfigurationCollection" />.</summary>
		// Token: 0x06000223 RID: 547 RVA: 0x00002050 File Offset: 0x00000250
		public void Clear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06000224 RID: 548 RVA: 0x00002057 File Offset: 0x00000257
		protected override ConfigurationElement CreateNewElement()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06000225 RID: 549 RVA: 0x00002057 File Offset: 0x00000257
		protected override object GetElementKey(ConfigurationElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Removes a <see cref="T:System.Configuration.NameValueConfigurationElement" /> object from the collection based on the provided parameter.</summary>
		/// <param name="nameValue">A <see cref="T:System.Configuration.NameValueConfigurationElement" /> object.</param>
		// Token: 0x06000226 RID: 550 RVA: 0x00002050 File Offset: 0x00000250
		public void Remove(NameValueConfigurationElement nameValue)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes a <see cref="T:System.Configuration.NameValueConfigurationElement" /> object from the collection based on the provided parameter.</summary>
		/// <param name="name">The name of the <see cref="T:System.Configuration.NameValueConfigurationElement" /> object.</param>
		// Token: 0x06000227 RID: 551 RVA: 0x00002050 File Offset: 0x00000250
		public void Remove(string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
