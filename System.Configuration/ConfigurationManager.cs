﻿using System;
using System.Collections.Specialized;
using Unity;

namespace System.Configuration
{
	/// <summary>Provides access to configuration files for client applications. This class cannot be inherited.</summary>
	// Token: 0x02000035 RID: 53
	public static class ConfigurationManager
	{
		/// <summary>Gets the <see cref="T:System.Configuration.AppSettingsSection" /> data for the current application's default configuration.</summary>
		/// <returns>Returns a <see cref="T:System.Collections.Specialized.NameValueCollection" /> object that contains the contents of the <see cref="T:System.Configuration.AppSettingsSection" /> object for the current application's default configuration. </returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">Could not retrieve a <see cref="T:System.Collections.Specialized.NameValueCollection" /> object with the application settings data.</exception>
		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x060001BF RID: 447 RVA: 0x00002057 File Offset: 0x00000257
		public static NameValueCollection AppSettings
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the <see cref="T:System.Configuration.ConnectionStringsSection" /> data for the current application's default configuration.</summary>
		/// <returns>Returns a <see cref="T:System.Configuration.ConnectionStringSettingsCollection" /> object that contains the contents of the <see cref="T:System.Configuration.ConnectionStringsSection" /> object for the current application's default configuration. </returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">Could not retrieve a <see cref="T:System.Configuration.ConnectionStringSettingsCollection" /> object.</exception>
		// Token: 0x170000AA RID: 170
		// (get) Token: 0x060001C0 RID: 448 RVA: 0x00002057 File Offset: 0x00000257
		public static ConnectionStringSettingsCollection ConnectionStrings
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Retrieves a specified configuration section for the current application's default configuration.</summary>
		/// <param name="sectionName">The configuration section path and name.</param>
		/// <returns>The specified <see cref="T:System.Configuration.ConfigurationSection" /> object, or <see langword="null" /> if the section does not exist.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">A configuration file could not be loaded.</exception>
		// Token: 0x060001C1 RID: 449 RVA: 0x00002057 File Offset: 0x00000257
		public static object GetSection(string sectionName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Opens the configuration file for the current application as a <see cref="T:System.Configuration.Configuration" /> object.</summary>
		/// <param name="userLevel">The <see cref="T:System.Configuration.ConfigurationUserLevel" /> for which you are opening the configuration.</param>
		/// <returns>A <see cref="T:System.Configuration.Configuration" /> object.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">A configuration file could not be loaded.</exception>
		// Token: 0x060001C2 RID: 450 RVA: 0x00002057 File Offset: 0x00000257
		public static Configuration OpenExeConfiguration(ConfigurationUserLevel userLevel)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Opens the specified client configuration file as a <see cref="T:System.Configuration.Configuration" /> object.</summary>
		/// <param name="exePath">The path of the executable (exe) file.</param>
		/// <returns>A <see cref="T:System.Configuration.Configuration" /> object.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">A configuration file could not be loaded.</exception>
		// Token: 0x060001C3 RID: 451 RVA: 0x00002057 File Offset: 0x00000257
		public static Configuration OpenExeConfiguration(string exePath)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Opens the machine configuration file on the current computer as a <see cref="T:System.Configuration.Configuration" /> object.</summary>
		/// <returns>A <see cref="T:System.Configuration.Configuration" /> object.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">A configuration file could not be loaded.</exception>
		// Token: 0x060001C4 RID: 452 RVA: 0x00002057 File Offset: 0x00000257
		public static Configuration OpenMachineConfiguration()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Opens the specified client configuration file as a <see cref="T:System.Configuration.Configuration" /> object that uses the specified file mapping and user level.</summary>
		/// <param name="fileMap">An <see cref="T:System.Configuration.ExeConfigurationFileMap" /> object that references configuration file to use instead of the application default configuration file.</param>
		/// <param name="userLevel">The <see cref="T:System.Configuration.ConfigurationUserLevel" /> object for which you are opening the configuration.</param>
		/// <returns>The configuration object.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">A configuration file could not be loaded.</exception>
		// Token: 0x060001C5 RID: 453 RVA: 0x00002057 File Offset: 0x00000257
		public static Configuration OpenMappedExeConfiguration(ExeConfigurationFileMap fileMap, ConfigurationUserLevel userLevel)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Opens the specified client configuration file as a <see cref="T:System.Configuration.Configuration" /> object that uses the specified file mapping, user level, and preload option.</summary>
		/// <param name="fileMap">An <see cref="T:System.Configuration.ExeConfigurationFileMap" /> object that references the configuration file to use instead of the default application configuration file.</param>
		/// <param name="userLevel">The <see cref="T:System.Configuration.ConfigurationUserLevel" /> object for which you are opening the configuration.</param>
		/// <param name="preLoad">
		///       <see langword="true" /> to preload all section groups and sections; otherwise, <see langword="false" />.</param>
		/// <returns>The configuration object.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">A configuration file could not be loaded.</exception>
		// Token: 0x060001C6 RID: 454 RVA: 0x00002057 File Offset: 0x00000257
		public static Configuration OpenMappedExeConfiguration(ExeConfigurationFileMap fileMap, ConfigurationUserLevel userLevel, bool preLoad)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Opens the machine configuration file as a <see cref="T:System.Configuration.Configuration" /> object that uses the specified file mapping.</summary>
		/// <param name="fileMap">An <see cref="T:System.Configuration.ExeConfigurationFileMap" /> object that references configuration file to use instead of the application default configuration file.</param>
		/// <returns>A <see cref="T:System.Configuration.Configuration" /> object.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">A configuration file could not be loaded.</exception>
		// Token: 0x060001C7 RID: 455 RVA: 0x00002057 File Offset: 0x00000257
		public static Configuration OpenMappedMachineConfiguration(ConfigurationFileMap fileMap)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Refreshes the named section so the next time that it is retrieved it will be re-read from disk.</summary>
		/// <param name="sectionName">The configuration section name or the configuration path and section name of the section to refresh.</param>
		// Token: 0x060001C8 RID: 456 RVA: 0x00002050 File Offset: 0x00000250
		public static void RefreshSection(string sectionName)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
