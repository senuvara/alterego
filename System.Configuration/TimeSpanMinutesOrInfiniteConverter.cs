﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Converts a <see cref="T:System.TimeSpan" /> expressed in minutes or as a standard infinite time span.</summary>
	// Token: 0x02000057 RID: 87
	public sealed class TimeSpanMinutesOrInfiniteConverter : TimeSpanMinutesConverter
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.TimeSpanMinutesOrInfiniteConverter" /> class.</summary>
		// Token: 0x06000267 RID: 615 RVA: 0x00002050 File Offset: 0x00000250
		public TimeSpanMinutesOrInfiniteConverter()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
