﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Converts a string to its canonical format. </summary>
	// Token: 0x0200005D RID: 93
	public sealed class WhiteSpaceTrimStringConverter : ConfigurationConverterBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.WhiteSpaceTrimStringConverter" /> class.</summary>
		// Token: 0x0600027A RID: 634 RVA: 0x00002050 File Offset: 0x00000250
		public WhiteSpaceTrimStringConverter()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
