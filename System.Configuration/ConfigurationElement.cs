﻿using System;
using System.Collections;
using System.Xml;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents a configuration element within a configuration file.</summary>
	// Token: 0x02000004 RID: 4
	public abstract class ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ConfigurationElement" /> class. </summary>
		// Token: 0x06000015 RID: 21 RVA: 0x00002050 File Offset: 0x00000250
		protected ConfigurationElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a reference to the top-level <see cref="T:System.Configuration.Configuration" /> instance that represents the configuration hierarchy that the current <see cref="T:System.Configuration.ConfigurationElement" /> instance belongs to.</summary>
		/// <returns>The top-level <see cref="T:System.Configuration.Configuration" /> instance that the current <see cref="T:System.Configuration.ConfigurationElement" /> instance belongs to.</returns>
		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000016 RID: 22 RVA: 0x00002057 File Offset: 0x00000257
		public Configuration CurrentConfiguration
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets an <see cref="T:System.Configuration.ElementInformation" /> object that contains the non-customizable information and functionality of the <see cref="T:System.Configuration.ConfigurationElement" /> object. </summary>
		/// <returns>An <see cref="T:System.Configuration.ElementInformation" /> that contains the non-customizable information and functionality of the <see cref="T:System.Configuration.ConfigurationElement" />.</returns>
		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000017 RID: 23 RVA: 0x00002057 File Offset: 0x00000257
		public ElementInformation ElementInformation
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the <see cref="T:System.Configuration.ConfigurationElementProperty" /> object that represents the <see cref="T:System.Configuration.ConfigurationElement" /> object itself.</summary>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationElementProperty" /> that represents the <see cref="T:System.Configuration.ConfigurationElement" /> itself.</returns>
		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000018 RID: 24 RVA: 0x00002057 File Offset: 0x00000257
		protected internal virtual ConfigurationElementProperty ElementProperty
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the <see cref="T:System.Configuration.ContextInformation" /> object for the <see cref="T:System.Configuration.ConfigurationElement" /> object.</summary>
		/// <returns>The <see cref="T:System.Configuration.ContextInformation" /> for the <see cref="T:System.Configuration.ConfigurationElement" />.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The current element is not associated with a context.</exception>
		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000019 RID: 25 RVA: 0x00002057 File Offset: 0x00000257
		protected ContextInformation EvaluationContext
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a value that indicates whether the <see cref="P:System.Configuration.ConfigurationElement.CurrentConfiguration" /> property is <see langword="null" />.</summary>
		/// <returns>false if the <see cref="P:System.Configuration.ConfigurationElement.CurrentConfiguration" /> property is <see langword="null" />; otherwise, <see langword="true" />.</returns>
		// Token: 0x17000009 RID: 9
		// (get) Token: 0x0600001A RID: 26 RVA: 0x000020EC File Offset: 0x000002EC
		protected bool HasContext
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		// Token: 0x0600001B RID: 27 RVA: 0x00002057 File Offset: 0x00000257
		protected internal object get_Item(ConfigurationProperty prop)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x0600001C RID: 28 RVA: 0x00002050 File Offset: 0x00000250
		protected internal void set_Item(ConfigurationProperty prop, object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets a property, attribute, or child element of this configuration element.</summary>
		/// <param name="propertyName">The name of the <see cref="T:System.Configuration.ConfigurationProperty" /> to access.</param>
		/// <returns>The specified property, attribute, or child element</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">
		///         <paramref name="prop" /> is read-only or locked.</exception>
		// Token: 0x1700000A RID: 10
		protected internal object this[string propertyName]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the collection of locked attributes.</summary>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationLockCollection" /> of locked attributes (properties) for the element.</returns>
		// Token: 0x1700000B RID: 11
		// (get) Token: 0x0600001F RID: 31 RVA: 0x00002057 File Offset: 0x00000257
		public ConfigurationLockCollection LockAllAttributesExcept
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the collection of locked elements.</summary>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationLockCollection" /> of locked elements.</returns>
		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000020 RID: 32 RVA: 0x00002057 File Offset: 0x00000257
		public ConfigurationLockCollection LockAllElementsExcept
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the collection of locked attributes </summary>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationLockCollection" /> of locked attributes (properties) for the element.</returns>
		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000021 RID: 33 RVA: 0x00002057 File Offset: 0x00000257
		public ConfigurationLockCollection LockAttributes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the collection of locked elements.</summary>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationLockCollection" /> of locked elements.</returns>
		// Token: 0x1700000E RID: 14
		// (get) Token: 0x06000022 RID: 34 RVA: 0x00002057 File Offset: 0x00000257
		public ConfigurationLockCollection LockElements
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets a value indicating whether the element is locked.</summary>
		/// <returns>
		///     <see langword="true" /> if the element is locked; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The element has already been locked at a higher configuration level.</exception>
		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000023 RID: 35 RVA: 0x00002108 File Offset: 0x00000308
		// (set) Token: 0x06000024 RID: 36 RVA: 0x00002050 File Offset: 0x00000250
		public bool LockItem
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the collection of properties.</summary>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationPropertyCollection" /> of properties for the element.</returns>
		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000025 RID: 37 RVA: 0x00002057 File Offset: 0x00000257
		protected internal virtual ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Reads XML from the configuration file.</summary>
		/// <param name="reader">The <see cref="T:System.Xml.XmlReader" /> that reads from the configuration file.</param>
		/// <param name="serializeCollectionKey">
		///       <see langword="true" /> to serialize only the collection key properties; otherwise, <see langword="false" />.</param>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The element to read is locked.- or -An attribute of the current node is not recognized.- or -The lock status of the current node cannot be determined.  </exception>
		// Token: 0x06000026 RID: 38 RVA: 0x00002050 File Offset: 0x00000250
		protected internal virtual void DeserializeElement(XmlReader reader, bool serializeCollectionKey)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns the transformed version of the specified assembly name. </summary>
		/// <param name="assemblyName">The name of the assembly.</param>
		/// <returns>The transformed version of the assembly name. If no transformer is available, the <paramref name="assemblyName" /> parameter value is returned unchanged. The <see cref="P:System.Configuration.Configuration.TypeStringTransformer" /> property is <see langword="null" /> if no transformer is available.</returns>
		// Token: 0x06000027 RID: 39 RVA: 0x00002057 File Offset: 0x00000257
		protected virtual string GetTransformedAssemblyString(string assemblyName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the transformed version of the specified type name.</summary>
		/// <param name="typeName">The name of the type.</param>
		/// <returns>The transformed version of the specified type name. If no transformer is available, the <paramref name="typeName" /> parameter value is returned unchanged. The <see cref="P:System.Configuration.Configuration.TypeStringTransformer" /> property is <see langword="null" /> if no transformer is available.</returns>
		// Token: 0x06000028 RID: 40 RVA: 0x00002057 File Offset: 0x00000257
		protected virtual string GetTransformedTypeString(string typeName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Sets the <see cref="T:System.Configuration.ConfigurationElement" /> object to its initial state.</summary>
		// Token: 0x06000029 RID: 41 RVA: 0x00002050 File Offset: 0x00000250
		protected internal virtual void Init()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Used to initialize a default set of values for the <see cref="T:System.Configuration.ConfigurationElement" /> object.</summary>
		// Token: 0x0600002A RID: 42 RVA: 0x00002050 File Offset: 0x00000250
		protected internal virtual void InitializeDefault()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Indicates whether this configuration element has been modified since it was last saved or loaded, when implemented in a derived class.</summary>
		/// <returns>
		///     <see langword="true" /> if the element has been modified; otherwise, <see langword="false" />. </returns>
		// Token: 0x0600002B RID: 43 RVA: 0x00002124 File Offset: 0x00000324
		protected internal virtual bool IsModified()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Gets a value indicating whether the <see cref="T:System.Configuration.ConfigurationElement" /> object is read-only.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Configuration.ConfigurationElement" /> object is read-only; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600002C RID: 44 RVA: 0x00002140 File Offset: 0x00000340
		public virtual bool IsReadOnly()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Adds the invalid-property errors in this <see cref="T:System.Configuration.ConfigurationElement" /> object, and in all subelements, to the passed list.</summary>
		/// <param name="errorList">An object that implements the <see cref="T:System.Collections.IList" /> interface.</param>
		// Token: 0x0600002D RID: 45 RVA: 0x00002050 File Offset: 0x00000250
		protected virtual void ListErrors(IList errorList)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a value indicating whether an unknown attribute is encountered during deserialization.</summary>
		/// <param name="name">The name of the unrecognized attribute.</param>
		/// <param name="value">The value of the unrecognized attribute.</param>
		/// <returns>
		///     <see langword="true" /> when an unknown attribute is encountered while deserializing; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600002E RID: 46 RVA: 0x0000215C File Offset: 0x0000035C
		protected virtual bool OnDeserializeUnrecognizedAttribute(string name, string value)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Gets a value indicating whether an unknown element is encountered during deserialization.</summary>
		/// <param name="elementName">The name of the unknown subelement.</param>
		/// <param name="reader">The <see cref="T:System.Xml.XmlReader" /> being used for deserialization.</param>
		/// <returns>
		///     <see langword="true" /> when an unknown element is encountered while deserializing; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The element identified by <paramref name="elementName" /> is locked.- or -One or more of the element's attributes is locked.- or -
		///         <paramref name="elementName" /> is unrecognized, or the element has an unrecognized attribute.- or -The element has a Boolean attribute with an invalid value.- or -An attempt was made to deserialize a property more than once.- or -An attempt was made to deserialize a property that is not a valid member of the element.- or -The element cannot contain a CDATA or text element.</exception>
		// Token: 0x0600002F RID: 47 RVA: 0x00002178 File Offset: 0x00000378
		protected virtual bool OnDeserializeUnrecognizedElement(string elementName, XmlReader reader)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Throws an exception when a required property is not found.</summary>
		/// <param name="name">The name of the required attribute that was not found.</param>
		/// <returns>None.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">In all cases.</exception>
		// Token: 0x06000030 RID: 48 RVA: 0x00002057 File Offset: 0x00000257
		protected virtual object OnRequiredPropertyNotFound(string name)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Called after deserialization.</summary>
		// Token: 0x06000031 RID: 49 RVA: 0x00002050 File Offset: 0x00000250
		protected virtual void PostDeserialize()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Called before serialization.</summary>
		/// <param name="writer">The <see cref="T:System.Xml.XmlWriter" /> that will be used to serialize the <see cref="T:System.Configuration.ConfigurationElement" />.</param>
		// Token: 0x06000032 RID: 50 RVA: 0x00002050 File Offset: 0x00000250
		protected virtual void PreSerialize(XmlWriter writer)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Resets the internal state of the <see cref="T:System.Configuration.ConfigurationElement" /> object, including the locks and the properties collections.</summary>
		/// <param name="parentElement">The parent node of the configuration element.</param>
		// Token: 0x06000033 RID: 51 RVA: 0x00002050 File Offset: 0x00000250
		protected internal virtual void Reset(ConfigurationElement parentElement)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Resets the value of the <see cref="M:System.Configuration.ConfigurationElement.IsModified" /> method to <see langword="false" /> when implemented in a derived class.</summary>
		// Token: 0x06000034 RID: 52 RVA: 0x00002050 File Offset: 0x00000250
		protected internal virtual void ResetModified()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Writes the contents of this configuration element to the configuration file when implemented in a derived class.</summary>
		/// <param name="writer">The <see cref="T:System.Xml.XmlWriter" /> that writes to the configuration file. </param>
		/// <param name="serializeCollectionKey">
		///       <see langword="true" /> to serialize only the collection key properties; otherwise, <see langword="false" />. </param>
		/// <returns>
		///     <see langword="true" /> if any data was actually serialized; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The current attribute is locked at a higher configuration level.</exception>
		// Token: 0x06000035 RID: 53 RVA: 0x00002194 File Offset: 0x00000394
		protected internal virtual bool SerializeElement(XmlWriter writer, bool serializeCollectionKey)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Writes the outer tags of this configuration element to the configuration file when implemented in a derived class.</summary>
		/// <param name="writer">The <see cref="T:System.Xml.XmlWriter" /> that writes to the configuration file. </param>
		/// <param name="elementName">The name of the <see cref="T:System.Configuration.ConfigurationElement" /> to be written. </param>
		/// <returns>
		///     <see langword="true" /> if writing was successful; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.Exception">The element has multiple child elements. </exception>
		// Token: 0x06000036 RID: 54 RVA: 0x000021B0 File Offset: 0x000003B0
		protected internal virtual bool SerializeToXmlElement(XmlWriter writer, string elementName)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Sets a property to the specified value.</summary>
		/// <param name="prop">The element property to set. </param>
		/// <param name="value">The value to assign to the property.</param>
		/// <param name="ignoreLocks">
		///       <see langword="true" /> if the locks on the property should be ignored; otherwise, <see langword="false" />.</param>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">Occurs if the element is read-only or <paramref name="ignoreLocks" /> is <see langword="true" /> but the locks cannot be ignored.</exception>
		// Token: 0x06000037 RID: 55 RVA: 0x00002050 File Offset: 0x00000250
		protected void SetPropertyValue(ConfigurationProperty prop, object value, bool ignoreLocks)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the <see cref="M:System.Configuration.ConfigurationElement.IsReadOnly" /> property for the <see cref="T:System.Configuration.ConfigurationElement" /> object and all subelements.</summary>
		// Token: 0x06000038 RID: 56 RVA: 0x00002050 File Offset: 0x00000250
		protected internal virtual void SetReadOnly()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Modifies the <see cref="T:System.Configuration.ConfigurationElement" /> object to remove all values that should not be saved. </summary>
		/// <param name="sourceElement">A <see cref="T:System.Configuration.ConfigurationElement" /> at the current level containing a merged view of the properties.</param>
		/// <param name="parentElement">The parent <see cref="T:System.Configuration.ConfigurationElement" />, or <see langword="null" /> if this is the top level.</param>
		/// <param name="saveMode">A <see cref="T:System.Configuration.ConfigurationSaveMode" /> that determines which property values to include.</param>
		// Token: 0x06000039 RID: 57 RVA: 0x00002050 File Offset: 0x00000250
		protected internal virtual void Unmerge(ConfigurationElement sourceElement, ConfigurationElement parentElement, ConfigurationSaveMode saveMode)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
