﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents a single, named connection string in the connection strings configuration file section.</summary>
	// Token: 0x0200000B RID: 11
	public sealed class ConnectionStringSettings : ConfigurationElement
	{
		/// <summary>Initializes a new instance of a <see cref="T:System.Configuration.ConnectionStringSettings" /> class.</summary>
		// Token: 0x06000092 RID: 146 RVA: 0x00002050 File Offset: 0x00000250
		public ConnectionStringSettings()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of a <see cref="T:System.Configuration.ConnectionStringSettings" /> class.</summary>
		/// <param name="name">The name of the connection string.</param>
		/// <param name="connectionString">The connection string.</param>
		// Token: 0x06000093 RID: 147 RVA: 0x00002050 File Offset: 0x00000250
		public ConnectionStringSettings(string name, string connectionString)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of a <see cref="T:System.Configuration.ConnectionStringSettings" /> object.</summary>
		/// <param name="name">The name of the connection string.</param>
		/// <param name="connectionString">The connection string.</param>
		/// <param name="providerName">The name of the provider to use with the connection string.</param>
		// Token: 0x06000094 RID: 148 RVA: 0x00002050 File Offset: 0x00000250
		public ConnectionStringSettings(string name, string connectionString, string providerName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the connection string.</summary>
		/// <returns>The string value assigned to the <see cref="P:System.Configuration.ConnectionStringSettings.ConnectionString" /> property.</returns>
		// Token: 0x1700002C RID: 44
		// (get) Token: 0x06000095 RID: 149 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x06000096 RID: 150 RVA: 0x00002050 File Offset: 0x00000250
		public string ConnectionString
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Configuration.ConnectionStringSettings" /> name.</summary>
		/// <returns>The string value assigned to the <see cref="P:System.Configuration.ConnectionStringSettings.Name" /> property.</returns>
		// Token: 0x1700002D RID: 45
		// (get) Token: 0x06000097 RID: 151 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x06000098 RID: 152 RVA: 0x00002050 File Offset: 0x00000250
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x06000099 RID: 153 RVA: 0x00002057 File Offset: 0x00000257
		protected internal override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the provider name property.</summary>
		/// <returns>Gets or sets the <see cref="P:System.Configuration.ConnectionStringSettings.ProviderName" /> property.</returns>
		// Token: 0x1700002F RID: 47
		// (get) Token: 0x0600009A RID: 154 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x0600009B RID: 155 RVA: 0x00002050 File Offset: 0x00000250
		public string ProviderName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
