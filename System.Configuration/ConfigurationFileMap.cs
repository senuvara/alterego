﻿using System;
using Unity;

namespace System.Configuration
{
	/// <summary>Defines the configuration file mapping for the machine configuration file. </summary>
	// Token: 0x02000034 RID: 52
	public class ConfigurationFileMap : ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ConfigurationFileMap" /> class. </summary>
		// Token: 0x060001BA RID: 442 RVA: 0x00002050 File Offset: 0x00000250
		public ConfigurationFileMap()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.ConfigurationFileMap" /> class based on the supplied parameter.</summary>
		/// <param name="machineConfigFilename">The name of the machine configuration file.</param>
		// Token: 0x060001BB RID: 443 RVA: 0x00002050 File Offset: 0x00000250
		public ConfigurationFileMap(string machineConfigFilename)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the name of the machine configuration file name.</summary>
		/// <returns>The machine configuration file name.</returns>
		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x060001BC RID: 444 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x060001BD RID: 445 RVA: 0x00002050 File Offset: 0x00000250
		public string MachineConfigFilename
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Creates a copy of the existing <see cref="T:System.Configuration.ConfigurationFileMap" /> object.</summary>
		/// <returns>A <see cref="T:System.Configuration.ConfigurationFileMap" /> object.</returns>
		// Token: 0x060001BE RID: 446 RVA: 0x00002057 File Offset: 0x00000257
		public virtual object Clone()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
