﻿using System;
using System.Runtime.Versioning;
using System.Security.Permissions;
using Unity;

namespace System.Configuration
{
	/// <summary>Represents a configuration file that is applicable to a particular computer, application, or resource. This class cannot be inherited.</summary>
	// Token: 0x02000005 RID: 5
	public sealed class Configuration
	{
		// Token: 0x0600003A RID: 58 RVA: 0x00002050 File Offset: 0x00000250
		internal Configuration()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the <see cref="T:System.Configuration.AppSettingsSection" /> object configuration section that applies to this <see cref="T:System.Configuration.Configuration" /> object.</summary>
		/// <returns>An <see cref="T:System.Configuration.AppSettingsSection" /> object representing the <see langword="appSettings" /> configuration section that applies to this <see cref="T:System.Configuration.Configuration" /> object.</returns>
		// Token: 0x17000011 RID: 17
		// (get) Token: 0x0600003B RID: 59 RVA: 0x00002057 File Offset: 0x00000257
		public AppSettingsSection AppSettings
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Specifies a function delegate that is used to transform assembly strings in configuration files.</summary>
		/// <returns>A delegate that transforms type strings. The default value is <see langword="null" />.</returns>
		// Token: 0x17000012 RID: 18
		// (get) Token: 0x0600003C RID: 60 RVA: 0x000021CB File Offset: 0x000003CB
		// (set) Token: 0x0600003D RID: 61 RVA: 0x00002050 File Offset: 0x00000250
		public Func<string, string> AssemblyStringTransformer
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			[ConfigurationPermission(SecurityAction.Demand, Unrestricted = true)]
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a <see cref="T:System.Configuration.ConnectionStringsSection" /> configuration-section object that applies to this <see cref="T:System.Configuration.Configuration" /> object.</summary>
		/// <returns>A <see cref="T:System.Configuration.ConnectionStringsSection" /> configuration-section object representing the <see langword="connectionStrings" /> configuration section that applies to this <see cref="T:System.Configuration.Configuration" /> object.</returns>
		// Token: 0x17000013 RID: 19
		// (get) Token: 0x0600003E RID: 62 RVA: 0x00002057 File Offset: 0x00000257
		public ConnectionStringsSection ConnectionStrings
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the <see cref="T:System.Configuration.ContextInformation" /> object for the <see cref="T:System.Configuration.Configuration" /> object.</summary>
		/// <returns>The <see cref="T:System.Configuration.ContextInformation" /> object for the <see cref="T:System.Configuration.Configuration" /> object.</returns>
		// Token: 0x17000014 RID: 20
		// (get) Token: 0x0600003F RID: 63 RVA: 0x00002057 File Offset: 0x00000257
		public ContextInformation EvaluationContext
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the physical path to the configuration file represented by this <see cref="T:System.Configuration.Configuration" /> object.</summary>
		/// <returns>The physical path to the configuration file represented by this <see cref="T:System.Configuration.Configuration" /> object.</returns>
		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000040 RID: 64 RVA: 0x00002057 File Offset: 0x00000257
		public string FilePath
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a value that indicates whether a file exists for the resource represented by this <see cref="T:System.Configuration.Configuration" /> object.</summary>
		/// <returns>
		///     <see langword="true" /> if there is a configuration file; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000041 RID: 65 RVA: 0x000021D4 File Offset: 0x000003D4
		public bool HasFile
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets the locations defined within this <see cref="T:System.Configuration.Configuration" /> object.</summary>
		/// <returns>A <see cref="T:System.Configuration.ConfigurationLocationCollection" /> containing the locations defined within this <see cref="T:System.Configuration.Configuration" /> object.</returns>
		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000042 RID: 66 RVA: 0x00002057 File Offset: 0x00000257
		public ConfigurationLocationCollection Locations
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets a value indicating whether the configuration file has an XML namespace.</summary>
		/// <returns>
		///     <see langword="true" /> if the configuration file has an XML namespace; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000043 RID: 67 RVA: 0x000021F0 File Offset: 0x000003F0
		// (set) Token: 0x06000044 RID: 68 RVA: 0x00002050 File Offset: 0x00000250
		public bool NamespaceDeclared
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the root <see cref="T:System.Configuration.ConfigurationSectionGroup" /> for this <see cref="T:System.Configuration.Configuration" /> object.</summary>
		/// <returns>The root section group for this <see cref="T:System.Configuration.Configuration" /> object.</returns>
		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000045 RID: 69 RVA: 0x00002057 File Offset: 0x00000257
		public ConfigurationSectionGroup RootSectionGroup
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a collection of the section groups defined by this configuration.</summary>
		/// <returns>A <see cref="T:System.Configuration.ConfigurationSectionGroupCollection" /> collection representing the collection of section groups for this <see cref="T:System.Configuration.Configuration" /> object.</returns>
		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000046 RID: 70 RVA: 0x00002057 File Offset: 0x00000257
		public ConfigurationSectionGroupCollection SectionGroups
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a collection of the sections defined by this <see cref="T:System.Configuration.Configuration" /> object.</summary>
		/// <returns>A collection of the sections defined by this <see cref="T:System.Configuration.Configuration" /> object.</returns>
		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000047 RID: 71 RVA: 0x00002057 File Offset: 0x00000257
		public ConfigurationSectionCollection Sections
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Specifies the targeted version of the .NET Framework when a version earlier than the current one is targeted.</summary>
		/// <returns>The name of the targeted version of the .NET Framework. The default value is <see langword="null" />, which indicates that the current version is targeted.</returns>
		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000048 RID: 72 RVA: 0x00002057 File Offset: 0x00000257
		// (set) Token: 0x06000049 RID: 73 RVA: 0x00002050 File Offset: 0x00000250
		public FrameworkName TargetFramework
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			[ConfigurationPermission(SecurityAction.Demand, Unrestricted = true)]
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Specifies a function delegate that is used to transform type strings in configuration files. </summary>
		/// <returns>A delegate that transforms type strings. The default value is <see langword="null" />.</returns>
		// Token: 0x1700001D RID: 29
		// (get) Token: 0x0600004A RID: 74 RVA: 0x000021CB File Offset: 0x000003CB
		// (set) Token: 0x0600004B RID: 75 RVA: 0x00002050 File Offset: 0x00000250
		public Func<string, string> TypeStringTransformer
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			[ConfigurationPermission(SecurityAction.Demand, Unrestricted = true)]
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Returns the specified <see cref="T:System.Configuration.ConfigurationSection" /> object.</summary>
		/// <param name="sectionName">The path to the section to be returned.</param>
		/// <returns>The specified <see cref="T:System.Configuration.ConfigurationSection" /> object.</returns>
		// Token: 0x0600004C RID: 76 RVA: 0x00002057 File Offset: 0x00000257
		public ConfigurationSection GetSection(string sectionName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the specified <see cref="T:System.Configuration.ConfigurationSectionGroup" /> object.</summary>
		/// <param name="sectionGroupName">The path name of the <see cref="T:System.Configuration.ConfigurationSectionGroup" /> to return.</param>
		/// <returns>The <see cref="T:System.Configuration.ConfigurationSectionGroup" /> specified.</returns>
		// Token: 0x0600004D RID: 77 RVA: 0x00002057 File Offset: 0x00000257
		public ConfigurationSectionGroup GetSectionGroup(string sectionGroupName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Writes the configuration settings contained within this <see cref="T:System.Configuration.Configuration" /> object to the current XML configuration file.</summary>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The configuration file could not be written to.- or -The configuration file has changed. </exception>
		// Token: 0x0600004E RID: 78 RVA: 0x00002050 File Offset: 0x00000250
		public void Save()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Writes the configuration settings contained within this <see cref="T:System.Configuration.Configuration" /> object to the current XML configuration file.</summary>
		/// <param name="saveMode">A <see cref="T:System.Configuration.ConfigurationSaveMode" /> value that determines which property values to save.</param>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The configuration file could not be written to.- or -The configuration file has changed. </exception>
		// Token: 0x0600004F RID: 79 RVA: 0x00002050 File Offset: 0x00000250
		public void Save(ConfigurationSaveMode saveMode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Writes the configuration settings contained within this <see cref="T:System.Configuration.Configuration" /> object to the current XML configuration file.</summary>
		/// <param name="saveMode">A <see cref="T:System.Configuration.ConfigurationSaveMode" /> value that determines which property values to save.</param>
		/// <param name="forceSaveAll">
		///       <see langword="true" /> to save even if the configuration was not modified; otherwise, <see langword="false" />.</param>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The configuration file could not be written to.- or -The configuration file has changed. </exception>
		// Token: 0x06000050 RID: 80 RVA: 0x00002050 File Offset: 0x00000250
		public void Save(ConfigurationSaveMode saveMode, bool forceSaveAll)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Writes the configuration settings contained within this <see cref="T:System.Configuration.Configuration" /> object to the specified XML configuration file.</summary>
		/// <param name="filename">The path and file name to save the configuration file to.</param>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The configuration file could not be written to.- or -The configuration file has changed. </exception>
		// Token: 0x06000051 RID: 81 RVA: 0x00002050 File Offset: 0x00000250
		public void SaveAs(string filename)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Writes the configuration settings contained within this <see cref="T:System.Configuration.Configuration" /> object to the specified XML configuration file.</summary>
		/// <param name="filename">The path and file name to save the configuration file to.</param>
		/// <param name="saveMode">A <see cref="T:System.Configuration.ConfigurationSaveMode" /> value that determines which property values to save.</param>
		/// <exception cref="T:System.Configuration.ConfigurationErrorsException">The configuration file could not be written to.- or -The configuration file has changed. </exception>
		// Token: 0x06000052 RID: 82 RVA: 0x00002050 File Offset: 0x00000250
		public void SaveAs(string filename, ConfigurationSaveMode saveMode)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Writes the configuration settings contained within this <see cref="T:System.Configuration.Configuration" /> object to the specified XML configuration file.</summary>
		/// <param name="filename">The path and file name to save the configuration file to.</param>
		/// <param name="saveMode">A <see cref="T:System.Configuration.ConfigurationSaveMode" /> value that determines which property values to save.</param>
		/// <param name="forceSaveAll">
		///       <see langword="true" /> to save even if the configuration was not modified; otherwise, <see langword="false" />.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="filename" /> is null or an empty string ("").</exception>
		// Token: 0x06000053 RID: 83 RVA: 0x00002050 File Offset: 0x00000250
		public void SaveAs(string filename, ConfigurationSaveMode saveMode, bool forceSaveAll)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
