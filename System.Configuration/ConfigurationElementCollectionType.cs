﻿using System;

namespace System.Configuration
{
	/// <summary>Specifies the type of a <see cref="T:System.Configuration.ConfigurationElementCollectionType" /> object.</summary>
	// Token: 0x02000009 RID: 9
	public enum ConfigurationElementCollectionType
	{
		/// <summary>The default type of <see cref="T:System.Configuration.ConfigurationElementCollection" />. Collections of this type contain elements that can be merged across a hierarchy of configuration files. At any particular level within such a hierarchy, <see langword="add" />, <see langword="remove" />, and <see langword="clear" /> directives are used to modify any inherited properties and specify new ones.</summary>
		// Token: 0x04000002 RID: 2
		AddRemoveClearMap = 1,
		/// <summary>Same as <see cref="F:System.Configuration.ConfigurationElementCollectionType.AddRemoveClearMap" />, except that this type causes the <see cref="T:System.Configuration.ConfigurationElementCollection" /> object to sort its contents such that inherited elements are listed last.</summary>
		// Token: 0x04000003 RID: 3
		AddRemoveClearMapAlternate = 3,
		/// <summary>Collections of this type contain elements that apply to the level at which they are specified, and to all child levels. A child level cannot modify the properties specified by a parent element of this type.</summary>
		// Token: 0x04000004 RID: 4
		BasicMap = 0,
		/// <summary>Same as <see cref="F:System.Configuration.ConfigurationElementCollectionType.BasicMap" />, except that this type causes the <see cref="T:System.Configuration.ConfigurationElementCollection" /> object to sort its contents such that inherited elements are listed last.</summary>
		// Token: 0x04000005 RID: 5
		BasicMapAlternate = 2
	}
}
