﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;
using UnityEngine.TextCore.LowLevel;

namespace UnityEngine.TextCore
{
	// Token: 0x02000005 RID: 5
	[UsedByNativeCode]
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	public class Glyph
	{
		// Token: 0x0600004A RID: 74 RVA: 0x000029D8 File Offset: 0x00000BD8
		public Glyph()
		{
			this.m_Index = 0U;
			this.m_Metrics = default(GlyphMetrics);
			this.m_GlyphRect = default(GlyphRect);
			this.m_Scale = 1f;
			this.m_AtlasIndex = 0;
		}

		// Token: 0x0600004B RID: 75 RVA: 0x00002A24 File Offset: 0x00000C24
		public Glyph(Glyph glyph)
		{
			this.m_Index = glyph.index;
			this.m_Metrics = glyph.metrics;
			this.m_GlyphRect = glyph.glyphRect;
			this.m_Scale = glyph.scale;
			this.m_AtlasIndex = glyph.atlasIndex;
		}

		// Token: 0x0600004C RID: 76 RVA: 0x00002A74 File Offset: 0x00000C74
		internal Glyph(GlyphMarshallingStruct glyphStruct)
		{
			this.m_Index = glyphStruct.index;
			this.m_Metrics = glyphStruct.metrics;
			this.m_GlyphRect = glyphStruct.glyphRect;
			this.m_Scale = glyphStruct.scale;
			this.m_AtlasIndex = glyphStruct.atlasIndex;
		}

		// Token: 0x0600004D RID: 77 RVA: 0x00002AC9 File Offset: 0x00000CC9
		public Glyph(uint index, GlyphMetrics metrics, GlyphRect glyphRect)
		{
			this.m_Index = index;
			this.m_Metrics = metrics;
			this.m_GlyphRect = glyphRect;
			this.m_Scale = 1f;
			this.m_AtlasIndex = 0;
		}

		// Token: 0x0600004E RID: 78 RVA: 0x00002AF9 File Offset: 0x00000CF9
		public Glyph(uint index, GlyphMetrics metrics, GlyphRect glyphRect, float scale, int atlasIndex)
		{
			this.m_Index = index;
			this.m_Metrics = metrics;
			this.m_GlyphRect = glyphRect;
			this.m_Scale = scale;
			this.m_AtlasIndex = atlasIndex;
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x0600004F RID: 79 RVA: 0x00002B28 File Offset: 0x00000D28
		// (set) Token: 0x06000050 RID: 80 RVA: 0x00002B43 File Offset: 0x00000D43
		public uint index
		{
			get
			{
				return this.m_Index;
			}
			set
			{
				this.m_Index = value;
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000051 RID: 81 RVA: 0x00002B50 File Offset: 0x00000D50
		// (set) Token: 0x06000052 RID: 82 RVA: 0x00002B6B File Offset: 0x00000D6B
		public GlyphMetrics metrics
		{
			get
			{
				return this.m_Metrics;
			}
			set
			{
				this.m_Metrics = value;
			}
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x06000053 RID: 83 RVA: 0x00002B78 File Offset: 0x00000D78
		// (set) Token: 0x06000054 RID: 84 RVA: 0x00002B93 File Offset: 0x00000D93
		public GlyphRect glyphRect
		{
			get
			{
				return this.m_GlyphRect;
			}
			set
			{
				this.m_GlyphRect = value;
			}
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x06000055 RID: 85 RVA: 0x00002BA0 File Offset: 0x00000DA0
		// (set) Token: 0x06000056 RID: 86 RVA: 0x00002BBB File Offset: 0x00000DBB
		public float scale
		{
			get
			{
				return this.m_Scale;
			}
			set
			{
				this.m_Scale = value;
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000057 RID: 87 RVA: 0x00002BC8 File Offset: 0x00000DC8
		// (set) Token: 0x06000058 RID: 88 RVA: 0x00002BE3 File Offset: 0x00000DE3
		public int atlasIndex
		{
			get
			{
				return this.m_AtlasIndex;
			}
			set
			{
				this.m_AtlasIndex = value;
			}
		}

		// Token: 0x06000059 RID: 89 RVA: 0x00002BF0 File Offset: 0x00000DF0
		public bool Compare(Glyph other)
		{
			return this.index == other.index && this.metrics == other.metrics && this.glyphRect == other.glyphRect && this.scale == other.scale && this.atlasIndex == other.atlasIndex;
		}

		// Token: 0x0400001E RID: 30
		[NativeName("index")]
		[SerializeField]
		private uint m_Index;

		// Token: 0x0400001F RID: 31
		[SerializeField]
		[NativeName("metrics")]
		private GlyphMetrics m_Metrics;

		// Token: 0x04000020 RID: 32
		[NativeName("glyphRect")]
		[SerializeField]
		private GlyphRect m_GlyphRect;

		// Token: 0x04000021 RID: 33
		[SerializeField]
		[NativeName("scale")]
		private float m_Scale;

		// Token: 0x04000022 RID: 34
		[SerializeField]
		[NativeName("atlasIndex")]
		private int m_AtlasIndex;
	}
}
