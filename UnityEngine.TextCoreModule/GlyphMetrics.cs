﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.TextCore
{
	// Token: 0x02000004 RID: 4
	[UsedByNativeCode]
	[Serializable]
	public struct GlyphMetrics : IEquatable<GlyphMetrics>
	{
		// Token: 0x0600003A RID: 58 RVA: 0x000027D7 File Offset: 0x000009D7
		public GlyphMetrics(float width, float height, float bearingX, float bearingY, float advance)
		{
			this.m_Width = width;
			this.m_Height = height;
			this.m_HorizontalBearingX = bearingX;
			this.m_HorizontalBearingY = bearingY;
			this.m_HorizontalAdvance = advance;
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x0600003B RID: 59 RVA: 0x00002800 File Offset: 0x00000A00
		// (set) Token: 0x0600003C RID: 60 RVA: 0x0000281B File Offset: 0x00000A1B
		public float width
		{
			get
			{
				return this.m_Width;
			}
			set
			{
				this.m_Width = value;
			}
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x0600003D RID: 61 RVA: 0x00002828 File Offset: 0x00000A28
		// (set) Token: 0x0600003E RID: 62 RVA: 0x00002843 File Offset: 0x00000A43
		public float height
		{
			get
			{
				return this.m_Height;
			}
			set
			{
				this.m_Height = value;
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x0600003F RID: 63 RVA: 0x00002850 File Offset: 0x00000A50
		// (set) Token: 0x06000040 RID: 64 RVA: 0x0000286B File Offset: 0x00000A6B
		public float horizontalBearingX
		{
			get
			{
				return this.m_HorizontalBearingX;
			}
			set
			{
				this.m_HorizontalBearingX = value;
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000041 RID: 65 RVA: 0x00002878 File Offset: 0x00000A78
		// (set) Token: 0x06000042 RID: 66 RVA: 0x00002893 File Offset: 0x00000A93
		public float horizontalBearingY
		{
			get
			{
				return this.m_HorizontalBearingY;
			}
			set
			{
				this.m_HorizontalBearingY = value;
			}
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x06000043 RID: 67 RVA: 0x000028A0 File Offset: 0x00000AA0
		// (set) Token: 0x06000044 RID: 68 RVA: 0x000028BB File Offset: 0x00000ABB
		public float horizontalAdvance
		{
			get
			{
				return this.m_HorizontalAdvance;
			}
			set
			{
				this.m_HorizontalAdvance = value;
			}
		}

		// Token: 0x06000045 RID: 69 RVA: 0x000028C8 File Offset: 0x00000AC8
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x06000046 RID: 70 RVA: 0x000028F0 File Offset: 0x00000AF0
		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		// Token: 0x06000047 RID: 71 RVA: 0x00002918 File Offset: 0x00000B18
		public bool Equals(GlyphMetrics other)
		{
			return base.Equals(other);
		}

		// Token: 0x06000048 RID: 72 RVA: 0x00002944 File Offset: 0x00000B44
		public static bool operator ==(GlyphMetrics lhs, GlyphMetrics rhs)
		{
			return lhs.width == rhs.width && lhs.height == rhs.height && lhs.horizontalBearingX == rhs.horizontalBearingX && lhs.horizontalBearingY == rhs.horizontalBearingY && lhs.horizontalAdvance == rhs.horizontalAdvance;
		}

		// Token: 0x06000049 RID: 73 RVA: 0x000029B8 File Offset: 0x00000BB8
		public static bool operator !=(GlyphMetrics lhs, GlyphMetrics rhs)
		{
			return !(lhs == rhs);
		}

		// Token: 0x04000019 RID: 25
		[NativeName("width")]
		[SerializeField]
		private float m_Width;

		// Token: 0x0400001A RID: 26
		[SerializeField]
		[NativeName("height")]
		private float m_Height;

		// Token: 0x0400001B RID: 27
		[SerializeField]
		[NativeName("horizontalBearingX")]
		private float m_HorizontalBearingX;

		// Token: 0x0400001C RID: 28
		[NativeName("horizontalBearingy")]
		[SerializeField]
		private float m_HorizontalBearingY;

		// Token: 0x0400001D RID: 29
		[SerializeField]
		[NativeName("horizontalAdvance")]
		private float m_HorizontalAdvance;
	}
}
