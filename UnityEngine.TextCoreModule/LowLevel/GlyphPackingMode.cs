﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.TextCore.LowLevel
{
	// Token: 0x0200000D RID: 13
	[UsedByNativeCode]
	public enum GlyphPackingMode
	{
		// Token: 0x0400005F RID: 95
		BestShortSideFit,
		// Token: 0x04000060 RID: 96
		BestLongSideFit,
		// Token: 0x04000061 RID: 97
		BestAreaFit,
		// Token: 0x04000062 RID: 98
		BottomLeftRule,
		// Token: 0x04000063 RID: 99
		ContactPointRule
	}
}
