﻿using System;

namespace UnityEngine.TextCore.LowLevel
{
	// Token: 0x0200000F RID: 15
	internal struct FontEngineUtilities
	{
		// Token: 0x060000A7 RID: 167 RVA: 0x00003BA4 File Offset: 0x00001DA4
		internal static bool Approximately(float a, float b)
		{
			return Mathf.Abs(a - b) < 0.001f;
		}
	}
}
