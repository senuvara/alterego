﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.TextCore.LowLevel
{
	// Token: 0x02000007 RID: 7
	[UsedByNativeCode]
	[Serializable]
	internal struct GlyphAdjustmentRecord
	{
		// Token: 0x06000064 RID: 100 RVA: 0x00002D92 File Offset: 0x00000F92
		public GlyphAdjustmentRecord(uint glyphIndex, GlyphValueRecord glyphValueRecord)
		{
			this.m_GlyphIndex = glyphIndex;
			this.m_GlyphValueRecord = glyphValueRecord;
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000065 RID: 101 RVA: 0x00002DA4 File Offset: 0x00000FA4
		// (set) Token: 0x06000066 RID: 102 RVA: 0x00002DBF File Offset: 0x00000FBF
		public uint glyphIndex
		{
			get
			{
				return this.m_GlyphIndex;
			}
			set
			{
				this.m_GlyphIndex = value;
			}
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x06000067 RID: 103 RVA: 0x00002DCC File Offset: 0x00000FCC
		// (set) Token: 0x06000068 RID: 104 RVA: 0x00002DE7 File Offset: 0x00000FE7
		public GlyphValueRecord glyphValueRecord
		{
			get
			{
				return this.m_GlyphValueRecord;
			}
			set
			{
				this.m_GlyphValueRecord = value;
			}
		}

		// Token: 0x04000027 RID: 39
		[NativeName("glyphIndex")]
		[SerializeField]
		private uint m_GlyphIndex;

		// Token: 0x04000028 RID: 40
		[SerializeField]
		[NativeName("glyphValueRecord")]
		private GlyphValueRecord m_GlyphValueRecord;
	}
}
