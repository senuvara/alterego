﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.TextCore.LowLevel
{
	// Token: 0x02000008 RID: 8
	[UsedByNativeCode]
	[Serializable]
	internal struct GlyphPairAdjustmentRecord
	{
		// Token: 0x17000029 RID: 41
		// (get) Token: 0x06000069 RID: 105 RVA: 0x00002DF4 File Offset: 0x00000FF4
		// (set) Token: 0x0600006A RID: 106 RVA: 0x00002E0F File Offset: 0x0000100F
		public GlyphAdjustmentRecord firstAdjustmentRecord
		{
			get
			{
				return this.m_FirstAdjustmentRecord;
			}
			set
			{
				this.m_FirstAdjustmentRecord = value;
			}
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x0600006B RID: 107 RVA: 0x00002E1C File Offset: 0x0000101C
		// (set) Token: 0x0600006C RID: 108 RVA: 0x00002E37 File Offset: 0x00001037
		public GlyphAdjustmentRecord secondAdjustmentRecord
		{
			get
			{
				return this.m_SecondAdjustmentRecord;
			}
			set
			{
				this.m_SecondAdjustmentRecord = value;
			}
		}

		// Token: 0x04000029 RID: 41
		[SerializeField]
		[NativeName("firstAdjustmentRecord")]
		private GlyphAdjustmentRecord m_FirstAdjustmentRecord;

		// Token: 0x0400002A RID: 42
		[SerializeField]
		[NativeName("secondAdjustmentRecord")]
		private GlyphAdjustmentRecord m_SecondAdjustmentRecord;
	}
}
