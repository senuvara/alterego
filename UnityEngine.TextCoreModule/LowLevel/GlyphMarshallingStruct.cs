﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.TextCore.LowLevel
{
	// Token: 0x02000010 RID: 16
	[UsedByNativeCode]
	internal struct GlyphMarshallingStruct
	{
		// Token: 0x060000A8 RID: 168 RVA: 0x00003BC8 File Offset: 0x00001DC8
		public GlyphMarshallingStruct(Glyph glyph)
		{
			this.index = glyph.index;
			this.metrics = glyph.metrics;
			this.glyphRect = glyph.glyphRect;
			this.scale = glyph.scale;
			this.atlasIndex = glyph.atlasIndex;
		}

		// Token: 0x060000A9 RID: 169 RVA: 0x00003C07 File Offset: 0x00001E07
		public GlyphMarshallingStruct(uint index, GlyphMetrics metrics, GlyphRect glyphRect, float scale, int atlasIndex)
		{
			this.index = index;
			this.metrics = metrics;
			this.glyphRect = glyphRect;
			this.scale = scale;
			this.atlasIndex = atlasIndex;
		}

		// Token: 0x0400006C RID: 108
		public uint index;

		// Token: 0x0400006D RID: 109
		public GlyphMetrics metrics;

		// Token: 0x0400006E RID: 110
		public GlyphRect glyphRect;

		// Token: 0x0400006F RID: 111
		public float scale;

		// Token: 0x04000070 RID: 112
		public int atlasIndex;
	}
}
