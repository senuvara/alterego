﻿using System;

namespace UnityEngine.TextCore.LowLevel
{
	// Token: 0x0200000B RID: 11
	public enum FontEngineError
	{
		// Token: 0x04000045 RID: 69
		Success,
		// Token: 0x04000046 RID: 70
		Invalid_File_Path,
		// Token: 0x04000047 RID: 71
		Invalid_File_Format,
		// Token: 0x04000048 RID: 72
		Invalid_File_Structure,
		// Token: 0x04000049 RID: 73
		Invalid_File,
		// Token: 0x0400004A RID: 74
		Invalid_Table = 8,
		// Token: 0x0400004B RID: 75
		Invalid_Glyph_Index = 16,
		// Token: 0x0400004C RID: 76
		Invalid_Character_Code,
		// Token: 0x0400004D RID: 77
		Invalid_Pixel_Size = 23,
		// Token: 0x0400004E RID: 78
		Invalid_Library = 33,
		// Token: 0x0400004F RID: 79
		Invalid_Face = 35,
		// Token: 0x04000050 RID: 80
		Invalid_Library_or_Face = 41,
		// Token: 0x04000051 RID: 81
		Atlas_Generation_Cancelled = 100,
		// Token: 0x04000052 RID: 82
		Invalid_SharedTextureData
	}
}
