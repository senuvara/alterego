﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.TextCore.LowLevel
{
	// Token: 0x0200000C RID: 12
	[UsedByNativeCode]
	public enum GlyphRenderMode
	{
		// Token: 0x04000054 RID: 84
		SMOOTH_HINTED = 4121,
		// Token: 0x04000055 RID: 85
		SMOOTH = 4117,
		// Token: 0x04000056 RID: 86
		RASTER_HINTED = 4122,
		// Token: 0x04000057 RID: 87
		RASTER = 4118,
		// Token: 0x04000058 RID: 88
		SDF = 4138,
		// Token: 0x04000059 RID: 89
		SDF8 = 8234,
		// Token: 0x0400005A RID: 90
		SDF16 = 16426,
		// Token: 0x0400005B RID: 91
		SDF32 = 32810,
		// Token: 0x0400005C RID: 92
		SDFAA_HINTED = 4169,
		// Token: 0x0400005D RID: 93
		SDFAA = 4165
	}
}
