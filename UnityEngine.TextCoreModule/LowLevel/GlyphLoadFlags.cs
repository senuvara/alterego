﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.TextCore.LowLevel
{
	// Token: 0x02000009 RID: 9
	[UsedByNativeCode]
	public enum GlyphLoadFlags
	{
		// Token: 0x0400002C RID: 44
		LOAD_DEFAULT,
		// Token: 0x0400002D RID: 45
		LOAD_NO_SCALE,
		// Token: 0x0400002E RID: 46
		LOAD_NO_HINTING,
		// Token: 0x0400002F RID: 47
		LOAD_RENDER = 4,
		// Token: 0x04000030 RID: 48
		LOAD_NO_BITMAP = 8,
		// Token: 0x04000031 RID: 49
		LOAD_FORCE_AUTOHINT = 32,
		// Token: 0x04000032 RID: 50
		LOAD_MONOCHROME = 4096,
		// Token: 0x04000033 RID: 51
		LOAD_NO_AUTOHINT = 32768,
		// Token: 0x04000034 RID: 52
		LOAD_COMPUTE_METRICS = 2097152,
		// Token: 0x04000035 RID: 53
		LOAD_BITMAP_METRICS_ONLY = 4194304
	}
}
