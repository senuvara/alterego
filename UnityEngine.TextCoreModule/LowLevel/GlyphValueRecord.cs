﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.TextCore.LowLevel
{
	// Token: 0x02000006 RID: 6
	[UsedByNativeCode]
	[Serializable]
	internal struct GlyphValueRecord
	{
		// Token: 0x0600005A RID: 90 RVA: 0x00002C64 File Offset: 0x00000E64
		public GlyphValueRecord(float xPlacement, float yPlacement, float xAdvance, float yAdvance)
		{
			this.m_XPlacement = xPlacement;
			this.m_YPlacement = yPlacement;
			this.m_XAdvance = xAdvance;
			this.m_YAdvance = yAdvance;
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x0600005B RID: 91 RVA: 0x00002C84 File Offset: 0x00000E84
		// (set) Token: 0x0600005C RID: 92 RVA: 0x00002C9F File Offset: 0x00000E9F
		public float xPlacement
		{
			get
			{
				return this.m_XPlacement;
			}
			set
			{
				this.m_XPlacement = value;
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x0600005D RID: 93 RVA: 0x00002CAC File Offset: 0x00000EAC
		// (set) Token: 0x0600005E RID: 94 RVA: 0x00002CC7 File Offset: 0x00000EC7
		public float yPlacement
		{
			get
			{
				return this.m_YPlacement;
			}
			set
			{
				this.m_YPlacement = value;
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x0600005F RID: 95 RVA: 0x00002CD4 File Offset: 0x00000ED4
		// (set) Token: 0x06000060 RID: 96 RVA: 0x00002CEF File Offset: 0x00000EEF
		public float xAdvance
		{
			get
			{
				return this.m_XAdvance;
			}
			set
			{
				this.m_XAdvance = value;
			}
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000061 RID: 97 RVA: 0x00002CFC File Offset: 0x00000EFC
		// (set) Token: 0x06000062 RID: 98 RVA: 0x00002D17 File Offset: 0x00000F17
		public float yAdvance
		{
			get
			{
				return this.m_YAdvance;
			}
			set
			{
				this.m_YAdvance = value;
			}
		}

		// Token: 0x06000063 RID: 99 RVA: 0x00002D24 File Offset: 0x00000F24
		public static GlyphValueRecord operator +(GlyphValueRecord a, GlyphValueRecord b)
		{
			GlyphValueRecord result;
			result.m_XPlacement = a.xPlacement + b.xPlacement;
			result.m_YPlacement = a.yPlacement + b.yPlacement;
			result.m_XAdvance = a.xAdvance + b.xAdvance;
			result.m_YAdvance = a.yAdvance + b.yAdvance;
			return result;
		}

		// Token: 0x04000023 RID: 35
		[SerializeField]
		[NativeName("xPlacement")]
		private float m_XPlacement;

		// Token: 0x04000024 RID: 36
		[SerializeField]
		[NativeName("yPlacement")]
		private float m_YPlacement;

		// Token: 0x04000025 RID: 37
		[SerializeField]
		[NativeName("xAdvance")]
		private float m_XAdvance;

		// Token: 0x04000026 RID: 38
		[SerializeField]
		[NativeName("yAdvance")]
		private float m_YAdvance;
	}
}
