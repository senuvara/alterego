﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.TextCore
{
	// Token: 0x02000003 RID: 3
	[UsedByNativeCode]
	[Serializable]
	public struct GlyphRect : IEquatable<GlyphRect>
	{
		// Token: 0x06000029 RID: 41 RVA: 0x000025AF File Offset: 0x000007AF
		public GlyphRect(int x, int y, int width, int height)
		{
			this.m_X = x;
			this.m_Y = y;
			this.m_Width = width;
			this.m_Height = height;
		}

		// Token: 0x0600002A RID: 42 RVA: 0x000025CF File Offset: 0x000007CF
		public GlyphRect(Rect rect)
		{
			this.m_X = (int)rect.x;
			this.m_Y = (int)rect.y;
			this.m_Width = (int)rect.width;
			this.m_Height = (int)rect.height;
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x0600002B RID: 43 RVA: 0x0000260C File Offset: 0x0000080C
		// (set) Token: 0x0600002C RID: 44 RVA: 0x00002627 File Offset: 0x00000827
		public int x
		{
			get
			{
				return this.m_X;
			}
			set
			{
				this.m_X = value;
			}
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x0600002D RID: 45 RVA: 0x00002634 File Offset: 0x00000834
		// (set) Token: 0x0600002E RID: 46 RVA: 0x0000264F File Offset: 0x0000084F
		public int y
		{
			get
			{
				return this.m_Y;
			}
			set
			{
				this.m_Y = value;
			}
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x0600002F RID: 47 RVA: 0x0000265C File Offset: 0x0000085C
		// (set) Token: 0x06000030 RID: 48 RVA: 0x00002677 File Offset: 0x00000877
		public int width
		{
			get
			{
				return this.m_Width;
			}
			set
			{
				this.m_Width = value;
			}
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000031 RID: 49 RVA: 0x00002684 File Offset: 0x00000884
		// (set) Token: 0x06000032 RID: 50 RVA: 0x0000269F File Offset: 0x0000089F
		public int height
		{
			get
			{
				return this.m_Height;
			}
			set
			{
				this.m_Height = value;
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000033 RID: 51 RVA: 0x000026AC File Offset: 0x000008AC
		public static GlyphRect zero
		{
			get
			{
				return GlyphRect.s_ZeroGlyphRect;
			}
		}

		// Token: 0x06000034 RID: 52 RVA: 0x000026C8 File Offset: 0x000008C8
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x06000035 RID: 53 RVA: 0x000026F0 File Offset: 0x000008F0
		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		// Token: 0x06000036 RID: 54 RVA: 0x00002718 File Offset: 0x00000918
		public bool Equals(GlyphRect other)
		{
			return base.Equals(other);
		}

		// Token: 0x06000037 RID: 55 RVA: 0x00002744 File Offset: 0x00000944
		public static bool operator ==(GlyphRect lhs, GlyphRect rhs)
		{
			return lhs.x == rhs.x && lhs.y == rhs.y && lhs.width == rhs.width && lhs.height == rhs.height;
		}

		// Token: 0x06000038 RID: 56 RVA: 0x000027A8 File Offset: 0x000009A8
		public static bool operator !=(GlyphRect lhs, GlyphRect rhs)
		{
			return !(lhs == rhs);
		}

		// Token: 0x06000039 RID: 57 RVA: 0x000027C7 File Offset: 0x000009C7
		// Note: this type is marked as 'beforefieldinit'.
		static GlyphRect()
		{
		}

		// Token: 0x04000014 RID: 20
		[SerializeField]
		[NativeName("x")]
		private int m_X;

		// Token: 0x04000015 RID: 21
		[SerializeField]
		[NativeName("y")]
		private int m_Y;

		// Token: 0x04000016 RID: 22
		[NativeName("width")]
		[SerializeField]
		private int m_Width;

		// Token: 0x04000017 RID: 23
		[SerializeField]
		[NativeName("height")]
		private int m_Height;

		// Token: 0x04000018 RID: 24
		private static readonly GlyphRect s_ZeroGlyphRect = new GlyphRect(0, 0, 0, 0);
	}
}
