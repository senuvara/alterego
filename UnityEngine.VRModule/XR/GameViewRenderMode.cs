﻿using System;

namespace UnityEngine.XR
{
	// Token: 0x0200000B RID: 11
	public enum GameViewRenderMode
	{
		// Token: 0x0400001C RID: 28
		LeftEye = 1,
		// Token: 0x0400001D RID: 29
		RightEye,
		// Token: 0x0400001E RID: 30
		BothEyes,
		// Token: 0x0400001F RID: 31
		OcclusionMesh
	}
}
