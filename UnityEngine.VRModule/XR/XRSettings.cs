﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.XR
{
	// Token: 0x0200000D RID: 13
	[NativeHeader("Runtime/VR/VRModule.h")]
	[NativeHeader("Runtime/GfxDevice/GfxDeviceTypes.h")]
	[NativeConditional("ENABLE_VR")]
	[NativeHeader("Runtime/Interfaces/IVRDevice.h")]
	[NativeHeader("Runtime/VR/ScriptBindings/XR.bindings.h")]
	public static class XRSettings
	{
		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000039 RID: 57
		// (set) Token: 0x0600003A RID: 58
		public static extern bool enabled { [StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeMethod("VRModuleBindings::SetDeviceEnabled", true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x0600003B RID: 59
		// (set) Token: 0x0600003C RID: 60
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		public static extern GameViewRenderMode gameViewRenderMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x0600003D RID: 61
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		[NativeName("Active")]
		public static extern bool isDeviceActive { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x0600003E RID: 62
		// (set) Token: 0x0600003F RID: 63
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		public static extern bool showDeviceView { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x06000040 RID: 64
		// (set) Token: 0x06000041 RID: 65
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		[Obsolete("renderScale is deprecated, use XRSettings.eyeTextureResolutionScale instead (UnityUpgradable) -> eyeTextureResolutionScale", false)]
		public static extern float renderScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x06000042 RID: 66
		// (set) Token: 0x06000043 RID: 67
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		[NativeName("RenderScale")]
		public static extern float eyeTextureResolutionScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000044 RID: 68
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		public static extern int eyeTextureWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x06000045 RID: 69
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		public static extern int eyeTextureHeight { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x06000046 RID: 70 RVA: 0x000020EC File Offset: 0x000002EC
		[NativeName("DefaultEyeTextureDesc")]
		[NativeConditional("ENABLE_VR", "RenderTextureDesc()")]
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		public static RenderTextureDescriptor eyeTextureDesc
		{
			get
			{
				RenderTextureDescriptor result;
				XRSettings.get_eyeTextureDesc_Injected(out result);
				return result;
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x06000047 RID: 71 RVA: 0x00002104 File Offset: 0x00000304
		// (set) Token: 0x06000048 RID: 72 RVA: 0x0000211E File Offset: 0x0000031E
		public static float renderViewportScale
		{
			get
			{
				return XRSettings.renderViewportScaleInternal;
			}
			set
			{
				if (value < 0f || value > 1f)
				{
					throw new ArgumentOutOfRangeException("value", "Render viewport scale should be between 0 and 1.");
				}
				XRSettings.renderViewportScaleInternal = value;
			}
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000049 RID: 73
		// (set) Token: 0x0600004A RID: 74
		[NativeName("RenderViewportScale")]
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		internal static extern float renderViewportScaleInternal { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x0600004B RID: 75
		// (set) Token: 0x0600004C RID: 76
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		public static extern float occlusionMaskScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x0600004D RID: 77
		// (set) Token: 0x0600004E RID: 78
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		public static extern bool useOcclusionMesh { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x0600004F RID: 79
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		[NativeName("DeviceName")]
		public static extern string loadedDeviceName { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000050 RID: 80 RVA: 0x0000214D File Offset: 0x0000034D
		public static void LoadDeviceByName(string deviceName)
		{
			XRSettings.LoadDeviceByName(new string[]
			{
				deviceName
			});
		}

		// Token: 0x06000051 RID: 81
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void LoadDeviceByName(string[] prioritizedDeviceNameList);

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x06000052 RID: 82
		public static extern string[] supportedDevices { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x06000053 RID: 83
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		public static extern XRSettings.StereoRenderingMode stereoRenderingMode { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000054 RID: 84
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_eyeTextureDesc_Injected(out RenderTextureDescriptor ret);

		// Token: 0x0200000E RID: 14
		public enum StereoRenderingMode
		{
			// Token: 0x04000025 RID: 37
			MultiPass,
			// Token: 0x04000026 RID: 38
			SinglePass,
			// Token: 0x04000027 RID: 39
			SinglePassInstanced,
			// Token: 0x04000028 RID: 40
			SinglePassMultiview
		}
	}
}
