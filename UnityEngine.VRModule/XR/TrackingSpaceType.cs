﻿using System;

namespace UnityEngine.XR
{
	// Token: 0x02000010 RID: 16
	public enum TrackingSpaceType
	{
		// Token: 0x0400002F RID: 47
		Stationary,
		// Token: 0x04000030 RID: 48
		RoomScale
	}
}
