﻿using System;

namespace UnityEngine.XR
{
	// Token: 0x0200000C RID: 12
	public enum TrackingOriginMode
	{
		// Token: 0x04000021 RID: 33
		Device,
		// Token: 0x04000022 RID: 34
		Floor,
		// Token: 0x04000023 RID: 35
		Unknown
	}
}
