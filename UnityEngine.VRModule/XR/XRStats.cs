﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.XR
{
	// Token: 0x02000012 RID: 18
	[NativeConditional("ENABLE_VR")]
	public static class XRStats
	{
		// Token: 0x06000065 RID: 101
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool TryGetGPUTimeLastFrame(out float gpuTimeLastFrame);

		// Token: 0x06000066 RID: 102
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool TryGetDroppedFrameCount(out int droppedFrameCount);

		// Token: 0x06000067 RID: 103
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool TryGetFramePresentCount(out int framePresentCount);

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x06000068 RID: 104 RVA: 0x000021EC File Offset: 0x000003EC
		[Obsolete("gpuTimeLastFrame is deprecated. Use XRStats.TryGetGPUTimeLastFrame instead.", false)]
		public static float gpuTimeLastFrame
		{
			get
			{
				float num;
				float result;
				if (XRStats.TryGetGPUTimeLastFrame(out num))
				{
					result = num;
				}
				else
				{
					result = 0f;
				}
				return result;
			}
		}
	}
}
