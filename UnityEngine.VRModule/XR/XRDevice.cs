﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.XR
{
	// Token: 0x02000011 RID: 17
	[NativeConditional("ENABLE_VR")]
	public static class XRDevice
	{
		// Token: 0x1700002C RID: 44
		// (get) Token: 0x06000055 RID: 85
		[NativeName("DeviceConnected")]
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		public static extern bool isPresent { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x06000056 RID: 86
		public static extern UserPresenceState userPresence { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x06000057 RID: 87
		[NativeName("DeviceName")]
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		[Obsolete("family is deprecated.  Use XRSettings.loadedDeviceName instead.", false)]
		public static extern string family { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x06000058 RID: 88
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		[NativeName("DeviceModel")]
		public static extern string model { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x06000059 RID: 89
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		[NativeName("DeviceRefreshRate")]
		public static extern float refreshRate { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600005A RID: 90
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern IntPtr GetNativePtr();

		// Token: 0x0600005B RID: 91
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern TrackingSpaceType GetTrackingSpaceType();

		// Token: 0x0600005C RID: 92
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool SetTrackingSpaceType(TrackingSpaceType trackingSpaceType);

		// Token: 0x0600005D RID: 93
		[NativeName("DisableAutoVRCameraTracking")]
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void DisableAutoXRCameraTracking([NotNull] Camera camera, bool disabled);

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x0600005E RID: 94
		// (set) Token: 0x0600005F RID: 95
		public static extern float fovZoomFactor { [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeName("SetProjectionZoomFactor")] [StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x06000060 RID: 96
		public static extern TrackingOriginMode trackingOriginMode { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x14000001 RID: 1
		// (add) Token: 0x06000061 RID: 97 RVA: 0x00002160 File Offset: 0x00000360
		// (remove) Token: 0x06000062 RID: 98 RVA: 0x00002194 File Offset: 0x00000394
		public static event Action<string> deviceLoaded
		{
			add
			{
				Action<string> action = XRDevice.deviceLoaded;
				Action<string> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<string>>(ref XRDevice.deviceLoaded, (Action<string>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<string> action = XRDevice.deviceLoaded;
				Action<string> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<string>>(ref XRDevice.deviceLoaded, (Action<string>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x06000063 RID: 99 RVA: 0x000021C8 File Offset: 0x000003C8
		[RequiredByNativeCode]
		private static void InvokeDeviceLoaded(string loadedDeviceName)
		{
			if (XRDevice.deviceLoaded != null)
			{
				XRDevice.deviceLoaded(loadedDeviceName);
			}
		}

		// Token: 0x06000064 RID: 100 RVA: 0x000021E2 File Offset: 0x000003E2
		// Note: this type is marked as 'beforefieldinit'.
		static XRDevice()
		{
		}

		// Token: 0x04000031 RID: 49
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private static Action<string> deviceLoaded = null;
	}
}
