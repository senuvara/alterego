﻿using System;

namespace UnityEngine.XR.WSA
{
	// Token: 0x0200001A RID: 26
	internal class SimulatedHand
	{
		// Token: 0x060000A2 RID: 162 RVA: 0x00002463 File Offset: 0x00000663
		internal SimulatedHand(GestureHand hand)
		{
			this.m_Hand = hand;
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x060000A3 RID: 163 RVA: 0x00002474 File Offset: 0x00000674
		// (set) Token: 0x060000A4 RID: 164 RVA: 0x00002494 File Offset: 0x00000694
		public Vector3 position
		{
			get
			{
				return HolographicAutomation.GetHandPosition(this.m_Hand);
			}
			set
			{
				HolographicAutomation.SetHandPosition(this.m_Hand, value);
			}
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x060000A5 RID: 165 RVA: 0x000024A4 File Offset: 0x000006A4
		// (set) Token: 0x060000A6 RID: 166 RVA: 0x000024C4 File Offset: 0x000006C4
		public bool activated
		{
			get
			{
				return HolographicAutomation.GetHandActivated(this.m_Hand);
			}
			set
			{
				HolographicAutomation.SetHandActivated(this.m_Hand, value);
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x060000A7 RID: 167 RVA: 0x000024D4 File Offset: 0x000006D4
		public bool visible
		{
			get
			{
				return HolographicAutomation.GetHandVisible(this.m_Hand);
			}
		}

		// Token: 0x060000A8 RID: 168 RVA: 0x000024F4 File Offset: 0x000006F4
		public void EnsureVisible()
		{
			HolographicAutomation.EnsureHandVisible(this.m_Hand);
		}

		// Token: 0x060000A9 RID: 169 RVA: 0x00002502 File Offset: 0x00000702
		public void PerformGesture(SimulatedGesture gesture)
		{
			HolographicAutomation.PerformGesture(this.m_Hand, gesture);
		}

		// Token: 0x0400003F RID: 63
		public GestureHand m_Hand;
	}
}
