﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.XR.WSA
{
	// Token: 0x0200001F RID: 31
	[NativeHeader("Runtime/VR/HoloLens/HolographicEmulation/HolographicEmulationManager.h")]
	[StaticAccessor("HolographicEmulation::HolographicEmulationManager::Get()", StaticAccessorType.Dot)]
	internal class HolographicEmulationHelper
	{
		// Token: 0x060000AB RID: 171 RVA: 0x000022A4 File Offset: 0x000004A4
		public HolographicEmulationHelper()
		{
		}

		// Token: 0x060000AC RID: 172
		[NativeConditional("ENABLE_HOLOLENS_MODULE", StubReturnStatement = "HolographicEmulation::EmulationMode_None")]
		[NativeName("GetEmulationMode")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern EmulationMode GetEmulationMode();
	}
}
