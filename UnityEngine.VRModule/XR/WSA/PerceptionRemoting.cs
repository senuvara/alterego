﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.XR.WSA
{
	// Token: 0x02000020 RID: 32
	[NativeHeader("Runtime/VR/HoloLens/PerceptionRemoting.h")]
	[NativeConditional("ENABLE_HOLOLENS_MODULE")]
	internal class PerceptionRemoting
	{
		// Token: 0x060000AD RID: 173 RVA: 0x000022A4 File Offset: 0x000004A4
		public PerceptionRemoting()
		{
		}

		// Token: 0x060000AE RID: 174
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Connect(string clientName);

		// Token: 0x060000AF RID: 175
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Disconnect();

		// Token: 0x060000B0 RID: 176
		[NativeConditional("ENABLE_HOLOLENS_MODULE", StubReturnStatement = "HolographicEmulation::None")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern HolographicStreamerConnectionFailureReason CheckForDisconnect();

		// Token: 0x060000B1 RID: 177
		[NativeConditional("ENABLE_HOLOLENS_MODULE", StubReturnStatement = "HolographicEmulation::Disconnected")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern HolographicStreamerConnectionState GetConnectionState();

		// Token: 0x060000B2 RID: 178
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetEnableAudio(bool enable);

		// Token: 0x060000B3 RID: 179
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetEnableVideo(bool enable);

		// Token: 0x060000B4 RID: 180
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetVideoEncodingParameters(int maxBitRate);
	}
}
