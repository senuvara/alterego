﻿using System;

namespace UnityEngine.XR.WSA
{
	// Token: 0x0200001B RID: 27
	internal static class HolographicEmulationHelpers
	{
		// Token: 0x060000AA RID: 170 RVA: 0x00002514 File Offset: 0x00000714
		public static Vector3 CalcExpectedCameraPosition(SimulatedHead head, SimulatedBody body)
		{
			Vector3 vector = body.position;
			vector.y += body.height - 1.776f;
			vector.y -= head.diameter / 2f;
			vector.y += 0.11599995f;
			Vector3 eulerAngles = head.eulerAngles;
			eulerAngles.y += body.rotation;
			Quaternion rotation = Quaternion.Euler(eulerAngles);
			vector += rotation * (0.0985f * Vector3.forward);
			return vector;
		}

		// Token: 0x04000040 RID: 64
		public const float k_DefaultBodyHeight = 1.776f;

		// Token: 0x04000041 RID: 65
		public const float k_DefaultHeadDiameter = 0.2319999f;

		// Token: 0x04000042 RID: 66
		public const float k_ForwardOffset = 0.0985f;
	}
}
