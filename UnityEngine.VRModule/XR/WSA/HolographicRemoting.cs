﻿using System;

namespace UnityEngine.XR.WSA
{
	// Token: 0x02000023 RID: 35
	public class HolographicRemoting
	{
		// Token: 0x060000C6 RID: 198 RVA: 0x000022A4 File Offset: 0x000004A4
		public HolographicRemoting()
		{
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x060000C7 RID: 199 RVA: 0x00002644 File Offset: 0x00000844
		public static HolographicStreamerConnectionState ConnectionState
		{
			get
			{
				return HolographicStreamerConnectionState.Disconnected;
			}
		}

		// Token: 0x060000C8 RID: 200 RVA: 0x00002626 File Offset: 0x00000826
		public static void Connect(string clientName, int maxBitRate = 9999)
		{
		}

		// Token: 0x060000C9 RID: 201 RVA: 0x00002626 File Offset: 0x00000826
		public static void Disconnect()
		{
		}
	}
}
