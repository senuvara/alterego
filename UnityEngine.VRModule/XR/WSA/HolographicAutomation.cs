﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.XR.WSA
{
	// Token: 0x02000017 RID: 23
	[NativeHeader("Runtime/VR/HoloLens/HolographicEmulation/HolographicEmulationManager.h")]
	[StaticAccessor("HolographicEmulation::HolographicEmulationManager::Get()", StaticAccessorType.Dot)]
	[NativeConditional("ENABLE_HOLOLENS_MODULE")]
	internal class HolographicAutomation
	{
		// Token: 0x06000073 RID: 115 RVA: 0x000022A4 File Offset: 0x000004A4
		public HolographicAutomation()
		{
		}

		// Token: 0x06000074 RID: 116
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Initialize();

		// Token: 0x06000075 RID: 117
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Shutdown();

		// Token: 0x06000076 RID: 118
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void LoadRoom(string id);

		// Token: 0x06000077 RID: 119
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetEmulationMode(EmulationMode mode);

		// Token: 0x06000078 RID: 120
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetGestureHand(GestureHand hand);

		// Token: 0x06000079 RID: 121
		[NativeName("ResetEmulationState")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Reset();

		// Token: 0x0600007A RID: 122
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void PerformGesture(GestureHand hand, SimulatedGesture gesture);

		// Token: 0x0600007B RID: 123 RVA: 0x000022AC File Offset: 0x000004AC
		[NativeConditional("ENABLE_HOLOLENS_MODULE", StubReturnStatement = "Vector3f::zero")]
		internal static Vector3 GetBodyPosition()
		{
			Vector3 result;
			HolographicAutomation.GetBodyPosition_Injected(out result);
			return result;
		}

		// Token: 0x0600007C RID: 124 RVA: 0x000022C1 File Offset: 0x000004C1
		internal static void SetBodyPosition(Vector3 position)
		{
			HolographicAutomation.SetBodyPosition_Injected(ref position);
		}

		// Token: 0x0600007D RID: 125
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern float GetBodyRotation();

		// Token: 0x0600007E RID: 126
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetBodyRotation(float degrees);

		// Token: 0x0600007F RID: 127
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern float GetBodyHeight();

		// Token: 0x06000080 RID: 128
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetBodyHeight(float degrees);

		// Token: 0x06000081 RID: 129 RVA: 0x000022CC File Offset: 0x000004CC
		[NativeConditional("ENABLE_HOLOLENS_MODULE", StubReturnStatement = "Vector3f::zero")]
		internal static Vector3 GetHeadRotation()
		{
			Vector3 result;
			HolographicAutomation.GetHeadRotation_Injected(out result);
			return result;
		}

		// Token: 0x06000082 RID: 130 RVA: 0x000022E1 File Offset: 0x000004E1
		internal static void SetHeadRotation(Vector3 degrees)
		{
			HolographicAutomation.SetHeadRotation_Injected(ref degrees);
		}

		// Token: 0x06000083 RID: 131
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern float GetHeadDiameter();

		// Token: 0x06000084 RID: 132
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetHeadDiameter(float degrees);

		// Token: 0x06000085 RID: 133 RVA: 0x000022EC File Offset: 0x000004EC
		[NativeConditional("ENABLE_HOLOLENS_MODULE", StubReturnStatement = "Vector3f::zero")]
		internal static Vector3 GetHandPosition(GestureHand hand)
		{
			Vector3 result;
			HolographicAutomation.GetHandPosition_Injected(hand, out result);
			return result;
		}

		// Token: 0x06000086 RID: 134 RVA: 0x00002302 File Offset: 0x00000502
		internal static void SetHandPosition(GestureHand hand, Vector3 position)
		{
			HolographicAutomation.SetHandPosition_Injected(hand, ref position);
		}

		// Token: 0x06000087 RID: 135
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool GetHandActivated(GestureHand hand);

		// Token: 0x06000088 RID: 136
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetHandActivated(GestureHand hand, bool activated);

		// Token: 0x06000089 RID: 137
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool GetHandVisible(GestureHand hand);

		// Token: 0x0600008A RID: 138
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void EnsureHandVisible(GestureHand hand);

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x0600008B RID: 139 RVA: 0x0000230C File Offset: 0x0000050C
		public static SimulatedBody simulatedBody
		{
			get
			{
				return HolographicAutomation.s_Body;
			}
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x0600008C RID: 140 RVA: 0x00002328 File Offset: 0x00000528
		public static SimulatedHead simulatedHead
		{
			get
			{
				return HolographicAutomation.s_Head;
			}
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x0600008D RID: 141 RVA: 0x00002344 File Offset: 0x00000544
		public static SimulatedHand simulatedLeftHand
		{
			get
			{
				return HolographicAutomation.s_LeftHand;
			}
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x0600008E RID: 142 RVA: 0x00002360 File Offset: 0x00000560
		public static SimulatedHand simulatedRightHand
		{
			get
			{
				return HolographicAutomation.s_RightHand;
			}
		}

		// Token: 0x0600008F RID: 143 RVA: 0x0000237A File Offset: 0x0000057A
		// Note: this type is marked as 'beforefieldinit'.
		static HolographicAutomation()
		{
		}

		// Token: 0x06000090 RID: 144
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetBodyPosition_Injected(out Vector3 ret);

		// Token: 0x06000091 RID: 145
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetBodyPosition_Injected(ref Vector3 position);

		// Token: 0x06000092 RID: 146
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetHeadRotation_Injected(out Vector3 ret);

		// Token: 0x06000093 RID: 147
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetHeadRotation_Injected(ref Vector3 degrees);

		// Token: 0x06000094 RID: 148
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetHandPosition_Injected(GestureHand hand, out Vector3 ret);

		// Token: 0x06000095 RID: 149
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetHandPosition_Injected(GestureHand hand, ref Vector3 position);

		// Token: 0x0400003B RID: 59
		private static SimulatedBody s_Body = new SimulatedBody();

		// Token: 0x0400003C RID: 60
		private static SimulatedHead s_Head = new SimulatedHead();

		// Token: 0x0400003D RID: 61
		private static SimulatedHand s_LeftHand = new SimulatedHand(GestureHand.Left);

		// Token: 0x0400003E RID: 62
		private static SimulatedHand s_RightHand = new SimulatedHand(GestureHand.Right);
	}
}
