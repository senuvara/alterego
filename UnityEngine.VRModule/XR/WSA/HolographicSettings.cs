﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.XR.WSA
{
	// Token: 0x02000021 RID: 33
	[NativeHeader("Runtime/VR/HoloLens/HolographicSettings.h")]
	[StaticAccessor("HolographicSettings::GetInstance()", StaticAccessorType.Dot)]
	public class HolographicSettings
	{
		// Token: 0x060000B5 RID: 181 RVA: 0x000022A4 File Offset: 0x000004A4
		public HolographicSettings()
		{
		}

		// Token: 0x060000B6 RID: 182 RVA: 0x000025B6 File Offset: 0x000007B6
		public static void SetFocusPointForFrame(Vector3 position)
		{
			HolographicSettings.InternalSetFocusPointForFrameP(position);
		}

		// Token: 0x060000B7 RID: 183 RVA: 0x000025BF File Offset: 0x000007BF
		public static void SetFocusPointForFrame(Vector3 position, Vector3 normal)
		{
			HolographicSettings.InternalSetFocusPointForFramePN(position, normal);
		}

		// Token: 0x060000B8 RID: 184 RVA: 0x000025C9 File Offset: 0x000007C9
		public static void SetFocusPointForFrame(Vector3 position, Vector3 normal, Vector3 velocity)
		{
			HolographicSettings.InternalSetFocusPointForFramePNV(position, normal, velocity);
		}

		// Token: 0x060000B9 RID: 185 RVA: 0x000025D4 File Offset: 0x000007D4
		[NativeConditional("ENABLE_HOLOLENS_MODULE")]
		[NativeName("SetFocusPointForFrame")]
		private static void InternalSetFocusPointForFrameP(Vector3 position)
		{
			HolographicSettings.InternalSetFocusPointForFrameP_Injected(ref position);
		}

		// Token: 0x060000BA RID: 186 RVA: 0x000025DD File Offset: 0x000007DD
		[NativeConditional("ENABLE_HOLOLENS_MODULE")]
		[NativeName("SetFocusPointForFrame")]
		private static void InternalSetFocusPointForFramePN(Vector3 position, Vector3 normal)
		{
			HolographicSettings.InternalSetFocusPointForFramePN_Injected(ref position, ref normal);
		}

		// Token: 0x060000BB RID: 187 RVA: 0x000025E8 File Offset: 0x000007E8
		[NativeConditional("ENABLE_HOLOLENS_MODULE")]
		[NativeName("SetFocusPointForFrame")]
		private static void InternalSetFocusPointForFramePNV(Vector3 position, Vector3 normal, Vector3 velocity)
		{
			HolographicSettings.InternalSetFocusPointForFramePNV_Injected(ref position, ref normal, ref velocity);
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x060000BC RID: 188 RVA: 0x000025F8 File Offset: 0x000007F8
		public static bool IsDisplayOpaque
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x060000BD RID: 189
		// (set) Token: 0x060000BE RID: 190
		[NativeConditional("ENABLE_HOLOLENS_MODULE")]
		public static extern bool IsContentProtectionEnabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x060000BF RID: 191 RVA: 0x00002610 File Offset: 0x00000810
		// (set) Token: 0x060000C0 RID: 192 RVA: 0x00002626 File Offset: 0x00000826
		public static HolographicSettings.HolographicReprojectionMode ReprojectionMode
		{
			get
			{
				return HolographicSettings.HolographicReprojectionMode.Disabled;
			}
			set
			{
			}
		}

		// Token: 0x060000C1 RID: 193 RVA: 0x00002626 File Offset: 0x00000826
		[Obsolete("Support for toggling latent frame presentation has been removed", true)]
		public static void ActivateLatentFramePresentation(bool activated)
		{
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x060000C2 RID: 194 RVA: 0x0000262C File Offset: 0x0000082C
		[Obsolete("Support for toggling latent frame presentation has been removed, and IsLatentFramePresentation will always return true", false)]
		public static bool IsLatentFramePresentation
		{
			get
			{
				return true;
			}
		}

		// Token: 0x060000C3 RID: 195
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalSetFocusPointForFrameP_Injected(ref Vector3 position);

		// Token: 0x060000C4 RID: 196
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalSetFocusPointForFramePN_Injected(ref Vector3 position, ref Vector3 normal);

		// Token: 0x060000C5 RID: 197
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalSetFocusPointForFramePNV_Injected(ref Vector3 position, ref Vector3 normal, ref Vector3 velocity);

		// Token: 0x02000022 RID: 34
		public enum HolographicReprojectionMode
		{
			// Token: 0x04000053 RID: 83
			PositionAndOrientation,
			// Token: 0x04000054 RID: 84
			OrientationOnly,
			// Token: 0x04000055 RID: 85
			Disabled
		}
	}
}
