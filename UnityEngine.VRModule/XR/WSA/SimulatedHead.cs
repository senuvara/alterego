﻿using System;

namespace UnityEngine.XR.WSA
{
	// Token: 0x02000019 RID: 25
	internal class SimulatedHead
	{
		// Token: 0x0600009D RID: 157 RVA: 0x000023A6 File Offset: 0x000005A6
		public SimulatedHead()
		{
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x0600009E RID: 158 RVA: 0x0000241C File Offset: 0x0000061C
		// (set) Token: 0x0600009F RID: 159 RVA: 0x00002436 File Offset: 0x00000636
		public float diameter
		{
			get
			{
				return HolographicAutomation.GetHeadDiameter();
			}
			set
			{
				HolographicAutomation.SetHeadDiameter(value);
			}
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x060000A0 RID: 160 RVA: 0x00002440 File Offset: 0x00000640
		// (set) Token: 0x060000A1 RID: 161 RVA: 0x0000245A File Offset: 0x0000065A
		public Vector3 eulerAngles
		{
			get
			{
				return HolographicAutomation.GetHeadRotation();
			}
			set
			{
				HolographicAutomation.SetHeadRotation(value);
			}
		}
	}
}
