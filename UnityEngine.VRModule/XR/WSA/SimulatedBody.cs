﻿using System;

namespace UnityEngine.XR.WSA
{
	// Token: 0x02000018 RID: 24
	internal class SimulatedBody
	{
		// Token: 0x06000096 RID: 150 RVA: 0x000023A6 File Offset: 0x000005A6
		public SimulatedBody()
		{
		}

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x06000097 RID: 151 RVA: 0x000023B0 File Offset: 0x000005B0
		// (set) Token: 0x06000098 RID: 152 RVA: 0x000023CA File Offset: 0x000005CA
		public Vector3 position
		{
			get
			{
				return HolographicAutomation.GetBodyPosition();
			}
			set
			{
				HolographicAutomation.SetBodyPosition(value);
			}
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x06000099 RID: 153 RVA: 0x000023D4 File Offset: 0x000005D4
		// (set) Token: 0x0600009A RID: 154 RVA: 0x000023EE File Offset: 0x000005EE
		public float rotation
		{
			get
			{
				return HolographicAutomation.GetBodyRotation();
			}
			set
			{
				HolographicAutomation.SetBodyRotation(value);
			}
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x0600009B RID: 155 RVA: 0x000023F8 File Offset: 0x000005F8
		// (set) Token: 0x0600009C RID: 156 RVA: 0x00002412 File Offset: 0x00000612
		public float height
		{
			get
			{
				return HolographicAutomation.GetBodyHeight();
			}
			set
			{
				HolographicAutomation.SetBodyHeight(value);
			}
		}
	}
}
