﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine.XR.WSA
{
	// Token: 0x0200001D RID: 29
	[NativeHeader("Runtime/VR/HoloLens/PerceptionRemoting.h")]
	public enum HolographicStreamerConnectionFailureReason
	{
		// Token: 0x04000048 RID: 72
		None,
		// Token: 0x04000049 RID: 73
		Unknown,
		// Token: 0x0400004A RID: 74
		Unreachable,
		// Token: 0x0400004B RID: 75
		HandshakeFailed,
		// Token: 0x0400004C RID: 76
		ProtocolVersionMismatch,
		// Token: 0x0400004D RID: 77
		ConnectionLost
	}
}
