﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine.XR.WSA
{
	// Token: 0x02000016 RID: 22
	[NativeHeader("Runtime/VR/HoloLens/HolographicEmulation/HolographicEmulationManager.h")]
	internal enum SimulatedGesture
	{
		// Token: 0x04000039 RID: 57
		FingerPressed,
		// Token: 0x0400003A RID: 58
		FingerReleased
	}
}
