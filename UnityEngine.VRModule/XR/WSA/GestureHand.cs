﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine.XR.WSA
{
	// Token: 0x02000015 RID: 21
	[NativeHeader("Runtime/VR/HoloLens/HolographicEmulation/HolographicEmulationManager.h")]
	internal enum GestureHand
	{
		// Token: 0x04000036 RID: 54
		Left,
		// Token: 0x04000037 RID: 55
		Right
	}
}
