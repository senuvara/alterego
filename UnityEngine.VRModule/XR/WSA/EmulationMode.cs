﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine.XR.WSA
{
	// Token: 0x0200001E RID: 30
	[NativeHeader("Runtime/VR/HoloLens/PerceptionRemoting.h")]
	internal enum EmulationMode
	{
		// Token: 0x0400004F RID: 79
		None,
		// Token: 0x04000050 RID: 80
		RemoteDevice,
		// Token: 0x04000051 RID: 81
		Simulated
	}
}
