﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine.XR.WSA
{
	// Token: 0x0200001C RID: 28
	[NativeHeader("Runtime/VR/HoloLens/PerceptionRemoting.h")]
	public enum HolographicStreamerConnectionState
	{
		// Token: 0x04000044 RID: 68
		Disconnected,
		// Token: 0x04000045 RID: 69
		Connecting,
		// Token: 0x04000046 RID: 70
		Connected
	}
}
