﻿using System;

namespace UnityEngine.XR
{
	// Token: 0x0200000F RID: 15
	public enum UserPresenceState
	{
		// Token: 0x0400002A RID: 42
		Unsupported = -1,
		// Token: 0x0400002B RID: 43
		NotPresent,
		// Token: 0x0400002C RID: 44
		Present,
		// Token: 0x0400002D RID: 45
		Unknown
	}
}
