﻿using System;
using System.ComponentModel;

namespace UnityEngine.VR
{
	// Token: 0x02000007 RID: 7
	[Obsolete("VRStats has been moved and renamed.  Use UnityEngine.XR.XRStats instead (UnityUpgradable) -> UnityEngine.XR.XRStats", true)]
	[EditorBrowsable(EditorBrowsableState.Never)]
	public static class VRStats
	{
		// Token: 0x0600001D RID: 29 RVA: 0x00002077 File Offset: 0x00000277
		public static bool TryGetGPUTimeLastFrame(out float gpuTimeLastFrame)
		{
			gpuTimeLastFrame = 0f;
			throw new NotSupportedException("VRStats has been moved and renamed.  Use UnityEngine.XR.XRStats instead.");
		}

		// Token: 0x0600001E RID: 30 RVA: 0x0000208B File Offset: 0x0000028B
		public static bool TryGetDroppedFrameCount(out int droppedFrameCount)
		{
			droppedFrameCount = 0;
			throw new NotSupportedException("VRStats has been moved and renamed.  Use UnityEngine.XR.XRStats instead.");
		}

		// Token: 0x0600001F RID: 31 RVA: 0x0000208B File Offset: 0x0000028B
		public static bool TryGetFramePresentCount(out int framePresentCount)
		{
			framePresentCount = 0;
			throw new NotSupportedException("VRStats has been moved and renamed.  Use UnityEngine.XR.XRStats instead.");
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000020 RID: 32 RVA: 0x0000209B File Offset: 0x0000029B
		[Obsolete("gpuTimeLastFrame is deprecated. Use XRStats.TryGetGPUTimeLastFrame instead.", true)]
		public static float gpuTimeLastFrame
		{
			get
			{
				throw new NotSupportedException("VRStats has been moved and renamed.  Use UnityEngine.XR.XRStats instead.");
			}
		}
	}
}
