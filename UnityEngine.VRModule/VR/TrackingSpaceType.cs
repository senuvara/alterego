﻿using System;
using System.ComponentModel;

namespace UnityEngine.VR
{
	// Token: 0x02000003 RID: 3
	[EditorBrowsable(EditorBrowsableState.Never)]
	[Obsolete("TrackingSpaceType has been moved.  Use UnityEngine.XR.TrackingSpaceType instead (UnityUpgradable) -> UnityEngine.XR.TrackingSpaceType", true)]
	public enum TrackingSpaceType
	{
		// Token: 0x0400000A RID: 10
		Stationary,
		// Token: 0x0400000B RID: 11
		RoomScale
	}
}
