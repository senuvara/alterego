﻿using System;

namespace UnityEngine.VR
{
	// Token: 0x02000009 RID: 9
	[Obsolete("VRNode has been moved and renamed.  Use UnityEngine.XR.XRNode instead (UnityUpgradable) -> UnityEngine.XR.XRNode", true)]
	public enum VRNode
	{
		// Token: 0x04000012 RID: 18
		LeftEye,
		// Token: 0x04000013 RID: 19
		RightEye,
		// Token: 0x04000014 RID: 20
		CenterEye,
		// Token: 0x04000015 RID: 21
		Head,
		// Token: 0x04000016 RID: 22
		LeftHand,
		// Token: 0x04000017 RID: 23
		RightHand,
		// Token: 0x04000018 RID: 24
		GameController,
		// Token: 0x04000019 RID: 25
		TrackingReference,
		// Token: 0x0400001A RID: 26
		HardwareTracker
	}
}
