﻿using System;
using System.ComponentModel;
using UnityEngine.XR;

namespace UnityEngine.VR
{
	// Token: 0x02000006 RID: 6
	[EditorBrowsable(EditorBrowsableState.Never)]
	[Obsolete("VRDevice has been moved and renamed.  Use UnityEngine.XR.XRDevice instead (UnityUpgradable) -> UnityEngine.XR.XRDevice", true)]
	public static class VRDevice
	{
		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000014 RID: 20 RVA: 0x0000206A File Offset: 0x0000026A
		public static bool isPresent
		{
			get
			{
				throw new NotSupportedException("VRDevice has been moved and renamed.  Use UnityEngine.XR.XRDevice instead.");
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000015 RID: 21 RVA: 0x0000206A File Offset: 0x0000026A
		public static UserPresenceState userPresence
		{
			get
			{
				throw new NotSupportedException("VRDevice has been moved and renamed.  Use UnityEngine.XR.XRDevice instead.");
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x06000016 RID: 22 RVA: 0x0000206A File Offset: 0x0000026A
		[Obsolete("family is deprecated.  Use XRSettings.loadedDeviceName instead.", true)]
		public static string family
		{
			get
			{
				throw new NotSupportedException("VRDevice has been moved and renamed.  Use UnityEngine.XR.XRDevice instead.");
			}
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000017 RID: 23 RVA: 0x0000206A File Offset: 0x0000026A
		public static string model
		{
			get
			{
				throw new NotSupportedException("VRDevice has been moved and renamed.  Use UnityEngine.XR.XRDevice instead.");
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000018 RID: 24 RVA: 0x0000206A File Offset: 0x0000026A
		public static float refreshRate
		{
			get
			{
				throw new NotSupportedException("VRDevice has been moved and renamed.  Use UnityEngine.XR.XRDevice instead.");
			}
		}

		// Token: 0x06000019 RID: 25 RVA: 0x0000206A File Offset: 0x0000026A
		public static TrackingSpaceType GetTrackingSpaceType()
		{
			throw new NotSupportedException("VRDevice has been moved and renamed.  Use UnityEngine.XR.XRDevice instead.");
		}

		// Token: 0x0600001A RID: 26 RVA: 0x0000206A File Offset: 0x0000026A
		public static bool SetTrackingSpaceType(TrackingSpaceType trackingSpaceType)
		{
			throw new NotSupportedException("VRDevice has been moved and renamed.  Use UnityEngine.XR.XRDevice instead.");
		}

		// Token: 0x0600001B RID: 27 RVA: 0x0000206A File Offset: 0x0000026A
		public static IntPtr GetNativePtr()
		{
			throw new NotSupportedException("VRDevice has been moved and renamed.  Use UnityEngine.XR.XRDevice instead.");
		}

		// Token: 0x0600001C RID: 28 RVA: 0x0000206A File Offset: 0x0000026A
		[Obsolete("DisableAutoVRCameraTracking has been moved and renamed.  Use UnityEngine.XR.XRDevice.DisableAutoXRCameraTracking instead (UnityUpgradable) -> UnityEngine.XR.XRDevice.DisableAutoXRCameraTracking(*)", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void DisableAutoVRCameraTracking(Camera camera, bool disabled)
		{
			throw new NotSupportedException("VRDevice has been moved and renamed.  Use UnityEngine.XR.XRDevice instead.");
		}
	}
}
