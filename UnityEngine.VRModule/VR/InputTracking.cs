﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace UnityEngine.VR
{
	// Token: 0x02000008 RID: 8
	[Obsolete("InputTracking has been moved.  Use UnityEngine.XR.InputTracking instead (UnityUpgradable) -> UnityEngine.XR.InputTracking", true)]
	[EditorBrowsable(EditorBrowsableState.Never)]
	public static class InputTracking
	{
		// Token: 0x06000021 RID: 33 RVA: 0x000020A8 File Offset: 0x000002A8
		public static Vector3 GetLocalPosition(VRNode node)
		{
			throw new NotSupportedException("InputTracking has been moved.  Use UnityEngine.XR.InputTracking instead.");
		}

		// Token: 0x06000022 RID: 34 RVA: 0x000020A8 File Offset: 0x000002A8
		public static void Recenter()
		{
			throw new NotSupportedException("InputTracking has been moved.  Use UnityEngine.XR.InputTracking instead.");
		}

		// Token: 0x06000023 RID: 35 RVA: 0x000020A8 File Offset: 0x000002A8
		public static string GetNodeName(ulong uniqueID)
		{
			throw new NotSupportedException("InputTracking has been moved.  Use UnityEngine.XR.InputTracking instead.");
		}

		// Token: 0x06000024 RID: 36 RVA: 0x000020A8 File Offset: 0x000002A8
		public static void GetNodeStates(List<VRNodeState> nodeStates)
		{
			throw new NotSupportedException("InputTracking has been moved.  Use UnityEngine.XR.InputTracking instead.");
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000025 RID: 37 RVA: 0x000020A8 File Offset: 0x000002A8
		// (set) Token: 0x06000026 RID: 38 RVA: 0x000020A8 File Offset: 0x000002A8
		public static bool disablePositionalTracking
		{
			get
			{
				throw new NotSupportedException("InputTracking has been moved.  Use UnityEngine.XR.InputTracking instead.");
			}
			set
			{
				throw new NotSupportedException("InputTracking has been moved.  Use UnityEngine.XR.InputTracking instead.");
			}
		}
	}
}
