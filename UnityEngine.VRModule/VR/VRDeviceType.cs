﻿using System;
using System.ComponentModel;

namespace UnityEngine.VR
{
	// Token: 0x02000002 RID: 2
	[Obsolete("VRDeviceType is deprecated. Use XRSettings.supportedDevices instead.", true)]
	public enum VRDeviceType
	{
		// Token: 0x04000002 RID: 2
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Enum member VRDeviceType.Morpheus has been deprecated. Use VRDeviceType.PlayStationVR instead (UnityUpgradable) -> PlayStationVR", true)]
		Morpheus = -1,
		// Token: 0x04000003 RID: 3
		None,
		// Token: 0x04000004 RID: 4
		Stereo,
		// Token: 0x04000005 RID: 5
		Split,
		// Token: 0x04000006 RID: 6
		Oculus,
		// Token: 0x04000007 RID: 7
		PlayStationVR,
		// Token: 0x04000008 RID: 8
		Unknown
	}
}
