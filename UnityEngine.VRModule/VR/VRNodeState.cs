﻿using System;
using System.ComponentModel;

namespace UnityEngine.VR
{
	// Token: 0x0200000A RID: 10
	[Obsolete("VRNodeState has been moved and renamed.  Use UnityEngine.XR.XRNodeState instead (UnityUpgradable) -> UnityEngine.XR.XRNodeState", true)]
	[EditorBrowsable(EditorBrowsableState.Never)]
	public struct VRNodeState
	{
		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000027 RID: 39 RVA: 0x000020B5 File Offset: 0x000002B5
		// (set) Token: 0x06000028 RID: 40 RVA: 0x000020B5 File Offset: 0x000002B5
		public ulong uniqueID
		{
			get
			{
				throw new NotSupportedException("VRNodeState has been moved and renamed.  Use UnityEngine.XR.XRNodeState instead.");
			}
			set
			{
				throw new NotSupportedException("VRNodeState has been moved and renamed.  Use UnityEngine.XR.XRNodeState instead.");
			}
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000029 RID: 41 RVA: 0x000020B5 File Offset: 0x000002B5
		// (set) Token: 0x0600002A RID: 42 RVA: 0x000020B5 File Offset: 0x000002B5
		public VRNode nodeType
		{
			get
			{
				throw new NotSupportedException("VRNodeState has been moved and renamed.  Use UnityEngine.XR.XRNodeState instead.");
			}
			set
			{
				throw new NotSupportedException("VRNodeState has been moved and renamed.  Use UnityEngine.XR.XRNodeState instead.");
			}
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x0600002B RID: 43 RVA: 0x000020B5 File Offset: 0x000002B5
		// (set) Token: 0x0600002C RID: 44 RVA: 0x000020B5 File Offset: 0x000002B5
		public bool tracked
		{
			get
			{
				throw new NotSupportedException("VRNodeState has been moved and renamed.  Use UnityEngine.XR.XRNodeState instead.");
			}
			set
			{
				throw new NotSupportedException("VRNodeState has been moved and renamed.  Use UnityEngine.XR.XRNodeState instead.");
			}
		}

		// Token: 0x17000016 RID: 22
		// (set) Token: 0x0600002D RID: 45 RVA: 0x000020B5 File Offset: 0x000002B5
		public Vector3 position
		{
			set
			{
				throw new NotSupportedException("VRNodeState has been moved and renamed.  Use UnityEngine.XR.XRNodeState instead.");
			}
		}

		// Token: 0x17000017 RID: 23
		// (set) Token: 0x0600002E RID: 46 RVA: 0x000020B5 File Offset: 0x000002B5
		public Quaternion rotation
		{
			set
			{
				throw new NotSupportedException("VRNodeState has been moved and renamed.  Use UnityEngine.XR.XRNodeState instead.");
			}
		}

		// Token: 0x17000018 RID: 24
		// (set) Token: 0x0600002F RID: 47 RVA: 0x000020B5 File Offset: 0x000002B5
		public Vector3 velocity
		{
			set
			{
				throw new NotSupportedException("VRNodeState has been moved and renamed.  Use UnityEngine.XR.XRNodeState instead.");
			}
		}

		// Token: 0x17000019 RID: 25
		// (set) Token: 0x06000030 RID: 48 RVA: 0x000020B5 File Offset: 0x000002B5
		public Vector3 angularVelocity
		{
			set
			{
				throw new NotSupportedException("VRNodeState has been moved and renamed.  Use UnityEngine.XR.XRNodeState instead.");
			}
		}

		// Token: 0x1700001A RID: 26
		// (set) Token: 0x06000031 RID: 49 RVA: 0x000020B5 File Offset: 0x000002B5
		public Vector3 acceleration
		{
			set
			{
				throw new NotSupportedException("VRNodeState has been moved and renamed.  Use UnityEngine.XR.XRNodeState instead.");
			}
		}

		// Token: 0x1700001B RID: 27
		// (set) Token: 0x06000032 RID: 50 RVA: 0x000020B5 File Offset: 0x000002B5
		public Vector3 angularAcceleration
		{
			set
			{
				throw new NotSupportedException("VRNodeState has been moved and renamed.  Use UnityEngine.XR.XRNodeState instead.");
			}
		}

		// Token: 0x06000033 RID: 51 RVA: 0x000020C2 File Offset: 0x000002C2
		public bool TryGetPosition(out Vector3 position)
		{
			position = default(Vector3);
			throw new NotSupportedException("VRNodeState has been moved and renamed.  Use UnityEngine.XR.XRNodeState instead.");
		}

		// Token: 0x06000034 RID: 52 RVA: 0x000020D6 File Offset: 0x000002D6
		public bool TryGetRotation(out Quaternion rotation)
		{
			rotation = default(Quaternion);
			throw new NotSupportedException("VRNodeState has been moved and renamed.  Use UnityEngine.XR.XRNodeState instead.");
		}

		// Token: 0x06000035 RID: 53 RVA: 0x000020C2 File Offset: 0x000002C2
		public bool TryGetVelocity(out Vector3 velocity)
		{
			velocity = default(Vector3);
			throw new NotSupportedException("VRNodeState has been moved and renamed.  Use UnityEngine.XR.XRNodeState instead.");
		}

		// Token: 0x06000036 RID: 54 RVA: 0x000020C2 File Offset: 0x000002C2
		public bool TryGetAngularVelocity(out Vector3 angularVelocity)
		{
			angularVelocity = default(Vector3);
			throw new NotSupportedException("VRNodeState has been moved and renamed.  Use UnityEngine.XR.XRNodeState instead.");
		}

		// Token: 0x06000037 RID: 55 RVA: 0x000020C2 File Offset: 0x000002C2
		public bool TryGetAcceleration(out Vector3 acceleration)
		{
			acceleration = default(Vector3);
			throw new NotSupportedException("VRNodeState has been moved and renamed.  Use UnityEngine.XR.XRNodeState instead.");
		}

		// Token: 0x06000038 RID: 56 RVA: 0x000020C2 File Offset: 0x000002C2
		public bool TryGetAngularAcceleration(out Vector3 angularAcceleration)
		{
			angularAcceleration = default(Vector3);
			throw new NotSupportedException("VRNodeState has been moved and renamed.  Use UnityEngine.XR.XRNodeState instead.");
		}
	}
}
