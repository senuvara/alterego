﻿using System;
using System.ComponentModel;

namespace UnityEngine.VR
{
	// Token: 0x02000004 RID: 4
	[Obsolete("UserPresenceState has been moved.  Use UnityEngine.XR.UserPresenceState instead (UnityUpgradable) -> UnityEngine.XR.UserPresenceState", true)]
	[EditorBrowsable(EditorBrowsableState.Never)]
	public enum UserPresenceState
	{
		// Token: 0x0400000D RID: 13
		Unsupported = -1,
		// Token: 0x0400000E RID: 14
		NotPresent,
		// Token: 0x0400000F RID: 15
		Present,
		// Token: 0x04000010 RID: 16
		Unknown
	}
}
