﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;

namespace UnityEngine.Experimental.XR
{
	// Token: 0x02000013 RID: 19
	[NativeConditional("ENABLE_VR")]
	public static class Boundary
	{
		// Token: 0x06000069 RID: 105 RVA: 0x0000221C File Offset: 0x0000041C
		public static bool TryGetDimensions(out Vector3 dimensionsOut)
		{
			return Boundary.TryGetDimensions(out dimensionsOut, Boundary.Type.PlayArea);
		}

		// Token: 0x0600006A RID: 106 RVA: 0x00002238 File Offset: 0x00000438
		public static bool TryGetDimensions(out Vector3 dimensionsOut, [DefaultValue("Type.PlayArea")] Boundary.Type boundaryType)
		{
			return Boundary.TryGetDimensionsInternal(out dimensionsOut, boundaryType);
		}

		// Token: 0x0600006B RID: 107
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		[NativeName("TryGetBoundaryDimensions")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool TryGetDimensionsInternal(out Vector3 dimensionsOut, Boundary.Type boundaryType);

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x0600006C RID: 108
		// (set) Token: 0x0600006D RID: 109
		[NativeName("BoundaryVisible")]
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		public static extern bool visible { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x0600006E RID: 110
		[StaticAccessor("GetIVRDevice()", StaticAccessorType.ArrowWithDefaultReturnIfNull)]
		[NativeName("BoundaryConfigured")]
		public static extern bool configured { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600006F RID: 111 RVA: 0x00002254 File Offset: 0x00000454
		public static bool TryGetGeometry(List<Vector3> geometry)
		{
			return Boundary.TryGetGeometry(geometry, Boundary.Type.PlayArea);
		}

		// Token: 0x06000070 RID: 112 RVA: 0x00002270 File Offset: 0x00000470
		public static bool TryGetGeometry(List<Vector3> geometry, [DefaultValue("Type.PlayArea")] Boundary.Type boundaryType)
		{
			if (geometry == null)
			{
				throw new ArgumentNullException("geometry");
			}
			geometry.Clear();
			return Boundary.TryGetGeometryScriptingInternal(geometry, boundaryType);
		}

		// Token: 0x06000071 RID: 113
		[NativeConditional("!ENABLE_DOTNET")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool TryGetGeometryScriptingInternal(List<Vector3> geometry, Boundary.Type boundaryType);

		// Token: 0x06000072 RID: 114
		[NativeConditional("ENABLE_DOTNET")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Vector3[] TryGetGeometryArrayInternal(Boundary.Type boundaryType);

		// Token: 0x02000014 RID: 20
		public enum Type
		{
			// Token: 0x04000033 RID: 51
			PlayArea,
			// Token: 0x04000034 RID: 52
			TrackedArea
		}
	}
}
