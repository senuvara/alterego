﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;
using UnityEngine.XR;

namespace UnityEngine.Internal.VR
{
	// Token: 0x02000024 RID: 36
	public static class VRTestMock
	{
		// Token: 0x060000CA RID: 202
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Reset();

		// Token: 0x060000CB RID: 203
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void AddTrackedDevice(XRNode nodeType);

		// Token: 0x060000CC RID: 204 RVA: 0x0000265A File Offset: 0x0000085A
		public static void UpdateTrackedDevice(XRNode nodeType, Vector3 position, Quaternion rotation)
		{
			VRTestMock.INTERNAL_CALL_UpdateTrackedDevice(nodeType, ref position, ref rotation);
		}

		// Token: 0x060000CD RID: 205
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_UpdateTrackedDevice(XRNode nodeType, ref Vector3 position, ref Quaternion rotation);

		// Token: 0x060000CE RID: 206 RVA: 0x00002667 File Offset: 0x00000867
		public static void UpdateLeftEye(Vector3 position, Quaternion rotation)
		{
			VRTestMock.INTERNAL_CALL_UpdateLeftEye(ref position, ref rotation);
		}

		// Token: 0x060000CF RID: 207
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_UpdateLeftEye(ref Vector3 position, ref Quaternion rotation);

		// Token: 0x060000D0 RID: 208 RVA: 0x00002673 File Offset: 0x00000873
		public static void UpdateRightEye(Vector3 position, Quaternion rotation)
		{
			VRTestMock.INTERNAL_CALL_UpdateRightEye(ref position, ref rotation);
		}

		// Token: 0x060000D1 RID: 209
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_UpdateRightEye(ref Vector3 position, ref Quaternion rotation);

		// Token: 0x060000D2 RID: 210 RVA: 0x0000267F File Offset: 0x0000087F
		public static void UpdateCenterEye(Vector3 position, Quaternion rotation)
		{
			VRTestMock.INTERNAL_CALL_UpdateCenterEye(ref position, ref rotation);
		}

		// Token: 0x060000D3 RID: 211
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_UpdateCenterEye(ref Vector3 position, ref Quaternion rotation);

		// Token: 0x060000D4 RID: 212 RVA: 0x0000268B File Offset: 0x0000088B
		public static void UpdateHead(Vector3 position, Quaternion rotation)
		{
			VRTestMock.INTERNAL_CALL_UpdateHead(ref position, ref rotation);
		}

		// Token: 0x060000D5 RID: 213
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_UpdateHead(ref Vector3 position, ref Quaternion rotation);

		// Token: 0x060000D6 RID: 214 RVA: 0x00002697 File Offset: 0x00000897
		public static void UpdateLeftHand(Vector3 position, Quaternion rotation)
		{
			VRTestMock.INTERNAL_CALL_UpdateLeftHand(ref position, ref rotation);
		}

		// Token: 0x060000D7 RID: 215
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_UpdateLeftHand(ref Vector3 position, ref Quaternion rotation);

		// Token: 0x060000D8 RID: 216 RVA: 0x000026A3 File Offset: 0x000008A3
		public static void UpdateRightHand(Vector3 position, Quaternion rotation)
		{
			VRTestMock.INTERNAL_CALL_UpdateRightHand(ref position, ref rotation);
		}

		// Token: 0x060000D9 RID: 217
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_UpdateRightHand(ref Vector3 position, ref Quaternion rotation);

		// Token: 0x060000DA RID: 218
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void AddController(string controllerName);

		// Token: 0x060000DB RID: 219
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void UpdateControllerAxis(string controllerName, int axis, float value);

		// Token: 0x060000DC RID: 220
		[GeneratedByOldBindingsGenerator]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void UpdateControllerButton(string controllerName, int button, bool pressed);
	}
}
