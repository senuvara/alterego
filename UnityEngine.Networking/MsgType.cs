﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000074 RID: 116
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class MsgType
	{
		// Token: 0x060005AE RID: 1454 RVA: 0x0001ED4C File Offset: 0x0001CF4C
		public MsgType()
		{
		}

		// Token: 0x060005AF RID: 1455 RVA: 0x0001ED54 File Offset: 0x0001CF54
		public static string MsgTypeToString(short value)
		{
			string result;
			if (value < 0 || value > 47)
			{
				result = string.Empty;
			}
			else
			{
				string text = MsgType.msgLabels[(int)value];
				if (string.IsNullOrEmpty(text))
				{
					text = "[" + value + "]";
				}
				result = text;
			}
			return result;
		}

		// Token: 0x060005B0 RID: 1456 RVA: 0x0001EDB0 File Offset: 0x0001CFB0
		// Note: this type is marked as 'beforefieldinit'.
		static MsgType()
		{
		}

		// Token: 0x04000241 RID: 577
		public const short ObjectDestroy = 1;

		// Token: 0x04000242 RID: 578
		public const short Rpc = 2;

		// Token: 0x04000243 RID: 579
		public const short ObjectSpawn = 3;

		// Token: 0x04000244 RID: 580
		public const short Owner = 4;

		// Token: 0x04000245 RID: 581
		public const short Command = 5;

		// Token: 0x04000246 RID: 582
		public const short LocalPlayerTransform = 6;

		// Token: 0x04000247 RID: 583
		public const short SyncEvent = 7;

		// Token: 0x04000248 RID: 584
		public const short UpdateVars = 8;

		// Token: 0x04000249 RID: 585
		public const short SyncList = 9;

		// Token: 0x0400024A RID: 586
		public const short ObjectSpawnScene = 10;

		// Token: 0x0400024B RID: 587
		public const short NetworkInfo = 11;

		// Token: 0x0400024C RID: 588
		public const short SpawnFinished = 12;

		// Token: 0x0400024D RID: 589
		public const short ObjectHide = 13;

		// Token: 0x0400024E RID: 590
		public const short CRC = 14;

		// Token: 0x0400024F RID: 591
		public const short LocalClientAuthority = 15;

		// Token: 0x04000250 RID: 592
		public const short LocalChildTransform = 16;

		// Token: 0x04000251 RID: 593
		public const short Fragment = 17;

		// Token: 0x04000252 RID: 594
		public const short PeerClientAuthority = 18;

		// Token: 0x04000253 RID: 595
		internal const short UserMessage = 0;

		// Token: 0x04000254 RID: 596
		internal const short HLAPIMsg = 28;

		// Token: 0x04000255 RID: 597
		internal const short LLAPIMsg = 29;

		// Token: 0x04000256 RID: 598
		internal const short HLAPIResend = 30;

		// Token: 0x04000257 RID: 599
		internal const short HLAPIPending = 31;

		// Token: 0x04000258 RID: 600
		public const short InternalHighest = 31;

		// Token: 0x04000259 RID: 601
		public const short Connect = 32;

		// Token: 0x0400025A RID: 602
		public const short Disconnect = 33;

		// Token: 0x0400025B RID: 603
		public const short Error = 34;

		// Token: 0x0400025C RID: 604
		public const short Ready = 35;

		// Token: 0x0400025D RID: 605
		public const short NotReady = 36;

		// Token: 0x0400025E RID: 606
		public const short AddPlayer = 37;

		// Token: 0x0400025F RID: 607
		public const short RemovePlayer = 38;

		// Token: 0x04000260 RID: 608
		public const short Scene = 39;

		// Token: 0x04000261 RID: 609
		public const short Animation = 40;

		// Token: 0x04000262 RID: 610
		public const short AnimationParameters = 41;

		// Token: 0x04000263 RID: 611
		public const short AnimationTrigger = 42;

		// Token: 0x04000264 RID: 612
		public const short LobbyReadyToBegin = 43;

		// Token: 0x04000265 RID: 613
		public const short LobbySceneLoaded = 44;

		// Token: 0x04000266 RID: 614
		public const short LobbyAddPlayerFailed = 45;

		// Token: 0x04000267 RID: 615
		public const short LobbyReturnToLobby = 46;

		// Token: 0x04000268 RID: 616
		public const short ReconnectPlayer = 47;

		// Token: 0x04000269 RID: 617
		public const short Highest = 47;

		// Token: 0x0400026A RID: 618
		internal static string[] msgLabels = new string[]
		{
			"none",
			"ObjectDestroy",
			"Rpc",
			"ObjectSpawn",
			"Owner",
			"Command",
			"LocalPlayerTransform",
			"SyncEvent",
			"UpdateVars",
			"SyncList",
			"ObjectSpawnScene",
			"NetworkInfo",
			"SpawnFinished",
			"ObjectHide",
			"CRC",
			"LocalClientAuthority",
			"LocalChildTransform",
			"Fragment",
			"PeerClientAuthority",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"Connect",
			"Disconnect",
			"Error",
			"Ready",
			"NotReady",
			"AddPlayer",
			"RemovePlayer",
			"Scene",
			"Animation",
			"AnimationParams",
			"AnimationTrigger",
			"LobbyReadyToBegin",
			"LobbySceneLoaded",
			"LobbyAddPlayerFailed",
			"LobbyReturnToLobby",
			"ReconnectPlayer"
		};
	}
}
