﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Networking
{
	// Token: 0x02000006 RID: 6
	internal class ConnectionArray
	{
		// Token: 0x0600005B RID: 91 RVA: 0x0000489D File Offset: 0x00002A9D
		public ConnectionArray()
		{
			this.m_Connections = new List<NetworkConnection>();
			this.m_LocalConnections = new List<NetworkConnection>();
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x0600005C RID: 92 RVA: 0x000048BC File Offset: 0x00002ABC
		internal List<NetworkConnection> localConnections
		{
			get
			{
				return this.m_LocalConnections;
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x0600005D RID: 93 RVA: 0x000048D8 File Offset: 0x00002AD8
		internal List<NetworkConnection> connections
		{
			get
			{
				return this.m_Connections;
			}
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x0600005E RID: 94 RVA: 0x000048F4 File Offset: 0x00002AF4
		public int Count
		{
			get
			{
				return this.m_Connections.Count;
			}
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x0600005F RID: 95 RVA: 0x00004914 File Offset: 0x00002B14
		public int LocalIndex
		{
			get
			{
				return -this.m_LocalConnections.Count;
			}
		}

		// Token: 0x06000060 RID: 96 RVA: 0x00004938 File Offset: 0x00002B38
		public int Add(int connId, NetworkConnection conn)
		{
			int result;
			if (connId < 0)
			{
				if (LogFilter.logWarn)
				{
					Debug.LogWarning("ConnectionArray Add bad id " + connId);
				}
				result = -1;
			}
			else if (connId < this.m_Connections.Count && this.m_Connections[connId] != null)
			{
				if (LogFilter.logWarn)
				{
					Debug.LogWarning("ConnectionArray Add dupe at " + connId);
				}
				result = -1;
			}
			else
			{
				while (connId > this.m_Connections.Count - 1)
				{
					this.m_Connections.Add(null);
				}
				this.m_Connections[connId] = conn;
				result = connId;
			}
			return result;
		}

		// Token: 0x06000061 RID: 97 RVA: 0x000049FC File Offset: 0x00002BFC
		public NetworkConnection Get(int connId)
		{
			NetworkConnection result;
			if (connId < 0)
			{
				result = this.m_LocalConnections[Mathf.Abs(connId) - 1];
			}
			else if (connId > this.m_Connections.Count)
			{
				if (LogFilter.logWarn)
				{
					Debug.LogWarning("ConnectionArray Get invalid index " + connId);
				}
				result = null;
			}
			else
			{
				result = this.m_Connections[connId];
			}
			return result;
		}

		// Token: 0x06000062 RID: 98 RVA: 0x00004A78 File Offset: 0x00002C78
		public NetworkConnection GetUnsafe(int connId)
		{
			NetworkConnection result;
			if (connId < 0 || connId > this.m_Connections.Count)
			{
				result = null;
			}
			else
			{
				result = this.m_Connections[connId];
			}
			return result;
		}

		// Token: 0x06000063 RID: 99 RVA: 0x00004ABC File Offset: 0x00002CBC
		public void Remove(int connId)
		{
			if (connId < 0)
			{
				this.m_LocalConnections[Mathf.Abs(connId) - 1] = null;
			}
			else if (connId > this.m_Connections.Count)
			{
				if (LogFilter.logWarn)
				{
					Debug.LogWarning("ConnectionArray Remove invalid index " + connId);
				}
			}
			else
			{
				this.m_Connections[connId] = null;
			}
		}

		// Token: 0x06000064 RID: 100 RVA: 0x00004B30 File Offset: 0x00002D30
		public int AddLocal(NetworkConnection conn)
		{
			this.m_LocalConnections.Add(conn);
			int num = -this.m_LocalConnections.Count;
			conn.connectionId = num;
			return num;
		}

		// Token: 0x06000065 RID: 101 RVA: 0x00004B68 File Offset: 0x00002D68
		public bool ContainsPlayer(GameObject player, out NetworkConnection conn)
		{
			conn = null;
			bool result;
			if (player == null)
			{
				result = false;
			}
			else
			{
				for (int i = this.LocalIndex; i < this.m_Connections.Count; i++)
				{
					conn = this.Get(i);
					if (conn != null)
					{
						for (int j = 0; j < conn.playerControllers.Count; j++)
						{
							if (conn.playerControllers[j].IsValid && conn.playerControllers[j].gameObject == player)
							{
								return true;
							}
						}
					}
				}
				result = false;
			}
			return result;
		}

		// Token: 0x04000048 RID: 72
		private List<NetworkConnection> m_LocalConnections;

		// Token: 0x04000049 RID: 73
		private List<NetworkConnection> m_Connections;
	}
}
