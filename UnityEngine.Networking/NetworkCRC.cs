﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine.Networking.NetworkSystem;

namespace UnityEngine.Networking
{
	// Token: 0x0200003F RID: 63
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class NetworkCRC
	{
		// Token: 0x06000197 RID: 407 RVA: 0x0000AD14 File Offset: 0x00008F14
		public NetworkCRC()
		{
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x06000198 RID: 408 RVA: 0x0000AD28 File Offset: 0x00008F28
		internal static NetworkCRC singleton
		{
			get
			{
				if (NetworkCRC.s_Singleton == null)
				{
					NetworkCRC.s_Singleton = new NetworkCRC();
				}
				return NetworkCRC.s_Singleton;
			}
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x06000199 RID: 409 RVA: 0x0000AD58 File Offset: 0x00008F58
		public Dictionary<string, int> scripts
		{
			get
			{
				return this.m_Scripts;
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x0600019A RID: 410 RVA: 0x0000AD74 File Offset: 0x00008F74
		// (set) Token: 0x0600019B RID: 411 RVA: 0x0000AD93 File Offset: 0x00008F93
		public static bool scriptCRCCheck
		{
			get
			{
				return NetworkCRC.singleton.m_ScriptCRCCheck;
			}
			set
			{
				NetworkCRC.singleton.m_ScriptCRCCheck = value;
			}
		}

		// Token: 0x0600019C RID: 412 RVA: 0x0000ADA4 File Offset: 0x00008FA4
		public static void ReinitializeScriptCRCs(Assembly callingAssembly)
		{
			NetworkCRC.singleton.m_Scripts.Clear();
			foreach (Type type in callingAssembly.GetTypes())
			{
				if (type.GetBaseType() == typeof(NetworkBehaviour))
				{
					MethodInfo method = type.GetMethod(".cctor", BindingFlags.Static);
					if (method != null)
					{
						method.Invoke(null, new object[0]);
					}
				}
			}
		}

		// Token: 0x0600019D RID: 413 RVA: 0x0000AE1C File Offset: 0x0000901C
		public static void RegisterBehaviour(string name, int channel)
		{
			NetworkCRC.singleton.m_Scripts[name] = channel;
		}

		// Token: 0x0600019E RID: 414 RVA: 0x0000AE30 File Offset: 0x00009030
		internal static bool Validate(CRCMessageEntry[] scripts, int numChannels)
		{
			return NetworkCRC.singleton.ValidateInternal(scripts, numChannels);
		}

		// Token: 0x0600019F RID: 415 RVA: 0x0000AE54 File Offset: 0x00009054
		private bool ValidateInternal(CRCMessageEntry[] remoteScripts, int numChannels)
		{
			bool result;
			if (this.m_Scripts.Count != remoteScripts.Length)
			{
				if (LogFilter.logWarn)
				{
					Debug.LogWarning("Network configuration mismatch detected. The number of networked scripts on the client does not match the number of networked scripts on the server. This could be caused by lazy loading of scripts on the client. This warning can be disabled by the checkbox in NetworkManager Script CRC Check.");
				}
				this.Dump(remoteScripts);
				result = false;
			}
			else
			{
				foreach (CRCMessageEntry crcmessageEntry in remoteScripts)
				{
					if (LogFilter.logDebug)
					{
						Debug.Log(string.Concat(new object[]
						{
							"Script: ",
							crcmessageEntry.name,
							" Channel: ",
							crcmessageEntry.channel
						}));
					}
					if (this.m_Scripts.ContainsKey(crcmessageEntry.name))
					{
						int num = this.m_Scripts[crcmessageEntry.name];
						if (num != (int)crcmessageEntry.channel)
						{
							if (LogFilter.logError)
							{
								Debug.LogError(string.Concat(new object[]
								{
									"HLAPI CRC Channel Mismatch. Script: ",
									crcmessageEntry.name,
									" LocalChannel: ",
									num,
									" RemoteChannel: ",
									crcmessageEntry.channel
								}));
							}
							this.Dump(remoteScripts);
							return false;
						}
					}
					if ((int)crcmessageEntry.channel >= numChannels)
					{
						if (LogFilter.logError)
						{
							Debug.LogError(string.Concat(new object[]
							{
								"HLAPI CRC channel out of range! Script: ",
								crcmessageEntry.name,
								" Channel: ",
								crcmessageEntry.channel
							}));
						}
						this.Dump(remoteScripts);
						return false;
					}
				}
				result = true;
			}
			return result;
		}

		// Token: 0x060001A0 RID: 416 RVA: 0x0000B00C File Offset: 0x0000920C
		private void Dump(CRCMessageEntry[] remoteScripts)
		{
			foreach (string text in this.m_Scripts.Keys)
			{
				Debug.Log(string.Concat(new object[]
				{
					"CRC Local Dump ",
					text,
					" : ",
					this.m_Scripts[text]
				}));
			}
			foreach (CRCMessageEntry crcmessageEntry in remoteScripts)
			{
				Debug.Log(string.Concat(new object[]
				{
					"CRC Remote Dump ",
					crcmessageEntry.name,
					" : ",
					crcmessageEntry.channel
				}));
			}
		}

		// Token: 0x040000CF RID: 207
		internal static NetworkCRC s_Singleton;

		// Token: 0x040000D0 RID: 208
		private Dictionary<string, int> m_Scripts = new Dictionary<string, int>();

		// Token: 0x040000D1 RID: 209
		private bool m_ScriptCRCCheck;
	}
}
