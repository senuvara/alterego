﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x0200000D RID: 13
	[AttributeUsage(AttributeTargets.Method)]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class ServerAttribute : Attribute
	{
		// Token: 0x0600006C RID: 108 RVA: 0x00004C82 File Offset: 0x00002E82
		public ServerAttribute()
		{
		}
	}
}
