﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Networking.NetworkSystem;

namespace UnityEngine.Networking
{
	// Token: 0x02000004 RID: 4
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class ClientScene
	{
		// Token: 0x06000024 RID: 36 RVA: 0x00002C44 File Offset: 0x00000E44
		public ClientScene()
		{
		}

		// Token: 0x06000025 RID: 37 RVA: 0x00002C4C File Offset: 0x00000E4C
		private static bool hasMigrationPending()
		{
			return ClientScene.s_ReconnectId != -1;
		}

		// Token: 0x06000026 RID: 38 RVA: 0x00002C6C File Offset: 0x00000E6C
		public static void SetReconnectId(int newReconnectId, PeerInfoMessage[] peers)
		{
			ClientScene.s_ReconnectId = newReconnectId;
			ClientScene.s_Peers = peers;
			if (LogFilter.logDebug)
			{
				Debug.Log("ClientScene::SetReconnectId: " + newReconnectId);
			}
		}

		// Token: 0x06000027 RID: 39 RVA: 0x00002C9C File Offset: 0x00000E9C
		internal static void SetNotReady()
		{
			ClientScene.s_IsReady = false;
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000028 RID: 40 RVA: 0x00002CA8 File Offset: 0x00000EA8
		public static List<PlayerController> localPlayers
		{
			get
			{
				return ClientScene.s_LocalPlayers;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000029 RID: 41 RVA: 0x00002CC4 File Offset: 0x00000EC4
		public static bool ready
		{
			get
			{
				return ClientScene.s_IsReady;
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x0600002A RID: 42 RVA: 0x00002CE0 File Offset: 0x00000EE0
		public static NetworkConnection readyConnection
		{
			get
			{
				return ClientScene.s_ReadyConnection;
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x0600002B RID: 43 RVA: 0x00002CFC File Offset: 0x00000EFC
		public static int reconnectId
		{
			get
			{
				return ClientScene.s_ReconnectId;
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x0600002C RID: 44 RVA: 0x00002D18 File Offset: 0x00000F18
		public static Dictionary<NetworkInstanceId, NetworkIdentity> objects
		{
			get
			{
				return ClientScene.s_NetworkScene.localObjects;
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600002D RID: 45 RVA: 0x00002D38 File Offset: 0x00000F38
		public static Dictionary<NetworkHash128, GameObject> prefabs
		{
			get
			{
				return NetworkScene.guidToPrefab;
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x0600002E RID: 46 RVA: 0x00002D54 File Offset: 0x00000F54
		public static Dictionary<NetworkSceneId, NetworkIdentity> spawnableObjects
		{
			get
			{
				return ClientScene.s_SpawnableObjects;
			}
		}

		// Token: 0x0600002F RID: 47 RVA: 0x00002D70 File Offset: 0x00000F70
		internal static void Shutdown()
		{
			ClientScene.s_NetworkScene.Shutdown();
			ClientScene.s_LocalPlayers = new List<PlayerController>();
			ClientScene.s_PendingOwnerIds = new List<ClientScene.PendingOwner>();
			ClientScene.s_SpawnableObjects = null;
			ClientScene.s_ReadyConnection = null;
			ClientScene.s_IsReady = false;
			ClientScene.s_IsSpawnFinished = false;
			ClientScene.s_ReconnectId = -1;
			NetworkManager.activeTransport.Shutdown();
			NetworkManager.activeTransport.Init();
		}

		// Token: 0x06000030 RID: 48 RVA: 0x00002DD0 File Offset: 0x00000FD0
		internal static bool GetPlayerController(short playerControllerId, out PlayerController player)
		{
			player = null;
			bool result;
			if ((int)playerControllerId >= ClientScene.localPlayers.Count)
			{
				if (LogFilter.logWarn)
				{
					Debug.Log("ClientScene::GetPlayer: no local player found for: " + playerControllerId);
				}
				result = false;
			}
			else if (ClientScene.localPlayers[(int)playerControllerId] == null)
			{
				if (LogFilter.logWarn)
				{
					Debug.LogWarning("ClientScene::GetPlayer: local player is null for: " + playerControllerId);
				}
				result = false;
			}
			else
			{
				player = ClientScene.localPlayers[(int)playerControllerId];
				result = (player.gameObject != null);
			}
			return result;
		}

		// Token: 0x06000031 RID: 49 RVA: 0x00002E74 File Offset: 0x00001074
		internal static void InternalAddPlayer(NetworkIdentity view, short playerControllerId)
		{
			if (LogFilter.logDebug)
			{
				Debug.LogWarning("ClientScene::InternalAddPlayer: playerControllerId : " + playerControllerId);
			}
			if ((int)playerControllerId >= ClientScene.s_LocalPlayers.Count)
			{
				if (LogFilter.logWarn)
				{
					Debug.LogWarning("ClientScene::InternalAddPlayer: playerControllerId higher than expected: " + playerControllerId);
				}
				while ((int)playerControllerId >= ClientScene.s_LocalPlayers.Count)
				{
					ClientScene.s_LocalPlayers.Add(new PlayerController());
				}
			}
			PlayerController playerController = new PlayerController
			{
				gameObject = view.gameObject,
				playerControllerId = playerControllerId,
				unetView = view
			};
			ClientScene.s_LocalPlayers[(int)playerControllerId] = playerController;
			if (ClientScene.s_ReadyConnection == null)
			{
				if (LogFilter.logWarn)
				{
					Debug.LogWarning("No ready connection found for setting player controller during InternalAddPlayer");
				}
			}
			else
			{
				ClientScene.s_ReadyConnection.SetPlayerController(playerController);
			}
		}

		// Token: 0x06000032 RID: 50 RVA: 0x00002F60 File Offset: 0x00001160
		public static bool AddPlayer(short playerControllerId)
		{
			return ClientScene.AddPlayer(null, playerControllerId);
		}

		// Token: 0x06000033 RID: 51 RVA: 0x00002F7C File Offset: 0x0000117C
		public static bool AddPlayer(NetworkConnection readyConn, short playerControllerId)
		{
			return ClientScene.AddPlayer(readyConn, playerControllerId, null);
		}

		// Token: 0x06000034 RID: 52 RVA: 0x00002F9C File Offset: 0x0000119C
		public static bool AddPlayer(NetworkConnection readyConn, short playerControllerId, MessageBase extraMessage)
		{
			bool result;
			if (playerControllerId < 0)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("ClientScene::AddPlayer: playerControllerId of " + playerControllerId + " is negative");
				}
				result = false;
			}
			else if (playerControllerId > 32)
			{
				if (LogFilter.logError)
				{
					Debug.LogError(string.Concat(new object[]
					{
						"ClientScene::AddPlayer: playerControllerId of ",
						playerControllerId,
						" is too high, max is ",
						32
					}));
				}
				result = false;
			}
			else
			{
				if (playerControllerId > 16)
				{
					if (LogFilter.logWarn)
					{
						Debug.LogWarning("ClientScene::AddPlayer: playerControllerId of " + playerControllerId + " is unusually high");
					}
				}
				while ((int)playerControllerId >= ClientScene.s_LocalPlayers.Count)
				{
					ClientScene.s_LocalPlayers.Add(new PlayerController());
				}
				if (readyConn == null)
				{
					if (!ClientScene.s_IsReady)
					{
						if (LogFilter.logError)
						{
							Debug.LogError("Must call AddPlayer() with a connection the first time to become ready.");
						}
						return false;
					}
				}
				else
				{
					ClientScene.s_IsReady = true;
					ClientScene.s_ReadyConnection = readyConn;
				}
				PlayerController playerController;
				if (ClientScene.s_ReadyConnection.GetPlayerController(playerControllerId, out playerController))
				{
					if (playerController.IsValid && playerController.gameObject != null)
					{
						if (LogFilter.logError)
						{
							Debug.LogError("ClientScene::AddPlayer: playerControllerId of " + playerControllerId + " already in use.");
						}
						return false;
					}
				}
				if (LogFilter.logDebug)
				{
					Debug.Log(string.Concat(new object[]
					{
						"ClientScene::AddPlayer() for ID ",
						playerControllerId,
						" called with connection [",
						ClientScene.s_ReadyConnection,
						"]"
					}));
				}
				if (!ClientScene.hasMigrationPending())
				{
					AddPlayerMessage addPlayerMessage = new AddPlayerMessage();
					addPlayerMessage.playerControllerId = playerControllerId;
					if (extraMessage != null)
					{
						NetworkWriter networkWriter = new NetworkWriter();
						extraMessage.Serialize(networkWriter);
						addPlayerMessage.msgData = networkWriter.ToArray();
						addPlayerMessage.msgSize = (int)networkWriter.Position;
					}
					ClientScene.s_ReadyConnection.Send(37, addPlayerMessage);
					result = true;
				}
				else
				{
					result = ClientScene.SendReconnectMessage(extraMessage);
				}
			}
			return result;
		}

		// Token: 0x06000035 RID: 53 RVA: 0x000031D8 File Offset: 0x000013D8
		public static bool SendReconnectMessage(MessageBase extraMessage)
		{
			bool result;
			if (!ClientScene.hasMigrationPending())
			{
				result = false;
			}
			else
			{
				if (LogFilter.logDebug)
				{
					Debug.Log("ClientScene::AddPlayer reconnect " + ClientScene.s_ReconnectId);
				}
				if (ClientScene.s_Peers == null)
				{
					ClientScene.SetReconnectId(-1, null);
					if (LogFilter.logError)
					{
						Debug.LogError("ClientScene::AddPlayer: reconnecting, but no peers.");
					}
					result = false;
				}
				else
				{
					for (int i = 0; i < ClientScene.s_Peers.Length; i++)
					{
						PeerInfoMessage peerInfoMessage = ClientScene.s_Peers[i];
						if (peerInfoMessage.playerIds != null)
						{
							if (peerInfoMessage.connectionId == ClientScene.s_ReconnectId)
							{
								for (int j = 0; j < peerInfoMessage.playerIds.Length; j++)
								{
									ReconnectMessage reconnectMessage = new ReconnectMessage();
									reconnectMessage.oldConnectionId = ClientScene.s_ReconnectId;
									reconnectMessage.netId = peerInfoMessage.playerIds[j].netId;
									reconnectMessage.playerControllerId = peerInfoMessage.playerIds[j].playerControllerId;
									if (extraMessage != null)
									{
										NetworkWriter networkWriter = new NetworkWriter();
										extraMessage.Serialize(networkWriter);
										reconnectMessage.msgData = networkWriter.ToArray();
										reconnectMessage.msgSize = (int)networkWriter.Position;
									}
									ClientScene.s_ReadyConnection.Send(47, reconnectMessage);
								}
							}
						}
					}
					ClientScene.SetReconnectId(-1, null);
					result = true;
				}
			}
			return result;
		}

		// Token: 0x06000036 RID: 54 RVA: 0x00003340 File Offset: 0x00001540
		public static bool RemovePlayer(short playerControllerId)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log(string.Concat(new object[]
				{
					"ClientScene::RemovePlayer() for ID ",
					playerControllerId,
					" called with connection [",
					ClientScene.s_ReadyConnection,
					"]"
				}));
			}
			PlayerController playerController;
			bool result;
			if (ClientScene.s_ReadyConnection.GetPlayerController(playerControllerId, out playerController))
			{
				RemovePlayerMessage removePlayerMessage = new RemovePlayerMessage();
				removePlayerMessage.playerControllerId = playerControllerId;
				ClientScene.s_ReadyConnection.Send(38, removePlayerMessage);
				ClientScene.s_ReadyConnection.RemovePlayerController(playerControllerId);
				ClientScene.s_LocalPlayers[(int)playerControllerId] = new PlayerController();
				Object.Destroy(playerController.gameObject);
				result = true;
			}
			else
			{
				if (LogFilter.logError)
				{
					Debug.LogError("Failed to find player ID " + playerControllerId);
				}
				result = false;
			}
			return result;
		}

		// Token: 0x06000037 RID: 55 RVA: 0x00003418 File Offset: 0x00001618
		public static bool Ready(NetworkConnection conn)
		{
			bool result;
			if (ClientScene.s_IsReady)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("A connection has already been set as ready. There can only be one.");
				}
				result = false;
			}
			else
			{
				if (LogFilter.logDebug)
				{
					Debug.Log("ClientScene::Ready() called with connection [" + conn + "]");
				}
				if (conn != null)
				{
					ReadyMessage msg = new ReadyMessage();
					conn.Send(35, msg);
					ClientScene.s_IsReady = true;
					ClientScene.s_ReadyConnection = conn;
					ClientScene.s_ReadyConnection.isReady = true;
					result = true;
				}
				else
				{
					if (LogFilter.logError)
					{
						Debug.LogError("Ready() called with invalid connection object: conn=null");
					}
					result = false;
				}
			}
			return result;
		}

		// Token: 0x06000038 RID: 56 RVA: 0x000034C4 File Offset: 0x000016C4
		public static NetworkClient ConnectLocalServer()
		{
			LocalClient localClient = new LocalClient();
			NetworkServer.instance.ActivateLocalClientScene();
			localClient.InternalConnectLocalServer(true);
			return localClient;
		}

		// Token: 0x06000039 RID: 57 RVA: 0x000034F4 File Offset: 0x000016F4
		internal static NetworkClient ReconnectLocalServer()
		{
			LocalClient localClient = new LocalClient();
			NetworkServer.instance.ActivateLocalClientScene();
			localClient.InternalConnectLocalServer(false);
			return localClient;
		}

		// Token: 0x0600003A RID: 58 RVA: 0x00003521 File Offset: 0x00001721
		internal static void ClearLocalPlayers()
		{
			ClientScene.s_LocalPlayers.Clear();
		}

		// Token: 0x0600003B RID: 59 RVA: 0x0000352E File Offset: 0x0000172E
		internal static void HandleClientDisconnect(NetworkConnection conn)
		{
			if (ClientScene.s_ReadyConnection == conn && ClientScene.s_IsReady)
			{
				ClientScene.s_IsReady = false;
				ClientScene.s_ReadyConnection = null;
			}
		}

		// Token: 0x0600003C RID: 60 RVA: 0x00003554 File Offset: 0x00001754
		internal static void PrepareToSpawnSceneObjects()
		{
			ClientScene.s_SpawnableObjects = new Dictionary<NetworkSceneId, NetworkIdentity>();
			foreach (NetworkIdentity networkIdentity in Resources.FindObjectsOfTypeAll<NetworkIdentity>())
			{
				if (!networkIdentity.gameObject.activeSelf)
				{
					if (networkIdentity.gameObject.hideFlags != HideFlags.NotEditable && networkIdentity.gameObject.hideFlags != HideFlags.HideAndDontSave)
					{
						if (!networkIdentity.sceneId.IsEmpty())
						{
							ClientScene.s_SpawnableObjects[networkIdentity.sceneId] = networkIdentity;
							if (LogFilter.logDebug)
							{
								Debug.Log("ClientScene::PrepareSpawnObjects sceneId:" + networkIdentity.sceneId);
							}
						}
					}
				}
			}
		}

		// Token: 0x0600003D RID: 61 RVA: 0x0000361C File Offset: 0x0000181C
		internal static NetworkIdentity SpawnSceneObject(NetworkSceneId sceneId)
		{
			NetworkIdentity result;
			if (ClientScene.s_SpawnableObjects.ContainsKey(sceneId))
			{
				NetworkIdentity networkIdentity = ClientScene.s_SpawnableObjects[sceneId];
				ClientScene.s_SpawnableObjects.Remove(sceneId);
				result = networkIdentity;
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x0600003E RID: 62 RVA: 0x00003664 File Offset: 0x00001864
		internal static void RegisterSystemHandlers(NetworkClient client, bool localClient)
		{
			if (localClient)
			{
				short msgType = 1;
				if (ClientScene.<>f__mg$cache0 == null)
				{
					ClientScene.<>f__mg$cache0 = new NetworkMessageDelegate(ClientScene.OnLocalClientObjectDestroy);
				}
				client.RegisterHandlerSafe(msgType, ClientScene.<>f__mg$cache0);
				short msgType2 = 13;
				if (ClientScene.<>f__mg$cache1 == null)
				{
					ClientScene.<>f__mg$cache1 = new NetworkMessageDelegate(ClientScene.OnLocalClientObjectHide);
				}
				client.RegisterHandlerSafe(msgType2, ClientScene.<>f__mg$cache1);
				short msgType3 = 3;
				if (ClientScene.<>f__mg$cache2 == null)
				{
					ClientScene.<>f__mg$cache2 = new NetworkMessageDelegate(ClientScene.OnLocalClientObjectSpawn);
				}
				client.RegisterHandlerSafe(msgType3, ClientScene.<>f__mg$cache2);
				short msgType4 = 10;
				if (ClientScene.<>f__mg$cache3 == null)
				{
					ClientScene.<>f__mg$cache3 = new NetworkMessageDelegate(ClientScene.OnLocalClientObjectSpawnScene);
				}
				client.RegisterHandlerSafe(msgType4, ClientScene.<>f__mg$cache3);
				short msgType5 = 15;
				if (ClientScene.<>f__mg$cache4 == null)
				{
					ClientScene.<>f__mg$cache4 = new NetworkMessageDelegate(ClientScene.OnClientAuthority);
				}
				client.RegisterHandlerSafe(msgType5, ClientScene.<>f__mg$cache4);
			}
			else
			{
				short msgType6 = 3;
				if (ClientScene.<>f__mg$cache5 == null)
				{
					ClientScene.<>f__mg$cache5 = new NetworkMessageDelegate(ClientScene.OnObjectSpawn);
				}
				client.RegisterHandlerSafe(msgType6, ClientScene.<>f__mg$cache5);
				short msgType7 = 10;
				if (ClientScene.<>f__mg$cache6 == null)
				{
					ClientScene.<>f__mg$cache6 = new NetworkMessageDelegate(ClientScene.OnObjectSpawnScene);
				}
				client.RegisterHandlerSafe(msgType7, ClientScene.<>f__mg$cache6);
				short msgType8 = 12;
				if (ClientScene.<>f__mg$cache7 == null)
				{
					ClientScene.<>f__mg$cache7 = new NetworkMessageDelegate(ClientScene.OnObjectSpawnFinished);
				}
				client.RegisterHandlerSafe(msgType8, ClientScene.<>f__mg$cache7);
				short msgType9 = 1;
				if (ClientScene.<>f__mg$cache8 == null)
				{
					ClientScene.<>f__mg$cache8 = new NetworkMessageDelegate(ClientScene.OnObjectDestroy);
				}
				client.RegisterHandlerSafe(msgType9, ClientScene.<>f__mg$cache8);
				short msgType10 = 13;
				if (ClientScene.<>f__mg$cache9 == null)
				{
					ClientScene.<>f__mg$cache9 = new NetworkMessageDelegate(ClientScene.OnObjectDestroy);
				}
				client.RegisterHandlerSafe(msgType10, ClientScene.<>f__mg$cache9);
				short msgType11 = 8;
				if (ClientScene.<>f__mg$cacheA == null)
				{
					ClientScene.<>f__mg$cacheA = new NetworkMessageDelegate(ClientScene.OnUpdateVarsMessage);
				}
				client.RegisterHandlerSafe(msgType11, ClientScene.<>f__mg$cacheA);
				short msgType12 = 4;
				if (ClientScene.<>f__mg$cacheB == null)
				{
					ClientScene.<>f__mg$cacheB = new NetworkMessageDelegate(ClientScene.OnOwnerMessage);
				}
				client.RegisterHandlerSafe(msgType12, ClientScene.<>f__mg$cacheB);
				short msgType13 = 9;
				if (ClientScene.<>f__mg$cacheC == null)
				{
					ClientScene.<>f__mg$cacheC = new NetworkMessageDelegate(ClientScene.OnSyncListMessage);
				}
				client.RegisterHandlerSafe(msgType13, ClientScene.<>f__mg$cacheC);
				short msgType14 = 40;
				if (ClientScene.<>f__mg$cacheD == null)
				{
					ClientScene.<>f__mg$cacheD = new NetworkMessageDelegate(NetworkAnimator.OnAnimationClientMessage);
				}
				client.RegisterHandlerSafe(msgType14, ClientScene.<>f__mg$cacheD);
				short msgType15 = 41;
				if (ClientScene.<>f__mg$cacheE == null)
				{
					ClientScene.<>f__mg$cacheE = new NetworkMessageDelegate(NetworkAnimator.OnAnimationParametersClientMessage);
				}
				client.RegisterHandlerSafe(msgType15, ClientScene.<>f__mg$cacheE);
				short msgType16 = 15;
				if (ClientScene.<>f__mg$cacheF == null)
				{
					ClientScene.<>f__mg$cacheF = new NetworkMessageDelegate(ClientScene.OnClientAuthority);
				}
				client.RegisterHandlerSafe(msgType16, ClientScene.<>f__mg$cacheF);
			}
			short msgType17 = 2;
			if (ClientScene.<>f__mg$cache10 == null)
			{
				ClientScene.<>f__mg$cache10 = new NetworkMessageDelegate(ClientScene.OnRPCMessage);
			}
			client.RegisterHandlerSafe(msgType17, ClientScene.<>f__mg$cache10);
			short msgType18 = 7;
			if (ClientScene.<>f__mg$cache11 == null)
			{
				ClientScene.<>f__mg$cache11 = new NetworkMessageDelegate(ClientScene.OnSyncEventMessage);
			}
			client.RegisterHandlerSafe(msgType18, ClientScene.<>f__mg$cache11);
			short msgType19 = 42;
			if (ClientScene.<>f__mg$cache12 == null)
			{
				ClientScene.<>f__mg$cache12 = new NetworkMessageDelegate(NetworkAnimator.OnAnimationTriggerClientMessage);
			}
			client.RegisterHandlerSafe(msgType19, ClientScene.<>f__mg$cache12);
		}

		// Token: 0x0600003F RID: 63 RVA: 0x00003938 File Offset: 0x00001B38
		internal static string GetStringForAssetId(NetworkHash128 assetId)
		{
			GameObject gameObject;
			string result;
			SpawnDelegate func;
			if (NetworkScene.GetPrefab(assetId, out gameObject))
			{
				result = gameObject.name;
			}
			else if (NetworkScene.GetSpawnHandler(assetId, out func))
			{
				result = func.GetMethodName();
			}
			else
			{
				result = "unknown";
			}
			return result;
		}

		// Token: 0x06000040 RID: 64 RVA: 0x00003986 File Offset: 0x00001B86
		public static void RegisterPrefab(GameObject prefab, NetworkHash128 newAssetId)
		{
			NetworkScene.RegisterPrefab(prefab, newAssetId);
		}

		// Token: 0x06000041 RID: 65 RVA: 0x00003990 File Offset: 0x00001B90
		public static void RegisterPrefab(GameObject prefab)
		{
			NetworkScene.RegisterPrefab(prefab);
		}

		// Token: 0x06000042 RID: 66 RVA: 0x00003999 File Offset: 0x00001B99
		public static void RegisterPrefab(GameObject prefab, SpawnDelegate spawnHandler, UnSpawnDelegate unspawnHandler)
		{
			NetworkScene.RegisterPrefab(prefab, spawnHandler, unspawnHandler);
		}

		// Token: 0x06000043 RID: 67 RVA: 0x000039A4 File Offset: 0x00001BA4
		public static void UnregisterPrefab(GameObject prefab)
		{
			NetworkScene.UnregisterPrefab(prefab);
		}

		// Token: 0x06000044 RID: 68 RVA: 0x000039AD File Offset: 0x00001BAD
		public static void RegisterSpawnHandler(NetworkHash128 assetId, SpawnDelegate spawnHandler, UnSpawnDelegate unspawnHandler)
		{
			NetworkScene.RegisterSpawnHandler(assetId, spawnHandler, unspawnHandler);
		}

		// Token: 0x06000045 RID: 69 RVA: 0x000039B8 File Offset: 0x00001BB8
		public static void UnregisterSpawnHandler(NetworkHash128 assetId)
		{
			NetworkScene.UnregisterSpawnHandler(assetId);
		}

		// Token: 0x06000046 RID: 70 RVA: 0x000039C1 File Offset: 0x00001BC1
		public static void ClearSpawners()
		{
			NetworkScene.ClearSpawners();
		}

		// Token: 0x06000047 RID: 71 RVA: 0x000039C9 File Offset: 0x00001BC9
		public static void DestroyAllClientObjects()
		{
			ClientScene.s_NetworkScene.DestroyAllClientObjects();
		}

		// Token: 0x06000048 RID: 72 RVA: 0x000039D6 File Offset: 0x00001BD6
		public static void SetLocalObject(NetworkInstanceId netId, GameObject obj)
		{
			ClientScene.s_NetworkScene.SetLocalObject(netId, obj, ClientScene.s_IsSpawnFinished, false);
		}

		// Token: 0x06000049 RID: 73 RVA: 0x000039EC File Offset: 0x00001BEC
		public static GameObject FindLocalObject(NetworkInstanceId netId)
		{
			return ClientScene.s_NetworkScene.FindLocalObject(netId);
		}

		// Token: 0x0600004A RID: 74 RVA: 0x00003A0C File Offset: 0x00001C0C
		private static void ApplySpawnPayload(NetworkIdentity uv, Vector3 position, byte[] payload, NetworkInstanceId netId, GameObject newGameObject, NetworkMessage netMsg)
		{
			if (!uv.gameObject.activeSelf)
			{
				uv.gameObject.SetActive(true);
			}
			uv.transform.position = position;
			if (payload != null && payload.Length > 0)
			{
				NetworkReader reader = new NetworkReader(payload);
				uv.OnUpdateVars(reader, true, netMsg);
			}
			if (!(newGameObject == null))
			{
				newGameObject.SetActive(true);
				uv.SetNetworkInstanceId(netId);
				ClientScene.SetLocalObject(netId, newGameObject);
				if (ClientScene.s_IsSpawnFinished)
				{
					uv.OnStartClient();
					ClientScene.CheckForOwner(uv);
				}
			}
		}

		// Token: 0x0600004B RID: 75 RVA: 0x00003AA8 File Offset: 0x00001CA8
		private static void OnObjectSpawn(NetworkMessage netMsg)
		{
			netMsg.ReadMessage<ObjectSpawnMessage>(ClientScene.s_ObjectSpawnMessage);
			if (!ClientScene.s_ObjectSpawnMessage.assetId.IsValid())
			{
				if (LogFilter.logError)
				{
					Debug.LogError("OnObjSpawn netId: " + ClientScene.s_ObjectSpawnMessage.netId + " has invalid asset Id");
				}
			}
			else
			{
				if (LogFilter.logDebug)
				{
					Debug.Log(string.Concat(new object[]
					{
						"Client spawn handler instantiating [netId:",
						ClientScene.s_ObjectSpawnMessage.netId,
						" asset ID:",
						ClientScene.s_ObjectSpawnMessage.assetId,
						" pos:",
						ClientScene.s_ObjectSpawnMessage.position,
						"]"
					}));
				}
				NetworkIdentity component;
				GameObject original;
				SpawnDelegate spawnDelegate;
				if (ClientScene.s_NetworkScene.GetNetworkIdentity(ClientScene.s_ObjectSpawnMessage.netId, out component))
				{
					ClientScene.ApplySpawnPayload(component, ClientScene.s_ObjectSpawnMessage.position, ClientScene.s_ObjectSpawnMessage.payload, ClientScene.s_ObjectSpawnMessage.netId, null, netMsg);
				}
				else if (NetworkScene.GetPrefab(ClientScene.s_ObjectSpawnMessage.assetId, out original))
				{
					GameObject gameObject = Object.Instantiate<GameObject>(original, ClientScene.s_ObjectSpawnMessage.position, ClientScene.s_ObjectSpawnMessage.rotation);
					if (LogFilter.logDebug)
					{
						Debug.Log(string.Concat(new object[]
						{
							"Client spawn handler instantiating [netId:",
							ClientScene.s_ObjectSpawnMessage.netId,
							" asset ID:",
							ClientScene.s_ObjectSpawnMessage.assetId,
							" pos:",
							ClientScene.s_ObjectSpawnMessage.position,
							" rotation: ",
							ClientScene.s_ObjectSpawnMessage.rotation,
							"]"
						}));
					}
					component = gameObject.GetComponent<NetworkIdentity>();
					if (component == null)
					{
						if (LogFilter.logError)
						{
							Debug.LogError("Client object spawned for " + ClientScene.s_ObjectSpawnMessage.assetId + " does not have a NetworkIdentity");
						}
					}
					else
					{
						component.Reset();
						ClientScene.ApplySpawnPayload(component, ClientScene.s_ObjectSpawnMessage.position, ClientScene.s_ObjectSpawnMessage.payload, ClientScene.s_ObjectSpawnMessage.netId, gameObject, netMsg);
					}
				}
				else if (NetworkScene.GetSpawnHandler(ClientScene.s_ObjectSpawnMessage.assetId, out spawnDelegate))
				{
					GameObject gameObject2 = spawnDelegate(ClientScene.s_ObjectSpawnMessage.position, ClientScene.s_ObjectSpawnMessage.assetId);
					if (gameObject2 == null)
					{
						if (LogFilter.logWarn)
						{
							Debug.LogWarning("Client spawn handler for " + ClientScene.s_ObjectSpawnMessage.assetId + " returned null");
						}
					}
					else
					{
						component = gameObject2.GetComponent<NetworkIdentity>();
						if (component == null)
						{
							if (LogFilter.logError)
							{
								Debug.LogError("Client object spawned for " + ClientScene.s_ObjectSpawnMessage.assetId + " does not have a network identity");
							}
						}
						else
						{
							component.Reset();
							component.SetDynamicAssetId(ClientScene.s_ObjectSpawnMessage.assetId);
							ClientScene.ApplySpawnPayload(component, ClientScene.s_ObjectSpawnMessage.position, ClientScene.s_ObjectSpawnMessage.payload, ClientScene.s_ObjectSpawnMessage.netId, gameObject2, netMsg);
						}
					}
				}
				else if (LogFilter.logError)
				{
					Debug.LogError(string.Concat(new object[]
					{
						"Failed to spawn server object, did you forget to add it to the NetworkManager? assetId=",
						ClientScene.s_ObjectSpawnMessage.assetId,
						" netId=",
						ClientScene.s_ObjectSpawnMessage.netId
					}));
				}
			}
		}

		// Token: 0x0600004C RID: 76 RVA: 0x00003E50 File Offset: 0x00002050
		private static void OnObjectSpawnScene(NetworkMessage netMsg)
		{
			netMsg.ReadMessage<ObjectSpawnSceneMessage>(ClientScene.s_ObjectSpawnSceneMessage);
			if (LogFilter.logDebug)
			{
				Debug.Log(string.Concat(new object[]
				{
					"Client spawn scene handler instantiating [netId:",
					ClientScene.s_ObjectSpawnSceneMessage.netId,
					" sceneId:",
					ClientScene.s_ObjectSpawnSceneMessage.sceneId,
					" pos:",
					ClientScene.s_ObjectSpawnSceneMessage.position
				}));
			}
			NetworkIdentity networkIdentity;
			if (ClientScene.s_NetworkScene.GetNetworkIdentity(ClientScene.s_ObjectSpawnSceneMessage.netId, out networkIdentity))
			{
				ClientScene.ApplySpawnPayload(networkIdentity, ClientScene.s_ObjectSpawnSceneMessage.position, ClientScene.s_ObjectSpawnSceneMessage.payload, ClientScene.s_ObjectSpawnSceneMessage.netId, networkIdentity.gameObject, netMsg);
			}
			else
			{
				NetworkIdentity networkIdentity2 = ClientScene.SpawnSceneObject(ClientScene.s_ObjectSpawnSceneMessage.sceneId);
				if (networkIdentity2 == null)
				{
					if (LogFilter.logError)
					{
						Debug.LogError("Spawn scene object not found for " + ClientScene.s_ObjectSpawnSceneMessage.sceneId);
					}
				}
				else
				{
					if (LogFilter.logDebug)
					{
						Debug.Log(string.Concat(new object[]
						{
							"Client spawn for [netId:",
							ClientScene.s_ObjectSpawnSceneMessage.netId,
							"] [sceneId:",
							ClientScene.s_ObjectSpawnSceneMessage.sceneId,
							"] obj:",
							networkIdentity2.gameObject.name
						}));
					}
					ClientScene.ApplySpawnPayload(networkIdentity2, ClientScene.s_ObjectSpawnSceneMessage.position, ClientScene.s_ObjectSpawnSceneMessage.payload, ClientScene.s_ObjectSpawnSceneMessage.netId, networkIdentity2.gameObject, netMsg);
				}
			}
		}

		// Token: 0x0600004D RID: 77 RVA: 0x00003FFC File Offset: 0x000021FC
		private static void OnObjectSpawnFinished(NetworkMessage netMsg)
		{
			netMsg.ReadMessage<ObjectSpawnFinishedMessage>(ClientScene.s_ObjectSpawnFinishedMessage);
			if (LogFilter.logDebug)
			{
				Debug.Log("SpawnFinished:" + ClientScene.s_ObjectSpawnFinishedMessage.state);
			}
			if (ClientScene.s_ObjectSpawnFinishedMessage.state == 0U)
			{
				ClientScene.PrepareToSpawnSceneObjects();
				ClientScene.s_IsSpawnFinished = false;
			}
			else
			{
				foreach (NetworkIdentity networkIdentity in ClientScene.objects.Values)
				{
					if (!networkIdentity.isClient)
					{
						networkIdentity.OnStartClient();
						ClientScene.CheckForOwner(networkIdentity);
					}
				}
				ClientScene.s_IsSpawnFinished = true;
			}
		}

		// Token: 0x0600004E RID: 78 RVA: 0x000040D0 File Offset: 0x000022D0
		private static void OnObjectDestroy(NetworkMessage netMsg)
		{
			netMsg.ReadMessage<ObjectDestroyMessage>(ClientScene.s_ObjectDestroyMessage);
			if (LogFilter.logDebug)
			{
				Debug.Log("ClientScene::OnObjDestroy netId:" + ClientScene.s_ObjectDestroyMessage.netId);
			}
			NetworkIdentity networkIdentity;
			if (ClientScene.s_NetworkScene.GetNetworkIdentity(ClientScene.s_ObjectDestroyMessage.netId, out networkIdentity))
			{
				networkIdentity.OnNetworkDestroy();
				if (!NetworkScene.InvokeUnSpawnHandler(networkIdentity.assetId, networkIdentity.gameObject))
				{
					if (networkIdentity.sceneId.IsEmpty())
					{
						Object.Destroy(networkIdentity.gameObject);
					}
					else
					{
						networkIdentity.gameObject.SetActive(false);
						ClientScene.s_SpawnableObjects[networkIdentity.sceneId] = networkIdentity;
					}
				}
				ClientScene.s_NetworkScene.RemoveLocalObject(ClientScene.s_ObjectDestroyMessage.netId);
				networkIdentity.MarkForReset();
			}
			else if (LogFilter.logDebug)
			{
				Debug.LogWarning("Did not find target for destroy message for " + ClientScene.s_ObjectDestroyMessage.netId);
			}
		}

		// Token: 0x0600004F RID: 79 RVA: 0x000041E0 File Offset: 0x000023E0
		private static void OnLocalClientObjectDestroy(NetworkMessage netMsg)
		{
			netMsg.ReadMessage<ObjectDestroyMessage>(ClientScene.s_ObjectDestroyMessage);
			if (LogFilter.logDebug)
			{
				Debug.Log("ClientScene::OnLocalObjectObjDestroy netId:" + ClientScene.s_ObjectDestroyMessage.netId);
			}
			ClientScene.s_NetworkScene.RemoveLocalObject(ClientScene.s_ObjectDestroyMessage.netId);
		}

		// Token: 0x06000050 RID: 80 RVA: 0x00004238 File Offset: 0x00002438
		private static void OnLocalClientObjectHide(NetworkMessage netMsg)
		{
			netMsg.ReadMessage<ObjectDestroyMessage>(ClientScene.s_ObjectDestroyMessage);
			if (LogFilter.logDebug)
			{
				Debug.Log("ClientScene::OnLocalObjectObjHide netId:" + ClientScene.s_ObjectDestroyMessage.netId);
			}
			NetworkIdentity networkIdentity;
			if (ClientScene.s_NetworkScene.GetNetworkIdentity(ClientScene.s_ObjectDestroyMessage.netId, out networkIdentity))
			{
				networkIdentity.OnSetLocalVisibility(false);
			}
		}

		// Token: 0x06000051 RID: 81 RVA: 0x000042A0 File Offset: 0x000024A0
		private static void OnLocalClientObjectSpawn(NetworkMessage netMsg)
		{
			netMsg.ReadMessage<ObjectSpawnMessage>(ClientScene.s_ObjectSpawnMessage);
			NetworkIdentity networkIdentity;
			if (ClientScene.s_NetworkScene.GetNetworkIdentity(ClientScene.s_ObjectSpawnMessage.netId, out networkIdentity))
			{
				networkIdentity.OnSetLocalVisibility(true);
			}
		}

		// Token: 0x06000052 RID: 82 RVA: 0x000042E0 File Offset: 0x000024E0
		private static void OnLocalClientObjectSpawnScene(NetworkMessage netMsg)
		{
			netMsg.ReadMessage<ObjectSpawnSceneMessage>(ClientScene.s_ObjectSpawnSceneMessage);
			NetworkIdentity networkIdentity;
			if (ClientScene.s_NetworkScene.GetNetworkIdentity(ClientScene.s_ObjectSpawnSceneMessage.netId, out networkIdentity))
			{
				networkIdentity.OnSetLocalVisibility(true);
			}
		}

		// Token: 0x06000053 RID: 83 RVA: 0x00004320 File Offset: 0x00002520
		private static void OnUpdateVarsMessage(NetworkMessage netMsg)
		{
			NetworkInstanceId networkInstanceId = netMsg.reader.ReadNetworkId();
			if (LogFilter.logDev)
			{
				Debug.Log(string.Concat(new object[]
				{
					"ClientScene::OnUpdateVarsMessage ",
					networkInstanceId,
					" channel:",
					netMsg.channelId
				}));
			}
			NetworkIdentity networkIdentity;
			if (ClientScene.s_NetworkScene.GetNetworkIdentity(networkInstanceId, out networkIdentity))
			{
				networkIdentity.OnUpdateVars(netMsg.reader, false, netMsg);
			}
			else if (LogFilter.logWarn)
			{
				Debug.LogWarning("Did not find target for sync message for " + networkInstanceId);
			}
		}

		// Token: 0x06000054 RID: 84 RVA: 0x000043C8 File Offset: 0x000025C8
		private static void OnRPCMessage(NetworkMessage netMsg)
		{
			int num = (int)netMsg.reader.ReadPackedUInt32();
			NetworkInstanceId networkInstanceId = netMsg.reader.ReadNetworkId();
			if (LogFilter.logDebug)
			{
				Debug.Log(string.Concat(new object[]
				{
					"ClientScene::OnRPCMessage hash:",
					num,
					" netId:",
					networkInstanceId
				}));
			}
			NetworkIdentity networkIdentity;
			if (ClientScene.s_NetworkScene.GetNetworkIdentity(networkInstanceId, out networkIdentity))
			{
				networkIdentity.HandleRPC(num, netMsg.reader);
			}
			else if (LogFilter.logWarn)
			{
				string cmdHashHandlerName = NetworkBehaviour.GetCmdHashHandlerName(num);
				Debug.LogWarningFormat("Could not find target object with netId:{0} for RPC call {1}", new object[]
				{
					networkInstanceId,
					cmdHashHandlerName
				});
			}
		}

		// Token: 0x06000055 RID: 85 RVA: 0x00004484 File Offset: 0x00002684
		private static void OnSyncEventMessage(NetworkMessage netMsg)
		{
			int cmdHash = (int)netMsg.reader.ReadPackedUInt32();
			NetworkInstanceId networkInstanceId = netMsg.reader.ReadNetworkId();
			if (LogFilter.logDebug)
			{
				Debug.Log("ClientScene::OnSyncEventMessage " + networkInstanceId);
			}
			NetworkIdentity networkIdentity;
			if (ClientScene.s_NetworkScene.GetNetworkIdentity(networkInstanceId, out networkIdentity))
			{
				networkIdentity.HandleSyncEvent(cmdHash, netMsg.reader);
			}
			else if (LogFilter.logWarn)
			{
				Debug.LogWarning("Did not find target for SyncEvent message for " + networkInstanceId);
			}
		}

		// Token: 0x06000056 RID: 86 RVA: 0x00004514 File Offset: 0x00002714
		private static void OnSyncListMessage(NetworkMessage netMsg)
		{
			NetworkInstanceId networkInstanceId = netMsg.reader.ReadNetworkId();
			int cmdHash = (int)netMsg.reader.ReadPackedUInt32();
			if (LogFilter.logDebug)
			{
				Debug.Log("ClientScene::OnSyncListMessage " + networkInstanceId);
			}
			NetworkIdentity networkIdentity;
			if (ClientScene.s_NetworkScene.GetNetworkIdentity(networkInstanceId, out networkIdentity))
			{
				networkIdentity.HandleSyncList(cmdHash, netMsg.reader);
			}
			else if (LogFilter.logWarn)
			{
				Debug.LogWarning("Did not find target for SyncList message for " + networkInstanceId);
			}
		}

		// Token: 0x06000057 RID: 87 RVA: 0x000045A4 File Offset: 0x000027A4
		private static void OnClientAuthority(NetworkMessage netMsg)
		{
			netMsg.ReadMessage<ClientAuthorityMessage>(ClientScene.s_ClientAuthorityMessage);
			if (LogFilter.logDebug)
			{
				Debug.Log(string.Concat(new object[]
				{
					"ClientScene::OnClientAuthority for  connectionId=",
					netMsg.conn.connectionId,
					" netId: ",
					ClientScene.s_ClientAuthorityMessage.netId
				}));
			}
			NetworkIdentity networkIdentity;
			if (ClientScene.s_NetworkScene.GetNetworkIdentity(ClientScene.s_ClientAuthorityMessage.netId, out networkIdentity))
			{
				networkIdentity.HandleClientAuthority(ClientScene.s_ClientAuthorityMessage.authority);
			}
		}

		// Token: 0x06000058 RID: 88 RVA: 0x0000463C File Offset: 0x0000283C
		private static void OnOwnerMessage(NetworkMessage netMsg)
		{
			netMsg.ReadMessage<OwnerMessage>(ClientScene.s_OwnerMessage);
			if (LogFilter.logDebug)
			{
				Debug.Log(string.Concat(new object[]
				{
					"ClientScene::OnOwnerMessage - connectionId=",
					netMsg.conn.connectionId,
					" netId: ",
					ClientScene.s_OwnerMessage.netId
				}));
			}
			PlayerController playerController;
			if (netMsg.conn.GetPlayerController(ClientScene.s_OwnerMessage.playerControllerId, out playerController))
			{
				playerController.unetView.SetNotLocalPlayer();
			}
			NetworkIdentity networkIdentity;
			if (ClientScene.s_NetworkScene.GetNetworkIdentity(ClientScene.s_OwnerMessage.netId, out networkIdentity))
			{
				networkIdentity.SetConnectionToServer(netMsg.conn);
				networkIdentity.SetLocalPlayer(ClientScene.s_OwnerMessage.playerControllerId);
				ClientScene.InternalAddPlayer(networkIdentity, ClientScene.s_OwnerMessage.playerControllerId);
			}
			else
			{
				ClientScene.PendingOwner item = new ClientScene.PendingOwner
				{
					netId = ClientScene.s_OwnerMessage.netId,
					playerControllerId = ClientScene.s_OwnerMessage.playerControllerId
				};
				ClientScene.s_PendingOwnerIds.Add(item);
			}
		}

		// Token: 0x06000059 RID: 89 RVA: 0x00004758 File Offset: 0x00002958
		private static void CheckForOwner(NetworkIdentity uv)
		{
			int i = 0;
			while (i < ClientScene.s_PendingOwnerIds.Count)
			{
				ClientScene.PendingOwner pendingOwner = ClientScene.s_PendingOwnerIds[i];
				if (pendingOwner.netId == uv.netId)
				{
					uv.SetConnectionToServer(ClientScene.s_ReadyConnection);
					uv.SetLocalPlayer(pendingOwner.playerControllerId);
					if (LogFilter.logDev)
					{
						Debug.Log("ClientScene::OnOwnerMessage - player=" + uv.gameObject.name);
					}
					if (ClientScene.s_ReadyConnection.connectionId < 0)
					{
						if (LogFilter.logError)
						{
							Debug.LogError("Owner message received on a local client.");
						}
						break;
					}
					ClientScene.InternalAddPlayer(uv, pendingOwner.playerControllerId);
					ClientScene.s_PendingOwnerIds.RemoveAt(i);
					break;
				}
				else
				{
					i++;
				}
			}
		}

		// Token: 0x0600005A RID: 90 RVA: 0x00004830 File Offset: 0x00002A30
		// Note: this type is marked as 'beforefieldinit'.
		static ClientScene()
		{
		}

		// Token: 0x04000022 RID: 34
		private static List<PlayerController> s_LocalPlayers = new List<PlayerController>();

		// Token: 0x04000023 RID: 35
		private static NetworkConnection s_ReadyConnection;

		// Token: 0x04000024 RID: 36
		private static Dictionary<NetworkSceneId, NetworkIdentity> s_SpawnableObjects;

		// Token: 0x04000025 RID: 37
		private static bool s_IsReady;

		// Token: 0x04000026 RID: 38
		private static bool s_IsSpawnFinished;

		// Token: 0x04000027 RID: 39
		private static NetworkScene s_NetworkScene = new NetworkScene();

		// Token: 0x04000028 RID: 40
		private static ObjectSpawnSceneMessage s_ObjectSpawnSceneMessage = new ObjectSpawnSceneMessage();

		// Token: 0x04000029 RID: 41
		private static ObjectSpawnFinishedMessage s_ObjectSpawnFinishedMessage = new ObjectSpawnFinishedMessage();

		// Token: 0x0400002A RID: 42
		private static ObjectDestroyMessage s_ObjectDestroyMessage = new ObjectDestroyMessage();

		// Token: 0x0400002B RID: 43
		private static ObjectSpawnMessage s_ObjectSpawnMessage = new ObjectSpawnMessage();

		// Token: 0x0400002C RID: 44
		private static OwnerMessage s_OwnerMessage = new OwnerMessage();

		// Token: 0x0400002D RID: 45
		private static ClientAuthorityMessage s_ClientAuthorityMessage = new ClientAuthorityMessage();

		// Token: 0x0400002E RID: 46
		public const int ReconnectIdInvalid = -1;

		// Token: 0x0400002F RID: 47
		public const int ReconnectIdHost = 0;

		// Token: 0x04000030 RID: 48
		private static int s_ReconnectId = -1;

		// Token: 0x04000031 RID: 49
		private static PeerInfoMessage[] s_Peers;

		// Token: 0x04000032 RID: 50
		private static List<ClientScene.PendingOwner> s_PendingOwnerIds = new List<ClientScene.PendingOwner>();

		// Token: 0x04000033 RID: 51
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cache0;

		// Token: 0x04000034 RID: 52
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cache1;

		// Token: 0x04000035 RID: 53
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cache2;

		// Token: 0x04000036 RID: 54
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cache3;

		// Token: 0x04000037 RID: 55
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cache4;

		// Token: 0x04000038 RID: 56
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cache5;

		// Token: 0x04000039 RID: 57
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cache6;

		// Token: 0x0400003A RID: 58
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cache7;

		// Token: 0x0400003B RID: 59
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cache8;

		// Token: 0x0400003C RID: 60
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cache9;

		// Token: 0x0400003D RID: 61
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cacheA;

		// Token: 0x0400003E RID: 62
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cacheB;

		// Token: 0x0400003F RID: 63
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cacheC;

		// Token: 0x04000040 RID: 64
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cacheD;

		// Token: 0x04000041 RID: 65
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cacheE;

		// Token: 0x04000042 RID: 66
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cacheF;

		// Token: 0x04000043 RID: 67
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cache10;

		// Token: 0x04000044 RID: 68
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cache11;

		// Token: 0x04000045 RID: 69
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cache12;

		// Token: 0x02000005 RID: 5
		private struct PendingOwner
		{
			// Token: 0x04000046 RID: 70
			public NetworkInstanceId netId;

			// Token: 0x04000047 RID: 71
			public short playerControllerId;
		}
	}
}
