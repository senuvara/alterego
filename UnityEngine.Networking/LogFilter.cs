﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000018 RID: 24
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class LogFilter
	{
		// Token: 0x060000C5 RID: 197 RVA: 0x000080F6 File Offset: 0x000062F6
		public LogFilter()
		{
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x060000C6 RID: 198 RVA: 0x00008100 File Offset: 0x00006300
		// (set) Token: 0x060000C7 RID: 199 RVA: 0x0000811A File Offset: 0x0000631A
		public static int currentLogLevel
		{
			get
			{
				return LogFilter.s_CurrentLogLevel;
			}
			set
			{
				LogFilter.s_CurrentLogLevel = value;
			}
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x060000C8 RID: 200 RVA: 0x00008124 File Offset: 0x00006324
		internal static bool logDev
		{
			get
			{
				return LogFilter.s_CurrentLogLevel <= 0;
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x060000C9 RID: 201 RVA: 0x00008144 File Offset: 0x00006344
		public static bool logDebug
		{
			get
			{
				return LogFilter.s_CurrentLogLevel <= 1;
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x060000CA RID: 202 RVA: 0x00008164 File Offset: 0x00006364
		public static bool logInfo
		{
			get
			{
				return LogFilter.s_CurrentLogLevel <= 2;
			}
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x060000CB RID: 203 RVA: 0x00008184 File Offset: 0x00006384
		public static bool logWarn
		{
			get
			{
				return LogFilter.s_CurrentLogLevel <= 3;
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x060000CC RID: 204 RVA: 0x000081A4 File Offset: 0x000063A4
		public static bool logError
		{
			get
			{
				return LogFilter.s_CurrentLogLevel <= 4;
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x060000CD RID: 205 RVA: 0x000081C4 File Offset: 0x000063C4
		public static bool logFatal
		{
			get
			{
				return LogFilter.s_CurrentLogLevel <= 5;
			}
		}

		// Token: 0x060000CE RID: 206 RVA: 0x000081E4 File Offset: 0x000063E4
		// Note: this type is marked as 'beforefieldinit'.
		static LogFilter()
		{
		}

		// Token: 0x0400005C RID: 92
		internal const int Developer = 0;

		// Token: 0x0400005D RID: 93
		internal const int SetInScripting = -1;

		// Token: 0x0400005E RID: 94
		public const int Debug = 1;

		// Token: 0x0400005F RID: 95
		public const int Info = 2;

		// Token: 0x04000060 RID: 96
		public const int Warn = 3;

		// Token: 0x04000061 RID: 97
		public const int Error = 4;

		// Token: 0x04000062 RID: 98
		public const int Fatal = 5;

		// Token: 0x04000063 RID: 99
		[Obsolete("Use LogFilter.currentLogLevel instead")]
		public static LogFilter.FilterLevel current = LogFilter.FilterLevel.Info;

		// Token: 0x04000064 RID: 100
		private static int s_CurrentLogLevel = 2;

		// Token: 0x02000019 RID: 25
		public enum FilterLevel
		{
			// Token: 0x04000066 RID: 102
			Developer,
			// Token: 0x04000067 RID: 103
			Debug,
			// Token: 0x04000068 RID: 104
			Info,
			// Token: 0x04000069 RID: 105
			Warn,
			// Token: 0x0400006A RID: 106
			Error,
			// Token: 0x0400006B RID: 107
			Fatal,
			// Token: 0x0400006C RID: 108
			SetInScripting = -1
		}
	}
}
