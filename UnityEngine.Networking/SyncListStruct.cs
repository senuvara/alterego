﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x0200006D RID: 109
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class SyncListStruct<T> : SyncList<T> where T : struct
	{
		// Token: 0x0600057F RID: 1407 RVA: 0x0001ECE1 File Offset: 0x0001CEE1
		public SyncListStruct()
		{
		}

		// Token: 0x06000580 RID: 1408 RVA: 0x0001ECE9 File Offset: 0x0001CEE9
		public new void AddInternal(T item)
		{
			base.AddInternal(item);
		}

		// Token: 0x06000581 RID: 1409 RVA: 0x0001ECF3 File Offset: 0x0001CEF3
		protected override void SerializeItem(NetworkWriter writer, T item)
		{
		}

		// Token: 0x06000582 RID: 1410 RVA: 0x0001ECF8 File Offset: 0x0001CEF8
		protected override T DeserializeItem(NetworkReader reader)
		{
			return Activator.CreateInstance<T>();
		}

		// Token: 0x06000583 RID: 1411 RVA: 0x0001ED14 File Offset: 0x0001CF14
		public T GetItem(int i)
		{
			return base[i];
		}

		// Token: 0x170000E0 RID: 224
		// (get) Token: 0x06000584 RID: 1412 RVA: 0x0001ED30 File Offset: 0x0001CF30
		public new ushort Count
		{
			get
			{
				return (ushort)base.Count;
			}
		}
	}
}
