﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Networking
{
	// Token: 0x02000055 RID: 85
	[AddComponentMenu("Network/NetworkProximityChecker")]
	[RequireComponent(typeof(NetworkIdentity))]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class NetworkProximityChecker : NetworkBehaviour
	{
		// Token: 0x060003A8 RID: 936 RVA: 0x00013F60 File Offset: 0x00012160
		public NetworkProximityChecker()
		{
		}

		// Token: 0x060003A9 RID: 937 RVA: 0x00013F8C File Offset: 0x0001218C
		private void Update()
		{
			if (NetworkServer.active)
			{
				if (Time.time - this.m_VisUpdateTime > this.visUpdateInterval)
				{
					base.GetComponent<NetworkIdentity>().RebuildObservers(false);
					this.m_VisUpdateTime = Time.time;
				}
			}
		}

		// Token: 0x060003AA RID: 938 RVA: 0x00013FDC File Offset: 0x000121DC
		public override bool OnCheckObserver(NetworkConnection newObserver)
		{
			bool result;
			if (this.forceHidden)
			{
				result = false;
			}
			else
			{
				GameObject gameObject = null;
				for (int i = 0; i < newObserver.playerControllers.Count; i++)
				{
					PlayerController playerController = newObserver.playerControllers[i];
					if (playerController != null && playerController.gameObject != null)
					{
						gameObject = playerController.gameObject;
						break;
					}
				}
				if (gameObject == null)
				{
					result = false;
				}
				else
				{
					Vector3 position = gameObject.transform.position;
					result = ((position - base.transform.position).magnitude < (float)this.visRange);
				}
			}
			return result;
		}

		// Token: 0x060003AB RID: 939 RVA: 0x00014098 File Offset: 0x00012298
		public override bool OnRebuildObservers(HashSet<NetworkConnection> observers, bool initial)
		{
			bool result;
			if (this.forceHidden)
			{
				NetworkIdentity component = base.GetComponent<NetworkIdentity>();
				if (component.connectionToClient != null)
				{
					observers.Add(component.connectionToClient);
				}
				result = true;
			}
			else
			{
				NetworkProximityChecker.CheckMethod checkMethod = this.checkMethod;
				if (checkMethod != NetworkProximityChecker.CheckMethod.Physics3D)
				{
					if (checkMethod != NetworkProximityChecker.CheckMethod.Physics2D)
					{
						result = false;
					}
					else
					{
						foreach (Collider2D collider2D in Physics2D.OverlapCircleAll(base.transform.position, (float)this.visRange))
						{
							NetworkIdentity component2 = collider2D.GetComponent<NetworkIdentity>();
							if (component2 != null && component2.connectionToClient != null)
							{
								observers.Add(component2.connectionToClient);
							}
						}
						result = true;
					}
				}
				else
				{
					foreach (Collider collider in Physics.OverlapSphere(base.transform.position, (float)this.visRange))
					{
						NetworkIdentity component3 = collider.GetComponent<NetworkIdentity>();
						if (component3 != null && component3.connectionToClient != null)
						{
							observers.Add(component3.connectionToClient);
						}
					}
					result = true;
				}
			}
			return result;
		}

		// Token: 0x060003AC RID: 940 RVA: 0x000141E7 File Offset: 0x000123E7
		public override void OnSetLocalVisibility(bool vis)
		{
			NetworkProximityChecker.SetVis(base.gameObject, vis);
		}

		// Token: 0x060003AD RID: 941 RVA: 0x000141F8 File Offset: 0x000123F8
		private static void SetVis(GameObject go, bool vis)
		{
			foreach (Renderer renderer in go.GetComponents<Renderer>())
			{
				renderer.enabled = vis;
			}
			for (int j = 0; j < go.transform.childCount; j++)
			{
				Transform child = go.transform.GetChild(j);
				NetworkProximityChecker.SetVis(child.gameObject, vis);
			}
		}

		// Token: 0x040001A7 RID: 423
		[Tooltip("The maximum range that objects will be visible at.")]
		public int visRange = 10;

		// Token: 0x040001A8 RID: 424
		[Tooltip("How often (in seconds) that this object should update the set of players that can see it.")]
		public float visUpdateInterval = 1f;

		// Token: 0x040001A9 RID: 425
		[Tooltip("Which method to use for checking proximity of players.\n\nPhysics3D uses 3D physics to determine proximity.\n\nPhysics2D uses 2D physics to determine proximity.")]
		public NetworkProximityChecker.CheckMethod checkMethod = NetworkProximityChecker.CheckMethod.Physics3D;

		// Token: 0x040001AA RID: 426
		[Tooltip("Enable to force this object to be hidden from players.")]
		public bool forceHidden = false;

		// Token: 0x040001AB RID: 427
		private float m_VisUpdateTime;

		// Token: 0x02000056 RID: 86
		public enum CheckMethod
		{
			// Token: 0x040001AD RID: 429
			Physics3D,
			// Token: 0x040001AE RID: 430
			Physics2D
		}
	}
}
