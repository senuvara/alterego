﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000007 RID: 7
	[AttributeUsage(AttributeTargets.Class)]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class NetworkSettingsAttribute : Attribute
	{
		// Token: 0x06000066 RID: 102 RVA: 0x00004C24 File Offset: 0x00002E24
		public NetworkSettingsAttribute()
		{
		}

		// Token: 0x0400004A RID: 74
		public int channel = 0;

		// Token: 0x0400004B RID: 75
		public float sendInterval = 0.1f;
	}
}
