﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking
{
	// Token: 0x0200005C RID: 92
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class NetworkServerSimple
	{
		// Token: 0x0600047D RID: 1149 RVA: 0x00018E60 File Offset: 0x00017060
		public NetworkServerSimple()
		{
			this.m_ConnectionsReadOnly = new ReadOnlyCollection<NetworkConnection>(this.m_Connections);
		}

		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x0600047E RID: 1150 RVA: 0x00018ED0 File Offset: 0x000170D0
		// (set) Token: 0x0600047F RID: 1151 RVA: 0x00018EEB File Offset: 0x000170EB
		public int listenPort
		{
			get
			{
				return this.m_ListenPort;
			}
			set
			{
				this.m_ListenPort = value;
			}
		}

		// Token: 0x170000B4 RID: 180
		// (get) Token: 0x06000480 RID: 1152 RVA: 0x00018EF8 File Offset: 0x000170F8
		// (set) Token: 0x06000481 RID: 1153 RVA: 0x00018F13 File Offset: 0x00017113
		public int serverHostId
		{
			get
			{
				return this.m_ServerHostId;
			}
			set
			{
				this.m_ServerHostId = value;
			}
		}

		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x06000482 RID: 1154 RVA: 0x00018F20 File Offset: 0x00017120
		public HostTopology hostTopology
		{
			get
			{
				return this.m_HostTopology;
			}
		}

		// Token: 0x170000B6 RID: 182
		// (get) Token: 0x06000483 RID: 1155 RVA: 0x00018F3C File Offset: 0x0001713C
		// (set) Token: 0x06000484 RID: 1156 RVA: 0x00018F57 File Offset: 0x00017157
		public bool useWebSockets
		{
			get
			{
				return this.m_UseWebSockets;
			}
			set
			{
				this.m_UseWebSockets = value;
			}
		}

		// Token: 0x170000B7 RID: 183
		// (get) Token: 0x06000485 RID: 1157 RVA: 0x00018F64 File Offset: 0x00017164
		public ReadOnlyCollection<NetworkConnection> connections
		{
			get
			{
				return this.m_ConnectionsReadOnly;
			}
		}

		// Token: 0x170000B8 RID: 184
		// (get) Token: 0x06000486 RID: 1158 RVA: 0x00018F80 File Offset: 0x00017180
		public Dictionary<short, NetworkMessageDelegate> handlers
		{
			get
			{
				return this.m_MessageHandlers.GetHandlers();
			}
		}

		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x06000487 RID: 1159 RVA: 0x00018FA0 File Offset: 0x000171A0
		public byte[] messageBuffer
		{
			get
			{
				return this.m_MsgBuffer;
			}
		}

		// Token: 0x170000BA RID: 186
		// (get) Token: 0x06000488 RID: 1160 RVA: 0x00018FBC File Offset: 0x000171BC
		public NetworkReader messageReader
		{
			get
			{
				return this.m_MsgReader;
			}
		}

		// Token: 0x170000BB RID: 187
		// (get) Token: 0x06000489 RID: 1161 RVA: 0x00018FD8 File Offset: 0x000171D8
		public Type networkConnectionClass
		{
			get
			{
				return this.m_NetworkConnectionClass;
			}
		}

		// Token: 0x0600048A RID: 1162 RVA: 0x00018FF3 File Offset: 0x000171F3
		public void SetNetworkConnectionClass<T>() where T : NetworkConnection
		{
			this.m_NetworkConnectionClass = typeof(T);
		}

		// Token: 0x0600048B RID: 1163 RVA: 0x00019008 File Offset: 0x00017208
		public virtual void Initialize()
		{
			if (!this.m_Initialized)
			{
				this.m_Initialized = true;
				NetworkManager.activeTransport.Init();
				this.m_MsgBuffer = new byte[65535];
				this.m_MsgReader = new NetworkReader(this.m_MsgBuffer);
				if (this.m_HostTopology == null)
				{
					ConnectionConfig connectionConfig = new ConnectionConfig();
					connectionConfig.AddChannel(QosType.ReliableSequenced);
					connectionConfig.AddChannel(QosType.Unreliable);
					this.m_HostTopology = new HostTopology(connectionConfig, 8);
				}
				if (LogFilter.logDebug)
				{
					Debug.Log("NetworkServerSimple initialize.");
				}
			}
		}

		// Token: 0x0600048C RID: 1164 RVA: 0x000190A0 File Offset: 0x000172A0
		public bool Configure(ConnectionConfig config, int maxConnections)
		{
			HostTopology topology = new HostTopology(config, maxConnections);
			return this.Configure(topology);
		}

		// Token: 0x0600048D RID: 1165 RVA: 0x000190C4 File Offset: 0x000172C4
		public bool Configure(HostTopology topology)
		{
			this.m_HostTopology = topology;
			return true;
		}

		// Token: 0x0600048E RID: 1166 RVA: 0x000190E4 File Offset: 0x000172E4
		public bool Listen(string ipAddress, int serverListenPort)
		{
			this.Initialize();
			this.m_ListenPort = serverListenPort;
			if (this.m_UseWebSockets)
			{
				this.m_ServerHostId = NetworkManager.activeTransport.AddWebsocketHost(this.m_HostTopology, serverListenPort, ipAddress);
			}
			else
			{
				this.m_ServerHostId = NetworkManager.activeTransport.AddHost(this.m_HostTopology, serverListenPort, ipAddress);
			}
			bool result;
			if (this.m_ServerHostId == -1)
			{
				result = false;
			}
			else
			{
				if (LogFilter.logDebug)
				{
					Debug.Log(string.Concat(new object[]
					{
						"NetworkServerSimple listen: ",
						ipAddress,
						":",
						this.m_ListenPort
					}));
				}
				result = true;
			}
			return result;
		}

		// Token: 0x0600048F RID: 1167 RVA: 0x000191A0 File Offset: 0x000173A0
		public bool Listen(int serverListenPort)
		{
			return this.Listen(serverListenPort, this.m_HostTopology);
		}

		// Token: 0x06000490 RID: 1168 RVA: 0x000191C4 File Offset: 0x000173C4
		public bool Listen(int serverListenPort, HostTopology topology)
		{
			this.m_HostTopology = topology;
			this.Initialize();
			this.m_ListenPort = serverListenPort;
			if (this.m_UseWebSockets)
			{
				this.m_ServerHostId = NetworkManager.activeTransport.AddWebsocketHost(this.m_HostTopology, serverListenPort, null);
			}
			else
			{
				this.m_ServerHostId = NetworkManager.activeTransport.AddHost(this.m_HostTopology, serverListenPort, null);
			}
			bool result;
			if (this.m_ServerHostId == -1)
			{
				result = false;
			}
			else
			{
				if (LogFilter.logDebug)
				{
					Debug.Log("NetworkServerSimple listen " + this.m_ListenPort);
				}
				result = true;
			}
			return result;
		}

		// Token: 0x06000491 RID: 1169 RVA: 0x0001926C File Offset: 0x0001746C
		public void ListenRelay(string relayIp, int relayPort, NetworkID netGuid, SourceID sourceId, NodeID nodeId)
		{
			this.Initialize();
			this.m_ServerHostId = NetworkManager.activeTransport.AddHost(this.m_HostTopology, this.listenPort, null);
			if (LogFilter.logDebug)
			{
				Debug.Log("Server Host Slot Id: " + this.m_ServerHostId);
			}
			this.Update();
			byte b;
			NetworkManager.activeTransport.ConnectAsNetworkHost(this.m_ServerHostId, relayIp, relayPort, netGuid, sourceId, nodeId, out b);
			this.m_RelaySlotId = 0;
			if (LogFilter.logDebug)
			{
				Debug.Log("Relay Slot Id: " + this.m_RelaySlotId);
			}
		}

		// Token: 0x06000492 RID: 1170 RVA: 0x0001930F File Offset: 0x0001750F
		public void Stop()
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkServerSimple stop ");
			}
			NetworkManager.activeTransport.RemoveHost(this.m_ServerHostId);
			this.m_ServerHostId = -1;
		}

		// Token: 0x06000493 RID: 1171 RVA: 0x00019340 File Offset: 0x00017540
		internal void RegisterHandlerSafe(short msgType, NetworkMessageDelegate handler)
		{
			this.m_MessageHandlers.RegisterHandlerSafe(msgType, handler);
		}

		// Token: 0x06000494 RID: 1172 RVA: 0x00019350 File Offset: 0x00017550
		public void RegisterHandler(short msgType, NetworkMessageDelegate handler)
		{
			this.m_MessageHandlers.RegisterHandler(msgType, handler);
		}

		// Token: 0x06000495 RID: 1173 RVA: 0x00019360 File Offset: 0x00017560
		public void UnregisterHandler(short msgType)
		{
			this.m_MessageHandlers.UnregisterHandler(msgType);
		}

		// Token: 0x06000496 RID: 1174 RVA: 0x0001936F File Offset: 0x0001756F
		public void ClearHandlers()
		{
			this.m_MessageHandlers.ClearMessageHandlers();
		}

		// Token: 0x06000497 RID: 1175 RVA: 0x00019380 File Offset: 0x00017580
		public void UpdateConnections()
		{
			for (int i = 0; i < this.m_Connections.Count; i++)
			{
				NetworkConnection networkConnection = this.m_Connections[i];
				if (networkConnection != null)
				{
					networkConnection.FlushChannels();
				}
			}
		}

		// Token: 0x06000498 RID: 1176 RVA: 0x000193C8 File Offset: 0x000175C8
		public void Update()
		{
			if (this.m_ServerHostId != -1)
			{
				NetworkEventType networkEventType;
				if (this.m_RelaySlotId != -1)
				{
					byte b;
					networkEventType = NetworkManager.activeTransport.ReceiveRelayEventFromHost(this.m_ServerHostId, out b);
					if (networkEventType != NetworkEventType.Nothing)
					{
						if (LogFilter.logDebug)
						{
							Debug.Log("NetGroup event:" + networkEventType);
						}
					}
					if (networkEventType == NetworkEventType.ConnectEvent)
					{
						if (LogFilter.logDebug)
						{
							Debug.Log("NetGroup server connected");
						}
					}
					if (networkEventType == NetworkEventType.DisconnectEvent)
					{
						if (LogFilter.logDebug)
						{
							Debug.Log("NetGroup server disconnected");
						}
					}
				}
				do
				{
					byte b;
					int connectionId;
					int channelId;
					int receivedSize;
					networkEventType = NetworkManager.activeTransport.ReceiveFromHost(this.m_ServerHostId, out connectionId, out channelId, this.m_MsgBuffer, this.m_MsgBuffer.Length, out receivedSize, out b);
					if (networkEventType != NetworkEventType.Nothing)
					{
						if (LogFilter.logDev)
						{
							Debug.Log(string.Concat(new object[]
							{
								"Server event: host=",
								this.m_ServerHostId,
								" event=",
								networkEventType,
								" error=",
								b
							}));
						}
					}
					switch (networkEventType)
					{
					case NetworkEventType.DataEvent:
						this.HandleData(connectionId, channelId, receivedSize, b);
						break;
					case NetworkEventType.ConnectEvent:
						this.HandleConnect(connectionId, b);
						break;
					case NetworkEventType.DisconnectEvent:
						this.HandleDisconnect(connectionId, b);
						break;
					case NetworkEventType.Nothing:
						break;
					default:
						if (LogFilter.logError)
						{
							Debug.LogError("Unknown network message type received: " + networkEventType);
						}
						break;
					}
				}
				while (networkEventType != NetworkEventType.Nothing);
				this.UpdateConnections();
			}
		}

		// Token: 0x06000499 RID: 1177 RVA: 0x00019584 File Offset: 0x00017784
		public NetworkConnection FindConnection(int connectionId)
		{
			NetworkConnection result;
			if (connectionId < 0 || connectionId >= this.m_Connections.Count)
			{
				result = null;
			}
			else
			{
				result = this.m_Connections[connectionId];
			}
			return result;
		}

		// Token: 0x0600049A RID: 1178 RVA: 0x000195C4 File Offset: 0x000177C4
		public bool SetConnectionAtIndex(NetworkConnection conn)
		{
			while (this.m_Connections.Count <= conn.connectionId)
			{
				this.m_Connections.Add(null);
			}
			bool result;
			if (this.m_Connections[conn.connectionId] != null)
			{
				result = false;
			}
			else
			{
				this.m_Connections[conn.connectionId] = conn;
				conn.SetHandlers(this.m_MessageHandlers);
				result = true;
			}
			return result;
		}

		// Token: 0x0600049B RID: 1179 RVA: 0x00019640 File Offset: 0x00017840
		public bool RemoveConnectionAtIndex(int connectionId)
		{
			bool result;
			if (connectionId < 0 || connectionId >= this.m_Connections.Count)
			{
				result = false;
			}
			else
			{
				this.m_Connections[connectionId] = null;
				result = true;
			}
			return result;
		}

		// Token: 0x0600049C RID: 1180 RVA: 0x00019684 File Offset: 0x00017884
		private void HandleConnect(int connectionId, byte error)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkServerSimple accepted client:" + connectionId);
			}
			if (error != 0)
			{
				this.OnConnectError(connectionId, error);
			}
			else
			{
				string networkAddress;
				int num;
				NetworkID networkID;
				NodeID nodeID;
				byte lastError;
				NetworkManager.activeTransport.GetConnectionInfo(this.m_ServerHostId, connectionId, out networkAddress, out num, out networkID, out nodeID, out lastError);
				NetworkConnection networkConnection = (NetworkConnection)Activator.CreateInstance(this.m_NetworkConnectionClass);
				networkConnection.SetHandlers(this.m_MessageHandlers);
				networkConnection.Initialize(networkAddress, this.m_ServerHostId, connectionId, this.m_HostTopology);
				networkConnection.lastError = (NetworkError)lastError;
				while (this.m_Connections.Count <= connectionId)
				{
					this.m_Connections.Add(null);
				}
				this.m_Connections[connectionId] = networkConnection;
				this.OnConnected(networkConnection);
			}
		}

		// Token: 0x0600049D RID: 1181 RVA: 0x0001975C File Offset: 0x0001795C
		private void HandleDisconnect(int connectionId, byte error)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkServerSimple disconnect client:" + connectionId);
			}
			NetworkConnection networkConnection = this.FindConnection(connectionId);
			if (networkConnection != null)
			{
				networkConnection.lastError = (NetworkError)error;
				if (error != 0)
				{
					if (error != 6)
					{
						this.m_Connections[connectionId] = null;
						if (LogFilter.logError)
						{
							Debug.LogError(string.Concat(new object[]
							{
								"Server client disconnect error, connectionId: ",
								connectionId,
								" error: ",
								(NetworkError)error
							}));
						}
						this.OnDisconnectError(networkConnection, error);
						return;
					}
				}
				networkConnection.Disconnect();
				this.m_Connections[connectionId] = null;
				if (LogFilter.logDebug)
				{
					Debug.Log("Server lost client:" + connectionId);
				}
				this.OnDisconnected(networkConnection);
			}
		}

		// Token: 0x0600049E RID: 1182 RVA: 0x0001984C File Offset: 0x00017A4C
		private void HandleData(int connectionId, int channelId, int receivedSize, byte error)
		{
			NetworkConnection networkConnection = this.FindConnection(connectionId);
			if (networkConnection == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("HandleData Unknown connectionId:" + connectionId);
				}
			}
			else
			{
				networkConnection.lastError = (NetworkError)error;
				if (error != 0)
				{
					this.OnDataError(networkConnection, error);
				}
				else
				{
					this.m_MsgReader.SeekZero();
					this.OnData(networkConnection, receivedSize, channelId);
				}
			}
		}

		// Token: 0x0600049F RID: 1183 RVA: 0x000198C4 File Offset: 0x00017AC4
		public void SendBytesTo(int connectionId, byte[] bytes, int numBytes, int channelId)
		{
			NetworkConnection networkConnection = this.FindConnection(connectionId);
			if (networkConnection != null)
			{
				networkConnection.SendBytes(bytes, numBytes, channelId);
			}
		}

		// Token: 0x060004A0 RID: 1184 RVA: 0x000198F4 File Offset: 0x00017AF4
		public void SendWriterTo(int connectionId, NetworkWriter writer, int channelId)
		{
			NetworkConnection networkConnection = this.FindConnection(connectionId);
			if (networkConnection != null)
			{
				networkConnection.SendWriter(writer, channelId);
			}
		}

		// Token: 0x060004A1 RID: 1185 RVA: 0x00019920 File Offset: 0x00017B20
		public void Disconnect(int connectionId)
		{
			NetworkConnection networkConnection = this.FindConnection(connectionId);
			if (networkConnection != null)
			{
				networkConnection.Disconnect();
				this.m_Connections[connectionId] = null;
			}
		}

		// Token: 0x060004A2 RID: 1186 RVA: 0x00019958 File Offset: 0x00017B58
		public void DisconnectAllConnections()
		{
			for (int i = 0; i < this.m_Connections.Count; i++)
			{
				NetworkConnection networkConnection = this.m_Connections[i];
				if (networkConnection != null)
				{
					networkConnection.Disconnect();
					networkConnection.Dispose();
				}
			}
		}

		// Token: 0x060004A3 RID: 1187 RVA: 0x000199A5 File Offset: 0x00017BA5
		public virtual void OnConnectError(int connectionId, byte error)
		{
			Debug.LogError("OnConnectError error:" + error);
		}

		// Token: 0x060004A4 RID: 1188 RVA: 0x000199BD File Offset: 0x00017BBD
		public virtual void OnDataError(NetworkConnection conn, byte error)
		{
			Debug.LogError("OnDataError error:" + error);
		}

		// Token: 0x060004A5 RID: 1189 RVA: 0x000199D5 File Offset: 0x00017BD5
		public virtual void OnDisconnectError(NetworkConnection conn, byte error)
		{
			Debug.LogError("OnDisconnectError error:" + error);
		}

		// Token: 0x060004A6 RID: 1190 RVA: 0x000199ED File Offset: 0x00017BED
		public virtual void OnConnected(NetworkConnection conn)
		{
			conn.InvokeHandlerNoData(32);
		}

		// Token: 0x060004A7 RID: 1191 RVA: 0x000199F9 File Offset: 0x00017BF9
		public virtual void OnDisconnected(NetworkConnection conn)
		{
			conn.InvokeHandlerNoData(33);
		}

		// Token: 0x060004A8 RID: 1192 RVA: 0x00019A05 File Offset: 0x00017C05
		public virtual void OnData(NetworkConnection conn, int receivedSize, int channelId)
		{
			conn.TransportReceive(this.m_MsgBuffer, receivedSize, channelId);
		}

		// Token: 0x040001D3 RID: 467
		private bool m_Initialized = false;

		// Token: 0x040001D4 RID: 468
		private int m_ListenPort;

		// Token: 0x040001D5 RID: 469
		private int m_ServerHostId = -1;

		// Token: 0x040001D6 RID: 470
		private int m_RelaySlotId = -1;

		// Token: 0x040001D7 RID: 471
		private bool m_UseWebSockets;

		// Token: 0x040001D8 RID: 472
		private byte[] m_MsgBuffer = null;

		// Token: 0x040001D9 RID: 473
		private NetworkReader m_MsgReader = null;

		// Token: 0x040001DA RID: 474
		private Type m_NetworkConnectionClass = typeof(NetworkConnection);

		// Token: 0x040001DB RID: 475
		private HostTopology m_HostTopology;

		// Token: 0x040001DC RID: 476
		private List<NetworkConnection> m_Connections = new List<NetworkConnection>();

		// Token: 0x040001DD RID: 477
		private ReadOnlyCollection<NetworkConnection> m_ConnectionsReadOnly;

		// Token: 0x040001DE RID: 478
		private NetworkMessageHandlers m_MessageHandlers = new NetworkMessageHandlers();
	}
}
