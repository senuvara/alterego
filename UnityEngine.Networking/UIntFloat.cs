﻿using System;
using System.Runtime.InteropServices;

namespace UnityEngine.Networking
{
	// Token: 0x0200003C RID: 60
	[StructLayout(LayoutKind.Explicit)]
	internal struct UIntFloat
	{
		// Token: 0x040000C8 RID: 200
		[FieldOffset(0)]
		public float floatValue;

		// Token: 0x040000C9 RID: 201
		[FieldOffset(0)]
		public uint intValue;

		// Token: 0x040000CA RID: 202
		[FieldOffset(0)]
		public double doubleValue;

		// Token: 0x040000CB RID: 203
		[FieldOffset(0)]
		public ulong longValue;
	}
}
