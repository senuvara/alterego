﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.SceneManagement;

namespace UnityEngine.Networking
{
	// Token: 0x0200004A RID: 74
	[AddComponentMenu("Network/NetworkLobbyManager")]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class NetworkLobbyManager : NetworkManager
	{
		// Token: 0x0600028C RID: 652 RVA: 0x0001030C File Offset: 0x0000E50C
		public NetworkLobbyManager()
		{
		}

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x0600028D RID: 653 RVA: 0x0001034C File Offset: 0x0000E54C
		// (set) Token: 0x0600028E RID: 654 RVA: 0x00010367 File Offset: 0x0000E567
		public bool showLobbyGUI
		{
			get
			{
				return this.m_ShowLobbyGUI;
			}
			set
			{
				this.m_ShowLobbyGUI = value;
			}
		}

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x0600028F RID: 655 RVA: 0x00010374 File Offset: 0x0000E574
		// (set) Token: 0x06000290 RID: 656 RVA: 0x0001038F File Offset: 0x0000E58F
		public int maxPlayers
		{
			get
			{
				return this.m_MaxPlayers;
			}
			set
			{
				this.m_MaxPlayers = value;
			}
		}

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x06000291 RID: 657 RVA: 0x0001039C File Offset: 0x0000E59C
		// (set) Token: 0x06000292 RID: 658 RVA: 0x000103B7 File Offset: 0x0000E5B7
		public int maxPlayersPerConnection
		{
			get
			{
				return this.m_MaxPlayersPerConnection;
			}
			set
			{
				this.m_MaxPlayersPerConnection = value;
			}
		}

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x06000293 RID: 659 RVA: 0x000103C4 File Offset: 0x0000E5C4
		// (set) Token: 0x06000294 RID: 660 RVA: 0x000103DF File Offset: 0x0000E5DF
		public int minPlayers
		{
			get
			{
				return this.m_MinPlayers;
			}
			set
			{
				this.m_MinPlayers = value;
			}
		}

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x06000295 RID: 661 RVA: 0x000103EC File Offset: 0x0000E5EC
		// (set) Token: 0x06000296 RID: 662 RVA: 0x00010407 File Offset: 0x0000E607
		public NetworkLobbyPlayer lobbyPlayerPrefab
		{
			get
			{
				return this.m_LobbyPlayerPrefab;
			}
			set
			{
				this.m_LobbyPlayerPrefab = value;
			}
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x06000297 RID: 663 RVA: 0x00010414 File Offset: 0x0000E614
		// (set) Token: 0x06000298 RID: 664 RVA: 0x0001042F File Offset: 0x0000E62F
		public GameObject gamePlayerPrefab
		{
			get
			{
				return this.m_GamePlayerPrefab;
			}
			set
			{
				this.m_GamePlayerPrefab = value;
			}
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x06000299 RID: 665 RVA: 0x0001043C File Offset: 0x0000E63C
		// (set) Token: 0x0600029A RID: 666 RVA: 0x00010457 File Offset: 0x0000E657
		public string lobbyScene
		{
			get
			{
				return this.m_LobbyScene;
			}
			set
			{
				this.m_LobbyScene = value;
				base.offlineScene = value;
			}
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x0600029B RID: 667 RVA: 0x00010468 File Offset: 0x0000E668
		// (set) Token: 0x0600029C RID: 668 RVA: 0x00010483 File Offset: 0x0000E683
		public string playScene
		{
			get
			{
				return this.m_PlayScene;
			}
			set
			{
				this.m_PlayScene = value;
			}
		}

		// Token: 0x0600029D RID: 669 RVA: 0x00010490 File Offset: 0x0000E690
		private void OnValidate()
		{
			if (this.m_MaxPlayers <= 0)
			{
				this.m_MaxPlayers = 1;
			}
			if (this.m_MaxPlayersPerConnection <= 0)
			{
				this.m_MaxPlayersPerConnection = 1;
			}
			if (this.m_MaxPlayersPerConnection > this.maxPlayers)
			{
				this.m_MaxPlayersPerConnection = this.maxPlayers;
			}
			if (this.m_MinPlayers < 0)
			{
				this.m_MinPlayers = 0;
			}
			if (this.m_MinPlayers > this.m_MaxPlayers)
			{
				this.m_MinPlayers = this.m_MaxPlayers;
			}
			if (this.m_LobbyPlayerPrefab != null)
			{
				NetworkIdentity component = this.m_LobbyPlayerPrefab.GetComponent<NetworkIdentity>();
				if (component == null)
				{
					this.m_LobbyPlayerPrefab = null;
					Debug.LogWarning("LobbyPlayer prefab must have a NetworkIdentity component.");
				}
			}
			if (this.m_GamePlayerPrefab != null)
			{
				NetworkIdentity component2 = this.m_GamePlayerPrefab.GetComponent<NetworkIdentity>();
				if (component2 == null)
				{
					this.m_GamePlayerPrefab = null;
					Debug.LogWarning("GamePlayer prefab must have a NetworkIdentity component.");
				}
			}
		}

		// Token: 0x0600029E RID: 670 RVA: 0x00010598 File Offset: 0x0000E798
		private byte FindSlot()
		{
			byte b = 0;
			while ((int)b < this.maxPlayers)
			{
				if (this.lobbySlots[(int)b] == null)
				{
					return b;
				}
				b += 1;
			}
			return byte.MaxValue;
		}

		// Token: 0x0600029F RID: 671 RVA: 0x000105E8 File Offset: 0x0000E7E8
		private void SceneLoadedForPlayer(NetworkConnection conn, GameObject lobbyPlayerGameObject)
		{
			NetworkLobbyPlayer component = lobbyPlayerGameObject.GetComponent<NetworkLobbyPlayer>();
			if (!(component == null))
			{
				string name = SceneManager.GetSceneAt(0).name;
				if (LogFilter.logDebug)
				{
					Debug.Log(string.Concat(new object[]
					{
						"NetworkLobby SceneLoadedForPlayer scene:",
						name,
						" ",
						conn
					}));
				}
				if (name == this.m_LobbyScene)
				{
					NetworkLobbyManager.PendingPlayer item;
					item.conn = conn;
					item.lobbyPlayer = lobbyPlayerGameObject;
					this.m_PendingPlayers.Add(item);
				}
				else
				{
					short playerControllerId = lobbyPlayerGameObject.GetComponent<NetworkIdentity>().playerControllerId;
					GameObject gameObject = this.OnLobbyServerCreateGamePlayer(conn, playerControllerId);
					if (gameObject == null)
					{
						Transform startPosition = base.GetStartPosition();
						if (startPosition != null)
						{
							gameObject = Object.Instantiate<GameObject>(this.gamePlayerPrefab, startPosition.position, startPosition.rotation);
						}
						else
						{
							gameObject = Object.Instantiate<GameObject>(this.gamePlayerPrefab, Vector3.zero, Quaternion.identity);
						}
					}
					if (this.OnLobbyServerSceneLoadedForPlayer(lobbyPlayerGameObject, gameObject))
					{
						NetworkServer.ReplacePlayerForConnection(conn, gameObject, playerControllerId);
					}
				}
			}
		}

		// Token: 0x060002A0 RID: 672 RVA: 0x0001071C File Offset: 0x0000E91C
		private static int CheckConnectionIsReadyToBegin(NetworkConnection conn)
		{
			int num = 0;
			for (int i = 0; i < conn.playerControllers.Count; i++)
			{
				PlayerController playerController = conn.playerControllers[i];
				if (playerController.IsValid)
				{
					NetworkLobbyPlayer component = playerController.gameObject.GetComponent<NetworkLobbyPlayer>();
					if (component.readyToBegin)
					{
						num++;
					}
				}
			}
			return num;
		}

		// Token: 0x060002A1 RID: 673 RVA: 0x0001078C File Offset: 0x0000E98C
		public void CheckReadyToBegin()
		{
			string name = SceneManager.GetSceneAt(0).name;
			if (!(name != this.m_LobbyScene))
			{
				int num = 0;
				int num2 = 0;
				for (int i = 0; i < NetworkServer.connections.Count; i++)
				{
					NetworkConnection networkConnection = NetworkServer.connections[i];
					if (networkConnection != null)
					{
						num2++;
						num += NetworkLobbyManager.CheckConnectionIsReadyToBegin(networkConnection);
					}
				}
				if (this.m_MinPlayers <= 0 || num >= this.m_MinPlayers)
				{
					if (num >= num2)
					{
						this.m_PendingPlayers.Clear();
						this.OnLobbyServerPlayersReady();
					}
				}
			}
		}

		// Token: 0x060002A2 RID: 674 RVA: 0x00010849 File Offset: 0x0000EA49
		public void ServerReturnToLobby()
		{
			if (!NetworkServer.active)
			{
				Debug.Log("ServerReturnToLobby called on client");
			}
			else
			{
				this.ServerChangeScene(this.m_LobbyScene);
			}
		}

		// Token: 0x060002A3 RID: 675 RVA: 0x00010874 File Offset: 0x0000EA74
		private void CallOnClientEnterLobby()
		{
			this.OnLobbyClientEnter();
			for (int i = 0; i < this.lobbySlots.Length; i++)
			{
				NetworkLobbyPlayer networkLobbyPlayer = this.lobbySlots[i];
				if (!(networkLobbyPlayer == null))
				{
					networkLobbyPlayer.readyToBegin = false;
					networkLobbyPlayer.OnClientEnterLobby();
				}
			}
		}

		// Token: 0x060002A4 RID: 676 RVA: 0x000108CC File Offset: 0x0000EACC
		private void CallOnClientExitLobby()
		{
			this.OnLobbyClientExit();
			for (int i = 0; i < this.lobbySlots.Length; i++)
			{
				NetworkLobbyPlayer networkLobbyPlayer = this.lobbySlots[i];
				if (!(networkLobbyPlayer == null))
				{
					networkLobbyPlayer.OnClientExitLobby();
				}
			}
		}

		// Token: 0x060002A5 RID: 677 RVA: 0x0001091C File Offset: 0x0000EB1C
		public bool SendReturnToLobby()
		{
			bool result;
			if (this.client == null || !this.client.isConnected)
			{
				result = false;
			}
			else
			{
				EmptyMessage msg = new EmptyMessage();
				this.client.Send(46, msg);
				result = true;
			}
			return result;
		}

		// Token: 0x060002A6 RID: 678 RVA: 0x0001096C File Offset: 0x0000EB6C
		public override void OnServerConnect(NetworkConnection conn)
		{
			if (base.numPlayers > this.maxPlayers)
			{
				if (LogFilter.logWarn)
				{
					Debug.LogWarning("NetworkLobbyManager can't accept new connection [" + conn + "], too many players connected.");
				}
				conn.Disconnect();
			}
			else
			{
				string name = SceneManager.GetSceneAt(0).name;
				if (name != this.m_LobbyScene)
				{
					if (LogFilter.logWarn)
					{
						Debug.LogWarning("NetworkLobbyManager can't accept new connection [" + conn + "], not in lobby and game already in progress.");
					}
					conn.Disconnect();
				}
				else
				{
					base.OnServerConnect(conn);
					for (int i = 0; i < this.lobbySlots.Length; i++)
					{
						if (this.lobbySlots[i])
						{
							this.lobbySlots[i].SetDirtyBit(1U);
						}
					}
					this.OnLobbyServerConnect(conn);
				}
			}
		}

		// Token: 0x060002A7 RID: 679 RVA: 0x00010A50 File Offset: 0x0000EC50
		public override void OnServerDisconnect(NetworkConnection conn)
		{
			base.OnServerDisconnect(conn);
			for (int i = 0; i < this.lobbySlots.Length; i++)
			{
				NetworkLobbyPlayer networkLobbyPlayer = this.lobbySlots[i];
				if (!(networkLobbyPlayer == null))
				{
					if (networkLobbyPlayer.connectionToClient == conn)
					{
						this.lobbySlots[i] = null;
						NetworkServer.Destroy(networkLobbyPlayer.gameObject);
					}
				}
			}
			this.OnLobbyServerDisconnect(conn);
		}

		// Token: 0x060002A8 RID: 680 RVA: 0x00010AC4 File Offset: 0x0000ECC4
		public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
		{
			string name = SceneManager.GetSceneAt(0).name;
			if (!(name != this.m_LobbyScene))
			{
				int num = 0;
				for (int i = 0; i < conn.playerControllers.Count; i++)
				{
					if (conn.playerControllers[i].IsValid)
					{
						num++;
					}
				}
				if (num >= this.maxPlayersPerConnection)
				{
					if (LogFilter.logWarn)
					{
						Debug.LogWarning("NetworkLobbyManager no more players for this connection.");
					}
					EmptyMessage msg = new EmptyMessage();
					conn.Send(45, msg);
				}
				else
				{
					byte b = this.FindSlot();
					if (b == 255)
					{
						if (LogFilter.logWarn)
						{
							Debug.LogWarning("NetworkLobbyManager no space for more players");
						}
						EmptyMessage msg2 = new EmptyMessage();
						conn.Send(45, msg2);
					}
					else
					{
						GameObject gameObject = this.OnLobbyServerCreateLobbyPlayer(conn, playerControllerId);
						if (gameObject == null)
						{
							gameObject = Object.Instantiate<GameObject>(this.lobbyPlayerPrefab.gameObject, Vector3.zero, Quaternion.identity);
						}
						NetworkLobbyPlayer component = gameObject.GetComponent<NetworkLobbyPlayer>();
						component.slot = b;
						this.lobbySlots[(int)b] = component;
						NetworkServer.AddPlayerForConnection(conn, gameObject, playerControllerId);
					}
				}
			}
		}

		// Token: 0x060002A9 RID: 681 RVA: 0x00010C0C File Offset: 0x0000EE0C
		public override void OnServerRemovePlayer(NetworkConnection conn, PlayerController player)
		{
			short playerControllerId = player.playerControllerId;
			byte slot = player.gameObject.GetComponent<NetworkLobbyPlayer>().slot;
			this.lobbySlots[(int)slot] = null;
			base.OnServerRemovePlayer(conn, player);
			for (int i = 0; i < this.lobbySlots.Length; i++)
			{
				NetworkLobbyPlayer networkLobbyPlayer = this.lobbySlots[i];
				if (networkLobbyPlayer != null)
				{
					networkLobbyPlayer.GetComponent<NetworkLobbyPlayer>().readyToBegin = false;
					NetworkLobbyManager.s_LobbyReadyToBeginMessage.slotId = networkLobbyPlayer.slot;
					NetworkLobbyManager.s_LobbyReadyToBeginMessage.readyState = false;
					NetworkServer.SendToReady(null, 43, NetworkLobbyManager.s_LobbyReadyToBeginMessage);
				}
			}
			this.OnLobbyServerPlayerRemoved(conn, playerControllerId);
		}

		// Token: 0x060002AA RID: 682 RVA: 0x00010CB4 File Offset: 0x0000EEB4
		public override void ServerChangeScene(string sceneName)
		{
			if (sceneName == this.m_LobbyScene)
			{
				for (int i = 0; i < this.lobbySlots.Length; i++)
				{
					NetworkLobbyPlayer networkLobbyPlayer = this.lobbySlots[i];
					if (!(networkLobbyPlayer == null))
					{
						NetworkIdentity component = networkLobbyPlayer.GetComponent<NetworkIdentity>();
						PlayerController playerController;
						if (component.connectionToClient.GetPlayerController(component.playerControllerId, out playerController))
						{
							NetworkServer.Destroy(playerController.gameObject);
						}
						if (NetworkServer.active)
						{
							networkLobbyPlayer.GetComponent<NetworkLobbyPlayer>().readyToBegin = false;
							NetworkServer.ReplacePlayerForConnection(component.connectionToClient, networkLobbyPlayer.gameObject, component.playerControllerId);
						}
					}
				}
			}
			base.ServerChangeScene(sceneName);
		}

		// Token: 0x060002AB RID: 683 RVA: 0x00010D70 File Offset: 0x0000EF70
		public override void OnServerSceneChanged(string sceneName)
		{
			if (sceneName != this.m_LobbyScene)
			{
				for (int i = 0; i < this.m_PendingPlayers.Count; i++)
				{
					NetworkLobbyManager.PendingPlayer pendingPlayer = this.m_PendingPlayers[i];
					this.SceneLoadedForPlayer(pendingPlayer.conn, pendingPlayer.lobbyPlayer);
				}
				this.m_PendingPlayers.Clear();
			}
			this.OnLobbyServerSceneChanged(sceneName);
		}

		// Token: 0x060002AC RID: 684 RVA: 0x00010DE4 File Offset: 0x0000EFE4
		private void OnServerReadyToBeginMessage(NetworkMessage netMsg)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkLobbyManager OnServerReadyToBeginMessage");
			}
			netMsg.ReadMessage<LobbyReadyToBeginMessage>(NetworkLobbyManager.s_ReadyToBeginMessage);
			PlayerController playerController;
			if (!netMsg.conn.GetPlayerController((short)NetworkLobbyManager.s_ReadyToBeginMessage.slotId, out playerController))
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkLobbyManager OnServerReadyToBeginMessage invalid playerControllerId " + NetworkLobbyManager.s_ReadyToBeginMessage.slotId);
				}
			}
			else
			{
				NetworkLobbyPlayer component = playerController.gameObject.GetComponent<NetworkLobbyPlayer>();
				component.readyToBegin = NetworkLobbyManager.s_ReadyToBeginMessage.readyState;
				NetworkServer.SendToReady(null, 43, new LobbyReadyToBeginMessage
				{
					slotId = component.slot,
					readyState = NetworkLobbyManager.s_ReadyToBeginMessage.readyState
				});
				this.CheckReadyToBegin();
			}
		}

		// Token: 0x060002AD RID: 685 RVA: 0x00010EB0 File Offset: 0x0000F0B0
		private void OnServerSceneLoadedMessage(NetworkMessage netMsg)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkLobbyManager OnSceneLoadedMessage");
			}
			netMsg.ReadMessage<IntegerMessage>(NetworkLobbyManager.s_SceneLoadedMessage);
			PlayerController playerController;
			if (!netMsg.conn.GetPlayerController((short)NetworkLobbyManager.s_SceneLoadedMessage.value, out playerController))
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkLobbyManager OnServerSceneLoadedMessage invalid playerControllerId " + NetworkLobbyManager.s_SceneLoadedMessage.value);
				}
			}
			else
			{
				this.SceneLoadedForPlayer(netMsg.conn, playerController.gameObject);
			}
		}

		// Token: 0x060002AE RID: 686 RVA: 0x00010F3E File Offset: 0x0000F13E
		private void OnServerReturnToLobbyMessage(NetworkMessage netMsg)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkLobbyManager OnServerReturnToLobbyMessage");
			}
			this.ServerReturnToLobby();
		}

		// Token: 0x060002AF RID: 687 RVA: 0x00010F60 File Offset: 0x0000F160
		public override void OnStartServer()
		{
			if (string.IsNullOrEmpty(this.m_LobbyScene))
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkLobbyManager LobbyScene is empty. Set the LobbyScene in the inspector for the NetworkLobbyMangaer");
				}
			}
			else if (string.IsNullOrEmpty(this.m_PlayScene))
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkLobbyManager PlayScene is empty. Set the PlayScene in the inspector for the NetworkLobbyMangaer");
				}
			}
			else
			{
				if (this.lobbySlots.Length == 0)
				{
					this.lobbySlots = new NetworkLobbyPlayer[this.maxPlayers];
				}
				NetworkServer.RegisterHandler(43, new NetworkMessageDelegate(this.OnServerReadyToBeginMessage));
				NetworkServer.RegisterHandler(44, new NetworkMessageDelegate(this.OnServerSceneLoadedMessage));
				NetworkServer.RegisterHandler(46, new NetworkMessageDelegate(this.OnServerReturnToLobbyMessage));
				this.OnLobbyStartServer();
			}
		}

		// Token: 0x060002B0 RID: 688 RVA: 0x00011025 File Offset: 0x0000F225
		public override void OnStartHost()
		{
			this.OnLobbyStartHost();
		}

		// Token: 0x060002B1 RID: 689 RVA: 0x0001102E File Offset: 0x0000F22E
		public override void OnStopHost()
		{
			this.OnLobbyStopHost();
		}

		// Token: 0x060002B2 RID: 690 RVA: 0x00011038 File Offset: 0x0000F238
		public override void OnStartClient(NetworkClient lobbyClient)
		{
			if (this.lobbySlots.Length == 0)
			{
				this.lobbySlots = new NetworkLobbyPlayer[this.maxPlayers];
			}
			if (this.m_LobbyPlayerPrefab == null || this.m_LobbyPlayerPrefab.gameObject == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkLobbyManager no LobbyPlayer prefab is registered. Please add a LobbyPlayer prefab.");
				}
			}
			else
			{
				ClientScene.RegisterPrefab(this.m_LobbyPlayerPrefab.gameObject);
			}
			if (this.m_GamePlayerPrefab == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkLobbyManager no GamePlayer prefab is registered. Please add a GamePlayer prefab.");
				}
			}
			else
			{
				ClientScene.RegisterPrefab(this.m_GamePlayerPrefab);
			}
			lobbyClient.RegisterHandler(43, new NetworkMessageDelegate(this.OnClientReadyToBegin));
			lobbyClient.RegisterHandler(45, new NetworkMessageDelegate(this.OnClientAddPlayerFailedMessage));
			this.OnLobbyStartClient(lobbyClient);
		}

		// Token: 0x060002B3 RID: 691 RVA: 0x00011126 File Offset: 0x0000F326
		public override void OnClientConnect(NetworkConnection conn)
		{
			this.OnLobbyClientConnect(conn);
			this.CallOnClientEnterLobby();
			base.OnClientConnect(conn);
		}

		// Token: 0x060002B4 RID: 692 RVA: 0x0001113D File Offset: 0x0000F33D
		public override void OnClientDisconnect(NetworkConnection conn)
		{
			this.OnLobbyClientDisconnect(conn);
			base.OnClientDisconnect(conn);
		}

		// Token: 0x060002B5 RID: 693 RVA: 0x0001114E File Offset: 0x0000F34E
		public override void OnStopClient()
		{
			this.OnLobbyStopClient();
			this.CallOnClientExitLobby();
		}

		// Token: 0x060002B6 RID: 694 RVA: 0x00011160 File Offset: 0x0000F360
		public override void OnClientSceneChanged(NetworkConnection conn)
		{
			string name = SceneManager.GetSceneAt(0).name;
			if (name == this.m_LobbyScene)
			{
				if (this.client.isConnected)
				{
					this.CallOnClientEnterLobby();
				}
			}
			else
			{
				this.CallOnClientExitLobby();
			}
			base.OnClientSceneChanged(conn);
			this.OnLobbyClientSceneChanged(conn);
		}

		// Token: 0x060002B7 RID: 695 RVA: 0x000111C4 File Offset: 0x0000F3C4
		private void OnClientReadyToBegin(NetworkMessage netMsg)
		{
			netMsg.ReadMessage<LobbyReadyToBeginMessage>(NetworkLobbyManager.s_LobbyReadyToBeginMessage);
			if ((int)NetworkLobbyManager.s_LobbyReadyToBeginMessage.slotId >= this.lobbySlots.Count<NetworkLobbyPlayer>())
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkLobbyManager OnClientReadyToBegin invalid lobby slot " + NetworkLobbyManager.s_LobbyReadyToBeginMessage.slotId);
				}
			}
			else
			{
				NetworkLobbyPlayer networkLobbyPlayer = this.lobbySlots[(int)NetworkLobbyManager.s_LobbyReadyToBeginMessage.slotId];
				if (networkLobbyPlayer == null || networkLobbyPlayer.gameObject == null)
				{
					if (LogFilter.logError)
					{
						Debug.LogError("NetworkLobbyManager OnClientReadyToBegin no player at lobby slot " + NetworkLobbyManager.s_LobbyReadyToBeginMessage.slotId);
					}
				}
				else
				{
					networkLobbyPlayer.readyToBegin = NetworkLobbyManager.s_LobbyReadyToBeginMessage.readyState;
					networkLobbyPlayer.OnClientReady(NetworkLobbyManager.s_LobbyReadyToBeginMessage.readyState);
				}
			}
		}

		// Token: 0x060002B8 RID: 696 RVA: 0x000112A6 File Offset: 0x0000F4A6
		private void OnClientAddPlayerFailedMessage(NetworkMessage netMsg)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkLobbyManager Add Player failed.");
			}
			this.OnLobbyClientAddPlayerFailed();
		}

		// Token: 0x060002B9 RID: 697 RVA: 0x000112C5 File Offset: 0x0000F4C5
		public virtual void OnLobbyStartHost()
		{
		}

		// Token: 0x060002BA RID: 698 RVA: 0x000112C8 File Offset: 0x0000F4C8
		public virtual void OnLobbyStopHost()
		{
		}

		// Token: 0x060002BB RID: 699 RVA: 0x000112CB File Offset: 0x0000F4CB
		public virtual void OnLobbyStartServer()
		{
		}

		// Token: 0x060002BC RID: 700 RVA: 0x000112CE File Offset: 0x0000F4CE
		public virtual void OnLobbyServerConnect(NetworkConnection conn)
		{
		}

		// Token: 0x060002BD RID: 701 RVA: 0x000112D1 File Offset: 0x0000F4D1
		public virtual void OnLobbyServerDisconnect(NetworkConnection conn)
		{
		}

		// Token: 0x060002BE RID: 702 RVA: 0x000112D4 File Offset: 0x0000F4D4
		public virtual void OnLobbyServerSceneChanged(string sceneName)
		{
		}

		// Token: 0x060002BF RID: 703 RVA: 0x000112D8 File Offset: 0x0000F4D8
		public virtual GameObject OnLobbyServerCreateLobbyPlayer(NetworkConnection conn, short playerControllerId)
		{
			return null;
		}

		// Token: 0x060002C0 RID: 704 RVA: 0x000112F0 File Offset: 0x0000F4F0
		public virtual GameObject OnLobbyServerCreateGamePlayer(NetworkConnection conn, short playerControllerId)
		{
			return null;
		}

		// Token: 0x060002C1 RID: 705 RVA: 0x00011306 File Offset: 0x0000F506
		public virtual void OnLobbyServerPlayerRemoved(NetworkConnection conn, short playerControllerId)
		{
		}

		// Token: 0x060002C2 RID: 706 RVA: 0x0001130C File Offset: 0x0000F50C
		public virtual bool OnLobbyServerSceneLoadedForPlayer(GameObject lobbyPlayer, GameObject gamePlayer)
		{
			return true;
		}

		// Token: 0x060002C3 RID: 707 RVA: 0x00011322 File Offset: 0x0000F522
		public virtual void OnLobbyServerPlayersReady()
		{
			this.ServerChangeScene(this.m_PlayScene);
		}

		// Token: 0x060002C4 RID: 708 RVA: 0x00011331 File Offset: 0x0000F531
		public virtual void OnLobbyClientEnter()
		{
		}

		// Token: 0x060002C5 RID: 709 RVA: 0x00011334 File Offset: 0x0000F534
		public virtual void OnLobbyClientExit()
		{
		}

		// Token: 0x060002C6 RID: 710 RVA: 0x00011337 File Offset: 0x0000F537
		public virtual void OnLobbyClientConnect(NetworkConnection conn)
		{
		}

		// Token: 0x060002C7 RID: 711 RVA: 0x0001133A File Offset: 0x0000F53A
		public virtual void OnLobbyClientDisconnect(NetworkConnection conn)
		{
		}

		// Token: 0x060002C8 RID: 712 RVA: 0x0001133D File Offset: 0x0000F53D
		public virtual void OnLobbyStartClient(NetworkClient lobbyClient)
		{
		}

		// Token: 0x060002C9 RID: 713 RVA: 0x00011340 File Offset: 0x0000F540
		public virtual void OnLobbyStopClient()
		{
		}

		// Token: 0x060002CA RID: 714 RVA: 0x00011343 File Offset: 0x0000F543
		public virtual void OnLobbyClientSceneChanged(NetworkConnection conn)
		{
		}

		// Token: 0x060002CB RID: 715 RVA: 0x00011346 File Offset: 0x0000F546
		public virtual void OnLobbyClientAddPlayerFailed()
		{
		}

		// Token: 0x060002CC RID: 716 RVA: 0x0001134C File Offset: 0x0000F54C
		private void OnGUI()
		{
			if (this.showLobbyGUI)
			{
				string name = SceneManager.GetSceneAt(0).name;
				if (!(name != this.m_LobbyScene))
				{
					Rect position = new Rect(90f, 180f, 500f, 150f);
					GUI.Box(position, "Players:");
					if (NetworkClient.active)
					{
						Rect position2 = new Rect(100f, 300f, 120f, 20f);
						if (GUI.Button(position2, "Add Player"))
						{
							this.TryToAddPlayer();
						}
					}
				}
			}
		}

		// Token: 0x060002CD RID: 717 RVA: 0x000113F4 File Offset: 0x0000F5F4
		public void TryToAddPlayer()
		{
			if (NetworkClient.active)
			{
				short num = -1;
				List<PlayerController> playerControllers = NetworkClient.allClients[0].connection.playerControllers;
				if (playerControllers.Count < this.maxPlayers)
				{
					num = (short)playerControllers.Count;
				}
				else
				{
					short num2 = 0;
					while ((int)num2 < this.maxPlayers)
					{
						if (!playerControllers[(int)num2].IsValid)
						{
							num = num2;
							break;
						}
						num2 += 1;
					}
				}
				if (LogFilter.logDebug)
				{
					Debug.Log(string.Concat(new object[]
					{
						"NetworkLobbyManager TryToAddPlayer controllerId ",
						num,
						" ready:",
						ClientScene.ready
					}));
				}
				if (num == -1)
				{
					if (LogFilter.logDebug)
					{
						Debug.Log("NetworkLobbyManager No Space!");
					}
				}
				else if (ClientScene.ready)
				{
					ClientScene.AddPlayer(num);
				}
				else
				{
					ClientScene.AddPlayer(NetworkClient.allClients[0].connection, num);
				}
			}
			else if (LogFilter.logDebug)
			{
				Debug.Log("NetworkLobbyManager NetworkClient not active!");
			}
		}

		// Token: 0x060002CE RID: 718 RVA: 0x0001152E File Offset: 0x0000F72E
		// Note: this type is marked as 'beforefieldinit'.
		static NetworkLobbyManager()
		{
		}

		// Token: 0x04000144 RID: 324
		[SerializeField]
		private bool m_ShowLobbyGUI = true;

		// Token: 0x04000145 RID: 325
		[SerializeField]
		private int m_MaxPlayers = 4;

		// Token: 0x04000146 RID: 326
		[SerializeField]
		private int m_MaxPlayersPerConnection = 1;

		// Token: 0x04000147 RID: 327
		[SerializeField]
		private int m_MinPlayers;

		// Token: 0x04000148 RID: 328
		[SerializeField]
		private NetworkLobbyPlayer m_LobbyPlayerPrefab;

		// Token: 0x04000149 RID: 329
		[SerializeField]
		private GameObject m_GamePlayerPrefab;

		// Token: 0x0400014A RID: 330
		[SerializeField]
		private string m_LobbyScene = "";

		// Token: 0x0400014B RID: 331
		[SerializeField]
		private string m_PlayScene = "";

		// Token: 0x0400014C RID: 332
		private List<NetworkLobbyManager.PendingPlayer> m_PendingPlayers = new List<NetworkLobbyManager.PendingPlayer>();

		// Token: 0x0400014D RID: 333
		public NetworkLobbyPlayer[] lobbySlots;

		// Token: 0x0400014E RID: 334
		private static LobbyReadyToBeginMessage s_ReadyToBeginMessage = new LobbyReadyToBeginMessage();

		// Token: 0x0400014F RID: 335
		private static IntegerMessage s_SceneLoadedMessage = new IntegerMessage();

		// Token: 0x04000150 RID: 336
		private static LobbyReadyToBeginMessage s_LobbyReadyToBeginMessage = new LobbyReadyToBeginMessage();

		// Token: 0x0200004B RID: 75
		private struct PendingPlayer
		{
			// Token: 0x04000151 RID: 337
			public NetworkConnection conn;

			// Token: 0x04000152 RID: 338
			public GameObject lobbyPlayer;
		}
	}
}
