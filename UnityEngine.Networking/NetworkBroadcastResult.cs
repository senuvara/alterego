﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000044 RID: 68
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public struct NetworkBroadcastResult
	{
		// Token: 0x04000108 RID: 264
		public string serverAddress;

		// Token: 0x04000109 RID: 265
		public byte[] broadcastData;
	}
}
