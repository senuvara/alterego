﻿using System;
using System.Net.Sockets;

namespace UnityEngine.Networking
{
	// Token: 0x02000012 RID: 18
	internal static class DotNetCompatibility
	{
		// Token: 0x0600008C RID: 140 RVA: 0x00004F6C File Offset: 0x0000316C
		internal static string GetMethodName(this Delegate func)
		{
			return func.Method.Name;
		}

		// Token: 0x0600008D RID: 141 RVA: 0x00004F8C File Offset: 0x0000318C
		internal static Type GetBaseType(this Type type)
		{
			return type.BaseType;
		}

		// Token: 0x0600008E RID: 142 RVA: 0x00004FA8 File Offset: 0x000031A8
		internal static string GetErrorCode(this SocketException e)
		{
			return e.ErrorCode.ToString();
		}
	}
}
