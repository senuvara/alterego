﻿using System;
using UnityEngine.Networking.NetworkSystem;

namespace UnityEngine.Networking
{
	// Token: 0x02000035 RID: 53
	[DisallowMultipleComponent]
	[AddComponentMenu("Network/NetworkAnimator")]
	[RequireComponent(typeof(NetworkIdentity))]
	[RequireComponent(typeof(Animator))]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class NetworkAnimator : NetworkBehaviour
	{
		// Token: 0x06000119 RID: 281 RVA: 0x00009B45 File Offset: 0x00007D45
		public NetworkAnimator()
		{
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x0600011A RID: 282 RVA: 0x00009B50 File Offset: 0x00007D50
		// (set) Token: 0x0600011B RID: 283 RVA: 0x00009B6B File Offset: 0x00007D6B
		public Animator animator
		{
			get
			{
				return this.m_Animator;
			}
			set
			{
				this.m_Animator = value;
				this.ResetParameterOptions();
			}
		}

		// Token: 0x0600011C RID: 284 RVA: 0x00009B7B File Offset: 0x00007D7B
		public void SetParameterAutoSend(int index, bool value)
		{
			if (value)
			{
				this.m_ParameterSendBits |= 1U << index;
			}
			else
			{
				this.m_ParameterSendBits &= ~(1U << index);
			}
		}

		// Token: 0x0600011D RID: 285 RVA: 0x00009BB4 File Offset: 0x00007DB4
		public bool GetParameterAutoSend(int index)
		{
			return (this.m_ParameterSendBits & 1U << index) != 0U;
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x0600011E RID: 286 RVA: 0x00009BDC File Offset: 0x00007DDC
		private bool sendMessagesAllowed
		{
			get
			{
				if (base.isServer)
				{
					if (!base.localPlayerAuthority)
					{
						return true;
					}
					if (base.netIdentity != null && base.netIdentity.clientAuthorityOwner == null)
					{
						return true;
					}
				}
				return base.hasAuthority;
			}
		}

		// Token: 0x0600011F RID: 287 RVA: 0x00009C4B File Offset: 0x00007E4B
		internal void ResetParameterOptions()
		{
			Debug.Log("ResetParameterOptions");
			this.m_ParameterSendBits = 0U;
		}

		// Token: 0x06000120 RID: 288 RVA: 0x00009C60 File Offset: 0x00007E60
		private void FixedUpdate()
		{
			if (this.sendMessagesAllowed)
			{
				if (this.m_ParameterWriter == null)
				{
					this.m_ParameterWriter = new NetworkWriter();
				}
				this.CheckSendRate();
				int stateHash;
				float normalizedTime;
				if (this.CheckAnimStateChanged(out stateHash, out normalizedTime))
				{
					AnimationMessage animationMessage = new AnimationMessage();
					animationMessage.netId = base.netId;
					animationMessage.stateHash = stateHash;
					animationMessage.normalizedTime = normalizedTime;
					this.m_ParameterWriter.SeekZero();
					this.WriteParameters(this.m_ParameterWriter, false);
					animationMessage.parameters = this.m_ParameterWriter.ToArray();
					this.SendMessage(40, animationMessage);
				}
			}
		}

		// Token: 0x06000121 RID: 289 RVA: 0x00009D04 File Offset: 0x00007F04
		private bool CheckAnimStateChanged(out int stateHash, out float normalizedTime)
		{
			stateHash = 0;
			normalizedTime = 0f;
			bool result;
			if (this.m_Animator.IsInTransition(0))
			{
				AnimatorTransitionInfo animatorTransitionInfo = this.m_Animator.GetAnimatorTransitionInfo(0);
				if (animatorTransitionInfo.fullPathHash != this.m_TransitionHash)
				{
					this.m_TransitionHash = animatorTransitionInfo.fullPathHash;
					this.m_AnimationHash = 0;
					result = true;
				}
				else
				{
					result = false;
				}
			}
			else
			{
				AnimatorStateInfo currentAnimatorStateInfo = this.m_Animator.GetCurrentAnimatorStateInfo(0);
				if (currentAnimatorStateInfo.fullPathHash != this.m_AnimationHash)
				{
					if (this.m_AnimationHash != 0)
					{
						stateHash = currentAnimatorStateInfo.fullPathHash;
						normalizedTime = currentAnimatorStateInfo.normalizedTime;
					}
					this.m_TransitionHash = 0;
					this.m_AnimationHash = currentAnimatorStateInfo.fullPathHash;
					result = true;
				}
				else
				{
					result = false;
				}
			}
			return result;
		}

		// Token: 0x06000122 RID: 290 RVA: 0x00009DD4 File Offset: 0x00007FD4
		private void CheckSendRate()
		{
			if (this.sendMessagesAllowed && this.GetNetworkSendInterval() != 0f && this.m_SendTimer < Time.time)
			{
				this.m_SendTimer = Time.time + this.GetNetworkSendInterval();
				AnimationParametersMessage animationParametersMessage = new AnimationParametersMessage();
				animationParametersMessage.netId = base.netId;
				this.m_ParameterWriter.SeekZero();
				this.WriteParameters(this.m_ParameterWriter, true);
				animationParametersMessage.parameters = this.m_ParameterWriter.ToArray();
				this.SendMessage(41, animationParametersMessage);
			}
		}

		// Token: 0x06000123 RID: 291 RVA: 0x00009E65 File Offset: 0x00008065
		private void SendMessage(short type, MessageBase msg)
		{
			if (base.isServer)
			{
				NetworkServer.SendToReady(base.gameObject, type, msg);
			}
			else if (ClientScene.readyConnection != null)
			{
				ClientScene.readyConnection.Send(type, msg);
			}
		}

		// Token: 0x06000124 RID: 292 RVA: 0x00009EA4 File Offset: 0x000080A4
		private void SetSendTrackingParam(string p, int i)
		{
			p = "Sent Param: " + p;
			if (i == 0)
			{
				this.param0 = p;
			}
			if (i == 1)
			{
				this.param1 = p;
			}
			if (i == 2)
			{
				this.param2 = p;
			}
			if (i == 3)
			{
				this.param3 = p;
			}
			if (i == 4)
			{
				this.param4 = p;
			}
			if (i == 5)
			{
				this.param5 = p;
			}
		}

		// Token: 0x06000125 RID: 293 RVA: 0x00009F14 File Offset: 0x00008114
		private void SetRecvTrackingParam(string p, int i)
		{
			p = "Recv Param: " + p;
			if (i == 0)
			{
				this.param0 = p;
			}
			if (i == 1)
			{
				this.param1 = p;
			}
			if (i == 2)
			{
				this.param2 = p;
			}
			if (i == 3)
			{
				this.param3 = p;
			}
			if (i == 4)
			{
				this.param4 = p;
			}
			if (i == 5)
			{
				this.param5 = p;
			}
		}

		// Token: 0x06000126 RID: 294 RVA: 0x00009F82 File Offset: 0x00008182
		internal void HandleAnimMsg(AnimationMessage msg, NetworkReader reader)
		{
			if (!base.hasAuthority)
			{
				if (msg.stateHash != 0)
				{
					this.m_Animator.Play(msg.stateHash, 0, msg.normalizedTime);
				}
				this.ReadParameters(reader, false);
			}
		}

		// Token: 0x06000127 RID: 295 RVA: 0x00009FC2 File Offset: 0x000081C2
		internal void HandleAnimParamsMsg(AnimationParametersMessage msg, NetworkReader reader)
		{
			if (!base.hasAuthority)
			{
				this.ReadParameters(reader, true);
			}
		}

		// Token: 0x06000128 RID: 296 RVA: 0x00009FDD File Offset: 0x000081DD
		internal void HandleAnimTriggerMsg(int hash)
		{
			this.m_Animator.SetTrigger(hash);
		}

		// Token: 0x06000129 RID: 297 RVA: 0x00009FEC File Offset: 0x000081EC
		private void WriteParameters(NetworkWriter writer, bool autoSend)
		{
			for (int i = 0; i < this.m_Animator.parameters.Length; i++)
			{
				if (!autoSend || this.GetParameterAutoSend(i))
				{
					AnimatorControllerParameter animatorControllerParameter = this.m_Animator.parameters[i];
					if (animatorControllerParameter.type == AnimatorControllerParameterType.Int)
					{
						writer.WritePackedUInt32((uint)this.m_Animator.GetInteger(animatorControllerParameter.nameHash));
						this.SetSendTrackingParam(animatorControllerParameter.name + ":" + this.m_Animator.GetInteger(animatorControllerParameter.nameHash), i);
					}
					if (animatorControllerParameter.type == AnimatorControllerParameterType.Float)
					{
						writer.Write(this.m_Animator.GetFloat(animatorControllerParameter.nameHash));
						this.SetSendTrackingParam(animatorControllerParameter.name + ":" + this.m_Animator.GetFloat(animatorControllerParameter.nameHash), i);
					}
					if (animatorControllerParameter.type == AnimatorControllerParameterType.Bool)
					{
						writer.Write(this.m_Animator.GetBool(animatorControllerParameter.nameHash));
						this.SetSendTrackingParam(animatorControllerParameter.name + ":" + this.m_Animator.GetBool(animatorControllerParameter.nameHash), i);
					}
				}
			}
		}

		// Token: 0x0600012A RID: 298 RVA: 0x0000A138 File Offset: 0x00008338
		private void ReadParameters(NetworkReader reader, bool autoSend)
		{
			for (int i = 0; i < this.m_Animator.parameters.Length; i++)
			{
				if (!autoSend || this.GetParameterAutoSend(i))
				{
					AnimatorControllerParameter animatorControllerParameter = this.m_Animator.parameters[i];
					if (animatorControllerParameter.type == AnimatorControllerParameterType.Int)
					{
						int num = (int)reader.ReadPackedUInt32();
						this.m_Animator.SetInteger(animatorControllerParameter.nameHash, num);
						this.SetRecvTrackingParam(animatorControllerParameter.name + ":" + num, i);
					}
					if (animatorControllerParameter.type == AnimatorControllerParameterType.Float)
					{
						float num2 = reader.ReadSingle();
						this.m_Animator.SetFloat(animatorControllerParameter.nameHash, num2);
						this.SetRecvTrackingParam(animatorControllerParameter.name + ":" + num2, i);
					}
					if (animatorControllerParameter.type == AnimatorControllerParameterType.Bool)
					{
						bool flag = reader.ReadBoolean();
						this.m_Animator.SetBool(animatorControllerParameter.nameHash, flag);
						this.SetRecvTrackingParam(animatorControllerParameter.name + ":" + flag, i);
					}
				}
			}
		}

		// Token: 0x0600012B RID: 299 RVA: 0x0000A25C File Offset: 0x0000845C
		public override bool OnSerialize(NetworkWriter writer, bool forceAll)
		{
			bool result;
			if (forceAll)
			{
				if (this.m_Animator.IsInTransition(0))
				{
					AnimatorStateInfo nextAnimatorStateInfo = this.m_Animator.GetNextAnimatorStateInfo(0);
					writer.Write(nextAnimatorStateInfo.fullPathHash);
					writer.Write(nextAnimatorStateInfo.normalizedTime);
				}
				else
				{
					AnimatorStateInfo currentAnimatorStateInfo = this.m_Animator.GetCurrentAnimatorStateInfo(0);
					writer.Write(currentAnimatorStateInfo.fullPathHash);
					writer.Write(currentAnimatorStateInfo.normalizedTime);
				}
				this.WriteParameters(writer, false);
				result = true;
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x0600012C RID: 300 RVA: 0x0000A2F0 File Offset: 0x000084F0
		public override void OnDeserialize(NetworkReader reader, bool initialState)
		{
			if (initialState)
			{
				int stateNameHash = reader.ReadInt32();
				float normalizedTime = reader.ReadSingle();
				this.ReadParameters(reader, false);
				this.m_Animator.Play(stateNameHash, 0, normalizedTime);
			}
		}

		// Token: 0x0600012D RID: 301 RVA: 0x0000A32A File Offset: 0x0000852A
		public void SetTrigger(string triggerName)
		{
			this.SetTrigger(Animator.StringToHash(triggerName));
		}

		// Token: 0x0600012E RID: 302 RVA: 0x0000A33C File Offset: 0x0000853C
		public void SetTrigger(int hash)
		{
			AnimationTriggerMessage animationTriggerMessage = new AnimationTriggerMessage();
			animationTriggerMessage.netId = base.netId;
			animationTriggerMessage.hash = hash;
			if (base.hasAuthority && base.localPlayerAuthority)
			{
				if (NetworkClient.allClients.Count > 0)
				{
					NetworkConnection readyConnection = ClientScene.readyConnection;
					if (readyConnection != null)
					{
						readyConnection.Send(42, animationTriggerMessage);
					}
				}
			}
			else if (base.isServer && !base.localPlayerAuthority)
			{
				NetworkServer.SendToReady(base.gameObject, 42, animationTriggerMessage);
			}
		}

		// Token: 0x0600012F RID: 303 RVA: 0x0000A3D0 File Offset: 0x000085D0
		internal static void OnAnimationServerMessage(NetworkMessage netMsg)
		{
			netMsg.ReadMessage<AnimationMessage>(NetworkAnimator.s_AnimationMessage);
			if (LogFilter.logDev)
			{
				Debug.Log(string.Concat(new object[]
				{
					"OnAnimationMessage for netId=",
					NetworkAnimator.s_AnimationMessage.netId,
					" conn=",
					netMsg.conn
				}));
			}
			GameObject gameObject = NetworkServer.FindLocalObject(NetworkAnimator.s_AnimationMessage.netId);
			if (!(gameObject == null))
			{
				NetworkAnimator component = gameObject.GetComponent<NetworkAnimator>();
				if (component != null)
				{
					NetworkReader reader = new NetworkReader(NetworkAnimator.s_AnimationMessage.parameters);
					component.HandleAnimMsg(NetworkAnimator.s_AnimationMessage, reader);
					NetworkServer.SendToReady(gameObject, 40, NetworkAnimator.s_AnimationMessage);
				}
			}
		}

		// Token: 0x06000130 RID: 304 RVA: 0x0000A494 File Offset: 0x00008694
		internal static void OnAnimationParametersServerMessage(NetworkMessage netMsg)
		{
			netMsg.ReadMessage<AnimationParametersMessage>(NetworkAnimator.s_AnimationParametersMessage);
			if (LogFilter.logDev)
			{
				Debug.Log(string.Concat(new object[]
				{
					"OnAnimationParametersMessage for netId=",
					NetworkAnimator.s_AnimationParametersMessage.netId,
					" conn=",
					netMsg.conn
				}));
			}
			GameObject gameObject = NetworkServer.FindLocalObject(NetworkAnimator.s_AnimationParametersMessage.netId);
			if (!(gameObject == null))
			{
				NetworkAnimator component = gameObject.GetComponent<NetworkAnimator>();
				if (component != null)
				{
					NetworkReader reader = new NetworkReader(NetworkAnimator.s_AnimationParametersMessage.parameters);
					component.HandleAnimParamsMsg(NetworkAnimator.s_AnimationParametersMessage, reader);
					NetworkServer.SendToReady(gameObject, 41, NetworkAnimator.s_AnimationParametersMessage);
				}
			}
		}

		// Token: 0x06000131 RID: 305 RVA: 0x0000A558 File Offset: 0x00008758
		internal static void OnAnimationTriggerServerMessage(NetworkMessage netMsg)
		{
			netMsg.ReadMessage<AnimationTriggerMessage>(NetworkAnimator.s_AnimationTriggerMessage);
			if (LogFilter.logDev)
			{
				Debug.Log(string.Concat(new object[]
				{
					"OnAnimationTriggerMessage for netId=",
					NetworkAnimator.s_AnimationTriggerMessage.netId,
					" conn=",
					netMsg.conn
				}));
			}
			GameObject gameObject = NetworkServer.FindLocalObject(NetworkAnimator.s_AnimationTriggerMessage.netId);
			if (!(gameObject == null))
			{
				NetworkAnimator component = gameObject.GetComponent<NetworkAnimator>();
				if (component != null)
				{
					component.HandleAnimTriggerMsg(NetworkAnimator.s_AnimationTriggerMessage.hash);
					NetworkServer.SendToReady(gameObject, 42, NetworkAnimator.s_AnimationTriggerMessage);
				}
			}
		}

		// Token: 0x06000132 RID: 306 RVA: 0x0000A610 File Offset: 0x00008810
		internal static void OnAnimationClientMessage(NetworkMessage netMsg)
		{
			netMsg.ReadMessage<AnimationMessage>(NetworkAnimator.s_AnimationMessage);
			GameObject gameObject = ClientScene.FindLocalObject(NetworkAnimator.s_AnimationMessage.netId);
			if (!(gameObject == null))
			{
				NetworkAnimator component = gameObject.GetComponent<NetworkAnimator>();
				if (component != null)
				{
					NetworkReader reader = new NetworkReader(NetworkAnimator.s_AnimationMessage.parameters);
					component.HandleAnimMsg(NetworkAnimator.s_AnimationMessage, reader);
				}
			}
		}

		// Token: 0x06000133 RID: 307 RVA: 0x0000A67C File Offset: 0x0000887C
		internal static void OnAnimationParametersClientMessage(NetworkMessage netMsg)
		{
			netMsg.ReadMessage<AnimationParametersMessage>(NetworkAnimator.s_AnimationParametersMessage);
			GameObject gameObject = ClientScene.FindLocalObject(NetworkAnimator.s_AnimationParametersMessage.netId);
			if (!(gameObject == null))
			{
				NetworkAnimator component = gameObject.GetComponent<NetworkAnimator>();
				if (component != null)
				{
					NetworkReader reader = new NetworkReader(NetworkAnimator.s_AnimationParametersMessage.parameters);
					component.HandleAnimParamsMsg(NetworkAnimator.s_AnimationParametersMessage, reader);
				}
			}
		}

		// Token: 0x06000134 RID: 308 RVA: 0x0000A6E8 File Offset: 0x000088E8
		internal static void OnAnimationTriggerClientMessage(NetworkMessage netMsg)
		{
			netMsg.ReadMessage<AnimationTriggerMessage>(NetworkAnimator.s_AnimationTriggerMessage);
			GameObject gameObject = ClientScene.FindLocalObject(NetworkAnimator.s_AnimationTriggerMessage.netId);
			if (!(gameObject == null))
			{
				NetworkAnimator component = gameObject.GetComponent<NetworkAnimator>();
				if (component != null)
				{
					component.HandleAnimTriggerMsg(NetworkAnimator.s_AnimationTriggerMessage.hash);
				}
			}
		}

		// Token: 0x06000135 RID: 309 RVA: 0x0000A748 File Offset: 0x00008948
		// Note: this type is marked as 'beforefieldinit'.
		static NetworkAnimator()
		{
		}

		// Token: 0x040000A6 RID: 166
		[SerializeField]
		private Animator m_Animator;

		// Token: 0x040000A7 RID: 167
		[SerializeField]
		private uint m_ParameterSendBits;

		// Token: 0x040000A8 RID: 168
		private static AnimationMessage s_AnimationMessage = new AnimationMessage();

		// Token: 0x040000A9 RID: 169
		private static AnimationParametersMessage s_AnimationParametersMessage = new AnimationParametersMessage();

		// Token: 0x040000AA RID: 170
		private static AnimationTriggerMessage s_AnimationTriggerMessage = new AnimationTriggerMessage();

		// Token: 0x040000AB RID: 171
		private int m_AnimationHash;

		// Token: 0x040000AC RID: 172
		private int m_TransitionHash;

		// Token: 0x040000AD RID: 173
		private NetworkWriter m_ParameterWriter;

		// Token: 0x040000AE RID: 174
		private float m_SendTimer;

		// Token: 0x040000AF RID: 175
		public string param0;

		// Token: 0x040000B0 RID: 176
		public string param1;

		// Token: 0x040000B1 RID: 177
		public string param2;

		// Token: 0x040000B2 RID: 178
		public string param3;

		// Token: 0x040000B3 RID: 179
		public string param4;

		// Token: 0x040000B4 RID: 180
		public string param5;
	}
}
