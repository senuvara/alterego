﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine.Networking.NetworkSystem;

namespace UnityEngine.Networking
{
	// Token: 0x02000047 RID: 71
	[ExecuteInEditMode]
	[DisallowMultipleComponent]
	[AddComponentMenu("Network/NetworkIdentity")]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public sealed class NetworkIdentity : MonoBehaviour
	{
		// Token: 0x06000242 RID: 578 RVA: 0x0000C229 File Offset: 0x0000A429
		public NetworkIdentity()
		{
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x06000243 RID: 579 RVA: 0x0000C240 File Offset: 0x0000A440
		public bool isClient
		{
			get
			{
				return this.m_IsClient;
			}
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x06000244 RID: 580 RVA: 0x0000C25C File Offset: 0x0000A45C
		public bool isServer
		{
			get
			{
				return this.m_IsServer && NetworkServer.active;
			}
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x06000245 RID: 581 RVA: 0x0000C284 File Offset: 0x0000A484
		public bool hasAuthority
		{
			get
			{
				return this.m_HasAuthority;
			}
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x06000246 RID: 582 RVA: 0x0000C2A0 File Offset: 0x0000A4A0
		public NetworkInstanceId netId
		{
			get
			{
				return this.m_NetId;
			}
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x06000247 RID: 583 RVA: 0x0000C2BC File Offset: 0x0000A4BC
		public NetworkSceneId sceneId
		{
			get
			{
				return this.m_SceneId;
			}
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x06000248 RID: 584 RVA: 0x0000C2D8 File Offset: 0x0000A4D8
		// (set) Token: 0x06000249 RID: 585 RVA: 0x0000C2F3 File Offset: 0x0000A4F3
		public bool serverOnly
		{
			get
			{
				return this.m_ServerOnly;
			}
			set
			{
				this.m_ServerOnly = value;
			}
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x0600024A RID: 586 RVA: 0x0000C300 File Offset: 0x0000A500
		// (set) Token: 0x0600024B RID: 587 RVA: 0x0000C31B File Offset: 0x0000A51B
		public bool localPlayerAuthority
		{
			get
			{
				return this.m_LocalPlayerAuthority;
			}
			set
			{
				this.m_LocalPlayerAuthority = value;
			}
		}

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x0600024C RID: 588 RVA: 0x0000C328 File Offset: 0x0000A528
		public NetworkConnection clientAuthorityOwner
		{
			get
			{
				return this.m_ClientAuthorityOwner;
			}
		}

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x0600024D RID: 589 RVA: 0x0000C344 File Offset: 0x0000A544
		public NetworkHash128 assetId
		{
			get
			{
				return this.m_AssetId;
			}
		}

		// Token: 0x0600024E RID: 590 RVA: 0x0000C360 File Offset: 0x0000A560
		internal void SetDynamicAssetId(NetworkHash128 newAssetId)
		{
			if (!this.m_AssetId.IsValid() || this.m_AssetId.Equals(newAssetId))
			{
				this.m_AssetId = newAssetId;
			}
			else if (LogFilter.logWarn)
			{
				Debug.LogWarning("SetDynamicAssetId object already has an assetId <" + this.m_AssetId + ">");
			}
		}

		// Token: 0x0600024F RID: 591 RVA: 0x0000C3D5 File Offset: 0x0000A5D5
		internal void SetClientOwner(NetworkConnection conn)
		{
			if (this.m_ClientAuthorityOwner != null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("SetClientOwner m_ClientAuthorityOwner already set!");
				}
			}
			this.m_ClientAuthorityOwner = conn;
			this.m_ClientAuthorityOwner.AddOwnedObject(this);
		}

		// Token: 0x06000250 RID: 592 RVA: 0x0000C40E File Offset: 0x0000A60E
		internal void ClearClientOwner()
		{
			this.m_ClientAuthorityOwner = null;
		}

		// Token: 0x06000251 RID: 593 RVA: 0x0000C418 File Offset: 0x0000A618
		internal void ForceAuthority(bool authority)
		{
			if (this.m_HasAuthority != authority)
			{
				this.m_HasAuthority = authority;
				if (authority)
				{
					this.OnStartAuthority();
				}
				else
				{
					this.OnStopAuthority();
				}
			}
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x06000252 RID: 594 RVA: 0x0000C450 File Offset: 0x0000A650
		public bool isLocalPlayer
		{
			get
			{
				return this.m_IsLocalPlayer;
			}
		}

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x06000253 RID: 595 RVA: 0x0000C46C File Offset: 0x0000A66C
		public short playerControllerId
		{
			get
			{
				return this.m_PlayerId;
			}
		}

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x06000254 RID: 596 RVA: 0x0000C488 File Offset: 0x0000A688
		public NetworkConnection connectionToServer
		{
			get
			{
				return this.m_ConnectionToServer;
			}
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x06000255 RID: 597 RVA: 0x0000C4A4 File Offset: 0x0000A6A4
		public NetworkConnection connectionToClient
		{
			get
			{
				return this.m_ConnectionToClient;
			}
		}

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x06000256 RID: 598 RVA: 0x0000C4C0 File Offset: 0x0000A6C0
		public ReadOnlyCollection<NetworkConnection> observers
		{
			get
			{
				ReadOnlyCollection<NetworkConnection> result;
				if (this.m_Observers == null)
				{
					result = null;
				}
				else
				{
					result = new ReadOnlyCollection<NetworkConnection>(this.m_Observers);
				}
				return result;
			}
		}

		// Token: 0x06000257 RID: 599 RVA: 0x0000C4F4 File Offset: 0x0000A6F4
		internal static NetworkInstanceId GetNextNetworkId()
		{
			uint value = NetworkIdentity.s_NextNetworkId;
			NetworkIdentity.s_NextNetworkId += 1U;
			return new NetworkInstanceId(value);
		}

		// Token: 0x06000258 RID: 600 RVA: 0x0000C521 File Offset: 0x0000A721
		private void CacheBehaviours()
		{
			if (this.m_NetworkBehaviours == null)
			{
				this.m_NetworkBehaviours = base.GetComponents<NetworkBehaviour>();
			}
		}

		// Token: 0x06000259 RID: 601 RVA: 0x0000C53D File Offset: 0x0000A73D
		internal static void AddNetworkId(uint id)
		{
			if (id >= NetworkIdentity.s_NextNetworkId)
			{
				NetworkIdentity.s_NextNetworkId = id + 1U;
			}
		}

		// Token: 0x0600025A RID: 602 RVA: 0x0000C555 File Offset: 0x0000A755
		internal void SetNetworkInstanceId(NetworkInstanceId newNetId)
		{
			this.m_NetId = newNetId;
			if (newNetId.Value == 0U)
			{
				this.m_IsServer = false;
			}
		}

		// Token: 0x0600025B RID: 603 RVA: 0x0000C574 File Offset: 0x0000A774
		public void ForceSceneId(int newSceneId)
		{
			this.m_SceneId = new NetworkSceneId((uint)newSceneId);
		}

		// Token: 0x0600025C RID: 604 RVA: 0x0000C583 File Offset: 0x0000A783
		internal void UpdateClientServer(bool isClientFlag, bool isServerFlag)
		{
			this.m_IsClient = (this.m_IsClient || isClientFlag);
			this.m_IsServer = (this.m_IsServer || isServerFlag);
		}

		// Token: 0x0600025D RID: 605 RVA: 0x0000C5A2 File Offset: 0x0000A7A2
		internal void SetNotLocalPlayer()
		{
			this.m_IsLocalPlayer = false;
			if (!NetworkServer.active || !NetworkServer.localClientActive)
			{
				this.m_HasAuthority = false;
			}
		}

		// Token: 0x0600025E RID: 606 RVA: 0x0000C5CD File Offset: 0x0000A7CD
		internal void RemoveObserverInternal(NetworkConnection conn)
		{
			if (this.m_Observers != null)
			{
				this.m_Observers.Remove(conn);
				this.m_ObserverConnections.Remove(conn.connectionId);
			}
		}

		// Token: 0x0600025F RID: 607 RVA: 0x0000C5FC File Offset: 0x0000A7FC
		private void OnDestroy()
		{
			if (this.m_IsServer && NetworkServer.active)
			{
				NetworkServer.Destroy(base.gameObject);
			}
		}

		// Token: 0x06000260 RID: 608 RVA: 0x0000C624 File Offset: 0x0000A824
		internal void OnStartServer(bool allowNonZeroNetId)
		{
			if (!this.m_IsServer)
			{
				this.m_IsServer = true;
				if (this.m_LocalPlayerAuthority)
				{
					this.m_HasAuthority = false;
				}
				else
				{
					this.m_HasAuthority = true;
				}
				this.m_Observers = new List<NetworkConnection>();
				this.m_ObserverConnections = new HashSet<int>();
				this.CacheBehaviours();
				if (this.netId.IsEmpty())
				{
					this.m_NetId = NetworkIdentity.GetNextNetworkId();
				}
				else if (!allowNonZeroNetId)
				{
					if (LogFilter.logError)
					{
						Debug.LogError(string.Concat(new object[]
						{
							"Object has non-zero netId ",
							this.netId,
							" for ",
							base.gameObject
						}));
					}
					return;
				}
				if (LogFilter.logDev)
				{
					Debug.Log(string.Concat(new object[]
					{
						"OnStartServer ",
						base.gameObject,
						" GUID:",
						this.netId
					}));
				}
				NetworkServer.instance.SetLocalObjectOnServer(this.netId, base.gameObject);
				for (int i = 0; i < this.m_NetworkBehaviours.Length; i++)
				{
					NetworkBehaviour networkBehaviour = this.m_NetworkBehaviours[i];
					try
					{
						networkBehaviour.OnStartServer();
					}
					catch (Exception ex)
					{
						Debug.LogError("Exception in OnStartServer:" + ex.Message + " " + ex.StackTrace);
					}
				}
				if (NetworkClient.active && NetworkServer.localClientActive)
				{
					ClientScene.SetLocalObject(this.netId, base.gameObject);
					this.OnStartClient();
				}
				if (this.m_HasAuthority)
				{
					this.OnStartAuthority();
				}
			}
		}

		// Token: 0x06000261 RID: 609 RVA: 0x0000C808 File Offset: 0x0000AA08
		internal void OnStartClient()
		{
			if (!this.m_IsClient)
			{
				this.m_IsClient = true;
			}
			this.CacheBehaviours();
			if (LogFilter.logDev)
			{
				Debug.Log(string.Concat(new object[]
				{
					"OnStartClient ",
					base.gameObject,
					" GUID:",
					this.netId,
					" localPlayerAuthority:",
					this.localPlayerAuthority
				}));
			}
			for (int i = 0; i < this.m_NetworkBehaviours.Length; i++)
			{
				NetworkBehaviour networkBehaviour = this.m_NetworkBehaviours[i];
				try
				{
					networkBehaviour.PreStartClient();
					networkBehaviour.OnStartClient();
				}
				catch (Exception ex)
				{
					Debug.LogError("Exception in OnStartClient:" + ex.Message + " " + ex.StackTrace);
				}
			}
		}

		// Token: 0x06000262 RID: 610 RVA: 0x0000C8F8 File Offset: 0x0000AAF8
		internal void OnStartAuthority()
		{
			for (int i = 0; i < this.m_NetworkBehaviours.Length; i++)
			{
				NetworkBehaviour networkBehaviour = this.m_NetworkBehaviours[i];
				try
				{
					networkBehaviour.OnStartAuthority();
				}
				catch (Exception ex)
				{
					Debug.LogError("Exception in OnStartAuthority:" + ex.Message + " " + ex.StackTrace);
				}
			}
		}

		// Token: 0x06000263 RID: 611 RVA: 0x0000C970 File Offset: 0x0000AB70
		internal void OnStopAuthority()
		{
			for (int i = 0; i < this.m_NetworkBehaviours.Length; i++)
			{
				NetworkBehaviour networkBehaviour = this.m_NetworkBehaviours[i];
				try
				{
					networkBehaviour.OnStopAuthority();
				}
				catch (Exception ex)
				{
					Debug.LogError("Exception in OnStopAuthority:" + ex.Message + " " + ex.StackTrace);
				}
			}
		}

		// Token: 0x06000264 RID: 612 RVA: 0x0000C9E8 File Offset: 0x0000ABE8
		internal void OnSetLocalVisibility(bool vis)
		{
			for (int i = 0; i < this.m_NetworkBehaviours.Length; i++)
			{
				NetworkBehaviour networkBehaviour = this.m_NetworkBehaviours[i];
				try
				{
					networkBehaviour.OnSetLocalVisibility(vis);
				}
				catch (Exception ex)
				{
					Debug.LogError("Exception in OnSetLocalVisibility:" + ex.Message + " " + ex.StackTrace);
				}
			}
		}

		// Token: 0x06000265 RID: 613 RVA: 0x0000CA60 File Offset: 0x0000AC60
		internal bool OnCheckObserver(NetworkConnection conn)
		{
			for (int i = 0; i < this.m_NetworkBehaviours.Length; i++)
			{
				NetworkBehaviour networkBehaviour = this.m_NetworkBehaviours[i];
				try
				{
					if (!networkBehaviour.OnCheckObserver(conn))
					{
						return false;
					}
				}
				catch (Exception ex)
				{
					Debug.LogError("Exception in OnCheckObserver:" + ex.Message + " " + ex.StackTrace);
				}
			}
			return true;
		}

		// Token: 0x06000266 RID: 614 RVA: 0x0000CAEC File Offset: 0x0000ACEC
		internal void UNetSerializeAllVars(NetworkWriter writer)
		{
			for (int i = 0; i < this.m_NetworkBehaviours.Length; i++)
			{
				NetworkBehaviour networkBehaviour = this.m_NetworkBehaviours[i];
				networkBehaviour.OnSerialize(writer, true);
			}
		}

		// Token: 0x06000267 RID: 615 RVA: 0x0000CB28 File Offset: 0x0000AD28
		internal void HandleClientAuthority(bool authority)
		{
			if (!this.localPlayerAuthority)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("HandleClientAuthority " + base.gameObject + " does not have localPlayerAuthority");
				}
			}
			else
			{
				this.ForceAuthority(authority);
			}
		}

		// Token: 0x06000268 RID: 616 RVA: 0x0000CB74 File Offset: 0x0000AD74
		private bool GetInvokeComponent(int cmdHash, Type invokeClass, out NetworkBehaviour invokeComponent)
		{
			NetworkBehaviour networkBehaviour = null;
			for (int i = 0; i < this.m_NetworkBehaviours.Length; i++)
			{
				NetworkBehaviour networkBehaviour2 = this.m_NetworkBehaviours[i];
				if (networkBehaviour2.GetType() == invokeClass || networkBehaviour2.GetType().IsSubclassOf(invokeClass))
				{
					networkBehaviour = networkBehaviour2;
					break;
				}
			}
			bool result;
			if (networkBehaviour == null)
			{
				string cmdHashHandlerName = NetworkBehaviour.GetCmdHashHandlerName(cmdHash);
				if (LogFilter.logError)
				{
					Debug.LogError(string.Concat(new object[]
					{
						"Found no behaviour for incoming [",
						cmdHashHandlerName,
						"] on ",
						base.gameObject,
						",  the server and client should have the same NetworkBehaviour instances [netId=",
						this.netId,
						"]."
					}));
				}
				invokeComponent = null;
				result = false;
			}
			else
			{
				invokeComponent = networkBehaviour;
				result = true;
			}
			return result;
		}

		// Token: 0x06000269 RID: 617 RVA: 0x0000CC50 File Offset: 0x0000AE50
		internal void HandleSyncEvent(int cmdHash, NetworkReader reader)
		{
			Type invokeClass;
			NetworkBehaviour.CmdDelegate cmdDelegate;
			NetworkBehaviour obj;
			if (base.gameObject == null)
			{
				string cmdHashHandlerName = NetworkBehaviour.GetCmdHashHandlerName(cmdHash);
				if (LogFilter.logWarn)
				{
					Debug.LogWarning(string.Concat(new object[]
					{
						"SyncEvent [",
						cmdHashHandlerName,
						"] received for deleted object [netId=",
						this.netId,
						"]"
					}));
				}
			}
			else if (!NetworkBehaviour.GetInvokerForHashSyncEvent(cmdHash, out invokeClass, out cmdDelegate))
			{
				string cmdHashHandlerName2 = NetworkBehaviour.GetCmdHashHandlerName(cmdHash);
				if (LogFilter.logError)
				{
					Debug.LogError(string.Concat(new object[]
					{
						"Found no receiver for incoming [",
						cmdHashHandlerName2,
						"] on ",
						base.gameObject,
						",  the server and client should have the same NetworkBehaviour instances [netId=",
						this.netId,
						"]."
					}));
				}
			}
			else if (!this.GetInvokeComponent(cmdHash, invokeClass, out obj))
			{
				string cmdHashHandlerName3 = NetworkBehaviour.GetCmdHashHandlerName(cmdHash);
				if (LogFilter.logWarn)
				{
					Debug.LogWarning(string.Concat(new object[]
					{
						"SyncEvent [",
						cmdHashHandlerName3,
						"] handler not found [netId=",
						this.netId,
						"]"
					}));
				}
			}
			else
			{
				cmdDelegate(obj, reader);
			}
		}

		// Token: 0x0600026A RID: 618 RVA: 0x0000CDA8 File Offset: 0x0000AFA8
		internal void HandleSyncList(int cmdHash, NetworkReader reader)
		{
			Type invokeClass;
			NetworkBehaviour.CmdDelegate cmdDelegate;
			NetworkBehaviour obj;
			if (base.gameObject == null)
			{
				string cmdHashHandlerName = NetworkBehaviour.GetCmdHashHandlerName(cmdHash);
				if (LogFilter.logWarn)
				{
					Debug.LogWarning(string.Concat(new object[]
					{
						"SyncList [",
						cmdHashHandlerName,
						"] received for deleted object [netId=",
						this.netId,
						"]"
					}));
				}
			}
			else if (!NetworkBehaviour.GetInvokerForHashSyncList(cmdHash, out invokeClass, out cmdDelegate))
			{
				string cmdHashHandlerName2 = NetworkBehaviour.GetCmdHashHandlerName(cmdHash);
				if (LogFilter.logError)
				{
					Debug.LogError(string.Concat(new object[]
					{
						"Found no receiver for incoming [",
						cmdHashHandlerName2,
						"] on ",
						base.gameObject,
						",  the server and client should have the same NetworkBehaviour instances [netId=",
						this.netId,
						"]."
					}));
				}
			}
			else if (!this.GetInvokeComponent(cmdHash, invokeClass, out obj))
			{
				string cmdHashHandlerName3 = NetworkBehaviour.GetCmdHashHandlerName(cmdHash);
				if (LogFilter.logWarn)
				{
					Debug.LogWarning(string.Concat(new object[]
					{
						"SyncList [",
						cmdHashHandlerName3,
						"] handler not found [netId=",
						this.netId,
						"]"
					}));
				}
			}
			else
			{
				cmdDelegate(obj, reader);
			}
		}

		// Token: 0x0600026B RID: 619 RVA: 0x0000CF00 File Offset: 0x0000B100
		internal void HandleCommand(int cmdHash, NetworkReader reader)
		{
			Type invokeClass;
			NetworkBehaviour.CmdDelegate cmdDelegate;
			NetworkBehaviour obj;
			if (base.gameObject == null)
			{
				string cmdHashHandlerName = NetworkBehaviour.GetCmdHashHandlerName(cmdHash);
				if (LogFilter.logWarn)
				{
					Debug.LogWarning(string.Concat(new object[]
					{
						"Command [",
						cmdHashHandlerName,
						"] received for deleted object [netId=",
						this.netId,
						"]"
					}));
				}
			}
			else if (!NetworkBehaviour.GetInvokerForHashCommand(cmdHash, out invokeClass, out cmdDelegate))
			{
				string cmdHashHandlerName2 = NetworkBehaviour.GetCmdHashHandlerName(cmdHash);
				if (LogFilter.logError)
				{
					Debug.LogError(string.Concat(new object[]
					{
						"Found no receiver for incoming [",
						cmdHashHandlerName2,
						"] on ",
						base.gameObject,
						",  the server and client should have the same NetworkBehaviour instances [netId=",
						this.netId,
						"]."
					}));
				}
			}
			else if (!this.GetInvokeComponent(cmdHash, invokeClass, out obj))
			{
				string cmdHashHandlerName3 = NetworkBehaviour.GetCmdHashHandlerName(cmdHash);
				if (LogFilter.logWarn)
				{
					Debug.LogWarning(string.Concat(new object[]
					{
						"Command [",
						cmdHashHandlerName3,
						"] handler not found [netId=",
						this.netId,
						"]"
					}));
				}
			}
			else
			{
				cmdDelegate(obj, reader);
			}
		}

		// Token: 0x0600026C RID: 620 RVA: 0x0000D058 File Offset: 0x0000B258
		internal void HandleRPC(int cmdHash, NetworkReader reader)
		{
			Type invokeClass;
			NetworkBehaviour.CmdDelegate cmdDelegate;
			NetworkBehaviour obj;
			if (base.gameObject == null)
			{
				string cmdHashHandlerName = NetworkBehaviour.GetCmdHashHandlerName(cmdHash);
				if (LogFilter.logWarn)
				{
					Debug.LogWarning(string.Concat(new object[]
					{
						"ClientRpc [",
						cmdHashHandlerName,
						"] received for deleted object [netId=",
						this.netId,
						"]"
					}));
				}
			}
			else if (!NetworkBehaviour.GetInvokerForHashClientRpc(cmdHash, out invokeClass, out cmdDelegate))
			{
				string cmdHashHandlerName2 = NetworkBehaviour.GetCmdHashHandlerName(cmdHash);
				if (LogFilter.logError)
				{
					Debug.LogError(string.Concat(new object[]
					{
						"Found no receiver for incoming [",
						cmdHashHandlerName2,
						"] on ",
						base.gameObject,
						",  the server and client should have the same NetworkBehaviour instances [netId=",
						this.netId,
						"]."
					}));
				}
			}
			else if (!this.GetInvokeComponent(cmdHash, invokeClass, out obj))
			{
				string cmdHashHandlerName3 = NetworkBehaviour.GetCmdHashHandlerName(cmdHash);
				if (LogFilter.logWarn)
				{
					Debug.LogWarning(string.Concat(new object[]
					{
						"ClientRpc [",
						cmdHashHandlerName3,
						"] handler not found [netId=",
						this.netId,
						"]"
					}));
				}
			}
			else
			{
				cmdDelegate(obj, reader);
			}
		}

		// Token: 0x0600026D RID: 621 RVA: 0x0000D1B0 File Offset: 0x0000B3B0
		internal void UNetUpdate()
		{
			uint num = 0U;
			for (int i = 0; i < this.m_NetworkBehaviours.Length; i++)
			{
				NetworkBehaviour networkBehaviour = this.m_NetworkBehaviours[i];
				int dirtyChannel = networkBehaviour.GetDirtyChannel();
				if (dirtyChannel != -1)
				{
					num |= 1U << dirtyChannel;
				}
			}
			if (num != 0U)
			{
				int j = 0;
				while (j < NetworkServer.numChannels)
				{
					if ((num & 1U << j) != 0U)
					{
						NetworkIdentity.s_UpdateWriter.StartMessage(8);
						NetworkIdentity.s_UpdateWriter.Write(this.netId);
						bool flag = false;
						NetworkBehaviour[] behavioursOfSameChannel = this.GetBehavioursOfSameChannel(j, false);
						for (int k = 0; k < behavioursOfSameChannel.Length; k++)
						{
							short position = NetworkIdentity.s_UpdateWriter.Position;
							NetworkBehaviour networkBehaviour2 = behavioursOfSameChannel[k];
							if (networkBehaviour2.OnSerialize(NetworkIdentity.s_UpdateWriter, false))
							{
								networkBehaviour2.ClearAllDirtyBits();
								flag = true;
							}
							if (NetworkIdentity.s_UpdateWriter.Position - position > (short)NetworkServer.maxPacketSize)
							{
								if (LogFilter.logWarn)
								{
									Debug.LogWarning(string.Concat(new object[]
									{
										"Large state update of ",
										(int)(NetworkIdentity.s_UpdateWriter.Position - position),
										" bytes for netId:",
										this.netId,
										" from script:",
										networkBehaviour2
									}));
								}
							}
						}
						if (flag)
						{
							NetworkIdentity.s_UpdateWriter.FinishMessage();
							NetworkServer.SendWriterToReady(base.gameObject, NetworkIdentity.s_UpdateWriter, j);
						}
					}
					IL_178:
					j++;
					continue;
					goto IL_178;
				}
			}
		}

		// Token: 0x0600026E RID: 622 RVA: 0x0000D348 File Offset: 0x0000B548
		private NetworkBehaviour[] GetBehavioursOfSameChannel(int channelId, bool initialState)
		{
			List<NetworkBehaviour> list = new List<NetworkBehaviour>();
			NetworkBehaviour[] result;
			if (initialState && this.m_NetworkBehaviours == null)
			{
				this.m_NetworkBehaviours = base.GetComponents<NetworkBehaviour>();
				result = this.m_NetworkBehaviours;
			}
			else
			{
				for (int i = 0; i < this.m_NetworkBehaviours.Length; i++)
				{
					NetworkBehaviour networkBehaviour = this.m_NetworkBehaviours[i];
					if (networkBehaviour.GetNetworkChannel() == channelId)
					{
						list.Add(networkBehaviour);
					}
				}
				result = list.ToArray();
			}
			return result;
		}

		// Token: 0x0600026F RID: 623 RVA: 0x0000D3CC File Offset: 0x0000B5CC
		internal void OnUpdateVars(NetworkReader reader, bool initialState, NetworkMessage netMsg)
		{
			foreach (NetworkBehaviour networkBehaviour in this.GetBehavioursOfSameChannel(netMsg.channelId, initialState))
			{
				networkBehaviour.OnDeserialize(reader, initialState);
			}
		}

		// Token: 0x06000270 RID: 624 RVA: 0x0000D40C File Offset: 0x0000B60C
		internal void SetLocalPlayer(short localPlayerControllerId)
		{
			this.m_IsLocalPlayer = true;
			this.m_PlayerId = localPlayerControllerId;
			bool hasAuthority = this.m_HasAuthority;
			if (this.localPlayerAuthority)
			{
				this.m_HasAuthority = true;
			}
			for (int i = 0; i < this.m_NetworkBehaviours.Length; i++)
			{
				NetworkBehaviour networkBehaviour = this.m_NetworkBehaviours[i];
				networkBehaviour.OnStartLocalPlayer();
				if (this.localPlayerAuthority && !hasAuthority)
				{
					networkBehaviour.OnStartAuthority();
				}
			}
		}

		// Token: 0x06000271 RID: 625 RVA: 0x0000D486 File Offset: 0x0000B686
		internal void SetConnectionToServer(NetworkConnection conn)
		{
			this.m_ConnectionToServer = conn;
		}

		// Token: 0x06000272 RID: 626 RVA: 0x0000D490 File Offset: 0x0000B690
		internal void SetConnectionToClient(NetworkConnection conn, short newPlayerControllerId)
		{
			this.m_PlayerId = newPlayerControllerId;
			this.m_ConnectionToClient = conn;
		}

		// Token: 0x06000273 RID: 627 RVA: 0x0000D4A4 File Offset: 0x0000B6A4
		internal void OnNetworkDestroy()
		{
			int num = 0;
			while (this.m_NetworkBehaviours != null && num < this.m_NetworkBehaviours.Length)
			{
				NetworkBehaviour networkBehaviour = this.m_NetworkBehaviours[num];
				networkBehaviour.OnNetworkDestroy();
				num++;
			}
			this.m_IsServer = false;
		}

		// Token: 0x06000274 RID: 628 RVA: 0x0000D4F0 File Offset: 0x0000B6F0
		internal void ClearObservers()
		{
			if (this.m_Observers != null)
			{
				int count = this.m_Observers.Count;
				for (int i = 0; i < count; i++)
				{
					NetworkConnection networkConnection = this.m_Observers[i];
					networkConnection.RemoveFromVisList(this, true);
				}
				this.m_Observers.Clear();
				this.m_ObserverConnections.Clear();
			}
		}

		// Token: 0x06000275 RID: 629 RVA: 0x0000D558 File Offset: 0x0000B758
		internal void AddObserver(NetworkConnection conn)
		{
			if (this.m_Observers == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("AddObserver for " + base.gameObject + " observer list is null");
				}
			}
			else if (this.m_ObserverConnections.Contains(conn.connectionId))
			{
				if (LogFilter.logDebug)
				{
					Debug.Log(string.Concat(new object[]
					{
						"Duplicate observer ",
						conn.address,
						" added for ",
						base.gameObject
					}));
				}
			}
			else
			{
				if (LogFilter.logDev)
				{
					Debug.Log(string.Concat(new object[]
					{
						"Added observer ",
						conn.address,
						" added for ",
						base.gameObject
					}));
				}
				this.m_Observers.Add(conn);
				this.m_ObserverConnections.Add(conn.connectionId);
				conn.AddToVisList(this);
			}
		}

		// Token: 0x06000276 RID: 630 RVA: 0x0000D65A File Offset: 0x0000B85A
		internal void RemoveObserver(NetworkConnection conn)
		{
			if (this.m_Observers != null)
			{
				this.m_Observers.Remove(conn);
				this.m_ObserverConnections.Remove(conn.connectionId);
				conn.RemoveFromVisList(this, false);
			}
		}

		// Token: 0x06000277 RID: 631 RVA: 0x0000D694 File Offset: 0x0000B894
		public void RebuildObservers(bool initialize)
		{
			if (this.m_Observers != null)
			{
				bool flag = false;
				bool flag2 = false;
				HashSet<NetworkConnection> hashSet = new HashSet<NetworkConnection>();
				HashSet<NetworkConnection> hashSet2 = new HashSet<NetworkConnection>(this.m_Observers);
				for (int i = 0; i < this.m_NetworkBehaviours.Length; i++)
				{
					NetworkBehaviour networkBehaviour = this.m_NetworkBehaviours[i];
					flag2 |= networkBehaviour.OnRebuildObservers(hashSet, initialize);
				}
				if (!flag2)
				{
					if (initialize)
					{
						for (int j = 0; j < NetworkServer.connections.Count; j++)
						{
							NetworkConnection networkConnection = NetworkServer.connections[j];
							if (networkConnection != null)
							{
								if (networkConnection.isReady)
								{
									this.AddObserver(networkConnection);
								}
							}
						}
						for (int k = 0; k < NetworkServer.localConnections.Count; k++)
						{
							NetworkConnection networkConnection2 = NetworkServer.localConnections[k];
							if (networkConnection2 != null)
							{
								if (networkConnection2.isReady)
								{
									this.AddObserver(networkConnection2);
								}
							}
						}
					}
				}
				else
				{
					foreach (NetworkConnection networkConnection3 in hashSet)
					{
						if (networkConnection3 != null)
						{
							if (!networkConnection3.isReady)
							{
								if (LogFilter.logWarn)
								{
									Debug.LogWarning(string.Concat(new object[]
									{
										"Observer is not ready for ",
										base.gameObject,
										" ",
										networkConnection3
									}));
								}
							}
							else if (initialize || !hashSet2.Contains(networkConnection3))
							{
								networkConnection3.AddToVisList(this);
								if (LogFilter.logDebug)
								{
									Debug.Log(string.Concat(new object[]
									{
										"New Observer for ",
										base.gameObject,
										" ",
										networkConnection3
									}));
								}
								flag = true;
							}
						}
					}
					foreach (NetworkConnection networkConnection4 in hashSet2)
					{
						if (!hashSet.Contains(networkConnection4))
						{
							networkConnection4.RemoveFromVisList(this, false);
							if (LogFilter.logDebug)
							{
								Debug.Log(string.Concat(new object[]
								{
									"Removed Observer for ",
									base.gameObject,
									" ",
									networkConnection4
								}));
							}
							flag = true;
						}
					}
					if (initialize)
					{
						for (int l = 0; l < NetworkServer.localConnections.Count; l++)
						{
							if (!hashSet.Contains(NetworkServer.localConnections[l]))
							{
								this.OnSetLocalVisibility(false);
							}
						}
					}
					if (flag)
					{
						this.m_Observers = new List<NetworkConnection>(hashSet);
						this.m_ObserverConnections.Clear();
						for (int m = 0; m < this.m_Observers.Count; m++)
						{
							this.m_ObserverConnections.Add(this.m_Observers[m].connectionId);
						}
					}
				}
			}
		}

		// Token: 0x06000278 RID: 632 RVA: 0x0000D9F0 File Offset: 0x0000BBF0
		public bool RemoveClientAuthority(NetworkConnection conn)
		{
			bool result;
			if (!this.isServer)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("RemoveClientAuthority can only be call on the server for spawned objects.");
				}
				result = false;
			}
			else if (this.connectionToClient != null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("RemoveClientAuthority cannot remove authority for a player object");
				}
				result = false;
			}
			else if (this.m_ClientAuthorityOwner == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("RemoveClientAuthority for " + base.gameObject + " has no clientAuthority owner.");
				}
				result = false;
			}
			else if (this.m_ClientAuthorityOwner != conn)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("RemoveClientAuthority for " + base.gameObject + " has different owner.");
				}
				result = false;
			}
			else
			{
				this.m_ClientAuthorityOwner.RemoveOwnedObject(this);
				this.m_ClientAuthorityOwner = null;
				this.ForceAuthority(true);
				conn.Send(15, new ClientAuthorityMessage
				{
					netId = this.netId,
					authority = false
				});
				if (NetworkIdentity.clientAuthorityCallback != null)
				{
					NetworkIdentity.clientAuthorityCallback(conn, this, false);
				}
				result = true;
			}
			return result;
		}

		// Token: 0x06000279 RID: 633 RVA: 0x0000DB24 File Offset: 0x0000BD24
		public bool AssignClientAuthority(NetworkConnection conn)
		{
			bool result;
			if (!this.isServer)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("AssignClientAuthority can only be call on the server for spawned objects.");
				}
				result = false;
			}
			else if (!this.localPlayerAuthority)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("AssignClientAuthority can only be used for NetworkIdentity component with LocalPlayerAuthority set.");
				}
				result = false;
			}
			else if (this.m_ClientAuthorityOwner != null && conn != this.m_ClientAuthorityOwner)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("AssignClientAuthority for " + base.gameObject + " already has an owner. Use RemoveClientAuthority() first.");
				}
				result = false;
			}
			else if (conn == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("AssignClientAuthority for " + base.gameObject + " owner cannot be null. Use RemoveClientAuthority() instead.");
				}
				result = false;
			}
			else
			{
				this.m_ClientAuthorityOwner = conn;
				this.m_ClientAuthorityOwner.AddOwnedObject(this);
				this.ForceAuthority(false);
				conn.Send(15, new ClientAuthorityMessage
				{
					netId = this.netId,
					authority = true
				});
				if (NetworkIdentity.clientAuthorityCallback != null)
				{
					NetworkIdentity.clientAuthorityCallback(conn, this, true);
				}
				result = true;
			}
			return result;
		}

		// Token: 0x0600027A RID: 634 RVA: 0x0000DC5B File Offset: 0x0000BE5B
		internal void MarkForReset()
		{
			this.m_Reset = true;
		}

		// Token: 0x0600027B RID: 635 RVA: 0x0000DC68 File Offset: 0x0000BE68
		internal void Reset()
		{
			if (this.m_Reset)
			{
				this.m_Reset = false;
				this.m_IsServer = false;
				this.m_IsClient = false;
				this.m_HasAuthority = false;
				this.m_NetId = NetworkInstanceId.Zero;
				this.m_IsLocalPlayer = false;
				this.m_ConnectionToServer = null;
				this.m_ConnectionToClient = null;
				this.m_PlayerId = -1;
				this.m_NetworkBehaviours = null;
				this.ClearObservers();
				this.m_ClientAuthorityOwner = null;
			}
		}

		// Token: 0x0600027C RID: 636 RVA: 0x0000DCDD File Offset: 0x0000BEDD
		internal static void UNetStaticUpdate()
		{
			NetworkServer.Update();
			NetworkClient.UpdateClients();
			NetworkManager.UpdateScene();
		}

		// Token: 0x0600027D RID: 637 RVA: 0x0000DCEF File Offset: 0x0000BEEF
		// Note: this type is marked as 'beforefieldinit'.
		static NetworkIdentity()
		{
		}

		// Token: 0x0400012D RID: 301
		[SerializeField]
		private NetworkSceneId m_SceneId;

		// Token: 0x0400012E RID: 302
		[SerializeField]
		private NetworkHash128 m_AssetId;

		// Token: 0x0400012F RID: 303
		[SerializeField]
		private bool m_ServerOnly;

		// Token: 0x04000130 RID: 304
		[SerializeField]
		private bool m_LocalPlayerAuthority;

		// Token: 0x04000131 RID: 305
		private bool m_IsClient;

		// Token: 0x04000132 RID: 306
		private bool m_IsServer;

		// Token: 0x04000133 RID: 307
		private bool m_HasAuthority;

		// Token: 0x04000134 RID: 308
		private NetworkInstanceId m_NetId;

		// Token: 0x04000135 RID: 309
		private bool m_IsLocalPlayer;

		// Token: 0x04000136 RID: 310
		private NetworkConnection m_ConnectionToServer;

		// Token: 0x04000137 RID: 311
		private NetworkConnection m_ConnectionToClient;

		// Token: 0x04000138 RID: 312
		private short m_PlayerId = -1;

		// Token: 0x04000139 RID: 313
		private NetworkBehaviour[] m_NetworkBehaviours;

		// Token: 0x0400013A RID: 314
		private HashSet<int> m_ObserverConnections;

		// Token: 0x0400013B RID: 315
		private List<NetworkConnection> m_Observers;

		// Token: 0x0400013C RID: 316
		private NetworkConnection m_ClientAuthorityOwner;

		// Token: 0x0400013D RID: 317
		private bool m_Reset = false;

		// Token: 0x0400013E RID: 318
		private static uint s_NextNetworkId = 1U;

		// Token: 0x0400013F RID: 319
		private static NetworkWriter s_UpdateWriter = new NetworkWriter();

		// Token: 0x04000140 RID: 320
		public static NetworkIdentity.ClientAuthorityCallback clientAuthorityCallback;

		// Token: 0x02000048 RID: 72
		// (Invoke) Token: 0x0600027F RID: 639
		public delegate void ClientAuthorityCallback(NetworkConnection conn, NetworkIdentity uv, bool authorityState);
	}
}
