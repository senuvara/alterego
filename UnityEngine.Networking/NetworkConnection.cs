﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnityEngine.Networking
{
	// Token: 0x02000042 RID: 66
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class NetworkConnection : IDisposable
	{
		// Token: 0x060001DF RID: 479 RVA: 0x00006F20 File Offset: 0x00005120
		public NetworkConnection()
		{
			this.m_Writer = new NetworkWriter();
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x060001E0 RID: 480 RVA: 0x00006F8C File Offset: 0x0000518C
		internal HashSet<NetworkIdentity> visList
		{
			get
			{
				return this.m_VisList;
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x060001E1 RID: 481 RVA: 0x00006FA8 File Offset: 0x000051A8
		public List<PlayerController> playerControllers
		{
			get
			{
				return this.m_PlayerControllers;
			}
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x060001E2 RID: 482 RVA: 0x00006FC4 File Offset: 0x000051C4
		public HashSet<NetworkInstanceId> clientOwnedObjects
		{
			get
			{
				return this.m_ClientOwnedObjects;
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x060001E3 RID: 483 RVA: 0x00006FE0 File Offset: 0x000051E0
		public bool isConnected
		{
			get
			{
				return this.hostId != -1;
			}
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x060001E4 RID: 484 RVA: 0x00007004 File Offset: 0x00005204
		// (set) Token: 0x060001E5 RID: 485 RVA: 0x0000701F File Offset: 0x0000521F
		public NetworkError lastError
		{
			get
			{
				return this.error;
			}
			internal set
			{
				this.error = value;
			}
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x060001E6 RID: 486 RVA: 0x0000702C File Offset: 0x0000522C
		internal Dictionary<short, NetworkConnection.PacketStat> packetStats
		{
			get
			{
				return this.m_PacketStats;
			}
		}

		// Token: 0x060001E7 RID: 487 RVA: 0x00007048 File Offset: 0x00005248
		public virtual void Initialize(string networkAddress, int networkHostId, int networkConnectionId, HostTopology hostTopology)
		{
			this.m_Writer = new NetworkWriter();
			this.address = networkAddress;
			this.hostId = networkHostId;
			this.connectionId = networkConnectionId;
			int channelCount = hostTopology.DefaultConfig.ChannelCount;
			int packetSize = (int)hostTopology.DefaultConfig.PacketSize;
			if (hostTopology.DefaultConfig.UsePlatformSpecificProtocols && Application.platform != RuntimePlatform.PS4)
			{
				throw new ArgumentOutOfRangeException("Platform specific protocols are not supported on this platform");
			}
			this.m_Channels = new ChannelBuffer[channelCount];
			for (int i = 0; i < channelCount; i++)
			{
				ChannelQOS channelQOS = hostTopology.DefaultConfig.Channels[i];
				int bufferSize = packetSize;
				if (channelQOS.QOS == QosType.ReliableFragmented || channelQOS.QOS == QosType.UnreliableFragmented)
				{
					bufferSize = (int)(hostTopology.DefaultConfig.FragmentSize * 128);
				}
				this.m_Channels[i] = new ChannelBuffer(this, bufferSize, (byte)i, NetworkConnection.IsReliableQoS(channelQOS.QOS), NetworkConnection.IsSequencedQoS(channelQOS.QOS));
			}
		}

		// Token: 0x060001E8 RID: 488 RVA: 0x00007144 File Offset: 0x00005344
		~NetworkConnection()
		{
			this.Dispose(false);
		}

		// Token: 0x060001E9 RID: 489 RVA: 0x00007178 File Offset: 0x00005378
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x060001EA RID: 490 RVA: 0x00007188 File Offset: 0x00005388
		protected virtual void Dispose(bool disposing)
		{
			if (!this.m_Disposed && this.m_Channels != null)
			{
				for (int i = 0; i < this.m_Channels.Length; i++)
				{
					this.m_Channels[i].Dispose();
				}
			}
			this.m_Channels = null;
			if (this.m_ClientOwnedObjects != null)
			{
				foreach (NetworkInstanceId netId in this.m_ClientOwnedObjects)
				{
					GameObject gameObject = NetworkServer.FindLocalObject(netId);
					if (gameObject != null)
					{
						gameObject.GetComponent<NetworkIdentity>().ClearClientOwner();
					}
				}
			}
			this.m_ClientOwnedObjects = null;
			this.m_Disposed = true;
		}

		// Token: 0x060001EB RID: 491 RVA: 0x00007264 File Offset: 0x00005464
		private static bool IsSequencedQoS(QosType qos)
		{
			return qos == QosType.ReliableSequenced || qos == QosType.UnreliableSequenced;
		}

		// Token: 0x060001EC RID: 492 RVA: 0x00007288 File Offset: 0x00005488
		private static bool IsReliableQoS(QosType qos)
		{
			return qos == QosType.Reliable || qos == QosType.ReliableFragmented || qos == QosType.ReliableSequenced || qos == QosType.ReliableStateUpdate;
		}

		// Token: 0x060001ED RID: 493 RVA: 0x000072BC File Offset: 0x000054BC
		public bool SetChannelOption(int channelId, ChannelOption option, int value)
		{
			return this.m_Channels != null && channelId >= 0 && channelId < this.m_Channels.Length && this.m_Channels[channelId].SetOption(option, value);
		}

		// Token: 0x060001EE RID: 494 RVA: 0x00007310 File Offset: 0x00005510
		public void Disconnect()
		{
			this.address = "";
			this.isReady = false;
			ClientScene.HandleClientDisconnect(this);
			if (this.hostId != -1)
			{
				byte b;
				NetworkManager.activeTransport.Disconnect(this.hostId, this.connectionId, out b);
				this.RemoveObservers();
			}
		}

		// Token: 0x060001EF RID: 495 RVA: 0x00007367 File Offset: 0x00005567
		internal void SetHandlers(NetworkMessageHandlers handlers)
		{
			this.m_MessageHandlers = handlers;
			this.m_MessageHandlersDict = handlers.GetHandlers();
		}

		// Token: 0x060001F0 RID: 496 RVA: 0x00007380 File Offset: 0x00005580
		public bool CheckHandler(short msgType)
		{
			return this.m_MessageHandlersDict.ContainsKey(msgType);
		}

		// Token: 0x060001F1 RID: 497 RVA: 0x000073A4 File Offset: 0x000055A4
		public bool InvokeHandlerNoData(short msgType)
		{
			return this.InvokeHandler(msgType, null, 0);
		}

		// Token: 0x060001F2 RID: 498 RVA: 0x000073C4 File Offset: 0x000055C4
		public bool InvokeHandler(short msgType, NetworkReader reader, int channelId)
		{
			bool result;
			if (this.m_MessageHandlersDict.ContainsKey(msgType))
			{
				this.m_MessageInfo.msgType = msgType;
				this.m_MessageInfo.conn = this;
				this.m_MessageInfo.reader = reader;
				this.m_MessageInfo.channelId = channelId;
				NetworkMessageDelegate networkMessageDelegate = this.m_MessageHandlersDict[msgType];
				if (networkMessageDelegate == null)
				{
					if (LogFilter.logError)
					{
						Debug.LogError("NetworkConnection InvokeHandler no handler for " + msgType);
					}
					result = false;
				}
				else
				{
					networkMessageDelegate(this.m_MessageInfo);
					result = true;
				}
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x060001F3 RID: 499 RVA: 0x0000746C File Offset: 0x0000566C
		public bool InvokeHandler(NetworkMessage netMsg)
		{
			bool result;
			if (this.m_MessageHandlersDict.ContainsKey(netMsg.msgType))
			{
				NetworkMessageDelegate networkMessageDelegate = this.m_MessageHandlersDict[netMsg.msgType];
				networkMessageDelegate(netMsg);
				result = true;
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x060001F4 RID: 500 RVA: 0x000074BC File Offset: 0x000056BC
		internal void HandleFragment(NetworkReader reader, int channelId)
		{
			if (channelId >= 0 && channelId < this.m_Channels.Length)
			{
				ChannelBuffer channelBuffer = this.m_Channels[channelId];
				if (channelBuffer.HandleFragment(reader))
				{
					NetworkReader networkReader = new NetworkReader(channelBuffer.fragmentBuffer.AsArraySegment().Array);
					networkReader.ReadInt16();
					short msgType = networkReader.ReadInt16();
					this.InvokeHandler(msgType, networkReader, channelId);
				}
			}
		}

		// Token: 0x060001F5 RID: 501 RVA: 0x0000752D File Offset: 0x0000572D
		public void RegisterHandler(short msgType, NetworkMessageDelegate handler)
		{
			this.m_MessageHandlers.RegisterHandler(msgType, handler);
		}

		// Token: 0x060001F6 RID: 502 RVA: 0x0000753D File Offset: 0x0000573D
		public void UnregisterHandler(short msgType)
		{
			this.m_MessageHandlers.UnregisterHandler(msgType);
		}

		// Token: 0x060001F7 RID: 503 RVA: 0x0000754C File Offset: 0x0000574C
		internal void SetPlayerController(PlayerController player)
		{
			while ((int)player.playerControllerId >= this.m_PlayerControllers.Count)
			{
				this.m_PlayerControllers.Add(new PlayerController());
			}
			this.m_PlayerControllers[(int)player.playerControllerId] = player;
		}

		// Token: 0x060001F8 RID: 504 RVA: 0x0000759C File Offset: 0x0000579C
		internal void RemovePlayerController(short playerControllerId)
		{
			for (int i = this.m_PlayerControllers.Count; i >= 0; i--)
			{
				if ((int)playerControllerId == i && playerControllerId == this.m_PlayerControllers[i].playerControllerId)
				{
					this.m_PlayerControllers[i] = new PlayerController();
					return;
				}
			}
			if (LogFilter.logError)
			{
				Debug.LogError("RemovePlayer player at playerControllerId " + playerControllerId + " not found");
				return;
			}
		}

		// Token: 0x060001F9 RID: 505 RVA: 0x00007624 File Offset: 0x00005824
		internal bool GetPlayerController(short playerControllerId, out PlayerController playerController)
		{
			playerController = null;
			bool result;
			if (this.playerControllers.Count > 0)
			{
				for (int i = 0; i < this.playerControllers.Count; i++)
				{
					if (this.playerControllers[i].IsValid && this.playerControllers[i].playerControllerId == playerControllerId)
					{
						playerController = this.playerControllers[i];
						return true;
					}
				}
				result = false;
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x060001FA RID: 506 RVA: 0x000076B8 File Offset: 0x000058B8
		public void FlushChannels()
		{
			if (this.m_Channels != null)
			{
				for (int i = 0; i < this.m_Channels.Length; i++)
				{
					this.m_Channels[i].CheckInternalBuffer();
				}
			}
		}

		// Token: 0x060001FB RID: 507 RVA: 0x00007700 File Offset: 0x00005900
		public void SetMaxDelay(float seconds)
		{
			if (this.m_Channels != null)
			{
				for (int i = 0; i < this.m_Channels.Length; i++)
				{
					this.m_Channels[i].maxDelay = seconds;
				}
			}
		}

		// Token: 0x060001FC RID: 508 RVA: 0x00007748 File Offset: 0x00005948
		public virtual bool Send(short msgType, MessageBase msg)
		{
			return this.SendByChannel(msgType, msg, 0);
		}

		// Token: 0x060001FD RID: 509 RVA: 0x00007768 File Offset: 0x00005968
		public virtual bool SendUnreliable(short msgType, MessageBase msg)
		{
			return this.SendByChannel(msgType, msg, 1);
		}

		// Token: 0x060001FE RID: 510 RVA: 0x00007788 File Offset: 0x00005988
		public virtual bool SendByChannel(short msgType, MessageBase msg, int channelId)
		{
			this.m_Writer.StartMessage(msgType);
			msg.Serialize(this.m_Writer);
			this.m_Writer.FinishMessage();
			return this.SendWriter(this.m_Writer, channelId);
		}

		// Token: 0x060001FF RID: 511 RVA: 0x000077D0 File Offset: 0x000059D0
		public virtual bool SendBytes(byte[] bytes, int numBytes, int channelId)
		{
			if (this.logNetworkMessages)
			{
				this.LogSend(bytes);
			}
			return this.CheckChannel(channelId) && this.m_Channels[channelId].SendBytes(bytes, numBytes);
		}

		// Token: 0x06000200 RID: 512 RVA: 0x00007818 File Offset: 0x00005A18
		public virtual bool SendWriter(NetworkWriter writer, int channelId)
		{
			if (this.logNetworkMessages)
			{
				this.LogSend(writer.ToArray());
			}
			return this.CheckChannel(channelId) && this.m_Channels[channelId].SendWriter(writer);
		}

		// Token: 0x06000201 RID: 513 RVA: 0x00007864 File Offset: 0x00005A64
		private void LogSend(byte[] bytes)
		{
			NetworkReader networkReader = new NetworkReader(bytes);
			ushort num = networkReader.ReadUInt16();
			ushort num2 = networkReader.ReadUInt16();
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 4; i < (int)(4 + num); i++)
			{
				stringBuilder.AppendFormat("{0:X2}", bytes[i]);
				if (i > 150)
				{
					break;
				}
			}
			Debug.Log(string.Concat(new object[]
			{
				"ConnectionSend con:",
				this.connectionId,
				" bytes:",
				num,
				" msgId:",
				num2,
				" ",
				stringBuilder
			}));
		}

		// Token: 0x06000202 RID: 514 RVA: 0x00007924 File Offset: 0x00005B24
		private bool CheckChannel(int channelId)
		{
			bool result;
			if (this.m_Channels == null)
			{
				if (LogFilter.logWarn)
				{
					Debug.LogWarning("Channels not initialized sending on id '" + channelId);
				}
				result = false;
			}
			else if (channelId < 0 || channelId >= this.m_Channels.Length)
			{
				if (LogFilter.logError)
				{
					Debug.LogError(string.Concat(new object[]
					{
						"Invalid channel when sending buffered data, '",
						channelId,
						"'. Current channel count is ",
						this.m_Channels.Length
					}));
				}
				result = false;
			}
			else
			{
				result = true;
			}
			return result;
		}

		// Token: 0x06000203 RID: 515 RVA: 0x000079D0 File Offset: 0x00005BD0
		public void ResetStats()
		{
		}

		// Token: 0x06000204 RID: 516 RVA: 0x000079D4 File Offset: 0x00005BD4
		protected void HandleBytes(byte[] buffer, int receivedSize, int channelId)
		{
			NetworkReader reader = new NetworkReader(buffer);
			this.HandleReader(reader, receivedSize, channelId);
		}

		// Token: 0x06000205 RID: 517 RVA: 0x000079F4 File Offset: 0x00005BF4
		protected void HandleReader(NetworkReader reader, int receivedSize, int channelId)
		{
			while ((ulong)reader.Position < (ulong)((long)receivedSize))
			{
				ushort num = reader.ReadUInt16();
				short num2 = reader.ReadInt16();
				byte[] array = reader.ReadBytes((int)num);
				NetworkReader reader2 = new NetworkReader(array);
				if (this.logNetworkMessages)
				{
					StringBuilder stringBuilder = new StringBuilder();
					for (int i = 0; i < (int)num; i++)
					{
						stringBuilder.AppendFormat("{0:X2}", array[i]);
						if (i > 150)
						{
							break;
						}
					}
					Debug.Log(string.Concat(new object[]
					{
						"ConnectionRecv con:",
						this.connectionId,
						" bytes:",
						num,
						" msgId:",
						num2,
						" ",
						stringBuilder
					}));
				}
				NetworkMessageDelegate networkMessageDelegate = null;
				if (this.m_MessageHandlersDict.ContainsKey(num2))
				{
					networkMessageDelegate = this.m_MessageHandlersDict[num2];
				}
				if (networkMessageDelegate == null)
				{
					if (LogFilter.logError)
					{
						Debug.LogError(string.Concat(new object[]
						{
							"Unknown message ID ",
							num2,
							" connId:",
							this.connectionId
						}));
					}
					break;
				}
				this.m_NetMsg.msgType = num2;
				this.m_NetMsg.reader = reader2;
				this.m_NetMsg.conn = this;
				this.m_NetMsg.channelId = channelId;
				networkMessageDelegate(this.m_NetMsg);
				this.lastMessageTime = Time.time;
			}
		}

		// Token: 0x06000206 RID: 518 RVA: 0x00007BA0 File Offset: 0x00005DA0
		public virtual void GetStatsOut(out int numMsgs, out int numBufferedMsgs, out int numBytes, out int lastBufferedPerSecond)
		{
			numMsgs = 0;
			numBufferedMsgs = 0;
			numBytes = 0;
			lastBufferedPerSecond = 0;
			for (int i = 0; i < this.m_Channels.Length; i++)
			{
				ChannelBuffer channelBuffer = this.m_Channels[i];
				numMsgs += channelBuffer.numMsgsOut;
				numBufferedMsgs += channelBuffer.numBufferedMsgsOut;
				numBytes += channelBuffer.numBytesOut;
				lastBufferedPerSecond += channelBuffer.lastBufferedPerSecond;
			}
		}

		// Token: 0x06000207 RID: 519 RVA: 0x00007C10 File Offset: 0x00005E10
		public virtual void GetStatsIn(out int numMsgs, out int numBytes)
		{
			numMsgs = 0;
			numBytes = 0;
			for (int i = 0; i < this.m_Channels.Length; i++)
			{
				ChannelBuffer channelBuffer = this.m_Channels[i];
				numMsgs += channelBuffer.numMsgsIn;
				numBytes += channelBuffer.numBytesIn;
			}
		}

		// Token: 0x06000208 RID: 520 RVA: 0x00007C60 File Offset: 0x00005E60
		public override string ToString()
		{
			return string.Format("hostId: {0} connectionId: {1} isReady: {2} channel count: {3}", new object[]
			{
				this.hostId,
				this.connectionId,
				this.isReady,
				(this.m_Channels == null) ? 0 : this.m_Channels.Length
			});
		}

		// Token: 0x06000209 RID: 521 RVA: 0x00007CD0 File Offset: 0x00005ED0
		internal void AddToVisList(NetworkIdentity uv)
		{
			this.m_VisList.Add(uv);
			NetworkServer.ShowForConnection(uv, this);
		}

		// Token: 0x0600020A RID: 522 RVA: 0x00007CE7 File Offset: 0x00005EE7
		internal void RemoveFromVisList(NetworkIdentity uv, bool isDestroyed)
		{
			this.m_VisList.Remove(uv);
			if (!isDestroyed)
			{
				NetworkServer.HideForConnection(uv, this);
			}
		}

		// Token: 0x0600020B RID: 523 RVA: 0x00007D08 File Offset: 0x00005F08
		internal void RemoveObservers()
		{
			foreach (NetworkIdentity networkIdentity in this.m_VisList)
			{
				networkIdentity.RemoveObserverInternal(this);
			}
			this.m_VisList.Clear();
		}

		// Token: 0x0600020C RID: 524 RVA: 0x00007D74 File Offset: 0x00005F74
		public virtual void TransportReceive(byte[] bytes, int numBytes, int channelId)
		{
			this.HandleBytes(bytes, numBytes, channelId);
		}

		// Token: 0x0600020D RID: 525 RVA: 0x00007D80 File Offset: 0x00005F80
		[Obsolete("TransportRecieve has been deprecated. Use TransportReceive instead.", false)]
		public virtual void TransportRecieve(byte[] bytes, int numBytes, int channelId)
		{
			this.TransportReceive(bytes, numBytes, channelId);
		}

		// Token: 0x0600020E RID: 526 RVA: 0x00007D8C File Offset: 0x00005F8C
		public virtual bool TransportSend(byte[] bytes, int numBytes, int channelId, out byte error)
		{
			return NetworkManager.activeTransport.Send(this.hostId, this.connectionId, channelId, bytes, numBytes, out error);
		}

		// Token: 0x0600020F RID: 527 RVA: 0x00007DBC File Offset: 0x00005FBC
		internal void AddOwnedObject(NetworkIdentity obj)
		{
			if (this.m_ClientOwnedObjects == null)
			{
				this.m_ClientOwnedObjects = new HashSet<NetworkInstanceId>();
			}
			this.m_ClientOwnedObjects.Add(obj.netId);
		}

		// Token: 0x06000210 RID: 528 RVA: 0x00007DE9 File Offset: 0x00005FE9
		internal void RemoveOwnedObject(NetworkIdentity obj)
		{
			if (this.m_ClientOwnedObjects != null)
			{
				this.m_ClientOwnedObjects.Remove(obj.netId);
			}
		}

		// Token: 0x06000211 RID: 529 RVA: 0x00007E0F File Offset: 0x0000600F
		internal static void OnFragment(NetworkMessage netMsg)
		{
			netMsg.conn.HandleFragment(netMsg.reader, netMsg.channelId);
		}

		// Token: 0x040000F2 RID: 242
		private ChannelBuffer[] m_Channels;

		// Token: 0x040000F3 RID: 243
		private List<PlayerController> m_PlayerControllers = new List<PlayerController>();

		// Token: 0x040000F4 RID: 244
		private NetworkMessage m_NetMsg = new NetworkMessage();

		// Token: 0x040000F5 RID: 245
		private HashSet<NetworkIdentity> m_VisList = new HashSet<NetworkIdentity>();

		// Token: 0x040000F6 RID: 246
		private NetworkWriter m_Writer;

		// Token: 0x040000F7 RID: 247
		private Dictionary<short, NetworkMessageDelegate> m_MessageHandlersDict;

		// Token: 0x040000F8 RID: 248
		private NetworkMessageHandlers m_MessageHandlers;

		// Token: 0x040000F9 RID: 249
		private HashSet<NetworkInstanceId> m_ClientOwnedObjects;

		// Token: 0x040000FA RID: 250
		private NetworkMessage m_MessageInfo = new NetworkMessage();

		// Token: 0x040000FB RID: 251
		private const int k_MaxMessageLogSize = 150;

		// Token: 0x040000FC RID: 252
		private NetworkError error;

		// Token: 0x040000FD RID: 253
		public int hostId = -1;

		// Token: 0x040000FE RID: 254
		public int connectionId = -1;

		// Token: 0x040000FF RID: 255
		public bool isReady;

		// Token: 0x04000100 RID: 256
		public string address;

		// Token: 0x04000101 RID: 257
		public float lastMessageTime;

		// Token: 0x04000102 RID: 258
		public bool logNetworkMessages = false;

		// Token: 0x04000103 RID: 259
		private Dictionary<short, NetworkConnection.PacketStat> m_PacketStats = new Dictionary<short, NetworkConnection.PacketStat>();

		// Token: 0x04000104 RID: 260
		private bool m_Disposed;

		// Token: 0x02000043 RID: 67
		public class PacketStat
		{
			// Token: 0x06000212 RID: 530 RVA: 0x00007E29 File Offset: 0x00006029
			public PacketStat()
			{
				this.msgType = 0;
				this.count = 0;
				this.bytes = 0;
			}

			// Token: 0x06000213 RID: 531 RVA: 0x00007E47 File Offset: 0x00006047
			public PacketStat(NetworkConnection.PacketStat s)
			{
				this.msgType = s.msgType;
				this.count = s.count;
				this.bytes = s.bytes;
			}

			// Token: 0x06000214 RID: 532 RVA: 0x00007E74 File Offset: 0x00006074
			public override string ToString()
			{
				return string.Concat(new object[]
				{
					MsgType.MsgTypeToString(this.msgType),
					": count=",
					this.count,
					" bytes=",
					this.bytes
				});
			}

			// Token: 0x04000105 RID: 261
			public short msgType;

			// Token: 0x04000106 RID: 262
			public int count;

			// Token: 0x04000107 RID: 263
			public int bytes;
		}
	}
}
