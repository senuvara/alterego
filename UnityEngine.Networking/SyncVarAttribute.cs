﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000008 RID: 8
	[AttributeUsage(AttributeTargets.Field)]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class SyncVarAttribute : Attribute
	{
		// Token: 0x06000067 RID: 103 RVA: 0x00004C3E File Offset: 0x00002E3E
		public SyncVarAttribute()
		{
		}

		// Token: 0x0400004C RID: 76
		public string hook;
	}
}
