﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x02000027 RID: 39
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class ReconnectMessage : MessageBase
	{
		// Token: 0x060000F2 RID: 242 RVA: 0x0000865E File Offset: 0x0000685E
		public ReconnectMessage()
		{
		}

		// Token: 0x060000F3 RID: 243 RVA: 0x00008668 File Offset: 0x00006868
		public override void Deserialize(NetworkReader reader)
		{
			this.oldConnectionId = (int)reader.ReadPackedUInt32();
			this.playerControllerId = (short)reader.ReadPackedUInt32();
			this.netId = reader.ReadNetworkId();
			this.msgData = reader.ReadBytesAndSize();
			this.msgSize = this.msgData.Length;
		}

		// Token: 0x060000F4 RID: 244 RVA: 0x000086B5 File Offset: 0x000068B5
		public override void Serialize(NetworkWriter writer)
		{
			writer.WritePackedUInt32((uint)this.oldConnectionId);
			writer.WritePackedUInt32((uint)this.playerControllerId);
			writer.Write(this.netId);
			writer.WriteBytesAndSize(this.msgData, this.msgSize);
		}

		// Token: 0x04000081 RID: 129
		public int oldConnectionId;

		// Token: 0x04000082 RID: 130
		public short playerControllerId;

		// Token: 0x04000083 RID: 131
		public NetworkInstanceId netId;

		// Token: 0x04000084 RID: 132
		public int msgSize;

		// Token: 0x04000085 RID: 133
		public byte[] msgData;
	}
}
