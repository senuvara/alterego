﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x02000034 RID: 52
	internal class CRCMessage : MessageBase
	{
		// Token: 0x06000116 RID: 278 RVA: 0x00008AC2 File Offset: 0x00006CC2
		public CRCMessage()
		{
		}

		// Token: 0x06000117 RID: 279 RVA: 0x00008ACC File Offset: 0x00006CCC
		public override void Deserialize(NetworkReader reader)
		{
			int num = (int)reader.ReadUInt16();
			this.scripts = new CRCMessageEntry[num];
			for (int i = 0; i < this.scripts.Length; i++)
			{
				CRCMessageEntry crcmessageEntry = default(CRCMessageEntry);
				crcmessageEntry.name = reader.ReadString();
				crcmessageEntry.channel = reader.ReadByte();
				this.scripts[i] = crcmessageEntry;
			}
		}

		// Token: 0x06000118 RID: 280 RVA: 0x00008B3C File Offset: 0x00006D3C
		public override void Serialize(NetworkWriter writer)
		{
			writer.Write((ushort)this.scripts.Length);
			for (int i = 0; i < this.scripts.Length; i++)
			{
				writer.Write(this.scripts[i].name);
				writer.Write(this.scripts[i].channel);
			}
		}

		// Token: 0x040000A5 RID: 165
		public CRCMessageEntry[] scripts;
	}
}
