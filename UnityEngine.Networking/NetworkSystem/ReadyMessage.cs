﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x0200001F RID: 31
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class ReadyMessage : EmptyMessage
	{
		// Token: 0x060000E0 RID: 224 RVA: 0x000082A3 File Offset: 0x000064A3
		public ReadyMessage()
		{
		}
	}
}
