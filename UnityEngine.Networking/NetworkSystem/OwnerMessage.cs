﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x0200002C RID: 44
	internal class OwnerMessage : MessageBase
	{
		// Token: 0x06000101 RID: 257 RVA: 0x00008856 File Offset: 0x00006A56
		public OwnerMessage()
		{
		}

		// Token: 0x06000102 RID: 258 RVA: 0x0000885E File Offset: 0x00006A5E
		public override void Deserialize(NetworkReader reader)
		{
			this.netId = reader.ReadNetworkId();
			this.playerControllerId = (short)reader.ReadPackedUInt32();
		}

		// Token: 0x06000103 RID: 259 RVA: 0x0000887A File Offset: 0x00006A7A
		public override void Serialize(NetworkWriter writer)
		{
			writer.Write(this.netId);
			writer.WritePackedUInt32((uint)this.playerControllerId);
		}

		// Token: 0x04000091 RID: 145
		public NetworkInstanceId netId;

		// Token: 0x04000092 RID: 146
		public short playerControllerId;
	}
}
