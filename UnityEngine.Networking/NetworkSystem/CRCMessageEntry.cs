﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x02000033 RID: 51
	internal struct CRCMessageEntry
	{
		// Token: 0x040000A3 RID: 163
		public string name;

		// Token: 0x040000A4 RID: 164
		public byte channel;
	}
}
