﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x02000022 RID: 34
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class RemovePlayerMessage : MessageBase
	{
		// Token: 0x060000E5 RID: 229 RVA: 0x0000832E File Offset: 0x0000652E
		public RemovePlayerMessage()
		{
		}

		// Token: 0x060000E6 RID: 230 RVA: 0x00008336 File Offset: 0x00006536
		public override void Deserialize(NetworkReader reader)
		{
			this.playerControllerId = (short)reader.ReadUInt16();
		}

		// Token: 0x060000E7 RID: 231 RVA: 0x00008346 File Offset: 0x00006546
		public override void Serialize(NetworkWriter writer)
		{
			writer.Write((ushort)this.playerControllerId);
		}

		// Token: 0x04000073 RID: 115
		public short playerControllerId;
	}
}
