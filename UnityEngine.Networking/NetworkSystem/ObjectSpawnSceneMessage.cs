﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x02000029 RID: 41
	internal class ObjectSpawnSceneMessage : MessageBase
	{
		// Token: 0x060000F8 RID: 248 RVA: 0x0000879C File Offset: 0x0000699C
		public ObjectSpawnSceneMessage()
		{
		}

		// Token: 0x060000F9 RID: 249 RVA: 0x000087A4 File Offset: 0x000069A4
		public override void Deserialize(NetworkReader reader)
		{
			this.netId = reader.ReadNetworkId();
			this.sceneId = reader.ReadSceneId();
			this.position = reader.ReadVector3();
			this.payload = reader.ReadBytesAndSize();
		}

		// Token: 0x060000FA RID: 250 RVA: 0x000087D7 File Offset: 0x000069D7
		public override void Serialize(NetworkWriter writer)
		{
			writer.Write(this.netId);
			writer.Write(this.sceneId);
			writer.Write(this.position);
			writer.WriteBytesFull(this.payload);
		}

		// Token: 0x0400008B RID: 139
		public NetworkInstanceId netId;

		// Token: 0x0400008C RID: 140
		public NetworkSceneId sceneId;

		// Token: 0x0400008D RID: 141
		public Vector3 position;

		// Token: 0x0400008E RID: 142
		public byte[] payload;
	}
}
