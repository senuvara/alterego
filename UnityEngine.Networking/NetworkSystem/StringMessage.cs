﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x0200001B RID: 27
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class StringMessage : MessageBase
	{
		// Token: 0x060000D2 RID: 210 RVA: 0x00008200 File Offset: 0x00006400
		public StringMessage()
		{
		}

		// Token: 0x060000D3 RID: 211 RVA: 0x00008209 File Offset: 0x00006409
		public StringMessage(string v)
		{
			this.value = v;
		}

		// Token: 0x060000D4 RID: 212 RVA: 0x00008219 File Offset: 0x00006419
		public override void Deserialize(NetworkReader reader)
		{
			this.value = reader.ReadString();
		}

		// Token: 0x060000D5 RID: 213 RVA: 0x00008228 File Offset: 0x00006428
		public override void Serialize(NetworkWriter writer)
		{
			writer.Write(this.value);
		}

		// Token: 0x0400006D RID: 109
		public string value;
	}
}
