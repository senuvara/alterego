﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x0200001D RID: 29
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class EmptyMessage : MessageBase
	{
		// Token: 0x060000DA RID: 218 RVA: 0x0000826E File Offset: 0x0000646E
		public EmptyMessage()
		{
		}

		// Token: 0x060000DB RID: 219 RVA: 0x00008276 File Offset: 0x00006476
		public override void Deserialize(NetworkReader reader)
		{
		}

		// Token: 0x060000DC RID: 220 RVA: 0x00008279 File Offset: 0x00006479
		public override void Serialize(NetworkWriter writer)
		{
		}
	}
}
