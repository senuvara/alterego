﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x02000021 RID: 33
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class AddPlayerMessage : MessageBase
	{
		// Token: 0x060000E2 RID: 226 RVA: 0x000082B3 File Offset: 0x000064B3
		public AddPlayerMessage()
		{
		}

		// Token: 0x060000E3 RID: 227 RVA: 0x000082BC File Offset: 0x000064BC
		public override void Deserialize(NetworkReader reader)
		{
			this.playerControllerId = (short)reader.ReadUInt16();
			this.msgData = reader.ReadBytesAndSize();
			if (this.msgData == null)
			{
				this.msgSize = 0;
			}
			else
			{
				this.msgSize = this.msgData.Length;
			}
		}

		// Token: 0x060000E4 RID: 228 RVA: 0x0000830C File Offset: 0x0000650C
		public override void Serialize(NetworkWriter writer)
		{
			writer.Write((ushort)this.playerControllerId);
			writer.WriteBytesAndSize(this.msgData, this.msgSize);
		}

		// Token: 0x04000070 RID: 112
		public short playerControllerId;

		// Token: 0x04000071 RID: 113
		public int msgSize;

		// Token: 0x04000072 RID: 114
		public byte[] msgData;
	}
}
