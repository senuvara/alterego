﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x02000025 RID: 37
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class PeerInfoMessage : MessageBase
	{
		// Token: 0x060000EB RID: 235 RVA: 0x000083AC File Offset: 0x000065AC
		public PeerInfoMessage()
		{
		}

		// Token: 0x060000EC RID: 236 RVA: 0x000083B4 File Offset: 0x000065B4
		public override void Deserialize(NetworkReader reader)
		{
			this.connectionId = (int)reader.ReadPackedUInt32();
			this.address = reader.ReadString();
			this.port = (int)reader.ReadPackedUInt32();
			this.isHost = reader.ReadBoolean();
			this.isYou = reader.ReadBoolean();
			uint num = reader.ReadPackedUInt32();
			if (num > 0U)
			{
				List<PeerInfoPlayer> list = new List<PeerInfoPlayer>();
				for (uint num2 = 0U; num2 < num; num2 += 1U)
				{
					PeerInfoPlayer item;
					item.netId = reader.ReadNetworkId();
					item.playerControllerId = (short)reader.ReadPackedUInt32();
					list.Add(item);
				}
				this.playerIds = list.ToArray();
			}
		}

		// Token: 0x060000ED RID: 237 RVA: 0x00008458 File Offset: 0x00006658
		public override void Serialize(NetworkWriter writer)
		{
			writer.WritePackedUInt32((uint)this.connectionId);
			writer.Write(this.address);
			writer.WritePackedUInt32((uint)this.port);
			writer.Write(this.isHost);
			writer.Write(this.isYou);
			if (this.playerIds == null)
			{
				writer.WritePackedUInt32(0U);
			}
			else
			{
				writer.WritePackedUInt32((uint)this.playerIds.Length);
				for (int i = 0; i < this.playerIds.Length; i++)
				{
					writer.Write(this.playerIds[i].netId);
					writer.WritePackedUInt32((uint)this.playerIds[i].playerControllerId);
				}
			}
		}

		// Token: 0x060000EE RID: 238 RVA: 0x00008518 File Offset: 0x00006718
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				"PeerInfo conn:",
				this.connectionId,
				" addr:",
				this.address,
				":",
				this.port,
				" host:",
				this.isHost,
				" isYou:",
				this.isYou
			});
		}

		// Token: 0x04000079 RID: 121
		public int connectionId;

		// Token: 0x0400007A RID: 122
		public string address;

		// Token: 0x0400007B RID: 123
		public int port;

		// Token: 0x0400007C RID: 124
		public bool isHost;

		// Token: 0x0400007D RID: 125
		public bool isYou;

		// Token: 0x0400007E RID: 126
		public PeerInfoPlayer[] playerIds;
	}
}
