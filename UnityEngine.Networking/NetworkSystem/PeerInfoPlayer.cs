﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x02000024 RID: 36
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public struct PeerInfoPlayer
	{
		// Token: 0x04000077 RID: 119
		public NetworkInstanceId netId;

		// Token: 0x04000078 RID: 120
		public short playerControllerId;
	}
}
