﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x0200001E RID: 30
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class ErrorMessage : MessageBase
	{
		// Token: 0x060000DD RID: 221 RVA: 0x0000827C File Offset: 0x0000647C
		public ErrorMessage()
		{
		}

		// Token: 0x060000DE RID: 222 RVA: 0x00008284 File Offset: 0x00006484
		public override void Deserialize(NetworkReader reader)
		{
			this.errorCode = (int)reader.ReadUInt16();
		}

		// Token: 0x060000DF RID: 223 RVA: 0x00008293 File Offset: 0x00006493
		public override void Serialize(NetworkWriter writer)
		{
			writer.Write((ushort)this.errorCode);
		}

		// Token: 0x0400006F RID: 111
		public int errorCode;
	}
}
