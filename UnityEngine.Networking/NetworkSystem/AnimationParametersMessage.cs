﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x02000030 RID: 48
	internal class AnimationParametersMessage : MessageBase
	{
		// Token: 0x0600010D RID: 269 RVA: 0x000089E3 File Offset: 0x00006BE3
		public AnimationParametersMessage()
		{
		}

		// Token: 0x0600010E RID: 270 RVA: 0x000089EB File Offset: 0x00006BEB
		public override void Deserialize(NetworkReader reader)
		{
			this.netId = reader.ReadNetworkId();
			this.parameters = reader.ReadBytesAndSize();
		}

		// Token: 0x0600010F RID: 271 RVA: 0x00008A06 File Offset: 0x00006C06
		public override void Serialize(NetworkWriter writer)
		{
			writer.Write(this.netId);
			if (this.parameters == null)
			{
				writer.WriteBytesAndSize(this.parameters, 0);
			}
			else
			{
				writer.WriteBytesAndSize(this.parameters, this.parameters.Length);
			}
		}

		// Token: 0x0400009D RID: 157
		public NetworkInstanceId netId;

		// Token: 0x0400009E RID: 158
		public byte[] parameters;
	}
}
