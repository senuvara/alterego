﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x02000026 RID: 38
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class PeerListMessage : MessageBase
	{
		// Token: 0x060000EF RID: 239 RVA: 0x000085A3 File Offset: 0x000067A3
		public PeerListMessage()
		{
		}

		// Token: 0x060000F0 RID: 240 RVA: 0x000085AC File Offset: 0x000067AC
		public override void Deserialize(NetworkReader reader)
		{
			this.oldServerConnectionId = (int)reader.ReadPackedUInt32();
			int num = (int)reader.ReadUInt16();
			this.peers = new PeerInfoMessage[num];
			for (int i = 0; i < this.peers.Length; i++)
			{
				PeerInfoMessage peerInfoMessage = new PeerInfoMessage();
				peerInfoMessage.Deserialize(reader);
				this.peers[i] = peerInfoMessage;
			}
		}

		// Token: 0x060000F1 RID: 241 RVA: 0x0000860C File Offset: 0x0000680C
		public override void Serialize(NetworkWriter writer)
		{
			writer.WritePackedUInt32((uint)this.oldServerConnectionId);
			writer.Write((ushort)this.peers.Length);
			for (int i = 0; i < this.peers.Length; i++)
			{
				this.peers[i].Serialize(writer);
			}
		}

		// Token: 0x0400007F RID: 127
		public PeerInfoMessage[] peers;

		// Token: 0x04000080 RID: 128
		public int oldServerConnectionId;
	}
}
