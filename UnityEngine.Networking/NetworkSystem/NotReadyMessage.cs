﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x02000020 RID: 32
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class NotReadyMessage : EmptyMessage
	{
		// Token: 0x060000E1 RID: 225 RVA: 0x000082AB File Offset: 0x000064AB
		public NotReadyMessage()
		{
		}
	}
}
