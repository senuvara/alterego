﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x02000023 RID: 35
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class PeerAuthorityMessage : MessageBase
	{
		// Token: 0x060000E8 RID: 232 RVA: 0x00008356 File Offset: 0x00006556
		public PeerAuthorityMessage()
		{
		}

		// Token: 0x060000E9 RID: 233 RVA: 0x0000835E File Offset: 0x0000655E
		public override void Deserialize(NetworkReader reader)
		{
			this.connectionId = (int)reader.ReadPackedUInt32();
			this.netId = reader.ReadNetworkId();
			this.authorityState = reader.ReadBoolean();
		}

		// Token: 0x060000EA RID: 234 RVA: 0x00008385 File Offset: 0x00006585
		public override void Serialize(NetworkWriter writer)
		{
			writer.WritePackedUInt32((uint)this.connectionId);
			writer.Write(this.netId);
			writer.Write(this.authorityState);
		}

		// Token: 0x04000074 RID: 116
		public int connectionId;

		// Token: 0x04000075 RID: 117
		public NetworkInstanceId netId;

		// Token: 0x04000076 RID: 118
		public bool authorityState;
	}
}
