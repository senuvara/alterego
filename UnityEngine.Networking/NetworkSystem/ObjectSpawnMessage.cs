﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x02000028 RID: 40
	internal class ObjectSpawnMessage : MessageBase
	{
		// Token: 0x060000F5 RID: 245 RVA: 0x000086EF File Offset: 0x000068EF
		public ObjectSpawnMessage()
		{
		}

		// Token: 0x060000F6 RID: 246 RVA: 0x000086F8 File Offset: 0x000068F8
		public override void Deserialize(NetworkReader reader)
		{
			this.netId = reader.ReadNetworkId();
			this.assetId = reader.ReadNetworkHash128();
			this.position = reader.ReadVector3();
			this.payload = reader.ReadBytesAndSize();
			uint num = 16U;
			if ((long)reader.Length - (long)((ulong)reader.Position) >= (long)((ulong)num))
			{
				this.rotation = reader.ReadQuaternion();
			}
		}

		// Token: 0x060000F7 RID: 247 RVA: 0x0000875D File Offset: 0x0000695D
		public override void Serialize(NetworkWriter writer)
		{
			writer.Write(this.netId);
			writer.Write(this.assetId);
			writer.Write(this.position);
			writer.WriteBytesFull(this.payload);
			writer.Write(this.rotation);
		}

		// Token: 0x04000086 RID: 134
		public NetworkInstanceId netId;

		// Token: 0x04000087 RID: 135
		public NetworkHash128 assetId;

		// Token: 0x04000088 RID: 136
		public Vector3 position;

		// Token: 0x04000089 RID: 137
		public byte[] payload;

		// Token: 0x0400008A RID: 138
		public Quaternion rotation;
	}
}
