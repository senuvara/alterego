﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x0200002B RID: 43
	internal class ObjectDestroyMessage : MessageBase
	{
		// Token: 0x060000FE RID: 254 RVA: 0x00008830 File Offset: 0x00006A30
		public ObjectDestroyMessage()
		{
		}

		// Token: 0x060000FF RID: 255 RVA: 0x00008838 File Offset: 0x00006A38
		public override void Deserialize(NetworkReader reader)
		{
			this.netId = reader.ReadNetworkId();
		}

		// Token: 0x06000100 RID: 256 RVA: 0x00008847 File Offset: 0x00006A47
		public override void Serialize(NetworkWriter writer)
		{
			writer.Write(this.netId);
		}

		// Token: 0x04000090 RID: 144
		public NetworkInstanceId netId;
	}
}
