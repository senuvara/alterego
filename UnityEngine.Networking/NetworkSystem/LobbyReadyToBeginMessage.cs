﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x02000032 RID: 50
	internal class LobbyReadyToBeginMessage : MessageBase
	{
		// Token: 0x06000113 RID: 275 RVA: 0x00008A84 File Offset: 0x00006C84
		public LobbyReadyToBeginMessage()
		{
		}

		// Token: 0x06000114 RID: 276 RVA: 0x00008A8C File Offset: 0x00006C8C
		public override void Deserialize(NetworkReader reader)
		{
			this.slotId = reader.ReadByte();
			this.readyState = reader.ReadBoolean();
		}

		// Token: 0x06000115 RID: 277 RVA: 0x00008AA7 File Offset: 0x00006CA7
		public override void Serialize(NetworkWriter writer)
		{
			writer.Write(this.slotId);
			writer.Write(this.readyState);
		}

		// Token: 0x040000A1 RID: 161
		public byte slotId;

		// Token: 0x040000A2 RID: 162
		public bool readyState;
	}
}
