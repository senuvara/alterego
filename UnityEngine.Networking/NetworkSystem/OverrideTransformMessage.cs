﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x0200002E RID: 46
	internal class OverrideTransformMessage : MessageBase
	{
		// Token: 0x06000107 RID: 263 RVA: 0x000088D4 File Offset: 0x00006AD4
		public OverrideTransformMessage()
		{
		}

		// Token: 0x06000108 RID: 264 RVA: 0x000088DC File Offset: 0x00006ADC
		public override void Deserialize(NetworkReader reader)
		{
			this.netId = reader.ReadNetworkId();
			this.payload = reader.ReadBytesAndSize();
			this.teleport = reader.ReadBoolean();
			this.time = (int)reader.ReadPackedUInt32();
		}

		// Token: 0x06000109 RID: 265 RVA: 0x0000890F File Offset: 0x00006B0F
		public override void Serialize(NetworkWriter writer)
		{
			writer.Write(this.netId);
			writer.WriteBytesFull(this.payload);
			writer.Write(this.teleport);
			writer.WritePackedUInt32((uint)this.time);
		}

		// Token: 0x04000095 RID: 149
		public NetworkInstanceId netId;

		// Token: 0x04000096 RID: 150
		public byte[] payload;

		// Token: 0x04000097 RID: 151
		public bool teleport;

		// Token: 0x04000098 RID: 152
		public int time;
	}
}
