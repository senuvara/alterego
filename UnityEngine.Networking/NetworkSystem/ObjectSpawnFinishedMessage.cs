﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x0200002A RID: 42
	internal class ObjectSpawnFinishedMessage : MessageBase
	{
		// Token: 0x060000FB RID: 251 RVA: 0x0000880A File Offset: 0x00006A0A
		public ObjectSpawnFinishedMessage()
		{
		}

		// Token: 0x060000FC RID: 252 RVA: 0x00008812 File Offset: 0x00006A12
		public override void Deserialize(NetworkReader reader)
		{
			this.state = reader.ReadPackedUInt32();
		}

		// Token: 0x060000FD RID: 253 RVA: 0x00008821 File Offset: 0x00006A21
		public override void Serialize(NetworkWriter writer)
		{
			writer.WritePackedUInt32(this.state);
		}

		// Token: 0x0400008F RID: 143
		public uint state;
	}
}
