﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x0200001C RID: 28
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class IntegerMessage : MessageBase
	{
		// Token: 0x060000D6 RID: 214 RVA: 0x00008237 File Offset: 0x00006437
		public IntegerMessage()
		{
		}

		// Token: 0x060000D7 RID: 215 RVA: 0x00008240 File Offset: 0x00006440
		public IntegerMessage(int v)
		{
			this.value = v;
		}

		// Token: 0x060000D8 RID: 216 RVA: 0x00008250 File Offset: 0x00006450
		public override void Deserialize(NetworkReader reader)
		{
			this.value = (int)reader.ReadPackedUInt32();
		}

		// Token: 0x060000D9 RID: 217 RVA: 0x0000825F File Offset: 0x0000645F
		public override void Serialize(NetworkWriter writer)
		{
			writer.WritePackedUInt32((uint)this.value);
		}

		// Token: 0x0400006E RID: 110
		public int value;
	}
}
