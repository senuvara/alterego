﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x0200002F RID: 47
	internal class AnimationMessage : MessageBase
	{
		// Token: 0x0600010A RID: 266 RVA: 0x00008942 File Offset: 0x00006B42
		public AnimationMessage()
		{
		}

		// Token: 0x0600010B RID: 267 RVA: 0x0000894A File Offset: 0x00006B4A
		public override void Deserialize(NetworkReader reader)
		{
			this.netId = reader.ReadNetworkId();
			this.stateHash = (int)reader.ReadPackedUInt32();
			this.normalizedTime = reader.ReadSingle();
			this.parameters = reader.ReadBytesAndSize();
		}

		// Token: 0x0600010C RID: 268 RVA: 0x00008980 File Offset: 0x00006B80
		public override void Serialize(NetworkWriter writer)
		{
			writer.Write(this.netId);
			writer.WritePackedUInt32((uint)this.stateHash);
			writer.Write(this.normalizedTime);
			if (this.parameters == null)
			{
				writer.WriteBytesAndSize(this.parameters, 0);
			}
			else
			{
				writer.WriteBytesAndSize(this.parameters, this.parameters.Length);
			}
		}

		// Token: 0x04000099 RID: 153
		public NetworkInstanceId netId;

		// Token: 0x0400009A RID: 154
		public int stateHash;

		// Token: 0x0400009B RID: 155
		public float normalizedTime;

		// Token: 0x0400009C RID: 156
		public byte[] parameters;
	}
}
