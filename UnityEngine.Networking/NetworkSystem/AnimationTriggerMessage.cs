﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x02000031 RID: 49
	internal class AnimationTriggerMessage : MessageBase
	{
		// Token: 0x06000110 RID: 272 RVA: 0x00008A46 File Offset: 0x00006C46
		public AnimationTriggerMessage()
		{
		}

		// Token: 0x06000111 RID: 273 RVA: 0x00008A4E File Offset: 0x00006C4E
		public override void Deserialize(NetworkReader reader)
		{
			this.netId = reader.ReadNetworkId();
			this.hash = (int)reader.ReadPackedUInt32();
		}

		// Token: 0x06000112 RID: 274 RVA: 0x00008A69 File Offset: 0x00006C69
		public override void Serialize(NetworkWriter writer)
		{
			writer.Write(this.netId);
			writer.WritePackedUInt32((uint)this.hash);
		}

		// Token: 0x0400009F RID: 159
		public NetworkInstanceId netId;

		// Token: 0x040000A0 RID: 160
		public int hash;
	}
}
