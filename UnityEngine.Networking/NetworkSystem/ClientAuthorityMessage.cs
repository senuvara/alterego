﻿using System;

namespace UnityEngine.Networking.NetworkSystem
{
	// Token: 0x0200002D RID: 45
	internal class ClientAuthorityMessage : MessageBase
	{
		// Token: 0x06000104 RID: 260 RVA: 0x00008896 File Offset: 0x00006A96
		public ClientAuthorityMessage()
		{
		}

		// Token: 0x06000105 RID: 261 RVA: 0x0000889E File Offset: 0x00006A9E
		public override void Deserialize(NetworkReader reader)
		{
			this.netId = reader.ReadNetworkId();
			this.authority = reader.ReadBoolean();
		}

		// Token: 0x06000106 RID: 262 RVA: 0x000088B9 File Offset: 0x00006AB9
		public override void Serialize(NetworkWriter writer)
		{
			writer.Write(this.netId);
			writer.Write(this.authority);
		}

		// Token: 0x04000093 RID: 147
		public NetworkInstanceId netId;

		// Token: 0x04000094 RID: 148
		public bool authority;
	}
}
