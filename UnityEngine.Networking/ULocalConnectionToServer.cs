﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000017 RID: 23
	internal class ULocalConnectionToServer : NetworkConnection
	{
		// Token: 0x060000BD RID: 189 RVA: 0x00007FD9 File Offset: 0x000061D9
		public ULocalConnectionToServer(NetworkServer localServer)
		{
			this.address = "localServer";
			this.m_LocalServer = localServer;
		}

		// Token: 0x060000BE RID: 190 RVA: 0x00007FF4 File Offset: 0x000061F4
		public override bool Send(short msgType, MessageBase msg)
		{
			return this.m_LocalServer.InvokeHandlerOnServer(this, msgType, msg, 0);
		}

		// Token: 0x060000BF RID: 191 RVA: 0x00008018 File Offset: 0x00006218
		public override bool SendUnreliable(short msgType, MessageBase msg)
		{
			return this.m_LocalServer.InvokeHandlerOnServer(this, msgType, msg, 1);
		}

		// Token: 0x060000C0 RID: 192 RVA: 0x0000803C File Offset: 0x0000623C
		public override bool SendByChannel(short msgType, MessageBase msg, int channelId)
		{
			return this.m_LocalServer.InvokeHandlerOnServer(this, msgType, msg, channelId);
		}

		// Token: 0x060000C1 RID: 193 RVA: 0x00008060 File Offset: 0x00006260
		public override bool SendBytes(byte[] bytes, int numBytes, int channelId)
		{
			bool result;
			if (numBytes <= 0)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("LocalConnection:SendBytes cannot send zero bytes");
				}
				result = false;
			}
			else
			{
				result = this.m_LocalServer.InvokeBytes(this, bytes, numBytes, channelId);
			}
			return result;
		}

		// Token: 0x060000C2 RID: 194 RVA: 0x000080AC File Offset: 0x000062AC
		public override bool SendWriter(NetworkWriter writer, int channelId)
		{
			return this.m_LocalServer.InvokeBytes(this, writer.AsArray(), (int)((short)writer.AsArray().Length), channelId);
		}

		// Token: 0x060000C3 RID: 195 RVA: 0x000080DD File Offset: 0x000062DD
		public override void GetStatsOut(out int numMsgs, out int numBufferedMsgs, out int numBytes, out int lastBufferedPerSecond)
		{
			numMsgs = 0;
			numBufferedMsgs = 0;
			numBytes = 0;
			lastBufferedPerSecond = 0;
		}

		// Token: 0x060000C4 RID: 196 RVA: 0x000080ED File Offset: 0x000062ED
		public override void GetStatsIn(out int numMsgs, out int numBytes)
		{
			numMsgs = 0;
			numBytes = 0;
		}

		// Token: 0x0400005B RID: 91
		private NetworkServer m_LocalServer;
	}
}
