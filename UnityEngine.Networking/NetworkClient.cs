﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.NetworkSystem;

namespace UnityEngine.Networking
{
	// Token: 0x02000040 RID: 64
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class NetworkClient
	{
		// Token: 0x060001A1 RID: 417 RVA: 0x00004FD4 File Offset: 0x000031D4
		public NetworkClient()
		{
			if (LogFilter.logDev)
			{
				Debug.Log("Client created version " + Version.Current);
			}
			this.m_MsgBuffer = new byte[65535];
			this.m_MsgReader = new NetworkReader(this.m_MsgBuffer);
			NetworkClient.AddClient(this);
		}

		// Token: 0x060001A2 RID: 418 RVA: 0x00005078 File Offset: 0x00003278
		public NetworkClient(NetworkConnection conn)
		{
			if (LogFilter.logDev)
			{
				Debug.Log("Client created version " + Version.Current);
			}
			this.m_MsgBuffer = new byte[65535];
			this.m_MsgReader = new NetworkReader(this.m_MsgBuffer);
			NetworkClient.AddClient(this);
			NetworkClient.SetActive(true);
			this.m_Connection = conn;
			this.m_AsyncConnect = NetworkClient.ConnectState.Connected;
			conn.SetHandlers(this.m_MessageHandlers);
			this.RegisterSystemHandlers(false);
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x060001A3 RID: 419 RVA: 0x00005144 File Offset: 0x00003344
		public static List<NetworkClient> allClients
		{
			get
			{
				return NetworkClient.s_Clients;
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x060001A4 RID: 420 RVA: 0x00005160 File Offset: 0x00003360
		public static bool active
		{
			get
			{
				return NetworkClient.s_IsActive;
			}
		}

		// Token: 0x060001A5 RID: 421 RVA: 0x0000517A File Offset: 0x0000337A
		internal void SetHandlers(NetworkConnection conn)
		{
			conn.SetHandlers(this.m_MessageHandlers);
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x060001A6 RID: 422 RVA: 0x0000518C File Offset: 0x0000338C
		public string serverIp
		{
			get
			{
				return this.m_ServerIp;
			}
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x060001A7 RID: 423 RVA: 0x000051A8 File Offset: 0x000033A8
		public int serverPort
		{
			get
			{
				return this.m_ServerPort;
			}
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x060001A8 RID: 424 RVA: 0x000051C4 File Offset: 0x000033C4
		public NetworkConnection connection
		{
			get
			{
				return this.m_Connection;
			}
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x060001A9 RID: 425 RVA: 0x000051E0 File Offset: 0x000033E0
		[Obsolete("Moved to NetworkMigrationManager.")]
		public PeerInfoMessage[] peers
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x060001AA RID: 426 RVA: 0x000051F8 File Offset: 0x000033F8
		internal int hostId
		{
			get
			{
				return this.m_ClientId;
			}
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x060001AB RID: 427 RVA: 0x00005214 File Offset: 0x00003414
		public Dictionary<short, NetworkMessageDelegate> handlers
		{
			get
			{
				return this.m_MessageHandlers.GetHandlers();
			}
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x060001AC RID: 428 RVA: 0x00005234 File Offset: 0x00003434
		public int numChannels
		{
			get
			{
				return this.m_HostTopology.DefaultConfig.ChannelCount;
			}
		}

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x060001AD RID: 429 RVA: 0x0000525C File Offset: 0x0000345C
		public HostTopology hostTopology
		{
			get
			{
				return this.m_HostTopology;
			}
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x060001AE RID: 430 RVA: 0x00005278 File Offset: 0x00003478
		// (set) Token: 0x060001AF RID: 431 RVA: 0x00005293 File Offset: 0x00003493
		public int hostPort
		{
			get
			{
				return this.m_HostPort;
			}
			set
			{
				if (value < 0)
				{
					throw new ArgumentException("Port must not be a negative number.");
				}
				if (value > 65535)
				{
					throw new ArgumentException("Port must not be greater than 65535.");
				}
				this.m_HostPort = value;
			}
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x060001B0 RID: 432 RVA: 0x000052C8 File Offset: 0x000034C8
		public bool isConnected
		{
			get
			{
				return this.m_AsyncConnect == NetworkClient.ConnectState.Connected;
			}
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x060001B1 RID: 433 RVA: 0x000052E8 File Offset: 0x000034E8
		public Type networkConnectionClass
		{
			get
			{
				return this.m_NetworkConnectionClass;
			}
		}

		// Token: 0x060001B2 RID: 434 RVA: 0x00005303 File Offset: 0x00003503
		public void SetNetworkConnectionClass<T>() where T : NetworkConnection
		{
			this.m_NetworkConnectionClass = typeof(T);
		}

		// Token: 0x060001B3 RID: 435 RVA: 0x00005318 File Offset: 0x00003518
		public bool Configure(ConnectionConfig config, int maxConnections)
		{
			HostTopology topology = new HostTopology(config, maxConnections);
			return this.Configure(topology);
		}

		// Token: 0x060001B4 RID: 436 RVA: 0x0000533C File Offset: 0x0000353C
		public bool Configure(HostTopology topology)
		{
			this.m_HostTopology = topology;
			return true;
		}

		// Token: 0x060001B5 RID: 437 RVA: 0x00005359 File Offset: 0x00003559
		public void Connect(MatchInfo matchInfo)
		{
			this.PrepareForConnect();
			this.ConnectWithRelay(matchInfo);
		}

		// Token: 0x060001B6 RID: 438 RVA: 0x0000536C File Offset: 0x0000356C
		public bool ReconnectToNewHost(string serverIp, int serverPort)
		{
			bool result;
			if (!NetworkClient.active)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("Reconnect - NetworkClient must be active");
				}
				result = false;
			}
			else if (this.m_Connection == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("Reconnect - no old connection exists");
				}
				result = false;
			}
			else
			{
				if (LogFilter.logInfo)
				{
					Debug.Log(string.Concat(new object[]
					{
						"NetworkClient Reconnect ",
						serverIp,
						":",
						serverPort
					}));
				}
				ClientScene.HandleClientDisconnect(this.m_Connection);
				ClientScene.ClearLocalPlayers();
				this.m_Connection.Disconnect();
				this.m_Connection = null;
				this.m_ClientId = NetworkManager.activeTransport.AddHost(this.m_HostTopology, this.m_HostPort, null);
				this.m_ServerPort = serverPort;
				if (Application.platform == RuntimePlatform.WebGLPlayer)
				{
					this.m_ServerIp = serverIp;
					this.m_AsyncConnect = NetworkClient.ConnectState.Resolved;
				}
				else if (serverIp.Equals("127.0.0.1") || serverIp.Equals("localhost"))
				{
					this.m_ServerIp = "127.0.0.1";
					this.m_AsyncConnect = NetworkClient.ConnectState.Resolved;
				}
				else
				{
					if (LogFilter.logDebug)
					{
						Debug.Log("Async DNS START:" + serverIp);
					}
					this.m_AsyncConnect = NetworkClient.ConnectState.Resolving;
					Dns.BeginGetHostAddresses(serverIp, new AsyncCallback(NetworkClient.GetHostAddressesCallback), this);
				}
				result = true;
			}
			return result;
		}

		// Token: 0x060001B7 RID: 439 RVA: 0x000054E8 File Offset: 0x000036E8
		public bool ReconnectToNewHost(EndPoint secureTunnelEndPoint)
		{
			bool result;
			if (!NetworkClient.active)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("Reconnect - NetworkClient must be active");
				}
				result = false;
			}
			else if (this.m_Connection == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("Reconnect - no old connection exists");
				}
				result = false;
			}
			else
			{
				if (LogFilter.logInfo)
				{
					Debug.Log("NetworkClient Reconnect to remoteSockAddr");
				}
				ClientScene.HandleClientDisconnect(this.m_Connection);
				ClientScene.ClearLocalPlayers();
				this.m_Connection.Disconnect();
				this.m_Connection = null;
				this.m_ClientId = NetworkManager.activeTransport.AddHost(this.m_HostTopology, this.m_HostPort, null);
				if (secureTunnelEndPoint == null)
				{
					if (LogFilter.logError)
					{
						Debug.LogError("Reconnect failed: null endpoint passed in");
					}
					this.m_AsyncConnect = NetworkClient.ConnectState.Failed;
					result = false;
				}
				else if (secureTunnelEndPoint.AddressFamily != AddressFamily.InterNetwork && secureTunnelEndPoint.AddressFamily != AddressFamily.InterNetworkV6)
				{
					if (LogFilter.logError)
					{
						Debug.LogError("Reconnect failed: Endpoint AddressFamily must be either InterNetwork or InterNetworkV6");
					}
					this.m_AsyncConnect = NetworkClient.ConnectState.Failed;
					result = false;
				}
				else
				{
					string fullName = secureTunnelEndPoint.GetType().FullName;
					if (fullName == "System.Net.IPEndPoint")
					{
						IPEndPoint ipendPoint = (IPEndPoint)secureTunnelEndPoint;
						this.Connect(ipendPoint.Address.ToString(), ipendPoint.Port);
						result = (this.m_AsyncConnect != NetworkClient.ConnectState.Failed);
					}
					else if (fullName != "UnityEngine.XboxOne.XboxOneEndPoint" && fullName != "UnityEngine.PS4.SceEndPoint")
					{
						if (LogFilter.logError)
						{
							Debug.LogError("Reconnect failed: invalid Endpoint (not IPEndPoint or XboxOneEndPoint or SceEndPoint)");
						}
						this.m_AsyncConnect = NetworkClient.ConnectState.Failed;
						result = false;
					}
					else
					{
						byte b = 0;
						this.m_RemoteEndPoint = secureTunnelEndPoint;
						this.m_AsyncConnect = NetworkClient.ConnectState.Connecting;
						try
						{
							this.m_ClientConnectionId = NetworkManager.activeTransport.ConnectEndPoint(this.m_ClientId, this.m_RemoteEndPoint, 0, out b);
						}
						catch (Exception arg)
						{
							if (LogFilter.logError)
							{
								Debug.LogError("Reconnect failed: Exception when trying to connect to EndPoint: " + arg);
							}
							this.m_AsyncConnect = NetworkClient.ConnectState.Failed;
							return false;
						}
						if (this.m_ClientConnectionId == 0)
						{
							if (LogFilter.logError)
							{
								Debug.LogError("Reconnect failed: Unable to connect to EndPoint (" + b + ")");
							}
							this.m_AsyncConnect = NetworkClient.ConnectState.Failed;
							result = false;
						}
						else
						{
							this.m_Connection = (NetworkConnection)Activator.CreateInstance(this.m_NetworkConnectionClass);
							this.m_Connection.SetHandlers(this.m_MessageHandlers);
							this.m_Connection.Initialize(this.m_ServerIp, this.m_ClientId, this.m_ClientConnectionId, this.m_HostTopology);
							result = true;
						}
					}
				}
			}
			return result;
		}

		// Token: 0x060001B8 RID: 440 RVA: 0x000057A0 File Offset: 0x000039A0
		public void ConnectWithSimulator(string serverIp, int serverPort, int latency, float packetLoss)
		{
			this.m_UseSimulator = true;
			this.m_SimulatedLatency = latency;
			this.m_PacketLoss = packetLoss;
			this.Connect(serverIp, serverPort);
		}

		// Token: 0x060001B9 RID: 441 RVA: 0x000057C4 File Offset: 0x000039C4
		private static bool IsValidIpV6(string address)
		{
			foreach (char c in address)
			{
				if (c != ':' && (c < '0' || c > '9') && (c < 'a' || c > 'f') && (c < 'A' || c > 'F'))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x060001BA RID: 442 RVA: 0x00005840 File Offset: 0x00003A40
		public void Connect(string serverIp, int serverPort)
		{
			this.PrepareForConnect();
			if (LogFilter.logDebug)
			{
				Debug.Log(string.Concat(new object[]
				{
					"Client Connect: ",
					serverIp,
					":",
					serverPort
				}));
			}
			this.m_ServerPort = serverPort;
			if (Application.platform == RuntimePlatform.WebGLPlayer)
			{
				this.m_ServerIp = serverIp;
				this.m_AsyncConnect = NetworkClient.ConnectState.Resolved;
			}
			else if (serverIp.Equals("127.0.0.1") || serverIp.Equals("localhost"))
			{
				this.m_ServerIp = "127.0.0.1";
				this.m_AsyncConnect = NetworkClient.ConnectState.Resolved;
			}
			else if (serverIp.IndexOf(":") != -1 && NetworkClient.IsValidIpV6(serverIp))
			{
				this.m_ServerIp = serverIp;
				this.m_AsyncConnect = NetworkClient.ConnectState.Resolved;
			}
			else
			{
				if (LogFilter.logDebug)
				{
					Debug.Log("Async DNS START:" + serverIp);
				}
				this.m_RequestedServerHost = serverIp;
				this.m_AsyncConnect = NetworkClient.ConnectState.Resolving;
				if (NetworkClient.<>f__mg$cache0 == null)
				{
					NetworkClient.<>f__mg$cache0 = new AsyncCallback(NetworkClient.GetHostAddressesCallback);
				}
				Dns.BeginGetHostAddresses(serverIp, NetworkClient.<>f__mg$cache0, this);
			}
		}

		// Token: 0x060001BB RID: 443 RVA: 0x00005974 File Offset: 0x00003B74
		public void Connect(EndPoint secureTunnelEndPoint)
		{
			bool usePlatformSpecificProtocols = NetworkManager.activeTransport.DoesEndPointUsePlatformProtocols(secureTunnelEndPoint);
			this.PrepareForConnect(usePlatformSpecificProtocols);
			if (LogFilter.logDebug)
			{
				Debug.Log("Client Connect to remoteSockAddr");
			}
			if (secureTunnelEndPoint == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("Connect failed: null endpoint passed in");
				}
				this.m_AsyncConnect = NetworkClient.ConnectState.Failed;
			}
			else if (secureTunnelEndPoint.AddressFamily != AddressFamily.InterNetwork && secureTunnelEndPoint.AddressFamily != AddressFamily.InterNetworkV6)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("Connect failed: Endpoint AddressFamily must be either InterNetwork or InterNetworkV6");
				}
				this.m_AsyncConnect = NetworkClient.ConnectState.Failed;
			}
			else
			{
				string fullName = secureTunnelEndPoint.GetType().FullName;
				if (fullName == "System.Net.IPEndPoint")
				{
					IPEndPoint ipendPoint = (IPEndPoint)secureTunnelEndPoint;
					this.Connect(ipendPoint.Address.ToString(), ipendPoint.Port);
				}
				else if (fullName != "UnityEngine.XboxOne.XboxOneEndPoint" && fullName != "UnityEngine.PS4.SceEndPoint")
				{
					if (LogFilter.logError)
					{
						Debug.LogError("Connect failed: invalid Endpoint (not IPEndPoint or XboxOneEndPoint or SceEndPoint)");
					}
					this.m_AsyncConnect = NetworkClient.ConnectState.Failed;
				}
				else
				{
					byte b = 0;
					this.m_RemoteEndPoint = secureTunnelEndPoint;
					this.m_AsyncConnect = NetworkClient.ConnectState.Connecting;
					try
					{
						this.m_ClientConnectionId = NetworkManager.activeTransport.ConnectEndPoint(this.m_ClientId, this.m_RemoteEndPoint, 0, out b);
					}
					catch (Exception arg)
					{
						if (LogFilter.logError)
						{
							Debug.LogError("Connect failed: Exception when trying to connect to EndPoint: " + arg);
						}
						this.m_AsyncConnect = NetworkClient.ConnectState.Failed;
						return;
					}
					if (this.m_ClientConnectionId == 0)
					{
						if (LogFilter.logError)
						{
							Debug.LogError("Connect failed: Unable to connect to EndPoint (" + b + ")");
						}
						this.m_AsyncConnect = NetworkClient.ConnectState.Failed;
					}
					else
					{
						this.m_Connection = (NetworkConnection)Activator.CreateInstance(this.m_NetworkConnectionClass);
						this.m_Connection.SetHandlers(this.m_MessageHandlers);
						this.m_Connection.Initialize(this.m_ServerIp, this.m_ClientId, this.m_ClientConnectionId, this.m_HostTopology);
					}
				}
			}
		}

		// Token: 0x060001BC RID: 444 RVA: 0x00005B90 File Offset: 0x00003D90
		private void PrepareForConnect()
		{
			this.PrepareForConnect(false);
		}

		// Token: 0x060001BD RID: 445 RVA: 0x00005B9C File Offset: 0x00003D9C
		private void PrepareForConnect(bool usePlatformSpecificProtocols)
		{
			NetworkClient.SetActive(true);
			this.RegisterSystemHandlers(false);
			if (this.m_HostTopology == null)
			{
				ConnectionConfig connectionConfig = new ConnectionConfig();
				connectionConfig.AddChannel(QosType.ReliableSequenced);
				connectionConfig.AddChannel(QosType.Unreliable);
				connectionConfig.UsePlatformSpecificProtocols = usePlatformSpecificProtocols;
				this.m_HostTopology = new HostTopology(connectionConfig, 8);
			}
			if (this.m_UseSimulator)
			{
				int num = this.m_SimulatedLatency / 3 - 1;
				if (num < 1)
				{
					num = 1;
				}
				int num2 = this.m_SimulatedLatency * 3;
				if (LogFilter.logDebug)
				{
					Debug.Log(string.Concat(new object[]
					{
						"AddHost Using Simulator ",
						num,
						"/",
						num2
					}));
				}
				this.m_ClientId = NetworkManager.activeTransport.AddHostWithSimulator(this.m_HostTopology, num, num2, this.m_HostPort);
			}
			else
			{
				this.m_ClientId = NetworkManager.activeTransport.AddHost(this.m_HostTopology, this.m_HostPort, null);
			}
		}

		// Token: 0x060001BE RID: 446 RVA: 0x00005C9C File Offset: 0x00003E9C
		internal static void GetHostAddressesCallback(IAsyncResult ar)
		{
			try
			{
				IPAddress[] array = Dns.EndGetHostAddresses(ar);
				NetworkClient networkClient = (NetworkClient)ar.AsyncState;
				if (array.Length == 0)
				{
					if (LogFilter.logError)
					{
						Debug.LogError("DNS lookup failed for:" + networkClient.m_RequestedServerHost);
					}
					networkClient.m_AsyncConnect = NetworkClient.ConnectState.Failed;
				}
				else
				{
					networkClient.m_ServerIp = array[0].ToString();
					networkClient.m_AsyncConnect = NetworkClient.ConnectState.Resolved;
					if (LogFilter.logDebug)
					{
						Debug.Log(string.Concat(new string[]
						{
							"Async DNS Result:",
							networkClient.m_ServerIp,
							" for ",
							networkClient.m_RequestedServerHost,
							": ",
							networkClient.m_ServerIp
						}));
					}
				}
			}
			catch (SocketException ex)
			{
				NetworkClient networkClient2 = (NetworkClient)ar.AsyncState;
				if (LogFilter.logError)
				{
					Debug.LogError("DNS resolution failed: " + ex.GetErrorCode());
				}
				if (LogFilter.logDebug)
				{
					Debug.Log("Exception:" + ex);
				}
				networkClient2.m_AsyncConnect = NetworkClient.ConnectState.Failed;
			}
		}

		// Token: 0x060001BF RID: 447 RVA: 0x00005DC8 File Offset: 0x00003FC8
		internal void ContinueConnect()
		{
			if (this.m_UseSimulator)
			{
				int num = this.m_SimulatedLatency / 3;
				if (num < 1)
				{
					num = 1;
				}
				if (LogFilter.logDebug)
				{
					Debug.Log(string.Concat(new object[]
					{
						"Connect Using Simulator ",
						this.m_SimulatedLatency / 3,
						"/",
						this.m_SimulatedLatency
					}));
				}
				ConnectionSimulatorConfig conf = new ConnectionSimulatorConfig(num, this.m_SimulatedLatency, num, this.m_SimulatedLatency, this.m_PacketLoss);
				byte b;
				this.m_ClientConnectionId = NetworkManager.activeTransport.ConnectWithSimulator(this.m_ClientId, this.m_ServerIp, this.m_ServerPort, 0, out b, conf);
			}
			else
			{
				byte b;
				this.m_ClientConnectionId = NetworkManager.activeTransport.Connect(this.m_ClientId, this.m_ServerIp, this.m_ServerPort, 0, out b);
			}
			this.m_Connection = (NetworkConnection)Activator.CreateInstance(this.m_NetworkConnectionClass);
			this.m_Connection.SetHandlers(this.m_MessageHandlers);
			this.m_Connection.Initialize(this.m_ServerIp, this.m_ClientId, this.m_ClientConnectionId, this.m_HostTopology);
		}

		// Token: 0x060001C0 RID: 448 RVA: 0x00005EF8 File Offset: 0x000040F8
		private void ConnectWithRelay(MatchInfo info)
		{
			this.m_AsyncConnect = NetworkClient.ConnectState.Connecting;
			this.Update();
			byte b;
			this.m_ClientConnectionId = NetworkManager.activeTransport.ConnectToNetworkPeer(this.m_ClientId, info.address, info.port, 0, 0, info.networkId, Utility.GetSourceID(), info.nodeId, out b);
			this.m_Connection = (NetworkConnection)Activator.CreateInstance(this.m_NetworkConnectionClass);
			this.m_Connection.SetHandlers(this.m_MessageHandlers);
			this.m_Connection.Initialize(info.address, this.m_ClientId, this.m_ClientConnectionId, this.m_HostTopology);
			if (b != 0)
			{
				Debug.LogError("ConnectToNetworkPeer Error: " + b);
			}
		}

		// Token: 0x060001C1 RID: 449 RVA: 0x00005FB4 File Offset: 0x000041B4
		public virtual void Disconnect()
		{
			this.m_AsyncConnect = NetworkClient.ConnectState.Disconnected;
			ClientScene.HandleClientDisconnect(this.m_Connection);
			if (this.m_Connection != null)
			{
				this.m_Connection.Disconnect();
				this.m_Connection.Dispose();
				this.m_Connection = null;
				if (this.m_ClientId != -1)
				{
					NetworkManager.activeTransport.RemoveHost(this.m_ClientId);
					this.m_ClientId = -1;
				}
			}
		}

		// Token: 0x060001C2 RID: 450 RVA: 0x00006024 File Offset: 0x00004224
		public bool Send(short msgType, MessageBase msg)
		{
			bool result;
			if (this.m_Connection != null)
			{
				if (this.m_AsyncConnect != NetworkClient.ConnectState.Connected)
				{
					if (LogFilter.logError)
					{
						Debug.LogError("NetworkClient Send when not connected to a server");
					}
					result = false;
				}
				else
				{
					result = this.m_Connection.Send(msgType, msg);
				}
			}
			else
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkClient Send with no connection");
				}
				result = false;
			}
			return result;
		}

		// Token: 0x060001C3 RID: 451 RVA: 0x0000609C File Offset: 0x0000429C
		public bool SendWriter(NetworkWriter writer, int channelId)
		{
			bool result;
			if (this.m_Connection != null)
			{
				if (this.m_AsyncConnect != NetworkClient.ConnectState.Connected)
				{
					if (LogFilter.logError)
					{
						Debug.LogError("NetworkClient SendWriter when not connected to a server");
					}
					result = false;
				}
				else
				{
					result = this.m_Connection.SendWriter(writer, channelId);
				}
			}
			else
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkClient SendWriter with no connection");
				}
				result = false;
			}
			return result;
		}

		// Token: 0x060001C4 RID: 452 RVA: 0x00006114 File Offset: 0x00004314
		public bool SendBytes(byte[] data, int numBytes, int channelId)
		{
			bool result;
			if (this.m_Connection != null)
			{
				if (this.m_AsyncConnect != NetworkClient.ConnectState.Connected)
				{
					if (LogFilter.logError)
					{
						Debug.LogError("NetworkClient SendBytes when not connected to a server");
					}
					result = false;
				}
				else
				{
					result = this.m_Connection.SendBytes(data, numBytes, channelId);
				}
			}
			else
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkClient SendBytes with no connection");
				}
				result = false;
			}
			return result;
		}

		// Token: 0x060001C5 RID: 453 RVA: 0x0000618C File Offset: 0x0000438C
		public bool SendUnreliable(short msgType, MessageBase msg)
		{
			bool result;
			if (this.m_Connection != null)
			{
				if (this.m_AsyncConnect != NetworkClient.ConnectState.Connected)
				{
					if (LogFilter.logError)
					{
						Debug.LogError("NetworkClient SendUnreliable when not connected to a server");
					}
					result = false;
				}
				else
				{
					result = this.m_Connection.SendUnreliable(msgType, msg);
				}
			}
			else
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkClient SendUnreliable with no connection");
				}
				result = false;
			}
			return result;
		}

		// Token: 0x060001C6 RID: 454 RVA: 0x00006204 File Offset: 0x00004404
		public bool SendByChannel(short msgType, MessageBase msg, int channelId)
		{
			bool result;
			if (this.m_Connection != null)
			{
				if (this.m_AsyncConnect != NetworkClient.ConnectState.Connected)
				{
					if (LogFilter.logError)
					{
						Debug.LogError("NetworkClient SendByChannel when not connected to a server");
					}
					result = false;
				}
				else
				{
					result = this.m_Connection.SendByChannel(msgType, msg, channelId);
				}
			}
			else
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkClient SendByChannel with no connection");
				}
				result = false;
			}
			return result;
		}

		// Token: 0x060001C7 RID: 455 RVA: 0x0000627A File Offset: 0x0000447A
		public void SetMaxDelay(float seconds)
		{
			if (this.m_Connection == null)
			{
				if (LogFilter.logWarn)
				{
					Debug.LogWarning("SetMaxDelay failed, not connected.");
				}
			}
			else
			{
				this.m_Connection.SetMaxDelay(seconds);
			}
		}

		// Token: 0x060001C8 RID: 456 RVA: 0x000062B0 File Offset: 0x000044B0
		public void Shutdown()
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("Shutting down client " + this.m_ClientId);
			}
			if (this.m_ClientId != -1)
			{
				NetworkManager.activeTransport.RemoveHost(this.m_ClientId);
				this.m_ClientId = -1;
			}
			NetworkClient.RemoveClient(this);
			if (NetworkClient.s_Clients.Count == 0)
			{
				NetworkClient.SetActive(false);
			}
		}

		// Token: 0x060001C9 RID: 457 RVA: 0x00006328 File Offset: 0x00004528
		internal virtual void Update()
		{
			if (this.m_ClientId != -1)
			{
				switch (this.m_AsyncConnect)
				{
				case NetworkClient.ConnectState.None:
				case NetworkClient.ConnectState.Resolving:
				case NetworkClient.ConnectState.Disconnected:
					return;
				case NetworkClient.ConnectState.Resolved:
					this.m_AsyncConnect = NetworkClient.ConnectState.Connecting;
					this.ContinueConnect();
					return;
				case NetworkClient.ConnectState.Failed:
					this.GenerateConnectError(11);
					this.m_AsyncConnect = NetworkClient.ConnectState.Disconnected;
					return;
				}
				if (this.m_Connection != null)
				{
					if ((int)Time.time != this.m_StatResetTime)
					{
						this.m_Connection.ResetStats();
						this.m_StatResetTime = (int)Time.time;
					}
				}
				int num = 0;
				byte b;
				for (;;)
				{
					int num2;
					int channelId;
					int numBytes;
					NetworkEventType networkEventType = NetworkManager.activeTransport.ReceiveFromHost(this.m_ClientId, out num2, out channelId, this.m_MsgBuffer, (int)((ushort)this.m_MsgBuffer.Length), out numBytes, out b);
					if (this.m_Connection != null)
					{
						this.m_Connection.lastError = (NetworkError)b;
					}
					if (networkEventType != NetworkEventType.Nothing)
					{
						if (LogFilter.logDev)
						{
							Debug.Log(string.Concat(new object[]
							{
								"Client event: host=",
								this.m_ClientId,
								" event=",
								networkEventType,
								" error=",
								b
							}));
						}
					}
					switch (networkEventType)
					{
					case NetworkEventType.DataEvent:
						if (b != 0)
						{
							goto Block_11;
						}
						this.m_MsgReader.SeekZero();
						this.m_Connection.TransportReceive(this.m_MsgBuffer, numBytes, channelId);
						break;
					case NetworkEventType.ConnectEvent:
						if (LogFilter.logDebug)
						{
							Debug.Log("Client connected");
						}
						if (b != 0)
						{
							goto Block_10;
						}
						this.m_AsyncConnect = NetworkClient.ConnectState.Connected;
						this.m_Connection.InvokeHandlerNoData(32);
						break;
					case NetworkEventType.DisconnectEvent:
						if (LogFilter.logDebug)
						{
							Debug.Log("Client disconnected");
						}
						this.m_AsyncConnect = NetworkClient.ConnectState.Disconnected;
						if (b != 0)
						{
							if (b != 6)
							{
								this.GenerateDisconnectError((int)b);
							}
						}
						ClientScene.HandleClientDisconnect(this.m_Connection);
						if (this.m_Connection != null)
						{
							this.m_Connection.InvokeHandlerNoData(33);
						}
						break;
					case NetworkEventType.Nothing:
						break;
					default:
						if (LogFilter.logError)
						{
							Debug.LogError("Unknown network message type received: " + networkEventType);
						}
						break;
					}
					if (++num >= 500)
					{
						goto Block_17;
					}
					if (this.m_ClientId == -1)
					{
						goto Block_19;
					}
					if (networkEventType == NetworkEventType.Nothing)
					{
						goto IL_2CB;
					}
				}
				Block_10:
				this.GenerateConnectError((int)b);
				return;
				Block_11:
				this.GenerateDataError((int)b);
				return;
				Block_17:
				if (LogFilter.logDebug)
				{
					Debug.Log("MaxEventsPerFrame hit (" + 500 + ")");
				}
				Block_19:
				IL_2CB:
				if (this.m_Connection != null && this.m_AsyncConnect == NetworkClient.ConnectState.Connected)
				{
					this.m_Connection.FlushChannels();
				}
			}
		}

		// Token: 0x060001CA RID: 458 RVA: 0x00006622 File Offset: 0x00004822
		private void GenerateConnectError(int error)
		{
			if (LogFilter.logError)
			{
				Debug.LogError("UNet Client Error Connect Error: " + error);
			}
			this.GenerateError(error);
		}

		// Token: 0x060001CB RID: 459 RVA: 0x00006650 File Offset: 0x00004850
		private void GenerateDataError(int error)
		{
			if (LogFilter.logError)
			{
				Debug.LogError("UNet Client Data Error: " + (NetworkError)error);
			}
			this.GenerateError(error);
		}

		// Token: 0x060001CC RID: 460 RVA: 0x00006688 File Offset: 0x00004888
		private void GenerateDisconnectError(int error)
		{
			if (LogFilter.logError)
			{
				Debug.LogError("UNet Client Disconnect Error: " + (NetworkError)error);
			}
			this.GenerateError(error);
		}

		// Token: 0x060001CD RID: 461 RVA: 0x000066C0 File Offset: 0x000048C0
		private void GenerateError(int error)
		{
			NetworkMessageDelegate handler = this.m_MessageHandlers.GetHandler(34);
			if (handler == null)
			{
				handler = this.m_MessageHandlers.GetHandler(34);
			}
			if (handler != null)
			{
				ErrorMessage errorMessage = new ErrorMessage();
				errorMessage.errorCode = error;
				byte[] buffer = new byte[200];
				NetworkWriter writer = new NetworkWriter(buffer);
				errorMessage.Serialize(writer);
				NetworkReader reader = new NetworkReader(buffer);
				handler(new NetworkMessage
				{
					msgType = 34,
					reader = reader,
					conn = this.m_Connection,
					channelId = 0
				});
			}
		}

		// Token: 0x060001CE RID: 462 RVA: 0x0000675E File Offset: 0x0000495E
		public void GetStatsOut(out int numMsgs, out int numBufferedMsgs, out int numBytes, out int lastBufferedPerSecond)
		{
			numMsgs = 0;
			numBufferedMsgs = 0;
			numBytes = 0;
			lastBufferedPerSecond = 0;
			if (this.m_Connection != null)
			{
				this.m_Connection.GetStatsOut(out numMsgs, out numBufferedMsgs, out numBytes, out lastBufferedPerSecond);
			}
		}

		// Token: 0x060001CF RID: 463 RVA: 0x0000678B File Offset: 0x0000498B
		public void GetStatsIn(out int numMsgs, out int numBytes)
		{
			numMsgs = 0;
			numBytes = 0;
			if (this.m_Connection != null)
			{
				this.m_Connection.GetStatsIn(out numMsgs, out numBytes);
			}
		}

		// Token: 0x060001D0 RID: 464 RVA: 0x000067B0 File Offset: 0x000049B0
		public Dictionary<short, NetworkConnection.PacketStat> GetConnectionStats()
		{
			Dictionary<short, NetworkConnection.PacketStat> result;
			if (this.m_Connection == null)
			{
				result = null;
			}
			else
			{
				result = this.m_Connection.packetStats;
			}
			return result;
		}

		// Token: 0x060001D1 RID: 465 RVA: 0x000067E2 File Offset: 0x000049E2
		public void ResetConnectionStats()
		{
			if (this.m_Connection != null)
			{
				this.m_Connection.ResetStats();
			}
		}

		// Token: 0x060001D2 RID: 466 RVA: 0x00006800 File Offset: 0x00004A00
		public int GetRTT()
		{
			int result;
			if (this.m_ClientId == -1)
			{
				result = 0;
			}
			else
			{
				byte b;
				result = NetworkManager.activeTransport.GetCurrentRTT(this.m_ClientId, this.m_ClientConnectionId, out b);
			}
			return result;
		}

		// Token: 0x060001D3 RID: 467 RVA: 0x00006840 File Offset: 0x00004A40
		internal void RegisterSystemHandlers(bool localClient)
		{
			ClientScene.RegisterSystemHandlers(this, localClient);
			this.RegisterHandlerSafe(14, new NetworkMessageDelegate(this.OnCRC));
			short msgType = 17;
			if (NetworkClient.<>f__mg$cache1 == null)
			{
				NetworkClient.<>f__mg$cache1 = new NetworkMessageDelegate(NetworkConnection.OnFragment);
			}
			this.RegisterHandlerSafe(msgType, NetworkClient.<>f__mg$cache1);
		}

		// Token: 0x060001D4 RID: 468 RVA: 0x0000688E File Offset: 0x00004A8E
		private void OnCRC(NetworkMessage netMsg)
		{
			netMsg.ReadMessage<CRCMessage>(NetworkClient.s_CRCMessage);
			NetworkCRC.Validate(NetworkClient.s_CRCMessage.scripts, this.numChannels);
		}

		// Token: 0x060001D5 RID: 469 RVA: 0x000068B2 File Offset: 0x00004AB2
		public void RegisterHandler(short msgType, NetworkMessageDelegate handler)
		{
			this.m_MessageHandlers.RegisterHandler(msgType, handler);
		}

		// Token: 0x060001D6 RID: 470 RVA: 0x000068C2 File Offset: 0x00004AC2
		public void RegisterHandlerSafe(short msgType, NetworkMessageDelegate handler)
		{
			this.m_MessageHandlers.RegisterHandlerSafe(msgType, handler);
		}

		// Token: 0x060001D7 RID: 471 RVA: 0x000068D2 File Offset: 0x00004AD2
		public void UnregisterHandler(short msgType)
		{
			this.m_MessageHandlers.UnregisterHandler(msgType);
		}

		// Token: 0x060001D8 RID: 472 RVA: 0x000068E4 File Offset: 0x00004AE4
		public static Dictionary<short, NetworkConnection.PacketStat> GetTotalConnectionStats()
		{
			Dictionary<short, NetworkConnection.PacketStat> dictionary = new Dictionary<short, NetworkConnection.PacketStat>();
			for (int i = 0; i < NetworkClient.s_Clients.Count; i++)
			{
				NetworkClient networkClient = NetworkClient.s_Clients[i];
				Dictionary<short, NetworkConnection.PacketStat> connectionStats = networkClient.GetConnectionStats();
				foreach (short key in connectionStats.Keys)
				{
					if (dictionary.ContainsKey(key))
					{
						NetworkConnection.PacketStat packetStat = dictionary[key];
						packetStat.count += connectionStats[key].count;
						packetStat.bytes += connectionStats[key].bytes;
						dictionary[key] = packetStat;
					}
					else
					{
						dictionary[key] = new NetworkConnection.PacketStat(connectionStats[key]);
					}
				}
			}
			return dictionary;
		}

		// Token: 0x060001D9 RID: 473 RVA: 0x000069F4 File Offset: 0x00004BF4
		internal static void AddClient(NetworkClient client)
		{
			NetworkClient.s_Clients.Add(client);
		}

		// Token: 0x060001DA RID: 474 RVA: 0x00006A04 File Offset: 0x00004C04
		internal static bool RemoveClient(NetworkClient client)
		{
			return NetworkClient.s_Clients.Remove(client);
		}

		// Token: 0x060001DB RID: 475 RVA: 0x00006A24 File Offset: 0x00004C24
		internal static void UpdateClients()
		{
			for (int i = 0; i < NetworkClient.s_Clients.Count; i++)
			{
				if (NetworkClient.s_Clients[i] != null)
				{
					NetworkClient.s_Clients[i].Update();
				}
				else
				{
					NetworkClient.s_Clients.RemoveAt(i);
				}
			}
		}

		// Token: 0x060001DC RID: 476 RVA: 0x00006A7F File Offset: 0x00004C7F
		public static void ShutdownAll()
		{
			while (NetworkClient.s_Clients.Count != 0)
			{
				NetworkClient.s_Clients[0].Shutdown();
			}
			NetworkClient.s_Clients = new List<NetworkClient>();
			NetworkClient.s_IsActive = false;
			ClientScene.Shutdown();
		}

		// Token: 0x060001DD RID: 477 RVA: 0x00006ABD File Offset: 0x00004CBD
		internal static void SetActive(bool state)
		{
			if (!NetworkClient.s_IsActive && state)
			{
				NetworkManager.activeTransport.Init();
			}
			NetworkClient.s_IsActive = state;
		}

		// Token: 0x060001DE RID: 478 RVA: 0x00006AE2 File Offset: 0x00004CE2
		// Note: this type is marked as 'beforefieldinit'.
		static NetworkClient()
		{
		}

		// Token: 0x040000D2 RID: 210
		private Type m_NetworkConnectionClass = typeof(NetworkConnection);

		// Token: 0x040000D3 RID: 211
		private const int k_MaxEventsPerFrame = 500;

		// Token: 0x040000D4 RID: 212
		private static List<NetworkClient> s_Clients = new List<NetworkClient>();

		// Token: 0x040000D5 RID: 213
		private static bool s_IsActive;

		// Token: 0x040000D6 RID: 214
		private HostTopology m_HostTopology;

		// Token: 0x040000D7 RID: 215
		private int m_HostPort;

		// Token: 0x040000D8 RID: 216
		private bool m_UseSimulator;

		// Token: 0x040000D9 RID: 217
		private int m_SimulatedLatency;

		// Token: 0x040000DA RID: 218
		private float m_PacketLoss;

		// Token: 0x040000DB RID: 219
		private string m_ServerIp = "";

		// Token: 0x040000DC RID: 220
		private int m_ServerPort;

		// Token: 0x040000DD RID: 221
		private int m_ClientId = -1;

		// Token: 0x040000DE RID: 222
		private int m_ClientConnectionId = -1;

		// Token: 0x040000DF RID: 223
		private int m_StatResetTime;

		// Token: 0x040000E0 RID: 224
		private EndPoint m_RemoteEndPoint;

		// Token: 0x040000E1 RID: 225
		private static CRCMessage s_CRCMessage = new CRCMessage();

		// Token: 0x040000E2 RID: 226
		private NetworkMessageHandlers m_MessageHandlers = new NetworkMessageHandlers();

		// Token: 0x040000E3 RID: 227
		protected NetworkConnection m_Connection;

		// Token: 0x040000E4 RID: 228
		private byte[] m_MsgBuffer;

		// Token: 0x040000E5 RID: 229
		private NetworkReader m_MsgReader;

		// Token: 0x040000E6 RID: 230
		protected NetworkClient.ConnectState m_AsyncConnect = NetworkClient.ConnectState.None;

		// Token: 0x040000E7 RID: 231
		private string m_RequestedServerHost = "";

		// Token: 0x040000E8 RID: 232
		[CompilerGenerated]
		private static AsyncCallback <>f__mg$cache0;

		// Token: 0x040000E9 RID: 233
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cache1;

		// Token: 0x02000041 RID: 65
		protected enum ConnectState
		{
			// Token: 0x040000EB RID: 235
			None,
			// Token: 0x040000EC RID: 236
			Resolving,
			// Token: 0x040000ED RID: 237
			Resolved,
			// Token: 0x040000EE RID: 238
			Connecting,
			// Token: 0x040000EF RID: 239
			Connected,
			// Token: 0x040000F0 RID: 240
			Disconnected,
			// Token: 0x040000F1 RID: 241
			Failed
		}
	}
}
