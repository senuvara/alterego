﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x0200000C RID: 12
	[AttributeUsage(AttributeTargets.Event)]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class SyncEventAttribute : Attribute
	{
		// Token: 0x0600006B RID: 107 RVA: 0x00004C73 File Offset: 0x00002E73
		public SyncEventAttribute()
		{
		}

		// Token: 0x04000050 RID: 80
		public int channel = 0;
	}
}
