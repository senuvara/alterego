﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x0200003E RID: 62
	internal class FloatConversion
	{
		// Token: 0x06000193 RID: 403 RVA: 0x0000AC7F File Offset: 0x00008E7F
		public FloatConversion()
		{
		}

		// Token: 0x06000194 RID: 404 RVA: 0x0000AC88 File Offset: 0x00008E88
		public static float ToSingle(uint value)
		{
			UIntFloat uintFloat = default(UIntFloat);
			uintFloat.intValue = value;
			return uintFloat.floatValue;
		}

		// Token: 0x06000195 RID: 405 RVA: 0x0000ACB4 File Offset: 0x00008EB4
		public static double ToDouble(ulong value)
		{
			UIntFloat uintFloat = default(UIntFloat);
			uintFloat.longValue = value;
			return uintFloat.doubleValue;
		}

		// Token: 0x06000196 RID: 406 RVA: 0x0000ACE0 File Offset: 0x00008EE0
		public static decimal ToDecimal(ulong value1, ulong value2)
		{
			UIntDecimal uintDecimal = default(UIntDecimal);
			uintDecimal.longValue1 = value1;
			uintDecimal.longValue2 = value2;
			return uintDecimal.decimalValue;
		}
	}
}
