﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x0200006A RID: 106
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class SyncListInt : SyncList<int>
	{
		// Token: 0x0600056D RID: 1389 RVA: 0x0001EA11 File Offset: 0x0001CC11
		public SyncListInt()
		{
		}

		// Token: 0x0600056E RID: 1390 RVA: 0x0001EA19 File Offset: 0x0001CC19
		protected override void SerializeItem(NetworkWriter writer, int item)
		{
			writer.WritePackedUInt32((uint)item);
		}

		// Token: 0x0600056F RID: 1391 RVA: 0x0001EA24 File Offset: 0x0001CC24
		protected override int DeserializeItem(NetworkReader reader)
		{
			return (int)reader.ReadPackedUInt32();
		}

		// Token: 0x06000570 RID: 1392 RVA: 0x0001EA40 File Offset: 0x0001CC40
		[Obsolete("ReadReference is now used instead")]
		public static SyncListInt ReadInstance(NetworkReader reader)
		{
			ushort num = reader.ReadUInt16();
			SyncListInt syncListInt = new SyncListInt();
			for (ushort num2 = 0; num2 < num; num2 += 1)
			{
				syncListInt.AddInternal((int)reader.ReadPackedUInt32());
			}
			return syncListInt;
		}

		// Token: 0x06000571 RID: 1393 RVA: 0x0001EA84 File Offset: 0x0001CC84
		public static void ReadReference(NetworkReader reader, SyncListInt syncList)
		{
			ushort num = reader.ReadUInt16();
			syncList.Clear();
			for (ushort num2 = 0; num2 < num; num2 += 1)
			{
				syncList.AddInternal((int)reader.ReadPackedUInt32());
			}
		}

		// Token: 0x06000572 RID: 1394 RVA: 0x0001EAC0 File Offset: 0x0001CCC0
		public static void WriteInstance(NetworkWriter writer, SyncListInt items)
		{
			writer.Write((ushort)items.Count);
			for (int i = 0; i < items.Count; i++)
			{
				writer.WritePackedUInt32((uint)items[i]);
			}
		}
	}
}
