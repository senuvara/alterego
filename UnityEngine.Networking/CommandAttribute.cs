﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000009 RID: 9
	[AttributeUsage(AttributeTargets.Method)]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class CommandAttribute : Attribute
	{
		// Token: 0x06000068 RID: 104 RVA: 0x00004C46 File Offset: 0x00002E46
		public CommandAttribute()
		{
		}

		// Token: 0x0400004D RID: 77
		public int channel = 0;
	}
}
