﻿using System;
using System.ComponentModel;

namespace UnityEngine.Networking
{
	// Token: 0x02000065 RID: 101
	[DisallowMultipleComponent]
	[AddComponentMenu("Network/NetworkTransformVisualizer")]
	[RequireComponent(typeof(NetworkTransform))]
	[EditorBrowsable(EditorBrowsableState.Never)]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class NetworkTransformVisualizer : NetworkBehaviour
	{
		// Token: 0x06000525 RID: 1317 RVA: 0x0001CD89 File Offset: 0x0001AF89
		public NetworkTransformVisualizer()
		{
		}

		// Token: 0x170000DD RID: 221
		// (get) Token: 0x06000526 RID: 1318 RVA: 0x0001CD94 File Offset: 0x0001AF94
		// (set) Token: 0x06000527 RID: 1319 RVA: 0x0001CDAF File Offset: 0x0001AFAF
		public GameObject visualizerPrefab
		{
			get
			{
				return this.m_VisualizerPrefab;
			}
			set
			{
				this.m_VisualizerPrefab = value;
			}
		}

		// Token: 0x06000528 RID: 1320 RVA: 0x0001CDBC File Offset: 0x0001AFBC
		public override void OnStartClient()
		{
			if (this.m_VisualizerPrefab != null)
			{
				this.m_NetworkTransform = base.GetComponent<NetworkTransform>();
				NetworkTransformVisualizer.CreateLineMaterial();
				this.m_Visualizer = Object.Instantiate<GameObject>(this.m_VisualizerPrefab, base.transform.position, Quaternion.identity);
			}
		}

		// Token: 0x06000529 RID: 1321 RVA: 0x0001CE10 File Offset: 0x0001B010
		public override void OnStartLocalPlayer()
		{
			if (!(this.m_Visualizer == null))
			{
				if (this.m_NetworkTransform.localPlayerAuthority || base.isServer)
				{
					Object.Destroy(this.m_Visualizer);
				}
			}
		}

		// Token: 0x0600052A RID: 1322 RVA: 0x0001CE5C File Offset: 0x0001B05C
		private void OnDestroy()
		{
			if (this.m_Visualizer != null)
			{
				Object.Destroy(this.m_Visualizer);
			}
		}

		// Token: 0x0600052B RID: 1323 RVA: 0x0001CE80 File Offset: 0x0001B080
		[ClientCallback]
		private void FixedUpdate()
		{
			if (!(this.m_Visualizer == null))
			{
				if (NetworkServer.active || NetworkClient.active)
				{
					if (base.isServer || base.isClient)
					{
						if (!base.hasAuthority || !this.m_NetworkTransform.localPlayerAuthority)
						{
							this.m_Visualizer.transform.position = this.m_NetworkTransform.targetSyncPosition;
							if (this.m_NetworkTransform.rigidbody3D != null && this.m_Visualizer.GetComponent<Rigidbody>() != null)
							{
								this.m_Visualizer.GetComponent<Rigidbody>().velocity = this.m_NetworkTransform.targetSyncVelocity;
							}
							if (this.m_NetworkTransform.rigidbody2D != null && this.m_Visualizer.GetComponent<Rigidbody2D>() != null)
							{
								this.m_Visualizer.GetComponent<Rigidbody2D>().velocity = this.m_NetworkTransform.targetSyncVelocity;
							}
							Quaternion rotation = Quaternion.identity;
							if (this.m_NetworkTransform.rigidbody3D != null)
							{
								rotation = this.m_NetworkTransform.targetSyncRotation3D;
							}
							if (this.m_NetworkTransform.rigidbody2D != null)
							{
								rotation = Quaternion.Euler(0f, 0f, this.m_NetworkTransform.targetSyncRotation2D);
							}
							this.m_Visualizer.transform.rotation = rotation;
						}
					}
				}
			}
		}

		// Token: 0x0600052C RID: 1324 RVA: 0x0001D018 File Offset: 0x0001B218
		private void OnRenderObject()
		{
			if (!(this.m_Visualizer == null))
			{
				if (!this.m_NetworkTransform.localPlayerAuthority || !base.hasAuthority)
				{
					if (this.m_NetworkTransform.lastSyncTime != 0f)
					{
						NetworkTransformVisualizer.s_LineMaterial.SetPass(0);
						GL.Begin(1);
						GL.Color(Color.white);
						GL.Vertex3(base.transform.position.x, base.transform.position.y, base.transform.position.z);
						GL.Vertex3(this.m_NetworkTransform.targetSyncPosition.x, this.m_NetworkTransform.targetSyncPosition.y, this.m_NetworkTransform.targetSyncPosition.z);
						GL.End();
						this.DrawRotationInterpolation();
					}
				}
			}
		}

		// Token: 0x0600052D RID: 1325 RVA: 0x0001D11C File Offset: 0x0001B31C
		private void DrawRotationInterpolation()
		{
			Quaternion quaternion = Quaternion.identity;
			if (this.m_NetworkTransform.rigidbody3D != null)
			{
				quaternion = this.m_NetworkTransform.targetSyncRotation3D;
			}
			if (this.m_NetworkTransform.rigidbody2D != null)
			{
				quaternion = Quaternion.Euler(0f, 0f, this.m_NetworkTransform.targetSyncRotation2D);
			}
			if (!(quaternion == Quaternion.identity))
			{
				GL.Begin(1);
				GL.Color(Color.yellow);
				GL.Vertex3(base.transform.position.x, base.transform.position.y, base.transform.position.z);
				Vector3 vector = base.transform.position + base.transform.right;
				GL.Vertex3(vector.x, vector.y, vector.z);
				GL.End();
				GL.Begin(1);
				GL.Color(Color.green);
				GL.Vertex3(base.transform.position.x, base.transform.position.y, base.transform.position.z);
				Vector3 b = quaternion * Vector3.right;
				Vector3 vector2 = base.transform.position + b;
				GL.Vertex3(vector2.x, vector2.y, vector2.z);
				GL.End();
			}
		}

		// Token: 0x0600052E RID: 1326 RVA: 0x0001D2B8 File Offset: 0x0001B4B8
		private static void CreateLineMaterial()
		{
			if (!NetworkTransformVisualizer.s_LineMaterial)
			{
				Shader shader = Shader.Find("Hidden/Internal-Colored");
				if (!shader)
				{
					Debug.LogWarning("Could not find Colored builtin shader");
				}
				else
				{
					NetworkTransformVisualizer.s_LineMaterial = new Material(shader);
					NetworkTransformVisualizer.s_LineMaterial.hideFlags = HideFlags.HideAndDontSave;
					NetworkTransformVisualizer.s_LineMaterial.SetInt("_ZWrite", 0);
				}
			}
		}

		// Token: 0x04000227 RID: 551
		[Tooltip("The prefab to use for the visualization object.")]
		[SerializeField]
		private GameObject m_VisualizerPrefab;

		// Token: 0x04000228 RID: 552
		private NetworkTransform m_NetworkTransform;

		// Token: 0x04000229 RID: 553
		private GameObject m_Visualizer;

		// Token: 0x0400022A RID: 554
		private static Material s_LineMaterial;
	}
}
