﻿using System;
using System.Collections.Generic;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking
{
	// Token: 0x02000051 RID: 81
	[AddComponentMenu("Network/NetworkMigrationManager")]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class NetworkMigrationManager : MonoBehaviour
	{
		// Token: 0x06000376 RID: 886 RVA: 0x000125F8 File Offset: 0x000107F8
		public NetworkMigrationManager()
		{
		}

		// Token: 0x06000377 RID: 887 RVA: 0x00012654 File Offset: 0x00010854
		private void AddPendingPlayer(GameObject obj, int connectionId, NetworkInstanceId netId, short playerControllerId)
		{
			if (!this.m_PendingPlayers.ContainsKey(connectionId))
			{
				NetworkMigrationManager.ConnectionPendingPlayers value = default(NetworkMigrationManager.ConnectionPendingPlayers);
				value.players = new List<NetworkMigrationManager.PendingPlayerInfo>();
				this.m_PendingPlayers[connectionId] = value;
			}
			NetworkMigrationManager.PendingPlayerInfo item = default(NetworkMigrationManager.PendingPlayerInfo);
			item.netId = netId;
			item.playerControllerId = playerControllerId;
			item.obj = obj;
			this.m_PendingPlayers[connectionId].players.Add(item);
		}

		// Token: 0x06000378 RID: 888 RVA: 0x000126D4 File Offset: 0x000108D4
		private GameObject FindPendingPlayer(int connectionId, NetworkInstanceId netId, short playerControllerId)
		{
			if (this.m_PendingPlayers.ContainsKey(connectionId))
			{
				for (int i = 0; i < this.m_PendingPlayers[connectionId].players.Count; i++)
				{
					NetworkMigrationManager.PendingPlayerInfo pendingPlayerInfo = this.m_PendingPlayers[connectionId].players[i];
					if (pendingPlayerInfo.netId == netId && pendingPlayerInfo.playerControllerId == playerControllerId)
					{
						return pendingPlayerInfo.obj;
					}
				}
			}
			return null;
		}

		// Token: 0x06000379 RID: 889 RVA: 0x00012772 File Offset: 0x00010972
		private void RemovePendingPlayer(int connectionId)
		{
			this.m_PendingPlayers.Remove(connectionId);
		}

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x0600037A RID: 890 RVA: 0x00012784 File Offset: 0x00010984
		// (set) Token: 0x0600037B RID: 891 RVA: 0x0001279F File Offset: 0x0001099F
		public bool hostMigration
		{
			get
			{
				return this.m_HostMigration;
			}
			set
			{
				this.m_HostMigration = value;
			}
		}

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x0600037C RID: 892 RVA: 0x000127AC File Offset: 0x000109AC
		// (set) Token: 0x0600037D RID: 893 RVA: 0x000127C7 File Offset: 0x000109C7
		public bool showGUI
		{
			get
			{
				return this.m_ShowGUI;
			}
			set
			{
				this.m_ShowGUI = value;
			}
		}

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x0600037E RID: 894 RVA: 0x000127D4 File Offset: 0x000109D4
		// (set) Token: 0x0600037F RID: 895 RVA: 0x000127EF File Offset: 0x000109EF
		public int offsetX
		{
			get
			{
				return this.m_OffsetX;
			}
			set
			{
				this.m_OffsetX = value;
			}
		}

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x06000380 RID: 896 RVA: 0x000127FC File Offset: 0x000109FC
		// (set) Token: 0x06000381 RID: 897 RVA: 0x00012817 File Offset: 0x00010A17
		public int offsetY
		{
			get
			{
				return this.m_OffsetY;
			}
			set
			{
				this.m_OffsetY = value;
			}
		}

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x06000382 RID: 898 RVA: 0x00012824 File Offset: 0x00010A24
		public NetworkClient client
		{
			get
			{
				return this.m_Client;
			}
		}

		// Token: 0x17000093 RID: 147
		// (get) Token: 0x06000383 RID: 899 RVA: 0x00012840 File Offset: 0x00010A40
		// (set) Token: 0x06000384 RID: 900 RVA: 0x0001285B File Offset: 0x00010A5B
		public bool waitingToBecomeNewHost
		{
			get
			{
				return this.m_WaitingToBecomeNewHost;
			}
			set
			{
				this.m_WaitingToBecomeNewHost = value;
			}
		}

		// Token: 0x17000094 RID: 148
		// (get) Token: 0x06000385 RID: 901 RVA: 0x00012868 File Offset: 0x00010A68
		// (set) Token: 0x06000386 RID: 902 RVA: 0x00012883 File Offset: 0x00010A83
		public bool waitingReconnectToNewHost
		{
			get
			{
				return this.m_WaitingReconnectToNewHost;
			}
			set
			{
				this.m_WaitingReconnectToNewHost = value;
			}
		}

		// Token: 0x17000095 RID: 149
		// (get) Token: 0x06000387 RID: 903 RVA: 0x00012890 File Offset: 0x00010A90
		public bool disconnectedFromHost
		{
			get
			{
				return this.m_DisconnectedFromHost;
			}
		}

		// Token: 0x17000096 RID: 150
		// (get) Token: 0x06000388 RID: 904 RVA: 0x000128AC File Offset: 0x00010AAC
		public bool hostWasShutdown
		{
			get
			{
				return this.m_HostWasShutdown;
			}
		}

		// Token: 0x17000097 RID: 151
		// (get) Token: 0x06000389 RID: 905 RVA: 0x000128C8 File Offset: 0x00010AC8
		public MatchInfo matchInfo
		{
			get
			{
				return this.m_MatchInfo;
			}
		}

		// Token: 0x17000098 RID: 152
		// (get) Token: 0x0600038A RID: 906 RVA: 0x000128E4 File Offset: 0x00010AE4
		public int oldServerConnectionId
		{
			get
			{
				return this.m_OldServerConnectionId;
			}
		}

		// Token: 0x17000099 RID: 153
		// (get) Token: 0x0600038B RID: 907 RVA: 0x00012900 File Offset: 0x00010B00
		// (set) Token: 0x0600038C RID: 908 RVA: 0x0001291B File Offset: 0x00010B1B
		public string newHostAddress
		{
			get
			{
				return this.m_NewHostAddress;
			}
			set
			{
				this.m_NewHostAddress = value;
			}
		}

		// Token: 0x1700009A RID: 154
		// (get) Token: 0x0600038D RID: 909 RVA: 0x00012928 File Offset: 0x00010B28
		public PeerInfoMessage[] peers
		{
			get
			{
				return this.m_Peers;
			}
		}

		// Token: 0x1700009B RID: 155
		// (get) Token: 0x0600038E RID: 910 RVA: 0x00012944 File Offset: 0x00010B44
		public Dictionary<int, NetworkMigrationManager.ConnectionPendingPlayers> pendingPlayers
		{
			get
			{
				return this.m_PendingPlayers;
			}
		}

		// Token: 0x0600038F RID: 911 RVA: 0x0001295F File Offset: 0x00010B5F
		private void Start()
		{
			this.Reset(-1);
		}

		// Token: 0x06000390 RID: 912 RVA: 0x0001296C File Offset: 0x00010B6C
		public void Reset(int reconnectId)
		{
			this.m_OldServerConnectionId = -1;
			this.m_WaitingToBecomeNewHost = false;
			this.m_WaitingReconnectToNewHost = false;
			this.m_DisconnectedFromHost = false;
			this.m_HostWasShutdown = false;
			ClientScene.SetReconnectId(reconnectId, this.m_Peers);
			if (NetworkManager.singleton != null)
			{
				NetworkManager.singleton.SetupMigrationManager(this);
			}
		}

		// Token: 0x06000391 RID: 913 RVA: 0x000129C8 File Offset: 0x00010BC8
		internal void AssignAuthorityCallback(NetworkConnection conn, NetworkIdentity uv, bool authorityState)
		{
			PeerAuthorityMessage peerAuthorityMessage = new PeerAuthorityMessage();
			peerAuthorityMessage.connectionId = conn.connectionId;
			peerAuthorityMessage.netId = uv.netId;
			peerAuthorityMessage.authorityState = authorityState;
			if (LogFilter.logDebug)
			{
				Debug.Log("AssignAuthorityCallback send for netId" + uv.netId);
			}
			for (int i = 0; i < NetworkServer.connections.Count; i++)
			{
				NetworkConnection networkConnection = NetworkServer.connections[i];
				if (networkConnection != null)
				{
					networkConnection.Send(18, peerAuthorityMessage);
				}
			}
		}

		// Token: 0x06000392 RID: 914 RVA: 0x00012A5C File Offset: 0x00010C5C
		public void Initialize(NetworkClient newClient, MatchInfo newMatchInfo)
		{
			if (LogFilter.logDev)
			{
				Debug.Log("NetworkMigrationManager initialize");
			}
			this.m_Client = newClient;
			this.m_MatchInfo = newMatchInfo;
			newClient.RegisterHandlerSafe(11, new NetworkMessageDelegate(this.OnPeerInfo));
			newClient.RegisterHandlerSafe(18, new NetworkMessageDelegate(this.OnPeerClientAuthority));
			NetworkIdentity.clientAuthorityCallback = new NetworkIdentity.ClientAuthorityCallback(this.AssignAuthorityCallback);
		}

		// Token: 0x06000393 RID: 915 RVA: 0x00012AC8 File Offset: 0x00010CC8
		public void DisablePlayerObjects()
		{
			if (LogFilter.logDev)
			{
				Debug.Log("NetworkMigrationManager DisablePlayerObjects");
			}
			if (this.m_Peers != null)
			{
				for (int i = 0; i < this.m_Peers.Length; i++)
				{
					PeerInfoMessage peerInfoMessage = this.m_Peers[i];
					if (peerInfoMessage.playerIds != null)
					{
						for (int j = 0; j < peerInfoMessage.playerIds.Length; j++)
						{
							PeerInfoPlayer peerInfoPlayer = peerInfoMessage.playerIds[j];
							if (LogFilter.logDev)
							{
								Debug.Log(string.Concat(new object[]
								{
									"DisablePlayerObjects disable player for ",
									peerInfoMessage.address,
									" netId:",
									peerInfoPlayer.netId,
									" control:",
									peerInfoPlayer.playerControllerId
								}));
							}
							GameObject gameObject = ClientScene.FindLocalObject(peerInfoPlayer.netId);
							if (gameObject != null)
							{
								gameObject.SetActive(false);
								this.AddPendingPlayer(gameObject, peerInfoMessage.connectionId, peerInfoPlayer.netId, peerInfoPlayer.playerControllerId);
							}
							else if (LogFilter.logWarn)
							{
								Debug.LogWarning(string.Concat(new object[]
								{
									"DisablePlayerObjects didnt find player Conn:",
									peerInfoMessage.connectionId,
									" NetId:",
									peerInfoPlayer.netId
								}));
							}
						}
					}
				}
			}
		}

		// Token: 0x06000394 RID: 916 RVA: 0x00012C48 File Offset: 0x00010E48
		public void SendPeerInfo()
		{
			if (this.m_HostMigration)
			{
				PeerListMessage peerListMessage = new PeerListMessage();
				List<PeerInfoMessage> list = new List<PeerInfoMessage>();
				for (int i = 0; i < NetworkServer.connections.Count; i++)
				{
					NetworkConnection networkConnection = NetworkServer.connections[i];
					if (networkConnection != null)
					{
						PeerInfoMessage peerInfoMessage = new PeerInfoMessage();
						string address;
						int port;
						NetworkID networkID;
						NodeID nodeID;
						byte b;
						NetworkManager.activeTransport.GetConnectionInfo(NetworkServer.serverHostId, networkConnection.connectionId, out address, out port, out networkID, out nodeID, out b);
						peerInfoMessage.connectionId = networkConnection.connectionId;
						peerInfoMessage.port = port;
						if (i == 0)
						{
							peerInfoMessage.port = NetworkServer.listenPort;
							peerInfoMessage.isHost = true;
							peerInfoMessage.address = "<host>";
						}
						else
						{
							peerInfoMessage.address = address;
							peerInfoMessage.isHost = false;
						}
						List<PeerInfoPlayer> list2 = new List<PeerInfoPlayer>();
						for (int j = 0; j < networkConnection.playerControllers.Count; j++)
						{
							PlayerController playerController = networkConnection.playerControllers[j];
							if (playerController != null && playerController.unetView != null)
							{
								PeerInfoPlayer item;
								item.netId = playerController.unetView.netId;
								item.playerControllerId = playerController.unetView.playerControllerId;
								list2.Add(item);
							}
						}
						if (networkConnection.clientOwnedObjects != null)
						{
							foreach (NetworkInstanceId netId in networkConnection.clientOwnedObjects)
							{
								GameObject gameObject = NetworkServer.FindLocalObject(netId);
								if (!(gameObject == null))
								{
									NetworkIdentity component = gameObject.GetComponent<NetworkIdentity>();
									if (component.playerControllerId == -1)
									{
										PeerInfoPlayer item2;
										item2.netId = netId;
										item2.playerControllerId = -1;
										list2.Add(item2);
									}
								}
							}
						}
						if (list2.Count > 0)
						{
							peerInfoMessage.playerIds = list2.ToArray();
						}
						list.Add(peerInfoMessage);
					}
				}
				peerListMessage.peers = list.ToArray();
				for (int k = 0; k < NetworkServer.connections.Count; k++)
				{
					NetworkConnection networkConnection2 = NetworkServer.connections[k];
					if (networkConnection2 != null)
					{
						peerListMessage.oldServerConnectionId = networkConnection2.connectionId;
						networkConnection2.Send(11, peerListMessage);
					}
				}
			}
		}

		// Token: 0x06000395 RID: 917 RVA: 0x00012ED0 File Offset: 0x000110D0
		private void OnPeerClientAuthority(NetworkMessage netMsg)
		{
			PeerAuthorityMessage peerAuthorityMessage = netMsg.ReadMessage<PeerAuthorityMessage>();
			if (LogFilter.logDebug)
			{
				Debug.Log("OnPeerClientAuthority for netId:" + peerAuthorityMessage.netId);
			}
			if (this.m_Peers != null)
			{
				for (int i = 0; i < this.m_Peers.Length; i++)
				{
					PeerInfoMessage peerInfoMessage = this.m_Peers[i];
					if (peerInfoMessage.connectionId == peerAuthorityMessage.connectionId)
					{
						if (peerInfoMessage.playerIds == null)
						{
							peerInfoMessage.playerIds = new PeerInfoPlayer[0];
						}
						if (peerAuthorityMessage.authorityState)
						{
							for (int j = 0; j < peerInfoMessage.playerIds.Length; j++)
							{
								if (peerInfoMessage.playerIds[j].netId == peerAuthorityMessage.netId)
								{
									return;
								}
							}
							PeerInfoPlayer item = default(PeerInfoPlayer);
							item.netId = peerAuthorityMessage.netId;
							item.playerControllerId = -1;
							peerInfoMessage.playerIds = new List<PeerInfoPlayer>(peerInfoMessage.playerIds)
							{
								item
							}.ToArray();
						}
						else
						{
							for (int k = 0; k < peerInfoMessage.playerIds.Length; k++)
							{
								if (peerInfoMessage.playerIds[k].netId == peerAuthorityMessage.netId)
								{
									List<PeerInfoPlayer> list = new List<PeerInfoPlayer>(peerInfoMessage.playerIds);
									list.RemoveAt(k);
									peerInfoMessage.playerIds = list.ToArray();
									break;
								}
							}
						}
					}
				}
				GameObject go = ClientScene.FindLocalObject(peerAuthorityMessage.netId);
				this.OnAuthorityUpdated(go, peerAuthorityMessage.connectionId, peerAuthorityMessage.authorityState);
			}
		}

		// Token: 0x06000396 RID: 918 RVA: 0x00013090 File Offset: 0x00011290
		private void OnPeerInfo(NetworkMessage netMsg)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("OnPeerInfo");
			}
			netMsg.ReadMessage<PeerListMessage>(this.m_PeerListMessage);
			this.m_Peers = this.m_PeerListMessage.peers;
			this.m_OldServerConnectionId = this.m_PeerListMessage.oldServerConnectionId;
			for (int i = 0; i < this.m_Peers.Length; i++)
			{
				if (LogFilter.logDebug)
				{
					Debug.Log(string.Concat(new object[]
					{
						"peer conn ",
						this.m_Peers[i].connectionId,
						" your conn ",
						this.m_PeerListMessage.oldServerConnectionId
					}));
				}
				if (this.m_Peers[i].connectionId == this.m_PeerListMessage.oldServerConnectionId)
				{
					this.m_Peers[i].isYou = true;
					break;
				}
			}
			this.OnPeersUpdated(this.m_PeerListMessage);
		}

		// Token: 0x06000397 RID: 919 RVA: 0x00013190 File Offset: 0x00011390
		private void OnServerReconnectPlayerMessage(NetworkMessage netMsg)
		{
			ReconnectMessage reconnectMessage = netMsg.ReadMessage<ReconnectMessage>();
			if (LogFilter.logDev)
			{
				Debug.Log(string.Concat(new object[]
				{
					"OnReconnectMessage: connId=",
					reconnectMessage.oldConnectionId,
					" playerControllerId:",
					reconnectMessage.playerControllerId,
					" netId:",
					reconnectMessage.netId
				}));
			}
			GameObject gameObject = this.FindPendingPlayer(reconnectMessage.oldConnectionId, reconnectMessage.netId, reconnectMessage.playerControllerId);
			if (gameObject == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError(string.Concat(new object[]
					{
						"OnReconnectMessage connId=",
						reconnectMessage.oldConnectionId,
						" player null for netId:",
						reconnectMessage.netId,
						" msg.playerControllerId:",
						reconnectMessage.playerControllerId
					}));
				}
			}
			else if (gameObject.activeSelf)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("OnReconnectMessage connId=" + reconnectMessage.oldConnectionId + " player already active?");
				}
			}
			else
			{
				if (LogFilter.logDebug)
				{
					Debug.Log("OnReconnectMessage: player=" + gameObject);
				}
				NetworkReader networkReader = null;
				if (reconnectMessage.msgSize != 0)
				{
					networkReader = new NetworkReader(reconnectMessage.msgData);
				}
				if (reconnectMessage.playerControllerId != -1)
				{
					if (networkReader == null)
					{
						this.OnServerReconnectPlayer(netMsg.conn, gameObject, reconnectMessage.oldConnectionId, reconnectMessage.playerControllerId);
					}
					else
					{
						this.OnServerReconnectPlayer(netMsg.conn, gameObject, reconnectMessage.oldConnectionId, reconnectMessage.playerControllerId, networkReader);
					}
				}
				else
				{
					this.OnServerReconnectObject(netMsg.conn, gameObject, reconnectMessage.oldConnectionId);
				}
			}
		}

		// Token: 0x06000398 RID: 920 RVA: 0x0001336C File Offset: 0x0001156C
		public bool ReconnectObjectForConnection(NetworkConnection newConnection, GameObject oldObject, int oldConnectionId)
		{
			bool result;
			if (!NetworkServer.active)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("ReconnectObjectForConnection must have active server");
				}
				result = false;
			}
			else
			{
				if (LogFilter.logDebug)
				{
					Debug.Log(string.Concat(new object[]
					{
						"ReconnectObjectForConnection: oldConnId=",
						oldConnectionId,
						" obj=",
						oldObject,
						" conn:",
						newConnection
					}));
				}
				if (!this.m_PendingPlayers.ContainsKey(oldConnectionId))
				{
					if (LogFilter.logError)
					{
						Debug.LogError("ReconnectObjectForConnection oldConnId=" + oldConnectionId + " not found.");
					}
					result = false;
				}
				else
				{
					oldObject.SetActive(true);
					oldObject.GetComponent<NetworkIdentity>().SetNetworkInstanceId(new NetworkInstanceId(0U));
					if (!NetworkServer.SpawnWithClientAuthority(oldObject, newConnection))
					{
						if (LogFilter.logError)
						{
							Debug.LogError("ReconnectObjectForConnection oldConnId=" + oldConnectionId + " SpawnWithClientAuthority failed.");
						}
						result = false;
					}
					else
					{
						result = true;
					}
				}
			}
			return result;
		}

		// Token: 0x06000399 RID: 921 RVA: 0x00013480 File Offset: 0x00011680
		public bool ReconnectPlayerForConnection(NetworkConnection newConnection, GameObject oldPlayer, int oldConnectionId, short playerControllerId)
		{
			bool result;
			if (!NetworkServer.active)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("ReconnectPlayerForConnection must have active server");
				}
				result = false;
			}
			else
			{
				if (LogFilter.logDebug)
				{
					Debug.Log(string.Concat(new object[]
					{
						"ReconnectPlayerForConnection: oldConnId=",
						oldConnectionId,
						" player=",
						oldPlayer,
						" conn:",
						newConnection
					}));
				}
				if (!this.m_PendingPlayers.ContainsKey(oldConnectionId))
				{
					if (LogFilter.logError)
					{
						Debug.LogError("ReconnectPlayerForConnection oldConnId=" + oldConnectionId + " not found.");
					}
					result = false;
				}
				else
				{
					oldPlayer.SetActive(true);
					NetworkServer.Spawn(oldPlayer);
					if (!NetworkServer.AddPlayerForConnection(newConnection, oldPlayer, playerControllerId))
					{
						if (LogFilter.logError)
						{
							Debug.LogError("ReconnectPlayerForConnection oldConnId=" + oldConnectionId + " AddPlayerForConnection failed.");
						}
						result = false;
					}
					else
					{
						if (NetworkServer.localClientActive)
						{
							this.SendPeerInfo();
						}
						result = true;
					}
				}
			}
			return result;
		}

		// Token: 0x0600039A RID: 922 RVA: 0x000135A0 File Offset: 0x000117A0
		public bool LostHostOnClient(NetworkConnection conn)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkMigrationManager client OnDisconnectedFromHost");
			}
			bool result;
			if (Application.platform == RuntimePlatform.WebGLPlayer)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("LostHostOnClient: Host migration not supported on WebGL");
				}
				result = false;
			}
			else if (this.m_Client == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkMigrationManager LostHostOnHost client was never initialized.");
				}
				result = false;
			}
			else if (!this.m_HostMigration)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkMigrationManager LostHostOnHost migration not enabled.");
				}
				result = false;
			}
			else
			{
				this.m_DisconnectedFromHost = true;
				this.DisablePlayerObjects();
				byte b;
				NetworkManager.activeTransport.Disconnect(this.m_Client.hostId, this.m_Client.connection.connectionId, out b);
				if (this.m_OldServerConnectionId != -1)
				{
					NetworkMigrationManager.SceneChangeOption sceneChangeOption;
					this.OnClientDisconnectedFromHost(conn, out sceneChangeOption);
					result = (sceneChangeOption == NetworkMigrationManager.SceneChangeOption.StayInOnlineScene);
				}
				else
				{
					result = false;
				}
			}
			return result;
		}

		// Token: 0x0600039B RID: 923 RVA: 0x000136A0 File Offset: 0x000118A0
		public void LostHostOnHost()
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkMigrationManager LostHostOnHost");
			}
			if (Application.platform == RuntimePlatform.WebGLPlayer)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("LostHostOnHost: Host migration not supported on WebGL");
				}
			}
			else
			{
				this.OnServerHostShutdown();
				if (this.m_Peers == null)
				{
					if (LogFilter.logError)
					{
						Debug.LogError("NetworkMigrationManager LostHostOnHost no peers");
					}
				}
				else if (this.m_Peers.Length != 1)
				{
					this.m_HostWasShutdown = true;
				}
			}
		}

		// Token: 0x0600039C RID: 924 RVA: 0x00013730 File Offset: 0x00011930
		public bool BecomeNewHost(int port)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkMigrationManager BecomeNewHost " + this.m_MatchInfo);
			}
			NetworkServer.RegisterHandler(47, new NetworkMessageDelegate(this.OnServerReconnectPlayerMessage));
			NetworkClient networkClient = NetworkServer.BecomeHost(this.m_Client, port, this.m_MatchInfo, this.oldServerConnectionId, this.peers);
			bool result;
			if (networkClient != null)
			{
				if (NetworkManager.singleton != null)
				{
					NetworkManager.singleton.RegisterServerMessages();
					NetworkManager.singleton.UseExternalClient(networkClient);
				}
				else
				{
					Debug.LogWarning("MigrationManager BecomeNewHost - No NetworkManager.");
				}
				networkClient.RegisterHandlerSafe(11, new NetworkMessageDelegate(this.OnPeerInfo));
				this.RemovePendingPlayer(this.m_OldServerConnectionId);
				this.Reset(-1);
				this.SendPeerInfo();
				result = true;
			}
			else
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkServer.BecomeHost failed");
				}
				result = false;
			}
			return result;
		}

		// Token: 0x0600039D RID: 925 RVA: 0x00013823 File Offset: 0x00011A23
		protected virtual void OnClientDisconnectedFromHost(NetworkConnection conn, out NetworkMigrationManager.SceneChangeOption sceneChange)
		{
			sceneChange = NetworkMigrationManager.SceneChangeOption.StayInOnlineScene;
		}

		// Token: 0x0600039E RID: 926 RVA: 0x00013829 File Offset: 0x00011A29
		protected virtual void OnServerHostShutdown()
		{
		}

		// Token: 0x0600039F RID: 927 RVA: 0x0001382C File Offset: 0x00011A2C
		protected virtual void OnServerReconnectPlayer(NetworkConnection newConnection, GameObject oldPlayer, int oldConnectionId, short playerControllerId)
		{
			this.ReconnectPlayerForConnection(newConnection, oldPlayer, oldConnectionId, playerControllerId);
		}

		// Token: 0x060003A0 RID: 928 RVA: 0x0001383B File Offset: 0x00011A3B
		protected virtual void OnServerReconnectPlayer(NetworkConnection newConnection, GameObject oldPlayer, int oldConnectionId, short playerControllerId, NetworkReader extraMessageReader)
		{
			this.ReconnectPlayerForConnection(newConnection, oldPlayer, oldConnectionId, playerControllerId);
		}

		// Token: 0x060003A1 RID: 929 RVA: 0x0001384A File Offset: 0x00011A4A
		protected virtual void OnServerReconnectObject(NetworkConnection newConnection, GameObject oldObject, int oldConnectionId)
		{
			this.ReconnectObjectForConnection(newConnection, oldObject, oldConnectionId);
		}

		// Token: 0x060003A2 RID: 930 RVA: 0x00013857 File Offset: 0x00011A57
		protected virtual void OnPeersUpdated(PeerListMessage peers)
		{
			if (LogFilter.logDev)
			{
				Debug.Log("NetworkMigrationManager NumPeers " + peers.peers.Length);
			}
		}

		// Token: 0x060003A3 RID: 931 RVA: 0x00013884 File Offset: 0x00011A84
		protected virtual void OnAuthorityUpdated(GameObject go, int connectionId, bool authorityState)
		{
			if (LogFilter.logDev)
			{
				Debug.Log(string.Concat(new object[]
				{
					"NetworkMigrationManager OnAuthorityUpdated for ",
					go,
					" conn:",
					connectionId,
					" state:",
					authorityState
				}));
			}
		}

		// Token: 0x060003A4 RID: 932 RVA: 0x000138DC File Offset: 0x00011ADC
		public virtual bool FindNewHost(out PeerInfoMessage newHostInfo, out bool youAreNewHost)
		{
			bool result;
			if (this.m_Peers == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkMigrationManager FindLowestHost no peers");
				}
				newHostInfo = null;
				youAreNewHost = false;
				result = false;
			}
			else
			{
				if (LogFilter.logDev)
				{
					Debug.Log("NetworkMigrationManager FindLowestHost");
				}
				newHostInfo = new PeerInfoMessage();
				newHostInfo.connectionId = 50000;
				newHostInfo.address = "";
				newHostInfo.port = 0;
				int num = -1;
				youAreNewHost = false;
				for (int i = 0; i < this.m_Peers.Length; i++)
				{
					PeerInfoMessage peerInfoMessage = this.m_Peers[i];
					if (peerInfoMessage.connectionId != 0)
					{
						if (!peerInfoMessage.isHost)
						{
							if (peerInfoMessage.isYou)
							{
								num = peerInfoMessage.connectionId;
							}
							if (peerInfoMessage.connectionId < newHostInfo.connectionId)
							{
								newHostInfo = peerInfoMessage;
							}
						}
					}
				}
				if (newHostInfo.connectionId == 50000)
				{
					result = false;
				}
				else
				{
					if (newHostInfo.connectionId == num)
					{
						youAreNewHost = true;
					}
					if (LogFilter.logDev)
					{
						Debug.Log("FindNewHost new host is " + newHostInfo.address);
					}
					result = true;
				}
			}
			return result;
		}

		// Token: 0x060003A5 RID: 933 RVA: 0x00013A24 File Offset: 0x00011C24
		private void OnGUIHost()
		{
			int num = this.m_OffsetY;
			GUI.Label(new Rect((float)this.m_OffsetX, (float)num, 200f, 40f), "Host Was Shutdown ID(" + this.m_OldServerConnectionId + ")");
			num += 25;
			if (Application.platform == RuntimePlatform.WebGLPlayer)
			{
				GUI.Label(new Rect((float)this.m_OffsetX, (float)num, 200f, 40f), "Host Migration not supported for WebGL");
			}
			else
			{
				if (this.m_WaitingReconnectToNewHost)
				{
					if (GUI.Button(new Rect((float)this.m_OffsetX, (float)num, 200f, 20f), "Reconnect as Client"))
					{
						this.Reset(0);
						if (NetworkManager.singleton != null)
						{
							NetworkManager.singleton.networkAddress = GUI.TextField(new Rect((float)(this.m_OffsetX + 100), (float)num, 95f, 20f), NetworkManager.singleton.networkAddress);
							NetworkManager.singleton.StartClient();
						}
						else
						{
							Debug.LogWarning("MigrationManager Old Host Reconnect - No NetworkManager.");
						}
					}
					num += 25;
				}
				else
				{
					if (GUI.Button(new Rect((float)this.m_OffsetX, (float)num, 200f, 20f), "Pick New Host"))
					{
						bool flag;
						if (this.FindNewHost(out this.m_NewHostInfo, out flag))
						{
							this.m_NewHostAddress = this.m_NewHostInfo.address;
							if (flag)
							{
								Debug.LogWarning("MigrationManager FindNewHost - new host is self?");
							}
							else
							{
								this.m_WaitingReconnectToNewHost = true;
							}
						}
					}
					num += 25;
				}
				if (GUI.Button(new Rect((float)this.m_OffsetX, (float)num, 200f, 20f), "Leave Game"))
				{
					if (NetworkManager.singleton != null)
					{
						NetworkManager.singleton.SetupMigrationManager(null);
						NetworkManager.singleton.StopHost();
					}
					else
					{
						Debug.LogWarning("MigrationManager Old Host LeaveGame - No NetworkManager.");
					}
					this.Reset(-1);
				}
				num += 25;
			}
		}

		// Token: 0x060003A6 RID: 934 RVA: 0x00013C38 File Offset: 0x00011E38
		private void OnGUIClient()
		{
			int num = this.m_OffsetY;
			GUI.Label(new Rect((float)this.m_OffsetX, (float)num, 200f, 40f), "Lost Connection To Host ID(" + this.m_OldServerConnectionId + ")");
			num += 25;
			if (Application.platform == RuntimePlatform.WebGLPlayer)
			{
				GUI.Label(new Rect((float)this.m_OffsetX, (float)num, 200f, 40f), "Host Migration not supported for WebGL");
			}
			else
			{
				if (this.m_WaitingToBecomeNewHost)
				{
					GUI.Label(new Rect((float)this.m_OffsetX, (float)num, 200f, 40f), "You are the new host");
					num += 25;
					if (GUI.Button(new Rect((float)this.m_OffsetX, (float)num, 200f, 20f), "Start As Host"))
					{
						if (NetworkManager.singleton != null)
						{
							this.BecomeNewHost(NetworkManager.singleton.networkPort);
						}
						else
						{
							Debug.LogWarning("MigrationManager Client BecomeNewHost - No NetworkManager.");
						}
					}
					num += 25;
				}
				else if (this.m_WaitingReconnectToNewHost)
				{
					GUI.Label(new Rect((float)this.m_OffsetX, (float)num, 200f, 40f), "New host is " + this.m_NewHostAddress);
					num += 25;
					if (GUI.Button(new Rect((float)this.m_OffsetX, (float)num, 200f, 20f), "Reconnect To New Host"))
					{
						this.Reset(this.m_OldServerConnectionId);
						if (NetworkManager.singleton != null)
						{
							NetworkManager.singleton.networkAddress = this.m_NewHostAddress;
							NetworkManager.singleton.client.ReconnectToNewHost(this.m_NewHostAddress, NetworkManager.singleton.networkPort);
						}
						else
						{
							Debug.LogWarning("MigrationManager Client reconnect - No NetworkManager.");
						}
					}
					num += 25;
				}
				else
				{
					if (GUI.Button(new Rect((float)this.m_OffsetX, (float)num, 200f, 20f), "Pick New Host"))
					{
						bool flag;
						if (this.FindNewHost(out this.m_NewHostInfo, out flag))
						{
							this.m_NewHostAddress = this.m_NewHostInfo.address;
							if (flag)
							{
								this.m_WaitingToBecomeNewHost = true;
							}
							else
							{
								this.m_WaitingReconnectToNewHost = true;
							}
						}
					}
					num += 25;
				}
				if (GUI.Button(new Rect((float)this.m_OffsetX, (float)num, 200f, 20f), "Leave Game"))
				{
					if (NetworkManager.singleton != null)
					{
						NetworkManager.singleton.SetupMigrationManager(null);
						NetworkManager.singleton.StopHost();
					}
					else
					{
						Debug.LogWarning("MigrationManager Client LeaveGame - No NetworkManager.");
					}
					this.Reset(-1);
				}
				num += 25;
			}
		}

		// Token: 0x060003A7 RID: 935 RVA: 0x00013F0C File Offset: 0x0001210C
		private void OnGUI()
		{
			if (this.m_ShowGUI)
			{
				if (this.m_HostWasShutdown)
				{
					this.OnGUIHost();
				}
				else if (this.m_DisconnectedFromHost && this.m_OldServerConnectionId != -1)
				{
					this.OnGUIClient();
				}
			}
		}

		// Token: 0x04000190 RID: 400
		[SerializeField]
		private bool m_HostMigration = true;

		// Token: 0x04000191 RID: 401
		[SerializeField]
		private bool m_ShowGUI = true;

		// Token: 0x04000192 RID: 402
		[SerializeField]
		private int m_OffsetX = 10;

		// Token: 0x04000193 RID: 403
		[SerializeField]
		private int m_OffsetY = 300;

		// Token: 0x04000194 RID: 404
		private NetworkClient m_Client;

		// Token: 0x04000195 RID: 405
		private bool m_WaitingToBecomeNewHost;

		// Token: 0x04000196 RID: 406
		private bool m_WaitingReconnectToNewHost;

		// Token: 0x04000197 RID: 407
		private bool m_DisconnectedFromHost;

		// Token: 0x04000198 RID: 408
		private bool m_HostWasShutdown;

		// Token: 0x04000199 RID: 409
		private MatchInfo m_MatchInfo;

		// Token: 0x0400019A RID: 410
		private int m_OldServerConnectionId = -1;

		// Token: 0x0400019B RID: 411
		private string m_NewHostAddress;

		// Token: 0x0400019C RID: 412
		private PeerInfoMessage m_NewHostInfo = new PeerInfoMessage();

		// Token: 0x0400019D RID: 413
		private PeerListMessage m_PeerListMessage = new PeerListMessage();

		// Token: 0x0400019E RID: 414
		private PeerInfoMessage[] m_Peers;

		// Token: 0x0400019F RID: 415
		private Dictionary<int, NetworkMigrationManager.ConnectionPendingPlayers> m_PendingPlayers = new Dictionary<int, NetworkMigrationManager.ConnectionPendingPlayers>();

		// Token: 0x02000052 RID: 82
		public enum SceneChangeOption
		{
			// Token: 0x040001A1 RID: 417
			StayInOnlineScene,
			// Token: 0x040001A2 RID: 418
			SwitchToOfflineScene
		}

		// Token: 0x02000053 RID: 83
		public struct PendingPlayerInfo
		{
			// Token: 0x040001A3 RID: 419
			public NetworkInstanceId netId;

			// Token: 0x040001A4 RID: 420
			public short playerControllerId;

			// Token: 0x040001A5 RID: 421
			public GameObject obj;
		}

		// Token: 0x02000054 RID: 84
		public struct ConnectionPendingPlayers
		{
			// Token: 0x040001A6 RID: 422
			public List<NetworkMigrationManager.PendingPlayerInfo> players;
		}
	}
}
