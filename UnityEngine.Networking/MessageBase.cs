﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x0200001A RID: 26
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public abstract class MessageBase
	{
		// Token: 0x060000CF RID: 207 RVA: 0x000081F2 File Offset: 0x000063F2
		protected MessageBase()
		{
		}

		// Token: 0x060000D0 RID: 208 RVA: 0x000081FA File Offset: 0x000063FA
		public virtual void Deserialize(NetworkReader reader)
		{
		}

		// Token: 0x060000D1 RID: 209 RVA: 0x000081FD File Offset: 0x000063FD
		public virtual void Serialize(NetworkWriter writer)
		{
		}
	}
}
