﻿using System;
using System.Net;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking
{
	// Token: 0x02000013 RID: 19
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public interface INetworkTransport
	{
		// Token: 0x0600008F RID: 143
		void Init();

		// Token: 0x06000090 RID: 144
		void Init(GlobalConfig config);

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000091 RID: 145
		bool IsStarted { get; }

		// Token: 0x06000092 RID: 146
		void Shutdown();

		// Token: 0x06000093 RID: 147
		int AddHost(HostTopology topology, int port, string ip);

		// Token: 0x06000094 RID: 148
		int AddWebsocketHost(HostTopology topology, int port, string ip);

		// Token: 0x06000095 RID: 149
		int ConnectWithSimulator(int hostId, string address, int port, int specialConnectionId, out byte error, ConnectionSimulatorConfig conf);

		// Token: 0x06000096 RID: 150
		int Connect(int hostId, string address, int port, int specialConnectionId, out byte error);

		// Token: 0x06000097 RID: 151
		void ConnectAsNetworkHost(int hostId, string address, int port, NetworkID network, SourceID source, NodeID node, out byte error);

		// Token: 0x06000098 RID: 152
		int ConnectToNetworkPeer(int hostId, string address, int port, int specialConnectionId, int relaySlotId, NetworkID network, SourceID source, NodeID node, out byte error);

		// Token: 0x06000099 RID: 153
		int ConnectEndPoint(int hostId, EndPoint endPoint, int specialConnectionId, out byte error);

		// Token: 0x0600009A RID: 154
		bool DoesEndPointUsePlatformProtocols(EndPoint endPoint);

		// Token: 0x0600009B RID: 155
		int AddHostWithSimulator(HostTopology topology, int minTimeout, int maxTimeout, int port);

		// Token: 0x0600009C RID: 156
		bool RemoveHost(int hostId);

		// Token: 0x0600009D RID: 157
		bool Send(int hostId, int connectionId, int channelId, byte[] buffer, int size, out byte error);

		// Token: 0x0600009E RID: 158
		NetworkEventType Receive(out int hostId, out int connectionId, out int channelId, byte[] buffer, int bufferSize, out int receivedSize, out byte error);

		// Token: 0x0600009F RID: 159
		NetworkEventType ReceiveFromHost(int hostId, out int connectionId, out int channelId, byte[] buffer, int bufferSize, out int receivedSize, out byte error);

		// Token: 0x060000A0 RID: 160
		NetworkEventType ReceiveRelayEventFromHost(int hostId, out byte error);

		// Token: 0x060000A1 RID: 161
		int GetCurrentRTT(int hostId, int connectionId, out byte error);

		// Token: 0x060000A2 RID: 162
		void GetConnectionInfo(int hostId, int connectionId, out string address, out int port, out NetworkID network, out NodeID dstNode, out byte error);

		// Token: 0x060000A3 RID: 163
		bool Disconnect(int hostId, int connectionId, out byte error);

		// Token: 0x060000A4 RID: 164
		void SetBroadcastCredentials(int hostId, int key, int version, int subversion, out byte error);

		// Token: 0x060000A5 RID: 165
		bool StartBroadcastDiscovery(int hostId, int broadcastPort, int key, int version, int subversion, byte[] buffer, int size, int timeout, out byte error);

		// Token: 0x060000A6 RID: 166
		void GetBroadcastConnectionInfo(int hostId, out string address, out int port, out byte error);

		// Token: 0x060000A7 RID: 167
		void GetBroadcastConnectionMessage(int hostId, byte[] buffer, int bufferSize, out int receivedSize, out byte error);

		// Token: 0x060000A8 RID: 168
		void StopBroadcastDiscovery();

		// Token: 0x060000A9 RID: 169
		void SetPacketStat(int direction, int packetStatId, int numMsgs, int numBytes);
	}
}
