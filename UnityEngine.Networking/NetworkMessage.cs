﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000075 RID: 117
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class NetworkMessage
	{
		// Token: 0x060005B1 RID: 1457 RVA: 0x0001EF70 File Offset: 0x0001D170
		public NetworkMessage()
		{
		}

		// Token: 0x060005B2 RID: 1458 RVA: 0x0001EF78 File Offset: 0x0001D178
		public static string Dump(byte[] payload, int sz)
		{
			string text = "[";
			for (int i = 0; i < sz; i++)
			{
				text = text + payload[i] + " ";
			}
			return text + "]";
		}

		// Token: 0x060005B3 RID: 1459 RVA: 0x0001EFC8 File Offset: 0x0001D1C8
		public TMsg ReadMessage<TMsg>() where TMsg : MessageBase, new()
		{
			TMsg result = Activator.CreateInstance<TMsg>();
			result.Deserialize(this.reader);
			return result;
		}

		// Token: 0x060005B4 RID: 1460 RVA: 0x0001EFF7 File Offset: 0x0001D1F7
		public void ReadMessage<TMsg>(TMsg msg) where TMsg : MessageBase
		{
			msg.Deserialize(this.reader);
		}

		// Token: 0x0400026B RID: 619
		public const int MaxMessageSize = 65535;

		// Token: 0x0400026C RID: 620
		public short msgType;

		// Token: 0x0400026D RID: 621
		public NetworkConnection conn;

		// Token: 0x0400026E RID: 622
		public NetworkReader reader;

		// Token: 0x0400026F RID: 623
		public int channelId;
	}
}
