﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000078 RID: 120
	public enum ChannelOption
	{
		// Token: 0x04000275 RID: 629
		MaxPendingBuffers = 1,
		// Token: 0x04000276 RID: 630
		AllowFragmentation,
		// Token: 0x04000277 RID: 631
		MaxPacketSize
	}
}
