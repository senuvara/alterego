﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace UnityEngine.Networking
{
	// Token: 0x0200006E RID: 110
	[EditorBrowsable(EditorBrowsableState.Never)]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public abstract class SyncList<T> : IList<T>, ICollection<T>, IEnumerable<T>, IEnumerable
	{
		// Token: 0x06000585 RID: 1413 RVA: 0x0001E39F File Offset: 0x0001C59F
		protected SyncList()
		{
		}

		// Token: 0x170000E1 RID: 225
		// (get) Token: 0x06000586 RID: 1414 RVA: 0x0001E3B4 File Offset: 0x0001C5B4
		public int Count
		{
			get
			{
				return this.m_Objects.Count;
			}
		}

		// Token: 0x170000E2 RID: 226
		// (get) Token: 0x06000587 RID: 1415 RVA: 0x0001E3D4 File Offset: 0x0001C5D4
		public bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170000E3 RID: 227
		// (get) Token: 0x06000588 RID: 1416 RVA: 0x0001E3EC File Offset: 0x0001C5EC
		// (set) Token: 0x06000589 RID: 1417 RVA: 0x0001E407 File Offset: 0x0001C607
		public SyncList<T>.SyncListChanged Callback
		{
			get
			{
				return this.m_Callback;
			}
			set
			{
				this.m_Callback = value;
			}
		}

		// Token: 0x0600058A RID: 1418
		protected abstract void SerializeItem(NetworkWriter writer, T item);

		// Token: 0x0600058B RID: 1419
		protected abstract T DeserializeItem(NetworkReader reader);

		// Token: 0x0600058C RID: 1420 RVA: 0x0001E411 File Offset: 0x0001C611
		public void InitializeBehaviour(NetworkBehaviour beh, int cmdHash)
		{
			this.m_Behaviour = beh;
			this.m_CmdHash = cmdHash;
		}

		// Token: 0x0600058D RID: 1421 RVA: 0x0001E424 File Offset: 0x0001C624
		private void SendMsg(SyncList<T>.Operation op, int itemIndex, T item)
		{
			if (this.m_Behaviour == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("SyncList not initialized");
				}
			}
			else
			{
				NetworkIdentity component = this.m_Behaviour.GetComponent<NetworkIdentity>();
				if (component == null)
				{
					if (LogFilter.logError)
					{
						Debug.LogError("SyncList no NetworkIdentity");
					}
				}
				else if (component.isServer)
				{
					NetworkWriter networkWriter = new NetworkWriter();
					networkWriter.StartMessage(9);
					networkWriter.Write(component.netId);
					networkWriter.WritePackedUInt32((uint)this.m_CmdHash);
					networkWriter.Write((byte)op);
					networkWriter.WritePackedUInt32((uint)itemIndex);
					this.SerializeItem(networkWriter, item);
					networkWriter.FinishMessage();
					NetworkServer.SendWriterToReady(component.gameObject, networkWriter, this.m_Behaviour.GetNetworkChannel());
					if (this.m_Behaviour.isServer && this.m_Behaviour.isClient && this.m_Callback != null)
					{
						this.m_Callback(op, itemIndex);
					}
				}
			}
		}

		// Token: 0x0600058E RID: 1422 RVA: 0x0001E538 File Offset: 0x0001C738
		private void SendMsg(SyncList<T>.Operation op, int itemIndex)
		{
			this.SendMsg(op, itemIndex, default(T));
		}

		// Token: 0x0600058F RID: 1423 RVA: 0x0001E558 File Offset: 0x0001C758
		public void HandleMsg(NetworkReader reader)
		{
			byte op = reader.ReadByte();
			int num = (int)reader.ReadPackedUInt32();
			T t = this.DeserializeItem(reader);
			switch (op)
			{
			case 0:
				this.m_Objects.Add(t);
				break;
			case 1:
				this.m_Objects.Clear();
				break;
			case 2:
				this.m_Objects.Insert(num, t);
				break;
			case 3:
				this.m_Objects.Remove(t);
				break;
			case 4:
				this.m_Objects.RemoveAt(num);
				break;
			case 5:
			case 6:
				this.m_Objects[num] = t;
				break;
			}
			if (this.m_Callback != null)
			{
				this.m_Callback((SyncList<T>.Operation)op, num);
			}
		}

		// Token: 0x06000590 RID: 1424 RVA: 0x0001E627 File Offset: 0x0001C827
		internal void AddInternal(T item)
		{
			this.m_Objects.Add(item);
		}

		// Token: 0x06000591 RID: 1425 RVA: 0x0001E636 File Offset: 0x0001C836
		public void Add(T item)
		{
			this.m_Objects.Add(item);
			this.SendMsg(SyncList<T>.Operation.OP_ADD, this.m_Objects.Count - 1, item);
		}

		// Token: 0x06000592 RID: 1426 RVA: 0x0001E65A File Offset: 0x0001C85A
		public void Clear()
		{
			this.m_Objects.Clear();
			this.SendMsg(SyncList<T>.Operation.OP_CLEAR, 0);
		}

		// Token: 0x06000593 RID: 1427 RVA: 0x0001E670 File Offset: 0x0001C870
		public bool Contains(T item)
		{
			return this.m_Objects.Contains(item);
		}

		// Token: 0x06000594 RID: 1428 RVA: 0x0001E691 File Offset: 0x0001C891
		public void CopyTo(T[] array, int index)
		{
			this.m_Objects.CopyTo(array, index);
		}

		// Token: 0x06000595 RID: 1429 RVA: 0x0001E6A4 File Offset: 0x0001C8A4
		public int IndexOf(T item)
		{
			return this.m_Objects.IndexOf(item);
		}

		// Token: 0x06000596 RID: 1430 RVA: 0x0001E6C5 File Offset: 0x0001C8C5
		public void Insert(int index, T item)
		{
			this.m_Objects.Insert(index, item);
			this.SendMsg(SyncList<T>.Operation.OP_INSERT, index, item);
		}

		// Token: 0x06000597 RID: 1431 RVA: 0x0001E6E0 File Offset: 0x0001C8E0
		public bool Remove(T item)
		{
			bool flag = this.m_Objects.Remove(item);
			if (flag)
			{
				this.SendMsg(SyncList<T>.Operation.OP_REMOVE, 0, item);
			}
			return flag;
		}

		// Token: 0x06000598 RID: 1432 RVA: 0x0001E714 File Offset: 0x0001C914
		public void RemoveAt(int index)
		{
			this.m_Objects.RemoveAt(index);
			this.SendMsg(SyncList<T>.Operation.OP_REMOVEAT, index);
		}

		// Token: 0x06000599 RID: 1433 RVA: 0x0001E72B File Offset: 0x0001C92B
		public void Dirty(int index)
		{
			this.SendMsg(SyncList<T>.Operation.OP_DIRTY, index, this.m_Objects[index]);
		}

		// Token: 0x170000E4 RID: 228
		public T this[int i]
		{
			get
			{
				return this.m_Objects[i];
			}
			set
			{
				bool flag;
				if (this.m_Objects[i] == null)
				{
					if (value == null)
					{
						return;
					}
					flag = true;
				}
				else
				{
					T t = this.m_Objects[i];
					flag = !t.Equals(value);
				}
				this.m_Objects[i] = value;
				if (flag)
				{
					this.SendMsg(SyncList<T>.Operation.OP_SET, i, value);
				}
			}
		}

		// Token: 0x0600059C RID: 1436 RVA: 0x0001E7EC File Offset: 0x0001C9EC
		public IEnumerator<T> GetEnumerator()
		{
			return this.m_Objects.GetEnumerator();
		}

		// Token: 0x0600059D RID: 1437 RVA: 0x0001E814 File Offset: 0x0001CA14
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x04000235 RID: 565
		private List<T> m_Objects = new List<T>();

		// Token: 0x04000236 RID: 566
		private NetworkBehaviour m_Behaviour;

		// Token: 0x04000237 RID: 567
		private int m_CmdHash;

		// Token: 0x04000238 RID: 568
		private SyncList<T>.SyncListChanged m_Callback;

		// Token: 0x0200006F RID: 111
		// (Invoke) Token: 0x0600059F RID: 1439
		public delegate void SyncListChanged(SyncList<T>.Operation op, int itemIndex);

		// Token: 0x02000070 RID: 112
		public enum Operation
		{
			// Token: 0x0400023A RID: 570
			OP_ADD,
			// Token: 0x0400023B RID: 571
			OP_CLEAR,
			// Token: 0x0400023C RID: 572
			OP_INSERT,
			// Token: 0x0400023D RID: 573
			OP_REMOVE,
			// Token: 0x0400023E RID: 574
			OP_REMOVEAT,
			// Token: 0x0400023F RID: 575
			OP_SET,
			// Token: 0x04000240 RID: 576
			OP_DIRTY
		}
	}
}
