﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000072 RID: 114
	// (Invoke) Token: 0x060005A7 RID: 1447
	public delegate GameObject SpawnDelegate(Vector3 position, NetworkHash128 assetId);
}
