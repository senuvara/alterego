﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000073 RID: 115
	// (Invoke) Token: 0x060005AB RID: 1451
	public delegate void UnSpawnDelegate(GameObject spawned);
}
