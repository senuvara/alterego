﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x0200000E RID: 14
	[AttributeUsage(AttributeTargets.Method)]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class ServerCallbackAttribute : Attribute
	{
		// Token: 0x0600006D RID: 109 RVA: 0x00004C8A File Offset: 0x00002E8A
		public ServerCallbackAttribute()
		{
		}
	}
}
