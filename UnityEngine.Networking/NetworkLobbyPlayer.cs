﻿using System;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.SceneManagement;

namespace UnityEngine.Networking
{
	// Token: 0x0200004C RID: 76
	[DisallowMultipleComponent]
	[AddComponentMenu("Network/NetworkLobbyPlayer")]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class NetworkLobbyPlayer : NetworkBehaviour
	{
		// Token: 0x060002CF RID: 719 RVA: 0x0001154E File Offset: 0x0000F74E
		public NetworkLobbyPlayer()
		{
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x060002D0 RID: 720 RVA: 0x00011560 File Offset: 0x0000F760
		// (set) Token: 0x060002D1 RID: 721 RVA: 0x0001157B File Offset: 0x0000F77B
		public byte slot
		{
			get
			{
				return this.m_Slot;
			}
			set
			{
				this.m_Slot = value;
			}
		}

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x060002D2 RID: 722 RVA: 0x00011588 File Offset: 0x0000F788
		// (set) Token: 0x060002D3 RID: 723 RVA: 0x000115A3 File Offset: 0x0000F7A3
		public bool readyToBegin
		{
			get
			{
				return this.m_ReadyToBegin;
			}
			set
			{
				this.m_ReadyToBegin = value;
			}
		}

		// Token: 0x060002D4 RID: 724 RVA: 0x000115AD File Offset: 0x0000F7AD
		private void Start()
		{
			Object.DontDestroyOnLoad(base.gameObject);
		}

		// Token: 0x060002D5 RID: 725 RVA: 0x000115BB File Offset: 0x0000F7BB
		private void OnEnable()
		{
			SceneManager.sceneLoaded += this.OnSceneLoaded;
		}

		// Token: 0x060002D6 RID: 726 RVA: 0x000115CF File Offset: 0x0000F7CF
		private void OnDisable()
		{
			SceneManager.sceneLoaded -= this.OnSceneLoaded;
		}

		// Token: 0x060002D7 RID: 727 RVA: 0x000115E4 File Offset: 0x0000F7E4
		public override void OnStartClient()
		{
			NetworkLobbyManager lobbyManager = this.GetLobbyManager();
			if (lobbyManager)
			{
				lobbyManager.lobbySlots[(int)this.m_Slot] = this;
				this.m_ReadyToBegin = false;
				this.OnClientEnterLobby();
			}
			else
			{
				Debug.LogError("LobbyPlayer could not find a NetworkLobbyManager. The LobbyPlayer requires a NetworkLobbyManager object to function. Make sure that there is one in the scene.");
			}
		}

		// Token: 0x060002D8 RID: 728 RVA: 0x00011634 File Offset: 0x0000F834
		public void SendReadyToBeginMessage()
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkLobbyPlayer SendReadyToBeginMessage");
			}
			NetworkLobbyManager lobbyManager = this.GetLobbyManager();
			if (lobbyManager)
			{
				LobbyReadyToBeginMessage lobbyReadyToBeginMessage = new LobbyReadyToBeginMessage();
				lobbyReadyToBeginMessage.slotId = (byte)base.playerControllerId;
				lobbyReadyToBeginMessage.readyState = true;
				lobbyManager.client.Send(43, lobbyReadyToBeginMessage);
			}
		}

		// Token: 0x060002D9 RID: 729 RVA: 0x00011698 File Offset: 0x0000F898
		public void SendNotReadyToBeginMessage()
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkLobbyPlayer SendReadyToBeginMessage");
			}
			NetworkLobbyManager lobbyManager = this.GetLobbyManager();
			if (lobbyManager)
			{
				LobbyReadyToBeginMessage lobbyReadyToBeginMessage = new LobbyReadyToBeginMessage();
				lobbyReadyToBeginMessage.slotId = (byte)base.playerControllerId;
				lobbyReadyToBeginMessage.readyState = false;
				lobbyManager.client.Send(43, lobbyReadyToBeginMessage);
			}
		}

		// Token: 0x060002DA RID: 730 RVA: 0x000116FC File Offset: 0x0000F8FC
		public void SendSceneLoadedMessage()
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkLobbyPlayer SendSceneLoadedMessage");
			}
			NetworkLobbyManager lobbyManager = this.GetLobbyManager();
			if (lobbyManager)
			{
				IntegerMessage msg = new IntegerMessage((int)base.playerControllerId);
				lobbyManager.client.Send(44, msg);
			}
		}

		// Token: 0x060002DB RID: 731 RVA: 0x00011750 File Offset: 0x0000F950
		private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
		{
			NetworkLobbyManager lobbyManager = this.GetLobbyManager();
			if (lobbyManager)
			{
				string name = scene.name;
				if (name == lobbyManager.lobbyScene)
				{
					return;
				}
			}
			if (base.isLocalPlayer)
			{
				this.SendSceneLoadedMessage();
			}
		}

		// Token: 0x060002DC RID: 732 RVA: 0x000117A4 File Offset: 0x0000F9A4
		private NetworkLobbyManager GetLobbyManager()
		{
			return NetworkManager.singleton as NetworkLobbyManager;
		}

		// Token: 0x060002DD RID: 733 RVA: 0x000117C4 File Offset: 0x0000F9C4
		public void RemovePlayer()
		{
			if (base.isLocalPlayer && !this.m_ReadyToBegin)
			{
				if (LogFilter.logDebug)
				{
					Debug.Log("NetworkLobbyPlayer RemovePlayer");
				}
				ClientScene.RemovePlayer(base.GetComponent<NetworkIdentity>().playerControllerId);
			}
		}

		// Token: 0x060002DE RID: 734 RVA: 0x00011811 File Offset: 0x0000FA11
		public virtual void OnClientEnterLobby()
		{
		}

		// Token: 0x060002DF RID: 735 RVA: 0x00011814 File Offset: 0x0000FA14
		public virtual void OnClientExitLobby()
		{
		}

		// Token: 0x060002E0 RID: 736 RVA: 0x00011817 File Offset: 0x0000FA17
		public virtual void OnClientReady(bool readyState)
		{
		}

		// Token: 0x060002E1 RID: 737 RVA: 0x0001181C File Offset: 0x0000FA1C
		public override bool OnSerialize(NetworkWriter writer, bool initialState)
		{
			writer.WritePackedUInt32(1U);
			writer.Write(this.m_Slot);
			writer.Write(this.m_ReadyToBegin);
			return true;
		}

		// Token: 0x060002E2 RID: 738 RVA: 0x00011854 File Offset: 0x0000FA54
		public override void OnDeserialize(NetworkReader reader, bool initialState)
		{
			if (reader.ReadPackedUInt32() != 0U)
			{
				this.m_Slot = reader.ReadByte();
				this.m_ReadyToBegin = reader.ReadBoolean();
			}
		}

		// Token: 0x060002E3 RID: 739 RVA: 0x0001188C File Offset: 0x0000FA8C
		private void OnGUI()
		{
			if (this.ShowLobbyGUI)
			{
				NetworkLobbyManager lobbyManager = this.GetLobbyManager();
				if (lobbyManager)
				{
					if (!lobbyManager.showLobbyGUI)
					{
						return;
					}
					string name = SceneManager.GetSceneAt(0).name;
					if (name != lobbyManager.lobbyScene)
					{
						return;
					}
				}
				Rect position = new Rect((float)(100 + this.m_Slot * 100), 200f, 90f, 20f);
				if (base.isLocalPlayer)
				{
					string text;
					if (this.m_ReadyToBegin)
					{
						text = "(Ready)";
					}
					else
					{
						text = "(Not Ready)";
					}
					GUI.Label(position, text);
					if (this.m_ReadyToBegin)
					{
						position.y += 25f;
						if (GUI.Button(position, "STOP"))
						{
							this.SendNotReadyToBeginMessage();
						}
					}
					else
					{
						position.y += 25f;
						if (GUI.Button(position, "START"))
						{
							this.SendReadyToBeginMessage();
						}
						position.y += 25f;
						if (GUI.Button(position, "Remove"))
						{
							ClientScene.RemovePlayer(base.GetComponent<NetworkIdentity>().playerControllerId);
						}
					}
				}
				else
				{
					GUI.Label(position, "Player [" + base.netId + "]");
					position.y += 25f;
					GUI.Label(position, "Ready [" + this.m_ReadyToBegin + "]");
				}
			}
		}

		// Token: 0x04000153 RID: 339
		[Tooltip("Enable to show the default lobby GUI for this player.")]
		[SerializeField]
		public bool ShowLobbyGUI = true;

		// Token: 0x04000154 RID: 340
		private byte m_Slot;

		// Token: 0x04000155 RID: 341
		private bool m_ReadyToBegin;
	}
}
