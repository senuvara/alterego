﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000077 RID: 119
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class Channels
	{
		// Token: 0x060005B5 RID: 1461 RVA: 0x0001F00D File Offset: 0x0001D20D
		public Channels()
		{
		}

		// Token: 0x04000272 RID: 626
		public const int DefaultReliable = 0;

		// Token: 0x04000273 RID: 627
		public const int DefaultUnreliable = 1;
	}
}
