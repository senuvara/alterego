﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x0200004D RID: 77
	public enum PlayerSpawnMethod
	{
		// Token: 0x04000157 RID: 343
		Random,
		// Token: 0x04000158 RID: 344
		RoundRobin
	}
}
