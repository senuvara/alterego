﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x0200000B RID: 11
	[AttributeUsage(AttributeTargets.Method)]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class TargetRpcAttribute : Attribute
	{
		// Token: 0x0600006A RID: 106 RVA: 0x00004C64 File Offset: 0x00002E64
		public TargetRpcAttribute()
		{
		}

		// Token: 0x0400004F RID: 79
		public int channel = 0;
	}
}
