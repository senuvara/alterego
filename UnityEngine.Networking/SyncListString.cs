﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000068 RID: 104
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public sealed class SyncListString : SyncList<string>
	{
		// Token: 0x06000561 RID: 1377 RVA: 0x0001E82F File Offset: 0x0001CA2F
		public SyncListString()
		{
		}

		// Token: 0x06000562 RID: 1378 RVA: 0x0001E837 File Offset: 0x0001CA37
		protected override void SerializeItem(NetworkWriter writer, string item)
		{
			writer.Write(item);
		}

		// Token: 0x06000563 RID: 1379 RVA: 0x0001E844 File Offset: 0x0001CA44
		protected override string DeserializeItem(NetworkReader reader)
		{
			return reader.ReadString();
		}

		// Token: 0x06000564 RID: 1380 RVA: 0x0001E860 File Offset: 0x0001CA60
		[Obsolete("ReadReference is now used instead")]
		public static SyncListString ReadInstance(NetworkReader reader)
		{
			ushort num = reader.ReadUInt16();
			SyncListString syncListString = new SyncListString();
			for (ushort num2 = 0; num2 < num; num2 += 1)
			{
				syncListString.AddInternal(reader.ReadString());
			}
			return syncListString;
		}

		// Token: 0x06000565 RID: 1381 RVA: 0x0001E8A4 File Offset: 0x0001CAA4
		public static void ReadReference(NetworkReader reader, SyncListString syncList)
		{
			ushort num = reader.ReadUInt16();
			syncList.Clear();
			for (ushort num2 = 0; num2 < num; num2 += 1)
			{
				syncList.AddInternal(reader.ReadString());
			}
		}

		// Token: 0x06000566 RID: 1382 RVA: 0x0001E8E0 File Offset: 0x0001CAE0
		public static void WriteInstance(NetworkWriter writer, SyncListString items)
		{
			writer.Write((ushort)items.Count);
			for (int i = 0; i < items.Count; i++)
			{
				writer.Write(items[i]);
			}
		}
	}
}
