﻿using System;
using System.Runtime.InteropServices;

namespace UnityEngine.Networking
{
	// Token: 0x0200003D RID: 61
	[StructLayout(LayoutKind.Explicit)]
	internal struct UIntDecimal
	{
		// Token: 0x040000CC RID: 204
		[FieldOffset(0)]
		public ulong longValue1;

		// Token: 0x040000CD RID: 205
		[FieldOffset(8)]
		public ulong longValue2;

		// Token: 0x040000CE RID: 206
		[FieldOffset(0)]
		public decimal decimalValue;
	}
}
