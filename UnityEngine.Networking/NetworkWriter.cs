﻿using System;
using System.Text;

namespace UnityEngine.Networking
{
	// Token: 0x02000066 RID: 102
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class NetworkWriter
	{
		// Token: 0x0600052F RID: 1327 RVA: 0x0001D327 File Offset: 0x0001B527
		public NetworkWriter()
		{
			this.m_Buffer = new NetBuffer();
			if (NetworkWriter.s_Encoding == null)
			{
				NetworkWriter.s_Encoding = new UTF8Encoding();
				NetworkWriter.s_StringWriteBuffer = new byte[32768];
			}
		}

		// Token: 0x06000530 RID: 1328 RVA: 0x0001D360 File Offset: 0x0001B560
		public NetworkWriter(byte[] buffer)
		{
			this.m_Buffer = new NetBuffer(buffer);
			if (NetworkWriter.s_Encoding == null)
			{
				NetworkWriter.s_Encoding = new UTF8Encoding();
				NetworkWriter.s_StringWriteBuffer = new byte[32768];
			}
		}

		// Token: 0x170000DE RID: 222
		// (get) Token: 0x06000531 RID: 1329 RVA: 0x0001D39C File Offset: 0x0001B59C
		public short Position
		{
			get
			{
				return (short)this.m_Buffer.Position;
			}
		}

		// Token: 0x06000532 RID: 1330 RVA: 0x0001D3C0 File Offset: 0x0001B5C0
		public byte[] ToArray()
		{
			byte[] array = new byte[this.m_Buffer.AsArraySegment().Count];
			Array.Copy(this.m_Buffer.AsArraySegment().Array, array, this.m_Buffer.AsArraySegment().Count);
			return array;
		}

		// Token: 0x06000533 RID: 1331 RVA: 0x0001D420 File Offset: 0x0001B620
		public byte[] AsArray()
		{
			return this.AsArraySegment().Array;
		}

		// Token: 0x06000534 RID: 1332 RVA: 0x0001D444 File Offset: 0x0001B644
		internal ArraySegment<byte> AsArraySegment()
		{
			return this.m_Buffer.AsArraySegment();
		}

		// Token: 0x06000535 RID: 1333 RVA: 0x0001D464 File Offset: 0x0001B664
		public void WritePackedUInt32(uint value)
		{
			if (value <= 240U)
			{
				this.Write((byte)value);
			}
			else if (value <= 2287U)
			{
				this.Write((byte)((value - 240U) / 256U + 241U));
				this.Write((byte)((value - 240U) % 256U));
			}
			else if (value <= 67823U)
			{
				this.Write(249);
				this.Write((byte)((value - 2288U) / 256U));
				this.Write((byte)((value - 2288U) % 256U));
			}
			else if (value <= 16777215U)
			{
				this.Write(250);
				this.Write((byte)(value & 255U));
				this.Write((byte)(value >> 8 & 255U));
				this.Write((byte)(value >> 16 & 255U));
			}
			else
			{
				this.Write(251);
				this.Write((byte)(value & 255U));
				this.Write((byte)(value >> 8 & 255U));
				this.Write((byte)(value >> 16 & 255U));
				this.Write((byte)(value >> 24 & 255U));
			}
		}

		// Token: 0x06000536 RID: 1334 RVA: 0x0001D5A4 File Offset: 0x0001B7A4
		public void WritePackedUInt64(ulong value)
		{
			if (value <= 240UL)
			{
				this.Write((byte)value);
			}
			else if (value <= 2287UL)
			{
				this.Write((byte)((value - 240UL) / 256UL + 241UL));
				this.Write((byte)((value - 240UL) % 256UL));
			}
			else if (value <= 67823UL)
			{
				this.Write(249);
				this.Write((byte)((value - 2288UL) / 256UL));
				this.Write((byte)((value - 2288UL) % 256UL));
			}
			else if (value <= 16777215UL)
			{
				this.Write(250);
				this.Write((byte)(value & 255UL));
				this.Write((byte)(value >> 8 & 255UL));
				this.Write((byte)(value >> 16 & 255UL));
			}
			else if (value <= (ulong)-1)
			{
				this.Write(251);
				this.Write((byte)(value & 255UL));
				this.Write((byte)(value >> 8 & 255UL));
				this.Write((byte)(value >> 16 & 255UL));
				this.Write((byte)(value >> 24 & 255UL));
			}
			else if (value <= 1099511627775UL)
			{
				this.Write(252);
				this.Write((byte)(value & 255UL));
				this.Write((byte)(value >> 8 & 255UL));
				this.Write((byte)(value >> 16 & 255UL));
				this.Write((byte)(value >> 24 & 255UL));
				this.Write((byte)(value >> 32 & 255UL));
			}
			else if (value <= 281474976710655UL)
			{
				this.Write(253);
				this.Write((byte)(value & 255UL));
				this.Write((byte)(value >> 8 & 255UL));
				this.Write((byte)(value >> 16 & 255UL));
				this.Write((byte)(value >> 24 & 255UL));
				this.Write((byte)(value >> 32 & 255UL));
				this.Write((byte)(value >> 40 & 255UL));
			}
			else if (value <= 72057594037927935UL)
			{
				this.Write(254);
				this.Write((byte)(value & 255UL));
				this.Write((byte)(value >> 8 & 255UL));
				this.Write((byte)(value >> 16 & 255UL));
				this.Write((byte)(value >> 24 & 255UL));
				this.Write((byte)(value >> 32 & 255UL));
				this.Write((byte)(value >> 40 & 255UL));
				this.Write((byte)(value >> 48 & 255UL));
			}
			else
			{
				this.Write(byte.MaxValue);
				this.Write((byte)(value & 255UL));
				this.Write((byte)(value >> 8 & 255UL));
				this.Write((byte)(value >> 16 & 255UL));
				this.Write((byte)(value >> 24 & 255UL));
				this.Write((byte)(value >> 32 & 255UL));
				this.Write((byte)(value >> 40 & 255UL));
				this.Write((byte)(value >> 48 & 255UL));
				this.Write((byte)(value >> 56 & 255UL));
			}
		}

		// Token: 0x06000537 RID: 1335 RVA: 0x0001D937 File Offset: 0x0001BB37
		public void Write(NetworkInstanceId value)
		{
			this.WritePackedUInt32(value.Value);
		}

		// Token: 0x06000538 RID: 1336 RVA: 0x0001D947 File Offset: 0x0001BB47
		public void Write(NetworkSceneId value)
		{
			this.WritePackedUInt32(value.Value);
		}

		// Token: 0x06000539 RID: 1337 RVA: 0x0001D957 File Offset: 0x0001BB57
		public void Write(char value)
		{
			this.m_Buffer.WriteByte((byte)value);
		}

		// Token: 0x0600053A RID: 1338 RVA: 0x0001D967 File Offset: 0x0001BB67
		public void Write(byte value)
		{
			this.m_Buffer.WriteByte(value);
		}

		// Token: 0x0600053B RID: 1339 RVA: 0x0001D976 File Offset: 0x0001BB76
		public void Write(sbyte value)
		{
			this.m_Buffer.WriteByte((byte)value);
		}

		// Token: 0x0600053C RID: 1340 RVA: 0x0001D986 File Offset: 0x0001BB86
		public void Write(short value)
		{
			this.m_Buffer.WriteByte2((byte)(value & 255), (byte)(value >> 8 & 255));
		}

		// Token: 0x0600053D RID: 1341 RVA: 0x0001D9A6 File Offset: 0x0001BBA6
		public void Write(ushort value)
		{
			this.m_Buffer.WriteByte2((byte)(value & 255), (byte)(value >> 8 & 255));
		}

		// Token: 0x0600053E RID: 1342 RVA: 0x0001D9C6 File Offset: 0x0001BBC6
		public void Write(int value)
		{
			this.m_Buffer.WriteByte4((byte)(value & 255), (byte)(value >> 8 & 255), (byte)(value >> 16 & 255), (byte)(value >> 24 & 255));
		}

		// Token: 0x0600053F RID: 1343 RVA: 0x0001D9FC File Offset: 0x0001BBFC
		public void Write(uint value)
		{
			this.m_Buffer.WriteByte4((byte)(value & 255U), (byte)(value >> 8 & 255U), (byte)(value >> 16 & 255U), (byte)(value >> 24 & 255U));
		}

		// Token: 0x06000540 RID: 1344 RVA: 0x0001DA34 File Offset: 0x0001BC34
		public void Write(long value)
		{
			this.m_Buffer.WriteByte8((byte)(value & 255L), (byte)(value >> 8 & 255L), (byte)(value >> 16 & 255L), (byte)(value >> 24 & 255L), (byte)(value >> 32 & 255L), (byte)(value >> 40 & 255L), (byte)(value >> 48 & 255L), (byte)(value >> 56 & 255L));
		}

		// Token: 0x06000541 RID: 1345 RVA: 0x0001DAAC File Offset: 0x0001BCAC
		public void Write(ulong value)
		{
			this.m_Buffer.WriteByte8((byte)(value & 255UL), (byte)(value >> 8 & 255UL), (byte)(value >> 16 & 255UL), (byte)(value >> 24 & 255UL), (byte)(value >> 32 & 255UL), (byte)(value >> 40 & 255UL), (byte)(value >> 48 & 255UL), (byte)(value >> 56 & 255UL));
		}

		// Token: 0x06000542 RID: 1346 RVA: 0x0001DB21 File Offset: 0x0001BD21
		public void Write(float value)
		{
			NetworkWriter.s_FloatConverter.floatValue = value;
			this.Write(NetworkWriter.s_FloatConverter.intValue);
		}

		// Token: 0x06000543 RID: 1347 RVA: 0x0001DB3F File Offset: 0x0001BD3F
		public void Write(double value)
		{
			NetworkWriter.s_FloatConverter.doubleValue = value;
			this.Write(NetworkWriter.s_FloatConverter.longValue);
		}

		// Token: 0x06000544 RID: 1348 RVA: 0x0001DB60 File Offset: 0x0001BD60
		public void Write(decimal value)
		{
			int[] bits = decimal.GetBits(value);
			this.Write(bits[0]);
			this.Write(bits[1]);
			this.Write(bits[2]);
			this.Write(bits[3]);
		}

		// Token: 0x06000545 RID: 1349 RVA: 0x0001DB9C File Offset: 0x0001BD9C
		public void Write(string value)
		{
			if (value == null)
			{
				this.m_Buffer.WriteByte2(0, 0);
			}
			else
			{
				int byteCount = NetworkWriter.s_Encoding.GetByteCount(value);
				if (byteCount >= 32768)
				{
					throw new IndexOutOfRangeException("Serialize(string) too long: " + value.Length);
				}
				this.Write((ushort)byteCount);
				int bytes = NetworkWriter.s_Encoding.GetBytes(value, 0, value.Length, NetworkWriter.s_StringWriteBuffer, 0);
				this.m_Buffer.WriteBytes(NetworkWriter.s_StringWriteBuffer, (ushort)bytes);
			}
		}

		// Token: 0x06000546 RID: 1350 RVA: 0x0001DC29 File Offset: 0x0001BE29
		public void Write(bool value)
		{
			if (value)
			{
				this.m_Buffer.WriteByte(1);
			}
			else
			{
				this.m_Buffer.WriteByte(0);
			}
		}

		// Token: 0x06000547 RID: 1351 RVA: 0x0001DC50 File Offset: 0x0001BE50
		public void Write(byte[] buffer, int count)
		{
			if (count > 65535)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkWriter Write: buffer is too large (" + count + ") bytes. The maximum buffer size is 64K bytes.");
				}
			}
			else
			{
				this.m_Buffer.WriteBytes(buffer, (ushort)count);
			}
		}

		// Token: 0x06000548 RID: 1352 RVA: 0x0001DCA4 File Offset: 0x0001BEA4
		public void Write(byte[] buffer, int offset, int count)
		{
			if (count > 65535)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkWriter Write: buffer is too large (" + count + ") bytes. The maximum buffer size is 64K bytes.");
				}
			}
			else
			{
				this.m_Buffer.WriteBytesAtOffset(buffer, (ushort)offset, (ushort)count);
			}
		}

		// Token: 0x06000549 RID: 1353 RVA: 0x0001DCFC File Offset: 0x0001BEFC
		public void WriteBytesAndSize(byte[] buffer, int count)
		{
			if (buffer == null || count == 0)
			{
				this.Write(0);
			}
			else if (count > 65535)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkWriter WriteBytesAndSize: buffer is too large (" + count + ") bytes. The maximum buffer size is 64K bytes.");
				}
			}
			else
			{
				this.Write((ushort)count);
				this.m_Buffer.WriteBytes(buffer, (ushort)count);
			}
		}

		// Token: 0x0600054A RID: 1354 RVA: 0x0001DD70 File Offset: 0x0001BF70
		public void WriteBytesFull(byte[] buffer)
		{
			if (buffer == null)
			{
				this.Write(0);
			}
			else if (buffer.Length > 65535)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkWriter WriteBytes: buffer is too large (" + buffer.Length + ") bytes. The maximum buffer size is 64K bytes.");
				}
			}
			else
			{
				this.Write((ushort)buffer.Length);
				this.m_Buffer.WriteBytes(buffer, (ushort)buffer.Length);
			}
		}

		// Token: 0x0600054B RID: 1355 RVA: 0x0001DDE6 File Offset: 0x0001BFE6
		public void Write(Vector2 value)
		{
			this.Write(value.x);
			this.Write(value.y);
		}

		// Token: 0x0600054C RID: 1356 RVA: 0x0001DE03 File Offset: 0x0001C003
		public void Write(Vector3 value)
		{
			this.Write(value.x);
			this.Write(value.y);
			this.Write(value.z);
		}

		// Token: 0x0600054D RID: 1357 RVA: 0x0001DE2D File Offset: 0x0001C02D
		public void Write(Vector4 value)
		{
			this.Write(value.x);
			this.Write(value.y);
			this.Write(value.z);
			this.Write(value.w);
		}

		// Token: 0x0600054E RID: 1358 RVA: 0x0001DE64 File Offset: 0x0001C064
		public void Write(Color value)
		{
			this.Write(value.r);
			this.Write(value.g);
			this.Write(value.b);
			this.Write(value.a);
		}

		// Token: 0x0600054F RID: 1359 RVA: 0x0001DE9B File Offset: 0x0001C09B
		public void Write(Color32 value)
		{
			this.Write(value.r);
			this.Write(value.g);
			this.Write(value.b);
			this.Write(value.a);
		}

		// Token: 0x06000550 RID: 1360 RVA: 0x0001DED2 File Offset: 0x0001C0D2
		public void Write(Quaternion value)
		{
			this.Write(value.x);
			this.Write(value.y);
			this.Write(value.z);
			this.Write(value.w);
		}

		// Token: 0x06000551 RID: 1361 RVA: 0x0001DF09 File Offset: 0x0001C109
		public void Write(Rect value)
		{
			this.Write(value.xMin);
			this.Write(value.yMin);
			this.Write(value.width);
			this.Write(value.height);
		}

		// Token: 0x06000552 RID: 1362 RVA: 0x0001DF40 File Offset: 0x0001C140
		public void Write(Plane value)
		{
			this.Write(value.normal);
			this.Write(value.distance);
		}

		// Token: 0x06000553 RID: 1363 RVA: 0x0001DF5D File Offset: 0x0001C15D
		public void Write(Ray value)
		{
			this.Write(value.direction);
			this.Write(value.origin);
		}

		// Token: 0x06000554 RID: 1364 RVA: 0x0001DF7C File Offset: 0x0001C17C
		public void Write(Matrix4x4 value)
		{
			this.Write(value.m00);
			this.Write(value.m01);
			this.Write(value.m02);
			this.Write(value.m03);
			this.Write(value.m10);
			this.Write(value.m11);
			this.Write(value.m12);
			this.Write(value.m13);
			this.Write(value.m20);
			this.Write(value.m21);
			this.Write(value.m22);
			this.Write(value.m23);
			this.Write(value.m30);
			this.Write(value.m31);
			this.Write(value.m32);
			this.Write(value.m33);
		}

		// Token: 0x06000555 RID: 1365 RVA: 0x0001E05C File Offset: 0x0001C25C
		public void Write(NetworkHash128 value)
		{
			this.Write(value.i0);
			this.Write(value.i1);
			this.Write(value.i2);
			this.Write(value.i3);
			this.Write(value.i4);
			this.Write(value.i5);
			this.Write(value.i6);
			this.Write(value.i7);
			this.Write(value.i8);
			this.Write(value.i9);
			this.Write(value.i10);
			this.Write(value.i11);
			this.Write(value.i12);
			this.Write(value.i13);
			this.Write(value.i14);
			this.Write(value.i15);
		}

		// Token: 0x06000556 RID: 1366 RVA: 0x0001E13A File Offset: 0x0001C33A
		public void Write(NetworkIdentity value)
		{
			if (value == null)
			{
				this.WritePackedUInt32(0U);
			}
			else
			{
				this.Write(value.netId);
			}
		}

		// Token: 0x06000557 RID: 1367 RVA: 0x0001E164 File Offset: 0x0001C364
		public void Write(Transform value)
		{
			if (value == null || value.gameObject == null)
			{
				this.WritePackedUInt32(0U);
			}
			else
			{
				NetworkIdentity component = value.gameObject.GetComponent<NetworkIdentity>();
				if (component != null)
				{
					this.Write(component.netId);
				}
				else
				{
					if (LogFilter.logWarn)
					{
						Debug.LogWarning("NetworkWriter " + value + " has no NetworkIdentity");
					}
					this.WritePackedUInt32(0U);
				}
			}
		}

		// Token: 0x06000558 RID: 1368 RVA: 0x0001E1F4 File Offset: 0x0001C3F4
		public void Write(GameObject value)
		{
			if (value == null)
			{
				this.WritePackedUInt32(0U);
			}
			else
			{
				NetworkIdentity component = value.GetComponent<NetworkIdentity>();
				if (component != null)
				{
					this.Write(component.netId);
				}
				else
				{
					if (LogFilter.logWarn)
					{
						Debug.LogWarning("NetworkWriter " + value + " has no NetworkIdentity");
					}
					this.WritePackedUInt32(0U);
				}
			}
		}

		// Token: 0x06000559 RID: 1369 RVA: 0x0001E26B File Offset: 0x0001C46B
		public void Write(MessageBase msg)
		{
			msg.Serialize(this);
		}

		// Token: 0x0600055A RID: 1370 RVA: 0x0001E275 File Offset: 0x0001C475
		public void SeekZero()
		{
			this.m_Buffer.SeekZero();
		}

		// Token: 0x0600055B RID: 1371 RVA: 0x0001E283 File Offset: 0x0001C483
		public void StartMessage(short msgType)
		{
			this.SeekZero();
			this.m_Buffer.WriteByte2(0, 0);
			this.Write(msgType);
		}

		// Token: 0x0600055C RID: 1372 RVA: 0x0001E2A0 File Offset: 0x0001C4A0
		public void FinishMessage()
		{
			this.m_Buffer.FinishMessage();
		}

		// Token: 0x0400022B RID: 555
		private const int k_MaxStringLength = 32768;

		// Token: 0x0400022C RID: 556
		private NetBuffer m_Buffer;

		// Token: 0x0400022D RID: 557
		private static Encoding s_Encoding;

		// Token: 0x0400022E RID: 558
		private static byte[] s_StringWriteBuffer;

		// Token: 0x0400022F RID: 559
		private static UIntFloat s_FloatConverter;
	}
}
