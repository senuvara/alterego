﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x0200006B RID: 107
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class SyncListUInt : SyncList<uint>
	{
		// Token: 0x06000573 RID: 1395 RVA: 0x0001EB01 File Offset: 0x0001CD01
		public SyncListUInt()
		{
		}

		// Token: 0x06000574 RID: 1396 RVA: 0x0001EB09 File Offset: 0x0001CD09
		protected override void SerializeItem(NetworkWriter writer, uint item)
		{
			writer.WritePackedUInt32(item);
		}

		// Token: 0x06000575 RID: 1397 RVA: 0x0001EB14 File Offset: 0x0001CD14
		protected override uint DeserializeItem(NetworkReader reader)
		{
			return reader.ReadPackedUInt32();
		}

		// Token: 0x06000576 RID: 1398 RVA: 0x0001EB30 File Offset: 0x0001CD30
		[Obsolete("ReadReference is now used instead")]
		public static SyncListUInt ReadInstance(NetworkReader reader)
		{
			ushort num = reader.ReadUInt16();
			SyncListUInt syncListUInt = new SyncListUInt();
			for (ushort num2 = 0; num2 < num; num2 += 1)
			{
				syncListUInt.AddInternal(reader.ReadPackedUInt32());
			}
			return syncListUInt;
		}

		// Token: 0x06000577 RID: 1399 RVA: 0x0001EB74 File Offset: 0x0001CD74
		public static void ReadReference(NetworkReader reader, SyncListUInt syncList)
		{
			ushort num = reader.ReadUInt16();
			syncList.Clear();
			for (ushort num2 = 0; num2 < num; num2 += 1)
			{
				syncList.AddInternal(reader.ReadPackedUInt32());
			}
		}

		// Token: 0x06000578 RID: 1400 RVA: 0x0001EBB0 File Offset: 0x0001CDB0
		public static void WriteInstance(NetworkWriter writer, SyncListUInt items)
		{
			writer.Write((ushort)items.Count);
			for (int i = 0; i < items.Count; i++)
			{
				writer.WritePackedUInt32(items[i]);
			}
		}
	}
}
