﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000049 RID: 73
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	[Serializable]
	public struct NetworkInstanceId : IEquatable<NetworkInstanceId>
	{
		// Token: 0x06000282 RID: 642 RVA: 0x0000DD01 File Offset: 0x0000BF01
		public NetworkInstanceId(uint value)
		{
			this.m_Value = value;
		}

		// Token: 0x06000283 RID: 643 RVA: 0x0000DD0C File Offset: 0x0000BF0C
		public bool IsEmpty()
		{
			return this.m_Value == 0U;
		}

		// Token: 0x06000284 RID: 644 RVA: 0x0000DD2C File Offset: 0x0000BF2C
		public override int GetHashCode()
		{
			return (int)this.m_Value;
		}

		// Token: 0x06000285 RID: 645 RVA: 0x0000DD48 File Offset: 0x0000BF48
		public override bool Equals(object obj)
		{
			return obj is NetworkInstanceId && this.Equals((NetworkInstanceId)obj);
		}

		// Token: 0x06000286 RID: 646 RVA: 0x0000DD78 File Offset: 0x0000BF78
		public bool Equals(NetworkInstanceId other)
		{
			return this == other;
		}

		// Token: 0x06000287 RID: 647 RVA: 0x0000DD9C File Offset: 0x0000BF9C
		public static bool operator ==(NetworkInstanceId c1, NetworkInstanceId c2)
		{
			return c1.m_Value == c2.m_Value;
		}

		// Token: 0x06000288 RID: 648 RVA: 0x0000DDC4 File Offset: 0x0000BFC4
		public static bool operator !=(NetworkInstanceId c1, NetworkInstanceId c2)
		{
			return c1.m_Value != c2.m_Value;
		}

		// Token: 0x06000289 RID: 649 RVA: 0x0000DDEC File Offset: 0x0000BFEC
		public override string ToString()
		{
			return this.m_Value.ToString();
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x0600028A RID: 650 RVA: 0x0000DE18 File Offset: 0x0000C018
		public uint Value
		{
			get
			{
				return this.m_Value;
			}
		}

		// Token: 0x0600028B RID: 651 RVA: 0x0000DE33 File Offset: 0x0000C033
		// Note: this type is marked as 'beforefieldinit'.
		static NetworkInstanceId()
		{
		}

		// Token: 0x04000141 RID: 321
		[SerializeField]
		private readonly uint m_Value;

		// Token: 0x04000142 RID: 322
		public static NetworkInstanceId Invalid = new NetworkInstanceId(uint.MaxValue);

		// Token: 0x04000143 RID: 323
		internal static NetworkInstanceId Zero = new NetworkInstanceId(0U);
	}
}
