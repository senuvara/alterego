﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x0200005F RID: 95
	[DisallowMultipleComponent]
	[AddComponentMenu("Network/NetworkTransform")]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class NetworkTransform : NetworkBehaviour
	{
		// Token: 0x060004D0 RID: 1232 RVA: 0x0001A670 File Offset: 0x00018870
		public NetworkTransform()
		{
		}

		// Token: 0x170000C8 RID: 200
		// (get) Token: 0x060004D1 RID: 1233 RVA: 0x0001A6E4 File Offset: 0x000188E4
		// (set) Token: 0x060004D2 RID: 1234 RVA: 0x0001A6FF File Offset: 0x000188FF
		public NetworkTransform.TransformSyncMode transformSyncMode
		{
			get
			{
				return this.m_TransformSyncMode;
			}
			set
			{
				this.m_TransformSyncMode = value;
			}
		}

		// Token: 0x170000C9 RID: 201
		// (get) Token: 0x060004D3 RID: 1235 RVA: 0x0001A70C File Offset: 0x0001890C
		// (set) Token: 0x060004D4 RID: 1236 RVA: 0x0001A727 File Offset: 0x00018927
		public float sendInterval
		{
			get
			{
				return this.m_SendInterval;
			}
			set
			{
				this.m_SendInterval = value;
			}
		}

		// Token: 0x170000CA RID: 202
		// (get) Token: 0x060004D5 RID: 1237 RVA: 0x0001A734 File Offset: 0x00018934
		// (set) Token: 0x060004D6 RID: 1238 RVA: 0x0001A74F File Offset: 0x0001894F
		public NetworkTransform.AxisSyncMode syncRotationAxis
		{
			get
			{
				return this.m_SyncRotationAxis;
			}
			set
			{
				this.m_SyncRotationAxis = value;
			}
		}

		// Token: 0x170000CB RID: 203
		// (get) Token: 0x060004D7 RID: 1239 RVA: 0x0001A75C File Offset: 0x0001895C
		// (set) Token: 0x060004D8 RID: 1240 RVA: 0x0001A777 File Offset: 0x00018977
		public NetworkTransform.CompressionSyncMode rotationSyncCompression
		{
			get
			{
				return this.m_RotationSyncCompression;
			}
			set
			{
				this.m_RotationSyncCompression = value;
			}
		}

		// Token: 0x170000CC RID: 204
		// (get) Token: 0x060004D9 RID: 1241 RVA: 0x0001A784 File Offset: 0x00018984
		// (set) Token: 0x060004DA RID: 1242 RVA: 0x0001A79F File Offset: 0x0001899F
		public bool syncSpin
		{
			get
			{
				return this.m_SyncSpin;
			}
			set
			{
				this.m_SyncSpin = value;
			}
		}

		// Token: 0x170000CD RID: 205
		// (get) Token: 0x060004DB RID: 1243 RVA: 0x0001A7AC File Offset: 0x000189AC
		// (set) Token: 0x060004DC RID: 1244 RVA: 0x0001A7C7 File Offset: 0x000189C7
		public float movementTheshold
		{
			get
			{
				return this.m_MovementTheshold;
			}
			set
			{
				this.m_MovementTheshold = value;
			}
		}

		// Token: 0x170000CE RID: 206
		// (get) Token: 0x060004DD RID: 1245 RVA: 0x0001A7D4 File Offset: 0x000189D4
		// (set) Token: 0x060004DE RID: 1246 RVA: 0x0001A7EF File Offset: 0x000189EF
		public float velocityThreshold
		{
			get
			{
				return this.m_VelocityThreshold;
			}
			set
			{
				this.m_VelocityThreshold = value;
			}
		}

		// Token: 0x170000CF RID: 207
		// (get) Token: 0x060004DF RID: 1247 RVA: 0x0001A7FC File Offset: 0x000189FC
		// (set) Token: 0x060004E0 RID: 1248 RVA: 0x0001A817 File Offset: 0x00018A17
		public float snapThreshold
		{
			get
			{
				return this.m_SnapThreshold;
			}
			set
			{
				this.m_SnapThreshold = value;
			}
		}

		// Token: 0x170000D0 RID: 208
		// (get) Token: 0x060004E1 RID: 1249 RVA: 0x0001A824 File Offset: 0x00018A24
		// (set) Token: 0x060004E2 RID: 1250 RVA: 0x0001A83F File Offset: 0x00018A3F
		public float interpolateRotation
		{
			get
			{
				return this.m_InterpolateRotation;
			}
			set
			{
				this.m_InterpolateRotation = value;
			}
		}

		// Token: 0x170000D1 RID: 209
		// (get) Token: 0x060004E3 RID: 1251 RVA: 0x0001A84C File Offset: 0x00018A4C
		// (set) Token: 0x060004E4 RID: 1252 RVA: 0x0001A867 File Offset: 0x00018A67
		public float interpolateMovement
		{
			get
			{
				return this.m_InterpolateMovement;
			}
			set
			{
				this.m_InterpolateMovement = value;
			}
		}

		// Token: 0x170000D2 RID: 210
		// (get) Token: 0x060004E5 RID: 1253 RVA: 0x0001A874 File Offset: 0x00018A74
		// (set) Token: 0x060004E6 RID: 1254 RVA: 0x0001A88F File Offset: 0x00018A8F
		public NetworkTransform.ClientMoveCallback3D clientMoveCallback3D
		{
			get
			{
				return this.m_ClientMoveCallback3D;
			}
			set
			{
				this.m_ClientMoveCallback3D = value;
			}
		}

		// Token: 0x170000D3 RID: 211
		// (get) Token: 0x060004E7 RID: 1255 RVA: 0x0001A89C File Offset: 0x00018A9C
		// (set) Token: 0x060004E8 RID: 1256 RVA: 0x0001A8B7 File Offset: 0x00018AB7
		public NetworkTransform.ClientMoveCallback2D clientMoveCallback2D
		{
			get
			{
				return this.m_ClientMoveCallback2D;
			}
			set
			{
				this.m_ClientMoveCallback2D = value;
			}
		}

		// Token: 0x170000D4 RID: 212
		// (get) Token: 0x060004E9 RID: 1257 RVA: 0x0001A8C4 File Offset: 0x00018AC4
		public CharacterController characterContoller
		{
			get
			{
				return this.m_CharacterController;
			}
		}

		// Token: 0x170000D5 RID: 213
		// (get) Token: 0x060004EA RID: 1258 RVA: 0x0001A8E0 File Offset: 0x00018AE0
		public Rigidbody rigidbody3D
		{
			get
			{
				return this.m_RigidBody3D;
			}
		}

		// Token: 0x170000D6 RID: 214
		// (get) Token: 0x060004EB RID: 1259 RVA: 0x0001A8FC File Offset: 0x00018AFC
		public Rigidbody2D rigidbody2D
		{
			get
			{
				return this.m_RigidBody2D;
			}
		}

		// Token: 0x170000D7 RID: 215
		// (get) Token: 0x060004EC RID: 1260 RVA: 0x0001A918 File Offset: 0x00018B18
		public float lastSyncTime
		{
			get
			{
				return this.m_LastClientSyncTime;
			}
		}

		// Token: 0x170000D8 RID: 216
		// (get) Token: 0x060004ED RID: 1261 RVA: 0x0001A934 File Offset: 0x00018B34
		public Vector3 targetSyncPosition
		{
			get
			{
				return this.m_TargetSyncPosition;
			}
		}

		// Token: 0x170000D9 RID: 217
		// (get) Token: 0x060004EE RID: 1262 RVA: 0x0001A950 File Offset: 0x00018B50
		public Vector3 targetSyncVelocity
		{
			get
			{
				return this.m_TargetSyncVelocity;
			}
		}

		// Token: 0x170000DA RID: 218
		// (get) Token: 0x060004EF RID: 1263 RVA: 0x0001A96C File Offset: 0x00018B6C
		public Quaternion targetSyncRotation3D
		{
			get
			{
				return this.m_TargetSyncRotation3D;
			}
		}

		// Token: 0x170000DB RID: 219
		// (get) Token: 0x060004F0 RID: 1264 RVA: 0x0001A988 File Offset: 0x00018B88
		public float targetSyncRotation2D
		{
			get
			{
				return this.m_TargetSyncRotation2D;
			}
		}

		// Token: 0x170000DC RID: 220
		// (get) Token: 0x060004F1 RID: 1265 RVA: 0x0001A9A4 File Offset: 0x00018BA4
		// (set) Token: 0x060004F2 RID: 1266 RVA: 0x0001A9BF File Offset: 0x00018BBF
		public bool grounded
		{
			get
			{
				return this.m_Grounded;
			}
			set
			{
				this.m_Grounded = value;
			}
		}

		// Token: 0x060004F3 RID: 1267 RVA: 0x0001A9CC File Offset: 0x00018BCC
		private void OnValidate()
		{
			if (this.m_TransformSyncMode < NetworkTransform.TransformSyncMode.SyncNone || this.m_TransformSyncMode > NetworkTransform.TransformSyncMode.SyncCharacterController)
			{
				this.m_TransformSyncMode = NetworkTransform.TransformSyncMode.SyncTransform;
			}
			if (this.m_SendInterval < 0f)
			{
				this.m_SendInterval = 0f;
			}
			if (this.m_SyncRotationAxis < NetworkTransform.AxisSyncMode.None || this.m_SyncRotationAxis > NetworkTransform.AxisSyncMode.AxisXYZ)
			{
				this.m_SyncRotationAxis = NetworkTransform.AxisSyncMode.None;
			}
			if (this.m_MovementTheshold < 0f)
			{
				this.m_MovementTheshold = 0f;
			}
			if (this.m_VelocityThreshold < 0f)
			{
				this.m_VelocityThreshold = 0f;
			}
			if (this.m_SnapThreshold < 0f)
			{
				this.m_SnapThreshold = 0.01f;
			}
			if (this.m_InterpolateRotation < 0f)
			{
				this.m_InterpolateRotation = 0.01f;
			}
			if (this.m_InterpolateMovement < 0f)
			{
				this.m_InterpolateMovement = 0.01f;
			}
		}

		// Token: 0x060004F4 RID: 1268 RVA: 0x0001AACC File Offset: 0x00018CCC
		private void Awake()
		{
			this.m_RigidBody3D = base.GetComponent<Rigidbody>();
			this.m_RigidBody2D = base.GetComponent<Rigidbody2D>();
			this.m_CharacterController = base.GetComponent<CharacterController>();
			this.m_PrevPosition = base.transform.position;
			this.m_PrevRotation = base.transform.rotation;
			this.m_PrevVelocity = 0f;
			if (base.localPlayerAuthority)
			{
				this.m_LocalTransformWriter = new NetworkWriter();
			}
		}

		// Token: 0x060004F5 RID: 1269 RVA: 0x0001AB43 File Offset: 0x00018D43
		public override void OnStartServer()
		{
			this.m_LastClientSyncTime = 0f;
		}

		// Token: 0x060004F6 RID: 1270 RVA: 0x0001AB54 File Offset: 0x00018D54
		public override bool OnSerialize(NetworkWriter writer, bool initialState)
		{
			if (!initialState)
			{
				if (base.syncVarDirtyBits == 0U)
				{
					writer.WritePackedUInt32(0U);
					return false;
				}
				writer.WritePackedUInt32(1U);
			}
			switch (this.transformSyncMode)
			{
			case NetworkTransform.TransformSyncMode.SyncNone:
				return false;
			case NetworkTransform.TransformSyncMode.SyncTransform:
				this.SerializeModeTransform(writer);
				break;
			case NetworkTransform.TransformSyncMode.SyncRigidbody2D:
				this.SerializeMode2D(writer);
				break;
			case NetworkTransform.TransformSyncMode.SyncRigidbody3D:
				this.SerializeMode3D(writer);
				break;
			case NetworkTransform.TransformSyncMode.SyncCharacterController:
				this.SerializeModeCharacterController(writer);
				break;
			}
			return true;
		}

		// Token: 0x060004F7 RID: 1271 RVA: 0x0001ABFC File Offset: 0x00018DFC
		private void SerializeModeTransform(NetworkWriter writer)
		{
			writer.Write(base.transform.position);
			if (this.m_SyncRotationAxis != NetworkTransform.AxisSyncMode.None)
			{
				NetworkTransform.SerializeRotation3D(writer, base.transform.rotation, this.syncRotationAxis, this.rotationSyncCompression);
			}
			this.m_PrevPosition = base.transform.position;
			this.m_PrevRotation = base.transform.rotation;
			this.m_PrevVelocity = 0f;
		}

		// Token: 0x060004F8 RID: 1272 RVA: 0x0001AC74 File Offset: 0x00018E74
		private void VerifySerializeComponentExists()
		{
			bool flag = false;
			Type type = null;
			NetworkTransform.TransformSyncMode transformSyncMode = this.transformSyncMode;
			if (transformSyncMode != NetworkTransform.TransformSyncMode.SyncCharacterController)
			{
				if (transformSyncMode != NetworkTransform.TransformSyncMode.SyncRigidbody2D)
				{
					if (transformSyncMode == NetworkTransform.TransformSyncMode.SyncRigidbody3D)
					{
						if (!this.m_RigidBody3D && !(this.m_RigidBody3D = base.GetComponent<Rigidbody>()))
						{
							flag = true;
							type = typeof(Rigidbody);
						}
					}
				}
				else if (!this.m_RigidBody2D && !(this.m_RigidBody2D = base.GetComponent<Rigidbody2D>()))
				{
					flag = true;
					type = typeof(Rigidbody2D);
				}
			}
			else if (!this.m_CharacterController && !(this.m_CharacterController = base.GetComponent<CharacterController>()))
			{
				flag = true;
				type = typeof(CharacterController);
			}
			if (flag && type != null)
			{
				throw new InvalidOperationException(string.Format("transformSyncMode set to {0} but no {1} component was found, did you call NetworkServer.Spawn on a prefab?", this.transformSyncMode, type.Name));
			}
		}

		// Token: 0x060004F9 RID: 1273 RVA: 0x0001AD90 File Offset: 0x00018F90
		private void SerializeMode3D(NetworkWriter writer)
		{
			this.VerifySerializeComponentExists();
			if (base.isServer && this.m_LastClientSyncTime != 0f)
			{
				writer.Write(this.m_TargetSyncPosition);
				NetworkTransform.SerializeVelocity3D(writer, this.m_TargetSyncVelocity, NetworkTransform.CompressionSyncMode.None);
				if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
				{
					NetworkTransform.SerializeRotation3D(writer, this.m_TargetSyncRotation3D, this.syncRotationAxis, this.rotationSyncCompression);
				}
			}
			else
			{
				writer.Write(this.m_RigidBody3D.position);
				NetworkTransform.SerializeVelocity3D(writer, this.m_RigidBody3D.velocity, NetworkTransform.CompressionSyncMode.None);
				if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
				{
					NetworkTransform.SerializeRotation3D(writer, this.m_RigidBody3D.rotation, this.syncRotationAxis, this.rotationSyncCompression);
				}
			}
			if (this.m_SyncSpin)
			{
				NetworkTransform.SerializeSpin3D(writer, this.m_RigidBody3D.angularVelocity, this.syncRotationAxis, this.rotationSyncCompression);
			}
			this.m_PrevPosition = this.m_RigidBody3D.position;
			this.m_PrevRotation = base.transform.rotation;
			this.m_PrevVelocity = this.m_RigidBody3D.velocity.sqrMagnitude;
		}

		// Token: 0x060004FA RID: 1274 RVA: 0x0001AEB8 File Offset: 0x000190B8
		private void SerializeModeCharacterController(NetworkWriter writer)
		{
			this.VerifySerializeComponentExists();
			if (base.isServer && this.m_LastClientSyncTime != 0f)
			{
				writer.Write(this.m_TargetSyncPosition);
				if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
				{
					NetworkTransform.SerializeRotation3D(writer, this.m_TargetSyncRotation3D, this.syncRotationAxis, this.rotationSyncCompression);
				}
			}
			else
			{
				writer.Write(base.transform.position);
				if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
				{
					NetworkTransform.SerializeRotation3D(writer, base.transform.rotation, this.syncRotationAxis, this.rotationSyncCompression);
				}
			}
			this.m_PrevPosition = base.transform.position;
			this.m_PrevRotation = base.transform.rotation;
			this.m_PrevVelocity = 0f;
		}

		// Token: 0x060004FB RID: 1275 RVA: 0x0001AF8C File Offset: 0x0001918C
		private void SerializeMode2D(NetworkWriter writer)
		{
			this.VerifySerializeComponentExists();
			if (base.isServer && this.m_LastClientSyncTime != 0f)
			{
				writer.Write(this.m_TargetSyncPosition);
				NetworkTransform.SerializeVelocity2D(writer, this.m_TargetSyncVelocity, NetworkTransform.CompressionSyncMode.None);
				if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
				{
					float num = this.m_TargetSyncRotation2D % 360f;
					if (num < 0f)
					{
						num += 360f;
					}
					NetworkTransform.SerializeRotation2D(writer, num, this.rotationSyncCompression);
				}
			}
			else
			{
				writer.Write(this.m_RigidBody2D.position);
				NetworkTransform.SerializeVelocity2D(writer, this.m_RigidBody2D.velocity, NetworkTransform.CompressionSyncMode.None);
				if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
				{
					float num2 = this.m_RigidBody2D.rotation % 360f;
					if (num2 < 0f)
					{
						num2 += 360f;
					}
					NetworkTransform.SerializeRotation2D(writer, num2, this.rotationSyncCompression);
				}
			}
			if (this.m_SyncSpin)
			{
				NetworkTransform.SerializeSpin2D(writer, this.m_RigidBody2D.angularVelocity, this.rotationSyncCompression);
			}
			this.m_PrevPosition = this.m_RigidBody2D.position;
			this.m_PrevRotation = base.transform.rotation;
			this.m_PrevVelocity = this.m_RigidBody2D.velocity.sqrMagnitude;
		}

		// Token: 0x060004FC RID: 1276 RVA: 0x0001B0E8 File Offset: 0x000192E8
		public override void OnDeserialize(NetworkReader reader, bool initialState)
		{
			if (!base.isServer || !NetworkServer.localClientActive)
			{
				if (!initialState)
				{
					if (reader.ReadPackedUInt32() == 0U)
					{
						return;
					}
				}
				switch (this.transformSyncMode)
				{
				case NetworkTransform.TransformSyncMode.SyncNone:
					return;
				case NetworkTransform.TransformSyncMode.SyncTransform:
					this.UnserializeModeTransform(reader, initialState);
					break;
				case NetworkTransform.TransformSyncMode.SyncRigidbody2D:
					this.UnserializeMode2D(reader, initialState);
					break;
				case NetworkTransform.TransformSyncMode.SyncRigidbody3D:
					this.UnserializeMode3D(reader, initialState);
					break;
				case NetworkTransform.TransformSyncMode.SyncCharacterController:
					this.UnserializeModeCharacterController(reader, initialState);
					break;
				}
				this.m_LastClientSyncTime = Time.time;
			}
		}

		// Token: 0x060004FD RID: 1277 RVA: 0x0001B198 File Offset: 0x00019398
		private void UnserializeModeTransform(NetworkReader reader, bool initialState)
		{
			if (base.hasAuthority)
			{
				reader.ReadVector3();
				if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
				{
					NetworkTransform.UnserializeRotation3D(reader, this.syncRotationAxis, this.rotationSyncCompression);
				}
			}
			else if (base.isServer && this.m_ClientMoveCallback3D != null)
			{
				Vector3 position = reader.ReadVector3();
				Vector3 zero = Vector3.zero;
				Quaternion rotation = Quaternion.identity;
				if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
				{
					rotation = NetworkTransform.UnserializeRotation3D(reader, this.syncRotationAxis, this.rotationSyncCompression);
				}
				if (this.m_ClientMoveCallback3D(ref position, ref zero, ref rotation))
				{
					base.transform.position = position;
					if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
					{
						base.transform.rotation = rotation;
					}
				}
			}
			else
			{
				base.transform.position = reader.ReadVector3();
				if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
				{
					base.transform.rotation = NetworkTransform.UnserializeRotation3D(reader, this.syncRotationAxis, this.rotationSyncCompression);
				}
			}
		}

		// Token: 0x060004FE RID: 1278 RVA: 0x0001B2B4 File Offset: 0x000194B4
		private void UnserializeMode3D(NetworkReader reader, bool initialState)
		{
			if (base.hasAuthority)
			{
				reader.ReadVector3();
				reader.ReadVector3();
				if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
				{
					NetworkTransform.UnserializeRotation3D(reader, this.syncRotationAxis, this.rotationSyncCompression);
				}
				if (this.syncSpin)
				{
					NetworkTransform.UnserializeSpin3D(reader, this.syncRotationAxis, this.rotationSyncCompression);
				}
			}
			else
			{
				if (base.isServer && this.m_ClientMoveCallback3D != null)
				{
					Vector3 targetSyncPosition = reader.ReadVector3();
					Vector3 targetSyncVelocity = reader.ReadVector3();
					Quaternion targetSyncRotation3D = Quaternion.identity;
					if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
					{
						targetSyncRotation3D = NetworkTransform.UnserializeRotation3D(reader, this.syncRotationAxis, this.rotationSyncCompression);
					}
					if (!this.m_ClientMoveCallback3D(ref targetSyncPosition, ref targetSyncVelocity, ref targetSyncRotation3D))
					{
						return;
					}
					this.m_TargetSyncPosition = targetSyncPosition;
					this.m_TargetSyncVelocity = targetSyncVelocity;
					if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
					{
						this.m_TargetSyncRotation3D = targetSyncRotation3D;
					}
				}
				else
				{
					this.m_TargetSyncPosition = reader.ReadVector3();
					this.m_TargetSyncVelocity = reader.ReadVector3();
					if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
					{
						this.m_TargetSyncRotation3D = NetworkTransform.UnserializeRotation3D(reader, this.syncRotationAxis, this.rotationSyncCompression);
					}
				}
				if (this.syncSpin)
				{
					this.m_TargetSyncAngularVelocity3D = NetworkTransform.UnserializeSpin3D(reader, this.syncRotationAxis, this.rotationSyncCompression);
				}
				if (!(this.m_RigidBody3D == null))
				{
					if (base.isServer && !base.isClient)
					{
						this.m_RigidBody3D.MovePosition(this.m_TargetSyncPosition);
						this.m_RigidBody3D.MoveRotation(this.m_TargetSyncRotation3D);
						this.m_RigidBody3D.velocity = this.m_TargetSyncVelocity;
					}
					else if (this.GetNetworkSendInterval() == 0f)
					{
						this.m_RigidBody3D.MovePosition(this.m_TargetSyncPosition);
						this.m_RigidBody3D.velocity = this.m_TargetSyncVelocity;
						if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
						{
							this.m_RigidBody3D.MoveRotation(this.m_TargetSyncRotation3D);
						}
						if (this.syncSpin)
						{
							this.m_RigidBody3D.angularVelocity = this.m_TargetSyncAngularVelocity3D;
						}
					}
					else
					{
						float magnitude = (this.m_RigidBody3D.position - this.m_TargetSyncPosition).magnitude;
						if (magnitude > this.snapThreshold)
						{
							this.m_RigidBody3D.position = this.m_TargetSyncPosition;
							this.m_RigidBody3D.velocity = this.m_TargetSyncVelocity;
						}
						if (this.interpolateRotation == 0f && this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
						{
							this.m_RigidBody3D.rotation = this.m_TargetSyncRotation3D;
							if (this.syncSpin)
							{
								this.m_RigidBody3D.angularVelocity = this.m_TargetSyncAngularVelocity3D;
							}
						}
						if (this.m_InterpolateMovement == 0f)
						{
							this.m_RigidBody3D.position = this.m_TargetSyncPosition;
						}
						if (initialState && this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
						{
							this.m_RigidBody3D.rotation = this.m_TargetSyncRotation3D;
						}
					}
				}
			}
		}

		// Token: 0x060004FF RID: 1279 RVA: 0x0001B5D8 File Offset: 0x000197D8
		private void UnserializeMode2D(NetworkReader reader, bool initialState)
		{
			if (base.hasAuthority)
			{
				reader.ReadVector2();
				reader.ReadVector2();
				if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
				{
					NetworkTransform.UnserializeRotation2D(reader, this.rotationSyncCompression);
				}
				if (this.syncSpin)
				{
					NetworkTransform.UnserializeSpin2D(reader, this.rotationSyncCompression);
				}
			}
			else if (!(this.m_RigidBody2D == null))
			{
				if (base.isServer && this.m_ClientMoveCallback2D != null)
				{
					Vector2 v = reader.ReadVector2();
					Vector2 v2 = reader.ReadVector2();
					float targetSyncRotation2D = 0f;
					if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
					{
						targetSyncRotation2D = NetworkTransform.UnserializeRotation2D(reader, this.rotationSyncCompression);
					}
					if (!this.m_ClientMoveCallback2D(ref v, ref v2, ref targetSyncRotation2D))
					{
						return;
					}
					this.m_TargetSyncPosition = v;
					this.m_TargetSyncVelocity = v2;
					if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
					{
						this.m_TargetSyncRotation2D = targetSyncRotation2D;
					}
				}
				else
				{
					this.m_TargetSyncPosition = reader.ReadVector2();
					this.m_TargetSyncVelocity = reader.ReadVector2();
					if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
					{
						this.m_TargetSyncRotation2D = NetworkTransform.UnserializeRotation2D(reader, this.rotationSyncCompression);
					}
				}
				if (this.syncSpin)
				{
					this.m_TargetSyncAngularVelocity2D = NetworkTransform.UnserializeSpin2D(reader, this.rotationSyncCompression);
				}
				if (base.isServer && !base.isClient)
				{
					base.transform.position = this.m_TargetSyncPosition;
					this.m_RigidBody2D.MoveRotation(this.m_TargetSyncRotation2D);
					this.m_RigidBody2D.velocity = this.m_TargetSyncVelocity;
				}
				else if (this.GetNetworkSendInterval() == 0f)
				{
					base.transform.position = this.m_TargetSyncPosition;
					this.m_RigidBody2D.velocity = this.m_TargetSyncVelocity;
					if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
					{
						this.m_RigidBody2D.MoveRotation(this.m_TargetSyncRotation2D);
					}
					if (this.syncSpin)
					{
						this.m_RigidBody2D.angularVelocity = this.m_TargetSyncAngularVelocity2D;
					}
				}
				else
				{
					float magnitude = (this.m_RigidBody2D.position - this.m_TargetSyncPosition).magnitude;
					if (magnitude > this.snapThreshold)
					{
						this.m_RigidBody2D.position = this.m_TargetSyncPosition;
						this.m_RigidBody2D.velocity = this.m_TargetSyncVelocity;
					}
					if (this.interpolateRotation == 0f && this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
					{
						this.m_RigidBody2D.rotation = this.m_TargetSyncRotation2D;
						if (this.syncSpin)
						{
							this.m_RigidBody2D.angularVelocity = this.m_TargetSyncAngularVelocity2D;
						}
					}
					if (this.m_InterpolateMovement == 0f)
					{
						this.m_RigidBody2D.position = this.m_TargetSyncPosition;
					}
					if (initialState)
					{
						this.m_RigidBody2D.rotation = this.m_TargetSyncRotation2D;
					}
				}
			}
		}

		// Token: 0x06000500 RID: 1280 RVA: 0x0001B904 File Offset: 0x00019B04
		private void UnserializeModeCharacterController(NetworkReader reader, bool initialState)
		{
			if (base.hasAuthority)
			{
				reader.ReadVector3();
				if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
				{
					NetworkTransform.UnserializeRotation3D(reader, this.syncRotationAxis, this.rotationSyncCompression);
				}
			}
			else
			{
				if (base.isServer && this.m_ClientMoveCallback3D != null)
				{
					Vector3 targetSyncPosition = reader.ReadVector3();
					Quaternion targetSyncRotation3D = Quaternion.identity;
					if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
					{
						targetSyncRotation3D = NetworkTransform.UnserializeRotation3D(reader, this.syncRotationAxis, this.rotationSyncCompression);
					}
					if (this.m_CharacterController == null)
					{
						return;
					}
					Vector3 velocity = this.m_CharacterController.velocity;
					if (!this.m_ClientMoveCallback3D(ref targetSyncPosition, ref velocity, ref targetSyncRotation3D))
					{
						return;
					}
					this.m_TargetSyncPosition = targetSyncPosition;
					this.m_TargetSyncVelocity = velocity;
					if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
					{
						this.m_TargetSyncRotation3D = targetSyncRotation3D;
					}
				}
				else
				{
					this.m_TargetSyncPosition = reader.ReadVector3();
					if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
					{
						this.m_TargetSyncRotation3D = NetworkTransform.UnserializeRotation3D(reader, this.syncRotationAxis, this.rotationSyncCompression);
					}
				}
				if (!(this.m_CharacterController == null))
				{
					Vector3 a = this.m_TargetSyncPosition - base.transform.position;
					Vector3 a2 = a / this.GetNetworkSendInterval();
					this.m_FixedPosDiff = a2 * Time.fixedDeltaTime;
					if (base.isServer && !base.isClient)
					{
						base.transform.position = this.m_TargetSyncPosition;
						base.transform.rotation = this.m_TargetSyncRotation3D;
					}
					else if (this.GetNetworkSendInterval() == 0f)
					{
						base.transform.position = this.m_TargetSyncPosition;
						if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
						{
							base.transform.rotation = this.m_TargetSyncRotation3D;
						}
					}
					else
					{
						float magnitude = (base.transform.position - this.m_TargetSyncPosition).magnitude;
						if (magnitude > this.snapThreshold)
						{
							base.transform.position = this.m_TargetSyncPosition;
						}
						if (this.interpolateRotation == 0f && this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
						{
							base.transform.rotation = this.m_TargetSyncRotation3D;
						}
						if (this.m_InterpolateMovement == 0f)
						{
							base.transform.position = this.m_TargetSyncPosition;
						}
						if (initialState && this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
						{
							base.transform.rotation = this.m_TargetSyncRotation3D;
						}
					}
				}
			}
		}

		// Token: 0x06000501 RID: 1281 RVA: 0x0001BBB4 File Offset: 0x00019DB4
		private void FixedUpdate()
		{
			if (base.isServer)
			{
				this.FixedUpdateServer();
			}
			if (base.isClient)
			{
				this.FixedUpdateClient();
			}
		}

		// Token: 0x06000502 RID: 1282 RVA: 0x0001BBE0 File Offset: 0x00019DE0
		private void FixedUpdateServer()
		{
			if (base.syncVarDirtyBits == 0U)
			{
				if (NetworkServer.active)
				{
					if (base.isServer)
					{
						if (this.GetNetworkSendInterval() != 0f)
						{
							float num = (base.transform.position - this.m_PrevPosition).magnitude;
							if (num < this.movementTheshold)
							{
								num = Quaternion.Angle(this.m_PrevRotation, base.transform.rotation);
								if (num < this.movementTheshold)
								{
									if (!this.CheckVelocityChanged())
									{
										return;
									}
								}
							}
							base.SetDirtyBit(1U);
						}
					}
				}
			}
		}

		// Token: 0x06000503 RID: 1283 RVA: 0x0001BC9C File Offset: 0x00019E9C
		private bool CheckVelocityChanged()
		{
			NetworkTransform.TransformSyncMode transformSyncMode = this.transformSyncMode;
			bool result;
			if (transformSyncMode != NetworkTransform.TransformSyncMode.SyncRigidbody2D)
			{
				result = (transformSyncMode == NetworkTransform.TransformSyncMode.SyncRigidbody3D && (this.m_RigidBody3D && this.m_VelocityThreshold > 0f) && Mathf.Abs(this.m_RigidBody3D.velocity.sqrMagnitude - this.m_PrevVelocity) >= this.m_VelocityThreshold);
			}
			else
			{
				result = (this.m_RigidBody2D && this.m_VelocityThreshold > 0f && Mathf.Abs(this.m_RigidBody2D.velocity.sqrMagnitude - this.m_PrevVelocity) >= this.m_VelocityThreshold);
			}
			return result;
		}

		// Token: 0x06000504 RID: 1284 RVA: 0x0001BD80 File Offset: 0x00019F80
		private void FixedUpdateClient()
		{
			if (this.m_LastClientSyncTime != 0f)
			{
				if (NetworkServer.active || NetworkClient.active)
				{
					if (base.isServer || base.isClient)
					{
						if (this.GetNetworkSendInterval() != 0f)
						{
							if (!base.hasAuthority)
							{
								switch (this.transformSyncMode)
								{
								case NetworkTransform.TransformSyncMode.SyncRigidbody2D:
									this.InterpolateTransformMode2D();
									break;
								case NetworkTransform.TransformSyncMode.SyncRigidbody3D:
									this.InterpolateTransformMode3D();
									break;
								case NetworkTransform.TransformSyncMode.SyncCharacterController:
									this.InterpolateTransformModeCharacterController();
									break;
								}
							}
						}
					}
				}
			}
		}

		// Token: 0x06000505 RID: 1285 RVA: 0x0001BE54 File Offset: 0x0001A054
		private void InterpolateTransformMode3D()
		{
			if (this.m_InterpolateMovement != 0f)
			{
				Vector3 velocity = (this.m_TargetSyncPosition - this.m_RigidBody3D.position) * this.m_InterpolateMovement / this.GetNetworkSendInterval();
				this.m_RigidBody3D.velocity = velocity;
			}
			if (this.interpolateRotation != 0f)
			{
				this.m_RigidBody3D.MoveRotation(Quaternion.Slerp(this.m_RigidBody3D.rotation, this.m_TargetSyncRotation3D, Time.fixedDeltaTime * this.interpolateRotation));
			}
			this.m_TargetSyncPosition += this.m_TargetSyncVelocity * Time.fixedDeltaTime * 0.1f;
		}

		// Token: 0x06000506 RID: 1286 RVA: 0x0001BF18 File Offset: 0x0001A118
		private void InterpolateTransformModeCharacterController()
		{
			if (!(this.m_FixedPosDiff == Vector3.zero) || !(this.m_TargetSyncRotation3D == base.transform.rotation))
			{
				if (this.m_InterpolateMovement != 0f)
				{
					this.m_CharacterController.Move(this.m_FixedPosDiff * this.m_InterpolateMovement);
				}
				if (this.interpolateRotation != 0f)
				{
					base.transform.rotation = Quaternion.Slerp(base.transform.rotation, this.m_TargetSyncRotation3D, Time.fixedDeltaTime * this.interpolateRotation * 10f);
				}
				if (Time.time - this.m_LastClientSyncTime > this.GetNetworkSendInterval())
				{
					this.m_FixedPosDiff = Vector3.zero;
					Vector3 motion = this.m_TargetSyncPosition - base.transform.position;
					this.m_CharacterController.Move(motion);
				}
			}
		}

		// Token: 0x06000507 RID: 1287 RVA: 0x0001C018 File Offset: 0x0001A218
		private void InterpolateTransformMode2D()
		{
			if (this.m_InterpolateMovement != 0f)
			{
				Vector2 velocity = this.m_RigidBody2D.velocity;
				Vector2 velocity2 = (this.m_TargetSyncPosition - this.m_RigidBody2D.position) * this.m_InterpolateMovement / this.GetNetworkSendInterval();
				if (!this.m_Grounded && velocity2.y < 0f)
				{
					velocity2.y = velocity.y;
				}
				this.m_RigidBody2D.velocity = velocity2;
			}
			if (this.interpolateRotation != 0f)
			{
				float num = this.m_RigidBody2D.rotation % 360f;
				if (num < 0f)
				{
					num += 360f;
				}
				Quaternion quaternion = Quaternion.Slerp(base.transform.rotation, Quaternion.Euler(0f, 0f, this.m_TargetSyncRotation2D), Time.fixedDeltaTime * this.interpolateRotation / this.GetNetworkSendInterval());
				this.m_RigidBody2D.MoveRotation(quaternion.eulerAngles.z);
				this.m_TargetSyncRotation2D += this.m_TargetSyncAngularVelocity2D * Time.fixedDeltaTime * 0.1f;
			}
			this.m_TargetSyncPosition += this.m_TargetSyncVelocity * Time.fixedDeltaTime * 0.1f;
		}

		// Token: 0x06000508 RID: 1288 RVA: 0x0001C188 File Offset: 0x0001A388
		private void Update()
		{
			if (base.hasAuthority)
			{
				if (base.localPlayerAuthority)
				{
					if (!NetworkServer.active)
					{
						if (Time.time - this.m_LastClientSendTime > this.GetNetworkSendInterval())
						{
							this.SendTransform();
							this.m_LastClientSendTime = Time.time;
						}
					}
				}
			}
		}

		// Token: 0x06000509 RID: 1289 RVA: 0x0001C1F0 File Offset: 0x0001A3F0
		private bool HasMoved()
		{
			float num;
			if (this.m_RigidBody3D != null)
			{
				num = (this.m_RigidBody3D.position - this.m_PrevPosition).magnitude;
			}
			else if (this.m_RigidBody2D != null)
			{
				num = (this.m_RigidBody2D.position - this.m_PrevPosition).magnitude;
			}
			else
			{
				num = (base.transform.position - this.m_PrevPosition).magnitude;
			}
			bool result;
			if (num > 1E-05f)
			{
				result = true;
			}
			else
			{
				if (this.m_RigidBody3D != null)
				{
					num = Quaternion.Angle(this.m_RigidBody3D.rotation, this.m_PrevRotation);
				}
				else if (this.m_RigidBody2D != null)
				{
					num = Math.Abs(this.m_RigidBody2D.rotation - this.m_PrevRotation2D);
				}
				else
				{
					num = Quaternion.Angle(base.transform.rotation, this.m_PrevRotation);
				}
				if (num > 1E-05f)
				{
					result = true;
				}
				else
				{
					if (this.m_RigidBody3D != null)
					{
						num = Mathf.Abs(this.m_RigidBody3D.velocity.sqrMagnitude - this.m_PrevVelocity);
					}
					else if (this.m_RigidBody2D != null)
					{
						num = Mathf.Abs(this.m_RigidBody2D.velocity.sqrMagnitude - this.m_PrevVelocity);
					}
					result = (num > 1E-05f);
				}
			}
			return result;
		}

		// Token: 0x0600050A RID: 1290 RVA: 0x0001C3C4 File Offset: 0x0001A5C4
		[Client]
		private void SendTransform()
		{
			if (this.HasMoved() && ClientScene.readyConnection != null)
			{
				this.m_LocalTransformWriter.StartMessage(6);
				this.m_LocalTransformWriter.Write(base.netId);
				switch (this.transformSyncMode)
				{
				case NetworkTransform.TransformSyncMode.SyncNone:
					return;
				case NetworkTransform.TransformSyncMode.SyncTransform:
					this.SerializeModeTransform(this.m_LocalTransformWriter);
					break;
				case NetworkTransform.TransformSyncMode.SyncRigidbody2D:
					this.SerializeMode2D(this.m_LocalTransformWriter);
					break;
				case NetworkTransform.TransformSyncMode.SyncRigidbody3D:
					this.SerializeMode3D(this.m_LocalTransformWriter);
					break;
				case NetworkTransform.TransformSyncMode.SyncCharacterController:
					this.SerializeModeCharacterController(this.m_LocalTransformWriter);
					break;
				}
				if (this.m_RigidBody3D != null)
				{
					this.m_PrevPosition = this.m_RigidBody3D.position;
					this.m_PrevRotation = this.m_RigidBody3D.rotation;
					this.m_PrevVelocity = this.m_RigidBody3D.velocity.sqrMagnitude;
				}
				else if (this.m_RigidBody2D != null)
				{
					this.m_PrevPosition = this.m_RigidBody2D.position;
					this.m_PrevRotation2D = this.m_RigidBody2D.rotation;
					this.m_PrevVelocity = this.m_RigidBody2D.velocity.sqrMagnitude;
				}
				else
				{
					this.m_PrevPosition = base.transform.position;
					this.m_PrevRotation = base.transform.rotation;
				}
				this.m_LocalTransformWriter.FinishMessage();
				ClientScene.readyConnection.SendWriter(this.m_LocalTransformWriter, this.GetNetworkChannel());
			}
		}

		// Token: 0x0600050B RID: 1291 RVA: 0x0001C570 File Offset: 0x0001A770
		public static void HandleTransform(NetworkMessage netMsg)
		{
			NetworkInstanceId networkInstanceId = netMsg.reader.ReadNetworkId();
			GameObject gameObject = NetworkServer.FindLocalObject(networkInstanceId);
			if (gameObject == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("Received NetworkTransform data for GameObject that doesn't exist");
				}
			}
			else
			{
				NetworkTransform component = gameObject.GetComponent<NetworkTransform>();
				if (component == null)
				{
					if (LogFilter.logError)
					{
						Debug.LogError("HandleTransform null target");
					}
				}
				else if (!component.localPlayerAuthority)
				{
					if (LogFilter.logError)
					{
						Debug.LogError("HandleTransform no localPlayerAuthority");
					}
				}
				else if (netMsg.conn.clientOwnedObjects == null)
				{
					if (LogFilter.logError)
					{
						Debug.LogError("HandleTransform object not owned by connection");
					}
				}
				else if (netMsg.conn.clientOwnedObjects.Contains(networkInstanceId))
				{
					switch (component.transformSyncMode)
					{
					case NetworkTransform.TransformSyncMode.SyncNone:
						return;
					case NetworkTransform.TransformSyncMode.SyncTransform:
						component.UnserializeModeTransform(netMsg.reader, false);
						break;
					case NetworkTransform.TransformSyncMode.SyncRigidbody2D:
						component.UnserializeMode2D(netMsg.reader, false);
						break;
					case NetworkTransform.TransformSyncMode.SyncRigidbody3D:
						component.UnserializeMode3D(netMsg.reader, false);
						break;
					case NetworkTransform.TransformSyncMode.SyncCharacterController:
						component.UnserializeModeCharacterController(netMsg.reader, false);
						break;
					}
					component.m_LastClientSyncTime = Time.time;
				}
				else if (LogFilter.logWarn)
				{
					Debug.LogWarning("HandleTransform netId:" + networkInstanceId + " is not for a valid player");
				}
			}
		}

		// Token: 0x0600050C RID: 1292 RVA: 0x0001C700 File Offset: 0x0001A900
		private static void WriteAngle(NetworkWriter writer, float angle, NetworkTransform.CompressionSyncMode compression)
		{
			if (compression != NetworkTransform.CompressionSyncMode.None)
			{
				if (compression != NetworkTransform.CompressionSyncMode.Low)
				{
					if (compression == NetworkTransform.CompressionSyncMode.High)
					{
						writer.Write((short)angle);
					}
				}
				else
				{
					writer.Write((short)angle);
				}
			}
			else
			{
				writer.Write(angle);
			}
		}

		// Token: 0x0600050D RID: 1293 RVA: 0x0001C750 File Offset: 0x0001A950
		private static float ReadAngle(NetworkReader reader, NetworkTransform.CompressionSyncMode compression)
		{
			float result;
			if (compression != NetworkTransform.CompressionSyncMode.None)
			{
				if (compression != NetworkTransform.CompressionSyncMode.Low)
				{
					if (compression != NetworkTransform.CompressionSyncMode.High)
					{
						result = 0f;
					}
					else
					{
						result = (float)reader.ReadInt16();
					}
				}
				else
				{
					result = (float)reader.ReadInt16();
				}
			}
			else
			{
				result = reader.ReadSingle();
			}
			return result;
		}

		// Token: 0x0600050E RID: 1294 RVA: 0x0001C7AC File Offset: 0x0001A9AC
		public static void SerializeVelocity3D(NetworkWriter writer, Vector3 velocity, NetworkTransform.CompressionSyncMode compression)
		{
			writer.Write(velocity);
		}

		// Token: 0x0600050F RID: 1295 RVA: 0x0001C7B6 File Offset: 0x0001A9B6
		public static void SerializeVelocity2D(NetworkWriter writer, Vector2 velocity, NetworkTransform.CompressionSyncMode compression)
		{
			writer.Write(velocity);
		}

		// Token: 0x06000510 RID: 1296 RVA: 0x0001C7C0 File Offset: 0x0001A9C0
		public static void SerializeRotation3D(NetworkWriter writer, Quaternion rot, NetworkTransform.AxisSyncMode mode, NetworkTransform.CompressionSyncMode compression)
		{
			switch (mode)
			{
			case NetworkTransform.AxisSyncMode.AxisX:
				NetworkTransform.WriteAngle(writer, rot.eulerAngles.x, compression);
				break;
			case NetworkTransform.AxisSyncMode.AxisY:
				NetworkTransform.WriteAngle(writer, rot.eulerAngles.y, compression);
				break;
			case NetworkTransform.AxisSyncMode.AxisZ:
				NetworkTransform.WriteAngle(writer, rot.eulerAngles.z, compression);
				break;
			case NetworkTransform.AxisSyncMode.AxisXY:
				NetworkTransform.WriteAngle(writer, rot.eulerAngles.x, compression);
				NetworkTransform.WriteAngle(writer, rot.eulerAngles.y, compression);
				break;
			case NetworkTransform.AxisSyncMode.AxisXZ:
				NetworkTransform.WriteAngle(writer, rot.eulerAngles.x, compression);
				NetworkTransform.WriteAngle(writer, rot.eulerAngles.z, compression);
				break;
			case NetworkTransform.AxisSyncMode.AxisYZ:
				NetworkTransform.WriteAngle(writer, rot.eulerAngles.y, compression);
				NetworkTransform.WriteAngle(writer, rot.eulerAngles.z, compression);
				break;
			case NetworkTransform.AxisSyncMode.AxisXYZ:
				NetworkTransform.WriteAngle(writer, rot.eulerAngles.x, compression);
				NetworkTransform.WriteAngle(writer, rot.eulerAngles.y, compression);
				NetworkTransform.WriteAngle(writer, rot.eulerAngles.z, compression);
				break;
			}
		}

		// Token: 0x06000511 RID: 1297 RVA: 0x0001C931 File Offset: 0x0001AB31
		public static void SerializeRotation2D(NetworkWriter writer, float rot, NetworkTransform.CompressionSyncMode compression)
		{
			NetworkTransform.WriteAngle(writer, rot, compression);
		}

		// Token: 0x06000512 RID: 1298 RVA: 0x0001C93C File Offset: 0x0001AB3C
		public static void SerializeSpin3D(NetworkWriter writer, Vector3 angularVelocity, NetworkTransform.AxisSyncMode mode, NetworkTransform.CompressionSyncMode compression)
		{
			switch (mode)
			{
			case NetworkTransform.AxisSyncMode.AxisX:
				NetworkTransform.WriteAngle(writer, angularVelocity.x, compression);
				break;
			case NetworkTransform.AxisSyncMode.AxisY:
				NetworkTransform.WriteAngle(writer, angularVelocity.y, compression);
				break;
			case NetworkTransform.AxisSyncMode.AxisZ:
				NetworkTransform.WriteAngle(writer, angularVelocity.z, compression);
				break;
			case NetworkTransform.AxisSyncMode.AxisXY:
				NetworkTransform.WriteAngle(writer, angularVelocity.x, compression);
				NetworkTransform.WriteAngle(writer, angularVelocity.y, compression);
				break;
			case NetworkTransform.AxisSyncMode.AxisXZ:
				NetworkTransform.WriteAngle(writer, angularVelocity.x, compression);
				NetworkTransform.WriteAngle(writer, angularVelocity.z, compression);
				break;
			case NetworkTransform.AxisSyncMode.AxisYZ:
				NetworkTransform.WriteAngle(writer, angularVelocity.y, compression);
				NetworkTransform.WriteAngle(writer, angularVelocity.z, compression);
				break;
			case NetworkTransform.AxisSyncMode.AxisXYZ:
				NetworkTransform.WriteAngle(writer, angularVelocity.x, compression);
				NetworkTransform.WriteAngle(writer, angularVelocity.y, compression);
				NetworkTransform.WriteAngle(writer, angularVelocity.z, compression);
				break;
			}
		}

		// Token: 0x06000513 RID: 1299 RVA: 0x0001CA45 File Offset: 0x0001AC45
		public static void SerializeSpin2D(NetworkWriter writer, float angularVelocity, NetworkTransform.CompressionSyncMode compression)
		{
			NetworkTransform.WriteAngle(writer, angularVelocity, compression);
		}

		// Token: 0x06000514 RID: 1300 RVA: 0x0001CA50 File Offset: 0x0001AC50
		public static Vector3 UnserializeVelocity3D(NetworkReader reader, NetworkTransform.CompressionSyncMode compression)
		{
			return reader.ReadVector3();
		}

		// Token: 0x06000515 RID: 1301 RVA: 0x0001CA6C File Offset: 0x0001AC6C
		public static Vector3 UnserializeVelocity2D(NetworkReader reader, NetworkTransform.CompressionSyncMode compression)
		{
			return reader.ReadVector2();
		}

		// Token: 0x06000516 RID: 1302 RVA: 0x0001CA8C File Offset: 0x0001AC8C
		public static Quaternion UnserializeRotation3D(NetworkReader reader, NetworkTransform.AxisSyncMode mode, NetworkTransform.CompressionSyncMode compression)
		{
			Quaternion identity = Quaternion.identity;
			Vector3 zero = Vector3.zero;
			switch (mode)
			{
			case NetworkTransform.AxisSyncMode.AxisX:
				zero.Set(NetworkTransform.ReadAngle(reader, compression), 0f, 0f);
				identity.eulerAngles = zero;
				break;
			case NetworkTransform.AxisSyncMode.AxisY:
				zero.Set(0f, NetworkTransform.ReadAngle(reader, compression), 0f);
				identity.eulerAngles = zero;
				break;
			case NetworkTransform.AxisSyncMode.AxisZ:
				zero.Set(0f, 0f, NetworkTransform.ReadAngle(reader, compression));
				identity.eulerAngles = zero;
				break;
			case NetworkTransform.AxisSyncMode.AxisXY:
				zero.Set(NetworkTransform.ReadAngle(reader, compression), NetworkTransform.ReadAngle(reader, compression), 0f);
				identity.eulerAngles = zero;
				break;
			case NetworkTransform.AxisSyncMode.AxisXZ:
				zero.Set(NetworkTransform.ReadAngle(reader, compression), 0f, NetworkTransform.ReadAngle(reader, compression));
				identity.eulerAngles = zero;
				break;
			case NetworkTransform.AxisSyncMode.AxisYZ:
				zero.Set(0f, NetworkTransform.ReadAngle(reader, compression), NetworkTransform.ReadAngle(reader, compression));
				identity.eulerAngles = zero;
				break;
			case NetworkTransform.AxisSyncMode.AxisXYZ:
				zero.Set(NetworkTransform.ReadAngle(reader, compression), NetworkTransform.ReadAngle(reader, compression), NetworkTransform.ReadAngle(reader, compression));
				identity.eulerAngles = zero;
				break;
			}
			return identity;
		}

		// Token: 0x06000517 RID: 1303 RVA: 0x0001CBEC File Offset: 0x0001ADEC
		public static float UnserializeRotation2D(NetworkReader reader, NetworkTransform.CompressionSyncMode compression)
		{
			return NetworkTransform.ReadAngle(reader, compression);
		}

		// Token: 0x06000518 RID: 1304 RVA: 0x0001CC08 File Offset: 0x0001AE08
		public static Vector3 UnserializeSpin3D(NetworkReader reader, NetworkTransform.AxisSyncMode mode, NetworkTransform.CompressionSyncMode compression)
		{
			Vector3 zero = Vector3.zero;
			switch (mode)
			{
			case NetworkTransform.AxisSyncMode.AxisX:
				zero.Set(NetworkTransform.ReadAngle(reader, compression), 0f, 0f);
				break;
			case NetworkTransform.AxisSyncMode.AxisY:
				zero.Set(0f, NetworkTransform.ReadAngle(reader, compression), 0f);
				break;
			case NetworkTransform.AxisSyncMode.AxisZ:
				zero.Set(0f, 0f, NetworkTransform.ReadAngle(reader, compression));
				break;
			case NetworkTransform.AxisSyncMode.AxisXY:
				zero.Set(NetworkTransform.ReadAngle(reader, compression), NetworkTransform.ReadAngle(reader, compression), 0f);
				break;
			case NetworkTransform.AxisSyncMode.AxisXZ:
				zero.Set(NetworkTransform.ReadAngle(reader, compression), 0f, NetworkTransform.ReadAngle(reader, compression));
				break;
			case NetworkTransform.AxisSyncMode.AxisYZ:
				zero.Set(0f, NetworkTransform.ReadAngle(reader, compression), NetworkTransform.ReadAngle(reader, compression));
				break;
			case NetworkTransform.AxisSyncMode.AxisXYZ:
				zero.Set(NetworkTransform.ReadAngle(reader, compression), NetworkTransform.ReadAngle(reader, compression), NetworkTransform.ReadAngle(reader, compression));
				break;
			}
			return zero;
		}

		// Token: 0x06000519 RID: 1305 RVA: 0x0001CD2C File Offset: 0x0001AF2C
		public static float UnserializeSpin2D(NetworkReader reader, NetworkTransform.CompressionSyncMode compression)
		{
			return NetworkTransform.ReadAngle(reader, compression);
		}

		// Token: 0x0600051A RID: 1306 RVA: 0x0001CD48 File Offset: 0x0001AF48
		public override int GetNetworkChannel()
		{
			return 1;
		}

		// Token: 0x0600051B RID: 1307 RVA: 0x0001CD60 File Offset: 0x0001AF60
		public override float GetNetworkSendInterval()
		{
			return this.m_SendInterval;
		}

		// Token: 0x0600051C RID: 1308 RVA: 0x0001CD7B File Offset: 0x0001AF7B
		public override void OnStartAuthority()
		{
			this.m_LastClientSyncTime = 0f;
		}

		// Token: 0x040001F2 RID: 498
		[SerializeField]
		private NetworkTransform.TransformSyncMode m_TransformSyncMode = NetworkTransform.TransformSyncMode.SyncNone;

		// Token: 0x040001F3 RID: 499
		[SerializeField]
		private float m_SendInterval = 0.1f;

		// Token: 0x040001F4 RID: 500
		[SerializeField]
		private NetworkTransform.AxisSyncMode m_SyncRotationAxis = NetworkTransform.AxisSyncMode.AxisXYZ;

		// Token: 0x040001F5 RID: 501
		[SerializeField]
		private NetworkTransform.CompressionSyncMode m_RotationSyncCompression = NetworkTransform.CompressionSyncMode.None;

		// Token: 0x040001F6 RID: 502
		[SerializeField]
		private bool m_SyncSpin;

		// Token: 0x040001F7 RID: 503
		[SerializeField]
		private float m_MovementTheshold = 0.001f;

		// Token: 0x040001F8 RID: 504
		[SerializeField]
		private float m_VelocityThreshold = 0.0001f;

		// Token: 0x040001F9 RID: 505
		[SerializeField]
		private float m_SnapThreshold = 5f;

		// Token: 0x040001FA RID: 506
		[SerializeField]
		private float m_InterpolateRotation = 1f;

		// Token: 0x040001FB RID: 507
		[SerializeField]
		private float m_InterpolateMovement = 1f;

		// Token: 0x040001FC RID: 508
		[SerializeField]
		private NetworkTransform.ClientMoveCallback3D m_ClientMoveCallback3D;

		// Token: 0x040001FD RID: 509
		[SerializeField]
		private NetworkTransform.ClientMoveCallback2D m_ClientMoveCallback2D;

		// Token: 0x040001FE RID: 510
		private Rigidbody m_RigidBody3D;

		// Token: 0x040001FF RID: 511
		private Rigidbody2D m_RigidBody2D;

		// Token: 0x04000200 RID: 512
		private CharacterController m_CharacterController;

		// Token: 0x04000201 RID: 513
		private bool m_Grounded = true;

		// Token: 0x04000202 RID: 514
		private Vector3 m_TargetSyncPosition;

		// Token: 0x04000203 RID: 515
		private Vector3 m_TargetSyncVelocity;

		// Token: 0x04000204 RID: 516
		private Vector3 m_FixedPosDiff;

		// Token: 0x04000205 RID: 517
		private Quaternion m_TargetSyncRotation3D;

		// Token: 0x04000206 RID: 518
		private Vector3 m_TargetSyncAngularVelocity3D;

		// Token: 0x04000207 RID: 519
		private float m_TargetSyncRotation2D;

		// Token: 0x04000208 RID: 520
		private float m_TargetSyncAngularVelocity2D;

		// Token: 0x04000209 RID: 521
		private float m_LastClientSyncTime;

		// Token: 0x0400020A RID: 522
		private float m_LastClientSendTime;

		// Token: 0x0400020B RID: 523
		private Vector3 m_PrevPosition;

		// Token: 0x0400020C RID: 524
		private Quaternion m_PrevRotation;

		// Token: 0x0400020D RID: 525
		private float m_PrevRotation2D;

		// Token: 0x0400020E RID: 526
		private float m_PrevVelocity;

		// Token: 0x0400020F RID: 527
		private const float k_LocalMovementThreshold = 1E-05f;

		// Token: 0x04000210 RID: 528
		private const float k_LocalRotationThreshold = 1E-05f;

		// Token: 0x04000211 RID: 529
		private const float k_LocalVelocityThreshold = 1E-05f;

		// Token: 0x04000212 RID: 530
		private const float k_MoveAheadRatio = 0.1f;

		// Token: 0x04000213 RID: 531
		private NetworkWriter m_LocalTransformWriter;

		// Token: 0x02000060 RID: 96
		public enum TransformSyncMode
		{
			// Token: 0x04000215 RID: 533
			SyncNone,
			// Token: 0x04000216 RID: 534
			SyncTransform,
			// Token: 0x04000217 RID: 535
			SyncRigidbody2D,
			// Token: 0x04000218 RID: 536
			SyncRigidbody3D,
			// Token: 0x04000219 RID: 537
			SyncCharacterController
		}

		// Token: 0x02000061 RID: 97
		public enum AxisSyncMode
		{
			// Token: 0x0400021B RID: 539
			None,
			// Token: 0x0400021C RID: 540
			AxisX,
			// Token: 0x0400021D RID: 541
			AxisY,
			// Token: 0x0400021E RID: 542
			AxisZ,
			// Token: 0x0400021F RID: 543
			AxisXY,
			// Token: 0x04000220 RID: 544
			AxisXZ,
			// Token: 0x04000221 RID: 545
			AxisYZ,
			// Token: 0x04000222 RID: 546
			AxisXYZ
		}

		// Token: 0x02000062 RID: 98
		public enum CompressionSyncMode
		{
			// Token: 0x04000224 RID: 548
			None,
			// Token: 0x04000225 RID: 549
			Low,
			// Token: 0x04000226 RID: 550
			High
		}

		// Token: 0x02000063 RID: 99
		// (Invoke) Token: 0x0600051E RID: 1310
		public delegate bool ClientMoveCallback3D(ref Vector3 position, ref Vector3 velocity, ref Quaternion rotation);

		// Token: 0x02000064 RID: 100
		// (Invoke) Token: 0x06000522 RID: 1314
		public delegate bool ClientMoveCallback2D(ref Vector2 position, ref Vector2 velocity, ref float rotation);
	}
}
