﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000046 RID: 70
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	[Serializable]
	public struct NetworkHash128
	{
		// Token: 0x0600023D RID: 573 RVA: 0x0000BCF0 File Offset: 0x00009EF0
		public void Reset()
		{
			this.i0 = 0;
			this.i1 = 0;
			this.i2 = 0;
			this.i3 = 0;
			this.i4 = 0;
			this.i5 = 0;
			this.i6 = 0;
			this.i7 = 0;
			this.i8 = 0;
			this.i9 = 0;
			this.i10 = 0;
			this.i11 = 0;
			this.i12 = 0;
			this.i13 = 0;
			this.i14 = 0;
			this.i15 = 0;
		}

		// Token: 0x0600023E RID: 574 RVA: 0x0000BD70 File Offset: 0x00009F70
		public bool IsValid()
		{
			return (this.i0 | this.i1 | this.i2 | this.i3 | this.i4 | this.i5 | this.i6 | this.i7 | this.i8 | this.i9 | this.i10 | this.i11 | this.i12 | this.i13 | this.i14 | this.i15) != 0;
		}

		// Token: 0x0600023F RID: 575 RVA: 0x0000BDFC File Offset: 0x00009FFC
		private static int HexToNumber(char c)
		{
			int result;
			if (c >= '0' && c <= '9')
			{
				result = (int)(c - '0');
			}
			else if (c >= 'a' && c <= 'f')
			{
				result = (int)(c - 'a' + '\n');
			}
			else if (c >= 'A' && c <= 'F')
			{
				result = (int)(c - 'A' + '\n');
			}
			else
			{
				result = 0;
			}
			return result;
		}

		// Token: 0x06000240 RID: 576 RVA: 0x0000BE68 File Offset: 0x0000A068
		public static NetworkHash128 Parse(string text)
		{
			int length = text.Length;
			if (length < 32)
			{
				string str = "";
				for (int i = 0; i < 32 - length; i++)
				{
					str += "0";
				}
				text = str + text;
			}
			NetworkHash128 result;
			result.i0 = (byte)(NetworkHash128.HexToNumber(text[0]) * 16 + NetworkHash128.HexToNumber(text[1]));
			result.i1 = (byte)(NetworkHash128.HexToNumber(text[2]) * 16 + NetworkHash128.HexToNumber(text[3]));
			result.i2 = (byte)(NetworkHash128.HexToNumber(text[4]) * 16 + NetworkHash128.HexToNumber(text[5]));
			result.i3 = (byte)(NetworkHash128.HexToNumber(text[6]) * 16 + NetworkHash128.HexToNumber(text[7]));
			result.i4 = (byte)(NetworkHash128.HexToNumber(text[8]) * 16 + NetworkHash128.HexToNumber(text[9]));
			result.i5 = (byte)(NetworkHash128.HexToNumber(text[10]) * 16 + NetworkHash128.HexToNumber(text[11]));
			result.i6 = (byte)(NetworkHash128.HexToNumber(text[12]) * 16 + NetworkHash128.HexToNumber(text[13]));
			result.i7 = (byte)(NetworkHash128.HexToNumber(text[14]) * 16 + NetworkHash128.HexToNumber(text[15]));
			result.i8 = (byte)(NetworkHash128.HexToNumber(text[16]) * 16 + NetworkHash128.HexToNumber(text[17]));
			result.i9 = (byte)(NetworkHash128.HexToNumber(text[18]) * 16 + NetworkHash128.HexToNumber(text[19]));
			result.i10 = (byte)(NetworkHash128.HexToNumber(text[20]) * 16 + NetworkHash128.HexToNumber(text[21]));
			result.i11 = (byte)(NetworkHash128.HexToNumber(text[22]) * 16 + NetworkHash128.HexToNumber(text[23]));
			result.i12 = (byte)(NetworkHash128.HexToNumber(text[24]) * 16 + NetworkHash128.HexToNumber(text[25]));
			result.i13 = (byte)(NetworkHash128.HexToNumber(text[26]) * 16 + NetworkHash128.HexToNumber(text[27]));
			result.i14 = (byte)(NetworkHash128.HexToNumber(text[28]) * 16 + NetworkHash128.HexToNumber(text[29]));
			result.i15 = (byte)(NetworkHash128.HexToNumber(text[30]) * 16 + NetworkHash128.HexToNumber(text[31]));
			return result;
		}

		// Token: 0x06000241 RID: 577 RVA: 0x0000C11C File Offset: 0x0000A31C
		public override string ToString()
		{
			return string.Format("{0:x2}{1:x2}{2:x2}{3:x2}{4:x2}{5:x2}{6:x2}{7:x2}{8:x2}{9:x2}{10:x2}{11:x2}{12:x2}{13:x2}{14:x2}{15:x2}", new object[]
			{
				this.i0,
				this.i1,
				this.i2,
				this.i3,
				this.i4,
				this.i5,
				this.i6,
				this.i7,
				this.i8,
				this.i9,
				this.i10,
				this.i11,
				this.i12,
				this.i13,
				this.i14,
				this.i15
			});
		}

		// Token: 0x0400011D RID: 285
		public byte i0;

		// Token: 0x0400011E RID: 286
		public byte i1;

		// Token: 0x0400011F RID: 287
		public byte i2;

		// Token: 0x04000120 RID: 288
		public byte i3;

		// Token: 0x04000121 RID: 289
		public byte i4;

		// Token: 0x04000122 RID: 290
		public byte i5;

		// Token: 0x04000123 RID: 291
		public byte i6;

		// Token: 0x04000124 RID: 292
		public byte i7;

		// Token: 0x04000125 RID: 293
		public byte i8;

		// Token: 0x04000126 RID: 294
		public byte i9;

		// Token: 0x04000127 RID: 295
		public byte i10;

		// Token: 0x04000128 RID: 296
		public byte i11;

		// Token: 0x04000129 RID: 297
		public byte i12;

		// Token: 0x0400012A RID: 298
		public byte i13;

		// Token: 0x0400012B RID: 299
		public byte i14;

		// Token: 0x0400012C RID: 300
		public byte i15;
	}
}
