﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000067 RID: 103
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class PlayerController
	{
		// Token: 0x0600055D RID: 1373 RVA: 0x0001E2AE File Offset: 0x0001C4AE
		public PlayerController()
		{
		}

		// Token: 0x0600055E RID: 1374 RVA: 0x0001E2BE File Offset: 0x0001C4BE
		internal PlayerController(GameObject go, short playerControllerId)
		{
			this.gameObject = go;
			this.unetView = go.GetComponent<NetworkIdentity>();
			this.playerControllerId = playerControllerId;
		}

		// Token: 0x170000DF RID: 223
		// (get) Token: 0x0600055F RID: 1375 RVA: 0x0001E2E8 File Offset: 0x0001C4E8
		public bool IsValid
		{
			get
			{
				return this.playerControllerId != -1;
			}
		}

		// Token: 0x06000560 RID: 1376 RVA: 0x0001E30C File Offset: 0x0001C50C
		public override string ToString()
		{
			return string.Format("ID={0} NetworkIdentity NetID={1} Player={2}", new object[]
			{
				this.playerControllerId,
				(!(this.unetView != null)) ? "null" : this.unetView.netId.ToString(),
				(!(this.gameObject != null)) ? "null" : this.gameObject.name
			});
		}

		// Token: 0x04000230 RID: 560
		internal const short kMaxLocalPlayers = 8;

		// Token: 0x04000231 RID: 561
		public short playerControllerId = -1;

		// Token: 0x04000232 RID: 562
		public NetworkIdentity unetView;

		// Token: 0x04000233 RID: 563
		public GameObject gameObject;

		// Token: 0x04000234 RID: 564
		public const int MaxPlayersPerClient = 32;
	}
}
