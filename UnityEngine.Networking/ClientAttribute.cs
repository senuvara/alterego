﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x0200000F RID: 15
	[AttributeUsage(AttributeTargets.Method)]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class ClientAttribute : Attribute
	{
		// Token: 0x0600006E RID: 110 RVA: 0x00004C92 File Offset: 0x00002E92
		public ClientAttribute()
		{
		}
	}
}
