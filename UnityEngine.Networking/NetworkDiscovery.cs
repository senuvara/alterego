﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Networking
{
	// Token: 0x02000045 RID: 69
	[DisallowMultipleComponent]
	[AddComponentMenu("Network/NetworkDiscovery")]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class NetworkDiscovery : MonoBehaviour
	{
		// Token: 0x06000215 RID: 533 RVA: 0x0000B108 File Offset: 0x00009308
		public NetworkDiscovery()
		{
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x06000216 RID: 534 RVA: 0x0000B16C File Offset: 0x0000936C
		// (set) Token: 0x06000217 RID: 535 RVA: 0x0000B187 File Offset: 0x00009387
		public int broadcastPort
		{
			get
			{
				return this.m_BroadcastPort;
			}
			set
			{
				this.m_BroadcastPort = value;
			}
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x06000218 RID: 536 RVA: 0x0000B194 File Offset: 0x00009394
		// (set) Token: 0x06000219 RID: 537 RVA: 0x0000B1AF File Offset: 0x000093AF
		public int broadcastKey
		{
			get
			{
				return this.m_BroadcastKey;
			}
			set
			{
				this.m_BroadcastKey = value;
			}
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x0600021A RID: 538 RVA: 0x0000B1BC File Offset: 0x000093BC
		// (set) Token: 0x0600021B RID: 539 RVA: 0x0000B1D7 File Offset: 0x000093D7
		public int broadcastVersion
		{
			get
			{
				return this.m_BroadcastVersion;
			}
			set
			{
				this.m_BroadcastVersion = value;
			}
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x0600021C RID: 540 RVA: 0x0000B1E4 File Offset: 0x000093E4
		// (set) Token: 0x0600021D RID: 541 RVA: 0x0000B1FF File Offset: 0x000093FF
		public int broadcastSubVersion
		{
			get
			{
				return this.m_BroadcastSubVersion;
			}
			set
			{
				this.m_BroadcastSubVersion = value;
			}
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x0600021E RID: 542 RVA: 0x0000B20C File Offset: 0x0000940C
		// (set) Token: 0x0600021F RID: 543 RVA: 0x0000B227 File Offset: 0x00009427
		public int broadcastInterval
		{
			get
			{
				return this.m_BroadcastInterval;
			}
			set
			{
				this.m_BroadcastInterval = value;
			}
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x06000220 RID: 544 RVA: 0x0000B234 File Offset: 0x00009434
		// (set) Token: 0x06000221 RID: 545 RVA: 0x0000B24F File Offset: 0x0000944F
		public bool useNetworkManager
		{
			get
			{
				return this.m_UseNetworkManager;
			}
			set
			{
				this.m_UseNetworkManager = value;
			}
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x06000222 RID: 546 RVA: 0x0000B25C File Offset: 0x0000945C
		// (set) Token: 0x06000223 RID: 547 RVA: 0x0000B277 File Offset: 0x00009477
		public string broadcastData
		{
			get
			{
				return this.m_BroadcastData;
			}
			set
			{
				this.m_BroadcastData = value;
				this.m_MsgOutBuffer = NetworkDiscovery.StringToBytes(this.m_BroadcastData);
				if (this.m_UseNetworkManager)
				{
					if (LogFilter.logWarn)
					{
						Debug.LogWarning("NetworkDiscovery broadcast data changed while using NetworkManager. This can prevent clients from finding the server. The format of the broadcast data must be 'NetworkManager:IPAddress:Port'.");
					}
				}
			}
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x06000224 RID: 548 RVA: 0x0000B2B8 File Offset: 0x000094B8
		// (set) Token: 0x06000225 RID: 549 RVA: 0x0000B2D3 File Offset: 0x000094D3
		public bool showGUI
		{
			get
			{
				return this.m_ShowGUI;
			}
			set
			{
				this.m_ShowGUI = value;
			}
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x06000226 RID: 550 RVA: 0x0000B2E0 File Offset: 0x000094E0
		// (set) Token: 0x06000227 RID: 551 RVA: 0x0000B2FB File Offset: 0x000094FB
		public int offsetX
		{
			get
			{
				return this.m_OffsetX;
			}
			set
			{
				this.m_OffsetX = value;
			}
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x06000228 RID: 552 RVA: 0x0000B308 File Offset: 0x00009508
		// (set) Token: 0x06000229 RID: 553 RVA: 0x0000B323 File Offset: 0x00009523
		public int offsetY
		{
			get
			{
				return this.m_OffsetY;
			}
			set
			{
				this.m_OffsetY = value;
			}
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x0600022A RID: 554 RVA: 0x0000B330 File Offset: 0x00009530
		// (set) Token: 0x0600022B RID: 555 RVA: 0x0000B34B File Offset: 0x0000954B
		public int hostId
		{
			get
			{
				return this.m_HostId;
			}
			set
			{
				this.m_HostId = value;
			}
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x0600022C RID: 556 RVA: 0x0000B358 File Offset: 0x00009558
		// (set) Token: 0x0600022D RID: 557 RVA: 0x0000B373 File Offset: 0x00009573
		public bool running
		{
			get
			{
				return this.m_Running;
			}
			set
			{
				this.m_Running = value;
			}
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x0600022E RID: 558 RVA: 0x0000B380 File Offset: 0x00009580
		// (set) Token: 0x0600022F RID: 559 RVA: 0x0000B39B File Offset: 0x0000959B
		public bool isServer
		{
			get
			{
				return this.m_IsServer;
			}
			set
			{
				this.m_IsServer = value;
			}
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x06000230 RID: 560 RVA: 0x0000B3A8 File Offset: 0x000095A8
		// (set) Token: 0x06000231 RID: 561 RVA: 0x0000B3C3 File Offset: 0x000095C3
		public bool isClient
		{
			get
			{
				return this.m_IsClient;
			}
			set
			{
				this.m_IsClient = value;
			}
		}

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x06000232 RID: 562 RVA: 0x0000B3D0 File Offset: 0x000095D0
		public Dictionary<string, NetworkBroadcastResult> broadcastsReceived
		{
			get
			{
				return this.m_BroadcastsReceived;
			}
		}

		// Token: 0x06000233 RID: 563 RVA: 0x0000B3EC File Offset: 0x000095EC
		private static byte[] StringToBytes(string str)
		{
			byte[] array = new byte[str.Length * 2];
			Buffer.BlockCopy(str.ToCharArray(), 0, array, 0, array.Length);
			return array;
		}

		// Token: 0x06000234 RID: 564 RVA: 0x0000B424 File Offset: 0x00009624
		private static string BytesToString(byte[] bytes)
		{
			char[] array = new char[bytes.Length / 2];
			Buffer.BlockCopy(bytes, 0, array, 0, bytes.Length);
			return new string(array);
		}

		// Token: 0x06000235 RID: 565 RVA: 0x0000B458 File Offset: 0x00009658
		public bool Initialize()
		{
			bool result;
			if (this.m_BroadcastData.Length >= 1024)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkDiscovery Initialize - data too large. max is " + 1024);
				}
				result = false;
			}
			else
			{
				if (!NetworkManager.activeTransport.IsStarted)
				{
					NetworkManager.activeTransport.Init();
				}
				if (this.m_UseNetworkManager && NetworkManager.singleton != null)
				{
					this.m_BroadcastData = string.Concat(new object[]
					{
						"NetworkManager:",
						NetworkManager.singleton.networkAddress,
						":",
						NetworkManager.singleton.networkPort
					});
					if (LogFilter.logInfo)
					{
						Debug.Log("NetworkDiscovery set broadcast data to:" + this.m_BroadcastData);
					}
				}
				this.m_MsgOutBuffer = NetworkDiscovery.StringToBytes(this.m_BroadcastData);
				this.m_MsgInBuffer = new byte[1024];
				this.m_BroadcastsReceived = new Dictionary<string, NetworkBroadcastResult>();
				ConnectionConfig connectionConfig = new ConnectionConfig();
				connectionConfig.AddChannel(QosType.Unreliable);
				this.m_DefaultTopology = new HostTopology(connectionConfig, 1);
				if (this.m_IsServer)
				{
					this.StartAsServer();
				}
				if (this.m_IsClient)
				{
					this.StartAsClient();
				}
				result = true;
			}
			return result;
		}

		// Token: 0x06000236 RID: 566 RVA: 0x0000B5B4 File Offset: 0x000097B4
		public bool StartAsClient()
		{
			bool result;
			if (this.m_HostId != -1 || this.m_Running)
			{
				if (LogFilter.logWarn)
				{
					Debug.LogWarning("NetworkDiscovery StartAsClient already started");
				}
				result = false;
			}
			else if (this.m_MsgInBuffer == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkDiscovery StartAsClient, NetworkDiscovery is not initialized");
				}
				result = false;
			}
			else
			{
				this.m_HostId = NetworkManager.activeTransport.AddHost(this.m_DefaultTopology, this.m_BroadcastPort, null);
				if (this.m_HostId == -1)
				{
					if (LogFilter.logError)
					{
						Debug.LogError("NetworkDiscovery StartAsClient - addHost failed");
					}
					result = false;
				}
				else
				{
					byte b;
					NetworkManager.activeTransport.SetBroadcastCredentials(this.m_HostId, this.m_BroadcastKey, this.m_BroadcastVersion, this.m_BroadcastSubVersion, out b);
					this.m_Running = true;
					this.m_IsClient = true;
					if (LogFilter.logDebug)
					{
						Debug.Log("StartAsClient Discovery listening");
					}
					result = true;
				}
			}
			return result;
		}

		// Token: 0x06000237 RID: 567 RVA: 0x0000B6B8 File Offset: 0x000098B8
		public bool StartAsServer()
		{
			bool result;
			if (this.m_HostId != -1 || this.m_Running)
			{
				if (LogFilter.logWarn)
				{
					Debug.LogWarning("NetworkDiscovery StartAsServer already started");
				}
				result = false;
			}
			else
			{
				this.m_HostId = NetworkManager.activeTransport.AddHost(this.m_DefaultTopology, 0, null);
				byte b;
				if (this.m_HostId == -1)
				{
					if (LogFilter.logError)
					{
						Debug.LogError("NetworkDiscovery StartAsServer - addHost failed");
					}
					result = false;
				}
				else if (!NetworkManager.activeTransport.StartBroadcastDiscovery(this.m_HostId, this.m_BroadcastPort, this.m_BroadcastKey, this.m_BroadcastVersion, this.m_BroadcastSubVersion, this.m_MsgOutBuffer, this.m_MsgOutBuffer.Length, this.m_BroadcastInterval, out b))
				{
					NetworkTransport.RemoveHost(this.m_HostId);
					this.m_HostId = -1;
					if (LogFilter.logError)
					{
						Debug.LogError("NetworkDiscovery StartBroadcast failed err: " + b);
					}
					result = false;
				}
				else
				{
					this.m_Running = true;
					this.m_IsServer = true;
					if (LogFilter.logDebug)
					{
						Debug.Log("StartAsServer Discovery broadcasting");
					}
					Object.DontDestroyOnLoad(base.gameObject);
					result = true;
				}
			}
			return result;
		}

		// Token: 0x06000238 RID: 568 RVA: 0x0000B7F4 File Offset: 0x000099F4
		public void StopBroadcast()
		{
			if (this.m_HostId == -1)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkDiscovery StopBroadcast not initialized");
				}
			}
			else if (!this.m_Running)
			{
				Debug.LogWarning("NetworkDiscovery StopBroadcast not started");
			}
			else
			{
				if (this.m_IsServer)
				{
					NetworkManager.activeTransport.StopBroadcastDiscovery();
				}
				NetworkManager.activeTransport.RemoveHost(this.m_HostId);
				this.m_HostId = -1;
				this.m_Running = false;
				this.m_IsServer = false;
				this.m_IsClient = false;
				this.m_MsgInBuffer = null;
				this.m_BroadcastsReceived = null;
				if (LogFilter.logDebug)
				{
					Debug.Log("Stopped Discovery broadcasting");
				}
			}
		}

		// Token: 0x06000239 RID: 569 RVA: 0x0000B8B0 File Offset: 0x00009AB0
		private void Update()
		{
			if (this.m_HostId != -1)
			{
				if (!this.m_IsServer)
				{
					NetworkEventType networkEventType;
					do
					{
						int num;
						int num2;
						int num3;
						byte b;
						networkEventType = NetworkManager.activeTransport.ReceiveFromHost(this.m_HostId, out num, out num2, this.m_MsgInBuffer, 1024, out num3, out b);
						if (networkEventType == NetworkEventType.BroadcastEvent)
						{
							NetworkManager.activeTransport.GetBroadcastConnectionMessage(this.m_HostId, this.m_MsgInBuffer, 1024, out num3, out b);
							string text;
							int num4;
							NetworkManager.activeTransport.GetBroadcastConnectionInfo(this.m_HostId, out text, out num4, out b);
							NetworkBroadcastResult value = default(NetworkBroadcastResult);
							value.serverAddress = text;
							value.broadcastData = new byte[num3];
							Buffer.BlockCopy(this.m_MsgInBuffer, 0, value.broadcastData, 0, num3);
							this.m_BroadcastsReceived[text] = value;
							this.OnReceivedBroadcast(text, NetworkDiscovery.BytesToString(this.m_MsgInBuffer));
						}
					}
					while (networkEventType != NetworkEventType.Nothing);
				}
			}
		}

		// Token: 0x0600023A RID: 570 RVA: 0x0000B9A0 File Offset: 0x00009BA0
		private void OnDestroy()
		{
			if (this.m_IsServer && this.m_Running && this.m_HostId != -1)
			{
				NetworkManager.activeTransport.StopBroadcastDiscovery();
				NetworkManager.activeTransport.RemoveHost(this.m_HostId);
			}
			if (this.m_IsClient && this.m_Running && this.m_HostId != -1)
			{
				NetworkManager.activeTransport.RemoveHost(this.m_HostId);
			}
		}

		// Token: 0x0600023B RID: 571 RVA: 0x0000BA22 File Offset: 0x00009C22
		public virtual void OnReceivedBroadcast(string fromAddress, string data)
		{
		}

		// Token: 0x0600023C RID: 572 RVA: 0x0000BA28 File Offset: 0x00009C28
		private void OnGUI()
		{
			if (this.m_ShowGUI)
			{
				int num = 10 + this.m_OffsetX;
				int num2 = 40 + this.m_OffsetY;
				if (Application.platform == RuntimePlatform.WebGLPlayer)
				{
					GUI.Box(new Rect((float)num, (float)num2, 200f, 20f), "( WebGL cannot broadcast )");
				}
				else if (this.m_MsgInBuffer == null)
				{
					if (GUI.Button(new Rect((float)num, (float)num2, 200f, 20f), "Initialize Broadcast"))
					{
						this.Initialize();
					}
				}
				else
				{
					string str = "";
					if (this.m_IsServer)
					{
						str = " (server)";
					}
					if (this.m_IsClient)
					{
						str = " (client)";
					}
					GUI.Label(new Rect((float)num, (float)num2, 200f, 20f), "initialized" + str);
					num2 += 24;
					if (this.m_Running)
					{
						if (GUI.Button(new Rect((float)num, (float)num2, 200f, 20f), "Stop"))
						{
							this.StopBroadcast();
						}
						num2 += 24;
						if (this.m_BroadcastsReceived != null)
						{
							foreach (string text in this.m_BroadcastsReceived.Keys)
							{
								NetworkBroadcastResult networkBroadcastResult = this.m_BroadcastsReceived[text];
								if (GUI.Button(new Rect((float)num, (float)(num2 + 20), 200f, 20f), "Game at " + text) && this.m_UseNetworkManager)
								{
									string text2 = NetworkDiscovery.BytesToString(networkBroadcastResult.broadcastData);
									string[] array = text2.Split(new char[]
									{
										':'
									});
									if (array.Length == 3 && array[0] == "NetworkManager")
									{
										if (NetworkManager.singleton != null && NetworkManager.singleton.client == null)
										{
											NetworkManager.singleton.networkAddress = array[1];
											NetworkManager.singleton.networkPort = Convert.ToInt32(array[2]);
											NetworkManager.singleton.StartClient();
										}
									}
								}
								num2 += 24;
							}
						}
					}
					else
					{
						if (GUI.Button(new Rect((float)num, (float)num2, 200f, 20f), "Start Broadcasting"))
						{
							this.StartAsServer();
						}
						num2 += 24;
						if (GUI.Button(new Rect((float)num, (float)num2, 200f, 20f), "Listen for Broadcast"))
						{
							this.StartAsClient();
						}
						num2 += 24;
					}
				}
			}
		}

		// Token: 0x0400010A RID: 266
		private const int k_MaxBroadcastMsgSize = 1024;

		// Token: 0x0400010B RID: 267
		[SerializeField]
		private int m_BroadcastPort = 47777;

		// Token: 0x0400010C RID: 268
		[SerializeField]
		private int m_BroadcastKey = 2222;

		// Token: 0x0400010D RID: 269
		[SerializeField]
		private int m_BroadcastVersion = 1;

		// Token: 0x0400010E RID: 270
		[SerializeField]
		private int m_BroadcastSubVersion = 1;

		// Token: 0x0400010F RID: 271
		[SerializeField]
		private int m_BroadcastInterval = 1000;

		// Token: 0x04000110 RID: 272
		[SerializeField]
		private bool m_UseNetworkManager = false;

		// Token: 0x04000111 RID: 273
		[SerializeField]
		private string m_BroadcastData = "HELLO";

		// Token: 0x04000112 RID: 274
		[SerializeField]
		private bool m_ShowGUI = true;

		// Token: 0x04000113 RID: 275
		[SerializeField]
		private int m_OffsetX;

		// Token: 0x04000114 RID: 276
		[SerializeField]
		private int m_OffsetY;

		// Token: 0x04000115 RID: 277
		private int m_HostId = -1;

		// Token: 0x04000116 RID: 278
		private bool m_Running;

		// Token: 0x04000117 RID: 279
		private bool m_IsServer;

		// Token: 0x04000118 RID: 280
		private bool m_IsClient;

		// Token: 0x04000119 RID: 281
		private byte[] m_MsgOutBuffer;

		// Token: 0x0400011A RID: 282
		private byte[] m_MsgInBuffer;

		// Token: 0x0400011B RID: 283
		private HostTopology m_DefaultTopology;

		// Token: 0x0400011C RID: 284
		private Dictionary<string, NetworkBroadcastResult> m_BroadcastsReceived;
	}
}
