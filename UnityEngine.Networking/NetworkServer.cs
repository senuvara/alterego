﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking
{
	// Token: 0x0200005A RID: 90
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public sealed class NetworkServer
	{
		// Token: 0x060003FC RID: 1020 RVA: 0x00015B58 File Offset: 0x00013D58
		private NetworkServer()
		{
			NetworkManager.activeTransport.Init();
			if (LogFilter.logDev)
			{
				Debug.Log("NetworkServer Created version " + Version.Current);
			}
			this.m_RemoveList = new HashSet<NetworkInstanceId>();
			this.m_ExternalConnections = new HashSet<int>();
			this.m_NetworkScene = new NetworkScene();
			this.m_SimpleServerSimple = new NetworkServer.ServerSimpleWrapper(this);
		}

		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x060003FD RID: 1021 RVA: 0x00015BE4 File Offset: 0x00013DE4
		public static List<NetworkConnection> localConnections
		{
			get
			{
				return NetworkServer.instance.m_LocalConnectionsFakeList;
			}
		}

		// Token: 0x170000A4 RID: 164
		// (get) Token: 0x060003FE RID: 1022 RVA: 0x00015C04 File Offset: 0x00013E04
		public static int listenPort
		{
			get
			{
				return NetworkServer.instance.m_SimpleServerSimple.listenPort;
			}
		}

		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x060003FF RID: 1023 RVA: 0x00015C28 File Offset: 0x00013E28
		public static int serverHostId
		{
			get
			{
				return NetworkServer.instance.m_SimpleServerSimple.serverHostId;
			}
		}

		// Token: 0x170000A6 RID: 166
		// (get) Token: 0x06000400 RID: 1024 RVA: 0x00015C4C File Offset: 0x00013E4C
		public static ReadOnlyCollection<NetworkConnection> connections
		{
			get
			{
				return NetworkServer.instance.m_SimpleServerSimple.connections;
			}
		}

		// Token: 0x170000A7 RID: 167
		// (get) Token: 0x06000401 RID: 1025 RVA: 0x00015C70 File Offset: 0x00013E70
		public static Dictionary<short, NetworkMessageDelegate> handlers
		{
			get
			{
				return NetworkServer.instance.m_SimpleServerSimple.handlers;
			}
		}

		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x06000402 RID: 1026 RVA: 0x00015C94 File Offset: 0x00013E94
		public static HostTopology hostTopology
		{
			get
			{
				return NetworkServer.instance.m_SimpleServerSimple.hostTopology;
			}
		}

		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x06000403 RID: 1027 RVA: 0x00015CB8 File Offset: 0x00013EB8
		public static Dictionary<NetworkInstanceId, NetworkIdentity> objects
		{
			get
			{
				return NetworkServer.instance.m_NetworkScene.localObjects;
			}
		}

		// Token: 0x170000AA RID: 170
		// (get) Token: 0x06000404 RID: 1028 RVA: 0x00015CDC File Offset: 0x00013EDC
		// (set) Token: 0x06000405 RID: 1029 RVA: 0x00015CF2 File Offset: 0x00013EF2
		[Obsolete("Moved to NetworkMigrationManager")]
		public static bool sendPeerInfo
		{
			get
			{
				return false;
			}
			set
			{
			}
		}

		// Token: 0x170000AB RID: 171
		// (get) Token: 0x06000406 RID: 1030 RVA: 0x00015CF8 File Offset: 0x00013EF8
		// (set) Token: 0x06000407 RID: 1031 RVA: 0x00015D12 File Offset: 0x00013F12
		public static bool dontListen
		{
			get
			{
				return NetworkServer.m_DontListen;
			}
			set
			{
				NetworkServer.m_DontListen = value;
			}
		}

		// Token: 0x170000AC RID: 172
		// (get) Token: 0x06000408 RID: 1032 RVA: 0x00015D1C File Offset: 0x00013F1C
		// (set) Token: 0x06000409 RID: 1033 RVA: 0x00015D40 File Offset: 0x00013F40
		public static bool useWebSockets
		{
			get
			{
				return NetworkServer.instance.m_SimpleServerSimple.useWebSockets;
			}
			set
			{
				NetworkServer.instance.m_SimpleServerSimple.useWebSockets = value;
			}
		}

		// Token: 0x170000AD RID: 173
		// (get) Token: 0x0600040A RID: 1034 RVA: 0x00015D54 File Offset: 0x00013F54
		internal static NetworkServer instance
		{
			get
			{
				if (NetworkServer.s_Instance == null)
				{
					object obj = NetworkServer.s_Sync;
					lock (obj)
					{
						if (NetworkServer.s_Instance == null)
						{
							NetworkServer.s_Instance = new NetworkServer();
						}
					}
				}
				return NetworkServer.s_Instance;
			}
		}

		// Token: 0x170000AE RID: 174
		// (get) Token: 0x0600040B RID: 1035 RVA: 0x00015DC4 File Offset: 0x00013FC4
		public static bool active
		{
			get
			{
				return NetworkServer.s_Active;
			}
		}

		// Token: 0x170000AF RID: 175
		// (get) Token: 0x0600040C RID: 1036 RVA: 0x00015DE0 File Offset: 0x00013FE0
		public static bool localClientActive
		{
			get
			{
				return NetworkServer.instance.m_LocalClientActive;
			}
		}

		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x0600040D RID: 1037 RVA: 0x00015E00 File Offset: 0x00014000
		public static int numChannels
		{
			get
			{
				return NetworkServer.instance.m_SimpleServerSimple.hostTopology.DefaultConfig.ChannelCount;
			}
		}

		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x0600040E RID: 1038 RVA: 0x00015E30 File Offset: 0x00014030
		// (set) Token: 0x0600040F RID: 1039 RVA: 0x00015E4F File Offset: 0x0001404F
		public static float maxDelay
		{
			get
			{
				return NetworkServer.instance.m_MaxDelay;
			}
			set
			{
				NetworkServer.instance.InternalSetMaxDelay(value);
			}
		}

		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x06000410 RID: 1040 RVA: 0x00015E60 File Offset: 0x00014060
		public static Type networkConnectionClass
		{
			get
			{
				return NetworkServer.instance.m_SimpleServerSimple.networkConnectionClass;
			}
		}

		// Token: 0x06000411 RID: 1041 RVA: 0x00015E84 File Offset: 0x00014084
		public static void SetNetworkConnectionClass<T>() where T : NetworkConnection
		{
			NetworkServer.instance.m_SimpleServerSimple.SetNetworkConnectionClass<T>();
		}

		// Token: 0x06000412 RID: 1042 RVA: 0x00015E98 File Offset: 0x00014098
		public static bool Configure(ConnectionConfig config, int maxConnections)
		{
			return NetworkServer.instance.m_SimpleServerSimple.Configure(config, maxConnections);
		}

		// Token: 0x06000413 RID: 1043 RVA: 0x00015EC0 File Offset: 0x000140C0
		public static bool Configure(HostTopology topology)
		{
			return NetworkServer.instance.m_SimpleServerSimple.Configure(topology);
		}

		// Token: 0x06000414 RID: 1044 RVA: 0x00015EE5 File Offset: 0x000140E5
		public static void Reset()
		{
			NetworkManager.activeTransport.Shutdown();
			NetworkManager.activeTransport.Init();
			NetworkServer.s_Instance = null;
			NetworkServer.s_Active = false;
		}

		// Token: 0x06000415 RID: 1045 RVA: 0x00015F0C File Offset: 0x0001410C
		public static void Shutdown()
		{
			if (NetworkServer.s_Instance != null)
			{
				NetworkServer.s_Instance.InternalDisconnectAll();
				if (!NetworkServer.m_DontListen)
				{
					NetworkServer.s_Instance.m_SimpleServerSimple.Stop();
				}
				NetworkServer.s_Instance = null;
			}
			NetworkServer.m_DontListen = false;
			NetworkServer.s_Active = false;
		}

		// Token: 0x06000416 RID: 1046 RVA: 0x00015F6C File Offset: 0x0001416C
		public static bool Listen(MatchInfo matchInfo, int listenPort)
		{
			bool result;
			if (!matchInfo.usingRelay)
			{
				result = NetworkServer.instance.InternalListen(null, listenPort);
			}
			else
			{
				NetworkServer.instance.InternalListenRelay(matchInfo.address, matchInfo.port, matchInfo.networkId, Utility.GetSourceID(), matchInfo.nodeId);
				result = true;
			}
			return result;
		}

		// Token: 0x06000417 RID: 1047 RVA: 0x00015FC8 File Offset: 0x000141C8
		internal void RegisterMessageHandlers()
		{
			NetworkServerSimple simpleServerSimple = this.m_SimpleServerSimple;
			short msgType = 35;
			if (NetworkServer.<>f__mg$cache0 == null)
			{
				NetworkServer.<>f__mg$cache0 = new NetworkMessageDelegate(NetworkServer.OnClientReadyMessage);
			}
			simpleServerSimple.RegisterHandlerSafe(msgType, NetworkServer.<>f__mg$cache0);
			NetworkServerSimple simpleServerSimple2 = this.m_SimpleServerSimple;
			short msgType2 = 5;
			if (NetworkServer.<>f__mg$cache1 == null)
			{
				NetworkServer.<>f__mg$cache1 = new NetworkMessageDelegate(NetworkServer.OnCommandMessage);
			}
			simpleServerSimple2.RegisterHandlerSafe(msgType2, NetworkServer.<>f__mg$cache1);
			NetworkServerSimple simpleServerSimple3 = this.m_SimpleServerSimple;
			short msgType3 = 6;
			if (NetworkServer.<>f__mg$cache2 == null)
			{
				NetworkServer.<>f__mg$cache2 = new NetworkMessageDelegate(NetworkTransform.HandleTransform);
			}
			simpleServerSimple3.RegisterHandlerSafe(msgType3, NetworkServer.<>f__mg$cache2);
			NetworkServerSimple simpleServerSimple4 = this.m_SimpleServerSimple;
			short msgType4 = 16;
			if (NetworkServer.<>f__mg$cache3 == null)
			{
				NetworkServer.<>f__mg$cache3 = new NetworkMessageDelegate(NetworkTransformChild.HandleChildTransform);
			}
			simpleServerSimple4.RegisterHandlerSafe(msgType4, NetworkServer.<>f__mg$cache3);
			NetworkServerSimple simpleServerSimple5 = this.m_SimpleServerSimple;
			short msgType5 = 38;
			if (NetworkServer.<>f__mg$cache4 == null)
			{
				NetworkServer.<>f__mg$cache4 = new NetworkMessageDelegate(NetworkServer.OnRemovePlayerMessage);
			}
			simpleServerSimple5.RegisterHandlerSafe(msgType5, NetworkServer.<>f__mg$cache4);
			NetworkServerSimple simpleServerSimple6 = this.m_SimpleServerSimple;
			short msgType6 = 40;
			if (NetworkServer.<>f__mg$cache5 == null)
			{
				NetworkServer.<>f__mg$cache5 = new NetworkMessageDelegate(NetworkAnimator.OnAnimationServerMessage);
			}
			simpleServerSimple6.RegisterHandlerSafe(msgType6, NetworkServer.<>f__mg$cache5);
			NetworkServerSimple simpleServerSimple7 = this.m_SimpleServerSimple;
			short msgType7 = 41;
			if (NetworkServer.<>f__mg$cache6 == null)
			{
				NetworkServer.<>f__mg$cache6 = new NetworkMessageDelegate(NetworkAnimator.OnAnimationParametersServerMessage);
			}
			simpleServerSimple7.RegisterHandlerSafe(msgType7, NetworkServer.<>f__mg$cache6);
			NetworkServerSimple simpleServerSimple8 = this.m_SimpleServerSimple;
			short msgType8 = 42;
			if (NetworkServer.<>f__mg$cache7 == null)
			{
				NetworkServer.<>f__mg$cache7 = new NetworkMessageDelegate(NetworkAnimator.OnAnimationTriggerServerMessage);
			}
			simpleServerSimple8.RegisterHandlerSafe(msgType8, NetworkServer.<>f__mg$cache7);
			NetworkServerSimple simpleServerSimple9 = this.m_SimpleServerSimple;
			short msgType9 = 17;
			if (NetworkServer.<>f__mg$cache8 == null)
			{
				NetworkServer.<>f__mg$cache8 = new NetworkMessageDelegate(NetworkConnection.OnFragment);
			}
			simpleServerSimple9.RegisterHandlerSafe(msgType9, NetworkServer.<>f__mg$cache8);
			NetworkServer.maxPacketSize = NetworkServer.hostTopology.DefaultConfig.PacketSize;
		}

		// Token: 0x06000418 RID: 1048 RVA: 0x00016162 File Offset: 0x00014362
		public static void ListenRelay(string relayIp, int relayPort, NetworkID netGuid, SourceID sourceId, NodeID nodeId)
		{
			NetworkServer.instance.InternalListenRelay(relayIp, relayPort, netGuid, sourceId, nodeId);
		}

		// Token: 0x06000419 RID: 1049 RVA: 0x00016175 File Offset: 0x00014375
		private void InternalListenRelay(string relayIp, int relayPort, NetworkID netGuid, SourceID sourceId, NodeID nodeId)
		{
			this.m_SimpleServerSimple.ListenRelay(relayIp, relayPort, netGuid, sourceId, nodeId);
			NetworkServer.s_Active = true;
			this.RegisterMessageHandlers();
		}

		// Token: 0x0600041A RID: 1050 RVA: 0x00016198 File Offset: 0x00014398
		public static bool Listen(int serverPort)
		{
			return NetworkServer.instance.InternalListen(null, serverPort);
		}

		// Token: 0x0600041B RID: 1051 RVA: 0x000161BC File Offset: 0x000143BC
		public static bool Listen(string ipAddress, int serverPort)
		{
			return NetworkServer.instance.InternalListen(ipAddress, serverPort);
		}

		// Token: 0x0600041C RID: 1052 RVA: 0x000161E0 File Offset: 0x000143E0
		internal bool InternalListen(string ipAddress, int serverPort)
		{
			if (NetworkServer.m_DontListen)
			{
				this.m_SimpleServerSimple.Initialize();
			}
			else if (!this.m_SimpleServerSimple.Listen(ipAddress, serverPort))
			{
				return false;
			}
			NetworkServer.maxPacketSize = NetworkServer.hostTopology.DefaultConfig.PacketSize;
			NetworkServer.s_Active = true;
			this.RegisterMessageHandlers();
			return true;
		}

		// Token: 0x0600041D RID: 1053 RVA: 0x00016250 File Offset: 0x00014450
		public static NetworkClient BecomeHost(NetworkClient oldClient, int port, MatchInfo matchInfo, int oldConnectionId, PeerInfoMessage[] peers)
		{
			return NetworkServer.instance.BecomeHostInternal(oldClient, port, matchInfo, oldConnectionId, peers);
		}

		// Token: 0x0600041E RID: 1054 RVA: 0x00016278 File Offset: 0x00014478
		internal NetworkClient BecomeHostInternal(NetworkClient oldClient, int port, MatchInfo matchInfo, int oldConnectionId, PeerInfoMessage[] peers)
		{
			NetworkClient result;
			if (NetworkServer.s_Active)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("BecomeHost already a server.");
				}
				result = null;
			}
			else if (!NetworkClient.active)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("BecomeHost NetworkClient not active.");
				}
				result = null;
			}
			else
			{
				NetworkServer.Configure(NetworkServer.hostTopology);
				if (matchInfo == null)
				{
					if (LogFilter.logDev)
					{
						Debug.Log("BecomeHost Listen on " + port);
					}
					if (!NetworkServer.Listen(port))
					{
						if (LogFilter.logError)
						{
							Debug.LogError("BecomeHost bind failed.");
						}
						return null;
					}
				}
				else
				{
					if (LogFilter.logDev)
					{
						Debug.Log("BecomeHost match:" + matchInfo.networkId);
					}
					NetworkServer.ListenRelay(matchInfo.address, matchInfo.port, matchInfo.networkId, Utility.GetSourceID(), matchInfo.nodeId);
				}
				foreach (NetworkIdentity networkIdentity in ClientScene.objects.Values)
				{
					if (!(networkIdentity == null) && !(networkIdentity.gameObject == null))
					{
						NetworkIdentity.AddNetworkId(networkIdentity.netId.Value);
						this.m_NetworkScene.SetLocalObject(networkIdentity.netId, networkIdentity.gameObject, false, false);
						networkIdentity.OnStartServer(true);
					}
				}
				if (LogFilter.logDev)
				{
					Debug.Log("NetworkServer BecomeHost done. oldConnectionId:" + oldConnectionId);
				}
				this.RegisterMessageHandlers();
				if (!NetworkClient.RemoveClient(oldClient))
				{
					if (LogFilter.logError)
					{
						Debug.LogError("BecomeHost failed to remove client");
					}
				}
				if (LogFilter.logDev)
				{
					Debug.Log("BecomeHost localClient ready");
				}
				NetworkClient networkClient = ClientScene.ReconnectLocalServer();
				ClientScene.Ready(networkClient.connection);
				ClientScene.SetReconnectId(oldConnectionId, peers);
				ClientScene.AddPlayer(ClientScene.readyConnection, 0);
				result = networkClient;
			}
			return result;
		}

		// Token: 0x0600041F RID: 1055 RVA: 0x000164BC File Offset: 0x000146BC
		private void InternalSetMaxDelay(float seconds)
		{
			for (int i = 0; i < NetworkServer.connections.Count; i++)
			{
				NetworkConnection networkConnection = NetworkServer.connections[i];
				if (networkConnection != null)
				{
					networkConnection.SetMaxDelay(seconds);
				}
			}
			this.m_MaxDelay = seconds;
		}

		// Token: 0x06000420 RID: 1056 RVA: 0x00016508 File Offset: 0x00014708
		internal int AddLocalClient(LocalClient localClient)
		{
			int result;
			if (this.m_LocalConnectionsFakeList.Count != 0)
			{
				Debug.LogError("Local Connection already exists");
				result = -1;
			}
			else
			{
				this.m_LocalConnection = new ULocalConnectionToClient(localClient);
				this.m_LocalConnection.connectionId = 0;
				this.m_SimpleServerSimple.SetConnectionAtIndex(this.m_LocalConnection);
				this.m_LocalConnectionsFakeList.Add(this.m_LocalConnection);
				this.m_LocalConnection.InvokeHandlerNoData(32);
				result = 0;
			}
			return result;
		}

		// Token: 0x06000421 RID: 1057 RVA: 0x0001658C File Offset: 0x0001478C
		internal void RemoveLocalClient(NetworkConnection localClientConnection)
		{
			for (int i = 0; i < this.m_LocalConnectionsFakeList.Count; i++)
			{
				if (this.m_LocalConnectionsFakeList[i].connectionId == localClientConnection.connectionId)
				{
					this.m_LocalConnectionsFakeList.RemoveAt(i);
					break;
				}
			}
			if (this.m_LocalConnection != null)
			{
				this.m_LocalConnection.Disconnect();
				this.m_LocalConnection.Dispose();
				this.m_LocalConnection = null;
			}
			this.m_LocalClientActive = false;
			this.m_SimpleServerSimple.RemoveConnectionAtIndex(0);
		}

		// Token: 0x06000422 RID: 1058 RVA: 0x00016624 File Offset: 0x00014824
		internal void SetLocalObjectOnServer(NetworkInstanceId netId, GameObject obj)
		{
			if (LogFilter.logDev)
			{
				Debug.Log(string.Concat(new object[]
				{
					"SetLocalObjectOnServer ",
					netId,
					" ",
					obj
				}));
			}
			this.m_NetworkScene.SetLocalObject(netId, obj, false, true);
		}

		// Token: 0x06000423 RID: 1059 RVA: 0x0001667C File Offset: 0x0001487C
		internal void ActivateLocalClientScene()
		{
			if (!this.m_LocalClientActive)
			{
				this.m_LocalClientActive = true;
				foreach (NetworkIdentity networkIdentity in NetworkServer.objects.Values)
				{
					if (!networkIdentity.isClient)
					{
						if (LogFilter.logDev)
						{
							Debug.Log(string.Concat(new object[]
							{
								"ActivateClientScene ",
								networkIdentity.netId,
								" ",
								networkIdentity.gameObject
							}));
						}
						ClientScene.SetLocalObject(networkIdentity.netId, networkIdentity.gameObject);
						networkIdentity.OnStartClient();
					}
				}
			}
		}

		// Token: 0x06000424 RID: 1060 RVA: 0x00016758 File Offset: 0x00014958
		public static bool SendToAll(short msgType, MessageBase msg)
		{
			if (LogFilter.logDev)
			{
				Debug.Log("Server.SendToAll msgType:" + msgType);
			}
			bool flag = true;
			for (int i = 0; i < NetworkServer.connections.Count; i++)
			{
				NetworkConnection networkConnection = NetworkServer.connections[i];
				if (networkConnection != null)
				{
					flag &= networkConnection.Send(msgType, msg);
				}
			}
			return flag;
		}

		// Token: 0x06000425 RID: 1061 RVA: 0x000167CC File Offset: 0x000149CC
		private static bool SendToObservers(GameObject contextObj, short msgType, MessageBase msg)
		{
			if (LogFilter.logDev)
			{
				Debug.Log("Server.SendToObservers id:" + msgType);
			}
			bool flag = true;
			NetworkIdentity component = contextObj.GetComponent<NetworkIdentity>();
			bool result;
			if (component == null || component.observers == null)
			{
				result = false;
			}
			else
			{
				int count = component.observers.Count;
				for (int i = 0; i < count; i++)
				{
					NetworkConnection networkConnection = component.observers[i];
					flag &= networkConnection.Send(msgType, msg);
				}
				result = flag;
			}
			return result;
		}

		// Token: 0x06000426 RID: 1062 RVA: 0x0001686C File Offset: 0x00014A6C
		public static bool SendToReady(GameObject contextObj, short msgType, MessageBase msg)
		{
			if (LogFilter.logDev)
			{
				Debug.Log("Server.SendToReady id:" + msgType);
			}
			bool result;
			if (contextObj == null)
			{
				for (int i = 0; i < NetworkServer.connections.Count; i++)
				{
					NetworkConnection networkConnection = NetworkServer.connections[i];
					if (networkConnection != null && networkConnection.isReady)
					{
						networkConnection.Send(msgType, msg);
					}
				}
				result = true;
			}
			else
			{
				bool flag = true;
				NetworkIdentity component = contextObj.GetComponent<NetworkIdentity>();
				if (component == null || component.observers == null)
				{
					result = false;
				}
				else
				{
					int count = component.observers.Count;
					for (int j = 0; j < count; j++)
					{
						NetworkConnection networkConnection2 = component.observers[j];
						if (networkConnection2.isReady)
						{
							flag &= networkConnection2.Send(msgType, msg);
						}
					}
					result = flag;
				}
			}
			return result;
		}

		// Token: 0x06000427 RID: 1063 RVA: 0x0001697C File Offset: 0x00014B7C
		public static void SendWriterToReady(GameObject contextObj, NetworkWriter writer, int channelId)
		{
			if (writer.AsArraySegment().Count > 32767)
			{
				throw new UnityException("NetworkWriter used buffer is too big!");
			}
			NetworkServer.SendBytesToReady(contextObj, writer.AsArraySegment().Array, writer.AsArraySegment().Count, channelId);
		}

		// Token: 0x06000428 RID: 1064 RVA: 0x000169D4 File Offset: 0x00014BD4
		public static void SendBytesToReady(GameObject contextObj, byte[] buffer, int numBytes, int channelId)
		{
			if (contextObj == null)
			{
				bool flag = true;
				for (int i = 0; i < NetworkServer.connections.Count; i++)
				{
					NetworkConnection networkConnection = NetworkServer.connections[i];
					if (networkConnection != null && networkConnection.isReady)
					{
						if (!networkConnection.SendBytes(buffer, numBytes, channelId))
						{
							flag = false;
						}
					}
				}
				if (!flag)
				{
					if (LogFilter.logWarn)
					{
						Debug.LogWarning("SendBytesToReady failed");
					}
				}
			}
			else
			{
				NetworkIdentity component = contextObj.GetComponent<NetworkIdentity>();
				try
				{
					bool flag2 = true;
					int count = component.observers.Count;
					for (int j = 0; j < count; j++)
					{
						NetworkConnection networkConnection2 = component.observers[j];
						if (networkConnection2.isReady)
						{
							if (!networkConnection2.SendBytes(buffer, numBytes, channelId))
							{
								flag2 = false;
							}
						}
					}
					if (!flag2)
					{
						if (LogFilter.logWarn)
						{
							Debug.LogWarning("SendBytesToReady failed for " + contextObj);
						}
					}
				}
				catch (NullReferenceException)
				{
					if (LogFilter.logWarn)
					{
						Debug.LogWarning("SendBytesToReady object " + contextObj + " has not been spawned");
					}
				}
			}
		}

		// Token: 0x06000429 RID: 1065 RVA: 0x00016B2C File Offset: 0x00014D2C
		public static void SendBytesToPlayer(GameObject player, byte[] buffer, int numBytes, int channelId)
		{
			for (int i = 0; i < NetworkServer.connections.Count; i++)
			{
				NetworkConnection networkConnection = NetworkServer.connections[i];
				if (networkConnection != null)
				{
					for (int j = 0; j < networkConnection.playerControllers.Count; j++)
					{
						if (networkConnection.playerControllers[j].IsValid && networkConnection.playerControllers[j].gameObject == player)
						{
							networkConnection.SendBytes(buffer, numBytes, channelId);
							break;
						}
					}
				}
			}
		}

		// Token: 0x0600042A RID: 1066 RVA: 0x00016BD0 File Offset: 0x00014DD0
		public static bool SendUnreliableToAll(short msgType, MessageBase msg)
		{
			if (LogFilter.logDev)
			{
				Debug.Log("Server.SendUnreliableToAll msgType:" + msgType);
			}
			bool flag = true;
			for (int i = 0; i < NetworkServer.connections.Count; i++)
			{
				NetworkConnection networkConnection = NetworkServer.connections[i];
				if (networkConnection != null)
				{
					flag &= networkConnection.SendUnreliable(msgType, msg);
				}
			}
			return flag;
		}

		// Token: 0x0600042B RID: 1067 RVA: 0x00016C44 File Offset: 0x00014E44
		public static bool SendUnreliableToReady(GameObject contextObj, short msgType, MessageBase msg)
		{
			if (LogFilter.logDev)
			{
				Debug.Log("Server.SendUnreliableToReady id:" + msgType);
			}
			bool result;
			if (contextObj == null)
			{
				for (int i = 0; i < NetworkServer.connections.Count; i++)
				{
					NetworkConnection networkConnection = NetworkServer.connections[i];
					if (networkConnection != null && networkConnection.isReady)
					{
						networkConnection.SendUnreliable(msgType, msg);
					}
				}
				result = true;
			}
			else
			{
				bool flag = true;
				NetworkIdentity component = contextObj.GetComponent<NetworkIdentity>();
				int count = component.observers.Count;
				for (int j = 0; j < count; j++)
				{
					NetworkConnection networkConnection2 = component.observers[j];
					if (networkConnection2.isReady)
					{
						flag &= networkConnection2.SendUnreliable(msgType, msg);
					}
				}
				result = flag;
			}
			return result;
		}

		// Token: 0x0600042C RID: 1068 RVA: 0x00016D34 File Offset: 0x00014F34
		public static bool SendByChannelToAll(short msgType, MessageBase msg, int channelId)
		{
			if (LogFilter.logDev)
			{
				Debug.Log("Server.SendByChannelToAll id:" + msgType);
			}
			bool flag = true;
			for (int i = 0; i < NetworkServer.connections.Count; i++)
			{
				NetworkConnection networkConnection = NetworkServer.connections[i];
				if (networkConnection != null)
				{
					flag &= networkConnection.SendByChannel(msgType, msg, channelId);
				}
			}
			return flag;
		}

		// Token: 0x0600042D RID: 1069 RVA: 0x00016DA8 File Offset: 0x00014FA8
		public static bool SendByChannelToReady(GameObject contextObj, short msgType, MessageBase msg, int channelId)
		{
			if (LogFilter.logDev)
			{
				Debug.Log("Server.SendByChannelToReady msgType:" + msgType);
			}
			bool result;
			if (contextObj == null)
			{
				for (int i = 0; i < NetworkServer.connections.Count; i++)
				{
					NetworkConnection networkConnection = NetworkServer.connections[i];
					if (networkConnection != null && networkConnection.isReady)
					{
						networkConnection.SendByChannel(msgType, msg, channelId);
					}
				}
				result = true;
			}
			else
			{
				bool flag = true;
				NetworkIdentity component = contextObj.GetComponent<NetworkIdentity>();
				int count = component.observers.Count;
				for (int j = 0; j < count; j++)
				{
					NetworkConnection networkConnection2 = component.observers[j];
					if (networkConnection2.isReady)
					{
						flag &= networkConnection2.SendByChannel(msgType, msg, channelId);
					}
				}
				result = flag;
			}
			return result;
		}

		// Token: 0x0600042E RID: 1070 RVA: 0x00016E98 File Offset: 0x00015098
		public static void DisconnectAll()
		{
			NetworkServer.instance.InternalDisconnectAll();
		}

		// Token: 0x0600042F RID: 1071 RVA: 0x00016EA5 File Offset: 0x000150A5
		internal void InternalDisconnectAll()
		{
			this.m_SimpleServerSimple.DisconnectAllConnections();
			if (this.m_LocalConnection != null)
			{
				this.m_LocalConnection.Disconnect();
				this.m_LocalConnection.Dispose();
				this.m_LocalConnection = null;
			}
			this.m_LocalClientActive = false;
		}

		// Token: 0x06000430 RID: 1072 RVA: 0x00016EE4 File Offset: 0x000150E4
		internal static void Update()
		{
			if (NetworkServer.s_Instance != null)
			{
				NetworkServer.s_Instance.InternalUpdate();
			}
		}

		// Token: 0x06000431 RID: 1073 RVA: 0x00016F00 File Offset: 0x00015100
		private void UpdateServerObjects()
		{
			foreach (NetworkIdentity networkIdentity in NetworkServer.objects.Values)
			{
				try
				{
					networkIdentity.UNetUpdate();
				}
				catch (NullReferenceException)
				{
				}
				catch (MissingReferenceException)
				{
				}
			}
			if (this.m_RemoveListCount++ % 100 == 0)
			{
				this.CheckForNullObjects();
			}
		}

		// Token: 0x06000432 RID: 1074 RVA: 0x00016FB4 File Offset: 0x000151B4
		private void CheckForNullObjects()
		{
			foreach (NetworkInstanceId networkInstanceId in NetworkServer.objects.Keys)
			{
				NetworkIdentity networkIdentity = NetworkServer.objects[networkInstanceId];
				if (networkIdentity == null || networkIdentity.gameObject == null)
				{
					this.m_RemoveList.Add(networkInstanceId);
				}
			}
			if (this.m_RemoveList.Count > 0)
			{
				foreach (NetworkInstanceId key in this.m_RemoveList)
				{
					NetworkServer.objects.Remove(key);
				}
				this.m_RemoveList.Clear();
			}
		}

		// Token: 0x06000433 RID: 1075 RVA: 0x000170BC File Offset: 0x000152BC
		internal void InternalUpdate()
		{
			this.m_SimpleServerSimple.Update();
			if (NetworkServer.m_DontListen)
			{
				this.m_SimpleServerSimple.UpdateConnections();
			}
			this.UpdateServerObjects();
		}

		// Token: 0x06000434 RID: 1076 RVA: 0x000170E8 File Offset: 0x000152E8
		private void OnConnected(NetworkConnection conn)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("Server accepted client:" + conn.connectionId);
			}
			conn.SetMaxDelay(this.m_MaxDelay);
			conn.InvokeHandlerNoData(32);
			NetworkServer.SendCrc(conn);
		}

		// Token: 0x06000435 RID: 1077 RVA: 0x00017138 File Offset: 0x00015338
		private void OnDisconnected(NetworkConnection conn)
		{
			conn.InvokeHandlerNoData(33);
			for (int i = 0; i < conn.playerControllers.Count; i++)
			{
				if (conn.playerControllers[i].gameObject != null)
				{
					if (LogFilter.logWarn)
					{
						Debug.LogWarning("Player not destroyed when connection disconnected.");
					}
				}
			}
			if (LogFilter.logDebug)
			{
				Debug.Log("Server lost client:" + conn.connectionId);
			}
			conn.RemoveObservers();
			conn.Dispose();
		}

		// Token: 0x06000436 RID: 1078 RVA: 0x000171D3 File Offset: 0x000153D3
		private void OnData(NetworkConnection conn, int receivedSize, int channelId)
		{
			conn.TransportReceive(this.m_SimpleServerSimple.messageBuffer, receivedSize, channelId);
		}

		// Token: 0x06000437 RID: 1079 RVA: 0x000171E9 File Offset: 0x000153E9
		private void GenerateConnectError(int error)
		{
			if (LogFilter.logError)
			{
				Debug.LogError("UNet Server Connect Error: " + error);
			}
			this.GenerateError(null, error);
		}

		// Token: 0x06000438 RID: 1080 RVA: 0x00017218 File Offset: 0x00015418
		private void GenerateDataError(NetworkConnection conn, int error)
		{
			if (LogFilter.logError)
			{
				Debug.LogError("UNet Server Data Error: " + (NetworkError)error);
			}
			this.GenerateError(conn, error);
		}

		// Token: 0x06000439 RID: 1081 RVA: 0x00017254 File Offset: 0x00015454
		private void GenerateDisconnectError(NetworkConnection conn, int error)
		{
			if (LogFilter.logError)
			{
				Debug.LogError(string.Concat(new object[]
				{
					"UNet Server Disconnect Error: ",
					(NetworkError)error,
					" conn:[",
					conn,
					"]:",
					conn.connectionId
				}));
			}
			this.GenerateError(conn, error);
		}

		// Token: 0x0600043A RID: 1082 RVA: 0x000172BC File Offset: 0x000154BC
		private void GenerateError(NetworkConnection conn, int error)
		{
			if (NetworkServer.handlers.ContainsKey(34))
			{
				ErrorMessage errorMessage = new ErrorMessage();
				errorMessage.errorCode = error;
				NetworkWriter writer = new NetworkWriter();
				errorMessage.Serialize(writer);
				NetworkReader reader = new NetworkReader(writer);
				conn.InvokeHandler(34, reader, 0);
			}
		}

		// Token: 0x0600043B RID: 1083 RVA: 0x00017309 File Offset: 0x00015509
		public static void RegisterHandler(short msgType, NetworkMessageDelegate handler)
		{
			NetworkServer.instance.m_SimpleServerSimple.RegisterHandler(msgType, handler);
		}

		// Token: 0x0600043C RID: 1084 RVA: 0x0001731D File Offset: 0x0001551D
		public static void UnregisterHandler(short msgType)
		{
			NetworkServer.instance.m_SimpleServerSimple.UnregisterHandler(msgType);
		}

		// Token: 0x0600043D RID: 1085 RVA: 0x00017330 File Offset: 0x00015530
		public static void ClearHandlers()
		{
			NetworkServer.instance.m_SimpleServerSimple.ClearHandlers();
		}

		// Token: 0x0600043E RID: 1086 RVA: 0x00017342 File Offset: 0x00015542
		public static void ClearSpawners()
		{
			NetworkScene.ClearSpawners();
		}

		// Token: 0x0600043F RID: 1087 RVA: 0x0001734C File Offset: 0x0001554C
		public static void GetStatsOut(out int numMsgs, out int numBufferedMsgs, out int numBytes, out int lastBufferedPerSecond)
		{
			numMsgs = 0;
			numBufferedMsgs = 0;
			numBytes = 0;
			lastBufferedPerSecond = 0;
			for (int i = 0; i < NetworkServer.connections.Count; i++)
			{
				NetworkConnection networkConnection = NetworkServer.connections[i];
				if (networkConnection != null)
				{
					int num;
					int num2;
					int num3;
					int num4;
					networkConnection.GetStatsOut(out num, out num2, out num3, out num4);
					numMsgs += num;
					numBufferedMsgs += num2;
					numBytes += num3;
					lastBufferedPerSecond += num4;
				}
			}
		}

		// Token: 0x06000440 RID: 1088 RVA: 0x000173C0 File Offset: 0x000155C0
		public static void GetStatsIn(out int numMsgs, out int numBytes)
		{
			numMsgs = 0;
			numBytes = 0;
			for (int i = 0; i < NetworkServer.connections.Count; i++)
			{
				NetworkConnection networkConnection = NetworkServer.connections[i];
				if (networkConnection != null)
				{
					int num;
					int num2;
					networkConnection.GetStatsIn(out num, out num2);
					numMsgs += num;
					numBytes += num2;
				}
			}
		}

		// Token: 0x06000441 RID: 1089 RVA: 0x0001741C File Offset: 0x0001561C
		public static void SendToClientOfPlayer(GameObject player, short msgType, MessageBase msg)
		{
			for (int i = 0; i < NetworkServer.connections.Count; i++)
			{
				NetworkConnection networkConnection = NetworkServer.connections[i];
				if (networkConnection != null)
				{
					for (int j = 0; j < networkConnection.playerControllers.Count; j++)
					{
						if (networkConnection.playerControllers[j].IsValid && networkConnection.playerControllers[j].gameObject == player)
						{
							networkConnection.Send(msgType, msg);
							return;
						}
					}
				}
			}
			if (LogFilter.logError)
			{
				Debug.LogError("Failed to send message to player object '" + player.name + ", not found in connection list");
				return;
			}
		}

		// Token: 0x06000442 RID: 1090 RVA: 0x000174E0 File Offset: 0x000156E0
		public static void SendToClient(int connectionId, short msgType, MessageBase msg)
		{
			if (connectionId < NetworkServer.connections.Count)
			{
				NetworkConnection networkConnection = NetworkServer.connections[connectionId];
				if (networkConnection != null)
				{
					networkConnection.Send(msgType, msg);
					return;
				}
			}
			if (LogFilter.logError)
			{
				Debug.LogError("Failed to send message to connection ID '" + connectionId + ", not found in connection list");
			}
		}

		// Token: 0x06000443 RID: 1091 RVA: 0x00017548 File Offset: 0x00015748
		public static bool ReplacePlayerForConnection(NetworkConnection conn, GameObject player, short playerControllerId, NetworkHash128 assetId)
		{
			NetworkIdentity networkIdentity;
			if (NetworkServer.GetNetworkIdentity(player, out networkIdentity))
			{
				networkIdentity.SetDynamicAssetId(assetId);
			}
			return NetworkServer.instance.InternalReplacePlayerForConnection(conn, player, playerControllerId);
		}

		// Token: 0x06000444 RID: 1092 RVA: 0x00017580 File Offset: 0x00015780
		public static bool ReplacePlayerForConnection(NetworkConnection conn, GameObject player, short playerControllerId)
		{
			return NetworkServer.instance.InternalReplacePlayerForConnection(conn, player, playerControllerId);
		}

		// Token: 0x06000445 RID: 1093 RVA: 0x000175A4 File Offset: 0x000157A4
		public static bool AddPlayerForConnection(NetworkConnection conn, GameObject player, short playerControllerId, NetworkHash128 assetId)
		{
			NetworkIdentity networkIdentity;
			if (NetworkServer.GetNetworkIdentity(player, out networkIdentity))
			{
				networkIdentity.SetDynamicAssetId(assetId);
			}
			return NetworkServer.instance.InternalAddPlayerForConnection(conn, player, playerControllerId);
		}

		// Token: 0x06000446 RID: 1094 RVA: 0x000175DC File Offset: 0x000157DC
		public static bool AddPlayerForConnection(NetworkConnection conn, GameObject player, short playerControllerId)
		{
			return NetworkServer.instance.InternalAddPlayerForConnection(conn, player, playerControllerId);
		}

		// Token: 0x06000447 RID: 1095 RVA: 0x00017600 File Offset: 0x00015800
		internal bool InternalAddPlayerForConnection(NetworkConnection conn, GameObject playerGameObject, short playerControllerId)
		{
			NetworkIdentity networkIdentity;
			bool result;
			if (!NetworkServer.GetNetworkIdentity(playerGameObject, out networkIdentity))
			{
				if (LogFilter.logError)
				{
					Debug.Log("AddPlayer: playerGameObject has no NetworkIdentity. Please add a NetworkIdentity to " + playerGameObject);
				}
				result = false;
			}
			else
			{
				networkIdentity.Reset();
				if (!NetworkServer.CheckPlayerControllerIdForConnection(conn, playerControllerId))
				{
					result = false;
				}
				else
				{
					PlayerController playerController = null;
					GameObject x = null;
					if (conn.GetPlayerController(playerControllerId, out playerController))
					{
						x = playerController.gameObject;
					}
					if (x != null)
					{
						if (LogFilter.logError)
						{
							Debug.Log("AddPlayer: player object already exists for playerControllerId of " + playerControllerId);
						}
						result = false;
					}
					else
					{
						PlayerController playerController2 = new PlayerController(playerGameObject, playerControllerId);
						conn.SetPlayerController(playerController2);
						networkIdentity.SetConnectionToClient(conn, playerController2.playerControllerId);
						NetworkServer.SetClientReady(conn);
						if (this.SetupLocalPlayerForConnection(conn, networkIdentity, playerController2))
						{
							result = true;
						}
						else
						{
							if (LogFilter.logDebug)
							{
								Debug.Log(string.Concat(new object[]
								{
									"Adding new playerGameObject object netId: ",
									playerGameObject.GetComponent<NetworkIdentity>().netId,
									" asset ID ",
									playerGameObject.GetComponent<NetworkIdentity>().assetId
								}));
							}
							NetworkServer.FinishPlayerForConnection(conn, networkIdentity, playerGameObject);
							if (networkIdentity.localPlayerAuthority)
							{
								networkIdentity.SetClientOwner(conn);
							}
							result = true;
						}
					}
				}
			}
			return result;
		}

		// Token: 0x06000448 RID: 1096 RVA: 0x0001775C File Offset: 0x0001595C
		private static bool CheckPlayerControllerIdForConnection(NetworkConnection conn, short playerControllerId)
		{
			bool result;
			if (playerControllerId < 0)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("AddPlayer: playerControllerId of " + playerControllerId + " is negative");
				}
				result = false;
			}
			else if (playerControllerId > 32)
			{
				if (LogFilter.logError)
				{
					Debug.Log(string.Concat(new object[]
					{
						"AddPlayer: playerControllerId of ",
						playerControllerId,
						" is too high. max is ",
						32
					}));
				}
				result = false;
			}
			else
			{
				if (playerControllerId > 16)
				{
					if (LogFilter.logWarn)
					{
						Debug.LogWarning("AddPlayer: playerControllerId of " + playerControllerId + " is unusually high");
					}
				}
				result = true;
			}
			return result;
		}

		// Token: 0x06000449 RID: 1097 RVA: 0x00017828 File Offset: 0x00015A28
		private bool SetupLocalPlayerForConnection(NetworkConnection conn, NetworkIdentity uv, PlayerController newPlayerController)
		{
			if (LogFilter.logDev)
			{
				Debug.Log("NetworkServer SetupLocalPlayerForConnection netID:" + uv.netId);
			}
			ULocalConnectionToClient ulocalConnectionToClient = conn as ULocalConnectionToClient;
			bool result;
			if (ulocalConnectionToClient != null)
			{
				if (LogFilter.logDev)
				{
					Debug.Log("NetworkServer AddPlayer handling ULocalConnectionToClient");
				}
				if (uv.netId.IsEmpty())
				{
					uv.OnStartServer(true);
				}
				uv.RebuildObservers(true);
				this.SendSpawnMessage(uv, null);
				ulocalConnectionToClient.localClient.AddLocalPlayer(newPlayerController);
				uv.SetClientOwner(conn);
				uv.ForceAuthority(true);
				uv.SetLocalPlayer(newPlayerController.playerControllerId);
				result = true;
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x0600044A RID: 1098 RVA: 0x000178E0 File Offset: 0x00015AE0
		private static void FinishPlayerForConnection(NetworkConnection conn, NetworkIdentity uv, GameObject playerGameObject)
		{
			if (uv.netId.IsEmpty())
			{
				NetworkServer.Spawn(playerGameObject);
			}
			conn.Send(4, new OwnerMessage
			{
				netId = uv.netId,
				playerControllerId = uv.playerControllerId
			});
		}

		// Token: 0x0600044B RID: 1099 RVA: 0x00017930 File Offset: 0x00015B30
		internal bool InternalReplacePlayerForConnection(NetworkConnection conn, GameObject playerGameObject, short playerControllerId)
		{
			NetworkIdentity networkIdentity;
			bool result;
			if (!NetworkServer.GetNetworkIdentity(playerGameObject, out networkIdentity))
			{
				if (LogFilter.logError)
				{
					Debug.LogError("ReplacePlayer: playerGameObject has no NetworkIdentity. Please add a NetworkIdentity to " + playerGameObject);
				}
				result = false;
			}
			else if (!NetworkServer.CheckPlayerControllerIdForConnection(conn, playerControllerId))
			{
				result = false;
			}
			else
			{
				if (LogFilter.logDev)
				{
					Debug.Log("NetworkServer ReplacePlayer");
				}
				PlayerController playerController;
				if (conn.GetPlayerController(playerControllerId, out playerController))
				{
					playerController.unetView.SetNotLocalPlayer();
					playerController.unetView.ClearClientOwner();
				}
				PlayerController playerController2 = new PlayerController(playerGameObject, playerControllerId);
				conn.SetPlayerController(playerController2);
				networkIdentity.SetConnectionToClient(conn, playerController2.playerControllerId);
				if (LogFilter.logDev)
				{
					Debug.Log("NetworkServer ReplacePlayer setup local");
				}
				if (this.SetupLocalPlayerForConnection(conn, networkIdentity, playerController2))
				{
					result = true;
				}
				else
				{
					if (LogFilter.logDebug)
					{
						Debug.Log(string.Concat(new object[]
						{
							"Replacing playerGameObject object netId: ",
							playerGameObject.GetComponent<NetworkIdentity>().netId,
							" asset ID ",
							playerGameObject.GetComponent<NetworkIdentity>().assetId
						}));
					}
					NetworkServer.FinishPlayerForConnection(conn, networkIdentity, playerGameObject);
					if (networkIdentity.localPlayerAuthority)
					{
						networkIdentity.SetClientOwner(conn);
					}
					result = true;
				}
			}
			return result;
		}

		// Token: 0x0600044C RID: 1100 RVA: 0x00017A7C File Offset: 0x00015C7C
		private static bool GetNetworkIdentity(GameObject go, out NetworkIdentity view)
		{
			view = go.GetComponent<NetworkIdentity>();
			bool result;
			if (view == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("UNET failure. GameObject doesn't have NetworkIdentity.");
				}
				result = false;
			}
			else
			{
				result = true;
			}
			return result;
		}

		// Token: 0x0600044D RID: 1101 RVA: 0x00017AC5 File Offset: 0x00015CC5
		public static void SetClientReady(NetworkConnection conn)
		{
			NetworkServer.instance.SetClientReadyInternal(conn);
		}

		// Token: 0x0600044E RID: 1102 RVA: 0x00017AD4 File Offset: 0x00015CD4
		internal void SetClientReadyInternal(NetworkConnection conn)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("SetClientReadyInternal for conn:" + conn.connectionId);
			}
			if (conn.isReady)
			{
				if (LogFilter.logDebug)
				{
					Debug.Log("SetClientReady conn " + conn.connectionId + " already ready");
				}
			}
			else
			{
				if (conn.playerControllers.Count == 0)
				{
					if (LogFilter.logDebug)
					{
						Debug.LogWarning("Ready with no player object");
					}
				}
				conn.isReady = true;
				ULocalConnectionToClient ulocalConnectionToClient = conn as ULocalConnectionToClient;
				if (ulocalConnectionToClient != null)
				{
					if (LogFilter.logDev)
					{
						Debug.Log("NetworkServer Ready handling ULocalConnectionToClient");
					}
					foreach (NetworkIdentity networkIdentity in NetworkServer.objects.Values)
					{
						if (networkIdentity != null && networkIdentity.gameObject != null)
						{
							bool flag = networkIdentity.OnCheckObserver(conn);
							if (flag)
							{
								networkIdentity.AddObserver(conn);
							}
							if (!networkIdentity.isClient)
							{
								if (LogFilter.logDev)
								{
									Debug.Log("LocalClient.SetSpawnObject calling OnStartClient");
								}
								networkIdentity.OnStartClient();
							}
						}
					}
				}
				else
				{
					if (LogFilter.logDebug)
					{
						Debug.Log(string.Concat(new object[]
						{
							"Spawning ",
							NetworkServer.objects.Count,
							" objects for conn ",
							conn.connectionId
						}));
					}
					ObjectSpawnFinishedMessage objectSpawnFinishedMessage = new ObjectSpawnFinishedMessage();
					objectSpawnFinishedMessage.state = 0U;
					conn.Send(12, objectSpawnFinishedMessage);
					foreach (NetworkIdentity networkIdentity2 in NetworkServer.objects.Values)
					{
						if (networkIdentity2 == null)
						{
							if (LogFilter.logWarn)
							{
								Debug.LogWarning("Invalid object found in server local object list (null NetworkIdentity).");
							}
						}
						else if (networkIdentity2.gameObject.activeSelf)
						{
							if (LogFilter.logDebug)
							{
								Debug.Log(string.Concat(new object[]
								{
									"Sending spawn message for current server objects name='",
									networkIdentity2.gameObject.name,
									"' netId=",
									networkIdentity2.netId
								}));
							}
							bool flag2 = networkIdentity2.OnCheckObserver(conn);
							if (flag2)
							{
								networkIdentity2.AddObserver(conn);
							}
						}
					}
					objectSpawnFinishedMessage.state = 1U;
					conn.Send(12, objectSpawnFinishedMessage);
				}
			}
		}

		// Token: 0x0600044F RID: 1103 RVA: 0x00017DB8 File Offset: 0x00015FB8
		internal static void ShowForConnection(NetworkIdentity uv, NetworkConnection conn)
		{
			if (conn.isReady)
			{
				NetworkServer.instance.SendSpawnMessage(uv, conn);
			}
		}

		// Token: 0x06000450 RID: 1104 RVA: 0x00017DD4 File Offset: 0x00015FD4
		internal static void HideForConnection(NetworkIdentity uv, NetworkConnection conn)
		{
			conn.Send(13, new ObjectDestroyMessage
			{
				netId = uv.netId
			});
		}

		// Token: 0x06000451 RID: 1105 RVA: 0x00017E00 File Offset: 0x00016000
		public static void SetAllClientsNotReady()
		{
			for (int i = 0; i < NetworkServer.connections.Count; i++)
			{
				NetworkConnection networkConnection = NetworkServer.connections[i];
				if (networkConnection != null)
				{
					NetworkServer.SetClientNotReady(networkConnection);
				}
			}
		}

		// Token: 0x06000452 RID: 1106 RVA: 0x00017E45 File Offset: 0x00016045
		public static void SetClientNotReady(NetworkConnection conn)
		{
			NetworkServer.instance.InternalSetClientNotReady(conn);
		}

		// Token: 0x06000453 RID: 1107 RVA: 0x00017E54 File Offset: 0x00016054
		internal void InternalSetClientNotReady(NetworkConnection conn)
		{
			if (conn.isReady)
			{
				if (LogFilter.logDebug)
				{
					Debug.Log("PlayerNotReady " + conn);
				}
				conn.isReady = false;
				conn.RemoveObservers();
				NotReadyMessage msg = new NotReadyMessage();
				conn.Send(36, msg);
			}
		}

		// Token: 0x06000454 RID: 1108 RVA: 0x00017EA8 File Offset: 0x000160A8
		private static void OnClientReadyMessage(NetworkMessage netMsg)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("Default handler for ready message from " + netMsg.conn);
			}
			NetworkServer.SetClientReady(netMsg.conn);
		}

		// Token: 0x06000455 RID: 1109 RVA: 0x00017ED8 File Offset: 0x000160D8
		private static void OnRemovePlayerMessage(NetworkMessage netMsg)
		{
			netMsg.ReadMessage<RemovePlayerMessage>(NetworkServer.s_RemovePlayerMessage);
			PlayerController playerController = null;
			netMsg.conn.GetPlayerController(NetworkServer.s_RemovePlayerMessage.playerControllerId, out playerController);
			if (playerController != null)
			{
				netMsg.conn.RemovePlayerController(NetworkServer.s_RemovePlayerMessage.playerControllerId);
				NetworkServer.Destroy(playerController.gameObject);
			}
			else if (LogFilter.logError)
			{
				Debug.LogError("Received remove player message but could not find the player ID: " + NetworkServer.s_RemovePlayerMessage.playerControllerId);
			}
		}

		// Token: 0x06000456 RID: 1110 RVA: 0x00017F64 File Offset: 0x00016164
		private static void OnCommandMessage(NetworkMessage netMsg)
		{
			int cmdHash = (int)netMsg.reader.ReadPackedUInt32();
			NetworkInstanceId networkInstanceId = netMsg.reader.ReadNetworkId();
			GameObject gameObject = NetworkServer.FindLocalObject(networkInstanceId);
			if (gameObject == null)
			{
				if (LogFilter.logWarn)
				{
					Debug.LogWarning("Instance not found when handling Command message [netId=" + networkInstanceId + "]");
				}
			}
			else
			{
				NetworkIdentity component = gameObject.GetComponent<NetworkIdentity>();
				if (component == null)
				{
					if (LogFilter.logWarn)
					{
						Debug.LogWarning("NetworkIdentity deleted when handling Command message [netId=" + networkInstanceId + "]");
					}
				}
				else
				{
					bool flag = false;
					for (int i = 0; i < netMsg.conn.playerControllers.Count; i++)
					{
						PlayerController playerController = netMsg.conn.playerControllers[i];
						if (playerController.gameObject != null && playerController.gameObject.GetComponent<NetworkIdentity>().netId == component.netId)
						{
							flag = true;
							break;
						}
					}
					if (!flag)
					{
						if (component.clientAuthorityOwner != netMsg.conn)
						{
							if (LogFilter.logWarn)
							{
								Debug.LogWarning("Command for object without authority [netId=" + networkInstanceId + "]");
							}
							return;
						}
					}
					if (LogFilter.logDev)
					{
						Debug.Log(string.Concat(new object[]
						{
							"OnCommandMessage for netId=",
							networkInstanceId,
							" conn=",
							netMsg.conn
						}));
					}
					component.HandleCommand(cmdHash, netMsg.reader);
				}
			}
		}

		// Token: 0x06000457 RID: 1111 RVA: 0x00018114 File Offset: 0x00016314
		internal void SpawnObject(GameObject obj)
		{
			NetworkIdentity networkIdentity;
			if (!NetworkServer.active)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("SpawnObject for " + obj + ", NetworkServer is not active. Cannot spawn objects without an active server.");
				}
			}
			else if (!NetworkServer.GetNetworkIdentity(obj, out networkIdentity))
			{
				if (LogFilter.logError)
				{
					Debug.LogError(string.Concat(new object[]
					{
						"SpawnObject ",
						obj,
						" has no NetworkIdentity. Please add a NetworkIdentity to ",
						obj
					}));
				}
			}
			else
			{
				networkIdentity.Reset();
				networkIdentity.OnStartServer(false);
				if (LogFilter.logDebug)
				{
					Debug.Log(string.Concat(new object[]
					{
						"SpawnObject instance ID ",
						networkIdentity.netId,
						" asset ID ",
						networkIdentity.assetId
					}));
				}
				networkIdentity.RebuildObservers(true);
			}
		}

		// Token: 0x06000458 RID: 1112 RVA: 0x000181F8 File Offset: 0x000163F8
		internal void SendSpawnMessage(NetworkIdentity uv, NetworkConnection conn)
		{
			if (!uv.serverOnly)
			{
				if (uv.sceneId.IsEmpty())
				{
					ObjectSpawnMessage objectSpawnMessage = new ObjectSpawnMessage();
					objectSpawnMessage.netId = uv.netId;
					objectSpawnMessage.assetId = uv.assetId;
					objectSpawnMessage.position = uv.transform.position;
					objectSpawnMessage.rotation = uv.transform.rotation;
					NetworkWriter networkWriter = new NetworkWriter();
					uv.UNetSerializeAllVars(networkWriter);
					if (networkWriter.Position > 0)
					{
						objectSpawnMessage.payload = networkWriter.ToArray();
					}
					if (conn != null)
					{
						conn.Send(3, objectSpawnMessage);
					}
					else
					{
						NetworkServer.SendToReady(uv.gameObject, 3, objectSpawnMessage);
					}
				}
				else
				{
					ObjectSpawnSceneMessage objectSpawnSceneMessage = new ObjectSpawnSceneMessage();
					objectSpawnSceneMessage.netId = uv.netId;
					objectSpawnSceneMessage.sceneId = uv.sceneId;
					objectSpawnSceneMessage.position = uv.transform.position;
					NetworkWriter networkWriter2 = new NetworkWriter();
					uv.UNetSerializeAllVars(networkWriter2);
					if (networkWriter2.Position > 0)
					{
						objectSpawnSceneMessage.payload = networkWriter2.ToArray();
					}
					if (conn != null)
					{
						conn.Send(10, objectSpawnSceneMessage);
					}
					else
					{
						NetworkServer.SendToReady(uv.gameObject, 3, objectSpawnSceneMessage);
					}
				}
			}
		}

		// Token: 0x06000459 RID: 1113 RVA: 0x00018340 File Offset: 0x00016540
		public static void DestroyPlayersForConnection(NetworkConnection conn)
		{
			if (conn.playerControllers.Count == 0)
			{
				if (LogFilter.logWarn)
				{
					Debug.LogWarning("Empty player list given to NetworkServer.Destroy(), nothing to do.");
				}
			}
			else
			{
				if (conn.clientOwnedObjects != null)
				{
					HashSet<NetworkInstanceId> hashSet = new HashSet<NetworkInstanceId>(conn.clientOwnedObjects);
					foreach (NetworkInstanceId netId in hashSet)
					{
						GameObject gameObject = NetworkServer.FindLocalObject(netId);
						if (gameObject != null)
						{
							NetworkServer.DestroyObject(gameObject);
						}
					}
				}
				for (int i = 0; i < conn.playerControllers.Count; i++)
				{
					PlayerController playerController = conn.playerControllers[i];
					if (playerController.IsValid)
					{
						if (!(playerController.unetView == null))
						{
							NetworkServer.DestroyObject(playerController.unetView, true);
						}
						playerController.gameObject = null;
					}
				}
				conn.playerControllers.Clear();
			}
		}

		// Token: 0x0600045A RID: 1114 RVA: 0x00018470 File Offset: 0x00016670
		private static void UnSpawnObject(GameObject obj)
		{
			NetworkIdentity uv;
			if (obj == null)
			{
				if (LogFilter.logDev)
				{
					Debug.Log("NetworkServer UnspawnObject is null");
				}
			}
			else if (NetworkServer.GetNetworkIdentity(obj, out uv))
			{
				NetworkServer.UnSpawnObject(uv);
			}
		}

		// Token: 0x0600045B RID: 1115 RVA: 0x000184BE File Offset: 0x000166BE
		private static void UnSpawnObject(NetworkIdentity uv)
		{
			NetworkServer.DestroyObject(uv, false);
		}

		// Token: 0x0600045C RID: 1116 RVA: 0x000184C8 File Offset: 0x000166C8
		private static void DestroyObject(GameObject obj)
		{
			NetworkIdentity uv;
			if (obj == null)
			{
				if (LogFilter.logDev)
				{
					Debug.Log("NetworkServer DestroyObject is null");
				}
			}
			else if (NetworkServer.GetNetworkIdentity(obj, out uv))
			{
				NetworkServer.DestroyObject(uv, true);
			}
		}

		// Token: 0x0600045D RID: 1117 RVA: 0x00018518 File Offset: 0x00016718
		private static void DestroyObject(NetworkIdentity uv, bool destroyServerObject)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("DestroyObject instance:" + uv.netId);
			}
			if (NetworkServer.objects.ContainsKey(uv.netId))
			{
				NetworkServer.objects.Remove(uv.netId);
			}
			if (uv.clientAuthorityOwner != null)
			{
				uv.clientAuthorityOwner.RemoveOwnedObject(uv);
			}
			ObjectDestroyMessage objectDestroyMessage = new ObjectDestroyMessage();
			objectDestroyMessage.netId = uv.netId;
			NetworkServer.SendToObservers(uv.gameObject, 1, objectDestroyMessage);
			uv.ClearObservers();
			if (NetworkClient.active && NetworkServer.instance.m_LocalClientActive)
			{
				uv.OnNetworkDestroy();
				ClientScene.SetLocalObject(objectDestroyMessage.netId, null);
			}
			if (destroyServerObject)
			{
				Object.Destroy(uv.gameObject);
			}
			uv.MarkForReset();
		}

		// Token: 0x0600045E RID: 1118 RVA: 0x000185F9 File Offset: 0x000167F9
		public static void ClearLocalObjects()
		{
			NetworkServer.objects.Clear();
		}

		// Token: 0x0600045F RID: 1119 RVA: 0x00018606 File Offset: 0x00016806
		public static void Spawn(GameObject obj)
		{
			if (NetworkServer.VerifyCanSpawn(obj))
			{
				NetworkServer.instance.SpawnObject(obj);
			}
		}

		// Token: 0x06000460 RID: 1120 RVA: 0x00018628 File Offset: 0x00016828
		private static bool CheckForPrefab(GameObject obj)
		{
			return false;
		}

		// Token: 0x06000461 RID: 1121 RVA: 0x00018640 File Offset: 0x00016840
		private static bool VerifyCanSpawn(GameObject obj)
		{
			bool result;
			if (NetworkServer.CheckForPrefab(obj))
			{
				Debug.LogErrorFormat("GameObject {0} is a prefab, it can't be spawned. This will cause errors in builds.", new object[]
				{
					obj.name
				});
				result = false;
			}
			else
			{
				result = true;
			}
			return result;
		}

		// Token: 0x06000462 RID: 1122 RVA: 0x00018684 File Offset: 0x00016884
		public static bool SpawnWithClientAuthority(GameObject obj, GameObject player)
		{
			NetworkIdentity component = player.GetComponent<NetworkIdentity>();
			bool result;
			if (component == null)
			{
				Debug.LogError("SpawnWithClientAuthority player object has no NetworkIdentity");
				result = false;
			}
			else if (component.connectionToClient == null)
			{
				Debug.LogError("SpawnWithClientAuthority player object is not a player.");
				result = false;
			}
			else
			{
				result = NetworkServer.SpawnWithClientAuthority(obj, component.connectionToClient);
			}
			return result;
		}

		// Token: 0x06000463 RID: 1123 RVA: 0x000186E8 File Offset: 0x000168E8
		public static bool SpawnWithClientAuthority(GameObject obj, NetworkConnection conn)
		{
			bool result;
			if (!conn.isReady)
			{
				Debug.LogError("SpawnWithClientAuthority NetworkConnection is not ready!");
				result = false;
			}
			else
			{
				NetworkServer.Spawn(obj);
				NetworkIdentity component = obj.GetComponent<NetworkIdentity>();
				result = (!(component == null) && component.isServer && component.AssignClientAuthority(conn));
			}
			return result;
		}

		// Token: 0x06000464 RID: 1124 RVA: 0x00018750 File Offset: 0x00016950
		public static bool SpawnWithClientAuthority(GameObject obj, NetworkHash128 assetId, NetworkConnection conn)
		{
			NetworkServer.Spawn(obj, assetId);
			NetworkIdentity component = obj.GetComponent<NetworkIdentity>();
			return !(component == null) && component.isServer && component.AssignClientAuthority(conn);
		}

		// Token: 0x06000465 RID: 1125 RVA: 0x0001879C File Offset: 0x0001699C
		public static void Spawn(GameObject obj, NetworkHash128 assetId)
		{
			if (NetworkServer.VerifyCanSpawn(obj))
			{
				NetworkIdentity networkIdentity;
				if (NetworkServer.GetNetworkIdentity(obj, out networkIdentity))
				{
					networkIdentity.SetDynamicAssetId(assetId);
				}
				NetworkServer.instance.SpawnObject(obj);
			}
		}

		// Token: 0x06000466 RID: 1126 RVA: 0x000187DC File Offset: 0x000169DC
		public static void Destroy(GameObject obj)
		{
			NetworkServer.DestroyObject(obj);
		}

		// Token: 0x06000467 RID: 1127 RVA: 0x000187E5 File Offset: 0x000169E5
		public static void UnSpawn(GameObject obj)
		{
			NetworkServer.UnSpawnObject(obj);
		}

		// Token: 0x06000468 RID: 1128 RVA: 0x000187F0 File Offset: 0x000169F0
		internal bool InvokeBytes(ULocalConnectionToServer conn, byte[] buffer, int numBytes, int channelId)
		{
			NetworkReader networkReader = new NetworkReader(buffer);
			networkReader.ReadInt16();
			short num = networkReader.ReadInt16();
			bool result;
			if (NetworkServer.handlers.ContainsKey(num) && this.m_LocalConnection != null)
			{
				this.m_LocalConnection.InvokeHandler(num, networkReader, channelId);
				result = true;
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x06000469 RID: 1129 RVA: 0x00018850 File Offset: 0x00016A50
		internal bool InvokeHandlerOnServer(ULocalConnectionToServer conn, short msgType, MessageBase msg, int channelId)
		{
			bool result;
			if (NetworkServer.handlers.ContainsKey(msgType) && this.m_LocalConnection != null)
			{
				NetworkWriter writer = new NetworkWriter();
				msg.Serialize(writer);
				NetworkReader reader = new NetworkReader(writer);
				this.m_LocalConnection.InvokeHandler(msgType, reader, channelId);
				result = true;
			}
			else
			{
				if (LogFilter.logError)
				{
					Debug.LogError(string.Concat(new object[]
					{
						"Local invoke: Failed to find local connection to invoke handler on [connectionId=",
						conn.connectionId,
						"] for MsgId:",
						msgType
					}));
				}
				result = false;
			}
			return result;
		}

		// Token: 0x0600046A RID: 1130 RVA: 0x000188F0 File Offset: 0x00016AF0
		public static GameObject FindLocalObject(NetworkInstanceId netId)
		{
			return NetworkServer.instance.m_NetworkScene.FindLocalObject(netId);
		}

		// Token: 0x0600046B RID: 1131 RVA: 0x00018918 File Offset: 0x00016B18
		public static Dictionary<short, NetworkConnection.PacketStat> GetConnectionStats()
		{
			Dictionary<short, NetworkConnection.PacketStat> dictionary = new Dictionary<short, NetworkConnection.PacketStat>();
			for (int i = 0; i < NetworkServer.connections.Count; i++)
			{
				NetworkConnection networkConnection = NetworkServer.connections[i];
				if (networkConnection != null)
				{
					foreach (short key in networkConnection.packetStats.Keys)
					{
						if (dictionary.ContainsKey(key))
						{
							NetworkConnection.PacketStat packetStat = dictionary[key];
							packetStat.count += networkConnection.packetStats[key].count;
							packetStat.bytes += networkConnection.packetStats[key].bytes;
							dictionary[key] = packetStat;
						}
						else
						{
							dictionary[key] = new NetworkConnection.PacketStat(networkConnection.packetStats[key]);
						}
					}
				}
			}
			return dictionary;
		}

		// Token: 0x0600046C RID: 1132 RVA: 0x00018A38 File Offset: 0x00016C38
		public static void ResetConnectionStats()
		{
			for (int i = 0; i < NetworkServer.connections.Count; i++)
			{
				NetworkConnection networkConnection = NetworkServer.connections[i];
				if (networkConnection != null)
				{
					networkConnection.ResetStats();
				}
			}
		}

		// Token: 0x0600046D RID: 1133 RVA: 0x00018A80 File Offset: 0x00016C80
		public static bool AddExternalConnection(NetworkConnection conn)
		{
			return NetworkServer.instance.AddExternalConnectionInternal(conn);
		}

		// Token: 0x0600046E RID: 1134 RVA: 0x00018AA0 File Offset: 0x00016CA0
		private bool AddExternalConnectionInternal(NetworkConnection conn)
		{
			bool result;
			if (conn.connectionId < 0)
			{
				result = false;
			}
			else if (conn.connectionId < NetworkServer.connections.Count && NetworkServer.connections[conn.connectionId] != null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("AddExternalConnection failed, already connection for id:" + conn.connectionId);
				}
				result = false;
			}
			else
			{
				if (LogFilter.logDebug)
				{
					Debug.Log("AddExternalConnection external connection " + conn.connectionId);
				}
				this.m_SimpleServerSimple.SetConnectionAtIndex(conn);
				this.m_ExternalConnections.Add(conn.connectionId);
				conn.InvokeHandlerNoData(32);
				result = true;
			}
			return result;
		}

		// Token: 0x0600046F RID: 1135 RVA: 0x00018B6F File Offset: 0x00016D6F
		public static void RemoveExternalConnection(int connectionId)
		{
			NetworkServer.instance.RemoveExternalConnectionInternal(connectionId);
		}

		// Token: 0x06000470 RID: 1136 RVA: 0x00018B80 File Offset: 0x00016D80
		private bool RemoveExternalConnectionInternal(int connectionId)
		{
			bool result;
			if (!this.m_ExternalConnections.Contains(connectionId))
			{
				if (LogFilter.logError)
				{
					Debug.LogError("RemoveExternalConnection failed, no connection for id:" + connectionId);
				}
				result = false;
			}
			else
			{
				if (LogFilter.logDebug)
				{
					Debug.Log("RemoveExternalConnection external connection " + connectionId);
				}
				NetworkConnection networkConnection = this.m_SimpleServerSimple.FindConnection(connectionId);
				if (networkConnection != null)
				{
					networkConnection.RemoveObservers();
				}
				this.m_SimpleServerSimple.RemoveConnectionAtIndex(connectionId);
				result = true;
			}
			return result;
		}

		// Token: 0x06000471 RID: 1137 RVA: 0x00018C1C File Offset: 0x00016E1C
		private static bool ValidateSceneObject(NetworkIdentity netId)
		{
			return netId.gameObject.hideFlags != HideFlags.NotEditable && netId.gameObject.hideFlags != HideFlags.HideAndDontSave && !netId.sceneId.IsEmpty();
		}

		// Token: 0x06000472 RID: 1138 RVA: 0x00018C78 File Offset: 0x00016E78
		public static bool SpawnObjects()
		{
			bool result;
			if (!NetworkServer.active)
			{
				result = true;
			}
			else
			{
				foreach (NetworkIdentity networkIdentity in Resources.FindObjectsOfTypeAll<NetworkIdentity>())
				{
					if (NetworkServer.ValidateSceneObject(networkIdentity))
					{
						if (LogFilter.logDebug)
						{
							Debug.Log(string.Concat(new object[]
							{
								"SpawnObjects sceneId:",
								networkIdentity.sceneId,
								" name:",
								networkIdentity.gameObject.name
							}));
						}
						networkIdentity.Reset();
						networkIdentity.gameObject.SetActive(true);
					}
				}
				NetworkIdentity[] array;
				foreach (NetworkIdentity networkIdentity2 in array)
				{
					if (NetworkServer.ValidateSceneObject(networkIdentity2))
					{
						NetworkServer.Spawn(networkIdentity2.gameObject);
						networkIdentity2.ForceAuthority(true);
					}
				}
				result = true;
			}
			return result;
		}

		// Token: 0x06000473 RID: 1139 RVA: 0x00018D70 File Offset: 0x00016F70
		private static void SendCrc(NetworkConnection targetConnection)
		{
			if (NetworkCRC.singleton != null)
			{
				if (NetworkCRC.scriptCRCCheck)
				{
					CRCMessage crcmessage = new CRCMessage();
					List<CRCMessageEntry> list = new List<CRCMessageEntry>();
					foreach (string text in NetworkCRC.singleton.scripts.Keys)
					{
						list.Add(new CRCMessageEntry
						{
							name = text,
							channel = (byte)NetworkCRC.singleton.scripts[text]
						});
					}
					crcmessage.scripts = list.ToArray();
					targetConnection.Send(14, crcmessage);
				}
			}
		}

		// Token: 0x06000474 RID: 1140 RVA: 0x00018E44 File Offset: 0x00017044
		[Obsolete("moved to NetworkMigrationManager")]
		public void SendNetworkInfo(NetworkConnection targetConnection)
		{
		}

		// Token: 0x06000475 RID: 1141 RVA: 0x00018E47 File Offset: 0x00017047
		// Note: this type is marked as 'beforefieldinit'.
		static NetworkServer()
		{
		}

		// Token: 0x040001B9 RID: 441
		private static bool s_Active;

		// Token: 0x040001BA RID: 442
		private static volatile NetworkServer s_Instance;

		// Token: 0x040001BB RID: 443
		private static object s_Sync = new Object();

		// Token: 0x040001BC RID: 444
		private static bool m_DontListen;

		// Token: 0x040001BD RID: 445
		private bool m_LocalClientActive;

		// Token: 0x040001BE RID: 446
		private List<NetworkConnection> m_LocalConnectionsFakeList = new List<NetworkConnection>();

		// Token: 0x040001BF RID: 447
		private ULocalConnectionToClient m_LocalConnection = null;

		// Token: 0x040001C0 RID: 448
		private NetworkScene m_NetworkScene;

		// Token: 0x040001C1 RID: 449
		private HashSet<int> m_ExternalConnections;

		// Token: 0x040001C2 RID: 450
		private NetworkServer.ServerSimpleWrapper m_SimpleServerSimple;

		// Token: 0x040001C3 RID: 451
		private float m_MaxDelay = 0.1f;

		// Token: 0x040001C4 RID: 452
		private HashSet<NetworkInstanceId> m_RemoveList;

		// Token: 0x040001C5 RID: 453
		private int m_RemoveListCount;

		// Token: 0x040001C6 RID: 454
		private const int k_RemoveListInterval = 100;

		// Token: 0x040001C7 RID: 455
		internal static ushort maxPacketSize;

		// Token: 0x040001C8 RID: 456
		private static RemovePlayerMessage s_RemovePlayerMessage = new RemovePlayerMessage();

		// Token: 0x040001C9 RID: 457
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cache0;

		// Token: 0x040001CA RID: 458
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cache1;

		// Token: 0x040001CB RID: 459
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cache2;

		// Token: 0x040001CC RID: 460
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cache3;

		// Token: 0x040001CD RID: 461
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cache4;

		// Token: 0x040001CE RID: 462
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cache5;

		// Token: 0x040001CF RID: 463
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cache6;

		// Token: 0x040001D0 RID: 464
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cache7;

		// Token: 0x040001D1 RID: 465
		[CompilerGenerated]
		private static NetworkMessageDelegate <>f__mg$cache8;

		// Token: 0x0200005B RID: 91
		private class ServerSimpleWrapper : NetworkServerSimple
		{
			// Token: 0x06000476 RID: 1142 RVA: 0x00019A16 File Offset: 0x00017C16
			public ServerSimpleWrapper(NetworkServer server)
			{
				this.m_Server = server;
			}

			// Token: 0x06000477 RID: 1143 RVA: 0x00019A26 File Offset: 0x00017C26
			public override void OnConnectError(int connectionId, byte error)
			{
				this.m_Server.GenerateConnectError((int)error);
			}

			// Token: 0x06000478 RID: 1144 RVA: 0x00019A35 File Offset: 0x00017C35
			public override void OnDataError(NetworkConnection conn, byte error)
			{
				this.m_Server.GenerateDataError(conn, (int)error);
			}

			// Token: 0x06000479 RID: 1145 RVA: 0x00019A45 File Offset: 0x00017C45
			public override void OnDisconnectError(NetworkConnection conn, byte error)
			{
				this.m_Server.GenerateDisconnectError(conn, (int)error);
			}

			// Token: 0x0600047A RID: 1146 RVA: 0x00019A55 File Offset: 0x00017C55
			public override void OnConnected(NetworkConnection conn)
			{
				this.m_Server.OnConnected(conn);
			}

			// Token: 0x0600047B RID: 1147 RVA: 0x00019A64 File Offset: 0x00017C64
			public override void OnDisconnected(NetworkConnection conn)
			{
				this.m_Server.OnDisconnected(conn);
			}

			// Token: 0x0600047C RID: 1148 RVA: 0x00019A73 File Offset: 0x00017C73
			public override void OnData(NetworkConnection conn, int receivedSize, int channelId)
			{
				this.m_Server.OnData(conn, receivedSize, channelId);
			}

			// Token: 0x040001D2 RID: 466
			private NetworkServer m_Server;
		}
	}
}
