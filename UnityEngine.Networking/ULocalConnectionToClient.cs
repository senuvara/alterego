﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000016 RID: 22
	internal class ULocalConnectionToClient : NetworkConnection
	{
		// Token: 0x060000B4 RID: 180 RVA: 0x00007ECE File Offset: 0x000060CE
		public ULocalConnectionToClient(LocalClient localClient)
		{
			this.address = "localClient";
			this.m_LocalClient = localClient;
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x060000B5 RID: 181 RVA: 0x00007EEC File Offset: 0x000060EC
		public LocalClient localClient
		{
			get
			{
				return this.m_LocalClient;
			}
		}

		// Token: 0x060000B6 RID: 182 RVA: 0x00007F08 File Offset: 0x00006108
		public override bool Send(short msgType, MessageBase msg)
		{
			this.m_LocalClient.InvokeHandlerOnClient(msgType, msg, 0);
			return true;
		}

		// Token: 0x060000B7 RID: 183 RVA: 0x00007F2C File Offset: 0x0000612C
		public override bool SendUnreliable(short msgType, MessageBase msg)
		{
			this.m_LocalClient.InvokeHandlerOnClient(msgType, msg, 1);
			return true;
		}

		// Token: 0x060000B8 RID: 184 RVA: 0x00007F50 File Offset: 0x00006150
		public override bool SendByChannel(short msgType, MessageBase msg, int channelId)
		{
			this.m_LocalClient.InvokeHandlerOnClient(msgType, msg, channelId);
			return true;
		}

		// Token: 0x060000B9 RID: 185 RVA: 0x00007F74 File Offset: 0x00006174
		public override bool SendBytes(byte[] bytes, int numBytes, int channelId)
		{
			this.m_LocalClient.InvokeBytesOnClient(bytes, channelId);
			return true;
		}

		// Token: 0x060000BA RID: 186 RVA: 0x00007F98 File Offset: 0x00006198
		public override bool SendWriter(NetworkWriter writer, int channelId)
		{
			this.m_LocalClient.InvokeBytesOnClient(writer.AsArray(), channelId);
			return true;
		}

		// Token: 0x060000BB RID: 187 RVA: 0x00007FC0 File Offset: 0x000061C0
		public override void GetStatsOut(out int numMsgs, out int numBufferedMsgs, out int numBytes, out int lastBufferedPerSecond)
		{
			numMsgs = 0;
			numBufferedMsgs = 0;
			numBytes = 0;
			lastBufferedPerSecond = 0;
		}

		// Token: 0x060000BC RID: 188 RVA: 0x00007FD0 File Offset: 0x000061D0
		public override void GetStatsIn(out int numMsgs, out int numBytes)
		{
			numMsgs = 0;
			numBytes = 0;
		}

		// Token: 0x0400005A RID: 90
		private LocalClient m_LocalClient;
	}
}
