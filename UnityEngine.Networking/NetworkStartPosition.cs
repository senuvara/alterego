﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x0200005D RID: 93
	[DisallowMultipleComponent]
	[AddComponentMenu("Network/NetworkStartPosition")]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class NetworkStartPosition : MonoBehaviour
	{
		// Token: 0x060004A9 RID: 1193 RVA: 0x00019A84 File Offset: 0x00017C84
		public NetworkStartPosition()
		{
		}

		// Token: 0x060004AA RID: 1194 RVA: 0x00019A8C File Offset: 0x00017C8C
		public void Awake()
		{
			NetworkManager.RegisterStartPosition(base.transform);
		}

		// Token: 0x060004AB RID: 1195 RVA: 0x00019A9A File Offset: 0x00017C9A
		public void OnDestroy()
		{
			NetworkManager.UnRegisterStartPosition(base.transform);
		}
	}
}
