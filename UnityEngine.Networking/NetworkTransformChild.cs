﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x0200005E RID: 94
	[AddComponentMenu("Network/NetworkTransformChild")]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class NetworkTransformChild : NetworkBehaviour
	{
		// Token: 0x060004AC RID: 1196 RVA: 0x00019AA8 File Offset: 0x00017CA8
		public NetworkTransformChild()
		{
		}

		// Token: 0x170000BC RID: 188
		// (get) Token: 0x060004AD RID: 1197 RVA: 0x00019AF8 File Offset: 0x00017CF8
		// (set) Token: 0x060004AE RID: 1198 RVA: 0x00019B13 File Offset: 0x00017D13
		public Transform target
		{
			get
			{
				return this.m_Target;
			}
			set
			{
				this.m_Target = value;
				this.OnValidate();
			}
		}

		// Token: 0x170000BD RID: 189
		// (get) Token: 0x060004AF RID: 1199 RVA: 0x00019B24 File Offset: 0x00017D24
		public uint childIndex
		{
			get
			{
				return this.m_ChildIndex;
			}
		}

		// Token: 0x170000BE RID: 190
		// (get) Token: 0x060004B0 RID: 1200 RVA: 0x00019B40 File Offset: 0x00017D40
		// (set) Token: 0x060004B1 RID: 1201 RVA: 0x00019B5B File Offset: 0x00017D5B
		public float sendInterval
		{
			get
			{
				return this.m_SendInterval;
			}
			set
			{
				this.m_SendInterval = value;
			}
		}

		// Token: 0x170000BF RID: 191
		// (get) Token: 0x060004B2 RID: 1202 RVA: 0x00019B68 File Offset: 0x00017D68
		// (set) Token: 0x060004B3 RID: 1203 RVA: 0x00019B83 File Offset: 0x00017D83
		public NetworkTransform.AxisSyncMode syncRotationAxis
		{
			get
			{
				return this.m_SyncRotationAxis;
			}
			set
			{
				this.m_SyncRotationAxis = value;
			}
		}

		// Token: 0x170000C0 RID: 192
		// (get) Token: 0x060004B4 RID: 1204 RVA: 0x00019B90 File Offset: 0x00017D90
		// (set) Token: 0x060004B5 RID: 1205 RVA: 0x00019BAB File Offset: 0x00017DAB
		public NetworkTransform.CompressionSyncMode rotationSyncCompression
		{
			get
			{
				return this.m_RotationSyncCompression;
			}
			set
			{
				this.m_RotationSyncCompression = value;
			}
		}

		// Token: 0x170000C1 RID: 193
		// (get) Token: 0x060004B6 RID: 1206 RVA: 0x00019BB8 File Offset: 0x00017DB8
		// (set) Token: 0x060004B7 RID: 1207 RVA: 0x00019BD3 File Offset: 0x00017DD3
		public float movementThreshold
		{
			get
			{
				return this.m_MovementThreshold;
			}
			set
			{
				this.m_MovementThreshold = value;
			}
		}

		// Token: 0x170000C2 RID: 194
		// (get) Token: 0x060004B8 RID: 1208 RVA: 0x00019BE0 File Offset: 0x00017DE0
		// (set) Token: 0x060004B9 RID: 1209 RVA: 0x00019BFB File Offset: 0x00017DFB
		public float interpolateRotation
		{
			get
			{
				return this.m_InterpolateRotation;
			}
			set
			{
				this.m_InterpolateRotation = value;
			}
		}

		// Token: 0x170000C3 RID: 195
		// (get) Token: 0x060004BA RID: 1210 RVA: 0x00019C08 File Offset: 0x00017E08
		// (set) Token: 0x060004BB RID: 1211 RVA: 0x00019C23 File Offset: 0x00017E23
		public float interpolateMovement
		{
			get
			{
				return this.m_InterpolateMovement;
			}
			set
			{
				this.m_InterpolateMovement = value;
			}
		}

		// Token: 0x170000C4 RID: 196
		// (get) Token: 0x060004BC RID: 1212 RVA: 0x00019C30 File Offset: 0x00017E30
		// (set) Token: 0x060004BD RID: 1213 RVA: 0x00019C4B File Offset: 0x00017E4B
		public NetworkTransform.ClientMoveCallback3D clientMoveCallback3D
		{
			get
			{
				return this.m_ClientMoveCallback3D;
			}
			set
			{
				this.m_ClientMoveCallback3D = value;
			}
		}

		// Token: 0x170000C5 RID: 197
		// (get) Token: 0x060004BE RID: 1214 RVA: 0x00019C58 File Offset: 0x00017E58
		public float lastSyncTime
		{
			get
			{
				return this.m_LastClientSyncTime;
			}
		}

		// Token: 0x170000C6 RID: 198
		// (get) Token: 0x060004BF RID: 1215 RVA: 0x00019C74 File Offset: 0x00017E74
		public Vector3 targetSyncPosition
		{
			get
			{
				return this.m_TargetSyncPosition;
			}
		}

		// Token: 0x170000C7 RID: 199
		// (get) Token: 0x060004C0 RID: 1216 RVA: 0x00019C90 File Offset: 0x00017E90
		public Quaternion targetSyncRotation3D
		{
			get
			{
				return this.m_TargetSyncRotation3D;
			}
		}

		// Token: 0x060004C1 RID: 1217 RVA: 0x00019CAC File Offset: 0x00017EAC
		private void OnValidate()
		{
			if (this.m_Target != null)
			{
				Transform parent = this.m_Target.parent;
				if (parent == null)
				{
					if (LogFilter.logError)
					{
						Debug.LogError("NetworkTransformChild target cannot be the root transform.");
					}
					this.m_Target = null;
					return;
				}
				while (parent.parent != null)
				{
					parent = parent.parent;
				}
				this.m_Root = parent.gameObject.GetComponent<NetworkTransform>();
				if (this.m_Root == null)
				{
					if (LogFilter.logError)
					{
						Debug.LogError("NetworkTransformChild root must have NetworkTransform");
					}
					this.m_Target = null;
					return;
				}
			}
			if (this.m_Root != null)
			{
				this.m_ChildIndex = uint.MaxValue;
				NetworkTransformChild[] components = this.m_Root.GetComponents<NetworkTransformChild>();
				uint num = 0U;
				while ((ulong)num < (ulong)((long)components.Length))
				{
					if (components[(int)((UIntPtr)num)] == this)
					{
						this.m_ChildIndex = num;
						break;
					}
					num += 1U;
				}
				if (this.m_ChildIndex == 4294967295U)
				{
					if (LogFilter.logError)
					{
						Debug.LogError("NetworkTransformChild component must be a child in the same hierarchy");
					}
					this.m_Target = null;
				}
			}
			if (this.m_SendInterval < 0f)
			{
				this.m_SendInterval = 0f;
			}
			if (this.m_SyncRotationAxis < NetworkTransform.AxisSyncMode.None || this.m_SyncRotationAxis > NetworkTransform.AxisSyncMode.AxisXYZ)
			{
				this.m_SyncRotationAxis = NetworkTransform.AxisSyncMode.None;
			}
			if (this.movementThreshold < 0f)
			{
				this.movementThreshold = 0f;
			}
			if (this.interpolateRotation < 0f)
			{
				this.interpolateRotation = 0.01f;
			}
			if (this.interpolateRotation > 1f)
			{
				this.interpolateRotation = 1f;
			}
			if (this.interpolateMovement < 0f)
			{
				this.interpolateMovement = 0.01f;
			}
			if (this.interpolateMovement > 1f)
			{
				this.interpolateMovement = 1f;
			}
		}

		// Token: 0x060004C2 RID: 1218 RVA: 0x00019EC0 File Offset: 0x000180C0
		private void Awake()
		{
			this.m_PrevPosition = this.m_Target.localPosition;
			this.m_PrevRotation = this.m_Target.localRotation;
			if (base.localPlayerAuthority)
			{
				this.m_LocalTransformWriter = new NetworkWriter();
			}
		}

		// Token: 0x060004C3 RID: 1219 RVA: 0x00019F00 File Offset: 0x00018100
		public override bool OnSerialize(NetworkWriter writer, bool initialState)
		{
			if (!initialState)
			{
				if (base.syncVarDirtyBits == 0U)
				{
					writer.WritePackedUInt32(0U);
					return false;
				}
				writer.WritePackedUInt32(1U);
			}
			this.SerializeModeTransform(writer);
			return true;
		}

		// Token: 0x060004C4 RID: 1220 RVA: 0x00019F50 File Offset: 0x00018150
		private void SerializeModeTransform(NetworkWriter writer)
		{
			writer.Write(this.m_Target.localPosition);
			if (this.m_SyncRotationAxis != NetworkTransform.AxisSyncMode.None)
			{
				NetworkTransform.SerializeRotation3D(writer, this.m_Target.localRotation, this.syncRotationAxis, this.rotationSyncCompression);
			}
			this.m_PrevPosition = this.m_Target.localPosition;
			this.m_PrevRotation = this.m_Target.localRotation;
		}

		// Token: 0x060004C5 RID: 1221 RVA: 0x00019FBC File Offset: 0x000181BC
		public override void OnDeserialize(NetworkReader reader, bool initialState)
		{
			if (!base.isServer || !NetworkServer.localClientActive)
			{
				if (!initialState)
				{
					if (reader.ReadPackedUInt32() == 0U)
					{
						return;
					}
				}
				this.UnserializeModeTransform(reader, initialState);
				this.m_LastClientSyncTime = Time.time;
			}
		}

		// Token: 0x060004C6 RID: 1222 RVA: 0x0001A010 File Offset: 0x00018210
		private void UnserializeModeTransform(NetworkReader reader, bool initialState)
		{
			if (base.hasAuthority)
			{
				reader.ReadVector3();
				if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
				{
					NetworkTransform.UnserializeRotation3D(reader, this.syncRotationAxis, this.rotationSyncCompression);
				}
			}
			else if (base.isServer && this.m_ClientMoveCallback3D != null)
			{
				Vector3 targetSyncPosition = reader.ReadVector3();
				Vector3 zero = Vector3.zero;
				Quaternion targetSyncRotation3D = Quaternion.identity;
				if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
				{
					targetSyncRotation3D = NetworkTransform.UnserializeRotation3D(reader, this.syncRotationAxis, this.rotationSyncCompression);
				}
				if (this.m_ClientMoveCallback3D(ref targetSyncPosition, ref zero, ref targetSyncRotation3D))
				{
					this.m_TargetSyncPosition = targetSyncPosition;
					if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
					{
						this.m_TargetSyncRotation3D = targetSyncRotation3D;
					}
				}
			}
			else
			{
				this.m_TargetSyncPosition = reader.ReadVector3();
				if (this.syncRotationAxis != NetworkTransform.AxisSyncMode.None)
				{
					this.m_TargetSyncRotation3D = NetworkTransform.UnserializeRotation3D(reader, this.syncRotationAxis, this.rotationSyncCompression);
				}
			}
		}

		// Token: 0x060004C7 RID: 1223 RVA: 0x0001A117 File Offset: 0x00018317
		private void FixedUpdate()
		{
			if (base.isServer)
			{
				this.FixedUpdateServer();
			}
			if (base.isClient)
			{
				this.FixedUpdateClient();
			}
		}

		// Token: 0x060004C8 RID: 1224 RVA: 0x0001A140 File Offset: 0x00018340
		private void FixedUpdateServer()
		{
			if (base.syncVarDirtyBits == 0U)
			{
				if (NetworkServer.active)
				{
					if (base.isServer)
					{
						if (this.GetNetworkSendInterval() != 0f)
						{
							float num = (this.m_Target.localPosition - this.m_PrevPosition).sqrMagnitude;
							if (num < this.movementThreshold)
							{
								num = Quaternion.Angle(this.m_PrevRotation, this.m_Target.localRotation);
								if (num < this.movementThreshold)
								{
									return;
								}
							}
							base.SetDirtyBit(1U);
						}
					}
				}
			}
		}

		// Token: 0x060004C9 RID: 1225 RVA: 0x0001A1F0 File Offset: 0x000183F0
		private void FixedUpdateClient()
		{
			if (this.m_LastClientSyncTime != 0f)
			{
				if (NetworkServer.active || NetworkClient.active)
				{
					if (base.isServer || base.isClient)
					{
						if (this.GetNetworkSendInterval() != 0f)
						{
							if (!base.hasAuthority)
							{
								if (this.m_LastClientSyncTime != 0f)
								{
									if (this.m_InterpolateMovement > 0f)
									{
										this.m_Target.localPosition = Vector3.Lerp(this.m_Target.localPosition, this.m_TargetSyncPosition, this.m_InterpolateMovement);
									}
									else
									{
										this.m_Target.localPosition = this.m_TargetSyncPosition;
									}
									if (this.m_InterpolateRotation > 0f)
									{
										this.m_Target.localRotation = Quaternion.Slerp(this.m_Target.localRotation, this.m_TargetSyncRotation3D, this.m_InterpolateRotation);
									}
									else
									{
										this.m_Target.localRotation = this.m_TargetSyncRotation3D;
									}
								}
							}
						}
					}
				}
			}
		}

		// Token: 0x060004CA RID: 1226 RVA: 0x0001A320 File Offset: 0x00018520
		private void Update()
		{
			if (base.hasAuthority)
			{
				if (base.localPlayerAuthority)
				{
					if (!NetworkServer.active)
					{
						if (Time.time - this.m_LastClientSendTime > this.GetNetworkSendInterval())
						{
							this.SendTransform();
							this.m_LastClientSendTime = Time.time;
						}
					}
				}
			}
		}

		// Token: 0x060004CB RID: 1227 RVA: 0x0001A388 File Offset: 0x00018588
		private bool HasMoved()
		{
			float num = (this.m_Target.localPosition - this.m_PrevPosition).sqrMagnitude;
			bool result;
			if (num > 1E-05f)
			{
				result = true;
			}
			else
			{
				num = Quaternion.Angle(this.m_Target.localRotation, this.m_PrevRotation);
				result = (num > 1E-05f);
			}
			return result;
		}

		// Token: 0x060004CC RID: 1228 RVA: 0x0001A400 File Offset: 0x00018600
		[Client]
		private void SendTransform()
		{
			if (this.HasMoved() && ClientScene.readyConnection != null)
			{
				this.m_LocalTransformWriter.StartMessage(16);
				this.m_LocalTransformWriter.Write(base.netId);
				this.m_LocalTransformWriter.WritePackedUInt32(this.m_ChildIndex);
				this.SerializeModeTransform(this.m_LocalTransformWriter);
				this.m_PrevPosition = this.m_Target.localPosition;
				this.m_PrevRotation = this.m_Target.localRotation;
				this.m_LocalTransformWriter.FinishMessage();
				ClientScene.readyConnection.SendWriter(this.m_LocalTransformWriter, this.GetNetworkChannel());
			}
		}

		// Token: 0x060004CD RID: 1229 RVA: 0x0001A4A8 File Offset: 0x000186A8
		internal static void HandleChildTransform(NetworkMessage netMsg)
		{
			NetworkInstanceId networkInstanceId = netMsg.reader.ReadNetworkId();
			uint num = netMsg.reader.ReadPackedUInt32();
			GameObject gameObject = NetworkServer.FindLocalObject(networkInstanceId);
			if (gameObject == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("Received NetworkTransformChild data for GameObject that doesn't exist");
				}
			}
			else
			{
				NetworkTransformChild[] components = gameObject.GetComponents<NetworkTransformChild>();
				if (components == null || components.Length == 0)
				{
					if (LogFilter.logError)
					{
						Debug.LogError("HandleChildTransform no children");
					}
				}
				else if ((ulong)num >= (ulong)((long)components.Length))
				{
					if (LogFilter.logError)
					{
						Debug.LogError("HandleChildTransform childIndex invalid");
					}
				}
				else
				{
					NetworkTransformChild networkTransformChild = components[(int)((UIntPtr)num)];
					if (networkTransformChild == null)
					{
						if (LogFilter.logError)
						{
							Debug.LogError("HandleChildTransform null target");
						}
					}
					else if (!networkTransformChild.localPlayerAuthority)
					{
						if (LogFilter.logError)
						{
							Debug.LogError("HandleChildTransform no localPlayerAuthority");
						}
					}
					else if (!netMsg.conn.clientOwnedObjects.Contains(networkInstanceId))
					{
						if (LogFilter.logWarn)
						{
							Debug.LogWarning("NetworkTransformChild netId:" + networkInstanceId + " is not for a valid player");
						}
					}
					else
					{
						networkTransformChild.UnserializeModeTransform(netMsg.reader, false);
						networkTransformChild.m_LastClientSyncTime = Time.time;
						if (!networkTransformChild.isClient)
						{
							networkTransformChild.m_Target.localPosition = networkTransformChild.m_TargetSyncPosition;
							networkTransformChild.m_Target.localRotation = networkTransformChild.m_TargetSyncRotation3D;
						}
					}
				}
			}
		}

		// Token: 0x060004CE RID: 1230 RVA: 0x0001A63C File Offset: 0x0001883C
		public override int GetNetworkChannel()
		{
			return 1;
		}

		// Token: 0x060004CF RID: 1231 RVA: 0x0001A654 File Offset: 0x00018854
		public override float GetNetworkSendInterval()
		{
			return this.m_SendInterval;
		}

		// Token: 0x040001DF RID: 479
		[SerializeField]
		private Transform m_Target;

		// Token: 0x040001E0 RID: 480
		[SerializeField]
		private uint m_ChildIndex;

		// Token: 0x040001E1 RID: 481
		private NetworkTransform m_Root;

		// Token: 0x040001E2 RID: 482
		[SerializeField]
		private float m_SendInterval = 0.1f;

		// Token: 0x040001E3 RID: 483
		[SerializeField]
		private NetworkTransform.AxisSyncMode m_SyncRotationAxis = NetworkTransform.AxisSyncMode.AxisXYZ;

		// Token: 0x040001E4 RID: 484
		[SerializeField]
		private NetworkTransform.CompressionSyncMode m_RotationSyncCompression = NetworkTransform.CompressionSyncMode.None;

		// Token: 0x040001E5 RID: 485
		[SerializeField]
		private float m_MovementThreshold = 0.001f;

		// Token: 0x040001E6 RID: 486
		[SerializeField]
		private float m_InterpolateRotation = 0.5f;

		// Token: 0x040001E7 RID: 487
		[SerializeField]
		private float m_InterpolateMovement = 0.5f;

		// Token: 0x040001E8 RID: 488
		[SerializeField]
		private NetworkTransform.ClientMoveCallback3D m_ClientMoveCallback3D;

		// Token: 0x040001E9 RID: 489
		private Vector3 m_TargetSyncPosition;

		// Token: 0x040001EA RID: 490
		private Quaternion m_TargetSyncRotation3D;

		// Token: 0x040001EB RID: 491
		private float m_LastClientSyncTime;

		// Token: 0x040001EC RID: 492
		private float m_LastClientSendTime;

		// Token: 0x040001ED RID: 493
		private Vector3 m_PrevPosition;

		// Token: 0x040001EE RID: 494
		private Quaternion m_PrevRotation;

		// Token: 0x040001EF RID: 495
		private const float k_LocalMovementThreshold = 1E-05f;

		// Token: 0x040001F0 RID: 496
		private const float k_LocalRotationThreshold = 1E-05f;

		// Token: 0x040001F1 RID: 497
		private NetworkWriter m_LocalTransformWriter;
	}
}
