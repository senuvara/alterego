﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000071 RID: 113
	// (Invoke) Token: 0x060005A3 RID: 1443
	public delegate void NetworkMessageDelegate(NetworkMessage netMsg);
}
