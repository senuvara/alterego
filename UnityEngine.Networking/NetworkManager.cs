﻿using System;
using System.Collections.Generic;
using System.Net;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.Networking.Types;
using UnityEngine.SceneManagement;

namespace UnityEngine.Networking
{
	// Token: 0x0200004E RID: 78
	[AddComponentMenu("Network/NetworkManager")]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class NetworkManager : MonoBehaviour
	{
		// Token: 0x060002E4 RID: 740 RVA: 0x0000DE4C File Offset: 0x0000C04C
		public NetworkManager()
		{
		}

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x060002E5 RID: 741 RVA: 0x0000DF20 File Offset: 0x0000C120
		// (set) Token: 0x060002E6 RID: 742 RVA: 0x0000DF3B File Offset: 0x0000C13B
		public int networkPort
		{
			get
			{
				return this.m_NetworkPort;
			}
			set
			{
				this.m_NetworkPort = value;
			}
		}

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x060002E7 RID: 743 RVA: 0x0000DF48 File Offset: 0x0000C148
		// (set) Token: 0x060002E8 RID: 744 RVA: 0x0000DF63 File Offset: 0x0000C163
		public bool serverBindToIP
		{
			get
			{
				return this.m_ServerBindToIP;
			}
			set
			{
				this.m_ServerBindToIP = value;
			}
		}

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x060002E9 RID: 745 RVA: 0x0000DF70 File Offset: 0x0000C170
		// (set) Token: 0x060002EA RID: 746 RVA: 0x0000DF8B File Offset: 0x0000C18B
		public string serverBindAddress
		{
			get
			{
				return this.m_ServerBindAddress;
			}
			set
			{
				this.m_ServerBindAddress = value;
			}
		}

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x060002EB RID: 747 RVA: 0x0000DF98 File Offset: 0x0000C198
		// (set) Token: 0x060002EC RID: 748 RVA: 0x0000DFB3 File Offset: 0x0000C1B3
		public string networkAddress
		{
			get
			{
				return this.m_NetworkAddress;
			}
			set
			{
				this.m_NetworkAddress = value;
			}
		}

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x060002ED RID: 749 RVA: 0x0000DFC0 File Offset: 0x0000C1C0
		// (set) Token: 0x060002EE RID: 750 RVA: 0x0000DFDB File Offset: 0x0000C1DB
		public bool dontDestroyOnLoad
		{
			get
			{
				return this.m_DontDestroyOnLoad;
			}
			set
			{
				this.m_DontDestroyOnLoad = value;
			}
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x060002EF RID: 751 RVA: 0x0000DFE8 File Offset: 0x0000C1E8
		// (set) Token: 0x060002F0 RID: 752 RVA: 0x0000E003 File Offset: 0x0000C203
		public bool runInBackground
		{
			get
			{
				return this.m_RunInBackground;
			}
			set
			{
				this.m_RunInBackground = value;
			}
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x060002F1 RID: 753 RVA: 0x0000E010 File Offset: 0x0000C210
		// (set) Token: 0x060002F2 RID: 754 RVA: 0x0000E02B File Offset: 0x0000C22B
		public bool scriptCRCCheck
		{
			get
			{
				return this.m_ScriptCRCCheck;
			}
			set
			{
				this.m_ScriptCRCCheck = value;
			}
		}

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x060002F3 RID: 755 RVA: 0x0000E038 File Offset: 0x0000C238
		// (set) Token: 0x060002F4 RID: 756 RVA: 0x0000E04E File Offset: 0x0000C24E
		[Obsolete("moved to NetworkMigrationManager")]
		public bool sendPeerInfo
		{
			get
			{
				return false;
			}
			set
			{
			}
		}

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x060002F5 RID: 757 RVA: 0x0000E054 File Offset: 0x0000C254
		// (set) Token: 0x060002F6 RID: 758 RVA: 0x0000E06F File Offset: 0x0000C26F
		public float maxDelay
		{
			get
			{
				return this.m_MaxDelay;
			}
			set
			{
				this.m_MaxDelay = value;
			}
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x060002F7 RID: 759 RVA: 0x0000E07C File Offset: 0x0000C27C
		// (set) Token: 0x060002F8 RID: 760 RVA: 0x0000E097 File Offset: 0x0000C297
		public LogFilter.FilterLevel logLevel
		{
			get
			{
				return this.m_LogLevel;
			}
			set
			{
				this.m_LogLevel = value;
				LogFilter.currentLogLevel = (int)value;
			}
		}

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x060002F9 RID: 761 RVA: 0x0000E0A8 File Offset: 0x0000C2A8
		// (set) Token: 0x060002FA RID: 762 RVA: 0x0000E0C3 File Offset: 0x0000C2C3
		public GameObject playerPrefab
		{
			get
			{
				return this.m_PlayerPrefab;
			}
			set
			{
				this.m_PlayerPrefab = value;
			}
		}

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x060002FB RID: 763 RVA: 0x0000E0D0 File Offset: 0x0000C2D0
		// (set) Token: 0x060002FC RID: 764 RVA: 0x0000E0EB File Offset: 0x0000C2EB
		public bool autoCreatePlayer
		{
			get
			{
				return this.m_AutoCreatePlayer;
			}
			set
			{
				this.m_AutoCreatePlayer = value;
			}
		}

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x060002FD RID: 765 RVA: 0x0000E0F8 File Offset: 0x0000C2F8
		// (set) Token: 0x060002FE RID: 766 RVA: 0x0000E113 File Offset: 0x0000C313
		public PlayerSpawnMethod playerSpawnMethod
		{
			get
			{
				return this.m_PlayerSpawnMethod;
			}
			set
			{
				this.m_PlayerSpawnMethod = value;
			}
		}

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x060002FF RID: 767 RVA: 0x0000E120 File Offset: 0x0000C320
		// (set) Token: 0x06000300 RID: 768 RVA: 0x0000E13B File Offset: 0x0000C33B
		public string offlineScene
		{
			get
			{
				return this.m_OfflineScene;
			}
			set
			{
				this.m_OfflineScene = value;
			}
		}

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x06000301 RID: 769 RVA: 0x0000E148 File Offset: 0x0000C348
		// (set) Token: 0x06000302 RID: 770 RVA: 0x0000E163 File Offset: 0x0000C363
		public string onlineScene
		{
			get
			{
				return this.m_OnlineScene;
			}
			set
			{
				this.m_OnlineScene = value;
			}
		}

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x06000303 RID: 771 RVA: 0x0000E170 File Offset: 0x0000C370
		public List<GameObject> spawnPrefabs
		{
			get
			{
				return this.m_SpawnPrefabs;
			}
		}

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x06000304 RID: 772 RVA: 0x0000E18C File Offset: 0x0000C38C
		public List<Transform> startPositions
		{
			get
			{
				return NetworkManager.s_StartPositions;
			}
		}

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x06000305 RID: 773 RVA: 0x0000E1A8 File Offset: 0x0000C3A8
		// (set) Token: 0x06000306 RID: 774 RVA: 0x0000E1C3 File Offset: 0x0000C3C3
		public bool customConfig
		{
			get
			{
				return this.m_CustomConfig;
			}
			set
			{
				this.m_CustomConfig = value;
			}
		}

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x06000307 RID: 775 RVA: 0x0000E1D0 File Offset: 0x0000C3D0
		public ConnectionConfig connectionConfig
		{
			get
			{
				if (this.m_ConnectionConfig == null)
				{
					this.m_ConnectionConfig = new ConnectionConfig();
				}
				return this.m_ConnectionConfig;
			}
		}

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x06000308 RID: 776 RVA: 0x0000E204 File Offset: 0x0000C404
		public GlobalConfig globalConfig
		{
			get
			{
				if (this.m_GlobalConfig == null)
				{
					this.m_GlobalConfig = new GlobalConfig();
				}
				return this.m_GlobalConfig;
			}
		}

		// Token: 0x17000080 RID: 128
		// (get) Token: 0x06000309 RID: 777 RVA: 0x0000E238 File Offset: 0x0000C438
		// (set) Token: 0x0600030A RID: 778 RVA: 0x0000E253 File Offset: 0x0000C453
		public int maxConnections
		{
			get
			{
				return this.m_MaxConnections;
			}
			set
			{
				this.m_MaxConnections = value;
			}
		}

		// Token: 0x17000081 RID: 129
		// (get) Token: 0x0600030B RID: 779 RVA: 0x0000E260 File Offset: 0x0000C460
		public List<QosType> channels
		{
			get
			{
				return this.m_Channels;
			}
		}

		// Token: 0x17000082 RID: 130
		// (get) Token: 0x0600030C RID: 780 RVA: 0x0000E27C File Offset: 0x0000C47C
		// (set) Token: 0x0600030D RID: 781 RVA: 0x0000E297 File Offset: 0x0000C497
		public EndPoint secureTunnelEndpoint
		{
			get
			{
				return this.m_EndPoint;
			}
			set
			{
				this.m_EndPoint = value;
			}
		}

		// Token: 0x17000083 RID: 131
		// (get) Token: 0x0600030E RID: 782 RVA: 0x0000E2A4 File Offset: 0x0000C4A4
		// (set) Token: 0x0600030F RID: 783 RVA: 0x0000E2BF File Offset: 0x0000C4BF
		public bool useWebSockets
		{
			get
			{
				return this.m_UseWebSockets;
			}
			set
			{
				this.m_UseWebSockets = value;
			}
		}

		// Token: 0x17000084 RID: 132
		// (get) Token: 0x06000310 RID: 784 RVA: 0x0000E2CC File Offset: 0x0000C4CC
		// (set) Token: 0x06000311 RID: 785 RVA: 0x0000E2E7 File Offset: 0x0000C4E7
		public bool useSimulator
		{
			get
			{
				return this.m_UseSimulator;
			}
			set
			{
				this.m_UseSimulator = value;
			}
		}

		// Token: 0x17000085 RID: 133
		// (get) Token: 0x06000312 RID: 786 RVA: 0x0000E2F4 File Offset: 0x0000C4F4
		// (set) Token: 0x06000313 RID: 787 RVA: 0x0000E30F File Offset: 0x0000C50F
		public int simulatedLatency
		{
			get
			{
				return this.m_SimulatedLatency;
			}
			set
			{
				this.m_SimulatedLatency = value;
			}
		}

		// Token: 0x17000086 RID: 134
		// (get) Token: 0x06000314 RID: 788 RVA: 0x0000E31C File Offset: 0x0000C51C
		// (set) Token: 0x06000315 RID: 789 RVA: 0x0000E337 File Offset: 0x0000C537
		public float packetLossPercentage
		{
			get
			{
				return this.m_PacketLossPercentage;
			}
			set
			{
				this.m_PacketLossPercentage = value;
			}
		}

		// Token: 0x17000087 RID: 135
		// (get) Token: 0x06000316 RID: 790 RVA: 0x0000E344 File Offset: 0x0000C544
		// (set) Token: 0x06000317 RID: 791 RVA: 0x0000E35F File Offset: 0x0000C55F
		public string matchHost
		{
			get
			{
				return this.m_MatchHost;
			}
			set
			{
				this.m_MatchHost = value;
			}
		}

		// Token: 0x17000088 RID: 136
		// (get) Token: 0x06000318 RID: 792 RVA: 0x0000E36C File Offset: 0x0000C56C
		// (set) Token: 0x06000319 RID: 793 RVA: 0x0000E387 File Offset: 0x0000C587
		public int matchPort
		{
			get
			{
				return this.m_MatchPort;
			}
			set
			{
				this.m_MatchPort = value;
			}
		}

		// Token: 0x17000089 RID: 137
		// (get) Token: 0x0600031A RID: 794 RVA: 0x0000E394 File Offset: 0x0000C594
		// (set) Token: 0x0600031B RID: 795 RVA: 0x0000E3AF File Offset: 0x0000C5AF
		public bool clientLoadedScene
		{
			get
			{
				return this.m_ClientLoadedScene;
			}
			set
			{
				this.m_ClientLoadedScene = value;
			}
		}

		// Token: 0x1700008A RID: 138
		// (get) Token: 0x0600031C RID: 796 RVA: 0x0000E3BC File Offset: 0x0000C5BC
		public NetworkMigrationManager migrationManager
		{
			get
			{
				return this.m_MigrationManager;
			}
		}

		// Token: 0x1700008B RID: 139
		// (get) Token: 0x0600031D RID: 797 RVA: 0x0000E3D8 File Offset: 0x0000C5D8
		public int numPlayers
		{
			get
			{
				int num = 0;
				for (int i = 0; i < NetworkServer.connections.Count; i++)
				{
					NetworkConnection networkConnection = NetworkServer.connections[i];
					if (networkConnection != null)
					{
						for (int j = 0; j < networkConnection.playerControllers.Count; j++)
						{
							if (networkConnection.playerControllers[j].IsValid)
							{
								num++;
							}
						}
					}
				}
				return num;
			}
		}

		// Token: 0x1700008C RID: 140
		// (get) Token: 0x0600031E RID: 798 RVA: 0x0000E460 File Offset: 0x0000C660
		public static INetworkTransport defaultTransport
		{
			get
			{
				return new DefaultNetworkTransport();
			}
		}

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x0600031F RID: 799 RVA: 0x0000E47C File Offset: 0x0000C67C
		// (set) Token: 0x06000320 RID: 800 RVA: 0x0000E496 File Offset: 0x0000C696
		public static INetworkTransport activeTransport
		{
			get
			{
				return NetworkManager.s_ActiveTransport;
			}
			set
			{
				if (NetworkManager.s_ActiveTransport != null && NetworkManager.s_ActiveTransport.IsStarted)
				{
					throw new InvalidOperationException("Cannot change network transport when current transport object is in use.");
				}
				if (value == null)
				{
					throw new ArgumentNullException("Cannot set active transport to null.");
				}
				NetworkManager.s_ActiveTransport = value;
			}
		}

		// Token: 0x06000321 RID: 801 RVA: 0x0000E4D6 File Offset: 0x0000C6D6
		private void Awake()
		{
			this.InitializeSingleton();
		}

		// Token: 0x06000322 RID: 802 RVA: 0x0000E4E0 File Offset: 0x0000C6E0
		private void InitializeSingleton()
		{
			if (!(NetworkManager.singleton != null) || !(NetworkManager.singleton == this))
			{
				int logLevel = (int)this.m_LogLevel;
				if (logLevel != -1)
				{
					LogFilter.currentLogLevel = logLevel;
				}
				if (this.m_DontDestroyOnLoad)
				{
					if (NetworkManager.singleton != null)
					{
						if (LogFilter.logDev)
						{
							Debug.Log("Multiple NetworkManagers detected in the scene. Only one NetworkManager can exist at a time. The duplicate NetworkManager will not be used.");
						}
						Object.Destroy(base.gameObject);
						return;
					}
					if (LogFilter.logDev)
					{
						Debug.Log("NetworkManager created singleton (DontDestroyOnLoad)");
					}
					NetworkManager.singleton = this;
					if (Application.isPlaying)
					{
						Object.DontDestroyOnLoad(base.gameObject);
					}
				}
				else
				{
					if (LogFilter.logDev)
					{
						Debug.Log("NetworkManager created singleton (ForScene)");
					}
					NetworkManager.singleton = this;
				}
				if (this.m_NetworkAddress != "")
				{
					NetworkManager.s_Address = this.m_NetworkAddress;
				}
				else if (NetworkManager.s_Address != "")
				{
					this.m_NetworkAddress = NetworkManager.s_Address;
				}
			}
		}

		// Token: 0x06000323 RID: 803 RVA: 0x0000E60C File Offset: 0x0000C80C
		private void OnValidate()
		{
			if (this.m_SimulatedLatency < 1)
			{
				this.m_SimulatedLatency = 1;
			}
			if (this.m_SimulatedLatency > 500)
			{
				this.m_SimulatedLatency = 500;
			}
			if (this.m_PacketLossPercentage < 0f)
			{
				this.m_PacketLossPercentage = 0f;
			}
			if (this.m_PacketLossPercentage > 99f)
			{
				this.m_PacketLossPercentage = 99f;
			}
			if (this.m_MaxConnections <= 0)
			{
				this.m_MaxConnections = 1;
			}
			if (this.m_MaxConnections > 32000)
			{
				this.m_MaxConnections = 32000;
			}
			if (this.m_MaxBufferedPackets <= 0)
			{
				this.m_MaxBufferedPackets = 0;
			}
			if (this.m_MaxBufferedPackets > 512)
			{
				this.m_MaxBufferedPackets = 512;
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkManager - MaxBufferedPackets cannot be more than " + 512);
				}
			}
			if (this.m_PlayerPrefab != null && this.m_PlayerPrefab.GetComponent<NetworkIdentity>() == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkManager - playerPrefab must have a NetworkIdentity.");
				}
				this.m_PlayerPrefab = null;
			}
			if (this.m_ConnectionConfig != null && this.m_ConnectionConfig.MinUpdateTimeout <= 0U)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("NetworkManager MinUpdateTimeout cannot be zero or less. The value will be reset to 1 millisecond");
				}
				this.m_ConnectionConfig.MinUpdateTimeout = 1U;
			}
			if (this.m_GlobalConfig != null)
			{
				if (this.m_GlobalConfig.ThreadAwakeTimeout <= 0U)
				{
					if (LogFilter.logError)
					{
						Debug.LogError("NetworkManager ThreadAwakeTimeout cannot be zero or less. The value will be reset to 1 millisecond");
					}
					this.m_GlobalConfig.ThreadAwakeTimeout = 1U;
				}
			}
		}

		// Token: 0x06000324 RID: 804 RVA: 0x0000E7CC File Offset: 0x0000C9CC
		internal void RegisterServerMessages()
		{
			NetworkServer.RegisterHandler(32, new NetworkMessageDelegate(this.OnServerConnectInternal));
			NetworkServer.RegisterHandler(33, new NetworkMessageDelegate(this.OnServerDisconnectInternal));
			NetworkServer.RegisterHandler(35, new NetworkMessageDelegate(this.OnServerReadyMessageInternal));
			NetworkServer.RegisterHandler(37, new NetworkMessageDelegate(this.OnServerAddPlayerMessageInternal));
			NetworkServer.RegisterHandler(38, new NetworkMessageDelegate(this.OnServerRemovePlayerMessageInternal));
			NetworkServer.RegisterHandler(34, new NetworkMessageDelegate(this.OnServerErrorInternal));
		}

		// Token: 0x06000325 RID: 805 RVA: 0x0000E84C File Offset: 0x0000CA4C
		public void SetupMigrationManager(NetworkMigrationManager man)
		{
			this.m_MigrationManager = man;
		}

		// Token: 0x06000326 RID: 806 RVA: 0x0000E858 File Offset: 0x0000CA58
		public bool StartServer(ConnectionConfig config, int maxConnections)
		{
			return this.StartServer(null, config, maxConnections);
		}

		// Token: 0x06000327 RID: 807 RVA: 0x0000E878 File Offset: 0x0000CA78
		public bool StartServer()
		{
			return this.StartServer(null);
		}

		// Token: 0x06000328 RID: 808 RVA: 0x0000E894 File Offset: 0x0000CA94
		public bool StartServer(MatchInfo info)
		{
			return this.StartServer(info, null, -1);
		}

		// Token: 0x06000329 RID: 809 RVA: 0x0000E8B4 File Offset: 0x0000CAB4
		private bool StartServer(MatchInfo info, ConnectionConfig config, int maxConnections)
		{
			this.InitializeSingleton();
			this.OnStartServer();
			if (this.m_RunInBackground)
			{
				Application.runInBackground = true;
			}
			NetworkCRC.scriptCRCCheck = this.scriptCRCCheck;
			NetworkServer.useWebSockets = this.m_UseWebSockets;
			if (this.m_GlobalConfig != null)
			{
				NetworkManager.activeTransport.Init(this.m_GlobalConfig);
			}
			if (this.m_CustomConfig && this.m_ConnectionConfig != null && config == null)
			{
				this.m_ConnectionConfig.Channels.Clear();
				for (int i = 0; i < this.m_Channels.Count; i++)
				{
					this.m_ConnectionConfig.AddChannel(this.m_Channels[i]);
				}
				NetworkServer.Configure(this.m_ConnectionConfig, this.m_MaxConnections);
			}
			if (config != null)
			{
				NetworkServer.Configure(config, maxConnections);
			}
			if (info != null)
			{
				if (!NetworkServer.Listen(info, this.m_NetworkPort))
				{
					if (LogFilter.logError)
					{
						Debug.LogError("StartServer listen failed.");
					}
					return false;
				}
			}
			else if (this.m_ServerBindToIP && !string.IsNullOrEmpty(this.m_ServerBindAddress))
			{
				if (!NetworkServer.Listen(this.m_ServerBindAddress, this.m_NetworkPort))
				{
					if (LogFilter.logError)
					{
						Debug.LogError("StartServer listen on " + this.m_ServerBindAddress + " failed.");
					}
					return false;
				}
			}
			else if (!NetworkServer.Listen(this.m_NetworkPort))
			{
				if (LogFilter.logError)
				{
					Debug.LogError("StartServer listen failed.");
				}
				return false;
			}
			this.RegisterServerMessages();
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkManager StartServer port:" + this.m_NetworkPort);
			}
			this.isNetworkActive = true;
			string name = SceneManager.GetSceneAt(0).name;
			if (!string.IsNullOrEmpty(this.m_OnlineScene) && this.m_OnlineScene != name && this.m_OnlineScene != this.m_OfflineScene)
			{
				this.ServerChangeScene(this.m_OnlineScene);
			}
			else
			{
				NetworkServer.SpawnObjects();
			}
			return true;
		}

		// Token: 0x0600032A RID: 810 RVA: 0x0000EB08 File Offset: 0x0000CD08
		internal void RegisterClientMessages(NetworkClient client)
		{
			client.RegisterHandler(32, new NetworkMessageDelegate(this.OnClientConnectInternal));
			client.RegisterHandler(33, new NetworkMessageDelegate(this.OnClientDisconnectInternal));
			client.RegisterHandler(36, new NetworkMessageDelegate(this.OnClientNotReadyMessageInternal));
			client.RegisterHandler(34, new NetworkMessageDelegate(this.OnClientErrorInternal));
			client.RegisterHandler(39, new NetworkMessageDelegate(this.OnClientSceneInternal));
			if (this.m_PlayerPrefab != null)
			{
				ClientScene.RegisterPrefab(this.m_PlayerPrefab);
			}
			for (int i = 0; i < this.m_SpawnPrefabs.Count; i++)
			{
				GameObject gameObject = this.m_SpawnPrefabs[i];
				if (gameObject != null)
				{
					ClientScene.RegisterPrefab(gameObject);
				}
			}
		}

		// Token: 0x0600032B RID: 811 RVA: 0x0000EBD8 File Offset: 0x0000CDD8
		public void UseExternalClient(NetworkClient externalClient)
		{
			if (this.m_RunInBackground)
			{
				Application.runInBackground = true;
			}
			if (externalClient != null)
			{
				this.client = externalClient;
				this.isNetworkActive = true;
				this.RegisterClientMessages(this.client);
				this.OnStartClient(this.client);
			}
			else
			{
				this.OnStopClient();
				ClientScene.DestroyAllClientObjects();
				ClientScene.HandleClientDisconnect(this.client.connection);
				this.client = null;
				if (!string.IsNullOrEmpty(this.m_OfflineScene))
				{
					this.ClientChangeScene(this.m_OfflineScene, false);
				}
			}
			NetworkManager.s_Address = this.m_NetworkAddress;
		}

		// Token: 0x0600032C RID: 812 RVA: 0x0000EC78 File Offset: 0x0000CE78
		public NetworkClient StartClient(MatchInfo info, ConnectionConfig config, int hostPort)
		{
			this.InitializeSingleton();
			this.matchInfo = info;
			if (this.m_RunInBackground)
			{
				Application.runInBackground = true;
			}
			this.isNetworkActive = true;
			if (this.m_GlobalConfig != null)
			{
				NetworkManager.activeTransport.Init(this.m_GlobalConfig);
			}
			this.client = new NetworkClient();
			this.client.hostPort = hostPort;
			if (config != null)
			{
				if (config.UsePlatformSpecificProtocols && Application.platform != RuntimePlatform.PS4)
				{
					throw new ArgumentOutOfRangeException("Platform specific protocols are not supported on this platform");
				}
				this.client.Configure(config, 1);
			}
			else if (this.m_CustomConfig && this.m_ConnectionConfig != null)
			{
				this.m_ConnectionConfig.Channels.Clear();
				for (int i = 0; i < this.m_Channels.Count; i++)
				{
					this.m_ConnectionConfig.AddChannel(this.m_Channels[i]);
				}
				if (this.m_ConnectionConfig.UsePlatformSpecificProtocols && Application.platform != RuntimePlatform.PS4)
				{
					throw new ArgumentOutOfRangeException("Platform specific protocols are not supported on this platform");
				}
				this.client.Configure(this.m_ConnectionConfig, this.m_MaxConnections);
			}
			this.RegisterClientMessages(this.client);
			if (this.matchInfo != null)
			{
				if (LogFilter.logDebug)
				{
					Debug.Log("NetworkManager StartClient match: " + this.matchInfo);
				}
				this.client.Connect(this.matchInfo);
			}
			else if (this.m_EndPoint != null)
			{
				if (LogFilter.logDebug)
				{
					Debug.Log("NetworkManager StartClient using provided SecureTunnel");
				}
				this.client.Connect(this.m_EndPoint);
			}
			else
			{
				if (string.IsNullOrEmpty(this.m_NetworkAddress))
				{
					if (LogFilter.logError)
					{
						Debug.LogError("Must set the Network Address field in the manager");
					}
					return null;
				}
				if (LogFilter.logDebug)
				{
					Debug.Log(string.Concat(new object[]
					{
						"NetworkManager StartClient address:",
						this.m_NetworkAddress,
						" port:",
						this.m_NetworkPort
					}));
				}
				if (this.m_UseSimulator)
				{
					this.client.ConnectWithSimulator(this.m_NetworkAddress, this.m_NetworkPort, this.m_SimulatedLatency, this.m_PacketLossPercentage);
				}
				else
				{
					this.client.Connect(this.m_NetworkAddress, this.m_NetworkPort);
				}
			}
			if (this.m_MigrationManager != null)
			{
				this.m_MigrationManager.Initialize(this.client, this.matchInfo);
			}
			this.OnStartClient(this.client);
			NetworkManager.s_Address = this.m_NetworkAddress;
			return this.client;
		}

		// Token: 0x0600032D RID: 813 RVA: 0x0000EF54 File Offset: 0x0000D154
		public NetworkClient StartClient(MatchInfo matchInfo)
		{
			return this.StartClient(matchInfo, null);
		}

		// Token: 0x0600032E RID: 814 RVA: 0x0000EF74 File Offset: 0x0000D174
		public NetworkClient StartClient()
		{
			return this.StartClient(null, null);
		}

		// Token: 0x0600032F RID: 815 RVA: 0x0000EF94 File Offset: 0x0000D194
		public NetworkClient StartClient(MatchInfo info, ConnectionConfig config)
		{
			return this.StartClient(info, config, 0);
		}

		// Token: 0x06000330 RID: 816 RVA: 0x0000EFB4 File Offset: 0x0000D1B4
		public virtual NetworkClient StartHost(ConnectionConfig config, int maxConnections)
		{
			this.OnStartHost();
			NetworkClient result;
			if (this.StartServer(null, config, maxConnections))
			{
				NetworkClient networkClient = this.ConnectLocalClient();
				this.OnServerConnect(networkClient.connection);
				this.OnStartClient(networkClient);
				result = networkClient;
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06000331 RID: 817 RVA: 0x0000F000 File Offset: 0x0000D200
		public virtual NetworkClient StartHost(MatchInfo info)
		{
			this.OnStartHost();
			this.matchInfo = info;
			NetworkClient result;
			if (this.StartServer(info))
			{
				NetworkClient networkClient = this.ConnectLocalClient();
				this.OnStartClient(networkClient);
				result = networkClient;
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06000332 RID: 818 RVA: 0x0000F048 File Offset: 0x0000D248
		public virtual NetworkClient StartHost()
		{
			this.OnStartHost();
			NetworkClient result;
			if (this.StartServer())
			{
				NetworkClient networkClient = this.ConnectLocalClient();
				this.OnStartClient(networkClient);
				result = networkClient;
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06000333 RID: 819 RVA: 0x0000F088 File Offset: 0x0000D288
		private NetworkClient ConnectLocalClient()
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkManager StartHost port:" + this.m_NetworkPort);
			}
			this.m_NetworkAddress = "localhost";
			this.client = ClientScene.ConnectLocalServer();
			this.RegisterClientMessages(this.client);
			if (this.m_MigrationManager != null)
			{
				this.m_MigrationManager.Initialize(this.client, this.matchInfo);
			}
			return this.client;
		}

		// Token: 0x06000334 RID: 820 RVA: 0x0000F118 File Offset: 0x0000D318
		public void StopHost()
		{
			bool active = NetworkServer.active;
			this.OnStopHost();
			this.StopServer();
			this.StopClient();
			if (this.m_MigrationManager != null)
			{
				if (active)
				{
					this.m_MigrationManager.LostHostOnHost();
				}
			}
		}

		// Token: 0x06000335 RID: 821 RVA: 0x0000F164 File Offset: 0x0000D364
		public void StopServer()
		{
			if (NetworkServer.active)
			{
				this.OnStopServer();
				if (LogFilter.logDebug)
				{
					Debug.Log("NetworkManager StopServer");
				}
				this.isNetworkActive = false;
				NetworkServer.Shutdown();
				this.StopMatchMaker();
				if (!string.IsNullOrEmpty(this.m_OfflineScene))
				{
					this.ServerChangeScene(this.m_OfflineScene);
				}
				this.CleanupNetworkIdentities();
			}
		}

		// Token: 0x06000336 RID: 822 RVA: 0x0000F1D4 File Offset: 0x0000D3D4
		public void StopClient()
		{
			this.OnStopClient();
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkManager StopClient");
			}
			this.isNetworkActive = false;
			if (this.client != null)
			{
				this.client.Disconnect();
				this.client.Shutdown();
				this.client = null;
			}
			this.StopMatchMaker();
			ClientScene.DestroyAllClientObjects();
			if (!string.IsNullOrEmpty(this.m_OfflineScene))
			{
				this.ClientChangeScene(this.m_OfflineScene, false);
			}
			this.CleanupNetworkIdentities();
		}

		// Token: 0x06000337 RID: 823 RVA: 0x0000F260 File Offset: 0x0000D460
		public virtual void ServerChangeScene(string newSceneName)
		{
			if (string.IsNullOrEmpty(newSceneName))
			{
				if (LogFilter.logError)
				{
					Debug.LogError("ServerChangeScene empty scene name");
				}
			}
			else
			{
				if (LogFilter.logDebug)
				{
					Debug.Log("ServerChangeScene " + newSceneName);
				}
				NetworkServer.SetAllClientsNotReady();
				NetworkManager.networkSceneName = newSceneName;
				NetworkManager.s_LoadingSceneAsync = SceneManager.LoadSceneAsync(newSceneName);
				StringMessage msg = new StringMessage(NetworkManager.networkSceneName);
				NetworkServer.SendToAll(39, msg);
				NetworkManager.s_StartPositionIndex = 0;
				NetworkManager.s_StartPositions.Clear();
			}
		}

		// Token: 0x06000338 RID: 824 RVA: 0x0000F2EC File Offset: 0x0000D4EC
		private void CleanupNetworkIdentities()
		{
			foreach (NetworkIdentity networkIdentity in Resources.FindObjectsOfTypeAll<NetworkIdentity>())
			{
				networkIdentity.MarkForReset();
			}
		}

		// Token: 0x06000339 RID: 825 RVA: 0x0000F324 File Offset: 0x0000D524
		internal void ClientChangeScene(string newSceneName, bool forceReload)
		{
			if (string.IsNullOrEmpty(newSceneName))
			{
				if (LogFilter.logError)
				{
					Debug.LogError("ClientChangeScene empty scene name");
				}
			}
			else
			{
				if (LogFilter.logDebug)
				{
					Debug.Log("ClientChangeScene newSceneName:" + newSceneName + " networkSceneName:" + NetworkManager.networkSceneName);
				}
				if (newSceneName == NetworkManager.networkSceneName)
				{
					if (this.m_MigrationManager != null)
					{
						this.FinishLoadScene();
						return;
					}
					if (!forceReload)
					{
						this.FinishLoadScene();
						return;
					}
				}
				NetworkManager.s_LoadingSceneAsync = SceneManager.LoadSceneAsync(newSceneName);
				NetworkManager.networkSceneName = newSceneName;
			}
		}

		// Token: 0x0600033A RID: 826 RVA: 0x0000F3D4 File Offset: 0x0000D5D4
		private void FinishLoadScene()
		{
			if (this.client != null)
			{
				if (NetworkManager.s_ClientReadyConnection != null)
				{
					this.m_ClientLoadedScene = true;
					this.OnClientConnect(NetworkManager.s_ClientReadyConnection);
					NetworkManager.s_ClientReadyConnection = null;
				}
			}
			else if (LogFilter.logDev)
			{
				Debug.Log("FinishLoadScene client is null");
			}
			if (NetworkServer.active)
			{
				NetworkServer.SpawnObjects();
				this.OnServerSceneChanged(NetworkManager.networkSceneName);
			}
			if (this.IsClientConnected() && this.client != null)
			{
				this.RegisterClientMessages(this.client);
				this.OnClientSceneChanged(this.client.connection);
			}
		}

		// Token: 0x0600033B RID: 827 RVA: 0x0000F484 File Offset: 0x0000D684
		internal static void UpdateScene()
		{
			if (!(NetworkManager.singleton == null))
			{
				if (NetworkManager.s_LoadingSceneAsync != null)
				{
					if (NetworkManager.s_LoadingSceneAsync.isDone)
					{
						if (LogFilter.logDebug)
						{
							Debug.Log("ClientChangeScene done readyCon:" + NetworkManager.s_ClientReadyConnection);
						}
						NetworkManager.singleton.FinishLoadScene();
						NetworkManager.s_LoadingSceneAsync.allowSceneActivation = true;
						NetworkManager.s_LoadingSceneAsync = null;
					}
				}
			}
		}

		// Token: 0x0600033C RID: 828 RVA: 0x0000F505 File Offset: 0x0000D705
		private void OnDestroy()
		{
			if (LogFilter.logDev)
			{
				Debug.Log("NetworkManager destroyed");
			}
		}

		// Token: 0x0600033D RID: 829 RVA: 0x0000F520 File Offset: 0x0000D720
		public static void RegisterStartPosition(Transform start)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log(string.Concat(new object[]
				{
					"RegisterStartPosition: (",
					start.gameObject.name,
					") ",
					start.position
				}));
			}
			NetworkManager.s_StartPositions.Add(start);
		}

		// Token: 0x0600033E RID: 830 RVA: 0x0000F584 File Offset: 0x0000D784
		public static void UnRegisterStartPosition(Transform start)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log(string.Concat(new object[]
				{
					"UnRegisterStartPosition: (",
					start.gameObject.name,
					") ",
					start.position
				}));
			}
			NetworkManager.s_StartPositions.Remove(start);
		}

		// Token: 0x0600033F RID: 831 RVA: 0x0000F5E8 File Offset: 0x0000D7E8
		public bool IsClientConnected()
		{
			return this.client != null && this.client.isConnected;
		}

		// Token: 0x06000340 RID: 832 RVA: 0x0000F616 File Offset: 0x0000D816
		public static void Shutdown()
		{
			if (!(NetworkManager.singleton == null))
			{
				NetworkManager.s_StartPositions.Clear();
				NetworkManager.s_StartPositionIndex = 0;
				NetworkManager.s_ClientReadyConnection = null;
				NetworkManager.singleton.StopHost();
				NetworkManager.singleton = null;
			}
		}

		// Token: 0x06000341 RID: 833 RVA: 0x0000F654 File Offset: 0x0000D854
		internal void OnServerConnectInternal(NetworkMessage netMsg)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkManager:OnServerConnectInternal");
			}
			netMsg.conn.SetMaxDelay(this.m_MaxDelay);
			if (this.m_MaxBufferedPackets != 512)
			{
				for (int i = 0; i < NetworkServer.numChannels; i++)
				{
					netMsg.conn.SetChannelOption(i, ChannelOption.MaxPendingBuffers, this.m_MaxBufferedPackets);
				}
			}
			if (!this.m_AllowFragmentation)
			{
				for (int j = 0; j < NetworkServer.numChannels; j++)
				{
					netMsg.conn.SetChannelOption(j, ChannelOption.AllowFragmentation, 0);
				}
			}
			if (NetworkManager.networkSceneName != "" && NetworkManager.networkSceneName != this.m_OfflineScene)
			{
				StringMessage msg = new StringMessage(NetworkManager.networkSceneName);
				netMsg.conn.Send(39, msg);
			}
			if (this.m_MigrationManager != null)
			{
				this.m_MigrationManager.SendPeerInfo();
			}
			this.OnServerConnect(netMsg.conn);
		}

		// Token: 0x06000342 RID: 834 RVA: 0x0000F76C File Offset: 0x0000D96C
		internal void OnServerDisconnectInternal(NetworkMessage netMsg)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkManager:OnServerDisconnectInternal");
			}
			if (this.m_MigrationManager != null)
			{
				this.m_MigrationManager.SendPeerInfo();
			}
			this.OnServerDisconnect(netMsg.conn);
		}

		// Token: 0x06000343 RID: 835 RVA: 0x0000F7BA File Offset: 0x0000D9BA
		internal void OnServerReadyMessageInternal(NetworkMessage netMsg)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkManager:OnServerReadyMessageInternal");
			}
			this.OnServerReady(netMsg.conn);
		}

		// Token: 0x06000344 RID: 836 RVA: 0x0000F7E0 File Offset: 0x0000D9E0
		internal void OnServerAddPlayerMessageInternal(NetworkMessage netMsg)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkManager:OnServerAddPlayerMessageInternal");
			}
			netMsg.ReadMessage<AddPlayerMessage>(NetworkManager.s_AddPlayerMessage);
			if (NetworkManager.s_AddPlayerMessage.msgSize != 0)
			{
				NetworkReader extraMessageReader = new NetworkReader(NetworkManager.s_AddPlayerMessage.msgData);
				this.OnServerAddPlayer(netMsg.conn, NetworkManager.s_AddPlayerMessage.playerControllerId, extraMessageReader);
			}
			else
			{
				this.OnServerAddPlayer(netMsg.conn, NetworkManager.s_AddPlayerMessage.playerControllerId);
			}
			if (this.m_MigrationManager != null)
			{
				this.m_MigrationManager.SendPeerInfo();
			}
		}

		// Token: 0x06000345 RID: 837 RVA: 0x0000F884 File Offset: 0x0000DA84
		internal void OnServerRemovePlayerMessageInternal(NetworkMessage netMsg)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkManager:OnServerRemovePlayerMessageInternal");
			}
			netMsg.ReadMessage<RemovePlayerMessage>(NetworkManager.s_RemovePlayerMessage);
			PlayerController player;
			netMsg.conn.GetPlayerController(NetworkManager.s_RemovePlayerMessage.playerControllerId, out player);
			this.OnServerRemovePlayer(netMsg.conn, player);
			netMsg.conn.RemovePlayerController(NetworkManager.s_RemovePlayerMessage.playerControllerId);
			if (this.m_MigrationManager != null)
			{
				this.m_MigrationManager.SendPeerInfo();
			}
		}

		// Token: 0x06000346 RID: 838 RVA: 0x0000F90B File Offset: 0x0000DB0B
		internal void OnServerErrorInternal(NetworkMessage netMsg)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkManager:OnServerErrorInternal");
			}
			netMsg.ReadMessage<ErrorMessage>(NetworkManager.s_ErrorMessage);
			this.OnServerError(netMsg.conn, NetworkManager.s_ErrorMessage.errorCode);
		}

		// Token: 0x06000347 RID: 839 RVA: 0x0000F948 File Offset: 0x0000DB48
		internal void OnClientConnectInternal(NetworkMessage netMsg)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkManager:OnClientConnectInternal");
			}
			netMsg.conn.SetMaxDelay(this.m_MaxDelay);
			string name = SceneManager.GetSceneAt(0).name;
			if (string.IsNullOrEmpty(this.m_OnlineScene) || this.m_OnlineScene == this.m_OfflineScene || name == this.m_OnlineScene)
			{
				this.m_ClientLoadedScene = false;
				this.OnClientConnect(netMsg.conn);
			}
			else
			{
				NetworkManager.s_ClientReadyConnection = netMsg.conn;
			}
		}

		// Token: 0x06000348 RID: 840 RVA: 0x0000F9EC File Offset: 0x0000DBEC
		internal void OnClientDisconnectInternal(NetworkMessage netMsg)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkManager:OnClientDisconnectInternal");
			}
			if (this.m_MigrationManager != null)
			{
				if (this.m_MigrationManager.LostHostOnClient(netMsg.conn))
				{
					return;
				}
			}
			if (!string.IsNullOrEmpty(this.m_OfflineScene))
			{
				this.ClientChangeScene(this.m_OfflineScene, false);
			}
			if (this.matchMaker != null && this.matchInfo != null && this.matchInfo.networkId != NetworkID.Invalid && this.matchInfo.nodeId != NodeID.Invalid)
			{
				this.matchMaker.DropConnection(this.matchInfo.networkId, this.matchInfo.nodeId, this.matchInfo.domain, new NetworkMatch.BasicResponseDelegate(this.OnDropConnection));
			}
			this.OnClientDisconnect(netMsg.conn);
		}

		// Token: 0x06000349 RID: 841 RVA: 0x0000FAE4 File Offset: 0x0000DCE4
		internal void OnClientNotReadyMessageInternal(NetworkMessage netMsg)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkManager:OnClientNotReadyMessageInternal");
			}
			ClientScene.SetNotReady();
			this.OnClientNotReady(netMsg.conn);
		}

		// Token: 0x0600034A RID: 842 RVA: 0x0000FB0E File Offset: 0x0000DD0E
		internal void OnClientErrorInternal(NetworkMessage netMsg)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkManager:OnClientErrorInternal");
			}
			netMsg.ReadMessage<ErrorMessage>(NetworkManager.s_ErrorMessage);
			this.OnClientError(netMsg.conn, NetworkManager.s_ErrorMessage.errorCode);
		}

		// Token: 0x0600034B RID: 843 RVA: 0x0000FB48 File Offset: 0x0000DD48
		internal void OnClientSceneInternal(NetworkMessage netMsg)
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkManager:OnClientSceneInternal");
			}
			string newSceneName = netMsg.reader.ReadString();
			if (this.IsClientConnected() && !NetworkServer.active)
			{
				this.ClientChangeScene(newSceneName, true);
			}
		}

		// Token: 0x0600034C RID: 844 RVA: 0x0000FB97 File Offset: 0x0000DD97
		public virtual void OnServerConnect(NetworkConnection conn)
		{
		}

		// Token: 0x0600034D RID: 845 RVA: 0x0000FB9A File Offset: 0x0000DD9A
		public virtual void OnServerDisconnect(NetworkConnection conn)
		{
			NetworkServer.DestroyPlayersForConnection(conn);
			if (conn.lastError != NetworkError.Ok)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("ServerDisconnected due to error: " + conn.lastError);
				}
			}
		}

		// Token: 0x0600034E RID: 846 RVA: 0x0000FBD6 File Offset: 0x0000DDD6
		public virtual void OnServerReady(NetworkConnection conn)
		{
			if (conn.playerControllers.Count == 0)
			{
				if (LogFilter.logDebug)
				{
					Debug.Log("Ready with no player object");
				}
			}
			NetworkServer.SetClientReady(conn);
		}

		// Token: 0x0600034F RID: 847 RVA: 0x0000FC07 File Offset: 0x0000DE07
		public virtual void OnServerAddPlayer(NetworkConnection conn, short playerControllerId, NetworkReader extraMessageReader)
		{
			this.OnServerAddPlayerInternal(conn, playerControllerId);
		}

		// Token: 0x06000350 RID: 848 RVA: 0x0000FC12 File Offset: 0x0000DE12
		public virtual void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
		{
			this.OnServerAddPlayerInternal(conn, playerControllerId);
		}

		// Token: 0x06000351 RID: 849 RVA: 0x0000FC20 File Offset: 0x0000DE20
		private void OnServerAddPlayerInternal(NetworkConnection conn, short playerControllerId)
		{
			if (this.m_PlayerPrefab == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("The PlayerPrefab is empty on the NetworkManager. Please setup a PlayerPrefab object.");
				}
			}
			else if (this.m_PlayerPrefab.GetComponent<NetworkIdentity>() == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("The PlayerPrefab does not have a NetworkIdentity. Please add a NetworkIdentity to the player prefab.");
				}
			}
			else if ((int)playerControllerId < conn.playerControllers.Count && conn.playerControllers[(int)playerControllerId].IsValid && conn.playerControllers[(int)playerControllerId].gameObject != null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("There is already a player at that playerControllerId for this connections.");
				}
			}
			else
			{
				Transform startPosition = this.GetStartPosition();
				GameObject player;
				if (startPosition != null)
				{
					player = Object.Instantiate<GameObject>(this.m_PlayerPrefab, startPosition.position, startPosition.rotation);
				}
				else
				{
					player = Object.Instantiate<GameObject>(this.m_PlayerPrefab, Vector3.zero, Quaternion.identity);
				}
				NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
			}
		}

		// Token: 0x06000352 RID: 850 RVA: 0x0000FD40 File Offset: 0x0000DF40
		public Transform GetStartPosition()
		{
			if (NetworkManager.s_StartPositions.Count > 0)
			{
				for (int i = NetworkManager.s_StartPositions.Count - 1; i >= 0; i--)
				{
					if (NetworkManager.s_StartPositions[i] == null)
					{
						NetworkManager.s_StartPositions.RemoveAt(i);
					}
				}
			}
			Transform result;
			if (this.m_PlayerSpawnMethod == PlayerSpawnMethod.Random && NetworkManager.s_StartPositions.Count > 0)
			{
				int index = Random.Range(0, NetworkManager.s_StartPositions.Count);
				result = NetworkManager.s_StartPositions[index];
			}
			else if (this.m_PlayerSpawnMethod == PlayerSpawnMethod.RoundRobin && NetworkManager.s_StartPositions.Count > 0)
			{
				if (NetworkManager.s_StartPositionIndex >= NetworkManager.s_StartPositions.Count)
				{
					NetworkManager.s_StartPositionIndex = 0;
				}
				Transform transform = NetworkManager.s_StartPositions[NetworkManager.s_StartPositionIndex];
				NetworkManager.s_StartPositionIndex++;
				result = transform;
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06000353 RID: 851 RVA: 0x0000FE42 File Offset: 0x0000E042
		public virtual void OnServerRemovePlayer(NetworkConnection conn, PlayerController player)
		{
			if (player.gameObject != null)
			{
				NetworkServer.Destroy(player.gameObject);
			}
		}

		// Token: 0x06000354 RID: 852 RVA: 0x0000FE63 File Offset: 0x0000E063
		public virtual void OnServerError(NetworkConnection conn, int errorCode)
		{
		}

		// Token: 0x06000355 RID: 853 RVA: 0x0000FE66 File Offset: 0x0000E066
		public virtual void OnServerSceneChanged(string sceneName)
		{
		}

		// Token: 0x06000356 RID: 854 RVA: 0x0000FE69 File Offset: 0x0000E069
		public virtual void OnClientConnect(NetworkConnection conn)
		{
			if (!this.clientLoadedScene)
			{
				ClientScene.Ready(conn);
				if (this.m_AutoCreatePlayer)
				{
					ClientScene.AddPlayer(0);
				}
			}
		}

		// Token: 0x06000357 RID: 855 RVA: 0x0000FE94 File Offset: 0x0000E094
		public virtual void OnClientDisconnect(NetworkConnection conn)
		{
			this.StopClient();
			if (conn.lastError != NetworkError.Ok)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("ClientDisconnected due to error: " + conn.lastError);
				}
			}
		}

		// Token: 0x06000358 RID: 856 RVA: 0x0000FED0 File Offset: 0x0000E0D0
		public virtual void OnClientError(NetworkConnection conn, int errorCode)
		{
		}

		// Token: 0x06000359 RID: 857 RVA: 0x0000FED3 File Offset: 0x0000E0D3
		public virtual void OnClientNotReady(NetworkConnection conn)
		{
		}

		// Token: 0x0600035A RID: 858 RVA: 0x0000FED8 File Offset: 0x0000E0D8
		public virtual void OnClientSceneChanged(NetworkConnection conn)
		{
			ClientScene.Ready(conn);
			if (this.m_AutoCreatePlayer)
			{
				bool flag = ClientScene.localPlayers.Count == 0;
				bool flag2 = false;
				for (int i = 0; i < ClientScene.localPlayers.Count; i++)
				{
					if (ClientScene.localPlayers[i].gameObject != null)
					{
						flag2 = true;
						break;
					}
				}
				if (!flag2)
				{
					flag = true;
				}
				if (flag)
				{
					ClientScene.AddPlayer(0);
				}
			}
		}

		// Token: 0x0600035B RID: 859 RVA: 0x0000FF67 File Offset: 0x0000E167
		public void StartMatchMaker()
		{
			if (LogFilter.logDebug)
			{
				Debug.Log("NetworkManager StartMatchMaker");
			}
			this.SetMatchHost(this.m_MatchHost, this.m_MatchPort, this.m_MatchPort == 443);
		}

		// Token: 0x0600035C RID: 860 RVA: 0x0000FFA0 File Offset: 0x0000E1A0
		public void StopMatchMaker()
		{
			if (this.matchMaker != null && this.matchInfo != null && this.matchInfo.networkId != NetworkID.Invalid && this.matchInfo.nodeId != NodeID.Invalid)
			{
				this.matchMaker.DropConnection(this.matchInfo.networkId, this.matchInfo.nodeId, this.matchInfo.domain, new NetworkMatch.BasicResponseDelegate(this.OnDropConnection));
			}
			if (this.matchMaker != null)
			{
				Object.Destroy(this.matchMaker);
				this.matchMaker = null;
			}
			this.matchInfo = null;
			this.matches = null;
		}

		// Token: 0x0600035D RID: 861 RVA: 0x0001005C File Offset: 0x0000E25C
		public void SetMatchHost(string newHost, int port, bool https)
		{
			if (this.matchMaker == null)
			{
				this.matchMaker = base.gameObject.AddComponent<NetworkMatch>();
			}
			if (newHost == "127.0.0.1")
			{
				newHost = "localhost";
			}
			string text = "http://";
			if (https)
			{
				text = "https://";
			}
			if (newHost.StartsWith("http://"))
			{
				newHost = newHost.Replace("http://", "");
			}
			if (newHost.StartsWith("https://"))
			{
				newHost = newHost.Replace("https://", "");
			}
			this.m_MatchHost = newHost;
			this.m_MatchPort = port;
			string text2 = string.Concat(new object[]
			{
				text,
				this.m_MatchHost,
				":",
				this.m_MatchPort
			});
			if (LogFilter.logDebug)
			{
				Debug.Log("SetMatchHost:" + text2);
			}
			this.matchMaker.baseUri = new Uri(text2);
		}

		// Token: 0x0600035E RID: 862 RVA: 0x0001016D File Offset: 0x0000E36D
		public virtual void OnStartHost()
		{
		}

		// Token: 0x0600035F RID: 863 RVA: 0x00010170 File Offset: 0x0000E370
		public virtual void OnStartServer()
		{
		}

		// Token: 0x06000360 RID: 864 RVA: 0x00010173 File Offset: 0x0000E373
		public virtual void OnStartClient(NetworkClient client)
		{
		}

		// Token: 0x06000361 RID: 865 RVA: 0x00010176 File Offset: 0x0000E376
		public virtual void OnStopServer()
		{
		}

		// Token: 0x06000362 RID: 866 RVA: 0x00010179 File Offset: 0x0000E379
		public virtual void OnStopClient()
		{
		}

		// Token: 0x06000363 RID: 867 RVA: 0x0001017C File Offset: 0x0000E37C
		public virtual void OnStopHost()
		{
		}

		// Token: 0x06000364 RID: 868 RVA: 0x0001017F File Offset: 0x0000E37F
		public virtual void OnMatchCreate(bool success, string extendedInfo, MatchInfo matchInfo)
		{
			if (LogFilter.logDebug)
			{
				Debug.LogFormat("NetworkManager OnMatchCreate Success:{0}, ExtendedInfo:{1}, matchInfo:{2}", new object[]
				{
					success,
					extendedInfo,
					matchInfo
				});
			}
			if (success)
			{
				this.StartHost(matchInfo);
			}
		}

		// Token: 0x06000365 RID: 869 RVA: 0x000101C0 File Offset: 0x0000E3C0
		public virtual void OnMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matchList)
		{
			if (LogFilter.logDebug)
			{
				Debug.LogFormat("NetworkManager OnMatchList Success:{0}, ExtendedInfo:{1}, matchList.Count:{2}", new object[]
				{
					success,
					extendedInfo,
					matchList.Count
				});
			}
			this.matches = matchList;
		}

		// Token: 0x06000366 RID: 870 RVA: 0x0001020C File Offset: 0x0000E40C
		public virtual void OnMatchJoined(bool success, string extendedInfo, MatchInfo matchInfo)
		{
			if (LogFilter.logDebug)
			{
				Debug.LogFormat("NetworkManager OnMatchJoined Success:{0}, ExtendedInfo:{1}, matchInfo:{2}", new object[]
				{
					success,
					extendedInfo,
					matchInfo
				});
			}
			if (success)
			{
				this.StartClient(matchInfo);
			}
		}

		// Token: 0x06000367 RID: 871 RVA: 0x0001024A File Offset: 0x0000E44A
		public virtual void OnDestroyMatch(bool success, string extendedInfo)
		{
			if (LogFilter.logDebug)
			{
				Debug.LogFormat("NetworkManager OnDestroyMatch Success:{0}, ExtendedInfo:{1}", new object[]
				{
					success,
					extendedInfo
				});
			}
		}

		// Token: 0x06000368 RID: 872 RVA: 0x00010276 File Offset: 0x0000E476
		public virtual void OnDropConnection(bool success, string extendedInfo)
		{
			if (LogFilter.logDebug)
			{
				Debug.LogFormat("NetworkManager OnDropConnection Success:{0}, ExtendedInfo:{1}", new object[]
				{
					success,
					extendedInfo
				});
			}
		}

		// Token: 0x06000369 RID: 873 RVA: 0x000102A2 File Offset: 0x0000E4A2
		public virtual void OnSetMatchAttributes(bool success, string extendedInfo)
		{
			if (LogFilter.logDebug)
			{
				Debug.LogFormat("NetworkManager OnSetMatchAttributes Success:{0}, ExtendedInfo:{1}", new object[]
				{
					success,
					extendedInfo
				});
			}
		}

		// Token: 0x0600036A RID: 874 RVA: 0x000102CE File Offset: 0x0000E4CE
		// Note: this type is marked as 'beforefieldinit'.
		static NetworkManager()
		{
		}

		// Token: 0x04000159 RID: 345
		[SerializeField]
		private int m_NetworkPort = 7777;

		// Token: 0x0400015A RID: 346
		[SerializeField]
		private bool m_ServerBindToIP;

		// Token: 0x0400015B RID: 347
		[SerializeField]
		private string m_ServerBindAddress = "";

		// Token: 0x0400015C RID: 348
		[SerializeField]
		private string m_NetworkAddress = "localhost";

		// Token: 0x0400015D RID: 349
		[SerializeField]
		private bool m_DontDestroyOnLoad = true;

		// Token: 0x0400015E RID: 350
		[SerializeField]
		private bool m_RunInBackground = true;

		// Token: 0x0400015F RID: 351
		[SerializeField]
		private bool m_ScriptCRCCheck = true;

		// Token: 0x04000160 RID: 352
		[SerializeField]
		private float m_MaxDelay = 0.01f;

		// Token: 0x04000161 RID: 353
		[SerializeField]
		private LogFilter.FilterLevel m_LogLevel = LogFilter.FilterLevel.Info;

		// Token: 0x04000162 RID: 354
		[SerializeField]
		private GameObject m_PlayerPrefab;

		// Token: 0x04000163 RID: 355
		[SerializeField]
		private bool m_AutoCreatePlayer = true;

		// Token: 0x04000164 RID: 356
		[SerializeField]
		private PlayerSpawnMethod m_PlayerSpawnMethod;

		// Token: 0x04000165 RID: 357
		[SerializeField]
		private string m_OfflineScene = "";

		// Token: 0x04000166 RID: 358
		[SerializeField]
		private string m_OnlineScene = "";

		// Token: 0x04000167 RID: 359
		[SerializeField]
		private List<GameObject> m_SpawnPrefabs = new List<GameObject>();

		// Token: 0x04000168 RID: 360
		[SerializeField]
		private bool m_CustomConfig;

		// Token: 0x04000169 RID: 361
		[SerializeField]
		private int m_MaxConnections = 4;

		// Token: 0x0400016A RID: 362
		[SerializeField]
		private ConnectionConfig m_ConnectionConfig;

		// Token: 0x0400016B RID: 363
		[SerializeField]
		private GlobalConfig m_GlobalConfig;

		// Token: 0x0400016C RID: 364
		[SerializeField]
		private List<QosType> m_Channels = new List<QosType>();

		// Token: 0x0400016D RID: 365
		[SerializeField]
		private bool m_UseWebSockets;

		// Token: 0x0400016E RID: 366
		[SerializeField]
		private bool m_UseSimulator;

		// Token: 0x0400016F RID: 367
		[SerializeField]
		private int m_SimulatedLatency = 1;

		// Token: 0x04000170 RID: 368
		[SerializeField]
		private float m_PacketLossPercentage;

		// Token: 0x04000171 RID: 369
		[SerializeField]
		private int m_MaxBufferedPackets = 16;

		// Token: 0x04000172 RID: 370
		[SerializeField]
		private bool m_AllowFragmentation = true;

		// Token: 0x04000173 RID: 371
		[SerializeField]
		private string m_MatchHost = "mm.unet.unity3d.com";

		// Token: 0x04000174 RID: 372
		[SerializeField]
		private int m_MatchPort = 443;

		// Token: 0x04000175 RID: 373
		[SerializeField]
		public string matchName = "default";

		// Token: 0x04000176 RID: 374
		[SerializeField]
		public uint matchSize = 4U;

		// Token: 0x04000177 RID: 375
		private NetworkMigrationManager m_MigrationManager;

		// Token: 0x04000178 RID: 376
		private EndPoint m_EndPoint;

		// Token: 0x04000179 RID: 377
		private bool m_ClientLoadedScene;

		// Token: 0x0400017A RID: 378
		private static INetworkTransport s_ActiveTransport = new DefaultNetworkTransport();

		// Token: 0x0400017B RID: 379
		public static string networkSceneName = "";

		// Token: 0x0400017C RID: 380
		public bool isNetworkActive;

		// Token: 0x0400017D RID: 381
		public NetworkClient client;

		// Token: 0x0400017E RID: 382
		private static List<Transform> s_StartPositions = new List<Transform>();

		// Token: 0x0400017F RID: 383
		private static int s_StartPositionIndex;

		// Token: 0x04000180 RID: 384
		public MatchInfo matchInfo;

		// Token: 0x04000181 RID: 385
		public NetworkMatch matchMaker;

		// Token: 0x04000182 RID: 386
		public List<MatchInfoSnapshot> matches;

		// Token: 0x04000183 RID: 387
		public static NetworkManager singleton;

		// Token: 0x04000184 RID: 388
		private static AddPlayerMessage s_AddPlayerMessage = new AddPlayerMessage();

		// Token: 0x04000185 RID: 389
		private static RemovePlayerMessage s_RemovePlayerMessage = new RemovePlayerMessage();

		// Token: 0x04000186 RID: 390
		private static ErrorMessage s_ErrorMessage = new ErrorMessage();

		// Token: 0x04000187 RID: 391
		private static AsyncOperation s_LoadingSceneAsync;

		// Token: 0x04000188 RID: 392
		private static NetworkConnection s_ClientReadyConnection;

		// Token: 0x04000189 RID: 393
		private static string s_Address;
	}
}
