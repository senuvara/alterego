﻿using System;
using System.Text;

namespace UnityEngine.Networking
{
	// Token: 0x02000057 RID: 87
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class NetworkReader
	{
		// Token: 0x060003AE RID: 942 RVA: 0x00014268 File Offset: 0x00012468
		public NetworkReader()
		{
			this.m_buf = new NetBuffer();
			NetworkReader.Initialize();
		}

		// Token: 0x060003AF RID: 943 RVA: 0x00014281 File Offset: 0x00012481
		public NetworkReader(NetworkWriter writer)
		{
			this.m_buf = new NetBuffer(writer.AsArray());
			NetworkReader.Initialize();
		}

		// Token: 0x060003B0 RID: 944 RVA: 0x000142A0 File Offset: 0x000124A0
		public NetworkReader(byte[] buffer)
		{
			this.m_buf = new NetBuffer(buffer);
			NetworkReader.Initialize();
		}

		// Token: 0x060003B1 RID: 945 RVA: 0x000142BA File Offset: 0x000124BA
		private static void Initialize()
		{
			if (NetworkReader.s_Encoding == null)
			{
				NetworkReader.s_StringReaderBuffer = new byte[1024];
				NetworkReader.s_Encoding = new UTF8Encoding();
			}
		}

		// Token: 0x1700009C RID: 156
		// (get) Token: 0x060003B2 RID: 946 RVA: 0x000142E4 File Offset: 0x000124E4
		public uint Position
		{
			get
			{
				return this.m_buf.Position;
			}
		}

		// Token: 0x1700009D RID: 157
		// (get) Token: 0x060003B3 RID: 947 RVA: 0x00014304 File Offset: 0x00012504
		public int Length
		{
			get
			{
				return this.m_buf.Length;
			}
		}

		// Token: 0x060003B4 RID: 948 RVA: 0x00014324 File Offset: 0x00012524
		public void SeekZero()
		{
			this.m_buf.SeekZero();
		}

		// Token: 0x060003B5 RID: 949 RVA: 0x00014332 File Offset: 0x00012532
		internal void Replace(byte[] buffer)
		{
			this.m_buf.Replace(buffer);
		}

		// Token: 0x060003B6 RID: 950 RVA: 0x00014344 File Offset: 0x00012544
		public uint ReadPackedUInt32()
		{
			byte b = this.ReadByte();
			uint result;
			if (b < 241)
			{
				result = (uint)b;
			}
			else
			{
				byte b2 = this.ReadByte();
				if (b >= 241 && b <= 248)
				{
					result = 240U + 256U * (uint)(b - 241) + (uint)b2;
				}
				else
				{
					byte b3 = this.ReadByte();
					if (b == 249)
					{
						result = 2288U + 256U * (uint)b2 + (uint)b3;
					}
					else
					{
						byte b4 = this.ReadByte();
						if (b == 250)
						{
							result = (uint)((int)b2 + ((int)b3 << 8) + ((int)b4 << 16));
						}
						else
						{
							byte b5 = this.ReadByte();
							if (b < 251)
							{
								throw new IndexOutOfRangeException("ReadPackedUInt32() failure: " + b);
							}
							result = (uint)((int)b2 + ((int)b3 << 8) + ((int)b4 << 16) + ((int)b5 << 24));
						}
					}
				}
			}
			return result;
		}

		// Token: 0x060003B7 RID: 951 RVA: 0x00014434 File Offset: 0x00012634
		public ulong ReadPackedUInt64()
		{
			byte b = this.ReadByte();
			ulong result;
			if (b < 241)
			{
				result = (ulong)b;
			}
			else
			{
				byte b2 = this.ReadByte();
				if (b >= 241 && b <= 248)
				{
					result = 240UL + 256UL * ((ulong)b - 241UL) + (ulong)b2;
				}
				else
				{
					byte b3 = this.ReadByte();
					if (b == 249)
					{
						result = 2288UL + 256UL * (ulong)b2 + (ulong)b3;
					}
					else
					{
						byte b4 = this.ReadByte();
						if (b == 250)
						{
							result = (ulong)b2 + ((ulong)b3 << 8) + ((ulong)b4 << 16);
						}
						else
						{
							byte b5 = this.ReadByte();
							if (b == 251)
							{
								result = (ulong)b2 + ((ulong)b3 << 8) + ((ulong)b4 << 16) + ((ulong)b5 << 24);
							}
							else
							{
								byte b6 = this.ReadByte();
								if (b == 252)
								{
									result = (ulong)b2 + ((ulong)b3 << 8) + ((ulong)b4 << 16) + ((ulong)b5 << 24) + ((ulong)b6 << 32);
								}
								else
								{
									byte b7 = this.ReadByte();
									if (b == 253)
									{
										result = (ulong)b2 + ((ulong)b3 << 8) + ((ulong)b4 << 16) + ((ulong)b5 << 24) + ((ulong)b6 << 32) + ((ulong)b7 << 40);
									}
									else
									{
										byte b8 = this.ReadByte();
										if (b == 254)
										{
											result = (ulong)b2 + ((ulong)b3 << 8) + ((ulong)b4 << 16) + ((ulong)b5 << 24) + ((ulong)b6 << 32) + ((ulong)b7 << 40) + ((ulong)b8 << 48);
										}
										else
										{
											byte b9 = this.ReadByte();
											if (b != 255)
											{
												throw new IndexOutOfRangeException("ReadPackedUInt64() failure: " + b);
											}
											result = (ulong)b2 + ((ulong)b3 << 8) + ((ulong)b4 << 16) + ((ulong)b5 << 24) + ((ulong)b6 << 32) + ((ulong)b7 << 40) + ((ulong)b8 << 48) + ((ulong)b9 << 56);
										}
									}
								}
							}
						}
					}
				}
			}
			return result;
		}

		// Token: 0x060003B8 RID: 952 RVA: 0x00014638 File Offset: 0x00012838
		public NetworkInstanceId ReadNetworkId()
		{
			return new NetworkInstanceId(this.ReadPackedUInt32());
		}

		// Token: 0x060003B9 RID: 953 RVA: 0x00014658 File Offset: 0x00012858
		public NetworkSceneId ReadSceneId()
		{
			return new NetworkSceneId(this.ReadPackedUInt32());
		}

		// Token: 0x060003BA RID: 954 RVA: 0x00014678 File Offset: 0x00012878
		public byte ReadByte()
		{
			return this.m_buf.ReadByte();
		}

		// Token: 0x060003BB RID: 955 RVA: 0x00014698 File Offset: 0x00012898
		public sbyte ReadSByte()
		{
			return (sbyte)this.m_buf.ReadByte();
		}

		// Token: 0x060003BC RID: 956 RVA: 0x000146BC File Offset: 0x000128BC
		public short ReadInt16()
		{
			ushort num = 0;
			num |= (ushort)this.m_buf.ReadByte();
			num |= (ushort)(this.m_buf.ReadByte() << 8);
			return (short)num;
		}

		// Token: 0x060003BD RID: 957 RVA: 0x000146F8 File Offset: 0x000128F8
		public ushort ReadUInt16()
		{
			ushort num = 0;
			num |= (ushort)this.m_buf.ReadByte();
			return num | (ushort)(this.m_buf.ReadByte() << 8);
		}

		// Token: 0x060003BE RID: 958 RVA: 0x00014734 File Offset: 0x00012934
		public int ReadInt32()
		{
			uint num = 0U;
			num |= (uint)this.m_buf.ReadByte();
			num |= (uint)((uint)this.m_buf.ReadByte() << 8);
			num |= (uint)((uint)this.m_buf.ReadByte() << 16);
			return (int)(num | (uint)((uint)this.m_buf.ReadByte() << 24));
		}

		// Token: 0x060003BF RID: 959 RVA: 0x0001478C File Offset: 0x0001298C
		public uint ReadUInt32()
		{
			uint num = 0U;
			num |= (uint)this.m_buf.ReadByte();
			num |= (uint)((uint)this.m_buf.ReadByte() << 8);
			num |= (uint)((uint)this.m_buf.ReadByte() << 16);
			return num | (uint)((uint)this.m_buf.ReadByte() << 24);
		}

		// Token: 0x060003C0 RID: 960 RVA: 0x000147E4 File Offset: 0x000129E4
		public long ReadInt64()
		{
			ulong num = 0UL;
			ulong num2 = (ulong)this.m_buf.ReadByte();
			num |= num2;
			num2 = (ulong)this.m_buf.ReadByte() << 8;
			num |= num2;
			num2 = (ulong)this.m_buf.ReadByte() << 16;
			num |= num2;
			num2 = (ulong)this.m_buf.ReadByte() << 24;
			num |= num2;
			num2 = (ulong)this.m_buf.ReadByte() << 32;
			num |= num2;
			num2 = (ulong)this.m_buf.ReadByte() << 40;
			num |= num2;
			num2 = (ulong)this.m_buf.ReadByte() << 48;
			num |= num2;
			num2 = (ulong)this.m_buf.ReadByte() << 56;
			return (long)(num | num2);
		}

		// Token: 0x060003C1 RID: 961 RVA: 0x0001489C File Offset: 0x00012A9C
		public ulong ReadUInt64()
		{
			ulong num = 0UL;
			ulong num2 = (ulong)this.m_buf.ReadByte();
			num |= num2;
			num2 = (ulong)this.m_buf.ReadByte() << 8;
			num |= num2;
			num2 = (ulong)this.m_buf.ReadByte() << 16;
			num |= num2;
			num2 = (ulong)this.m_buf.ReadByte() << 24;
			num |= num2;
			num2 = (ulong)this.m_buf.ReadByte() << 32;
			num |= num2;
			num2 = (ulong)this.m_buf.ReadByte() << 40;
			num |= num2;
			num2 = (ulong)this.m_buf.ReadByte() << 48;
			num |= num2;
			num2 = (ulong)this.m_buf.ReadByte() << 56;
			return num | num2;
		}

		// Token: 0x060003C2 RID: 962 RVA: 0x00014954 File Offset: 0x00012B54
		public decimal ReadDecimal()
		{
			return new decimal(new int[]
			{
				this.ReadInt32(),
				this.ReadInt32(),
				this.ReadInt32(),
				this.ReadInt32()
			});
		}

		// Token: 0x060003C3 RID: 963 RVA: 0x0001499C File Offset: 0x00012B9C
		public float ReadSingle()
		{
			uint value = this.ReadUInt32();
			return FloatConversion.ToSingle(value);
		}

		// Token: 0x060003C4 RID: 964 RVA: 0x000149C0 File Offset: 0x00012BC0
		public double ReadDouble()
		{
			ulong value = this.ReadUInt64();
			return FloatConversion.ToDouble(value);
		}

		// Token: 0x060003C5 RID: 965 RVA: 0x000149E4 File Offset: 0x00012BE4
		public string ReadString()
		{
			ushort num = this.ReadUInt16();
			string result;
			if (num == 0)
			{
				result = "";
			}
			else
			{
				if (num >= 32768)
				{
					throw new IndexOutOfRangeException("ReadString() too long: " + num);
				}
				while ((int)num > NetworkReader.s_StringReaderBuffer.Length)
				{
					NetworkReader.s_StringReaderBuffer = new byte[NetworkReader.s_StringReaderBuffer.Length * 2];
				}
				this.m_buf.ReadBytes(NetworkReader.s_StringReaderBuffer, (uint)num);
				char[] chars = NetworkReader.s_Encoding.GetChars(NetworkReader.s_StringReaderBuffer, 0, (int)num);
				result = new string(chars);
			}
			return result;
		}

		// Token: 0x060003C6 RID: 966 RVA: 0x00014A84 File Offset: 0x00012C84
		public char ReadChar()
		{
			return (char)this.m_buf.ReadByte();
		}

		// Token: 0x060003C7 RID: 967 RVA: 0x00014AA8 File Offset: 0x00012CA8
		public bool ReadBoolean()
		{
			int num = (int)this.m_buf.ReadByte();
			return num == 1;
		}

		// Token: 0x060003C8 RID: 968 RVA: 0x00014AD0 File Offset: 0x00012CD0
		public byte[] ReadBytes(int count)
		{
			if (count < 0)
			{
				throw new IndexOutOfRangeException("NetworkReader ReadBytes " + count);
			}
			byte[] array = new byte[count];
			this.m_buf.ReadBytes(array, (uint)count);
			return array;
		}

		// Token: 0x060003C9 RID: 969 RVA: 0x00014B18 File Offset: 0x00012D18
		public byte[] ReadBytesAndSize()
		{
			ushort num = this.ReadUInt16();
			byte[] result;
			if (num == 0)
			{
				result = new byte[0];
			}
			else
			{
				result = this.ReadBytes((int)num);
			}
			return result;
		}

		// Token: 0x060003CA RID: 970 RVA: 0x00014B50 File Offset: 0x00012D50
		public Vector2 ReadVector2()
		{
			return new Vector2(this.ReadSingle(), this.ReadSingle());
		}

		// Token: 0x060003CB RID: 971 RVA: 0x00014B78 File Offset: 0x00012D78
		public Vector3 ReadVector3()
		{
			return new Vector3(this.ReadSingle(), this.ReadSingle(), this.ReadSingle());
		}

		// Token: 0x060003CC RID: 972 RVA: 0x00014BA4 File Offset: 0x00012DA4
		public Vector4 ReadVector4()
		{
			return new Vector4(this.ReadSingle(), this.ReadSingle(), this.ReadSingle(), this.ReadSingle());
		}

		// Token: 0x060003CD RID: 973 RVA: 0x00014BD8 File Offset: 0x00012DD8
		public Color ReadColor()
		{
			return new Color(this.ReadSingle(), this.ReadSingle(), this.ReadSingle(), this.ReadSingle());
		}

		// Token: 0x060003CE RID: 974 RVA: 0x00014C0C File Offset: 0x00012E0C
		public Color32 ReadColor32()
		{
			return new Color32(this.ReadByte(), this.ReadByte(), this.ReadByte(), this.ReadByte());
		}

		// Token: 0x060003CF RID: 975 RVA: 0x00014C40 File Offset: 0x00012E40
		public Quaternion ReadQuaternion()
		{
			return new Quaternion(this.ReadSingle(), this.ReadSingle(), this.ReadSingle(), this.ReadSingle());
		}

		// Token: 0x060003D0 RID: 976 RVA: 0x00014C74 File Offset: 0x00012E74
		public Rect ReadRect()
		{
			return new Rect(this.ReadSingle(), this.ReadSingle(), this.ReadSingle(), this.ReadSingle());
		}

		// Token: 0x060003D1 RID: 977 RVA: 0x00014CA8 File Offset: 0x00012EA8
		public Plane ReadPlane()
		{
			return new Plane(this.ReadVector3(), this.ReadSingle());
		}

		// Token: 0x060003D2 RID: 978 RVA: 0x00014CD0 File Offset: 0x00012ED0
		public Ray ReadRay()
		{
			return new Ray(this.ReadVector3(), this.ReadVector3());
		}

		// Token: 0x060003D3 RID: 979 RVA: 0x00014CF8 File Offset: 0x00012EF8
		public Matrix4x4 ReadMatrix4x4()
		{
			return new Matrix4x4
			{
				m00 = this.ReadSingle(),
				m01 = this.ReadSingle(),
				m02 = this.ReadSingle(),
				m03 = this.ReadSingle(),
				m10 = this.ReadSingle(),
				m11 = this.ReadSingle(),
				m12 = this.ReadSingle(),
				m13 = this.ReadSingle(),
				m20 = this.ReadSingle(),
				m21 = this.ReadSingle(),
				m22 = this.ReadSingle(),
				m23 = this.ReadSingle(),
				m30 = this.ReadSingle(),
				m31 = this.ReadSingle(),
				m32 = this.ReadSingle(),
				m33 = this.ReadSingle()
			};
		}

		// Token: 0x060003D4 RID: 980 RVA: 0x00014DE8 File Offset: 0x00012FE8
		public NetworkHash128 ReadNetworkHash128()
		{
			NetworkHash128 result;
			result.i0 = this.ReadByte();
			result.i1 = this.ReadByte();
			result.i2 = this.ReadByte();
			result.i3 = this.ReadByte();
			result.i4 = this.ReadByte();
			result.i5 = this.ReadByte();
			result.i6 = this.ReadByte();
			result.i7 = this.ReadByte();
			result.i8 = this.ReadByte();
			result.i9 = this.ReadByte();
			result.i10 = this.ReadByte();
			result.i11 = this.ReadByte();
			result.i12 = this.ReadByte();
			result.i13 = this.ReadByte();
			result.i14 = this.ReadByte();
			result.i15 = this.ReadByte();
			return result;
		}

		// Token: 0x060003D5 RID: 981 RVA: 0x00014ED0 File Offset: 0x000130D0
		public Transform ReadTransform()
		{
			NetworkInstanceId networkInstanceId = this.ReadNetworkId();
			Transform result;
			if (networkInstanceId.IsEmpty())
			{
				result = null;
			}
			else
			{
				GameObject gameObject = ClientScene.FindLocalObject(networkInstanceId);
				if (gameObject == null)
				{
					if (LogFilter.logDebug)
					{
						Debug.Log("ReadTransform netId:" + networkInstanceId);
					}
					result = null;
				}
				else
				{
					result = gameObject.transform;
				}
			}
			return result;
		}

		// Token: 0x060003D6 RID: 982 RVA: 0x00014F44 File Offset: 0x00013144
		public GameObject ReadGameObject()
		{
			NetworkInstanceId networkInstanceId = this.ReadNetworkId();
			GameObject result;
			if (networkInstanceId.IsEmpty())
			{
				result = null;
			}
			else
			{
				GameObject gameObject;
				if (NetworkServer.active)
				{
					gameObject = NetworkServer.FindLocalObject(networkInstanceId);
				}
				else
				{
					gameObject = ClientScene.FindLocalObject(networkInstanceId);
				}
				if (gameObject == null)
				{
					if (LogFilter.logDebug)
					{
						Debug.Log("ReadGameObject netId:" + networkInstanceId + "go: null");
					}
				}
				result = gameObject;
			}
			return result;
		}

		// Token: 0x060003D7 RID: 983 RVA: 0x00014FCC File Offset: 0x000131CC
		public NetworkIdentity ReadNetworkIdentity()
		{
			NetworkInstanceId networkInstanceId = this.ReadNetworkId();
			NetworkIdentity result;
			if (networkInstanceId.IsEmpty())
			{
				result = null;
			}
			else
			{
				GameObject gameObject;
				if (NetworkServer.active)
				{
					gameObject = NetworkServer.FindLocalObject(networkInstanceId);
				}
				else
				{
					gameObject = ClientScene.FindLocalObject(networkInstanceId);
				}
				if (gameObject == null)
				{
					if (LogFilter.logDebug)
					{
						Debug.Log("ReadNetworkIdentity netId:" + networkInstanceId + "go: null");
					}
					result = null;
				}
				else
				{
					result = gameObject.GetComponent<NetworkIdentity>();
				}
			}
			return result;
		}

		// Token: 0x060003D8 RID: 984 RVA: 0x00015060 File Offset: 0x00013260
		public override string ToString()
		{
			return this.m_buf.ToString();
		}

		// Token: 0x060003D9 RID: 985 RVA: 0x00015080 File Offset: 0x00013280
		public TMsg ReadMessage<TMsg>() where TMsg : MessageBase, new()
		{
			TMsg result = Activator.CreateInstance<TMsg>();
			result.Deserialize(this);
			return result;
		}

		// Token: 0x040001AF RID: 431
		private NetBuffer m_buf;

		// Token: 0x040001B0 RID: 432
		private const int k_MaxStringLength = 32768;

		// Token: 0x040001B1 RID: 433
		private const int k_InitialStringBufferSize = 1024;

		// Token: 0x040001B2 RID: 434
		private static byte[] s_StringReaderBuffer;

		// Token: 0x040001B3 RID: 435
		private static Encoding s_Encoding;
	}
}
