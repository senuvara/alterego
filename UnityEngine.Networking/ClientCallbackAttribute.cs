﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000010 RID: 16
	[AttributeUsage(AttributeTargets.Method)]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class ClientCallbackAttribute : Attribute
	{
		// Token: 0x0600006F RID: 111 RVA: 0x00004C9A File Offset: 0x00002E9A
		public ClientCallbackAttribute()
		{
		}
	}
}
