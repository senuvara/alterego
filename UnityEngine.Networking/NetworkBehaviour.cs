﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace UnityEngine.Networking
{
	// Token: 0x02000036 RID: 54
	[RequireComponent(typeof(NetworkIdentity))]
	[AddComponentMenu("")]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class NetworkBehaviour : MonoBehaviour
	{
		// Token: 0x06000136 RID: 310 RVA: 0x00008BA2 File Offset: 0x00006DA2
		public NetworkBehaviour()
		{
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000137 RID: 311 RVA: 0x00008BAC File Offset: 0x00006DAC
		public bool localPlayerAuthority
		{
			get
			{
				return this.myView.localPlayerAuthority;
			}
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x06000138 RID: 312 RVA: 0x00008BCC File Offset: 0x00006DCC
		public bool isServer
		{
			get
			{
				return this.myView.isServer;
			}
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x06000139 RID: 313 RVA: 0x00008BEC File Offset: 0x00006DEC
		public bool isClient
		{
			get
			{
				return this.myView.isClient;
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x0600013A RID: 314 RVA: 0x00008C0C File Offset: 0x00006E0C
		public bool isLocalPlayer
		{
			get
			{
				return this.myView.isLocalPlayer;
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x0600013B RID: 315 RVA: 0x00008C2C File Offset: 0x00006E2C
		public bool hasAuthority
		{
			get
			{
				return this.myView.hasAuthority;
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x0600013C RID: 316 RVA: 0x00008C4C File Offset: 0x00006E4C
		public NetworkInstanceId netId
		{
			get
			{
				return this.myView.netId;
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x0600013D RID: 317 RVA: 0x00008C6C File Offset: 0x00006E6C
		public NetworkConnection connectionToServer
		{
			get
			{
				return this.myView.connectionToServer;
			}
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x0600013E RID: 318 RVA: 0x00008C8C File Offset: 0x00006E8C
		public NetworkConnection connectionToClient
		{
			get
			{
				return this.myView.connectionToClient;
			}
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x0600013F RID: 319 RVA: 0x00008CAC File Offset: 0x00006EAC
		public short playerControllerId
		{
			get
			{
				return this.myView.playerControllerId;
			}
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x06000140 RID: 320 RVA: 0x00008CCC File Offset: 0x00006ECC
		protected uint syncVarDirtyBits
		{
			get
			{
				return this.m_SyncVarDirtyBits;
			}
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x06000141 RID: 321 RVA: 0x00008CE8 File Offset: 0x00006EE8
		// (set) Token: 0x06000142 RID: 322 RVA: 0x00008D03 File Offset: 0x00006F03
		protected bool syncVarHookGuard
		{
			get
			{
				return this.m_SyncVarGuard;
			}
			set
			{
				this.m_SyncVarGuard = value;
			}
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x06000143 RID: 323 RVA: 0x00008D10 File Offset: 0x00006F10
		internal NetworkIdentity netIdentity
		{
			get
			{
				return this.myView;
			}
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x06000144 RID: 324 RVA: 0x00008D2C File Offset: 0x00006F2C
		private NetworkIdentity myView
		{
			get
			{
				NetworkIdentity myView;
				if (this.m_MyView == null)
				{
					this.m_MyView = base.GetComponent<NetworkIdentity>();
					if (this.m_MyView == null)
					{
						if (LogFilter.logError)
						{
							Debug.LogError("There is no NetworkIdentity on this object. Please add one.");
						}
					}
					myView = this.m_MyView;
				}
				else
				{
					myView = this.m_MyView;
				}
				return myView;
			}
		}

		// Token: 0x06000145 RID: 325 RVA: 0x00008D9C File Offset: 0x00006F9C
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected void SendCommandInternal(NetworkWriter writer, int channelId, string cmdName)
		{
			if (!this.isLocalPlayer && !this.hasAuthority)
			{
				if (LogFilter.logWarn)
				{
					Debug.LogWarning("Trying to send command for object without authority.");
				}
			}
			else if (ClientScene.readyConnection == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("Send command attempted with no client running [client=" + this.connectionToServer + "].");
				}
			}
			else
			{
				writer.FinishMessage();
				ClientScene.readyConnection.SendWriter(writer, channelId);
			}
		}

		// Token: 0x06000146 RID: 326 RVA: 0x00008E28 File Offset: 0x00007028
		[EditorBrowsable(EditorBrowsableState.Never)]
		public virtual bool InvokeCommand(int cmdHash, NetworkReader reader)
		{
			return this.InvokeCommandDelegate(cmdHash, reader);
		}

		// Token: 0x06000147 RID: 327 RVA: 0x00008E53 File Offset: 0x00007053
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected void SendRPCInternal(NetworkWriter writer, int channelId, string rpcName)
		{
			if (!this.isServer)
			{
				if (LogFilter.logWarn)
				{
					Debug.LogWarning("ClientRpc call on un-spawned object");
				}
			}
			else
			{
				writer.FinishMessage();
				NetworkServer.SendWriterToReady(base.gameObject, writer, channelId);
			}
		}

		// Token: 0x06000148 RID: 328 RVA: 0x00008E90 File Offset: 0x00007090
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected void SendTargetRPCInternal(NetworkConnection conn, NetworkWriter writer, int channelId, string rpcName)
		{
			if (!this.isServer)
			{
				if (LogFilter.logWarn)
				{
					Debug.LogWarning("TargetRpc call on un-spawned object");
				}
			}
			else
			{
				writer.FinishMessage();
				conn.SendWriter(writer, channelId);
			}
		}

		// Token: 0x06000149 RID: 329 RVA: 0x00008ECC File Offset: 0x000070CC
		[EditorBrowsable(EditorBrowsableState.Never)]
		public virtual bool InvokeRPC(int cmdHash, NetworkReader reader)
		{
			return this.InvokeRpcDelegate(cmdHash, reader);
		}

		// Token: 0x0600014A RID: 330 RVA: 0x00008EF7 File Offset: 0x000070F7
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected void SendEventInternal(NetworkWriter writer, int channelId, string eventName)
		{
			if (!NetworkServer.active)
			{
				if (LogFilter.logWarn)
				{
					Debug.LogWarning("SendEvent no server?");
				}
			}
			else
			{
				writer.FinishMessage();
				NetworkServer.SendWriterToReady(base.gameObject, writer, channelId);
			}
		}

		// Token: 0x0600014B RID: 331 RVA: 0x00008F34 File Offset: 0x00007134
		[EditorBrowsable(EditorBrowsableState.Never)]
		public virtual bool InvokeSyncEvent(int cmdHash, NetworkReader reader)
		{
			return this.InvokeSyncEventDelegate(cmdHash, reader);
		}

		// Token: 0x0600014C RID: 332 RVA: 0x00008F60 File Offset: 0x00007160
		[EditorBrowsable(EditorBrowsableState.Never)]
		public virtual bool InvokeSyncList(int cmdHash, NetworkReader reader)
		{
			return this.InvokeSyncListDelegate(cmdHash, reader);
		}

		// Token: 0x0600014D RID: 333 RVA: 0x00008F8C File Offset: 0x0000718C
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected static void RegisterCommandDelegate(Type invokeClass, int cmdHash, NetworkBehaviour.CmdDelegate func)
		{
			if (!NetworkBehaviour.s_CmdHandlerDelegates.ContainsKey(cmdHash))
			{
				NetworkBehaviour.Invoker invoker = new NetworkBehaviour.Invoker();
				invoker.invokeType = NetworkBehaviour.UNetInvokeType.Command;
				invoker.invokeClass = invokeClass;
				invoker.invokeFunction = func;
				NetworkBehaviour.s_CmdHandlerDelegates[cmdHash] = invoker;
				if (LogFilter.logDev)
				{
					Debug.Log(string.Concat(new object[]
					{
						"RegisterCommandDelegate hash:",
						cmdHash,
						" ",
						func.GetMethodName()
					}));
				}
			}
		}

		// Token: 0x0600014E RID: 334 RVA: 0x00009018 File Offset: 0x00007218
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected static void RegisterRpcDelegate(Type invokeClass, int cmdHash, NetworkBehaviour.CmdDelegate func)
		{
			if (!NetworkBehaviour.s_CmdHandlerDelegates.ContainsKey(cmdHash))
			{
				NetworkBehaviour.Invoker invoker = new NetworkBehaviour.Invoker();
				invoker.invokeType = NetworkBehaviour.UNetInvokeType.ClientRpc;
				invoker.invokeClass = invokeClass;
				invoker.invokeFunction = func;
				NetworkBehaviour.s_CmdHandlerDelegates[cmdHash] = invoker;
				if (LogFilter.logDev)
				{
					Debug.Log(string.Concat(new object[]
					{
						"RegisterRpcDelegate hash:",
						cmdHash,
						" ",
						func.GetMethodName()
					}));
				}
			}
		}

		// Token: 0x0600014F RID: 335 RVA: 0x000090A4 File Offset: 0x000072A4
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected static void RegisterEventDelegate(Type invokeClass, int cmdHash, NetworkBehaviour.CmdDelegate func)
		{
			if (!NetworkBehaviour.s_CmdHandlerDelegates.ContainsKey(cmdHash))
			{
				NetworkBehaviour.Invoker invoker = new NetworkBehaviour.Invoker();
				invoker.invokeType = NetworkBehaviour.UNetInvokeType.SyncEvent;
				invoker.invokeClass = invokeClass;
				invoker.invokeFunction = func;
				NetworkBehaviour.s_CmdHandlerDelegates[cmdHash] = invoker;
				if (LogFilter.logDev)
				{
					Debug.Log(string.Concat(new object[]
					{
						"RegisterEventDelegate hash:",
						cmdHash,
						" ",
						func.GetMethodName()
					}));
				}
			}
		}

		// Token: 0x06000150 RID: 336 RVA: 0x00009130 File Offset: 0x00007330
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected static void RegisterSyncListDelegate(Type invokeClass, int cmdHash, NetworkBehaviour.CmdDelegate func)
		{
			if (!NetworkBehaviour.s_CmdHandlerDelegates.ContainsKey(cmdHash))
			{
				NetworkBehaviour.Invoker invoker = new NetworkBehaviour.Invoker();
				invoker.invokeType = NetworkBehaviour.UNetInvokeType.SyncList;
				invoker.invokeClass = invokeClass;
				invoker.invokeFunction = func;
				NetworkBehaviour.s_CmdHandlerDelegates[cmdHash] = invoker;
				if (LogFilter.logDev)
				{
					Debug.Log(string.Concat(new object[]
					{
						"RegisterSyncListDelegate hash:",
						cmdHash,
						" ",
						func.GetMethodName()
					}));
				}
			}
		}

		// Token: 0x06000151 RID: 337 RVA: 0x000091BC File Offset: 0x000073BC
		internal static string GetInvoker(int cmdHash)
		{
			string result;
			if (!NetworkBehaviour.s_CmdHandlerDelegates.ContainsKey(cmdHash))
			{
				result = null;
			}
			else
			{
				NetworkBehaviour.Invoker invoker = NetworkBehaviour.s_CmdHandlerDelegates[cmdHash];
				result = invoker.DebugString();
			}
			return result;
		}

		// Token: 0x06000152 RID: 338 RVA: 0x000091FC File Offset: 0x000073FC
		internal static bool GetInvokerForHashCommand(int cmdHash, out Type invokeClass, out NetworkBehaviour.CmdDelegate invokeFunction)
		{
			return NetworkBehaviour.GetInvokerForHash(cmdHash, NetworkBehaviour.UNetInvokeType.Command, out invokeClass, out invokeFunction);
		}

		// Token: 0x06000153 RID: 339 RVA: 0x0000921C File Offset: 0x0000741C
		internal static bool GetInvokerForHashClientRpc(int cmdHash, out Type invokeClass, out NetworkBehaviour.CmdDelegate invokeFunction)
		{
			return NetworkBehaviour.GetInvokerForHash(cmdHash, NetworkBehaviour.UNetInvokeType.ClientRpc, out invokeClass, out invokeFunction);
		}

		// Token: 0x06000154 RID: 340 RVA: 0x0000923C File Offset: 0x0000743C
		internal static bool GetInvokerForHashSyncList(int cmdHash, out Type invokeClass, out NetworkBehaviour.CmdDelegate invokeFunction)
		{
			return NetworkBehaviour.GetInvokerForHash(cmdHash, NetworkBehaviour.UNetInvokeType.SyncList, out invokeClass, out invokeFunction);
		}

		// Token: 0x06000155 RID: 341 RVA: 0x0000925C File Offset: 0x0000745C
		internal static bool GetInvokerForHashSyncEvent(int cmdHash, out Type invokeClass, out NetworkBehaviour.CmdDelegate invokeFunction)
		{
			return NetworkBehaviour.GetInvokerForHash(cmdHash, NetworkBehaviour.UNetInvokeType.SyncEvent, out invokeClass, out invokeFunction);
		}

		// Token: 0x06000156 RID: 342 RVA: 0x0000927C File Offset: 0x0000747C
		private static bool GetInvokerForHash(int cmdHash, NetworkBehaviour.UNetInvokeType invokeType, out Type invokeClass, out NetworkBehaviour.CmdDelegate invokeFunction)
		{
			NetworkBehaviour.Invoker invoker = null;
			bool result;
			if (!NetworkBehaviour.s_CmdHandlerDelegates.TryGetValue(cmdHash, out invoker))
			{
				if (LogFilter.logDev)
				{
					Debug.Log("GetInvokerForHash hash:" + cmdHash + " not found");
				}
				invokeClass = null;
				invokeFunction = null;
				result = false;
			}
			else if (invoker == null)
			{
				if (LogFilter.logDev)
				{
					Debug.Log("GetInvokerForHash hash:" + cmdHash + " invoker null");
				}
				invokeClass = null;
				invokeFunction = null;
				result = false;
			}
			else if (invoker.invokeType != invokeType)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("GetInvokerForHash hash:" + cmdHash + " mismatched invokeType");
				}
				invokeClass = null;
				invokeFunction = null;
				result = false;
			}
			else
			{
				invokeClass = invoker.invokeClass;
				invokeFunction = invoker.invokeFunction;
				result = true;
			}
			return result;
		}

		// Token: 0x06000157 RID: 343 RVA: 0x00009364 File Offset: 0x00007564
		internal static void DumpInvokers()
		{
			Debug.Log("DumpInvokers size:" + NetworkBehaviour.s_CmdHandlerDelegates.Count);
			foreach (KeyValuePair<int, NetworkBehaviour.Invoker> keyValuePair in NetworkBehaviour.s_CmdHandlerDelegates)
			{
				Debug.Log(string.Concat(new object[]
				{
					"  Invoker:",
					keyValuePair.Value.invokeClass,
					":",
					keyValuePair.Value.invokeFunction.GetMethodName(),
					" ",
					keyValuePair.Value.invokeType,
					" ",
					keyValuePair.Key
				}));
			}
		}

		// Token: 0x06000158 RID: 344 RVA: 0x00009450 File Offset: 0x00007650
		internal bool ContainsCommandDelegate(int cmdHash)
		{
			return NetworkBehaviour.s_CmdHandlerDelegates.ContainsKey(cmdHash);
		}

		// Token: 0x06000159 RID: 345 RVA: 0x00009470 File Offset: 0x00007670
		internal bool InvokeCommandDelegate(int cmdHash, NetworkReader reader)
		{
			bool result;
			if (!NetworkBehaviour.s_CmdHandlerDelegates.ContainsKey(cmdHash))
			{
				result = false;
			}
			else
			{
				NetworkBehaviour.Invoker invoker = NetworkBehaviour.s_CmdHandlerDelegates[cmdHash];
				if (invoker.invokeType != NetworkBehaviour.UNetInvokeType.Command)
				{
					result = false;
				}
				else
				{
					if (base.GetType() != invoker.invokeClass)
					{
						if (!base.GetType().IsSubclassOf(invoker.invokeClass))
						{
							return false;
						}
					}
					invoker.invokeFunction(this, reader);
					result = true;
				}
			}
			return result;
		}

		// Token: 0x0600015A RID: 346 RVA: 0x00009504 File Offset: 0x00007704
		internal bool InvokeRpcDelegate(int cmdHash, NetworkReader reader)
		{
			bool result;
			if (!NetworkBehaviour.s_CmdHandlerDelegates.ContainsKey(cmdHash))
			{
				result = false;
			}
			else
			{
				NetworkBehaviour.Invoker invoker = NetworkBehaviour.s_CmdHandlerDelegates[cmdHash];
				if (invoker.invokeType != NetworkBehaviour.UNetInvokeType.ClientRpc)
				{
					result = false;
				}
				else
				{
					if (base.GetType() != invoker.invokeClass)
					{
						if (!base.GetType().IsSubclassOf(invoker.invokeClass))
						{
							return false;
						}
					}
					invoker.invokeFunction(this, reader);
					result = true;
				}
			}
			return result;
		}

		// Token: 0x0600015B RID: 347 RVA: 0x00009598 File Offset: 0x00007798
		internal bool InvokeSyncEventDelegate(int cmdHash, NetworkReader reader)
		{
			bool result;
			if (!NetworkBehaviour.s_CmdHandlerDelegates.ContainsKey(cmdHash))
			{
				result = false;
			}
			else
			{
				NetworkBehaviour.Invoker invoker = NetworkBehaviour.s_CmdHandlerDelegates[cmdHash];
				if (invoker.invokeType != NetworkBehaviour.UNetInvokeType.SyncEvent)
				{
					result = false;
				}
				else
				{
					invoker.invokeFunction(this, reader);
					result = true;
				}
			}
			return result;
		}

		// Token: 0x0600015C RID: 348 RVA: 0x000095F4 File Offset: 0x000077F4
		internal bool InvokeSyncListDelegate(int cmdHash, NetworkReader reader)
		{
			bool result;
			if (!NetworkBehaviour.s_CmdHandlerDelegates.ContainsKey(cmdHash))
			{
				result = false;
			}
			else
			{
				NetworkBehaviour.Invoker invoker = NetworkBehaviour.s_CmdHandlerDelegates[cmdHash];
				if (invoker.invokeType != NetworkBehaviour.UNetInvokeType.SyncList)
				{
					result = false;
				}
				else if (base.GetType() != invoker.invokeClass)
				{
					result = false;
				}
				else
				{
					invoker.invokeFunction(this, reader);
					result = true;
				}
			}
			return result;
		}

		// Token: 0x0600015D RID: 349 RVA: 0x00009668 File Offset: 0x00007868
		internal static string GetCmdHashHandlerName(int cmdHash)
		{
			string result;
			if (!NetworkBehaviour.s_CmdHandlerDelegates.ContainsKey(cmdHash))
			{
				result = cmdHash.ToString();
			}
			else
			{
				NetworkBehaviour.Invoker invoker = NetworkBehaviour.s_CmdHandlerDelegates[cmdHash];
				result = invoker.invokeType + ":" + invoker.invokeFunction.GetMethodName();
			}
			return result;
		}

		// Token: 0x0600015E RID: 350 RVA: 0x000096D0 File Offset: 0x000078D0
		private static string GetCmdHashPrefixName(int cmdHash, string prefix)
		{
			string result;
			if (!NetworkBehaviour.s_CmdHandlerDelegates.ContainsKey(cmdHash))
			{
				result = cmdHash.ToString();
			}
			else
			{
				NetworkBehaviour.Invoker invoker = NetworkBehaviour.s_CmdHandlerDelegates[cmdHash];
				string text = invoker.invokeFunction.GetMethodName();
				int num = text.IndexOf(prefix);
				if (num > -1)
				{
					text = text.Substring(prefix.Length);
				}
				result = text;
			}
			return result;
		}

		// Token: 0x0600015F RID: 351 RVA: 0x00009740 File Offset: 0x00007940
		internal static string GetCmdHashCmdName(int cmdHash)
		{
			return NetworkBehaviour.GetCmdHashPrefixName(cmdHash, "InvokeCmd");
		}

		// Token: 0x06000160 RID: 352 RVA: 0x00009760 File Offset: 0x00007960
		internal static string GetCmdHashRpcName(int cmdHash)
		{
			return NetworkBehaviour.GetCmdHashPrefixName(cmdHash, "InvokeRpc");
		}

		// Token: 0x06000161 RID: 353 RVA: 0x00009780 File Offset: 0x00007980
		internal static string GetCmdHashEventName(int cmdHash)
		{
			return NetworkBehaviour.GetCmdHashPrefixName(cmdHash, "InvokeSyncEvent");
		}

		// Token: 0x06000162 RID: 354 RVA: 0x000097A0 File Offset: 0x000079A0
		internal static string GetCmdHashListName(int cmdHash)
		{
			return NetworkBehaviour.GetCmdHashPrefixName(cmdHash, "InvokeSyncList");
		}

		// Token: 0x06000163 RID: 355 RVA: 0x000097C0 File Offset: 0x000079C0
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected void SetSyncVarGameObject(GameObject newGameObject, ref GameObject gameObjectField, uint dirtyBit, ref NetworkInstanceId netIdField)
		{
			if (!this.m_SyncVarGuard)
			{
				NetworkInstanceId networkInstanceId = default(NetworkInstanceId);
				if (newGameObject != null)
				{
					NetworkIdentity component = newGameObject.GetComponent<NetworkIdentity>();
					if (component != null)
					{
						networkInstanceId = component.netId;
						if (networkInstanceId.IsEmpty())
						{
							if (LogFilter.logWarn)
							{
								Debug.LogWarning("SetSyncVarGameObject GameObject " + newGameObject + " has a zero netId. Maybe it is not spawned yet?");
							}
						}
					}
				}
				NetworkInstanceId networkInstanceId2 = default(NetworkInstanceId);
				if (gameObjectField != null)
				{
					networkInstanceId2 = gameObjectField.GetComponent<NetworkIdentity>().netId;
				}
				if (networkInstanceId != networkInstanceId2)
				{
					if (LogFilter.logDev)
					{
						Debug.Log(string.Concat(new object[]
						{
							"SetSyncVar GameObject ",
							base.GetType().Name,
							" bit [",
							dirtyBit,
							"] netfieldId:",
							networkInstanceId2,
							"->",
							networkInstanceId
						}));
					}
					this.SetDirtyBit(dirtyBit);
					gameObjectField = newGameObject;
					netIdField = networkInstanceId;
				}
			}
		}

		// Token: 0x06000164 RID: 356 RVA: 0x000098E8 File Offset: 0x00007AE8
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected void SetSyncVar<T>(T value, ref T fieldValue, uint dirtyBit)
		{
			bool flag = false;
			if (value == null)
			{
				if (fieldValue != null)
				{
					flag = true;
				}
			}
			else
			{
				flag = !value.Equals(fieldValue);
			}
			if (flag)
			{
				if (LogFilter.logDev)
				{
					Debug.Log(string.Concat(new object[]
					{
						"SetSyncVar ",
						base.GetType().Name,
						" bit [",
						dirtyBit,
						"] ",
						fieldValue,
						"->",
						value
					}));
				}
				this.SetDirtyBit(dirtyBit);
				fieldValue = value;
			}
		}

		// Token: 0x06000165 RID: 357 RVA: 0x000099BA File Offset: 0x00007BBA
		public void SetDirtyBit(uint dirtyBit)
		{
			this.m_SyncVarDirtyBits |= dirtyBit;
		}

		// Token: 0x06000166 RID: 358 RVA: 0x000099CB File Offset: 0x00007BCB
		public void ClearAllDirtyBits()
		{
			this.m_LastSendTime = Time.time;
			this.m_SyncVarDirtyBits = 0U;
		}

		// Token: 0x06000167 RID: 359 RVA: 0x000099E0 File Offset: 0x00007BE0
		internal int GetDirtyChannel()
		{
			if (Time.time - this.m_LastSendTime > this.GetNetworkSendInterval())
			{
				if (this.m_SyncVarDirtyBits != 0U)
				{
					return this.GetNetworkChannel();
				}
			}
			return -1;
		}

		// Token: 0x06000168 RID: 360 RVA: 0x00009A28 File Offset: 0x00007C28
		public virtual bool OnSerialize(NetworkWriter writer, bool initialState)
		{
			if (!initialState)
			{
				writer.WritePackedUInt32(0U);
			}
			return false;
		}

		// Token: 0x06000169 RID: 361 RVA: 0x00009A4D File Offset: 0x00007C4D
		public virtual void OnDeserialize(NetworkReader reader, bool initialState)
		{
			if (!initialState)
			{
				reader.ReadPackedUInt32();
			}
		}

		// Token: 0x0600016A RID: 362 RVA: 0x00009A5F File Offset: 0x00007C5F
		[EditorBrowsable(EditorBrowsableState.Never)]
		public virtual void PreStartClient()
		{
		}

		// Token: 0x0600016B RID: 363 RVA: 0x00009A62 File Offset: 0x00007C62
		public virtual void OnNetworkDestroy()
		{
		}

		// Token: 0x0600016C RID: 364 RVA: 0x00009A65 File Offset: 0x00007C65
		public virtual void OnStartServer()
		{
		}

		// Token: 0x0600016D RID: 365 RVA: 0x00009A68 File Offset: 0x00007C68
		public virtual void OnStartClient()
		{
		}

		// Token: 0x0600016E RID: 366 RVA: 0x00009A6B File Offset: 0x00007C6B
		public virtual void OnStartLocalPlayer()
		{
		}

		// Token: 0x0600016F RID: 367 RVA: 0x00009A6E File Offset: 0x00007C6E
		public virtual void OnStartAuthority()
		{
		}

		// Token: 0x06000170 RID: 368 RVA: 0x00009A71 File Offset: 0x00007C71
		public virtual void OnStopAuthority()
		{
		}

		// Token: 0x06000171 RID: 369 RVA: 0x00009A74 File Offset: 0x00007C74
		public virtual bool OnRebuildObservers(HashSet<NetworkConnection> observers, bool initialize)
		{
			return false;
		}

		// Token: 0x06000172 RID: 370 RVA: 0x00009A8A File Offset: 0x00007C8A
		public virtual void OnSetLocalVisibility(bool vis)
		{
		}

		// Token: 0x06000173 RID: 371 RVA: 0x00009A90 File Offset: 0x00007C90
		public virtual bool OnCheckObserver(NetworkConnection conn)
		{
			return true;
		}

		// Token: 0x06000174 RID: 372 RVA: 0x00009AA8 File Offset: 0x00007CA8
		public virtual int GetNetworkChannel()
		{
			return 0;
		}

		// Token: 0x06000175 RID: 373 RVA: 0x00009AC0 File Offset: 0x00007CC0
		public virtual float GetNetworkSendInterval()
		{
			return 0.1f;
		}

		// Token: 0x06000176 RID: 374 RVA: 0x00009ADA File Offset: 0x00007CDA
		// Note: this type is marked as 'beforefieldinit'.
		static NetworkBehaviour()
		{
		}

		// Token: 0x040000B5 RID: 181
		private uint m_SyncVarDirtyBits;

		// Token: 0x040000B6 RID: 182
		private float m_LastSendTime;

		// Token: 0x040000B7 RID: 183
		private bool m_SyncVarGuard;

		// Token: 0x040000B8 RID: 184
		private const float k_DefaultSendInterval = 0.1f;

		// Token: 0x040000B9 RID: 185
		private NetworkIdentity m_MyView;

		// Token: 0x040000BA RID: 186
		private static Dictionary<int, NetworkBehaviour.Invoker> s_CmdHandlerDelegates = new Dictionary<int, NetworkBehaviour.Invoker>();

		// Token: 0x02000037 RID: 55
		// (Invoke) Token: 0x06000178 RID: 376
		public delegate void CmdDelegate(NetworkBehaviour obj, NetworkReader reader);

		// Token: 0x02000038 RID: 56
		// (Invoke) Token: 0x0600017C RID: 380
		protected delegate void EventDelegate(List<Delegate> targets, NetworkReader reader);

		// Token: 0x02000039 RID: 57
		protected enum UNetInvokeType
		{
			// Token: 0x040000BC RID: 188
			Command,
			// Token: 0x040000BD RID: 189
			ClientRpc,
			// Token: 0x040000BE RID: 190
			SyncEvent,
			// Token: 0x040000BF RID: 191
			SyncList
		}

		// Token: 0x0200003A RID: 58
		protected class Invoker
		{
			// Token: 0x0600017F RID: 383 RVA: 0x00009AE6 File Offset: 0x00007CE6
			public Invoker()
			{
			}

			// Token: 0x06000180 RID: 384 RVA: 0x00009AF0 File Offset: 0x00007CF0
			public string DebugString()
			{
				return string.Concat(new object[]
				{
					this.invokeType,
					":",
					this.invokeClass,
					":",
					this.invokeFunction.GetMethodName()
				});
			}

			// Token: 0x040000C0 RID: 192
			public NetworkBehaviour.UNetInvokeType invokeType;

			// Token: 0x040000C1 RID: 193
			public Type invokeClass;

			// Token: 0x040000C2 RID: 194
			public NetworkBehaviour.CmdDelegate invokeFunction;
		}
	}
}
