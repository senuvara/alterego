﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Networking
{
	// Token: 0x02000058 RID: 88
	internal class NetworkScene
	{
		// Token: 0x060003DA RID: 986 RVA: 0x000150AA File Offset: 0x000132AA
		public NetworkScene()
		{
		}

		// Token: 0x1700009E RID: 158
		// (get) Token: 0x060003DB RID: 987 RVA: 0x000150C0 File Offset: 0x000132C0
		internal Dictionary<NetworkInstanceId, NetworkIdentity> localObjects
		{
			get
			{
				return this.m_LocalObjects;
			}
		}

		// Token: 0x1700009F RID: 159
		// (get) Token: 0x060003DC RID: 988 RVA: 0x000150DC File Offset: 0x000132DC
		internal static Dictionary<NetworkHash128, GameObject> guidToPrefab
		{
			get
			{
				return NetworkScene.s_GuidToPrefab;
			}
		}

		// Token: 0x170000A0 RID: 160
		// (get) Token: 0x060003DD RID: 989 RVA: 0x000150F8 File Offset: 0x000132F8
		internal static Dictionary<NetworkHash128, SpawnDelegate> spawnHandlers
		{
			get
			{
				return NetworkScene.s_SpawnHandlers;
			}
		}

		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x060003DE RID: 990 RVA: 0x00015114 File Offset: 0x00013314
		internal static Dictionary<NetworkHash128, UnSpawnDelegate> unspawnHandlers
		{
			get
			{
				return NetworkScene.s_UnspawnHandlers;
			}
		}

		// Token: 0x060003DF RID: 991 RVA: 0x0001512E File Offset: 0x0001332E
		internal void Shutdown()
		{
			this.ClearLocalObjects();
			NetworkScene.ClearSpawners();
		}

		// Token: 0x060003E0 RID: 992 RVA: 0x0001513C File Offset: 0x0001333C
		internal void SetLocalObject(NetworkInstanceId netId, GameObject obj, bool isClient, bool isServer)
		{
			if (LogFilter.logDev)
			{
				Debug.Log(string.Concat(new object[]
				{
					"SetLocalObject ",
					netId,
					" ",
					obj
				}));
			}
			if (obj == null)
			{
				this.m_LocalObjects[netId] = null;
			}
			else
			{
				NetworkIdentity networkIdentity = null;
				if (this.m_LocalObjects.ContainsKey(netId))
				{
					networkIdentity = this.m_LocalObjects[netId];
				}
				if (networkIdentity == null)
				{
					networkIdentity = obj.GetComponent<NetworkIdentity>();
					this.m_LocalObjects[netId] = networkIdentity;
				}
				networkIdentity.UpdateClientServer(isClient, isServer);
			}
		}

		// Token: 0x060003E1 RID: 993 RVA: 0x000151F0 File Offset: 0x000133F0
		internal GameObject FindLocalObject(NetworkInstanceId netId)
		{
			if (this.m_LocalObjects.ContainsKey(netId))
			{
				NetworkIdentity networkIdentity = this.m_LocalObjects[netId];
				if (networkIdentity != null)
				{
					return networkIdentity.gameObject;
				}
			}
			return null;
		}

		// Token: 0x060003E2 RID: 994 RVA: 0x00015240 File Offset: 0x00013440
		internal bool GetNetworkIdentity(NetworkInstanceId netId, out NetworkIdentity uv)
		{
			bool result;
			if (this.m_LocalObjects.ContainsKey(netId) && this.m_LocalObjects[netId] != null)
			{
				uv = this.m_LocalObjects[netId];
				result = true;
			}
			else
			{
				uv = null;
				result = false;
			}
			return result;
		}

		// Token: 0x060003E3 RID: 995 RVA: 0x00015298 File Offset: 0x00013498
		internal bool RemoveLocalObject(NetworkInstanceId netId)
		{
			return this.m_LocalObjects.Remove(netId);
		}

		// Token: 0x060003E4 RID: 996 RVA: 0x000152BC File Offset: 0x000134BC
		internal bool RemoveLocalObjectAndDestroy(NetworkInstanceId netId)
		{
			bool result;
			if (this.m_LocalObjects.ContainsKey(netId))
			{
				NetworkIdentity networkIdentity = this.m_LocalObjects[netId];
				Object.Destroy(networkIdentity.gameObject);
				result = this.m_LocalObjects.Remove(netId);
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x060003E5 RID: 997 RVA: 0x0001530E File Offset: 0x0001350E
		internal void ClearLocalObjects()
		{
			this.m_LocalObjects.Clear();
		}

		// Token: 0x060003E6 RID: 998 RVA: 0x0001531C File Offset: 0x0001351C
		internal static void RegisterPrefab(GameObject prefab, NetworkHash128 newAssetId)
		{
			NetworkIdentity component = prefab.GetComponent<NetworkIdentity>();
			if (component)
			{
				component.SetDynamicAssetId(newAssetId);
				if (LogFilter.logDebug)
				{
					Debug.Log(string.Concat(new object[]
					{
						"Registering prefab '",
						prefab.name,
						"' as asset:",
						component.assetId
					}));
				}
				NetworkScene.s_GuidToPrefab[component.assetId] = prefab;
			}
			else if (LogFilter.logError)
			{
				Debug.LogError("Could not register '" + prefab.name + "' since it contains no NetworkIdentity component");
			}
		}

		// Token: 0x060003E7 RID: 999 RVA: 0x000153C8 File Offset: 0x000135C8
		internal static void RegisterPrefab(GameObject prefab)
		{
			NetworkIdentity component = prefab.GetComponent<NetworkIdentity>();
			if (component)
			{
				if (LogFilter.logDebug)
				{
					Debug.Log(string.Concat(new object[]
					{
						"Registering prefab '",
						prefab.name,
						"' as asset:",
						component.assetId
					}));
				}
				NetworkScene.s_GuidToPrefab[component.assetId] = prefab;
				NetworkIdentity[] componentsInChildren = prefab.GetComponentsInChildren<NetworkIdentity>();
				if (componentsInChildren.Length > 1)
				{
					if (LogFilter.logWarn)
					{
						Debug.LogWarning("The prefab '" + prefab.name + "' has multiple NetworkIdentity components. There can only be one NetworkIdentity on a prefab, and it must be on the root object.");
					}
				}
			}
			else if (LogFilter.logError)
			{
				Debug.LogError("Could not register '" + prefab.name + "' since it contains no NetworkIdentity component");
			}
		}

		// Token: 0x060003E8 RID: 1000 RVA: 0x000154A4 File Offset: 0x000136A4
		internal static bool GetPrefab(NetworkHash128 assetId, out GameObject prefab)
		{
			bool result;
			if (!assetId.IsValid())
			{
				prefab = null;
				result = false;
			}
			else if (NetworkScene.s_GuidToPrefab.ContainsKey(assetId) && NetworkScene.s_GuidToPrefab[assetId] != null)
			{
				prefab = NetworkScene.s_GuidToPrefab[assetId];
				result = true;
			}
			else
			{
				prefab = null;
				result = false;
			}
			return result;
		}

		// Token: 0x060003E9 RID: 1001 RVA: 0x0001550F File Offset: 0x0001370F
		internal static void ClearSpawners()
		{
			NetworkScene.s_GuidToPrefab.Clear();
			NetworkScene.s_SpawnHandlers.Clear();
			NetworkScene.s_UnspawnHandlers.Clear();
		}

		// Token: 0x060003EA RID: 1002 RVA: 0x00015530 File Offset: 0x00013730
		public static void UnregisterSpawnHandler(NetworkHash128 assetId)
		{
			NetworkScene.s_SpawnHandlers.Remove(assetId);
			NetworkScene.s_UnspawnHandlers.Remove(assetId);
		}

		// Token: 0x060003EB RID: 1003 RVA: 0x0001554C File Offset: 0x0001374C
		internal static void RegisterSpawnHandler(NetworkHash128 assetId, SpawnDelegate spawnHandler, UnSpawnDelegate unspawnHandler)
		{
			if (spawnHandler == null || unspawnHandler == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("RegisterSpawnHandler custom spawn function null for " + assetId);
				}
			}
			else
			{
				if (LogFilter.logDebug)
				{
					Debug.Log(string.Concat(new object[]
					{
						"RegisterSpawnHandler asset '",
						assetId,
						"' ",
						spawnHandler.GetMethodName(),
						"/",
						unspawnHandler.GetMethodName()
					}));
				}
				NetworkScene.s_SpawnHandlers[assetId] = spawnHandler;
				NetworkScene.s_UnspawnHandlers[assetId] = unspawnHandler;
			}
		}

		// Token: 0x060003EC RID: 1004 RVA: 0x000155F4 File Offset: 0x000137F4
		internal static void UnregisterPrefab(GameObject prefab)
		{
			NetworkIdentity component = prefab.GetComponent<NetworkIdentity>();
			if (component == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("Could not unregister '" + prefab.name + "' since it contains no NetworkIdentity component");
				}
			}
			else
			{
				NetworkScene.s_SpawnHandlers.Remove(component.assetId);
				NetworkScene.s_UnspawnHandlers.Remove(component.assetId);
			}
		}

		// Token: 0x060003ED RID: 1005 RVA: 0x00015664 File Offset: 0x00013864
		internal static void RegisterPrefab(GameObject prefab, SpawnDelegate spawnHandler, UnSpawnDelegate unspawnHandler)
		{
			NetworkIdentity component = prefab.GetComponent<NetworkIdentity>();
			if (component == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("Could not register '" + prefab.name + "' since it contains no NetworkIdentity component");
				}
			}
			else if (spawnHandler == null || unspawnHandler == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("RegisterPrefab custom spawn function null for " + component.assetId);
				}
			}
			else if (!component.assetId.IsValid())
			{
				if (LogFilter.logError)
				{
					Debug.LogError("RegisterPrefab game object " + prefab.name + " has no prefab. Use RegisterSpawnHandler() instead?");
				}
			}
			else
			{
				if (LogFilter.logDebug)
				{
					Debug.Log(string.Concat(new object[]
					{
						"Registering custom prefab '",
						prefab.name,
						"' as asset:",
						component.assetId,
						" ",
						spawnHandler.GetMethodName(),
						"/",
						unspawnHandler.GetMethodName()
					}));
				}
				NetworkScene.s_SpawnHandlers[component.assetId] = spawnHandler;
				NetworkScene.s_UnspawnHandlers[component.assetId] = unspawnHandler;
			}
		}

		// Token: 0x060003EE RID: 1006 RVA: 0x000157B0 File Offset: 0x000139B0
		internal static bool GetSpawnHandler(NetworkHash128 assetId, out SpawnDelegate handler)
		{
			bool result;
			if (NetworkScene.s_SpawnHandlers.ContainsKey(assetId))
			{
				handler = NetworkScene.s_SpawnHandlers[assetId];
				result = true;
			}
			else
			{
				handler = null;
				result = false;
			}
			return result;
		}

		// Token: 0x060003EF RID: 1007 RVA: 0x000157F0 File Offset: 0x000139F0
		internal static bool InvokeUnSpawnHandler(NetworkHash128 assetId, GameObject obj)
		{
			bool result;
			if (NetworkScene.s_UnspawnHandlers.ContainsKey(assetId) && NetworkScene.s_UnspawnHandlers[assetId] != null)
			{
				UnSpawnDelegate unSpawnDelegate = NetworkScene.s_UnspawnHandlers[assetId];
				unSpawnDelegate(obj);
				result = true;
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x060003F0 RID: 1008 RVA: 0x00015844 File Offset: 0x00013A44
		internal void DestroyAllClientObjects()
		{
			foreach (NetworkInstanceId key in this.m_LocalObjects.Keys)
			{
				NetworkIdentity networkIdentity = this.m_LocalObjects[key];
				if (networkIdentity != null && networkIdentity.gameObject != null)
				{
					if (!NetworkScene.InvokeUnSpawnHandler(networkIdentity.assetId, networkIdentity.gameObject))
					{
						if (networkIdentity.sceneId.IsEmpty())
						{
							Object.Destroy(networkIdentity.gameObject);
						}
						else
						{
							networkIdentity.MarkForReset();
							networkIdentity.gameObject.SetActive(false);
						}
					}
				}
			}
			this.ClearLocalObjects();
		}

		// Token: 0x060003F1 RID: 1009 RVA: 0x00015928 File Offset: 0x00013B28
		internal void DumpAllClientObjects()
		{
			foreach (NetworkInstanceId networkInstanceId in this.m_LocalObjects.Keys)
			{
				NetworkIdentity networkIdentity = this.m_LocalObjects[networkInstanceId];
				if (networkIdentity != null)
				{
					Debug.Log(string.Concat(new object[]
					{
						"ID:",
						networkInstanceId,
						" OBJ:",
						networkIdentity.gameObject,
						" AS:",
						networkIdentity.assetId
					}));
				}
				else
				{
					Debug.Log("ID:" + networkInstanceId + " OBJ: null");
				}
			}
		}

		// Token: 0x060003F2 RID: 1010 RVA: 0x00015A08 File Offset: 0x00013C08
		// Note: this type is marked as 'beforefieldinit'.
		static NetworkScene()
		{
		}

		// Token: 0x040001B4 RID: 436
		private Dictionary<NetworkInstanceId, NetworkIdentity> m_LocalObjects = new Dictionary<NetworkInstanceId, NetworkIdentity>();

		// Token: 0x040001B5 RID: 437
		private static Dictionary<NetworkHash128, GameObject> s_GuidToPrefab = new Dictionary<NetworkHash128, GameObject>();

		// Token: 0x040001B6 RID: 438
		private static Dictionary<NetworkHash128, SpawnDelegate> s_SpawnHandlers = new Dictionary<NetworkHash128, SpawnDelegate>();

		// Token: 0x040001B7 RID: 439
		private static Dictionary<NetworkHash128, UnSpawnDelegate> s_UnspawnHandlers = new Dictionary<NetworkHash128, UnSpawnDelegate>();
	}
}
