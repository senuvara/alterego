﻿using System;
using System.Net;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking
{
	// Token: 0x02000011 RID: 17
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	internal class DefaultNetworkTransport : INetworkTransport
	{
		// Token: 0x06000070 RID: 112 RVA: 0x00004CA2 File Offset: 0x00002EA2
		public DefaultNetworkTransport()
		{
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000071 RID: 113 RVA: 0x00004CAC File Offset: 0x00002EAC
		public bool IsStarted
		{
			get
			{
				return NetworkTransport.IsStarted;
			}
		}

		// Token: 0x06000072 RID: 114 RVA: 0x00004CC8 File Offset: 0x00002EC8
		public int AddHost(HostTopology topology, int port, string ip)
		{
			return NetworkTransport.AddHost(topology, port, ip);
		}

		// Token: 0x06000073 RID: 115 RVA: 0x00004CE8 File Offset: 0x00002EE8
		public int AddHostWithSimulator(HostTopology topology, int minTimeout, int maxTimeout, int port)
		{
			return NetworkTransport.AddHostWithSimulator(topology, minTimeout, maxTimeout, port);
		}

		// Token: 0x06000074 RID: 116 RVA: 0x00004D08 File Offset: 0x00002F08
		public int AddWebsocketHost(HostTopology topology, int port, string ip)
		{
			return NetworkTransport.AddWebsocketHost(topology, port, ip);
		}

		// Token: 0x06000075 RID: 117 RVA: 0x00004D28 File Offset: 0x00002F28
		public int Connect(int hostId, string address, int port, int specialConnectionId, out byte error)
		{
			return NetworkTransport.Connect(hostId, address, port, specialConnectionId, out error);
		}

		// Token: 0x06000076 RID: 118 RVA: 0x00004D49 File Offset: 0x00002F49
		public void ConnectAsNetworkHost(int hostId, string address, int port, NetworkID network, SourceID source, NodeID node, out byte error)
		{
			NetworkTransport.ConnectAsNetworkHost(hostId, address, port, network, source, node, out error);
		}

		// Token: 0x06000077 RID: 119 RVA: 0x00004D5C File Offset: 0x00002F5C
		public int ConnectEndPoint(int hostId, EndPoint endPoint, int specialConnectionId, out byte error)
		{
			return NetworkTransport.ConnectEndPoint(hostId, endPoint, specialConnectionId, out error);
		}

		// Token: 0x06000078 RID: 120 RVA: 0x00004D7C File Offset: 0x00002F7C
		public int ConnectToNetworkPeer(int hostId, string address, int port, int specialConnectionId, int relaySlotId, NetworkID network, SourceID source, NodeID node, out byte error)
		{
			return NetworkTransport.ConnectToNetworkPeer(hostId, address, port, specialConnectionId, relaySlotId, network, source, node, out error);
		}

		// Token: 0x06000079 RID: 121 RVA: 0x00004DA8 File Offset: 0x00002FA8
		public int ConnectWithSimulator(int hostId, string address, int port, int specialConnectionId, out byte error, ConnectionSimulatorConfig conf)
		{
			return NetworkTransport.ConnectWithSimulator(hostId, address, port, specialConnectionId, out error, conf);
		}

		// Token: 0x0600007A RID: 122 RVA: 0x00004DCC File Offset: 0x00002FCC
		public bool Disconnect(int hostId, int connectionId, out byte error)
		{
			return NetworkTransport.Disconnect(hostId, connectionId, out error);
		}

		// Token: 0x0600007B RID: 123 RVA: 0x00004DEC File Offset: 0x00002FEC
		public bool DoesEndPointUsePlatformProtocols(EndPoint endPoint)
		{
			return NetworkTransport.DoesEndPointUsePlatformProtocols(endPoint);
		}

		// Token: 0x0600007C RID: 124 RVA: 0x00004E07 File Offset: 0x00003007
		public void GetBroadcastConnectionInfo(int hostId, out string address, out int port, out byte error)
		{
			NetworkTransport.GetBroadcastConnectionInfo(hostId, out address, out port, out error);
		}

		// Token: 0x0600007D RID: 125 RVA: 0x00004E14 File Offset: 0x00003014
		public void GetBroadcastConnectionMessage(int hostId, byte[] buffer, int bufferSize, out int receivedSize, out byte error)
		{
			NetworkTransport.GetBroadcastConnectionMessage(hostId, buffer, bufferSize, out receivedSize, out error);
		}

		// Token: 0x0600007E RID: 126 RVA: 0x00004E23 File Offset: 0x00003023
		public void GetConnectionInfo(int hostId, int connectionId, out string address, out int port, out NetworkID network, out NodeID dstNode, out byte error)
		{
			NetworkTransport.GetConnectionInfo(hostId, connectionId, out address, out port, out network, out dstNode, out error);
		}

		// Token: 0x0600007F RID: 127 RVA: 0x00004E38 File Offset: 0x00003038
		public int GetCurrentRTT(int hostId, int connectionId, out byte error)
		{
			return NetworkTransport.GetCurrentRTT(hostId, connectionId, out error);
		}

		// Token: 0x06000080 RID: 128 RVA: 0x00004E55 File Offset: 0x00003055
		public void Init()
		{
			NetworkTransport.Init();
		}

		// Token: 0x06000081 RID: 129 RVA: 0x00004E5D File Offset: 0x0000305D
		public void Init(GlobalConfig config)
		{
			NetworkTransport.Init(config);
		}

		// Token: 0x06000082 RID: 130 RVA: 0x00004E68 File Offset: 0x00003068
		public NetworkEventType Receive(out int hostId, out int connectionId, out int channelId, byte[] buffer, int bufferSize, out int receivedSize, out byte error)
		{
			return NetworkTransport.Receive(out hostId, out connectionId, out channelId, buffer, bufferSize, out receivedSize, out error);
		}

		// Token: 0x06000083 RID: 131 RVA: 0x00004E90 File Offset: 0x00003090
		public NetworkEventType ReceiveFromHost(int hostId, out int connectionId, out int channelId, byte[] buffer, int bufferSize, out int receivedSize, out byte error)
		{
			return NetworkTransport.ReceiveFromHost(hostId, out connectionId, out channelId, buffer, bufferSize, out receivedSize, out error);
		}

		// Token: 0x06000084 RID: 132 RVA: 0x00004EB8 File Offset: 0x000030B8
		public NetworkEventType ReceiveRelayEventFromHost(int hostId, out byte error)
		{
			return NetworkTransport.ReceiveRelayEventFromHost(hostId, out error);
		}

		// Token: 0x06000085 RID: 133 RVA: 0x00004ED4 File Offset: 0x000030D4
		public bool RemoveHost(int hostId)
		{
			return NetworkTransport.RemoveHost(hostId);
		}

		// Token: 0x06000086 RID: 134 RVA: 0x00004EF0 File Offset: 0x000030F0
		public bool Send(int hostId, int connectionId, int channelId, byte[] buffer, int size, out byte error)
		{
			return NetworkTransport.Send(hostId, connectionId, channelId, buffer, size, out error);
		}

		// Token: 0x06000087 RID: 135 RVA: 0x00004F13 File Offset: 0x00003113
		public void SetBroadcastCredentials(int hostId, int key, int version, int subversion, out byte error)
		{
			NetworkTransport.SetBroadcastCredentials(hostId, key, version, subversion, out error);
		}

		// Token: 0x06000088 RID: 136 RVA: 0x00004F22 File Offset: 0x00003122
		public void SetPacketStat(int direction, int packetStatId, int numMsgs, int numBytes)
		{
			NetworkTransport.SetPacketStat(direction, packetStatId, numMsgs, numBytes);
		}

		// Token: 0x06000089 RID: 137 RVA: 0x00004F2F File Offset: 0x0000312F
		public void Shutdown()
		{
			NetworkTransport.Shutdown();
		}

		// Token: 0x0600008A RID: 138 RVA: 0x00004F38 File Offset: 0x00003138
		public bool StartBroadcastDiscovery(int hostId, int broadcastPort, int key, int version, int subversion, byte[] buffer, int size, int timeout, out byte error)
		{
			return NetworkTransport.StartBroadcastDiscovery(hostId, broadcastPort, key, version, subversion, buffer, size, timeout, out error);
		}

		// Token: 0x0600008B RID: 139 RVA: 0x00004F61 File Offset: 0x00003161
		public void StopBroadcastDiscovery()
		{
			NetworkTransport.StopBroadcastDiscovery();
		}
	}
}
