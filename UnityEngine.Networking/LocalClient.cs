﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Networking
{
	// Token: 0x02000014 RID: 20
	internal sealed class LocalClient : NetworkClient
	{
		// Token: 0x060000AA RID: 170 RVA: 0x00006AF8 File Offset: 0x00004CF8
		public LocalClient()
		{
		}

		// Token: 0x060000AB RID: 171 RVA: 0x00006B24 File Offset: 0x00004D24
		public override void Disconnect()
		{
			ClientScene.HandleClientDisconnect(this.m_Connection);
			if (this.m_Connected)
			{
				this.PostInternalMessage(33);
				this.m_Connected = false;
			}
			this.m_AsyncConnect = NetworkClient.ConnectState.Disconnected;
			this.m_LocalServer.RemoveLocalClient(this.m_Connection);
		}

		// Token: 0x060000AC RID: 172 RVA: 0x00006B74 File Offset: 0x00004D74
		internal void InternalConnectLocalServer(bool generateConnectMsg)
		{
			if (this.m_FreeMessages == null)
			{
				this.m_FreeMessages = new Stack<LocalClient.InternalMsg>();
				for (int i = 0; i < 64; i++)
				{
					LocalClient.InternalMsg item = default(LocalClient.InternalMsg);
					this.m_FreeMessages.Push(item);
				}
			}
			this.m_LocalServer = NetworkServer.instance;
			this.m_Connection = new ULocalConnectionToServer(this.m_LocalServer);
			base.SetHandlers(this.m_Connection);
			this.m_Connection.connectionId = this.m_LocalServer.AddLocalClient(this);
			this.m_AsyncConnect = NetworkClient.ConnectState.Connected;
			NetworkClient.SetActive(true);
			base.RegisterSystemHandlers(true);
			if (generateConnectMsg)
			{
				this.PostInternalMessage(32);
			}
			this.m_Connected = true;
		}

		// Token: 0x060000AD RID: 173 RVA: 0x00006C2D File Offset: 0x00004E2D
		internal override void Update()
		{
			this.ProcessInternalMessages();
		}

		// Token: 0x060000AE RID: 174 RVA: 0x00006C38 File Offset: 0x00004E38
		internal void AddLocalPlayer(PlayerController localPlayer)
		{
			if (LogFilter.logDev)
			{
				Debug.Log(string.Concat(new object[]
				{
					"Local client AddLocalPlayer ",
					localPlayer.gameObject.name,
					" conn=",
					this.m_Connection.connectionId
				}));
			}
			this.m_Connection.isReady = true;
			this.m_Connection.SetPlayerController(localPlayer);
			NetworkIdentity unetView = localPlayer.unetView;
			if (unetView != null)
			{
				ClientScene.SetLocalObject(unetView.netId, localPlayer.gameObject);
				unetView.SetConnectionToServer(this.m_Connection);
			}
			ClientScene.InternalAddPlayer(unetView, localPlayer.playerControllerId);
		}

		// Token: 0x060000AF RID: 175 RVA: 0x00006CE8 File Offset: 0x00004EE8
		private void PostInternalMessage(byte[] buffer, int channelId)
		{
			LocalClient.InternalMsg item;
			if (this.m_FreeMessages.Count == 0)
			{
				item = default(LocalClient.InternalMsg);
			}
			else
			{
				item = this.m_FreeMessages.Pop();
			}
			item.buffer = buffer;
			item.channelId = channelId;
			this.m_InternalMsgs.Add(item);
		}

		// Token: 0x060000B0 RID: 176 RVA: 0x00006D40 File Offset: 0x00004F40
		private void PostInternalMessage(short msgType)
		{
			NetworkWriter networkWriter = new NetworkWriter();
			networkWriter.StartMessage(msgType);
			networkWriter.FinishMessage();
			this.PostInternalMessage(networkWriter.AsArray(), 0);
		}

		// Token: 0x060000B1 RID: 177 RVA: 0x00006D70 File Offset: 0x00004F70
		private void ProcessInternalMessages()
		{
			if (this.m_InternalMsgs.Count != 0)
			{
				List<LocalClient.InternalMsg> internalMsgs = this.m_InternalMsgs;
				this.m_InternalMsgs = this.m_InternalMsgs2;
				for (int i = 0; i < internalMsgs.Count; i++)
				{
					LocalClient.InternalMsg item = internalMsgs[i];
					if (this.s_InternalMessage.reader == null)
					{
						this.s_InternalMessage.reader = new NetworkReader(item.buffer);
					}
					else
					{
						this.s_InternalMessage.reader.Replace(item.buffer);
					}
					this.s_InternalMessage.reader.ReadInt16();
					this.s_InternalMessage.channelId = item.channelId;
					this.s_InternalMessage.conn = base.connection;
					this.s_InternalMessage.msgType = this.s_InternalMessage.reader.ReadInt16();
					this.m_Connection.InvokeHandler(this.s_InternalMessage);
					this.m_FreeMessages.Push(item);
					base.connection.lastMessageTime = Time.time;
				}
				this.m_InternalMsgs = internalMsgs;
				this.m_InternalMsgs.Clear();
				for (int j = 0; j < this.m_InternalMsgs2.Count; j++)
				{
					this.m_InternalMsgs.Add(this.m_InternalMsgs2[j]);
				}
				this.m_InternalMsgs2.Clear();
			}
		}

		// Token: 0x060000B2 RID: 178 RVA: 0x00006EE0 File Offset: 0x000050E0
		internal void InvokeHandlerOnClient(short msgType, MessageBase msg, int channelId)
		{
			NetworkWriter networkWriter = new NetworkWriter();
			networkWriter.StartMessage(msgType);
			msg.Serialize(networkWriter);
			networkWriter.FinishMessage();
			this.InvokeBytesOnClient(networkWriter.AsArray(), channelId);
		}

		// Token: 0x060000B3 RID: 179 RVA: 0x00006F15 File Offset: 0x00005115
		internal void InvokeBytesOnClient(byte[] buffer, int channelId)
		{
			this.PostInternalMessage(buffer, channelId);
		}

		// Token: 0x04000051 RID: 81
		private const int k_InitialFreeMessagePoolSize = 64;

		// Token: 0x04000052 RID: 82
		private List<LocalClient.InternalMsg> m_InternalMsgs = new List<LocalClient.InternalMsg>();

		// Token: 0x04000053 RID: 83
		private List<LocalClient.InternalMsg> m_InternalMsgs2 = new List<LocalClient.InternalMsg>();

		// Token: 0x04000054 RID: 84
		private Stack<LocalClient.InternalMsg> m_FreeMessages;

		// Token: 0x04000055 RID: 85
		private NetworkServer m_LocalServer;

		// Token: 0x04000056 RID: 86
		private bool m_Connected;

		// Token: 0x04000057 RID: 87
		private NetworkMessage s_InternalMessage = new NetworkMessage();

		// Token: 0x02000015 RID: 21
		private struct InternalMsg
		{
			// Token: 0x04000058 RID: 88
			internal byte[] buffer;

			// Token: 0x04000059 RID: 89
			internal int channelId;
		}
	}
}
