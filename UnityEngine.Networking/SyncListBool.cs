﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x0200006C RID: 108
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class SyncListBool : SyncList<bool>
	{
		// Token: 0x06000579 RID: 1401 RVA: 0x0001EBF1 File Offset: 0x0001CDF1
		public SyncListBool()
		{
		}

		// Token: 0x0600057A RID: 1402 RVA: 0x0001EBF9 File Offset: 0x0001CDF9
		protected override void SerializeItem(NetworkWriter writer, bool item)
		{
			writer.Write(item);
		}

		// Token: 0x0600057B RID: 1403 RVA: 0x0001EC04 File Offset: 0x0001CE04
		protected override bool DeserializeItem(NetworkReader reader)
		{
			return reader.ReadBoolean();
		}

		// Token: 0x0600057C RID: 1404 RVA: 0x0001EC20 File Offset: 0x0001CE20
		[Obsolete("ReadReference is now used instead")]
		public static SyncListBool ReadInstance(NetworkReader reader)
		{
			ushort num = reader.ReadUInt16();
			SyncListBool syncListBool = new SyncListBool();
			for (ushort num2 = 0; num2 < num; num2 += 1)
			{
				syncListBool.AddInternal(reader.ReadBoolean());
			}
			return syncListBool;
		}

		// Token: 0x0600057D RID: 1405 RVA: 0x0001EC64 File Offset: 0x0001CE64
		public static void ReadReference(NetworkReader reader, SyncListBool syncList)
		{
			ushort num = reader.ReadUInt16();
			syncList.Clear();
			for (ushort num2 = 0; num2 < num; num2 += 1)
			{
				syncList.AddInternal(reader.ReadBoolean());
			}
		}

		// Token: 0x0600057E RID: 1406 RVA: 0x0001ECA0 File Offset: 0x0001CEA0
		public static void WriteInstance(NetworkWriter writer, SyncListBool items)
		{
			writer.Write((ushort)items.Count);
			for (int i = 0; i < items.Count; i++)
			{
				writer.Write(items[i]);
			}
		}
	}
}
