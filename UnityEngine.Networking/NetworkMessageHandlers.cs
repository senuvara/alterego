﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Networking
{
	// Token: 0x02000050 RID: 80
	internal class NetworkMessageHandlers
	{
		// Token: 0x0600036F RID: 879 RVA: 0x000123CA File Offset: 0x000105CA
		public NetworkMessageHandlers()
		{
		}

		// Token: 0x06000370 RID: 880 RVA: 0x000123E0 File Offset: 0x000105E0
		internal void RegisterHandlerSafe(short msgType, NetworkMessageDelegate handler)
		{
			if (handler == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("RegisterHandlerSafe id:" + msgType + " handler is null");
				}
			}
			else
			{
				if (LogFilter.logDebug)
				{
					Debug.Log(string.Concat(new object[]
					{
						"RegisterHandlerSafe id:",
						msgType,
						" handler:",
						handler.GetMethodName()
					}));
				}
				if (!this.m_MsgHandlers.ContainsKey(msgType))
				{
					this.m_MsgHandlers.Add(msgType, handler);
				}
			}
		}

		// Token: 0x06000371 RID: 881 RVA: 0x00012484 File Offset: 0x00010684
		public void RegisterHandler(short msgType, NetworkMessageDelegate handler)
		{
			if (handler == null)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("RegisterHandler id:" + msgType + " handler is null");
				}
			}
			else if (msgType <= 31)
			{
				if (LogFilter.logError)
				{
					Debug.LogError("RegisterHandler: Cannot replace system message handler " + msgType);
				}
			}
			else
			{
				if (this.m_MsgHandlers.ContainsKey(msgType))
				{
					if (LogFilter.logDebug)
					{
						Debug.Log("RegisterHandler replacing " + msgType);
					}
					this.m_MsgHandlers.Remove(msgType);
				}
				if (LogFilter.logDebug)
				{
					Debug.Log(string.Concat(new object[]
					{
						"RegisterHandler id:",
						msgType,
						" handler:",
						handler.GetMethodName()
					}));
				}
				this.m_MsgHandlers.Add(msgType, handler);
			}
		}

		// Token: 0x06000372 RID: 882 RVA: 0x0001257F File Offset: 0x0001077F
		public void UnregisterHandler(short msgType)
		{
			this.m_MsgHandlers.Remove(msgType);
		}

		// Token: 0x06000373 RID: 883 RVA: 0x00012590 File Offset: 0x00010790
		internal NetworkMessageDelegate GetHandler(short msgType)
		{
			NetworkMessageDelegate result;
			if (this.m_MsgHandlers.ContainsKey(msgType))
			{
				result = this.m_MsgHandlers[msgType];
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06000374 RID: 884 RVA: 0x000125CC File Offset: 0x000107CC
		internal Dictionary<short, NetworkMessageDelegate> GetHandlers()
		{
			return this.m_MsgHandlers;
		}

		// Token: 0x06000375 RID: 885 RVA: 0x000125E7 File Offset: 0x000107E7
		internal void ClearMessageHandlers()
		{
			this.m_MsgHandlers.Clear();
		}

		// Token: 0x0400018F RID: 399
		private Dictionary<short, NetworkMessageDelegate> m_MsgHandlers = new Dictionary<short, NetworkMessageDelegate>();
	}
}
