﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000069 RID: 105
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public sealed class SyncListFloat : SyncList<float>
	{
		// Token: 0x06000567 RID: 1383 RVA: 0x0001E921 File Offset: 0x0001CB21
		public SyncListFloat()
		{
		}

		// Token: 0x06000568 RID: 1384 RVA: 0x0001E929 File Offset: 0x0001CB29
		protected override void SerializeItem(NetworkWriter writer, float item)
		{
			writer.Write(item);
		}

		// Token: 0x06000569 RID: 1385 RVA: 0x0001E934 File Offset: 0x0001CB34
		protected override float DeserializeItem(NetworkReader reader)
		{
			return reader.ReadSingle();
		}

		// Token: 0x0600056A RID: 1386 RVA: 0x0001E950 File Offset: 0x0001CB50
		[Obsolete("ReadReference is now used instead")]
		public static SyncListFloat ReadInstance(NetworkReader reader)
		{
			ushort num = reader.ReadUInt16();
			SyncListFloat syncListFloat = new SyncListFloat();
			for (ushort num2 = 0; num2 < num; num2 += 1)
			{
				syncListFloat.AddInternal(reader.ReadSingle());
			}
			return syncListFloat;
		}

		// Token: 0x0600056B RID: 1387 RVA: 0x0001E994 File Offset: 0x0001CB94
		public static void ReadReference(NetworkReader reader, SyncListFloat syncList)
		{
			ushort num = reader.ReadUInt16();
			syncList.Clear();
			for (ushort num2 = 0; num2 < num; num2 += 1)
			{
				syncList.AddInternal(reader.ReadSingle());
			}
		}

		// Token: 0x0600056C RID: 1388 RVA: 0x0001E9D0 File Offset: 0x0001CBD0
		public static void WriteInstance(NetworkWriter writer, SyncListFloat items)
		{
			writer.Write((ushort)items.Count);
			for (int i = 0; i < items.Count; i++)
			{
				writer.Write(items[i]);
			}
		}
	}
}
