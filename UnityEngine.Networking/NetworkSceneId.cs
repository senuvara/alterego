﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000059 RID: 89
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	[Serializable]
	public struct NetworkSceneId : IEquatable<NetworkSceneId>
	{
		// Token: 0x060003F3 RID: 1011 RVA: 0x00015A28 File Offset: 0x00013C28
		public NetworkSceneId(uint value)
		{
			this.m_Value = value;
		}

		// Token: 0x060003F4 RID: 1012 RVA: 0x00015A34 File Offset: 0x00013C34
		public bool IsEmpty()
		{
			return this.m_Value == 0U;
		}

		// Token: 0x060003F5 RID: 1013 RVA: 0x00015A54 File Offset: 0x00013C54
		public override int GetHashCode()
		{
			return (int)this.m_Value;
		}

		// Token: 0x060003F6 RID: 1014 RVA: 0x00015A70 File Offset: 0x00013C70
		public override bool Equals(object obj)
		{
			return obj is NetworkSceneId && this.Equals((NetworkSceneId)obj);
		}

		// Token: 0x060003F7 RID: 1015 RVA: 0x00015AA0 File Offset: 0x00013CA0
		public bool Equals(NetworkSceneId other)
		{
			return this == other;
		}

		// Token: 0x060003F8 RID: 1016 RVA: 0x00015AC4 File Offset: 0x00013CC4
		public static bool operator ==(NetworkSceneId c1, NetworkSceneId c2)
		{
			return c1.m_Value == c2.m_Value;
		}

		// Token: 0x060003F9 RID: 1017 RVA: 0x00015AEC File Offset: 0x00013CEC
		public static bool operator !=(NetworkSceneId c1, NetworkSceneId c2)
		{
			return c1.m_Value != c2.m_Value;
		}

		// Token: 0x060003FA RID: 1018 RVA: 0x00015B14 File Offset: 0x00013D14
		public override string ToString()
		{
			return this.m_Value.ToString();
		}

		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x060003FB RID: 1019 RVA: 0x00015B3C File Offset: 0x00013D3C
		public uint Value
		{
			get
			{
				return this.m_Value;
			}
		}

		// Token: 0x040001B8 RID: 440
		[SerializeField]
		private uint m_Value;
	}
}
