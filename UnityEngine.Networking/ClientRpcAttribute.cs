﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x0200000A RID: 10
	[AttributeUsage(AttributeTargets.Method)]
	[Obsolete("The high level API classes are deprecated and will be removed in the future.")]
	public class ClientRpcAttribute : Attribute
	{
		// Token: 0x06000069 RID: 105 RVA: 0x00004C55 File Offset: 0x00002E55
		public ClientRpcAttribute()
		{
		}

		// Token: 0x0400004E RID: 78
		public int channel = 0;
	}
}
