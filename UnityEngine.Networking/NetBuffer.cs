﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x0200003B RID: 59
	internal class NetBuffer
	{
		// Token: 0x06000181 RID: 385 RVA: 0x0000A768 File Offset: 0x00008968
		public NetBuffer()
		{
			this.m_Buffer = new byte[64];
		}

		// Token: 0x06000182 RID: 386 RVA: 0x0000A77E File Offset: 0x0000897E
		public NetBuffer(byte[] buffer)
		{
			this.m_Buffer = buffer;
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x06000183 RID: 387 RVA: 0x0000A790 File Offset: 0x00008990
		public uint Position
		{
			get
			{
				return this.m_Pos;
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x06000184 RID: 388 RVA: 0x0000A7AC File Offset: 0x000089AC
		public int Length
		{
			get
			{
				return this.m_Buffer.Length;
			}
		}

		// Token: 0x06000185 RID: 389 RVA: 0x0000A7CC File Offset: 0x000089CC
		public byte ReadByte()
		{
			if ((ulong)this.m_Pos >= (ulong)((long)this.m_Buffer.Length))
			{
				throw new IndexOutOfRangeException("NetworkReader:ReadByte out of range:" + this.ToString());
			}
			return this.m_Buffer[(int)((UIntPtr)(this.m_Pos++))];
		}

		// Token: 0x06000186 RID: 390 RVA: 0x0000A828 File Offset: 0x00008A28
		public void ReadBytes(byte[] buffer, uint count)
		{
			if ((ulong)(this.m_Pos + count) > (ulong)((long)this.m_Buffer.Length))
			{
				throw new IndexOutOfRangeException(string.Concat(new object[]
				{
					"NetworkReader:ReadBytes out of range: (",
					count,
					") ",
					this.ToString()
				}));
			}
			ushort num = 0;
			while ((uint)num < count)
			{
				buffer[(int)num] = this.m_Buffer[(int)((UIntPtr)(this.m_Pos + (uint)num))];
				num += 1;
			}
			this.m_Pos += count;
		}

		// Token: 0x06000187 RID: 391 RVA: 0x0000A8B8 File Offset: 0x00008AB8
		internal ArraySegment<byte> AsArraySegment()
		{
			return new ArraySegment<byte>(this.m_Buffer, 0, (int)this.m_Pos);
		}

		// Token: 0x06000188 RID: 392 RVA: 0x0000A8DF File Offset: 0x00008ADF
		public void WriteByte(byte value)
		{
			this.WriteCheckForSpace(1);
			this.m_Buffer[(int)((UIntPtr)this.m_Pos)] = value;
			this.m_Pos += 1U;
		}

		// Token: 0x06000189 RID: 393 RVA: 0x0000A906 File Offset: 0x00008B06
		public void WriteByte2(byte value0, byte value1)
		{
			this.WriteCheckForSpace(2);
			this.m_Buffer[(int)((UIntPtr)this.m_Pos)] = value0;
			this.m_Buffer[(int)((UIntPtr)(this.m_Pos + 1U))] = value1;
			this.m_Pos += 2U;
		}

		// Token: 0x0600018A RID: 394 RVA: 0x0000A940 File Offset: 0x00008B40
		public void WriteByte4(byte value0, byte value1, byte value2, byte value3)
		{
			this.WriteCheckForSpace(4);
			this.m_Buffer[(int)((UIntPtr)this.m_Pos)] = value0;
			this.m_Buffer[(int)((UIntPtr)(this.m_Pos + 1U))] = value1;
			this.m_Buffer[(int)((UIntPtr)(this.m_Pos + 2U))] = value2;
			this.m_Buffer[(int)((UIntPtr)(this.m_Pos + 3U))] = value3;
			this.m_Pos += 4U;
		}

		// Token: 0x0600018B RID: 395 RVA: 0x0000A9A8 File Offset: 0x00008BA8
		public void WriteByte8(byte value0, byte value1, byte value2, byte value3, byte value4, byte value5, byte value6, byte value7)
		{
			this.WriteCheckForSpace(8);
			this.m_Buffer[(int)((UIntPtr)this.m_Pos)] = value0;
			this.m_Buffer[(int)((UIntPtr)(this.m_Pos + 1U))] = value1;
			this.m_Buffer[(int)((UIntPtr)(this.m_Pos + 2U))] = value2;
			this.m_Buffer[(int)((UIntPtr)(this.m_Pos + 3U))] = value3;
			this.m_Buffer[(int)((UIntPtr)(this.m_Pos + 4U))] = value4;
			this.m_Buffer[(int)((UIntPtr)(this.m_Pos + 5U))] = value5;
			this.m_Buffer[(int)((UIntPtr)(this.m_Pos + 6U))] = value6;
			this.m_Buffer[(int)((UIntPtr)(this.m_Pos + 7U))] = value7;
			this.m_Pos += 8U;
		}

		// Token: 0x0600018C RID: 396 RVA: 0x0000AA58 File Offset: 0x00008C58
		public void WriteBytesAtOffset(byte[] buffer, ushort targetOffset, ushort count)
		{
			uint num = (uint)(count + targetOffset);
			this.WriteCheckForSpace((ushort)num);
			if (targetOffset == 0 && (int)count == buffer.Length)
			{
				buffer.CopyTo(this.m_Buffer, (int)this.m_Pos);
			}
			else
			{
				for (int i = 0; i < (int)count; i++)
				{
					this.m_Buffer[(int)targetOffset + i] = buffer[i];
				}
			}
			if (num > this.m_Pos)
			{
				this.m_Pos = num;
			}
		}

		// Token: 0x0600018D RID: 397 RVA: 0x0000AAD4 File Offset: 0x00008CD4
		public void WriteBytes(byte[] buffer, ushort count)
		{
			this.WriteCheckForSpace(count);
			if ((int)count == buffer.Length)
			{
				buffer.CopyTo(this.m_Buffer, (int)this.m_Pos);
			}
			else
			{
				for (int i = 0; i < (int)count; i++)
				{
					this.m_Buffer[(int)(checked((IntPtr)(unchecked((ulong)this.m_Pos + (ulong)((long)i)))))] = buffer[i];
				}
			}
			this.m_Pos += (uint)count;
		}

		// Token: 0x0600018E RID: 398 RVA: 0x0000AB44 File Offset: 0x00008D44
		private void WriteCheckForSpace(ushort count)
		{
			if ((ulong)(this.m_Pos + (uint)count) >= (ulong)((long)this.m_Buffer.Length))
			{
				int num = (int)Math.Ceiling((double)((float)this.m_Buffer.Length * 1.5f));
				while ((ulong)(this.m_Pos + (uint)count) >= (ulong)((long)num))
				{
					num = (int)Math.Ceiling((double)((float)num * 1.5f));
					if (num > 134217728)
					{
						Debug.LogWarning("NetworkBuffer size is " + num + " bytes!");
					}
				}
				byte[] array = new byte[num];
				this.m_Buffer.CopyTo(array, 0);
				this.m_Buffer = array;
			}
		}

		// Token: 0x0600018F RID: 399 RVA: 0x0000ABF0 File Offset: 0x00008DF0
		public void FinishMessage()
		{
			ushort num = (ushort)(this.m_Pos - 4U);
			this.m_Buffer[0] = (byte)(num & 255);
			this.m_Buffer[1] = (byte)(num >> 8 & 255);
		}

		// Token: 0x06000190 RID: 400 RVA: 0x0000AC2A File Offset: 0x00008E2A
		public void SeekZero()
		{
			this.m_Pos = 0U;
		}

		// Token: 0x06000191 RID: 401 RVA: 0x0000AC34 File Offset: 0x00008E34
		public void Replace(byte[] buffer)
		{
			this.m_Buffer = buffer;
			this.m_Pos = 0U;
		}

		// Token: 0x06000192 RID: 402 RVA: 0x0000AC48 File Offset: 0x00008E48
		public override string ToString()
		{
			return string.Format("NetBuf sz:{0} pos:{1}", this.m_Buffer.Length, this.m_Pos);
		}

		// Token: 0x040000C3 RID: 195
		private byte[] m_Buffer;

		// Token: 0x040000C4 RID: 196
		private uint m_Pos;

		// Token: 0x040000C5 RID: 197
		private const int k_InitialSize = 64;

		// Token: 0x040000C6 RID: 198
		private const float k_GrowthFactor = 1.5f;

		// Token: 0x040000C7 RID: 199
		private const int k_BufferSizeWarning = 134217728;
	}
}
