﻿using System;
using System.Collections.Generic;
using UnityEngine.Bindings;

namespace UnityEngine.StyleSheets
{
	// Token: 0x02000009 RID: 9
	[VisibleToOtherModules(new string[]
	{
		"UnityEngine.UIElementsModule"
	})]
	[Serializable]
	internal class StyleSheet : ScriptableObject
	{
		// Token: 0x06000026 RID: 38 RVA: 0x00002476 File Offset: 0x00000676
		public StyleSheet()
		{
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000027 RID: 39 RVA: 0x00002480 File Offset: 0x00000680
		// (set) Token: 0x06000028 RID: 40 RVA: 0x0000249B File Offset: 0x0000069B
		public StyleRule[] rules
		{
			get
			{
				return this.m_Rules;
			}
			internal set
			{
				this.m_Rules = value;
				this.SetupReferences();
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000029 RID: 41 RVA: 0x000024AC File Offset: 0x000006AC
		// (set) Token: 0x0600002A RID: 42 RVA: 0x000024C7 File Offset: 0x000006C7
		public StyleComplexSelector[] complexSelectors
		{
			get
			{
				return this.m_ComplexSelectors;
			}
			internal set
			{
				this.m_ComplexSelectors = value;
				this.SetupReferences();
			}
		}

		// Token: 0x0600002B RID: 43 RVA: 0x000024D8 File Offset: 0x000006D8
		private static bool TryCheckAccess<T>(T[] list, StyleValueType type, StyleValueHandle[] handles, int index, out T value)
		{
			bool result = false;
			value = default(T);
			if (index < handles.Length)
			{
				StyleValueHandle styleValueHandle = handles[index];
				if (styleValueHandle.valueType == type && styleValueHandle.valueIndex >= 0 && styleValueHandle.valueIndex < list.Length)
				{
					value = list[styleValueHandle.valueIndex];
					result = true;
				}
				else
				{
					Debug.LogErrorFormat("Trying to read value of type {0} while reading a value of type {1}", new object[]
					{
						type,
						styleValueHandle.valueType
					});
				}
			}
			return result;
		}

		// Token: 0x0600002C RID: 44 RVA: 0x00002588 File Offset: 0x00000788
		private static T CheckAccess<T>(T[] list, StyleValueType type, StyleValueHandle handle)
		{
			T result = default(T);
			if (handle.valueType != type)
			{
				Debug.LogErrorFormat("Trying to read value of type {0} while reading a value of type {1}", new object[]
				{
					type,
					handle.valueType
				});
			}
			else if (list == null || handle.valueIndex < 0 || handle.valueIndex >= list.Length)
			{
				Debug.LogError("Accessing invalid property");
			}
			else
			{
				result = list[handle.valueIndex];
			}
			return result;
		}

		// Token: 0x0600002D RID: 45 RVA: 0x00002627 File Offset: 0x00000827
		private void OnEnable()
		{
			this.hasSelectorsCached = false;
			this.SetupReferences();
		}

		// Token: 0x0600002E RID: 46 RVA: 0x00002638 File Offset: 0x00000838
		private void SetupReferences()
		{
			if (this.complexSelectors != null && this.rules != null)
			{
				this.orderedClassSelectors = new Dictionary<string, StyleComplexSelector>(StringComparer.Ordinal);
				this.orderedNameSelectors = new Dictionary<string, StyleComplexSelector>(StringComparer.Ordinal);
				this.orderedTypeSelectors = new Dictionary<string, StyleComplexSelector>(StringComparer.Ordinal);
				int i = 0;
				while (i < this.complexSelectors.Length)
				{
					StyleComplexSelector styleComplexSelector = this.complexSelectors[i];
					if (styleComplexSelector.ruleIndex < this.rules.Length)
					{
						styleComplexSelector.rule = this.rules[styleComplexSelector.ruleIndex];
					}
					styleComplexSelector.orderInStyleSheet = i;
					StyleSelector styleSelector = styleComplexSelector.selectors[styleComplexSelector.selectors.Length - 1];
					StyleSelectorPart styleSelectorPart = styleSelector.parts[0];
					string key = styleSelectorPart.value;
					Dictionary<string, StyleComplexSelector> dictionary = null;
					switch (styleSelectorPart.type)
					{
					case StyleSelectorType.Wildcard:
					case StyleSelectorType.Type:
						key = (styleSelectorPart.value ?? "*");
						dictionary = this.orderedTypeSelectors;
						break;
					case StyleSelectorType.Class:
						dictionary = this.orderedClassSelectors;
						break;
					case StyleSelectorType.PseudoClass:
						key = "*";
						dictionary = this.orderedTypeSelectors;
						break;
					case StyleSelectorType.RecursivePseudoClass:
						goto IL_13B;
					case StyleSelectorType.ID:
						dictionary = this.orderedNameSelectors;
						break;
					default:
						goto IL_13B;
					}
					IL_15B:
					if (dictionary != null)
					{
						StyleComplexSelector nextInTable;
						if (dictionary.TryGetValue(key, out nextInTable))
						{
							styleComplexSelector.nextInTable = nextInTable;
						}
						dictionary[key] = styleComplexSelector;
					}
					i++;
					continue;
					IL_13B:
					Debug.LogError(string.Format("Invalid first part type {0}", styleSelectorPart.type));
					goto IL_15B;
				}
			}
		}

		// Token: 0x0600002F RID: 47 RVA: 0x000027E0 File Offset: 0x000009E0
		public StyleValueKeyword ReadKeyword(StyleValueHandle handle)
		{
			return (StyleValueKeyword)handle.valueIndex;
		}

		// Token: 0x06000030 RID: 48 RVA: 0x000027FC File Offset: 0x000009FC
		public float ReadFloat(StyleValueHandle handle)
		{
			return StyleSheet.CheckAccess<float>(this.floats, StyleValueType.Float, handle);
		}

		// Token: 0x06000031 RID: 49 RVA: 0x00002820 File Offset: 0x00000A20
		public bool TryReadFloat(StyleValueHandle[] handles, int index, out float value)
		{
			return StyleSheet.TryCheckAccess<float>(this.floats, StyleValueType.Float, handles, index, out value);
		}

		// Token: 0x06000032 RID: 50 RVA: 0x00002844 File Offset: 0x00000A44
		public Color ReadColor(StyleValueHandle handle)
		{
			return StyleSheet.CheckAccess<Color>(this.colors, StyleValueType.Color, handle);
		}

		// Token: 0x06000033 RID: 51 RVA: 0x00002868 File Offset: 0x00000A68
		public bool TryReadColor(StyleValueHandle[] handles, int index, out Color value)
		{
			return StyleSheet.TryCheckAccess<Color>(this.colors, StyleValueType.Color, handles, index, out value);
		}

		// Token: 0x06000034 RID: 52 RVA: 0x0000288C File Offset: 0x00000A8C
		public string ReadString(StyleValueHandle handle)
		{
			return StyleSheet.CheckAccess<string>(this.strings, StyleValueType.String, handle);
		}

		// Token: 0x06000035 RID: 53 RVA: 0x000028B0 File Offset: 0x00000AB0
		public bool TryReadString(StyleValueHandle[] handles, int index, out string value)
		{
			return StyleSheet.TryCheckAccess<string>(this.strings, StyleValueType.String, handles, index, out value);
		}

		// Token: 0x06000036 RID: 54 RVA: 0x000028D4 File Offset: 0x00000AD4
		public string ReadEnum(StyleValueHandle handle)
		{
			return StyleSheet.CheckAccess<string>(this.strings, StyleValueType.Enum, handle);
		}

		// Token: 0x06000037 RID: 55 RVA: 0x000028F8 File Offset: 0x00000AF8
		public bool TryReadEnum(StyleValueHandle[] handles, int index, out string value)
		{
			return StyleSheet.TryCheckAccess<string>(this.strings, StyleValueType.Enum, handles, index, out value);
		}

		// Token: 0x06000038 RID: 56 RVA: 0x0000291C File Offset: 0x00000B1C
		public string ReadResourcePath(StyleValueHandle handle)
		{
			return StyleSheet.CheckAccess<string>(this.strings, StyleValueType.ResourcePath, handle);
		}

		// Token: 0x06000039 RID: 57 RVA: 0x00002940 File Offset: 0x00000B40
		public bool TryReadResourcePath(StyleValueHandle[] handles, int index, out string value)
		{
			return StyleSheet.TryCheckAccess<string>(this.strings, StyleValueType.ResourcePath, handles, index, out value);
		}

		// Token: 0x0600003A RID: 58 RVA: 0x00002964 File Offset: 0x00000B64
		public Object ReadAssetReference(StyleValueHandle handle)
		{
			return StyleSheet.CheckAccess<Object>(this.assets, StyleValueType.AssetReference, handle);
		}

		// Token: 0x0600003B RID: 59 RVA: 0x00002988 File Offset: 0x00000B88
		public bool TryReadAssetReference(StyleValueHandle[] handles, int index, out Object value)
		{
			return StyleSheet.TryCheckAccess<Object>(this.assets, StyleValueType.AssetReference, handles, index, out value);
		}

		// Token: 0x04000021 RID: 33
		[SerializeField]
		private StyleRule[] m_Rules;

		// Token: 0x04000022 RID: 34
		[SerializeField]
		private StyleComplexSelector[] m_ComplexSelectors;

		// Token: 0x04000023 RID: 35
		[SerializeField]
		internal float[] floats;

		// Token: 0x04000024 RID: 36
		[SerializeField]
		internal Color[] colors;

		// Token: 0x04000025 RID: 37
		[SerializeField]
		internal string[] strings;

		// Token: 0x04000026 RID: 38
		[SerializeField]
		internal Object[] assets;

		// Token: 0x04000027 RID: 39
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		[NonSerialized]
		internal Dictionary<string, StyleComplexSelector> orderedNameSelectors;

		// Token: 0x04000028 RID: 40
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		[NonSerialized]
		internal Dictionary<string, StyleComplexSelector> orderedTypeSelectors;

		// Token: 0x04000029 RID: 41
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		[NonSerialized]
		internal Dictionary<string, StyleComplexSelector> orderedClassSelectors;

		// Token: 0x0400002A RID: 42
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		[NonSerialized]
		internal bool hasSelectorsCached;
	}
}
