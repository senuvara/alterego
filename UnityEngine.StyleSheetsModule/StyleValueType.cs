﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine.StyleSheets
{
	// Token: 0x0200000C RID: 12
	[VisibleToOtherModules(new string[]
	{
		"UnityEngine.UIElementsModule"
	})]
	internal enum StyleValueType
	{
		// Token: 0x04000035 RID: 53
		Keyword,
		// Token: 0x04000036 RID: 54
		Float,
		// Token: 0x04000037 RID: 55
		Color,
		// Token: 0x04000038 RID: 56
		ResourcePath,
		// Token: 0x04000039 RID: 57
		AssetReference,
		// Token: 0x0400003A RID: 58
		Enum,
		// Token: 0x0400003B RID: 59
		String
	}
}
