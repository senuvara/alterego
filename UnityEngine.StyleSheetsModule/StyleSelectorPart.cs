﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine.StyleSheets
{
	// Token: 0x02000006 RID: 6
	[VisibleToOtherModules(new string[]
	{
		"UnityEngine.UIElementsModule"
	})]
	[Serializable]
	internal struct StyleSelectorPart
	{
		// Token: 0x1700000A RID: 10
		// (get) Token: 0x0600001A RID: 26 RVA: 0x000022AC File Offset: 0x000004AC
		// (set) Token: 0x0600001B RID: 27 RVA: 0x000022C7 File Offset: 0x000004C7
		public string value
		{
			get
			{
				return this.m_Value;
			}
			internal set
			{
				this.m_Value = value;
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x0600001C RID: 28 RVA: 0x000022D4 File Offset: 0x000004D4
		// (set) Token: 0x0600001D RID: 29 RVA: 0x000022EF File Offset: 0x000004EF
		public StyleSelectorType type
		{
			get
			{
				return this.m_Type;
			}
			[VisibleToOtherModules(new string[]
			{
				"UnityEngine.UIElementsModule"
			})]
			internal set
			{
				this.m_Type = value;
			}
		}

		// Token: 0x0600001E RID: 30 RVA: 0x000022FC File Offset: 0x000004FC
		public override string ToString()
		{
			return string.Format("[StyleSelectorPart: value={0}, type={1}]", this.value, this.type);
		}

		// Token: 0x0600001F RID: 31 RVA: 0x0000232C File Offset: 0x0000052C
		public static StyleSelectorPart CreateClass(string className)
		{
			return new StyleSelectorPart
			{
				m_Type = StyleSelectorType.Class,
				m_Value = className
			};
		}

		// Token: 0x06000020 RID: 32 RVA: 0x0000235C File Offset: 0x0000055C
		public static StyleSelectorPart CreatePseudoClass(string className)
		{
			return new StyleSelectorPart
			{
				m_Type = StyleSelectorType.PseudoClass,
				m_Value = className
			};
		}

		// Token: 0x06000021 RID: 33 RVA: 0x0000238C File Offset: 0x0000058C
		public static StyleSelectorPart CreateId(string Id)
		{
			return new StyleSelectorPart
			{
				m_Type = StyleSelectorType.ID,
				m_Value = Id
			};
		}

		// Token: 0x06000022 RID: 34 RVA: 0x000023BC File Offset: 0x000005BC
		public static StyleSelectorPart CreateType(Type t)
		{
			return new StyleSelectorPart
			{
				m_Type = StyleSelectorType.Type,
				m_Value = t.Name
			};
		}

		// Token: 0x06000023 RID: 35 RVA: 0x000023F0 File Offset: 0x000005F0
		public static StyleSelectorPart CreateType(string typeName)
		{
			return new StyleSelectorPart
			{
				m_Type = StyleSelectorType.Type,
				m_Value = typeName
			};
		}

		// Token: 0x06000024 RID: 36 RVA: 0x00002420 File Offset: 0x00000620
		public static StyleSelectorPart CreatePredicate(object predicate)
		{
			return new StyleSelectorPart
			{
				m_Type = StyleSelectorType.Predicate,
				tempData = predicate
			};
		}

		// Token: 0x06000025 RID: 37 RVA: 0x00002450 File Offset: 0x00000650
		public static StyleSelectorPart CreateWildCard()
		{
			return new StyleSelectorPart
			{
				m_Type = StyleSelectorType.Wildcard
			};
		}

		// Token: 0x04000011 RID: 17
		[SerializeField]
		private string m_Value;

		// Token: 0x04000012 RID: 18
		[SerializeField]
		private StyleSelectorType m_Type;

		// Token: 0x04000013 RID: 19
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal object tempData;
	}
}
