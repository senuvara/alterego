﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine.StyleSheets
{
	// Token: 0x02000003 RID: 3
	[VisibleToOtherModules(new string[]
	{
		"UnityEngine.UIElementsModule"
	})]
	[Serializable]
	internal class StyleProperty
	{
		// Token: 0x0600000B RID: 11 RVA: 0x00002050 File Offset: 0x00000250
		public StyleProperty()
		{
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x0600000C RID: 12 RVA: 0x00002160 File Offset: 0x00000360
		// (set) Token: 0x0600000D RID: 13 RVA: 0x0000217B File Offset: 0x0000037B
		public string name
		{
			get
			{
				return this.m_Name;
			}
			internal set
			{
				this.m_Name = value;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x0600000E RID: 14 RVA: 0x00002188 File Offset: 0x00000388
		// (set) Token: 0x0600000F RID: 15 RVA: 0x000021A3 File Offset: 0x000003A3
		public StyleValueHandle[] values
		{
			get
			{
				return this.m_Values;
			}
			internal set
			{
				this.m_Values = value;
			}
		}

		// Token: 0x04000008 RID: 8
		[SerializeField]
		private string m_Name;

		// Token: 0x04000009 RID: 9
		[SerializeField]
		private StyleValueHandle[] m_Values;
	}
}
