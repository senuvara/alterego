﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine.StyleSheets
{
	// Token: 0x02000007 RID: 7
	[VisibleToOtherModules(new string[]
	{
		"UnityEngine.UIElementsModule"
	})]
	internal enum StyleSelectorRelationship
	{
		// Token: 0x04000015 RID: 21
		None,
		// Token: 0x04000016 RID: 22
		Child,
		// Token: 0x04000017 RID: 23
		Descendent
	}
}
