﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.StyleSheets
{
	// Token: 0x02000005 RID: 5
	[VisibleToOtherModules(new string[]
	{
		"UnityEngine.UIElementsModule"
	})]
	[Serializable]
	internal class StyleSelector
	{
		// Token: 0x06000013 RID: 19 RVA: 0x000021D5 File Offset: 0x000003D5
		public StyleSelector()
		{
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000014 RID: 20 RVA: 0x000021EC File Offset: 0x000003EC
		// (set) Token: 0x06000015 RID: 21 RVA: 0x00002207 File Offset: 0x00000407
		public StyleSelectorPart[] parts
		{
			get
			{
				return this.m_Parts;
			}
			[VisibleToOtherModules(new string[]
			{
				"UnityEngine.UIElementsModule"
			})]
			internal set
			{
				this.m_Parts = value;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000016 RID: 22 RVA: 0x00002214 File Offset: 0x00000414
		// (set) Token: 0x06000017 RID: 23 RVA: 0x0000222F File Offset: 0x0000042F
		public StyleSelectorRelationship previousRelationship
		{
			get
			{
				return this.m_PreviousRelationship;
			}
			[VisibleToOtherModules(new string[]
			{
				"UnityEngine.UIElementsModule"
			})]
			internal set
			{
				this.m_PreviousRelationship = value;
			}
		}

		// Token: 0x06000018 RID: 24 RVA: 0x0000223C File Offset: 0x0000043C
		public override string ToString()
		{
			return string.Join(", ", (from p in this.parts
			select p.ToString()).ToArray<string>());
		}

		// Token: 0x06000019 RID: 25 RVA: 0x00002288 File Offset: 0x00000488
		[CompilerGenerated]
		private static string <ToString>m__0(StyleSelectorPart p)
		{
			return p.ToString();
		}

		// Token: 0x0400000C RID: 12
		[SerializeField]
		private StyleSelectorPart[] m_Parts;

		// Token: 0x0400000D RID: 13
		[SerializeField]
		private StyleSelectorRelationship m_PreviousRelationship;

		// Token: 0x0400000E RID: 14
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal int pseudoStateMask = -1;

		// Token: 0x0400000F RID: 15
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal int negatedPseudoStateMask = -1;

		// Token: 0x04000010 RID: 16
		[CompilerGenerated]
		private static Func<StyleSelectorPart, string> <>f__am$cache0;
	}
}
