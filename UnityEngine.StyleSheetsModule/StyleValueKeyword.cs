﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine.StyleSheets
{
	// Token: 0x0200000B RID: 11
	[VisibleToOtherModules(new string[]
	{
		"UnityEngine.UIElementsModule"
	})]
	internal enum StyleValueKeyword
	{
		// Token: 0x0400002E RID: 46
		Inherit,
		// Token: 0x0400002F RID: 47
		Auto,
		// Token: 0x04000030 RID: 48
		Unset,
		// Token: 0x04000031 RID: 49
		True,
		// Token: 0x04000032 RID: 50
		False,
		// Token: 0x04000033 RID: 51
		None
	}
}
