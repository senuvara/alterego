﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine.StyleSheets
{
	// Token: 0x02000008 RID: 8
	[VisibleToOtherModules(new string[]
	{
		"UnityEngine.UIElementsModule"
	})]
	internal enum StyleSelectorType
	{
		// Token: 0x04000019 RID: 25
		Unknown,
		// Token: 0x0400001A RID: 26
		Wildcard,
		// Token: 0x0400001B RID: 27
		Type,
		// Token: 0x0400001C RID: 28
		Class,
		// Token: 0x0400001D RID: 29
		PseudoClass,
		// Token: 0x0400001E RID: 30
		RecursivePseudoClass,
		// Token: 0x0400001F RID: 31
		ID,
		// Token: 0x04000020 RID: 32
		Predicate
	}
}
