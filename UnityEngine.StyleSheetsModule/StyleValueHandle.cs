﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine.StyleSheets
{
	// Token: 0x0200000A RID: 10
	[VisibleToOtherModules(new string[]
	{
		"UnityEngine.UIElementsModule"
	})]
	[Serializable]
	internal struct StyleValueHandle
	{
		// Token: 0x0600003C RID: 60 RVA: 0x000029AC File Offset: 0x00000BAC
		internal StyleValueHandle(int valueIndex, StyleValueType valueType)
		{
			this.valueIndex = valueIndex;
			this.m_ValueType = valueType;
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x0600003D RID: 61 RVA: 0x000029C0 File Offset: 0x00000BC0
		// (set) Token: 0x0600003E RID: 62 RVA: 0x000029DB File Offset: 0x00000BDB
		public StyleValueType valueType
		{
			get
			{
				return this.m_ValueType;
			}
			internal set
			{
				this.m_ValueType = value;
			}
		}

		// Token: 0x0400002B RID: 43
		[SerializeField]
		private StyleValueType m_ValueType;

		// Token: 0x0400002C RID: 44
		[SerializeField]
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal int valueIndex;
	}
}
