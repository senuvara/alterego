﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine.StyleSheets
{
	// Token: 0x02000004 RID: 4
	[VisibleToOtherModules(new string[]
	{
		"UnityEngine.UIElementsModule"
	})]
	[Serializable]
	internal class StyleRule
	{
		// Token: 0x06000010 RID: 16 RVA: 0x00002050 File Offset: 0x00000250
		public StyleRule()
		{
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000011 RID: 17 RVA: 0x000021B0 File Offset: 0x000003B0
		// (set) Token: 0x06000012 RID: 18 RVA: 0x000021CB File Offset: 0x000003CB
		public StyleProperty[] properties
		{
			get
			{
				return this.m_Properties;
			}
			internal set
			{
				this.m_Properties = value;
			}
		}

		// Token: 0x0400000A RID: 10
		[SerializeField]
		private StyleProperty[] m_Properties;

		// Token: 0x0400000B RID: 11
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		[SerializeField]
		internal int line;
	}
}
