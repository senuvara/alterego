﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.StyleSheets
{
	// Token: 0x02000002 RID: 2
	[VisibleToOtherModules(new string[]
	{
		"UnityEngine.UIElementsModule"
	})]
	[Serializable]
	internal class StyleComplexSelector
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public StyleComplexSelector()
		{
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000002 RID: 2 RVA: 0x00002058 File Offset: 0x00000258
		// (set) Token: 0x06000003 RID: 3 RVA: 0x00002073 File Offset: 0x00000273
		public int specificity
		{
			get
			{
				return this.m_Specificity;
			}
			internal set
			{
				this.m_Specificity = value;
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000004 RID: 4 RVA: 0x00002080 File Offset: 0x00000280
		// (set) Token: 0x06000005 RID: 5 RVA: 0x0000209A File Offset: 0x0000029A
		public StyleRule rule
		{
			[CompilerGenerated]
			get
			{
				return this.<rule>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<rule>k__BackingField = value;
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000006 RID: 6 RVA: 0x000020A4 File Offset: 0x000002A4
		public bool isSimple
		{
			get
			{
				return this.selectors.Length == 1;
			}
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000007 RID: 7 RVA: 0x000020C4 File Offset: 0x000002C4
		// (set) Token: 0x06000008 RID: 8 RVA: 0x000020DF File Offset: 0x000002DF
		public StyleSelector[] selectors
		{
			get
			{
				return this.m_Selectors;
			}
			[VisibleToOtherModules(new string[]
			{
				"UnityEngine.UIElementsModule"
			})]
			internal set
			{
				this.m_Selectors = value;
			}
		}

		// Token: 0x06000009 RID: 9 RVA: 0x000020EC File Offset: 0x000002EC
		public override string ToString()
		{
			return string.Format("[{0}]", string.Join(", ", (from x in this.m_Selectors
			select x.ToString()).ToArray<string>()));
		}

		// Token: 0x0600000A RID: 10 RVA: 0x00002144 File Offset: 0x00000344
		[CompilerGenerated]
		private static string <ToString>m__0(StyleSelector x)
		{
			return x.ToString();
		}

		// Token: 0x04000001 RID: 1
		[SerializeField]
		private int m_Specificity;

		// Token: 0x04000002 RID: 2
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private StyleRule <rule>k__BackingField;

		// Token: 0x04000003 RID: 3
		[SerializeField]
		private StyleSelector[] m_Selectors;

		// Token: 0x04000004 RID: 4
		[SerializeField]
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		internal int ruleIndex;

		// Token: 0x04000005 RID: 5
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		[NonSerialized]
		internal StyleComplexSelector nextInTable;

		// Token: 0x04000006 RID: 6
		[VisibleToOtherModules(new string[]
		{
			"UnityEngine.UIElementsModule"
		})]
		[NonSerialized]
		internal int orderInStyleSheet;

		// Token: 0x04000007 RID: 7
		[CompilerGenerated]
		private static Func<StyleSelector, string> <>f__am$cache0;
	}
}
