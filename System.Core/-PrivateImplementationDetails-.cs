﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Token: 0x02000485 RID: 1157
[CompilerGenerated]
internal sealed class <PrivateImplementationDetails>
{
	// Token: 0x04000CF0 RID: 3312 RVA: 0x00054424 File Offset: 0x00052624
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=120 0AA802CD6847EB893FE786B5EA5168B2FDCD7B93;

	// Token: 0x04000CF1 RID: 3313 RVA: 0x0005449C File Offset: 0x0005269C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=256 0C4110BC17D746F018F47B49E0EB0D6590F69939;

	// Token: 0x04000CF2 RID: 3314 RVA: 0x0005459C File Offset: 0x0005279C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1024 20733E1283D873EBE47133A95C233E11B76F5F11;

	// Token: 0x04000CF3 RID: 3315 RVA: 0x0005499C File Offset: 0x00052B9C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1024 21F4CBF8283FF1CAEB4A39316A97FC1D6DF1D35E;

	// Token: 0x04000CF4 RID: 3316 RVA: 0x00054D9C File Offset: 0x00052F9C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1024 23DFDCA6F045D4257BF5AC8CB1CF2EFADAFE9B94;

	// Token: 0x04000CF5 RID: 3317 RVA: 0x0005519C File Offset: 0x0005339C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1024 30A0358B25B1372DD598BB4B1AC56AD6B8F08A47;

	// Token: 0x04000CF6 RID: 3318 RVA: 0x0005559C File Offset: 0x0005379C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1024 5B5DF5A459E902D96F7DB0FB235A25346CA85C5D;

	// Token: 0x04000CF7 RID: 3319 RVA: 0x0005599C File Offset: 0x00053B9C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1024 5BE411F1438EAEF33726D855E99011D5FECDDD4E;

	// Token: 0x04000CF8 RID: 3320 RVA: 0x00055D9C File Offset: 0x00053F9C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=256 8F22C9ECE1331718CBD268A9BBFD2F5E451441E3;

	// Token: 0x04000CF9 RID: 3321 RVA: 0x00055E9C File Offset: 0x0005409C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1024 A02DD1D8604EA8EC2D2BDA717A93A4EE85F13E53;

	// Token: 0x04000CFA RID: 3322 RVA: 0x0005629C File Offset: 0x0005449C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1024 AE2F76ECEF8B08F0BC7EA95DCFE945E1727927C9;

	// Token: 0x02000486 RID: 1158
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 120)]
	private struct __StaticArrayInitTypeSize=120
	{
	}

	// Token: 0x02000487 RID: 1159
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 256)]
	private struct __StaticArrayInitTypeSize=256
	{
	}

	// Token: 0x02000488 RID: 1160
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 1024)]
	private struct __StaticArrayInitTypeSize=1024
	{
	}
}
