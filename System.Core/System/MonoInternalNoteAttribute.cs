﻿using System;

namespace System
{
	// Token: 0x0200000E RID: 14
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
	internal class MonoInternalNoteAttribute : MonoTODOAttribute
	{
		// Token: 0x06000029 RID: 41 RVA: 0x000022D5 File Offset: 0x000004D5
		public MonoInternalNoteAttribute(string comment) : base(comment)
		{
		}
	}
}
