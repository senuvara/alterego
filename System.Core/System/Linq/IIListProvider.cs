﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Linq
{
	// Token: 0x020000D5 RID: 213
	internal interface IIListProvider<TElement> : IEnumerable<!0>, IEnumerable
	{
		// Token: 0x0600079A RID: 1946
		TElement[] ToArray();

		// Token: 0x0600079B RID: 1947
		List<TElement> ToList();

		// Token: 0x0600079C RID: 1948
		int GetCount(bool onlyIfCheap);
	}
}
