﻿using System;
using System.Collections.Generic;

namespace System.Linq
{
	// Token: 0x020000D1 RID: 209
	internal struct Buffer<TElement>
	{
		// Token: 0x06000792 RID: 1938 RVA: 0x00016A0C File Offset: 0x00014C0C
		internal Buffer(IEnumerable<TElement> source)
		{
			TElement[] array = null;
			int num = 0;
			ICollection<TElement> collection = source as ICollection<!0>;
			if (collection != null)
			{
				num = collection.Count;
				if (num > 0)
				{
					array = new TElement[num];
					collection.CopyTo(array, 0);
				}
			}
			else
			{
				foreach (TElement telement in source)
				{
					if (array == null)
					{
						array = new TElement[4];
					}
					else if (array.Length == num)
					{
						TElement[] array2 = new TElement[checked(num * 2)];
						Array.Copy(array, 0, array2, 0, num);
						array = array2;
					}
					array[num] = telement;
					num++;
				}
			}
			this.items = array;
			this.count = num;
		}

		// Token: 0x06000793 RID: 1939 RVA: 0x00016AC0 File Offset: 0x00014CC0
		internal TElement[] ToArray()
		{
			if (this.count == 0)
			{
				return new TElement[0];
			}
			if (this.items.Length == this.count)
			{
				return this.items;
			}
			TElement[] array = new TElement[this.count];
			Array.Copy(this.items, 0, array, 0, this.count);
			return array;
		}

		// Token: 0x04000507 RID: 1287
		internal TElement[] items;

		// Token: 0x04000508 RID: 1288
		internal int count;
	}
}
