﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace System.Linq
{
	// Token: 0x020000D2 RID: 210
	internal sealed class SystemCore_EnumerableDebugView<T>
	{
		// Token: 0x06000794 RID: 1940 RVA: 0x00016B14 File Offset: 0x00014D14
		public SystemCore_EnumerableDebugView(IEnumerable<T> enumerable)
		{
			if (enumerable == null)
			{
				throw new ArgumentNullException("enumerable");
			}
			this.enumerable = enumerable;
		}

		// Token: 0x17000115 RID: 277
		// (get) Token: 0x06000795 RID: 1941 RVA: 0x00016B34 File Offset: 0x00014D34
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public T[] Items
		{
			get
			{
				List<T> list = new List<T>();
				IEnumerator<T> enumerator = this.enumerable.GetEnumerator();
				if (enumerator != null)
				{
					this.count = 0;
					while (enumerator.MoveNext())
					{
						!0 item = enumerator.Current;
						list.Add(item);
						this.count++;
					}
				}
				if (this.count == 0)
				{
					throw new SystemCore_EnumerableDebugViewEmptyException();
				}
				this.cachedCollection = new T[this.count];
				list.CopyTo(this.cachedCollection, 0);
				return this.cachedCollection;
			}
		}

		// Token: 0x04000509 RID: 1289
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private IEnumerable<T> enumerable;

		// Token: 0x0400050A RID: 1290
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private T[] cachedCollection;

		// Token: 0x0400050B RID: 1291
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int count;
	}
}
