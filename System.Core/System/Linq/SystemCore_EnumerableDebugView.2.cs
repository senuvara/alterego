﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace System.Linq
{
	// Token: 0x020000D4 RID: 212
	internal sealed class SystemCore_EnumerableDebugView
	{
		// Token: 0x06000798 RID: 1944 RVA: 0x00016BC2 File Offset: 0x00014DC2
		public SystemCore_EnumerableDebugView(IEnumerable enumerable)
		{
			if (enumerable == null)
			{
				throw new ArgumentNullException("enumerable");
			}
			this.enumerable = enumerable;
			this.count = 0;
			this.cachedCollection = null;
		}

		// Token: 0x17000117 RID: 279
		// (get) Token: 0x06000799 RID: 1945 RVA: 0x00016BF0 File Offset: 0x00014DF0
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public object[] Items
		{
			get
			{
				List<object> list = new List<object>();
				IEnumerator enumerator = this.enumerable.GetEnumerator();
				if (enumerator != null)
				{
					this.count = 0;
					while (enumerator.MoveNext())
					{
						object item = enumerator.Current;
						list.Add(item);
						this.count++;
					}
				}
				if (this.count == 0)
				{
					throw new SystemCore_EnumerableDebugViewEmptyException();
				}
				this.cachedCollection = new object[this.count];
				list.CopyTo(this.cachedCollection, 0);
				return this.cachedCollection;
			}
		}

		// Token: 0x0400050C RID: 1292
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private IEnumerable enumerable;

		// Token: 0x0400050D RID: 1293
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private object[] cachedCollection;

		// Token: 0x0400050E RID: 1294
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private int count;
	}
}
