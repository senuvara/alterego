﻿using System;
using System.Collections.Generic;
using System.Linq.Parallel;
using System.Runtime.CompilerServices;

namespace System.Linq
{
	// Token: 0x02000076 RID: 118
	internal static class AggregationMinMaxHelpers<T>
	{
		// Token: 0x060002DB RID: 731 RVA: 0x000077D8 File Offset: 0x000059D8
		private static T Reduce(IEnumerable<T> source, int sign)
		{
			Func<Pair<bool, T>, T, Pair<bool, T>> intermediateReduce = AggregationMinMaxHelpers<T>.MakeIntermediateReduceFunction(sign);
			Func<Pair<bool, T>, Pair<bool, T>, Pair<bool, T>> finalReduce = AggregationMinMaxHelpers<T>.MakeFinalReduceFunction(sign);
			Func<Pair<bool, T>, T> resultSelector = AggregationMinMaxHelpers<T>.MakeResultSelectorFunction();
			return new AssociativeAggregationOperator<T, Pair<bool, T>, T>(source, new Pair<bool, T>(false, default(T)), null, true, intermediateReduce, finalReduce, resultSelector, default(T) != null, QueryAggregationOptions.AssociativeCommutative).Aggregate();
		}

		// Token: 0x060002DC RID: 732 RVA: 0x0000782A File Offset: 0x00005A2A
		internal static T ReduceMin(IEnumerable<T> source)
		{
			return AggregationMinMaxHelpers<T>.Reduce(source, -1);
		}

		// Token: 0x060002DD RID: 733 RVA: 0x00007833 File Offset: 0x00005A33
		internal static T ReduceMax(IEnumerable<T> source)
		{
			return AggregationMinMaxHelpers<T>.Reduce(source, 1);
		}

		// Token: 0x060002DE RID: 734 RVA: 0x0000783C File Offset: 0x00005A3C
		private static Func<Pair<bool, T>, T, Pair<bool, T>> MakeIntermediateReduceFunction(int sign)
		{
			Comparer<T> comparer = Util.GetDefaultComparer<T>();
			return delegate(Pair<bool, T> accumulator, T element)
			{
				if ((default(T) != null || element != null) && (!accumulator.First || Util.Sign(comparer.Compare(element, accumulator.Second)) == sign))
				{
					return new Pair<bool, T>(true, element);
				}
				return accumulator;
			};
		}

		// Token: 0x060002DF RID: 735 RVA: 0x00007860 File Offset: 0x00005A60
		private static Func<Pair<bool, T>, Pair<bool, T>, Pair<bool, T>> MakeFinalReduceFunction(int sign)
		{
			Comparer<T> comparer = Util.GetDefaultComparer<T>();
			return delegate(Pair<bool, T> accumulator, Pair<bool, T> element)
			{
				if (element.First && (!accumulator.First || Util.Sign(comparer.Compare(element.Second, accumulator.Second)) == sign))
				{
					return new Pair<bool, T>(true, element.Second);
				}
				return accumulator;
			};
		}

		// Token: 0x060002E0 RID: 736 RVA: 0x00007884 File Offset: 0x00005A84
		private static Func<Pair<bool, T>, T> MakeResultSelectorFunction()
		{
			return (Pair<bool, T> accumulator) => accumulator.Second;
		}

		// Token: 0x02000077 RID: 119
		[CompilerGenerated]
		private sealed class <>c__DisplayClass3_0
		{
			// Token: 0x060002E1 RID: 737 RVA: 0x00002310 File Offset: 0x00000510
			public <>c__DisplayClass3_0()
			{
			}

			// Token: 0x060002E2 RID: 738 RVA: 0x000078A8 File Offset: 0x00005AA8
			internal Pair<bool, T> <MakeIntermediateReduceFunction>b__0(Pair<bool, T> accumulator, T element)
			{
				if ((default(T) != null || element != null) && (!accumulator.First || Util.Sign(this.comparer.Compare(element, accumulator.Second)) == this.sign))
				{
					return new Pair<bool, T>(true, element);
				}
				return accumulator;
			}

			// Token: 0x040002EE RID: 750
			public Comparer<T> comparer;

			// Token: 0x040002EF RID: 751
			public int sign;
		}

		// Token: 0x02000078 RID: 120
		[CompilerGenerated]
		private sealed class <>c__DisplayClass4_0
		{
			// Token: 0x060002E3 RID: 739 RVA: 0x00002310 File Offset: 0x00000510
			public <>c__DisplayClass4_0()
			{
			}

			// Token: 0x060002E4 RID: 740 RVA: 0x00007900 File Offset: 0x00005B00
			internal Pair<bool, T> <MakeFinalReduceFunction>b__0(Pair<bool, T> accumulator, Pair<bool, T> element)
			{
				if (element.First && (!accumulator.First || Util.Sign(this.comparer.Compare(element.Second, accumulator.Second)) == this.sign))
				{
					return new Pair<bool, T>(true, element.Second);
				}
				return accumulator;
			}

			// Token: 0x040002F0 RID: 752
			public Comparer<T> comparer;

			// Token: 0x040002F1 RID: 753
			public int sign;
		}

		// Token: 0x02000079 RID: 121
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x060002E5 RID: 741 RVA: 0x00007954 File Offset: 0x00005B54
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x060002E6 RID: 742 RVA: 0x00002310 File Offset: 0x00000510
			public <>c()
			{
			}

			// Token: 0x060002E7 RID: 743 RVA: 0x00007960 File Offset: 0x00005B60
			internal T <MakeResultSelectorFunction>b__5_0(Pair<bool, T> accumulator)
			{
				return accumulator.Second;
			}

			// Token: 0x040002F2 RID: 754
			public static readonly AggregationMinMaxHelpers<T>.<>c <>9 = new AggregationMinMaxHelpers<T>.<>c();

			// Token: 0x040002F3 RID: 755
			public static Func<Pair<bool, T>, T> <>9__5_0;
		}
	}
}
