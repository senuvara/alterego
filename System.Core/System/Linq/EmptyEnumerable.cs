﻿using System;

namespace System.Linq
{
	// Token: 0x020000BD RID: 189
	internal class EmptyEnumerable<TElement>
	{
		// Token: 0x06000731 RID: 1841 RVA: 0x00002310 File Offset: 0x00000510
		public EmptyEnumerable()
		{
		}

		// Token: 0x06000732 RID: 1842 RVA: 0x00015B7B File Offset: 0x00013D7B
		// Note: this type is marked as 'beforefieldinit'.
		static EmptyEnumerable()
		{
		}

		// Token: 0x040004CA RID: 1226
		public static readonly TElement[] Instance = new TElement[0];
	}
}
