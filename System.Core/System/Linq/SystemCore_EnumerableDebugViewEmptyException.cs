﻿using System;

namespace System.Linq
{
	// Token: 0x020000D3 RID: 211
	internal sealed class SystemCore_EnumerableDebugViewEmptyException : Exception
	{
		// Token: 0x17000116 RID: 278
		// (get) Token: 0x06000796 RID: 1942 RVA: 0x00016BB3 File Offset: 0x00014DB3
		public string Empty
		{
			get
			{
				return "Enumeration yielded no results";
			}
		}

		// Token: 0x06000797 RID: 1943 RVA: 0x00016BBA File Offset: 0x00014DBA
		public SystemCore_EnumerableDebugViewEmptyException()
		{
		}
	}
}
