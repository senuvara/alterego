﻿using System;
using System.Linq.Expressions;

namespace System.Linq
{
	/// <summary>Represents an expression tree and provides functionality to execute the expression tree after rewriting it.</summary>
	// Token: 0x02000089 RID: 137
	public abstract class EnumerableExecutor
	{
		// Token: 0x0600045D RID: 1117
		internal abstract object ExecuteBoxed();

		// Token: 0x0600045E RID: 1118 RVA: 0x0000B6CF File Offset: 0x000098CF
		internal static EnumerableExecutor Create(Expression expression)
		{
			return (EnumerableExecutor)Activator.CreateInstance(typeof(EnumerableExecutor<>).MakeGenericType(new Type[]
			{
				expression.Type
			}), new object[]
			{
				expression
			});
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Linq.EnumerableExecutor" /> class.</summary>
		// Token: 0x0600045F RID: 1119 RVA: 0x00002310 File Offset: 0x00000510
		protected EnumerableExecutor()
		{
		}
	}
}
