﻿using System;
using System.Collections.Generic;

namespace System.Linq
{
	// Token: 0x020000CE RID: 206
	internal class OrderedEnumerable<TElement, TKey> : OrderedEnumerable<TElement>
	{
		// Token: 0x06000788 RID: 1928 RVA: 0x000167C0 File Offset: 0x000149C0
		internal OrderedEnumerable(IEnumerable<TElement> source, Func<TElement, TKey> keySelector, IComparer<TKey> comparer, bool descending)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (keySelector == null)
			{
				throw Error.ArgumentNull("keySelector");
			}
			this.source = source;
			this.parent = null;
			this.keySelector = keySelector;
			IComparer<TKey> comparer2;
			if (comparer == null)
			{
				IComparer<TKey> @default = Comparer<TKey>.Default;
				comparer2 = @default;
			}
			else
			{
				comparer2 = comparer;
			}
			this.comparer = comparer2;
			this.descending = descending;
		}

		// Token: 0x06000789 RID: 1929 RVA: 0x00016820 File Offset: 0x00014A20
		internal override EnumerableSorter<TElement> GetEnumerableSorter(EnumerableSorter<TElement> next)
		{
			EnumerableSorter<TElement> enumerableSorter = new EnumerableSorter<TElement, TKey>(this.keySelector, this.comparer, this.descending, next);
			if (this.parent != null)
			{
				enumerableSorter = this.parent.GetEnumerableSorter(enumerableSorter);
			}
			return enumerableSorter;
		}

		// Token: 0x040004FE RID: 1278
		internal OrderedEnumerable<TElement> parent;

		// Token: 0x040004FF RID: 1279
		internal Func<TElement, TKey> keySelector;

		// Token: 0x04000500 RID: 1280
		internal IComparer<TKey> comparer;

		// Token: 0x04000501 RID: 1281
		internal bool descending;
	}
}
