﻿using System;
using System.Collections.Generic;
using System.Linq.Parallel;
using Unity;

namespace System.Linq
{
	/// <summary>Represents a sorted, parallel sequence.</summary>
	/// <typeparam name="TSource">The type of elements in the source collection.</typeparam>
	// Token: 0x0200007A RID: 122
	public class OrderedParallelQuery<TSource> : ParallelQuery<TSource>
	{
		// Token: 0x060002E8 RID: 744 RVA: 0x00007969 File Offset: 0x00005B69
		internal OrderedParallelQuery(QueryOperator<TSource> sortOp) : base(sortOp.SpecifiedQuerySettings)
		{
			this._sortOp = sortOp;
		}

		// Token: 0x170000AB RID: 171
		// (get) Token: 0x060002E9 RID: 745 RVA: 0x0000797E File Offset: 0x00005B7E
		internal QueryOperator<TSource> SortOperator
		{
			get
			{
				return this._sortOp;
			}
		}

		// Token: 0x170000AC RID: 172
		// (get) Token: 0x060002EA RID: 746 RVA: 0x00007986 File Offset: 0x00005B86
		internal IOrderedEnumerable<TSource> OrderedEnumerable
		{
			get
			{
				return (IOrderedEnumerable<TSource>)this._sortOp;
			}
		}

		/// <summary>Returns an enumerator that iterates through the sequence.</summary>
		/// <returns>An enumerator that iterates through the sequence.</returns>
		// Token: 0x060002EB RID: 747 RVA: 0x00007993 File Offset: 0x00005B93
		public override IEnumerator<TSource> GetEnumerator()
		{
			return this._sortOp.GetEnumerator();
		}

		// Token: 0x060002EC RID: 748 RVA: 0x0000220F File Offset: 0x0000040F
		internal OrderedParallelQuery()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040002F4 RID: 756
		private QueryOperator<TSource> _sortOp;
	}
}
