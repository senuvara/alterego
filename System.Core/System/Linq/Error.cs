﻿using System;

namespace System.Linq
{
	// Token: 0x02000091 RID: 145
	internal static class Error
	{
		// Token: 0x06000494 RID: 1172 RVA: 0x0000C4BF File Offset: 0x0000A6BF
		internal static Exception ArgumentNotIEnumerableGeneric(string message)
		{
			return new ArgumentException(Strings.ArgumentNotIEnumerableGeneric(message));
		}

		// Token: 0x06000495 RID: 1173 RVA: 0x0000C4CC File Offset: 0x0000A6CC
		internal static Exception ArgumentNotValid(string message)
		{
			return new ArgumentException(Strings.ArgumentNotValid(message));
		}

		// Token: 0x06000496 RID: 1174 RVA: 0x0000C4D9 File Offset: 0x0000A6D9
		internal static Exception NoMethodOnType(string name, object type)
		{
			return new InvalidOperationException(Strings.NoMethodOnType(name, type));
		}

		// Token: 0x06000497 RID: 1175 RVA: 0x0000C4E7 File Offset: 0x0000A6E7
		internal static Exception NoMethodOnTypeMatchingArguments(string name, object type)
		{
			return new InvalidOperationException(Strings.NoMethodOnTypeMatchingArguments(name, type));
		}

		// Token: 0x06000498 RID: 1176 RVA: 0x0000C4F5 File Offset: 0x0000A6F5
		internal static Exception EnumeratingNullEnumerableExpression()
		{
			return new InvalidOperationException(Strings.EnumeratingNullEnumerableExpression());
		}

		// Token: 0x06000499 RID: 1177 RVA: 0x0000C501 File Offset: 0x0000A701
		internal static Exception ArgumentNull(string s)
		{
			return new ArgumentNullException(s);
		}

		// Token: 0x0600049A RID: 1178 RVA: 0x0000C509 File Offset: 0x0000A709
		internal static Exception ArgumentOutOfRange(string s)
		{
			return new ArgumentOutOfRangeException(s);
		}

		// Token: 0x0600049B RID: 1179 RVA: 0x0000C511 File Offset: 0x0000A711
		internal static Exception MoreThanOneElement()
		{
			return new InvalidOperationException("Sequence contains more than one element");
		}

		// Token: 0x0600049C RID: 1180 RVA: 0x0000C51D File Offset: 0x0000A71D
		internal static Exception MoreThanOneMatch()
		{
			return new InvalidOperationException("Sequence contains more than one matching element");
		}

		// Token: 0x0600049D RID: 1181 RVA: 0x0000C529 File Offset: 0x0000A729
		internal static Exception NoElements()
		{
			return new InvalidOperationException("Sequence contains no elements");
		}

		// Token: 0x0600049E RID: 1182 RVA: 0x0000C535 File Offset: 0x0000A735
		internal static Exception NoMatch()
		{
			return new InvalidOperationException("Sequence contains no matching element");
		}

		// Token: 0x0600049F RID: 1183 RVA: 0x0000C541 File Offset: 0x0000A741
		internal static Exception NotSupported()
		{
			return new NotSupportedException();
		}
	}
}
