﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace System.Linq
{
	// Token: 0x0200008D RID: 141
	internal class EnumerableRewriter : ExpressionVisitor
	{
		// Token: 0x06000477 RID: 1143 RVA: 0x0000B944 File Offset: 0x00009B44
		protected internal override Expression VisitMethodCall(MethodCallExpression m)
		{
			Expression expression = this.Visit(m.Object);
			ReadOnlyCollection<Expression> readOnlyCollection = base.Visit(m.Arguments);
			if (expression == m.Object && readOnlyCollection == m.Arguments)
			{
				return m;
			}
			MethodInfo method = m.Method;
			Type[] typeArgs = method.IsGenericMethod ? method.GetGenericArguments() : null;
			if ((method.IsStatic || method.DeclaringType.IsAssignableFrom(expression.Type)) && EnumerableRewriter.ArgsMatch(method, readOnlyCollection, typeArgs))
			{
				return Expression.Call(expression, method, readOnlyCollection);
			}
			if (method.DeclaringType == typeof(Queryable))
			{
				MethodInfo methodInfo = EnumerableRewriter.FindEnumerableMethod(method.Name, readOnlyCollection, typeArgs);
				readOnlyCollection = this.FixupQuotedArgs(methodInfo, readOnlyCollection);
				return Expression.Call(expression, methodInfo, readOnlyCollection);
			}
			MethodInfo methodInfo2 = EnumerableRewriter.FindMethod(method.DeclaringType, method.Name, readOnlyCollection, typeArgs);
			readOnlyCollection = this.FixupQuotedArgs(methodInfo2, readOnlyCollection);
			return Expression.Call(expression, methodInfo2, readOnlyCollection);
		}

		// Token: 0x06000478 RID: 1144 RVA: 0x0000BA2C File Offset: 0x00009C2C
		private ReadOnlyCollection<Expression> FixupQuotedArgs(MethodInfo mi, ReadOnlyCollection<Expression> argList)
		{
			ParameterInfo[] parameters = mi.GetParameters();
			if (parameters.Length != 0)
			{
				List<Expression> list = null;
				int i = 0;
				int num = parameters.Length;
				while (i < num)
				{
					Expression expression = argList[i];
					ParameterInfo parameterInfo = parameters[i];
					expression = this.FixupQuotedExpression(parameterInfo.ParameterType, expression);
					if (list == null && expression != argList[i])
					{
						list = new List<Expression>(argList.Count);
						for (int j = 0; j < i; j++)
						{
							list.Add(argList[j]);
						}
					}
					if (list != null)
					{
						list.Add(expression);
					}
					i++;
				}
				if (list != null)
				{
					argList = list.AsReadOnly();
				}
			}
			return argList;
		}

		// Token: 0x06000479 RID: 1145 RVA: 0x0000BAC4 File Offset: 0x00009CC4
		private Expression FixupQuotedExpression(Type type, Expression expression)
		{
			Expression expression2 = expression;
			while (!type.IsAssignableFrom(expression2.Type))
			{
				if (expression2.NodeType != ExpressionType.Quote)
				{
					if (!type.IsAssignableFrom(expression2.Type) && type.IsArray && expression2.NodeType == ExpressionType.NewArrayInit)
					{
						Type c = EnumerableRewriter.StripExpression(expression2.Type);
						if (type.IsAssignableFrom(c))
						{
							Type elementType = type.GetElementType();
							NewArrayExpression newArrayExpression = (NewArrayExpression)expression2;
							List<Expression> list = new List<Expression>(newArrayExpression.Expressions.Count);
							int i = 0;
							int count = newArrayExpression.Expressions.Count;
							while (i < count)
							{
								list.Add(this.FixupQuotedExpression(elementType, newArrayExpression.Expressions[i]));
								i++;
							}
							expression = Expression.NewArrayInit(elementType, list);
						}
					}
					return expression;
				}
				expression2 = ((UnaryExpression)expression2).Operand;
			}
			return expression2;
		}

		// Token: 0x0600047A RID: 1146 RVA: 0x000021A3 File Offset: 0x000003A3
		protected internal override Expression VisitLambda<T>(Expression<T> node)
		{
			return node;
		}

		// Token: 0x0600047B RID: 1147 RVA: 0x0000BB9C File Offset: 0x00009D9C
		private static Type GetPublicType(Type t)
		{
			if (t.IsGenericType && t.GetGenericTypeDefinition().GetInterfaces().Contains(typeof(IGrouping<, >)))
			{
				return typeof(IGrouping<, >).MakeGenericType(t.GetGenericArguments());
			}
			if (!t.IsNestedPrivate)
			{
				return t;
			}
			foreach (Type type in t.GetInterfaces())
			{
				if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(IEnumerable<>))
				{
					return type;
				}
			}
			if (typeof(IEnumerable).IsAssignableFrom(t))
			{
				return typeof(IEnumerable);
			}
			return t;
		}

		// Token: 0x0600047C RID: 1148 RVA: 0x0000BC48 File Offset: 0x00009E48
		private Type GetEquivalentType(Type type)
		{
			if (this._equivalentTypeCache == null)
			{
				this._equivalentTypeCache = new Dictionary<Type, Type>
				{
					{
						typeof(IQueryable),
						typeof(IEnumerable)
					},
					{
						typeof(IEnumerable),
						typeof(IEnumerable)
					}
				};
			}
			Type type2;
			if (!this._equivalentTypeCache.TryGetValue(type, out type2))
			{
				Type publicType = EnumerableRewriter.GetPublicType(type);
				if (publicType.IsInterface && publicType.IsGenericType)
				{
					Type genericTypeDefinition = publicType.GetGenericTypeDefinition();
					if (genericTypeDefinition == typeof(IOrderedEnumerable<>))
					{
						type2 = publicType;
					}
					else if (genericTypeDefinition == typeof(IOrderedQueryable<>))
					{
						type2 = typeof(IOrderedEnumerable<>).MakeGenericType(new Type[]
						{
							publicType.GenericTypeArguments[0]
						});
					}
					else if (genericTypeDefinition == typeof(IEnumerable<>))
					{
						type2 = publicType;
					}
					else if (genericTypeDefinition == typeof(IQueryable<>))
					{
						type2 = typeof(IEnumerable<>).MakeGenericType(new Type[]
						{
							publicType.GenericTypeArguments[0]
						});
					}
				}
				if (type2 == null)
				{
					var source = (from i in publicType.GetInterfaces().Select(new Func<Type, TypeInfo>(IntrospectionExtensions.GetTypeInfo)).ToArray<TypeInfo>()
					where i.IsGenericType && i.GenericTypeArguments.Length == 1
					select new
					{
						Info = i,
						GenType = i.GetGenericTypeDefinition()
					}).ToArray();
					Type type3 = (from i in source
					where i.GenType == typeof(IOrderedQueryable<>) || i.GenType == typeof(IOrderedEnumerable<>)
					select i.Info.GenericTypeArguments[0]).Distinct<Type>().SingleOrDefault<Type>();
					if (type3 != null)
					{
						type2 = typeof(IOrderedEnumerable<>).MakeGenericType(new Type[]
						{
							type3
						});
					}
					else
					{
						type3 = (from i in source
						where i.GenType == typeof(IQueryable<>) || i.GenType == typeof(IEnumerable<>)
						select i.Info.GenericTypeArguments[0]).Distinct<Type>().Single<Type>();
						type2 = typeof(IEnumerable<>).MakeGenericType(new Type[]
						{
							type3
						});
					}
				}
				this._equivalentTypeCache.Add(type, type2);
			}
			return type2;
		}

		// Token: 0x0600047D RID: 1149 RVA: 0x0000BED8 File Offset: 0x0000A0D8
		protected internal override Expression VisitConstant(ConstantExpression c)
		{
			EnumerableQuery enumerableQuery = c.Value as EnumerableQuery;
			if (enumerableQuery != null)
			{
				if (enumerableQuery.Enumerable != null)
				{
					Type publicType = EnumerableRewriter.GetPublicType(enumerableQuery.Enumerable.GetType());
					return Expression.Constant(enumerableQuery.Enumerable, publicType);
				}
				Expression expression = enumerableQuery.Expression;
				if (expression != c)
				{
					return this.Visit(expression);
				}
			}
			return c;
		}

		// Token: 0x0600047E RID: 1150 RVA: 0x0000BF30 File Offset: 0x0000A130
		private static MethodInfo FindEnumerableMethod(string name, ReadOnlyCollection<Expression> args, params Type[] typeArgs)
		{
			if (EnumerableRewriter.s_seqMethods == null)
			{
				EnumerableRewriter.s_seqMethods = typeof(Enumerable).GetStaticMethods().ToLookup((MethodInfo m) => m.Name);
			}
			MethodInfo methodInfo = EnumerableRewriter.s_seqMethods[name].FirstOrDefault((MethodInfo m) => EnumerableRewriter.ArgsMatch(m, args, typeArgs));
			if (typeArgs != null)
			{
				return methodInfo.MakeGenericMethod(typeArgs);
			}
			return methodInfo;
		}

		// Token: 0x0600047F RID: 1151 RVA: 0x0000BFC4 File Offset: 0x0000A1C4
		private static MethodInfo FindMethod(Type type, string name, ReadOnlyCollection<Expression> args, Type[] typeArgs)
		{
			using (IEnumerator<MethodInfo> enumerator = (from m in type.GetStaticMethods()
			where m.Name == name
			select m).GetEnumerator())
			{
				if (!enumerator.MoveNext())
				{
					throw Error.NoMethodOnType(name, type);
				}
				MethodInfo methodInfo;
				for (;;)
				{
					methodInfo = enumerator.Current;
					if (EnumerableRewriter.ArgsMatch(methodInfo, args, typeArgs))
					{
						break;
					}
					if (!enumerator.MoveNext())
					{
						goto Block_6;
					}
				}
				return (typeArgs != null) ? methodInfo.MakeGenericMethod(typeArgs) : methodInfo;
				Block_6:;
			}
			throw Error.NoMethodOnTypeMatchingArguments(name, type);
		}

		// Token: 0x06000480 RID: 1152 RVA: 0x0000C064 File Offset: 0x0000A264
		private static bool ArgsMatch(MethodInfo m, ReadOnlyCollection<Expression> args, Type[] typeArgs)
		{
			ParameterInfo[] parameters = m.GetParameters();
			if (parameters.Length != args.Count)
			{
				return false;
			}
			if (!m.IsGenericMethod && typeArgs != null && typeArgs.Length != 0)
			{
				return false;
			}
			if (!m.IsGenericMethodDefinition && m.IsGenericMethod && m.ContainsGenericParameters)
			{
				m = m.GetGenericMethodDefinition();
			}
			if (m.IsGenericMethodDefinition)
			{
				if (typeArgs == null || typeArgs.Length == 0)
				{
					return false;
				}
				if (m.GetGenericArguments().Length != typeArgs.Length)
				{
					return false;
				}
				m = m.MakeGenericMethod(typeArgs);
				parameters = m.GetParameters();
			}
			int i = 0;
			int count = args.Count;
			while (i < count)
			{
				Type type = parameters[i].ParameterType;
				if (type == null)
				{
					return false;
				}
				if (type.IsByRef)
				{
					type = type.GetElementType();
				}
				Expression expression = args[i];
				if (!type.IsAssignableFrom(expression.Type))
				{
					if (expression.NodeType == ExpressionType.Quote)
					{
						expression = ((UnaryExpression)expression).Operand;
					}
					if (!type.IsAssignableFrom(expression.Type) && !type.IsAssignableFrom(EnumerableRewriter.StripExpression(expression.Type)))
					{
						return false;
					}
				}
				i++;
			}
			return true;
		}

		// Token: 0x06000481 RID: 1153 RVA: 0x0000C178 File Offset: 0x0000A378
		private static Type StripExpression(Type type)
		{
			bool isArray = type.IsArray;
			Type type2 = isArray ? type.GetElementType() : type;
			Type type3 = TypeHelper.FindGenericType(typeof(Expression<>), type2);
			if (type3 != null)
			{
				type2 = type3.GetGenericArguments()[0];
			}
			if (!isArray)
			{
				return type;
			}
			int arrayRank = type.GetArrayRank();
			if (arrayRank != 1)
			{
				return type2.MakeArrayType(arrayRank);
			}
			return type2.MakeArrayType();
		}

		// Token: 0x06000482 RID: 1154 RVA: 0x0000C1D8 File Offset: 0x0000A3D8
		protected internal override Expression VisitConditional(ConditionalExpression c)
		{
			Type type = c.Type;
			if (!typeof(IQueryable).IsAssignableFrom(type))
			{
				return base.VisitConditional(c);
			}
			Expression test = this.Visit(c.Test);
			Expression expression = this.Visit(c.IfTrue);
			Expression expression2 = this.Visit(c.IfFalse);
			Type type2 = expression.Type;
			Type type3 = expression2.Type;
			if (type2.IsAssignableFrom(type3))
			{
				return Expression.Condition(test, expression, expression2, type2);
			}
			if (type3.IsAssignableFrom(type2))
			{
				return Expression.Condition(test, expression, expression2, type3);
			}
			return Expression.Condition(test, expression, expression2, this.GetEquivalentType(type));
		}

		// Token: 0x06000483 RID: 1155 RVA: 0x0000C278 File Offset: 0x0000A478
		protected internal override Expression VisitBlock(BlockExpression node)
		{
			Type type = node.Type;
			if (!typeof(IQueryable).IsAssignableFrom(type))
			{
				return base.VisitBlock(node);
			}
			ReadOnlyCollection<Expression> expressions = base.Visit(node.Expressions);
			ReadOnlyCollection<ParameterExpression> variables = base.VisitAndConvert<ParameterExpression>(node.Variables, "EnumerableRewriter.VisitBlock");
			if (type == node.Expressions.Last<Expression>().Type)
			{
				return Expression.Block(variables, expressions);
			}
			return Expression.Block(this.GetEquivalentType(type), variables, expressions);
		}

		// Token: 0x06000484 RID: 1156 RVA: 0x0000C2F4 File Offset: 0x0000A4F4
		protected internal override Expression VisitGoto(GotoExpression node)
		{
			Type type = node.Value.Type;
			if (!typeof(IQueryable).IsAssignableFrom(type))
			{
				return base.VisitGoto(node);
			}
			LabelTarget target = this.VisitLabelTarget(node.Target);
			Expression expression = this.Visit(node.Value);
			return Expression.MakeGoto(node.Kind, target, expression, this.GetEquivalentType(typeof(EnumerableQuery).IsAssignableFrom(type) ? expression.Type : type));
		}

		// Token: 0x06000485 RID: 1157 RVA: 0x0000C370 File Offset: 0x0000A570
		protected override LabelTarget VisitLabelTarget(LabelTarget node)
		{
			LabelTarget labelTarget;
			if (this._targetCache == null)
			{
				this._targetCache = new Dictionary<LabelTarget, LabelTarget>();
			}
			else if (this._targetCache.TryGetValue(node, out labelTarget))
			{
				return labelTarget;
			}
			Type type = node.Type;
			if (!typeof(IQueryable).IsAssignableFrom(type))
			{
				labelTarget = base.VisitLabelTarget(node);
			}
			else
			{
				labelTarget = Expression.Label(this.GetEquivalentType(type), node.Name);
			}
			this._targetCache.Add(node, labelTarget);
			return labelTarget;
		}

		// Token: 0x06000486 RID: 1158 RVA: 0x0000C3E8 File Offset: 0x0000A5E8
		public EnumerableRewriter()
		{
		}

		// Token: 0x0400038C RID: 908
		private Dictionary<LabelTarget, LabelTarget> _targetCache;

		// Token: 0x0400038D RID: 909
		private Dictionary<Type, Type> _equivalentTypeCache;

		// Token: 0x0400038E RID: 910
		private static ILookup<string, MethodInfo> s_seqMethods;

		// Token: 0x0200008E RID: 142
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000487 RID: 1159 RVA: 0x0000C3F0 File Offset: 0x0000A5F0
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000488 RID: 1160 RVA: 0x00002310 File Offset: 0x00000510
			public <>c()
			{
			}

			// Token: 0x06000489 RID: 1161 RVA: 0x0000C3FC File Offset: 0x0000A5FC
			internal bool <GetEquivalentType>b__7_0(TypeInfo i)
			{
				return i.IsGenericType && i.GenericTypeArguments.Length == 1;
			}

			// Token: 0x0600048A RID: 1162 RVA: 0x0000C413 File Offset: 0x0000A613
			internal <>f__AnonymousType0<TypeInfo, Type> <GetEquivalentType>b__7_1(TypeInfo i)
			{
				return new
				{
					Info = i,
					GenType = i.GetGenericTypeDefinition()
				};
			}

			// Token: 0x0600048B RID: 1163 RVA: 0x0000C421 File Offset: 0x0000A621
			internal bool <GetEquivalentType>b__7_2(<>f__AnonymousType0<TypeInfo, Type> i)
			{
				return i.GenType == typeof(IOrderedQueryable<>) || i.GenType == typeof(IOrderedEnumerable<>);
			}

			// Token: 0x0600048C RID: 1164 RVA: 0x0000C451 File Offset: 0x0000A651
			internal Type <GetEquivalentType>b__7_3(<>f__AnonymousType0<TypeInfo, Type> i)
			{
				return i.Info.GenericTypeArguments[0];
			}

			// Token: 0x0600048D RID: 1165 RVA: 0x0000C460 File Offset: 0x0000A660
			internal bool <GetEquivalentType>b__7_4(<>f__AnonymousType0<TypeInfo, Type> i)
			{
				return i.GenType == typeof(IQueryable<>) || i.GenType == typeof(IEnumerable<>);
			}

			// Token: 0x0600048E RID: 1166 RVA: 0x0000C451 File Offset: 0x0000A651
			internal Type <GetEquivalentType>b__7_5(<>f__AnonymousType0<TypeInfo, Type> i)
			{
				return i.Info.GenericTypeArguments[0];
			}

			// Token: 0x0600048F RID: 1167 RVA: 0x0000C490 File Offset: 0x0000A690
			internal string <FindEnumerableMethod>b__10_0(MethodInfo m)
			{
				return m.Name;
			}

			// Token: 0x0400038F RID: 911
			public static readonly EnumerableRewriter.<>c <>9 = new EnumerableRewriter.<>c();

			// Token: 0x04000390 RID: 912
			public static Func<TypeInfo, bool> <>9__7_0;

			// Token: 0x04000391 RID: 913
			public static Func<TypeInfo, <>f__AnonymousType0<TypeInfo, Type>> <>9__7_1;

			// Token: 0x04000392 RID: 914
			public static Func<<>f__AnonymousType0<TypeInfo, Type>, bool> <>9__7_2;

			// Token: 0x04000393 RID: 915
			public static Func<<>f__AnonymousType0<TypeInfo, Type>, Type> <>9__7_3;

			// Token: 0x04000394 RID: 916
			public static Func<<>f__AnonymousType0<TypeInfo, Type>, bool> <>9__7_4;

			// Token: 0x04000395 RID: 917
			public static Func<<>f__AnonymousType0<TypeInfo, Type>, Type> <>9__7_5;

			// Token: 0x04000396 RID: 918
			public static Func<MethodInfo, string> <>9__10_0;
		}

		// Token: 0x0200008F RID: 143
		[CompilerGenerated]
		private sealed class <>c__DisplayClass10_0
		{
			// Token: 0x06000490 RID: 1168 RVA: 0x00002310 File Offset: 0x00000510
			public <>c__DisplayClass10_0()
			{
			}

			// Token: 0x06000491 RID: 1169 RVA: 0x0000C498 File Offset: 0x0000A698
			internal bool <FindEnumerableMethod>b__1(MethodInfo m)
			{
				return EnumerableRewriter.ArgsMatch(m, this.args, this.typeArgs);
			}

			// Token: 0x04000397 RID: 919
			public ReadOnlyCollection<Expression> args;

			// Token: 0x04000398 RID: 920
			public Type[] typeArgs;
		}

		// Token: 0x02000090 RID: 144
		[CompilerGenerated]
		private sealed class <>c__DisplayClass11_0
		{
			// Token: 0x06000492 RID: 1170 RVA: 0x00002310 File Offset: 0x00000510
			public <>c__DisplayClass11_0()
			{
			}

			// Token: 0x06000493 RID: 1171 RVA: 0x0000C4AC File Offset: 0x0000A6AC
			internal bool <FindMethod>b__0(MethodInfo m)
			{
				return m.Name == this.name;
			}

			// Token: 0x04000399 RID: 921
			public string name;
		}
	}
}
