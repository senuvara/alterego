﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace System.Linq
{
	// Token: 0x020000CC RID: 204
	internal abstract class OrderedEnumerable<TElement> : IOrderedEnumerable<!0>, IEnumerable<!0>, IEnumerable
	{
		// Token: 0x0600077D RID: 1917 RVA: 0x0001668A File Offset: 0x0001488A
		public IEnumerator<TElement> GetEnumerator()
		{
			Buffer<TElement> buffer = new Buffer<TElement>(this.source);
			if (buffer.count > 0)
			{
				EnumerableSorter<TElement> enumerableSorter = this.GetEnumerableSorter(null);
				int[] map = enumerableSorter.Sort(buffer.items, buffer.count);
				int num;
				for (int i = 0; i < buffer.count; i = num + 1)
				{
					yield return buffer.items[map[i]];
					num = i;
				}
				map = null;
			}
			yield break;
		}

		// Token: 0x0600077E RID: 1918
		internal abstract EnumerableSorter<TElement> GetEnumerableSorter(EnumerableSorter<TElement> next);

		// Token: 0x0600077F RID: 1919 RVA: 0x00016699 File Offset: 0x00014899
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x06000780 RID: 1920 RVA: 0x000166A1 File Offset: 0x000148A1
		IOrderedEnumerable<TElement> IOrderedEnumerable<!0>.CreateOrderedEnumerable<TKey>(Func<TElement, TKey> keySelector, IComparer<TKey> comparer, bool descending)
		{
			return new OrderedEnumerable<TElement, TKey>(this.source, keySelector, comparer, descending)
			{
				parent = this
			};
		}

		// Token: 0x06000781 RID: 1921 RVA: 0x00002310 File Offset: 0x00000510
		protected OrderedEnumerable()
		{
		}

		// Token: 0x040004F7 RID: 1271
		internal IEnumerable<TElement> source;

		// Token: 0x020000CD RID: 205
		[CompilerGenerated]
		private sealed class <GetEnumerator>d__1 : IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x06000782 RID: 1922 RVA: 0x000166B8 File Offset: 0x000148B8
			[DebuggerHidden]
			public <GetEnumerator>d__1(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x06000783 RID: 1923 RVA: 0x000039E8 File Offset: 0x00001BE8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06000784 RID: 1924 RVA: 0x000166C8 File Offset: 0x000148C8
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				OrderedEnumerable<TElement> orderedEnumerable = this;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
					int num2 = i;
					i = num2 + 1;
				}
				else
				{
					this.<>1__state = -1;
					buffer = new Buffer<TElement>(orderedEnumerable.source);
					if (buffer.count <= 0)
					{
						return false;
					}
					EnumerableSorter<TElement> enumerableSorter = orderedEnumerable.GetEnumerableSorter(null);
					map = enumerableSorter.Sort(buffer.items, buffer.count);
					i = 0;
				}
				if (i < buffer.count)
				{
					this.<>2__current = buffer.items[map[i]];
					this.<>1__state = 1;
					return true;
				}
				map = null;
				return false;
			}

			// Token: 0x17000113 RID: 275
			// (get) Token: 0x06000785 RID: 1925 RVA: 0x000167AB File Offset: 0x000149AB
			TElement IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000786 RID: 1926 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000114 RID: 276
			// (get) Token: 0x06000787 RID: 1927 RVA: 0x000167B3 File Offset: 0x000149B3
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x040004F8 RID: 1272
			private int <>1__state;

			// Token: 0x040004F9 RID: 1273
			private TElement <>2__current;

			// Token: 0x040004FA RID: 1274
			public OrderedEnumerable<TElement> <>4__this;

			// Token: 0x040004FB RID: 1275
			private Buffer<TElement> <buffer>5__1;

			// Token: 0x040004FC RID: 1276
			private int[] <map>5__2;

			// Token: 0x040004FD RID: 1277
			private int <i>5__3;
		}
	}
}
