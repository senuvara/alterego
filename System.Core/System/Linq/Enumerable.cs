﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;

namespace System.Linq
{
	/// <summary>Provides a set of <see langword="static" /> (<see langword="Shared" /> in Visual Basic) methods for querying objects that implement <see cref="T:System.Collections.Generic.IEnumerable`1" />.</summary>
	// Token: 0x02000096 RID: 150
	public static class Enumerable
	{
		/// <summary>Filters a sequence of values based on a predicate.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to filter.</param>
		/// <param name="predicate">A function to test each element for a condition.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains elements from the input sequence that satisfy the condition.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="predicate" /> is <see langword="null" />.</exception>
		// Token: 0x0600052A RID: 1322 RVA: 0x0000F1B8 File Offset: 0x0000D3B8
		public static IEnumerable<TSource> Where<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (predicate == null)
			{
				throw Error.ArgumentNull("predicate");
			}
			if (source is Enumerable.Iterator<TSource>)
			{
				return ((Enumerable.Iterator<TSource>)source).Where(predicate);
			}
			if (source is TSource[])
			{
				return new Enumerable.WhereArrayIterator<TSource>((TSource[])source, predicate);
			}
			if (source is List<TSource>)
			{
				return new Enumerable.WhereListIterator<TSource>((List<TSource>)source, predicate);
			}
			return new Enumerable.WhereEnumerableIterator<TSource>(source, predicate);
		}

		/// <summary>Filters a sequence of values based on a predicate. Each element's index is used in the logic of the predicate function.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to filter.</param>
		/// <param name="predicate">A function to test each source element for a condition; the second parameter of the function represents the index of the source element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains elements from the input sequence that satisfy the condition.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="predicate" /> is <see langword="null" />.</exception>
		// Token: 0x0600052B RID: 1323 RVA: 0x0000F227 File Offset: 0x0000D427
		public static IEnumerable<TSource> Where<TSource>(this IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (predicate == null)
			{
				throw Error.ArgumentNull("predicate");
			}
			return Enumerable.WhereIterator<TSource>(source, predicate);
		}

		// Token: 0x0600052C RID: 1324 RVA: 0x0000F24C File Offset: 0x0000D44C
		private static IEnumerable<TSource> WhereIterator<TSource>(IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
		{
			int index = -1;
			foreach (TSource tsource in source)
			{
				int num = index;
				index = checked(num + 1);
				if (predicate(tsource, index))
				{
					yield return tsource;
				}
			}
			IEnumerator<TSource> enumerator = null;
			yield break;
			yield break;
		}

		/// <summary>Projects each element of a sequence into a new form.</summary>
		/// <param name="source">A sequence of values to invoke a transform function on.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TResult">The type of the value returned by <paramref name="selector" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose elements are the result of invoking the transform function on each element of <paramref name="source" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		// Token: 0x0600052D RID: 1325 RVA: 0x0000F264 File Offset: 0x0000D464
		public static IEnumerable<TResult> Select<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (selector == null)
			{
				throw Error.ArgumentNull("selector");
			}
			if (source is Enumerable.Iterator<TSource>)
			{
				return ((Enumerable.Iterator<TSource>)source).Select<TResult>(selector);
			}
			if (source is TSource[])
			{
				return new Enumerable.WhereSelectArrayIterator<TSource, TResult>((TSource[])source, null, selector);
			}
			if (source is List<TSource>)
			{
				return new Enumerable.WhereSelectListIterator<TSource, TResult>((List<TSource>)source, null, selector);
			}
			return new Enumerable.WhereSelectEnumerableIterator<TSource, TResult>(source, null, selector);
		}

		/// <summary>Projects each element of a sequence into a new form by incorporating the element's index.</summary>
		/// <param name="source">A sequence of values to invoke a transform function on.</param>
		/// <param name="selector">A transform function to apply to each source element; the second parameter of the function represents the index of the source element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TResult">The type of the value returned by <paramref name="selector" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose elements are the result of invoking the transform function on each element of <paramref name="source" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		// Token: 0x0600052E RID: 1326 RVA: 0x0000F2D6 File Offset: 0x0000D4D6
		public static IEnumerable<TResult> Select<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, int, TResult> selector)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (selector == null)
			{
				throw Error.ArgumentNull("selector");
			}
			return Enumerable.SelectIterator<TSource, TResult>(source, selector);
		}

		// Token: 0x0600052F RID: 1327 RVA: 0x0000F2FB File Offset: 0x0000D4FB
		private static IEnumerable<TResult> SelectIterator<TSource, TResult>(IEnumerable<TSource> source, Func<TSource, int, TResult> selector)
		{
			int index = -1;
			foreach (TSource arg in source)
			{
				int num = index;
				index = checked(num + 1);
				yield return selector(arg, index);
			}
			IEnumerator<TSource> enumerator = null;
			yield break;
			yield break;
		}

		// Token: 0x06000530 RID: 1328 RVA: 0x0000F312 File Offset: 0x0000D512
		private static Func<TSource, bool> CombinePredicates<TSource>(Func<TSource, bool> predicate1, Func<TSource, bool> predicate2)
		{
			return (TSource x) => predicate1(x) && predicate2(x);
		}

		// Token: 0x06000531 RID: 1329 RVA: 0x0000F332 File Offset: 0x0000D532
		private static Func<TSource, TResult> CombineSelectors<TSource, TMiddle, TResult>(Func<TSource, TMiddle> selector1, Func<TMiddle, TResult> selector2)
		{
			return (TSource x) => selector2(selector1(x));
		}

		/// <summary>Projects each element of a sequence to an <see cref="T:System.Collections.Generic.IEnumerable`1" /> and flattens the resulting sequences into one sequence.</summary>
		/// <param name="source">A sequence of values to project.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TResult">The type of the elements of the sequence returned by <paramref name="selector" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose elements are the result of invoking the one-to-many transform function on each element of the input sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		// Token: 0x06000532 RID: 1330 RVA: 0x0000F352 File Offset: 0x0000D552
		public static IEnumerable<TResult> SelectMany<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, IEnumerable<TResult>> selector)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (selector == null)
			{
				throw Error.ArgumentNull("selector");
			}
			return Enumerable.SelectManyIterator<TSource, TResult>(source, selector);
		}

		// Token: 0x06000533 RID: 1331 RVA: 0x0000F377 File Offset: 0x0000D577
		private static IEnumerable<TResult> SelectManyIterator<TSource, TResult>(IEnumerable<TSource> source, Func<TSource, IEnumerable<TResult>> selector)
		{
			foreach (TSource arg in source)
			{
				foreach (TResult tresult in selector(arg))
				{
					yield return tresult;
				}
				IEnumerator<TResult> enumerator2 = null;
			}
			IEnumerator<TSource> enumerator = null;
			yield break;
			yield break;
		}

		/// <summary>Projects each element of a sequence to an <see cref="T:System.Collections.Generic.IEnumerable`1" />, and flattens the resulting sequences into one sequence. The index of each source element is used in the projected form of that element.</summary>
		/// <param name="source">A sequence of values to project.</param>
		/// <param name="selector">A transform function to apply to each source element; the second parameter of the function represents the index of the source element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TResult">The type of the elements of the sequence returned by <paramref name="selector" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose elements are the result of invoking the one-to-many transform function on each element of an input sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		// Token: 0x06000534 RID: 1332 RVA: 0x0000F38E File Offset: 0x0000D58E
		public static IEnumerable<TResult> SelectMany<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, int, IEnumerable<TResult>> selector)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (selector == null)
			{
				throw Error.ArgumentNull("selector");
			}
			return Enumerable.SelectManyIterator<TSource, TResult>(source, selector);
		}

		// Token: 0x06000535 RID: 1333 RVA: 0x0000F3B3 File Offset: 0x0000D5B3
		private static IEnumerable<TResult> SelectManyIterator<TSource, TResult>(IEnumerable<TSource> source, Func<TSource, int, IEnumerable<TResult>> selector)
		{
			int index = -1;
			foreach (TSource arg in source)
			{
				int num = index;
				index = checked(num + 1);
				foreach (TResult tresult in selector(arg, index))
				{
					yield return tresult;
				}
				IEnumerator<TResult> enumerator2 = null;
			}
			IEnumerator<TSource> enumerator = null;
			yield break;
			yield break;
		}

		/// <summary>Projects each element of a sequence to an <see cref="T:System.Collections.Generic.IEnumerable`1" />, flattens the resulting sequences into one sequence, and invokes a result selector function on each element therein. The index of each source element is used in the intermediate projected form of that element.</summary>
		/// <param name="source">A sequence of values to project.</param>
		/// <param name="collectionSelector">A transform function to apply to each source element; the second parameter of the function represents the index of the source element.</param>
		/// <param name="resultSelector">A transform function to apply to each element of the intermediate sequence.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TCollection">The type of the intermediate elements collected by <paramref name="collectionSelector" />.</typeparam>
		/// <typeparam name="TResult">The type of the elements of the resulting sequence.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose elements are the result of invoking the one-to-many transform function <paramref name="collectionSelector" /> on each element of <paramref name="source" /> and then mapping each of those sequence elements and their corresponding source element to a result element.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="collectionSelector" /> or <paramref name="resultSelector" /> is <see langword="null" />.</exception>
		// Token: 0x06000536 RID: 1334 RVA: 0x0000F3CA File Offset: 0x0000D5CA
		public static IEnumerable<TResult> SelectMany<TSource, TCollection, TResult>(this IEnumerable<TSource> source, Func<TSource, int, IEnumerable<TCollection>> collectionSelector, Func<TSource, TCollection, TResult> resultSelector)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (collectionSelector == null)
			{
				throw Error.ArgumentNull("collectionSelector");
			}
			if (resultSelector == null)
			{
				throw Error.ArgumentNull("resultSelector");
			}
			return Enumerable.SelectManyIterator<TSource, TCollection, TResult>(source, collectionSelector, resultSelector);
		}

		// Token: 0x06000537 RID: 1335 RVA: 0x0000F3FE File Offset: 0x0000D5FE
		private static IEnumerable<TResult> SelectManyIterator<TSource, TCollection, TResult>(IEnumerable<TSource> source, Func<TSource, int, IEnumerable<TCollection>> collectionSelector, Func<TSource, TCollection, TResult> resultSelector)
		{
			int index = -1;
			foreach (TSource element in source)
			{
				int num = index;
				index = checked(num + 1);
				foreach (TCollection arg in collectionSelector(element, index))
				{
					yield return resultSelector(element, arg);
				}
				IEnumerator<TCollection> enumerator2 = null;
				element = default(TSource);
			}
			IEnumerator<TSource> enumerator = null;
			yield break;
			yield break;
		}

		/// <summary>Projects each element of a sequence to an <see cref="T:System.Collections.Generic.IEnumerable`1" />, flattens the resulting sequences into one sequence, and invokes a result selector function on each element therein.</summary>
		/// <param name="source">A sequence of values to project.</param>
		/// <param name="collectionSelector">A transform function to apply to each element of the input sequence.</param>
		/// <param name="resultSelector">A transform function to apply to each element of the intermediate sequence.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TCollection">The type of the intermediate elements collected by <paramref name="collectionSelector" />.</typeparam>
		/// <typeparam name="TResult">The type of the elements of the resulting sequence.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose elements are the result of invoking the one-to-many transform function <paramref name="collectionSelector" /> on each element of <paramref name="source" /> and then mapping each of those sequence elements and their corresponding source element to a result element.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="collectionSelector" /> or <paramref name="resultSelector" /> is <see langword="null" />.</exception>
		// Token: 0x06000538 RID: 1336 RVA: 0x0000F41C File Offset: 0x0000D61C
		public static IEnumerable<TResult> SelectMany<TSource, TCollection, TResult>(this IEnumerable<TSource> source, Func<TSource, IEnumerable<TCollection>> collectionSelector, Func<TSource, TCollection, TResult> resultSelector)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (collectionSelector == null)
			{
				throw Error.ArgumentNull("collectionSelector");
			}
			if (resultSelector == null)
			{
				throw Error.ArgumentNull("resultSelector");
			}
			return Enumerable.SelectManyIterator<TSource, TCollection, TResult>(source, collectionSelector, resultSelector);
		}

		// Token: 0x06000539 RID: 1337 RVA: 0x0000F450 File Offset: 0x0000D650
		private static IEnumerable<TResult> SelectManyIterator<TSource, TCollection, TResult>(IEnumerable<TSource> source, Func<TSource, IEnumerable<TCollection>> collectionSelector, Func<TSource, TCollection, TResult> resultSelector)
		{
			foreach (TSource element in source)
			{
				foreach (TCollection arg in collectionSelector(element))
				{
					yield return resultSelector(element, arg);
				}
				IEnumerator<TCollection> enumerator2 = null;
				element = default(TSource);
			}
			IEnumerator<TSource> enumerator = null;
			yield break;
			yield break;
		}

		/// <summary>Returns a specified number of contiguous elements from the start of a sequence.</summary>
		/// <param name="source">The sequence to return elements from.</param>
		/// <param name="count">The number of elements to return.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains the specified number of elements from the start of the input sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x0600053A RID: 1338 RVA: 0x0000F46E File Offset: 0x0000D66E
		public static IEnumerable<TSource> Take<TSource>(this IEnumerable<TSource> source, int count)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			return Enumerable.TakeIterator<TSource>(source, count);
		}

		// Token: 0x0600053B RID: 1339 RVA: 0x0000F485 File Offset: 0x0000D685
		private static IEnumerable<TSource> TakeIterator<TSource>(IEnumerable<TSource> source, int count)
		{
			if (count > 0)
			{
				foreach (TSource tsource in source)
				{
					yield return tsource;
					int num = count - 1;
					count = num;
					if (num == 0)
					{
						break;
					}
				}
				IEnumerator<TSource> enumerator = null;
			}
			yield break;
			yield break;
		}

		/// <summary>Returns elements from a sequence as long as a specified condition is true.</summary>
		/// <param name="source">A sequence to return elements from.</param>
		/// <param name="predicate">A function to test each element for a condition.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains the elements from the input sequence that occur before the element at which the test no longer passes.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="predicate" /> is <see langword="null" />.</exception>
		// Token: 0x0600053C RID: 1340 RVA: 0x0000F49C File Offset: 0x0000D69C
		public static IEnumerable<TSource> TakeWhile<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (predicate == null)
			{
				throw Error.ArgumentNull("predicate");
			}
			return Enumerable.TakeWhileIterator<TSource>(source, predicate);
		}

		// Token: 0x0600053D RID: 1341 RVA: 0x0000F4C1 File Offset: 0x0000D6C1
		private static IEnumerable<TSource> TakeWhileIterator<TSource>(IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			foreach (TSource tsource in source)
			{
				if (!predicate(tsource))
				{
					break;
				}
				yield return tsource;
			}
			IEnumerator<TSource> enumerator = null;
			yield break;
			yield break;
		}

		/// <summary>Returns elements from a sequence as long as a specified condition is true. The element's index is used in the logic of the predicate function.</summary>
		/// <param name="source">The sequence to return elements from.</param>
		/// <param name="predicate">A function to test each source element for a condition; the second parameter of the function represents the index of the source element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains elements from the input sequence that occur before the element at which the test no longer passes.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="predicate" /> is <see langword="null" />.</exception>
		// Token: 0x0600053E RID: 1342 RVA: 0x0000F4D8 File Offset: 0x0000D6D8
		public static IEnumerable<TSource> TakeWhile<TSource>(this IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (predicate == null)
			{
				throw Error.ArgumentNull("predicate");
			}
			return Enumerable.TakeWhileIterator<TSource>(source, predicate);
		}

		// Token: 0x0600053F RID: 1343 RVA: 0x0000F4FD File Offset: 0x0000D6FD
		private static IEnumerable<TSource> TakeWhileIterator<TSource>(IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
		{
			int index = -1;
			foreach (TSource tsource in source)
			{
				int num = index;
				index = checked(num + 1);
				if (!predicate(tsource, index))
				{
					break;
				}
				yield return tsource;
			}
			IEnumerator<TSource> enumerator = null;
			yield break;
			yield break;
		}

		/// <summary>Bypasses a specified number of elements in a sequence and then returns the remaining elements.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to return elements from.</param>
		/// <param name="count">The number of elements to skip before returning the remaining elements.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains the elements that occur after the specified index in the input sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x06000540 RID: 1344 RVA: 0x0000F514 File Offset: 0x0000D714
		public static IEnumerable<TSource> Skip<TSource>(this IEnumerable<TSource> source, int count)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			return Enumerable.SkipIterator<TSource>(source, count);
		}

		// Token: 0x06000541 RID: 1345 RVA: 0x0000F52B File Offset: 0x0000D72B
		private static IEnumerable<TSource> SkipIterator<TSource>(IEnumerable<TSource> source, int count)
		{
			using (IEnumerator<TSource> e = source.GetEnumerator())
			{
				while (count > 0 && e.MoveNext())
				{
					int num = count;
					count = num - 1;
				}
				if (count <= 0)
				{
					while (e.MoveNext())
					{
						!0 ! = e.Current;
						yield return !;
					}
				}
			}
			IEnumerator<TSource> e = null;
			yield break;
			yield break;
		}

		/// <summary>Bypasses elements in a sequence as long as a specified condition is true and then returns the remaining elements.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to return elements from.</param>
		/// <param name="predicate">A function to test each element for a condition.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains the elements from the input sequence starting at the first element in the linear series that does not pass the test specified by <paramref name="predicate" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="predicate" /> is <see langword="null" />.</exception>
		// Token: 0x06000542 RID: 1346 RVA: 0x0000F542 File Offset: 0x0000D742
		public static IEnumerable<TSource> SkipWhile<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (predicate == null)
			{
				throw Error.ArgumentNull("predicate");
			}
			return Enumerable.SkipWhileIterator<TSource>(source, predicate);
		}

		// Token: 0x06000543 RID: 1347 RVA: 0x0000F567 File Offset: 0x0000D767
		private static IEnumerable<TSource> SkipWhileIterator<TSource>(IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			bool yielding = false;
			foreach (TSource tsource in source)
			{
				if (!yielding && !predicate(tsource))
				{
					yielding = true;
				}
				if (yielding)
				{
					yield return tsource;
				}
			}
			IEnumerator<TSource> enumerator = null;
			yield break;
			yield break;
		}

		/// <summary>Bypasses elements in a sequence as long as a specified condition is true and then returns the remaining elements. The element's index is used in the logic of the predicate function.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to return elements from.</param>
		/// <param name="predicate">A function to test each source element for a condition; the second parameter of the function represents the index of the source element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains the elements from the input sequence starting at the first element in the linear series that does not pass the test specified by <paramref name="predicate" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="predicate" /> is <see langword="null" />.</exception>
		// Token: 0x06000544 RID: 1348 RVA: 0x0000F57E File Offset: 0x0000D77E
		public static IEnumerable<TSource> SkipWhile<TSource>(this IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (predicate == null)
			{
				throw Error.ArgumentNull("predicate");
			}
			return Enumerable.SkipWhileIterator<TSource>(source, predicate);
		}

		// Token: 0x06000545 RID: 1349 RVA: 0x0000F5A3 File Offset: 0x0000D7A3
		private static IEnumerable<TSource> SkipWhileIterator<TSource>(IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
		{
			int index = -1;
			bool yielding = false;
			foreach (TSource tsource in source)
			{
				int num = index;
				index = checked(num + 1);
				if (!yielding && !predicate(tsource, index))
				{
					yielding = true;
				}
				if (yielding)
				{
					yield return tsource;
				}
			}
			IEnumerator<TSource> enumerator = null;
			yield break;
			yield break;
		}

		/// <summary>Correlates the elements of two sequences based on matching keys. The default equality comparer is used to compare keys.</summary>
		/// <param name="outer">The first sequence to join.</param>
		/// <param name="inner">The sequence to join to the first sequence.</param>
		/// <param name="outerKeySelector">A function to extract the join key from each element of the first sequence.</param>
		/// <param name="innerKeySelector">A function to extract the join key from each element of the second sequence.</param>
		/// <param name="resultSelector">A function to create a result element from two matching elements.</param>
		/// <typeparam name="TOuter">The type of the elements of the first sequence.</typeparam>
		/// <typeparam name="TInner">The type of the elements of the second sequence.</typeparam>
		/// <typeparam name="TKey">The type of the keys returned by the key selector functions.</typeparam>
		/// <typeparam name="TResult">The type of the result elements.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that has elements of type <paramref name="TResult" /> that are obtained by performing an inner join on two sequences.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="outer" /> or <paramref name="inner" /> or <paramref name="outerKeySelector" /> or <paramref name="innerKeySelector" /> or <paramref name="resultSelector" /> is <see langword="null" />.</exception>
		// Token: 0x06000546 RID: 1350 RVA: 0x0000F5BC File Offset: 0x0000D7BC
		public static IEnumerable<TResult> Join<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, TInner, TResult> resultSelector)
		{
			if (outer == null)
			{
				throw Error.ArgumentNull("outer");
			}
			if (inner == null)
			{
				throw Error.ArgumentNull("inner");
			}
			if (outerKeySelector == null)
			{
				throw Error.ArgumentNull("outerKeySelector");
			}
			if (innerKeySelector == null)
			{
				throw Error.ArgumentNull("innerKeySelector");
			}
			if (resultSelector == null)
			{
				throw Error.ArgumentNull("resultSelector");
			}
			return Enumerable.JoinIterator<TOuter, TInner, TKey, TResult>(outer, inner, outerKeySelector, innerKeySelector, resultSelector, null);
		}

		/// <summary>Correlates the elements of two sequences based on matching keys. A specified <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> is used to compare keys.</summary>
		/// <param name="outer">The first sequence to join.</param>
		/// <param name="inner">The sequence to join to the first sequence.</param>
		/// <param name="outerKeySelector">A function to extract the join key from each element of the first sequence.</param>
		/// <param name="innerKeySelector">A function to extract the join key from each element of the second sequence.</param>
		/// <param name="resultSelector">A function to create a result element from two matching elements.</param>
		/// <param name="comparer">An <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> to hash and compare keys.</param>
		/// <typeparam name="TOuter">The type of the elements of the first sequence.</typeparam>
		/// <typeparam name="TInner">The type of the elements of the second sequence.</typeparam>
		/// <typeparam name="TKey">The type of the keys returned by the key selector functions.</typeparam>
		/// <typeparam name="TResult">The type of the result elements.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that has elements of type <paramref name="TResult" /> that are obtained by performing an inner join on two sequences.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="outer" /> or <paramref name="inner" /> or <paramref name="outerKeySelector" /> or <paramref name="innerKeySelector" /> or <paramref name="resultSelector" /> is <see langword="null" />.</exception>
		// Token: 0x06000547 RID: 1351 RVA: 0x0000F61C File Offset: 0x0000D81C
		public static IEnumerable<TResult> Join<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, TInner, TResult> resultSelector, IEqualityComparer<TKey> comparer)
		{
			if (outer == null)
			{
				throw Error.ArgumentNull("outer");
			}
			if (inner == null)
			{
				throw Error.ArgumentNull("inner");
			}
			if (outerKeySelector == null)
			{
				throw Error.ArgumentNull("outerKeySelector");
			}
			if (innerKeySelector == null)
			{
				throw Error.ArgumentNull("innerKeySelector");
			}
			if (resultSelector == null)
			{
				throw Error.ArgumentNull("resultSelector");
			}
			return Enumerable.JoinIterator<TOuter, TInner, TKey, TResult>(outer, inner, outerKeySelector, innerKeySelector, resultSelector, comparer);
		}

		// Token: 0x06000548 RID: 1352 RVA: 0x0000F67D File Offset: 0x0000D87D
		private static IEnumerable<TResult> JoinIterator<TOuter, TInner, TKey, TResult>(IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, TInner, TResult> resultSelector, IEqualityComparer<TKey> comparer)
		{
			Lookup<TKey, TInner> lookup = Lookup<TKey, TInner>.CreateForJoin(inner, innerKeySelector, comparer);
			foreach (TOuter item in outer)
			{
				Lookup<TKey, TInner>.Grouping g = lookup.GetGrouping(outerKeySelector(item), false);
				if (g != null)
				{
					int num;
					for (int i = 0; i < g.count; i = num + 1)
					{
						yield return resultSelector(item, g.elements[i]);
						num = i;
					}
				}
				g = null;
				item = default(TOuter);
			}
			IEnumerator<TOuter> enumerator = null;
			yield break;
			yield break;
		}

		/// <summary>Correlates the elements of two sequences based on equality of keys and groups the results. The default equality comparer is used to compare keys.</summary>
		/// <param name="outer">The first sequence to join.</param>
		/// <param name="inner">The sequence to join to the first sequence.</param>
		/// <param name="outerKeySelector">A function to extract the join key from each element of the first sequence.</param>
		/// <param name="innerKeySelector">A function to extract the join key from each element of the second sequence.</param>
		/// <param name="resultSelector">A function to create a result element from an element from the first sequence and a collection of matching elements from the second sequence.</param>
		/// <typeparam name="TOuter">The type of the elements of the first sequence.</typeparam>
		/// <typeparam name="TInner">The type of the elements of the second sequence.</typeparam>
		/// <typeparam name="TKey">The type of the keys returned by the key selector functions.</typeparam>
		/// <typeparam name="TResult">The type of the result elements.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains elements of type <paramref name="TResult" /> that are obtained by performing a grouped join on two sequences.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="outer" /> or <paramref name="inner" /> or <paramref name="outerKeySelector" /> or <paramref name="innerKeySelector" /> or <paramref name="resultSelector" /> is <see langword="null" />.</exception>
		// Token: 0x06000549 RID: 1353 RVA: 0x0000F6B4 File Offset: 0x0000D8B4
		public static IEnumerable<TResult> GroupJoin<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, IEnumerable<TInner>, TResult> resultSelector)
		{
			if (outer == null)
			{
				throw Error.ArgumentNull("outer");
			}
			if (inner == null)
			{
				throw Error.ArgumentNull("inner");
			}
			if (outerKeySelector == null)
			{
				throw Error.ArgumentNull("outerKeySelector");
			}
			if (innerKeySelector == null)
			{
				throw Error.ArgumentNull("innerKeySelector");
			}
			if (resultSelector == null)
			{
				throw Error.ArgumentNull("resultSelector");
			}
			return Enumerable.GroupJoinIterator<TOuter, TInner, TKey, TResult>(outer, inner, outerKeySelector, innerKeySelector, resultSelector, null);
		}

		/// <summary>Correlates the elements of two sequences based on key equality and groups the results. A specified <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> is used to compare keys.</summary>
		/// <param name="outer">The first sequence to join.</param>
		/// <param name="inner">The sequence to join to the first sequence.</param>
		/// <param name="outerKeySelector">A function to extract the join key from each element of the first sequence.</param>
		/// <param name="innerKeySelector">A function to extract the join key from each element of the second sequence.</param>
		/// <param name="resultSelector">A function to create a result element from an element from the first sequence and a collection of matching elements from the second sequence.</param>
		/// <param name="comparer">An <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> to hash and compare keys.</param>
		/// <typeparam name="TOuter">The type of the elements of the first sequence.</typeparam>
		/// <typeparam name="TInner">The type of the elements of the second sequence.</typeparam>
		/// <typeparam name="TKey">The type of the keys returned by the key selector functions.</typeparam>
		/// <typeparam name="TResult">The type of the result elements.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains elements of type <paramref name="TResult" /> that are obtained by performing a grouped join on two sequences.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="outer" /> or <paramref name="inner" /> or <paramref name="outerKeySelector" /> or <paramref name="innerKeySelector" /> or <paramref name="resultSelector" /> is <see langword="null" />.</exception>
		// Token: 0x0600054A RID: 1354 RVA: 0x0000F714 File Offset: 0x0000D914
		public static IEnumerable<TResult> GroupJoin<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, IEnumerable<TInner>, TResult> resultSelector, IEqualityComparer<TKey> comparer)
		{
			if (outer == null)
			{
				throw Error.ArgumentNull("outer");
			}
			if (inner == null)
			{
				throw Error.ArgumentNull("inner");
			}
			if (outerKeySelector == null)
			{
				throw Error.ArgumentNull("outerKeySelector");
			}
			if (innerKeySelector == null)
			{
				throw Error.ArgumentNull("innerKeySelector");
			}
			if (resultSelector == null)
			{
				throw Error.ArgumentNull("resultSelector");
			}
			return Enumerable.GroupJoinIterator<TOuter, TInner, TKey, TResult>(outer, inner, outerKeySelector, innerKeySelector, resultSelector, comparer);
		}

		// Token: 0x0600054B RID: 1355 RVA: 0x0000F775 File Offset: 0x0000D975
		private static IEnumerable<TResult> GroupJoinIterator<TOuter, TInner, TKey, TResult>(IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, IEnumerable<TInner>, TResult> resultSelector, IEqualityComparer<TKey> comparer)
		{
			Lookup<TKey, TInner> lookup = Lookup<TKey, TInner>.CreateForJoin(inner, innerKeySelector, comparer);
			foreach (TOuter touter in outer)
			{
				yield return resultSelector(touter, lookup[outerKeySelector(touter)]);
			}
			IEnumerator<TOuter> enumerator = null;
			yield break;
			yield break;
		}

		/// <summary>Sorts the elements of a sequence in ascending order according to a key.</summary>
		/// <param name="source">A sequence of values to order.</param>
		/// <param name="keySelector">A function to extract a key from an element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
		/// <returns>An <see cref="T:System.Linq.IOrderedEnumerable`1" /> whose elements are sorted according to a key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="keySelector" /> is <see langword="null" />.</exception>
		// Token: 0x0600054C RID: 1356 RVA: 0x0000F7AA File Offset: 0x0000D9AA
		public static IOrderedEnumerable<TSource> OrderBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			return new OrderedEnumerable<TSource, TKey>(source, keySelector, null, false);
		}

		/// <summary>Sorts the elements of a sequence in ascending order by using a specified comparer.</summary>
		/// <param name="source">A sequence of values to order.</param>
		/// <param name="keySelector">A function to extract a key from an element.</param>
		/// <param name="comparer">An <see cref="T:System.Collections.Generic.IComparer`1" /> to compare keys.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
		/// <returns>An <see cref="T:System.Linq.IOrderedEnumerable`1" /> whose elements are sorted according to a key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="keySelector" /> is <see langword="null" />.</exception>
		// Token: 0x0600054D RID: 1357 RVA: 0x0000F7B5 File Offset: 0x0000D9B5
		public static IOrderedEnumerable<TSource> OrderBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IComparer<TKey> comparer)
		{
			return new OrderedEnumerable<TSource, TKey>(source, keySelector, comparer, false);
		}

		/// <summary>Sorts the elements of a sequence in descending order according to a key.</summary>
		/// <param name="source">A sequence of values to order.</param>
		/// <param name="keySelector">A function to extract a key from an element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
		/// <returns>An <see cref="T:System.Linq.IOrderedEnumerable`1" /> whose elements are sorted in descending order according to a key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="keySelector" /> is <see langword="null" />.</exception>
		// Token: 0x0600054E RID: 1358 RVA: 0x0000F7C0 File Offset: 0x0000D9C0
		public static IOrderedEnumerable<TSource> OrderByDescending<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			return new OrderedEnumerable<TSource, TKey>(source, keySelector, null, true);
		}

		/// <summary>Sorts the elements of a sequence in descending order by using a specified comparer.</summary>
		/// <param name="source">A sequence of values to order.</param>
		/// <param name="keySelector">A function to extract a key from an element.</param>
		/// <param name="comparer">An <see cref="T:System.Collections.Generic.IComparer`1" /> to compare keys.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
		/// <returns>An <see cref="T:System.Linq.IOrderedEnumerable`1" /> whose elements are sorted in descending order according to a key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="keySelector" /> is <see langword="null" />.</exception>
		// Token: 0x0600054F RID: 1359 RVA: 0x0000F7CB File Offset: 0x0000D9CB
		public static IOrderedEnumerable<TSource> OrderByDescending<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IComparer<TKey> comparer)
		{
			return new OrderedEnumerable<TSource, TKey>(source, keySelector, comparer, true);
		}

		/// <summary>Performs a subsequent ordering of the elements in a sequence in ascending order according to a key.</summary>
		/// <param name="source">An <see cref="T:System.Linq.IOrderedEnumerable`1" /> that contains elements to sort.</param>
		/// <param name="keySelector">A function to extract a key from each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
		/// <returns>An <see cref="T:System.Linq.IOrderedEnumerable`1" /> whose elements are sorted according to a key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="keySelector" /> is <see langword="null" />.</exception>
		// Token: 0x06000550 RID: 1360 RVA: 0x0000F7D6 File Offset: 0x0000D9D6
		public static IOrderedEnumerable<TSource> ThenBy<TSource, TKey>(this IOrderedEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			return source.CreateOrderedEnumerable<TKey>(keySelector, null, false);
		}

		/// <summary>Performs a subsequent ordering of the elements in a sequence in ascending order by using a specified comparer.</summary>
		/// <param name="source">An <see cref="T:System.Linq.IOrderedEnumerable`1" /> that contains elements to sort.</param>
		/// <param name="keySelector">A function to extract a key from each element.</param>
		/// <param name="comparer">An <see cref="T:System.Collections.Generic.IComparer`1" /> to compare keys.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
		/// <returns>An <see cref="T:System.Linq.IOrderedEnumerable`1" /> whose elements are sorted according to a key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="keySelector" /> is <see langword="null" />.</exception>
		// Token: 0x06000551 RID: 1361 RVA: 0x0000F7EF File Offset: 0x0000D9EF
		public static IOrderedEnumerable<TSource> ThenBy<TSource, TKey>(this IOrderedEnumerable<TSource> source, Func<TSource, TKey> keySelector, IComparer<TKey> comparer)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			return source.CreateOrderedEnumerable<TKey>(keySelector, comparer, false);
		}

		/// <summary>Performs a subsequent ordering of the elements in a sequence in descending order, according to a key.</summary>
		/// <param name="source">An <see cref="T:System.Linq.IOrderedEnumerable`1" /> that contains elements to sort.</param>
		/// <param name="keySelector">A function to extract a key from each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
		/// <returns>An <see cref="T:System.Linq.IOrderedEnumerable`1" /> whose elements are sorted in descending order according to a key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="keySelector" /> is <see langword="null" />.</exception>
		// Token: 0x06000552 RID: 1362 RVA: 0x0000F808 File Offset: 0x0000DA08
		public static IOrderedEnumerable<TSource> ThenByDescending<TSource, TKey>(this IOrderedEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			return source.CreateOrderedEnumerable<TKey>(keySelector, null, true);
		}

		/// <summary>Performs a subsequent ordering of the elements in a sequence in descending order by using a specified comparer.</summary>
		/// <param name="source">An <see cref="T:System.Linq.IOrderedEnumerable`1" /> that contains elements to sort.</param>
		/// <param name="keySelector">A function to extract a key from each element.</param>
		/// <param name="comparer">An <see cref="T:System.Collections.Generic.IComparer`1" /> to compare keys.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
		/// <returns>An <see cref="T:System.Linq.IOrderedEnumerable`1" /> whose elements are sorted in descending order according to a key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="keySelector" /> is <see langword="null" />.</exception>
		// Token: 0x06000553 RID: 1363 RVA: 0x0000F821 File Offset: 0x0000DA21
		public static IOrderedEnumerable<TSource> ThenByDescending<TSource, TKey>(this IOrderedEnumerable<TSource> source, Func<TSource, TKey> keySelector, IComparer<TKey> comparer)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			return source.CreateOrderedEnumerable<TKey>(keySelector, comparer, true);
		}

		/// <summary>Groups the elements of a sequence according to a specified key selector function.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose elements to group.</param>
		/// <param name="keySelector">A function to extract the key for each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
		/// <returns>An IEnumerable&lt;IGrouping&lt;TKey, TSource&gt;&gt; in C# or IEnumerable(Of IGrouping(Of TKey, TSource)) in Visual Basic where each <see cref="T:System.Linq.IGrouping`2" /> object contains a sequence of objects and a key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="keySelector" /> is <see langword="null" />.</exception>
		// Token: 0x06000554 RID: 1364 RVA: 0x0000F83A File Offset: 0x0000DA3A
		public static IEnumerable<IGrouping<TKey, TSource>> GroupBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			return new GroupedEnumerable<TSource, TKey, TSource>(source, keySelector, IdentityFunction<TSource>.Instance, null);
		}

		/// <summary>Groups the elements of a sequence according to a specified key selector function and compares the keys by using a specified comparer.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose elements to group.</param>
		/// <param name="keySelector">A function to extract the key for each element.</param>
		/// <param name="comparer">An <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> to compare keys.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
		/// <returns>An IEnumerable&lt;IGrouping&lt;TKey, TSource&gt;&gt; in C# or IEnumerable(Of IGrouping(Of TKey, TSource)) in Visual Basic where each <see cref="T:System.Linq.IGrouping`2" /> object contains a collection of objects and a key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="keySelector" /> is <see langword="null" />.</exception>
		// Token: 0x06000555 RID: 1365 RVA: 0x0000F849 File Offset: 0x0000DA49
		public static IEnumerable<IGrouping<TKey, TSource>> GroupBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer)
		{
			return new GroupedEnumerable<TSource, TKey, TSource>(source, keySelector, IdentityFunction<TSource>.Instance, comparer);
		}

		/// <summary>Groups the elements of a sequence according to a specified key selector function and projects the elements for each group by using a specified function.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose elements to group.</param>
		/// <param name="keySelector">A function to extract the key for each element.</param>
		/// <param name="elementSelector">A function to map each source element to an element in the <see cref="T:System.Linq.IGrouping`2" />.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
		/// <typeparam name="TElement">The type of the elements in the <see cref="T:System.Linq.IGrouping`2" />.</typeparam>
		/// <returns>An IEnumerable&lt;IGrouping&lt;TKey, TElement&gt;&gt; in C# or IEnumerable(Of IGrouping(Of TKey, TElement)) in Visual Basic where each <see cref="T:System.Linq.IGrouping`2" /> object contains a collection of objects of type <paramref name="TElement" /> and a key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="keySelector" /> or <paramref name="elementSelector" /> is <see langword="null" />.</exception>
		// Token: 0x06000556 RID: 1366 RVA: 0x0000F858 File Offset: 0x0000DA58
		public static IEnumerable<IGrouping<TKey, TElement>> GroupBy<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector)
		{
			return new GroupedEnumerable<TSource, TKey, TElement>(source, keySelector, elementSelector, null);
		}

		/// <summary>Groups the elements of a sequence according to a key selector function. The keys are compared by using a comparer and each group's elements are projected by using a specified function.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose elements to group.</param>
		/// <param name="keySelector">A function to extract the key for each element.</param>
		/// <param name="elementSelector">A function to map each source element to an element in an <see cref="T:System.Linq.IGrouping`2" />.</param>
		/// <param name="comparer">An <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> to compare keys.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
		/// <typeparam name="TElement">The type of the elements in the <see cref="T:System.Linq.IGrouping`2" />.</typeparam>
		/// <returns>An IEnumerable&lt;IGrouping&lt;TKey, TElement&gt;&gt; in C# or IEnumerable(Of IGrouping(Of TKey, TElement)) in Visual Basic where each <see cref="T:System.Linq.IGrouping`2" /> object contains a collection of objects of type <paramref name="TElement" /> and a key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="keySelector" /> or <paramref name="elementSelector" /> is <see langword="null" />.</exception>
		// Token: 0x06000557 RID: 1367 RVA: 0x0000F863 File Offset: 0x0000DA63
		public static IEnumerable<IGrouping<TKey, TElement>> GroupBy<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, IEqualityComparer<TKey> comparer)
		{
			return new GroupedEnumerable<TSource, TKey, TElement>(source, keySelector, elementSelector, comparer);
		}

		/// <summary>Groups the elements of a sequence according to a specified key selector function and creates a result value from each group and its key.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose elements to group.</param>
		/// <param name="keySelector">A function to extract the key for each element.</param>
		/// <param name="resultSelector">A function to create a result value from each group.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
		/// <typeparam name="TResult">The type of the result value returned by <paramref name="resultSelector" />.</typeparam>
		/// <returns>A collection of elements of type <paramref name="TResult" /> where each element represents a projection over a group and its key.</returns>
		// Token: 0x06000558 RID: 1368 RVA: 0x0000F86E File Offset: 0x0000DA6E
		public static IEnumerable<TResult> GroupBy<TSource, TKey, TResult>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TKey, IEnumerable<TSource>, TResult> resultSelector)
		{
			return new GroupedEnumerable<TSource, TKey, TSource, TResult>(source, keySelector, IdentityFunction<TSource>.Instance, resultSelector, null);
		}

		/// <summary>Groups the elements of a sequence according to a specified key selector function and creates a result value from each group and its key. The elements of each group are projected by using a specified function.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose elements to group.</param>
		/// <param name="keySelector">A function to extract the key for each element.</param>
		/// <param name="elementSelector">A function to map each source element to an element in an <see cref="T:System.Linq.IGrouping`2" />.</param>
		/// <param name="resultSelector">A function to create a result value from each group.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
		/// <typeparam name="TElement">The type of the elements in each <see cref="T:System.Linq.IGrouping`2" />.</typeparam>
		/// <typeparam name="TResult">The type of the result value returned by <paramref name="resultSelector" />.</typeparam>
		/// <returns>A collection of elements of type <paramref name="TResult" /> where each element represents a projection over a group and its key.</returns>
		// Token: 0x06000559 RID: 1369 RVA: 0x0000F87E File Offset: 0x0000DA7E
		public static IEnumerable<TResult> GroupBy<TSource, TKey, TElement, TResult>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, Func<TKey, IEnumerable<TElement>, TResult> resultSelector)
		{
			return new GroupedEnumerable<TSource, TKey, TElement, TResult>(source, keySelector, elementSelector, resultSelector, null);
		}

		/// <summary>Groups the elements of a sequence according to a specified key selector function and creates a result value from each group and its key. The keys are compared by using a specified comparer.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose elements to group.</param>
		/// <param name="keySelector">A function to extract the key for each element.</param>
		/// <param name="resultSelector">A function to create a result value from each group.</param>
		/// <param name="comparer">An <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> to compare keys with.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
		/// <typeparam name="TResult">The type of the result value returned by <paramref name="resultSelector" />.</typeparam>
		/// <returns>A collection of elements of type <paramref name="TResult" /> where each element represents a projection over a group and its key.</returns>
		// Token: 0x0600055A RID: 1370 RVA: 0x0000F88A File Offset: 0x0000DA8A
		public static IEnumerable<TResult> GroupBy<TSource, TKey, TResult>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TKey, IEnumerable<TSource>, TResult> resultSelector, IEqualityComparer<TKey> comparer)
		{
			return new GroupedEnumerable<TSource, TKey, TSource, TResult>(source, keySelector, IdentityFunction<TSource>.Instance, resultSelector, comparer);
		}

		/// <summary>Groups the elements of a sequence according to a specified key selector function and creates a result value from each group and its key. Key values are compared by using a specified comparer, and the elements of each group are projected by using a specified function.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose elements to group.</param>
		/// <param name="keySelector">A function to extract the key for each element.</param>
		/// <param name="elementSelector">A function to map each source element to an element in an <see cref="T:System.Linq.IGrouping`2" />.</param>
		/// <param name="resultSelector">A function to create a result value from each group.</param>
		/// <param name="comparer">An <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> to compare keys with.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
		/// <typeparam name="TElement">The type of the elements in each <see cref="T:System.Linq.IGrouping`2" />.</typeparam>
		/// <typeparam name="TResult">The type of the result value returned by <paramref name="resultSelector" />.</typeparam>
		/// <returns>A collection of elements of type <paramref name="TResult" /> where each element represents a projection over a group and its key.</returns>
		// Token: 0x0600055B RID: 1371 RVA: 0x0000F89A File Offset: 0x0000DA9A
		public static IEnumerable<TResult> GroupBy<TSource, TKey, TElement, TResult>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, Func<TKey, IEnumerable<TElement>, TResult> resultSelector, IEqualityComparer<TKey> comparer)
		{
			return new GroupedEnumerable<TSource, TKey, TElement, TResult>(source, keySelector, elementSelector, resultSelector, comparer);
		}

		/// <summary>Concatenates two sequences.</summary>
		/// <param name="first">The first sequence to concatenate.</param>
		/// <param name="second">The sequence to concatenate to the first sequence.</param>
		/// <typeparam name="TSource">The type of the elements of the input sequences.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains the concatenated elements of the two input sequences.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="first" /> or <paramref name="second" /> is <see langword="null" />.</exception>
		// Token: 0x0600055C RID: 1372 RVA: 0x0000F8A7 File Offset: 0x0000DAA7
		public static IEnumerable<TSource> Concat<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
		{
			if (first == null)
			{
				throw Error.ArgumentNull("first");
			}
			if (second == null)
			{
				throw Error.ArgumentNull("second");
			}
			return Enumerable.ConcatIterator<TSource>(first, second);
		}

		// Token: 0x0600055D RID: 1373 RVA: 0x0000F8CC File Offset: 0x0000DACC
		private static IEnumerable<TSource> ConcatIterator<TSource>(IEnumerable<TSource> first, IEnumerable<TSource> second)
		{
			foreach (TSource tsource in first)
			{
				yield return tsource;
			}
			IEnumerator<TSource> enumerator = null;
			foreach (TSource tsource2 in second)
			{
				yield return tsource2;
			}
			enumerator = null;
			yield break;
			yield break;
		}

		/// <summary>Appends a value to the end of the sequence.</summary>
		/// <param name="source">A sequence of values. </param>
		/// <param name="element">The value to append to <paramref name="source" />.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />. </typeparam>
		/// <returns>A new sequence that ends with <paramref name="element" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x0600055E RID: 1374 RVA: 0x0000F8E3 File Offset: 0x0000DAE3
		public static IEnumerable<TSource> Append<TSource>(this IEnumerable<TSource> source, TSource element)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			return Enumerable.AppendIterator<TSource>(source, element);
		}

		// Token: 0x0600055F RID: 1375 RVA: 0x0000F8FA File Offset: 0x0000DAFA
		private static IEnumerable<TSource> AppendIterator<TSource>(IEnumerable<TSource> source, TSource element)
		{
			foreach (TSource tsource in source)
			{
				yield return tsource;
			}
			IEnumerator<TSource> enumerator = null;
			yield return element;
			yield break;
			yield break;
		}

		/// <summary>Adds a value to the beginning of the sequence.</summary>
		/// <param name="source">A sequence of values. </param>
		/// <param name="element">The value to prepend to <paramref name="source" />. </param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>A new sequence that begins with <paramref name="element" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="source" /> is <see langword="null" />. </exception>
		// Token: 0x06000560 RID: 1376 RVA: 0x0000F911 File Offset: 0x0000DB11
		public static IEnumerable<TSource> Prepend<TSource>(this IEnumerable<TSource> source, TSource element)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			return Enumerable.PrependIterator<TSource>(source, element);
		}

		// Token: 0x06000561 RID: 1377 RVA: 0x0000F928 File Offset: 0x0000DB28
		private static IEnumerable<TSource> PrependIterator<TSource>(IEnumerable<TSource> source, TSource element)
		{
			yield return element;
			foreach (TSource tsource in source)
			{
				yield return tsource;
			}
			IEnumerator<TSource> enumerator = null;
			yield break;
			yield break;
		}

		/// <summary>Applies a specified function to the corresponding elements of two sequences, producing a sequence of the results.</summary>
		/// <param name="first">The first sequence to merge.</param>
		/// <param name="second">The second sequence to merge.</param>
		/// <param name="resultSelector">A function that specifies how to merge the elements from the two sequences.</param>
		/// <typeparam name="TFirst">The type of the elements of the first input sequence.</typeparam>
		/// <typeparam name="TSecond">The type of the elements of the second input sequence.</typeparam>
		/// <typeparam name="TResult">The type of the elements of the result sequence.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains merged elements of two input sequences.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="first" /> or <paramref name="second" /> is <see langword="null" />.</exception>
		// Token: 0x06000562 RID: 1378 RVA: 0x0000F93F File Offset: 0x0000DB3F
		public static IEnumerable<TResult> Zip<TFirst, TSecond, TResult>(this IEnumerable<TFirst> first, IEnumerable<TSecond> second, Func<TFirst, TSecond, TResult> resultSelector)
		{
			if (first == null)
			{
				throw Error.ArgumentNull("first");
			}
			if (second == null)
			{
				throw Error.ArgumentNull("second");
			}
			if (resultSelector == null)
			{
				throw Error.ArgumentNull("resultSelector");
			}
			return Enumerable.ZipIterator<TFirst, TSecond, TResult>(first, second, resultSelector);
		}

		// Token: 0x06000563 RID: 1379 RVA: 0x0000F973 File Offset: 0x0000DB73
		private static IEnumerable<TResult> ZipIterator<TFirst, TSecond, TResult>(IEnumerable<TFirst> first, IEnumerable<TSecond> second, Func<TFirst, TSecond, TResult> resultSelector)
		{
			using (IEnumerator<TFirst> e = first.GetEnumerator())
			{
				using (IEnumerator<TSecond> e2 = second.GetEnumerator())
				{
					while (e.MoveNext() && e2.MoveNext())
					{
						yield return resultSelector(e.Current, e2.Current);
					}
				}
				IEnumerator<TSecond> e2 = null;
			}
			IEnumerator<TFirst> e = null;
			yield break;
			yield break;
		}

		/// <summary>Returns distinct elements from a sequence by using the default equality comparer to compare values.</summary>
		/// <param name="source">The sequence to remove duplicate elements from.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains distinct elements from the source sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x06000564 RID: 1380 RVA: 0x0000F991 File Offset: 0x0000DB91
		public static IEnumerable<TSource> Distinct<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			return Enumerable.DistinctIterator<TSource>(source, null);
		}

		/// <summary>Returns distinct elements from a sequence by using a specified <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> to compare values.</summary>
		/// <param name="source">The sequence to remove duplicate elements from.</param>
		/// <param name="comparer">An <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> to compare values.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains distinct elements from the source sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x06000565 RID: 1381 RVA: 0x0000F9A8 File Offset: 0x0000DBA8
		public static IEnumerable<TSource> Distinct<TSource>(this IEnumerable<TSource> source, IEqualityComparer<TSource> comparer)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			return Enumerable.DistinctIterator<TSource>(source, comparer);
		}

		// Token: 0x06000566 RID: 1382 RVA: 0x0000F9BF File Offset: 0x0000DBBF
		private static IEnumerable<TSource> DistinctIterator<TSource>(IEnumerable<TSource> source, IEqualityComparer<TSource> comparer)
		{
			Set<TSource> set = new Set<TSource>(comparer);
			foreach (TSource tsource in source)
			{
				if (set.Add(tsource))
				{
					yield return tsource;
				}
			}
			IEnumerator<TSource> enumerator = null;
			yield break;
			yield break;
		}

		/// <summary>Produces the set union of two sequences by using the default equality comparer.</summary>
		/// <param name="first">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose distinct elements form the first set for the union.</param>
		/// <param name="second">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose distinct elements form the second set for the union.</param>
		/// <typeparam name="TSource">The type of the elements of the input sequences.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains the elements from both input sequences, excluding duplicates.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="first" /> or <paramref name="second" /> is <see langword="null" />.</exception>
		// Token: 0x06000567 RID: 1383 RVA: 0x0000F9D6 File Offset: 0x0000DBD6
		public static IEnumerable<TSource> Union<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
		{
			if (first == null)
			{
				throw Error.ArgumentNull("first");
			}
			if (second == null)
			{
				throw Error.ArgumentNull("second");
			}
			return Enumerable.UnionIterator<TSource>(first, second, null);
		}

		/// <summary>Produces the set union of two sequences by using a specified <see cref="T:System.Collections.Generic.IEqualityComparer`1" />.</summary>
		/// <param name="first">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose distinct elements form the first set for the union.</param>
		/// <param name="second">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose distinct elements form the second set for the union.</param>
		/// <param name="comparer">The <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> to compare values.</param>
		/// <typeparam name="TSource">The type of the elements of the input sequences.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains the elements from both input sequences, excluding duplicates.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="first" /> or <paramref name="second" /> is <see langword="null" />.</exception>
		// Token: 0x06000568 RID: 1384 RVA: 0x0000F9FC File Offset: 0x0000DBFC
		public static IEnumerable<TSource> Union<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			if (first == null)
			{
				throw Error.ArgumentNull("first");
			}
			if (second == null)
			{
				throw Error.ArgumentNull("second");
			}
			return Enumerable.UnionIterator<TSource>(first, second, comparer);
		}

		// Token: 0x06000569 RID: 1385 RVA: 0x0000FA22 File Offset: 0x0000DC22
		private static IEnumerable<TSource> UnionIterator<TSource>(IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			Set<TSource> set = new Set<TSource>(comparer);
			foreach (TSource tsource in first)
			{
				if (set.Add(tsource))
				{
					yield return tsource;
				}
			}
			IEnumerator<TSource> enumerator = null;
			foreach (TSource tsource2 in second)
			{
				if (set.Add(tsource2))
				{
					yield return tsource2;
				}
			}
			enumerator = null;
			yield break;
			yield break;
		}

		/// <summary>Produces the set intersection of two sequences by using the default equality comparer to compare values.</summary>
		/// <param name="first">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose distinct elements that also appear in <paramref name="second" /> will be returned.</param>
		/// <param name="second">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose distinct elements that also appear in the first sequence will be returned.</param>
		/// <typeparam name="TSource">The type of the elements of the input sequences.</typeparam>
		/// <returns>A sequence that contains the elements that form the set intersection of two sequences.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="first" /> or <paramref name="second" /> is <see langword="null" />.</exception>
		// Token: 0x0600056A RID: 1386 RVA: 0x0000FA40 File Offset: 0x0000DC40
		public static IEnumerable<TSource> Intersect<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
		{
			if (first == null)
			{
				throw Error.ArgumentNull("first");
			}
			if (second == null)
			{
				throw Error.ArgumentNull("second");
			}
			return Enumerable.IntersectIterator<TSource>(first, second, null);
		}

		/// <summary>Produces the set intersection of two sequences by using the specified <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> to compare values.</summary>
		/// <param name="first">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose distinct elements that also appear in <paramref name="second" /> will be returned.</param>
		/// <param name="second">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose distinct elements that also appear in the first sequence will be returned.</param>
		/// <param name="comparer">An <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> to compare values.</param>
		/// <typeparam name="TSource">The type of the elements of the input sequences.</typeparam>
		/// <returns>A sequence that contains the elements that form the set intersection of two sequences.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="first" /> or <paramref name="second" /> is <see langword="null" />.</exception>
		// Token: 0x0600056B RID: 1387 RVA: 0x0000FA66 File Offset: 0x0000DC66
		public static IEnumerable<TSource> Intersect<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			if (first == null)
			{
				throw Error.ArgumentNull("first");
			}
			if (second == null)
			{
				throw Error.ArgumentNull("second");
			}
			return Enumerable.IntersectIterator<TSource>(first, second, comparer);
		}

		// Token: 0x0600056C RID: 1388 RVA: 0x0000FA8C File Offset: 0x0000DC8C
		private static IEnumerable<TSource> IntersectIterator<TSource>(IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			Set<TSource> set = new Set<TSource>(comparer);
			foreach (TSource value in second)
			{
				set.Add(value);
			}
			foreach (TSource tsource in first)
			{
				if (set.Remove(tsource))
				{
					yield return tsource;
				}
			}
			IEnumerator<TSource> enumerator2 = null;
			yield break;
			yield break;
		}

		/// <summary>Produces the set difference of two sequences by using the default equality comparer to compare values.</summary>
		/// <param name="first">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose elements that are not also in <paramref name="second" /> will be returned.</param>
		/// <param name="second">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose elements that also occur in the first sequence will cause those elements to be removed from the returned sequence.</param>
		/// <typeparam name="TSource">The type of the elements of the input sequences.</typeparam>
		/// <returns>A sequence that contains the set difference of the elements of two sequences.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="first" /> or <paramref name="second" /> is <see langword="null" />.</exception>
		// Token: 0x0600056D RID: 1389 RVA: 0x0000FAAA File Offset: 0x0000DCAA
		public static IEnumerable<TSource> Except<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
		{
			if (first == null)
			{
				throw Error.ArgumentNull("first");
			}
			if (second == null)
			{
				throw Error.ArgumentNull("second");
			}
			return Enumerable.ExceptIterator<TSource>(first, second, null);
		}

		/// <summary>Produces the set difference of two sequences by using the specified <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> to compare values.</summary>
		/// <param name="first">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose elements that are not also in <paramref name="second" /> will be returned.</param>
		/// <param name="second">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose elements that also occur in the first sequence will cause those elements to be removed from the returned sequence.</param>
		/// <param name="comparer">An <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> to compare values.</param>
		/// <typeparam name="TSource">The type of the elements of the input sequences.</typeparam>
		/// <returns>A sequence that contains the set difference of the elements of two sequences.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="first" /> or <paramref name="second" /> is <see langword="null" />.</exception>
		// Token: 0x0600056E RID: 1390 RVA: 0x0000FAD0 File Offset: 0x0000DCD0
		public static IEnumerable<TSource> Except<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			if (first == null)
			{
				throw Error.ArgumentNull("first");
			}
			if (second == null)
			{
				throw Error.ArgumentNull("second");
			}
			return Enumerable.ExceptIterator<TSource>(first, second, comparer);
		}

		// Token: 0x0600056F RID: 1391 RVA: 0x0000FAF6 File Offset: 0x0000DCF6
		private static IEnumerable<TSource> ExceptIterator<TSource>(IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			Set<TSource> set = new Set<TSource>(comparer);
			foreach (TSource value in second)
			{
				set.Add(value);
			}
			foreach (TSource tsource in first)
			{
				if (set.Add(tsource))
				{
					yield return tsource;
				}
			}
			IEnumerator<TSource> enumerator2 = null;
			yield break;
			yield break;
		}

		/// <summary>Inverts the order of the elements in a sequence.</summary>
		/// <param name="source">A sequence of values to reverse.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>A sequence whose elements correspond to those of the input sequence in reverse order.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x06000570 RID: 1392 RVA: 0x0000FB14 File Offset: 0x0000DD14
		public static IEnumerable<TSource> Reverse<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			return Enumerable.ReverseIterator<TSource>(source);
		}

		// Token: 0x06000571 RID: 1393 RVA: 0x0000FB2A File Offset: 0x0000DD2A
		private static IEnumerable<TSource> ReverseIterator<TSource>(IEnumerable<TSource> source)
		{
			Buffer<TSource> buffer = new Buffer<TSource>(source);
			int num;
			for (int i = buffer.count - 1; i >= 0; i = num - 1)
			{
				yield return buffer.items[i];
				num = i;
			}
			yield break;
		}

		/// <summary>Determines whether two sequences are equal by comparing the elements by using the default equality comparer for their type.</summary>
		/// <param name="first">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to compare to <paramref name="second" />.</param>
		/// <param name="second">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to compare to the first sequence.</param>
		/// <typeparam name="TSource">The type of the elements of the input sequences.</typeparam>
		/// <returns>
		///     <see langword="true" /> if the two source sequences are of equal length and their corresponding elements are equal according to the default equality comparer for their type; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="first" /> or <paramref name="second" /> is <see langword="null" />.</exception>
		// Token: 0x06000572 RID: 1394 RVA: 0x0000FB3A File Offset: 0x0000DD3A
		public static bool SequenceEqual<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
		{
			return first.SequenceEqual(second, null);
		}

		/// <summary>Determines whether two sequences are equal by comparing their elements by using a specified <see cref="T:System.Collections.Generic.IEqualityComparer`1" />.</summary>
		/// <param name="first">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to compare to <paramref name="second" />.</param>
		/// <param name="second">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to compare to the first sequence.</param>
		/// <param name="comparer">An <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> to use to compare elements.</param>
		/// <typeparam name="TSource">The type of the elements of the input sequences.</typeparam>
		/// <returns>
		///     <see langword="true" /> if the two source sequences are of equal length and their corresponding elements compare equal according to <paramref name="comparer" />; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="first" /> or <paramref name="second" /> is <see langword="null" />.</exception>
		// Token: 0x06000573 RID: 1395 RVA: 0x0000FB44 File Offset: 0x0000DD44
		public static bool SequenceEqual<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			if (comparer == null)
			{
				comparer = EqualityComparer<TSource>.Default;
			}
			if (first == null)
			{
				throw Error.ArgumentNull("first");
			}
			if (second == null)
			{
				throw Error.ArgumentNull("second");
			}
			using (IEnumerator<TSource> enumerator = first.GetEnumerator())
			{
				using (IEnumerator<TSource> enumerator2 = second.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						if (!enumerator2.MoveNext() || !comparer.Equals(enumerator.Current, enumerator2.Current))
						{
							return false;
						}
					}
					if (enumerator2.MoveNext())
					{
						return false;
					}
				}
			}
			return true;
		}

		/// <summary>Returns the input typed as <see cref="T:System.Collections.Generic.IEnumerable`1" />.</summary>
		/// <param name="source">The sequence to type as <see cref="T:System.Collections.Generic.IEnumerable`1" />.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The input sequence typed as <see cref="T:System.Collections.Generic.IEnumerable`1" />.</returns>
		// Token: 0x06000574 RID: 1396 RVA: 0x000021A0 File Offset: 0x000003A0
		public static IEnumerable<TSource> AsEnumerable<TSource>(this IEnumerable<TSource> source)
		{
			return source;
		}

		/// <summary>Creates an array from a <see cref="T:System.Collections.Generic.IEnumerable`1" />.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to create an array from.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>An array that contains the elements from the input sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x06000575 RID: 1397 RVA: 0x0000FBF0 File Offset: 0x0000DDF0
		public static TSource[] ToArray<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			return new Buffer<TSource>(source).ToArray();
		}

		/// <summary>Creates a <see cref="T:System.Collections.Generic.List`1" /> from an <see cref="T:System.Collections.Generic.IEnumerable`1" />.</summary>
		/// <param name="source">The <see cref="T:System.Collections.Generic.IEnumerable`1" /> to create a <see cref="T:System.Collections.Generic.List`1" /> from.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>A <see cref="T:System.Collections.Generic.List`1" /> that contains elements from the input sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x06000576 RID: 1398 RVA: 0x0000FC19 File Offset: 0x0000DE19
		public static List<TSource> ToList<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			return new List<TSource>(source);
		}

		/// <summary>Creates a <see cref="T:System.Collections.Generic.Dictionary`2" /> from an <see cref="T:System.Collections.Generic.IEnumerable`1" /> according to a specified key selector function.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to create a <see cref="T:System.Collections.Generic.Dictionary`2" /> from.</param>
		/// <param name="keySelector">A function to extract a key from each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
		/// <returns>A <see cref="T:System.Collections.Generic.Dictionary`2" /> that contains keys and values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="keySelector" /> is <see langword="null" />.-or-
		///         <paramref name="keySelector" /> produces a key that is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="keySelector" /> produces duplicate keys for two elements.</exception>
		// Token: 0x06000577 RID: 1399 RVA: 0x0000FC2F File Offset: 0x0000DE2F
		public static Dictionary<TKey, TSource> ToDictionary<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			return source.ToDictionary(keySelector, IdentityFunction<TSource>.Instance, null);
		}

		/// <summary>Creates a <see cref="T:System.Collections.Generic.Dictionary`2" /> from an <see cref="T:System.Collections.Generic.IEnumerable`1" /> according to a specified key selector function and key comparer.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to create a <see cref="T:System.Collections.Generic.Dictionary`2" /> from.</param>
		/// <param name="keySelector">A function to extract a key from each element.</param>
		/// <param name="comparer">An <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> to compare keys.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the keys returned by <paramref name="keySelector" />.</typeparam>
		/// <returns>A <see cref="T:System.Collections.Generic.Dictionary`2" /> that contains keys and values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="keySelector" /> is <see langword="null" />.-or-
		///         <paramref name="keySelector" /> produces a key that is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="keySelector" /> produces duplicate keys for two elements.</exception>
		// Token: 0x06000578 RID: 1400 RVA: 0x0000FC3E File Offset: 0x0000DE3E
		public static Dictionary<TKey, TSource> ToDictionary<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer)
		{
			return source.ToDictionary(keySelector, IdentityFunction<TSource>.Instance, comparer);
		}

		/// <summary>Creates a <see cref="T:System.Collections.Generic.Dictionary`2" /> from an <see cref="T:System.Collections.Generic.IEnumerable`1" /> according to specified key selector and element selector functions.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to create a <see cref="T:System.Collections.Generic.Dictionary`2" /> from.</param>
		/// <param name="keySelector">A function to extract a key from each element.</param>
		/// <param name="elementSelector">A transform function to produce a result element value from each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
		/// <typeparam name="TElement">The type of the value returned by <paramref name="elementSelector" />.</typeparam>
		/// <returns>A <see cref="T:System.Collections.Generic.Dictionary`2" /> that contains values of type <paramref name="TElement" /> selected from the input sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="keySelector" /> or <paramref name="elementSelector" /> is <see langword="null" />.-or-
		///         <paramref name="keySelector" /> produces a key that is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="keySelector" /> produces duplicate keys for two elements.</exception>
		// Token: 0x06000579 RID: 1401 RVA: 0x0000FC4D File Offset: 0x0000DE4D
		public static Dictionary<TKey, TElement> ToDictionary<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector)
		{
			return source.ToDictionary(keySelector, elementSelector, null);
		}

		/// <summary>Creates a <see cref="T:System.Collections.Generic.Dictionary`2" /> from an <see cref="T:System.Collections.Generic.IEnumerable`1" /> according to a specified key selector function, a comparer, and an element selector function.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to create a <see cref="T:System.Collections.Generic.Dictionary`2" /> from.</param>
		/// <param name="keySelector">A function to extract a key from each element.</param>
		/// <param name="elementSelector">A transform function to produce a result element value from each element.</param>
		/// <param name="comparer">An <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> to compare keys.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
		/// <typeparam name="TElement">The type of the value returned by <paramref name="elementSelector" />.</typeparam>
		/// <returns>A <see cref="T:System.Collections.Generic.Dictionary`2" /> that contains values of type <paramref name="TElement" /> selected from the input sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="keySelector" /> or <paramref name="elementSelector" /> is <see langword="null" />.-or-
		///         <paramref name="keySelector" /> produces a key that is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="keySelector" /> produces duplicate keys for two elements.</exception>
		// Token: 0x0600057A RID: 1402 RVA: 0x0000FC58 File Offset: 0x0000DE58
		public static Dictionary<TKey, TElement> ToDictionary<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, IEqualityComparer<TKey> comparer)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (keySelector == null)
			{
				throw Error.ArgumentNull("keySelector");
			}
			if (elementSelector == null)
			{
				throw Error.ArgumentNull("elementSelector");
			}
			Dictionary<TKey, TElement> dictionary = new Dictionary<TKey, TElement>(comparer);
			foreach (TSource arg in source)
			{
				dictionary.Add(keySelector(arg), elementSelector(arg));
			}
			return dictionary;
		}

		/// <summary>Creates a <see cref="T:System.Linq.Lookup`2" /> from an <see cref="T:System.Collections.Generic.IEnumerable`1" /> according to a specified key selector function.</summary>
		/// <param name="source">The <see cref="T:System.Collections.Generic.IEnumerable`1" /> to create a <see cref="T:System.Linq.Lookup`2" /> from.</param>
		/// <param name="keySelector">A function to extract a key from each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
		/// <returns>A <see cref="T:System.Linq.Lookup`2" /> that contains keys and values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="keySelector" /> is <see langword="null" />.</exception>
		// Token: 0x0600057B RID: 1403 RVA: 0x0000FCE0 File Offset: 0x0000DEE0
		public static ILookup<TKey, TSource> ToLookup<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			return Lookup<TKey, TSource>.Create<TSource>(source, keySelector, IdentityFunction<TSource>.Instance, null);
		}

		/// <summary>Creates a <see cref="T:System.Linq.Lookup`2" /> from an <see cref="T:System.Collections.Generic.IEnumerable`1" /> according to a specified key selector function and key comparer.</summary>
		/// <param name="source">The <see cref="T:System.Collections.Generic.IEnumerable`1" /> to create a <see cref="T:System.Linq.Lookup`2" /> from.</param>
		/// <param name="keySelector">A function to extract a key from each element.</param>
		/// <param name="comparer">An <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> to compare keys.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
		/// <returns>A <see cref="T:System.Linq.Lookup`2" /> that contains keys and values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="keySelector" /> is <see langword="null" />.</exception>
		// Token: 0x0600057C RID: 1404 RVA: 0x0000FCEF File Offset: 0x0000DEEF
		public static ILookup<TKey, TSource> ToLookup<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer)
		{
			return Lookup<TKey, TSource>.Create<TSource>(source, keySelector, IdentityFunction<TSource>.Instance, comparer);
		}

		/// <summary>Creates a <see cref="T:System.Linq.Lookup`2" /> from an <see cref="T:System.Collections.Generic.IEnumerable`1" /> according to specified key selector and element selector functions.</summary>
		/// <param name="source">The <see cref="T:System.Collections.Generic.IEnumerable`1" /> to create a <see cref="T:System.Linq.Lookup`2" /> from.</param>
		/// <param name="keySelector">A function to extract a key from each element.</param>
		/// <param name="elementSelector">A transform function to produce a result element value from each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
		/// <typeparam name="TElement">The type of the value returned by <paramref name="elementSelector" />.</typeparam>
		/// <returns>A <see cref="T:System.Linq.Lookup`2" /> that contains values of type <paramref name="TElement" /> selected from the input sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="keySelector" /> or <paramref name="elementSelector" /> is <see langword="null" />.</exception>
		// Token: 0x0600057D RID: 1405 RVA: 0x0000FCFE File Offset: 0x0000DEFE
		public static ILookup<TKey, TElement> ToLookup<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector)
		{
			return Lookup<TKey, TElement>.Create<TSource>(source, keySelector, elementSelector, null);
		}

		/// <summary>Creates a <see cref="T:System.Linq.Lookup`2" /> from an <see cref="T:System.Collections.Generic.IEnumerable`1" /> according to a specified key selector function, a comparer and an element selector function.</summary>
		/// <param name="source">The <see cref="T:System.Collections.Generic.IEnumerable`1" /> to create a <see cref="T:System.Linq.Lookup`2" /> from.</param>
		/// <param name="keySelector">A function to extract a key from each element.</param>
		/// <param name="elementSelector">A transform function to produce a result element value from each element.</param>
		/// <param name="comparer">An <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> to compare keys.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TKey">The type of the key returned by <paramref name="keySelector" />.</typeparam>
		/// <typeparam name="TElement">The type of the value returned by <paramref name="elementSelector" />.</typeparam>
		/// <returns>A <see cref="T:System.Linq.Lookup`2" /> that contains values of type <paramref name="TElement" /> selected from the input sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="keySelector" /> or <paramref name="elementSelector" /> is <see langword="null" />.</exception>
		// Token: 0x0600057E RID: 1406 RVA: 0x0000FD09 File Offset: 0x0000DF09
		public static ILookup<TKey, TElement> ToLookup<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, IEqualityComparer<TKey> comparer)
		{
			return Lookup<TKey, TElement>.Create<TSource>(source, keySelector, elementSelector, comparer);
		}

		/// <summary>Returns the elements of the specified sequence or the type parameter's default value in a singleton collection if the sequence is empty.</summary>
		/// <param name="source">The sequence to return a default value for if it is empty.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> object that contains the default value for the <paramref name="TSource" /> type if <paramref name="source" /> is empty; otherwise, <paramref name="source" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x0600057F RID: 1407 RVA: 0x0000FD14 File Offset: 0x0000DF14
		public static IEnumerable<TSource> DefaultIfEmpty<TSource>(this IEnumerable<TSource> source)
		{
			return source.DefaultIfEmpty(default(TSource));
		}

		/// <summary>Returns the elements of the specified sequence or the specified value in a singleton collection if the sequence is empty.</summary>
		/// <param name="source">The sequence to return the specified value for if it is empty.</param>
		/// <param name="defaultValue">The value to return if the sequence is empty.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains <paramref name="defaultValue" /> if <paramref name="source" /> is empty; otherwise, <paramref name="source" />.</returns>
		// Token: 0x06000580 RID: 1408 RVA: 0x0000FD30 File Offset: 0x0000DF30
		public static IEnumerable<TSource> DefaultIfEmpty<TSource>(this IEnumerable<TSource> source, TSource defaultValue)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			return Enumerable.DefaultIfEmptyIterator<TSource>(source, defaultValue);
		}

		// Token: 0x06000581 RID: 1409 RVA: 0x0000FD47 File Offset: 0x0000DF47
		private static IEnumerable<TSource> DefaultIfEmptyIterator<TSource>(IEnumerable<TSource> source, TSource defaultValue)
		{
			using (IEnumerator<TSource> e = source.GetEnumerator())
			{
				if (e.MoveNext())
				{
					do
					{
						yield return e.Current;
					}
					while (e.MoveNext());
				}
				else
				{
					yield return defaultValue;
				}
			}
			IEnumerator<TSource> e = null;
			yield break;
			yield break;
		}

		/// <summary>Filters the elements of an <see cref="T:System.Collections.IEnumerable" /> based on a specified type.</summary>
		/// <param name="source">The <see cref="T:System.Collections.IEnumerable" /> whose elements to filter.</param>
		/// <typeparam name="TResult">The type to filter the elements of the sequence on.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains elements from the input sequence of type <paramref name="TResult" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x06000582 RID: 1410 RVA: 0x0000FD5E File Offset: 0x0000DF5E
		public static IEnumerable<TResult> OfType<TResult>(this IEnumerable source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			return Enumerable.OfTypeIterator<TResult>(source);
		}

		// Token: 0x06000583 RID: 1411 RVA: 0x0000FD74 File Offset: 0x0000DF74
		private static IEnumerable<TResult> OfTypeIterator<TResult>(IEnumerable source)
		{
			foreach (object obj in source)
			{
				if (obj is TResult)
				{
					yield return (TResult)((object)obj);
				}
			}
			IEnumerator enumerator = null;
			yield break;
			yield break;
		}

		/// <summary>Casts the elements of an <see cref="T:System.Collections.IEnumerable" /> to the specified type.</summary>
		/// <param name="source">The <see cref="T:System.Collections.IEnumerable" /> that contains the elements to be cast to type <paramref name="TResult" />.</param>
		/// <typeparam name="TResult">The type to cast the elements of <paramref name="source" /> to.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains each element of the source sequence cast to the specified type.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidCastException">An element in the sequence cannot be cast to type <paramref name="TResult" />.</exception>
		// Token: 0x06000584 RID: 1412 RVA: 0x0000FD84 File Offset: 0x0000DF84
		public static IEnumerable<TResult> Cast<TResult>(this IEnumerable source)
		{
			IEnumerable<TResult> enumerable = source as IEnumerable<TResult>;
			if (enumerable != null)
			{
				return enumerable;
			}
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			return Enumerable.CastIterator<TResult>(source);
		}

		// Token: 0x06000585 RID: 1413 RVA: 0x0000FDB1 File Offset: 0x0000DFB1
		private static IEnumerable<TResult> CastIterator<TResult>(IEnumerable source)
		{
			foreach (object obj in source)
			{
				yield return (TResult)((object)obj);
			}
			IEnumerator enumerator = null;
			yield break;
			yield break;
		}

		/// <summary>Returns the first element of a sequence.</summary>
		/// <param name="source">The <see cref="T:System.Collections.Generic.IEnumerable`1" /> to return the first element of.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The first element in the specified sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The source sequence is empty.</exception>
		// Token: 0x06000586 RID: 1414 RVA: 0x0000FDC4 File Offset: 0x0000DFC4
		public static TSource First<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			IList<TSource> list = source as IList<TSource>;
			if (list != null)
			{
				if (list.Count > 0)
				{
					return list[0];
				}
			}
			else
			{
				using (IEnumerator<TSource> enumerator = source.GetEnumerator())
				{
					if (enumerator.MoveNext())
					{
						return enumerator.Current;
					}
				}
			}
			throw Error.NoElements();
		}

		/// <summary>Returns the first element in a sequence that satisfies a specified condition.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to return an element from.</param>
		/// <param name="predicate">A function to test each element for a condition.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The first element in the sequence that passes the test in the specified predicate function.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="predicate" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">No element satisfies the condition in <paramref name="predicate" />.-or-The source sequence is empty.</exception>
		// Token: 0x06000587 RID: 1415 RVA: 0x0000FE38 File Offset: 0x0000E038
		public static TSource First<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (predicate == null)
			{
				throw Error.ArgumentNull("predicate");
			}
			foreach (TSource tsource in source)
			{
				if (predicate(tsource))
				{
					return tsource;
				}
			}
			throw Error.NoMatch();
		}

		/// <summary>Returns the first element of a sequence, or a default value if the sequence contains no elements.</summary>
		/// <param name="source">The <see cref="T:System.Collections.Generic.IEnumerable`1" /> to return the first element of.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>
		///     <see langword="default" />(<paramref name="TSource" />) if <paramref name="source" /> is empty; otherwise, the first element in <paramref name="source" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x06000588 RID: 1416 RVA: 0x0000FEAC File Offset: 0x0000E0AC
		public static TSource FirstOrDefault<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			IList<TSource> list = source as IList<TSource>;
			if (list != null)
			{
				if (list.Count > 0)
				{
					return list[0];
				}
			}
			else
			{
				using (IEnumerator<TSource> enumerator = source.GetEnumerator())
				{
					if (enumerator.MoveNext())
					{
						return enumerator.Current;
					}
				}
			}
			return default(TSource);
		}

		/// <summary>Returns the first element of the sequence that satisfies a condition or a default value if no such element is found.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to return an element from.</param>
		/// <param name="predicate">A function to test each element for a condition.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>
		///     <see langword="default" />(<paramref name="TSource" />) if <paramref name="source" /> is empty or if no element passes the test specified by <paramref name="predicate" />; otherwise, the first element in <paramref name="source" /> that passes the test specified by <paramref name="predicate" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="predicate" /> is <see langword="null" />.</exception>
		// Token: 0x06000589 RID: 1417 RVA: 0x0000FF24 File Offset: 0x0000E124
		public static TSource FirstOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (predicate == null)
			{
				throw Error.ArgumentNull("predicate");
			}
			foreach (TSource tsource in source)
			{
				if (predicate(tsource))
				{
					return tsource;
				}
			}
			return default(TSource);
		}

		/// <summary>Returns the last element of a sequence.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to return the last element of.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The value at the last position in the source sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The source sequence is empty.</exception>
		// Token: 0x0600058A RID: 1418 RVA: 0x0000FF9C File Offset: 0x0000E19C
		public static TSource Last<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			IList<TSource> list = source as IList<TSource>;
			if (list != null)
			{
				int count = list.Count;
				if (count > 0)
				{
					return list[count - 1];
				}
			}
			else
			{
				using (IEnumerator<TSource> enumerator = source.GetEnumerator())
				{
					if (enumerator.MoveNext())
					{
						TSource result;
						do
						{
							result = enumerator.Current;
						}
						while (enumerator.MoveNext());
						return result;
					}
				}
			}
			throw Error.NoElements();
		}

		/// <summary>Returns the last element of a sequence that satisfies a specified condition.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to return an element from.</param>
		/// <param name="predicate">A function to test each element for a condition.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The last element in the sequence that passes the test in the specified predicate function.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="predicate" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">No element satisfies the condition in <paramref name="predicate" />.-or-The source sequence is empty.</exception>
		// Token: 0x0600058B RID: 1419 RVA: 0x00010020 File Offset: 0x0000E220
		public static TSource Last<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (predicate == null)
			{
				throw Error.ArgumentNull("predicate");
			}
			TSource result = default(TSource);
			bool flag = false;
			foreach (TSource tsource in source)
			{
				if (predicate(tsource))
				{
					result = tsource;
					flag = true;
				}
			}
			if (flag)
			{
				return result;
			}
			throw Error.NoMatch();
		}

		/// <summary>Returns the last element of a sequence, or a default value if the sequence contains no elements.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to return the last element of.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>
		///     <see langword="default" />(<paramref name="TSource" />) if the source sequence is empty; otherwise, the last element in the <see cref="T:System.Collections.Generic.IEnumerable`1" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x0600058C RID: 1420 RVA: 0x000100A0 File Offset: 0x0000E2A0
		public static TSource LastOrDefault<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			IList<TSource> list = source as IList<TSource>;
			if (list != null)
			{
				int count = list.Count;
				if (count > 0)
				{
					return list[count - 1];
				}
			}
			else
			{
				using (IEnumerator<TSource> enumerator = source.GetEnumerator())
				{
					if (enumerator.MoveNext())
					{
						TSource result;
						do
						{
							result = enumerator.Current;
						}
						while (enumerator.MoveNext());
						return result;
					}
				}
			}
			return default(TSource);
		}

		/// <summary>Returns the last element of a sequence that satisfies a condition or a default value if no such element is found.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to return an element from.</param>
		/// <param name="predicate">A function to test each element for a condition.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>
		///     <see langword="default" />(<paramref name="TSource" />) if the sequence is empty or if no elements pass the test in the predicate function; otherwise, the last element that passes the test in the predicate function.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="predicate" /> is <see langword="null" />.</exception>
		// Token: 0x0600058D RID: 1421 RVA: 0x00010128 File Offset: 0x0000E328
		public static TSource LastOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (predicate == null)
			{
				throw Error.ArgumentNull("predicate");
			}
			TSource result = default(TSource);
			foreach (TSource tsource in source)
			{
				if (predicate(tsource))
				{
					result = tsource;
				}
			}
			return result;
		}

		/// <summary>Returns the only element of a sequence, and throws an exception if there is not exactly one element in the sequence.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to return the single element of.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The single element of the input sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The input sequence contains more than one element.-or-The input sequence is empty.</exception>
		// Token: 0x0600058E RID: 1422 RVA: 0x0001019C File Offset: 0x0000E39C
		public static TSource Single<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			IList<TSource> list = source as IList<TSource>;
			if (list != null)
			{
				int count = list.Count;
				if (count == 0)
				{
					throw Error.NoElements();
				}
				if (count == 1)
				{
					return list[0];
				}
			}
			else
			{
				using (IEnumerator<TSource> enumerator = source.GetEnumerator())
				{
					if (!enumerator.MoveNext())
					{
						throw Error.NoElements();
					}
					TSource result = enumerator.Current;
					if (!enumerator.MoveNext())
					{
						return result;
					}
				}
			}
			throw Error.MoreThanOneElement();
		}

		/// <summary>Returns the only element of a sequence that satisfies a specified condition, and throws an exception if more than one such element exists.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to return a single element from.</param>
		/// <param name="predicate">A function to test an element for a condition.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The single element of the input sequence that satisfies a condition.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="predicate" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">No element satisfies the condition in <paramref name="predicate" />.-or-More than one element satisfies the condition in <paramref name="predicate" />.-or-The source sequence is empty.</exception>
		// Token: 0x0600058F RID: 1423 RVA: 0x0001022C File Offset: 0x0000E42C
		public static TSource Single<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (predicate == null)
			{
				throw Error.ArgumentNull("predicate");
			}
			TSource result = default(TSource);
			long num = 0L;
			checked
			{
				foreach (TSource tsource in source)
				{
					if (predicate(tsource))
					{
						result = tsource;
						num += 1L;
					}
				}
				if (num == 0L)
				{
					throw Error.NoMatch();
				}
				if (num != 1L)
				{
					throw Error.MoreThanOneMatch();
				}
				return result;
			}
		}

		/// <summary>Returns the only element of a sequence, or a default value if the sequence is empty; this method throws an exception if there is more than one element in the sequence.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to return the single element of.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The single element of the input sequence, or <see langword="default" />(<paramref name="TSource" />) if the sequence contains no elements.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The input sequence contains more than one element.</exception>
		// Token: 0x06000590 RID: 1424 RVA: 0x000102BC File Offset: 0x0000E4BC
		public static TSource SingleOrDefault<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			IList<TSource> list = source as IList<TSource>;
			if (list != null)
			{
				int count = list.Count;
				if (count == 0)
				{
					TSource result = default(TSource);
					return result;
				}
				if (count == 1)
				{
					return list[0];
				}
			}
			else
			{
				using (IEnumerator<TSource> enumerator = source.GetEnumerator())
				{
					if (!enumerator.MoveNext())
					{
						TSource result = default(TSource);
						return result;
					}
					TSource result2 = enumerator.Current;
					if (!enumerator.MoveNext())
					{
						return result2;
					}
				}
			}
			throw Error.MoreThanOneElement();
		}

		/// <summary>Returns the only element of a sequence that satisfies a specified condition or a default value if no such element exists; this method throws an exception if more than one element satisfies the condition.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to return a single element from.</param>
		/// <param name="predicate">A function to test an element for a condition.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The single element of the input sequence that satisfies the condition, or <see langword="default" />(<paramref name="TSource" />) if no such element is found.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="predicate" /> is <see langword="null" />.</exception>
		// Token: 0x06000591 RID: 1425 RVA: 0x00010358 File Offset: 0x0000E558
		public static TSource SingleOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (predicate == null)
			{
				throw Error.ArgumentNull("predicate");
			}
			TSource result = default(TSource);
			long num = 0L;
			checked
			{
				foreach (TSource tsource in source)
				{
					if (predicate(tsource))
					{
						result = tsource;
						num += 1L;
					}
				}
				if (num == 0L)
				{
					return default(TSource);
				}
				if (num != 1L)
				{
					throw Error.MoreThanOneMatch();
				}
				return result;
			}
		}

		/// <summary>Returns the element at a specified index in a sequence.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to return an element from.</param>
		/// <param name="index">The zero-based index of the element to retrieve.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The element at the specified position in the source sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than 0 or greater than or equal to the number of elements in <paramref name="source" />.</exception>
		// Token: 0x06000592 RID: 1426 RVA: 0x000103EC File Offset: 0x0000E5EC
		public static TSource ElementAt<TSource>(this IEnumerable<TSource> source, int index)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			IList<TSource> list = source as IList<TSource>;
			if (list != null)
			{
				return list[index];
			}
			if (index < 0)
			{
				throw Error.ArgumentOutOfRange("index");
			}
			using (IEnumerator<TSource> enumerator = source.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					if (index == 0)
					{
						return enumerator.Current;
					}
					index--;
				}
				throw Error.ArgumentOutOfRange("index");
			}
			TSource result;
			return result;
		}

		/// <summary>Returns the element at a specified index in a sequence or a default value if the index is out of range.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to return an element from.</param>
		/// <param name="index">The zero-based index of the element to retrieve.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>
		///     <see langword="default" />(<paramref name="TSource" />) if the index is outside the bounds of the source sequence; otherwise, the element at the specified position in the source sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x06000593 RID: 1427 RVA: 0x00010470 File Offset: 0x0000E670
		public static TSource ElementAtOrDefault<TSource>(this IEnumerable<TSource> source, int index)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (index >= 0)
			{
				IList<TSource> list = source as IList<TSource>;
				if (list != null)
				{
					if (index < list.Count)
					{
						return list[index];
					}
				}
				else
				{
					using (IEnumerator<TSource> enumerator = source.GetEnumerator())
					{
						while (enumerator.MoveNext())
						{
							if (index == 0)
							{
								return enumerator.Current;
							}
							index--;
						}
					}
				}
			}
			return default(TSource);
		}

		/// <summary>Generates a sequence of integral numbers within a specified range.</summary>
		/// <param name="start">The value of the first integer in the sequence.</param>
		/// <param name="count">The number of sequential integers to generate.</param>
		/// <returns>An IEnumerable&lt;Int32&gt; in C# or IEnumerable(Of Int32) in Visual Basic that contains a range of sequential integral numbers.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is less than 0.-or-
		///         <paramref name="start" /> + <paramref name="count" /> -1 is larger than <see cref="F:System.Int32.MaxValue" />.</exception>
		// Token: 0x06000594 RID: 1428 RVA: 0x000104F4 File Offset: 0x0000E6F4
		public static IEnumerable<int> Range(int start, int count)
		{
			long num = (long)start + (long)count - 1L;
			if (count < 0 || num > 2147483647L)
			{
				throw Error.ArgumentOutOfRange("count");
			}
			return Enumerable.RangeIterator(start, count);
		}

		// Token: 0x06000595 RID: 1429 RVA: 0x00010529 File Offset: 0x0000E729
		private static IEnumerable<int> RangeIterator(int start, int count)
		{
			int num;
			for (int i = 0; i < count; i = num + 1)
			{
				yield return start + i;
				num = i;
			}
			yield break;
		}

		/// <summary>Generates a sequence that contains one repeated value.</summary>
		/// <param name="element">The value to be repeated.</param>
		/// <param name="count">The number of times to repeat the value in the generated sequence.</param>
		/// <typeparam name="TResult">The type of the value to be repeated in the result sequence.</typeparam>
		/// <returns>An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains a repeated value.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> is less than 0.</exception>
		// Token: 0x06000596 RID: 1430 RVA: 0x00010540 File Offset: 0x0000E740
		public static IEnumerable<TResult> Repeat<TResult>(TResult element, int count)
		{
			if (count < 0)
			{
				throw Error.ArgumentOutOfRange("count");
			}
			return Enumerable.RepeatIterator<TResult>(element, count);
		}

		// Token: 0x06000597 RID: 1431 RVA: 0x00010558 File Offset: 0x0000E758
		private static IEnumerable<TResult> RepeatIterator<TResult>(TResult element, int count)
		{
			int num;
			for (int i = 0; i < count; i = num + 1)
			{
				yield return element;
				num = i;
			}
			yield break;
		}

		/// <summary>Returns an empty <see cref="T:System.Collections.Generic.IEnumerable`1" /> that has the specified type argument.</summary>
		/// <typeparam name="TResult">The type to assign to the type parameter of the returned generic <see cref="T:System.Collections.Generic.IEnumerable`1" />.</typeparam>
		/// <returns>An empty <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose type argument is <paramref name="TResult" />.</returns>
		// Token: 0x06000598 RID: 1432 RVA: 0x0001056F File Offset: 0x0000E76F
		public static IEnumerable<TResult> Empty<TResult>()
		{
			return EmptyEnumerable<TResult>.Instance;
		}

		/// <summary>Determines whether a sequence contains any elements.</summary>
		/// <param name="source">The <see cref="T:System.Collections.Generic.IEnumerable`1" /> to check for emptiness.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>
		///     <see langword="true" /> if the source sequence contains any elements; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x06000599 RID: 1433 RVA: 0x00010578 File Offset: 0x0000E778
		public static bool Any<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			using (IEnumerator<TSource> enumerator = source.GetEnumerator())
			{
				if (enumerator.MoveNext())
				{
					return true;
				}
			}
			return false;
		}

		/// <summary>Determines whether any element of a sequence satisfies a condition.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> whose elements to apply the predicate to.</param>
		/// <param name="predicate">A function to test each element for a condition.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>
		///     <see langword="true" /> if any elements in the source sequence pass the test in the specified predicate; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="predicate" /> is <see langword="null" />.</exception>
		// Token: 0x0600059A RID: 1434 RVA: 0x000105C8 File Offset: 0x0000E7C8
		public static bool Any<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (predicate == null)
			{
				throw Error.ArgumentNull("predicate");
			}
			foreach (TSource arg in source)
			{
				if (predicate(arg))
				{
					return true;
				}
			}
			return false;
		}

		/// <summary>Determines whether all elements of a sequence satisfy a condition.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains the elements to apply the predicate to.</param>
		/// <param name="predicate">A function to test each element for a condition.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>
		///     <see langword="true" /> if every element of the source sequence passes the test in the specified predicate, or if the sequence is empty; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="predicate" /> is <see langword="null" />.</exception>
		// Token: 0x0600059B RID: 1435 RVA: 0x00010638 File Offset: 0x0000E838
		public static bool All<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (predicate == null)
			{
				throw Error.ArgumentNull("predicate");
			}
			foreach (TSource arg in source)
			{
				if (!predicate(arg))
				{
					return false;
				}
			}
			return true;
		}

		/// <summary>Returns the number of elements in a sequence.</summary>
		/// <param name="source">A sequence that contains elements to be counted.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The number of elements in the input sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.OverflowException">The number of elements in <paramref name="source" /> is larger than <see cref="F:System.Int32.MaxValue" />.</exception>
		// Token: 0x0600059C RID: 1436 RVA: 0x000106A8 File Offset: 0x0000E8A8
		public static int Count<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			ICollection<TSource> collection = source as ICollection<TSource>;
			if (collection != null)
			{
				return collection.Count;
			}
			ICollection collection2 = source as ICollection;
			if (collection2 != null)
			{
				return collection2.Count;
			}
			int num = 0;
			checked
			{
				using (IEnumerator<TSource> enumerator = source.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						num++;
					}
				}
				return num;
			}
		}

		/// <summary>Returns a number that represents how many elements in the specified sequence satisfy a condition.</summary>
		/// <param name="source">A sequence that contains elements to be tested and counted.</param>
		/// <param name="predicate">A function to test each element for a condition.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>A number that represents how many elements in the sequence satisfy the condition in the predicate function.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="predicate" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.OverflowException">The number of elements in <paramref name="source" /> is larger than <see cref="F:System.Int32.MaxValue" />.</exception>
		// Token: 0x0600059D RID: 1437 RVA: 0x0001071C File Offset: 0x0000E91C
		public static int Count<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (predicate == null)
			{
				throw Error.ArgumentNull("predicate");
			}
			int num = 0;
			checked
			{
				foreach (TSource arg in source)
				{
					if (predicate(arg))
					{
						num++;
					}
				}
				return num;
			}
		}

		/// <summary>Returns an <see cref="T:System.Int64" /> that represents the total number of elements in a sequence.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains the elements to be counted.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The number of elements in the source sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.OverflowException">The number of elements exceeds <see cref="F:System.Int64.MaxValue" />.</exception>
		// Token: 0x0600059E RID: 1438 RVA: 0x0001078C File Offset: 0x0000E98C
		public static long LongCount<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			long num = 0L;
			checked
			{
				using (IEnumerator<TSource> enumerator = source.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						num += 1L;
					}
				}
				return num;
			}
		}

		/// <summary>Returns an <see cref="T:System.Int64" /> that represents how many elements in a sequence satisfy a condition.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> that contains the elements to be counted.</param>
		/// <param name="predicate">A function to test each element for a condition.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>A number that represents how many elements in the sequence satisfy the condition in the predicate function.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="predicate" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.OverflowException">The number of matching elements exceeds <see cref="F:System.Int64.MaxValue" />.</exception>
		// Token: 0x0600059F RID: 1439 RVA: 0x000107E0 File Offset: 0x0000E9E0
		public static long LongCount<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (predicate == null)
			{
				throw Error.ArgumentNull("predicate");
			}
			long num = 0L;
			checked
			{
				foreach (TSource arg in source)
				{
					if (predicate(arg))
					{
						num += 1L;
					}
				}
				return num;
			}
		}

		/// <summary>Determines whether a sequence contains a specified element by using the default equality comparer.</summary>
		/// <param name="source">A sequence in which to locate a value.</param>
		/// <param name="value">The value to locate in the sequence.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>
		///     <see langword="true" /> if the source sequence contains an element that has the specified value; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x060005A0 RID: 1440 RVA: 0x00010850 File Offset: 0x0000EA50
		public static bool Contains<TSource>(this IEnumerable<TSource> source, TSource value)
		{
			ICollection<TSource> collection = source as ICollection<TSource>;
			if (collection != null)
			{
				return collection.Contains(value);
			}
			return source.Contains(value, null);
		}

		/// <summary>Determines whether a sequence contains a specified element by using a specified <see cref="T:System.Collections.Generic.IEqualityComparer`1" />.</summary>
		/// <param name="source">A sequence in which to locate a value.</param>
		/// <param name="value">The value to locate in the sequence.</param>
		/// <param name="comparer">An equality comparer to compare values.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>
		///     <see langword="true" /> if the source sequence contains an element that has the specified value; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x060005A1 RID: 1441 RVA: 0x00010878 File Offset: 0x0000EA78
		public static bool Contains<TSource>(this IEnumerable<TSource> source, TSource value, IEqualityComparer<TSource> comparer)
		{
			if (comparer == null)
			{
				comparer = EqualityComparer<TSource>.Default;
			}
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			foreach (TSource x in source)
			{
				if (comparer.Equals(x, value))
				{
					return true;
				}
			}
			return false;
		}

		/// <summary>Applies an accumulator function over a sequence.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to aggregate over.</param>
		/// <param name="func">An accumulator function to be invoked on each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The final accumulator value.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="func" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005A2 RID: 1442 RVA: 0x000108E4 File Offset: 0x0000EAE4
		public static TSource Aggregate<TSource>(this IEnumerable<TSource> source, Func<TSource, TSource, TSource> func)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (func == null)
			{
				throw Error.ArgumentNull("func");
			}
			TSource result;
			using (IEnumerator<TSource> enumerator = source.GetEnumerator())
			{
				if (!enumerator.MoveNext())
				{
					throw Error.NoElements();
				}
				TSource tsource = enumerator.Current;
				while (enumerator.MoveNext())
				{
					TSource arg = enumerator.Current;
					tsource = func(tsource, arg);
				}
				result = tsource;
			}
			return result;
		}

		/// <summary>Applies an accumulator function over a sequence. The specified seed value is used as the initial accumulator value.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to aggregate over.</param>
		/// <param name="seed">The initial accumulator value.</param>
		/// <param name="func">An accumulator function to be invoked on each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TAccumulate">The type of the accumulator value.</typeparam>
		/// <returns>The final accumulator value.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="func" /> is <see langword="null" />.</exception>
		// Token: 0x060005A3 RID: 1443 RVA: 0x00010960 File Offset: 0x0000EB60
		public static TAccumulate Aggregate<TSource, TAccumulate>(this IEnumerable<TSource> source, TAccumulate seed, Func<TAccumulate, TSource, TAccumulate> func)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (func == null)
			{
				throw Error.ArgumentNull("func");
			}
			TAccumulate taccumulate = seed;
			foreach (TSource arg in source)
			{
				taccumulate = func(taccumulate, arg);
			}
			return taccumulate;
		}

		/// <summary>Applies an accumulator function over a sequence. The specified seed value is used as the initial accumulator value, and the specified function is used to select the result value.</summary>
		/// <param name="source">An <see cref="T:System.Collections.Generic.IEnumerable`1" /> to aggregate over.</param>
		/// <param name="seed">The initial accumulator value.</param>
		/// <param name="func">An accumulator function to be invoked on each element.</param>
		/// <param name="resultSelector">A function to transform the final accumulator value into the result value.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TAccumulate">The type of the accumulator value.</typeparam>
		/// <typeparam name="TResult">The type of the resulting value.</typeparam>
		/// <returns>The transformed final accumulator value.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="func" /> or <paramref name="resultSelector" /> is <see langword="null" />.</exception>
		// Token: 0x060005A4 RID: 1444 RVA: 0x000109CC File Offset: 0x0000EBCC
		public static TResult Aggregate<TSource, TAccumulate, TResult>(this IEnumerable<TSource> source, TAccumulate seed, Func<TAccumulate, TSource, TAccumulate> func, Func<TAccumulate, TResult> resultSelector)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (func == null)
			{
				throw Error.ArgumentNull("func");
			}
			if (resultSelector == null)
			{
				throw Error.ArgumentNull("resultSelector");
			}
			TAccumulate taccumulate = seed;
			foreach (TSource arg in source)
			{
				taccumulate = func(taccumulate, arg);
			}
			return resultSelector(taccumulate);
		}

		/// <summary>Computes the sum of a sequence of <see cref="T:System.Int32" /> values.</summary>
		/// <param name="source">A sequence of <see cref="T:System.Int32" /> values to calculate the sum of.</param>
		/// <returns>The sum of the values in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.OverflowException">The sum is larger than <see cref="F:System.Int32.MaxValue" />.</exception>
		// Token: 0x060005A5 RID: 1445 RVA: 0x00010A4C File Offset: 0x0000EC4C
		public static int Sum(this IEnumerable<int> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			int num = 0;
			checked
			{
				foreach (int num2 in source)
				{
					num += num2;
				}
				return num;
			}
		}

		/// <summary>Computes the sum of a sequence of nullable <see cref="T:System.Int32" /> values.</summary>
		/// <param name="source">A sequence of nullable <see cref="T:System.Int32" /> values to calculate the sum of.</param>
		/// <returns>The sum of the values in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.OverflowException">The sum is larger than <see cref="F:System.Int32.MaxValue" />.</exception>
		// Token: 0x060005A6 RID: 1446 RVA: 0x00010AA4 File Offset: 0x0000ECA4
		public static int? Sum(this IEnumerable<int?> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			int num = 0;
			checked
			{
				foreach (int? num2 in source)
				{
					if (num2 != null)
					{
						num += num2.GetValueOrDefault();
					}
				}
				return new int?(num);
			}
		}

		/// <summary>Computes the sum of a sequence of <see cref="T:System.Int64" /> values.</summary>
		/// <param name="source">A sequence of <see cref="T:System.Int64" /> values to calculate the sum of.</param>
		/// <returns>The sum of the values in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.OverflowException">The sum is larger than <see cref="F:System.Int64.MaxValue" />.</exception>
		// Token: 0x060005A7 RID: 1447 RVA: 0x00010B10 File Offset: 0x0000ED10
		public static long Sum(this IEnumerable<long> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			long num = 0L;
			checked
			{
				foreach (long num2 in source)
				{
					num += num2;
				}
				return num;
			}
		}

		/// <summary>Computes the sum of a sequence of nullable <see cref="T:System.Int64" /> values.</summary>
		/// <param name="source">A sequence of nullable <see cref="T:System.Int64" /> values to calculate the sum of.</param>
		/// <returns>The sum of the values in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.OverflowException">The sum is larger than <see cref="F:System.Int64.MaxValue" />.</exception>
		// Token: 0x060005A8 RID: 1448 RVA: 0x00010B68 File Offset: 0x0000ED68
		public static long? Sum(this IEnumerable<long?> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			long num = 0L;
			checked
			{
				foreach (long? num2 in source)
				{
					if (num2 != null)
					{
						num += num2.GetValueOrDefault();
					}
				}
				return new long?(num);
			}
		}

		/// <summary>Computes the sum of a sequence of <see cref="T:System.Single" /> values.</summary>
		/// <param name="source">A sequence of <see cref="T:System.Single" /> values to calculate the sum of.</param>
		/// <returns>The sum of the values in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x060005A9 RID: 1449 RVA: 0x00010BD4 File Offset: 0x0000EDD4
		public static float Sum(this IEnumerable<float> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			double num = 0.0;
			foreach (float num2 in source)
			{
				num += (double)num2;
			}
			return (float)num;
		}

		/// <summary>Computes the sum of a sequence of nullable <see cref="T:System.Single" /> values.</summary>
		/// <param name="source">A sequence of nullable <see cref="T:System.Single" /> values to calculate the sum of.</param>
		/// <returns>The sum of the values in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x060005AA RID: 1450 RVA: 0x00010C34 File Offset: 0x0000EE34
		public static float? Sum(this IEnumerable<float?> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			double num = 0.0;
			foreach (float? num2 in source)
			{
				if (num2 != null)
				{
					num += (double)num2.GetValueOrDefault();
				}
			}
			return new float?((float)num);
		}

		/// <summary>Computes the sum of a sequence of <see cref="T:System.Double" /> values.</summary>
		/// <param name="source">A sequence of <see cref="T:System.Double" /> values to calculate the sum of.</param>
		/// <returns>The sum of the values in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x060005AB RID: 1451 RVA: 0x00010CA8 File Offset: 0x0000EEA8
		public static double Sum(this IEnumerable<double> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			double num = 0.0;
			foreach (double num2 in source)
			{
				num += num2;
			}
			return num;
		}

		/// <summary>Computes the sum of a sequence of nullable <see cref="T:System.Double" /> values.</summary>
		/// <param name="source">A sequence of nullable <see cref="T:System.Double" /> values to calculate the sum of.</param>
		/// <returns>The sum of the values in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x060005AC RID: 1452 RVA: 0x00010D08 File Offset: 0x0000EF08
		public static double? Sum(this IEnumerable<double?> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			double num = 0.0;
			foreach (double? num2 in source)
			{
				if (num2 != null)
				{
					num += num2.GetValueOrDefault();
				}
			}
			return new double?(num);
		}

		/// <summary>Computes the sum of a sequence of <see cref="T:System.Decimal" /> values.</summary>
		/// <param name="source">A sequence of <see cref="T:System.Decimal" /> values to calculate the sum of.</param>
		/// <returns>The sum of the values in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.OverflowException">The sum is larger than <see cref="F:System.Decimal.MaxValue" />.</exception>
		// Token: 0x060005AD RID: 1453 RVA: 0x00010D7C File Offset: 0x0000EF7C
		public static decimal Sum(this IEnumerable<decimal> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			decimal num = 0m;
			foreach (decimal d in source)
			{
				num += d;
			}
			return num;
		}

		/// <summary>Computes the sum of a sequence of nullable <see cref="T:System.Decimal" /> values.</summary>
		/// <param name="source">A sequence of nullable <see cref="T:System.Decimal" /> values to calculate the sum of.</param>
		/// <returns>The sum of the values in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.OverflowException">The sum is larger than <see cref="F:System.Decimal.MaxValue" />.</exception>
		// Token: 0x060005AE RID: 1454 RVA: 0x00010DDC File Offset: 0x0000EFDC
		public static decimal? Sum(this IEnumerable<decimal?> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			decimal num = 0m;
			foreach (decimal? num2 in source)
			{
				if (num2 != null)
				{
					num += num2.GetValueOrDefault();
				}
			}
			return new decimal?(num);
		}

		/// <summary>Computes the sum of the sequence of <see cref="T:System.Int32" /> values that are obtained by invoking a transform function on each element of the input sequence.</summary>
		/// <param name="source">A sequence of values that are used to calculate a sum.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The sum of the projected values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.OverflowException">The sum is larger than <see cref="F:System.Int32.MaxValue" />.</exception>
		// Token: 0x060005AF RID: 1455 RVA: 0x00010E50 File Offset: 0x0000F050
		public static int Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector)
		{
			return source.Select(selector).Sum();
		}

		/// <summary>Computes the sum of the sequence of nullable <see cref="T:System.Int32" /> values that are obtained by invoking a transform function on each element of the input sequence.</summary>
		/// <param name="source">A sequence of values that are used to calculate a sum.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The sum of the projected values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.OverflowException">The sum is larger than <see cref="F:System.Int32.MaxValue" />.</exception>
		// Token: 0x060005B0 RID: 1456 RVA: 0x00010E5E File Offset: 0x0000F05E
		public static int? Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, int?> selector)
		{
			return source.Select(selector).Sum();
		}

		/// <summary>Computes the sum of the sequence of <see cref="T:System.Int64" /> values that are obtained by invoking a transform function on each element of the input sequence.</summary>
		/// <param name="source">A sequence of values that are used to calculate a sum.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The sum of the projected values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.OverflowException">The sum is larger than <see cref="F:System.Int64.MaxValue" />.</exception>
		// Token: 0x060005B1 RID: 1457 RVA: 0x00010E6C File Offset: 0x0000F06C
		public static long Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, long> selector)
		{
			return source.Select(selector).Sum();
		}

		/// <summary>Computes the sum of the sequence of nullable <see cref="T:System.Int64" /> values that are obtained by invoking a transform function on each element of the input sequence.</summary>
		/// <param name="source">A sequence of values that are used to calculate a sum.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The sum of the projected values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.OverflowException">The sum is larger than <see cref="F:System.Int64.MaxValue" />.</exception>
		// Token: 0x060005B2 RID: 1458 RVA: 0x00010E7A File Offset: 0x0000F07A
		public static long? Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, long?> selector)
		{
			return source.Select(selector).Sum();
		}

		/// <summary>Computes the sum of the sequence of <see cref="T:System.Single" /> values that are obtained by invoking a transform function on each element of the input sequence.</summary>
		/// <param name="source">A sequence of values that are used to calculate a sum.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The sum of the projected values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		// Token: 0x060005B3 RID: 1459 RVA: 0x00010E88 File Offset: 0x0000F088
		public static float Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, float> selector)
		{
			return source.Select(selector).Sum();
		}

		/// <summary>Computes the sum of the sequence of nullable <see cref="T:System.Single" /> values that are obtained by invoking a transform function on each element of the input sequence.</summary>
		/// <param name="source">A sequence of values that are used to calculate a sum.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The sum of the projected values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		// Token: 0x060005B4 RID: 1460 RVA: 0x00010E96 File Offset: 0x0000F096
		public static float? Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, float?> selector)
		{
			return source.Select(selector).Sum();
		}

		/// <summary>Computes the sum of the sequence of <see cref="T:System.Double" /> values that are obtained by invoking a transform function on each element of the input sequence.</summary>
		/// <param name="source">A sequence of values that are used to calculate a sum.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The sum of the projected values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		// Token: 0x060005B5 RID: 1461 RVA: 0x00010EA4 File Offset: 0x0000F0A4
		public static double Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, double> selector)
		{
			return source.Select(selector).Sum();
		}

		/// <summary>Computes the sum of the sequence of nullable <see cref="T:System.Double" /> values that are obtained by invoking a transform function on each element of the input sequence.</summary>
		/// <param name="source">A sequence of values that are used to calculate a sum.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The sum of the projected values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		// Token: 0x060005B6 RID: 1462 RVA: 0x00010EB2 File Offset: 0x0000F0B2
		public static double? Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, double?> selector)
		{
			return source.Select(selector).Sum();
		}

		/// <summary>Computes the sum of the sequence of <see cref="T:System.Decimal" /> values that are obtained by invoking a transform function on each element of the input sequence.</summary>
		/// <param name="source">A sequence of values that are used to calculate a sum.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The sum of the projected values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.OverflowException">The sum is larger than <see cref="F:System.Decimal.MaxValue" />.</exception>
		// Token: 0x060005B7 RID: 1463 RVA: 0x00010EC0 File Offset: 0x0000F0C0
		public static decimal Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> selector)
		{
			return source.Select(selector).Sum();
		}

		/// <summary>Computes the sum of the sequence of nullable <see cref="T:System.Decimal" /> values that are obtained by invoking a transform function on each element of the input sequence.</summary>
		/// <param name="source">A sequence of values that are used to calculate a sum.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The sum of the projected values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.OverflowException">The sum is larger than <see cref="F:System.Decimal.MaxValue" />.</exception>
		// Token: 0x060005B8 RID: 1464 RVA: 0x00010ECE File Offset: 0x0000F0CE
		public static decimal? Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> selector)
		{
			return source.Select(selector).Sum();
		}

		/// <summary>Returns the minimum value in a sequence of <see cref="T:System.Int32" /> values.</summary>
		/// <param name="source">A sequence of <see cref="T:System.Int32" /> values to determine the minimum value of.</param>
		/// <returns>The minimum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005B9 RID: 1465 RVA: 0x00010EDC File Offset: 0x0000F0DC
		public static int Min(this IEnumerable<int> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			int num = 0;
			bool flag = false;
			foreach (int num2 in source)
			{
				if (flag)
				{
					if (num2 < num)
					{
						num = num2;
					}
				}
				else
				{
					num = num2;
					flag = true;
				}
			}
			if (flag)
			{
				return num;
			}
			throw Error.NoElements();
		}

		/// <summary>Returns the minimum value in a sequence of nullable <see cref="T:System.Int32" /> values.</summary>
		/// <param name="source">A sequence of nullable <see cref="T:System.Int32" /> values to determine the minimum value of.</param>
		/// <returns>A value of type Nullable&lt;Int32&gt; in C# or Nullable(Of Int32) in Visual Basic that corresponds to the minimum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x060005BA RID: 1466 RVA: 0x00010F48 File Offset: 0x0000F148
		public static int? Min(this IEnumerable<int?> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			int? num = null;
			foreach (int? num2 in source)
			{
				if (num == null || num2 < num)
				{
					num = num2;
				}
			}
			return num;
		}

		/// <summary>Returns the minimum value in a sequence of <see cref="T:System.Int64" /> values.</summary>
		/// <param name="source">A sequence of <see cref="T:System.Int64" /> values to determine the minimum value of.</param>
		/// <returns>The minimum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005BB RID: 1467 RVA: 0x00010FD4 File Offset: 0x0000F1D4
		public static long Min(this IEnumerable<long> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			long num = 0L;
			bool flag = false;
			foreach (long num2 in source)
			{
				if (flag)
				{
					if (num2 < num)
					{
						num = num2;
					}
				}
				else
				{
					num = num2;
					flag = true;
				}
			}
			if (flag)
			{
				return num;
			}
			throw Error.NoElements();
		}

		/// <summary>Returns the minimum value in a sequence of nullable <see cref="T:System.Int64" /> values.</summary>
		/// <param name="source">A sequence of nullable <see cref="T:System.Int64" /> values to determine the minimum value of.</param>
		/// <returns>A value of type Nullable&lt;Int64&gt; in C# or Nullable(Of Int64) in Visual Basic that corresponds to the minimum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x060005BC RID: 1468 RVA: 0x00011044 File Offset: 0x0000F244
		public static long? Min(this IEnumerable<long?> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			long? num = null;
			foreach (long? num2 in source)
			{
				if (num == null || num2 < num)
				{
					num = num2;
				}
			}
			return num;
		}

		/// <summary>Returns the minimum value in a sequence of <see cref="T:System.Single" /> values.</summary>
		/// <param name="source">A sequence of <see cref="T:System.Single" /> values to determine the minimum value of.</param>
		/// <returns>The minimum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005BD RID: 1469 RVA: 0x000110D0 File Offset: 0x0000F2D0
		public static float Min(this IEnumerable<float> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			float num = 0f;
			bool flag = false;
			foreach (float num2 in source)
			{
				if (flag)
				{
					if (num2 < num || float.IsNaN(num2))
					{
						num = num2;
					}
				}
				else
				{
					num = num2;
					flag = true;
				}
			}
			if (flag)
			{
				return num;
			}
			throw Error.NoElements();
		}

		/// <summary>Returns the minimum value in a sequence of nullable <see cref="T:System.Single" /> values.</summary>
		/// <param name="source">A sequence of nullable <see cref="T:System.Single" /> values to determine the minimum value of.</param>
		/// <returns>A value of type Nullable&lt;Single&gt; in C# or Nullable(Of Single) in Visual Basic that corresponds to the minimum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x060005BE RID: 1470 RVA: 0x00011148 File Offset: 0x0000F348
		public static float? Min(this IEnumerable<float?> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			float? num = null;
			foreach (float? num2 in source)
			{
				if (num2 != null && (num == null || num2 < num || float.IsNaN(num2.Value)))
				{
					num = num2;
				}
			}
			return num;
		}

		/// <summary>Returns the minimum value in a sequence of <see cref="T:System.Double" /> values.</summary>
		/// <param name="source">A sequence of <see cref="T:System.Double" /> values to determine the minimum value of.</param>
		/// <returns>The minimum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005BF RID: 1471 RVA: 0x000111EC File Offset: 0x0000F3EC
		public static double Min(this IEnumerable<double> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			double num = 0.0;
			bool flag = false;
			foreach (double num2 in source)
			{
				if (flag)
				{
					if (num2 < num || double.IsNaN(num2))
					{
						num = num2;
					}
				}
				else
				{
					num = num2;
					flag = true;
				}
			}
			if (flag)
			{
				return num;
			}
			throw Error.NoElements();
		}

		/// <summary>Returns the minimum value in a sequence of nullable <see cref="T:System.Double" /> values.</summary>
		/// <param name="source">A sequence of nullable <see cref="T:System.Double" /> values to determine the minimum value of.</param>
		/// <returns>A value of type Nullable&lt;Double&gt; in C# or Nullable(Of Double) in Visual Basic that corresponds to the minimum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x060005C0 RID: 1472 RVA: 0x00011268 File Offset: 0x0000F468
		public static double? Min(this IEnumerable<double?> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			double? num = null;
			foreach (double? num2 in source)
			{
				if (num2 != null && (num == null || num2 < num || double.IsNaN(num2.Value)))
				{
					num = num2;
				}
			}
			return num;
		}

		/// <summary>Returns the minimum value in a sequence of <see cref="T:System.Decimal" /> values.</summary>
		/// <param name="source">A sequence of <see cref="T:System.Decimal" /> values to determine the minimum value of.</param>
		/// <returns>The minimum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005C1 RID: 1473 RVA: 0x0001130C File Offset: 0x0000F50C
		public static decimal Min(this IEnumerable<decimal> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			decimal num = 0m;
			bool flag = false;
			foreach (decimal num2 in source)
			{
				if (flag)
				{
					if (num2 < num)
					{
						num = num2;
					}
				}
				else
				{
					num = num2;
					flag = true;
				}
			}
			if (flag)
			{
				return num;
			}
			throw Error.NoElements();
		}

		/// <summary>Returns the minimum value in a sequence of nullable <see cref="T:System.Decimal" /> values.</summary>
		/// <param name="source">A sequence of nullable <see cref="T:System.Decimal" /> values to determine the minimum value of.</param>
		/// <returns>A value of type Nullable&lt;Decimal&gt; in C# or Nullable(Of Decimal) in Visual Basic that corresponds to the minimum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x060005C2 RID: 1474 RVA: 0x00011384 File Offset: 0x0000F584
		public static decimal? Min(this IEnumerable<decimal?> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			decimal? num = null;
			foreach (decimal? num2 in source)
			{
				if (num == null || num2 < num)
				{
					num = num2;
				}
			}
			return num;
		}

		/// <summary>Returns the minimum value in a generic sequence.</summary>
		/// <param name="source">A sequence of values to determine the minimum value of.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The minimum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x060005C3 RID: 1475 RVA: 0x00011418 File Offset: 0x0000F618
		public static TSource Min<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			Comparer<TSource> @default = Comparer<TSource>.Default;
			TSource tsource = default(TSource);
			if (tsource == null)
			{
				foreach (TSource tsource2 in source)
				{
					if (tsource2 != null && (tsource == null || @default.Compare(tsource2, tsource) < 0))
					{
						tsource = tsource2;
					}
				}
				return tsource;
			}
			bool flag = false;
			foreach (TSource tsource3 in source)
			{
				if (flag)
				{
					if (@default.Compare(tsource3, tsource) < 0)
					{
						tsource = tsource3;
					}
				}
				else
				{
					tsource = tsource3;
					flag = true;
				}
			}
			if (flag)
			{
				return tsource;
			}
			throw Error.NoElements();
		}

		/// <summary>Invokes a transform function on each element of a sequence and returns the minimum <see cref="T:System.Int32" /> value.</summary>
		/// <param name="source">A sequence of values to determine the minimum value of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The minimum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005C4 RID: 1476 RVA: 0x000114F8 File Offset: 0x0000F6F8
		public static int Min<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector)
		{
			return source.Select(selector).Min();
		}

		/// <summary>Invokes a transform function on each element of a sequence and returns the minimum nullable <see cref="T:System.Int32" /> value.</summary>
		/// <param name="source">A sequence of values to determine the minimum value of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The value of type Nullable&lt;Int32&gt; in C# or Nullable(Of Int32) in Visual Basic that corresponds to the minimum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		// Token: 0x060005C5 RID: 1477 RVA: 0x00011506 File Offset: 0x0000F706
		public static int? Min<TSource>(this IEnumerable<TSource> source, Func<TSource, int?> selector)
		{
			return source.Select(selector).Min();
		}

		/// <summary>Invokes a transform function on each element of a sequence and returns the minimum <see cref="T:System.Int64" /> value.</summary>
		/// <param name="source">A sequence of values to determine the minimum value of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The minimum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005C6 RID: 1478 RVA: 0x00011514 File Offset: 0x0000F714
		public static long Min<TSource>(this IEnumerable<TSource> source, Func<TSource, long> selector)
		{
			return source.Select(selector).Min();
		}

		/// <summary>Invokes a transform function on each element of a sequence and returns the minimum nullable <see cref="T:System.Int64" /> value.</summary>
		/// <param name="source">A sequence of values to determine the minimum value of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The value of type Nullable&lt;Int64&gt; in C# or Nullable(Of Int64) in Visual Basic that corresponds to the minimum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		// Token: 0x060005C7 RID: 1479 RVA: 0x00011522 File Offset: 0x0000F722
		public static long? Min<TSource>(this IEnumerable<TSource> source, Func<TSource, long?> selector)
		{
			return source.Select(selector).Min();
		}

		/// <summary>Invokes a transform function on each element of a sequence and returns the minimum <see cref="T:System.Single" /> value.</summary>
		/// <param name="source">A sequence of values to determine the minimum value of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The minimum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005C8 RID: 1480 RVA: 0x00011530 File Offset: 0x0000F730
		public static float Min<TSource>(this IEnumerable<TSource> source, Func<TSource, float> selector)
		{
			return source.Select(selector).Min();
		}

		/// <summary>Invokes a transform function on each element of a sequence and returns the minimum nullable <see cref="T:System.Single" /> value.</summary>
		/// <param name="source">A sequence of values to determine the minimum value of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The value of type Nullable&lt;Single&gt; in C# or Nullable(Of Single) in Visual Basic that corresponds to the minimum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		// Token: 0x060005C9 RID: 1481 RVA: 0x0001153E File Offset: 0x0000F73E
		public static float? Min<TSource>(this IEnumerable<TSource> source, Func<TSource, float?> selector)
		{
			return source.Select(selector).Min();
		}

		/// <summary>Invokes a transform function on each element of a sequence and returns the minimum <see cref="T:System.Double" /> value.</summary>
		/// <param name="source">A sequence of values to determine the minimum value of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The minimum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005CA RID: 1482 RVA: 0x0001154C File Offset: 0x0000F74C
		public static double Min<TSource>(this IEnumerable<TSource> source, Func<TSource, double> selector)
		{
			return source.Select(selector).Min();
		}

		/// <summary>Invokes a transform function on each element of a sequence and returns the minimum nullable <see cref="T:System.Double" /> value.</summary>
		/// <param name="source">A sequence of values to determine the minimum value of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The value of type Nullable&lt;Double&gt; in C# or Nullable(Of Double) in Visual Basic that corresponds to the minimum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		// Token: 0x060005CB RID: 1483 RVA: 0x0001155A File Offset: 0x0000F75A
		public static double? Min<TSource>(this IEnumerable<TSource> source, Func<TSource, double?> selector)
		{
			return source.Select(selector).Min();
		}

		/// <summary>Invokes a transform function on each element of a sequence and returns the minimum <see cref="T:System.Decimal" /> value.</summary>
		/// <param name="source">A sequence of values to determine the minimum value of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The minimum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005CC RID: 1484 RVA: 0x00011568 File Offset: 0x0000F768
		public static decimal Min<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> selector)
		{
			return source.Select(selector).Min();
		}

		/// <summary>Invokes a transform function on each element of a sequence and returns the minimum nullable <see cref="T:System.Decimal" /> value.</summary>
		/// <param name="source">A sequence of values to determine the minimum value of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The value of type Nullable&lt;Decimal&gt; in C# or Nullable(Of Decimal) in Visual Basic that corresponds to the minimum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		// Token: 0x060005CD RID: 1485 RVA: 0x00011576 File Offset: 0x0000F776
		public static decimal? Min<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> selector)
		{
			return source.Select(selector).Min();
		}

		/// <summary>Invokes a transform function on each element of a generic sequence and returns the minimum resulting value.</summary>
		/// <param name="source">A sequence of values to determine the minimum value of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TResult">The type of the value returned by <paramref name="selector" />.</typeparam>
		/// <returns>The minimum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		// Token: 0x060005CE RID: 1486 RVA: 0x00011584 File Offset: 0x0000F784
		public static TResult Min<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector)
		{
			return source.Select(selector).Min<TResult>();
		}

		/// <summary>Returns the maximum value in a sequence of <see cref="T:System.Int32" /> values.</summary>
		/// <param name="source">A sequence of <see cref="T:System.Int32" /> values to determine the maximum value of.</param>
		/// <returns>The maximum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005CF RID: 1487 RVA: 0x00011594 File Offset: 0x0000F794
		public static int Max(this IEnumerable<int> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			int num = 0;
			bool flag = false;
			foreach (int num2 in source)
			{
				if (flag)
				{
					if (num2 > num)
					{
						num = num2;
					}
				}
				else
				{
					num = num2;
					flag = true;
				}
			}
			if (flag)
			{
				return num;
			}
			throw Error.NoElements();
		}

		/// <summary>Returns the maximum value in a sequence of nullable <see cref="T:System.Int32" /> values.</summary>
		/// <param name="source">A sequence of nullable <see cref="T:System.Int32" /> values to determine the maximum value of.</param>
		/// <returns>A value of type Nullable&lt;Int32&gt; in C# or Nullable(Of Int32) in Visual Basic that corresponds to the maximum value in the sequence. </returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x060005D0 RID: 1488 RVA: 0x00011600 File Offset: 0x0000F800
		public static int? Max(this IEnumerable<int?> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			int? num = null;
			foreach (int? num2 in source)
			{
				if (num == null || num2 > num)
				{
					num = num2;
				}
			}
			return num;
		}

		/// <summary>Returns the maximum value in a sequence of <see cref="T:System.Int64" /> values.</summary>
		/// <param name="source">A sequence of <see cref="T:System.Int64" /> values to determine the maximum value of.</param>
		/// <returns>The maximum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005D1 RID: 1489 RVA: 0x0001168C File Offset: 0x0000F88C
		public static long Max(this IEnumerable<long> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			long num = 0L;
			bool flag = false;
			foreach (long num2 in source)
			{
				if (flag)
				{
					if (num2 > num)
					{
						num = num2;
					}
				}
				else
				{
					num = num2;
					flag = true;
				}
			}
			if (flag)
			{
				return num;
			}
			throw Error.NoElements();
		}

		/// <summary>Returns the maximum value in a sequence of nullable <see cref="T:System.Int64" /> values.</summary>
		/// <param name="source">A sequence of nullable <see cref="T:System.Int64" /> values to determine the maximum value of.</param>
		/// <returns>A value of type Nullable&lt;Int64&gt; in C# or Nullable(Of Int64) in Visual Basic that corresponds to the maximum value in the sequence. </returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x060005D2 RID: 1490 RVA: 0x000116FC File Offset: 0x0000F8FC
		public static long? Max(this IEnumerable<long?> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			long? num = null;
			foreach (long? num2 in source)
			{
				if (num == null || num2 > num)
				{
					num = num2;
				}
			}
			return num;
		}

		/// <summary>Returns the maximum value in a sequence of <see cref="T:System.Double" /> values.</summary>
		/// <param name="source">A sequence of <see cref="T:System.Double" /> values to determine the maximum value of.</param>
		/// <returns>The maximum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005D3 RID: 1491 RVA: 0x00011788 File Offset: 0x0000F988
		public static double Max(this IEnumerable<double> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			double num = 0.0;
			bool flag = false;
			foreach (double num2 in source)
			{
				if (flag)
				{
					if (num2 > num || double.IsNaN(num))
					{
						num = num2;
					}
				}
				else
				{
					num = num2;
					flag = true;
				}
			}
			if (flag)
			{
				return num;
			}
			throw Error.NoElements();
		}

		/// <summary>Returns the maximum value in a sequence of nullable <see cref="T:System.Double" /> values.</summary>
		/// <param name="source">A sequence of nullable <see cref="T:System.Double" /> values to determine the maximum value of.</param>
		/// <returns>A value of type Nullable&lt;Double&gt; in C# or Nullable(Of Double) in Visual Basic that corresponds to the maximum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x060005D4 RID: 1492 RVA: 0x00011804 File Offset: 0x0000FA04
		public static double? Max(this IEnumerable<double?> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			double? num = null;
			foreach (double? num2 in source)
			{
				if (num2 != null && (num == null || num2 > num || double.IsNaN(num.Value)))
				{
					num = num2;
				}
			}
			return num;
		}

		/// <summary>Returns the maximum value in a sequence of <see cref="T:System.Single" /> values.</summary>
		/// <param name="source">A sequence of <see cref="T:System.Single" /> values to determine the maximum value of.</param>
		/// <returns>The maximum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005D5 RID: 1493 RVA: 0x000118A8 File Offset: 0x0000FAA8
		public static float Max(this IEnumerable<float> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			float num = 0f;
			bool flag = false;
			foreach (float num2 in source)
			{
				if (flag)
				{
					if (num2 > num || double.IsNaN((double)num))
					{
						num = num2;
					}
				}
				else
				{
					num = num2;
					flag = true;
				}
			}
			if (flag)
			{
				return num;
			}
			throw Error.NoElements();
		}

		/// <summary>Returns the maximum value in a sequence of nullable <see cref="T:System.Single" /> values.</summary>
		/// <param name="source">A sequence of nullable <see cref="T:System.Single" /> values to determine the maximum value of.</param>
		/// <returns>A value of type Nullable&lt;Single&gt; in C# or Nullable(Of Single) in Visual Basic that corresponds to the maximum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x060005D6 RID: 1494 RVA: 0x00011924 File Offset: 0x0000FB24
		public static float? Max(this IEnumerable<float?> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			float? num = null;
			foreach (float? num2 in source)
			{
				if (num2 != null && (num == null || num2 > num || float.IsNaN(num.Value)))
				{
					num = num2;
				}
			}
			return num;
		}

		/// <summary>Returns the maximum value in a sequence of <see cref="T:System.Decimal" /> values.</summary>
		/// <param name="source">A sequence of <see cref="T:System.Decimal" /> values to determine the maximum value of.</param>
		/// <returns>The maximum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005D7 RID: 1495 RVA: 0x000119C8 File Offset: 0x0000FBC8
		public static decimal Max(this IEnumerable<decimal> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			decimal num = 0m;
			bool flag = false;
			foreach (decimal num2 in source)
			{
				if (flag)
				{
					if (num2 > num)
					{
						num = num2;
					}
				}
				else
				{
					num = num2;
					flag = true;
				}
			}
			if (flag)
			{
				return num;
			}
			throw Error.NoElements();
		}

		/// <summary>Returns the maximum value in a sequence of nullable <see cref="T:System.Decimal" /> values.</summary>
		/// <param name="source">A sequence of nullable <see cref="T:System.Decimal" /> values to determine the maximum value of.</param>
		/// <returns>A value of type Nullable&lt;Decimal&gt; in C# or Nullable(Of Decimal) in Visual Basic that corresponds to the maximum value in the sequence. </returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x060005D8 RID: 1496 RVA: 0x00011A40 File Offset: 0x0000FC40
		public static decimal? Max(this IEnumerable<decimal?> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			decimal? num = null;
			foreach (decimal? num2 in source)
			{
				if (num == null || num2 > num)
				{
					num = num2;
				}
			}
			return num;
		}

		/// <summary>Returns the maximum value in a generic sequence.</summary>
		/// <param name="source">A sequence of values to determine the maximum value of.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The maximum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x060005D9 RID: 1497 RVA: 0x00011AD4 File Offset: 0x0000FCD4
		public static TSource Max<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			Comparer<TSource> @default = Comparer<TSource>.Default;
			TSource tsource = default(TSource);
			if (tsource == null)
			{
				foreach (TSource tsource2 in source)
				{
					if (tsource2 != null && (tsource == null || @default.Compare(tsource2, tsource) > 0))
					{
						tsource = tsource2;
					}
				}
				return tsource;
			}
			bool flag = false;
			foreach (TSource tsource3 in source)
			{
				if (flag)
				{
					if (@default.Compare(tsource3, tsource) > 0)
					{
						tsource = tsource3;
					}
				}
				else
				{
					tsource = tsource3;
					flag = true;
				}
			}
			if (flag)
			{
				return tsource;
			}
			throw Error.NoElements();
		}

		/// <summary>Invokes a transform function on each element of a sequence and returns the maximum <see cref="T:System.Int32" /> value.</summary>
		/// <param name="source">A sequence of values to determine the maximum value of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The maximum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005DA RID: 1498 RVA: 0x00011BB4 File Offset: 0x0000FDB4
		public static int Max<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector)
		{
			return source.Select(selector).Max();
		}

		/// <summary>Invokes a transform function on each element of a sequence and returns the maximum nullable <see cref="T:System.Int32" /> value.</summary>
		/// <param name="source">A sequence of values to determine the maximum value of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The value of type Nullable&lt;Int32&gt; in C# or Nullable(Of Int32) in Visual Basic that corresponds to the maximum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		// Token: 0x060005DB RID: 1499 RVA: 0x00011BC2 File Offset: 0x0000FDC2
		public static int? Max<TSource>(this IEnumerable<TSource> source, Func<TSource, int?> selector)
		{
			return source.Select(selector).Max();
		}

		/// <summary>Invokes a transform function on each element of a sequence and returns the maximum <see cref="T:System.Int64" /> value.</summary>
		/// <param name="source">A sequence of values to determine the maximum value of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The maximum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005DC RID: 1500 RVA: 0x00011BD0 File Offset: 0x0000FDD0
		public static long Max<TSource>(this IEnumerable<TSource> source, Func<TSource, long> selector)
		{
			return source.Select(selector).Max();
		}

		/// <summary>Invokes a transform function on each element of a sequence and returns the maximum nullable <see cref="T:System.Int64" /> value.</summary>
		/// <param name="source">A sequence of values to determine the maximum value of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The value of type Nullable&lt;Int64&gt; in C# or Nullable(Of Int64) in Visual Basic that corresponds to the maximum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		// Token: 0x060005DD RID: 1501 RVA: 0x00011BDE File Offset: 0x0000FDDE
		public static long? Max<TSource>(this IEnumerable<TSource> source, Func<TSource, long?> selector)
		{
			return source.Select(selector).Max();
		}

		/// <summary>Invokes a transform function on each element of a sequence and returns the maximum <see cref="T:System.Single" /> value.</summary>
		/// <param name="source">A sequence of values to determine the maximum value of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The maximum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005DE RID: 1502 RVA: 0x00011BEC File Offset: 0x0000FDEC
		public static float Max<TSource>(this IEnumerable<TSource> source, Func<TSource, float> selector)
		{
			return source.Select(selector).Max();
		}

		/// <summary>Invokes a transform function on each element of a sequence and returns the maximum nullable <see cref="T:System.Single" /> value.</summary>
		/// <param name="source">A sequence of values to determine the maximum value of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The value of type Nullable&lt;Single&gt; in C# or Nullable(Of Single) in Visual Basic that corresponds to the maximum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		// Token: 0x060005DF RID: 1503 RVA: 0x00011BFA File Offset: 0x0000FDFA
		public static float? Max<TSource>(this IEnumerable<TSource> source, Func<TSource, float?> selector)
		{
			return source.Select(selector).Max();
		}

		/// <summary>Invokes a transform function on each element of a sequence and returns the maximum <see cref="T:System.Double" /> value.</summary>
		/// <param name="source">A sequence of values to determine the maximum value of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The maximum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005E0 RID: 1504 RVA: 0x00011C08 File Offset: 0x0000FE08
		public static double Max<TSource>(this IEnumerable<TSource> source, Func<TSource, double> selector)
		{
			return source.Select(selector).Max();
		}

		/// <summary>Invokes a transform function on each element of a sequence and returns the maximum nullable <see cref="T:System.Double" /> value.</summary>
		/// <param name="source">A sequence of values to determine the maximum value of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The value of type Nullable&lt;Double&gt; in C# or Nullable(Of Double) in Visual Basic that corresponds to the maximum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		// Token: 0x060005E1 RID: 1505 RVA: 0x00011C16 File Offset: 0x0000FE16
		public static double? Max<TSource>(this IEnumerable<TSource> source, Func<TSource, double?> selector)
		{
			return source.Select(selector).Max();
		}

		/// <summary>Invokes a transform function on each element of a sequence and returns the maximum <see cref="T:System.Decimal" /> value.</summary>
		/// <param name="source">A sequence of values to determine the maximum value of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The maximum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005E2 RID: 1506 RVA: 0x00011C24 File Offset: 0x0000FE24
		public static decimal Max<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> selector)
		{
			return source.Select(selector).Max();
		}

		/// <summary>Invokes a transform function on each element of a sequence and returns the maximum nullable <see cref="T:System.Decimal" /> value.</summary>
		/// <param name="source">A sequence of values to determine the maximum value of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The value of type Nullable&lt;Decimal&gt; in C# or Nullable(Of Decimal) in Visual Basic that corresponds to the maximum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		// Token: 0x060005E3 RID: 1507 RVA: 0x00011C32 File Offset: 0x0000FE32
		public static decimal? Max<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> selector)
		{
			return source.Select(selector).Max();
		}

		/// <summary>Invokes a transform function on each element of a generic sequence and returns the maximum resulting value.</summary>
		/// <param name="source">A sequence of values to determine the maximum value of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <typeparam name="TResult">The type of the value returned by <paramref name="selector" />.</typeparam>
		/// <returns>The maximum value in the sequence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		// Token: 0x060005E4 RID: 1508 RVA: 0x00011C40 File Offset: 0x0000FE40
		public static TResult Max<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector)
		{
			return source.Select(selector).Max<TResult>();
		}

		/// <summary>Computes the average of a sequence of <see cref="T:System.Int32" /> values.</summary>
		/// <param name="source">A sequence of <see cref="T:System.Int32" /> values to calculate the average of.</param>
		/// <returns>The average of the sequence of values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005E5 RID: 1509 RVA: 0x00011C50 File Offset: 0x0000FE50
		public static double Average(this IEnumerable<int> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			long num = 0L;
			long num2 = 0L;
			checked
			{
				foreach (int num3 in source)
				{
					num += unchecked((long)num3);
					num2 += 1L;
				}
				if (num2 > 0L)
				{
					return (double)num / (double)num2;
				}
				throw Error.NoElements();
			}
		}

		/// <summary>Computes the average of a sequence of nullable <see cref="T:System.Int32" /> values.</summary>
		/// <param name="source">A sequence of nullable <see cref="T:System.Int32" /> values to calculate the average of.</param>
		/// <returns>The average of the sequence of values, or <see langword="null" /> if the source sequence is empty or contains only values that are <see langword="null" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.OverflowException">The sum of the elements in the sequence is larger than <see cref="F:System.Int64.MaxValue" />.</exception>
		// Token: 0x060005E6 RID: 1510 RVA: 0x00011CC0 File Offset: 0x0000FEC0
		public static double? Average(this IEnumerable<int?> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			long num = 0L;
			long num2 = 0L;
			checked
			{
				foreach (int? num3 in source)
				{
					if (num3 != null)
					{
						num += unchecked((long)num3.GetValueOrDefault());
						num2 += 1L;
					}
				}
				if (num2 > 0L)
				{
					return new double?((double)num / (double)num2);
				}
				return null;
			}
		}

		/// <summary>Computes the average of a sequence of <see cref="T:System.Int64" /> values.</summary>
		/// <param name="source">A sequence of <see cref="T:System.Int64" /> values to calculate the average of.</param>
		/// <returns>The average of the sequence of values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005E7 RID: 1511 RVA: 0x00011D48 File Offset: 0x0000FF48
		public static double Average(this IEnumerable<long> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			long num = 0L;
			long num2 = 0L;
			checked
			{
				foreach (long num3 in source)
				{
					num += num3;
					num2 += 1L;
				}
				if (num2 > 0L)
				{
					return (double)num / (double)num2;
				}
				throw Error.NoElements();
			}
		}

		/// <summary>Computes the average of a sequence of nullable <see cref="T:System.Int64" /> values.</summary>
		/// <param name="source">A sequence of nullable <see cref="T:System.Int64" /> values to calculate the average of.</param>
		/// <returns>The average of the sequence of values, or <see langword="null" /> if the source sequence is empty or contains only values that are <see langword="null" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.OverflowException">The sum of the elements in the sequence is larger than <see cref="F:System.Int64.MaxValue" />.</exception>
		// Token: 0x060005E8 RID: 1512 RVA: 0x00011DB8 File Offset: 0x0000FFB8
		public static double? Average(this IEnumerable<long?> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			long num = 0L;
			long num2 = 0L;
			checked
			{
				foreach (long? num3 in source)
				{
					if (num3 != null)
					{
						num += num3.GetValueOrDefault();
						num2 += 1L;
					}
				}
				if (num2 > 0L)
				{
					return new double?((double)num / (double)num2);
				}
				return null;
			}
		}

		/// <summary>Computes the average of a sequence of <see cref="T:System.Single" /> values.</summary>
		/// <param name="source">A sequence of <see cref="T:System.Single" /> values to calculate the average of.</param>
		/// <returns>The average of the sequence of values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005E9 RID: 1513 RVA: 0x00011E40 File Offset: 0x00010040
		public static float Average(this IEnumerable<float> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			double num = 0.0;
			long num2 = 0L;
			foreach (float num3 in source)
			{
				num += (double)num3;
				checked
				{
					num2 += 1L;
				}
			}
			if (num2 > 0L)
			{
				return (float)(num / (double)num2);
			}
			throw Error.NoElements();
		}

		/// <summary>Computes the average of a sequence of nullable <see cref="T:System.Single" /> values.</summary>
		/// <param name="source">A sequence of nullable <see cref="T:System.Single" /> values to calculate the average of.</param>
		/// <returns>The average of the sequence of values, or <see langword="null" /> if the source sequence is empty or contains only values that are <see langword="null" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x060005EA RID: 1514 RVA: 0x00011EB8 File Offset: 0x000100B8
		public static float? Average(this IEnumerable<float?> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			double num = 0.0;
			long num2 = 0L;
			foreach (float? num3 in source)
			{
				if (num3 != null)
				{
					num += (double)num3.GetValueOrDefault();
					checked
					{
						num2 += 1L;
					}
				}
			}
			if (num2 > 0L)
			{
				return new float?((float)(num / (double)num2));
			}
			return null;
		}

		/// <summary>Computes the average of a sequence of <see cref="T:System.Double" /> values.</summary>
		/// <param name="source">A sequence of <see cref="T:System.Double" /> values to calculate the average of.</param>
		/// <returns>The average of the sequence of values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005EB RID: 1515 RVA: 0x00011F48 File Offset: 0x00010148
		public static double Average(this IEnumerable<double> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			double num = 0.0;
			long num2 = 0L;
			foreach (double num3 in source)
			{
				num += num3;
				checked
				{
					num2 += 1L;
				}
			}
			if (num2 > 0L)
			{
				return num / (double)num2;
			}
			throw Error.NoElements();
		}

		/// <summary>Computes the average of a sequence of nullable <see cref="T:System.Double" /> values.</summary>
		/// <param name="source">A sequence of nullable <see cref="T:System.Double" /> values to calculate the average of.</param>
		/// <returns>The average of the sequence of values, or <see langword="null" /> if the source sequence is empty or contains only values that are <see langword="null" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		// Token: 0x060005EC RID: 1516 RVA: 0x00011FBC File Offset: 0x000101BC
		public static double? Average(this IEnumerable<double?> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			double num = 0.0;
			long num2 = 0L;
			foreach (double? num3 in source)
			{
				if (num3 != null)
				{
					num += num3.GetValueOrDefault();
					checked
					{
						num2 += 1L;
					}
				}
			}
			if (num2 > 0L)
			{
				return new double?(num / (double)num2);
			}
			return null;
		}

		/// <summary>Computes the average of a sequence of <see cref="T:System.Decimal" /> values.</summary>
		/// <param name="source">A sequence of <see cref="T:System.Decimal" /> values to calculate the average of.</param>
		/// <returns>The average of the sequence of values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005ED RID: 1517 RVA: 0x0001204C File Offset: 0x0001024C
		public static decimal Average(this IEnumerable<decimal> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			decimal d = 0m;
			long num = 0L;
			checked
			{
				foreach (decimal d2 in source)
				{
					d += d2;
					num += 1L;
				}
				if (num > 0L)
				{
					return d / num;
				}
				throw Error.NoElements();
			}
		}

		/// <summary>Computes the average of a sequence of nullable <see cref="T:System.Decimal" /> values.</summary>
		/// <param name="source">A sequence of nullable <see cref="T:System.Decimal" /> values to calculate the average of.</param>
		/// <returns>The average of the sequence of values, or <see langword="null" /> if the source sequence is empty or contains only values that are <see langword="null" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.OverflowException">The sum of the elements in the sequence is larger than <see cref="F:System.Decimal.MaxValue" />.</exception>
		// Token: 0x060005EE RID: 1518 RVA: 0x000120CC File Offset: 0x000102CC
		public static decimal? Average(this IEnumerable<decimal?> source)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			decimal d = 0m;
			long num = 0L;
			checked
			{
				foreach (decimal? num2 in source)
				{
					if (num2 != null)
					{
						d += num2.GetValueOrDefault();
						num += 1L;
					}
				}
				if (num > 0L)
				{
					return new decimal?(d / num);
				}
				return null;
			}
		}

		/// <summary>Computes the average of a sequence of <see cref="T:System.Int32" /> values that are obtained by invoking a transform function on each element of the input sequence.</summary>
		/// <param name="source">A sequence of values to calculate the average of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The average of the sequence of values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		/// <exception cref="T:System.OverflowException">The sum of the elements in the sequence is larger than <see cref="F:System.Int64.MaxValue" />.</exception>
		// Token: 0x060005EF RID: 1519 RVA: 0x00012164 File Offset: 0x00010364
		public static double Average<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector)
		{
			return source.Select(selector).Average();
		}

		/// <summary>Computes the average of a sequence of nullable <see cref="T:System.Int32" /> values that are obtained by invoking a transform function on each element of the input sequence.</summary>
		/// <param name="source">A sequence of values to calculate the average of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The average of the sequence of values, or <see langword="null" /> if the source sequence is empty or contains only values that are <see langword="null" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.OverflowException">The sum of the elements in the sequence is larger than <see cref="F:System.Int64.MaxValue" />.</exception>
		// Token: 0x060005F0 RID: 1520 RVA: 0x00012172 File Offset: 0x00010372
		public static double? Average<TSource>(this IEnumerable<TSource> source, Func<TSource, int?> selector)
		{
			return source.Select(selector).Average();
		}

		/// <summary>Computes the average of a sequence of <see cref="T:System.Int64" /> values that are obtained by invoking a transform function on each element of the input sequence.</summary>
		/// <param name="source">A sequence of values to calculate the average of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of source.</typeparam>
		/// <returns>The average of the sequence of values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		/// <exception cref="T:System.OverflowException">The sum of the elements in the sequence is larger than <see cref="F:System.Int64.MaxValue" />.</exception>
		// Token: 0x060005F1 RID: 1521 RVA: 0x00012180 File Offset: 0x00010380
		public static double Average<TSource>(this IEnumerable<TSource> source, Func<TSource, long> selector)
		{
			return source.Select(selector).Average();
		}

		/// <summary>Computes the average of a sequence of nullable <see cref="T:System.Int64" /> values that are obtained by invoking a transform function on each element of the input sequence.</summary>
		/// <param name="source">A sequence of values to calculate the average of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The average of the sequence of values, or <see langword="null" /> if the source sequence is empty or contains only values that are <see langword="null" />.</returns>
		// Token: 0x060005F2 RID: 1522 RVA: 0x0001218E File Offset: 0x0001038E
		public static double? Average<TSource>(this IEnumerable<TSource> source, Func<TSource, long?> selector)
		{
			return source.Select(selector).Average();
		}

		/// <summary>Computes the average of a sequence of <see cref="T:System.Single" /> values that are obtained by invoking a transform function on each element of the input sequence.</summary>
		/// <param name="source">A sequence of values to calculate the average of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The average of the sequence of values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005F3 RID: 1523 RVA: 0x0001219C File Offset: 0x0001039C
		public static float Average<TSource>(this IEnumerable<TSource> source, Func<TSource, float> selector)
		{
			return source.Select(selector).Average();
		}

		/// <summary>Computes the average of a sequence of nullable <see cref="T:System.Single" /> values that are obtained by invoking a transform function on each element of the input sequence.</summary>
		/// <param name="source">A sequence of values to calculate the average of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The average of the sequence of values, or <see langword="null" /> if the source sequence is empty or contains only values that are <see langword="null" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		// Token: 0x060005F4 RID: 1524 RVA: 0x000121AA File Offset: 0x000103AA
		public static float? Average<TSource>(this IEnumerable<TSource> source, Func<TSource, float?> selector)
		{
			return source.Select(selector).Average();
		}

		/// <summary>Computes the average of a sequence of <see cref="T:System.Double" /> values that are obtained by invoking a transform function on each element of the input sequence.</summary>
		/// <param name="source">A sequence of values to calculate the average of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The average of the sequence of values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		// Token: 0x060005F5 RID: 1525 RVA: 0x000121B8 File Offset: 0x000103B8
		public static double Average<TSource>(this IEnumerable<TSource> source, Func<TSource, double> selector)
		{
			return source.Select(selector).Average();
		}

		/// <summary>Computes the average of a sequence of nullable <see cref="T:System.Double" /> values that are obtained by invoking a transform function on each element of the input sequence.</summary>
		/// <param name="source">A sequence of values to calculate the average of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The average of the sequence of values, or <see langword="null" /> if the source sequence is empty or contains only values that are <see langword="null" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		// Token: 0x060005F6 RID: 1526 RVA: 0x000121C6 File Offset: 0x000103C6
		public static double? Average<TSource>(this IEnumerable<TSource> source, Func<TSource, double?> selector)
		{
			return source.Select(selector).Average();
		}

		/// <summary>Computes the average of a sequence of <see cref="T:System.Decimal" /> values that are obtained by invoking a transform function on each element of the input sequence.</summary>
		/// <param name="source">A sequence of values that are used to calculate an average.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The average of the sequence of values.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="source" /> contains no elements.</exception>
		/// <exception cref="T:System.OverflowException">The sum of the elements in the sequence is larger than <see cref="F:System.Decimal.MaxValue" />.</exception>
		// Token: 0x060005F7 RID: 1527 RVA: 0x000121D4 File Offset: 0x000103D4
		public static decimal Average<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> selector)
		{
			return source.Select(selector).Average();
		}

		/// <summary>Computes the average of a sequence of nullable <see cref="T:System.Decimal" /> values that are obtained by invoking a transform function on each element of the input sequence.</summary>
		/// <param name="source">A sequence of values to calculate the average of.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source" />.</typeparam>
		/// <returns>The average of the sequence of values, or <see langword="null" /> if the source sequence is empty or contains only values that are <see langword="null" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="source" /> or <paramref name="selector" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.OverflowException">The sum of the elements in the sequence is larger than <see cref="F:System.Decimal.MaxValue" />.</exception>
		// Token: 0x060005F8 RID: 1528 RVA: 0x000121E2 File Offset: 0x000103E2
		public static decimal? Average<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> selector)
		{
			return source.Select(selector).Average();
		}

		// Token: 0x02000097 RID: 151
		private abstract class Iterator<TSource> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x060005F9 RID: 1529 RVA: 0x000121F0 File Offset: 0x000103F0
			public Iterator()
			{
				this.threadId = Thread.CurrentThread.ManagedThreadId;
			}

			// Token: 0x170000C9 RID: 201
			// (get) Token: 0x060005FA RID: 1530 RVA: 0x00012208 File Offset: 0x00010408
			public TSource Current
			{
				get
				{
					return this.current;
				}
			}

			// Token: 0x060005FB RID: 1531
			public abstract Enumerable.Iterator<TSource> Clone();

			// Token: 0x060005FC RID: 1532 RVA: 0x00012210 File Offset: 0x00010410
			public virtual void Dispose()
			{
				this.current = default(TSource);
				this.state = -1;
			}

			// Token: 0x060005FD RID: 1533 RVA: 0x00012225 File Offset: 0x00010425
			public IEnumerator<TSource> GetEnumerator()
			{
				if (this.threadId == Thread.CurrentThread.ManagedThreadId && this.state == 0)
				{
					this.state = 1;
					return this;
				}
				Enumerable.Iterator<TSource> iterator = this.Clone();
				iterator.state = 1;
				return iterator;
			}

			// Token: 0x060005FE RID: 1534
			public abstract bool MoveNext();

			// Token: 0x060005FF RID: 1535
			public abstract IEnumerable<TResult> Select<TResult>(Func<TSource, TResult> selector);

			// Token: 0x06000600 RID: 1536
			public abstract IEnumerable<TSource> Where(Func<TSource, bool> predicate);

			// Token: 0x170000CA RID: 202
			// (get) Token: 0x06000601 RID: 1537 RVA: 0x00012257 File Offset: 0x00010457
			object IEnumerator.Current
			{
				get
				{
					return this.Current;
				}
			}

			// Token: 0x06000602 RID: 1538 RVA: 0x00012264 File Offset: 0x00010464
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			// Token: 0x06000603 RID: 1539 RVA: 0x0000227E File Offset: 0x0000047E
			void IEnumerator.Reset()
			{
				throw new NotImplementedException();
			}

			// Token: 0x0400039C RID: 924
			private int threadId;

			// Token: 0x0400039D RID: 925
			internal int state;

			// Token: 0x0400039E RID: 926
			internal TSource current;
		}

		// Token: 0x02000098 RID: 152
		private class WhereEnumerableIterator<TSource> : Enumerable.Iterator<TSource>
		{
			// Token: 0x06000604 RID: 1540 RVA: 0x0001226C File Offset: 0x0001046C
			public WhereEnumerableIterator(IEnumerable<TSource> source, Func<TSource, bool> predicate)
			{
				this.source = source;
				this.predicate = predicate;
			}

			// Token: 0x06000605 RID: 1541 RVA: 0x00012282 File Offset: 0x00010482
			public override Enumerable.Iterator<TSource> Clone()
			{
				return new Enumerable.WhereEnumerableIterator<TSource>(this.source, this.predicate);
			}

			// Token: 0x06000606 RID: 1542 RVA: 0x00012295 File Offset: 0x00010495
			public override void Dispose()
			{
				if (this.enumerator != null)
				{
					this.enumerator.Dispose();
				}
				this.enumerator = null;
				base.Dispose();
			}

			// Token: 0x06000607 RID: 1543 RVA: 0x000122B8 File Offset: 0x000104B8
			public override bool MoveNext()
			{
				int state = this.state;
				if (state != 1)
				{
					if (state != 2)
					{
						return false;
					}
				}
				else
				{
					this.enumerator = this.source.GetEnumerator();
					this.state = 2;
				}
				while (this.enumerator.MoveNext())
				{
					TSource tsource = this.enumerator.Current;
					if (this.predicate(tsource))
					{
						this.current = tsource;
						return true;
					}
				}
				this.Dispose();
				return false;
			}

			// Token: 0x06000608 RID: 1544 RVA: 0x00012327 File Offset: 0x00010527
			public override IEnumerable<TResult> Select<TResult>(Func<TSource, TResult> selector)
			{
				return new Enumerable.WhereSelectEnumerableIterator<TSource, TResult>(this.source, this.predicate, selector);
			}

			// Token: 0x06000609 RID: 1545 RVA: 0x0001233B File Offset: 0x0001053B
			public override IEnumerable<TSource> Where(Func<TSource, bool> predicate)
			{
				return new Enumerable.WhereEnumerableIterator<TSource>(this.source, Enumerable.CombinePredicates<TSource>(this.predicate, predicate));
			}

			// Token: 0x0400039F RID: 927
			private IEnumerable<TSource> source;

			// Token: 0x040003A0 RID: 928
			private Func<TSource, bool> predicate;

			// Token: 0x040003A1 RID: 929
			private IEnumerator<TSource> enumerator;
		}

		// Token: 0x02000099 RID: 153
		private class WhereArrayIterator<TSource> : Enumerable.Iterator<TSource>
		{
			// Token: 0x0600060A RID: 1546 RVA: 0x00012354 File Offset: 0x00010554
			public WhereArrayIterator(TSource[] source, Func<TSource, bool> predicate)
			{
				this.source = source;
				this.predicate = predicate;
			}

			// Token: 0x0600060B RID: 1547 RVA: 0x0001236A File Offset: 0x0001056A
			public override Enumerable.Iterator<TSource> Clone()
			{
				return new Enumerable.WhereArrayIterator<TSource>(this.source, this.predicate);
			}

			// Token: 0x0600060C RID: 1548 RVA: 0x00012380 File Offset: 0x00010580
			public override bool MoveNext()
			{
				if (this.state == 1)
				{
					while (this.index < this.source.Length)
					{
						TSource tsource = this.source[this.index];
						this.index++;
						if (this.predicate(tsource))
						{
							this.current = tsource;
							return true;
						}
					}
					this.Dispose();
				}
				return false;
			}

			// Token: 0x0600060D RID: 1549 RVA: 0x000123E6 File Offset: 0x000105E6
			public override IEnumerable<TResult> Select<TResult>(Func<TSource, TResult> selector)
			{
				return new Enumerable.WhereSelectArrayIterator<TSource, TResult>(this.source, this.predicate, selector);
			}

			// Token: 0x0600060E RID: 1550 RVA: 0x000123FA File Offset: 0x000105FA
			public override IEnumerable<TSource> Where(Func<TSource, bool> predicate)
			{
				return new Enumerable.WhereArrayIterator<TSource>(this.source, Enumerable.CombinePredicates<TSource>(this.predicate, predicate));
			}

			// Token: 0x040003A2 RID: 930
			private TSource[] source;

			// Token: 0x040003A3 RID: 931
			private Func<TSource, bool> predicate;

			// Token: 0x040003A4 RID: 932
			private int index;
		}

		// Token: 0x0200009A RID: 154
		private class WhereListIterator<TSource> : Enumerable.Iterator<TSource>
		{
			// Token: 0x0600060F RID: 1551 RVA: 0x00012413 File Offset: 0x00010613
			public WhereListIterator(List<TSource> source, Func<TSource, bool> predicate)
			{
				this.source = source;
				this.predicate = predicate;
			}

			// Token: 0x06000610 RID: 1552 RVA: 0x00012429 File Offset: 0x00010629
			public override Enumerable.Iterator<TSource> Clone()
			{
				return new Enumerable.WhereListIterator<TSource>(this.source, this.predicate);
			}

			// Token: 0x06000611 RID: 1553 RVA: 0x0001243C File Offset: 0x0001063C
			public override bool MoveNext()
			{
				int state = this.state;
				if (state != 1)
				{
					if (state != 2)
					{
						return false;
					}
				}
				else
				{
					this.enumerator = this.source.GetEnumerator();
					this.state = 2;
				}
				while (this.enumerator.MoveNext())
				{
					TSource tsource = this.enumerator.Current;
					if (this.predicate(tsource))
					{
						this.current = tsource;
						return true;
					}
				}
				this.Dispose();
				return false;
			}

			// Token: 0x06000612 RID: 1554 RVA: 0x000124AB File Offset: 0x000106AB
			public override IEnumerable<TResult> Select<TResult>(Func<TSource, TResult> selector)
			{
				return new Enumerable.WhereSelectListIterator<TSource, TResult>(this.source, this.predicate, selector);
			}

			// Token: 0x06000613 RID: 1555 RVA: 0x000124BF File Offset: 0x000106BF
			public override IEnumerable<TSource> Where(Func<TSource, bool> predicate)
			{
				return new Enumerable.WhereListIterator<TSource>(this.source, Enumerable.CombinePredicates<TSource>(this.predicate, predicate));
			}

			// Token: 0x040003A5 RID: 933
			private List<TSource> source;

			// Token: 0x040003A6 RID: 934
			private Func<TSource, bool> predicate;

			// Token: 0x040003A7 RID: 935
			private List<TSource>.Enumerator enumerator;
		}

		// Token: 0x0200009B RID: 155
		private class SelectEnumerableIterator<TSource, TResult> : Enumerable.Iterator<TResult>, IIListProvider<TResult>, IEnumerable<!1>, IEnumerable
		{
			// Token: 0x06000614 RID: 1556 RVA: 0x000124D8 File Offset: 0x000106D8
			public SelectEnumerableIterator(IEnumerable<TSource> source, Func<TSource, TResult> selector)
			{
				this._source = source;
				this._selector = selector;
			}

			// Token: 0x06000615 RID: 1557 RVA: 0x000124EE File Offset: 0x000106EE
			public override Enumerable.Iterator<TResult> Clone()
			{
				return new Enumerable.SelectEnumerableIterator<TSource, TResult>(this._source, this._selector);
			}

			// Token: 0x06000616 RID: 1558 RVA: 0x00012501 File Offset: 0x00010701
			public override void Dispose()
			{
				if (this._enumerator != null)
				{
					this._enumerator.Dispose();
					this._enumerator = null;
				}
				base.Dispose();
			}

			// Token: 0x06000617 RID: 1559 RVA: 0x00012524 File Offset: 0x00010724
			public override bool MoveNext()
			{
				int state = this.state;
				if (state != 1)
				{
					if (state != 2)
					{
						return false;
					}
				}
				else
				{
					this._enumerator = this._source.GetEnumerator();
					this.state = 2;
				}
				if (this._enumerator.MoveNext())
				{
					this.current = this._selector(this._enumerator.Current);
					return true;
				}
				this.Dispose();
				return false;
			}

			// Token: 0x06000618 RID: 1560 RVA: 0x0001258C File Offset: 0x0001078C
			public override IEnumerable<TResult2> Select<TResult2>(Func<TResult, TResult2> selector)
			{
				return new Enumerable.SelectEnumerableIterator<TSource, TResult2>(this._source, Enumerable.CombineSelectors<TSource, TResult, TResult2>(this._selector, selector));
			}

			// Token: 0x06000619 RID: 1561 RVA: 0x000125A5 File Offset: 0x000107A5
			public override IEnumerable<TResult> Where(Func<TResult, bool> predicate)
			{
				return new Enumerable.WhereEnumerableIterator<TResult>(this, predicate);
			}

			// Token: 0x0600061A RID: 1562 RVA: 0x000125B0 File Offset: 0x000107B0
			public TResult[] ToArray()
			{
				LargeArrayBuilder<TResult> largeArrayBuilder = new LargeArrayBuilder<TResult>(true);
				foreach (TSource arg in this._source)
				{
					largeArrayBuilder.Add(this._selector(arg));
				}
				return largeArrayBuilder.ToArray();
			}

			// Token: 0x0600061B RID: 1563 RVA: 0x00012618 File Offset: 0x00010818
			public List<TResult> ToList()
			{
				List<TResult> list = new List<TResult>();
				foreach (TSource arg in this._source)
				{
					list.Add(this._selector(arg));
				}
				return list;
			}

			// Token: 0x0600061C RID: 1564 RVA: 0x00012678 File Offset: 0x00010878
			public int GetCount(bool onlyIfCheap)
			{
				if (onlyIfCheap)
				{
					return -1;
				}
				int num = 0;
				checked
				{
					foreach (TSource arg in this._source)
					{
						this._selector(arg);
						num++;
					}
					return num;
				}
			}

			// Token: 0x040003A8 RID: 936
			private readonly IEnumerable<TSource> _source;

			// Token: 0x040003A9 RID: 937
			private readonly Func<TSource, TResult> _selector;

			// Token: 0x040003AA RID: 938
			private IEnumerator<TSource> _enumerator;
		}

		// Token: 0x0200009C RID: 156
		private class WhereSelectEnumerableIterator<TSource, TResult> : Enumerable.Iterator<TResult>
		{
			// Token: 0x0600061D RID: 1565 RVA: 0x000126D8 File Offset: 0x000108D8
			public WhereSelectEnumerableIterator(IEnumerable<TSource> source, Func<TSource, bool> predicate, Func<TSource, TResult> selector)
			{
				this.source = source;
				this.predicate = predicate;
				this.selector = selector;
			}

			// Token: 0x0600061E RID: 1566 RVA: 0x000126F5 File Offset: 0x000108F5
			public override Enumerable.Iterator<TResult> Clone()
			{
				return new Enumerable.WhereSelectEnumerableIterator<TSource, TResult>(this.source, this.predicate, this.selector);
			}

			// Token: 0x0600061F RID: 1567 RVA: 0x0001270E File Offset: 0x0001090E
			public override void Dispose()
			{
				if (this.enumerator != null)
				{
					this.enumerator.Dispose();
				}
				this.enumerator = null;
				base.Dispose();
			}

			// Token: 0x06000620 RID: 1568 RVA: 0x00012730 File Offset: 0x00010930
			public override bool MoveNext()
			{
				int state = this.state;
				if (state != 1)
				{
					if (state != 2)
					{
						return false;
					}
				}
				else
				{
					this.enumerator = this.source.GetEnumerator();
					this.state = 2;
				}
				while (this.enumerator.MoveNext())
				{
					TSource arg = this.enumerator.Current;
					if (this.predicate == null || this.predicate(arg))
					{
						this.current = this.selector(arg);
						return true;
					}
				}
				this.Dispose();
				return false;
			}

			// Token: 0x06000621 RID: 1569 RVA: 0x000127B2 File Offset: 0x000109B2
			public override IEnumerable<TResult2> Select<TResult2>(Func<TResult, TResult2> selector)
			{
				return new Enumerable.WhereSelectEnumerableIterator<TSource, TResult2>(this.source, this.predicate, Enumerable.CombineSelectors<TSource, TResult, TResult2>(this.selector, selector));
			}

			// Token: 0x06000622 RID: 1570 RVA: 0x000125A5 File Offset: 0x000107A5
			public override IEnumerable<TResult> Where(Func<TResult, bool> predicate)
			{
				return new Enumerable.WhereEnumerableIterator<TResult>(this, predicate);
			}

			// Token: 0x040003AB RID: 939
			private IEnumerable<TSource> source;

			// Token: 0x040003AC RID: 940
			private Func<TSource, bool> predicate;

			// Token: 0x040003AD RID: 941
			private Func<TSource, TResult> selector;

			// Token: 0x040003AE RID: 942
			private IEnumerator<TSource> enumerator;
		}

		// Token: 0x0200009D RID: 157
		private class WhereSelectArrayIterator<TSource, TResult> : Enumerable.Iterator<TResult>
		{
			// Token: 0x06000623 RID: 1571 RVA: 0x000127D1 File Offset: 0x000109D1
			public WhereSelectArrayIterator(TSource[] source, Func<TSource, bool> predicate, Func<TSource, TResult> selector)
			{
				this.source = source;
				this.predicate = predicate;
				this.selector = selector;
			}

			// Token: 0x06000624 RID: 1572 RVA: 0x000127EE File Offset: 0x000109EE
			public override Enumerable.Iterator<TResult> Clone()
			{
				return new Enumerable.WhereSelectArrayIterator<TSource, TResult>(this.source, this.predicate, this.selector);
			}

			// Token: 0x06000625 RID: 1573 RVA: 0x00012808 File Offset: 0x00010A08
			public override bool MoveNext()
			{
				if (this.state == 1)
				{
					while (this.index < this.source.Length)
					{
						TSource arg = this.source[this.index];
						this.index++;
						if (this.predicate == null || this.predicate(arg))
						{
							this.current = this.selector(arg);
							return true;
						}
					}
					this.Dispose();
				}
				return false;
			}

			// Token: 0x06000626 RID: 1574 RVA: 0x00012881 File Offset: 0x00010A81
			public override IEnumerable<TResult2> Select<TResult2>(Func<TResult, TResult2> selector)
			{
				return new Enumerable.WhereSelectArrayIterator<TSource, TResult2>(this.source, this.predicate, Enumerable.CombineSelectors<TSource, TResult, TResult2>(this.selector, selector));
			}

			// Token: 0x06000627 RID: 1575 RVA: 0x000125A5 File Offset: 0x000107A5
			public override IEnumerable<TResult> Where(Func<TResult, bool> predicate)
			{
				return new Enumerable.WhereEnumerableIterator<TResult>(this, predicate);
			}

			// Token: 0x040003AF RID: 943
			private TSource[] source;

			// Token: 0x040003B0 RID: 944
			private Func<TSource, bool> predicate;

			// Token: 0x040003B1 RID: 945
			private Func<TSource, TResult> selector;

			// Token: 0x040003B2 RID: 946
			private int index;
		}

		// Token: 0x0200009E RID: 158
		private class WhereSelectListIterator<TSource, TResult> : Enumerable.Iterator<TResult>
		{
			// Token: 0x06000628 RID: 1576 RVA: 0x000128A0 File Offset: 0x00010AA0
			public WhereSelectListIterator(List<TSource> source, Func<TSource, bool> predicate, Func<TSource, TResult> selector)
			{
				this.source = source;
				this.predicate = predicate;
				this.selector = selector;
			}

			// Token: 0x06000629 RID: 1577 RVA: 0x000128BD File Offset: 0x00010ABD
			public override Enumerable.Iterator<TResult> Clone()
			{
				return new Enumerable.WhereSelectListIterator<TSource, TResult>(this.source, this.predicate, this.selector);
			}

			// Token: 0x0600062A RID: 1578 RVA: 0x000128D8 File Offset: 0x00010AD8
			public override bool MoveNext()
			{
				int state = this.state;
				if (state != 1)
				{
					if (state != 2)
					{
						return false;
					}
				}
				else
				{
					this.enumerator = this.source.GetEnumerator();
					this.state = 2;
				}
				while (this.enumerator.MoveNext())
				{
					TSource arg = this.enumerator.Current;
					if (this.predicate == null || this.predicate(arg))
					{
						this.current = this.selector(arg);
						return true;
					}
				}
				this.Dispose();
				return false;
			}

			// Token: 0x0600062B RID: 1579 RVA: 0x0001295A File Offset: 0x00010B5A
			public override IEnumerable<TResult2> Select<TResult2>(Func<TResult, TResult2> selector)
			{
				return new Enumerable.WhereSelectListIterator<TSource, TResult2>(this.source, this.predicate, Enumerable.CombineSelectors<TSource, TResult, TResult2>(this.selector, selector));
			}

			// Token: 0x0600062C RID: 1580 RVA: 0x000125A5 File Offset: 0x000107A5
			public override IEnumerable<TResult> Where(Func<TResult, bool> predicate)
			{
				return new Enumerable.WhereEnumerableIterator<TResult>(this, predicate);
			}

			// Token: 0x040003B3 RID: 947
			private List<TSource> source;

			// Token: 0x040003B4 RID: 948
			private Func<TSource, bool> predicate;

			// Token: 0x040003B5 RID: 949
			private Func<TSource, TResult> selector;

			// Token: 0x040003B6 RID: 950
			private List<TSource>.Enumerator enumerator;
		}

		// Token: 0x0200009F RID: 159
		[CompilerGenerated]
		private sealed class <WhereIterator>d__2<TSource> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x0600062D RID: 1581 RVA: 0x00012979 File Offset: 0x00010B79
			[DebuggerHidden]
			public <WhereIterator>d__2(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x0600062E RID: 1582 RVA: 0x00012994 File Offset: 0x00010B94
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x0600062F RID: 1583 RVA: 0x000129CC File Offset: 0x00010BCC
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						index = -1;
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
					}
					while (enumerator.MoveNext())
					{
						TSource arg = enumerator.Current;
						int num2 = index;
						index = checked(num2 + 1);
						if (predicate(arg, index))
						{
							this.<>2__current = arg;
							this.<>1__state = 1;
							return true;
						}
					}
					this.<>m__Finally1();
					enumerator = null;
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x06000630 RID: 1584 RVA: 0x00012A98 File Offset: 0x00010C98
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x170000CB RID: 203
			// (get) Token: 0x06000631 RID: 1585 RVA: 0x00012AB4 File Offset: 0x00010CB4
			TSource IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000632 RID: 1586 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000CC RID: 204
			// (get) Token: 0x06000633 RID: 1587 RVA: 0x00012ABC File Offset: 0x00010CBC
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000634 RID: 1588 RVA: 0x00012ACC File Offset: 0x00010CCC
			[DebuggerHidden]
			IEnumerator<TSource> IEnumerable<!0>.GetEnumerator()
			{
				Enumerable.<WhereIterator>d__2<TSource> <WhereIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<WhereIterator>d__ = this;
				}
				else
				{
					<WhereIterator>d__ = new Enumerable.<WhereIterator>d__2<TSource>(0);
				}
				<WhereIterator>d__.source = source;
				<WhereIterator>d__.predicate = predicate;
				return <WhereIterator>d__;
			}

			// Token: 0x06000635 RID: 1589 RVA: 0x00012B1B File Offset: 0x00010D1B
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TSource>.GetEnumerator();
			}

			// Token: 0x040003B7 RID: 951
			private int <>1__state;

			// Token: 0x040003B8 RID: 952
			private TSource <>2__current;

			// Token: 0x040003B9 RID: 953
			private int <>l__initialThreadId;

			// Token: 0x040003BA RID: 954
			private IEnumerable<TSource> source;

			// Token: 0x040003BB RID: 955
			public IEnumerable<TSource> <>3__source;

			// Token: 0x040003BC RID: 956
			private int <index>5__1;

			// Token: 0x040003BD RID: 957
			private Func<TSource, int, bool> predicate;

			// Token: 0x040003BE RID: 958
			public Func<TSource, int, bool> <>3__predicate;

			// Token: 0x040003BF RID: 959
			private IEnumerator<TSource> <>7__wrap1;
		}

		// Token: 0x020000A0 RID: 160
		[CompilerGenerated]
		private sealed class <SelectIterator>d__5<TSource, TResult> : IEnumerable<!1>, IEnumerable, IEnumerator<TResult>, IDisposable, IEnumerator
		{
			// Token: 0x06000636 RID: 1590 RVA: 0x00012B23 File Offset: 0x00010D23
			[DebuggerHidden]
			public <SelectIterator>d__5(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000637 RID: 1591 RVA: 0x00012B40 File Offset: 0x00010D40
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x06000638 RID: 1592 RVA: 0x00012B78 File Offset: 0x00010D78
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						index = -1;
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
					}
					if (!enumerator.MoveNext())
					{
						this.<>m__Finally1();
						enumerator = null;
						result = false;
					}
					else
					{
						TSource arg = enumerator.Current;
						int num2 = index;
						index = checked(num2 + 1);
						this.<>2__current = selector(arg, index);
						this.<>1__state = 1;
						result = true;
					}
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x06000639 RID: 1593 RVA: 0x00012C40 File Offset: 0x00010E40
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x170000CD RID: 205
			// (get) Token: 0x0600063A RID: 1594 RVA: 0x00012C5C File Offset: 0x00010E5C
			TResult IEnumerator<!1>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600063B RID: 1595 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000CE RID: 206
			// (get) Token: 0x0600063C RID: 1596 RVA: 0x00012C64 File Offset: 0x00010E64
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600063D RID: 1597 RVA: 0x00012C74 File Offset: 0x00010E74
			[DebuggerHidden]
			IEnumerator<TResult> IEnumerable<!1>.GetEnumerator()
			{
				Enumerable.<SelectIterator>d__5<TSource, TResult> <SelectIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<SelectIterator>d__ = this;
				}
				else
				{
					<SelectIterator>d__ = new Enumerable.<SelectIterator>d__5<TSource, TResult>(0);
				}
				<SelectIterator>d__.source = source;
				<SelectIterator>d__.selector = selector;
				return <SelectIterator>d__;
			}

			// Token: 0x0600063E RID: 1598 RVA: 0x00012CC3 File Offset: 0x00010EC3
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TResult>.GetEnumerator();
			}

			// Token: 0x040003C0 RID: 960
			private int <>1__state;

			// Token: 0x040003C1 RID: 961
			private TResult <>2__current;

			// Token: 0x040003C2 RID: 962
			private int <>l__initialThreadId;

			// Token: 0x040003C3 RID: 963
			private IEnumerable<TSource> source;

			// Token: 0x040003C4 RID: 964
			public IEnumerable<TSource> <>3__source;

			// Token: 0x040003C5 RID: 965
			private int <index>5__1;

			// Token: 0x040003C6 RID: 966
			private Func<TSource, int, TResult> selector;

			// Token: 0x040003C7 RID: 967
			public Func<TSource, int, TResult> <>3__selector;

			// Token: 0x040003C8 RID: 968
			private IEnumerator<TSource> <>7__wrap1;
		}

		// Token: 0x020000A1 RID: 161
		[CompilerGenerated]
		private sealed class <>c__DisplayClass6_0<TSource>
		{
			// Token: 0x0600063F RID: 1599 RVA: 0x00002310 File Offset: 0x00000510
			public <>c__DisplayClass6_0()
			{
			}

			// Token: 0x06000640 RID: 1600 RVA: 0x00012CCB File Offset: 0x00010ECB
			internal bool <CombinePredicates>b__0(TSource x)
			{
				return this.predicate1(x) && this.predicate2(x);
			}

			// Token: 0x040003C9 RID: 969
			public Func<TSource, bool> predicate1;

			// Token: 0x040003CA RID: 970
			public Func<TSource, bool> predicate2;
		}

		// Token: 0x020000A2 RID: 162
		[CompilerGenerated]
		private sealed class <>c__DisplayClass7_0<TSource, TMiddle, TResult>
		{
			// Token: 0x06000641 RID: 1601 RVA: 0x00002310 File Offset: 0x00000510
			public <>c__DisplayClass7_0()
			{
			}

			// Token: 0x06000642 RID: 1602 RVA: 0x00012CE9 File Offset: 0x00010EE9
			internal TResult <CombineSelectors>b__0(TSource x)
			{
				return this.selector2(this.selector1(x));
			}

			// Token: 0x040003CB RID: 971
			public Func<TMiddle, TResult> selector2;

			// Token: 0x040003CC RID: 972
			public Func<TSource, TMiddle> selector1;
		}

		// Token: 0x020000A3 RID: 163
		[CompilerGenerated]
		private sealed class <SelectManyIterator>d__17<TSource, TResult> : IEnumerable<!1>, IEnumerable, IEnumerator<!1>, IDisposable, IEnumerator
		{
			// Token: 0x06000643 RID: 1603 RVA: 0x00012D02 File Offset: 0x00010F02
			[DebuggerHidden]
			public <SelectManyIterator>d__17(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000644 RID: 1604 RVA: 0x00012D1C File Offset: 0x00010F1C
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num - -4 <= 1 || num == 1)
				{
					try
					{
						if (num == -4 || num == 1)
						{
							try
							{
							}
							finally
							{
								this.<>m__Finally2();
							}
						}
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x06000645 RID: 1605 RVA: 0x00012D74 File Offset: 0x00010F74
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num == 0)
					{
						this.<>1__state = -1;
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
						goto IL_A4;
					}
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -4;
					IL_8A:
					if (enumerator2.MoveNext())
					{
						TResult tresult = enumerator2.Current;
						this.<>2__current = tresult;
						this.<>1__state = 1;
						return true;
					}
					this.<>m__Finally2();
					enumerator2 = null;
					IL_A4:
					if (enumerator.MoveNext())
					{
						TSource arg = enumerator.Current;
						enumerator2 = selector(arg).GetEnumerator();
						this.<>1__state = -4;
						goto IL_8A;
					}
					this.<>m__Finally1();
					enumerator = null;
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x06000646 RID: 1606 RVA: 0x00012E5C File Offset: 0x0001105C
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x06000647 RID: 1607 RVA: 0x00012E78 File Offset: 0x00011078
			private void <>m__Finally2()
			{
				this.<>1__state = -3;
				if (enumerator2 != null)
				{
					enumerator2.Dispose();
				}
			}

			// Token: 0x170000CF RID: 207
			// (get) Token: 0x06000648 RID: 1608 RVA: 0x00012E95 File Offset: 0x00011095
			TResult IEnumerator<!1>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000649 RID: 1609 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000D0 RID: 208
			// (get) Token: 0x0600064A RID: 1610 RVA: 0x00012E9D File Offset: 0x0001109D
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600064B RID: 1611 RVA: 0x00012EAC File Offset: 0x000110AC
			[DebuggerHidden]
			IEnumerator<TResult> IEnumerable<!1>.GetEnumerator()
			{
				Enumerable.<SelectManyIterator>d__17<TSource, TResult> <SelectManyIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<SelectManyIterator>d__ = this;
				}
				else
				{
					<SelectManyIterator>d__ = new Enumerable.<SelectManyIterator>d__17<TSource, TResult>(0);
				}
				<SelectManyIterator>d__.source = source;
				<SelectManyIterator>d__.selector = selector;
				return <SelectManyIterator>d__;
			}

			// Token: 0x0600064C RID: 1612 RVA: 0x00012EFB File Offset: 0x000110FB
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TResult>.GetEnumerator();
			}

			// Token: 0x040003CD RID: 973
			private int <>1__state;

			// Token: 0x040003CE RID: 974
			private TResult <>2__current;

			// Token: 0x040003CF RID: 975
			private int <>l__initialThreadId;

			// Token: 0x040003D0 RID: 976
			private IEnumerable<TSource> source;

			// Token: 0x040003D1 RID: 977
			public IEnumerable<TSource> <>3__source;

			// Token: 0x040003D2 RID: 978
			private Func<TSource, IEnumerable<TResult>> selector;

			// Token: 0x040003D3 RID: 979
			public Func<TSource, IEnumerable<TResult>> <>3__selector;

			// Token: 0x040003D4 RID: 980
			private IEnumerator<TSource> <>7__wrap1;

			// Token: 0x040003D5 RID: 981
			private IEnumerator<TResult> <>7__wrap2;
		}

		// Token: 0x020000A4 RID: 164
		[CompilerGenerated]
		private sealed class <SelectManyIterator>d__19<TSource, TResult> : IEnumerable<!1>, IEnumerable, IEnumerator<!1>, IDisposable, IEnumerator
		{
			// Token: 0x0600064D RID: 1613 RVA: 0x00012F03 File Offset: 0x00011103
			[DebuggerHidden]
			public <SelectManyIterator>d__19(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x0600064E RID: 1614 RVA: 0x00012F20 File Offset: 0x00011120
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num - -4 <= 1 || num == 1)
				{
					try
					{
						if (num == -4 || num == 1)
						{
							try
							{
							}
							finally
							{
								this.<>m__Finally2();
							}
						}
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x0600064F RID: 1615 RVA: 0x00012F78 File Offset: 0x00011178
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num == 0)
					{
						this.<>1__state = -1;
						index = -1;
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
						goto IL_C9;
					}
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -4;
					IL_AF:
					if (enumerator2.MoveNext())
					{
						TResult tresult = enumerator2.Current;
						this.<>2__current = tresult;
						this.<>1__state = 1;
						return true;
					}
					this.<>m__Finally2();
					enumerator2 = null;
					IL_C9:
					if (enumerator.MoveNext())
					{
						TSource arg = enumerator.Current;
						int num2 = index;
						index = checked(num2 + 1);
						enumerator2 = selector(arg, index).GetEnumerator();
						this.<>1__state = -4;
						goto IL_AF;
					}
					this.<>m__Finally1();
					enumerator = null;
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x06000650 RID: 1616 RVA: 0x00013088 File Offset: 0x00011288
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x06000651 RID: 1617 RVA: 0x000130A4 File Offset: 0x000112A4
			private void <>m__Finally2()
			{
				this.<>1__state = -3;
				if (enumerator2 != null)
				{
					enumerator2.Dispose();
				}
			}

			// Token: 0x170000D1 RID: 209
			// (get) Token: 0x06000652 RID: 1618 RVA: 0x000130C1 File Offset: 0x000112C1
			TResult IEnumerator<!1>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000653 RID: 1619 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000D2 RID: 210
			// (get) Token: 0x06000654 RID: 1620 RVA: 0x000130C9 File Offset: 0x000112C9
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000655 RID: 1621 RVA: 0x000130D8 File Offset: 0x000112D8
			[DebuggerHidden]
			IEnumerator<TResult> IEnumerable<!1>.GetEnumerator()
			{
				Enumerable.<SelectManyIterator>d__19<TSource, TResult> <SelectManyIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<SelectManyIterator>d__ = this;
				}
				else
				{
					<SelectManyIterator>d__ = new Enumerable.<SelectManyIterator>d__19<TSource, TResult>(0);
				}
				<SelectManyIterator>d__.source = source;
				<SelectManyIterator>d__.selector = selector;
				return <SelectManyIterator>d__;
			}

			// Token: 0x06000656 RID: 1622 RVA: 0x00013127 File Offset: 0x00011327
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TResult>.GetEnumerator();
			}

			// Token: 0x040003D6 RID: 982
			private int <>1__state;

			// Token: 0x040003D7 RID: 983
			private TResult <>2__current;

			// Token: 0x040003D8 RID: 984
			private int <>l__initialThreadId;

			// Token: 0x040003D9 RID: 985
			private IEnumerable<TSource> source;

			// Token: 0x040003DA RID: 986
			public IEnumerable<TSource> <>3__source;

			// Token: 0x040003DB RID: 987
			private int <index>5__1;

			// Token: 0x040003DC RID: 988
			private Func<TSource, int, IEnumerable<TResult>> selector;

			// Token: 0x040003DD RID: 989
			public Func<TSource, int, IEnumerable<TResult>> <>3__selector;

			// Token: 0x040003DE RID: 990
			private IEnumerator<TSource> <>7__wrap1;

			// Token: 0x040003DF RID: 991
			private IEnumerator<TResult> <>7__wrap2;
		}

		// Token: 0x020000A5 RID: 165
		[CompilerGenerated]
		private sealed class <SelectManyIterator>d__21<TSource, TCollection, TResult> : IEnumerable<!2>, IEnumerable, IEnumerator<!2>, IDisposable, IEnumerator
		{
			// Token: 0x06000657 RID: 1623 RVA: 0x0001312F File Offset: 0x0001132F
			[DebuggerHidden]
			public <SelectManyIterator>d__21(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000658 RID: 1624 RVA: 0x0001314C File Offset: 0x0001134C
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num - -4 <= 1 || num == 1)
				{
					try
					{
						if (num == -4 || num == 1)
						{
							try
							{
							}
							finally
							{
								this.<>m__Finally2();
							}
						}
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x06000659 RID: 1625 RVA: 0x000131A4 File Offset: 0x000113A4
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num == 0)
					{
						this.<>1__state = -1;
						index = -1;
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
						goto IL_EE;
					}
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -4;
					IL_C8:
					if (enumerator2.MoveNext())
					{
						TCollection arg = enumerator2.Current;
						this.<>2__current = resultSelector(element, arg);
						this.<>1__state = 1;
						return true;
					}
					this.<>m__Finally2();
					enumerator2 = null;
					element = default(TSource);
					IL_EE:
					if (enumerator.MoveNext())
					{
						element = enumerator.Current;
						int num2 = index;
						index = checked(num2 + 1);
						enumerator2 = collectionSelector(element, index).GetEnumerator();
						this.<>1__state = -4;
						goto IL_C8;
					}
					this.<>m__Finally1();
					enumerator = null;
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x0600065A RID: 1626 RVA: 0x000132E4 File Offset: 0x000114E4
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x0600065B RID: 1627 RVA: 0x00013300 File Offset: 0x00011500
			private void <>m__Finally2()
			{
				this.<>1__state = -3;
				if (enumerator2 != null)
				{
					enumerator2.Dispose();
				}
			}

			// Token: 0x170000D3 RID: 211
			// (get) Token: 0x0600065C RID: 1628 RVA: 0x0001331D File Offset: 0x0001151D
			TResult IEnumerator<!2>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600065D RID: 1629 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000D4 RID: 212
			// (get) Token: 0x0600065E RID: 1630 RVA: 0x00013325 File Offset: 0x00011525
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600065F RID: 1631 RVA: 0x00013334 File Offset: 0x00011534
			[DebuggerHidden]
			IEnumerator<TResult> IEnumerable<!2>.GetEnumerator()
			{
				Enumerable.<SelectManyIterator>d__21<TSource, TCollection, TResult> <SelectManyIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<SelectManyIterator>d__ = this;
				}
				else
				{
					<SelectManyIterator>d__ = new Enumerable.<SelectManyIterator>d__21<TSource, TCollection, TResult>(0);
				}
				<SelectManyIterator>d__.source = source;
				<SelectManyIterator>d__.collectionSelector = collectionSelector;
				<SelectManyIterator>d__.resultSelector = resultSelector;
				return <SelectManyIterator>d__;
			}

			// Token: 0x06000660 RID: 1632 RVA: 0x0001338F File Offset: 0x0001158F
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TResult>.GetEnumerator();
			}

			// Token: 0x040003E0 RID: 992
			private int <>1__state;

			// Token: 0x040003E1 RID: 993
			private TResult <>2__current;

			// Token: 0x040003E2 RID: 994
			private int <>l__initialThreadId;

			// Token: 0x040003E3 RID: 995
			private IEnumerable<TSource> source;

			// Token: 0x040003E4 RID: 996
			public IEnumerable<TSource> <>3__source;

			// Token: 0x040003E5 RID: 997
			private int <index>5__1;

			// Token: 0x040003E6 RID: 998
			private Func<TSource, int, IEnumerable<TCollection>> collectionSelector;

			// Token: 0x040003E7 RID: 999
			public Func<TSource, int, IEnumerable<TCollection>> <>3__collectionSelector;

			// Token: 0x040003E8 RID: 1000
			private Func<TSource, TCollection, TResult> resultSelector;

			// Token: 0x040003E9 RID: 1001
			public Func<TSource, TCollection, TResult> <>3__resultSelector;

			// Token: 0x040003EA RID: 1002
			private TSource <element>5__2;

			// Token: 0x040003EB RID: 1003
			private IEnumerator<TSource> <>7__wrap1;

			// Token: 0x040003EC RID: 1004
			private IEnumerator<TCollection> <>7__wrap2;
		}

		// Token: 0x020000A6 RID: 166
		[CompilerGenerated]
		private sealed class <SelectManyIterator>d__23<TSource, TCollection, TResult> : IEnumerable<!2>, IEnumerable, IEnumerator<!2>, IDisposable, IEnumerator
		{
			// Token: 0x06000661 RID: 1633 RVA: 0x00013397 File Offset: 0x00011597
			[DebuggerHidden]
			public <SelectManyIterator>d__23(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000662 RID: 1634 RVA: 0x000133B4 File Offset: 0x000115B4
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num - -4 <= 1 || num == 1)
				{
					try
					{
						if (num == -4 || num == 1)
						{
							try
							{
							}
							finally
							{
								this.<>m__Finally2();
							}
						}
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x06000663 RID: 1635 RVA: 0x0001340C File Offset: 0x0001160C
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num == 0)
					{
						this.<>1__state = -1;
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
						goto IL_D1;
					}
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -4;
					IL_AB:
					if (enumerator2.MoveNext())
					{
						TCollection arg = enumerator2.Current;
						this.<>2__current = resultSelector(element, arg);
						this.<>1__state = 1;
						return true;
					}
					this.<>m__Finally2();
					enumerator2 = null;
					element = default(TSource);
					IL_D1:
					if (enumerator.MoveNext())
					{
						element = enumerator.Current;
						enumerator2 = collectionSelector(element).GetEnumerator();
						this.<>1__state = -4;
						goto IL_AB;
					}
					this.<>m__Finally1();
					enumerator = null;
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x06000664 RID: 1636 RVA: 0x00013524 File Offset: 0x00011724
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x06000665 RID: 1637 RVA: 0x00013540 File Offset: 0x00011740
			private void <>m__Finally2()
			{
				this.<>1__state = -3;
				if (enumerator2 != null)
				{
					enumerator2.Dispose();
				}
			}

			// Token: 0x170000D5 RID: 213
			// (get) Token: 0x06000666 RID: 1638 RVA: 0x0001355D File Offset: 0x0001175D
			TResult IEnumerator<!2>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000667 RID: 1639 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000D6 RID: 214
			// (get) Token: 0x06000668 RID: 1640 RVA: 0x00013565 File Offset: 0x00011765
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000669 RID: 1641 RVA: 0x00013574 File Offset: 0x00011774
			[DebuggerHidden]
			IEnumerator<TResult> IEnumerable<!2>.GetEnumerator()
			{
				Enumerable.<SelectManyIterator>d__23<TSource, TCollection, TResult> <SelectManyIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<SelectManyIterator>d__ = this;
				}
				else
				{
					<SelectManyIterator>d__ = new Enumerable.<SelectManyIterator>d__23<TSource, TCollection, TResult>(0);
				}
				<SelectManyIterator>d__.source = source;
				<SelectManyIterator>d__.collectionSelector = collectionSelector;
				<SelectManyIterator>d__.resultSelector = resultSelector;
				return <SelectManyIterator>d__;
			}

			// Token: 0x0600066A RID: 1642 RVA: 0x000135CF File Offset: 0x000117CF
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TResult>.GetEnumerator();
			}

			// Token: 0x040003ED RID: 1005
			private int <>1__state;

			// Token: 0x040003EE RID: 1006
			private TResult <>2__current;

			// Token: 0x040003EF RID: 1007
			private int <>l__initialThreadId;

			// Token: 0x040003F0 RID: 1008
			private IEnumerable<TSource> source;

			// Token: 0x040003F1 RID: 1009
			public IEnumerable<TSource> <>3__source;

			// Token: 0x040003F2 RID: 1010
			private Func<TSource, IEnumerable<TCollection>> collectionSelector;

			// Token: 0x040003F3 RID: 1011
			public Func<TSource, IEnumerable<TCollection>> <>3__collectionSelector;

			// Token: 0x040003F4 RID: 1012
			private Func<TSource, TCollection, TResult> resultSelector;

			// Token: 0x040003F5 RID: 1013
			public Func<TSource, TCollection, TResult> <>3__resultSelector;

			// Token: 0x040003F6 RID: 1014
			private TSource <element>5__1;

			// Token: 0x040003F7 RID: 1015
			private IEnumerator<TSource> <>7__wrap1;

			// Token: 0x040003F8 RID: 1016
			private IEnumerator<TCollection> <>7__wrap2;
		}

		// Token: 0x020000A7 RID: 167
		[CompilerGenerated]
		private sealed class <TakeIterator>d__25<TSource> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x0600066B RID: 1643 RVA: 0x000135D7 File Offset: 0x000117D7
			[DebuggerHidden]
			public <TakeIterator>d__25(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x0600066C RID: 1644 RVA: 0x000135F4 File Offset: 0x000117F4
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x0600066D RID: 1645 RVA: 0x0001362C File Offset: 0x0001182C
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
						int num2 = count - 1;
						count = num2;
						if (num2 == 0)
						{
							goto IL_86;
						}
					}
					else
					{
						this.<>1__state = -1;
						if (count <= 0)
						{
							goto IL_93;
						}
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
					}
					if (enumerator.MoveNext())
					{
						TSource tsource = enumerator.Current;
						this.<>2__current = tsource;
						this.<>1__state = 1;
						return true;
					}
					IL_86:
					this.<>m__Finally1();
					enumerator = null;
					IL_93:
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x0600066E RID: 1646 RVA: 0x000136E8 File Offset: 0x000118E8
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x170000D7 RID: 215
			// (get) Token: 0x0600066F RID: 1647 RVA: 0x00013704 File Offset: 0x00011904
			TSource IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000670 RID: 1648 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000D8 RID: 216
			// (get) Token: 0x06000671 RID: 1649 RVA: 0x0001370C File Offset: 0x0001190C
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000672 RID: 1650 RVA: 0x0001371C File Offset: 0x0001191C
			[DebuggerHidden]
			IEnumerator<TSource> IEnumerable<!0>.GetEnumerator()
			{
				Enumerable.<TakeIterator>d__25<TSource> <TakeIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<TakeIterator>d__ = this;
				}
				else
				{
					<TakeIterator>d__ = new Enumerable.<TakeIterator>d__25<TSource>(0);
				}
				<TakeIterator>d__.source = source;
				<TakeIterator>d__.count = count;
				return <TakeIterator>d__;
			}

			// Token: 0x06000673 RID: 1651 RVA: 0x0001376B File Offset: 0x0001196B
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TSource>.GetEnumerator();
			}

			// Token: 0x040003F9 RID: 1017
			private int <>1__state;

			// Token: 0x040003FA RID: 1018
			private TSource <>2__current;

			// Token: 0x040003FB RID: 1019
			private int <>l__initialThreadId;

			// Token: 0x040003FC RID: 1020
			private int count;

			// Token: 0x040003FD RID: 1021
			public int <>3__count;

			// Token: 0x040003FE RID: 1022
			private IEnumerable<TSource> source;

			// Token: 0x040003FF RID: 1023
			public IEnumerable<TSource> <>3__source;

			// Token: 0x04000400 RID: 1024
			private IEnumerator<TSource> <>7__wrap1;
		}

		// Token: 0x020000A8 RID: 168
		[CompilerGenerated]
		private sealed class <TakeWhileIterator>d__27<TSource> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x06000674 RID: 1652 RVA: 0x00013773 File Offset: 0x00011973
			[DebuggerHidden]
			public <TakeWhileIterator>d__27(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000675 RID: 1653 RVA: 0x00013790 File Offset: 0x00011990
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x06000676 RID: 1654 RVA: 0x000137C8 File Offset: 0x000119C8
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
					}
					if (enumerator.MoveNext())
					{
						TSource arg = enumerator.Current;
						if (predicate(arg))
						{
							this.<>2__current = arg;
							this.<>1__state = 1;
							return true;
						}
					}
					this.<>m__Finally1();
					enumerator = null;
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x06000677 RID: 1655 RVA: 0x00013874 File Offset: 0x00011A74
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x170000D9 RID: 217
			// (get) Token: 0x06000678 RID: 1656 RVA: 0x00013890 File Offset: 0x00011A90
			TSource IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000679 RID: 1657 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000DA RID: 218
			// (get) Token: 0x0600067A RID: 1658 RVA: 0x00013898 File Offset: 0x00011A98
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600067B RID: 1659 RVA: 0x000138A8 File Offset: 0x00011AA8
			[DebuggerHidden]
			IEnumerator<TSource> IEnumerable<!0>.GetEnumerator()
			{
				Enumerable.<TakeWhileIterator>d__27<TSource> <TakeWhileIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<TakeWhileIterator>d__ = this;
				}
				else
				{
					<TakeWhileIterator>d__ = new Enumerable.<TakeWhileIterator>d__27<TSource>(0);
				}
				<TakeWhileIterator>d__.source = source;
				<TakeWhileIterator>d__.predicate = predicate;
				return <TakeWhileIterator>d__;
			}

			// Token: 0x0600067C RID: 1660 RVA: 0x000138F7 File Offset: 0x00011AF7
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TSource>.GetEnumerator();
			}

			// Token: 0x04000401 RID: 1025
			private int <>1__state;

			// Token: 0x04000402 RID: 1026
			private TSource <>2__current;

			// Token: 0x04000403 RID: 1027
			private int <>l__initialThreadId;

			// Token: 0x04000404 RID: 1028
			private IEnumerable<TSource> source;

			// Token: 0x04000405 RID: 1029
			public IEnumerable<TSource> <>3__source;

			// Token: 0x04000406 RID: 1030
			private Func<TSource, bool> predicate;

			// Token: 0x04000407 RID: 1031
			public Func<TSource, bool> <>3__predicate;

			// Token: 0x04000408 RID: 1032
			private IEnumerator<TSource> <>7__wrap1;
		}

		// Token: 0x020000A9 RID: 169
		[CompilerGenerated]
		private sealed class <TakeWhileIterator>d__29<TSource> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x0600067D RID: 1661 RVA: 0x000138FF File Offset: 0x00011AFF
			[DebuggerHidden]
			public <TakeWhileIterator>d__29(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x0600067E RID: 1662 RVA: 0x0001391C File Offset: 0x00011B1C
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x0600067F RID: 1663 RVA: 0x00013954 File Offset: 0x00011B54
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						index = -1;
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
					}
					if (enumerator.MoveNext())
					{
						TSource arg = enumerator.Current;
						int num2 = index;
						index = checked(num2 + 1);
						if (predicate(arg, index))
						{
							this.<>2__current = arg;
							this.<>1__state = 1;
							return true;
						}
					}
					this.<>m__Finally1();
					enumerator = null;
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x06000680 RID: 1664 RVA: 0x00013A20 File Offset: 0x00011C20
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x170000DB RID: 219
			// (get) Token: 0x06000681 RID: 1665 RVA: 0x00013A3C File Offset: 0x00011C3C
			TSource IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000682 RID: 1666 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000DC RID: 220
			// (get) Token: 0x06000683 RID: 1667 RVA: 0x00013A44 File Offset: 0x00011C44
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000684 RID: 1668 RVA: 0x00013A54 File Offset: 0x00011C54
			[DebuggerHidden]
			IEnumerator<TSource> IEnumerable<!0>.GetEnumerator()
			{
				Enumerable.<TakeWhileIterator>d__29<TSource> <TakeWhileIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<TakeWhileIterator>d__ = this;
				}
				else
				{
					<TakeWhileIterator>d__ = new Enumerable.<TakeWhileIterator>d__29<TSource>(0);
				}
				<TakeWhileIterator>d__.source = source;
				<TakeWhileIterator>d__.predicate = predicate;
				return <TakeWhileIterator>d__;
			}

			// Token: 0x06000685 RID: 1669 RVA: 0x00013AA3 File Offset: 0x00011CA3
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TSource>.GetEnumerator();
			}

			// Token: 0x04000409 RID: 1033
			private int <>1__state;

			// Token: 0x0400040A RID: 1034
			private TSource <>2__current;

			// Token: 0x0400040B RID: 1035
			private int <>l__initialThreadId;

			// Token: 0x0400040C RID: 1036
			private IEnumerable<TSource> source;

			// Token: 0x0400040D RID: 1037
			public IEnumerable<TSource> <>3__source;

			// Token: 0x0400040E RID: 1038
			private int <index>5__1;

			// Token: 0x0400040F RID: 1039
			private Func<TSource, int, bool> predicate;

			// Token: 0x04000410 RID: 1040
			public Func<TSource, int, bool> <>3__predicate;

			// Token: 0x04000411 RID: 1041
			private IEnumerator<TSource> <>7__wrap1;
		}

		// Token: 0x020000AA RID: 170
		[CompilerGenerated]
		private sealed class <SkipIterator>d__31<TSource> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x06000686 RID: 1670 RVA: 0x00013AAB File Offset: 0x00011CAB
			[DebuggerHidden]
			public <SkipIterator>d__31(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000687 RID: 1671 RVA: 0x00013AC8 File Offset: 0x00011CC8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x06000688 RID: 1672 RVA: 0x00013B00 File Offset: 0x00011D00
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						e = source.GetEnumerator();
						this.<>1__state = -3;
						while (count > 0 && e.MoveNext())
						{
							int num2 = count;
							count = num2 - 1;
						}
						if (count > 0)
						{
							goto IL_99;
						}
					}
					if (e.MoveNext())
					{
						this.<>2__current = e.Current;
						this.<>1__state = 1;
						return true;
					}
					IL_99:
					this.<>m__Finally1();
					e = null;
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x06000689 RID: 1673 RVA: 0x00013BD0 File Offset: 0x00011DD0
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (e != null)
				{
					e.Dispose();
				}
			}

			// Token: 0x170000DD RID: 221
			// (get) Token: 0x0600068A RID: 1674 RVA: 0x00013BEC File Offset: 0x00011DEC
			TSource IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600068B RID: 1675 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000DE RID: 222
			// (get) Token: 0x0600068C RID: 1676 RVA: 0x00013BF4 File Offset: 0x00011DF4
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600068D RID: 1677 RVA: 0x00013C04 File Offset: 0x00011E04
			[DebuggerHidden]
			IEnumerator<TSource> IEnumerable<!0>.GetEnumerator()
			{
				Enumerable.<SkipIterator>d__31<TSource> <SkipIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<SkipIterator>d__ = this;
				}
				else
				{
					<SkipIterator>d__ = new Enumerable.<SkipIterator>d__31<TSource>(0);
				}
				<SkipIterator>d__.source = source;
				<SkipIterator>d__.count = count;
				return <SkipIterator>d__;
			}

			// Token: 0x0600068E RID: 1678 RVA: 0x00013C53 File Offset: 0x00011E53
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TSource>.GetEnumerator();
			}

			// Token: 0x04000412 RID: 1042
			private int <>1__state;

			// Token: 0x04000413 RID: 1043
			private TSource <>2__current;

			// Token: 0x04000414 RID: 1044
			private int <>l__initialThreadId;

			// Token: 0x04000415 RID: 1045
			private IEnumerable<TSource> source;

			// Token: 0x04000416 RID: 1046
			public IEnumerable<TSource> <>3__source;

			// Token: 0x04000417 RID: 1047
			private int count;

			// Token: 0x04000418 RID: 1048
			public int <>3__count;

			// Token: 0x04000419 RID: 1049
			private IEnumerator<TSource> <e>5__1;
		}

		// Token: 0x020000AB RID: 171
		[CompilerGenerated]
		private sealed class <SkipWhileIterator>d__33<TSource> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x0600068F RID: 1679 RVA: 0x00013C5B File Offset: 0x00011E5B
			[DebuggerHidden]
			public <SkipWhileIterator>d__33(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000690 RID: 1680 RVA: 0x00013C78 File Offset: 0x00011E78
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x06000691 RID: 1681 RVA: 0x00013CB0 File Offset: 0x00011EB0
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						yielding = false;
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
					}
					while (enumerator.MoveNext())
					{
						TSource arg = enumerator.Current;
						if (!yielding && !predicate(arg))
						{
							yielding = true;
						}
						if (yielding)
						{
							this.<>2__current = arg;
							this.<>1__state = 1;
							return true;
						}
					}
					this.<>m__Finally1();
					enumerator = null;
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x06000692 RID: 1682 RVA: 0x00013D7C File Offset: 0x00011F7C
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x170000DF RID: 223
			// (get) Token: 0x06000693 RID: 1683 RVA: 0x00013D98 File Offset: 0x00011F98
			TSource IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000694 RID: 1684 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000E0 RID: 224
			// (get) Token: 0x06000695 RID: 1685 RVA: 0x00013DA0 File Offset: 0x00011FA0
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000696 RID: 1686 RVA: 0x00013DB0 File Offset: 0x00011FB0
			[DebuggerHidden]
			IEnumerator<TSource> IEnumerable<!0>.GetEnumerator()
			{
				Enumerable.<SkipWhileIterator>d__33<TSource> <SkipWhileIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<SkipWhileIterator>d__ = this;
				}
				else
				{
					<SkipWhileIterator>d__ = new Enumerable.<SkipWhileIterator>d__33<TSource>(0);
				}
				<SkipWhileIterator>d__.source = source;
				<SkipWhileIterator>d__.predicate = predicate;
				return <SkipWhileIterator>d__;
			}

			// Token: 0x06000697 RID: 1687 RVA: 0x00013DFF File Offset: 0x00011FFF
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TSource>.GetEnumerator();
			}

			// Token: 0x0400041A RID: 1050
			private int <>1__state;

			// Token: 0x0400041B RID: 1051
			private TSource <>2__current;

			// Token: 0x0400041C RID: 1052
			private int <>l__initialThreadId;

			// Token: 0x0400041D RID: 1053
			private IEnumerable<TSource> source;

			// Token: 0x0400041E RID: 1054
			public IEnumerable<TSource> <>3__source;

			// Token: 0x0400041F RID: 1055
			private bool <yielding>5__1;

			// Token: 0x04000420 RID: 1056
			private Func<TSource, bool> predicate;

			// Token: 0x04000421 RID: 1057
			public Func<TSource, bool> <>3__predicate;

			// Token: 0x04000422 RID: 1058
			private IEnumerator<TSource> <>7__wrap1;
		}

		// Token: 0x020000AC RID: 172
		[CompilerGenerated]
		private sealed class <SkipWhileIterator>d__35<TSource> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x06000698 RID: 1688 RVA: 0x00013E07 File Offset: 0x00012007
			[DebuggerHidden]
			public <SkipWhileIterator>d__35(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000699 RID: 1689 RVA: 0x00013E24 File Offset: 0x00012024
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x0600069A RID: 1690 RVA: 0x00013E5C File Offset: 0x0001205C
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						index = -1;
						yielding = false;
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
					}
					while (enumerator.MoveNext())
					{
						TSource arg = enumerator.Current;
						int num2 = index;
						index = checked(num2 + 1);
						if (!yielding && !predicate(arg, index))
						{
							yielding = true;
						}
						if (yielding)
						{
							this.<>2__current = arg;
							this.<>1__state = 1;
							return true;
						}
					}
					this.<>m__Finally1();
					enumerator = null;
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x0600069B RID: 1691 RVA: 0x00013F48 File Offset: 0x00012148
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x170000E1 RID: 225
			// (get) Token: 0x0600069C RID: 1692 RVA: 0x00013F64 File Offset: 0x00012164
			TSource IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600069D RID: 1693 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000E2 RID: 226
			// (get) Token: 0x0600069E RID: 1694 RVA: 0x00013F6C File Offset: 0x0001216C
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600069F RID: 1695 RVA: 0x00013F7C File Offset: 0x0001217C
			[DebuggerHidden]
			IEnumerator<TSource> IEnumerable<!0>.GetEnumerator()
			{
				Enumerable.<SkipWhileIterator>d__35<TSource> <SkipWhileIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<SkipWhileIterator>d__ = this;
				}
				else
				{
					<SkipWhileIterator>d__ = new Enumerable.<SkipWhileIterator>d__35<TSource>(0);
				}
				<SkipWhileIterator>d__.source = source;
				<SkipWhileIterator>d__.predicate = predicate;
				return <SkipWhileIterator>d__;
			}

			// Token: 0x060006A0 RID: 1696 RVA: 0x00013FCB File Offset: 0x000121CB
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TSource>.GetEnumerator();
			}

			// Token: 0x04000423 RID: 1059
			private int <>1__state;

			// Token: 0x04000424 RID: 1060
			private TSource <>2__current;

			// Token: 0x04000425 RID: 1061
			private int <>l__initialThreadId;

			// Token: 0x04000426 RID: 1062
			private IEnumerable<TSource> source;

			// Token: 0x04000427 RID: 1063
			public IEnumerable<TSource> <>3__source;

			// Token: 0x04000428 RID: 1064
			private int <index>5__1;

			// Token: 0x04000429 RID: 1065
			private bool <yielding>5__2;

			// Token: 0x0400042A RID: 1066
			private Func<TSource, int, bool> predicate;

			// Token: 0x0400042B RID: 1067
			public Func<TSource, int, bool> <>3__predicate;

			// Token: 0x0400042C RID: 1068
			private IEnumerator<TSource> <>7__wrap1;
		}

		// Token: 0x020000AD RID: 173
		[CompilerGenerated]
		private sealed class <JoinIterator>d__38<TOuter, TInner, TKey, TResult> : IEnumerable<TResult>, IEnumerable, IEnumerator<TResult>, IDisposable, IEnumerator
		{
			// Token: 0x060006A1 RID: 1697 RVA: 0x00013FD3 File Offset: 0x000121D3
			[DebuggerHidden]
			public <JoinIterator>d__38(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060006A2 RID: 1698 RVA: 0x00013FF0 File Offset: 0x000121F0
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x060006A3 RID: 1699 RVA: 0x00014028 File Offset: 0x00012228
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num == 0)
					{
						this.<>1__state = -1;
						lookup = Lookup<TKey, TInner>.CreateForJoin(inner, innerKeySelector, comparer);
						enumerator = outer.GetEnumerator();
						this.<>1__state = -3;
						goto IL_115;
					}
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -3;
					int num2 = i;
					i = num2 + 1;
					IL_EF:
					if (i < g.count)
					{
						this.<>2__current = resultSelector(item, g.elements[i]);
						this.<>1__state = 1;
						return true;
					}
					IL_102:
					g = null;
					item = default(TOuter);
					IL_115:
					if (!enumerator.MoveNext())
					{
						this.<>m__Finally1();
						enumerator = null;
						result = false;
					}
					else
					{
						item = enumerator.Current;
						g = lookup.GetGrouping(outerKeySelector(item), false);
						if (g != null)
						{
							i = 0;
							goto IL_EF;
						}
						goto IL_102;
					}
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x060006A4 RID: 1700 RVA: 0x00014190 File Offset: 0x00012390
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x170000E3 RID: 227
			// (get) Token: 0x060006A5 RID: 1701 RVA: 0x000141AC File Offset: 0x000123AC
			TResult IEnumerator<!3>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060006A6 RID: 1702 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000E4 RID: 228
			// (get) Token: 0x060006A7 RID: 1703 RVA: 0x000141B4 File Offset: 0x000123B4
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060006A8 RID: 1704 RVA: 0x000141C4 File Offset: 0x000123C4
			[DebuggerHidden]
			IEnumerator<TResult> IEnumerable<!3>.GetEnumerator()
			{
				Enumerable.<JoinIterator>d__38<TOuter, TInner, TKey, TResult> <JoinIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<JoinIterator>d__ = this;
				}
				else
				{
					<JoinIterator>d__ = new Enumerable.<JoinIterator>d__38<TOuter, TInner, TKey, TResult>(0);
				}
				<JoinIterator>d__.outer = outer;
				<JoinIterator>d__.inner = inner;
				<JoinIterator>d__.outerKeySelector = outerKeySelector;
				<JoinIterator>d__.innerKeySelector = innerKeySelector;
				<JoinIterator>d__.resultSelector = resultSelector;
				<JoinIterator>d__.comparer = comparer;
				return <JoinIterator>d__;
			}

			// Token: 0x060006A9 RID: 1705 RVA: 0x00014243 File Offset: 0x00012443
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TResult>.GetEnumerator();
			}

			// Token: 0x0400042D RID: 1069
			private int <>1__state;

			// Token: 0x0400042E RID: 1070
			private TResult <>2__current;

			// Token: 0x0400042F RID: 1071
			private int <>l__initialThreadId;

			// Token: 0x04000430 RID: 1072
			private IEnumerable<TInner> inner;

			// Token: 0x04000431 RID: 1073
			public IEnumerable<TInner> <>3__inner;

			// Token: 0x04000432 RID: 1074
			private Func<TInner, TKey> innerKeySelector;

			// Token: 0x04000433 RID: 1075
			public Func<TInner, TKey> <>3__innerKeySelector;

			// Token: 0x04000434 RID: 1076
			private IEqualityComparer<TKey> comparer;

			// Token: 0x04000435 RID: 1077
			public IEqualityComparer<TKey> <>3__comparer;

			// Token: 0x04000436 RID: 1078
			private IEnumerable<TOuter> outer;

			// Token: 0x04000437 RID: 1079
			public IEnumerable<TOuter> <>3__outer;

			// Token: 0x04000438 RID: 1080
			private Lookup<TKey, TInner> <lookup>5__1;

			// Token: 0x04000439 RID: 1081
			private Func<TOuter, TKey> outerKeySelector;

			// Token: 0x0400043A RID: 1082
			public Func<TOuter, TKey> <>3__outerKeySelector;

			// Token: 0x0400043B RID: 1083
			private Func<TOuter, TInner, TResult> resultSelector;

			// Token: 0x0400043C RID: 1084
			public Func<TOuter, TInner, TResult> <>3__resultSelector;

			// Token: 0x0400043D RID: 1085
			private TOuter <item>5__2;

			// Token: 0x0400043E RID: 1086
			private Lookup<TKey, TInner>.Grouping <g>5__3;

			// Token: 0x0400043F RID: 1087
			private int <i>5__4;

			// Token: 0x04000440 RID: 1088
			private IEnumerator<TOuter> <>7__wrap1;
		}

		// Token: 0x020000AE RID: 174
		[CompilerGenerated]
		private sealed class <GroupJoinIterator>d__41<TOuter, TInner, TKey, TResult> : IEnumerable<!3>, IEnumerable, IEnumerator<!3>, IDisposable, IEnumerator
		{
			// Token: 0x060006AA RID: 1706 RVA: 0x0001424B File Offset: 0x0001244B
			[DebuggerHidden]
			public <GroupJoinIterator>d__41(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060006AB RID: 1707 RVA: 0x00014268 File Offset: 0x00012468
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x060006AC RID: 1708 RVA: 0x000142A0 File Offset: 0x000124A0
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						lookup = Lookup<TKey, TInner>.CreateForJoin(inner, innerKeySelector, comparer);
						enumerator = outer.GetEnumerator();
						this.<>1__state = -3;
					}
					if (!enumerator.MoveNext())
					{
						this.<>m__Finally1();
						enumerator = null;
						result = false;
					}
					else
					{
						TOuter touter = enumerator.Current;
						this.<>2__current = resultSelector(touter, lookup[outerKeySelector(touter)]);
						this.<>1__state = 1;
						result = true;
					}
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x060006AD RID: 1709 RVA: 0x00014384 File Offset: 0x00012584
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x170000E5 RID: 229
			// (get) Token: 0x060006AE RID: 1710 RVA: 0x000143A0 File Offset: 0x000125A0
			TResult IEnumerator<!3>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060006AF RID: 1711 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000E6 RID: 230
			// (get) Token: 0x060006B0 RID: 1712 RVA: 0x000143A8 File Offset: 0x000125A8
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060006B1 RID: 1713 RVA: 0x000143B8 File Offset: 0x000125B8
			[DebuggerHidden]
			IEnumerator<TResult> IEnumerable<!3>.GetEnumerator()
			{
				Enumerable.<GroupJoinIterator>d__41<TOuter, TInner, TKey, TResult> <GroupJoinIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<GroupJoinIterator>d__ = this;
				}
				else
				{
					<GroupJoinIterator>d__ = new Enumerable.<GroupJoinIterator>d__41<TOuter, TInner, TKey, TResult>(0);
				}
				<GroupJoinIterator>d__.outer = outer;
				<GroupJoinIterator>d__.inner = inner;
				<GroupJoinIterator>d__.outerKeySelector = outerKeySelector;
				<GroupJoinIterator>d__.innerKeySelector = innerKeySelector;
				<GroupJoinIterator>d__.resultSelector = resultSelector;
				<GroupJoinIterator>d__.comparer = comparer;
				return <GroupJoinIterator>d__;
			}

			// Token: 0x060006B2 RID: 1714 RVA: 0x00014437 File Offset: 0x00012637
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TResult>.GetEnumerator();
			}

			// Token: 0x04000441 RID: 1089
			private int <>1__state;

			// Token: 0x04000442 RID: 1090
			private TResult <>2__current;

			// Token: 0x04000443 RID: 1091
			private int <>l__initialThreadId;

			// Token: 0x04000444 RID: 1092
			private IEnumerable<TInner> inner;

			// Token: 0x04000445 RID: 1093
			public IEnumerable<TInner> <>3__inner;

			// Token: 0x04000446 RID: 1094
			private Func<TInner, TKey> innerKeySelector;

			// Token: 0x04000447 RID: 1095
			public Func<TInner, TKey> <>3__innerKeySelector;

			// Token: 0x04000448 RID: 1096
			private IEqualityComparer<TKey> comparer;

			// Token: 0x04000449 RID: 1097
			public IEqualityComparer<TKey> <>3__comparer;

			// Token: 0x0400044A RID: 1098
			private IEnumerable<TOuter> outer;

			// Token: 0x0400044B RID: 1099
			public IEnumerable<TOuter> <>3__outer;

			// Token: 0x0400044C RID: 1100
			private Func<TOuter, IEnumerable<TInner>, TResult> resultSelector;

			// Token: 0x0400044D RID: 1101
			public Func<TOuter, IEnumerable<TInner>, TResult> <>3__resultSelector;

			// Token: 0x0400044E RID: 1102
			private Lookup<TKey, TInner> <lookup>5__1;

			// Token: 0x0400044F RID: 1103
			private Func<TOuter, TKey> outerKeySelector;

			// Token: 0x04000450 RID: 1104
			public Func<TOuter, TKey> <>3__outerKeySelector;

			// Token: 0x04000451 RID: 1105
			private IEnumerator<TOuter> <>7__wrap1;
		}

		// Token: 0x020000AF RID: 175
		[CompilerGenerated]
		private sealed class <ConcatIterator>d__59<TSource> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x060006B3 RID: 1715 RVA: 0x0001443F File Offset: 0x0001263F
			[DebuggerHidden]
			public <ConcatIterator>d__59(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060006B4 RID: 1716 RVA: 0x0001445C File Offset: 0x0001265C
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				switch (this.<>1__state)
				{
				case -4:
				case 2:
					break;
				case -3:
				case 1:
					try
					{
						return;
					}
					finally
					{
						this.<>m__Finally1();
					}
					break;
				case -2:
				case -1:
				case 0:
					return;
				default:
					return;
				}
				try
				{
				}
				finally
				{
					this.<>m__Finally2();
				}
			}

			// Token: 0x060006B5 RID: 1717 RVA: 0x000144C8 File Offset: 0x000126C8
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					switch (this.<>1__state)
					{
					case 0:
						this.<>1__state = -1;
						enumerator = first.GetEnumerator();
						this.<>1__state = -3;
						break;
					case 1:
						this.<>1__state = -3;
						break;
					case 2:
						this.<>1__state = -4;
						goto IL_C6;
					default:
						return false;
					}
					if (enumerator.MoveNext())
					{
						TSource tsource = enumerator.Current;
						this.<>2__current = tsource;
						this.<>1__state = 1;
						return true;
					}
					this.<>m__Finally1();
					enumerator = null;
					enumerator = second.GetEnumerator();
					this.<>1__state = -4;
					IL_C6:
					if (!enumerator.MoveNext())
					{
						this.<>m__Finally2();
						enumerator = null;
						result = false;
					}
					else
					{
						TSource tsource2 = enumerator.Current;
						this.<>2__current = tsource2;
						this.<>1__state = 2;
						result = true;
					}
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x060006B6 RID: 1718 RVA: 0x000145D4 File Offset: 0x000127D4
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x060006B7 RID: 1719 RVA: 0x000145D4 File Offset: 0x000127D4
			private void <>m__Finally2()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x170000E7 RID: 231
			// (get) Token: 0x060006B8 RID: 1720 RVA: 0x000145F0 File Offset: 0x000127F0
			TSource IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060006B9 RID: 1721 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000E8 RID: 232
			// (get) Token: 0x060006BA RID: 1722 RVA: 0x000145F8 File Offset: 0x000127F8
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060006BB RID: 1723 RVA: 0x00014608 File Offset: 0x00012808
			[DebuggerHidden]
			IEnumerator<TSource> IEnumerable<!0>.GetEnumerator()
			{
				Enumerable.<ConcatIterator>d__59<TSource> <ConcatIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<ConcatIterator>d__ = this;
				}
				else
				{
					<ConcatIterator>d__ = new Enumerable.<ConcatIterator>d__59<TSource>(0);
				}
				<ConcatIterator>d__.first = first;
				<ConcatIterator>d__.second = second;
				return <ConcatIterator>d__;
			}

			// Token: 0x060006BC RID: 1724 RVA: 0x00014657 File Offset: 0x00012857
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TSource>.GetEnumerator();
			}

			// Token: 0x04000452 RID: 1106
			private int <>1__state;

			// Token: 0x04000453 RID: 1107
			private TSource <>2__current;

			// Token: 0x04000454 RID: 1108
			private int <>l__initialThreadId;

			// Token: 0x04000455 RID: 1109
			private IEnumerable<TSource> first;

			// Token: 0x04000456 RID: 1110
			public IEnumerable<TSource> <>3__first;

			// Token: 0x04000457 RID: 1111
			private IEnumerable<TSource> second;

			// Token: 0x04000458 RID: 1112
			public IEnumerable<TSource> <>3__second;

			// Token: 0x04000459 RID: 1113
			private IEnumerator<TSource> <>7__wrap1;
		}

		// Token: 0x020000B0 RID: 176
		[CompilerGenerated]
		private sealed class <AppendIterator>d__61<TSource> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x060006BD RID: 1725 RVA: 0x0001465F File Offset: 0x0001285F
			[DebuggerHidden]
			public <AppendIterator>d__61(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060006BE RID: 1726 RVA: 0x0001467C File Offset: 0x0001287C
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x060006BF RID: 1727 RVA: 0x000146B4 File Offset: 0x000128B4
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					switch (this.<>1__state)
					{
					case 0:
						this.<>1__state = -1;
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
						break;
					case 1:
						this.<>1__state = -3;
						break;
					case 2:
						this.<>1__state = -1;
						return false;
					default:
						return false;
					}
					if (!enumerator.MoveNext())
					{
						this.<>m__Finally1();
						enumerator = null;
						this.<>2__current = element;
						this.<>1__state = 2;
						result = true;
					}
					else
					{
						TSource tsource = enumerator.Current;
						this.<>2__current = tsource;
						this.<>1__state = 1;
						result = true;
					}
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x060006C0 RID: 1728 RVA: 0x00014780 File Offset: 0x00012980
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x170000E9 RID: 233
			// (get) Token: 0x060006C1 RID: 1729 RVA: 0x0001479C File Offset: 0x0001299C
			TSource IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060006C2 RID: 1730 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000EA RID: 234
			// (get) Token: 0x060006C3 RID: 1731 RVA: 0x000147A4 File Offset: 0x000129A4
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060006C4 RID: 1732 RVA: 0x000147B4 File Offset: 0x000129B4
			[DebuggerHidden]
			IEnumerator<TSource> IEnumerable<!0>.GetEnumerator()
			{
				Enumerable.<AppendIterator>d__61<TSource> <AppendIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<AppendIterator>d__ = this;
				}
				else
				{
					<AppendIterator>d__ = new Enumerable.<AppendIterator>d__61<TSource>(0);
				}
				<AppendIterator>d__.source = source;
				<AppendIterator>d__.element = element;
				return <AppendIterator>d__;
			}

			// Token: 0x060006C5 RID: 1733 RVA: 0x00014803 File Offset: 0x00012A03
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TSource>.GetEnumerator();
			}

			// Token: 0x0400045A RID: 1114
			private int <>1__state;

			// Token: 0x0400045B RID: 1115
			private TSource <>2__current;

			// Token: 0x0400045C RID: 1116
			private int <>l__initialThreadId;

			// Token: 0x0400045D RID: 1117
			private IEnumerable<TSource> source;

			// Token: 0x0400045E RID: 1118
			public IEnumerable<TSource> <>3__source;

			// Token: 0x0400045F RID: 1119
			private TSource element;

			// Token: 0x04000460 RID: 1120
			public TSource <>3__element;

			// Token: 0x04000461 RID: 1121
			private IEnumerator<TSource> <>7__wrap1;
		}

		// Token: 0x020000B1 RID: 177
		[CompilerGenerated]
		private sealed class <PrependIterator>d__63<TSource> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x060006C6 RID: 1734 RVA: 0x0001480B File Offset: 0x00012A0B
			[DebuggerHidden]
			public <PrependIterator>d__63(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060006C7 RID: 1735 RVA: 0x00014828 File Offset: 0x00012A28
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 2)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x060006C8 RID: 1736 RVA: 0x00014860 File Offset: 0x00012A60
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					switch (this.<>1__state)
					{
					case 0:
						this.<>1__state = -1;
						this.<>2__current = element;
						this.<>1__state = 1;
						return true;
					case 1:
						this.<>1__state = -1;
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
						break;
					case 2:
						this.<>1__state = -3;
						break;
					default:
						return false;
					}
					if (!enumerator.MoveNext())
					{
						this.<>m__Finally1();
						enumerator = null;
						result = false;
					}
					else
					{
						TSource tsource = enumerator.Current;
						this.<>2__current = tsource;
						this.<>1__state = 2;
						result = true;
					}
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x060006C9 RID: 1737 RVA: 0x0001492C File Offset: 0x00012B2C
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x170000EB RID: 235
			// (get) Token: 0x060006CA RID: 1738 RVA: 0x00014948 File Offset: 0x00012B48
			TSource IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060006CB RID: 1739 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000EC RID: 236
			// (get) Token: 0x060006CC RID: 1740 RVA: 0x00014950 File Offset: 0x00012B50
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060006CD RID: 1741 RVA: 0x00014960 File Offset: 0x00012B60
			[DebuggerHidden]
			IEnumerator<TSource> IEnumerable<!0>.GetEnumerator()
			{
				Enumerable.<PrependIterator>d__63<TSource> <PrependIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<PrependIterator>d__ = this;
				}
				else
				{
					<PrependIterator>d__ = new Enumerable.<PrependIterator>d__63<TSource>(0);
				}
				<PrependIterator>d__.source = source;
				<PrependIterator>d__.element = element;
				return <PrependIterator>d__;
			}

			// Token: 0x060006CE RID: 1742 RVA: 0x000149AF File Offset: 0x00012BAF
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TSource>.GetEnumerator();
			}

			// Token: 0x04000462 RID: 1122
			private int <>1__state;

			// Token: 0x04000463 RID: 1123
			private TSource <>2__current;

			// Token: 0x04000464 RID: 1124
			private int <>l__initialThreadId;

			// Token: 0x04000465 RID: 1125
			private TSource element;

			// Token: 0x04000466 RID: 1126
			public TSource <>3__element;

			// Token: 0x04000467 RID: 1127
			private IEnumerable<TSource> source;

			// Token: 0x04000468 RID: 1128
			public IEnumerable<TSource> <>3__source;

			// Token: 0x04000469 RID: 1129
			private IEnumerator<TSource> <>7__wrap1;
		}

		// Token: 0x020000B2 RID: 178
		[CompilerGenerated]
		private sealed class <ZipIterator>d__65<TFirst, TSecond, TResult> : IEnumerable<!2>, IEnumerable, IEnumerator<!2>, IDisposable, IEnumerator
		{
			// Token: 0x060006CF RID: 1743 RVA: 0x000149B7 File Offset: 0x00012BB7
			[DebuggerHidden]
			public <ZipIterator>d__65(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060006D0 RID: 1744 RVA: 0x000149D4 File Offset: 0x00012BD4
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num - -4 <= 1 || num == 1)
				{
					try
					{
						if (num == -4 || num == 1)
						{
							try
							{
							}
							finally
							{
								this.<>m__Finally2();
							}
						}
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x060006D1 RID: 1745 RVA: 0x00014A2C File Offset: 0x00012C2C
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -4;
					}
					else
					{
						this.<>1__state = -1;
						e = first.GetEnumerator();
						this.<>1__state = -3;
						e2 = second.GetEnumerator();
						this.<>1__state = -4;
					}
					if (!e.MoveNext() || !e2.MoveNext())
					{
						this.<>m__Finally2();
						e2 = null;
						this.<>m__Finally1();
						e = null;
						result = false;
					}
					else
					{
						this.<>2__current = resultSelector(e.Current, e2.Current);
						this.<>1__state = 1;
						result = true;
					}
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x060006D2 RID: 1746 RVA: 0x00014B14 File Offset: 0x00012D14
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (e != null)
				{
					e.Dispose();
				}
			}

			// Token: 0x060006D3 RID: 1747 RVA: 0x00014B30 File Offset: 0x00012D30
			private void <>m__Finally2()
			{
				this.<>1__state = -3;
				if (e2 != null)
				{
					e2.Dispose();
				}
			}

			// Token: 0x170000ED RID: 237
			// (get) Token: 0x060006D4 RID: 1748 RVA: 0x00014B4D File Offset: 0x00012D4D
			TResult IEnumerator<!2>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060006D5 RID: 1749 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000EE RID: 238
			// (get) Token: 0x060006D6 RID: 1750 RVA: 0x00014B55 File Offset: 0x00012D55
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060006D7 RID: 1751 RVA: 0x00014B64 File Offset: 0x00012D64
			[DebuggerHidden]
			IEnumerator<TResult> IEnumerable<!2>.GetEnumerator()
			{
				Enumerable.<ZipIterator>d__65<TFirst, TSecond, TResult> <ZipIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<ZipIterator>d__ = this;
				}
				else
				{
					<ZipIterator>d__ = new Enumerable.<ZipIterator>d__65<TFirst, TSecond, TResult>(0);
				}
				<ZipIterator>d__.first = first;
				<ZipIterator>d__.second = second;
				<ZipIterator>d__.resultSelector = resultSelector;
				return <ZipIterator>d__;
			}

			// Token: 0x060006D8 RID: 1752 RVA: 0x00014BBF File Offset: 0x00012DBF
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TResult>.GetEnumerator();
			}

			// Token: 0x0400046A RID: 1130
			private int <>1__state;

			// Token: 0x0400046B RID: 1131
			private TResult <>2__current;

			// Token: 0x0400046C RID: 1132
			private int <>l__initialThreadId;

			// Token: 0x0400046D RID: 1133
			private IEnumerable<TFirst> first;

			// Token: 0x0400046E RID: 1134
			public IEnumerable<TFirst> <>3__first;

			// Token: 0x0400046F RID: 1135
			private IEnumerable<TSecond> second;

			// Token: 0x04000470 RID: 1136
			public IEnumerable<TSecond> <>3__second;

			// Token: 0x04000471 RID: 1137
			private Func<TFirst, TSecond, TResult> resultSelector;

			// Token: 0x04000472 RID: 1138
			public Func<TFirst, TSecond, TResult> <>3__resultSelector;

			// Token: 0x04000473 RID: 1139
			private IEnumerator<TFirst> <e1>5__1;

			// Token: 0x04000474 RID: 1140
			private IEnumerator<TSecond> <e2>5__2;
		}

		// Token: 0x020000B3 RID: 179
		[CompilerGenerated]
		private sealed class <DistinctIterator>d__68<TSource> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x060006D9 RID: 1753 RVA: 0x00014BC7 File Offset: 0x00012DC7
			[DebuggerHidden]
			public <DistinctIterator>d__68(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060006DA RID: 1754 RVA: 0x00014BE4 File Offset: 0x00012DE4
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x060006DB RID: 1755 RVA: 0x00014C1C File Offset: 0x00012E1C
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						set = new Set<TSource>(comparer);
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
					}
					while (enumerator.MoveNext())
					{
						TSource value = enumerator.Current;
						if (set.Add(value))
						{
							this.<>2__current = value;
							this.<>1__state = 1;
							return true;
						}
					}
					this.<>m__Finally1();
					enumerator = null;
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x060006DC RID: 1756 RVA: 0x00014CDC File Offset: 0x00012EDC
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x170000EF RID: 239
			// (get) Token: 0x060006DD RID: 1757 RVA: 0x00014CF8 File Offset: 0x00012EF8
			TSource IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060006DE RID: 1758 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000F0 RID: 240
			// (get) Token: 0x060006DF RID: 1759 RVA: 0x00014D00 File Offset: 0x00012F00
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060006E0 RID: 1760 RVA: 0x00014D10 File Offset: 0x00012F10
			[DebuggerHidden]
			IEnumerator<TSource> IEnumerable<!0>.GetEnumerator()
			{
				Enumerable.<DistinctIterator>d__68<TSource> <DistinctIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<DistinctIterator>d__ = this;
				}
				else
				{
					<DistinctIterator>d__ = new Enumerable.<DistinctIterator>d__68<TSource>(0);
				}
				<DistinctIterator>d__.source = source;
				<DistinctIterator>d__.comparer = comparer;
				return <DistinctIterator>d__;
			}

			// Token: 0x060006E1 RID: 1761 RVA: 0x00014D5F File Offset: 0x00012F5F
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TSource>.GetEnumerator();
			}

			// Token: 0x04000475 RID: 1141
			private int <>1__state;

			// Token: 0x04000476 RID: 1142
			private TSource <>2__current;

			// Token: 0x04000477 RID: 1143
			private int <>l__initialThreadId;

			// Token: 0x04000478 RID: 1144
			private IEqualityComparer<TSource> comparer;

			// Token: 0x04000479 RID: 1145
			public IEqualityComparer<TSource> <>3__comparer;

			// Token: 0x0400047A RID: 1146
			private IEnumerable<TSource> source;

			// Token: 0x0400047B RID: 1147
			public IEnumerable<TSource> <>3__source;

			// Token: 0x0400047C RID: 1148
			private Set<TSource> <set>5__1;

			// Token: 0x0400047D RID: 1149
			private IEnumerator<TSource> <>7__wrap1;
		}

		// Token: 0x020000B4 RID: 180
		[CompilerGenerated]
		private sealed class <UnionIterator>d__71<TSource> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x060006E2 RID: 1762 RVA: 0x00014D67 File Offset: 0x00012F67
			[DebuggerHidden]
			public <UnionIterator>d__71(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060006E3 RID: 1763 RVA: 0x00014D84 File Offset: 0x00012F84
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				switch (this.<>1__state)
				{
				case -4:
				case 2:
					break;
				case -3:
				case 1:
					try
					{
						return;
					}
					finally
					{
						this.<>m__Finally1();
					}
					break;
				case -2:
				case -1:
				case 0:
					return;
				default:
					return;
				}
				try
				{
				}
				finally
				{
					this.<>m__Finally2();
				}
			}

			// Token: 0x060006E4 RID: 1764 RVA: 0x00014DF0 File Offset: 0x00012FF0
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					switch (this.<>1__state)
					{
					case 0:
						this.<>1__state = -1;
						set = new Set<TSource>(comparer);
						enumerator = first.GetEnumerator();
						this.<>1__state = -3;
						break;
					case 1:
						this.<>1__state = -3;
						break;
					case 2:
						this.<>1__state = -4;
						goto IL_F3;
					default:
						return false;
					}
					while (enumerator.MoveNext())
					{
						TSource value = enumerator.Current;
						if (set.Add(value))
						{
							this.<>2__current = value;
							this.<>1__state = 1;
							return true;
						}
					}
					this.<>m__Finally1();
					enumerator = null;
					enumerator = second.GetEnumerator();
					this.<>1__state = -4;
					IL_F3:
					while (enumerator.MoveNext())
					{
						TSource value2 = enumerator.Current;
						if (set.Add(value2))
						{
							this.<>2__current = value2;
							this.<>1__state = 2;
							return true;
						}
					}
					this.<>m__Finally2();
					enumerator = null;
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x060006E5 RID: 1765 RVA: 0x00014F34 File Offset: 0x00013134
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x060006E6 RID: 1766 RVA: 0x00014F34 File Offset: 0x00013134
			private void <>m__Finally2()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x170000F1 RID: 241
			// (get) Token: 0x060006E7 RID: 1767 RVA: 0x00014F50 File Offset: 0x00013150
			TSource IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060006E8 RID: 1768 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000F2 RID: 242
			// (get) Token: 0x060006E9 RID: 1769 RVA: 0x00014F58 File Offset: 0x00013158
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060006EA RID: 1770 RVA: 0x00014F68 File Offset: 0x00013168
			[DebuggerHidden]
			IEnumerator<TSource> IEnumerable<!0>.GetEnumerator()
			{
				Enumerable.<UnionIterator>d__71<TSource> <UnionIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<UnionIterator>d__ = this;
				}
				else
				{
					<UnionIterator>d__ = new Enumerable.<UnionIterator>d__71<TSource>(0);
				}
				<UnionIterator>d__.first = first;
				<UnionIterator>d__.second = second;
				<UnionIterator>d__.comparer = comparer;
				return <UnionIterator>d__;
			}

			// Token: 0x060006EB RID: 1771 RVA: 0x00014FC3 File Offset: 0x000131C3
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TSource>.GetEnumerator();
			}

			// Token: 0x0400047E RID: 1150
			private int <>1__state;

			// Token: 0x0400047F RID: 1151
			private TSource <>2__current;

			// Token: 0x04000480 RID: 1152
			private int <>l__initialThreadId;

			// Token: 0x04000481 RID: 1153
			private IEqualityComparer<TSource> comparer;

			// Token: 0x04000482 RID: 1154
			public IEqualityComparer<TSource> <>3__comparer;

			// Token: 0x04000483 RID: 1155
			private IEnumerable<TSource> first;

			// Token: 0x04000484 RID: 1156
			public IEnumerable<TSource> <>3__first;

			// Token: 0x04000485 RID: 1157
			private Set<TSource> <set>5__1;

			// Token: 0x04000486 RID: 1158
			private IEnumerable<TSource> second;

			// Token: 0x04000487 RID: 1159
			public IEnumerable<TSource> <>3__second;

			// Token: 0x04000488 RID: 1160
			private IEnumerator<TSource> <>7__wrap1;
		}

		// Token: 0x020000B5 RID: 181
		[CompilerGenerated]
		private sealed class <IntersectIterator>d__74<TSource> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x060006EC RID: 1772 RVA: 0x00014FCB File Offset: 0x000131CB
			[DebuggerHidden]
			public <IntersectIterator>d__74(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060006ED RID: 1773 RVA: 0x00014FE8 File Offset: 0x000131E8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x060006EE RID: 1774 RVA: 0x00015020 File Offset: 0x00013220
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						set = new Set<TSource>(comparer);
						foreach (TSource value in second)
						{
							set.Add(value);
						}
						enumerator2 = first.GetEnumerator();
						this.<>1__state = -3;
					}
					while (enumerator2.MoveNext())
					{
						TSource value2 = enumerator2.Current;
						if (set.Remove(value2))
						{
							this.<>2__current = value2;
							this.<>1__state = 1;
							return true;
						}
					}
					this.<>m__Finally1();
					enumerator2 = null;
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x060006EF RID: 1775 RVA: 0x00015128 File Offset: 0x00013328
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator2 != null)
				{
					enumerator2.Dispose();
				}
			}

			// Token: 0x170000F3 RID: 243
			// (get) Token: 0x060006F0 RID: 1776 RVA: 0x00015144 File Offset: 0x00013344
			TSource IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060006F1 RID: 1777 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000F4 RID: 244
			// (get) Token: 0x060006F2 RID: 1778 RVA: 0x0001514C File Offset: 0x0001334C
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060006F3 RID: 1779 RVA: 0x0001515C File Offset: 0x0001335C
			[DebuggerHidden]
			IEnumerator<TSource> IEnumerable<!0>.GetEnumerator()
			{
				Enumerable.<IntersectIterator>d__74<TSource> <IntersectIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<IntersectIterator>d__ = this;
				}
				else
				{
					<IntersectIterator>d__ = new Enumerable.<IntersectIterator>d__74<TSource>(0);
				}
				<IntersectIterator>d__.first = first;
				<IntersectIterator>d__.second = second;
				<IntersectIterator>d__.comparer = comparer;
				return <IntersectIterator>d__;
			}

			// Token: 0x060006F4 RID: 1780 RVA: 0x000151B7 File Offset: 0x000133B7
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TSource>.GetEnumerator();
			}

			// Token: 0x04000489 RID: 1161
			private int <>1__state;

			// Token: 0x0400048A RID: 1162
			private TSource <>2__current;

			// Token: 0x0400048B RID: 1163
			private int <>l__initialThreadId;

			// Token: 0x0400048C RID: 1164
			private IEqualityComparer<TSource> comparer;

			// Token: 0x0400048D RID: 1165
			public IEqualityComparer<TSource> <>3__comparer;

			// Token: 0x0400048E RID: 1166
			private IEnumerable<TSource> second;

			// Token: 0x0400048F RID: 1167
			public IEnumerable<TSource> <>3__second;

			// Token: 0x04000490 RID: 1168
			private IEnumerable<TSource> first;

			// Token: 0x04000491 RID: 1169
			public IEnumerable<TSource> <>3__first;

			// Token: 0x04000492 RID: 1170
			private Set<TSource> <set>5__1;

			// Token: 0x04000493 RID: 1171
			private IEnumerator<TSource> <>7__wrap1;
		}

		// Token: 0x020000B6 RID: 182
		[CompilerGenerated]
		private sealed class <ExceptIterator>d__77<TSource> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x060006F5 RID: 1781 RVA: 0x000151BF File Offset: 0x000133BF
			[DebuggerHidden]
			public <ExceptIterator>d__77(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060006F6 RID: 1782 RVA: 0x000151DC File Offset: 0x000133DC
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x060006F7 RID: 1783 RVA: 0x00015214 File Offset: 0x00013414
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						set = new Set<TSource>(comparer);
						foreach (TSource value in second)
						{
							set.Add(value);
						}
						enumerator2 = first.GetEnumerator();
						this.<>1__state = -3;
					}
					while (enumerator2.MoveNext())
					{
						TSource value2 = enumerator2.Current;
						if (set.Add(value2))
						{
							this.<>2__current = value2;
							this.<>1__state = 1;
							return true;
						}
					}
					this.<>m__Finally1();
					enumerator2 = null;
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x060006F8 RID: 1784 RVA: 0x0001531C File Offset: 0x0001351C
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator2 != null)
				{
					enumerator2.Dispose();
				}
			}

			// Token: 0x170000F5 RID: 245
			// (get) Token: 0x060006F9 RID: 1785 RVA: 0x00015338 File Offset: 0x00013538
			TSource IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060006FA RID: 1786 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000F6 RID: 246
			// (get) Token: 0x060006FB RID: 1787 RVA: 0x00015340 File Offset: 0x00013540
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060006FC RID: 1788 RVA: 0x00015350 File Offset: 0x00013550
			[DebuggerHidden]
			IEnumerator<TSource> IEnumerable<!0>.GetEnumerator()
			{
				Enumerable.<ExceptIterator>d__77<TSource> <ExceptIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<ExceptIterator>d__ = this;
				}
				else
				{
					<ExceptIterator>d__ = new Enumerable.<ExceptIterator>d__77<TSource>(0);
				}
				<ExceptIterator>d__.first = first;
				<ExceptIterator>d__.second = second;
				<ExceptIterator>d__.comparer = comparer;
				return <ExceptIterator>d__;
			}

			// Token: 0x060006FD RID: 1789 RVA: 0x000153AB File Offset: 0x000135AB
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TSource>.GetEnumerator();
			}

			// Token: 0x04000494 RID: 1172
			private int <>1__state;

			// Token: 0x04000495 RID: 1173
			private TSource <>2__current;

			// Token: 0x04000496 RID: 1174
			private int <>l__initialThreadId;

			// Token: 0x04000497 RID: 1175
			private IEqualityComparer<TSource> comparer;

			// Token: 0x04000498 RID: 1176
			public IEqualityComparer<TSource> <>3__comparer;

			// Token: 0x04000499 RID: 1177
			private IEnumerable<TSource> second;

			// Token: 0x0400049A RID: 1178
			public IEnumerable<TSource> <>3__second;

			// Token: 0x0400049B RID: 1179
			private IEnumerable<TSource> first;

			// Token: 0x0400049C RID: 1180
			public IEnumerable<TSource> <>3__first;

			// Token: 0x0400049D RID: 1181
			private Set<TSource> <set>5__1;

			// Token: 0x0400049E RID: 1182
			private IEnumerator<TSource> <>7__wrap1;
		}

		// Token: 0x020000B7 RID: 183
		[CompilerGenerated]
		private sealed class <ReverseIterator>d__79<TSource> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x060006FE RID: 1790 RVA: 0x000153B3 File Offset: 0x000135B3
			[DebuggerHidden]
			public <ReverseIterator>d__79(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060006FF RID: 1791 RVA: 0x000039E8 File Offset: 0x00001BE8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06000700 RID: 1792 RVA: 0x000153D0 File Offset: 0x000135D0
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
					int num2 = i;
					i = num2 - 1;
				}
				else
				{
					this.<>1__state = -1;
					buffer = new Buffer<TSource>(source);
					i = buffer.count - 1;
				}
				if (i < 0)
				{
					return false;
				}
				this.<>2__current = buffer.items[i];
				this.<>1__state = 1;
				return true;
			}

			// Token: 0x170000F7 RID: 247
			// (get) Token: 0x06000701 RID: 1793 RVA: 0x00015460 File Offset: 0x00013660
			TSource IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000702 RID: 1794 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000F8 RID: 248
			// (get) Token: 0x06000703 RID: 1795 RVA: 0x00015468 File Offset: 0x00013668
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000704 RID: 1796 RVA: 0x00015478 File Offset: 0x00013678
			[DebuggerHidden]
			IEnumerator<TSource> IEnumerable<!0>.GetEnumerator()
			{
				Enumerable.<ReverseIterator>d__79<TSource> <ReverseIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<ReverseIterator>d__ = this;
				}
				else
				{
					<ReverseIterator>d__ = new Enumerable.<ReverseIterator>d__79<TSource>(0);
				}
				<ReverseIterator>d__.source = source;
				return <ReverseIterator>d__;
			}

			// Token: 0x06000705 RID: 1797 RVA: 0x000154BB File Offset: 0x000136BB
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TSource>.GetEnumerator();
			}

			// Token: 0x0400049F RID: 1183
			private int <>1__state;

			// Token: 0x040004A0 RID: 1184
			private TSource <>2__current;

			// Token: 0x040004A1 RID: 1185
			private int <>l__initialThreadId;

			// Token: 0x040004A2 RID: 1186
			private IEnumerable<TSource> source;

			// Token: 0x040004A3 RID: 1187
			public IEnumerable<TSource> <>3__source;

			// Token: 0x040004A4 RID: 1188
			private Buffer<TSource> <buffer>5__1;

			// Token: 0x040004A5 RID: 1189
			private int <i>5__2;
		}

		// Token: 0x020000B8 RID: 184
		[CompilerGenerated]
		private sealed class <DefaultIfEmptyIterator>d__95<TSource> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x06000706 RID: 1798 RVA: 0x000154C3 File Offset: 0x000136C3
			[DebuggerHidden]
			public <DefaultIfEmptyIterator>d__95(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000707 RID: 1799 RVA: 0x000154E0 File Offset: 0x000136E0
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num - 1 <= 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x06000708 RID: 1800 RVA: 0x0001551C File Offset: 0x0001371C
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					switch (this.<>1__state)
					{
					case 0:
						this.<>1__state = -1;
						e = source.GetEnumerator();
						this.<>1__state = -3;
						if (!e.MoveNext())
						{
							this.<>2__current = defaultValue;
							this.<>1__state = 2;
							return true;
						}
						break;
					case 1:
						this.<>1__state = -3;
						if (!e.MoveNext())
						{
							goto IL_9F;
						}
						break;
					case 2:
						this.<>1__state = -3;
						goto IL_9F;
					default:
						return false;
					}
					this.<>2__current = e.Current;
					this.<>1__state = 1;
					return true;
					IL_9F:
					this.<>m__Finally1();
					e = null;
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x06000709 RID: 1801 RVA: 0x000155F4 File Offset: 0x000137F4
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (e != null)
				{
					e.Dispose();
				}
			}

			// Token: 0x170000F9 RID: 249
			// (get) Token: 0x0600070A RID: 1802 RVA: 0x00015610 File Offset: 0x00013810
			TSource IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600070B RID: 1803 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000FA RID: 250
			// (get) Token: 0x0600070C RID: 1804 RVA: 0x00015618 File Offset: 0x00013818
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600070D RID: 1805 RVA: 0x00015628 File Offset: 0x00013828
			[DebuggerHidden]
			IEnumerator<TSource> IEnumerable<!0>.GetEnumerator()
			{
				Enumerable.<DefaultIfEmptyIterator>d__95<TSource> <DefaultIfEmptyIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<DefaultIfEmptyIterator>d__ = this;
				}
				else
				{
					<DefaultIfEmptyIterator>d__ = new Enumerable.<DefaultIfEmptyIterator>d__95<TSource>(0);
				}
				<DefaultIfEmptyIterator>d__.source = source;
				<DefaultIfEmptyIterator>d__.defaultValue = defaultValue;
				return <DefaultIfEmptyIterator>d__;
			}

			// Token: 0x0600070E RID: 1806 RVA: 0x00015677 File Offset: 0x00013877
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TSource>.GetEnumerator();
			}

			// Token: 0x040004A6 RID: 1190
			private int <>1__state;

			// Token: 0x040004A7 RID: 1191
			private TSource <>2__current;

			// Token: 0x040004A8 RID: 1192
			private int <>l__initialThreadId;

			// Token: 0x040004A9 RID: 1193
			private IEnumerable<TSource> source;

			// Token: 0x040004AA RID: 1194
			public IEnumerable<TSource> <>3__source;

			// Token: 0x040004AB RID: 1195
			private IEnumerator<TSource> <e>5__1;

			// Token: 0x040004AC RID: 1196
			private TSource defaultValue;

			// Token: 0x040004AD RID: 1197
			public TSource <>3__defaultValue;
		}

		// Token: 0x020000B9 RID: 185
		[CompilerGenerated]
		private sealed class <OfTypeIterator>d__97<TResult> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x0600070F RID: 1807 RVA: 0x0001567F File Offset: 0x0001387F
			[DebuggerHidden]
			public <OfTypeIterator>d__97(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000710 RID: 1808 RVA: 0x0001569C File Offset: 0x0001389C
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x06000711 RID: 1809 RVA: 0x000156D4 File Offset: 0x000138D4
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
					}
					while (enumerator.MoveNext())
					{
						object obj = enumerator.Current;
						if (obj is TResult)
						{
							this.<>2__current = (TResult)((object)obj);
							this.<>1__state = 1;
							return true;
						}
					}
					this.<>m__Finally1();
					enumerator = null;
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x06000712 RID: 1810 RVA: 0x00015780 File Offset: 0x00013980
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}

			// Token: 0x170000FB RID: 251
			// (get) Token: 0x06000713 RID: 1811 RVA: 0x000157A9 File Offset: 0x000139A9
			TResult IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000714 RID: 1812 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000FC RID: 252
			// (get) Token: 0x06000715 RID: 1813 RVA: 0x000157B1 File Offset: 0x000139B1
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000716 RID: 1814 RVA: 0x000157C0 File Offset: 0x000139C0
			[DebuggerHidden]
			IEnumerator<TResult> IEnumerable<!0>.GetEnumerator()
			{
				Enumerable.<OfTypeIterator>d__97<TResult> <OfTypeIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<OfTypeIterator>d__ = this;
				}
				else
				{
					<OfTypeIterator>d__ = new Enumerable.<OfTypeIterator>d__97<TResult>(0);
				}
				<OfTypeIterator>d__.source = source;
				return <OfTypeIterator>d__;
			}

			// Token: 0x06000717 RID: 1815 RVA: 0x00015803 File Offset: 0x00013A03
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TResult>.GetEnumerator();
			}

			// Token: 0x040004AE RID: 1198
			private int <>1__state;

			// Token: 0x040004AF RID: 1199
			private TResult <>2__current;

			// Token: 0x040004B0 RID: 1200
			private int <>l__initialThreadId;

			// Token: 0x040004B1 RID: 1201
			private IEnumerable source;

			// Token: 0x040004B2 RID: 1202
			public IEnumerable <>3__source;

			// Token: 0x040004B3 RID: 1203
			private IEnumerator <>7__wrap1;
		}

		// Token: 0x020000BA RID: 186
		[CompilerGenerated]
		private sealed class <CastIterator>d__99<TResult> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x06000718 RID: 1816 RVA: 0x0001580B File Offset: 0x00013A0B
			[DebuggerHidden]
			public <CastIterator>d__99(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000719 RID: 1817 RVA: 0x00015828 File Offset: 0x00013A28
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x0600071A RID: 1818 RVA: 0x00015860 File Offset: 0x00013A60
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
					}
					if (!enumerator.MoveNext())
					{
						this.<>m__Finally1();
						enumerator = null;
						result = false;
					}
					else
					{
						object obj = enumerator.Current;
						this.<>2__current = (TResult)((object)obj);
						this.<>1__state = 1;
						result = true;
					}
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x0600071B RID: 1819 RVA: 0x00015904 File Offset: 0x00013B04
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}

			// Token: 0x170000FD RID: 253
			// (get) Token: 0x0600071C RID: 1820 RVA: 0x0001592D File Offset: 0x00013B2D
			TResult IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600071D RID: 1821 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000FE RID: 254
			// (get) Token: 0x0600071E RID: 1822 RVA: 0x00015935 File Offset: 0x00013B35
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600071F RID: 1823 RVA: 0x00015944 File Offset: 0x00013B44
			[DebuggerHidden]
			IEnumerator<TResult> IEnumerable<!0>.GetEnumerator()
			{
				Enumerable.<CastIterator>d__99<TResult> <CastIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<CastIterator>d__ = this;
				}
				else
				{
					<CastIterator>d__ = new Enumerable.<CastIterator>d__99<TResult>(0);
				}
				<CastIterator>d__.source = source;
				return <CastIterator>d__;
			}

			// Token: 0x06000720 RID: 1824 RVA: 0x00015987 File Offset: 0x00013B87
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TResult>.GetEnumerator();
			}

			// Token: 0x040004B4 RID: 1204
			private int <>1__state;

			// Token: 0x040004B5 RID: 1205
			private TResult <>2__current;

			// Token: 0x040004B6 RID: 1206
			private int <>l__initialThreadId;

			// Token: 0x040004B7 RID: 1207
			private IEnumerable source;

			// Token: 0x040004B8 RID: 1208
			public IEnumerable <>3__source;

			// Token: 0x040004B9 RID: 1209
			private IEnumerator <>7__wrap1;
		}

		// Token: 0x020000BB RID: 187
		[CompilerGenerated]
		private sealed class <RangeIterator>d__115 : IEnumerable<int>, IEnumerable, IEnumerator<int>, IDisposable, IEnumerator
		{
			// Token: 0x06000721 RID: 1825 RVA: 0x0001598F File Offset: 0x00013B8F
			[DebuggerHidden]
			public <RangeIterator>d__115(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000722 RID: 1826 RVA: 0x000039E8 File Offset: 0x00001BE8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06000723 RID: 1827 RVA: 0x000159AC File Offset: 0x00013BAC
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
					int num2 = i;
					i = num2 + 1;
				}
				else
				{
					this.<>1__state = -1;
					i = 0;
				}
				if (i >= count)
				{
					return false;
				}
				this.<>2__current = start + i;
				this.<>1__state = 1;
				return true;
			}

			// Token: 0x170000FF RID: 255
			// (get) Token: 0x06000724 RID: 1828 RVA: 0x00015A1B File Offset: 0x00013C1B
			int IEnumerator<int>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000725 RID: 1829 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000100 RID: 256
			// (get) Token: 0x06000726 RID: 1830 RVA: 0x00015A23 File Offset: 0x00013C23
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000727 RID: 1831 RVA: 0x00015A30 File Offset: 0x00013C30
			[DebuggerHidden]
			IEnumerator<int> IEnumerable<int>.GetEnumerator()
			{
				Enumerable.<RangeIterator>d__115 <RangeIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<RangeIterator>d__ = this;
				}
				else
				{
					<RangeIterator>d__ = new Enumerable.<RangeIterator>d__115(0);
				}
				<RangeIterator>d__.start = start;
				<RangeIterator>d__.count = count;
				return <RangeIterator>d__;
			}

			// Token: 0x06000728 RID: 1832 RVA: 0x00015A7F File Offset: 0x00013C7F
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Int32>.GetEnumerator();
			}

			// Token: 0x040004BA RID: 1210
			private int <>1__state;

			// Token: 0x040004BB RID: 1211
			private int <>2__current;

			// Token: 0x040004BC RID: 1212
			private int <>l__initialThreadId;

			// Token: 0x040004BD RID: 1213
			private int start;

			// Token: 0x040004BE RID: 1214
			public int <>3__start;

			// Token: 0x040004BF RID: 1215
			private int <i>5__1;

			// Token: 0x040004C0 RID: 1216
			private int count;

			// Token: 0x040004C1 RID: 1217
			public int <>3__count;
		}

		// Token: 0x020000BC RID: 188
		[CompilerGenerated]
		private sealed class <RepeatIterator>d__117<TResult> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x06000729 RID: 1833 RVA: 0x00015A87 File Offset: 0x00013C87
			[DebuggerHidden]
			public <RepeatIterator>d__117(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x0600072A RID: 1834 RVA: 0x000039E8 File Offset: 0x00001BE8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x0600072B RID: 1835 RVA: 0x00015AA4 File Offset: 0x00013CA4
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
					int num2 = i;
					i = num2 + 1;
				}
				else
				{
					this.<>1__state = -1;
					i = 0;
				}
				if (i >= count)
				{
					return false;
				}
				this.<>2__current = element;
				this.<>1__state = 1;
				return true;
			}

			// Token: 0x17000101 RID: 257
			// (get) Token: 0x0600072C RID: 1836 RVA: 0x00015B0C File Offset: 0x00013D0C
			TResult IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600072D RID: 1837 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000102 RID: 258
			// (get) Token: 0x0600072E RID: 1838 RVA: 0x00015B14 File Offset: 0x00013D14
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600072F RID: 1839 RVA: 0x00015B24 File Offset: 0x00013D24
			[DebuggerHidden]
			IEnumerator<TResult> IEnumerable<!0>.GetEnumerator()
			{
				Enumerable.<RepeatIterator>d__117<TResult> <RepeatIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<RepeatIterator>d__ = this;
				}
				else
				{
					<RepeatIterator>d__ = new Enumerable.<RepeatIterator>d__117<TResult>(0);
				}
				<RepeatIterator>d__.element = element;
				<RepeatIterator>d__.count = count;
				return <RepeatIterator>d__;
			}

			// Token: 0x06000730 RID: 1840 RVA: 0x00015B73 File Offset: 0x00013D73
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TResult>.GetEnumerator();
			}

			// Token: 0x040004C2 RID: 1218
			private int <>1__state;

			// Token: 0x040004C3 RID: 1219
			private TResult <>2__current;

			// Token: 0x040004C4 RID: 1220
			private int <>l__initialThreadId;

			// Token: 0x040004C5 RID: 1221
			private TResult element;

			// Token: 0x040004C6 RID: 1222
			public TResult <>3__element;

			// Token: 0x040004C7 RID: 1223
			private int <i>5__1;

			// Token: 0x040004C8 RID: 1224
			private int count;

			// Token: 0x040004C9 RID: 1225
			public int <>3__count;
		}
	}
}
