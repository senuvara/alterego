﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x02000239 RID: 569
	internal class DynamicExpression4 : DynamicExpression, IArgumentProvider
	{
		// Token: 0x06000FB9 RID: 4025 RVA: 0x00031991 File Offset: 0x0002FB91
		internal DynamicExpression4(Type delegateType, CallSiteBinder binder, Expression arg0, Expression arg1, Expression arg2, Expression arg3) : base(delegateType, binder)
		{
			this._arg0 = arg0;
			this._arg1 = arg1;
			this._arg2 = arg2;
			this._arg3 = arg3;
		}

		// Token: 0x06000FBA RID: 4026 RVA: 0x000319BC File Offset: 0x0002FBBC
		Expression IArgumentProvider.GetArgument(int index)
		{
			switch (index)
			{
			case 0:
				return ExpressionUtils.ReturnObject<Expression>(this._arg0);
			case 1:
				return this._arg1;
			case 2:
				return this._arg2;
			case 3:
				return this._arg3;
			default:
				throw new ArgumentOutOfRangeException("index");
			}
		}

		// Token: 0x170002E1 RID: 737
		// (get) Token: 0x06000FBB RID: 4027 RVA: 0x0002F078 File Offset: 0x0002D278
		int IArgumentProvider.ArgumentCount
		{
			get
			{
				return 4;
			}
		}

		// Token: 0x06000FBC RID: 4028 RVA: 0x00031A0C File Offset: 0x0002FC0C
		internal override bool SameArguments(ICollection<Expression> arguments)
		{
			if (arguments != null && arguments.Count == 4)
			{
				ReadOnlyCollection<Expression> readOnlyCollection = this._arg0 as ReadOnlyCollection<Expression>;
				if (readOnlyCollection != null)
				{
					return ExpressionUtils.SameElements<Expression>(arguments, readOnlyCollection);
				}
				using (IEnumerator<Expression> enumerator = arguments.GetEnumerator())
				{
					enumerator.MoveNext();
					if (enumerator.Current == this._arg0)
					{
						enumerator.MoveNext();
						if (enumerator.Current == this._arg1)
						{
							enumerator.MoveNext();
							if (enumerator.Current == this._arg2)
							{
								enumerator.MoveNext();
								return enumerator.Current == this._arg3;
							}
						}
					}
				}
				return false;
			}
			return false;
		}

		// Token: 0x06000FBD RID: 4029 RVA: 0x00031AC0 File Offset: 0x0002FCC0
		internal override ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			return ExpressionUtils.ReturnReadOnly(this, ref this._arg0);
		}

		// Token: 0x06000FBE RID: 4030 RVA: 0x00031ACE File Offset: 0x0002FCCE
		internal override DynamicExpression Rewrite(Expression[] args)
		{
			return ExpressionExtension.MakeDynamic(base.DelegateType, base.Binder, args[0], args[1], args[2], args[3]);
		}

		// Token: 0x040008CC RID: 2252
		private object _arg0;

		// Token: 0x040008CD RID: 2253
		private readonly Expression _arg1;

		// Token: 0x040008CE RID: 2254
		private readonly Expression _arg2;

		// Token: 0x040008CF RID: 2255
		private readonly Expression _arg3;
	}
}
