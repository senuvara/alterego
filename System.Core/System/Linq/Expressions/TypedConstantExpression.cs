﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x02000229 RID: 553
	internal class TypedConstantExpression : ConstantExpression
	{
		// Token: 0x06000F0C RID: 3852 RVA: 0x0002F8DF File Offset: 0x0002DADF
		internal TypedConstantExpression(object value, Type type) : base(value)
		{
			this.Type = type;
		}

		// Token: 0x170002BA RID: 698
		// (get) Token: 0x06000F0D RID: 3853 RVA: 0x0002F8EF File Offset: 0x0002DAEF
		public sealed override Type Type
		{
			[CompilerGenerated]
			get
			{
				return this.<Type>k__BackingField;
			}
		}

		// Token: 0x040008A8 RID: 2216
		[CompilerGenerated]
		private readonly Type <Type>k__BackingField;
	}
}
