﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace System.Linq.Expressions
{
	// Token: 0x0200023E RID: 574
	internal static class Error
	{
		// Token: 0x06000FDB RID: 4059 RVA: 0x00032373 File Offset: 0x00030573
		internal static Exception ReducibleMustOverrideReduce()
		{
			return new ArgumentException(Strings.ReducibleMustOverrideReduce);
		}

		// Token: 0x06000FDC RID: 4060 RVA: 0x0003237F File Offset: 0x0003057F
		internal static Exception ArgCntMustBeGreaterThanNameCnt()
		{
			return new ArgumentException(Strings.ArgCntMustBeGreaterThanNameCnt);
		}

		// Token: 0x06000FDD RID: 4061 RVA: 0x0003238B File Offset: 0x0003058B
		internal static Exception InvalidMetaObjectCreated(object p0)
		{
			return new InvalidOperationException(Strings.InvalidMetaObjectCreated(p0));
		}

		// Token: 0x06000FDE RID: 4062 RVA: 0x00032398 File Offset: 0x00030598
		internal static Exception AmbiguousMatchInExpandoObject(object p0)
		{
			return new AmbiguousMatchException(Strings.AmbiguousMatchInExpandoObject(p0));
		}

		// Token: 0x06000FDF RID: 4063 RVA: 0x000323A5 File Offset: 0x000305A5
		internal static Exception SameKeyExistsInExpando(object key)
		{
			return new ArgumentException(Strings.SameKeyExistsInExpando(key), "key");
		}

		// Token: 0x06000FE0 RID: 4064 RVA: 0x000323B7 File Offset: 0x000305B7
		internal static Exception KeyDoesNotExistInExpando(object p0)
		{
			return new KeyNotFoundException(Strings.KeyDoesNotExistInExpando(p0));
		}

		// Token: 0x06000FE1 RID: 4065 RVA: 0x000323C4 File Offset: 0x000305C4
		internal static Exception CollectionModifiedWhileEnumerating()
		{
			return new InvalidOperationException(Strings.CollectionModifiedWhileEnumerating);
		}

		// Token: 0x06000FE2 RID: 4066 RVA: 0x000323D0 File Offset: 0x000305D0
		internal static Exception CollectionReadOnly()
		{
			return new NotSupportedException(Strings.CollectionReadOnly);
		}

		// Token: 0x06000FE3 RID: 4067 RVA: 0x000323DC File Offset: 0x000305DC
		internal static Exception MustReduceToDifferent()
		{
			return new ArgumentException(Strings.MustReduceToDifferent);
		}

		// Token: 0x06000FE4 RID: 4068 RVA: 0x000323E8 File Offset: 0x000305E8
		internal static Exception BinderNotCompatibleWithCallSite(object p0, object p1, object p2)
		{
			return new InvalidOperationException(Strings.BinderNotCompatibleWithCallSite(p0, p1, p2));
		}

		// Token: 0x06000FE5 RID: 4069 RVA: 0x000323F7 File Offset: 0x000305F7
		internal static Exception DynamicBindingNeedsRestrictions(object p0, object p1)
		{
			return new InvalidOperationException(Strings.DynamicBindingNeedsRestrictions(p0, p1));
		}

		// Token: 0x06000FE6 RID: 4070 RVA: 0x00032405 File Offset: 0x00030605
		internal static Exception DynamicObjectResultNotAssignable(object p0, object p1, object p2, object p3)
		{
			return new InvalidCastException(Strings.DynamicObjectResultNotAssignable(p0, p1, p2, p3));
		}

		// Token: 0x06000FE7 RID: 4071 RVA: 0x00032415 File Offset: 0x00030615
		internal static Exception DynamicBinderResultNotAssignable(object p0, object p1, object p2)
		{
			return new InvalidCastException(Strings.DynamicBinderResultNotAssignable(p0, p1, p2));
		}

		// Token: 0x06000FE8 RID: 4072 RVA: 0x00032424 File Offset: 0x00030624
		internal static Exception BindingCannotBeNull()
		{
			return new InvalidOperationException(Strings.BindingCannotBeNull);
		}

		// Token: 0x06000FE9 RID: 4073 RVA: 0x00032430 File Offset: 0x00030630
		internal static Exception ReducedNotCompatible()
		{
			return new ArgumentException(Strings.ReducedNotCompatible);
		}

		// Token: 0x06000FEA RID: 4074 RVA: 0x0003243C File Offset: 0x0003063C
		internal static Exception SetterHasNoParams(string paramName)
		{
			return new ArgumentException(Strings.SetterHasNoParams, paramName);
		}

		// Token: 0x06000FEB RID: 4075 RVA: 0x00032449 File Offset: 0x00030649
		internal static Exception PropertyCannotHaveRefType(string paramName)
		{
			return new ArgumentException(Strings.PropertyCannotHaveRefType, paramName);
		}

		// Token: 0x06000FEC RID: 4076 RVA: 0x00032456 File Offset: 0x00030656
		internal static Exception IndexesOfSetGetMustMatch(string paramName)
		{
			return new ArgumentException(Strings.IndexesOfSetGetMustMatch, paramName);
		}

		// Token: 0x06000FED RID: 4077 RVA: 0x00032463 File Offset: 0x00030663
		internal static Exception TypeParameterIsNotDelegate(object p0)
		{
			return new InvalidOperationException(Strings.TypeParameterIsNotDelegate(p0));
		}

		// Token: 0x06000FEE RID: 4078 RVA: 0x00032470 File Offset: 0x00030670
		internal static Exception FirstArgumentMustBeCallSite()
		{
			return new ArgumentException(Strings.FirstArgumentMustBeCallSite);
		}

		// Token: 0x06000FEF RID: 4079 RVA: 0x0003247C File Offset: 0x0003067C
		internal static Exception AccessorsCannotHaveVarArgs(string paramName)
		{
			return new ArgumentException(Strings.AccessorsCannotHaveVarArgs, paramName);
		}

		// Token: 0x06000FF0 RID: 4080 RVA: 0x00032489 File Offset: 0x00030689
		private static Exception AccessorsCannotHaveByRefArgs(string paramName)
		{
			return new ArgumentException(Strings.AccessorsCannotHaveByRefArgs, paramName);
		}

		// Token: 0x06000FF1 RID: 4081 RVA: 0x00032496 File Offset: 0x00030696
		internal static Exception AccessorsCannotHaveByRefArgs(string paramName, int index)
		{
			return Error.AccessorsCannotHaveByRefArgs(Error.GetParamName(paramName, index));
		}

		// Token: 0x06000FF2 RID: 4082 RVA: 0x000324A4 File Offset: 0x000306A4
		internal static Exception TypeMustBeDerivedFromSystemDelegate()
		{
			return new ArgumentException(Strings.TypeMustBeDerivedFromSystemDelegate);
		}

		// Token: 0x06000FF3 RID: 4083 RVA: 0x000324B0 File Offset: 0x000306B0
		internal static Exception NoOrInvalidRuleProduced()
		{
			return new InvalidOperationException(Strings.NoOrInvalidRuleProduced);
		}

		// Token: 0x06000FF4 RID: 4084 RVA: 0x000324BC File Offset: 0x000306BC
		internal static Exception BoundsCannotBeLessThanOne(string paramName)
		{
			return new ArgumentException(Strings.BoundsCannotBeLessThanOne, paramName);
		}

		// Token: 0x06000FF5 RID: 4085 RVA: 0x000324C9 File Offset: 0x000306C9
		internal static Exception TypeMustNotBeByRef(string paramName)
		{
			return new ArgumentException(Strings.TypeMustNotBeByRef, paramName);
		}

		// Token: 0x06000FF6 RID: 4086 RVA: 0x000324D6 File Offset: 0x000306D6
		internal static Exception TypeMustNotBePointer(string paramName)
		{
			return new ArgumentException(Strings.TypeMustNotBePointer, paramName);
		}

		// Token: 0x06000FF7 RID: 4087 RVA: 0x000324E3 File Offset: 0x000306E3
		internal static Exception SetterMustBeVoid(string paramName)
		{
			return new ArgumentException(Strings.SetterMustBeVoid, paramName);
		}

		// Token: 0x06000FF8 RID: 4088 RVA: 0x000324F0 File Offset: 0x000306F0
		internal static Exception PropertyTypeMustMatchGetter(string paramName)
		{
			return new ArgumentException(Strings.PropertyTypeMustMatchGetter, paramName);
		}

		// Token: 0x06000FF9 RID: 4089 RVA: 0x000324FD File Offset: 0x000306FD
		internal static Exception PropertyTypeMustMatchSetter(string paramName)
		{
			return new ArgumentException(Strings.PropertyTypeMustMatchSetter, paramName);
		}

		// Token: 0x06000FFA RID: 4090 RVA: 0x0003250A File Offset: 0x0003070A
		internal static Exception BothAccessorsMustBeStatic(string paramName)
		{
			return new ArgumentException(Strings.BothAccessorsMustBeStatic, paramName);
		}

		// Token: 0x06000FFB RID: 4091 RVA: 0x00032517 File Offset: 0x00030717
		internal static Exception OnlyStaticFieldsHaveNullInstance(string paramName)
		{
			return new ArgumentException(Strings.OnlyStaticFieldsHaveNullInstance, paramName);
		}

		// Token: 0x06000FFC RID: 4092 RVA: 0x00032524 File Offset: 0x00030724
		internal static Exception OnlyStaticPropertiesHaveNullInstance(string paramName)
		{
			return new ArgumentException(Strings.OnlyStaticPropertiesHaveNullInstance, paramName);
		}

		// Token: 0x06000FFD RID: 4093 RVA: 0x00032531 File Offset: 0x00030731
		internal static Exception OnlyStaticMethodsHaveNullInstance()
		{
			return new ArgumentException(Strings.OnlyStaticMethodsHaveNullInstance);
		}

		// Token: 0x06000FFE RID: 4094 RVA: 0x0003253D File Offset: 0x0003073D
		internal static Exception PropertyTypeCannotBeVoid(string paramName)
		{
			return new ArgumentException(Strings.PropertyTypeCannotBeVoid, paramName);
		}

		// Token: 0x06000FFF RID: 4095 RVA: 0x0003254A File Offset: 0x0003074A
		internal static Exception InvalidUnboxType(string paramName)
		{
			return new ArgumentException(Strings.InvalidUnboxType, paramName);
		}

		// Token: 0x06001000 RID: 4096 RVA: 0x00032557 File Offset: 0x00030757
		internal static Exception ExpressionMustBeWriteable(string paramName)
		{
			return new ArgumentException(Strings.ExpressionMustBeWriteable, paramName);
		}

		// Token: 0x06001001 RID: 4097 RVA: 0x00032564 File Offset: 0x00030764
		internal static Exception ArgumentMustNotHaveValueType(string paramName)
		{
			return new ArgumentException(Strings.ArgumentMustNotHaveValueType, paramName);
		}

		// Token: 0x06001002 RID: 4098 RVA: 0x00032571 File Offset: 0x00030771
		internal static Exception MustBeReducible()
		{
			return new ArgumentException(Strings.MustBeReducible);
		}

		// Token: 0x06001003 RID: 4099 RVA: 0x0003257D File Offset: 0x0003077D
		internal static Exception AllTestValuesMustHaveSameType(string paramName)
		{
			return new ArgumentException(Strings.AllTestValuesMustHaveSameType, paramName);
		}

		// Token: 0x06001004 RID: 4100 RVA: 0x0003258A File Offset: 0x0003078A
		internal static Exception AllCaseBodiesMustHaveSameType(string paramName)
		{
			return new ArgumentException(Strings.AllCaseBodiesMustHaveSameType, paramName);
		}

		// Token: 0x06001005 RID: 4101 RVA: 0x00032597 File Offset: 0x00030797
		internal static Exception DefaultBodyMustBeSupplied(string paramName)
		{
			return new ArgumentException(Strings.DefaultBodyMustBeSupplied, paramName);
		}

		// Token: 0x06001006 RID: 4102 RVA: 0x000325A4 File Offset: 0x000307A4
		internal static Exception LabelMustBeVoidOrHaveExpression(string paramName)
		{
			return new ArgumentException(Strings.LabelMustBeVoidOrHaveExpression, paramName);
		}

		// Token: 0x06001007 RID: 4103 RVA: 0x000325B1 File Offset: 0x000307B1
		internal static Exception LabelTypeMustBeVoid(string paramName)
		{
			return new ArgumentException(Strings.LabelTypeMustBeVoid, paramName);
		}

		// Token: 0x06001008 RID: 4104 RVA: 0x000325BE File Offset: 0x000307BE
		internal static Exception QuotedExpressionMustBeLambda(string paramName)
		{
			return new ArgumentException(Strings.QuotedExpressionMustBeLambda, paramName);
		}

		// Token: 0x06001009 RID: 4105 RVA: 0x000325CB File Offset: 0x000307CB
		internal static Exception VariableMustNotBeByRef(object p0, object p1, string paramName)
		{
			return new ArgumentException(Strings.VariableMustNotBeByRef(p0, p1), paramName);
		}

		// Token: 0x0600100A RID: 4106 RVA: 0x000325DA File Offset: 0x000307DA
		internal static Exception VariableMustNotBeByRef(object p0, object p1, string paramName, int index)
		{
			return Error.VariableMustNotBeByRef(p0, p1, Error.GetParamName(paramName, index));
		}

		// Token: 0x0600100B RID: 4107 RVA: 0x000325EA File Offset: 0x000307EA
		private static Exception DuplicateVariable(object p0, string paramName)
		{
			return new ArgumentException(Strings.DuplicateVariable(p0), paramName);
		}

		// Token: 0x0600100C RID: 4108 RVA: 0x000325F8 File Offset: 0x000307F8
		internal static Exception DuplicateVariable(object p0, string paramName, int index)
		{
			return Error.DuplicateVariable(p0, Error.GetParamName(paramName, index));
		}

		// Token: 0x0600100D RID: 4109 RVA: 0x00032607 File Offset: 0x00030807
		internal static Exception StartEndMustBeOrdered()
		{
			return new ArgumentException(Strings.StartEndMustBeOrdered);
		}

		// Token: 0x0600100E RID: 4110 RVA: 0x00032613 File Offset: 0x00030813
		internal static Exception FaultCannotHaveCatchOrFinally(string paramName)
		{
			return new ArgumentException(Strings.FaultCannotHaveCatchOrFinally, paramName);
		}

		// Token: 0x0600100F RID: 4111 RVA: 0x00032620 File Offset: 0x00030820
		internal static Exception TryMustHaveCatchFinallyOrFault()
		{
			return new ArgumentException(Strings.TryMustHaveCatchFinallyOrFault);
		}

		// Token: 0x06001010 RID: 4112 RVA: 0x0003262C File Offset: 0x0003082C
		internal static Exception BodyOfCatchMustHaveSameTypeAsBodyOfTry()
		{
			return new ArgumentException(Strings.BodyOfCatchMustHaveSameTypeAsBodyOfTry);
		}

		// Token: 0x06001011 RID: 4113 RVA: 0x00032638 File Offset: 0x00030838
		internal static Exception ExtensionNodeMustOverrideProperty(object p0)
		{
			return new InvalidOperationException(Strings.ExtensionNodeMustOverrideProperty(p0));
		}

		// Token: 0x06001012 RID: 4114 RVA: 0x00032645 File Offset: 0x00030845
		internal static Exception UserDefinedOperatorMustBeStatic(object p0, string paramName)
		{
			return new ArgumentException(Strings.UserDefinedOperatorMustBeStatic(p0), paramName);
		}

		// Token: 0x06001013 RID: 4115 RVA: 0x00032653 File Offset: 0x00030853
		internal static Exception UserDefinedOperatorMustNotBeVoid(object p0, string paramName)
		{
			return new ArgumentException(Strings.UserDefinedOperatorMustNotBeVoid(p0), paramName);
		}

		// Token: 0x06001014 RID: 4116 RVA: 0x00032661 File Offset: 0x00030861
		internal static Exception CoercionOperatorNotDefined(object p0, object p1)
		{
			return new InvalidOperationException(Strings.CoercionOperatorNotDefined(p0, p1));
		}

		// Token: 0x06001015 RID: 4117 RVA: 0x0003266F File Offset: 0x0003086F
		internal static Exception UnaryOperatorNotDefined(object p0, object p1)
		{
			return new InvalidOperationException(Strings.UnaryOperatorNotDefined(p0, p1));
		}

		// Token: 0x06001016 RID: 4118 RVA: 0x0003267D File Offset: 0x0003087D
		internal static Exception BinaryOperatorNotDefined(object p0, object p1, object p2)
		{
			return new InvalidOperationException(Strings.BinaryOperatorNotDefined(p0, p1, p2));
		}

		// Token: 0x06001017 RID: 4119 RVA: 0x0003268C File Offset: 0x0003088C
		internal static Exception ReferenceEqualityNotDefined(object p0, object p1)
		{
			return new InvalidOperationException(Strings.ReferenceEqualityNotDefined(p0, p1));
		}

		// Token: 0x06001018 RID: 4120 RVA: 0x0003269A File Offset: 0x0003089A
		internal static Exception OperandTypesDoNotMatchParameters(object p0, object p1)
		{
			return new InvalidOperationException(Strings.OperandTypesDoNotMatchParameters(p0, p1));
		}

		// Token: 0x06001019 RID: 4121 RVA: 0x000326A8 File Offset: 0x000308A8
		internal static Exception OverloadOperatorTypeDoesNotMatchConversionType(object p0, object p1)
		{
			return new InvalidOperationException(Strings.OverloadOperatorTypeDoesNotMatchConversionType(p0, p1));
		}

		// Token: 0x0600101A RID: 4122 RVA: 0x000326B6 File Offset: 0x000308B6
		internal static Exception ConversionIsNotSupportedForArithmeticTypes()
		{
			return new InvalidOperationException(Strings.ConversionIsNotSupportedForArithmeticTypes);
		}

		// Token: 0x0600101B RID: 4123 RVA: 0x000326C2 File Offset: 0x000308C2
		internal static Exception ArgumentTypeCannotBeVoid()
		{
			return new ArgumentException(Strings.ArgumentTypeCannotBeVoid);
		}

		// Token: 0x0600101C RID: 4124 RVA: 0x000326CE File Offset: 0x000308CE
		internal static Exception ArgumentMustBeArray(string paramName)
		{
			return new ArgumentException(Strings.ArgumentMustBeArray, paramName);
		}

		// Token: 0x0600101D RID: 4125 RVA: 0x000326DB File Offset: 0x000308DB
		internal static Exception ArgumentMustBeBoolean(string paramName)
		{
			return new ArgumentException(Strings.ArgumentMustBeBoolean, paramName);
		}

		// Token: 0x0600101E RID: 4126 RVA: 0x000326E8 File Offset: 0x000308E8
		internal static Exception EqualityMustReturnBoolean(object p0, string paramName)
		{
			return new ArgumentException(Strings.EqualityMustReturnBoolean(p0), paramName);
		}

		// Token: 0x0600101F RID: 4127 RVA: 0x000326F6 File Offset: 0x000308F6
		internal static Exception ArgumentMustBeFieldInfoOrPropertyInfo(string paramName)
		{
			return new ArgumentException(Strings.ArgumentMustBeFieldInfoOrPropertyInfo, paramName);
		}

		// Token: 0x06001020 RID: 4128 RVA: 0x00032703 File Offset: 0x00030903
		private static Exception ArgumentMustBeFieldInfoOrPropertyInfoOrMethod(string paramName)
		{
			return new ArgumentException(Strings.ArgumentMustBeFieldInfoOrPropertyInfoOrMethod, paramName);
		}

		// Token: 0x06001021 RID: 4129 RVA: 0x00032710 File Offset: 0x00030910
		internal static Exception ArgumentMustBeFieldInfoOrPropertyInfoOrMethod(string paramName, int index)
		{
			return Error.ArgumentMustBeFieldInfoOrPropertyInfoOrMethod(Error.GetParamName(paramName, index));
		}

		// Token: 0x06001022 RID: 4130 RVA: 0x0003271E File Offset: 0x0003091E
		private static Exception ArgumentMustBeInstanceMember(string paramName)
		{
			return new ArgumentException(Strings.ArgumentMustBeInstanceMember, paramName);
		}

		// Token: 0x06001023 RID: 4131 RVA: 0x0003272B File Offset: 0x0003092B
		internal static Exception ArgumentMustBeInstanceMember(string paramName, int index)
		{
			return Error.ArgumentMustBeInstanceMember(Error.GetParamName(paramName, index));
		}

		// Token: 0x06001024 RID: 4132 RVA: 0x00032739 File Offset: 0x00030939
		private static Exception ArgumentMustBeInteger(string paramName)
		{
			return new ArgumentException(Strings.ArgumentMustBeInteger, paramName);
		}

		// Token: 0x06001025 RID: 4133 RVA: 0x00032746 File Offset: 0x00030946
		internal static Exception ArgumentMustBeInteger(string paramName, int index)
		{
			return Error.ArgumentMustBeInteger(Error.GetParamName(paramName, index));
		}

		// Token: 0x06001026 RID: 4134 RVA: 0x00032754 File Offset: 0x00030954
		internal static Exception ArgumentMustBeArrayIndexType(string paramName)
		{
			return new ArgumentException(Strings.ArgumentMustBeArrayIndexType, paramName);
		}

		// Token: 0x06001027 RID: 4135 RVA: 0x00032761 File Offset: 0x00030961
		internal static Exception ArgumentMustBeArrayIndexType(string paramName, int index)
		{
			return Error.ArgumentMustBeArrayIndexType(Error.GetParamName(paramName, index));
		}

		// Token: 0x06001028 RID: 4136 RVA: 0x0003276F File Offset: 0x0003096F
		internal static Exception ArgumentMustBeSingleDimensionalArrayType(string paramName)
		{
			return new ArgumentException(Strings.ArgumentMustBeSingleDimensionalArrayType, paramName);
		}

		// Token: 0x06001029 RID: 4137 RVA: 0x0003277C File Offset: 0x0003097C
		internal static Exception ArgumentTypesMustMatch()
		{
			return new ArgumentException(Strings.ArgumentTypesMustMatch);
		}

		// Token: 0x0600102A RID: 4138 RVA: 0x00032788 File Offset: 0x00030988
		internal static Exception ArgumentTypesMustMatch(string paramName)
		{
			return new ArgumentException(Strings.ArgumentTypesMustMatch, paramName);
		}

		// Token: 0x0600102B RID: 4139 RVA: 0x00032795 File Offset: 0x00030995
		internal static Exception CannotAutoInitializeValueTypeElementThroughProperty(object p0)
		{
			return new InvalidOperationException(Strings.CannotAutoInitializeValueTypeElementThroughProperty(p0));
		}

		// Token: 0x0600102C RID: 4140 RVA: 0x000327A2 File Offset: 0x000309A2
		internal static Exception CannotAutoInitializeValueTypeMemberThroughProperty(object p0)
		{
			return new InvalidOperationException(Strings.CannotAutoInitializeValueTypeMemberThroughProperty(p0));
		}

		// Token: 0x0600102D RID: 4141 RVA: 0x000327AF File Offset: 0x000309AF
		internal static Exception IncorrectTypeForTypeAs(object p0, string paramName)
		{
			return new ArgumentException(Strings.IncorrectTypeForTypeAs(p0), paramName);
		}

		// Token: 0x0600102E RID: 4142 RVA: 0x000327BD File Offset: 0x000309BD
		internal static Exception CoalesceUsedOnNonNullType()
		{
			return new InvalidOperationException(Strings.CoalesceUsedOnNonNullType);
		}

		// Token: 0x0600102F RID: 4143 RVA: 0x000327C9 File Offset: 0x000309C9
		internal static Exception ExpressionTypeCannotInitializeArrayType(object p0, object p1)
		{
			return new InvalidOperationException(Strings.ExpressionTypeCannotInitializeArrayType(p0, p1));
		}

		// Token: 0x06001030 RID: 4144 RVA: 0x000327D7 File Offset: 0x000309D7
		private static Exception ArgumentTypeDoesNotMatchMember(object p0, object p1, string paramName)
		{
			return new ArgumentException(Strings.ArgumentTypeDoesNotMatchMember(p0, p1), paramName);
		}

		// Token: 0x06001031 RID: 4145 RVA: 0x000327E6 File Offset: 0x000309E6
		internal static Exception ArgumentTypeDoesNotMatchMember(object p0, object p1, string paramName, int index)
		{
			return Error.ArgumentTypeDoesNotMatchMember(p0, p1, Error.GetParamName(paramName, index));
		}

		// Token: 0x06001032 RID: 4146 RVA: 0x000327F6 File Offset: 0x000309F6
		private static Exception ArgumentMemberNotDeclOnType(object p0, object p1, string paramName)
		{
			return new ArgumentException(Strings.ArgumentMemberNotDeclOnType(p0, p1), paramName);
		}

		// Token: 0x06001033 RID: 4147 RVA: 0x00032805 File Offset: 0x00030A05
		internal static Exception ArgumentMemberNotDeclOnType(object p0, object p1, string paramName, int index)
		{
			return Error.ArgumentMemberNotDeclOnType(p0, p1, Error.GetParamName(paramName, index));
		}

		// Token: 0x06001034 RID: 4148 RVA: 0x00032815 File Offset: 0x00030A15
		internal static Exception ExpressionTypeDoesNotMatchReturn(object p0, object p1)
		{
			return new ArgumentException(Strings.ExpressionTypeDoesNotMatchReturn(p0, p1));
		}

		// Token: 0x06001035 RID: 4149 RVA: 0x00032823 File Offset: 0x00030A23
		internal static Exception ExpressionTypeDoesNotMatchAssignment(object p0, object p1)
		{
			return new ArgumentException(Strings.ExpressionTypeDoesNotMatchAssignment(p0, p1));
		}

		// Token: 0x06001036 RID: 4150 RVA: 0x00032831 File Offset: 0x00030A31
		internal static Exception ExpressionTypeDoesNotMatchLabel(object p0, object p1)
		{
			return new ArgumentException(Strings.ExpressionTypeDoesNotMatchLabel(p0, p1));
		}

		// Token: 0x06001037 RID: 4151 RVA: 0x0003283F File Offset: 0x00030A3F
		internal static Exception ExpressionTypeNotInvocable(object p0, string paramName)
		{
			return new ArgumentException(Strings.ExpressionTypeNotInvocable(p0), paramName);
		}

		// Token: 0x06001038 RID: 4152 RVA: 0x0003284D File Offset: 0x00030A4D
		internal static Exception FieldNotDefinedForType(object p0, object p1)
		{
			return new ArgumentException(Strings.FieldNotDefinedForType(p0, p1));
		}

		// Token: 0x06001039 RID: 4153 RVA: 0x0003285B File Offset: 0x00030A5B
		internal static Exception InstanceFieldNotDefinedForType(object p0, object p1)
		{
			return new ArgumentException(Strings.InstanceFieldNotDefinedForType(p0, p1));
		}

		// Token: 0x0600103A RID: 4154 RVA: 0x00032869 File Offset: 0x00030A69
		internal static Exception FieldInfoNotDefinedForType(object p0, object p1, object p2)
		{
			return new ArgumentException(Strings.FieldInfoNotDefinedForType(p0, p1, p2));
		}

		// Token: 0x0600103B RID: 4155 RVA: 0x00032878 File Offset: 0x00030A78
		internal static Exception IncorrectNumberOfIndexes()
		{
			return new ArgumentException(Strings.IncorrectNumberOfIndexes);
		}

		// Token: 0x0600103C RID: 4156 RVA: 0x00032884 File Offset: 0x00030A84
		internal static Exception IncorrectNumberOfLambdaDeclarationParameters()
		{
			return new ArgumentException(Strings.IncorrectNumberOfLambdaDeclarationParameters);
		}

		// Token: 0x0600103D RID: 4157 RVA: 0x00032890 File Offset: 0x00030A90
		internal static Exception IncorrectNumberOfMembersForGivenConstructor()
		{
			return new ArgumentException(Strings.IncorrectNumberOfMembersForGivenConstructor);
		}

		// Token: 0x0600103E RID: 4158 RVA: 0x0003289C File Offset: 0x00030A9C
		internal static Exception IncorrectNumberOfArgumentsForMembers()
		{
			return new ArgumentException(Strings.IncorrectNumberOfArgumentsForMembers);
		}

		// Token: 0x0600103F RID: 4159 RVA: 0x000328A8 File Offset: 0x00030AA8
		internal static Exception LambdaTypeMustBeDerivedFromSystemDelegate(string paramName)
		{
			return new ArgumentException(Strings.LambdaTypeMustBeDerivedFromSystemDelegate, paramName);
		}

		// Token: 0x06001040 RID: 4160 RVA: 0x000328B5 File Offset: 0x00030AB5
		internal static Exception MemberNotFieldOrProperty(object p0, string paramName)
		{
			return new ArgumentException(Strings.MemberNotFieldOrProperty(p0), paramName);
		}

		// Token: 0x06001041 RID: 4161 RVA: 0x000328C3 File Offset: 0x00030AC3
		internal static Exception MethodContainsGenericParameters(object p0, string paramName)
		{
			return new ArgumentException(Strings.MethodContainsGenericParameters(p0), paramName);
		}

		// Token: 0x06001042 RID: 4162 RVA: 0x000328D1 File Offset: 0x00030AD1
		internal static Exception MethodIsGeneric(object p0, string paramName)
		{
			return new ArgumentException(Strings.MethodIsGeneric(p0), paramName);
		}

		// Token: 0x06001043 RID: 4163 RVA: 0x000328DF File Offset: 0x00030ADF
		private static Exception MethodNotPropertyAccessor(object p0, object p1, string paramName)
		{
			return new ArgumentException(Strings.MethodNotPropertyAccessor(p0, p1), paramName);
		}

		// Token: 0x06001044 RID: 4164 RVA: 0x000328EE File Offset: 0x00030AEE
		internal static Exception MethodNotPropertyAccessor(object p0, object p1, string paramName, int index)
		{
			return Error.MethodNotPropertyAccessor(p0, p1, Error.GetParamName(paramName, index));
		}

		// Token: 0x06001045 RID: 4165 RVA: 0x000328FE File Offset: 0x00030AFE
		internal static Exception PropertyDoesNotHaveGetter(object p0, string paramName)
		{
			return new ArgumentException(Strings.PropertyDoesNotHaveGetter(p0), paramName);
		}

		// Token: 0x06001046 RID: 4166 RVA: 0x0003290C File Offset: 0x00030B0C
		internal static Exception PropertyDoesNotHaveGetter(object p0, string paramName, int index)
		{
			return Error.PropertyDoesNotHaveGetter(p0, Error.GetParamName(paramName, index));
		}

		// Token: 0x06001047 RID: 4167 RVA: 0x0003291B File Offset: 0x00030B1B
		internal static Exception PropertyDoesNotHaveSetter(object p0, string paramName)
		{
			return new ArgumentException(Strings.PropertyDoesNotHaveSetter(p0), paramName);
		}

		// Token: 0x06001048 RID: 4168 RVA: 0x00032929 File Offset: 0x00030B29
		internal static Exception PropertyDoesNotHaveAccessor(object p0, string paramName)
		{
			return new ArgumentException(Strings.PropertyDoesNotHaveAccessor(p0), paramName);
		}

		// Token: 0x06001049 RID: 4169 RVA: 0x00032937 File Offset: 0x00030B37
		internal static Exception NotAMemberOfType(object p0, object p1, string paramName)
		{
			return new ArgumentException(Strings.NotAMemberOfType(p0, p1), paramName);
		}

		// Token: 0x0600104A RID: 4170 RVA: 0x00032946 File Offset: 0x00030B46
		internal static Exception NotAMemberOfType(object p0, object p1, string paramName, int index)
		{
			return Error.NotAMemberOfType(p0, p1, Error.GetParamName(paramName, index));
		}

		// Token: 0x0600104B RID: 4171 RVA: 0x00032956 File Offset: 0x00030B56
		internal static Exception NotAMemberOfAnyType(object p0, string paramName)
		{
			return new ArgumentException(Strings.NotAMemberOfAnyType(p0), paramName);
		}

		// Token: 0x0600104C RID: 4172 RVA: 0x00032964 File Offset: 0x00030B64
		internal static Exception ParameterExpressionNotValidAsDelegate(object p0, object p1)
		{
			return new ArgumentException(Strings.ParameterExpressionNotValidAsDelegate(p0, p1));
		}

		// Token: 0x0600104D RID: 4173 RVA: 0x00032972 File Offset: 0x00030B72
		internal static Exception PropertyNotDefinedForType(object p0, object p1, string paramName)
		{
			return new ArgumentException(Strings.PropertyNotDefinedForType(p0, p1), paramName);
		}

		// Token: 0x0600104E RID: 4174 RVA: 0x00032981 File Offset: 0x00030B81
		internal static Exception InstancePropertyNotDefinedForType(object p0, object p1, string paramName)
		{
			return new ArgumentException(Strings.InstancePropertyNotDefinedForType(p0, p1), paramName);
		}

		// Token: 0x0600104F RID: 4175 RVA: 0x00032990 File Offset: 0x00030B90
		internal static Exception InstancePropertyWithoutParameterNotDefinedForType(object p0, object p1)
		{
			return new ArgumentException(Strings.InstancePropertyWithoutParameterNotDefinedForType(p0, p1));
		}

		// Token: 0x06001050 RID: 4176 RVA: 0x0003299E File Offset: 0x00030B9E
		internal static Exception InstancePropertyWithSpecifiedParametersNotDefinedForType(object p0, object p1, object p2, string paramName)
		{
			return new ArgumentException(Strings.InstancePropertyWithSpecifiedParametersNotDefinedForType(p0, p1, p2), paramName);
		}

		// Token: 0x06001051 RID: 4177 RVA: 0x000329AE File Offset: 0x00030BAE
		internal static Exception InstanceAndMethodTypeMismatch(object p0, object p1, object p2)
		{
			return new ArgumentException(Strings.InstanceAndMethodTypeMismatch(p0, p1, p2));
		}

		// Token: 0x06001052 RID: 4178 RVA: 0x000329BD File Offset: 0x00030BBD
		internal static Exception TypeMissingDefaultConstructor(object p0, string paramName)
		{
			return new ArgumentException(Strings.TypeMissingDefaultConstructor(p0), paramName);
		}

		// Token: 0x06001053 RID: 4179 RVA: 0x000329CB File Offset: 0x00030BCB
		internal static Exception ElementInitializerMethodNotAdd(string paramName)
		{
			return new ArgumentException(Strings.ElementInitializerMethodNotAdd, paramName);
		}

		// Token: 0x06001054 RID: 4180 RVA: 0x000329D8 File Offset: 0x00030BD8
		internal static Exception ElementInitializerMethodNoRefOutParam(object p0, object p1, string paramName)
		{
			return new ArgumentException(Strings.ElementInitializerMethodNoRefOutParam(p0, p1), paramName);
		}

		// Token: 0x06001055 RID: 4181 RVA: 0x000329E7 File Offset: 0x00030BE7
		internal static Exception ElementInitializerMethodWithZeroArgs(string paramName)
		{
			return new ArgumentException(Strings.ElementInitializerMethodWithZeroArgs, paramName);
		}

		// Token: 0x06001056 RID: 4182 RVA: 0x000329F4 File Offset: 0x00030BF4
		internal static Exception ElementInitializerMethodStatic(string paramName)
		{
			return new ArgumentException(Strings.ElementInitializerMethodStatic, paramName);
		}

		// Token: 0x06001057 RID: 4183 RVA: 0x00032A01 File Offset: 0x00030C01
		internal static Exception TypeNotIEnumerable(object p0, string paramName)
		{
			return new ArgumentException(Strings.TypeNotIEnumerable(p0), paramName);
		}

		// Token: 0x06001058 RID: 4184 RVA: 0x00032A0F File Offset: 0x00030C0F
		internal static Exception UnhandledBinary(object p0, string paramName)
		{
			return new ArgumentException(Strings.UnhandledBinary(p0), paramName);
		}

		// Token: 0x06001059 RID: 4185 RVA: 0x00032A1D File Offset: 0x00030C1D
		internal static Exception UnhandledBinding()
		{
			return new ArgumentException(Strings.UnhandledBinding);
		}

		// Token: 0x0600105A RID: 4186 RVA: 0x00032A29 File Offset: 0x00030C29
		internal static Exception UnhandledBindingType(object p0)
		{
			return new ArgumentException(Strings.UnhandledBindingType(p0));
		}

		// Token: 0x0600105B RID: 4187 RVA: 0x00032A36 File Offset: 0x00030C36
		internal static Exception UnhandledUnary(object p0, string paramName)
		{
			return new ArgumentException(Strings.UnhandledUnary(p0), paramName);
		}

		// Token: 0x0600105C RID: 4188 RVA: 0x00032A44 File Offset: 0x00030C44
		internal static Exception UnknownBindingType(int index)
		{
			return new ArgumentException(Strings.UnknownBindingType, string.Format("bindings[{0}]", index));
		}

		// Token: 0x0600105D RID: 4189 RVA: 0x00032A60 File Offset: 0x00030C60
		internal static Exception UserDefinedOpMustHaveConsistentTypes(object p0, object p1)
		{
			return new ArgumentException(Strings.UserDefinedOpMustHaveConsistentTypes(p0, p1));
		}

		// Token: 0x0600105E RID: 4190 RVA: 0x00032A6E File Offset: 0x00030C6E
		internal static Exception UserDefinedOpMustHaveValidReturnType(object p0, object p1)
		{
			return new ArgumentException(Strings.UserDefinedOpMustHaveValidReturnType(p0, p1));
		}

		// Token: 0x0600105F RID: 4191 RVA: 0x00032A7C File Offset: 0x00030C7C
		internal static Exception LogicalOperatorMustHaveBooleanOperators(object p0, object p1)
		{
			return new ArgumentException(Strings.LogicalOperatorMustHaveBooleanOperators(p0, p1));
		}

		// Token: 0x06001060 RID: 4192 RVA: 0x00032A8A File Offset: 0x00030C8A
		internal static Exception MethodWithArgsDoesNotExistOnType(object p0, object p1)
		{
			return new InvalidOperationException(Strings.MethodWithArgsDoesNotExistOnType(p0, p1));
		}

		// Token: 0x06001061 RID: 4193 RVA: 0x00032A98 File Offset: 0x00030C98
		internal static Exception GenericMethodWithArgsDoesNotExistOnType(object p0, object p1)
		{
			return new InvalidOperationException(Strings.GenericMethodWithArgsDoesNotExistOnType(p0, p1));
		}

		// Token: 0x06001062 RID: 4194 RVA: 0x00032AA6 File Offset: 0x00030CA6
		internal static Exception MethodWithMoreThanOneMatch(object p0, object p1)
		{
			return new InvalidOperationException(Strings.MethodWithMoreThanOneMatch(p0, p1));
		}

		// Token: 0x06001063 RID: 4195 RVA: 0x00032AB4 File Offset: 0x00030CB4
		internal static Exception PropertyWithMoreThanOneMatch(object p0, object p1)
		{
			return new InvalidOperationException(Strings.PropertyWithMoreThanOneMatch(p0, p1));
		}

		// Token: 0x06001064 RID: 4196 RVA: 0x00032AC2 File Offset: 0x00030CC2
		internal static Exception IncorrectNumberOfTypeArgsForFunc(string paramName)
		{
			return new ArgumentException(Strings.IncorrectNumberOfTypeArgsForFunc, paramName);
		}

		// Token: 0x06001065 RID: 4197 RVA: 0x00032ACF File Offset: 0x00030CCF
		internal static Exception IncorrectNumberOfTypeArgsForAction(string paramName)
		{
			return new ArgumentException(Strings.IncorrectNumberOfTypeArgsForAction, paramName);
		}

		// Token: 0x06001066 RID: 4198 RVA: 0x00032ADC File Offset: 0x00030CDC
		internal static Exception ArgumentCannotBeOfTypeVoid(string paramName)
		{
			return new ArgumentException(Strings.ArgumentCannotBeOfTypeVoid, paramName);
		}

		// Token: 0x06001067 RID: 4199 RVA: 0x00032AE9 File Offset: 0x00030CE9
		internal static Exception OutOfRange(string paramName, object p1)
		{
			return new ArgumentOutOfRangeException(paramName, Strings.OutOfRange(paramName, p1));
		}

		// Token: 0x06001068 RID: 4200 RVA: 0x00032AF8 File Offset: 0x00030CF8
		internal static Exception LabelTargetAlreadyDefined(object p0)
		{
			return new InvalidOperationException(Strings.LabelTargetAlreadyDefined(p0));
		}

		// Token: 0x06001069 RID: 4201 RVA: 0x00032B05 File Offset: 0x00030D05
		internal static Exception LabelTargetUndefined(object p0)
		{
			return new InvalidOperationException(Strings.LabelTargetUndefined(p0));
		}

		// Token: 0x0600106A RID: 4202 RVA: 0x00032B12 File Offset: 0x00030D12
		internal static Exception ControlCannotLeaveFinally()
		{
			return new InvalidOperationException(Strings.ControlCannotLeaveFinally);
		}

		// Token: 0x0600106B RID: 4203 RVA: 0x00032B1E File Offset: 0x00030D1E
		internal static Exception ControlCannotLeaveFilterTest()
		{
			return new InvalidOperationException(Strings.ControlCannotLeaveFilterTest);
		}

		// Token: 0x0600106C RID: 4204 RVA: 0x00032B2A File Offset: 0x00030D2A
		internal static Exception AmbiguousJump(object p0)
		{
			return new InvalidOperationException(Strings.AmbiguousJump(p0));
		}

		// Token: 0x0600106D RID: 4205 RVA: 0x00032B37 File Offset: 0x00030D37
		internal static Exception ControlCannotEnterTry()
		{
			return new InvalidOperationException(Strings.ControlCannotEnterTry);
		}

		// Token: 0x0600106E RID: 4206 RVA: 0x00032B43 File Offset: 0x00030D43
		internal static Exception ControlCannotEnterExpression()
		{
			return new InvalidOperationException(Strings.ControlCannotEnterExpression);
		}

		// Token: 0x0600106F RID: 4207 RVA: 0x00032B4F File Offset: 0x00030D4F
		internal static Exception NonLocalJumpWithValue(object p0)
		{
			return new InvalidOperationException(Strings.NonLocalJumpWithValue(p0));
		}

		// Token: 0x06001070 RID: 4208 RVA: 0x00032B5C File Offset: 0x00030D5C
		internal static Exception InvalidLvalue(ExpressionType p0)
		{
			return new InvalidOperationException(Strings.InvalidLvalue(p0));
		}

		// Token: 0x06001071 RID: 4209 RVA: 0x00032B6E File Offset: 0x00030D6E
		internal static Exception UndefinedVariable(object p0, object p1, object p2)
		{
			return new InvalidOperationException(Strings.UndefinedVariable(p0, p1, p2));
		}

		// Token: 0x06001072 RID: 4210 RVA: 0x00032B7D File Offset: 0x00030D7D
		internal static Exception CannotCloseOverByRef(object p0, object p1)
		{
			return new InvalidOperationException(Strings.CannotCloseOverByRef(p0, p1));
		}

		// Token: 0x06001073 RID: 4211 RVA: 0x00032B8B File Offset: 0x00030D8B
		internal static Exception UnexpectedVarArgsCall(object p0)
		{
			return new InvalidOperationException(Strings.UnexpectedVarArgsCall(p0));
		}

		// Token: 0x06001074 RID: 4212 RVA: 0x00032B98 File Offset: 0x00030D98
		internal static Exception RethrowRequiresCatch()
		{
			return new InvalidOperationException(Strings.RethrowRequiresCatch);
		}

		// Token: 0x06001075 RID: 4213 RVA: 0x00032BA4 File Offset: 0x00030DA4
		internal static Exception TryNotAllowedInFilter()
		{
			return new InvalidOperationException(Strings.TryNotAllowedInFilter);
		}

		// Token: 0x06001076 RID: 4214 RVA: 0x00032BB0 File Offset: 0x00030DB0
		internal static Exception MustRewriteToSameNode(object p0, object p1, object p2)
		{
			return new InvalidOperationException(Strings.MustRewriteToSameNode(p0, p1, p2));
		}

		// Token: 0x06001077 RID: 4215 RVA: 0x00032BBF File Offset: 0x00030DBF
		internal static Exception MustRewriteChildToSameType(object p0, object p1, object p2)
		{
			return new InvalidOperationException(Strings.MustRewriteChildToSameType(p0, p1, p2));
		}

		// Token: 0x06001078 RID: 4216 RVA: 0x00032BCE File Offset: 0x00030DCE
		internal static Exception MustRewriteWithoutMethod(object p0, object p1)
		{
			return new InvalidOperationException(Strings.MustRewriteWithoutMethod(p0, p1));
		}

		// Token: 0x06001079 RID: 4217 RVA: 0x00032BDC File Offset: 0x00030DDC
		internal static Exception TryNotSupportedForMethodsWithRefArgs(object p0)
		{
			return new NotSupportedException(Strings.TryNotSupportedForMethodsWithRefArgs(p0));
		}

		// Token: 0x0600107A RID: 4218 RVA: 0x00032BE9 File Offset: 0x00030DE9
		internal static Exception TryNotSupportedForValueTypeInstances(object p0)
		{
			return new NotSupportedException(Strings.TryNotSupportedForValueTypeInstances(p0));
		}

		// Token: 0x0600107B RID: 4219 RVA: 0x00032BF6 File Offset: 0x00030DF6
		internal static Exception TestValueTypeDoesNotMatchComparisonMethodParameter(object p0, object p1)
		{
			return new ArgumentException(Strings.TestValueTypeDoesNotMatchComparisonMethodParameter(p0, p1));
		}

		// Token: 0x0600107C RID: 4220 RVA: 0x00032C04 File Offset: 0x00030E04
		internal static Exception SwitchValueTypeDoesNotMatchComparisonMethodParameter(object p0, object p1)
		{
			return new ArgumentException(Strings.SwitchValueTypeDoesNotMatchComparisonMethodParameter(p0, p1));
		}

		// Token: 0x0600107D RID: 4221 RVA: 0x0000C509 File Offset: 0x0000A709
		internal static Exception ArgumentOutOfRange(string paramName)
		{
			return new ArgumentOutOfRangeException(paramName);
		}

		// Token: 0x0600107E RID: 4222 RVA: 0x0000C541 File Offset: 0x0000A741
		internal static Exception NotSupported()
		{
			return new NotSupportedException();
		}

		// Token: 0x0600107F RID: 4223 RVA: 0x00032C12 File Offset: 0x00030E12
		internal static Exception NonStaticConstructorRequired(string paramName)
		{
			return new ArgumentException(Strings.NonStaticConstructorRequired, paramName);
		}

		// Token: 0x06001080 RID: 4224 RVA: 0x00032C1F File Offset: 0x00030E1F
		internal static Exception NonAbstractConstructorRequired()
		{
			return new InvalidOperationException(Strings.NonAbstractConstructorRequired);
		}

		// Token: 0x06001081 RID: 4225 RVA: 0x00032C2B File Offset: 0x00030E2B
		internal static Exception InvalidProgram()
		{
			return new InvalidProgramException();
		}

		// Token: 0x06001082 RID: 4226 RVA: 0x00032C32 File Offset: 0x00030E32
		internal static Exception EnumerationIsDone()
		{
			return new InvalidOperationException(Strings.EnumerationIsDone);
		}

		// Token: 0x06001083 RID: 4227 RVA: 0x00032C3E File Offset: 0x00030E3E
		private static Exception TypeContainsGenericParameters(object p0, string paramName)
		{
			return new ArgumentException(Strings.TypeContainsGenericParameters(p0), paramName);
		}

		// Token: 0x06001084 RID: 4228 RVA: 0x00032C4C File Offset: 0x00030E4C
		internal static Exception TypeContainsGenericParameters(object p0, string paramName, int index)
		{
			return Error.TypeContainsGenericParameters(p0, Error.GetParamName(paramName, index));
		}

		// Token: 0x06001085 RID: 4229 RVA: 0x00032C5B File Offset: 0x00030E5B
		internal static Exception TypeIsGeneric(object p0, string paramName)
		{
			return new ArgumentException(Strings.TypeIsGeneric(p0), paramName);
		}

		// Token: 0x06001086 RID: 4230 RVA: 0x00032C69 File Offset: 0x00030E69
		internal static Exception TypeIsGeneric(object p0, string paramName, int index)
		{
			return Error.TypeIsGeneric(p0, Error.GetParamName(paramName, index));
		}

		// Token: 0x06001087 RID: 4231 RVA: 0x00032C78 File Offset: 0x00030E78
		internal static Exception IncorrectNumberOfConstructorArguments()
		{
			return new ArgumentException(Strings.IncorrectNumberOfConstructorArguments);
		}

		// Token: 0x06001088 RID: 4232 RVA: 0x00032C84 File Offset: 0x00030E84
		internal static Exception ExpressionTypeDoesNotMatchMethodParameter(object p0, object p1, object p2, string paramName)
		{
			return new ArgumentException(Strings.ExpressionTypeDoesNotMatchMethodParameter(p0, p1, p2), paramName);
		}

		// Token: 0x06001089 RID: 4233 RVA: 0x00032C94 File Offset: 0x00030E94
		internal static Exception ExpressionTypeDoesNotMatchMethodParameter(object p0, object p1, object p2, string paramName, int index)
		{
			return Error.ExpressionTypeDoesNotMatchMethodParameter(p0, p1, p2, Error.GetParamName(paramName, index));
		}

		// Token: 0x0600108A RID: 4234 RVA: 0x00032CA6 File Offset: 0x00030EA6
		internal static Exception ExpressionTypeDoesNotMatchParameter(object p0, object p1, string paramName)
		{
			return new ArgumentException(Strings.ExpressionTypeDoesNotMatchParameter(p0, p1), paramName);
		}

		// Token: 0x0600108B RID: 4235 RVA: 0x00032CB5 File Offset: 0x00030EB5
		internal static Exception ExpressionTypeDoesNotMatchParameter(object p0, object p1, string paramName, int index)
		{
			return Error.ExpressionTypeDoesNotMatchParameter(p0, p1, Error.GetParamName(paramName, index));
		}

		// Token: 0x0600108C RID: 4236 RVA: 0x00032CC5 File Offset: 0x00030EC5
		internal static Exception IncorrectNumberOfLambdaArguments()
		{
			return new InvalidOperationException(Strings.IncorrectNumberOfLambdaArguments);
		}

		// Token: 0x0600108D RID: 4237 RVA: 0x00032CD1 File Offset: 0x00030ED1
		internal static Exception IncorrectNumberOfMethodCallArguments(object p0, string paramName)
		{
			return new ArgumentException(Strings.IncorrectNumberOfMethodCallArguments(p0), paramName);
		}

		// Token: 0x0600108E RID: 4238 RVA: 0x00032CDF File Offset: 0x00030EDF
		internal static Exception ExpressionTypeDoesNotMatchConstructorParameter(object p0, object p1, string paramName)
		{
			return new ArgumentException(Strings.ExpressionTypeDoesNotMatchConstructorParameter(p0, p1), paramName);
		}

		// Token: 0x0600108F RID: 4239 RVA: 0x00032CEE File Offset: 0x00030EEE
		internal static Exception ExpressionTypeDoesNotMatchConstructorParameter(object p0, object p1, string paramName, int index)
		{
			return Error.ExpressionTypeDoesNotMatchConstructorParameter(p0, p1, Error.GetParamName(paramName, index));
		}

		// Token: 0x06001090 RID: 4240 RVA: 0x00032CFE File Offset: 0x00030EFE
		internal static Exception ExpressionMustBeReadable(string paramName)
		{
			return new ArgumentException(Strings.ExpressionMustBeReadable, paramName);
		}

		// Token: 0x06001091 RID: 4241 RVA: 0x00032D0B File Offset: 0x00030F0B
		internal static Exception ExpressionMustBeReadable(string paramName, int index)
		{
			return Error.ExpressionMustBeReadable(Error.GetParamName(paramName, index));
		}

		// Token: 0x06001092 RID: 4242 RVA: 0x00032D19 File Offset: 0x00030F19
		internal static Exception InvalidArgumentValue(string paramName)
		{
			return new ArgumentException(Strings.InvalidArgumentValue, paramName);
		}

		// Token: 0x06001093 RID: 4243 RVA: 0x00032D26 File Offset: 0x00030F26
		internal static Exception NonEmptyCollectionRequired(string paramName)
		{
			return new ArgumentException(Strings.NonEmptyCollectionRequired, paramName);
		}

		// Token: 0x06001094 RID: 4244 RVA: 0x00032D33 File Offset: 0x00030F33
		internal static Exception InvalidNullValue(Type type, string paramName)
		{
			return new ArgumentException(Strings.InvalidNullValue(type), paramName);
		}

		// Token: 0x06001095 RID: 4245 RVA: 0x00032D41 File Offset: 0x00030F41
		internal static Exception InvalidTypeException(object value, Type type, string paramName)
		{
			return new ArgumentException(Strings.InvalidObjectType(((value != null) ? value.GetType() : null) ?? "null", type), paramName);
		}

		// Token: 0x06001096 RID: 4246 RVA: 0x00032D64 File Offset: 0x00030F64
		private static string GetParamName(string paramName, int index)
		{
			if (index >= 0)
			{
				return string.Format("{0}[{1}]", paramName, index);
			}
			return paramName;
		}
	}
}
