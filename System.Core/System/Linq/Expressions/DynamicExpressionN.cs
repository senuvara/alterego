﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x02000231 RID: 561
	internal class DynamicExpressionN : DynamicExpression, IArgumentProvider
	{
		// Token: 0x06000F99 RID: 3993 RVA: 0x0003160B File Offset: 0x0002F80B
		internal DynamicExpressionN(Type delegateType, CallSiteBinder binder, IReadOnlyList<Expression> arguments) : base(delegateType, binder)
		{
			this._arguments = arguments;
		}

		// Token: 0x06000F9A RID: 3994 RVA: 0x0003161C File Offset: 0x0002F81C
		Expression IArgumentProvider.GetArgument(int index)
		{
			return this._arguments[index];
		}

		// Token: 0x06000F9B RID: 3995 RVA: 0x0003162A File Offset: 0x0002F82A
		internal override bool SameArguments(ICollection<Expression> arguments)
		{
			return ExpressionUtils.SameElements<Expression>(arguments, this._arguments);
		}

		// Token: 0x170002D9 RID: 729
		// (get) Token: 0x06000F9C RID: 3996 RVA: 0x00031638 File Offset: 0x0002F838
		int IArgumentProvider.ArgumentCount
		{
			get
			{
				return this._arguments.Count;
			}
		}

		// Token: 0x06000F9D RID: 3997 RVA: 0x00031645 File Offset: 0x0002F845
		internal override ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			return ExpressionUtils.ReturnReadOnly<Expression>(ref this._arguments);
		}

		// Token: 0x06000F9E RID: 3998 RVA: 0x00031652 File Offset: 0x0002F852
		internal override DynamicExpression Rewrite(Expression[] args)
		{
			return ExpressionExtension.MakeDynamic(base.DelegateType, base.Binder, args);
		}

		// Token: 0x040008C1 RID: 2241
		private IReadOnlyList<Expression> _arguments;
	}
}
