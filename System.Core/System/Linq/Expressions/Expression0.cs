﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;

namespace System.Linq.Expressions
{
	// Token: 0x02000255 RID: 597
	internal sealed class Expression0<TDelegate> : Expression<TDelegate>
	{
		// Token: 0x06001171 RID: 4465 RVA: 0x00035015 File Offset: 0x00033215
		public Expression0(Expression body) : base(body)
		{
		}

		// Token: 0x17000316 RID: 790
		// (get) Token: 0x06001172 RID: 4466 RVA: 0x00002285 File Offset: 0x00000485
		internal override int ParameterCount
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x06001173 RID: 4467 RVA: 0x0002ED10 File Offset: 0x0002CF10
		internal override bool SameParameters(ICollection<ParameterExpression> parameters)
		{
			return parameters == null || parameters.Count == 0;
		}

		// Token: 0x06001174 RID: 4468 RVA: 0x0003501E File Offset: 0x0003321E
		internal override ParameterExpression GetParameter(int index)
		{
			throw Error.ArgumentOutOfRange("index");
		}

		// Token: 0x06001175 RID: 4469 RVA: 0x0002ED27 File Offset: 0x0002CF27
		internal override ReadOnlyCollection<ParameterExpression> GetOrMakeParameters()
		{
			return EmptyReadOnlyCollection<ParameterExpression>.Instance;
		}

		// Token: 0x06001176 RID: 4470 RVA: 0x0003502A File Offset: 0x0003322A
		internal override Expression<TDelegate> Rewrite(Expression body, ParameterExpression[] parameters)
		{
			return Expression.Lambda<TDelegate>(body, parameters);
		}
	}
}
