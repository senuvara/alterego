﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Dynamic.Utils;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Represents a constructor call that has a collection initializer.</summary>
	// Token: 0x0200025B RID: 603
	[DebuggerTypeProxy(typeof(Expression.ListInitExpressionProxy))]
	public sealed class ListInitExpression : Expression
	{
		// Token: 0x06001192 RID: 4498 RVA: 0x000353C9 File Offset: 0x000335C9
		internal ListInitExpression(NewExpression newExpression, ReadOnlyCollection<ElementInit> initializers)
		{
			this.NewExpression = newExpression;
			this.Initializers = initializers;
		}

		/// <summary>Returns the node type of this <see cref="T:System.Linq.Expressions.Expression" />.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.ExpressionType" /> that represents this expression.</returns>
		// Token: 0x1700031D RID: 797
		// (get) Token: 0x06001193 RID: 4499 RVA: 0x000353DF File Offset: 0x000335DF
		public sealed override ExpressionType NodeType
		{
			get
			{
				return ExpressionType.ListInit;
			}
		}

		/// <summary>Gets the static type of the expression that this <see cref="T:System.Linq.Expressions.Expression" /> represents.</summary>
		/// <returns>The <see cref="P:System.Linq.Expressions.ListInitExpression.Type" /> that represents the static type of the expression.</returns>
		// Token: 0x1700031E RID: 798
		// (get) Token: 0x06001194 RID: 4500 RVA: 0x000353E3 File Offset: 0x000335E3
		public sealed override Type Type
		{
			get
			{
				return this.NewExpression.Type;
			}
		}

		/// <summary>Gets a value that indicates whether the expression tree node can be reduced.</summary>
		/// <returns>True if the node can be reduced, otherwise false.</returns>
		// Token: 0x1700031F RID: 799
		// (get) Token: 0x06001195 RID: 4501 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override bool CanReduce
		{
			get
			{
				return true;
			}
		}

		/// <summary>Gets the expression that contains a call to the constructor of a collection type.</summary>
		/// <returns>A <see cref="T:System.Linq.Expressions.NewExpression" /> that represents the call to the constructor of a collection type.</returns>
		// Token: 0x17000320 RID: 800
		// (get) Token: 0x06001196 RID: 4502 RVA: 0x000353F0 File Offset: 0x000335F0
		public NewExpression NewExpression
		{
			[CompilerGenerated]
			get
			{
				return this.<NewExpression>k__BackingField;
			}
		}

		/// <summary>Gets the element initializers that are used to initialize a collection.</summary>
		/// <returns>A <see cref="T:System.Collections.ObjectModel.ReadOnlyCollection`1" /> of <see cref="T:System.Linq.Expressions.ElementInit" /> objects which represent the elements that are used to initialize the collection.</returns>
		// Token: 0x17000321 RID: 801
		// (get) Token: 0x06001197 RID: 4503 RVA: 0x000353F8 File Offset: 0x000335F8
		public ReadOnlyCollection<ElementInit> Initializers
		{
			[CompilerGenerated]
			get
			{
				return this.<Initializers>k__BackingField;
			}
		}

		// Token: 0x06001198 RID: 4504 RVA: 0x00035400 File Offset: 0x00033600
		protected internal override Expression Accept(ExpressionVisitor visitor)
		{
			return visitor.VisitListInit(this);
		}

		/// <summary>Reduces the binary expression node to a simpler expression.</summary>
		/// <returns>The reduced expression.</returns>
		// Token: 0x06001199 RID: 4505 RVA: 0x00035409 File Offset: 0x00033609
		public override Expression Reduce()
		{
			return MemberInitExpression.ReduceListInit(this.NewExpression, this.Initializers, true);
		}

		/// <summary>Creates a new expression that is like this one, but using the supplied children. If all of the children are the same, it will return this expression.</summary>
		/// <param name="newExpression">The <see cref="P:System.Linq.Expressions.ListInitExpression.NewExpression" /> property of the result.</param>
		/// <param name="initializers">The <see cref="P:System.Linq.Expressions.ListInitExpression.Initializers" /> property of the result.</param>
		/// <returns>This expression if no children are changed or an expression with the updated children.</returns>
		// Token: 0x0600119A RID: 4506 RVA: 0x0003541D File Offset: 0x0003361D
		public ListInitExpression Update(NewExpression newExpression, IEnumerable<ElementInit> initializers)
		{
			if ((newExpression == this.NewExpression & initializers != null) && ExpressionUtils.SameElements<ElementInit>(ref initializers, this.Initializers))
			{
				return this;
			}
			return Expression.ListInit(newExpression, initializers);
		}

		// Token: 0x0600119B RID: 4507 RVA: 0x0000220F File Offset: 0x0000040F
		internal ListInitExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000957 RID: 2391
		[CompilerGenerated]
		private readonly NewExpression <NewExpression>k__BackingField;

		// Token: 0x04000958 RID: 2392
		[CompilerGenerated]
		private readonly ReadOnlyCollection<ElementInit> <Initializers>k__BackingField;
	}
}
