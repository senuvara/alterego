﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Represents a label, which can be put in any <see cref="T:System.Linq.Expressions.Expression" /> context. If it is jumped to, it will get the value provided by the corresponding <see cref="T:System.Linq.Expressions.GotoExpression" />. Otherwise, it receives the value in <see cref="P:System.Linq.Expressions.LabelExpression.DefaultValue" />. If the <see cref="T:System.Type" /> equals System.Void, no value should be provided.</summary>
	// Token: 0x02000250 RID: 592
	[DebuggerTypeProxy(typeof(Expression.LabelExpressionProxy))]
	public sealed class LabelExpression : Expression
	{
		// Token: 0x06001141 RID: 4417 RVA: 0x00034DD7 File Offset: 0x00032FD7
		internal LabelExpression(LabelTarget label, Expression defaultValue)
		{
			this.Target = label;
			this.DefaultValue = defaultValue;
		}

		/// <summary>Gets the static type of the expression that this <see cref="T:System.Linq.Expressions.Expression" /> represents.</summary>
		/// <returns>The <see cref="P:System.Linq.Expressions.LabelExpression.Type" /> that represents the static type of the expression.</returns>
		// Token: 0x17000301 RID: 769
		// (get) Token: 0x06001142 RID: 4418 RVA: 0x00034DED File Offset: 0x00032FED
		public sealed override Type Type
		{
			get
			{
				return this.Target.Type;
			}
		}

		/// <summary>Returns the node type of this <see cref="T:System.Linq.Expressions.Expression" />.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.ExpressionType" /> that represents this expression.</returns>
		// Token: 0x17000302 RID: 770
		// (get) Token: 0x06001143 RID: 4419 RVA: 0x00034DFA File Offset: 0x00032FFA
		public sealed override ExpressionType NodeType
		{
			get
			{
				return ExpressionType.Label;
			}
		}

		/// <summary>The <see cref="T:System.Linq.Expressions.LabelTarget" /> which this label is associated with.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.LabelTarget" /> which this label is associated with.</returns>
		// Token: 0x17000303 RID: 771
		// (get) Token: 0x06001144 RID: 4420 RVA: 0x00034DFE File Offset: 0x00032FFE
		public LabelTarget Target
		{
			[CompilerGenerated]
			get
			{
				return this.<Target>k__BackingField;
			}
		}

		/// <summary>The value of the <see cref="T:System.Linq.Expressions.LabelExpression" /> when the label is reached through regular control flow (for example, is not jumped to).</summary>
		/// <returns>The Expression object representing the value of the <see cref="T:System.Linq.Expressions.LabelExpression" />.</returns>
		// Token: 0x17000304 RID: 772
		// (get) Token: 0x06001145 RID: 4421 RVA: 0x00034E06 File Offset: 0x00033006
		public Expression DefaultValue
		{
			[CompilerGenerated]
			get
			{
				return this.<DefaultValue>k__BackingField;
			}
		}

		// Token: 0x06001146 RID: 4422 RVA: 0x00034E0E File Offset: 0x0003300E
		protected internal override Expression Accept(ExpressionVisitor visitor)
		{
			return visitor.VisitLabel(this);
		}

		/// <summary>Creates a new expression that is like this one, but using the supplied children. If all of the children are the same, it will return this expression.</summary>
		/// <param name="target">The <see cref="P:System.Linq.Expressions.LabelExpression.Target" /> property of the result.</param>
		/// <param name="defaultValue">The <see cref="P:System.Linq.Expressions.LabelExpression.DefaultValue" /> property of the result</param>
		/// <returns>This expression if no children are changed or an expression with the updated children.</returns>
		// Token: 0x06001147 RID: 4423 RVA: 0x00034E17 File Offset: 0x00033017
		public LabelExpression Update(LabelTarget target, Expression defaultValue)
		{
			if (target == this.Target && defaultValue == this.DefaultValue)
			{
				return this;
			}
			return Expression.Label(target, defaultValue);
		}

		// Token: 0x06001148 RID: 4424 RVA: 0x0000220F File Offset: 0x0000040F
		internal LabelExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000949 RID: 2377
		[CompilerGenerated]
		private readonly LabelTarget <Target>k__BackingField;

		// Token: 0x0400094A RID: 2378
		[CompilerGenerated]
		private readonly Expression <DefaultValue>k__BackingField;
	}
}
