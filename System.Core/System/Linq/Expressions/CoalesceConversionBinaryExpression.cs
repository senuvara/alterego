﻿using System;

namespace System.Linq.Expressions
{
	// Token: 0x020001F3 RID: 499
	internal sealed class CoalesceConversionBinaryExpression : BinaryExpression
	{
		// Token: 0x06000C2D RID: 3117 RVA: 0x00027192 File Offset: 0x00025392
		internal CoalesceConversionBinaryExpression(Expression left, Expression right, LambdaExpression conversion) : base(left, right)
		{
			this._conversion = conversion;
		}

		// Token: 0x06000C2E RID: 3118 RVA: 0x000271A3 File Offset: 0x000253A3
		internal override LambdaExpression GetConversion()
		{
			return this._conversion;
		}

		// Token: 0x170001E7 RID: 487
		// (get) Token: 0x06000C2F RID: 3119 RVA: 0x000271AB File Offset: 0x000253AB
		public sealed override ExpressionType NodeType
		{
			get
			{
				return ExpressionType.Coalesce;
			}
		}

		// Token: 0x170001E8 RID: 488
		// (get) Token: 0x06000C30 RID: 3120 RVA: 0x000271AE File Offset: 0x000253AE
		public sealed override Type Type
		{
			get
			{
				return base.Right.Type;
			}
		}

		// Token: 0x04000856 RID: 2134
		private readonly LambdaExpression _conversion;
	}
}
