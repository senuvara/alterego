﻿using System;

namespace System.Linq.Expressions
{
	// Token: 0x0200022B RID: 555
	internal sealed class SpanDebugInfoExpression : DebugInfoExpression
	{
		// Token: 0x06000F19 RID: 3865 RVA: 0x0002F927 File Offset: 0x0002DB27
		internal SpanDebugInfoExpression(SymbolDocumentInfo document, int startLine, int startColumn, int endLine, int endColumn) : base(document)
		{
			this._startLine = startLine;
			this._startColumn = startColumn;
			this._endLine = endLine;
			this._endColumn = endColumn;
		}

		// Token: 0x170002C3 RID: 707
		// (get) Token: 0x06000F1A RID: 3866 RVA: 0x0002F94E File Offset: 0x0002DB4E
		public override int StartLine
		{
			get
			{
				return this._startLine;
			}
		}

		// Token: 0x170002C4 RID: 708
		// (get) Token: 0x06000F1B RID: 3867 RVA: 0x0002F956 File Offset: 0x0002DB56
		public override int StartColumn
		{
			get
			{
				return this._startColumn;
			}
		}

		// Token: 0x170002C5 RID: 709
		// (get) Token: 0x06000F1C RID: 3868 RVA: 0x0002F95E File Offset: 0x0002DB5E
		public override int EndLine
		{
			get
			{
				return this._endLine;
			}
		}

		// Token: 0x170002C6 RID: 710
		// (get) Token: 0x06000F1D RID: 3869 RVA: 0x0002F966 File Offset: 0x0002DB66
		public override int EndColumn
		{
			get
			{
				return this._endColumn;
			}
		}

		// Token: 0x170002C7 RID: 711
		// (get) Token: 0x06000F1E RID: 3870 RVA: 0x00002285 File Offset: 0x00000485
		public override bool IsClear
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06000F1F RID: 3871 RVA: 0x0002F91E File Offset: 0x0002DB1E
		protected internal override Expression Accept(ExpressionVisitor visitor)
		{
			return visitor.VisitDebugInfo(this);
		}

		// Token: 0x040008AA RID: 2218
		private readonly int _startLine;

		// Token: 0x040008AB RID: 2219
		private readonly int _startColumn;

		// Token: 0x040008AC RID: 2220
		private readonly int _endLine;

		// Token: 0x040008AD RID: 2221
		private readonly int _endColumn;
	}
}
