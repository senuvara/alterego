﻿using System;
using System.Reflection;

namespace System.Linq.Expressions
{
	// Token: 0x02000261 RID: 609
	internal sealed class FieldExpression : MemberExpression
	{
		// Token: 0x060011BA RID: 4538 RVA: 0x000355C3 File Offset: 0x000337C3
		public FieldExpression(Expression expression, FieldInfo member) : base(expression)
		{
			this._field = member;
		}

		// Token: 0x060011BB RID: 4539 RVA: 0x000355D3 File Offset: 0x000337D3
		internal override MemberInfo GetMember()
		{
			return this._field;
		}

		// Token: 0x1700032D RID: 813
		// (get) Token: 0x060011BC RID: 4540 RVA: 0x000355DB File Offset: 0x000337DB
		public sealed override Type Type
		{
			get
			{
				return this._field.FieldType;
			}
		}

		// Token: 0x04000964 RID: 2404
		private readonly FieldInfo _field;
	}
}
