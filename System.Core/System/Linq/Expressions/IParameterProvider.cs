﻿using System;

namespace System.Linq.Expressions
{
	// Token: 0x02000246 RID: 582
	internal interface IParameterProvider
	{
		// Token: 0x06001103 RID: 4355
		ParameterExpression GetParameter(int index);

		// Token: 0x170002ED RID: 749
		// (get) Token: 0x06001104 RID: 4356
		int ParameterCount { get; }
	}
}
