﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;
using System.Reflection;

namespace System.Linq.Expressions
{
	// Token: 0x02000268 RID: 616
	internal sealed class MethodCallExpressionN : MethodCallExpression, IArgumentProvider
	{
		// Token: 0x060011E8 RID: 4584 RVA: 0x00035912 File Offset: 0x00033B12
		public MethodCallExpressionN(MethodInfo method, IReadOnlyList<Expression> args) : base(method)
		{
			this._arguments = args;
		}

		// Token: 0x060011E9 RID: 4585 RVA: 0x00035922 File Offset: 0x00033B22
		public override Expression GetArgument(int index)
		{
			return this._arguments[index];
		}

		// Token: 0x1700033C RID: 828
		// (get) Token: 0x060011EA RID: 4586 RVA: 0x00035930 File Offset: 0x00033B30
		public override int ArgumentCount
		{
			get
			{
				return this._arguments.Count;
			}
		}

		// Token: 0x060011EB RID: 4587 RVA: 0x0003593D File Offset: 0x00033B3D
		internal override ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			return ExpressionUtils.ReturnReadOnly<Expression>(ref this._arguments);
		}

		// Token: 0x060011EC RID: 4588 RVA: 0x0003594A File Offset: 0x00033B4A
		internal override bool SameArguments(ICollection<Expression> arguments)
		{
			return ExpressionUtils.SameElements<Expression>(arguments, this._arguments);
		}

		// Token: 0x060011ED RID: 4589 RVA: 0x00035958 File Offset: 0x00033B58
		internal override MethodCallExpression Rewrite(Expression instance, IReadOnlyList<Expression> args)
		{
			return Expression.Call(base.Method, args ?? this._arguments);
		}

		// Token: 0x0400096C RID: 2412
		private IReadOnlyList<Expression> _arguments;
	}
}
