﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;

namespace System.Linq.Expressions
{
	// Token: 0x02000258 RID: 600
	internal sealed class Expression3<TDelegate> : Expression<TDelegate>
	{
		// Token: 0x06001183 RID: 4483 RVA: 0x000351F2 File Offset: 0x000333F2
		public Expression3(Expression body, ParameterExpression par0, ParameterExpression par1, ParameterExpression par2) : base(body)
		{
			this._par0 = par0;
			this._par1 = par1;
			this._par2 = par2;
		}

		// Token: 0x17000319 RID: 793
		// (get) Token: 0x06001184 RID: 4484 RVA: 0x0002EF32 File Offset: 0x0002D132
		internal override int ParameterCount
		{
			get
			{
				return 3;
			}
		}

		// Token: 0x06001185 RID: 4485 RVA: 0x00035211 File Offset: 0x00033411
		internal override ParameterExpression GetParameter(int index)
		{
			switch (index)
			{
			case 0:
				return ExpressionUtils.ReturnObject<ParameterExpression>(this._par0);
			case 1:
				return this._par1;
			case 2:
				return this._par2;
			default:
				throw Error.ArgumentOutOfRange("index");
			}
		}

		// Token: 0x06001186 RID: 4486 RVA: 0x0003524C File Offset: 0x0003344C
		internal override bool SameParameters(ICollection<ParameterExpression> parameters)
		{
			if (parameters != null && parameters.Count == 3)
			{
				ReadOnlyCollection<ParameterExpression> readOnlyCollection = this._par0 as ReadOnlyCollection<ParameterExpression>;
				if (readOnlyCollection != null)
				{
					return ExpressionUtils.SameElements<ParameterExpression>(parameters, readOnlyCollection);
				}
				using (IEnumerator<ParameterExpression> enumerator = parameters.GetEnumerator())
				{
					enumerator.MoveNext();
					if (enumerator.Current == this._par0)
					{
						enumerator.MoveNext();
						if (enumerator.Current == this._par1)
						{
							enumerator.MoveNext();
							return enumerator.Current == this._par2;
						}
					}
				}
				return false;
			}
			return false;
		}

		// Token: 0x06001187 RID: 4487 RVA: 0x000352E4 File Offset: 0x000334E4
		internal override ReadOnlyCollection<ParameterExpression> GetOrMakeParameters()
		{
			return ExpressionUtils.ReturnReadOnly(this, ref this._par0);
		}

		// Token: 0x06001188 RID: 4488 RVA: 0x000352F2 File Offset: 0x000334F2
		internal override Expression<TDelegate> Rewrite(Expression body, ParameterExpression[] parameters)
		{
			if (parameters != null)
			{
				return Expression.Lambda<TDelegate>(body, parameters);
			}
			return Expression.Lambda<TDelegate>(body, new ParameterExpression[]
			{
				ExpressionUtils.ReturnObject<ParameterExpression>(this._par0),
				this._par1,
				this._par2
			});
		}

		// Token: 0x04000951 RID: 2385
		private object _par0;

		// Token: 0x04000952 RID: 2386
		private readonly ParameterExpression _par1;

		// Token: 0x04000953 RID: 2387
		private readonly ParameterExpression _par2;
	}
}
