﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Dynamic.Utils;
using System.Linq.Expressions.Interpreter;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Describes a lambda expression. This captures a block of code that is similar to a .NET method body.</summary>
	// Token: 0x02000252 RID: 594
	[DebuggerTypeProxy(typeof(Expression.LambdaExpressionProxy))]
	public abstract class LambdaExpression : Expression, IParameterProvider
	{
		// Token: 0x0600114E RID: 4430 RVA: 0x00034E75 File Offset: 0x00033075
		internal LambdaExpression(Expression body)
		{
			this._body = body;
		}

		/// <summary>Gets the static type of the expression that this <see cref="T:System.Linq.Expressions.Expression" /> represents.</summary>
		/// <returns>The <see cref="P:System.Linq.Expressions.LambdaExpression.Type" /> that represents the static type of the expression.</returns>
		// Token: 0x17000307 RID: 775
		// (get) Token: 0x0600114F RID: 4431 RVA: 0x00034E84 File Offset: 0x00033084
		public sealed override Type Type
		{
			get
			{
				return this.TypeCore;
			}
		}

		// Token: 0x17000308 RID: 776
		// (get) Token: 0x06001150 RID: 4432
		internal abstract Type TypeCore { get; }

		// Token: 0x17000309 RID: 777
		// (get) Token: 0x06001151 RID: 4433
		internal abstract Type PublicType { get; }

		/// <summary>Returns the node type of this <see cref="T:System.Linq.Expressions.Expression" />.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.ExpressionType" /> that represents this expression.</returns>
		// Token: 0x1700030A RID: 778
		// (get) Token: 0x06001152 RID: 4434 RVA: 0x00034E8C File Offset: 0x0003308C
		public sealed override ExpressionType NodeType
		{
			get
			{
				return ExpressionType.Lambda;
			}
		}

		/// <summary>Gets the parameters of the lambda expression.</summary>
		/// <returns>A <see cref="T:System.Collections.ObjectModel.ReadOnlyCollection`1" /> of <see cref="T:System.Linq.Expressions.ParameterExpression" /> objects that represent the parameters of the lambda expression.</returns>
		// Token: 0x1700030B RID: 779
		// (get) Token: 0x06001153 RID: 4435 RVA: 0x00034E90 File Offset: 0x00033090
		public ReadOnlyCollection<ParameterExpression> Parameters
		{
			get
			{
				return this.GetOrMakeParameters();
			}
		}

		/// <summary>Gets the name of the lambda expression.</summary>
		/// <returns>The name of the lambda expression.</returns>
		// Token: 0x1700030C RID: 780
		// (get) Token: 0x06001154 RID: 4436 RVA: 0x00034E98 File Offset: 0x00033098
		public string Name
		{
			get
			{
				return this.NameCore;
			}
		}

		// Token: 0x1700030D RID: 781
		// (get) Token: 0x06001155 RID: 4437 RVA: 0x000053B1 File Offset: 0x000035B1
		internal virtual string NameCore
		{
			get
			{
				return null;
			}
		}

		/// <summary>Gets the body of the lambda expression.</summary>
		/// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> that represents the body of the lambda expression.</returns>
		// Token: 0x1700030E RID: 782
		// (get) Token: 0x06001156 RID: 4438 RVA: 0x00034EA0 File Offset: 0x000330A0
		public Expression Body
		{
			get
			{
				return this._body;
			}
		}

		/// <summary>Gets the return type of the lambda expression.</summary>
		/// <returns>The <see cref="T:System.Type" /> object representing the type of the lambda expression.</returns>
		// Token: 0x1700030F RID: 783
		// (get) Token: 0x06001157 RID: 4439 RVA: 0x00034EA8 File Offset: 0x000330A8
		public Type ReturnType
		{
			get
			{
				return this.Type.GetInvokeMethod().ReturnType;
			}
		}

		/// <summary>Gets the value that indicates if the lambda expression will be compiled with the tail call optimization.</summary>
		/// <returns>True if the lambda expression will be compiled with the tail call optimization, otherwise false.</returns>
		// Token: 0x17000310 RID: 784
		// (get) Token: 0x06001158 RID: 4440 RVA: 0x00034EBA File Offset: 0x000330BA
		public bool TailCall
		{
			get
			{
				return this.TailCallCore;
			}
		}

		// Token: 0x17000311 RID: 785
		// (get) Token: 0x06001159 RID: 4441 RVA: 0x00002285 File Offset: 0x00000485
		internal virtual bool TailCallCore
		{
			get
			{
				return false;
			}
		}

		// Token: 0x0600115A RID: 4442 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		internal virtual ReadOnlyCollection<ParameterExpression> GetOrMakeParameters()
		{
			throw ContractUtils.Unreachable;
		}

		// Token: 0x0600115B RID: 4443 RVA: 0x00034EC2 File Offset: 0x000330C2
		[ExcludeFromCodeCoverage]
		ParameterExpression IParameterProvider.GetParameter(int index)
		{
			return this.GetParameter(index);
		}

		// Token: 0x0600115C RID: 4444 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		internal virtual ParameterExpression GetParameter(int index)
		{
			throw ContractUtils.Unreachable;
		}

		// Token: 0x17000312 RID: 786
		// (get) Token: 0x0600115D RID: 4445 RVA: 0x00034ECB File Offset: 0x000330CB
		[ExcludeFromCodeCoverage]
		int IParameterProvider.ParameterCount
		{
			get
			{
				return this.ParameterCount;
			}
		}

		// Token: 0x17000313 RID: 787
		// (get) Token: 0x0600115E RID: 4446 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		internal virtual int ParameterCount
		{
			get
			{
				throw ContractUtils.Unreachable;
			}
		}

		/// <summary>Produces a delegate that represents the lambda expression.</summary>
		/// <returns>A <see cref="T:System.Delegate" /> that contains the compiled version of the lambda expression.</returns>
		// Token: 0x0600115F RID: 4447 RVA: 0x00034ED3 File Offset: 0x000330D3
		public Delegate Compile()
		{
			return this.Compile(false);
		}

		/// <summary>Produces an interpreted or compiled delegate that represents the lambda expression. </summary>
		/// <param name="preferInterpretation">
		///   <see langword="true" /> to indicate that the expression should be compiled to an interpreted form, if it's available; otherwise, <see langword="false" />.</param>
		/// <returns>A delegate that represents the compiled lambda expression described by the <see cref="T:System.Linq.Expressions.LambdaExpression" /> object.</returns>
		// Token: 0x06001160 RID: 4448 RVA: 0x00034EDC File Offset: 0x000330DC
		public Delegate Compile(bool preferInterpretation)
		{
			return new LightCompiler().CompileTop(this).CreateDelegate();
		}

		/// <summary>Produces a delegate that represents the lambda expression.</summary>
		/// <param name="debugInfoGenerator">Debugging information generator used by the compiler to mark sequence points and annotate local variables.</param>
		/// <returns>A delegate containing the compiled version of the lambda.</returns>
		// Token: 0x06001161 RID: 4449 RVA: 0x00034EEE File Offset: 0x000330EE
		public Delegate Compile(DebugInfoGenerator debugInfoGenerator)
		{
			return this.Compile();
		}

		// Token: 0x06001162 RID: 4450 RVA: 0x0000220F File Offset: 0x0000040F
		internal LambdaExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Compiles the lambda into a method definition.</summary>
		/// <param name="method">A <see cref="T:System.Reflection.Emit.MethodBuilder" /> which will be used to hold the lambda's IL.</param>
		// Token: 0x06001163 RID: 4451 RVA: 0x0000220F File Offset: 0x0000040F
		public void CompileToMethod(MethodBuilder method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Compiles the lambda into a method definition and custom debug information.</summary>
		/// <param name="method">A <see cref="T:System.Reflection.Emit.MethodBuilder" /> which will be used to hold the lambda's IL.</param>
		/// <param name="debugInfoGenerator">Debugging information generator used by the compiler to mark sequence points and annotate local variables.</param>
		// Token: 0x06001164 RID: 4452 RVA: 0x0000220F File Offset: 0x0000040F
		public void CompileToMethod(MethodBuilder method, DebugInfoGenerator debugInfoGenerator)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0400094D RID: 2381
		private readonly Expression _body;
	}
}
