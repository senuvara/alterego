﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Compiler
{
	// Token: 0x0200041B RID: 1051
	internal sealed class HoistedLocals
	{
		// Token: 0x0600195C RID: 6492 RVA: 0x0004B0FC File Offset: 0x000492FC
		internal HoistedLocals(HoistedLocals parent, ReadOnlyCollection<ParameterExpression> vars)
		{
			if (parent != null)
			{
				vars = vars.AddFirst(parent.SelfVariable);
			}
			Dictionary<Expression, int> dictionary = new Dictionary<Expression, int>(vars.Count);
			for (int i = 0; i < vars.Count; i++)
			{
				dictionary.Add(vars[i], i);
			}
			this.SelfVariable = Expression.Variable(typeof(object[]), null);
			this.Parent = parent;
			this.Variables = vars;
			this.Indexes = new ReadOnlyDictionary<Expression, int>(dictionary);
		}

		// Token: 0x170004E5 RID: 1253
		// (get) Token: 0x0600195D RID: 6493 RVA: 0x0004B17B File Offset: 0x0004937B
		internal ParameterExpression ParentVariable
		{
			get
			{
				HoistedLocals parent = this.Parent;
				if (parent == null)
				{
					return null;
				}
				return parent.SelfVariable;
			}
		}

		// Token: 0x0600195E RID: 6494 RVA: 0x0004B18E File Offset: 0x0004938E
		internal static object[] GetParent(object[] locals)
		{
			return ((StrongBox<object[]>)locals[0]).Value;
		}

		// Token: 0x04000C08 RID: 3080
		internal readonly HoistedLocals Parent;

		// Token: 0x04000C09 RID: 3081
		internal readonly ReadOnlyDictionary<Expression, int> Indexes;

		// Token: 0x04000C0A RID: 3082
		internal readonly ReadOnlyCollection<ParameterExpression> Variables;

		// Token: 0x04000C0B RID: 3083
		internal readonly ParameterExpression SelfVariable;
	}
}
