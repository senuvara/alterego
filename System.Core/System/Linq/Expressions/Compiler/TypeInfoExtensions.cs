﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Compiler
{
	// Token: 0x0200041C RID: 1052
	internal static class TypeInfoExtensions
	{
		// Token: 0x0600195F RID: 6495 RVA: 0x0004B19D File Offset: 0x0004939D
		public static Type MakeDelegateType(this DelegateHelpers.TypeInfo info, Type retType, params Expression[] args)
		{
			return info.MakeDelegateType(retType, args);
		}

		// Token: 0x06001960 RID: 6496 RVA: 0x0004B1A8 File Offset: 0x000493A8
		public static Type MakeDelegateType(this DelegateHelpers.TypeInfo info, Type retType, IList<Expression> args)
		{
			Type[] array = new Type[args.Count + 2];
			array[0] = typeof(CallSite);
			array[array.Length - 1] = retType;
			for (int i = 0; i < args.Count; i++)
			{
				array[i + 1] = args[i].Type;
			}
			return info.DelegateType = DelegateHelpers.MakeNewDelegate(array);
		}
	}
}
