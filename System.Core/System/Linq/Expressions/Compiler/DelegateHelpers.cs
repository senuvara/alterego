﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.Dynamic.Utils;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Compiler
{
	// Token: 0x02000411 RID: 1041
	internal static class DelegateHelpers
	{
		// Token: 0x0600192E RID: 6446 RVA: 0x0004A89C File Offset: 0x00048A9C
		internal static Type MakeDelegateType(Type[] types)
		{
			DelegateHelpers.TypeInfo delegateCache = DelegateHelpers._DelegateCache;
			Type delegateType;
			lock (delegateCache)
			{
				DelegateHelpers.TypeInfo typeInfo = DelegateHelpers._DelegateCache;
				for (int i = 0; i < types.Length; i++)
				{
					typeInfo = DelegateHelpers.NextTypeInfo(types[i], typeInfo);
				}
				if (typeInfo.DelegateType == null)
				{
					typeInfo.DelegateType = DelegateHelpers.MakeNewDelegate((Type[])types.Clone());
				}
				delegateType = typeInfo.DelegateType;
			}
			return delegateType;
		}

		// Token: 0x0600192F RID: 6447 RVA: 0x0004A924 File Offset: 0x00048B24
		internal static DelegateHelpers.TypeInfo NextTypeInfo(Type initialArg)
		{
			DelegateHelpers.TypeInfo delegateCache = DelegateHelpers._DelegateCache;
			DelegateHelpers.TypeInfo result;
			lock (delegateCache)
			{
				result = DelegateHelpers.NextTypeInfo(initialArg, DelegateHelpers._DelegateCache);
			}
			return result;
		}

		// Token: 0x06001930 RID: 6448 RVA: 0x0004A96C File Offset: 0x00048B6C
		internal static DelegateHelpers.TypeInfo GetNextTypeInfo(Type initialArg, DelegateHelpers.TypeInfo curTypeInfo)
		{
			DelegateHelpers.TypeInfo delegateCache = DelegateHelpers._DelegateCache;
			DelegateHelpers.TypeInfo result;
			lock (delegateCache)
			{
				result = DelegateHelpers.NextTypeInfo(initialArg, curTypeInfo);
			}
			return result;
		}

		// Token: 0x06001931 RID: 6449 RVA: 0x0004A9B0 File Offset: 0x00048BB0
		private static DelegateHelpers.TypeInfo NextTypeInfo(Type initialArg, DelegateHelpers.TypeInfo curTypeInfo)
		{
			if (curTypeInfo.TypeChain == null)
			{
				curTypeInfo.TypeChain = new Dictionary<Type, DelegateHelpers.TypeInfo>();
			}
			DelegateHelpers.TypeInfo typeInfo;
			if (!curTypeInfo.TypeChain.TryGetValue(initialArg, out typeInfo))
			{
				typeInfo = new DelegateHelpers.TypeInfo();
				if (initialArg.CanCache())
				{
					curTypeInfo.TypeChain[initialArg] = typeInfo;
				}
			}
			return typeInfo;
		}

		// Token: 0x06001932 RID: 6450 RVA: 0x0004AA00 File Offset: 0x00048C00
		private static Type TryMakeVBStyledCallSite(Type[] types)
		{
			if (types.Length < 3 || types[0].IsByRef || types[1] != typeof(object) || types[types.Length - 1] != typeof(object))
			{
				return null;
			}
			for (int i = 2; i < types.Length - 2; i++)
			{
				Type type = types[i];
				if (!type.IsByRef || type.GetElementType() != typeof(object))
				{
					return null;
				}
			}
			switch (types.Length - 1)
			{
			case 2:
				return typeof(DelegateHelpers.VBCallSiteDelegate0<>).MakeGenericType(new Type[]
				{
					types[0]
				});
			case 3:
				return typeof(DelegateHelpers.VBCallSiteDelegate1<>).MakeGenericType(new Type[]
				{
					types[0]
				});
			case 4:
				return typeof(DelegateHelpers.VBCallSiteDelegate2<>).MakeGenericType(new Type[]
				{
					types[0]
				});
			case 5:
				return typeof(DelegateHelpers.VBCallSiteDelegate3<>).MakeGenericType(new Type[]
				{
					types[0]
				});
			case 6:
				return typeof(DelegateHelpers.VBCallSiteDelegate4<>).MakeGenericType(new Type[]
				{
					types[0]
				});
			case 7:
				return typeof(DelegateHelpers.VBCallSiteDelegate5<>).MakeGenericType(new Type[]
				{
					types[0]
				});
			case 8:
				return typeof(DelegateHelpers.VBCallSiteDelegate6<>).MakeGenericType(new Type[]
				{
					types[0]
				});
			case 9:
				return typeof(DelegateHelpers.VBCallSiteDelegate7<>).MakeGenericType(new Type[]
				{
					types[0]
				});
			default:
				return null;
			}
		}

		// Token: 0x06001933 RID: 6451 RVA: 0x0004AB94 File Offset: 0x00048D94
		internal static Type MakeNewDelegate(Type[] types)
		{
			bool flag;
			if (types.Length > 17)
			{
				flag = true;
			}
			else
			{
				flag = false;
				foreach (Type type in types)
				{
					if (type.IsByRef || type.IsPointer)
					{
						flag = true;
						break;
					}
				}
			}
			if (flag)
			{
				return DelegateHelpers.TryMakeVBStyledCallSite(types) ?? DelegateHelpers.MakeNewCustomDelegate(types);
			}
			Type result;
			if (types[types.Length - 1] == typeof(void))
			{
				result = DelegateHelpers.GetActionType(types.RemoveLast<Type>());
			}
			else
			{
				result = DelegateHelpers.GetFuncType(types);
			}
			return result;
		}

		// Token: 0x06001934 RID: 6452 RVA: 0x0004AC18 File Offset: 0x00048E18
		internal static Type GetFuncType(Type[] types)
		{
			switch (types.Length)
			{
			case 1:
				return typeof(Func<>).MakeGenericType(types);
			case 2:
				return typeof(Func<, >).MakeGenericType(types);
			case 3:
				return typeof(Func<, , >).MakeGenericType(types);
			case 4:
				return typeof(Func<, , , >).MakeGenericType(types);
			case 5:
				return typeof(Func<, , , , >).MakeGenericType(types);
			case 6:
				return typeof(Func<, , , , , >).MakeGenericType(types);
			case 7:
				return typeof(Func<, , , , , , >).MakeGenericType(types);
			case 8:
				return typeof(Func<, , , , , , , >).MakeGenericType(types);
			case 9:
				return typeof(Func<, , , , , , , , >).MakeGenericType(types);
			case 10:
				return typeof(Func<, , , , , , , , , >).MakeGenericType(types);
			case 11:
				return typeof(Func<, , , , , , , , , , >).MakeGenericType(types);
			case 12:
				return typeof(Func<, , , , , , , , , , , >).MakeGenericType(types);
			case 13:
				return typeof(Func<, , , , , , , , , , , , >).MakeGenericType(types);
			case 14:
				return typeof(Func<, , , , , , , , , , , , , >).MakeGenericType(types);
			case 15:
				return typeof(Func<, , , , , , , , , , , , , , >).MakeGenericType(types);
			case 16:
				return typeof(Func<, , , , , , , , , , , , , , , >).MakeGenericType(types);
			case 17:
				return typeof(Func<, , , , , , , , , , , , , , , , >).MakeGenericType(types);
			default:
				return null;
			}
		}

		// Token: 0x06001935 RID: 6453 RVA: 0x0004AD9C File Offset: 0x00048F9C
		internal static Type GetActionType(Type[] types)
		{
			switch (types.Length)
			{
			case 0:
				return typeof(Action);
			case 1:
				return typeof(Action<>).MakeGenericType(types);
			case 2:
				return typeof(Action<, >).MakeGenericType(types);
			case 3:
				return typeof(Action<, , >).MakeGenericType(types);
			case 4:
				return typeof(Action<, , , >).MakeGenericType(types);
			case 5:
				return typeof(Action<, , , , >).MakeGenericType(types);
			case 6:
				return typeof(Action<, , , , , >).MakeGenericType(types);
			case 7:
				return typeof(Action<, , , , , , >).MakeGenericType(types);
			case 8:
				return typeof(Action<, , , , , , , >).MakeGenericType(types);
			case 9:
				return typeof(Action<, , , , , , , , >).MakeGenericType(types);
			case 10:
				return typeof(Action<, , , , , , , , , >).MakeGenericType(types);
			case 11:
				return typeof(Action<, , , , , , , , , , >).MakeGenericType(types);
			case 12:
				return typeof(Action<, , , , , , , , , , , >).MakeGenericType(types);
			case 13:
				return typeof(Action<, , , , , , , , , , , , >).MakeGenericType(types);
			case 14:
				return typeof(Action<, , , , , , , , , , , , , >).MakeGenericType(types);
			case 15:
				return typeof(Action<, , , , , , , , , , , , , , >).MakeGenericType(types);
			case 16:
				return typeof(Action<, , , , , , , , , , , , , , , >).MakeGenericType(types);
			default:
				return null;
			}
		}

		// Token: 0x06001936 RID: 6454 RVA: 0x0004AF18 File Offset: 0x00049118
		internal static Type MakeCallSiteDelegate(ReadOnlyCollection<Expression> types, Type returnType)
		{
			DelegateHelpers.TypeInfo delegateCache = DelegateHelpers._DelegateCache;
			Type delegateType;
			lock (delegateCache)
			{
				DelegateHelpers.TypeInfo typeInfo = DelegateHelpers._DelegateCache;
				typeInfo = DelegateHelpers.NextTypeInfo(typeof(CallSite), typeInfo);
				for (int i = 0; i < types.Count; i++)
				{
					typeInfo = DelegateHelpers.NextTypeInfo(types[i].Type, typeInfo);
				}
				typeInfo = DelegateHelpers.NextTypeInfo(returnType, typeInfo);
				if (typeInfo.DelegateType == null)
				{
					typeInfo.MakeDelegateType(returnType, types);
				}
				delegateType = typeInfo.DelegateType;
			}
			return delegateType;
		}

		// Token: 0x06001937 RID: 6455 RVA: 0x0004AFB8 File Offset: 0x000491B8
		internal static Type MakeDeferredSiteDelegate(DynamicMetaObject[] args, Type returnType)
		{
			DelegateHelpers.TypeInfo delegateCache = DelegateHelpers._DelegateCache;
			Type delegateType;
			lock (delegateCache)
			{
				DelegateHelpers.TypeInfo typeInfo = DelegateHelpers._DelegateCache;
				typeInfo = DelegateHelpers.NextTypeInfo(typeof(CallSite), typeInfo);
				foreach (DynamicMetaObject dynamicMetaObject in args)
				{
					Type type = dynamicMetaObject.Expression.Type;
					if (DelegateHelpers.IsByRef(dynamicMetaObject))
					{
						type = type.MakeByRefType();
					}
					typeInfo = DelegateHelpers.NextTypeInfo(type, typeInfo);
				}
				typeInfo = DelegateHelpers.NextTypeInfo(returnType, typeInfo);
				if (typeInfo.DelegateType == null)
				{
					Type[] array = new Type[args.Length + 2];
					array[0] = typeof(CallSite);
					array[array.Length - 1] = returnType;
					for (int j = 0; j < args.Length; j++)
					{
						DynamicMetaObject dynamicMetaObject2 = args[j];
						Type type2 = dynamicMetaObject2.Expression.Type;
						if (DelegateHelpers.IsByRef(dynamicMetaObject2))
						{
							type2 = type2.MakeByRefType();
						}
						array[j + 1] = type2;
					}
					typeInfo.DelegateType = DelegateHelpers.MakeNewDelegate(array);
				}
				delegateType = typeInfo.DelegateType;
			}
			return delegateType;
		}

		// Token: 0x06001938 RID: 6456 RVA: 0x0004B0CC File Offset: 0x000492CC
		private static bool IsByRef(DynamicMetaObject mo)
		{
			ParameterExpression parameterExpression = mo.Expression as ParameterExpression;
			return parameterExpression != null && parameterExpression.IsByRef;
		}

		// Token: 0x06001939 RID: 6457 RVA: 0x00003A76 File Offset: 0x00001C76
		private static Type MakeNewCustomDelegate(Type[] types)
		{
			throw new PlatformNotSupportedException();
		}

		// Token: 0x0600193A RID: 6458 RVA: 0x0004B0F0 File Offset: 0x000492F0
		// Note: this type is marked as 'beforefieldinit'.
		static DelegateHelpers()
		{
		}

		// Token: 0x04000C04 RID: 3076
		private static DelegateHelpers.TypeInfo _DelegateCache = new DelegateHelpers.TypeInfo();

		// Token: 0x04000C05 RID: 3077
		private const int MaximumArity = 17;

		// Token: 0x02000412 RID: 1042
		internal class TypeInfo
		{
			// Token: 0x0600193B RID: 6459 RVA: 0x00002310 File Offset: 0x00000510
			public TypeInfo()
			{
			}

			// Token: 0x04000C06 RID: 3078
			public Type DelegateType;

			// Token: 0x04000C07 RID: 3079
			public Dictionary<Type, DelegateHelpers.TypeInfo> TypeChain;
		}

		// Token: 0x02000413 RID: 1043
		// (Invoke) Token: 0x0600193D RID: 6461
		public delegate object VBCallSiteDelegate0<T>(T callSite, object instance);

		// Token: 0x02000414 RID: 1044
		// (Invoke) Token: 0x06001941 RID: 6465
		public delegate object VBCallSiteDelegate1<T>(T callSite, object instance, ref object arg1);

		// Token: 0x02000415 RID: 1045
		// (Invoke) Token: 0x06001945 RID: 6469
		public delegate object VBCallSiteDelegate2<T>(T callSite, object instance, ref object arg1, ref object arg2);

		// Token: 0x02000416 RID: 1046
		// (Invoke) Token: 0x06001949 RID: 6473
		public delegate object VBCallSiteDelegate3<T>(T callSite, object instance, ref object arg1, ref object arg2, ref object arg3);

		// Token: 0x02000417 RID: 1047
		// (Invoke) Token: 0x0600194D RID: 6477
		public delegate object VBCallSiteDelegate4<T>(T callSite, object instance, ref object arg1, ref object arg2, ref object arg3, ref object arg4);

		// Token: 0x02000418 RID: 1048
		// (Invoke) Token: 0x06001951 RID: 6481
		public delegate object VBCallSiteDelegate5<T>(T callSite, object instance, ref object arg1, ref object arg2, ref object arg3, ref object arg4, ref object arg5);

		// Token: 0x02000419 RID: 1049
		// (Invoke) Token: 0x06001955 RID: 6485
		public delegate object VBCallSiteDelegate6<T>(T callSite, object instance, ref object arg1, ref object arg2, ref object arg3, ref object arg4, ref object arg5, ref object arg6);

		// Token: 0x0200041A RID: 1050
		// (Invoke) Token: 0x06001959 RID: 6489
		public delegate object VBCallSiteDelegate7<T>(T callSite, object instance, ref object arg1, ref object arg2, ref object arg3, ref object arg4, ref object arg5, ref object arg6, ref object arg7);
	}
}
