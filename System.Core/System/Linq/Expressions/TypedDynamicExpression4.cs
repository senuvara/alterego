﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x0200023A RID: 570
	internal sealed class TypedDynamicExpression4 : DynamicExpression4
	{
		// Token: 0x06000FBF RID: 4031 RVA: 0x00031AED File Offset: 0x0002FCED
		internal TypedDynamicExpression4(Type retType, Type delegateType, CallSiteBinder binder, Expression arg0, Expression arg1, Expression arg2, Expression arg3) : base(delegateType, binder, arg0, arg1, arg2, arg3)
		{
			this.Type = retType;
		}

		// Token: 0x170002E2 RID: 738
		// (get) Token: 0x06000FC0 RID: 4032 RVA: 0x00031B06 File Offset: 0x0002FD06
		public sealed override Type Type
		{
			[CompilerGenerated]
			get
			{
				return this.<Type>k__BackingField;
			}
		}

		// Token: 0x040008D0 RID: 2256
		[CompilerGenerated]
		private readonly Type <Type>k__BackingField;
	}
}
