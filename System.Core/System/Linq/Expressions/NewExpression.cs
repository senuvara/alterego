﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Dynamic.Utils;
using System.Reflection;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Represents a constructor call.</summary>
	// Token: 0x02000277 RID: 631
	[DebuggerTypeProxy(typeof(Expression.NewExpressionProxy))]
	public class NewExpression : Expression, IArgumentProvider
	{
		// Token: 0x0600123B RID: 4667 RVA: 0x00036488 File Offset: 0x00034688
		internal NewExpression(ConstructorInfo constructor, IReadOnlyList<Expression> arguments, ReadOnlyCollection<MemberInfo> members)
		{
			this.Constructor = constructor;
			this._arguments = arguments;
			this.Members = members;
		}

		/// <summary>Gets the static type of the expression that this <see cref="T:System.Linq.Expressions.Expression" /> represents.</summary>
		/// <returns>The <see cref="P:System.Linq.Expressions.NewExpression.Type" /> that represents the static type of the expression.</returns>
		// Token: 0x1700034C RID: 844
		// (get) Token: 0x0600123C RID: 4668 RVA: 0x000364A5 File Offset: 0x000346A5
		public override Type Type
		{
			get
			{
				return this.Constructor.DeclaringType;
			}
		}

		/// <summary>Returns the node type of this <see cref="T:System.Linq.Expressions.Expression" />.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.ExpressionType" /> that represents this expression.</returns>
		// Token: 0x1700034D RID: 845
		// (get) Token: 0x0600123D RID: 4669 RVA: 0x000364B2 File Offset: 0x000346B2
		public sealed override ExpressionType NodeType
		{
			get
			{
				return ExpressionType.New;
			}
		}

		/// <summary>Gets the called constructor.</summary>
		/// <returns>The <see cref="T:System.Reflection.ConstructorInfo" /> that represents the called constructor.</returns>
		// Token: 0x1700034E RID: 846
		// (get) Token: 0x0600123E RID: 4670 RVA: 0x000364B6 File Offset: 0x000346B6
		public ConstructorInfo Constructor
		{
			[CompilerGenerated]
			get
			{
				return this.<Constructor>k__BackingField;
			}
		}

		/// <summary>Gets the arguments to the constructor.</summary>
		/// <returns>A collection of <see cref="T:System.Linq.Expressions.Expression" /> objects that represent the arguments to the constructor.</returns>
		// Token: 0x1700034F RID: 847
		// (get) Token: 0x0600123F RID: 4671 RVA: 0x000364BE File Offset: 0x000346BE
		public ReadOnlyCollection<Expression> Arguments
		{
			get
			{
				return ExpressionUtils.ReturnReadOnly<Expression>(ref this._arguments);
			}
		}

		// Token: 0x06001240 RID: 4672 RVA: 0x000364CB File Offset: 0x000346CB
		public Expression GetArgument(int index)
		{
			return this._arguments[index];
		}

		// Token: 0x17000350 RID: 848
		// (get) Token: 0x06001241 RID: 4673 RVA: 0x000364D9 File Offset: 0x000346D9
		public int ArgumentCount
		{
			get
			{
				return this._arguments.Count;
			}
		}

		/// <summary>Gets the members that can retrieve the values of the fields that were initialized with constructor arguments.</summary>
		/// <returns>A collection of <see cref="T:System.Reflection.MemberInfo" /> objects that represent the members that can retrieve the values of the fields that were initialized with constructor arguments.</returns>
		// Token: 0x17000351 RID: 849
		// (get) Token: 0x06001242 RID: 4674 RVA: 0x000364E6 File Offset: 0x000346E6
		public ReadOnlyCollection<MemberInfo> Members
		{
			[CompilerGenerated]
			get
			{
				return this.<Members>k__BackingField;
			}
		}

		/// <summary>Dispatches to the specific visit method for this node type. For example, <see cref="T:System.Linq.Expressions.MethodCallExpression" /> calls the <see cref="M:System.Linq.Expressions.ExpressionVisitor.VisitMethodCall(System.Linq.Expressions.MethodCallExpression)" />.</summary>
		/// <param name="visitor">The visitor to visit this node with.</param>
		/// <returns>The result of visiting this node.</returns>
		// Token: 0x06001243 RID: 4675 RVA: 0x000364EE File Offset: 0x000346EE
		protected internal override Expression Accept(ExpressionVisitor visitor)
		{
			return visitor.VisitNew(this);
		}

		/// <summary>Creates a new expression that is like this one, but using the supplied children. If all of the children are the same, it will return this expression.</summary>
		/// <param name="arguments">The <see cref="P:System.Linq.Expressions.NewExpression.Arguments" /> property of the result.</param>
		/// <returns>This expression if no children are changed or an expression with the updated children.</returns>
		// Token: 0x06001244 RID: 4676 RVA: 0x000364F7 File Offset: 0x000346F7
		public NewExpression Update(IEnumerable<Expression> arguments)
		{
			if (ExpressionUtils.SameElements<Expression>(ref arguments, this.Arguments))
			{
				return this;
			}
			if (this.Members == null)
			{
				return Expression.New(this.Constructor, arguments);
			}
			return Expression.New(this.Constructor, arguments, this.Members);
		}

		// Token: 0x06001245 RID: 4677 RVA: 0x0000220F File Offset: 0x0000040F
		internal NewExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000985 RID: 2437
		private IReadOnlyList<Expression> _arguments;

		// Token: 0x04000986 RID: 2438
		[CompilerGenerated]
		private readonly ConstructorInfo <Constructor>k__BackingField;

		// Token: 0x04000987 RID: 2439
		[CompilerGenerated]
		private readonly ReadOnlyCollection<MemberInfo> <Members>k__BackingField;
	}
}
