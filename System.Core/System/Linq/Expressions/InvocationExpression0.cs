﻿using System;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;

namespace System.Linq.Expressions
{
	// Token: 0x0200024A RID: 586
	internal sealed class InvocationExpression0 : InvocationExpression
	{
		// Token: 0x06001123 RID: 4387 RVA: 0x00034A94 File Offset: 0x00032C94
		public InvocationExpression0(Expression lambda, Type returnType) : base(lambda, returnType)
		{
		}

		// Token: 0x06001124 RID: 4388 RVA: 0x00034A9E File Offset: 0x00032C9E
		internal override ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			return EmptyReadOnlyCollection<Expression>.Instance;
		}

		// Token: 0x06001125 RID: 4389 RVA: 0x00034AA5 File Offset: 0x00032CA5
		public override Expression GetArgument(int index)
		{
			throw new ArgumentOutOfRangeException("index");
		}

		// Token: 0x170002FB RID: 763
		// (get) Token: 0x06001126 RID: 4390 RVA: 0x00002285 File Offset: 0x00000485
		public override int ArgumentCount
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x06001127 RID: 4391 RVA: 0x00034AB1 File Offset: 0x00032CB1
		internal override InvocationExpression Rewrite(Expression lambda, Expression[] arguments)
		{
			return Expression.Invoke(lambda);
		}
	}
}
