﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x020001F5 RID: 501
	internal class SimpleBinaryExpression : BinaryExpression
	{
		// Token: 0x06000C33 RID: 3123 RVA: 0x000271DA File Offset: 0x000253DA
		internal SimpleBinaryExpression(ExpressionType nodeType, Expression left, Expression right, Type type) : base(left, right)
		{
			this.NodeType = nodeType;
			this.Type = type;
		}

		// Token: 0x170001E9 RID: 489
		// (get) Token: 0x06000C34 RID: 3124 RVA: 0x000271F3 File Offset: 0x000253F3
		public sealed override ExpressionType NodeType
		{
			[CompilerGenerated]
			get
			{
				return this.<NodeType>k__BackingField;
			}
		}

		// Token: 0x170001EA RID: 490
		// (get) Token: 0x06000C35 RID: 3125 RVA: 0x000271FB File Offset: 0x000253FB
		public sealed override Type Type
		{
			[CompilerGenerated]
			get
			{
				return this.<Type>k__BackingField;
			}
		}

		// Token: 0x04000858 RID: 2136
		[CompilerGenerated]
		private readonly ExpressionType <NodeType>k__BackingField;

		// Token: 0x04000859 RID: 2137
		[CompilerGenerated]
		private readonly Type <Type>k__BackingField;
	}
}
