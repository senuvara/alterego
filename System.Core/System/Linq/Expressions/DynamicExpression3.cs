﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x02000237 RID: 567
	internal class DynamicExpression3 : DynamicExpression, IArgumentProvider
	{
		// Token: 0x06000FB1 RID: 4017 RVA: 0x00031854 File Offset: 0x0002FA54
		internal DynamicExpression3(Type delegateType, CallSiteBinder binder, Expression arg0, Expression arg1, Expression arg2) : base(delegateType, binder)
		{
			this._arg0 = arg0;
			this._arg1 = arg1;
			this._arg2 = arg2;
		}

		// Token: 0x06000FB2 RID: 4018 RVA: 0x00031875 File Offset: 0x0002FA75
		Expression IArgumentProvider.GetArgument(int index)
		{
			switch (index)
			{
			case 0:
				return ExpressionUtils.ReturnObject<Expression>(this._arg0);
			case 1:
				return this._arg1;
			case 2:
				return this._arg2;
			default:
				throw new ArgumentOutOfRangeException("index");
			}
		}

		// Token: 0x170002DF RID: 735
		// (get) Token: 0x06000FB3 RID: 4019 RVA: 0x0002EF32 File Offset: 0x0002D132
		int IArgumentProvider.ArgumentCount
		{
			get
			{
				return 3;
			}
		}

		// Token: 0x06000FB4 RID: 4020 RVA: 0x000318B0 File Offset: 0x0002FAB0
		internal override bool SameArguments(ICollection<Expression> arguments)
		{
			if (arguments != null && arguments.Count == 3)
			{
				ReadOnlyCollection<Expression> readOnlyCollection = this._arg0 as ReadOnlyCollection<Expression>;
				if (readOnlyCollection != null)
				{
					return ExpressionUtils.SameElements<Expression>(arguments, readOnlyCollection);
				}
				using (IEnumerator<Expression> enumerator = arguments.GetEnumerator())
				{
					enumerator.MoveNext();
					if (enumerator.Current == this._arg0)
					{
						enumerator.MoveNext();
						if (enumerator.Current == this._arg1)
						{
							enumerator.MoveNext();
							return enumerator.Current == this._arg2;
						}
					}
				}
				return false;
			}
			return false;
		}

		// Token: 0x06000FB5 RID: 4021 RVA: 0x00031948 File Offset: 0x0002FB48
		internal override ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			return ExpressionUtils.ReturnReadOnly(this, ref this._arg0);
		}

		// Token: 0x06000FB6 RID: 4022 RVA: 0x00031956 File Offset: 0x0002FB56
		internal override DynamicExpression Rewrite(Expression[] args)
		{
			return ExpressionExtension.MakeDynamic(base.DelegateType, base.Binder, args[0], args[1], args[2]);
		}

		// Token: 0x040008C8 RID: 2248
		private object _arg0;

		// Token: 0x040008C9 RID: 2249
		private readonly Expression _arg1;

		// Token: 0x040008CA RID: 2250
		private readonly Expression _arg2;
	}
}
