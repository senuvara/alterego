﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;

namespace System.Linq.Expressions
{
	// Token: 0x0200021C RID: 540
	internal sealed class Scope1 : ScopeExpression
	{
		// Token: 0x06000EC5 RID: 3781 RVA: 0x0002F2B3 File Offset: 0x0002D4B3
		internal Scope1(IReadOnlyList<ParameterExpression> variables, Expression body) : this(variables, body)
		{
		}

		// Token: 0x06000EC6 RID: 3782 RVA: 0x0002F2BD File Offset: 0x0002D4BD
		private Scope1(IReadOnlyList<ParameterExpression> variables, object body) : base(variables)
		{
			this._body = body;
		}

		// Token: 0x06000EC7 RID: 3783 RVA: 0x0002F2D0 File Offset: 0x0002D4D0
		internal override bool SameExpressions(ICollection<Expression> expressions)
		{
			if (expressions.Count == 1)
			{
				ReadOnlyCollection<Expression> readOnlyCollection = this._body as ReadOnlyCollection<Expression>;
				if (readOnlyCollection != null)
				{
					return ExpressionUtils.SameElements<Expression>(expressions, readOnlyCollection);
				}
				using (IEnumerator<Expression> enumerator = expressions.GetEnumerator())
				{
					enumerator.MoveNext();
					return ExpressionUtils.ReturnObject<Expression>(this._body) == enumerator.Current;
				}
				return false;
			}
			return false;
		}

		// Token: 0x06000EC8 RID: 3784 RVA: 0x0002F340 File Offset: 0x0002D540
		internal override Expression GetExpression(int index)
		{
			if (index == 0)
			{
				return ExpressionUtils.ReturnObject<Expression>(this._body);
			}
			throw Error.ArgumentOutOfRange("index");
		}

		// Token: 0x170002A4 RID: 676
		// (get) Token: 0x06000EC9 RID: 3785 RVA: 0x00009CDF File Offset: 0x00007EDF
		internal override int ExpressionCount
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x06000ECA RID: 3786 RVA: 0x0002F35B File Offset: 0x0002D55B
		internal override ReadOnlyCollection<Expression> GetOrMakeExpressions()
		{
			return BlockExpression.ReturnReadOnlyExpressions(this, ref this._body);
		}

		// Token: 0x06000ECB RID: 3787 RVA: 0x0002F369 File Offset: 0x0002D569
		internal override BlockExpression Rewrite(ReadOnlyCollection<ParameterExpression> variables, Expression[] args)
		{
			if (args == null)
			{
				Expression.ValidateVariables(variables, "variables");
				return new Scope1(variables, this._body);
			}
			return new Scope1(base.ReuseOrValidateVariables(variables), args[0]);
		}

		// Token: 0x04000891 RID: 2193
		private object _body;
	}
}
