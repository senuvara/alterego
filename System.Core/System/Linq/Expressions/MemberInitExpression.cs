﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Dynamic.Utils;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Represents calling a constructor and initializing one or more members of the new object.</summary>
	// Token: 0x02000263 RID: 611
	[DebuggerTypeProxy(typeof(Expression.MemberInitExpressionProxy))]
	public sealed class MemberInitExpression : Expression
	{
		// Token: 0x060011C0 RID: 4544 RVA: 0x0003560D File Offset: 0x0003380D
		internal MemberInitExpression(NewExpression newExpression, ReadOnlyCollection<MemberBinding> bindings)
		{
			this.NewExpression = newExpression;
			this.Bindings = bindings;
		}

		/// <summary>Gets the static type of the expression that this <see cref="T:System.Linq.Expressions.Expression" /> represents.</summary>
		/// <returns>The <see cref="P:System.Linq.Expressions.MemberInitExpression.Type" /> that represents the static type of the expression.</returns>
		// Token: 0x1700032F RID: 815
		// (get) Token: 0x060011C1 RID: 4545 RVA: 0x00035623 File Offset: 0x00033823
		public sealed override Type Type
		{
			get
			{
				return this.NewExpression.Type;
			}
		}

		/// <summary>Gets a value that indicates whether the expression tree node can be reduced.</summary>
		/// <returns>True if the node can be reduced, otherwise false.</returns>
		// Token: 0x17000330 RID: 816
		// (get) Token: 0x060011C2 RID: 4546 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override bool CanReduce
		{
			get
			{
				return true;
			}
		}

		/// <summary>Returns the node type of this Expression. Extension nodes should return <see cref="F:System.Linq.Expressions.ExpressionType.Extension" /> when overriding this method.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.ExpressionType" /> of the expression.</returns>
		// Token: 0x17000331 RID: 817
		// (get) Token: 0x060011C3 RID: 4547 RVA: 0x00035630 File Offset: 0x00033830
		public sealed override ExpressionType NodeType
		{
			get
			{
				return ExpressionType.MemberInit;
			}
		}

		/// <summary>Gets the expression that represents the constructor call.</summary>
		/// <returns>A <see cref="T:System.Linq.Expressions.NewExpression" /> that represents the constructor call.</returns>
		// Token: 0x17000332 RID: 818
		// (get) Token: 0x060011C4 RID: 4548 RVA: 0x00035634 File Offset: 0x00033834
		public NewExpression NewExpression
		{
			[CompilerGenerated]
			get
			{
				return this.<NewExpression>k__BackingField;
			}
		}

		/// <summary>Gets the bindings that describe how to initialize the members of the newly created object.</summary>
		/// <returns>A <see cref="T:System.Collections.ObjectModel.ReadOnlyCollection`1" /> of <see cref="T:System.Linq.Expressions.MemberBinding" /> objects which describe how to initialize the members.</returns>
		// Token: 0x17000333 RID: 819
		// (get) Token: 0x060011C5 RID: 4549 RVA: 0x0003563C File Offset: 0x0003383C
		public ReadOnlyCollection<MemberBinding> Bindings
		{
			[CompilerGenerated]
			get
			{
				return this.<Bindings>k__BackingField;
			}
		}

		// Token: 0x060011C6 RID: 4550 RVA: 0x00035644 File Offset: 0x00033844
		protected internal override Expression Accept(ExpressionVisitor visitor)
		{
			return visitor.VisitMemberInit(this);
		}

		/// <summary>Reduces the <see cref="T:System.Linq.Expressions.MemberInitExpression" /> to a simpler expression. </summary>
		/// <returns>The reduced expression.</returns>
		// Token: 0x060011C7 RID: 4551 RVA: 0x0003564D File Offset: 0x0003384D
		public override Expression Reduce()
		{
			return MemberInitExpression.ReduceMemberInit(this.NewExpression, this.Bindings, true);
		}

		// Token: 0x060011C8 RID: 4552 RVA: 0x00035664 File Offset: 0x00033864
		private static Expression ReduceMemberInit(Expression objExpression, ReadOnlyCollection<MemberBinding> bindings, bool keepOnStack)
		{
			ParameterExpression parameterExpression = Expression.Variable(objExpression.Type);
			int count = bindings.Count;
			Expression[] array = new Expression[count + 2];
			array[0] = Expression.Assign(parameterExpression, objExpression);
			for (int i = 0; i < count; i++)
			{
				array[i + 1] = MemberInitExpression.ReduceMemberBinding(parameterExpression, bindings[i]);
			}
			array[count + 1] = (keepOnStack ? parameterExpression : Utils.Empty);
			return Expression.Block(new ParameterExpression[]
			{
				parameterExpression
			}, array);
		}

		// Token: 0x060011C9 RID: 4553 RVA: 0x000356D8 File Offset: 0x000338D8
		internal static Expression ReduceListInit(Expression listExpression, ReadOnlyCollection<ElementInit> initializers, bool keepOnStack)
		{
			ParameterExpression parameterExpression = Expression.Variable(listExpression.Type);
			int count = initializers.Count;
			Expression[] array = new Expression[count + 2];
			array[0] = Expression.Assign(parameterExpression, listExpression);
			for (int i = 0; i < count; i++)
			{
				ElementInit elementInit = initializers[i];
				array[i + 1] = Expression.Call(parameterExpression, elementInit.AddMethod, elementInit.Arguments);
			}
			array[count + 1] = (keepOnStack ? parameterExpression : Utils.Empty);
			return Expression.Block(new ParameterExpression[]
			{
				parameterExpression
			}, array);
		}

		// Token: 0x060011CA RID: 4554 RVA: 0x0003575C File Offset: 0x0003395C
		internal static Expression ReduceMemberBinding(ParameterExpression objVar, MemberBinding binding)
		{
			MemberExpression memberExpression = Expression.MakeMemberAccess(objVar, binding.Member);
			switch (binding.BindingType)
			{
			case MemberBindingType.Assignment:
				return Expression.Assign(memberExpression, ((MemberAssignment)binding).Expression);
			case MemberBindingType.MemberBinding:
				return MemberInitExpression.ReduceMemberInit(memberExpression, ((MemberMemberBinding)binding).Bindings, false);
			case MemberBindingType.ListBinding:
				return MemberInitExpression.ReduceListInit(memberExpression, ((MemberListBinding)binding).Initializers, false);
			default:
				throw ContractUtils.Unreachable;
			}
		}

		/// <summary>Creates a new expression that is like this one, but using the supplied children. If all of the children are the same, it will return this expression.</summary>
		/// <param name="newExpression">The <see cref="P:System.Linq.Expressions.MemberInitExpression.NewExpression" /> property of the result.</param>
		/// <param name="bindings">The <see cref="P:System.Linq.Expressions.MemberInitExpression.Bindings" /> property of the result.</param>
		/// <returns>This expression if no children are changed or an expression with the updated children.</returns>
		// Token: 0x060011CB RID: 4555 RVA: 0x000357CE File Offset: 0x000339CE
		public MemberInitExpression Update(NewExpression newExpression, IEnumerable<MemberBinding> bindings)
		{
			if ((newExpression == this.NewExpression & bindings != null) && ExpressionUtils.SameElements<MemberBinding>(ref bindings, this.Bindings))
			{
				return this;
			}
			return Expression.MemberInit(newExpression, bindings);
		}

		// Token: 0x060011CC RID: 4556 RVA: 0x0000220F File Offset: 0x0000040F
		internal MemberInitExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000966 RID: 2406
		[CompilerGenerated]
		private readonly NewExpression <NewExpression>k__BackingField;

		// Token: 0x04000967 RID: 2407
		[CompilerGenerated]
		private readonly ReadOnlyCollection<MemberBinding> <Bindings>k__BackingField;
	}
}
