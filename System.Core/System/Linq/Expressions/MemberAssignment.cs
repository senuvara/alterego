﻿using System;
using System.Reflection;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Represents assignment operation for a field or property of an object.</summary>
	// Token: 0x0200025D RID: 605
	public sealed class MemberAssignment : MemberBinding
	{
		// Token: 0x060011A5 RID: 4517 RVA: 0x000354D0 File Offset: 0x000336D0
		internal MemberAssignment(MemberInfo member, Expression expression) : base(MemberBindingType.Assignment, member)
		{
			this._expression = expression;
		}

		/// <summary>Gets the expression to assign to the field or property.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.Expression" /> that represents the value to assign to the field or property.</returns>
		// Token: 0x17000327 RID: 807
		// (get) Token: 0x060011A6 RID: 4518 RVA: 0x000354E1 File Offset: 0x000336E1
		public Expression Expression
		{
			get
			{
				return this._expression;
			}
		}

		/// <summary>Creates a new expression that is like this one, but using the supplied children. If all of the children are the same, it will return this expression.</summary>
		/// <param name="expression">The <see cref="P:System.Linq.Expressions.MemberAssignment.Expression" /> property of the result.</param>
		/// <returns>This expression if no children are changed or an expression with the updated children.</returns>
		// Token: 0x060011A7 RID: 4519 RVA: 0x000354E9 File Offset: 0x000336E9
		public MemberAssignment Update(Expression expression)
		{
			if (expression == this.Expression)
			{
				return this;
			}
			return Expression.Bind(base.Member, expression);
		}

		// Token: 0x060011A8 RID: 4520 RVA: 0x000039E8 File Offset: 0x00001BE8
		internal override void ValidateAsDefinedHere(int index)
		{
		}

		// Token: 0x060011A9 RID: 4521 RVA: 0x0000220F File Offset: 0x0000040F
		internal MemberAssignment()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0400095C RID: 2396
		private readonly Expression _expression;
	}
}
