﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x02000232 RID: 562
	internal class TypedDynamicExpressionN : DynamicExpressionN
	{
		// Token: 0x06000F9F RID: 3999 RVA: 0x00031666 File Offset: 0x0002F866
		internal TypedDynamicExpressionN(Type returnType, Type delegateType, CallSiteBinder binder, IReadOnlyList<Expression> arguments) : base(delegateType, binder, arguments)
		{
			this.Type = returnType;
		}

		// Token: 0x170002DA RID: 730
		// (get) Token: 0x06000FA0 RID: 4000 RVA: 0x00031679 File Offset: 0x0002F879
		public sealed override Type Type
		{
			[CompilerGenerated]
			get
			{
				return this.<Type>k__BackingField;
			}
		}

		// Token: 0x040008C2 RID: 2242
		[CompilerGenerated]
		private readonly Type <Type>k__BackingField;
	}
}
