﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions
{
	// Token: 0x02000224 RID: 548
	internal static class ConstantCheck
	{
		// Token: 0x06000EF4 RID: 3828 RVA: 0x0002F6B8 File Offset: 0x0002D8B8
		internal static bool IsNull(Expression e)
		{
			ExpressionType nodeType = e.NodeType;
			if (nodeType != ExpressionType.Constant)
			{
				return nodeType == ExpressionType.Default && e.Type.IsNullableOrReferenceType();
			}
			return ((ConstantExpression)e).Value == null;
		}

		// Token: 0x06000EF5 RID: 3829 RVA: 0x0002F6F4 File Offset: 0x0002D8F4
		internal static AnalyzeTypeIsResult AnalyzeTypeIs(TypeBinaryExpression typeIs)
		{
			return ConstantCheck.AnalyzeTypeIs(typeIs.Expression, typeIs.TypeOperand);
		}

		// Token: 0x06000EF6 RID: 3830 RVA: 0x0002F708 File Offset: 0x0002D908
		private static AnalyzeTypeIsResult AnalyzeTypeIs(Expression operand, Type testType)
		{
			Type type = operand.Type;
			if (type == typeof(void))
			{
				if (!(testType == typeof(void)))
				{
					return AnalyzeTypeIsResult.KnownFalse;
				}
				return AnalyzeTypeIsResult.KnownTrue;
			}
			else
			{
				if (testType == typeof(void) || testType.IsPointer)
				{
					return AnalyzeTypeIsResult.KnownFalse;
				}
				Type nonNullableType = type.GetNonNullableType();
				if (!testType.GetNonNullableType().IsAssignableFrom(nonNullableType))
				{
					return AnalyzeTypeIsResult.Unknown;
				}
				if (type.IsValueType && !type.IsNullableType())
				{
					return AnalyzeTypeIsResult.KnownTrue;
				}
				return AnalyzeTypeIsResult.KnownAssignable;
			}
		}
	}
}
