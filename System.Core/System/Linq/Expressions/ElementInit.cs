﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Represents an initializer for a single element of an <see cref="T:System.Collections.IEnumerable" /> collection.</summary>
	// Token: 0x0200023D RID: 573
	public sealed class ElementInit : IArgumentProvider
	{
		// Token: 0x06000FD3 RID: 4051 RVA: 0x00032311 File Offset: 0x00030511
		internal ElementInit(MethodInfo addMethod, ReadOnlyCollection<Expression> arguments)
		{
			this.AddMethod = addMethod;
			this.Arguments = arguments;
		}

		/// <summary>Gets the instance method that is used to add an element to an <see cref="T:System.Collections.IEnumerable" /> collection.</summary>
		/// <returns>A <see cref="T:System.Reflection.MethodInfo" /> that represents an instance method that adds an element to a collection.</returns>
		// Token: 0x170002E3 RID: 739
		// (get) Token: 0x06000FD4 RID: 4052 RVA: 0x00032327 File Offset: 0x00030527
		public MethodInfo AddMethod
		{
			[CompilerGenerated]
			get
			{
				return this.<AddMethod>k__BackingField;
			}
		}

		/// <summary>Gets the collection of arguments that are passed to a method that adds an element to an <see cref="T:System.Collections.IEnumerable" /> collection.</summary>
		/// <returns>A <see cref="T:System.Collections.ObjectModel.ReadOnlyCollection`1" /> of <see cref="T:System.Linq.Expressions.Expression" /> objects that represent the arguments for a method that adds an element to a collection.</returns>
		// Token: 0x170002E4 RID: 740
		// (get) Token: 0x06000FD5 RID: 4053 RVA: 0x0003232F File Offset: 0x0003052F
		public ReadOnlyCollection<Expression> Arguments
		{
			[CompilerGenerated]
			get
			{
				return this.<Arguments>k__BackingField;
			}
		}

		// Token: 0x06000FD6 RID: 4054 RVA: 0x00032337 File Offset: 0x00030537
		public Expression GetArgument(int index)
		{
			return this.Arguments[index];
		}

		// Token: 0x170002E5 RID: 741
		// (get) Token: 0x06000FD7 RID: 4055 RVA: 0x00032345 File Offset: 0x00030545
		public int ArgumentCount
		{
			get
			{
				return this.Arguments.Count;
			}
		}

		/// <summary>Returns a textual representation of an <see cref="T:System.Linq.Expressions.ElementInit" /> object.</summary>
		/// <returns>A textual representation of the <see cref="T:System.Linq.Expressions.ElementInit" /> object.</returns>
		// Token: 0x06000FD8 RID: 4056 RVA: 0x00032352 File Offset: 0x00030552
		public override string ToString()
		{
			return ExpressionStringBuilder.ElementInitBindingToString(this);
		}

		/// <summary>Creates a new expression that is like this one, but using the supplied children. If all of the children are the same, it will return this expression.</summary>
		/// <param name="arguments">The <see cref="P:System.Linq.Expressions.ElementInit.Arguments" /> property of the result.</param>
		/// <returns>This expression if no children are changed or an expression with the updated children.</returns>
		// Token: 0x06000FD9 RID: 4057 RVA: 0x0003235A File Offset: 0x0003055A
		public ElementInit Update(IEnumerable<Expression> arguments)
		{
			if (arguments == this.Arguments)
			{
				return this;
			}
			return Expression.ElementInit(this.AddMethod, arguments);
		}

		// Token: 0x06000FDA RID: 4058 RVA: 0x0000220F File Offset: 0x0000040F
		internal ElementInit()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040008D1 RID: 2257
		[CompilerGenerated]
		private readonly MethodInfo <AddMethod>k__BackingField;

		// Token: 0x040008D2 RID: 2258
		[CompilerGenerated]
		private readonly ReadOnlyCollection<Expression> <Arguments>k__BackingField;
	}
}
