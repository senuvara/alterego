﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x02000238 RID: 568
	internal sealed class TypedDynamicExpression3 : DynamicExpression3
	{
		// Token: 0x06000FB7 RID: 4023 RVA: 0x00031972 File Offset: 0x0002FB72
		internal TypedDynamicExpression3(Type retType, Type delegateType, CallSiteBinder binder, Expression arg0, Expression arg1, Expression arg2) : base(delegateType, binder, arg0, arg1, arg2)
		{
			this.Type = retType;
		}

		// Token: 0x170002E0 RID: 736
		// (get) Token: 0x06000FB8 RID: 4024 RVA: 0x00031989 File Offset: 0x0002FB89
		public sealed override Type Type
		{
			[CompilerGenerated]
			get
			{
				return this.<Type>k__BackingField;
			}
		}

		// Token: 0x040008CB RID: 2251
		[CompilerGenerated]
		private readonly Type <Type>k__BackingField;
	}
}
