﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;

namespace System.Linq.Expressions
{
	// Token: 0x02000256 RID: 598
	internal sealed class Expression1<TDelegate> : Expression<TDelegate>
	{
		// Token: 0x06001177 RID: 4471 RVA: 0x00035033 File Offset: 0x00033233
		public Expression1(Expression body, ParameterExpression par0) : base(body)
		{
			this._par0 = par0;
		}

		// Token: 0x17000317 RID: 791
		// (get) Token: 0x06001178 RID: 4472 RVA: 0x00009CDF File Offset: 0x00007EDF
		internal override int ParameterCount
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x06001179 RID: 4473 RVA: 0x00035043 File Offset: 0x00033243
		internal override ParameterExpression GetParameter(int index)
		{
			if (index == 0)
			{
				return ExpressionUtils.ReturnObject<ParameterExpression>(this._par0);
			}
			throw Error.ArgumentOutOfRange("index");
		}

		// Token: 0x0600117A RID: 4474 RVA: 0x00035060 File Offset: 0x00033260
		internal override bool SameParameters(ICollection<ParameterExpression> parameters)
		{
			if (parameters != null && parameters.Count == 1)
			{
				using (IEnumerator<ParameterExpression> enumerator = parameters.GetEnumerator())
				{
					enumerator.MoveNext();
					return enumerator.Current == ExpressionUtils.ReturnObject<ParameterExpression>(this._par0);
				}
				return false;
			}
			return false;
		}

		// Token: 0x0600117B RID: 4475 RVA: 0x000350BC File Offset: 0x000332BC
		internal override ReadOnlyCollection<ParameterExpression> GetOrMakeParameters()
		{
			return ExpressionUtils.ReturnReadOnly(this, ref this._par0);
		}

		// Token: 0x0600117C RID: 4476 RVA: 0x000350CA File Offset: 0x000332CA
		internal override Expression<TDelegate> Rewrite(Expression body, ParameterExpression[] parameters)
		{
			if (parameters != null)
			{
				return Expression.Lambda<TDelegate>(body, parameters);
			}
			return Expression.Lambda<TDelegate>(body, new ParameterExpression[]
			{
				ExpressionUtils.ReturnObject<ParameterExpression>(this._par0)
			});
		}

		// Token: 0x0400094E RID: 2382
		private object _par0;
	}
}
