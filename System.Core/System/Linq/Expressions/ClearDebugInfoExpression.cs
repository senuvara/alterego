﻿using System;

namespace System.Linq.Expressions
{
	// Token: 0x0200022C RID: 556
	internal sealed class ClearDebugInfoExpression : DebugInfoExpression
	{
		// Token: 0x06000F20 RID: 3872 RVA: 0x0002F96E File Offset: 0x0002DB6E
		internal ClearDebugInfoExpression(SymbolDocumentInfo document) : base(document)
		{
		}

		// Token: 0x170002C8 RID: 712
		// (get) Token: 0x06000F21 RID: 3873 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override bool IsClear
		{
			get
			{
				return true;
			}
		}

		// Token: 0x170002C9 RID: 713
		// (get) Token: 0x06000F22 RID: 3874 RVA: 0x0002F977 File Offset: 0x0002DB77
		public override int StartLine
		{
			get
			{
				return 16707566;
			}
		}

		// Token: 0x170002CA RID: 714
		// (get) Token: 0x06000F23 RID: 3875 RVA: 0x00002285 File Offset: 0x00000485
		public override int StartColumn
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x170002CB RID: 715
		// (get) Token: 0x06000F24 RID: 3876 RVA: 0x0002F977 File Offset: 0x0002DB77
		public override int EndLine
		{
			get
			{
				return 16707566;
			}
		}

		// Token: 0x170002CC RID: 716
		// (get) Token: 0x06000F25 RID: 3877 RVA: 0x00002285 File Offset: 0x00000485
		public override int EndColumn
		{
			get
			{
				return 0;
			}
		}
	}
}
