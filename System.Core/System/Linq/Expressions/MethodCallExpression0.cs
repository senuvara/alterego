﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;
using System.Reflection;

namespace System.Linq.Expressions
{
	// Token: 0x0200026A RID: 618
	internal sealed class MethodCallExpression0 : MethodCallExpression, IArgumentProvider
	{
		// Token: 0x060011F4 RID: 4596 RVA: 0x000359D0 File Offset: 0x00033BD0
		public MethodCallExpression0(MethodInfo method) : base(method)
		{
		}

		// Token: 0x060011F5 RID: 4597 RVA: 0x00034AA5 File Offset: 0x00032CA5
		public override Expression GetArgument(int index)
		{
			throw new ArgumentOutOfRangeException("index");
		}

		// Token: 0x1700033E RID: 830
		// (get) Token: 0x060011F6 RID: 4598 RVA: 0x00002285 File Offset: 0x00000485
		public override int ArgumentCount
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x060011F7 RID: 4599 RVA: 0x00034A9E File Offset: 0x00032C9E
		internal override ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			return EmptyReadOnlyCollection<Expression>.Instance;
		}

		// Token: 0x060011F8 RID: 4600 RVA: 0x000359D9 File Offset: 0x00033BD9
		internal override bool SameArguments(ICollection<Expression> arguments)
		{
			return arguments == null || arguments.Count == 0;
		}

		// Token: 0x060011F9 RID: 4601 RVA: 0x000359E9 File Offset: 0x00033BE9
		internal override MethodCallExpression Rewrite(Expression instance, IReadOnlyList<Expression> args)
		{
			return Expression.Call(base.Method);
		}
	}
}
