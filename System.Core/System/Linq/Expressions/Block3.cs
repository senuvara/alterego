﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;

namespace System.Linq.Expressions
{
	// Token: 0x02000217 RID: 535
	internal sealed class Block3 : BlockExpression
	{
		// Token: 0x06000EA8 RID: 3752 RVA: 0x0002EE42 File Offset: 0x0002D042
		internal Block3(Expression arg0, Expression arg1, Expression arg2)
		{
			this._arg0 = arg0;
			this._arg1 = arg1;
			this._arg2 = arg2;
		}

		// Token: 0x06000EA9 RID: 3753 RVA: 0x0002EE60 File Offset: 0x0002D060
		internal override bool SameExpressions(ICollection<Expression> expressions)
		{
			if (expressions.Count == 3)
			{
				ReadOnlyCollection<Expression> readOnlyCollection = this._arg0 as ReadOnlyCollection<Expression>;
				if (readOnlyCollection != null)
				{
					return ExpressionUtils.SameElements<Expression>(expressions, readOnlyCollection);
				}
				using (IEnumerator<Expression> enumerator = expressions.GetEnumerator())
				{
					enumerator.MoveNext();
					if (enumerator.Current == this._arg0)
					{
						enumerator.MoveNext();
						if (enumerator.Current == this._arg1)
						{
							enumerator.MoveNext();
							return enumerator.Current == this._arg2;
						}
					}
				}
				return false;
			}
			return false;
		}

		// Token: 0x06000EAA RID: 3754 RVA: 0x0002EEF8 File Offset: 0x0002D0F8
		internal override Expression GetExpression(int index)
		{
			switch (index)
			{
			case 0:
				return ExpressionUtils.ReturnObject<Expression>(this._arg0);
			case 1:
				return this._arg1;
			case 2:
				return this._arg2;
			default:
				throw Error.ArgumentOutOfRange("index");
			}
		}

		// Token: 0x1700029F RID: 671
		// (get) Token: 0x06000EAB RID: 3755 RVA: 0x0002EF32 File Offset: 0x0002D132
		internal override int ExpressionCount
		{
			get
			{
				return 3;
			}
		}

		// Token: 0x06000EAC RID: 3756 RVA: 0x0002EF35 File Offset: 0x0002D135
		internal override ReadOnlyCollection<Expression> GetOrMakeExpressions()
		{
			return BlockExpression.ReturnReadOnlyExpressions(this, ref this._arg0);
		}

		// Token: 0x06000EAD RID: 3757 RVA: 0x0002EF43 File Offset: 0x0002D143
		internal override BlockExpression Rewrite(ReadOnlyCollection<ParameterExpression> variables, Expression[] args)
		{
			return new Block3(args[0], args[1], args[2]);
		}

		// Token: 0x04000883 RID: 2179
		private object _arg0;

		// Token: 0x04000884 RID: 2180
		private readonly Expression _arg1;

		// Token: 0x04000885 RID: 2181
		private readonly Expression _arg2;
	}
}
