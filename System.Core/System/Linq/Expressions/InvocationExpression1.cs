﻿using System;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;

namespace System.Linq.Expressions
{
	// Token: 0x0200024B RID: 587
	internal sealed class InvocationExpression1 : InvocationExpression
	{
		// Token: 0x06001128 RID: 4392 RVA: 0x00034AB9 File Offset: 0x00032CB9
		public InvocationExpression1(Expression lambda, Type returnType, Expression arg0) : base(lambda, returnType)
		{
			this._arg0 = arg0;
		}

		// Token: 0x06001129 RID: 4393 RVA: 0x00034ACA File Offset: 0x00032CCA
		internal override ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			return ExpressionUtils.ReturnReadOnly(this, ref this._arg0);
		}

		// Token: 0x0600112A RID: 4394 RVA: 0x00034AD8 File Offset: 0x00032CD8
		public override Expression GetArgument(int index)
		{
			if (index == 0)
			{
				return ExpressionUtils.ReturnObject<Expression>(this._arg0);
			}
			throw new ArgumentOutOfRangeException("index");
		}

		// Token: 0x170002FC RID: 764
		// (get) Token: 0x0600112B RID: 4395 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ArgumentCount
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x0600112C RID: 4396 RVA: 0x00034AF3 File Offset: 0x00032CF3
		internal override InvocationExpression Rewrite(Expression lambda, Expression[] arguments)
		{
			if (arguments != null)
			{
				return Expression.Invoke(lambda, arguments[0]);
			}
			return Expression.Invoke(lambda, ExpressionUtils.ReturnObject<Expression>(this._arg0));
		}

		// Token: 0x0400093A RID: 2362
		private object _arg0;
	}
}
