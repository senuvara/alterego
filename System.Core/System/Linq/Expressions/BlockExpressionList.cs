﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Dynamic.Utils;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x0200021F RID: 543
	internal class BlockExpressionList : IList<Expression>, ICollection<Expression>, IEnumerable<Expression>, IEnumerable
	{
		// Token: 0x06000ED6 RID: 3798 RVA: 0x0002F45C File Offset: 0x0002D65C
		internal BlockExpressionList(BlockExpression provider, Expression arg0)
		{
			this._block = provider;
			this._arg0 = arg0;
		}

		// Token: 0x06000ED7 RID: 3799 RVA: 0x0002F474 File Offset: 0x0002D674
		public int IndexOf(Expression item)
		{
			if (this._arg0 == item)
			{
				return 0;
			}
			for (int i = 1; i < this._block.ExpressionCount; i++)
			{
				if (this._block.GetExpression(i) == item)
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x06000ED8 RID: 3800 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		public void Insert(int index, Expression item)
		{
			throw ContractUtils.Unreachable;
		}

		// Token: 0x06000ED9 RID: 3801 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		public void RemoveAt(int index)
		{
			throw ContractUtils.Unreachable;
		}

		// Token: 0x170002A8 RID: 680
		public Expression this[int index]
		{
			get
			{
				if (index == 0)
				{
					return this._arg0;
				}
				return this._block.GetExpression(index);
			}
			[ExcludeFromCodeCoverage]
			set
			{
				throw ContractUtils.Unreachable;
			}
		}

		// Token: 0x06000EDC RID: 3804 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		public void Add(Expression item)
		{
			throw ContractUtils.Unreachable;
		}

		// Token: 0x06000EDD RID: 3805 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		public void Clear()
		{
			throw ContractUtils.Unreachable;
		}

		// Token: 0x06000EDE RID: 3806 RVA: 0x0002F4CC File Offset: 0x0002D6CC
		public bool Contains(Expression item)
		{
			return this.IndexOf(item) != -1;
		}

		// Token: 0x06000EDF RID: 3807 RVA: 0x0002F4DC File Offset: 0x0002D6DC
		public void CopyTo(Expression[] array, int index)
		{
			ContractUtils.RequiresNotNull(array, "array");
			if (index < 0)
			{
				throw Error.ArgumentOutOfRange("index");
			}
			int expressionCount = this._block.ExpressionCount;
			if (index + expressionCount > array.Length)
			{
				throw new ArgumentException();
			}
			array[index++] = this._arg0;
			for (int i = 1; i < expressionCount; i++)
			{
				array[index++] = this._block.GetExpression(i);
			}
		}

		// Token: 0x170002A9 RID: 681
		// (get) Token: 0x06000EE0 RID: 3808 RVA: 0x0002F54B File Offset: 0x0002D74B
		public int Count
		{
			get
			{
				return this._block.ExpressionCount;
			}
		}

		// Token: 0x170002AA RID: 682
		// (get) Token: 0x06000EE1 RID: 3809 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		public bool IsReadOnly
		{
			get
			{
				throw ContractUtils.Unreachable;
			}
		}

		// Token: 0x06000EE2 RID: 3810 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		public bool Remove(Expression item)
		{
			throw ContractUtils.Unreachable;
		}

		// Token: 0x06000EE3 RID: 3811 RVA: 0x0002F558 File Offset: 0x0002D758
		public IEnumerator<Expression> GetEnumerator()
		{
			yield return this._arg0;
			int num;
			for (int i = 1; i < this._block.ExpressionCount; i = num + 1)
			{
				yield return this._block.GetExpression(i);
				num = i;
			}
			yield break;
		}

		// Token: 0x06000EE4 RID: 3812 RVA: 0x0002F567 File Offset: 0x0002D767
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x04000894 RID: 2196
		private readonly BlockExpression _block;

		// Token: 0x04000895 RID: 2197
		private readonly Expression _arg0;

		// Token: 0x02000220 RID: 544
		[CompilerGenerated]
		private sealed class <GetEnumerator>d__18 : IEnumerator<Expression>, IDisposable, IEnumerator
		{
			// Token: 0x06000EE5 RID: 3813 RVA: 0x0002F56F File Offset: 0x0002D76F
			[DebuggerHidden]
			public <GetEnumerator>d__18(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x06000EE6 RID: 3814 RVA: 0x000039E8 File Offset: 0x00001BE8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06000EE7 RID: 3815 RVA: 0x0002F580 File Offset: 0x0002D780
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				BlockExpressionList blockExpressionList = this;
				switch (num)
				{
				case 0:
					this.<>1__state = -1;
					this.<>2__current = blockExpressionList._arg0;
					this.<>1__state = 1;
					return true;
				case 1:
					this.<>1__state = -1;
					i = 1;
					break;
				case 2:
				{
					this.<>1__state = -1;
					int num2 = i;
					i = num2 + 1;
					break;
				}
				default:
					return false;
				}
				if (i >= blockExpressionList._block.ExpressionCount)
				{
					return false;
				}
				this.<>2__current = blockExpressionList._block.GetExpression(i);
				this.<>1__state = 2;
				return true;
			}

			// Token: 0x170002AB RID: 683
			// (get) Token: 0x06000EE8 RID: 3816 RVA: 0x0002F626 File Offset: 0x0002D826
			Expression IEnumerator<Expression>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000EE9 RID: 3817 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170002AC RID: 684
			// (get) Token: 0x06000EEA RID: 3818 RVA: 0x0002F626 File Offset: 0x0002D826
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x04000896 RID: 2198
			private int <>1__state;

			// Token: 0x04000897 RID: 2199
			private Expression <>2__current;

			// Token: 0x04000898 RID: 2200
			public BlockExpressionList <>4__this;

			// Token: 0x04000899 RID: 2201
			private int <i>5__1;
		}
	}
}
