﻿using System;

namespace System.Linq.Expressions
{
	// Token: 0x02000226 RID: 550
	internal class FullConditionalExpression : ConditionalExpression
	{
		// Token: 0x06000F02 RID: 3842 RVA: 0x0002F867 File Offset: 0x0002DA67
		internal FullConditionalExpression(Expression test, Expression ifTrue, Expression ifFalse) : base(test, ifTrue)
		{
			this._false = ifFalse;
		}

		// Token: 0x06000F03 RID: 3843 RVA: 0x0002F878 File Offset: 0x0002DA78
		internal override Expression GetFalse()
		{
			return this._false;
		}

		// Token: 0x040008A5 RID: 2213
		private readonly Expression _false;
	}
}
