﻿using System;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Used to represent the target of a <see cref="T:System.Linq.Expressions.GotoExpression" />.</summary>
	// Token: 0x02000251 RID: 593
	public sealed class LabelTarget
	{
		// Token: 0x06001149 RID: 4425 RVA: 0x00034E34 File Offset: 0x00033034
		internal LabelTarget(Type type, string name)
		{
			this.Type = type;
			this.Name = name;
		}

		/// <summary>Gets the name of the label.</summary>
		/// <returns>The name of the label.</returns>
		// Token: 0x17000305 RID: 773
		// (get) Token: 0x0600114A RID: 4426 RVA: 0x00034E4A File Offset: 0x0003304A
		public string Name
		{
			[CompilerGenerated]
			get
			{
				return this.<Name>k__BackingField;
			}
		}

		/// <summary>The type of value that is passed when jumping to the label (or <see cref="T:System.Void" /> if no value should be passed).</summary>
		/// <returns>The <see cref="T:System.Type" /> object representing the type of the value that is passed when jumping to the label or <see cref="T:System.Void" /> if no value should be passed</returns>
		// Token: 0x17000306 RID: 774
		// (get) Token: 0x0600114B RID: 4427 RVA: 0x00034E52 File Offset: 0x00033052
		public Type Type
		{
			[CompilerGenerated]
			get
			{
				return this.<Type>k__BackingField;
			}
		}

		/// <summary>Returns a <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.</summary>
		/// <returns>A <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.</returns>
		// Token: 0x0600114C RID: 4428 RVA: 0x00034E5A File Offset: 0x0003305A
		public override string ToString()
		{
			if (!string.IsNullOrEmpty(this.Name))
			{
				return this.Name;
			}
			return "UnamedLabel";
		}

		// Token: 0x0600114D RID: 4429 RVA: 0x0000220F File Offset: 0x0000040F
		internal LabelTarget()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0400094B RID: 2379
		[CompilerGenerated]
		private readonly string <Name>k__BackingField;

		// Token: 0x0400094C RID: 2380
		[CompilerGenerated]
		private readonly Type <Type>k__BackingField;
	}
}
