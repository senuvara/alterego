﻿using System;
using System.Reflection;

namespace System.Linq.Expressions
{
	// Token: 0x02000267 RID: 615
	internal class InstanceMethodCallExpression : MethodCallExpression, IArgumentProvider
	{
		// Token: 0x060011E6 RID: 4582 RVA: 0x000358FA File Offset: 0x00033AFA
		public InstanceMethodCallExpression(MethodInfo method, Expression instance) : base(method)
		{
			this._instance = instance;
		}

		// Token: 0x060011E7 RID: 4583 RVA: 0x0003590A File Offset: 0x00033B0A
		internal override Expression GetInstance()
		{
			return this._instance;
		}

		// Token: 0x0400096B RID: 2411
		private readonly Expression _instance;
	}
}
