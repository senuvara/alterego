﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;

namespace System.Linq.Expressions
{
	// Token: 0x02000216 RID: 534
	internal sealed class Block2 : BlockExpression
	{
		// Token: 0x06000EA2 RID: 3746 RVA: 0x0002ED63 File Offset: 0x0002CF63
		internal Block2(Expression arg0, Expression arg1)
		{
			this._arg0 = arg0;
			this._arg1 = arg1;
		}

		// Token: 0x06000EA3 RID: 3747 RVA: 0x0002ED79 File Offset: 0x0002CF79
		internal override Expression GetExpression(int index)
		{
			if (index == 0)
			{
				return ExpressionUtils.ReturnObject<Expression>(this._arg0);
			}
			if (index != 1)
			{
				throw Error.ArgumentOutOfRange("index");
			}
			return this._arg1;
		}

		// Token: 0x06000EA4 RID: 3748 RVA: 0x0002EDA4 File Offset: 0x0002CFA4
		internal override bool SameExpressions(ICollection<Expression> expressions)
		{
			if (expressions.Count == 2)
			{
				ReadOnlyCollection<Expression> readOnlyCollection = this._arg0 as ReadOnlyCollection<Expression>;
				if (readOnlyCollection != null)
				{
					return ExpressionUtils.SameElements<Expression>(expressions, readOnlyCollection);
				}
				using (IEnumerator<Expression> enumerator = expressions.GetEnumerator())
				{
					enumerator.MoveNext();
					if (enumerator.Current == this._arg0)
					{
						enumerator.MoveNext();
						return enumerator.Current == this._arg1;
					}
				}
				return false;
			}
			return false;
		}

		// Token: 0x1700029E RID: 670
		// (get) Token: 0x06000EA5 RID: 3749 RVA: 0x0002EE24 File Offset: 0x0002D024
		internal override int ExpressionCount
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x06000EA6 RID: 3750 RVA: 0x0002EE27 File Offset: 0x0002D027
		internal override ReadOnlyCollection<Expression> GetOrMakeExpressions()
		{
			return BlockExpression.ReturnReadOnlyExpressions(this, ref this._arg0);
		}

		// Token: 0x06000EA7 RID: 3751 RVA: 0x0002EE35 File Offset: 0x0002D035
		internal override BlockExpression Rewrite(ReadOnlyCollection<ParameterExpression> variables, Expression[] args)
		{
			return new Block2(args[0], args[1]);
		}

		// Token: 0x04000881 RID: 2177
		private object _arg0;

		// Token: 0x04000882 RID: 2178
		private readonly Expression _arg1;
	}
}
