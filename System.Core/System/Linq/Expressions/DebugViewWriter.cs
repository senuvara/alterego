﻿using System;
using System.Collections.Generic;
using System.Dynamic.Utils;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x0200022D RID: 557
	internal sealed class DebugViewWriter : ExpressionVisitor
	{
		// Token: 0x06000F26 RID: 3878 RVA: 0x0002F97E File Offset: 0x0002DB7E
		private DebugViewWriter(TextWriter file)
		{
			this._out = file;
		}

		// Token: 0x170002CD RID: 717
		// (get) Token: 0x06000F27 RID: 3879 RVA: 0x0002F998 File Offset: 0x0002DB98
		private int Base
		{
			get
			{
				if (this._stack.Count <= 0)
				{
					return 0;
				}
				return this._stack.Peek();
			}
		}

		// Token: 0x170002CE RID: 718
		// (get) Token: 0x06000F28 RID: 3880 RVA: 0x0002F9B5 File Offset: 0x0002DBB5
		private int Delta
		{
			get
			{
				return this._delta;
			}
		}

		// Token: 0x170002CF RID: 719
		// (get) Token: 0x06000F29 RID: 3881 RVA: 0x0002F9BD File Offset: 0x0002DBBD
		private int Depth
		{
			get
			{
				return this.Base + this.Delta;
			}
		}

		// Token: 0x06000F2A RID: 3882 RVA: 0x0002F9CC File Offset: 0x0002DBCC
		private void Indent()
		{
			this._delta += 4;
		}

		// Token: 0x06000F2B RID: 3883 RVA: 0x0002F9DC File Offset: 0x0002DBDC
		private void Dedent()
		{
			this._delta -= 4;
		}

		// Token: 0x06000F2C RID: 3884 RVA: 0x0002F9EC File Offset: 0x0002DBEC
		private void NewLine()
		{
			this._flow = DebugViewWriter.Flow.NewLine;
		}

		// Token: 0x06000F2D RID: 3885 RVA: 0x0002F9F8 File Offset: 0x0002DBF8
		private static int GetId<T>(T e, ref Dictionary<T, int> ids)
		{
			if (ids == null)
			{
				ids = new Dictionary<T, int>();
				ids.Add(e, 1);
				return 1;
			}
			int num;
			if (!ids.TryGetValue(e, out num))
			{
				num = ids.Count + 1;
				ids.Add(e, num);
			}
			return num;
		}

		// Token: 0x06000F2E RID: 3886 RVA: 0x0002FA3B File Offset: 0x0002DC3B
		private int GetLambdaId(LambdaExpression le)
		{
			return DebugViewWriter.GetId<LambdaExpression>(le, ref this._lambdaIds);
		}

		// Token: 0x06000F2F RID: 3887 RVA: 0x0002FA49 File Offset: 0x0002DC49
		private int GetParamId(ParameterExpression p)
		{
			return DebugViewWriter.GetId<ParameterExpression>(p, ref this._paramIds);
		}

		// Token: 0x06000F30 RID: 3888 RVA: 0x0002FA57 File Offset: 0x0002DC57
		private int GetLabelTargetId(LabelTarget target)
		{
			return DebugViewWriter.GetId<LabelTarget>(target, ref this._labelIds);
		}

		// Token: 0x06000F31 RID: 3889 RVA: 0x0002FA65 File Offset: 0x0002DC65
		internal static void WriteTo(Expression node, TextWriter writer)
		{
			new DebugViewWriter(writer).WriteTo(node);
		}

		// Token: 0x06000F32 RID: 3890 RVA: 0x0002FA74 File Offset: 0x0002DC74
		private void WriteTo(Expression node)
		{
			LambdaExpression lambdaExpression = node as LambdaExpression;
			if (lambdaExpression != null)
			{
				this.WriteLambda(lambdaExpression);
			}
			else
			{
				this.Visit(node);
			}
			while (this._lambdas != null && this._lambdas.Count > 0)
			{
				this.WriteLine();
				this.WriteLine();
				this.WriteLambda(this._lambdas.Dequeue());
			}
		}

		// Token: 0x06000F33 RID: 3891 RVA: 0x0002FAD1 File Offset: 0x0002DCD1
		private void Out(string s)
		{
			this.Out(DebugViewWriter.Flow.None, s, DebugViewWriter.Flow.None);
		}

		// Token: 0x06000F34 RID: 3892 RVA: 0x0002FADC File Offset: 0x0002DCDC
		private void Out(DebugViewWriter.Flow before, string s)
		{
			this.Out(before, s, DebugViewWriter.Flow.None);
		}

		// Token: 0x06000F35 RID: 3893 RVA: 0x0002FAE7 File Offset: 0x0002DCE7
		private void Out(string s, DebugViewWriter.Flow after)
		{
			this.Out(DebugViewWriter.Flow.None, s, after);
		}

		// Token: 0x06000F36 RID: 3894 RVA: 0x0002FAF4 File Offset: 0x0002DCF4
		private void Out(DebugViewWriter.Flow before, string s, DebugViewWriter.Flow after)
		{
			switch (this.GetFlow(before))
			{
			case DebugViewWriter.Flow.Space:
				this.Write(" ");
				break;
			case DebugViewWriter.Flow.NewLine:
				this.WriteLine();
				this.Write(new string(' ', this.Depth));
				break;
			}
			this.Write(s);
			this._flow = after;
		}

		// Token: 0x06000F37 RID: 3895 RVA: 0x0002FB51 File Offset: 0x0002DD51
		private void WriteLine()
		{
			this._out.WriteLine();
			this._column = 0;
		}

		// Token: 0x06000F38 RID: 3896 RVA: 0x0002FB65 File Offset: 0x0002DD65
		private void Write(string s)
		{
			this._out.Write(s);
			this._column += s.Length;
		}

		// Token: 0x06000F39 RID: 3897 RVA: 0x0002FB86 File Offset: 0x0002DD86
		private DebugViewWriter.Flow GetFlow(DebugViewWriter.Flow flow)
		{
			int val = (int)this.CheckBreak(this._flow);
			flow = this.CheckBreak(flow);
			return (DebugViewWriter.Flow)Math.Max(val, (int)flow);
		}

		// Token: 0x06000F3A RID: 3898 RVA: 0x0002FBA3 File Offset: 0x0002DDA3
		private DebugViewWriter.Flow CheckBreak(DebugViewWriter.Flow flow)
		{
			if ((flow & DebugViewWriter.Flow.Break) != DebugViewWriter.Flow.None)
			{
				if (this._column > 120 + this.Depth)
				{
					flow = DebugViewWriter.Flow.NewLine;
				}
				else
				{
					flow &= ~DebugViewWriter.Flow.Break;
				}
			}
			return flow;
		}

		// Token: 0x06000F3B RID: 3899 RVA: 0x0002FBCE File Offset: 0x0002DDCE
		private void VisitExpressions<T>(char open, IReadOnlyList<T> expressions) where T : Expression
		{
			this.VisitExpressions<T>(open, ',', expressions);
		}

		// Token: 0x06000F3C RID: 3900 RVA: 0x0002FBDA File Offset: 0x0002DDDA
		private void VisitExpressions<T>(char open, char separator, IReadOnlyList<T> expressions) where T : Expression
		{
			this.VisitExpressions<T>(open, separator, expressions, delegate(T e)
			{
				this.Visit(e);
			});
		}

		// Token: 0x06000F3D RID: 3901 RVA: 0x0002FBF1 File Offset: 0x0002DDF1
		private void VisitDeclarations(IReadOnlyList<ParameterExpression> expressions)
		{
			this.VisitExpressions<ParameterExpression>('(', ',', expressions, delegate(ParameterExpression variable)
			{
				this.Out(variable.Type.ToString());
				if (variable.IsByRef)
				{
					this.Out("&");
				}
				this.Out(" ");
				this.VisitParameter(variable);
			});
		}

		// Token: 0x06000F3E RID: 3902 RVA: 0x0002FC0C File Offset: 0x0002DE0C
		private void VisitExpressions<T>(char open, char separator, IReadOnlyList<T> expressions, Action<T> visit)
		{
			this.Out(open.ToString());
			if (expressions != null)
			{
				this.Indent();
				bool flag = true;
				foreach (T obj in expressions)
				{
					if (flag)
					{
						if (open == '{' || expressions.Count > 1)
						{
							this.NewLine();
						}
						flag = false;
					}
					else
					{
						this.Out(separator.ToString(), DebugViewWriter.Flow.NewLine);
					}
					visit(obj);
				}
				this.Dedent();
			}
			char c;
			if (open != '(')
			{
				if (open != '[')
				{
					if (open != '{')
					{
						throw ContractUtils.Unreachable;
					}
					c = '}';
				}
				else
				{
					c = ']';
				}
			}
			else
			{
				c = ')';
			}
			if (open == '{')
			{
				this.NewLine();
			}
			this.Out(c.ToString(), DebugViewWriter.Flow.Break);
		}

		// Token: 0x06000F3F RID: 3903 RVA: 0x0002FCE0 File Offset: 0x0002DEE0
		protected internal override Expression VisitBinary(BinaryExpression node)
		{
			if (node.NodeType == ExpressionType.ArrayIndex)
			{
				this.ParenthesizedVisit(node, node.Left);
				this.Out("[");
				this.Visit(node.Right);
				this.Out("]");
			}
			else
			{
				bool flag = DebugViewWriter.NeedsParentheses(node, node.Left);
				bool flag2 = DebugViewWriter.NeedsParentheses(node, node.Right);
				DebugViewWriter.Flow before = DebugViewWriter.Flow.Space;
				ExpressionType nodeType = node.NodeType;
				string s;
				switch (nodeType)
				{
				case ExpressionType.Add:
					s = "+";
					goto IL_2F0;
				case ExpressionType.AddChecked:
					s = "#+";
					goto IL_2F0;
				case ExpressionType.And:
					s = "&";
					goto IL_2F0;
				case ExpressionType.AndAlso:
					s = "&&";
					before = (DebugViewWriter.Flow.Space | DebugViewWriter.Flow.Break);
					goto IL_2F0;
				case ExpressionType.ArrayLength:
				case ExpressionType.ArrayIndex:
				case ExpressionType.Call:
				case ExpressionType.Conditional:
				case ExpressionType.Constant:
				case ExpressionType.Convert:
				case ExpressionType.ConvertChecked:
				case ExpressionType.Invoke:
				case ExpressionType.Lambda:
				case ExpressionType.ListInit:
				case ExpressionType.MemberAccess:
				case ExpressionType.MemberInit:
				case ExpressionType.Negate:
				case ExpressionType.UnaryPlus:
				case ExpressionType.NegateChecked:
				case ExpressionType.New:
				case ExpressionType.NewArrayInit:
				case ExpressionType.NewArrayBounds:
				case ExpressionType.Not:
				case ExpressionType.Parameter:
				case ExpressionType.Quote:
				case ExpressionType.TypeAs:
				case ExpressionType.TypeIs:
					break;
				case ExpressionType.Coalesce:
					s = "??";
					goto IL_2F0;
				case ExpressionType.Divide:
					s = "/";
					goto IL_2F0;
				case ExpressionType.Equal:
					s = "==";
					goto IL_2F0;
				case ExpressionType.ExclusiveOr:
					s = "^";
					goto IL_2F0;
				case ExpressionType.GreaterThan:
					s = ">";
					goto IL_2F0;
				case ExpressionType.GreaterThanOrEqual:
					s = ">=";
					goto IL_2F0;
				case ExpressionType.LeftShift:
					s = "<<";
					goto IL_2F0;
				case ExpressionType.LessThan:
					s = "<";
					goto IL_2F0;
				case ExpressionType.LessThanOrEqual:
					s = "<=";
					goto IL_2F0;
				case ExpressionType.Modulo:
					s = "%";
					goto IL_2F0;
				case ExpressionType.Multiply:
					s = "*";
					goto IL_2F0;
				case ExpressionType.MultiplyChecked:
					s = "#*";
					goto IL_2F0;
				case ExpressionType.NotEqual:
					s = "!=";
					goto IL_2F0;
				case ExpressionType.Or:
					s = "|";
					goto IL_2F0;
				case ExpressionType.OrElse:
					s = "||";
					before = (DebugViewWriter.Flow.Space | DebugViewWriter.Flow.Break);
					goto IL_2F0;
				case ExpressionType.Power:
					s = "**";
					goto IL_2F0;
				case ExpressionType.RightShift:
					s = ">>";
					goto IL_2F0;
				case ExpressionType.Subtract:
					s = "-";
					goto IL_2F0;
				case ExpressionType.SubtractChecked:
					s = "#-";
					goto IL_2F0;
				case ExpressionType.Assign:
					s = "=";
					goto IL_2F0;
				default:
					switch (nodeType)
					{
					case ExpressionType.AddAssign:
						s = "+=";
						goto IL_2F0;
					case ExpressionType.AndAssign:
						s = "&=";
						goto IL_2F0;
					case ExpressionType.DivideAssign:
						s = "/=";
						goto IL_2F0;
					case ExpressionType.ExclusiveOrAssign:
						s = "^=";
						goto IL_2F0;
					case ExpressionType.LeftShiftAssign:
						s = "<<=";
						goto IL_2F0;
					case ExpressionType.ModuloAssign:
						s = "%=";
						goto IL_2F0;
					case ExpressionType.MultiplyAssign:
						s = "*=";
						goto IL_2F0;
					case ExpressionType.OrAssign:
						s = "|=";
						goto IL_2F0;
					case ExpressionType.PowerAssign:
						s = "**=";
						goto IL_2F0;
					case ExpressionType.RightShiftAssign:
						s = ">>=";
						goto IL_2F0;
					case ExpressionType.SubtractAssign:
						s = "-=";
						goto IL_2F0;
					case ExpressionType.AddAssignChecked:
						s = "#+=";
						goto IL_2F0;
					case ExpressionType.MultiplyAssignChecked:
						s = "#*=";
						goto IL_2F0;
					case ExpressionType.SubtractAssignChecked:
						s = "#-=";
						goto IL_2F0;
					}
					break;
				}
				throw new InvalidOperationException();
				IL_2F0:
				if (flag)
				{
					this.Out("(", DebugViewWriter.Flow.None);
				}
				this.Visit(node.Left);
				if (flag)
				{
					this.Out(DebugViewWriter.Flow.None, ")", DebugViewWriter.Flow.Break);
				}
				this.Out(before, s, DebugViewWriter.Flow.Space | DebugViewWriter.Flow.Break);
				if (flag2)
				{
					this.Out("(", DebugViewWriter.Flow.None);
				}
				this.Visit(node.Right);
				if (flag2)
				{
					this.Out(DebugViewWriter.Flow.None, ")", DebugViewWriter.Flow.Break);
				}
			}
			return node;
		}

		// Token: 0x06000F40 RID: 3904 RVA: 0x0003004C File Offset: 0x0002E24C
		protected internal override Expression VisitParameter(ParameterExpression node)
		{
			this.Out("$");
			if (string.IsNullOrEmpty(node.Name))
			{
				int paramId = this.GetParamId(node);
				this.Out("var" + paramId);
			}
			else
			{
				this.Out(DebugViewWriter.GetDisplayName(node.Name));
			}
			return node;
		}

		// Token: 0x06000F41 RID: 3905 RVA: 0x000300A4 File Offset: 0x0002E2A4
		protected internal override Expression VisitLambda<T>(Expression<T> node)
		{
			this.Out(string.Format(CultureInfo.CurrentCulture, ".Lambda {0}<{1}>", this.GetLambdaName(node), node.Type.ToString()));
			if (this._lambdas == null)
			{
				this._lambdas = new Queue<LambdaExpression>();
			}
			if (!this._lambdas.Contains(node))
			{
				this._lambdas.Enqueue(node);
			}
			return node;
		}

		// Token: 0x06000F42 RID: 3906 RVA: 0x00030108 File Offset: 0x0002E308
		private static bool IsSimpleExpression(Expression node)
		{
			BinaryExpression binaryExpression = node as BinaryExpression;
			return binaryExpression != null && !(binaryExpression.Left is BinaryExpression) && !(binaryExpression.Right is BinaryExpression);
		}

		// Token: 0x06000F43 RID: 3907 RVA: 0x00030144 File Offset: 0x0002E344
		protected internal override Expression VisitConditional(ConditionalExpression node)
		{
			if (DebugViewWriter.IsSimpleExpression(node.Test))
			{
				this.Out(".If (");
				this.Visit(node.Test);
				this.Out(") {", DebugViewWriter.Flow.NewLine);
			}
			else
			{
				this.Out(".If (", DebugViewWriter.Flow.NewLine);
				this.Indent();
				this.Visit(node.Test);
				this.Dedent();
				this.Out(DebugViewWriter.Flow.NewLine, ") {", DebugViewWriter.Flow.NewLine);
			}
			this.Indent();
			this.Visit(node.IfTrue);
			this.Dedent();
			this.Out(DebugViewWriter.Flow.NewLine, "} .Else {", DebugViewWriter.Flow.NewLine);
			this.Indent();
			this.Visit(node.IfFalse);
			this.Dedent();
			this.Out(DebugViewWriter.Flow.NewLine, "}");
			return node;
		}

		// Token: 0x06000F44 RID: 3908 RVA: 0x00030204 File Offset: 0x0002E404
		protected internal override Expression VisitConstant(ConstantExpression node)
		{
			object value = node.Value;
			if (value == null)
			{
				this.Out("null");
			}
			else if (value is string && node.Type == typeof(string))
			{
				this.Out(string.Format(CultureInfo.CurrentCulture, "\"{0}\"", value));
			}
			else if (value is char && node.Type == typeof(char))
			{
				this.Out(string.Format(CultureInfo.CurrentCulture, "'{0}'", value));
			}
			else if ((value is int && node.Type == typeof(int)) || (value is bool && node.Type == typeof(bool)))
			{
				this.Out(value.ToString());
			}
			else
			{
				string constantValueSuffix = DebugViewWriter.GetConstantValueSuffix(node.Type);
				if (constantValueSuffix != null)
				{
					this.Out(value.ToString());
					this.Out(constantValueSuffix);
				}
				else
				{
					this.Out(string.Format(CultureInfo.CurrentCulture, ".Constant<{0}>({1})", node.Type.ToString(), value));
				}
			}
			return node;
		}

		// Token: 0x06000F45 RID: 3909 RVA: 0x00030334 File Offset: 0x0002E534
		private static string GetConstantValueSuffix(Type type)
		{
			if (type == typeof(uint))
			{
				return "U";
			}
			if (type == typeof(long))
			{
				return "L";
			}
			if (type == typeof(ulong))
			{
				return "UL";
			}
			if (type == typeof(double))
			{
				return "D";
			}
			if (type == typeof(float))
			{
				return "F";
			}
			if (type == typeof(decimal))
			{
				return "M";
			}
			return null;
		}

		// Token: 0x06000F46 RID: 3910 RVA: 0x000303D2 File Offset: 0x0002E5D2
		protected internal override Expression VisitRuntimeVariables(RuntimeVariablesExpression node)
		{
			this.Out(".RuntimeVariables");
			this.VisitExpressions<ParameterExpression>('(', node.Variables);
			return node;
		}

		// Token: 0x06000F47 RID: 3911 RVA: 0x000303F0 File Offset: 0x0002E5F0
		private void OutMember(Expression node, Expression instance, MemberInfo member)
		{
			if (instance != null)
			{
				this.ParenthesizedVisit(node, instance);
				this.Out("." + member.Name);
				return;
			}
			this.Out(member.DeclaringType.ToString() + "." + member.Name);
		}

		// Token: 0x06000F48 RID: 3912 RVA: 0x00030440 File Offset: 0x0002E640
		protected internal override Expression VisitMember(MemberExpression node)
		{
			this.OutMember(node, node.Expression, node.Member);
			return node;
		}

		// Token: 0x06000F49 RID: 3913 RVA: 0x00030456 File Offset: 0x0002E656
		protected internal override Expression VisitInvocation(InvocationExpression node)
		{
			this.Out(".Invoke ");
			this.ParenthesizedVisit(node, node.Expression);
			this.VisitExpressions<Expression>('(', node.Arguments);
			return node;
		}

		// Token: 0x06000F4A RID: 3914 RVA: 0x00030480 File Offset: 0x0002E680
		private static bool NeedsParentheses(Expression parent, Expression child)
		{
			if (child == null)
			{
				return false;
			}
			ExpressionType nodeType = parent.NodeType;
			if (nodeType <= ExpressionType.Increment)
			{
				if (nodeType != ExpressionType.Decrement && nodeType != ExpressionType.Increment)
				{
					goto IL_2B;
				}
			}
			else if (nodeType != ExpressionType.Unbox && nodeType - ExpressionType.IsTrue > 1)
			{
				goto IL_2B;
			}
			return true;
			IL_2B:
			int operatorPrecedence = DebugViewWriter.GetOperatorPrecedence(child);
			int operatorPrecedence2 = DebugViewWriter.GetOperatorPrecedence(parent);
			if (operatorPrecedence == operatorPrecedence2)
			{
				nodeType = parent.NodeType;
				if (nodeType <= ExpressionType.ExclusiveOr)
				{
					if (nodeType <= ExpressionType.AndAlso)
					{
						if (nodeType <= ExpressionType.AddChecked)
						{
							return false;
						}
						if (nodeType - ExpressionType.And > 1)
						{
							return true;
						}
					}
					else
					{
						if (nodeType == ExpressionType.Divide)
						{
							goto IL_8C;
						}
						if (nodeType != ExpressionType.ExclusiveOr)
						{
							return true;
						}
					}
				}
				else if (nodeType <= ExpressionType.MultiplyChecked)
				{
					if (nodeType == ExpressionType.Modulo)
					{
						goto IL_8C;
					}
					if (nodeType - ExpressionType.Multiply > 1)
					{
						return true;
					}
					return false;
				}
				else if (nodeType - ExpressionType.Or > 1)
				{
					if (nodeType - ExpressionType.Subtract > 1)
					{
						return true;
					}
					goto IL_8C;
				}
				return false;
				IL_8C:
				BinaryExpression binaryExpression = parent as BinaryExpression;
				return child == binaryExpression.Right;
			}
			return (child != null && child.NodeType == ExpressionType.Constant && (parent.NodeType == ExpressionType.Negate || parent.NodeType == ExpressionType.NegateChecked)) || operatorPrecedence < operatorPrecedence2;
		}

		// Token: 0x06000F4B RID: 3915 RVA: 0x00030554 File Offset: 0x0002E754
		private static int GetOperatorPrecedence(Expression node)
		{
			switch (node.NodeType)
			{
			case ExpressionType.Add:
			case ExpressionType.AddChecked:
			case ExpressionType.Subtract:
			case ExpressionType.SubtractChecked:
				return 10;
			case ExpressionType.And:
				return 6;
			case ExpressionType.AndAlso:
				return 3;
			case ExpressionType.Coalesce:
			case ExpressionType.Assign:
			case ExpressionType.AddAssign:
			case ExpressionType.AndAssign:
			case ExpressionType.DivideAssign:
			case ExpressionType.ExclusiveOrAssign:
			case ExpressionType.LeftShiftAssign:
			case ExpressionType.ModuloAssign:
			case ExpressionType.MultiplyAssign:
			case ExpressionType.OrAssign:
			case ExpressionType.PowerAssign:
			case ExpressionType.RightShiftAssign:
			case ExpressionType.SubtractAssign:
			case ExpressionType.AddAssignChecked:
			case ExpressionType.MultiplyAssignChecked:
			case ExpressionType.SubtractAssignChecked:
				return 1;
			case ExpressionType.Constant:
			case ExpressionType.Parameter:
				return 15;
			case ExpressionType.Convert:
			case ExpressionType.ConvertChecked:
			case ExpressionType.Negate:
			case ExpressionType.UnaryPlus:
			case ExpressionType.NegateChecked:
			case ExpressionType.Not:
			case ExpressionType.Decrement:
			case ExpressionType.Increment:
			case ExpressionType.Throw:
			case ExpressionType.Unbox:
			case ExpressionType.PreIncrementAssign:
			case ExpressionType.PreDecrementAssign:
			case ExpressionType.OnesComplement:
			case ExpressionType.IsTrue:
			case ExpressionType.IsFalse:
				return 12;
			case ExpressionType.Divide:
			case ExpressionType.Modulo:
			case ExpressionType.Multiply:
			case ExpressionType.MultiplyChecked:
				return 11;
			case ExpressionType.Equal:
			case ExpressionType.NotEqual:
				return 7;
			case ExpressionType.ExclusiveOr:
				return 5;
			case ExpressionType.GreaterThan:
			case ExpressionType.GreaterThanOrEqual:
			case ExpressionType.LessThan:
			case ExpressionType.LessThanOrEqual:
			case ExpressionType.TypeAs:
			case ExpressionType.TypeIs:
			case ExpressionType.TypeEqual:
				return 8;
			case ExpressionType.LeftShift:
			case ExpressionType.RightShift:
				return 9;
			case ExpressionType.Or:
				return 4;
			case ExpressionType.OrElse:
				return 2;
			case ExpressionType.Power:
				return 13;
			}
			return 14;
		}

		// Token: 0x06000F4C RID: 3916 RVA: 0x000306E8 File Offset: 0x0002E8E8
		private void ParenthesizedVisit(Expression parent, Expression nodeToVisit)
		{
			if (DebugViewWriter.NeedsParentheses(parent, nodeToVisit))
			{
				this.Out("(");
				this.Visit(nodeToVisit);
				this.Out(")");
				return;
			}
			this.Visit(nodeToVisit);
		}

		// Token: 0x06000F4D RID: 3917 RVA: 0x0003071C File Offset: 0x0002E91C
		protected internal override Expression VisitMethodCall(MethodCallExpression node)
		{
			this.Out(".Call ");
			if (node.Object != null)
			{
				this.ParenthesizedVisit(node, node.Object);
			}
			else if (node.Method.DeclaringType != null)
			{
				this.Out(node.Method.DeclaringType.ToString());
			}
			else
			{
				this.Out("<UnknownType>");
			}
			this.Out(".");
			this.Out(node.Method.Name);
			this.VisitExpressions<Expression>('(', node.Arguments);
			return node;
		}

		// Token: 0x06000F4E RID: 3918 RVA: 0x000307AC File Offset: 0x0002E9AC
		protected internal override Expression VisitNewArray(NewArrayExpression node)
		{
			if (node.NodeType == ExpressionType.NewArrayBounds)
			{
				this.Out(".NewArray " + node.Type.GetElementType().ToString());
				this.VisitExpressions<Expression>('[', node.Expressions);
			}
			else
			{
				this.Out(".NewArray " + node.Type.ToString(), DebugViewWriter.Flow.Space);
				this.VisitExpressions<Expression>('{', node.Expressions);
			}
			return node;
		}

		// Token: 0x06000F4F RID: 3919 RVA: 0x0003081E File Offset: 0x0002EA1E
		protected internal override Expression VisitNew(NewExpression node)
		{
			this.Out(".New " + node.Type.ToString());
			this.VisitExpressions<Expression>('(', node.Arguments);
			return node;
		}

		// Token: 0x06000F50 RID: 3920 RVA: 0x0003084A File Offset: 0x0002EA4A
		protected override ElementInit VisitElementInit(ElementInit node)
		{
			if (node.Arguments.Count == 1)
			{
				this.Visit(node.Arguments[0]);
			}
			else
			{
				this.VisitExpressions<Expression>('{', node.Arguments);
			}
			return node;
		}

		// Token: 0x06000F51 RID: 3921 RVA: 0x0003087E File Offset: 0x0002EA7E
		protected internal override Expression VisitListInit(ListInitExpression node)
		{
			this.Visit(node.NewExpression);
			this.VisitExpressions<ElementInit>('{', ',', node.Initializers, delegate(ElementInit e)
			{
				this.VisitElementInit(e);
			});
			return node;
		}

		// Token: 0x06000F52 RID: 3922 RVA: 0x000308AA File Offset: 0x0002EAAA
		protected override MemberAssignment VisitMemberAssignment(MemberAssignment assignment)
		{
			this.Out(assignment.Member.Name);
			this.Out(DebugViewWriter.Flow.Space, "=", DebugViewWriter.Flow.Space);
			this.Visit(assignment.Expression);
			return assignment;
		}

		// Token: 0x06000F53 RID: 3923 RVA: 0x000308D8 File Offset: 0x0002EAD8
		protected override MemberListBinding VisitMemberListBinding(MemberListBinding binding)
		{
			this.Out(binding.Member.Name);
			this.Out(DebugViewWriter.Flow.Space, "=", DebugViewWriter.Flow.Space);
			this.VisitExpressions<ElementInit>('{', ',', binding.Initializers, delegate(ElementInit e)
			{
				this.VisitElementInit(e);
			});
			return binding;
		}

		// Token: 0x06000F54 RID: 3924 RVA: 0x00030915 File Offset: 0x0002EB15
		protected override MemberMemberBinding VisitMemberMemberBinding(MemberMemberBinding binding)
		{
			this.Out(binding.Member.Name);
			this.Out(DebugViewWriter.Flow.Space, "=", DebugViewWriter.Flow.Space);
			this.VisitExpressions<MemberBinding>('{', ',', binding.Bindings, delegate(MemberBinding e)
			{
				this.VisitMemberBinding(e);
			});
			return binding;
		}

		// Token: 0x06000F55 RID: 3925 RVA: 0x00030952 File Offset: 0x0002EB52
		protected internal override Expression VisitMemberInit(MemberInitExpression node)
		{
			this.Visit(node.NewExpression);
			this.VisitExpressions<MemberBinding>('{', ',', node.Bindings, delegate(MemberBinding e)
			{
				this.VisitMemberBinding(e);
			});
			return node;
		}

		// Token: 0x06000F56 RID: 3926 RVA: 0x00030980 File Offset: 0x0002EB80
		protected internal override Expression VisitTypeBinary(TypeBinaryExpression node)
		{
			this.ParenthesizedVisit(node, node.Expression);
			ExpressionType nodeType = node.NodeType;
			if (nodeType != ExpressionType.TypeIs)
			{
				if (nodeType == ExpressionType.TypeEqual)
				{
					this.Out(DebugViewWriter.Flow.Space, ".TypeEqual", DebugViewWriter.Flow.Space);
				}
			}
			else
			{
				this.Out(DebugViewWriter.Flow.Space, ".Is", DebugViewWriter.Flow.Space);
			}
			this.Out(node.TypeOperand.ToString());
			return node;
		}

		// Token: 0x06000F57 RID: 3927 RVA: 0x000309DC File Offset: 0x0002EBDC
		protected internal override Expression VisitUnary(UnaryExpression node)
		{
			ExpressionType nodeType = node.NodeType;
			if (nodeType <= ExpressionType.Quote)
			{
				if (nodeType <= ExpressionType.Convert)
				{
					if (nodeType != ExpressionType.ArrayLength)
					{
						if (nodeType == ExpressionType.Convert)
						{
							this.Out("(" + node.Type.ToString() + ")");
						}
					}
				}
				else if (nodeType != ExpressionType.ConvertChecked)
				{
					switch (nodeType)
					{
					case ExpressionType.Negate:
						this.Out("-");
						break;
					case ExpressionType.UnaryPlus:
						this.Out("+");
						break;
					case ExpressionType.NegateChecked:
						this.Out("#-");
						break;
					case ExpressionType.New:
					case ExpressionType.NewArrayInit:
					case ExpressionType.NewArrayBounds:
						break;
					case ExpressionType.Not:
						this.Out((node.Type == typeof(bool)) ? "!" : "~");
						break;
					default:
						if (nodeType == ExpressionType.Quote)
						{
							this.Out("'");
						}
						break;
					}
				}
				else
				{
					this.Out("#(" + node.Type.ToString() + ")");
				}
			}
			else if (nodeType <= ExpressionType.Increment)
			{
				if (nodeType != ExpressionType.TypeAs)
				{
					if (nodeType != ExpressionType.Decrement)
					{
						if (nodeType == ExpressionType.Increment)
						{
							this.Out(".Increment");
						}
					}
					else
					{
						this.Out(".Decrement");
					}
				}
			}
			else if (nodeType != ExpressionType.Throw)
			{
				if (nodeType != ExpressionType.Unbox)
				{
					switch (nodeType)
					{
					case ExpressionType.PreIncrementAssign:
						this.Out("++");
						break;
					case ExpressionType.PreDecrementAssign:
						this.Out("--");
						break;
					case ExpressionType.OnesComplement:
						this.Out("~");
						break;
					case ExpressionType.IsTrue:
						this.Out(".IsTrue");
						break;
					case ExpressionType.IsFalse:
						this.Out(".IsFalse");
						break;
					}
				}
				else
				{
					this.Out(".Unbox");
				}
			}
			else if (node.Operand == null)
			{
				this.Out(".Rethrow");
			}
			else
			{
				this.Out(".Throw", DebugViewWriter.Flow.Space);
			}
			this.ParenthesizedVisit(node, node.Operand);
			nodeType = node.NodeType;
			if (nodeType <= ExpressionType.TypeAs)
			{
				if (nodeType != ExpressionType.ArrayLength)
				{
					if (nodeType == ExpressionType.TypeAs)
					{
						this.Out(DebugViewWriter.Flow.Space, ".As", DebugViewWriter.Flow.Space | DebugViewWriter.Flow.Break);
						this.Out(node.Type.ToString());
					}
				}
				else
				{
					this.Out(".Length");
				}
			}
			else if (nodeType != ExpressionType.PostIncrementAssign)
			{
				if (nodeType == ExpressionType.PostDecrementAssign)
				{
					this.Out("--");
				}
			}
			else
			{
				this.Out("++");
			}
			return node;
		}

		// Token: 0x06000F58 RID: 3928 RVA: 0x00030C64 File Offset: 0x0002EE64
		protected internal override Expression VisitBlock(BlockExpression node)
		{
			this.Out(".Block");
			if (node.Type != node.GetExpression(node.ExpressionCount - 1).Type)
			{
				this.Out(string.Format(CultureInfo.CurrentCulture, "<{0}>", node.Type.ToString()));
			}
			this.VisitDeclarations(node.Variables);
			this.Out(" ");
			this.VisitExpressions<Expression>('{', ';', node.Expressions);
			return node;
		}

		// Token: 0x06000F59 RID: 3929 RVA: 0x00030CE4 File Offset: 0x0002EEE4
		protected internal override Expression VisitDefault(DefaultExpression node)
		{
			this.Out(".Default(" + node.Type.ToString() + ")");
			return node;
		}

		// Token: 0x06000F5A RID: 3930 RVA: 0x00030D07 File Offset: 0x0002EF07
		protected internal override Expression VisitLabel(LabelExpression node)
		{
			this.Out(".Label", DebugViewWriter.Flow.NewLine);
			this.Indent();
			this.Visit(node.DefaultValue);
			this.Dedent();
			this.NewLine();
			this.DumpLabel(node.Target);
			return node;
		}

		// Token: 0x06000F5B RID: 3931 RVA: 0x00030D44 File Offset: 0x0002EF44
		protected internal override Expression VisitGoto(GotoExpression node)
		{
			this.Out("." + node.Kind.ToString(), DebugViewWriter.Flow.Space);
			this.Out(this.GetLabelTargetName(node.Target), DebugViewWriter.Flow.Space);
			this.Out("{", DebugViewWriter.Flow.Space);
			this.Visit(node.Value);
			this.Out(DebugViewWriter.Flow.Space, "}");
			return node;
		}

		// Token: 0x06000F5C RID: 3932 RVA: 0x00030DB0 File Offset: 0x0002EFB0
		protected internal override Expression VisitLoop(LoopExpression node)
		{
			this.Out(".Loop", DebugViewWriter.Flow.Space);
			if (node.ContinueLabel != null)
			{
				this.DumpLabel(node.ContinueLabel);
			}
			this.Out(" {", DebugViewWriter.Flow.NewLine);
			this.Indent();
			this.Visit(node.Body);
			this.Dedent();
			this.Out(DebugViewWriter.Flow.NewLine, "}");
			if (node.BreakLabel != null)
			{
				this.Out("", DebugViewWriter.Flow.NewLine);
				this.DumpLabel(node.BreakLabel);
			}
			return node;
		}

		// Token: 0x06000F5D RID: 3933 RVA: 0x00030E30 File Offset: 0x0002F030
		protected override SwitchCase VisitSwitchCase(SwitchCase node)
		{
			foreach (Expression node2 in node.TestValues)
			{
				this.Out(".Case (");
				this.Visit(node2);
				this.Out("):", DebugViewWriter.Flow.NewLine);
			}
			this.Indent();
			this.Indent();
			this.Visit(node.Body);
			this.Dedent();
			this.Dedent();
			this.NewLine();
			return node;
		}

		// Token: 0x06000F5E RID: 3934 RVA: 0x00030EC4 File Offset: 0x0002F0C4
		protected internal override Expression VisitSwitch(SwitchExpression node)
		{
			this.Out(".Switch ");
			this.Out("(");
			this.Visit(node.SwitchValue);
			this.Out(") {", DebugViewWriter.Flow.NewLine);
			ExpressionVisitor.Visit<SwitchCase>(node.Cases, new Func<SwitchCase, SwitchCase>(this.VisitSwitchCase));
			if (node.DefaultBody != null)
			{
				this.Out(".Default:", DebugViewWriter.Flow.NewLine);
				this.Indent();
				this.Indent();
				this.Visit(node.DefaultBody);
				this.Dedent();
				this.Dedent();
				this.NewLine();
			}
			this.Out("}");
			return node;
		}

		// Token: 0x06000F5F RID: 3935 RVA: 0x00030F64 File Offset: 0x0002F164
		protected override CatchBlock VisitCatchBlock(CatchBlock node)
		{
			this.Out(DebugViewWriter.Flow.NewLine, "} .Catch (" + node.Test.ToString());
			if (node.Variable != null)
			{
				this.Out(DebugViewWriter.Flow.Space, "");
				this.VisitParameter(node.Variable);
			}
			if (node.Filter != null)
			{
				this.Out(") .If (", DebugViewWriter.Flow.Break);
				this.Visit(node.Filter);
			}
			this.Out(") {", DebugViewWriter.Flow.NewLine);
			this.Indent();
			this.Visit(node.Body);
			this.Dedent();
			return node;
		}

		// Token: 0x06000F60 RID: 3936 RVA: 0x00030FFC File Offset: 0x0002F1FC
		protected internal override Expression VisitTry(TryExpression node)
		{
			this.Out(".Try {", DebugViewWriter.Flow.NewLine);
			this.Indent();
			this.Visit(node.Body);
			this.Dedent();
			ExpressionVisitor.Visit<CatchBlock>(node.Handlers, new Func<CatchBlock, CatchBlock>(this.VisitCatchBlock));
			if (node.Finally != null)
			{
				this.Out(DebugViewWriter.Flow.NewLine, "} .Finally {", DebugViewWriter.Flow.NewLine);
				this.Indent();
				this.Visit(node.Finally);
				this.Dedent();
			}
			else if (node.Fault != null)
			{
				this.Out(DebugViewWriter.Flow.NewLine, "} .Fault {", DebugViewWriter.Flow.NewLine);
				this.Indent();
				this.Visit(node.Fault);
				this.Dedent();
			}
			this.Out(DebugViewWriter.Flow.NewLine, "}");
			return node;
		}

		// Token: 0x06000F61 RID: 3937 RVA: 0x000310B4 File Offset: 0x0002F2B4
		protected internal override Expression VisitIndex(IndexExpression node)
		{
			if (node.Indexer != null)
			{
				this.OutMember(node, node.Object, node.Indexer);
			}
			else
			{
				this.ParenthesizedVisit(node, node.Object);
			}
			this.VisitExpressions<Expression>('[', node.Arguments);
			return node;
		}

		// Token: 0x06000F62 RID: 3938 RVA: 0x00031100 File Offset: 0x0002F300
		protected internal override Expression VisitExtension(Expression node)
		{
			this.Out(string.Format(CultureInfo.CurrentCulture, ".Extension<{0}>", node.GetType().ToString()));
			if (node.CanReduce)
			{
				this.Out(DebugViewWriter.Flow.Space, "{", DebugViewWriter.Flow.NewLine);
				this.Indent();
				this.Visit(node.Reduce());
				this.Dedent();
				this.Out(DebugViewWriter.Flow.NewLine, "}");
			}
			return node;
		}

		// Token: 0x06000F63 RID: 3939 RVA: 0x00031168 File Offset: 0x0002F368
		protected internal override Expression VisitDebugInfo(DebugInfoExpression node)
		{
			this.Out(string.Format(CultureInfo.CurrentCulture, ".DebugInfo({0}: {1}, {2} - {3}, {4})", new object[]
			{
				node.Document.FileName,
				node.StartLine,
				node.StartColumn,
				node.EndLine,
				node.EndColumn
			}));
			return node;
		}

		// Token: 0x06000F64 RID: 3940 RVA: 0x000311D7 File Offset: 0x0002F3D7
		private void DumpLabel(LabelTarget target)
		{
			this.Out(string.Format(CultureInfo.CurrentCulture, ".LabelTarget {0}:", this.GetLabelTargetName(target)));
		}

		// Token: 0x06000F65 RID: 3941 RVA: 0x000311F5 File Offset: 0x0002F3F5
		private string GetLabelTargetName(LabelTarget target)
		{
			if (string.IsNullOrEmpty(target.Name))
			{
				return "#Label" + this.GetLabelTargetId(target);
			}
			return DebugViewWriter.GetDisplayName(target.Name);
		}

		// Token: 0x06000F66 RID: 3942 RVA: 0x00031228 File Offset: 0x0002F428
		private void WriteLambda(LambdaExpression lambda)
		{
			this.Out(string.Format(CultureInfo.CurrentCulture, ".Lambda {0}<{1}>", this.GetLambdaName(lambda), lambda.Type.ToString()));
			this.VisitDeclarations(lambda.Parameters);
			this.Out(DebugViewWriter.Flow.Space, "{", DebugViewWriter.Flow.NewLine);
			this.Indent();
			this.Visit(lambda.Body);
			this.Dedent();
			this.Out(DebugViewWriter.Flow.NewLine, "}");
		}

		// Token: 0x06000F67 RID: 3943 RVA: 0x0003129A File Offset: 0x0002F49A
		private string GetLambdaName(LambdaExpression lambda)
		{
			if (string.IsNullOrEmpty(lambda.Name))
			{
				return "#Lambda" + this.GetLambdaId(lambda);
			}
			return DebugViewWriter.GetDisplayName(lambda.Name);
		}

		// Token: 0x06000F68 RID: 3944 RVA: 0x000312CC File Offset: 0x0002F4CC
		private static bool ContainsWhiteSpace(string name)
		{
			for (int i = 0; i < name.Length; i++)
			{
				if (char.IsWhiteSpace(name[i]))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000F69 RID: 3945 RVA: 0x000312FD File Offset: 0x0002F4FD
		private static string QuoteName(string name)
		{
			return string.Format(CultureInfo.CurrentCulture, "'{0}'", name);
		}

		// Token: 0x06000F6A RID: 3946 RVA: 0x0003130F File Offset: 0x0002F50F
		private static string GetDisplayName(string name)
		{
			if (DebugViewWriter.ContainsWhiteSpace(name))
			{
				return DebugViewWriter.QuoteName(name);
			}
			return name;
		}

		// Token: 0x06000F6B RID: 3947 RVA: 0x00031321 File Offset: 0x0002F521
		[CompilerGenerated]
		private void <VisitExpressions>b__37_0<T>(T e) where T : Expression
		{
			this.Visit(e);
		}

		// Token: 0x06000F6C RID: 3948 RVA: 0x00031330 File Offset: 0x0002F530
		[CompilerGenerated]
		private void <VisitDeclarations>b__38_0(ParameterExpression variable)
		{
			this.Out(variable.Type.ToString());
			if (variable.IsByRef)
			{
				this.Out("&");
			}
			this.Out(" ");
			this.VisitParameter(variable);
		}

		// Token: 0x06000F6D RID: 3949 RVA: 0x00031369 File Offset: 0x0002F569
		[CompilerGenerated]
		private void <VisitListInit>b__58_0(ElementInit e)
		{
			this.VisitElementInit(e);
		}

		// Token: 0x06000F6E RID: 3950 RVA: 0x00031369 File Offset: 0x0002F569
		[CompilerGenerated]
		private void <VisitMemberListBinding>b__60_0(ElementInit e)
		{
			this.VisitElementInit(e);
		}

		// Token: 0x06000F6F RID: 3951 RVA: 0x00031373 File Offset: 0x0002F573
		[CompilerGenerated]
		private void <VisitMemberMemberBinding>b__61_0(MemberBinding e)
		{
			this.VisitMemberBinding(e);
		}

		// Token: 0x06000F70 RID: 3952 RVA: 0x00031373 File Offset: 0x0002F573
		[CompilerGenerated]
		private void <VisitMemberInit>b__62_0(MemberBinding e)
		{
			this.VisitMemberBinding(e);
		}

		// Token: 0x040008AE RID: 2222
		private const int Tab = 4;

		// Token: 0x040008AF RID: 2223
		private const int MaxColumn = 120;

		// Token: 0x040008B0 RID: 2224
		private readonly TextWriter _out;

		// Token: 0x040008B1 RID: 2225
		private int _column;

		// Token: 0x040008B2 RID: 2226
		private readonly Stack<int> _stack = new Stack<int>();

		// Token: 0x040008B3 RID: 2227
		private int _delta;

		// Token: 0x040008B4 RID: 2228
		private DebugViewWriter.Flow _flow;

		// Token: 0x040008B5 RID: 2229
		private Queue<LambdaExpression> _lambdas;

		// Token: 0x040008B6 RID: 2230
		private Dictionary<LambdaExpression, int> _lambdaIds;

		// Token: 0x040008B7 RID: 2231
		private Dictionary<ParameterExpression, int> _paramIds;

		// Token: 0x040008B8 RID: 2232
		private Dictionary<LabelTarget, int> _labelIds;

		// Token: 0x0200022E RID: 558
		[Flags]
		private enum Flow
		{
			// Token: 0x040008BA RID: 2234
			None = 0,
			// Token: 0x040008BB RID: 2235
			Space = 1,
			// Token: 0x040008BC RID: 2236
			NewLine = 2,
			// Token: 0x040008BD RID: 2237
			Break = 32768
		}
	}
}
