﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;
using System.Reflection;

namespace System.Linq.Expressions
{
	// Token: 0x02000269 RID: 617
	internal sealed class InstanceMethodCallExpressionN : InstanceMethodCallExpression, IArgumentProvider
	{
		// Token: 0x060011EE RID: 4590 RVA: 0x00035970 File Offset: 0x00033B70
		public InstanceMethodCallExpressionN(MethodInfo method, Expression instance, IReadOnlyList<Expression> args) : base(method, instance)
		{
			this._arguments = args;
		}

		// Token: 0x060011EF RID: 4591 RVA: 0x00035981 File Offset: 0x00033B81
		public override Expression GetArgument(int index)
		{
			return this._arguments[index];
		}

		// Token: 0x1700033D RID: 829
		// (get) Token: 0x060011F0 RID: 4592 RVA: 0x0003598F File Offset: 0x00033B8F
		public override int ArgumentCount
		{
			get
			{
				return this._arguments.Count;
			}
		}

		// Token: 0x060011F1 RID: 4593 RVA: 0x0003599C File Offset: 0x00033B9C
		internal override bool SameArguments(ICollection<Expression> arguments)
		{
			return ExpressionUtils.SameElements<Expression>(arguments, this._arguments);
		}

		// Token: 0x060011F2 RID: 4594 RVA: 0x000359AA File Offset: 0x00033BAA
		internal override ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			return ExpressionUtils.ReturnReadOnly<Expression>(ref this._arguments);
		}

		// Token: 0x060011F3 RID: 4595 RVA: 0x000359B7 File Offset: 0x00033BB7
		internal override MethodCallExpression Rewrite(Expression instance, IReadOnlyList<Expression> args)
		{
			return Expression.Call(instance, base.Method, args ?? this._arguments);
		}

		// Token: 0x0400096D RID: 2413
		private IReadOnlyList<Expression> _arguments;
	}
}
