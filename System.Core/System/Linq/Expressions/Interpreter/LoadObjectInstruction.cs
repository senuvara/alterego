﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020003E1 RID: 993
	internal sealed class LoadObjectInstruction : Instruction
	{
		// Token: 0x06001891 RID: 6289 RVA: 0x00048DA6 File Offset: 0x00046FA6
		internal LoadObjectInstruction(object value)
		{
			this._value = value;
		}

		// Token: 0x170004BE RID: 1214
		// (get) Token: 0x06001892 RID: 6290 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004BF RID: 1215
		// (get) Token: 0x06001893 RID: 6291 RVA: 0x00048DB5 File Offset: 0x00046FB5
		public override string InstructionName
		{
			get
			{
				return "LoadObject";
			}
		}

		// Token: 0x06001894 RID: 6292 RVA: 0x00048DBC File Offset: 0x00046FBC
		public override int Run(InterpretedFrame frame)
		{
			object[] data = frame.Data;
			int stackIndex = frame.StackIndex;
			frame.StackIndex = stackIndex + 1;
			data[stackIndex] = this._value;
			return 1;
		}

		// Token: 0x06001895 RID: 6293 RVA: 0x00048DE8 File Offset: 0x00046FE8
		public override string ToString()
		{
			return "LoadObject(" + (this._value ?? "null") + ")";
		}

		// Token: 0x04000BC6 RID: 3014
		private readonly object _value;
	}
}
