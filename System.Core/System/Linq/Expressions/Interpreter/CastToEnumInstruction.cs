﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000406 RID: 1030
	internal sealed class CastToEnumInstruction : CastInstruction
	{
		// Token: 0x06001908 RID: 6408 RVA: 0x00049B82 File Offset: 0x00047D82
		public CastToEnumInstruction(Type t)
		{
			this._t = t;
		}

		// Token: 0x06001909 RID: 6409 RVA: 0x00049B94 File Offset: 0x00047D94
		public override int Run(InterpretedFrame frame)
		{
			object obj = frame.Pop();
			frame.Push((obj == null) ? null : Enum.ToObject(this._t, obj));
			return 1;
		}

		// Token: 0x04000BF3 RID: 3059
		private readonly Type _t;
	}
}
