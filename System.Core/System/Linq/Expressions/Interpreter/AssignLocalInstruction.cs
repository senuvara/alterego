﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200036C RID: 876
	internal sealed class AssignLocalInstruction : LocalAccessInstruction, IBoxableInstruction
	{
		// Token: 0x06001743 RID: 5955 RVA: 0x000459CD File Offset: 0x00043BCD
		internal AssignLocalInstruction(int index) : base(index)
		{
		}

		// Token: 0x17000473 RID: 1139
		// (get) Token: 0x06001744 RID: 5956 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000474 RID: 1140
		// (get) Token: 0x06001745 RID: 5957 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000475 RID: 1141
		// (get) Token: 0x06001746 RID: 5958 RVA: 0x00045AE9 File Offset: 0x00043CE9
		public override string InstructionName
		{
			get
			{
				return "AssignLocal";
			}
		}

		// Token: 0x06001747 RID: 5959 RVA: 0x00045AF0 File Offset: 0x00043CF0
		public override int Run(InterpretedFrame frame)
		{
			frame.Data[this._index] = frame.Peek();
			return 1;
		}

		// Token: 0x06001748 RID: 5960 RVA: 0x00045B06 File Offset: 0x00043D06
		public Instruction BoxIfIndexMatches(int index)
		{
			if (index != this._index)
			{
				return null;
			}
			return InstructionList.AssignLocalBoxed(index);
		}
	}
}
