﻿using System;
using System.Collections.Generic;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002B2 RID: 690
	internal abstract class OffsetInstruction : Instruction
	{
		// Token: 0x170003F0 RID: 1008
		// (get) Token: 0x060013DB RID: 5083
		public abstract Instruction[] Cache { get; }

		// Token: 0x060013DC RID: 5084 RVA: 0x00038F34 File Offset: 0x00037134
		public Instruction Fixup(int offset)
		{
			this._offset = offset;
			Instruction[] cache = this.Cache;
			if (cache != null && offset >= 0 && offset < cache.Length)
			{
				Instruction result;
				if ((result = cache[offset]) == null)
				{
					cache[offset] = this;
					result = this;
				}
				return result;
			}
			return this;
		}

		// Token: 0x060013DD RID: 5085 RVA: 0x00038F6C File Offset: 0x0003716C
		public override string ToDebugString(int instructionIndex, object cookie, Func<int, int> labelIndexer, IReadOnlyList<object> objects)
		{
			return this.ToString() + ((this._offset != int.MinValue) ? (" -> " + (instructionIndex + this._offset)) : "");
		}

		// Token: 0x060013DE RID: 5086 RVA: 0x00038FA4 File Offset: 0x000371A4
		public override string ToString()
		{
			return this.InstructionName + ((this._offset == int.MinValue) ? "(?)" : ("(" + this._offset + ")"));
		}

		// Token: 0x060013DF RID: 5087 RVA: 0x00038FDF File Offset: 0x000371DF
		protected OffsetInstruction()
		{
		}

		// Token: 0x040009F6 RID: 2550
		internal const int Unknown = -2147483648;

		// Token: 0x040009F7 RID: 2551
		internal const int CacheSize = 32;

		// Token: 0x040009F8 RID: 2552
		protected int _offset = int.MinValue;
	}
}
