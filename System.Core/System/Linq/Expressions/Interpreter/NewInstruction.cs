﻿using System;
using System.Dynamic.Utils;
using System.Reflection;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020003A3 RID: 931
	internal class NewInstruction : Instruction
	{
		// Token: 0x060017EC RID: 6124 RVA: 0x00046ED5 File Offset: 0x000450D5
		public NewInstruction(ConstructorInfo constructor, int argumentCount)
		{
			this._constructor = constructor;
			this._argumentCount = argumentCount;
		}

		// Token: 0x170004A3 RID: 1187
		// (get) Token: 0x060017ED RID: 6125 RVA: 0x00046EEB File Offset: 0x000450EB
		public override int ConsumedStack
		{
			get
			{
				return this._argumentCount;
			}
		}

		// Token: 0x170004A4 RID: 1188
		// (get) Token: 0x060017EE RID: 6126 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004A5 RID: 1189
		// (get) Token: 0x060017EF RID: 6127 RVA: 0x00046EF3 File Offset: 0x000450F3
		public override string InstructionName
		{
			get
			{
				return "New";
			}
		}

		// Token: 0x060017F0 RID: 6128 RVA: 0x00046EFC File Offset: 0x000450FC
		public override int Run(InterpretedFrame frame)
		{
			int num = frame.StackIndex - this._argumentCount;
			object[] args = this.GetArgs(frame, num);
			object obj;
			try
			{
				obj = this._constructor.Invoke(args);
			}
			catch (TargetInvocationException exception)
			{
				ExceptionHelpers.UnwrapAndRethrow(exception);
				throw ContractUtils.Unreachable;
			}
			frame.Data[num] = obj;
			frame.StackIndex = num + 1;
			return 1;
		}

		// Token: 0x060017F1 RID: 6129 RVA: 0x00046F60 File Offset: 0x00045160
		protected object[] GetArgs(InterpretedFrame frame, int first)
		{
			if (this._argumentCount > 0)
			{
				object[] array = new object[this._argumentCount];
				for (int i = 0; i < array.Length; i++)
				{
					array[i] = frame.Data[first + i];
				}
				return array;
			}
			return Array.Empty<object>();
		}

		// Token: 0x060017F2 RID: 6130 RVA: 0x00046FA4 File Offset: 0x000451A4
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				"New ",
				this._constructor.DeclaringType.Name,
				"(",
				this._constructor,
				")"
			});
		}

		// Token: 0x04000B8C RID: 2956
		protected readonly ConstructorInfo _constructor;

		// Token: 0x04000B8D RID: 2957
		protected readonly int _argumentCount;
	}
}
