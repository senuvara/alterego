﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020003F6 RID: 1014
	internal sealed class TypeIsInstruction : Instruction
	{
		// Token: 0x060018D1 RID: 6353 RVA: 0x000495C1 File Offset: 0x000477C1
		internal TypeIsInstruction(Type type)
		{
			this._type = type;
		}

		// Token: 0x170004CF RID: 1231
		// (get) Token: 0x060018D2 RID: 6354 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004D0 RID: 1232
		// (get) Token: 0x060018D3 RID: 6355 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004D1 RID: 1233
		// (get) Token: 0x060018D4 RID: 6356 RVA: 0x000495D0 File Offset: 0x000477D0
		public override string InstructionName
		{
			get
			{
				return "TypeIs";
			}
		}

		// Token: 0x060018D5 RID: 6357 RVA: 0x000495D7 File Offset: 0x000477D7
		public override int Run(InterpretedFrame frame)
		{
			frame.Push(this._type.IsInstanceOfType(frame.Pop()));
			return 1;
		}

		// Token: 0x060018D6 RID: 6358 RVA: 0x000495F1 File Offset: 0x000477F1
		public override string ToString()
		{
			return "TypeIs " + this._type.ToString();
		}

		// Token: 0x04000BD9 RID: 3033
		private readonly Type _type;
	}
}
