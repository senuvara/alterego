﻿using System;
using System.Dynamic.Utils;
using System.Reflection;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020003A4 RID: 932
	internal class ByRefNewInstruction : NewInstruction
	{
		// Token: 0x060017F3 RID: 6131 RVA: 0x00046FF0 File Offset: 0x000451F0
		internal ByRefNewInstruction(ConstructorInfo target, int argumentCount, ByRefUpdater[] byrefArgs) : base(target, argumentCount)
		{
			this._byrefArgs = byrefArgs;
		}

		// Token: 0x170004A6 RID: 1190
		// (get) Token: 0x060017F4 RID: 6132 RVA: 0x00047001 File Offset: 0x00045201
		public override string InstructionName
		{
			get
			{
				return "ByRefNew";
			}
		}

		// Token: 0x060017F5 RID: 6133 RVA: 0x00047008 File Offset: 0x00045208
		public sealed override int Run(InterpretedFrame frame)
		{
			int num = frame.StackIndex - this._argumentCount;
			object[] args = base.GetArgs(frame, num);
			try
			{
				object obj;
				try
				{
					obj = this._constructor.Invoke(args);
				}
				catch (TargetInvocationException exception)
				{
					ExceptionHelpers.UnwrapAndRethrow(exception);
					throw ContractUtils.Unreachable;
				}
				frame.Data[num] = obj;
				frame.StackIndex = num + 1;
			}
			finally
			{
				foreach (ByRefUpdater byRefUpdater in this._byrefArgs)
				{
					byRefUpdater.Update(frame, args[byRefUpdater.ArgumentIndex]);
				}
			}
			return 1;
		}

		// Token: 0x04000B8E RID: 2958
		private readonly ByRefUpdater[] _byrefArgs;
	}
}
