﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000407 RID: 1031
	internal sealed class CastReferenceToEnumInstruction : CastInstruction
	{
		// Token: 0x0600190A RID: 6410 RVA: 0x00049BC1 File Offset: 0x00047DC1
		public CastReferenceToEnumInstruction(Type t)
		{
			this._t = t;
		}

		// Token: 0x0600190B RID: 6411 RVA: 0x00049BD0 File Offset: 0x00047DD0
		public override int Run(InterpretedFrame frame)
		{
			object obj = frame.Pop();
			switch (this._t.GetTypeCode())
			{
			case TypeCode.Char:
				frame.Push(Enum.ToObject(this._t, (ushort)((char)obj)));
				break;
			case TypeCode.SByte:
				frame.Push(Enum.ToObject(this._t, (sbyte)obj));
				break;
			case TypeCode.Byte:
				frame.Push(Enum.ToObject(this._t, (byte)obj));
				break;
			case TypeCode.Int16:
				frame.Push(Enum.ToObject(this._t, (short)obj));
				break;
			case TypeCode.UInt16:
				frame.Push(Enum.ToObject(this._t, (ushort)obj));
				break;
			case TypeCode.Int32:
				frame.Push(Enum.ToObject(this._t, (int)obj));
				break;
			case TypeCode.UInt32:
				frame.Push(Enum.ToObject(this._t, (uint)obj));
				break;
			case TypeCode.Int64:
				frame.Push(Enum.ToObject(this._t, (long)obj));
				break;
			case TypeCode.UInt64:
				frame.Push(Enum.ToObject(this._t, (ulong)obj));
				break;
			default:
				frame.Push(Enum.ToObject(this._t, (bool)obj));
				break;
			}
			return 1;
		}

		// Token: 0x04000BF4 RID: 3060
		private readonly Type _t;
	}
}
