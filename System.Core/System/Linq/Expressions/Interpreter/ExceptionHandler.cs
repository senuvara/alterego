﻿using System;
using System.Globalization;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000352 RID: 850
	internal sealed class ExceptionHandler
	{
		// Token: 0x06001648 RID: 5704 RVA: 0x0003E828 File Offset: 0x0003CA28
		internal ExceptionHandler(int labelIndex, int handlerStartIndex, int handlerEndIndex, Type exceptionType, ExceptionFilter filter)
		{
			this.LabelIndex = labelIndex;
			this._exceptionType = exceptionType;
			this.HandlerStartIndex = handlerStartIndex;
			this.HandlerEndIndex = handlerEndIndex;
			this.Filter = filter;
		}

		// Token: 0x06001649 RID: 5705 RVA: 0x0003E855 File Offset: 0x0003CA55
		public bool Matches(Type exceptionType)
		{
			return this._exceptionType.IsAssignableFrom(exceptionType);
		}

		// Token: 0x0600164A RID: 5706 RVA: 0x0003E863 File Offset: 0x0003CA63
		public override string ToString()
		{
			return string.Format(CultureInfo.InvariantCulture, "catch({0}) [{1}->{2}]", this._exceptionType.Name, this.HandlerStartIndex, this.HandlerEndIndex);
		}

		// Token: 0x04000B19 RID: 2841
		private readonly Type _exceptionType;

		// Token: 0x04000B1A RID: 2842
		public readonly int LabelIndex;

		// Token: 0x04000B1B RID: 2843
		public readonly int HandlerStartIndex;

		// Token: 0x04000B1C RID: 2844
		public readonly int HandlerEndIndex;

		// Token: 0x04000B1D RID: 2845
		public readonly ExceptionFilter Filter;
	}
}
