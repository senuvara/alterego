﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200036D RID: 877
	internal sealed class StoreLocalInstruction : LocalAccessInstruction, IBoxableInstruction
	{
		// Token: 0x06001749 RID: 5961 RVA: 0x000459CD File Offset: 0x00043BCD
		internal StoreLocalInstruction(int index) : base(index)
		{
		}

		// Token: 0x17000476 RID: 1142
		// (get) Token: 0x0600174A RID: 5962 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000477 RID: 1143
		// (get) Token: 0x0600174B RID: 5963 RVA: 0x00045B19 File Offset: 0x00043D19
		public override string InstructionName
		{
			get
			{
				return "StoreLocal";
			}
		}

		// Token: 0x0600174C RID: 5964 RVA: 0x00045B20 File Offset: 0x00043D20
		public override int Run(InterpretedFrame frame)
		{
			frame.Data[this._index] = frame.Pop();
			return 1;
		}

		// Token: 0x0600174D RID: 5965 RVA: 0x00045B36 File Offset: 0x00043D36
		public Instruction BoxIfIndexMatches(int index)
		{
			if (index != this._index)
			{
				return null;
			}
			return InstructionList.StoreLocalBoxed(index);
		}
	}
}
