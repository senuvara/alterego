﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020003E2 RID: 994
	internal sealed class LoadCachedObjectInstruction : Instruction
	{
		// Token: 0x06001896 RID: 6294 RVA: 0x00048E08 File Offset: 0x00047008
		internal LoadCachedObjectInstruction(uint index)
		{
			this._index = index;
		}

		// Token: 0x170004C0 RID: 1216
		// (get) Token: 0x06001897 RID: 6295 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004C1 RID: 1217
		// (get) Token: 0x06001898 RID: 6296 RVA: 0x00048E17 File Offset: 0x00047017
		public override string InstructionName
		{
			get
			{
				return "LoadCachedObject";
			}
		}

		// Token: 0x06001899 RID: 6297 RVA: 0x00048E20 File Offset: 0x00047020
		public override int Run(InterpretedFrame frame)
		{
			object[] data = frame.Data;
			int stackIndex = frame.StackIndex;
			frame.StackIndex = stackIndex + 1;
			data[stackIndex] = frame.Interpreter._objects[(int)this._index];
			return 1;
		}

		// Token: 0x0600189A RID: 6298 RVA: 0x00048E58 File Offset: 0x00047058
		public override string ToDebugString(int instructionIndex, object cookie, Func<int, int> labelIndexer, IReadOnlyList<object> objects)
		{
			return string.Format(CultureInfo.InvariantCulture, "LoadCached({0}: {1})", this._index, objects[(int)this._index]);
		}

		// Token: 0x0600189B RID: 6299 RVA: 0x00048E81 File Offset: 0x00047081
		public override string ToString()
		{
			return "LoadCached(" + this._index + ")";
		}

		// Token: 0x04000BC7 RID: 3015
		private readonly uint _index;
	}
}
