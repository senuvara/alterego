﻿using System;
using System.Collections.Generic;
using System.Dynamic.Utils;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000408 RID: 1032
	internal sealed class QuoteInstruction : Instruction
	{
		// Token: 0x0600190C RID: 6412 RVA: 0x00049D2E File Offset: 0x00047F2E
		public QuoteInstruction(Expression operand, Dictionary<ParameterExpression, LocalVariable> hoistedVariables)
		{
			this._operand = operand;
			this._hoistedVariables = hoistedVariables;
		}

		// Token: 0x170004E0 RID: 1248
		// (get) Token: 0x0600190D RID: 6413 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004E1 RID: 1249
		// (get) Token: 0x0600190E RID: 6414 RVA: 0x00049D44 File Offset: 0x00047F44
		public override string InstructionName
		{
			get
			{
				return "Quote";
			}
		}

		// Token: 0x0600190F RID: 6415 RVA: 0x00049D4C File Offset: 0x00047F4C
		public override int Run(InterpretedFrame frame)
		{
			Expression expression = this._operand;
			if (this._hoistedVariables != null)
			{
				expression = new QuoteInstruction.ExpressionQuoter(this._hoistedVariables, frame).Visit(expression);
			}
			frame.Push(expression);
			return 1;
		}

		// Token: 0x04000BF5 RID: 3061
		private readonly Expression _operand;

		// Token: 0x04000BF6 RID: 3062
		private readonly Dictionary<ParameterExpression, LocalVariable> _hoistedVariables;

		// Token: 0x02000409 RID: 1033
		private sealed class ExpressionQuoter : ExpressionVisitor
		{
			// Token: 0x06001910 RID: 6416 RVA: 0x00049D83 File Offset: 0x00047F83
			internal ExpressionQuoter(Dictionary<ParameterExpression, LocalVariable> hoistedVariables, InterpretedFrame frame)
			{
				this._variables = hoistedVariables;
				this._frame = frame;
			}

			// Token: 0x06001911 RID: 6417 RVA: 0x00049DA4 File Offset: 0x00047FA4
			protected internal override Expression VisitLambda<T>(Expression<T> node)
			{
				if (node.ParameterCount > 0)
				{
					HashSet<ParameterExpression> hashSet = new HashSet<ParameterExpression>();
					int i = 0;
					int parameterCount = node.ParameterCount;
					while (i < parameterCount)
					{
						hashSet.Add(node.GetParameter(i));
						i++;
					}
					this._shadowedVars.Push(hashSet);
				}
				Expression expression = this.Visit(node.Body);
				if (node.ParameterCount > 0)
				{
					this._shadowedVars.Pop();
				}
				if (expression == node.Body)
				{
					return node;
				}
				return node.Rewrite(expression, null);
			}

			// Token: 0x06001912 RID: 6418 RVA: 0x00049E24 File Offset: 0x00048024
			protected internal override Expression VisitBlock(BlockExpression node)
			{
				if (node.Variables.Count > 0)
				{
					this._shadowedVars.Push(new HashSet<ParameterExpression>(node.Variables));
				}
				Expression[] array = ExpressionVisitorUtils.VisitBlockExpressions(this, node);
				if (node.Variables.Count > 0)
				{
					this._shadowedVars.Pop();
				}
				if (array == null)
				{
					return node;
				}
				return node.Rewrite(node.Variables, array);
			}

			// Token: 0x06001913 RID: 6419 RVA: 0x00049E8C File Offset: 0x0004808C
			protected override CatchBlock VisitCatchBlock(CatchBlock node)
			{
				if (node.Variable != null)
				{
					this._shadowedVars.Push(new HashSet<ParameterExpression>
					{
						node.Variable
					});
				}
				Expression expression = this.Visit(node.Body);
				Expression expression2 = this.Visit(node.Filter);
				if (node.Variable != null)
				{
					this._shadowedVars.Pop();
				}
				if (expression == node.Body && expression2 == node.Filter)
				{
					return node;
				}
				return Expression.MakeCatchBlock(node.Test, node.Variable, expression, expression2);
			}

			// Token: 0x06001914 RID: 6420 RVA: 0x00049F14 File Offset: 0x00048114
			protected internal override Expression VisitRuntimeVariables(RuntimeVariablesExpression node)
			{
				int count = node.Variables.Count;
				List<IStrongBox> list = new List<IStrongBox>();
				List<ParameterExpression> list2 = new List<ParameterExpression>();
				int[] array = new int[count];
				for (int i = 0; i < array.Length; i++)
				{
					IStrongBox box = this.GetBox(node.Variables[i]);
					if (box == null)
					{
						array[i] = list2.Count;
						list2.Add(node.Variables[i]);
					}
					else
					{
						array[i] = -1 - list.Count;
						list.Add(box);
					}
				}
				if (list.Count == 0)
				{
					return node;
				}
				ConstantExpression constantExpression = Expression.Constant(new RuntimeOps.RuntimeVariables(list.ToArray()), typeof(IRuntimeVariables));
				if (list2.Count == 0)
				{
					return constantExpression;
				}
				return Expression.Invoke(Expression.Constant(new Func<IRuntimeVariables, IRuntimeVariables, int[], IRuntimeVariables>(QuoteInstruction.ExpressionQuoter.MergeRuntimeVariables)), Expression.RuntimeVariables(new TrueReadOnlyCollection<ParameterExpression>(list2.ToArray())), constantExpression, Expression.Constant(array));
			}

			// Token: 0x06001915 RID: 6421 RVA: 0x00049FF8 File Offset: 0x000481F8
			private static IRuntimeVariables MergeRuntimeVariables(IRuntimeVariables first, IRuntimeVariables second, int[] indexes)
			{
				return new RuntimeOps.MergedRuntimeVariables(first, second, indexes);
			}

			// Token: 0x06001916 RID: 6422 RVA: 0x0004A004 File Offset: 0x00048204
			protected internal override Expression VisitParameter(ParameterExpression node)
			{
				IStrongBox box = this.GetBox(node);
				if (box == null)
				{
					return node;
				}
				return Expression.Convert(Expression.Field(Expression.Constant(box), "Value"), node.Type);
			}

			// Token: 0x06001917 RID: 6423 RVA: 0x0004A03C File Offset: 0x0004823C
			private IStrongBox GetBox(ParameterExpression variable)
			{
				LocalVariable localVariable;
				if (!this._variables.TryGetValue(variable, out localVariable))
				{
					return null;
				}
				if (localVariable.InClosure)
				{
					return this._frame.Closure[localVariable.Index];
				}
				return (IStrongBox)this._frame.Data[localVariable.Index];
			}

			// Token: 0x04000BF7 RID: 3063
			private readonly Dictionary<ParameterExpression, LocalVariable> _variables;

			// Token: 0x04000BF8 RID: 3064
			private readonly InterpretedFrame _frame;

			// Token: 0x04000BF9 RID: 3065
			private readonly Stack<HashSet<ParameterExpression>> _shadowedVars = new Stack<HashSet<ParameterExpression>>();
		}
	}
}
