﻿using System;
using System.Dynamic.Utils;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000372 RID: 882
	internal abstract class InitializeLocalInstruction : LocalAccessInstruction
	{
		// Token: 0x06001762 RID: 5986 RVA: 0x000459CD File Offset: 0x00043BCD
		internal InitializeLocalInstruction(int index) : base(index)
		{
		}

		// Token: 0x02000373 RID: 883
		internal sealed class Reference : InitializeLocalInstruction, IBoxableInstruction
		{
			// Token: 0x06001763 RID: 5987 RVA: 0x00045C13 File Offset: 0x00043E13
			internal Reference(int index) : base(index)
			{
			}

			// Token: 0x06001764 RID: 5988 RVA: 0x00045C1C File Offset: 0x00043E1C
			public override int Run(InterpretedFrame frame)
			{
				frame.Data[this._index] = null;
				return 1;
			}

			// Token: 0x06001765 RID: 5989 RVA: 0x00045C2D File Offset: 0x00043E2D
			public Instruction BoxIfIndexMatches(int index)
			{
				if (index != this._index)
				{
					return null;
				}
				return InstructionList.InitImmutableRefBox(index);
			}

			// Token: 0x17000483 RID: 1155
			// (get) Token: 0x06001766 RID: 5990 RVA: 0x00045C40 File Offset: 0x00043E40
			public override string InstructionName
			{
				get
				{
					return "InitRef";
				}
			}
		}

		// Token: 0x02000374 RID: 884
		internal sealed class ImmutableValue : InitializeLocalInstruction, IBoxableInstruction
		{
			// Token: 0x06001767 RID: 5991 RVA: 0x00045C47 File Offset: 0x00043E47
			internal ImmutableValue(int index, object defaultValue) : base(index)
			{
				this._defaultValue = defaultValue;
			}

			// Token: 0x06001768 RID: 5992 RVA: 0x00045C57 File Offset: 0x00043E57
			public override int Run(InterpretedFrame frame)
			{
				frame.Data[this._index] = this._defaultValue;
				return 1;
			}

			// Token: 0x06001769 RID: 5993 RVA: 0x00045C6D File Offset: 0x00043E6D
			public Instruction BoxIfIndexMatches(int index)
			{
				if (index != this._index)
				{
					return null;
				}
				return new InitializeLocalInstruction.ImmutableBox(index, this._defaultValue);
			}

			// Token: 0x17000484 RID: 1156
			// (get) Token: 0x0600176A RID: 5994 RVA: 0x00045C86 File Offset: 0x00043E86
			public override string InstructionName
			{
				get
				{
					return "InitImmutableValue";
				}
			}

			// Token: 0x04000B5A RID: 2906
			private readonly object _defaultValue;
		}

		// Token: 0x02000375 RID: 885
		internal sealed class ImmutableBox : InitializeLocalInstruction
		{
			// Token: 0x0600176B RID: 5995 RVA: 0x00045C8D File Offset: 0x00043E8D
			internal ImmutableBox(int index, object defaultValue) : base(index)
			{
				this._defaultValue = defaultValue;
			}

			// Token: 0x0600176C RID: 5996 RVA: 0x00045C9D File Offset: 0x00043E9D
			public override int Run(InterpretedFrame frame)
			{
				frame.Data[this._index] = new StrongBox<object>(this._defaultValue);
				return 1;
			}

			// Token: 0x17000485 RID: 1157
			// (get) Token: 0x0600176D RID: 5997 RVA: 0x00045CB8 File Offset: 0x00043EB8
			public override string InstructionName
			{
				get
				{
					return "InitImmutableBox";
				}
			}

			// Token: 0x04000B5B RID: 2907
			private readonly object _defaultValue;
		}

		// Token: 0x02000376 RID: 886
		internal sealed class ImmutableRefBox : InitializeLocalInstruction
		{
			// Token: 0x0600176E RID: 5998 RVA: 0x00045C13 File Offset: 0x00043E13
			internal ImmutableRefBox(int index) : base(index)
			{
			}

			// Token: 0x0600176F RID: 5999 RVA: 0x00045CBF File Offset: 0x00043EBF
			public override int Run(InterpretedFrame frame)
			{
				frame.Data[this._index] = new StrongBox<object>();
				return 1;
			}

			// Token: 0x17000486 RID: 1158
			// (get) Token: 0x06001770 RID: 6000 RVA: 0x00045CB8 File Offset: 0x00043EB8
			public override string InstructionName
			{
				get
				{
					return "InitImmutableBox";
				}
			}
		}

		// Token: 0x02000377 RID: 887
		internal sealed class ParameterBox : InitializeLocalInstruction
		{
			// Token: 0x06001771 RID: 6001 RVA: 0x00045C13 File Offset: 0x00043E13
			public ParameterBox(int index) : base(index)
			{
			}

			// Token: 0x06001772 RID: 6002 RVA: 0x00045CD4 File Offset: 0x00043ED4
			public override int Run(InterpretedFrame frame)
			{
				frame.Data[this._index] = new StrongBox<object>(frame.Data[this._index]);
				return 1;
			}

			// Token: 0x17000487 RID: 1159
			// (get) Token: 0x06001773 RID: 6003 RVA: 0x00045CF6 File Offset: 0x00043EF6
			public override string InstructionName
			{
				get
				{
					return "InitParameterBox";
				}
			}
		}

		// Token: 0x02000378 RID: 888
		internal sealed class Parameter : InitializeLocalInstruction, IBoxableInstruction
		{
			// Token: 0x06001774 RID: 6004 RVA: 0x00045C13 File Offset: 0x00043E13
			internal Parameter(int index) : base(index)
			{
			}

			// Token: 0x06001775 RID: 6005 RVA: 0x00009CDF File Offset: 0x00007EDF
			public override int Run(InterpretedFrame frame)
			{
				return 1;
			}

			// Token: 0x06001776 RID: 6006 RVA: 0x00045CFD File Offset: 0x00043EFD
			public Instruction BoxIfIndexMatches(int index)
			{
				if (index == this._index)
				{
					return InstructionList.ParameterBox(index);
				}
				return null;
			}

			// Token: 0x17000488 RID: 1160
			// (get) Token: 0x06001777 RID: 6007 RVA: 0x00045D10 File Offset: 0x00043F10
			public override string InstructionName
			{
				get
				{
					return "InitParameter";
				}
			}
		}

		// Token: 0x02000379 RID: 889
		internal sealed class MutableValue : InitializeLocalInstruction, IBoxableInstruction
		{
			// Token: 0x06001778 RID: 6008 RVA: 0x00045D17 File Offset: 0x00043F17
			internal MutableValue(int index, Type type) : base(index)
			{
				this._type = type;
			}

			// Token: 0x06001779 RID: 6009 RVA: 0x00045D28 File Offset: 0x00043F28
			public override int Run(InterpretedFrame frame)
			{
				try
				{
					frame.Data[this._index] = Activator.CreateInstance(this._type);
				}
				catch (TargetInvocationException exception)
				{
					ExceptionHelpers.UnwrapAndRethrow(exception);
					throw ContractUtils.Unreachable;
				}
				return 1;
			}

			// Token: 0x0600177A RID: 6010 RVA: 0x00045D6C File Offset: 0x00043F6C
			public Instruction BoxIfIndexMatches(int index)
			{
				if (index != this._index)
				{
					return null;
				}
				return new InitializeLocalInstruction.MutableBox(index, this._type);
			}

			// Token: 0x17000489 RID: 1161
			// (get) Token: 0x0600177B RID: 6011 RVA: 0x00045D85 File Offset: 0x00043F85
			public override string InstructionName
			{
				get
				{
					return "InitMutableValue";
				}
			}

			// Token: 0x04000B5C RID: 2908
			private readonly Type _type;
		}

		// Token: 0x0200037A RID: 890
		internal sealed class MutableBox : InitializeLocalInstruction
		{
			// Token: 0x0600177C RID: 6012 RVA: 0x00045D8C File Offset: 0x00043F8C
			internal MutableBox(int index, Type type) : base(index)
			{
				this._type = type;
			}

			// Token: 0x0600177D RID: 6013 RVA: 0x00045D9C File Offset: 0x00043F9C
			public override int Run(InterpretedFrame frame)
			{
				object value;
				try
				{
					value = Activator.CreateInstance(this._type);
				}
				catch (TargetInvocationException exception)
				{
					ExceptionHelpers.UnwrapAndRethrow(exception);
					throw ContractUtils.Unreachable;
				}
				frame.Data[this._index] = new StrongBox<object>(value);
				return 1;
			}

			// Token: 0x1700048A RID: 1162
			// (get) Token: 0x0600177E RID: 6014 RVA: 0x00045DE8 File Offset: 0x00043FE8
			public override string InstructionName
			{
				get
				{
					return "InitMutableBox";
				}
			}

			// Token: 0x04000B5D RID: 2909
			private readonly Type _type;
		}
	}
}
