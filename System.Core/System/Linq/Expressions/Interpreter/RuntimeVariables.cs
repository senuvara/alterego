﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020003E0 RID: 992
	internal sealed class RuntimeVariables : IRuntimeVariables
	{
		// Token: 0x0600188C RID: 6284 RVA: 0x00048D66 File Offset: 0x00046F66
		private RuntimeVariables(IStrongBox[] boxes)
		{
			this._boxes = boxes;
		}

		// Token: 0x170004BC RID: 1212
		// (get) Token: 0x0600188D RID: 6285 RVA: 0x00048D75 File Offset: 0x00046F75
		int IRuntimeVariables.Count
		{
			get
			{
				return this._boxes.Length;
			}
		}

		// Token: 0x170004BD RID: 1213
		object IRuntimeVariables.this[int index]
		{
			get
			{
				return this._boxes[index].Value;
			}
			set
			{
				this._boxes[index].Value = value;
			}
		}

		// Token: 0x06001890 RID: 6288 RVA: 0x00048D9E File Offset: 0x00046F9E
		internal static IRuntimeVariables Create(IStrongBox[] boxes)
		{
			return new RuntimeVariables(boxes);
		}

		// Token: 0x04000BC5 RID: 3013
		private readonly IStrongBox[] _boxes;
	}
}
