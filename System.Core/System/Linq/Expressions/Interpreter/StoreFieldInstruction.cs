﻿using System;
using System.Reflection;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000300 RID: 768
	internal sealed class StoreFieldInstruction : FieldInstruction
	{
		// Token: 0x060014D8 RID: 5336 RVA: 0x0003ADC8 File Offset: 0x00038FC8
		public StoreFieldInstruction(FieldInfo field) : base(field)
		{
		}

		// Token: 0x17000436 RID: 1078
		// (get) Token: 0x060014D9 RID: 5337 RVA: 0x0003AE21 File Offset: 0x00039021
		public override string InstructionName
		{
			get
			{
				return "StoreField";
			}
		}

		// Token: 0x17000437 RID: 1079
		// (get) Token: 0x060014DA RID: 5338 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x060014DB RID: 5339 RVA: 0x0003AE28 File Offset: 0x00039028
		public override int Run(InterpretedFrame frame)
		{
			object value = frame.Pop();
			object obj = frame.Pop();
			Instruction.NullCheck(obj);
			this._field.SetValue(obj, value);
			return 1;
		}
	}
}
