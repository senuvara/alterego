﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002C3 RID: 707
	internal sealed class ThrowInstruction : Instruction
	{
		// Token: 0x06001440 RID: 5184 RVA: 0x000398A9 File Offset: 0x00037AA9
		private ThrowInstruction(bool hasResult, bool isRethrow)
		{
			this._hasResult = hasResult;
			this._rethrow = isRethrow;
		}

		// Token: 0x1700041C RID: 1052
		// (get) Token: 0x06001441 RID: 5185 RVA: 0x000398BF File Offset: 0x00037ABF
		public override string InstructionName
		{
			get
			{
				return "Throw";
			}
		}

		// Token: 0x1700041D RID: 1053
		// (get) Token: 0x06001442 RID: 5186 RVA: 0x000398C6 File Offset: 0x00037AC6
		public override int ProducedStack
		{
			get
			{
				if (!this._hasResult)
				{
					return 0;
				}
				return 1;
			}
		}

		// Token: 0x1700041E RID: 1054
		// (get) Token: 0x06001443 RID: 5187 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x06001444 RID: 5188 RVA: 0x000398D3 File Offset: 0x00037AD3
		public override int Run(InterpretedFrame frame)
		{
			Exception ex = ThrowInstruction.WrapThrownObject(frame.Pop());
			if (this._rethrow)
			{
				throw new RethrowException();
			}
			throw ex;
		}

		// Token: 0x06001445 RID: 5189 RVA: 0x000398EE File Offset: 0x00037AEE
		private static Exception WrapThrownObject(object thrown)
		{
			Exception result;
			if (thrown != null)
			{
				if ((result = (thrown as Exception)) == null)
				{
					return ThrowInstruction.RuntimeWrap(thrown);
				}
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06001446 RID: 5190 RVA: 0x00039908 File Offset: 0x00037B08
		private static RuntimeWrappedException RuntimeWrap(object thrown)
		{
			ConstructorInfo constructorInfo;
			if ((constructorInfo = ThrowInstruction._runtimeWrappedExceptionCtor) == null)
			{
				constructorInfo = (ThrowInstruction._runtimeWrappedExceptionCtor = typeof(RuntimeWrappedException).GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.ExactBinding, null, new Type[]
				{
					typeof(object)
				}, null));
			}
			return (RuntimeWrappedException)constructorInfo.Invoke(new object[]
			{
				thrown
			});
		}

		// Token: 0x06001447 RID: 5191 RVA: 0x00039961 File Offset: 0x00037B61
		// Note: this type is marked as 'beforefieldinit'.
		static ThrowInstruction()
		{
		}

		// Token: 0x04000A14 RID: 2580
		internal static readonly ThrowInstruction Throw = new ThrowInstruction(true, false);

		// Token: 0x04000A15 RID: 2581
		internal static readonly ThrowInstruction VoidThrow = new ThrowInstruction(false, false);

		// Token: 0x04000A16 RID: 2582
		internal static readonly ThrowInstruction Rethrow = new ThrowInstruction(true, true);

		// Token: 0x04000A17 RID: 2583
		internal static readonly ThrowInstruction VoidRethrow = new ThrowInstruction(false, true);

		// Token: 0x04000A18 RID: 2584
		private static ConstructorInfo _runtimeWrappedExceptionCtor;

		// Token: 0x04000A19 RID: 2585
		private readonly bool _hasResult;

		// Token: 0x04000A1A RID: 2586
		private readonly bool _rethrow;
	}
}
