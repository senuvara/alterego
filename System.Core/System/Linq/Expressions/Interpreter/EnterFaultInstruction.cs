﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002BD RID: 701
	internal sealed class EnterFaultInstruction : IndexedBranchInstruction
	{
		// Token: 0x0600141D RID: 5149 RVA: 0x00039573 File Offset: 0x00037773
		private EnterFaultInstruction(int labelIndex) : base(labelIndex)
		{
		}

		// Token: 0x1700040D RID: 1037
		// (get) Token: 0x0600141E RID: 5150 RVA: 0x00039736 File Offset: 0x00037936
		public override string InstructionName
		{
			get
			{
				return "EnterFault";
			}
		}

		// Token: 0x1700040E RID: 1038
		// (get) Token: 0x0600141F RID: 5151 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ProducedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x06001420 RID: 5152 RVA: 0x00039740 File Offset: 0x00037940
		internal static EnterFaultInstruction Create(int labelIndex)
		{
			if (labelIndex < 32)
			{
				EnterFaultInstruction result;
				if ((result = EnterFaultInstruction.s_cache[labelIndex]) == null)
				{
					result = (EnterFaultInstruction.s_cache[labelIndex] = new EnterFaultInstruction(labelIndex));
				}
				return result;
			}
			return new EnterFaultInstruction(labelIndex);
		}

		// Token: 0x06001421 RID: 5153 RVA: 0x00039774 File Offset: 0x00037974
		public override int Run(InterpretedFrame frame)
		{
			frame.SetStackDepth(base.GetLabel(frame).StackDepth);
			frame.PushPendingContinuation();
			frame.RemoveContinuation();
			return 1;
		}

		// Token: 0x06001422 RID: 5154 RVA: 0x00039795 File Offset: 0x00037995
		// Note: this type is marked as 'beforefieldinit'.
		static EnterFaultInstruction()
		{
		}

		// Token: 0x04000A0B RID: 2571
		private static readonly EnterFaultInstruction[] s_cache = new EnterFaultInstruction[32];
	}
}
