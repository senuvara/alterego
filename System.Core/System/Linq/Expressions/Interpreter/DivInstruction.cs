﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002D0 RID: 720
	internal abstract class DivInstruction : Instruction
	{
		// Token: 0x17000428 RID: 1064
		// (get) Token: 0x0600146A RID: 5226 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x17000429 RID: 1065
		// (get) Token: 0x0600146B RID: 5227 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x1700042A RID: 1066
		// (get) Token: 0x0600146C RID: 5228 RVA: 0x00039D18 File Offset: 0x00037F18
		public override string InstructionName
		{
			get
			{
				return "Div";
			}
		}

		// Token: 0x0600146D RID: 5229 RVA: 0x00037C51 File Offset: 0x00035E51
		private DivInstruction()
		{
		}

		// Token: 0x0600146E RID: 5230 RVA: 0x00039D20 File Offset: 0x00037F20
		public static Instruction Create(Type type)
		{
			switch (type.GetNonNullableType().GetTypeCode())
			{
			case TypeCode.Int16:
			{
				Instruction result;
				if ((result = DivInstruction.s_Int16) == null)
				{
					result = (DivInstruction.s_Int16 = new DivInstruction.DivInt16());
				}
				return result;
			}
			case TypeCode.UInt16:
			{
				Instruction result2;
				if ((result2 = DivInstruction.s_UInt16) == null)
				{
					result2 = (DivInstruction.s_UInt16 = new DivInstruction.DivUInt16());
				}
				return result2;
			}
			case TypeCode.Int32:
			{
				Instruction result3;
				if ((result3 = DivInstruction.s_Int32) == null)
				{
					result3 = (DivInstruction.s_Int32 = new DivInstruction.DivInt32());
				}
				return result3;
			}
			case TypeCode.UInt32:
			{
				Instruction result4;
				if ((result4 = DivInstruction.s_UInt32) == null)
				{
					result4 = (DivInstruction.s_UInt32 = new DivInstruction.DivUInt32());
				}
				return result4;
			}
			case TypeCode.Int64:
			{
				Instruction result5;
				if ((result5 = DivInstruction.s_Int64) == null)
				{
					result5 = (DivInstruction.s_Int64 = new DivInstruction.DivInt64());
				}
				return result5;
			}
			case TypeCode.UInt64:
			{
				Instruction result6;
				if ((result6 = DivInstruction.s_UInt64) == null)
				{
					result6 = (DivInstruction.s_UInt64 = new DivInstruction.DivUInt64());
				}
				return result6;
			}
			case TypeCode.Single:
			{
				Instruction result7;
				if ((result7 = DivInstruction.s_Single) == null)
				{
					result7 = (DivInstruction.s_Single = new DivInstruction.DivSingle());
				}
				return result7;
			}
			case TypeCode.Double:
			{
				Instruction result8;
				if ((result8 = DivInstruction.s_Double) == null)
				{
					result8 = (DivInstruction.s_Double = new DivInstruction.DivDouble());
				}
				return result8;
			}
			default:
				throw ContractUtils.Unreachable;
			}
		}

		// Token: 0x04000A27 RID: 2599
		private static Instruction s_Int16;

		// Token: 0x04000A28 RID: 2600
		private static Instruction s_Int32;

		// Token: 0x04000A29 RID: 2601
		private static Instruction s_Int64;

		// Token: 0x04000A2A RID: 2602
		private static Instruction s_UInt16;

		// Token: 0x04000A2B RID: 2603
		private static Instruction s_UInt32;

		// Token: 0x04000A2C RID: 2604
		private static Instruction s_UInt64;

		// Token: 0x04000A2D RID: 2605
		private static Instruction s_Single;

		// Token: 0x04000A2E RID: 2606
		private static Instruction s_Double;

		// Token: 0x020002D1 RID: 721
		private sealed class DivInt16 : DivInstruction
		{
			// Token: 0x0600146F RID: 5231 RVA: 0x00039E14 File Offset: 0x00038014
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((short)obj / (short)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x06001470 RID: 5232 RVA: 0x00039E66 File Offset: 0x00038066
			public DivInt16()
			{
			}
		}

		// Token: 0x020002D2 RID: 722
		private sealed class DivInt32 : DivInstruction
		{
			// Token: 0x06001471 RID: 5233 RVA: 0x00039E70 File Offset: 0x00038070
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ScriptingRuntimeHelpers.Int32ToObject((int)obj / (int)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x06001472 RID: 5234 RVA: 0x00039E66 File Offset: 0x00038066
			public DivInt32()
			{
			}
		}

		// Token: 0x020002D3 RID: 723
		private sealed class DivInt64 : DivInstruction
		{
			// Token: 0x06001473 RID: 5235 RVA: 0x00039EC4 File Offset: 0x000380C4
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((long)obj / (long)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x06001474 RID: 5236 RVA: 0x00039E66 File Offset: 0x00038066
			public DivInt64()
			{
			}
		}

		// Token: 0x020002D4 RID: 724
		private sealed class DivUInt16 : DivInstruction
		{
			// Token: 0x06001475 RID: 5237 RVA: 0x00039F18 File Offset: 0x00038118
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((ushort)obj / (ushort)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x06001476 RID: 5238 RVA: 0x00039E66 File Offset: 0x00038066
			public DivUInt16()
			{
			}
		}

		// Token: 0x020002D5 RID: 725
		private sealed class DivUInt32 : DivInstruction
		{
			// Token: 0x06001477 RID: 5239 RVA: 0x00039F6C File Offset: 0x0003816C
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((uint)obj / (uint)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x06001478 RID: 5240 RVA: 0x00039E66 File Offset: 0x00038066
			public DivUInt32()
			{
			}
		}

		// Token: 0x020002D6 RID: 726
		private sealed class DivUInt64 : DivInstruction
		{
			// Token: 0x06001479 RID: 5241 RVA: 0x00039FC0 File Offset: 0x000381C0
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((ulong)obj / (ulong)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x0600147A RID: 5242 RVA: 0x00039E66 File Offset: 0x00038066
			public DivUInt64()
			{
			}
		}

		// Token: 0x020002D7 RID: 727
		private sealed class DivSingle : DivInstruction
		{
			// Token: 0x0600147B RID: 5243 RVA: 0x0003A014 File Offset: 0x00038214
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((float)obj / (float)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x0600147C RID: 5244 RVA: 0x00039E66 File Offset: 0x00038066
			public DivSingle()
			{
			}
		}

		// Token: 0x020002D8 RID: 728
		private sealed class DivDouble : DivInstruction
		{
			// Token: 0x0600147D RID: 5245 RVA: 0x0003A068 File Offset: 0x00038268
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((double)obj / (double)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x0600147E RID: 5246 RVA: 0x00039E66 File Offset: 0x00038066
			public DivDouble()
			{
			}
		}
	}
}
