﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200036A RID: 874
	internal sealed class LoadLocalFromClosureInstruction : LocalAccessInstruction
	{
		// Token: 0x0600173B RID: 5947 RVA: 0x000459CD File Offset: 0x00043BCD
		internal LoadLocalFromClosureInstruction(int index) : base(index)
		{
		}

		// Token: 0x1700046F RID: 1135
		// (get) Token: 0x0600173C RID: 5948 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000470 RID: 1136
		// (get) Token: 0x0600173D RID: 5949 RVA: 0x00045A6F File Offset: 0x00043C6F
		public override string InstructionName
		{
			get
			{
				return "LoadLocalClosure";
			}
		}

		// Token: 0x0600173E RID: 5950 RVA: 0x00045A78 File Offset: 0x00043C78
		public override int Run(InterpretedFrame frame)
		{
			IStrongBox strongBox = frame.Closure[this._index];
			object[] data = frame.Data;
			int stackIndex = frame.StackIndex;
			frame.StackIndex = stackIndex + 1;
			data[stackIndex] = strongBox.Value;
			return 1;
		}
	}
}
