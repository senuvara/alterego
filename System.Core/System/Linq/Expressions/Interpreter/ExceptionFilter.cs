﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000351 RID: 849
	internal sealed class ExceptionFilter
	{
		// Token: 0x06001647 RID: 5703 RVA: 0x0003E80B File Offset: 0x0003CA0B
		internal ExceptionFilter(int labelIndex, int start, int end)
		{
			this.LabelIndex = labelIndex;
			this.StartIndex = start;
			this.EndIndex = end;
		}

		// Token: 0x04000B16 RID: 2838
		public readonly int LabelIndex;

		// Token: 0x04000B17 RID: 2839
		public readonly int StartIndex;

		// Token: 0x04000B18 RID: 2840
		public readonly int EndIndex;
	}
}
