﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200035C RID: 860
	internal abstract class ByRefUpdater
	{
		// Token: 0x060016C5 RID: 5829 RVA: 0x0004318A File Offset: 0x0004138A
		public ByRefUpdater(int argumentIndex)
		{
			this.ArgumentIndex = argumentIndex;
		}

		// Token: 0x060016C6 RID: 5830
		public abstract void Update(InterpretedFrame frame, object value);

		// Token: 0x060016C7 RID: 5831 RVA: 0x000039E8 File Offset: 0x00001BE8
		public virtual void UndefineTemps(InstructionList instructions, LocalVariables locals)
		{
		}

		// Token: 0x04000B3F RID: 2879
		public readonly int ArgumentIndex;
	}
}
