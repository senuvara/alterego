﻿using System;
using System.Globalization;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002AD RID: 685
	internal struct RuntimeLabel
	{
		// Token: 0x060013BB RID: 5051 RVA: 0x00038889 File Offset: 0x00036A89
		public RuntimeLabel(int index, int continuationStackDepth, int stackDepth)
		{
			this.Index = index;
			this.ContinuationStackDepth = continuationStackDepth;
			this.StackDepth = stackDepth;
		}

		// Token: 0x060013BC RID: 5052 RVA: 0x000388A0 File Offset: 0x00036AA0
		public override string ToString()
		{
			return string.Format(CultureInfo.InvariantCulture, "->{0} C({1}) S({2})", this.Index, this.ContinuationStackDepth, this.StackDepth);
		}

		// Token: 0x040009E9 RID: 2537
		public readonly int Index;

		// Token: 0x040009EA RID: 2538
		public readonly int StackDepth;

		// Token: 0x040009EB RID: 2539
		public readonly int ContinuationStackDepth;
	}
}
