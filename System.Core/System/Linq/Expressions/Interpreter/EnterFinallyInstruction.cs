﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002BB RID: 699
	internal sealed class EnterFinallyInstruction : IndexedBranchInstruction
	{
		// Token: 0x06001411 RID: 5137 RVA: 0x00039573 File Offset: 0x00037773
		private EnterFinallyInstruction(int labelIndex) : base(labelIndex)
		{
		}

		// Token: 0x17000408 RID: 1032
		// (get) Token: 0x06001412 RID: 5138 RVA: 0x00039698 File Offset: 0x00037898
		public override string InstructionName
		{
			get
			{
				return "EnterFinally";
			}
		}

		// Token: 0x17000409 RID: 1033
		// (get) Token: 0x06001413 RID: 5139 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ProducedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x1700040A RID: 1034
		// (get) Token: 0x06001414 RID: 5140 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedContinuations
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x06001415 RID: 5141 RVA: 0x000396A0 File Offset: 0x000378A0
		internal static EnterFinallyInstruction Create(int labelIndex)
		{
			if (labelIndex < 32)
			{
				EnterFinallyInstruction result;
				if ((result = EnterFinallyInstruction.s_cache[labelIndex]) == null)
				{
					result = (EnterFinallyInstruction.s_cache[labelIndex] = new EnterFinallyInstruction(labelIndex));
				}
				return result;
			}
			return new EnterFinallyInstruction(labelIndex);
		}

		// Token: 0x06001416 RID: 5142 RVA: 0x000396D4 File Offset: 0x000378D4
		public override int Run(InterpretedFrame frame)
		{
			if (!frame.IsJumpHappened())
			{
				frame.SetStackDepth(base.GetLabel(frame).StackDepth);
			}
			frame.PushPendingContinuation();
			frame.RemoveContinuation();
			return 1;
		}

		// Token: 0x06001417 RID: 5143 RVA: 0x000396FD File Offset: 0x000378FD
		// Note: this type is marked as 'beforefieldinit'.
		static EnterFinallyInstruction()
		{
		}

		// Token: 0x04000A09 RID: 2569
		private static readonly EnterFinallyInstruction[] s_cache = new EnterFinallyInstruction[32];
	}
}
