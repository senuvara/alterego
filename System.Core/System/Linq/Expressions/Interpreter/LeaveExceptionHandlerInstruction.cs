﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002C2 RID: 706
	internal sealed class LeaveExceptionHandlerInstruction : IndexedBranchInstruction
	{
		// Token: 0x06001439 RID: 5177 RVA: 0x00039820 File Offset: 0x00037A20
		private LeaveExceptionHandlerInstruction(int labelIndex, bool hasValue) : base(labelIndex)
		{
			this._hasValue = hasValue;
		}

		// Token: 0x17000419 RID: 1049
		// (get) Token: 0x0600143A RID: 5178 RVA: 0x00039830 File Offset: 0x00037A30
		public override string InstructionName
		{
			get
			{
				return "LeaveExceptionHandler";
			}
		}

		// Token: 0x1700041A RID: 1050
		// (get) Token: 0x0600143B RID: 5179 RVA: 0x00039837 File Offset: 0x00037A37
		public override int ConsumedStack
		{
			get
			{
				if (!this._hasValue)
				{
					return 0;
				}
				return 1;
			}
		}

		// Token: 0x1700041B RID: 1051
		// (get) Token: 0x0600143C RID: 5180 RVA: 0x00039837 File Offset: 0x00037A37
		public override int ProducedStack
		{
			get
			{
				if (!this._hasValue)
				{
					return 0;
				}
				return 1;
			}
		}

		// Token: 0x0600143D RID: 5181 RVA: 0x00039844 File Offset: 0x00037A44
		internal static LeaveExceptionHandlerInstruction Create(int labelIndex, bool hasValue)
		{
			if (labelIndex < 32)
			{
				int num = 2 * labelIndex | (hasValue ? 1 : 0);
				LeaveExceptionHandlerInstruction result;
				if ((result = LeaveExceptionHandlerInstruction.s_cache[num]) == null)
				{
					result = (LeaveExceptionHandlerInstruction.s_cache[num] = new LeaveExceptionHandlerInstruction(labelIndex, hasValue));
				}
				return result;
			}
			return new LeaveExceptionHandlerInstruction(labelIndex, hasValue);
		}

		// Token: 0x0600143E RID: 5182 RVA: 0x00039886 File Offset: 0x00037A86
		public override int Run(InterpretedFrame frame)
		{
			return base.GetLabel(frame).Index - frame.InstructionIndex;
		}

		// Token: 0x0600143F RID: 5183 RVA: 0x0003989B File Offset: 0x00037A9B
		// Note: this type is marked as 'beforefieldinit'.
		static LeaveExceptionHandlerInstruction()
		{
		}

		// Token: 0x04000A12 RID: 2578
		private static readonly LeaveExceptionHandlerInstruction[] s_cache = new LeaveExceptionHandlerInstruction[64];

		// Token: 0x04000A13 RID: 2579
		private readonly bool _hasValue;
	}
}
