﻿using System;
using System.Collections.Generic;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002C4 RID: 708
	internal sealed class IntSwitchInstruction<T> : Instruction
	{
		// Token: 0x06001448 RID: 5192 RVA: 0x00039993 File Offset: 0x00037B93
		internal IntSwitchInstruction(Dictionary<T, int> cases)
		{
			this._cases = cases;
		}

		// Token: 0x1700041F RID: 1055
		// (get) Token: 0x06001449 RID: 5193 RVA: 0x000399A2 File Offset: 0x00037BA2
		public override string InstructionName
		{
			get
			{
				return "IntSwitch";
			}
		}

		// Token: 0x17000420 RID: 1056
		// (get) Token: 0x0600144A RID: 5194 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x0600144B RID: 5195 RVA: 0x000399AC File Offset: 0x00037BAC
		public override int Run(InterpretedFrame frame)
		{
			int result;
			if (!this._cases.TryGetValue((T)((object)frame.Pop()), out result))
			{
				return 1;
			}
			return result;
		}

		// Token: 0x04000A1B RID: 2587
		private readonly Dictionary<T, int> _cases;
	}
}
