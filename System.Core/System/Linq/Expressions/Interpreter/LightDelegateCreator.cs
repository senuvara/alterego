﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000362 RID: 866
	internal sealed class LightDelegateCreator
	{
		// Token: 0x060016D6 RID: 5846 RVA: 0x0004352A File Offset: 0x0004172A
		internal LightDelegateCreator(Interpreter interpreter, LambdaExpression lambda)
		{
			this.Interpreter = interpreter;
			this._lambda = lambda;
		}

		// Token: 0x17000469 RID: 1129
		// (get) Token: 0x060016D7 RID: 5847 RVA: 0x00043540 File Offset: 0x00041740
		internal Interpreter Interpreter
		{
			[CompilerGenerated]
			get
			{
				return this.<Interpreter>k__BackingField;
			}
		}

		// Token: 0x060016D8 RID: 5848 RVA: 0x00043548 File Offset: 0x00041748
		public Delegate CreateDelegate()
		{
			return this.CreateDelegate(null);
		}

		// Token: 0x060016D9 RID: 5849 RVA: 0x00043551 File Offset: 0x00041751
		internal Delegate CreateDelegate(IStrongBox[] closure)
		{
			return new LightLambda(this, closure).MakeDelegate(this._lambda.Type);
		}

		// Token: 0x04000B4A RID: 2890
		private readonly LambdaExpression _lambda;

		// Token: 0x04000B4B RID: 2891
		[CompilerGenerated]
		private readonly Interpreter <Interpreter>k__BackingField;
	}
}
