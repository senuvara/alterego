﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000366 RID: 870
	internal interface IBoxableInstruction
	{
		// Token: 0x0600172F RID: 5935
		Instruction BoxIfIndexMatches(int index);
	}
}
