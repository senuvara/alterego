﻿using System;
using System.Reflection;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002FE RID: 766
	internal sealed class LoadStaticFieldInstruction : FieldInstruction
	{
		// Token: 0x060014CF RID: 5327 RVA: 0x0003ADC8 File Offset: 0x00038FC8
		public LoadStaticFieldInstruction(FieldInfo field) : base(field)
		{
		}

		// Token: 0x17000431 RID: 1073
		// (get) Token: 0x060014D0 RID: 5328 RVA: 0x0003ADD1 File Offset: 0x00038FD1
		public override string InstructionName
		{
			get
			{
				return "LoadStaticField";
			}
		}

		// Token: 0x17000432 RID: 1074
		// (get) Token: 0x060014D1 RID: 5329 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x060014D2 RID: 5330 RVA: 0x0003ADD8 File Offset: 0x00038FD8
		public override int Run(InterpretedFrame frame)
		{
			frame.Push(this._field.GetValue(null));
			return 1;
		}
	}
}
