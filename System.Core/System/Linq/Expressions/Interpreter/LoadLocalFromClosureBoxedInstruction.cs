﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200036B RID: 875
	internal sealed class LoadLocalFromClosureBoxedInstruction : LocalAccessInstruction
	{
		// Token: 0x0600173F RID: 5951 RVA: 0x000459CD File Offset: 0x00043BCD
		internal LoadLocalFromClosureBoxedInstruction(int index) : base(index)
		{
		}

		// Token: 0x17000471 RID: 1137
		// (get) Token: 0x06001740 RID: 5952 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000472 RID: 1138
		// (get) Token: 0x06001741 RID: 5953 RVA: 0x000459D6 File Offset: 0x00043BD6
		public override string InstructionName
		{
			get
			{
				return "LoadLocal";
			}
		}

		// Token: 0x06001742 RID: 5954 RVA: 0x00045AB4 File Offset: 0x00043CB4
		public override int Run(InterpretedFrame frame)
		{
			IStrongBox strongBox = frame.Closure[this._index];
			object[] data = frame.Data;
			int stackIndex = frame.StackIndex;
			frame.StackIndex = stackIndex + 1;
			data[stackIndex] = strongBox;
			return 1;
		}
	}
}
