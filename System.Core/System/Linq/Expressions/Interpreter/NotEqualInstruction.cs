﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020003A5 RID: 933
	internal abstract class NotEqualInstruction : Instruction
	{
		// Token: 0x170004A7 RID: 1191
		// (get) Token: 0x060017F6 RID: 6134 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x170004A8 RID: 1192
		// (get) Token: 0x060017F7 RID: 6135 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004A9 RID: 1193
		// (get) Token: 0x060017F8 RID: 6136 RVA: 0x000470A8 File Offset: 0x000452A8
		public override string InstructionName
		{
			get
			{
				return "NotEqual";
			}
		}

		// Token: 0x060017F9 RID: 6137 RVA: 0x00037C51 File Offset: 0x00035E51
		private NotEqualInstruction()
		{
		}

		// Token: 0x060017FA RID: 6138 RVA: 0x000470B0 File Offset: 0x000452B0
		public static Instruction Create(Type type, bool liftedToNull)
		{
			if (liftedToNull)
			{
				switch (type.GetNonNullableType().GetTypeCode())
				{
				case TypeCode.Boolean:
					return ExclusiveOrInstruction.Create(type);
				case TypeCode.Char:
				{
					Instruction result;
					if ((result = NotEqualInstruction.s_CharLiftedToNull) == null)
					{
						result = (NotEqualInstruction.s_CharLiftedToNull = new NotEqualInstruction.NotEqualCharLiftedToNull());
					}
					return result;
				}
				case TypeCode.SByte:
				{
					Instruction result2;
					if ((result2 = NotEqualInstruction.s_SByteLiftedToNull) == null)
					{
						result2 = (NotEqualInstruction.s_SByteLiftedToNull = new NotEqualInstruction.NotEqualSByteLiftedToNull());
					}
					return result2;
				}
				case TypeCode.Byte:
				{
					Instruction result3;
					if ((result3 = NotEqualInstruction.s_ByteLiftedToNull) == null)
					{
						result3 = (NotEqualInstruction.s_ByteLiftedToNull = new NotEqualInstruction.NotEqualByteLiftedToNull());
					}
					return result3;
				}
				case TypeCode.Int16:
				{
					Instruction result4;
					if ((result4 = NotEqualInstruction.s_Int16LiftedToNull) == null)
					{
						result4 = (NotEqualInstruction.s_Int16LiftedToNull = new NotEqualInstruction.NotEqualInt16LiftedToNull());
					}
					return result4;
				}
				case TypeCode.UInt16:
				{
					Instruction result5;
					if ((result5 = NotEqualInstruction.s_UInt16LiftedToNull) == null)
					{
						result5 = (NotEqualInstruction.s_UInt16LiftedToNull = new NotEqualInstruction.NotEqualUInt16LiftedToNull());
					}
					return result5;
				}
				case TypeCode.Int32:
				{
					Instruction result6;
					if ((result6 = NotEqualInstruction.s_Int32LiftedToNull) == null)
					{
						result6 = (NotEqualInstruction.s_Int32LiftedToNull = new NotEqualInstruction.NotEqualInt32LiftedToNull());
					}
					return result6;
				}
				case TypeCode.UInt32:
				{
					Instruction result7;
					if ((result7 = NotEqualInstruction.s_UInt32LiftedToNull) == null)
					{
						result7 = (NotEqualInstruction.s_UInt32LiftedToNull = new NotEqualInstruction.NotEqualUInt32LiftedToNull());
					}
					return result7;
				}
				case TypeCode.Int64:
				{
					Instruction result8;
					if ((result8 = NotEqualInstruction.s_Int64LiftedToNull) == null)
					{
						result8 = (NotEqualInstruction.s_Int64LiftedToNull = new NotEqualInstruction.NotEqualInt64LiftedToNull());
					}
					return result8;
				}
				case TypeCode.UInt64:
				{
					Instruction result9;
					if ((result9 = NotEqualInstruction.s_UInt64LiftedToNull) == null)
					{
						result9 = (NotEqualInstruction.s_UInt64LiftedToNull = new NotEqualInstruction.NotEqualUInt64LiftedToNull());
					}
					return result9;
				}
				case TypeCode.Single:
				{
					Instruction result10;
					if ((result10 = NotEqualInstruction.s_SingleLiftedToNull) == null)
					{
						result10 = (NotEqualInstruction.s_SingleLiftedToNull = new NotEqualInstruction.NotEqualSingleLiftedToNull());
					}
					return result10;
				}
				default:
				{
					Instruction result11;
					if ((result11 = NotEqualInstruction.s_DoubleLiftedToNull) == null)
					{
						result11 = (NotEqualInstruction.s_DoubleLiftedToNull = new NotEqualInstruction.NotEqualDoubleLiftedToNull());
					}
					return result11;
				}
				}
			}
			else
			{
				switch (type.GetNonNullableType().GetTypeCode())
				{
				case TypeCode.Boolean:
				{
					Instruction result12;
					if ((result12 = NotEqualInstruction.s_Boolean) == null)
					{
						result12 = (NotEqualInstruction.s_Boolean = new NotEqualInstruction.NotEqualBoolean());
					}
					return result12;
				}
				case TypeCode.Char:
				{
					Instruction result13;
					if ((result13 = NotEqualInstruction.s_Char) == null)
					{
						result13 = (NotEqualInstruction.s_Char = new NotEqualInstruction.NotEqualChar());
					}
					return result13;
				}
				case TypeCode.SByte:
				{
					Instruction result14;
					if ((result14 = NotEqualInstruction.s_SByte) == null)
					{
						result14 = (NotEqualInstruction.s_SByte = new NotEqualInstruction.NotEqualSByte());
					}
					return result14;
				}
				case TypeCode.Byte:
				{
					Instruction result15;
					if ((result15 = NotEqualInstruction.s_Byte) == null)
					{
						result15 = (NotEqualInstruction.s_Byte = new NotEqualInstruction.NotEqualByte());
					}
					return result15;
				}
				case TypeCode.Int16:
				{
					Instruction result16;
					if ((result16 = NotEqualInstruction.s_Int16) == null)
					{
						result16 = (NotEqualInstruction.s_Int16 = new NotEqualInstruction.NotEqualInt16());
					}
					return result16;
				}
				case TypeCode.UInt16:
				{
					Instruction result17;
					if ((result17 = NotEqualInstruction.s_UInt16) == null)
					{
						result17 = (NotEqualInstruction.s_UInt16 = new NotEqualInstruction.NotEqualUInt16());
					}
					return result17;
				}
				case TypeCode.Int32:
				{
					Instruction result18;
					if ((result18 = NotEqualInstruction.s_Int32) == null)
					{
						result18 = (NotEqualInstruction.s_Int32 = new NotEqualInstruction.NotEqualInt32());
					}
					return result18;
				}
				case TypeCode.UInt32:
				{
					Instruction result19;
					if ((result19 = NotEqualInstruction.s_UInt32) == null)
					{
						result19 = (NotEqualInstruction.s_UInt32 = new NotEqualInstruction.NotEqualUInt32());
					}
					return result19;
				}
				case TypeCode.Int64:
				{
					Instruction result20;
					if ((result20 = NotEqualInstruction.s_Int64) == null)
					{
						result20 = (NotEqualInstruction.s_Int64 = new NotEqualInstruction.NotEqualInt64());
					}
					return result20;
				}
				case TypeCode.UInt64:
				{
					Instruction result21;
					if ((result21 = NotEqualInstruction.s_UInt64) == null)
					{
						result21 = (NotEqualInstruction.s_UInt64 = new NotEqualInstruction.NotEqualUInt64());
					}
					return result21;
				}
				case TypeCode.Single:
				{
					Instruction result22;
					if ((result22 = NotEqualInstruction.s_Single) == null)
					{
						result22 = (NotEqualInstruction.s_Single = new NotEqualInstruction.NotEqualSingle());
					}
					return result22;
				}
				case TypeCode.Double:
				{
					Instruction result23;
					if ((result23 = NotEqualInstruction.s_Double) == null)
					{
						result23 = (NotEqualInstruction.s_Double = new NotEqualInstruction.NotEqualDouble());
					}
					return result23;
				}
				default:
				{
					Instruction result24;
					if ((result24 = NotEqualInstruction.s_reference) == null)
					{
						result24 = (NotEqualInstruction.s_reference = new NotEqualInstruction.NotEqualReference());
					}
					return result24;
				}
				}
			}
		}

		// Token: 0x04000B8F RID: 2959
		private static Instruction s_reference;

		// Token: 0x04000B90 RID: 2960
		private static Instruction s_Boolean;

		// Token: 0x04000B91 RID: 2961
		private static Instruction s_SByte;

		// Token: 0x04000B92 RID: 2962
		private static Instruction s_Int16;

		// Token: 0x04000B93 RID: 2963
		private static Instruction s_Char;

		// Token: 0x04000B94 RID: 2964
		private static Instruction s_Int32;

		// Token: 0x04000B95 RID: 2965
		private static Instruction s_Int64;

		// Token: 0x04000B96 RID: 2966
		private static Instruction s_Byte;

		// Token: 0x04000B97 RID: 2967
		private static Instruction s_UInt16;

		// Token: 0x04000B98 RID: 2968
		private static Instruction s_UInt32;

		// Token: 0x04000B99 RID: 2969
		private static Instruction s_UInt64;

		// Token: 0x04000B9A RID: 2970
		private static Instruction s_Single;

		// Token: 0x04000B9B RID: 2971
		private static Instruction s_Double;

		// Token: 0x04000B9C RID: 2972
		private static Instruction s_SByteLiftedToNull;

		// Token: 0x04000B9D RID: 2973
		private static Instruction s_Int16LiftedToNull;

		// Token: 0x04000B9E RID: 2974
		private static Instruction s_CharLiftedToNull;

		// Token: 0x04000B9F RID: 2975
		private static Instruction s_Int32LiftedToNull;

		// Token: 0x04000BA0 RID: 2976
		private static Instruction s_Int64LiftedToNull;

		// Token: 0x04000BA1 RID: 2977
		private static Instruction s_ByteLiftedToNull;

		// Token: 0x04000BA2 RID: 2978
		private static Instruction s_UInt16LiftedToNull;

		// Token: 0x04000BA3 RID: 2979
		private static Instruction s_UInt32LiftedToNull;

		// Token: 0x04000BA4 RID: 2980
		private static Instruction s_UInt64LiftedToNull;

		// Token: 0x04000BA5 RID: 2981
		private static Instruction s_SingleLiftedToNull;

		// Token: 0x04000BA6 RID: 2982
		private static Instruction s_DoubleLiftedToNull;

		// Token: 0x020003A6 RID: 934
		private sealed class NotEqualBoolean : NotEqualInstruction
		{
			// Token: 0x060017FB RID: 6139 RVA: 0x00047350 File Offset: 0x00045550
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj != null);
				}
				else if (obj == null)
				{
					frame.Push(true);
				}
				else
				{
					frame.Push((bool)obj2 != (bool)obj);
				}
				return 1;
			}

			// Token: 0x060017FC RID: 6140 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualBoolean()
			{
			}
		}

		// Token: 0x020003A7 RID: 935
		private sealed class NotEqualSByte : NotEqualInstruction
		{
			// Token: 0x060017FD RID: 6141 RVA: 0x000473A8 File Offset: 0x000455A8
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj != null);
				}
				else if (obj == null)
				{
					frame.Push(true);
				}
				else
				{
					frame.Push((sbyte)obj2 != (sbyte)obj);
				}
				return 1;
			}

			// Token: 0x060017FE RID: 6142 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualSByte()
			{
			}
		}

		// Token: 0x020003A8 RID: 936
		private sealed class NotEqualInt16 : NotEqualInstruction
		{
			// Token: 0x060017FF RID: 6143 RVA: 0x000473F8 File Offset: 0x000455F8
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj != null);
				}
				else if (obj == null)
				{
					frame.Push(true);
				}
				else
				{
					frame.Push((short)obj2 != (short)obj);
				}
				return 1;
			}

			// Token: 0x06001800 RID: 6144 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualInt16()
			{
			}
		}

		// Token: 0x020003A9 RID: 937
		private sealed class NotEqualChar : NotEqualInstruction
		{
			// Token: 0x06001801 RID: 6145 RVA: 0x00047448 File Offset: 0x00045648
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj != null);
				}
				else if (obj == null)
				{
					frame.Push(true);
				}
				else
				{
					frame.Push((char)obj2 != (char)obj);
				}
				return 1;
			}

			// Token: 0x06001802 RID: 6146 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualChar()
			{
			}
		}

		// Token: 0x020003AA RID: 938
		private sealed class NotEqualInt32 : NotEqualInstruction
		{
			// Token: 0x06001803 RID: 6147 RVA: 0x00047498 File Offset: 0x00045698
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj != null);
				}
				else if (obj == null)
				{
					frame.Push(true);
				}
				else
				{
					frame.Push((int)obj2 != (int)obj);
				}
				return 1;
			}

			// Token: 0x06001804 RID: 6148 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualInt32()
			{
			}
		}

		// Token: 0x020003AB RID: 939
		private sealed class NotEqualInt64 : NotEqualInstruction
		{
			// Token: 0x06001805 RID: 6149 RVA: 0x000474E8 File Offset: 0x000456E8
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj != null);
				}
				else if (obj == null)
				{
					frame.Push(true);
				}
				else
				{
					frame.Push((long)obj2 != (long)obj);
				}
				return 1;
			}

			// Token: 0x06001806 RID: 6150 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualInt64()
			{
			}
		}

		// Token: 0x020003AC RID: 940
		private sealed class NotEqualByte : NotEqualInstruction
		{
			// Token: 0x06001807 RID: 6151 RVA: 0x00047538 File Offset: 0x00045738
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj != null);
				}
				else if (obj == null)
				{
					frame.Push(true);
				}
				else
				{
					frame.Push((byte)obj2 != (byte)obj);
				}
				return 1;
			}

			// Token: 0x06001808 RID: 6152 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualByte()
			{
			}
		}

		// Token: 0x020003AD RID: 941
		private sealed class NotEqualUInt16 : NotEqualInstruction
		{
			// Token: 0x06001809 RID: 6153 RVA: 0x00047588 File Offset: 0x00045788
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj != null);
				}
				else if (obj == null)
				{
					frame.Push(true);
				}
				else
				{
					frame.Push((ushort)obj2 != (ushort)obj);
				}
				return 1;
			}

			// Token: 0x0600180A RID: 6154 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualUInt16()
			{
			}
		}

		// Token: 0x020003AE RID: 942
		private sealed class NotEqualUInt32 : NotEqualInstruction
		{
			// Token: 0x0600180B RID: 6155 RVA: 0x000475D8 File Offset: 0x000457D8
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj != null);
				}
				else if (obj == null)
				{
					frame.Push(true);
				}
				else
				{
					frame.Push((uint)obj2 != (uint)obj);
				}
				return 1;
			}

			// Token: 0x0600180C RID: 6156 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualUInt32()
			{
			}
		}

		// Token: 0x020003AF RID: 943
		private sealed class NotEqualUInt64 : NotEqualInstruction
		{
			// Token: 0x0600180D RID: 6157 RVA: 0x00047628 File Offset: 0x00045828
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj != null);
				}
				else if (obj == null)
				{
					frame.Push(true);
				}
				else
				{
					frame.Push((ulong)obj2 != (ulong)obj);
				}
				return 1;
			}

			// Token: 0x0600180E RID: 6158 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualUInt64()
			{
			}
		}

		// Token: 0x020003B0 RID: 944
		private sealed class NotEqualSingle : NotEqualInstruction
		{
			// Token: 0x0600180F RID: 6159 RVA: 0x00047678 File Offset: 0x00045878
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj != null);
				}
				else if (obj == null)
				{
					frame.Push(true);
				}
				else
				{
					frame.Push((float)obj2 != (float)obj);
				}
				return 1;
			}

			// Token: 0x06001810 RID: 6160 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualSingle()
			{
			}
		}

		// Token: 0x020003B1 RID: 945
		private sealed class NotEqualDouble : NotEqualInstruction
		{
			// Token: 0x06001811 RID: 6161 RVA: 0x000476C8 File Offset: 0x000458C8
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj != null);
				}
				else if (obj == null)
				{
					frame.Push(true);
				}
				else
				{
					frame.Push((double)obj2 != (double)obj);
				}
				return 1;
			}

			// Token: 0x06001812 RID: 6162 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualDouble()
			{
			}
		}

		// Token: 0x020003B2 RID: 946
		private sealed class NotEqualReference : NotEqualInstruction
		{
			// Token: 0x06001813 RID: 6163 RVA: 0x00047716 File Offset: 0x00045916
			public override int Run(InterpretedFrame frame)
			{
				frame.Push(frame.Pop() != frame.Pop());
				return 1;
			}

			// Token: 0x06001814 RID: 6164 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualReference()
			{
			}
		}

		// Token: 0x020003B3 RID: 947
		private sealed class NotEqualSByteLiftedToNull : NotEqualInstruction
		{
			// Token: 0x06001815 RID: 6165 RVA: 0x00047730 File Offset: 0x00045930
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((sbyte)obj2 != (sbyte)obj);
				}
				return 1;
			}

			// Token: 0x06001816 RID: 6166 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualSByteLiftedToNull()
			{
			}
		}

		// Token: 0x020003B4 RID: 948
		private sealed class NotEqualInt16LiftedToNull : NotEqualInstruction
		{
			// Token: 0x06001817 RID: 6167 RVA: 0x00047774 File Offset: 0x00045974
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((short)obj2 != (short)obj);
				}
				return 1;
			}

			// Token: 0x06001818 RID: 6168 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualInt16LiftedToNull()
			{
			}
		}

		// Token: 0x020003B5 RID: 949
		private sealed class NotEqualCharLiftedToNull : NotEqualInstruction
		{
			// Token: 0x06001819 RID: 6169 RVA: 0x000477B8 File Offset: 0x000459B8
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((char)obj2 != (char)obj);
				}
				return 1;
			}

			// Token: 0x0600181A RID: 6170 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualCharLiftedToNull()
			{
			}
		}

		// Token: 0x020003B6 RID: 950
		private sealed class NotEqualInt32LiftedToNull : NotEqualInstruction
		{
			// Token: 0x0600181B RID: 6171 RVA: 0x000477FC File Offset: 0x000459FC
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((int)obj2 != (int)obj);
				}
				return 1;
			}

			// Token: 0x0600181C RID: 6172 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualInt32LiftedToNull()
			{
			}
		}

		// Token: 0x020003B7 RID: 951
		private sealed class NotEqualInt64LiftedToNull : NotEqualInstruction
		{
			// Token: 0x0600181D RID: 6173 RVA: 0x00047840 File Offset: 0x00045A40
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((long)obj2 != (long)obj);
				}
				return 1;
			}

			// Token: 0x0600181E RID: 6174 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualInt64LiftedToNull()
			{
			}
		}

		// Token: 0x020003B8 RID: 952
		private sealed class NotEqualByteLiftedToNull : NotEqualInstruction
		{
			// Token: 0x0600181F RID: 6175 RVA: 0x00047884 File Offset: 0x00045A84
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((byte)obj2 != (byte)obj);
				}
				return 1;
			}

			// Token: 0x06001820 RID: 6176 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualByteLiftedToNull()
			{
			}
		}

		// Token: 0x020003B9 RID: 953
		private sealed class NotEqualUInt16LiftedToNull : NotEqualInstruction
		{
			// Token: 0x06001821 RID: 6177 RVA: 0x000478C8 File Offset: 0x00045AC8
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((ushort)obj2 != (ushort)obj);
				}
				return 1;
			}

			// Token: 0x06001822 RID: 6178 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualUInt16LiftedToNull()
			{
			}
		}

		// Token: 0x020003BA RID: 954
		private sealed class NotEqualUInt32LiftedToNull : NotEqualInstruction
		{
			// Token: 0x06001823 RID: 6179 RVA: 0x0004790C File Offset: 0x00045B0C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((uint)obj2 != (uint)obj);
				}
				return 1;
			}

			// Token: 0x06001824 RID: 6180 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualUInt32LiftedToNull()
			{
			}
		}

		// Token: 0x020003BB RID: 955
		private sealed class NotEqualUInt64LiftedToNull : NotEqualInstruction
		{
			// Token: 0x06001825 RID: 6181 RVA: 0x00047950 File Offset: 0x00045B50
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((ulong)obj2 != (ulong)obj);
				}
				return 1;
			}

			// Token: 0x06001826 RID: 6182 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualUInt64LiftedToNull()
			{
			}
		}

		// Token: 0x020003BC RID: 956
		private sealed class NotEqualSingleLiftedToNull : NotEqualInstruction
		{
			// Token: 0x06001827 RID: 6183 RVA: 0x00047994 File Offset: 0x00045B94
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((float)obj2 != (float)obj);
				}
				return 1;
			}

			// Token: 0x06001828 RID: 6184 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualSingleLiftedToNull()
			{
			}
		}

		// Token: 0x020003BD RID: 957
		private sealed class NotEqualDoubleLiftedToNull : NotEqualInstruction
		{
			// Token: 0x06001829 RID: 6185 RVA: 0x000479D8 File Offset: 0x00045BD8
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((double)obj2 != (double)obj);
				}
				return 1;
			}

			// Token: 0x0600182A RID: 6186 RVA: 0x0004739E File Offset: 0x0004559E
			public NotEqualDoubleLiftedToNull()
			{
			}
		}
	}
}
