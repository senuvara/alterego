﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200037D RID: 893
	internal struct LocalDefinition
	{
		// Token: 0x06001789 RID: 6025 RVA: 0x00045EE9 File Offset: 0x000440E9
		internal LocalDefinition(int localIndex, ParameterExpression parameter)
		{
			this.Index = localIndex;
			this.Parameter = parameter;
		}

		// Token: 0x17000490 RID: 1168
		// (get) Token: 0x0600178A RID: 6026 RVA: 0x00045EF9 File Offset: 0x000440F9
		public int Index
		{
			[CompilerGenerated]
			get
			{
				return this.<Index>k__BackingField;
			}
		}

		// Token: 0x17000491 RID: 1169
		// (get) Token: 0x0600178B RID: 6027 RVA: 0x00045F01 File Offset: 0x00044101
		public ParameterExpression Parameter
		{
			[CompilerGenerated]
			get
			{
				return this.<Parameter>k__BackingField;
			}
		}

		// Token: 0x0600178C RID: 6028 RVA: 0x00045F0C File Offset: 0x0004410C
		public override bool Equals(object obj)
		{
			if (obj is LocalDefinition)
			{
				LocalDefinition localDefinition = (LocalDefinition)obj;
				return localDefinition.Index == this.Index && localDefinition.Parameter == this.Parameter;
			}
			return false;
		}

		// Token: 0x0600178D RID: 6029 RVA: 0x00045F4C File Offset: 0x0004414C
		public override int GetHashCode()
		{
			if (this.Parameter == null)
			{
				return 0;
			}
			return this.Parameter.GetHashCode() ^ this.Index.GetHashCode();
		}

		// Token: 0x04000B63 RID: 2915
		[CompilerGenerated]
		private readonly int <Index>k__BackingField;

		// Token: 0x04000B64 RID: 2916
		[CompilerGenerated]
		private readonly ParameterExpression <Parameter>k__BackingField;
	}
}
