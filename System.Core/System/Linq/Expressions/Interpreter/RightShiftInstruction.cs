﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020003D7 RID: 983
	internal abstract class RightShiftInstruction : Instruction
	{
		// Token: 0x170004B9 RID: 1209
		// (get) Token: 0x06001877 RID: 6263 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x170004BA RID: 1210
		// (get) Token: 0x06001878 RID: 6264 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004BB RID: 1211
		// (get) Token: 0x06001879 RID: 6265 RVA: 0x00048A39 File Offset: 0x00046C39
		public override string InstructionName
		{
			get
			{
				return "RightShift";
			}
		}

		// Token: 0x0600187A RID: 6266 RVA: 0x00037C51 File Offset: 0x00035E51
		private RightShiftInstruction()
		{
		}

		// Token: 0x0600187B RID: 6267 RVA: 0x00048A40 File Offset: 0x00046C40
		public static Instruction Create(Type type)
		{
			switch (type.GetNonNullableType().GetTypeCode())
			{
			case TypeCode.SByte:
			{
				Instruction result;
				if ((result = RightShiftInstruction.s_SByte) == null)
				{
					result = (RightShiftInstruction.s_SByte = new RightShiftInstruction.RightShiftSByte());
				}
				return result;
			}
			case TypeCode.Byte:
			{
				Instruction result2;
				if ((result2 = RightShiftInstruction.s_Byte) == null)
				{
					result2 = (RightShiftInstruction.s_Byte = new RightShiftInstruction.RightShiftByte());
				}
				return result2;
			}
			case TypeCode.Int16:
			{
				Instruction result3;
				if ((result3 = RightShiftInstruction.s_Int16) == null)
				{
					result3 = (RightShiftInstruction.s_Int16 = new RightShiftInstruction.RightShiftInt16());
				}
				return result3;
			}
			case TypeCode.UInt16:
			{
				Instruction result4;
				if ((result4 = RightShiftInstruction.s_UInt16) == null)
				{
					result4 = (RightShiftInstruction.s_UInt16 = new RightShiftInstruction.RightShiftUInt16());
				}
				return result4;
			}
			case TypeCode.Int32:
			{
				Instruction result5;
				if ((result5 = RightShiftInstruction.s_Int32) == null)
				{
					result5 = (RightShiftInstruction.s_Int32 = new RightShiftInstruction.RightShiftInt32());
				}
				return result5;
			}
			case TypeCode.UInt32:
			{
				Instruction result6;
				if ((result6 = RightShiftInstruction.s_UInt32) == null)
				{
					result6 = (RightShiftInstruction.s_UInt32 = new RightShiftInstruction.RightShiftUInt32());
				}
				return result6;
			}
			case TypeCode.Int64:
			{
				Instruction result7;
				if ((result7 = RightShiftInstruction.s_Int64) == null)
				{
					result7 = (RightShiftInstruction.s_Int64 = new RightShiftInstruction.RightShiftInt64());
				}
				return result7;
			}
			case TypeCode.UInt64:
			{
				Instruction result8;
				if ((result8 = RightShiftInstruction.s_UInt64) == null)
				{
					result8 = (RightShiftInstruction.s_UInt64 = new RightShiftInstruction.RightShiftUInt64());
				}
				return result8;
			}
			default:
				throw ContractUtils.Unreachable;
			}
		}

		// Token: 0x04000BBD RID: 3005
		private static Instruction s_SByte;

		// Token: 0x04000BBE RID: 3006
		private static Instruction s_Int16;

		// Token: 0x04000BBF RID: 3007
		private static Instruction s_Int32;

		// Token: 0x04000BC0 RID: 3008
		private static Instruction s_Int64;

		// Token: 0x04000BC1 RID: 3009
		private static Instruction s_Byte;

		// Token: 0x04000BC2 RID: 3010
		private static Instruction s_UInt16;

		// Token: 0x04000BC3 RID: 3011
		private static Instruction s_UInt32;

		// Token: 0x04000BC4 RID: 3012
		private static Instruction s_UInt64;

		// Token: 0x020003D8 RID: 984
		private sealed class RightShiftSByte : RightShiftInstruction
		{
			// Token: 0x0600187C RID: 6268 RVA: 0x00048B34 File Offset: 0x00046D34
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((sbyte)((sbyte)obj2 >> (int)obj));
				}
				return 1;
			}

			// Token: 0x0600187D RID: 6269 RVA: 0x00048B76 File Offset: 0x00046D76
			public RightShiftSByte()
			{
			}
		}

		// Token: 0x020003D9 RID: 985
		private sealed class RightShiftInt16 : RightShiftInstruction
		{
			// Token: 0x0600187E RID: 6270 RVA: 0x00048B80 File Offset: 0x00046D80
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((short)((short)obj2 >> (int)obj));
				}
				return 1;
			}

			// Token: 0x0600187F RID: 6271 RVA: 0x00048B76 File Offset: 0x00046D76
			public RightShiftInt16()
			{
			}
		}

		// Token: 0x020003DA RID: 986
		private sealed class RightShiftInt32 : RightShiftInstruction
		{
			// Token: 0x06001880 RID: 6272 RVA: 0x00048BC4 File Offset: 0x00046DC4
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((int)obj2 >> (int)obj);
				}
				return 1;
			}

			// Token: 0x06001881 RID: 6273 RVA: 0x00048B76 File Offset: 0x00046D76
			public RightShiftInt32()
			{
			}
		}

		// Token: 0x020003DB RID: 987
		private sealed class RightShiftInt64 : RightShiftInstruction
		{
			// Token: 0x06001882 RID: 6274 RVA: 0x00048C08 File Offset: 0x00046E08
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((long)obj2 >> (int)obj);
				}
				return 1;
			}

			// Token: 0x06001883 RID: 6275 RVA: 0x00048B76 File Offset: 0x00046D76
			public RightShiftInt64()
			{
			}
		}

		// Token: 0x020003DC RID: 988
		private sealed class RightShiftByte : RightShiftInstruction
		{
			// Token: 0x06001884 RID: 6276 RVA: 0x00048C50 File Offset: 0x00046E50
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((byte)((byte)obj2 >> (int)obj));
				}
				return 1;
			}

			// Token: 0x06001885 RID: 6277 RVA: 0x00048B76 File Offset: 0x00046D76
			public RightShiftByte()
			{
			}
		}

		// Token: 0x020003DD RID: 989
		private sealed class RightShiftUInt16 : RightShiftInstruction
		{
			// Token: 0x06001886 RID: 6278 RVA: 0x00048C94 File Offset: 0x00046E94
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((ushort)((ushort)obj2 >> (int)obj));
				}
				return 1;
			}

			// Token: 0x06001887 RID: 6279 RVA: 0x00048B76 File Offset: 0x00046D76
			public RightShiftUInt16()
			{
			}
		}

		// Token: 0x020003DE RID: 990
		private sealed class RightShiftUInt32 : RightShiftInstruction
		{
			// Token: 0x06001888 RID: 6280 RVA: 0x00048CD8 File Offset: 0x00046ED8
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((uint)obj2 >> (int)obj);
				}
				return 1;
			}

			// Token: 0x06001889 RID: 6281 RVA: 0x00048B76 File Offset: 0x00046D76
			public RightShiftUInt32()
			{
			}
		}

		// Token: 0x020003DF RID: 991
		private sealed class RightShiftUInt64 : RightShiftInstruction
		{
			// Token: 0x0600188A RID: 6282 RVA: 0x00048D20 File Offset: 0x00046F20
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((ulong)obj2 >> (int)obj);
				}
				return 1;
			}

			// Token: 0x0600188B RID: 6283 RVA: 0x00048B76 File Offset: 0x00046D76
			public RightShiftUInt64()
			{
			}
		}
	}
}
