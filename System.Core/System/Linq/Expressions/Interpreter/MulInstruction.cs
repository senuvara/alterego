﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000389 RID: 905
	internal abstract class MulInstruction : Instruction
	{
		// Token: 0x17000497 RID: 1175
		// (get) Token: 0x060017AC RID: 6060 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x17000498 RID: 1176
		// (get) Token: 0x060017AD RID: 6061 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000499 RID: 1177
		// (get) Token: 0x060017AE RID: 6062 RVA: 0x00046591 File Offset: 0x00044791
		public override string InstructionName
		{
			get
			{
				return "Mul";
			}
		}

		// Token: 0x060017AF RID: 6063 RVA: 0x00037C51 File Offset: 0x00035E51
		private MulInstruction()
		{
		}

		// Token: 0x060017B0 RID: 6064 RVA: 0x00046598 File Offset: 0x00044798
		public static Instruction Create(Type type)
		{
			switch (type.GetNonNullableType().GetTypeCode())
			{
			case TypeCode.Int16:
			{
				Instruction result;
				if ((result = MulInstruction.s_Int16) == null)
				{
					result = (MulInstruction.s_Int16 = new MulInstruction.MulInt16());
				}
				return result;
			}
			case TypeCode.UInt16:
			{
				Instruction result2;
				if ((result2 = MulInstruction.s_UInt16) == null)
				{
					result2 = (MulInstruction.s_UInt16 = new MulInstruction.MulUInt16());
				}
				return result2;
			}
			case TypeCode.Int32:
			{
				Instruction result3;
				if ((result3 = MulInstruction.s_Int32) == null)
				{
					result3 = (MulInstruction.s_Int32 = new MulInstruction.MulInt32());
				}
				return result3;
			}
			case TypeCode.UInt32:
			{
				Instruction result4;
				if ((result4 = MulInstruction.s_UInt32) == null)
				{
					result4 = (MulInstruction.s_UInt32 = new MulInstruction.MulUInt32());
				}
				return result4;
			}
			case TypeCode.Int64:
			{
				Instruction result5;
				if ((result5 = MulInstruction.s_Int64) == null)
				{
					result5 = (MulInstruction.s_Int64 = new MulInstruction.MulInt64());
				}
				return result5;
			}
			case TypeCode.UInt64:
			{
				Instruction result6;
				if ((result6 = MulInstruction.s_UInt64) == null)
				{
					result6 = (MulInstruction.s_UInt64 = new MulInstruction.MulUInt64());
				}
				return result6;
			}
			case TypeCode.Single:
			{
				Instruction result7;
				if ((result7 = MulInstruction.s_Single) == null)
				{
					result7 = (MulInstruction.s_Single = new MulInstruction.MulSingle());
				}
				return result7;
			}
			case TypeCode.Double:
			{
				Instruction result8;
				if ((result8 = MulInstruction.s_Double) == null)
				{
					result8 = (MulInstruction.s_Double = new MulInstruction.MulDouble());
				}
				return result8;
			}
			default:
				throw ContractUtils.Unreachable;
			}
		}

		// Token: 0x04000B76 RID: 2934
		private static Instruction s_Int16;

		// Token: 0x04000B77 RID: 2935
		private static Instruction s_Int32;

		// Token: 0x04000B78 RID: 2936
		private static Instruction s_Int64;

		// Token: 0x04000B79 RID: 2937
		private static Instruction s_UInt16;

		// Token: 0x04000B7A RID: 2938
		private static Instruction s_UInt32;

		// Token: 0x04000B7B RID: 2939
		private static Instruction s_UInt64;

		// Token: 0x04000B7C RID: 2940
		private static Instruction s_Single;

		// Token: 0x04000B7D RID: 2941
		private static Instruction s_Double;

		// Token: 0x0200038A RID: 906
		private sealed class MulInt16 : MulInstruction
		{
			// Token: 0x060017B1 RID: 6065 RVA: 0x0004668C File Offset: 0x0004488C
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((short)obj * (short)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060017B2 RID: 6066 RVA: 0x000466DE File Offset: 0x000448DE
			public MulInt16()
			{
			}
		}

		// Token: 0x0200038B RID: 907
		private sealed class MulInt32 : MulInstruction
		{
			// Token: 0x060017B3 RID: 6067 RVA: 0x000466E8 File Offset: 0x000448E8
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ScriptingRuntimeHelpers.Int32ToObject((int)obj * (int)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060017B4 RID: 6068 RVA: 0x000466DE File Offset: 0x000448DE
			public MulInt32()
			{
			}
		}

		// Token: 0x0200038C RID: 908
		private sealed class MulInt64 : MulInstruction
		{
			// Token: 0x060017B5 RID: 6069 RVA: 0x0004673C File Offset: 0x0004493C
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((long)obj * (long)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060017B6 RID: 6070 RVA: 0x000466DE File Offset: 0x000448DE
			public MulInt64()
			{
			}
		}

		// Token: 0x0200038D RID: 909
		private sealed class MulUInt16 : MulInstruction
		{
			// Token: 0x060017B7 RID: 6071 RVA: 0x00046790 File Offset: 0x00044990
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((ushort)obj * (ushort)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060017B8 RID: 6072 RVA: 0x000466DE File Offset: 0x000448DE
			public MulUInt16()
			{
			}
		}

		// Token: 0x0200038E RID: 910
		private sealed class MulUInt32 : MulInstruction
		{
			// Token: 0x060017B9 RID: 6073 RVA: 0x000467E4 File Offset: 0x000449E4
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((uint)obj * (uint)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060017BA RID: 6074 RVA: 0x000466DE File Offset: 0x000448DE
			public MulUInt32()
			{
			}
		}

		// Token: 0x0200038F RID: 911
		private sealed class MulUInt64 : MulInstruction
		{
			// Token: 0x060017BB RID: 6075 RVA: 0x00046838 File Offset: 0x00044A38
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((ulong)obj * (ulong)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060017BC RID: 6076 RVA: 0x000466DE File Offset: 0x000448DE
			public MulUInt64()
			{
			}
		}

		// Token: 0x02000390 RID: 912
		private sealed class MulSingle : MulInstruction
		{
			// Token: 0x060017BD RID: 6077 RVA: 0x0004688C File Offset: 0x00044A8C
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((float)obj * (float)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060017BE RID: 6078 RVA: 0x000466DE File Offset: 0x000448DE
			public MulSingle()
			{
			}
		}

		// Token: 0x02000391 RID: 913
		private sealed class MulDouble : MulInstruction
		{
			// Token: 0x060017BF RID: 6079 RVA: 0x000468E0 File Offset: 0x00044AE0
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((double)obj * (double)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060017C0 RID: 6080 RVA: 0x000466DE File Offset: 0x000448DE
			public MulDouble()
			{
			}
		}
	}
}
