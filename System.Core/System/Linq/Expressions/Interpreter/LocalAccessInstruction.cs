﻿using System;
using System.Collections.Generic;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000367 RID: 871
	internal abstract class LocalAccessInstruction : Instruction
	{
		// Token: 0x06001730 RID: 5936 RVA: 0x0004593A File Offset: 0x00043B3A
		protected LocalAccessInstruction(int index)
		{
			this._index = index;
		}

		// Token: 0x06001731 RID: 5937 RVA: 0x0004594C File Offset: 0x00043B4C
		public override string ToDebugString(int instructionIndex, object cookie, Func<int, int> labelIndexer, IReadOnlyList<object> objects)
		{
			if (cookie != null)
			{
				return string.Concat(new object[]
				{
					this.InstructionName,
					"(",
					cookie,
					": ",
					this._index,
					")"
				});
			}
			return string.Concat(new object[]
			{
				this.InstructionName,
				"(",
				this._index,
				")"
			});
		}

		// Token: 0x04000B58 RID: 2904
		internal readonly int _index;
	}
}
