﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000354 RID: 852
	internal sealed class TryFaultHandler
	{
		// Token: 0x06001652 RID: 5714 RVA: 0x0003EA5C File Offset: 0x0003CC5C
		internal TryFaultHandler(int tryStart, int tryEnd, int finallyStart, int finallyEnd)
		{
			this.TryStartIndex = tryStart;
			this.TryEndIndex = tryEnd;
			this.FinallyStartIndex = finallyStart;
			this.FinallyEndIndex = finallyEnd;
		}

		// Token: 0x04000B24 RID: 2852
		internal readonly int TryStartIndex;

		// Token: 0x04000B25 RID: 2853
		internal readonly int TryEndIndex;

		// Token: 0x04000B26 RID: 2854
		internal readonly int FinallyStartIndex;

		// Token: 0x04000B27 RID: 2855
		internal readonly int FinallyEndIndex;
	}
}
