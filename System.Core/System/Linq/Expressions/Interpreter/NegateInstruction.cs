﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000399 RID: 921
	internal abstract class NegateInstruction : Instruction
	{
		// Token: 0x1700049D RID: 1181
		// (get) Token: 0x060017D2 RID: 6098 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x1700049E RID: 1182
		// (get) Token: 0x060017D3 RID: 6099 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x1700049F RID: 1183
		// (get) Token: 0x060017D4 RID: 6100 RVA: 0x00046BF5 File Offset: 0x00044DF5
		public override string InstructionName
		{
			get
			{
				return "Negate";
			}
		}

		// Token: 0x060017D5 RID: 6101 RVA: 0x00037C51 File Offset: 0x00035E51
		private NegateInstruction()
		{
		}

		// Token: 0x060017D6 RID: 6102 RVA: 0x00046BFC File Offset: 0x00044DFC
		public static Instruction Create(Type type)
		{
			switch (type.GetNonNullableType().GetTypeCode())
			{
			case TypeCode.Int16:
			{
				Instruction result;
				if ((result = NegateInstruction.s_Int16) == null)
				{
					result = (NegateInstruction.s_Int16 = new NegateInstruction.NegateInt16());
				}
				return result;
			}
			case TypeCode.Int32:
			{
				Instruction result2;
				if ((result2 = NegateInstruction.s_Int32) == null)
				{
					result2 = (NegateInstruction.s_Int32 = new NegateInstruction.NegateInt32());
				}
				return result2;
			}
			case TypeCode.Int64:
			{
				Instruction result3;
				if ((result3 = NegateInstruction.s_Int64) == null)
				{
					result3 = (NegateInstruction.s_Int64 = new NegateInstruction.NegateInt64());
				}
				return result3;
			}
			case TypeCode.Single:
			{
				Instruction result4;
				if ((result4 = NegateInstruction.s_Single) == null)
				{
					result4 = (NegateInstruction.s_Single = new NegateInstruction.NegateSingle());
				}
				return result4;
			}
			case TypeCode.Double:
			{
				Instruction result5;
				if ((result5 = NegateInstruction.s_Double) == null)
				{
					result5 = (NegateInstruction.s_Double = new NegateInstruction.NegateDouble());
				}
				return result5;
			}
			}
			throw ContractUtils.Unreachable;
		}

		// Token: 0x04000B84 RID: 2948
		private static Instruction s_Int16;

		// Token: 0x04000B85 RID: 2949
		private static Instruction s_Int32;

		// Token: 0x04000B86 RID: 2950
		private static Instruction s_Int64;

		// Token: 0x04000B87 RID: 2951
		private static Instruction s_Single;

		// Token: 0x04000B88 RID: 2952
		private static Instruction s_Double;

		// Token: 0x0200039A RID: 922
		private sealed class NegateInt16 : NegateInstruction
		{
			// Token: 0x060017D7 RID: 6103 RVA: 0x00046CB0 File Offset: 0x00044EB0
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(-(short)obj);
				}
				return 1;
			}

			// Token: 0x060017D8 RID: 6104 RVA: 0x00046CDF File Offset: 0x00044EDF
			public NegateInt16()
			{
			}
		}

		// Token: 0x0200039B RID: 923
		private sealed class NegateInt32 : NegateInstruction
		{
			// Token: 0x060017D9 RID: 6105 RVA: 0x00046CE8 File Offset: 0x00044EE8
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(-(int)obj);
				}
				return 1;
			}

			// Token: 0x060017DA RID: 6106 RVA: 0x00046CDF File Offset: 0x00044EDF
			public NegateInt32()
			{
			}
		}

		// Token: 0x0200039C RID: 924
		private sealed class NegateInt64 : NegateInstruction
		{
			// Token: 0x060017DB RID: 6107 RVA: 0x00046D18 File Offset: 0x00044F18
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(-(long)obj);
				}
				return 1;
			}

			// Token: 0x060017DC RID: 6108 RVA: 0x00046CDF File Offset: 0x00044EDF
			public NegateInt64()
			{
			}
		}

		// Token: 0x0200039D RID: 925
		private sealed class NegateSingle : NegateInstruction
		{
			// Token: 0x060017DD RID: 6109 RVA: 0x00046D4C File Offset: 0x00044F4C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(-(float)obj);
				}
				return 1;
			}

			// Token: 0x060017DE RID: 6110 RVA: 0x00046CDF File Offset: 0x00044EDF
			public NegateSingle()
			{
			}
		}

		// Token: 0x0200039E RID: 926
		private sealed class NegateDouble : NegateInstruction
		{
			// Token: 0x060017DF RID: 6111 RVA: 0x00046D80 File Offset: 0x00044F80
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(-(double)obj);
				}
				return 1;
			}

			// Token: 0x060017E0 RID: 6112 RVA: 0x00046CDF File Offset: 0x00044EDF
			public NegateDouble()
			{
			}
		}
	}
}
