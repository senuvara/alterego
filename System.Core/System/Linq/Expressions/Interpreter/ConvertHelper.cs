﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002AC RID: 684
	internal static class ConvertHelper
	{
		// Token: 0x060013BA RID: 5050 RVA: 0x00038864 File Offset: 0x00036A64
		public static int ToInt32NoNull(object val)
		{
			if (val != null)
			{
				return Convert.ToInt32(val);
			}
			return ((int?)val).Value;
		}
	}
}
