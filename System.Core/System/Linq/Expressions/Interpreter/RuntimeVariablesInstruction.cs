﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200037B RID: 891
	internal sealed class RuntimeVariablesInstruction : Instruction
	{
		// Token: 0x0600177F RID: 6015 RVA: 0x00045DEF File Offset: 0x00043FEF
		public RuntimeVariablesInstruction(int count)
		{
			this._count = count;
		}

		// Token: 0x1700048B RID: 1163
		// (get) Token: 0x06001780 RID: 6016 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x1700048C RID: 1164
		// (get) Token: 0x06001781 RID: 6017 RVA: 0x00045DFE File Offset: 0x00043FFE
		public override int ConsumedStack
		{
			get
			{
				return this._count;
			}
		}

		// Token: 0x1700048D RID: 1165
		// (get) Token: 0x06001782 RID: 6018 RVA: 0x00045E06 File Offset: 0x00044006
		public override string InstructionName
		{
			get
			{
				return "GetRuntimeVariables";
			}
		}

		// Token: 0x06001783 RID: 6019 RVA: 0x00045E10 File Offset: 0x00044010
		public override int Run(InterpretedFrame frame)
		{
			IStrongBox[] array = new IStrongBox[this._count];
			for (int i = array.Length - 1; i >= 0; i--)
			{
				array[i] = (IStrongBox)frame.Pop();
			}
			frame.Push(RuntimeVariables.Create(array));
			return 1;
		}

		// Token: 0x04000B5E RID: 2910
		private readonly int _count;
	}
}
