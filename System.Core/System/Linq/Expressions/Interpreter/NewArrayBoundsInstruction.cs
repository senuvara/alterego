﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002A8 RID: 680
	internal sealed class NewArrayBoundsInstruction : Instruction
	{
		// Token: 0x060013A4 RID: 5028 RVA: 0x00038714 File Offset: 0x00036914
		internal NewArrayBoundsInstruction(Type elementType, int rank)
		{
			this._elementType = elementType;
			this._rank = rank;
		}

		// Token: 0x170003DC RID: 988
		// (get) Token: 0x060013A5 RID: 5029 RVA: 0x0003872A File Offset: 0x0003692A
		public override int ConsumedStack
		{
			get
			{
				return this._rank;
			}
		}

		// Token: 0x170003DD RID: 989
		// (get) Token: 0x060013A6 RID: 5030 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170003DE RID: 990
		// (get) Token: 0x060013A7 RID: 5031 RVA: 0x00038732 File Offset: 0x00036932
		public override string InstructionName
		{
			get
			{
				return "NewArrayBounds";
			}
		}

		// Token: 0x060013A8 RID: 5032 RVA: 0x0003873C File Offset: 0x0003693C
		public override int Run(InterpretedFrame frame)
		{
			int[] array = new int[this._rank];
			for (int i = this._rank - 1; i >= 0; i--)
			{
				int num = ConvertHelper.ToInt32NoNull(frame.Pop());
				if (num < 0)
				{
					throw new OverflowException();
				}
				array[i] = num;
			}
			Array value = Array.CreateInstance(this._elementType, array);
			frame.Push(value);
			return 1;
		}

		// Token: 0x040009E4 RID: 2532
		private readonly Type _elementType;

		// Token: 0x040009E5 RID: 2533
		private readonly int _rank;
	}
}
