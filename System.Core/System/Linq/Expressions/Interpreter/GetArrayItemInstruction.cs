﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002A9 RID: 681
	internal sealed class GetArrayItemInstruction : Instruction
	{
		// Token: 0x060013A9 RID: 5033 RVA: 0x00037C51 File Offset: 0x00035E51
		private GetArrayItemInstruction()
		{
		}

		// Token: 0x170003DF RID: 991
		// (get) Token: 0x060013AA RID: 5034 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x170003E0 RID: 992
		// (get) Token: 0x060013AB RID: 5035 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170003E1 RID: 993
		// (get) Token: 0x060013AC RID: 5036 RVA: 0x00038797 File Offset: 0x00036997
		public override string InstructionName
		{
			get
			{
				return "GetArrayItem";
			}
		}

		// Token: 0x060013AD RID: 5037 RVA: 0x000387A0 File Offset: 0x000369A0
		public override int Run(InterpretedFrame frame)
		{
			int index = ConvertHelper.ToInt32NoNull(frame.Pop());
			Array array = (Array)frame.Pop();
			frame.Push(array.GetValue(index));
			return 1;
		}

		// Token: 0x060013AE RID: 5038 RVA: 0x000387D3 File Offset: 0x000369D3
		// Note: this type is marked as 'beforefieldinit'.
		static GetArrayItemInstruction()
		{
		}

		// Token: 0x040009E6 RID: 2534
		internal static readonly GetArrayItemInstruction Instance = new GetArrayItemInstruction();
	}
}
