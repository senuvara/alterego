﻿using System;
using System.Collections.Generic;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002B7 RID: 695
	internal abstract class IndexedBranchInstruction : Instruction
	{
		// Token: 0x060013F7 RID: 5111 RVA: 0x0003914E File Offset: 0x0003734E
		public IndexedBranchInstruction(int labelIndex)
		{
			this._labelIndex = labelIndex;
		}

		// Token: 0x060013F8 RID: 5112 RVA: 0x0003915D File Offset: 0x0003735D
		public RuntimeLabel GetLabel(InterpretedFrame frame)
		{
			return frame.Interpreter._labels[this._labelIndex];
		}

		// Token: 0x060013F9 RID: 5113 RVA: 0x00039178 File Offset: 0x00037378
		public override string ToDebugString(int instructionIndex, object cookie, Func<int, int> labelIndexer, IReadOnlyList<object> objects)
		{
			int num = labelIndexer(this._labelIndex);
			return this.ToString() + ((num != int.MinValue) ? (" -> " + num) : "");
		}

		// Token: 0x060013FA RID: 5114 RVA: 0x000391BC File Offset: 0x000373BC
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				this.InstructionName,
				"[",
				this._labelIndex,
				"]"
			});
		}

		// Token: 0x040009FF RID: 2559
		protected const int CacheSize = 32;

		// Token: 0x04000A00 RID: 2560
		internal readonly int _labelIndex;
	}
}
