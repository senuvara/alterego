﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002AA RID: 682
	internal sealed class SetArrayItemInstruction : Instruction
	{
		// Token: 0x060013AF RID: 5039 RVA: 0x00037C51 File Offset: 0x00035E51
		private SetArrayItemInstruction()
		{
		}

		// Token: 0x170003E2 RID: 994
		// (get) Token: 0x060013B0 RID: 5040 RVA: 0x0002EF32 File Offset: 0x0002D132
		public override int ConsumedStack
		{
			get
			{
				return 3;
			}
		}

		// Token: 0x170003E3 RID: 995
		// (get) Token: 0x060013B1 RID: 5041 RVA: 0x000387DF File Offset: 0x000369DF
		public override string InstructionName
		{
			get
			{
				return "SetArrayItem";
			}
		}

		// Token: 0x060013B2 RID: 5042 RVA: 0x000387E8 File Offset: 0x000369E8
		public override int Run(InterpretedFrame frame)
		{
			object value = frame.Pop();
			int index = ConvertHelper.ToInt32NoNull(frame.Pop());
			((Array)frame.Pop()).SetValue(value, index);
			return 1;
		}

		// Token: 0x060013B3 RID: 5043 RVA: 0x0003881B File Offset: 0x00036A1B
		// Note: this type is marked as 'beforefieldinit'.
		static SetArrayItemInstruction()
		{
		}

		// Token: 0x040009E7 RID: 2535
		internal static readonly SetArrayItemInstruction Instance = new SetArrayItemInstruction();
	}
}
