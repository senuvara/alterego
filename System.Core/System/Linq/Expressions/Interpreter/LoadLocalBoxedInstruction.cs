﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000369 RID: 873
	internal sealed class LoadLocalBoxedInstruction : LocalAccessInstruction
	{
		// Token: 0x06001737 RID: 5943 RVA: 0x000459CD File Offset: 0x00043BCD
		internal LoadLocalBoxedInstruction(int index) : base(index)
		{
		}

		// Token: 0x1700046D RID: 1133
		// (get) Token: 0x06001738 RID: 5944 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x1700046E RID: 1134
		// (get) Token: 0x06001739 RID: 5945 RVA: 0x00045A26 File Offset: 0x00043C26
		public override string InstructionName
		{
			get
			{
				return "LoadLocalBox";
			}
		}

		// Token: 0x0600173A RID: 5946 RVA: 0x00045A30 File Offset: 0x00043C30
		public override int Run(InterpretedFrame frame)
		{
			IStrongBox strongBox = (IStrongBox)frame.Data[this._index];
			object[] data = frame.Data;
			int stackIndex = frame.StackIndex;
			frame.StackIndex = stackIndex + 1;
			data[stackIndex] = strongBox.Value;
			return 1;
		}
	}
}
