﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002BA RID: 698
	internal sealed class EnterTryFaultInstruction : IndexedBranchInstruction
	{
		// Token: 0x0600140B RID: 5131 RVA: 0x00039573 File Offset: 0x00037773
		internal EnterTryFaultInstruction(int targetIndex) : base(targetIndex)
		{
		}

		// Token: 0x17000405 RID: 1029
		// (get) Token: 0x0600140C RID: 5132 RVA: 0x0003957C File Offset: 0x0003777C
		public override string InstructionName
		{
			get
			{
				return "EnterTryFault";
			}
		}

		// Token: 0x17000406 RID: 1030
		// (get) Token: 0x0600140D RID: 5133 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedContinuations
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000407 RID: 1031
		// (get) Token: 0x0600140E RID: 5134 RVA: 0x00039583 File Offset: 0x00037783
		internal TryFaultHandler Handler
		{
			get
			{
				return this._tryHandler;
			}
		}

		// Token: 0x0600140F RID: 5135 RVA: 0x0003958B File Offset: 0x0003778B
		internal void SetTryHandler(TryFaultHandler tryHandler)
		{
			this._tryHandler = tryHandler;
		}

		// Token: 0x06001410 RID: 5136 RVA: 0x00039594 File Offset: 0x00037794
		public override int Run(InterpretedFrame frame)
		{
			frame.PushContinuation(this._labelIndex);
			int instructionIndex = frame.InstructionIndex;
			frame.InstructionIndex++;
			Instruction[] instructions = frame.Interpreter.Instructions.Instructions;
			bool flag = false;
			try
			{
				int num = frame.InstructionIndex;
				while (num >= this._tryHandler.TryStartIndex && num < this._tryHandler.TryEndIndex)
				{
					num += instructions[num].Run(frame);
					frame.InstructionIndex = num;
				}
				flag = true;
				frame.RemoveContinuation();
				frame.InstructionIndex += instructions[num].Run(frame);
			}
			finally
			{
				if (!flag)
				{
					int num2 = frame.InstructionIndex = this._tryHandler.FinallyStartIndex;
					while (num2 >= this._tryHandler.FinallyStartIndex && num2 < this._tryHandler.FinallyEndIndex)
					{
						num2 += instructions[num2].Run(frame);
						frame.InstructionIndex = num2;
					}
				}
			}
			return frame.InstructionIndex - instructionIndex;
		}

		// Token: 0x04000A08 RID: 2568
		private TryFaultHandler _tryHandler;
	}
}
