﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000353 RID: 851
	internal sealed class TryCatchFinallyHandler
	{
		// Token: 0x17000465 RID: 1125
		// (get) Token: 0x0600164B RID: 5707 RVA: 0x0003E895 File Offset: 0x0003CA95
		internal bool IsFinallyBlockExist
		{
			get
			{
				return this.FinallyStartIndex != int.MaxValue;
			}
		}

		// Token: 0x17000466 RID: 1126
		// (get) Token: 0x0600164C RID: 5708 RVA: 0x0003E8A7 File Offset: 0x0003CAA7
		internal ExceptionHandler[] Handlers
		{
			get
			{
				return this._handlers;
			}
		}

		// Token: 0x17000467 RID: 1127
		// (get) Token: 0x0600164D RID: 5709 RVA: 0x0003E8AF File Offset: 0x0003CAAF
		internal bool IsCatchBlockExist
		{
			get
			{
				return this._handlers != null;
			}
		}

		// Token: 0x0600164E RID: 5710 RVA: 0x0003E8BA File Offset: 0x0003CABA
		internal TryCatchFinallyHandler(int tryStart, int tryEnd, int gotoEndTargetIndex, ExceptionHandler[] handlers) : this(tryStart, tryEnd, gotoEndTargetIndex, int.MaxValue, int.MaxValue, handlers)
		{
		}

		// Token: 0x0600164F RID: 5711 RVA: 0x0003E8D1 File Offset: 0x0003CAD1
		internal TryCatchFinallyHandler(int tryStart, int tryEnd, int gotoEndLabelIndex, int finallyStart, int finallyEnd, ExceptionHandler[] handlers)
		{
			this.TryStartIndex = tryStart;
			this.TryEndIndex = tryEnd;
			this.FinallyStartIndex = finallyStart;
			this.FinallyEndIndex = finallyEnd;
			this.GotoEndTargetIndex = gotoEndLabelIndex;
			this._handlers = handlers;
		}

		// Token: 0x06001650 RID: 5712 RVA: 0x0003E908 File Offset: 0x0003CB08
		internal bool HasHandler(InterpretedFrame frame, Exception exception, out ExceptionHandler handler, out object unwrappedException)
		{
			frame.SaveTraceToException(exception);
			if (this.IsCatchBlockExist)
			{
				RuntimeWrappedException ex = exception as RuntimeWrappedException;
				unwrappedException = ((ex != null) ? ex.WrappedException : exception);
				Type type = unwrappedException.GetType();
				foreach (ExceptionHandler exceptionHandler in this._handlers)
				{
					if (exceptionHandler.Matches(type) && (exceptionHandler.Filter == null || TryCatchFinallyHandler.FilterPasses(frame, ref unwrappedException, exceptionHandler.Filter)))
					{
						handler = exceptionHandler;
						return true;
					}
				}
			}
			else
			{
				unwrappedException = null;
			}
			handler = null;
			return false;
		}

		// Token: 0x06001651 RID: 5713 RVA: 0x0003E994 File Offset: 0x0003CB94
		private static bool FilterPasses(InterpretedFrame frame, ref object exception, ExceptionFilter filter)
		{
			Interpreter interpreter = frame.Interpreter;
			Instruction[] instructions = interpreter.Instructions.Instructions;
			int stackIndex = frame.StackIndex;
			int instructionIndex = frame.InstructionIndex;
			try
			{
				int num = interpreter._labels[filter.LabelIndex].Index;
				frame.InstructionIndex = num;
				frame.Push(exception);
				while (num >= filter.StartIndex && num < filter.EndIndex)
				{
					num += instructions[num].Run(frame);
					frame.InstructionIndex = num;
				}
				object obj = frame.Pop();
				if ((bool)frame.Pop())
				{
					exception = obj;
					return true;
				}
			}
			catch
			{
			}
			frame.StackIndex = stackIndex;
			frame.InstructionIndex = instructionIndex;
			return false;
		}

		// Token: 0x04000B1E RID: 2846
		internal readonly int TryStartIndex;

		// Token: 0x04000B1F RID: 2847
		internal readonly int TryEndIndex;

		// Token: 0x04000B20 RID: 2848
		internal readonly int FinallyStartIndex;

		// Token: 0x04000B21 RID: 2849
		internal readonly int FinallyEndIndex;

		// Token: 0x04000B22 RID: 2850
		internal readonly int GotoEndTargetIndex;

		// Token: 0x04000B23 RID: 2851
		private readonly ExceptionHandler[] _handlers;
	}
}
