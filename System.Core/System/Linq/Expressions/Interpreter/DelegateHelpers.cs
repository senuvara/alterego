﻿using System;
using System.Dynamic.Utils;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200040A RID: 1034
	internal static class DelegateHelpers
	{
		// Token: 0x06001918 RID: 6424 RVA: 0x0004A090 File Offset: 0x00048290
		internal static Type MakeDelegate(Type[] types)
		{
			if (types.Length <= 17)
			{
				if (!types.Any((Type t) => t.IsByRef))
				{
					if (types[types.Length - 1] == typeof(void))
					{
						Array.Resize<Type>(ref types, types.Length - 1);
						switch (types.Length)
						{
						case 0:
							return typeof(Action);
						case 1:
							return typeof(Action<>).MakeGenericType(types);
						case 2:
							return typeof(Action<, >).MakeGenericType(types);
						case 3:
							return typeof(Action<, , >).MakeGenericType(types);
						case 4:
							return typeof(Action<, , , >).MakeGenericType(types);
						case 5:
							return typeof(Action<, , , , >).MakeGenericType(types);
						case 6:
							return typeof(Action<, , , , , >).MakeGenericType(types);
						case 7:
							return typeof(Action<, , , , , , >).MakeGenericType(types);
						case 8:
							return typeof(Action<, , , , , , , >).MakeGenericType(types);
						case 9:
							return typeof(Action<, , , , , , , , >).MakeGenericType(types);
						case 10:
							return typeof(Action<, , , , , , , , , >).MakeGenericType(types);
						case 11:
							return typeof(Action<, , , , , , , , , , >).MakeGenericType(types);
						case 12:
							return typeof(Action<, , , , , , , , , , , >).MakeGenericType(types);
						case 13:
							return typeof(Action<, , , , , , , , , , , , >).MakeGenericType(types);
						case 14:
							return typeof(Action<, , , , , , , , , , , , , >).MakeGenericType(types);
						case 15:
							return typeof(Action<, , , , , , , , , , , , , , >).MakeGenericType(types);
						case 16:
							return typeof(Action<, , , , , , , , , , , , , , , >).MakeGenericType(types);
						}
					}
					else
					{
						switch (types.Length)
						{
						case 1:
							return typeof(Func<>).MakeGenericType(types);
						case 2:
							return typeof(Func<, >).MakeGenericType(types);
						case 3:
							return typeof(Func<, , >).MakeGenericType(types);
						case 4:
							return typeof(Func<, , , >).MakeGenericType(types);
						case 5:
							return typeof(Func<, , , , >).MakeGenericType(types);
						case 6:
							return typeof(Func<, , , , , >).MakeGenericType(types);
						case 7:
							return typeof(Func<, , , , , , >).MakeGenericType(types);
						case 8:
							return typeof(Func<, , , , , , , >).MakeGenericType(types);
						case 9:
							return typeof(Func<, , , , , , , , >).MakeGenericType(types);
						case 10:
							return typeof(Func<, , , , , , , , , >).MakeGenericType(types);
						case 11:
							return typeof(Func<, , , , , , , , , , >).MakeGenericType(types);
						case 12:
							return typeof(Func<, , , , , , , , , , , >).MakeGenericType(types);
						case 13:
							return typeof(Func<, , , , , , , , , , , , >).MakeGenericType(types);
						case 14:
							return typeof(Func<, , , , , , , , , , , , , >).MakeGenericType(types);
						case 15:
							return typeof(Func<, , , , , , , , , , , , , , >).MakeGenericType(types);
						case 16:
							return typeof(Func<, , , , , , , , , , , , , , , >).MakeGenericType(types);
						case 17:
							return typeof(Func<, , , , , , , , , , , , , , , , >).MakeGenericType(types);
						}
					}
					throw ContractUtils.Unreachable;
				}
			}
			throw ContractUtils.Unreachable;
		}

		// Token: 0x04000BFA RID: 3066
		private const int MaximumArity = 17;

		// Token: 0x0200040B RID: 1035
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06001919 RID: 6425 RVA: 0x0004A3E1 File Offset: 0x000485E1
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x0600191A RID: 6426 RVA: 0x00002310 File Offset: 0x00000510
			public <>c()
			{
			}

			// Token: 0x0600191B RID: 6427 RVA: 0x0004A3ED File Offset: 0x000485ED
			internal bool <MakeDelegate>b__1_0(Type t)
			{
				return t.IsByRef;
			}

			// Token: 0x04000BFB RID: 3067
			public static readonly DelegateHelpers.<>c <>9 = new DelegateHelpers.<>c();

			// Token: 0x04000BFC RID: 3068
			public static Func<Type, bool> <>9__1_0;
		}
	}
}
