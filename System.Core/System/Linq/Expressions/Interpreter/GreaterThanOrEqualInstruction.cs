﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200030E RID: 782
	internal abstract class GreaterThanOrEqualInstruction : Instruction
	{
		// Token: 0x1700043D RID: 1085
		// (get) Token: 0x060014FB RID: 5371 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x1700043E RID: 1086
		// (get) Token: 0x060014FC RID: 5372 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x1700043F RID: 1087
		// (get) Token: 0x060014FD RID: 5373 RVA: 0x0003B448 File Offset: 0x00039648
		public override string InstructionName
		{
			get
			{
				return "GreaterThanOrEqual";
			}
		}

		// Token: 0x060014FE RID: 5374 RVA: 0x0003B44F File Offset: 0x0003964F
		private GreaterThanOrEqualInstruction(object nullValue)
		{
			this._nullValue = nullValue;
		}

		// Token: 0x060014FF RID: 5375 RVA: 0x0003B460 File Offset: 0x00039660
		public static Instruction Create(Type type, bool liftedToNull = false)
		{
			if (liftedToNull)
			{
				switch (type.GetNonNullableType().GetTypeCode())
				{
				case TypeCode.Char:
				{
					Instruction result;
					if ((result = GreaterThanOrEqualInstruction.s_liftedToNullChar) == null)
					{
						result = (GreaterThanOrEqualInstruction.s_liftedToNullChar = new GreaterThanOrEqualInstruction.GreaterThanOrEqualChar(null));
					}
					return result;
				}
				case TypeCode.SByte:
				{
					Instruction result2;
					if ((result2 = GreaterThanOrEqualInstruction.s_liftedToNullSByte) == null)
					{
						result2 = (GreaterThanOrEqualInstruction.s_liftedToNullSByte = new GreaterThanOrEqualInstruction.GreaterThanOrEqualSByte(null));
					}
					return result2;
				}
				case TypeCode.Byte:
				{
					Instruction result3;
					if ((result3 = GreaterThanOrEqualInstruction.s_liftedToNullByte) == null)
					{
						result3 = (GreaterThanOrEqualInstruction.s_liftedToNullByte = new GreaterThanOrEqualInstruction.GreaterThanOrEqualByte(null));
					}
					return result3;
				}
				case TypeCode.Int16:
				{
					Instruction result4;
					if ((result4 = GreaterThanOrEqualInstruction.s_liftedToNullInt16) == null)
					{
						result4 = (GreaterThanOrEqualInstruction.s_liftedToNullInt16 = new GreaterThanOrEqualInstruction.GreaterThanOrEqualInt16(null));
					}
					return result4;
				}
				case TypeCode.UInt16:
				{
					Instruction result5;
					if ((result5 = GreaterThanOrEqualInstruction.s_liftedToNullUInt16) == null)
					{
						result5 = (GreaterThanOrEqualInstruction.s_liftedToNullUInt16 = new GreaterThanOrEqualInstruction.GreaterThanOrEqualUInt16(null));
					}
					return result5;
				}
				case TypeCode.Int32:
				{
					Instruction result6;
					if ((result6 = GreaterThanOrEqualInstruction.s_liftedToNullInt32) == null)
					{
						result6 = (GreaterThanOrEqualInstruction.s_liftedToNullInt32 = new GreaterThanOrEqualInstruction.GreaterThanOrEqualInt32(null));
					}
					return result6;
				}
				case TypeCode.UInt32:
				{
					Instruction result7;
					if ((result7 = GreaterThanOrEqualInstruction.s_liftedToNullUInt32) == null)
					{
						result7 = (GreaterThanOrEqualInstruction.s_liftedToNullUInt32 = new GreaterThanOrEqualInstruction.GreaterThanOrEqualUInt32(null));
					}
					return result7;
				}
				case TypeCode.Int64:
				{
					Instruction result8;
					if ((result8 = GreaterThanOrEqualInstruction.s_liftedToNullInt64) == null)
					{
						result8 = (GreaterThanOrEqualInstruction.s_liftedToNullInt64 = new GreaterThanOrEqualInstruction.GreaterThanOrEqualInt64(null));
					}
					return result8;
				}
				case TypeCode.UInt64:
				{
					Instruction result9;
					if ((result9 = GreaterThanOrEqualInstruction.s_liftedToNullUInt64) == null)
					{
						result9 = (GreaterThanOrEqualInstruction.s_liftedToNullUInt64 = new GreaterThanOrEqualInstruction.GreaterThanOrEqualUInt64(null));
					}
					return result9;
				}
				case TypeCode.Single:
				{
					Instruction result10;
					if ((result10 = GreaterThanOrEqualInstruction.s_liftedToNullSingle) == null)
					{
						result10 = (GreaterThanOrEqualInstruction.s_liftedToNullSingle = new GreaterThanOrEqualInstruction.GreaterThanOrEqualSingle(null));
					}
					return result10;
				}
				case TypeCode.Double:
				{
					Instruction result11;
					if ((result11 = GreaterThanOrEqualInstruction.s_liftedToNullDouble) == null)
					{
						result11 = (GreaterThanOrEqualInstruction.s_liftedToNullDouble = new GreaterThanOrEqualInstruction.GreaterThanOrEqualDouble(null));
					}
					return result11;
				}
				default:
					throw ContractUtils.Unreachable;
				}
			}
			else
			{
				switch (type.GetNonNullableType().GetTypeCode())
				{
				case TypeCode.Char:
				{
					Instruction result12;
					if ((result12 = GreaterThanOrEqualInstruction.s_Char) == null)
					{
						result12 = (GreaterThanOrEqualInstruction.s_Char = new GreaterThanOrEqualInstruction.GreaterThanOrEqualChar(Utils.BoxedFalse));
					}
					return result12;
				}
				case TypeCode.SByte:
				{
					Instruction result13;
					if ((result13 = GreaterThanOrEqualInstruction.s_SByte) == null)
					{
						result13 = (GreaterThanOrEqualInstruction.s_SByte = new GreaterThanOrEqualInstruction.GreaterThanOrEqualSByte(Utils.BoxedFalse));
					}
					return result13;
				}
				case TypeCode.Byte:
				{
					Instruction result14;
					if ((result14 = GreaterThanOrEqualInstruction.s_Byte) == null)
					{
						result14 = (GreaterThanOrEqualInstruction.s_Byte = new GreaterThanOrEqualInstruction.GreaterThanOrEqualByte(Utils.BoxedFalse));
					}
					return result14;
				}
				case TypeCode.Int16:
				{
					Instruction result15;
					if ((result15 = GreaterThanOrEqualInstruction.s_Int16) == null)
					{
						result15 = (GreaterThanOrEqualInstruction.s_Int16 = new GreaterThanOrEqualInstruction.GreaterThanOrEqualInt16(Utils.BoxedFalse));
					}
					return result15;
				}
				case TypeCode.UInt16:
				{
					Instruction result16;
					if ((result16 = GreaterThanOrEqualInstruction.s_UInt16) == null)
					{
						result16 = (GreaterThanOrEqualInstruction.s_UInt16 = new GreaterThanOrEqualInstruction.GreaterThanOrEqualUInt16(Utils.BoxedFalse));
					}
					return result16;
				}
				case TypeCode.Int32:
				{
					Instruction result17;
					if ((result17 = GreaterThanOrEqualInstruction.s_Int32) == null)
					{
						result17 = (GreaterThanOrEqualInstruction.s_Int32 = new GreaterThanOrEqualInstruction.GreaterThanOrEqualInt32(Utils.BoxedFalse));
					}
					return result17;
				}
				case TypeCode.UInt32:
				{
					Instruction result18;
					if ((result18 = GreaterThanOrEqualInstruction.s_UInt32) == null)
					{
						result18 = (GreaterThanOrEqualInstruction.s_UInt32 = new GreaterThanOrEqualInstruction.GreaterThanOrEqualUInt32(Utils.BoxedFalse));
					}
					return result18;
				}
				case TypeCode.Int64:
				{
					Instruction result19;
					if ((result19 = GreaterThanOrEqualInstruction.s_Int64) == null)
					{
						result19 = (GreaterThanOrEqualInstruction.s_Int64 = new GreaterThanOrEqualInstruction.GreaterThanOrEqualInt64(Utils.BoxedFalse));
					}
					return result19;
				}
				case TypeCode.UInt64:
				{
					Instruction result20;
					if ((result20 = GreaterThanOrEqualInstruction.s_UInt64) == null)
					{
						result20 = (GreaterThanOrEqualInstruction.s_UInt64 = new GreaterThanOrEqualInstruction.GreaterThanOrEqualUInt64(Utils.BoxedFalse));
					}
					return result20;
				}
				case TypeCode.Single:
				{
					Instruction result21;
					if ((result21 = GreaterThanOrEqualInstruction.s_Single) == null)
					{
						result21 = (GreaterThanOrEqualInstruction.s_Single = new GreaterThanOrEqualInstruction.GreaterThanOrEqualSingle(Utils.BoxedFalse));
					}
					return result21;
				}
				case TypeCode.Double:
				{
					Instruction result22;
					if ((result22 = GreaterThanOrEqualInstruction.s_Double) == null)
					{
						result22 = (GreaterThanOrEqualInstruction.s_Double = new GreaterThanOrEqualInstruction.GreaterThanOrEqualDouble(Utils.BoxedFalse));
					}
					return result22;
				}
				default:
					throw ContractUtils.Unreachable;
				}
			}
		}

		// Token: 0x04000A69 RID: 2665
		private readonly object _nullValue;

		// Token: 0x04000A6A RID: 2666
		private static Instruction s_SByte;

		// Token: 0x04000A6B RID: 2667
		private static Instruction s_Int16;

		// Token: 0x04000A6C RID: 2668
		private static Instruction s_Char;

		// Token: 0x04000A6D RID: 2669
		private static Instruction s_Int32;

		// Token: 0x04000A6E RID: 2670
		private static Instruction s_Int64;

		// Token: 0x04000A6F RID: 2671
		private static Instruction s_Byte;

		// Token: 0x04000A70 RID: 2672
		private static Instruction s_UInt16;

		// Token: 0x04000A71 RID: 2673
		private static Instruction s_UInt32;

		// Token: 0x04000A72 RID: 2674
		private static Instruction s_UInt64;

		// Token: 0x04000A73 RID: 2675
		private static Instruction s_Single;

		// Token: 0x04000A74 RID: 2676
		private static Instruction s_Double;

		// Token: 0x04000A75 RID: 2677
		private static Instruction s_liftedToNullSByte;

		// Token: 0x04000A76 RID: 2678
		private static Instruction s_liftedToNullInt16;

		// Token: 0x04000A77 RID: 2679
		private static Instruction s_liftedToNullChar;

		// Token: 0x04000A78 RID: 2680
		private static Instruction s_liftedToNullInt32;

		// Token: 0x04000A79 RID: 2681
		private static Instruction s_liftedToNullInt64;

		// Token: 0x04000A7A RID: 2682
		private static Instruction s_liftedToNullByte;

		// Token: 0x04000A7B RID: 2683
		private static Instruction s_liftedToNullUInt16;

		// Token: 0x04000A7C RID: 2684
		private static Instruction s_liftedToNullUInt32;

		// Token: 0x04000A7D RID: 2685
		private static Instruction s_liftedToNullUInt64;

		// Token: 0x04000A7E RID: 2686
		private static Instruction s_liftedToNullSingle;

		// Token: 0x04000A7F RID: 2687
		private static Instruction s_liftedToNullDouble;

		// Token: 0x0200030F RID: 783
		private sealed class GreaterThanOrEqualSByte : GreaterThanOrEqualInstruction
		{
			// Token: 0x06001500 RID: 5376 RVA: 0x0003B718 File Offset: 0x00039918
			public GreaterThanOrEqualSByte(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x06001501 RID: 5377 RVA: 0x0003B724 File Offset: 0x00039924
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((sbyte)obj2 >= (sbyte)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000310 RID: 784
		private sealed class GreaterThanOrEqualInt16 : GreaterThanOrEqualInstruction
		{
			// Token: 0x06001502 RID: 5378 RVA: 0x0003B718 File Offset: 0x00039918
			public GreaterThanOrEqualInt16(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x06001503 RID: 5379 RVA: 0x0003B76C File Offset: 0x0003996C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((short)obj2 >= (short)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000311 RID: 785
		private sealed class GreaterThanOrEqualChar : GreaterThanOrEqualInstruction
		{
			// Token: 0x06001504 RID: 5380 RVA: 0x0003B718 File Offset: 0x00039918
			public GreaterThanOrEqualChar(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x06001505 RID: 5381 RVA: 0x0003B7B4 File Offset: 0x000399B4
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((char)obj2 >= (char)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000312 RID: 786
		private sealed class GreaterThanOrEqualInt32 : GreaterThanOrEqualInstruction
		{
			// Token: 0x06001506 RID: 5382 RVA: 0x0003B718 File Offset: 0x00039918
			public GreaterThanOrEqualInt32(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x06001507 RID: 5383 RVA: 0x0003B7FC File Offset: 0x000399FC
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((int)obj2 >= (int)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000313 RID: 787
		private sealed class GreaterThanOrEqualInt64 : GreaterThanOrEqualInstruction
		{
			// Token: 0x06001508 RID: 5384 RVA: 0x0003B718 File Offset: 0x00039918
			public GreaterThanOrEqualInt64(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x06001509 RID: 5385 RVA: 0x0003B844 File Offset: 0x00039A44
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((long)obj2 >= (long)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000314 RID: 788
		private sealed class GreaterThanOrEqualByte : GreaterThanOrEqualInstruction
		{
			// Token: 0x0600150A RID: 5386 RVA: 0x0003B718 File Offset: 0x00039918
			public GreaterThanOrEqualByte(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x0600150B RID: 5387 RVA: 0x0003B88C File Offset: 0x00039A8C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((byte)obj2 >= (byte)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000315 RID: 789
		private sealed class GreaterThanOrEqualUInt16 : GreaterThanOrEqualInstruction
		{
			// Token: 0x0600150C RID: 5388 RVA: 0x0003B718 File Offset: 0x00039918
			public GreaterThanOrEqualUInt16(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x0600150D RID: 5389 RVA: 0x0003B8D4 File Offset: 0x00039AD4
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((ushort)obj2 >= (ushort)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000316 RID: 790
		private sealed class GreaterThanOrEqualUInt32 : GreaterThanOrEqualInstruction
		{
			// Token: 0x0600150E RID: 5390 RVA: 0x0003B718 File Offset: 0x00039918
			public GreaterThanOrEqualUInt32(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x0600150F RID: 5391 RVA: 0x0003B91C File Offset: 0x00039B1C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((uint)obj2 >= (uint)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000317 RID: 791
		private sealed class GreaterThanOrEqualUInt64 : GreaterThanOrEqualInstruction
		{
			// Token: 0x06001510 RID: 5392 RVA: 0x0003B718 File Offset: 0x00039918
			public GreaterThanOrEqualUInt64(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x06001511 RID: 5393 RVA: 0x0003B964 File Offset: 0x00039B64
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((ulong)obj2 >= (ulong)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000318 RID: 792
		private sealed class GreaterThanOrEqualSingle : GreaterThanOrEqualInstruction
		{
			// Token: 0x06001512 RID: 5394 RVA: 0x0003B718 File Offset: 0x00039918
			public GreaterThanOrEqualSingle(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x06001513 RID: 5395 RVA: 0x0003B9AC File Offset: 0x00039BAC
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((float)obj2 >= (float)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000319 RID: 793
		private sealed class GreaterThanOrEqualDouble : GreaterThanOrEqualInstruction
		{
			// Token: 0x06001514 RID: 5396 RVA: 0x0003B718 File Offset: 0x00039918
			public GreaterThanOrEqualDouble(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x06001515 RID: 5397 RVA: 0x0003B9F4 File Offset: 0x00039BF4
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((double)obj2 >= (double)obj);
				}
				return 1;
			}
		}
	}
}
