﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020003E3 RID: 995
	internal sealed class PopInstruction : Instruction
	{
		// Token: 0x0600189C RID: 6300 RVA: 0x00037C51 File Offset: 0x00035E51
		private PopInstruction()
		{
		}

		// Token: 0x170004C2 RID: 1218
		// (get) Token: 0x0600189D RID: 6301 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004C3 RID: 1219
		// (get) Token: 0x0600189E RID: 6302 RVA: 0x00048E9D File Offset: 0x0004709D
		public override string InstructionName
		{
			get
			{
				return "Pop";
			}
		}

		// Token: 0x0600189F RID: 6303 RVA: 0x00048EA4 File Offset: 0x000470A4
		public override int Run(InterpretedFrame frame)
		{
			frame.Pop();
			return 1;
		}

		// Token: 0x060018A0 RID: 6304 RVA: 0x00048EAE File Offset: 0x000470AE
		// Note: this type is marked as 'beforefieldinit'.
		static PopInstruction()
		{
		}

		// Token: 0x04000BC8 RID: 3016
		internal static readonly PopInstruction Instance = new PopInstruction();
	}
}
