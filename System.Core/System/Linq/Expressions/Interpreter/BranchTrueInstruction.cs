﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002B4 RID: 692
	internal sealed class BranchTrueInstruction : OffsetInstruction
	{
		// Token: 0x170003F4 RID: 1012
		// (get) Token: 0x060013E5 RID: 5093 RVA: 0x00039032 File Offset: 0x00037232
		public override Instruction[] Cache
		{
			get
			{
				if (BranchTrueInstruction.s_cache == null)
				{
					BranchTrueInstruction.s_cache = new Instruction[32];
				}
				return BranchTrueInstruction.s_cache;
			}
		}

		// Token: 0x170003F5 RID: 1013
		// (get) Token: 0x060013E6 RID: 5094 RVA: 0x0003904C File Offset: 0x0003724C
		public override string InstructionName
		{
			get
			{
				return "BranchTrue";
			}
		}

		// Token: 0x170003F6 RID: 1014
		// (get) Token: 0x060013E7 RID: 5095 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x060013E8 RID: 5096 RVA: 0x00039053 File Offset: 0x00037253
		public override int Run(InterpretedFrame frame)
		{
			if ((bool)frame.Pop())
			{
				return this._offset;
			}
			return 1;
		}

		// Token: 0x060013E9 RID: 5097 RVA: 0x0003902A File Offset: 0x0003722A
		public BranchTrueInstruction()
		{
		}

		// Token: 0x040009FA RID: 2554
		private static Instruction[] s_cache;
	}
}
