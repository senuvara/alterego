﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000356 RID: 854
	internal sealed class DebugInfo
	{
		// Token: 0x06001654 RID: 5716 RVA: 0x0003EA84 File Offset: 0x0003CC84
		public static DebugInfo GetMatchingDebugInfo(DebugInfo[] debugInfos, int index)
		{
			DebugInfo value = new DebugInfo
			{
				Index = index
			};
			int num = Array.BinarySearch<DebugInfo>(debugInfos, value, DebugInfo.s_debugComparer);
			if (num < 0)
			{
				num = ~num;
				if (num == 0)
				{
					return null;
				}
				num--;
			}
			return debugInfos[num];
		}

		// Token: 0x06001655 RID: 5717 RVA: 0x0003EAC0 File Offset: 0x0003CCC0
		public override string ToString()
		{
			if (this.IsClear)
			{
				return string.Format(CultureInfo.InvariantCulture, "{0}: clear", this.Index);
			}
			return string.Format(CultureInfo.InvariantCulture, "{0}: [{1}-{2}] '{3}'", new object[]
			{
				this.Index,
				this.StartLine,
				this.EndLine,
				this.FileName
			});
		}

		// Token: 0x06001656 RID: 5718 RVA: 0x00002310 File Offset: 0x00000510
		public DebugInfo()
		{
		}

		// Token: 0x06001657 RID: 5719 RVA: 0x0003EB38 File Offset: 0x0003CD38
		// Note: this type is marked as 'beforefieldinit'.
		static DebugInfo()
		{
		}

		// Token: 0x04000B28 RID: 2856
		public int StartLine;

		// Token: 0x04000B29 RID: 2857
		public int EndLine;

		// Token: 0x04000B2A RID: 2858
		public int Index;

		// Token: 0x04000B2B RID: 2859
		public string FileName;

		// Token: 0x04000B2C RID: 2860
		public bool IsClear;

		// Token: 0x04000B2D RID: 2861
		private static readonly DebugInfo.DebugInfoComparer s_debugComparer = new DebugInfo.DebugInfoComparer();

		// Token: 0x02000357 RID: 855
		private class DebugInfoComparer : IComparer<DebugInfo>
		{
			// Token: 0x06001658 RID: 5720 RVA: 0x0003EB44 File Offset: 0x0003CD44
			int IComparer<DebugInfo>.Compare(DebugInfo d1, DebugInfo d2)
			{
				if (d1.Index > d2.Index)
				{
					return 1;
				}
				if (d1.Index == d2.Index)
				{
					return 0;
				}
				return -1;
			}

			// Token: 0x06001659 RID: 5721 RVA: 0x00002310 File Offset: 0x00000510
			public DebugInfoComparer()
			{
			}
		}
	}
}
