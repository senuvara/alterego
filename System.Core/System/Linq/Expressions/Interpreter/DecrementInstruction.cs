﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002C6 RID: 710
	internal abstract class DecrementInstruction : Instruction
	{
		// Token: 0x17000423 RID: 1059
		// (get) Token: 0x06001450 RID: 5200 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000424 RID: 1060
		// (get) Token: 0x06001451 RID: 5201 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000425 RID: 1061
		// (get) Token: 0x06001452 RID: 5202 RVA: 0x00039A2F File Offset: 0x00037C2F
		public override string InstructionName
		{
			get
			{
				return "Decrement";
			}
		}

		// Token: 0x06001453 RID: 5203 RVA: 0x00037C51 File Offset: 0x00035E51
		private DecrementInstruction()
		{
		}

		// Token: 0x06001454 RID: 5204 RVA: 0x00039A38 File Offset: 0x00037C38
		public static Instruction Create(Type type)
		{
			switch (type.GetNonNullableType().GetTypeCode())
			{
			case TypeCode.Int16:
			{
				Instruction result;
				if ((result = DecrementInstruction.s_Int16) == null)
				{
					result = (DecrementInstruction.s_Int16 = new DecrementInstruction.DecrementInt16());
				}
				return result;
			}
			case TypeCode.UInt16:
			{
				Instruction result2;
				if ((result2 = DecrementInstruction.s_UInt16) == null)
				{
					result2 = (DecrementInstruction.s_UInt16 = new DecrementInstruction.DecrementUInt16());
				}
				return result2;
			}
			case TypeCode.Int32:
			{
				Instruction result3;
				if ((result3 = DecrementInstruction.s_Int32) == null)
				{
					result3 = (DecrementInstruction.s_Int32 = new DecrementInstruction.DecrementInt32());
				}
				return result3;
			}
			case TypeCode.UInt32:
			{
				Instruction result4;
				if ((result4 = DecrementInstruction.s_UInt32) == null)
				{
					result4 = (DecrementInstruction.s_UInt32 = new DecrementInstruction.DecrementUInt32());
				}
				return result4;
			}
			case TypeCode.Int64:
			{
				Instruction result5;
				if ((result5 = DecrementInstruction.s_Int64) == null)
				{
					result5 = (DecrementInstruction.s_Int64 = new DecrementInstruction.DecrementInt64());
				}
				return result5;
			}
			case TypeCode.UInt64:
			{
				Instruction result6;
				if ((result6 = DecrementInstruction.s_UInt64) == null)
				{
					result6 = (DecrementInstruction.s_UInt64 = new DecrementInstruction.DecrementUInt64());
				}
				return result6;
			}
			case TypeCode.Single:
			{
				Instruction result7;
				if ((result7 = DecrementInstruction.s_Single) == null)
				{
					result7 = (DecrementInstruction.s_Single = new DecrementInstruction.DecrementSingle());
				}
				return result7;
			}
			case TypeCode.Double:
			{
				Instruction result8;
				if ((result8 = DecrementInstruction.s_Double) == null)
				{
					result8 = (DecrementInstruction.s_Double = new DecrementInstruction.DecrementDouble());
				}
				return result8;
			}
			default:
				throw ContractUtils.Unreachable;
			}
		}

		// Token: 0x04000A1E RID: 2590
		private static Instruction s_Int16;

		// Token: 0x04000A1F RID: 2591
		private static Instruction s_Int32;

		// Token: 0x04000A20 RID: 2592
		private static Instruction s_Int64;

		// Token: 0x04000A21 RID: 2593
		private static Instruction s_UInt16;

		// Token: 0x04000A22 RID: 2594
		private static Instruction s_UInt32;

		// Token: 0x04000A23 RID: 2595
		private static Instruction s_UInt64;

		// Token: 0x04000A24 RID: 2596
		private static Instruction s_Single;

		// Token: 0x04000A25 RID: 2597
		private static Instruction s_Double;

		// Token: 0x020002C7 RID: 711
		private sealed class DecrementInt16 : DecrementInstruction
		{
			// Token: 0x06001455 RID: 5205 RVA: 0x00039B2C File Offset: 0x00037D2C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((short)obj - 1);
				}
				return 1;
			}

			// Token: 0x06001456 RID: 5206 RVA: 0x00039B5C File Offset: 0x00037D5C
			public DecrementInt16()
			{
			}
		}

		// Token: 0x020002C8 RID: 712
		private sealed class DecrementInt32 : DecrementInstruction
		{
			// Token: 0x06001457 RID: 5207 RVA: 0x00039B64 File Offset: 0x00037D64
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((int)obj - 1);
				}
				return 1;
			}

			// Token: 0x06001458 RID: 5208 RVA: 0x00039B5C File Offset: 0x00037D5C
			public DecrementInt32()
			{
			}
		}

		// Token: 0x020002C9 RID: 713
		private sealed class DecrementInt64 : DecrementInstruction
		{
			// Token: 0x06001459 RID: 5209 RVA: 0x00039B94 File Offset: 0x00037D94
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((long)obj - 1L);
				}
				return 1;
			}

			// Token: 0x0600145A RID: 5210 RVA: 0x00039B5C File Offset: 0x00037D5C
			public DecrementInt64()
			{
			}
		}

		// Token: 0x020002CA RID: 714
		private sealed class DecrementUInt16 : DecrementInstruction
		{
			// Token: 0x0600145B RID: 5211 RVA: 0x00039BCC File Offset: 0x00037DCC
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((ushort)obj - 1);
				}
				return 1;
			}

			// Token: 0x0600145C RID: 5212 RVA: 0x00039B5C File Offset: 0x00037D5C
			public DecrementUInt16()
			{
			}
		}

		// Token: 0x020002CB RID: 715
		private sealed class DecrementUInt32 : DecrementInstruction
		{
			// Token: 0x0600145D RID: 5213 RVA: 0x00039BFC File Offset: 0x00037DFC
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((uint)obj - 1U);
				}
				return 1;
			}

			// Token: 0x0600145E RID: 5214 RVA: 0x00039B5C File Offset: 0x00037D5C
			public DecrementUInt32()
			{
			}
		}

		// Token: 0x020002CC RID: 716
		private sealed class DecrementUInt64 : DecrementInstruction
		{
			// Token: 0x0600145F RID: 5215 RVA: 0x00039C30 File Offset: 0x00037E30
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((ulong)obj - 1UL);
				}
				return 1;
			}

			// Token: 0x06001460 RID: 5216 RVA: 0x00039B5C File Offset: 0x00037D5C
			public DecrementUInt64()
			{
			}
		}

		// Token: 0x020002CD RID: 717
		private sealed class DecrementSingle : DecrementInstruction
		{
			// Token: 0x06001461 RID: 5217 RVA: 0x00039C68 File Offset: 0x00037E68
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((float)obj - 1f);
				}
				return 1;
			}

			// Token: 0x06001462 RID: 5218 RVA: 0x00039B5C File Offset: 0x00037D5C
			public DecrementSingle()
			{
			}
		}

		// Token: 0x020002CE RID: 718
		private sealed class DecrementDouble : DecrementInstruction
		{
			// Token: 0x06001463 RID: 5219 RVA: 0x00039CA0 File Offset: 0x00037EA0
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((double)obj - 1.0);
				}
				return 1;
			}

			// Token: 0x06001464 RID: 5220 RVA: 0x00039B5C File Offset: 0x00037D5C
			public DecrementDouble()
			{
			}
		}
	}
}
