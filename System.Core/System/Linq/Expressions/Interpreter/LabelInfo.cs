﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200032C RID: 812
	internal sealed class LabelInfo
	{
		// Token: 0x060015E7 RID: 5607 RVA: 0x0003D4A3 File Offset: 0x0003B6A3
		internal LabelInfo(LabelTarget node)
		{
			this._node = node;
		}

		// Token: 0x060015E8 RID: 5608 RVA: 0x0003D4BD File Offset: 0x0003B6BD
		internal BranchLabel GetLabel(LightCompiler compiler)
		{
			this.EnsureLabel(compiler);
			return this._label;
		}

		// Token: 0x060015E9 RID: 5609 RVA: 0x0003D4CC File Offset: 0x0003B6CC
		internal void Reference(LabelScopeInfo block)
		{
			this._references.Add(block);
			if (this.HasDefinitions)
			{
				this.ValidateJump(block);
			}
		}

		// Token: 0x060015EA RID: 5610 RVA: 0x0003D4EC File Offset: 0x0003B6EC
		internal void Define(LabelScopeInfo block)
		{
			for (LabelScopeInfo labelScopeInfo = block; labelScopeInfo != null; labelScopeInfo = labelScopeInfo.Parent)
			{
				if (labelScopeInfo.ContainsTarget(this._node))
				{
					throw Error.LabelTargetAlreadyDefined(this._node.Name);
				}
			}
			this.AddDefinition(block);
			block.AddLabelInfo(this._node, this);
			if (this.HasDefinitions && !this.HasMultipleDefinitions)
			{
				using (List<LabelScopeInfo>.Enumerator enumerator = this._references.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						LabelScopeInfo reference = enumerator.Current;
						this.ValidateJump(reference);
					}
					return;
				}
			}
			if (this._acrossBlockJump)
			{
				throw Error.AmbiguousJump(this._node.Name);
			}
			this._label = null;
		}

		// Token: 0x060015EB RID: 5611 RVA: 0x0003D5B0 File Offset: 0x0003B7B0
		private void ValidateJump(LabelScopeInfo reference)
		{
			for (LabelScopeInfo labelScopeInfo = reference; labelScopeInfo != null; labelScopeInfo = labelScopeInfo.Parent)
			{
				if (this.DefinedIn(labelScopeInfo))
				{
					return;
				}
				if (labelScopeInfo.Kind == LabelScopeKind.Finally || labelScopeInfo.Kind == LabelScopeKind.Filter)
				{
					break;
				}
			}
			this._acrossBlockJump = true;
			if (this._node != null && this._node.Type != typeof(void))
			{
				throw Error.NonLocalJumpWithValue(this._node.Name);
			}
			if (this.HasMultipleDefinitions)
			{
				throw Error.AmbiguousJump(this._node.Name);
			}
			LabelScopeInfo labelScopeInfo2 = this.FirstDefinition();
			LabelScopeInfo labelScopeInfo3 = LabelInfo.CommonNode<LabelScopeInfo>(labelScopeInfo2, reference, (LabelScopeInfo b) => b.Parent);
			for (LabelScopeInfo labelScopeInfo4 = reference; labelScopeInfo4 != labelScopeInfo3; labelScopeInfo4 = labelScopeInfo4.Parent)
			{
				if (labelScopeInfo4.Kind == LabelScopeKind.Finally)
				{
					throw Error.ControlCannotLeaveFinally();
				}
				if (labelScopeInfo4.Kind == LabelScopeKind.Filter)
				{
					throw Error.ControlCannotLeaveFilterTest();
				}
			}
			LabelScopeInfo labelScopeInfo5 = labelScopeInfo2;
			while (labelScopeInfo5 != labelScopeInfo3)
			{
				if (!labelScopeInfo5.CanJumpInto)
				{
					if (labelScopeInfo5.Kind == LabelScopeKind.Expression)
					{
						throw Error.ControlCannotEnterExpression();
					}
					throw Error.ControlCannotEnterTry();
				}
				else
				{
					labelScopeInfo5 = labelScopeInfo5.Parent;
				}
			}
		}

		// Token: 0x060015EC RID: 5612 RVA: 0x0003D6C9 File Offset: 0x0003B8C9
		internal void ValidateFinish()
		{
			if (this._references.Count > 0 && !this.HasDefinitions)
			{
				throw Error.LabelTargetUndefined(this._node.Name);
			}
		}

		// Token: 0x060015ED RID: 5613 RVA: 0x0003D6F2 File Offset: 0x0003B8F2
		private void EnsureLabel(LightCompiler compiler)
		{
			if (this._label == null)
			{
				this._label = compiler.Instructions.MakeLabel();
			}
		}

		// Token: 0x060015EE RID: 5614 RVA: 0x0003D710 File Offset: 0x0003B910
		private bool DefinedIn(LabelScopeInfo scope)
		{
			if (this._definitions == scope)
			{
				return true;
			}
			HashSet<LabelScopeInfo> hashSet = this._definitions as HashSet<LabelScopeInfo>;
			return hashSet != null && hashSet.Contains(scope);
		}

		// Token: 0x17000459 RID: 1113
		// (get) Token: 0x060015EF RID: 5615 RVA: 0x0003D740 File Offset: 0x0003B940
		private bool HasDefinitions
		{
			get
			{
				return this._definitions != null;
			}
		}

		// Token: 0x060015F0 RID: 5616 RVA: 0x0003D74C File Offset: 0x0003B94C
		private LabelScopeInfo FirstDefinition()
		{
			LabelScopeInfo labelScopeInfo = this._definitions as LabelScopeInfo;
			if (labelScopeInfo != null)
			{
				return labelScopeInfo;
			}
			using (HashSet<LabelScopeInfo>.Enumerator enumerator = ((HashSet<LabelScopeInfo>)this._definitions).GetEnumerator())
			{
				if (enumerator.MoveNext())
				{
					return enumerator.Current;
				}
			}
			throw new InvalidOperationException();
		}

		// Token: 0x060015F1 RID: 5617 RVA: 0x0003D7B8 File Offset: 0x0003B9B8
		private void AddDefinition(LabelScopeInfo scope)
		{
			if (this._definitions == null)
			{
				this._definitions = scope;
				return;
			}
			HashSet<LabelScopeInfo> hashSet = this._definitions as HashSet<LabelScopeInfo>;
			if (hashSet == null)
			{
				HashSet<LabelScopeInfo> hashSet2 = new HashSet<LabelScopeInfo>();
				hashSet2.Add((LabelScopeInfo)this._definitions);
				hashSet = hashSet2;
				this._definitions = hashSet2;
			}
			hashSet.Add(scope);
		}

		// Token: 0x1700045A RID: 1114
		// (get) Token: 0x060015F2 RID: 5618 RVA: 0x0003D80B File Offset: 0x0003BA0B
		private bool HasMultipleDefinitions
		{
			get
			{
				return this._definitions is HashSet<LabelScopeInfo>;
			}
		}

		// Token: 0x060015F3 RID: 5619 RVA: 0x0003D81C File Offset: 0x0003BA1C
		internal static T CommonNode<T>(T first, T second, Func<T, T> parent) where T : class
		{
			EqualityComparer<T> @default = EqualityComparer<T>.Default;
			if (@default.Equals(first, second))
			{
				return first;
			}
			HashSet<T> hashSet = new HashSet<T>(@default);
			for (T t = first; t != null; t = parent(t))
			{
				hashSet.Add(t);
			}
			for (T t2 = second; t2 != null; t2 = parent(t2))
			{
				if (hashSet.Contains(t2))
				{
					return t2;
				}
			}
			return default(T);
		}

		// Token: 0x04000ACC RID: 2764
		private readonly LabelTarget _node;

		// Token: 0x04000ACD RID: 2765
		private BranchLabel _label;

		// Token: 0x04000ACE RID: 2766
		private object _definitions;

		// Token: 0x04000ACF RID: 2767
		private readonly List<LabelScopeInfo> _references = new List<LabelScopeInfo>();

		// Token: 0x04000AD0 RID: 2768
		private bool _acrossBlockJump;

		// Token: 0x0200032D RID: 813
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x060015F4 RID: 5620 RVA: 0x0003D887 File Offset: 0x0003BA87
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x060015F5 RID: 5621 RVA: 0x00002310 File Offset: 0x00000510
			public <>c()
			{
			}

			// Token: 0x060015F6 RID: 5622 RVA: 0x0003D893 File Offset: 0x0003BA93
			internal LabelScopeInfo <ValidateJump>b__9_0(LabelScopeInfo b)
			{
				return b.Parent;
			}

			// Token: 0x04000AD1 RID: 2769
			public static readonly LabelInfo.<>c <>9 = new LabelInfo.<>c();

			// Token: 0x04000AD2 RID: 2770
			public static Func<LabelScopeInfo, LabelScopeInfo> <>9__9_0;
		}
	}
}
