﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000339 RID: 825
	internal abstract class LessThanInstruction : Instruction
	{
		// Token: 0x1700045F RID: 1119
		// (get) Token: 0x06001611 RID: 5649 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x17000460 RID: 1120
		// (get) Token: 0x06001612 RID: 5650 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000461 RID: 1121
		// (get) Token: 0x06001613 RID: 5651 RVA: 0x0003DC52 File Offset: 0x0003BE52
		public override string InstructionName
		{
			get
			{
				return "LessThan";
			}
		}

		// Token: 0x06001614 RID: 5652 RVA: 0x0003DC59 File Offset: 0x0003BE59
		private LessThanInstruction(object nullValue)
		{
			this._nullValue = nullValue;
		}

		// Token: 0x06001615 RID: 5653 RVA: 0x0003DC68 File Offset: 0x0003BE68
		public static Instruction Create(Type type, bool liftedToNull = false)
		{
			if (liftedToNull)
			{
				switch (type.GetNonNullableType().GetTypeCode())
				{
				case TypeCode.Char:
				{
					Instruction result;
					if ((result = LessThanInstruction.s_liftedToNullChar) == null)
					{
						result = (LessThanInstruction.s_liftedToNullChar = new LessThanInstruction.LessThanChar(null));
					}
					return result;
				}
				case TypeCode.SByte:
				{
					Instruction result2;
					if ((result2 = LessThanInstruction.s_liftedToNullSByte) == null)
					{
						result2 = (LessThanInstruction.s_liftedToNullSByte = new LessThanInstruction.LessThanSByte(null));
					}
					return result2;
				}
				case TypeCode.Byte:
				{
					Instruction result3;
					if ((result3 = LessThanInstruction.s_liftedToNullByte) == null)
					{
						result3 = (LessThanInstruction.s_liftedToNullByte = new LessThanInstruction.LessThanByte(null));
					}
					return result3;
				}
				case TypeCode.Int16:
				{
					Instruction result4;
					if ((result4 = LessThanInstruction.s_liftedToNullInt16) == null)
					{
						result4 = (LessThanInstruction.s_liftedToNullInt16 = new LessThanInstruction.LessThanInt16(null));
					}
					return result4;
				}
				case TypeCode.UInt16:
				{
					Instruction result5;
					if ((result5 = LessThanInstruction.s_liftedToNullUInt16) == null)
					{
						result5 = (LessThanInstruction.s_liftedToNullUInt16 = new LessThanInstruction.LessThanUInt16(null));
					}
					return result5;
				}
				case TypeCode.Int32:
				{
					Instruction result6;
					if ((result6 = LessThanInstruction.s_liftedToNullInt32) == null)
					{
						result6 = (LessThanInstruction.s_liftedToNullInt32 = new LessThanInstruction.LessThanInt32(null));
					}
					return result6;
				}
				case TypeCode.UInt32:
				{
					Instruction result7;
					if ((result7 = LessThanInstruction.s_liftedToNullUInt32) == null)
					{
						result7 = (LessThanInstruction.s_liftedToNullUInt32 = new LessThanInstruction.LessThanUInt32(null));
					}
					return result7;
				}
				case TypeCode.Int64:
				{
					Instruction result8;
					if ((result8 = LessThanInstruction.s_liftedToNullInt64) == null)
					{
						result8 = (LessThanInstruction.s_liftedToNullInt64 = new LessThanInstruction.LessThanInt64(null));
					}
					return result8;
				}
				case TypeCode.UInt64:
				{
					Instruction result9;
					if ((result9 = LessThanInstruction.s_liftedToNullUInt64) == null)
					{
						result9 = (LessThanInstruction.s_liftedToNullUInt64 = new LessThanInstruction.LessThanUInt64(null));
					}
					return result9;
				}
				case TypeCode.Single:
				{
					Instruction result10;
					if ((result10 = LessThanInstruction.s_liftedToNullSingle) == null)
					{
						result10 = (LessThanInstruction.s_liftedToNullSingle = new LessThanInstruction.LessThanSingle(null));
					}
					return result10;
				}
				case TypeCode.Double:
				{
					Instruction result11;
					if ((result11 = LessThanInstruction.s_liftedToNullDouble) == null)
					{
						result11 = (LessThanInstruction.s_liftedToNullDouble = new LessThanInstruction.LessThanDouble(null));
					}
					return result11;
				}
				default:
					throw ContractUtils.Unreachable;
				}
			}
			else
			{
				switch (type.GetNonNullableType().GetTypeCode())
				{
				case TypeCode.Char:
				{
					Instruction result12;
					if ((result12 = LessThanInstruction.s_Char) == null)
					{
						result12 = (LessThanInstruction.s_Char = new LessThanInstruction.LessThanChar(Utils.BoxedFalse));
					}
					return result12;
				}
				case TypeCode.SByte:
				{
					Instruction result13;
					if ((result13 = LessThanInstruction.s_SByte) == null)
					{
						result13 = (LessThanInstruction.s_SByte = new LessThanInstruction.LessThanSByte(Utils.BoxedFalse));
					}
					return result13;
				}
				case TypeCode.Byte:
				{
					Instruction result14;
					if ((result14 = LessThanInstruction.s_Byte) == null)
					{
						result14 = (LessThanInstruction.s_Byte = new LessThanInstruction.LessThanByte(Utils.BoxedFalse));
					}
					return result14;
				}
				case TypeCode.Int16:
				{
					Instruction result15;
					if ((result15 = LessThanInstruction.s_Int16) == null)
					{
						result15 = (LessThanInstruction.s_Int16 = new LessThanInstruction.LessThanInt16(Utils.BoxedFalse));
					}
					return result15;
				}
				case TypeCode.UInt16:
				{
					Instruction result16;
					if ((result16 = LessThanInstruction.s_UInt16) == null)
					{
						result16 = (LessThanInstruction.s_UInt16 = new LessThanInstruction.LessThanUInt16(Utils.BoxedFalse));
					}
					return result16;
				}
				case TypeCode.Int32:
				{
					Instruction result17;
					if ((result17 = LessThanInstruction.s_Int32) == null)
					{
						result17 = (LessThanInstruction.s_Int32 = new LessThanInstruction.LessThanInt32(Utils.BoxedFalse));
					}
					return result17;
				}
				case TypeCode.UInt32:
				{
					Instruction result18;
					if ((result18 = LessThanInstruction.s_UInt32) == null)
					{
						result18 = (LessThanInstruction.s_UInt32 = new LessThanInstruction.LessThanUInt32(Utils.BoxedFalse));
					}
					return result18;
				}
				case TypeCode.Int64:
				{
					Instruction result19;
					if ((result19 = LessThanInstruction.s_Int64) == null)
					{
						result19 = (LessThanInstruction.s_Int64 = new LessThanInstruction.LessThanInt64(Utils.BoxedFalse));
					}
					return result19;
				}
				case TypeCode.UInt64:
				{
					Instruction result20;
					if ((result20 = LessThanInstruction.s_UInt64) == null)
					{
						result20 = (LessThanInstruction.s_UInt64 = new LessThanInstruction.LessThanUInt64(Utils.BoxedFalse));
					}
					return result20;
				}
				case TypeCode.Single:
				{
					Instruction result21;
					if ((result21 = LessThanInstruction.s_Single) == null)
					{
						result21 = (LessThanInstruction.s_Single = new LessThanInstruction.LessThanSingle(Utils.BoxedFalse));
					}
					return result21;
				}
				case TypeCode.Double:
				{
					Instruction result22;
					if ((result22 = LessThanInstruction.s_Double) == null)
					{
						result22 = (LessThanInstruction.s_Double = new LessThanInstruction.LessThanDouble(Utils.BoxedFalse));
					}
					return result22;
				}
				default:
					throw ContractUtils.Unreachable;
				}
			}
		}

		// Token: 0x04000AE8 RID: 2792
		private readonly object _nullValue;

		// Token: 0x04000AE9 RID: 2793
		private static Instruction s_SByte;

		// Token: 0x04000AEA RID: 2794
		private static Instruction s_Int16;

		// Token: 0x04000AEB RID: 2795
		private static Instruction s_Char;

		// Token: 0x04000AEC RID: 2796
		private static Instruction s_Int32;

		// Token: 0x04000AED RID: 2797
		private static Instruction s_Int64;

		// Token: 0x04000AEE RID: 2798
		private static Instruction s_Byte;

		// Token: 0x04000AEF RID: 2799
		private static Instruction s_UInt16;

		// Token: 0x04000AF0 RID: 2800
		private static Instruction s_UInt32;

		// Token: 0x04000AF1 RID: 2801
		private static Instruction s_UInt64;

		// Token: 0x04000AF2 RID: 2802
		private static Instruction s_Single;

		// Token: 0x04000AF3 RID: 2803
		private static Instruction s_Double;

		// Token: 0x04000AF4 RID: 2804
		private static Instruction s_liftedToNullSByte;

		// Token: 0x04000AF5 RID: 2805
		private static Instruction s_liftedToNullInt16;

		// Token: 0x04000AF6 RID: 2806
		private static Instruction s_liftedToNullChar;

		// Token: 0x04000AF7 RID: 2807
		private static Instruction s_liftedToNullInt32;

		// Token: 0x04000AF8 RID: 2808
		private static Instruction s_liftedToNullInt64;

		// Token: 0x04000AF9 RID: 2809
		private static Instruction s_liftedToNullByte;

		// Token: 0x04000AFA RID: 2810
		private static Instruction s_liftedToNullUInt16;

		// Token: 0x04000AFB RID: 2811
		private static Instruction s_liftedToNullUInt32;

		// Token: 0x04000AFC RID: 2812
		private static Instruction s_liftedToNullUInt64;

		// Token: 0x04000AFD RID: 2813
		private static Instruction s_liftedToNullSingle;

		// Token: 0x04000AFE RID: 2814
		private static Instruction s_liftedToNullDouble;

		// Token: 0x0200033A RID: 826
		private sealed class LessThanSByte : LessThanInstruction
		{
			// Token: 0x06001616 RID: 5654 RVA: 0x0003DF20 File Offset: 0x0003C120
			public LessThanSByte(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x06001617 RID: 5655 RVA: 0x0003DF2C File Offset: 0x0003C12C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((sbyte)obj2 < (sbyte)obj);
				}
				return 1;
			}
		}

		// Token: 0x0200033B RID: 827
		private sealed class LessThanInt16 : LessThanInstruction
		{
			// Token: 0x06001618 RID: 5656 RVA: 0x0003DF20 File Offset: 0x0003C120
			public LessThanInt16(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x06001619 RID: 5657 RVA: 0x0003DF70 File Offset: 0x0003C170
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((short)obj2 < (short)obj);
				}
				return 1;
			}
		}

		// Token: 0x0200033C RID: 828
		private sealed class LessThanChar : LessThanInstruction
		{
			// Token: 0x0600161A RID: 5658 RVA: 0x0003DF20 File Offset: 0x0003C120
			public LessThanChar(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x0600161B RID: 5659 RVA: 0x0003DFB4 File Offset: 0x0003C1B4
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((char)obj2 < (char)obj);
				}
				return 1;
			}
		}

		// Token: 0x0200033D RID: 829
		private sealed class LessThanInt32 : LessThanInstruction
		{
			// Token: 0x0600161C RID: 5660 RVA: 0x0003DF20 File Offset: 0x0003C120
			public LessThanInt32(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x0600161D RID: 5661 RVA: 0x0003DFF8 File Offset: 0x0003C1F8
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((int)obj2 < (int)obj);
				}
				return 1;
			}
		}

		// Token: 0x0200033E RID: 830
		private sealed class LessThanInt64 : LessThanInstruction
		{
			// Token: 0x0600161E RID: 5662 RVA: 0x0003DF20 File Offset: 0x0003C120
			public LessThanInt64(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x0600161F RID: 5663 RVA: 0x0003E03C File Offset: 0x0003C23C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((long)obj2 < (long)obj);
				}
				return 1;
			}
		}

		// Token: 0x0200033F RID: 831
		private sealed class LessThanByte : LessThanInstruction
		{
			// Token: 0x06001620 RID: 5664 RVA: 0x0003DF20 File Offset: 0x0003C120
			public LessThanByte(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x06001621 RID: 5665 RVA: 0x0003E080 File Offset: 0x0003C280
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((byte)obj2 < (byte)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000340 RID: 832
		private sealed class LessThanUInt16 : LessThanInstruction
		{
			// Token: 0x06001622 RID: 5666 RVA: 0x0003DF20 File Offset: 0x0003C120
			public LessThanUInt16(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x06001623 RID: 5667 RVA: 0x0003E0C4 File Offset: 0x0003C2C4
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((ushort)obj2 < (ushort)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000341 RID: 833
		private sealed class LessThanUInt32 : LessThanInstruction
		{
			// Token: 0x06001624 RID: 5668 RVA: 0x0003DF20 File Offset: 0x0003C120
			public LessThanUInt32(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x06001625 RID: 5669 RVA: 0x0003E108 File Offset: 0x0003C308
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((uint)obj2 < (uint)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000342 RID: 834
		private sealed class LessThanUInt64 : LessThanInstruction
		{
			// Token: 0x06001626 RID: 5670 RVA: 0x0003DF20 File Offset: 0x0003C120
			public LessThanUInt64(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x06001627 RID: 5671 RVA: 0x0003E14C File Offset: 0x0003C34C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((ulong)obj2 < (ulong)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000343 RID: 835
		private sealed class LessThanSingle : LessThanInstruction
		{
			// Token: 0x06001628 RID: 5672 RVA: 0x0003DF20 File Offset: 0x0003C120
			public LessThanSingle(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x06001629 RID: 5673 RVA: 0x0003E190 File Offset: 0x0003C390
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((float)obj2 < (float)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000344 RID: 836
		private sealed class LessThanDouble : LessThanInstruction
		{
			// Token: 0x0600162A RID: 5674 RVA: 0x0003DF20 File Offset: 0x0003C120
			public LessThanDouble(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x0600162B RID: 5675 RVA: 0x0003E1D4 File Offset: 0x0003C3D4
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((double)obj2 < (double)obj);
				}
				return 1;
			}
		}
	}
}
