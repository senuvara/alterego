﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002C1 RID: 705
	internal sealed class EnterExceptionHandlerInstruction : Instruction
	{
		// Token: 0x06001433 RID: 5171 RVA: 0x000397E5 File Offset: 0x000379E5
		private EnterExceptionHandlerInstruction(bool hasValue)
		{
			this._hasValue = hasValue;
		}

		// Token: 0x17000416 RID: 1046
		// (get) Token: 0x06001434 RID: 5172 RVA: 0x000397F4 File Offset: 0x000379F4
		public override string InstructionName
		{
			get
			{
				return "EnterExceptionHandler";
			}
		}

		// Token: 0x17000417 RID: 1047
		// (get) Token: 0x06001435 RID: 5173 RVA: 0x000397FB File Offset: 0x000379FB
		public override int ConsumedStack
		{
			get
			{
				if (!this._hasValue)
				{
					return 0;
				}
				return 1;
			}
		}

		// Token: 0x17000418 RID: 1048
		// (get) Token: 0x06001436 RID: 5174 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x06001437 RID: 5175 RVA: 0x00009CDF File Offset: 0x00007EDF
		[ExcludeFromCodeCoverage]
		public override int Run(InterpretedFrame frame)
		{
			return 1;
		}

		// Token: 0x06001438 RID: 5176 RVA: 0x00039808 File Offset: 0x00037A08
		// Note: this type is marked as 'beforefieldinit'.
		static EnterExceptionHandlerInstruction()
		{
		}

		// Token: 0x04000A0F RID: 2575
		internal static readonly EnterExceptionHandlerInstruction Void = new EnterExceptionHandlerInstruction(false);

		// Token: 0x04000A10 RID: 2576
		internal static readonly EnterExceptionHandlerInstruction NonVoid = new EnterExceptionHandlerInstruction(true);

		// Token: 0x04000A11 RID: 2577
		private readonly bool _hasValue;
	}
}
