﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020003F8 RID: 1016
	internal sealed class TypeEqualsInstruction : Instruction
	{
		// Token: 0x170004D5 RID: 1237
		// (get) Token: 0x060018DD RID: 6365 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x170004D6 RID: 1238
		// (get) Token: 0x060018DE RID: 6366 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004D7 RID: 1239
		// (get) Token: 0x060018DF RID: 6367 RVA: 0x00049664 File Offset: 0x00047864
		public override string InstructionName
		{
			get
			{
				return "TypeEquals";
			}
		}

		// Token: 0x060018E0 RID: 6368 RVA: 0x00037C51 File Offset: 0x00035E51
		private TypeEqualsInstruction()
		{
		}

		// Token: 0x060018E1 RID: 6369 RVA: 0x0004966C File Offset: 0x0004786C
		public override int Run(InterpretedFrame frame)
		{
			object obj = frame.Pop();
			object obj2 = frame.Pop();
			frame.Push(((obj2 != null) ? obj2.GetType() : null) == obj);
			return 1;
		}

		// Token: 0x060018E2 RID: 6370 RVA: 0x0004969D File Offset: 0x0004789D
		// Note: this type is marked as 'beforefieldinit'.
		static TypeEqualsInstruction()
		{
		}

		// Token: 0x04000BDB RID: 3035
		public static readonly TypeEqualsInstruction Instance = new TypeEqualsInstruction();
	}
}
