﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002B9 RID: 697
	internal sealed class EnterTryCatchFinallyInstruction : IndexedBranchInstruction
	{
		// Token: 0x06001402 RID: 5122 RVA: 0x000392D9 File Offset: 0x000374D9
		internal void SetTryHandler(TryCatchFinallyHandler tryHandler)
		{
			this._tryHandler = tryHandler;
		}

		// Token: 0x17000402 RID: 1026
		// (get) Token: 0x06001403 RID: 5123 RVA: 0x000392E2 File Offset: 0x000374E2
		internal TryCatchFinallyHandler Handler
		{
			get
			{
				return this._tryHandler;
			}
		}

		// Token: 0x17000403 RID: 1027
		// (get) Token: 0x06001404 RID: 5124 RVA: 0x000392EA File Offset: 0x000374EA
		public override int ProducedContinuations
		{
			get
			{
				if (!this._hasFinally)
				{
					return 0;
				}
				return 1;
			}
		}

		// Token: 0x06001405 RID: 5125 RVA: 0x000392F7 File Offset: 0x000374F7
		private EnterTryCatchFinallyInstruction(int targetIndex, bool hasFinally) : base(targetIndex)
		{
			this._hasFinally = hasFinally;
		}

		// Token: 0x06001406 RID: 5126 RVA: 0x00039307 File Offset: 0x00037507
		internal static EnterTryCatchFinallyInstruction CreateTryFinally(int labelIndex)
		{
			return new EnterTryCatchFinallyInstruction(labelIndex, true);
		}

		// Token: 0x06001407 RID: 5127 RVA: 0x00039310 File Offset: 0x00037510
		internal static EnterTryCatchFinallyInstruction CreateTryCatch()
		{
			return new EnterTryCatchFinallyInstruction(int.MaxValue, false);
		}

		// Token: 0x06001408 RID: 5128 RVA: 0x00039320 File Offset: 0x00037520
		public override int Run(InterpretedFrame frame)
		{
			if (this._hasFinally)
			{
				frame.PushContinuation(this._labelIndex);
			}
			int instructionIndex = frame.InstructionIndex;
			frame.InstructionIndex++;
			Instruction[] instructions = frame.Interpreter.Instructions.Instructions;
			ExceptionHandler exceptionHandler;
			object value;
			try
			{
				int num = frame.InstructionIndex;
				while (num >= this._tryHandler.TryStartIndex && num < this._tryHandler.TryEndIndex)
				{
					num += instructions[num].Run(frame);
					frame.InstructionIndex = num;
				}
				if (num == this._tryHandler.GotoEndTargetIndex)
				{
					frame.InstructionIndex += instructions[num].Run(frame);
				}
			}
			catch (Exception exception) when (this._tryHandler.HasHandler(frame, exception, out exceptionHandler, out value))
			{
				frame.InstructionIndex += frame.Goto(exceptionHandler.LabelIndex, value, true);
				bool flag = false;
				try
				{
					int num2 = frame.InstructionIndex;
					while (num2 >= exceptionHandler.HandlerStartIndex && num2 < exceptionHandler.HandlerEndIndex)
					{
						num2 += instructions[num2].Run(frame);
						frame.InstructionIndex = num2;
					}
					if (num2 == this._tryHandler.GotoEndTargetIndex)
					{
						frame.InstructionIndex += instructions[num2].Run(frame);
					}
				}
				catch (RethrowException)
				{
					flag = true;
				}
				if (flag)
				{
					throw;
				}
			}
			finally
			{
				if (this._tryHandler.IsFinallyBlockExist)
				{
					int num3 = frame.InstructionIndex = this._tryHandler.FinallyStartIndex;
					while (num3 >= this._tryHandler.FinallyStartIndex && num3 < this._tryHandler.FinallyEndIndex)
					{
						num3 += instructions[num3].Run(frame);
						frame.InstructionIndex = num3;
					}
				}
			}
			return frame.InstructionIndex - instructionIndex;
		}

		// Token: 0x17000404 RID: 1028
		// (get) Token: 0x06001409 RID: 5129 RVA: 0x00039534 File Offset: 0x00037734
		public override string InstructionName
		{
			get
			{
				if (!this._hasFinally)
				{
					return "EnterTryCatch";
				}
				return "EnterTryFinally";
			}
		}

		// Token: 0x0600140A RID: 5130 RVA: 0x00039549 File Offset: 0x00037749
		public override string ToString()
		{
			if (!this._hasFinally)
			{
				return "EnterTryCatch";
			}
			return "EnterTryFinally[" + this._labelIndex + "]";
		}

		// Token: 0x04000A06 RID: 2566
		private readonly bool _hasFinally;

		// Token: 0x04000A07 RID: 2567
		private TryCatchFinallyHandler _tryHandler;
	}
}
