﻿using System;
using System.Reflection;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000301 RID: 769
	internal sealed class StoreStaticFieldInstruction : FieldInstruction
	{
		// Token: 0x060014DC RID: 5340 RVA: 0x0003ADC8 File Offset: 0x00038FC8
		public StoreStaticFieldInstruction(FieldInfo field) : base(field)
		{
		}

		// Token: 0x17000438 RID: 1080
		// (get) Token: 0x060014DD RID: 5341 RVA: 0x0003AE57 File Offset: 0x00039057
		public override string InstructionName
		{
			get
			{
				return "StoreStaticField";
			}
		}

		// Token: 0x17000439 RID: 1081
		// (get) Token: 0x060014DE RID: 5342 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x060014DF RID: 5343 RVA: 0x0003AE60 File Offset: 0x00039060
		public override int Run(InterpretedFrame frame)
		{
			object value = frame.Pop();
			this._field.SetValue(null, value);
			return 1;
		}
	}
}
