﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000370 RID: 880
	internal sealed class AssignLocalToClosureInstruction : LocalAccessInstruction
	{
		// Token: 0x06001757 RID: 5975 RVA: 0x000459CD File Offset: 0x00043BCD
		internal AssignLocalToClosureInstruction(int index) : base(index)
		{
		}

		// Token: 0x1700047D RID: 1149
		// (get) Token: 0x06001758 RID: 5976 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x1700047E RID: 1150
		// (get) Token: 0x06001759 RID: 5977 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x1700047F RID: 1151
		// (get) Token: 0x0600175A RID: 5978 RVA: 0x00045BB5 File Offset: 0x00043DB5
		public override string InstructionName
		{
			get
			{
				return "AssignLocalClosure";
			}
		}

		// Token: 0x0600175B RID: 5979 RVA: 0x00045BBC File Offset: 0x00043DBC
		public override int Run(InterpretedFrame frame)
		{
			frame.Closure[this._index].Value = frame.Peek();
			return 1;
		}
	}
}
