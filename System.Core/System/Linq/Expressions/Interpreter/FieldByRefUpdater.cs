﻿using System;
using System.Reflection;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200035F RID: 863
	internal sealed class FieldByRefUpdater : ByRefUpdater
	{
		// Token: 0x060016CD RID: 5837 RVA: 0x000432A3 File Offset: 0x000414A3
		public FieldByRefUpdater(LocalDefinition? obj, FieldInfo field, int argumentIndex) : base(argumentIndex)
		{
			this._object = obj;
			this._field = field;
		}

		// Token: 0x060016CE RID: 5838 RVA: 0x000432BC File Offset: 0x000414BC
		public override void Update(InterpretedFrame frame, object value)
		{
			object obj = (this._object == null) ? null : frame.Data[this._object.GetValueOrDefault().Index];
			this._field.SetValue(obj, value);
		}

		// Token: 0x060016CF RID: 5839 RVA: 0x00043308 File Offset: 0x00041508
		public override void UndefineTemps(InstructionList instructions, LocalVariables locals)
		{
			if (this._object != null)
			{
				locals.UndefineLocal(this._object.GetValueOrDefault(), instructions.Count);
			}
		}

		// Token: 0x04000B43 RID: 2883
		private readonly LocalDefinition? _object;

		// Token: 0x04000B44 RID: 2884
		private readonly FieldInfo _field;
	}
}
