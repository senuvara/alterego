﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200036F RID: 879
	internal sealed class StoreLocalBoxedInstruction : LocalAccessInstruction
	{
		// Token: 0x06001753 RID: 5971 RVA: 0x000459CD File Offset: 0x00043BCD
		internal StoreLocalBoxedInstruction(int index) : base(index)
		{
		}

		// Token: 0x1700047B RID: 1147
		// (get) Token: 0x06001754 RID: 5972 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x1700047C RID: 1148
		// (get) Token: 0x06001755 RID: 5973 RVA: 0x00045B70 File Offset: 0x00043D70
		public override string InstructionName
		{
			get
			{
				return "StoreLocalBox";
			}
		}

		// Token: 0x06001756 RID: 5974 RVA: 0x00045B78 File Offset: 0x00043D78
		public override int Run(InterpretedFrame frame)
		{
			IStrongBox strongBox = (IStrongBox)frame.Data[this._index];
			object[] data = frame.Data;
			int num = frame.StackIndex - 1;
			frame.StackIndex = num;
			strongBox.Value = data[num];
			return 1;
		}
	}
}
