﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002AE RID: 686
	internal sealed class BranchLabel
	{
		// Token: 0x170003E7 RID: 999
		// (get) Token: 0x060013BD RID: 5053 RVA: 0x000388D2 File Offset: 0x00036AD2
		// (set) Token: 0x060013BE RID: 5054 RVA: 0x000388DA File Offset: 0x00036ADA
		internal int LabelIndex
		{
			[CompilerGenerated]
			get
			{
				return this.<LabelIndex>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<LabelIndex>k__BackingField = value;
			}
		} = int.MinValue;

		// Token: 0x170003E8 RID: 1000
		// (get) Token: 0x060013BF RID: 5055 RVA: 0x000388E3 File Offset: 0x00036AE3
		internal bool HasRuntimeLabel
		{
			get
			{
				return this.LabelIndex != int.MinValue;
			}
		}

		// Token: 0x170003E9 RID: 1001
		// (get) Token: 0x060013C0 RID: 5056 RVA: 0x000388F5 File Offset: 0x00036AF5
		internal int TargetIndex
		{
			get
			{
				return this._targetIndex;
			}
		}

		// Token: 0x060013C1 RID: 5057 RVA: 0x000388FD File Offset: 0x00036AFD
		internal RuntimeLabel ToRuntimeLabel()
		{
			return new RuntimeLabel(this._targetIndex, this._continuationStackDepth, this._stackDepth);
		}

		// Token: 0x060013C2 RID: 5058 RVA: 0x00038918 File Offset: 0x00036B18
		internal void Mark(InstructionList instructions)
		{
			this._stackDepth = instructions.CurrentStackDepth;
			this._continuationStackDepth = instructions.CurrentContinuationsDepth;
			this._targetIndex = instructions.Count;
			if (this._forwardBranchFixups != null)
			{
				foreach (int branchIndex in this._forwardBranchFixups)
				{
					this.FixupBranch(instructions, branchIndex);
				}
				this._forwardBranchFixups = null;
			}
		}

		// Token: 0x060013C3 RID: 5059 RVA: 0x000389A0 File Offset: 0x00036BA0
		internal void AddBranch(InstructionList instructions, int branchIndex)
		{
			if (this._targetIndex == -2147483648)
			{
				if (this._forwardBranchFixups == null)
				{
					this._forwardBranchFixups = new List<int>();
				}
				this._forwardBranchFixups.Add(branchIndex);
				return;
			}
			this.FixupBranch(instructions, branchIndex);
		}

		// Token: 0x060013C4 RID: 5060 RVA: 0x000389D7 File Offset: 0x00036BD7
		internal void FixupBranch(InstructionList instructions, int branchIndex)
		{
			instructions.FixupBranch(branchIndex, this._targetIndex - branchIndex);
		}

		// Token: 0x060013C5 RID: 5061 RVA: 0x000389E8 File Offset: 0x00036BE8
		public BranchLabel()
		{
		}

		// Token: 0x040009EC RID: 2540
		internal const int UnknownIndex = -2147483648;

		// Token: 0x040009ED RID: 2541
		internal const int UnknownDepth = -2147483648;

		// Token: 0x040009EE RID: 2542
		private int _targetIndex = int.MinValue;

		// Token: 0x040009EF RID: 2543
		private int _stackDepth = int.MinValue;

		// Token: 0x040009F0 RID: 2544
		private int _continuationStackDepth = int.MinValue;

		// Token: 0x040009F1 RID: 2545
		private List<int> _forwardBranchFixups;

		// Token: 0x040009F2 RID: 2546
		[CompilerGenerated]
		private int <LabelIndex>k__BackingField;
	}
}
