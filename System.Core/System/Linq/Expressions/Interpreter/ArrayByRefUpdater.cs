﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200035E RID: 862
	internal sealed class ArrayByRefUpdater : ByRefUpdater
	{
		// Token: 0x060016CA RID: 5834 RVA: 0x0004321D File Offset: 0x0004141D
		public ArrayByRefUpdater(LocalDefinition array, LocalDefinition index, int argumentIndex) : base(argumentIndex)
		{
			this._array = array;
			this._index = index;
		}

		// Token: 0x060016CB RID: 5835 RVA: 0x00043234 File Offset: 0x00041434
		public override void Update(InterpretedFrame frame, object value)
		{
			object obj = frame.Data[this._index.Index];
			((Array)frame.Data[this._array.Index]).SetValue(value, (int)obj);
		}

		// Token: 0x060016CC RID: 5836 RVA: 0x0004327D File Offset: 0x0004147D
		public override void UndefineTemps(InstructionList instructions, LocalVariables locals)
		{
			locals.UndefineLocal(this._array, instructions.Count);
			locals.UndefineLocal(this._index, instructions.Count);
		}

		// Token: 0x04000B41 RID: 2881
		private readonly LocalDefinition _array;

		// Token: 0x04000B42 RID: 2882
		private readonly LocalDefinition _index;
	}
}
