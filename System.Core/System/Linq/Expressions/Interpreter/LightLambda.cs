﻿using System;
using System.Collections.Generic;
using System.Dynamic.Utils;
using System.Globalization;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000363 RID: 867
	internal class LightLambda
	{
		// Token: 0x060016DA RID: 5850 RVA: 0x0004356C File Offset: 0x0004176C
		internal TRet Run0<TRet>()
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
			return (TRet)((object)interpretedFrame.Pop());
		}

		// Token: 0x060016DB RID: 5851 RVA: 0x000435B8 File Offset: 0x000417B8
		internal void RunVoid0()
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
		}

		// Token: 0x060016DC RID: 5852 RVA: 0x000435FC File Offset: 0x000417FC
		internal static Delegate MakeRun0<TRet>(LightLambda lambda)
		{
			return new Func<TRet>(lambda.Run0<TRet>);
		}

		// Token: 0x060016DD RID: 5853 RVA: 0x0004360A File Offset: 0x0004180A
		internal static Delegate MakeRunVoid0(LightLambda lambda)
		{
			return new Action(lambda.RunVoid0);
		}

		// Token: 0x060016DE RID: 5854 RVA: 0x00043618 File Offset: 0x00041818
		internal TRet Run1<T0, TRet>(T0 arg0)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
			return (TRet)((object)interpretedFrame.Pop());
		}

		// Token: 0x060016DF RID: 5855 RVA: 0x00043674 File Offset: 0x00041874
		internal void RunVoid1<T0>(T0 arg0)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
		}

		// Token: 0x060016E0 RID: 5856 RVA: 0x000436C4 File Offset: 0x000418C4
		internal static Delegate MakeRun1<T0, TRet>(LightLambda lambda)
		{
			return new Func<T0, TRet>(lambda.Run1<T0, TRet>);
		}

		// Token: 0x060016E1 RID: 5857 RVA: 0x000436D2 File Offset: 0x000418D2
		internal static Delegate MakeRunVoid1<T0>(LightLambda lambda)
		{
			return new Action<T0>(lambda.RunVoid1<T0>);
		}

		// Token: 0x060016E2 RID: 5858 RVA: 0x000436E0 File Offset: 0x000418E0
		internal TRet Run2<T0, T1, TRet>(T0 arg0, T1 arg1)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
			return (TRet)((object)interpretedFrame.Pop());
		}

		// Token: 0x060016E3 RID: 5859 RVA: 0x00043748 File Offset: 0x00041948
		internal void RunVoid2<T0, T1>(T0 arg0, T1 arg1)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
		}

		// Token: 0x060016E4 RID: 5860 RVA: 0x000437A8 File Offset: 0x000419A8
		internal static Delegate MakeRun2<T0, T1, TRet>(LightLambda lambda)
		{
			return new Func<T0, T1, TRet>(lambda.Run2<T0, T1, TRet>);
		}

		// Token: 0x060016E5 RID: 5861 RVA: 0x000437B6 File Offset: 0x000419B6
		internal static Delegate MakeRunVoid2<T0, T1>(LightLambda lambda)
		{
			return new Action<T0, T1>(lambda.RunVoid2<T0, T1>);
		}

		// Token: 0x060016E6 RID: 5862 RVA: 0x000437C4 File Offset: 0x000419C4
		internal TRet Run3<T0, T1, T2, TRet>(T0 arg0, T1 arg1, T2 arg2)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
			return (TRet)((object)interpretedFrame.Pop());
		}

		// Token: 0x060016E7 RID: 5863 RVA: 0x0004383C File Offset: 0x00041A3C
		internal void RunVoid3<T0, T1, T2>(T0 arg0, T1 arg1, T2 arg2)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
		}

		// Token: 0x060016E8 RID: 5864 RVA: 0x000438A8 File Offset: 0x00041AA8
		internal static Delegate MakeRun3<T0, T1, T2, TRet>(LightLambda lambda)
		{
			return new Func<T0, T1, T2, TRet>(lambda.Run3<T0, T1, T2, TRet>);
		}

		// Token: 0x060016E9 RID: 5865 RVA: 0x000438B6 File Offset: 0x00041AB6
		internal static Delegate MakeRunVoid3<T0, T1, T2>(LightLambda lambda)
		{
			return new Action<T0, T1, T2>(lambda.RunVoid3<T0, T1, T2>);
		}

		// Token: 0x060016EA RID: 5866 RVA: 0x000438C4 File Offset: 0x00041AC4
		internal TRet Run4<T0, T1, T2, T3, TRet>(T0 arg0, T1 arg1, T2 arg2, T3 arg3)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
			return (TRet)((object)interpretedFrame.Pop());
		}

		// Token: 0x060016EB RID: 5867 RVA: 0x0004394C File Offset: 0x00041B4C
		internal void RunVoid4<T0, T1, T2, T3>(T0 arg0, T1 arg1, T2 arg2, T3 arg3)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
		}

		// Token: 0x060016EC RID: 5868 RVA: 0x000439C8 File Offset: 0x00041BC8
		internal static Delegate MakeRun4<T0, T1, T2, T3, TRet>(LightLambda lambda)
		{
			return new Func<T0, T1, T2, T3, TRet>(lambda.Run4<T0, T1, T2, T3, TRet>);
		}

		// Token: 0x060016ED RID: 5869 RVA: 0x000439D6 File Offset: 0x00041BD6
		internal static Delegate MakeRunVoid4<T0, T1, T2, T3>(LightLambda lambda)
		{
			return new Action<T0, T1, T2, T3>(lambda.RunVoid4<T0, T1, T2, T3>);
		}

		// Token: 0x060016EE RID: 5870 RVA: 0x000439E4 File Offset: 0x00041BE4
		internal TRet Run5<T0, T1, T2, T3, T4, TRet>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			interpretedFrame.Data[4] = arg4;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
			return (TRet)((object)interpretedFrame.Pop());
		}

		// Token: 0x060016EF RID: 5871 RVA: 0x00043A78 File Offset: 0x00041C78
		internal void RunVoid5<T0, T1, T2, T3, T4>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			interpretedFrame.Data[4] = arg4;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
		}

		// Token: 0x060016F0 RID: 5872 RVA: 0x00043B04 File Offset: 0x00041D04
		internal static Delegate MakeRun5<T0, T1, T2, T3, T4, TRet>(LightLambda lambda)
		{
			return new Func<T0, T1, T2, T3, T4, TRet>(lambda.Run5<T0, T1, T2, T3, T4, TRet>);
		}

		// Token: 0x060016F1 RID: 5873 RVA: 0x00043B12 File Offset: 0x00041D12
		internal static Delegate MakeRunVoid5<T0, T1, T2, T3, T4>(LightLambda lambda)
		{
			return new Action<T0, T1, T2, T3, T4>(lambda.RunVoid5<T0, T1, T2, T3, T4>);
		}

		// Token: 0x060016F2 RID: 5874 RVA: 0x00043B20 File Offset: 0x00041D20
		internal TRet Run6<T0, T1, T2, T3, T4, T5, TRet>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			interpretedFrame.Data[4] = arg4;
			interpretedFrame.Data[5] = arg5;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
			return (TRet)((object)interpretedFrame.Pop());
		}

		// Token: 0x060016F3 RID: 5875 RVA: 0x00043BC4 File Offset: 0x00041DC4
		internal void RunVoid6<T0, T1, T2, T3, T4, T5>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			interpretedFrame.Data[4] = arg4;
			interpretedFrame.Data[5] = arg5;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
		}

		// Token: 0x060016F4 RID: 5876 RVA: 0x00043C5C File Offset: 0x00041E5C
		internal static Delegate MakeRun6<T0, T1, T2, T3, T4, T5, TRet>(LightLambda lambda)
		{
			return new Func<T0, T1, T2, T3, T4, T5, TRet>(lambda.Run6<T0, T1, T2, T3, T4, T5, TRet>);
		}

		// Token: 0x060016F5 RID: 5877 RVA: 0x00043C6A File Offset: 0x00041E6A
		internal static Delegate MakeRunVoid6<T0, T1, T2, T3, T4, T5>(LightLambda lambda)
		{
			return new Action<T0, T1, T2, T3, T4, T5>(lambda.RunVoid6<T0, T1, T2, T3, T4, T5>);
		}

		// Token: 0x060016F6 RID: 5878 RVA: 0x00043C78 File Offset: 0x00041E78
		internal TRet Run7<T0, T1, T2, T3, T4, T5, T6, TRet>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			interpretedFrame.Data[4] = arg4;
			interpretedFrame.Data[5] = arg5;
			interpretedFrame.Data[6] = arg6;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
			return (TRet)((object)interpretedFrame.Pop());
		}

		// Token: 0x060016F7 RID: 5879 RVA: 0x00043D2C File Offset: 0x00041F2C
		internal void RunVoid7<T0, T1, T2, T3, T4, T5, T6>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			interpretedFrame.Data[4] = arg4;
			interpretedFrame.Data[5] = arg5;
			interpretedFrame.Data[6] = arg6;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
		}

		// Token: 0x060016F8 RID: 5880 RVA: 0x00043DD4 File Offset: 0x00041FD4
		internal static Delegate MakeRun7<T0, T1, T2, T3, T4, T5, T6, TRet>(LightLambda lambda)
		{
			return new Func<T0, T1, T2, T3, T4, T5, T6, TRet>(lambda.Run7<T0, T1, T2, T3, T4, T5, T6, TRet>);
		}

		// Token: 0x060016F9 RID: 5881 RVA: 0x00043DE2 File Offset: 0x00041FE2
		internal static Delegate MakeRunVoid7<T0, T1, T2, T3, T4, T5, T6>(LightLambda lambda)
		{
			return new Action<T0, T1, T2, T3, T4, T5, T6>(lambda.RunVoid7<T0, T1, T2, T3, T4, T5, T6>);
		}

		// Token: 0x060016FA RID: 5882 RVA: 0x00043DF0 File Offset: 0x00041FF0
		internal TRet Run8<T0, T1, T2, T3, T4, T5, T6, T7, TRet>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			interpretedFrame.Data[4] = arg4;
			interpretedFrame.Data[5] = arg5;
			interpretedFrame.Data[6] = arg6;
			interpretedFrame.Data[7] = arg7;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
			return (TRet)((object)interpretedFrame.Pop());
		}

		// Token: 0x060016FB RID: 5883 RVA: 0x00043EB4 File Offset: 0x000420B4
		internal void RunVoid8<T0, T1, T2, T3, T4, T5, T6, T7>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			interpretedFrame.Data[4] = arg4;
			interpretedFrame.Data[5] = arg5;
			interpretedFrame.Data[6] = arg6;
			interpretedFrame.Data[7] = arg7;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
		}

		// Token: 0x060016FC RID: 5884 RVA: 0x00043F6C File Offset: 0x0004216C
		internal static Delegate MakeRun8<T0, T1, T2, T3, T4, T5, T6, T7, TRet>(LightLambda lambda)
		{
			return new Func<T0, T1, T2, T3, T4, T5, T6, T7, TRet>(lambda.Run8<T0, T1, T2, T3, T4, T5, T6, T7, TRet>);
		}

		// Token: 0x060016FD RID: 5885 RVA: 0x00043F7A File Offset: 0x0004217A
		internal static Delegate MakeRunVoid8<T0, T1, T2, T3, T4, T5, T6, T7>(LightLambda lambda)
		{
			return new Action<T0, T1, T2, T3, T4, T5, T6, T7>(lambda.RunVoid8<T0, T1, T2, T3, T4, T5, T6, T7>);
		}

		// Token: 0x060016FE RID: 5886 RVA: 0x00043F88 File Offset: 0x00042188
		internal TRet Run9<T0, T1, T2, T3, T4, T5, T6, T7, T8, TRet>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			interpretedFrame.Data[4] = arg4;
			interpretedFrame.Data[5] = arg5;
			interpretedFrame.Data[6] = arg6;
			interpretedFrame.Data[7] = arg7;
			interpretedFrame.Data[8] = arg8;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
			return (TRet)((object)interpretedFrame.Pop());
		}

		// Token: 0x060016FF RID: 5887 RVA: 0x00044058 File Offset: 0x00042258
		internal void RunVoid9<T0, T1, T2, T3, T4, T5, T6, T7, T8>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			interpretedFrame.Data[4] = arg4;
			interpretedFrame.Data[5] = arg5;
			interpretedFrame.Data[6] = arg6;
			interpretedFrame.Data[7] = arg7;
			interpretedFrame.Data[8] = arg8;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
		}

		// Token: 0x06001700 RID: 5888 RVA: 0x00044120 File Offset: 0x00042320
		internal static Delegate MakeRun9<T0, T1, T2, T3, T4, T5, T6, T7, T8, TRet>(LightLambda lambda)
		{
			return new Func<T0, T1, T2, T3, T4, T5, T6, T7, T8, TRet>(lambda.Run9<T0, T1, T2, T3, T4, T5, T6, T7, T8, TRet>);
		}

		// Token: 0x06001701 RID: 5889 RVA: 0x0004412E File Offset: 0x0004232E
		internal static Delegate MakeRunVoid9<T0, T1, T2, T3, T4, T5, T6, T7, T8>(LightLambda lambda)
		{
			return new Action<T0, T1, T2, T3, T4, T5, T6, T7, T8>(lambda.RunVoid9<T0, T1, T2, T3, T4, T5, T6, T7, T8>);
		}

		// Token: 0x06001702 RID: 5890 RVA: 0x0004413C File Offset: 0x0004233C
		internal TRet Run10<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, TRet>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			interpretedFrame.Data[4] = arg4;
			interpretedFrame.Data[5] = arg5;
			interpretedFrame.Data[6] = arg6;
			interpretedFrame.Data[7] = arg7;
			interpretedFrame.Data[8] = arg8;
			interpretedFrame.Data[9] = arg9;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
			return (TRet)((object)interpretedFrame.Pop());
		}

		// Token: 0x06001703 RID: 5891 RVA: 0x0004421C File Offset: 0x0004241C
		internal void RunVoid10<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			interpretedFrame.Data[4] = arg4;
			interpretedFrame.Data[5] = arg5;
			interpretedFrame.Data[6] = arg6;
			interpretedFrame.Data[7] = arg7;
			interpretedFrame.Data[8] = arg8;
			interpretedFrame.Data[9] = arg9;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
		}

		// Token: 0x06001704 RID: 5892 RVA: 0x000442F4 File Offset: 0x000424F4
		internal static Delegate MakeRun10<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, TRet>(LightLambda lambda)
		{
			return new Func<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, TRet>(lambda.Run10<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, TRet>);
		}

		// Token: 0x06001705 RID: 5893 RVA: 0x00044302 File Offset: 0x00042502
		internal static Delegate MakeRunVoid10<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(LightLambda lambda)
		{
			return new Action<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>(lambda.RunVoid10<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>);
		}

		// Token: 0x06001706 RID: 5894 RVA: 0x00044310 File Offset: 0x00042510
		internal TRet Run11<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TRet>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			interpretedFrame.Data[4] = arg4;
			interpretedFrame.Data[5] = arg5;
			interpretedFrame.Data[6] = arg6;
			interpretedFrame.Data[7] = arg7;
			interpretedFrame.Data[8] = arg8;
			interpretedFrame.Data[9] = arg9;
			interpretedFrame.Data[10] = arg10;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
			return (TRet)((object)interpretedFrame.Pop());
		}

		// Token: 0x06001707 RID: 5895 RVA: 0x00044400 File Offset: 0x00042600
		internal void RunVoid11<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			interpretedFrame.Data[4] = arg4;
			interpretedFrame.Data[5] = arg5;
			interpretedFrame.Data[6] = arg6;
			interpretedFrame.Data[7] = arg7;
			interpretedFrame.Data[8] = arg8;
			interpretedFrame.Data[9] = arg9;
			interpretedFrame.Data[10] = arg10;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
		}

		// Token: 0x06001708 RID: 5896 RVA: 0x000444E8 File Offset: 0x000426E8
		internal static Delegate MakeRun11<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TRet>(LightLambda lambda)
		{
			return new Func<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TRet>(lambda.Run11<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TRet>);
		}

		// Token: 0x06001709 RID: 5897 RVA: 0x000444F6 File Offset: 0x000426F6
		internal static Delegate MakeRunVoid11<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(LightLambda lambda)
		{
			return new Action<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(lambda.RunVoid11<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>);
		}

		// Token: 0x0600170A RID: 5898 RVA: 0x00044504 File Offset: 0x00042704
		internal TRet Run12<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TRet>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			interpretedFrame.Data[4] = arg4;
			interpretedFrame.Data[5] = arg5;
			interpretedFrame.Data[6] = arg6;
			interpretedFrame.Data[7] = arg7;
			interpretedFrame.Data[8] = arg8;
			interpretedFrame.Data[9] = arg9;
			interpretedFrame.Data[10] = arg10;
			interpretedFrame.Data[11] = arg11;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
			return (TRet)((object)interpretedFrame.Pop());
		}

		// Token: 0x0600170B RID: 5899 RVA: 0x00044604 File Offset: 0x00042804
		internal void RunVoid12<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			interpretedFrame.Data[4] = arg4;
			interpretedFrame.Data[5] = arg5;
			interpretedFrame.Data[6] = arg6;
			interpretedFrame.Data[7] = arg7;
			interpretedFrame.Data[8] = arg8;
			interpretedFrame.Data[9] = arg9;
			interpretedFrame.Data[10] = arg10;
			interpretedFrame.Data[11] = arg11;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
		}

		// Token: 0x0600170C RID: 5900 RVA: 0x000446FC File Offset: 0x000428FC
		internal static Delegate MakeRun12<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TRet>(LightLambda lambda)
		{
			return new Func<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TRet>(lambda.Run12<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TRet>);
		}

		// Token: 0x0600170D RID: 5901 RVA: 0x0004470A File Offset: 0x0004290A
		internal static Delegate MakeRunVoid12<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(LightLambda lambda)
		{
			return new Action<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(lambda.RunVoid12<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>);
		}

		// Token: 0x0600170E RID: 5902 RVA: 0x00044718 File Offset: 0x00042918
		internal TRet Run13<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TRet>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			interpretedFrame.Data[4] = arg4;
			interpretedFrame.Data[5] = arg5;
			interpretedFrame.Data[6] = arg6;
			interpretedFrame.Data[7] = arg7;
			interpretedFrame.Data[8] = arg8;
			interpretedFrame.Data[9] = arg9;
			interpretedFrame.Data[10] = arg10;
			interpretedFrame.Data[11] = arg11;
			interpretedFrame.Data[12] = arg12;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
			return (TRet)((object)interpretedFrame.Pop());
		}

		// Token: 0x0600170F RID: 5903 RVA: 0x00044828 File Offset: 0x00042A28
		internal void RunVoid13<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			interpretedFrame.Data[4] = arg4;
			interpretedFrame.Data[5] = arg5;
			interpretedFrame.Data[6] = arg6;
			interpretedFrame.Data[7] = arg7;
			interpretedFrame.Data[8] = arg8;
			interpretedFrame.Data[9] = arg9;
			interpretedFrame.Data[10] = arg10;
			interpretedFrame.Data[11] = arg11;
			interpretedFrame.Data[12] = arg12;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
		}

		// Token: 0x06001710 RID: 5904 RVA: 0x00044930 File Offset: 0x00042B30
		internal static Delegate MakeRun13<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TRet>(LightLambda lambda)
		{
			return new Func<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TRet>(lambda.Run13<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TRet>);
		}

		// Token: 0x06001711 RID: 5905 RVA: 0x0004493E File Offset: 0x00042B3E
		internal static Delegate MakeRunVoid13<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(LightLambda lambda)
		{
			return new Action<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(lambda.RunVoid13<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>);
		}

		// Token: 0x06001712 RID: 5906 RVA: 0x0004494C File Offset: 0x00042B4C
		internal TRet Run14<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TRet>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			interpretedFrame.Data[4] = arg4;
			interpretedFrame.Data[5] = arg5;
			interpretedFrame.Data[6] = arg6;
			interpretedFrame.Data[7] = arg7;
			interpretedFrame.Data[8] = arg8;
			interpretedFrame.Data[9] = arg9;
			interpretedFrame.Data[10] = arg10;
			interpretedFrame.Data[11] = arg11;
			interpretedFrame.Data[12] = arg12;
			interpretedFrame.Data[13] = arg13;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
			return (TRet)((object)interpretedFrame.Pop());
		}

		// Token: 0x06001713 RID: 5907 RVA: 0x00044A6C File Offset: 0x00042C6C
		internal void RunVoid14<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			interpretedFrame.Data[4] = arg4;
			interpretedFrame.Data[5] = arg5;
			interpretedFrame.Data[6] = arg6;
			interpretedFrame.Data[7] = arg7;
			interpretedFrame.Data[8] = arg8;
			interpretedFrame.Data[9] = arg9;
			interpretedFrame.Data[10] = arg10;
			interpretedFrame.Data[11] = arg11;
			interpretedFrame.Data[12] = arg12;
			interpretedFrame.Data[13] = arg13;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
		}

		// Token: 0x06001714 RID: 5908 RVA: 0x00044B84 File Offset: 0x00042D84
		internal static Delegate MakeRun14<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TRet>(LightLambda lambda)
		{
			return new Func<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TRet>(lambda.Run14<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TRet>);
		}

		// Token: 0x06001715 RID: 5909 RVA: 0x00044B92 File Offset: 0x00042D92
		internal static Delegate MakeRunVoid14<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(LightLambda lambda)
		{
			return new Action<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(lambda.RunVoid14<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>);
		}

		// Token: 0x06001716 RID: 5910 RVA: 0x00044BA0 File Offset: 0x00042DA0
		internal TRet Run15<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TRet>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13, T14 arg14)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			interpretedFrame.Data[4] = arg4;
			interpretedFrame.Data[5] = arg5;
			interpretedFrame.Data[6] = arg6;
			interpretedFrame.Data[7] = arg7;
			interpretedFrame.Data[8] = arg8;
			interpretedFrame.Data[9] = arg9;
			interpretedFrame.Data[10] = arg10;
			interpretedFrame.Data[11] = arg11;
			interpretedFrame.Data[12] = arg12;
			interpretedFrame.Data[13] = arg13;
			interpretedFrame.Data[14] = arg14;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
			return (TRet)((object)interpretedFrame.Pop());
		}

		// Token: 0x06001717 RID: 5911 RVA: 0x00044CD0 File Offset: 0x00042ED0
		internal void RunVoid15<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13, T14 arg14)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			interpretedFrame.Data[2] = arg2;
			interpretedFrame.Data[3] = arg3;
			interpretedFrame.Data[4] = arg4;
			interpretedFrame.Data[5] = arg5;
			interpretedFrame.Data[6] = arg6;
			interpretedFrame.Data[7] = arg7;
			interpretedFrame.Data[8] = arg8;
			interpretedFrame.Data[9] = arg9;
			interpretedFrame.Data[10] = arg10;
			interpretedFrame.Data[11] = arg11;
			interpretedFrame.Data[12] = arg12;
			interpretedFrame.Data[13] = arg13;
			interpretedFrame.Data[14] = arg14;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
			}
		}

		// Token: 0x06001718 RID: 5912 RVA: 0x00044DF8 File Offset: 0x00042FF8
		internal static Delegate MakeRun15<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TRet>(LightLambda lambda)
		{
			return new Func<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TRet>(lambda.Run15<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TRet>);
		}

		// Token: 0x06001719 RID: 5913 RVA: 0x00044E06 File Offset: 0x00043006
		internal static Delegate MakeRunVoid15<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(LightLambda lambda)
		{
			return new Action<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(lambda.RunVoid15<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>);
		}

		// Token: 0x0600171A RID: 5914 RVA: 0x00044E14 File Offset: 0x00043014
		internal LightLambda(LightDelegateCreator delegateCreator, IStrongBox[] closure)
		{
			this._delegateCreator = delegateCreator;
			this._closure = closure;
			this._interpreter = delegateCreator.Interpreter;
		}

		// Token: 0x1700046A RID: 1130
		// (get) Token: 0x0600171B RID: 5915 RVA: 0x00044E36 File Offset: 0x00043036
		internal string DebugView
		{
			get
			{
				return new LightLambda.DebugViewPrinter(this._interpreter).ToString();
			}
		}

		// Token: 0x0600171C RID: 5916 RVA: 0x00044E48 File Offset: 0x00043048
		private static Func<LightLambda, Delegate> GetRunDelegateCtor(Type delegateType)
		{
			CacheDict<Type, Func<LightLambda, Delegate>> runCache = LightLambda._runCache;
			Func<LightLambda, Delegate> result;
			lock (runCache)
			{
				Func<LightLambda, Delegate> func;
				if (LightLambda._runCache.TryGetValue(delegateType, out func))
				{
					result = func;
				}
				else
				{
					result = LightLambda.MakeRunDelegateCtor(delegateType);
				}
			}
			return result;
		}

		// Token: 0x0600171D RID: 5917 RVA: 0x00044E9C File Offset: 0x0004309C
		private static Func<LightLambda, Delegate> MakeRunDelegateCtor(Type delegateType)
		{
			MethodInfo invokeMethod = delegateType.GetInvokeMethod();
			ParameterInfo[] parametersCached = invokeMethod.GetParametersCached();
			string text = "Run";
			if (parametersCached.Length >= 16)
			{
				return null;
			}
			Type[] array;
			if (invokeMethod.ReturnType == typeof(void))
			{
				text += "Void";
				array = new Type[parametersCached.Length];
			}
			else
			{
				array = new Type[parametersCached.Length + 1];
				array[array.Length - 1] = invokeMethod.ReturnType;
			}
			MethodInfo method;
			if (invokeMethod.ReturnType == typeof(void) && array.Length == 2 && parametersCached[0].ParameterType.IsByRef && parametersCached[1].ParameterType.IsByRef)
			{
				method = typeof(LightLambda).GetMethod("RunVoidRef2", BindingFlags.Instance | BindingFlags.NonPublic);
				array[0] = parametersCached[0].ParameterType.GetElementType();
				array[1] = parametersCached[1].ParameterType.GetElementType();
			}
			else if (invokeMethod.ReturnType == typeof(void) && array.Length == 0)
			{
				method = typeof(LightLambda).GetMethod("RunVoid0", BindingFlags.Instance | BindingFlags.NonPublic);
			}
			else
			{
				for (int i = 0; i < parametersCached.Length; i++)
				{
					array[i] = parametersCached[i].ParameterType;
					if (array[i].IsByRef)
					{
						return null;
					}
				}
				if (DelegateHelpers.MakeDelegate(array) == delegateType)
				{
					text = "Make" + text + parametersCached.Length;
					MethodInfo methodInfo = typeof(LightLambda).GetMethod(text, BindingFlags.Static | BindingFlags.NonPublic).MakeGenericMethod(array);
					return LightLambda._runCache[delegateType] = (Func<LightLambda, Delegate>)methodInfo.CreateDelegate(typeof(Func<LightLambda, Delegate>));
				}
				method = typeof(LightLambda).GetMethod(text + parametersCached.Length, BindingFlags.Instance | BindingFlags.NonPublic);
			}
			MethodInfo targetMethod = method.IsGenericMethodDefinition ? method.MakeGenericMethod(array) : method;
			return LightLambda._runCache[delegateType] = ((LightLambda lambda) => targetMethod.CreateDelegate(delegateType, lambda));
		}

		// Token: 0x0600171E RID: 5918 RVA: 0x000450CC File Offset: 0x000432CC
		private Delegate CreateCustomDelegate(Type delegateType)
		{
			MethodInfo invokeMethod = delegateType.GetInvokeMethod();
			ParameterInfo[] parametersCached = invokeMethod.GetParametersCached();
			ParameterExpression[] array = new ParameterExpression[parametersCached.Length];
			Expression[] array2 = new Expression[parametersCached.Length];
			bool flag = false;
			for (int i = 0; i < parametersCached.Length; i++)
			{
				ParameterExpression parameterExpression = Expression.Parameter(parametersCached[i].ParameterType, parametersCached[i].Name);
				flag = (flag || parametersCached[i].ParameterType.IsByRef);
				array[i] = parameterExpression;
				array2[i] = Expression.Convert(parameterExpression, typeof(object));
			}
			NewArrayExpression right = Expression.NewArrayInit(typeof(object), array2);
			ConstantExpression expression = Expression.Constant(new Func<object[], object>(this.Run));
			ParameterExpression parameterExpression2 = Expression.Parameter(typeof(object[]), "$args");
			Expression expression2;
			if (invokeMethod.ReturnType == typeof(void))
			{
				expression2 = Expression.Block(typeof(void), new Expression[]
				{
					Expression.Invoke(expression, parameterExpression2)
				});
			}
			else
			{
				expression2 = Expression.Convert(Expression.Invoke(expression, parameterExpression2), invokeMethod.ReturnType);
			}
			if (flag)
			{
				List<Expression> list = new List<Expression>();
				for (int j = 0; j < parametersCached.Length; j++)
				{
					if (parametersCached[j].ParameterType.IsByRef)
					{
						list.Add(Expression.Assign(array[j], Expression.Convert(Expression.ArrayAccess(parameterExpression2, new Expression[]
						{
							Expression.Constant(j)
						}), parametersCached[j].ParameterType.GetElementType())));
					}
				}
				expression2 = Expression.TryFinally(expression2, Expression.Block(typeof(void), list));
			}
			expression2 = Expression.Block(invokeMethod.ReturnType, new ParameterExpression[]
			{
				parameterExpression2
			}, new Expression[]
			{
				Expression.Assign(parameterExpression2, right),
				expression2
			});
			Expression.Lambda(delegateType, expression2, array);
			throw new NotImplementedException("byref delegate");
		}

		// Token: 0x0600171F RID: 5919 RVA: 0x000452B4 File Offset: 0x000434B4
		internal Delegate MakeDelegate(Type delegateType)
		{
			Func<LightLambda, Delegate> runDelegateCtor = LightLambda.GetRunDelegateCtor(delegateType);
			if (runDelegateCtor != null)
			{
				return runDelegateCtor(this);
			}
			return this.CreateCustomDelegate(delegateType);
		}

		// Token: 0x06001720 RID: 5920 RVA: 0x000452DA File Offset: 0x000434DA
		private InterpretedFrame MakeFrame()
		{
			return new InterpretedFrame(this._interpreter, this._closure);
		}

		// Token: 0x06001721 RID: 5921 RVA: 0x000452F0 File Offset: 0x000434F0
		internal void RunVoidRef2<T0, T1>(ref T0 arg0, ref T1 arg1)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			interpretedFrame.Data[0] = arg0;
			interpretedFrame.Data[1] = arg1;
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				interpretedFrame.Leave(prevFrame);
				arg0 = (T0)((object)interpretedFrame.Data[0]);
				arg1 = (T1)((object)interpretedFrame.Data[1]);
			}
		}

		// Token: 0x06001722 RID: 5922 RVA: 0x00045380 File Offset: 0x00043580
		public object Run(params object[] arguments)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			for (int i = 0; i < arguments.Length; i++)
			{
				interpretedFrame.Data[i] = arguments[i];
			}
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				for (int j = 0; j < arguments.Length; j++)
				{
					arguments[j] = interpretedFrame.Data[j];
				}
				interpretedFrame.Leave(prevFrame);
			}
			return interpretedFrame.Pop();
		}

		// Token: 0x06001723 RID: 5923 RVA: 0x000453FC File Offset: 0x000435FC
		public object RunVoid(params object[] arguments)
		{
			InterpretedFrame interpretedFrame = this.MakeFrame();
			for (int i = 0; i < arguments.Length; i++)
			{
				interpretedFrame.Data[i] = arguments[i];
			}
			InterpretedFrame prevFrame = interpretedFrame.Enter();
			try
			{
				this._interpreter.Run(interpretedFrame);
			}
			finally
			{
				for (int j = 0; j < arguments.Length; j++)
				{
					arguments[j] = interpretedFrame.Data[j];
				}
				interpretedFrame.Leave(prevFrame);
			}
			return null;
		}

		// Token: 0x06001724 RID: 5924 RVA: 0x00045470 File Offset: 0x00043670
		// Note: this type is marked as 'beforefieldinit'.
		static LightLambda()
		{
		}

		// Token: 0x04000B4C RID: 2892
		internal const int MaxParameters = 16;

		// Token: 0x04000B4D RID: 2893
		private readonly IStrongBox[] _closure;

		// Token: 0x04000B4E RID: 2894
		private readonly Interpreter _interpreter;

		// Token: 0x04000B4F RID: 2895
		private static readonly CacheDict<Type, Func<LightLambda, Delegate>> _runCache = new CacheDict<Type, Func<LightLambda, Delegate>>(100);

		// Token: 0x04000B50 RID: 2896
		private readonly LightDelegateCreator _delegateCreator;

		// Token: 0x02000364 RID: 868
		private class DebugViewPrinter
		{
			// Token: 0x06001725 RID: 5925 RVA: 0x00045480 File Offset: 0x00043680
			public DebugViewPrinter(Interpreter interpreter)
			{
				this._interpreter = interpreter;
				this.Analyze();
			}

			// Token: 0x06001726 RID: 5926 RVA: 0x000454CC File Offset: 0x000436CC
			private void Analyze()
			{
				foreach (Instruction instruction in this._interpreter.Instructions.Instructions)
				{
					EnterTryCatchFinallyInstruction enterTryCatchFinallyInstruction = instruction as EnterTryCatchFinallyInstruction;
					if (enterTryCatchFinallyInstruction != null)
					{
						TryCatchFinallyHandler handler = enterTryCatchFinallyInstruction.Handler;
						this.AddTryStart(handler.TryStartIndex);
						this.AddHandlerExit(handler.TryEndIndex + 1);
						if (handler.IsFinallyBlockExist)
						{
							this._handlerEnter.Add(handler.FinallyStartIndex, "finally");
							this.AddHandlerExit(handler.FinallyEndIndex);
						}
						if (handler.IsCatchBlockExist)
						{
							foreach (ExceptionHandler exceptionHandler in handler.Handlers)
							{
								this._handlerEnter.Add(exceptionHandler.HandlerStartIndex - 1, exceptionHandler.ToString());
								this.AddHandlerExit(exceptionHandler.HandlerEndIndex);
								ExceptionFilter filter = exceptionHandler.Filter;
								if (filter != null)
								{
									this._handlerEnter.Add(filter.StartIndex - 1, "filter");
									this.AddHandlerExit(filter.EndIndex);
								}
							}
						}
					}
					EnterTryFaultInstruction enterTryFaultInstruction = instruction as EnterTryFaultInstruction;
					if (enterTryFaultInstruction != null)
					{
						TryFaultHandler handler2 = enterTryFaultInstruction.Handler;
						this.AddTryStart(handler2.TryStartIndex);
						this.AddHandlerExit(handler2.TryEndIndex + 1);
						this._handlerEnter.Add(handler2.FinallyStartIndex, "fault");
						this.AddHandlerExit(handler2.FinallyEndIndex);
					}
				}
			}

			// Token: 0x06001727 RID: 5927 RVA: 0x0004563C File Offset: 0x0004383C
			private void AddTryStart(int index)
			{
				int num;
				if (!this._tryStart.TryGetValue(index, out num))
				{
					this._tryStart.Add(index, 1);
					return;
				}
				this._tryStart[index] = num + 1;
			}

			// Token: 0x06001728 RID: 5928 RVA: 0x00045678 File Offset: 0x00043878
			private void AddHandlerExit(int index)
			{
				int num;
				this._handlerExit[index] = (this._handlerExit.TryGetValue(index, out num) ? (num + 1) : 1);
			}

			// Token: 0x06001729 RID: 5929 RVA: 0x000456A7 File Offset: 0x000438A7
			private void Indent()
			{
				this._indent = new string(' ', this._indent.Length + 2);
			}

			// Token: 0x0600172A RID: 5930 RVA: 0x000456C3 File Offset: 0x000438C3
			private void Dedent()
			{
				this._indent = new string(' ', this._indent.Length - 2);
			}

			// Token: 0x0600172B RID: 5931 RVA: 0x000456E0 File Offset: 0x000438E0
			public override string ToString()
			{
				StringBuilder stringBuilder = new StringBuilder();
				string value = this._interpreter.Name ?? "lambda_method";
				stringBuilder.Append("object ").Append(value).AppendLine("(object[])");
				stringBuilder.AppendLine("{");
				stringBuilder.Append("  .locals ").Append(this._interpreter.LocalCount).AppendLine();
				stringBuilder.Append("  .maxstack ").Append(this._interpreter.Instructions.MaxStackDepth).AppendLine();
				stringBuilder.Append("  .maxcontinuation ").Append(this._interpreter.Instructions.MaxContinuationDepth).AppendLine();
				stringBuilder.AppendLine();
				Instruction[] instructions = this._interpreter.Instructions.Instructions;
				InstructionList.DebugView.InstructionView[] instructionViews = new InstructionArray.DebugView(this._interpreter.Instructions).GetInstructionViews(false);
				for (int i = 0; i < instructions.Length; i++)
				{
					this.EmitExits(stringBuilder, i);
					int num;
					if (this._tryStart.TryGetValue(i, out num))
					{
						for (int j = 0; j < num; j++)
						{
							stringBuilder.Append(this._indent).AppendLine(".try");
							stringBuilder.Append(this._indent).AppendLine("{");
							this.Indent();
						}
					}
					string value2;
					if (this._handlerEnter.TryGetValue(i, out value2))
					{
						stringBuilder.Append(this._indent).AppendLine(value2);
						stringBuilder.Append(this._indent).AppendLine("{");
						this.Indent();
					}
					InstructionList.DebugView.InstructionView instructionView = instructionViews[i];
					stringBuilder.AppendFormat(CultureInfo.InvariantCulture, "{0}IP_{1}: {2}", this._indent, i.ToString().PadLeft(4, '0'), instructionView.GetValue()).AppendLine();
				}
				this.EmitExits(stringBuilder, instructions.Length);
				stringBuilder.AppendLine("}");
				return stringBuilder.ToString();
			}

			// Token: 0x0600172C RID: 5932 RVA: 0x000458E0 File Offset: 0x00043AE0
			private void EmitExits(StringBuilder sb, int index)
			{
				int num;
				if (this._handlerExit.TryGetValue(index, out num))
				{
					for (int i = 0; i < num; i++)
					{
						this.Dedent();
						sb.Append(this._indent).AppendLine("}");
					}
				}
			}

			// Token: 0x04000B51 RID: 2897
			private readonly Interpreter _interpreter;

			// Token: 0x04000B52 RID: 2898
			private readonly Dictionary<int, int> _tryStart = new Dictionary<int, int>();

			// Token: 0x04000B53 RID: 2899
			private readonly Dictionary<int, string> _handlerEnter = new Dictionary<int, string>();

			// Token: 0x04000B54 RID: 2900
			private readonly Dictionary<int, int> _handlerExit = new Dictionary<int, int>();

			// Token: 0x04000B55 RID: 2901
			private string _indent = "  ";
		}

		// Token: 0x02000365 RID: 869
		[CompilerGenerated]
		private sealed class <>c__DisplayClass74_0
		{
			// Token: 0x0600172D RID: 5933 RVA: 0x00002310 File Offset: 0x00000510
			public <>c__DisplayClass74_0()
			{
			}

			// Token: 0x0600172E RID: 5934 RVA: 0x00045926 File Offset: 0x00043B26
			internal Delegate <MakeRunDelegateCtor>b__0(LightLambda lambda)
			{
				return this.targetMethod.CreateDelegate(this.delegateType, lambda);
			}

			// Token: 0x04000B56 RID: 2902
			public MethodInfo targetMethod;

			// Token: 0x04000B57 RID: 2903
			public Type delegateType;
		}
	}
}
