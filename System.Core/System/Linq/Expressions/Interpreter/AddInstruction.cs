﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200028C RID: 652
	internal abstract class AddInstruction : Instruction
	{
		// Token: 0x170003CD RID: 973
		// (get) Token: 0x0600135D RID: 4957 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x170003CE RID: 974
		// (get) Token: 0x0600135E RID: 4958 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170003CF RID: 975
		// (get) Token: 0x0600135F RID: 4959 RVA: 0x00037C4A File Offset: 0x00035E4A
		public override string InstructionName
		{
			get
			{
				return "Add";
			}
		}

		// Token: 0x06001360 RID: 4960 RVA: 0x00037C51 File Offset: 0x00035E51
		private AddInstruction()
		{
		}

		// Token: 0x06001361 RID: 4961 RVA: 0x00037C5C File Offset: 0x00035E5C
		public static Instruction Create(Type type)
		{
			switch (type.GetNonNullableType().GetTypeCode())
			{
			case TypeCode.Int16:
			{
				Instruction result;
				if ((result = AddInstruction.s_Int16) == null)
				{
					result = (AddInstruction.s_Int16 = new AddInstruction.AddInt16());
				}
				return result;
			}
			case TypeCode.UInt16:
			{
				Instruction result2;
				if ((result2 = AddInstruction.s_UInt16) == null)
				{
					result2 = (AddInstruction.s_UInt16 = new AddInstruction.AddUInt16());
				}
				return result2;
			}
			case TypeCode.Int32:
			{
				Instruction result3;
				if ((result3 = AddInstruction.s_Int32) == null)
				{
					result3 = (AddInstruction.s_Int32 = new AddInstruction.AddInt32());
				}
				return result3;
			}
			case TypeCode.UInt32:
			{
				Instruction result4;
				if ((result4 = AddInstruction.s_UInt32) == null)
				{
					result4 = (AddInstruction.s_UInt32 = new AddInstruction.AddUInt32());
				}
				return result4;
			}
			case TypeCode.Int64:
			{
				Instruction result5;
				if ((result5 = AddInstruction.s_Int64) == null)
				{
					result5 = (AddInstruction.s_Int64 = new AddInstruction.AddInt64());
				}
				return result5;
			}
			case TypeCode.UInt64:
			{
				Instruction result6;
				if ((result6 = AddInstruction.s_UInt64) == null)
				{
					result6 = (AddInstruction.s_UInt64 = new AddInstruction.AddUInt64());
				}
				return result6;
			}
			case TypeCode.Single:
			{
				Instruction result7;
				if ((result7 = AddInstruction.s_Single) == null)
				{
					result7 = (AddInstruction.s_Single = new AddInstruction.AddSingle());
				}
				return result7;
			}
			case TypeCode.Double:
			{
				Instruction result8;
				if ((result8 = AddInstruction.s_Double) == null)
				{
					result8 = (AddInstruction.s_Double = new AddInstruction.AddDouble());
				}
				return result8;
			}
			default:
				throw ContractUtils.Unreachable;
			}
		}

		// Token: 0x040009CA RID: 2506
		private static Instruction s_Int16;

		// Token: 0x040009CB RID: 2507
		private static Instruction s_Int32;

		// Token: 0x040009CC RID: 2508
		private static Instruction s_Int64;

		// Token: 0x040009CD RID: 2509
		private static Instruction s_UInt16;

		// Token: 0x040009CE RID: 2510
		private static Instruction s_UInt32;

		// Token: 0x040009CF RID: 2511
		private static Instruction s_UInt64;

		// Token: 0x040009D0 RID: 2512
		private static Instruction s_Single;

		// Token: 0x040009D1 RID: 2513
		private static Instruction s_Double;

		// Token: 0x0200028D RID: 653
		private sealed class AddInt16 : AddInstruction
		{
			// Token: 0x06001362 RID: 4962 RVA: 0x00037D50 File Offset: 0x00035F50
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((short)obj + (short)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x06001363 RID: 4963 RVA: 0x00037DA2 File Offset: 0x00035FA2
			public AddInt16()
			{
			}
		}

		// Token: 0x0200028E RID: 654
		private sealed class AddInt32 : AddInstruction
		{
			// Token: 0x06001364 RID: 4964 RVA: 0x00037DAC File Offset: 0x00035FAC
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ScriptingRuntimeHelpers.Int32ToObject((int)obj + (int)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x06001365 RID: 4965 RVA: 0x00037DA2 File Offset: 0x00035FA2
			public AddInt32()
			{
			}
		}

		// Token: 0x0200028F RID: 655
		private sealed class AddInt64 : AddInstruction
		{
			// Token: 0x06001366 RID: 4966 RVA: 0x00037E00 File Offset: 0x00036000
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((long)obj + (long)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x06001367 RID: 4967 RVA: 0x00037DA2 File Offset: 0x00035FA2
			public AddInt64()
			{
			}
		}

		// Token: 0x02000290 RID: 656
		private sealed class AddUInt16 : AddInstruction
		{
			// Token: 0x06001368 RID: 4968 RVA: 0x00037E54 File Offset: 0x00036054
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((ushort)obj + (ushort)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x06001369 RID: 4969 RVA: 0x00037DA2 File Offset: 0x00035FA2
			public AddUInt16()
			{
			}
		}

		// Token: 0x02000291 RID: 657
		private sealed class AddUInt32 : AddInstruction
		{
			// Token: 0x0600136A RID: 4970 RVA: 0x00037EA8 File Offset: 0x000360A8
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((uint)obj + (uint)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x0600136B RID: 4971 RVA: 0x00037DA2 File Offset: 0x00035FA2
			public AddUInt32()
			{
			}
		}

		// Token: 0x02000292 RID: 658
		private sealed class AddUInt64 : AddInstruction
		{
			// Token: 0x0600136C RID: 4972 RVA: 0x00037EFC File Offset: 0x000360FC
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((ulong)obj + (ulong)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x0600136D RID: 4973 RVA: 0x00037DA2 File Offset: 0x00035FA2
			public AddUInt64()
			{
			}
		}

		// Token: 0x02000293 RID: 659
		private sealed class AddSingle : AddInstruction
		{
			// Token: 0x0600136E RID: 4974 RVA: 0x00037F50 File Offset: 0x00036150
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((float)obj + (float)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x0600136F RID: 4975 RVA: 0x00037DA2 File Offset: 0x00035FA2
			public AddSingle()
			{
			}
		}

		// Token: 0x02000294 RID: 660
		private sealed class AddDouble : AddInstruction
		{
			// Token: 0x06001370 RID: 4976 RVA: 0x00037FA4 File Offset: 0x000361A4
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((double)obj + (double)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x06001371 RID: 4977 RVA: 0x00037DA2 File Offset: 0x00035FA2
			public AddDouble()
			{
			}
		}
	}
}
