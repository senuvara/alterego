﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020003BE RID: 958
	internal abstract class NotInstruction : Instruction
	{
		// Token: 0x0600182B RID: 6187 RVA: 0x00037C51 File Offset: 0x00035E51
		private NotInstruction()
		{
		}

		// Token: 0x170004AA RID: 1194
		// (get) Token: 0x0600182C RID: 6188 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004AB RID: 1195
		// (get) Token: 0x0600182D RID: 6189 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004AC RID: 1196
		// (get) Token: 0x0600182E RID: 6190 RVA: 0x00047A1A File Offset: 0x00045C1A
		public override string InstructionName
		{
			get
			{
				return "Not";
			}
		}

		// Token: 0x0600182F RID: 6191 RVA: 0x00047A24 File Offset: 0x00045C24
		public static Instruction Create(Type type)
		{
			switch (type.GetNonNullableType().GetTypeCode())
			{
			case TypeCode.Boolean:
			{
				Instruction result;
				if ((result = NotInstruction.s_Boolean) == null)
				{
					result = (NotInstruction.s_Boolean = new NotInstruction.NotBoolean());
				}
				return result;
			}
			case TypeCode.SByte:
			{
				Instruction result2;
				if ((result2 = NotInstruction.s_SByte) == null)
				{
					result2 = (NotInstruction.s_SByte = new NotInstruction.NotSByte());
				}
				return result2;
			}
			case TypeCode.Byte:
			{
				Instruction result3;
				if ((result3 = NotInstruction.s_Byte) == null)
				{
					result3 = (NotInstruction.s_Byte = new NotInstruction.NotByte());
				}
				return result3;
			}
			case TypeCode.Int16:
			{
				Instruction result4;
				if ((result4 = NotInstruction.s_Int16) == null)
				{
					result4 = (NotInstruction.s_Int16 = new NotInstruction.NotInt16());
				}
				return result4;
			}
			case TypeCode.UInt16:
			{
				Instruction result5;
				if ((result5 = NotInstruction.s_UInt16) == null)
				{
					result5 = (NotInstruction.s_UInt16 = new NotInstruction.NotUInt16());
				}
				return result5;
			}
			case TypeCode.Int32:
			{
				Instruction result6;
				if ((result6 = NotInstruction.s_Int32) == null)
				{
					result6 = (NotInstruction.s_Int32 = new NotInstruction.NotInt32());
				}
				return result6;
			}
			case TypeCode.UInt32:
			{
				Instruction result7;
				if ((result7 = NotInstruction.s_UInt32) == null)
				{
					result7 = (NotInstruction.s_UInt32 = new NotInstruction.NotUInt32());
				}
				return result7;
			}
			case TypeCode.Int64:
			{
				Instruction result8;
				if ((result8 = NotInstruction.s_Int64) == null)
				{
					result8 = (NotInstruction.s_Int64 = new NotInstruction.NotInt64());
				}
				return result8;
			}
			case TypeCode.UInt64:
			{
				Instruction result9;
				if ((result9 = NotInstruction.s_UInt64) == null)
				{
					result9 = (NotInstruction.s_UInt64 = new NotInstruction.NotUInt64());
				}
				return result9;
			}
			}
			throw ContractUtils.Unreachable;
		}

		// Token: 0x04000BA7 RID: 2983
		public static Instruction s_Boolean;

		// Token: 0x04000BA8 RID: 2984
		public static Instruction s_Int64;

		// Token: 0x04000BA9 RID: 2985
		public static Instruction s_Int32;

		// Token: 0x04000BAA RID: 2986
		public static Instruction s_Int16;

		// Token: 0x04000BAB RID: 2987
		public static Instruction s_UInt64;

		// Token: 0x04000BAC RID: 2988
		public static Instruction s_UInt32;

		// Token: 0x04000BAD RID: 2989
		public static Instruction s_UInt16;

		// Token: 0x04000BAE RID: 2990
		public static Instruction s_Byte;

		// Token: 0x04000BAF RID: 2991
		public static Instruction s_SByte;

		// Token: 0x020003BF RID: 959
		private sealed class NotBoolean : NotInstruction
		{
			// Token: 0x06001830 RID: 6192 RVA: 0x00047B34 File Offset: 0x00045D34
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(!(bool)obj);
				}
				return 1;
			}

			// Token: 0x06001831 RID: 6193 RVA: 0x00047B64 File Offset: 0x00045D64
			public NotBoolean()
			{
			}
		}

		// Token: 0x020003C0 RID: 960
		private sealed class NotInt64 : NotInstruction
		{
			// Token: 0x06001832 RID: 6194 RVA: 0x00047B6C File Offset: 0x00045D6C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(~(long)obj);
				}
				return 1;
			}

			// Token: 0x06001833 RID: 6195 RVA: 0x00047B64 File Offset: 0x00045D64
			public NotInt64()
			{
			}
		}

		// Token: 0x020003C1 RID: 961
		private sealed class NotInt32 : NotInstruction
		{
			// Token: 0x06001834 RID: 6196 RVA: 0x00047BA0 File Offset: 0x00045DA0
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(~(int)obj);
				}
				return 1;
			}

			// Token: 0x06001835 RID: 6197 RVA: 0x00047B64 File Offset: 0x00045D64
			public NotInt32()
			{
			}
		}

		// Token: 0x020003C2 RID: 962
		private sealed class NotInt16 : NotInstruction
		{
			// Token: 0x06001836 RID: 6198 RVA: 0x00047BD0 File Offset: 0x00045DD0
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(~(short)obj);
				}
				return 1;
			}

			// Token: 0x06001837 RID: 6199 RVA: 0x00047B64 File Offset: 0x00045D64
			public NotInt16()
			{
			}
		}

		// Token: 0x020003C3 RID: 963
		private sealed class NotUInt64 : NotInstruction
		{
			// Token: 0x06001838 RID: 6200 RVA: 0x00047C00 File Offset: 0x00045E00
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(~(ulong)obj);
				}
				return 1;
			}

			// Token: 0x06001839 RID: 6201 RVA: 0x00047B64 File Offset: 0x00045D64
			public NotUInt64()
			{
			}
		}

		// Token: 0x020003C4 RID: 964
		private sealed class NotUInt32 : NotInstruction
		{
			// Token: 0x0600183A RID: 6202 RVA: 0x00047C34 File Offset: 0x00045E34
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(~(uint)obj);
				}
				return 1;
			}

			// Token: 0x0600183B RID: 6203 RVA: 0x00047B64 File Offset: 0x00045D64
			public NotUInt32()
			{
			}
		}

		// Token: 0x020003C5 RID: 965
		private sealed class NotUInt16 : NotInstruction
		{
			// Token: 0x0600183C RID: 6204 RVA: 0x00047C68 File Offset: 0x00045E68
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(~(ushort)obj);
				}
				return 1;
			}

			// Token: 0x0600183D RID: 6205 RVA: 0x00047B64 File Offset: 0x00045D64
			public NotUInt16()
			{
			}
		}

		// Token: 0x020003C6 RID: 966
		private sealed class NotByte : NotInstruction
		{
			// Token: 0x0600183E RID: 6206 RVA: 0x00047C98 File Offset: 0x00045E98
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(~(byte)obj);
				}
				return 1;
			}

			// Token: 0x0600183F RID: 6207 RVA: 0x00047B64 File Offset: 0x00045D64
			public NotByte()
			{
			}
		}

		// Token: 0x020003C7 RID: 967
		private sealed class NotSByte : NotInstruction
		{
			// Token: 0x06001840 RID: 6208 RVA: 0x00047CC8 File Offset: 0x00045EC8
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(~(sbyte)obj);
				}
				return 1;
			}

			// Token: 0x06001841 RID: 6209 RVA: 0x00047B64 File Offset: 0x00045D64
			public NotSByte()
			{
			}
		}
	}
}
