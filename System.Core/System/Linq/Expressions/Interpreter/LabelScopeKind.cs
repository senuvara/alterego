﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200032E RID: 814
	internal enum LabelScopeKind
	{
		// Token: 0x04000AD4 RID: 2772
		Statement,
		// Token: 0x04000AD5 RID: 2773
		Block,
		// Token: 0x04000AD6 RID: 2774
		Switch,
		// Token: 0x04000AD7 RID: 2775
		Lambda,
		// Token: 0x04000AD8 RID: 2776
		Try,
		// Token: 0x04000AD9 RID: 2777
		Catch,
		// Token: 0x04000ADA RID: 2778
		Finally,
		// Token: 0x04000ADB RID: 2779
		Filter,
		// Token: 0x04000ADC RID: 2780
		Expression
	}
}
