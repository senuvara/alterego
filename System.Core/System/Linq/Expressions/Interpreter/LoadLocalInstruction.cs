﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000368 RID: 872
	internal sealed class LoadLocalInstruction : LocalAccessInstruction, IBoxableInstruction
	{
		// Token: 0x06001732 RID: 5938 RVA: 0x000459CD File Offset: 0x00043BCD
		internal LoadLocalInstruction(int index) : base(index)
		{
		}

		// Token: 0x1700046B RID: 1131
		// (get) Token: 0x06001733 RID: 5939 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x1700046C RID: 1132
		// (get) Token: 0x06001734 RID: 5940 RVA: 0x000459D6 File Offset: 0x00043BD6
		public override string InstructionName
		{
			get
			{
				return "LoadLocal";
			}
		}

		// Token: 0x06001735 RID: 5941 RVA: 0x000459E0 File Offset: 0x00043BE0
		public override int Run(InterpretedFrame frame)
		{
			object[] data = frame.Data;
			int stackIndex = frame.StackIndex;
			frame.StackIndex = stackIndex + 1;
			data[stackIndex] = frame.Data[this._index];
			return 1;
		}

		// Token: 0x06001736 RID: 5942 RVA: 0x00045A13 File Offset: 0x00043C13
		public Instruction BoxIfIndexMatches(int index)
		{
			if (index != this._index)
			{
				return null;
			}
			return InstructionList.LoadLocalBoxed(index);
		}
	}
}
