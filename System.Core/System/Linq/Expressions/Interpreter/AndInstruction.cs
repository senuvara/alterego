﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200029C RID: 668
	internal abstract class AndInstruction : Instruction
	{
		// Token: 0x170003D3 RID: 979
		// (get) Token: 0x06001383 RID: 4995 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x170003D4 RID: 980
		// (get) Token: 0x06001384 RID: 4996 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170003D5 RID: 981
		// (get) Token: 0x06001385 RID: 4997 RVA: 0x000382B9 File Offset: 0x000364B9
		public override string InstructionName
		{
			get
			{
				return "And";
			}
		}

		// Token: 0x06001386 RID: 4998 RVA: 0x00037C51 File Offset: 0x00035E51
		private AndInstruction()
		{
		}

		// Token: 0x06001387 RID: 4999 RVA: 0x000382C0 File Offset: 0x000364C0
		public static Instruction Create(Type type)
		{
			switch (type.GetNonNullableType().GetTypeCode())
			{
			case TypeCode.Boolean:
			{
				Instruction result;
				if ((result = AndInstruction.s_Boolean) == null)
				{
					result = (AndInstruction.s_Boolean = new AndInstruction.AndBoolean());
				}
				return result;
			}
			case TypeCode.SByte:
			{
				Instruction result2;
				if ((result2 = AndInstruction.s_SByte) == null)
				{
					result2 = (AndInstruction.s_SByte = new AndInstruction.AndSByte());
				}
				return result2;
			}
			case TypeCode.Byte:
			{
				Instruction result3;
				if ((result3 = AndInstruction.s_Byte) == null)
				{
					result3 = (AndInstruction.s_Byte = new AndInstruction.AndByte());
				}
				return result3;
			}
			case TypeCode.Int16:
			{
				Instruction result4;
				if ((result4 = AndInstruction.s_Int16) == null)
				{
					result4 = (AndInstruction.s_Int16 = new AndInstruction.AndInt16());
				}
				return result4;
			}
			case TypeCode.UInt16:
			{
				Instruction result5;
				if ((result5 = AndInstruction.s_UInt16) == null)
				{
					result5 = (AndInstruction.s_UInt16 = new AndInstruction.AndUInt16());
				}
				return result5;
			}
			case TypeCode.Int32:
			{
				Instruction result6;
				if ((result6 = AndInstruction.s_Int32) == null)
				{
					result6 = (AndInstruction.s_Int32 = new AndInstruction.AndInt32());
				}
				return result6;
			}
			case TypeCode.UInt32:
			{
				Instruction result7;
				if ((result7 = AndInstruction.s_UInt32) == null)
				{
					result7 = (AndInstruction.s_UInt32 = new AndInstruction.AndUInt32());
				}
				return result7;
			}
			case TypeCode.Int64:
			{
				Instruction result8;
				if ((result8 = AndInstruction.s_Int64) == null)
				{
					result8 = (AndInstruction.s_Int64 = new AndInstruction.AndInt64());
				}
				return result8;
			}
			case TypeCode.UInt64:
			{
				Instruction result9;
				if ((result9 = AndInstruction.s_UInt64) == null)
				{
					result9 = (AndInstruction.s_UInt64 = new AndInstruction.AndUInt64());
				}
				return result9;
			}
			}
			throw ContractUtils.Unreachable;
		}

		// Token: 0x040009D8 RID: 2520
		private static Instruction s_SByte;

		// Token: 0x040009D9 RID: 2521
		private static Instruction s_Int16;

		// Token: 0x040009DA RID: 2522
		private static Instruction s_Int32;

		// Token: 0x040009DB RID: 2523
		private static Instruction s_Int64;

		// Token: 0x040009DC RID: 2524
		private static Instruction s_Byte;

		// Token: 0x040009DD RID: 2525
		private static Instruction s_UInt16;

		// Token: 0x040009DE RID: 2526
		private static Instruction s_UInt32;

		// Token: 0x040009DF RID: 2527
		private static Instruction s_UInt64;

		// Token: 0x040009E0 RID: 2528
		private static Instruction s_Boolean;

		// Token: 0x0200029D RID: 669
		private sealed class AndSByte : AndInstruction
		{
			// Token: 0x06001388 RID: 5000 RVA: 0x000383D0 File Offset: 0x000365D0
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((sbyte)obj & (sbyte)obj2);
				return 1;
			}

			// Token: 0x06001389 RID: 5001 RVA: 0x0003840F File Offset: 0x0003660F
			public AndSByte()
			{
			}
		}

		// Token: 0x0200029E RID: 670
		private sealed class AndInt16 : AndInstruction
		{
			// Token: 0x0600138A RID: 5002 RVA: 0x00038418 File Offset: 0x00036618
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((short)obj & (short)obj2);
				return 1;
			}

			// Token: 0x0600138B RID: 5003 RVA: 0x0003840F File Offset: 0x0003660F
			public AndInt16()
			{
			}
		}

		// Token: 0x0200029F RID: 671
		private sealed class AndInt32 : AndInstruction
		{
			// Token: 0x0600138C RID: 5004 RVA: 0x00038458 File Offset: 0x00036658
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((int)obj & (int)obj2);
				return 1;
			}

			// Token: 0x0600138D RID: 5005 RVA: 0x0003840F File Offset: 0x0003660F
			public AndInt32()
			{
			}
		}

		// Token: 0x020002A0 RID: 672
		private sealed class AndInt64 : AndInstruction
		{
			// Token: 0x0600138E RID: 5006 RVA: 0x00038498 File Offset: 0x00036698
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((long)obj & (long)obj2);
				return 1;
			}

			// Token: 0x0600138F RID: 5007 RVA: 0x0003840F File Offset: 0x0003660F
			public AndInt64()
			{
			}
		}

		// Token: 0x020002A1 RID: 673
		private sealed class AndByte : AndInstruction
		{
			// Token: 0x06001390 RID: 5008 RVA: 0x000384DC File Offset: 0x000366DC
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((byte)obj & (byte)obj2);
				return 1;
			}

			// Token: 0x06001391 RID: 5009 RVA: 0x0003840F File Offset: 0x0003660F
			public AndByte()
			{
			}
		}

		// Token: 0x020002A2 RID: 674
		private sealed class AndUInt16 : AndInstruction
		{
			// Token: 0x06001392 RID: 5010 RVA: 0x0003851C File Offset: 0x0003671C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((ushort)obj & (ushort)obj2);
				return 1;
			}

			// Token: 0x06001393 RID: 5011 RVA: 0x0003840F File Offset: 0x0003660F
			public AndUInt16()
			{
			}
		}

		// Token: 0x020002A3 RID: 675
		private sealed class AndUInt32 : AndInstruction
		{
			// Token: 0x06001394 RID: 5012 RVA: 0x0003855C File Offset: 0x0003675C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((uint)obj & (uint)obj2);
				return 1;
			}

			// Token: 0x06001395 RID: 5013 RVA: 0x0003840F File Offset: 0x0003660F
			public AndUInt32()
			{
			}
		}

		// Token: 0x020002A4 RID: 676
		private sealed class AndUInt64 : AndInstruction
		{
			// Token: 0x06001396 RID: 5014 RVA: 0x000385A0 File Offset: 0x000367A0
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((ulong)obj & (ulong)obj2);
				return 1;
			}

			// Token: 0x06001397 RID: 5015 RVA: 0x0003840F File Offset: 0x0003660F
			public AndUInt64()
			{
			}
		}

		// Token: 0x020002A5 RID: 677
		private sealed class AndBoolean : AndInstruction
		{
			// Token: 0x06001398 RID: 5016 RVA: 0x000385E4 File Offset: 0x000367E4
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					if (obj == null)
					{
						frame.Push(null);
					}
					else
					{
						frame.Push(((bool)obj) ? null : Utils.BoxedFalse);
					}
					return 1;
				}
				if (obj == null)
				{
					frame.Push(((bool)obj2) ? null : Utils.BoxedFalse);
					return 1;
				}
				frame.Push((bool)obj2 & (bool)obj);
				return 1;
			}

			// Token: 0x06001399 RID: 5017 RVA: 0x0003840F File Offset: 0x0003660F
			public AndBoolean()
			{
			}
		}
	}
}
