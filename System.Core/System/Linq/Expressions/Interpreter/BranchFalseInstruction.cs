﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002B3 RID: 691
	internal sealed class BranchFalseInstruction : OffsetInstruction
	{
		// Token: 0x170003F1 RID: 1009
		// (get) Token: 0x060013E0 RID: 5088 RVA: 0x00038FF2 File Offset: 0x000371F2
		public override Instruction[] Cache
		{
			get
			{
				if (BranchFalseInstruction.s_cache == null)
				{
					BranchFalseInstruction.s_cache = new Instruction[32];
				}
				return BranchFalseInstruction.s_cache;
			}
		}

		// Token: 0x170003F2 RID: 1010
		// (get) Token: 0x060013E1 RID: 5089 RVA: 0x0003900C File Offset: 0x0003720C
		public override string InstructionName
		{
			get
			{
				return "BranchFalse";
			}
		}

		// Token: 0x170003F3 RID: 1011
		// (get) Token: 0x060013E2 RID: 5090 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x060013E3 RID: 5091 RVA: 0x00039013 File Offset: 0x00037213
		public override int Run(InterpretedFrame frame)
		{
			if (!(bool)frame.Pop())
			{
				return this._offset;
			}
			return 1;
		}

		// Token: 0x060013E4 RID: 5092 RVA: 0x0003902A File Offset: 0x0003722A
		public BranchFalseInstruction()
		{
		}

		// Token: 0x040009F9 RID: 2553
		private static Instruction[] s_cache;
	}
}
