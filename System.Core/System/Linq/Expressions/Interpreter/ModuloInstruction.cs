﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000380 RID: 896
	internal abstract class ModuloInstruction : Instruction
	{
		// Token: 0x17000494 RID: 1172
		// (get) Token: 0x06001797 RID: 6039 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x17000495 RID: 1173
		// (get) Token: 0x06001798 RID: 6040 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000496 RID: 1174
		// (get) Token: 0x06001799 RID: 6041 RVA: 0x000461EF File Offset: 0x000443EF
		public override string InstructionName
		{
			get
			{
				return "Modulo";
			}
		}

		// Token: 0x0600179A RID: 6042 RVA: 0x00037C51 File Offset: 0x00035E51
		private ModuloInstruction()
		{
		}

		// Token: 0x0600179B RID: 6043 RVA: 0x000461F8 File Offset: 0x000443F8
		public static Instruction Create(Type type)
		{
			switch (type.GetNonNullableType().GetTypeCode())
			{
			case TypeCode.Int16:
			{
				Instruction result;
				if ((result = ModuloInstruction.s_Int16) == null)
				{
					result = (ModuloInstruction.s_Int16 = new ModuloInstruction.ModuloInt16());
				}
				return result;
			}
			case TypeCode.UInt16:
			{
				Instruction result2;
				if ((result2 = ModuloInstruction.s_UInt16) == null)
				{
					result2 = (ModuloInstruction.s_UInt16 = new ModuloInstruction.ModuloUInt16());
				}
				return result2;
			}
			case TypeCode.Int32:
			{
				Instruction result3;
				if ((result3 = ModuloInstruction.s_Int32) == null)
				{
					result3 = (ModuloInstruction.s_Int32 = new ModuloInstruction.ModuloInt32());
				}
				return result3;
			}
			case TypeCode.UInt32:
			{
				Instruction result4;
				if ((result4 = ModuloInstruction.s_UInt32) == null)
				{
					result4 = (ModuloInstruction.s_UInt32 = new ModuloInstruction.ModuloUInt32());
				}
				return result4;
			}
			case TypeCode.Int64:
			{
				Instruction result5;
				if ((result5 = ModuloInstruction.s_Int64) == null)
				{
					result5 = (ModuloInstruction.s_Int64 = new ModuloInstruction.ModuloInt64());
				}
				return result5;
			}
			case TypeCode.UInt64:
			{
				Instruction result6;
				if ((result6 = ModuloInstruction.s_UInt64) == null)
				{
					result6 = (ModuloInstruction.s_UInt64 = new ModuloInstruction.ModuloUInt64());
				}
				return result6;
			}
			case TypeCode.Single:
			{
				Instruction result7;
				if ((result7 = ModuloInstruction.s_Single) == null)
				{
					result7 = (ModuloInstruction.s_Single = new ModuloInstruction.ModuloSingle());
				}
				return result7;
			}
			case TypeCode.Double:
			{
				Instruction result8;
				if ((result8 = ModuloInstruction.s_Double) == null)
				{
					result8 = (ModuloInstruction.s_Double = new ModuloInstruction.ModuloDouble());
				}
				return result8;
			}
			default:
				throw ContractUtils.Unreachable;
			}
		}

		// Token: 0x04000B6E RID: 2926
		private static Instruction s_Int16;

		// Token: 0x04000B6F RID: 2927
		private static Instruction s_Int32;

		// Token: 0x04000B70 RID: 2928
		private static Instruction s_Int64;

		// Token: 0x04000B71 RID: 2929
		private static Instruction s_UInt16;

		// Token: 0x04000B72 RID: 2930
		private static Instruction s_UInt32;

		// Token: 0x04000B73 RID: 2931
		private static Instruction s_UInt64;

		// Token: 0x04000B74 RID: 2932
		private static Instruction s_Single;

		// Token: 0x04000B75 RID: 2933
		private static Instruction s_Double;

		// Token: 0x02000381 RID: 897
		private sealed class ModuloInt16 : ModuloInstruction
		{
			// Token: 0x0600179C RID: 6044 RVA: 0x000462EC File Offset: 0x000444EC
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((short)obj % (short)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x0600179D RID: 6045 RVA: 0x0004633E File Offset: 0x0004453E
			public ModuloInt16()
			{
			}
		}

		// Token: 0x02000382 RID: 898
		private sealed class ModuloInt32 : ModuloInstruction
		{
			// Token: 0x0600179E RID: 6046 RVA: 0x00046348 File Offset: 0x00044548
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ScriptingRuntimeHelpers.Int32ToObject((int)obj % (int)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x0600179F RID: 6047 RVA: 0x0004633E File Offset: 0x0004453E
			public ModuloInt32()
			{
			}
		}

		// Token: 0x02000383 RID: 899
		private sealed class ModuloInt64 : ModuloInstruction
		{
			// Token: 0x060017A0 RID: 6048 RVA: 0x0004639C File Offset: 0x0004459C
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((long)obj % (long)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060017A1 RID: 6049 RVA: 0x0004633E File Offset: 0x0004453E
			public ModuloInt64()
			{
			}
		}

		// Token: 0x02000384 RID: 900
		private sealed class ModuloUInt16 : ModuloInstruction
		{
			// Token: 0x060017A2 RID: 6050 RVA: 0x000463F0 File Offset: 0x000445F0
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((ushort)obj % (ushort)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060017A3 RID: 6051 RVA: 0x0004633E File Offset: 0x0004453E
			public ModuloUInt16()
			{
			}
		}

		// Token: 0x02000385 RID: 901
		private sealed class ModuloUInt32 : ModuloInstruction
		{
			// Token: 0x060017A4 RID: 6052 RVA: 0x00046444 File Offset: 0x00044644
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((uint)obj % (uint)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060017A5 RID: 6053 RVA: 0x0004633E File Offset: 0x0004453E
			public ModuloUInt32()
			{
			}
		}

		// Token: 0x02000386 RID: 902
		private sealed class ModuloUInt64 : ModuloInstruction
		{
			// Token: 0x060017A6 RID: 6054 RVA: 0x00046498 File Offset: 0x00044698
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((ulong)obj % (ulong)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060017A7 RID: 6055 RVA: 0x0004633E File Offset: 0x0004453E
			public ModuloUInt64()
			{
			}
		}

		// Token: 0x02000387 RID: 903
		private sealed class ModuloSingle : ModuloInstruction
		{
			// Token: 0x060017A8 RID: 6056 RVA: 0x000464EC File Offset: 0x000446EC
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((float)obj % (float)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060017A9 RID: 6057 RVA: 0x0004633E File Offset: 0x0004453E
			public ModuloSingle()
			{
			}
		}

		// Token: 0x02000388 RID: 904
		private sealed class ModuloDouble : ModuloInstruction
		{
			// Token: 0x060017AA RID: 6058 RVA: 0x00046540 File Offset: 0x00044740
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((double)obj % (double)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060017AB RID: 6059 RVA: 0x0004633E File Offset: 0x0004453E
			public ModuloDouble()
			{
			}
		}
	}
}
