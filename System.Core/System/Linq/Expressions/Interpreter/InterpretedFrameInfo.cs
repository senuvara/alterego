﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000358 RID: 856
	internal struct InterpretedFrameInfo
	{
		// Token: 0x0600165A RID: 5722 RVA: 0x0003EB67 File Offset: 0x0003CD67
		public InterpretedFrameInfo(string methodName, DebugInfo info)
		{
			this._methodName = methodName;
			this._debugInfo = info;
		}

		// Token: 0x0600165B RID: 5723 RVA: 0x0003EB77 File Offset: 0x0003CD77
		public override string ToString()
		{
			if (this._debugInfo == null)
			{
				return this._methodName;
			}
			return this._methodName + ": " + this._debugInfo;
		}

		// Token: 0x04000B2E RID: 2862
		private readonly string _methodName;

		// Token: 0x04000B2F RID: 2863
		private readonly DebugInfo _debugInfo;
	}
}
