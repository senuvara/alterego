﻿using System;
using System.Collections.Generic;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200037E RID: 894
	internal sealed class LocalVariables
	{
		// Token: 0x0600178E RID: 6030 RVA: 0x00045F80 File Offset: 0x00044180
		public LocalDefinition DefineLocal(ParameterExpression variable, int start)
		{
			int localCount = this._localCount;
			this._localCount = localCount + 1;
			LocalVariable localVariable = new LocalVariable(localCount, false);
			this._maxLocalCount = Math.Max(this._localCount, this._maxLocalCount);
			LocalVariables.VariableScope variableScope;
			LocalVariables.VariableScope variableScope2;
			if (this._variables.TryGetValue(variable, out variableScope))
			{
				variableScope2 = new LocalVariables.VariableScope(localVariable, start, variableScope);
				if (variableScope.ChildScopes == null)
				{
					variableScope.ChildScopes = new List<LocalVariables.VariableScope>();
				}
				variableScope.ChildScopes.Add(variableScope2);
			}
			else
			{
				variableScope2 = new LocalVariables.VariableScope(localVariable, start, null);
			}
			this._variables[variable] = variableScope2;
			return new LocalDefinition(localVariable.Index, variable);
		}

		// Token: 0x0600178F RID: 6031 RVA: 0x00046018 File Offset: 0x00044218
		public void UndefineLocal(LocalDefinition definition, int end)
		{
			LocalVariables.VariableScope variableScope = this._variables[definition.Parameter];
			variableScope.Stop = end;
			if (variableScope.Parent != null)
			{
				this._variables[definition.Parameter] = variableScope.Parent;
			}
			else
			{
				this._variables.Remove(definition.Parameter);
			}
			this._localCount--;
		}

		// Token: 0x06001790 RID: 6032 RVA: 0x00046084 File Offset: 0x00044284
		internal void Box(ParameterExpression variable, InstructionList instructions)
		{
			LocalVariables.VariableScope variableScope = this._variables[variable];
			LocalVariable variable2 = variableScope.Variable;
			this._variables[variable].Variable.IsBoxed = true;
			int num = 0;
			int num2 = variableScope.Start;
			while (num2 < variableScope.Stop && num2 < instructions.Count)
			{
				if (variableScope.ChildScopes != null && variableScope.ChildScopes[num].Start == num2)
				{
					num2 = variableScope.ChildScopes[num].Stop;
					num++;
				}
				else
				{
					instructions.SwitchToBoxed(variable2.Index, num2);
				}
				num2++;
			}
		}

		// Token: 0x17000492 RID: 1170
		// (get) Token: 0x06001791 RID: 6033 RVA: 0x0004611E File Offset: 0x0004431E
		public int LocalCount
		{
			get
			{
				return this._maxLocalCount;
			}
		}

		// Token: 0x06001792 RID: 6034 RVA: 0x00046128 File Offset: 0x00044328
		public bool TryGetLocalOrClosure(ParameterExpression var, out LocalVariable local)
		{
			LocalVariables.VariableScope variableScope;
			if (this._variables.TryGetValue(var, out variableScope))
			{
				local = variableScope.Variable;
				return true;
			}
			if (this._closureVariables != null && this._closureVariables.TryGetValue(var, out local))
			{
				return true;
			}
			local = null;
			return false;
		}

		// Token: 0x17000493 RID: 1171
		// (get) Token: 0x06001793 RID: 6035 RVA: 0x0004616C File Offset: 0x0004436C
		internal Dictionary<ParameterExpression, LocalVariable> ClosureVariables
		{
			get
			{
				return this._closureVariables;
			}
		}

		// Token: 0x06001794 RID: 6036 RVA: 0x00046174 File Offset: 0x00044374
		internal LocalVariable AddClosureVariable(ParameterExpression variable)
		{
			if (this._closureVariables == null)
			{
				this._closureVariables = new Dictionary<ParameterExpression, LocalVariable>();
			}
			LocalVariable localVariable = new LocalVariable(this._closureVariables.Count, true);
			this._closureVariables.Add(variable, localVariable);
			return localVariable;
		}

		// Token: 0x06001795 RID: 6037 RVA: 0x000461B4 File Offset: 0x000443B4
		public LocalVariables()
		{
		}

		// Token: 0x04000B65 RID: 2917
		private readonly HybridReferenceDictionary<ParameterExpression, LocalVariables.VariableScope> _variables = new HybridReferenceDictionary<ParameterExpression, LocalVariables.VariableScope>();

		// Token: 0x04000B66 RID: 2918
		private Dictionary<ParameterExpression, LocalVariable> _closureVariables;

		// Token: 0x04000B67 RID: 2919
		private int _localCount;

		// Token: 0x04000B68 RID: 2920
		private int _maxLocalCount;

		// Token: 0x0200037F RID: 895
		private sealed class VariableScope
		{
			// Token: 0x06001796 RID: 6038 RVA: 0x000461C7 File Offset: 0x000443C7
			public VariableScope(LocalVariable variable, int start, LocalVariables.VariableScope parent)
			{
				this.Variable = variable;
				this.Start = start;
				this.Parent = parent;
			}

			// Token: 0x04000B69 RID: 2921
			public readonly int Start;

			// Token: 0x04000B6A RID: 2922
			public int Stop = int.MaxValue;

			// Token: 0x04000B6B RID: 2923
			public readonly LocalVariable Variable;

			// Token: 0x04000B6C RID: 2924
			public readonly LocalVariables.VariableScope Parent;

			// Token: 0x04000B6D RID: 2925
			public List<LocalVariables.VariableScope> ChildScopes;
		}
	}
}
