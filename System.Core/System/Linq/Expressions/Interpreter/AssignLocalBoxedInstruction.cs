﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200036E RID: 878
	internal sealed class AssignLocalBoxedInstruction : LocalAccessInstruction
	{
		// Token: 0x0600174E RID: 5966 RVA: 0x000459CD File Offset: 0x00043BCD
		internal AssignLocalBoxedInstruction(int index) : base(index)
		{
		}

		// Token: 0x17000478 RID: 1144
		// (get) Token: 0x0600174F RID: 5967 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000479 RID: 1145
		// (get) Token: 0x06001750 RID: 5968 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x1700047A RID: 1146
		// (get) Token: 0x06001751 RID: 5969 RVA: 0x00045B49 File Offset: 0x00043D49
		public override string InstructionName
		{
			get
			{
				return "AssignLocalBox";
			}
		}

		// Token: 0x06001752 RID: 5970 RVA: 0x00045B50 File Offset: 0x00043D50
		public override int Run(InterpretedFrame frame)
		{
			((IStrongBox)frame.Data[this._index]).Value = frame.Peek();
			return 1;
		}
	}
}
