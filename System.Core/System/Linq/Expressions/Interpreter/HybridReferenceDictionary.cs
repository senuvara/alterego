﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200040E RID: 1038
	internal class HybridReferenceDictionary<TKey, TValue> where TKey : class
	{
		// Token: 0x0600191F RID: 6431 RVA: 0x0004A528 File Offset: 0x00048728
		public bool TryGetValue(TKey key, out TValue value)
		{
			if (this._dict != null)
			{
				return this._dict.TryGetValue(key, out value);
			}
			if (this._keysAndValues != null)
			{
				for (int i = 0; i < this._keysAndValues.Length; i++)
				{
					if (this._keysAndValues[i].Key == key)
					{
						value = this._keysAndValues[i].Value;
						return true;
					}
				}
			}
			value = default(TValue);
			return false;
		}

		// Token: 0x06001920 RID: 6432 RVA: 0x0004A5A8 File Offset: 0x000487A8
		public void Remove(TKey key)
		{
			if (this._dict != null)
			{
				this._dict.Remove(key);
				return;
			}
			if (this._keysAndValues != null)
			{
				for (int i = 0; i < this._keysAndValues.Length; i++)
				{
					if (this._keysAndValues[i].Key == key)
					{
						this._keysAndValues[i] = default(KeyValuePair<TKey, TValue>);
						return;
					}
				}
			}
		}

		// Token: 0x06001921 RID: 6433 RVA: 0x0004A618 File Offset: 0x00048818
		public bool ContainsKey(TKey key)
		{
			if (this._dict != null)
			{
				return this._dict.ContainsKey(key);
			}
			KeyValuePair<TKey, TValue>[] keysAndValues = this._keysAndValues;
			if (keysAndValues != null)
			{
				for (int i = 0; i < keysAndValues.Length; i++)
				{
					if (keysAndValues[i].Key == key)
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x06001922 RID: 6434 RVA: 0x0004A66E File Offset: 0x0004886E
		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
		{
			if (this._dict != null)
			{
				return this._dict.GetEnumerator();
			}
			return this.GetEnumeratorWorker();
		}

		// Token: 0x06001923 RID: 6435 RVA: 0x0004A68F File Offset: 0x0004888F
		private IEnumerator<KeyValuePair<TKey, TValue>> GetEnumeratorWorker()
		{
			if (this._keysAndValues != null)
			{
				int num;
				for (int i = 0; i < this._keysAndValues.Length; i = num + 1)
				{
					if (this._keysAndValues[i].Key != null)
					{
						yield return this._keysAndValues[i];
					}
					num = i;
				}
			}
			yield break;
		}

		// Token: 0x170004E2 RID: 1250
		public TValue this[TKey key]
		{
			get
			{
				TValue result;
				if (this.TryGetValue(key, out result))
				{
					return result;
				}
				throw new KeyNotFoundException();
			}
			set
			{
				if (this._dict != null)
				{
					this._dict[key] = value;
					return;
				}
				int num;
				if (this._keysAndValues != null)
				{
					num = -1;
					for (int i = 0; i < this._keysAndValues.Length; i++)
					{
						if (this._keysAndValues[i].Key == key)
						{
							this._keysAndValues[i] = new KeyValuePair<TKey, TValue>(key, value);
							return;
						}
						if (this._keysAndValues[i].Key == null)
						{
							num = i;
						}
					}
				}
				else
				{
					this._keysAndValues = new KeyValuePair<TKey, TValue>[10];
					num = 0;
				}
				if (num != -1)
				{
					this._keysAndValues[num] = new KeyValuePair<TKey, TValue>(key, value);
					return;
				}
				this._dict = new Dictionary<TKey, TValue>();
				for (int j = 0; j < this._keysAndValues.Length; j++)
				{
					this._dict[this._keysAndValues[j].Key] = this._keysAndValues[j].Value;
				}
				this._keysAndValues = null;
				this._dict[key] = value;
			}
		}

		// Token: 0x06001926 RID: 6438 RVA: 0x00002310 File Offset: 0x00000510
		public HybridReferenceDictionary()
		{
		}

		// Token: 0x04000BFD RID: 3069
		private KeyValuePair<TKey, TValue>[] _keysAndValues;

		// Token: 0x04000BFE RID: 3070
		private Dictionary<TKey, TValue> _dict;

		// Token: 0x04000BFF RID: 3071
		private const int ArraySize = 10;

		// Token: 0x0200040F RID: 1039
		[CompilerGenerated]
		private sealed class <GetEnumeratorWorker>d__7 : IEnumerator<KeyValuePair<TKey, TValue>>, IDisposable, IEnumerator
		{
			// Token: 0x06001927 RID: 6439 RVA: 0x0004A7D4 File Offset: 0x000489D4
			[DebuggerHidden]
			public <GetEnumeratorWorker>d__7(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x06001928 RID: 6440 RVA: 0x000039E8 File Offset: 0x00001BE8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06001929 RID: 6441 RVA: 0x0004A7E4 File Offset: 0x000489E4
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				HybridReferenceDictionary<TKey, TValue> hybridReferenceDictionary = this;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
				}
				else
				{
					this.<>1__state = -1;
					if (hybridReferenceDictionary._keysAndValues != null)
					{
						i = 0;
						goto IL_83;
					}
					return false;
				}
				IL_73:
				int num2 = i;
				i = num2 + 1;
				IL_83:
				if (i < hybridReferenceDictionary._keysAndValues.Length)
				{
					if (hybridReferenceDictionary._keysAndValues[i].Key != null)
					{
						this.<>2__current = hybridReferenceDictionary._keysAndValues[i];
						this.<>1__state = 1;
						return true;
					}
					goto IL_73;
				}
				return false;
			}

			// Token: 0x170004E3 RID: 1251
			// (get) Token: 0x0600192A RID: 6442 RVA: 0x0004A885 File Offset: 0x00048A85
			KeyValuePair<TKey, TValue> IEnumerator<KeyValuePair<!0, !1>>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600192B RID: 6443 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170004E4 RID: 1252
			// (get) Token: 0x0600192C RID: 6444 RVA: 0x0004A88D File Offset: 0x00048A8D
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x04000C00 RID: 3072
			private int <>1__state;

			// Token: 0x04000C01 RID: 3073
			private KeyValuePair<TKey, TValue> <>2__current;

			// Token: 0x04000C02 RID: 3074
			public HybridReferenceDictionary<TKey, TValue> <>4__this;

			// Token: 0x04000C03 RID: 3075
			private int <i>5__1;
		}
	}
}
