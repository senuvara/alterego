﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic.Utils;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000326 RID: 806
	[DebuggerTypeProxy(typeof(InstructionList.DebugView))]
	internal sealed class InstructionList
	{
		// Token: 0x0600153D RID: 5437 RVA: 0x0003BDD5 File Offset: 0x00039FD5
		public void Emit(Instruction instruction)
		{
			this._instructions.Add(instruction);
			this.UpdateStackDepth(instruction);
		}

		// Token: 0x0600153E RID: 5438 RVA: 0x0003BDEC File Offset: 0x00039FEC
		private void UpdateStackDepth(Instruction instruction)
		{
			this._currentStackDepth -= instruction.ConsumedStack;
			this._currentStackDepth += instruction.ProducedStack;
			if (this._currentStackDepth > this._maxStackDepth)
			{
				this._maxStackDepth = this._currentStackDepth;
			}
			this._currentContinuationsDepth -= instruction.ConsumedContinuations;
			this._currentContinuationsDepth += instruction.ProducedContinuations;
			if (this._currentContinuationsDepth > this._maxContinuationDepth)
			{
				this._maxContinuationDepth = this._currentContinuationsDepth;
			}
		}

		// Token: 0x0600153F RID: 5439 RVA: 0x0003BE7C File Offset: 0x0003A07C
		public void UnEmit()
		{
			Instruction instruction = this._instructions[this._instructions.Count - 1];
			this._instructions.RemoveAt(this._instructions.Count - 1);
			this._currentContinuationsDepth -= instruction.ProducedContinuations;
			this._currentContinuationsDepth += instruction.ConsumedContinuations;
			this._currentStackDepth -= instruction.ProducedStack;
			this._currentStackDepth += instruction.ConsumedStack;
		}

		// Token: 0x06001540 RID: 5440 RVA: 0x000039E8 File Offset: 0x00001BE8
		[Conditional("DEBUG")]
		public void SetDebugCookie(object cookie)
		{
		}

		// Token: 0x1700044B RID: 1099
		// (get) Token: 0x06001541 RID: 5441 RVA: 0x0003BF06 File Offset: 0x0003A106
		public int Count
		{
			get
			{
				return this._instructions.Count;
			}
		}

		// Token: 0x1700044C RID: 1100
		// (get) Token: 0x06001542 RID: 5442 RVA: 0x0003BF13 File Offset: 0x0003A113
		public int CurrentStackDepth
		{
			get
			{
				return this._currentStackDepth;
			}
		}

		// Token: 0x1700044D RID: 1101
		// (get) Token: 0x06001543 RID: 5443 RVA: 0x0003BF1B File Offset: 0x0003A11B
		public int CurrentContinuationsDepth
		{
			get
			{
				return this._currentContinuationsDepth;
			}
		}

		// Token: 0x1700044E RID: 1102
		// (get) Token: 0x06001544 RID: 5444 RVA: 0x0003BF23 File Offset: 0x0003A123
		public int MaxStackDepth
		{
			get
			{
				return this._maxStackDepth;
			}
		}

		// Token: 0x06001545 RID: 5445 RVA: 0x0003BF2B File Offset: 0x0003A12B
		internal Instruction GetInstruction(int index)
		{
			return this._instructions[index];
		}

		// Token: 0x06001546 RID: 5446 RVA: 0x0003BF39 File Offset: 0x0003A139
		public InstructionArray ToArray()
		{
			int maxStackDepth = this._maxStackDepth;
			int maxContinuationDepth = this._maxContinuationDepth;
			Instruction[] instructions = this._instructions.ToArray();
			List<object> objects = this._objects;
			return new InstructionArray(maxStackDepth, maxContinuationDepth, instructions, (objects != null) ? objects.ToArray() : null, this.BuildRuntimeLabels(), this._debugCookies);
		}

		// Token: 0x06001547 RID: 5447 RVA: 0x0003BF75 File Offset: 0x0003A175
		public void EmitLoad(object value)
		{
			this.EmitLoad(value, null);
		}

		// Token: 0x06001548 RID: 5448 RVA: 0x0003BF80 File Offset: 0x0003A180
		public void EmitLoad(bool value)
		{
			if (value)
			{
				Instruction instruction;
				if ((instruction = InstructionList.s_true) == null)
				{
					instruction = (InstructionList.s_true = new LoadObjectInstruction(Utils.BoxedTrue));
				}
				this.Emit(instruction);
				return;
			}
			Instruction instruction2;
			if ((instruction2 = InstructionList.s_false) == null)
			{
				instruction2 = (InstructionList.s_false = new LoadObjectInstruction(Utils.BoxedFalse));
			}
			this.Emit(instruction2);
		}

		// Token: 0x06001549 RID: 5449 RVA: 0x0003BFD0 File Offset: 0x0003A1D0
		public void EmitLoad(object value, Type type)
		{
			if (value == null)
			{
				Instruction instruction;
				if ((instruction = InstructionList.s_null) == null)
				{
					instruction = (InstructionList.s_null = new LoadObjectInstruction(null));
				}
				this.Emit(instruction);
				return;
			}
			if (type == null || type.IsValueType)
			{
				if (value is bool)
				{
					this.EmitLoad((bool)value);
					return;
				}
				if (value is int)
				{
					int num = (int)value;
					if (num >= -100 && num <= 100)
					{
						if (InstructionList.s_Ints == null)
						{
							InstructionList.s_Ints = new Instruction[201];
						}
						num -= -100;
						Instruction instruction2;
						if ((instruction2 = InstructionList.s_Ints[num]) == null)
						{
							instruction2 = (InstructionList.s_Ints[num] = new LoadObjectInstruction(value));
						}
						this.Emit(instruction2);
						return;
					}
				}
			}
			if (this._objects == null)
			{
				this._objects = new List<object>();
				if (InstructionList.s_loadObjectCached == null)
				{
					InstructionList.s_loadObjectCached = new Instruction[256];
				}
			}
			if (this._objects.Count < InstructionList.s_loadObjectCached.Length)
			{
				uint count = (uint)this._objects.Count;
				this._objects.Add(value);
				Instruction instruction3;
				if ((instruction3 = InstructionList.s_loadObjectCached[(int)count]) == null)
				{
					instruction3 = (InstructionList.s_loadObjectCached[(int)count] = new LoadCachedObjectInstruction(count));
				}
				this.Emit(instruction3);
				return;
			}
			this.Emit(new LoadObjectInstruction(value));
		}

		// Token: 0x0600154A RID: 5450 RVA: 0x0003C0FB File Offset: 0x0003A2FB
		public void EmitDup()
		{
			this.Emit(DupInstruction.Instance);
		}

		// Token: 0x0600154B RID: 5451 RVA: 0x0003C108 File Offset: 0x0003A308
		public void EmitPop()
		{
			this.Emit(PopInstruction.Instance);
		}

		// Token: 0x0600154C RID: 5452 RVA: 0x0003C118 File Offset: 0x0003A318
		internal void SwitchToBoxed(int index, int instructionIndex)
		{
			IBoxableInstruction boxableInstruction = this._instructions[instructionIndex] as IBoxableInstruction;
			if (boxableInstruction != null)
			{
				Instruction instruction = boxableInstruction.BoxIfIndexMatches(index);
				if (instruction != null)
				{
					this._instructions[instructionIndex] = instruction;
				}
			}
		}

		// Token: 0x0600154D RID: 5453 RVA: 0x0003C154 File Offset: 0x0003A354
		public void EmitLoadLocal(int index)
		{
			if (InstructionList.s_loadLocal == null)
			{
				InstructionList.s_loadLocal = new Instruction[64];
			}
			if (index < InstructionList.s_loadLocal.Length)
			{
				Instruction instruction;
				if ((instruction = InstructionList.s_loadLocal[index]) == null)
				{
					instruction = (InstructionList.s_loadLocal[index] = new LoadLocalInstruction(index));
				}
				this.Emit(instruction);
				return;
			}
			this.Emit(new LoadLocalInstruction(index));
		}

		// Token: 0x0600154E RID: 5454 RVA: 0x0003C1AC File Offset: 0x0003A3AC
		public void EmitLoadLocalBoxed(int index)
		{
			this.Emit(InstructionList.LoadLocalBoxed(index));
		}

		// Token: 0x0600154F RID: 5455 RVA: 0x0003C1BC File Offset: 0x0003A3BC
		internal static Instruction LoadLocalBoxed(int index)
		{
			if (InstructionList.s_loadLocalBoxed == null)
			{
				InstructionList.s_loadLocalBoxed = new Instruction[64];
			}
			if (index < InstructionList.s_loadLocalBoxed.Length)
			{
				Instruction result;
				if ((result = InstructionList.s_loadLocalBoxed[index]) == null)
				{
					result = (InstructionList.s_loadLocalBoxed[index] = new LoadLocalBoxedInstruction(index));
				}
				return result;
			}
			return new LoadLocalBoxedInstruction(index);
		}

		// Token: 0x06001550 RID: 5456 RVA: 0x0003C208 File Offset: 0x0003A408
		public void EmitLoadLocalFromClosure(int index)
		{
			if (InstructionList.s_loadLocalFromClosure == null)
			{
				InstructionList.s_loadLocalFromClosure = new Instruction[64];
			}
			if (index < InstructionList.s_loadLocalFromClosure.Length)
			{
				Instruction instruction;
				if ((instruction = InstructionList.s_loadLocalFromClosure[index]) == null)
				{
					instruction = (InstructionList.s_loadLocalFromClosure[index] = new LoadLocalFromClosureInstruction(index));
				}
				this.Emit(instruction);
				return;
			}
			this.Emit(new LoadLocalFromClosureInstruction(index));
		}

		// Token: 0x06001551 RID: 5457 RVA: 0x0003C260 File Offset: 0x0003A460
		public void EmitLoadLocalFromClosureBoxed(int index)
		{
			if (InstructionList.s_loadLocalFromClosureBoxed == null)
			{
				InstructionList.s_loadLocalFromClosureBoxed = new Instruction[64];
			}
			if (index < InstructionList.s_loadLocalFromClosureBoxed.Length)
			{
				Instruction instruction;
				if ((instruction = InstructionList.s_loadLocalFromClosureBoxed[index]) == null)
				{
					instruction = (InstructionList.s_loadLocalFromClosureBoxed[index] = new LoadLocalFromClosureBoxedInstruction(index));
				}
				this.Emit(instruction);
				return;
			}
			this.Emit(new LoadLocalFromClosureBoxedInstruction(index));
		}

		// Token: 0x06001552 RID: 5458 RVA: 0x0003C2B8 File Offset: 0x0003A4B8
		public void EmitAssignLocal(int index)
		{
			if (InstructionList.s_assignLocal == null)
			{
				InstructionList.s_assignLocal = new Instruction[64];
			}
			if (index < InstructionList.s_assignLocal.Length)
			{
				Instruction instruction;
				if ((instruction = InstructionList.s_assignLocal[index]) == null)
				{
					instruction = (InstructionList.s_assignLocal[index] = new AssignLocalInstruction(index));
				}
				this.Emit(instruction);
				return;
			}
			this.Emit(new AssignLocalInstruction(index));
		}

		// Token: 0x06001553 RID: 5459 RVA: 0x0003C310 File Offset: 0x0003A510
		public void EmitStoreLocal(int index)
		{
			if (InstructionList.s_storeLocal == null)
			{
				InstructionList.s_storeLocal = new Instruction[64];
			}
			if (index < InstructionList.s_storeLocal.Length)
			{
				Instruction instruction;
				if ((instruction = InstructionList.s_storeLocal[index]) == null)
				{
					instruction = (InstructionList.s_storeLocal[index] = new StoreLocalInstruction(index));
				}
				this.Emit(instruction);
				return;
			}
			this.Emit(new StoreLocalInstruction(index));
		}

		// Token: 0x06001554 RID: 5460 RVA: 0x0003C368 File Offset: 0x0003A568
		public void EmitAssignLocalBoxed(int index)
		{
			this.Emit(InstructionList.AssignLocalBoxed(index));
		}

		// Token: 0x06001555 RID: 5461 RVA: 0x0003C378 File Offset: 0x0003A578
		internal static Instruction AssignLocalBoxed(int index)
		{
			if (InstructionList.s_assignLocalBoxed == null)
			{
				InstructionList.s_assignLocalBoxed = new Instruction[64];
			}
			if (index < InstructionList.s_assignLocalBoxed.Length)
			{
				Instruction result;
				if ((result = InstructionList.s_assignLocalBoxed[index]) == null)
				{
					result = (InstructionList.s_assignLocalBoxed[index] = new AssignLocalBoxedInstruction(index));
				}
				return result;
			}
			return new AssignLocalBoxedInstruction(index);
		}

		// Token: 0x06001556 RID: 5462 RVA: 0x0003C3C4 File Offset: 0x0003A5C4
		public void EmitStoreLocalBoxed(int index)
		{
			this.Emit(InstructionList.StoreLocalBoxed(index));
		}

		// Token: 0x06001557 RID: 5463 RVA: 0x0003C3D4 File Offset: 0x0003A5D4
		internal static Instruction StoreLocalBoxed(int index)
		{
			if (InstructionList.s_storeLocalBoxed == null)
			{
				InstructionList.s_storeLocalBoxed = new Instruction[64];
			}
			if (index < InstructionList.s_storeLocalBoxed.Length)
			{
				Instruction result;
				if ((result = InstructionList.s_storeLocalBoxed[index]) == null)
				{
					result = (InstructionList.s_storeLocalBoxed[index] = new StoreLocalBoxedInstruction(index));
				}
				return result;
			}
			return new StoreLocalBoxedInstruction(index);
		}

		// Token: 0x06001558 RID: 5464 RVA: 0x0003C420 File Offset: 0x0003A620
		public void EmitAssignLocalToClosure(int index)
		{
			if (InstructionList.s_assignLocalToClosure == null)
			{
				InstructionList.s_assignLocalToClosure = new Instruction[64];
			}
			if (index < InstructionList.s_assignLocalToClosure.Length)
			{
				Instruction instruction;
				if ((instruction = InstructionList.s_assignLocalToClosure[index]) == null)
				{
					instruction = (InstructionList.s_assignLocalToClosure[index] = new AssignLocalToClosureInstruction(index));
				}
				this.Emit(instruction);
				return;
			}
			this.Emit(new AssignLocalToClosureInstruction(index));
		}

		// Token: 0x06001559 RID: 5465 RVA: 0x0003C478 File Offset: 0x0003A678
		public void EmitStoreLocalToClosure(int index)
		{
			this.EmitAssignLocalToClosure(index);
			this.EmitPop();
		}

		// Token: 0x0600155A RID: 5466 RVA: 0x0003C488 File Offset: 0x0003A688
		public void EmitInitializeLocal(int index, Type type)
		{
			object primitiveDefaultValue = ScriptingRuntimeHelpers.GetPrimitiveDefaultValue(type);
			if (primitiveDefaultValue != null)
			{
				this.Emit(new InitializeLocalInstruction.ImmutableValue(index, primitiveDefaultValue));
				return;
			}
			if (type.IsValueType)
			{
				this.Emit(new InitializeLocalInstruction.MutableValue(index, type));
				return;
			}
			this.Emit(InstructionList.InitReference(index));
		}

		// Token: 0x0600155B RID: 5467 RVA: 0x0003C4CF File Offset: 0x0003A6CF
		internal void EmitInitializeParameter(int index)
		{
			this.Emit(InstructionList.Parameter(index));
		}

		// Token: 0x0600155C RID: 5468 RVA: 0x0003C4DD File Offset: 0x0003A6DD
		internal static Instruction Parameter(int index)
		{
			return new InitializeLocalInstruction.Parameter(index);
		}

		// Token: 0x0600155D RID: 5469 RVA: 0x0003C4E5 File Offset: 0x0003A6E5
		internal static Instruction ParameterBox(int index)
		{
			return new InitializeLocalInstruction.ParameterBox(index);
		}

		// Token: 0x0600155E RID: 5470 RVA: 0x0003C4ED File Offset: 0x0003A6ED
		internal static Instruction InitReference(int index)
		{
			return new InitializeLocalInstruction.Reference(index);
		}

		// Token: 0x0600155F RID: 5471 RVA: 0x0003C4F5 File Offset: 0x0003A6F5
		internal static Instruction InitImmutableRefBox(int index)
		{
			return new InitializeLocalInstruction.ImmutableRefBox(index);
		}

		// Token: 0x06001560 RID: 5472 RVA: 0x0003C4FD File Offset: 0x0003A6FD
		public void EmitNewRuntimeVariables(int count)
		{
			this.Emit(new RuntimeVariablesInstruction(count));
		}

		// Token: 0x06001561 RID: 5473 RVA: 0x0003C50B File Offset: 0x0003A70B
		public void EmitGetArrayItem()
		{
			this.Emit(GetArrayItemInstruction.Instance);
		}

		// Token: 0x06001562 RID: 5474 RVA: 0x0003C518 File Offset: 0x0003A718
		public void EmitSetArrayItem()
		{
			this.Emit(SetArrayItemInstruction.Instance);
		}

		// Token: 0x06001563 RID: 5475 RVA: 0x0003C525 File Offset: 0x0003A725
		public void EmitNewArray(Type elementType)
		{
			this.Emit(new NewArrayInstruction(elementType));
		}

		// Token: 0x06001564 RID: 5476 RVA: 0x0003C533 File Offset: 0x0003A733
		public void EmitNewArrayBounds(Type elementType, int rank)
		{
			this.Emit(new NewArrayBoundsInstruction(elementType, rank));
		}

		// Token: 0x06001565 RID: 5477 RVA: 0x0003C542 File Offset: 0x0003A742
		public void EmitNewArrayInit(Type elementType, int elementCount)
		{
			this.Emit(new NewArrayInitInstruction(elementType, elementCount));
		}

		// Token: 0x06001566 RID: 5478 RVA: 0x0003C551 File Offset: 0x0003A751
		public void EmitAdd(Type type, bool @checked)
		{
			this.Emit(@checked ? AddOvfInstruction.Create(type) : AddInstruction.Create(type));
		}

		// Token: 0x06001567 RID: 5479 RVA: 0x0003C56A File Offset: 0x0003A76A
		public void EmitSub(Type type, bool @checked)
		{
			this.Emit(@checked ? SubOvfInstruction.Create(type) : SubInstruction.Create(type));
		}

		// Token: 0x06001568 RID: 5480 RVA: 0x0003C583 File Offset: 0x0003A783
		public void EmitMul(Type type, bool @checked)
		{
			this.Emit(@checked ? MulOvfInstruction.Create(type) : MulInstruction.Create(type));
		}

		// Token: 0x06001569 RID: 5481 RVA: 0x0003C59C File Offset: 0x0003A79C
		public void EmitDiv(Type type)
		{
			this.Emit(DivInstruction.Create(type));
		}

		// Token: 0x0600156A RID: 5482 RVA: 0x0003C5AA File Offset: 0x0003A7AA
		public void EmitModulo(Type type)
		{
			this.Emit(ModuloInstruction.Create(type));
		}

		// Token: 0x0600156B RID: 5483 RVA: 0x0003C5B8 File Offset: 0x0003A7B8
		public void EmitExclusiveOr(Type type)
		{
			this.Emit(ExclusiveOrInstruction.Create(type));
		}

		// Token: 0x0600156C RID: 5484 RVA: 0x0003C5C6 File Offset: 0x0003A7C6
		public void EmitAnd(Type type)
		{
			this.Emit(AndInstruction.Create(type));
		}

		// Token: 0x0600156D RID: 5485 RVA: 0x0003C5D4 File Offset: 0x0003A7D4
		public void EmitOr(Type type)
		{
			this.Emit(OrInstruction.Create(type));
		}

		// Token: 0x0600156E RID: 5486 RVA: 0x0003C5E2 File Offset: 0x0003A7E2
		public void EmitLeftShift(Type type)
		{
			this.Emit(LeftShiftInstruction.Create(type));
		}

		// Token: 0x0600156F RID: 5487 RVA: 0x0003C5F0 File Offset: 0x0003A7F0
		public void EmitRightShift(Type type)
		{
			this.Emit(RightShiftInstruction.Create(type));
		}

		// Token: 0x06001570 RID: 5488 RVA: 0x0003C5FE File Offset: 0x0003A7FE
		public void EmitEqual(Type type, bool liftedToNull = false)
		{
			this.Emit(EqualInstruction.Create(type, liftedToNull));
		}

		// Token: 0x06001571 RID: 5489 RVA: 0x0003C60D File Offset: 0x0003A80D
		public void EmitNotEqual(Type type, bool liftedToNull = false)
		{
			this.Emit(NotEqualInstruction.Create(type, liftedToNull));
		}

		// Token: 0x06001572 RID: 5490 RVA: 0x0003C61C File Offset: 0x0003A81C
		public void EmitLessThan(Type type, bool liftedToNull)
		{
			this.Emit(LessThanInstruction.Create(type, liftedToNull));
		}

		// Token: 0x06001573 RID: 5491 RVA: 0x0003C62B File Offset: 0x0003A82B
		public void EmitLessThanOrEqual(Type type, bool liftedToNull)
		{
			this.Emit(LessThanOrEqualInstruction.Create(type, liftedToNull));
		}

		// Token: 0x06001574 RID: 5492 RVA: 0x0003C63A File Offset: 0x0003A83A
		public void EmitGreaterThan(Type type, bool liftedToNull)
		{
			this.Emit(GreaterThanInstruction.Create(type, liftedToNull));
		}

		// Token: 0x06001575 RID: 5493 RVA: 0x0003C649 File Offset: 0x0003A849
		public void EmitGreaterThanOrEqual(Type type, bool liftedToNull)
		{
			this.Emit(GreaterThanOrEqualInstruction.Create(type, liftedToNull));
		}

		// Token: 0x06001576 RID: 5494 RVA: 0x0003C658 File Offset: 0x0003A858
		public void EmitNumericConvertChecked(TypeCode from, TypeCode to, bool isLiftedToNull)
		{
			this.Emit(new NumericConvertInstruction.Checked(from, to, isLiftedToNull));
		}

		// Token: 0x06001577 RID: 5495 RVA: 0x0003C668 File Offset: 0x0003A868
		public void EmitNumericConvertUnchecked(TypeCode from, TypeCode to, bool isLiftedToNull)
		{
			this.Emit(new NumericConvertInstruction.Unchecked(from, to, isLiftedToNull));
		}

		// Token: 0x06001578 RID: 5496 RVA: 0x0003C678 File Offset: 0x0003A878
		public void EmitConvertToUnderlying(TypeCode to, bool isLiftedToNull)
		{
			this.Emit(new NumericConvertInstruction.ToUnderlying(to, isLiftedToNull));
		}

		// Token: 0x06001579 RID: 5497 RVA: 0x0003C687 File Offset: 0x0003A887
		public void EmitCast(Type toType)
		{
			this.Emit(CastInstruction.Create(toType));
		}

		// Token: 0x0600157A RID: 5498 RVA: 0x0003C695 File Offset: 0x0003A895
		public void EmitCastToEnum(Type toType)
		{
			this.Emit(new CastToEnumInstruction(toType));
		}

		// Token: 0x0600157B RID: 5499 RVA: 0x0003C6A3 File Offset: 0x0003A8A3
		public void EmitCastReferenceToEnum(Type toType)
		{
			this.Emit(new CastReferenceToEnumInstruction(toType));
		}

		// Token: 0x0600157C RID: 5500 RVA: 0x0003C6B1 File Offset: 0x0003A8B1
		public void EmitNot(Type type)
		{
			this.Emit(NotInstruction.Create(type));
		}

		// Token: 0x0600157D RID: 5501 RVA: 0x0003C6BF File Offset: 0x0003A8BF
		public void EmitDefaultValue(Type type)
		{
			this.Emit(new DefaultValueInstruction(type));
		}

		// Token: 0x0600157E RID: 5502 RVA: 0x0003C6CD File Offset: 0x0003A8CD
		public void EmitNew(ConstructorInfo constructorInfo, ParameterInfo[] parameters)
		{
			this.Emit(new NewInstruction(constructorInfo, parameters.Length));
		}

		// Token: 0x0600157F RID: 5503 RVA: 0x0003C6DE File Offset: 0x0003A8DE
		public void EmitByRefNew(ConstructorInfo constructorInfo, ParameterInfo[] parameters, ByRefUpdater[] updaters)
		{
			this.Emit(new ByRefNewInstruction(constructorInfo, parameters.Length, updaters));
		}

		// Token: 0x06001580 RID: 5504 RVA: 0x0003C6F0 File Offset: 0x0003A8F0
		internal void EmitCreateDelegate(LightDelegateCreator creator)
		{
			this.Emit(new CreateDelegateInstruction(creator));
		}

		// Token: 0x06001581 RID: 5505 RVA: 0x0003C6FE File Offset: 0x0003A8FE
		public void EmitTypeEquals()
		{
			this.Emit(TypeEqualsInstruction.Instance);
		}

		// Token: 0x06001582 RID: 5506 RVA: 0x0003C70B File Offset: 0x0003A90B
		public void EmitArrayLength()
		{
			this.Emit(ArrayLengthInstruction.Instance);
		}

		// Token: 0x06001583 RID: 5507 RVA: 0x0003C718 File Offset: 0x0003A918
		public void EmitNegate(Type type)
		{
			this.Emit(NegateInstruction.Create(type));
		}

		// Token: 0x06001584 RID: 5508 RVA: 0x0003C726 File Offset: 0x0003A926
		public void EmitNegateChecked(Type type)
		{
			this.Emit(NegateCheckedInstruction.Create(type));
		}

		// Token: 0x06001585 RID: 5509 RVA: 0x0003C734 File Offset: 0x0003A934
		public void EmitIncrement(Type type)
		{
			this.Emit(IncrementInstruction.Create(type));
		}

		// Token: 0x06001586 RID: 5510 RVA: 0x0003C742 File Offset: 0x0003A942
		public void EmitDecrement(Type type)
		{
			this.Emit(DecrementInstruction.Create(type));
		}

		// Token: 0x06001587 RID: 5511 RVA: 0x0003C750 File Offset: 0x0003A950
		public void EmitTypeIs(Type type)
		{
			this.Emit(new TypeIsInstruction(type));
		}

		// Token: 0x06001588 RID: 5512 RVA: 0x0003C75E File Offset: 0x0003A95E
		public void EmitTypeAs(Type type)
		{
			this.Emit(new TypeAsInstruction(type));
		}

		// Token: 0x06001589 RID: 5513 RVA: 0x0003C76C File Offset: 0x0003A96C
		public void EmitLoadField(FieldInfo field)
		{
			this.Emit(this.GetLoadField(field));
		}

		// Token: 0x0600158A RID: 5514 RVA: 0x0003C77C File Offset: 0x0003A97C
		private Instruction GetLoadField(FieldInfo field)
		{
			Dictionary<FieldInfo, Instruction> obj = InstructionList.s_loadFields;
			Instruction result;
			lock (obj)
			{
				Instruction instruction;
				if (!InstructionList.s_loadFields.TryGetValue(field, out instruction))
				{
					if (field.IsStatic)
					{
						instruction = new LoadStaticFieldInstruction(field);
					}
					else
					{
						instruction = new LoadFieldInstruction(field);
					}
					InstructionList.s_loadFields.Add(field, instruction);
				}
				result = instruction;
			}
			return result;
		}

		// Token: 0x0600158B RID: 5515 RVA: 0x0003C7EC File Offset: 0x0003A9EC
		public void EmitStoreField(FieldInfo field)
		{
			if (field.IsStatic)
			{
				this.Emit(new StoreStaticFieldInstruction(field));
				return;
			}
			this.Emit(new StoreFieldInstruction(field));
		}

		// Token: 0x0600158C RID: 5516 RVA: 0x0003C80F File Offset: 0x0003AA0F
		public void EmitCall(MethodInfo method)
		{
			this.EmitCall(method, method.GetParametersCached());
		}

		// Token: 0x0600158D RID: 5517 RVA: 0x0003C81E File Offset: 0x0003AA1E
		public void EmitCall(MethodInfo method, ParameterInfo[] parameters)
		{
			this.Emit(CallInstruction.Create(method, parameters));
		}

		// Token: 0x0600158E RID: 5518 RVA: 0x0003C82D File Offset: 0x0003AA2D
		public void EmitByRefCall(MethodInfo method, ParameterInfo[] parameters, ByRefUpdater[] byrefArgs)
		{
			this.Emit(new ByRefMethodInfoCallInstruction(method, method.IsStatic ? parameters.Length : (parameters.Length + 1), byrefArgs));
		}

		// Token: 0x0600158F RID: 5519 RVA: 0x0003C84E File Offset: 0x0003AA4E
		public void EmitNullableCall(MethodInfo method, ParameterInfo[] parameters)
		{
			this.Emit(NullableMethodCallInstruction.Create(method.Name, parameters.Length, method));
		}

		// Token: 0x06001590 RID: 5520 RVA: 0x0003C868 File Offset: 0x0003AA68
		private RuntimeLabel[] BuildRuntimeLabels()
		{
			if (this._runtimeLabelCount == 0)
			{
				return InstructionList.s_emptyRuntimeLabels;
			}
			RuntimeLabel[] array = new RuntimeLabel[this._runtimeLabelCount + 1];
			foreach (BranchLabel branchLabel in this._labels)
			{
				if (branchLabel.HasRuntimeLabel)
				{
					array[branchLabel.LabelIndex] = branchLabel.ToRuntimeLabel();
				}
			}
			array[array.Length - 1] = new RuntimeLabel(int.MaxValue, 0, 0);
			return array;
		}

		// Token: 0x06001591 RID: 5521 RVA: 0x0003C904 File Offset: 0x0003AB04
		public BranchLabel MakeLabel()
		{
			if (this._labels == null)
			{
				this._labels = new List<BranchLabel>();
			}
			BranchLabel branchLabel = new BranchLabel();
			this._labels.Add(branchLabel);
			return branchLabel;
		}

		// Token: 0x06001592 RID: 5522 RVA: 0x0003C937 File Offset: 0x0003AB37
		internal void FixupBranch(int branchIndex, int offset)
		{
			this._instructions[branchIndex] = ((OffsetInstruction)this._instructions[branchIndex]).Fixup(offset);
		}

		// Token: 0x06001593 RID: 5523 RVA: 0x0003C95C File Offset: 0x0003AB5C
		private int EnsureLabelIndex(BranchLabel label)
		{
			if (label.HasRuntimeLabel)
			{
				return label.LabelIndex;
			}
			label.LabelIndex = this._runtimeLabelCount;
			this._runtimeLabelCount++;
			return label.LabelIndex;
		}

		// Token: 0x06001594 RID: 5524 RVA: 0x0003C990 File Offset: 0x0003AB90
		public int MarkRuntimeLabel()
		{
			BranchLabel label = this.MakeLabel();
			this.MarkLabel(label);
			return this.EnsureLabelIndex(label);
		}

		// Token: 0x06001595 RID: 5525 RVA: 0x0003C9B2 File Offset: 0x0003ABB2
		public void MarkLabel(BranchLabel label)
		{
			label.Mark(this);
		}

		// Token: 0x06001596 RID: 5526 RVA: 0x0003C9BB File Offset: 0x0003ABBB
		public void EmitGoto(BranchLabel label, bool hasResult, bool hasValue, bool labelTargetGetsValue)
		{
			this.Emit(GotoInstruction.Create(this.EnsureLabelIndex(label), hasResult, hasValue, labelTargetGetsValue));
		}

		// Token: 0x06001597 RID: 5527 RVA: 0x0003C9D3 File Offset: 0x0003ABD3
		private void EmitBranch(OffsetInstruction instruction, BranchLabel label)
		{
			this.Emit(instruction);
			label.AddBranch(this, this.Count - 1);
		}

		// Token: 0x06001598 RID: 5528 RVA: 0x0003C9EB File Offset: 0x0003ABEB
		public void EmitBranch(BranchLabel label)
		{
			this.EmitBranch(new BranchInstruction(), label);
		}

		// Token: 0x06001599 RID: 5529 RVA: 0x0003C9F9 File Offset: 0x0003ABF9
		public void EmitBranch(BranchLabel label, bool hasResult, bool hasValue)
		{
			this.EmitBranch(new BranchInstruction(hasResult, hasValue), label);
		}

		// Token: 0x0600159A RID: 5530 RVA: 0x0003CA09 File Offset: 0x0003AC09
		public void EmitCoalescingBranch(BranchLabel leftNotNull)
		{
			this.EmitBranch(new CoalescingBranchInstruction(), leftNotNull);
		}

		// Token: 0x0600159B RID: 5531 RVA: 0x0003CA17 File Offset: 0x0003AC17
		public void EmitBranchTrue(BranchLabel elseLabel)
		{
			this.EmitBranch(new BranchTrueInstruction(), elseLabel);
		}

		// Token: 0x0600159C RID: 5532 RVA: 0x0003CA25 File Offset: 0x0003AC25
		public void EmitBranchFalse(BranchLabel elseLabel)
		{
			this.EmitBranch(new BranchFalseInstruction(), elseLabel);
		}

		// Token: 0x0600159D RID: 5533 RVA: 0x0003CA33 File Offset: 0x0003AC33
		public void EmitThrow()
		{
			this.Emit(ThrowInstruction.Throw);
		}

		// Token: 0x0600159E RID: 5534 RVA: 0x0003CA40 File Offset: 0x0003AC40
		public void EmitThrowVoid()
		{
			this.Emit(ThrowInstruction.VoidThrow);
		}

		// Token: 0x0600159F RID: 5535 RVA: 0x0003CA4D File Offset: 0x0003AC4D
		public void EmitRethrow()
		{
			this.Emit(ThrowInstruction.Rethrow);
		}

		// Token: 0x060015A0 RID: 5536 RVA: 0x0003CA5A File Offset: 0x0003AC5A
		public void EmitRethrowVoid()
		{
			this.Emit(ThrowInstruction.VoidRethrow);
		}

		// Token: 0x060015A1 RID: 5537 RVA: 0x0003CA67 File Offset: 0x0003AC67
		public void EmitEnterTryFinally(BranchLabel finallyStartLabel)
		{
			this.Emit(EnterTryCatchFinallyInstruction.CreateTryFinally(this.EnsureLabelIndex(finallyStartLabel)));
		}

		// Token: 0x060015A2 RID: 5538 RVA: 0x0003CA7B File Offset: 0x0003AC7B
		public void EmitEnterTryCatch()
		{
			this.Emit(EnterTryCatchFinallyInstruction.CreateTryCatch());
		}

		// Token: 0x060015A3 RID: 5539 RVA: 0x0003CA88 File Offset: 0x0003AC88
		public EnterTryFaultInstruction EmitEnterTryFault(BranchLabel tryEnd)
		{
			EnterTryFaultInstruction enterTryFaultInstruction = new EnterTryFaultInstruction(this.EnsureLabelIndex(tryEnd));
			this.Emit(enterTryFaultInstruction);
			return enterTryFaultInstruction;
		}

		// Token: 0x060015A4 RID: 5540 RVA: 0x0003CAAA File Offset: 0x0003ACAA
		public void EmitEnterFinally(BranchLabel finallyStartLabel)
		{
			this.Emit(EnterFinallyInstruction.Create(this.EnsureLabelIndex(finallyStartLabel)));
		}

		// Token: 0x060015A5 RID: 5541 RVA: 0x0003CABE File Offset: 0x0003ACBE
		public void EmitLeaveFinally()
		{
			this.Emit(LeaveFinallyInstruction.Instance);
		}

		// Token: 0x060015A6 RID: 5542 RVA: 0x0003CACB File Offset: 0x0003ACCB
		public void EmitEnterFault(BranchLabel faultStartLabel)
		{
			this.Emit(EnterFaultInstruction.Create(this.EnsureLabelIndex(faultStartLabel)));
		}

		// Token: 0x060015A7 RID: 5543 RVA: 0x0003CADF File Offset: 0x0003ACDF
		public void EmitLeaveFault()
		{
			this.Emit(LeaveFaultInstruction.Instance);
		}

		// Token: 0x060015A8 RID: 5544 RVA: 0x0003CAEC File Offset: 0x0003ACEC
		public void EmitEnterExceptionFilter()
		{
			this.Emit(EnterExceptionFilterInstruction.Instance);
		}

		// Token: 0x060015A9 RID: 5545 RVA: 0x0003CAF9 File Offset: 0x0003ACF9
		public void EmitLeaveExceptionFilter()
		{
			this.Emit(LeaveExceptionFilterInstruction.Instance);
		}

		// Token: 0x060015AA RID: 5546 RVA: 0x0003CB06 File Offset: 0x0003AD06
		public void EmitEnterExceptionHandlerNonVoid()
		{
			this.Emit(EnterExceptionHandlerInstruction.NonVoid);
		}

		// Token: 0x060015AB RID: 5547 RVA: 0x0003CB13 File Offset: 0x0003AD13
		public void EmitEnterExceptionHandlerVoid()
		{
			this.Emit(EnterExceptionHandlerInstruction.Void);
		}

		// Token: 0x060015AC RID: 5548 RVA: 0x0003CB20 File Offset: 0x0003AD20
		public void EmitLeaveExceptionHandler(bool hasValue, BranchLabel tryExpressionEndLabel)
		{
			this.Emit(LeaveExceptionHandlerInstruction.Create(this.EnsureLabelIndex(tryExpressionEndLabel), hasValue));
		}

		// Token: 0x060015AD RID: 5549 RVA: 0x0003CB35 File Offset: 0x0003AD35
		public void EmitIntSwitch<T>(Dictionary<T, int> cases)
		{
			this.Emit(new IntSwitchInstruction<T>(cases));
		}

		// Token: 0x060015AE RID: 5550 RVA: 0x0003CB43 File Offset: 0x0003AD43
		public void EmitStringSwitch(Dictionary<string, int> cases, StrongBox<int> nullCase)
		{
			this.Emit(new StringSwitchInstruction(cases, nullCase));
		}

		// Token: 0x060015AF RID: 5551 RVA: 0x0003CB52 File Offset: 0x0003AD52
		public InstructionList()
		{
		}

		// Token: 0x060015B0 RID: 5552 RVA: 0x0003CB65 File Offset: 0x0003AD65
		// Note: this type is marked as 'beforefieldinit'.
		static InstructionList()
		{
		}

		// Token: 0x04000A90 RID: 2704
		private readonly List<Instruction> _instructions = new List<Instruction>();

		// Token: 0x04000A91 RID: 2705
		private List<object> _objects;

		// Token: 0x04000A92 RID: 2706
		private int _currentStackDepth;

		// Token: 0x04000A93 RID: 2707
		private int _maxStackDepth;

		// Token: 0x04000A94 RID: 2708
		private int _currentContinuationsDepth;

		// Token: 0x04000A95 RID: 2709
		private int _maxContinuationDepth;

		// Token: 0x04000A96 RID: 2710
		private int _runtimeLabelCount;

		// Token: 0x04000A97 RID: 2711
		private List<BranchLabel> _labels;

		// Token: 0x04000A98 RID: 2712
		private List<KeyValuePair<int, object>> _debugCookies;

		// Token: 0x04000A99 RID: 2713
		private const int PushIntMinCachedValue = -100;

		// Token: 0x04000A9A RID: 2714
		private const int PushIntMaxCachedValue = 100;

		// Token: 0x04000A9B RID: 2715
		private const int CachedObjectCount = 256;

		// Token: 0x04000A9C RID: 2716
		private static Instruction s_null;

		// Token: 0x04000A9D RID: 2717
		private static Instruction s_true;

		// Token: 0x04000A9E RID: 2718
		private static Instruction s_false;

		// Token: 0x04000A9F RID: 2719
		private static Instruction[] s_Ints;

		// Token: 0x04000AA0 RID: 2720
		private static Instruction[] s_loadObjectCached;

		// Token: 0x04000AA1 RID: 2721
		private const int LocalInstrCacheSize = 64;

		// Token: 0x04000AA2 RID: 2722
		private static Instruction[] s_loadLocal;

		// Token: 0x04000AA3 RID: 2723
		private static Instruction[] s_loadLocalBoxed;

		// Token: 0x04000AA4 RID: 2724
		private static Instruction[] s_loadLocalFromClosure;

		// Token: 0x04000AA5 RID: 2725
		private static Instruction[] s_loadLocalFromClosureBoxed;

		// Token: 0x04000AA6 RID: 2726
		private static Instruction[] s_assignLocal;

		// Token: 0x04000AA7 RID: 2727
		private static Instruction[] s_storeLocal;

		// Token: 0x04000AA8 RID: 2728
		private static Instruction[] s_assignLocalBoxed;

		// Token: 0x04000AA9 RID: 2729
		private static Instruction[] s_storeLocalBoxed;

		// Token: 0x04000AAA RID: 2730
		private static Instruction[] s_assignLocalToClosure;

		// Token: 0x04000AAB RID: 2731
		private static readonly Dictionary<FieldInfo, Instruction> s_loadFields = new Dictionary<FieldInfo, Instruction>();

		// Token: 0x04000AAC RID: 2732
		private static readonly RuntimeLabel[] s_emptyRuntimeLabels = new RuntimeLabel[]
		{
			new RuntimeLabel(int.MaxValue, 0, 0)
		};

		// Token: 0x02000327 RID: 807
		internal sealed class DebugView
		{
			// Token: 0x060015B1 RID: 5553 RVA: 0x0003CB8F File Offset: 0x0003AD8F
			public DebugView(InstructionList list)
			{
				ContractUtils.RequiresNotNull(list, "list");
				this._list = list;
			}

			// Token: 0x1700044F RID: 1103
			// (get) Token: 0x060015B2 RID: 5554 RVA: 0x0003CBA9 File Offset: 0x0003ADA9
			[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
			public InstructionList.DebugView.InstructionView[] A0
			{
				get
				{
					return this.GetInstructionViews(true);
				}
			}

			// Token: 0x060015B3 RID: 5555 RVA: 0x0003CBB2 File Offset: 0x0003ADB2
			public InstructionList.DebugView.InstructionView[] GetInstructionViews(bool includeDebugCookies = false)
			{
				return InstructionList.DebugView.GetInstructionViews(this._list._instructions, this._list._objects, (int index) => this._list._labels[index].TargetIndex, includeDebugCookies ? this._list._debugCookies : null);
			}

			// Token: 0x060015B4 RID: 5556 RVA: 0x0003CBEC File Offset: 0x0003ADEC
			internal static InstructionList.DebugView.InstructionView[] GetInstructionViews(IReadOnlyList<Instruction> instructions, IReadOnlyList<object> objects, Func<int, int> labelIndexer, IReadOnlyList<KeyValuePair<int, object>> debugCookies)
			{
				List<InstructionList.DebugView.InstructionView> list = new List<InstructionList.DebugView.InstructionView>();
				int num = 0;
				int num2 = 0;
				int num3 = 0;
				IEnumerator<KeyValuePair<int, object>> enumerator = (debugCookies ?? Array.Empty<KeyValuePair<int, object>>()).GetEnumerator();
				bool flag = enumerator.MoveNext();
				int i = 0;
				int count = instructions.Count;
				while (i < count)
				{
					Instruction instruction = instructions[i];
					object cookie = null;
					while (flag)
					{
						KeyValuePair<int, object> keyValuePair = enumerator.Current;
						if (keyValuePair.Key != i)
						{
							break;
						}
						keyValuePair = enumerator.Current;
						cookie = keyValuePair.Value;
						flag = enumerator.MoveNext();
					}
					int stackBalance = instruction.StackBalance;
					int continuationsBalance = instruction.ContinuationsBalance;
					string name = instruction.ToDebugString(i, cookie, labelIndexer, objects);
					list.Add(new InstructionList.DebugView.InstructionView(instruction, name, i, num2, num3));
					num++;
					num2 += stackBalance;
					num3 += continuationsBalance;
					i++;
				}
				return list.ToArray();
			}

			// Token: 0x060015B5 RID: 5557 RVA: 0x0003CCC8 File Offset: 0x0003AEC8
			[CompilerGenerated]
			private int <GetInstructionViews>b__4_0(int index)
			{
				return this._list._labels[index].TargetIndex;
			}

			// Token: 0x04000AAD RID: 2733
			private readonly InstructionList _list;

			// Token: 0x02000328 RID: 808
			[DebuggerDisplay("{GetValue(),nq}", Name = "{GetName(),nq}", Type = "{GetDisplayType(), nq}")]
			internal struct InstructionView
			{
				// Token: 0x060015B6 RID: 5558 RVA: 0x0003CCE0 File Offset: 0x0003AEE0
				internal string GetName()
				{
					return this._index + ((this._continuationsDepth == 0) ? "" : (" C(" + this._continuationsDepth + ")")) + ((this._stackDepth == 0) ? "" : (" S(" + this._stackDepth + ")"));
				}

				// Token: 0x060015B7 RID: 5559 RVA: 0x0003CD4F File Offset: 0x0003AF4F
				internal string GetValue()
				{
					return this._name;
				}

				// Token: 0x060015B8 RID: 5560 RVA: 0x0003CD57 File Offset: 0x0003AF57
				internal string GetDisplayType()
				{
					return this._instruction.ContinuationsBalance + "/" + this._instruction.StackBalance;
				}

				// Token: 0x060015B9 RID: 5561 RVA: 0x0003CD83 File Offset: 0x0003AF83
				public InstructionView(Instruction instruction, string name, int index, int stackDepth, int continuationsDepth)
				{
					this._instruction = instruction;
					this._name = name;
					this._index = index;
					this._stackDepth = stackDepth;
					this._continuationsDepth = continuationsDepth;
				}

				// Token: 0x04000AAE RID: 2734
				private readonly int _index;

				// Token: 0x04000AAF RID: 2735
				private readonly int _stackDepth;

				// Token: 0x04000AB0 RID: 2736
				private readonly int _continuationsDepth;

				// Token: 0x04000AB1 RID: 2737
				private readonly string _name;

				// Token: 0x04000AB2 RID: 2738
				private readonly Instruction _instruction;
			}
		}
	}
}
