﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002B8 RID: 696
	internal sealed class GotoInstruction : IndexedBranchInstruction
	{
		// Token: 0x170003FF RID: 1023
		// (get) Token: 0x060013FB RID: 5115 RVA: 0x000391F0 File Offset: 0x000373F0
		public override string InstructionName
		{
			get
			{
				return "Goto";
			}
		}

		// Token: 0x17000400 RID: 1024
		// (get) Token: 0x060013FC RID: 5116 RVA: 0x000391F7 File Offset: 0x000373F7
		public override int ConsumedStack
		{
			get
			{
				if (!this._hasValue)
				{
					return 0;
				}
				return 1;
			}
		}

		// Token: 0x17000401 RID: 1025
		// (get) Token: 0x060013FD RID: 5117 RVA: 0x00039204 File Offset: 0x00037404
		public override int ProducedStack
		{
			get
			{
				if (!this._hasResult)
				{
					return 0;
				}
				return 1;
			}
		}

		// Token: 0x060013FE RID: 5118 RVA: 0x00039211 File Offset: 0x00037411
		private GotoInstruction(int targetIndex, bool hasResult, bool hasValue, bool labelTargetGetsValue) : base(targetIndex)
		{
			this._hasResult = hasResult;
			this._hasValue = hasValue;
			this._labelTargetGetsValue = labelTargetGetsValue;
		}

		// Token: 0x060013FF RID: 5119 RVA: 0x00039230 File Offset: 0x00037430
		internal static GotoInstruction Create(int labelIndex, bool hasResult, bool hasValue, bool labelTargetGetsValue)
		{
			if (labelIndex < 32)
			{
				int num = 8 * labelIndex | (labelTargetGetsValue ? 4 : 0) | (hasResult ? 2 : 0) | (hasValue ? 1 : 0);
				GotoInstruction result;
				if ((result = GotoInstruction.s_cache[num]) == null)
				{
					result = (GotoInstruction.s_cache[num] = new GotoInstruction(labelIndex, hasResult, hasValue, labelTargetGetsValue));
				}
				return result;
			}
			return new GotoInstruction(labelIndex, hasResult, hasValue, labelTargetGetsValue);
		}

		// Token: 0x06001400 RID: 5120 RVA: 0x00039288 File Offset: 0x00037488
		public override int Run(InterpretedFrame frame)
		{
			object obj = this._hasValue ? frame.Pop() : Interpreter.NoValue;
			return frame.Goto(this._labelIndex, this._labelTargetGetsValue ? obj : Interpreter.NoValue, false);
		}

		// Token: 0x06001401 RID: 5121 RVA: 0x000392C8 File Offset: 0x000374C8
		// Note: this type is marked as 'beforefieldinit'.
		static GotoInstruction()
		{
		}

		// Token: 0x04000A01 RID: 2561
		private const int Variants = 8;

		// Token: 0x04000A02 RID: 2562
		private static readonly GotoInstruction[] s_cache = new GotoInstruction[256];

		// Token: 0x04000A03 RID: 2563
		private readonly bool _hasResult;

		// Token: 0x04000A04 RID: 2564
		private readonly bool _hasValue;

		// Token: 0x04000A05 RID: 2565
		private readonly bool _labelTargetGetsValue;
	}
}
