﻿using System;
using System.Dynamic.Utils;
using System.Reflection;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000361 RID: 865
	internal sealed class IndexMethodByRefUpdater : ByRefUpdater
	{
		// Token: 0x060016D3 RID: 5843 RVA: 0x000433F7 File Offset: 0x000415F7
		public IndexMethodByRefUpdater(LocalDefinition? obj, LocalDefinition[] args, MethodInfo indexer, int argumentIndex) : base(argumentIndex)
		{
			this._obj = obj;
			this._args = args;
			this._indexer = indexer;
		}

		// Token: 0x060016D4 RID: 5844 RVA: 0x00043418 File Offset: 0x00041618
		public override void Update(InterpretedFrame frame, object value)
		{
			object[] array = new object[this._args.Length + 1];
			for (int i = 0; i < array.Length - 1; i++)
			{
				array[i] = frame.Data[this._args[i].Index];
			}
			array[array.Length - 1] = value;
			object obj = (this._obj == null) ? null : frame.Data[this._obj.GetValueOrDefault().Index];
			try
			{
				this._indexer.Invoke(obj, array);
			}
			catch (TargetInvocationException exception)
			{
				ExceptionHelpers.UnwrapAndRethrow(exception);
				throw ContractUtils.Unreachable;
			}
		}

		// Token: 0x060016D5 RID: 5845 RVA: 0x000434C8 File Offset: 0x000416C8
		public override void UndefineTemps(InstructionList instructions, LocalVariables locals)
		{
			if (this._obj != null)
			{
				locals.UndefineLocal(this._obj.GetValueOrDefault(), instructions.Count);
			}
			for (int i = 0; i < this._args.Length; i++)
			{
				locals.UndefineLocal(this._args[i], instructions.Count);
			}
		}

		// Token: 0x04000B47 RID: 2887
		private readonly MethodInfo _indexer;

		// Token: 0x04000B48 RID: 2888
		private readonly LocalDefinition? _obj;

		// Token: 0x04000B49 RID: 2889
		private readonly LocalDefinition[] _args;
	}
}
