﻿using System;
using System.Dynamic.Utils;
using System.Reflection;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002AF RID: 687
	internal abstract class CallInstruction : Instruction
	{
		// Token: 0x170003EA RID: 1002
		// (get) Token: 0x060013C6 RID: 5062
		public abstract int ArgumentCount { get; }

		// Token: 0x170003EB RID: 1003
		// (get) Token: 0x060013C7 RID: 5063 RVA: 0x00038A1C File Offset: 0x00036C1C
		public override string InstructionName
		{
			get
			{
				return "Call";
			}
		}

		// Token: 0x060013C8 RID: 5064 RVA: 0x00038A23 File Offset: 0x00036C23
		public static CallInstruction Create(MethodInfo info)
		{
			return CallInstruction.Create(info, info.GetParametersCached());
		}

		// Token: 0x060013C9 RID: 5065 RVA: 0x00038A34 File Offset: 0x00036C34
		public static CallInstruction Create(MethodInfo info, ParameterInfo[] parameters)
		{
			int num = parameters.Length;
			if (!info.IsStatic)
			{
				num++;
			}
			if (info.DeclaringType != null && info.DeclaringType.IsArray && (info.Name == "Get" || info.Name == "Set"))
			{
				return CallInstruction.GetArrayAccessor(info, num);
			}
			return new MethodInfoCallInstruction(info, num);
		}

		// Token: 0x060013CA RID: 5066 RVA: 0x00038AA0 File Offset: 0x00036CA0
		private static CallInstruction GetArrayAccessor(MethodInfo info, int argumentCount)
		{
			Type declaringType = info.DeclaringType;
			bool flag = info.Name == "Get";
			MethodInfo methodInfo = null;
			switch (declaringType.GetArrayRank())
			{
			case 1:
				methodInfo = (flag ? declaringType.GetMethod("GetValue", new Type[]
				{
					typeof(int)
				}) : typeof(CallInstruction).GetMethod("ArrayItemSetter1"));
				break;
			case 2:
				methodInfo = (flag ? declaringType.GetMethod("GetValue", new Type[]
				{
					typeof(int),
					typeof(int)
				}) : typeof(CallInstruction).GetMethod("ArrayItemSetter2"));
				break;
			case 3:
				methodInfo = (flag ? declaringType.GetMethod("GetValue", new Type[]
				{
					typeof(int),
					typeof(int),
					typeof(int)
				}) : typeof(CallInstruction).GetMethod("ArrayItemSetter3"));
				break;
			}
			if (methodInfo == null)
			{
				return new MethodInfoCallInstruction(info, argumentCount);
			}
			return CallInstruction.Create(methodInfo);
		}

		// Token: 0x060013CB RID: 5067 RVA: 0x00038BCE File Offset: 0x00036DCE
		public static void ArrayItemSetter1(Array array, int index0, object value)
		{
			array.SetValue(value, index0);
		}

		// Token: 0x060013CC RID: 5068 RVA: 0x00038BD8 File Offset: 0x00036DD8
		public static void ArrayItemSetter2(Array array, int index0, int index1, object value)
		{
			array.SetValue(value, index0, index1);
		}

		// Token: 0x060013CD RID: 5069 RVA: 0x00038BE3 File Offset: 0x00036DE3
		public static void ArrayItemSetter3(Array array, int index0, int index1, int index2, object value)
		{
			array.SetValue(value, index0, index1, index2);
		}

		// Token: 0x170003EC RID: 1004
		// (get) Token: 0x060013CE RID: 5070 RVA: 0x00038BF0 File Offset: 0x00036DF0
		public override int ConsumedStack
		{
			get
			{
				return this.ArgumentCount;
			}
		}

		// Token: 0x060013CF RID: 5071 RVA: 0x00038BF8 File Offset: 0x00036DF8
		protected static bool TryGetLightLambdaTarget(object instance, out LightLambda lightLambda)
		{
			Delegate @delegate = instance as Delegate;
			if (@delegate != null)
			{
				Func<object[], object> func = @delegate.Target as Func<object[], object>;
				if (func != null)
				{
					lightLambda = (func.Target as LightLambda);
					if (lightLambda != null)
					{
						return true;
					}
				}
			}
			lightLambda = null;
			return false;
		}

		// Token: 0x060013D0 RID: 5072 RVA: 0x00038C35 File Offset: 0x00036E35
		protected object InterpretLambdaInvoke(LightLambda targetLambda, object[] args)
		{
			if (this.ProducedStack > 0)
			{
				return targetLambda.Run(args);
			}
			return targetLambda.RunVoid(args);
		}

		// Token: 0x060013D1 RID: 5073 RVA: 0x00037C51 File Offset: 0x00035E51
		protected CallInstruction()
		{
		}
	}
}
