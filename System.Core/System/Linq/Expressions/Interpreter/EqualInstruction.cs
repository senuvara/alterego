﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002D9 RID: 729
	internal abstract class EqualInstruction : Instruction
	{
		// Token: 0x1700042B RID: 1067
		// (get) Token: 0x0600147F RID: 5247 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x1700042C RID: 1068
		// (get) Token: 0x06001480 RID: 5248 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x1700042D RID: 1069
		// (get) Token: 0x06001481 RID: 5249 RVA: 0x0003A0B9 File Offset: 0x000382B9
		public override string InstructionName
		{
			get
			{
				return "Equal";
			}
		}

		// Token: 0x06001482 RID: 5250 RVA: 0x00037C51 File Offset: 0x00035E51
		private EqualInstruction()
		{
		}

		// Token: 0x06001483 RID: 5251 RVA: 0x0003A0C0 File Offset: 0x000382C0
		public static Instruction Create(Type type, bool liftedToNull)
		{
			if (liftedToNull)
			{
				switch (type.GetNonNullableType().GetTypeCode())
				{
				case TypeCode.Boolean:
				{
					Instruction result;
					if ((result = EqualInstruction.s_BooleanLiftedToNull) == null)
					{
						result = (EqualInstruction.s_BooleanLiftedToNull = new EqualInstruction.EqualBooleanLiftedToNull());
					}
					return result;
				}
				case TypeCode.Char:
				{
					Instruction result2;
					if ((result2 = EqualInstruction.s_CharLiftedToNull) == null)
					{
						result2 = (EqualInstruction.s_CharLiftedToNull = new EqualInstruction.EqualCharLiftedToNull());
					}
					return result2;
				}
				case TypeCode.SByte:
				{
					Instruction result3;
					if ((result3 = EqualInstruction.s_SByteLiftedToNull) == null)
					{
						result3 = (EqualInstruction.s_SByteLiftedToNull = new EqualInstruction.EqualSByteLiftedToNull());
					}
					return result3;
				}
				case TypeCode.Byte:
				{
					Instruction result4;
					if ((result4 = EqualInstruction.s_ByteLiftedToNull) == null)
					{
						result4 = (EqualInstruction.s_ByteLiftedToNull = new EqualInstruction.EqualByteLiftedToNull());
					}
					return result4;
				}
				case TypeCode.Int16:
				{
					Instruction result5;
					if ((result5 = EqualInstruction.s_Int16LiftedToNull) == null)
					{
						result5 = (EqualInstruction.s_Int16LiftedToNull = new EqualInstruction.EqualInt16LiftedToNull());
					}
					return result5;
				}
				case TypeCode.UInt16:
				{
					Instruction result6;
					if ((result6 = EqualInstruction.s_UInt16LiftedToNull) == null)
					{
						result6 = (EqualInstruction.s_UInt16LiftedToNull = new EqualInstruction.EqualUInt16LiftedToNull());
					}
					return result6;
				}
				case TypeCode.Int32:
				{
					Instruction result7;
					if ((result7 = EqualInstruction.s_Int32LiftedToNull) == null)
					{
						result7 = (EqualInstruction.s_Int32LiftedToNull = new EqualInstruction.EqualInt32LiftedToNull());
					}
					return result7;
				}
				case TypeCode.UInt32:
				{
					Instruction result8;
					if ((result8 = EqualInstruction.s_UInt32LiftedToNull) == null)
					{
						result8 = (EqualInstruction.s_UInt32LiftedToNull = new EqualInstruction.EqualUInt32LiftedToNull());
					}
					return result8;
				}
				case TypeCode.Int64:
				{
					Instruction result9;
					if ((result9 = EqualInstruction.s_Int64LiftedToNull) == null)
					{
						result9 = (EqualInstruction.s_Int64LiftedToNull = new EqualInstruction.EqualInt64LiftedToNull());
					}
					return result9;
				}
				case TypeCode.UInt64:
				{
					Instruction result10;
					if ((result10 = EqualInstruction.s_UInt64LiftedToNull) == null)
					{
						result10 = (EqualInstruction.s_UInt64LiftedToNull = new EqualInstruction.EqualUInt64LiftedToNull());
					}
					return result10;
				}
				case TypeCode.Single:
				{
					Instruction result11;
					if ((result11 = EqualInstruction.s_SingleLiftedToNull) == null)
					{
						result11 = (EqualInstruction.s_SingleLiftedToNull = new EqualInstruction.EqualSingleLiftedToNull());
					}
					return result11;
				}
				default:
				{
					Instruction result12;
					if ((result12 = EqualInstruction.s_DoubleLiftedToNull) == null)
					{
						result12 = (EqualInstruction.s_DoubleLiftedToNull = new EqualInstruction.EqualDoubleLiftedToNull());
					}
					return result12;
				}
				}
			}
			else
			{
				switch (type.GetNonNullableType().GetTypeCode())
				{
				case TypeCode.Boolean:
				{
					Instruction result13;
					if ((result13 = EqualInstruction.s_Boolean) == null)
					{
						result13 = (EqualInstruction.s_Boolean = new EqualInstruction.EqualBoolean());
					}
					return result13;
				}
				case TypeCode.Char:
				{
					Instruction result14;
					if ((result14 = EqualInstruction.s_Char) == null)
					{
						result14 = (EqualInstruction.s_Char = new EqualInstruction.EqualChar());
					}
					return result14;
				}
				case TypeCode.SByte:
				{
					Instruction result15;
					if ((result15 = EqualInstruction.s_SByte) == null)
					{
						result15 = (EqualInstruction.s_SByte = new EqualInstruction.EqualSByte());
					}
					return result15;
				}
				case TypeCode.Byte:
				{
					Instruction result16;
					if ((result16 = EqualInstruction.s_Byte) == null)
					{
						result16 = (EqualInstruction.s_Byte = new EqualInstruction.EqualByte());
					}
					return result16;
				}
				case TypeCode.Int16:
				{
					Instruction result17;
					if ((result17 = EqualInstruction.s_Int16) == null)
					{
						result17 = (EqualInstruction.s_Int16 = new EqualInstruction.EqualInt16());
					}
					return result17;
				}
				case TypeCode.UInt16:
				{
					Instruction result18;
					if ((result18 = EqualInstruction.s_UInt16) == null)
					{
						result18 = (EqualInstruction.s_UInt16 = new EqualInstruction.EqualUInt16());
					}
					return result18;
				}
				case TypeCode.Int32:
				{
					Instruction result19;
					if ((result19 = EqualInstruction.s_Int32) == null)
					{
						result19 = (EqualInstruction.s_Int32 = new EqualInstruction.EqualInt32());
					}
					return result19;
				}
				case TypeCode.UInt32:
				{
					Instruction result20;
					if ((result20 = EqualInstruction.s_UInt32) == null)
					{
						result20 = (EqualInstruction.s_UInt32 = new EqualInstruction.EqualUInt32());
					}
					return result20;
				}
				case TypeCode.Int64:
				{
					Instruction result21;
					if ((result21 = EqualInstruction.s_Int64) == null)
					{
						result21 = (EqualInstruction.s_Int64 = new EqualInstruction.EqualInt64());
					}
					return result21;
				}
				case TypeCode.UInt64:
				{
					Instruction result22;
					if ((result22 = EqualInstruction.s_UInt64) == null)
					{
						result22 = (EqualInstruction.s_UInt64 = new EqualInstruction.EqualUInt64());
					}
					return result22;
				}
				case TypeCode.Single:
				{
					Instruction result23;
					if ((result23 = EqualInstruction.s_Single) == null)
					{
						result23 = (EqualInstruction.s_Single = new EqualInstruction.EqualSingle());
					}
					return result23;
				}
				case TypeCode.Double:
				{
					Instruction result24;
					if ((result24 = EqualInstruction.s_Double) == null)
					{
						result24 = (EqualInstruction.s_Double = new EqualInstruction.EqualDouble());
					}
					return result24;
				}
				default:
				{
					Instruction result25;
					if ((result25 = EqualInstruction.s_reference) == null)
					{
						result25 = (EqualInstruction.s_reference = new EqualInstruction.EqualReference());
					}
					return result25;
				}
				}
			}
		}

		// Token: 0x04000A2F RID: 2607
		private static Instruction s_reference;

		// Token: 0x04000A30 RID: 2608
		private static Instruction s_Boolean;

		// Token: 0x04000A31 RID: 2609
		private static Instruction s_SByte;

		// Token: 0x04000A32 RID: 2610
		private static Instruction s_Int16;

		// Token: 0x04000A33 RID: 2611
		private static Instruction s_Char;

		// Token: 0x04000A34 RID: 2612
		private static Instruction s_Int32;

		// Token: 0x04000A35 RID: 2613
		private static Instruction s_Int64;

		// Token: 0x04000A36 RID: 2614
		private static Instruction s_Byte;

		// Token: 0x04000A37 RID: 2615
		private static Instruction s_UInt16;

		// Token: 0x04000A38 RID: 2616
		private static Instruction s_UInt32;

		// Token: 0x04000A39 RID: 2617
		private static Instruction s_UInt64;

		// Token: 0x04000A3A RID: 2618
		private static Instruction s_Single;

		// Token: 0x04000A3B RID: 2619
		private static Instruction s_Double;

		// Token: 0x04000A3C RID: 2620
		private static Instruction s_BooleanLiftedToNull;

		// Token: 0x04000A3D RID: 2621
		private static Instruction s_SByteLiftedToNull;

		// Token: 0x04000A3E RID: 2622
		private static Instruction s_Int16LiftedToNull;

		// Token: 0x04000A3F RID: 2623
		private static Instruction s_CharLiftedToNull;

		// Token: 0x04000A40 RID: 2624
		private static Instruction s_Int32LiftedToNull;

		// Token: 0x04000A41 RID: 2625
		private static Instruction s_Int64LiftedToNull;

		// Token: 0x04000A42 RID: 2626
		private static Instruction s_ByteLiftedToNull;

		// Token: 0x04000A43 RID: 2627
		private static Instruction s_UInt16LiftedToNull;

		// Token: 0x04000A44 RID: 2628
		private static Instruction s_UInt32LiftedToNull;

		// Token: 0x04000A45 RID: 2629
		private static Instruction s_UInt64LiftedToNull;

		// Token: 0x04000A46 RID: 2630
		private static Instruction s_SingleLiftedToNull;

		// Token: 0x04000A47 RID: 2631
		private static Instruction s_DoubleLiftedToNull;

		// Token: 0x020002DA RID: 730
		private sealed class EqualBoolean : EqualInstruction
		{
			// Token: 0x06001484 RID: 5252 RVA: 0x0003A370 File Offset: 0x00038570
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj == null);
				}
				else if (obj == null)
				{
					frame.Push(false);
				}
				else
				{
					frame.Push((bool)obj2 == (bool)obj);
				}
				return 1;
			}

			// Token: 0x06001485 RID: 5253 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualBoolean()
			{
			}
		}

		// Token: 0x020002DB RID: 731
		private sealed class EqualSByte : EqualInstruction
		{
			// Token: 0x06001486 RID: 5254 RVA: 0x0003A3C4 File Offset: 0x000385C4
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj == null);
				}
				else if (obj == null)
				{
					frame.Push(false);
				}
				else
				{
					frame.Push((sbyte)obj2 == (sbyte)obj);
				}
				return 1;
			}

			// Token: 0x06001487 RID: 5255 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualSByte()
			{
			}
		}

		// Token: 0x020002DC RID: 732
		private sealed class EqualInt16 : EqualInstruction
		{
			// Token: 0x06001488 RID: 5256 RVA: 0x0003A410 File Offset: 0x00038610
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj == null);
				}
				else if (obj == null)
				{
					frame.Push(false);
				}
				else
				{
					frame.Push((short)obj2 == (short)obj);
				}
				return 1;
			}

			// Token: 0x06001489 RID: 5257 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualInt16()
			{
			}
		}

		// Token: 0x020002DD RID: 733
		private sealed class EqualChar : EqualInstruction
		{
			// Token: 0x0600148A RID: 5258 RVA: 0x0003A45C File Offset: 0x0003865C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj == null);
				}
				else if (obj == null)
				{
					frame.Push(false);
				}
				else
				{
					frame.Push((char)obj2 == (char)obj);
				}
				return 1;
			}

			// Token: 0x0600148B RID: 5259 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualChar()
			{
			}
		}

		// Token: 0x020002DE RID: 734
		private sealed class EqualInt32 : EqualInstruction
		{
			// Token: 0x0600148C RID: 5260 RVA: 0x0003A4A8 File Offset: 0x000386A8
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj == null);
				}
				else if (obj == null)
				{
					frame.Push(false);
				}
				else
				{
					frame.Push((int)obj2 == (int)obj);
				}
				return 1;
			}

			// Token: 0x0600148D RID: 5261 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualInt32()
			{
			}
		}

		// Token: 0x020002DF RID: 735
		private sealed class EqualInt64 : EqualInstruction
		{
			// Token: 0x0600148E RID: 5262 RVA: 0x0003A4F4 File Offset: 0x000386F4
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj == null);
				}
				else if (obj == null)
				{
					frame.Push(false);
				}
				else
				{
					frame.Push((long)obj2 == (long)obj);
				}
				return 1;
			}

			// Token: 0x0600148F RID: 5263 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualInt64()
			{
			}
		}

		// Token: 0x020002E0 RID: 736
		private sealed class EqualByte : EqualInstruction
		{
			// Token: 0x06001490 RID: 5264 RVA: 0x0003A540 File Offset: 0x00038740
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj == null);
				}
				else if (obj == null)
				{
					frame.Push(false);
				}
				else
				{
					frame.Push((byte)obj2 == (byte)obj);
				}
				return 1;
			}

			// Token: 0x06001491 RID: 5265 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualByte()
			{
			}
		}

		// Token: 0x020002E1 RID: 737
		private sealed class EqualUInt16 : EqualInstruction
		{
			// Token: 0x06001492 RID: 5266 RVA: 0x0003A58C File Offset: 0x0003878C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj == null);
				}
				else if (obj == null)
				{
					frame.Push(false);
				}
				else
				{
					frame.Push((ushort)obj2 == (ushort)obj);
				}
				return 1;
			}

			// Token: 0x06001493 RID: 5267 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualUInt16()
			{
			}
		}

		// Token: 0x020002E2 RID: 738
		private sealed class EqualUInt32 : EqualInstruction
		{
			// Token: 0x06001494 RID: 5268 RVA: 0x0003A5D8 File Offset: 0x000387D8
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj == null);
				}
				else if (obj == null)
				{
					frame.Push(false);
				}
				else
				{
					frame.Push((uint)obj2 == (uint)obj);
				}
				return 1;
			}

			// Token: 0x06001495 RID: 5269 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualUInt32()
			{
			}
		}

		// Token: 0x020002E3 RID: 739
		private sealed class EqualUInt64 : EqualInstruction
		{
			// Token: 0x06001496 RID: 5270 RVA: 0x0003A624 File Offset: 0x00038824
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj == null);
				}
				else if (obj == null)
				{
					frame.Push(false);
				}
				else
				{
					frame.Push((ulong)obj2 == (ulong)obj);
				}
				return 1;
			}

			// Token: 0x06001497 RID: 5271 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualUInt64()
			{
			}
		}

		// Token: 0x020002E4 RID: 740
		private sealed class EqualSingle : EqualInstruction
		{
			// Token: 0x06001498 RID: 5272 RVA: 0x0003A670 File Offset: 0x00038870
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj == null);
				}
				else if (obj == null)
				{
					frame.Push(false);
				}
				else
				{
					frame.Push((float)obj2 == (float)obj);
				}
				return 1;
			}

			// Token: 0x06001499 RID: 5273 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualSingle()
			{
			}
		}

		// Token: 0x020002E5 RID: 741
		private sealed class EqualDouble : EqualInstruction
		{
			// Token: 0x0600149A RID: 5274 RVA: 0x0003A6BC File Offset: 0x000388BC
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj == null);
				}
				else if (obj == null)
				{
					frame.Push(false);
				}
				else
				{
					frame.Push((double)obj2 == (double)obj);
				}
				return 1;
			}

			// Token: 0x0600149B RID: 5275 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualDouble()
			{
			}
		}

		// Token: 0x020002E6 RID: 742
		private sealed class EqualReference : EqualInstruction
		{
			// Token: 0x0600149C RID: 5276 RVA: 0x0003A707 File Offset: 0x00038907
			public override int Run(InterpretedFrame frame)
			{
				frame.Push(frame.Pop() == frame.Pop());
				return 1;
			}

			// Token: 0x0600149D RID: 5277 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualReference()
			{
			}
		}

		// Token: 0x020002E7 RID: 743
		private sealed class EqualBooleanLiftedToNull : EqualInstruction
		{
			// Token: 0x0600149E RID: 5278 RVA: 0x0003A720 File Offset: 0x00038920
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((bool)obj2 == (bool)obj);
				}
				return 1;
			}

			// Token: 0x0600149F RID: 5279 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualBooleanLiftedToNull()
			{
			}
		}

		// Token: 0x020002E8 RID: 744
		private sealed class EqualSByteLiftedToNull : EqualInstruction
		{
			// Token: 0x060014A0 RID: 5280 RVA: 0x0003A760 File Offset: 0x00038960
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((sbyte)obj2 == (sbyte)obj);
				}
				return 1;
			}

			// Token: 0x060014A1 RID: 5281 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualSByteLiftedToNull()
			{
			}
		}

		// Token: 0x020002E9 RID: 745
		private sealed class EqualInt16LiftedToNull : EqualInstruction
		{
			// Token: 0x060014A2 RID: 5282 RVA: 0x0003A7A0 File Offset: 0x000389A0
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((short)obj2 == (short)obj);
				}
				return 1;
			}

			// Token: 0x060014A3 RID: 5283 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualInt16LiftedToNull()
			{
			}
		}

		// Token: 0x020002EA RID: 746
		private sealed class EqualCharLiftedToNull : EqualInstruction
		{
			// Token: 0x060014A4 RID: 5284 RVA: 0x0003A7E0 File Offset: 0x000389E0
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((char)obj2 == (char)obj);
				}
				return 1;
			}

			// Token: 0x060014A5 RID: 5285 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualCharLiftedToNull()
			{
			}
		}

		// Token: 0x020002EB RID: 747
		private sealed class EqualInt32LiftedToNull : EqualInstruction
		{
			// Token: 0x060014A6 RID: 5286 RVA: 0x0003A820 File Offset: 0x00038A20
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((int)obj2 == (int)obj);
				}
				return 1;
			}

			// Token: 0x060014A7 RID: 5287 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualInt32LiftedToNull()
			{
			}
		}

		// Token: 0x020002EC RID: 748
		private sealed class EqualInt64LiftedToNull : EqualInstruction
		{
			// Token: 0x060014A8 RID: 5288 RVA: 0x0003A860 File Offset: 0x00038A60
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((long)obj2 == (long)obj);
				}
				return 1;
			}

			// Token: 0x060014A9 RID: 5289 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualInt64LiftedToNull()
			{
			}
		}

		// Token: 0x020002ED RID: 749
		private sealed class EqualByteLiftedToNull : EqualInstruction
		{
			// Token: 0x060014AA RID: 5290 RVA: 0x0003A8A0 File Offset: 0x00038AA0
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((byte)obj2 == (byte)obj);
				}
				return 1;
			}

			// Token: 0x060014AB RID: 5291 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualByteLiftedToNull()
			{
			}
		}

		// Token: 0x020002EE RID: 750
		private sealed class EqualUInt16LiftedToNull : EqualInstruction
		{
			// Token: 0x060014AC RID: 5292 RVA: 0x0003A8E0 File Offset: 0x00038AE0
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((ushort)obj2 == (ushort)obj);
				}
				return 1;
			}

			// Token: 0x060014AD RID: 5293 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualUInt16LiftedToNull()
			{
			}
		}

		// Token: 0x020002EF RID: 751
		private sealed class EqualUInt32LiftedToNull : EqualInstruction
		{
			// Token: 0x060014AE RID: 5294 RVA: 0x0003A920 File Offset: 0x00038B20
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((uint)obj2 == (uint)obj);
				}
				return 1;
			}

			// Token: 0x060014AF RID: 5295 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualUInt32LiftedToNull()
			{
			}
		}

		// Token: 0x020002F0 RID: 752
		private sealed class EqualUInt64LiftedToNull : EqualInstruction
		{
			// Token: 0x060014B0 RID: 5296 RVA: 0x0003A960 File Offset: 0x00038B60
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((ulong)obj2 == (ulong)obj);
				}
				return 1;
			}

			// Token: 0x060014B1 RID: 5297 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualUInt64LiftedToNull()
			{
			}
		}

		// Token: 0x020002F1 RID: 753
		private sealed class EqualSingleLiftedToNull : EqualInstruction
		{
			// Token: 0x060014B2 RID: 5298 RVA: 0x0003A9A0 File Offset: 0x00038BA0
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((float)obj2 == (float)obj);
				}
				return 1;
			}

			// Token: 0x060014B3 RID: 5299 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualSingleLiftedToNull()
			{
			}
		}

		// Token: 0x020002F2 RID: 754
		private sealed class EqualDoubleLiftedToNull : EqualInstruction
		{
			// Token: 0x060014B4 RID: 5300 RVA: 0x0003A9E0 File Offset: 0x00038BE0
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((double)obj2 == (double)obj);
				}
				return 1;
			}

			// Token: 0x060014B5 RID: 5301 RVA: 0x0003A3BB File Offset: 0x000385BB
			public EqualDoubleLiftedToNull()
			{
			}
		}
	}
}
