﻿using System;
using System.Globalization;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200037C RID: 892
	internal sealed class LocalVariable
	{
		// Token: 0x1700048E RID: 1166
		// (get) Token: 0x06001784 RID: 6020 RVA: 0x00045E54 File Offset: 0x00044054
		// (set) Token: 0x06001785 RID: 6021 RVA: 0x00045E61 File Offset: 0x00044061
		public bool IsBoxed
		{
			get
			{
				return (this._flags & 1) != 0;
			}
			set
			{
				if (value)
				{
					this._flags |= 1;
					return;
				}
				this._flags &= -2;
			}
		}

		// Token: 0x1700048F RID: 1167
		// (get) Token: 0x06001786 RID: 6022 RVA: 0x00045E84 File Offset: 0x00044084
		public bool InClosure
		{
			get
			{
				return (this._flags & 2) != 0;
			}
		}

		// Token: 0x06001787 RID: 6023 RVA: 0x00045E91 File Offset: 0x00044091
		internal LocalVariable(int index, bool closure)
		{
			this.Index = index;
			this._flags = (closure ? 2 : 0);
		}

		// Token: 0x06001788 RID: 6024 RVA: 0x00045EAD File Offset: 0x000440AD
		public override string ToString()
		{
			return string.Format(CultureInfo.InvariantCulture, "{0}: {1} {2}", this.Index, this.IsBoxed ? "boxed" : null, this.InClosure ? "in closure" : null);
		}

		// Token: 0x04000B5F RID: 2911
		private const int IsBoxedFlag = 1;

		// Token: 0x04000B60 RID: 2912
		private const int InClosureFlag = 2;

		// Token: 0x04000B61 RID: 2913
		public readonly int Index;

		// Token: 0x04000B62 RID: 2914
		private int _flags;
	}
}
