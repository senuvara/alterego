﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020003F5 RID: 1013
	internal sealed class CreateDelegateInstruction : Instruction
	{
		// Token: 0x060018CC RID: 6348 RVA: 0x0004953D File Offset: 0x0004773D
		internal CreateDelegateInstruction(LightDelegateCreator delegateCreator)
		{
			this._creator = delegateCreator;
		}

		// Token: 0x170004CC RID: 1228
		// (get) Token: 0x060018CD RID: 6349 RVA: 0x0004954C File Offset: 0x0004774C
		public override int ConsumedStack
		{
			get
			{
				return this._creator.Interpreter.ClosureSize;
			}
		}

		// Token: 0x170004CD RID: 1229
		// (get) Token: 0x060018CE RID: 6350 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004CE RID: 1230
		// (get) Token: 0x060018CF RID: 6351 RVA: 0x0004955E File Offset: 0x0004775E
		public override string InstructionName
		{
			get
			{
				return "CreateDelegate";
			}
		}

		// Token: 0x060018D0 RID: 6352 RVA: 0x00049568 File Offset: 0x00047768
		public override int Run(InterpretedFrame frame)
		{
			IStrongBox[] array;
			if (this.ConsumedStack > 0)
			{
				array = new IStrongBox[this.ConsumedStack];
				for (int i = array.Length - 1; i >= 0; i--)
				{
					array[i] = (IStrongBox)frame.Pop();
				}
			}
			else
			{
				array = null;
			}
			Delegate value = this._creator.CreateDelegate(array);
			frame.Push(value);
			return 1;
		}

		// Token: 0x04000BD8 RID: 3032
		private readonly LightDelegateCreator _creator;
	}
}
