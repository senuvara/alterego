﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200031A RID: 794
	internal abstract class IncrementInstruction : Instruction
	{
		// Token: 0x17000440 RID: 1088
		// (get) Token: 0x06001516 RID: 5398 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000441 RID: 1089
		// (get) Token: 0x06001517 RID: 5399 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000442 RID: 1090
		// (get) Token: 0x06001518 RID: 5400 RVA: 0x0003BA3B File Offset: 0x00039C3B
		public override string InstructionName
		{
			get
			{
				return "Increment";
			}
		}

		// Token: 0x06001519 RID: 5401 RVA: 0x00037C51 File Offset: 0x00035E51
		private IncrementInstruction()
		{
		}

		// Token: 0x0600151A RID: 5402 RVA: 0x0003BA44 File Offset: 0x00039C44
		public static Instruction Create(Type type)
		{
			switch (type.GetNonNullableType().GetTypeCode())
			{
			case TypeCode.Int16:
			{
				Instruction result;
				if ((result = IncrementInstruction.s_Int16) == null)
				{
					result = (IncrementInstruction.s_Int16 = new IncrementInstruction.IncrementInt16());
				}
				return result;
			}
			case TypeCode.UInt16:
			{
				Instruction result2;
				if ((result2 = IncrementInstruction.s_UInt16) == null)
				{
					result2 = (IncrementInstruction.s_UInt16 = new IncrementInstruction.IncrementUInt16());
				}
				return result2;
			}
			case TypeCode.Int32:
			{
				Instruction result3;
				if ((result3 = IncrementInstruction.s_Int32) == null)
				{
					result3 = (IncrementInstruction.s_Int32 = new IncrementInstruction.IncrementInt32());
				}
				return result3;
			}
			case TypeCode.UInt32:
			{
				Instruction result4;
				if ((result4 = IncrementInstruction.s_UInt32) == null)
				{
					result4 = (IncrementInstruction.s_UInt32 = new IncrementInstruction.IncrementUInt32());
				}
				return result4;
			}
			case TypeCode.Int64:
			{
				Instruction result5;
				if ((result5 = IncrementInstruction.s_Int64) == null)
				{
					result5 = (IncrementInstruction.s_Int64 = new IncrementInstruction.IncrementInt64());
				}
				return result5;
			}
			case TypeCode.UInt64:
			{
				Instruction result6;
				if ((result6 = IncrementInstruction.s_UInt64) == null)
				{
					result6 = (IncrementInstruction.s_UInt64 = new IncrementInstruction.IncrementUInt64());
				}
				return result6;
			}
			case TypeCode.Single:
			{
				Instruction result7;
				if ((result7 = IncrementInstruction.s_Single) == null)
				{
					result7 = (IncrementInstruction.s_Single = new IncrementInstruction.IncrementSingle());
				}
				return result7;
			}
			case TypeCode.Double:
			{
				Instruction result8;
				if ((result8 = IncrementInstruction.s_Double) == null)
				{
					result8 = (IncrementInstruction.s_Double = new IncrementInstruction.IncrementDouble());
				}
				return result8;
			}
			default:
				throw ContractUtils.Unreachable;
			}
		}

		// Token: 0x04000A80 RID: 2688
		private static Instruction s_Int16;

		// Token: 0x04000A81 RID: 2689
		private static Instruction s_Int32;

		// Token: 0x04000A82 RID: 2690
		private static Instruction s_Int64;

		// Token: 0x04000A83 RID: 2691
		private static Instruction s_UInt16;

		// Token: 0x04000A84 RID: 2692
		private static Instruction s_UInt32;

		// Token: 0x04000A85 RID: 2693
		private static Instruction s_UInt64;

		// Token: 0x04000A86 RID: 2694
		private static Instruction s_Single;

		// Token: 0x04000A87 RID: 2695
		private static Instruction s_Double;

		// Token: 0x0200031B RID: 795
		private sealed class IncrementInt16 : IncrementInstruction
		{
			// Token: 0x0600151B RID: 5403 RVA: 0x0003BB38 File Offset: 0x00039D38
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(1 + (short)obj);
				}
				return 1;
			}

			// Token: 0x0600151C RID: 5404 RVA: 0x0003BB68 File Offset: 0x00039D68
			public IncrementInt16()
			{
			}
		}

		// Token: 0x0200031C RID: 796
		private sealed class IncrementInt32 : IncrementInstruction
		{
			// Token: 0x0600151D RID: 5405 RVA: 0x0003BB70 File Offset: 0x00039D70
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(1 + (int)obj);
				}
				return 1;
			}

			// Token: 0x0600151E RID: 5406 RVA: 0x0003BB68 File Offset: 0x00039D68
			public IncrementInt32()
			{
			}
		}

		// Token: 0x0200031D RID: 797
		private sealed class IncrementInt64 : IncrementInstruction
		{
			// Token: 0x0600151F RID: 5407 RVA: 0x0003BBA0 File Offset: 0x00039DA0
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(1L + (long)obj);
				}
				return 1;
			}

			// Token: 0x06001520 RID: 5408 RVA: 0x0003BB68 File Offset: 0x00039D68
			public IncrementInt64()
			{
			}
		}

		// Token: 0x0200031E RID: 798
		private sealed class IncrementUInt16 : IncrementInstruction
		{
			// Token: 0x06001521 RID: 5409 RVA: 0x0003BBD8 File Offset: 0x00039DD8
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(1 + (ushort)obj);
				}
				return 1;
			}

			// Token: 0x06001522 RID: 5410 RVA: 0x0003BB68 File Offset: 0x00039D68
			public IncrementUInt16()
			{
			}
		}

		// Token: 0x0200031F RID: 799
		private sealed class IncrementUInt32 : IncrementInstruction
		{
			// Token: 0x06001523 RID: 5411 RVA: 0x0003BC08 File Offset: 0x00039E08
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(1U + (uint)obj);
				}
				return 1;
			}

			// Token: 0x06001524 RID: 5412 RVA: 0x0003BB68 File Offset: 0x00039D68
			public IncrementUInt32()
			{
			}
		}

		// Token: 0x02000320 RID: 800
		private sealed class IncrementUInt64 : IncrementInstruction
		{
			// Token: 0x06001525 RID: 5413 RVA: 0x0003BC3C File Offset: 0x00039E3C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(1UL + (ulong)obj);
				}
				return 1;
			}

			// Token: 0x06001526 RID: 5414 RVA: 0x0003BB68 File Offset: 0x00039D68
			public IncrementUInt64()
			{
			}
		}

		// Token: 0x02000321 RID: 801
		private sealed class IncrementSingle : IncrementInstruction
		{
			// Token: 0x06001527 RID: 5415 RVA: 0x0003BC74 File Offset: 0x00039E74
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(1f + (float)obj);
				}
				return 1;
			}

			// Token: 0x06001528 RID: 5416 RVA: 0x0003BB68 File Offset: 0x00039D68
			public IncrementSingle()
			{
			}
		}

		// Token: 0x02000322 RID: 802
		private sealed class IncrementDouble : IncrementInstruction
		{
			// Token: 0x06001529 RID: 5417 RVA: 0x0003BCAC File Offset: 0x00039EAC
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(1.0 + (double)obj);
				}
				return 1;
			}

			// Token: 0x0600152A RID: 5418 RVA: 0x0003BB68 File Offset: 0x00039D68
			public IncrementDouble()
			{
			}
		}
	}
}
