﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002BE RID: 702
	internal sealed class LeaveFaultInstruction : Instruction
	{
		// Token: 0x06001423 RID: 5155 RVA: 0x00037C51 File Offset: 0x00035E51
		private LeaveFaultInstruction()
		{
		}

		// Token: 0x1700040F RID: 1039
		// (get) Token: 0x06001424 RID: 5156 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x17000410 RID: 1040
		// (get) Token: 0x06001425 RID: 5157 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedContinuations
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000411 RID: 1041
		// (get) Token: 0x06001426 RID: 5158 RVA: 0x000397A3 File Offset: 0x000379A3
		public override string InstructionName
		{
			get
			{
				return "LeaveFault";
			}
		}

		// Token: 0x06001427 RID: 5159 RVA: 0x000397AA File Offset: 0x000379AA
		public override int Run(InterpretedFrame frame)
		{
			frame.PopPendingContinuation();
			return 1;
		}

		// Token: 0x06001428 RID: 5160 RVA: 0x000397B3 File Offset: 0x000379B3
		// Note: this type is marked as 'beforefieldinit'.
		static LeaveFaultInstruction()
		{
		}

		// Token: 0x04000A0C RID: 2572
		internal static readonly Instruction Instance = new LeaveFaultInstruction();
	}
}
