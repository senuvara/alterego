﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020003C8 RID: 968
	internal sealed class NullCheckInstruction : Instruction
	{
		// Token: 0x06001842 RID: 6210 RVA: 0x00037C51 File Offset: 0x00035E51
		private NullCheckInstruction()
		{
		}

		// Token: 0x170004AD RID: 1197
		// (get) Token: 0x06001843 RID: 6211 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004AE RID: 1198
		// (get) Token: 0x06001844 RID: 6212 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004AF RID: 1199
		// (get) Token: 0x06001845 RID: 6213 RVA: 0x00047CF7 File Offset: 0x00045EF7
		public override string InstructionName
		{
			get
			{
				return "Unbox";
			}
		}

		// Token: 0x06001846 RID: 6214 RVA: 0x00047CFE File Offset: 0x00045EFE
		public override int Run(InterpretedFrame frame)
		{
			if (frame.Peek() == null)
			{
				throw new NullReferenceException();
			}
			return 1;
		}

		// Token: 0x06001847 RID: 6215 RVA: 0x00047D0F File Offset: 0x00045F0F
		// Note: this type is marked as 'beforefieldinit'.
		static NullCheckInstruction()
		{
		}

		// Token: 0x04000BB0 RID: 2992
		public static readonly Instruction Instance = new NullCheckInstruction();
	}
}
