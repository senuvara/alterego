﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200040C RID: 1036
	internal static class ScriptingRuntimeHelpers
	{
		// Token: 0x0600191C RID: 6428 RVA: 0x0004A3F8 File Offset: 0x000485F8
		public static object Int32ToObject(int i)
		{
			switch (i)
			{
			case -1:
				return Utils.BoxedIntM1;
			case 0:
				return Utils.BoxedInt0;
			case 1:
				return Utils.BoxedInt1;
			case 2:
				return Utils.BoxedInt2;
			case 3:
				return Utils.BoxedInt3;
			default:
				return i;
			}
		}

		// Token: 0x0600191D RID: 6429 RVA: 0x0004A448 File Offset: 0x00048648
		internal static object GetPrimitiveDefaultValue(Type type)
		{
			object obj;
			switch (type.GetTypeCode())
			{
			case TypeCode.Boolean:
				obj = Utils.BoxedFalse;
				break;
			case TypeCode.Char:
				obj = Utils.BoxedDefaultChar;
				break;
			case TypeCode.SByte:
				obj = Utils.BoxedDefaultSByte;
				break;
			case TypeCode.Byte:
				obj = Utils.BoxedDefaultByte;
				break;
			case TypeCode.Int16:
				obj = Utils.BoxedDefaultInt16;
				break;
			case TypeCode.UInt16:
				obj = Utils.BoxedDefaultUInt16;
				break;
			case TypeCode.Int32:
				obj = Utils.BoxedInt0;
				break;
			case TypeCode.UInt32:
				obj = Utils.BoxedDefaultUInt32;
				break;
			case TypeCode.Int64:
				obj = Utils.BoxedDefaultInt64;
				break;
			case TypeCode.UInt64:
				obj = Utils.BoxedDefaultUInt64;
				break;
			case TypeCode.Single:
				return Utils.BoxedDefaultSingle;
			case TypeCode.Double:
				return Utils.BoxedDefaultDouble;
			case TypeCode.Decimal:
				return Utils.BoxedDefaultDecimal;
			case TypeCode.DateTime:
				return Utils.BoxedDefaultDateTime;
			default:
				return null;
			}
			if (type.IsEnum)
			{
				obj = Enum.ToObject(type, obj);
			}
			return obj;
		}
	}
}
