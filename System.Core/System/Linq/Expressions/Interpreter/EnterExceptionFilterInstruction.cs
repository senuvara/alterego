﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002BF RID: 703
	internal sealed class EnterExceptionFilterInstruction : Instruction
	{
		// Token: 0x06001429 RID: 5161 RVA: 0x00037C51 File Offset: 0x00035E51
		private EnterExceptionFilterInstruction()
		{
		}

		// Token: 0x17000412 RID: 1042
		// (get) Token: 0x0600142A RID: 5162 RVA: 0x000397BF File Offset: 0x000379BF
		public override string InstructionName
		{
			get
			{
				return "EnterExceptionFilter";
			}
		}

		// Token: 0x17000413 RID: 1043
		// (get) Token: 0x0600142B RID: 5163 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x0600142C RID: 5164 RVA: 0x00009CDF File Offset: 0x00007EDF
		[ExcludeFromCodeCoverage]
		public override int Run(InterpretedFrame frame)
		{
			return 1;
		}

		// Token: 0x0600142D RID: 5165 RVA: 0x000397C6 File Offset: 0x000379C6
		// Note: this type is marked as 'beforefieldinit'.
		static EnterExceptionFilterInstruction()
		{
		}

		// Token: 0x04000A0D RID: 2573
		internal static readonly EnterExceptionFilterInstruction Instance = new EnterExceptionFilterInstruction();
	}
}
