﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200039F RID: 927
	internal abstract class NegateCheckedInstruction : Instruction
	{
		// Token: 0x170004A0 RID: 1184
		// (get) Token: 0x060017E1 RID: 6113 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004A1 RID: 1185
		// (get) Token: 0x060017E2 RID: 6114 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004A2 RID: 1186
		// (get) Token: 0x060017E3 RID: 6115 RVA: 0x00046DB3 File Offset: 0x00044FB3
		public override string InstructionName
		{
			get
			{
				return "NegateChecked";
			}
		}

		// Token: 0x060017E4 RID: 6116 RVA: 0x00037C51 File Offset: 0x00035E51
		private NegateCheckedInstruction()
		{
		}

		// Token: 0x060017E5 RID: 6117 RVA: 0x00046DBC File Offset: 0x00044FBC
		public static Instruction Create(Type type)
		{
			switch (type.GetNonNullableType().GetTypeCode())
			{
			case TypeCode.Int16:
			{
				Instruction result;
				if ((result = NegateCheckedInstruction.s_Int16) == null)
				{
					result = (NegateCheckedInstruction.s_Int16 = new NegateCheckedInstruction.NegateCheckedInt16());
				}
				return result;
			}
			case TypeCode.Int32:
			{
				Instruction result2;
				if ((result2 = NegateCheckedInstruction.s_Int32) == null)
				{
					result2 = (NegateCheckedInstruction.s_Int32 = new NegateCheckedInstruction.NegateCheckedInt32());
				}
				return result2;
			}
			case TypeCode.Int64:
			{
				Instruction result3;
				if ((result3 = NegateCheckedInstruction.s_Int64) == null)
				{
					result3 = (NegateCheckedInstruction.s_Int64 = new NegateCheckedInstruction.NegateCheckedInt64());
				}
				return result3;
			}
			}
			return NegateInstruction.Create(type);
		}

		// Token: 0x04000B89 RID: 2953
		private static Instruction s_Int16;

		// Token: 0x04000B8A RID: 2954
		private static Instruction s_Int32;

		// Token: 0x04000B8B RID: 2955
		private static Instruction s_Int64;

		// Token: 0x020003A0 RID: 928
		private sealed class NegateCheckedInt32 : NegateCheckedInstruction
		{
			// Token: 0x060017E6 RID: 6118 RVA: 0x00046E38 File Offset: 0x00045038
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(checked(0 - (int)obj));
				}
				return 1;
			}

			// Token: 0x060017E7 RID: 6119 RVA: 0x00046E67 File Offset: 0x00045067
			public NegateCheckedInt32()
			{
			}
		}

		// Token: 0x020003A1 RID: 929
		private sealed class NegateCheckedInt16 : NegateCheckedInstruction
		{
			// Token: 0x060017E8 RID: 6120 RVA: 0x00046E70 File Offset: 0x00045070
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(checked(0 - (short)obj));
				}
				return 1;
			}

			// Token: 0x060017E9 RID: 6121 RVA: 0x00046E67 File Offset: 0x00045067
			public NegateCheckedInt16()
			{
			}
		}

		// Token: 0x020003A2 RID: 930
		private sealed class NegateCheckedInt64 : NegateCheckedInstruction
		{
			// Token: 0x060017EA RID: 6122 RVA: 0x00046EA0 File Offset: 0x000450A0
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push(checked(0L - (long)obj));
				}
				return 1;
			}

			// Token: 0x060017EB RID: 6123 RVA: 0x00046E67 File Offset: 0x00045067
			public NegateCheckedInt64()
			{
			}
		}
	}
}
