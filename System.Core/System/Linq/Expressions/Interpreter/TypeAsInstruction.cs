﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020003F7 RID: 1015
	internal sealed class TypeAsInstruction : Instruction
	{
		// Token: 0x060018D7 RID: 6359 RVA: 0x00049608 File Offset: 0x00047808
		internal TypeAsInstruction(Type type)
		{
			this._type = type;
		}

		// Token: 0x170004D2 RID: 1234
		// (get) Token: 0x060018D8 RID: 6360 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004D3 RID: 1235
		// (get) Token: 0x060018D9 RID: 6361 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004D4 RID: 1236
		// (get) Token: 0x060018DA RID: 6362 RVA: 0x00049617 File Offset: 0x00047817
		public override string InstructionName
		{
			get
			{
				return "TypeAs";
			}
		}

		// Token: 0x060018DB RID: 6363 RVA: 0x00049620 File Offset: 0x00047820
		public override int Run(InterpretedFrame frame)
		{
			object obj = frame.Pop();
			frame.Push(this._type.IsInstanceOfType(obj) ? obj : null);
			return 1;
		}

		// Token: 0x060018DC RID: 6364 RVA: 0x0004964D File Offset: 0x0004784D
		public override string ToString()
		{
			return "TypeAs " + this._type.ToString();
		}

		// Token: 0x04000BDA RID: 3034
		private readonly Type _type;
	}
}
