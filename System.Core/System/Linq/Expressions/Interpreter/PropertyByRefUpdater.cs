﻿using System;
using System.Dynamic.Utils;
using System.Reflection;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000360 RID: 864
	internal sealed class PropertyByRefUpdater : ByRefUpdater
	{
		// Token: 0x060016D0 RID: 5840 RVA: 0x0004333F File Offset: 0x0004153F
		public PropertyByRefUpdater(LocalDefinition? obj, PropertyInfo property, int argumentIndex) : base(argumentIndex)
		{
			this._object = obj;
			this._property = property;
		}

		// Token: 0x060016D1 RID: 5841 RVA: 0x00043358 File Offset: 0x00041558
		public override void Update(InterpretedFrame frame, object value)
		{
			object obj = (this._object == null) ? null : frame.Data[this._object.GetValueOrDefault().Index];
			try
			{
				this._property.SetValue(obj, value);
			}
			catch (TargetInvocationException exception)
			{
				ExceptionHelpers.UnwrapAndRethrow(exception);
				throw ContractUtils.Unreachable;
			}
		}

		// Token: 0x060016D2 RID: 5842 RVA: 0x000433C0 File Offset: 0x000415C0
		public override void UndefineTemps(InstructionList instructions, LocalVariables locals)
		{
			if (this._object != null)
			{
				locals.UndefineLocal(this._object.GetValueOrDefault(), instructions.Count);
			}
		}

		// Token: 0x04000B45 RID: 2885
		private readonly LocalDefinition? _object;

		// Token: 0x04000B46 RID: 2886
		private readonly PropertyInfo _property;
	}
}
