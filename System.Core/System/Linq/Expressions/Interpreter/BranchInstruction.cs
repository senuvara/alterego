﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002B6 RID: 694
	internal class BranchInstruction : OffsetInstruction
	{
		// Token: 0x170003FB RID: 1019
		// (get) Token: 0x060013F0 RID: 5104 RVA: 0x000390A0 File Offset: 0x000372A0
		public override Instruction[] Cache
		{
			get
			{
				if (BranchInstruction.s_caches == null)
				{
					BranchInstruction.s_caches = new Instruction[][][]
					{
						new Instruction[2][],
						new Instruction[2][]
					};
				}
				Instruction[] result;
				if ((result = BranchInstruction.s_caches[this.ConsumedStack][this.ProducedStack]) == null)
				{
					result = (BranchInstruction.s_caches[this.ConsumedStack][this.ProducedStack] = new Instruction[32]);
				}
				return result;
			}
		}

		// Token: 0x060013F1 RID: 5105 RVA: 0x00039105 File Offset: 0x00037305
		internal BranchInstruction() : this(false, false)
		{
		}

		// Token: 0x060013F2 RID: 5106 RVA: 0x0003910F File Offset: 0x0003730F
		public BranchInstruction(bool hasResult, bool hasValue)
		{
			this._hasResult = hasResult;
			this._hasValue = hasValue;
		}

		// Token: 0x170003FC RID: 1020
		// (get) Token: 0x060013F3 RID: 5107 RVA: 0x00039125 File Offset: 0x00037325
		public override string InstructionName
		{
			get
			{
				return "Branch";
			}
		}

		// Token: 0x170003FD RID: 1021
		// (get) Token: 0x060013F4 RID: 5108 RVA: 0x0003912C File Offset: 0x0003732C
		public override int ConsumedStack
		{
			get
			{
				if (!this._hasValue)
				{
					return 0;
				}
				return 1;
			}
		}

		// Token: 0x170003FE RID: 1022
		// (get) Token: 0x060013F5 RID: 5109 RVA: 0x00039139 File Offset: 0x00037339
		public override int ProducedStack
		{
			get
			{
				if (!this._hasResult)
				{
					return 0;
				}
				return 1;
			}
		}

		// Token: 0x060013F6 RID: 5110 RVA: 0x00039146 File Offset: 0x00037346
		public override int Run(InterpretedFrame frame)
		{
			return this._offset;
		}

		// Token: 0x040009FC RID: 2556
		private static Instruction[][][] s_caches;

		// Token: 0x040009FD RID: 2557
		internal readonly bool _hasResult;

		// Token: 0x040009FE RID: 2558
		internal readonly bool _hasValue;
	}
}
