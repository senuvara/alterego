﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200032F RID: 815
	internal sealed class LabelScopeInfo
	{
		// Token: 0x060015F7 RID: 5623 RVA: 0x0003D89B File Offset: 0x0003BA9B
		internal LabelScopeInfo(LabelScopeInfo parent, LabelScopeKind kind)
		{
			this.Parent = parent;
			this.Kind = kind;
		}

		// Token: 0x1700045B RID: 1115
		// (get) Token: 0x060015F8 RID: 5624 RVA: 0x0003D8B4 File Offset: 0x0003BAB4
		internal bool CanJumpInto
		{
			get
			{
				LabelScopeKind kind = this.Kind;
				return kind <= LabelScopeKind.Lambda;
			}
		}

		// Token: 0x060015F9 RID: 5625 RVA: 0x0003D8CF File Offset: 0x0003BACF
		internal bool ContainsTarget(LabelTarget target)
		{
			return this._labels != null && this._labels.ContainsKey(target);
		}

		// Token: 0x060015FA RID: 5626 RVA: 0x0003D8E7 File Offset: 0x0003BAE7
		internal bool TryGetLabelInfo(LabelTarget target, out LabelInfo info)
		{
			if (this._labels == null)
			{
				info = null;
				return false;
			}
			return this._labels.TryGetValue(target, out info);
		}

		// Token: 0x060015FB RID: 5627 RVA: 0x0003D903 File Offset: 0x0003BB03
		internal void AddLabelInfo(LabelTarget target, LabelInfo info)
		{
			if (this._labels == null)
			{
				this._labels = new HybridReferenceDictionary<LabelTarget, LabelInfo>();
			}
			this._labels[target] = info;
		}

		// Token: 0x04000ADD RID: 2781
		private HybridReferenceDictionary<LabelTarget, LabelInfo> _labels;

		// Token: 0x04000ADE RID: 2782
		internal readonly LabelScopeKind Kind;

		// Token: 0x04000ADF RID: 2783
		internal readonly LabelScopeInfo Parent;
	}
}
