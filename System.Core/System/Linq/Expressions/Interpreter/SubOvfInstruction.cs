﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020003EE RID: 1006
	internal abstract class SubOvfInstruction : Instruction
	{
		// Token: 0x170004C9 RID: 1225
		// (get) Token: 0x060018BB RID: 6331 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x170004CA RID: 1226
		// (get) Token: 0x060018BC RID: 6332 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004CB RID: 1227
		// (get) Token: 0x060018BD RID: 6333 RVA: 0x00049279 File Offset: 0x00047479
		public override string InstructionName
		{
			get
			{
				return "SubOvf";
			}
		}

		// Token: 0x060018BE RID: 6334 RVA: 0x00037C51 File Offset: 0x00035E51
		private SubOvfInstruction()
		{
		}

		// Token: 0x060018BF RID: 6335 RVA: 0x00049280 File Offset: 0x00047480
		public static Instruction Create(Type type)
		{
			switch (type.GetNonNullableType().GetTypeCode())
			{
			case TypeCode.Int16:
			{
				Instruction result;
				if ((result = SubOvfInstruction.s_Int16) == null)
				{
					result = (SubOvfInstruction.s_Int16 = new SubOvfInstruction.SubOvfInt16());
				}
				return result;
			}
			case TypeCode.UInt16:
			{
				Instruction result2;
				if ((result2 = SubOvfInstruction.s_UInt16) == null)
				{
					result2 = (SubOvfInstruction.s_UInt16 = new SubOvfInstruction.SubOvfUInt16());
				}
				return result2;
			}
			case TypeCode.Int32:
			{
				Instruction result3;
				if ((result3 = SubOvfInstruction.s_Int32) == null)
				{
					result3 = (SubOvfInstruction.s_Int32 = new SubOvfInstruction.SubOvfInt32());
				}
				return result3;
			}
			case TypeCode.UInt32:
			{
				Instruction result4;
				if ((result4 = SubOvfInstruction.s_UInt32) == null)
				{
					result4 = (SubOvfInstruction.s_UInt32 = new SubOvfInstruction.SubOvfUInt32());
				}
				return result4;
			}
			case TypeCode.Int64:
			{
				Instruction result5;
				if ((result5 = SubOvfInstruction.s_Int64) == null)
				{
					result5 = (SubOvfInstruction.s_Int64 = new SubOvfInstruction.SubOvfInt64());
				}
				return result5;
			}
			case TypeCode.UInt64:
			{
				Instruction result6;
				if ((result6 = SubOvfInstruction.s_UInt64) == null)
				{
					result6 = (SubOvfInstruction.s_UInt64 = new SubOvfInstruction.SubOvfUInt64());
				}
				return result6;
			}
			default:
				return SubInstruction.Create(type);
			}
		}

		// Token: 0x04000BD2 RID: 3026
		private static Instruction s_Int16;

		// Token: 0x04000BD3 RID: 3027
		private static Instruction s_Int32;

		// Token: 0x04000BD4 RID: 3028
		private static Instruction s_Int64;

		// Token: 0x04000BD5 RID: 3029
		private static Instruction s_UInt16;

		// Token: 0x04000BD6 RID: 3030
		private static Instruction s_UInt32;

		// Token: 0x04000BD7 RID: 3031
		private static Instruction s_UInt64;

		// Token: 0x020003EF RID: 1007
		private sealed class SubOvfInt16 : SubOvfInstruction
		{
			// Token: 0x060018C0 RID: 6336 RVA: 0x00049340 File Offset: 0x00047540
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : checked((short)obj - (short)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060018C1 RID: 6337 RVA: 0x00049392 File Offset: 0x00047592
			public SubOvfInt16()
			{
			}
		}

		// Token: 0x020003F0 RID: 1008
		private sealed class SubOvfInt32 : SubOvfInstruction
		{
			// Token: 0x060018C2 RID: 6338 RVA: 0x0004939C File Offset: 0x0004759C
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ScriptingRuntimeHelpers.Int32ToObject(checked((int)obj - (int)obj2)));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060018C3 RID: 6339 RVA: 0x00049392 File Offset: 0x00047592
			public SubOvfInt32()
			{
			}
		}

		// Token: 0x020003F1 RID: 1009
		private sealed class SubOvfInt64 : SubOvfInstruction
		{
			// Token: 0x060018C4 RID: 6340 RVA: 0x000493F0 File Offset: 0x000475F0
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : checked((long)obj - (long)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060018C5 RID: 6341 RVA: 0x00049392 File Offset: 0x00047592
			public SubOvfInt64()
			{
			}
		}

		// Token: 0x020003F2 RID: 1010
		private sealed class SubOvfUInt16 : SubOvfInstruction
		{
			// Token: 0x060018C6 RID: 6342 RVA: 0x00049444 File Offset: 0x00047644
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : checked((ushort)obj - (ushort)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060018C7 RID: 6343 RVA: 0x00049392 File Offset: 0x00047592
			public SubOvfUInt16()
			{
			}
		}

		// Token: 0x020003F3 RID: 1011
		private sealed class SubOvfUInt32 : SubOvfInstruction
		{
			// Token: 0x060018C8 RID: 6344 RVA: 0x00049498 File Offset: 0x00047698
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : checked((uint)obj - (uint)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060018C9 RID: 6345 RVA: 0x00049392 File Offset: 0x00047592
			public SubOvfUInt32()
			{
			}
		}

		// Token: 0x020003F4 RID: 1012
		private sealed class SubOvfUInt64 : SubOvfInstruction
		{
			// Token: 0x060018CA RID: 6346 RVA: 0x000494EC File Offset: 0x000476EC
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : checked((ulong)obj - (ulong)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060018CB RID: 6347 RVA: 0x00049392 File Offset: 0x00047592
			public SubOvfUInt64()
			{
			}
		}
	}
}
