﻿using System;
using System.Collections.Generic;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000323 RID: 803
	internal abstract class Instruction
	{
		// Token: 0x17000443 RID: 1091
		// (get) Token: 0x0600152B RID: 5419 RVA: 0x00002285 File Offset: 0x00000485
		public virtual int ConsumedStack
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x17000444 RID: 1092
		// (get) Token: 0x0600152C RID: 5420 RVA: 0x00002285 File Offset: 0x00000485
		public virtual int ProducedStack
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x17000445 RID: 1093
		// (get) Token: 0x0600152D RID: 5421 RVA: 0x00002285 File Offset: 0x00000485
		public virtual int ConsumedContinuations
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x17000446 RID: 1094
		// (get) Token: 0x0600152E RID: 5422 RVA: 0x00002285 File Offset: 0x00000485
		public virtual int ProducedContinuations
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x17000447 RID: 1095
		// (get) Token: 0x0600152F RID: 5423 RVA: 0x0003BCE8 File Offset: 0x00039EE8
		public int StackBalance
		{
			get
			{
				return this.ProducedStack - this.ConsumedStack;
			}
		}

		// Token: 0x17000448 RID: 1096
		// (get) Token: 0x06001530 RID: 5424 RVA: 0x0003BCF7 File Offset: 0x00039EF7
		public int ContinuationsBalance
		{
			get
			{
				return this.ProducedContinuations - this.ConsumedContinuations;
			}
		}

		// Token: 0x06001531 RID: 5425
		public abstract int Run(InterpretedFrame frame);

		// Token: 0x17000449 RID: 1097
		// (get) Token: 0x06001532 RID: 5426
		public abstract string InstructionName { get; }

		// Token: 0x06001533 RID: 5427 RVA: 0x0003BD06 File Offset: 0x00039F06
		public override string ToString()
		{
			return this.InstructionName + "()";
		}

		// Token: 0x06001534 RID: 5428 RVA: 0x0003BD18 File Offset: 0x00039F18
		public virtual string ToDebugString(int instructionIndex, object cookie, Func<int, int> labelIndexer, IReadOnlyList<object> objects)
		{
			return this.ToString();
		}

		// Token: 0x06001535 RID: 5429 RVA: 0x000053B1 File Offset: 0x000035B1
		public virtual object GetDebugCookie(LightCompiler compiler)
		{
			return null;
		}

		// Token: 0x06001536 RID: 5430 RVA: 0x0003BD20 File Offset: 0x00039F20
		protected static void NullCheck(object o)
		{
			if (o == null)
			{
				o.GetType();
			}
		}

		// Token: 0x06001537 RID: 5431 RVA: 0x00002310 File Offset: 0x00000510
		protected Instruction()
		{
		}

		// Token: 0x04000A88 RID: 2696
		public const int UnknownInstrIndex = 2147483647;
	}
}
