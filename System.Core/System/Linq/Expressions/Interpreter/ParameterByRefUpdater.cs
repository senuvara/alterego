﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200035D RID: 861
	internal sealed class ParameterByRefUpdater : ByRefUpdater
	{
		// Token: 0x060016C8 RID: 5832 RVA: 0x00043199 File Offset: 0x00041399
		public ParameterByRefUpdater(LocalVariable parameter, int argumentIndex) : base(argumentIndex)
		{
			this._parameter = parameter;
		}

		// Token: 0x060016C9 RID: 5833 RVA: 0x000431AC File Offset: 0x000413AC
		public override void Update(InterpretedFrame frame, object value)
		{
			if (this._parameter.InClosure)
			{
				frame.Closure[this._parameter.Index].Value = value;
				return;
			}
			if (this._parameter.IsBoxed)
			{
				((IStrongBox)frame.Data[this._parameter.Index]).Value = value;
				return;
			}
			frame.Data[this._parameter.Index] = value;
		}

		// Token: 0x04000B40 RID: 2880
		private readonly LocalVariable _parameter;
	}
}
