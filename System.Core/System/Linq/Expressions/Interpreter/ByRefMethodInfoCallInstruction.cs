﻿using System;
using System.Dynamic.Utils;
using System.Reflection;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002B1 RID: 689
	internal class ByRefMethodInfoCallInstruction : MethodInfoCallInstruction
	{
		// Token: 0x060013D8 RID: 5080 RVA: 0x00038DDC File Offset: 0x00036FDC
		internal ByRefMethodInfoCallInstruction(MethodInfo target, int argumentCount, ByRefUpdater[] byrefArgs) : base(target, argumentCount)
		{
			this._byrefArgs = byrefArgs;
		}

		// Token: 0x170003EF RID: 1007
		// (get) Token: 0x060013D9 RID: 5081 RVA: 0x00038C6D File Offset: 0x00036E6D
		public override int ProducedStack
		{
			get
			{
				if (!(this._target.ReturnType == typeof(void)))
				{
					return 1;
				}
				return 0;
			}
		}

		// Token: 0x060013DA RID: 5082 RVA: 0x00038DF0 File Offset: 0x00036FF0
		public sealed override int Run(InterpretedFrame frame)
		{
			int num = frame.StackIndex - this._argumentCount;
			object[] array = null;
			object obj = null;
			try
			{
				object obj2;
				if (this._target.IsStatic)
				{
					array = base.GetArgs(frame, num, 0);
					try
					{
						obj2 = this._target.Invoke(null, array);
						goto IL_8F;
					}
					catch (TargetInvocationException exception)
					{
						ExceptionHelpers.UnwrapAndRethrow(exception);
						throw ContractUtils.Unreachable;
					}
				}
				obj = frame.Data[num];
				Instruction.NullCheck(obj);
				array = base.GetArgs(frame, num, 1);
				LightLambda targetLambda;
				if (CallInstruction.TryGetLightLambdaTarget(obj, out targetLambda))
				{
					obj2 = base.InterpretLambdaInvoke(targetLambda, array);
				}
				else
				{
					try
					{
						obj2 = this._target.Invoke(obj, array);
					}
					catch (TargetInvocationException exception2)
					{
						ExceptionHelpers.UnwrapAndRethrow(exception2);
						throw ContractUtils.Unreachable;
					}
				}
				IL_8F:
				if (this._target.ReturnType != typeof(void))
				{
					frame.Data[num] = obj2;
					frame.StackIndex = num + 1;
				}
				else
				{
					frame.StackIndex = num;
				}
			}
			finally
			{
				if (array != null)
				{
					foreach (ByRefUpdater byRefUpdater in this._byrefArgs)
					{
						byRefUpdater.Update(frame, (byRefUpdater.ArgumentIndex == -1) ? obj : array[byRefUpdater.ArgumentIndex]);
					}
				}
			}
			return 1;
		}

		// Token: 0x040009F5 RID: 2549
		private readonly ByRefUpdater[] _byrefArgs;
	}
}
