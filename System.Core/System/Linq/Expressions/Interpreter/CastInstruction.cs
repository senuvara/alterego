﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000401 RID: 1025
	internal abstract class CastInstruction : Instruction
	{
		// Token: 0x170004DD RID: 1245
		// (get) Token: 0x060018F9 RID: 6393 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004DE RID: 1246
		// (get) Token: 0x060018FA RID: 6394 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004DF RID: 1247
		// (get) Token: 0x060018FB RID: 6395 RVA: 0x000498FB File Offset: 0x00047AFB
		public override string InstructionName
		{
			get
			{
				return "Cast";
			}
		}

		// Token: 0x060018FC RID: 6396 RVA: 0x00049904 File Offset: 0x00047B04
		public static Instruction Create(Type t)
		{
			switch (t.GetTypeCode())
			{
			case TypeCode.Boolean:
			{
				CastInstruction result;
				if ((result = CastInstruction.s_Boolean) == null)
				{
					result = (CastInstruction.s_Boolean = new CastInstruction.CastInstructionT<bool>());
				}
				return result;
			}
			case TypeCode.Char:
			{
				CastInstruction result2;
				if ((result2 = CastInstruction.s_Char) == null)
				{
					result2 = (CastInstruction.s_Char = new CastInstruction.CastInstructionT<char>());
				}
				return result2;
			}
			case TypeCode.SByte:
			{
				CastInstruction result3;
				if ((result3 = CastInstruction.s_SByte) == null)
				{
					result3 = (CastInstruction.s_SByte = new CastInstruction.CastInstructionT<sbyte>());
				}
				return result3;
			}
			case TypeCode.Byte:
			{
				CastInstruction result4;
				if ((result4 = CastInstruction.s_Byte) == null)
				{
					result4 = (CastInstruction.s_Byte = new CastInstruction.CastInstructionT<byte>());
				}
				return result4;
			}
			case TypeCode.Int16:
			{
				CastInstruction result5;
				if ((result5 = CastInstruction.s_Int16) == null)
				{
					result5 = (CastInstruction.s_Int16 = new CastInstruction.CastInstructionT<short>());
				}
				return result5;
			}
			case TypeCode.UInt16:
			{
				CastInstruction result6;
				if ((result6 = CastInstruction.s_UInt16) == null)
				{
					result6 = (CastInstruction.s_UInt16 = new CastInstruction.CastInstructionT<ushort>());
				}
				return result6;
			}
			case TypeCode.Int32:
			{
				CastInstruction result7;
				if ((result7 = CastInstruction.s_Int32) == null)
				{
					result7 = (CastInstruction.s_Int32 = new CastInstruction.CastInstructionT<int>());
				}
				return result7;
			}
			case TypeCode.UInt32:
			{
				CastInstruction result8;
				if ((result8 = CastInstruction.s_UInt32) == null)
				{
					result8 = (CastInstruction.s_UInt32 = new CastInstruction.CastInstructionT<uint>());
				}
				return result8;
			}
			case TypeCode.Int64:
			{
				CastInstruction result9;
				if ((result9 = CastInstruction.s_Int64) == null)
				{
					result9 = (CastInstruction.s_Int64 = new CastInstruction.CastInstructionT<long>());
				}
				return result9;
			}
			case TypeCode.UInt64:
			{
				CastInstruction result10;
				if ((result10 = CastInstruction.s_UInt64) == null)
				{
					result10 = (CastInstruction.s_UInt64 = new CastInstruction.CastInstructionT<ulong>());
				}
				return result10;
			}
			case TypeCode.Single:
			{
				CastInstruction result11;
				if ((result11 = CastInstruction.s_Single) == null)
				{
					result11 = (CastInstruction.s_Single = new CastInstruction.CastInstructionT<float>());
				}
				return result11;
			}
			case TypeCode.Double:
			{
				CastInstruction result12;
				if ((result12 = CastInstruction.s_Double) == null)
				{
					result12 = (CastInstruction.s_Double = new CastInstruction.CastInstructionT<double>());
				}
				return result12;
			}
			case TypeCode.Decimal:
			{
				CastInstruction result13;
				if ((result13 = CastInstruction.s_Decimal) == null)
				{
					result13 = (CastInstruction.s_Decimal = new CastInstruction.CastInstructionT<decimal>());
				}
				return result13;
			}
			case TypeCode.DateTime:
			{
				CastInstruction result14;
				if ((result14 = CastInstruction.s_DateTime) == null)
				{
					result14 = (CastInstruction.s_DateTime = new CastInstruction.CastInstructionT<DateTime>());
				}
				return result14;
			}
			case TypeCode.String:
			{
				CastInstruction result15;
				if ((result15 = CastInstruction.s_String) == null)
				{
					result15 = (CastInstruction.s_String = new CastInstruction.CastInstructionT<string>());
				}
				return result15;
			}
			}
			return CastInstruction.CastInstructionNoT.Create(t);
		}

		// Token: 0x060018FD RID: 6397 RVA: 0x00037C51 File Offset: 0x00035E51
		protected CastInstruction()
		{
		}

		// Token: 0x04000BE3 RID: 3043
		private static CastInstruction s_Boolean;

		// Token: 0x04000BE4 RID: 3044
		private static CastInstruction s_Byte;

		// Token: 0x04000BE5 RID: 3045
		private static CastInstruction s_Char;

		// Token: 0x04000BE6 RID: 3046
		private static CastInstruction s_DateTime;

		// Token: 0x04000BE7 RID: 3047
		private static CastInstruction s_Decimal;

		// Token: 0x04000BE8 RID: 3048
		private static CastInstruction s_Double;

		// Token: 0x04000BE9 RID: 3049
		private static CastInstruction s_Int16;

		// Token: 0x04000BEA RID: 3050
		private static CastInstruction s_Int32;

		// Token: 0x04000BEB RID: 3051
		private static CastInstruction s_Int64;

		// Token: 0x04000BEC RID: 3052
		private static CastInstruction s_SByte;

		// Token: 0x04000BED RID: 3053
		private static CastInstruction s_Single;

		// Token: 0x04000BEE RID: 3054
		private static CastInstruction s_String;

		// Token: 0x04000BEF RID: 3055
		private static CastInstruction s_UInt16;

		// Token: 0x04000BF0 RID: 3056
		private static CastInstruction s_UInt32;

		// Token: 0x04000BF1 RID: 3057
		private static CastInstruction s_UInt64;

		// Token: 0x02000402 RID: 1026
		private sealed class CastInstructionT<T> : CastInstruction
		{
			// Token: 0x060018FE RID: 6398 RVA: 0x00049AA8 File Offset: 0x00047CA8
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				frame.Push((T)((object)obj));
				return 1;
			}

			// Token: 0x060018FF RID: 6399 RVA: 0x00049ACE File Offset: 0x00047CCE
			public CastInstructionT()
			{
			}
		}

		// Token: 0x02000403 RID: 1027
		private abstract class CastInstructionNoT : CastInstruction
		{
			// Token: 0x06001900 RID: 6400 RVA: 0x00049AD6 File Offset: 0x00047CD6
			protected CastInstructionNoT(Type t)
			{
				this._t = t;
			}

			// Token: 0x06001901 RID: 6401 RVA: 0x00049AE5 File Offset: 0x00047CE5
			public new static CastInstruction Create(Type t)
			{
				if (t.IsValueType && !t.IsNullableType())
				{
					return new CastInstruction.CastInstructionNoT.Value(t);
				}
				return new CastInstruction.CastInstructionNoT.Ref(t);
			}

			// Token: 0x06001902 RID: 6402 RVA: 0x00049B04 File Offset: 0x00047D04
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				if (obj != null)
				{
					Type type = obj.GetType();
					if (!type.HasReferenceConversionTo(this._t) && !type.HasIdentityPrimitiveOrNullableConversionTo(this._t))
					{
						throw new InvalidCastException();
					}
					if (!this._t.IsAssignableFrom(type))
					{
						throw new InvalidCastException();
					}
					frame.Push(obj);
				}
				else
				{
					this.ConvertNull(frame);
				}
				return 1;
			}

			// Token: 0x06001903 RID: 6403
			protected abstract void ConvertNull(InterpretedFrame frame);

			// Token: 0x04000BF2 RID: 3058
			private readonly Type _t;

			// Token: 0x02000404 RID: 1028
			private sealed class Ref : CastInstruction.CastInstructionNoT
			{
				// Token: 0x06001904 RID: 6404 RVA: 0x00049B69 File Offset: 0x00047D69
				public Ref(Type t) : base(t)
				{
				}

				// Token: 0x06001905 RID: 6405 RVA: 0x00049B72 File Offset: 0x00047D72
				protected override void ConvertNull(InterpretedFrame frame)
				{
					frame.Push(null);
				}
			}

			// Token: 0x02000405 RID: 1029
			private sealed class Value : CastInstruction.CastInstructionNoT
			{
				// Token: 0x06001906 RID: 6406 RVA: 0x00049B69 File Offset: 0x00047D69
				public Value(Type t) : base(t)
				{
				}

				// Token: 0x06001907 RID: 6407 RVA: 0x00049B7B File Offset: 0x00047D7B
				protected override void ConvertNull(InterpretedFrame frame)
				{
					throw new NullReferenceException();
				}
			}
		}
	}
}
