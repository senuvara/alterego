﻿using System;
using System.Dynamic.Utils;
using System.Reflection;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020003F9 RID: 1017
	internal abstract class NullableMethodCallInstruction : Instruction
	{
		// Token: 0x170004D8 RID: 1240
		// (get) Token: 0x060018E3 RID: 6371 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004D9 RID: 1241
		// (get) Token: 0x060018E4 RID: 6372 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004DA RID: 1242
		// (get) Token: 0x060018E5 RID: 6373 RVA: 0x000496A9 File Offset: 0x000478A9
		public override string InstructionName
		{
			get
			{
				return "NullableMethod";
			}
		}

		// Token: 0x060018E6 RID: 6374 RVA: 0x00037C51 File Offset: 0x00035E51
		private NullableMethodCallInstruction()
		{
		}

		// Token: 0x060018E7 RID: 6375 RVA: 0x000496B0 File Offset: 0x000478B0
		public static Instruction Create(string method, int argCount, MethodInfo mi)
		{
			if (method == "get_HasValue")
			{
				NullableMethodCallInstruction result;
				if ((result = NullableMethodCallInstruction.s_hasValue) == null)
				{
					result = (NullableMethodCallInstruction.s_hasValue = new NullableMethodCallInstruction.HasValue());
				}
				return result;
			}
			if (method == "get_Value")
			{
				NullableMethodCallInstruction result2;
				if ((result2 = NullableMethodCallInstruction.s_value) == null)
				{
					result2 = (NullableMethodCallInstruction.s_value = new NullableMethodCallInstruction.GetValue());
				}
				return result2;
			}
			if (method == "Equals")
			{
				NullableMethodCallInstruction result3;
				if ((result3 = NullableMethodCallInstruction.s_equals) == null)
				{
					result3 = (NullableMethodCallInstruction.s_equals = new NullableMethodCallInstruction.EqualsClass());
				}
				return result3;
			}
			if (method == "GetHashCode")
			{
				NullableMethodCallInstruction result4;
				if ((result4 = NullableMethodCallInstruction.s_getHashCode) == null)
				{
					result4 = (NullableMethodCallInstruction.s_getHashCode = new NullableMethodCallInstruction.GetHashCodeClass());
				}
				return result4;
			}
			if (!(method == "GetValueOrDefault"))
			{
				if (!(method == "ToString"))
				{
					throw ContractUtils.Unreachable;
				}
				NullableMethodCallInstruction result5;
				if ((result5 = NullableMethodCallInstruction.s_toString) == null)
				{
					result5 = (NullableMethodCallInstruction.s_toString = new NullableMethodCallInstruction.ToStringClass());
				}
				return result5;
			}
			else
			{
				if (argCount == 0)
				{
					return new NullableMethodCallInstruction.GetValueOrDefault(mi);
				}
				NullableMethodCallInstruction result6;
				if ((result6 = NullableMethodCallInstruction.s_getValueOrDefault1) == null)
				{
					result6 = (NullableMethodCallInstruction.s_getValueOrDefault1 = new NullableMethodCallInstruction.GetValueOrDefault1());
				}
				return result6;
			}
		}

		// Token: 0x060018E8 RID: 6376 RVA: 0x0004979D File Offset: 0x0004799D
		public static Instruction CreateGetValue()
		{
			NullableMethodCallInstruction result;
			if ((result = NullableMethodCallInstruction.s_value) == null)
			{
				result = (NullableMethodCallInstruction.s_value = new NullableMethodCallInstruction.GetValue());
			}
			return result;
		}

		// Token: 0x04000BDC RID: 3036
		private static NullableMethodCallInstruction s_hasValue;

		// Token: 0x04000BDD RID: 3037
		private static NullableMethodCallInstruction s_value;

		// Token: 0x04000BDE RID: 3038
		private static NullableMethodCallInstruction s_equals;

		// Token: 0x04000BDF RID: 3039
		private static NullableMethodCallInstruction s_getHashCode;

		// Token: 0x04000BE0 RID: 3040
		private static NullableMethodCallInstruction s_getValueOrDefault1;

		// Token: 0x04000BE1 RID: 3041
		private static NullableMethodCallInstruction s_toString;

		// Token: 0x020003FA RID: 1018
		private sealed class HasValue : NullableMethodCallInstruction
		{
			// Token: 0x060018E9 RID: 6377 RVA: 0x000497B4 File Offset: 0x000479B4
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				frame.Push(obj != null);
				return 1;
			}

			// Token: 0x060018EA RID: 6378 RVA: 0x000497D3 File Offset: 0x000479D3
			public HasValue()
			{
			}
		}

		// Token: 0x020003FB RID: 1019
		private sealed class GetValue : NullableMethodCallInstruction
		{
			// Token: 0x060018EB RID: 6379 RVA: 0x000497DC File Offset: 0x000479DC
			public override int Run(InterpretedFrame frame)
			{
				if (frame.Peek() == null)
				{
					return ((int?)null).Value;
				}
				return 1;
			}

			// Token: 0x060018EC RID: 6380 RVA: 0x000497D3 File Offset: 0x000479D3
			public GetValue()
			{
			}
		}

		// Token: 0x020003FC RID: 1020
		private sealed class GetValueOrDefault : NullableMethodCallInstruction
		{
			// Token: 0x060018ED RID: 6381 RVA: 0x00049801 File Offset: 0x00047A01
			public GetValueOrDefault(MethodInfo mi)
			{
				this.defaultValueType = mi.ReturnType;
			}

			// Token: 0x060018EE RID: 6382 RVA: 0x00049815 File Offset: 0x00047A15
			public override int Run(InterpretedFrame frame)
			{
				if (frame.Peek() == null)
				{
					frame.Pop();
					frame.Push(Activator.CreateInstance(this.defaultValueType));
				}
				return 1;
			}

			// Token: 0x04000BE2 RID: 3042
			private readonly Type defaultValueType;
		}

		// Token: 0x020003FD RID: 1021
		private sealed class GetValueOrDefault1 : NullableMethodCallInstruction
		{
			// Token: 0x170004DB RID: 1243
			// (get) Token: 0x060018EF RID: 6383 RVA: 0x0002EE24 File Offset: 0x0002D024
			public override int ConsumedStack
			{
				get
				{
					return 2;
				}
			}

			// Token: 0x060018F0 RID: 6384 RVA: 0x00049838 File Offset: 0x00047A38
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				frame.Push(obj2 ?? obj);
				return 1;
			}

			// Token: 0x060018F1 RID: 6385 RVA: 0x000497D3 File Offset: 0x000479D3
			public GetValueOrDefault1()
			{
			}
		}

		// Token: 0x020003FE RID: 1022
		private sealed class EqualsClass : NullableMethodCallInstruction
		{
			// Token: 0x170004DC RID: 1244
			// (get) Token: 0x060018F2 RID: 6386 RVA: 0x0002EE24 File Offset: 0x0002D024
			public override int ConsumedStack
			{
				get
				{
					return 2;
				}
			}

			// Token: 0x060018F3 RID: 6387 RVA: 0x00049860 File Offset: 0x00047A60
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					frame.Push(obj == null);
				}
				else if (obj == null)
				{
					frame.Push(Utils.BoxedFalse);
				}
				else
				{
					frame.Push(obj2.Equals(obj));
				}
				return 1;
			}

			// Token: 0x060018F4 RID: 6388 RVA: 0x000497D3 File Offset: 0x000479D3
			public EqualsClass()
			{
			}
		}

		// Token: 0x020003FF RID: 1023
		private sealed class ToStringClass : NullableMethodCallInstruction
		{
			// Token: 0x060018F5 RID: 6389 RVA: 0x000498A8 File Offset: 0x00047AA8
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				frame.Push((obj == null) ? "" : obj.ToString());
				return 1;
			}

			// Token: 0x060018F6 RID: 6390 RVA: 0x000497D3 File Offset: 0x000479D3
			public ToStringClass()
			{
			}
		}

		// Token: 0x02000400 RID: 1024
		private sealed class GetHashCodeClass : NullableMethodCallInstruction
		{
			// Token: 0x060018F7 RID: 6391 RVA: 0x000498D4 File Offset: 0x00047AD4
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				frame.Push((obj != null) ? obj.GetHashCode() : 0);
				return 1;
			}

			// Token: 0x060018F8 RID: 6392 RVA: 0x000497D3 File Offset: 0x000479D3
			public GetHashCodeClass()
			{
			}
		}
	}
}
