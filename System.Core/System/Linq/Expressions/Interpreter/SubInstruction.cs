﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020003E5 RID: 997
	internal abstract class SubInstruction : Instruction
	{
		// Token: 0x170004C6 RID: 1222
		// (get) Token: 0x060018A6 RID: 6310 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x170004C7 RID: 1223
		// (get) Token: 0x060018A7 RID: 6311 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004C8 RID: 1224
		// (get) Token: 0x060018A8 RID: 6312 RVA: 0x00048ED6 File Offset: 0x000470D6
		public override string InstructionName
		{
			get
			{
				return "Sub";
			}
		}

		// Token: 0x060018A9 RID: 6313 RVA: 0x00037C51 File Offset: 0x00035E51
		private SubInstruction()
		{
		}

		// Token: 0x060018AA RID: 6314 RVA: 0x00048EE0 File Offset: 0x000470E0
		public static Instruction Create(Type type)
		{
			switch (type.GetNonNullableType().GetTypeCode())
			{
			case TypeCode.Int16:
			{
				Instruction result;
				if ((result = SubInstruction.s_Int16) == null)
				{
					result = (SubInstruction.s_Int16 = new SubInstruction.SubInt16());
				}
				return result;
			}
			case TypeCode.UInt16:
			{
				Instruction result2;
				if ((result2 = SubInstruction.s_UInt16) == null)
				{
					result2 = (SubInstruction.s_UInt16 = new SubInstruction.SubUInt16());
				}
				return result2;
			}
			case TypeCode.Int32:
			{
				Instruction result3;
				if ((result3 = SubInstruction.s_Int32) == null)
				{
					result3 = (SubInstruction.s_Int32 = new SubInstruction.SubInt32());
				}
				return result3;
			}
			case TypeCode.UInt32:
			{
				Instruction result4;
				if ((result4 = SubInstruction.s_UInt32) == null)
				{
					result4 = (SubInstruction.s_UInt32 = new SubInstruction.SubUInt32());
				}
				return result4;
			}
			case TypeCode.Int64:
			{
				Instruction result5;
				if ((result5 = SubInstruction.s_Int64) == null)
				{
					result5 = (SubInstruction.s_Int64 = new SubInstruction.SubInt64());
				}
				return result5;
			}
			case TypeCode.UInt64:
			{
				Instruction result6;
				if ((result6 = SubInstruction.s_UInt64) == null)
				{
					result6 = (SubInstruction.s_UInt64 = new SubInstruction.SubUInt64());
				}
				return result6;
			}
			case TypeCode.Single:
			{
				Instruction result7;
				if ((result7 = SubInstruction.s_Single) == null)
				{
					result7 = (SubInstruction.s_Single = new SubInstruction.SubSingle());
				}
				return result7;
			}
			case TypeCode.Double:
			{
				Instruction result8;
				if ((result8 = SubInstruction.s_Double) == null)
				{
					result8 = (SubInstruction.s_Double = new SubInstruction.SubDouble());
				}
				return result8;
			}
			default:
				throw ContractUtils.Unreachable;
			}
		}

		// Token: 0x04000BCA RID: 3018
		private static Instruction s_Int16;

		// Token: 0x04000BCB RID: 3019
		private static Instruction s_Int32;

		// Token: 0x04000BCC RID: 3020
		private static Instruction s_Int64;

		// Token: 0x04000BCD RID: 3021
		private static Instruction s_UInt16;

		// Token: 0x04000BCE RID: 3022
		private static Instruction s_UInt32;

		// Token: 0x04000BCF RID: 3023
		private static Instruction s_UInt64;

		// Token: 0x04000BD0 RID: 3024
		private static Instruction s_Single;

		// Token: 0x04000BD1 RID: 3025
		private static Instruction s_Double;

		// Token: 0x020003E6 RID: 998
		private sealed class SubInt16 : SubInstruction
		{
			// Token: 0x060018AB RID: 6315 RVA: 0x00048FD4 File Offset: 0x000471D4
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((short)obj - (short)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060018AC RID: 6316 RVA: 0x00049026 File Offset: 0x00047226
			public SubInt16()
			{
			}
		}

		// Token: 0x020003E7 RID: 999
		private sealed class SubInt32 : SubInstruction
		{
			// Token: 0x060018AD RID: 6317 RVA: 0x00049030 File Offset: 0x00047230
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ScriptingRuntimeHelpers.Int32ToObject((int)obj - (int)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060018AE RID: 6318 RVA: 0x00049026 File Offset: 0x00047226
			public SubInt32()
			{
			}
		}

		// Token: 0x020003E8 RID: 1000
		private sealed class SubInt64 : SubInstruction
		{
			// Token: 0x060018AF RID: 6319 RVA: 0x00049084 File Offset: 0x00047284
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((long)obj - (long)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060018B0 RID: 6320 RVA: 0x00049026 File Offset: 0x00047226
			public SubInt64()
			{
			}
		}

		// Token: 0x020003E9 RID: 1001
		private sealed class SubUInt16 : SubInstruction
		{
			// Token: 0x060018B1 RID: 6321 RVA: 0x000490D8 File Offset: 0x000472D8
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((ushort)obj - (ushort)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060018B2 RID: 6322 RVA: 0x00049026 File Offset: 0x00047226
			public SubUInt16()
			{
			}
		}

		// Token: 0x020003EA RID: 1002
		private sealed class SubUInt32 : SubInstruction
		{
			// Token: 0x060018B3 RID: 6323 RVA: 0x0004912C File Offset: 0x0004732C
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((uint)obj - (uint)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060018B4 RID: 6324 RVA: 0x00049026 File Offset: 0x00047226
			public SubUInt32()
			{
			}
		}

		// Token: 0x020003EB RID: 1003
		private sealed class SubUInt64 : SubInstruction
		{
			// Token: 0x060018B5 RID: 6325 RVA: 0x00049180 File Offset: 0x00047380
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((ulong)obj - (ulong)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060018B6 RID: 6326 RVA: 0x00049026 File Offset: 0x00047226
			public SubUInt64()
			{
			}
		}

		// Token: 0x020003EC RID: 1004
		private sealed class SubSingle : SubInstruction
		{
			// Token: 0x060018B7 RID: 6327 RVA: 0x000491D4 File Offset: 0x000473D4
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((float)obj - (float)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060018B8 RID: 6328 RVA: 0x00049026 File Offset: 0x00047226
			public SubSingle()
			{
			}
		}

		// Token: 0x020003ED RID: 1005
		private sealed class SubDouble : SubInstruction
		{
			// Token: 0x060018B9 RID: 6329 RVA: 0x00049228 File Offset: 0x00047428
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ((double)obj - (double)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060018BA RID: 6330 RVA: 0x00049026 File Offset: 0x00047226
			public SubDouble()
			{
			}
		}
	}
}
