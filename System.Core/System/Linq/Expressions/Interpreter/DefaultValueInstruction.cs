﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002CF RID: 719
	internal sealed class DefaultValueInstruction : Instruction
	{
		// Token: 0x06001465 RID: 5221 RVA: 0x00039CDC File Offset: 0x00037EDC
		internal DefaultValueInstruction(Type type)
		{
			this._type = type;
		}

		// Token: 0x17000426 RID: 1062
		// (get) Token: 0x06001466 RID: 5222 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000427 RID: 1063
		// (get) Token: 0x06001467 RID: 5223 RVA: 0x00039CEB File Offset: 0x00037EEB
		public override string InstructionName
		{
			get
			{
				return "DefaultValue";
			}
		}

		// Token: 0x06001468 RID: 5224 RVA: 0x00039CF2 File Offset: 0x00037EF2
		public override int Run(InterpretedFrame frame)
		{
			frame.Push(Activator.CreateInstance(this._type));
			return 1;
		}

		// Token: 0x06001469 RID: 5225 RVA: 0x00039D06 File Offset: 0x00037F06
		public override string ToString()
		{
			return "DefaultValue " + this._type;
		}

		// Token: 0x04000A26 RID: 2598
		private readonly Type _type;
	}
}
