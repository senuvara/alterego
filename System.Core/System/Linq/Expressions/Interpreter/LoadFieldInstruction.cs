﻿using System;
using System.Reflection;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002FF RID: 767
	internal sealed class LoadFieldInstruction : FieldInstruction
	{
		// Token: 0x060014D3 RID: 5331 RVA: 0x0003ADC8 File Offset: 0x00038FC8
		public LoadFieldInstruction(FieldInfo field) : base(field)
		{
		}

		// Token: 0x17000433 RID: 1075
		// (get) Token: 0x060014D4 RID: 5332 RVA: 0x0003ADED File Offset: 0x00038FED
		public override string InstructionName
		{
			get
			{
				return "LoadField";
			}
		}

		// Token: 0x17000434 RID: 1076
		// (get) Token: 0x060014D5 RID: 5333 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000435 RID: 1077
		// (get) Token: 0x060014D6 RID: 5334 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x060014D7 RID: 5335 RVA: 0x0003ADF4 File Offset: 0x00038FF4
		public override int Run(InterpretedFrame frame)
		{
			object obj = frame.Pop();
			Instruction.NullCheck(obj);
			frame.Push(this._field.GetValue(obj));
			return 1;
		}
	}
}
