﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002C5 RID: 709
	internal sealed class StringSwitchInstruction : Instruction
	{
		// Token: 0x0600144C RID: 5196 RVA: 0x000399D6 File Offset: 0x00037BD6
		internal StringSwitchInstruction(Dictionary<string, int> cases, StrongBox<int> nullCase)
		{
			this._cases = cases;
			this._nullCase = nullCase;
		}

		// Token: 0x17000421 RID: 1057
		// (get) Token: 0x0600144D RID: 5197 RVA: 0x000399EC File Offset: 0x00037BEC
		public override string InstructionName
		{
			get
			{
				return "StringSwitch";
			}
		}

		// Token: 0x17000422 RID: 1058
		// (get) Token: 0x0600144E RID: 5198 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x0600144F RID: 5199 RVA: 0x000399F4 File Offset: 0x00037BF4
		public override int Run(InterpretedFrame frame)
		{
			object obj = frame.Pop();
			if (obj == null)
			{
				return this._nullCase.Value;
			}
			int result;
			if (!this._cases.TryGetValue((string)obj, out result))
			{
				return 1;
			}
			return result;
		}

		// Token: 0x04000A1C RID: 2588
		private readonly Dictionary<string, int> _cases;

		// Token: 0x04000A1D RID: 2589
		private readonly StrongBox<int> _nullCase;
	}
}
