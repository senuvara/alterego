﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000302 RID: 770
	internal abstract class GreaterThanInstruction : Instruction
	{
		// Token: 0x1700043A RID: 1082
		// (get) Token: 0x060014E0 RID: 5344 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x1700043B RID: 1083
		// (get) Token: 0x060014E1 RID: 5345 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x1700043C RID: 1084
		// (get) Token: 0x060014E2 RID: 5346 RVA: 0x0003AE82 File Offset: 0x00039082
		public override string InstructionName
		{
			get
			{
				return "GreaterThan";
			}
		}

		// Token: 0x060014E3 RID: 5347 RVA: 0x0003AE89 File Offset: 0x00039089
		private GreaterThanInstruction(object nullValue)
		{
			this._nullValue = nullValue;
		}

		// Token: 0x060014E4 RID: 5348 RVA: 0x0003AE98 File Offset: 0x00039098
		public static Instruction Create(Type type, bool liftedToNull = false)
		{
			if (liftedToNull)
			{
				switch (type.GetNonNullableType().GetTypeCode())
				{
				case TypeCode.Char:
				{
					Instruction result;
					if ((result = GreaterThanInstruction.s_liftedToNullChar) == null)
					{
						result = (GreaterThanInstruction.s_liftedToNullChar = new GreaterThanInstruction.GreaterThanChar(null));
					}
					return result;
				}
				case TypeCode.SByte:
				{
					Instruction result2;
					if ((result2 = GreaterThanInstruction.s_liftedToNullSByte) == null)
					{
						result2 = (GreaterThanInstruction.s_liftedToNullSByte = new GreaterThanInstruction.GreaterThanSByte(null));
					}
					return result2;
				}
				case TypeCode.Byte:
				{
					Instruction result3;
					if ((result3 = GreaterThanInstruction.s_liftedToNullByte) == null)
					{
						result3 = (GreaterThanInstruction.s_liftedToNullByte = new GreaterThanInstruction.GreaterThanByte(null));
					}
					return result3;
				}
				case TypeCode.Int16:
				{
					Instruction result4;
					if ((result4 = GreaterThanInstruction.s_liftedToNullInt16) == null)
					{
						result4 = (GreaterThanInstruction.s_liftedToNullInt16 = new GreaterThanInstruction.GreaterThanInt16(null));
					}
					return result4;
				}
				case TypeCode.UInt16:
				{
					Instruction result5;
					if ((result5 = GreaterThanInstruction.s_liftedToNullUInt16) == null)
					{
						result5 = (GreaterThanInstruction.s_liftedToNullUInt16 = new GreaterThanInstruction.GreaterThanUInt16(null));
					}
					return result5;
				}
				case TypeCode.Int32:
				{
					Instruction result6;
					if ((result6 = GreaterThanInstruction.s_liftedToNullInt32) == null)
					{
						result6 = (GreaterThanInstruction.s_liftedToNullInt32 = new GreaterThanInstruction.GreaterThanInt32(null));
					}
					return result6;
				}
				case TypeCode.UInt32:
				{
					Instruction result7;
					if ((result7 = GreaterThanInstruction.s_liftedToNullUInt32) == null)
					{
						result7 = (GreaterThanInstruction.s_liftedToNullUInt32 = new GreaterThanInstruction.GreaterThanUInt32(null));
					}
					return result7;
				}
				case TypeCode.Int64:
				{
					Instruction result8;
					if ((result8 = GreaterThanInstruction.s_liftedToNullInt64) == null)
					{
						result8 = (GreaterThanInstruction.s_liftedToNullInt64 = new GreaterThanInstruction.GreaterThanInt64(null));
					}
					return result8;
				}
				case TypeCode.UInt64:
				{
					Instruction result9;
					if ((result9 = GreaterThanInstruction.s_liftedToNullUInt64) == null)
					{
						result9 = (GreaterThanInstruction.s_liftedToNullUInt64 = new GreaterThanInstruction.GreaterThanUInt64(null));
					}
					return result9;
				}
				case TypeCode.Single:
				{
					Instruction result10;
					if ((result10 = GreaterThanInstruction.s_liftedToNullSingle) == null)
					{
						result10 = (GreaterThanInstruction.s_liftedToNullSingle = new GreaterThanInstruction.GreaterThanSingle(null));
					}
					return result10;
				}
				case TypeCode.Double:
				{
					Instruction result11;
					if ((result11 = GreaterThanInstruction.s_liftedToNullDouble) == null)
					{
						result11 = (GreaterThanInstruction.s_liftedToNullDouble = new GreaterThanInstruction.GreaterThanDouble(null));
					}
					return result11;
				}
				default:
					throw ContractUtils.Unreachable;
				}
			}
			else
			{
				switch (type.GetNonNullableType().GetTypeCode())
				{
				case TypeCode.Char:
				{
					Instruction result12;
					if ((result12 = GreaterThanInstruction.s_Char) == null)
					{
						result12 = (GreaterThanInstruction.s_Char = new GreaterThanInstruction.GreaterThanChar(Utils.BoxedFalse));
					}
					return result12;
				}
				case TypeCode.SByte:
				{
					Instruction result13;
					if ((result13 = GreaterThanInstruction.s_SByte) == null)
					{
						result13 = (GreaterThanInstruction.s_SByte = new GreaterThanInstruction.GreaterThanSByte(Utils.BoxedFalse));
					}
					return result13;
				}
				case TypeCode.Byte:
				{
					Instruction result14;
					if ((result14 = GreaterThanInstruction.s_Byte) == null)
					{
						result14 = (GreaterThanInstruction.s_Byte = new GreaterThanInstruction.GreaterThanByte(Utils.BoxedFalse));
					}
					return result14;
				}
				case TypeCode.Int16:
				{
					Instruction result15;
					if ((result15 = GreaterThanInstruction.s_Int16) == null)
					{
						result15 = (GreaterThanInstruction.s_Int16 = new GreaterThanInstruction.GreaterThanInt16(Utils.BoxedFalse));
					}
					return result15;
				}
				case TypeCode.UInt16:
				{
					Instruction result16;
					if ((result16 = GreaterThanInstruction.s_UInt16) == null)
					{
						result16 = (GreaterThanInstruction.s_UInt16 = new GreaterThanInstruction.GreaterThanUInt16(Utils.BoxedFalse));
					}
					return result16;
				}
				case TypeCode.Int32:
				{
					Instruction result17;
					if ((result17 = GreaterThanInstruction.s_Int32) == null)
					{
						result17 = (GreaterThanInstruction.s_Int32 = new GreaterThanInstruction.GreaterThanInt32(Utils.BoxedFalse));
					}
					return result17;
				}
				case TypeCode.UInt32:
				{
					Instruction result18;
					if ((result18 = GreaterThanInstruction.s_UInt32) == null)
					{
						result18 = (GreaterThanInstruction.s_UInt32 = new GreaterThanInstruction.GreaterThanUInt32(Utils.BoxedFalse));
					}
					return result18;
				}
				case TypeCode.Int64:
				{
					Instruction result19;
					if ((result19 = GreaterThanInstruction.s_Int64) == null)
					{
						result19 = (GreaterThanInstruction.s_Int64 = new GreaterThanInstruction.GreaterThanInt64(Utils.BoxedFalse));
					}
					return result19;
				}
				case TypeCode.UInt64:
				{
					Instruction result20;
					if ((result20 = GreaterThanInstruction.s_UInt64) == null)
					{
						result20 = (GreaterThanInstruction.s_UInt64 = new GreaterThanInstruction.GreaterThanUInt64(Utils.BoxedFalse));
					}
					return result20;
				}
				case TypeCode.Single:
				{
					Instruction result21;
					if ((result21 = GreaterThanInstruction.s_Single) == null)
					{
						result21 = (GreaterThanInstruction.s_Single = new GreaterThanInstruction.GreaterThanSingle(Utils.BoxedFalse));
					}
					return result21;
				}
				case TypeCode.Double:
				{
					Instruction result22;
					if ((result22 = GreaterThanInstruction.s_Double) == null)
					{
						result22 = (GreaterThanInstruction.s_Double = new GreaterThanInstruction.GreaterThanDouble(Utils.BoxedFalse));
					}
					return result22;
				}
				default:
					throw ContractUtils.Unreachable;
				}
			}
		}

		// Token: 0x04000A52 RID: 2642
		private readonly object _nullValue;

		// Token: 0x04000A53 RID: 2643
		private static Instruction s_SByte;

		// Token: 0x04000A54 RID: 2644
		private static Instruction s_Int16;

		// Token: 0x04000A55 RID: 2645
		private static Instruction s_Char;

		// Token: 0x04000A56 RID: 2646
		private static Instruction s_Int32;

		// Token: 0x04000A57 RID: 2647
		private static Instruction s_Int64;

		// Token: 0x04000A58 RID: 2648
		private static Instruction s_Byte;

		// Token: 0x04000A59 RID: 2649
		private static Instruction s_UInt16;

		// Token: 0x04000A5A RID: 2650
		private static Instruction s_UInt32;

		// Token: 0x04000A5B RID: 2651
		private static Instruction s_UInt64;

		// Token: 0x04000A5C RID: 2652
		private static Instruction s_Single;

		// Token: 0x04000A5D RID: 2653
		private static Instruction s_Double;

		// Token: 0x04000A5E RID: 2654
		private static Instruction s_liftedToNullSByte;

		// Token: 0x04000A5F RID: 2655
		private static Instruction s_liftedToNullInt16;

		// Token: 0x04000A60 RID: 2656
		private static Instruction s_liftedToNullChar;

		// Token: 0x04000A61 RID: 2657
		private static Instruction s_liftedToNullInt32;

		// Token: 0x04000A62 RID: 2658
		private static Instruction s_liftedToNullInt64;

		// Token: 0x04000A63 RID: 2659
		private static Instruction s_liftedToNullByte;

		// Token: 0x04000A64 RID: 2660
		private static Instruction s_liftedToNullUInt16;

		// Token: 0x04000A65 RID: 2661
		private static Instruction s_liftedToNullUInt32;

		// Token: 0x04000A66 RID: 2662
		private static Instruction s_liftedToNullUInt64;

		// Token: 0x04000A67 RID: 2663
		private static Instruction s_liftedToNullSingle;

		// Token: 0x04000A68 RID: 2664
		private static Instruction s_liftedToNullDouble;

		// Token: 0x02000303 RID: 771
		private sealed class GreaterThanSByte : GreaterThanInstruction
		{
			// Token: 0x060014E5 RID: 5349 RVA: 0x0003B150 File Offset: 0x00039350
			public GreaterThanSByte(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x060014E6 RID: 5350 RVA: 0x0003B15C File Offset: 0x0003935C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((sbyte)obj2 > (sbyte)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000304 RID: 772
		private sealed class GreaterThanInt16 : GreaterThanInstruction
		{
			// Token: 0x060014E7 RID: 5351 RVA: 0x0003B150 File Offset: 0x00039350
			public GreaterThanInt16(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x060014E8 RID: 5352 RVA: 0x0003B1A0 File Offset: 0x000393A0
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((short)obj2 > (short)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000305 RID: 773
		private sealed class GreaterThanChar : GreaterThanInstruction
		{
			// Token: 0x060014E9 RID: 5353 RVA: 0x0003B150 File Offset: 0x00039350
			public GreaterThanChar(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x060014EA RID: 5354 RVA: 0x0003B1E4 File Offset: 0x000393E4
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((char)obj2 > (char)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000306 RID: 774
		private sealed class GreaterThanInt32 : GreaterThanInstruction
		{
			// Token: 0x060014EB RID: 5355 RVA: 0x0003B150 File Offset: 0x00039350
			public GreaterThanInt32(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x060014EC RID: 5356 RVA: 0x0003B228 File Offset: 0x00039428
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((int)obj2 > (int)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000307 RID: 775
		private sealed class GreaterThanInt64 : GreaterThanInstruction
		{
			// Token: 0x060014ED RID: 5357 RVA: 0x0003B150 File Offset: 0x00039350
			public GreaterThanInt64(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x060014EE RID: 5358 RVA: 0x0003B26C File Offset: 0x0003946C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((long)obj2 > (long)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000308 RID: 776
		private sealed class GreaterThanByte : GreaterThanInstruction
		{
			// Token: 0x060014EF RID: 5359 RVA: 0x0003B150 File Offset: 0x00039350
			public GreaterThanByte(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x060014F0 RID: 5360 RVA: 0x0003B2B0 File Offset: 0x000394B0
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((byte)obj2 > (byte)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000309 RID: 777
		private sealed class GreaterThanUInt16 : GreaterThanInstruction
		{
			// Token: 0x060014F1 RID: 5361 RVA: 0x0003B150 File Offset: 0x00039350
			public GreaterThanUInt16(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x060014F2 RID: 5362 RVA: 0x0003B2F4 File Offset: 0x000394F4
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((ushort)obj2 > (ushort)obj);
				}
				return 1;
			}
		}

		// Token: 0x0200030A RID: 778
		private sealed class GreaterThanUInt32 : GreaterThanInstruction
		{
			// Token: 0x060014F3 RID: 5363 RVA: 0x0003B150 File Offset: 0x00039350
			public GreaterThanUInt32(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x060014F4 RID: 5364 RVA: 0x0003B338 File Offset: 0x00039538
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((uint)obj2 > (uint)obj);
				}
				return 1;
			}
		}

		// Token: 0x0200030B RID: 779
		private sealed class GreaterThanUInt64 : GreaterThanInstruction
		{
			// Token: 0x060014F5 RID: 5365 RVA: 0x0003B150 File Offset: 0x00039350
			public GreaterThanUInt64(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x060014F6 RID: 5366 RVA: 0x0003B37C File Offset: 0x0003957C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((ulong)obj2 > (ulong)obj);
				}
				return 1;
			}
		}

		// Token: 0x0200030C RID: 780
		private sealed class GreaterThanSingle : GreaterThanInstruction
		{
			// Token: 0x060014F7 RID: 5367 RVA: 0x0003B150 File Offset: 0x00039350
			public GreaterThanSingle(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x060014F8 RID: 5368 RVA: 0x0003B3C0 File Offset: 0x000395C0
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((float)obj2 > (float)obj);
				}
				return 1;
			}
		}

		// Token: 0x0200030D RID: 781
		private sealed class GreaterThanDouble : GreaterThanInstruction
		{
			// Token: 0x060014F9 RID: 5369 RVA: 0x0003B150 File Offset: 0x00039350
			public GreaterThanDouble(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x060014FA RID: 5370 RVA: 0x0003B404 File Offset: 0x00039604
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((double)obj2 > (double)obj);
				}
				return 1;
			}
		}
	}
}
