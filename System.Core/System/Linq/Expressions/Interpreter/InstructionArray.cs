﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic.Utils;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000324 RID: 804
	[DebuggerTypeProxy(typeof(InstructionArray.DebugView))]
	internal struct InstructionArray
	{
		// Token: 0x06001538 RID: 5432 RVA: 0x0003BD2C File Offset: 0x00039F2C
		internal InstructionArray(int maxStackDepth, int maxContinuationDepth, Instruction[] instructions, object[] objects, RuntimeLabel[] labels, List<KeyValuePair<int, object>> debugCookies)
		{
			this.MaxStackDepth = maxStackDepth;
			this.MaxContinuationDepth = maxContinuationDepth;
			this.Instructions = instructions;
			this.DebugCookies = debugCookies;
			this.Objects = objects;
			this.Labels = labels;
		}

		// Token: 0x04000A89 RID: 2697
		internal readonly int MaxStackDepth;

		// Token: 0x04000A8A RID: 2698
		internal readonly int MaxContinuationDepth;

		// Token: 0x04000A8B RID: 2699
		internal readonly Instruction[] Instructions;

		// Token: 0x04000A8C RID: 2700
		internal readonly object[] Objects;

		// Token: 0x04000A8D RID: 2701
		internal readonly RuntimeLabel[] Labels;

		// Token: 0x04000A8E RID: 2702
		internal readonly List<KeyValuePair<int, object>> DebugCookies;

		// Token: 0x02000325 RID: 805
		internal sealed class DebugView
		{
			// Token: 0x06001539 RID: 5433 RVA: 0x0003BD5B File Offset: 0x00039F5B
			public DebugView(InstructionArray array)
			{
				ContractUtils.RequiresNotNull(array, "array");
				this._array = array;
			}

			// Token: 0x1700044A RID: 1098
			// (get) Token: 0x0600153A RID: 5434 RVA: 0x0003BD7A File Offset: 0x00039F7A
			[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
			public InstructionList.DebugView.InstructionView[] A0
			{
				get
				{
					return this.GetInstructionViews(true);
				}
			}

			// Token: 0x0600153B RID: 5435 RVA: 0x0003BD83 File Offset: 0x00039F83
			public InstructionList.DebugView.InstructionView[] GetInstructionViews(bool includeDebugCookies = false)
			{
				return InstructionList.DebugView.GetInstructionViews(this._array.Instructions, this._array.Objects, (int index) => this._array.Labels[index].Index, includeDebugCookies ? this._array.DebugCookies : null);
			}

			// Token: 0x0600153C RID: 5436 RVA: 0x0003BDBD File Offset: 0x00039FBD
			[CompilerGenerated]
			private int <GetInstructionViews>b__4_0(int index)
			{
				return this._array.Labels[index].Index;
			}

			// Token: 0x04000A8F RID: 2703
			private readonly InstructionArray _array;
		}
	}
}
