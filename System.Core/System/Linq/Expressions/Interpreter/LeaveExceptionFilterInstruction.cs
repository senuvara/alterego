﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002C0 RID: 704
	internal sealed class LeaveExceptionFilterInstruction : Instruction
	{
		// Token: 0x0600142E RID: 5166 RVA: 0x00037C51 File Offset: 0x00035E51
		private LeaveExceptionFilterInstruction()
		{
		}

		// Token: 0x17000414 RID: 1044
		// (get) Token: 0x0600142F RID: 5167 RVA: 0x000397D2 File Offset: 0x000379D2
		public override string InstructionName
		{
			get
			{
				return "LeaveExceptionFilter";
			}
		}

		// Token: 0x17000415 RID: 1045
		// (get) Token: 0x06001430 RID: 5168 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x06001431 RID: 5169 RVA: 0x00009CDF File Offset: 0x00007EDF
		[ExcludeFromCodeCoverage]
		public override int Run(InterpretedFrame frame)
		{
			return 1;
		}

		// Token: 0x06001432 RID: 5170 RVA: 0x000397D9 File Offset: 0x000379D9
		// Note: this type is marked as 'beforefieldinit'.
		static LeaveExceptionFilterInstruction()
		{
		}

		// Token: 0x04000A0E RID: 2574
		internal static readonly LeaveExceptionFilterInstruction Instance = new LeaveExceptionFilterInstruction();
	}
}
