﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020003C9 RID: 969
	internal abstract class NumericConvertInstruction : Instruction
	{
		// Token: 0x06001848 RID: 6216 RVA: 0x00047D1B File Offset: 0x00045F1B
		protected NumericConvertInstruction(TypeCode from, TypeCode to, bool isLiftedToNull)
		{
			this._from = from;
			this._to = to;
			this._isLiftedToNull = isLiftedToNull;
		}

		// Token: 0x06001849 RID: 6217 RVA: 0x00047D38 File Offset: 0x00045F38
		public sealed override int Run(InterpretedFrame frame)
		{
			object obj = frame.Pop();
			object value;
			if (obj == null)
			{
				if (!this._isLiftedToNull)
				{
					return ((int?)obj).Value;
				}
				value = null;
			}
			else
			{
				value = this.Convert(obj);
			}
			frame.Push(value);
			return 1;
		}

		// Token: 0x0600184A RID: 6218
		protected abstract object Convert(object obj);

		// Token: 0x170004B0 RID: 1200
		// (get) Token: 0x0600184B RID: 6219 RVA: 0x00047D7A File Offset: 0x00045F7A
		public override string InstructionName
		{
			get
			{
				return "NumericConvert";
			}
		}

		// Token: 0x170004B1 RID: 1201
		// (get) Token: 0x0600184C RID: 6220 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004B2 RID: 1202
		// (get) Token: 0x0600184D RID: 6221 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x0600184E RID: 6222 RVA: 0x00047D84 File Offset: 0x00045F84
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				this.InstructionName,
				"(",
				this._from,
				"->",
				this._to,
				")"
			});
		}

		// Token: 0x04000BB1 RID: 2993
		internal readonly TypeCode _from;

		// Token: 0x04000BB2 RID: 2994
		internal readonly TypeCode _to;

		// Token: 0x04000BB3 RID: 2995
		private readonly bool _isLiftedToNull;

		// Token: 0x020003CA RID: 970
		internal sealed class Unchecked : NumericConvertInstruction
		{
			// Token: 0x170004B3 RID: 1203
			// (get) Token: 0x0600184F RID: 6223 RVA: 0x00047DD9 File Offset: 0x00045FD9
			public override string InstructionName
			{
				get
				{
					return "UncheckedConvert";
				}
			}

			// Token: 0x06001850 RID: 6224 RVA: 0x00047DE0 File Offset: 0x00045FE0
			public Unchecked(TypeCode from, TypeCode to, bool isLiftedToNull) : base(from, to, isLiftedToNull)
			{
			}

			// Token: 0x06001851 RID: 6225 RVA: 0x00047DEC File Offset: 0x00045FEC
			protected override object Convert(object obj)
			{
				switch (this._from)
				{
				case TypeCode.Boolean:
					return this.ConvertInt32(((bool)obj) ? 1 : 0);
				case TypeCode.Char:
					return this.ConvertInt32((int)((char)obj));
				case TypeCode.SByte:
					return this.ConvertInt32((int)((sbyte)obj));
				case TypeCode.Byte:
					return this.ConvertInt32((int)((byte)obj));
				case TypeCode.Int16:
					return this.ConvertInt32((int)((short)obj));
				case TypeCode.UInt16:
					return this.ConvertInt32((int)((ushort)obj));
				case TypeCode.Int32:
					return this.ConvertInt32((int)obj);
				case TypeCode.UInt32:
					return this.ConvertInt64((long)((ulong)((uint)obj)));
				case TypeCode.Int64:
					return this.ConvertInt64((long)obj);
				case TypeCode.UInt64:
					return this.ConvertUInt64((ulong)obj);
				case TypeCode.Single:
					return this.ConvertDouble((double)((float)obj));
				case TypeCode.Double:
					return this.ConvertDouble((double)obj);
				default:
					throw ContractUtils.Unreachable;
				}
			}

			// Token: 0x06001852 RID: 6226 RVA: 0x00047EE8 File Offset: 0x000460E8
			private object ConvertInt32(int obj)
			{
				switch (this._to)
				{
				case TypeCode.Boolean:
					return obj != 0;
				case TypeCode.Char:
					return (char)obj;
				case TypeCode.SByte:
					return (sbyte)obj;
				case TypeCode.Byte:
					return (byte)obj;
				case TypeCode.Int16:
					return (short)obj;
				case TypeCode.UInt16:
					return (ushort)obj;
				case TypeCode.Int32:
					return obj;
				case TypeCode.UInt32:
					return (uint)obj;
				case TypeCode.Int64:
					return (long)obj;
				case TypeCode.UInt64:
					return (ulong)((long)obj);
				case TypeCode.Single:
					return (float)obj;
				case TypeCode.Double:
					return (double)obj;
				case TypeCode.Decimal:
					return obj;
				default:
					throw ContractUtils.Unreachable;
				}
			}

			// Token: 0x06001853 RID: 6227 RVA: 0x00047FAC File Offset: 0x000461AC
			private object ConvertInt64(long obj)
			{
				switch (this._to)
				{
				case TypeCode.Char:
					return (char)obj;
				case TypeCode.SByte:
					return (sbyte)obj;
				case TypeCode.Byte:
					return (byte)obj;
				case TypeCode.Int16:
					return (short)obj;
				case TypeCode.UInt16:
					return (ushort)obj;
				case TypeCode.Int32:
					return (int)obj;
				case TypeCode.UInt32:
					return (uint)obj;
				case TypeCode.Int64:
					return obj;
				case TypeCode.UInt64:
					return (ulong)obj;
				case TypeCode.Single:
					return (float)obj;
				case TypeCode.Double:
					return (double)obj;
				case TypeCode.Decimal:
					return obj;
				default:
					throw ContractUtils.Unreachable;
				}
			}

			// Token: 0x06001854 RID: 6228 RVA: 0x00048064 File Offset: 0x00046264
			private object ConvertUInt64(ulong obj)
			{
				switch (this._to)
				{
				case TypeCode.Char:
					return (char)obj;
				case TypeCode.SByte:
					return (sbyte)obj;
				case TypeCode.Byte:
					return (byte)obj;
				case TypeCode.Int16:
					return (short)obj;
				case TypeCode.UInt16:
					return (ushort)obj;
				case TypeCode.Int32:
					return (int)obj;
				case TypeCode.UInt32:
					return (uint)obj;
				case TypeCode.Int64:
					return (long)obj;
				case TypeCode.UInt64:
					return obj;
				case TypeCode.Single:
					return obj;
				case TypeCode.Double:
					return obj;
				case TypeCode.Decimal:
					return obj;
				default:
					throw ContractUtils.Unreachable;
				}
			}

			// Token: 0x06001855 RID: 6229 RVA: 0x0004811C File Offset: 0x0004631C
			private object ConvertDouble(double obj)
			{
				switch (this._to)
				{
				case TypeCode.Char:
					return (char)obj;
				case TypeCode.SByte:
					return (sbyte)obj;
				case TypeCode.Byte:
					return (byte)obj;
				case TypeCode.Int16:
					return (short)obj;
				case TypeCode.UInt16:
					return (ushort)obj;
				case TypeCode.Int32:
					return (int)obj;
				case TypeCode.UInt32:
					return (uint)obj;
				case TypeCode.Int64:
					return (long)obj;
				case TypeCode.UInt64:
					return (ulong)obj;
				case TypeCode.Single:
					return (float)obj;
				case TypeCode.Double:
					return obj;
				case TypeCode.Decimal:
					return (decimal)obj;
				default:
					throw ContractUtils.Unreachable;
				}
			}
		}

		// Token: 0x020003CB RID: 971
		internal sealed class Checked : NumericConvertInstruction
		{
			// Token: 0x170004B4 RID: 1204
			// (get) Token: 0x06001856 RID: 6230 RVA: 0x000481D3 File Offset: 0x000463D3
			public override string InstructionName
			{
				get
				{
					return "CheckedConvert";
				}
			}

			// Token: 0x06001857 RID: 6231 RVA: 0x00047DE0 File Offset: 0x00045FE0
			public Checked(TypeCode from, TypeCode to, bool isLiftedToNull) : base(from, to, isLiftedToNull)
			{
			}

			// Token: 0x06001858 RID: 6232 RVA: 0x000481DC File Offset: 0x000463DC
			protected override object Convert(object obj)
			{
				switch (this._from)
				{
				case TypeCode.Boolean:
					return this.ConvertInt32(((bool)obj) ? 1 : 0);
				case TypeCode.Char:
					return this.ConvertInt32((int)((char)obj));
				case TypeCode.SByte:
					return this.ConvertInt32((int)((sbyte)obj));
				case TypeCode.Byte:
					return this.ConvertInt32((int)((byte)obj));
				case TypeCode.Int16:
					return this.ConvertInt32((int)((short)obj));
				case TypeCode.UInt16:
					return this.ConvertInt32((int)((ushort)obj));
				case TypeCode.Int32:
					return this.ConvertInt32((int)obj);
				case TypeCode.UInt32:
					return this.ConvertInt64((long)((ulong)((uint)obj)));
				case TypeCode.Int64:
					return this.ConvertInt64((long)obj);
				case TypeCode.UInt64:
					return this.ConvertUInt64((ulong)obj);
				case TypeCode.Single:
					return this.ConvertDouble((double)((float)obj));
				case TypeCode.Double:
					return this.ConvertDouble((double)obj);
				default:
					throw ContractUtils.Unreachable;
				}
			}

			// Token: 0x06001859 RID: 6233 RVA: 0x000482D8 File Offset: 0x000464D8
			private object ConvertInt32(int obj)
			{
				checked
				{
					switch (this._to)
					{
					case TypeCode.Boolean:
						return obj != 0;
					case TypeCode.Char:
						return (char)obj;
					case TypeCode.SByte:
						return (sbyte)obj;
					case TypeCode.Byte:
						return (byte)obj;
					case TypeCode.Int16:
						return (short)obj;
					case TypeCode.UInt16:
						return (ushort)obj;
					case TypeCode.Int32:
						return obj;
					case TypeCode.UInt32:
						return (uint)obj;
					case TypeCode.Int64:
						return unchecked((long)obj);
					case TypeCode.UInt64:
						return (ulong)obj;
					case TypeCode.Single:
						return (float)obj;
					case TypeCode.Double:
						return (double)obj;
					case TypeCode.Decimal:
						return obj;
					default:
						throw ContractUtils.Unreachable;
					}
				}
			}

			// Token: 0x0600185A RID: 6234 RVA: 0x0004839C File Offset: 0x0004659C
			private object ConvertInt64(long obj)
			{
				checked
				{
					switch (this._to)
					{
					case TypeCode.Char:
						return (char)obj;
					case TypeCode.SByte:
						return (sbyte)obj;
					case TypeCode.Byte:
						return (byte)obj;
					case TypeCode.Int16:
						return (short)obj;
					case TypeCode.UInt16:
						return (ushort)obj;
					case TypeCode.Int32:
						return (int)obj;
					case TypeCode.UInt32:
						return (uint)obj;
					case TypeCode.Int64:
						return obj;
					case TypeCode.UInt64:
						return (ulong)obj;
					case TypeCode.Single:
						return (float)obj;
					case TypeCode.Double:
						return (double)obj;
					case TypeCode.Decimal:
						return obj;
					default:
						throw ContractUtils.Unreachable;
					}
				}
			}

			// Token: 0x0600185B RID: 6235 RVA: 0x00048454 File Offset: 0x00046654
			private object ConvertUInt64(ulong obj)
			{
				checked
				{
					switch (this._to)
					{
					case TypeCode.Char:
						return (char)obj;
					case TypeCode.SByte:
						return (sbyte)obj;
					case TypeCode.Byte:
						return (byte)obj;
					case TypeCode.Int16:
						return (short)obj;
					case TypeCode.UInt16:
						return (ushort)obj;
					case TypeCode.Int32:
						return (int)obj;
					case TypeCode.UInt32:
						return (uint)obj;
					case TypeCode.Int64:
						return (long)obj;
					case TypeCode.UInt64:
						return obj;
					case TypeCode.Single:
						return obj;
					case TypeCode.Double:
						return obj;
					case TypeCode.Decimal:
						return obj;
					default:
						throw ContractUtils.Unreachable;
					}
				}
			}

			// Token: 0x0600185C RID: 6236 RVA: 0x0004850C File Offset: 0x0004670C
			private object ConvertDouble(double obj)
			{
				checked
				{
					switch (this._to)
					{
					case TypeCode.Char:
						return (char)obj;
					case TypeCode.SByte:
						return (sbyte)obj;
					case TypeCode.Byte:
						return (byte)obj;
					case TypeCode.Int16:
						return (short)obj;
					case TypeCode.UInt16:
						return (ushort)obj;
					case TypeCode.Int32:
						return (int)obj;
					case TypeCode.UInt32:
						return (uint)obj;
					case TypeCode.Int64:
						return (long)obj;
					case TypeCode.UInt64:
						return (ulong)obj;
					case TypeCode.Single:
						return (float)obj;
					case TypeCode.Double:
						return obj;
					case TypeCode.Decimal:
						return (decimal)obj;
					default:
						throw ContractUtils.Unreachable;
					}
				}
			}
		}

		// Token: 0x020003CC RID: 972
		internal sealed class ToUnderlying : NumericConvertInstruction
		{
			// Token: 0x170004B5 RID: 1205
			// (get) Token: 0x0600185D RID: 6237 RVA: 0x000485C3 File Offset: 0x000467C3
			public override string InstructionName
			{
				get
				{
					return "ConvertToUnderlying";
				}
			}

			// Token: 0x0600185E RID: 6238 RVA: 0x000485CA File Offset: 0x000467CA
			public ToUnderlying(TypeCode to, bool isLiftedToNull) : base(to, to, isLiftedToNull)
			{
			}

			// Token: 0x0600185F RID: 6239 RVA: 0x000485D8 File Offset: 0x000467D8
			protected override object Convert(object obj)
			{
				switch (this._to)
				{
				case TypeCode.Boolean:
					return (bool)obj;
				case TypeCode.Char:
					return (char)obj;
				case TypeCode.SByte:
					return (sbyte)obj;
				case TypeCode.Byte:
					return (byte)obj;
				case TypeCode.Int16:
					return (short)obj;
				case TypeCode.UInt16:
					return (ushort)obj;
				case TypeCode.Int32:
					return (int)obj;
				case TypeCode.UInt32:
					return (uint)obj;
				case TypeCode.Int64:
					return (long)obj;
				case TypeCode.UInt64:
					return (ulong)obj;
				default:
					throw ContractUtils.Unreachable;
				}
			}
		}
	}
}
