﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000329 RID: 809
	internal sealed class InterpretedFrame
	{
		// Token: 0x060015BA RID: 5562 RVA: 0x0003CDAC File Offset: 0x0003AFAC
		internal InterpretedFrame(Interpreter interpreter, IStrongBox[] closure)
		{
			this.Interpreter = interpreter;
			this.StackIndex = interpreter.LocalCount;
			this.Data = new object[this.StackIndex + interpreter.Instructions.MaxStackDepth];
			int maxContinuationDepth = interpreter.Instructions.MaxContinuationDepth;
			if (maxContinuationDepth > 0)
			{
				this._continuations = new int[maxContinuationDepth];
			}
			this.Closure = closure;
			this._pendingContinuation = -1;
			this._pendingValue = Interpreter.NoValue;
		}

		// Token: 0x060015BB RID: 5563 RVA: 0x0003CE24 File Offset: 0x0003B024
		public DebugInfo GetDebugInfo(int instructionIndex)
		{
			return DebugInfo.GetMatchingDebugInfo(this.Interpreter._debugInfos, instructionIndex);
		}

		// Token: 0x17000450 RID: 1104
		// (get) Token: 0x060015BC RID: 5564 RVA: 0x0003CE37 File Offset: 0x0003B037
		public string Name
		{
			get
			{
				return this.Interpreter.Name;
			}
		}

		// Token: 0x060015BD RID: 5565 RVA: 0x0003CE44 File Offset: 0x0003B044
		public void Push(object value)
		{
			object[] data = this.Data;
			int stackIndex = this.StackIndex;
			this.StackIndex = stackIndex + 1;
			data[stackIndex] = value;
		}

		// Token: 0x060015BE RID: 5566 RVA: 0x0003CE6C File Offset: 0x0003B06C
		public void Push(bool value)
		{
			object[] data = this.Data;
			int stackIndex = this.StackIndex;
			this.StackIndex = stackIndex + 1;
			data[stackIndex] = (value ? Utils.BoxedTrue : Utils.BoxedFalse);
		}

		// Token: 0x060015BF RID: 5567 RVA: 0x0003CEA0 File Offset: 0x0003B0A0
		public void Push(int value)
		{
			object[] data = this.Data;
			int stackIndex = this.StackIndex;
			this.StackIndex = stackIndex + 1;
			data[stackIndex] = ScriptingRuntimeHelpers.Int32ToObject(value);
		}

		// Token: 0x060015C0 RID: 5568 RVA: 0x0003CECC File Offset: 0x0003B0CC
		public void Push(byte value)
		{
			object[] data = this.Data;
			int stackIndex = this.StackIndex;
			this.StackIndex = stackIndex + 1;
			data[stackIndex] = value;
		}

		// Token: 0x060015C1 RID: 5569 RVA: 0x0003CEF8 File Offset: 0x0003B0F8
		public void Push(sbyte value)
		{
			object[] data = this.Data;
			int stackIndex = this.StackIndex;
			this.StackIndex = stackIndex + 1;
			data[stackIndex] = value;
		}

		// Token: 0x060015C2 RID: 5570 RVA: 0x0003CF24 File Offset: 0x0003B124
		public void Push(short value)
		{
			object[] data = this.Data;
			int stackIndex = this.StackIndex;
			this.StackIndex = stackIndex + 1;
			data[stackIndex] = value;
		}

		// Token: 0x060015C3 RID: 5571 RVA: 0x0003CF50 File Offset: 0x0003B150
		public void Push(ushort value)
		{
			object[] data = this.Data;
			int stackIndex = this.StackIndex;
			this.StackIndex = stackIndex + 1;
			data[stackIndex] = value;
		}

		// Token: 0x060015C4 RID: 5572 RVA: 0x0003CF7C File Offset: 0x0003B17C
		public object Pop()
		{
			object[] data = this.Data;
			int num = this.StackIndex - 1;
			this.StackIndex = num;
			return data[num];
		}

		// Token: 0x060015C5 RID: 5573 RVA: 0x0003CFA1 File Offset: 0x0003B1A1
		internal void SetStackDepth(int depth)
		{
			this.StackIndex = this.Interpreter.LocalCount + depth;
		}

		// Token: 0x060015C6 RID: 5574 RVA: 0x0003CFB6 File Offset: 0x0003B1B6
		public object Peek()
		{
			return this.Data[this.StackIndex - 1];
		}

		// Token: 0x060015C7 RID: 5575 RVA: 0x0003CFC8 File Offset: 0x0003B1C8
		public void Dup()
		{
			int stackIndex = this.StackIndex;
			this.Data[stackIndex] = this.Data[stackIndex - 1];
			this.StackIndex = stackIndex + 1;
		}

		// Token: 0x17000451 RID: 1105
		// (get) Token: 0x060015C8 RID: 5576 RVA: 0x0003CFF7 File Offset: 0x0003B1F7
		public InterpretedFrame Parent
		{
			get
			{
				return this._parent;
			}
		}

		// Token: 0x060015C9 RID: 5577 RVA: 0x0003CFFF File Offset: 0x0003B1FF
		public static bool IsInterpretedFrame(MethodBase method)
		{
			return method.DeclaringType == typeof(Interpreter) && method.Name == "Run";
		}

		// Token: 0x060015CA RID: 5578 RVA: 0x0003D02A File Offset: 0x0003B22A
		public IEnumerable<InterpretedFrameInfo> GetStackTraceDebugInfo()
		{
			InterpretedFrame frame = this;
			do
			{
				yield return new InterpretedFrameInfo(frame.Name, frame.GetDebugInfo(frame.InstructionIndex));
				frame = frame.Parent;
			}
			while (frame != null);
			yield break;
		}

		// Token: 0x060015CB RID: 5579 RVA: 0x0003D03A File Offset: 0x0003B23A
		internal void SaveTraceToException(Exception exception)
		{
			if (exception.Data[typeof(InterpretedFrameInfo)] == null)
			{
				exception.Data[typeof(InterpretedFrameInfo)] = new List<InterpretedFrameInfo>(this.GetStackTraceDebugInfo()).ToArray();
			}
		}

		// Token: 0x060015CC RID: 5580 RVA: 0x0003D078 File Offset: 0x0003B278
		public static InterpretedFrameInfo[] GetExceptionStackTrace(Exception exception)
		{
			return exception.Data[typeof(InterpretedFrameInfo)] as InterpretedFrameInfo[];
		}

		// Token: 0x060015CD RID: 5581 RVA: 0x0003D094 File Offset: 0x0003B294
		internal InterpretedFrame Enter()
		{
			InterpretedFrame parent = InterpretedFrame.s_currentFrame;
			InterpretedFrame.s_currentFrame = this;
			return this._parent = parent;
		}

		// Token: 0x060015CE RID: 5582 RVA: 0x0003D0B7 File Offset: 0x0003B2B7
		internal void Leave(InterpretedFrame prevFrame)
		{
			InterpretedFrame.s_currentFrame = prevFrame;
		}

		// Token: 0x060015CF RID: 5583 RVA: 0x0003D0BF File Offset: 0x0003B2BF
		internal bool IsJumpHappened()
		{
			return this._pendingContinuation >= 0;
		}

		// Token: 0x060015D0 RID: 5584 RVA: 0x0003D0CD File Offset: 0x0003B2CD
		public void RemoveContinuation()
		{
			this._continuationIndex--;
		}

		// Token: 0x060015D1 RID: 5585 RVA: 0x0003D0E0 File Offset: 0x0003B2E0
		public void PushContinuation(int continuation)
		{
			int[] continuations = this._continuations;
			int continuationIndex = this._continuationIndex;
			this._continuationIndex = continuationIndex + 1;
			continuations[continuationIndex] = continuation;
		}

		// Token: 0x060015D2 RID: 5586 RVA: 0x0003D108 File Offset: 0x0003B308
		public int YieldToCurrentContinuation()
		{
			RuntimeLabel runtimeLabel = this.Interpreter._labels[this._continuations[this._continuationIndex - 1]];
			this.SetStackDepth(runtimeLabel.StackDepth);
			return runtimeLabel.Index - this.InstructionIndex;
		}

		// Token: 0x060015D3 RID: 5587 RVA: 0x0003D150 File Offset: 0x0003B350
		public int YieldToPendingContinuation()
		{
			RuntimeLabel runtimeLabel = this.Interpreter._labels[this._pendingContinuation];
			if (runtimeLabel.ContinuationStackDepth < this._continuationIndex)
			{
				RuntimeLabel runtimeLabel2 = this.Interpreter._labels[this._continuations[this._continuationIndex - 1]];
				this.SetStackDepth(runtimeLabel2.StackDepth);
				return runtimeLabel2.Index - this.InstructionIndex;
			}
			this.SetStackDepth(runtimeLabel.StackDepth);
			if (this._pendingValue != Interpreter.NoValue)
			{
				this.Data[this.StackIndex - 1] = this._pendingValue;
			}
			this._pendingContinuation = -1;
			this._pendingValue = Interpreter.NoValue;
			return runtimeLabel.Index - this.InstructionIndex;
		}

		// Token: 0x060015D4 RID: 5588 RVA: 0x0003D209 File Offset: 0x0003B409
		internal void PushPendingContinuation()
		{
			this.Push(this._pendingContinuation);
			this.Push(this._pendingValue);
			this._pendingContinuation = -1;
			this._pendingValue = Interpreter.NoValue;
		}

		// Token: 0x060015D5 RID: 5589 RVA: 0x0003D235 File Offset: 0x0003B435
		internal void PopPendingContinuation()
		{
			this._pendingValue = this.Pop();
			this._pendingContinuation = (int)this.Pop();
		}

		// Token: 0x060015D6 RID: 5590 RVA: 0x0003D254 File Offset: 0x0003B454
		public int Goto(int labelIndex, object value, bool gotoExceptionHandler)
		{
			RuntimeLabel runtimeLabel = this.Interpreter._labels[labelIndex];
			if (this._continuationIndex == runtimeLabel.ContinuationStackDepth)
			{
				this.SetStackDepth(runtimeLabel.StackDepth);
				if (value != Interpreter.NoValue)
				{
					this.Data[this.StackIndex - 1] = value;
				}
				return runtimeLabel.Index - this.InstructionIndex;
			}
			this._pendingContinuation = labelIndex;
			this._pendingValue = value;
			return this.YieldToCurrentContinuation();
		}

		// Token: 0x04000AB3 RID: 2739
		[ThreadStatic]
		private static InterpretedFrame s_currentFrame;

		// Token: 0x04000AB4 RID: 2740
		internal readonly Interpreter Interpreter;

		// Token: 0x04000AB5 RID: 2741
		internal InterpretedFrame _parent;

		// Token: 0x04000AB6 RID: 2742
		private readonly int[] _continuations;

		// Token: 0x04000AB7 RID: 2743
		private int _continuationIndex;

		// Token: 0x04000AB8 RID: 2744
		private int _pendingContinuation;

		// Token: 0x04000AB9 RID: 2745
		private object _pendingValue;

		// Token: 0x04000ABA RID: 2746
		public readonly object[] Data;

		// Token: 0x04000ABB RID: 2747
		public readonly IStrongBox[] Closure;

		// Token: 0x04000ABC RID: 2748
		public int StackIndex;

		// Token: 0x04000ABD RID: 2749
		public int InstructionIndex;

		// Token: 0x0200032A RID: 810
		[CompilerGenerated]
		private sealed class <GetStackTraceDebugInfo>d__29 : IEnumerable<InterpretedFrameInfo>, IEnumerable, IEnumerator<InterpretedFrameInfo>, IDisposable, IEnumerator
		{
			// Token: 0x060015D7 RID: 5591 RVA: 0x0003D2C7 File Offset: 0x0003B4C7
			[DebuggerHidden]
			public <GetStackTraceDebugInfo>d__29(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060015D8 RID: 5592 RVA: 0x000039E8 File Offset: 0x00001BE8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x060015D9 RID: 5593 RVA: 0x0003D2E4 File Offset: 0x0003B4E4
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				InterpretedFrame interpretedFrame = this;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
					frame = frame.Parent;
					if (frame == null)
					{
						return false;
					}
				}
				else
				{
					this.<>1__state = -1;
					frame = interpretedFrame;
				}
				this.<>2__current = new InterpretedFrameInfo(frame.Name, frame.GetDebugInfo(frame.InstructionIndex));
				this.<>1__state = 1;
				return true;
			}

			// Token: 0x17000452 RID: 1106
			// (get) Token: 0x060015DA RID: 5594 RVA: 0x0003D36C File Offset: 0x0003B56C
			InterpretedFrameInfo IEnumerator<InterpretedFrameInfo>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060015DB RID: 5595 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000453 RID: 1107
			// (get) Token: 0x060015DC RID: 5596 RVA: 0x0003D374 File Offset: 0x0003B574
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060015DD RID: 5597 RVA: 0x0003D384 File Offset: 0x0003B584
			[DebuggerHidden]
			IEnumerator<InterpretedFrameInfo> IEnumerable<InterpretedFrameInfo>.GetEnumerator()
			{
				InterpretedFrame.<GetStackTraceDebugInfo>d__29 <GetStackTraceDebugInfo>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<GetStackTraceDebugInfo>d__ = this;
				}
				else
				{
					<GetStackTraceDebugInfo>d__ = new InterpretedFrame.<GetStackTraceDebugInfo>d__29(0);
					<GetStackTraceDebugInfo>d__.<>4__this = this;
				}
				return <GetStackTraceDebugInfo>d__;
			}

			// Token: 0x060015DE RID: 5598 RVA: 0x0003D3C7 File Offset: 0x0003B5C7
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Linq.Expressions.Interpreter.InterpretedFrameInfo>.GetEnumerator();
			}

			// Token: 0x04000ABE RID: 2750
			private int <>1__state;

			// Token: 0x04000ABF RID: 2751
			private InterpretedFrameInfo <>2__current;

			// Token: 0x04000AC0 RID: 2752
			private int <>l__initialThreadId;

			// Token: 0x04000AC1 RID: 2753
			public InterpretedFrame <>4__this;

			// Token: 0x04000AC2 RID: 2754
			private InterpretedFrame <frame>5__1;
		}
	}
}
