﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002A6 RID: 678
	internal sealed class NewArrayInitInstruction : Instruction
	{
		// Token: 0x0600139A RID: 5018 RVA: 0x00038655 File Offset: 0x00036855
		internal NewArrayInitInstruction(Type elementType, int elementCount)
		{
			this._elementType = elementType;
			this._elementCount = elementCount;
		}

		// Token: 0x170003D6 RID: 982
		// (get) Token: 0x0600139B RID: 5019 RVA: 0x0003866B File Offset: 0x0003686B
		public override int ConsumedStack
		{
			get
			{
				return this._elementCount;
			}
		}

		// Token: 0x170003D7 RID: 983
		// (get) Token: 0x0600139C RID: 5020 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170003D8 RID: 984
		// (get) Token: 0x0600139D RID: 5021 RVA: 0x00038673 File Offset: 0x00036873
		public override string InstructionName
		{
			get
			{
				return "NewArrayInit";
			}
		}

		// Token: 0x0600139E RID: 5022 RVA: 0x0003867C File Offset: 0x0003687C
		public override int Run(InterpretedFrame frame)
		{
			Array array = Array.CreateInstance(this._elementType, this._elementCount);
			for (int i = this._elementCount - 1; i >= 0; i--)
			{
				array.SetValue(frame.Pop(), i);
			}
			frame.Push(array);
			return 1;
		}

		// Token: 0x040009E1 RID: 2529
		private readonly Type _elementType;

		// Token: 0x040009E2 RID: 2530
		private readonly int _elementCount;
	}
}
