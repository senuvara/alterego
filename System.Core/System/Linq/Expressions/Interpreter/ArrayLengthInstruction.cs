﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002AB RID: 683
	internal sealed class ArrayLengthInstruction : Instruction
	{
		// Token: 0x170003E4 RID: 996
		// (get) Token: 0x060013B4 RID: 5044 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170003E5 RID: 997
		// (get) Token: 0x060013B5 RID: 5045 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170003E6 RID: 998
		// (get) Token: 0x060013B6 RID: 5046 RVA: 0x00038827 File Offset: 0x00036A27
		public override string InstructionName
		{
			get
			{
				return "ArrayLength";
			}
		}

		// Token: 0x060013B7 RID: 5047 RVA: 0x00037C51 File Offset: 0x00035E51
		private ArrayLengthInstruction()
		{
		}

		// Token: 0x060013B8 RID: 5048 RVA: 0x00038830 File Offset: 0x00036A30
		public override int Run(InterpretedFrame frame)
		{
			object obj = frame.Pop();
			frame.Push(((Array)obj).Length);
			return 1;
		}

		// Token: 0x060013B9 RID: 5049 RVA: 0x00038856 File Offset: 0x00036A56
		// Note: this type is marked as 'beforefieldinit'.
		static ArrayLengthInstruction()
		{
		}

		// Token: 0x040009E8 RID: 2536
		public static readonly ArrayLengthInstruction Instance = new ArrayLengthInstruction();
	}
}
