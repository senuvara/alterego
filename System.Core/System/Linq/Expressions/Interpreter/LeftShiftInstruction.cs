﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000330 RID: 816
	internal abstract class LeftShiftInstruction : Instruction
	{
		// Token: 0x1700045C RID: 1116
		// (get) Token: 0x060015FC RID: 5628 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x1700045D RID: 1117
		// (get) Token: 0x060015FD RID: 5629 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x1700045E RID: 1118
		// (get) Token: 0x060015FE RID: 5630 RVA: 0x0003D925 File Offset: 0x0003BB25
		public override string InstructionName
		{
			get
			{
				return "LeftShift";
			}
		}

		// Token: 0x060015FF RID: 5631 RVA: 0x00037C51 File Offset: 0x00035E51
		private LeftShiftInstruction()
		{
		}

		// Token: 0x06001600 RID: 5632 RVA: 0x0003D92C File Offset: 0x0003BB2C
		public static Instruction Create(Type type)
		{
			switch (type.GetNonNullableType().GetTypeCode())
			{
			case TypeCode.SByte:
			{
				Instruction result;
				if ((result = LeftShiftInstruction.s_SByte) == null)
				{
					result = (LeftShiftInstruction.s_SByte = new LeftShiftInstruction.LeftShiftSByte());
				}
				return result;
			}
			case TypeCode.Byte:
			{
				Instruction result2;
				if ((result2 = LeftShiftInstruction.s_Byte) == null)
				{
					result2 = (LeftShiftInstruction.s_Byte = new LeftShiftInstruction.LeftShiftByte());
				}
				return result2;
			}
			case TypeCode.Int16:
			{
				Instruction result3;
				if ((result3 = LeftShiftInstruction.s_Int16) == null)
				{
					result3 = (LeftShiftInstruction.s_Int16 = new LeftShiftInstruction.LeftShiftInt16());
				}
				return result3;
			}
			case TypeCode.UInt16:
			{
				Instruction result4;
				if ((result4 = LeftShiftInstruction.s_UInt16) == null)
				{
					result4 = (LeftShiftInstruction.s_UInt16 = new LeftShiftInstruction.LeftShiftUInt16());
				}
				return result4;
			}
			case TypeCode.Int32:
			{
				Instruction result5;
				if ((result5 = LeftShiftInstruction.s_Int32) == null)
				{
					result5 = (LeftShiftInstruction.s_Int32 = new LeftShiftInstruction.LeftShiftInt32());
				}
				return result5;
			}
			case TypeCode.UInt32:
			{
				Instruction result6;
				if ((result6 = LeftShiftInstruction.s_UInt32) == null)
				{
					result6 = (LeftShiftInstruction.s_UInt32 = new LeftShiftInstruction.LeftShiftUInt32());
				}
				return result6;
			}
			case TypeCode.Int64:
			{
				Instruction result7;
				if ((result7 = LeftShiftInstruction.s_Int64) == null)
				{
					result7 = (LeftShiftInstruction.s_Int64 = new LeftShiftInstruction.LeftShiftInt64());
				}
				return result7;
			}
			case TypeCode.UInt64:
			{
				Instruction result8;
				if ((result8 = LeftShiftInstruction.s_UInt64) == null)
				{
					result8 = (LeftShiftInstruction.s_UInt64 = new LeftShiftInstruction.LeftShiftUInt64());
				}
				return result8;
			}
			default:
				throw ContractUtils.Unreachable;
			}
		}

		// Token: 0x04000AE0 RID: 2784
		private static Instruction s_SByte;

		// Token: 0x04000AE1 RID: 2785
		private static Instruction s_Int16;

		// Token: 0x04000AE2 RID: 2786
		private static Instruction s_Int32;

		// Token: 0x04000AE3 RID: 2787
		private static Instruction s_Int64;

		// Token: 0x04000AE4 RID: 2788
		private static Instruction s_Byte;

		// Token: 0x04000AE5 RID: 2789
		private static Instruction s_UInt16;

		// Token: 0x04000AE6 RID: 2790
		private static Instruction s_UInt32;

		// Token: 0x04000AE7 RID: 2791
		private static Instruction s_UInt64;

		// Token: 0x02000331 RID: 817
		private sealed class LeftShiftSByte : LeftShiftInstruction
		{
			// Token: 0x06001601 RID: 5633 RVA: 0x0003DA20 File Offset: 0x0003BC20
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((sbyte)((sbyte)obj2 << (int)obj));
				}
				return 1;
			}

			// Token: 0x06001602 RID: 5634 RVA: 0x0003DA62 File Offset: 0x0003BC62
			public LeftShiftSByte()
			{
			}
		}

		// Token: 0x02000332 RID: 818
		private sealed class LeftShiftInt16 : LeftShiftInstruction
		{
			// Token: 0x06001603 RID: 5635 RVA: 0x0003DA6C File Offset: 0x0003BC6C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((short)((short)obj2 << (int)obj));
				}
				return 1;
			}

			// Token: 0x06001604 RID: 5636 RVA: 0x0003DA62 File Offset: 0x0003BC62
			public LeftShiftInt16()
			{
			}
		}

		// Token: 0x02000333 RID: 819
		private sealed class LeftShiftInt32 : LeftShiftInstruction
		{
			// Token: 0x06001605 RID: 5637 RVA: 0x0003DAB0 File Offset: 0x0003BCB0
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((int)obj2 << (int)obj);
				}
				return 1;
			}

			// Token: 0x06001606 RID: 5638 RVA: 0x0003DA62 File Offset: 0x0003BC62
			public LeftShiftInt32()
			{
			}
		}

		// Token: 0x02000334 RID: 820
		private sealed class LeftShiftInt64 : LeftShiftInstruction
		{
			// Token: 0x06001607 RID: 5639 RVA: 0x0003DAF4 File Offset: 0x0003BCF4
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((long)obj2 << (int)obj);
				}
				return 1;
			}

			// Token: 0x06001608 RID: 5640 RVA: 0x0003DA62 File Offset: 0x0003BC62
			public LeftShiftInt64()
			{
			}
		}

		// Token: 0x02000335 RID: 821
		private sealed class LeftShiftByte : LeftShiftInstruction
		{
			// Token: 0x06001609 RID: 5641 RVA: 0x0003DB3C File Offset: 0x0003BD3C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((byte)((byte)obj2 << (int)obj));
				}
				return 1;
			}

			// Token: 0x0600160A RID: 5642 RVA: 0x0003DA62 File Offset: 0x0003BC62
			public LeftShiftByte()
			{
			}
		}

		// Token: 0x02000336 RID: 822
		private sealed class LeftShiftUInt16 : LeftShiftInstruction
		{
			// Token: 0x0600160B RID: 5643 RVA: 0x0003DB80 File Offset: 0x0003BD80
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((ushort)((ushort)obj2 << (int)obj));
				}
				return 1;
			}

			// Token: 0x0600160C RID: 5644 RVA: 0x0003DA62 File Offset: 0x0003BC62
			public LeftShiftUInt16()
			{
			}
		}

		// Token: 0x02000337 RID: 823
		private sealed class LeftShiftUInt32 : LeftShiftInstruction
		{
			// Token: 0x0600160D RID: 5645 RVA: 0x0003DBC4 File Offset: 0x0003BDC4
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((uint)obj2 << (int)obj);
				}
				return 1;
			}

			// Token: 0x0600160E RID: 5646 RVA: 0x0003DA62 File Offset: 0x0003BC62
			public LeftShiftUInt32()
			{
			}
		}

		// Token: 0x02000338 RID: 824
		private sealed class LeftShiftUInt64 : LeftShiftInstruction
		{
			// Token: 0x0600160F RID: 5647 RVA: 0x0003DC0C File Offset: 0x0003BE0C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(null);
				}
				else
				{
					frame.Push((ulong)obj2 << (int)obj);
				}
				return 1;
			}

			// Token: 0x06001610 RID: 5648 RVA: 0x0003DA62 File Offset: 0x0003BC62
			public LeftShiftUInt64()
			{
			}
		}
	}
}
