﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002B5 RID: 693
	internal sealed class CoalescingBranchInstruction : OffsetInstruction
	{
		// Token: 0x170003F7 RID: 1015
		// (get) Token: 0x060013EA RID: 5098 RVA: 0x0003906A File Offset: 0x0003726A
		public override Instruction[] Cache
		{
			get
			{
				if (CoalescingBranchInstruction.s_cache == null)
				{
					CoalescingBranchInstruction.s_cache = new Instruction[32];
				}
				return CoalescingBranchInstruction.s_cache;
			}
		}

		// Token: 0x170003F8 RID: 1016
		// (get) Token: 0x060013EB RID: 5099 RVA: 0x00039084 File Offset: 0x00037284
		public override string InstructionName
		{
			get
			{
				return "CoalescingBranch";
			}
		}

		// Token: 0x170003F9 RID: 1017
		// (get) Token: 0x060013EC RID: 5100 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170003FA RID: 1018
		// (get) Token: 0x060013ED RID: 5101 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x060013EE RID: 5102 RVA: 0x0003908B File Offset: 0x0003728B
		public override int Run(InterpretedFrame frame)
		{
			if (frame.Peek() != null)
			{
				return this._offset;
			}
			return 1;
		}

		// Token: 0x060013EF RID: 5103 RVA: 0x0003902A File Offset: 0x0003722A
		public CoalescingBranchInstruction()
		{
		}

		// Token: 0x040009FB RID: 2555
		private static Instruction[] s_cache;
	}
}
