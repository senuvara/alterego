﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002F3 RID: 755
	internal abstract class ExclusiveOrInstruction : Instruction
	{
		// Token: 0x1700042E RID: 1070
		// (get) Token: 0x060014B6 RID: 5302 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x1700042F RID: 1071
		// (get) Token: 0x060014B7 RID: 5303 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000430 RID: 1072
		// (get) Token: 0x060014B8 RID: 5304 RVA: 0x0003AA1F File Offset: 0x00038C1F
		public override string InstructionName
		{
			get
			{
				return "ExclusiveOr";
			}
		}

		// Token: 0x060014B9 RID: 5305 RVA: 0x00037C51 File Offset: 0x00035E51
		private ExclusiveOrInstruction()
		{
		}

		// Token: 0x060014BA RID: 5306 RVA: 0x0003AA28 File Offset: 0x00038C28
		public static Instruction Create(Type type)
		{
			switch (type.GetNonNullableType().GetTypeCode())
			{
			case TypeCode.Boolean:
			{
				Instruction result;
				if ((result = ExclusiveOrInstruction.s_Boolean) == null)
				{
					result = (ExclusiveOrInstruction.s_Boolean = new ExclusiveOrInstruction.ExclusiveOrBoolean());
				}
				return result;
			}
			case TypeCode.SByte:
			{
				Instruction result2;
				if ((result2 = ExclusiveOrInstruction.s_SByte) == null)
				{
					result2 = (ExclusiveOrInstruction.s_SByte = new ExclusiveOrInstruction.ExclusiveOrSByte());
				}
				return result2;
			}
			case TypeCode.Byte:
			{
				Instruction result3;
				if ((result3 = ExclusiveOrInstruction.s_Byte) == null)
				{
					result3 = (ExclusiveOrInstruction.s_Byte = new ExclusiveOrInstruction.ExclusiveOrByte());
				}
				return result3;
			}
			case TypeCode.Int16:
			{
				Instruction result4;
				if ((result4 = ExclusiveOrInstruction.s_Int16) == null)
				{
					result4 = (ExclusiveOrInstruction.s_Int16 = new ExclusiveOrInstruction.ExclusiveOrInt16());
				}
				return result4;
			}
			case TypeCode.UInt16:
			{
				Instruction result5;
				if ((result5 = ExclusiveOrInstruction.s_UInt16) == null)
				{
					result5 = (ExclusiveOrInstruction.s_UInt16 = new ExclusiveOrInstruction.ExclusiveOrUInt16());
				}
				return result5;
			}
			case TypeCode.Int32:
			{
				Instruction result6;
				if ((result6 = ExclusiveOrInstruction.s_Int32) == null)
				{
					result6 = (ExclusiveOrInstruction.s_Int32 = new ExclusiveOrInstruction.ExclusiveOrInt32());
				}
				return result6;
			}
			case TypeCode.UInt32:
			{
				Instruction result7;
				if ((result7 = ExclusiveOrInstruction.s_UInt32) == null)
				{
					result7 = (ExclusiveOrInstruction.s_UInt32 = new ExclusiveOrInstruction.ExclusiveOrUInt32());
				}
				return result7;
			}
			case TypeCode.Int64:
			{
				Instruction result8;
				if ((result8 = ExclusiveOrInstruction.s_Int64) == null)
				{
					result8 = (ExclusiveOrInstruction.s_Int64 = new ExclusiveOrInstruction.ExclusiveOrInt64());
				}
				return result8;
			}
			case TypeCode.UInt64:
			{
				Instruction result9;
				if ((result9 = ExclusiveOrInstruction.s_UInt64) == null)
				{
					result9 = (ExclusiveOrInstruction.s_UInt64 = new ExclusiveOrInstruction.ExclusiveOrUInt64());
				}
				return result9;
			}
			}
			throw ContractUtils.Unreachable;
		}

		// Token: 0x04000A48 RID: 2632
		private static Instruction s_SByte;

		// Token: 0x04000A49 RID: 2633
		private static Instruction s_Int16;

		// Token: 0x04000A4A RID: 2634
		private static Instruction s_Int32;

		// Token: 0x04000A4B RID: 2635
		private static Instruction s_Int64;

		// Token: 0x04000A4C RID: 2636
		private static Instruction s_Byte;

		// Token: 0x04000A4D RID: 2637
		private static Instruction s_UInt16;

		// Token: 0x04000A4E RID: 2638
		private static Instruction s_UInt32;

		// Token: 0x04000A4F RID: 2639
		private static Instruction s_UInt64;

		// Token: 0x04000A50 RID: 2640
		private static Instruction s_Boolean;

		// Token: 0x020002F4 RID: 756
		private sealed class ExclusiveOrSByte : ExclusiveOrInstruction
		{
			// Token: 0x060014BB RID: 5307 RVA: 0x0003AB38 File Offset: 0x00038D38
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((sbyte)obj ^ (sbyte)obj2);
				return 1;
			}

			// Token: 0x060014BC RID: 5308 RVA: 0x0003AB77 File Offset: 0x00038D77
			public ExclusiveOrSByte()
			{
			}
		}

		// Token: 0x020002F5 RID: 757
		private sealed class ExclusiveOrInt16 : ExclusiveOrInstruction
		{
			// Token: 0x060014BD RID: 5309 RVA: 0x0003AB80 File Offset: 0x00038D80
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((short)obj ^ (short)obj2);
				return 1;
			}

			// Token: 0x060014BE RID: 5310 RVA: 0x0003AB77 File Offset: 0x00038D77
			public ExclusiveOrInt16()
			{
			}
		}

		// Token: 0x020002F6 RID: 758
		private sealed class ExclusiveOrInt32 : ExclusiveOrInstruction
		{
			// Token: 0x060014BF RID: 5311 RVA: 0x0003ABC0 File Offset: 0x00038DC0
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((int)obj ^ (int)obj2);
				return 1;
			}

			// Token: 0x060014C0 RID: 5312 RVA: 0x0003AB77 File Offset: 0x00038D77
			public ExclusiveOrInt32()
			{
			}
		}

		// Token: 0x020002F7 RID: 759
		private sealed class ExclusiveOrInt64 : ExclusiveOrInstruction
		{
			// Token: 0x060014C1 RID: 5313 RVA: 0x0003AC00 File Offset: 0x00038E00
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((long)obj ^ (long)obj2);
				return 1;
			}

			// Token: 0x060014C2 RID: 5314 RVA: 0x0003AB77 File Offset: 0x00038D77
			public ExclusiveOrInt64()
			{
			}
		}

		// Token: 0x020002F8 RID: 760
		private sealed class ExclusiveOrByte : ExclusiveOrInstruction
		{
			// Token: 0x060014C3 RID: 5315 RVA: 0x0003AC44 File Offset: 0x00038E44
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((byte)obj ^ (byte)obj2);
				return 1;
			}

			// Token: 0x060014C4 RID: 5316 RVA: 0x0003AB77 File Offset: 0x00038D77
			public ExclusiveOrByte()
			{
			}
		}

		// Token: 0x020002F9 RID: 761
		private sealed class ExclusiveOrUInt16 : ExclusiveOrInstruction
		{
			// Token: 0x060014C5 RID: 5317 RVA: 0x0003AC84 File Offset: 0x00038E84
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((ushort)obj ^ (ushort)obj2);
				return 1;
			}

			// Token: 0x060014C6 RID: 5318 RVA: 0x0003AB77 File Offset: 0x00038D77
			public ExclusiveOrUInt16()
			{
			}
		}

		// Token: 0x020002FA RID: 762
		private sealed class ExclusiveOrUInt32 : ExclusiveOrInstruction
		{
			// Token: 0x060014C7 RID: 5319 RVA: 0x0003ACC4 File Offset: 0x00038EC4
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((uint)obj ^ (uint)obj2);
				return 1;
			}

			// Token: 0x060014C8 RID: 5320 RVA: 0x0003AB77 File Offset: 0x00038D77
			public ExclusiveOrUInt32()
			{
			}
		}

		// Token: 0x020002FB RID: 763
		private sealed class ExclusiveOrUInt64 : ExclusiveOrInstruction
		{
			// Token: 0x060014C9 RID: 5321 RVA: 0x0003AD08 File Offset: 0x00038F08
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((ulong)obj ^ (ulong)obj2);
				return 1;
			}

			// Token: 0x060014CA RID: 5322 RVA: 0x0003AB77 File Offset: 0x00038D77
			public ExclusiveOrUInt64()
			{
			}
		}

		// Token: 0x020002FC RID: 764
		private sealed class ExclusiveOrBoolean : ExclusiveOrInstruction
		{
			// Token: 0x060014CB RID: 5323 RVA: 0x0003AD4C File Offset: 0x00038F4C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((bool)obj ^ (bool)obj2);
				return 1;
			}

			// Token: 0x060014CC RID: 5324 RVA: 0x0003AB77 File Offset: 0x00038D77
			public ExclusiveOrBoolean()
			{
			}
		}
	}
}
