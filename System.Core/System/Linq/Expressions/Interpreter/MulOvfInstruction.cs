﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000392 RID: 914
	internal abstract class MulOvfInstruction : Instruction
	{
		// Token: 0x1700049A RID: 1178
		// (get) Token: 0x060017C1 RID: 6081 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x1700049B RID: 1179
		// (get) Token: 0x060017C2 RID: 6082 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x1700049C RID: 1180
		// (get) Token: 0x060017C3 RID: 6083 RVA: 0x00046931 File Offset: 0x00044B31
		public override string InstructionName
		{
			get
			{
				return "MulOvf";
			}
		}

		// Token: 0x060017C4 RID: 6084 RVA: 0x00037C51 File Offset: 0x00035E51
		private MulOvfInstruction()
		{
		}

		// Token: 0x060017C5 RID: 6085 RVA: 0x00046938 File Offset: 0x00044B38
		public static Instruction Create(Type type)
		{
			switch (type.GetNonNullableType().GetTypeCode())
			{
			case TypeCode.Int16:
			{
				Instruction result;
				if ((result = MulOvfInstruction.s_Int16) == null)
				{
					result = (MulOvfInstruction.s_Int16 = new MulOvfInstruction.MulOvfInt16());
				}
				return result;
			}
			case TypeCode.UInt16:
			{
				Instruction result2;
				if ((result2 = MulOvfInstruction.s_UInt16) == null)
				{
					result2 = (MulOvfInstruction.s_UInt16 = new MulOvfInstruction.MulOvfUInt16());
				}
				return result2;
			}
			case TypeCode.Int32:
			{
				Instruction result3;
				if ((result3 = MulOvfInstruction.s_Int32) == null)
				{
					result3 = (MulOvfInstruction.s_Int32 = new MulOvfInstruction.MulOvfInt32());
				}
				return result3;
			}
			case TypeCode.UInt32:
			{
				Instruction result4;
				if ((result4 = MulOvfInstruction.s_UInt32) == null)
				{
					result4 = (MulOvfInstruction.s_UInt32 = new MulOvfInstruction.MulOvfUInt32());
				}
				return result4;
			}
			case TypeCode.Int64:
			{
				Instruction result5;
				if ((result5 = MulOvfInstruction.s_Int64) == null)
				{
					result5 = (MulOvfInstruction.s_Int64 = new MulOvfInstruction.MulOvfInt64());
				}
				return result5;
			}
			case TypeCode.UInt64:
			{
				Instruction result6;
				if ((result6 = MulOvfInstruction.s_UInt64) == null)
				{
					result6 = (MulOvfInstruction.s_UInt64 = new MulOvfInstruction.MulOvfUInt64());
				}
				return result6;
			}
			default:
				return MulInstruction.Create(type);
			}
		}

		// Token: 0x04000B7E RID: 2942
		private static Instruction s_Int16;

		// Token: 0x04000B7F RID: 2943
		private static Instruction s_Int32;

		// Token: 0x04000B80 RID: 2944
		private static Instruction s_Int64;

		// Token: 0x04000B81 RID: 2945
		private static Instruction s_UInt16;

		// Token: 0x04000B82 RID: 2946
		private static Instruction s_UInt32;

		// Token: 0x04000B83 RID: 2947
		private static Instruction s_UInt64;

		// Token: 0x02000393 RID: 915
		private sealed class MulOvfInt16 : MulOvfInstruction
		{
			// Token: 0x060017C6 RID: 6086 RVA: 0x000469F8 File Offset: 0x00044BF8
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : checked((short)obj * (short)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060017C7 RID: 6087 RVA: 0x00046A4A File Offset: 0x00044C4A
			public MulOvfInt16()
			{
			}
		}

		// Token: 0x02000394 RID: 916
		private sealed class MulOvfInt32 : MulOvfInstruction
		{
			// Token: 0x060017C8 RID: 6088 RVA: 0x00046A54 File Offset: 0x00044C54
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ScriptingRuntimeHelpers.Int32ToObject(checked((int)obj * (int)obj2)));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060017C9 RID: 6089 RVA: 0x00046A4A File Offset: 0x00044C4A
			public MulOvfInt32()
			{
			}
		}

		// Token: 0x02000395 RID: 917
		private sealed class MulOvfInt64 : MulOvfInstruction
		{
			// Token: 0x060017CA RID: 6090 RVA: 0x00046AA8 File Offset: 0x00044CA8
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : checked((long)obj * (long)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060017CB RID: 6091 RVA: 0x00046A4A File Offset: 0x00044C4A
			public MulOvfInt64()
			{
			}
		}

		// Token: 0x02000396 RID: 918
		private sealed class MulOvfUInt16 : MulOvfInstruction
		{
			// Token: 0x060017CC RID: 6092 RVA: 0x00046AFC File Offset: 0x00044CFC
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : checked((ushort)obj * (ushort)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060017CD RID: 6093 RVA: 0x00046A4A File Offset: 0x00044C4A
			public MulOvfUInt16()
			{
			}
		}

		// Token: 0x02000397 RID: 919
		private sealed class MulOvfUInt32 : MulOvfInstruction
		{
			// Token: 0x060017CE RID: 6094 RVA: 0x00046B50 File Offset: 0x00044D50
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : checked((uint)obj * (uint)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060017CF RID: 6095 RVA: 0x00046A4A File Offset: 0x00044C4A
			public MulOvfUInt32()
			{
			}
		}

		// Token: 0x02000398 RID: 920
		private sealed class MulOvfUInt64 : MulOvfInstruction
		{
			// Token: 0x060017D0 RID: 6096 RVA: 0x00046BA4 File Offset: 0x00044DA4
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : checked((ulong)obj * (ulong)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x060017D1 RID: 6097 RVA: 0x00046A4A File Offset: 0x00044C4A
			public MulOvfUInt64()
			{
			}
		}
	}
}
