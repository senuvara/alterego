﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000371 RID: 881
	internal sealed class ValueTypeCopyInstruction : Instruction
	{
		// Token: 0x17000480 RID: 1152
		// (get) Token: 0x0600175C RID: 5980 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000481 RID: 1153
		// (get) Token: 0x0600175D RID: 5981 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000482 RID: 1154
		// (get) Token: 0x0600175E RID: 5982 RVA: 0x00045BD7 File Offset: 0x00043DD7
		public override string InstructionName
		{
			get
			{
				return "ValueTypeCopy";
			}
		}

		// Token: 0x0600175F RID: 5983 RVA: 0x00045BE0 File Offset: 0x00043DE0
		public override int Run(InterpretedFrame frame)
		{
			object obj = frame.Pop();
			frame.Push((obj == null) ? obj : RuntimeHelpers.GetObjectValue(obj));
			return 1;
		}

		// Token: 0x06001760 RID: 5984 RVA: 0x00037C51 File Offset: 0x00035E51
		public ValueTypeCopyInstruction()
		{
		}

		// Token: 0x06001761 RID: 5985 RVA: 0x00045C07 File Offset: 0x00043E07
		// Note: this type is marked as 'beforefieldinit'.
		static ValueTypeCopyInstruction()
		{
		}

		// Token: 0x04000B59 RID: 2905
		public static readonly ValueTypeCopyInstruction Instruction = new ValueTypeCopyInstruction();
	}
}
