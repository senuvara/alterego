﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020003E4 RID: 996
	internal sealed class DupInstruction : Instruction
	{
		// Token: 0x060018A1 RID: 6305 RVA: 0x00037C51 File Offset: 0x00035E51
		private DupInstruction()
		{
		}

		// Token: 0x170004C4 RID: 1220
		// (get) Token: 0x060018A2 RID: 6306 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004C5 RID: 1221
		// (get) Token: 0x060018A3 RID: 6307 RVA: 0x00048EBA File Offset: 0x000470BA
		public override string InstructionName
		{
			get
			{
				return "Dup";
			}
		}

		// Token: 0x060018A4 RID: 6308 RVA: 0x00048EC1 File Offset: 0x000470C1
		public override int Run(InterpretedFrame frame)
		{
			frame.Dup();
			return 1;
		}

		// Token: 0x060018A5 RID: 6309 RVA: 0x00048ECA File Offset: 0x000470CA
		// Note: this type is marked as 'beforefieldinit'.
		static DupInstruction()
		{
		}

		// Token: 0x04000BC9 RID: 3017
		internal static readonly DupInstruction Instance = new DupInstruction();
	}
}
