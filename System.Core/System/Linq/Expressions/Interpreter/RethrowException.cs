﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000355 RID: 853
	internal sealed class RethrowException : Exception
	{
		// Token: 0x06001653 RID: 5715 RVA: 0x00016BBA File Offset: 0x00014DBA
		public RethrowException()
		{
		}
	}
}
