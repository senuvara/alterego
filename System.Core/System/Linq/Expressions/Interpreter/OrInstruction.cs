﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020003CD RID: 973
	internal abstract class OrInstruction : Instruction
	{
		// Token: 0x170004B6 RID: 1206
		// (get) Token: 0x06001860 RID: 6240 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x170004B7 RID: 1207
		// (get) Token: 0x06001861 RID: 6241 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170004B8 RID: 1208
		// (get) Token: 0x06001862 RID: 6242 RVA: 0x0004869B File Offset: 0x0004689B
		public override string InstructionName
		{
			get
			{
				return "Or";
			}
		}

		// Token: 0x06001863 RID: 6243 RVA: 0x00037C51 File Offset: 0x00035E51
		private OrInstruction()
		{
		}

		// Token: 0x06001864 RID: 6244 RVA: 0x000486A4 File Offset: 0x000468A4
		public static Instruction Create(Type type)
		{
			switch (type.GetNonNullableType().GetTypeCode())
			{
			case TypeCode.Boolean:
			{
				Instruction result;
				if ((result = OrInstruction.s_Boolean) == null)
				{
					result = (OrInstruction.s_Boolean = new OrInstruction.OrBoolean());
				}
				return result;
			}
			case TypeCode.SByte:
			{
				Instruction result2;
				if ((result2 = OrInstruction.s_SByte) == null)
				{
					result2 = (OrInstruction.s_SByte = new OrInstruction.OrSByte());
				}
				return result2;
			}
			case TypeCode.Byte:
			{
				Instruction result3;
				if ((result3 = OrInstruction.s_Byte) == null)
				{
					result3 = (OrInstruction.s_Byte = new OrInstruction.OrByte());
				}
				return result3;
			}
			case TypeCode.Int16:
			{
				Instruction result4;
				if ((result4 = OrInstruction.s_Int16) == null)
				{
					result4 = (OrInstruction.s_Int16 = new OrInstruction.OrInt16());
				}
				return result4;
			}
			case TypeCode.UInt16:
			{
				Instruction result5;
				if ((result5 = OrInstruction.s_UInt16) == null)
				{
					result5 = (OrInstruction.s_UInt16 = new OrInstruction.OrUInt16());
				}
				return result5;
			}
			case TypeCode.Int32:
			{
				Instruction result6;
				if ((result6 = OrInstruction.s_Int32) == null)
				{
					result6 = (OrInstruction.s_Int32 = new OrInstruction.OrInt32());
				}
				return result6;
			}
			case TypeCode.UInt32:
			{
				Instruction result7;
				if ((result7 = OrInstruction.s_UInt32) == null)
				{
					result7 = (OrInstruction.s_UInt32 = new OrInstruction.OrUInt32());
				}
				return result7;
			}
			case TypeCode.Int64:
			{
				Instruction result8;
				if ((result8 = OrInstruction.s_Int64) == null)
				{
					result8 = (OrInstruction.s_Int64 = new OrInstruction.OrInt64());
				}
				return result8;
			}
			case TypeCode.UInt64:
			{
				Instruction result9;
				if ((result9 = OrInstruction.s_UInt64) == null)
				{
					result9 = (OrInstruction.s_UInt64 = new OrInstruction.OrUInt64());
				}
				return result9;
			}
			}
			throw ContractUtils.Unreachable;
		}

		// Token: 0x04000BB4 RID: 2996
		private static Instruction s_SByte;

		// Token: 0x04000BB5 RID: 2997
		private static Instruction s_Int16;

		// Token: 0x04000BB6 RID: 2998
		private static Instruction s_Int32;

		// Token: 0x04000BB7 RID: 2999
		private static Instruction s_Int64;

		// Token: 0x04000BB8 RID: 3000
		private static Instruction s_Byte;

		// Token: 0x04000BB9 RID: 3001
		private static Instruction s_UInt16;

		// Token: 0x04000BBA RID: 3002
		private static Instruction s_UInt32;

		// Token: 0x04000BBB RID: 3003
		private static Instruction s_UInt64;

		// Token: 0x04000BBC RID: 3004
		private static Instruction s_Boolean;

		// Token: 0x020003CE RID: 974
		private sealed class OrSByte : OrInstruction
		{
			// Token: 0x06001865 RID: 6245 RVA: 0x000487B4 File Offset: 0x000469B4
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((sbyte)obj | (sbyte)obj2);
				return 1;
			}

			// Token: 0x06001866 RID: 6246 RVA: 0x000487F3 File Offset: 0x000469F3
			public OrSByte()
			{
			}
		}

		// Token: 0x020003CF RID: 975
		private sealed class OrInt16 : OrInstruction
		{
			// Token: 0x06001867 RID: 6247 RVA: 0x000487FC File Offset: 0x000469FC
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((short)obj | (short)obj2);
				return 1;
			}

			// Token: 0x06001868 RID: 6248 RVA: 0x000487F3 File Offset: 0x000469F3
			public OrInt16()
			{
			}
		}

		// Token: 0x020003D0 RID: 976
		private sealed class OrInt32 : OrInstruction
		{
			// Token: 0x06001869 RID: 6249 RVA: 0x0004883C File Offset: 0x00046A3C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((int)obj | (int)obj2);
				return 1;
			}

			// Token: 0x0600186A RID: 6250 RVA: 0x000487F3 File Offset: 0x000469F3
			public OrInt32()
			{
			}
		}

		// Token: 0x020003D1 RID: 977
		private sealed class OrInt64 : OrInstruction
		{
			// Token: 0x0600186B RID: 6251 RVA: 0x0004887C File Offset: 0x00046A7C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((long)obj | (long)obj2);
				return 1;
			}

			// Token: 0x0600186C RID: 6252 RVA: 0x000487F3 File Offset: 0x000469F3
			public OrInt64()
			{
			}
		}

		// Token: 0x020003D2 RID: 978
		private sealed class OrByte : OrInstruction
		{
			// Token: 0x0600186D RID: 6253 RVA: 0x000488C0 File Offset: 0x00046AC0
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((byte)obj | (byte)obj2);
				return 1;
			}

			// Token: 0x0600186E RID: 6254 RVA: 0x000487F3 File Offset: 0x000469F3
			public OrByte()
			{
			}
		}

		// Token: 0x020003D3 RID: 979
		private sealed class OrUInt16 : OrInstruction
		{
			// Token: 0x0600186F RID: 6255 RVA: 0x00048900 File Offset: 0x00046B00
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((ushort)obj | (ushort)obj2);
				return 1;
			}

			// Token: 0x06001870 RID: 6256 RVA: 0x000487F3 File Offset: 0x000469F3
			public OrUInt16()
			{
			}
		}

		// Token: 0x020003D4 RID: 980
		private sealed class OrUInt32 : OrInstruction
		{
			// Token: 0x06001871 RID: 6257 RVA: 0x00048940 File Offset: 0x00046B40
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((uint)obj | (uint)obj2);
				return 1;
			}

			// Token: 0x06001872 RID: 6258 RVA: 0x000487F3 File Offset: 0x000469F3
			public OrUInt32()
			{
			}
		}

		// Token: 0x020003D5 RID: 981
		private sealed class OrUInt64 : OrInstruction
		{
			// Token: 0x06001873 RID: 6259 RVA: 0x00048984 File Offset: 0x00046B84
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj == null || obj2 == null)
				{
					frame.Push(null);
					return 1;
				}
				frame.Push((ulong)obj | (ulong)obj2);
				return 1;
			}

			// Token: 0x06001874 RID: 6260 RVA: 0x000487F3 File Offset: 0x000469F3
			public OrUInt64()
			{
			}
		}

		// Token: 0x020003D6 RID: 982
		private sealed class OrBoolean : OrInstruction
		{
			// Token: 0x06001875 RID: 6261 RVA: 0x000489C8 File Offset: 0x00046BC8
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null)
				{
					if (obj == null)
					{
						frame.Push(null);
					}
					else
					{
						frame.Push(((bool)obj) ? Utils.BoxedTrue : null);
					}
					return 1;
				}
				if (obj == null)
				{
					frame.Push(((bool)obj2) ? Utils.BoxedTrue : null);
					return 1;
				}
				frame.Push((bool)obj2 | (bool)obj);
				return 1;
			}

			// Token: 0x06001876 RID: 6262 RVA: 0x000487F3 File Offset: 0x000469F3
			public OrBoolean()
			{
			}
		}
	}
}
