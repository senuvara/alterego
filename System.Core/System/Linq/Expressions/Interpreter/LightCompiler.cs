﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000359 RID: 857
	internal sealed class LightCompiler
	{
		// Token: 0x0600165C RID: 5724 RVA: 0x0003EBA0 File Offset: 0x0003CDA0
		public LightCompiler()
		{
			this._instructions = new InstructionList();
		}

		// Token: 0x0600165D RID: 5725 RVA: 0x0003EC02 File Offset: 0x0003CE02
		private LightCompiler(LightCompiler parent) : this()
		{
			this._parent = parent;
		}

		// Token: 0x17000468 RID: 1128
		// (get) Token: 0x0600165E RID: 5726 RVA: 0x0003EC11 File Offset: 0x0003CE11
		public InstructionList Instructions
		{
			get
			{
				return this._instructions;
			}
		}

		// Token: 0x0600165F RID: 5727 RVA: 0x0003EC1C File Offset: 0x0003CE1C
		public LightDelegateCreator CompileTop(LambdaExpression node)
		{
			int i = 0;
			int parameterCount = node.ParameterCount;
			while (i < parameterCount)
			{
				ParameterExpression parameter = node.GetParameter(i);
				LocalDefinition localDefinition = this._locals.DefineLocal(parameter, 0);
				this._instructions.EmitInitializeParameter(localDefinition.Index);
				i++;
			}
			this.Compile(node.Body);
			if (node.Body.Type != typeof(void) && node.ReturnType == typeof(void))
			{
				this._instructions.EmitPop();
			}
			return new LightDelegateCreator(this.MakeInterpreter(node.Name), node);
		}

		// Token: 0x06001660 RID: 5728 RVA: 0x0003ECC0 File Offset: 0x0003CEC0
		private Interpreter MakeInterpreter(string lambdaName)
		{
			DebugInfo[] debugInfos = this._debugInfos.ToArray();
			foreach (KeyValuePair<LabelTarget, LabelInfo> keyValuePair in this._treeLabels)
			{
				keyValuePair.Value.ValidateFinish();
			}
			return new Interpreter(lambdaName, this._locals, this._instructions.ToArray(), debugInfos);
		}

		// Token: 0x06001661 RID: 5729 RVA: 0x0003ED38 File Offset: 0x0003CF38
		private void CompileConstantExpression(Expression expr)
		{
			ConstantExpression constantExpression = (ConstantExpression)expr;
			this._instructions.EmitLoad(constantExpression.Value, constantExpression.Type);
		}

		// Token: 0x06001662 RID: 5730 RVA: 0x0003ED63 File Offset: 0x0003CF63
		private void CompileDefaultExpression(Expression expr)
		{
			this.CompileDefaultExpression(expr.Type);
		}

		// Token: 0x06001663 RID: 5731 RVA: 0x0003ED74 File Offset: 0x0003CF74
		private void CompileDefaultExpression(Type type)
		{
			if (type != typeof(void))
			{
				if (type.IsNullableOrReferenceType())
				{
					this._instructions.EmitLoad(null);
					return;
				}
				object primitiveDefaultValue = ScriptingRuntimeHelpers.GetPrimitiveDefaultValue(type);
				if (primitiveDefaultValue != null)
				{
					this._instructions.EmitLoad(primitiveDefaultValue);
					return;
				}
				this._instructions.EmitDefaultValue(type);
			}
		}

		// Token: 0x06001664 RID: 5732 RVA: 0x0003EDCC File Offset: 0x0003CFCC
		private LocalVariable EnsureAvailableForClosure(ParameterExpression expr)
		{
			LocalVariable localVariable;
			if (this._locals.TryGetLocalOrClosure(expr, out localVariable))
			{
				if (!localVariable.InClosure && !localVariable.IsBoxed)
				{
					this._locals.Box(expr, this._instructions);
				}
				return localVariable;
			}
			if (this._parent != null)
			{
				this._parent.EnsureAvailableForClosure(expr);
				return this._locals.AddClosureVariable(expr);
			}
			throw new InvalidOperationException("unbound variable: " + expr);
		}

		// Token: 0x06001665 RID: 5733 RVA: 0x0003EE40 File Offset: 0x0003D040
		private LocalVariable ResolveLocal(ParameterExpression variable)
		{
			LocalVariable result;
			if (!this._locals.TryGetLocalOrClosure(variable, out result))
			{
				result = this.EnsureAvailableForClosure(variable);
			}
			return result;
		}

		// Token: 0x06001666 RID: 5734 RVA: 0x0003EE66 File Offset: 0x0003D066
		private void CompileGetVariable(ParameterExpression variable)
		{
			this.LoadLocalNoValueTypeCopy(variable);
			this.EmitCopyValueType(variable.Type);
		}

		// Token: 0x06001667 RID: 5735 RVA: 0x0003EE7B File Offset: 0x0003D07B
		private void EmitCopyValueType(Type valueType)
		{
			if (this.MaybeMutableValueType(valueType))
			{
				this._instructions.Emit(ValueTypeCopyInstruction.Instruction);
			}
		}

		// Token: 0x06001668 RID: 5736 RVA: 0x0003EE98 File Offset: 0x0003D098
		private void LoadLocalNoValueTypeCopy(ParameterExpression variable)
		{
			LocalVariable localVariable = this.ResolveLocal(variable);
			if (localVariable.InClosure)
			{
				this._instructions.EmitLoadLocalFromClosure(localVariable.Index);
				return;
			}
			if (localVariable.IsBoxed)
			{
				this._instructions.EmitLoadLocalBoxed(localVariable.Index);
				return;
			}
			this._instructions.EmitLoadLocal(localVariable.Index);
		}

		// Token: 0x06001669 RID: 5737 RVA: 0x0003EEF2 File Offset: 0x0003D0F2
		private bool MaybeMutableValueType(Type type)
		{
			return type.IsValueType && !type.IsEnum && !type.IsPrimitive;
		}

		// Token: 0x0600166A RID: 5738 RVA: 0x0003EF10 File Offset: 0x0003D110
		private void CompileGetBoxedVariable(ParameterExpression variable)
		{
			LocalVariable localVariable = this.ResolveLocal(variable);
			if (localVariable.InClosure)
			{
				this._instructions.EmitLoadLocalFromClosureBoxed(localVariable.Index);
				return;
			}
			this._instructions.EmitLoadLocal(localVariable.Index);
		}

		// Token: 0x0600166B RID: 5739 RVA: 0x0003EF50 File Offset: 0x0003D150
		private void CompileSetVariable(ParameterExpression variable, bool isVoid)
		{
			LocalVariable localVariable = this.ResolveLocal(variable);
			if (localVariable.InClosure)
			{
				if (isVoid)
				{
					this._instructions.EmitStoreLocalToClosure(localVariable.Index);
					return;
				}
				this._instructions.EmitAssignLocalToClosure(localVariable.Index);
				return;
			}
			else if (localVariable.IsBoxed)
			{
				if (isVoid)
				{
					this._instructions.EmitStoreLocalBoxed(localVariable.Index);
					return;
				}
				this._instructions.EmitAssignLocalBoxed(localVariable.Index);
				return;
			}
			else
			{
				if (isVoid)
				{
					this._instructions.EmitStoreLocal(localVariable.Index);
					return;
				}
				this._instructions.EmitAssignLocal(localVariable.Index);
				return;
			}
		}

		// Token: 0x0600166C RID: 5740 RVA: 0x0003EFEC File Offset: 0x0003D1EC
		private void CompileParameterExpression(Expression expr)
		{
			ParameterExpression variable = (ParameterExpression)expr;
			this.CompileGetVariable(variable);
		}

		// Token: 0x0600166D RID: 5741 RVA: 0x0003F008 File Offset: 0x0003D208
		private void CompileBlockExpression(Expression expr, bool asVoid)
		{
			BlockExpression blockExpression = (BlockExpression)expr;
			if (blockExpression.ExpressionCount != 0)
			{
				LocalDefinition[] locals = this.CompileBlockStart(blockExpression);
				Expression expr2 = blockExpression.Expressions[blockExpression.Expressions.Count - 1];
				this.Compile(expr2, asVoid);
				this.CompileBlockEnd(locals);
			}
		}

		// Token: 0x0600166E RID: 5742 RVA: 0x0003F054 File Offset: 0x0003D254
		private LocalDefinition[] CompileBlockStart(BlockExpression node)
		{
			int count = this._instructions.Count;
			ReadOnlyCollection<ParameterExpression> variables = node.Variables;
			LocalDefinition[] array;
			if (variables.Count != 0)
			{
				array = new LocalDefinition[variables.Count];
				int num = 0;
				using (IEnumerator<ParameterExpression> enumerator = variables.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						ParameterExpression parameterExpression = enumerator.Current;
						LocalDefinition localDefinition = this._locals.DefineLocal(parameterExpression, count);
						array[num++] = localDefinition;
						this._instructions.EmitInitializeLocal(localDefinition.Index, parameterExpression.Type);
					}
					goto IL_8F;
				}
			}
			array = LightCompiler.s_emptyLocals;
			IL_8F:
			for (int i = 0; i < node.Expressions.Count - 1; i++)
			{
				this.CompileAsVoid(node.Expressions[i]);
			}
			return array;
		}

		// Token: 0x0600166F RID: 5743 RVA: 0x0003F130 File Offset: 0x0003D330
		private void CompileBlockEnd(LocalDefinition[] locals)
		{
			foreach (LocalDefinition definition in locals)
			{
				this._locals.UndefineLocal(definition, this._instructions.Count);
			}
		}

		// Token: 0x06001670 RID: 5744 RVA: 0x0003F16C File Offset: 0x0003D36C
		private void CompileIndexExpression(Expression expr)
		{
			IndexExpression indexExpression = (IndexExpression)expr;
			if (indexExpression.Object != null)
			{
				this.EmitThisForMethodCall(indexExpression.Object);
			}
			int i = 0;
			int argumentCount = indexExpression.ArgumentCount;
			while (i < argumentCount)
			{
				this.Compile(indexExpression.GetArgument(i));
				i++;
			}
			this.EmitIndexGet(indexExpression);
		}

		// Token: 0x06001671 RID: 5745 RVA: 0x0003F1BC File Offset: 0x0003D3BC
		private void EmitIndexGet(IndexExpression index)
		{
			if (index.Indexer != null)
			{
				this._instructions.EmitCall(index.Indexer.GetGetMethod(true));
				return;
			}
			if (index.ArgumentCount != 1)
			{
				this._instructions.EmitCall(index.Object.Type.GetMethod("Get", BindingFlags.Instance | BindingFlags.Public));
				return;
			}
			this._instructions.EmitGetArrayItem();
		}

		// Token: 0x06001672 RID: 5746 RVA: 0x0003F228 File Offset: 0x0003D428
		private void CompileIndexAssignment(BinaryExpression node, bool asVoid)
		{
			IndexExpression indexExpression = (IndexExpression)node.Left;
			if (indexExpression.Object != null)
			{
				this.EmitThisForMethodCall(indexExpression.Object);
			}
			int i = 0;
			int argumentCount = indexExpression.ArgumentCount;
			while (i < argumentCount)
			{
				this.Compile(indexExpression.GetArgument(i));
				i++;
			}
			this.Compile(node.Right);
			LocalDefinition definition = default(LocalDefinition);
			if (!asVoid)
			{
				definition = this._locals.DefineLocal(Expression.Parameter(node.Right.Type), this._instructions.Count);
				this._instructions.EmitAssignLocal(definition.Index);
			}
			if (indexExpression.Indexer != null)
			{
				this._instructions.EmitCall(indexExpression.Indexer.GetSetMethod(true));
			}
			else if (indexExpression.ArgumentCount != 1)
			{
				this._instructions.EmitCall(indexExpression.Object.Type.GetMethod("Set", BindingFlags.Instance | BindingFlags.Public));
			}
			else
			{
				this._instructions.EmitSetArrayItem();
			}
			if (!asVoid)
			{
				this._instructions.EmitLoadLocal(definition.Index);
				this._locals.UndefineLocal(definition, this._instructions.Count);
			}
		}

		// Token: 0x06001673 RID: 5747 RVA: 0x0003F350 File Offset: 0x0003D550
		private void CompileMemberAssignment(BinaryExpression node, bool asVoid)
		{
			MemberExpression memberExpression = (MemberExpression)node.Left;
			Expression expression = memberExpression.Expression;
			if (expression != null)
			{
				this.EmitThisForMethodCall(expression);
			}
			this.CompileMemberAssignment(asVoid, memberExpression.Member, node.Right, false);
		}

		// Token: 0x06001674 RID: 5748 RVA: 0x0003F390 File Offset: 0x0003D590
		private void CompileMemberAssignment(bool asVoid, MemberInfo refMember, Expression value, bool forBinding)
		{
			PropertyInfo propertyInfo = refMember as PropertyInfo;
			if (propertyInfo != null)
			{
				MethodInfo setMethod = propertyInfo.GetSetMethod(true);
				if (forBinding && setMethod.IsStatic)
				{
					throw Error.InvalidProgram();
				}
				this.EmitThisForMethodCall(value);
				int count = this._instructions.Count;
				if (!asVoid)
				{
					LocalDefinition definition = this._locals.DefineLocal(Expression.Parameter(value.Type), count);
					this._instructions.EmitAssignLocal(definition.Index);
					this._instructions.EmitCall(setMethod);
					this._instructions.EmitLoadLocal(definition.Index);
					this._locals.UndefineLocal(definition, this._instructions.Count);
					return;
				}
				this._instructions.EmitCall(setMethod);
				return;
			}
			else
			{
				FieldInfo fieldInfo = (FieldInfo)refMember;
				if (fieldInfo.IsLiteral)
				{
					throw Error.NotSupported();
				}
				if (forBinding && fieldInfo.IsStatic)
				{
					this._instructions.UnEmit();
				}
				this.EmitThisForMethodCall(value);
				int count2 = this._instructions.Count;
				if (!asVoid)
				{
					LocalDefinition definition2 = this._locals.DefineLocal(Expression.Parameter(value.Type), count2);
					this._instructions.EmitAssignLocal(definition2.Index);
					this._instructions.EmitStoreField(fieldInfo);
					this._instructions.EmitLoadLocal(definition2.Index);
					this._locals.UndefineLocal(definition2, this._instructions.Count);
					return;
				}
				this._instructions.EmitStoreField(fieldInfo);
				return;
			}
		}

		// Token: 0x06001675 RID: 5749 RVA: 0x0003F504 File Offset: 0x0003D704
		private void CompileVariableAssignment(BinaryExpression node, bool asVoid)
		{
			this.Compile(node.Right);
			ParameterExpression variable = (ParameterExpression)node.Left;
			this.CompileSetVariable(variable, asVoid);
		}

		// Token: 0x06001676 RID: 5750 RVA: 0x0003F534 File Offset: 0x0003D734
		private void CompileAssignBinaryExpression(Expression expr, bool asVoid)
		{
			BinaryExpression binaryExpression = (BinaryExpression)expr;
			ExpressionType nodeType = binaryExpression.Left.NodeType;
			if (nodeType <= ExpressionType.Parameter)
			{
				if (nodeType == ExpressionType.MemberAccess)
				{
					this.CompileMemberAssignment(binaryExpression, asVoid);
					return;
				}
				if (nodeType != ExpressionType.Parameter)
				{
					goto IL_49;
				}
			}
			else if (nodeType != ExpressionType.Extension)
			{
				if (nodeType == ExpressionType.Index)
				{
					this.CompileIndexAssignment(binaryExpression, asVoid);
					return;
				}
				goto IL_49;
			}
			this.CompileVariableAssignment(binaryExpression, asVoid);
			return;
			IL_49:
			throw Error.InvalidLvalue(binaryExpression.Left.NodeType);
		}

		// Token: 0x06001677 RID: 5751 RVA: 0x0003F59C File Offset: 0x0003D79C
		private void CompileBinaryExpression(Expression expr)
		{
			BinaryExpression binaryExpression = (BinaryExpression)expr;
			if (!(binaryExpression.Method != null))
			{
				ExpressionType nodeType = binaryExpression.NodeType;
				switch (nodeType)
				{
				case ExpressionType.Add:
				case ExpressionType.AddChecked:
				case ExpressionType.Divide:
				case ExpressionType.Modulo:
				case ExpressionType.Multiply:
				case ExpressionType.MultiplyChecked:
					break;
				case ExpressionType.And:
					this.Compile(binaryExpression.Left);
					this.Compile(binaryExpression.Right);
					this._instructions.EmitAnd(binaryExpression.Left.Type);
					return;
				case ExpressionType.AndAlso:
				case ExpressionType.ArrayLength:
				case ExpressionType.Call:
				case ExpressionType.Coalesce:
				case ExpressionType.Conditional:
				case ExpressionType.Constant:
				case ExpressionType.Convert:
				case ExpressionType.ConvertChecked:
				case ExpressionType.Invoke:
				case ExpressionType.Lambda:
				case ExpressionType.ListInit:
				case ExpressionType.MemberAccess:
				case ExpressionType.MemberInit:
					goto IL_668;
				case ExpressionType.ArrayIndex:
					this.Compile(binaryExpression.Left);
					this.Compile(binaryExpression.Right);
					this._instructions.EmitGetArrayItem();
					return;
				case ExpressionType.Equal:
					this.CompileEqual(binaryExpression.Left, binaryExpression.Right, binaryExpression.IsLiftedToNull);
					return;
				case ExpressionType.ExclusiveOr:
					this.Compile(binaryExpression.Left);
					this.Compile(binaryExpression.Right);
					this._instructions.EmitExclusiveOr(binaryExpression.Left.Type);
					return;
				case ExpressionType.GreaterThan:
				case ExpressionType.GreaterThanOrEqual:
				case ExpressionType.LessThan:
				case ExpressionType.LessThanOrEqual:
					this.CompileComparison(binaryExpression);
					return;
				case ExpressionType.LeftShift:
					this.Compile(binaryExpression.Left);
					this.Compile(binaryExpression.Right);
					this._instructions.EmitLeftShift(binaryExpression.Left.Type);
					return;
				default:
					switch (nodeType)
					{
					case ExpressionType.NotEqual:
						this.CompileNotEqual(binaryExpression.Left, binaryExpression.Right, binaryExpression.IsLiftedToNull);
						return;
					case ExpressionType.Or:
						this.Compile(binaryExpression.Left);
						this.Compile(binaryExpression.Right);
						this._instructions.EmitOr(binaryExpression.Left.Type);
						return;
					case ExpressionType.OrElse:
					case ExpressionType.Parameter:
					case ExpressionType.Power:
					case ExpressionType.Quote:
						goto IL_668;
					case ExpressionType.RightShift:
						this.Compile(binaryExpression.Left);
						this.Compile(binaryExpression.Right);
						this._instructions.EmitRightShift(binaryExpression.Left.Type);
						return;
					case ExpressionType.Subtract:
					case ExpressionType.SubtractChecked:
						break;
					default:
						goto IL_668;
					}
					break;
				}
				this.CompileArithmetic(binaryExpression.NodeType, binaryExpression.Left, binaryExpression.Right);
				return;
				IL_668:
				throw new PlatformNotSupportedException(SR.Format("The expression type '{0}' is not supported", binaryExpression.NodeType));
			}
			if (binaryExpression.IsLifted)
			{
				BranchLabel label = this._instructions.MakeLabel();
				LocalDefinition definition = this._locals.DefineLocal(Expression.Parameter(binaryExpression.Left.Type), this._instructions.Count);
				this.Compile(binaryExpression.Left);
				this._instructions.EmitStoreLocal(definition.Index);
				LocalDefinition definition2 = this._locals.DefineLocal(Expression.Parameter(binaryExpression.Right.Type), this._instructions.Count);
				this.Compile(binaryExpression.Right);
				this._instructions.EmitStoreLocal(definition2.Index);
				ExpressionType nodeType = binaryExpression.NodeType;
				if ((nodeType == ExpressionType.Equal || nodeType == ExpressionType.NotEqual) && !binaryExpression.IsLiftedToNull)
				{
					BranchLabel branchLabel = this._instructions.MakeLabel();
					BranchLabel branchLabel2 = this._instructions.MakeLabel();
					this._instructions.EmitLoadLocal(definition.Index);
					this._instructions.EmitLoad(null, typeof(object));
					this._instructions.EmitEqual(typeof(object), false);
					this._instructions.EmitBranchFalse(branchLabel);
					this._instructions.EmitLoadLocal(definition2.Index);
					this._instructions.EmitLoad(null, typeof(object));
					if (binaryExpression.NodeType == ExpressionType.Equal)
					{
						this._instructions.EmitEqual(typeof(object), false);
					}
					else
					{
						this._instructions.EmitNotEqual(typeof(object), false);
					}
					this._instructions.EmitBranch(label, false, true);
					this._instructions.MarkLabel(branchLabel);
					this._instructions.EmitLoadLocal(definition2.Index);
					this._instructions.EmitLoad(null, typeof(object));
					this._instructions.EmitEqual(typeof(object), false);
					this._instructions.EmitBranchFalse(branchLabel2);
					this._instructions.EmitLoad((binaryExpression.NodeType == ExpressionType.Equal) ? Utils.BoxedFalse : Utils.BoxedTrue, typeof(bool));
					this._instructions.EmitBranch(label, false, true);
					this._instructions.MarkLabel(branchLabel2);
					this._instructions.EmitLoadLocal(definition.Index);
					this._instructions.EmitLoadLocal(definition2.Index);
					this._instructions.EmitCall(binaryExpression.Method);
				}
				else
				{
					BranchLabel branchLabel3 = this._instructions.MakeLabel();
					if (binaryExpression.Left.Type.IsNullableOrReferenceType())
					{
						this._instructions.EmitLoadLocal(definition.Index);
						this._instructions.EmitLoad(null, typeof(object));
						this._instructions.EmitEqual(typeof(object), false);
						this._instructions.EmitBranchTrue(branchLabel3);
					}
					if (binaryExpression.Right.Type.IsNullableOrReferenceType())
					{
						this._instructions.EmitLoadLocal(definition2.Index);
						this._instructions.EmitLoad(null, typeof(object));
						this._instructions.EmitEqual(typeof(object), false);
						this._instructions.EmitBranchTrue(branchLabel3);
					}
					this._instructions.EmitLoadLocal(definition.Index);
					this._instructions.EmitLoadLocal(definition2.Index);
					this._instructions.EmitCall(binaryExpression.Method);
					this._instructions.EmitBranch(label, false, true);
					this._instructions.MarkLabel(branchLabel3);
					nodeType = binaryExpression.NodeType;
					if ((nodeType - ExpressionType.GreaterThan <= 1 || nodeType - ExpressionType.LessThan <= 1) && !binaryExpression.IsLiftedToNull)
					{
						this._instructions.EmitLoad(Utils.BoxedFalse, typeof(object));
					}
					else
					{
						this._instructions.EmitLoad(null, typeof(object));
					}
				}
				this._instructions.MarkLabel(label);
				this._locals.UndefineLocal(definition, this._instructions.Count);
				this._locals.UndefineLocal(definition2, this._instructions.Count);
				return;
			}
			this.Compile(binaryExpression.Left);
			this.Compile(binaryExpression.Right);
			this._instructions.EmitCall(binaryExpression.Method);
		}

		// Token: 0x06001678 RID: 5752 RVA: 0x0003FC2B File Offset: 0x0003DE2B
		private void CompileEqual(Expression left, Expression right, bool liftedToNull)
		{
			this.Compile(left);
			this.Compile(right);
			this._instructions.EmitEqual(left.Type, liftedToNull);
		}

		// Token: 0x06001679 RID: 5753 RVA: 0x0003FC4D File Offset: 0x0003DE4D
		private void CompileNotEqual(Expression left, Expression right, bool liftedToNull)
		{
			this.Compile(left);
			this.Compile(right);
			this._instructions.EmitNotEqual(left.Type, liftedToNull);
		}

		// Token: 0x0600167A RID: 5754 RVA: 0x0003FC70 File Offset: 0x0003DE70
		private void CompileComparison(BinaryExpression node)
		{
			Expression left = node.Left;
			Expression right = node.Right;
			this.Compile(left);
			this.Compile(right);
			switch (node.NodeType)
			{
			case ExpressionType.GreaterThan:
				this._instructions.EmitGreaterThan(left.Type, node.IsLiftedToNull);
				return;
			case ExpressionType.GreaterThanOrEqual:
				this._instructions.EmitGreaterThanOrEqual(left.Type, node.IsLiftedToNull);
				return;
			case ExpressionType.LessThan:
				this._instructions.EmitLessThan(left.Type, node.IsLiftedToNull);
				return;
			case ExpressionType.LessThanOrEqual:
				this._instructions.EmitLessThanOrEqual(left.Type, node.IsLiftedToNull);
				return;
			}
			throw ContractUtils.Unreachable;
		}

		// Token: 0x0600167B RID: 5755 RVA: 0x0003FD2C File Offset: 0x0003DF2C
		private void CompileArithmetic(ExpressionType nodeType, Expression left, Expression right)
		{
			this.Compile(left);
			this.Compile(right);
			if (nodeType <= ExpressionType.Divide)
			{
				if (nodeType == ExpressionType.Add)
				{
					this._instructions.EmitAdd(left.Type, false);
					return;
				}
				if (nodeType == ExpressionType.AddChecked)
				{
					this._instructions.EmitAdd(left.Type, true);
					return;
				}
				if (nodeType == ExpressionType.Divide)
				{
					this._instructions.EmitDiv(left.Type);
					return;
				}
			}
			else
			{
				switch (nodeType)
				{
				case ExpressionType.Modulo:
					this._instructions.EmitModulo(left.Type);
					return;
				case ExpressionType.Multiply:
					this._instructions.EmitMul(left.Type, false);
					return;
				case ExpressionType.MultiplyChecked:
					this._instructions.EmitMul(left.Type, true);
					return;
				default:
					if (nodeType == ExpressionType.Subtract)
					{
						this._instructions.EmitSub(left.Type, false);
						return;
					}
					if (nodeType == ExpressionType.SubtractChecked)
					{
						this._instructions.EmitSub(left.Type, true);
						return;
					}
					break;
				}
			}
			throw ContractUtils.Unreachable;
		}

		// Token: 0x0600167C RID: 5756 RVA: 0x0003FE20 File Offset: 0x0003E020
		private void CompileConvertUnaryExpression(Expression expr)
		{
			UnaryExpression unaryExpression = (UnaryExpression)expr;
			if (unaryExpression.Method != null)
			{
				BranchLabel label = this._instructions.MakeLabel();
				BranchLabel branchLabel = this._instructions.MakeLabel();
				LocalDefinition definition = this._locals.DefineLocal(Expression.Parameter(unaryExpression.Operand.Type), this._instructions.Count);
				this.Compile(unaryExpression.Operand);
				this._instructions.EmitStoreLocal(definition.Index);
				if (!unaryExpression.Operand.Type.IsValueType || (unaryExpression.Operand.Type.IsNullableType() && unaryExpression.IsLiftedToNull))
				{
					this._instructions.EmitLoadLocal(definition.Index);
					this._instructions.EmitLoad(null, typeof(object));
					this._instructions.EmitEqual(typeof(object), false);
					this._instructions.EmitBranchTrue(branchLabel);
				}
				this._instructions.EmitLoadLocal(definition.Index);
				if (unaryExpression.Operand.Type.IsNullableType() && unaryExpression.Method.GetParametersCached()[0].ParameterType.Equals(unaryExpression.Operand.Type.GetNonNullableType()))
				{
					this._instructions.Emit(NullableMethodCallInstruction.CreateGetValue());
				}
				this._instructions.EmitCall(unaryExpression.Method);
				this._instructions.EmitBranch(label, false, true);
				this._instructions.MarkLabel(branchLabel);
				this._instructions.EmitLoad(null, typeof(object));
				this._instructions.MarkLabel(label);
				this._locals.UndefineLocal(definition, this._instructions.Count);
				return;
			}
			if (unaryExpression.Type == typeof(void))
			{
				this.CompileAsVoid(unaryExpression.Operand);
				return;
			}
			this.Compile(unaryExpression.Operand);
			this.CompileConvertToType(unaryExpression.Operand.Type, unaryExpression.Type, unaryExpression.NodeType == ExpressionType.ConvertChecked, unaryExpression.IsLiftedToNull);
		}

		// Token: 0x0600167D RID: 5757 RVA: 0x00040034 File Offset: 0x0003E234
		private void CompileConvertToType(Type typeFrom, Type typeTo, bool isChecked, bool isLiftedToNull)
		{
			if (typeTo.Equals(typeFrom))
			{
				return;
			}
			if (typeFrom.IsValueType && typeTo.IsNullableType() && typeTo.GetNonNullableType().Equals(typeFrom))
			{
				return;
			}
			if (typeTo.IsValueType && typeFrom.IsNullableType() && typeFrom.GetNonNullableType().Equals(typeTo))
			{
				this._instructions.Emit(NullableMethodCallInstruction.CreateGetValue());
				return;
			}
			Type type = typeFrom.GetNonNullableType();
			Type type2 = typeTo.GetNonNullableType();
			if ((type.IsNumericOrBool() || type.IsEnum) && (type2.IsNumericOrBool() || type2.IsEnum || type2 == typeof(decimal)))
			{
				Type type3 = null;
				if (type.IsEnum)
				{
					type = Enum.GetUnderlyingType(type);
				}
				if (type2.IsEnum)
				{
					type3 = type2;
					type2 = Enum.GetUnderlyingType(type2);
				}
				TypeCode typeCode = type.GetTypeCode();
				TypeCode typeCode2 = type2.GetTypeCode();
				if (typeCode == typeCode2)
				{
					if (type3 != null)
					{
						if (typeFrom.IsNullableType() && !typeTo.IsNullableType())
						{
							this._instructions.Emit(NullableMethodCallInstruction.CreateGetValue());
						}
					}
					else
					{
						this._instructions.EmitConvertToUnderlying(typeCode2, isLiftedToNull);
					}
				}
				else if (isChecked)
				{
					this._instructions.EmitNumericConvertChecked(typeCode, typeCode2, isLiftedToNull);
				}
				else
				{
					this._instructions.EmitNumericConvertUnchecked(typeCode, typeCode2, isLiftedToNull);
				}
				if (type3 != null)
				{
					this._instructions.EmitCastToEnum(type3);
				}
				return;
			}
			if (typeTo.IsEnum)
			{
				this._instructions.Emit(NullCheckInstruction.Instance);
				this._instructions.EmitCastReferenceToEnum(typeTo);
				return;
			}
			if (typeTo == typeof(object) || typeTo.IsAssignableFrom(typeFrom))
			{
				return;
			}
			this._instructions.EmitCast(typeTo);
		}

		// Token: 0x0600167E RID: 5758 RVA: 0x000401CD File Offset: 0x0003E3CD
		private void CompileNotExpression(UnaryExpression node)
		{
			this.Compile(node.Operand);
			this._instructions.EmitNot(node.Operand.Type);
		}

		// Token: 0x0600167F RID: 5759 RVA: 0x000401F4 File Offset: 0x0003E3F4
		private void CompileUnaryExpression(Expression expr)
		{
			UnaryExpression unaryExpression = (UnaryExpression)expr;
			if (unaryExpression.Method != null)
			{
				this.EmitUnaryMethodCall(unaryExpression);
				return;
			}
			ExpressionType nodeType = unaryExpression.NodeType;
			if (nodeType <= ExpressionType.TypeAs)
			{
				if (nodeType == ExpressionType.ArrayLength)
				{
					this.Compile(unaryExpression.Operand);
					this._instructions.EmitArrayLength();
					return;
				}
				switch (nodeType)
				{
				case ExpressionType.Negate:
					this.Compile(unaryExpression.Operand);
					this._instructions.EmitNegate(unaryExpression.Type);
					return;
				case ExpressionType.UnaryPlus:
					this.Compile(unaryExpression.Operand);
					return;
				case ExpressionType.NegateChecked:
					this.Compile(unaryExpression.Operand);
					this._instructions.EmitNegateChecked(unaryExpression.Type);
					return;
				case ExpressionType.New:
				case ExpressionType.NewArrayInit:
				case ExpressionType.NewArrayBounds:
					goto IL_13C;
				case ExpressionType.Not:
					break;
				default:
					if (nodeType != ExpressionType.TypeAs)
					{
						goto IL_13C;
					}
					this.CompileTypeAsExpression(unaryExpression);
					return;
				}
			}
			else if (nodeType <= ExpressionType.Increment)
			{
				if (nodeType == ExpressionType.Decrement)
				{
					this.Compile(unaryExpression.Operand);
					this._instructions.EmitDecrement(unaryExpression.Type);
					return;
				}
				if (nodeType != ExpressionType.Increment)
				{
					goto IL_13C;
				}
				this.Compile(unaryExpression.Operand);
				this._instructions.EmitIncrement(unaryExpression.Type);
				return;
			}
			else if (nodeType != ExpressionType.OnesComplement)
			{
				if (nodeType - ExpressionType.IsTrue > 1)
				{
					goto IL_13C;
				}
				this.EmitUnaryBoolCheck(unaryExpression);
				return;
			}
			this.CompileNotExpression(unaryExpression);
			return;
			IL_13C:
			throw new PlatformNotSupportedException(SR.Format("The expression type '{0}' is not supported", unaryExpression.NodeType));
		}

		// Token: 0x06001680 RID: 5760 RVA: 0x00040358 File Offset: 0x0003E558
		private void EmitUnaryMethodCall(UnaryExpression node)
		{
			this.Compile(node.Operand);
			if (node.IsLifted)
			{
				BranchLabel branchLabel = this._instructions.MakeLabel();
				BranchLabel label = this._instructions.MakeLabel();
				this._instructions.EmitCoalescingBranch(branchLabel);
				this._instructions.EmitBranch(label);
				this._instructions.MarkLabel(branchLabel);
				this._instructions.EmitCall(node.Method);
				this._instructions.MarkLabel(label);
				return;
			}
			this._instructions.EmitCall(node.Method);
		}

		// Token: 0x06001681 RID: 5761 RVA: 0x000403E4 File Offset: 0x0003E5E4
		private void EmitUnaryBoolCheck(UnaryExpression node)
		{
			this.Compile(node.Operand);
			if (node.IsLifted)
			{
				BranchLabel branchLabel = this._instructions.MakeLabel();
				BranchLabel label = this._instructions.MakeLabel();
				this._instructions.EmitCoalescingBranch(branchLabel);
				this._instructions.EmitBranch(label);
				this._instructions.MarkLabel(branchLabel);
				this._instructions.EmitLoad(node.NodeType == ExpressionType.IsTrue);
				this._instructions.EmitEqual(typeof(bool), false);
				this._instructions.MarkLabel(label);
				return;
			}
			this._instructions.EmitLoad(node.NodeType == ExpressionType.IsTrue);
			this._instructions.EmitEqual(typeof(bool), false);
		}

		// Token: 0x06001682 RID: 5762 RVA: 0x000404A4 File Offset: 0x0003E6A4
		private void CompileAndAlsoBinaryExpression(Expression expr)
		{
			this.CompileLogicalBinaryExpression((BinaryExpression)expr, true);
		}

		// Token: 0x06001683 RID: 5763 RVA: 0x000404B3 File Offset: 0x0003E6B3
		private void CompileOrElseBinaryExpression(Expression expr)
		{
			this.CompileLogicalBinaryExpression((BinaryExpression)expr, false);
		}

		// Token: 0x06001684 RID: 5764 RVA: 0x000404C4 File Offset: 0x0003E6C4
		private void CompileLogicalBinaryExpression(BinaryExpression b, bool andAlso)
		{
			if (b.Method != null && !b.IsLiftedLogical)
			{
				this.CompileMethodLogicalBinaryExpression(b, andAlso);
				return;
			}
			if (b.Left.Type == typeof(bool?))
			{
				this.CompileLiftedLogicalBinaryExpression(b, andAlso);
				return;
			}
			if (b.IsLiftedLogical)
			{
				this.Compile(b.ReduceUserdefinedLifted());
				return;
			}
			this.CompileUnliftedLogicalBinaryExpression(b, andAlso);
		}

		// Token: 0x06001685 RID: 5765 RVA: 0x00040534 File Offset: 0x0003E734
		private void CompileMethodLogicalBinaryExpression(BinaryExpression expr, bool andAlso)
		{
			BranchLabel branchLabel = this._instructions.MakeLabel();
			this.Compile(expr.Left);
			this._instructions.EmitDup();
			MethodInfo booleanOperator = TypeUtils.GetBooleanOperator(expr.Method.DeclaringType, andAlso ? "op_False" : "op_True");
			this._instructions.EmitCall(booleanOperator);
			this._instructions.EmitBranchTrue(branchLabel);
			this.Compile(expr.Right);
			this._instructions.EmitCall(expr.Method);
			this._instructions.MarkLabel(branchLabel);
		}

		// Token: 0x06001686 RID: 5766 RVA: 0x000405C8 File Offset: 0x0003E7C8
		private void CompileLiftedLogicalBinaryExpression(BinaryExpression node, bool andAlso)
		{
			BranchLabel branchLabel = this._instructions.MakeLabel();
			BranchLabel branchLabel2 = this._instructions.MakeLabel();
			BranchLabel branchLabel3 = this._instructions.MakeLabel();
			BranchLabel label = this._instructions.MakeLabel();
			LocalDefinition definition = this._locals.DefineLocal(Expression.Parameter(node.Left.Type), this._instructions.Count);
			LocalDefinition definition2 = this._locals.DefineLocal(Expression.Parameter(node.Left.Type), this._instructions.Count);
			this.Compile(node.Left);
			this._instructions.EmitStoreLocal(definition2.Index);
			this._instructions.EmitLoadLocal(definition2.Index);
			this._instructions.EmitLoad(null, typeof(object));
			this._instructions.EmitEqual(typeof(object), false);
			this._instructions.EmitBranchTrue(branchLabel);
			this._instructions.EmitLoadLocal(definition2.Index);
			if (andAlso)
			{
				this._instructions.EmitBranchFalse(branchLabel2);
			}
			else
			{
				this._instructions.EmitBranchTrue(branchLabel2);
			}
			this._instructions.MarkLabel(branchLabel);
			LocalDefinition definition3 = this._locals.DefineLocal(Expression.Parameter(node.Right.Type), this._instructions.Count);
			this.Compile(node.Right);
			this._instructions.EmitStoreLocal(definition3.Index);
			this._instructions.EmitLoadLocal(definition3.Index);
			this._instructions.EmitLoad(null, typeof(object));
			this._instructions.EmitEqual(typeof(object), false);
			this._instructions.EmitBranchTrue(branchLabel3);
			this._instructions.EmitLoadLocal(definition3.Index);
			if (andAlso)
			{
				this._instructions.EmitBranchFalse(branchLabel2);
			}
			else
			{
				this._instructions.EmitBranchTrue(branchLabel2);
			}
			this._instructions.EmitLoadLocal(definition2.Index);
			this._instructions.EmitLoad(null, typeof(object));
			this._instructions.EmitEqual(typeof(object), false);
			this._instructions.EmitBranchTrue(branchLabel3);
			this._instructions.EmitLoad(andAlso ? Utils.BoxedTrue : Utils.BoxedFalse, typeof(object));
			this._instructions.EmitStoreLocal(definition.Index);
			this._instructions.EmitBranch(label);
			this._instructions.MarkLabel(branchLabel2);
			this._instructions.EmitLoad(andAlso ? Utils.BoxedFalse : Utils.BoxedTrue, typeof(object));
			this._instructions.EmitStoreLocal(definition.Index);
			this._instructions.EmitBranch(label);
			this._instructions.MarkLabel(branchLabel3);
			this._instructions.EmitLoad(null, typeof(object));
			this._instructions.EmitStoreLocal(definition.Index);
			this._instructions.MarkLabel(label);
			this._instructions.EmitLoadLocal(definition.Index);
			this._locals.UndefineLocal(definition2, this._instructions.Count);
			this._locals.UndefineLocal(definition3, this._instructions.Count);
			this._locals.UndefineLocal(definition, this._instructions.Count);
		}

		// Token: 0x06001687 RID: 5767 RVA: 0x0004092C File Offset: 0x0003EB2C
		private void CompileUnliftedLogicalBinaryExpression(BinaryExpression expr, bool andAlso)
		{
			BranchLabel branchLabel = this._instructions.MakeLabel();
			BranchLabel label = this._instructions.MakeLabel();
			this.Compile(expr.Left);
			if (andAlso)
			{
				this._instructions.EmitBranchFalse(branchLabel);
			}
			else
			{
				this._instructions.EmitBranchTrue(branchLabel);
			}
			this.Compile(expr.Right);
			this._instructions.EmitBranch(label, false, true);
			this._instructions.MarkLabel(branchLabel);
			this._instructions.EmitLoad(!andAlso);
			this._instructions.MarkLabel(label);
		}

		// Token: 0x06001688 RID: 5768 RVA: 0x000409BC File Offset: 0x0003EBBC
		private void CompileConditionalExpression(Expression expr, bool asVoid)
		{
			ConditionalExpression conditionalExpression = (ConditionalExpression)expr;
			this.Compile(conditionalExpression.Test);
			if (conditionalExpression.IfTrue == Utils.Empty)
			{
				BranchLabel branchLabel = this._instructions.MakeLabel();
				this._instructions.EmitBranchTrue(branchLabel);
				this.Compile(conditionalExpression.IfFalse, asVoid);
				this._instructions.MarkLabel(branchLabel);
				return;
			}
			BranchLabel branchLabel2 = this._instructions.MakeLabel();
			this._instructions.EmitBranchFalse(branchLabel2);
			this.Compile(conditionalExpression.IfTrue, asVoid);
			if (conditionalExpression.IfFalse != Utils.Empty)
			{
				BranchLabel label = this._instructions.MakeLabel();
				this._instructions.EmitBranch(label, false, !asVoid);
				this._instructions.MarkLabel(branchLabel2);
				this.Compile(conditionalExpression.IfFalse, asVoid);
				this._instructions.MarkLabel(label);
				return;
			}
			this._instructions.MarkLabel(branchLabel2);
		}

		// Token: 0x06001689 RID: 5769 RVA: 0x00040A9C File Offset: 0x0003EC9C
		private void CompileLoopExpression(Expression expr)
		{
			LoopExpression loopExpression = (LoopExpression)expr;
			this.PushLabelBlock(LabelScopeKind.Statement);
			LabelInfo labelInfo = this.DefineLabel(loopExpression.BreakLabel);
			LabelInfo labelInfo2 = this.DefineLabel(loopExpression.ContinueLabel);
			this._instructions.MarkLabel(labelInfo2.GetLabel(this));
			this.CompileAsVoid(loopExpression.Body);
			this._instructions.EmitBranch(labelInfo2.GetLabel(this), loopExpression.Type != typeof(void), false);
			this._instructions.MarkLabel(labelInfo.GetLabel(this));
			this.PopLabelBlock(LabelScopeKind.Statement);
		}

		// Token: 0x0600168A RID: 5770 RVA: 0x00040B30 File Offset: 0x0003ED30
		private void CompileSwitchExpression(Expression expr)
		{
			SwitchExpression switchExpression = (SwitchExpression)expr;
			if (switchExpression.Cases.All((SwitchCase c) => c.TestValues.All((Expression t) => t is ConstantExpression)))
			{
				if (switchExpression.Cases.Count == 0)
				{
					this.CompileAsVoid(switchExpression.SwitchValue);
					if (switchExpression.DefaultBody != null)
					{
						this.Compile(switchExpression.DefaultBody);
					}
					return;
				}
				TypeCode typeCode = switchExpression.SwitchValue.Type.GetTypeCode();
				if (switchExpression.Comparison == null)
				{
					switch (typeCode)
					{
					case TypeCode.SByte:
					case TypeCode.Byte:
					case TypeCode.Int16:
					case TypeCode.UInt16:
					case TypeCode.UInt32:
					case TypeCode.Int64:
					case TypeCode.UInt64:
						this.CompileIntSwitchExpression<object>(switchExpression);
						return;
					case TypeCode.Int32:
						this.CompileIntSwitchExpression<int>(switchExpression);
						return;
					}
				}
				if (typeCode == TypeCode.String)
				{
					MethodInfo methodInfo = CachedReflectionInfo.String_op_Equality_String_String;
					if (methodInfo != null && !methodInfo.IsStatic)
					{
						methodInfo = null;
					}
					if (object.Equals(switchExpression.Comparison, methodInfo))
					{
						this.CompileStringSwitchExpression(switchExpression);
						return;
					}
				}
			}
			LocalDefinition definition = this._locals.DefineLocal(Expression.Parameter(switchExpression.SwitchValue.Type), this._instructions.Count);
			this.Compile(switchExpression.SwitchValue);
			this._instructions.EmitStoreLocal(definition.Index);
			LabelTarget target = Expression.Label(switchExpression.Type, "done");
			foreach (SwitchCase switchCase in switchExpression.Cases)
			{
				foreach (Expression right in switchCase.TestValues)
				{
					this.CompileConditionalExpression(Expression.Condition(Expression.Equal(definition.Parameter, right, false, switchExpression.Comparison), Expression.Goto(target, switchCase.Body), Utils.Empty), true);
				}
			}
			this.CompileLabelExpression(Expression.Label(target, switchExpression.DefaultBody));
			this._locals.UndefineLocal(definition, this._instructions.Count);
		}

		// Token: 0x0600168B RID: 5771 RVA: 0x00040D60 File Offset: 0x0003EF60
		private void CompileIntSwitchExpression<T>(SwitchExpression node)
		{
			LabelInfo labelInfo = this.DefineLabel(null);
			bool flag = node.Type != typeof(void);
			this.Compile(node.SwitchValue);
			Dictionary<T, int> dictionary = new Dictionary<T, int>();
			int count = this._instructions.Count;
			this._instructions.EmitIntSwitch<T>(dictionary);
			if (node.DefaultBody != null)
			{
				this.Compile(node.DefaultBody, !flag);
			}
			this._instructions.EmitBranch(labelInfo.GetLabel(this), false, flag);
			for (int i = 0; i < node.Cases.Count; i++)
			{
				SwitchCase switchCase = node.Cases[i];
				int value = this._instructions.Count - count;
				foreach (Expression expression in switchCase.TestValues)
				{
					T key = (T)((object)((ConstantExpression)expression).Value);
					dictionary.TryAdd(key, value);
				}
				this.Compile(switchCase.Body, !flag);
				if (i < node.Cases.Count - 1)
				{
					this._instructions.EmitBranch(labelInfo.GetLabel(this), false, flag);
				}
			}
			this._instructions.MarkLabel(labelInfo.GetLabel(this));
		}

		// Token: 0x0600168C RID: 5772 RVA: 0x00040EC4 File Offset: 0x0003F0C4
		private void CompileStringSwitchExpression(SwitchExpression node)
		{
			LabelInfo labelInfo = this.DefineLabel(null);
			bool flag = node.Type != typeof(void);
			this.Compile(node.SwitchValue);
			Dictionary<string, int> dictionary = new Dictionary<string, int>();
			int count = this._instructions.Count;
			StrongBox<int> strongBox = new StrongBox<int>(1);
			this._instructions.EmitStringSwitch(dictionary, strongBox);
			if (node.DefaultBody != null)
			{
				this.Compile(node.DefaultBody, !flag);
			}
			this._instructions.EmitBranch(labelInfo.GetLabel(this), false, flag);
			for (int i = 0; i < node.Cases.Count; i++)
			{
				SwitchCase switchCase = node.Cases[i];
				int value = this._instructions.Count - count;
				foreach (Expression expression in switchCase.TestValues)
				{
					string text = (string)((ConstantExpression)expression).Value;
					if (text == null)
					{
						if (strongBox.Value == 1)
						{
							strongBox.Value = value;
						}
					}
					else
					{
						dictionary.TryAdd(text, value);
					}
				}
				this.Compile(switchCase.Body, !flag);
				if (i < node.Cases.Count - 1)
				{
					this._instructions.EmitBranch(labelInfo.GetLabel(this), false, flag);
				}
			}
			this._instructions.MarkLabel(labelInfo.GetLabel(this));
		}

		// Token: 0x0600168D RID: 5773 RVA: 0x00041048 File Offset: 0x0003F248
		private void CompileLabelExpression(Expression expr)
		{
			LabelExpression labelExpression = (LabelExpression)expr;
			LabelInfo labelInfo = null;
			if (this._labelBlock.Kind == LabelScopeKind.Block)
			{
				this._labelBlock.TryGetLabelInfo(labelExpression.Target, out labelInfo);
				if (labelInfo == null && this._labelBlock.Parent.Kind == LabelScopeKind.Switch)
				{
					this._labelBlock.Parent.TryGetLabelInfo(labelExpression.Target, out labelInfo);
				}
			}
			if (labelInfo == null)
			{
				labelInfo = this.DefineLabel(labelExpression.Target);
			}
			if (labelExpression.DefaultValue != null)
			{
				if (labelExpression.Target.Type == typeof(void))
				{
					this.CompileAsVoid(labelExpression.DefaultValue);
				}
				else
				{
					this.Compile(labelExpression.DefaultValue);
				}
			}
			this._instructions.MarkLabel(labelInfo.GetLabel(this));
		}

		// Token: 0x0600168E RID: 5774 RVA: 0x00041110 File Offset: 0x0003F310
		private void CompileGotoExpression(Expression expr)
		{
			GotoExpression gotoExpression = (GotoExpression)expr;
			LabelInfo labelInfo = this.ReferenceLabel(gotoExpression.Target);
			if (gotoExpression.Value != null)
			{
				this.Compile(gotoExpression.Value);
			}
			this._instructions.EmitGoto(labelInfo.GetLabel(this), gotoExpression.Type != typeof(void), gotoExpression.Value != null && gotoExpression.Value.Type != typeof(void), gotoExpression.Target.Type != typeof(void));
		}

		// Token: 0x0600168F RID: 5775 RVA: 0x000411AB File Offset: 0x0003F3AB
		private void PushLabelBlock(LabelScopeKind type)
		{
			this._labelBlock = new LabelScopeInfo(this._labelBlock, type);
		}

		// Token: 0x06001690 RID: 5776 RVA: 0x000411BF File Offset: 0x0003F3BF
		private void PopLabelBlock(LabelScopeKind kind)
		{
			this._labelBlock = this._labelBlock.Parent;
		}

		// Token: 0x06001691 RID: 5777 RVA: 0x000411D4 File Offset: 0x0003F3D4
		private LabelInfo EnsureLabel(LabelTarget node)
		{
			LabelInfo result;
			if (!this._treeLabels.TryGetValue(node, out result))
			{
				result = (this._treeLabels[node] = new LabelInfo(node));
			}
			return result;
		}

		// Token: 0x06001692 RID: 5778 RVA: 0x00041206 File Offset: 0x0003F406
		private LabelInfo ReferenceLabel(LabelTarget node)
		{
			LabelInfo labelInfo = this.EnsureLabel(node);
			labelInfo.Reference(this._labelBlock);
			return labelInfo;
		}

		// Token: 0x06001693 RID: 5779 RVA: 0x0004121B File Offset: 0x0003F41B
		private LabelInfo DefineLabel(LabelTarget node)
		{
			if (node == null)
			{
				return new LabelInfo(null);
			}
			LabelInfo labelInfo = this.EnsureLabel(node);
			labelInfo.Define(this._labelBlock);
			return labelInfo;
		}

		// Token: 0x06001694 RID: 5780 RVA: 0x0004123C File Offset: 0x0003F43C
		private bool TryPushLabelBlock(Expression node)
		{
			ExpressionType nodeType = node.NodeType;
			if (nodeType <= ExpressionType.Convert)
			{
				if (nodeType == ExpressionType.Conditional)
				{
					goto IL_157;
				}
				if (nodeType == ExpressionType.Convert)
				{
					if (!(node.Type != typeof(void)))
					{
						this.PushLabelBlock(LabelScopeKind.Statement);
						return true;
					}
				}
			}
			else
			{
				if (nodeType == ExpressionType.Block)
				{
					this.PushLabelBlock(LabelScopeKind.Block);
					if (this._labelBlock.Parent.Kind != LabelScopeKind.Switch)
					{
						this.DefineBlockLabels(node);
					}
					return true;
				}
				switch (nodeType)
				{
				case ExpressionType.Goto:
				case ExpressionType.Loop:
					goto IL_157;
				case ExpressionType.Label:
					if (this._labelBlock.Kind == LabelScopeKind.Block)
					{
						LabelTarget target = ((LabelExpression)node).Target;
						if (this._labelBlock.ContainsTarget(target))
						{
							return false;
						}
						if (this._labelBlock.Parent.Kind == LabelScopeKind.Switch && this._labelBlock.Parent.ContainsTarget(target))
						{
							return false;
						}
					}
					this.PushLabelBlock(LabelScopeKind.Statement);
					return true;
				case ExpressionType.Switch:
				{
					this.PushLabelBlock(LabelScopeKind.Switch);
					SwitchExpression switchExpression = (SwitchExpression)node;
					foreach (SwitchCase switchCase in switchExpression.Cases)
					{
						this.DefineBlockLabels(switchCase.Body);
					}
					this.DefineBlockLabels(switchExpression.DefaultBody);
					return true;
				}
				}
			}
			if (this._labelBlock.Kind != LabelScopeKind.Expression)
			{
				this.PushLabelBlock(LabelScopeKind.Expression);
				return true;
			}
			return false;
			IL_157:
			this.PushLabelBlock(LabelScopeKind.Statement);
			return true;
		}

		// Token: 0x06001695 RID: 5781 RVA: 0x000413B8 File Offset: 0x0003F5B8
		private void DefineBlockLabels(Expression node)
		{
			BlockExpression blockExpression = node as BlockExpression;
			if (blockExpression == null)
			{
				return;
			}
			int i = 0;
			int count = blockExpression.Expressions.Count;
			while (i < count)
			{
				LabelExpression labelExpression = blockExpression.Expressions[i] as LabelExpression;
				if (labelExpression != null)
				{
					this.DefineLabel(labelExpression.Target);
				}
				i++;
			}
		}

		// Token: 0x06001696 RID: 5782 RVA: 0x0004140C File Offset: 0x0003F60C
		private void CheckRethrow()
		{
			for (LabelScopeInfo labelScopeInfo = this._labelBlock; labelScopeInfo != null; labelScopeInfo = labelScopeInfo.Parent)
			{
				if (labelScopeInfo.Kind == LabelScopeKind.Catch)
				{
					return;
				}
				if (labelScopeInfo.Kind == LabelScopeKind.Finally)
				{
					break;
				}
			}
			throw Error.RethrowRequiresCatch();
		}

		// Token: 0x06001697 RID: 5783 RVA: 0x00041444 File Offset: 0x0003F644
		private void CompileThrowUnaryExpression(Expression expr, bool asVoid)
		{
			UnaryExpression unaryExpression = (UnaryExpression)expr;
			if (unaryExpression.Operand == null)
			{
				this.CheckRethrow();
				this.CompileParameterExpression(this._exceptionForRethrowStack.Peek());
				if (asVoid)
				{
					this._instructions.EmitRethrowVoid();
					return;
				}
				this._instructions.EmitRethrow();
				return;
			}
			else
			{
				this.Compile(unaryExpression.Operand);
				if (asVoid)
				{
					this._instructions.EmitThrowVoid();
					return;
				}
				this._instructions.EmitThrow();
				return;
			}
		}

		// Token: 0x06001698 RID: 5784 RVA: 0x000414B8 File Offset: 0x0003F6B8
		private void CompileTryExpression(Expression expr)
		{
			TryExpression tryExpression = (TryExpression)expr;
			if (tryExpression.Fault != null)
			{
				this.CompileTryFaultExpression(tryExpression);
				return;
			}
			BranchLabel label = this._instructions.MakeLabel();
			BranchLabel branchLabel = this._instructions.MakeLabel();
			int count = this._instructions.Count;
			BranchLabel branchLabel2 = null;
			if (tryExpression.Finally != null)
			{
				branchLabel2 = this._instructions.MakeLabel();
				this._instructions.EmitEnterTryFinally(branchLabel2);
			}
			else
			{
				this._instructions.EmitEnterTryCatch();
			}
			List<ExceptionHandler> list = null;
			EnterTryCatchFinallyInstruction enterTryCatchFinallyInstruction = this._instructions.GetInstruction(count) as EnterTryCatchFinallyInstruction;
			this.PushLabelBlock(LabelScopeKind.Try);
			bool flag = tryExpression.Type != typeof(void);
			this.Compile(tryExpression.Body, !flag);
			int count2 = this._instructions.Count;
			this._instructions.MarkLabel(branchLabel);
			this._instructions.EmitGoto(label, flag, flag, flag);
			if (tryExpression.Handlers.Count > 0)
			{
				list = new List<ExceptionHandler>();
				foreach (CatchBlock catchBlock in tryExpression.Handlers)
				{
					ParameterExpression parameterExpression = catchBlock.Variable ?? Expression.Parameter(catchBlock.Test);
					LocalDefinition definition = this._locals.DefineLocal(parameterExpression, this._instructions.Count);
					this._exceptionForRethrowStack.Push(parameterExpression);
					ExceptionFilter filter = null;
					if (catchBlock.Filter != null)
					{
						this.PushLabelBlock(LabelScopeKind.Filter);
						this._instructions.EmitEnterExceptionFilter();
						int labelIndex = this._instructions.MarkRuntimeLabel();
						int count3 = this._instructions.Count;
						this.CompileSetVariable(parameterExpression, true);
						this.Compile(catchBlock.Filter);
						this.CompileGetVariable(parameterExpression);
						filter = new ExceptionFilter(labelIndex, count3, this._instructions.Count);
						this._instructions.EmitLeaveExceptionFilter();
						this.PopLabelBlock(LabelScopeKind.Filter);
					}
					this.PushLabelBlock(LabelScopeKind.Catch);
					if (flag)
					{
						this._instructions.EmitEnterExceptionHandlerNonVoid();
					}
					else
					{
						this._instructions.EmitEnterExceptionHandlerVoid();
					}
					int labelIndex2 = this._instructions.MarkRuntimeLabel();
					int count4 = this._instructions.Count;
					this.CompileSetVariable(parameterExpression, true);
					this.Compile(catchBlock.Body, !flag);
					this._exceptionForRethrowStack.Pop();
					this._instructions.EmitLeaveExceptionHandler(flag, branchLabel);
					list.Add(new ExceptionHandler(labelIndex2, count4, this._instructions.Count, catchBlock.Test, filter));
					this.PopLabelBlock(LabelScopeKind.Catch);
					this._locals.UndefineLocal(definition, this._instructions.Count);
				}
			}
			if (tryExpression.Finally != null)
			{
				this.PushLabelBlock(LabelScopeKind.Finally);
				this._instructions.MarkLabel(branchLabel2);
				this._instructions.EmitEnterFinally(branchLabel2);
				this.CompileAsVoid(tryExpression.Finally);
				this._instructions.EmitLeaveFinally();
				enterTryCatchFinallyInstruction.SetTryHandler(new TryCatchFinallyHandler(count, count2, branchLabel.TargetIndex, branchLabel2.TargetIndex, this._instructions.Count, (list != null) ? list.ToArray() : null));
				this.PopLabelBlock(LabelScopeKind.Finally);
			}
			else
			{
				enterTryCatchFinallyInstruction.SetTryHandler(new TryCatchFinallyHandler(count, count2, branchLabel.TargetIndex, list.ToArray()));
			}
			this._instructions.MarkLabel(label);
			this.PopLabelBlock(LabelScopeKind.Try);
		}

		// Token: 0x06001699 RID: 5785 RVA: 0x00041828 File Offset: 0x0003FA28
		private void CompileTryFaultExpression(TryExpression expr)
		{
			int count = this._instructions.Count;
			BranchLabel branchLabel = this._instructions.MakeLabel();
			EnterTryFaultInstruction enterTryFaultInstruction = this._instructions.EmitEnterTryFault(branchLabel);
			this.PushLabelBlock(LabelScopeKind.Try);
			bool flag = expr.Type != typeof(void);
			this.Compile(expr.Body, !flag);
			int count2 = this._instructions.Count;
			this._instructions.EmitGoto(branchLabel, flag, flag, flag);
			this.PushLabelBlock(LabelScopeKind.Finally);
			BranchLabel branchLabel2 = this._instructions.MakeLabel();
			this._instructions.MarkLabel(branchLabel2);
			this._instructions.EmitEnterFault(branchLabel2);
			this.CompileAsVoid(expr.Fault);
			this._instructions.EmitLeaveFault();
			enterTryFaultInstruction.SetTryHandler(new TryFaultHandler(count, count2, branchLabel2.TargetIndex, this._instructions.Count));
			this.PopLabelBlock(LabelScopeKind.Finally);
			this.PopLabelBlock(LabelScopeKind.Try);
			this._instructions.MarkLabel(branchLabel);
		}

		// Token: 0x0600169A RID: 5786 RVA: 0x00041920 File Offset: 0x0003FB20
		private void CompileMethodCallExpression(Expression expr)
		{
			MethodCallExpression methodCallExpression = (MethodCallExpression)expr;
			this.CompileMethodCallExpression(methodCallExpression.Object, methodCallExpression.Method, methodCallExpression);
		}

		// Token: 0x0600169B RID: 5787 RVA: 0x00041948 File Offset: 0x0003FB48
		private void CompileMethodCallExpression(Expression @object, MethodInfo method, IArgumentProvider arguments)
		{
			ParameterInfo[] parametersCached = method.GetParametersCached();
			List<ByRefUpdater> list = null;
			if (!method.IsStatic)
			{
				ByRefUpdater byRefUpdater = this.CompileAddress(@object, -1);
				if (byRefUpdater != null)
				{
					list = new List<ByRefUpdater>
					{
						byRefUpdater
					};
				}
			}
			int i = 0;
			int argumentCount = arguments.ArgumentCount;
			while (i < argumentCount)
			{
				Expression argument = arguments.GetArgument(i);
				if (parametersCached[i].ParameterType.IsByRef)
				{
					ByRefUpdater byRefUpdater2 = this.CompileAddress(argument, i);
					if (byRefUpdater2 != null)
					{
						if (list == null)
						{
							list = new List<ByRefUpdater>();
						}
						list.Add(byRefUpdater2);
					}
				}
				else
				{
					this.Compile(argument);
				}
				i++;
			}
			if (!method.IsStatic && @object.Type.IsNullableType())
			{
				this._instructions.EmitNullableCall(method, parametersCached);
				return;
			}
			if (list == null)
			{
				this._instructions.EmitCall(method, parametersCached);
				return;
			}
			this._instructions.EmitByRefCall(method, parametersCached, list.ToArray());
			foreach (ByRefUpdater byRefUpdater3 in list)
			{
				byRefUpdater3.UndefineTemps(this._instructions, this._locals);
			}
		}

		// Token: 0x0600169C RID: 5788 RVA: 0x00041A68 File Offset: 0x0003FC68
		private ByRefUpdater CompileArrayIndexAddress(Expression array, Expression index, int argumentIndex)
		{
			LocalDefinition array2 = this._locals.DefineLocal(Expression.Parameter(array.Type, "array"), this._instructions.Count);
			LocalDefinition index2 = this._locals.DefineLocal(Expression.Parameter(index.Type, "index"), this._instructions.Count);
			this.Compile(array);
			this._instructions.EmitStoreLocal(array2.Index);
			this.Compile(index);
			this._instructions.EmitStoreLocal(index2.Index);
			this._instructions.EmitLoadLocal(array2.Index);
			this._instructions.EmitLoadLocal(index2.Index);
			this._instructions.EmitGetArrayItem();
			return new ArrayByRefUpdater(array2, index2, argumentIndex);
		}

		// Token: 0x0600169D RID: 5789 RVA: 0x00041B2C File Offset: 0x0003FD2C
		private void EmitThisForMethodCall(Expression node)
		{
			this.CompileAddress(node, -1);
		}

		// Token: 0x0600169E RID: 5790 RVA: 0x00041B38 File Offset: 0x0003FD38
		private static bool ShouldWritebackNode(Expression node)
		{
			if (node.Type.IsValueType)
			{
				ExpressionType nodeType = node.NodeType;
				if (nodeType <= ExpressionType.MemberAccess)
				{
					if (nodeType - ExpressionType.ArrayIndex > 1)
					{
						if (nodeType != ExpressionType.MemberAccess)
						{
							return false;
						}
						return ((MemberExpression)node).Member is FieldInfo;
					}
				}
				else if (nodeType != ExpressionType.Parameter)
				{
					if (nodeType != ExpressionType.Index)
					{
						return false;
					}
					return ((IndexExpression)node).Object.Type.IsArray;
				}
				return true;
			}
			return false;
		}

		// Token: 0x0600169F RID: 5791 RVA: 0x00041BA4 File Offset: 0x0003FDA4
		private ByRefUpdater CompileAddress(Expression node, int index)
		{
			if (index != -1 || LightCompiler.ShouldWritebackNode(node))
			{
				ExpressionType nodeType = node.NodeType;
				if (nodeType <= ExpressionType.Call)
				{
					if (nodeType == ExpressionType.ArrayIndex)
					{
						BinaryExpression binaryExpression = (BinaryExpression)node;
						return this.CompileArrayIndexAddress(binaryExpression.Left, binaryExpression.Right, index);
					}
					if (nodeType == ExpressionType.Call)
					{
						MethodCallExpression methodCallExpression = (MethodCallExpression)node;
						if (!methodCallExpression.Method.IsStatic && methodCallExpression.Object.Type.IsArray && methodCallExpression.Method == methodCallExpression.Object.Type.GetMethod("Get", BindingFlags.Instance | BindingFlags.Public))
						{
							return this.CompileMultiDimArrayAccess(methodCallExpression.Object, methodCallExpression, index);
						}
					}
				}
				else if (nodeType != ExpressionType.MemberAccess)
				{
					if (nodeType == ExpressionType.Parameter)
					{
						this.LoadLocalNoValueTypeCopy((ParameterExpression)node);
						return new ParameterByRefUpdater(this.ResolveLocal((ParameterExpression)node), index);
					}
					if (nodeType == ExpressionType.Index)
					{
						IndexExpression indexExpression = (IndexExpression)node;
						if (indexExpression.Indexer != null)
						{
							LocalDefinition? obj = null;
							if (indexExpression.Object != null)
							{
								obj = new LocalDefinition?(this._locals.DefineLocal(Expression.Parameter(indexExpression.Object.Type), this._instructions.Count));
								this.EmitThisForMethodCall(indexExpression.Object);
								this._instructions.EmitDup();
								this._instructions.EmitStoreLocal(obj.GetValueOrDefault().Index);
							}
							int argumentCount = indexExpression.ArgumentCount;
							LocalDefinition[] array = new LocalDefinition[argumentCount];
							for (int i = 0; i < argumentCount; i++)
							{
								Expression argument = indexExpression.GetArgument(i);
								this.Compile(argument);
								LocalDefinition localDefinition = this._locals.DefineLocal(Expression.Parameter(argument.Type), this._instructions.Count);
								this._instructions.EmitDup();
								this._instructions.EmitStoreLocal(localDefinition.Index);
								array[i] = localDefinition;
							}
							this.EmitIndexGet(indexExpression);
							return new IndexMethodByRefUpdater(obj, array, indexExpression.Indexer.GetSetMethod(), index);
						}
						if (indexExpression.ArgumentCount == 1)
						{
							return this.CompileArrayIndexAddress(indexExpression.Object, indexExpression.GetArgument(0), index);
						}
						return this.CompileMultiDimArrayAccess(indexExpression.Object, indexExpression, index);
					}
				}
				else
				{
					MemberExpression memberExpression = (MemberExpression)node;
					LocalDefinition? obj2 = null;
					if (memberExpression.Expression != null)
					{
						obj2 = new LocalDefinition?(this._locals.DefineLocal(Expression.Parameter(memberExpression.Expression.Type, "member"), this._instructions.Count));
						this.EmitThisForMethodCall(memberExpression.Expression);
						this._instructions.EmitDup();
						this._instructions.EmitStoreLocal(obj2.GetValueOrDefault().Index);
					}
					FieldInfo fieldInfo = memberExpression.Member as FieldInfo;
					if (fieldInfo != null)
					{
						this._instructions.EmitLoadField(fieldInfo);
						if (!fieldInfo.IsLiteral && !fieldInfo.IsInitOnly)
						{
							return new FieldByRefUpdater(obj2, fieldInfo, index);
						}
						return null;
					}
					else
					{
						PropertyInfo propertyInfo = (PropertyInfo)memberExpression.Member;
						this._instructions.EmitCall(propertyInfo.GetGetMethod(true));
						if (propertyInfo.CanWrite)
						{
							return new PropertyByRefUpdater(obj2, propertyInfo, index);
						}
						return null;
					}
				}
			}
			this.Compile(node);
			return null;
		}

		// Token: 0x060016A0 RID: 5792 RVA: 0x00041EDC File Offset: 0x000400DC
		private ByRefUpdater CompileMultiDimArrayAccess(Expression array, IArgumentProvider arguments, int index)
		{
			this.Compile(array);
			LocalDefinition value = this._locals.DefineLocal(Expression.Parameter(array.Type), this._instructions.Count);
			this._instructions.EmitDup();
			this._instructions.EmitStoreLocal(value.Index);
			int argumentCount = arguments.ArgumentCount;
			LocalDefinition[] array2 = new LocalDefinition[argumentCount];
			for (int i = 0; i < argumentCount; i++)
			{
				Expression argument = arguments.GetArgument(i);
				this.Compile(argument);
				LocalDefinition localDefinition = this._locals.DefineLocal(Expression.Parameter(argument.Type), this._instructions.Count);
				this._instructions.EmitDup();
				this._instructions.EmitStoreLocal(localDefinition.Index);
				array2[i] = localDefinition;
			}
			this._instructions.EmitCall(array.Type.GetMethod("Get", BindingFlags.Instance | BindingFlags.Public));
			return new IndexMethodByRefUpdater(new LocalDefinition?(value), array2, array.Type.GetMethod("Set", BindingFlags.Instance | BindingFlags.Public), index);
		}

		// Token: 0x060016A1 RID: 5793 RVA: 0x00041FE0 File Offset: 0x000401E0
		private void CompileNewExpression(Expression expr)
		{
			NewExpression newExpression = (NewExpression)expr;
			if (newExpression.Constructor != null)
			{
				if (newExpression.Constructor.DeclaringType.IsAbstract)
				{
					throw Error.NonAbstractConstructorRequired();
				}
				ParameterInfo[] parametersCached = newExpression.Constructor.GetParametersCached();
				List<ByRefUpdater> list = null;
				for (int i = 0; i < parametersCached.Length; i++)
				{
					Expression argument = newExpression.GetArgument(i);
					if (parametersCached[i].ParameterType.IsByRef)
					{
						ByRefUpdater byRefUpdater = this.CompileAddress(argument, i);
						if (byRefUpdater != null)
						{
							if (list == null)
							{
								list = new List<ByRefUpdater>();
							}
							list.Add(byRefUpdater);
						}
					}
					else
					{
						this.Compile(argument);
					}
				}
				if (list != null)
				{
					this._instructions.EmitByRefNew(newExpression.Constructor, parametersCached, list.ToArray());
					return;
				}
				this._instructions.EmitNew(newExpression.Constructor, parametersCached);
				return;
			}
			else
			{
				Type type = newExpression.Type;
				if (type.IsNullableType())
				{
					this._instructions.EmitLoad(null);
					return;
				}
				this._instructions.EmitDefaultValue(type);
				return;
			}
		}

		// Token: 0x060016A2 RID: 5794 RVA: 0x000420D8 File Offset: 0x000402D8
		private void CompileMemberExpression(Expression expr)
		{
			MemberExpression memberExpression = (MemberExpression)expr;
			this.CompileMember(memberExpression.Expression, memberExpression.Member, false);
		}

		// Token: 0x060016A3 RID: 5795 RVA: 0x00042100 File Offset: 0x00040300
		private void CompileMember(Expression from, MemberInfo member, bool forBinding)
		{
			FieldInfo fieldInfo = member as FieldInfo;
			if (!(fieldInfo != null))
			{
				PropertyInfo propertyInfo = (PropertyInfo)member;
				if (propertyInfo != null)
				{
					MethodInfo getMethod = propertyInfo.GetGetMethod(true);
					if (forBinding && getMethod.IsStatic)
					{
						throw Error.InvalidProgram();
					}
					if (from != null)
					{
						this.EmitThisForMethodCall(from);
					}
					if (!getMethod.IsStatic && from != null && from.Type.IsNullableType())
					{
						this._instructions.EmitNullableCall(getMethod, Array.Empty<ParameterInfo>());
						return;
					}
					this._instructions.EmitCall(getMethod);
				}
				return;
			}
			if (fieldInfo.IsLiteral)
			{
				this._instructions.EmitLoad(fieldInfo.GetValue(null), fieldInfo.FieldType);
				return;
			}
			if (!fieldInfo.IsStatic)
			{
				if (from != null)
				{
					this.EmitThisForMethodCall(from);
				}
				this._instructions.EmitLoadField(fieldInfo);
				return;
			}
			if (forBinding)
			{
				throw Error.InvalidProgram();
			}
			if (fieldInfo.IsInitOnly)
			{
				this._instructions.EmitLoad(fieldInfo.GetValue(null), fieldInfo.FieldType);
				return;
			}
			this._instructions.EmitLoadField(fieldInfo);
		}

		// Token: 0x060016A4 RID: 5796 RVA: 0x00042200 File Offset: 0x00040400
		private void CompileNewArrayExpression(Expression expr)
		{
			NewArrayExpression newArrayExpression = (NewArrayExpression)expr;
			foreach (Expression expr2 in newArrayExpression.Expressions)
			{
				this.Compile(expr2);
			}
			Type elementType = newArrayExpression.Type.GetElementType();
			int count = newArrayExpression.Expressions.Count;
			if (newArrayExpression.NodeType == ExpressionType.NewArrayInit)
			{
				this._instructions.EmitNewArrayInit(elementType, count);
				return;
			}
			if (count == 1)
			{
				this._instructions.EmitNewArray(elementType);
				return;
			}
			this._instructions.EmitNewArrayBounds(elementType, count);
		}

		// Token: 0x060016A5 RID: 5797 RVA: 0x000422A4 File Offset: 0x000404A4
		private void CompileDebugInfoExpression(Expression expr)
		{
			DebugInfoExpression debugInfoExpression = (DebugInfoExpression)expr;
			int count = this._instructions.Count;
			DebugInfo item = new DebugInfo
			{
				Index = count,
				FileName = debugInfoExpression.Document.FileName,
				StartLine = debugInfoExpression.StartLine,
				EndLine = debugInfoExpression.EndLine,
				IsClear = debugInfoExpression.IsClear
			};
			this._debugInfos.Add(item);
		}

		// Token: 0x060016A6 RID: 5798 RVA: 0x00042314 File Offset: 0x00040514
		private void CompileRuntimeVariablesExpression(Expression expr)
		{
			RuntimeVariablesExpression runtimeVariablesExpression = (RuntimeVariablesExpression)expr;
			foreach (ParameterExpression parameterExpression in runtimeVariablesExpression.Variables)
			{
				this.EnsureAvailableForClosure(parameterExpression);
				this.CompileGetBoxedVariable(parameterExpression);
			}
			this._instructions.EmitNewRuntimeVariables(runtimeVariablesExpression.Variables.Count);
		}

		// Token: 0x060016A7 RID: 5799 RVA: 0x00042388 File Offset: 0x00040588
		private void CompileLambdaExpression(Expression expr)
		{
			LambdaExpression node = (LambdaExpression)expr;
			LightCompiler lightCompiler = new LightCompiler(this);
			LightDelegateCreator creator = lightCompiler.CompileTop(node);
			if (lightCompiler._locals.ClosureVariables != null)
			{
				foreach (ParameterExpression parameterExpression in lightCompiler._locals.ClosureVariables.Keys)
				{
					this.EnsureAvailableForClosure(parameterExpression);
					this.CompileGetBoxedVariable(parameterExpression);
				}
			}
			this._instructions.EmitCreateDelegate(creator);
		}

		// Token: 0x060016A8 RID: 5800 RVA: 0x00042420 File Offset: 0x00040620
		private void CompileCoalesceBinaryExpression(Expression expr)
		{
			BinaryExpression binaryExpression = (BinaryExpression)expr;
			bool flag = binaryExpression.Conversion != null;
			bool flag2 = false;
			if (!flag && binaryExpression.Left.Type.IsNullableType())
			{
				Type nonNullableType = binaryExpression.Left.Type.GetNonNullableType();
				if (!TypeUtils.AreEquivalent(binaryExpression.Type, nonNullableType))
				{
					flag2 = true;
					flag = true;
				}
			}
			BranchLabel branchLabel = this._instructions.MakeLabel();
			BranchLabel label = null;
			this.Compile(binaryExpression.Left);
			this._instructions.EmitCoalescingBranch(branchLabel);
			this._instructions.EmitPop();
			this.Compile(binaryExpression.Right);
			if (flag)
			{
				label = this._instructions.MakeLabel();
				this._instructions.EmitBranch(label);
			}
			this._instructions.MarkLabel(branchLabel);
			if (binaryExpression.Conversion != null)
			{
				ParameterExpression parameterExpression = Expression.Parameter(binaryExpression.Left.Type, "temp");
				LocalDefinition definition = this._locals.DefineLocal(parameterExpression, this._instructions.Count);
				this._instructions.EmitStoreLocal(definition.Index);
				this.CompileMethodCallExpression(Expression.Call(binaryExpression.Conversion, binaryExpression.Conversion.Type.GetInvokeMethod(), new ParameterExpression[]
				{
					parameterExpression
				}));
				this._locals.UndefineLocal(definition, this._instructions.Count);
			}
			else if (flag2)
			{
				Type nonNullableType2 = binaryExpression.Left.Type.GetNonNullableType();
				this.CompileConvertToType(nonNullableType2, binaryExpression.Type, true, false);
			}
			if (flag)
			{
				this._instructions.MarkLabel(label);
			}
		}

		// Token: 0x060016A9 RID: 5801 RVA: 0x000425AC File Offset: 0x000407AC
		private void CompileInvocationExpression(Expression expr)
		{
			InvocationExpression invocationExpression = (InvocationExpression)expr;
			if (typeof(LambdaExpression).IsAssignableFrom(invocationExpression.Expression.Type))
			{
				MethodInfo method = invocationExpression.Expression.Type.GetMethod("Compile", Array.Empty<Type>());
				this.CompileMethodCallExpression(Expression.Call(invocationExpression.Expression, method), method.ReturnType.GetInvokeMethod(), invocationExpression);
				return;
			}
			this.CompileMethodCallExpression(invocationExpression.Expression, invocationExpression.Expression.Type.GetInvokeMethod(), invocationExpression);
		}

		// Token: 0x060016AA RID: 5802 RVA: 0x00042634 File Offset: 0x00040834
		private void CompileListInitExpression(Expression expr)
		{
			ListInitExpression listInitExpression = (ListInitExpression)expr;
			this.EmitThisForMethodCall(listInitExpression.NewExpression);
			ReadOnlyCollection<ElementInit> initializers = listInitExpression.Initializers;
			this.CompileListInit(initializers);
		}

		// Token: 0x060016AB RID: 5803 RVA: 0x00042664 File Offset: 0x00040864
		private void CompileListInit(ReadOnlyCollection<ElementInit> initializers)
		{
			for (int i = 0; i < initializers.Count; i++)
			{
				ElementInit elementInit = initializers[i];
				this._instructions.EmitDup();
				foreach (Expression expr in elementInit.Arguments)
				{
					this.Compile(expr);
				}
				MethodInfo addMethod = elementInit.AddMethod;
				this._instructions.EmitCall(addMethod);
				if (addMethod.ReturnType != typeof(void))
				{
					this._instructions.EmitPop();
				}
			}
		}

		// Token: 0x060016AC RID: 5804 RVA: 0x00042710 File Offset: 0x00040910
		private void CompileMemberInitExpression(Expression expr)
		{
			MemberInitExpression memberInitExpression = (MemberInitExpression)expr;
			this.EmitThisForMethodCall(memberInitExpression.NewExpression);
			this.CompileMemberInit(memberInitExpression.Bindings);
		}

		// Token: 0x060016AD RID: 5805 RVA: 0x0004273C File Offset: 0x0004093C
		private void CompileMemberInit(ReadOnlyCollection<MemberBinding> bindings)
		{
			foreach (MemberBinding memberBinding in bindings)
			{
				switch (memberBinding.BindingType)
				{
				case MemberBindingType.Assignment:
					this._instructions.EmitDup();
					this.CompileMemberAssignment(true, ((MemberAssignment)memberBinding).Member, ((MemberAssignment)memberBinding).Expression, true);
					break;
				case MemberBindingType.MemberBinding:
				{
					MemberMemberBinding memberMemberBinding = (MemberMemberBinding)memberBinding;
					this._instructions.EmitDup();
					Type memberType = LightCompiler.GetMemberType(memberMemberBinding.Member);
					if (memberMemberBinding.Member is PropertyInfo && memberType.IsValueType)
					{
						throw Error.CannotAutoInitializeValueTypeMemberThroughProperty(memberMemberBinding.Bindings);
					}
					this.CompileMember(null, memberMemberBinding.Member, true);
					this.CompileMemberInit(memberMemberBinding.Bindings);
					this._instructions.EmitPop();
					break;
				}
				case MemberBindingType.ListBinding:
				{
					MemberListBinding memberListBinding = (MemberListBinding)memberBinding;
					this._instructions.EmitDup();
					this.CompileMember(null, memberListBinding.Member, true);
					this.CompileListInit(memberListBinding.Initializers);
					this._instructions.EmitPop();
					break;
				}
				}
			}
		}

		// Token: 0x060016AE RID: 5806 RVA: 0x00042880 File Offset: 0x00040A80
		private static Type GetMemberType(MemberInfo member)
		{
			FieldInfo fieldInfo = member as FieldInfo;
			if (fieldInfo != null)
			{
				return fieldInfo.FieldType;
			}
			PropertyInfo propertyInfo = member as PropertyInfo;
			if (propertyInfo != null)
			{
				return propertyInfo.PropertyType;
			}
			throw new InvalidOperationException("MemberNotFieldOrProperty");
		}

		// Token: 0x060016AF RID: 5807 RVA: 0x000428C8 File Offset: 0x00040AC8
		private void CompileQuoteUnaryExpression(Expression expr)
		{
			UnaryExpression unaryExpression = (UnaryExpression)expr;
			LightCompiler.QuoteVisitor quoteVisitor = new LightCompiler.QuoteVisitor();
			quoteVisitor.Visit(unaryExpression.Operand);
			Dictionary<ParameterExpression, LocalVariable> dictionary = new Dictionary<ParameterExpression, LocalVariable>();
			foreach (ParameterExpression parameterExpression in quoteVisitor._hoistedParameters)
			{
				this.EnsureAvailableForClosure(parameterExpression);
				dictionary[parameterExpression] = this.ResolveLocal(parameterExpression);
			}
			this._instructions.Emit(new QuoteInstruction(unaryExpression.Operand, (dictionary.Count > 0) ? dictionary : null));
		}

		// Token: 0x060016B0 RID: 5808 RVA: 0x0004296C File Offset: 0x00040B6C
		private void CompileUnboxUnaryExpression(Expression expr)
		{
			UnaryExpression unaryExpression = (UnaryExpression)expr;
			this.Compile(unaryExpression.Operand);
			if (unaryExpression.Type.IsValueType && !unaryExpression.Type.IsNullableType())
			{
				this._instructions.Emit(NullCheckInstruction.Instance);
			}
		}

		// Token: 0x060016B1 RID: 5809 RVA: 0x000429B8 File Offset: 0x00040BB8
		private void CompileTypeEqualExpression(Expression expr)
		{
			TypeBinaryExpression typeBinaryExpression = (TypeBinaryExpression)expr;
			this.Compile(typeBinaryExpression.Expression);
			if (typeBinaryExpression.Expression.Type == typeof(void))
			{
				this._instructions.EmitLoad(typeBinaryExpression.TypeOperand == typeof(void), typeof(bool));
				return;
			}
			this._instructions.EmitLoad(typeBinaryExpression.TypeOperand.GetNonNullableType());
			this._instructions.EmitTypeEquals();
		}

		// Token: 0x060016B2 RID: 5810 RVA: 0x00042A45 File Offset: 0x00040C45
		private void CompileTypeAsExpression(UnaryExpression node)
		{
			this.Compile(node.Operand);
			this._instructions.EmitTypeAs(node.Type);
		}

		// Token: 0x060016B3 RID: 5811 RVA: 0x00042A64 File Offset: 0x00040C64
		private void CompileTypeIsExpression(Expression expr)
		{
			TypeBinaryExpression typeBinaryExpression = (TypeBinaryExpression)expr;
			AnalyzeTypeIsResult analyzeTypeIsResult = ConstantCheck.AnalyzeTypeIs(typeBinaryExpression);
			this.Compile(typeBinaryExpression.Expression);
			if (analyzeTypeIsResult <= AnalyzeTypeIsResult.KnownTrue)
			{
				if (typeBinaryExpression.Expression.Type != typeof(void))
				{
					this._instructions.EmitPop();
				}
				this._instructions.EmitLoad(analyzeTypeIsResult == AnalyzeTypeIsResult.KnownTrue);
				return;
			}
			if (analyzeTypeIsResult == AnalyzeTypeIsResult.KnownAssignable)
			{
				this._instructions.EmitLoad(null);
				this._instructions.EmitNotEqual(typeof(object), false);
				return;
			}
			if (typeBinaryExpression.TypeOperand.IsValueType)
			{
				this._instructions.EmitLoad(typeBinaryExpression.TypeOperand.GetNonNullableType());
				this._instructions.EmitTypeEquals();
				return;
			}
			this._instructions.EmitTypeIs(typeBinaryExpression.TypeOperand);
		}

		// Token: 0x060016B4 RID: 5812 RVA: 0x00042B2F File Offset: 0x00040D2F
		private void Compile(Expression expr, bool asVoid)
		{
			if (asVoid)
			{
				this.CompileAsVoid(expr);
				return;
			}
			this.Compile(expr);
		}

		// Token: 0x060016B5 RID: 5813 RVA: 0x00042B44 File Offset: 0x00040D44
		private void CompileAsVoid(Expression expr)
		{
			bool flag = this.TryPushLabelBlock(expr);
			int currentStackDepth = this._instructions.CurrentStackDepth;
			ExpressionType nodeType = expr.NodeType;
			if (nodeType <= ExpressionType.Assign)
			{
				if (nodeType == ExpressionType.Constant || nodeType == ExpressionType.Parameter)
				{
					goto IL_89;
				}
				if (nodeType == ExpressionType.Assign)
				{
					this.CompileAssignBinaryExpression(expr, true);
					goto IL_89;
				}
			}
			else
			{
				if (nodeType == ExpressionType.Block)
				{
					this.CompileBlockExpression(expr, true);
					goto IL_89;
				}
				if (nodeType == ExpressionType.Default)
				{
					goto IL_89;
				}
				if (nodeType == ExpressionType.Throw)
				{
					this.CompileThrowUnaryExpression(expr, true);
					goto IL_89;
				}
			}
			this.CompileNoLabelPush(expr);
			if (expr.Type != typeof(void))
			{
				this._instructions.EmitPop();
			}
			IL_89:
			if (flag)
			{
				this.PopLabelBlock(this._labelBlock.Kind);
			}
		}

		// Token: 0x060016B6 RID: 5814 RVA: 0x00042BF0 File Offset: 0x00040DF0
		private void CompileNoLabelPush(Expression expr)
		{
			if (!this._guard.TryEnterOnCurrentStack())
			{
				this._guard.RunOnEmptyStack<LightCompiler, Expression>(delegate(LightCompiler @this, Expression e)
				{
					@this.CompileNoLabelPush(e);
				}, this, expr);
				return;
			}
			int currentStackDepth = this._instructions.CurrentStackDepth;
			switch (expr.NodeType)
			{
			case ExpressionType.Add:
			case ExpressionType.AddChecked:
			case ExpressionType.And:
			case ExpressionType.ArrayIndex:
			case ExpressionType.Divide:
			case ExpressionType.Equal:
			case ExpressionType.ExclusiveOr:
			case ExpressionType.GreaterThan:
			case ExpressionType.GreaterThanOrEqual:
			case ExpressionType.LeftShift:
			case ExpressionType.LessThan:
			case ExpressionType.LessThanOrEqual:
			case ExpressionType.Modulo:
			case ExpressionType.Multiply:
			case ExpressionType.MultiplyChecked:
			case ExpressionType.NotEqual:
			case ExpressionType.Or:
			case ExpressionType.Power:
			case ExpressionType.RightShift:
			case ExpressionType.Subtract:
			case ExpressionType.SubtractChecked:
				this.CompileBinaryExpression(expr);
				return;
			case ExpressionType.AndAlso:
				this.CompileAndAlsoBinaryExpression(expr);
				return;
			case ExpressionType.ArrayLength:
			case ExpressionType.Negate:
			case ExpressionType.UnaryPlus:
			case ExpressionType.NegateChecked:
			case ExpressionType.Not:
			case ExpressionType.TypeAs:
			case ExpressionType.Decrement:
			case ExpressionType.Increment:
			case ExpressionType.OnesComplement:
			case ExpressionType.IsTrue:
			case ExpressionType.IsFalse:
				this.CompileUnaryExpression(expr);
				return;
			case ExpressionType.Call:
				this.CompileMethodCallExpression(expr);
				return;
			case ExpressionType.Coalesce:
				this.CompileCoalesceBinaryExpression(expr);
				return;
			case ExpressionType.Conditional:
				this.CompileConditionalExpression(expr, expr.Type == typeof(void));
				return;
			case ExpressionType.Constant:
				this.CompileConstantExpression(expr);
				return;
			case ExpressionType.Convert:
			case ExpressionType.ConvertChecked:
				this.CompileConvertUnaryExpression(expr);
				return;
			case ExpressionType.Invoke:
				this.CompileInvocationExpression(expr);
				return;
			case ExpressionType.Lambda:
				this.CompileLambdaExpression(expr);
				return;
			case ExpressionType.ListInit:
				this.CompileListInitExpression(expr);
				return;
			case ExpressionType.MemberAccess:
				this.CompileMemberExpression(expr);
				return;
			case ExpressionType.MemberInit:
				this.CompileMemberInitExpression(expr);
				return;
			case ExpressionType.New:
				this.CompileNewExpression(expr);
				return;
			case ExpressionType.NewArrayInit:
			case ExpressionType.NewArrayBounds:
				this.CompileNewArrayExpression(expr);
				return;
			case ExpressionType.OrElse:
				this.CompileOrElseBinaryExpression(expr);
				return;
			case ExpressionType.Parameter:
				this.CompileParameterExpression(expr);
				return;
			case ExpressionType.Quote:
				this.CompileQuoteUnaryExpression(expr);
				return;
			case ExpressionType.TypeIs:
				this.CompileTypeIsExpression(expr);
				return;
			case ExpressionType.Assign:
				this.CompileAssignBinaryExpression(expr, expr.Type == typeof(void));
				return;
			case ExpressionType.Block:
				this.CompileBlockExpression(expr, expr.Type == typeof(void));
				return;
			case ExpressionType.DebugInfo:
				this.CompileDebugInfoExpression(expr);
				return;
			case ExpressionType.Default:
				this.CompileDefaultExpression(expr);
				return;
			case ExpressionType.Goto:
				this.CompileGotoExpression(expr);
				return;
			case ExpressionType.Index:
				this.CompileIndexExpression(expr);
				return;
			case ExpressionType.Label:
				this.CompileLabelExpression(expr);
				return;
			case ExpressionType.RuntimeVariables:
				this.CompileRuntimeVariablesExpression(expr);
				return;
			case ExpressionType.Loop:
				this.CompileLoopExpression(expr);
				return;
			case ExpressionType.Switch:
				this.CompileSwitchExpression(expr);
				return;
			case ExpressionType.Throw:
				this.CompileThrowUnaryExpression(expr, expr.Type == typeof(void));
				return;
			case ExpressionType.Try:
				this.CompileTryExpression(expr);
				return;
			case ExpressionType.Unbox:
				this.CompileUnboxUnaryExpression(expr);
				return;
			case ExpressionType.TypeEqual:
				this.CompileTypeEqualExpression(expr);
				return;
			}
			this.Compile(expr.ReduceAndCheck());
		}

		// Token: 0x060016B7 RID: 5815 RVA: 0x00042F11 File Offset: 0x00041111
		private void Compile(Expression expr)
		{
			bool flag = this.TryPushLabelBlock(expr);
			this.CompileNoLabelPush(expr);
			if (flag)
			{
				this.PopLabelBlock(this._labelBlock.Kind);
			}
		}

		// Token: 0x060016B8 RID: 5816 RVA: 0x00042F34 File Offset: 0x00041134
		// Note: this type is marked as 'beforefieldinit'.
		static LightCompiler()
		{
		}

		// Token: 0x04000B30 RID: 2864
		private readonly InstructionList _instructions;

		// Token: 0x04000B31 RID: 2865
		private readonly LocalVariables _locals = new LocalVariables();

		// Token: 0x04000B32 RID: 2866
		private readonly List<DebugInfo> _debugInfos = new List<DebugInfo>();

		// Token: 0x04000B33 RID: 2867
		private readonly HybridReferenceDictionary<LabelTarget, LabelInfo> _treeLabels = new HybridReferenceDictionary<LabelTarget, LabelInfo>();

		// Token: 0x04000B34 RID: 2868
		private LabelScopeInfo _labelBlock = new LabelScopeInfo(null, LabelScopeKind.Lambda);

		// Token: 0x04000B35 RID: 2869
		private readonly Stack<ParameterExpression> _exceptionForRethrowStack = new Stack<ParameterExpression>();

		// Token: 0x04000B36 RID: 2870
		private readonly LightCompiler _parent;

		// Token: 0x04000B37 RID: 2871
		private readonly StackGuard _guard = new StackGuard();

		// Token: 0x04000B38 RID: 2872
		private static readonly LocalDefinition[] s_emptyLocals = Array.Empty<LocalDefinition>();

		// Token: 0x0200035A RID: 858
		private sealed class QuoteVisitor : ExpressionVisitor
		{
			// Token: 0x060016B9 RID: 5817 RVA: 0x00042F40 File Offset: 0x00041140
			protected internal override Expression VisitParameter(ParameterExpression node)
			{
				if (!this._definedParameters.ContainsKey(node))
				{
					this._hoistedParameters.Add(node);
				}
				return node;
			}

			// Token: 0x060016BA RID: 5818 RVA: 0x00042F5E File Offset: 0x0004115E
			protected internal override Expression VisitBlock(BlockExpression node)
			{
				this.PushParameters(node.Variables);
				base.VisitBlock(node);
				this.PopParameters(node.Variables);
				return node;
			}

			// Token: 0x060016BB RID: 5819 RVA: 0x00042F84 File Offset: 0x00041184
			protected override CatchBlock VisitCatchBlock(CatchBlock node)
			{
				if (node.Variable != null)
				{
					this.PushParameters(new ParameterExpression[]
					{
						node.Variable
					});
				}
				this.Visit(node.Body);
				this.Visit(node.Filter);
				if (node.Variable != null)
				{
					this.PopParameters(new ParameterExpression[]
					{
						node.Variable
					});
				}
				return node;
			}

			// Token: 0x060016BC RID: 5820 RVA: 0x00042FE8 File Offset: 0x000411E8
			protected internal override Expression VisitLambda<T>(Expression<T> node)
			{
				IEnumerable<ParameterExpression> parameters = Array.Empty<ParameterExpression>();
				int parameterCount = node.ParameterCount;
				if (parameterCount > 0)
				{
					List<ParameterExpression> list = new List<ParameterExpression>(parameterCount);
					for (int i = 0; i < parameterCount; i++)
					{
						list.Add(node.GetParameter(i));
					}
					parameters = list;
				}
				this.PushParameters(parameters);
				base.VisitLambda<T>(node);
				this.PopParameters(parameters);
				return node;
			}

			// Token: 0x060016BD RID: 5821 RVA: 0x00043040 File Offset: 0x00041240
			private void PushParameters(IEnumerable<ParameterExpression> parameters)
			{
				foreach (ParameterExpression key in parameters)
				{
					int num;
					if (this._definedParameters.TryGetValue(key, out num))
					{
						this._definedParameters[key] = num + 1;
					}
					else
					{
						this._definedParameters[key] = 1;
					}
				}
			}

			// Token: 0x060016BE RID: 5822 RVA: 0x000430B0 File Offset: 0x000412B0
			private void PopParameters(IEnumerable<ParameterExpression> parameters)
			{
				foreach (ParameterExpression key in parameters)
				{
					int num = this._definedParameters[key];
					if (num == 0)
					{
						this._definedParameters.Remove(key);
					}
					else
					{
						this._definedParameters[key] = num - 1;
					}
				}
			}

			// Token: 0x060016BF RID: 5823 RVA: 0x00043120 File Offset: 0x00041320
			public QuoteVisitor()
			{
			}

			// Token: 0x04000B39 RID: 2873
			private readonly Dictionary<ParameterExpression, int> _definedParameters = new Dictionary<ParameterExpression, int>();

			// Token: 0x04000B3A RID: 2874
			public readonly HashSet<ParameterExpression> _hoistedParameters = new HashSet<ParameterExpression>();
		}

		// Token: 0x0200035B RID: 859
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x060016C0 RID: 5824 RVA: 0x0004313E File Offset: 0x0004133E
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x060016C1 RID: 5825 RVA: 0x00002310 File Offset: 0x00000510
			public <>c()
			{
			}

			// Token: 0x060016C2 RID: 5826 RVA: 0x0004314A File Offset: 0x0004134A
			internal bool <CompileSwitchExpression>b__56_0(SwitchCase c)
			{
				return c.TestValues.All((Expression t) => t is ConstantExpression);
			}

			// Token: 0x060016C3 RID: 5827 RVA: 0x00043176 File Offset: 0x00041376
			internal bool <CompileSwitchExpression>b__56_1(Expression t)
			{
				return t is ConstantExpression;
			}

			// Token: 0x060016C4 RID: 5828 RVA: 0x00043181 File Offset: 0x00041381
			internal void <CompileNoLabelPush>b__101_0(LightCompiler @this, Expression e)
			{
				@this.CompileNoLabelPush(e);
			}

			// Token: 0x04000B3B RID: 2875
			public static readonly LightCompiler.<>c <>9 = new LightCompiler.<>c();

			// Token: 0x04000B3C RID: 2876
			public static Func<Expression, bool> <>9__56_1;

			// Token: 0x04000B3D RID: 2877
			public static Func<SwitchCase, bool> <>9__56_0;

			// Token: 0x04000B3E RID: 2878
			public static Action<LightCompiler, Expression> <>9__101_0;
		}
	}
}
