﻿using System;
using System.Reflection;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002FD RID: 765
	internal abstract class FieldInstruction : Instruction
	{
		// Token: 0x060014CD RID: 5325 RVA: 0x0003AD8A File Offset: 0x00038F8A
		public FieldInstruction(FieldInfo field)
		{
			this._field = field;
		}

		// Token: 0x060014CE RID: 5326 RVA: 0x0003AD99 File Offset: 0x00038F99
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				this.InstructionName,
				"(",
				this._field,
				")"
			});
		}

		// Token: 0x04000A51 RID: 2641
		protected readonly FieldInfo _field;
	}
}
