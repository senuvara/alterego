﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000345 RID: 837
	internal abstract class LessThanOrEqualInstruction : Instruction
	{
		// Token: 0x17000462 RID: 1122
		// (get) Token: 0x0600162C RID: 5676 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x17000463 RID: 1123
		// (get) Token: 0x0600162D RID: 5677 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000464 RID: 1124
		// (get) Token: 0x0600162E RID: 5678 RVA: 0x0003E218 File Offset: 0x0003C418
		public override string InstructionName
		{
			get
			{
				return "LessThanOrEqual";
			}
		}

		// Token: 0x0600162F RID: 5679 RVA: 0x0003E21F File Offset: 0x0003C41F
		private LessThanOrEqualInstruction(object nullValue)
		{
			this._nullValue = nullValue;
		}

		// Token: 0x06001630 RID: 5680 RVA: 0x0003E230 File Offset: 0x0003C430
		public static Instruction Create(Type type, bool liftedToNull = false)
		{
			if (liftedToNull)
			{
				switch (type.GetNonNullableType().GetTypeCode())
				{
				case TypeCode.Char:
				{
					Instruction result;
					if ((result = LessThanOrEqualInstruction.s_liftedToNullChar) == null)
					{
						result = (LessThanOrEqualInstruction.s_liftedToNullChar = new LessThanOrEqualInstruction.LessThanOrEqualChar(null));
					}
					return result;
				}
				case TypeCode.SByte:
				{
					Instruction result2;
					if ((result2 = LessThanOrEqualInstruction.s_liftedToNullSByte) == null)
					{
						result2 = (LessThanOrEqualInstruction.s_liftedToNullSByte = new LessThanOrEqualInstruction.LessThanOrEqualSByte(null));
					}
					return result2;
				}
				case TypeCode.Byte:
				{
					Instruction result3;
					if ((result3 = LessThanOrEqualInstruction.s_liftedToNullByte) == null)
					{
						result3 = (LessThanOrEqualInstruction.s_liftedToNullByte = new LessThanOrEqualInstruction.LessThanOrEqualByte(null));
					}
					return result3;
				}
				case TypeCode.Int16:
				{
					Instruction result4;
					if ((result4 = LessThanOrEqualInstruction.s_liftedToNullInt16) == null)
					{
						result4 = (LessThanOrEqualInstruction.s_liftedToNullInt16 = new LessThanOrEqualInstruction.LessThanOrEqualInt16(null));
					}
					return result4;
				}
				case TypeCode.UInt16:
				{
					Instruction result5;
					if ((result5 = LessThanOrEqualInstruction.s_liftedToNullUInt16) == null)
					{
						result5 = (LessThanOrEqualInstruction.s_liftedToNullUInt16 = new LessThanOrEqualInstruction.LessThanOrEqualUInt16(null));
					}
					return result5;
				}
				case TypeCode.Int32:
				{
					Instruction result6;
					if ((result6 = LessThanOrEqualInstruction.s_liftedToNullInt32) == null)
					{
						result6 = (LessThanOrEqualInstruction.s_liftedToNullInt32 = new LessThanOrEqualInstruction.LessThanOrEqualInt32(null));
					}
					return result6;
				}
				case TypeCode.UInt32:
				{
					Instruction result7;
					if ((result7 = LessThanOrEqualInstruction.s_liftedToNullUInt32) == null)
					{
						result7 = (LessThanOrEqualInstruction.s_liftedToNullUInt32 = new LessThanOrEqualInstruction.LessThanOrEqualUInt32(null));
					}
					return result7;
				}
				case TypeCode.Int64:
				{
					Instruction result8;
					if ((result8 = LessThanOrEqualInstruction.s_liftedToNullInt64) == null)
					{
						result8 = (LessThanOrEqualInstruction.s_liftedToNullInt64 = new LessThanOrEqualInstruction.LessThanOrEqualInt64(null));
					}
					return result8;
				}
				case TypeCode.UInt64:
				{
					Instruction result9;
					if ((result9 = LessThanOrEqualInstruction.s_liftedToNullUInt64) == null)
					{
						result9 = (LessThanOrEqualInstruction.s_liftedToNullUInt64 = new LessThanOrEqualInstruction.LessThanOrEqualUInt64(null));
					}
					return result9;
				}
				case TypeCode.Single:
				{
					Instruction result10;
					if ((result10 = LessThanOrEqualInstruction.s_liftedToNullSingle) == null)
					{
						result10 = (LessThanOrEqualInstruction.s_liftedToNullSingle = new LessThanOrEqualInstruction.LessThanOrEqualSingle(null));
					}
					return result10;
				}
				case TypeCode.Double:
				{
					Instruction result11;
					if ((result11 = LessThanOrEqualInstruction.s_liftedToNullDouble) == null)
					{
						result11 = (LessThanOrEqualInstruction.s_liftedToNullDouble = new LessThanOrEqualInstruction.LessThanOrEqualDouble(null));
					}
					return result11;
				}
				default:
					throw ContractUtils.Unreachable;
				}
			}
			else
			{
				switch (type.GetNonNullableType().GetTypeCode())
				{
				case TypeCode.Char:
				{
					Instruction result12;
					if ((result12 = LessThanOrEqualInstruction.s_Char) == null)
					{
						result12 = (LessThanOrEqualInstruction.s_Char = new LessThanOrEqualInstruction.LessThanOrEqualChar(Utils.BoxedFalse));
					}
					return result12;
				}
				case TypeCode.SByte:
				{
					Instruction result13;
					if ((result13 = LessThanOrEqualInstruction.s_SByte) == null)
					{
						result13 = (LessThanOrEqualInstruction.s_SByte = new LessThanOrEqualInstruction.LessThanOrEqualSByte(Utils.BoxedFalse));
					}
					return result13;
				}
				case TypeCode.Byte:
				{
					Instruction result14;
					if ((result14 = LessThanOrEqualInstruction.s_Byte) == null)
					{
						result14 = (LessThanOrEqualInstruction.s_Byte = new LessThanOrEqualInstruction.LessThanOrEqualByte(Utils.BoxedFalse));
					}
					return result14;
				}
				case TypeCode.Int16:
				{
					Instruction result15;
					if ((result15 = LessThanOrEqualInstruction.s_Int16) == null)
					{
						result15 = (LessThanOrEqualInstruction.s_Int16 = new LessThanOrEqualInstruction.LessThanOrEqualInt16(Utils.BoxedFalse));
					}
					return result15;
				}
				case TypeCode.UInt16:
				{
					Instruction result16;
					if ((result16 = LessThanOrEqualInstruction.s_UInt16) == null)
					{
						result16 = (LessThanOrEqualInstruction.s_UInt16 = new LessThanOrEqualInstruction.LessThanOrEqualUInt16(Utils.BoxedFalse));
					}
					return result16;
				}
				case TypeCode.Int32:
				{
					Instruction result17;
					if ((result17 = LessThanOrEqualInstruction.s_Int32) == null)
					{
						result17 = (LessThanOrEqualInstruction.s_Int32 = new LessThanOrEqualInstruction.LessThanOrEqualInt32(Utils.BoxedFalse));
					}
					return result17;
				}
				case TypeCode.UInt32:
				{
					Instruction result18;
					if ((result18 = LessThanOrEqualInstruction.s_UInt32) == null)
					{
						result18 = (LessThanOrEqualInstruction.s_UInt32 = new LessThanOrEqualInstruction.LessThanOrEqualUInt32(Utils.BoxedFalse));
					}
					return result18;
				}
				case TypeCode.Int64:
				{
					Instruction result19;
					if ((result19 = LessThanOrEqualInstruction.s_Int64) == null)
					{
						result19 = (LessThanOrEqualInstruction.s_Int64 = new LessThanOrEqualInstruction.LessThanOrEqualInt64(Utils.BoxedFalse));
					}
					return result19;
				}
				case TypeCode.UInt64:
				{
					Instruction result20;
					if ((result20 = LessThanOrEqualInstruction.s_UInt64) == null)
					{
						result20 = (LessThanOrEqualInstruction.s_UInt64 = new LessThanOrEqualInstruction.LessThanOrEqualUInt64(Utils.BoxedFalse));
					}
					return result20;
				}
				case TypeCode.Single:
				{
					Instruction result21;
					if ((result21 = LessThanOrEqualInstruction.s_Single) == null)
					{
						result21 = (LessThanOrEqualInstruction.s_Single = new LessThanOrEqualInstruction.LessThanOrEqualSingle(Utils.BoxedFalse));
					}
					return result21;
				}
				case TypeCode.Double:
				{
					Instruction result22;
					if ((result22 = LessThanOrEqualInstruction.s_Double) == null)
					{
						result22 = (LessThanOrEqualInstruction.s_Double = new LessThanOrEqualInstruction.LessThanOrEqualDouble(Utils.BoxedFalse));
					}
					return result22;
				}
				default:
					throw ContractUtils.Unreachable;
				}
			}
		}

		// Token: 0x04000AFF RID: 2815
		private readonly object _nullValue;

		// Token: 0x04000B00 RID: 2816
		private static Instruction s_SByte;

		// Token: 0x04000B01 RID: 2817
		private static Instruction s_Int16;

		// Token: 0x04000B02 RID: 2818
		private static Instruction s_Char;

		// Token: 0x04000B03 RID: 2819
		private static Instruction s_Int32;

		// Token: 0x04000B04 RID: 2820
		private static Instruction s_Int64;

		// Token: 0x04000B05 RID: 2821
		private static Instruction s_Byte;

		// Token: 0x04000B06 RID: 2822
		private static Instruction s_UInt16;

		// Token: 0x04000B07 RID: 2823
		private static Instruction s_UInt32;

		// Token: 0x04000B08 RID: 2824
		private static Instruction s_UInt64;

		// Token: 0x04000B09 RID: 2825
		private static Instruction s_Single;

		// Token: 0x04000B0A RID: 2826
		private static Instruction s_Double;

		// Token: 0x04000B0B RID: 2827
		private static Instruction s_liftedToNullSByte;

		// Token: 0x04000B0C RID: 2828
		private static Instruction s_liftedToNullInt16;

		// Token: 0x04000B0D RID: 2829
		private static Instruction s_liftedToNullChar;

		// Token: 0x04000B0E RID: 2830
		private static Instruction s_liftedToNullInt32;

		// Token: 0x04000B0F RID: 2831
		private static Instruction s_liftedToNullInt64;

		// Token: 0x04000B10 RID: 2832
		private static Instruction s_liftedToNullByte;

		// Token: 0x04000B11 RID: 2833
		private static Instruction s_liftedToNullUInt16;

		// Token: 0x04000B12 RID: 2834
		private static Instruction s_liftedToNullUInt32;

		// Token: 0x04000B13 RID: 2835
		private static Instruction s_liftedToNullUInt64;

		// Token: 0x04000B14 RID: 2836
		private static Instruction s_liftedToNullSingle;

		// Token: 0x04000B15 RID: 2837
		private static Instruction s_liftedToNullDouble;

		// Token: 0x02000346 RID: 838
		private sealed class LessThanOrEqualSByte : LessThanOrEqualInstruction
		{
			// Token: 0x06001631 RID: 5681 RVA: 0x0003E4E8 File Offset: 0x0003C6E8
			public LessThanOrEqualSByte(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x06001632 RID: 5682 RVA: 0x0003E4F4 File Offset: 0x0003C6F4
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((sbyte)obj2 <= (sbyte)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000347 RID: 839
		private sealed class LessThanOrEqualInt16 : LessThanOrEqualInstruction
		{
			// Token: 0x06001633 RID: 5683 RVA: 0x0003E4E8 File Offset: 0x0003C6E8
			public LessThanOrEqualInt16(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x06001634 RID: 5684 RVA: 0x0003E53C File Offset: 0x0003C73C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((short)obj2 <= (short)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000348 RID: 840
		private sealed class LessThanOrEqualChar : LessThanOrEqualInstruction
		{
			// Token: 0x06001635 RID: 5685 RVA: 0x0003E4E8 File Offset: 0x0003C6E8
			public LessThanOrEqualChar(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x06001636 RID: 5686 RVA: 0x0003E584 File Offset: 0x0003C784
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((char)obj2 <= (char)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000349 RID: 841
		private sealed class LessThanOrEqualInt32 : LessThanOrEqualInstruction
		{
			// Token: 0x06001637 RID: 5687 RVA: 0x0003E4E8 File Offset: 0x0003C6E8
			public LessThanOrEqualInt32(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x06001638 RID: 5688 RVA: 0x0003E5CC File Offset: 0x0003C7CC
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((int)obj2 <= (int)obj);
				}
				return 1;
			}
		}

		// Token: 0x0200034A RID: 842
		private sealed class LessThanOrEqualInt64 : LessThanOrEqualInstruction
		{
			// Token: 0x06001639 RID: 5689 RVA: 0x0003E4E8 File Offset: 0x0003C6E8
			public LessThanOrEqualInt64(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x0600163A RID: 5690 RVA: 0x0003E614 File Offset: 0x0003C814
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((long)obj2 <= (long)obj);
				}
				return 1;
			}
		}

		// Token: 0x0200034B RID: 843
		private sealed class LessThanOrEqualByte : LessThanOrEqualInstruction
		{
			// Token: 0x0600163B RID: 5691 RVA: 0x0003E4E8 File Offset: 0x0003C6E8
			public LessThanOrEqualByte(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x0600163C RID: 5692 RVA: 0x0003E65C File Offset: 0x0003C85C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((byte)obj2 <= (byte)obj);
				}
				return 1;
			}
		}

		// Token: 0x0200034C RID: 844
		private sealed class LessThanOrEqualUInt16 : LessThanOrEqualInstruction
		{
			// Token: 0x0600163D RID: 5693 RVA: 0x0003E4E8 File Offset: 0x0003C6E8
			public LessThanOrEqualUInt16(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x0600163E RID: 5694 RVA: 0x0003E6A4 File Offset: 0x0003C8A4
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((ushort)obj2 <= (ushort)obj);
				}
				return 1;
			}
		}

		// Token: 0x0200034D RID: 845
		private sealed class LessThanOrEqualUInt32 : LessThanOrEqualInstruction
		{
			// Token: 0x0600163F RID: 5695 RVA: 0x0003E4E8 File Offset: 0x0003C6E8
			public LessThanOrEqualUInt32(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x06001640 RID: 5696 RVA: 0x0003E6EC File Offset: 0x0003C8EC
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((uint)obj2 <= (uint)obj);
				}
				return 1;
			}
		}

		// Token: 0x0200034E RID: 846
		private sealed class LessThanOrEqualUInt64 : LessThanOrEqualInstruction
		{
			// Token: 0x06001641 RID: 5697 RVA: 0x0003E4E8 File Offset: 0x0003C6E8
			public LessThanOrEqualUInt64(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x06001642 RID: 5698 RVA: 0x0003E734 File Offset: 0x0003C934
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((ulong)obj2 <= (ulong)obj);
				}
				return 1;
			}
		}

		// Token: 0x0200034F RID: 847
		private sealed class LessThanOrEqualSingle : LessThanOrEqualInstruction
		{
			// Token: 0x06001643 RID: 5699 RVA: 0x0003E4E8 File Offset: 0x0003C6E8
			public LessThanOrEqualSingle(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x06001644 RID: 5700 RVA: 0x0003E77C File Offset: 0x0003C97C
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((float)obj2 <= (float)obj);
				}
				return 1;
			}
		}

		// Token: 0x02000350 RID: 848
		private sealed class LessThanOrEqualDouble : LessThanOrEqualInstruction
		{
			// Token: 0x06001645 RID: 5701 RVA: 0x0003E4E8 File Offset: 0x0003C6E8
			public LessThanOrEqualDouble(object nullValue) : base(nullValue)
			{
			}

			// Token: 0x06001646 RID: 5702 RVA: 0x0003E7C4 File Offset: 0x0003C9C4
			public override int Run(InterpretedFrame frame)
			{
				object obj = frame.Pop();
				object obj2 = frame.Pop();
				if (obj2 == null || obj == null)
				{
					frame.Push(this._nullValue);
				}
				else
				{
					frame.Push((double)obj2 <= (double)obj);
				}
				return 1;
			}
		}
	}
}
