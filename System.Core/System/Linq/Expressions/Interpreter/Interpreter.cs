﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200032B RID: 811
	internal sealed class Interpreter
	{
		// Token: 0x060015DF RID: 5599 RVA: 0x0003D3D0 File Offset: 0x0003B5D0
		internal Interpreter(string name, LocalVariables locals, InstructionArray instructions, DebugInfo[] debugInfos)
		{
			this.Name = name;
			this.LocalCount = locals.LocalCount;
			this.ClosureVariables = locals.ClosureVariables;
			this._instructions = instructions;
			this._objects = instructions.Objects;
			this._labels = instructions.Labels;
			this._debugInfos = debugInfos;
		}

		// Token: 0x17000454 RID: 1108
		// (get) Token: 0x060015E0 RID: 5600 RVA: 0x0003D429 File Offset: 0x0003B629
		internal string Name
		{
			[CompilerGenerated]
			get
			{
				return this.<Name>k__BackingField;
			}
		}

		// Token: 0x17000455 RID: 1109
		// (get) Token: 0x060015E1 RID: 5601 RVA: 0x0003D431 File Offset: 0x0003B631
		internal int LocalCount
		{
			[CompilerGenerated]
			get
			{
				return this.<LocalCount>k__BackingField;
			}
		}

		// Token: 0x17000456 RID: 1110
		// (get) Token: 0x060015E2 RID: 5602 RVA: 0x0003D439 File Offset: 0x0003B639
		internal int ClosureSize
		{
			get
			{
				Dictionary<ParameterExpression, LocalVariable> closureVariables = this.ClosureVariables;
				if (closureVariables == null)
				{
					return 0;
				}
				return closureVariables.Count;
			}
		}

		// Token: 0x17000457 RID: 1111
		// (get) Token: 0x060015E3 RID: 5603 RVA: 0x0003D44C File Offset: 0x0003B64C
		internal InstructionArray Instructions
		{
			get
			{
				return this._instructions;
			}
		}

		// Token: 0x17000458 RID: 1112
		// (get) Token: 0x060015E4 RID: 5604 RVA: 0x0003D454 File Offset: 0x0003B654
		internal Dictionary<ParameterExpression, LocalVariable> ClosureVariables
		{
			[CompilerGenerated]
			get
			{
				return this.<ClosureVariables>k__BackingField;
			}
		}

		// Token: 0x060015E5 RID: 5605 RVA: 0x0003D45C File Offset: 0x0003B65C
		[MethodImpl(MethodImplOptions.NoInlining)]
		public void Run(InterpretedFrame frame)
		{
			Instruction[] instructions = this._instructions.Instructions;
			int i = frame.InstructionIndex;
			while (i < instructions.Length)
			{
				i += instructions[i].Run(frame);
				frame.InstructionIndex = i;
			}
		}

		// Token: 0x060015E6 RID: 5606 RVA: 0x0003D497 File Offset: 0x0003B697
		// Note: this type is marked as 'beforefieldinit'.
		static Interpreter()
		{
		}

		// Token: 0x04000AC3 RID: 2755
		internal static readonly object NoValue = new object();

		// Token: 0x04000AC4 RID: 2756
		internal const int RethrowOnReturn = 2147483647;

		// Token: 0x04000AC5 RID: 2757
		private readonly InstructionArray _instructions;

		// Token: 0x04000AC6 RID: 2758
		internal readonly object[] _objects;

		// Token: 0x04000AC7 RID: 2759
		internal readonly RuntimeLabel[] _labels;

		// Token: 0x04000AC8 RID: 2760
		internal readonly DebugInfo[] _debugInfos;

		// Token: 0x04000AC9 RID: 2761
		[CompilerGenerated]
		private readonly string <Name>k__BackingField;

		// Token: 0x04000ACA RID: 2762
		[CompilerGenerated]
		private readonly int <LocalCount>k__BackingField;

		// Token: 0x04000ACB RID: 2763
		[CompilerGenerated]
		private readonly Dictionary<ParameterExpression, LocalVariable> <ClosureVariables>k__BackingField;
	}
}
