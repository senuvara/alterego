﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002BC RID: 700
	internal sealed class LeaveFinallyInstruction : Instruction
	{
		// Token: 0x06001418 RID: 5144 RVA: 0x00037C51 File Offset: 0x00035E51
		private LeaveFinallyInstruction()
		{
		}

		// Token: 0x1700040B RID: 1035
		// (get) Token: 0x06001419 RID: 5145 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x1700040C RID: 1036
		// (get) Token: 0x0600141A RID: 5146 RVA: 0x0003970B File Offset: 0x0003790B
		public override string InstructionName
		{
			get
			{
				return "LeaveFinally";
			}
		}

		// Token: 0x0600141B RID: 5147 RVA: 0x00039712 File Offset: 0x00037912
		public override int Run(InterpretedFrame frame)
		{
			frame.PopPendingContinuation();
			if (!frame.IsJumpHappened())
			{
				return 1;
			}
			return frame.YieldToPendingContinuation();
		}

		// Token: 0x0600141C RID: 5148 RVA: 0x0003972A File Offset: 0x0003792A
		// Note: this type is marked as 'beforefieldinit'.
		static LeaveFinallyInstruction()
		{
		}

		// Token: 0x04000A0A RID: 2570
		internal static readonly Instruction Instance = new LeaveFinallyInstruction();
	}
}
