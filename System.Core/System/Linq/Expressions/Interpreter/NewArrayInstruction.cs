﻿using System;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002A7 RID: 679
	internal sealed class NewArrayInstruction : Instruction
	{
		// Token: 0x0600139F RID: 5023 RVA: 0x000386C3 File Offset: 0x000368C3
		internal NewArrayInstruction(Type elementType)
		{
			this._elementType = elementType;
		}

		// Token: 0x170003D9 RID: 985
		// (get) Token: 0x060013A0 RID: 5024 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ConsumedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170003DA RID: 986
		// (get) Token: 0x060013A1 RID: 5025 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170003DB RID: 987
		// (get) Token: 0x060013A2 RID: 5026 RVA: 0x000386D2 File Offset: 0x000368D2
		public override string InstructionName
		{
			get
			{
				return "NewArray";
			}
		}

		// Token: 0x060013A3 RID: 5027 RVA: 0x000386DC File Offset: 0x000368DC
		public override int Run(InterpretedFrame frame)
		{
			int num = ConvertHelper.ToInt32NoNull(frame.Pop());
			frame.Push((num < 0) ? new int[num] : Array.CreateInstance(this._elementType, num));
			return 1;
		}

		// Token: 0x040009E3 RID: 2531
		private readonly Type _elementType;
	}
}
