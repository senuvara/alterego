﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x02000295 RID: 661
	internal abstract class AddOvfInstruction : Instruction
	{
		// Token: 0x170003D0 RID: 976
		// (get) Token: 0x06001372 RID: 4978 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ConsumedStack
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x170003D1 RID: 977
		// (get) Token: 0x06001373 RID: 4979 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ProducedStack
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x170003D2 RID: 978
		// (get) Token: 0x06001374 RID: 4980 RVA: 0x00037FF5 File Offset: 0x000361F5
		public override string InstructionName
		{
			get
			{
				return "AddOvf";
			}
		}

		// Token: 0x06001375 RID: 4981 RVA: 0x00037C51 File Offset: 0x00035E51
		private AddOvfInstruction()
		{
		}

		// Token: 0x06001376 RID: 4982 RVA: 0x00037FFC File Offset: 0x000361FC
		public static Instruction Create(Type type)
		{
			switch (type.GetNonNullableType().GetTypeCode())
			{
			case TypeCode.Int16:
			{
				Instruction result;
				if ((result = AddOvfInstruction.s_Int16) == null)
				{
					result = (AddOvfInstruction.s_Int16 = new AddOvfInstruction.AddOvfInt16());
				}
				return result;
			}
			case TypeCode.UInt16:
			{
				Instruction result2;
				if ((result2 = AddOvfInstruction.s_UInt16) == null)
				{
					result2 = (AddOvfInstruction.s_UInt16 = new AddOvfInstruction.AddOvfUInt16());
				}
				return result2;
			}
			case TypeCode.Int32:
			{
				Instruction result3;
				if ((result3 = AddOvfInstruction.s_Int32) == null)
				{
					result3 = (AddOvfInstruction.s_Int32 = new AddOvfInstruction.AddOvfInt32());
				}
				return result3;
			}
			case TypeCode.UInt32:
			{
				Instruction result4;
				if ((result4 = AddOvfInstruction.s_UInt32) == null)
				{
					result4 = (AddOvfInstruction.s_UInt32 = new AddOvfInstruction.AddOvfUInt32());
				}
				return result4;
			}
			case TypeCode.Int64:
			{
				Instruction result5;
				if ((result5 = AddOvfInstruction.s_Int64) == null)
				{
					result5 = (AddOvfInstruction.s_Int64 = new AddOvfInstruction.AddOvfInt64());
				}
				return result5;
			}
			case TypeCode.UInt64:
			{
				Instruction result6;
				if ((result6 = AddOvfInstruction.s_UInt64) == null)
				{
					result6 = (AddOvfInstruction.s_UInt64 = new AddOvfInstruction.AddOvfUInt64());
				}
				return result6;
			}
			default:
				return AddInstruction.Create(type);
			}
		}

		// Token: 0x040009D2 RID: 2514
		private static Instruction s_Int16;

		// Token: 0x040009D3 RID: 2515
		private static Instruction s_Int32;

		// Token: 0x040009D4 RID: 2516
		private static Instruction s_Int64;

		// Token: 0x040009D5 RID: 2517
		private static Instruction s_UInt16;

		// Token: 0x040009D6 RID: 2518
		private static Instruction s_UInt32;

		// Token: 0x040009D7 RID: 2519
		private static Instruction s_UInt64;

		// Token: 0x02000296 RID: 662
		private sealed class AddOvfInt16 : AddOvfInstruction
		{
			// Token: 0x06001377 RID: 4983 RVA: 0x000380BC File Offset: 0x000362BC
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : checked((short)obj + (short)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x06001378 RID: 4984 RVA: 0x0003810E File Offset: 0x0003630E
			public AddOvfInt16()
			{
			}
		}

		// Token: 0x02000297 RID: 663
		private sealed class AddOvfInt32 : AddOvfInstruction
		{
			// Token: 0x06001379 RID: 4985 RVA: 0x00038118 File Offset: 0x00036318
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : ScriptingRuntimeHelpers.Int32ToObject(checked((int)obj + (int)obj2)));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x0600137A RID: 4986 RVA: 0x0003810E File Offset: 0x0003630E
			public AddOvfInt32()
			{
			}
		}

		// Token: 0x02000298 RID: 664
		private sealed class AddOvfInt64 : AddOvfInstruction
		{
			// Token: 0x0600137B RID: 4987 RVA: 0x0003816C File Offset: 0x0003636C
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : checked((long)obj + (long)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x0600137C RID: 4988 RVA: 0x0003810E File Offset: 0x0003630E
			public AddOvfInt64()
			{
			}
		}

		// Token: 0x02000299 RID: 665
		private sealed class AddOvfUInt16 : AddOvfInstruction
		{
			// Token: 0x0600137D RID: 4989 RVA: 0x000381C0 File Offset: 0x000363C0
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : checked((ushort)obj + (ushort)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x0600137E RID: 4990 RVA: 0x0003810E File Offset: 0x0003630E
			public AddOvfUInt16()
			{
			}
		}

		// Token: 0x0200029A RID: 666
		private sealed class AddOvfUInt32 : AddOvfInstruction
		{
			// Token: 0x0600137F RID: 4991 RVA: 0x00038214 File Offset: 0x00036414
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : checked((uint)obj + (uint)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x06001380 RID: 4992 RVA: 0x0003810E File Offset: 0x0003630E
			public AddOvfUInt32()
			{
			}
		}

		// Token: 0x0200029B RID: 667
		private sealed class AddOvfUInt64 : AddOvfInstruction
		{
			// Token: 0x06001381 RID: 4993 RVA: 0x00038268 File Offset: 0x00036468
			public override int Run(InterpretedFrame frame)
			{
				int stackIndex = frame.StackIndex;
				object[] data = frame.Data;
				object obj = data[stackIndex - 2];
				if (obj != null)
				{
					object obj2 = data[stackIndex - 1];
					data[stackIndex - 2] = ((obj2 == null) ? null : checked((ulong)obj + (ulong)obj2));
				}
				frame.StackIndex = stackIndex - 1;
				return 1;
			}

			// Token: 0x06001382 RID: 4994 RVA: 0x0003810E File Offset: 0x0003630E
			public AddOvfUInt64()
			{
			}
		}
	}
}
