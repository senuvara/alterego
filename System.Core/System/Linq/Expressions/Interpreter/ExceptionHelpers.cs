﻿using System;
using System.Reflection;
using System.Runtime.ExceptionServices;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x0200040D RID: 1037
	internal static class ExceptionHelpers
	{
		// Token: 0x0600191E RID: 6430 RVA: 0x0004A519 File Offset: 0x00048719
		public static void UnwrapAndRethrow(TargetInvocationException exception)
		{
			ExceptionDispatchInfo.Throw(exception.InnerException);
		}
	}
}
