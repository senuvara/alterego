﻿using System;
using System.Dynamic.Utils;
using System.Reflection;

namespace System.Linq.Expressions.Interpreter
{
	// Token: 0x020002B0 RID: 688
	internal class MethodInfoCallInstruction : CallInstruction
	{
		// Token: 0x170003ED RID: 1005
		// (get) Token: 0x060013D2 RID: 5074 RVA: 0x00038C4F File Offset: 0x00036E4F
		public override int ArgumentCount
		{
			get
			{
				return this._argumentCount;
			}
		}

		// Token: 0x060013D3 RID: 5075 RVA: 0x00038C57 File Offset: 0x00036E57
		internal MethodInfoCallInstruction(MethodInfo target, int argumentCount)
		{
			this._target = target;
			this._argumentCount = argumentCount;
		}

		// Token: 0x170003EE RID: 1006
		// (get) Token: 0x060013D4 RID: 5076 RVA: 0x00038C6D File Offset: 0x00036E6D
		public override int ProducedStack
		{
			get
			{
				if (!(this._target.ReturnType == typeof(void)))
				{
					return 1;
				}
				return 0;
			}
		}

		// Token: 0x060013D5 RID: 5077 RVA: 0x00038C90 File Offset: 0x00036E90
		public override int Run(InterpretedFrame frame)
		{
			int num = frame.StackIndex - this._argumentCount;
			object obj;
			if (this._target.IsStatic)
			{
				object[] args = this.GetArgs(frame, num, 0);
				try
				{
					obj = this._target.Invoke(null, args);
					goto IL_8E;
				}
				catch (TargetInvocationException exception)
				{
					ExceptionHelpers.UnwrapAndRethrow(exception);
					throw ContractUtils.Unreachable;
				}
			}
			object obj2 = frame.Data[num];
			Instruction.NullCheck(obj2);
			object[] args2 = this.GetArgs(frame, num, 1);
			LightLambda targetLambda;
			if (CallInstruction.TryGetLightLambdaTarget(obj2, out targetLambda))
			{
				obj = base.InterpretLambdaInvoke(targetLambda, args2);
			}
			else
			{
				try
				{
					obj = this._target.Invoke(obj2, args2);
				}
				catch (TargetInvocationException exception2)
				{
					ExceptionHelpers.UnwrapAndRethrow(exception2);
					throw ContractUtils.Unreachable;
				}
			}
			IL_8E:
			if (this._target.ReturnType != typeof(void))
			{
				frame.Data[num] = obj;
				frame.StackIndex = num + 1;
			}
			else
			{
				frame.StackIndex = num;
			}
			return 1;
		}

		// Token: 0x060013D6 RID: 5078 RVA: 0x00038D80 File Offset: 0x00036F80
		protected object[] GetArgs(InterpretedFrame frame, int first, int skip)
		{
			int num = this._argumentCount - skip;
			if (num > 0)
			{
				object[] array = new object[num];
				for (int i = 0; i < array.Length; i++)
				{
					array[i] = frame.Data[first + i + skip];
				}
				return array;
			}
			return Array.Empty<object>();
		}

		// Token: 0x060013D7 RID: 5079 RVA: 0x00038DC5 File Offset: 0x00036FC5
		public override string ToString()
		{
			return "Call(" + this._target + ")";
		}

		// Token: 0x040009F3 RID: 2547
		protected readonly MethodInfo _target;

		// Token: 0x040009F4 RID: 2548
		protected readonly int _argumentCount;
	}
}
