﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;

namespace System.Linq.Expressions
{
	// Token: 0x0200021D RID: 541
	internal class ScopeN : ScopeExpression
	{
		// Token: 0x06000ECC RID: 3788 RVA: 0x0002F395 File Offset: 0x0002D595
		internal ScopeN(IReadOnlyList<ParameterExpression> variables, IReadOnlyList<Expression> body) : base(variables)
		{
			this._body = body;
		}

		// Token: 0x06000ECD RID: 3789 RVA: 0x0002F3A5 File Offset: 0x0002D5A5
		internal override bool SameExpressions(ICollection<Expression> expressions)
		{
			return ExpressionUtils.SameElements<Expression>(expressions, this._body);
		}

		// Token: 0x170002A5 RID: 677
		// (get) Token: 0x06000ECE RID: 3790 RVA: 0x0002F3B3 File Offset: 0x0002D5B3
		protected IReadOnlyList<Expression> Body
		{
			get
			{
				return this._body;
			}
		}

		// Token: 0x06000ECF RID: 3791 RVA: 0x0002F3BB File Offset: 0x0002D5BB
		internal override Expression GetExpression(int index)
		{
			return this._body[index];
		}

		// Token: 0x170002A6 RID: 678
		// (get) Token: 0x06000ED0 RID: 3792 RVA: 0x0002F3C9 File Offset: 0x0002D5C9
		internal override int ExpressionCount
		{
			get
			{
				return this._body.Count;
			}
		}

		// Token: 0x06000ED1 RID: 3793 RVA: 0x0002F3D6 File Offset: 0x0002D5D6
		internal override ReadOnlyCollection<Expression> GetOrMakeExpressions()
		{
			return ExpressionUtils.ReturnReadOnly<Expression>(ref this._body);
		}

		// Token: 0x06000ED2 RID: 3794 RVA: 0x0002F3E3 File Offset: 0x0002D5E3
		internal override BlockExpression Rewrite(ReadOnlyCollection<ParameterExpression> variables, Expression[] args)
		{
			if (args == null)
			{
				Expression.ValidateVariables(variables, "variables");
				return new ScopeN(variables, this._body);
			}
			return new ScopeN(base.ReuseOrValidateVariables(variables), args);
		}

		// Token: 0x04000892 RID: 2194
		private IReadOnlyList<Expression> _body;
	}
}
