﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Represents an unconditional jump. This includes return statements, break and continue statements, and other jumps.</summary>
	// Token: 0x02000243 RID: 579
	[DebuggerTypeProxy(typeof(Expression.GotoExpressionProxy))]
	public sealed class GotoExpression : Expression
	{
		// Token: 0x060010F5 RID: 4341 RVA: 0x00034839 File Offset: 0x00032A39
		internal GotoExpression(GotoExpressionKind kind, LabelTarget target, Expression value, Type type)
		{
			this.Kind = kind;
			this.Value = value;
			this.Target = target;
			this.Type = type;
		}

		/// <summary>Gets the static type of the expression that this <see cref="T:System.Linq.Expressions.Expression" /> represents.</summary>
		/// <returns>The <see cref="P:System.Linq.Expressions.GotoExpression.Type" /> that represents the static type of the expression.</returns>
		// Token: 0x170002E6 RID: 742
		// (get) Token: 0x060010F6 RID: 4342 RVA: 0x0003485E File Offset: 0x00032A5E
		public sealed override Type Type
		{
			[CompilerGenerated]
			get
			{
				return this.<Type>k__BackingField;
			}
		}

		/// <summary>Returns the node type of this <see cref="T:System.Linq.Expressions.Expression" />.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.ExpressionType" /> that represents this expression.</returns>
		// Token: 0x170002E7 RID: 743
		// (get) Token: 0x060010F7 RID: 4343 RVA: 0x00034866 File Offset: 0x00032A66
		public sealed override ExpressionType NodeType
		{
			get
			{
				return ExpressionType.Goto;
			}
		}

		/// <summary>The value passed to the target, or null if the target is of type System.Void.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.Expression" /> object representing the value passed to the target or null.</returns>
		// Token: 0x170002E8 RID: 744
		// (get) Token: 0x060010F8 RID: 4344 RVA: 0x0003486A File Offset: 0x00032A6A
		public Expression Value
		{
			[CompilerGenerated]
			get
			{
				return this.<Value>k__BackingField;
			}
		}

		/// <summary>The target label where this node jumps to.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.LabelTarget" /> object representing the target label for this node.</returns>
		// Token: 0x170002E9 RID: 745
		// (get) Token: 0x060010F9 RID: 4345 RVA: 0x00034872 File Offset: 0x00032A72
		public LabelTarget Target
		{
			[CompilerGenerated]
			get
			{
				return this.<Target>k__BackingField;
			}
		}

		/// <summary>The kind of the "go to" expression. Serves information purposes only.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.GotoExpressionKind" /> object representing the kind of the "go to" expression.</returns>
		// Token: 0x170002EA RID: 746
		// (get) Token: 0x060010FA RID: 4346 RVA: 0x0003487A File Offset: 0x00032A7A
		public GotoExpressionKind Kind
		{
			[CompilerGenerated]
			get
			{
				return this.<Kind>k__BackingField;
			}
		}

		// Token: 0x060010FB RID: 4347 RVA: 0x00034882 File Offset: 0x00032A82
		protected internal override Expression Accept(ExpressionVisitor visitor)
		{
			return visitor.VisitGoto(this);
		}

		/// <summary>Creates a new expression that is like this one, but using the supplied children. If all of the children are the same, it will return this expression.</summary>
		/// <param name="target">The <see cref="P:System.Linq.Expressions.GotoExpression.Target" /> property of the result. </param>
		/// <param name="value">The <see cref="P:System.Linq.Expressions.GotoExpression.Value" /> property of the result. </param>
		/// <returns>This expression if no children are changed or an expression with the updated children.</returns>
		// Token: 0x060010FC RID: 4348 RVA: 0x0003488B File Offset: 0x00032A8B
		public GotoExpression Update(LabelTarget target, Expression value)
		{
			if (target == this.Target && value == this.Value)
			{
				return this;
			}
			return Expression.MakeGoto(this.Kind, target, value, this.Type);
		}

		// Token: 0x060010FD RID: 4349 RVA: 0x0000220F File Offset: 0x0000040F
		internal GotoExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000930 RID: 2352
		[CompilerGenerated]
		private readonly Type <Type>k__BackingField;

		// Token: 0x04000931 RID: 2353
		[CompilerGenerated]
		private readonly Expression <Value>k__BackingField;

		// Token: 0x04000932 RID: 2354
		[CompilerGenerated]
		private readonly LabelTarget <Target>k__BackingField;

		// Token: 0x04000933 RID: 2355
		[CompilerGenerated]
		private readonly GotoExpressionKind <Kind>k__BackingField;
	}
}
