﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Dynamic.Utils;
using System.Reflection;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Represents indexing a property or array.</summary>
	// Token: 0x02000247 RID: 583
	[DebuggerTypeProxy(typeof(Expression.IndexExpressionProxy))]
	public sealed class IndexExpression : Expression, IArgumentProvider
	{
		// Token: 0x06001105 RID: 4357 RVA: 0x000348B4 File Offset: 0x00032AB4
		internal IndexExpression(Expression instance, PropertyInfo indexer, IReadOnlyList<Expression> arguments)
		{
			indexer == null;
			this.Object = instance;
			this.Indexer = indexer;
			this._arguments = arguments;
		}

		/// <summary>Returns the node type of this <see cref="T:System.Linq.Expressions.Expression" />.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.ExpressionType" /> that represents this expression.</returns>
		// Token: 0x170002EE RID: 750
		// (get) Token: 0x06001106 RID: 4358 RVA: 0x000348D9 File Offset: 0x00032AD9
		public sealed override ExpressionType NodeType
		{
			get
			{
				return ExpressionType.Index;
			}
		}

		/// <summary>Gets the static type of the expression that this <see cref="T:System.Linq.Expressions.Expression" /> represents.</summary>
		/// <returns>The <see cref="P:System.Linq.Expressions.IndexExpression.Type" /> that represents the static type of the expression.</returns>
		// Token: 0x170002EF RID: 751
		// (get) Token: 0x06001107 RID: 4359 RVA: 0x000348DD File Offset: 0x00032ADD
		public sealed override Type Type
		{
			get
			{
				if (this.Indexer != null)
				{
					return this.Indexer.PropertyType;
				}
				return this.Object.Type.GetElementType();
			}
		}

		/// <summary>An object to index.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.Expression" /> representing the object to index.</returns>
		// Token: 0x170002F0 RID: 752
		// (get) Token: 0x06001108 RID: 4360 RVA: 0x00034909 File Offset: 0x00032B09
		public Expression Object
		{
			[CompilerGenerated]
			get
			{
				return this.<Object>k__BackingField;
			}
		}

		/// <summary>Gets the <see cref="T:System.Reflection.PropertyInfo" /> for the property if the expression represents an indexed property, returns null otherwise.</summary>
		/// <returns>The <see cref="T:System.Reflection.PropertyInfo" /> for the property if the expression represents an indexed property, otherwise null.</returns>
		// Token: 0x170002F1 RID: 753
		// (get) Token: 0x06001109 RID: 4361 RVA: 0x00034911 File Offset: 0x00032B11
		public PropertyInfo Indexer
		{
			[CompilerGenerated]
			get
			{
				return this.<Indexer>k__BackingField;
			}
		}

		/// <summary>Gets the arguments that will be used to index the property or array.</summary>
		/// <returns>The read-only collection containing the arguments that will be used to index the property or array.</returns>
		// Token: 0x170002F2 RID: 754
		// (get) Token: 0x0600110A RID: 4362 RVA: 0x00034919 File Offset: 0x00032B19
		public ReadOnlyCollection<Expression> Arguments
		{
			get
			{
				return ExpressionUtils.ReturnReadOnly<Expression>(ref this._arguments);
			}
		}

		/// <summary>Creates a new expression that is like this one, but using the supplied children. If all of the children are the same, it will return this expression.</summary>
		/// <param name="object">The <see cref="P:System.Linq.Expressions.IndexExpression.Object" /> property of the result.</param>
		/// <param name="arguments">The <see cref="P:System.Linq.Expressions.IndexExpression.Arguments" /> property of the result.</param>
		/// <returns>This expression if no children are changed or an expression with the updated children.</returns>
		// Token: 0x0600110B RID: 4363 RVA: 0x00034926 File Offset: 0x00032B26
		public IndexExpression Update(Expression @object, IEnumerable<Expression> arguments)
		{
			if ((@object == this.Object & arguments != null) && ExpressionUtils.SameElements<Expression>(ref arguments, this.Arguments))
			{
				return this;
			}
			return Expression.MakeIndex(@object, this.Indexer, arguments);
		}

		// Token: 0x0600110C RID: 4364 RVA: 0x00034956 File Offset: 0x00032B56
		public Expression GetArgument(int index)
		{
			return this._arguments[index];
		}

		// Token: 0x170002F3 RID: 755
		// (get) Token: 0x0600110D RID: 4365 RVA: 0x00034964 File Offset: 0x00032B64
		public int ArgumentCount
		{
			get
			{
				return this._arguments.Count;
			}
		}

		// Token: 0x0600110E RID: 4366 RVA: 0x00034971 File Offset: 0x00032B71
		protected internal override Expression Accept(ExpressionVisitor visitor)
		{
			return visitor.VisitIndex(this);
		}

		// Token: 0x0600110F RID: 4367 RVA: 0x0003497C File Offset: 0x00032B7C
		internal Expression Rewrite(Expression instance, Expression[] arguments)
		{
			return Expression.MakeIndex(instance, this.Indexer, arguments ?? this._arguments);
		}

		// Token: 0x06001110 RID: 4368 RVA: 0x0000220F File Offset: 0x0000040F
		internal IndexExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000934 RID: 2356
		private IReadOnlyList<Expression> _arguments;

		// Token: 0x04000935 RID: 2357
		[CompilerGenerated]
		private readonly Expression <Object>k__BackingField;

		// Token: 0x04000936 RID: 2358
		[CompilerGenerated]
		private readonly PropertyInfo <Indexer>k__BackingField;
	}
}
