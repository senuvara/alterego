﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Dynamic.Utils;
using System.Reflection;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Represents a control expression that handles multiple selections by passing control to <see cref="T:System.Linq.Expressions.SwitchCase" />.</summary>
	// Token: 0x02000285 RID: 645
	[DebuggerTypeProxy(typeof(Expression.SwitchExpressionProxy))]
	public sealed class SwitchExpression : Expression
	{
		// Token: 0x0600131C RID: 4892 RVA: 0x000370A2 File Offset: 0x000352A2
		internal SwitchExpression(Type type, Expression switchValue, Expression defaultBody, MethodInfo comparison, ReadOnlyCollection<SwitchCase> cases)
		{
			this.Type = type;
			this.SwitchValue = switchValue;
			this.DefaultBody = defaultBody;
			this.Comparison = comparison;
			this.Cases = cases;
		}

		/// <summary>Gets the static type of the expression that this <see cref="T:System.Linq.Expressions.Expression" /> represents.</summary>
		/// <returns>The <see cref="P:System.Linq.Expressions.SwitchExpression.Type" /> that represents the static type of the expression.</returns>
		// Token: 0x170003AD RID: 941
		// (get) Token: 0x0600131D RID: 4893 RVA: 0x000370CF File Offset: 0x000352CF
		public sealed override Type Type
		{
			[CompilerGenerated]
			get
			{
				return this.<Type>k__BackingField;
			}
		}

		/// <summary>Returns the node type of this Expression. Extension nodes should return <see cref="F:System.Linq.Expressions.ExpressionType.Extension" /> when overriding this method.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.ExpressionType" /> of the expression.</returns>
		// Token: 0x170003AE RID: 942
		// (get) Token: 0x0600131E RID: 4894 RVA: 0x000370D7 File Offset: 0x000352D7
		public sealed override ExpressionType NodeType
		{
			get
			{
				return ExpressionType.Switch;
			}
		}

		/// <summary>Gets the test for the switch.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.Expression" /> object representing the test for the switch.</returns>
		// Token: 0x170003AF RID: 943
		// (get) Token: 0x0600131F RID: 4895 RVA: 0x000370DB File Offset: 0x000352DB
		public Expression SwitchValue
		{
			[CompilerGenerated]
			get
			{
				return this.<SwitchValue>k__BackingField;
			}
		}

		/// <summary>Gets the collection of <see cref="T:System.Linq.Expressions.SwitchCase" /> objects for the switch.</summary>
		/// <returns>The collection of <see cref="T:System.Linq.Expressions.SwitchCase" /> objects.</returns>
		// Token: 0x170003B0 RID: 944
		// (get) Token: 0x06001320 RID: 4896 RVA: 0x000370E3 File Offset: 0x000352E3
		public ReadOnlyCollection<SwitchCase> Cases
		{
			[CompilerGenerated]
			get
			{
				return this.<Cases>k__BackingField;
			}
		}

		/// <summary>Gets the test for the switch.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.Expression" /> object representing the test for the switch.</returns>
		// Token: 0x170003B1 RID: 945
		// (get) Token: 0x06001321 RID: 4897 RVA: 0x000370EB File Offset: 0x000352EB
		public Expression DefaultBody
		{
			[CompilerGenerated]
			get
			{
				return this.<DefaultBody>k__BackingField;
			}
		}

		/// <summary>Gets the equality comparison method, if any.</summary>
		/// <returns>The <see cref="T:System.Reflection.MethodInfo" /> object representing the equality comparison method.</returns>
		// Token: 0x170003B2 RID: 946
		// (get) Token: 0x06001322 RID: 4898 RVA: 0x000370F3 File Offset: 0x000352F3
		public MethodInfo Comparison
		{
			[CompilerGenerated]
			get
			{
				return this.<Comparison>k__BackingField;
			}
		}

		// Token: 0x06001323 RID: 4899 RVA: 0x000370FB File Offset: 0x000352FB
		protected internal override Expression Accept(ExpressionVisitor visitor)
		{
			return visitor.VisitSwitch(this);
		}

		// Token: 0x170003B3 RID: 947
		// (get) Token: 0x06001324 RID: 4900 RVA: 0x00037104 File Offset: 0x00035304
		internal bool IsLifted
		{
			get
			{
				return this.SwitchValue.Type.IsNullableType() && (this.Comparison == null || !TypeUtils.AreEquivalent(this.SwitchValue.Type, this.Comparison.GetParametersCached()[0].ParameterType.GetNonRefType()));
			}
		}

		/// <summary>Creates a new expression that is like this one, but using the supplied children. If all of the children are the same, it will return this expression.</summary>
		/// <param name="switchValue">The <see cref="P:System.Linq.Expressions.SwitchExpression.SwitchValue" /> property of the result.</param>
		/// <param name="cases">The <see cref="P:System.Linq.Expressions.SwitchExpression.Cases" /> property of the result.</param>
		/// <param name="defaultBody">The <see cref="P:System.Linq.Expressions.SwitchExpression.DefaultBody" /> property of the result.</param>
		/// <returns>This expression if no children are changed or an expression with the updated children.</returns>
		// Token: 0x06001325 RID: 4901 RVA: 0x00037160 File Offset: 0x00035360
		public SwitchExpression Update(Expression switchValue, IEnumerable<SwitchCase> cases, Expression defaultBody)
		{
			if ((switchValue == this.SwitchValue & defaultBody == this.DefaultBody & cases != null) && ExpressionUtils.SameElements<SwitchCase>(ref cases, this.Cases))
			{
				return this;
			}
			return Expression.Switch(this.Type, switchValue, defaultBody, this.Comparison, cases);
		}

		// Token: 0x06001326 RID: 4902 RVA: 0x0000220F File Offset: 0x0000040F
		internal SwitchExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000998 RID: 2456
		[CompilerGenerated]
		private readonly Type <Type>k__BackingField;

		// Token: 0x04000999 RID: 2457
		[CompilerGenerated]
		private readonly Expression <SwitchValue>k__BackingField;

		// Token: 0x0400099A RID: 2458
		[CompilerGenerated]
		private readonly ReadOnlyCollection<SwitchCase> <Cases>k__BackingField;

		// Token: 0x0400099B RID: 2459
		[CompilerGenerated]
		private readonly Expression <DefaultBody>k__BackingField;

		// Token: 0x0400099C RID: 2460
		[CompilerGenerated]
		private readonly MethodInfo <Comparison>k__BackingField;
	}
}
