﻿using System;

namespace System.Linq.Expressions
{
	// Token: 0x0200028B RID: 651
	internal static class Utils
	{
		// Token: 0x0600135A RID: 4954 RVA: 0x00037A70 File Offset: 0x00035C70
		public static ConstantExpression Constant(bool value)
		{
			if (!value)
			{
				return Utils.s_false;
			}
			return Utils.s_true;
		}

		// Token: 0x0600135B RID: 4955 RVA: 0x00037A80 File Offset: 0x00035C80
		public static ConstantExpression Constant(int value)
		{
			switch (value)
			{
			case -1:
				return Utils.s_m1;
			case 0:
				return Utils.s_0;
			case 1:
				return Utils.s_1;
			case 2:
				return Utils.s_2;
			case 3:
				return Utils.s_3;
			default:
				return Expression.Constant(value);
			}
		}

		// Token: 0x0600135C RID: 4956 RVA: 0x00037AD4 File Offset: 0x00035CD4
		// Note: this type is marked as 'beforefieldinit'.
		static Utils()
		{
		}

		// Token: 0x040009AE RID: 2478
		public static readonly object BoxedFalse = false;

		// Token: 0x040009AF RID: 2479
		public static readonly object BoxedTrue = true;

		// Token: 0x040009B0 RID: 2480
		public static readonly object BoxedIntM1 = -1;

		// Token: 0x040009B1 RID: 2481
		public static readonly object BoxedInt0 = 0;

		// Token: 0x040009B2 RID: 2482
		public static readonly object BoxedInt1 = 1;

		// Token: 0x040009B3 RID: 2483
		public static readonly object BoxedInt2 = 2;

		// Token: 0x040009B4 RID: 2484
		public static readonly object BoxedInt3 = 3;

		// Token: 0x040009B5 RID: 2485
		public static readonly object BoxedDefaultSByte = 0;

		// Token: 0x040009B6 RID: 2486
		public static readonly object BoxedDefaultChar = '\0';

		// Token: 0x040009B7 RID: 2487
		public static readonly object BoxedDefaultInt16 = 0;

		// Token: 0x040009B8 RID: 2488
		public static readonly object BoxedDefaultInt64 = 0L;

		// Token: 0x040009B9 RID: 2489
		public static readonly object BoxedDefaultByte = 0;

		// Token: 0x040009BA RID: 2490
		public static readonly object BoxedDefaultUInt16 = 0;

		// Token: 0x040009BB RID: 2491
		public static readonly object BoxedDefaultUInt32 = 0U;

		// Token: 0x040009BC RID: 2492
		public static readonly object BoxedDefaultUInt64 = 0UL;

		// Token: 0x040009BD RID: 2493
		public static readonly object BoxedDefaultSingle = 0f;

		// Token: 0x040009BE RID: 2494
		public static readonly object BoxedDefaultDouble = 0.0;

		// Token: 0x040009BF RID: 2495
		public static readonly object BoxedDefaultDecimal = 0m;

		// Token: 0x040009C0 RID: 2496
		public static readonly object BoxedDefaultDateTime = default(DateTime);

		// Token: 0x040009C1 RID: 2497
		private static readonly ConstantExpression s_true = Expression.Constant(Utils.BoxedTrue);

		// Token: 0x040009C2 RID: 2498
		private static readonly ConstantExpression s_false = Expression.Constant(Utils.BoxedFalse);

		// Token: 0x040009C3 RID: 2499
		private static readonly ConstantExpression s_m1 = Expression.Constant(Utils.BoxedIntM1);

		// Token: 0x040009C4 RID: 2500
		private static readonly ConstantExpression s_0 = Expression.Constant(Utils.BoxedInt0);

		// Token: 0x040009C5 RID: 2501
		private static readonly ConstantExpression s_1 = Expression.Constant(Utils.BoxedInt1);

		// Token: 0x040009C6 RID: 2502
		private static readonly ConstantExpression s_2 = Expression.Constant(Utils.BoxedInt2);

		// Token: 0x040009C7 RID: 2503
		private static readonly ConstantExpression s_3 = Expression.Constant(Utils.BoxedInt3);

		// Token: 0x040009C8 RID: 2504
		public static readonly DefaultExpression Empty = Expression.Empty();

		// Token: 0x040009C9 RID: 2505
		public static readonly ConstantExpression Null = Expression.Constant(null);
	}
}
