﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Represents the default value of a type or an empty expression.</summary>
	// Token: 0x0200022F RID: 559
	[DebuggerTypeProxy(typeof(Expression.DefaultExpressionProxy))]
	public sealed class DefaultExpression : Expression
	{
		// Token: 0x06000F71 RID: 3953 RVA: 0x0003137D File Offset: 0x0002F57D
		internal DefaultExpression(Type type)
		{
			this.Type = type;
		}

		/// <summary>Gets the static type of the expression that this <see cref="T:System.Linq.Expressions.Expression" /> represents.</summary>
		/// <returns>The <see cref="P:System.Linq.Expressions.DefaultExpression.Type" /> that represents the static type of the expression.</returns>
		// Token: 0x170002D0 RID: 720
		// (get) Token: 0x06000F72 RID: 3954 RVA: 0x0003138C File Offset: 0x0002F58C
		public sealed override Type Type
		{
			[CompilerGenerated]
			get
			{
				return this.<Type>k__BackingField;
			}
		}

		/// <summary>Returns the node type of this expression. Extension nodes should return <see cref="F:System.Linq.Expressions.ExpressionType.Extension" /> when overriding this method.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.ExpressionType" /> of the expression.</returns>
		// Token: 0x170002D1 RID: 721
		// (get) Token: 0x06000F73 RID: 3955 RVA: 0x00031394 File Offset: 0x0002F594
		public sealed override ExpressionType NodeType
		{
			get
			{
				return ExpressionType.Default;
			}
		}

		// Token: 0x06000F74 RID: 3956 RVA: 0x00031398 File Offset: 0x0002F598
		protected internal override Expression Accept(ExpressionVisitor visitor)
		{
			return visitor.VisitDefault(this);
		}

		// Token: 0x06000F75 RID: 3957 RVA: 0x0000220F File Offset: 0x0000040F
		internal DefaultExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040008BE RID: 2238
		[CompilerGenerated]
		private readonly Type <Type>k__BackingField;
	}
}
