﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Dynamic.Utils;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>An expression that provides runtime read/write permission for variables.</summary>
	// Token: 0x0200027D RID: 637
	[DebuggerTypeProxy(typeof(Expression.RuntimeVariablesExpressionProxy))]
	public sealed class RuntimeVariablesExpression : Expression
	{
		// Token: 0x06001257 RID: 4695 RVA: 0x000366DE File Offset: 0x000348DE
		internal RuntimeVariablesExpression(ReadOnlyCollection<ParameterExpression> variables)
		{
			this.Variables = variables;
		}

		/// <summary>Gets the static type of the expression that this <see cref="T:System.Linq.Expressions.Expression" /> represents.</summary>
		/// <returns>The <see cref="P:System.Linq.Expressions.RuntimeVariablesExpression.Type" /> that represents the static type of the expression.</returns>
		// Token: 0x17000359 RID: 857
		// (get) Token: 0x06001258 RID: 4696 RVA: 0x000366ED File Offset: 0x000348ED
		public sealed override Type Type
		{
			get
			{
				return typeof(IRuntimeVariables);
			}
		}

		/// <summary>Returns the node type of this Expression. Extension nodes should return <see cref="F:System.Linq.Expressions.ExpressionType.Extension" /> when overriding this method.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.ExpressionType" /> of the expression.</returns>
		// Token: 0x1700035A RID: 858
		// (get) Token: 0x06001259 RID: 4697 RVA: 0x000366F9 File Offset: 0x000348F9
		public sealed override ExpressionType NodeType
		{
			get
			{
				return ExpressionType.RuntimeVariables;
			}
		}

		/// <summary>The variables or parameters to which to provide runtime access.</summary>
		/// <returns>The read-only collection containing parameters that will be provided the runtime access.</returns>
		// Token: 0x1700035B RID: 859
		// (get) Token: 0x0600125A RID: 4698 RVA: 0x000366FD File Offset: 0x000348FD
		public ReadOnlyCollection<ParameterExpression> Variables
		{
			[CompilerGenerated]
			get
			{
				return this.<Variables>k__BackingField;
			}
		}

		// Token: 0x0600125B RID: 4699 RVA: 0x00036705 File Offset: 0x00034905
		protected internal override Expression Accept(ExpressionVisitor visitor)
		{
			return visitor.VisitRuntimeVariables(this);
		}

		/// <summary>Creates a new expression that is like this one, but using the supplied children. If all of the children are the same, it will return this expression.</summary>
		/// <param name="variables">The <see cref="P:System.Linq.Expressions.RuntimeVariablesExpression.Variables" /> property of the result.</param>
		/// <returns>This expression if no children are changed or an expression with the updated children.</returns>
		// Token: 0x0600125C RID: 4700 RVA: 0x0003670E File Offset: 0x0003490E
		public RuntimeVariablesExpression Update(IEnumerable<ParameterExpression> variables)
		{
			if (variables != null && ExpressionUtils.SameElements<ParameterExpression>(ref variables, this.Variables))
			{
				return this;
			}
			return Expression.RuntimeVariables(variables);
		}

		// Token: 0x0600125D RID: 4701 RVA: 0x0000220F File Offset: 0x0000040F
		internal RuntimeVariablesExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0400098B RID: 2443
		[CompilerGenerated]
		private readonly ReadOnlyCollection<ParameterExpression> <Variables>k__BackingField;
	}
}
