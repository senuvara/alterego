﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Dynamic.Utils;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Represents one case of a <see cref="T:System.Linq.Expressions.SwitchExpression" />.</summary>
	// Token: 0x02000284 RID: 644
	[DebuggerTypeProxy(typeof(Expression.SwitchCaseProxy))]
	public sealed class SwitchCase
	{
		// Token: 0x06001316 RID: 4886 RVA: 0x0003704A File Offset: 0x0003524A
		internal SwitchCase(Expression body, ReadOnlyCollection<Expression> testValues)
		{
			this.Body = body;
			this.TestValues = testValues;
		}

		/// <summary>Gets the values of this case. This case is selected for execution when the <see cref="P:System.Linq.Expressions.SwitchExpression.SwitchValue" /> matches any of these values.</summary>
		/// <returns>The read-only collection of the values for this case block.</returns>
		// Token: 0x170003AB RID: 939
		// (get) Token: 0x06001317 RID: 4887 RVA: 0x00037060 File Offset: 0x00035260
		public ReadOnlyCollection<Expression> TestValues
		{
			[CompilerGenerated]
			get
			{
				return this.<TestValues>k__BackingField;
			}
		}

		/// <summary>Gets the body of this case.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.Expression" /> object that represents the body of the case block.</returns>
		// Token: 0x170003AC RID: 940
		// (get) Token: 0x06001318 RID: 4888 RVA: 0x00037068 File Offset: 0x00035268
		public Expression Body
		{
			[CompilerGenerated]
			get
			{
				return this.<Body>k__BackingField;
			}
		}

		/// <summary>Returns a <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.</summary>
		/// <returns>A <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.</returns>
		// Token: 0x06001319 RID: 4889 RVA: 0x00037070 File Offset: 0x00035270
		public override string ToString()
		{
			return ExpressionStringBuilder.SwitchCaseToString(this);
		}

		/// <summary>Creates a new expression that is like this one, but using the supplied children. If all of the children are the same, it will return this expression.</summary>
		/// <param name="testValues">The <see cref="P:System.Linq.Expressions.SwitchCase.TestValues" /> property of the result.</param>
		/// <param name="body">The <see cref="P:System.Linq.Expressions.SwitchCase.Body" /> property of the result.</param>
		/// <returns>This expression if no children are changed or an expression with the updated children.</returns>
		// Token: 0x0600131A RID: 4890 RVA: 0x00037078 File Offset: 0x00035278
		public SwitchCase Update(IEnumerable<Expression> testValues, Expression body)
		{
			if ((body == this.Body & testValues != null) && ExpressionUtils.SameElements<Expression>(ref testValues, this.TestValues))
			{
				return this;
			}
			return Expression.SwitchCase(body, testValues);
		}

		// Token: 0x0600131B RID: 4891 RVA: 0x0000220F File Offset: 0x0000040F
		internal SwitchCase()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000996 RID: 2454
		[CompilerGenerated]
		private readonly ReadOnlyCollection<Expression> <TestValues>k__BackingField;

		// Token: 0x04000997 RID: 2455
		[CompilerGenerated]
		private readonly Expression <Body>k__BackingField;
	}
}
