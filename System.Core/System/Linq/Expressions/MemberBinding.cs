﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	/// <summary>Provides the base class from which the classes that represent bindings that are used to initialize members of a newly created object derive.</summary>
	// Token: 0x0200025F RID: 607
	public abstract class MemberBinding
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Linq.Expressions.MemberBinding" /> class.</summary>
		/// <param name="type">The <see cref="T:System.Linq.Expressions.MemberBindingType" /> that discriminates the type of binding that is represented.</param>
		/// <param name="member">The <see cref="T:System.Reflection.MemberInfo" /> that represents a field or property to be initialized.</param>
		// Token: 0x060011AA RID: 4522 RVA: 0x00035502 File Offset: 0x00033702
		[Obsolete("Do not use this constructor. It will be removed in future releases.")]
		protected MemberBinding(MemberBindingType type, MemberInfo member)
		{
			this.BindingType = type;
			this.Member = member;
		}

		/// <summary>Gets the type of binding that is represented.</summary>
		/// <returns>One of the <see cref="T:System.Linq.Expressions.MemberBindingType" /> values.</returns>
		// Token: 0x17000328 RID: 808
		// (get) Token: 0x060011AB RID: 4523 RVA: 0x00035518 File Offset: 0x00033718
		public MemberBindingType BindingType
		{
			[CompilerGenerated]
			get
			{
				return this.<BindingType>k__BackingField;
			}
		}

		/// <summary>Gets the field or property to be initialized.</summary>
		/// <returns>The <see cref="T:System.Reflection.MemberInfo" /> that represents the field or property to be initialized.</returns>
		// Token: 0x17000329 RID: 809
		// (get) Token: 0x060011AC RID: 4524 RVA: 0x00035520 File Offset: 0x00033720
		public MemberInfo Member
		{
			[CompilerGenerated]
			get
			{
				return this.<Member>k__BackingField;
			}
		}

		/// <summary>Returns a textual representation of the <see cref="T:System.Linq.Expressions.MemberBinding" />.</summary>
		/// <returns>A textual representation of the <see cref="T:System.Linq.Expressions.MemberBinding" />.</returns>
		// Token: 0x060011AD RID: 4525 RVA: 0x00035528 File Offset: 0x00033728
		public override string ToString()
		{
			return ExpressionStringBuilder.MemberBindingToString(this);
		}

		// Token: 0x060011AE RID: 4526 RVA: 0x00035530 File Offset: 0x00033730
		internal virtual void ValidateAsDefinedHere(int index)
		{
			throw Error.UnknownBindingType(index);
		}

		// Token: 0x04000961 RID: 2401
		[CompilerGenerated]
		private readonly MemberBindingType <BindingType>k__BackingField;

		// Token: 0x04000962 RID: 2402
		[CompilerGenerated]
		private readonly MemberInfo <Member>k__BackingField;
	}
}
