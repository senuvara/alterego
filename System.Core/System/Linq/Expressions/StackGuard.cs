﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace System.Linq.Expressions
{
	// Token: 0x0200027E RID: 638
	internal sealed class StackGuard
	{
		// Token: 0x0600125E RID: 4702 RVA: 0x0003672A File Offset: 0x0003492A
		public bool TryEnterOnCurrentStack()
		{
			if (RuntimeHelpers.TryEnsureSufficientExecutionStack())
			{
				return true;
			}
			if (this._executionStackCount < 1024)
			{
				return false;
			}
			throw new InsufficientExecutionStackException();
		}

		// Token: 0x0600125F RID: 4703 RVA: 0x00036749 File Offset: 0x00034949
		public void RunOnEmptyStack<T1, T2>(Action<T1, T2> action, T1 arg1, T2 arg2)
		{
			this.RunOnEmptyStackCore<object>(delegate(object s)
			{
				Tuple<Action<T1, T2>, T1, T2> tuple = (Tuple<Action<T1, T2>, T1, T2>)s;
				tuple.Item1(tuple.Item2, tuple.Item3);
				return null;
			}, Tuple.Create<Action<T1, T2>, T1, T2>(action, arg1, arg2));
		}

		// Token: 0x06001260 RID: 4704 RVA: 0x00036779 File Offset: 0x00034979
		public void RunOnEmptyStack<T1, T2, T3>(Action<T1, T2, T3> action, T1 arg1, T2 arg2, T3 arg3)
		{
			this.RunOnEmptyStackCore<object>(delegate(object s)
			{
				Tuple<Action<T1, T2, T3>, T1, T2, T3> tuple = (Tuple<Action<T1, T2, T3>, T1, T2, T3>)s;
				tuple.Item1(tuple.Item2, tuple.Item3, tuple.Item4);
				return null;
			}, Tuple.Create<Action<T1, T2, T3>, T1, T2, T3>(action, arg1, arg2, arg3));
		}

		// Token: 0x06001261 RID: 4705 RVA: 0x000367AB File Offset: 0x000349AB
		public R RunOnEmptyStack<T1, T2, R>(Func<T1, T2, R> action, T1 arg1, T2 arg2)
		{
			return this.RunOnEmptyStackCore<R>(delegate(object s)
			{
				Tuple<Func<T1, T2, R>, T1, T2> tuple = (Tuple<Func<T1, T2, R>, T1, T2>)s;
				return tuple.Item1(tuple.Item2, tuple.Item3);
			}, Tuple.Create<Func<T1, T2, R>, T1, T2>(action, arg1, arg2));
		}

		// Token: 0x06001262 RID: 4706 RVA: 0x000367DA File Offset: 0x000349DA
		public R RunOnEmptyStack<T1, T2, T3, R>(Func<T1, T2, T3, R> action, T1 arg1, T2 arg2, T3 arg3)
		{
			return this.RunOnEmptyStackCore<R>(delegate(object s)
			{
				Tuple<Func<T1, T2, T3, R>, T1, T2, T3> tuple = (Tuple<Func<T1, T2, T3, R>, T1, T2, T3>)s;
				return tuple.Item1(tuple.Item2, tuple.Item3, tuple.Item4);
			}, Tuple.Create<Func<T1, T2, T3, R>, T1, T2, T3>(action, arg1, arg2, arg3));
		}

		// Token: 0x06001263 RID: 4707 RVA: 0x0003680C File Offset: 0x00034A0C
		private R RunOnEmptyStackCore<R>(Func<object, R> action, object state)
		{
			this._executionStackCount++;
			R result;
			try
			{
				Task<R> task = Task.Factory.StartNew<R>(action, state, CancellationToken.None, TaskCreationOptions.DenyChildAttach, TaskScheduler.Default);
				TaskAwaiter<R> awaiter = task.GetAwaiter();
				if (!awaiter.IsCompleted)
				{
					((IAsyncResult)task).AsyncWaitHandle.WaitOne();
				}
				result = awaiter.GetResult();
			}
			finally
			{
				this._executionStackCount--;
			}
			return result;
		}

		// Token: 0x06001264 RID: 4708 RVA: 0x00002310 File Offset: 0x00000510
		public StackGuard()
		{
		}

		// Token: 0x0400098C RID: 2444
		private const int MaxExecutionStackCount = 1024;

		// Token: 0x0400098D RID: 2445
		private int _executionStackCount;

		// Token: 0x0200027F RID: 639
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c__3<T1, T2>
		{
			// Token: 0x06001265 RID: 4709 RVA: 0x00036888 File Offset: 0x00034A88
			// Note: this type is marked as 'beforefieldinit'.
			static <>c__3()
			{
			}

			// Token: 0x06001266 RID: 4710 RVA: 0x00002310 File Offset: 0x00000510
			public <>c__3()
			{
			}

			// Token: 0x06001267 RID: 4711 RVA: 0x00036894 File Offset: 0x00034A94
			internal object <RunOnEmptyStack>b__3_0(object s)
			{
				Tuple<Action<T1, T2>, T1, T2> tuple = (Tuple<Action<T1, T2>, T1, T2>)s;
				tuple.Item1(tuple.Item2, tuple.Item3);
				return null;
			}

			// Token: 0x0400098E RID: 2446
			public static readonly StackGuard.<>c__3<T1, T2> <>9 = new StackGuard.<>c__3<T1, T2>();

			// Token: 0x0400098F RID: 2447
			public static Func<object, object> <>9__3_0;
		}

		// Token: 0x02000280 RID: 640
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c__4<T1, T2, T3>
		{
			// Token: 0x06001268 RID: 4712 RVA: 0x000368C0 File Offset: 0x00034AC0
			// Note: this type is marked as 'beforefieldinit'.
			static <>c__4()
			{
			}

			// Token: 0x06001269 RID: 4713 RVA: 0x00002310 File Offset: 0x00000510
			public <>c__4()
			{
			}

			// Token: 0x0600126A RID: 4714 RVA: 0x000368CC File Offset: 0x00034ACC
			internal object <RunOnEmptyStack>b__4_0(object s)
			{
				Tuple<Action<T1, T2, T3>, T1, T2, T3> tuple = (Tuple<Action<T1, T2, T3>, T1, T2, T3>)s;
				tuple.Item1(tuple.Item2, tuple.Item3, tuple.Item4);
				return null;
			}

			// Token: 0x04000990 RID: 2448
			public static readonly StackGuard.<>c__4<T1, T2, T3> <>9 = new StackGuard.<>c__4<T1, T2, T3>();

			// Token: 0x04000991 RID: 2449
			public static Func<object, object> <>9__4_0;
		}

		// Token: 0x02000281 RID: 641
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c__5<T1, T2, R>
		{
			// Token: 0x0600126B RID: 4715 RVA: 0x000368FE File Offset: 0x00034AFE
			// Note: this type is marked as 'beforefieldinit'.
			static <>c__5()
			{
			}

			// Token: 0x0600126C RID: 4716 RVA: 0x00002310 File Offset: 0x00000510
			public <>c__5()
			{
			}

			// Token: 0x0600126D RID: 4717 RVA: 0x0003690C File Offset: 0x00034B0C
			internal R <RunOnEmptyStack>b__5_0(object s)
			{
				Tuple<Func<T1, T2, R>, T1, T2> tuple = (Tuple<Func<T1, T2, R>, T1, T2>)s;
				return tuple.Item1(tuple.Item2, tuple.Item3);
			}

			// Token: 0x04000992 RID: 2450
			public static readonly StackGuard.<>c__5<T1, T2, R> <>9 = new StackGuard.<>c__5<T1, T2, R>();

			// Token: 0x04000993 RID: 2451
			public static Func<object, R> <>9__5_0;
		}

		// Token: 0x02000282 RID: 642
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c__6<T1, T2, T3, R>
		{
			// Token: 0x0600126E RID: 4718 RVA: 0x00036937 File Offset: 0x00034B37
			// Note: this type is marked as 'beforefieldinit'.
			static <>c__6()
			{
			}

			// Token: 0x0600126F RID: 4719 RVA: 0x00002310 File Offset: 0x00000510
			public <>c__6()
			{
			}

			// Token: 0x06001270 RID: 4720 RVA: 0x00036944 File Offset: 0x00034B44
			internal R <RunOnEmptyStack>b__6_0(object s)
			{
				Tuple<Func<T1, T2, T3, R>, T1, T2, T3> tuple = (Tuple<Func<T1, T2, T3, R>, T1, T2, T3>)s;
				return tuple.Item1(tuple.Item2, tuple.Item3, tuple.Item4);
			}

			// Token: 0x04000994 RID: 2452
			public static readonly StackGuard.<>c__6<T1, T2, T3, R> <>9 = new StackGuard.<>c__6<T1, T2, T3, R>();

			// Token: 0x04000995 RID: 2453
			public static Func<object, R> <>9__6_0;
		}
	}
}
