﻿using System;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;

namespace System.Linq.Expressions
{
	// Token: 0x0200024C RID: 588
	internal sealed class InvocationExpression2 : InvocationExpression
	{
		// Token: 0x0600112D RID: 4397 RVA: 0x00034B13 File Offset: 0x00032D13
		public InvocationExpression2(Expression lambda, Type returnType, Expression arg0, Expression arg1) : base(lambda, returnType)
		{
			this._arg0 = arg0;
			this._arg1 = arg1;
		}

		// Token: 0x0600112E RID: 4398 RVA: 0x00034B2C File Offset: 0x00032D2C
		internal override ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			return ExpressionUtils.ReturnReadOnly(this, ref this._arg0);
		}

		// Token: 0x0600112F RID: 4399 RVA: 0x00034B3A File Offset: 0x00032D3A
		public override Expression GetArgument(int index)
		{
			if (index == 0)
			{
				return ExpressionUtils.ReturnObject<Expression>(this._arg0);
			}
			if (index != 1)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			return this._arg1;
		}

		// Token: 0x170002FD RID: 765
		// (get) Token: 0x06001130 RID: 4400 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ArgumentCount
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x06001131 RID: 4401 RVA: 0x00034B62 File Offset: 0x00032D62
		internal override InvocationExpression Rewrite(Expression lambda, Expression[] arguments)
		{
			if (arguments != null)
			{
				return Expression.Invoke(lambda, arguments[0], arguments[1]);
			}
			return Expression.Invoke(lambda, ExpressionUtils.ReturnObject<Expression>(this._arg0), this._arg1);
		}

		// Token: 0x0400093B RID: 2363
		private object _arg0;

		// Token: 0x0400093C RID: 2364
		private readonly Expression _arg1;
	}
}
