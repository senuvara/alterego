﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;

namespace System.Linq.Expressions
{
	// Token: 0x0200021A RID: 538
	internal class BlockN : BlockExpression
	{
		// Token: 0x06000EBA RID: 3770 RVA: 0x0002F213 File Offset: 0x0002D413
		internal BlockN(IReadOnlyList<Expression> expressions)
		{
			this._expressions = expressions;
		}

		// Token: 0x06000EBB RID: 3771 RVA: 0x0002F222 File Offset: 0x0002D422
		internal override bool SameExpressions(ICollection<Expression> expressions)
		{
			return ExpressionUtils.SameElements<Expression>(expressions, this._expressions);
		}

		// Token: 0x06000EBC RID: 3772 RVA: 0x0002F230 File Offset: 0x0002D430
		internal override Expression GetExpression(int index)
		{
			return this._expressions[index];
		}

		// Token: 0x170002A2 RID: 674
		// (get) Token: 0x06000EBD RID: 3773 RVA: 0x0002F23E File Offset: 0x0002D43E
		internal override int ExpressionCount
		{
			get
			{
				return this._expressions.Count;
			}
		}

		// Token: 0x06000EBE RID: 3774 RVA: 0x0002F24B File Offset: 0x0002D44B
		internal override ReadOnlyCollection<Expression> GetOrMakeExpressions()
		{
			return ExpressionUtils.ReturnReadOnly<Expression>(ref this._expressions);
		}

		// Token: 0x06000EBF RID: 3775 RVA: 0x0002F258 File Offset: 0x0002D458
		internal override BlockExpression Rewrite(ReadOnlyCollection<ParameterExpression> variables, Expression[] args)
		{
			return new BlockN(args);
		}

		// Token: 0x0400088F RID: 2191
		private IReadOnlyList<Expression> _expressions;
	}
}
