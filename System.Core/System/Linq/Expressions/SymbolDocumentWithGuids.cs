﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x02000287 RID: 647
	internal sealed class SymbolDocumentWithGuids : SymbolDocumentInfo
	{
		// Token: 0x0600132E RID: 4910 RVA: 0x00037218 File Offset: 0x00035418
		internal SymbolDocumentWithGuids(string fileName, ref Guid language) : base(fileName)
		{
			this.Language = language;
			this.DocumentType = SymbolDocumentInfo.DocumentType_Text;
		}

		// Token: 0x0600132F RID: 4911 RVA: 0x00037238 File Offset: 0x00035438
		internal SymbolDocumentWithGuids(string fileName, ref Guid language, ref Guid vendor) : base(fileName)
		{
			this.Language = language;
			this.LanguageVendor = vendor;
			this.DocumentType = SymbolDocumentInfo.DocumentType_Text;
		}

		// Token: 0x06001330 RID: 4912 RVA: 0x00037264 File Offset: 0x00035464
		internal SymbolDocumentWithGuids(string fileName, ref Guid language, ref Guid vendor, ref Guid documentType) : base(fileName)
		{
			this.Language = language;
			this.LanguageVendor = vendor;
			this.DocumentType = documentType;
		}

		// Token: 0x170003B8 RID: 952
		// (get) Token: 0x06001331 RID: 4913 RVA: 0x00037292 File Offset: 0x00035492
		public override Guid Language
		{
			[CompilerGenerated]
			get
			{
				return this.<Language>k__BackingField;
			}
		}

		// Token: 0x170003B9 RID: 953
		// (get) Token: 0x06001332 RID: 4914 RVA: 0x0003729A File Offset: 0x0003549A
		public override Guid LanguageVendor
		{
			[CompilerGenerated]
			get
			{
				return this.<LanguageVendor>k__BackingField;
			}
		}

		// Token: 0x170003BA RID: 954
		// (get) Token: 0x06001333 RID: 4915 RVA: 0x000372A2 File Offset: 0x000354A2
		public override Guid DocumentType
		{
			[CompilerGenerated]
			get
			{
				return this.<DocumentType>k__BackingField;
			}
		}

		// Token: 0x0400099F RID: 2463
		[CompilerGenerated]
		private readonly Guid <Language>k__BackingField;

		// Token: 0x040009A0 RID: 2464
		[CompilerGenerated]
		private readonly Guid <LanguageVendor>k__BackingField;

		// Token: 0x040009A1 RID: 2465
		[CompilerGenerated]
		private readonly Guid <DocumentType>k__BackingField;
	}
}
