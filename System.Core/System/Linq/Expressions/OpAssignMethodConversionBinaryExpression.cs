﻿using System;
using System.Reflection;

namespace System.Linq.Expressions
{
	// Token: 0x020001F4 RID: 500
	internal sealed class OpAssignMethodConversionBinaryExpression : MethodBinaryExpression
	{
		// Token: 0x06000C31 RID: 3121 RVA: 0x000271BB File Offset: 0x000253BB
		internal OpAssignMethodConversionBinaryExpression(ExpressionType nodeType, Expression left, Expression right, Type type, MethodInfo method, LambdaExpression conversion) : base(nodeType, left, right, type, method)
		{
			this._conversion = conversion;
		}

		// Token: 0x06000C32 RID: 3122 RVA: 0x000271D2 File Offset: 0x000253D2
		internal override LambdaExpression GetConversion()
		{
			return this._conversion;
		}

		// Token: 0x04000857 RID: 2135
		private readonly LambdaExpression _conversion;
	}
}
