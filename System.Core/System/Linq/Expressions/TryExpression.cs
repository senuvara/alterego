﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Dynamic.Utils;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Represents a try/catch/finally/fault block.</summary>
	// Token: 0x02000288 RID: 648
	[DebuggerTypeProxy(typeof(Expression.TryExpressionProxy))]
	public sealed class TryExpression : Expression
	{
		// Token: 0x06001334 RID: 4916 RVA: 0x000372AA File Offset: 0x000354AA
		internal TryExpression(Type type, Expression body, Expression @finally, Expression fault, ReadOnlyCollection<CatchBlock> handlers)
		{
			this.Type = type;
			this.Body = body;
			this.Handlers = handlers;
			this.Finally = @finally;
			this.Fault = fault;
		}

		/// <summary>Gets the static type of the expression that this <see cref="T:System.Linq.Expressions.Expression" /> represents.</summary>
		/// <returns>The <see cref="P:System.Linq.Expressions.TryExpression.Type" /> that represents the static type of the expression.</returns>
		// Token: 0x170003BB RID: 955
		// (get) Token: 0x06001335 RID: 4917 RVA: 0x000372D7 File Offset: 0x000354D7
		public sealed override Type Type
		{
			[CompilerGenerated]
			get
			{
				return this.<Type>k__BackingField;
			}
		}

		/// <summary>Returns the node type of this <see cref="T:System.Linq.Expressions.Expression" />.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.ExpressionType" /> that represents this expression.</returns>
		// Token: 0x170003BC RID: 956
		// (get) Token: 0x06001336 RID: 4918 RVA: 0x000372DF File Offset: 0x000354DF
		public sealed override ExpressionType NodeType
		{
			get
			{
				return ExpressionType.Try;
			}
		}

		/// <summary>Gets the <see cref="T:System.Linq.Expressions.Expression" /> representing the body of the try block.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.Expression" /> representing the body of the try block.</returns>
		// Token: 0x170003BD RID: 957
		// (get) Token: 0x06001337 RID: 4919 RVA: 0x000372E3 File Offset: 0x000354E3
		public Expression Body
		{
			[CompilerGenerated]
			get
			{
				return this.<Body>k__BackingField;
			}
		}

		/// <summary>Gets the collection of <see cref="T:System.Linq.Expressions.CatchBlock" /> expressions associated with the try block.</summary>
		/// <returns>The collection of <see cref="T:System.Linq.Expressions.CatchBlock" /> expressions associated with the try block.</returns>
		// Token: 0x170003BE RID: 958
		// (get) Token: 0x06001338 RID: 4920 RVA: 0x000372EB File Offset: 0x000354EB
		public ReadOnlyCollection<CatchBlock> Handlers
		{
			[CompilerGenerated]
			get
			{
				return this.<Handlers>k__BackingField;
			}
		}

		/// <summary>Gets the <see cref="T:System.Linq.Expressions.Expression" /> representing the finally block.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.Expression" /> representing the finally block.</returns>
		// Token: 0x170003BF RID: 959
		// (get) Token: 0x06001339 RID: 4921 RVA: 0x000372F3 File Offset: 0x000354F3
		public Expression Finally
		{
			[CompilerGenerated]
			get
			{
				return this.<Finally>k__BackingField;
			}
		}

		/// <summary>Gets the <see cref="T:System.Linq.Expressions.Expression" /> representing the fault block.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.Expression" /> representing the fault block.</returns>
		// Token: 0x170003C0 RID: 960
		// (get) Token: 0x0600133A RID: 4922 RVA: 0x000372FB File Offset: 0x000354FB
		public Expression Fault
		{
			[CompilerGenerated]
			get
			{
				return this.<Fault>k__BackingField;
			}
		}

		// Token: 0x0600133B RID: 4923 RVA: 0x00037303 File Offset: 0x00035503
		protected internal override Expression Accept(ExpressionVisitor visitor)
		{
			return visitor.VisitTry(this);
		}

		/// <summary>Creates a new expression that is like this one, but using the supplied children. If all of the children are the same, it will return this expression.</summary>
		/// <param name="body">The <see cref="P:System.Linq.Expressions.TryExpression.Body" /> property of the result.</param>
		/// <param name="handlers">The <see cref="P:System.Linq.Expressions.TryExpression.Handlers" /> property of the result.</param>
		/// <param name="finally">The <see cref="P:System.Linq.Expressions.TryExpression.Finally" /> property of the result.</param>
		/// <param name="fault">The <see cref="P:System.Linq.Expressions.TryExpression.Fault" /> property of the result.</param>
		/// <returns>This expression if no children are changed or an expression with the updated children.</returns>
		// Token: 0x0600133C RID: 4924 RVA: 0x0003730C File Offset: 0x0003550C
		public TryExpression Update(Expression body, IEnumerable<CatchBlock> handlers, Expression @finally, Expression fault)
		{
			if ((body == this.Body & @finally == this.Finally & fault == this.Fault) && ExpressionUtils.SameElements<CatchBlock>(ref handlers, this.Handlers))
			{
				return this;
			}
			return Expression.MakeTry(this.Type, body, @finally, fault, handlers);
		}

		// Token: 0x0600133D RID: 4925 RVA: 0x0000220F File Offset: 0x0000040F
		internal TryExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040009A2 RID: 2466
		[CompilerGenerated]
		private readonly Type <Type>k__BackingField;

		// Token: 0x040009A3 RID: 2467
		[CompilerGenerated]
		private readonly Expression <Body>k__BackingField;

		// Token: 0x040009A4 RID: 2468
		[CompilerGenerated]
		private readonly ReadOnlyCollection<CatchBlock> <Handlers>k__BackingField;

		// Token: 0x040009A5 RID: 2469
		[CompilerGenerated]
		private readonly Expression <Finally>k__BackingField;

		// Token: 0x040009A6 RID: 2470
		[CompilerGenerated]
		private readonly Expression <Fault>k__BackingField;
	}
}
