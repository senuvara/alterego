﻿using System;
using System.Dynamic.Utils;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Stores information necessary to emit debugging symbol information for a source file, in particular the file name and unique language identifier.</summary>
	// Token: 0x02000286 RID: 646
	public class SymbolDocumentInfo
	{
		// Token: 0x06001327 RID: 4903 RVA: 0x000371AC File Offset: 0x000353AC
		internal SymbolDocumentInfo(string fileName)
		{
			ContractUtils.RequiresNotNull(fileName, "fileName");
			this.FileName = fileName;
		}

		/// <summary>The source file name.</summary>
		/// <returns>The string representing the source file name.</returns>
		// Token: 0x170003B4 RID: 948
		// (get) Token: 0x06001328 RID: 4904 RVA: 0x000371C6 File Offset: 0x000353C6
		public string FileName
		{
			[CompilerGenerated]
			get
			{
				return this.<FileName>k__BackingField;
			}
		}

		/// <summary>Returns the language's unique identifier, if any.</summary>
		/// <returns>The language's unique identifier</returns>
		// Token: 0x170003B5 RID: 949
		// (get) Token: 0x06001329 RID: 4905 RVA: 0x000371CE File Offset: 0x000353CE
		public virtual Guid Language
		{
			get
			{
				return Guid.Empty;
			}
		}

		/// <summary>Returns the language vendor's unique identifier, if any.</summary>
		/// <returns>The language vendor's unique identifier.</returns>
		// Token: 0x170003B6 RID: 950
		// (get) Token: 0x0600132A RID: 4906 RVA: 0x000371CE File Offset: 0x000353CE
		public virtual Guid LanguageVendor
		{
			get
			{
				return Guid.Empty;
			}
		}

		/// <summary>Returns the document type's unique identifier, if any. Defaults to the GUID for a text file.</summary>
		/// <returns>The document type's unique identifier.</returns>
		// Token: 0x170003B7 RID: 951
		// (get) Token: 0x0600132B RID: 4907 RVA: 0x000371D5 File Offset: 0x000353D5
		public virtual Guid DocumentType
		{
			get
			{
				return SymbolDocumentInfo.DocumentType_Text;
			}
		}

		// Token: 0x0600132C RID: 4908 RVA: 0x000371DC File Offset: 0x000353DC
		// Note: this type is marked as 'beforefieldinit'.
		static SymbolDocumentInfo()
		{
		}

		// Token: 0x0600132D RID: 4909 RVA: 0x0000220F File Offset: 0x0000040F
		internal SymbolDocumentInfo()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0400099D RID: 2461
		[CompilerGenerated]
		private readonly string <FileName>k__BackingField;

		// Token: 0x0400099E RID: 2462
		internal static readonly Guid DocumentType_Text = new Guid(1518771467, 26129, 4563, 189, 42, 0, 0, 248, 8, 73, 189);
	}
}
