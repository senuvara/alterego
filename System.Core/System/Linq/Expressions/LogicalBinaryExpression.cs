﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x020001F0 RID: 496
	internal sealed class LogicalBinaryExpression : BinaryExpression
	{
		// Token: 0x06000C23 RID: 3107 RVA: 0x00027134 File Offset: 0x00025334
		internal LogicalBinaryExpression(ExpressionType nodeType, Expression left, Expression right) : base(left, right)
		{
			this.NodeType = nodeType;
		}

		// Token: 0x170001E1 RID: 481
		// (get) Token: 0x06000C24 RID: 3108 RVA: 0x00027145 File Offset: 0x00025345
		public sealed override Type Type
		{
			get
			{
				return typeof(bool);
			}
		}

		// Token: 0x170001E2 RID: 482
		// (get) Token: 0x06000C25 RID: 3109 RVA: 0x00027151 File Offset: 0x00025351
		public sealed override ExpressionType NodeType
		{
			[CompilerGenerated]
			get
			{
				return this.<NodeType>k__BackingField;
			}
		}

		// Token: 0x04000855 RID: 2133
		[CompilerGenerated]
		private readonly ExpressionType <NodeType>k__BackingField;
	}
}
