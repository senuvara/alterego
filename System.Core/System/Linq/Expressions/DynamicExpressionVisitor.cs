﻿using System;
using System.Dynamic.Utils;

namespace System.Linq.Expressions
{
	/// <summary>Represents a visitor or rewriter for dynamic expression trees.</summary>
	// Token: 0x0200023C RID: 572
	public class DynamicExpressionVisitor : ExpressionVisitor
	{
		/// <summary>Visits the children of the <see cref="T:System.Linq.Expressions.DynamicExpression" />.</summary>
		/// <param name="node">The expression to visit.</param>
		/// <returns>Returns <see cref="T:System.Linq.Expressions.Expression" />, the modified expression, if it or any subexpression is modified; otherwise, returns the original expression.</returns>
		// Token: 0x06000FD1 RID: 4049 RVA: 0x000322F0 File Offset: 0x000304F0
		protected internal override Expression VisitDynamic(DynamicExpression node)
		{
			Expression[] array = ExpressionVisitorUtils.VisitArguments(this, node);
			if (array == null)
			{
				return node;
			}
			return node.Rewrite(array);
		}

		/// <summary>Initializes a new instance of <see cref="T:System.Linq.Expressions.DynamicExpressionVisitor" />.</summary>
		// Token: 0x06000FD2 RID: 4050 RVA: 0x0000C3E8 File Offset: 0x0000A5E8
		public DynamicExpressionVisitor()
		{
		}
	}
}
