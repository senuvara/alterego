﻿using System;

namespace System.Linq.Expressions
{
	// Token: 0x0200027C RID: 636
	internal sealed class PrimitiveParameterExpression<T> : ParameterExpression
	{
		// Token: 0x06001255 RID: 4693 RVA: 0x000366D5 File Offset: 0x000348D5
		internal PrimitiveParameterExpression(string name) : base(name)
		{
		}

		// Token: 0x17000358 RID: 856
		// (get) Token: 0x06001256 RID: 4694 RVA: 0x0000B7AA File Offset: 0x000099AA
		public sealed override Type Type
		{
			get
			{
				return typeof(T);
			}
		}
	}
}
