﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;
using System.Reflection;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Represents initializing the elements of a collection member of a newly created object.</summary>
	// Token: 0x02000264 RID: 612
	public sealed class MemberListBinding : MemberBinding
	{
		// Token: 0x060011CD RID: 4557 RVA: 0x000357F8 File Offset: 0x000339F8
		internal MemberListBinding(MemberInfo member, ReadOnlyCollection<ElementInit> initializers) : base(MemberBindingType.ListBinding, member)
		{
			this.Initializers = initializers;
		}

		/// <summary>Gets the element initializers for initializing a collection member of a newly created object.</summary>
		/// <returns>A <see cref="T:System.Collections.ObjectModel.ReadOnlyCollection`1" /> of <see cref="T:System.Linq.Expressions.ElementInit" /> objects to initialize a collection member with.</returns>
		// Token: 0x17000334 RID: 820
		// (get) Token: 0x060011CE RID: 4558 RVA: 0x00035809 File Offset: 0x00033A09
		public ReadOnlyCollection<ElementInit> Initializers
		{
			[CompilerGenerated]
			get
			{
				return this.<Initializers>k__BackingField;
			}
		}

		/// <summary>Creates a new expression that is like this one, but using the supplied children. If all of the children are the same, it will return this expression.</summary>
		/// <param name="initializers">The <see cref="P:System.Linq.Expressions.MemberListBinding.Initializers" /> property of the result.</param>
		/// <returns>This expression if no children are changed or an expression with the updated children.</returns>
		// Token: 0x060011CF RID: 4559 RVA: 0x00035811 File Offset: 0x00033A11
		public MemberListBinding Update(IEnumerable<ElementInit> initializers)
		{
			if (initializers != null && ExpressionUtils.SameElements<ElementInit>(ref initializers, this.Initializers))
			{
				return this;
			}
			return Expression.ListBind(base.Member, initializers);
		}

		// Token: 0x060011D0 RID: 4560 RVA: 0x000039E8 File Offset: 0x00001BE8
		internal override void ValidateAsDefinedHere(int index)
		{
		}

		// Token: 0x060011D1 RID: 4561 RVA: 0x0000220F File Offset: 0x0000040F
		internal MemberListBinding()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000968 RID: 2408
		[CompilerGenerated]
		private readonly ReadOnlyCollection<ElementInit> <Initializers>k__BackingField;
	}
}
