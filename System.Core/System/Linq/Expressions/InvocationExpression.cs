﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Dynamic.Utils;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Represents an expression that applies a delegate or lambda expression to a list of argument expressions.</summary>
	// Token: 0x02000248 RID: 584
	[DebuggerTypeProxy(typeof(Expression.InvocationExpressionProxy))]
	public class InvocationExpression : Expression, IArgumentProvider
	{
		// Token: 0x06001111 RID: 4369 RVA: 0x000349A2 File Offset: 0x00032BA2
		internal InvocationExpression(Expression expression, Type returnType)
		{
			this.Expression = expression;
			this.Type = returnType;
		}

		/// <summary>Gets the static type of the expression that this <see cref="P:System.Linq.Expressions.InvocationExpression.Expression" /> represents.</summary>
		/// <returns>The <see cref="P:System.Linq.Expressions.InvocationExpression.Type" /> that represents the static type of the expression.</returns>
		// Token: 0x170002F4 RID: 756
		// (get) Token: 0x06001112 RID: 4370 RVA: 0x000349B8 File Offset: 0x00032BB8
		public sealed override Type Type
		{
			[CompilerGenerated]
			get
			{
				return this.<Type>k__BackingField;
			}
		}

		/// <summary>Returns the node type of this expression. Extension nodes should return <see cref="F:System.Linq.Expressions.ExpressionType.Extension" /> when overriding this method.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.ExpressionType" /> of the expression.</returns>
		// Token: 0x170002F5 RID: 757
		// (get) Token: 0x06001113 RID: 4371 RVA: 0x000349C0 File Offset: 0x00032BC0
		public sealed override ExpressionType NodeType
		{
			get
			{
				return ExpressionType.Invoke;
			}
		}

		/// <summary>Gets the delegate or lambda expression to be applied.</summary>
		/// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> that represents the delegate to be applied.</returns>
		// Token: 0x170002F6 RID: 758
		// (get) Token: 0x06001114 RID: 4372 RVA: 0x000349C4 File Offset: 0x00032BC4
		public Expression Expression
		{
			[CompilerGenerated]
			get
			{
				return this.<Expression>k__BackingField;
			}
		}

		/// <summary>Gets the arguments that the delegate or lambda expression is applied to.</summary>
		/// <returns>A <see cref="T:System.Collections.ObjectModel.ReadOnlyCollection`1" /> of <see cref="T:System.Linq.Expressions.Expression" /> objects which represent the arguments that the delegate is applied to.</returns>
		// Token: 0x170002F7 RID: 759
		// (get) Token: 0x06001115 RID: 4373 RVA: 0x000349CC File Offset: 0x00032BCC
		public ReadOnlyCollection<Expression> Arguments
		{
			get
			{
				return this.GetOrMakeArguments();
			}
		}

		/// <summary>Creates a new expression that is like this one, but using the supplied children. If all of the children are the same, it will return this expression.</summary>
		/// <param name="expression">The <see cref="P:System.Linq.Expressions.InvocationExpression.Expression" /> property of the result.</param>
		/// <param name="arguments">The <see cref="P:System.Linq.Expressions.InvocationExpression.Arguments" /> property of the result.</param>
		/// <returns>This expression if no children are changed or an expression with the updated children.</returns>
		// Token: 0x06001116 RID: 4374 RVA: 0x000349D4 File Offset: 0x00032BD4
		public InvocationExpression Update(Expression expression, IEnumerable<Expression> arguments)
		{
			if ((expression == this.Expression & arguments != null) && ExpressionUtils.SameElements<Expression>(ref arguments, this.Arguments))
			{
				return this;
			}
			return Expression.Invoke(expression, arguments);
		}

		// Token: 0x06001117 RID: 4375 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		internal virtual ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			throw ContractUtils.Unreachable;
		}

		// Token: 0x06001118 RID: 4376 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		public virtual Expression GetArgument(int index)
		{
			throw ContractUtils.Unreachable;
		}

		// Token: 0x170002F8 RID: 760
		// (get) Token: 0x06001119 RID: 4377 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		public virtual int ArgumentCount
		{
			get
			{
				throw ContractUtils.Unreachable;
			}
		}

		// Token: 0x0600111A RID: 4378 RVA: 0x000349FE File Offset: 0x00032BFE
		protected internal override Expression Accept(ExpressionVisitor visitor)
		{
			return visitor.VisitInvocation(this);
		}

		// Token: 0x0600111B RID: 4379 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		internal virtual InvocationExpression Rewrite(Expression lambda, Expression[] arguments)
		{
			throw ContractUtils.Unreachable;
		}

		// Token: 0x170002F9 RID: 761
		// (get) Token: 0x0600111C RID: 4380 RVA: 0x00034A07 File Offset: 0x00032C07
		internal LambdaExpression LambdaOperand
		{
			get
			{
				if (this.Expression.NodeType != ExpressionType.Quote)
				{
					return this.Expression as LambdaExpression;
				}
				return (LambdaExpression)((UnaryExpression)this.Expression).Operand;
			}
		}

		// Token: 0x0600111D RID: 4381 RVA: 0x0000220F File Offset: 0x0000040F
		internal InvocationExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000937 RID: 2359
		[CompilerGenerated]
		private readonly Type <Type>k__BackingField;

		// Token: 0x04000938 RID: 2360
		[CompilerGenerated]
		private readonly Expression <Expression>k__BackingField;
	}
}
