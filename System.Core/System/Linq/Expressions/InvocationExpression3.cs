﻿using System;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;

namespace System.Linq.Expressions
{
	// Token: 0x0200024D RID: 589
	internal sealed class InvocationExpression3 : InvocationExpression
	{
		// Token: 0x06001132 RID: 4402 RVA: 0x00034B8B File Offset: 0x00032D8B
		public InvocationExpression3(Expression lambda, Type returnType, Expression arg0, Expression arg1, Expression arg2) : base(lambda, returnType)
		{
			this._arg0 = arg0;
			this._arg1 = arg1;
			this._arg2 = arg2;
		}

		// Token: 0x06001133 RID: 4403 RVA: 0x00034BAC File Offset: 0x00032DAC
		internal override ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			return ExpressionUtils.ReturnReadOnly(this, ref this._arg0);
		}

		// Token: 0x06001134 RID: 4404 RVA: 0x00034BBA File Offset: 0x00032DBA
		public override Expression GetArgument(int index)
		{
			switch (index)
			{
			case 0:
				return ExpressionUtils.ReturnObject<Expression>(this._arg0);
			case 1:
				return this._arg1;
			case 2:
				return this._arg2;
			default:
				throw new ArgumentOutOfRangeException("index");
			}
		}

		// Token: 0x170002FE RID: 766
		// (get) Token: 0x06001135 RID: 4405 RVA: 0x0002EF32 File Offset: 0x0002D132
		public override int ArgumentCount
		{
			get
			{
				return 3;
			}
		}

		// Token: 0x06001136 RID: 4406 RVA: 0x00034BF4 File Offset: 0x00032DF4
		internal override InvocationExpression Rewrite(Expression lambda, Expression[] arguments)
		{
			if (arguments != null)
			{
				return Expression.Invoke(lambda, arguments[0], arguments[1], arguments[2]);
			}
			return Expression.Invoke(lambda, ExpressionUtils.ReturnObject<Expression>(this._arg0), this._arg1, this._arg2);
		}

		// Token: 0x0400093D RID: 2365
		private object _arg0;

		// Token: 0x0400093E RID: 2366
		private readonly Expression _arg1;

		// Token: 0x0400093F RID: 2367
		private readonly Expression _arg2;
	}
}
