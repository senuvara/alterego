﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x02000233 RID: 563
	internal class DynamicExpression1 : DynamicExpression, IArgumentProvider
	{
		// Token: 0x06000FA1 RID: 4001 RVA: 0x00031681 File Offset: 0x0002F881
		internal DynamicExpression1(Type delegateType, CallSiteBinder binder, Expression arg0) : base(delegateType, binder)
		{
			this._arg0 = arg0;
		}

		// Token: 0x06000FA2 RID: 4002 RVA: 0x00031692 File Offset: 0x0002F892
		Expression IArgumentProvider.GetArgument(int index)
		{
			if (index == 0)
			{
				return ExpressionUtils.ReturnObject<Expression>(this._arg0);
			}
			throw new ArgumentOutOfRangeException("index");
		}

		// Token: 0x170002DB RID: 731
		// (get) Token: 0x06000FA3 RID: 4003 RVA: 0x00009CDF File Offset: 0x00007EDF
		int IArgumentProvider.ArgumentCount
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x06000FA4 RID: 4004 RVA: 0x000316B0 File Offset: 0x0002F8B0
		internal override bool SameArguments(ICollection<Expression> arguments)
		{
			if (arguments != null && arguments.Count == 1)
			{
				using (IEnumerator<Expression> enumerator = arguments.GetEnumerator())
				{
					enumerator.MoveNext();
					return enumerator.Current == ExpressionUtils.ReturnObject<Expression>(this._arg0);
				}
				return false;
			}
			return false;
		}

		// Token: 0x06000FA5 RID: 4005 RVA: 0x0003170C File Offset: 0x0002F90C
		internal override ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			return ExpressionUtils.ReturnReadOnly(this, ref this._arg0);
		}

		// Token: 0x06000FA6 RID: 4006 RVA: 0x0003171A File Offset: 0x0002F91A
		internal override DynamicExpression Rewrite(Expression[] args)
		{
			return ExpressionExtension.MakeDynamic(base.DelegateType, base.Binder, args[0]);
		}

		// Token: 0x040008C3 RID: 2243
		private object _arg0;
	}
}
