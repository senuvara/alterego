﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;
using System.Reflection;

namespace System.Linq.Expressions
{
	// Token: 0x02000272 RID: 626
	internal sealed class InstanceMethodCallExpression2 : InstanceMethodCallExpression, IArgumentProvider
	{
		// Token: 0x06001224 RID: 4644 RVA: 0x0003616E File Offset: 0x0003436E
		public InstanceMethodCallExpression2(MethodInfo method, Expression instance, Expression arg0, Expression arg1) : base(method, instance)
		{
			this._arg0 = arg0;
			this._arg1 = arg1;
		}

		// Token: 0x06001225 RID: 4645 RVA: 0x00036187 File Offset: 0x00034387
		public override Expression GetArgument(int index)
		{
			if (index == 0)
			{
				return ExpressionUtils.ReturnObject<Expression>(this._arg0);
			}
			if (index != 1)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			return this._arg1;
		}

		// Token: 0x17000346 RID: 838
		// (get) Token: 0x06001226 RID: 4646 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ArgumentCount
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x06001227 RID: 4647 RVA: 0x000361B0 File Offset: 0x000343B0
		internal override bool SameArguments(ICollection<Expression> arguments)
		{
			if (arguments != null && arguments.Count == 2)
			{
				ReadOnlyCollection<Expression> readOnlyCollection = this._arg0 as ReadOnlyCollection<Expression>;
				if (readOnlyCollection != null)
				{
					return ExpressionUtils.SameElements<Expression>(arguments, readOnlyCollection);
				}
				using (IEnumerator<Expression> enumerator = arguments.GetEnumerator())
				{
					enumerator.MoveNext();
					if (enumerator.Current == this._arg0)
					{
						enumerator.MoveNext();
						return enumerator.Current == this._arg1;
					}
				}
				return false;
			}
			return false;
		}

		// Token: 0x06001228 RID: 4648 RVA: 0x00036234 File Offset: 0x00034434
		internal override ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			return ExpressionUtils.ReturnReadOnly(this, ref this._arg0);
		}

		// Token: 0x06001229 RID: 4649 RVA: 0x00036242 File Offset: 0x00034442
		internal override MethodCallExpression Rewrite(Expression instance, IReadOnlyList<Expression> args)
		{
			if (args != null)
			{
				return Expression.Call(instance, base.Method, args[0], args[1]);
			}
			return Expression.Call(instance, base.Method, ExpressionUtils.ReturnObject<Expression>(this._arg0), this._arg1);
		}

		// Token: 0x0400097E RID: 2430
		private object _arg0;

		// Token: 0x0400097F RID: 2431
		private readonly Expression _arg1;
	}
}
