﻿using System;
using System.Collections.ObjectModel;

namespace System.Linq.Expressions
{
	// Token: 0x02000275 RID: 629
	internal sealed class NewArrayInitExpression : NewArrayExpression
	{
		// Token: 0x06001237 RID: 4663 RVA: 0x00036476 File Offset: 0x00034676
		internal NewArrayInitExpression(Type type, ReadOnlyCollection<Expression> expressions) : base(type, expressions)
		{
		}

		// Token: 0x1700034A RID: 842
		// (get) Token: 0x06001238 RID: 4664 RVA: 0x00036480 File Offset: 0x00034680
		public sealed override ExpressionType NodeType
		{
			get
			{
				return ExpressionType.NewArrayInit;
			}
		}
	}
}
