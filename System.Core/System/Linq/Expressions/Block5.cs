﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;

namespace System.Linq.Expressions
{
	// Token: 0x02000219 RID: 537
	internal sealed class Block5 : BlockExpression
	{
		// Token: 0x06000EB4 RID: 3764 RVA: 0x0002F09C File Offset: 0x0002D29C
		internal Block5(Expression arg0, Expression arg1, Expression arg2, Expression arg3, Expression arg4)
		{
			this._arg0 = arg0;
			this._arg1 = arg1;
			this._arg2 = arg2;
			this._arg3 = arg3;
			this._arg4 = arg4;
		}

		// Token: 0x06000EB5 RID: 3765 RVA: 0x0002F0CC File Offset: 0x0002D2CC
		internal override Expression GetExpression(int index)
		{
			switch (index)
			{
			case 0:
				return ExpressionUtils.ReturnObject<Expression>(this._arg0);
			case 1:
				return this._arg1;
			case 2:
				return this._arg2;
			case 3:
				return this._arg3;
			case 4:
				return this._arg4;
			default:
				throw Error.ArgumentOutOfRange("index");
			}
		}

		// Token: 0x06000EB6 RID: 3766 RVA: 0x0002F128 File Offset: 0x0002D328
		internal override bool SameExpressions(ICollection<Expression> expressions)
		{
			if (expressions.Count == 5)
			{
				ReadOnlyCollection<Expression> readOnlyCollection = this._arg0 as ReadOnlyCollection<Expression>;
				if (readOnlyCollection != null)
				{
					return ExpressionUtils.SameElements<Expression>(expressions, readOnlyCollection);
				}
				using (IEnumerator<Expression> enumerator = expressions.GetEnumerator())
				{
					enumerator.MoveNext();
					if (enumerator.Current == this._arg0)
					{
						enumerator.MoveNext();
						if (enumerator.Current == this._arg1)
						{
							enumerator.MoveNext();
							if (enumerator.Current == this._arg2)
							{
								enumerator.MoveNext();
								if (enumerator.Current == this._arg3)
								{
									enumerator.MoveNext();
									return enumerator.Current == this._arg4;
								}
							}
						}
					}
				}
				return false;
			}
			return false;
		}

		// Token: 0x170002A1 RID: 673
		// (get) Token: 0x06000EB7 RID: 3767 RVA: 0x0002F1EC File Offset: 0x0002D3EC
		internal override int ExpressionCount
		{
			get
			{
				return 5;
			}
		}

		// Token: 0x06000EB8 RID: 3768 RVA: 0x0002F1EF File Offset: 0x0002D3EF
		internal override ReadOnlyCollection<Expression> GetOrMakeExpressions()
		{
			return BlockExpression.ReturnReadOnlyExpressions(this, ref this._arg0);
		}

		// Token: 0x06000EB9 RID: 3769 RVA: 0x0002F1FD File Offset: 0x0002D3FD
		internal override BlockExpression Rewrite(ReadOnlyCollection<ParameterExpression> variables, Expression[] args)
		{
			return new Block5(args[0], args[1], args[2], args[3], args[4]);
		}

		// Token: 0x0400088A RID: 2186
		private object _arg0;

		// Token: 0x0400088B RID: 2187
		private readonly Expression _arg1;

		// Token: 0x0400088C RID: 2188
		private readonly Expression _arg2;

		// Token: 0x0400088D RID: 2189
		private readonly Expression _arg3;

		// Token: 0x0400088E RID: 2190
		private readonly Expression _arg4;
	}
}
