﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;

namespace System.Linq.Expressions
{
	// Token: 0x02000259 RID: 601
	internal class ExpressionN<TDelegate> : Expression<TDelegate>
	{
		// Token: 0x06001189 RID: 4489 RVA: 0x0003532B File Offset: 0x0003352B
		public ExpressionN(Expression body, IReadOnlyList<ParameterExpression> parameters) : base(body)
		{
			this._parameters = parameters;
		}

		// Token: 0x1700031A RID: 794
		// (get) Token: 0x0600118A RID: 4490 RVA: 0x0003533B File Offset: 0x0003353B
		internal override int ParameterCount
		{
			get
			{
				return this._parameters.Count;
			}
		}

		// Token: 0x0600118B RID: 4491 RVA: 0x00035348 File Offset: 0x00033548
		internal override ParameterExpression GetParameter(int index)
		{
			return this._parameters[index];
		}

		// Token: 0x0600118C RID: 4492 RVA: 0x00035356 File Offset: 0x00033556
		internal override bool SameParameters(ICollection<ParameterExpression> parameters)
		{
			return ExpressionUtils.SameElements<ParameterExpression>(parameters, this._parameters);
		}

		// Token: 0x0600118D RID: 4493 RVA: 0x00035364 File Offset: 0x00033564
		internal override ReadOnlyCollection<ParameterExpression> GetOrMakeParameters()
		{
			return ExpressionUtils.ReturnReadOnly<ParameterExpression>(ref this._parameters);
		}

		// Token: 0x0600118E RID: 4494 RVA: 0x00035374 File Offset: 0x00033574
		internal override Expression<TDelegate> Rewrite(Expression body, ParameterExpression[] parameters)
		{
			return Expression.Lambda<TDelegate>(body, base.Name, base.TailCall, parameters ?? this._parameters);
		}

		// Token: 0x04000954 RID: 2388
		private IReadOnlyList<ParameterExpression> _parameters;
	}
}
