﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Dynamic.Utils;
using System.Linq.Expressions.Interpreter;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Represents a strongly typed lambda expression as a data structure in the form of an expression tree. This class cannot be inherited.</summary>
	/// <typeparam name="TDelegate">The type of the delegate that the <see cref="T:System.Linq.Expressions.Expression`1" /> represents.</typeparam>
	// Token: 0x02000253 RID: 595
	public class Expression<TDelegate> : LambdaExpression
	{
		// Token: 0x06001165 RID: 4453 RVA: 0x00034EF6 File Offset: 0x000330F6
		internal Expression(Expression body) : base(body)
		{
		}

		// Token: 0x17000314 RID: 788
		// (get) Token: 0x06001166 RID: 4454 RVA: 0x0000B7AA File Offset: 0x000099AA
		internal sealed override Type TypeCore
		{
			get
			{
				return typeof(TDelegate);
			}
		}

		// Token: 0x17000315 RID: 789
		// (get) Token: 0x06001167 RID: 4455 RVA: 0x00034EFF File Offset: 0x000330FF
		internal override Type PublicType
		{
			get
			{
				return typeof(Expression<TDelegate>);
			}
		}

		/// <summary>Compiles the lambda expression described by the expression tree into executable code and produces a delegate that represents the lambda expression.</summary>
		/// <returns>A delegate of type <paramref name="TDelegate" /> that represents the compiled lambda expression described by the <see cref="T:System.Linq.Expressions.Expression`1" />.</returns>
		// Token: 0x06001168 RID: 4456 RVA: 0x00034F0B File Offset: 0x0003310B
		public new TDelegate Compile()
		{
			return this.Compile(false);
		}

		/// <summary>Compiles the lambda expression described by the expression tree into interpreted or compiled code and produces a delegate that represents the lambda expression.</summary>
		/// <param name="preferInterpretation">
		///   <see langword="true" /> to indicate that the expression should be compiled to an interpreted form, if it is available; <see langword="false" /> otherwise.</param>
		/// <returns>A delegate that represents the compiled lambda expression described by the <see cref="T:System.Linq.Expressions.Expression`1" />.</returns>
		// Token: 0x06001169 RID: 4457 RVA: 0x00034F14 File Offset: 0x00033114
		public new TDelegate Compile(bool preferInterpretation)
		{
			return (TDelegate)((object)new LightCompiler().CompileTop(this).CreateDelegate());
		}

		/// <summary>Creates a new expression that is like this one, but using the supplied children. If all of the children are the same, it will return this expression.</summary>
		/// <param name="body">The <see cref="P:System.Linq.Expressions.LambdaExpression.Body" /> property of the result.</param>
		/// <param name="parameters">The <see cref="P:System.Linq.Expressions.LambdaExpression.Parameters" /> property of the result. </param>
		/// <returns>This expression if no children are changed or an expression with the updated children.</returns>
		// Token: 0x0600116A RID: 4458 RVA: 0x00034F2C File Offset: 0x0003312C
		public Expression<TDelegate> Update(Expression body, IEnumerable<ParameterExpression> parameters)
		{
			if (body == base.Body)
			{
				ICollection<ParameterExpression> collection;
				if (parameters == null)
				{
					collection = null;
				}
				else
				{
					collection = (parameters as ICollection<ParameterExpression>);
					if (collection == null)
					{
						collection = (parameters = parameters.ToReadOnly<ParameterExpression>());
					}
				}
				if (this.SameParameters(collection))
				{
					return this;
				}
			}
			return Expression.Lambda<TDelegate>(body, base.Name, base.TailCall, parameters);
		}

		// Token: 0x0600116B RID: 4459 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		internal virtual bool SameParameters(ICollection<ParameterExpression> parameters)
		{
			throw ContractUtils.Unreachable;
		}

		// Token: 0x0600116C RID: 4460 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		internal virtual Expression<TDelegate> Rewrite(Expression body, ParameterExpression[] parameters)
		{
			throw ContractUtils.Unreachable;
		}

		// Token: 0x0600116D RID: 4461 RVA: 0x00034F7B File Offset: 0x0003317B
		protected internal override Expression Accept(ExpressionVisitor visitor)
		{
			return visitor.VisitLambda<TDelegate>(this);
		}

		/// <summary>Produces a delegate that represents the lambda expression.</summary>
		/// <param name="debugInfoGenerator">Debugging information generator used by the compiler to mark sequence points and annotate local variables.</param>
		/// <returns>A delegate containing the compiled version of the lambda.</returns>
		// Token: 0x0600116E RID: 4462 RVA: 0x00034F84 File Offset: 0x00033184
		public new TDelegate Compile(DebugInfoGenerator debugInfoGenerator)
		{
			return this.Compile();
		}

		// Token: 0x0600116F RID: 4463 RVA: 0x0000220F File Offset: 0x0000040F
		internal Expression()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
