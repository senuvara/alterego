﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;

namespace System.Linq.Expressions
{
	// Token: 0x02000249 RID: 585
	internal sealed class InvocationExpressionN : InvocationExpression
	{
		// Token: 0x0600111E RID: 4382 RVA: 0x00034A39 File Offset: 0x00032C39
		public InvocationExpressionN(Expression lambda, IReadOnlyList<Expression> arguments, Type returnType) : base(lambda, returnType)
		{
			this._arguments = arguments;
		}

		// Token: 0x0600111F RID: 4383 RVA: 0x00034A4A File Offset: 0x00032C4A
		internal override ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			return ExpressionUtils.ReturnReadOnly<Expression>(ref this._arguments);
		}

		// Token: 0x06001120 RID: 4384 RVA: 0x00034A57 File Offset: 0x00032C57
		public override Expression GetArgument(int index)
		{
			return this._arguments[index];
		}

		// Token: 0x170002FA RID: 762
		// (get) Token: 0x06001121 RID: 4385 RVA: 0x00034A65 File Offset: 0x00032C65
		public override int ArgumentCount
		{
			get
			{
				return this._arguments.Count;
			}
		}

		// Token: 0x06001122 RID: 4386 RVA: 0x00034A74 File Offset: 0x00032C74
		internal override InvocationExpression Rewrite(Expression lambda, Expression[] arguments)
		{
			return Expression.Invoke(lambda, arguments ?? this._arguments);
		}

		// Token: 0x04000939 RID: 2361
		private IReadOnlyList<Expression> _arguments;
	}
}
