﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Represents an expression that has a constant value.</summary>
	// Token: 0x02000228 RID: 552
	[DebuggerTypeProxy(typeof(Expression.ConstantExpressionProxy))]
	public class ConstantExpression : Expression
	{
		// Token: 0x06000F06 RID: 3846 RVA: 0x0002F89B File Offset: 0x0002DA9B
		internal ConstantExpression(object value)
		{
			this.Value = value;
		}

		/// <summary>Gets the static type of the expression that this <see cref="T:System.Linq.Expressions.Expression" /> represents.</summary>
		/// <returns>The <see cref="P:System.Linq.Expressions.ConstantExpression.Type" /> that represents the static type of the expression.</returns>
		// Token: 0x170002B7 RID: 695
		// (get) Token: 0x06000F07 RID: 3847 RVA: 0x0002F8AA File Offset: 0x0002DAAA
		public override Type Type
		{
			get
			{
				if (this.Value == null)
				{
					return typeof(object);
				}
				return this.Value.GetType();
			}
		}

		/// <summary>Returns the node type of this Expression. Extension nodes should return <see cref="F:System.Linq.Expressions.ExpressionType.Extension" /> when overriding this method.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.ExpressionType" /> of the expression.</returns>
		// Token: 0x170002B8 RID: 696
		// (get) Token: 0x06000F08 RID: 3848 RVA: 0x0002F8CA File Offset: 0x0002DACA
		public sealed override ExpressionType NodeType
		{
			get
			{
				return ExpressionType.Constant;
			}
		}

		/// <summary>Gets the value of the constant expression.</summary>
		/// <returns>An <see cref="T:System.Object" /> equal to the value of the represented expression.</returns>
		// Token: 0x170002B9 RID: 697
		// (get) Token: 0x06000F09 RID: 3849 RVA: 0x0002F8CE File Offset: 0x0002DACE
		public object Value
		{
			[CompilerGenerated]
			get
			{
				return this.<Value>k__BackingField;
			}
		}

		/// <summary>Dispatches to the specific visit method for this node type. For example, <see cref="T:System.Linq.Expressions.MethodCallExpression" /> calls the <see cref="M:System.Linq.Expressions.ExpressionVisitor.VisitMethodCall(System.Linq.Expressions.MethodCallExpression)" />.</summary>
		/// <param name="visitor">The visitor to visit this node with.</param>
		/// <returns>The result of visiting this node.</returns>
		// Token: 0x06000F0A RID: 3850 RVA: 0x0002F8D6 File Offset: 0x0002DAD6
		protected internal override Expression Accept(ExpressionVisitor visitor)
		{
			return visitor.VisitConstant(this);
		}

		// Token: 0x06000F0B RID: 3851 RVA: 0x0000220F File Offset: 0x0000040F
		internal ConstantExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040008A7 RID: 2215
		[CompilerGenerated]
		private readonly object <Value>k__BackingField;
	}
}
