﻿using System;

namespace System.Linq.Expressions
{
	// Token: 0x020001F2 RID: 498
	internal class ByRefAssignBinaryExpression : AssignBinaryExpression
	{
		// Token: 0x06000C2B RID: 3115 RVA: 0x00027188 File Offset: 0x00025388
		internal ByRefAssignBinaryExpression(Expression left, Expression right) : base(left, right)
		{
		}

		// Token: 0x170001E6 RID: 486
		// (get) Token: 0x06000C2C RID: 3116 RVA: 0x00009CDF File Offset: 0x00007EDF
		internal override bool IsByRef
		{
			get
			{
				return true;
			}
		}
	}
}
