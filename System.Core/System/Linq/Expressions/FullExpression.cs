﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x0200025A RID: 602
	internal sealed class FullExpression<TDelegate> : ExpressionN<TDelegate>
	{
		// Token: 0x0600118F RID: 4495 RVA: 0x000353A0 File Offset: 0x000335A0
		public FullExpression(Expression body, string name, bool tailCall, IReadOnlyList<ParameterExpression> parameters) : base(body, parameters)
		{
			this.NameCore = name;
			this.TailCallCore = tailCall;
		}

		// Token: 0x1700031B RID: 795
		// (get) Token: 0x06001190 RID: 4496 RVA: 0x000353B9 File Offset: 0x000335B9
		internal override string NameCore
		{
			[CompilerGenerated]
			get
			{
				return this.<NameCore>k__BackingField;
			}
		}

		// Token: 0x1700031C RID: 796
		// (get) Token: 0x06001191 RID: 4497 RVA: 0x000353C1 File Offset: 0x000335C1
		internal override bool TailCallCore
		{
			[CompilerGenerated]
			get
			{
				return this.<TailCallCore>k__BackingField;
			}
		}

		// Token: 0x04000955 RID: 2389
		[CompilerGenerated]
		private readonly string <NameCore>k__BackingField;

		// Token: 0x04000956 RID: 2390
		[CompilerGenerated]
		private readonly bool <TailCallCore>k__BackingField;
	}
}
