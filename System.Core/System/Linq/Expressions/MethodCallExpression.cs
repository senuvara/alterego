﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Dynamic.Utils;
using System.Reflection;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Represents a call to either static or an instance method.</summary>
	// Token: 0x02000266 RID: 614
	[DebuggerTypeProxy(typeof(Expression.MethodCallExpressionProxy))]
	public class MethodCallExpression : Expression, IArgumentProvider
	{
		// Token: 0x060011D7 RID: 4567 RVA: 0x0003586E File Offset: 0x00033A6E
		internal MethodCallExpression(MethodInfo method)
		{
			this.Method = method;
		}

		// Token: 0x060011D8 RID: 4568 RVA: 0x000053B1 File Offset: 0x000035B1
		internal virtual Expression GetInstance()
		{
			return null;
		}

		/// <summary>Returns the node type of this <see cref="T:System.Linq.Expressions.Expression" />.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.ExpressionType" /> that represents this expression.</returns>
		// Token: 0x17000336 RID: 822
		// (get) Token: 0x060011D9 RID: 4569 RVA: 0x0003587D File Offset: 0x00033A7D
		public sealed override ExpressionType NodeType
		{
			get
			{
				return ExpressionType.Call;
			}
		}

		/// <summary>Gets the static type of the expression that this <see cref="T:System.Linq.Expressions.Expression" /> represents.</summary>
		/// <returns>The <see cref="P:System.Linq.Expressions.MethodCallExpression.Type" /> that represents the static type of the expression.</returns>
		// Token: 0x17000337 RID: 823
		// (get) Token: 0x060011DA RID: 4570 RVA: 0x00035880 File Offset: 0x00033A80
		public sealed override Type Type
		{
			get
			{
				return this.Method.ReturnType;
			}
		}

		/// <summary>Gets the <see cref="T:System.Reflection.MethodInfo" /> for the method to be called.</summary>
		/// <returns>The <see cref="T:System.Reflection.MethodInfo" /> that represents the called method.</returns>
		// Token: 0x17000338 RID: 824
		// (get) Token: 0x060011DB RID: 4571 RVA: 0x0003588D File Offset: 0x00033A8D
		public MethodInfo Method
		{
			[CompilerGenerated]
			get
			{
				return this.<Method>k__BackingField;
			}
		}

		/// <summary>Gets the <see cref="T:System.Linq.Expressions.Expression" /> that represents the instance for instance method calls or null for static method calls.</summary>
		/// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> that represents the receiving object of the method.</returns>
		// Token: 0x17000339 RID: 825
		// (get) Token: 0x060011DC RID: 4572 RVA: 0x00035895 File Offset: 0x00033A95
		public Expression Object
		{
			get
			{
				return this.GetInstance();
			}
		}

		/// <summary>Gets a collection of expressions that represent arguments of the called method.</summary>
		/// <returns>A <see cref="T:System.Collections.ObjectModel.ReadOnlyCollection`1" /> of <see cref="T:System.Linq.Expressions.Expression" /> objects which represent the arguments to the called method.</returns>
		// Token: 0x1700033A RID: 826
		// (get) Token: 0x060011DD RID: 4573 RVA: 0x0003589D File Offset: 0x00033A9D
		public ReadOnlyCollection<Expression> Arguments
		{
			get
			{
				return this.GetOrMakeArguments();
			}
		}

		/// <summary>Creates a new expression that is like this one, but using the supplied children. If all of the children are the same, it will return this expression.</summary>
		/// <param name="object">The <see cref="P:System.Linq.Expressions.MethodCallExpression.Object" /> property of the result.</param>
		/// <param name="arguments">The <see cref="P:System.Linq.Expressions.MethodCallExpression.Arguments" /> property of the result.</param>
		/// <returns>This expression if no children are changed or an expression with the updated children.</returns>
		// Token: 0x060011DE RID: 4574 RVA: 0x000358A8 File Offset: 0x00033AA8
		public MethodCallExpression Update(Expression @object, IEnumerable<Expression> arguments)
		{
			if (@object == this.Object)
			{
				ICollection<Expression> collection;
				if (arguments == null)
				{
					collection = null;
				}
				else
				{
					collection = (arguments as ICollection<Expression>);
					if (collection == null)
					{
						collection = (arguments = arguments.ToReadOnly<Expression>());
					}
				}
				if (this.SameArguments(collection))
				{
					return this;
				}
			}
			return Expression.Call(@object, this.Method, arguments);
		}

		// Token: 0x060011DF RID: 4575 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		internal virtual bool SameArguments(ICollection<Expression> arguments)
		{
			throw ContractUtils.Unreachable;
		}

		// Token: 0x060011E0 RID: 4576 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		internal virtual ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			throw ContractUtils.Unreachable;
		}

		/// <summary>Dispatches to the specific visit method for this node type. For example, <see cref="T:System.Linq.Expressions.MethodCallExpression" /> calls the <see cref="M:System.Linq.Expressions.ExpressionVisitor.VisitMethodCall(System.Linq.Expressions.MethodCallExpression)" />.</summary>
		/// <param name="visitor">The visitor to visit this node with.</param>
		/// <returns>The result of visiting this node.</returns>
		// Token: 0x060011E1 RID: 4577 RVA: 0x000358F1 File Offset: 0x00033AF1
		protected internal override Expression Accept(ExpressionVisitor visitor)
		{
			return visitor.VisitMethodCall(this);
		}

		// Token: 0x060011E2 RID: 4578 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		internal virtual MethodCallExpression Rewrite(Expression instance, IReadOnlyList<Expression> args)
		{
			throw ContractUtils.Unreachable;
		}

		// Token: 0x060011E3 RID: 4579 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		public virtual Expression GetArgument(int index)
		{
			throw ContractUtils.Unreachable;
		}

		// Token: 0x1700033B RID: 827
		// (get) Token: 0x060011E4 RID: 4580 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		public virtual int ArgumentCount
		{
			get
			{
				throw ContractUtils.Unreachable;
			}
		}

		// Token: 0x060011E5 RID: 4581 RVA: 0x0000220F File Offset: 0x0000040F
		internal MethodCallExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0400096A RID: 2410
		[CompilerGenerated]
		private readonly MethodInfo <Method>k__BackingField;
	}
}
