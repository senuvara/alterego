﻿using System;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;

namespace System.Linq.Expressions
{
	// Token: 0x0200024E RID: 590
	internal sealed class InvocationExpression4 : InvocationExpression
	{
		// Token: 0x06001137 RID: 4407 RVA: 0x00034C26 File Offset: 0x00032E26
		public InvocationExpression4(Expression lambda, Type returnType, Expression arg0, Expression arg1, Expression arg2, Expression arg3) : base(lambda, returnType)
		{
			this._arg0 = arg0;
			this._arg1 = arg1;
			this._arg2 = arg2;
			this._arg3 = arg3;
		}

		// Token: 0x06001138 RID: 4408 RVA: 0x00034C4F File Offset: 0x00032E4F
		internal override ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			return ExpressionUtils.ReturnReadOnly(this, ref this._arg0);
		}

		// Token: 0x06001139 RID: 4409 RVA: 0x00034C60 File Offset: 0x00032E60
		public override Expression GetArgument(int index)
		{
			switch (index)
			{
			case 0:
				return ExpressionUtils.ReturnObject<Expression>(this._arg0);
			case 1:
				return this._arg1;
			case 2:
				return this._arg2;
			case 3:
				return this._arg3;
			default:
				throw new ArgumentOutOfRangeException("index");
			}
		}

		// Token: 0x170002FF RID: 767
		// (get) Token: 0x0600113A RID: 4410 RVA: 0x0002F078 File Offset: 0x0002D278
		public override int ArgumentCount
		{
			get
			{
				return 4;
			}
		}

		// Token: 0x0600113B RID: 4411 RVA: 0x00034CB0 File Offset: 0x00032EB0
		internal override InvocationExpression Rewrite(Expression lambda, Expression[] arguments)
		{
			if (arguments != null)
			{
				return Expression.Invoke(lambda, arguments[0], arguments[1], arguments[2], arguments[3]);
			}
			return Expression.Invoke(lambda, ExpressionUtils.ReturnObject<Expression>(this._arg0), this._arg1, this._arg2, this._arg3);
		}

		// Token: 0x04000940 RID: 2368
		private object _arg0;

		// Token: 0x04000941 RID: 2369
		private readonly Expression _arg1;

		// Token: 0x04000942 RID: 2370
		private readonly Expression _arg2;

		// Token: 0x04000943 RID: 2371
		private readonly Expression _arg3;
	}
}
