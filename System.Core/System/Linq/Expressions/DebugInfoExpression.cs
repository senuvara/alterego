﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Dynamic.Utils;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Emits or clears a sequence point for debug information. This allows the debugger to highlight the correct source code when debugging.</summary>
	// Token: 0x0200022A RID: 554
	[DebuggerTypeProxy(typeof(Expression.DebugInfoExpressionProxy))]
	public class DebugInfoExpression : Expression
	{
		// Token: 0x06000F0E RID: 3854 RVA: 0x0002F8F7 File Offset: 0x0002DAF7
		internal DebugInfoExpression(SymbolDocumentInfo document)
		{
			this.Document = document;
		}

		/// <summary>Gets the static type of the expression that this <see cref="T:System.Linq.Expressions.Expression" /> represents.</summary>
		/// <returns>The <see cref="P:System.Linq.Expressions.DebugInfoExpression.Type" /> that represents the static type of the expression.</returns>
		// Token: 0x170002BB RID: 699
		// (get) Token: 0x06000F0F RID: 3855 RVA: 0x0002F906 File Offset: 0x0002DB06
		public sealed override Type Type
		{
			get
			{
				return typeof(void);
			}
		}

		/// <summary>Returns the node type of this <see cref="T:System.Linq.Expressions.Expression" />.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.ExpressionType" /> that represents this expression.</returns>
		// Token: 0x170002BC RID: 700
		// (get) Token: 0x06000F10 RID: 3856 RVA: 0x0002F912 File Offset: 0x0002DB12
		public sealed override ExpressionType NodeType
		{
			get
			{
				return ExpressionType.DebugInfo;
			}
		}

		/// <summary>Gets the start line of this <see cref="T:System.Linq.Expressions.DebugInfoExpression" />.</summary>
		/// <returns>The number of the start line of the code that was used to generate the wrapped expression.</returns>
		// Token: 0x170002BD RID: 701
		// (get) Token: 0x06000F11 RID: 3857 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		public virtual int StartLine
		{
			get
			{
				throw ContractUtils.Unreachable;
			}
		}

		/// <summary>Gets the start column of this <see cref="T:System.Linq.Expressions.DebugInfoExpression" />.</summary>
		/// <returns>The number of the start column of the code that was used to generate the wrapped expression.</returns>
		// Token: 0x170002BE RID: 702
		// (get) Token: 0x06000F12 RID: 3858 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		public virtual int StartColumn
		{
			get
			{
				throw ContractUtils.Unreachable;
			}
		}

		/// <summary>Gets the end line of this <see cref="T:System.Linq.Expressions.DebugInfoExpression" />.</summary>
		/// <returns>The number of the end line of the code that was used to generate the wrapped expression.</returns>
		// Token: 0x170002BF RID: 703
		// (get) Token: 0x06000F13 RID: 3859 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		public virtual int EndLine
		{
			get
			{
				throw ContractUtils.Unreachable;
			}
		}

		/// <summary>Gets the end column of this <see cref="T:System.Linq.Expressions.DebugInfoExpression" />.</summary>
		/// <returns>The number of the end column of the code that was used to generate the wrapped expression.</returns>
		// Token: 0x170002C0 RID: 704
		// (get) Token: 0x06000F14 RID: 3860 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		public virtual int EndColumn
		{
			get
			{
				throw ContractUtils.Unreachable;
			}
		}

		/// <summary>Gets the <see cref="T:System.Linq.Expressions.SymbolDocumentInfo" /> that represents the source file.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.SymbolDocumentInfo" /> that represents the source file.</returns>
		// Token: 0x170002C1 RID: 705
		// (get) Token: 0x06000F15 RID: 3861 RVA: 0x0002F916 File Offset: 0x0002DB16
		public SymbolDocumentInfo Document
		{
			[CompilerGenerated]
			get
			{
				return this.<Document>k__BackingField;
			}
		}

		/// <summary>Gets the value to indicate if the <see cref="T:System.Linq.Expressions.DebugInfoExpression" /> is for clearing a sequence point.</summary>
		/// <returns>True if the <see cref="T:System.Linq.Expressions.DebugInfoExpression" /> is for clearing a sequence point, otherwise false.</returns>
		// Token: 0x170002C2 RID: 706
		// (get) Token: 0x06000F16 RID: 3862 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		public virtual bool IsClear
		{
			get
			{
				throw ContractUtils.Unreachable;
			}
		}

		/// <summary>Dispatches to the specific visit method for this node type. For example, <see cref="T:System.Linq.Expressions.MethodCallExpression" /> calls the <see cref="M:System.Linq.Expressions.ExpressionVisitor.VisitMethodCall(System.Linq.Expressions.MethodCallExpression)" />.</summary>
		/// <param name="visitor">The visitor to visit this node with.</param>
		/// <returns>The result of visiting this node.</returns>
		// Token: 0x06000F17 RID: 3863 RVA: 0x0002F91E File Offset: 0x0002DB1E
		protected internal override Expression Accept(ExpressionVisitor visitor)
		{
			return visitor.VisitDebugInfo(this);
		}

		// Token: 0x06000F18 RID: 3864 RVA: 0x0000220F File Offset: 0x0000040F
		internal DebugInfoExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040008A9 RID: 2217
		[CompilerGenerated]
		private readonly SymbolDocumentInfo <Document>k__BackingField;
	}
}
