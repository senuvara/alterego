﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Represents an infinite loop. It can be exited with "break".</summary>
	// Token: 0x0200025C RID: 604
	[DebuggerTypeProxy(typeof(Expression.LoopExpressionProxy))]
	public sealed class LoopExpression : Expression
	{
		// Token: 0x0600119C RID: 4508 RVA: 0x00035447 File Offset: 0x00033647
		internal LoopExpression(Expression body, LabelTarget @break, LabelTarget @continue)
		{
			this.Body = body;
			this.BreakLabel = @break;
			this.ContinueLabel = @continue;
		}

		/// <summary>Gets the static type of the expression that this <see cref="T:System.Linq.Expressions.Expression" /> represents.</summary>
		/// <returns>The <see cref="P:System.Linq.Expressions.LoopExpression.Type" /> that represents the static type of the expression.</returns>
		// Token: 0x17000322 RID: 802
		// (get) Token: 0x0600119D RID: 4509 RVA: 0x00035464 File Offset: 0x00033664
		public sealed override Type Type
		{
			get
			{
				if (this.BreakLabel != null)
				{
					return this.BreakLabel.Type;
				}
				return typeof(void);
			}
		}

		/// <summary>Returns the node type of this expression. Extension nodes should return <see cref="F:System.Linq.Expressions.ExpressionType.Extension" /> when overriding this method.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.ExpressionType" /> of the expression.</returns>
		// Token: 0x17000323 RID: 803
		// (get) Token: 0x0600119E RID: 4510 RVA: 0x00035484 File Offset: 0x00033684
		public sealed override ExpressionType NodeType
		{
			get
			{
				return ExpressionType.Loop;
			}
		}

		/// <summary>Gets the <see cref="T:System.Linq.Expressions.Expression" /> that is the body of the loop.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.Expression" /> that is the body of the loop.</returns>
		// Token: 0x17000324 RID: 804
		// (get) Token: 0x0600119F RID: 4511 RVA: 0x00035488 File Offset: 0x00033688
		public Expression Body
		{
			[CompilerGenerated]
			get
			{
				return this.<Body>k__BackingField;
			}
		}

		/// <summary>Gets the <see cref="T:System.Linq.Expressions.LabelTarget" /> that is used by the loop body as a break statement target.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.LabelTarget" /> that is used by the loop body as a break statement target.</returns>
		// Token: 0x17000325 RID: 805
		// (get) Token: 0x060011A0 RID: 4512 RVA: 0x00035490 File Offset: 0x00033690
		public LabelTarget BreakLabel
		{
			[CompilerGenerated]
			get
			{
				return this.<BreakLabel>k__BackingField;
			}
		}

		/// <summary>Gets the <see cref="T:System.Linq.Expressions.LabelTarget" /> that is used by the loop body as a continue statement target.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.LabelTarget" /> that is used by the loop body as a continue statement target.</returns>
		// Token: 0x17000326 RID: 806
		// (get) Token: 0x060011A1 RID: 4513 RVA: 0x00035498 File Offset: 0x00033698
		public LabelTarget ContinueLabel
		{
			[CompilerGenerated]
			get
			{
				return this.<ContinueLabel>k__BackingField;
			}
		}

		// Token: 0x060011A2 RID: 4514 RVA: 0x000354A0 File Offset: 0x000336A0
		protected internal override Expression Accept(ExpressionVisitor visitor)
		{
			return visitor.VisitLoop(this);
		}

		/// <summary>Creates a new expression that is like this one, but using the supplied children. If all of the children are the same, it will return this expression.</summary>
		/// <param name="breakLabel">The <see cref="P:System.Linq.Expressions.LoopExpression.BreakLabel" /> property of the result.</param>
		/// <param name="continueLabel">The <see cref="P:System.Linq.Expressions.LoopExpression.ContinueLabel" /> property of the result.</param>
		/// <param name="body">The <see cref="P:System.Linq.Expressions.LoopExpression.Body" /> property of the result.</param>
		/// <returns>This expression if no children are changed or an expression with the updated children.</returns>
		// Token: 0x060011A3 RID: 4515 RVA: 0x000354A9 File Offset: 0x000336A9
		public LoopExpression Update(LabelTarget breakLabel, LabelTarget continueLabel, Expression body)
		{
			if (breakLabel == this.BreakLabel && continueLabel == this.ContinueLabel && body == this.Body)
			{
				return this;
			}
			return Expression.Loop(body, breakLabel, continueLabel);
		}

		// Token: 0x060011A4 RID: 4516 RVA: 0x0000220F File Offset: 0x0000040F
		internal LoopExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000959 RID: 2393
		[CompilerGenerated]
		private readonly Expression <Body>k__BackingField;

		// Token: 0x0400095A RID: 2394
		[CompilerGenerated]
		private readonly LabelTarget <BreakLabel>k__BackingField;

		// Token: 0x0400095B RID: 2395
		[CompilerGenerated]
		private readonly LabelTarget <ContinueLabel>k__BackingField;
	}
}
