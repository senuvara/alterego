﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x020001EE RID: 494
	internal static class CachedReflectionInfo
	{
		// Token: 0x170001A0 RID: 416
		// (get) Token: 0x06000BD4 RID: 3028 RVA: 0x00025F4C File Offset: 0x0002414C
		public static MethodInfo String_Format_String_ObjectArray
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_String_Format_String_ObjectArray) == null)
				{
					result = (CachedReflectionInfo.s_String_Format_String_ObjectArray = typeof(string).GetMethod("Format", new Type[]
					{
						typeof(string),
						typeof(object[])
					}));
				}
				return result;
			}
		}

		// Token: 0x170001A1 RID: 417
		// (get) Token: 0x06000BD5 RID: 3029 RVA: 0x00025F9C File Offset: 0x0002419C
		public static ConstructorInfo InvalidCastException_Ctor_String
		{
			get
			{
				ConstructorInfo result;
				if ((result = CachedReflectionInfo.s_InvalidCastException_Ctor_String) == null)
				{
					result = (CachedReflectionInfo.s_InvalidCastException_Ctor_String = typeof(InvalidCastException).GetConstructor(new Type[]
					{
						typeof(string)
					}));
				}
				return result;
			}
		}

		// Token: 0x170001A2 RID: 418
		// (get) Token: 0x06000BD6 RID: 3030 RVA: 0x00025FCF File Offset: 0x000241CF
		public static MethodInfo CallSiteOps_SetNotMatched
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_CallSiteOps_SetNotMatched) == null)
				{
					result = (CachedReflectionInfo.s_CallSiteOps_SetNotMatched = typeof(CallSiteOps).GetMethod("SetNotMatched"));
				}
				return result;
			}
		}

		// Token: 0x170001A3 RID: 419
		// (get) Token: 0x06000BD7 RID: 3031 RVA: 0x00025FF4 File Offset: 0x000241F4
		public static MethodInfo CallSiteOps_CreateMatchmaker
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_CallSiteOps_CreateMatchmaker) == null)
				{
					result = (CachedReflectionInfo.s_CallSiteOps_CreateMatchmaker = typeof(CallSiteOps).GetMethod("CreateMatchmaker"));
				}
				return result;
			}
		}

		// Token: 0x170001A4 RID: 420
		// (get) Token: 0x06000BD8 RID: 3032 RVA: 0x00026019 File Offset: 0x00024219
		public static MethodInfo CallSiteOps_GetMatch
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_CallSiteOps_GetMatch) == null)
				{
					result = (CachedReflectionInfo.s_CallSiteOps_GetMatch = typeof(CallSiteOps).GetMethod("GetMatch"));
				}
				return result;
			}
		}

		// Token: 0x170001A5 RID: 421
		// (get) Token: 0x06000BD9 RID: 3033 RVA: 0x0002603E File Offset: 0x0002423E
		public static MethodInfo CallSiteOps_ClearMatch
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_CallSiteOps_ClearMatch) == null)
				{
					result = (CachedReflectionInfo.s_CallSiteOps_ClearMatch = typeof(CallSiteOps).GetMethod("ClearMatch"));
				}
				return result;
			}
		}

		// Token: 0x170001A6 RID: 422
		// (get) Token: 0x06000BDA RID: 3034 RVA: 0x00026063 File Offset: 0x00024263
		public static MethodInfo CallSiteOps_UpdateRules
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_CallSiteOps_UpdateRules) == null)
				{
					result = (CachedReflectionInfo.s_CallSiteOps_UpdateRules = typeof(CallSiteOps).GetMethod("UpdateRules"));
				}
				return result;
			}
		}

		// Token: 0x170001A7 RID: 423
		// (get) Token: 0x06000BDB RID: 3035 RVA: 0x00026088 File Offset: 0x00024288
		public static MethodInfo CallSiteOps_GetRules
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_CallSiteOps_GetRules) == null)
				{
					result = (CachedReflectionInfo.s_CallSiteOps_GetRules = typeof(CallSiteOps).GetMethod("GetRules"));
				}
				return result;
			}
		}

		// Token: 0x170001A8 RID: 424
		// (get) Token: 0x06000BDC RID: 3036 RVA: 0x000260AD File Offset: 0x000242AD
		public static MethodInfo CallSiteOps_GetRuleCache
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_CallSiteOps_GetRuleCache) == null)
				{
					result = (CachedReflectionInfo.s_CallSiteOps_GetRuleCache = typeof(CallSiteOps).GetMethod("GetRuleCache"));
				}
				return result;
			}
		}

		// Token: 0x170001A9 RID: 425
		// (get) Token: 0x06000BDD RID: 3037 RVA: 0x000260D2 File Offset: 0x000242D2
		public static MethodInfo CallSiteOps_GetCachedRules
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_CallSiteOps_GetCachedRules) == null)
				{
					result = (CachedReflectionInfo.s_CallSiteOps_GetCachedRules = typeof(CallSiteOps).GetMethod("GetCachedRules"));
				}
				return result;
			}
		}

		// Token: 0x170001AA RID: 426
		// (get) Token: 0x06000BDE RID: 3038 RVA: 0x000260F7 File Offset: 0x000242F7
		public static MethodInfo CallSiteOps_AddRule
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_CallSiteOps_AddRule) == null)
				{
					result = (CachedReflectionInfo.s_CallSiteOps_AddRule = typeof(CallSiteOps).GetMethod("AddRule"));
				}
				return result;
			}
		}

		// Token: 0x170001AB RID: 427
		// (get) Token: 0x06000BDF RID: 3039 RVA: 0x0002611C File Offset: 0x0002431C
		public static MethodInfo CallSiteOps_MoveRule
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_CallSiteOps_MoveRule) == null)
				{
					result = (CachedReflectionInfo.s_CallSiteOps_MoveRule = typeof(CallSiteOps).GetMethod("MoveRule"));
				}
				return result;
			}
		}

		// Token: 0x170001AC RID: 428
		// (get) Token: 0x06000BE0 RID: 3040 RVA: 0x00026141 File Offset: 0x00024341
		public static MethodInfo CallSiteOps_Bind
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_CallSiteOps_Bind) == null)
				{
					result = (CachedReflectionInfo.s_CallSiteOps_Bind = typeof(CallSiteOps).GetMethod("Bind"));
				}
				return result;
			}
		}

		// Token: 0x170001AD RID: 429
		// (get) Token: 0x06000BE1 RID: 3041 RVA: 0x00026166 File Offset: 0x00024366
		public static MethodInfo DynamicObject_TryGetMember
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_DynamicObject_TryGetMember) == null)
				{
					result = (CachedReflectionInfo.s_DynamicObject_TryGetMember = typeof(DynamicObject).GetMethod("TryGetMember"));
				}
				return result;
			}
		}

		// Token: 0x170001AE RID: 430
		// (get) Token: 0x06000BE2 RID: 3042 RVA: 0x0002618B File Offset: 0x0002438B
		public static MethodInfo DynamicObject_TrySetMember
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_DynamicObject_TrySetMember) == null)
				{
					result = (CachedReflectionInfo.s_DynamicObject_TrySetMember = typeof(DynamicObject).GetMethod("TrySetMember"));
				}
				return result;
			}
		}

		// Token: 0x170001AF RID: 431
		// (get) Token: 0x06000BE3 RID: 3043 RVA: 0x000261B0 File Offset: 0x000243B0
		public static MethodInfo DynamicObject_TryDeleteMember
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_DynamicObject_TryDeleteMember) == null)
				{
					result = (CachedReflectionInfo.s_DynamicObject_TryDeleteMember = typeof(DynamicObject).GetMethod("TryDeleteMember"));
				}
				return result;
			}
		}

		// Token: 0x170001B0 RID: 432
		// (get) Token: 0x06000BE4 RID: 3044 RVA: 0x000261D5 File Offset: 0x000243D5
		public static MethodInfo DynamicObject_TryGetIndex
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_DynamicObject_TryGetIndex) == null)
				{
					result = (CachedReflectionInfo.s_DynamicObject_TryGetIndex = typeof(DynamicObject).GetMethod("TryGetIndex"));
				}
				return result;
			}
		}

		// Token: 0x170001B1 RID: 433
		// (get) Token: 0x06000BE5 RID: 3045 RVA: 0x000261FA File Offset: 0x000243FA
		public static MethodInfo DynamicObject_TrySetIndex
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_DynamicObject_TrySetIndex) == null)
				{
					result = (CachedReflectionInfo.s_DynamicObject_TrySetIndex = typeof(DynamicObject).GetMethod("TrySetIndex"));
				}
				return result;
			}
		}

		// Token: 0x170001B2 RID: 434
		// (get) Token: 0x06000BE6 RID: 3046 RVA: 0x0002621F File Offset: 0x0002441F
		public static MethodInfo DynamicObject_TryDeleteIndex
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_DynamicObject_TryDeleteIndex) == null)
				{
					result = (CachedReflectionInfo.s_DynamicObject_TryDeleteIndex = typeof(DynamicObject).GetMethod("TryDeleteIndex"));
				}
				return result;
			}
		}

		// Token: 0x170001B3 RID: 435
		// (get) Token: 0x06000BE7 RID: 3047 RVA: 0x00026244 File Offset: 0x00024444
		public static MethodInfo DynamicObject_TryConvert
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_DynamicObject_TryConvert) == null)
				{
					result = (CachedReflectionInfo.s_DynamicObject_TryConvert = typeof(DynamicObject).GetMethod("TryConvert"));
				}
				return result;
			}
		}

		// Token: 0x170001B4 RID: 436
		// (get) Token: 0x06000BE8 RID: 3048 RVA: 0x00026269 File Offset: 0x00024469
		public static MethodInfo DynamicObject_TryInvoke
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_DynamicObject_TryInvoke) == null)
				{
					result = (CachedReflectionInfo.s_DynamicObject_TryInvoke = typeof(DynamicObject).GetMethod("TryInvoke"));
				}
				return result;
			}
		}

		// Token: 0x170001B5 RID: 437
		// (get) Token: 0x06000BE9 RID: 3049 RVA: 0x0002628E File Offset: 0x0002448E
		public static MethodInfo DynamicObject_TryInvokeMember
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_DynamicObject_TryInvokeMember) == null)
				{
					result = (CachedReflectionInfo.s_DynamicObject_TryInvokeMember = typeof(DynamicObject).GetMethod("TryInvokeMember"));
				}
				return result;
			}
		}

		// Token: 0x170001B6 RID: 438
		// (get) Token: 0x06000BEA RID: 3050 RVA: 0x000262B3 File Offset: 0x000244B3
		public static MethodInfo DynamicObject_TryBinaryOperation
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_DynamicObject_TryBinaryOperation) == null)
				{
					result = (CachedReflectionInfo.s_DynamicObject_TryBinaryOperation = typeof(DynamicObject).GetMethod("TryBinaryOperation"));
				}
				return result;
			}
		}

		// Token: 0x170001B7 RID: 439
		// (get) Token: 0x06000BEB RID: 3051 RVA: 0x000262D8 File Offset: 0x000244D8
		public static MethodInfo DynamicObject_TryUnaryOperation
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_DynamicObject_TryUnaryOperation) == null)
				{
					result = (CachedReflectionInfo.s_DynamicObject_TryUnaryOperation = typeof(DynamicObject).GetMethod("TryUnaryOperation"));
				}
				return result;
			}
		}

		// Token: 0x170001B8 RID: 440
		// (get) Token: 0x06000BEC RID: 3052 RVA: 0x000262FD File Offset: 0x000244FD
		public static MethodInfo DynamicObject_TryCreateInstance
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_DynamicObject_TryCreateInstance) == null)
				{
					result = (CachedReflectionInfo.s_DynamicObject_TryCreateInstance = typeof(DynamicObject).GetMethod("TryCreateInstance"));
				}
				return result;
			}
		}

		// Token: 0x170001B9 RID: 441
		// (get) Token: 0x06000BED RID: 3053 RVA: 0x00026322 File Offset: 0x00024522
		public static ConstructorInfo Nullable_Boolean_Ctor
		{
			get
			{
				ConstructorInfo result;
				if ((result = CachedReflectionInfo.s_Nullable_Boolean_Ctor) == null)
				{
					result = (CachedReflectionInfo.s_Nullable_Boolean_Ctor = typeof(bool?).GetConstructor(new Type[]
					{
						typeof(bool)
					}));
				}
				return result;
			}
		}

		// Token: 0x170001BA RID: 442
		// (get) Token: 0x06000BEE RID: 3054 RVA: 0x00026355 File Offset: 0x00024555
		public static ConstructorInfo Decimal_Ctor_Int32
		{
			get
			{
				ConstructorInfo result;
				if ((result = CachedReflectionInfo.s_Decimal_Ctor_Int32) == null)
				{
					result = (CachedReflectionInfo.s_Decimal_Ctor_Int32 = typeof(decimal).GetConstructor(new Type[]
					{
						typeof(int)
					}));
				}
				return result;
			}
		}

		// Token: 0x170001BB RID: 443
		// (get) Token: 0x06000BEF RID: 3055 RVA: 0x00026388 File Offset: 0x00024588
		public static ConstructorInfo Decimal_Ctor_UInt32
		{
			get
			{
				ConstructorInfo result;
				if ((result = CachedReflectionInfo.s_Decimal_Ctor_UInt32) == null)
				{
					result = (CachedReflectionInfo.s_Decimal_Ctor_UInt32 = typeof(decimal).GetConstructor(new Type[]
					{
						typeof(uint)
					}));
				}
				return result;
			}
		}

		// Token: 0x170001BC RID: 444
		// (get) Token: 0x06000BF0 RID: 3056 RVA: 0x000263BB File Offset: 0x000245BB
		public static ConstructorInfo Decimal_Ctor_Int64
		{
			get
			{
				ConstructorInfo result;
				if ((result = CachedReflectionInfo.s_Decimal_Ctor_Int64) == null)
				{
					result = (CachedReflectionInfo.s_Decimal_Ctor_Int64 = typeof(decimal).GetConstructor(new Type[]
					{
						typeof(long)
					}));
				}
				return result;
			}
		}

		// Token: 0x170001BD RID: 445
		// (get) Token: 0x06000BF1 RID: 3057 RVA: 0x000263EE File Offset: 0x000245EE
		public static ConstructorInfo Decimal_Ctor_UInt64
		{
			get
			{
				ConstructorInfo result;
				if ((result = CachedReflectionInfo.s_Decimal_Ctor_UInt64) == null)
				{
					result = (CachedReflectionInfo.s_Decimal_Ctor_UInt64 = typeof(decimal).GetConstructor(new Type[]
					{
						typeof(ulong)
					}));
				}
				return result;
			}
		}

		// Token: 0x170001BE RID: 446
		// (get) Token: 0x06000BF2 RID: 3058 RVA: 0x00026424 File Offset: 0x00024624
		public static ConstructorInfo Decimal_Ctor_Int32_Int32_Int32_Bool_Byte
		{
			get
			{
				ConstructorInfo result;
				if ((result = CachedReflectionInfo.s_Decimal_Ctor_Int32_Int32_Int32_Bool_Byte) == null)
				{
					result = (CachedReflectionInfo.s_Decimal_Ctor_Int32_Int32_Int32_Bool_Byte = typeof(decimal).GetConstructor(new Type[]
					{
						typeof(int),
						typeof(int),
						typeof(int),
						typeof(bool),
						typeof(byte)
					}));
				}
				return result;
			}
		}

		// Token: 0x170001BF RID: 447
		// (get) Token: 0x06000BF3 RID: 3059 RVA: 0x00026496 File Offset: 0x00024696
		public static FieldInfo Decimal_One
		{
			get
			{
				FieldInfo result;
				if ((result = CachedReflectionInfo.s_Decimal_One) == null)
				{
					result = (CachedReflectionInfo.s_Decimal_One = typeof(decimal).GetField("One"));
				}
				return result;
			}
		}

		// Token: 0x170001C0 RID: 448
		// (get) Token: 0x06000BF4 RID: 3060 RVA: 0x000264BB File Offset: 0x000246BB
		public static FieldInfo Decimal_MinusOne
		{
			get
			{
				FieldInfo result;
				if ((result = CachedReflectionInfo.s_Decimal_MinusOne) == null)
				{
					result = (CachedReflectionInfo.s_Decimal_MinusOne = typeof(decimal).GetField("MinusOne"));
				}
				return result;
			}
		}

		// Token: 0x170001C1 RID: 449
		// (get) Token: 0x06000BF5 RID: 3061 RVA: 0x000264E0 File Offset: 0x000246E0
		public static FieldInfo Decimal_MinValue
		{
			get
			{
				FieldInfo result;
				if ((result = CachedReflectionInfo.s_Decimal_MinValue) == null)
				{
					result = (CachedReflectionInfo.s_Decimal_MinValue = typeof(decimal).GetField("MinValue"));
				}
				return result;
			}
		}

		// Token: 0x170001C2 RID: 450
		// (get) Token: 0x06000BF6 RID: 3062 RVA: 0x00026505 File Offset: 0x00024705
		public static FieldInfo Decimal_MaxValue
		{
			get
			{
				FieldInfo result;
				if ((result = CachedReflectionInfo.s_Decimal_MaxValue) == null)
				{
					result = (CachedReflectionInfo.s_Decimal_MaxValue = typeof(decimal).GetField("MaxValue"));
				}
				return result;
			}
		}

		// Token: 0x170001C3 RID: 451
		// (get) Token: 0x06000BF7 RID: 3063 RVA: 0x0002652A File Offset: 0x0002472A
		public static FieldInfo Decimal_Zero
		{
			get
			{
				FieldInfo result;
				if ((result = CachedReflectionInfo.s_Decimal_Zero) == null)
				{
					result = (CachedReflectionInfo.s_Decimal_Zero = typeof(decimal).GetField("Zero"));
				}
				return result;
			}
		}

		// Token: 0x170001C4 RID: 452
		// (get) Token: 0x06000BF8 RID: 3064 RVA: 0x0002654F File Offset: 0x0002474F
		public static FieldInfo DateTime_MinValue
		{
			get
			{
				FieldInfo result;
				if ((result = CachedReflectionInfo.s_DateTime_MinValue) == null)
				{
					result = (CachedReflectionInfo.s_DateTime_MinValue = typeof(DateTime).GetField("MinValue"));
				}
				return result;
			}
		}

		// Token: 0x170001C5 RID: 453
		// (get) Token: 0x06000BF9 RID: 3065 RVA: 0x00026574 File Offset: 0x00024774
		public static MethodInfo MethodBase_GetMethodFromHandle_RuntimeMethodHandle
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle) == null)
				{
					result = (CachedReflectionInfo.s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle = typeof(MethodBase).GetMethod("GetMethodFromHandle", new Type[]
					{
						typeof(RuntimeMethodHandle)
					}));
				}
				return result;
			}
		}

		// Token: 0x170001C6 RID: 454
		// (get) Token: 0x06000BFA RID: 3066 RVA: 0x000265AC File Offset: 0x000247AC
		public static MethodInfo MethodBase_GetMethodFromHandle_RuntimeMethodHandle_RuntimeTypeHandle
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_RuntimeTypeHandle) == null)
				{
					result = (CachedReflectionInfo.s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_RuntimeTypeHandle = typeof(MethodBase).GetMethod("GetMethodFromHandle", new Type[]
					{
						typeof(RuntimeMethodHandle),
						typeof(RuntimeTypeHandle)
					}));
				}
				return result;
			}
		}

		// Token: 0x170001C7 RID: 455
		// (get) Token: 0x06000BFB RID: 3067 RVA: 0x000265FC File Offset: 0x000247FC
		public static MethodInfo MethodInfo_CreateDelegate_Type_Object
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_MethodInfo_CreateDelegate_Type_Object) == null)
				{
					result = (CachedReflectionInfo.s_MethodInfo_CreateDelegate_Type_Object = typeof(MethodInfo).GetMethod("CreateDelegate", new Type[]
					{
						typeof(Type),
						typeof(object)
					}));
				}
				return result;
			}
		}

		// Token: 0x170001C8 RID: 456
		// (get) Token: 0x06000BFC RID: 3068 RVA: 0x0002664C File Offset: 0x0002484C
		public static MethodInfo String_op_Equality_String_String
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_String_op_Equality_String_String) == null)
				{
					result = (CachedReflectionInfo.s_String_op_Equality_String_String = typeof(string).GetMethod("op_Equality", new Type[]
					{
						typeof(string),
						typeof(string)
					}));
				}
				return result;
			}
		}

		// Token: 0x170001C9 RID: 457
		// (get) Token: 0x06000BFD RID: 3069 RVA: 0x0002669C File Offset: 0x0002489C
		public static MethodInfo String_Equals_String_String
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_String_Equals_String_String) == null)
				{
					result = (CachedReflectionInfo.s_String_Equals_String_String = typeof(string).GetMethod("Equals", new Type[]
					{
						typeof(string),
						typeof(string)
					}));
				}
				return result;
			}
		}

		// Token: 0x170001CA RID: 458
		// (get) Token: 0x06000BFE RID: 3070 RVA: 0x000266EC File Offset: 0x000248EC
		public static MethodInfo DictionaryOfStringInt32_Add_String_Int32
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_DictionaryOfStringInt32_Add_String_Int32) == null)
				{
					result = (CachedReflectionInfo.s_DictionaryOfStringInt32_Add_String_Int32 = typeof(Dictionary<string, int>).GetMethod("Add", new Type[]
					{
						typeof(string),
						typeof(int)
					}));
				}
				return result;
			}
		}

		// Token: 0x170001CB RID: 459
		// (get) Token: 0x06000BFF RID: 3071 RVA: 0x0002673C File Offset: 0x0002493C
		public static ConstructorInfo DictionaryOfStringInt32_Ctor_Int32
		{
			get
			{
				ConstructorInfo result;
				if ((result = CachedReflectionInfo.s_DictionaryOfStringInt32_Ctor_Int32) == null)
				{
					result = (CachedReflectionInfo.s_DictionaryOfStringInt32_Ctor_Int32 = typeof(Dictionary<string, int>).GetConstructor(new Type[]
					{
						typeof(int)
					}));
				}
				return result;
			}
		}

		// Token: 0x170001CC RID: 460
		// (get) Token: 0x06000C00 RID: 3072 RVA: 0x0002676F File Offset: 0x0002496F
		public static MethodInfo Type_GetTypeFromHandle
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Type_GetTypeFromHandle) == null)
				{
					result = (CachedReflectionInfo.s_Type_GetTypeFromHandle = typeof(Type).GetMethod("GetTypeFromHandle"));
				}
				return result;
			}
		}

		// Token: 0x170001CD RID: 461
		// (get) Token: 0x06000C01 RID: 3073 RVA: 0x00026794 File Offset: 0x00024994
		public static MethodInfo Object_GetType
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Object_GetType) == null)
				{
					result = (CachedReflectionInfo.s_Object_GetType = typeof(object).GetMethod("GetType"));
				}
				return result;
			}
		}

		// Token: 0x170001CE RID: 462
		// (get) Token: 0x06000C02 RID: 3074 RVA: 0x000267B9 File Offset: 0x000249B9
		public static MethodInfo Decimal_op_Implicit_Byte
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Decimal_op_Implicit_Byte) == null)
				{
					result = (CachedReflectionInfo.s_Decimal_op_Implicit_Byte = typeof(decimal).GetMethod("op_Implicit", new Type[]
					{
						typeof(byte)
					}));
				}
				return result;
			}
		}

		// Token: 0x170001CF RID: 463
		// (get) Token: 0x06000C03 RID: 3075 RVA: 0x000267F1 File Offset: 0x000249F1
		public static MethodInfo Decimal_op_Implicit_SByte
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Decimal_op_Implicit_SByte) == null)
				{
					result = (CachedReflectionInfo.s_Decimal_op_Implicit_SByte = typeof(decimal).GetMethod("op_Implicit", new Type[]
					{
						typeof(sbyte)
					}));
				}
				return result;
			}
		}

		// Token: 0x170001D0 RID: 464
		// (get) Token: 0x06000C04 RID: 3076 RVA: 0x00026829 File Offset: 0x00024A29
		public static MethodInfo Decimal_op_Implicit_Int16
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Decimal_op_Implicit_Int16) == null)
				{
					result = (CachedReflectionInfo.s_Decimal_op_Implicit_Int16 = typeof(decimal).GetMethod("op_Implicit", new Type[]
					{
						typeof(short)
					}));
				}
				return result;
			}
		}

		// Token: 0x170001D1 RID: 465
		// (get) Token: 0x06000C05 RID: 3077 RVA: 0x00026861 File Offset: 0x00024A61
		public static MethodInfo Decimal_op_Implicit_UInt16
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Decimal_op_Implicit_UInt16) == null)
				{
					result = (CachedReflectionInfo.s_Decimal_op_Implicit_UInt16 = typeof(decimal).GetMethod("op_Implicit", new Type[]
					{
						typeof(ushort)
					}));
				}
				return result;
			}
		}

		// Token: 0x170001D2 RID: 466
		// (get) Token: 0x06000C06 RID: 3078 RVA: 0x00026899 File Offset: 0x00024A99
		public static MethodInfo Decimal_op_Implicit_Int32
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Decimal_op_Implicit_Int32) == null)
				{
					result = (CachedReflectionInfo.s_Decimal_op_Implicit_Int32 = typeof(decimal).GetMethod("op_Implicit", new Type[]
					{
						typeof(int)
					}));
				}
				return result;
			}
		}

		// Token: 0x170001D3 RID: 467
		// (get) Token: 0x06000C07 RID: 3079 RVA: 0x000268D1 File Offset: 0x00024AD1
		public static MethodInfo Decimal_op_Implicit_UInt32
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Decimal_op_Implicit_UInt32) == null)
				{
					result = (CachedReflectionInfo.s_Decimal_op_Implicit_UInt32 = typeof(decimal).GetMethod("op_Implicit", new Type[]
					{
						typeof(uint)
					}));
				}
				return result;
			}
		}

		// Token: 0x170001D4 RID: 468
		// (get) Token: 0x06000C08 RID: 3080 RVA: 0x00026909 File Offset: 0x00024B09
		public static MethodInfo Decimal_op_Implicit_Int64
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Decimal_op_Implicit_Int64) == null)
				{
					result = (CachedReflectionInfo.s_Decimal_op_Implicit_Int64 = typeof(decimal).GetMethod("op_Implicit", new Type[]
					{
						typeof(long)
					}));
				}
				return result;
			}
		}

		// Token: 0x170001D5 RID: 469
		// (get) Token: 0x06000C09 RID: 3081 RVA: 0x00026941 File Offset: 0x00024B41
		public static MethodInfo Decimal_op_Implicit_UInt64
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Decimal_op_Implicit_UInt64) == null)
				{
					result = (CachedReflectionInfo.s_Decimal_op_Implicit_UInt64 = typeof(decimal).GetMethod("op_Implicit", new Type[]
					{
						typeof(ulong)
					}));
				}
				return result;
			}
		}

		// Token: 0x170001D6 RID: 470
		// (get) Token: 0x06000C0A RID: 3082 RVA: 0x00026979 File Offset: 0x00024B79
		public static MethodInfo Decimal_op_Implicit_Char
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Decimal_op_Implicit_Char) == null)
				{
					result = (CachedReflectionInfo.s_Decimal_op_Implicit_Char = typeof(decimal).GetMethod("op_Implicit", new Type[]
					{
						typeof(char)
					}));
				}
				return result;
			}
		}

		// Token: 0x170001D7 RID: 471
		// (get) Token: 0x06000C0B RID: 3083 RVA: 0x000269B4 File Offset: 0x00024BB4
		public static MethodInfo Math_Pow_Double_Double
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Math_Pow_Double_Double) == null)
				{
					result = (CachedReflectionInfo.s_Math_Pow_Double_Double = typeof(Math).GetMethod("Pow", new Type[]
					{
						typeof(double),
						typeof(double)
					}));
				}
				return result;
			}
		}

		// Token: 0x0400081B RID: 2075
		private static MethodInfo s_String_Format_String_ObjectArray;

		// Token: 0x0400081C RID: 2076
		private static ConstructorInfo s_InvalidCastException_Ctor_String;

		// Token: 0x0400081D RID: 2077
		private static MethodInfo s_CallSiteOps_SetNotMatched;

		// Token: 0x0400081E RID: 2078
		private static MethodInfo s_CallSiteOps_CreateMatchmaker;

		// Token: 0x0400081F RID: 2079
		private static MethodInfo s_CallSiteOps_GetMatch;

		// Token: 0x04000820 RID: 2080
		private static MethodInfo s_CallSiteOps_ClearMatch;

		// Token: 0x04000821 RID: 2081
		private static MethodInfo s_CallSiteOps_UpdateRules;

		// Token: 0x04000822 RID: 2082
		private static MethodInfo s_CallSiteOps_GetRules;

		// Token: 0x04000823 RID: 2083
		private static MethodInfo s_CallSiteOps_GetRuleCache;

		// Token: 0x04000824 RID: 2084
		private static MethodInfo s_CallSiteOps_GetCachedRules;

		// Token: 0x04000825 RID: 2085
		private static MethodInfo s_CallSiteOps_AddRule;

		// Token: 0x04000826 RID: 2086
		private static MethodInfo s_CallSiteOps_MoveRule;

		// Token: 0x04000827 RID: 2087
		private static MethodInfo s_CallSiteOps_Bind;

		// Token: 0x04000828 RID: 2088
		private static MethodInfo s_DynamicObject_TryGetMember;

		// Token: 0x04000829 RID: 2089
		private static MethodInfo s_DynamicObject_TrySetMember;

		// Token: 0x0400082A RID: 2090
		private static MethodInfo s_DynamicObject_TryDeleteMember;

		// Token: 0x0400082B RID: 2091
		private static MethodInfo s_DynamicObject_TryGetIndex;

		// Token: 0x0400082C RID: 2092
		private static MethodInfo s_DynamicObject_TrySetIndex;

		// Token: 0x0400082D RID: 2093
		private static MethodInfo s_DynamicObject_TryDeleteIndex;

		// Token: 0x0400082E RID: 2094
		private static MethodInfo s_DynamicObject_TryConvert;

		// Token: 0x0400082F RID: 2095
		private static MethodInfo s_DynamicObject_TryInvoke;

		// Token: 0x04000830 RID: 2096
		private static MethodInfo s_DynamicObject_TryInvokeMember;

		// Token: 0x04000831 RID: 2097
		private static MethodInfo s_DynamicObject_TryBinaryOperation;

		// Token: 0x04000832 RID: 2098
		private static MethodInfo s_DynamicObject_TryUnaryOperation;

		// Token: 0x04000833 RID: 2099
		private static MethodInfo s_DynamicObject_TryCreateInstance;

		// Token: 0x04000834 RID: 2100
		private static ConstructorInfo s_Nullable_Boolean_Ctor;

		// Token: 0x04000835 RID: 2101
		private static ConstructorInfo s_Decimal_Ctor_Int32;

		// Token: 0x04000836 RID: 2102
		private static ConstructorInfo s_Decimal_Ctor_UInt32;

		// Token: 0x04000837 RID: 2103
		private static ConstructorInfo s_Decimal_Ctor_Int64;

		// Token: 0x04000838 RID: 2104
		private static ConstructorInfo s_Decimal_Ctor_UInt64;

		// Token: 0x04000839 RID: 2105
		private static ConstructorInfo s_Decimal_Ctor_Int32_Int32_Int32_Bool_Byte;

		// Token: 0x0400083A RID: 2106
		private static FieldInfo s_Decimal_One;

		// Token: 0x0400083B RID: 2107
		private static FieldInfo s_Decimal_MinusOne;

		// Token: 0x0400083C RID: 2108
		private static FieldInfo s_Decimal_MinValue;

		// Token: 0x0400083D RID: 2109
		private static FieldInfo s_Decimal_MaxValue;

		// Token: 0x0400083E RID: 2110
		private static FieldInfo s_Decimal_Zero;

		// Token: 0x0400083F RID: 2111
		private static FieldInfo s_DateTime_MinValue;

		// Token: 0x04000840 RID: 2112
		private static MethodInfo s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle;

		// Token: 0x04000841 RID: 2113
		private static MethodInfo s_MethodBase_GetMethodFromHandle_RuntimeMethodHandle_RuntimeTypeHandle;

		// Token: 0x04000842 RID: 2114
		private static MethodInfo s_MethodInfo_CreateDelegate_Type_Object;

		// Token: 0x04000843 RID: 2115
		private static MethodInfo s_String_op_Equality_String_String;

		// Token: 0x04000844 RID: 2116
		private static MethodInfo s_String_Equals_String_String;

		// Token: 0x04000845 RID: 2117
		private static MethodInfo s_DictionaryOfStringInt32_Add_String_Int32;

		// Token: 0x04000846 RID: 2118
		private static ConstructorInfo s_DictionaryOfStringInt32_Ctor_Int32;

		// Token: 0x04000847 RID: 2119
		private static MethodInfo s_Type_GetTypeFromHandle;

		// Token: 0x04000848 RID: 2120
		private static MethodInfo s_Object_GetType;

		// Token: 0x04000849 RID: 2121
		private static MethodInfo s_Decimal_op_Implicit_Byte;

		// Token: 0x0400084A RID: 2122
		private static MethodInfo s_Decimal_op_Implicit_SByte;

		// Token: 0x0400084B RID: 2123
		private static MethodInfo s_Decimal_op_Implicit_Int16;

		// Token: 0x0400084C RID: 2124
		private static MethodInfo s_Decimal_op_Implicit_UInt16;

		// Token: 0x0400084D RID: 2125
		private static MethodInfo s_Decimal_op_Implicit_Int32;

		// Token: 0x0400084E RID: 2126
		private static MethodInfo s_Decimal_op_Implicit_UInt32;

		// Token: 0x0400084F RID: 2127
		private static MethodInfo s_Decimal_op_Implicit_Int64;

		// Token: 0x04000850 RID: 2128
		private static MethodInfo s_Decimal_op_Implicit_UInt64;

		// Token: 0x04000851 RID: 2129
		private static MethodInfo s_Decimal_op_Implicit_Char;

		// Token: 0x04000852 RID: 2130
		private static MethodInfo s_Math_Pow_Double_Double;
	}
}
