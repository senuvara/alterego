﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x02000236 RID: 566
	internal sealed class TypedDynamicExpression2 : DynamicExpression2
	{
		// Token: 0x06000FAF RID: 4015 RVA: 0x00031837 File Offset: 0x0002FA37
		internal TypedDynamicExpression2(Type retType, Type delegateType, CallSiteBinder binder, Expression arg0, Expression arg1) : base(delegateType, binder, arg0, arg1)
		{
			this.Type = retType;
		}

		// Token: 0x170002DE RID: 734
		// (get) Token: 0x06000FB0 RID: 4016 RVA: 0x0003184C File Offset: 0x0002FA4C
		public sealed override Type Type
		{
			[CompilerGenerated]
			get
			{
				return this.<Type>k__BackingField;
			}
		}

		// Token: 0x040008C7 RID: 2247
		[CompilerGenerated]
		private readonly Type <Type>k__BackingField;
	}
}
