﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;
using System.Reflection;

namespace System.Linq.Expressions
{
	// Token: 0x0200026B RID: 619
	internal sealed class MethodCallExpression1 : MethodCallExpression, IArgumentProvider
	{
		// Token: 0x060011FA RID: 4602 RVA: 0x000359F6 File Offset: 0x00033BF6
		public MethodCallExpression1(MethodInfo method, Expression arg0) : base(method)
		{
			this._arg0 = arg0;
		}

		// Token: 0x060011FB RID: 4603 RVA: 0x00035A06 File Offset: 0x00033C06
		public override Expression GetArgument(int index)
		{
			if (index == 0)
			{
				return ExpressionUtils.ReturnObject<Expression>(this._arg0);
			}
			throw new ArgumentOutOfRangeException("index");
		}

		// Token: 0x1700033F RID: 831
		// (get) Token: 0x060011FC RID: 4604 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ArgumentCount
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x060011FD RID: 4605 RVA: 0x00035A21 File Offset: 0x00033C21
		internal override ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			return ExpressionUtils.ReturnReadOnly(this, ref this._arg0);
		}

		// Token: 0x060011FE RID: 4606 RVA: 0x00035A30 File Offset: 0x00033C30
		internal override bool SameArguments(ICollection<Expression> arguments)
		{
			if (arguments != null && arguments.Count == 1)
			{
				using (IEnumerator<Expression> enumerator = arguments.GetEnumerator())
				{
					enumerator.MoveNext();
					return enumerator.Current == ExpressionUtils.ReturnObject<Expression>(this._arg0);
				}
				return false;
			}
			return false;
		}

		// Token: 0x060011FF RID: 4607 RVA: 0x00035A8C File Offset: 0x00033C8C
		internal override MethodCallExpression Rewrite(Expression instance, IReadOnlyList<Expression> args)
		{
			if (args != null)
			{
				return Expression.Call(base.Method, args[0]);
			}
			return Expression.Call(base.Method, ExpressionUtils.ReturnObject<Expression>(this._arg0));
		}

		// Token: 0x0400096E RID: 2414
		private object _arg0;
	}
}
