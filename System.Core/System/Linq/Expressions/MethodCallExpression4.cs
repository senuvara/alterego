﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;
using System.Reflection;

namespace System.Linq.Expressions
{
	// Token: 0x0200026E RID: 622
	internal sealed class MethodCallExpression4 : MethodCallExpression, IArgumentProvider
	{
		// Token: 0x0600120C RID: 4620 RVA: 0x00035D1F File Offset: 0x00033F1F
		public MethodCallExpression4(MethodInfo method, Expression arg0, Expression arg1, Expression arg2, Expression arg3) : base(method)
		{
			this._arg0 = arg0;
			this._arg1 = arg1;
			this._arg2 = arg2;
			this._arg3 = arg3;
		}

		// Token: 0x0600120D RID: 4621 RVA: 0x00035D48 File Offset: 0x00033F48
		public override Expression GetArgument(int index)
		{
			switch (index)
			{
			case 0:
				return ExpressionUtils.ReturnObject<Expression>(this._arg0);
			case 1:
				return this._arg1;
			case 2:
				return this._arg2;
			case 3:
				return this._arg3;
			default:
				throw new ArgumentOutOfRangeException("index");
			}
		}

		// Token: 0x17000342 RID: 834
		// (get) Token: 0x0600120E RID: 4622 RVA: 0x0002F078 File Offset: 0x0002D278
		public override int ArgumentCount
		{
			get
			{
				return 4;
			}
		}

		// Token: 0x0600120F RID: 4623 RVA: 0x00035D98 File Offset: 0x00033F98
		internal override bool SameArguments(ICollection<Expression> arguments)
		{
			if (arguments != null && arguments.Count == 4)
			{
				ReadOnlyCollection<Expression> readOnlyCollection = this._arg0 as ReadOnlyCollection<Expression>;
				if (readOnlyCollection != null)
				{
					return ExpressionUtils.SameElements<Expression>(arguments, readOnlyCollection);
				}
				using (IEnumerator<Expression> enumerator = arguments.GetEnumerator())
				{
					enumerator.MoveNext();
					if (enumerator.Current == this._arg0)
					{
						enumerator.MoveNext();
						if (enumerator.Current == this._arg1)
						{
							enumerator.MoveNext();
							if (enumerator.Current == this._arg2)
							{
								enumerator.MoveNext();
								return enumerator.Current == this._arg3;
							}
						}
					}
				}
				return false;
			}
			return false;
		}

		// Token: 0x06001210 RID: 4624 RVA: 0x00035E4C File Offset: 0x0003404C
		internal override ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			return ExpressionUtils.ReturnReadOnly(this, ref this._arg0);
		}

		// Token: 0x06001211 RID: 4625 RVA: 0x00035E5C File Offset: 0x0003405C
		internal override MethodCallExpression Rewrite(Expression instance, IReadOnlyList<Expression> args)
		{
			if (args != null)
			{
				return Expression.Call(base.Method, args[0], args[1], args[2], args[3]);
			}
			return Expression.Call(base.Method, ExpressionUtils.ReturnObject<Expression>(this._arg0), this._arg1, this._arg2, this._arg3);
		}

		// Token: 0x04000974 RID: 2420
		private object _arg0;

		// Token: 0x04000975 RID: 2421
		private readonly Expression _arg1;

		// Token: 0x04000976 RID: 2422
		private readonly Expression _arg2;

		// Token: 0x04000977 RID: 2423
		private readonly Expression _arg3;
	}
}
