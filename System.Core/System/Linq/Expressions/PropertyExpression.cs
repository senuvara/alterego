﻿using System;
using System.Reflection;

namespace System.Linq.Expressions
{
	// Token: 0x02000262 RID: 610
	internal sealed class PropertyExpression : MemberExpression
	{
		// Token: 0x060011BD RID: 4541 RVA: 0x000355E8 File Offset: 0x000337E8
		public PropertyExpression(Expression expression, PropertyInfo member) : base(expression)
		{
			this._property = member;
		}

		// Token: 0x060011BE RID: 4542 RVA: 0x000355F8 File Offset: 0x000337F8
		internal override MemberInfo GetMember()
		{
			return this._property;
		}

		// Token: 0x1700032E RID: 814
		// (get) Token: 0x060011BF RID: 4543 RVA: 0x00035600 File Offset: 0x00033800
		public sealed override Type Type
		{
			get
			{
				return this._property.PropertyType;
			}
		}

		// Token: 0x04000965 RID: 2405
		private readonly PropertyInfo _property;
	}
}
