﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;
using System.Reflection;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Represents initializing members of a member of a newly created object.</summary>
	// Token: 0x02000265 RID: 613
	public sealed class MemberMemberBinding : MemberBinding
	{
		// Token: 0x060011D2 RID: 4562 RVA: 0x00035833 File Offset: 0x00033A33
		internal MemberMemberBinding(MemberInfo member, ReadOnlyCollection<MemberBinding> bindings) : base(MemberBindingType.MemberBinding, member)
		{
			this.Bindings = bindings;
		}

		/// <summary>Gets the bindings that describe how to initialize the members of a member.</summary>
		/// <returns>A <see cref="T:System.Collections.ObjectModel.ReadOnlyCollection`1" /> of <see cref="T:System.Linq.Expressions.MemberBinding" /> objects that describe how to initialize the members of the member.</returns>
		// Token: 0x17000335 RID: 821
		// (get) Token: 0x060011D3 RID: 4563 RVA: 0x00035844 File Offset: 0x00033A44
		public ReadOnlyCollection<MemberBinding> Bindings
		{
			[CompilerGenerated]
			get
			{
				return this.<Bindings>k__BackingField;
			}
		}

		/// <summary>Creates a new expression that is like this one, but using the supplied children. If all of the children are the same, it will return this expression.</summary>
		/// <param name="bindings">The <see cref="P:System.Linq.Expressions.MemberMemberBinding.Bindings" /> property of the result.</param>
		/// <returns>This expression if no children are changed or an expression with the updated children.</returns>
		// Token: 0x060011D4 RID: 4564 RVA: 0x0003584C File Offset: 0x00033A4C
		public MemberMemberBinding Update(IEnumerable<MemberBinding> bindings)
		{
			if (bindings != null && ExpressionUtils.SameElements<MemberBinding>(ref bindings, this.Bindings))
			{
				return this;
			}
			return Expression.MemberBind(base.Member, bindings);
		}

		// Token: 0x060011D5 RID: 4565 RVA: 0x000039E8 File Offset: 0x00001BE8
		internal override void ValidateAsDefinedHere(int index)
		{
		}

		// Token: 0x060011D6 RID: 4566 RVA: 0x0000220F File Offset: 0x0000040F
		internal MemberMemberBinding()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000969 RID: 2409
		[CompilerGenerated]
		private readonly ReadOnlyCollection<MemberBinding> <Bindings>k__BackingField;
	}
}
