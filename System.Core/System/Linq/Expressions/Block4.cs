﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;

namespace System.Linq.Expressions
{
	// Token: 0x02000218 RID: 536
	internal sealed class Block4 : BlockExpression
	{
		// Token: 0x06000EAE RID: 3758 RVA: 0x0002EF53 File Offset: 0x0002D153
		internal Block4(Expression arg0, Expression arg1, Expression arg2, Expression arg3)
		{
			this._arg0 = arg0;
			this._arg1 = arg1;
			this._arg2 = arg2;
			this._arg3 = arg3;
		}

		// Token: 0x06000EAF RID: 3759 RVA: 0x0002EF78 File Offset: 0x0002D178
		internal override bool SameExpressions(ICollection<Expression> expressions)
		{
			if (expressions.Count == 4)
			{
				ReadOnlyCollection<Expression> readOnlyCollection = this._arg0 as ReadOnlyCollection<Expression>;
				if (readOnlyCollection != null)
				{
					return ExpressionUtils.SameElements<Expression>(expressions, readOnlyCollection);
				}
				using (IEnumerator<Expression> enumerator = expressions.GetEnumerator())
				{
					enumerator.MoveNext();
					if (enumerator.Current == this._arg0)
					{
						enumerator.MoveNext();
						if (enumerator.Current == this._arg1)
						{
							enumerator.MoveNext();
							if (enumerator.Current == this._arg2)
							{
								enumerator.MoveNext();
								return enumerator.Current == this._arg3;
							}
						}
					}
				}
				return false;
			}
			return false;
		}

		// Token: 0x06000EB0 RID: 3760 RVA: 0x0002F028 File Offset: 0x0002D228
		internal override Expression GetExpression(int index)
		{
			switch (index)
			{
			case 0:
				return ExpressionUtils.ReturnObject<Expression>(this._arg0);
			case 1:
				return this._arg1;
			case 2:
				return this._arg2;
			case 3:
				return this._arg3;
			default:
				throw Error.ArgumentOutOfRange("index");
			}
		}

		// Token: 0x170002A0 RID: 672
		// (get) Token: 0x06000EB1 RID: 3761 RVA: 0x0002F078 File Offset: 0x0002D278
		internal override int ExpressionCount
		{
			get
			{
				return 4;
			}
		}

		// Token: 0x06000EB2 RID: 3762 RVA: 0x0002F07B File Offset: 0x0002D27B
		internal override ReadOnlyCollection<Expression> GetOrMakeExpressions()
		{
			return BlockExpression.ReturnReadOnlyExpressions(this, ref this._arg0);
		}

		// Token: 0x06000EB3 RID: 3763 RVA: 0x0002F089 File Offset: 0x0002D289
		internal override BlockExpression Rewrite(ReadOnlyCollection<ParameterExpression> variables, Expression[] args)
		{
			return new Block4(args[0], args[1], args[2], args[3]);
		}

		// Token: 0x04000886 RID: 2182
		private object _arg0;

		// Token: 0x04000887 RID: 2183
		private readonly Expression _arg1;

		// Token: 0x04000888 RID: 2184
		private readonly Expression _arg2;

		// Token: 0x04000889 RID: 2185
		private readonly Expression _arg3;
	}
}
