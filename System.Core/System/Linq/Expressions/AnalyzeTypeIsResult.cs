﻿using System;

namespace System.Linq.Expressions
{
	// Token: 0x02000223 RID: 547
	internal enum AnalyzeTypeIsResult
	{
		// Token: 0x0400089F RID: 2207
		KnownFalse,
		// Token: 0x040008A0 RID: 2208
		KnownTrue,
		// Token: 0x040008A1 RID: 2209
		KnownAssignable,
		// Token: 0x040008A2 RID: 2210
		Unknown
	}
}
