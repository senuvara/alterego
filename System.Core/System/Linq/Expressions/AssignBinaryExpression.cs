﻿using System;

namespace System.Linq.Expressions
{
	// Token: 0x020001F1 RID: 497
	internal class AssignBinaryExpression : BinaryExpression
	{
		// Token: 0x06000C26 RID: 3110 RVA: 0x00027159 File Offset: 0x00025359
		internal AssignBinaryExpression(Expression left, Expression right) : base(left, right)
		{
		}

		// Token: 0x06000C27 RID: 3111 RVA: 0x00027163 File Offset: 0x00025363
		public static AssignBinaryExpression Make(Expression left, Expression right, bool byRef)
		{
			if (byRef)
			{
				return new ByRefAssignBinaryExpression(left, right);
			}
			return new AssignBinaryExpression(left, right);
		}

		// Token: 0x170001E3 RID: 483
		// (get) Token: 0x06000C28 RID: 3112 RVA: 0x00002285 File Offset: 0x00000485
		internal virtual bool IsByRef
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170001E4 RID: 484
		// (get) Token: 0x06000C29 RID: 3113 RVA: 0x00027177 File Offset: 0x00025377
		public sealed override Type Type
		{
			get
			{
				return base.Left.Type;
			}
		}

		// Token: 0x170001E5 RID: 485
		// (get) Token: 0x06000C2A RID: 3114 RVA: 0x00027184 File Offset: 0x00025384
		public sealed override ExpressionType NodeType
		{
			get
			{
				return ExpressionType.Assign;
			}
		}
	}
}
