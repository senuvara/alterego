﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x02000235 RID: 565
	internal class DynamicExpression2 : DynamicExpression, IArgumentProvider
	{
		// Token: 0x06000FA9 RID: 4009 RVA: 0x0003174B File Offset: 0x0002F94B
		internal DynamicExpression2(Type delegateType, CallSiteBinder binder, Expression arg0, Expression arg1) : base(delegateType, binder)
		{
			this._arg0 = arg0;
			this._arg1 = arg1;
		}

		// Token: 0x06000FAA RID: 4010 RVA: 0x00031764 File Offset: 0x0002F964
		Expression IArgumentProvider.GetArgument(int index)
		{
			if (index == 0)
			{
				return ExpressionUtils.ReturnObject<Expression>(this._arg0);
			}
			if (index != 1)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			return this._arg1;
		}

		// Token: 0x170002DD RID: 733
		// (get) Token: 0x06000FAB RID: 4011 RVA: 0x0002EE24 File Offset: 0x0002D024
		int IArgumentProvider.ArgumentCount
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x06000FAC RID: 4012 RVA: 0x0003178C File Offset: 0x0002F98C
		internal override bool SameArguments(ICollection<Expression> arguments)
		{
			if (arguments != null && arguments.Count == 2)
			{
				ReadOnlyCollection<Expression> readOnlyCollection = this._arg0 as ReadOnlyCollection<Expression>;
				if (readOnlyCollection != null)
				{
					return ExpressionUtils.SameElements<Expression>(arguments, readOnlyCollection);
				}
				using (IEnumerator<Expression> enumerator = arguments.GetEnumerator())
				{
					enumerator.MoveNext();
					if (enumerator.Current == this._arg0)
					{
						enumerator.MoveNext();
						return enumerator.Current == this._arg1;
					}
				}
				return false;
			}
			return false;
		}

		// Token: 0x06000FAD RID: 4013 RVA: 0x00031810 File Offset: 0x0002FA10
		internal override ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			return ExpressionUtils.ReturnReadOnly(this, ref this._arg0);
		}

		// Token: 0x06000FAE RID: 4014 RVA: 0x0003181E File Offset: 0x0002FA1E
		internal override DynamicExpression Rewrite(Expression[] args)
		{
			return ExpressionExtension.MakeDynamic(base.DelegateType, base.Binder, args[0], args[1]);
		}

		// Token: 0x040008C5 RID: 2245
		private object _arg0;

		// Token: 0x040008C6 RID: 2246
		private readonly Expression _arg1;
	}
}
