﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;
using System.Reflection;

namespace System.Linq.Expressions
{
	// Token: 0x02000271 RID: 625
	internal sealed class InstanceMethodCallExpression1 : InstanceMethodCallExpression, IArgumentProvider
	{
		// Token: 0x0600121E RID: 4638 RVA: 0x000360A5 File Offset: 0x000342A5
		public InstanceMethodCallExpression1(MethodInfo method, Expression instance, Expression arg0) : base(method, instance)
		{
			this._arg0 = arg0;
		}

		// Token: 0x0600121F RID: 4639 RVA: 0x000360B6 File Offset: 0x000342B6
		public override Expression GetArgument(int index)
		{
			if (index == 0)
			{
				return ExpressionUtils.ReturnObject<Expression>(this._arg0);
			}
			throw new ArgumentOutOfRangeException("index");
		}

		// Token: 0x17000345 RID: 837
		// (get) Token: 0x06001220 RID: 4640 RVA: 0x00009CDF File Offset: 0x00007EDF
		public override int ArgumentCount
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x06001221 RID: 4641 RVA: 0x000360D4 File Offset: 0x000342D4
		internal override bool SameArguments(ICollection<Expression> arguments)
		{
			if (arguments != null && arguments.Count == 1)
			{
				using (IEnumerator<Expression> enumerator = arguments.GetEnumerator())
				{
					enumerator.MoveNext();
					return enumerator.Current == ExpressionUtils.ReturnObject<Expression>(this._arg0);
				}
				return false;
			}
			return false;
		}

		// Token: 0x06001222 RID: 4642 RVA: 0x00036130 File Offset: 0x00034330
		internal override ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			return ExpressionUtils.ReturnReadOnly(this, ref this._arg0);
		}

		// Token: 0x06001223 RID: 4643 RVA: 0x0003613E File Offset: 0x0003433E
		internal override MethodCallExpression Rewrite(Expression instance, IReadOnlyList<Expression> args)
		{
			if (args != null)
			{
				return Expression.Call(instance, base.Method, args[0]);
			}
			return Expression.Call(instance, base.Method, ExpressionUtils.ReturnObject<Expression>(this._arg0));
		}

		// Token: 0x0400097D RID: 2429
		private object _arg0;
	}
}
