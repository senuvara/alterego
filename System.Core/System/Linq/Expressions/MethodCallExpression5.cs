﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;
using System.Reflection;

namespace System.Linq.Expressions
{
	// Token: 0x0200026F RID: 623
	internal sealed class MethodCallExpression5 : MethodCallExpression, IArgumentProvider
	{
		// Token: 0x06001212 RID: 4626 RVA: 0x00035EBC File Offset: 0x000340BC
		public MethodCallExpression5(MethodInfo method, Expression arg0, Expression arg1, Expression arg2, Expression arg3, Expression arg4) : base(method)
		{
			this._arg0 = arg0;
			this._arg1 = arg1;
			this._arg2 = arg2;
			this._arg3 = arg3;
			this._arg4 = arg4;
		}

		// Token: 0x06001213 RID: 4627 RVA: 0x00035EEC File Offset: 0x000340EC
		public override Expression GetArgument(int index)
		{
			switch (index)
			{
			case 0:
				return ExpressionUtils.ReturnObject<Expression>(this._arg0);
			case 1:
				return this._arg1;
			case 2:
				return this._arg2;
			case 3:
				return this._arg3;
			case 4:
				return this._arg4;
			default:
				throw new ArgumentOutOfRangeException("index");
			}
		}

		// Token: 0x17000343 RID: 835
		// (get) Token: 0x06001214 RID: 4628 RVA: 0x0002F1EC File Offset: 0x0002D3EC
		public override int ArgumentCount
		{
			get
			{
				return 5;
			}
		}

		// Token: 0x06001215 RID: 4629 RVA: 0x00035F48 File Offset: 0x00034148
		internal override bool SameArguments(ICollection<Expression> arguments)
		{
			if (arguments != null && arguments.Count == 5)
			{
				ReadOnlyCollection<Expression> readOnlyCollection = this._arg0 as ReadOnlyCollection<Expression>;
				if (readOnlyCollection != null)
				{
					return ExpressionUtils.SameElements<Expression>(arguments, readOnlyCollection);
				}
				using (IEnumerator<Expression> enumerator = arguments.GetEnumerator())
				{
					enumerator.MoveNext();
					if (enumerator.Current == this._arg0)
					{
						enumerator.MoveNext();
						if (enumerator.Current == this._arg1)
						{
							enumerator.MoveNext();
							if (enumerator.Current == this._arg2)
							{
								enumerator.MoveNext();
								if (enumerator.Current == this._arg3)
								{
									enumerator.MoveNext();
									return enumerator.Current == this._arg4;
								}
							}
						}
					}
				}
				return false;
			}
			return false;
		}

		// Token: 0x06001216 RID: 4630 RVA: 0x00036010 File Offset: 0x00034210
		internal override ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			return ExpressionUtils.ReturnReadOnly(this, ref this._arg0);
		}

		// Token: 0x06001217 RID: 4631 RVA: 0x00036020 File Offset: 0x00034220
		internal override MethodCallExpression Rewrite(Expression instance, IReadOnlyList<Expression> args)
		{
			if (args != null)
			{
				return Expression.Call(base.Method, args[0], args[1], args[2], args[3], args[4]);
			}
			return Expression.Call(base.Method, ExpressionUtils.ReturnObject<Expression>(this._arg0), this._arg1, this._arg2, this._arg3, this._arg4);
		}

		// Token: 0x04000978 RID: 2424
		private object _arg0;

		// Token: 0x04000979 RID: 2425
		private readonly Expression _arg1;

		// Token: 0x0400097A RID: 2426
		private readonly Expression _arg2;

		// Token: 0x0400097B RID: 2427
		private readonly Expression _arg3;

		// Token: 0x0400097C RID: 2428
		private readonly Expression _arg4;
	}
}
