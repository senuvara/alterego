﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

namespace System.Linq.Expressions
{
	// Token: 0x0200023F RID: 575
	internal sealed class ExpressionStringBuilder : ExpressionVisitor
	{
		// Token: 0x06001097 RID: 4247 RVA: 0x00032D7D File Offset: 0x00030F7D
		private ExpressionStringBuilder()
		{
			this._out = new StringBuilder();
		}

		// Token: 0x06001098 RID: 4248 RVA: 0x00032D90 File Offset: 0x00030F90
		public override string ToString()
		{
			return this._out.ToString();
		}

		// Token: 0x06001099 RID: 4249 RVA: 0x00032D9D File Offset: 0x00030F9D
		private int GetLabelId(LabelTarget label)
		{
			return this.GetId(label);
		}

		// Token: 0x0600109A RID: 4250 RVA: 0x00032D9D File Offset: 0x00030F9D
		private int GetParamId(ParameterExpression p)
		{
			return this.GetId(p);
		}

		// Token: 0x0600109B RID: 4251 RVA: 0x00032DA8 File Offset: 0x00030FA8
		private int GetId(object o)
		{
			if (this._ids == null)
			{
				this._ids = new Dictionary<object, int>();
			}
			int count;
			if (!this._ids.TryGetValue(o, out count))
			{
				count = this._ids.Count;
				this._ids.Add(o, count);
			}
			return count;
		}

		// Token: 0x0600109C RID: 4252 RVA: 0x00032DF2 File Offset: 0x00030FF2
		private void Out(string s)
		{
			this._out.Append(s);
		}

		// Token: 0x0600109D RID: 4253 RVA: 0x00032E01 File Offset: 0x00031001
		private void Out(char c)
		{
			this._out.Append(c);
		}

		// Token: 0x0600109E RID: 4254 RVA: 0x00032E10 File Offset: 0x00031010
		internal static string ExpressionToString(Expression node)
		{
			ExpressionStringBuilder expressionStringBuilder = new ExpressionStringBuilder();
			expressionStringBuilder.Visit(node);
			return expressionStringBuilder.ToString();
		}

		// Token: 0x0600109F RID: 4255 RVA: 0x00032E24 File Offset: 0x00031024
		internal static string CatchBlockToString(CatchBlock node)
		{
			ExpressionStringBuilder expressionStringBuilder = new ExpressionStringBuilder();
			expressionStringBuilder.VisitCatchBlock(node);
			return expressionStringBuilder.ToString();
		}

		// Token: 0x060010A0 RID: 4256 RVA: 0x00032E38 File Offset: 0x00031038
		internal static string SwitchCaseToString(SwitchCase node)
		{
			ExpressionStringBuilder expressionStringBuilder = new ExpressionStringBuilder();
			expressionStringBuilder.VisitSwitchCase(node);
			return expressionStringBuilder.ToString();
		}

		// Token: 0x060010A1 RID: 4257 RVA: 0x00032E4C File Offset: 0x0003104C
		internal static string MemberBindingToString(MemberBinding node)
		{
			ExpressionStringBuilder expressionStringBuilder = new ExpressionStringBuilder();
			expressionStringBuilder.VisitMemberBinding(node);
			return expressionStringBuilder.ToString();
		}

		// Token: 0x060010A2 RID: 4258 RVA: 0x00032E60 File Offset: 0x00031060
		internal static string ElementInitBindingToString(ElementInit node)
		{
			ExpressionStringBuilder expressionStringBuilder = new ExpressionStringBuilder();
			expressionStringBuilder.VisitElementInit(node);
			return expressionStringBuilder.ToString();
		}

		// Token: 0x060010A3 RID: 4259 RVA: 0x00032E74 File Offset: 0x00031074
		private void VisitExpressions<T>(char open, ReadOnlyCollection<T> expressions, char close) where T : Expression
		{
			this.VisitExpressions<T>(open, expressions, close, ", ");
		}

		// Token: 0x060010A4 RID: 4260 RVA: 0x00032E84 File Offset: 0x00031084
		private void VisitExpressions<T>(char open, ReadOnlyCollection<T> expressions, char close, string seperator) where T : Expression
		{
			this.Out(open);
			if (expressions != null)
			{
				bool flag = true;
				foreach (T t in expressions)
				{
					if (flag)
					{
						flag = false;
					}
					else
					{
						this.Out(seperator);
					}
					this.Visit(t);
				}
			}
			this.Out(close);
		}

		// Token: 0x060010A5 RID: 4261 RVA: 0x00032EF4 File Offset: 0x000310F4
		protected internal override Expression VisitBinary(BinaryExpression node)
		{
			if (node.NodeType == ExpressionType.ArrayIndex)
			{
				this.Visit(node.Left);
				this.Out('[');
				this.Visit(node.Right);
				this.Out(']');
			}
			else
			{
				ExpressionType nodeType = node.NodeType;
				string s;
				switch (nodeType)
				{
				case ExpressionType.Add:
				case ExpressionType.AddChecked:
					s = "+";
					goto IL_2CE;
				case ExpressionType.And:
					s = (ExpressionStringBuilder.IsBool(node) ? "And" : "&");
					goto IL_2CE;
				case ExpressionType.AndAlso:
					s = "AndAlso";
					goto IL_2CE;
				case ExpressionType.ArrayLength:
				case ExpressionType.ArrayIndex:
				case ExpressionType.Call:
				case ExpressionType.Conditional:
				case ExpressionType.Constant:
				case ExpressionType.Convert:
				case ExpressionType.ConvertChecked:
				case ExpressionType.Invoke:
				case ExpressionType.Lambda:
				case ExpressionType.ListInit:
				case ExpressionType.MemberAccess:
				case ExpressionType.MemberInit:
				case ExpressionType.Negate:
				case ExpressionType.UnaryPlus:
				case ExpressionType.NegateChecked:
				case ExpressionType.New:
				case ExpressionType.NewArrayInit:
				case ExpressionType.NewArrayBounds:
				case ExpressionType.Not:
				case ExpressionType.Parameter:
				case ExpressionType.Quote:
				case ExpressionType.TypeAs:
				case ExpressionType.TypeIs:
					break;
				case ExpressionType.Coalesce:
					s = "??";
					goto IL_2CE;
				case ExpressionType.Divide:
					s = "/";
					goto IL_2CE;
				case ExpressionType.Equal:
					s = "==";
					goto IL_2CE;
				case ExpressionType.ExclusiveOr:
					s = "^";
					goto IL_2CE;
				case ExpressionType.GreaterThan:
					s = ">";
					goto IL_2CE;
				case ExpressionType.GreaterThanOrEqual:
					s = ">=";
					goto IL_2CE;
				case ExpressionType.LeftShift:
					s = "<<";
					goto IL_2CE;
				case ExpressionType.LessThan:
					s = "<";
					goto IL_2CE;
				case ExpressionType.LessThanOrEqual:
					s = "<=";
					goto IL_2CE;
				case ExpressionType.Modulo:
					s = "%";
					goto IL_2CE;
				case ExpressionType.Multiply:
				case ExpressionType.MultiplyChecked:
					s = "*";
					goto IL_2CE;
				case ExpressionType.NotEqual:
					s = "!=";
					goto IL_2CE;
				case ExpressionType.Or:
					s = (ExpressionStringBuilder.IsBool(node) ? "Or" : "|");
					goto IL_2CE;
				case ExpressionType.OrElse:
					s = "OrElse";
					goto IL_2CE;
				case ExpressionType.Power:
					s = "**";
					goto IL_2CE;
				case ExpressionType.RightShift:
					s = ">>";
					goto IL_2CE;
				case ExpressionType.Subtract:
				case ExpressionType.SubtractChecked:
					s = "-";
					goto IL_2CE;
				case ExpressionType.Assign:
					s = "=";
					goto IL_2CE;
				default:
					switch (nodeType)
					{
					case ExpressionType.AddAssign:
					case ExpressionType.AddAssignChecked:
						s = "+=";
						goto IL_2CE;
					case ExpressionType.AndAssign:
						s = (ExpressionStringBuilder.IsBool(node) ? "&&=" : "&=");
						goto IL_2CE;
					case ExpressionType.DivideAssign:
						s = "/=";
						goto IL_2CE;
					case ExpressionType.ExclusiveOrAssign:
						s = "^=";
						goto IL_2CE;
					case ExpressionType.LeftShiftAssign:
						s = "<<=";
						goto IL_2CE;
					case ExpressionType.ModuloAssign:
						s = "%=";
						goto IL_2CE;
					case ExpressionType.MultiplyAssign:
					case ExpressionType.MultiplyAssignChecked:
						s = "*=";
						goto IL_2CE;
					case ExpressionType.OrAssign:
						s = (ExpressionStringBuilder.IsBool(node) ? "||=" : "|=");
						goto IL_2CE;
					case ExpressionType.PowerAssign:
						s = "**=";
						goto IL_2CE;
					case ExpressionType.RightShiftAssign:
						s = ">>=";
						goto IL_2CE;
					case ExpressionType.SubtractAssign:
					case ExpressionType.SubtractAssignChecked:
						s = "-=";
						goto IL_2CE;
					}
					break;
				}
				throw new InvalidOperationException();
				IL_2CE:
				this.Out('(');
				this.Visit(node.Left);
				this.Out(' ');
				this.Out(s);
				this.Out(' ');
				this.Visit(node.Right);
				this.Out(')');
			}
			return node;
		}

		// Token: 0x060010A6 RID: 4262 RVA: 0x00033214 File Offset: 0x00031414
		protected internal override Expression VisitParameter(ParameterExpression node)
		{
			if (node.IsByRef)
			{
				this.Out("ref ");
			}
			string name = node.Name;
			if (string.IsNullOrEmpty(name))
			{
				this.Out("Param_" + this.GetParamId(node));
			}
			else
			{
				this.Out(name);
			}
			return node;
		}

		// Token: 0x060010A7 RID: 4263 RVA: 0x0003326C File Offset: 0x0003146C
		protected internal override Expression VisitLambda<T>(Expression<T> node)
		{
			if (node.ParameterCount == 1)
			{
				this.Visit(node.GetParameter(0));
			}
			else
			{
				this.Out('(');
				string s = ", ";
				int i = 0;
				int parameterCount = node.ParameterCount;
				while (i < parameterCount)
				{
					if (i > 0)
					{
						this.Out(s);
					}
					this.Visit(node.GetParameter(i));
					i++;
				}
				this.Out(')');
			}
			this.Out(" => ");
			this.Visit(node.Body);
			return node;
		}

		// Token: 0x060010A8 RID: 4264 RVA: 0x000332F0 File Offset: 0x000314F0
		protected internal override Expression VisitListInit(ListInitExpression node)
		{
			this.Visit(node.NewExpression);
			this.Out(" {");
			int i = 0;
			int count = node.Initializers.Count;
			while (i < count)
			{
				if (i > 0)
				{
					this.Out(", ");
				}
				this.VisitElementInit(node.Initializers[i]);
				i++;
			}
			this.Out('}');
			return node;
		}

		// Token: 0x060010A9 RID: 4265 RVA: 0x00033358 File Offset: 0x00031558
		protected internal override Expression VisitConditional(ConditionalExpression node)
		{
			this.Out("IIF(");
			this.Visit(node.Test);
			this.Out(", ");
			this.Visit(node.IfTrue);
			this.Out(", ");
			this.Visit(node.IfFalse);
			this.Out(')');
			return node;
		}

		// Token: 0x060010AA RID: 4266 RVA: 0x000333B8 File Offset: 0x000315B8
		protected internal override Expression VisitConstant(ConstantExpression node)
		{
			if (node.Value != null)
			{
				string text = node.Value.ToString();
				if (node.Value is string)
				{
					this.Out('"');
					this.Out(text);
					this.Out('"');
				}
				else if (text == node.Value.GetType().ToString())
				{
					this.Out("value(");
					this.Out(text);
					this.Out(')');
				}
				else
				{
					this.Out(text);
				}
			}
			else
			{
				this.Out("null");
			}
			return node;
		}

		// Token: 0x060010AB RID: 4267 RVA: 0x00033448 File Offset: 0x00031648
		protected internal override Expression VisitDebugInfo(DebugInfoExpression node)
		{
			string s = string.Format(CultureInfo.CurrentCulture, "<DebugInfo({0}: {1}, {2}, {3}, {4})>", new object[]
			{
				node.Document.FileName,
				node.StartLine,
				node.StartColumn,
				node.EndLine,
				node.EndColumn
			});
			this.Out(s);
			return node;
		}

		// Token: 0x060010AC RID: 4268 RVA: 0x000334B9 File Offset: 0x000316B9
		protected internal override Expression VisitRuntimeVariables(RuntimeVariablesExpression node)
		{
			this.VisitExpressions<ParameterExpression>('(', node.Variables, ')');
			return node;
		}

		// Token: 0x060010AD RID: 4269 RVA: 0x000334CC File Offset: 0x000316CC
		private void OutMember(Expression instance, MemberInfo member)
		{
			if (instance != null)
			{
				this.Visit(instance);
			}
			else
			{
				this.Out(member.DeclaringType.Name);
			}
			this.Out('.');
			this.Out(member.Name);
		}

		// Token: 0x060010AE RID: 4270 RVA: 0x00033500 File Offset: 0x00031700
		protected internal override Expression VisitMember(MemberExpression node)
		{
			this.OutMember(node.Expression, node.Member);
			return node;
		}

		// Token: 0x060010AF RID: 4271 RVA: 0x00033518 File Offset: 0x00031718
		protected internal override Expression VisitMemberInit(MemberInitExpression node)
		{
			if (node.NewExpression.ArgumentCount == 0 && node.NewExpression.Type.Name.Contains("<"))
			{
				this.Out("new");
			}
			else
			{
				this.Visit(node.NewExpression);
			}
			this.Out(" {");
			int i = 0;
			int count = node.Bindings.Count;
			while (i < count)
			{
				MemberBinding node2 = node.Bindings[i];
				if (i > 0)
				{
					this.Out(", ");
				}
				this.VisitMemberBinding(node2);
				i++;
			}
			this.Out('}');
			return node;
		}

		// Token: 0x060010B0 RID: 4272 RVA: 0x000335B8 File Offset: 0x000317B8
		protected override MemberAssignment VisitMemberAssignment(MemberAssignment assignment)
		{
			this.Out(assignment.Member.Name);
			this.Out(" = ");
			this.Visit(assignment.Expression);
			return assignment;
		}

		// Token: 0x060010B1 RID: 4273 RVA: 0x000335E4 File Offset: 0x000317E4
		protected override MemberListBinding VisitMemberListBinding(MemberListBinding binding)
		{
			this.Out(binding.Member.Name);
			this.Out(" = {");
			int i = 0;
			int count = binding.Initializers.Count;
			while (i < count)
			{
				if (i > 0)
				{
					this.Out(", ");
				}
				this.VisitElementInit(binding.Initializers[i]);
				i++;
			}
			this.Out('}');
			return binding;
		}

		// Token: 0x060010B2 RID: 4274 RVA: 0x00033650 File Offset: 0x00031850
		protected override MemberMemberBinding VisitMemberMemberBinding(MemberMemberBinding binding)
		{
			this.Out(binding.Member.Name);
			this.Out(" = {");
			int i = 0;
			int count = binding.Bindings.Count;
			while (i < count)
			{
				if (i > 0)
				{
					this.Out(", ");
				}
				this.VisitMemberBinding(binding.Bindings[i]);
				i++;
			}
			this.Out('}');
			return binding;
		}

		// Token: 0x060010B3 RID: 4275 RVA: 0x000336BC File Offset: 0x000318BC
		protected override ElementInit VisitElementInit(ElementInit initializer)
		{
			this.Out(initializer.AddMethod.ToString());
			string s = ", ";
			this.Out('(');
			int i = 0;
			int argumentCount = initializer.ArgumentCount;
			while (i < argumentCount)
			{
				if (i > 0)
				{
					this.Out(s);
				}
				this.Visit(initializer.GetArgument(i));
				i++;
			}
			this.Out(')');
			return initializer;
		}

		// Token: 0x060010B4 RID: 4276 RVA: 0x00033720 File Offset: 0x00031920
		protected internal override Expression VisitInvocation(InvocationExpression node)
		{
			this.Out("Invoke(");
			this.Visit(node.Expression);
			string s = ", ";
			int i = 0;
			int argumentCount = node.ArgumentCount;
			while (i < argumentCount)
			{
				this.Out(s);
				this.Visit(node.GetArgument(i));
				i++;
			}
			this.Out(')');
			return node;
		}

		// Token: 0x060010B5 RID: 4277 RVA: 0x0003377C File Offset: 0x0003197C
		protected internal override Expression VisitMethodCall(MethodCallExpression node)
		{
			int num = 0;
			Expression expression = node.Object;
			if (node.Method.GetCustomAttribute(typeof(ExtensionAttribute)) != null)
			{
				num = 1;
				expression = node.GetArgument(0);
			}
			if (expression != null)
			{
				this.Visit(expression);
				this.Out('.');
			}
			this.Out(node.Method.Name);
			this.Out('(');
			int i = num;
			int argumentCount = node.ArgumentCount;
			while (i < argumentCount)
			{
				if (i > num)
				{
					this.Out(", ");
				}
				this.Visit(node.GetArgument(i));
				i++;
			}
			this.Out(')');
			return node;
		}

		// Token: 0x060010B6 RID: 4278 RVA: 0x00033818 File Offset: 0x00031A18
		protected internal override Expression VisitNewArray(NewArrayExpression node)
		{
			ExpressionType nodeType = node.NodeType;
			if (nodeType != ExpressionType.NewArrayInit)
			{
				if (nodeType == ExpressionType.NewArrayBounds)
				{
					this.Out("new ");
					this.Out(node.Type.ToString());
					this.VisitExpressions<Expression>('(', node.Expressions, ')');
				}
			}
			else
			{
				this.Out("new [] ");
				this.VisitExpressions<Expression>('{', node.Expressions, '}');
			}
			return node;
		}

		// Token: 0x060010B7 RID: 4279 RVA: 0x00033880 File Offset: 0x00031A80
		protected internal override Expression VisitNew(NewExpression node)
		{
			this.Out("new ");
			this.Out(node.Type.Name);
			this.Out('(');
			ReadOnlyCollection<MemberInfo> members = node.Members;
			for (int i = 0; i < node.ArgumentCount; i++)
			{
				if (i > 0)
				{
					this.Out(", ");
				}
				if (members != null)
				{
					string name = members[i].Name;
					this.Out(name);
					this.Out(" = ");
				}
				this.Visit(node.GetArgument(i));
			}
			this.Out(')');
			return node;
		}

		// Token: 0x060010B8 RID: 4280 RVA: 0x00033914 File Offset: 0x00031B14
		protected internal override Expression VisitTypeBinary(TypeBinaryExpression node)
		{
			this.Out('(');
			this.Visit(node.Expression);
			ExpressionType nodeType = node.NodeType;
			if (nodeType != ExpressionType.TypeIs)
			{
				if (nodeType == ExpressionType.TypeEqual)
				{
					this.Out(" TypeEqual ");
				}
			}
			else
			{
				this.Out(" Is ");
			}
			this.Out(node.TypeOperand.Name);
			this.Out(')');
			return node;
		}

		// Token: 0x060010B9 RID: 4281 RVA: 0x0003397C File Offset: 0x00031B7C
		protected internal override Expression VisitUnary(UnaryExpression node)
		{
			ExpressionType nodeType = node.NodeType;
			if (nodeType <= ExpressionType.Quote)
			{
				if (nodeType <= ExpressionType.Convert)
				{
					if (nodeType == ExpressionType.ArrayLength)
					{
						this.Out("ArrayLength(");
						goto IL_19E;
					}
					if (nodeType == ExpressionType.Convert)
					{
						this.Out("Convert(");
						goto IL_19E;
					}
				}
				else
				{
					if (nodeType == ExpressionType.ConvertChecked)
					{
						this.Out("ConvertChecked(");
						goto IL_19E;
					}
					switch (nodeType)
					{
					case ExpressionType.Negate:
					case ExpressionType.NegateChecked:
						this.Out('-');
						goto IL_19E;
					case ExpressionType.UnaryPlus:
						this.Out('+');
						goto IL_19E;
					case ExpressionType.New:
					case ExpressionType.NewArrayInit:
					case ExpressionType.NewArrayBounds:
						break;
					case ExpressionType.Not:
						this.Out("Not(");
						goto IL_19E;
					default:
						if (nodeType == ExpressionType.Quote)
						{
							goto IL_19E;
						}
						break;
					}
				}
			}
			else if (nodeType <= ExpressionType.Increment)
			{
				if (nodeType == ExpressionType.TypeAs)
				{
					this.Out('(');
					goto IL_19E;
				}
				if (nodeType == ExpressionType.Decrement)
				{
					this.Out("Decrement(");
					goto IL_19E;
				}
				if (nodeType == ExpressionType.Increment)
				{
					this.Out("Increment(");
					goto IL_19E;
				}
			}
			else
			{
				if (nodeType == ExpressionType.Throw)
				{
					this.Out("throw(");
					goto IL_19E;
				}
				if (nodeType == ExpressionType.Unbox)
				{
					this.Out("Unbox(");
					goto IL_19E;
				}
				switch (nodeType)
				{
				case ExpressionType.PreIncrementAssign:
					this.Out("++");
					goto IL_19E;
				case ExpressionType.PreDecrementAssign:
					this.Out("--");
					goto IL_19E;
				case ExpressionType.PostIncrementAssign:
				case ExpressionType.PostDecrementAssign:
					goto IL_19E;
				case ExpressionType.OnesComplement:
					this.Out("~(");
					goto IL_19E;
				case ExpressionType.IsTrue:
					this.Out("IsTrue(");
					goto IL_19E;
				case ExpressionType.IsFalse:
					this.Out("IsFalse(");
					goto IL_19E;
				}
			}
			throw new InvalidOperationException();
			IL_19E:
			this.Visit(node.Operand);
			nodeType = node.NodeType;
			if (nodeType <= ExpressionType.NegateChecked)
			{
				if (nodeType - ExpressionType.Convert <= 1)
				{
					this.Out(", ");
					this.Out(node.Type.Name);
					this.Out(')');
					return node;
				}
				if (nodeType - ExpressionType.Negate <= 2)
				{
					return node;
				}
			}
			else
			{
				if (nodeType == ExpressionType.Quote)
				{
					return node;
				}
				if (nodeType == ExpressionType.TypeAs)
				{
					this.Out(" As ");
					this.Out(node.Type.Name);
					this.Out(')');
					return node;
				}
				switch (nodeType)
				{
				case ExpressionType.PreIncrementAssign:
				case ExpressionType.PreDecrementAssign:
					return node;
				case ExpressionType.PostIncrementAssign:
					this.Out("++");
					return node;
				case ExpressionType.PostDecrementAssign:
					this.Out("--");
					return node;
				}
			}
			this.Out(')');
			return node;
		}

		// Token: 0x060010BA RID: 4282 RVA: 0x00033BF0 File Offset: 0x00031DF0
		protected internal override Expression VisitBlock(BlockExpression node)
		{
			this.Out('{');
			foreach (ParameterExpression node2 in node.Variables)
			{
				this.Out("var ");
				this.Visit(node2);
				this.Out(';');
			}
			this.Out(" ... }");
			return node;
		}

		// Token: 0x060010BB RID: 4283 RVA: 0x00033C68 File Offset: 0x00031E68
		protected internal override Expression VisitDefault(DefaultExpression node)
		{
			this.Out("default(");
			this.Out(node.Type.Name);
			this.Out(')');
			return node;
		}

		// Token: 0x060010BC RID: 4284 RVA: 0x00033C8F File Offset: 0x00031E8F
		protected internal override Expression VisitLabel(LabelExpression node)
		{
			this.Out("{ ... } ");
			this.DumpLabel(node.Target);
			this.Out(':');
			return node;
		}

		// Token: 0x060010BD RID: 4285 RVA: 0x00033CB4 File Offset: 0x00031EB4
		protected internal override Expression VisitGoto(GotoExpression node)
		{
			string s;
			switch (node.Kind)
			{
			case GotoExpressionKind.Goto:
				s = "goto";
				break;
			case GotoExpressionKind.Return:
				s = "return";
				break;
			case GotoExpressionKind.Break:
				s = "break";
				break;
			case GotoExpressionKind.Continue:
				s = "continue";
				break;
			default:
				throw new InvalidOperationException();
			}
			this.Out(s);
			this.Out(' ');
			this.DumpLabel(node.Target);
			if (node.Value != null)
			{
				this.Out(" (");
				this.Visit(node.Value);
				this.Out(")");
			}
			return node;
		}

		// Token: 0x060010BE RID: 4286 RVA: 0x00033D4D File Offset: 0x00031F4D
		protected internal override Expression VisitLoop(LoopExpression node)
		{
			this.Out("loop { ... }");
			return node;
		}

		// Token: 0x060010BF RID: 4287 RVA: 0x00033D5B File Offset: 0x00031F5B
		protected override SwitchCase VisitSwitchCase(SwitchCase node)
		{
			this.Out("case ");
			this.VisitExpressions<Expression>('(', node.TestValues, ')');
			this.Out(": ...");
			return node;
		}

		// Token: 0x060010C0 RID: 4288 RVA: 0x00033D84 File Offset: 0x00031F84
		protected internal override Expression VisitSwitch(SwitchExpression node)
		{
			this.Out("switch ");
			this.Out('(');
			this.Visit(node.SwitchValue);
			this.Out(") { ... }");
			return node;
		}

		// Token: 0x060010C1 RID: 4289 RVA: 0x00033DB4 File Offset: 0x00031FB4
		protected override CatchBlock VisitCatchBlock(CatchBlock node)
		{
			this.Out("catch (");
			this.Out(node.Test.Name);
			ParameterExpression variable = node.Variable;
			if (!string.IsNullOrEmpty((variable != null) ? variable.Name : null))
			{
				this.Out(' ');
				this.Out(node.Variable.Name);
			}
			this.Out(") { ... }");
			return node;
		}

		// Token: 0x060010C2 RID: 4290 RVA: 0x00033E1B File Offset: 0x0003201B
		protected internal override Expression VisitTry(TryExpression node)
		{
			this.Out("try { ... }");
			return node;
		}

		// Token: 0x060010C3 RID: 4291 RVA: 0x00033E2C File Offset: 0x0003202C
		protected internal override Expression VisitIndex(IndexExpression node)
		{
			if (node.Object != null)
			{
				this.Visit(node.Object);
			}
			else
			{
				this.Out(node.Indexer.DeclaringType.Name);
			}
			if (node.Indexer != null)
			{
				this.Out('.');
				this.Out(node.Indexer.Name);
			}
			this.Out('[');
			int i = 0;
			int argumentCount = node.ArgumentCount;
			while (i < argumentCount)
			{
				if (i > 0)
				{
					this.Out(", ");
				}
				this.Visit(node.GetArgument(i));
				i++;
			}
			this.Out(']');
			return node;
		}

		// Token: 0x060010C4 RID: 4292 RVA: 0x00033ED0 File Offset: 0x000320D0
		protected internal override Expression VisitExtension(Expression node)
		{
			MethodInfo method = node.GetType().GetMethod("ToString", Type.EmptyTypes);
			if (method.DeclaringType != typeof(Expression) && !method.IsStatic)
			{
				this.Out(node.ToString());
				return node;
			}
			this.Out('[');
			this.Out((node.NodeType == ExpressionType.Extension) ? node.GetType().FullName : node.NodeType.ToString());
			this.Out(']');
			return node;
		}

		// Token: 0x060010C5 RID: 4293 RVA: 0x00033F64 File Offset: 0x00032164
		private void DumpLabel(LabelTarget target)
		{
			if (!string.IsNullOrEmpty(target.Name))
			{
				this.Out(target.Name);
				return;
			}
			int labelId = this.GetLabelId(target);
			this.Out("UnnamedLabel_" + labelId);
		}

		// Token: 0x060010C6 RID: 4294 RVA: 0x00033FA9 File Offset: 0x000321A9
		private static bool IsBool(Expression node)
		{
			return node.Type == typeof(bool) || node.Type == typeof(bool?);
		}

		// Token: 0x040008D3 RID: 2259
		private readonly StringBuilder _out;

		// Token: 0x040008D4 RID: 2260
		private Dictionary<object, int> _ids;
	}
}
