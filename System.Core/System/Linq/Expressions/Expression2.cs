﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;

namespace System.Linq.Expressions
{
	// Token: 0x02000257 RID: 599
	internal sealed class Expression2<TDelegate> : Expression<TDelegate>
	{
		// Token: 0x0600117D RID: 4477 RVA: 0x000350F1 File Offset: 0x000332F1
		public Expression2(Expression body, ParameterExpression par0, ParameterExpression par1) : base(body)
		{
			this._par0 = par0;
			this._par1 = par1;
		}

		// Token: 0x17000318 RID: 792
		// (get) Token: 0x0600117E RID: 4478 RVA: 0x0002EE24 File Offset: 0x0002D024
		internal override int ParameterCount
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x0600117F RID: 4479 RVA: 0x00035108 File Offset: 0x00033308
		internal override ParameterExpression GetParameter(int index)
		{
			if (index == 0)
			{
				return ExpressionUtils.ReturnObject<ParameterExpression>(this._par0);
			}
			if (index != 1)
			{
				throw Error.ArgumentOutOfRange("index");
			}
			return this._par1;
		}

		// Token: 0x06001180 RID: 4480 RVA: 0x00035130 File Offset: 0x00033330
		internal override bool SameParameters(ICollection<ParameterExpression> parameters)
		{
			if (parameters != null && parameters.Count == 2)
			{
				ReadOnlyCollection<ParameterExpression> readOnlyCollection = this._par0 as ReadOnlyCollection<ParameterExpression>;
				if (readOnlyCollection != null)
				{
					return ExpressionUtils.SameElements<ParameterExpression>(parameters, readOnlyCollection);
				}
				using (IEnumerator<ParameterExpression> enumerator = parameters.GetEnumerator())
				{
					enumerator.MoveNext();
					if (enumerator.Current == this._par0)
					{
						enumerator.MoveNext();
						return enumerator.Current == this._par1;
					}
				}
				return false;
			}
			return false;
		}

		// Token: 0x06001181 RID: 4481 RVA: 0x000351B4 File Offset: 0x000333B4
		internal override ReadOnlyCollection<ParameterExpression> GetOrMakeParameters()
		{
			return ExpressionUtils.ReturnReadOnly(this, ref this._par0);
		}

		// Token: 0x06001182 RID: 4482 RVA: 0x000351C2 File Offset: 0x000333C2
		internal override Expression<TDelegate> Rewrite(Expression body, ParameterExpression[] parameters)
		{
			if (parameters != null)
			{
				return Expression.Lambda<TDelegate>(body, parameters);
			}
			return Expression.Lambda<TDelegate>(body, new ParameterExpression[]
			{
				ExpressionUtils.ReturnObject<ParameterExpression>(this._par0),
				this._par1
			});
		}

		// Token: 0x0400094F RID: 2383
		private object _par0;

		// Token: 0x04000950 RID: 2384
		private readonly ParameterExpression _par1;
	}
}
