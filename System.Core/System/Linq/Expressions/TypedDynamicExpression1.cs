﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x02000234 RID: 564
	internal sealed class TypedDynamicExpression1 : DynamicExpression1
	{
		// Token: 0x06000FA7 RID: 4007 RVA: 0x00031730 File Offset: 0x0002F930
		internal TypedDynamicExpression1(Type retType, Type delegateType, CallSiteBinder binder, Expression arg0) : base(delegateType, binder, arg0)
		{
			this.Type = retType;
		}

		// Token: 0x170002DC RID: 732
		// (get) Token: 0x06000FA8 RID: 4008 RVA: 0x00031743 File Offset: 0x0002F943
		public sealed override Type Type
		{
			[CompilerGenerated]
			get
			{
				return this.<Type>k__BackingField;
			}
		}

		// Token: 0x040008C4 RID: 2244
		[CompilerGenerated]
		private readonly Type <Type>k__BackingField;
	}
}
