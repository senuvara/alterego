﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Represents an expression that has a conditional operator.</summary>
	// Token: 0x02000225 RID: 549
	[DebuggerTypeProxy(typeof(Expression.ConditionalExpressionProxy))]
	public class ConditionalExpression : Expression
	{
		// Token: 0x06000EF7 RID: 3831 RVA: 0x0002F78A File Offset: 0x0002D98A
		internal ConditionalExpression(Expression test, Expression ifTrue)
		{
			this.Test = test;
			this.IfTrue = ifTrue;
		}

		// Token: 0x06000EF8 RID: 3832 RVA: 0x0002F7A0 File Offset: 0x0002D9A0
		internal static ConditionalExpression Make(Expression test, Expression ifTrue, Expression ifFalse, Type type)
		{
			if (ifTrue.Type != type || ifFalse.Type != type)
			{
				return new FullConditionalExpressionWithType(test, ifTrue, ifFalse, type);
			}
			if (ifFalse is DefaultExpression && ifFalse.Type == typeof(void))
			{
				return new ConditionalExpression(test, ifTrue);
			}
			return new FullConditionalExpression(test, ifTrue, ifFalse);
		}

		/// <summary>Returns the node type of this expression. Extension nodes should return <see cref="F:System.Linq.Expressions.ExpressionType.Extension" /> when overriding this method.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.ExpressionType" /> of the expression.</returns>
		// Token: 0x170002B1 RID: 689
		// (get) Token: 0x06000EF9 RID: 3833 RVA: 0x0002F802 File Offset: 0x0002DA02
		public sealed override ExpressionType NodeType
		{
			get
			{
				return ExpressionType.Conditional;
			}
		}

		/// <summary>Gets the static type of the expression that this <see cref="T:System.Linq.Expressions.Expression" /> represents.</summary>
		/// <returns>The <see cref="P:System.Linq.Expressions.ConditionalExpression.Type" /> that represents the static type of the expression.</returns>
		// Token: 0x170002B2 RID: 690
		// (get) Token: 0x06000EFA RID: 3834 RVA: 0x0002F805 File Offset: 0x0002DA05
		public override Type Type
		{
			get
			{
				return this.IfTrue.Type;
			}
		}

		/// <summary>Gets the test of the conditional operation.</summary>
		/// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> that represents the test of the conditional operation.</returns>
		// Token: 0x170002B3 RID: 691
		// (get) Token: 0x06000EFB RID: 3835 RVA: 0x0002F812 File Offset: 0x0002DA12
		public Expression Test
		{
			[CompilerGenerated]
			get
			{
				return this.<Test>k__BackingField;
			}
		}

		/// <summary>Gets the expression to execute if the test evaluates to <see langword="true" />.</summary>
		/// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> that represents the expression to execute if the test is <see langword="true" />.</returns>
		// Token: 0x170002B4 RID: 692
		// (get) Token: 0x06000EFC RID: 3836 RVA: 0x0002F81A File Offset: 0x0002DA1A
		public Expression IfTrue
		{
			[CompilerGenerated]
			get
			{
				return this.<IfTrue>k__BackingField;
			}
		}

		/// <summary>Gets the expression to execute if the test evaluates to <see langword="false" />.</summary>
		/// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> that represents the expression to execute if the test is <see langword="false" />.</returns>
		// Token: 0x170002B5 RID: 693
		// (get) Token: 0x06000EFD RID: 3837 RVA: 0x0002F822 File Offset: 0x0002DA22
		public Expression IfFalse
		{
			get
			{
				return this.GetFalse();
			}
		}

		// Token: 0x06000EFE RID: 3838 RVA: 0x0002F82A File Offset: 0x0002DA2A
		internal virtual Expression GetFalse()
		{
			return Utils.Empty;
		}

		/// <summary>Dispatches to the specific visit method for this node type. For example, <see cref="T:System.Linq.Expressions.MethodCallExpression" /> calls the <see cref="M:System.Linq.Expressions.ExpressionVisitor.VisitMethodCall(System.Linq.Expressions.MethodCallExpression)" />.</summary>
		/// <param name="visitor">The visitor to visit this node with.</param>
		/// <returns>The result of visiting this node.</returns>
		// Token: 0x06000EFF RID: 3839 RVA: 0x0002F831 File Offset: 0x0002DA31
		protected internal override Expression Accept(ExpressionVisitor visitor)
		{
			return visitor.VisitConditional(this);
		}

		/// <summary>Creates a new expression that is like this one, but using the supplied children. If all of the children are the same, it will return this expression</summary>
		/// <param name="test">The <see cref="P:System.Linq.Expressions.ConditionalExpression.Test" /> property of the result.</param>
		/// <param name="ifTrue">The <see cref="P:System.Linq.Expressions.ConditionalExpression.IfTrue" /> property of the result.</param>
		/// <param name="ifFalse">The <see cref="P:System.Linq.Expressions.ConditionalExpression.IfFalse" /> property of the result.</param>
		/// <returns>This expression if no children changed, or an expression with the updated children.</returns>
		// Token: 0x06000F00 RID: 3840 RVA: 0x0002F83A File Offset: 0x0002DA3A
		public ConditionalExpression Update(Expression test, Expression ifTrue, Expression ifFalse)
		{
			if (test == this.Test && ifTrue == this.IfTrue && ifFalse == this.IfFalse)
			{
				return this;
			}
			return Expression.Condition(test, ifTrue, ifFalse, this.Type);
		}

		// Token: 0x06000F01 RID: 3841 RVA: 0x0000220F File Offset: 0x0000040F
		internal ConditionalExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040008A3 RID: 2211
		[CompilerGenerated]
		private readonly Expression <Test>k__BackingField;

		// Token: 0x040008A4 RID: 2212
		[CompilerGenerated]
		private readonly Expression <IfTrue>k__BackingField;
	}
}
