﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x0200021E RID: 542
	internal sealed class ScopeWithType : ScopeN
	{
		// Token: 0x06000ED3 RID: 3795 RVA: 0x0002F40D File Offset: 0x0002D60D
		internal ScopeWithType(IReadOnlyList<ParameterExpression> variables, IReadOnlyList<Expression> expressions, Type type) : base(variables, expressions)
		{
			this.Type = type;
		}

		// Token: 0x170002A7 RID: 679
		// (get) Token: 0x06000ED4 RID: 3796 RVA: 0x0002F41E File Offset: 0x0002D61E
		public sealed override Type Type
		{
			[CompilerGenerated]
			get
			{
				return this.<Type>k__BackingField;
			}
		}

		// Token: 0x06000ED5 RID: 3797 RVA: 0x0002F426 File Offset: 0x0002D626
		internal override BlockExpression Rewrite(ReadOnlyCollection<ParameterExpression> variables, Expression[] args)
		{
			if (args == null)
			{
				Expression.ValidateVariables(variables, "variables");
				return new ScopeWithType(variables, base.Body, this.Type);
			}
			return new ScopeWithType(base.ReuseOrValidateVariables(variables), args, this.Type);
		}

		// Token: 0x04000893 RID: 2195
		[CompilerGenerated]
		private readonly Type <Type>k__BackingField;
	}
}
