﻿using System;
using System.Reflection;

namespace System.Linq.Expressions
{
	// Token: 0x020001F6 RID: 502
	internal class MethodBinaryExpression : SimpleBinaryExpression
	{
		// Token: 0x06000C36 RID: 3126 RVA: 0x00027203 File Offset: 0x00025403
		internal MethodBinaryExpression(ExpressionType nodeType, Expression left, Expression right, Type type, MethodInfo method) : base(nodeType, left, right, type)
		{
			this._method = method;
		}

		// Token: 0x06000C37 RID: 3127 RVA: 0x00027218 File Offset: 0x00025418
		internal override MethodInfo GetMethod()
		{
			return this._method;
		}

		// Token: 0x0400085A RID: 2138
		private readonly MethodInfo _method;
	}
}
