﻿using System;

namespace System.Linq.Expressions
{
	// Token: 0x02000283 RID: 643
	internal static class Strings
	{
		// Token: 0x1700035C RID: 860
		// (get) Token: 0x06001271 RID: 4721 RVA: 0x00036975 File Offset: 0x00034B75
		internal static string ReducibleMustOverrideReduce
		{
			get
			{
				return "reducible nodes must override Expression.Reduce()";
			}
		}

		// Token: 0x1700035D RID: 861
		// (get) Token: 0x06001272 RID: 4722 RVA: 0x0003697C File Offset: 0x00034B7C
		internal static string MustReduceToDifferent
		{
			get
			{
				return "node cannot reduce to itself or null";
			}
		}

		// Token: 0x1700035E RID: 862
		// (get) Token: 0x06001273 RID: 4723 RVA: 0x00036983 File Offset: 0x00034B83
		internal static string ReducedNotCompatible
		{
			get
			{
				return "cannot assign from the reduced node type to the original node type";
			}
		}

		// Token: 0x1700035F RID: 863
		// (get) Token: 0x06001274 RID: 4724 RVA: 0x0003698A File Offset: 0x00034B8A
		internal static string SetterHasNoParams
		{
			get
			{
				return "Setter must have parameters.";
			}
		}

		// Token: 0x17000360 RID: 864
		// (get) Token: 0x06001275 RID: 4725 RVA: 0x00036991 File Offset: 0x00034B91
		internal static string PropertyCannotHaveRefType
		{
			get
			{
				return "Property cannot have a managed pointer type.";
			}
		}

		// Token: 0x17000361 RID: 865
		// (get) Token: 0x06001276 RID: 4726 RVA: 0x00036998 File Offset: 0x00034B98
		internal static string IndexesOfSetGetMustMatch
		{
			get
			{
				return "Indexing parameters of getter and setter must match.";
			}
		}

		// Token: 0x17000362 RID: 866
		// (get) Token: 0x06001277 RID: 4727 RVA: 0x0003699F File Offset: 0x00034B9F
		internal static string AccessorsCannotHaveVarArgs
		{
			get
			{
				return "Accessor method should not have VarArgs.";
			}
		}

		// Token: 0x17000363 RID: 867
		// (get) Token: 0x06001278 RID: 4728 RVA: 0x000369A6 File Offset: 0x00034BA6
		internal static string AccessorsCannotHaveByRefArgs
		{
			get
			{
				return "Accessor indexes cannot be passed ByRef.";
			}
		}

		// Token: 0x17000364 RID: 868
		// (get) Token: 0x06001279 RID: 4729 RVA: 0x000369AD File Offset: 0x00034BAD
		internal static string BoundsCannotBeLessThanOne
		{
			get
			{
				return "Bounds count cannot be less than 1";
			}
		}

		// Token: 0x17000365 RID: 869
		// (get) Token: 0x0600127A RID: 4730 RVA: 0x000369B4 File Offset: 0x00034BB4
		internal static string TypeMustNotBeByRef
		{
			get
			{
				return "Type must not be ByRef";
			}
		}

		// Token: 0x17000366 RID: 870
		// (get) Token: 0x0600127B RID: 4731 RVA: 0x000369BB File Offset: 0x00034BBB
		internal static string TypeMustNotBePointer
		{
			get
			{
				return "Type must not be a pointer type";
			}
		}

		// Token: 0x17000367 RID: 871
		// (get) Token: 0x0600127C RID: 4732 RVA: 0x000369C2 File Offset: 0x00034BC2
		internal static string SetterMustBeVoid
		{
			get
			{
				return "Setter should have void type.";
			}
		}

		// Token: 0x17000368 RID: 872
		// (get) Token: 0x0600127D RID: 4733 RVA: 0x000369C9 File Offset: 0x00034BC9
		internal static string PropertyTypeMustMatchGetter
		{
			get
			{
				return "Property type must match the value type of getter";
			}
		}

		// Token: 0x17000369 RID: 873
		// (get) Token: 0x0600127E RID: 4734 RVA: 0x000369D0 File Offset: 0x00034BD0
		internal static string PropertyTypeMustMatchSetter
		{
			get
			{
				return "Property type must match the value type of setter";
			}
		}

		// Token: 0x1700036A RID: 874
		// (get) Token: 0x0600127F RID: 4735 RVA: 0x000369D7 File Offset: 0x00034BD7
		internal static string BothAccessorsMustBeStatic
		{
			get
			{
				return "Both accessors must be static.";
			}
		}

		// Token: 0x1700036B RID: 875
		// (get) Token: 0x06001280 RID: 4736 RVA: 0x000369DE File Offset: 0x00034BDE
		internal static string OnlyStaticFieldsHaveNullInstance
		{
			get
			{
				return "Static field requires null instance, non-static field requires non-null instance.";
			}
		}

		// Token: 0x1700036C RID: 876
		// (get) Token: 0x06001281 RID: 4737 RVA: 0x000369E5 File Offset: 0x00034BE5
		internal static string OnlyStaticPropertiesHaveNullInstance
		{
			get
			{
				return "Static property requires null instance, non-static property requires non-null instance.";
			}
		}

		// Token: 0x1700036D RID: 877
		// (get) Token: 0x06001282 RID: 4738 RVA: 0x000369EC File Offset: 0x00034BEC
		internal static string OnlyStaticMethodsHaveNullInstance
		{
			get
			{
				return "Static method requires null instance, non-static method requires non-null instance.";
			}
		}

		// Token: 0x1700036E RID: 878
		// (get) Token: 0x06001283 RID: 4739 RVA: 0x000369F3 File Offset: 0x00034BF3
		internal static string PropertyTypeCannotBeVoid
		{
			get
			{
				return "Property cannot have a void type.";
			}
		}

		// Token: 0x1700036F RID: 879
		// (get) Token: 0x06001284 RID: 4740 RVA: 0x000369FA File Offset: 0x00034BFA
		internal static string InvalidUnboxType
		{
			get
			{
				return "Can only unbox from an object or interface type to a value type.";
			}
		}

		// Token: 0x17000370 RID: 880
		// (get) Token: 0x06001285 RID: 4741 RVA: 0x00036A01 File Offset: 0x00034C01
		internal static string ExpressionMustBeWriteable
		{
			get
			{
				return "Expression must be writeable";
			}
		}

		// Token: 0x17000371 RID: 881
		// (get) Token: 0x06001286 RID: 4742 RVA: 0x00036A08 File Offset: 0x00034C08
		internal static string ArgumentMustNotHaveValueType
		{
			get
			{
				return "Argument must not have a value type.";
			}
		}

		// Token: 0x17000372 RID: 882
		// (get) Token: 0x06001287 RID: 4743 RVA: 0x00036A0F File Offset: 0x00034C0F
		internal static string MustBeReducible
		{
			get
			{
				return "must be reducible node";
			}
		}

		// Token: 0x17000373 RID: 883
		// (get) Token: 0x06001288 RID: 4744 RVA: 0x00036A16 File Offset: 0x00034C16
		internal static string AllTestValuesMustHaveSameType
		{
			get
			{
				return "All test values must have the same type.";
			}
		}

		// Token: 0x17000374 RID: 884
		// (get) Token: 0x06001289 RID: 4745 RVA: 0x00036A1D File Offset: 0x00034C1D
		internal static string AllCaseBodiesMustHaveSameType
		{
			get
			{
				return "All case bodies and the default body must have the same type.";
			}
		}

		// Token: 0x17000375 RID: 885
		// (get) Token: 0x0600128A RID: 4746 RVA: 0x00036A24 File Offset: 0x00034C24
		internal static string DefaultBodyMustBeSupplied
		{
			get
			{
				return "Default body must be supplied if case bodies are not System.Void.";
			}
		}

		// Token: 0x17000376 RID: 886
		// (get) Token: 0x0600128B RID: 4747 RVA: 0x00036A2B File Offset: 0x00034C2B
		internal static string LabelMustBeVoidOrHaveExpression
		{
			get
			{
				return "Label type must be System.Void if an expression is not supplied";
			}
		}

		// Token: 0x17000377 RID: 887
		// (get) Token: 0x0600128C RID: 4748 RVA: 0x00036A32 File Offset: 0x00034C32
		internal static string LabelTypeMustBeVoid
		{
			get
			{
				return "Type must be System.Void for this label argument";
			}
		}

		// Token: 0x17000378 RID: 888
		// (get) Token: 0x0600128D RID: 4749 RVA: 0x00036A39 File Offset: 0x00034C39
		internal static string QuotedExpressionMustBeLambda
		{
			get
			{
				return "Quoted expression must be a lambda";
			}
		}

		// Token: 0x17000379 RID: 889
		// (get) Token: 0x0600128E RID: 4750 RVA: 0x00036A40 File Offset: 0x00034C40
		internal static string CollectionModifiedWhileEnumerating
		{
			get
			{
				return "Collection was modified; enumeration operation may not execute.";
			}
		}

		// Token: 0x0600128F RID: 4751 RVA: 0x00036A47 File Offset: 0x00034C47
		internal static string VariableMustNotBeByRef(object p0, object p1)
		{
			return SR.Format("Variable '{0}' uses unsupported type '{1}'. Reference types are not supported for variables.", p0, p1);
		}

		// Token: 0x1700037A RID: 890
		// (get) Token: 0x06001290 RID: 4752 RVA: 0x00036A55 File Offset: 0x00034C55
		internal static string CollectionReadOnly
		{
			get
			{
				return "Collection is read-only.";
			}
		}

		// Token: 0x06001291 RID: 4753 RVA: 0x00036A5C File Offset: 0x00034C5C
		internal static string AmbiguousMatchInExpandoObject(object p0)
		{
			return SR.Format("More than one key matching '{0}' was found in the ExpandoObject.", p0);
		}

		// Token: 0x06001292 RID: 4754 RVA: 0x00036A69 File Offset: 0x00034C69
		internal static string SameKeyExistsInExpando(object p0)
		{
			return SR.Format("An element with the same key '{0}' already exists in the ExpandoObject.", p0);
		}

		// Token: 0x06001293 RID: 4755 RVA: 0x00036A76 File Offset: 0x00034C76
		internal static string KeyDoesNotExistInExpando(object p0)
		{
			return SR.Format("The specified key '{0}' does not exist in the ExpandoObject.", p0);
		}

		// Token: 0x1700037B RID: 891
		// (get) Token: 0x06001294 RID: 4756 RVA: 0x00036A83 File Offset: 0x00034C83
		internal static string ArgCntMustBeGreaterThanNameCnt
		{
			get
			{
				return "Argument count must be greater than number of named arguments.";
			}
		}

		// Token: 0x06001295 RID: 4757 RVA: 0x00036A8A File Offset: 0x00034C8A
		internal static string InvalidMetaObjectCreated(object p0)
		{
			return SR.Format("An IDynamicMetaObjectProvider {0} created an invalid DynamicMetaObject instance.", p0);
		}

		// Token: 0x06001296 RID: 4758 RVA: 0x00036A97 File Offset: 0x00034C97
		internal static string BinderNotCompatibleWithCallSite(object p0, object p1, object p2)
		{
			return SR.Format("The result type '{0}' of the binder '{1}' is not compatible with the result type '{2}' expected by the call site.", p0, p1, p2);
		}

		// Token: 0x06001297 RID: 4759 RVA: 0x00036AA6 File Offset: 0x00034CA6
		internal static string DynamicBindingNeedsRestrictions(object p0, object p1)
		{
			return SR.Format("The result of the dynamic binding produced by the object with type '{0}' for the binder '{1}' needs at least one restriction.", p0, p1);
		}

		// Token: 0x06001298 RID: 4760 RVA: 0x00036AB4 File Offset: 0x00034CB4
		internal static string DynamicObjectResultNotAssignable(object p0, object p1, object p2, object p3)
		{
			return SR.Format("The result type '{0}' of the dynamic binding produced by the object with type '{1}' for the binder '{2}' is not compatible with the result type '{3}' expected by the call site.", new object[]
			{
				p0,
				p1,
				p2,
				p3
			});
		}

		// Token: 0x06001299 RID: 4761 RVA: 0x00036AD6 File Offset: 0x00034CD6
		internal static string DynamicBinderResultNotAssignable(object p0, object p1, object p2)
		{
			return SR.Format("The result type '{0}' of the dynamic binding produced by binder '{1}' is not compatible with the result type '{2}' expected by the call site.", p0, p1, p2);
		}

		// Token: 0x1700037C RID: 892
		// (get) Token: 0x0600129A RID: 4762 RVA: 0x00036AE5 File Offset: 0x00034CE5
		internal static string BindingCannotBeNull
		{
			get
			{
				return "Bind cannot return null.";
			}
		}

		// Token: 0x0600129B RID: 4763 RVA: 0x00036AEC File Offset: 0x00034CEC
		internal static string DuplicateVariable(object p0)
		{
			return SR.Format("Found duplicate parameter '{0}'. Each ParameterExpression in the list must be a unique object.", p0);
		}

		// Token: 0x1700037D RID: 893
		// (get) Token: 0x0600129C RID: 4764 RVA: 0x00036AF9 File Offset: 0x00034CF9
		internal static string ArgumentTypeCannotBeVoid
		{
			get
			{
				return "Argument type cannot be void";
			}
		}

		// Token: 0x0600129D RID: 4765 RVA: 0x00036B00 File Offset: 0x00034D00
		internal static string TypeParameterIsNotDelegate(object p0)
		{
			return SR.Format("Type parameter is {0}. Expected a delegate.", p0);
		}

		// Token: 0x1700037E RID: 894
		// (get) Token: 0x0600129E RID: 4766 RVA: 0x00036B0D File Offset: 0x00034D0D
		internal static string NoOrInvalidRuleProduced
		{
			get
			{
				return "No or Invalid rule produced";
			}
		}

		// Token: 0x1700037F RID: 895
		// (get) Token: 0x0600129F RID: 4767 RVA: 0x00036B14 File Offset: 0x00034D14
		internal static string TypeMustBeDerivedFromSystemDelegate
		{
			get
			{
				return "Type must be derived from System.Delegate";
			}
		}

		// Token: 0x17000380 RID: 896
		// (get) Token: 0x060012A0 RID: 4768 RVA: 0x00036B1B File Offset: 0x00034D1B
		internal static string FirstArgumentMustBeCallSite
		{
			get
			{
				return "First argument of delegate must be CallSite";
			}
		}

		// Token: 0x17000381 RID: 897
		// (get) Token: 0x060012A1 RID: 4769 RVA: 0x00036B22 File Offset: 0x00034D22
		internal static string StartEndMustBeOrdered
		{
			get
			{
				return "Start and End must be well ordered";
			}
		}

		// Token: 0x17000382 RID: 898
		// (get) Token: 0x060012A2 RID: 4770 RVA: 0x00036B29 File Offset: 0x00034D29
		internal static string FaultCannotHaveCatchOrFinally
		{
			get
			{
				return "fault cannot be used with catch or finally clauses";
			}
		}

		// Token: 0x17000383 RID: 899
		// (get) Token: 0x060012A3 RID: 4771 RVA: 0x00036B30 File Offset: 0x00034D30
		internal static string TryMustHaveCatchFinallyOrFault
		{
			get
			{
				return "try must have at least one catch, finally, or fault clause";
			}
		}

		// Token: 0x17000384 RID: 900
		// (get) Token: 0x060012A4 RID: 4772 RVA: 0x00036B37 File Offset: 0x00034D37
		internal static string BodyOfCatchMustHaveSameTypeAsBodyOfTry
		{
			get
			{
				return "Body of catch must have the same type as body of try.";
			}
		}

		// Token: 0x060012A5 RID: 4773 RVA: 0x00036B3E File Offset: 0x00034D3E
		internal static string ExtensionNodeMustOverrideProperty(object p0)
		{
			return SR.Format("Extension node must override the property {0}.", p0);
		}

		// Token: 0x060012A6 RID: 4774 RVA: 0x00036B4B File Offset: 0x00034D4B
		internal static string UserDefinedOperatorMustBeStatic(object p0)
		{
			return SR.Format("User-defined operator method '{0}' must be static.", p0);
		}

		// Token: 0x060012A7 RID: 4775 RVA: 0x00036B58 File Offset: 0x00034D58
		internal static string UserDefinedOperatorMustNotBeVoid(object p0)
		{
			return SR.Format("User-defined operator method '{0}' must not be void.", p0);
		}

		// Token: 0x060012A8 RID: 4776 RVA: 0x00036B65 File Offset: 0x00034D65
		internal static string CoercionOperatorNotDefined(object p0, object p1)
		{
			return SR.Format("No coercion operator is defined between types '{0}' and '{1}'.", p0, p1);
		}

		// Token: 0x060012A9 RID: 4777 RVA: 0x00036B73 File Offset: 0x00034D73
		internal static string UnaryOperatorNotDefined(object p0, object p1)
		{
			return SR.Format("The unary operator {0} is not defined for the type '{1}'.", p0, p1);
		}

		// Token: 0x060012AA RID: 4778 RVA: 0x00036B81 File Offset: 0x00034D81
		internal static string BinaryOperatorNotDefined(object p0, object p1, object p2)
		{
			return SR.Format("The binary operator {0} is not defined for the types '{1}' and '{2}'.", p0, p1, p2);
		}

		// Token: 0x060012AB RID: 4779 RVA: 0x00036B90 File Offset: 0x00034D90
		internal static string ReferenceEqualityNotDefined(object p0, object p1)
		{
			return SR.Format("Reference equality is not defined for the types '{0}' and '{1}'.", p0, p1);
		}

		// Token: 0x060012AC RID: 4780 RVA: 0x00036B9E File Offset: 0x00034D9E
		internal static string OperandTypesDoNotMatchParameters(object p0, object p1)
		{
			return SR.Format("The operands for operator '{0}' do not match the parameters of method '{1}'.", p0, p1);
		}

		// Token: 0x060012AD RID: 4781 RVA: 0x00036BAC File Offset: 0x00034DAC
		internal static string OverloadOperatorTypeDoesNotMatchConversionType(object p0, object p1)
		{
			return SR.Format("The return type of overload method for operator '{0}' does not match the parameter type of conversion method '{1}'.", p0, p1);
		}

		// Token: 0x17000385 RID: 901
		// (get) Token: 0x060012AE RID: 4782 RVA: 0x00036BBA File Offset: 0x00034DBA
		internal static string ConversionIsNotSupportedForArithmeticTypes
		{
			get
			{
				return "Conversion is not supported for arithmetic types without operator overloading.";
			}
		}

		// Token: 0x17000386 RID: 902
		// (get) Token: 0x060012AF RID: 4783 RVA: 0x00036BC1 File Offset: 0x00034DC1
		internal static string ArgumentMustBeArray
		{
			get
			{
				return "Argument must be array";
			}
		}

		// Token: 0x17000387 RID: 903
		// (get) Token: 0x060012B0 RID: 4784 RVA: 0x00036BC8 File Offset: 0x00034DC8
		internal static string ArgumentMustBeBoolean
		{
			get
			{
				return "Argument must be boolean";
			}
		}

		// Token: 0x060012B1 RID: 4785 RVA: 0x00036BCF File Offset: 0x00034DCF
		internal static string EqualityMustReturnBoolean(object p0)
		{
			return SR.Format("The user-defined equality method '{0}' must return a boolean value.", p0);
		}

		// Token: 0x17000388 RID: 904
		// (get) Token: 0x060012B2 RID: 4786 RVA: 0x00036BDC File Offset: 0x00034DDC
		internal static string ArgumentMustBeFieldInfoOrPropertyInfo
		{
			get
			{
				return "Argument must be either a FieldInfo or PropertyInfo";
			}
		}

		// Token: 0x17000389 RID: 905
		// (get) Token: 0x060012B3 RID: 4787 RVA: 0x00036BE3 File Offset: 0x00034DE3
		internal static string ArgumentMustBeFieldInfoOrPropertyInfoOrMethod
		{
			get
			{
				return "Argument must be either a FieldInfo, PropertyInfo or MethodInfo";
			}
		}

		// Token: 0x1700038A RID: 906
		// (get) Token: 0x060012B4 RID: 4788 RVA: 0x00036BEA File Offset: 0x00034DEA
		internal static string ArgumentMustBeInstanceMember
		{
			get
			{
				return "Argument must be an instance member";
			}
		}

		// Token: 0x1700038B RID: 907
		// (get) Token: 0x060012B5 RID: 4789 RVA: 0x00036BF1 File Offset: 0x00034DF1
		internal static string ArgumentMustBeInteger
		{
			get
			{
				return "Argument must be of an integer type";
			}
		}

		// Token: 0x1700038C RID: 908
		// (get) Token: 0x060012B6 RID: 4790 RVA: 0x00036BF8 File Offset: 0x00034DF8
		internal static string ArgumentMustBeArrayIndexType
		{
			get
			{
				return "Argument for array index must be of type Int32";
			}
		}

		// Token: 0x1700038D RID: 909
		// (get) Token: 0x060012B7 RID: 4791 RVA: 0x00036BFF File Offset: 0x00034DFF
		internal static string ArgumentMustBeSingleDimensionalArrayType
		{
			get
			{
				return "Argument must be single-dimensional, zero-based array type";
			}
		}

		// Token: 0x1700038E RID: 910
		// (get) Token: 0x060012B8 RID: 4792 RVA: 0x00036C06 File Offset: 0x00034E06
		internal static string ArgumentTypesMustMatch
		{
			get
			{
				return "Argument types do not match";
			}
		}

		// Token: 0x060012B9 RID: 4793 RVA: 0x00036C0D File Offset: 0x00034E0D
		internal static string CannotAutoInitializeValueTypeElementThroughProperty(object p0)
		{
			return SR.Format("Cannot auto initialize elements of value type through property '{0}', use assignment instead", p0);
		}

		// Token: 0x060012BA RID: 4794 RVA: 0x00036C1A File Offset: 0x00034E1A
		internal static string CannotAutoInitializeValueTypeMemberThroughProperty(object p0)
		{
			return SR.Format("Cannot auto initialize members of value type through property '{0}', use assignment instead", p0);
		}

		// Token: 0x060012BB RID: 4795 RVA: 0x00036C27 File Offset: 0x00034E27
		internal static string IncorrectTypeForTypeAs(object p0)
		{
			return SR.Format("The type used in TypeAs Expression must be of reference or nullable type, {0} is neither", p0);
		}

		// Token: 0x1700038F RID: 911
		// (get) Token: 0x060012BC RID: 4796 RVA: 0x00036C34 File Offset: 0x00034E34
		internal static string CoalesceUsedOnNonNullType
		{
			get
			{
				return "Coalesce used with type that cannot be null";
			}
		}

		// Token: 0x060012BD RID: 4797 RVA: 0x00036C3B File Offset: 0x00034E3B
		internal static string ExpressionTypeCannotInitializeArrayType(object p0, object p1)
		{
			return SR.Format("An expression of type '{0}' cannot be used to initialize an array of type '{1}'", p0, p1);
		}

		// Token: 0x060012BE RID: 4798 RVA: 0x00036C49 File Offset: 0x00034E49
		internal static string ArgumentTypeDoesNotMatchMember(object p0, object p1)
		{
			return SR.Format(" Argument type '{0}' does not match the corresponding member type '{1}'", p0, p1);
		}

		// Token: 0x060012BF RID: 4799 RVA: 0x00036C57 File Offset: 0x00034E57
		internal static string ArgumentMemberNotDeclOnType(object p0, object p1)
		{
			return SR.Format(" The member '{0}' is not declared on type '{1}' being created", p0, p1);
		}

		// Token: 0x060012C0 RID: 4800 RVA: 0x00036C65 File Offset: 0x00034E65
		internal static string ExpressionTypeDoesNotMatchReturn(object p0, object p1)
		{
			return SR.Format("Expression of type '{0}' cannot be used for return type '{1}'", p0, p1);
		}

		// Token: 0x060012C1 RID: 4801 RVA: 0x00036C73 File Offset: 0x00034E73
		internal static string ExpressionTypeDoesNotMatchAssignment(object p0, object p1)
		{
			return SR.Format("Expression of type '{0}' cannot be used for assignment to type '{1}'", p0, p1);
		}

		// Token: 0x060012C2 RID: 4802 RVA: 0x00036C81 File Offset: 0x00034E81
		internal static string ExpressionTypeDoesNotMatchLabel(object p0, object p1)
		{
			return SR.Format("Expression of type '{0}' cannot be used for label of type '{1}'", p0, p1);
		}

		// Token: 0x060012C3 RID: 4803 RVA: 0x00036C8F File Offset: 0x00034E8F
		internal static string ExpressionTypeNotInvocable(object p0)
		{
			return SR.Format("Expression of type '{0}' cannot be invoked", p0);
		}

		// Token: 0x060012C4 RID: 4804 RVA: 0x00036C9C File Offset: 0x00034E9C
		internal static string FieldNotDefinedForType(object p0, object p1)
		{
			return SR.Format("Field '{0}' is not defined for type '{1}'", p0, p1);
		}

		// Token: 0x060012C5 RID: 4805 RVA: 0x00036CAA File Offset: 0x00034EAA
		internal static string InstanceFieldNotDefinedForType(object p0, object p1)
		{
			return SR.Format("Instance field '{0}' is not defined for type '{1}'", p0, p1);
		}

		// Token: 0x060012C6 RID: 4806 RVA: 0x00036CB8 File Offset: 0x00034EB8
		internal static string FieldInfoNotDefinedForType(object p0, object p1, object p2)
		{
			return SR.Format("Field '{0}.{1}' is not defined for type '{2}'", p0, p1, p2);
		}

		// Token: 0x17000390 RID: 912
		// (get) Token: 0x060012C7 RID: 4807 RVA: 0x00036CC7 File Offset: 0x00034EC7
		internal static string IncorrectNumberOfIndexes
		{
			get
			{
				return "Incorrect number of indexes";
			}
		}

		// Token: 0x17000391 RID: 913
		// (get) Token: 0x060012C8 RID: 4808 RVA: 0x00036CCE File Offset: 0x00034ECE
		internal static string IncorrectNumberOfLambdaDeclarationParameters
		{
			get
			{
				return "Incorrect number of parameters supplied for lambda declaration";
			}
		}

		// Token: 0x17000392 RID: 914
		// (get) Token: 0x060012C9 RID: 4809 RVA: 0x00036CD5 File Offset: 0x00034ED5
		internal static string IncorrectNumberOfMembersForGivenConstructor
		{
			get
			{
				return " Incorrect number of members for constructor";
			}
		}

		// Token: 0x17000393 RID: 915
		// (get) Token: 0x060012CA RID: 4810 RVA: 0x00036CDC File Offset: 0x00034EDC
		internal static string IncorrectNumberOfArgumentsForMembers
		{
			get
			{
				return "Incorrect number of arguments for the given members ";
			}
		}

		// Token: 0x17000394 RID: 916
		// (get) Token: 0x060012CB RID: 4811 RVA: 0x00036CE3 File Offset: 0x00034EE3
		internal static string LambdaTypeMustBeDerivedFromSystemDelegate
		{
			get
			{
				return "Lambda type parameter must be derived from System.MulticastDelegate";
			}
		}

		// Token: 0x060012CC RID: 4812 RVA: 0x00036CEA File Offset: 0x00034EEA
		internal static string MemberNotFieldOrProperty(object p0)
		{
			return SR.Format("Member '{0}' not field or property", p0);
		}

		// Token: 0x060012CD RID: 4813 RVA: 0x00036CF7 File Offset: 0x00034EF7
		internal static string MethodContainsGenericParameters(object p0)
		{
			return SR.Format("Method {0} contains generic parameters", p0);
		}

		// Token: 0x060012CE RID: 4814 RVA: 0x00036D04 File Offset: 0x00034F04
		internal static string MethodIsGeneric(object p0)
		{
			return SR.Format("Method {0} is a generic method definition", p0);
		}

		// Token: 0x060012CF RID: 4815 RVA: 0x00036D11 File Offset: 0x00034F11
		internal static string MethodNotPropertyAccessor(object p0, object p1)
		{
			return SR.Format("The method '{0}.{1}' is not a property accessor", p0, p1);
		}

		// Token: 0x060012D0 RID: 4816 RVA: 0x00036D1F File Offset: 0x00034F1F
		internal static string PropertyDoesNotHaveGetter(object p0)
		{
			return SR.Format("The property '{0}' has no 'get' accessor", p0);
		}

		// Token: 0x060012D1 RID: 4817 RVA: 0x00036D2C File Offset: 0x00034F2C
		internal static string PropertyDoesNotHaveSetter(object p0)
		{
			return SR.Format("The property '{0}' has no 'set' accessor", p0);
		}

		// Token: 0x060012D2 RID: 4818 RVA: 0x00036D39 File Offset: 0x00034F39
		internal static string PropertyDoesNotHaveAccessor(object p0)
		{
			return SR.Format("The property '{0}' has no 'get' or 'set' accessors", p0);
		}

		// Token: 0x060012D3 RID: 4819 RVA: 0x00036D46 File Offset: 0x00034F46
		internal static string NotAMemberOfType(object p0, object p1)
		{
			return SR.Format("'{0}' is not a member of type '{1}'", p0, p1);
		}

		// Token: 0x060012D4 RID: 4820 RVA: 0x00036D54 File Offset: 0x00034F54
		internal static string NotAMemberOfAnyType(object p0)
		{
			return SR.Format("'{0}' is not a member of any type", p0);
		}

		// Token: 0x060012D5 RID: 4821 RVA: 0x00036D61 File Offset: 0x00034F61
		internal static string ParameterExpressionNotValidAsDelegate(object p0, object p1)
		{
			return SR.Format("ParameterExpression of type '{0}' cannot be used for delegate parameter of type '{1}'", p0, p1);
		}

		// Token: 0x060012D6 RID: 4822 RVA: 0x00036D6F File Offset: 0x00034F6F
		internal static string PropertyNotDefinedForType(object p0, object p1)
		{
			return SR.Format("Property '{0}' is not defined for type '{1}'", p0, p1);
		}

		// Token: 0x060012D7 RID: 4823 RVA: 0x00036D7D File Offset: 0x00034F7D
		internal static string InstancePropertyNotDefinedForType(object p0, object p1)
		{
			return SR.Format("Instance property '{0}' is not defined for type '{1}'", p0, p1);
		}

		// Token: 0x060012D8 RID: 4824 RVA: 0x00036D8B File Offset: 0x00034F8B
		internal static string InstancePropertyWithoutParameterNotDefinedForType(object p0, object p1)
		{
			return SR.Format("Instance property '{0}' that takes no argument is not defined for type '{1}'", p0, p1);
		}

		// Token: 0x060012D9 RID: 4825 RVA: 0x00036D99 File Offset: 0x00034F99
		internal static string InstancePropertyWithSpecifiedParametersNotDefinedForType(object p0, object p1, object p2)
		{
			return SR.Format("Instance property '{0}{1}' is not defined for type '{2}'", p0, p1, p2);
		}

		// Token: 0x060012DA RID: 4826 RVA: 0x00036DA8 File Offset: 0x00034FA8
		internal static string InstanceAndMethodTypeMismatch(object p0, object p1, object p2)
		{
			return SR.Format("Method '{0}' declared on type '{1}' cannot be called with instance of type '{2}'", p0, p1, p2);
		}

		// Token: 0x060012DB RID: 4827 RVA: 0x00036DB7 File Offset: 0x00034FB7
		internal static string TypeMissingDefaultConstructor(object p0)
		{
			return SR.Format("Type '{0}' does not have a default constructor", p0);
		}

		// Token: 0x17000395 RID: 917
		// (get) Token: 0x060012DC RID: 4828 RVA: 0x00036DC4 File Offset: 0x00034FC4
		internal static string ElementInitializerMethodNotAdd
		{
			get
			{
				return "Element initializer method must be named 'Add'";
			}
		}

		// Token: 0x060012DD RID: 4829 RVA: 0x00036DCB File Offset: 0x00034FCB
		internal static string ElementInitializerMethodNoRefOutParam(object p0, object p1)
		{
			return SR.Format("Parameter '{0}' of element initializer method '{1}' must not be a pass by reference parameter", p0, p1);
		}

		// Token: 0x17000396 RID: 918
		// (get) Token: 0x060012DE RID: 4830 RVA: 0x00036DD9 File Offset: 0x00034FD9
		internal static string ElementInitializerMethodWithZeroArgs
		{
			get
			{
				return "Element initializer method must have at least 1 parameter";
			}
		}

		// Token: 0x17000397 RID: 919
		// (get) Token: 0x060012DF RID: 4831 RVA: 0x00036DE0 File Offset: 0x00034FE0
		internal static string ElementInitializerMethodStatic
		{
			get
			{
				return "Element initializer method must be an instance method";
			}
		}

		// Token: 0x060012E0 RID: 4832 RVA: 0x00036DE7 File Offset: 0x00034FE7
		internal static string TypeNotIEnumerable(object p0)
		{
			return SR.Format("Type '{0}' is not IEnumerable", p0);
		}

		// Token: 0x060012E1 RID: 4833 RVA: 0x00036DF4 File Offset: 0x00034FF4
		internal static string UnhandledBinary(object p0)
		{
			return SR.Format("Unhandled binary: {0}", p0);
		}

		// Token: 0x17000398 RID: 920
		// (get) Token: 0x060012E2 RID: 4834 RVA: 0x00036E01 File Offset: 0x00035001
		internal static string UnhandledBinding
		{
			get
			{
				return "Unhandled binding ";
			}
		}

		// Token: 0x060012E3 RID: 4835 RVA: 0x00036E08 File Offset: 0x00035008
		internal static string UnhandledBindingType(object p0)
		{
			return SR.Format("Unhandled Binding Type: {0}", p0);
		}

		// Token: 0x060012E4 RID: 4836 RVA: 0x00036E15 File Offset: 0x00035015
		internal static string UnhandledUnary(object p0)
		{
			return SR.Format("Unhandled unary: {0}", p0);
		}

		// Token: 0x17000399 RID: 921
		// (get) Token: 0x060012E5 RID: 4837 RVA: 0x00036E22 File Offset: 0x00035022
		internal static string UnknownBindingType
		{
			get
			{
				return "Unknown binding type";
			}
		}

		// Token: 0x060012E6 RID: 4838 RVA: 0x00036E29 File Offset: 0x00035029
		internal static string UserDefinedOpMustHaveConsistentTypes(object p0, object p1)
		{
			return SR.Format("The user-defined operator method '{1}' for operator '{0}' must have identical parameter and return types.", p0, p1);
		}

		// Token: 0x060012E7 RID: 4839 RVA: 0x00036E37 File Offset: 0x00035037
		internal static string UserDefinedOpMustHaveValidReturnType(object p0, object p1)
		{
			return SR.Format("The user-defined operator method '{1}' for operator '{0}' must return the same type as its parameter or a derived type.", p0, p1);
		}

		// Token: 0x060012E8 RID: 4840 RVA: 0x00036E45 File Offset: 0x00035045
		internal static string LogicalOperatorMustHaveBooleanOperators(object p0, object p1)
		{
			return SR.Format("The user-defined operator method '{1}' for operator '{0}' must have associated boolean True and False operators.", p0, p1);
		}

		// Token: 0x060012E9 RID: 4841 RVA: 0x00036E53 File Offset: 0x00035053
		internal static string MethodWithArgsDoesNotExistOnType(object p0, object p1)
		{
			return SR.Format("No method '{0}' on type '{1}' is compatible with the supplied arguments.", p0, p1);
		}

		// Token: 0x060012EA RID: 4842 RVA: 0x00036E61 File Offset: 0x00035061
		internal static string GenericMethodWithArgsDoesNotExistOnType(object p0, object p1)
		{
			return SR.Format("No generic method '{0}' on type '{1}' is compatible with the supplied type arguments and arguments. No type arguments should be provided if the method is non-generic. ", p0, p1);
		}

		// Token: 0x060012EB RID: 4843 RVA: 0x00036E6F File Offset: 0x0003506F
		internal static string MethodWithMoreThanOneMatch(object p0, object p1)
		{
			return SR.Format("More than one method '{0}' on type '{1}' is compatible with the supplied arguments.", p0, p1);
		}

		// Token: 0x060012EC RID: 4844 RVA: 0x00036E7D File Offset: 0x0003507D
		internal static string PropertyWithMoreThanOneMatch(object p0, object p1)
		{
			return SR.Format("More than one property '{0}' on type '{1}' is compatible with the supplied arguments.", p0, p1);
		}

		// Token: 0x1700039A RID: 922
		// (get) Token: 0x060012ED RID: 4845 RVA: 0x00036E8B File Offset: 0x0003508B
		internal static string IncorrectNumberOfTypeArgsForFunc
		{
			get
			{
				return "An incorrect number of type arguments were specified for the declaration of a Func type.";
			}
		}

		// Token: 0x1700039B RID: 923
		// (get) Token: 0x060012EE RID: 4846 RVA: 0x00036E92 File Offset: 0x00035092
		internal static string IncorrectNumberOfTypeArgsForAction
		{
			get
			{
				return "An incorrect number of type arguments were specified for the declaration of an Action type.";
			}
		}

		// Token: 0x1700039C RID: 924
		// (get) Token: 0x060012EF RID: 4847 RVA: 0x00036E99 File Offset: 0x00035099
		internal static string ArgumentCannotBeOfTypeVoid
		{
			get
			{
				return "Argument type cannot be System.Void.";
			}
		}

		// Token: 0x060012F0 RID: 4848 RVA: 0x00036EA0 File Offset: 0x000350A0
		internal static string OutOfRange(object p0, object p1)
		{
			return SR.Format("{0} must be greater than or equal to {1}", p0, p1);
		}

		// Token: 0x060012F1 RID: 4849 RVA: 0x00036EAE File Offset: 0x000350AE
		internal static string LabelTargetAlreadyDefined(object p0)
		{
			return SR.Format("Cannot redefine label '{0}' in an inner block.", p0);
		}

		// Token: 0x060012F2 RID: 4850 RVA: 0x00036EBB File Offset: 0x000350BB
		internal static string LabelTargetUndefined(object p0)
		{
			return SR.Format("Cannot jump to undefined label '{0}'.", p0);
		}

		// Token: 0x1700039D RID: 925
		// (get) Token: 0x060012F3 RID: 4851 RVA: 0x00036EC8 File Offset: 0x000350C8
		internal static string ControlCannotLeaveFinally
		{
			get
			{
				return "Control cannot leave a finally block.";
			}
		}

		// Token: 0x1700039E RID: 926
		// (get) Token: 0x060012F4 RID: 4852 RVA: 0x00036ECF File Offset: 0x000350CF
		internal static string ControlCannotLeaveFilterTest
		{
			get
			{
				return "Control cannot leave a filter test.";
			}
		}

		// Token: 0x060012F5 RID: 4853 RVA: 0x00036ED6 File Offset: 0x000350D6
		internal static string AmbiguousJump(object p0)
		{
			return SR.Format("Cannot jump to ambiguous label '{0}'.", p0);
		}

		// Token: 0x1700039F RID: 927
		// (get) Token: 0x060012F6 RID: 4854 RVA: 0x00036EE3 File Offset: 0x000350E3
		internal static string ControlCannotEnterTry
		{
			get
			{
				return "Control cannot enter a try block.";
			}
		}

		// Token: 0x170003A0 RID: 928
		// (get) Token: 0x060012F7 RID: 4855 RVA: 0x00036EEA File Offset: 0x000350EA
		internal static string ControlCannotEnterExpression
		{
			get
			{
				return "Control cannot enter an expression--only statements can be jumped into.";
			}
		}

		// Token: 0x060012F8 RID: 4856 RVA: 0x00036EF1 File Offset: 0x000350F1
		internal static string NonLocalJumpWithValue(object p0)
		{
			return SR.Format("Cannot jump to non-local label '{0}' with a value. Only jumps to labels defined in outer blocks can pass values.", p0);
		}

		// Token: 0x060012F9 RID: 4857 RVA: 0x00036EFE File Offset: 0x000350FE
		internal static string InvalidLvalue(object p0)
		{
			return SR.Format("Invalid lvalue for assignment: {0}.", p0);
		}

		// Token: 0x060012FA RID: 4858 RVA: 0x00036F0B File Offset: 0x0003510B
		internal static string UndefinedVariable(object p0, object p1, object p2)
		{
			return SR.Format("variable '{0}' of type '{1}' referenced from scope '{2}', but it is not defined", p0, p1, p2);
		}

		// Token: 0x060012FB RID: 4859 RVA: 0x00036F1A File Offset: 0x0003511A
		internal static string CannotCloseOverByRef(object p0, object p1)
		{
			return SR.Format("Cannot close over byref parameter '{0}' referenced in lambda '{1}'", p0, p1);
		}

		// Token: 0x060012FC RID: 4860 RVA: 0x00036F28 File Offset: 0x00035128
		internal static string UnexpectedVarArgsCall(object p0)
		{
			return SR.Format("Unexpected VarArgs call to method '{0}'", p0);
		}

		// Token: 0x170003A1 RID: 929
		// (get) Token: 0x060012FD RID: 4861 RVA: 0x00036F35 File Offset: 0x00035135
		internal static string RethrowRequiresCatch
		{
			get
			{
				return "Rethrow statement is valid only inside a Catch block.";
			}
		}

		// Token: 0x170003A2 RID: 930
		// (get) Token: 0x060012FE RID: 4862 RVA: 0x00036F3C File Offset: 0x0003513C
		internal static string TryNotAllowedInFilter
		{
			get
			{
				return "Try expression is not allowed inside a filter body.";
			}
		}

		// Token: 0x060012FF RID: 4863 RVA: 0x00036F43 File Offset: 0x00035143
		internal static string MustRewriteToSameNode(object p0, object p1, object p2)
		{
			return SR.Format("When called from '{0}', rewriting a node of type '{1}' must return a non-null value of the same type. Alternatively, override '{2}' and change it to not visit children of this type.", p0, p1, p2);
		}

		// Token: 0x06001300 RID: 4864 RVA: 0x00036F52 File Offset: 0x00035152
		internal static string MustRewriteChildToSameType(object p0, object p1, object p2)
		{
			return SR.Format("Rewriting child expression from type '{0}' to type '{1}' is not allowed, because it would change the meaning of the operation. If this is intentional, override '{2}' and change it to allow this rewrite.", p0, p1, p2);
		}

		// Token: 0x06001301 RID: 4865 RVA: 0x00036F61 File Offset: 0x00035161
		internal static string MustRewriteWithoutMethod(object p0, object p1)
		{
			return SR.Format("Rewritten expression calls operator method '{0}', but the original node had no operator method. If this is intentional, override '{1}' and change it to allow this rewrite.", p0, p1);
		}

		// Token: 0x06001302 RID: 4866 RVA: 0x00036F6F File Offset: 0x0003516F
		internal static string TryNotSupportedForMethodsWithRefArgs(object p0)
		{
			return SR.Format("TryExpression is not supported as an argument to method '{0}' because it has an argument with by-ref type. Construct the tree so the TryExpression is not nested inside of this expression.", p0);
		}

		// Token: 0x06001303 RID: 4867 RVA: 0x00036F7C File Offset: 0x0003517C
		internal static string TryNotSupportedForValueTypeInstances(object p0)
		{
			return SR.Format("TryExpression is not supported as a child expression when accessing a member on type '{0}' because it is a value type. Construct the tree so the TryExpression is not nested inside of this expression.", p0);
		}

		// Token: 0x06001304 RID: 4868 RVA: 0x00036F89 File Offset: 0x00035189
		internal static string TestValueTypeDoesNotMatchComparisonMethodParameter(object p0, object p1)
		{
			return SR.Format("Test value of type '{0}' cannot be used for the comparison method parameter of type '{1}'", p0, p1);
		}

		// Token: 0x06001305 RID: 4869 RVA: 0x00036F97 File Offset: 0x00035197
		internal static string SwitchValueTypeDoesNotMatchComparisonMethodParameter(object p0, object p1)
		{
			return SR.Format("Switch value of type '{0}' cannot be used for the comparison method parameter of type '{1}'", p0, p1);
		}

		// Token: 0x170003A3 RID: 931
		// (get) Token: 0x06001306 RID: 4870 RVA: 0x00036FA5 File Offset: 0x000351A5
		internal static string NonStaticConstructorRequired
		{
			get
			{
				return "The constructor should not be static";
			}
		}

		// Token: 0x170003A4 RID: 932
		// (get) Token: 0x06001307 RID: 4871 RVA: 0x00036FAC File Offset: 0x000351AC
		internal static string NonAbstractConstructorRequired
		{
			get
			{
				return "Can't compile a NewExpression with a constructor declared on an abstract class";
			}
		}

		// Token: 0x170003A5 RID: 933
		// (get) Token: 0x06001308 RID: 4872 RVA: 0x00036FB3 File Offset: 0x000351B3
		internal static string ExpressionMustBeReadable
		{
			get
			{
				return "Expression must be readable";
			}
		}

		// Token: 0x06001309 RID: 4873 RVA: 0x00036FBA File Offset: 0x000351BA
		internal static string ExpressionTypeDoesNotMatchConstructorParameter(object p0, object p1)
		{
			return SR.Format("Expression of type '{0}' cannot be used for constructor parameter of type '{1}'", p0, p1);
		}

		// Token: 0x170003A6 RID: 934
		// (get) Token: 0x0600130A RID: 4874 RVA: 0x00036FC8 File Offset: 0x000351C8
		internal static string EnumerationIsDone
		{
			get
			{
				return "Enumeration has either not started or has already finished.";
			}
		}

		// Token: 0x0600130B RID: 4875 RVA: 0x00036FCF File Offset: 0x000351CF
		internal static string TypeContainsGenericParameters(object p0)
		{
			return SR.Format("Type {0} contains generic parameters", p0);
		}

		// Token: 0x0600130C RID: 4876 RVA: 0x00036FDC File Offset: 0x000351DC
		internal static string TypeIsGeneric(object p0)
		{
			return SR.Format("Type {0} is a generic type definition", p0);
		}

		// Token: 0x170003A7 RID: 935
		// (get) Token: 0x0600130D RID: 4877 RVA: 0x00036FE9 File Offset: 0x000351E9
		internal static string InvalidArgumentValue
		{
			get
			{
				return "Invalid argument value";
			}
		}

		// Token: 0x170003A8 RID: 936
		// (get) Token: 0x0600130E RID: 4878 RVA: 0x00036FF0 File Offset: 0x000351F0
		internal static string NonEmptyCollectionRequired
		{
			get
			{
				return "Non-empty collection required";
			}
		}

		// Token: 0x0600130F RID: 4879 RVA: 0x00036FF7 File Offset: 0x000351F7
		internal static string InvalidNullValue(object p0)
		{
			return SR.Format("The value null is not of type '{0}' and cannot be used in this collection.", p0);
		}

		// Token: 0x06001310 RID: 4880 RVA: 0x00037004 File Offset: 0x00035204
		internal static string InvalidObjectType(object p0, object p1)
		{
			return SR.Format("The value '{0}' is not of type '{1}' and cannot be used in this collection.", p0, p1);
		}

		// Token: 0x06001311 RID: 4881 RVA: 0x00037012 File Offset: 0x00035212
		internal static string ExpressionTypeDoesNotMatchMethodParameter(object p0, object p1, object p2)
		{
			return SR.Format("Expression of type '{0}' cannot be used for parameter of type '{1}' of method '{2}'", p0, p1, p2);
		}

		// Token: 0x06001312 RID: 4882 RVA: 0x00037021 File Offset: 0x00035221
		internal static string ExpressionTypeDoesNotMatchParameter(object p0, object p1)
		{
			return SR.Format("Expression of type '{0}' cannot be used for parameter of type '{1}'", p0, p1);
		}

		// Token: 0x06001313 RID: 4883 RVA: 0x0003702F File Offset: 0x0003522F
		internal static string IncorrectNumberOfMethodCallArguments(object p0)
		{
			return SR.Format("Incorrect number of arguments supplied for call to method '{0}'", p0);
		}

		// Token: 0x170003A9 RID: 937
		// (get) Token: 0x06001314 RID: 4884 RVA: 0x0003703C File Offset: 0x0003523C
		internal static string IncorrectNumberOfLambdaArguments
		{
			get
			{
				return "Incorrect number of arguments supplied for lambda invocation";
			}
		}

		// Token: 0x170003AA RID: 938
		// (get) Token: 0x06001315 RID: 4885 RVA: 0x00037043 File Offset: 0x00035243
		internal static string IncorrectNumberOfConstructorArguments
		{
			get
			{
				return "Incorrect number of arguments for constructor";
			}
		}
	}
}
