﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Represents a catch statement in a try block.</summary>
	// Token: 0x02000221 RID: 545
	[DebuggerTypeProxy(typeof(Expression.CatchBlockProxy))]
	public sealed class CatchBlock
	{
		// Token: 0x06000EEB RID: 3819 RVA: 0x0002F62E File Offset: 0x0002D82E
		internal CatchBlock(Type test, ParameterExpression variable, Expression body, Expression filter)
		{
			this.Test = test;
			this.Variable = variable;
			this.Body = body;
			this.Filter = filter;
		}

		/// <summary>Gets a reference to the <see cref="T:System.Exception" /> object caught by this handler.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.ParameterExpression" /> object representing a reference to the <see cref="T:System.Exception" /> object caught by this handler.</returns>
		// Token: 0x170002AD RID: 685
		// (get) Token: 0x06000EEC RID: 3820 RVA: 0x0002F653 File Offset: 0x0002D853
		public ParameterExpression Variable
		{
			[CompilerGenerated]
			get
			{
				return this.<Variable>k__BackingField;
			}
		}

		/// <summary>Gets the type of <see cref="T:System.Exception" /> this handler catches.</summary>
		/// <returns>The <see cref="T:System.Type" /> object representing the type of <see cref="T:System.Exception" /> this handler catches.</returns>
		// Token: 0x170002AE RID: 686
		// (get) Token: 0x06000EED RID: 3821 RVA: 0x0002F65B File Offset: 0x0002D85B
		public Type Test
		{
			[CompilerGenerated]
			get
			{
				return this.<Test>k__BackingField;
			}
		}

		/// <summary>Gets the body of the catch block.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.Expression" /> object representing the catch body.</returns>
		// Token: 0x170002AF RID: 687
		// (get) Token: 0x06000EEE RID: 3822 RVA: 0x0002F663 File Offset: 0x0002D863
		public Expression Body
		{
			[CompilerGenerated]
			get
			{
				return this.<Body>k__BackingField;
			}
		}

		/// <summary>Gets the body of the <see cref="T:System.Linq.Expressions.CatchBlock" /> filter.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.Expression" /> object representing the body of the <see cref="T:System.Linq.Expressions.CatchBlock" /> filter.</returns>
		// Token: 0x170002B0 RID: 688
		// (get) Token: 0x06000EEF RID: 3823 RVA: 0x0002F66B File Offset: 0x0002D86B
		public Expression Filter
		{
			[CompilerGenerated]
			get
			{
				return this.<Filter>k__BackingField;
			}
		}

		/// <summary>Returns a <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.</summary>
		/// <returns>A <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.</returns>
		// Token: 0x06000EF0 RID: 3824 RVA: 0x0002F673 File Offset: 0x0002D873
		public override string ToString()
		{
			return ExpressionStringBuilder.CatchBlockToString(this);
		}

		/// <summary>Creates a new expression that is like this one, but using the supplied children. If all of the children are the same, it will return this expression.</summary>
		/// <param name="variable">The <see cref="P:System.Linq.Expressions.CatchBlock.Variable" /> property of the result.</param>
		/// <param name="filter">The <see cref="P:System.Linq.Expressions.CatchBlock.Filter" /> property of the result.</param>
		/// <param name="body">The <see cref="P:System.Linq.Expressions.CatchBlock.Body" /> property of the result.</param>
		/// <returns>This expression if no children are changed or an expression with the updated children.</returns>
		// Token: 0x06000EF1 RID: 3825 RVA: 0x0002F67B File Offset: 0x0002D87B
		public CatchBlock Update(ParameterExpression variable, Expression filter, Expression body)
		{
			if (variable == this.Variable && filter == this.Filter && body == this.Body)
			{
				return this;
			}
			return Expression.MakeCatchBlock(this.Test, variable, body, filter);
		}

		// Token: 0x06000EF2 RID: 3826 RVA: 0x0000220F File Offset: 0x0000040F
		internal CatchBlock()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0400089A RID: 2202
		[CompilerGenerated]
		private readonly ParameterExpression <Variable>k__BackingField;

		// Token: 0x0400089B RID: 2203
		[CompilerGenerated]
		private readonly Type <Test>k__BackingField;

		// Token: 0x0400089C RID: 2204
		[CompilerGenerated]
		private readonly Expression <Body>k__BackingField;

		// Token: 0x0400089D RID: 2205
		[CompilerGenerated]
		private readonly Expression <Filter>k__BackingField;
	}
}
