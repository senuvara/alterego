﻿using System;
using System.Collections.ObjectModel;

namespace System.Linq.Expressions
{
	// Token: 0x02000276 RID: 630
	internal sealed class NewArrayBoundsExpression : NewArrayExpression
	{
		// Token: 0x06001239 RID: 4665 RVA: 0x00036476 File Offset: 0x00034676
		internal NewArrayBoundsExpression(Type type, ReadOnlyCollection<Expression> expressions) : base(type, expressions)
		{
		}

		// Token: 0x1700034B RID: 843
		// (get) Token: 0x0600123A RID: 4666 RVA: 0x00036484 File Offset: 0x00034684
		public sealed override ExpressionType NodeType
		{
			get
			{
				return ExpressionType.NewArrayBounds;
			}
		}
	}
}
