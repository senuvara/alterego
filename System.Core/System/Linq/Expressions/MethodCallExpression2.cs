﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;
using System.Reflection;

namespace System.Linq.Expressions
{
	// Token: 0x0200026C RID: 620
	internal sealed class MethodCallExpression2 : MethodCallExpression, IArgumentProvider
	{
		// Token: 0x06001200 RID: 4608 RVA: 0x00035ABA File Offset: 0x00033CBA
		public MethodCallExpression2(MethodInfo method, Expression arg0, Expression arg1) : base(method)
		{
			this._arg0 = arg0;
			this._arg1 = arg1;
		}

		// Token: 0x06001201 RID: 4609 RVA: 0x00035AD1 File Offset: 0x00033CD1
		public override Expression GetArgument(int index)
		{
			if (index == 0)
			{
				return ExpressionUtils.ReturnObject<Expression>(this._arg0);
			}
			if (index != 1)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			return this._arg1;
		}

		// Token: 0x17000340 RID: 832
		// (get) Token: 0x06001202 RID: 4610 RVA: 0x0002EE24 File Offset: 0x0002D024
		public override int ArgumentCount
		{
			get
			{
				return 2;
			}
		}

		// Token: 0x06001203 RID: 4611 RVA: 0x00035AFC File Offset: 0x00033CFC
		internal override bool SameArguments(ICollection<Expression> arguments)
		{
			if (arguments != null && arguments.Count == 2)
			{
				ReadOnlyCollection<Expression> readOnlyCollection = this._arg0 as ReadOnlyCollection<Expression>;
				if (readOnlyCollection != null)
				{
					return ExpressionUtils.SameElements<Expression>(arguments, readOnlyCollection);
				}
				using (IEnumerator<Expression> enumerator = arguments.GetEnumerator())
				{
					enumerator.MoveNext();
					if (enumerator.Current == this._arg0)
					{
						enumerator.MoveNext();
						return enumerator.Current == this._arg1;
					}
				}
				return false;
			}
			return false;
		}

		// Token: 0x06001204 RID: 4612 RVA: 0x00035B80 File Offset: 0x00033D80
		internal override ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			return ExpressionUtils.ReturnReadOnly(this, ref this._arg0);
		}

		// Token: 0x06001205 RID: 4613 RVA: 0x00035B8E File Offset: 0x00033D8E
		internal override MethodCallExpression Rewrite(Expression instance, IReadOnlyList<Expression> args)
		{
			if (args != null)
			{
				return Expression.Call(base.Method, args[0], args[1]);
			}
			return Expression.Call(base.Method, ExpressionUtils.ReturnObject<Expression>(this._arg0), this._arg1);
		}

		// Token: 0x0400096F RID: 2415
		private object _arg0;

		// Token: 0x04000970 RID: 2416
		private readonly Expression _arg1;
	}
}
