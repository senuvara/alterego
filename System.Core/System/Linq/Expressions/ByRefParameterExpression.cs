﻿using System;

namespace System.Linq.Expressions
{
	// Token: 0x0200027A RID: 634
	internal sealed class ByRefParameterExpression : TypedParameterExpression
	{
		// Token: 0x06001251 RID: 4689 RVA: 0x000366B3 File Offset: 0x000348B3
		internal ByRefParameterExpression(Type type, string name) : base(type, name)
		{
		}

		// Token: 0x06001252 RID: 4690 RVA: 0x00009CDF File Offset: 0x00007EDF
		internal override bool GetIsByRef()
		{
			return true;
		}
	}
}
