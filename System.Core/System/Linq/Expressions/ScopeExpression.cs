﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;

namespace System.Linq.Expressions
{
	// Token: 0x0200021B RID: 539
	internal class ScopeExpression : BlockExpression
	{
		// Token: 0x06000EC0 RID: 3776 RVA: 0x0002F260 File Offset: 0x0002D460
		internal ScopeExpression(IReadOnlyList<ParameterExpression> variables)
		{
			this._variables = variables;
		}

		// Token: 0x06000EC1 RID: 3777 RVA: 0x0002F26F File Offset: 0x0002D46F
		internal override bool SameVariables(ICollection<ParameterExpression> variables)
		{
			return ExpressionUtils.SameElements<ParameterExpression>(variables, this._variables);
		}

		// Token: 0x06000EC2 RID: 3778 RVA: 0x0002F27D File Offset: 0x0002D47D
		internal override ReadOnlyCollection<ParameterExpression> GetOrMakeVariables()
		{
			return ExpressionUtils.ReturnReadOnly<ParameterExpression>(ref this._variables);
		}

		// Token: 0x170002A3 RID: 675
		// (get) Token: 0x06000EC3 RID: 3779 RVA: 0x0002F28A File Offset: 0x0002D48A
		protected IReadOnlyList<ParameterExpression> VariablesList
		{
			get
			{
				return this._variables;
			}
		}

		// Token: 0x06000EC4 RID: 3780 RVA: 0x0002F292 File Offset: 0x0002D492
		internal IReadOnlyList<ParameterExpression> ReuseOrValidateVariables(ReadOnlyCollection<ParameterExpression> variables)
		{
			if (variables != null && variables != this.VariablesList)
			{
				Expression.ValidateVariables(variables, "variables");
				return variables;
			}
			return this.VariablesList;
		}

		// Token: 0x04000890 RID: 2192
		private IReadOnlyList<ParameterExpression> _variables;
	}
}
