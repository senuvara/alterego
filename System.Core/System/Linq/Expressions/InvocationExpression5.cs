﻿using System;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;

namespace System.Linq.Expressions
{
	// Token: 0x0200024F RID: 591
	internal sealed class InvocationExpression5 : InvocationExpression
	{
		// Token: 0x0600113C RID: 4412 RVA: 0x00034CEB File Offset: 0x00032EEB
		public InvocationExpression5(Expression lambda, Type returnType, Expression arg0, Expression arg1, Expression arg2, Expression arg3, Expression arg4) : base(lambda, returnType)
		{
			this._arg0 = arg0;
			this._arg1 = arg1;
			this._arg2 = arg2;
			this._arg3 = arg3;
			this._arg4 = arg4;
		}

		// Token: 0x0600113D RID: 4413 RVA: 0x00034D1C File Offset: 0x00032F1C
		internal override ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			return ExpressionUtils.ReturnReadOnly(this, ref this._arg0);
		}

		// Token: 0x0600113E RID: 4414 RVA: 0x00034D2C File Offset: 0x00032F2C
		public override Expression GetArgument(int index)
		{
			switch (index)
			{
			case 0:
				return ExpressionUtils.ReturnObject<Expression>(this._arg0);
			case 1:
				return this._arg1;
			case 2:
				return this._arg2;
			case 3:
				return this._arg3;
			case 4:
				return this._arg4;
			default:
				throw new ArgumentOutOfRangeException("index");
			}
		}

		// Token: 0x17000300 RID: 768
		// (get) Token: 0x0600113F RID: 4415 RVA: 0x0002F1EC File Offset: 0x0002D3EC
		public override int ArgumentCount
		{
			get
			{
				return 5;
			}
		}

		// Token: 0x06001140 RID: 4416 RVA: 0x00034D88 File Offset: 0x00032F88
		internal override InvocationExpression Rewrite(Expression lambda, Expression[] arguments)
		{
			if (arguments != null)
			{
				return Expression.Invoke(lambda, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
			}
			return Expression.Invoke(lambda, ExpressionUtils.ReturnObject<Expression>(this._arg0), this._arg1, this._arg2, this._arg3, this._arg4);
		}

		// Token: 0x04000944 RID: 2372
		private object _arg0;

		// Token: 0x04000945 RID: 2373
		private readonly Expression _arg1;

		// Token: 0x04000946 RID: 2374
		private readonly Expression _arg2;

		// Token: 0x04000947 RID: 2375
		private readonly Expression _arg3;

		// Token: 0x04000948 RID: 2376
		private readonly Expression _arg4;
	}
}
