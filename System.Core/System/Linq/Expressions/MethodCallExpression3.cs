﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;
using System.Reflection;

namespace System.Linq.Expressions
{
	// Token: 0x0200026D RID: 621
	internal sealed class MethodCallExpression3 : MethodCallExpression, IArgumentProvider
	{
		// Token: 0x06001206 RID: 4614 RVA: 0x00035BC9 File Offset: 0x00033DC9
		public MethodCallExpression3(MethodInfo method, Expression arg0, Expression arg1, Expression arg2) : base(method)
		{
			this._arg0 = arg0;
			this._arg1 = arg1;
			this._arg2 = arg2;
		}

		// Token: 0x06001207 RID: 4615 RVA: 0x00035BE8 File Offset: 0x00033DE8
		public override Expression GetArgument(int index)
		{
			switch (index)
			{
			case 0:
				return ExpressionUtils.ReturnObject<Expression>(this._arg0);
			case 1:
				return this._arg1;
			case 2:
				return this._arg2;
			default:
				throw new ArgumentOutOfRangeException("index");
			}
		}

		// Token: 0x17000341 RID: 833
		// (get) Token: 0x06001208 RID: 4616 RVA: 0x0002EF32 File Offset: 0x0002D132
		public override int ArgumentCount
		{
			get
			{
				return 3;
			}
		}

		// Token: 0x06001209 RID: 4617 RVA: 0x00035C24 File Offset: 0x00033E24
		internal override bool SameArguments(ICollection<Expression> arguments)
		{
			if (arguments != null && arguments.Count == 3)
			{
				ReadOnlyCollection<Expression> readOnlyCollection = this._arg0 as ReadOnlyCollection<Expression>;
				if (readOnlyCollection != null)
				{
					return ExpressionUtils.SameElements<Expression>(arguments, readOnlyCollection);
				}
				using (IEnumerator<Expression> enumerator = arguments.GetEnumerator())
				{
					enumerator.MoveNext();
					if (enumerator.Current == this._arg0)
					{
						enumerator.MoveNext();
						if (enumerator.Current == this._arg1)
						{
							enumerator.MoveNext();
							return enumerator.Current == this._arg2;
						}
					}
				}
				return false;
			}
			return false;
		}

		// Token: 0x0600120A RID: 4618 RVA: 0x00035CBC File Offset: 0x00033EBC
		internal override ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			return ExpressionUtils.ReturnReadOnly(this, ref this._arg0);
		}

		// Token: 0x0600120B RID: 4619 RVA: 0x00035CCC File Offset: 0x00033ECC
		internal override MethodCallExpression Rewrite(Expression instance, IReadOnlyList<Expression> args)
		{
			if (args != null)
			{
				return Expression.Call(base.Method, args[0], args[1], args[2]);
			}
			return Expression.Call(base.Method, ExpressionUtils.ReturnObject<Expression>(this._arg0), this._arg1, this._arg2);
		}

		// Token: 0x04000971 RID: 2417
		private object _arg0;

		// Token: 0x04000972 RID: 2418
		private readonly Expression _arg1;

		// Token: 0x04000973 RID: 2419
		private readonly Expression _arg2;
	}
}
