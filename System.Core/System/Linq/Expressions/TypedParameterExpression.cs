﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x0200027B RID: 635
	internal class TypedParameterExpression : ParameterExpression
	{
		// Token: 0x06001253 RID: 4691 RVA: 0x000366BD File Offset: 0x000348BD
		internal TypedParameterExpression(Type type, string name) : base(name)
		{
			this.Type = type;
		}

		// Token: 0x17000357 RID: 855
		// (get) Token: 0x06001254 RID: 4692 RVA: 0x000366CD File Offset: 0x000348CD
		public sealed override Type Type
		{
			[CompilerGenerated]
			get
			{
				return this.<Type>k__BackingField;
			}
		}

		// Token: 0x0400098A RID: 2442
		[CompilerGenerated]
		private readonly Type <Type>k__BackingField;
	}
}
