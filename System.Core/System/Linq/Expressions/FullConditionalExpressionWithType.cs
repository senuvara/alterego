﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x02000227 RID: 551
	internal sealed class FullConditionalExpressionWithType : FullConditionalExpression
	{
		// Token: 0x06000F04 RID: 3844 RVA: 0x0002F880 File Offset: 0x0002DA80
		internal FullConditionalExpressionWithType(Expression test, Expression ifTrue, Expression ifFalse, Type type) : base(test, ifTrue, ifFalse)
		{
			this.Type = type;
		}

		// Token: 0x170002B6 RID: 694
		// (get) Token: 0x06000F05 RID: 3845 RVA: 0x0002F893 File Offset: 0x0002DA93
		public sealed override Type Type
		{
			[CompilerGenerated]
			get
			{
				return this.<Type>k__BackingField;
			}
		}

		// Token: 0x040008A6 RID: 2214
		[CompilerGenerated]
		private readonly Type <Type>k__BackingField;
	}
}
