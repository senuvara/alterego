﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Dynamic.Utils;
using System.Reflection;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq.Expressions
{
	/// <summary>Represents accessing a field or property.</summary>
	// Token: 0x02000260 RID: 608
	[DebuggerTypeProxy(typeof(Expression.MemberExpressionProxy))]
	public class MemberExpression : Expression
	{
		/// <summary>Gets the field or property to be accessed.</summary>
		/// <returns>The <see cref="T:System.Reflection.MemberInfo" /> that represents the field or property to be accessed.</returns>
		// Token: 0x1700032A RID: 810
		// (get) Token: 0x060011AF RID: 4527 RVA: 0x00035538 File Offset: 0x00033738
		public MemberInfo Member
		{
			get
			{
				return this.GetMember();
			}
		}

		/// <summary>Gets the containing object of the field or property.</summary>
		/// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> that represents the containing object of the field or property.</returns>
		// Token: 0x1700032B RID: 811
		// (get) Token: 0x060011B0 RID: 4528 RVA: 0x00035540 File Offset: 0x00033740
		public Expression Expression
		{
			[CompilerGenerated]
			get
			{
				return this.<Expression>k__BackingField;
			}
		}

		// Token: 0x060011B1 RID: 4529 RVA: 0x00035548 File Offset: 0x00033748
		internal MemberExpression(Expression expression)
		{
			this.Expression = expression;
		}

		// Token: 0x060011B2 RID: 4530 RVA: 0x00035557 File Offset: 0x00033757
		internal static PropertyExpression Make(Expression expression, PropertyInfo property)
		{
			return new PropertyExpression(expression, property);
		}

		// Token: 0x060011B3 RID: 4531 RVA: 0x00035560 File Offset: 0x00033760
		internal static FieldExpression Make(Expression expression, FieldInfo field)
		{
			return new FieldExpression(expression, field);
		}

		// Token: 0x060011B4 RID: 4532 RVA: 0x0003556C File Offset: 0x0003376C
		internal static MemberExpression Make(Expression expression, MemberInfo member)
		{
			FieldInfo fieldInfo = member as FieldInfo;
			if (!(fieldInfo == null))
			{
				return MemberExpression.Make(expression, fieldInfo);
			}
			return MemberExpression.Make(expression, (PropertyInfo)member);
		}

		/// <summary>Returns the node type of this <see cref="P:System.Linq.Expressions.MemberExpression.Expression" />.</summary>
		/// <returns>The <see cref="T:System.Linq.Expressions.ExpressionType" /> that represents this expression.</returns>
		// Token: 0x1700032C RID: 812
		// (get) Token: 0x060011B5 RID: 4533 RVA: 0x0003559D File Offset: 0x0003379D
		public sealed override ExpressionType NodeType
		{
			get
			{
				return ExpressionType.MemberAccess;
			}
		}

		// Token: 0x060011B6 RID: 4534 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		internal virtual MemberInfo GetMember()
		{
			throw ContractUtils.Unreachable;
		}

		/// <summary>Dispatches to the specific visit method for this node type. For example, <see cref="T:System.Linq.Expressions.MethodCallExpression" /> calls the <see cref="M:System.Linq.Expressions.ExpressionVisitor.VisitMethodCall(System.Linq.Expressions.MethodCallExpression)" />.</summary>
		/// <param name="visitor">The visitor to visit this node with.</param>
		/// <returns>The result of visiting this node.</returns>
		// Token: 0x060011B7 RID: 4535 RVA: 0x000355A1 File Offset: 0x000337A1
		protected internal override Expression Accept(ExpressionVisitor visitor)
		{
			return visitor.VisitMember(this);
		}

		/// <summary>Creates a new expression that is like this one, but using the supplied children. If all of the children are the same, it will return this expression.</summary>
		/// <param name="expression">The <see cref="P:System.Linq.Expressions.MemberExpression.Expression" /> property of the result.</param>
		/// <returns>This expression if no children are changed or an expression with the updated children.</returns>
		// Token: 0x060011B8 RID: 4536 RVA: 0x000355AA File Offset: 0x000337AA
		public MemberExpression Update(Expression expression)
		{
			if (expression == this.Expression)
			{
				return this;
			}
			return Expression.MakeMemberAccess(expression, this.Member);
		}

		// Token: 0x060011B9 RID: 4537 RVA: 0x0000220F File Offset: 0x0000040F
		internal MemberExpression()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000963 RID: 2403
		[CompilerGenerated]
		private readonly Expression <Expression>k__BackingField;
	}
}
