﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;
using System.Reflection;

namespace System.Linq.Expressions
{
	// Token: 0x02000270 RID: 624
	internal sealed class InstanceMethodCallExpression0 : InstanceMethodCallExpression, IArgumentProvider
	{
		// Token: 0x06001218 RID: 4632 RVA: 0x0003608D File Offset: 0x0003428D
		public InstanceMethodCallExpression0(MethodInfo method, Expression instance) : base(method, instance)
		{
		}

		// Token: 0x06001219 RID: 4633 RVA: 0x00034AA5 File Offset: 0x00032CA5
		public override Expression GetArgument(int index)
		{
			throw new ArgumentOutOfRangeException("index");
		}

		// Token: 0x17000344 RID: 836
		// (get) Token: 0x0600121A RID: 4634 RVA: 0x00002285 File Offset: 0x00000485
		public override int ArgumentCount
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x0600121B RID: 4635 RVA: 0x00034A9E File Offset: 0x00032C9E
		internal override ReadOnlyCollection<Expression> GetOrMakeArguments()
		{
			return EmptyReadOnlyCollection<Expression>.Instance;
		}

		// Token: 0x0600121C RID: 4636 RVA: 0x000359D9 File Offset: 0x00033BD9
		internal override bool SameArguments(ICollection<Expression> arguments)
		{
			return arguments == null || arguments.Count == 0;
		}

		// Token: 0x0600121D RID: 4637 RVA: 0x00036097 File Offset: 0x00034297
		internal override MethodCallExpression Rewrite(Expression instance, IReadOnlyList<Expression> args)
		{
			return Expression.Call(instance, base.Method);
		}
	}
}
