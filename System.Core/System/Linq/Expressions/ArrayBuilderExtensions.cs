﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x02000222 RID: 546
	internal static class ArrayBuilderExtensions
	{
		// Token: 0x06000EF3 RID: 3827 RVA: 0x0002F6A8 File Offset: 0x0002D8A8
		public static ReadOnlyCollection<T> ToReadOnly<T>(this ArrayBuilder<T> builder)
		{
			return new TrueReadOnlyCollection<T>(builder.ToArray());
		}
	}
}
