﻿using System;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x02000278 RID: 632
	internal sealed class NewValueTypeExpression : NewExpression
	{
		// Token: 0x06001246 RID: 4678 RVA: 0x00036531 File Offset: 0x00034731
		internal NewValueTypeExpression(Type type, ReadOnlyCollection<Expression> arguments, ReadOnlyCollection<MemberInfo> members) : base(null, arguments, members)
		{
			this.Type = type;
		}

		// Token: 0x17000352 RID: 850
		// (get) Token: 0x06001247 RID: 4679 RVA: 0x00036543 File Offset: 0x00034743
		public sealed override Type Type
		{
			[CompilerGenerated]
			get
			{
				return this.<Type>k__BackingField;
			}
		}

		// Token: 0x04000988 RID: 2440
		[CompilerGenerated]
		private readonly Type <Type>k__BackingField;
	}
}
