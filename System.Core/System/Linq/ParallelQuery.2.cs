﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Parallel;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq
{
	/// <summary>Represents a parallel sequence.</summary>
	/// <typeparam name="TSource">The type of element in the source sequence.</typeparam>
	// Token: 0x0200007C RID: 124
	public class ParallelQuery<TSource> : ParallelQuery, IEnumerable<TSource>, IEnumerable
	{
		// Token: 0x060002F4 RID: 756 RVA: 0x000079BF File Offset: 0x00005BBF
		internal ParallelQuery(QuerySettings settings) : base(settings)
		{
		}

		// Token: 0x060002F5 RID: 757 RVA: 0x000079C8 File Offset: 0x00005BC8
		internal sealed override ParallelQuery<TCastTo> Cast<TCastTo>()
		{
			return from elem in this
			select (TCastTo)((object)elem);
		}

		// Token: 0x060002F6 RID: 758 RVA: 0x000079F0 File Offset: 0x00005BF0
		internal sealed override ParallelQuery<TCastTo> OfType<TCastTo>()
		{
			return from elem in this
			where elem is TCastTo
			select (TCastTo)((object)elem);
		}

		// Token: 0x060002F7 RID: 759 RVA: 0x00007A46 File Offset: 0x00005C46
		internal override IEnumerator GetEnumeratorUntyped()
		{
			return ((IEnumerable<TSource>)this).GetEnumerator();
		}

		/// <summary>Returns an enumerator that iterates through the sequence.</summary>
		/// <returns>An enumerator that iterates through the sequence.</returns>
		// Token: 0x060002F8 RID: 760 RVA: 0x00003A6B File Offset: 0x00001C6B
		public virtual IEnumerator<TSource> GetEnumerator()
		{
			throw new NotSupportedException();
		}

		// Token: 0x060002F9 RID: 761 RVA: 0x0000220F File Offset: 0x0000040F
		internal ParallelQuery()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0200007D RID: 125
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c__1<TCastTo>
		{
			// Token: 0x060002FA RID: 762 RVA: 0x00007A4E File Offset: 0x00005C4E
			// Note: this type is marked as 'beforefieldinit'.
			static <>c__1()
			{
			}

			// Token: 0x060002FB RID: 763 RVA: 0x00002310 File Offset: 0x00000510
			public <>c__1()
			{
			}

			// Token: 0x060002FC RID: 764 RVA: 0x00007A5A File Offset: 0x00005C5A
			internal TCastTo <Cast>b__1_0(TSource elem)
			{
				return (TCastTo)((object)elem);
			}

			// Token: 0x040002F6 RID: 758
			public static readonly ParallelQuery<TSource>.<>c__1<TCastTo> <>9 = new ParallelQuery<TSource>.<>c__1<TCastTo>();

			// Token: 0x040002F7 RID: 759
			public static Func<TSource, TCastTo> <>9__1_0;
		}

		// Token: 0x0200007E RID: 126
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c__2<TCastTo>
		{
			// Token: 0x060002FD RID: 765 RVA: 0x00007A67 File Offset: 0x00005C67
			// Note: this type is marked as 'beforefieldinit'.
			static <>c__2()
			{
			}

			// Token: 0x060002FE RID: 766 RVA: 0x00002310 File Offset: 0x00000510
			public <>c__2()
			{
			}

			// Token: 0x060002FF RID: 767 RVA: 0x00007A73 File Offset: 0x00005C73
			internal bool <OfType>b__2_0(TSource elem)
			{
				return elem is TCastTo;
			}

			// Token: 0x06000300 RID: 768 RVA: 0x00007A5A File Offset: 0x00005C5A
			internal TCastTo <OfType>b__2_1(TSource elem)
			{
				return (TCastTo)((object)elem);
			}

			// Token: 0x040002F8 RID: 760
			public static readonly ParallelQuery<TSource>.<>c__2<TCastTo> <>9 = new ParallelQuery<TSource>.<>c__2<TCastTo>();

			// Token: 0x040002F9 RID: 761
			public static Func<TSource, bool> <>9__2_0;

			// Token: 0x040002FA RID: 762
			public static Func<TSource, TCastTo> <>9__2_1;
		}
	}
}
