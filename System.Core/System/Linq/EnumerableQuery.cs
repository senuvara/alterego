﻿using System;
using System.Collections;
using System.Linq.Expressions;

namespace System.Linq
{
	/// <summary>Represents an <see cref="T:System.Collections.IEnumerable" /> as an <see cref="T:System.Linq.EnumerableQuery" /> data source. </summary>
	// Token: 0x0200008B RID: 139
	public abstract class EnumerableQuery
	{
		// Token: 0x170000C2 RID: 194
		// (get) Token: 0x06000463 RID: 1123
		internal abstract Expression Expression { get; }

		// Token: 0x170000C3 RID: 195
		// (get) Token: 0x06000464 RID: 1124
		internal abstract IEnumerable Enumerable { get; }

		// Token: 0x06000465 RID: 1125 RVA: 0x0000B741 File Offset: 0x00009941
		internal static IQueryable Create(Type elementType, IEnumerable sequence)
		{
			return (IQueryable)Activator.CreateInstance(typeof(EnumerableQuery<>).MakeGenericType(new Type[]
			{
				elementType
			}), new object[]
			{
				sequence
			});
		}

		// Token: 0x06000466 RID: 1126 RVA: 0x0000B741 File Offset: 0x00009941
		internal static IQueryable Create(Type elementType, Expression expression)
		{
			return (IQueryable)Activator.CreateInstance(typeof(EnumerableQuery<>).MakeGenericType(new Type[]
			{
				elementType
			}), new object[]
			{
				expression
			});
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Linq.EnumerableQuery" /> class.</summary>
		// Token: 0x06000467 RID: 1127 RVA: 0x00002310 File Offset: 0x00000510
		protected EnumerableQuery()
		{
		}
	}
}
