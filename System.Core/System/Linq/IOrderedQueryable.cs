﻿using System;
using System.Collections;

namespace System.Linq
{
	/// <summary>Represents the result of a sorting operation.</summary>
	// Token: 0x02000074 RID: 116
	public interface IOrderedQueryable : IQueryable, IEnumerable
	{
	}
}
