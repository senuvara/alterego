﻿using System;

namespace System.Linq
{
	// Token: 0x02000093 RID: 147
	internal static class Strings
	{
		// Token: 0x06000520 RID: 1312 RVA: 0x0000F094 File Offset: 0x0000D294
		internal static string ArgumentNotIEnumerableGeneric(string message)
		{
			return SR.Format("{0} is not IEnumerable<>", message);
		}

		// Token: 0x06000521 RID: 1313 RVA: 0x0000F0A1 File Offset: 0x0000D2A1
		internal static string ArgumentNotValid(string message)
		{
			return SR.Format("Argument {0} is not valid", message);
		}

		// Token: 0x06000522 RID: 1314 RVA: 0x0000F0AE File Offset: 0x0000D2AE
		internal static string NoMethodOnType(string name, object type)
		{
			return SR.Format("There is no method '{0}' on type '{1}'", name, type);
		}

		// Token: 0x06000523 RID: 1315 RVA: 0x0000F0BC File Offset: 0x0000D2BC
		internal static string NoMethodOnTypeMatchingArguments(string name, object type)
		{
			return SR.Format("There is no method '{0}' on type '{1}' that matches the specified arguments", name, type);
		}

		// Token: 0x06000524 RID: 1316 RVA: 0x0000F0CA File Offset: 0x0000D2CA
		internal static string EnumeratingNullEnumerableExpression()
		{
			return "Cannot enumerate a query created from a null IEnumerable<>";
		}
	}
}
