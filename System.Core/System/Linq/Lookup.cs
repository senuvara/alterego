﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Linq
{
	/// <summary>Represents a collection of keys each mapped to one or more values.</summary>
	/// <typeparam name="TKey">The type of the keys in the <see cref="T:System.Linq.Lookup`2" />.</typeparam>
	/// <typeparam name="TElement">The type of the elements of each <see cref="T:System.Collections.Generic.IEnumerable`1" /> value in the <see cref="T:System.Linq.Lookup`2" />.</typeparam>
	// Token: 0x020000C3 RID: 195
	public class Lookup<TKey, TElement> : IEnumerable<IGrouping<TKey, TElement>>, IEnumerable, ILookup<TKey, TElement>
	{
		// Token: 0x0600073D RID: 1853 RVA: 0x00015BB8 File Offset: 0x00013DB8
		internal static Lookup<TKey, TElement> Create<TSource>(IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, IEqualityComparer<TKey> comparer)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (keySelector == null)
			{
				throw Error.ArgumentNull("keySelector");
			}
			if (elementSelector == null)
			{
				throw Error.ArgumentNull("elementSelector");
			}
			Lookup<TKey, TElement> lookup = new Lookup<TKey, TElement>(comparer);
			foreach (TSource arg in source)
			{
				lookup.GetGrouping(keySelector(arg), true).Add(elementSelector(arg));
			}
			return lookup;
		}

		// Token: 0x0600073E RID: 1854 RVA: 0x00015C48 File Offset: 0x00013E48
		internal static Lookup<TKey, TElement> CreateForJoin(IEnumerable<TElement> source, Func<TElement, TKey> keySelector, IEqualityComparer<TKey> comparer)
		{
			Lookup<TKey, TElement> lookup = new Lookup<TKey, TElement>(comparer);
			foreach (TElement telement in source)
			{
				TKey tkey = keySelector(telement);
				if (tkey != null)
				{
					lookup.GetGrouping(tkey, true).Add(telement);
				}
			}
			return lookup;
		}

		// Token: 0x0600073F RID: 1855 RVA: 0x00015CB0 File Offset: 0x00013EB0
		private Lookup(IEqualityComparer<TKey> comparer)
		{
			if (comparer == null)
			{
				comparer = EqualityComparer<TKey>.Default;
			}
			this.comparer = comparer;
			this.groupings = new Lookup<TKey, TElement>.Grouping[7];
		}

		/// <summary>Gets the number of key/value collection pairs in the <see cref="T:System.Linq.Lookup`2" />.</summary>
		/// <returns>The number of key/value collection pairs in the <see cref="T:System.Linq.Lookup`2" />.</returns>
		// Token: 0x17000107 RID: 263
		// (get) Token: 0x06000740 RID: 1856 RVA: 0x00015CD5 File Offset: 0x00013ED5
		public int Count
		{
			get
			{
				return this.count;
			}
		}

		/// <summary>Gets the collection of values indexed by the specified key.</summary>
		/// <param name="key">The key of the desired collection of values.</param>
		/// <returns>The collection of values indexed by the specified key.</returns>
		// Token: 0x17000108 RID: 264
		public IEnumerable<TElement> this[TKey key]
		{
			get
			{
				Lookup<TKey, TElement>.Grouping grouping = this.GetGrouping(key, false);
				if (grouping != null)
				{
					return grouping;
				}
				return EmptyEnumerable<TElement>.Instance;
			}
		}

		/// <summary>Determines whether a specified key is in the <see cref="T:System.Linq.Lookup`2" />.</summary>
		/// <param name="key">The key to find in the <see cref="T:System.Linq.Lookup`2" />.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="key" /> is in the <see cref="T:System.Linq.Lookup`2" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000742 RID: 1858 RVA: 0x00015D00 File Offset: 0x00013F00
		public bool Contains(TKey key)
		{
			return this.GetGrouping(key, false) != null;
		}

		/// <summary>Returns a generic enumerator that iterates through the <see cref="T:System.Linq.Lookup`2" />.</summary>
		/// <returns>An enumerator for the <see cref="T:System.Linq.Lookup`2" />.</returns>
		// Token: 0x06000743 RID: 1859 RVA: 0x00015D0D File Offset: 0x00013F0D
		public IEnumerator<IGrouping<TKey, TElement>> GetEnumerator()
		{
			Lookup<TKey, TElement>.Grouping g = this.lastGrouping;
			if (g != null)
			{
				do
				{
					g = g.next;
					yield return g;
				}
				while (g != this.lastGrouping);
			}
			yield break;
		}

		/// <summary>Applies a transform function to each key and its associated values and returns the results.</summary>
		/// <param name="resultSelector">A function to project a result value from each key and its associated values.</param>
		/// <typeparam name="TResult">The type of the result values produced by <paramref name="resultSelector" />.</typeparam>
		/// <returns>A collection that contains one value for each key/value collection pair in the <see cref="T:System.Linq.Lookup`2" />.</returns>
		// Token: 0x06000744 RID: 1860 RVA: 0x00015D1C File Offset: 0x00013F1C
		public IEnumerable<TResult> ApplyResultSelector<TResult>(Func<TKey, IEnumerable<TElement>, TResult> resultSelector)
		{
			Lookup<TKey, TElement>.Grouping g = this.lastGrouping;
			if (g != null)
			{
				do
				{
					g = g.next;
					if (g.count != g.elements.Length)
					{
						Array.Resize<TElement>(ref g.elements, g.count);
					}
					yield return resultSelector(g.key, g.elements);
				}
				while (g != this.lastGrouping);
			}
			yield break;
		}

		/// <summary>Returns an enumerator that iterates through the <see cref="T:System.Linq.Lookup`2" />. This class cannot be inherited.</summary>
		/// <returns>An enumerator for the <see cref="T:System.Linq.Lookup`2" />.</returns>
		// Token: 0x06000745 RID: 1861 RVA: 0x00015D33 File Offset: 0x00013F33
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x06000746 RID: 1862 RVA: 0x00015D3B File Offset: 0x00013F3B
		internal int InternalGetHashCode(TKey key)
		{
			if (key != null)
			{
				return this.comparer.GetHashCode(key) & int.MaxValue;
			}
			return 0;
		}

		// Token: 0x06000747 RID: 1863 RVA: 0x00015D5C File Offset: 0x00013F5C
		internal Lookup<TKey, TElement>.Grouping GetGrouping(TKey key, bool create)
		{
			int num = this.InternalGetHashCode(key);
			for (Lookup<TKey, TElement>.Grouping grouping = this.groupings[num % this.groupings.Length]; grouping != null; grouping = grouping.hashNext)
			{
				if (grouping.hashCode == num && this.comparer.Equals(grouping.key, key))
				{
					return grouping;
				}
			}
			if (create)
			{
				if (this.count == this.groupings.Length)
				{
					this.Resize();
				}
				int num2 = num % this.groupings.Length;
				Lookup<TKey, TElement>.Grouping grouping2 = new Lookup<TKey, TElement>.Grouping();
				grouping2.key = key;
				grouping2.hashCode = num;
				grouping2.elements = new TElement[1];
				grouping2.hashNext = this.groupings[num2];
				this.groupings[num2] = grouping2;
				if (this.lastGrouping == null)
				{
					grouping2.next = grouping2;
				}
				else
				{
					grouping2.next = this.lastGrouping.next;
					this.lastGrouping.next = grouping2;
				}
				this.lastGrouping = grouping2;
				this.count++;
				return grouping2;
			}
			return null;
		}

		// Token: 0x06000748 RID: 1864 RVA: 0x00015E54 File Offset: 0x00014054
		private void Resize()
		{
			int num = checked(this.count * 2 + 1);
			Lookup<TKey, TElement>.Grouping[] array = new Lookup<TKey, TElement>.Grouping[num];
			Lookup<TKey, TElement>.Grouping next = this.lastGrouping;
			do
			{
				next = next.next;
				int num2 = next.hashCode % num;
				next.hashNext = array[num2];
				array[num2] = next;
			}
			while (next != this.lastGrouping);
			this.groupings = array;
		}

		// Token: 0x06000749 RID: 1865 RVA: 0x0000220F File Offset: 0x0000040F
		internal Lookup()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040004CD RID: 1229
		private IEqualityComparer<TKey> comparer;

		// Token: 0x040004CE RID: 1230
		private Lookup<TKey, TElement>.Grouping[] groupings;

		// Token: 0x040004CF RID: 1231
		private Lookup<TKey, TElement>.Grouping lastGrouping;

		// Token: 0x040004D0 RID: 1232
		private int count;

		// Token: 0x020000C4 RID: 196
		internal class Grouping : IGrouping<!0, !1>, IEnumerable<!1>, IEnumerable, IList<TElement>, ICollection<TElement>
		{
			// Token: 0x0600074A RID: 1866 RVA: 0x00015EA8 File Offset: 0x000140A8
			internal void Add(TElement element)
			{
				if (this.elements.Length == this.count)
				{
					Array.Resize<TElement>(ref this.elements, checked(this.count * 2));
				}
				this.elements[this.count] = element;
				this.count++;
			}

			// Token: 0x0600074B RID: 1867 RVA: 0x00015EF8 File Offset: 0x000140F8
			public IEnumerator<TElement> GetEnumerator()
			{
				int num;
				for (int i = 0; i < this.count; i = num + 1)
				{
					yield return this.elements[i];
					num = i;
				}
				yield break;
			}

			// Token: 0x0600074C RID: 1868 RVA: 0x00015F07 File Offset: 0x00014107
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			// Token: 0x17000109 RID: 265
			// (get) Token: 0x0600074D RID: 1869 RVA: 0x00015F0F File Offset: 0x0001410F
			public TKey Key
			{
				get
				{
					return this.key;
				}
			}

			// Token: 0x1700010A RID: 266
			// (get) Token: 0x0600074E RID: 1870 RVA: 0x00015F17 File Offset: 0x00014117
			int ICollection<!1>.Count
			{
				get
				{
					return this.count;
				}
			}

			// Token: 0x1700010B RID: 267
			// (get) Token: 0x0600074F RID: 1871 RVA: 0x00009CDF File Offset: 0x00007EDF
			bool ICollection<!1>.IsReadOnly
			{
				get
				{
					return true;
				}
			}

			// Token: 0x06000750 RID: 1872 RVA: 0x00015F1F File Offset: 0x0001411F
			void ICollection<!1>.Add(TElement item)
			{
				throw Error.NotSupported();
			}

			// Token: 0x06000751 RID: 1873 RVA: 0x00015F1F File Offset: 0x0001411F
			void ICollection<!1>.Clear()
			{
				throw Error.NotSupported();
			}

			// Token: 0x06000752 RID: 1874 RVA: 0x00015F26 File Offset: 0x00014126
			bool ICollection<!1>.Contains(TElement item)
			{
				return Array.IndexOf<TElement>(this.elements, item, 0, this.count) >= 0;
			}

			// Token: 0x06000753 RID: 1875 RVA: 0x00015F41 File Offset: 0x00014141
			void ICollection<!1>.CopyTo(TElement[] array, int arrayIndex)
			{
				Array.Copy(this.elements, 0, array, arrayIndex, this.count);
			}

			// Token: 0x06000754 RID: 1876 RVA: 0x00015F1F File Offset: 0x0001411F
			bool ICollection<!1>.Remove(TElement item)
			{
				throw Error.NotSupported();
			}

			// Token: 0x06000755 RID: 1877 RVA: 0x00015F57 File Offset: 0x00014157
			int IList<!1>.IndexOf(TElement item)
			{
				return Array.IndexOf<TElement>(this.elements, item, 0, this.count);
			}

			// Token: 0x06000756 RID: 1878 RVA: 0x00015F1F File Offset: 0x0001411F
			void IList<!1>.Insert(int index, TElement item)
			{
				throw Error.NotSupported();
			}

			// Token: 0x06000757 RID: 1879 RVA: 0x00015F1F File Offset: 0x0001411F
			void IList<!1>.RemoveAt(int index)
			{
				throw Error.NotSupported();
			}

			// Token: 0x1700010C RID: 268
			TElement IList<!1>.this[int index]
			{
				get
				{
					if (index < 0 || index >= this.count)
					{
						throw Error.ArgumentOutOfRange("index");
					}
					return this.elements[index];
				}
				set
				{
					throw Error.NotSupported();
				}
			}

			// Token: 0x0600075A RID: 1882 RVA: 0x00002310 File Offset: 0x00000510
			public Grouping()
			{
			}

			// Token: 0x040004D1 RID: 1233
			internal TKey key;

			// Token: 0x040004D2 RID: 1234
			internal int hashCode;

			// Token: 0x040004D3 RID: 1235
			internal TElement[] elements;

			// Token: 0x040004D4 RID: 1236
			internal int count;

			// Token: 0x040004D5 RID: 1237
			internal Lookup<TKey, TElement>.Grouping hashNext;

			// Token: 0x040004D6 RID: 1238
			internal Lookup<TKey, TElement>.Grouping next;

			// Token: 0x020000C5 RID: 197
			[CompilerGenerated]
			private sealed class <GetEnumerator>d__7 : IEnumerator<!1>, IDisposable, IEnumerator
			{
				// Token: 0x0600075B RID: 1883 RVA: 0x00015F92 File Offset: 0x00014192
				[DebuggerHidden]
				public <GetEnumerator>d__7(int <>1__state)
				{
					this.<>1__state = <>1__state;
				}

				// Token: 0x0600075C RID: 1884 RVA: 0x000039E8 File Offset: 0x00001BE8
				[DebuggerHidden]
				void IDisposable.Dispose()
				{
				}

				// Token: 0x0600075D RID: 1885 RVA: 0x00015FA4 File Offset: 0x000141A4
				bool IEnumerator.MoveNext()
				{
					int num = this.<>1__state;
					Lookup<TKey, TElement>.Grouping grouping = this;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -1;
						int num2 = i;
						i = num2 + 1;
					}
					else
					{
						this.<>1__state = -1;
						i = 0;
					}
					if (i >= grouping.count)
					{
						return false;
					}
					this.<>2__current = grouping.elements[i];
					this.<>1__state = 1;
					return true;
				}

				// Token: 0x1700010D RID: 269
				// (get) Token: 0x0600075E RID: 1886 RVA: 0x0001601E File Offset: 0x0001421E
				TElement IEnumerator<!1>.Current
				{
					[DebuggerHidden]
					get
					{
						return this.<>2__current;
					}
				}

				// Token: 0x0600075F RID: 1887 RVA: 0x00003A6B File Offset: 0x00001C6B
				[DebuggerHidden]
				void IEnumerator.Reset()
				{
					throw new NotSupportedException();
				}

				// Token: 0x1700010E RID: 270
				// (get) Token: 0x06000760 RID: 1888 RVA: 0x00016026 File Offset: 0x00014226
				object IEnumerator.Current
				{
					[DebuggerHidden]
					get
					{
						return this.<>2__current;
					}
				}

				// Token: 0x040004D7 RID: 1239
				private int <>1__state;

				// Token: 0x040004D8 RID: 1240
				private TElement <>2__current;

				// Token: 0x040004D9 RID: 1241
				public Lookup<TKey, TElement>.Grouping <>4__this;

				// Token: 0x040004DA RID: 1242
				private int <i>5__1;
			}
		}

		// Token: 0x020000C6 RID: 198
		[CompilerGenerated]
		private sealed class <GetEnumerator>d__12 : IEnumerator<IGrouping<!0, !1>>, IDisposable, IEnumerator
		{
			// Token: 0x06000761 RID: 1889 RVA: 0x00016033 File Offset: 0x00014233
			[DebuggerHidden]
			public <GetEnumerator>d__12(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x06000762 RID: 1890 RVA: 0x000039E8 File Offset: 0x00001BE8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06000763 RID: 1891 RVA: 0x00016044 File Offset: 0x00014244
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				Lookup<TKey, TElement> lookup = this;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
					if (g == lookup.lastGrouping)
					{
						return false;
					}
				}
				else
				{
					this.<>1__state = -1;
					g = lookup.lastGrouping;
					if (g == null)
					{
						return false;
					}
				}
				g = g.next;
				this.<>2__current = g;
				this.<>1__state = 1;
				return true;
			}

			// Token: 0x1700010F RID: 271
			// (get) Token: 0x06000764 RID: 1892 RVA: 0x000160BF File Offset: 0x000142BF
			IGrouping<TKey, TElement> IEnumerator<IGrouping<!0, !1>>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000765 RID: 1893 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000110 RID: 272
			// (get) Token: 0x06000766 RID: 1894 RVA: 0x000160BF File Offset: 0x000142BF
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x040004DB RID: 1243
			private int <>1__state;

			// Token: 0x040004DC RID: 1244
			private IGrouping<TKey, TElement> <>2__current;

			// Token: 0x040004DD RID: 1245
			public Lookup<TKey, TElement> <>4__this;

			// Token: 0x040004DE RID: 1246
			private Lookup<TKey, TElement>.Grouping <g>5__1;
		}

		// Token: 0x020000C7 RID: 199
		[CompilerGenerated]
		private sealed class <ApplyResultSelector>d__13<TResult> : IEnumerable<!2>, IEnumerable, IEnumerator<!2>, IDisposable, IEnumerator
		{
			// Token: 0x06000767 RID: 1895 RVA: 0x000160C7 File Offset: 0x000142C7
			[DebuggerHidden]
			public <ApplyResultSelector>d__13(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000768 RID: 1896 RVA: 0x000039E8 File Offset: 0x00001BE8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06000769 RID: 1897 RVA: 0x000160E4 File Offset: 0x000142E4
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				Lookup<TKey, TElement> lookup = this;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
					if (g == lookup.lastGrouping)
					{
						return false;
					}
				}
				else
				{
					this.<>1__state = -1;
					g = lookup.lastGrouping;
					if (g == null)
					{
						return false;
					}
				}
				g = g.next;
				if (g.count != g.elements.Length)
				{
					Array.Resize<TElement>(ref g.elements, g.count);
				}
				this.<>2__current = resultSelector(g.key, g.elements);
				this.<>1__state = 1;
				return true;
			}

			// Token: 0x17000111 RID: 273
			// (get) Token: 0x0600076A RID: 1898 RVA: 0x000161B8 File Offset: 0x000143B8
			TResult IEnumerator<!2>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600076B RID: 1899 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000112 RID: 274
			// (get) Token: 0x0600076C RID: 1900 RVA: 0x000161C0 File Offset: 0x000143C0
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600076D RID: 1901 RVA: 0x000161D0 File Offset: 0x000143D0
			[DebuggerHidden]
			IEnumerator<TResult> IEnumerable<!2>.GetEnumerator()
			{
				Lookup<TKey, TElement>.<ApplyResultSelector>d__13<TResult> <ApplyResultSelector>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<ApplyResultSelector>d__ = this;
				}
				else
				{
					<ApplyResultSelector>d__ = new Lookup<TKey, TElement>.<ApplyResultSelector>d__13<TResult>(0);
					<ApplyResultSelector>d__.<>4__this = this;
				}
				<ApplyResultSelector>d__.resultSelector = resultSelector;
				return <ApplyResultSelector>d__;
			}

			// Token: 0x0600076E RID: 1902 RVA: 0x0001621F File Offset: 0x0001441F
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TResult>.GetEnumerator();
			}

			// Token: 0x040004DF RID: 1247
			private int <>1__state;

			// Token: 0x040004E0 RID: 1248
			private TResult <>2__current;

			// Token: 0x040004E1 RID: 1249
			private int <>l__initialThreadId;

			// Token: 0x040004E2 RID: 1250
			public Lookup<TKey, TElement> <>4__this;

			// Token: 0x040004E3 RID: 1251
			private Lookup<TKey, TElement>.Grouping <g>5__1;

			// Token: 0x040004E4 RID: 1252
			private Func<TKey, IEnumerable<TElement>, TResult> resultSelector;

			// Token: 0x040004E5 RID: 1253
			public Func<TKey, IEnumerable<TElement>, TResult> <>3__resultSelector;
		}
	}
}
