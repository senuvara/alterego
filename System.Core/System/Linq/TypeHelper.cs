﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace System.Linq
{
	// Token: 0x02000094 RID: 148
	internal static class TypeHelper
	{
		// Token: 0x06000525 RID: 1317 RVA: 0x0000F0D4 File Offset: 0x0000D2D4
		internal static Type FindGenericType(Type definition, Type type)
		{
			bool? flag = null;
			while (type != null && type != typeof(object))
			{
				if (type.IsGenericType && type.GetGenericTypeDefinition() == definition)
				{
					return type;
				}
				if (flag == null)
				{
					flag = new bool?(definition.IsInterface);
				}
				if (flag.GetValueOrDefault())
				{
					foreach (Type type2 in type.GetInterfaces())
					{
						Type type3 = TypeHelper.FindGenericType(definition, type2);
						if (type3 != null)
						{
							return type3;
						}
					}
				}
				type = type.BaseType;
			}
			return null;
		}

		// Token: 0x06000526 RID: 1318 RVA: 0x0000F178 File Offset: 0x0000D378
		internal static IEnumerable<MethodInfo> GetStaticMethods(this Type type)
		{
			return from m in type.GetRuntimeMethods()
			where m.IsStatic
			select m;
		}

		// Token: 0x02000095 RID: 149
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000527 RID: 1319 RVA: 0x0000F1A4 File Offset: 0x0000D3A4
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000528 RID: 1320 RVA: 0x00002310 File Offset: 0x00000510
			public <>c()
			{
			}

			// Token: 0x06000529 RID: 1321 RVA: 0x0000F1B0 File Offset: 0x0000D3B0
			internal bool <GetStaticMethods>b__1_0(MethodInfo m)
			{
				return m.IsStatic;
			}

			// Token: 0x0400039A RID: 922
			public static readonly TypeHelper.<>c <>9 = new TypeHelper.<>c();

			// Token: 0x0400039B RID: 923
			public static Func<MethodInfo, bool> <>9__1_0;
		}
	}
}
