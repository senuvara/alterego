﻿using System;

namespace System.Linq.Parallel
{
	// Token: 0x0200011E RID: 286
	internal abstract class BinaryQueryOperator<TLeftInput, TRightInput, TOutput> : QueryOperator<TOutput>
	{
		// Token: 0x060008AC RID: 2220 RVA: 0x0001B84F File Offset: 0x00019A4F
		internal BinaryQueryOperator(ParallelQuery<TLeftInput> leftChild, ParallelQuery<TRightInput> rightChild) : this(QueryOperator<TLeftInput>.AsQueryOperator(leftChild), QueryOperator<TRightInput>.AsQueryOperator(rightChild))
		{
		}

		// Token: 0x060008AD RID: 2221 RVA: 0x0001B864 File Offset: 0x00019A64
		internal BinaryQueryOperator(QueryOperator<TLeftInput> leftChild, QueryOperator<TRightInput> rightChild) : base(false, leftChild.SpecifiedQuerySettings.Merge(rightChild.SpecifiedQuerySettings))
		{
			this._leftChild = leftChild;
			this._rightChild = rightChild;
		}

		// Token: 0x1700013A RID: 314
		// (get) Token: 0x060008AE RID: 2222 RVA: 0x0001B8A1 File Offset: 0x00019AA1
		internal QueryOperator<TLeftInput> LeftChild
		{
			get
			{
				return this._leftChild;
			}
		}

		// Token: 0x1700013B RID: 315
		// (get) Token: 0x060008AF RID: 2223 RVA: 0x0001B8A9 File Offset: 0x00019AA9
		internal QueryOperator<TRightInput> RightChild
		{
			get
			{
				return this._rightChild;
			}
		}

		// Token: 0x1700013C RID: 316
		// (get) Token: 0x060008B0 RID: 2224 RVA: 0x0001B8B1 File Offset: 0x00019AB1
		internal sealed override OrdinalIndexState OrdinalIndexState
		{
			get
			{
				return this._indexState;
			}
		}

		// Token: 0x060008B1 RID: 2225 RVA: 0x0001B8B9 File Offset: 0x00019AB9
		protected void SetOrdinalIndex(OrdinalIndexState indexState)
		{
			this._indexState = indexState;
		}

		// Token: 0x060008B2 RID: 2226
		public abstract void WrapPartitionedStream<TLeftKey, TRightKey>(PartitionedStream<TLeftInput, TLeftKey> leftPartitionedStream, PartitionedStream<TRightInput, TRightKey> rightPartitionedStream, IPartitionedStreamRecipient<TOutput> outputRecipient, bool preferStriping, QuerySettings settings);

		// Token: 0x04000625 RID: 1573
		private readonly QueryOperator<TLeftInput> _leftChild;

		// Token: 0x04000626 RID: 1574
		private readonly QueryOperator<TRightInput> _rightChild;

		// Token: 0x04000627 RID: 1575
		private OrdinalIndexState _indexState = OrdinalIndexState.Shuffled;

		// Token: 0x0200011F RID: 287
		internal class BinaryQueryOperatorResults : QueryResults<TOutput>
		{
			// Token: 0x060008B3 RID: 2227 RVA: 0x0001B8C2 File Offset: 0x00019AC2
			internal BinaryQueryOperatorResults(QueryResults<TLeftInput> leftChildQueryResults, QueryResults<TRightInput> rightChildQueryResults, BinaryQueryOperator<TLeftInput, TRightInput, TOutput> op, QuerySettings settings, bool preferStriping)
			{
				this._leftChildQueryResults = leftChildQueryResults;
				this._rightChildQueryResults = rightChildQueryResults;
				this._op = op;
				this._settings = settings;
				this._preferStriping = preferStriping;
			}

			// Token: 0x060008B4 RID: 2228 RVA: 0x0001B8F0 File Offset: 0x00019AF0
			internal override void GivePartitionedStream(IPartitionedStreamRecipient<TOutput> recipient)
			{
				if (this._settings.ExecutionMode.Value == ParallelExecutionMode.Default && this._op.LimitsParallelism)
				{
					PartitionedStream<TOutput, int> partitionedStream = ExchangeUtilities.PartitionDataSource<TOutput>(this._op.AsSequentialQuery(this._settings.CancellationState.ExternalCancellationToken), this._settings.DegreeOfParallelism.Value, this._preferStriping);
					recipient.Receive<int>(partitionedStream);
					return;
				}
				if (this.IsIndexible)
				{
					PartitionedStream<TOutput, int> partitionedStream2 = ExchangeUtilities.PartitionDataSource<TOutput>(this, this._settings.DegreeOfParallelism.Value, this._preferStriping);
					recipient.Receive<int>(partitionedStream2);
					return;
				}
				this._leftChildQueryResults.GivePartitionedStream(new BinaryQueryOperator<TLeftInput, TRightInput, TOutput>.BinaryQueryOperatorResults.LeftChildResultsRecipient(recipient, this, this._preferStriping, this._settings));
			}

			// Token: 0x04000628 RID: 1576
			protected QueryResults<TLeftInput> _leftChildQueryResults;

			// Token: 0x04000629 RID: 1577
			protected QueryResults<TRightInput> _rightChildQueryResults;

			// Token: 0x0400062A RID: 1578
			private BinaryQueryOperator<TLeftInput, TRightInput, TOutput> _op;

			// Token: 0x0400062B RID: 1579
			private QuerySettings _settings;

			// Token: 0x0400062C RID: 1580
			private bool _preferStriping;

			// Token: 0x02000120 RID: 288
			private class LeftChildResultsRecipient : IPartitionedStreamRecipient<TLeftInput>
			{
				// Token: 0x060008B5 RID: 2229 RVA: 0x0001B9AF File Offset: 0x00019BAF
				internal LeftChildResultsRecipient(IPartitionedStreamRecipient<TOutput> outputRecipient, BinaryQueryOperator<TLeftInput, TRightInput, TOutput>.BinaryQueryOperatorResults results, bool preferStriping, QuerySettings settings)
				{
					this._outputRecipient = outputRecipient;
					this._results = results;
					this._preferStriping = preferStriping;
					this._settings = settings;
				}

				// Token: 0x060008B6 RID: 2230 RVA: 0x0001B9D4 File Offset: 0x00019BD4
				public void Receive<TLeftKey>(PartitionedStream<TLeftInput, TLeftKey> source)
				{
					BinaryQueryOperator<TLeftInput, TRightInput, TOutput>.BinaryQueryOperatorResults.RightChildResultsRecipient<TLeftKey> recipient = new BinaryQueryOperator<TLeftInput, TRightInput, TOutput>.BinaryQueryOperatorResults.RightChildResultsRecipient<TLeftKey>(this._outputRecipient, this._results._op, source, this._preferStriping, this._settings);
					this._results._rightChildQueryResults.GivePartitionedStream(recipient);
				}

				// Token: 0x0400062D RID: 1581
				private IPartitionedStreamRecipient<TOutput> _outputRecipient;

				// Token: 0x0400062E RID: 1582
				private BinaryQueryOperator<TLeftInput, TRightInput, TOutput>.BinaryQueryOperatorResults _results;

				// Token: 0x0400062F RID: 1583
				private bool _preferStriping;

				// Token: 0x04000630 RID: 1584
				private QuerySettings _settings;
			}

			// Token: 0x02000121 RID: 289
			private class RightChildResultsRecipient<TLeftKey> : IPartitionedStreamRecipient<TRightInput>
			{
				// Token: 0x060008B7 RID: 2231 RVA: 0x0001BA16 File Offset: 0x00019C16
				internal RightChildResultsRecipient(IPartitionedStreamRecipient<TOutput> outputRecipient, BinaryQueryOperator<TLeftInput, TRightInput, TOutput> op, PartitionedStream<TLeftInput, TLeftKey> leftPartitionedStream, bool preferStriping, QuerySettings settings)
				{
					this._outputRecipient = outputRecipient;
					this._op = op;
					this._preferStriping = preferStriping;
					this._leftPartitionedStream = leftPartitionedStream;
					this._settings = settings;
				}

				// Token: 0x060008B8 RID: 2232 RVA: 0x0001BA43 File Offset: 0x00019C43
				public void Receive<TRightKey>(PartitionedStream<TRightInput, TRightKey> rightPartitionedStream)
				{
					this._op.WrapPartitionedStream<TLeftKey, TRightKey>(this._leftPartitionedStream, rightPartitionedStream, this._outputRecipient, this._preferStriping, this._settings);
				}

				// Token: 0x04000631 RID: 1585
				private IPartitionedStreamRecipient<TOutput> _outputRecipient;

				// Token: 0x04000632 RID: 1586
				private PartitionedStream<TLeftInput, TLeftKey> _leftPartitionedStream;

				// Token: 0x04000633 RID: 1587
				private BinaryQueryOperator<TLeftInput, TRightInput, TOutput> _op;

				// Token: 0x04000634 RID: 1588
				private bool _preferStriping;

				// Token: 0x04000635 RID: 1589
				private QuerySettings _settings;
			}
		}
	}
}
