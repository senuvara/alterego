﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x0200017B RID: 379
	internal sealed class DefaultIfEmptyQueryOperator<TSource> : UnaryQueryOperator<TSource, TSource>
	{
		// Token: 0x06000A18 RID: 2584 RVA: 0x0001FDC8 File Offset: 0x0001DFC8
		internal DefaultIfEmptyQueryOperator(IEnumerable<TSource> child, TSource defaultValue) : base(child)
		{
			this._defaultValue = defaultValue;
			base.SetOrdinalIndexState(base.Child.OrdinalIndexState.Worse(OrdinalIndexState.Correct));
		}

		// Token: 0x06000A19 RID: 2585 RVA: 0x0001FDEF File Offset: 0x0001DFEF
		internal override QueryResults<TSource> Open(QuerySettings settings, bool preferStriping)
		{
			return new UnaryQueryOperator<TSource, TSource>.UnaryQueryOperatorResults(base.Child.Open(settings, preferStriping), this, settings, preferStriping);
		}

		// Token: 0x06000A1A RID: 2586 RVA: 0x0001FE08 File Offset: 0x0001E008
		internal override void WrapPartitionedStream<TKey>(PartitionedStream<TSource, TKey> inputStream, IPartitionedStreamRecipient<TSource> recipient, bool preferStriping, QuerySettings settings)
		{
			int partitionCount = inputStream.PartitionCount;
			Shared<int> sharedEmptyCount = new Shared<int>(0);
			CountdownEvent sharedLatch = new CountdownEvent(partitionCount - 1);
			PartitionedStream<TSource, TKey> partitionedStream = new PartitionedStream<TSource, TKey>(partitionCount, inputStream.KeyComparer, this.OrdinalIndexState);
			for (int i = 0; i < partitionCount; i++)
			{
				partitionedStream[i] = new DefaultIfEmptyQueryOperator<TSource>.DefaultIfEmptyQueryOperatorEnumerator<TKey>(inputStream[i], this._defaultValue, i, partitionCount, sharedEmptyCount, sharedLatch, settings.CancellationState.MergedCancellationToken);
			}
			recipient.Receive<TKey>(partitionedStream);
		}

		// Token: 0x06000A1B RID: 2587 RVA: 0x0001FE82 File Offset: 0x0001E082
		internal override IEnumerable<TSource> AsSequentialQuery(CancellationToken token)
		{
			return base.Child.AsSequentialQuery(token).DefaultIfEmpty(this._defaultValue);
		}

		// Token: 0x17000164 RID: 356
		// (get) Token: 0x06000A1C RID: 2588 RVA: 0x00002285 File Offset: 0x00000485
		internal override bool LimitsParallelism
		{
			get
			{
				return false;
			}
		}

		// Token: 0x040006B3 RID: 1715
		private readonly TSource _defaultValue;

		// Token: 0x0200017C RID: 380
		private class DefaultIfEmptyQueryOperatorEnumerator<TKey> : QueryOperatorEnumerator<TSource, TKey>
		{
			// Token: 0x06000A1D RID: 2589 RVA: 0x0001FE9B File Offset: 0x0001E09B
			internal DefaultIfEmptyQueryOperatorEnumerator(QueryOperatorEnumerator<TSource, TKey> source, TSource defaultValue, int partitionIndex, int partitionCount, Shared<int> sharedEmptyCount, CountdownEvent sharedLatch, CancellationToken cancelToken)
			{
				this._source = source;
				this._defaultValue = defaultValue;
				this._partitionIndex = partitionIndex;
				this._partitionCount = partitionCount;
				this._sharedEmptyCount = sharedEmptyCount;
				this._sharedLatch = sharedLatch;
				this._cancelToken = cancelToken;
			}

			// Token: 0x06000A1E RID: 2590 RVA: 0x0001FED8 File Offset: 0x0001E0D8
			internal override bool MoveNext(ref TSource currentElement, ref TKey currentKey)
			{
				bool flag = this._source.MoveNext(ref currentElement, ref currentKey);
				if (!this._lookedForEmpty)
				{
					this._lookedForEmpty = true;
					if (!flag)
					{
						if (this._partitionIndex == 0)
						{
							this._sharedLatch.Wait(this._cancelToken);
							this._sharedLatch.Dispose();
							if (this._sharedEmptyCount.Value == this._partitionCount - 1)
							{
								currentElement = this._defaultValue;
								currentKey = default(TKey);
								return true;
							}
							return false;
						}
						else
						{
							Interlocked.Increment(ref this._sharedEmptyCount.Value);
						}
					}
					if (this._partitionIndex != 0)
					{
						this._sharedLatch.Signal();
					}
				}
				return flag;
			}

			// Token: 0x06000A1F RID: 2591 RVA: 0x0001FF7B File Offset: 0x0001E17B
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x040006B4 RID: 1716
			private QueryOperatorEnumerator<TSource, TKey> _source;

			// Token: 0x040006B5 RID: 1717
			private bool _lookedForEmpty;

			// Token: 0x040006B6 RID: 1718
			private int _partitionIndex;

			// Token: 0x040006B7 RID: 1719
			private int _partitionCount;

			// Token: 0x040006B8 RID: 1720
			private TSource _defaultValue;

			// Token: 0x040006B9 RID: 1721
			private Shared<int> _sharedEmptyCount;

			// Token: 0x040006BA RID: 1722
			private CountdownEvent _sharedLatch;

			// Token: 0x040006BB RID: 1723
			private CancellationToken _cancelToken;
		}
	}
}
