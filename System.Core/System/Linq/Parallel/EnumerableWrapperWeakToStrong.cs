﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x020000DA RID: 218
	internal class EnumerableWrapperWeakToStrong : IEnumerable<object>, IEnumerable
	{
		// Token: 0x060007BF RID: 1983 RVA: 0x000171EA File Offset: 0x000153EA
		internal EnumerableWrapperWeakToStrong(IEnumerable wrappedEnumerable)
		{
			this._wrappedEnumerable = wrappedEnumerable;
		}

		// Token: 0x060007C0 RID: 1984 RVA: 0x000171F9 File Offset: 0x000153F9
		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable<object>)this).GetEnumerator();
		}

		// Token: 0x060007C1 RID: 1985 RVA: 0x00017201 File Offset: 0x00015401
		public IEnumerator<object> GetEnumerator()
		{
			return new EnumerableWrapperWeakToStrong.WrapperEnumeratorWeakToStrong(this._wrappedEnumerable.GetEnumerator());
		}

		// Token: 0x04000521 RID: 1313
		private readonly IEnumerable _wrappedEnumerable;

		// Token: 0x020000DB RID: 219
		private class WrapperEnumeratorWeakToStrong : IEnumerator<object>, IDisposable, IEnumerator
		{
			// Token: 0x060007C2 RID: 1986 RVA: 0x00017213 File Offset: 0x00015413
			internal WrapperEnumeratorWeakToStrong(IEnumerator wrappedEnumerator)
			{
				this._wrappedEnumerator = wrappedEnumerator;
			}

			// Token: 0x1700011F RID: 287
			// (get) Token: 0x060007C3 RID: 1987 RVA: 0x00017222 File Offset: 0x00015422
			object IEnumerator.Current
			{
				get
				{
					return this._wrappedEnumerator.Current;
				}
			}

			// Token: 0x17000120 RID: 288
			// (get) Token: 0x060007C4 RID: 1988 RVA: 0x00017222 File Offset: 0x00015422
			object IEnumerator<object>.Current
			{
				get
				{
					return this._wrappedEnumerator.Current;
				}
			}

			// Token: 0x060007C5 RID: 1989 RVA: 0x00017230 File Offset: 0x00015430
			void IDisposable.Dispose()
			{
				IDisposable disposable = this._wrappedEnumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}

			// Token: 0x060007C6 RID: 1990 RVA: 0x00017252 File Offset: 0x00015452
			bool IEnumerator.MoveNext()
			{
				return this._wrappedEnumerator.MoveNext();
			}

			// Token: 0x060007C7 RID: 1991 RVA: 0x0001725F File Offset: 0x0001545F
			void IEnumerator.Reset()
			{
				this._wrappedEnumerator.Reset();
			}

			// Token: 0x04000522 RID: 1314
			private IEnumerator _wrappedEnumerator;
		}
	}
}
