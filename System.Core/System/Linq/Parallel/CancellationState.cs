﻿using System;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x020001BA RID: 442
	internal class CancellationState
	{
		// Token: 0x17000184 RID: 388
		// (get) Token: 0x06000B06 RID: 2822 RVA: 0x00023545 File Offset: 0x00021745
		internal CancellationToken MergedCancellationToken
		{
			get
			{
				if (this.MergedCancellationTokenSource != null)
				{
					return this.MergedCancellationTokenSource.Token;
				}
				return new CancellationToken(false);
			}
		}

		// Token: 0x06000B07 RID: 2823 RVA: 0x00023561 File Offset: 0x00021761
		internal CancellationState(CancellationToken externalCancellationToken)
		{
			this.ExternalCancellationToken = externalCancellationToken;
			this.TopLevelDisposedFlag = new Shared<bool>(false);
		}

		// Token: 0x06000B08 RID: 2824 RVA: 0x0002357C File Offset: 0x0002177C
		internal static void ThrowIfCanceled(CancellationToken token)
		{
			if (token.IsCancellationRequested)
			{
				throw new OperationCanceledException(token);
			}
		}

		// Token: 0x06000B09 RID: 2825 RVA: 0x0002358E File Offset: 0x0002178E
		internal static void ThrowWithStandardMessageIfCanceled(CancellationToken externalCancellationToken)
		{
			if (externalCancellationToken.IsCancellationRequested)
			{
				throw new OperationCanceledException("The query has been canceled via the token supplied to WithCancellation.", externalCancellationToken);
			}
		}

		// Token: 0x0400077E RID: 1918
		internal CancellationTokenSource InternalCancellationTokenSource;

		// Token: 0x0400077F RID: 1919
		internal CancellationToken ExternalCancellationToken;

		// Token: 0x04000780 RID: 1920
		internal CancellationTokenSource MergedCancellationTokenSource;

		// Token: 0x04000781 RID: 1921
		internal Shared<bool> TopLevelDisposedFlag;

		// Token: 0x04000782 RID: 1922
		internal const int POLL_INTERVAL = 63;
	}
}
