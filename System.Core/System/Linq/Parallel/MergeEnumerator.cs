﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x020000EC RID: 236
	internal abstract class MergeEnumerator<TInputOutput> : IEnumerator<TInputOutput>, IDisposable, IEnumerator
	{
		// Token: 0x060007F6 RID: 2038 RVA: 0x00017DE4 File Offset: 0x00015FE4
		protected MergeEnumerator(QueryTaskGroupState taskGroupState)
		{
			this._taskGroupState = taskGroupState;
		}

		// Token: 0x17000123 RID: 291
		// (get) Token: 0x060007F7 RID: 2039
		public abstract TInputOutput Current { get; }

		// Token: 0x060007F8 RID: 2040
		public abstract bool MoveNext();

		// Token: 0x17000124 RID: 292
		// (get) Token: 0x060007F9 RID: 2041 RVA: 0x00017DF3 File Offset: 0x00015FF3
		object IEnumerator.Current
		{
			get
			{
				return ((IEnumerator<TInputOutput>)this).Current;
			}
		}

		// Token: 0x060007FA RID: 2042 RVA: 0x000039E8 File Offset: 0x00001BE8
		public virtual void Reset()
		{
		}

		// Token: 0x060007FB RID: 2043 RVA: 0x00017E00 File Offset: 0x00016000
		public virtual void Dispose()
		{
			if (!this._taskGroupState.IsAlreadyEnded)
			{
				this._taskGroupState.QueryEnd(true);
			}
		}

		// Token: 0x0400054E RID: 1358
		protected QueryTaskGroupState _taskGroupState;
	}
}
