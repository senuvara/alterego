﻿using System;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x020000DE RID: 222
	internal class ParallelEnumerableWrapper<T> : ParallelQuery<T>
	{
		// Token: 0x060007CC RID: 1996 RVA: 0x0001729F File Offset: 0x0001549F
		internal ParallelEnumerableWrapper(IEnumerable<T> wrappedEnumerable) : base(QuerySettings.Empty)
		{
			this._wrappedEnumerable = wrappedEnumerable;
		}

		// Token: 0x17000121 RID: 289
		// (get) Token: 0x060007CD RID: 1997 RVA: 0x000172B3 File Offset: 0x000154B3
		internal IEnumerable<T> WrappedEnumerable
		{
			get
			{
				return this._wrappedEnumerable;
			}
		}

		// Token: 0x060007CE RID: 1998 RVA: 0x000172BB File Offset: 0x000154BB
		public override IEnumerator<T> GetEnumerator()
		{
			return this._wrappedEnumerable.GetEnumerator();
		}

		// Token: 0x04000524 RID: 1316
		private readonly IEnumerable<T> _wrappedEnumerable;
	}
}
