﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;

namespace System.Linq.Parallel
{
	// Token: 0x020000EF RID: 239
	internal class OrderPreservingPipeliningMergeHelper<TOutput, TKey> : IMergeHelper<!0>
	{
		// Token: 0x06000808 RID: 2056 RVA: 0x00017FB8 File Offset: 0x000161B8
		internal OrderPreservingPipeliningMergeHelper(PartitionedStream<TOutput, TKey> partitions, TaskScheduler taskScheduler, CancellationState cancellationState, bool autoBuffered, int queryId, IComparer<TKey> keyComparer)
		{
			this._taskGroupState = new QueryTaskGroupState(cancellationState, queryId);
			this._partitions = partitions;
			this._taskScheduler = taskScheduler;
			this._autoBuffered = autoBuffered;
			int partitionCount = this._partitions.PartitionCount;
			this._buffers = new Queue<Pair<TKey, TOutput>>[partitionCount];
			this._producerDone = new bool[partitionCount];
			this._consumerWaiting = new bool[partitionCount];
			this._producerWaiting = new bool[partitionCount];
			this._bufferLocks = new object[partitionCount];
			if (keyComparer == Util.GetDefaultComparer<int>())
			{
				this._producerComparer = (IComparer<Producer<TKey>>)new ProducerComparerInt();
				return;
			}
			this._producerComparer = new OrderPreservingPipeliningMergeHelper<TOutput, TKey>.ProducerComparer(keyComparer);
		}

		// Token: 0x06000809 RID: 2057 RVA: 0x00018060 File Offset: 0x00016260
		void IMergeHelper<!0>.Execute()
		{
			OrderPreservingPipeliningSpoolingTask<TOutput, TKey>.Spool(this._taskGroupState, this._partitions, this._consumerWaiting, this._producerWaiting, this._producerDone, this._buffers, this._bufferLocks, this._taskScheduler, this._autoBuffered);
		}

		// Token: 0x0600080A RID: 2058 RVA: 0x000180A8 File Offset: 0x000162A8
		IEnumerator<TOutput> IMergeHelper<!0>.GetEnumerator()
		{
			return new OrderPreservingPipeliningMergeHelper<TOutput, TKey>.OrderedPipeliningMergeEnumerator(this, this._producerComparer);
		}

		// Token: 0x0600080B RID: 2059 RVA: 0x000180B6 File Offset: 0x000162B6
		[ExcludeFromCodeCoverage]
		public TOutput[] GetResultsAsArray()
		{
			throw new InvalidOperationException();
		}

		// Token: 0x04000554 RID: 1364
		private readonly QueryTaskGroupState _taskGroupState;

		// Token: 0x04000555 RID: 1365
		private readonly PartitionedStream<TOutput, TKey> _partitions;

		// Token: 0x04000556 RID: 1366
		private readonly TaskScheduler _taskScheduler;

		// Token: 0x04000557 RID: 1367
		private readonly bool _autoBuffered;

		// Token: 0x04000558 RID: 1368
		private readonly Queue<Pair<TKey, TOutput>>[] _buffers;

		// Token: 0x04000559 RID: 1369
		private readonly bool[] _producerDone;

		// Token: 0x0400055A RID: 1370
		private readonly bool[] _producerWaiting;

		// Token: 0x0400055B RID: 1371
		private readonly bool[] _consumerWaiting;

		// Token: 0x0400055C RID: 1372
		private readonly object[] _bufferLocks;

		// Token: 0x0400055D RID: 1373
		private IComparer<Producer<TKey>> _producerComparer;

		// Token: 0x0400055E RID: 1374
		internal const int INITIAL_BUFFER_SIZE = 128;

		// Token: 0x0400055F RID: 1375
		internal const int STEAL_BUFFER_SIZE = 1024;

		// Token: 0x04000560 RID: 1376
		internal const int MAX_BUFFER_SIZE = 8192;

		// Token: 0x020000F0 RID: 240
		private class ProducerComparer : IComparer<Producer<TKey>>
		{
			// Token: 0x0600080C RID: 2060 RVA: 0x000180BD File Offset: 0x000162BD
			internal ProducerComparer(IComparer<TKey> keyComparer)
			{
				this._keyComparer = keyComparer;
			}

			// Token: 0x0600080D RID: 2061 RVA: 0x000180CC File Offset: 0x000162CC
			public int Compare(Producer<TKey> x, Producer<TKey> y)
			{
				return this._keyComparer.Compare(y.MaxKey, x.MaxKey);
			}

			// Token: 0x04000561 RID: 1377
			private IComparer<TKey> _keyComparer;
		}

		// Token: 0x020000F1 RID: 241
		private class OrderedPipeliningMergeEnumerator : MergeEnumerator<TOutput>
		{
			// Token: 0x0600080E RID: 2062 RVA: 0x000180E8 File Offset: 0x000162E8
			internal OrderedPipeliningMergeEnumerator(OrderPreservingPipeliningMergeHelper<TOutput, TKey> mergeHelper, IComparer<Producer<TKey>> producerComparer) : base(mergeHelper._taskGroupState)
			{
				int partitionCount = mergeHelper._partitions.PartitionCount;
				this._mergeHelper = mergeHelper;
				this._producerHeap = new FixedMaxHeap<Producer<TKey>>(partitionCount, producerComparer);
				this._privateBuffer = new Queue<Pair<TKey, TOutput>>[partitionCount];
				this._producerNextElement = new TOutput[partitionCount];
			}

			// Token: 0x17000125 RID: 293
			// (get) Token: 0x0600080F RID: 2063 RVA: 0x0001813C File Offset: 0x0001633C
			public override TOutput Current
			{
				get
				{
					int producerIndex = this._producerHeap.MaxValue.ProducerIndex;
					return this._producerNextElement[producerIndex];
				}
			}

			// Token: 0x06000810 RID: 2064 RVA: 0x00018168 File Offset: 0x00016368
			public override bool MoveNext()
			{
				if (!this._initialized)
				{
					this._initialized = true;
					for (int i = 0; i < this._mergeHelper._partitions.PartitionCount; i++)
					{
						Pair<TKey, TOutput> pair = default(Pair<TKey, TOutput>);
						if (this.TryWaitForElement(i, ref pair))
						{
							this._producerHeap.Insert(new Producer<TKey>(pair.First, i));
							this._producerNextElement[i] = pair.Second;
						}
						else
						{
							this.ThrowIfInTearDown();
						}
					}
				}
				else
				{
					if (this._producerHeap.Count == 0)
					{
						return false;
					}
					int producerIndex = this._producerHeap.MaxValue.ProducerIndex;
					Pair<TKey, TOutput> pair2 = default(Pair<TKey, TOutput>);
					if (this.TryGetPrivateElement(producerIndex, ref pair2) || this.TryWaitForElement(producerIndex, ref pair2))
					{
						this._producerHeap.ReplaceMax(new Producer<TKey>(pair2.First, producerIndex));
						this._producerNextElement[producerIndex] = pair2.Second;
					}
					else
					{
						this.ThrowIfInTearDown();
						this._producerHeap.RemoveMax();
					}
				}
				return this._producerHeap.Count > 0;
			}

			// Token: 0x06000811 RID: 2065 RVA: 0x00018274 File Offset: 0x00016474
			private void ThrowIfInTearDown()
			{
				if (this._mergeHelper._taskGroupState.CancellationState.MergedCancellationToken.IsCancellationRequested)
				{
					try
					{
						object[] bufferLocks = this._mergeHelper._bufferLocks;
						for (int i = 0; i < bufferLocks.Length; i++)
						{
							object obj = bufferLocks[i];
							lock (obj)
							{
								Monitor.Pulse(bufferLocks[i]);
							}
						}
						this._taskGroupState.QueryEnd(false);
					}
					finally
					{
						this._producerHeap.Clear();
					}
				}
			}

			// Token: 0x06000812 RID: 2066 RVA: 0x00018314 File Offset: 0x00016514
			private bool TryWaitForElement(int producer, ref Pair<TKey, TOutput> element)
			{
				Queue<Pair<TKey, TOutput>> queue = this._mergeHelper._buffers[producer];
				object obj = this._mergeHelper._bufferLocks[producer];
				object obj2 = obj;
				lock (obj2)
				{
					if (queue.Count == 0)
					{
						if (this._mergeHelper._producerDone[producer])
						{
							element = default(Pair<TKey, TOutput>);
							return false;
						}
						this._mergeHelper._consumerWaiting[producer] = true;
						Monitor.Wait(obj);
						if (queue.Count == 0)
						{
							element = default(Pair<TKey, TOutput>);
							return false;
						}
					}
					if (this._mergeHelper._producerWaiting[producer])
					{
						Monitor.Pulse(obj);
						this._mergeHelper._producerWaiting[producer] = false;
					}
					if (queue.Count < 1024)
					{
						element = queue.Dequeue();
						return true;
					}
					this._privateBuffer[producer] = this._mergeHelper._buffers[producer];
					this._mergeHelper._buffers[producer] = new Queue<Pair<TKey, TOutput>>(128);
				}
				this.TryGetPrivateElement(producer, ref element);
				return true;
			}

			// Token: 0x06000813 RID: 2067 RVA: 0x00018434 File Offset: 0x00016634
			private bool TryGetPrivateElement(int producer, ref Pair<TKey, TOutput> element)
			{
				Queue<Pair<TKey, TOutput>> queue = this._privateBuffer[producer];
				if (queue != null)
				{
					if (queue.Count > 0)
					{
						element = queue.Dequeue();
						return true;
					}
					this._privateBuffer[producer] = null;
				}
				return false;
			}

			// Token: 0x06000814 RID: 2068 RVA: 0x00018470 File Offset: 0x00016670
			public override void Dispose()
			{
				int num = this._mergeHelper._buffers.Length;
				for (int i = 0; i < num; i++)
				{
					object obj = this._mergeHelper._bufferLocks[i];
					object obj2 = obj;
					lock (obj2)
					{
						if (this._mergeHelper._producerWaiting[i])
						{
							Monitor.Pulse(obj);
						}
					}
				}
				base.Dispose();
			}

			// Token: 0x04000562 RID: 1378
			private OrderPreservingPipeliningMergeHelper<TOutput, TKey> _mergeHelper;

			// Token: 0x04000563 RID: 1379
			private readonly FixedMaxHeap<Producer<TKey>> _producerHeap;

			// Token: 0x04000564 RID: 1380
			private readonly TOutput[] _producerNextElement;

			// Token: 0x04000565 RID: 1381
			private readonly Queue<Pair<TKey, TOutput>>[] _privateBuffer;

			// Token: 0x04000566 RID: 1382
			private bool _initialized;
		}
	}
}
