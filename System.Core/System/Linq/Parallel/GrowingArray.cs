﻿using System;

namespace System.Linq.Parallel
{
	// Token: 0x020001D5 RID: 469
	internal class GrowingArray<T>
	{
		// Token: 0x06000B74 RID: 2932 RVA: 0x00024B34 File Offset: 0x00022D34
		internal GrowingArray()
		{
			this._array = new T[1024];
			this._count = 0;
		}

		// Token: 0x17000191 RID: 401
		// (get) Token: 0x06000B75 RID: 2933 RVA: 0x00024B53 File Offset: 0x00022D53
		internal T[] InternalArray
		{
			get
			{
				return this._array;
			}
		}

		// Token: 0x17000192 RID: 402
		// (get) Token: 0x06000B76 RID: 2934 RVA: 0x00024B5B File Offset: 0x00022D5B
		internal int Count
		{
			get
			{
				return this._count;
			}
		}

		// Token: 0x06000B77 RID: 2935 RVA: 0x00024B64 File Offset: 0x00022D64
		internal void Add(T element)
		{
			if (this._count >= this._array.Length)
			{
				this.GrowArray(2 * this._array.Length);
			}
			T[] array = this._array;
			int count = this._count;
			this._count = count + 1;
			array[count] = element;
		}

		// Token: 0x06000B78 RID: 2936 RVA: 0x00024BB0 File Offset: 0x00022DB0
		private void GrowArray(int newSize)
		{
			T[] array = new T[newSize];
			this._array.CopyTo(array, 0);
			this._array = array;
		}

		// Token: 0x06000B79 RID: 2937 RVA: 0x00024BD8 File Offset: 0x00022DD8
		internal void CopyFrom(T[] otherArray, int otherCount)
		{
			if (this._count + otherCount > this._array.Length)
			{
				this.GrowArray(this._count + otherCount);
			}
			Array.Copy(otherArray, 0, this._array, this._count, otherCount);
			this._count += otherCount;
		}

		// Token: 0x040007DC RID: 2012
		private T[] _array;

		// Token: 0x040007DD RID: 2013
		private int _count;

		// Token: 0x040007DE RID: 2014
		private const int DEFAULT_ARRAY_SIZE = 1024;
	}
}
