﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace System.Linq.Parallel
{
	// Token: 0x02000174 RID: 372
	internal struct QuerySettings
	{
		// Token: 0x17000158 RID: 344
		// (get) Token: 0x060009EB RID: 2539 RVA: 0x0001F55A File Offset: 0x0001D75A
		// (set) Token: 0x060009EC RID: 2540 RVA: 0x0001F562 File Offset: 0x0001D762
		internal CancellationState CancellationState
		{
			get
			{
				return this._cancellationState;
			}
			set
			{
				this._cancellationState = value;
			}
		}

		// Token: 0x17000159 RID: 345
		// (get) Token: 0x060009ED RID: 2541 RVA: 0x0001F56B File Offset: 0x0001D76B
		// (set) Token: 0x060009EE RID: 2542 RVA: 0x0001F573 File Offset: 0x0001D773
		internal TaskScheduler TaskScheduler
		{
			get
			{
				return this._taskScheduler;
			}
			set
			{
				this._taskScheduler = value;
			}
		}

		// Token: 0x1700015A RID: 346
		// (get) Token: 0x060009EF RID: 2543 RVA: 0x0001F57C File Offset: 0x0001D77C
		// (set) Token: 0x060009F0 RID: 2544 RVA: 0x0001F584 File Offset: 0x0001D784
		internal int? DegreeOfParallelism
		{
			get
			{
				return this._degreeOfParallelism;
			}
			set
			{
				this._degreeOfParallelism = value;
			}
		}

		// Token: 0x1700015B RID: 347
		// (get) Token: 0x060009F1 RID: 2545 RVA: 0x0001F58D File Offset: 0x0001D78D
		// (set) Token: 0x060009F2 RID: 2546 RVA: 0x0001F595 File Offset: 0x0001D795
		internal ParallelExecutionMode? ExecutionMode
		{
			get
			{
				return this._executionMode;
			}
			set
			{
				this._executionMode = value;
			}
		}

		// Token: 0x1700015C RID: 348
		// (get) Token: 0x060009F3 RID: 2547 RVA: 0x0001F59E File Offset: 0x0001D79E
		// (set) Token: 0x060009F4 RID: 2548 RVA: 0x0001F5A6 File Offset: 0x0001D7A6
		internal ParallelMergeOptions? MergeOptions
		{
			get
			{
				return this._mergeOptions;
			}
			set
			{
				this._mergeOptions = value;
			}
		}

		// Token: 0x1700015D RID: 349
		// (get) Token: 0x060009F5 RID: 2549 RVA: 0x0001F5AF File Offset: 0x0001D7AF
		internal int QueryId
		{
			get
			{
				return this._queryId;
			}
		}

		// Token: 0x060009F6 RID: 2550 RVA: 0x0001F5B7 File Offset: 0x0001D7B7
		internal QuerySettings(TaskScheduler taskScheduler, int? degreeOfParallelism, CancellationToken externalCancellationToken, ParallelExecutionMode? executionMode, ParallelMergeOptions? mergeOptions)
		{
			this._taskScheduler = taskScheduler;
			this._degreeOfParallelism = degreeOfParallelism;
			this._cancellationState = new CancellationState(externalCancellationToken);
			this._executionMode = executionMode;
			this._mergeOptions = mergeOptions;
			this._queryId = -1;
		}

		// Token: 0x060009F7 RID: 2551 RVA: 0x0001F5EC File Offset: 0x0001D7EC
		internal QuerySettings Merge(QuerySettings settings2)
		{
			if (this.TaskScheduler != null && settings2.TaskScheduler != null)
			{
				throw new InvalidOperationException("The WithTaskScheduler operator may be used at most once in a query.");
			}
			if (this.DegreeOfParallelism != null && settings2.DegreeOfParallelism != null)
			{
				throw new InvalidOperationException("The WithDegreeOfParallelism operator may be used at most once in a query.");
			}
			if (this.CancellationState.ExternalCancellationToken.CanBeCanceled && settings2.CancellationState.ExternalCancellationToken.CanBeCanceled)
			{
				throw new InvalidOperationException("The WithCancellation operator may by used at most once in a query.");
			}
			if (this.ExecutionMode != null && settings2.ExecutionMode != null)
			{
				throw new InvalidOperationException("The WithExecutionMode operator may be used at most once in a query.");
			}
			if (this.MergeOptions != null && settings2.MergeOptions != null)
			{
				throw new InvalidOperationException("The WithMergeOptions operator may be used at most once in a query.");
			}
			TaskScheduler taskScheduler = (this.TaskScheduler == null) ? settings2.TaskScheduler : this.TaskScheduler;
			int? degreeOfParallelism = (this.DegreeOfParallelism != null) ? this.DegreeOfParallelism : settings2.DegreeOfParallelism;
			CancellationToken externalCancellationToken = this.CancellationState.ExternalCancellationToken.CanBeCanceled ? this.CancellationState.ExternalCancellationToken : settings2.CancellationState.ExternalCancellationToken;
			ParallelExecutionMode? executionMode = (this.ExecutionMode != null) ? this.ExecutionMode : settings2.ExecutionMode;
			ParallelMergeOptions? mergeOptions = (this.MergeOptions != null) ? this.MergeOptions : settings2.MergeOptions;
			return new QuerySettings(taskScheduler, degreeOfParallelism, externalCancellationToken, executionMode, mergeOptions);
		}

		// Token: 0x060009F8 RID: 2552 RVA: 0x0001F77E File Offset: 0x0001D97E
		internal QuerySettings WithPerExecutionSettings()
		{
			return this.WithPerExecutionSettings(new CancellationTokenSource(), new Shared<bool>(false));
		}

		// Token: 0x060009F9 RID: 2553 RVA: 0x0001F794 File Offset: 0x0001D994
		internal QuerySettings WithPerExecutionSettings(CancellationTokenSource topLevelCancellationTokenSource, Shared<bool> topLevelDisposedFlag)
		{
			QuerySettings result = new QuerySettings(this.TaskScheduler, this.DegreeOfParallelism, this.CancellationState.ExternalCancellationToken, this.ExecutionMode, this.MergeOptions);
			result.CancellationState.InternalCancellationTokenSource = topLevelCancellationTokenSource;
			result.CancellationState.MergedCancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(result.CancellationState.InternalCancellationTokenSource.Token, result.CancellationState.ExternalCancellationToken);
			result.CancellationState.TopLevelDisposedFlag = topLevelDisposedFlag;
			result._queryId = PlinqEtwProvider.NextQueryId();
			return result;
		}

		// Token: 0x060009FA RID: 2554 RVA: 0x0001F820 File Offset: 0x0001DA20
		internal QuerySettings WithDefaults()
		{
			QuerySettings result = this;
			if (result.TaskScheduler == null)
			{
				result.TaskScheduler = TaskScheduler.Default;
			}
			if (result.DegreeOfParallelism == null)
			{
				result.DegreeOfParallelism = new int?(Scheduling.GetDefaultDegreeOfParallelism());
			}
			if (result.ExecutionMode == null)
			{
				result.ExecutionMode = new ParallelExecutionMode?(ParallelExecutionMode.Default);
			}
			if (result.MergeOptions == null)
			{
				result.MergeOptions = new ParallelMergeOptions?(ParallelMergeOptions.Default);
			}
			if (result.MergeOptions == ParallelMergeOptions.Default)
			{
				result.MergeOptions = new ParallelMergeOptions?(ParallelMergeOptions.AutoBuffered);
			}
			return result;
		}

		// Token: 0x1700015E RID: 350
		// (get) Token: 0x060009FB RID: 2555 RVA: 0x0001F8D8 File Offset: 0x0001DAD8
		internal static QuerySettings Empty
		{
			get
			{
				return new QuerySettings(null, null, default(CancellationToken), null, null);
			}
		}

		// Token: 0x060009FC RID: 2556 RVA: 0x0001F90F File Offset: 0x0001DB0F
		public void CleanStateAtQueryEnd()
		{
			this._cancellationState.MergedCancellationTokenSource.Dispose();
		}

		// Token: 0x0400069A RID: 1690
		private TaskScheduler _taskScheduler;

		// Token: 0x0400069B RID: 1691
		private int? _degreeOfParallelism;

		// Token: 0x0400069C RID: 1692
		private CancellationState _cancellationState;

		// Token: 0x0400069D RID: 1693
		private ParallelExecutionMode? _executionMode;

		// Token: 0x0400069E RID: 1694
		private ParallelMergeOptions? _mergeOptions;

		// Token: 0x0400069F RID: 1695
		private int _queryId;
	}
}
