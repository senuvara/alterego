﻿using System;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x020001C8 RID: 456
	internal class StopAndGoSpoolingTask<TInputOutput, TIgnoreKey> : SpoolingTaskBase
	{
		// Token: 0x06000B33 RID: 2867 RVA: 0x00023F9F File Offset: 0x0002219F
		internal StopAndGoSpoolingTask(int taskIndex, QueryTaskGroupState groupState, QueryOperatorEnumerator<TInputOutput, TIgnoreKey> source, SynchronousChannel<TInputOutput> destination) : base(taskIndex, groupState)
		{
			this._source = source;
			this._destination = destination;
		}

		// Token: 0x06000B34 RID: 2868 RVA: 0x00023FB8 File Offset: 0x000221B8
		protected override void SpoolingWork()
		{
			TInputOutput item = default(TInputOutput);
			TIgnoreKey tignoreKey = default(TIgnoreKey);
			QueryOperatorEnumerator<TInputOutput, TIgnoreKey> source = this._source;
			SynchronousChannel<TInputOutput> destination = this._destination;
			CancellationToken mergedCancellationToken = this._groupState.CancellationState.MergedCancellationToken;
			destination.Init();
			while (source.MoveNext(ref item, ref tignoreKey) && !mergedCancellationToken.IsCancellationRequested)
			{
				destination.Enqueue(item);
			}
		}

		// Token: 0x06000B35 RID: 2869 RVA: 0x00024019 File Offset: 0x00022219
		protected override void SpoolingFinally()
		{
			base.SpoolingFinally();
			if (this._destination != null)
			{
				this._destination.SetDone();
			}
			this._source.Dispose();
		}

		// Token: 0x040007B8 RID: 1976
		private QueryOperatorEnumerator<TInputOutput, TIgnoreKey> _source;

		// Token: 0x040007B9 RID: 1977
		private SynchronousChannel<TInputOutput> _destination;
	}
}
