﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x020001A0 RID: 416
	internal sealed class SelectManyQueryOperator<TLeftInput, TRightInput, TOutput> : UnaryQueryOperator<TLeftInput, TOutput>
	{
		// Token: 0x06000AA2 RID: 2722 RVA: 0x00021B5C File Offset: 0x0001FD5C
		internal SelectManyQueryOperator(IEnumerable<TLeftInput> leftChild, Func<TLeftInput, IEnumerable<TRightInput>> rightChildSelector, Func<TLeftInput, int, IEnumerable<TRightInput>> indexedRightChildSelector, Func<TLeftInput, TRightInput, TOutput> resultSelector) : base(leftChild)
		{
			this._rightChildSelector = rightChildSelector;
			this._indexedRightChildSelector = indexedRightChildSelector;
			this._resultSelector = resultSelector;
			this._outputOrdered = (base.Child.OutputOrdered || indexedRightChildSelector != null);
			this.InitOrderIndex();
		}

		// Token: 0x06000AA3 RID: 2723 RVA: 0x00021B9C File Offset: 0x0001FD9C
		private void InitOrderIndex()
		{
			OrdinalIndexState ordinalIndexState = base.Child.OrdinalIndexState;
			if (this._indexedRightChildSelector != null)
			{
				this._prematureMerge = ordinalIndexState.IsWorseThan(OrdinalIndexState.Correct);
				this._limitsParallelism = (this._prematureMerge && ordinalIndexState != OrdinalIndexState.Shuffled);
			}
			else if (base.OutputOrdered)
			{
				this._prematureMerge = ordinalIndexState.IsWorseThan(OrdinalIndexState.Increasing);
			}
			base.SetOrdinalIndexState(OrdinalIndexState.Increasing);
		}

		// Token: 0x06000AA4 RID: 2724 RVA: 0x00021C00 File Offset: 0x0001FE00
		internal override void WrapPartitionedStream<TLeftKey>(PartitionedStream<TLeftInput, TLeftKey> inputStream, IPartitionedStreamRecipient<TOutput> recipient, bool preferStriping, QuerySettings settings)
		{
			int partitionCount = inputStream.PartitionCount;
			if (this._indexedRightChildSelector != null)
			{
				PartitionedStream<TLeftInput, int> inputStream2;
				if (this._prematureMerge)
				{
					inputStream2 = QueryOperator<TLeftInput>.ExecuteAndCollectResults<TLeftKey>(inputStream, partitionCount, base.OutputOrdered, preferStriping, settings).GetPartitionedStream();
				}
				else
				{
					inputStream2 = (PartitionedStream<TLeftInput, int>)inputStream;
				}
				this.WrapPartitionedStreamIndexed(inputStream2, recipient, settings);
				return;
			}
			if (this._prematureMerge)
			{
				PartitionedStream<TLeftInput, int> partitionedStream = QueryOperator<TLeftInput>.ExecuteAndCollectResults<TLeftKey>(inputStream, partitionCount, base.OutputOrdered, preferStriping, settings).GetPartitionedStream();
				this.WrapPartitionedStreamNotIndexed<int>(partitionedStream, recipient, settings);
				return;
			}
			this.WrapPartitionedStreamNotIndexed<TLeftKey>(inputStream, recipient, settings);
		}

		// Token: 0x06000AA5 RID: 2725 RVA: 0x00021C84 File Offset: 0x0001FE84
		private void WrapPartitionedStreamNotIndexed<TLeftKey>(PartitionedStream<TLeftInput, TLeftKey> inputStream, IPartitionedStreamRecipient<TOutput> recipient, QuerySettings settings)
		{
			int partitionCount = inputStream.PartitionCount;
			PairComparer<TLeftKey, int> keyComparer = new PairComparer<TLeftKey, int>(inputStream.KeyComparer, Util.GetDefaultComparer<int>());
			PartitionedStream<TOutput, Pair<TLeftKey, int>> partitionedStream = new PartitionedStream<TOutput, Pair<TLeftKey, int>>(partitionCount, keyComparer, this.OrdinalIndexState);
			for (int i = 0; i < partitionCount; i++)
			{
				partitionedStream[i] = new SelectManyQueryOperator<TLeftInput, TRightInput, TOutput>.SelectManyQueryOperatorEnumerator<TLeftKey>(inputStream[i], this, settings.CancellationState.MergedCancellationToken);
			}
			recipient.Receive<Pair<TLeftKey, int>>(partitionedStream);
		}

		// Token: 0x06000AA6 RID: 2726 RVA: 0x00021CEC File Offset: 0x0001FEEC
		private void WrapPartitionedStreamIndexed(PartitionedStream<TLeftInput, int> inputStream, IPartitionedStreamRecipient<TOutput> recipient, QuerySettings settings)
		{
			PairComparer<int, int> keyComparer = new PairComparer<int, int>(inputStream.KeyComparer, Util.GetDefaultComparer<int>());
			PartitionedStream<TOutput, Pair<int, int>> partitionedStream = new PartitionedStream<TOutput, Pair<int, int>>(inputStream.PartitionCount, keyComparer, this.OrdinalIndexState);
			for (int i = 0; i < inputStream.PartitionCount; i++)
			{
				partitionedStream[i] = new SelectManyQueryOperator<TLeftInput, TRightInput, TOutput>.IndexedSelectManyQueryOperatorEnumerator(inputStream[i], this, settings.CancellationState.MergedCancellationToken);
			}
			recipient.Receive<Pair<int, int>>(partitionedStream);
		}

		// Token: 0x06000AA7 RID: 2727 RVA: 0x00021D55 File Offset: 0x0001FF55
		internal override QueryResults<TOutput> Open(QuerySettings settings, bool preferStriping)
		{
			return new UnaryQueryOperator<TLeftInput, TOutput>.UnaryQueryOperatorResults(base.Child.Open(settings, preferStriping), this, settings, preferStriping);
		}

		// Token: 0x06000AA8 RID: 2728 RVA: 0x00021D6C File Offset: 0x0001FF6C
		internal override IEnumerable<TOutput> AsSequentialQuery(CancellationToken token)
		{
			if (this._rightChildSelector != null)
			{
				if (this._resultSelector != null)
				{
					return CancellableEnumerable.Wrap<TLeftInput>(base.Child.AsSequentialQuery(token), token).SelectMany(this._rightChildSelector, this._resultSelector);
				}
				return (IEnumerable<!2>)CancellableEnumerable.Wrap<TLeftInput>(base.Child.AsSequentialQuery(token), token).SelectMany(this._rightChildSelector);
			}
			else
			{
				if (this._resultSelector != null)
				{
					return CancellableEnumerable.Wrap<TLeftInput>(base.Child.AsSequentialQuery(token), token).SelectMany(this._indexedRightChildSelector, this._resultSelector);
				}
				return (IEnumerable<!2>)CancellableEnumerable.Wrap<TLeftInput>(base.Child.AsSequentialQuery(token), token).SelectMany(this._indexedRightChildSelector);
			}
		}

		// Token: 0x17000176 RID: 374
		// (get) Token: 0x06000AA9 RID: 2729 RVA: 0x00021E1E File Offset: 0x0002001E
		internal override bool LimitsParallelism
		{
			get
			{
				return this._limitsParallelism;
			}
		}

		// Token: 0x0400071E RID: 1822
		private readonly Func<TLeftInput, IEnumerable<TRightInput>> _rightChildSelector;

		// Token: 0x0400071F RID: 1823
		private readonly Func<TLeftInput, int, IEnumerable<TRightInput>> _indexedRightChildSelector;

		// Token: 0x04000720 RID: 1824
		private readonly Func<TLeftInput, TRightInput, TOutput> _resultSelector;

		// Token: 0x04000721 RID: 1825
		private bool _prematureMerge;

		// Token: 0x04000722 RID: 1826
		private bool _limitsParallelism;

		// Token: 0x020001A1 RID: 417
		private class IndexedSelectManyQueryOperatorEnumerator : QueryOperatorEnumerator<TOutput, Pair<int, int>>
		{
			// Token: 0x06000AAA RID: 2730 RVA: 0x00021E26 File Offset: 0x00020026
			internal IndexedSelectManyQueryOperatorEnumerator(QueryOperatorEnumerator<TLeftInput, int> leftSource, SelectManyQueryOperator<TLeftInput, TRightInput, TOutput> selectManyOperator, CancellationToken cancellationToken)
			{
				this._leftSource = leftSource;
				this._selectManyOperator = selectManyOperator;
				this._cancellationToken = cancellationToken;
			}

			// Token: 0x06000AAB RID: 2731 RVA: 0x00021E44 File Offset: 0x00020044
			internal override bool MoveNext(ref TOutput currentElement, ref Pair<int, int> currentKey)
			{
				for (;;)
				{
					if (this._currentRightSource == null)
					{
						this._mutables = new SelectManyQueryOperator<TLeftInput, TRightInput, TOutput>.IndexedSelectManyQueryOperatorEnumerator.Mutables();
						SelectManyQueryOperator<TLeftInput, TRightInput, TOutput>.IndexedSelectManyQueryOperatorEnumerator.Mutables mutables = this._mutables;
						int lhsCount = mutables._lhsCount;
						mutables._lhsCount = lhsCount + 1;
						if ((lhsCount & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(this._cancellationToken);
						}
						if (!this._leftSource.MoveNext(ref this._mutables._currentLeftElement, ref this._mutables._currentLeftSourceIndex))
						{
							break;
						}
						IEnumerable<TRightInput> enumerable = this._selectManyOperator._indexedRightChildSelector(this._mutables._currentLeftElement, this._mutables._currentLeftSourceIndex);
						this._currentRightSource = enumerable.GetEnumerator();
						if (this._selectManyOperator._resultSelector == null)
						{
							this._currentRightSourceAsOutput = (IEnumerator<!2>)this._currentRightSource;
						}
					}
					if (this._currentRightSource.MoveNext())
					{
						goto Block_4;
					}
					this._currentRightSource.Dispose();
					this._currentRightSource = null;
					this._currentRightSourceAsOutput = null;
				}
				return false;
				Block_4:
				this._mutables._currentRightSourceIndex++;
				if (this._selectManyOperator._resultSelector != null)
				{
					currentElement = this._selectManyOperator._resultSelector(this._mutables._currentLeftElement, this._currentRightSource.Current);
				}
				else
				{
					currentElement = this._currentRightSourceAsOutput.Current;
				}
				currentKey = new Pair<int, int>(this._mutables._currentLeftSourceIndex, this._mutables._currentRightSourceIndex);
				return true;
			}

			// Token: 0x06000AAC RID: 2732 RVA: 0x00021FB2 File Offset: 0x000201B2
			protected override void Dispose(bool disposing)
			{
				this._leftSource.Dispose();
				if (this._currentRightSource != null)
				{
					this._currentRightSource.Dispose();
				}
			}

			// Token: 0x04000723 RID: 1827
			private readonly QueryOperatorEnumerator<TLeftInput, int> _leftSource;

			// Token: 0x04000724 RID: 1828
			private readonly SelectManyQueryOperator<TLeftInput, TRightInput, TOutput> _selectManyOperator;

			// Token: 0x04000725 RID: 1829
			private IEnumerator<TRightInput> _currentRightSource;

			// Token: 0x04000726 RID: 1830
			private IEnumerator<TOutput> _currentRightSourceAsOutput;

			// Token: 0x04000727 RID: 1831
			private SelectManyQueryOperator<TLeftInput, TRightInput, TOutput>.IndexedSelectManyQueryOperatorEnumerator.Mutables _mutables;

			// Token: 0x04000728 RID: 1832
			private readonly CancellationToken _cancellationToken;

			// Token: 0x020001A2 RID: 418
			private class Mutables
			{
				// Token: 0x06000AAD RID: 2733 RVA: 0x00021FD2 File Offset: 0x000201D2
				public Mutables()
				{
				}

				// Token: 0x04000729 RID: 1833
				internal int _currentRightSourceIndex = -1;

				// Token: 0x0400072A RID: 1834
				internal TLeftInput _currentLeftElement;

				// Token: 0x0400072B RID: 1835
				internal int _currentLeftSourceIndex;

				// Token: 0x0400072C RID: 1836
				internal int _lhsCount;
			}
		}

		// Token: 0x020001A3 RID: 419
		private class SelectManyQueryOperatorEnumerator<TLeftKey> : QueryOperatorEnumerator<TOutput, Pair<TLeftKey, int>>
		{
			// Token: 0x06000AAE RID: 2734 RVA: 0x00021FE1 File Offset: 0x000201E1
			internal SelectManyQueryOperatorEnumerator(QueryOperatorEnumerator<TLeftInput, TLeftKey> leftSource, SelectManyQueryOperator<TLeftInput, TRightInput, TOutput> selectManyOperator, CancellationToken cancellationToken)
			{
				this._leftSource = leftSource;
				this._selectManyOperator = selectManyOperator;
				this._cancellationToken = cancellationToken;
			}

			// Token: 0x06000AAF RID: 2735 RVA: 0x00022000 File Offset: 0x00020200
			internal override bool MoveNext(ref TOutput currentElement, ref Pair<TLeftKey, int> currentKey)
			{
				for (;;)
				{
					if (this._currentRightSource == null)
					{
						this._mutables = new SelectManyQueryOperator<TLeftInput, TRightInput, TOutput>.SelectManyQueryOperatorEnumerator<TLeftKey>.Mutables();
						SelectManyQueryOperator<TLeftInput, TRightInput, TOutput>.SelectManyQueryOperatorEnumerator<TLeftKey>.Mutables mutables = this._mutables;
						int lhsCount = mutables._lhsCount;
						mutables._lhsCount = lhsCount + 1;
						if ((lhsCount & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(this._cancellationToken);
						}
						if (!this._leftSource.MoveNext(ref this._mutables._currentLeftElement, ref this._mutables._currentLeftKey))
						{
							break;
						}
						IEnumerable<TRightInput> enumerable = this._selectManyOperator._rightChildSelector(this._mutables._currentLeftElement);
						this._currentRightSource = enumerable.GetEnumerator();
						if (this._selectManyOperator._resultSelector == null)
						{
							this._currentRightSourceAsOutput = (IEnumerator<!2>)this._currentRightSource;
						}
					}
					if (this._currentRightSource.MoveNext())
					{
						goto Block_4;
					}
					this._currentRightSource.Dispose();
					this._currentRightSource = null;
					this._currentRightSourceAsOutput = null;
				}
				return false;
				Block_4:
				this._mutables._currentRightSourceIndex++;
				if (this._selectManyOperator._resultSelector != null)
				{
					currentElement = this._selectManyOperator._resultSelector(this._mutables._currentLeftElement, this._currentRightSource.Current);
				}
				else
				{
					currentElement = this._currentRightSourceAsOutput.Current;
				}
				currentKey = new Pair<TLeftKey, int>(this._mutables._currentLeftKey, this._mutables._currentRightSourceIndex);
				return true;
			}

			// Token: 0x06000AB0 RID: 2736 RVA: 0x00022163 File Offset: 0x00020363
			protected override void Dispose(bool disposing)
			{
				this._leftSource.Dispose();
				if (this._currentRightSource != null)
				{
					this._currentRightSource.Dispose();
				}
			}

			// Token: 0x0400072D RID: 1837
			private readonly QueryOperatorEnumerator<TLeftInput, TLeftKey> _leftSource;

			// Token: 0x0400072E RID: 1838
			private readonly SelectManyQueryOperator<TLeftInput, TRightInput, TOutput> _selectManyOperator;

			// Token: 0x0400072F RID: 1839
			private IEnumerator<TRightInput> _currentRightSource;

			// Token: 0x04000730 RID: 1840
			private IEnumerator<TOutput> _currentRightSourceAsOutput;

			// Token: 0x04000731 RID: 1841
			private SelectManyQueryOperator<TLeftInput, TRightInput, TOutput>.SelectManyQueryOperatorEnumerator<TLeftKey>.Mutables _mutables;

			// Token: 0x04000732 RID: 1842
			private readonly CancellationToken _cancellationToken;

			// Token: 0x020001A4 RID: 420
			private class Mutables
			{
				// Token: 0x06000AB1 RID: 2737 RVA: 0x00022183 File Offset: 0x00020383
				public Mutables()
				{
				}

				// Token: 0x04000733 RID: 1843
				internal int _currentRightSourceIndex = -1;

				// Token: 0x04000734 RID: 1844
				internal TLeftInput _currentLeftElement;

				// Token: 0x04000735 RID: 1845
				internal TLeftKey _currentLeftKey;

				// Token: 0x04000736 RID: 1846
				internal int _lhsCount;
			}
		}
	}
}
