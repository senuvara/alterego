﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000124 RID: 292
	internal sealed class DecimalAverageAggregationOperator : InlinedAggregationOperator<decimal, Pair<decimal, long>, decimal>
	{
		// Token: 0x060008BF RID: 2239 RVA: 0x0001BB4F File Offset: 0x00019D4F
		internal DecimalAverageAggregationOperator(IEnumerable<decimal> child) : base(child)
		{
		}

		// Token: 0x060008C0 RID: 2240 RVA: 0x0001BB58 File Offset: 0x00019D58
		protected override decimal InternalAggregate(ref Exception singularExceptionToThrow)
		{
			decimal result;
			using (IEnumerator<Pair<decimal, long>> enumerator = this.GetEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered), true))
			{
				if (!enumerator.MoveNext())
				{
					singularExceptionToThrow = new InvalidOperationException("Sequence contains no elements");
					result = 0m;
				}
				else
				{
					Pair<decimal, long> pair = enumerator.Current;
					while (enumerator.MoveNext())
					{
						decimal first = pair.First;
						Pair<decimal, long> pair2 = enumerator.Current;
						pair.First = first + pair2.First;
						long second = pair.Second;
						pair2 = enumerator.Current;
						pair.Second = checked(second + pair2.Second);
					}
					result = pair.First / pair.Second;
				}
			}
			return result;
		}

		// Token: 0x060008C1 RID: 2241 RVA: 0x0001BC14 File Offset: 0x00019E14
		protected override QueryOperatorEnumerator<Pair<decimal, long>, int> CreateEnumerator<TKey>(int index, int count, QueryOperatorEnumerator<decimal, TKey> source, object sharedData, CancellationToken cancellationToken)
		{
			return new DecimalAverageAggregationOperator.DecimalAverageAggregationOperatorEnumerator<TKey>(source, index, cancellationToken);
		}

		// Token: 0x02000125 RID: 293
		private class DecimalAverageAggregationOperatorEnumerator<TKey> : InlinedAggregationOperatorEnumerator<Pair<decimal, long>>
		{
			// Token: 0x060008C2 RID: 2242 RVA: 0x0001BC1F File Offset: 0x00019E1F
			internal DecimalAverageAggregationOperatorEnumerator(QueryOperatorEnumerator<decimal, TKey> source, int partitionIndex, CancellationToken cancellationToken) : base(partitionIndex, cancellationToken)
			{
				this._source = source;
			}

			// Token: 0x060008C3 RID: 2243 RVA: 0x0001BC30 File Offset: 0x00019E30
			protected override bool MoveNextCore(ref Pair<decimal, long> currentElement)
			{
				decimal num = 0.0m;
				long num2 = 0L;
				QueryOperatorEnumerator<decimal, TKey> source = this._source;
				decimal d = 0m;
				TKey tkey = default(TKey);
				if (source.MoveNext(ref d, ref tkey))
				{
					int num3 = 0;
					do
					{
						if ((num3++ & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(this._cancellationToken);
						}
						num += d;
						checked
						{
							num2 += 1L;
						}
					}
					while (source.MoveNext(ref d, ref tkey));
					currentElement = new Pair<decimal, long>(num, num2);
					return true;
				}
				return false;
			}

			// Token: 0x060008C4 RID: 2244 RVA: 0x0001BCB2 File Offset: 0x00019EB2
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x04000637 RID: 1591
			private QueryOperatorEnumerator<decimal, TKey> _source;
		}
	}
}
