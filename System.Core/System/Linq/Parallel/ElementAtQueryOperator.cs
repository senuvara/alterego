﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000180 RID: 384
	internal sealed class ElementAtQueryOperator<TSource> : UnaryQueryOperator<TSource, TSource>
	{
		// Token: 0x06000A2C RID: 2604 RVA: 0x000202BC File Offset: 0x0001E4BC
		internal ElementAtQueryOperator(IEnumerable<TSource> child, int index) : base(child)
		{
			this._index = index;
			OrdinalIndexState ordinalIndexState = base.Child.OrdinalIndexState;
			if (ordinalIndexState.IsWorseThan(OrdinalIndexState.Correct))
			{
				this._prematureMerge = true;
				this._limitsParallelism = (ordinalIndexState != OrdinalIndexState.Shuffled);
			}
		}

		// Token: 0x06000A2D RID: 2605 RVA: 0x00020300 File Offset: 0x0001E500
		internal override QueryResults<TSource> Open(QuerySettings settings, bool preferStriping)
		{
			return new UnaryQueryOperator<TSource, TSource>.UnaryQueryOperatorResults(base.Child.Open(settings, false), this, settings, preferStriping);
		}

		// Token: 0x06000A2E RID: 2606 RVA: 0x00020318 File Offset: 0x0001E518
		internal override void WrapPartitionedStream<TKey>(PartitionedStream<TSource, TKey> inputStream, IPartitionedStreamRecipient<TSource> recipient, bool preferStriping, QuerySettings settings)
		{
			int partitionCount = inputStream.PartitionCount;
			PartitionedStream<TSource, int> partitionedStream;
			if (this._prematureMerge)
			{
				partitionedStream = QueryOperator<TSource>.ExecuteAndCollectResults<TKey>(inputStream, partitionCount, base.Child.OutputOrdered, preferStriping, settings).GetPartitionedStream();
			}
			else
			{
				partitionedStream = (PartitionedStream<TSource, int>)inputStream;
			}
			Shared<bool> resultFoundFlag = new Shared<bool>(false);
			PartitionedStream<TSource, int> partitionedStream2 = new PartitionedStream<TSource, int>(partitionCount, Util.GetDefaultComparer<int>(), OrdinalIndexState.Correct);
			for (int i = 0; i < partitionCount; i++)
			{
				partitionedStream2[i] = new ElementAtQueryOperator<TSource>.ElementAtQueryOperatorEnumerator(partitionedStream[i], this._index, resultFoundFlag, settings.CancellationState.MergedCancellationToken);
			}
			recipient.Receive<int>(partitionedStream2);
		}

		// Token: 0x06000A2F RID: 2607 RVA: 0x00003A6B File Offset: 0x00001C6B
		[ExcludeFromCodeCoverage]
		internal override IEnumerable<TSource> AsSequentialQuery(CancellationToken token)
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000166 RID: 358
		// (get) Token: 0x06000A30 RID: 2608 RVA: 0x000203AB File Offset: 0x0001E5AB
		internal override bool LimitsParallelism
		{
			get
			{
				return this._limitsParallelism;
			}
		}

		// Token: 0x06000A31 RID: 2609 RVA: 0x000203B4 File Offset: 0x0001E5B4
		internal bool Aggregate(out TSource result, bool withDefaultValue)
		{
			if (this.LimitsParallelism && base.SpecifiedQuerySettings.WithDefaults().ExecutionMode.Value != ParallelExecutionMode.ForceParallelism)
			{
				CancellationState cancellationState = base.SpecifiedQuerySettings.CancellationState;
				if (withDefaultValue)
				{
					IEnumerable<TSource> source = CancellableEnumerable.Wrap<TSource>(base.Child.AsSequentialQuery(cancellationState.ExternalCancellationToken), cancellationState.ExternalCancellationToken);
					result = ExceptionAggregator.WrapEnumerable<TSource>(source, cancellationState).ElementAtOrDefault(this._index);
				}
				else
				{
					IEnumerable<TSource> source2 = CancellableEnumerable.Wrap<TSource>(base.Child.AsSequentialQuery(cancellationState.ExternalCancellationToken), cancellationState.ExternalCancellationToken);
					result = ExceptionAggregator.WrapEnumerable<TSource>(source2, cancellationState).ElementAt(this._index);
				}
				return true;
			}
			using (IEnumerator<TSource> enumerator = base.GetEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered)))
			{
				if (enumerator.MoveNext())
				{
					TSource tsource = enumerator.Current;
					result = tsource;
					return true;
				}
			}
			result = default(TSource);
			return false;
		}

		// Token: 0x040006C6 RID: 1734
		private readonly int _index;

		// Token: 0x040006C7 RID: 1735
		private readonly bool _prematureMerge;

		// Token: 0x040006C8 RID: 1736
		private readonly bool _limitsParallelism;

		// Token: 0x02000181 RID: 385
		private class ElementAtQueryOperatorEnumerator : QueryOperatorEnumerator<TSource, int>
		{
			// Token: 0x06000A32 RID: 2610 RVA: 0x000204C8 File Offset: 0x0001E6C8
			internal ElementAtQueryOperatorEnumerator(QueryOperatorEnumerator<TSource, int> source, int index, Shared<bool> resultFoundFlag, CancellationToken cancellationToken)
			{
				this._source = source;
				this._index = index;
				this._resultFoundFlag = resultFoundFlag;
				this._cancellationToken = cancellationToken;
			}

			// Token: 0x06000A33 RID: 2611 RVA: 0x000204F0 File Offset: 0x0001E6F0
			internal override bool MoveNext(ref TSource currentElement, ref int currentKey)
			{
				int num = 0;
				while (this._source.MoveNext(ref currentElement, ref currentKey))
				{
					if ((num++ & 63) == 0)
					{
						CancellationState.ThrowIfCanceled(this._cancellationToken);
					}
					if (this._resultFoundFlag.Value)
					{
						break;
					}
					if (currentKey == this._index)
					{
						this._resultFoundFlag.Value = true;
						return true;
					}
				}
				return false;
			}

			// Token: 0x06000A34 RID: 2612 RVA: 0x0002054B File Offset: 0x0001E74B
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x040006C9 RID: 1737
			private QueryOperatorEnumerator<TSource, int> _source;

			// Token: 0x040006CA RID: 1738
			private int _index;

			// Token: 0x040006CB RID: 1739
			private Shared<bool> _resultFoundFlag;

			// Token: 0x040006CC RID: 1740
			private CancellationToken _cancellationToken;
		}
	}
}
