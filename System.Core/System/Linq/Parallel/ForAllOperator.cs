﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000185 RID: 389
	internal sealed class ForAllOperator<TInput> : UnaryQueryOperator<TInput, TInput>
	{
		// Token: 0x06000A3F RID: 2623 RVA: 0x00020800 File Offset: 0x0001EA00
		internal ForAllOperator(IEnumerable<TInput> child, Action<TInput> elementAction) : base(child)
		{
			this._elementAction = elementAction;
		}

		// Token: 0x06000A40 RID: 2624 RVA: 0x00020810 File Offset: 0x0001EA10
		internal void RunSynchronously()
		{
			Shared<bool> topLevelDisposedFlag = new Shared<bool>(false);
			CancellationTokenSource topLevelCancellationTokenSource = new CancellationTokenSource();
			QuerySettings querySettings = base.SpecifiedQuerySettings.WithPerExecutionSettings(topLevelCancellationTokenSource, topLevelDisposedFlag).WithDefaults();
			QueryLifecycle.LogicalQueryExecutionBegin(querySettings.QueryId);
			base.GetOpenedEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered), true, true, querySettings);
			querySettings.CleanStateAtQueryEnd();
			QueryLifecycle.LogicalQueryExecutionEnd(querySettings.QueryId);
		}

		// Token: 0x06000A41 RID: 2625 RVA: 0x0001FDEF File Offset: 0x0001DFEF
		internal override QueryResults<TInput> Open(QuerySettings settings, bool preferStriping)
		{
			return new UnaryQueryOperator<TInput, TInput>.UnaryQueryOperatorResults(base.Child.Open(settings, preferStriping), this, settings, preferStriping);
		}

		// Token: 0x06000A42 RID: 2626 RVA: 0x00020874 File Offset: 0x0001EA74
		internal override void WrapPartitionedStream<TKey>(PartitionedStream<TInput, TKey> inputStream, IPartitionedStreamRecipient<TInput> recipient, bool preferStriping, QuerySettings settings)
		{
			int partitionCount = inputStream.PartitionCount;
			PartitionedStream<TInput, int> partitionedStream = new PartitionedStream<TInput, int>(partitionCount, Util.GetDefaultComparer<int>(), OrdinalIndexState.Correct);
			for (int i = 0; i < partitionCount; i++)
			{
				partitionedStream[i] = new ForAllOperator<TInput>.ForAllEnumerator<TKey>(inputStream[i], this._elementAction, settings.CancellationState.MergedCancellationToken);
			}
			recipient.Receive<int>(partitionedStream);
		}

		// Token: 0x06000A43 RID: 2627 RVA: 0x000180B6 File Offset: 0x000162B6
		[ExcludeFromCodeCoverage]
		internal override IEnumerable<TInput> AsSequentialQuery(CancellationToken token)
		{
			throw new InvalidOperationException();
		}

		// Token: 0x17000168 RID: 360
		// (get) Token: 0x06000A44 RID: 2628 RVA: 0x00002285 File Offset: 0x00000485
		internal override bool LimitsParallelism
		{
			get
			{
				return false;
			}
		}

		// Token: 0x040006D9 RID: 1753
		private readonly Action<TInput> _elementAction;

		// Token: 0x02000186 RID: 390
		private class ForAllEnumerator<TKey> : QueryOperatorEnumerator<TInput, int>
		{
			// Token: 0x06000A45 RID: 2629 RVA: 0x000208CD File Offset: 0x0001EACD
			internal ForAllEnumerator(QueryOperatorEnumerator<TInput, TKey> source, Action<TInput> elementAction, CancellationToken cancellationToken)
			{
				this._source = source;
				this._elementAction = elementAction;
				this._cancellationToken = cancellationToken;
			}

			// Token: 0x06000A46 RID: 2630 RVA: 0x000208EC File Offset: 0x0001EAEC
			internal override bool MoveNext(ref TInput currentElement, ref int currentKey)
			{
				TInput obj = default(TInput);
				TKey tkey = default(TKey);
				int num = 0;
				while (this._source.MoveNext(ref obj, ref tkey))
				{
					if ((num++ & 63) == 0)
					{
						CancellationState.ThrowIfCanceled(this._cancellationToken);
					}
					this._elementAction(obj);
				}
				return false;
			}

			// Token: 0x06000A47 RID: 2631 RVA: 0x00020940 File Offset: 0x0001EB40
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x040006DA RID: 1754
			private readonly QueryOperatorEnumerator<TInput, TKey> _source;

			// Token: 0x040006DB RID: 1755
			private readonly Action<TInput> _elementAction;

			// Token: 0x040006DC RID: 1756
			private CancellationToken _cancellationToken;
		}
	}
}
