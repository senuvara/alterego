﻿using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace System.Linq.Parallel
{
	// Token: 0x020001C4 RID: 452
	internal static class SpoolingTask
	{
		// Token: 0x06000B2A RID: 2858 RVA: 0x00023D3C File Offset: 0x00021F3C
		internal static void SpoolStopAndGo<TInputOutput, TIgnoreKey>(QueryTaskGroupState groupState, PartitionedStream<TInputOutput, TIgnoreKey> partitions, SynchronousChannel<TInputOutput>[] channels, TaskScheduler taskScheduler)
		{
			Task task = new Task(delegate()
			{
				int num = partitions.PartitionCount - 1;
				for (int i = 0; i < num; i++)
				{
					new StopAndGoSpoolingTask<TInputOutput, TIgnoreKey>(i, groupState, partitions[i], channels[i]).RunAsynchronously(taskScheduler);
				}
				new StopAndGoSpoolingTask<TInputOutput, TIgnoreKey>(num, groupState, partitions[num], channels[num]).RunSynchronously(taskScheduler);
			});
			groupState.QueryBegin(task);
			task.RunSynchronously(taskScheduler);
			groupState.QueryEnd(false);
		}

		// Token: 0x06000B2B RID: 2859 RVA: 0x00023DA4 File Offset: 0x00021FA4
		internal static void SpoolPipeline<TInputOutput, TIgnoreKey>(QueryTaskGroupState groupState, PartitionedStream<TInputOutput, TIgnoreKey> partitions, AsynchronousChannel<TInputOutput>[] channels, TaskScheduler taskScheduler)
		{
			Task task = new Task(delegate()
			{
				for (int i = 0; i < partitions.PartitionCount; i++)
				{
					new PipelineSpoolingTask<TInputOutput, TIgnoreKey>(i, groupState, partitions[i], channels[i]).RunAsynchronously(taskScheduler);
				}
			});
			groupState.QueryBegin(task);
			task.Start(taskScheduler);
		}

		// Token: 0x06000B2C RID: 2860 RVA: 0x00023E00 File Offset: 0x00022000
		internal static void SpoolForAll<TInputOutput, TIgnoreKey>(QueryTaskGroupState groupState, PartitionedStream<TInputOutput, TIgnoreKey> partitions, TaskScheduler taskScheduler)
		{
			Task task = new Task(delegate()
			{
				int num = partitions.PartitionCount - 1;
				for (int i = 0; i < num; i++)
				{
					new ForAllSpoolingTask<TInputOutput, TIgnoreKey>(i, groupState, partitions[i]).RunAsynchronously(taskScheduler);
				}
				new ForAllSpoolingTask<TInputOutput, TIgnoreKey>(num, groupState, partitions[num]).RunSynchronously(taskScheduler);
			});
			groupState.QueryBegin(task);
			task.RunSynchronously(taskScheduler);
			groupState.QueryEnd(false);
		}

		// Token: 0x020001C5 RID: 453
		[CompilerGenerated]
		private sealed class <>c__DisplayClass0_0<TInputOutput, TIgnoreKey>
		{
			// Token: 0x06000B2D RID: 2861 RVA: 0x00002310 File Offset: 0x00000510
			public <>c__DisplayClass0_0()
			{
			}

			// Token: 0x06000B2E RID: 2862 RVA: 0x00023E60 File Offset: 0x00022060
			internal void <SpoolStopAndGo>b__0()
			{
				int num = this.partitions.PartitionCount - 1;
				for (int i = 0; i < num; i++)
				{
					new StopAndGoSpoolingTask<TInputOutput, TIgnoreKey>(i, this.groupState, this.partitions[i], this.channels[i]).RunAsynchronously(this.taskScheduler);
				}
				new StopAndGoSpoolingTask<TInputOutput, TIgnoreKey>(num, this.groupState, this.partitions[num], this.channels[num]).RunSynchronously(this.taskScheduler);
			}

			// Token: 0x040007AD RID: 1965
			public PartitionedStream<TInputOutput, TIgnoreKey> partitions;

			// Token: 0x040007AE RID: 1966
			public QueryTaskGroupState groupState;

			// Token: 0x040007AF RID: 1967
			public SynchronousChannel<TInputOutput>[] channels;

			// Token: 0x040007B0 RID: 1968
			public TaskScheduler taskScheduler;
		}

		// Token: 0x020001C6 RID: 454
		[CompilerGenerated]
		private sealed class <>c__DisplayClass1_0<TInputOutput, TIgnoreKey>
		{
			// Token: 0x06000B2F RID: 2863 RVA: 0x00002310 File Offset: 0x00000510
			public <>c__DisplayClass1_0()
			{
			}

			// Token: 0x06000B30 RID: 2864 RVA: 0x00023EE0 File Offset: 0x000220E0
			internal void <SpoolPipeline>b__0()
			{
				for (int i = 0; i < this.partitions.PartitionCount; i++)
				{
					new PipelineSpoolingTask<TInputOutput, TIgnoreKey>(i, this.groupState, this.partitions[i], this.channels[i]).RunAsynchronously(this.taskScheduler);
				}
			}

			// Token: 0x040007B1 RID: 1969
			public QueryTaskGroupState groupState;

			// Token: 0x040007B2 RID: 1970
			public PartitionedStream<TInputOutput, TIgnoreKey> partitions;

			// Token: 0x040007B3 RID: 1971
			public AsynchronousChannel<TInputOutput>[] channels;

			// Token: 0x040007B4 RID: 1972
			public TaskScheduler taskScheduler;
		}

		// Token: 0x020001C7 RID: 455
		[CompilerGenerated]
		private sealed class <>c__DisplayClass2_0<TInputOutput, TIgnoreKey>
		{
			// Token: 0x06000B31 RID: 2865 RVA: 0x00002310 File Offset: 0x00000510
			public <>c__DisplayClass2_0()
			{
			}

			// Token: 0x06000B32 RID: 2866 RVA: 0x00023F30 File Offset: 0x00022130
			internal void <SpoolForAll>b__0()
			{
				int num = this.partitions.PartitionCount - 1;
				for (int i = 0; i < num; i++)
				{
					new ForAllSpoolingTask<TInputOutput, TIgnoreKey>(i, this.groupState, this.partitions[i]).RunAsynchronously(this.taskScheduler);
				}
				new ForAllSpoolingTask<TInputOutput, TIgnoreKey>(num, this.groupState, this.partitions[num]).RunSynchronously(this.taskScheduler);
			}

			// Token: 0x040007B5 RID: 1973
			public PartitionedStream<TInputOutput, TIgnoreKey> partitions;

			// Token: 0x040007B6 RID: 1974
			public QueryTaskGroupState groupState;

			// Token: 0x040007B7 RID: 1975
			public TaskScheduler taskScheduler;
		}
	}
}
