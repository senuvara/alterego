﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace System.Linq.Parallel
{
	// Token: 0x020000EE RID: 238
	internal class OrderPreservingMergeHelper<TInputOutput, TKey> : IMergeHelper<!0>
	{
		// Token: 0x06000804 RID: 2052 RVA: 0x00017F47 File Offset: 0x00016147
		internal OrderPreservingMergeHelper(PartitionedStream<TInputOutput, TKey> partitions, TaskScheduler taskScheduler, CancellationState cancellationState, int queryId)
		{
			this._taskGroupState = new QueryTaskGroupState(cancellationState, queryId);
			this._partitions = partitions;
			this._results = new Shared<TInputOutput[]>(null);
			this._taskScheduler = taskScheduler;
		}

		// Token: 0x06000805 RID: 2053 RVA: 0x00017F77 File Offset: 0x00016177
		void IMergeHelper<!0>.Execute()
		{
			OrderPreservingSpoolingTask<TInputOutput, TKey>.Spool(this._taskGroupState, this._partitions, this._results, this._taskScheduler);
		}

		// Token: 0x06000806 RID: 2054 RVA: 0x00017F96 File Offset: 0x00016196
		IEnumerator<TInputOutput> IMergeHelper<!0>.GetEnumerator()
		{
			return this._results.Value.GetEnumerator();
		}

		// Token: 0x06000807 RID: 2055 RVA: 0x00017FA8 File Offset: 0x000161A8
		public TInputOutput[] GetResultsAsArray()
		{
			return this._results.Value;
		}

		// Token: 0x04000550 RID: 1360
		private QueryTaskGroupState _taskGroupState;

		// Token: 0x04000551 RID: 1361
		private PartitionedStream<TInputOutput, TKey> _partitions;

		// Token: 0x04000552 RID: 1362
		private Shared<TInputOutput[]> _results;

		// Token: 0x04000553 RID: 1363
		private TaskScheduler _taskScheduler;
	}
}
