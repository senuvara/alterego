﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x0200013A RID: 314
	internal sealed class IntMinMaxAggregationOperator : InlinedAggregationOperator<int, int, int>
	{
		// Token: 0x06000906 RID: 2310 RVA: 0x0001C9F9 File Offset: 0x0001ABF9
		internal IntMinMaxAggregationOperator(IEnumerable<int> child, int sign) : base(child)
		{
			this._sign = sign;
		}

		// Token: 0x06000907 RID: 2311 RVA: 0x0001CA0C File Offset: 0x0001AC0C
		protected override int InternalAggregate(ref Exception singularExceptionToThrow)
		{
			int result;
			using (IEnumerator<int> enumerator = this.GetEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered), true))
			{
				if (!enumerator.MoveNext())
				{
					singularExceptionToThrow = new InvalidOperationException("Sequence contains no elements");
					result = 0;
				}
				else
				{
					int num = enumerator.Current;
					if (this._sign == -1)
					{
						while (enumerator.MoveNext())
						{
							int num2 = enumerator.Current;
							if (num2 < num)
							{
								num = num2;
							}
						}
					}
					else
					{
						while (enumerator.MoveNext())
						{
							int num3 = enumerator.Current;
							if (num3 > num)
							{
								num = num3;
							}
						}
					}
					result = num;
				}
			}
			return result;
		}

		// Token: 0x06000908 RID: 2312 RVA: 0x0001CAA0 File Offset: 0x0001ACA0
		protected override QueryOperatorEnumerator<int, int> CreateEnumerator<TKey>(int index, int count, QueryOperatorEnumerator<int, TKey> source, object sharedData, CancellationToken cancellationToken)
		{
			return new IntMinMaxAggregationOperator.IntMinMaxAggregationOperatorEnumerator<TKey>(source, index, this._sign, cancellationToken);
		}

		// Token: 0x0400064A RID: 1610
		private readonly int _sign;

		// Token: 0x0200013B RID: 315
		private class IntMinMaxAggregationOperatorEnumerator<TKey> : InlinedAggregationOperatorEnumerator<int>
		{
			// Token: 0x06000909 RID: 2313 RVA: 0x0001CAB1 File Offset: 0x0001ACB1
			internal IntMinMaxAggregationOperatorEnumerator(QueryOperatorEnumerator<int, TKey> source, int partitionIndex, int sign, CancellationToken cancellationToken) : base(partitionIndex, cancellationToken)
			{
				this._source = source;
				this._sign = sign;
			}

			// Token: 0x0600090A RID: 2314 RVA: 0x0001CACC File Offset: 0x0001ACCC
			protected override bool MoveNextCore(ref int currentElement)
			{
				QueryOperatorEnumerator<int, TKey> source = this._source;
				TKey tkey = default(TKey);
				if (source.MoveNext(ref currentElement, ref tkey))
				{
					int num = 0;
					if (this._sign == -1)
					{
						int num2 = 0;
						while (source.MoveNext(ref num2, ref tkey))
						{
							if ((num++ & 63) == 0)
							{
								CancellationState.ThrowIfCanceled(this._cancellationToken);
							}
							if (num2 < currentElement)
							{
								currentElement = num2;
							}
						}
					}
					else
					{
						int num3 = 0;
						while (source.MoveNext(ref num3, ref tkey))
						{
							if ((num++ & 63) == 0)
							{
								CancellationState.ThrowIfCanceled(this._cancellationToken);
							}
							if (num3 > currentElement)
							{
								currentElement = num3;
							}
						}
					}
					return true;
				}
				return false;
			}

			// Token: 0x0600090B RID: 2315 RVA: 0x0001CB60 File Offset: 0x0001AD60
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x0400064B RID: 1611
			private readonly QueryOperatorEnumerator<int, TKey> _source;

			// Token: 0x0400064C RID: 1612
			private readonly int _sign;
		}
	}
}
