﻿using System;

namespace System.Linq.Parallel
{
	// Token: 0x020000E5 RID: 229
	internal class JaggedArray<TElement>
	{
		// Token: 0x060007DC RID: 2012 RVA: 0x00017534 File Offset: 0x00015734
		public static TElement[][] Allocate(int size1, int size2)
		{
			TElement[][] array = new TElement[size1][];
			for (int i = 0; i < size1; i++)
			{
				array[i] = new TElement[size2];
			}
			return array;
		}

		// Token: 0x060007DD RID: 2013 RVA: 0x00002310 File Offset: 0x00000510
		public JaggedArray()
		{
		}
	}
}
