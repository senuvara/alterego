﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x020001B2 RID: 434
	internal sealed class TakeOrSkipWhileQueryOperator<TResult> : UnaryQueryOperator<TResult, TResult>
	{
		// Token: 0x06000AE6 RID: 2790 RVA: 0x00022C17 File Offset: 0x00020E17
		internal TakeOrSkipWhileQueryOperator(IEnumerable<TResult> child, Func<TResult, bool> predicate, Func<TResult, int, bool> indexedPredicate, bool take) : base(child)
		{
			this._predicate = predicate;
			this._indexedPredicate = indexedPredicate;
			this._take = take;
			this.InitOrderIndexState();
		}

		// Token: 0x06000AE7 RID: 2791 RVA: 0x00022C3C File Offset: 0x00020E3C
		private void InitOrderIndexState()
		{
			OrdinalIndexState state = OrdinalIndexState.Increasing;
			OrdinalIndexState ordinalIndexState = base.Child.OrdinalIndexState;
			if (this._indexedPredicate != null)
			{
				state = OrdinalIndexState.Correct;
				this._limitsParallelism = (ordinalIndexState == OrdinalIndexState.Increasing);
			}
			OrdinalIndexState ordinalIndexState2 = ordinalIndexState.Worse(OrdinalIndexState.Correct);
			if (ordinalIndexState2.IsWorseThan(state))
			{
				this._prematureMerge = true;
			}
			if (!this._take)
			{
				ordinalIndexState2 = ordinalIndexState2.Worse(OrdinalIndexState.Increasing);
			}
			base.SetOrdinalIndexState(ordinalIndexState2);
		}

		// Token: 0x06000AE8 RID: 2792 RVA: 0x00022C9C File Offset: 0x00020E9C
		internal override void WrapPartitionedStream<TKey>(PartitionedStream<TResult, TKey> inputStream, IPartitionedStreamRecipient<TResult> recipient, bool preferStriping, QuerySettings settings)
		{
			if (this._prematureMerge)
			{
				PartitionedStream<TResult, int> partitionedStream = QueryOperator<TResult>.ExecuteAndCollectResults<TKey>(inputStream, inputStream.PartitionCount, base.Child.OutputOrdered, preferStriping, settings).GetPartitionedStream();
				this.WrapHelper<int>(partitionedStream, recipient, settings);
				return;
			}
			this.WrapHelper<TKey>(inputStream, recipient, settings);
		}

		// Token: 0x06000AE9 RID: 2793 RVA: 0x00022CE8 File Offset: 0x00020EE8
		private void WrapHelper<TKey>(PartitionedStream<TResult, TKey> inputStream, IPartitionedStreamRecipient<TResult> recipient, QuerySettings settings)
		{
			int partitionCount = inputStream.PartitionCount;
			TakeOrSkipWhileQueryOperator<TResult>.OperatorState<TKey> operatorState = new TakeOrSkipWhileQueryOperator<TResult>.OperatorState<TKey>();
			CountdownEvent sharedBarrier = new CountdownEvent(partitionCount);
			Func<TResult, TKey, bool> indexedPredicate = (Func<TResult, TKey, bool>)this._indexedPredicate;
			PartitionedStream<TResult, TKey> partitionedStream = new PartitionedStream<TResult, TKey>(partitionCount, inputStream.KeyComparer, this.OrdinalIndexState);
			for (int i = 0; i < partitionCount; i++)
			{
				partitionedStream[i] = new TakeOrSkipWhileQueryOperator<TResult>.TakeOrSkipWhileQueryOperatorEnumerator<TKey>(inputStream[i], this._predicate, indexedPredicate, this._take, operatorState, sharedBarrier, settings.CancellationState.MergedCancellationToken, inputStream.KeyComparer);
			}
			recipient.Receive<TKey>(partitionedStream);
		}

		// Token: 0x06000AEA RID: 2794 RVA: 0x00022D78 File Offset: 0x00020F78
		internal override QueryResults<TResult> Open(QuerySettings settings, bool preferStriping)
		{
			return new UnaryQueryOperator<TResult, TResult>.UnaryQueryOperatorResults(base.Child.Open(settings, true), this, settings, preferStriping);
		}

		// Token: 0x06000AEB RID: 2795 RVA: 0x00022D90 File Offset: 0x00020F90
		internal override IEnumerable<TResult> AsSequentialQuery(CancellationToken token)
		{
			if (this._take)
			{
				if (this._indexedPredicate != null)
				{
					return base.Child.AsSequentialQuery(token).TakeWhile(this._indexedPredicate);
				}
				return base.Child.AsSequentialQuery(token).TakeWhile(this._predicate);
			}
			else
			{
				if (this._indexedPredicate != null)
				{
					return CancellableEnumerable.Wrap<TResult>(base.Child.AsSequentialQuery(token), token).SkipWhile(this._indexedPredicate);
				}
				return CancellableEnumerable.Wrap<TResult>(base.Child.AsSequentialQuery(token), token).SkipWhile(this._predicate);
			}
		}

		// Token: 0x17000180 RID: 384
		// (get) Token: 0x06000AEC RID: 2796 RVA: 0x00022E20 File Offset: 0x00021020
		internal override bool LimitsParallelism
		{
			get
			{
				return this._limitsParallelism;
			}
		}

		// Token: 0x0400075C RID: 1884
		private Func<TResult, bool> _predicate;

		// Token: 0x0400075D RID: 1885
		private Func<TResult, int, bool> _indexedPredicate;

		// Token: 0x0400075E RID: 1886
		private readonly bool _take;

		// Token: 0x0400075F RID: 1887
		private bool _prematureMerge;

		// Token: 0x04000760 RID: 1888
		private bool _limitsParallelism;

		// Token: 0x020001B3 RID: 435
		private class TakeOrSkipWhileQueryOperatorEnumerator<TKey> : QueryOperatorEnumerator<TResult, TKey>
		{
			// Token: 0x06000AED RID: 2797 RVA: 0x00022E28 File Offset: 0x00021028
			internal TakeOrSkipWhileQueryOperatorEnumerator(QueryOperatorEnumerator<TResult, TKey> source, Func<TResult, bool> predicate, Func<TResult, TKey, bool> indexedPredicate, bool take, TakeOrSkipWhileQueryOperator<TResult>.OperatorState<TKey> operatorState, CountdownEvent sharedBarrier, CancellationToken cancelToken, IComparer<TKey> keyComparer)
			{
				this._source = source;
				this._predicate = predicate;
				this._indexedPredicate = indexedPredicate;
				this._take = take;
				this._operatorState = operatorState;
				this._sharedBarrier = sharedBarrier;
				this._cancellationToken = cancelToken;
				this._keyComparer = keyComparer;
			}

			// Token: 0x06000AEE RID: 2798 RVA: 0x00022E78 File Offset: 0x00021078
			internal override bool MoveNext(ref TResult currentElement, ref TKey currentKey)
			{
				if (this._buffer == null)
				{
					List<Pair<TResult, TKey>> list = new List<Pair<TResult, TKey>>();
					try
					{
						TResult tresult = default(TResult);
						TKey tkey = default(TKey);
						int num = 0;
						while (this._source.MoveNext(ref tresult, ref tkey))
						{
							if ((num++ & 63) == 0)
							{
								CancellationState.ThrowIfCanceled(this._cancellationToken);
							}
							list.Add(new Pair<TResult, TKey>(tresult, tkey));
							if (this._updatesSeen != this._operatorState._updatesDone)
							{
								TakeOrSkipWhileQueryOperator<TResult>.OperatorState<TKey> operatorState = this._operatorState;
								lock (operatorState)
								{
									this._currentLowKey = this._operatorState._currentLowKey;
									this._updatesSeen = this._operatorState._updatesDone;
								}
							}
							if (this._updatesSeen > 0 && this._keyComparer.Compare(tkey, this._currentLowKey) > 0)
							{
								break;
							}
							bool flag2;
							if (this._predicate != null)
							{
								flag2 = this._predicate(tresult);
							}
							else
							{
								flag2 = this._indexedPredicate(tresult, tkey);
							}
							if (!flag2)
							{
								TakeOrSkipWhileQueryOperator<TResult>.OperatorState<TKey> operatorState = this._operatorState;
								lock (operatorState)
								{
									if (this._operatorState._updatesDone == 0 || this._keyComparer.Compare(this._operatorState._currentLowKey, tkey) > 0)
									{
										this._currentLowKey = (this._operatorState._currentLowKey = tkey);
										TakeOrSkipWhileQueryOperator<TResult>.OperatorState<TKey> operatorState2 = this._operatorState;
										int num2 = operatorState2._updatesDone + 1;
										operatorState2._updatesDone = num2;
										this._updatesSeen = num2;
									}
									break;
								}
							}
						}
					}
					finally
					{
						this._sharedBarrier.Signal();
					}
					this._sharedBarrier.Wait(this._cancellationToken);
					this._buffer = list;
					this._bufferIndex = new Shared<int>(-1);
				}
				if (this._take)
				{
					if (this._bufferIndex.Value >= this._buffer.Count - 1)
					{
						return false;
					}
					this._bufferIndex.Value++;
					currentElement = this._buffer[this._bufferIndex.Value].First;
					currentKey = this._buffer[this._bufferIndex.Value].Second;
					return this._operatorState._updatesDone == 0 || this._keyComparer.Compare(this._operatorState._currentLowKey, currentKey) > 0;
				}
				else
				{
					if (this._operatorState._updatesDone == 0)
					{
						return false;
					}
					if (this._bufferIndex.Value < this._buffer.Count - 1)
					{
						this._bufferIndex.Value++;
						while (this._bufferIndex.Value < this._buffer.Count)
						{
							if (this._keyComparer.Compare(this._buffer[this._bufferIndex.Value].Second, this._operatorState._currentLowKey) >= 0)
							{
								currentElement = this._buffer[this._bufferIndex.Value].First;
								currentKey = this._buffer[this._bufferIndex.Value].Second;
								return true;
							}
							this._bufferIndex.Value++;
						}
					}
					return this._source.MoveNext(ref currentElement, ref currentKey);
				}
			}

			// Token: 0x06000AEF RID: 2799 RVA: 0x00023254 File Offset: 0x00021454
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x04000761 RID: 1889
			private readonly QueryOperatorEnumerator<TResult, TKey> _source;

			// Token: 0x04000762 RID: 1890
			private readonly Func<TResult, bool> _predicate;

			// Token: 0x04000763 RID: 1891
			private readonly Func<TResult, TKey, bool> _indexedPredicate;

			// Token: 0x04000764 RID: 1892
			private readonly bool _take;

			// Token: 0x04000765 RID: 1893
			private readonly IComparer<TKey> _keyComparer;

			// Token: 0x04000766 RID: 1894
			private readonly TakeOrSkipWhileQueryOperator<TResult>.OperatorState<TKey> _operatorState;

			// Token: 0x04000767 RID: 1895
			private readonly CountdownEvent _sharedBarrier;

			// Token: 0x04000768 RID: 1896
			private readonly CancellationToken _cancellationToken;

			// Token: 0x04000769 RID: 1897
			private List<Pair<TResult, TKey>> _buffer;

			// Token: 0x0400076A RID: 1898
			private Shared<int> _bufferIndex;

			// Token: 0x0400076B RID: 1899
			private int _updatesSeen;

			// Token: 0x0400076C RID: 1900
			private TKey _currentLowKey;
		}

		// Token: 0x020001B4 RID: 436
		private class OperatorState<TKey>
		{
			// Token: 0x06000AF0 RID: 2800 RVA: 0x00002310 File Offset: 0x00000510
			public OperatorState()
			{
			}

			// Token: 0x0400076D RID: 1901
			internal volatile int _updatesDone;

			// Token: 0x0400076E RID: 1902
			internal TKey _currentLowKey;
		}
	}
}
