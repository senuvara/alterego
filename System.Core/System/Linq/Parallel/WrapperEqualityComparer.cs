﻿using System;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x020001ED RID: 493
	internal struct WrapperEqualityComparer<T> : IEqualityComparer<Wrapper<T>>
	{
		// Token: 0x06000BD1 RID: 3025 RVA: 0x00025F07 File Offset: 0x00024107
		internal WrapperEqualityComparer(IEqualityComparer<T> comparer)
		{
			if (comparer == null)
			{
				this._comparer = EqualityComparer<T>.Default;
				return;
			}
			this._comparer = comparer;
		}

		// Token: 0x06000BD2 RID: 3026 RVA: 0x00025F1F File Offset: 0x0002411F
		public bool Equals(Wrapper<T> x, Wrapper<T> y)
		{
			return this._comparer.Equals(x.Value, y.Value);
		}

		// Token: 0x06000BD3 RID: 3027 RVA: 0x00025F38 File Offset: 0x00024138
		public int GetHashCode(Wrapper<T> x)
		{
			return this._comparer.GetHashCode(x.Value);
		}

		// Token: 0x0400081A RID: 2074
		private IEqualityComparer<T> _comparer;
	}
}
