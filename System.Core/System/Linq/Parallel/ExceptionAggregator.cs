﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace System.Linq.Parallel
{
	// Token: 0x020001CE RID: 462
	internal static class ExceptionAggregator
	{
		// Token: 0x06000B4A RID: 2890 RVA: 0x0002438F File Offset: 0x0002258F
		internal static IEnumerable<TElement> WrapEnumerable<TElement>(IEnumerable<TElement> source, CancellationState cancellationState)
		{
			using (IEnumerator<TElement> enumerator = source.GetEnumerator())
			{
				for (;;)
				{
					TElement telement = default(TElement);
					try
					{
						if (!enumerator.MoveNext())
						{
							yield break;
						}
						telement = enumerator.Current;
					}
					catch (Exception ex)
					{
						ExceptionAggregator.ThrowOCEorAggregateException(ex, cancellationState);
					}
					yield return telement;
				}
			}
			yield break;
			yield break;
		}

		// Token: 0x06000B4B RID: 2891 RVA: 0x000243A6 File Offset: 0x000225A6
		internal static IEnumerable<TElement> WrapQueryEnumerator<TElement, TIgnoreKey>(QueryOperatorEnumerator<TElement, TIgnoreKey> source, CancellationState cancellationState)
		{
			TElement elem = default(TElement);
			TIgnoreKey ignoreKey = default(TIgnoreKey);
			try
			{
				for (;;)
				{
					try
					{
						if (!source.MoveNext(ref elem, ref ignoreKey))
						{
							yield break;
						}
					}
					catch (Exception ex)
					{
						ExceptionAggregator.ThrowOCEorAggregateException(ex, cancellationState);
					}
					yield return elem;
				}
			}
			finally
			{
				source.Dispose();
			}
			yield break;
			yield break;
		}

		// Token: 0x06000B4C RID: 2892 RVA: 0x000243BD File Offset: 0x000225BD
		internal static void ThrowOCEorAggregateException(Exception ex, CancellationState cancellationState)
		{
			if (ExceptionAggregator.ThrowAnOCE(ex, cancellationState))
			{
				CancellationState.ThrowWithStandardMessageIfCanceled(cancellationState.ExternalCancellationToken);
				return;
			}
			throw new AggregateException(new Exception[]
			{
				ex
			});
		}

		// Token: 0x06000B4D RID: 2893 RVA: 0x000243E3 File Offset: 0x000225E3
		internal static Func<T, U> WrapFunc<T, U>(Func<T, U> f, CancellationState cancellationState)
		{
			return delegate(T t)
			{
				U result = default(U);
				try
				{
					result = f(t);
				}
				catch (Exception ex)
				{
					ExceptionAggregator.ThrowOCEorAggregateException(ex, cancellationState);
				}
				return result;
			};
		}

		// Token: 0x06000B4E RID: 2894 RVA: 0x00024404 File Offset: 0x00022604
		private static bool ThrowAnOCE(Exception ex, CancellationState cancellationState)
		{
			OperationCanceledException ex2 = ex as OperationCanceledException;
			return (ex2 != null && ex2.CancellationToken == cancellationState.ExternalCancellationToken && cancellationState.ExternalCancellationToken.IsCancellationRequested) || (ex2 != null && ex2.CancellationToken == cancellationState.MergedCancellationToken && cancellationState.MergedCancellationToken.IsCancellationRequested && cancellationState.ExternalCancellationToken.IsCancellationRequested);
		}

		// Token: 0x020001CF RID: 463
		[CompilerGenerated]
		private sealed class <WrapEnumerable>d__0<TElement> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x06000B4F RID: 2895 RVA: 0x00024473 File Offset: 0x00022673
			[DebuggerHidden]
			public <WrapEnumerable>d__0(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000B50 RID: 2896 RVA: 0x00024490 File Offset: 0x00022690
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x06000B51 RID: 2897 RVA: 0x000244C8 File Offset: 0x000226C8
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
					}
					TElement telement = default(TElement);
					try
					{
						if (!enumerator.MoveNext())
						{
							result = false;
							goto IL_82;
						}
						telement = enumerator.Current;
					}
					catch (Exception ex)
					{
						ExceptionAggregator.ThrowOCEorAggregateException(ex, cancellationState);
					}
					this.<>2__current = telement;
					this.<>1__state = 1;
					return true;
					IL_82:
					this.<>m__Finally1();
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x06000B52 RID: 2898 RVA: 0x00024584 File Offset: 0x00022784
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x1700018A RID: 394
			// (get) Token: 0x06000B53 RID: 2899 RVA: 0x000245A0 File Offset: 0x000227A0
			TElement IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000B54 RID: 2900 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700018B RID: 395
			// (get) Token: 0x06000B55 RID: 2901 RVA: 0x000245A8 File Offset: 0x000227A8
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000B56 RID: 2902 RVA: 0x000245B8 File Offset: 0x000227B8
			[DebuggerHidden]
			IEnumerator<TElement> IEnumerable<!0>.GetEnumerator()
			{
				ExceptionAggregator.<WrapEnumerable>d__0<TElement> <WrapEnumerable>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<WrapEnumerable>d__ = this;
				}
				else
				{
					<WrapEnumerable>d__ = new ExceptionAggregator.<WrapEnumerable>d__0<TElement>(0);
				}
				<WrapEnumerable>d__.source = source;
				<WrapEnumerable>d__.cancellationState = cancellationState;
				return <WrapEnumerable>d__;
			}

			// Token: 0x06000B57 RID: 2903 RVA: 0x00024607 File Offset: 0x00022807
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TElement>.GetEnumerator();
			}

			// Token: 0x040007C6 RID: 1990
			private int <>1__state;

			// Token: 0x040007C7 RID: 1991
			private TElement <>2__current;

			// Token: 0x040007C8 RID: 1992
			private int <>l__initialThreadId;

			// Token: 0x040007C9 RID: 1993
			private IEnumerable<TElement> source;

			// Token: 0x040007CA RID: 1994
			public IEnumerable<TElement> <>3__source;

			// Token: 0x040007CB RID: 1995
			private IEnumerator<TElement> <enumerator>5__1;

			// Token: 0x040007CC RID: 1996
			private CancellationState cancellationState;

			// Token: 0x040007CD RID: 1997
			public CancellationState <>3__cancellationState;
		}

		// Token: 0x020001D0 RID: 464
		[CompilerGenerated]
		private sealed class <WrapQueryEnumerator>d__1<TElement, TIgnoreKey> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x06000B58 RID: 2904 RVA: 0x0002460F File Offset: 0x0002280F
			[DebuggerHidden]
			public <WrapQueryEnumerator>d__1(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000B59 RID: 2905 RVA: 0x0002462C File Offset: 0x0002282C
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x06000B5A RID: 2906 RVA: 0x00024664 File Offset: 0x00022864
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						elem = default(TElement);
						ignoreKey = default(TIgnoreKey);
						this.<>1__state = -3;
					}
					try
					{
						if (!source.MoveNext(ref elem, ref ignoreKey))
						{
							result = false;
							goto IL_8A;
						}
					}
					catch (Exception ex)
					{
						ExceptionAggregator.ThrowOCEorAggregateException(ex, cancellationState);
					}
					this.<>2__current = elem;
					this.<>1__state = 1;
					return true;
					IL_8A:
					this.<>m__Finally1();
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x06000B5B RID: 2907 RVA: 0x00024728 File Offset: 0x00022928
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				source.Dispose();
			}

			// Token: 0x1700018C RID: 396
			// (get) Token: 0x06000B5C RID: 2908 RVA: 0x0002473C File Offset: 0x0002293C
			TElement IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000B5D RID: 2909 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700018D RID: 397
			// (get) Token: 0x06000B5E RID: 2910 RVA: 0x00024744 File Offset: 0x00022944
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000B5F RID: 2911 RVA: 0x00024754 File Offset: 0x00022954
			[DebuggerHidden]
			IEnumerator<TElement> IEnumerable<!0>.GetEnumerator()
			{
				ExceptionAggregator.<WrapQueryEnumerator>d__1<TElement, TIgnoreKey> <WrapQueryEnumerator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<WrapQueryEnumerator>d__ = this;
				}
				else
				{
					<WrapQueryEnumerator>d__ = new ExceptionAggregator.<WrapQueryEnumerator>d__1<TElement, TIgnoreKey>(0);
				}
				<WrapQueryEnumerator>d__.source = source;
				<WrapQueryEnumerator>d__.cancellationState = cancellationState;
				return <WrapQueryEnumerator>d__;
			}

			// Token: 0x06000B60 RID: 2912 RVA: 0x000247A3 File Offset: 0x000229A3
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TElement>.GetEnumerator();
			}

			// Token: 0x040007CE RID: 1998
			private int <>1__state;

			// Token: 0x040007CF RID: 1999
			private TElement <>2__current;

			// Token: 0x040007D0 RID: 2000
			private int <>l__initialThreadId;

			// Token: 0x040007D1 RID: 2001
			private QueryOperatorEnumerator<TElement, TIgnoreKey> source;

			// Token: 0x040007D2 RID: 2002
			public QueryOperatorEnumerator<TElement, TIgnoreKey> <>3__source;

			// Token: 0x040007D3 RID: 2003
			private TElement <elem>5__1;

			// Token: 0x040007D4 RID: 2004
			private TIgnoreKey <ignoreKey>5__2;

			// Token: 0x040007D5 RID: 2005
			private CancellationState cancellationState;

			// Token: 0x040007D6 RID: 2006
			public CancellationState <>3__cancellationState;
		}

		// Token: 0x020001D1 RID: 465
		[CompilerGenerated]
		private sealed class <>c__DisplayClass3_0<T, U>
		{
			// Token: 0x06000B61 RID: 2913 RVA: 0x00002310 File Offset: 0x00000510
			public <>c__DisplayClass3_0()
			{
			}

			// Token: 0x06000B62 RID: 2914 RVA: 0x000247AC File Offset: 0x000229AC
			internal U <WrapFunc>b__0(T t)
			{
				U result = default(U);
				try
				{
					result = this.f(t);
				}
				catch (Exception ex)
				{
					ExceptionAggregator.ThrowOCEorAggregateException(ex, this.cancellationState);
				}
				return result;
			}

			// Token: 0x040007D7 RID: 2007
			public Func<T, U> f;

			// Token: 0x040007D8 RID: 2008
			public CancellationState cancellationState;
		}
	}
}
