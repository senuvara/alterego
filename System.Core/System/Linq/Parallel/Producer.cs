﻿using System;

namespace System.Linq.Parallel
{
	// Token: 0x020000F2 RID: 242
	internal struct Producer<TKey>
	{
		// Token: 0x06000815 RID: 2069 RVA: 0x000184EC File Offset: 0x000166EC
		internal Producer(TKey maxKey, int producerIndex)
		{
			this.MaxKey = maxKey;
			this.ProducerIndex = producerIndex;
		}

		// Token: 0x04000567 RID: 1383
		internal readonly TKey MaxKey;

		// Token: 0x04000568 RID: 1384
		internal readonly int ProducerIndex;
	}
}
