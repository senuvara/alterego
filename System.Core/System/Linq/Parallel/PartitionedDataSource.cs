﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x020000FC RID: 252
	internal class PartitionedDataSource<T> : PartitionedStream<T, int>
	{
		// Token: 0x0600082A RID: 2090 RVA: 0x00018E4A File Offset: 0x0001704A
		internal PartitionedDataSource(IEnumerable<T> source, int partitionCount, bool useStriping) : base(partitionCount, Util.GetDefaultComparer<int>(), (source is IList<T>) ? OrdinalIndexState.Indexable : OrdinalIndexState.Correct)
		{
			this.InitializePartitions(source, partitionCount, useStriping);
		}

		// Token: 0x0600082B RID: 2091 RVA: 0x00018E70 File Offset: 0x00017070
		private void InitializePartitions(IEnumerable<T> source, int partitionCount, bool useStriping)
		{
			ParallelEnumerableWrapper<T> parallelEnumerableWrapper = source as ParallelEnumerableWrapper<T>;
			if (parallelEnumerableWrapper != null)
			{
				source = parallelEnumerableWrapper.WrappedEnumerable;
			}
			IList<T> list = source as IList<T>;
			if (list != null)
			{
				QueryOperatorEnumerator<T, int>[] array = new QueryOperatorEnumerator<T, int>[partitionCount];
				T[] array2 = source as T[];
				int num = -1;
				if (useStriping)
				{
					num = Scheduling.GetDefaultChunkSize<T>();
					if (num < 1)
					{
						num = 1;
					}
				}
				for (int i = 0; i < partitionCount; i++)
				{
					if (array2 != null)
					{
						if (useStriping)
						{
							array[i] = new PartitionedDataSource<T>.ArrayIndexRangeEnumerator(array2, partitionCount, i, num);
						}
						else
						{
							array[i] = new PartitionedDataSource<T>.ArrayContiguousIndexRangeEnumerator(array2, partitionCount, i);
						}
					}
					else if (useStriping)
					{
						array[i] = new PartitionedDataSource<T>.ListIndexRangeEnumerator(list, partitionCount, i, num);
					}
					else
					{
						array[i] = new PartitionedDataSource<T>.ListContiguousIndexRangeEnumerator(list, partitionCount, i);
					}
				}
				this._partitions = array;
				return;
			}
			this._partitions = PartitionedDataSource<T>.MakePartitions(source.GetEnumerator(), partitionCount);
		}

		// Token: 0x0600082C RID: 2092 RVA: 0x00018F30 File Offset: 0x00017130
		private static QueryOperatorEnumerator<T, int>[] MakePartitions(IEnumerator<T> source, int partitionCount)
		{
			QueryOperatorEnumerator<T, int>[] array = new QueryOperatorEnumerator<T, int>[partitionCount];
			object sourceSyncLock = new object();
			Shared<int> currentIndex = new Shared<int>(0);
			Shared<int> degreeOfParallelism = new Shared<int>(partitionCount);
			Shared<bool> exceptionTracker = new Shared<bool>(false);
			for (int i = 0; i < partitionCount; i++)
			{
				array[i] = new PartitionedDataSource<T>.ContiguousChunkLazyEnumerator(source, exceptionTracker, sourceSyncLock, currentIndex, degreeOfParallelism);
			}
			return array;
		}

		// Token: 0x020000FD RID: 253
		internal sealed class ArrayIndexRangeEnumerator : QueryOperatorEnumerator<T, int>
		{
			// Token: 0x0600082D RID: 2093 RVA: 0x00018F80 File Offset: 0x00017180
			internal ArrayIndexRangeEnumerator(T[] data, int partitionCount, int partitionIndex, int maxChunkSize)
			{
				this._data = data;
				this._elementCount = data.Length;
				this._partitionCount = partitionCount;
				this._partitionIndex = partitionIndex;
				this._maxChunkSize = maxChunkSize;
				int num = maxChunkSize * partitionCount;
				this._sectionCount = this._elementCount / num + ((this._elementCount % num == 0) ? 0 : 1);
			}

			// Token: 0x0600082E RID: 2094 RVA: 0x00018FDC File Offset: 0x000171DC
			internal override bool MoveNext(ref T currentElement, ref int currentKey)
			{
				PartitionedDataSource<T>.ArrayIndexRangeEnumerator.Mutables mutables = this._mutables;
				if (mutables == null)
				{
					mutables = (this._mutables = new PartitionedDataSource<T>.ArrayIndexRangeEnumerator.Mutables());
				}
				PartitionedDataSource<T>.ArrayIndexRangeEnumerator.Mutables mutables2 = mutables;
				int num = mutables2._currentPositionInChunk + 1;
				mutables2._currentPositionInChunk = num;
				if (num < mutables._currentChunkSize || this.MoveNextSlowPath())
				{
					currentKey = mutables._currentChunkOffset + mutables._currentPositionInChunk;
					currentElement = this._data[currentKey];
					return true;
				}
				return false;
			}

			// Token: 0x0600082F RID: 2095 RVA: 0x00019048 File Offset: 0x00017248
			private bool MoveNextSlowPath()
			{
				PartitionedDataSource<T>.ArrayIndexRangeEnumerator.Mutables mutables = this._mutables;
				PartitionedDataSource<T>.ArrayIndexRangeEnumerator.Mutables mutables2 = mutables;
				int num = mutables2._currentSection + 1;
				mutables2._currentSection = num;
				int num2 = num;
				int num3 = this._sectionCount - num2;
				if (num3 <= 0)
				{
					return false;
				}
				int num4 = num2 * this._partitionCount * this._maxChunkSize;
				mutables._currentPositionInChunk = 0;
				if (num3 > 1)
				{
					mutables._currentChunkSize = this._maxChunkSize;
					mutables._currentChunkOffset = num4 + this._partitionIndex * this._maxChunkSize;
				}
				else
				{
					int num5 = this._elementCount - num4;
					int num6 = num5 / this._partitionCount;
					int num7 = num5 % this._partitionCount;
					mutables._currentChunkSize = num6;
					if (this._partitionIndex < num7)
					{
						mutables._currentChunkSize++;
					}
					if (mutables._currentChunkSize == 0)
					{
						return false;
					}
					mutables._currentChunkOffset = num4 + this._partitionIndex * num6 + ((this._partitionIndex < num7) ? this._partitionIndex : num7);
				}
				return true;
			}

			// Token: 0x0400058D RID: 1421
			private readonly T[] _data;

			// Token: 0x0400058E RID: 1422
			private readonly int _elementCount;

			// Token: 0x0400058F RID: 1423
			private readonly int _partitionCount;

			// Token: 0x04000590 RID: 1424
			private readonly int _partitionIndex;

			// Token: 0x04000591 RID: 1425
			private readonly int _maxChunkSize;

			// Token: 0x04000592 RID: 1426
			private readonly int _sectionCount;

			// Token: 0x04000593 RID: 1427
			private PartitionedDataSource<T>.ArrayIndexRangeEnumerator.Mutables _mutables;

			// Token: 0x020000FE RID: 254
			private class Mutables
			{
				// Token: 0x06000830 RID: 2096 RVA: 0x0001912A File Offset: 0x0001732A
				internal Mutables()
				{
					this._currentSection = -1;
				}

				// Token: 0x04000594 RID: 1428
				internal int _currentSection;

				// Token: 0x04000595 RID: 1429
				internal int _currentChunkSize;

				// Token: 0x04000596 RID: 1430
				internal int _currentPositionInChunk;

				// Token: 0x04000597 RID: 1431
				internal int _currentChunkOffset;
			}
		}

		// Token: 0x020000FF RID: 255
		internal sealed class ArrayContiguousIndexRangeEnumerator : QueryOperatorEnumerator<T, int>
		{
			// Token: 0x06000831 RID: 2097 RVA: 0x0001913C File Offset: 0x0001733C
			internal ArrayContiguousIndexRangeEnumerator(T[] data, int partitionCount, int partitionIndex)
			{
				this._data = data;
				int num = data.Length / partitionCount;
				int num2 = data.Length % partitionCount;
				int num3 = partitionIndex * num + ((partitionIndex < num2) ? partitionIndex : num2);
				this._startIndex = num3 - 1;
				this._maximumIndex = num3 + num + ((partitionIndex < num2) ? 1 : 0);
			}

			// Token: 0x06000832 RID: 2098 RVA: 0x0001918C File Offset: 0x0001738C
			internal override bool MoveNext(ref T currentElement, ref int currentKey)
			{
				if (this._currentIndex == null)
				{
					this._currentIndex = new Shared<int>(this._startIndex);
				}
				Shared<int> currentIndex = this._currentIndex;
				int num = currentIndex.Value + 1;
				currentIndex.Value = num;
				int num2 = num;
				if (num2 < this._maximumIndex)
				{
					currentKey = num2;
					currentElement = this._data[num2];
					return true;
				}
				return false;
			}

			// Token: 0x04000598 RID: 1432
			private readonly T[] _data;

			// Token: 0x04000599 RID: 1433
			private readonly int _startIndex;

			// Token: 0x0400059A RID: 1434
			private readonly int _maximumIndex;

			// Token: 0x0400059B RID: 1435
			private Shared<int> _currentIndex;
		}

		// Token: 0x02000100 RID: 256
		internal sealed class ListIndexRangeEnumerator : QueryOperatorEnumerator<T, int>
		{
			// Token: 0x06000833 RID: 2099 RVA: 0x000191EC File Offset: 0x000173EC
			internal ListIndexRangeEnumerator(IList<T> data, int partitionCount, int partitionIndex, int maxChunkSize)
			{
				this._data = data;
				this._elementCount = data.Count;
				this._partitionCount = partitionCount;
				this._partitionIndex = partitionIndex;
				this._maxChunkSize = maxChunkSize;
				int num = maxChunkSize * partitionCount;
				this._sectionCount = this._elementCount / num + ((this._elementCount % num == 0) ? 0 : 1);
			}

			// Token: 0x06000834 RID: 2100 RVA: 0x0001924C File Offset: 0x0001744C
			internal override bool MoveNext(ref T currentElement, ref int currentKey)
			{
				PartitionedDataSource<T>.ListIndexRangeEnumerator.Mutables mutables = this._mutables;
				if (mutables == null)
				{
					mutables = (this._mutables = new PartitionedDataSource<T>.ListIndexRangeEnumerator.Mutables());
				}
				PartitionedDataSource<T>.ListIndexRangeEnumerator.Mutables mutables2 = mutables;
				int num = mutables2._currentPositionInChunk + 1;
				mutables2._currentPositionInChunk = num;
				if (num < mutables._currentChunkSize || this.MoveNextSlowPath())
				{
					currentKey = mutables._currentChunkOffset + mutables._currentPositionInChunk;
					currentElement = this._data[currentKey];
					return true;
				}
				return false;
			}

			// Token: 0x06000835 RID: 2101 RVA: 0x000192B8 File Offset: 0x000174B8
			private bool MoveNextSlowPath()
			{
				PartitionedDataSource<T>.ListIndexRangeEnumerator.Mutables mutables = this._mutables;
				PartitionedDataSource<T>.ListIndexRangeEnumerator.Mutables mutables2 = mutables;
				int num = mutables2._currentSection + 1;
				mutables2._currentSection = num;
				int num2 = num;
				int num3 = this._sectionCount - num2;
				if (num3 <= 0)
				{
					return false;
				}
				int num4 = num2 * this._partitionCount * this._maxChunkSize;
				mutables._currentPositionInChunk = 0;
				if (num3 > 1)
				{
					mutables._currentChunkSize = this._maxChunkSize;
					mutables._currentChunkOffset = num4 + this._partitionIndex * this._maxChunkSize;
				}
				else
				{
					int num5 = this._elementCount - num4;
					int num6 = num5 / this._partitionCount;
					int num7 = num5 % this._partitionCount;
					mutables._currentChunkSize = num6;
					if (this._partitionIndex < num7)
					{
						mutables._currentChunkSize++;
					}
					if (mutables._currentChunkSize == 0)
					{
						return false;
					}
					mutables._currentChunkOffset = num4 + this._partitionIndex * num6 + ((this._partitionIndex < num7) ? this._partitionIndex : num7);
				}
				return true;
			}

			// Token: 0x0400059C RID: 1436
			private readonly IList<T> _data;

			// Token: 0x0400059D RID: 1437
			private readonly int _elementCount;

			// Token: 0x0400059E RID: 1438
			private readonly int _partitionCount;

			// Token: 0x0400059F RID: 1439
			private readonly int _partitionIndex;

			// Token: 0x040005A0 RID: 1440
			private readonly int _maxChunkSize;

			// Token: 0x040005A1 RID: 1441
			private readonly int _sectionCount;

			// Token: 0x040005A2 RID: 1442
			private PartitionedDataSource<T>.ListIndexRangeEnumerator.Mutables _mutables;

			// Token: 0x02000101 RID: 257
			private class Mutables
			{
				// Token: 0x06000836 RID: 2102 RVA: 0x0001939A File Offset: 0x0001759A
				internal Mutables()
				{
					this._currentSection = -1;
				}

				// Token: 0x040005A3 RID: 1443
				internal int _currentSection;

				// Token: 0x040005A4 RID: 1444
				internal int _currentChunkSize;

				// Token: 0x040005A5 RID: 1445
				internal int _currentPositionInChunk;

				// Token: 0x040005A6 RID: 1446
				internal int _currentChunkOffset;
			}
		}

		// Token: 0x02000102 RID: 258
		internal sealed class ListContiguousIndexRangeEnumerator : QueryOperatorEnumerator<T, int>
		{
			// Token: 0x06000837 RID: 2103 RVA: 0x000193AC File Offset: 0x000175AC
			internal ListContiguousIndexRangeEnumerator(IList<T> data, int partitionCount, int partitionIndex)
			{
				this._data = data;
				int num = data.Count / partitionCount;
				int num2 = data.Count % partitionCount;
				int num3 = partitionIndex * num + ((partitionIndex < num2) ? partitionIndex : num2);
				this._startIndex = num3 - 1;
				this._maximumIndex = num3 + num + ((partitionIndex < num2) ? 1 : 0);
			}

			// Token: 0x06000838 RID: 2104 RVA: 0x00019400 File Offset: 0x00017600
			internal override bool MoveNext(ref T currentElement, ref int currentKey)
			{
				if (this._currentIndex == null)
				{
					this._currentIndex = new Shared<int>(this._startIndex);
				}
				Shared<int> currentIndex = this._currentIndex;
				int num = currentIndex.Value + 1;
				currentIndex.Value = num;
				int num2 = num;
				if (num2 < this._maximumIndex)
				{
					currentKey = num2;
					currentElement = this._data[num2];
					return true;
				}
				return false;
			}

			// Token: 0x040005A7 RID: 1447
			private readonly IList<T> _data;

			// Token: 0x040005A8 RID: 1448
			private readonly int _startIndex;

			// Token: 0x040005A9 RID: 1449
			private readonly int _maximumIndex;

			// Token: 0x040005AA RID: 1450
			private Shared<int> _currentIndex;
		}

		// Token: 0x02000103 RID: 259
		private class ContiguousChunkLazyEnumerator : QueryOperatorEnumerator<T, int>
		{
			// Token: 0x06000839 RID: 2105 RVA: 0x0001945E File Offset: 0x0001765E
			internal ContiguousChunkLazyEnumerator(IEnumerator<T> source, Shared<bool> exceptionTracker, object sourceSyncLock, Shared<int> currentIndex, Shared<int> degreeOfParallelism)
			{
				this._source = source;
				this._sourceSyncLock = sourceSyncLock;
				this._currentIndex = currentIndex;
				this._activeEnumeratorsCount = degreeOfParallelism;
				this._exceptionTracker = exceptionTracker;
			}

			// Token: 0x0600083A RID: 2106 RVA: 0x0001948C File Offset: 0x0001768C
			internal override bool MoveNext(ref T currentElement, ref int currentKey)
			{
				PartitionedDataSource<T>.ContiguousChunkLazyEnumerator.Mutables mutables = this._mutables;
				if (mutables == null)
				{
					mutables = (this._mutables = new PartitionedDataSource<T>.ContiguousChunkLazyEnumerator.Mutables());
				}
				T[] chunkBuffer;
				int num2;
				for (;;)
				{
					chunkBuffer = mutables._chunkBuffer;
					PartitionedDataSource<T>.ContiguousChunkLazyEnumerator.Mutables mutables2 = mutables;
					int num = mutables2._currentChunkIndex + 1;
					mutables2._currentChunkIndex = num;
					num2 = num;
					if (num2 < mutables._currentChunkSize)
					{
						break;
					}
					object sourceSyncLock = this._sourceSyncLock;
					lock (sourceSyncLock)
					{
						int num3 = 0;
						if (this._exceptionTracker.Value)
						{
							return false;
						}
						try
						{
							while (num3 < mutables._nextChunkMaxSize && this._source.MoveNext())
							{
								chunkBuffer[num3] = this._source.Current;
								num3++;
							}
						}
						catch
						{
							this._exceptionTracker.Value = true;
							throw;
						}
						mutables._currentChunkSize = num3;
						if (num3 == 0)
						{
							return false;
						}
						mutables._chunkBaseIndex = this._currentIndex.Value;
						checked
						{
							this._currentIndex.Value += num3;
						}
					}
					if (mutables._nextChunkMaxSize < chunkBuffer.Length)
					{
						PartitionedDataSource<T>.ContiguousChunkLazyEnumerator.Mutables mutables3 = mutables;
						num = mutables3._chunkCounter;
						mutables3._chunkCounter = num + 1;
						if ((num & 7) == 7)
						{
							mutables._nextChunkMaxSize *= 2;
							if (mutables._nextChunkMaxSize > chunkBuffer.Length)
							{
								mutables._nextChunkMaxSize = chunkBuffer.Length;
							}
						}
					}
					mutables._currentChunkIndex = -1;
				}
				currentElement = chunkBuffer[num2];
				currentKey = mutables._chunkBaseIndex + num2;
				return true;
			}

			// Token: 0x0600083B RID: 2107 RVA: 0x00019618 File Offset: 0x00017818
			protected override void Dispose(bool disposing)
			{
				if (Interlocked.Decrement(ref this._activeEnumeratorsCount.Value) == 0)
				{
					this._source.Dispose();
				}
			}

			// Token: 0x040005AB RID: 1451
			private const int chunksPerChunkSize = 7;

			// Token: 0x040005AC RID: 1452
			private readonly IEnumerator<T> _source;

			// Token: 0x040005AD RID: 1453
			private readonly object _sourceSyncLock;

			// Token: 0x040005AE RID: 1454
			private readonly Shared<int> _currentIndex;

			// Token: 0x040005AF RID: 1455
			private readonly Shared<int> _activeEnumeratorsCount;

			// Token: 0x040005B0 RID: 1456
			private readonly Shared<bool> _exceptionTracker;

			// Token: 0x040005B1 RID: 1457
			private PartitionedDataSource<T>.ContiguousChunkLazyEnumerator.Mutables _mutables;

			// Token: 0x02000104 RID: 260
			private class Mutables
			{
				// Token: 0x0600083C RID: 2108 RVA: 0x00019637 File Offset: 0x00017837
				internal Mutables()
				{
					this._nextChunkMaxSize = 1;
					this._chunkBuffer = new T[Scheduling.GetDefaultChunkSize<T>()];
					this._currentChunkSize = 0;
					this._currentChunkIndex = -1;
					this._chunkBaseIndex = 0;
					this._chunkCounter = 0;
				}

				// Token: 0x040005B2 RID: 1458
				internal readonly T[] _chunkBuffer;

				// Token: 0x040005B3 RID: 1459
				internal int _nextChunkMaxSize;

				// Token: 0x040005B4 RID: 1460
				internal int _currentChunkSize;

				// Token: 0x040005B5 RID: 1461
				internal int _currentChunkIndex;

				// Token: 0x040005B6 RID: 1462
				internal int _chunkBaseIndex;

				// Token: 0x040005B7 RID: 1463
				internal int _chunkCounter;
			}
		}
	}
}
