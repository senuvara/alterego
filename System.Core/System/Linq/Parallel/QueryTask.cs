﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace System.Linq.Parallel
{
	// Token: 0x020001C0 RID: 448
	internal abstract class QueryTask
	{
		// Token: 0x06000B17 RID: 2839 RVA: 0x00023A96 File Offset: 0x00021C96
		protected QueryTask(int taskIndex, QueryTaskGroupState groupState)
		{
			this._taskIndex = taskIndex;
			this._groupState = groupState;
		}

		// Token: 0x06000B18 RID: 2840 RVA: 0x00023AAC File Offset: 0x00021CAC
		private static void RunTaskSynchronously(object o)
		{
			((QueryTask)o).BaseWork(null);
		}

		// Token: 0x06000B19 RID: 2841 RVA: 0x00023ABA File Offset: 0x00021CBA
		internal Task RunSynchronously(TaskScheduler taskScheduler)
		{
			Task task = new Task(QueryTask.s_runTaskSynchronouslyDelegate, this, TaskCreationOptions.AttachedToParent);
			task.RunSynchronously(taskScheduler);
			return task;
		}

		// Token: 0x06000B1A RID: 2842 RVA: 0x00023AD0 File Offset: 0x00021CD0
		internal Task RunAsynchronously(TaskScheduler taskScheduler)
		{
			return Task.Factory.StartNew(QueryTask.s_baseWorkDelegate, this, default(CancellationToken), TaskCreationOptions.PreferFairness | TaskCreationOptions.AttachedToParent, taskScheduler);
		}

		// Token: 0x06000B1B RID: 2843 RVA: 0x00023AF8 File Offset: 0x00021CF8
		private void BaseWork(object unused)
		{
			PlinqEtwProvider.Log.ParallelQueryFork(this._groupState.QueryId);
			try
			{
				this.Work();
			}
			finally
			{
				PlinqEtwProvider.Log.ParallelQueryJoin(this._groupState.QueryId);
			}
		}

		// Token: 0x06000B1C RID: 2844
		protected abstract void Work();

		// Token: 0x06000B1D RID: 2845 RVA: 0x00023B48 File Offset: 0x00021D48
		// Note: this type is marked as 'beforefieldinit'.
		static QueryTask()
		{
		}

		// Token: 0x0400079E RID: 1950
		protected int _taskIndex;

		// Token: 0x0400079F RID: 1951
		protected QueryTaskGroupState _groupState;

		// Token: 0x040007A0 RID: 1952
		private static Action<object> s_runTaskSynchronouslyDelegate = new Action<object>(QueryTask.RunTaskSynchronously);

		// Token: 0x040007A1 RID: 1953
		private static Action<object> s_baseWorkDelegate = delegate(object o)
		{
			((QueryTask)o).BaseWork(null);
		};

		// Token: 0x020001C1 RID: 449
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000B1E RID: 2846 RVA: 0x00023B70 File Offset: 0x00021D70
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000B1F RID: 2847 RVA: 0x00002310 File Offset: 0x00000510
			public <>c()
			{
			}

			// Token: 0x06000B20 RID: 2848 RVA: 0x00023B7C File Offset: 0x00021D7C
			internal void <.cctor>b__10_0(object o)
			{
				((QueryTask)o).BaseWork(null);
			}

			// Token: 0x040007A2 RID: 1954
			public static readonly QueryTask.<>c <>9 = new QueryTask.<>c();
		}
	}
}
