﻿using System;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x020001E0 RID: 480
	internal sealed class PairComparer<T, U> : IComparer<Pair<T, U>>
	{
		// Token: 0x06000BB2 RID: 2994 RVA: 0x000254EA File Offset: 0x000236EA
		public PairComparer(IComparer<T> comparer1, IComparer<U> comparer2)
		{
			this._comparer1 = comparer1;
			this._comparer2 = comparer2;
		}

		// Token: 0x06000BB3 RID: 2995 RVA: 0x00025500 File Offset: 0x00023700
		public int Compare(Pair<T, U> x, Pair<T, U> y)
		{
			int num = this._comparer1.Compare(x.First, y.First);
			if (num != 0)
			{
				return num;
			}
			return this._comparer2.Compare(x.Second, y.Second);
		}

		// Token: 0x04000806 RID: 2054
		private readonly IComparer<T> _comparer1;

		// Token: 0x04000807 RID: 2055
		private readonly IComparer<U> _comparer2;
	}
}
