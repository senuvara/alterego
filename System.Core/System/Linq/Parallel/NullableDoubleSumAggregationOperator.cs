﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000150 RID: 336
	internal sealed class NullableDoubleSumAggregationOperator : InlinedAggregationOperator<double?, double?, double?>
	{
		// Token: 0x06000948 RID: 2376 RVA: 0x0001D9EA File Offset: 0x0001BBEA
		internal NullableDoubleSumAggregationOperator(IEnumerable<double?> child) : base(child)
		{
		}

		// Token: 0x06000949 RID: 2377 RVA: 0x0001D9F4 File Offset: 0x0001BBF4
		protected override double? InternalAggregate(ref Exception singularExceptionToThrow)
		{
			double? result;
			using (IEnumerator<double?> enumerator = this.GetEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered), true))
			{
				double num = 0.0;
				while (enumerator.MoveNext())
				{
					double num2 = num;
					result = enumerator.Current;
					num = num2 + result.GetValueOrDefault();
				}
				result = new double?(num);
			}
			return result;
		}

		// Token: 0x0600094A RID: 2378 RVA: 0x0001DA58 File Offset: 0x0001BC58
		protected override QueryOperatorEnumerator<double?, int> CreateEnumerator<TKey>(int index, int count, QueryOperatorEnumerator<double?, TKey> source, object sharedData, CancellationToken cancellationToken)
		{
			return new NullableDoubleSumAggregationOperator.NullableDoubleSumAggregationOperatorEnumerator<TKey>(source, index, cancellationToken);
		}

		// Token: 0x02000151 RID: 337
		private class NullableDoubleSumAggregationOperatorEnumerator<TKey> : InlinedAggregationOperatorEnumerator<double?>
		{
			// Token: 0x0600094B RID: 2379 RVA: 0x0001DA63 File Offset: 0x0001BC63
			internal NullableDoubleSumAggregationOperatorEnumerator(QueryOperatorEnumerator<double?, TKey> source, int partitionIndex, CancellationToken cancellationToken) : base(partitionIndex, cancellationToken)
			{
				this._source = source;
			}

			// Token: 0x0600094C RID: 2380 RVA: 0x0001DA74 File Offset: 0x0001BC74
			protected override bool MoveNextCore(ref double? currentElement)
			{
				double? num = null;
				TKey tkey = default(TKey);
				QueryOperatorEnumerator<double?, TKey> source = this._source;
				if (source.MoveNext(ref num, ref tkey))
				{
					double num2 = 0.0;
					int num3 = 0;
					do
					{
						if ((num3++ & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(this._cancellationToken);
						}
						num2 += num.GetValueOrDefault();
					}
					while (source.MoveNext(ref num, ref tkey));
					currentElement = new double?(num2);
					return true;
				}
				return false;
			}

			// Token: 0x0600094D RID: 2381 RVA: 0x0001DAED File Offset: 0x0001BCED
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x0400065D RID: 1629
			private readonly QueryOperatorEnumerator<double?, TKey> _source;
		}
	}
}
