﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000175 RID: 373
	internal sealed class ScanQueryOperator<TElement> : QueryOperator<TElement>
	{
		// Token: 0x060009FD RID: 2557 RVA: 0x0001F924 File Offset: 0x0001DB24
		internal ScanQueryOperator(IEnumerable<TElement> data) : base(false, QuerySettings.Empty)
		{
			ParallelEnumerableWrapper<TElement> parallelEnumerableWrapper = data as ParallelEnumerableWrapper<TElement>;
			if (parallelEnumerableWrapper != null)
			{
				data = parallelEnumerableWrapper.WrappedEnumerable;
			}
			this._data = data;
		}

		// Token: 0x1700015F RID: 351
		// (get) Token: 0x060009FE RID: 2558 RVA: 0x0001F956 File Offset: 0x0001DB56
		public IEnumerable<TElement> Data
		{
			get
			{
				return this._data;
			}
		}

		// Token: 0x060009FF RID: 2559 RVA: 0x0001F960 File Offset: 0x0001DB60
		internal override QueryResults<TElement> Open(QuerySettings settings, bool preferStriping)
		{
			IList<TElement> list = this._data as IList<!0>;
			if (list != null)
			{
				return new ListQueryResults<TElement>(list, settings.DegreeOfParallelism.GetValueOrDefault(), preferStriping);
			}
			return new ScanQueryOperator<TElement>.ScanEnumerableQueryOperatorResults(this._data, settings);
		}

		// Token: 0x06000A00 RID: 2560 RVA: 0x0001F99F File Offset: 0x0001DB9F
		internal override IEnumerator<TElement> GetEnumerator(ParallelMergeOptions? mergeOptions, bool suppressOrderPreservation)
		{
			return this._data.GetEnumerator();
		}

		// Token: 0x06000A01 RID: 2561 RVA: 0x0001F956 File Offset: 0x0001DB56
		internal override IEnumerable<TElement> AsSequentialQuery(CancellationToken token)
		{
			return this._data;
		}

		// Token: 0x17000160 RID: 352
		// (get) Token: 0x06000A02 RID: 2562 RVA: 0x0001F9AC File Offset: 0x0001DBAC
		internal override OrdinalIndexState OrdinalIndexState
		{
			get
			{
				if (!(this._data is IList<!0>))
				{
					return OrdinalIndexState.Correct;
				}
				return OrdinalIndexState.Indexable;
			}
		}

		// Token: 0x17000161 RID: 353
		// (get) Token: 0x06000A03 RID: 2563 RVA: 0x00002285 File Offset: 0x00000485
		internal override bool LimitsParallelism
		{
			get
			{
				return false;
			}
		}

		// Token: 0x040006A0 RID: 1696
		private readonly IEnumerable<TElement> _data;

		// Token: 0x02000176 RID: 374
		private class ScanEnumerableQueryOperatorResults : QueryResults<TElement>
		{
			// Token: 0x06000A04 RID: 2564 RVA: 0x0001F9BE File Offset: 0x0001DBBE
			internal ScanEnumerableQueryOperatorResults(IEnumerable<TElement> data, QuerySettings settings)
			{
				this._data = data;
				this._settings = settings;
			}

			// Token: 0x06000A05 RID: 2565 RVA: 0x0001F9D4 File Offset: 0x0001DBD4
			internal override void GivePartitionedStream(IPartitionedStreamRecipient<TElement> recipient)
			{
				PartitionedStream<TElement, int> partitionedStream = ExchangeUtilities.PartitionDataSource<TElement>(this._data, this._settings.DegreeOfParallelism.Value, false);
				recipient.Receive<int>(partitionedStream);
			}

			// Token: 0x040006A1 RID: 1697
			private IEnumerable<TElement> _data;

			// Token: 0x040006A2 RID: 1698
			private QuerySettings _settings;
		}
	}
}
