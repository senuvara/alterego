﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000122 RID: 290
	internal sealed class CountAggregationOperator<TSource> : InlinedAggregationOperator<TSource, int, int>
	{
		// Token: 0x060008B9 RID: 2233 RVA: 0x0001BA69 File Offset: 0x00019C69
		internal CountAggregationOperator(IEnumerable<TSource> child) : base(child)
		{
		}

		// Token: 0x060008BA RID: 2234 RVA: 0x0001BA74 File Offset: 0x00019C74
		protected override int InternalAggregate(ref Exception singularExceptionToThrow)
		{
			checked
			{
				int result;
				using (IEnumerator<int> enumerator = this.GetEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered), true))
				{
					int num = 0;
					while (enumerator.MoveNext())
					{
						int num2 = enumerator.Current;
						num += num2;
					}
					result = num;
				}
				return result;
			}
		}

		// Token: 0x060008BB RID: 2235 RVA: 0x0001BAC4 File Offset: 0x00019CC4
		protected override QueryOperatorEnumerator<int, int> CreateEnumerator<TKey>(int index, int count, QueryOperatorEnumerator<TSource, TKey> source, object sharedData, CancellationToken cancellationToken)
		{
			return new CountAggregationOperator<TSource>.CountAggregationOperatorEnumerator<TKey>(source, index, cancellationToken);
		}

		// Token: 0x02000123 RID: 291
		private class CountAggregationOperatorEnumerator<TKey> : InlinedAggregationOperatorEnumerator<int>
		{
			// Token: 0x060008BC RID: 2236 RVA: 0x0001BACF File Offset: 0x00019CCF
			internal CountAggregationOperatorEnumerator(QueryOperatorEnumerator<TSource, TKey> source, int partitionIndex, CancellationToken cancellationToken) : base(partitionIndex, cancellationToken)
			{
				this._source = source;
			}

			// Token: 0x060008BD RID: 2237 RVA: 0x0001BAE0 File Offset: 0x00019CE0
			protected override bool MoveNextCore(ref int currentElement)
			{
				TSource tsource = default(TSource);
				TKey tkey = default(TKey);
				QueryOperatorEnumerator<TSource, TKey> source = this._source;
				if (source.MoveNext(ref tsource, ref tkey))
				{
					int num = 0;
					int num2 = 0;
					do
					{
						if ((num2++ & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(this._cancellationToken);
						}
						checked
						{
							num++;
						}
					}
					while (source.MoveNext(ref tsource, ref tkey));
					currentElement = num;
					return true;
				}
				return false;
			}

			// Token: 0x060008BE RID: 2238 RVA: 0x0001BB42 File Offset: 0x00019D42
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x04000636 RID: 1590
			private readonly QueryOperatorEnumerator<TSource, TKey> _source;
		}
	}
}
