﻿using System;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x020000D6 RID: 214
	internal sealed class AsynchronousChannel<T> : IDisposable
	{
		// Token: 0x0600079D RID: 1949 RVA: 0x00016C6F File Offset: 0x00014E6F
		internal AsynchronousChannel(int index, int chunkSize, CancellationToken cancellationToken, IntValueEvent consumerEvent) : this(index, 512, chunkSize, cancellationToken, consumerEvent)
		{
		}

		// Token: 0x0600079E RID: 1950 RVA: 0x00016C84 File Offset: 0x00014E84
		internal AsynchronousChannel(int index, int capacity, int chunkSize, CancellationToken cancellationToken, IntValueEvent consumerEvent)
		{
			if (chunkSize == 0)
			{
				chunkSize = Scheduling.GetDefaultChunkSize<T>();
			}
			this._index = index;
			this._buffer = new T[capacity + 1][];
			this._producerBufferIndex = 0;
			this._consumerBufferIndex = 0;
			this._producerEvent = new ManualResetEventSlim();
			this._consumerEvent = consumerEvent;
			this._chunkSize = chunkSize;
			this._producerChunk = new T[chunkSize];
			this._producerChunkIndex = 0;
			this._cancellationToken = cancellationToken;
		}

		// Token: 0x17000118 RID: 280
		// (get) Token: 0x0600079F RID: 1951 RVA: 0x00016D00 File Offset: 0x00014F00
		internal bool IsFull
		{
			get
			{
				int producerBufferIndex = this._producerBufferIndex;
				int consumerBufferIndex = this._consumerBufferIndex;
				return producerBufferIndex == consumerBufferIndex - 1 || (consumerBufferIndex == 0 && producerBufferIndex == this._buffer.Length - 1);
			}
		}

		// Token: 0x17000119 RID: 281
		// (get) Token: 0x060007A0 RID: 1952 RVA: 0x00016D39 File Offset: 0x00014F39
		internal bool IsChunkBufferEmpty
		{
			get
			{
				return this._producerBufferIndex == this._consumerBufferIndex;
			}
		}

		// Token: 0x1700011A RID: 282
		// (get) Token: 0x060007A1 RID: 1953 RVA: 0x00016D4D File Offset: 0x00014F4D
		internal bool IsDone
		{
			get
			{
				return this._done;
			}
		}

		// Token: 0x060007A2 RID: 1954 RVA: 0x00016D57 File Offset: 0x00014F57
		internal void FlushBuffers()
		{
			this.FlushCachedChunk();
		}

		// Token: 0x060007A3 RID: 1955 RVA: 0x00016D60 File Offset: 0x00014F60
		internal void SetDone()
		{
			this._done = true;
			lock (this)
			{
				if (this._consumerEvent != null)
				{
					this._consumerEvent.Set(this._index);
				}
			}
		}

		// Token: 0x060007A4 RID: 1956 RVA: 0x00016DB8 File Offset: 0x00014FB8
		internal void Enqueue(T item)
		{
			int producerChunkIndex = this._producerChunkIndex;
			this._producerChunk[producerChunkIndex] = item;
			if (producerChunkIndex == this._chunkSize - 1)
			{
				this.EnqueueChunk(this._producerChunk);
				this._producerChunk = new T[this._chunkSize];
			}
			this._producerChunkIndex = (producerChunkIndex + 1) % this._chunkSize;
		}

		// Token: 0x060007A5 RID: 1957 RVA: 0x00016E14 File Offset: 0x00015014
		private void EnqueueChunk(T[] chunk)
		{
			if (this.IsFull)
			{
				this.WaitUntilNonFull();
			}
			int producerBufferIndex = this._producerBufferIndex;
			this._buffer[producerBufferIndex] = chunk;
			Interlocked.Exchange(ref this._producerBufferIndex, (producerBufferIndex + 1) % this._buffer.Length);
			if (this._consumerIsWaiting == 1 && !this.IsChunkBufferEmpty)
			{
				this._consumerIsWaiting = 0;
				this._consumerEvent.Set(this._index);
			}
		}

		// Token: 0x060007A6 RID: 1958 RVA: 0x00016E88 File Offset: 0x00015088
		private void WaitUntilNonFull()
		{
			do
			{
				this._producerEvent.Reset();
				Interlocked.Exchange(ref this._producerIsWaiting, 1);
				if (this.IsFull)
				{
					this._producerEvent.Wait(this._cancellationToken);
				}
				else
				{
					this._producerIsWaiting = 0;
				}
			}
			while (this.IsFull);
		}

		// Token: 0x060007A7 RID: 1959 RVA: 0x00016EDC File Offset: 0x000150DC
		private void FlushCachedChunk()
		{
			if (this._producerChunk != null && this._producerChunkIndex != 0)
			{
				T[] array = new T[this._producerChunkIndex];
				Array.Copy(this._producerChunk, 0, array, 0, this._producerChunkIndex);
				this.EnqueueChunk(array);
				this._producerChunk = null;
			}
		}

		// Token: 0x060007A8 RID: 1960 RVA: 0x00016F28 File Offset: 0x00015128
		internal bool TryDequeue(ref T item)
		{
			if (this._consumerChunk == null)
			{
				if (!this.TryDequeueChunk(ref this._consumerChunk))
				{
					return false;
				}
				this._consumerChunkIndex = 0;
			}
			item = this._consumerChunk[this._consumerChunkIndex];
			this._consumerChunkIndex++;
			if (this._consumerChunkIndex == this._consumerChunk.Length)
			{
				this._consumerChunk = null;
			}
			return true;
		}

		// Token: 0x060007A9 RID: 1961 RVA: 0x00016F91 File Offset: 0x00015191
		private bool TryDequeueChunk(ref T[] chunk)
		{
			if (this.IsChunkBufferEmpty)
			{
				return false;
			}
			chunk = this.InternalDequeueChunk();
			return true;
		}

		// Token: 0x060007AA RID: 1962 RVA: 0x00016FA8 File Offset: 0x000151A8
		internal bool TryDequeue(ref T item, ref bool isDone)
		{
			isDone = false;
			if (this._consumerChunk == null)
			{
				if (!this.TryDequeueChunk(ref this._consumerChunk, ref isDone))
				{
					return false;
				}
				this._consumerChunkIndex = 0;
			}
			item = this._consumerChunk[this._consumerChunkIndex];
			this._consumerChunkIndex++;
			if (this._consumerChunkIndex == this._consumerChunk.Length)
			{
				this._consumerChunk = null;
			}
			return true;
		}

		// Token: 0x060007AB RID: 1963 RVA: 0x00017018 File Offset: 0x00015218
		private bool TryDequeueChunk(ref T[] chunk, ref bool isDone)
		{
			isDone = false;
			while (this.IsChunkBufferEmpty)
			{
				if (this.IsDone && this.IsChunkBufferEmpty)
				{
					isDone = true;
					return false;
				}
				Interlocked.Exchange(ref this._consumerIsWaiting, 1);
				if (this.IsChunkBufferEmpty && !this.IsDone)
				{
					return false;
				}
				this._consumerIsWaiting = 0;
			}
			chunk = this.InternalDequeueChunk();
			return true;
		}

		// Token: 0x060007AC RID: 1964 RVA: 0x00017078 File Offset: 0x00015278
		private T[] InternalDequeueChunk()
		{
			int consumerBufferIndex = this._consumerBufferIndex;
			T[] result = this._buffer[consumerBufferIndex];
			this._buffer[consumerBufferIndex] = null;
			Interlocked.Exchange(ref this._consumerBufferIndex, (consumerBufferIndex + 1) % this._buffer.Length);
			if (this._producerIsWaiting == 1 && !this.IsFull)
			{
				this._producerIsWaiting = 0;
				this._producerEvent.Set();
			}
			return result;
		}

		// Token: 0x060007AD RID: 1965 RVA: 0x000170DE File Offset: 0x000152DE
		internal void DoneWithDequeueWait()
		{
			this._consumerIsWaiting = 0;
		}

		// Token: 0x060007AE RID: 1966 RVA: 0x000170EC File Offset: 0x000152EC
		public void Dispose()
		{
			lock (this)
			{
				this._producerEvent.Dispose();
				this._producerEvent = null;
				this._consumerEvent = null;
			}
		}

		// Token: 0x0400050F RID: 1295
		private T[][] _buffer;

		// Token: 0x04000510 RID: 1296
		private readonly int _index;

		// Token: 0x04000511 RID: 1297
		private volatile int _producerBufferIndex;

		// Token: 0x04000512 RID: 1298
		private volatile int _consumerBufferIndex;

		// Token: 0x04000513 RID: 1299
		private volatile bool _done;

		// Token: 0x04000514 RID: 1300
		private T[] _producerChunk;

		// Token: 0x04000515 RID: 1301
		private int _producerChunkIndex;

		// Token: 0x04000516 RID: 1302
		private T[] _consumerChunk;

		// Token: 0x04000517 RID: 1303
		private int _consumerChunkIndex;

		// Token: 0x04000518 RID: 1304
		private int _chunkSize;

		// Token: 0x04000519 RID: 1305
		private ManualResetEventSlim _producerEvent;

		// Token: 0x0400051A RID: 1306
		private IntValueEvent _consumerEvent;

		// Token: 0x0400051B RID: 1307
		private volatile int _producerIsWaiting;

		// Token: 0x0400051C RID: 1308
		private volatile int _consumerIsWaiting;

		// Token: 0x0400051D RID: 1309
		private CancellationToken _cancellationToken;
	}
}
