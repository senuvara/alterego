﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x0200014E RID: 334
	internal sealed class NullableDoubleMinMaxAggregationOperator : InlinedAggregationOperator<double?, double?, double?>
	{
		// Token: 0x06000942 RID: 2370 RVA: 0x0001D73C File Offset: 0x0001B93C
		internal NullableDoubleMinMaxAggregationOperator(IEnumerable<double?> child, int sign) : base(child)
		{
			this._sign = sign;
		}

		// Token: 0x06000943 RID: 2371 RVA: 0x0001D74C File Offset: 0x0001B94C
		protected override double? InternalAggregate(ref Exception singularExceptionToThrow)
		{
			double? num;
			using (IEnumerator<double?> enumerator = this.GetEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered), true))
			{
				if (!enumerator.MoveNext())
				{
					num = null;
					num = num;
				}
				else
				{
					double? num2 = enumerator.Current;
					if (this._sign == -1)
					{
						while (enumerator.MoveNext())
						{
							double? num3 = enumerator.Current;
							if (num3 != null && (num2 == null || num3 < num2 || double.IsNaN(num3.GetValueOrDefault())))
							{
								num2 = num3;
							}
						}
					}
					else
					{
						while (enumerator.MoveNext())
						{
							double? num4 = enumerator.Current;
							if (num4 != null && (num2 == null || num4 > num2 || double.IsNaN(num2.GetValueOrDefault())))
							{
								num2 = num4;
							}
						}
					}
					num = num2;
				}
			}
			return num;
		}

		// Token: 0x06000944 RID: 2372 RVA: 0x0001D870 File Offset: 0x0001BA70
		protected override QueryOperatorEnumerator<double?, int> CreateEnumerator<TKey>(int index, int count, QueryOperatorEnumerator<double?, TKey> source, object sharedData, CancellationToken cancellationToken)
		{
			return new NullableDoubleMinMaxAggregationOperator.NullableDoubleMinMaxAggregationOperatorEnumerator<TKey>(source, index, this._sign, cancellationToken);
		}

		// Token: 0x0400065A RID: 1626
		private readonly int _sign;

		// Token: 0x0200014F RID: 335
		private class NullableDoubleMinMaxAggregationOperatorEnumerator<TKey> : InlinedAggregationOperatorEnumerator<double?>
		{
			// Token: 0x06000945 RID: 2373 RVA: 0x0001D881 File Offset: 0x0001BA81
			internal NullableDoubleMinMaxAggregationOperatorEnumerator(QueryOperatorEnumerator<double?, TKey> source, int partitionIndex, int sign, CancellationToken cancellationToken) : base(partitionIndex, cancellationToken)
			{
				this._source = source;
				this._sign = sign;
			}

			// Token: 0x06000946 RID: 2374 RVA: 0x0001D89C File Offset: 0x0001BA9C
			protected override bool MoveNextCore(ref double? currentElement)
			{
				QueryOperatorEnumerator<double?, TKey> source = this._source;
				TKey tkey = default(TKey);
				if (source.MoveNext(ref currentElement, ref tkey))
				{
					int num = 0;
					if (this._sign == -1)
					{
						double? num2 = null;
						while (source.MoveNext(ref num2, ref tkey))
						{
							if ((num++ & 63) == 0)
							{
								CancellationState.ThrowIfCanceled(this._cancellationToken);
							}
							if (num2 != null && (currentElement == null || num2 < currentElement || double.IsNaN(num2.GetValueOrDefault())))
							{
								currentElement = num2;
							}
						}
					}
					else
					{
						double? num3 = null;
						while (source.MoveNext(ref num3, ref tkey))
						{
							if ((num++ & 63) == 0)
							{
								CancellationState.ThrowIfCanceled(this._cancellationToken);
							}
							if (num3 != null && (currentElement == null || num3 > currentElement || double.IsNaN(currentElement.GetValueOrDefault())))
							{
								currentElement = num3;
							}
						}
					}
					return true;
				}
				return false;
			}

			// Token: 0x06000947 RID: 2375 RVA: 0x0001D9DD File Offset: 0x0001BBDD
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x0400065B RID: 1627
			private QueryOperatorEnumerator<double?, TKey> _source;

			// Token: 0x0400065C RID: 1628
			private int _sign;
		}
	}
}
