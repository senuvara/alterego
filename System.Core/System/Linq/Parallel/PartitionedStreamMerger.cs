﻿using System;
using System.Threading.Tasks;

namespace System.Linq.Parallel
{
	// Token: 0x02000168 RID: 360
	internal class PartitionedStreamMerger<TOutput> : IPartitionedStreamRecipient<TOutput>
	{
		// Token: 0x17000144 RID: 324
		// (get) Token: 0x06000995 RID: 2453 RVA: 0x0001EAD4 File Offset: 0x0001CCD4
		internal MergeExecutor<TOutput> MergeExecutor
		{
			get
			{
				return this._mergeExecutor;
			}
		}

		// Token: 0x06000996 RID: 2454 RVA: 0x0001EADC File Offset: 0x0001CCDC
		internal PartitionedStreamMerger(bool forEffectMerge, ParallelMergeOptions mergeOptions, TaskScheduler taskScheduler, bool outputOrdered, CancellationState cancellationState, int queryId)
		{
			this._forEffectMerge = forEffectMerge;
			this._mergeOptions = mergeOptions;
			this._isOrdered = outputOrdered;
			this._taskScheduler = taskScheduler;
			this._cancellationState = cancellationState;
			this._queryId = queryId;
		}

		// Token: 0x06000997 RID: 2455 RVA: 0x0001EB11 File Offset: 0x0001CD11
		public void Receive<TKey>(PartitionedStream<TOutput, TKey> partitionedStream)
		{
			this._mergeExecutor = MergeExecutor<TOutput>.Execute<TKey>(partitionedStream, this._forEffectMerge, this._mergeOptions, this._taskScheduler, this._isOrdered, this._cancellationState, this._queryId);
		}

		// Token: 0x04000679 RID: 1657
		private bool _forEffectMerge;

		// Token: 0x0400067A RID: 1658
		private ParallelMergeOptions _mergeOptions;

		// Token: 0x0400067B RID: 1659
		private bool _isOrdered;

		// Token: 0x0400067C RID: 1660
		private MergeExecutor<TOutput> _mergeExecutor;

		// Token: 0x0400067D RID: 1661
		private TaskScheduler _taskScheduler;

		// Token: 0x0400067E RID: 1662
		private int _queryId;

		// Token: 0x0400067F RID: 1663
		private CancellationState _cancellationState;
	}
}
