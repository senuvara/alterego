﻿using System;

namespace System.Linq.Parallel
{
	// Token: 0x020001E2 RID: 482
	internal class Shared<T>
	{
		// Token: 0x06000BB6 RID: 2998 RVA: 0x00025563 File Offset: 0x00023763
		internal Shared(T value)
		{
			this.Value = value;
		}

		// Token: 0x04000809 RID: 2057
		internal T Value;
	}
}
