﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000154 RID: 340
	internal sealed class NullableFloatMinMaxAggregationOperator : InlinedAggregationOperator<float?, float?, float?>
	{
		// Token: 0x06000954 RID: 2388 RVA: 0x0001DC5D File Offset: 0x0001BE5D
		internal NullableFloatMinMaxAggregationOperator(IEnumerable<float?> child, int sign) : base(child)
		{
			this._sign = sign;
		}

		// Token: 0x06000955 RID: 2389 RVA: 0x0001DC70 File Offset: 0x0001BE70
		protected override float? InternalAggregate(ref Exception singularExceptionToThrow)
		{
			float? num;
			using (IEnumerator<float?> enumerator = this.GetEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered), true))
			{
				if (!enumerator.MoveNext())
				{
					num = null;
					num = num;
				}
				else
				{
					float? num2 = enumerator.Current;
					if (this._sign == -1)
					{
						while (enumerator.MoveNext())
						{
							float? num3 = enumerator.Current;
							if (num3 != null && (num2 == null || num3 < num2 || float.IsNaN(num3.GetValueOrDefault())))
							{
								num2 = num3;
							}
						}
					}
					else
					{
						while (enumerator.MoveNext())
						{
							float? num4 = enumerator.Current;
							if (num4 != null && (num2 == null || num4 > num2 || float.IsNaN(num2.GetValueOrDefault())))
							{
								num2 = num4;
							}
						}
					}
					num = num2;
				}
			}
			return num;
		}

		// Token: 0x06000956 RID: 2390 RVA: 0x0001DD94 File Offset: 0x0001BF94
		protected override QueryOperatorEnumerator<float?, int> CreateEnumerator<TKey>(int index, int count, QueryOperatorEnumerator<float?, TKey> source, object sharedData, CancellationToken cancellationToken)
		{
			return new NullableFloatMinMaxAggregationOperator.NullableFloatMinMaxAggregationOperatorEnumerator<TKey>(source, index, this._sign, cancellationToken);
		}

		// Token: 0x0400065F RID: 1631
		private readonly int _sign;

		// Token: 0x02000155 RID: 341
		private class NullableFloatMinMaxAggregationOperatorEnumerator<TKey> : InlinedAggregationOperatorEnumerator<float?>
		{
			// Token: 0x06000957 RID: 2391 RVA: 0x0001DDA5 File Offset: 0x0001BFA5
			internal NullableFloatMinMaxAggregationOperatorEnumerator(QueryOperatorEnumerator<float?, TKey> source, int partitionIndex, int sign, CancellationToken cancellationToken) : base(partitionIndex, cancellationToken)
			{
				this._source = source;
				this._sign = sign;
			}

			// Token: 0x06000958 RID: 2392 RVA: 0x0001DDC0 File Offset: 0x0001BFC0
			protected override bool MoveNextCore(ref float? currentElement)
			{
				QueryOperatorEnumerator<float?, TKey> source = this._source;
				TKey tkey = default(TKey);
				if (source.MoveNext(ref currentElement, ref tkey))
				{
					int num = 0;
					if (this._sign == -1)
					{
						float? num2 = null;
						while (source.MoveNext(ref num2, ref tkey))
						{
							if ((num++ & 63) == 0)
							{
								CancellationState.ThrowIfCanceled(this._cancellationToken);
							}
							if (num2 != null && (currentElement == null || num2 < currentElement || float.IsNaN(num2.GetValueOrDefault())))
							{
								currentElement = num2;
							}
						}
					}
					else
					{
						float? num3 = null;
						while (source.MoveNext(ref num3, ref tkey))
						{
							if ((num++ & 63) == 0)
							{
								CancellationState.ThrowIfCanceled(this._cancellationToken);
							}
							if (num3 != null && (currentElement == null || num3 > currentElement || float.IsNaN(currentElement.GetValueOrDefault())))
							{
								currentElement = num3;
							}
						}
					}
					return true;
				}
				return false;
			}

			// Token: 0x06000959 RID: 2393 RVA: 0x0001DF01 File Offset: 0x0001C101
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x04000660 RID: 1632
			private QueryOperatorEnumerator<float?, TKey> _source;

			// Token: 0x04000661 RID: 1633
			private int _sign;
		}
	}
}
