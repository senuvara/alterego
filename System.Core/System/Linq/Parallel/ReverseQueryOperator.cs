﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x0200019D RID: 413
	internal sealed class ReverseQueryOperator<TSource> : UnaryQueryOperator<TSource, TSource>
	{
		// Token: 0x06000A95 RID: 2709 RVA: 0x0002193C File Offset: 0x0001FB3C
		internal ReverseQueryOperator(IEnumerable<TSource> child) : base(child)
		{
			if (base.Child.OrdinalIndexState == OrdinalIndexState.Indexable)
			{
				base.SetOrdinalIndexState(OrdinalIndexState.Indexable);
				return;
			}
			base.SetOrdinalIndexState(OrdinalIndexState.Shuffled);
		}

		// Token: 0x06000A96 RID: 2710 RVA: 0x00021964 File Offset: 0x0001FB64
		internal override void WrapPartitionedStream<TKey>(PartitionedStream<TSource, TKey> inputStream, IPartitionedStreamRecipient<TSource> recipient, bool preferStriping, QuerySettings settings)
		{
			int partitionCount = inputStream.PartitionCount;
			PartitionedStream<TSource, TKey> partitionedStream = new PartitionedStream<TSource, TKey>(partitionCount, new ReverseComparer<TKey>(inputStream.KeyComparer), OrdinalIndexState.Shuffled);
			for (int i = 0; i < partitionCount; i++)
			{
				partitionedStream[i] = new ReverseQueryOperator<TSource>.ReverseQueryOperatorEnumerator<TKey>(inputStream[i], settings.CancellationState.MergedCancellationToken);
			}
			recipient.Receive<TKey>(partitionedStream);
		}

		// Token: 0x06000A97 RID: 2711 RVA: 0x000219BD File Offset: 0x0001FBBD
		internal override QueryResults<TSource> Open(QuerySettings settings, bool preferStriping)
		{
			return ReverseQueryOperator<TSource>.ReverseQueryOperatorResults.NewResults(base.Child.Open(settings, false), this, settings, preferStriping);
		}

		// Token: 0x06000A98 RID: 2712 RVA: 0x000219D4 File Offset: 0x0001FBD4
		internal override IEnumerable<TSource> AsSequentialQuery(CancellationToken token)
		{
			return CancellableEnumerable.Wrap<TSource>(base.Child.AsSequentialQuery(token), token).Reverse<TSource>();
		}

		// Token: 0x17000173 RID: 371
		// (get) Token: 0x06000A99 RID: 2713 RVA: 0x00002285 File Offset: 0x00000485
		internal override bool LimitsParallelism
		{
			get
			{
				return false;
			}
		}

		// Token: 0x0200019E RID: 414
		private class ReverseQueryOperatorEnumerator<TKey> : QueryOperatorEnumerator<TSource, TKey>
		{
			// Token: 0x06000A9A RID: 2714 RVA: 0x000219ED File Offset: 0x0001FBED
			internal ReverseQueryOperatorEnumerator(QueryOperatorEnumerator<TSource, TKey> source, CancellationToken cancellationToken)
			{
				this._source = source;
				this._cancellationToken = cancellationToken;
			}

			// Token: 0x06000A9B RID: 2715 RVA: 0x00021A04 File Offset: 0x0001FC04
			internal override bool MoveNext(ref TSource currentElement, ref TKey currentKey)
			{
				if (this._buffer == null)
				{
					this._bufferIndex = new Shared<int>(0);
					this._buffer = new List<Pair<TSource, TKey>>();
					TSource first = default(TSource);
					TKey second = default(TKey);
					int num = 0;
					while (this._source.MoveNext(ref first, ref second))
					{
						if ((num++ & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(this._cancellationToken);
						}
						this._buffer.Add(new Pair<TSource, TKey>(first, second));
						this._bufferIndex.Value++;
					}
				}
				Shared<int> bufferIndex = this._bufferIndex;
				int num2 = bufferIndex.Value - 1;
				bufferIndex.Value = num2;
				if (num2 >= 0)
				{
					currentElement = this._buffer[this._bufferIndex.Value].First;
					currentKey = this._buffer[this._bufferIndex.Value].Second;
					return true;
				}
				return false;
			}

			// Token: 0x06000A9C RID: 2716 RVA: 0x00021AF5 File Offset: 0x0001FCF5
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x04000719 RID: 1817
			private readonly QueryOperatorEnumerator<TSource, TKey> _source;

			// Token: 0x0400071A RID: 1818
			private readonly CancellationToken _cancellationToken;

			// Token: 0x0400071B RID: 1819
			private List<Pair<TSource, TKey>> _buffer;

			// Token: 0x0400071C RID: 1820
			private Shared<int> _bufferIndex;
		}

		// Token: 0x0200019F RID: 415
		private class ReverseQueryOperatorResults : UnaryQueryOperator<TSource, TSource>.UnaryQueryOperatorResults
		{
			// Token: 0x06000A9D RID: 2717 RVA: 0x00021B02 File Offset: 0x0001FD02
			public static QueryResults<TSource> NewResults(QueryResults<TSource> childQueryResults, ReverseQueryOperator<TSource> op, QuerySettings settings, bool preferStriping)
			{
				if (childQueryResults.IsIndexible)
				{
					return new ReverseQueryOperator<TSource>.ReverseQueryOperatorResults(childQueryResults, op, settings, preferStriping);
				}
				return new UnaryQueryOperator<TSource, TSource>.UnaryQueryOperatorResults(childQueryResults, op, settings, preferStriping);
			}

			// Token: 0x06000A9E RID: 2718 RVA: 0x00021B1F File Offset: 0x0001FD1F
			private ReverseQueryOperatorResults(QueryResults<TSource> childQueryResults, ReverseQueryOperator<TSource> op, QuerySettings settings, bool preferStriping) : base(childQueryResults, op, settings, preferStriping)
			{
				this._count = this._childQueryResults.ElementsCount;
			}

			// Token: 0x17000174 RID: 372
			// (get) Token: 0x06000A9F RID: 2719 RVA: 0x00009CDF File Offset: 0x00007EDF
			internal override bool IsIndexible
			{
				get
				{
					return true;
				}
			}

			// Token: 0x17000175 RID: 373
			// (get) Token: 0x06000AA0 RID: 2720 RVA: 0x00021B3D File Offset: 0x0001FD3D
			internal override int ElementsCount
			{
				get
				{
					return this._count;
				}
			}

			// Token: 0x06000AA1 RID: 2721 RVA: 0x00021B45 File Offset: 0x0001FD45
			internal override TSource GetElement(int index)
			{
				return this._childQueryResults.GetElement(this._count - index - 1);
			}

			// Token: 0x0400071D RID: 1821
			private int _count;
		}
	}
}
