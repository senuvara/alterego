﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace System.Linq.Parallel
{
	// Token: 0x020000E8 RID: 232
	internal class ArrayMergeHelper<TInputOutput> : IMergeHelper<TInputOutput>
	{
		// Token: 0x060007E5 RID: 2021 RVA: 0x00017850 File Offset: 0x00015A50
		public ArrayMergeHelper(QuerySettings settings, QueryResults<TInputOutput> queryResults)
		{
			this._settings = settings;
			this._queryResults = queryResults;
			int count = this._queryResults.Count;
			this._outputArray = new TInputOutput[count];
		}

		// Token: 0x060007E6 RID: 2022 RVA: 0x00017889 File Offset: 0x00015A89
		private void ToArrayElement(int index)
		{
			this._outputArray[index] = this._queryResults[index];
		}

		// Token: 0x060007E7 RID: 2023 RVA: 0x000178A3 File Offset: 0x00015AA3
		public void Execute()
		{
			new QueryExecutionOption<int>(QueryOperator<int>.AsQueryOperator(ParallelEnumerable.Range(0, this._queryResults.Count)), this._settings).ForAll(new Action<int>(this.ToArrayElement));
		}

		// Token: 0x060007E8 RID: 2024 RVA: 0x000178D7 File Offset: 0x00015AD7
		[ExcludeFromCodeCoverage]
		public IEnumerator<TInputOutput> GetEnumerator()
		{
			return this.GetResultsAsArray().GetEnumerator();
		}

		// Token: 0x060007E9 RID: 2025 RVA: 0x000178E4 File Offset: 0x00015AE4
		public TInputOutput[] GetResultsAsArray()
		{
			return this._outputArray;
		}

		// Token: 0x0400053F RID: 1343
		private QueryResults<TInputOutput> _queryResults;

		// Token: 0x04000540 RID: 1344
		private TInputOutput[] _outputArray;

		// Token: 0x04000541 RID: 1345
		private QuerySettings _settings;
	}
}
