﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000188 RID: 392
	internal abstract class GroupByQueryOperatorEnumerator<TSource, TGroupKey, TElement, TOrderKey> : QueryOperatorEnumerator<IGrouping<TGroupKey, TElement>, TOrderKey>
	{
		// Token: 0x06000A4F RID: 2639 RVA: 0x00020B7C File Offset: 0x0001ED7C
		protected GroupByQueryOperatorEnumerator(QueryOperatorEnumerator<Pair<TSource, TGroupKey>, TOrderKey> source, IEqualityComparer<TGroupKey> keyComparer, CancellationToken cancellationToken)
		{
			this._source = source;
			this._keyComparer = keyComparer;
			this._cancellationToken = cancellationToken;
		}

		// Token: 0x06000A50 RID: 2640 RVA: 0x00020B9C File Offset: 0x0001ED9C
		internal override bool MoveNext(ref IGrouping<TGroupKey, TElement> currentElement, ref TOrderKey currentKey)
		{
			GroupByQueryOperatorEnumerator<TSource, TGroupKey, TElement, TOrderKey>.Mutables mutables = this._mutables;
			if (mutables == null)
			{
				mutables = (this._mutables = new GroupByQueryOperatorEnumerator<TSource, TGroupKey, TElement, TOrderKey>.Mutables());
				mutables._hashLookup = this.BuildHashLookup();
				mutables._hashLookupIndex = -1;
			}
			GroupByQueryOperatorEnumerator<TSource, TGroupKey, TElement, TOrderKey>.Mutables mutables2 = mutables;
			int num = mutables2._hashLookupIndex + 1;
			mutables2._hashLookupIndex = num;
			if (num < mutables._hashLookup.Count)
			{
				currentElement = new GroupByGrouping<TGroupKey, TElement>(mutables._hashLookup[mutables._hashLookupIndex]);
				return true;
			}
			return false;
		}

		// Token: 0x06000A51 RID: 2641
		protected abstract HashLookup<Wrapper<TGroupKey>, ListChunk<TElement>> BuildHashLookup();

		// Token: 0x06000A52 RID: 2642 RVA: 0x00020C0E File Offset: 0x0001EE0E
		protected override void Dispose(bool disposing)
		{
			this._source.Dispose();
		}

		// Token: 0x040006E0 RID: 1760
		protected readonly QueryOperatorEnumerator<Pair<TSource, TGroupKey>, TOrderKey> _source;

		// Token: 0x040006E1 RID: 1761
		protected readonly IEqualityComparer<TGroupKey> _keyComparer;

		// Token: 0x040006E2 RID: 1762
		protected readonly CancellationToken _cancellationToken;

		// Token: 0x040006E3 RID: 1763
		private GroupByQueryOperatorEnumerator<TSource, TGroupKey, TElement, TOrderKey>.Mutables _mutables;

		// Token: 0x02000189 RID: 393
		private class Mutables
		{
			// Token: 0x06000A53 RID: 2643 RVA: 0x00002310 File Offset: 0x00000510
			public Mutables()
			{
			}

			// Token: 0x040006E4 RID: 1764
			internal HashLookup<Wrapper<TGroupKey>, ListChunk<TElement>> _hashLookup;

			// Token: 0x040006E5 RID: 1765
			internal int _hashLookupIndex;
		}
	}
}
