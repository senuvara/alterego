﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace System.Linq.Parallel
{
	// Token: 0x020001C2 RID: 450
	internal class QueryTaskGroupState
	{
		// Token: 0x06000B21 RID: 2849 RVA: 0x00023B8A File Offset: 0x00021D8A
		internal QueryTaskGroupState(CancellationState cancellationState, int queryId)
		{
			this._cancellationState = cancellationState;
			this._queryId = queryId;
		}

		// Token: 0x17000185 RID: 389
		// (get) Token: 0x06000B22 RID: 2850 RVA: 0x00023BA0 File Offset: 0x00021DA0
		internal bool IsAlreadyEnded
		{
			get
			{
				return this._alreadyEnded == 1;
			}
		}

		// Token: 0x17000186 RID: 390
		// (get) Token: 0x06000B23 RID: 2851 RVA: 0x00023BAB File Offset: 0x00021DAB
		internal CancellationState CancellationState
		{
			get
			{
				return this._cancellationState;
			}
		}

		// Token: 0x17000187 RID: 391
		// (get) Token: 0x06000B24 RID: 2852 RVA: 0x00023BB3 File Offset: 0x00021DB3
		internal int QueryId
		{
			get
			{
				return this._queryId;
			}
		}

		// Token: 0x06000B25 RID: 2853 RVA: 0x00023BBB File Offset: 0x00021DBB
		internal void QueryBegin(Task rootTask)
		{
			this._rootTask = rootTask;
		}

		// Token: 0x06000B26 RID: 2854 RVA: 0x00023BC4 File Offset: 0x00021DC4
		internal void QueryEnd(bool userInitiatedDispose)
		{
			if (Interlocked.Exchange(ref this._alreadyEnded, 1) == 0)
			{
				try
				{
					this._rootTask.Wait();
				}
				catch (AggregateException ex)
				{
					AggregateException ex2 = ex.Flatten();
					bool flag = true;
					for (int i = 0; i < ex2.InnerExceptions.Count; i++)
					{
						OperationCanceledException ex3 = ex2.InnerExceptions[i] as OperationCanceledException;
						if (ex3 == null || !ex3.CancellationToken.IsCancellationRequested || ex3.CancellationToken != this._cancellationState.ExternalCancellationToken)
						{
							flag = false;
							break;
						}
					}
					if (!flag)
					{
						throw ex2;
					}
				}
				finally
				{
					IDisposable rootTask = this._rootTask;
					if (rootTask != null)
					{
						rootTask.Dispose();
					}
				}
				if (this._cancellationState.MergedCancellationToken.IsCancellationRequested)
				{
					if (!this._cancellationState.TopLevelDisposedFlag.Value)
					{
						CancellationState.ThrowWithStandardMessageIfCanceled(this._cancellationState.ExternalCancellationToken);
					}
					if (!userInitiatedDispose)
					{
						throw new ObjectDisposedException("enumerator", "The query enumerator has been disposed.");
					}
				}
			}
		}

		// Token: 0x040007A3 RID: 1955
		private Task _rootTask;

		// Token: 0x040007A4 RID: 1956
		private int _alreadyEnded;

		// Token: 0x040007A5 RID: 1957
		private CancellationState _cancellationState;

		// Token: 0x040007A6 RID: 1958
		private int _queryId;
	}
}
