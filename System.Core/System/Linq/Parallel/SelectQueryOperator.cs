﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x020001A5 RID: 421
	internal sealed class SelectQueryOperator<TInput, TOutput> : UnaryQueryOperator<TInput, TOutput>
	{
		// Token: 0x06000AB2 RID: 2738 RVA: 0x00022192 File Offset: 0x00020392
		internal SelectQueryOperator(IEnumerable<TInput> child, Func<TInput, TOutput> selector) : base(child)
		{
			this._selector = selector;
			base.SetOrdinalIndexState(base.Child.OrdinalIndexState);
		}

		// Token: 0x06000AB3 RID: 2739 RVA: 0x000221B4 File Offset: 0x000203B4
		internal override void WrapPartitionedStream<TKey>(PartitionedStream<TInput, TKey> inputStream, IPartitionedStreamRecipient<TOutput> recipient, bool preferStriping, QuerySettings settings)
		{
			PartitionedStream<TOutput, TKey> partitionedStream = new PartitionedStream<TOutput, TKey>(inputStream.PartitionCount, inputStream.KeyComparer, this.OrdinalIndexState);
			for (int i = 0; i < inputStream.PartitionCount; i++)
			{
				partitionedStream[i] = new SelectQueryOperator<TInput, TOutput>.SelectQueryOperatorEnumerator<TKey>(inputStream[i], this._selector);
			}
			recipient.Receive<TKey>(partitionedStream);
		}

		// Token: 0x06000AB4 RID: 2740 RVA: 0x0002220A File Offset: 0x0002040A
		internal override QueryResults<TOutput> Open(QuerySettings settings, bool preferStriping)
		{
			return SelectQueryOperator<TInput, TOutput>.SelectQueryOperatorResults.NewResults(base.Child.Open(settings, preferStriping), this, settings, preferStriping);
		}

		// Token: 0x06000AB5 RID: 2741 RVA: 0x00022221 File Offset: 0x00020421
		internal override IEnumerable<TOutput> AsSequentialQuery(CancellationToken token)
		{
			return base.Child.AsSequentialQuery(token).Select(this._selector);
		}

		// Token: 0x17000177 RID: 375
		// (get) Token: 0x06000AB6 RID: 2742 RVA: 0x00002285 File Offset: 0x00000485
		internal override bool LimitsParallelism
		{
			get
			{
				return false;
			}
		}

		// Token: 0x04000737 RID: 1847
		private Func<TInput, TOutput> _selector;

		// Token: 0x020001A6 RID: 422
		private class SelectQueryOperatorEnumerator<TKey> : QueryOperatorEnumerator<TOutput, TKey>
		{
			// Token: 0x06000AB7 RID: 2743 RVA: 0x0002223A File Offset: 0x0002043A
			internal SelectQueryOperatorEnumerator(QueryOperatorEnumerator<TInput, TKey> source, Func<TInput, TOutput> selector)
			{
				this._source = source;
				this._selector = selector;
			}

			// Token: 0x06000AB8 RID: 2744 RVA: 0x00022250 File Offset: 0x00020450
			internal override bool MoveNext(ref TOutput currentElement, ref TKey currentKey)
			{
				TInput arg = default(TInput);
				if (this._source.MoveNext(ref arg, ref currentKey))
				{
					currentElement = this._selector(arg);
					return true;
				}
				return false;
			}

			// Token: 0x06000AB9 RID: 2745 RVA: 0x0002228A File Offset: 0x0002048A
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x04000738 RID: 1848
			private readonly QueryOperatorEnumerator<TInput, TKey> _source;

			// Token: 0x04000739 RID: 1849
			private readonly Func<TInput, TOutput> _selector;
		}

		// Token: 0x020001A7 RID: 423
		private class SelectQueryOperatorResults : UnaryQueryOperator<TInput, TOutput>.UnaryQueryOperatorResults
		{
			// Token: 0x06000ABA RID: 2746 RVA: 0x00022297 File Offset: 0x00020497
			public static QueryResults<TOutput> NewResults(QueryResults<TInput> childQueryResults, SelectQueryOperator<TInput, TOutput> op, QuerySettings settings, bool preferStriping)
			{
				if (childQueryResults.IsIndexible)
				{
					return new SelectQueryOperator<TInput, TOutput>.SelectQueryOperatorResults(childQueryResults, op, settings, preferStriping);
				}
				return new UnaryQueryOperator<TInput, TOutput>.UnaryQueryOperatorResults(childQueryResults, op, settings, preferStriping);
			}

			// Token: 0x06000ABB RID: 2747 RVA: 0x000222B4 File Offset: 0x000204B4
			private SelectQueryOperatorResults(QueryResults<TInput> childQueryResults, SelectQueryOperator<TInput, TOutput> op, QuerySettings settings, bool preferStriping) : base(childQueryResults, op, settings, preferStriping)
			{
				this._selector = op._selector;
				this._childCount = this._childQueryResults.ElementsCount;
			}

			// Token: 0x17000178 RID: 376
			// (get) Token: 0x06000ABC RID: 2748 RVA: 0x00009CDF File Offset: 0x00007EDF
			internal override bool IsIndexible
			{
				get
				{
					return true;
				}
			}

			// Token: 0x17000179 RID: 377
			// (get) Token: 0x06000ABD RID: 2749 RVA: 0x000222DE File Offset: 0x000204DE
			internal override int ElementsCount
			{
				get
				{
					return this._childCount;
				}
			}

			// Token: 0x06000ABE RID: 2750 RVA: 0x000222E6 File Offset: 0x000204E6
			internal override TOutput GetElement(int index)
			{
				return this._selector(this._childQueryResults.GetElement(index));
			}

			// Token: 0x0400073A RID: 1850
			private Func<TInput, TOutput> _selector;

			// Token: 0x0400073B RID: 1851
			private int _childCount;
		}
	}
}
