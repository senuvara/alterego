﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x0200014A RID: 330
	internal sealed class NullableDecimalSumAggregationOperator : InlinedAggregationOperator<decimal?, decimal?, decimal?>
	{
		// Token: 0x06000936 RID: 2358 RVA: 0x0001D4B9 File Offset: 0x0001B6B9
		internal NullableDecimalSumAggregationOperator(IEnumerable<decimal?> child) : base(child)
		{
		}

		// Token: 0x06000937 RID: 2359 RVA: 0x0001D4C4 File Offset: 0x0001B6C4
		protected override decimal? InternalAggregate(ref Exception singularExceptionToThrow)
		{
			decimal? result;
			using (IEnumerator<decimal?> enumerator = this.GetEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered), true))
			{
				decimal num = 0.0m;
				while (enumerator.MoveNext())
				{
					decimal d = num;
					result = enumerator.Current;
					num = d + result.GetValueOrDefault();
				}
				result = new decimal?(num);
			}
			return result;
		}

		// Token: 0x06000938 RID: 2360 RVA: 0x0001D530 File Offset: 0x0001B730
		protected override QueryOperatorEnumerator<decimal?, int> CreateEnumerator<TKey>(int index, int count, QueryOperatorEnumerator<decimal?, TKey> source, object sharedData, CancellationToken cancellationToken)
		{
			return new NullableDecimalSumAggregationOperator.NullableDecimalSumAggregationOperatorEnumerator<TKey>(source, index, cancellationToken);
		}

		// Token: 0x0200014B RID: 331
		private class NullableDecimalSumAggregationOperatorEnumerator<TKey> : InlinedAggregationOperatorEnumerator<decimal?>
		{
			// Token: 0x06000939 RID: 2361 RVA: 0x0001D53B File Offset: 0x0001B73B
			internal NullableDecimalSumAggregationOperatorEnumerator(QueryOperatorEnumerator<decimal?, TKey> source, int partitionIndex, CancellationToken cancellationToken) : base(partitionIndex, cancellationToken)
			{
				this._source = source;
			}

			// Token: 0x0600093A RID: 2362 RVA: 0x0001D54C File Offset: 0x0001B74C
			protected override bool MoveNextCore(ref decimal? currentElement)
			{
				decimal? num = null;
				TKey tkey = default(TKey);
				QueryOperatorEnumerator<decimal?, TKey> source = this._source;
				if (source.MoveNext(ref num, ref tkey))
				{
					decimal num2 = 0.0m;
					int num3 = 0;
					do
					{
						if ((num3++ & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(this._cancellationToken);
						}
						num2 += num.GetValueOrDefault();
					}
					while (source.MoveNext(ref num, ref tkey));
					currentElement = new decimal?(num2);
					return true;
				}
				return false;
			}

			// Token: 0x0600093B RID: 2363 RVA: 0x0001D5CB File Offset: 0x0001B7CB
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x04000658 RID: 1624
			private readonly QueryOperatorEnumerator<decimal?, TKey> _source;
		}
	}
}
