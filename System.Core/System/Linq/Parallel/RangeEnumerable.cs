﻿using System;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x020000E1 RID: 225
	internal class RangeEnumerable : ParallelQuery<int>, IParallelPartitionable<int>
	{
		// Token: 0x060007D0 RID: 2000 RVA: 0x000172DB File Offset: 0x000154DB
		internal RangeEnumerable(int from, int count) : base(QuerySettings.Empty)
		{
			this._from = from;
			this._count = count;
		}

		// Token: 0x060007D1 RID: 2001 RVA: 0x000172F8 File Offset: 0x000154F8
		public QueryOperatorEnumerator<int, int>[] GetPartitions(int partitionCount)
		{
			int num = this._count / partitionCount;
			int num2 = this._count % partitionCount;
			int num3 = 0;
			QueryOperatorEnumerator<int, int>[] array = new QueryOperatorEnumerator<int, int>[partitionCount];
			for (int i = 0; i < partitionCount; i++)
			{
				int num4 = (i < num2) ? (num + 1) : num;
				array[i] = new RangeEnumerable.RangeEnumerator(this._from + num3, num4, num3);
				num3 += num4;
			}
			return array;
		}

		// Token: 0x060007D2 RID: 2002 RVA: 0x00017357 File Offset: 0x00015557
		public override IEnumerator<int> GetEnumerator()
		{
			return new RangeEnumerable.RangeEnumerator(this._from, this._count, 0).AsClassicEnumerator();
		}

		// Token: 0x0400052A RID: 1322
		private int _from;

		// Token: 0x0400052B RID: 1323
		private int _count;

		// Token: 0x020000E2 RID: 226
		private class RangeEnumerator : QueryOperatorEnumerator<int, int>
		{
			// Token: 0x060007D3 RID: 2003 RVA: 0x00017370 File Offset: 0x00015570
			internal RangeEnumerator(int from, int count, int initialIndex)
			{
				this._from = from;
				this._count = count;
				this._initialIndex = initialIndex;
			}

			// Token: 0x060007D4 RID: 2004 RVA: 0x00017390 File Offset: 0x00015590
			internal override bool MoveNext(ref int currentElement, ref int currentKey)
			{
				if (this._currentCount == null)
				{
					this._currentCount = new Shared<int>(-1);
				}
				int num = this._currentCount.Value + 1;
				if (num < this._count)
				{
					this._currentCount.Value = num;
					currentElement = num + this._from;
					currentKey = num + this._initialIndex;
					return true;
				}
				return false;
			}

			// Token: 0x060007D5 RID: 2005 RVA: 0x000173EB File Offset: 0x000155EB
			internal override void Reset()
			{
				this._currentCount = null;
			}

			// Token: 0x0400052C RID: 1324
			private readonly int _from;

			// Token: 0x0400052D RID: 1325
			private readonly int _count;

			// Token: 0x0400052E RID: 1326
			private readonly int _initialIndex;

			// Token: 0x0400052F RID: 1327
			private Shared<int> _currentCount;
		}
	}
}
