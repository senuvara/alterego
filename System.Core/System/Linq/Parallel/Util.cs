﻿using System;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x020001E6 RID: 486
	internal static class Util
	{
		// Token: 0x06000BC3 RID: 3011 RVA: 0x00025D91 File Offset: 0x00023F91
		internal static int Sign(int x)
		{
			if (x < 0)
			{
				return -1;
			}
			if (x != 0)
			{
				return 1;
			}
			return 0;
		}

		// Token: 0x06000BC4 RID: 3012 RVA: 0x00025DA0 File Offset: 0x00023FA0
		internal static Comparer<TKey> GetDefaultComparer<TKey>()
		{
			if (typeof(TKey) == typeof(int))
			{
				return (Comparer<TKey>)Util.s_fastIntComparer;
			}
			if (typeof(TKey) == typeof(long))
			{
				return (Comparer<TKey>)Util.s_fastLongComparer;
			}
			if (typeof(TKey) == typeof(float))
			{
				return (Comparer<TKey>)Util.s_fastFloatComparer;
			}
			if (typeof(TKey) == typeof(double))
			{
				return (Comparer<TKey>)Util.s_fastDoubleComparer;
			}
			if (typeof(TKey) == typeof(DateTime))
			{
				return (Comparer<TKey>)Util.s_fastDateTimeComparer;
			}
			return Comparer<TKey>.Default;
		}

		// Token: 0x06000BC5 RID: 3013 RVA: 0x00025E70 File Offset: 0x00024070
		// Note: this type is marked as 'beforefieldinit'.
		static Util()
		{
		}

		// Token: 0x04000814 RID: 2068
		private static Util.FastIntComparer s_fastIntComparer = new Util.FastIntComparer();

		// Token: 0x04000815 RID: 2069
		private static Util.FastLongComparer s_fastLongComparer = new Util.FastLongComparer();

		// Token: 0x04000816 RID: 2070
		private static Util.FastFloatComparer s_fastFloatComparer = new Util.FastFloatComparer();

		// Token: 0x04000817 RID: 2071
		private static Util.FastDoubleComparer s_fastDoubleComparer = new Util.FastDoubleComparer();

		// Token: 0x04000818 RID: 2072
		private static Util.FastDateTimeComparer s_fastDateTimeComparer = new Util.FastDateTimeComparer();

		// Token: 0x020001E7 RID: 487
		private class FastIntComparer : Comparer<int>
		{
			// Token: 0x06000BC6 RID: 3014 RVA: 0x00025EA4 File Offset: 0x000240A4
			public override int Compare(int x, int y)
			{
				return x.CompareTo(y);
			}

			// Token: 0x06000BC7 RID: 3015 RVA: 0x00025EAE File Offset: 0x000240AE
			public FastIntComparer()
			{
			}
		}

		// Token: 0x020001E8 RID: 488
		private class FastLongComparer : Comparer<long>
		{
			// Token: 0x06000BC8 RID: 3016 RVA: 0x00025EB6 File Offset: 0x000240B6
			public override int Compare(long x, long y)
			{
				return x.CompareTo(y);
			}

			// Token: 0x06000BC9 RID: 3017 RVA: 0x00025EC0 File Offset: 0x000240C0
			public FastLongComparer()
			{
			}
		}

		// Token: 0x020001E9 RID: 489
		private class FastFloatComparer : Comparer<float>
		{
			// Token: 0x06000BCA RID: 3018 RVA: 0x00025EC8 File Offset: 0x000240C8
			public override int Compare(float x, float y)
			{
				return x.CompareTo(y);
			}

			// Token: 0x06000BCB RID: 3019 RVA: 0x00025ED2 File Offset: 0x000240D2
			public FastFloatComparer()
			{
			}
		}

		// Token: 0x020001EA RID: 490
		private class FastDoubleComparer : Comparer<double>
		{
			// Token: 0x06000BCC RID: 3020 RVA: 0x00025EDA File Offset: 0x000240DA
			public override int Compare(double x, double y)
			{
				return x.CompareTo(y);
			}

			// Token: 0x06000BCD RID: 3021 RVA: 0x00025EE4 File Offset: 0x000240E4
			public FastDoubleComparer()
			{
			}
		}

		// Token: 0x020001EB RID: 491
		private class FastDateTimeComparer : Comparer<DateTime>
		{
			// Token: 0x06000BCE RID: 3022 RVA: 0x00025EEC File Offset: 0x000240EC
			public override int Compare(DateTime x, DateTime y)
			{
				return x.CompareTo(y);
			}

			// Token: 0x06000BCF RID: 3023 RVA: 0x00025EF6 File Offset: 0x000240F6
			public FastDateTimeComparer()
			{
			}
		}
	}
}
