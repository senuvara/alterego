﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x020001AA RID: 426
	internal sealed class SortQueryOperator<TInputOutput, TSortKey> : UnaryQueryOperator<TInputOutput, TInputOutput>, IOrderedEnumerable<TInputOutput>, IEnumerable<!0>, IEnumerable
	{
		// Token: 0x06000AC7 RID: 2759 RVA: 0x00022444 File Offset: 0x00020644
		internal SortQueryOperator(IEnumerable<TInputOutput> source, Func<TInputOutput, TSortKey> keySelector, IComparer<TSortKey> comparer, bool descending) : base(source, true)
		{
			this._keySelector = keySelector;
			if (comparer == null)
			{
				this._comparer = Util.GetDefaultComparer<TSortKey>();
			}
			else
			{
				this._comparer = comparer;
			}
			if (descending)
			{
				this._comparer = new ReverseComparer<TSortKey>(this._comparer);
			}
			base.SetOrdinalIndexState(OrdinalIndexState.Shuffled);
		}

		// Token: 0x06000AC8 RID: 2760 RVA: 0x00022494 File Offset: 0x00020694
		IOrderedEnumerable<TInputOutput> IOrderedEnumerable<!0>.CreateOrderedEnumerable<TKey2>(Func<TInputOutput, TKey2> key2Selector, IComparer<TKey2> key2Comparer, bool descending)
		{
			key2Comparer = (key2Comparer ?? Util.GetDefaultComparer<TKey2>());
			if (descending)
			{
				key2Comparer = new ReverseComparer<TKey2>(key2Comparer);
			}
			IComparer<Pair<TSortKey, TKey2>> comparer = new PairComparer<TSortKey, TKey2>(this._comparer, key2Comparer);
			Func<TInputOutput, Pair<TSortKey, TKey2>> keySelector = (TInputOutput elem) => new Pair<TSortKey, TKey2>(this._keySelector(elem), key2Selector(elem));
			return new SortQueryOperator<TInputOutput, Pair<TSortKey, TKey2>>(base.Child, keySelector, comparer, false);
		}

		// Token: 0x06000AC9 RID: 2761 RVA: 0x000224F2 File Offset: 0x000206F2
		internal override QueryResults<TInputOutput> Open(QuerySettings settings, bool preferStriping)
		{
			return new SortQueryOperatorResults<TInputOutput, TSortKey>(base.Child.Open(settings, false), this, settings);
		}

		// Token: 0x06000ACA RID: 2762 RVA: 0x00022508 File Offset: 0x00020708
		internal override void WrapPartitionedStream<TKey>(PartitionedStream<TInputOutput, TKey> inputStream, IPartitionedStreamRecipient<TInputOutput> recipient, bool preferStriping, QuerySettings settings)
		{
			PartitionedStream<TInputOutput, TSortKey> partitionedStream = new PartitionedStream<TInputOutput, TSortKey>(inputStream.PartitionCount, this._comparer, this.OrdinalIndexState);
			for (int i = 0; i < partitionedStream.PartitionCount; i++)
			{
				partitionedStream[i] = new SortQueryOperatorEnumerator<TInputOutput, TKey, TSortKey>(inputStream[i], this._keySelector);
			}
			recipient.Receive<TSortKey>(partitionedStream);
		}

		// Token: 0x06000ACB RID: 2763 RVA: 0x0002255E File Offset: 0x0002075E
		internal override IEnumerable<TInputOutput> AsSequentialQuery(CancellationToken token)
		{
			return CancellableEnumerable.Wrap<TInputOutput>(base.Child.AsSequentialQuery(token), token).OrderBy(this._keySelector, this._comparer);
		}

		// Token: 0x1700017B RID: 379
		// (get) Token: 0x06000ACC RID: 2764 RVA: 0x00002285 File Offset: 0x00000485
		internal override bool LimitsParallelism
		{
			get
			{
				return false;
			}
		}

		// Token: 0x04000742 RID: 1858
		private readonly Func<TInputOutput, TSortKey> _keySelector;

		// Token: 0x04000743 RID: 1859
		private readonly IComparer<TSortKey> _comparer;

		// Token: 0x020001AB RID: 427
		[CompilerGenerated]
		private sealed class <>c__DisplayClass3_0<TKey2>
		{
			// Token: 0x06000ACD RID: 2765 RVA: 0x00002310 File Offset: 0x00000510
			public <>c__DisplayClass3_0()
			{
			}

			// Token: 0x06000ACE RID: 2766 RVA: 0x00022583 File Offset: 0x00020783
			internal Pair<TSortKey, TKey2> <System.Linq.IOrderedEnumerable<TInputOutput>.CreateOrderedEnumerable>b__0(TInputOutput elem)
			{
				return new Pair<TSortKey, TKey2>(this.<>4__this._keySelector(elem), this.key2Selector(elem));
			}

			// Token: 0x04000744 RID: 1860
			public SortQueryOperator<TInputOutput, TSortKey> <>4__this;

			// Token: 0x04000745 RID: 1861
			public Func<TInputOutput, TKey2> key2Selector;
		}
	}
}
