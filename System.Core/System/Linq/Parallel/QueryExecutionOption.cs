﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000166 RID: 358
	internal class QueryExecutionOption<TSource> : QueryOperator<TSource>
	{
		// Token: 0x06000990 RID: 2448 RVA: 0x0001EA6F File Offset: 0x0001CC6F
		internal QueryExecutionOption(QueryOperator<TSource> source, QuerySettings settings) : base(source.OutputOrdered, settings.Merge(source.SpecifiedQuerySettings))
		{
			this._child = source;
			this._indexState = this._child.OrdinalIndexState;
		}

		// Token: 0x06000991 RID: 2449 RVA: 0x0001EAA2 File Offset: 0x0001CCA2
		internal override QueryResults<TSource> Open(QuerySettings settings, bool preferStriping)
		{
			return this._child.Open(settings, preferStriping);
		}

		// Token: 0x06000992 RID: 2450 RVA: 0x0001EAB1 File Offset: 0x0001CCB1
		internal override IEnumerable<TSource> AsSequentialQuery(CancellationToken token)
		{
			return this._child.AsSequentialQuery(token);
		}

		// Token: 0x17000142 RID: 322
		// (get) Token: 0x06000993 RID: 2451 RVA: 0x0001EABF File Offset: 0x0001CCBF
		internal override OrdinalIndexState OrdinalIndexState
		{
			get
			{
				return this._indexState;
			}
		}

		// Token: 0x17000143 RID: 323
		// (get) Token: 0x06000994 RID: 2452 RVA: 0x0001EAC7 File Offset: 0x0001CCC7
		internal override bool LimitsParallelism
		{
			get
			{
				return this._child.LimitsParallelism;
			}
		}

		// Token: 0x04000672 RID: 1650
		private QueryOperator<TSource> _child;

		// Token: 0x04000673 RID: 1651
		private OrdinalIndexState _indexState;
	}
}
