﻿using System;

namespace System.Linq.Parallel
{
	// Token: 0x020000E0 RID: 224
	internal static class QueryAggregationOptionsExtensions
	{
		// Token: 0x060007CF RID: 1999 RVA: 0x000172C8 File Offset: 0x000154C8
		public static bool IsValidQueryAggregationOption(this QueryAggregationOptions value)
		{
			return value == QueryAggregationOptions.None || value == QueryAggregationOptions.Associative || value == QueryAggregationOptions.Commutative || value == QueryAggregationOptions.AssociativeCommutative;
		}
	}
}
