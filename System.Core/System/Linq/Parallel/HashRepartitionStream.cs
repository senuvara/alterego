﻿using System;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x020000F7 RID: 247
	internal abstract class HashRepartitionStream<TInputOutput, THashKey, TOrderKey> : PartitionedStream<Pair<TInputOutput, THashKey>, TOrderKey>
	{
		// Token: 0x06000820 RID: 2080 RVA: 0x0001890B File Offset: 0x00016B0B
		internal HashRepartitionStream(int partitionsCount, IComparer<TOrderKey> orderKeyComparer, IEqualityComparer<THashKey> hashKeyComparer, IEqualityComparer<TInputOutput> elementComparer) : base(partitionsCount, orderKeyComparer, OrdinalIndexState.Shuffled)
		{
			this._keyComparer = hashKeyComparer;
			this._elementComparer = elementComparer;
			this._distributionMod = 503;
			checked
			{
				while (this._distributionMod < partitionsCount)
				{
					this._distributionMod *= 2;
				}
			}
		}

		// Token: 0x06000821 RID: 2081 RVA: 0x00018949 File Offset: 0x00016B49
		internal int GetHashCode(TInputOutput element)
		{
			return (int.MaxValue & ((this._elementComparer == null) ? ((element == null) ? 0 : element.GetHashCode()) : this._elementComparer.GetHashCode(element))) % this._distributionMod;
		}

		// Token: 0x06000822 RID: 2082 RVA: 0x00018986 File Offset: 0x00016B86
		internal int GetHashCode(THashKey key)
		{
			return (int.MaxValue & ((this._keyComparer == null) ? ((key == null) ? 0 : key.GetHashCode()) : this._keyComparer.GetHashCode(key))) % this._distributionMod;
		}

		// Token: 0x04000579 RID: 1401
		private readonly IEqualityComparer<THashKey> _keyComparer;

		// Token: 0x0400057A RID: 1402
		private readonly IEqualityComparer<TInputOutput> _elementComparer;

		// Token: 0x0400057B RID: 1403
		private readonly int _distributionMod;

		// Token: 0x0400057C RID: 1404
		private const int NULL_ELEMENT_HASH_CODE = 0;

		// Token: 0x0400057D RID: 1405
		private const int HashCodeMask = 2147483647;
	}
}
