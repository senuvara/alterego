﻿using System;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x020001B7 RID: 439
	internal abstract class UnaryQueryOperator<TInput, TOutput> : QueryOperator<TOutput>
	{
		// Token: 0x06000AF9 RID: 2809 RVA: 0x000233A3 File Offset: 0x000215A3
		internal UnaryQueryOperator(IEnumerable<TInput> child) : this(QueryOperator<TInput>.AsQueryOperator(child))
		{
		}

		// Token: 0x06000AFA RID: 2810 RVA: 0x000233B1 File Offset: 0x000215B1
		internal UnaryQueryOperator(IEnumerable<TInput> child, bool outputOrdered) : this(QueryOperator<TInput>.AsQueryOperator(child), outputOrdered)
		{
		}

		// Token: 0x06000AFB RID: 2811 RVA: 0x000233C0 File Offset: 0x000215C0
		private UnaryQueryOperator(QueryOperator<TInput> child) : this(child, child.OutputOrdered, child.SpecifiedQuerySettings)
		{
		}

		// Token: 0x06000AFC RID: 2812 RVA: 0x000233D5 File Offset: 0x000215D5
		internal UnaryQueryOperator(QueryOperator<TInput> child, bool outputOrdered) : this(child, outputOrdered, child.SpecifiedQuerySettings)
		{
		}

		// Token: 0x06000AFD RID: 2813 RVA: 0x000233E5 File Offset: 0x000215E5
		private UnaryQueryOperator(QueryOperator<TInput> child, bool outputOrdered, QuerySettings settings) : base(outputOrdered, settings)
		{
			this._child = child;
		}

		// Token: 0x17000182 RID: 386
		// (get) Token: 0x06000AFE RID: 2814 RVA: 0x000233FD File Offset: 0x000215FD
		internal QueryOperator<TInput> Child
		{
			get
			{
				return this._child;
			}
		}

		// Token: 0x17000183 RID: 387
		// (get) Token: 0x06000AFF RID: 2815 RVA: 0x00023405 File Offset: 0x00021605
		internal sealed override OrdinalIndexState OrdinalIndexState
		{
			get
			{
				return this._indexState;
			}
		}

		// Token: 0x06000B00 RID: 2816 RVA: 0x0002340D File Offset: 0x0002160D
		protected void SetOrdinalIndexState(OrdinalIndexState indexState)
		{
			this._indexState = indexState;
		}

		// Token: 0x06000B01 RID: 2817
		internal abstract void WrapPartitionedStream<TKey>(PartitionedStream<TInput, TKey> inputStream, IPartitionedStreamRecipient<TOutput> recipient, bool preferStriping, QuerySettings settings);

		// Token: 0x04000774 RID: 1908
		private readonly QueryOperator<TInput> _child;

		// Token: 0x04000775 RID: 1909
		private OrdinalIndexState _indexState = OrdinalIndexState.Shuffled;

		// Token: 0x020001B8 RID: 440
		internal class UnaryQueryOperatorResults : QueryResults<TOutput>
		{
			// Token: 0x06000B02 RID: 2818 RVA: 0x00023416 File Offset: 0x00021616
			internal UnaryQueryOperatorResults(QueryResults<TInput> childQueryResults, UnaryQueryOperator<TInput, TOutput> op, QuerySettings settings, bool preferStriping)
			{
				this._childQueryResults = childQueryResults;
				this._op = op;
				this._settings = settings;
				this._preferStriping = preferStriping;
			}

			// Token: 0x06000B03 RID: 2819 RVA: 0x0002343C File Offset: 0x0002163C
			internal override void GivePartitionedStream(IPartitionedStreamRecipient<TOutput> recipient)
			{
				if (this._settings.ExecutionMode.Value == ParallelExecutionMode.Default && this._op.LimitsParallelism)
				{
					PartitionedStream<TOutput, int> partitionedStream = ExchangeUtilities.PartitionDataSource<TOutput>(this._op.AsSequentialQuery(this._settings.CancellationState.ExternalCancellationToken), this._settings.DegreeOfParallelism.Value, this._preferStriping);
					recipient.Receive<int>(partitionedStream);
					return;
				}
				if (this.IsIndexible)
				{
					PartitionedStream<TOutput, int> partitionedStream2 = ExchangeUtilities.PartitionDataSource<TOutput>(this, this._settings.DegreeOfParallelism.Value, this._preferStriping);
					recipient.Receive<int>(partitionedStream2);
					return;
				}
				this._childQueryResults.GivePartitionedStream(new UnaryQueryOperator<TInput, TOutput>.UnaryQueryOperatorResults.ChildResultsRecipient(recipient, this._op, this._preferStriping, this._settings));
			}

			// Token: 0x04000776 RID: 1910
			protected QueryResults<TInput> _childQueryResults;

			// Token: 0x04000777 RID: 1911
			private UnaryQueryOperator<TInput, TOutput> _op;

			// Token: 0x04000778 RID: 1912
			private QuerySettings _settings;

			// Token: 0x04000779 RID: 1913
			private bool _preferStriping;

			// Token: 0x020001B9 RID: 441
			private class ChildResultsRecipient : IPartitionedStreamRecipient<TInput>
			{
				// Token: 0x06000B04 RID: 2820 RVA: 0x00023500 File Offset: 0x00021700
				internal ChildResultsRecipient(IPartitionedStreamRecipient<TOutput> outputRecipient, UnaryQueryOperator<TInput, TOutput> op, bool preferStriping, QuerySettings settings)
				{
					this._outputRecipient = outputRecipient;
					this._op = op;
					this._preferStriping = preferStriping;
					this._settings = settings;
				}

				// Token: 0x06000B05 RID: 2821 RVA: 0x00023525 File Offset: 0x00021725
				public void Receive<TKey>(PartitionedStream<TInput, TKey> inputStream)
				{
					this._op.WrapPartitionedStream<TKey>(inputStream, this._outputRecipient, this._preferStriping, this._settings);
				}

				// Token: 0x0400077A RID: 1914
				private IPartitionedStreamRecipient<TOutput> _outputRecipient;

				// Token: 0x0400077B RID: 1915
				private UnaryQueryOperator<TInput, TOutput> _op;

				// Token: 0x0400077C RID: 1916
				private bool _preferStriping;

				// Token: 0x0400077D RID: 1917
				private QuerySettings _settings;
			}
		}
	}
}
