﻿using System;

namespace System.Linq.Parallel
{
	// Token: 0x020000F4 RID: 244
	internal sealed class SynchronousChannelMergeEnumerator<T> : MergeEnumerator<T>
	{
		// Token: 0x06000818 RID: 2072 RVA: 0x0001850B File Offset: 0x0001670B
		internal SynchronousChannelMergeEnumerator(QueryTaskGroupState taskGroupState, SynchronousChannel<T>[] channels) : base(taskGroupState)
		{
			this._channels = channels;
			this._channelIndex = -1;
		}

		// Token: 0x17000126 RID: 294
		// (get) Token: 0x06000819 RID: 2073 RVA: 0x00018522 File Offset: 0x00016722
		public override T Current
		{
			get
			{
				if (this._channelIndex == -1 || this._channelIndex == this._channels.Length)
				{
					throw new InvalidOperationException("Enumeration has not started. MoveNext must be called to initiate enumeration.");
				}
				return this._currentElement;
			}
		}

		// Token: 0x0600081A RID: 2074 RVA: 0x00018550 File Offset: 0x00016750
		public override bool MoveNext()
		{
			if (this._channelIndex == -1)
			{
				this._channelIndex = 0;
			}
			while (this._channelIndex != this._channels.Length)
			{
				SynchronousChannel<T> synchronousChannel = this._channels[this._channelIndex];
				if (synchronousChannel.Count != 0)
				{
					this._currentElement = synchronousChannel.Dequeue();
					return true;
				}
				this._channelIndex++;
			}
			return false;
		}

		// Token: 0x04000569 RID: 1385
		private SynchronousChannel<T>[] _channels;

		// Token: 0x0400056A RID: 1386
		private int _channelIndex;

		// Token: 0x0400056B RID: 1387
		private T _currentElement;
	}
}
