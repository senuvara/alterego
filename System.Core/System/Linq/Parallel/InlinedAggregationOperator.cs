﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000136 RID: 310
	internal abstract class InlinedAggregationOperator<TSource, TIntermediate, TResult> : UnaryQueryOperator<TSource, TIntermediate>
	{
		// Token: 0x060008F5 RID: 2293 RVA: 0x0001C772 File Offset: 0x0001A972
		internal InlinedAggregationOperator(IEnumerable<TSource> child) : base(child)
		{
		}

		// Token: 0x060008F6 RID: 2294 RVA: 0x0001C77C File Offset: 0x0001A97C
		internal TResult Aggregate()
		{
			Exception ex = null;
			TResult result;
			try
			{
				result = this.InternalAggregate(ref ex);
			}
			catch (Exception ex2)
			{
				if (ex2 is AggregateException)
				{
					throw;
				}
				OperationCanceledException ex3 = ex2 as OperationCanceledException;
				if (ex3 != null && ex3.CancellationToken == base.SpecifiedQuerySettings.CancellationState.ExternalCancellationToken && base.SpecifiedQuerySettings.CancellationState.ExternalCancellationToken.IsCancellationRequested)
				{
					throw;
				}
				throw new AggregateException(new Exception[]
				{
					ex2
				});
			}
			if (ex != null)
			{
				throw ex;
			}
			return result;
		}

		// Token: 0x060008F7 RID: 2295
		protected abstract TResult InternalAggregate(ref Exception singularExceptionToThrow);

		// Token: 0x060008F8 RID: 2296 RVA: 0x00019878 File Offset: 0x00017A78
		internal override QueryResults<TIntermediate> Open(QuerySettings settings, bool preferStriping)
		{
			return new UnaryQueryOperator<TSource, TIntermediate>.UnaryQueryOperatorResults(base.Child.Open(settings, preferStriping), this, settings, preferStriping);
		}

		// Token: 0x060008F9 RID: 2297 RVA: 0x0001C810 File Offset: 0x0001AA10
		internal override void WrapPartitionedStream<TKey>(PartitionedStream<TSource, TKey> inputStream, IPartitionedStreamRecipient<TIntermediate> recipient, bool preferStriping, QuerySettings settings)
		{
			int partitionCount = inputStream.PartitionCount;
			PartitionedStream<TIntermediate, int> partitionedStream = new PartitionedStream<TIntermediate, int>(partitionCount, Util.GetDefaultComparer<int>(), OrdinalIndexState.Correct);
			for (int i = 0; i < partitionCount; i++)
			{
				partitionedStream[i] = this.CreateEnumerator<TKey>(i, partitionCount, inputStream[i], null, settings.CancellationState.MergedCancellationToken);
			}
			recipient.Receive<int>(partitionedStream);
		}

		// Token: 0x060008FA RID: 2298
		protected abstract QueryOperatorEnumerator<TIntermediate, int> CreateEnumerator<TKey>(int index, int count, QueryOperatorEnumerator<TSource, TKey> source, object sharedData, CancellationToken cancellationToken);

		// Token: 0x060008FB RID: 2299 RVA: 0x00003A6B File Offset: 0x00001C6B
		[ExcludeFromCodeCoverage]
		internal override IEnumerable<TIntermediate> AsSequentialQuery(CancellationToken token)
		{
			throw new NotSupportedException();
		}

		// Token: 0x1700013D RID: 317
		// (get) Token: 0x060008FC RID: 2300 RVA: 0x00002285 File Offset: 0x00000485
		internal override bool LimitsParallelism
		{
			get
			{
				return false;
			}
		}
	}
}
