﻿using System;

namespace System.Linq.Parallel
{
	// Token: 0x020001E3 RID: 483
	internal abstract class SortHelper<TInputOutput>
	{
		// Token: 0x06000BB7 RID: 2999
		internal abstract TInputOutput[] Sort();

		// Token: 0x06000BB8 RID: 3000 RVA: 0x00002310 File Offset: 0x00000510
		protected SortHelper()
		{
		}
	}
}
