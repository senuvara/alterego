﻿using System;

namespace System.Linq.Parallel
{
	// Token: 0x020001AE RID: 430
	internal class SortQueryOperatorEnumerator<TInputOutput, TKey, TSortKey> : QueryOperatorEnumerator<TInputOutput, TSortKey>
	{
		// Token: 0x06000AD4 RID: 2772 RVA: 0x0002261B File Offset: 0x0002081B
		internal SortQueryOperatorEnumerator(QueryOperatorEnumerator<TInputOutput, TKey> source, Func<TInputOutput, TSortKey> keySelector)
		{
			this._source = source;
			this._keySelector = keySelector;
		}

		// Token: 0x06000AD5 RID: 2773 RVA: 0x00022634 File Offset: 0x00020834
		internal override bool MoveNext(ref TInputOutput currentElement, ref TSortKey currentKey)
		{
			TKey tkey = default(TKey);
			if (!this._source.MoveNext(ref currentElement, ref tkey))
			{
				return false;
			}
			currentKey = this._keySelector(currentElement);
			return true;
		}

		// Token: 0x06000AD6 RID: 2774 RVA: 0x00022673 File Offset: 0x00020873
		protected override void Dispose(bool disposing)
		{
			this._source.Dispose();
		}

		// Token: 0x0400074C RID: 1868
		private readonly QueryOperatorEnumerator<TInputOutput, TKey> _source;

		// Token: 0x0400074D RID: 1869
		private readonly Func<TInputOutput, TSortKey> _keySelector;
	}
}
