﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x020001D2 RID: 466
	internal static class ExchangeUtilities
	{
		// Token: 0x06000B63 RID: 2915 RVA: 0x000247F0 File Offset: 0x000229F0
		internal static PartitionedStream<T, int> PartitionDataSource<T>(IEnumerable<T> source, int partitionCount, bool useStriping)
		{
			IParallelPartitionable<T> parallelPartitionable = source as IParallelPartitionable<T>;
			PartitionedStream<T, int> result;
			if (parallelPartitionable != null)
			{
				QueryOperatorEnumerator<T, int>[] partitions = parallelPartitionable.GetPartitions(partitionCount);
				if (partitions == null)
				{
					throw new InvalidOperationException("The return value must not be null.");
				}
				if (partitions.Length != partitionCount)
				{
					throw new InvalidOperationException("The returned array's length must equal the number of partitions requested.");
				}
				PartitionedStream<T, int> partitionedStream = new PartitionedStream<T, int>(partitionCount, Util.GetDefaultComparer<int>(), OrdinalIndexState.Correct);
				for (int i = 0; i < partitionCount; i++)
				{
					QueryOperatorEnumerator<T, int> queryOperatorEnumerator = partitions[i];
					if (queryOperatorEnumerator == null)
					{
						throw new InvalidOperationException("Elements returned must not be null.");
					}
					partitionedStream[i] = queryOperatorEnumerator;
				}
				result = partitionedStream;
			}
			else
			{
				result = new PartitionedDataSource<T>(source, partitionCount, useStriping);
			}
			return result;
		}

		// Token: 0x06000B64 RID: 2916 RVA: 0x00024878 File Offset: 0x00022A78
		internal static PartitionedStream<Pair<TElement, THashKey>, int> HashRepartition<TElement, THashKey, TIgnoreKey>(PartitionedStream<TElement, TIgnoreKey> source, Func<TElement, THashKey> keySelector, IEqualityComparer<THashKey> keyComparer, IEqualityComparer<TElement> elementComparer, CancellationToken cancellationToken)
		{
			return new UnorderedHashRepartitionStream<TElement, THashKey, TIgnoreKey>(source, keySelector, keyComparer, elementComparer, cancellationToken);
		}

		// Token: 0x06000B65 RID: 2917 RVA: 0x00024885 File Offset: 0x00022A85
		internal static PartitionedStream<Pair<TElement, THashKey>, TOrderKey> HashRepartitionOrdered<TElement, THashKey, TOrderKey>(PartitionedStream<TElement, TOrderKey> source, Func<TElement, THashKey> keySelector, IEqualityComparer<THashKey> keyComparer, IEqualityComparer<TElement> elementComparer, CancellationToken cancellationToken)
		{
			return new OrderedHashRepartitionStream<TElement, THashKey, TOrderKey>(source, keySelector, keyComparer, elementComparer, cancellationToken);
		}

		// Token: 0x06000B66 RID: 2918 RVA: 0x00024892 File Offset: 0x00022A92
		internal static OrdinalIndexState Worse(this OrdinalIndexState state1, OrdinalIndexState state2)
		{
			if (state1 <= state2)
			{
				return state2;
			}
			return state1;
		}

		// Token: 0x06000B67 RID: 2919 RVA: 0x0002489B File Offset: 0x00022A9B
		internal static bool IsWorseThan(this OrdinalIndexState state1, OrdinalIndexState state2)
		{
			return state1 > state2;
		}
	}
}
