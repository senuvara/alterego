﻿using System;

namespace System.Linq.Parallel
{
	// Token: 0x020000DF RID: 223
	[Flags]
	internal enum QueryAggregationOptions
	{
		// Token: 0x04000526 RID: 1318
		None = 0,
		// Token: 0x04000527 RID: 1319
		Associative = 1,
		// Token: 0x04000528 RID: 1320
		Commutative = 2,
		// Token: 0x04000529 RID: 1321
		AssociativeCommutative = 3
	}
}
