﻿using System;
using System.Diagnostics.Tracing;
using System.Threading;
using System.Threading.Tasks;

namespace System.Linq.Parallel
{
	// Token: 0x020001DD RID: 477
	[EventSource(Name = "System.Linq.Parallel.PlinqEventSource", Guid = "159eeeec-4a14-4418-a8fe-faabcd987887")]
	internal sealed class PlinqEtwProvider : EventSource
	{
		// Token: 0x06000BA1 RID: 2977 RVA: 0x00025354 File Offset: 0x00023554
		private PlinqEtwProvider()
		{
		}

		// Token: 0x06000BA2 RID: 2978 RVA: 0x0002535C File Offset: 0x0002355C
		[NonEvent]
		internal static int NextQueryId()
		{
			return Interlocked.Increment(ref PlinqEtwProvider.s_queryId);
		}

		// Token: 0x06000BA3 RID: 2979 RVA: 0x00025368 File Offset: 0x00023568
		[NonEvent]
		internal void ParallelQueryBegin(int queryId)
		{
			if (base.IsEnabled(EventLevel.Informational, EventKeywords.All))
			{
				int taskId = Task.CurrentId ?? 0;
				this.ParallelQueryBegin(PlinqEtwProvider.s_defaultSchedulerId, taskId, queryId);
			}
		}

		// Token: 0x06000BA4 RID: 2980 RVA: 0x000253A7 File Offset: 0x000235A7
		[Event(1, Level = EventLevel.Informational, Task = (EventTask)1, Opcode = EventOpcode.Start)]
		private void ParallelQueryBegin(int taskSchedulerId, int taskId, int queryId)
		{
			base.WriteEvent(1, taskSchedulerId, taskId, queryId);
		}

		// Token: 0x06000BA5 RID: 2981 RVA: 0x000253B4 File Offset: 0x000235B4
		[NonEvent]
		internal void ParallelQueryEnd(int queryId)
		{
			if (base.IsEnabled(EventLevel.Informational, EventKeywords.All))
			{
				int taskId = Task.CurrentId ?? 0;
				this.ParallelQueryEnd(PlinqEtwProvider.s_defaultSchedulerId, taskId, queryId);
			}
		}

		// Token: 0x06000BA6 RID: 2982 RVA: 0x000253F3 File Offset: 0x000235F3
		[Event(2, Level = EventLevel.Informational, Task = (EventTask)1, Opcode = EventOpcode.Stop)]
		private void ParallelQueryEnd(int taskSchedulerId, int taskId, int queryId)
		{
			base.WriteEvent(2, taskSchedulerId, taskId, queryId);
		}

		// Token: 0x06000BA7 RID: 2983 RVA: 0x00025400 File Offset: 0x00023600
		[NonEvent]
		internal void ParallelQueryFork(int queryId)
		{
			if (base.IsEnabled(EventLevel.Verbose, EventKeywords.All))
			{
				int taskId = Task.CurrentId ?? 0;
				this.ParallelQueryFork(PlinqEtwProvider.s_defaultSchedulerId, taskId, queryId);
			}
		}

		// Token: 0x06000BA8 RID: 2984 RVA: 0x0002543F File Offset: 0x0002363F
		[Event(3, Level = EventLevel.Verbose, Task = (EventTask)2, Opcode = EventOpcode.Start)]
		private void ParallelQueryFork(int taskSchedulerId, int taskId, int queryId)
		{
			base.WriteEvent(3, taskSchedulerId, taskId, queryId);
		}

		// Token: 0x06000BA9 RID: 2985 RVA: 0x0002544C File Offset: 0x0002364C
		[NonEvent]
		internal void ParallelQueryJoin(int queryId)
		{
			if (base.IsEnabled(EventLevel.Verbose, EventKeywords.All))
			{
				int taskId = Task.CurrentId ?? 0;
				this.ParallelQueryJoin(PlinqEtwProvider.s_defaultSchedulerId, taskId, queryId);
			}
		}

		// Token: 0x06000BAA RID: 2986 RVA: 0x0002548B File Offset: 0x0002368B
		[Event(4, Level = EventLevel.Verbose, Task = (EventTask)2, Opcode = EventOpcode.Stop)]
		private void ParallelQueryJoin(int taskSchedulerId, int taskId, int queryId)
		{
			base.WriteEvent(4, taskSchedulerId, taskId, queryId);
		}

		// Token: 0x06000BAB RID: 2987 RVA: 0x00025497 File Offset: 0x00023697
		// Note: this type is marked as 'beforefieldinit'.
		static PlinqEtwProvider()
		{
		}

		// Token: 0x040007FA RID: 2042
		internal static PlinqEtwProvider Log = new PlinqEtwProvider();

		// Token: 0x040007FB RID: 2043
		private static readonly int s_defaultSchedulerId = TaskScheduler.Default.Id;

		// Token: 0x040007FC RID: 2044
		private static int s_queryId = 0;

		// Token: 0x040007FD RID: 2045
		private const EventKeywords ALL_KEYWORDS = EventKeywords.All;

		// Token: 0x040007FE RID: 2046
		private const int PARALLELQUERYBEGIN_EVENTID = 1;

		// Token: 0x040007FF RID: 2047
		private const int PARALLELQUERYEND_EVENTID = 2;

		// Token: 0x04000800 RID: 2048
		private const int PARALLELQUERYFORK_EVENTID = 3;

		// Token: 0x04000801 RID: 2049
		private const int PARALLELQUERYJOIN_EVENTID = 4;

		// Token: 0x020001DE RID: 478
		public class Tasks
		{
			// Token: 0x06000BAC RID: 2988 RVA: 0x00002310 File Offset: 0x00000510
			public Tasks()
			{
			}

			// Token: 0x04000802 RID: 2050
			public const EventTask Query = (EventTask)1;

			// Token: 0x04000803 RID: 2051
			public const EventTask ForkJoin = (EventTask)2;
		}
	}
}
