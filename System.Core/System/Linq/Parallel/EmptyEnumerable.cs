﻿using System;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x020000D8 RID: 216
	internal class EmptyEnumerable<T> : ParallelQuery<T>
	{
		// Token: 0x060007B6 RID: 1974 RVA: 0x00017180 File Offset: 0x00015380
		private EmptyEnumerable() : base(QuerySettings.Empty)
		{
		}

		// Token: 0x1700011C RID: 284
		// (get) Token: 0x060007B7 RID: 1975 RVA: 0x0001718D File Offset: 0x0001538D
		internal static EmptyEnumerable<T> Instance
		{
			get
			{
				if (EmptyEnumerable<T>.s_instance == null)
				{
					EmptyEnumerable<T>.s_instance = new EmptyEnumerable<T>();
				}
				return EmptyEnumerable<T>.s_instance;
			}
		}

		// Token: 0x060007B8 RID: 1976 RVA: 0x000171AB File Offset: 0x000153AB
		public override IEnumerator<T> GetEnumerator()
		{
			if (EmptyEnumerable<T>.s_enumeratorInstance == null)
			{
				EmptyEnumerable<T>.s_enumeratorInstance = new EmptyEnumerator<T>();
			}
			return EmptyEnumerable<T>.s_enumeratorInstance;
		}

		// Token: 0x0400051F RID: 1311
		private static volatile EmptyEnumerable<T> s_instance;

		// Token: 0x04000520 RID: 1312
		private static volatile EmptyEnumerator<T> s_enumeratorInstance;
	}
}
