﻿using System;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x020001C9 RID: 457
	internal class PipelineSpoolingTask<TInputOutput, TIgnoreKey> : SpoolingTaskBase
	{
		// Token: 0x06000B36 RID: 2870 RVA: 0x0002403F File Offset: 0x0002223F
		internal PipelineSpoolingTask(int taskIndex, QueryTaskGroupState groupState, QueryOperatorEnumerator<TInputOutput, TIgnoreKey> source, AsynchronousChannel<TInputOutput> destination) : base(taskIndex, groupState)
		{
			this._source = source;
			this._destination = destination;
		}

		// Token: 0x06000B37 RID: 2871 RVA: 0x00024058 File Offset: 0x00022258
		protected override void SpoolingWork()
		{
			TInputOutput item = default(TInputOutput);
			TIgnoreKey tignoreKey = default(TIgnoreKey);
			QueryOperatorEnumerator<TInputOutput, TIgnoreKey> source = this._source;
			AsynchronousChannel<TInputOutput> destination = this._destination;
			CancellationToken mergedCancellationToken = this._groupState.CancellationState.MergedCancellationToken;
			while (source.MoveNext(ref item, ref tignoreKey) && !mergedCancellationToken.IsCancellationRequested)
			{
				destination.Enqueue(item);
			}
			destination.FlushBuffers();
		}

		// Token: 0x06000B38 RID: 2872 RVA: 0x000240B9 File Offset: 0x000222B9
		protected override void SpoolingFinally()
		{
			base.SpoolingFinally();
			if (this._destination != null)
			{
				this._destination.SetDone();
			}
			this._source.Dispose();
		}

		// Token: 0x040007BA RID: 1978
		private QueryOperatorEnumerator<TInputOutput, TIgnoreKey> _source;

		// Token: 0x040007BB RID: 1979
		private AsynchronousChannel<TInputOutput> _destination;
	}
}
