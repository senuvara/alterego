﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x0200018C RID: 396
	internal abstract class OrderedGroupByQueryOperatorEnumerator<TSource, TGroupKey, TElement, TOrderKey> : QueryOperatorEnumerator<IGrouping<TGroupKey, TElement>, TOrderKey>
	{
		// Token: 0x06000A58 RID: 2648 RVA: 0x00020D7F File Offset: 0x0001EF7F
		protected OrderedGroupByQueryOperatorEnumerator(QueryOperatorEnumerator<Pair<TSource, TGroupKey>, TOrderKey> source, Func<TSource, TGroupKey> keySelector, IEqualityComparer<TGroupKey> keyComparer, IComparer<TOrderKey> orderComparer, CancellationToken cancellationToken)
		{
			this._source = source;
			this._keySelector = keySelector;
			this._keyComparer = keyComparer;
			this._orderComparer = orderComparer;
			this._cancellationToken = cancellationToken;
		}

		// Token: 0x06000A59 RID: 2649 RVA: 0x00020DAC File Offset: 0x0001EFAC
		internal override bool MoveNext(ref IGrouping<TGroupKey, TElement> currentElement, ref TOrderKey currentKey)
		{
			OrderedGroupByQueryOperatorEnumerator<TSource, TGroupKey, TElement, TOrderKey>.Mutables mutables = this._mutables;
			if (mutables == null)
			{
				mutables = (this._mutables = new OrderedGroupByQueryOperatorEnumerator<TSource, TGroupKey, TElement, TOrderKey>.Mutables());
				mutables._hashLookup = this.BuildHashLookup();
				mutables._hashLookupIndex = -1;
			}
			OrderedGroupByQueryOperatorEnumerator<TSource, TGroupKey, TElement, TOrderKey>.Mutables mutables2 = mutables;
			int num = mutables2._hashLookupIndex + 1;
			mutables2._hashLookupIndex = num;
			if (num < mutables._hashLookup.Count)
			{
				OrderedGroupByQueryOperatorEnumerator<TSource, TGroupKey, TElement, TOrderKey>.GroupKeyData value = mutables._hashLookup[mutables._hashLookupIndex].Value;
				currentElement = value._grouping;
				currentKey = value._orderKey;
				return true;
			}
			return false;
		}

		// Token: 0x06000A5A RID: 2650
		protected abstract HashLookup<Wrapper<TGroupKey>, OrderedGroupByQueryOperatorEnumerator<TSource, TGroupKey, TElement, TOrderKey>.GroupKeyData> BuildHashLookup();

		// Token: 0x06000A5B RID: 2651 RVA: 0x00020E35 File Offset: 0x0001F035
		protected override void Dispose(bool disposing)
		{
			this._source.Dispose();
		}

		// Token: 0x040006E7 RID: 1767
		protected readonly QueryOperatorEnumerator<Pair<TSource, TGroupKey>, TOrderKey> _source;

		// Token: 0x040006E8 RID: 1768
		private readonly Func<TSource, TGroupKey> _keySelector;

		// Token: 0x040006E9 RID: 1769
		protected readonly IEqualityComparer<TGroupKey> _keyComparer;

		// Token: 0x040006EA RID: 1770
		protected readonly IComparer<TOrderKey> _orderComparer;

		// Token: 0x040006EB RID: 1771
		protected readonly CancellationToken _cancellationToken;

		// Token: 0x040006EC RID: 1772
		private OrderedGroupByQueryOperatorEnumerator<TSource, TGroupKey, TElement, TOrderKey>.Mutables _mutables;

		// Token: 0x0200018D RID: 397
		private class Mutables
		{
			// Token: 0x06000A5C RID: 2652 RVA: 0x00002310 File Offset: 0x00000510
			public Mutables()
			{
			}

			// Token: 0x040006ED RID: 1773
			internal HashLookup<Wrapper<TGroupKey>, OrderedGroupByQueryOperatorEnumerator<TSource, TGroupKey, TElement, TOrderKey>.GroupKeyData> _hashLookup;

			// Token: 0x040006EE RID: 1774
			internal int _hashLookupIndex;
		}

		// Token: 0x0200018E RID: 398
		protected class GroupKeyData
		{
			// Token: 0x06000A5D RID: 2653 RVA: 0x00020E42 File Offset: 0x0001F042
			internal GroupKeyData(TOrderKey orderKey, TGroupKey hashKey, IComparer<TOrderKey> orderComparer)
			{
				this._orderKey = orderKey;
				this._grouping = new OrderedGroupByGrouping<TGroupKey, TOrderKey, TElement>(hashKey, orderComparer);
			}

			// Token: 0x040006EF RID: 1775
			internal TOrderKey _orderKey;

			// Token: 0x040006F0 RID: 1776
			internal OrderedGroupByGrouping<TGroupKey, TOrderKey, TElement> _grouping;
		}
	}
}
