﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000107 RID: 263
	internal sealed class AssociativeAggregationOperator<TInput, TIntermediate, TOutput> : UnaryQueryOperator<TInput, TIntermediate>
	{
		// Token: 0x06000844 RID: 2116 RVA: 0x00019746 File Offset: 0x00017946
		internal AssociativeAggregationOperator(IEnumerable<TInput> child, TIntermediate seed, Func<TIntermediate> seedFactory, bool seedIsSpecified, Func<TIntermediate, TInput, TIntermediate> intermediateReduce, Func<TIntermediate, TIntermediate, TIntermediate> finalReduce, Func<TIntermediate, TOutput> resultSelector, bool throwIfEmpty, QueryAggregationOptions options) : base(child)
		{
			this._seed = seed;
			this._seedFactory = seedFactory;
			this._seedIsSpecified = seedIsSpecified;
			this._intermediateReduce = intermediateReduce;
			this._finalReduce = finalReduce;
			this._resultSelector = resultSelector;
			this._throwIfEmpty = throwIfEmpty;
		}

		// Token: 0x06000845 RID: 2117 RVA: 0x00019788 File Offset: 0x00017988
		internal TOutput Aggregate()
		{
			TIntermediate tintermediate = default(TIntermediate);
			bool flag = false;
			using (IEnumerator<TIntermediate> enumerator = this.GetEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered), true))
			{
				while (enumerator.MoveNext())
				{
					if (flag)
					{
						try
						{
							tintermediate = this._finalReduce(tintermediate, enumerator.Current);
							continue;
						}
						catch (Exception ex)
						{
							throw new AggregateException(new Exception[]
							{
								ex
							});
						}
					}
					tintermediate = enumerator.Current;
					flag = true;
				}
				if (!flag)
				{
					if (this._throwIfEmpty)
					{
						throw new InvalidOperationException("Sequence contains no elements");
					}
					tintermediate = ((this._seedFactory == null) ? this._seed : this._seedFactory());
				}
			}
			TOutput result;
			try
			{
				result = this._resultSelector(tintermediate);
			}
			catch (Exception ex2)
			{
				throw new AggregateException(new Exception[]
				{
					ex2
				});
			}
			return result;
		}

		// Token: 0x06000846 RID: 2118 RVA: 0x00019878 File Offset: 0x00017A78
		internal override QueryResults<TIntermediate> Open(QuerySettings settings, bool preferStriping)
		{
			return new UnaryQueryOperator<TInput, TIntermediate>.UnaryQueryOperatorResults(base.Child.Open(settings, preferStriping), this, settings, preferStriping);
		}

		// Token: 0x06000847 RID: 2119 RVA: 0x00019890 File Offset: 0x00017A90
		internal override void WrapPartitionedStream<TKey>(PartitionedStream<TInput, TKey> inputStream, IPartitionedStreamRecipient<TIntermediate> recipient, bool preferStriping, QuerySettings settings)
		{
			int partitionCount = inputStream.PartitionCount;
			PartitionedStream<TIntermediate, int> partitionedStream = new PartitionedStream<TIntermediate, int>(partitionCount, Util.GetDefaultComparer<int>(), OrdinalIndexState.Correct);
			for (int i = 0; i < partitionCount; i++)
			{
				partitionedStream[i] = new AssociativeAggregationOperator<TInput, TIntermediate, TOutput>.AssociativeAggregationOperatorEnumerator<TKey>(inputStream[i], this, i, settings.CancellationState.MergedCancellationToken);
			}
			recipient.Receive<int>(partitionedStream);
		}

		// Token: 0x06000848 RID: 2120 RVA: 0x00003A6B File Offset: 0x00001C6B
		[ExcludeFromCodeCoverage]
		internal override IEnumerable<TIntermediate> AsSequentialQuery(CancellationToken token)
		{
			throw new NotSupportedException();
		}

		// Token: 0x1700012B RID: 299
		// (get) Token: 0x06000849 RID: 2121 RVA: 0x00002285 File Offset: 0x00000485
		internal override bool LimitsParallelism
		{
			get
			{
				return false;
			}
		}

		// Token: 0x040005BB RID: 1467
		private readonly TIntermediate _seed;

		// Token: 0x040005BC RID: 1468
		private readonly bool _seedIsSpecified;

		// Token: 0x040005BD RID: 1469
		private readonly bool _throwIfEmpty;

		// Token: 0x040005BE RID: 1470
		private Func<TIntermediate, TInput, TIntermediate> _intermediateReduce;

		// Token: 0x040005BF RID: 1471
		private Func<TIntermediate, TIntermediate, TIntermediate> _finalReduce;

		// Token: 0x040005C0 RID: 1472
		private Func<TIntermediate, TOutput> _resultSelector;

		// Token: 0x040005C1 RID: 1473
		private Func<TIntermediate> _seedFactory;

		// Token: 0x02000108 RID: 264
		private class AssociativeAggregationOperatorEnumerator<TKey> : QueryOperatorEnumerator<TIntermediate, int>
		{
			// Token: 0x0600084A RID: 2122 RVA: 0x000198E5 File Offset: 0x00017AE5
			internal AssociativeAggregationOperatorEnumerator(QueryOperatorEnumerator<TInput, TKey> source, AssociativeAggregationOperator<TInput, TIntermediate, TOutput> reduceOperator, int partitionIndex, CancellationToken cancellationToken)
			{
				this._source = source;
				this._reduceOperator = reduceOperator;
				this._partitionIndex = partitionIndex;
				this._cancellationToken = cancellationToken;
			}

			// Token: 0x0600084B RID: 2123 RVA: 0x0001990C File Offset: 0x00017B0C
			internal override bool MoveNext(ref TIntermediate currentElement, ref int currentKey)
			{
				if (this._accumulated)
				{
					return false;
				}
				this._accumulated = true;
				bool flag = false;
				TIntermediate tintermediate = default(TIntermediate);
				if (this._reduceOperator._seedIsSpecified)
				{
					tintermediate = ((this._reduceOperator._seedFactory == null) ? this._reduceOperator._seed : this._reduceOperator._seedFactory());
				}
				else
				{
					TInput tinput = default(TInput);
					TKey tkey = default(TKey);
					if (!this._source.MoveNext(ref tinput, ref tkey))
					{
						return false;
					}
					flag = true;
					tintermediate = (TIntermediate)((object)tinput);
				}
				TInput arg = default(TInput);
				TKey tkey2 = default(TKey);
				int num = 0;
				while (this._source.MoveNext(ref arg, ref tkey2))
				{
					if ((num++ & 63) == 0)
					{
						CancellationState.ThrowIfCanceled(this._cancellationToken);
					}
					flag = true;
					tintermediate = this._reduceOperator._intermediateReduce(tintermediate, arg);
				}
				if (flag)
				{
					currentElement = tintermediate;
					currentKey = this._partitionIndex;
					return true;
				}
				return false;
			}

			// Token: 0x0600084C RID: 2124 RVA: 0x00019A07 File Offset: 0x00017C07
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x040005C2 RID: 1474
			private readonly QueryOperatorEnumerator<TInput, TKey> _source;

			// Token: 0x040005C3 RID: 1475
			private readonly AssociativeAggregationOperator<TInput, TIntermediate, TOutput> _reduceOperator;

			// Token: 0x040005C4 RID: 1476
			private readonly int _partitionIndex;

			// Token: 0x040005C5 RID: 1477
			private readonly CancellationToken _cancellationToken;

			// Token: 0x040005C6 RID: 1478
			private bool _accumulated;
		}
	}
}
