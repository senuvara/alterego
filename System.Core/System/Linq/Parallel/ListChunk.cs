﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace System.Linq.Parallel
{
	// Token: 0x020001D9 RID: 473
	internal class ListChunk<TInputOutput> : IEnumerable<TInputOutput>, IEnumerable
	{
		// Token: 0x06000B87 RID: 2951 RVA: 0x00024F66 File Offset: 0x00023166
		internal ListChunk(int size)
		{
			this._chunk = new TInputOutput[size];
			this._chunkCount = 0;
			this._tailChunk = this;
		}

		// Token: 0x06000B88 RID: 2952 RVA: 0x00024F88 File Offset: 0x00023188
		internal void Add(TInputOutput e)
		{
			ListChunk<TInputOutput> listChunk = this._tailChunk;
			if (listChunk._chunkCount == listChunk._chunk.Length)
			{
				this._tailChunk = new ListChunk<TInputOutput>(listChunk._chunkCount * 2);
				listChunk = (listChunk._nextChunk = this._tailChunk);
			}
			TInputOutput[] chunk = listChunk._chunk;
			ListChunk<TInputOutput> listChunk2 = listChunk;
			int chunkCount = listChunk2._chunkCount;
			listChunk2._chunkCount = chunkCount + 1;
			chunk[chunkCount] = e;
		}

		// Token: 0x17000196 RID: 406
		// (get) Token: 0x06000B89 RID: 2953 RVA: 0x00024FEC File Offset: 0x000231EC
		internal ListChunk<TInputOutput> Next
		{
			get
			{
				return this._nextChunk;
			}
		}

		// Token: 0x17000197 RID: 407
		// (get) Token: 0x06000B8A RID: 2954 RVA: 0x00024FF4 File Offset: 0x000231F4
		internal int Count
		{
			get
			{
				return this._chunkCount;
			}
		}

		// Token: 0x06000B8B RID: 2955 RVA: 0x00024FFC File Offset: 0x000231FC
		public IEnumerator<TInputOutput> GetEnumerator()
		{
			for (ListChunk<TInputOutput> curr = this; curr != null; curr = curr._nextChunk)
			{
				int num;
				for (int i = 0; i < curr._chunkCount; i = num + 1)
				{
					yield return curr._chunk[i];
					num = i;
				}
			}
			yield break;
		}

		// Token: 0x06000B8C RID: 2956 RVA: 0x00007A46 File Offset: 0x00005C46
		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable<!0>)this).GetEnumerator();
		}

		// Token: 0x040007EA RID: 2026
		internal TInputOutput[] _chunk;

		// Token: 0x040007EB RID: 2027
		private int _chunkCount;

		// Token: 0x040007EC RID: 2028
		private ListChunk<TInputOutput> _nextChunk;

		// Token: 0x040007ED RID: 2029
		private ListChunk<TInputOutput> _tailChunk;

		// Token: 0x020001DA RID: 474
		[CompilerGenerated]
		private sealed class <GetEnumerator>d__10 : IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x06000B8D RID: 2957 RVA: 0x0002500B File Offset: 0x0002320B
			[DebuggerHidden]
			public <GetEnumerator>d__10(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x06000B8E RID: 2958 RVA: 0x000039E8 File Offset: 0x00001BE8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06000B8F RID: 2959 RVA: 0x0002501C File Offset: 0x0002321C
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				ListChunk<TInputOutput> listChunk = this;
				if (num == 0)
				{
					this.<>1__state = -1;
					curr = listChunk;
					goto IL_90;
				}
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				int num2 = i;
				i = num2 + 1;
				IL_6C:
				if (i < curr._chunkCount)
				{
					this.<>2__current = curr._chunk[i];
					this.<>1__state = 1;
					return true;
				}
				curr = curr._nextChunk;
				IL_90:
				if (curr == null)
				{
					return false;
				}
				i = 0;
				goto IL_6C;
			}

			// Token: 0x17000198 RID: 408
			// (get) Token: 0x06000B90 RID: 2960 RVA: 0x000250C2 File Offset: 0x000232C2
			TInputOutput IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000B91 RID: 2961 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000199 RID: 409
			// (get) Token: 0x06000B92 RID: 2962 RVA: 0x000250CA File Offset: 0x000232CA
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x040007EE RID: 2030
			private int <>1__state;

			// Token: 0x040007EF RID: 2031
			private TInputOutput <>2__current;

			// Token: 0x040007F0 RID: 2032
			public ListChunk<TInputOutput> <>4__this;

			// Token: 0x040007F1 RID: 2033
			private ListChunk<TInputOutput> <curr>5__1;

			// Token: 0x040007F2 RID: 2034
			private int <i>5__2;
		}
	}
}
