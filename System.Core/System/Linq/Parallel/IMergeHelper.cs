﻿using System;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x020000EB RID: 235
	internal interface IMergeHelper<TInputOutput>
	{
		// Token: 0x060007F3 RID: 2035
		void Execute();

		// Token: 0x060007F4 RID: 2036
		IEnumerator<TInputOutput> GetEnumerator();

		// Token: 0x060007F5 RID: 2037
		TInputOutput[] GetResultsAsArray();
	}
}
