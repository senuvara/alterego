﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace System.Linq.Parallel
{
	// Token: 0x02000172 RID: 370
	internal abstract class QueryResults<T> : IList<T>, ICollection<T>, IEnumerable<T>, IEnumerable
	{
		// Token: 0x060009D2 RID: 2514
		internal abstract void GivePartitionedStream(IPartitionedStreamRecipient<T> recipient);

		// Token: 0x17000151 RID: 337
		// (get) Token: 0x060009D3 RID: 2515 RVA: 0x00002285 File Offset: 0x00000485
		internal virtual bool IsIndexible
		{
			get
			{
				return false;
			}
		}

		// Token: 0x060009D4 RID: 2516 RVA: 0x00003A6B File Offset: 0x00001C6B
		internal virtual T GetElement(int index)
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000152 RID: 338
		// (get) Token: 0x060009D5 RID: 2517 RVA: 0x00003A6B File Offset: 0x00001C6B
		internal virtual int ElementsCount
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x060009D6 RID: 2518 RVA: 0x00003A6B File Offset: 0x00001C6B
		int IList<!0>.IndexOf(T item)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060009D7 RID: 2519 RVA: 0x00003A6B File Offset: 0x00001C6B
		void IList<!0>.Insert(int index, T item)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060009D8 RID: 2520 RVA: 0x00003A6B File Offset: 0x00001C6B
		void IList<!0>.RemoveAt(int index)
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000153 RID: 339
		public T this[int index]
		{
			get
			{
				return this.GetElement(index);
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x060009DB RID: 2523 RVA: 0x00003A6B File Offset: 0x00001C6B
		void ICollection<!0>.Add(T item)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060009DC RID: 2524 RVA: 0x00003A6B File Offset: 0x00001C6B
		void ICollection<!0>.Clear()
		{
			throw new NotSupportedException();
		}

		// Token: 0x060009DD RID: 2525 RVA: 0x00003A6B File Offset: 0x00001C6B
		bool ICollection<!0>.Contains(T item)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060009DE RID: 2526 RVA: 0x00003A6B File Offset: 0x00001C6B
		void ICollection<!0>.CopyTo(T[] array, int arrayIndex)
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000154 RID: 340
		// (get) Token: 0x060009DF RID: 2527 RVA: 0x0001F4A7 File Offset: 0x0001D6A7
		public int Count
		{
			get
			{
				return this.ElementsCount;
			}
		}

		// Token: 0x17000155 RID: 341
		// (get) Token: 0x060009E0 RID: 2528 RVA: 0x00009CDF File Offset: 0x00007EDF
		bool ICollection<!0>.IsReadOnly
		{
			get
			{
				return true;
			}
		}

		// Token: 0x060009E1 RID: 2529 RVA: 0x00003A6B File Offset: 0x00001C6B
		bool ICollection<!0>.Remove(T item)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060009E2 RID: 2530 RVA: 0x0001F4AF File Offset: 0x0001D6AF
		IEnumerator<T> IEnumerable<!0>.GetEnumerator()
		{
			int num;
			for (int index = 0; index < this.Count; index = num + 1)
			{
				yield return this[index];
				num = index;
			}
			yield break;
		}

		// Token: 0x060009E3 RID: 2531 RVA: 0x00007A46 File Offset: 0x00005C46
		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable<!0>)this).GetEnumerator();
		}

		// Token: 0x060009E4 RID: 2532 RVA: 0x00002310 File Offset: 0x00000510
		protected QueryResults()
		{
		}

		// Token: 0x02000173 RID: 371
		[CompilerGenerated]
		private sealed class <System-Collections-Generic-IEnumerable<T>-GetEnumerator>d__21 : IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x060009E5 RID: 2533 RVA: 0x0001F4BE File Offset: 0x0001D6BE
			[DebuggerHidden]
			public <System-Collections-Generic-IEnumerable<T>-GetEnumerator>d__21(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x060009E6 RID: 2534 RVA: 0x000039E8 File Offset: 0x00001BE8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x060009E7 RID: 2535 RVA: 0x0001F4D0 File Offset: 0x0001D6D0
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				QueryResults<T> queryResults = this;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
					int num2 = index;
					index = num2 + 1;
				}
				else
				{
					this.<>1__state = -1;
					index = 0;
				}
				if (index >= queryResults.Count)
				{
					return false;
				}
				this.<>2__current = queryResults[index];
				this.<>1__state = 1;
				return true;
			}

			// Token: 0x17000156 RID: 342
			// (get) Token: 0x060009E8 RID: 2536 RVA: 0x0001F545 File Offset: 0x0001D745
			T IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060009E9 RID: 2537 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000157 RID: 343
			// (get) Token: 0x060009EA RID: 2538 RVA: 0x0001F54D File Offset: 0x0001D74D
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x04000696 RID: 1686
			private int <>1__state;

			// Token: 0x04000697 RID: 1687
			private T <>2__current;

			// Token: 0x04000698 RID: 1688
			public QueryResults<T> <>4__this;

			// Token: 0x04000699 RID: 1689
			private int <index>5__1;
		}
	}
}
