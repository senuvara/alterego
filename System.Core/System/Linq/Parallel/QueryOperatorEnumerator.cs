﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x02000170 RID: 368
	internal abstract class QueryOperatorEnumerator<TElement, TKey>
	{
		// Token: 0x060009C6 RID: 2502
		internal abstract bool MoveNext(ref TElement currentElement, ref TKey currentKey);

		// Token: 0x060009C7 RID: 2503 RVA: 0x0001F41E File Offset: 0x0001D61E
		public void Dispose()
		{
			this.Dispose(true);
		}

		// Token: 0x060009C8 RID: 2504 RVA: 0x000039E8 File Offset: 0x00001BE8
		protected virtual void Dispose(bool disposing)
		{
		}

		// Token: 0x060009C9 RID: 2505 RVA: 0x000039E8 File Offset: 0x00001BE8
		internal virtual void Reset()
		{
		}

		// Token: 0x060009CA RID: 2506 RVA: 0x0001F427 File Offset: 0x0001D627
		internal IEnumerator<TElement> AsClassicEnumerator()
		{
			return new QueryOperatorEnumerator<TElement, TKey>.QueryOperatorClassicEnumerator(this);
		}

		// Token: 0x060009CB RID: 2507 RVA: 0x00002310 File Offset: 0x00000510
		protected QueryOperatorEnumerator()
		{
		}

		// Token: 0x02000171 RID: 369
		private class QueryOperatorClassicEnumerator : IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x060009CC RID: 2508 RVA: 0x0001F42F File Offset: 0x0001D62F
			internal QueryOperatorClassicEnumerator(QueryOperatorEnumerator<TElement, TKey> operatorEnumerator)
			{
				this._operatorEnumerator = operatorEnumerator;
			}

			// Token: 0x060009CD RID: 2509 RVA: 0x0001F440 File Offset: 0x0001D640
			public bool MoveNext()
			{
				TKey tkey = default(TKey);
				return this._operatorEnumerator.MoveNext(ref this._current, ref tkey);
			}

			// Token: 0x1700014F RID: 335
			// (get) Token: 0x060009CE RID: 2510 RVA: 0x0001F468 File Offset: 0x0001D668
			public TElement Current
			{
				get
				{
					return this._current;
				}
			}

			// Token: 0x17000150 RID: 336
			// (get) Token: 0x060009CF RID: 2511 RVA: 0x0001F470 File Offset: 0x0001D670
			object IEnumerator.Current
			{
				get
				{
					return this._current;
				}
			}

			// Token: 0x060009D0 RID: 2512 RVA: 0x0001F47D File Offset: 0x0001D67D
			public void Dispose()
			{
				this._operatorEnumerator.Dispose();
				this._operatorEnumerator = null;
			}

			// Token: 0x060009D1 RID: 2513 RVA: 0x0001F491 File Offset: 0x0001D691
			public void Reset()
			{
				this._operatorEnumerator.Reset();
			}

			// Token: 0x04000694 RID: 1684
			private QueryOperatorEnumerator<TElement, TKey> _operatorEnumerator;

			// Token: 0x04000695 RID: 1685
			private TElement _current;
		}
	}
}
