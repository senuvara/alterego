﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x0200013C RID: 316
	internal sealed class IntSumAggregationOperator : InlinedAggregationOperator<int, int, int>
	{
		// Token: 0x0600090C RID: 2316 RVA: 0x0001CB6D File Offset: 0x0001AD6D
		internal IntSumAggregationOperator(IEnumerable<int> child) : base(child)
		{
		}

		// Token: 0x0600090D RID: 2317 RVA: 0x0001CB78 File Offset: 0x0001AD78
		protected override int InternalAggregate(ref Exception singularExceptionToThrow)
		{
			checked
			{
				int result;
				using (IEnumerator<int> enumerator = this.GetEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered), true))
				{
					int num = 0;
					while (enumerator.MoveNext())
					{
						int num2 = enumerator.Current;
						num += num2;
					}
					result = num;
				}
				return result;
			}
		}

		// Token: 0x0600090E RID: 2318 RVA: 0x0001CBC8 File Offset: 0x0001ADC8
		protected override QueryOperatorEnumerator<int, int> CreateEnumerator<TKey>(int index, int count, QueryOperatorEnumerator<int, TKey> source, object sharedData, CancellationToken cancellationToken)
		{
			return new IntSumAggregationOperator.IntSumAggregationOperatorEnumerator<TKey>(source, index, cancellationToken);
		}

		// Token: 0x0200013D RID: 317
		private class IntSumAggregationOperatorEnumerator<TKey> : InlinedAggregationOperatorEnumerator<int>
		{
			// Token: 0x0600090F RID: 2319 RVA: 0x0001CBD3 File Offset: 0x0001ADD3
			internal IntSumAggregationOperatorEnumerator(QueryOperatorEnumerator<int, TKey> source, int partitionIndex, CancellationToken cancellationToken) : base(partitionIndex, cancellationToken)
			{
				this._source = source;
			}

			// Token: 0x06000910 RID: 2320 RVA: 0x0001CBE4 File Offset: 0x0001ADE4
			protected override bool MoveNextCore(ref int currentElement)
			{
				int num = 0;
				TKey tkey = default(TKey);
				QueryOperatorEnumerator<int, TKey> source = this._source;
				if (source.MoveNext(ref num, ref tkey))
				{
					int num2 = 0;
					int num3 = 0;
					do
					{
						if ((num3++ & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(this._cancellationToken);
						}
						checked
						{
							num2 += num;
						}
					}
					while (source.MoveNext(ref num, ref tkey));
					currentElement = num2;
					return true;
				}
				return false;
			}

			// Token: 0x06000911 RID: 2321 RVA: 0x0001CC40 File Offset: 0x0001AE40
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x0400064D RID: 1613
			private readonly QueryOperatorEnumerator<int, TKey> _source;
		}
	}
}
