﻿using System;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x02000105 RID: 261
	internal class PartitionedStream<TElement, TKey>
	{
		// Token: 0x0600083D RID: 2109 RVA: 0x00019672 File Offset: 0x00017872
		internal PartitionedStream(int partitionCount, IComparer<TKey> keyComparer, OrdinalIndexState indexState)
		{
			this._partitions = new QueryOperatorEnumerator<TElement, TKey>[partitionCount];
			this._keyComparer = keyComparer;
			this._indexState = indexState;
		}

		// Token: 0x17000127 RID: 295
		internal QueryOperatorEnumerator<TElement, TKey> this[int index]
		{
			get
			{
				return this._partitions[index];
			}
			set
			{
				this._partitions[index] = value;
			}
		}

		// Token: 0x17000128 RID: 296
		// (get) Token: 0x06000840 RID: 2112 RVA: 0x000196A9 File Offset: 0x000178A9
		public int PartitionCount
		{
			get
			{
				return this._partitions.Length;
			}
		}

		// Token: 0x17000129 RID: 297
		// (get) Token: 0x06000841 RID: 2113 RVA: 0x000196B3 File Offset: 0x000178B3
		internal IComparer<TKey> KeyComparer
		{
			get
			{
				return this._keyComparer;
			}
		}

		// Token: 0x1700012A RID: 298
		// (get) Token: 0x06000842 RID: 2114 RVA: 0x000196BB File Offset: 0x000178BB
		internal OrdinalIndexState OrdinalIndexState
		{
			get
			{
				return this._indexState;
			}
		}

		// Token: 0x040005B8 RID: 1464
		protected QueryOperatorEnumerator<TElement, TKey>[] _partitions;

		// Token: 0x040005B9 RID: 1465
		private readonly IComparer<TKey> _keyComparer;

		// Token: 0x040005BA RID: 1466
		private readonly OrdinalIndexState _indexState;
	}
}
