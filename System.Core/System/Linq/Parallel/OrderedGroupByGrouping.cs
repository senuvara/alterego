﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace System.Linq.Parallel
{
	// Token: 0x02000192 RID: 402
	internal class OrderedGroupByGrouping<TGroupKey, TOrderKey, TElement> : IGrouping<TGroupKey, TElement>, IEnumerable<!2>, IEnumerable
	{
		// Token: 0x06000A66 RID: 2662 RVA: 0x000210D7 File Offset: 0x0001F2D7
		internal OrderedGroupByGrouping(TGroupKey groupKey, IComparer<TOrderKey> orderComparer)
		{
			this._groupKey = groupKey;
			this._values = new GrowingArray<TElement>();
			this._orderKeys = new GrowingArray<TOrderKey>();
			this._orderComparer = orderComparer;
			this._wrappedComparer = new OrderedGroupByGrouping<TGroupKey, TOrderKey, TElement>.KeyAndValuesComparer(this._orderComparer);
		}

		// Token: 0x1700016B RID: 363
		// (get) Token: 0x06000A67 RID: 2663 RVA: 0x00021114 File Offset: 0x0001F314
		TGroupKey IGrouping<!0, !2>.Key
		{
			get
			{
				return this._groupKey;
			}
		}

		// Token: 0x06000A68 RID: 2664 RVA: 0x0002111C File Offset: 0x0001F31C
		IEnumerator<TElement> IEnumerable<!2>.GetEnumerator()
		{
			int valueCount = this._values.Count;
			TElement[] valueArray = this._values.InternalArray;
			int num;
			for (int i = 0; i < valueCount; i = num + 1)
			{
				yield return valueArray[i];
				num = i;
			}
			yield break;
		}

		// Token: 0x06000A69 RID: 2665 RVA: 0x0002112B File Offset: 0x0001F32B
		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable<!2>)this).GetEnumerator();
		}

		// Token: 0x06000A6A RID: 2666 RVA: 0x00021133 File Offset: 0x0001F333
		internal void Add(TElement value, TOrderKey orderKey)
		{
			this._values.Add(value);
			this._orderKeys.Add(orderKey);
		}

		// Token: 0x06000A6B RID: 2667 RVA: 0x00021150 File Offset: 0x0001F350
		internal void DoneAdding()
		{
			List<KeyValuePair<TOrderKey, TElement>> list = new List<KeyValuePair<TOrderKey, TElement>>();
			for (int i = 0; i < this._orderKeys.InternalArray.Length; i++)
			{
				list.Add(new KeyValuePair<TOrderKey, TElement>(this._orderKeys.InternalArray[i], this._values.InternalArray[i]));
			}
			list.Sort(0, this._values.Count, this._wrappedComparer);
			for (int j = 0; j < this._values.InternalArray.Length; j++)
			{
				this._orderKeys.InternalArray[j] = list[j].Key;
				this._values.InternalArray[j] = list[j].Value;
			}
		}

		// Token: 0x040006F3 RID: 1779
		private TGroupKey _groupKey;

		// Token: 0x040006F4 RID: 1780
		private GrowingArray<TElement> _values;

		// Token: 0x040006F5 RID: 1781
		private GrowingArray<TOrderKey> _orderKeys;

		// Token: 0x040006F6 RID: 1782
		private IComparer<TOrderKey> _orderComparer;

		// Token: 0x040006F7 RID: 1783
		private OrderedGroupByGrouping<TGroupKey, TOrderKey, TElement>.KeyAndValuesComparer _wrappedComparer;

		// Token: 0x02000193 RID: 403
		private class KeyAndValuesComparer : IComparer<KeyValuePair<TOrderKey, TElement>>
		{
			// Token: 0x06000A6C RID: 2668 RVA: 0x00021218 File Offset: 0x0001F418
			public KeyAndValuesComparer(IComparer<TOrderKey> comparer)
			{
				this.myComparer = comparer;
			}

			// Token: 0x06000A6D RID: 2669 RVA: 0x00021227 File Offset: 0x0001F427
			public int Compare(KeyValuePair<TOrderKey, TElement> x, KeyValuePair<TOrderKey, TElement> y)
			{
				return this.myComparer.Compare(x.Key, y.Key);
			}

			// Token: 0x040006F8 RID: 1784
			private IComparer<TOrderKey> myComparer;
		}

		// Token: 0x02000194 RID: 404
		[CompilerGenerated]
		private sealed class <System-Collections-Generic-IEnumerable<TElement>-GetEnumerator>d__8 : IEnumerator<!2>, IDisposable, IEnumerator
		{
			// Token: 0x06000A6E RID: 2670 RVA: 0x00021242 File Offset: 0x0001F442
			[DebuggerHidden]
			public <System-Collections-Generic-IEnumerable<TElement>-GetEnumerator>d__8(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x06000A6F RID: 2671 RVA: 0x000039E8 File Offset: 0x00001BE8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06000A70 RID: 2672 RVA: 0x00021254 File Offset: 0x0001F454
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				OrderedGroupByGrouping<TGroupKey, TOrderKey, TElement> orderedGroupByGrouping = this;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
					int num2 = i;
					i = num2 + 1;
				}
				else
				{
					this.<>1__state = -1;
					valueCount = orderedGroupByGrouping._values.Count;
					valueArray = orderedGroupByGrouping._values.InternalArray;
					i = 0;
				}
				if (i >= valueCount)
				{
					return false;
				}
				this.<>2__current = valueArray[i];
				this.<>1__state = 1;
				return true;
			}

			// Token: 0x1700016C RID: 364
			// (get) Token: 0x06000A71 RID: 2673 RVA: 0x000212F0 File Offset: 0x0001F4F0
			TElement IEnumerator<!2>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000A72 RID: 2674 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700016D RID: 365
			// (get) Token: 0x06000A73 RID: 2675 RVA: 0x000212F8 File Offset: 0x0001F4F8
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x040006F9 RID: 1785
			private int <>1__state;

			// Token: 0x040006FA RID: 1786
			private TElement <>2__current;

			// Token: 0x040006FB RID: 1787
			public OrderedGroupByGrouping<TGroupKey, TOrderKey, TElement> <>4__this;

			// Token: 0x040006FC RID: 1788
			private TElement[] <valueArray>5__1;

			// Token: 0x040006FD RID: 1789
			private int <i>5__2;

			// Token: 0x040006FE RID: 1790
			private int <valueCount>5__3;
		}
	}
}
