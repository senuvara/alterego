﻿using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace System.Linq.Parallel
{
	// Token: 0x020001BD RID: 445
	internal class OrderPreservingSpoolingTask<TInputOutput, TKey> : SpoolingTaskBase
	{
		// Token: 0x06000B10 RID: 2832 RVA: 0x00023904 File Offset: 0x00021B04
		private OrderPreservingSpoolingTask(int taskIndex, QueryTaskGroupState groupState, Shared<TInputOutput[]> results, SortHelper<TInputOutput> sortHelper) : base(taskIndex, groupState)
		{
			this._results = results;
			this._sortHelper = sortHelper;
		}

		// Token: 0x06000B11 RID: 2833 RVA: 0x00023920 File Offset: 0x00021B20
		internal static void Spool(QueryTaskGroupState groupState, PartitionedStream<TInputOutput, TKey> partitions, Shared<TInputOutput[]> results, TaskScheduler taskScheduler)
		{
			int maxToRunInParallel = partitions.PartitionCount - 1;
			SortHelper<TInputOutput, TKey>[] sortHelpers = SortHelper<TInputOutput, TKey>.GenerateSortHelpers(partitions, groupState);
			Task task = new Task(delegate()
			{
				for (int j = 0; j < maxToRunInParallel; j++)
				{
					new OrderPreservingSpoolingTask<TInputOutput, TKey>(j, groupState, results, sortHelpers[j]).RunAsynchronously(taskScheduler);
				}
				new OrderPreservingSpoolingTask<TInputOutput, TKey>(maxToRunInParallel, groupState, results, sortHelpers[maxToRunInParallel]).RunSynchronously(taskScheduler);
			});
			groupState.QueryBegin(task);
			task.RunSynchronously(taskScheduler);
			for (int i = 0; i < sortHelpers.Length; i++)
			{
				sortHelpers[i].Dispose();
			}
			groupState.QueryEnd(false);
		}

		// Token: 0x06000B12 RID: 2834 RVA: 0x000239C0 File Offset: 0x00021BC0
		protected override void SpoolingWork()
		{
			TInputOutput[] value = this._sortHelper.Sort();
			if (!this._groupState.CancellationState.MergedCancellationToken.IsCancellationRequested && this._taskIndex == 0)
			{
				this._results.Value = value;
			}
		}

		// Token: 0x04000797 RID: 1943
		private Shared<TInputOutput[]> _results;

		// Token: 0x04000798 RID: 1944
		private SortHelper<TInputOutput> _sortHelper;

		// Token: 0x020001BE RID: 446
		[CompilerGenerated]
		private sealed class <>c__DisplayClass3_0
		{
			// Token: 0x06000B13 RID: 2835 RVA: 0x00002310 File Offset: 0x00000510
			public <>c__DisplayClass3_0()
			{
			}

			// Token: 0x06000B14 RID: 2836 RVA: 0x00023A08 File Offset: 0x00021C08
			internal void <Spool>b__0()
			{
				for (int i = 0; i < this.maxToRunInParallel; i++)
				{
					new OrderPreservingSpoolingTask<TInputOutput, TKey>(i, this.groupState, this.results, this.sortHelpers[i]).RunAsynchronously(this.taskScheduler);
				}
				new OrderPreservingSpoolingTask<TInputOutput, TKey>(this.maxToRunInParallel, this.groupState, this.results, this.sortHelpers[this.maxToRunInParallel]).RunSynchronously(this.taskScheduler);
			}

			// Token: 0x04000799 RID: 1945
			public QueryTaskGroupState groupState;

			// Token: 0x0400079A RID: 1946
			public Shared<TInputOutput[]> results;

			// Token: 0x0400079B RID: 1947
			public SortHelper<TInputOutput, TKey>[] sortHelpers;

			// Token: 0x0400079C RID: 1948
			public TaskScheduler taskScheduler;

			// Token: 0x0400079D RID: 1949
			public int maxToRunInParallel;
		}
	}
}
