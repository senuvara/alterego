﻿using System;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x020000D7 RID: 215
	internal sealed class SynchronousChannel<T>
	{
		// Token: 0x060007AF RID: 1967 RVA: 0x00002310 File Offset: 0x00000510
		internal SynchronousChannel()
		{
		}

		// Token: 0x060007B0 RID: 1968 RVA: 0x0001713C File Offset: 0x0001533C
		internal void Init()
		{
			this._queue = new Queue<T>();
		}

		// Token: 0x060007B1 RID: 1969 RVA: 0x00017149 File Offset: 0x00015349
		internal void Enqueue(T item)
		{
			this._queue.Enqueue(item);
		}

		// Token: 0x060007B2 RID: 1970 RVA: 0x00017157 File Offset: 0x00015357
		internal T Dequeue()
		{
			return this._queue.Dequeue();
		}

		// Token: 0x060007B3 RID: 1971 RVA: 0x000039E8 File Offset: 0x00001BE8
		internal void SetDone()
		{
		}

		// Token: 0x060007B4 RID: 1972 RVA: 0x00017164 File Offset: 0x00015364
		internal void CopyTo(T[] array, int arrayIndex)
		{
			this._queue.CopyTo(array, arrayIndex);
		}

		// Token: 0x1700011B RID: 283
		// (get) Token: 0x060007B5 RID: 1973 RVA: 0x00017173 File Offset: 0x00015373
		internal int Count
		{
			get
			{
				return this._queue.Count;
			}
		}

		// Token: 0x0400051E RID: 1310
		private Queue<T> _queue;
	}
}
