﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x02000191 RID: 401
	internal class GroupByGrouping<TGroupKey, TElement> : IGrouping<TGroupKey, TElement>, IEnumerable<TElement>, IEnumerable
	{
		// Token: 0x06000A62 RID: 2658 RVA: 0x0002109C File Offset: 0x0001F29C
		internal GroupByGrouping(KeyValuePair<Wrapper<TGroupKey>, ListChunk<TElement>> keyValues)
		{
			this._keyValues = keyValues;
		}

		// Token: 0x1700016A RID: 362
		// (get) Token: 0x06000A63 RID: 2659 RVA: 0x000210AB File Offset: 0x0001F2AB
		TGroupKey IGrouping<!0, !1>.Key
		{
			get
			{
				return this._keyValues.Key.Value;
			}
		}

		// Token: 0x06000A64 RID: 2660 RVA: 0x000210BD File Offset: 0x0001F2BD
		IEnumerator<TElement> IEnumerable<!1>.GetEnumerator()
		{
			return this._keyValues.Value.GetEnumerator();
		}

		// Token: 0x06000A65 RID: 2661 RVA: 0x000210CF File Offset: 0x0001F2CF
		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable<!1>)this).GetEnumerator();
		}

		// Token: 0x040006F2 RID: 1778
		private KeyValuePair<Wrapper<TGroupKey>, ListChunk<TElement>> _keyValues;
	}
}
