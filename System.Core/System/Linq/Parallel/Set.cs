﻿using System;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x020000E6 RID: 230
	internal class Set<TElement>
	{
		// Token: 0x060007DE RID: 2014 RVA: 0x0001755E File Offset: 0x0001575E
		public Set(IEqualityComparer<TElement> comparer)
		{
			if (comparer == null)
			{
				comparer = EqualityComparer<TElement>.Default;
			}
			this._comparer = comparer;
			this._buckets = new int[7];
			this._slots = new Set<TElement>.Slot[7];
		}

		// Token: 0x060007DF RID: 2015 RVA: 0x0001758F File Offset: 0x0001578F
		public bool Add(TElement value)
		{
			return !this.Find(value, true);
		}

		// Token: 0x060007E0 RID: 2016 RVA: 0x0001759C File Offset: 0x0001579C
		public bool Contains(TElement value)
		{
			return this.Find(value, false);
		}

		// Token: 0x060007E1 RID: 2017 RVA: 0x000175A8 File Offset: 0x000157A8
		public bool Remove(TElement value)
		{
			int num = this.InternalGetHashCode(value);
			int num2 = num % this._buckets.Length;
			int num3 = -1;
			for (int i = this._buckets[num2] - 1; i >= 0; i = this._slots[i].next)
			{
				if (this._slots[i].hashCode == num && this._comparer.Equals(this._slots[i].value, value))
				{
					if (num3 < 0)
					{
						this._buckets[num2] = this._slots[i].next + 1;
					}
					else
					{
						this._slots[num3].next = this._slots[i].next;
					}
					this._slots[i].hashCode = -1;
					this._slots[i].value = default(TElement);
					this._slots[i].next = -1;
					return true;
				}
				num3 = i;
			}
			return false;
		}

		// Token: 0x060007E2 RID: 2018 RVA: 0x000176B0 File Offset: 0x000158B0
		private bool Find(TElement value, bool add)
		{
			int num = this.InternalGetHashCode(value);
			for (int i = this._buckets[num % this._buckets.Length] - 1; i >= 0; i = this._slots[i].next)
			{
				if (this._slots[i].hashCode == num && this._comparer.Equals(this._slots[i].value, value))
				{
					return true;
				}
			}
			if (add)
			{
				if (this._count == this._slots.Length)
				{
					this.Resize();
				}
				int count = this._count;
				this._count++;
				int num2 = num % this._buckets.Length;
				this._slots[count].hashCode = num;
				this._slots[count].value = value;
				this._slots[count].next = this._buckets[num2] - 1;
				this._buckets[num2] = count + 1;
			}
			return false;
		}

		// Token: 0x060007E3 RID: 2019 RVA: 0x000177B0 File Offset: 0x000159B0
		private void Resize()
		{
			int num = checked(this._count * 2 + 1);
			int[] array = new int[num];
			Set<TElement>.Slot[] array2 = new Set<TElement>.Slot[num];
			Array.Copy(this._slots, 0, array2, 0, this._count);
			for (int i = 0; i < this._count; i++)
			{
				int num2 = array2[i].hashCode % num;
				array2[i].next = array[num2] - 1;
				array[num2] = i + 1;
			}
			this._buckets = array;
			this._slots = array2;
		}

		// Token: 0x060007E4 RID: 2020 RVA: 0x00017832 File Offset: 0x00015A32
		internal int InternalGetHashCode(TElement value)
		{
			if (value != null)
			{
				return this._comparer.GetHashCode(value) & int.MaxValue;
			}
			return 0;
		}

		// Token: 0x04000536 RID: 1334
		private int[] _buckets;

		// Token: 0x04000537 RID: 1335
		private Set<TElement>.Slot[] _slots;

		// Token: 0x04000538 RID: 1336
		private int _count;

		// Token: 0x04000539 RID: 1337
		private readonly IEqualityComparer<TElement> _comparer;

		// Token: 0x0400053A RID: 1338
		private const int InitialSize = 7;

		// Token: 0x0400053B RID: 1339
		private const int HashCodeMask = 2147483647;

		// Token: 0x020000E7 RID: 231
		internal struct Slot
		{
			// Token: 0x0400053C RID: 1340
			internal int hashCode;

			// Token: 0x0400053D RID: 1341
			internal int next;

			// Token: 0x0400053E RID: 1342
			internal TElement value;
		}
	}
}
