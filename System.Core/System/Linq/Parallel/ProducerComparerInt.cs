﻿using System;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x020000F3 RID: 243
	internal class ProducerComparerInt : IComparer<Producer<int>>
	{
		// Token: 0x06000816 RID: 2070 RVA: 0x000184FC File Offset: 0x000166FC
		public int Compare(Producer<int> x, Producer<int> y)
		{
			return y.MaxKey - x.MaxKey;
		}

		// Token: 0x06000817 RID: 2071 RVA: 0x00002310 File Offset: 0x00000510
		public ProducerComparerInt()
		{
		}
	}
}
