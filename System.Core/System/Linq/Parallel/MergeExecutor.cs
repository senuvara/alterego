﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace System.Linq.Parallel
{
	// Token: 0x020000ED RID: 237
	internal class MergeExecutor<TInputOutput> : IEnumerable<TInputOutput>, IEnumerable
	{
		// Token: 0x060007FC RID: 2044 RVA: 0x00002310 File Offset: 0x00000510
		private MergeExecutor()
		{
		}

		// Token: 0x060007FD RID: 2045 RVA: 0x00017E1C File Offset: 0x0001601C
		internal static MergeExecutor<TInputOutput> Execute<TKey>(PartitionedStream<TInputOutput, TKey> partitions, bool ignoreOutput, ParallelMergeOptions options, TaskScheduler taskScheduler, bool isOrdered, CancellationState cancellationState, int queryId)
		{
			MergeExecutor<TInputOutput> mergeExecutor = new MergeExecutor<TInputOutput>();
			if (isOrdered && !ignoreOutput)
			{
				if (options != ParallelMergeOptions.FullyBuffered && !partitions.OrdinalIndexState.IsWorseThan(OrdinalIndexState.Increasing))
				{
					bool autoBuffered = options == ParallelMergeOptions.AutoBuffered;
					if (partitions.PartitionCount > 1)
					{
						mergeExecutor._mergeHelper = new OrderPreservingPipeliningMergeHelper<TInputOutput, TKey>(partitions, taskScheduler, cancellationState, autoBuffered, queryId, partitions.KeyComparer);
					}
					else
					{
						mergeExecutor._mergeHelper = new DefaultMergeHelper<TInputOutput, TKey>(partitions, false, options, taskScheduler, cancellationState, queryId);
					}
				}
				else
				{
					mergeExecutor._mergeHelper = new OrderPreservingMergeHelper<TInputOutput, TKey>(partitions, taskScheduler, cancellationState, queryId);
				}
			}
			else
			{
				mergeExecutor._mergeHelper = new DefaultMergeHelper<TInputOutput, TKey>(partitions, ignoreOutput, options, taskScheduler, cancellationState, queryId);
			}
			mergeExecutor.Execute();
			return mergeExecutor;
		}

		// Token: 0x060007FE RID: 2046 RVA: 0x00017EB2 File Offset: 0x000160B2
		private void Execute()
		{
			this._mergeHelper.Execute();
		}

		// Token: 0x060007FF RID: 2047 RVA: 0x00017EBF File Offset: 0x000160BF
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x06000800 RID: 2048 RVA: 0x00017EC7 File Offset: 0x000160C7
		public IEnumerator<TInputOutput> GetEnumerator()
		{
			return this._mergeHelper.GetEnumerator();
		}

		// Token: 0x06000801 RID: 2049 RVA: 0x00017ED4 File Offset: 0x000160D4
		internal TInputOutput[] GetResultsAsArray()
		{
			return this._mergeHelper.GetResultsAsArray();
		}

		// Token: 0x06000802 RID: 2050 RVA: 0x00017EE4 File Offset: 0x000160E4
		internal static AsynchronousChannel<TInputOutput>[] MakeAsynchronousChannels(int partitionCount, ParallelMergeOptions options, IntValueEvent consumerEvent, CancellationToken cancellationToken)
		{
			AsynchronousChannel<TInputOutput>[] array = new AsynchronousChannel<TInputOutput>[partitionCount];
			int chunkSize = 0;
			if (options == ParallelMergeOptions.NotBuffered)
			{
				chunkSize = 1;
			}
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = new AsynchronousChannel<TInputOutput>(i, chunkSize, cancellationToken, consumerEvent);
			}
			return array;
		}

		// Token: 0x06000803 RID: 2051 RVA: 0x00017F1C File Offset: 0x0001611C
		internal static SynchronousChannel<TInputOutput>[] MakeSynchronousChannels(int partitionCount)
		{
			SynchronousChannel<TInputOutput>[] array = new SynchronousChannel<TInputOutput>[partitionCount];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = new SynchronousChannel<TInputOutput>();
			}
			return array;
		}

		// Token: 0x0400054F RID: 1359
		private IMergeHelper<TInputOutput> _mergeHelper;
	}
}
