﻿using System;

namespace System.Linq.Parallel
{
	// Token: 0x02000167 RID: 359
	internal enum OrdinalIndexState : byte
	{
		// Token: 0x04000675 RID: 1653
		Indexable,
		// Token: 0x04000676 RID: 1654
		Correct,
		// Token: 0x04000677 RID: 1655
		Increasing,
		// Token: 0x04000678 RID: 1656
		Shuffled
	}
}
