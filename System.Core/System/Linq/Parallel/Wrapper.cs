﻿using System;

namespace System.Linq.Parallel
{
	// Token: 0x020001EC RID: 492
	internal struct Wrapper<T>
	{
		// Token: 0x06000BD0 RID: 3024 RVA: 0x00025EFE File Offset: 0x000240FE
		internal Wrapper(T value)
		{
			this.Value = value;
		}

		// Token: 0x04000819 RID: 2073
		internal T Value;
	}
}
