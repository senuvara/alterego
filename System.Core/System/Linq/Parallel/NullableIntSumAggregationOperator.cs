﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x0200015C RID: 348
	internal sealed class NullableIntSumAggregationOperator : InlinedAggregationOperator<int?, int?, int?>
	{
		// Token: 0x0600096C RID: 2412 RVA: 0x0001E3CB File Offset: 0x0001C5CB
		internal NullableIntSumAggregationOperator(IEnumerable<int?> child) : base(child)
		{
		}

		// Token: 0x0600096D RID: 2413 RVA: 0x0001E3D4 File Offset: 0x0001C5D4
		protected override int? InternalAggregate(ref Exception singularExceptionToThrow)
		{
			int? result;
			using (IEnumerator<int?> enumerator = this.GetEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered), true))
			{
				int num = 0;
				while (enumerator.MoveNext())
				{
					int num2 = num;
					result = enumerator.Current;
					num = checked(num2 + result.GetValueOrDefault());
				}
				result = new int?(num);
			}
			return result;
		}

		// Token: 0x0600096E RID: 2414 RVA: 0x0001E430 File Offset: 0x0001C630
		protected override QueryOperatorEnumerator<int?, int> CreateEnumerator<TKey>(int index, int count, QueryOperatorEnumerator<int?, TKey> source, object sharedData, CancellationToken cancellationToken)
		{
			return new NullableIntSumAggregationOperator.NullableIntSumAggregationOperatorEnumerator<TKey>(source, index, cancellationToken);
		}

		// Token: 0x0200015D RID: 349
		private class NullableIntSumAggregationOperatorEnumerator<TKey> : InlinedAggregationOperatorEnumerator<int?>
		{
			// Token: 0x0600096F RID: 2415 RVA: 0x0001E43B File Offset: 0x0001C63B
			internal NullableIntSumAggregationOperatorEnumerator(QueryOperatorEnumerator<int?, TKey> source, int partitionIndex, CancellationToken cancellationToken) : base(partitionIndex, cancellationToken)
			{
				this._source = source;
			}

			// Token: 0x06000970 RID: 2416 RVA: 0x0001E44C File Offset: 0x0001C64C
			protected override bool MoveNextCore(ref int? currentElement)
			{
				int? num = null;
				TKey tkey = default(TKey);
				QueryOperatorEnumerator<int?, TKey> source = this._source;
				if (source.MoveNext(ref num, ref tkey))
				{
					int num2 = 0;
					int num3 = 0;
					do
					{
						if ((num3++ & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(this._cancellationToken);
						}
						checked
						{
							num2 += num.GetValueOrDefault();
						}
					}
					while (source.MoveNext(ref num, ref tkey));
					currentElement = new int?(num2);
					return true;
				}
				return false;
			}

			// Token: 0x06000971 RID: 2417 RVA: 0x0001E4BD File Offset: 0x0001C6BD
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x04000667 RID: 1639
			private QueryOperatorEnumerator<int?, TKey> _source;
		}
	}
}
