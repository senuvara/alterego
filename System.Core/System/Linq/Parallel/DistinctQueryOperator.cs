﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x0200017D RID: 381
	internal sealed class DistinctQueryOperator<TInputOutput> : UnaryQueryOperator<TInputOutput, TInputOutput>
	{
		// Token: 0x06000A20 RID: 2592 RVA: 0x0001FF88 File Offset: 0x0001E188
		internal DistinctQueryOperator(IEnumerable<TInputOutput> source, IEqualityComparer<TInputOutput> comparer) : base(source)
		{
			this._comparer = comparer;
			base.SetOrdinalIndexState(OrdinalIndexState.Shuffled);
		}

		// Token: 0x06000A21 RID: 2593 RVA: 0x0001FF9F File Offset: 0x0001E19F
		internal override QueryResults<TInputOutput> Open(QuerySettings settings, bool preferStriping)
		{
			return new UnaryQueryOperator<TInputOutput, TInputOutput>.UnaryQueryOperatorResults(base.Child.Open(settings, false), this, settings, false);
		}

		// Token: 0x06000A22 RID: 2594 RVA: 0x0001FFB8 File Offset: 0x0001E1B8
		internal override void WrapPartitionedStream<TKey>(PartitionedStream<TInputOutput, TKey> inputStream, IPartitionedStreamRecipient<TInputOutput> recipient, bool preferStriping, QuerySettings settings)
		{
			if (base.OutputOrdered)
			{
				this.WrapPartitionedStreamHelper<TKey>(ExchangeUtilities.HashRepartitionOrdered<TInputOutput, NoKeyMemoizationRequired, TKey>(inputStream, null, null, this._comparer, settings.CancellationState.MergedCancellationToken), recipient, settings.CancellationState.MergedCancellationToken);
				return;
			}
			this.WrapPartitionedStreamHelper<int>(ExchangeUtilities.HashRepartition<TInputOutput, NoKeyMemoizationRequired, TKey>(inputStream, null, null, this._comparer, settings.CancellationState.MergedCancellationToken), recipient, settings.CancellationState.MergedCancellationToken);
		}

		// Token: 0x06000A23 RID: 2595 RVA: 0x00020028 File Offset: 0x0001E228
		private void WrapPartitionedStreamHelper<TKey>(PartitionedStream<Pair<TInputOutput, NoKeyMemoizationRequired>, TKey> hashStream, IPartitionedStreamRecipient<TInputOutput> recipient, CancellationToken cancellationToken)
		{
			int partitionCount = hashStream.PartitionCount;
			PartitionedStream<TInputOutput, TKey> partitionedStream = new PartitionedStream<TInputOutput, TKey>(partitionCount, hashStream.KeyComparer, OrdinalIndexState.Shuffled);
			for (int i = 0; i < partitionCount; i++)
			{
				if (base.OutputOrdered)
				{
					partitionedStream[i] = new DistinctQueryOperator<TInputOutput>.OrderedDistinctQueryOperatorEnumerator<TKey>(hashStream[i], this._comparer, hashStream.KeyComparer, cancellationToken);
				}
				else
				{
					partitionedStream[i] = (QueryOperatorEnumerator<TInputOutput, TKey>)new DistinctQueryOperator<TInputOutput>.DistinctQueryOperatorEnumerator<TKey>(hashStream[i], this._comparer, cancellationToken);
				}
			}
			recipient.Receive<TKey>(partitionedStream);
		}

		// Token: 0x17000165 RID: 357
		// (get) Token: 0x06000A24 RID: 2596 RVA: 0x00002285 File Offset: 0x00000485
		internal override bool LimitsParallelism
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06000A25 RID: 2597 RVA: 0x000200A6 File Offset: 0x0001E2A6
		internal override IEnumerable<TInputOutput> AsSequentialQuery(CancellationToken token)
		{
			return CancellableEnumerable.Wrap<TInputOutput>(base.Child.AsSequentialQuery(token), token).Distinct(this._comparer);
		}

		// Token: 0x040006BC RID: 1724
		private readonly IEqualityComparer<TInputOutput> _comparer;

		// Token: 0x0200017E RID: 382
		private class DistinctQueryOperatorEnumerator<TKey> : QueryOperatorEnumerator<TInputOutput, int>
		{
			// Token: 0x06000A26 RID: 2598 RVA: 0x000200C5 File Offset: 0x0001E2C5
			internal DistinctQueryOperatorEnumerator(QueryOperatorEnumerator<Pair<TInputOutput, NoKeyMemoizationRequired>, TKey> source, IEqualityComparer<TInputOutput> comparer, CancellationToken cancellationToken)
			{
				this._source = source;
				this._hashLookup = new Set<TInputOutput>(comparer);
				this._cancellationToken = cancellationToken;
			}

			// Token: 0x06000A27 RID: 2599 RVA: 0x000200E8 File Offset: 0x0001E2E8
			internal override bool MoveNext(ref TInputOutput currentElement, ref int currentKey)
			{
				TKey tkey = default(TKey);
				Pair<TInputOutput, NoKeyMemoizationRequired> pair = default(Pair<TInputOutput, NoKeyMemoizationRequired>);
				if (this._outputLoopCount == null)
				{
					this._outputLoopCount = new Shared<int>(0);
				}
				while (this._source.MoveNext(ref pair, ref tkey))
				{
					Shared<int> outputLoopCount = this._outputLoopCount;
					int value = outputLoopCount.Value;
					outputLoopCount.Value = value + 1;
					if ((value & 63) == 0)
					{
						CancellationState.ThrowIfCanceled(this._cancellationToken);
					}
					if (this._hashLookup.Add(pair.First))
					{
						currentElement = pair.First;
						return true;
					}
				}
				return false;
			}

			// Token: 0x06000A28 RID: 2600 RVA: 0x00020176 File Offset: 0x0001E376
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x040006BD RID: 1725
			private QueryOperatorEnumerator<Pair<TInputOutput, NoKeyMemoizationRequired>, TKey> _source;

			// Token: 0x040006BE RID: 1726
			private Set<TInputOutput> _hashLookup;

			// Token: 0x040006BF RID: 1727
			private CancellationToken _cancellationToken;

			// Token: 0x040006C0 RID: 1728
			private Shared<int> _outputLoopCount;
		}

		// Token: 0x0200017F RID: 383
		private class OrderedDistinctQueryOperatorEnumerator<TKey> : QueryOperatorEnumerator<TInputOutput, TKey>
		{
			// Token: 0x06000A29 RID: 2601 RVA: 0x00020183 File Offset: 0x0001E383
			internal OrderedDistinctQueryOperatorEnumerator(QueryOperatorEnumerator<Pair<TInputOutput, NoKeyMemoizationRequired>, TKey> source, IEqualityComparer<TInputOutput> comparer, IComparer<TKey> keyComparer, CancellationToken cancellationToken)
			{
				this._source = source;
				this._keyComparer = keyComparer;
				this._hashLookup = new Dictionary<Wrapper<TInputOutput>, TKey>(new WrapperEqualityComparer<TInputOutput>(comparer));
				this._cancellationToken = cancellationToken;
			}

			// Token: 0x06000A2A RID: 2602 RVA: 0x000201B8 File Offset: 0x0001E3B8
			internal override bool MoveNext(ref TInputOutput currentElement, ref TKey currentKey)
			{
				if (this._hashLookupEnumerator == null)
				{
					Pair<TInputOutput, NoKeyMemoizationRequired> pair = default(Pair<TInputOutput, NoKeyMemoizationRequired>);
					TKey tkey = default(TKey);
					int num = 0;
					while (this._source.MoveNext(ref pair, ref tkey))
					{
						if ((num++ & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(this._cancellationToken);
						}
						Wrapper<TInputOutput> key = new Wrapper<TInputOutput>(pair.First);
						TKey y;
						if (!this._hashLookup.TryGetValue(key, out y) || this._keyComparer.Compare(tkey, y) < 0)
						{
							this._hashLookup[key] = tkey;
						}
					}
					this._hashLookupEnumerator = this._hashLookup.GetEnumerator();
				}
				if (this._hashLookupEnumerator.MoveNext())
				{
					KeyValuePair<Wrapper<TInputOutput>, TKey> keyValuePair = this._hashLookupEnumerator.Current;
					currentElement = keyValuePair.Key.Value;
					currentKey = keyValuePair.Value;
					return true;
				}
				return false;
			}

			// Token: 0x06000A2B RID: 2603 RVA: 0x00020299 File Offset: 0x0001E499
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
				if (this._hashLookupEnumerator != null)
				{
					this._hashLookupEnumerator.Dispose();
				}
			}

			// Token: 0x040006C1 RID: 1729
			private QueryOperatorEnumerator<Pair<TInputOutput, NoKeyMemoizationRequired>, TKey> _source;

			// Token: 0x040006C2 RID: 1730
			private Dictionary<Wrapper<TInputOutput>, TKey> _hashLookup;

			// Token: 0x040006C3 RID: 1731
			private IComparer<TKey> _keyComparer;

			// Token: 0x040006C4 RID: 1732
			private IEnumerator<KeyValuePair<Wrapper<TInputOutput>, TKey>> _hashLookupEnumerator;

			// Token: 0x040006C5 RID: 1733
			private CancellationToken _cancellationToken;
		}
	}
}
