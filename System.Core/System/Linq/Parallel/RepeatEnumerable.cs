﻿using System;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x020000E3 RID: 227
	internal class RepeatEnumerable<TResult> : ParallelQuery<TResult>, IParallelPartitionable<TResult>
	{
		// Token: 0x060007D6 RID: 2006 RVA: 0x000173F4 File Offset: 0x000155F4
		internal RepeatEnumerable(TResult element, int count) : base(QuerySettings.Empty)
		{
			this._element = element;
			this._count = count;
		}

		// Token: 0x060007D7 RID: 2007 RVA: 0x00017410 File Offset: 0x00015610
		public QueryOperatorEnumerator<TResult, int>[] GetPartitions(int partitionCount)
		{
			int num = (this._count + partitionCount - 1) / partitionCount;
			QueryOperatorEnumerator<TResult, int>[] array = new QueryOperatorEnumerator<TResult, int>[partitionCount];
			int i = 0;
			int num2 = 0;
			while (i < partitionCount)
			{
				if (num2 + num > this._count)
				{
					array[i] = new RepeatEnumerable<TResult>.RepeatEnumerator(this._element, (num2 < this._count) ? (this._count - num2) : 0, num2);
				}
				else
				{
					array[i] = new RepeatEnumerable<TResult>.RepeatEnumerator(this._element, num, num2);
				}
				i++;
				num2 += num;
			}
			return array;
		}

		// Token: 0x060007D8 RID: 2008 RVA: 0x00017484 File Offset: 0x00015684
		public override IEnumerator<TResult> GetEnumerator()
		{
			return new RepeatEnumerable<TResult>.RepeatEnumerator(this._element, this._count, 0).AsClassicEnumerator();
		}

		// Token: 0x04000530 RID: 1328
		private TResult _element;

		// Token: 0x04000531 RID: 1329
		private int _count;

		// Token: 0x020000E4 RID: 228
		private class RepeatEnumerator : QueryOperatorEnumerator<TResult, int>
		{
			// Token: 0x060007D9 RID: 2009 RVA: 0x0001749D File Offset: 0x0001569D
			internal RepeatEnumerator(TResult element, int count, int indexOffset)
			{
				this._element = element;
				this._count = count;
				this._indexOffset = indexOffset;
			}

			// Token: 0x060007DA RID: 2010 RVA: 0x000174BC File Offset: 0x000156BC
			internal override bool MoveNext(ref TResult currentElement, ref int currentKey)
			{
				if (this._currentIndex == null)
				{
					this._currentIndex = new Shared<int>(-1);
				}
				if (this._currentIndex.Value < this._count - 1)
				{
					this._currentIndex.Value++;
					currentElement = this._element;
					currentKey = this._currentIndex.Value + this._indexOffset;
					return true;
				}
				return false;
			}

			// Token: 0x060007DB RID: 2011 RVA: 0x00017528 File Offset: 0x00015728
			internal override void Reset()
			{
				this._currentIndex = null;
			}

			// Token: 0x04000532 RID: 1330
			private readonly TResult _element;

			// Token: 0x04000533 RID: 1331
			private readonly int _count;

			// Token: 0x04000534 RID: 1332
			private readonly int _indexOffset;

			// Token: 0x04000535 RID: 1333
			private Shared<int> _currentIndex;
		}
	}
}
