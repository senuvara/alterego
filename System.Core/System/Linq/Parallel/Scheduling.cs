﻿using System;

namespace System.Linq.Parallel
{
	// Token: 0x020001C3 RID: 451
	internal static class Scheduling
	{
		// Token: 0x06000B27 RID: 2855 RVA: 0x00023CD4 File Offset: 0x00021ED4
		internal static int GetDefaultDegreeOfParallelism()
		{
			return Scheduling.DefaultDegreeOfParallelism;
		}

		// Token: 0x06000B28 RID: 2856 RVA: 0x00023CDC File Offset: 0x00021EDC
		internal static int GetDefaultChunkSize<T>()
		{
			int result;
			if (default(T) != null || Nullable.GetUnderlyingType(typeof(T)) != null)
			{
				result = 128;
			}
			else
			{
				result = 512 / IntPtr.Size;
			}
			return result;
		}

		// Token: 0x06000B29 RID: 2857 RVA: 0x00023D25 File Offset: 0x00021F25
		// Note: this type is marked as 'beforefieldinit'.
		static Scheduling()
		{
		}

		// Token: 0x040007A7 RID: 1959
		internal const bool DefaultPreserveOrder = false;

		// Token: 0x040007A8 RID: 1960
		internal static int DefaultDegreeOfParallelism = Math.Min(Environment.ProcessorCount, 512);

		// Token: 0x040007A9 RID: 1961
		internal const int DEFAULT_BOUNDED_BUFFER_CAPACITY = 512;

		// Token: 0x040007AA RID: 1962
		internal const int DEFAULT_BYTES_PER_CHUNK = 512;

		// Token: 0x040007AB RID: 1963
		internal const int ZOMBIED_PRODUCER_TIMEOUT = -1;

		// Token: 0x040007AC RID: 1964
		internal const int MAX_SUPPORTED_DOP = 512;
	}
}
