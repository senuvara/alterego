﻿using System;

namespace System.Linq.Parallel
{
	// Token: 0x020001DF RID: 479
	internal struct Pair<T, U>
	{
		// Token: 0x06000BAD RID: 2989 RVA: 0x000254B8 File Offset: 0x000236B8
		public Pair(T first, U second)
		{
			this._first = first;
			this._second = second;
		}

		// Token: 0x1700019E RID: 414
		// (get) Token: 0x06000BAE RID: 2990 RVA: 0x000254C8 File Offset: 0x000236C8
		// (set) Token: 0x06000BAF RID: 2991 RVA: 0x000254D0 File Offset: 0x000236D0
		public T First
		{
			get
			{
				return this._first;
			}
			set
			{
				this._first = value;
			}
		}

		// Token: 0x1700019F RID: 415
		// (get) Token: 0x06000BB0 RID: 2992 RVA: 0x000254D9 File Offset: 0x000236D9
		// (set) Token: 0x06000BB1 RID: 2993 RVA: 0x000254E1 File Offset: 0x000236E1
		public U Second
		{
			get
			{
				return this._second;
			}
			set
			{
				this._second = value;
			}
		}

		// Token: 0x04000804 RID: 2052
		internal T _first;

		// Token: 0x04000805 RID: 2053
		internal U _second;
	}
}
