﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x0200016E RID: 366
	internal class QueryOpeningEnumerator<TOutput> : IEnumerator<!0>, IDisposable, IEnumerator
	{
		// Token: 0x060009B0 RID: 2480 RVA: 0x0001EF1B File Offset: 0x0001D11B
		internal QueryOpeningEnumerator(QueryOperator<TOutput> queryOperator, ParallelMergeOptions? mergeOptions, bool suppressOrderPreservation)
		{
			this._queryOperator = queryOperator;
			this._mergeOptions = mergeOptions;
			this._suppressOrderPreservation = suppressOrderPreservation;
		}

		// Token: 0x1700014A RID: 330
		// (get) Token: 0x060009B1 RID: 2481 RVA: 0x0001EF4F File Offset: 0x0001D14F
		public TOutput Current
		{
			get
			{
				if (this._openedQueryEnumerator == null)
				{
					throw new InvalidOperationException("Enumeration has not started. MoveNext must be called to initiate enumeration.");
				}
				return this._openedQueryEnumerator.Current;
			}
		}

		// Token: 0x060009B2 RID: 2482 RVA: 0x0001EF70 File Offset: 0x0001D170
		public void Dispose()
		{
			this._topLevelDisposedFlag.Value = true;
			this._topLevelCancellationTokenSource.Cancel();
			if (this._openedQueryEnumerator != null)
			{
				this._openedQueryEnumerator.Dispose();
				this._querySettings.CleanStateAtQueryEnd();
			}
			QueryLifecycle.LogicalQueryExecutionEnd(this._querySettings.QueryId);
		}

		// Token: 0x1700014B RID: 331
		// (get) Token: 0x060009B3 RID: 2483 RVA: 0x00017DF3 File Offset: 0x00015FF3
		object IEnumerator.Current
		{
			get
			{
				return ((IEnumerator<!0>)this).Current;
			}
		}

		// Token: 0x060009B4 RID: 2484 RVA: 0x0001EFC4 File Offset: 0x0001D1C4
		public bool MoveNext()
		{
			if (this._topLevelDisposedFlag.Value)
			{
				throw new ObjectDisposedException("enumerator", "The query enumerator has been disposed.");
			}
			if (this._openedQueryEnumerator == null)
			{
				this.OpenQuery();
			}
			bool result = this._openedQueryEnumerator.MoveNext();
			if ((this._moveNextIteration & 63) == 0)
			{
				CancellationState.ThrowWithStandardMessageIfCanceled(this._querySettings.CancellationState.ExternalCancellationToken);
			}
			this._moveNextIteration++;
			return result;
		}

		// Token: 0x060009B5 RID: 2485 RVA: 0x0001F038 File Offset: 0x0001D238
		private void OpenQuery()
		{
			if (this._hasQueryOpeningFailed)
			{
				throw new InvalidOperationException("The query enumerator previously threw an exception.");
			}
			try
			{
				this._querySettings = this._queryOperator.SpecifiedQuerySettings.WithPerExecutionSettings(this._topLevelCancellationTokenSource, this._topLevelDisposedFlag).WithDefaults();
				QueryLifecycle.LogicalQueryExecutionBegin(this._querySettings.QueryId);
				this._openedQueryEnumerator = this._queryOperator.GetOpenedEnumerator(this._mergeOptions, this._suppressOrderPreservation, false, this._querySettings);
				CancellationState.ThrowWithStandardMessageIfCanceled(this._querySettings.CancellationState.ExternalCancellationToken);
			}
			catch
			{
				this._hasQueryOpeningFailed = true;
				throw;
			}
		}

		// Token: 0x060009B6 RID: 2486 RVA: 0x00003A6B File Offset: 0x00001C6B
		public void Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x0400068A RID: 1674
		private readonly QueryOperator<TOutput> _queryOperator;

		// Token: 0x0400068B RID: 1675
		private IEnumerator<TOutput> _openedQueryEnumerator;

		// Token: 0x0400068C RID: 1676
		private QuerySettings _querySettings;

		// Token: 0x0400068D RID: 1677
		private readonly ParallelMergeOptions? _mergeOptions;

		// Token: 0x0400068E RID: 1678
		private readonly bool _suppressOrderPreservation;

		// Token: 0x0400068F RID: 1679
		private int _moveNextIteration;

		// Token: 0x04000690 RID: 1680
		private bool _hasQueryOpeningFailed;

		// Token: 0x04000691 RID: 1681
		private readonly Shared<bool> _topLevelDisposedFlag = new Shared<bool>(false);

		// Token: 0x04000692 RID: 1682
		private readonly CancellationTokenSource _topLevelCancellationTokenSource = new CancellationTokenSource();
	}
}
