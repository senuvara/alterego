﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace System.Linq.Parallel
{
	// Token: 0x020000EA RID: 234
	internal class DefaultMergeHelper<TInputOutput, TIgnoreKey> : IMergeHelper<TInputOutput>
	{
		// Token: 0x060007EF RID: 2031 RVA: 0x00017BC8 File Offset: 0x00015DC8
		internal DefaultMergeHelper(PartitionedStream<TInputOutput, TIgnoreKey> partitions, bool ignoreOutput, ParallelMergeOptions options, TaskScheduler taskScheduler, CancellationState cancellationState, int queryId)
		{
			this._taskGroupState = new QueryTaskGroupState(cancellationState, queryId);
			this._partitions = partitions;
			this._taskScheduler = taskScheduler;
			this._ignoreOutput = ignoreOutput;
			IntValueEvent consumerEvent = new IntValueEvent();
			if (!ignoreOutput)
			{
				if (options != ParallelMergeOptions.FullyBuffered)
				{
					if (partitions.PartitionCount > 1)
					{
						this._asyncChannels = MergeExecutor<TInputOutput>.MakeAsynchronousChannels(partitions.PartitionCount, options, consumerEvent, cancellationState.MergedCancellationToken);
						this._channelEnumerator = new AsynchronousChannelMergeEnumerator<TInputOutput>(this._taskGroupState, this._asyncChannels, consumerEvent);
						return;
					}
					this._channelEnumerator = ExceptionAggregator.WrapQueryEnumerator<TInputOutput, TIgnoreKey>(partitions[0], this._taskGroupState.CancellationState).GetEnumerator();
					return;
				}
				else
				{
					this._syncChannels = MergeExecutor<TInputOutput>.MakeSynchronousChannels(partitions.PartitionCount);
					this._channelEnumerator = new SynchronousChannelMergeEnumerator<TInputOutput>(this._taskGroupState, this._syncChannels);
				}
			}
		}

		// Token: 0x060007F0 RID: 2032 RVA: 0x00017C98 File Offset: 0x00015E98
		void IMergeHelper<!0>.Execute()
		{
			if (this._asyncChannels != null)
			{
				SpoolingTask.SpoolPipeline<TInputOutput, TIgnoreKey>(this._taskGroupState, this._partitions, this._asyncChannels, this._taskScheduler);
				return;
			}
			if (this._syncChannels != null)
			{
				SpoolingTask.SpoolStopAndGo<TInputOutput, TIgnoreKey>(this._taskGroupState, this._partitions, this._syncChannels, this._taskScheduler);
				return;
			}
			if (this._ignoreOutput)
			{
				SpoolingTask.SpoolForAll<TInputOutput, TIgnoreKey>(this._taskGroupState, this._partitions, this._taskScheduler);
			}
		}

		// Token: 0x060007F1 RID: 2033 RVA: 0x00017D10 File Offset: 0x00015F10
		IEnumerator<TInputOutput> IMergeHelper<!0>.GetEnumerator()
		{
			return this._channelEnumerator;
		}

		// Token: 0x060007F2 RID: 2034 RVA: 0x00017D18 File Offset: 0x00015F18
		public TInputOutput[] GetResultsAsArray()
		{
			if (this._syncChannels != null)
			{
				int num = 0;
				for (int i = 0; i < this._syncChannels.Length; i++)
				{
					num += this._syncChannels[i].Count;
				}
				TInputOutput[] array = new TInputOutput[num];
				int num2 = 0;
				for (int j = 0; j < this._syncChannels.Length; j++)
				{
					this._syncChannels[j].CopyTo(array, num2);
					num2 += this._syncChannels[j].Count;
				}
				return array;
			}
			List<TInputOutput> list = new List<TInputOutput>();
			foreach (TInputOutput item in ((IMergeHelper<!0>)this))
			{
				list.Add(item);
			}
			return list.ToArray();
		}

		// Token: 0x04000547 RID: 1351
		private QueryTaskGroupState _taskGroupState;

		// Token: 0x04000548 RID: 1352
		private PartitionedStream<TInputOutput, TIgnoreKey> _partitions;

		// Token: 0x04000549 RID: 1353
		private AsynchronousChannel<TInputOutput>[] _asyncChannels;

		// Token: 0x0400054A RID: 1354
		private SynchronousChannel<TInputOutput>[] _syncChannels;

		// Token: 0x0400054B RID: 1355
		private IEnumerator<TInputOutput> _channelEnumerator;

		// Token: 0x0400054C RID: 1356
		private TaskScheduler _taskScheduler;

		// Token: 0x0400054D RID: 1357
		private bool _ignoreOutput;
	}
}
