﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000134 RID: 308
	internal sealed class FloatSumAggregationOperator : InlinedAggregationOperator<float, double, float>
	{
		// Token: 0x060008EF RID: 2287 RVA: 0x0001C67D File Offset: 0x0001A87D
		internal FloatSumAggregationOperator(IEnumerable<float> child) : base(child)
		{
		}

		// Token: 0x060008F0 RID: 2288 RVA: 0x0001C688 File Offset: 0x0001A888
		protected override float InternalAggregate(ref Exception singularExceptionToThrow)
		{
			float result;
			using (IEnumerator<double> enumerator = this.GetEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered), true))
			{
				double num = 0.0;
				while (enumerator.MoveNext())
				{
					double num2 = enumerator.Current;
					num += num2;
				}
				result = (float)num;
			}
			return result;
		}

		// Token: 0x060008F1 RID: 2289 RVA: 0x0001C6E0 File Offset: 0x0001A8E0
		protected override QueryOperatorEnumerator<double, int> CreateEnumerator<TKey>(int index, int count, QueryOperatorEnumerator<float, TKey> source, object sharedData, CancellationToken cancellationToken)
		{
			return new FloatSumAggregationOperator.FloatSumAggregationOperatorEnumerator<TKey>(source, index, cancellationToken);
		}

		// Token: 0x02000135 RID: 309
		private class FloatSumAggregationOperatorEnumerator<TKey> : InlinedAggregationOperatorEnumerator<double>
		{
			// Token: 0x060008F2 RID: 2290 RVA: 0x0001C6EB File Offset: 0x0001A8EB
			internal FloatSumAggregationOperatorEnumerator(QueryOperatorEnumerator<float, TKey> source, int partitionIndex, CancellationToken cancellationToken) : base(partitionIndex, cancellationToken)
			{
				this._source = source;
			}

			// Token: 0x060008F3 RID: 2291 RVA: 0x0001C6FC File Offset: 0x0001A8FC
			protected override bool MoveNextCore(ref double currentElement)
			{
				float num = 0f;
				TKey tkey = default(TKey);
				QueryOperatorEnumerator<float, TKey> source = this._source;
				if (source.MoveNext(ref num, ref tkey))
				{
					double num2 = 0.0;
					int num3 = 0;
					do
					{
						if ((num3++ & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(this._cancellationToken);
						}
						num2 += (double)num;
					}
					while (source.MoveNext(ref num, ref tkey));
					currentElement = num2;
					return true;
				}
				return false;
			}

			// Token: 0x060008F4 RID: 2292 RVA: 0x0001C765 File Offset: 0x0001A965
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x04000645 RID: 1605
			private readonly QueryOperatorEnumerator<float, TKey> _source;
		}
	}
}
