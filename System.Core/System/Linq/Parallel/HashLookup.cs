﻿using System;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x020001D6 RID: 470
	internal class HashLookup<TKey, TValue>
	{
		// Token: 0x06000B7A RID: 2938 RVA: 0x00024C27 File Offset: 0x00022E27
		internal HashLookup() : this(null)
		{
		}

		// Token: 0x06000B7B RID: 2939 RVA: 0x00024C30 File Offset: 0x00022E30
		internal HashLookup(IEqualityComparer<TKey> comparer)
		{
			this.comparer = comparer;
			this.buckets = new int[7];
			this.slots = new HashLookup<TKey, TValue>.Slot[7];
			this.freeList = -1;
		}

		// Token: 0x06000B7C RID: 2940 RVA: 0x00024C5E File Offset: 0x00022E5E
		internal bool Add(TKey key, TValue value)
		{
			return !this.Find(key, true, false, ref value);
		}

		// Token: 0x06000B7D RID: 2941 RVA: 0x00024C6E File Offset: 0x00022E6E
		internal bool TryGetValue(TKey key, ref TValue value)
		{
			return this.Find(key, false, false, ref value);
		}

		// Token: 0x17000193 RID: 403
		internal TValue this[TKey key]
		{
			set
			{
				TValue tvalue = value;
				this.Find(key, false, true, ref tvalue);
			}
		}

		// Token: 0x06000B7F RID: 2943 RVA: 0x00024C97 File Offset: 0x00022E97
		private int GetKeyHashCode(TKey key)
		{
			return int.MaxValue & ((this.comparer == null) ? ((key == null) ? 0 : key.GetHashCode()) : this.comparer.GetHashCode(key));
		}

		// Token: 0x06000B80 RID: 2944 RVA: 0x00024CD0 File Offset: 0x00022ED0
		private bool AreKeysEqual(TKey key1, TKey key2)
		{
			if (this.comparer != null)
			{
				return this.comparer.Equals(key1, key2);
			}
			return (key1 == null && key2 == null) || (key1 != null && key1.Equals(key2));
		}

		// Token: 0x06000B81 RID: 2945 RVA: 0x00024D24 File Offset: 0x00022F24
		private bool Find(TKey key, bool add, bool set, ref TValue value)
		{
			int keyHashCode = this.GetKeyHashCode(key);
			int i = this.buckets[keyHashCode % this.buckets.Length] - 1;
			while (i >= 0)
			{
				if (this.slots[i].hashCode == keyHashCode && this.AreKeysEqual(this.slots[i].key, key))
				{
					if (set)
					{
						this.slots[i].value = value;
						return true;
					}
					value = this.slots[i].value;
					return true;
				}
				else
				{
					i = this.slots[i].next;
				}
			}
			if (add)
			{
				int num;
				if (this.freeList >= 0)
				{
					num = this.freeList;
					this.freeList = this.slots[num].next;
				}
				else
				{
					if (this.count == this.slots.Length)
					{
						this.Resize();
					}
					num = this.count;
					this.count++;
				}
				int num2 = keyHashCode % this.buckets.Length;
				this.slots[num].hashCode = keyHashCode;
				this.slots[num].key = key;
				this.slots[num].value = value;
				this.slots[num].next = this.buckets[num2] - 1;
				this.buckets[num2] = num + 1;
			}
			return false;
		}

		// Token: 0x06000B82 RID: 2946 RVA: 0x00024E94 File Offset: 0x00023094
		private void Resize()
		{
			int num = checked(this.count * 2 + 1);
			int[] array = new int[num];
			HashLookup<TKey, TValue>.Slot[] array2 = new HashLookup<TKey, TValue>.Slot[num];
			Array.Copy(this.slots, 0, array2, 0, this.count);
			for (int i = 0; i < this.count; i++)
			{
				int num2 = array2[i].hashCode % num;
				array2[i].next = array[num2] - 1;
				array[num2] = i + 1;
			}
			this.buckets = array;
			this.slots = array2;
		}

		// Token: 0x17000194 RID: 404
		// (get) Token: 0x06000B83 RID: 2947 RVA: 0x00024F16 File Offset: 0x00023116
		internal int Count
		{
			get
			{
				return this.count;
			}
		}

		// Token: 0x17000195 RID: 405
		internal KeyValuePair<TKey, TValue> this[int index]
		{
			get
			{
				return new KeyValuePair<TKey, TValue>(this.slots[index].key, this.slots[index].value);
			}
		}

		// Token: 0x040007DF RID: 2015
		private int[] buckets;

		// Token: 0x040007E0 RID: 2016
		private HashLookup<TKey, TValue>.Slot[] slots;

		// Token: 0x040007E1 RID: 2017
		private int count;

		// Token: 0x040007E2 RID: 2018
		private int freeList;

		// Token: 0x040007E3 RID: 2019
		private IEqualityComparer<TKey> comparer;

		// Token: 0x040007E4 RID: 2020
		private const int HashCodeMask = 2147483647;

		// Token: 0x020001D7 RID: 471
		internal struct Slot
		{
			// Token: 0x040007E5 RID: 2021
			internal int hashCode;

			// Token: 0x040007E6 RID: 2022
			internal int next;

			// Token: 0x040007E7 RID: 2023
			internal TKey key;

			// Token: 0x040007E8 RID: 2024
			internal TValue value;
		}
	}
}
