﻿using System;

namespace System.Linq.Parallel
{
	// Token: 0x020001CB RID: 459
	internal abstract class SpoolingTaskBase : QueryTask
	{
		// Token: 0x06000B3C RID: 2876 RVA: 0x00024131 File Offset: 0x00022331
		protected SpoolingTaskBase(int taskIndex, QueryTaskGroupState groupState) : base(taskIndex, groupState)
		{
		}

		// Token: 0x06000B3D RID: 2877 RVA: 0x0002413C File Offset: 0x0002233C
		protected override void Work()
		{
			try
			{
				this.SpoolingWork();
			}
			catch (Exception ex)
			{
				OperationCanceledException ex2 = ex as OperationCanceledException;
				if (ex2 == null || !(ex2.CancellationToken == this._groupState.CancellationState.MergedCancellationToken) || !this._groupState.CancellationState.MergedCancellationToken.IsCancellationRequested)
				{
					this._groupState.CancellationState.InternalCancellationTokenSource.Cancel();
					throw;
				}
			}
			finally
			{
				this.SpoolingFinally();
			}
		}

		// Token: 0x06000B3E RID: 2878
		protected abstract void SpoolingWork();

		// Token: 0x06000B3F RID: 2879 RVA: 0x000039E8 File Offset: 0x00001BE8
		protected virtual void SpoolingFinally()
		{
		}
	}
}
