﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000177 RID: 375
	internal sealed class AnyAllSearchOperator<TInput> : UnaryQueryOperator<TInput, bool>
	{
		// Token: 0x06000A06 RID: 2566 RVA: 0x0001FA08 File Offset: 0x0001DC08
		internal AnyAllSearchOperator(IEnumerable<TInput> child, bool qualification, Func<TInput, bool> predicate) : base(child)
		{
			this._qualification = qualification;
			this._predicate = predicate;
		}

		// Token: 0x06000A07 RID: 2567 RVA: 0x0001FA20 File Offset: 0x0001DC20
		internal bool Aggregate()
		{
			using (IEnumerator<bool> enumerator = this.GetEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered), true))
			{
				while (enumerator.MoveNext())
				{
					if (enumerator.Current == this._qualification)
					{
						return this._qualification;
					}
				}
			}
			return !this._qualification;
		}

		// Token: 0x06000A08 RID: 2568 RVA: 0x0001FA84 File Offset: 0x0001DC84
		internal override QueryResults<bool> Open(QuerySettings settings, bool preferStriping)
		{
			return new UnaryQueryOperator<TInput, bool>.UnaryQueryOperatorResults(base.Child.Open(settings, preferStriping), this, settings, preferStriping);
		}

		// Token: 0x06000A09 RID: 2569 RVA: 0x0001FA9C File Offset: 0x0001DC9C
		internal override void WrapPartitionedStream<TKey>(PartitionedStream<TInput, TKey> inputStream, IPartitionedStreamRecipient<bool> recipient, bool preferStriping, QuerySettings settings)
		{
			Shared<bool> resultFoundFlag = new Shared<bool>(false);
			int partitionCount = inputStream.PartitionCount;
			PartitionedStream<bool, int> partitionedStream = new PartitionedStream<bool, int>(partitionCount, Util.GetDefaultComparer<int>(), OrdinalIndexState.Correct);
			for (int i = 0; i < partitionCount; i++)
			{
				partitionedStream[i] = new AnyAllSearchOperator<TInput>.AnyAllSearchOperatorEnumerator<TKey>(inputStream[i], this._qualification, this._predicate, i, resultFoundFlag, settings.CancellationState.MergedCancellationToken);
			}
			recipient.Receive<int>(partitionedStream);
		}

		// Token: 0x06000A0A RID: 2570 RVA: 0x00003A6B File Offset: 0x00001C6B
		[ExcludeFromCodeCoverage]
		internal override IEnumerable<bool> AsSequentialQuery(CancellationToken token)
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000162 RID: 354
		// (get) Token: 0x06000A0B RID: 2571 RVA: 0x00002285 File Offset: 0x00000485
		internal override bool LimitsParallelism
		{
			get
			{
				return false;
			}
		}

		// Token: 0x040006A3 RID: 1699
		private readonly Func<TInput, bool> _predicate;

		// Token: 0x040006A4 RID: 1700
		private readonly bool _qualification;

		// Token: 0x02000178 RID: 376
		private class AnyAllSearchOperatorEnumerator<TKey> : QueryOperatorEnumerator<bool, int>
		{
			// Token: 0x06000A0C RID: 2572 RVA: 0x0001FB04 File Offset: 0x0001DD04
			internal AnyAllSearchOperatorEnumerator(QueryOperatorEnumerator<TInput, TKey> source, bool qualification, Func<TInput, bool> predicate, int partitionIndex, Shared<bool> resultFoundFlag, CancellationToken cancellationToken)
			{
				this._source = source;
				this._qualification = qualification;
				this._predicate = predicate;
				this._partitionIndex = partitionIndex;
				this._resultFoundFlag = resultFoundFlag;
				this._cancellationToken = cancellationToken;
			}

			// Token: 0x06000A0D RID: 2573 RVA: 0x0001FB3C File Offset: 0x0001DD3C
			internal override bool MoveNext(ref bool currentElement, ref int currentKey)
			{
				if (this._resultFoundFlag.Value)
				{
					return false;
				}
				TInput arg = default(TInput);
				TKey tkey = default(TKey);
				if (this._source.MoveNext(ref arg, ref tkey))
				{
					currentElement = !this._qualification;
					currentKey = this._partitionIndex;
					int num = 0;
					for (;;)
					{
						if ((num++ & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(this._cancellationToken);
						}
						if (this._resultFoundFlag.Value)
						{
							break;
						}
						if (this._predicate(arg) == this._qualification)
						{
							goto Block_5;
						}
						if (!this._source.MoveNext(ref arg, ref tkey))
						{
							return true;
						}
					}
					return false;
					Block_5:
					this._resultFoundFlag.Value = true;
					currentElement = this._qualification;
					return true;
				}
				return false;
			}

			// Token: 0x06000A0E RID: 2574 RVA: 0x0001FBF0 File Offset: 0x0001DDF0
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x040006A5 RID: 1701
			private readonly QueryOperatorEnumerator<TInput, TKey> _source;

			// Token: 0x040006A6 RID: 1702
			private readonly Func<TInput, bool> _predicate;

			// Token: 0x040006A7 RID: 1703
			private readonly bool _qualification;

			// Token: 0x040006A8 RID: 1704
			private readonly int _partitionIndex;

			// Token: 0x040006A9 RID: 1705
			private readonly Shared<bool> _resultFoundFlag;

			// Token: 0x040006AA RID: 1706
			private readonly CancellationToken _cancellationToken;
		}
	}
}
