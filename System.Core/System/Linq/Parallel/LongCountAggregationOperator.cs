﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000140 RID: 320
	internal sealed class LongCountAggregationOperator<TSource> : InlinedAggregationOperator<TSource, long, long>
	{
		// Token: 0x06000918 RID: 2328 RVA: 0x0001CDA5 File Offset: 0x0001AFA5
		internal LongCountAggregationOperator(IEnumerable<TSource> child) : base(child)
		{
		}

		// Token: 0x06000919 RID: 2329 RVA: 0x0001CDB0 File Offset: 0x0001AFB0
		protected override long InternalAggregate(ref Exception singularExceptionToThrow)
		{
			checked
			{
				long result;
				using (IEnumerator<long> enumerator = this.GetEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered), true))
				{
					long num = 0L;
					while (enumerator.MoveNext())
					{
						long num2 = enumerator.Current;
						num += num2;
					}
					result = num;
				}
				return result;
			}
		}

		// Token: 0x0600091A RID: 2330 RVA: 0x0001CE00 File Offset: 0x0001B000
		protected override QueryOperatorEnumerator<long, int> CreateEnumerator<TKey>(int index, int count, QueryOperatorEnumerator<TSource, TKey> source, object sharedData, CancellationToken cancellationToken)
		{
			return new LongCountAggregationOperator<TSource>.LongCountAggregationOperatorEnumerator<TKey>(source, index, cancellationToken);
		}

		// Token: 0x02000141 RID: 321
		private class LongCountAggregationOperatorEnumerator<TKey> : InlinedAggregationOperatorEnumerator<long>
		{
			// Token: 0x0600091B RID: 2331 RVA: 0x0001CE0B File Offset: 0x0001B00B
			internal LongCountAggregationOperatorEnumerator(QueryOperatorEnumerator<TSource, TKey> source, int partitionIndex, CancellationToken cancellationToken) : base(partitionIndex, cancellationToken)
			{
				this._source = source;
			}

			// Token: 0x0600091C RID: 2332 RVA: 0x0001CE1C File Offset: 0x0001B01C
			protected override bool MoveNextCore(ref long currentElement)
			{
				TSource tsource = default(TSource);
				TKey tkey = default(TKey);
				QueryOperatorEnumerator<TSource, TKey> source = this._source;
				if (source.MoveNext(ref tsource, ref tkey))
				{
					long num = 0L;
					int num2 = 0;
					do
					{
						if ((num2++ & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(this._cancellationToken);
						}
						checked
						{
							num += 1L;
						}
					}
					while (source.MoveNext(ref tsource, ref tkey));
					currentElement = num;
					return true;
				}
				return false;
			}

			// Token: 0x0600091D RID: 2333 RVA: 0x0001CE80 File Offset: 0x0001B080
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x0400064F RID: 1615
			private readonly QueryOperatorEnumerator<TSource, TKey> _source;
		}
	}
}
