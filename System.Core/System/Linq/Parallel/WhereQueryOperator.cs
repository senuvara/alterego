﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x020001B5 RID: 437
	internal sealed class WhereQueryOperator<TInputOutput> : UnaryQueryOperator<TInputOutput, TInputOutput>
	{
		// Token: 0x06000AF1 RID: 2801 RVA: 0x00023261 File Offset: 0x00021461
		internal WhereQueryOperator(IEnumerable<TInputOutput> child, Func<TInputOutput, bool> predicate) : base(child)
		{
			base.SetOrdinalIndexState(base.Child.OrdinalIndexState.Worse(OrdinalIndexState.Increasing));
			this._predicate = predicate;
		}

		// Token: 0x06000AF2 RID: 2802 RVA: 0x00023288 File Offset: 0x00021488
		internal override void WrapPartitionedStream<TKey>(PartitionedStream<TInputOutput, TKey> inputStream, IPartitionedStreamRecipient<TInputOutput> recipient, bool preferStriping, QuerySettings settings)
		{
			PartitionedStream<TInputOutput, TKey> partitionedStream = new PartitionedStream<TInputOutput, TKey>(inputStream.PartitionCount, inputStream.KeyComparer, this.OrdinalIndexState);
			for (int i = 0; i < inputStream.PartitionCount; i++)
			{
				partitionedStream[i] = new WhereQueryOperator<TInputOutput>.WhereQueryOperatorEnumerator<TKey>(inputStream[i], this._predicate, settings.CancellationState.MergedCancellationToken);
			}
			recipient.Receive<TKey>(partitionedStream);
		}

		// Token: 0x06000AF3 RID: 2803 RVA: 0x0001FDEF File Offset: 0x0001DFEF
		internal override QueryResults<TInputOutput> Open(QuerySettings settings, bool preferStriping)
		{
			return new UnaryQueryOperator<TInputOutput, TInputOutput>.UnaryQueryOperatorResults(base.Child.Open(settings, preferStriping), this, settings, preferStriping);
		}

		// Token: 0x06000AF4 RID: 2804 RVA: 0x000232EA File Offset: 0x000214EA
		internal override IEnumerable<TInputOutput> AsSequentialQuery(CancellationToken token)
		{
			return CancellableEnumerable.Wrap<TInputOutput>(base.Child.AsSequentialQuery(token), token).Where(this._predicate);
		}

		// Token: 0x17000181 RID: 385
		// (get) Token: 0x06000AF5 RID: 2805 RVA: 0x00002285 File Offset: 0x00000485
		internal override bool LimitsParallelism
		{
			get
			{
				return false;
			}
		}

		// Token: 0x0400076F RID: 1903
		private Func<TInputOutput, bool> _predicate;

		// Token: 0x020001B6 RID: 438
		private class WhereQueryOperatorEnumerator<TKey> : QueryOperatorEnumerator<TInputOutput, TKey>
		{
			// Token: 0x06000AF6 RID: 2806 RVA: 0x00023309 File Offset: 0x00021509
			internal WhereQueryOperatorEnumerator(QueryOperatorEnumerator<TInputOutput, TKey> source, Func<TInputOutput, bool> predicate, CancellationToken cancellationToken)
			{
				this._source = source;
				this._predicate = predicate;
				this._cancellationToken = cancellationToken;
			}

			// Token: 0x06000AF7 RID: 2807 RVA: 0x00023328 File Offset: 0x00021528
			internal override bool MoveNext(ref TInputOutput currentElement, ref TKey currentKey)
			{
				if (this._outputLoopCount == null)
				{
					this._outputLoopCount = new Shared<int>(0);
				}
				while (this._source.MoveNext(ref currentElement, ref currentKey))
				{
					Shared<int> outputLoopCount = this._outputLoopCount;
					int value = outputLoopCount.Value;
					outputLoopCount.Value = value + 1;
					if ((value & 63) == 0)
					{
						CancellationState.ThrowIfCanceled(this._cancellationToken);
					}
					if (this._predicate(currentElement))
					{
						return true;
					}
				}
				return false;
			}

			// Token: 0x06000AF8 RID: 2808 RVA: 0x00023396 File Offset: 0x00021596
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x04000770 RID: 1904
			private readonly QueryOperatorEnumerator<TInputOutput, TKey> _source;

			// Token: 0x04000771 RID: 1905
			private readonly Func<TInputOutput, bool> _predicate;

			// Token: 0x04000772 RID: 1906
			private CancellationToken _cancellationToken;

			// Token: 0x04000773 RID: 1907
			private Shared<int> _outputLoopCount;
		}
	}
}
