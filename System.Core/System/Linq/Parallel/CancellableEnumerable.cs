﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x020001CC RID: 460
	internal static class CancellableEnumerable
	{
		// Token: 0x06000B40 RID: 2880 RVA: 0x000241D0 File Offset: 0x000223D0
		internal static IEnumerable<TElement> Wrap<TElement>(IEnumerable<TElement> source, CancellationToken token)
		{
			int count = 0;
			foreach (TElement telement in source)
			{
				int num = count;
				count = num + 1;
				if ((num & 63) == 0)
				{
					CancellationState.ThrowIfCanceled(token);
				}
				yield return telement;
			}
			IEnumerator<TElement> enumerator = null;
			yield break;
			yield break;
		}

		// Token: 0x020001CD RID: 461
		[CompilerGenerated]
		private sealed class <Wrap>d__0<TElement> : IEnumerable<!0>, IEnumerable, IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x06000B41 RID: 2881 RVA: 0x000241E7 File Offset: 0x000223E7
			[DebuggerHidden]
			public <Wrap>d__0(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000B42 RID: 2882 RVA: 0x00024204 File Offset: 0x00022404
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x06000B43 RID: 2883 RVA: 0x0002423C File Offset: 0x0002243C
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						count = 0;
						enumerator = source.GetEnumerator();
						this.<>1__state = -3;
					}
					if (!enumerator.MoveNext())
					{
						this.<>m__Finally1();
						enumerator = null;
						result = false;
					}
					else
					{
						TElement telement = enumerator.Current;
						int num2 = count;
						count = num2 + 1;
						if ((num2 & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(token);
						}
						this.<>2__current = telement;
						this.<>1__state = 1;
						result = true;
					}
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x06000B44 RID: 2884 RVA: 0x00024304 File Offset: 0x00022504
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x17000188 RID: 392
			// (get) Token: 0x06000B45 RID: 2885 RVA: 0x00024320 File Offset: 0x00022520
			TElement IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000B46 RID: 2886 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000189 RID: 393
			// (get) Token: 0x06000B47 RID: 2887 RVA: 0x00024328 File Offset: 0x00022528
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000B48 RID: 2888 RVA: 0x00024338 File Offset: 0x00022538
			[DebuggerHidden]
			IEnumerator<TElement> IEnumerable<!0>.GetEnumerator()
			{
				CancellableEnumerable.<Wrap>d__0<TElement> <Wrap>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<Wrap>d__ = this;
				}
				else
				{
					<Wrap>d__ = new CancellableEnumerable.<Wrap>d__0<TElement>(0);
				}
				<Wrap>d__.source = source;
				<Wrap>d__.token = token;
				return <Wrap>d__;
			}

			// Token: 0x06000B49 RID: 2889 RVA: 0x00024387 File Offset: 0x00022587
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TElement>.GetEnumerator();
			}

			// Token: 0x040007BD RID: 1981
			private int <>1__state;

			// Token: 0x040007BE RID: 1982
			private TElement <>2__current;

			// Token: 0x040007BF RID: 1983
			private int <>l__initialThreadId;

			// Token: 0x040007C0 RID: 1984
			private IEnumerable<TElement> source;

			// Token: 0x040007C1 RID: 1985
			public IEnumerable<TElement> <>3__source;

			// Token: 0x040007C2 RID: 1986
			private int <count>5__1;

			// Token: 0x040007C3 RID: 1987
			private CancellationToken token;

			// Token: 0x040007C4 RID: 1988
			public CancellationToken <>3__token;

			// Token: 0x040007C5 RID: 1989
			private IEnumerator<TElement> <>7__wrap1;
		}
	}
}
