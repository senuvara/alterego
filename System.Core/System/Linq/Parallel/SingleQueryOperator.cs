﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x020001A8 RID: 424
	internal sealed class SingleQueryOperator<TSource> : UnaryQueryOperator<TSource, TSource>
	{
		// Token: 0x06000ABF RID: 2751 RVA: 0x000222FF File Offset: 0x000204FF
		internal SingleQueryOperator(IEnumerable<TSource> child, Func<TSource, bool> predicate) : base(child)
		{
			this._predicate = predicate;
		}

		// Token: 0x06000AC0 RID: 2752 RVA: 0x00020300 File Offset: 0x0001E500
		internal override QueryResults<TSource> Open(QuerySettings settings, bool preferStriping)
		{
			return new UnaryQueryOperator<TSource, TSource>.UnaryQueryOperatorResults(base.Child.Open(settings, false), this, settings, preferStriping);
		}

		// Token: 0x06000AC1 RID: 2753 RVA: 0x00022310 File Offset: 0x00020510
		internal override void WrapPartitionedStream<TKey>(PartitionedStream<TSource, TKey> inputStream, IPartitionedStreamRecipient<TSource> recipient, bool preferStriping, QuerySettings settings)
		{
			int partitionCount = inputStream.PartitionCount;
			PartitionedStream<TSource, int> partitionedStream = new PartitionedStream<TSource, int>(partitionCount, Util.GetDefaultComparer<int>(), OrdinalIndexState.Shuffled);
			Shared<int> totalElementCount = new Shared<int>(0);
			for (int i = 0; i < partitionCount; i++)
			{
				partitionedStream[i] = new SingleQueryOperator<TSource>.SingleQueryOperatorEnumerator<TKey>(inputStream[i], this._predicate, totalElementCount);
			}
			recipient.Receive<int>(partitionedStream);
		}

		// Token: 0x06000AC2 RID: 2754 RVA: 0x00003A6B File Offset: 0x00001C6B
		[ExcludeFromCodeCoverage]
		internal override IEnumerable<TSource> AsSequentialQuery(CancellationToken token)
		{
			throw new NotSupportedException();
		}

		// Token: 0x1700017A RID: 378
		// (get) Token: 0x06000AC3 RID: 2755 RVA: 0x00002285 File Offset: 0x00000485
		internal override bool LimitsParallelism
		{
			get
			{
				return false;
			}
		}

		// Token: 0x0400073C RID: 1852
		private readonly Func<TSource, bool> _predicate;

		// Token: 0x020001A9 RID: 425
		private class SingleQueryOperatorEnumerator<TKey> : QueryOperatorEnumerator<TSource, int>
		{
			// Token: 0x06000AC4 RID: 2756 RVA: 0x00022365 File Offset: 0x00020565
			internal SingleQueryOperatorEnumerator(QueryOperatorEnumerator<TSource, TKey> source, Func<TSource, bool> predicate, Shared<int> totalElementCount)
			{
				this._source = source;
				this._predicate = predicate;
				this._totalElementCount = totalElementCount;
			}

			// Token: 0x06000AC5 RID: 2757 RVA: 0x00022384 File Offset: 0x00020584
			internal override bool MoveNext(ref TSource currentElement, ref int currentKey)
			{
				if (!this._alreadySearched)
				{
					bool flag = false;
					TSource tsource = default(TSource);
					TKey tkey = default(TKey);
					while (this._source.MoveNext(ref tsource, ref tkey))
					{
						if (this._predicate == null || this._predicate(tsource))
						{
							Interlocked.Increment(ref this._totalElementCount.Value);
							currentElement = tsource;
							currentKey = 0;
							if (flag)
							{
								this._yieldExtra = true;
								break;
							}
							flag = true;
						}
						if (Volatile.Read(ref this._totalElementCount.Value) > 1)
						{
							break;
						}
					}
					this._alreadySearched = true;
					return flag;
				}
				if (this._yieldExtra)
				{
					this._yieldExtra = false;
					currentElement = default(TSource);
					currentKey = 0;
					return true;
				}
				return false;
			}

			// Token: 0x06000AC6 RID: 2758 RVA: 0x00022435 File Offset: 0x00020635
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x0400073D RID: 1853
			private QueryOperatorEnumerator<TSource, TKey> _source;

			// Token: 0x0400073E RID: 1854
			private Func<TSource, bool> _predicate;

			// Token: 0x0400073F RID: 1855
			private bool _alreadySearched;

			// Token: 0x04000740 RID: 1856
			private bool _yieldExtra;

			// Token: 0x04000741 RID: 1857
			private Shared<int> _totalElementCount;
		}
	}
}
