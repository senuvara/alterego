﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x020000DD RID: 221
	internal class ParallelEnumerableWrapper : ParallelQuery<object>
	{
		// Token: 0x060007C9 RID: 1993 RVA: 0x0001726C File Offset: 0x0001546C
		internal ParallelEnumerableWrapper(IEnumerable source) : base(QuerySettings.Empty)
		{
			this._source = source;
		}

		// Token: 0x060007CA RID: 1994 RVA: 0x00017280 File Offset: 0x00015480
		internal override IEnumerator GetEnumeratorUntyped()
		{
			return this._source.GetEnumerator();
		}

		// Token: 0x060007CB RID: 1995 RVA: 0x0001728D File Offset: 0x0001548D
		public override IEnumerator<object> GetEnumerator()
		{
			return new EnumerableWrapperWeakToStrong(this._source).GetEnumerator();
		}

		// Token: 0x04000523 RID: 1315
		private readonly IEnumerable _source;
	}
}
