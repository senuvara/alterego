﻿using System;

namespace System.Linq.Parallel
{
	// Token: 0x020001CA RID: 458
	internal class ForAllSpoolingTask<TInputOutput, TIgnoreKey> : SpoolingTaskBase
	{
		// Token: 0x06000B39 RID: 2873 RVA: 0x000240DF File Offset: 0x000222DF
		internal ForAllSpoolingTask(int taskIndex, QueryTaskGroupState groupState, QueryOperatorEnumerator<TInputOutput, TIgnoreKey> source) : base(taskIndex, groupState)
		{
			this._source = source;
		}

		// Token: 0x06000B3A RID: 2874 RVA: 0x000240F0 File Offset: 0x000222F0
		protected override void SpoolingWork()
		{
			TInputOutput tinputOutput = default(TInputOutput);
			TIgnoreKey tignoreKey = default(TIgnoreKey);
			while (this._source.MoveNext(ref tinputOutput, ref tignoreKey))
			{
			}
		}

		// Token: 0x06000B3B RID: 2875 RVA: 0x0002411E File Offset: 0x0002231E
		protected override void SpoolingFinally()
		{
			base.SpoolingFinally();
			this._source.Dispose();
		}

		// Token: 0x040007BC RID: 1980
		private QueryOperatorEnumerator<TInputOutput, TIgnoreKey> _source;
	}
}
