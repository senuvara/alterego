﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x020001AF RID: 431
	internal sealed class TakeOrSkipQueryOperator<TResult> : UnaryQueryOperator<TResult, TResult>
	{
		// Token: 0x06000AD7 RID: 2775 RVA: 0x00022680 File Offset: 0x00020880
		internal TakeOrSkipQueryOperator(IEnumerable<TResult> child, int count, bool take) : base(child)
		{
			this._count = count;
			this._take = take;
			base.SetOrdinalIndexState(this.OutputOrdinalIndexState());
		}

		// Token: 0x06000AD8 RID: 2776 RVA: 0x000226A4 File Offset: 0x000208A4
		private OrdinalIndexState OutputOrdinalIndexState()
		{
			OrdinalIndexState ordinalIndexState = base.Child.OrdinalIndexState;
			if (ordinalIndexState == OrdinalIndexState.Indexable)
			{
				return OrdinalIndexState.Indexable;
			}
			if (ordinalIndexState.IsWorseThan(OrdinalIndexState.Increasing))
			{
				this._prematureMerge = true;
				ordinalIndexState = OrdinalIndexState.Correct;
			}
			if (!this._take && ordinalIndexState == OrdinalIndexState.Correct)
			{
				ordinalIndexState = OrdinalIndexState.Increasing;
			}
			return ordinalIndexState;
		}

		// Token: 0x06000AD9 RID: 2777 RVA: 0x000226E4 File Offset: 0x000208E4
		internal override void WrapPartitionedStream<TKey>(PartitionedStream<TResult, TKey> inputStream, IPartitionedStreamRecipient<TResult> recipient, bool preferStriping, QuerySettings settings)
		{
			if (this._prematureMerge)
			{
				PartitionedStream<TResult, int> partitionedStream = QueryOperator<TResult>.ExecuteAndCollectResults<TKey>(inputStream, inputStream.PartitionCount, base.Child.OutputOrdered, preferStriping, settings).GetPartitionedStream();
				this.WrapHelper<int>(partitionedStream, recipient, settings);
				return;
			}
			this.WrapHelper<TKey>(inputStream, recipient, settings);
		}

		// Token: 0x06000ADA RID: 2778 RVA: 0x00022730 File Offset: 0x00020930
		private void WrapHelper<TKey>(PartitionedStream<TResult, TKey> inputStream, IPartitionedStreamRecipient<TResult> recipient, QuerySettings settings)
		{
			int partitionCount = inputStream.PartitionCount;
			FixedMaxHeap<TKey> sharedIndices = new FixedMaxHeap<TKey>(this._count, inputStream.KeyComparer);
			CountdownEvent sharedBarrier = new CountdownEvent(partitionCount);
			PartitionedStream<TResult, TKey> partitionedStream = new PartitionedStream<TResult, TKey>(partitionCount, inputStream.KeyComparer, this.OrdinalIndexState);
			for (int i = 0; i < partitionCount; i++)
			{
				partitionedStream[i] = new TakeOrSkipQueryOperator<TResult>.TakeOrSkipQueryOperatorEnumerator<TKey>(inputStream[i], this._take, sharedIndices, sharedBarrier, settings.CancellationState.MergedCancellationToken, inputStream.KeyComparer);
			}
			recipient.Receive<TKey>(partitionedStream);
		}

		// Token: 0x06000ADB RID: 2779 RVA: 0x000227B6 File Offset: 0x000209B6
		internal override QueryResults<TResult> Open(QuerySettings settings, bool preferStriping)
		{
			return TakeOrSkipQueryOperator<TResult>.TakeOrSkipQueryOperatorResults.NewResults(base.Child.Open(settings, true), this, settings, preferStriping);
		}

		// Token: 0x1700017D RID: 381
		// (get) Token: 0x06000ADC RID: 2780 RVA: 0x00002285 File Offset: 0x00000485
		internal override bool LimitsParallelism
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06000ADD RID: 2781 RVA: 0x000227CD File Offset: 0x000209CD
		internal override IEnumerable<TResult> AsSequentialQuery(CancellationToken token)
		{
			if (this._take)
			{
				return base.Child.AsSequentialQuery(token).Take(this._count);
			}
			return CancellableEnumerable.Wrap<TResult>(base.Child.AsSequentialQuery(token), token).Skip(this._count);
		}

		// Token: 0x0400074E RID: 1870
		private readonly int _count;

		// Token: 0x0400074F RID: 1871
		private readonly bool _take;

		// Token: 0x04000750 RID: 1872
		private bool _prematureMerge;

		// Token: 0x020001B0 RID: 432
		private class TakeOrSkipQueryOperatorEnumerator<TKey> : QueryOperatorEnumerator<TResult, TKey>
		{
			// Token: 0x06000ADE RID: 2782 RVA: 0x0002280C File Offset: 0x00020A0C
			internal TakeOrSkipQueryOperatorEnumerator(QueryOperatorEnumerator<TResult, TKey> source, bool take, FixedMaxHeap<TKey> sharedIndices, CountdownEvent sharedBarrier, CancellationToken cancellationToken, IComparer<TKey> keyComparer)
			{
				this._source = source;
				this._count = sharedIndices.Size;
				this._take = take;
				this._sharedIndices = sharedIndices;
				this._sharedBarrier = sharedBarrier;
				this._cancellationToken = cancellationToken;
				this._keyComparer = keyComparer;
			}

			// Token: 0x06000ADF RID: 2783 RVA: 0x00022858 File Offset: 0x00020A58
			internal override bool MoveNext(ref TResult currentElement, ref TKey currentKey)
			{
				if (this._buffer == null && this._count > 0)
				{
					List<Pair<TResult, TKey>> list = new List<Pair<TResult, TKey>>();
					TResult first = default(TResult);
					TKey tkey = default(TKey);
					int num = 0;
					while (list.Count < this._count && this._source.MoveNext(ref first, ref tkey))
					{
						if ((num++ & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(this._cancellationToken);
						}
						list.Add(new Pair<TResult, TKey>(first, tkey));
						FixedMaxHeap<TKey> sharedIndices = this._sharedIndices;
						lock (sharedIndices)
						{
							if (!this._sharedIndices.Insert(tkey))
							{
								break;
							}
						}
					}
					this._sharedBarrier.Signal();
					this._sharedBarrier.Wait(this._cancellationToken);
					this._buffer = list;
					this._bufferIndex = new Shared<int>(-1);
				}
				if (!this._take)
				{
					TKey y = default(TKey);
					if (this._count > 0)
					{
						if (this._sharedIndices.Count < this._count)
						{
							return false;
						}
						y = this._sharedIndices.MaxValue;
						if (this._bufferIndex.Value < this._buffer.Count - 1)
						{
							this._bufferIndex.Value++;
							while (this._bufferIndex.Value < this._buffer.Count)
							{
								if (this._keyComparer.Compare(this._buffer[this._bufferIndex.Value].Second, y) > 0)
								{
									currentElement = this._buffer[this._bufferIndex.Value].First;
									currentKey = this._buffer[this._bufferIndex.Value].Second;
									return true;
								}
								this._bufferIndex.Value++;
							}
						}
					}
					return this._source.MoveNext(ref currentElement, ref currentKey);
				}
				if (this._count == 0 || this._bufferIndex.Value >= this._buffer.Count - 1)
				{
					return false;
				}
				this._bufferIndex.Value++;
				currentElement = this._buffer[this._bufferIndex.Value].First;
				currentKey = this._buffer[this._bufferIndex.Value].Second;
				return this._sharedIndices.Count == 0 || this._keyComparer.Compare(this._buffer[this._bufferIndex.Value].Second, this._sharedIndices.MaxValue) <= 0;
			}

			// Token: 0x06000AE0 RID: 2784 RVA: 0x00022B48 File Offset: 0x00020D48
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x04000751 RID: 1873
			private readonly QueryOperatorEnumerator<TResult, TKey> _source;

			// Token: 0x04000752 RID: 1874
			private readonly int _count;

			// Token: 0x04000753 RID: 1875
			private readonly bool _take;

			// Token: 0x04000754 RID: 1876
			private readonly IComparer<TKey> _keyComparer;

			// Token: 0x04000755 RID: 1877
			private readonly FixedMaxHeap<TKey> _sharedIndices;

			// Token: 0x04000756 RID: 1878
			private readonly CountdownEvent _sharedBarrier;

			// Token: 0x04000757 RID: 1879
			private readonly CancellationToken _cancellationToken;

			// Token: 0x04000758 RID: 1880
			private List<Pair<TResult, TKey>> _buffer;

			// Token: 0x04000759 RID: 1881
			private Shared<int> _bufferIndex;
		}

		// Token: 0x020001B1 RID: 433
		private class TakeOrSkipQueryOperatorResults : UnaryQueryOperator<TResult, TResult>.UnaryQueryOperatorResults
		{
			// Token: 0x06000AE1 RID: 2785 RVA: 0x00022B55 File Offset: 0x00020D55
			public static QueryResults<TResult> NewResults(QueryResults<TResult> childQueryResults, TakeOrSkipQueryOperator<TResult> op, QuerySettings settings, bool preferStriping)
			{
				if (childQueryResults.IsIndexible)
				{
					return new TakeOrSkipQueryOperator<TResult>.TakeOrSkipQueryOperatorResults(childQueryResults, op, settings, preferStriping);
				}
				return new UnaryQueryOperator<TResult, TResult>.UnaryQueryOperatorResults(childQueryResults, op, settings, preferStriping);
			}

			// Token: 0x06000AE2 RID: 2786 RVA: 0x00022B72 File Offset: 0x00020D72
			private TakeOrSkipQueryOperatorResults(QueryResults<TResult> childQueryResults, TakeOrSkipQueryOperator<TResult> takeOrSkipOp, QuerySettings settings, bool preferStriping) : base(childQueryResults, takeOrSkipOp, settings, preferStriping)
			{
				this._takeOrSkipOp = takeOrSkipOp;
				this._childCount = this._childQueryResults.ElementsCount;
			}

			// Token: 0x1700017E RID: 382
			// (get) Token: 0x06000AE3 RID: 2787 RVA: 0x00022B97 File Offset: 0x00020D97
			internal override bool IsIndexible
			{
				get
				{
					return this._childCount >= 0;
				}
			}

			// Token: 0x1700017F RID: 383
			// (get) Token: 0x06000AE4 RID: 2788 RVA: 0x00022BA5 File Offset: 0x00020DA5
			internal override int ElementsCount
			{
				get
				{
					if (this._takeOrSkipOp._take)
					{
						return Math.Min(this._childCount, this._takeOrSkipOp._count);
					}
					return Math.Max(this._childCount - this._takeOrSkipOp._count, 0);
				}
			}

			// Token: 0x06000AE5 RID: 2789 RVA: 0x00022BE3 File Offset: 0x00020DE3
			internal override TResult GetElement(int index)
			{
				if (this._takeOrSkipOp._take)
				{
					return this._childQueryResults.GetElement(index);
				}
				return this._childQueryResults.GetElement(this._takeOrSkipOp._count + index);
			}

			// Token: 0x0400075A RID: 1882
			private TakeOrSkipQueryOperator<TResult> _takeOrSkipOp;

			// Token: 0x0400075B RID: 1883
			private int _childCount;
		}
	}
}
