﻿using System;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x02000164 RID: 356
	internal class ListQueryResults<T> : QueryResults<T>
	{
		// Token: 0x06000984 RID: 2436 RVA: 0x0001E977 File Offset: 0x0001CB77
		internal ListQueryResults(IList<T> source, int partitionCount, bool useStriping)
		{
			this._source = source;
			this._partitionCount = partitionCount;
			this._useStriping = useStriping;
		}

		// Token: 0x06000985 RID: 2437 RVA: 0x0001E994 File Offset: 0x0001CB94
		internal override void GivePartitionedStream(IPartitionedStreamRecipient<T> recipient)
		{
			PartitionedStream<T, int> partitionedStream = this.GetPartitionedStream();
			recipient.Receive<int>(partitionedStream);
		}

		// Token: 0x1700013E RID: 318
		// (get) Token: 0x06000986 RID: 2438 RVA: 0x00009CDF File Offset: 0x00007EDF
		internal override bool IsIndexible
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700013F RID: 319
		// (get) Token: 0x06000987 RID: 2439 RVA: 0x0001E9AF File Offset: 0x0001CBAF
		internal override int ElementsCount
		{
			get
			{
				return this._source.Count;
			}
		}

		// Token: 0x06000988 RID: 2440 RVA: 0x0001E9BC File Offset: 0x0001CBBC
		internal override T GetElement(int index)
		{
			return this._source[index];
		}

		// Token: 0x06000989 RID: 2441 RVA: 0x0001E9CA File Offset: 0x0001CBCA
		internal PartitionedStream<T, int> GetPartitionedStream()
		{
			return ExchangeUtilities.PartitionDataSource<T>(this._source, this._partitionCount, this._useStriping);
		}

		// Token: 0x0400066D RID: 1645
		private IList<T> _source;

		// Token: 0x0400066E RID: 1646
		private int _partitionCount;

		// Token: 0x0400066F RID: 1647
		private bool _useStriping;
	}
}
