﻿using System;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x020001E1 RID: 481
	internal class ReverseComparer<T> : IComparer<T>
	{
		// Token: 0x06000BB4 RID: 2996 RVA: 0x00025545 File Offset: 0x00023745
		internal ReverseComparer(IComparer<T> comparer)
		{
			this._comparer = comparer;
		}

		// Token: 0x06000BB5 RID: 2997 RVA: 0x00025554 File Offset: 0x00023754
		public int Compare(T x, T y)
		{
			return this._comparer.Compare(y, x);
		}

		// Token: 0x04000808 RID: 2056
		private IComparer<T> _comparer;
	}
}
