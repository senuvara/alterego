﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000169 RID: 361
	internal class PartitionerQueryOperator<TElement> : QueryOperator<TElement>
	{
		// Token: 0x06000998 RID: 2456 RVA: 0x0001EB43 File Offset: 0x0001CD43
		internal PartitionerQueryOperator(Partitioner<TElement> partitioner) : base(false, QuerySettings.Empty)
		{
			this._partitioner = partitioner;
		}

		// Token: 0x17000145 RID: 325
		// (get) Token: 0x06000999 RID: 2457 RVA: 0x0001EB58 File Offset: 0x0001CD58
		internal bool Orderable
		{
			get
			{
				return this._partitioner is OrderablePartitioner<TElement>;
			}
		}

		// Token: 0x0600099A RID: 2458 RVA: 0x0001EB68 File Offset: 0x0001CD68
		internal override QueryResults<TElement> Open(QuerySettings settings, bool preferStriping)
		{
			return new PartitionerQueryOperator<TElement>.PartitionerQueryOperatorResults(this._partitioner, settings);
		}

		// Token: 0x0600099B RID: 2459 RVA: 0x0001EB76 File Offset: 0x0001CD76
		internal override IEnumerable<TElement> AsSequentialQuery(CancellationToken token)
		{
			using (IEnumerator<TElement> enumerator = this._partitioner.GetPartitions(1)[0])
			{
				while (enumerator.MoveNext())
				{
					!0 ! = enumerator.Current;
					yield return !;
				}
			}
			IEnumerator<TElement> enumerator = null;
			yield break;
			yield break;
		}

		// Token: 0x17000146 RID: 326
		// (get) Token: 0x0600099C RID: 2460 RVA: 0x0001EB86 File Offset: 0x0001CD86
		internal override OrdinalIndexState OrdinalIndexState
		{
			get
			{
				return PartitionerQueryOperator<TElement>.GetOrdinalIndexState(this._partitioner);
			}
		}

		// Token: 0x0600099D RID: 2461 RVA: 0x0001EB94 File Offset: 0x0001CD94
		internal static OrdinalIndexState GetOrdinalIndexState(Partitioner<TElement> partitioner)
		{
			OrderablePartitioner<TElement> orderablePartitioner = partitioner as OrderablePartitioner<TElement>;
			if (orderablePartitioner == null)
			{
				return OrdinalIndexState.Shuffled;
			}
			if (!orderablePartitioner.KeysOrderedInEachPartition)
			{
				return OrdinalIndexState.Shuffled;
			}
			if (orderablePartitioner.KeysNormalized)
			{
				return OrdinalIndexState.Correct;
			}
			return OrdinalIndexState.Increasing;
		}

		// Token: 0x17000147 RID: 327
		// (get) Token: 0x0600099E RID: 2462 RVA: 0x00002285 File Offset: 0x00000485
		internal override bool LimitsParallelism
		{
			get
			{
				return false;
			}
		}

		// Token: 0x04000680 RID: 1664
		private Partitioner<TElement> _partitioner;

		// Token: 0x0200016A RID: 362
		private class PartitionerQueryOperatorResults : QueryResults<TElement>
		{
			// Token: 0x0600099F RID: 2463 RVA: 0x0001EBC2 File Offset: 0x0001CDC2
			internal PartitionerQueryOperatorResults(Partitioner<TElement> partitioner, QuerySettings settings)
			{
				this._partitioner = partitioner;
				this._settings = settings;
			}

			// Token: 0x060009A0 RID: 2464 RVA: 0x0001EBD8 File Offset: 0x0001CDD8
			internal override void GivePartitionedStream(IPartitionedStreamRecipient<TElement> recipient)
			{
				int value = this._settings.DegreeOfParallelism.Value;
				OrderablePartitioner<TElement> orderablePartitioner = this._partitioner as OrderablePartitioner<TElement>;
				OrdinalIndexState indexState = (orderablePartitioner != null) ? PartitionerQueryOperator<TElement>.GetOrdinalIndexState(orderablePartitioner) : OrdinalIndexState.Shuffled;
				PartitionedStream<TElement, int> partitionedStream = new PartitionedStream<TElement, int>(value, Util.GetDefaultComparer<int>(), indexState);
				if (orderablePartitioner != null)
				{
					IList<IEnumerator<KeyValuePair<long, TElement>>> orderablePartitions = orderablePartitioner.GetOrderablePartitions(value);
					if (orderablePartitions == null)
					{
						throw new InvalidOperationException("Partitioner returned null instead of a list of partitions.");
					}
					if (orderablePartitions.Count != value)
					{
						throw new InvalidOperationException("Partitioner returned a wrong number of partitions.");
					}
					for (int i = 0; i < value; i++)
					{
						IEnumerator<KeyValuePair<long, TElement>> enumerator = orderablePartitions[i];
						if (enumerator == null)
						{
							throw new InvalidOperationException("Partitioner returned a null partition.");
						}
						partitionedStream[i] = new PartitionerQueryOperator<TElement>.OrderablePartitionerEnumerator(enumerator);
					}
				}
				else
				{
					IList<IEnumerator<TElement>> partitions = this._partitioner.GetPartitions(value);
					if (partitions == null)
					{
						throw new InvalidOperationException("Partitioner returned null instead of a list of partitions.");
					}
					if (partitions.Count != value)
					{
						throw new InvalidOperationException("Partitioner returned a wrong number of partitions.");
					}
					for (int j = 0; j < value; j++)
					{
						IEnumerator<TElement> enumerator2 = partitions[j];
						if (enumerator2 == null)
						{
							throw new InvalidOperationException("Partitioner returned a null partition.");
						}
						partitionedStream[j] = new PartitionerQueryOperator<TElement>.PartitionerEnumerator(enumerator2);
					}
				}
				recipient.Receive<int>(partitionedStream);
			}

			// Token: 0x04000681 RID: 1665
			private Partitioner<TElement> _partitioner;

			// Token: 0x04000682 RID: 1666
			private QuerySettings _settings;
		}

		// Token: 0x0200016B RID: 363
		private class OrderablePartitionerEnumerator : QueryOperatorEnumerator<TElement, int>
		{
			// Token: 0x060009A1 RID: 2465 RVA: 0x0001ECFD File Offset: 0x0001CEFD
			internal OrderablePartitionerEnumerator(IEnumerator<KeyValuePair<long, TElement>> sourceEnumerator)
			{
				this._sourceEnumerator = sourceEnumerator;
			}

			// Token: 0x060009A2 RID: 2466 RVA: 0x0001ED0C File Offset: 0x0001CF0C
			internal override bool MoveNext(ref TElement currentElement, ref int currentKey)
			{
				if (!this._sourceEnumerator.MoveNext())
				{
					return false;
				}
				KeyValuePair<long, TElement> keyValuePair = this._sourceEnumerator.Current;
				currentElement = keyValuePair.Value;
				currentKey = checked((int)keyValuePair.Key);
				return true;
			}

			// Token: 0x060009A3 RID: 2467 RVA: 0x0001ED4C File Offset: 0x0001CF4C
			protected override void Dispose(bool disposing)
			{
				this._sourceEnumerator.Dispose();
			}

			// Token: 0x04000683 RID: 1667
			private IEnumerator<KeyValuePair<long, TElement>> _sourceEnumerator;
		}

		// Token: 0x0200016C RID: 364
		private class PartitionerEnumerator : QueryOperatorEnumerator<TElement, int>
		{
			// Token: 0x060009A4 RID: 2468 RVA: 0x0001ED59 File Offset: 0x0001CF59
			internal PartitionerEnumerator(IEnumerator<TElement> sourceEnumerator)
			{
				this._sourceEnumerator = sourceEnumerator;
			}

			// Token: 0x060009A5 RID: 2469 RVA: 0x0001ED68 File Offset: 0x0001CF68
			internal override bool MoveNext(ref TElement currentElement, ref int currentKey)
			{
				if (!this._sourceEnumerator.MoveNext())
				{
					return false;
				}
				currentElement = this._sourceEnumerator.Current;
				currentKey = 0;
				return true;
			}

			// Token: 0x060009A6 RID: 2470 RVA: 0x0001ED8E File Offset: 0x0001CF8E
			protected override void Dispose(bool disposing)
			{
				this._sourceEnumerator.Dispose();
			}

			// Token: 0x04000684 RID: 1668
			private IEnumerator<TElement> _sourceEnumerator;
		}

		// Token: 0x0200016D RID: 365
		[CompilerGenerated]
		private sealed class <AsSequentialQuery>d__5 : IEnumerable<TElement>, IEnumerable, IEnumerator<TElement>, IDisposable, IEnumerator
		{
			// Token: 0x060009A7 RID: 2471 RVA: 0x0001ED9B File Offset: 0x0001CF9B
			[DebuggerHidden]
			public <AsSequentialQuery>d__5(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060009A8 RID: 2472 RVA: 0x0001EDB8 File Offset: 0x0001CFB8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x060009A9 RID: 2473 RVA: 0x0001EDF0 File Offset: 0x0001CFF0
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					PartitionerQueryOperator<TElement> partitionerQueryOperator = this;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						enumerator = partitionerQueryOperator._partitioner.GetPartitions(1)[0];
						this.<>1__state = -3;
					}
					if (!enumerator.MoveNext())
					{
						this.<>m__Finally1();
						enumerator = null;
						result = false;
					}
					else
					{
						this.<>2__current = enumerator.Current;
						this.<>1__state = 1;
						result = true;
					}
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x060009AA RID: 2474 RVA: 0x0001EE9C File Offset: 0x0001D09C
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x17000148 RID: 328
			// (get) Token: 0x060009AB RID: 2475 RVA: 0x0001EEB8 File Offset: 0x0001D0B8
			TElement IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060009AC RID: 2476 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000149 RID: 329
			// (get) Token: 0x060009AD RID: 2477 RVA: 0x0001EEC0 File Offset: 0x0001D0C0
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060009AE RID: 2478 RVA: 0x0001EED0 File Offset: 0x0001D0D0
			[DebuggerHidden]
			IEnumerator<TElement> IEnumerable<!0>.GetEnumerator()
			{
				PartitionerQueryOperator<TElement>.<AsSequentialQuery>d__5 <AsSequentialQuery>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<AsSequentialQuery>d__ = this;
				}
				else
				{
					<AsSequentialQuery>d__ = new PartitionerQueryOperator<TElement>.<AsSequentialQuery>d__5(0);
					<AsSequentialQuery>d__.<>4__this = this;
				}
				return <AsSequentialQuery>d__;
			}

			// Token: 0x060009AF RID: 2479 RVA: 0x0001EF13 File Offset: 0x0001D113
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TElement>.GetEnumerator();
			}

			// Token: 0x04000685 RID: 1669
			private int <>1__state;

			// Token: 0x04000686 RID: 1670
			private TElement <>2__current;

			// Token: 0x04000687 RID: 1671
			private int <>l__initialThreadId;

			// Token: 0x04000688 RID: 1672
			public PartitionerQueryOperator<TElement> <>4__this;

			// Token: 0x04000689 RID: 1673
			private IEnumerator<TElement> <enumerator>5__1;
		}
	}
}
