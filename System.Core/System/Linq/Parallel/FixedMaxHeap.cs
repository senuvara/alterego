﻿using System;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x020001D4 RID: 468
	internal class FixedMaxHeap<TElement>
	{
		// Token: 0x06000B68 RID: 2920 RVA: 0x000248A1 File Offset: 0x00022AA1
		internal FixedMaxHeap(int maximumSize) : this(maximumSize, Util.GetDefaultComparer<TElement>())
		{
		}

		// Token: 0x06000B69 RID: 2921 RVA: 0x000248AF File Offset: 0x00022AAF
		internal FixedMaxHeap(int maximumSize, IComparer<TElement> comparer)
		{
			this._elements = new TElement[maximumSize];
			this._comparer = comparer;
		}

		// Token: 0x1700018E RID: 398
		// (get) Token: 0x06000B6A RID: 2922 RVA: 0x000248CA File Offset: 0x00022ACA
		internal int Count
		{
			get
			{
				return this._count;
			}
		}

		// Token: 0x1700018F RID: 399
		// (get) Token: 0x06000B6B RID: 2923 RVA: 0x000248D2 File Offset: 0x00022AD2
		internal int Size
		{
			get
			{
				return this._elements.Length;
			}
		}

		// Token: 0x17000190 RID: 400
		// (get) Token: 0x06000B6C RID: 2924 RVA: 0x000248DC File Offset: 0x00022ADC
		internal TElement MaxValue
		{
			get
			{
				if (this._count == 0)
				{
					throw new InvalidOperationException("Sequence contains no elements");
				}
				return this._elements[0];
			}
		}

		// Token: 0x06000B6D RID: 2925 RVA: 0x000248FD File Offset: 0x00022AFD
		internal void Clear()
		{
			this._count = 0;
		}

		// Token: 0x06000B6E RID: 2926 RVA: 0x00024908 File Offset: 0x00022B08
		internal bool Insert(TElement e)
		{
			if (this._count < this._elements.Length)
			{
				this._elements[this._count] = e;
				this._count++;
				this.HeapifyLastLeaf();
				return true;
			}
			if (this._comparer.Compare(e, this._elements[0]) < 0)
			{
				this._elements[0] = e;
				this.HeapifyRoot();
				return true;
			}
			return false;
		}

		// Token: 0x06000B6F RID: 2927 RVA: 0x0002497E File Offset: 0x00022B7E
		internal void ReplaceMax(TElement newValue)
		{
			this._elements[0] = newValue;
			this.HeapifyRoot();
		}

		// Token: 0x06000B70 RID: 2928 RVA: 0x00024993 File Offset: 0x00022B93
		internal void RemoveMax()
		{
			this._count--;
			if (this._count > 0)
			{
				this._elements[0] = this._elements[this._count];
				this.HeapifyRoot();
			}
		}

		// Token: 0x06000B71 RID: 2929 RVA: 0x000249D0 File Offset: 0x00022BD0
		private void Swap(int i, int j)
		{
			TElement telement = this._elements[i];
			this._elements[i] = this._elements[j];
			this._elements[j] = telement;
		}

		// Token: 0x06000B72 RID: 2930 RVA: 0x00024A10 File Offset: 0x00022C10
		private void HeapifyRoot()
		{
			int i = 0;
			int count = this._count;
			while (i < count)
			{
				int num = (i + 1) * 2 - 1;
				int num2 = num + 1;
				if (num < count && this._comparer.Compare(this._elements[i], this._elements[num]) < 0)
				{
					if (num2 < count && this._comparer.Compare(this._elements[num], this._elements[num2]) < 0)
					{
						this.Swap(i, num2);
						i = num2;
					}
					else
					{
						this.Swap(i, num);
						i = num;
					}
				}
				else
				{
					if (num2 >= count || this._comparer.Compare(this._elements[i], this._elements[num2]) >= 0)
					{
						break;
					}
					this.Swap(i, num2);
					i = num2;
				}
			}
		}

		// Token: 0x06000B73 RID: 2931 RVA: 0x00024AE0 File Offset: 0x00022CE0
		private void HeapifyLastLeaf()
		{
			int num;
			for (int i = this._count - 1; i > 0; i = num)
			{
				num = (i + 1) / 2 - 1;
				if (this._comparer.Compare(this._elements[i], this._elements[num]) <= 0)
				{
					break;
				}
				this.Swap(i, num);
			}
		}

		// Token: 0x040007D9 RID: 2009
		private TElement[] _elements;

		// Token: 0x040007DA RID: 2010
		private int _count;

		// Token: 0x040007DB RID: 2011
		private IComparer<TElement> _comparer;
	}
}
