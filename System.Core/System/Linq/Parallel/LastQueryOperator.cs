﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x0200019A RID: 410
	internal sealed class LastQueryOperator<TSource> : UnaryQueryOperator<TSource, TSource>
	{
		// Token: 0x06000A8B RID: 2699 RVA: 0x00021695 File Offset: 0x0001F895
		internal LastQueryOperator(IEnumerable<TSource> child, Func<TSource, bool> predicate) : base(child)
		{
			this._predicate = predicate;
			this._prematureMergeNeeded = base.Child.OrdinalIndexState.IsWorseThan(OrdinalIndexState.Increasing);
		}

		// Token: 0x06000A8C RID: 2700 RVA: 0x00020300 File Offset: 0x0001E500
		internal override QueryResults<TSource> Open(QuerySettings settings, bool preferStriping)
		{
			return new UnaryQueryOperator<TSource, TSource>.UnaryQueryOperatorResults(base.Child.Open(settings, false), this, settings, preferStriping);
		}

		// Token: 0x06000A8D RID: 2701 RVA: 0x000216BC File Offset: 0x0001F8BC
		internal override void WrapPartitionedStream<TKey>(PartitionedStream<TSource, TKey> inputStream, IPartitionedStreamRecipient<TSource> recipient, bool preferStriping, QuerySettings settings)
		{
			if (this._prematureMergeNeeded)
			{
				PartitionedStream<TSource, int> partitionedStream = QueryOperator<TSource>.ExecuteAndCollectResults<TKey>(inputStream, inputStream.PartitionCount, base.Child.OutputOrdered, preferStriping, settings).GetPartitionedStream();
				this.WrapHelper<int>(partitionedStream, recipient, settings);
				return;
			}
			this.WrapHelper<TKey>(inputStream, recipient, settings);
		}

		// Token: 0x06000A8E RID: 2702 RVA: 0x00021708 File Offset: 0x0001F908
		private void WrapHelper<TKey>(PartitionedStream<TSource, TKey> inputStream, IPartitionedStreamRecipient<TSource> recipient, QuerySettings settings)
		{
			int partitionCount = inputStream.PartitionCount;
			LastQueryOperator<TSource>.LastQueryOperatorState<TKey> operatorState = new LastQueryOperator<TSource>.LastQueryOperatorState<TKey>();
			CountdownEvent sharedBarrier = new CountdownEvent(partitionCount);
			PartitionedStream<TSource, int> partitionedStream = new PartitionedStream<TSource, int>(partitionCount, Util.GetDefaultComparer<int>(), OrdinalIndexState.Shuffled);
			for (int i = 0; i < partitionCount; i++)
			{
				partitionedStream[i] = new LastQueryOperator<TSource>.LastQueryOperatorEnumerator<TKey>(inputStream[i], this._predicate, operatorState, sharedBarrier, settings.CancellationState.MergedCancellationToken, inputStream.KeyComparer, i);
			}
			recipient.Receive<int>(partitionedStream);
		}

		// Token: 0x06000A8F RID: 2703 RVA: 0x00003A6B File Offset: 0x00001C6B
		[ExcludeFromCodeCoverage]
		internal override IEnumerable<TSource> AsSequentialQuery(CancellationToken token)
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000172 RID: 370
		// (get) Token: 0x06000A90 RID: 2704 RVA: 0x00002285 File Offset: 0x00000485
		internal override bool LimitsParallelism
		{
			get
			{
				return false;
			}
		}

		// Token: 0x0400070D RID: 1805
		private readonly Func<TSource, bool> _predicate;

		// Token: 0x0400070E RID: 1806
		private readonly bool _prematureMergeNeeded;

		// Token: 0x0200019B RID: 411
		private class LastQueryOperatorEnumerator<TKey> : QueryOperatorEnumerator<TSource, int>
		{
			// Token: 0x06000A91 RID: 2705 RVA: 0x0002177E File Offset: 0x0001F97E
			internal LastQueryOperatorEnumerator(QueryOperatorEnumerator<TSource, TKey> source, Func<TSource, bool> predicate, LastQueryOperator<TSource>.LastQueryOperatorState<TKey> operatorState, CountdownEvent sharedBarrier, CancellationToken cancelToken, IComparer<TKey> keyComparer, int partitionId)
			{
				this._source = source;
				this._predicate = predicate;
				this._operatorState = operatorState;
				this._sharedBarrier = sharedBarrier;
				this._cancellationToken = cancelToken;
				this._keyComparer = keyComparer;
				this._partitionId = partitionId;
			}

			// Token: 0x06000A92 RID: 2706 RVA: 0x000217BC File Offset: 0x0001F9BC
			internal override bool MoveNext(ref TSource currentElement, ref int currentKey)
			{
				if (this._alreadySearched)
				{
					return false;
				}
				TSource tsource = default(TSource);
				TKey tkey = default(TKey);
				bool flag = false;
				try
				{
					int num = 0;
					TSource tsource2 = default(TSource);
					TKey tkey2 = default(TKey);
					while (this._source.MoveNext(ref tsource2, ref tkey2))
					{
						if ((num & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(this._cancellationToken);
						}
						if (this._predicate == null || this._predicate(tsource2))
						{
							tsource = tsource2;
							tkey = tkey2;
							flag = true;
						}
						num++;
					}
					if (flag)
					{
						LastQueryOperator<TSource>.LastQueryOperatorState<TKey> operatorState = this._operatorState;
						lock (operatorState)
						{
							if (this._operatorState._partitionId == -1 || this._keyComparer.Compare(tkey, this._operatorState._key) > 0)
							{
								this._operatorState._partitionId = this._partitionId;
								this._operatorState._key = tkey;
							}
						}
					}
				}
				finally
				{
					this._sharedBarrier.Signal();
				}
				this._alreadySearched = true;
				if (this._partitionId == this._operatorState._partitionId)
				{
					this._sharedBarrier.Wait(this._cancellationToken);
					if (this._operatorState._partitionId == this._partitionId)
					{
						currentElement = tsource;
						currentKey = 0;
						return true;
					}
				}
				return false;
			}

			// Token: 0x06000A93 RID: 2707 RVA: 0x00021920 File Offset: 0x0001FB20
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x0400070F RID: 1807
			private QueryOperatorEnumerator<TSource, TKey> _source;

			// Token: 0x04000710 RID: 1808
			private Func<TSource, bool> _predicate;

			// Token: 0x04000711 RID: 1809
			private bool _alreadySearched;

			// Token: 0x04000712 RID: 1810
			private int _partitionId;

			// Token: 0x04000713 RID: 1811
			private LastQueryOperator<TSource>.LastQueryOperatorState<TKey> _operatorState;

			// Token: 0x04000714 RID: 1812
			private CountdownEvent _sharedBarrier;

			// Token: 0x04000715 RID: 1813
			private CancellationToken _cancellationToken;

			// Token: 0x04000716 RID: 1814
			private IComparer<TKey> _keyComparer;
		}

		// Token: 0x0200019C RID: 412
		private class LastQueryOperatorState<TKey>
		{
			// Token: 0x06000A94 RID: 2708 RVA: 0x0002192D File Offset: 0x0001FB2D
			public LastQueryOperatorState()
			{
			}

			// Token: 0x04000717 RID: 1815
			internal TKey _key;

			// Token: 0x04000718 RID: 1816
			internal int _partitionId = -1;
		}
	}
}
