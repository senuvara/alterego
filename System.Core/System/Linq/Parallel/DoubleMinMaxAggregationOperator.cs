﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x0200012C RID: 300
	internal sealed class DoubleMinMaxAggregationOperator : InlinedAggregationOperator<double, double, double>
	{
		// Token: 0x060008D7 RID: 2263 RVA: 0x0001C0D7 File Offset: 0x0001A2D7
		internal DoubleMinMaxAggregationOperator(IEnumerable<double> child, int sign) : base(child)
		{
			this._sign = sign;
		}

		// Token: 0x060008D8 RID: 2264 RVA: 0x0001C0E8 File Offset: 0x0001A2E8
		protected override double InternalAggregate(ref Exception singularExceptionToThrow)
		{
			double result;
			using (IEnumerator<double> enumerator = this.GetEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered), true))
			{
				if (!enumerator.MoveNext())
				{
					singularExceptionToThrow = new InvalidOperationException("Sequence contains no elements");
					result = 0.0;
				}
				else
				{
					double num = enumerator.Current;
					if (this._sign == -1)
					{
						while (enumerator.MoveNext())
						{
							double num2 = enumerator.Current;
							if (num2 < num || double.IsNaN(num2))
							{
								num = num2;
							}
						}
					}
					else
					{
						while (enumerator.MoveNext())
						{
							double num3 = enumerator.Current;
							if (num3 > num || double.IsNaN(num))
							{
								num = num3;
							}
						}
					}
					result = num;
				}
			}
			return result;
		}

		// Token: 0x060008D9 RID: 2265 RVA: 0x0001C194 File Offset: 0x0001A394
		protected override QueryOperatorEnumerator<double, int> CreateEnumerator<TKey>(int index, int count, QueryOperatorEnumerator<double, TKey> source, object sharedData, CancellationToken cancellationToken)
		{
			return new DoubleMinMaxAggregationOperator.DoubleMinMaxAggregationOperatorEnumerator<TKey>(source, index, this._sign, cancellationToken);
		}

		// Token: 0x0400063D RID: 1597
		private readonly int _sign;

		// Token: 0x0200012D RID: 301
		private class DoubleMinMaxAggregationOperatorEnumerator<TKey> : InlinedAggregationOperatorEnumerator<double>
		{
			// Token: 0x060008DA RID: 2266 RVA: 0x0001C1A5 File Offset: 0x0001A3A5
			internal DoubleMinMaxAggregationOperatorEnumerator(QueryOperatorEnumerator<double, TKey> source, int partitionIndex, int sign, CancellationToken cancellationToken) : base(partitionIndex, cancellationToken)
			{
				this._source = source;
				this._sign = sign;
			}

			// Token: 0x060008DB RID: 2267 RVA: 0x0001C1C0 File Offset: 0x0001A3C0
			protected override bool MoveNextCore(ref double currentElement)
			{
				QueryOperatorEnumerator<double, TKey> source = this._source;
				TKey tkey = default(TKey);
				if (source.MoveNext(ref currentElement, ref tkey))
				{
					int num = 0;
					if (this._sign == -1)
					{
						double num2 = 0.0;
						while (source.MoveNext(ref num2, ref tkey))
						{
							if ((num++ & 63) == 0)
							{
								CancellationState.ThrowIfCanceled(this._cancellationToken);
							}
							if (num2 < currentElement || double.IsNaN(num2))
							{
								currentElement = num2;
							}
						}
					}
					else
					{
						double num3 = 0.0;
						while (source.MoveNext(ref num3, ref tkey))
						{
							if ((num++ & 63) == 0)
							{
								CancellationState.ThrowIfCanceled(this._cancellationToken);
							}
							if (num3 > currentElement || double.IsNaN(currentElement))
							{
								currentElement = num3;
							}
						}
					}
					return true;
				}
				return false;
			}

			// Token: 0x060008DC RID: 2268 RVA: 0x0001C278 File Offset: 0x0001A478
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x0400063E RID: 1598
			private QueryOperatorEnumerator<double, TKey> _source;

			// Token: 0x0400063F RID: 1599
			private int _sign;
		}
	}
}
