﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000142 RID: 322
	internal sealed class LongMinMaxAggregationOperator : InlinedAggregationOperator<long, long, long>
	{
		// Token: 0x0600091E RID: 2334 RVA: 0x0001CE8D File Offset: 0x0001B08D
		internal LongMinMaxAggregationOperator(IEnumerable<long> child, int sign) : base(child)
		{
			this._sign = sign;
		}

		// Token: 0x0600091F RID: 2335 RVA: 0x0001CEA0 File Offset: 0x0001B0A0
		protected override long InternalAggregate(ref Exception singularExceptionToThrow)
		{
			long result;
			using (IEnumerator<long> enumerator = this.GetEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered), true))
			{
				if (!enumerator.MoveNext())
				{
					singularExceptionToThrow = new InvalidOperationException("Sequence contains no elements");
					result = 0L;
				}
				else
				{
					long num = enumerator.Current;
					if (this._sign == -1)
					{
						while (enumerator.MoveNext())
						{
							long num2 = enumerator.Current;
							if (num2 < num)
							{
								num = num2;
							}
						}
					}
					else
					{
						while (enumerator.MoveNext())
						{
							long num3 = enumerator.Current;
							if (num3 > num)
							{
								num = num3;
							}
						}
					}
					result = num;
				}
			}
			return result;
		}

		// Token: 0x06000920 RID: 2336 RVA: 0x0001CF34 File Offset: 0x0001B134
		protected override QueryOperatorEnumerator<long, int> CreateEnumerator<TKey>(int index, int count, QueryOperatorEnumerator<long, TKey> source, object sharedData, CancellationToken cancellationToken)
		{
			return new LongMinMaxAggregationOperator.LongMinMaxAggregationOperatorEnumerator<TKey>(source, index, this._sign, cancellationToken);
		}

		// Token: 0x04000650 RID: 1616
		private readonly int _sign;

		// Token: 0x02000143 RID: 323
		private class LongMinMaxAggregationOperatorEnumerator<TKey> : InlinedAggregationOperatorEnumerator<long>
		{
			// Token: 0x06000921 RID: 2337 RVA: 0x0001CF45 File Offset: 0x0001B145
			internal LongMinMaxAggregationOperatorEnumerator(QueryOperatorEnumerator<long, TKey> source, int partitionIndex, int sign, CancellationToken cancellationToken) : base(partitionIndex, cancellationToken)
			{
				this._source = source;
				this._sign = sign;
			}

			// Token: 0x06000922 RID: 2338 RVA: 0x0001CF60 File Offset: 0x0001B160
			protected override bool MoveNextCore(ref long currentElement)
			{
				QueryOperatorEnumerator<long, TKey> source = this._source;
				TKey tkey = default(TKey);
				if (source.MoveNext(ref currentElement, ref tkey))
				{
					int num = 0;
					if (this._sign == -1)
					{
						long num2 = 0L;
						while (source.MoveNext(ref num2, ref tkey))
						{
							if ((num++ & 63) == 0)
							{
								CancellationState.ThrowIfCanceled(this._cancellationToken);
							}
							if (num2 < currentElement)
							{
								currentElement = num2;
							}
						}
					}
					else
					{
						long num3 = 0L;
						while (source.MoveNext(ref num3, ref tkey))
						{
							if ((num++ & 63) == 0)
							{
								CancellationState.ThrowIfCanceled(this._cancellationToken);
							}
							if (num3 > currentElement)
							{
								currentElement = num3;
							}
						}
					}
					return true;
				}
				return false;
			}

			// Token: 0x06000923 RID: 2339 RVA: 0x0001CFF6 File Offset: 0x0001B1F6
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x04000651 RID: 1617
			private QueryOperatorEnumerator<long, TKey> _source;

			// Token: 0x04000652 RID: 1618
			private int _sign;
		}
	}
}
