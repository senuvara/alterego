﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000165 RID: 357
	internal sealed class OrderingQueryOperator<TSource> : QueryOperator<TSource>
	{
		// Token: 0x0600098A RID: 2442 RVA: 0x0001E9E3 File Offset: 0x0001CBE3
		public OrderingQueryOperator(QueryOperator<TSource> child, bool orderOn) : base(orderOn, child.SpecifiedQuerySettings)
		{
			this._child = child;
			this._ordinalIndexState = this._child.OrdinalIndexState;
		}

		// Token: 0x0600098B RID: 2443 RVA: 0x0001EA0A File Offset: 0x0001CC0A
		internal override QueryResults<TSource> Open(QuerySettings settings, bool preferStriping)
		{
			return this._child.Open(settings, preferStriping);
		}

		// Token: 0x0600098C RID: 2444 RVA: 0x0001EA1C File Offset: 0x0001CC1C
		internal override IEnumerator<TSource> GetEnumerator(ParallelMergeOptions? mergeOptions, bool suppressOrderPreservation)
		{
			ScanQueryOperator<TSource> scanQueryOperator = this._child as ScanQueryOperator<TSource>;
			if (scanQueryOperator != null)
			{
				return scanQueryOperator.Data.GetEnumerator();
			}
			return base.GetEnumerator(mergeOptions, suppressOrderPreservation);
		}

		// Token: 0x0600098D RID: 2445 RVA: 0x0001EA4C File Offset: 0x0001CC4C
		internal override IEnumerable<TSource> AsSequentialQuery(CancellationToken token)
		{
			return this._child.AsSequentialQuery(token);
		}

		// Token: 0x17000140 RID: 320
		// (get) Token: 0x0600098E RID: 2446 RVA: 0x0001EA5A File Offset: 0x0001CC5A
		internal override bool LimitsParallelism
		{
			get
			{
				return this._child.LimitsParallelism;
			}
		}

		// Token: 0x17000141 RID: 321
		// (get) Token: 0x0600098F RID: 2447 RVA: 0x0001EA67 File Offset: 0x0001CC67
		internal override OrdinalIndexState OrdinalIndexState
		{
			get
			{
				return this._ordinalIndexState;
			}
		}

		// Token: 0x04000670 RID: 1648
		private QueryOperator<TSource> _child;

		// Token: 0x04000671 RID: 1649
		private OrdinalIndexState _ordinalIndexState;
	}
}
