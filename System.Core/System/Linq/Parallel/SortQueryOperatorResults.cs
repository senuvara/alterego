﻿using System;

namespace System.Linq.Parallel
{
	// Token: 0x020001AC RID: 428
	internal class SortQueryOperatorResults<TInputOutput, TSortKey> : QueryResults<TInputOutput>
	{
		// Token: 0x06000ACF RID: 2767 RVA: 0x000225A7 File Offset: 0x000207A7
		internal SortQueryOperatorResults(QueryResults<TInputOutput> childQueryResults, SortQueryOperator<TInputOutput, TSortKey> op, QuerySettings settings)
		{
			this._childQueryResults = childQueryResults;
			this._op = op;
			this._settings = settings;
		}

		// Token: 0x1700017C RID: 380
		// (get) Token: 0x06000AD0 RID: 2768 RVA: 0x00002285 File Offset: 0x00000485
		internal override bool IsIndexible
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06000AD1 RID: 2769 RVA: 0x000225C4 File Offset: 0x000207C4
		internal override void GivePartitionedStream(IPartitionedStreamRecipient<TInputOutput> recipient)
		{
			this._childQueryResults.GivePartitionedStream(new SortQueryOperatorResults<TInputOutput, TSortKey>.ChildResultsRecipient(recipient, this._op, this._settings));
		}

		// Token: 0x04000746 RID: 1862
		protected QueryResults<TInputOutput> _childQueryResults;

		// Token: 0x04000747 RID: 1863
		private SortQueryOperator<TInputOutput, TSortKey> _op;

		// Token: 0x04000748 RID: 1864
		private QuerySettings _settings;

		// Token: 0x020001AD RID: 429
		private class ChildResultsRecipient : IPartitionedStreamRecipient<TInputOutput>
		{
			// Token: 0x06000AD2 RID: 2770 RVA: 0x000225E3 File Offset: 0x000207E3
			internal ChildResultsRecipient(IPartitionedStreamRecipient<TInputOutput> outputRecipient, SortQueryOperator<TInputOutput, TSortKey> op, QuerySettings settings)
			{
				this._outputRecipient = outputRecipient;
				this._op = op;
				this._settings = settings;
			}

			// Token: 0x06000AD3 RID: 2771 RVA: 0x00022600 File Offset: 0x00020800
			public void Receive<TKey>(PartitionedStream<TInputOutput, TKey> childPartitionedStream)
			{
				this._op.WrapPartitionedStream<TKey>(childPartitionedStream, this._outputRecipient, false, this._settings);
			}

			// Token: 0x04000749 RID: 1865
			private IPartitionedStreamRecipient<TInputOutput> _outputRecipient;

			// Token: 0x0400074A RID: 1866
			private SortQueryOperator<TInputOutput, TSortKey> _op;

			// Token: 0x0400074B RID: 1867
			private QuerySettings _settings;
		}
	}
}
