﻿using System;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x0200010C RID: 268
	internal struct ConcatKey<TLeftKey, TRightKey>
	{
		// Token: 0x0600085C RID: 2140 RVA: 0x00019D3B File Offset: 0x00017F3B
		private ConcatKey(TLeftKey leftKey, TRightKey rightKey, bool isLeft)
		{
			this._leftKey = leftKey;
			this._rightKey = rightKey;
			this._isLeft = isLeft;
		}

		// Token: 0x0600085D RID: 2141 RVA: 0x00019D54 File Offset: 0x00017F54
		internal static ConcatKey<TLeftKey, TRightKey> MakeLeft(TLeftKey leftKey)
		{
			return new ConcatKey<TLeftKey, TRightKey>(leftKey, default(TRightKey), true);
		}

		// Token: 0x0600085E RID: 2142 RVA: 0x00019D74 File Offset: 0x00017F74
		internal static ConcatKey<TLeftKey, TRightKey> MakeRight(TRightKey rightKey)
		{
			return new ConcatKey<TLeftKey, TRightKey>(default(TLeftKey), rightKey, false);
		}

		// Token: 0x0600085F RID: 2143 RVA: 0x00019D91 File Offset: 0x00017F91
		internal static IComparer<ConcatKey<TLeftKey, TRightKey>> MakeComparer(IComparer<TLeftKey> leftComparer, IComparer<TRightKey> rightComparer)
		{
			return new ConcatKey<TLeftKey, TRightKey>.ConcatKeyComparer(leftComparer, rightComparer);
		}

		// Token: 0x040005CE RID: 1486
		private readonly TLeftKey _leftKey;

		// Token: 0x040005CF RID: 1487
		private readonly TRightKey _rightKey;

		// Token: 0x040005D0 RID: 1488
		private readonly bool _isLeft;

		// Token: 0x0200010D RID: 269
		private class ConcatKeyComparer : IComparer<ConcatKey<TLeftKey, TRightKey>>
		{
			// Token: 0x06000860 RID: 2144 RVA: 0x00019D9A File Offset: 0x00017F9A
			internal ConcatKeyComparer(IComparer<TLeftKey> leftComparer, IComparer<TRightKey> rightComparer)
			{
				this._leftComparer = leftComparer;
				this._rightComparer = rightComparer;
			}

			// Token: 0x06000861 RID: 2145 RVA: 0x00019DB0 File Offset: 0x00017FB0
			public int Compare(ConcatKey<TLeftKey, TRightKey> x, ConcatKey<TLeftKey, TRightKey> y)
			{
				if (x._isLeft != y._isLeft)
				{
					if (!x._isLeft)
					{
						return 1;
					}
					return -1;
				}
				else
				{
					if (x._isLeft)
					{
						return this._leftComparer.Compare(x._leftKey, y._leftKey);
					}
					return this._rightComparer.Compare(x._rightKey, y._rightKey);
				}
			}

			// Token: 0x040005D1 RID: 1489
			private IComparer<TLeftKey> _leftComparer;

			// Token: 0x040005D2 RID: 1490
			private IComparer<TRightKey> _rightComparer;
		}
	}
}
