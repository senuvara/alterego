﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x0200011B RID: 283
	internal sealed class ZipQueryOperator<TLeftInput, TRightInput, TOutput> : QueryOperator<TOutput>
	{
		// Token: 0x06000897 RID: 2199 RVA: 0x0001B3B2 File Offset: 0x000195B2
		internal ZipQueryOperator(ParallelQuery<TLeftInput> leftChildSource, ParallelQuery<TRightInput> rightChildSource, Func<TLeftInput, TRightInput, TOutput> resultSelector) : this(QueryOperator<TLeftInput>.AsQueryOperator(leftChildSource), QueryOperator<TRightInput>.AsQueryOperator(rightChildSource), resultSelector)
		{
		}

		// Token: 0x06000898 RID: 2200 RVA: 0x0001B3C8 File Offset: 0x000195C8
		private ZipQueryOperator(QueryOperator<TLeftInput> left, QueryOperator<TRightInput> right, Func<TLeftInput, TRightInput, TOutput> resultSelector) : base(left.SpecifiedQuerySettings.Merge(right.SpecifiedQuerySettings))
		{
			this._leftChild = left;
			this._rightChild = right;
			this._resultSelector = resultSelector;
			this._outputOrdered = (this._leftChild.OutputOrdered || this._rightChild.OutputOrdered);
			OrdinalIndexState ordinalIndexState = this._leftChild.OrdinalIndexState;
			OrdinalIndexState ordinalIndexState2 = this._rightChild.OrdinalIndexState;
			this._prematureMergeLeft = (ordinalIndexState > OrdinalIndexState.Indexable);
			this._prematureMergeRight = (ordinalIndexState2 > OrdinalIndexState.Indexable);
			this._limitsParallelism = ((this._prematureMergeLeft && ordinalIndexState != OrdinalIndexState.Shuffled) || (this._prematureMergeRight && ordinalIndexState2 != OrdinalIndexState.Shuffled));
		}

		// Token: 0x06000899 RID: 2201 RVA: 0x0001B478 File Offset: 0x00019678
		internal override QueryResults<TOutput> Open(QuerySettings settings, bool preferStriping)
		{
			QueryResults<TLeftInput> queryResults = this._leftChild.Open(settings, preferStriping);
			QueryResults<TRightInput> queryResults2 = this._rightChild.Open(settings, preferStriping);
			int value = settings.DegreeOfParallelism.Value;
			if (this._prematureMergeLeft)
			{
				PartitionedStreamMerger<TLeftInput> partitionedStreamMerger = new PartitionedStreamMerger<TLeftInput>(false, ParallelMergeOptions.FullyBuffered, settings.TaskScheduler, this._leftChild.OutputOrdered, settings.CancellationState, settings.QueryId);
				queryResults.GivePartitionedStream(partitionedStreamMerger);
				queryResults = new ListQueryResults<TLeftInput>(partitionedStreamMerger.MergeExecutor.GetResultsAsArray(), value, preferStriping);
			}
			if (this._prematureMergeRight)
			{
				PartitionedStreamMerger<TRightInput> partitionedStreamMerger2 = new PartitionedStreamMerger<TRightInput>(false, ParallelMergeOptions.FullyBuffered, settings.TaskScheduler, this._rightChild.OutputOrdered, settings.CancellationState, settings.QueryId);
				queryResults2.GivePartitionedStream(partitionedStreamMerger2);
				queryResults2 = new ListQueryResults<TRightInput>(partitionedStreamMerger2.MergeExecutor.GetResultsAsArray(), value, preferStriping);
			}
			return new ZipQueryOperator<TLeftInput, TRightInput, TOutput>.ZipQueryOperatorResults(queryResults, queryResults2, this._resultSelector, value, preferStriping);
		}

		// Token: 0x0600089A RID: 2202 RVA: 0x0001B55A File Offset: 0x0001975A
		internal override IEnumerable<TOutput> AsSequentialQuery(CancellationToken token)
		{
			using (IEnumerator<TLeftInput> leftEnumerator = this._leftChild.AsSequentialQuery(token).GetEnumerator())
			{
				using (IEnumerator<TRightInput> rightEnumerator = this._rightChild.AsSequentialQuery(token).GetEnumerator())
				{
					while (leftEnumerator.MoveNext() && rightEnumerator.MoveNext())
					{
						yield return this._resultSelector(leftEnumerator.Current, rightEnumerator.Current);
					}
				}
				IEnumerator<TRightInput> rightEnumerator = null;
			}
			IEnumerator<TLeftInput> leftEnumerator = null;
			yield break;
			yield break;
		}

		// Token: 0x17000134 RID: 308
		// (get) Token: 0x0600089B RID: 2203 RVA: 0x00002285 File Offset: 0x00000485
		internal override OrdinalIndexState OrdinalIndexState
		{
			get
			{
				return OrdinalIndexState.Indexable;
			}
		}

		// Token: 0x17000135 RID: 309
		// (get) Token: 0x0600089C RID: 2204 RVA: 0x0001B571 File Offset: 0x00019771
		internal override bool LimitsParallelism
		{
			get
			{
				return this._limitsParallelism;
			}
		}

		// Token: 0x04000611 RID: 1553
		private readonly Func<TLeftInput, TRightInput, TOutput> _resultSelector;

		// Token: 0x04000612 RID: 1554
		private readonly QueryOperator<TLeftInput> _leftChild;

		// Token: 0x04000613 RID: 1555
		private readonly QueryOperator<TRightInput> _rightChild;

		// Token: 0x04000614 RID: 1556
		private readonly bool _prematureMergeLeft;

		// Token: 0x04000615 RID: 1557
		private readonly bool _prematureMergeRight;

		// Token: 0x04000616 RID: 1558
		private readonly bool _limitsParallelism;

		// Token: 0x0200011C RID: 284
		internal class ZipQueryOperatorResults : QueryResults<TOutput>
		{
			// Token: 0x0600089D RID: 2205 RVA: 0x0001B57C File Offset: 0x0001977C
			internal ZipQueryOperatorResults(QueryResults<TLeftInput> leftChildResults, QueryResults<TRightInput> rightChildResults, Func<TLeftInput, TRightInput, TOutput> resultSelector, int partitionCount, bool preferStriping)
			{
				this._leftChildResults = leftChildResults;
				this._rightChildResults = rightChildResults;
				this._resultSelector = resultSelector;
				this._partitionCount = partitionCount;
				this._preferStriping = preferStriping;
				this._count = Math.Min(this._leftChildResults.Count, this._rightChildResults.Count);
			}

			// Token: 0x17000136 RID: 310
			// (get) Token: 0x0600089E RID: 2206 RVA: 0x0001B5D5 File Offset: 0x000197D5
			internal override int ElementsCount
			{
				get
				{
					return this._count;
				}
			}

			// Token: 0x17000137 RID: 311
			// (get) Token: 0x0600089F RID: 2207 RVA: 0x00009CDF File Offset: 0x00007EDF
			internal override bool IsIndexible
			{
				get
				{
					return true;
				}
			}

			// Token: 0x060008A0 RID: 2208 RVA: 0x0001B5DD File Offset: 0x000197DD
			internal override TOutput GetElement(int index)
			{
				return this._resultSelector(this._leftChildResults.GetElement(index), this._rightChildResults.GetElement(index));
			}

			// Token: 0x060008A1 RID: 2209 RVA: 0x0001B604 File Offset: 0x00019804
			internal override void GivePartitionedStream(IPartitionedStreamRecipient<TOutput> recipient)
			{
				PartitionedStream<TOutput, int> partitionedStream = ExchangeUtilities.PartitionDataSource<TOutput>(this, this._partitionCount, this._preferStriping);
				recipient.Receive<int>(partitionedStream);
			}

			// Token: 0x04000617 RID: 1559
			private readonly QueryResults<TLeftInput> _leftChildResults;

			// Token: 0x04000618 RID: 1560
			private readonly QueryResults<TRightInput> _rightChildResults;

			// Token: 0x04000619 RID: 1561
			private readonly Func<TLeftInput, TRightInput, TOutput> _resultSelector;

			// Token: 0x0400061A RID: 1562
			private readonly int _count;

			// Token: 0x0400061B RID: 1563
			private readonly int _partitionCount;

			// Token: 0x0400061C RID: 1564
			private readonly bool _preferStriping;
		}

		// Token: 0x0200011D RID: 285
		[CompilerGenerated]
		private sealed class <AsSequentialQuery>d__9 : IEnumerable<TOutput>, IEnumerable, IEnumerator<TOutput>, IDisposable, IEnumerator
		{
			// Token: 0x060008A2 RID: 2210 RVA: 0x0001B62B File Offset: 0x0001982B
			[DebuggerHidden]
			public <AsSequentialQuery>d__9(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060008A3 RID: 2211 RVA: 0x0001B648 File Offset: 0x00019848
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num - -4 <= 1 || num == 1)
				{
					try
					{
						if (num == -4 || num == 1)
						{
							try
							{
							}
							finally
							{
								this.<>m__Finally2();
							}
						}
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x060008A4 RID: 2212 RVA: 0x0001B6A0 File Offset: 0x000198A0
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					ZipQueryOperator<TLeftInput, TRightInput, TOutput> zipQueryOperator = this;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -4;
					}
					else
					{
						this.<>1__state = -1;
						leftEnumerator = zipQueryOperator._leftChild.AsSequentialQuery(token).GetEnumerator();
						this.<>1__state = -3;
						rightEnumerator = zipQueryOperator._rightChild.AsSequentialQuery(token).GetEnumerator();
						this.<>1__state = -4;
					}
					if (!leftEnumerator.MoveNext() || !rightEnumerator.MoveNext())
					{
						this.<>m__Finally2();
						rightEnumerator = null;
						this.<>m__Finally1();
						leftEnumerator = null;
						result = false;
					}
					else
					{
						this.<>2__current = zipQueryOperator._resultSelector(leftEnumerator.Current, rightEnumerator.Current);
						this.<>1__state = 1;
						result = true;
					}
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x060008A5 RID: 2213 RVA: 0x0001B7A8 File Offset: 0x000199A8
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (leftEnumerator != null)
				{
					leftEnumerator.Dispose();
				}
			}

			// Token: 0x060008A6 RID: 2214 RVA: 0x0001B7C4 File Offset: 0x000199C4
			private void <>m__Finally2()
			{
				this.<>1__state = -3;
				if (rightEnumerator != null)
				{
					rightEnumerator.Dispose();
				}
			}

			// Token: 0x17000138 RID: 312
			// (get) Token: 0x060008A7 RID: 2215 RVA: 0x0001B7E1 File Offset: 0x000199E1
			TOutput IEnumerator<!2>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060008A8 RID: 2216 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000139 RID: 313
			// (get) Token: 0x060008A9 RID: 2217 RVA: 0x0001B7E9 File Offset: 0x000199E9
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060008AA RID: 2218 RVA: 0x0001B7F8 File Offset: 0x000199F8
			[DebuggerHidden]
			IEnumerator<TOutput> IEnumerable<!2>.GetEnumerator()
			{
				ZipQueryOperator<TLeftInput, TRightInput, TOutput>.<AsSequentialQuery>d__9 <AsSequentialQuery>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<AsSequentialQuery>d__ = this;
				}
				else
				{
					<AsSequentialQuery>d__ = new ZipQueryOperator<TLeftInput, TRightInput, TOutput>.<AsSequentialQuery>d__9(0);
					<AsSequentialQuery>d__.<>4__this = this;
				}
				<AsSequentialQuery>d__.token = token;
				return <AsSequentialQuery>d__;
			}

			// Token: 0x060008AB RID: 2219 RVA: 0x0001B847 File Offset: 0x00019A47
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<TOutput>.GetEnumerator();
			}

			// Token: 0x0400061D RID: 1565
			private int <>1__state;

			// Token: 0x0400061E RID: 1566
			private TOutput <>2__current;

			// Token: 0x0400061F RID: 1567
			private int <>l__initialThreadId;

			// Token: 0x04000620 RID: 1568
			public ZipQueryOperator<TLeftInput, TRightInput, TOutput> <>4__this;

			// Token: 0x04000621 RID: 1569
			private CancellationToken token;

			// Token: 0x04000622 RID: 1570
			public CancellationToken <>3__token;

			// Token: 0x04000623 RID: 1571
			private IEnumerator<TLeftInput> <leftEnumerator>5__1;

			// Token: 0x04000624 RID: 1572
			private IEnumerator<TRightInput> <rightEnumerator>5__2;
		}
	}
}
