﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace System.Linq.Parallel
{
	// Token: 0x0200016F RID: 367
	internal abstract class QueryOperator<TOutput> : ParallelQuery<TOutput>
	{
		// Token: 0x060009B7 RID: 2487 RVA: 0x0001F0EC File Offset: 0x0001D2EC
		internal QueryOperator(QuerySettings settings) : this(false, settings)
		{
		}

		// Token: 0x060009B8 RID: 2488 RVA: 0x0001F0F6 File Offset: 0x0001D2F6
		internal QueryOperator(bool isOrdered, QuerySettings settings) : base(settings)
		{
			this._outputOrdered = isOrdered;
		}

		// Token: 0x060009B9 RID: 2489
		internal abstract QueryResults<TOutput> Open(QuerySettings settings, bool preferStriping);

		// Token: 0x060009BA RID: 2490 RVA: 0x0001F108 File Offset: 0x0001D308
		public override IEnumerator<TOutput> GetEnumerator()
		{
			return this.GetEnumerator(null, false);
		}

		// Token: 0x060009BB RID: 2491 RVA: 0x0001F125 File Offset: 0x0001D325
		public IEnumerator<TOutput> GetEnumerator(ParallelMergeOptions? mergeOptions)
		{
			return this.GetEnumerator(mergeOptions, false);
		}

		// Token: 0x1700014C RID: 332
		// (get) Token: 0x060009BC RID: 2492 RVA: 0x0001F12F File Offset: 0x0001D32F
		internal bool OutputOrdered
		{
			get
			{
				return this._outputOrdered;
			}
		}

		// Token: 0x060009BD RID: 2493 RVA: 0x0001F137 File Offset: 0x0001D337
		internal virtual IEnumerator<TOutput> GetEnumerator(ParallelMergeOptions? mergeOptions, bool suppressOrderPreservation)
		{
			return new QueryOpeningEnumerator<TOutput>(this, mergeOptions, suppressOrderPreservation);
		}

		// Token: 0x060009BE RID: 2494 RVA: 0x0001F144 File Offset: 0x0001D344
		internal IEnumerator<TOutput> GetOpenedEnumerator(ParallelMergeOptions? mergeOptions, bool suppressOrder, bool forEffect, QuerySettings querySettings)
		{
			if (querySettings.ExecutionMode.Value == ParallelExecutionMode.Default && this.LimitsParallelism)
			{
				return ExceptionAggregator.WrapEnumerable<TOutput>(this.AsSequentialQuery(querySettings.CancellationState.ExternalCancellationToken), querySettings.CancellationState).GetEnumerator();
			}
			QueryResults<TOutput> queryResults = this.GetQueryResults(querySettings);
			if (mergeOptions == null)
			{
				mergeOptions = querySettings.MergeOptions;
			}
			if (querySettings.CancellationState.MergedCancellationToken.IsCancellationRequested)
			{
				if (querySettings.CancellationState.ExternalCancellationToken.IsCancellationRequested)
				{
					throw new OperationCanceledException(querySettings.CancellationState.ExternalCancellationToken);
				}
				throw new OperationCanceledException();
			}
			else
			{
				bool outputOrdered = this.OutputOrdered && !suppressOrder;
				PartitionedStreamMerger<TOutput> partitionedStreamMerger = new PartitionedStreamMerger<TOutput>(forEffect, mergeOptions.GetValueOrDefault(), querySettings.TaskScheduler, outputOrdered, querySettings.CancellationState, querySettings.QueryId);
				queryResults.GivePartitionedStream(partitionedStreamMerger);
				if (forEffect)
				{
					return null;
				}
				return partitionedStreamMerger.MergeExecutor.GetEnumerator();
			}
		}

		// Token: 0x060009BF RID: 2495 RVA: 0x0001F233 File Offset: 0x0001D433
		private QueryResults<TOutput> GetQueryResults(QuerySettings querySettings)
		{
			return this.Open(querySettings, false);
		}

		// Token: 0x060009C0 RID: 2496 RVA: 0x0001F240 File Offset: 0x0001D440
		internal TOutput[] ExecuteAndGetResultsAsArray()
		{
			QuerySettings querySettings = base.SpecifiedQuerySettings.WithPerExecutionSettings().WithDefaults();
			QueryLifecycle.LogicalQueryExecutionBegin(querySettings.QueryId);
			TOutput[] result;
			try
			{
				if (querySettings.ExecutionMode.Value == ParallelExecutionMode.Default && this.LimitsParallelism)
				{
					result = ExceptionAggregator.WrapEnumerable<TOutput>(CancellableEnumerable.Wrap<TOutput>(this.AsSequentialQuery(querySettings.CancellationState.ExternalCancellationToken), querySettings.CancellationState.ExternalCancellationToken), querySettings.CancellationState).ToArray<TOutput>();
				}
				else
				{
					QueryResults<TOutput> queryResults = this.GetQueryResults(querySettings);
					if (querySettings.CancellationState.MergedCancellationToken.IsCancellationRequested)
					{
						if (querySettings.CancellationState.ExternalCancellationToken.IsCancellationRequested)
						{
							throw new OperationCanceledException(querySettings.CancellationState.ExternalCancellationToken);
						}
						throw new OperationCanceledException();
					}
					else if (queryResults.IsIndexible && this.OutputOrdered)
					{
						ArrayMergeHelper<TOutput> arrayMergeHelper = new ArrayMergeHelper<TOutput>(base.SpecifiedQuerySettings, queryResults);
						arrayMergeHelper.Execute();
						TOutput[] resultsAsArray = arrayMergeHelper.GetResultsAsArray();
						querySettings.CleanStateAtQueryEnd();
						result = resultsAsArray;
					}
					else
					{
						PartitionedStreamMerger<TOutput> partitionedStreamMerger = new PartitionedStreamMerger<TOutput>(false, ParallelMergeOptions.FullyBuffered, querySettings.TaskScheduler, this.OutputOrdered, querySettings.CancellationState, querySettings.QueryId);
						queryResults.GivePartitionedStream(partitionedStreamMerger);
						TOutput[] resultsAsArray2 = partitionedStreamMerger.MergeExecutor.GetResultsAsArray();
						querySettings.CleanStateAtQueryEnd();
						result = resultsAsArray2;
					}
				}
			}
			finally
			{
				QueryLifecycle.LogicalQueryExecutionEnd(querySettings.QueryId);
			}
			return result;
		}

		// Token: 0x060009C1 RID: 2497
		internal abstract IEnumerable<TOutput> AsSequentialQuery(CancellationToken token);

		// Token: 0x1700014D RID: 333
		// (get) Token: 0x060009C2 RID: 2498
		internal abstract bool LimitsParallelism { get; }

		// Token: 0x1700014E RID: 334
		// (get) Token: 0x060009C3 RID: 2499
		internal abstract OrdinalIndexState OrdinalIndexState { get; }

		// Token: 0x060009C4 RID: 2500 RVA: 0x0001F3B0 File Offset: 0x0001D5B0
		internal static ListQueryResults<TOutput> ExecuteAndCollectResults<TKey>(PartitionedStream<TOutput, TKey> openedChild, int partitionCount, bool outputOrdered, bool useStriping, QuerySettings settings)
		{
			TaskScheduler taskScheduler = settings.TaskScheduler;
			return new ListQueryResults<TOutput>(MergeExecutor<TOutput>.Execute<TKey>(openedChild, false, ParallelMergeOptions.FullyBuffered, taskScheduler, outputOrdered, settings.CancellationState, settings.QueryId).GetResultsAsArray(), partitionCount, useStriping);
		}

		// Token: 0x060009C5 RID: 2501 RVA: 0x0001F3EC File Offset: 0x0001D5EC
		internal static QueryOperator<TOutput> AsQueryOperator(IEnumerable<TOutput> source)
		{
			QueryOperator<TOutput> queryOperator = source as QueryOperator<TOutput>;
			if (queryOperator == null)
			{
				OrderedParallelQuery<TOutput> orderedParallelQuery = source as OrderedParallelQuery<TOutput>;
				if (orderedParallelQuery != null)
				{
					queryOperator = orderedParallelQuery.SortOperator;
				}
				else
				{
					queryOperator = new ScanQueryOperator<TOutput>(source);
				}
			}
			return queryOperator;
		}

		// Token: 0x04000693 RID: 1683
		protected bool _outputOrdered;
	}
}
