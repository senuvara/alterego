﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000109 RID: 265
	internal sealed class ConcatQueryOperator<TSource> : BinaryQueryOperator<TSource, TSource, TSource>
	{
		// Token: 0x0600084D RID: 2125 RVA: 0x00019A14 File Offset: 0x00017C14
		internal ConcatQueryOperator(ParallelQuery<TSource> firstChild, ParallelQuery<TSource> secondChild) : base(firstChild, secondChild)
		{
			this._outputOrdered = (base.LeftChild.OutputOrdered || base.RightChild.OutputOrdered);
			this._prematureMergeLeft = base.LeftChild.OrdinalIndexState.IsWorseThan(OrdinalIndexState.Increasing);
			this._prematureMergeRight = base.RightChild.OrdinalIndexState.IsWorseThan(OrdinalIndexState.Increasing);
			if (base.LeftChild.OrdinalIndexState == OrdinalIndexState.Indexable && base.RightChild.OrdinalIndexState == OrdinalIndexState.Indexable)
			{
				base.SetOrdinalIndex(OrdinalIndexState.Indexable);
				return;
			}
			base.SetOrdinalIndex(OrdinalIndexState.Increasing.Worse(base.LeftChild.OrdinalIndexState.Worse(base.RightChild.OrdinalIndexState)));
		}

		// Token: 0x0600084E RID: 2126 RVA: 0x00019AC4 File Offset: 0x00017CC4
		internal override QueryResults<TSource> Open(QuerySettings settings, bool preferStriping)
		{
			QueryResults<TSource> leftChildQueryResults = base.LeftChild.Open(settings, preferStriping);
			QueryResults<TSource> rightChildQueryResults = base.RightChild.Open(settings, preferStriping);
			return ConcatQueryOperator<TSource>.ConcatQueryOperatorResults.NewResults(leftChildQueryResults, rightChildQueryResults, this, settings, preferStriping);
		}

		// Token: 0x0600084F RID: 2127 RVA: 0x00019AF8 File Offset: 0x00017CF8
		public override void WrapPartitionedStream<TLeftKey, TRightKey>(PartitionedStream<TSource, TLeftKey> leftStream, PartitionedStream<TSource, TRightKey> rightStream, IPartitionedStreamRecipient<TSource> outputRecipient, bool preferStriping, QuerySettings settings)
		{
			if (this._prematureMergeLeft)
			{
				PartitionedStream<TSource, int> partitionedStream = QueryOperator<TSource>.ExecuteAndCollectResults<TLeftKey>(leftStream, leftStream.PartitionCount, base.LeftChild.OutputOrdered, preferStriping, settings).GetPartitionedStream();
				this.WrapHelper<int, TRightKey>(partitionedStream, rightStream, outputRecipient, settings, preferStriping);
				return;
			}
			this.WrapHelper<TLeftKey, TRightKey>(leftStream, rightStream, outputRecipient, settings, preferStriping);
		}

		// Token: 0x06000850 RID: 2128 RVA: 0x00019B4C File Offset: 0x00017D4C
		private void WrapHelper<TLeftKey, TRightKey>(PartitionedStream<TSource, TLeftKey> leftStreamInc, PartitionedStream<TSource, TRightKey> rightStream, IPartitionedStreamRecipient<TSource> outputRecipient, QuerySettings settings, bool preferStriping)
		{
			if (this._prematureMergeRight)
			{
				PartitionedStream<TSource, int> partitionedStream = QueryOperator<TSource>.ExecuteAndCollectResults<TRightKey>(rightStream, leftStreamInc.PartitionCount, base.LeftChild.OutputOrdered, preferStriping, settings).GetPartitionedStream();
				this.WrapHelper2<TLeftKey, int>(leftStreamInc, partitionedStream, outputRecipient);
				return;
			}
			this.WrapHelper2<TLeftKey, TRightKey>(leftStreamInc, rightStream, outputRecipient);
		}

		// Token: 0x06000851 RID: 2129 RVA: 0x00019B98 File Offset: 0x00017D98
		private void WrapHelper2<TLeftKey, TRightKey>(PartitionedStream<TSource, TLeftKey> leftStreamInc, PartitionedStream<TSource, TRightKey> rightStreamInc, IPartitionedStreamRecipient<TSource> outputRecipient)
		{
			int partitionCount = leftStreamInc.PartitionCount;
			IComparer<ConcatKey<TLeftKey, TRightKey>> keyComparer = ConcatKey<TLeftKey, TRightKey>.MakeComparer(leftStreamInc.KeyComparer, rightStreamInc.KeyComparer);
			PartitionedStream<TSource, ConcatKey<TLeftKey, TRightKey>> partitionedStream = new PartitionedStream<TSource, ConcatKey<TLeftKey, TRightKey>>(partitionCount, keyComparer, this.OrdinalIndexState);
			for (int i = 0; i < partitionCount; i++)
			{
				partitionedStream[i] = new ConcatQueryOperator<TSource>.ConcatQueryOperatorEnumerator<TLeftKey, TRightKey>(leftStreamInc[i], rightStreamInc[i]);
			}
			outputRecipient.Receive<ConcatKey<TLeftKey, TRightKey>>(partitionedStream);
		}

		// Token: 0x06000852 RID: 2130 RVA: 0x00019BF9 File Offset: 0x00017DF9
		internal override IEnumerable<TSource> AsSequentialQuery(CancellationToken token)
		{
			return base.LeftChild.AsSequentialQuery(token).Concat(base.RightChild.AsSequentialQuery(token));
		}

		// Token: 0x1700012C RID: 300
		// (get) Token: 0x06000853 RID: 2131 RVA: 0x00002285 File Offset: 0x00000485
		internal override bool LimitsParallelism
		{
			get
			{
				return false;
			}
		}

		// Token: 0x040005C7 RID: 1479
		private readonly bool _prematureMergeLeft;

		// Token: 0x040005C8 RID: 1480
		private readonly bool _prematureMergeRight;

		// Token: 0x0200010A RID: 266
		private sealed class ConcatQueryOperatorEnumerator<TLeftKey, TRightKey> : QueryOperatorEnumerator<TSource, ConcatKey<TLeftKey, TRightKey>>
		{
			// Token: 0x06000854 RID: 2132 RVA: 0x00019C18 File Offset: 0x00017E18
			internal ConcatQueryOperatorEnumerator(QueryOperatorEnumerator<TSource, TLeftKey> firstSource, QueryOperatorEnumerator<TSource, TRightKey> secondSource)
			{
				this._firstSource = firstSource;
				this._secondSource = secondSource;
			}

			// Token: 0x06000855 RID: 2133 RVA: 0x00019C30 File Offset: 0x00017E30
			internal override bool MoveNext(ref TSource currentElement, ref ConcatKey<TLeftKey, TRightKey> currentKey)
			{
				if (!this._begunSecond)
				{
					TLeftKey leftKey = default(TLeftKey);
					if (this._firstSource.MoveNext(ref currentElement, ref leftKey))
					{
						currentKey = ConcatKey<TLeftKey, TRightKey>.MakeLeft(leftKey);
						return true;
					}
					this._begunSecond = true;
				}
				TRightKey rightKey = default(TRightKey);
				if (this._secondSource.MoveNext(ref currentElement, ref rightKey))
				{
					currentKey = ConcatKey<TLeftKey, TRightKey>.MakeRight(rightKey);
					return true;
				}
				return false;
			}

			// Token: 0x06000856 RID: 2134 RVA: 0x00019C99 File Offset: 0x00017E99
			protected override void Dispose(bool disposing)
			{
				this._firstSource.Dispose();
				this._secondSource.Dispose();
			}

			// Token: 0x040005C9 RID: 1481
			private QueryOperatorEnumerator<TSource, TLeftKey> _firstSource;

			// Token: 0x040005CA RID: 1482
			private QueryOperatorEnumerator<TSource, TRightKey> _secondSource;

			// Token: 0x040005CB RID: 1483
			private bool _begunSecond;
		}

		// Token: 0x0200010B RID: 267
		private class ConcatQueryOperatorResults : BinaryQueryOperator<TSource, TSource, TSource>.BinaryQueryOperatorResults
		{
			// Token: 0x06000857 RID: 2135 RVA: 0x00019CB1 File Offset: 0x00017EB1
			public static QueryResults<TSource> NewResults(QueryResults<TSource> leftChildQueryResults, QueryResults<TSource> rightChildQueryResults, ConcatQueryOperator<TSource> op, QuerySettings settings, bool preferStriping)
			{
				if (leftChildQueryResults.IsIndexible && rightChildQueryResults.IsIndexible)
				{
					return new ConcatQueryOperator<TSource>.ConcatQueryOperatorResults(leftChildQueryResults, rightChildQueryResults, op, settings, preferStriping);
				}
				return new BinaryQueryOperator<TSource, TSource, TSource>.BinaryQueryOperatorResults(leftChildQueryResults, rightChildQueryResults, op, settings, preferStriping);
			}

			// Token: 0x06000858 RID: 2136 RVA: 0x00019CDA File Offset: 0x00017EDA
			private ConcatQueryOperatorResults(QueryResults<TSource> leftChildQueryResults, QueryResults<TSource> rightChildQueryResults, ConcatQueryOperator<TSource> concatOp, QuerySettings settings, bool preferStriping) : base(leftChildQueryResults, rightChildQueryResults, concatOp, settings, preferStriping)
			{
				this._leftChildCount = leftChildQueryResults.ElementsCount;
				this._rightChildCount = rightChildQueryResults.ElementsCount;
			}

			// Token: 0x1700012D RID: 301
			// (get) Token: 0x06000859 RID: 2137 RVA: 0x00009CDF File Offset: 0x00007EDF
			internal override bool IsIndexible
			{
				get
				{
					return true;
				}
			}

			// Token: 0x1700012E RID: 302
			// (get) Token: 0x0600085A RID: 2138 RVA: 0x00019D01 File Offset: 0x00017F01
			internal override int ElementsCount
			{
				get
				{
					return this._leftChildCount + this._rightChildCount;
				}
			}

			// Token: 0x0600085B RID: 2139 RVA: 0x00019D10 File Offset: 0x00017F10
			internal override TSource GetElement(int index)
			{
				if (index < this._leftChildCount)
				{
					return this._leftChildQueryResults.GetElement(index);
				}
				return this._rightChildQueryResults.GetElement(index - this._leftChildCount);
			}

			// Token: 0x040005CC RID: 1484
			private int _leftChildCount;

			// Token: 0x040005CD RID: 1485
			private int _rightChildCount;
		}
	}
}
