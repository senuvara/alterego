﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000156 RID: 342
	internal sealed class NullableFloatSumAggregationOperator : InlinedAggregationOperator<float?, double?, float?>
	{
		// Token: 0x0600095A RID: 2394 RVA: 0x0001DF0E File Offset: 0x0001C10E
		internal NullableFloatSumAggregationOperator(IEnumerable<float?> child) : base(child)
		{
		}

		// Token: 0x0600095B RID: 2395 RVA: 0x0001DF18 File Offset: 0x0001C118
		protected override float? InternalAggregate(ref Exception singularExceptionToThrow)
		{
			float? result;
			using (IEnumerator<double?> enumerator = this.GetEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered), true))
			{
				double num = 0.0;
				while (enumerator.MoveNext())
				{
					double num2 = num;
					double? num3 = enumerator.Current;
					num = num2 + num3.GetValueOrDefault();
				}
				result = new float?((float)num);
			}
			return result;
		}

		// Token: 0x0600095C RID: 2396 RVA: 0x0001DF80 File Offset: 0x0001C180
		protected override QueryOperatorEnumerator<double?, int> CreateEnumerator<TKey>(int index, int count, QueryOperatorEnumerator<float?, TKey> source, object sharedData, CancellationToken cancellationToken)
		{
			return new NullableFloatSumAggregationOperator.NullableFloatSumAggregationOperatorEnumerator<TKey>(source, index, cancellationToken);
		}

		// Token: 0x02000157 RID: 343
		private class NullableFloatSumAggregationOperatorEnumerator<TKey> : InlinedAggregationOperatorEnumerator<double?>
		{
			// Token: 0x0600095D RID: 2397 RVA: 0x0001DF8B File Offset: 0x0001C18B
			internal NullableFloatSumAggregationOperatorEnumerator(QueryOperatorEnumerator<float?, TKey> source, int partitionIndex, CancellationToken cancellationToken) : base(partitionIndex, cancellationToken)
			{
				this._source = source;
			}

			// Token: 0x0600095E RID: 2398 RVA: 0x0001DF9C File Offset: 0x0001C19C
			protected override bool MoveNextCore(ref double? currentElement)
			{
				float? num = null;
				TKey tkey = default(TKey);
				QueryOperatorEnumerator<float?, TKey> source = this._source;
				if (source.MoveNext(ref num, ref tkey))
				{
					double num2 = 0.0;
					int num3 = 0;
					do
					{
						if ((num3++ & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(this._cancellationToken);
						}
						num2 += (double)num.GetValueOrDefault();
					}
					while (source.MoveNext(ref num, ref tkey));
					currentElement = new double?(num2);
					return true;
				}
				return false;
			}

			// Token: 0x0600095F RID: 2399 RVA: 0x0001E016 File Offset: 0x0001C216
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x04000662 RID: 1634
			private readonly QueryOperatorEnumerator<float?, TKey> _source;
		}
	}
}
