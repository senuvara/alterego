﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000195 RID: 405
	internal sealed class IndexedSelectQueryOperator<TInput, TOutput> : UnaryQueryOperator<TInput, TOutput>
	{
		// Token: 0x06000A74 RID: 2676 RVA: 0x00021305 File Offset: 0x0001F505
		internal IndexedSelectQueryOperator(IEnumerable<TInput> child, Func<TInput, int, TOutput> selector) : base(child)
		{
			this._selector = selector;
			this._outputOrdered = true;
			this.InitOrdinalIndexState();
		}

		// Token: 0x06000A75 RID: 2677 RVA: 0x00021324 File Offset: 0x0001F524
		private void InitOrdinalIndexState()
		{
			OrdinalIndexState ordinalIndexState = base.Child.OrdinalIndexState;
			OrdinalIndexState ordinalIndexState2 = ordinalIndexState;
			if (ordinalIndexState.IsWorseThan(OrdinalIndexState.Correct))
			{
				this._prematureMerge = true;
				this._limitsParallelism = (ordinalIndexState != OrdinalIndexState.Shuffled);
				ordinalIndexState2 = OrdinalIndexState.Correct;
			}
			base.SetOrdinalIndexState(ordinalIndexState2);
		}

		// Token: 0x06000A76 RID: 2678 RVA: 0x00021365 File Offset: 0x0001F565
		internal override QueryResults<TOutput> Open(QuerySettings settings, bool preferStriping)
		{
			return IndexedSelectQueryOperator<TInput, TOutput>.IndexedSelectQueryOperatorResults.NewResults(base.Child.Open(settings, preferStriping), this, settings, preferStriping);
		}

		// Token: 0x06000A77 RID: 2679 RVA: 0x0002137C File Offset: 0x0001F57C
		internal override void WrapPartitionedStream<TKey>(PartitionedStream<TInput, TKey> inputStream, IPartitionedStreamRecipient<TOutput> recipient, bool preferStriping, QuerySettings settings)
		{
			int partitionCount = inputStream.PartitionCount;
			PartitionedStream<TInput, int> partitionedStream;
			if (this._prematureMerge)
			{
				partitionedStream = QueryOperator<TInput>.ExecuteAndCollectResults<TKey>(inputStream, partitionCount, base.Child.OutputOrdered, preferStriping, settings).GetPartitionedStream();
			}
			else
			{
				partitionedStream = (PartitionedStream<TInput, int>)inputStream;
			}
			PartitionedStream<TOutput, int> partitionedStream2 = new PartitionedStream<TOutput, int>(partitionCount, Util.GetDefaultComparer<int>(), this.OrdinalIndexState);
			for (int i = 0; i < partitionCount; i++)
			{
				partitionedStream2[i] = new IndexedSelectQueryOperator<TInput, TOutput>.IndexedSelectQueryOperatorEnumerator(partitionedStream[i], this._selector);
			}
			recipient.Receive<int>(partitionedStream2);
		}

		// Token: 0x1700016E RID: 366
		// (get) Token: 0x06000A78 RID: 2680 RVA: 0x000213FA File Offset: 0x0001F5FA
		internal override bool LimitsParallelism
		{
			get
			{
				return this._limitsParallelism;
			}
		}

		// Token: 0x06000A79 RID: 2681 RVA: 0x00021402 File Offset: 0x0001F602
		internal override IEnumerable<TOutput> AsSequentialQuery(CancellationToken token)
		{
			return base.Child.AsSequentialQuery(token).Select(this._selector);
		}

		// Token: 0x040006FF RID: 1791
		private readonly Func<TInput, int, TOutput> _selector;

		// Token: 0x04000700 RID: 1792
		private bool _prematureMerge;

		// Token: 0x04000701 RID: 1793
		private bool _limitsParallelism;

		// Token: 0x02000196 RID: 406
		private class IndexedSelectQueryOperatorEnumerator : QueryOperatorEnumerator<TOutput, int>
		{
			// Token: 0x06000A7A RID: 2682 RVA: 0x0002141B File Offset: 0x0001F61B
			internal IndexedSelectQueryOperatorEnumerator(QueryOperatorEnumerator<TInput, int> source, Func<TInput, int, TOutput> selector)
			{
				this._source = source;
				this._selector = selector;
			}

			// Token: 0x06000A7B RID: 2683 RVA: 0x00021434 File Offset: 0x0001F634
			internal override bool MoveNext(ref TOutput currentElement, ref int currentKey)
			{
				TInput arg = default(TInput);
				if (this._source.MoveNext(ref arg, ref currentKey))
				{
					currentElement = this._selector(arg, currentKey);
					return true;
				}
				return false;
			}

			// Token: 0x06000A7C RID: 2684 RVA: 0x00021470 File Offset: 0x0001F670
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x04000702 RID: 1794
			private readonly QueryOperatorEnumerator<TInput, int> _source;

			// Token: 0x04000703 RID: 1795
			private readonly Func<TInput, int, TOutput> _selector;
		}

		// Token: 0x02000197 RID: 407
		private class IndexedSelectQueryOperatorResults : UnaryQueryOperator<TInput, TOutput>.UnaryQueryOperatorResults
		{
			// Token: 0x06000A7D RID: 2685 RVA: 0x0002147D File Offset: 0x0001F67D
			public static QueryResults<TOutput> NewResults(QueryResults<TInput> childQueryResults, IndexedSelectQueryOperator<TInput, TOutput> op, QuerySettings settings, bool preferStriping)
			{
				if (childQueryResults.IsIndexible)
				{
					return new IndexedSelectQueryOperator<TInput, TOutput>.IndexedSelectQueryOperatorResults(childQueryResults, op, settings, preferStriping);
				}
				return new UnaryQueryOperator<TInput, TOutput>.UnaryQueryOperatorResults(childQueryResults, op, settings, preferStriping);
			}

			// Token: 0x06000A7E RID: 2686 RVA: 0x0002149A File Offset: 0x0001F69A
			private IndexedSelectQueryOperatorResults(QueryResults<TInput> childQueryResults, IndexedSelectQueryOperator<TInput, TOutput> op, QuerySettings settings, bool preferStriping) : base(childQueryResults, op, settings, preferStriping)
			{
				this._selectOp = op;
				this._childCount = this._childQueryResults.ElementsCount;
			}

			// Token: 0x1700016F RID: 367
			// (get) Token: 0x06000A7F RID: 2687 RVA: 0x000214BF File Offset: 0x0001F6BF
			internal override int ElementsCount
			{
				get
				{
					return this._childQueryResults.ElementsCount;
				}
			}

			// Token: 0x17000170 RID: 368
			// (get) Token: 0x06000A80 RID: 2688 RVA: 0x00009CDF File Offset: 0x00007EDF
			internal override bool IsIndexible
			{
				get
				{
					return true;
				}
			}

			// Token: 0x06000A81 RID: 2689 RVA: 0x000214CC File Offset: 0x0001F6CC
			internal override TOutput GetElement(int index)
			{
				return this._selectOp._selector(this._childQueryResults.GetElement(index), index);
			}

			// Token: 0x04000704 RID: 1796
			private IndexedSelectQueryOperator<TInput, TOutput> _selectOp;

			// Token: 0x04000705 RID: 1797
			private int _childCount;
		}
	}
}
