﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000198 RID: 408
	internal sealed class IndexedWhereQueryOperator<TInputOutput> : UnaryQueryOperator<TInputOutput, TInputOutput>
	{
		// Token: 0x06000A82 RID: 2690 RVA: 0x000214EB File Offset: 0x0001F6EB
		internal IndexedWhereQueryOperator(IEnumerable<TInputOutput> child, Func<TInputOutput, int, bool> predicate) : base(child)
		{
			this._predicate = predicate;
			this._outputOrdered = true;
			this.InitOrdinalIndexState();
		}

		// Token: 0x06000A83 RID: 2691 RVA: 0x00021508 File Offset: 0x0001F708
		private void InitOrdinalIndexState()
		{
			OrdinalIndexState ordinalIndexState = base.Child.OrdinalIndexState;
			if (ordinalIndexState.IsWorseThan(OrdinalIndexState.Correct))
			{
				this._prematureMerge = true;
				this._limitsParallelism = (ordinalIndexState != OrdinalIndexState.Shuffled);
			}
			base.SetOrdinalIndexState(OrdinalIndexState.Increasing);
		}

		// Token: 0x06000A84 RID: 2692 RVA: 0x0001FDEF File Offset: 0x0001DFEF
		internal override QueryResults<TInputOutput> Open(QuerySettings settings, bool preferStriping)
		{
			return new UnaryQueryOperator<TInputOutput, TInputOutput>.UnaryQueryOperatorResults(base.Child.Open(settings, preferStriping), this, settings, preferStriping);
		}

		// Token: 0x06000A85 RID: 2693 RVA: 0x00021548 File Offset: 0x0001F748
		internal override void WrapPartitionedStream<TKey>(PartitionedStream<TInputOutput, TKey> inputStream, IPartitionedStreamRecipient<TInputOutput> recipient, bool preferStriping, QuerySettings settings)
		{
			int partitionCount = inputStream.PartitionCount;
			PartitionedStream<TInputOutput, int> partitionedStream;
			if (this._prematureMerge)
			{
				partitionedStream = QueryOperator<TInputOutput>.ExecuteAndCollectResults<TKey>(inputStream, partitionCount, base.Child.OutputOrdered, preferStriping, settings).GetPartitionedStream();
			}
			else
			{
				partitionedStream = (PartitionedStream<TInputOutput, int>)inputStream;
			}
			PartitionedStream<TInputOutput, int> partitionedStream2 = new PartitionedStream<TInputOutput, int>(partitionCount, Util.GetDefaultComparer<int>(), this.OrdinalIndexState);
			for (int i = 0; i < partitionCount; i++)
			{
				partitionedStream2[i] = new IndexedWhereQueryOperator<TInputOutput>.IndexedWhereQueryOperatorEnumerator(partitionedStream[i], this._predicate, settings.CancellationState.MergedCancellationToken);
			}
			recipient.Receive<int>(partitionedStream2);
		}

		// Token: 0x06000A86 RID: 2694 RVA: 0x000215D2 File Offset: 0x0001F7D2
		internal override IEnumerable<TInputOutput> AsSequentialQuery(CancellationToken token)
		{
			return CancellableEnumerable.Wrap<TInputOutput>(base.Child.AsSequentialQuery(token), token).Where(this._predicate);
		}

		// Token: 0x17000171 RID: 369
		// (get) Token: 0x06000A87 RID: 2695 RVA: 0x000215F1 File Offset: 0x0001F7F1
		internal override bool LimitsParallelism
		{
			get
			{
				return this._limitsParallelism;
			}
		}

		// Token: 0x04000706 RID: 1798
		private Func<TInputOutput, int, bool> _predicate;

		// Token: 0x04000707 RID: 1799
		private bool _prematureMerge;

		// Token: 0x04000708 RID: 1800
		private bool _limitsParallelism;

		// Token: 0x02000199 RID: 409
		private class IndexedWhereQueryOperatorEnumerator : QueryOperatorEnumerator<TInputOutput, int>
		{
			// Token: 0x06000A88 RID: 2696 RVA: 0x000215F9 File Offset: 0x0001F7F9
			internal IndexedWhereQueryOperatorEnumerator(QueryOperatorEnumerator<TInputOutput, int> source, Func<TInputOutput, int, bool> predicate, CancellationToken cancellationToken)
			{
				this._source = source;
				this._predicate = predicate;
				this._cancellationToken = cancellationToken;
			}

			// Token: 0x06000A89 RID: 2697 RVA: 0x00021618 File Offset: 0x0001F818
			internal override bool MoveNext(ref TInputOutput currentElement, ref int currentKey)
			{
				if (this._outputLoopCount == null)
				{
					this._outputLoopCount = new Shared<int>(0);
				}
				while (this._source.MoveNext(ref currentElement, ref currentKey))
				{
					Shared<int> outputLoopCount = this._outputLoopCount;
					int value = outputLoopCount.Value;
					outputLoopCount.Value = value + 1;
					if ((value & 63) == 0)
					{
						CancellationState.ThrowIfCanceled(this._cancellationToken);
					}
					if (this._predicate(currentElement, currentKey))
					{
						return true;
					}
				}
				return false;
			}

			// Token: 0x06000A8A RID: 2698 RVA: 0x00021688 File Offset: 0x0001F888
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x04000709 RID: 1801
			private readonly QueryOperatorEnumerator<TInputOutput, int> _source;

			// Token: 0x0400070A RID: 1802
			private readonly Func<TInputOutput, int, bool> _predicate;

			// Token: 0x0400070B RID: 1803
			private CancellationToken _cancellationToken;

			// Token: 0x0400070C RID: 1804
			private Shared<int> _outputLoopCount;
		}
	}
}
