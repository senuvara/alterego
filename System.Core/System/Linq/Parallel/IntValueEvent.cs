﻿using System;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x020001D8 RID: 472
	internal class IntValueEvent : ManualResetEventSlim
	{
		// Token: 0x06000B85 RID: 2949 RVA: 0x00024F47 File Offset: 0x00023147
		internal IntValueEvent() : base(false)
		{
			this.Value = 0;
		}

		// Token: 0x06000B86 RID: 2950 RVA: 0x00024F57 File Offset: 0x00023157
		internal void Set(int index)
		{
			this.Value = index;
			base.Set();
		}

		// Token: 0x040007E9 RID: 2025
		internal int Value;
	}
}
