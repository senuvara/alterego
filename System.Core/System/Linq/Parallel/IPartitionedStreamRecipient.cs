﻿using System;

namespace System.Linq.Parallel
{
	// Token: 0x020000F8 RID: 248
	internal interface IPartitionedStreamRecipient<TElement>
	{
		// Token: 0x06000823 RID: 2083
		void Receive<TKey>(PartitionedStream<TElement, TKey> partitionedStream);
	}
}
