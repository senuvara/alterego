﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace System.Linq.Parallel
{
	// Token: 0x020001DB RID: 475
	internal class Lookup<TKey, TElement> : ILookup<TKey, TElement>, IEnumerable<IGrouping<TKey, TElement>>, IEnumerable
	{
		// Token: 0x06000B93 RID: 2963 RVA: 0x000250D7 File Offset: 0x000232D7
		internal Lookup(IEqualityComparer<TKey> comparer)
		{
			this._comparer = comparer;
			this._dict = new Dictionary<TKey, IGrouping<TKey, TElement>>(this._comparer);
		}

		// Token: 0x1700019A RID: 410
		// (get) Token: 0x06000B94 RID: 2964 RVA: 0x000250F8 File Offset: 0x000232F8
		public int Count
		{
			get
			{
				int num = this._dict.Count;
				if (this._defaultKeyGrouping != null)
				{
					num++;
				}
				return num;
			}
		}

		// Token: 0x1700019B RID: 411
		public IEnumerable<TElement> this[TKey key]
		{
			get
			{
				if (this._comparer.Equals(key, default(TKey)))
				{
					if (this._defaultKeyGrouping != null)
					{
						return this._defaultKeyGrouping;
					}
					return Enumerable.Empty<TElement>();
				}
				else
				{
					IGrouping<TKey, TElement> result;
					if (this._dict.TryGetValue(key, out result))
					{
						return result;
					}
					return Enumerable.Empty<TElement>();
				}
			}
		}

		// Token: 0x06000B96 RID: 2966 RVA: 0x00025170 File Offset: 0x00023370
		public bool Contains(TKey key)
		{
			if (this._comparer.Equals(key, default(TKey)))
			{
				return this._defaultKeyGrouping != null;
			}
			return this._dict.ContainsKey(key);
		}

		// Token: 0x06000B97 RID: 2967 RVA: 0x000251AC File Offset: 0x000233AC
		internal void Add(IGrouping<TKey, TElement> grouping)
		{
			if (this._comparer.Equals(grouping.Key, default(TKey)))
			{
				this._defaultKeyGrouping = grouping;
				return;
			}
			this._dict.Add(grouping.Key, grouping);
		}

		// Token: 0x06000B98 RID: 2968 RVA: 0x000251EF File Offset: 0x000233EF
		public IEnumerator<IGrouping<TKey, TElement>> GetEnumerator()
		{
			foreach (IGrouping<TKey, TElement> grouping in this._dict.Values)
			{
				yield return grouping;
			}
			IEnumerator<IGrouping<TKey, TElement>> enumerator = null;
			if (this._defaultKeyGrouping != null)
			{
				yield return this._defaultKeyGrouping;
			}
			yield break;
			yield break;
		}

		// Token: 0x06000B99 RID: 2969 RVA: 0x000251FE File Offset: 0x000233FE
		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable<IGrouping<TKey, TElement>>)this).GetEnumerator();
		}

		// Token: 0x040007F3 RID: 2035
		private IDictionary<TKey, IGrouping<TKey, TElement>> _dict;

		// Token: 0x040007F4 RID: 2036
		private IEqualityComparer<TKey> _comparer;

		// Token: 0x040007F5 RID: 2037
		private IGrouping<TKey, TElement> _defaultKeyGrouping;

		// Token: 0x020001DC RID: 476
		[CompilerGenerated]
		private sealed class <GetEnumerator>d__10 : IEnumerator<IGrouping<TKey, TElement>>, IDisposable, IEnumerator
		{
			// Token: 0x06000B9A RID: 2970 RVA: 0x00025206 File Offset: 0x00023406
			[DebuggerHidden]
			public <GetEnumerator>d__10(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x06000B9B RID: 2971 RVA: 0x00025218 File Offset: 0x00023418
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x06000B9C RID: 2972 RVA: 0x00025250 File Offset: 0x00023450
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					Lookup<TKey, TElement> lookup = this;
					switch (num)
					{
					case 0:
						this.<>1__state = -1;
						enumerator = lookup._dict.Values.GetEnumerator();
						this.<>1__state = -3;
						break;
					case 1:
						this.<>1__state = -3;
						break;
					case 2:
						this.<>1__state = -1;
						goto IL_B4;
					default:
						return false;
					}
					if (enumerator.MoveNext())
					{
						IGrouping<TKey, TElement> grouping = enumerator.Current;
						this.<>2__current = grouping;
						this.<>1__state = 1;
						return true;
					}
					this.<>m__Finally1();
					enumerator = null;
					if (lookup._defaultKeyGrouping != null)
					{
						this.<>2__current = lookup._defaultKeyGrouping;
						this.<>1__state = 2;
						return true;
					}
					IL_B4:
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x06000B9D RID: 2973 RVA: 0x00025330 File Offset: 0x00023530
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x1700019C RID: 412
			// (get) Token: 0x06000B9E RID: 2974 RVA: 0x0002534C File Offset: 0x0002354C
			IGrouping<TKey, TElement> IEnumerator<IGrouping<!0, !1>>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000B9F RID: 2975 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700019D RID: 413
			// (get) Token: 0x06000BA0 RID: 2976 RVA: 0x0002534C File Offset: 0x0002354C
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x040007F6 RID: 2038
			private int <>1__state;

			// Token: 0x040007F7 RID: 2039
			private IGrouping<TKey, TElement> <>2__current;

			// Token: 0x040007F8 RID: 2040
			public Lookup<TKey, TElement> <>4__this;

			// Token: 0x040007F9 RID: 2041
			private IEnumerator<IGrouping<TKey, TElement>> <>7__wrap1;
		}
	}
}
