﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000179 RID: 377
	internal sealed class ContainsSearchOperator<TInput> : UnaryQueryOperator<TInput, bool>
	{
		// Token: 0x06000A0F RID: 2575 RVA: 0x0001FBFD File Offset: 0x0001DDFD
		internal ContainsSearchOperator(IEnumerable<TInput> child, TInput searchValue, IEqualityComparer<TInput> comparer) : base(child)
		{
			this._searchValue = searchValue;
			if (comparer == null)
			{
				this._comparer = EqualityComparer<TInput>.Default;
				return;
			}
			this._comparer = comparer;
		}

		// Token: 0x06000A10 RID: 2576 RVA: 0x0001FC24 File Offset: 0x0001DE24
		internal bool Aggregate()
		{
			using (IEnumerator<bool> enumerator = this.GetEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered), true))
			{
				while (enumerator.MoveNext())
				{
					if (enumerator.Current)
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x06000A11 RID: 2577 RVA: 0x0001FA84 File Offset: 0x0001DC84
		internal override QueryResults<bool> Open(QuerySettings settings, bool preferStriping)
		{
			return new UnaryQueryOperator<TInput, bool>.UnaryQueryOperatorResults(base.Child.Open(settings, preferStriping), this, settings, preferStriping);
		}

		// Token: 0x06000A12 RID: 2578 RVA: 0x0001FC74 File Offset: 0x0001DE74
		internal override void WrapPartitionedStream<TKey>(PartitionedStream<TInput, TKey> inputStream, IPartitionedStreamRecipient<bool> recipient, bool preferStriping, QuerySettings settings)
		{
			int partitionCount = inputStream.PartitionCount;
			PartitionedStream<bool, int> partitionedStream = new PartitionedStream<bool, int>(partitionCount, Util.GetDefaultComparer<int>(), OrdinalIndexState.Correct);
			Shared<bool> resultFoundFlag = new Shared<bool>(false);
			for (int i = 0; i < partitionCount; i++)
			{
				partitionedStream[i] = new ContainsSearchOperator<TInput>.ContainsSearchOperatorEnumerator<TKey>(inputStream[i], this._searchValue, this._comparer, i, resultFoundFlag, settings.CancellationState.MergedCancellationToken);
			}
			recipient.Receive<int>(partitionedStream);
		}

		// Token: 0x06000A13 RID: 2579 RVA: 0x00003A6B File Offset: 0x00001C6B
		[ExcludeFromCodeCoverage]
		internal override IEnumerable<bool> AsSequentialQuery(CancellationToken token)
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000163 RID: 355
		// (get) Token: 0x06000A14 RID: 2580 RVA: 0x00002285 File Offset: 0x00000485
		internal override bool LimitsParallelism
		{
			get
			{
				return false;
			}
		}

		// Token: 0x040006AB RID: 1707
		private readonly TInput _searchValue;

		// Token: 0x040006AC RID: 1708
		private readonly IEqualityComparer<TInput> _comparer;

		// Token: 0x0200017A RID: 378
		private class ContainsSearchOperatorEnumerator<TKey> : QueryOperatorEnumerator<bool, int>
		{
			// Token: 0x06000A15 RID: 2581 RVA: 0x0001FCDC File Offset: 0x0001DEDC
			internal ContainsSearchOperatorEnumerator(QueryOperatorEnumerator<TInput, TKey> source, TInput searchValue, IEqualityComparer<TInput> comparer, int partitionIndex, Shared<bool> resultFoundFlag, CancellationToken cancellationToken)
			{
				this._source = source;
				this._searchValue = searchValue;
				this._comparer = comparer;
				this._partitionIndex = partitionIndex;
				this._resultFoundFlag = resultFoundFlag;
				this._cancellationToken = cancellationToken;
			}

			// Token: 0x06000A16 RID: 2582 RVA: 0x0001FD14 File Offset: 0x0001DF14
			internal override bool MoveNext(ref bool currentElement, ref int currentKey)
			{
				if (this._resultFoundFlag.Value)
				{
					return false;
				}
				TInput x = default(TInput);
				TKey tkey = default(TKey);
				if (this._source.MoveNext(ref x, ref tkey))
				{
					currentElement = false;
					currentKey = this._partitionIndex;
					int num = 0;
					for (;;)
					{
						if ((num++ & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(this._cancellationToken);
						}
						if (this._resultFoundFlag.Value)
						{
							break;
						}
						if (this._comparer.Equals(x, this._searchValue))
						{
							goto Block_5;
						}
						if (!this._source.MoveNext(ref x, ref tkey))
						{
							return true;
						}
					}
					return false;
					Block_5:
					this._resultFoundFlag.Value = true;
					currentElement = true;
					return true;
				}
				return false;
			}

			// Token: 0x06000A17 RID: 2583 RVA: 0x0001FDBB File Offset: 0x0001DFBB
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x040006AD RID: 1709
			private readonly QueryOperatorEnumerator<TInput, TKey> _source;

			// Token: 0x040006AE RID: 1710
			private readonly TInput _searchValue;

			// Token: 0x040006AF RID: 1711
			private readonly IEqualityComparer<TInput> _comparer;

			// Token: 0x040006B0 RID: 1712
			private readonly int _partitionIndex;

			// Token: 0x040006B1 RID: 1713
			private readonly Shared<bool> _resultFoundFlag;

			// Token: 0x040006B2 RID: 1714
			private CancellationToken _cancellationToken;
		}
	}
}
