﻿using System;

namespace System.Linq.Parallel
{
	// Token: 0x020001BF RID: 447
	internal static class QueryLifecycle
	{
		// Token: 0x06000B15 RID: 2837 RVA: 0x00023A7C File Offset: 0x00021C7C
		internal static void LogicalQueryExecutionBegin(int queryID)
		{
			PlinqEtwProvider.Log.ParallelQueryBegin(queryID);
		}

		// Token: 0x06000B16 RID: 2838 RVA: 0x00023A89 File Offset: 0x00021C89
		internal static void LogicalQueryExecutionEnd(int queryID)
		{
			PlinqEtwProvider.Log.ParallelQueryEnd(queryID);
		}
	}
}
