﻿using System;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000137 RID: 311
	internal abstract class InlinedAggregationOperatorEnumerator<TIntermediate> : QueryOperatorEnumerator<TIntermediate, int>
	{
		// Token: 0x060008FD RID: 2301 RVA: 0x0001C867 File Offset: 0x0001AA67
		internal InlinedAggregationOperatorEnumerator(int partitionIndex, CancellationToken cancellationToken)
		{
			this._partitionIndex = partitionIndex;
			this._cancellationToken = cancellationToken;
		}

		// Token: 0x060008FE RID: 2302 RVA: 0x0001C87D File Offset: 0x0001AA7D
		internal sealed override bool MoveNext(ref TIntermediate currentElement, ref int currentKey)
		{
			if (!this._done && this.MoveNextCore(ref currentElement))
			{
				currentKey = this._partitionIndex;
				this._done = true;
				return true;
			}
			return false;
		}

		// Token: 0x060008FF RID: 2303
		protected abstract bool MoveNextCore(ref TIntermediate currentElement);

		// Token: 0x04000646 RID: 1606
		private int _partitionIndex;

		// Token: 0x04000647 RID: 1607
		private bool _done;

		// Token: 0x04000648 RID: 1608
		protected CancellationToken _cancellationToken;
	}
}
