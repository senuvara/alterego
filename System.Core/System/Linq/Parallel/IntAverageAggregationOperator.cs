﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000138 RID: 312
	internal sealed class IntAverageAggregationOperator : InlinedAggregationOperator<int, Pair<long, long>, double>
	{
		// Token: 0x06000900 RID: 2304 RVA: 0x0001C8A2 File Offset: 0x0001AAA2
		internal IntAverageAggregationOperator(IEnumerable<int> child) : base(child)
		{
		}

		// Token: 0x06000901 RID: 2305 RVA: 0x0001C8AC File Offset: 0x0001AAAC
		protected override double InternalAggregate(ref Exception singularExceptionToThrow)
		{
			checked
			{
				double result;
				using (IEnumerator<Pair<long, long>> enumerator = this.GetEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered), true))
				{
					if (!enumerator.MoveNext())
					{
						singularExceptionToThrow = new InvalidOperationException("Sequence contains no elements");
						result = 0.0;
					}
					else
					{
						Pair<long, long> pair = enumerator.Current;
						while (enumerator.MoveNext())
						{
							long first = pair.First;
							Pair<long, long> pair2 = enumerator.Current;
							pair.First = first + pair2.First;
							long second = pair.Second;
							pair2 = enumerator.Current;
							pair.Second = second + pair2.Second;
						}
						result = (double)pair.First / (double)pair.Second;
					}
				}
				return result;
			}
		}

		// Token: 0x06000902 RID: 2306 RVA: 0x0001C960 File Offset: 0x0001AB60
		protected override QueryOperatorEnumerator<Pair<long, long>, int> CreateEnumerator<TKey>(int index, int count, QueryOperatorEnumerator<int, TKey> source, object sharedData, CancellationToken cancellationToken)
		{
			return new IntAverageAggregationOperator.IntAverageAggregationOperatorEnumerator<TKey>(source, index, cancellationToken);
		}

		// Token: 0x02000139 RID: 313
		private class IntAverageAggregationOperatorEnumerator<TKey> : InlinedAggregationOperatorEnumerator<Pair<long, long>>
		{
			// Token: 0x06000903 RID: 2307 RVA: 0x0001C96B File Offset: 0x0001AB6B
			internal IntAverageAggregationOperatorEnumerator(QueryOperatorEnumerator<int, TKey> source, int partitionIndex, CancellationToken cancellationToken) : base(partitionIndex, cancellationToken)
			{
				this._source = source;
			}

			// Token: 0x06000904 RID: 2308 RVA: 0x0001C97C File Offset: 0x0001AB7C
			protected override bool MoveNextCore(ref Pair<long, long> currentElement)
			{
				long num = 0L;
				long num2 = 0L;
				QueryOperatorEnumerator<int, TKey> source = this._source;
				int num3 = 0;
				TKey tkey = default(TKey);
				if (source.MoveNext(ref num3, ref tkey))
				{
					int num4 = 0;
					do
					{
						if ((num4++ & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(this._cancellationToken);
						}
						checked
						{
							num += unchecked((long)num3);
							num2 += 1L;
						}
					}
					while (source.MoveNext(ref num3, ref tkey));
					currentElement = new Pair<long, long>(num, num2);
					return true;
				}
				return false;
			}

			// Token: 0x06000905 RID: 2309 RVA: 0x0001C9EC File Offset: 0x0001ABEC
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x04000649 RID: 1609
			private QueryOperatorEnumerator<int, TKey> _source;
		}
	}
}
