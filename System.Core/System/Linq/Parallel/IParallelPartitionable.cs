﻿using System;

namespace System.Linq.Parallel
{
	// Token: 0x020000DC RID: 220
	internal interface IParallelPartitionable<T>
	{
		// Token: 0x060007C8 RID: 1992
		QueryOperatorEnumerator<T, int>[] GetPartitions(int partitionCount);
	}
}
