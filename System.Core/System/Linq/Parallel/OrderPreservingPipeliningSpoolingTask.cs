﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace System.Linq.Parallel
{
	// Token: 0x020001BB RID: 443
	internal class OrderPreservingPipeliningSpoolingTask<TOutput, TKey> : SpoolingTaskBase
	{
		// Token: 0x06000B0A RID: 2826 RVA: 0x000235A8 File Offset: 0x000217A8
		internal OrderPreservingPipeliningSpoolingTask(QueryOperatorEnumerator<TOutput, TKey> partition, QueryTaskGroupState taskGroupState, bool[] consumerWaiting, bool[] producerWaiting, bool[] producerDone, int partitionIndex, Queue<Pair<TKey, TOutput>>[] buffers, object bufferLock, bool autoBuffered) : base(partitionIndex, taskGroupState)
		{
			this._partition = partition;
			this._taskGroupState = taskGroupState;
			this._producerDone = producerDone;
			this._consumerWaiting = consumerWaiting;
			this._producerWaiting = producerWaiting;
			this._partitionIndex = partitionIndex;
			this._buffers = buffers;
			this._bufferLock = bufferLock;
			this._autoBuffered = autoBuffered;
		}

		// Token: 0x06000B0B RID: 2827 RVA: 0x00023604 File Offset: 0x00021804
		protected override void SpoolingWork()
		{
			TOutput second = default(TOutput);
			TKey first = default(TKey);
			int num = this._autoBuffered ? 16 : 1;
			Pair<TKey, TOutput>[] array = new Pair<TKey, TOutput>[num];
			QueryOperatorEnumerator<TOutput, TKey> partition = this._partition;
			CancellationToken mergedCancellationToken = this._taskGroupState.CancellationState.MergedCancellationToken;
			int num2;
			do
			{
				num2 = 0;
				while (num2 < num && partition.MoveNext(ref second, ref first))
				{
					array[num2] = new Pair<TKey, TOutput>(first, second);
					num2++;
				}
				if (num2 == 0)
				{
					break;
				}
				object bufferLock = this._bufferLock;
				lock (bufferLock)
				{
					if (mergedCancellationToken.IsCancellationRequested)
					{
						break;
					}
					for (int i = 0; i < num2; i++)
					{
						this._buffers[this._partitionIndex].Enqueue(array[i]);
					}
					if (this._consumerWaiting[this._partitionIndex])
					{
						Monitor.Pulse(this._bufferLock);
						this._consumerWaiting[this._partitionIndex] = false;
					}
					if (this._buffers[this._partitionIndex].Count >= 8192)
					{
						this._producerWaiting[this._partitionIndex] = true;
						Monitor.Wait(this._bufferLock);
					}
				}
			}
			while (num2 == num);
		}

		// Token: 0x06000B0C RID: 2828 RVA: 0x00023754 File Offset: 0x00021954
		public static void Spool(QueryTaskGroupState groupState, PartitionedStream<TOutput, TKey> partitions, bool[] consumerWaiting, bool[] producerWaiting, bool[] producerDone, Queue<Pair<TKey, TOutput>>[] buffers, object[] bufferLocks, TaskScheduler taskScheduler, bool autoBuffered)
		{
			int degreeOfParallelism = partitions.PartitionCount;
			for (int i = 0; i < degreeOfParallelism; i++)
			{
				buffers[i] = new Queue<Pair<TKey, TOutput>>(128);
				bufferLocks[i] = new object();
			}
			Task task = new Task(delegate()
			{
				for (int j = 0; j < degreeOfParallelism; j++)
				{
					new OrderPreservingPipeliningSpoolingTask<TOutput, TKey>(partitions[j], groupState, consumerWaiting, producerWaiting, producerDone, j, buffers, bufferLocks[j], autoBuffered).RunAsynchronously(taskScheduler);
				}
			});
			groupState.QueryBegin(task);
			task.Start(taskScheduler);
		}

		// Token: 0x06000B0D RID: 2829 RVA: 0x00023818 File Offset: 0x00021A18
		protected override void SpoolingFinally()
		{
			object bufferLock = this._bufferLock;
			lock (bufferLock)
			{
				this._producerDone[this._partitionIndex] = true;
				if (this._consumerWaiting[this._partitionIndex])
				{
					Monitor.Pulse(this._bufferLock);
					this._consumerWaiting[this._partitionIndex] = false;
				}
			}
			base.SpoolingFinally();
			this._partition.Dispose();
		}

		// Token: 0x04000783 RID: 1923
		private readonly QueryTaskGroupState _taskGroupState;

		// Token: 0x04000784 RID: 1924
		private readonly QueryOperatorEnumerator<TOutput, TKey> _partition;

		// Token: 0x04000785 RID: 1925
		private readonly bool[] _consumerWaiting;

		// Token: 0x04000786 RID: 1926
		private readonly bool[] _producerWaiting;

		// Token: 0x04000787 RID: 1927
		private readonly bool[] _producerDone;

		// Token: 0x04000788 RID: 1928
		private readonly int _partitionIndex;

		// Token: 0x04000789 RID: 1929
		private readonly Queue<Pair<TKey, TOutput>>[] _buffers;

		// Token: 0x0400078A RID: 1930
		private readonly object _bufferLock;

		// Token: 0x0400078B RID: 1931
		private readonly bool _autoBuffered;

		// Token: 0x0400078C RID: 1932
		private const int PRODUCER_BUFFER_AUTO_SIZE = 16;

		// Token: 0x020001BC RID: 444
		[CompilerGenerated]
		private sealed class <>c__DisplayClass12_0
		{
			// Token: 0x06000B0E RID: 2830 RVA: 0x00002310 File Offset: 0x00000510
			public <>c__DisplayClass12_0()
			{
			}

			// Token: 0x06000B0F RID: 2831 RVA: 0x0002389C File Offset: 0x00021A9C
			internal void <Spool>b__0()
			{
				for (int i = 0; i < this.degreeOfParallelism; i++)
				{
					new OrderPreservingPipeliningSpoolingTask<TOutput, TKey>(this.partitions[i], this.groupState, this.consumerWaiting, this.producerWaiting, this.producerDone, i, this.buffers, this.bufferLocks[i], this.autoBuffered).RunAsynchronously(this.taskScheduler);
				}
			}

			// Token: 0x0400078D RID: 1933
			public PartitionedStream<TOutput, TKey> partitions;

			// Token: 0x0400078E RID: 1934
			public QueryTaskGroupState groupState;

			// Token: 0x0400078F RID: 1935
			public bool[] consumerWaiting;

			// Token: 0x04000790 RID: 1936
			public bool[] producerWaiting;

			// Token: 0x04000791 RID: 1937
			public bool[] producerDone;

			// Token: 0x04000792 RID: 1938
			public Queue<Pair<TKey, TOutput>>[] buffers;

			// Token: 0x04000793 RID: 1939
			public object[] bufferLocks;

			// Token: 0x04000794 RID: 1940
			public bool autoBuffered;

			// Token: 0x04000795 RID: 1941
			public TaskScheduler taskScheduler;

			// Token: 0x04000796 RID: 1942
			public int degreeOfParallelism;
		}
	}
}
