﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000162 RID: 354
	internal sealed class NullableLongSumAggregationOperator : InlinedAggregationOperator<long?, long?, long?>
	{
		// Token: 0x0600097E RID: 2430 RVA: 0x0001E873 File Offset: 0x0001CA73
		internal NullableLongSumAggregationOperator(IEnumerable<long?> child) : base(child)
		{
		}

		// Token: 0x0600097F RID: 2431 RVA: 0x0001E87C File Offset: 0x0001CA7C
		protected override long? InternalAggregate(ref Exception singularExceptionToThrow)
		{
			long? result;
			using (IEnumerator<long?> enumerator = this.GetEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered), true))
			{
				long num = 0L;
				while (enumerator.MoveNext())
				{
					long num2 = num;
					result = enumerator.Current;
					num = checked(num2 + result.GetValueOrDefault());
				}
				result = new long?(num);
			}
			return result;
		}

		// Token: 0x06000980 RID: 2432 RVA: 0x0001E8DC File Offset: 0x0001CADC
		protected override QueryOperatorEnumerator<long?, int> CreateEnumerator<TKey>(int index, int count, QueryOperatorEnumerator<long?, TKey> source, object sharedData, CancellationToken cancellationToken)
		{
			return new NullableLongSumAggregationOperator.NullableLongSumAggregationOperatorEnumerator<TKey>(source, index, cancellationToken);
		}

		// Token: 0x02000163 RID: 355
		private class NullableLongSumAggregationOperatorEnumerator<TKey> : InlinedAggregationOperatorEnumerator<long?>
		{
			// Token: 0x06000981 RID: 2433 RVA: 0x0001E8E7 File Offset: 0x0001CAE7
			internal NullableLongSumAggregationOperatorEnumerator(QueryOperatorEnumerator<long?, TKey> source, int partitionIndex, CancellationToken cancellationToken) : base(partitionIndex, cancellationToken)
			{
				this._source = source;
			}

			// Token: 0x06000982 RID: 2434 RVA: 0x0001E8F8 File Offset: 0x0001CAF8
			protected override bool MoveNextCore(ref long? currentElement)
			{
				long? num = null;
				TKey tkey = default(TKey);
				QueryOperatorEnumerator<long?, TKey> source = this._source;
				if (source.MoveNext(ref num, ref tkey))
				{
					long num2 = 0L;
					int num3 = 0;
					do
					{
						if ((num3++ & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(this._cancellationToken);
						}
						checked
						{
							num2 += num.GetValueOrDefault();
						}
					}
					while (source.MoveNext(ref num, ref tkey));
					currentElement = new long?(num2);
					return true;
				}
				return false;
			}

			// Token: 0x06000983 RID: 2435 RVA: 0x0001E96A File Offset: 0x0001CB6A
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x0400066C RID: 1644
			private readonly QueryOperatorEnumerator<long?, TKey> _source;
		}
	}
}
