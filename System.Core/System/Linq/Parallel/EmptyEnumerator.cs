﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Linq.Parallel
{
	// Token: 0x020000D9 RID: 217
	internal class EmptyEnumerator<T> : QueryOperatorEnumerator<T, int>, IEnumerator<!0>, IDisposable, IEnumerator
	{
		// Token: 0x060007B9 RID: 1977 RVA: 0x00002285 File Offset: 0x00000485
		internal override bool MoveNext(ref T currentElement, ref int currentKey)
		{
			return false;
		}

		// Token: 0x1700011D RID: 285
		// (get) Token: 0x060007BA RID: 1978 RVA: 0x000171CC File Offset: 0x000153CC
		public T Current
		{
			get
			{
				return default(T);
			}
		}

		// Token: 0x1700011E RID: 286
		// (get) Token: 0x060007BB RID: 1979 RVA: 0x000053B1 File Offset: 0x000035B1
		object IEnumerator.Current
		{
			get
			{
				return null;
			}
		}

		// Token: 0x060007BC RID: 1980 RVA: 0x00002285 File Offset: 0x00000485
		public bool MoveNext()
		{
			return false;
		}

		// Token: 0x060007BD RID: 1981 RVA: 0x000039E8 File Offset: 0x00001BE8
		void IEnumerator.Reset()
		{
		}

		// Token: 0x060007BE RID: 1982 RVA: 0x000171E2 File Offset: 0x000153E2
		public EmptyEnumerator()
		{
		}
	}
}
