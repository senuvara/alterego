﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000144 RID: 324
	internal sealed class LongSumAggregationOperator : InlinedAggregationOperator<long, long, long>
	{
		// Token: 0x06000924 RID: 2340 RVA: 0x0001D003 File Offset: 0x0001B203
		internal LongSumAggregationOperator(IEnumerable<long> child) : base(child)
		{
		}

		// Token: 0x06000925 RID: 2341 RVA: 0x0001D00C File Offset: 0x0001B20C
		protected override long InternalAggregate(ref Exception singularExceptionToThrow)
		{
			checked
			{
				long result;
				using (IEnumerator<long> enumerator = this.GetEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered), true))
				{
					long num = 0L;
					while (enumerator.MoveNext())
					{
						long num2 = enumerator.Current;
						num += num2;
					}
					result = num;
				}
				return result;
			}
		}

		// Token: 0x06000926 RID: 2342 RVA: 0x0001D05C File Offset: 0x0001B25C
		protected override QueryOperatorEnumerator<long, int> CreateEnumerator<TKey>(int index, int count, QueryOperatorEnumerator<long, TKey> source, object sharedData, CancellationToken cancellationToken)
		{
			return new LongSumAggregationOperator.LongSumAggregationOperatorEnumerator<TKey>(source, index, cancellationToken);
		}

		// Token: 0x02000145 RID: 325
		private class LongSumAggregationOperatorEnumerator<TKey> : InlinedAggregationOperatorEnumerator<long>
		{
			// Token: 0x06000927 RID: 2343 RVA: 0x0001D067 File Offset: 0x0001B267
			internal LongSumAggregationOperatorEnumerator(QueryOperatorEnumerator<long, TKey> source, int partitionIndex, CancellationToken cancellationToken) : base(partitionIndex, cancellationToken)
			{
				this._source = source;
			}

			// Token: 0x06000928 RID: 2344 RVA: 0x0001D078 File Offset: 0x0001B278
			protected override bool MoveNextCore(ref long currentElement)
			{
				long num = 0L;
				TKey tkey = default(TKey);
				QueryOperatorEnumerator<long, TKey> source = this._source;
				if (source.MoveNext(ref num, ref tkey))
				{
					long num2 = 0L;
					int num3 = 0;
					do
					{
						if ((num3++ & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(this._cancellationToken);
						}
						checked
						{
							num2 += num;
						}
					}
					while (source.MoveNext(ref num, ref tkey));
					currentElement = num2;
					return true;
				}
				return false;
			}

			// Token: 0x06000929 RID: 2345 RVA: 0x0001D0D6 File Offset: 0x0001B2D6
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x04000653 RID: 1619
			private readonly QueryOperatorEnumerator<long, TKey> _source;
		}
	}
}
