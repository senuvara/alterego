﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000187 RID: 391
	internal sealed class GroupByQueryOperator<TSource, TGroupKey, TElement> : UnaryQueryOperator<TSource, IGrouping<TGroupKey, TElement>>
	{
		// Token: 0x06000A48 RID: 2632 RVA: 0x0002094D File Offset: 0x0001EB4D
		internal GroupByQueryOperator(IEnumerable<TSource> child, Func<TSource, TGroupKey> keySelector, Func<TSource, TElement> elementSelector, IEqualityComparer<TGroupKey> keyComparer) : base(child)
		{
			this._keySelector = keySelector;
			this._elementSelector = elementSelector;
			this._keyComparer = keyComparer;
			base.SetOrdinalIndexState(OrdinalIndexState.Shuffled);
		}

		// Token: 0x06000A49 RID: 2633 RVA: 0x00020974 File Offset: 0x0001EB74
		internal override void WrapPartitionedStream<TKey>(PartitionedStream<TSource, TKey> inputStream, IPartitionedStreamRecipient<IGrouping<TGroupKey, TElement>> recipient, bool preferStriping, QuerySettings settings)
		{
			if (base.Child.OutputOrdered)
			{
				this.WrapPartitionedStreamHelperOrdered<TKey>(ExchangeUtilities.HashRepartitionOrdered<TSource, TGroupKey, TKey>(inputStream, this._keySelector, this._keyComparer, null, settings.CancellationState.MergedCancellationToken), recipient, settings.CancellationState.MergedCancellationToken);
				return;
			}
			this.WrapPartitionedStreamHelper<TKey, int>(ExchangeUtilities.HashRepartition<TSource, TGroupKey, TKey>(inputStream, this._keySelector, this._keyComparer, null, settings.CancellationState.MergedCancellationToken), recipient, settings.CancellationState.MergedCancellationToken);
		}

		// Token: 0x06000A4A RID: 2634 RVA: 0x000209F4 File Offset: 0x0001EBF4
		private void WrapPartitionedStreamHelper<TIgnoreKey, TKey>(PartitionedStream<Pair<TSource, TGroupKey>, TKey> hashStream, IPartitionedStreamRecipient<IGrouping<TGroupKey, TElement>> recipient, CancellationToken cancellationToken)
		{
			int partitionCount = hashStream.PartitionCount;
			PartitionedStream<IGrouping<TGroupKey, TElement>, TKey> partitionedStream = new PartitionedStream<IGrouping<TGroupKey, TElement>, TKey>(partitionCount, hashStream.KeyComparer, OrdinalIndexState.Shuffled);
			for (int i = 0; i < partitionCount; i++)
			{
				if (this._elementSelector == null)
				{
					GroupByIdentityQueryOperatorEnumerator<TSource, TGroupKey, TKey> groupByIdentityQueryOperatorEnumerator = new GroupByIdentityQueryOperatorEnumerator<TSource, TGroupKey, TKey>(hashStream[i], this._keyComparer, cancellationToken);
					partitionedStream[i] = (QueryOperatorEnumerator<IGrouping<TGroupKey, TElement>, TKey>)groupByIdentityQueryOperatorEnumerator;
				}
				else
				{
					partitionedStream[i] = new GroupByElementSelectorQueryOperatorEnumerator<TSource, TGroupKey, TElement, TKey>(hashStream[i], this._keyComparer, this._elementSelector, cancellationToken);
				}
			}
			recipient.Receive<TKey>(partitionedStream);
		}

		// Token: 0x06000A4B RID: 2635 RVA: 0x00020A74 File Offset: 0x0001EC74
		private void WrapPartitionedStreamHelperOrdered<TKey>(PartitionedStream<Pair<TSource, TGroupKey>, TKey> hashStream, IPartitionedStreamRecipient<IGrouping<TGroupKey, TElement>> recipient, CancellationToken cancellationToken)
		{
			int partitionCount = hashStream.PartitionCount;
			PartitionedStream<IGrouping<TGroupKey, TElement>, TKey> partitionedStream = new PartitionedStream<IGrouping<TGroupKey, TElement>, TKey>(partitionCount, hashStream.KeyComparer, OrdinalIndexState.Shuffled);
			IComparer<TKey> keyComparer = hashStream.KeyComparer;
			for (int i = 0; i < partitionCount; i++)
			{
				if (this._elementSelector == null)
				{
					OrderedGroupByIdentityQueryOperatorEnumerator<TSource, TGroupKey, TKey> orderedGroupByIdentityQueryOperatorEnumerator = new OrderedGroupByIdentityQueryOperatorEnumerator<TSource, TGroupKey, TKey>(hashStream[i], this._keySelector, this._keyComparer, keyComparer, cancellationToken);
					partitionedStream[i] = (QueryOperatorEnumerator<IGrouping<TGroupKey, TElement>, TKey>)orderedGroupByIdentityQueryOperatorEnumerator;
				}
				else
				{
					partitionedStream[i] = new OrderedGroupByElementSelectorQueryOperatorEnumerator<TSource, TGroupKey, TElement, TKey>(hashStream[i], this._keySelector, this._elementSelector, this._keyComparer, keyComparer, cancellationToken);
				}
			}
			recipient.Receive<TKey>(partitionedStream);
		}

		// Token: 0x06000A4C RID: 2636 RVA: 0x00020B0B File Offset: 0x0001ED0B
		internal override QueryResults<IGrouping<TGroupKey, TElement>> Open(QuerySettings settings, bool preferStriping)
		{
			return new UnaryQueryOperator<TSource, IGrouping<TGroupKey, TElement>>.UnaryQueryOperatorResults(base.Child.Open(settings, false), this, settings, false);
		}

		// Token: 0x06000A4D RID: 2637 RVA: 0x00020B24 File Offset: 0x0001ED24
		internal override IEnumerable<IGrouping<TGroupKey, TElement>> AsSequentialQuery(CancellationToken token)
		{
			IEnumerable<TSource> source = CancellableEnumerable.Wrap<TSource>(base.Child.AsSequentialQuery(token), token);
			if (this._elementSelector == null)
			{
				return (IEnumerable<IGrouping<TGroupKey, TElement>>)source.GroupBy(this._keySelector, this._keyComparer);
			}
			return source.GroupBy(this._keySelector, this._elementSelector, this._keyComparer);
		}

		// Token: 0x17000169 RID: 361
		// (get) Token: 0x06000A4E RID: 2638 RVA: 0x00002285 File Offset: 0x00000485
		internal override bool LimitsParallelism
		{
			get
			{
				return false;
			}
		}

		// Token: 0x040006DD RID: 1757
		private readonly Func<TSource, TGroupKey> _keySelector;

		// Token: 0x040006DE RID: 1758
		private readonly Func<TSource, TElement> _elementSelector;

		// Token: 0x040006DF RID: 1759
		private readonly IEqualityComparer<TGroupKey> _keyComparer;
	}
}
