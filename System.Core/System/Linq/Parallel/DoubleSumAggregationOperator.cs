﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x0200012E RID: 302
	internal sealed class DoubleSumAggregationOperator : InlinedAggregationOperator<double, double, double>
	{
		// Token: 0x060008DD RID: 2269 RVA: 0x0001C285 File Offset: 0x0001A485
		internal DoubleSumAggregationOperator(IEnumerable<double> child) : base(child)
		{
		}

		// Token: 0x060008DE RID: 2270 RVA: 0x0001C290 File Offset: 0x0001A490
		protected override double InternalAggregate(ref Exception singularExceptionToThrow)
		{
			double result;
			using (IEnumerator<double> enumerator = this.GetEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered), true))
			{
				double num = 0.0;
				while (enumerator.MoveNext())
				{
					double num2 = enumerator.Current;
					num += num2;
				}
				result = num;
			}
			return result;
		}

		// Token: 0x060008DF RID: 2271 RVA: 0x0001C2E8 File Offset: 0x0001A4E8
		protected override QueryOperatorEnumerator<double, int> CreateEnumerator<TKey>(int index, int count, QueryOperatorEnumerator<double, TKey> source, object sharedData, CancellationToken cancellationToken)
		{
			return new DoubleSumAggregationOperator.DoubleSumAggregationOperatorEnumerator<TKey>(source, index, cancellationToken);
		}

		// Token: 0x0200012F RID: 303
		private class DoubleSumAggregationOperatorEnumerator<TKey> : InlinedAggregationOperatorEnumerator<double>
		{
			// Token: 0x060008E0 RID: 2272 RVA: 0x0001C2F3 File Offset: 0x0001A4F3
			internal DoubleSumAggregationOperatorEnumerator(QueryOperatorEnumerator<double, TKey> source, int partitionIndex, CancellationToken cancellationToken) : base(partitionIndex, cancellationToken)
			{
				this._source = source;
			}

			// Token: 0x060008E1 RID: 2273 RVA: 0x0001C304 File Offset: 0x0001A504
			protected override bool MoveNextCore(ref double currentElement)
			{
				double num = 0.0;
				TKey tkey = default(TKey);
				QueryOperatorEnumerator<double, TKey> source = this._source;
				if (source.MoveNext(ref num, ref tkey))
				{
					double num2 = 0.0;
					int num3 = 0;
					do
					{
						if ((num3++ & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(this._cancellationToken);
						}
						num2 += num;
					}
					while (source.MoveNext(ref num, ref tkey));
					currentElement = num2;
					return true;
				}
				return false;
			}

			// Token: 0x060008E2 RID: 2274 RVA: 0x0001C370 File Offset: 0x0001A570
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x04000640 RID: 1600
			private readonly QueryOperatorEnumerator<double, TKey> _source;
		}
	}
}
