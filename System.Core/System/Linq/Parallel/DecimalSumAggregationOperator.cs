﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000128 RID: 296
	internal sealed class DecimalSumAggregationOperator : InlinedAggregationOperator<decimal, decimal, decimal>
	{
		// Token: 0x060008CB RID: 2251 RVA: 0x0001BE69 File Offset: 0x0001A069
		internal DecimalSumAggregationOperator(IEnumerable<decimal> child) : base(child)
		{
		}

		// Token: 0x060008CC RID: 2252 RVA: 0x0001BE74 File Offset: 0x0001A074
		protected override decimal InternalAggregate(ref Exception singularExceptionToThrow)
		{
			decimal result;
			using (IEnumerator<decimal> enumerator = this.GetEnumerator(new ParallelMergeOptions?(ParallelMergeOptions.FullyBuffered), true))
			{
				decimal num = 0.0m;
				while (enumerator.MoveNext())
				{
					decimal d = enumerator.Current;
					num += d;
				}
				result = num;
			}
			return result;
		}

		// Token: 0x060008CD RID: 2253 RVA: 0x0001BED4 File Offset: 0x0001A0D4
		protected override QueryOperatorEnumerator<decimal, int> CreateEnumerator<TKey>(int index, int count, QueryOperatorEnumerator<decimal, TKey> source, object sharedData, CancellationToken cancellationToken)
		{
			return new DecimalSumAggregationOperator.DecimalSumAggregationOperatorEnumerator<TKey>(source, index, cancellationToken);
		}

		// Token: 0x02000129 RID: 297
		private class DecimalSumAggregationOperatorEnumerator<TKey> : InlinedAggregationOperatorEnumerator<decimal>
		{
			// Token: 0x060008CE RID: 2254 RVA: 0x0001BEDF File Offset: 0x0001A0DF
			internal DecimalSumAggregationOperatorEnumerator(QueryOperatorEnumerator<decimal, TKey> source, int partitionIndex, CancellationToken cancellationToken) : base(partitionIndex, cancellationToken)
			{
				this._source = source;
			}

			// Token: 0x060008CF RID: 2255 RVA: 0x0001BEF0 File Offset: 0x0001A0F0
			protected override bool MoveNextCore(ref decimal currentElement)
			{
				decimal d = 0m;
				TKey tkey = default(TKey);
				QueryOperatorEnumerator<decimal, TKey> source = this._source;
				if (source.MoveNext(ref d, ref tkey))
				{
					decimal num = 0.0m;
					int num2 = 0;
					do
					{
						if ((num2++ & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(this._cancellationToken);
						}
						num += d;
					}
					while (source.MoveNext(ref d, ref tkey));
					currentElement = num;
					return true;
				}
				return false;
			}

			// Token: 0x060008D0 RID: 2256 RVA: 0x0001BF64 File Offset: 0x0001A164
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x0400063B RID: 1595
			private QueryOperatorEnumerator<decimal, TKey> _source;
		}
	}
}
