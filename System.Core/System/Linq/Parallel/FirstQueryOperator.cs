﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;

namespace System.Linq.Parallel
{
	// Token: 0x02000182 RID: 386
	internal sealed class FirstQueryOperator<TSource> : UnaryQueryOperator<TSource, TSource>
	{
		// Token: 0x06000A35 RID: 2613 RVA: 0x00020558 File Offset: 0x0001E758
		internal FirstQueryOperator(IEnumerable<TSource> child, Func<TSource, bool> predicate) : base(child)
		{
			this._predicate = predicate;
			this._prematureMergeNeeded = base.Child.OrdinalIndexState.IsWorseThan(OrdinalIndexState.Increasing);
		}

		// Token: 0x06000A36 RID: 2614 RVA: 0x00020300 File Offset: 0x0001E500
		internal override QueryResults<TSource> Open(QuerySettings settings, bool preferStriping)
		{
			return new UnaryQueryOperator<TSource, TSource>.UnaryQueryOperatorResults(base.Child.Open(settings, false), this, settings, preferStriping);
		}

		// Token: 0x06000A37 RID: 2615 RVA: 0x00020580 File Offset: 0x0001E780
		internal override void WrapPartitionedStream<TKey>(PartitionedStream<TSource, TKey> inputStream, IPartitionedStreamRecipient<TSource> recipient, bool preferStriping, QuerySettings settings)
		{
			if (this._prematureMergeNeeded)
			{
				ListQueryResults<TSource> listQueryResults = QueryOperator<TSource>.ExecuteAndCollectResults<TKey>(inputStream, inputStream.PartitionCount, base.Child.OutputOrdered, preferStriping, settings);
				this.WrapHelper<int>(listQueryResults.GetPartitionedStream(), recipient, settings);
				return;
			}
			this.WrapHelper<TKey>(inputStream, recipient, settings);
		}

		// Token: 0x06000A38 RID: 2616 RVA: 0x000205CC File Offset: 0x0001E7CC
		private void WrapHelper<TKey>(PartitionedStream<TSource, TKey> inputStream, IPartitionedStreamRecipient<TSource> recipient, QuerySettings settings)
		{
			int partitionCount = inputStream.PartitionCount;
			FirstQueryOperator<TSource>.FirstQueryOperatorState<TKey> operatorState = new FirstQueryOperator<TSource>.FirstQueryOperatorState<TKey>();
			CountdownEvent sharedBarrier = new CountdownEvent(partitionCount);
			PartitionedStream<TSource, int> partitionedStream = new PartitionedStream<TSource, int>(partitionCount, Util.GetDefaultComparer<int>(), OrdinalIndexState.Shuffled);
			for (int i = 0; i < partitionCount; i++)
			{
				partitionedStream[i] = new FirstQueryOperator<TSource>.FirstQueryOperatorEnumerator<TKey>(inputStream[i], this._predicate, operatorState, sharedBarrier, settings.CancellationState.MergedCancellationToken, inputStream.KeyComparer, i);
			}
			recipient.Receive<int>(partitionedStream);
		}

		// Token: 0x06000A39 RID: 2617 RVA: 0x00003A6B File Offset: 0x00001C6B
		[ExcludeFromCodeCoverage]
		internal override IEnumerable<TSource> AsSequentialQuery(CancellationToken token)
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000167 RID: 359
		// (get) Token: 0x06000A3A RID: 2618 RVA: 0x00002285 File Offset: 0x00000485
		internal override bool LimitsParallelism
		{
			get
			{
				return false;
			}
		}

		// Token: 0x040006CD RID: 1741
		private readonly Func<TSource, bool> _predicate;

		// Token: 0x040006CE RID: 1742
		private readonly bool _prematureMergeNeeded;

		// Token: 0x02000183 RID: 387
		private class FirstQueryOperatorEnumerator<TKey> : QueryOperatorEnumerator<TSource, int>
		{
			// Token: 0x06000A3B RID: 2619 RVA: 0x00020642 File Offset: 0x0001E842
			internal FirstQueryOperatorEnumerator(QueryOperatorEnumerator<TSource, TKey> source, Func<TSource, bool> predicate, FirstQueryOperator<TSource>.FirstQueryOperatorState<TKey> operatorState, CountdownEvent sharedBarrier, CancellationToken cancellationToken, IComparer<TKey> keyComparer, int partitionId)
			{
				this._source = source;
				this._predicate = predicate;
				this._operatorState = operatorState;
				this._sharedBarrier = sharedBarrier;
				this._cancellationToken = cancellationToken;
				this._keyComparer = keyComparer;
				this._partitionId = partitionId;
			}

			// Token: 0x06000A3C RID: 2620 RVA: 0x00020680 File Offset: 0x0001E880
			internal override bool MoveNext(ref TSource currentElement, ref int currentKey)
			{
				if (this._alreadySearched)
				{
					return false;
				}
				TSource tsource = default(TSource);
				TKey tkey = default(TKey);
				try
				{
					TSource tsource2 = default(TSource);
					TKey tkey2 = default(TKey);
					int num = 0;
					while (this._source.MoveNext(ref tsource2, ref tkey2))
					{
						if ((num++ & 63) == 0)
						{
							CancellationState.ThrowIfCanceled(this._cancellationToken);
						}
						if (this._predicate == null || this._predicate(tsource2))
						{
							tsource = tsource2;
							tkey = tkey2;
							FirstQueryOperator<TSource>.FirstQueryOperatorState<TKey> operatorState = this._operatorState;
							lock (operatorState)
							{
								if (this._operatorState._partitionId == -1 || this._keyComparer.Compare(tkey, this._operatorState._key) < 0)
								{
									this._operatorState._key = tkey;
									this._operatorState._partitionId = this._partitionId;
								}
								break;
							}
						}
					}
				}
				finally
				{
					this._sharedBarrier.Signal();
				}
				this._alreadySearched = true;
				if (this._partitionId == this._operatorState._partitionId)
				{
					this._sharedBarrier.Wait(this._cancellationToken);
					if (this._partitionId == this._operatorState._partitionId)
					{
						currentElement = tsource;
						currentKey = 0;
						return true;
					}
				}
				return false;
			}

			// Token: 0x06000A3D RID: 2621 RVA: 0x000207E4 File Offset: 0x0001E9E4
			protected override void Dispose(bool disposing)
			{
				this._source.Dispose();
			}

			// Token: 0x040006CF RID: 1743
			private QueryOperatorEnumerator<TSource, TKey> _source;

			// Token: 0x040006D0 RID: 1744
			private Func<TSource, bool> _predicate;

			// Token: 0x040006D1 RID: 1745
			private bool _alreadySearched;

			// Token: 0x040006D2 RID: 1746
			private int _partitionId;

			// Token: 0x040006D3 RID: 1747
			private FirstQueryOperator<TSource>.FirstQueryOperatorState<TKey> _operatorState;

			// Token: 0x040006D4 RID: 1748
			private CountdownEvent _sharedBarrier;

			// Token: 0x040006D5 RID: 1749
			private CancellationToken _cancellationToken;

			// Token: 0x040006D6 RID: 1750
			private IComparer<TKey> _keyComparer;
		}

		// Token: 0x02000184 RID: 388
		private class FirstQueryOperatorState<TKey>
		{
			// Token: 0x06000A3E RID: 2622 RVA: 0x000207F1 File Offset: 0x0001E9F1
			public FirstQueryOperatorState()
			{
			}

			// Token: 0x040006D7 RID: 1751
			internal TKey _key;

			// Token: 0x040006D8 RID: 1752
			internal int _partitionId = -1;
		}
	}
}
