﻿using System;
using System.Runtime.CompilerServices;

namespace System.Linq
{
	// Token: 0x020000BE RID: 190
	internal class IdentityFunction<TElement>
	{
		// Token: 0x17000103 RID: 259
		// (get) Token: 0x06000733 RID: 1843 RVA: 0x00015B88 File Offset: 0x00013D88
		public static Func<TElement, TElement> Instance
		{
			get
			{
				return (TElement x) => x;
			}
		}

		// Token: 0x06000734 RID: 1844 RVA: 0x00002310 File Offset: 0x00000510
		public IdentityFunction()
		{
		}

		// Token: 0x020000BF RID: 191
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000735 RID: 1845 RVA: 0x00015BA9 File Offset: 0x00013DA9
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000736 RID: 1846 RVA: 0x00002310 File Offset: 0x00000510
			public <>c()
			{
			}

			// Token: 0x06000737 RID: 1847 RVA: 0x000021A3 File Offset: 0x000003A3
			internal TElement <get_Instance>b__1_0(TElement x)
			{
				return x;
			}

			// Token: 0x040004CB RID: 1227
			public static readonly IdentityFunction<TElement>.<>c <>9 = new IdentityFunction<TElement>.<>c();

			// Token: 0x040004CC RID: 1228
			public static Func<TElement, TElement> <>9__1_0;
		}
	}
}
