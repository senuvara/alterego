﻿using System;
using System.Linq.Expressions;

namespace System.Linq
{
	/// <summary>Represents an expression tree and provides functionality to execute the expression tree after rewriting it.</summary>
	/// <typeparam name="T">The data type of the value that results from executing the expression tree.</typeparam>
	// Token: 0x0200008A RID: 138
	public class EnumerableExecutor<T> : EnumerableExecutor
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Linq.EnumerableExecutor`1" /> class.</summary>
		/// <param name="expression">An expression tree to associate with the new instance.</param>
		// Token: 0x06000460 RID: 1120 RVA: 0x0000B703 File Offset: 0x00009903
		public EnumerableExecutor(Expression expression)
		{
			this._expression = expression;
		}

		// Token: 0x06000461 RID: 1121 RVA: 0x0000B712 File Offset: 0x00009912
		internal override object ExecuteBoxed()
		{
			return this.Execute();
		}

		// Token: 0x06000462 RID: 1122 RVA: 0x0000B71F File Offset: 0x0000991F
		internal T Execute()
		{
			return Expression.Lambda<Func<T>>(new EnumerableRewriter().Visit(this._expression), null).Compile()();
		}

		// Token: 0x04000389 RID: 905
		private readonly Expression _expression;
	}
}
