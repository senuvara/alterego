﻿using System;
using System.Collections.Generic;

namespace System.Linq
{
	// Token: 0x020000D0 RID: 208
	internal class EnumerableSorter<TElement, TKey> : EnumerableSorter<TElement>
	{
		// Token: 0x0600078F RID: 1935 RVA: 0x00016932 File Offset: 0x00014B32
		internal EnumerableSorter(Func<TElement, TKey> keySelector, IComparer<TKey> comparer, bool descending, EnumerableSorter<TElement> next)
		{
			this.keySelector = keySelector;
			this.comparer = comparer;
			this.descending = descending;
			this.next = next;
		}

		// Token: 0x06000790 RID: 1936 RVA: 0x00016958 File Offset: 0x00014B58
		internal override void ComputeKeys(TElement[] elements, int count)
		{
			this.keys = new TKey[count];
			for (int i = 0; i < count; i++)
			{
				this.keys[i] = this.keySelector(elements[i]);
			}
			if (this.next != null)
			{
				this.next.ComputeKeys(elements, count);
			}
		}

		// Token: 0x06000791 RID: 1937 RVA: 0x000169B0 File Offset: 0x00014BB0
		internal override int CompareKeys(int index1, int index2)
		{
			int num = this.comparer.Compare(this.keys[index1], this.keys[index2]);
			if (num == 0)
			{
				if (this.next == null)
				{
					return index1 - index2;
				}
				return this.next.CompareKeys(index1, index2);
			}
			else
			{
				if (!this.descending)
				{
					return num;
				}
				return -num;
			}
		}

		// Token: 0x04000502 RID: 1282
		internal Func<TElement, TKey> keySelector;

		// Token: 0x04000503 RID: 1283
		internal IComparer<TKey> comparer;

		// Token: 0x04000504 RID: 1284
		internal bool descending;

		// Token: 0x04000505 RID: 1285
		internal EnumerableSorter<TElement> next;

		// Token: 0x04000506 RID: 1286
		internal TKey[] keys;
	}
}
