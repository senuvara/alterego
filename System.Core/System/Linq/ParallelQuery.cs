﻿using System;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using System.Linq.Parallel;
using Unity;

namespace System.Linq
{
	/// <summary>Represents a parallel sequence.</summary>
	// Token: 0x0200007B RID: 123
	public class ParallelQuery : IEnumerable
	{
		// Token: 0x060002ED RID: 749 RVA: 0x000079A0 File Offset: 0x00005BA0
		internal ParallelQuery(QuerySettings specifiedSettings)
		{
			this._specifiedSettings = specifiedSettings;
		}

		// Token: 0x170000AD RID: 173
		// (get) Token: 0x060002EE RID: 750 RVA: 0x000079AF File Offset: 0x00005BAF
		internal QuerySettings SpecifiedQuerySettings
		{
			get
			{
				return this._specifiedSettings;
			}
		}

		// Token: 0x060002EF RID: 751 RVA: 0x00003A6B File Offset: 0x00001C6B
		[ExcludeFromCodeCoverage]
		internal virtual ParallelQuery<TCastTo> Cast<TCastTo>()
		{
			throw new NotSupportedException();
		}

		// Token: 0x060002F0 RID: 752 RVA: 0x00003A6B File Offset: 0x00001C6B
		[ExcludeFromCodeCoverage]
		internal virtual ParallelQuery<TCastTo> OfType<TCastTo>()
		{
			throw new NotSupportedException();
		}

		// Token: 0x060002F1 RID: 753 RVA: 0x00003A6B File Offset: 0x00001C6B
		[ExcludeFromCodeCoverage]
		internal virtual IEnumerator GetEnumeratorUntyped()
		{
			throw new NotSupportedException();
		}

		/// <summary>Returns an enumerator that iterates through the sequence.</summary>
		/// <returns>An enumerator that iterates through the sequence.</returns>
		// Token: 0x060002F2 RID: 754 RVA: 0x000079B7 File Offset: 0x00005BB7
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumeratorUntyped();
		}

		// Token: 0x060002F3 RID: 755 RVA: 0x0000220F File Offset: 0x0000040F
		internal ParallelQuery()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040002F5 RID: 757
		private QuerySettings _specifiedSettings;
	}
}
