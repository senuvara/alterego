﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace System.Linq
{
	// Token: 0x02000088 RID: 136
	internal static class CachedReflectionInfo
	{
		// Token: 0x060003E0 RID: 992 RVA: 0x00009CE2 File Offset: 0x00007EE2
		public static MethodInfo Aggregate_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Aggregate_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Aggregate_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, object, object>>, object>(Queryable.Aggregate<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x060003E1 RID: 993 RVA: 0x00009D18 File Offset: 0x00007F18
		public static MethodInfo Aggregate_TSource_TAccumulate_3(Type TSource, Type TAccumulate)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Aggregate_TSource_TAccumulate_3) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Aggregate_TSource_TAccumulate_3 = new Func<IQueryable<object>, object, Expression<Func<object, object, object>>, object>(Queryable.Aggregate<object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TAccumulate
			});
		}

		// Token: 0x060003E2 RID: 994 RVA: 0x00009D52 File Offset: 0x00007F52
		public static MethodInfo Aggregate_TSource_TAccumulate_TResult_4(Type TSource, Type TAccumulate, Type TResult)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Aggregate_TSource_TAccumulate_TResult_4) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Aggregate_TSource_TAccumulate_TResult_4 = new Func<IQueryable<object>, object, Expression<Func<object, object, object>>, Expression<Func<object, object>>, object>(Queryable.Aggregate<object, object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TAccumulate,
				TResult
			});
		}

		// Token: 0x060003E3 RID: 995 RVA: 0x00009D90 File Offset: 0x00007F90
		public static MethodInfo All_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_All_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_All_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, bool>>, bool>(Queryable.All<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x060003E4 RID: 996 RVA: 0x00009DC6 File Offset: 0x00007FC6
		public static MethodInfo Any_TSource_1(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Any_TSource_1) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Any_TSource_1 = new Func<IQueryable<object>, bool>(Queryable.Any<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x060003E5 RID: 997 RVA: 0x00009DFC File Offset: 0x00007FFC
		public static MethodInfo Any_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Any_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Any_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, bool>>, bool>(Queryable.Any<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x170000AE RID: 174
		// (get) Token: 0x060003E6 RID: 998 RVA: 0x00009E32 File Offset: 0x00008032
		public static MethodInfo Average_Int32_1
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Average_Int32_1) == null)
				{
					result = (CachedReflectionInfo.s_Average_Int32_1 = new Func<IQueryable<int>, double>(Queryable.Average).GetMethodInfo());
				}
				return result;
			}
		}

		// Token: 0x170000AF RID: 175
		// (get) Token: 0x060003E7 RID: 999 RVA: 0x00009E54 File Offset: 0x00008054
		public static MethodInfo Average_NullableInt32_1
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Average_NullableInt32_1) == null)
				{
					result = (CachedReflectionInfo.s_Average_NullableInt32_1 = new Func<IQueryable<int?>, double?>(Queryable.Average).GetMethodInfo());
				}
				return result;
			}
		}

		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x060003E8 RID: 1000 RVA: 0x00009E76 File Offset: 0x00008076
		public static MethodInfo Average_Int64_1
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Average_Int64_1) == null)
				{
					result = (CachedReflectionInfo.s_Average_Int64_1 = new Func<IQueryable<long>, double>(Queryable.Average).GetMethodInfo());
				}
				return result;
			}
		}

		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x060003E9 RID: 1001 RVA: 0x00009E98 File Offset: 0x00008098
		public static MethodInfo Average_NullableInt64_1
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Average_NullableInt64_1) == null)
				{
					result = (CachedReflectionInfo.s_Average_NullableInt64_1 = new Func<IQueryable<long?>, double?>(Queryable.Average).GetMethodInfo());
				}
				return result;
			}
		}

		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x060003EA RID: 1002 RVA: 0x00009EBA File Offset: 0x000080BA
		public static MethodInfo Average_Single_1
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Average_Single_1) == null)
				{
					result = (CachedReflectionInfo.s_Average_Single_1 = new Func<IQueryable<float>, float>(Queryable.Average).GetMethodInfo());
				}
				return result;
			}
		}

		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x060003EB RID: 1003 RVA: 0x00009EDC File Offset: 0x000080DC
		public static MethodInfo Average_NullableSingle_1
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Average_NullableSingle_1) == null)
				{
					result = (CachedReflectionInfo.s_Average_NullableSingle_1 = new Func<IQueryable<float?>, float?>(Queryable.Average).GetMethodInfo());
				}
				return result;
			}
		}

		// Token: 0x170000B4 RID: 180
		// (get) Token: 0x060003EC RID: 1004 RVA: 0x00009EFE File Offset: 0x000080FE
		public static MethodInfo Average_Double_1
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Average_Double_1) == null)
				{
					result = (CachedReflectionInfo.s_Average_Double_1 = new Func<IQueryable<double>, double>(Queryable.Average).GetMethodInfo());
				}
				return result;
			}
		}

		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x060003ED RID: 1005 RVA: 0x00009F20 File Offset: 0x00008120
		public static MethodInfo Average_NullableDouble_1
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Average_NullableDouble_1) == null)
				{
					result = (CachedReflectionInfo.s_Average_NullableDouble_1 = new Func<IQueryable<double?>, double?>(Queryable.Average).GetMethodInfo());
				}
				return result;
			}
		}

		// Token: 0x170000B6 RID: 182
		// (get) Token: 0x060003EE RID: 1006 RVA: 0x00009F42 File Offset: 0x00008142
		public static MethodInfo Average_Decimal_1
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Average_Decimal_1) == null)
				{
					result = (CachedReflectionInfo.s_Average_Decimal_1 = new Func<IQueryable<decimal>, decimal>(Queryable.Average).GetMethodInfo());
				}
				return result;
			}
		}

		// Token: 0x170000B7 RID: 183
		// (get) Token: 0x060003EF RID: 1007 RVA: 0x00009F64 File Offset: 0x00008164
		public static MethodInfo Average_NullableDecimal_1
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Average_NullableDecimal_1) == null)
				{
					result = (CachedReflectionInfo.s_Average_NullableDecimal_1 = new Func<IQueryable<decimal?>, decimal?>(Queryable.Average).GetMethodInfo());
				}
				return result;
			}
		}

		// Token: 0x060003F0 RID: 1008 RVA: 0x00009F86 File Offset: 0x00008186
		public static MethodInfo Average_Int32_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Average_Int32_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Average_Int32_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, int>>, double>(Queryable.Average<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x060003F1 RID: 1009 RVA: 0x00009FBC File Offset: 0x000081BC
		public static MethodInfo Average_NullableInt32_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Average_NullableInt32_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Average_NullableInt32_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, int?>>, double?>(Queryable.Average<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x060003F2 RID: 1010 RVA: 0x00009FF2 File Offset: 0x000081F2
		public static MethodInfo Average_Single_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Average_Single_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Average_Single_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, float>>, float>(Queryable.Average<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x060003F3 RID: 1011 RVA: 0x0000A028 File Offset: 0x00008228
		public static MethodInfo Average_NullableSingle_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Average_NullableSingle_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Average_NullableSingle_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, float?>>, float?>(Queryable.Average<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x060003F4 RID: 1012 RVA: 0x0000A05E File Offset: 0x0000825E
		public static MethodInfo Average_Int64_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Average_Int64_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Average_Int64_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, long>>, double>(Queryable.Average<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x060003F5 RID: 1013 RVA: 0x0000A094 File Offset: 0x00008294
		public static MethodInfo Average_NullableInt64_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Average_NullableInt64_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Average_NullableInt64_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, long?>>, double?>(Queryable.Average<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x060003F6 RID: 1014 RVA: 0x0000A0CA File Offset: 0x000082CA
		public static MethodInfo Average_Double_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Average_Double_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Average_Double_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, double>>, double>(Queryable.Average<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x060003F7 RID: 1015 RVA: 0x0000A100 File Offset: 0x00008300
		public static MethodInfo Average_NullableDouble_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Average_NullableDouble_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Average_NullableDouble_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, double?>>, double?>(Queryable.Average<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x060003F8 RID: 1016 RVA: 0x0000A136 File Offset: 0x00008336
		public static MethodInfo Average_Decimal_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Average_Decimal_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Average_Decimal_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, decimal>>, decimal>(Queryable.Average<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x060003F9 RID: 1017 RVA: 0x0000A16C File Offset: 0x0000836C
		public static MethodInfo Average_NullableDecimal_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Average_NullableDecimal_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Average_NullableDecimal_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, decimal?>>, decimal?>(Queryable.Average<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x060003FA RID: 1018 RVA: 0x0000A1A2 File Offset: 0x000083A2
		public static MethodInfo Cast_TResult_1(Type TResult)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Cast_TResult_1) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Cast_TResult_1 = new Func<IQueryable, IQueryable<object>>(Queryable.Cast<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TResult
			});
		}

		// Token: 0x060003FB RID: 1019 RVA: 0x0000A1D8 File Offset: 0x000083D8
		public static MethodInfo Concat_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Concat_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Concat_TSource_2 = new Func<IQueryable<object>, IEnumerable<object>, IQueryable<object>>(Queryable.Concat<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x060003FC RID: 1020 RVA: 0x0000A20E File Offset: 0x0000840E
		public static MethodInfo Contains_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Contains_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Contains_TSource_2 = new Func<IQueryable<object>, object, bool>(Queryable.Contains<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x060003FD RID: 1021 RVA: 0x0000A244 File Offset: 0x00008444
		public static MethodInfo Contains_TSource_3(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Contains_TSource_3) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Contains_TSource_3 = new Func<IQueryable<object>, object, IEqualityComparer<object>, bool>(Queryable.Contains<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x060003FE RID: 1022 RVA: 0x0000A27A File Offset: 0x0000847A
		public static MethodInfo Count_TSource_1(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Count_TSource_1) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Count_TSource_1 = new Func<IQueryable<object>, int>(Queryable.Count<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x060003FF RID: 1023 RVA: 0x0000A2B0 File Offset: 0x000084B0
		public static MethodInfo Count_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Count_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Count_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, bool>>, int>(Queryable.Count<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000400 RID: 1024 RVA: 0x0000A2E6 File Offset: 0x000084E6
		public static MethodInfo DefaultIfEmpty_TSource_1(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_DefaultIfEmpty_TSource_1) == null)
			{
				methodInfo = (CachedReflectionInfo.s_DefaultIfEmpty_TSource_1 = new Func<IQueryable<object>, IQueryable<object>>(Queryable.DefaultIfEmpty<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000401 RID: 1025 RVA: 0x0000A31C File Offset: 0x0000851C
		public static MethodInfo DefaultIfEmpty_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_DefaultIfEmpty_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_DefaultIfEmpty_TSource_2 = new Func<IQueryable<object>, object, IQueryable<object>>(Queryable.DefaultIfEmpty<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000402 RID: 1026 RVA: 0x0000A352 File Offset: 0x00008552
		public static MethodInfo Distinct_TSource_1(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Distinct_TSource_1) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Distinct_TSource_1 = new Func<IQueryable<object>, IQueryable<object>>(Queryable.Distinct<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000403 RID: 1027 RVA: 0x0000A388 File Offset: 0x00008588
		public static MethodInfo Distinct_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Distinct_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Distinct_TSource_2 = new Func<IQueryable<object>, IEqualityComparer<object>, IQueryable<object>>(Queryable.Distinct<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000404 RID: 1028 RVA: 0x0000A3BE File Offset: 0x000085BE
		public static MethodInfo ElementAt_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_ElementAt_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_ElementAt_TSource_2 = new Func<IQueryable<object>, int, object>(Queryable.ElementAt<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000405 RID: 1029 RVA: 0x0000A3F4 File Offset: 0x000085F4
		public static MethodInfo ElementAtOrDefault_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_ElementAtOrDefault_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_ElementAtOrDefault_TSource_2 = new Func<IQueryable<object>, int, object>(Queryable.ElementAtOrDefault<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000406 RID: 1030 RVA: 0x0000A42A File Offset: 0x0000862A
		public static MethodInfo Except_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Except_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Except_TSource_2 = new Func<IQueryable<object>, IEnumerable<object>, IQueryable<object>>(Queryable.Except<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000407 RID: 1031 RVA: 0x0000A460 File Offset: 0x00008660
		public static MethodInfo Except_TSource_3(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Except_TSource_3) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Except_TSource_3 = new Func<IQueryable<object>, IEnumerable<object>, IEqualityComparer<object>, IQueryable<object>>(Queryable.Except<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000408 RID: 1032 RVA: 0x0000A496 File Offset: 0x00008696
		public static MethodInfo First_TSource_1(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_First_TSource_1) == null)
			{
				methodInfo = (CachedReflectionInfo.s_First_TSource_1 = new Func<IQueryable<object>, object>(Queryable.First<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000409 RID: 1033 RVA: 0x0000A4CC File Offset: 0x000086CC
		public static MethodInfo First_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_First_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_First_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, bool>>, object>(Queryable.First<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x0600040A RID: 1034 RVA: 0x0000A502 File Offset: 0x00008702
		public static MethodInfo FirstOrDefault_TSource_1(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_FirstOrDefault_TSource_1) == null)
			{
				methodInfo = (CachedReflectionInfo.s_FirstOrDefault_TSource_1 = new Func<IQueryable<object>, object>(Queryable.FirstOrDefault<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x0600040B RID: 1035 RVA: 0x0000A538 File Offset: 0x00008738
		public static MethodInfo FirstOrDefault_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_FirstOrDefault_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_FirstOrDefault_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, bool>>, object>(Queryable.FirstOrDefault<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x0600040C RID: 1036 RVA: 0x0000A56E File Offset: 0x0000876E
		public static MethodInfo GroupBy_TSource_TKey_2(Type TSource, Type TKey)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_GroupBy_TSource_TKey_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_GroupBy_TSource_TKey_2 = new Func<IQueryable<object>, Expression<Func<object, object>>, IQueryable<IGrouping<object, object>>>(Queryable.GroupBy<object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TKey
			});
		}

		// Token: 0x0600040D RID: 1037 RVA: 0x0000A5A8 File Offset: 0x000087A8
		public static MethodInfo GroupBy_TSource_TKey_3(Type TSource, Type TKey)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_GroupBy_TSource_TKey_3) == null)
			{
				methodInfo = (CachedReflectionInfo.s_GroupBy_TSource_TKey_3 = new Func<IQueryable<object>, Expression<Func<object, object>>, IEqualityComparer<object>, IQueryable<IGrouping<object, object>>>(Queryable.GroupBy<object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TKey
			});
		}

		// Token: 0x0600040E RID: 1038 RVA: 0x0000A5E2 File Offset: 0x000087E2
		public static MethodInfo GroupBy_TSource_TKey_TElement_3(Type TSource, Type TKey, Type TElement)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_GroupBy_TSource_TKey_TElement_3) == null)
			{
				methodInfo = (CachedReflectionInfo.s_GroupBy_TSource_TKey_TElement_3 = new Func<IQueryable<object>, Expression<Func<object, object>>, Expression<Func<object, object>>, IQueryable<IGrouping<object, object>>>(Queryable.GroupBy<object, object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TKey,
				TElement
			});
		}

		// Token: 0x0600040F RID: 1039 RVA: 0x0000A620 File Offset: 0x00008820
		public static MethodInfo GroupBy_TSource_TKey_TElement_4(Type TSource, Type TKey, Type TElement)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_GroupBy_TSource_TKey_TElement_4) == null)
			{
				methodInfo = (CachedReflectionInfo.s_GroupBy_TSource_TKey_TElement_4 = new Func<IQueryable<object>, Expression<Func<object, object>>, Expression<Func<object, object>>, IEqualityComparer<object>, IQueryable<IGrouping<object, object>>>(Queryable.GroupBy<object, object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TKey,
				TElement
			});
		}

		// Token: 0x06000410 RID: 1040 RVA: 0x0000A65E File Offset: 0x0000885E
		public static MethodInfo GroupBy_TSource_TKey_TResult_3(Type TSource, Type TKey, Type TResult)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_GroupBy_TSource_TKey_TResult_3) == null)
			{
				methodInfo = (CachedReflectionInfo.s_GroupBy_TSource_TKey_TResult_3 = new Func<IQueryable<object>, Expression<Func<object, object>>, Expression<Func<object, IEnumerable<object>, object>>, IQueryable<object>>(Queryable.GroupBy<object, object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TKey,
				TResult
			});
		}

		// Token: 0x06000411 RID: 1041 RVA: 0x0000A69C File Offset: 0x0000889C
		public static MethodInfo GroupBy_TSource_TKey_TResult_4(Type TSource, Type TKey, Type TResult)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_GroupBy_TSource_TKey_TResult_4) == null)
			{
				methodInfo = (CachedReflectionInfo.s_GroupBy_TSource_TKey_TResult_4 = new Func<IQueryable<object>, Expression<Func<object, object>>, Expression<Func<object, IEnumerable<object>, object>>, IEqualityComparer<object>, IQueryable<object>>(Queryable.GroupBy<object, object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TKey,
				TResult
			});
		}

		// Token: 0x06000412 RID: 1042 RVA: 0x0000A6DC File Offset: 0x000088DC
		public static MethodInfo GroupBy_TSource_TKey_TElement_TResult_4(Type TSource, Type TKey, Type TElement, Type TResult)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_GroupBy_TSource_TKey_TElement_TResult_4) == null)
			{
				methodInfo = (CachedReflectionInfo.s_GroupBy_TSource_TKey_TElement_TResult_4 = new Func<IQueryable<object>, Expression<Func<object, object>>, Expression<Func<object, object>>, Expression<Func<object, IEnumerable<object>, object>>, IQueryable<object>>(Queryable.GroupBy<object, object, object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TKey,
				TElement,
				TResult
			});
		}

		// Token: 0x06000413 RID: 1043 RVA: 0x0000A72C File Offset: 0x0000892C
		public static MethodInfo GroupBy_TSource_TKey_TElement_TResult_5(Type TSource, Type TKey, Type TElement, Type TResult)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_GroupBy_TSource_TKey_TElement_TResult_5) == null)
			{
				methodInfo = (CachedReflectionInfo.s_GroupBy_TSource_TKey_TElement_TResult_5 = new Func<IQueryable<object>, Expression<Func<object, object>>, Expression<Func<object, object>>, Expression<Func<object, IEnumerable<object>, object>>, IEqualityComparer<object>, IQueryable<object>>(Queryable.GroupBy<object, object, object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TKey,
				TElement,
				TResult
			});
		}

		// Token: 0x06000414 RID: 1044 RVA: 0x0000A77C File Offset: 0x0000897C
		public static MethodInfo GroupJoin_TOuter_TInner_TKey_TResult_5(Type TOuter, Type TInner, Type TKey, Type TResult)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_GroupJoin_TOuter_TInner_TKey_TResult_5) == null)
			{
				methodInfo = (CachedReflectionInfo.s_GroupJoin_TOuter_TInner_TKey_TResult_5 = new Func<IQueryable<object>, IEnumerable<object>, Expression<Func<object, object>>, Expression<Func<object, object>>, Expression<Func<object, IEnumerable<object>, object>>, IQueryable<object>>(Queryable.GroupJoin<object, object, object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TOuter,
				TInner,
				TKey,
				TResult
			});
		}

		// Token: 0x06000415 RID: 1045 RVA: 0x0000A7CC File Offset: 0x000089CC
		public static MethodInfo GroupJoin_TOuter_TInner_TKey_TResult_6(Type TOuter, Type TInner, Type TKey, Type TResult)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_GroupJoin_TOuter_TInner_TKey_TResult_6) == null)
			{
				methodInfo = (CachedReflectionInfo.s_GroupJoin_TOuter_TInner_TKey_TResult_6 = new Func<IQueryable<object>, IEnumerable<object>, Expression<Func<object, object>>, Expression<Func<object, object>>, Expression<Func<object, IEnumerable<object>, object>>, IEqualityComparer<object>, IQueryable<object>>(Queryable.GroupJoin<object, object, object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TOuter,
				TInner,
				TKey,
				TResult
			});
		}

		// Token: 0x06000416 RID: 1046 RVA: 0x0000A819 File Offset: 0x00008A19
		public static MethodInfo Intersect_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Intersect_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Intersect_TSource_2 = new Func<IQueryable<object>, IEnumerable<object>, IQueryable<object>>(Queryable.Intersect<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000417 RID: 1047 RVA: 0x0000A84F File Offset: 0x00008A4F
		public static MethodInfo Intersect_TSource_3(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Intersect_TSource_3) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Intersect_TSource_3 = new Func<IQueryable<object>, IEnumerable<object>, IEqualityComparer<object>, IQueryable<object>>(Queryable.Intersect<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000418 RID: 1048 RVA: 0x0000A888 File Offset: 0x00008A88
		public static MethodInfo Join_TOuter_TInner_TKey_TResult_5(Type TOuter, Type TInner, Type TKey, Type TResult)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Join_TOuter_TInner_TKey_TResult_5) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Join_TOuter_TInner_TKey_TResult_5 = new Func<IQueryable<object>, IEnumerable<object>, Expression<Func<object, object>>, Expression<Func<object, object>>, Expression<Func<object, object, object>>, IQueryable<object>>(Queryable.Join<object, object, object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TOuter,
				TInner,
				TKey,
				TResult
			});
		}

		// Token: 0x06000419 RID: 1049 RVA: 0x0000A8D8 File Offset: 0x00008AD8
		public static MethodInfo Join_TOuter_TInner_TKey_TResult_6(Type TOuter, Type TInner, Type TKey, Type TResult)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Join_TOuter_TInner_TKey_TResult_6) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Join_TOuter_TInner_TKey_TResult_6 = new Func<IQueryable<object>, IEnumerable<object>, Expression<Func<object, object>>, Expression<Func<object, object>>, Expression<Func<object, object, object>>, IEqualityComparer<object>, IQueryable<object>>(Queryable.Join<object, object, object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TOuter,
				TInner,
				TKey,
				TResult
			});
		}

		// Token: 0x0600041A RID: 1050 RVA: 0x0000A925 File Offset: 0x00008B25
		public static MethodInfo Last_TSource_1(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Last_TSource_1) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Last_TSource_1 = new Func<IQueryable<object>, object>(Queryable.Last<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x0600041B RID: 1051 RVA: 0x0000A95B File Offset: 0x00008B5B
		public static MethodInfo Last_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Last_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Last_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, bool>>, object>(Queryable.Last<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x0600041C RID: 1052 RVA: 0x0000A991 File Offset: 0x00008B91
		public static MethodInfo LastOrDefault_TSource_1(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_LastOrDefault_TSource_1) == null)
			{
				methodInfo = (CachedReflectionInfo.s_LastOrDefault_TSource_1 = new Func<IQueryable<object>, object>(Queryable.LastOrDefault<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x0600041D RID: 1053 RVA: 0x0000A9C7 File Offset: 0x00008BC7
		public static MethodInfo LastOrDefault_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_LastOrDefault_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_LastOrDefault_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, bool>>, object>(Queryable.LastOrDefault<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x0600041E RID: 1054 RVA: 0x0000A9FD File Offset: 0x00008BFD
		public static MethodInfo LongCount_TSource_1(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_LongCount_TSource_1) == null)
			{
				methodInfo = (CachedReflectionInfo.s_LongCount_TSource_1 = new Func<IQueryable<object>, long>(Queryable.LongCount<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x0600041F RID: 1055 RVA: 0x0000AA33 File Offset: 0x00008C33
		public static MethodInfo LongCount_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_LongCount_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_LongCount_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, bool>>, long>(Queryable.LongCount<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000420 RID: 1056 RVA: 0x0000AA69 File Offset: 0x00008C69
		public static MethodInfo Max_TSource_1(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Max_TSource_1) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Max_TSource_1 = new Func<IQueryable<object>, object>(Queryable.Max<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000421 RID: 1057 RVA: 0x0000AA9F File Offset: 0x00008C9F
		public static MethodInfo Max_TSource_TResult_2(Type TSource, Type TResult)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Max_TSource_TResult_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Max_TSource_TResult_2 = new Func<IQueryable<object>, Expression<Func<object, object>>, object>(Queryable.Max<object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TResult
			});
		}

		// Token: 0x06000422 RID: 1058 RVA: 0x0000AAD9 File Offset: 0x00008CD9
		public static MethodInfo Min_TSource_1(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Min_TSource_1) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Min_TSource_1 = new Func<IQueryable<object>, object>(Queryable.Min<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000423 RID: 1059 RVA: 0x0000AB0F File Offset: 0x00008D0F
		public static MethodInfo Min_TSource_TResult_2(Type TSource, Type TResult)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Min_TSource_TResult_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Min_TSource_TResult_2 = new Func<IQueryable<object>, Expression<Func<object, object>>, object>(Queryable.Min<object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TResult
			});
		}

		// Token: 0x06000424 RID: 1060 RVA: 0x0000AB49 File Offset: 0x00008D49
		public static MethodInfo OfType_TResult_1(Type TResult)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_OfType_TResult_1) == null)
			{
				methodInfo = (CachedReflectionInfo.s_OfType_TResult_1 = new Func<IQueryable, IQueryable<object>>(Queryable.OfType<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TResult
			});
		}

		// Token: 0x06000425 RID: 1061 RVA: 0x0000AB7F File Offset: 0x00008D7F
		public static MethodInfo OrderBy_TSource_TKey_2(Type TSource, Type TKey)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_OrderBy_TSource_TKey_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_OrderBy_TSource_TKey_2 = new Func<IQueryable<object>, Expression<Func<object, object>>, IOrderedQueryable<object>>(Queryable.OrderBy<object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TKey
			});
		}

		// Token: 0x06000426 RID: 1062 RVA: 0x0000ABB9 File Offset: 0x00008DB9
		public static MethodInfo OrderBy_TSource_TKey_3(Type TSource, Type TKey)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_OrderBy_TSource_TKey_3) == null)
			{
				methodInfo = (CachedReflectionInfo.s_OrderBy_TSource_TKey_3 = new Func<IQueryable<object>, Expression<Func<object, object>>, IComparer<object>, IOrderedQueryable<object>>(Queryable.OrderBy<object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TKey
			});
		}

		// Token: 0x06000427 RID: 1063 RVA: 0x0000ABF3 File Offset: 0x00008DF3
		public static MethodInfo OrderByDescending_TSource_TKey_2(Type TSource, Type TKey)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_OrderByDescending_TSource_TKey_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_OrderByDescending_TSource_TKey_2 = new Func<IQueryable<object>, Expression<Func<object, object>>, IOrderedQueryable<object>>(Queryable.OrderByDescending<object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TKey
			});
		}

		// Token: 0x06000428 RID: 1064 RVA: 0x0000AC2D File Offset: 0x00008E2D
		public static MethodInfo OrderByDescending_TSource_TKey_3(Type TSource, Type TKey)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_OrderByDescending_TSource_TKey_3) == null)
			{
				methodInfo = (CachedReflectionInfo.s_OrderByDescending_TSource_TKey_3 = new Func<IQueryable<object>, Expression<Func<object, object>>, IComparer<object>, IOrderedQueryable<object>>(Queryable.OrderByDescending<object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TKey
			});
		}

		// Token: 0x06000429 RID: 1065 RVA: 0x0000AC67 File Offset: 0x00008E67
		public static MethodInfo Reverse_TSource_1(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Reverse_TSource_1) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Reverse_TSource_1 = new Func<IQueryable<object>, IQueryable<object>>(Queryable.Reverse<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x0600042A RID: 1066 RVA: 0x0000AC9D File Offset: 0x00008E9D
		public static MethodInfo Select_TSource_TResult_2(Type TSource, Type TResult)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Select_TSource_TResult_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Select_TSource_TResult_2 = new Func<IQueryable<object>, Expression<Func<object, object>>, IQueryable<object>>(Queryable.Select<object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TResult
			});
		}

		// Token: 0x0600042B RID: 1067 RVA: 0x0000ACD7 File Offset: 0x00008ED7
		public static MethodInfo Select_Index_TSource_TResult_2(Type TSource, Type TResult)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Select_Index_TSource_TResult_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Select_Index_TSource_TResult_2 = new Func<IQueryable<object>, Expression<Func<object, int, object>>, IQueryable<object>>(Queryable.Select<object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TResult
			});
		}

		// Token: 0x0600042C RID: 1068 RVA: 0x0000AD11 File Offset: 0x00008F11
		public static MethodInfo SelectMany_TSource_TResult_2(Type TSource, Type TResult)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_SelectMany_TSource_TResult_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_SelectMany_TSource_TResult_2 = new Func<IQueryable<object>, Expression<Func<object, IEnumerable<object>>>, IQueryable<object>>(Queryable.SelectMany<object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TResult
			});
		}

		// Token: 0x0600042D RID: 1069 RVA: 0x0000AD4B File Offset: 0x00008F4B
		public static MethodInfo SelectMany_Index_TSource_TResult_2(Type TSource, Type TResult)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_SelectMany_Index_TSource_TResult_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_SelectMany_Index_TSource_TResult_2 = new Func<IQueryable<object>, Expression<Func<object, int, IEnumerable<object>>>, IQueryable<object>>(Queryable.SelectMany<object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TResult
			});
		}

		// Token: 0x0600042E RID: 1070 RVA: 0x0000AD85 File Offset: 0x00008F85
		public static MethodInfo SelectMany_Index_TSource_TCollection_TResult_3(Type TSource, Type TCollection, Type TResult)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_SelectMany_Index_TSource_TCollection_TResult_3) == null)
			{
				methodInfo = (CachedReflectionInfo.s_SelectMany_Index_TSource_TCollection_TResult_3 = new Func<IQueryable<object>, Expression<Func<object, int, IEnumerable<object>>>, Expression<Func<object, object, object>>, IQueryable<object>>(Queryable.SelectMany<object, object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TCollection,
				TResult
			});
		}

		// Token: 0x0600042F RID: 1071 RVA: 0x0000ADC3 File Offset: 0x00008FC3
		public static MethodInfo SelectMany_TSource_TCollection_TResult_3(Type TSource, Type TCollection, Type TResult)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_SelectMany_TSource_TCollection_TResult_3) == null)
			{
				methodInfo = (CachedReflectionInfo.s_SelectMany_TSource_TCollection_TResult_3 = new Func<IQueryable<object>, Expression<Func<object, IEnumerable<object>>>, Expression<Func<object, object, object>>, IQueryable<object>>(Queryable.SelectMany<object, object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TCollection,
				TResult
			});
		}

		// Token: 0x06000430 RID: 1072 RVA: 0x0000AE01 File Offset: 0x00009001
		public static MethodInfo SequenceEqual_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_SequenceEqual_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_SequenceEqual_TSource_2 = new Func<IQueryable<object>, IEnumerable<object>, bool>(Queryable.SequenceEqual<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000431 RID: 1073 RVA: 0x0000AE37 File Offset: 0x00009037
		public static MethodInfo SequenceEqual_TSource_3(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_SequenceEqual_TSource_3) == null)
			{
				methodInfo = (CachedReflectionInfo.s_SequenceEqual_TSource_3 = new Func<IQueryable<object>, IEnumerable<object>, IEqualityComparer<object>, bool>(Queryable.SequenceEqual<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000432 RID: 1074 RVA: 0x0000AE6D File Offset: 0x0000906D
		public static MethodInfo Single_TSource_1(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Single_TSource_1) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Single_TSource_1 = new Func<IQueryable<object>, object>(Queryable.Single<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000433 RID: 1075 RVA: 0x0000AEA3 File Offset: 0x000090A3
		public static MethodInfo Single_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Single_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Single_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, bool>>, object>(Queryable.Single<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000434 RID: 1076 RVA: 0x0000AED9 File Offset: 0x000090D9
		public static MethodInfo SingleOrDefault_TSource_1(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_SingleOrDefault_TSource_1) == null)
			{
				methodInfo = (CachedReflectionInfo.s_SingleOrDefault_TSource_1 = new Func<IQueryable<object>, object>(Queryable.SingleOrDefault<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000435 RID: 1077 RVA: 0x0000AF0F File Offset: 0x0000910F
		public static MethodInfo SingleOrDefault_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_SingleOrDefault_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_SingleOrDefault_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, bool>>, object>(Queryable.SingleOrDefault<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000436 RID: 1078 RVA: 0x0000AF45 File Offset: 0x00009145
		public static MethodInfo Skip_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Skip_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Skip_TSource_2 = new Func<IQueryable<object>, int, IQueryable<object>>(Queryable.Skip<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000437 RID: 1079 RVA: 0x0000AF7B File Offset: 0x0000917B
		public static MethodInfo SkipWhile_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_SkipWhile_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_SkipWhile_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, bool>>, IQueryable<object>>(Queryable.SkipWhile<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000438 RID: 1080 RVA: 0x0000AFB1 File Offset: 0x000091B1
		public static MethodInfo SkipWhile_Index_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_SkipWhile_Index_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_SkipWhile_Index_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, int, bool>>, IQueryable<object>>(Queryable.SkipWhile<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x170000B8 RID: 184
		// (get) Token: 0x06000439 RID: 1081 RVA: 0x0000AFE7 File Offset: 0x000091E7
		public static MethodInfo Sum_Int32_1
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Sum_Int32_1) == null)
				{
					result = (CachedReflectionInfo.s_Sum_Int32_1 = new Func<IQueryable<int>, int>(Queryable.Sum).GetMethodInfo());
				}
				return result;
			}
		}

		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x0600043A RID: 1082 RVA: 0x0000B009 File Offset: 0x00009209
		public static MethodInfo Sum_NullableInt32_1
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Sum_NullableInt32_1) == null)
				{
					result = (CachedReflectionInfo.s_Sum_NullableInt32_1 = new Func<IQueryable<int?>, int?>(Queryable.Sum).GetMethodInfo());
				}
				return result;
			}
		}

		// Token: 0x170000BA RID: 186
		// (get) Token: 0x0600043B RID: 1083 RVA: 0x0000B02B File Offset: 0x0000922B
		public static MethodInfo Sum_Int64_1
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Sum_Int64_1) == null)
				{
					result = (CachedReflectionInfo.s_Sum_Int64_1 = new Func<IQueryable<long>, long>(Queryable.Sum).GetMethodInfo());
				}
				return result;
			}
		}

		// Token: 0x170000BB RID: 187
		// (get) Token: 0x0600043C RID: 1084 RVA: 0x0000B04D File Offset: 0x0000924D
		public static MethodInfo Sum_NullableInt64_1
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Sum_NullableInt64_1) == null)
				{
					result = (CachedReflectionInfo.s_Sum_NullableInt64_1 = new Func<IQueryable<long?>, long?>(Queryable.Sum).GetMethodInfo());
				}
				return result;
			}
		}

		// Token: 0x170000BC RID: 188
		// (get) Token: 0x0600043D RID: 1085 RVA: 0x0000B06F File Offset: 0x0000926F
		public static MethodInfo Sum_Single_1
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Sum_Single_1) == null)
				{
					result = (CachedReflectionInfo.s_Sum_Single_1 = new Func<IQueryable<float>, float>(Queryable.Sum).GetMethodInfo());
				}
				return result;
			}
		}

		// Token: 0x170000BD RID: 189
		// (get) Token: 0x0600043E RID: 1086 RVA: 0x0000B091 File Offset: 0x00009291
		public static MethodInfo Sum_NullableSingle_1
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Sum_NullableSingle_1) == null)
				{
					result = (CachedReflectionInfo.s_Sum_NullableSingle_1 = new Func<IQueryable<float?>, float?>(Queryable.Sum).GetMethodInfo());
				}
				return result;
			}
		}

		// Token: 0x170000BE RID: 190
		// (get) Token: 0x0600043F RID: 1087 RVA: 0x0000B0B3 File Offset: 0x000092B3
		public static MethodInfo Sum_Double_1
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Sum_Double_1) == null)
				{
					result = (CachedReflectionInfo.s_Sum_Double_1 = new Func<IQueryable<double>, double>(Queryable.Sum).GetMethodInfo());
				}
				return result;
			}
		}

		// Token: 0x170000BF RID: 191
		// (get) Token: 0x06000440 RID: 1088 RVA: 0x0000B0D5 File Offset: 0x000092D5
		public static MethodInfo Sum_NullableDouble_1
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Sum_NullableDouble_1) == null)
				{
					result = (CachedReflectionInfo.s_Sum_NullableDouble_1 = new Func<IQueryable<double?>, double?>(Queryable.Sum).GetMethodInfo());
				}
				return result;
			}
		}

		// Token: 0x170000C0 RID: 192
		// (get) Token: 0x06000441 RID: 1089 RVA: 0x0000B0F7 File Offset: 0x000092F7
		public static MethodInfo Sum_Decimal_1
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Sum_Decimal_1) == null)
				{
					result = (CachedReflectionInfo.s_Sum_Decimal_1 = new Func<IQueryable<decimal>, decimal>(Queryable.Sum).GetMethodInfo());
				}
				return result;
			}
		}

		// Token: 0x170000C1 RID: 193
		// (get) Token: 0x06000442 RID: 1090 RVA: 0x0000B119 File Offset: 0x00009319
		public static MethodInfo Sum_NullableDecimal_1
		{
			get
			{
				MethodInfo result;
				if ((result = CachedReflectionInfo.s_Sum_NullableDecimal_1) == null)
				{
					result = (CachedReflectionInfo.s_Sum_NullableDecimal_1 = new Func<IQueryable<decimal?>, decimal?>(Queryable.Sum).GetMethodInfo());
				}
				return result;
			}
		}

		// Token: 0x06000443 RID: 1091 RVA: 0x0000B13B File Offset: 0x0000933B
		public static MethodInfo Sum_NullableDecimal_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Sum_NullableDecimal_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Sum_NullableDecimal_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, decimal?>>, decimal?>(Queryable.Sum<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000444 RID: 1092 RVA: 0x0000B171 File Offset: 0x00009371
		public static MethodInfo Sum_Int32_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Sum_Int32_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Sum_Int32_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, int>>, int>(Queryable.Sum<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000445 RID: 1093 RVA: 0x0000B1A7 File Offset: 0x000093A7
		public static MethodInfo Sum_NullableInt32_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Sum_NullableInt32_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Sum_NullableInt32_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, int?>>, int?>(Queryable.Sum<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000446 RID: 1094 RVA: 0x0000B1DD File Offset: 0x000093DD
		public static MethodInfo Sum_Int64_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Sum_Int64_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Sum_Int64_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, long>>, long>(Queryable.Sum<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000447 RID: 1095 RVA: 0x0000B213 File Offset: 0x00009413
		public static MethodInfo Sum_NullableInt64_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Sum_NullableInt64_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Sum_NullableInt64_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, long?>>, long?>(Queryable.Sum<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000448 RID: 1096 RVA: 0x0000B249 File Offset: 0x00009449
		public static MethodInfo Sum_Single_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Sum_Single_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Sum_Single_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, float>>, float>(Queryable.Sum<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000449 RID: 1097 RVA: 0x0000B27F File Offset: 0x0000947F
		public static MethodInfo Sum_NullableSingle_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Sum_NullableSingle_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Sum_NullableSingle_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, float?>>, float?>(Queryable.Sum<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x0600044A RID: 1098 RVA: 0x0000B2B5 File Offset: 0x000094B5
		public static MethodInfo Sum_Double_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Sum_Double_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Sum_Double_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, double>>, double>(Queryable.Sum<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x0600044B RID: 1099 RVA: 0x0000B2EB File Offset: 0x000094EB
		public static MethodInfo Sum_NullableDouble_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Sum_NullableDouble_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Sum_NullableDouble_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, double?>>, double?>(Queryable.Sum<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x0600044C RID: 1100 RVA: 0x0000B321 File Offset: 0x00009521
		public static MethodInfo Sum_Decimal_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Sum_Decimal_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Sum_Decimal_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, decimal>>, decimal>(Queryable.Sum<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x0600044D RID: 1101 RVA: 0x0000B357 File Offset: 0x00009557
		public static MethodInfo Take_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Take_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Take_TSource_2 = new Func<IQueryable<object>, int, IQueryable<object>>(Queryable.Take<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x0600044E RID: 1102 RVA: 0x0000B38D File Offset: 0x0000958D
		public static MethodInfo TakeWhile_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_TakeWhile_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_TakeWhile_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, bool>>, IQueryable<object>>(Queryable.TakeWhile<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x0600044F RID: 1103 RVA: 0x0000B3C3 File Offset: 0x000095C3
		public static MethodInfo TakeWhile_Index_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_TakeWhile_Index_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_TakeWhile_Index_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, int, bool>>, IQueryable<object>>(Queryable.TakeWhile<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000450 RID: 1104 RVA: 0x0000B3F9 File Offset: 0x000095F9
		public static MethodInfo ThenBy_TSource_TKey_2(Type TSource, Type TKey)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_ThenBy_TSource_TKey_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_ThenBy_TSource_TKey_2 = new Func<IOrderedQueryable<object>, Expression<Func<object, object>>, IOrderedQueryable<object>>(Queryable.ThenBy<object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TKey
			});
		}

		// Token: 0x06000451 RID: 1105 RVA: 0x0000B433 File Offset: 0x00009633
		public static MethodInfo ThenBy_TSource_TKey_3(Type TSource, Type TKey)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_ThenBy_TSource_TKey_3) == null)
			{
				methodInfo = (CachedReflectionInfo.s_ThenBy_TSource_TKey_3 = new Func<IOrderedQueryable<object>, Expression<Func<object, object>>, IComparer<object>, IOrderedQueryable<object>>(Queryable.ThenBy<object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TKey
			});
		}

		// Token: 0x06000452 RID: 1106 RVA: 0x0000B46D File Offset: 0x0000966D
		public static MethodInfo ThenByDescending_TSource_TKey_2(Type TSource, Type TKey)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_ThenByDescending_TSource_TKey_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_ThenByDescending_TSource_TKey_2 = new Func<IOrderedQueryable<object>, Expression<Func<object, object>>, IOrderedQueryable<object>>(Queryable.ThenByDescending<object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TKey
			});
		}

		// Token: 0x06000453 RID: 1107 RVA: 0x0000B4A7 File Offset: 0x000096A7
		public static MethodInfo ThenByDescending_TSource_TKey_3(Type TSource, Type TKey)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_ThenByDescending_TSource_TKey_3) == null)
			{
				methodInfo = (CachedReflectionInfo.s_ThenByDescending_TSource_TKey_3 = new Func<IOrderedQueryable<object>, Expression<Func<object, object>>, IComparer<object>, IOrderedQueryable<object>>(Queryable.ThenByDescending<object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource,
				TKey
			});
		}

		// Token: 0x06000454 RID: 1108 RVA: 0x0000B4E1 File Offset: 0x000096E1
		public static MethodInfo Union_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Union_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Union_TSource_2 = new Func<IQueryable<object>, IEnumerable<object>, IQueryable<object>>(Queryable.Union<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000455 RID: 1109 RVA: 0x0000B517 File Offset: 0x00009717
		public static MethodInfo Union_TSource_3(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Union_TSource_3) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Union_TSource_3 = new Func<IQueryable<object>, IEnumerable<object>, IEqualityComparer<object>, IQueryable<object>>(Queryable.Union<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000456 RID: 1110 RVA: 0x0000B54D File Offset: 0x0000974D
		public static MethodInfo Where_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Where_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Where_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, bool>>, IQueryable<object>>(Queryable.Where<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000457 RID: 1111 RVA: 0x0000B583 File Offset: 0x00009783
		public static MethodInfo Where_Index_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Where_Index_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Where_Index_TSource_2 = new Func<IQueryable<object>, Expression<Func<object, int, bool>>, IQueryable<object>>(Queryable.Where<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x06000458 RID: 1112 RVA: 0x0000B5B9 File Offset: 0x000097B9
		public static MethodInfo Zip_TFirst_TSecond_TResult_3(Type TFirst, Type TSecond, Type TResult)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Zip_TFirst_TSecond_TResult_3) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Zip_TFirst_TSecond_TResult_3 = new Func<IQueryable<object>, IEnumerable<object>, Expression<Func<object, object, object>>, IQueryable<object>>(Queryable.Zip<object, object, object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TFirst,
				TSecond,
				TResult
			});
		}

		// Token: 0x06000459 RID: 1113 RVA: 0x0000B5F7 File Offset: 0x000097F7
		public static MethodInfo SkipLast_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_SkipLast_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_SkipLast_TSource_2 = new Func<IQueryable<object>, int, IQueryable<object>>(Queryable.SkipLast<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x0600045A RID: 1114 RVA: 0x0000B62D File Offset: 0x0000982D
		public static MethodInfo TakeLast_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_TakeLast_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_TakeLast_TSource_2 = new Func<IQueryable<object>, int, IQueryable<object>>(Queryable.TakeLast<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x0600045B RID: 1115 RVA: 0x0000B663 File Offset: 0x00009863
		public static MethodInfo Append_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Append_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Append_TSource_2 = new Func<IQueryable<object>, object, IQueryable<object>>(Queryable.Append<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x0600045C RID: 1116 RVA: 0x0000B699 File Offset: 0x00009899
		public static MethodInfo Prepend_TSource_2(Type TSource)
		{
			MethodInfo methodInfo;
			if ((methodInfo = CachedReflectionInfo.s_Prepend_TSource_2) == null)
			{
				methodInfo = (CachedReflectionInfo.s_Prepend_TSource_2 = new Func<IQueryable<object>, object, IQueryable<object>>(Queryable.Prepend<object>).GetMethodInfo().GetGenericMethodDefinition());
			}
			return methodInfo.MakeGenericMethod(new Type[]
			{
				TSource
			});
		}

		// Token: 0x0400030C RID: 780
		private static MethodInfo s_Aggregate_TSource_2;

		// Token: 0x0400030D RID: 781
		private static MethodInfo s_Aggregate_TSource_TAccumulate_3;

		// Token: 0x0400030E RID: 782
		private static MethodInfo s_Aggregate_TSource_TAccumulate_TResult_4;

		// Token: 0x0400030F RID: 783
		private static MethodInfo s_All_TSource_2;

		// Token: 0x04000310 RID: 784
		private static MethodInfo s_Any_TSource_1;

		// Token: 0x04000311 RID: 785
		private static MethodInfo s_Any_TSource_2;

		// Token: 0x04000312 RID: 786
		private static MethodInfo s_Average_Int32_1;

		// Token: 0x04000313 RID: 787
		private static MethodInfo s_Average_NullableInt32_1;

		// Token: 0x04000314 RID: 788
		private static MethodInfo s_Average_Int64_1;

		// Token: 0x04000315 RID: 789
		private static MethodInfo s_Average_NullableInt64_1;

		// Token: 0x04000316 RID: 790
		private static MethodInfo s_Average_Single_1;

		// Token: 0x04000317 RID: 791
		private static MethodInfo s_Average_NullableSingle_1;

		// Token: 0x04000318 RID: 792
		private static MethodInfo s_Average_Double_1;

		// Token: 0x04000319 RID: 793
		private static MethodInfo s_Average_NullableDouble_1;

		// Token: 0x0400031A RID: 794
		private static MethodInfo s_Average_Decimal_1;

		// Token: 0x0400031B RID: 795
		private static MethodInfo s_Average_NullableDecimal_1;

		// Token: 0x0400031C RID: 796
		private static MethodInfo s_Average_Int32_TSource_2;

		// Token: 0x0400031D RID: 797
		private static MethodInfo s_Average_NullableInt32_TSource_2;

		// Token: 0x0400031E RID: 798
		private static MethodInfo s_Average_Single_TSource_2;

		// Token: 0x0400031F RID: 799
		private static MethodInfo s_Average_NullableSingle_TSource_2;

		// Token: 0x04000320 RID: 800
		private static MethodInfo s_Average_Int64_TSource_2;

		// Token: 0x04000321 RID: 801
		private static MethodInfo s_Average_NullableInt64_TSource_2;

		// Token: 0x04000322 RID: 802
		private static MethodInfo s_Average_Double_TSource_2;

		// Token: 0x04000323 RID: 803
		private static MethodInfo s_Average_NullableDouble_TSource_2;

		// Token: 0x04000324 RID: 804
		private static MethodInfo s_Average_Decimal_TSource_2;

		// Token: 0x04000325 RID: 805
		private static MethodInfo s_Average_NullableDecimal_TSource_2;

		// Token: 0x04000326 RID: 806
		private static MethodInfo s_Cast_TResult_1;

		// Token: 0x04000327 RID: 807
		private static MethodInfo s_Concat_TSource_2;

		// Token: 0x04000328 RID: 808
		private static MethodInfo s_Contains_TSource_2;

		// Token: 0x04000329 RID: 809
		private static MethodInfo s_Contains_TSource_3;

		// Token: 0x0400032A RID: 810
		private static MethodInfo s_Count_TSource_1;

		// Token: 0x0400032B RID: 811
		private static MethodInfo s_Count_TSource_2;

		// Token: 0x0400032C RID: 812
		private static MethodInfo s_DefaultIfEmpty_TSource_1;

		// Token: 0x0400032D RID: 813
		private static MethodInfo s_DefaultIfEmpty_TSource_2;

		// Token: 0x0400032E RID: 814
		private static MethodInfo s_Distinct_TSource_1;

		// Token: 0x0400032F RID: 815
		private static MethodInfo s_Distinct_TSource_2;

		// Token: 0x04000330 RID: 816
		private static MethodInfo s_ElementAt_TSource_2;

		// Token: 0x04000331 RID: 817
		private static MethodInfo s_ElementAtOrDefault_TSource_2;

		// Token: 0x04000332 RID: 818
		private static MethodInfo s_Except_TSource_2;

		// Token: 0x04000333 RID: 819
		private static MethodInfo s_Except_TSource_3;

		// Token: 0x04000334 RID: 820
		private static MethodInfo s_First_TSource_1;

		// Token: 0x04000335 RID: 821
		private static MethodInfo s_First_TSource_2;

		// Token: 0x04000336 RID: 822
		private static MethodInfo s_FirstOrDefault_TSource_1;

		// Token: 0x04000337 RID: 823
		private static MethodInfo s_FirstOrDefault_TSource_2;

		// Token: 0x04000338 RID: 824
		private static MethodInfo s_GroupBy_TSource_TKey_2;

		// Token: 0x04000339 RID: 825
		private static MethodInfo s_GroupBy_TSource_TKey_3;

		// Token: 0x0400033A RID: 826
		private static MethodInfo s_GroupBy_TSource_TKey_TElement_3;

		// Token: 0x0400033B RID: 827
		private static MethodInfo s_GroupBy_TSource_TKey_TElement_4;

		// Token: 0x0400033C RID: 828
		private static MethodInfo s_GroupBy_TSource_TKey_TResult_3;

		// Token: 0x0400033D RID: 829
		private static MethodInfo s_GroupBy_TSource_TKey_TResult_4;

		// Token: 0x0400033E RID: 830
		private static MethodInfo s_GroupBy_TSource_TKey_TElement_TResult_4;

		// Token: 0x0400033F RID: 831
		private static MethodInfo s_GroupBy_TSource_TKey_TElement_TResult_5;

		// Token: 0x04000340 RID: 832
		private static MethodInfo s_GroupJoin_TOuter_TInner_TKey_TResult_5;

		// Token: 0x04000341 RID: 833
		private static MethodInfo s_GroupJoin_TOuter_TInner_TKey_TResult_6;

		// Token: 0x04000342 RID: 834
		private static MethodInfo s_Intersect_TSource_2;

		// Token: 0x04000343 RID: 835
		private static MethodInfo s_Intersect_TSource_3;

		// Token: 0x04000344 RID: 836
		private static MethodInfo s_Join_TOuter_TInner_TKey_TResult_5;

		// Token: 0x04000345 RID: 837
		private static MethodInfo s_Join_TOuter_TInner_TKey_TResult_6;

		// Token: 0x04000346 RID: 838
		private static MethodInfo s_Last_TSource_1;

		// Token: 0x04000347 RID: 839
		private static MethodInfo s_Last_TSource_2;

		// Token: 0x04000348 RID: 840
		private static MethodInfo s_LastOrDefault_TSource_1;

		// Token: 0x04000349 RID: 841
		private static MethodInfo s_LastOrDefault_TSource_2;

		// Token: 0x0400034A RID: 842
		private static MethodInfo s_LongCount_TSource_1;

		// Token: 0x0400034B RID: 843
		private static MethodInfo s_LongCount_TSource_2;

		// Token: 0x0400034C RID: 844
		private static MethodInfo s_Max_TSource_1;

		// Token: 0x0400034D RID: 845
		private static MethodInfo s_Max_TSource_TResult_2;

		// Token: 0x0400034E RID: 846
		private static MethodInfo s_Min_TSource_1;

		// Token: 0x0400034F RID: 847
		private static MethodInfo s_Min_TSource_TResult_2;

		// Token: 0x04000350 RID: 848
		private static MethodInfo s_OfType_TResult_1;

		// Token: 0x04000351 RID: 849
		private static MethodInfo s_OrderBy_TSource_TKey_2;

		// Token: 0x04000352 RID: 850
		private static MethodInfo s_OrderBy_TSource_TKey_3;

		// Token: 0x04000353 RID: 851
		private static MethodInfo s_OrderByDescending_TSource_TKey_2;

		// Token: 0x04000354 RID: 852
		private static MethodInfo s_OrderByDescending_TSource_TKey_3;

		// Token: 0x04000355 RID: 853
		private static MethodInfo s_Reverse_TSource_1;

		// Token: 0x04000356 RID: 854
		private static MethodInfo s_Select_TSource_TResult_2;

		// Token: 0x04000357 RID: 855
		private static MethodInfo s_Select_Index_TSource_TResult_2;

		// Token: 0x04000358 RID: 856
		private static MethodInfo s_SelectMany_TSource_TResult_2;

		// Token: 0x04000359 RID: 857
		private static MethodInfo s_SelectMany_Index_TSource_TResult_2;

		// Token: 0x0400035A RID: 858
		private static MethodInfo s_SelectMany_Index_TSource_TCollection_TResult_3;

		// Token: 0x0400035B RID: 859
		private static MethodInfo s_SelectMany_TSource_TCollection_TResult_3;

		// Token: 0x0400035C RID: 860
		private static MethodInfo s_SequenceEqual_TSource_2;

		// Token: 0x0400035D RID: 861
		private static MethodInfo s_SequenceEqual_TSource_3;

		// Token: 0x0400035E RID: 862
		private static MethodInfo s_Single_TSource_1;

		// Token: 0x0400035F RID: 863
		private static MethodInfo s_Single_TSource_2;

		// Token: 0x04000360 RID: 864
		private static MethodInfo s_SingleOrDefault_TSource_1;

		// Token: 0x04000361 RID: 865
		private static MethodInfo s_SingleOrDefault_TSource_2;

		// Token: 0x04000362 RID: 866
		private static MethodInfo s_Skip_TSource_2;

		// Token: 0x04000363 RID: 867
		private static MethodInfo s_SkipWhile_TSource_2;

		// Token: 0x04000364 RID: 868
		private static MethodInfo s_SkipWhile_Index_TSource_2;

		// Token: 0x04000365 RID: 869
		private static MethodInfo s_Sum_Int32_1;

		// Token: 0x04000366 RID: 870
		private static MethodInfo s_Sum_NullableInt32_1;

		// Token: 0x04000367 RID: 871
		private static MethodInfo s_Sum_Int64_1;

		// Token: 0x04000368 RID: 872
		private static MethodInfo s_Sum_NullableInt64_1;

		// Token: 0x04000369 RID: 873
		private static MethodInfo s_Sum_Single_1;

		// Token: 0x0400036A RID: 874
		private static MethodInfo s_Sum_NullableSingle_1;

		// Token: 0x0400036B RID: 875
		private static MethodInfo s_Sum_Double_1;

		// Token: 0x0400036C RID: 876
		private static MethodInfo s_Sum_NullableDouble_1;

		// Token: 0x0400036D RID: 877
		private static MethodInfo s_Sum_Decimal_1;

		// Token: 0x0400036E RID: 878
		private static MethodInfo s_Sum_NullableDecimal_1;

		// Token: 0x0400036F RID: 879
		private static MethodInfo s_Sum_NullableDecimal_TSource_2;

		// Token: 0x04000370 RID: 880
		private static MethodInfo s_Sum_Int32_TSource_2;

		// Token: 0x04000371 RID: 881
		private static MethodInfo s_Sum_NullableInt32_TSource_2;

		// Token: 0x04000372 RID: 882
		private static MethodInfo s_Sum_Int64_TSource_2;

		// Token: 0x04000373 RID: 883
		private static MethodInfo s_Sum_NullableInt64_TSource_2;

		// Token: 0x04000374 RID: 884
		private static MethodInfo s_Sum_Single_TSource_2;

		// Token: 0x04000375 RID: 885
		private static MethodInfo s_Sum_NullableSingle_TSource_2;

		// Token: 0x04000376 RID: 886
		private static MethodInfo s_Sum_Double_TSource_2;

		// Token: 0x04000377 RID: 887
		private static MethodInfo s_Sum_NullableDouble_TSource_2;

		// Token: 0x04000378 RID: 888
		private static MethodInfo s_Sum_Decimal_TSource_2;

		// Token: 0x04000379 RID: 889
		private static MethodInfo s_Take_TSource_2;

		// Token: 0x0400037A RID: 890
		private static MethodInfo s_TakeWhile_TSource_2;

		// Token: 0x0400037B RID: 891
		private static MethodInfo s_TakeWhile_Index_TSource_2;

		// Token: 0x0400037C RID: 892
		private static MethodInfo s_ThenBy_TSource_TKey_2;

		// Token: 0x0400037D RID: 893
		private static MethodInfo s_ThenBy_TSource_TKey_3;

		// Token: 0x0400037E RID: 894
		private static MethodInfo s_ThenByDescending_TSource_TKey_2;

		// Token: 0x0400037F RID: 895
		private static MethodInfo s_ThenByDescending_TSource_TKey_3;

		// Token: 0x04000380 RID: 896
		private static MethodInfo s_Union_TSource_2;

		// Token: 0x04000381 RID: 897
		private static MethodInfo s_Union_TSource_3;

		// Token: 0x04000382 RID: 898
		private static MethodInfo s_Where_TSource_2;

		// Token: 0x04000383 RID: 899
		private static MethodInfo s_Where_Index_TSource_2;

		// Token: 0x04000384 RID: 900
		private static MethodInfo s_Zip_TFirst_TSecond_TResult_3;

		// Token: 0x04000385 RID: 901
		private static MethodInfo s_SkipLast_TSource_2;

		// Token: 0x04000386 RID: 902
		private static MethodInfo s_TakeLast_TSource_2;

		// Token: 0x04000387 RID: 903
		private static MethodInfo s_Append_TSource_2;

		// Token: 0x04000388 RID: 904
		private static MethodInfo s_Prepend_TSource_2;
	}
}
