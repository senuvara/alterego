﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Linq
{
	// Token: 0x020000CA RID: 202
	internal class GroupedEnumerable<TSource, TKey, TElement, TResult> : IEnumerable<!3>, IEnumerable
	{
		// Token: 0x06000777 RID: 1911 RVA: 0x0001655C File Offset: 0x0001475C
		public GroupedEnumerable(IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, Func<TKey, IEnumerable<TElement>, TResult> resultSelector, IEqualityComparer<TKey> comparer)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (keySelector == null)
			{
				throw Error.ArgumentNull("keySelector");
			}
			if (elementSelector == null)
			{
				throw Error.ArgumentNull("elementSelector");
			}
			if (resultSelector == null)
			{
				throw Error.ArgumentNull("resultSelector");
			}
			this.source = source;
			this.keySelector = keySelector;
			this.elementSelector = elementSelector;
			this.comparer = comparer;
			this.resultSelector = resultSelector;
		}

		// Token: 0x06000778 RID: 1912 RVA: 0x000165CD File Offset: 0x000147CD
		public IEnumerator<TResult> GetEnumerator()
		{
			return Lookup<TKey, TElement>.Create<TSource>(this.source, this.keySelector, this.elementSelector, this.comparer).ApplyResultSelector<TResult>(this.resultSelector).GetEnumerator();
		}

		// Token: 0x06000779 RID: 1913 RVA: 0x000165FC File Offset: 0x000147FC
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x040004EE RID: 1262
		private IEnumerable<TSource> source;

		// Token: 0x040004EF RID: 1263
		private Func<TSource, TKey> keySelector;

		// Token: 0x040004F0 RID: 1264
		private Func<TSource, TElement> elementSelector;

		// Token: 0x040004F1 RID: 1265
		private IEqualityComparer<TKey> comparer;

		// Token: 0x040004F2 RID: 1266
		private Func<TKey, IEnumerable<TElement>, TResult> resultSelector;
	}
}
