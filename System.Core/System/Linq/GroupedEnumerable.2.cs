﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Linq
{
	// Token: 0x020000CB RID: 203
	internal class GroupedEnumerable<TSource, TKey, TElement> : IEnumerable<IGrouping<TKey, TElement>>, IEnumerable
	{
		// Token: 0x0600077A RID: 1914 RVA: 0x00016604 File Offset: 0x00014804
		public GroupedEnumerable(IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, IEqualityComparer<TKey> comparer)
		{
			if (source == null)
			{
				throw Error.ArgumentNull("source");
			}
			if (keySelector == null)
			{
				throw Error.ArgumentNull("keySelector");
			}
			if (elementSelector == null)
			{
				throw Error.ArgumentNull("elementSelector");
			}
			this.source = source;
			this.keySelector = keySelector;
			this.elementSelector = elementSelector;
			this.comparer = comparer;
		}

		// Token: 0x0600077B RID: 1915 RVA: 0x0001665E File Offset: 0x0001485E
		public IEnumerator<IGrouping<TKey, TElement>> GetEnumerator()
		{
			return Lookup<TKey, TElement>.Create<TSource>(this.source, this.keySelector, this.elementSelector, this.comparer).GetEnumerator();
		}

		// Token: 0x0600077C RID: 1916 RVA: 0x00016682 File Offset: 0x00014882
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x040004F3 RID: 1267
		private IEnumerable<TSource> source;

		// Token: 0x040004F4 RID: 1268
		private Func<TSource, TKey> keySelector;

		// Token: 0x040004F5 RID: 1269
		private Func<TSource, TElement> elementSelector;

		// Token: 0x040004F6 RID: 1270
		private IEqualityComparer<TKey> comparer;
	}
}
