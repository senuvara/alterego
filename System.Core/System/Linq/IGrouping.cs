﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Linq
{
	/// <summary>Represents a collection of objects that have a common key.</summary>
	/// <typeparam name="TKey">The type of the key of the <see cref="T:System.Linq.IGrouping`2" />.</typeparam>
	/// <typeparam name="TElement">The type of the values in the <see cref="T:System.Linq.IGrouping`2" />.</typeparam>
	// Token: 0x020000C1 RID: 193
	public interface IGrouping<out TKey, out TElement> : IEnumerable<!1>, IEnumerable
	{
		/// <summary>Gets the key of the <see cref="T:System.Linq.IGrouping`2" />.</summary>
		/// <returns>The key of the <see cref="T:System.Linq.IGrouping`2" />.</returns>
		// Token: 0x17000104 RID: 260
		// (get) Token: 0x06000739 RID: 1849
		TKey Key { get; }
	}
}
