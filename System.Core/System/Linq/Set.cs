﻿using System;
using System.Collections.Generic;

namespace System.Linq
{
	// Token: 0x020000C8 RID: 200
	internal class Set<TElement>
	{
		// Token: 0x0600076F RID: 1903 RVA: 0x00016227 File Offset: 0x00014427
		public Set() : this(null)
		{
		}

		// Token: 0x06000770 RID: 1904 RVA: 0x00016230 File Offset: 0x00014430
		public Set(IEqualityComparer<TElement> comparer)
		{
			if (comparer == null)
			{
				comparer = EqualityComparer<TElement>.Default;
			}
			this.comparer = comparer;
			this.buckets = new int[7];
			this.slots = new Set<TElement>.Slot[7];
			this.freeList = -1;
		}

		// Token: 0x06000771 RID: 1905 RVA: 0x00016268 File Offset: 0x00014468
		public bool Add(TElement value)
		{
			return !this.Find(value, true);
		}

		// Token: 0x06000772 RID: 1906 RVA: 0x00016275 File Offset: 0x00014475
		public bool Contains(TElement value)
		{
			return this.Find(value, false);
		}

		// Token: 0x06000773 RID: 1907 RVA: 0x00016280 File Offset: 0x00014480
		public bool Remove(TElement value)
		{
			int num = this.InternalGetHashCode(value);
			int num2 = num % this.buckets.Length;
			int num3 = -1;
			for (int i = this.buckets[num2] - 1; i >= 0; i = this.slots[i].next)
			{
				if (this.slots[i].hashCode == num && this.comparer.Equals(this.slots[i].value, value))
				{
					if (num3 < 0)
					{
						this.buckets[num2] = this.slots[i].next + 1;
					}
					else
					{
						this.slots[num3].next = this.slots[i].next;
					}
					this.slots[i].hashCode = -1;
					this.slots[i].value = default(TElement);
					this.slots[i].next = this.freeList;
					this.freeList = i;
					return true;
				}
				num3 = i;
			}
			return false;
		}

		// Token: 0x06000774 RID: 1908 RVA: 0x00016394 File Offset: 0x00014594
		private bool Find(TElement value, bool add)
		{
			int num = this.InternalGetHashCode(value);
			for (int i = this.buckets[num % this.buckets.Length] - 1; i >= 0; i = this.slots[i].next)
			{
				if (this.slots[i].hashCode == num && this.comparer.Equals(this.slots[i].value, value))
				{
					return true;
				}
			}
			if (add)
			{
				int num2;
				if (this.freeList >= 0)
				{
					num2 = this.freeList;
					this.freeList = this.slots[num2].next;
				}
				else
				{
					if (this.count == this.slots.Length)
					{
						this.Resize();
					}
					num2 = this.count;
					this.count++;
				}
				int num3 = num % this.buckets.Length;
				this.slots[num2].hashCode = num;
				this.slots[num2].value = value;
				this.slots[num2].next = this.buckets[num3] - 1;
				this.buckets[num3] = num2 + 1;
			}
			return false;
		}

		// Token: 0x06000775 RID: 1909 RVA: 0x000164BC File Offset: 0x000146BC
		private void Resize()
		{
			int num = checked(this.count * 2 + 1);
			int[] array = new int[num];
			Set<TElement>.Slot[] array2 = new Set<TElement>.Slot[num];
			Array.Copy(this.slots, 0, array2, 0, this.count);
			for (int i = 0; i < this.count; i++)
			{
				int num2 = array2[i].hashCode % num;
				array2[i].next = array[num2] - 1;
				array[num2] = i + 1;
			}
			this.buckets = array;
			this.slots = array2;
		}

		// Token: 0x06000776 RID: 1910 RVA: 0x0001653E File Offset: 0x0001473E
		internal int InternalGetHashCode(TElement value)
		{
			if (value != null)
			{
				return this.comparer.GetHashCode(value) & int.MaxValue;
			}
			return 0;
		}

		// Token: 0x040004E6 RID: 1254
		private int[] buckets;

		// Token: 0x040004E7 RID: 1255
		private Set<TElement>.Slot[] slots;

		// Token: 0x040004E8 RID: 1256
		private int count;

		// Token: 0x040004E9 RID: 1257
		private int freeList;

		// Token: 0x040004EA RID: 1258
		private IEqualityComparer<TElement> comparer;

		// Token: 0x020000C9 RID: 201
		internal struct Slot
		{
			// Token: 0x040004EB RID: 1259
			internal int hashCode;

			// Token: 0x040004EC RID: 1260
			internal TElement value;

			// Token: 0x040004ED RID: 1261
			internal int next;
		}
	}
}
