﻿using System;

namespace System
{
	// Token: 0x02000021 RID: 33
	internal static class MonoUtil
	{
		// Token: 0x0600006C RID: 108 RVA: 0x000022E0 File Offset: 0x000004E0
		static MonoUtil()
		{
			int platform = (int)Environment.OSVersion.Platform;
			MonoUtil.IsUnix = (platform == 4 || platform == 128 || platform == 6);
		}

		// Token: 0x040001CC RID: 460
		public static readonly bool IsUnix;
	}
}
