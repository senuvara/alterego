﻿using System;

namespace System
{
	// Token: 0x0200000B RID: 11
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
	internal class MonoTODOAttribute : Attribute
	{
		// Token: 0x06000024 RID: 36 RVA: 0x000022B6 File Offset: 0x000004B6
		public MonoTODOAttribute()
		{
		}

		// Token: 0x06000025 RID: 37 RVA: 0x000022BE File Offset: 0x000004BE
		public MonoTODOAttribute(string comment)
		{
			this.comment = comment;
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000026 RID: 38 RVA: 0x000022CD File Offset: 0x000004CD
		public string Comment
		{
			get
			{
				return this.comment;
			}
		}

		// Token: 0x040001CB RID: 459
		private string comment;
	}
}
