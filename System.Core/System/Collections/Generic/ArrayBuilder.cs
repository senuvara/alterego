﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x0200047A RID: 1146
	internal struct ArrayBuilder<T>
	{
		// Token: 0x06001BF0 RID: 7152 RVA: 0x00051AD3 File Offset: 0x0004FCD3
		public ArrayBuilder(int capacity)
		{
			this = default(ArrayBuilder<T>);
			if (capacity > 0)
			{
				this._array = new T[capacity];
			}
		}

		// Token: 0x1700056C RID: 1388
		// (get) Token: 0x06001BF1 RID: 7153 RVA: 0x00051AEC File Offset: 0x0004FCEC
		public int Capacity
		{
			get
			{
				T[] array = this._array;
				if (array == null)
				{
					return 0;
				}
				return array.Length;
			}
		}

		// Token: 0x1700056D RID: 1389
		// (get) Token: 0x06001BF2 RID: 7154 RVA: 0x00051AFC File Offset: 0x0004FCFC
		public int Count
		{
			get
			{
				return this._count;
			}
		}

		// Token: 0x1700056E RID: 1390
		public T this[int index]
		{
			get
			{
				return this._array[index];
			}
			set
			{
				this._array[index] = value;
			}
		}

		// Token: 0x06001BF5 RID: 7157 RVA: 0x00051B21 File Offset: 0x0004FD21
		public void Add(T item)
		{
			if (this._count == this.Capacity)
			{
				this.EnsureCapacity(this._count + 1);
			}
			this.UncheckedAdd(item);
		}

		// Token: 0x06001BF6 RID: 7158 RVA: 0x00051B46 File Offset: 0x0004FD46
		public T First()
		{
			return this._array[0];
		}

		// Token: 0x06001BF7 RID: 7159 RVA: 0x00051B54 File Offset: 0x0004FD54
		public T Last()
		{
			return this._array[this._count - 1];
		}

		// Token: 0x06001BF8 RID: 7160 RVA: 0x00051B6C File Offset: 0x0004FD6C
		public T[] ToArray()
		{
			if (this._count == 0)
			{
				return Array.Empty<T>();
			}
			T[] array = this._array;
			if (this._count < array.Length)
			{
				array = new T[this._count];
				Array.Copy(this._array, 0, array, 0, this._count);
			}
			return array;
		}

		// Token: 0x06001BF9 RID: 7161 RVA: 0x00051BBC File Offset: 0x0004FDBC
		public void UncheckedAdd(T item)
		{
			T[] array = this._array;
			int count = this._count;
			this._count = count + 1;
			array[count] = item;
		}

		// Token: 0x06001BFA RID: 7162 RVA: 0x00051BE8 File Offset: 0x0004FDE8
		private void EnsureCapacity(int minimum)
		{
			int capacity = this.Capacity;
			int num = (capacity == 0) ? 4 : (2 * capacity);
			if (num > 2146435071)
			{
				num = Math.Max(capacity + 1, 2146435071);
			}
			num = Math.Max(num, minimum);
			T[] array = new T[num];
			if (this._count > 0)
			{
				Array.Copy(this._array, 0, array, 0, this._count);
			}
			this._array = array;
		}

		// Token: 0x04000CC7 RID: 3271
		private const int DefaultCapacity = 4;

		// Token: 0x04000CC8 RID: 3272
		private const int MaxCoreClrArrayLength = 2146435071;

		// Token: 0x04000CC9 RID: 3273
		private T[] _array;

		// Token: 0x04000CCA RID: 3274
		private int _count;
	}
}
