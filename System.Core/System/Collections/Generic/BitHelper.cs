﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x0200047E RID: 1150
	internal sealed class BitHelper
	{
		// Token: 0x06001C0D RID: 7181 RVA: 0x0005202E File Offset: 0x0005022E
		internal unsafe BitHelper(int* bitArrayPtr, int length)
		{
			this._arrayPtr = bitArrayPtr;
			this._length = length;
			this._useStackAlloc = true;
		}

		// Token: 0x06001C0E RID: 7182 RVA: 0x0005204B File Offset: 0x0005024B
		internal BitHelper(int[] bitArray, int length)
		{
			this._array = bitArray;
			this._length = length;
		}

		// Token: 0x06001C0F RID: 7183 RVA: 0x00052064 File Offset: 0x00050264
		internal unsafe void MarkBit(int bitPosition)
		{
			int num = bitPosition / 32;
			if (num < this._length && num >= 0)
			{
				int num2 = 1 << bitPosition % 32;
				if (this._useStackAlloc)
				{
					this._arrayPtr[num] |= num2;
					return;
				}
				this._array[num] |= num2;
			}
		}

		// Token: 0x06001C10 RID: 7184 RVA: 0x000520B8 File Offset: 0x000502B8
		internal unsafe bool IsMarked(int bitPosition)
		{
			int num = bitPosition / 32;
			if (num >= this._length || num < 0)
			{
				return false;
			}
			int num2 = 1 << bitPosition % 32;
			if (this._useStackAlloc)
			{
				return (this._arrayPtr[num] & num2) != 0;
			}
			return (this._array[num] & num2) != 0;
		}

		// Token: 0x06001C11 RID: 7185 RVA: 0x0005210A File Offset: 0x0005030A
		internal static int ToIntArrayLength(int n)
		{
			if (n <= 0)
			{
				return 0;
			}
			return (n - 1) / 32 + 1;
		}

		// Token: 0x04000CD0 RID: 3280
		private const byte MarkedBitFlag = 1;

		// Token: 0x04000CD1 RID: 3281
		private const byte IntSize = 32;

		// Token: 0x04000CD2 RID: 3282
		private readonly int _length;

		// Token: 0x04000CD3 RID: 3283
		private unsafe readonly int* _arrayPtr;

		// Token: 0x04000CD4 RID: 3284
		private readonly int[] _array;

		// Token: 0x04000CD5 RID: 3285
		private readonly bool _useStackAlloc;
	}
}
