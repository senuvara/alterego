﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace System.Collections.Generic
{
	// Token: 0x02000477 RID: 1143
	[DebuggerDisplay("{DebuggerDisplay,nq}")]
	internal struct CopyPosition
	{
		// Token: 0x06001BDD RID: 7133 RVA: 0x000516AE File Offset: 0x0004F8AE
		internal CopyPosition(int row, int column)
		{
			this.Row = row;
			this.Column = column;
		}

		// Token: 0x17000567 RID: 1383
		// (get) Token: 0x06001BDE RID: 7134 RVA: 0x000516C0 File Offset: 0x0004F8C0
		public static CopyPosition Start
		{
			get
			{
				return default(CopyPosition);
			}
		}

		// Token: 0x17000568 RID: 1384
		// (get) Token: 0x06001BDF RID: 7135 RVA: 0x000516D6 File Offset: 0x0004F8D6
		internal int Row
		{
			[CompilerGenerated]
			get
			{
				return this.<Row>k__BackingField;
			}
		}

		// Token: 0x17000569 RID: 1385
		// (get) Token: 0x06001BE0 RID: 7136 RVA: 0x000516DE File Offset: 0x0004F8DE
		internal int Column
		{
			[CompilerGenerated]
			get
			{
				return this.<Column>k__BackingField;
			}
		}

		// Token: 0x06001BE1 RID: 7137 RVA: 0x000516E6 File Offset: 0x0004F8E6
		public CopyPosition Normalize(int endColumn)
		{
			if (this.Column != endColumn)
			{
				return this;
			}
			return new CopyPosition(this.Row + 1, 0);
		}

		// Token: 0x1700056A RID: 1386
		// (get) Token: 0x06001BE2 RID: 7138 RVA: 0x00051706 File Offset: 0x0004F906
		private string DebuggerDisplay
		{
			get
			{
				return string.Format("[{0}, {1}]", this.Row, this.Column);
			}
		}

		// Token: 0x04000CBA RID: 3258
		[CompilerGenerated]
		private readonly int <Row>k__BackingField;

		// Token: 0x04000CBB RID: 3259
		[CompilerGenerated]
		private readonly int <Column>k__BackingField;
	}
}
