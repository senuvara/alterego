﻿using System;
using System.Linq;

namespace System.Collections.Generic
{
	// Token: 0x0200047B RID: 1147
	internal static class EnumerableHelpers
	{
		// Token: 0x06001BFB RID: 7163 RVA: 0x00051C50 File Offset: 0x0004FE50
		internal static bool TryGetCount<T>(IEnumerable<T> source, out int count)
		{
			ICollection<T> collection;
			if ((collection = (source as ICollection<T>)) != null)
			{
				count = collection.Count;
				return true;
			}
			IIListProvider<T> iilistProvider;
			if ((iilistProvider = (source as IIListProvider<T>)) != null)
			{
				return (count = iilistProvider.GetCount(true)) >= 0;
			}
			count = -1;
			return false;
		}

		// Token: 0x06001BFC RID: 7164 RVA: 0x00051C94 File Offset: 0x0004FE94
		internal static void Copy<T>(IEnumerable<T> source, T[] array, int arrayIndex, int count)
		{
			ICollection<T> collection;
			if ((collection = (source as ICollection<T>)) != null)
			{
				collection.CopyTo(array, arrayIndex);
				return;
			}
			EnumerableHelpers.IterativeCopy<T>(source, array, arrayIndex, count);
		}

		// Token: 0x06001BFD RID: 7165 RVA: 0x00051CC0 File Offset: 0x0004FEC0
		internal static void IterativeCopy<T>(IEnumerable<T> source, T[] array, int arrayIndex, int count)
		{
			foreach (T t in source)
			{
				array[arrayIndex++] = t;
			}
		}

		// Token: 0x06001BFE RID: 7166 RVA: 0x00051D10 File Offset: 0x0004FF10
		internal static T[] ToArray<T>(IEnumerable<T> source)
		{
			ICollection<T> collection;
			if ((collection = (source as ICollection<T>)) == null)
			{
				LargeArrayBuilder<T> largeArrayBuilder = new LargeArrayBuilder<T>(true);
				largeArrayBuilder.AddRange(source);
				return largeArrayBuilder.ToArray();
			}
			int count = collection.Count;
			if (count == 0)
			{
				return Array.Empty<T>();
			}
			T[] array = new T[count];
			collection.CopyTo(array, 0);
			return array;
		}

		// Token: 0x06001BFF RID: 7167 RVA: 0x00051D60 File Offset: 0x0004FF60
		internal static T[] ToArray<T>(IEnumerable<T> source, out int length)
		{
			ICollection<T> collection;
			if ((collection = (source as ICollection<T>)) != null)
			{
				int count = collection.Count;
				if (count != 0)
				{
					T[] array = new T[count];
					collection.CopyTo(array, 0);
					length = count;
					return array;
				}
			}
			else
			{
				using (IEnumerator<T> enumerator = source.GetEnumerator())
				{
					if (enumerator.MoveNext())
					{
						T[] array2 = new T[4];
						array2[0] = enumerator.Current;
						int num = 1;
						while (enumerator.MoveNext())
						{
							if (num == array2.Length)
							{
								int num2 = num << 1;
								if (num2 > 2146435071)
								{
									num2 = ((2146435071 <= num) ? (num + 1) : 2146435071);
								}
								Array.Resize<T>(ref array2, num2);
							}
							array2[num++] = enumerator.Current;
						}
						length = num;
						return array2;
					}
				}
			}
			length = 0;
			return Array.Empty<T>();
		}
	}
}
