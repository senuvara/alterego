﻿using System;
using System.Diagnostics;

namespace System.Collections.Generic
{
	// Token: 0x02000484 RID: 1156
	internal sealed class ICollectionDebugView<T>
	{
		// Token: 0x06001C54 RID: 7252 RVA: 0x0005399C File Offset: 0x00051B9C
		public ICollectionDebugView(ICollection<T> collection)
		{
			if (collection == null)
			{
				throw new ArgumentNullException("collection");
			}
			this._collection = collection;
		}

		// Token: 0x17000579 RID: 1401
		// (get) Token: 0x06001C55 RID: 7253 RVA: 0x000539BC File Offset: 0x00051BBC
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public T[] Items
		{
			get
			{
				T[] array = new T[this._collection.Count];
				this._collection.CopyTo(array, 0);
				return array;
			}
		}

		// Token: 0x04000CEF RID: 3311
		private readonly ICollection<T> _collection;
	}
}
