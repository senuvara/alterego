﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x0200047D RID: 1149
	internal struct SparseArrayBuilder<T>
	{
		// Token: 0x06001C04 RID: 7172 RVA: 0x00051EA6 File Offset: 0x000500A6
		public SparseArrayBuilder(bool initialize)
		{
			this = default(SparseArrayBuilder<T>);
			this._builder = new LargeArrayBuilder<T>(true);
		}

		// Token: 0x17000572 RID: 1394
		// (get) Token: 0x06001C05 RID: 7173 RVA: 0x00051EBB File Offset: 0x000500BB
		public int Count
		{
			get
			{
				return checked(this._builder.Count + this._reservedCount);
			}
		}

		// Token: 0x17000573 RID: 1395
		// (get) Token: 0x06001C06 RID: 7174 RVA: 0x00051ECF File Offset: 0x000500CF
		public ArrayBuilder<Marker> Markers
		{
			get
			{
				return this._markers;
			}
		}

		// Token: 0x06001C07 RID: 7175 RVA: 0x00051ED7 File Offset: 0x000500D7
		public void Add(T item)
		{
			this._builder.Add(item);
		}

		// Token: 0x06001C08 RID: 7176 RVA: 0x00051EE5 File Offset: 0x000500E5
		public void AddRange(IEnumerable<T> items)
		{
			this._builder.AddRange(items);
		}

		// Token: 0x06001C09 RID: 7177 RVA: 0x00051EF4 File Offset: 0x000500F4
		public void CopyTo(T[] array, int arrayIndex, int count)
		{
			int num = 0;
			CopyPosition position = CopyPosition.Start;
			for (int i = 0; i < this._markers.Count; i++)
			{
				Marker marker = this._markers[i];
				int num2 = Math.Min(marker.Index - num, count);
				if (num2 > 0)
				{
					position = this._builder.CopyTo(position, array, arrayIndex, num2);
					arrayIndex += num2;
					num += num2;
					count -= num2;
				}
				if (count == 0)
				{
					return;
				}
				int num3 = Math.Min(marker.Count, count);
				arrayIndex += num3;
				num += num3;
				count -= num3;
			}
			if (count > 0)
			{
				this._builder.CopyTo(position, array, arrayIndex, count);
			}
		}

		// Token: 0x06001C0A RID: 7178 RVA: 0x00051F9C File Offset: 0x0005019C
		public void Reserve(int count)
		{
			this._markers.Add(new Marker(count, this.Count));
			checked
			{
				this._reservedCount += count;
			}
		}

		// Token: 0x06001C0B RID: 7179 RVA: 0x00051FC4 File Offset: 0x000501C4
		public bool ReserveOrAdd(IEnumerable<T> items)
		{
			int num;
			if (EnumerableHelpers.TryGetCount<T>(items, out num))
			{
				if (num > 0)
				{
					this.Reserve(num);
					return true;
				}
			}
			else
			{
				this.AddRange(items);
			}
			return false;
		}

		// Token: 0x06001C0C RID: 7180 RVA: 0x00051FF0 File Offset: 0x000501F0
		public T[] ToArray()
		{
			if (this._markers.Count == 0)
			{
				return this._builder.ToArray();
			}
			T[] array = new T[this.Count];
			this.CopyTo(array, 0, array.Length);
			return array;
		}

		// Token: 0x04000CCD RID: 3277
		private LargeArrayBuilder<T> _builder;

		// Token: 0x04000CCE RID: 3278
		private ArrayBuilder<Marker> _markers;

		// Token: 0x04000CCF RID: 3279
		private int _reservedCount;
	}
}
