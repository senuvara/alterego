﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Collections.Generic
{
	// Token: 0x02000478 RID: 1144
	internal struct LargeArrayBuilder<T>
	{
		// Token: 0x06001BE3 RID: 7139 RVA: 0x00051728 File Offset: 0x0004F928
		public LargeArrayBuilder(bool initialize)
		{
			this = new LargeArrayBuilder<T>(int.MaxValue);
		}

		// Token: 0x06001BE4 RID: 7140 RVA: 0x00051738 File Offset: 0x0004F938
		public LargeArrayBuilder(int maxCapacity)
		{
			this = default(LargeArrayBuilder<T>);
			this._first = (this._current = Array.Empty<T>());
			this._maxCapacity = maxCapacity;
		}

		// Token: 0x1700056B RID: 1387
		// (get) Token: 0x06001BE5 RID: 7141 RVA: 0x00051767 File Offset: 0x0004F967
		public int Count
		{
			get
			{
				return this._count;
			}
		}

		// Token: 0x06001BE6 RID: 7142 RVA: 0x00051770 File Offset: 0x0004F970
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void Add(T item)
		{
			if (this._index == this._current.Length)
			{
				this.AllocateBuffer();
			}
			T[] current = this._current;
			int index = this._index;
			this._index = index + 1;
			current[index] = item;
			this._count++;
		}

		// Token: 0x06001BE7 RID: 7143 RVA: 0x000517C0 File Offset: 0x0004F9C0
		public void AddRange(IEnumerable<T> items)
		{
			using (IEnumerator<T> enumerator = items.GetEnumerator())
			{
				T[] current = this._current;
				int index = this._index;
				while (enumerator.MoveNext())
				{
					if (index == current.Length)
					{
						this._count += index - this._index;
						this._index = index;
						this.AllocateBuffer();
						current = this._current;
						index = this._index;
					}
					current[index++] = enumerator.Current;
				}
				this._count += index - this._index;
				this._index = index;
			}
		}

		// Token: 0x06001BE8 RID: 7144 RVA: 0x0005186C File Offset: 0x0004FA6C
		public void CopyTo(T[] array, int arrayIndex, int count)
		{
			int num = 0;
			while (count > 0)
			{
				T[] buffer = this.GetBuffer(num);
				int num2 = Math.Min(count, buffer.Length);
				Array.Copy(buffer, 0, array, arrayIndex, num2);
				count -= num2;
				arrayIndex += num2;
				num++;
			}
		}

		// Token: 0x06001BE9 RID: 7145 RVA: 0x000518AC File Offset: 0x0004FAAC
		public CopyPosition CopyTo(CopyPosition position, T[] array, int arrayIndex, int count)
		{
			LargeArrayBuilder<T>.<>c__DisplayClass15_0 CS$<>8__locals1;
			CS$<>8__locals1.count = count;
			CS$<>8__locals1.array = array;
			CS$<>8__locals1.arrayIndex = arrayIndex;
			int num = position.Row;
			int column = position.Column;
			T[] buffer = this.GetBuffer(num);
			int num2 = LargeArrayBuilder<T>.<CopyTo>g__CopyToCore|15_0(buffer, column, ref CS$<>8__locals1);
			if (CS$<>8__locals1.count == 0)
			{
				return new CopyPosition(num, column + num2).Normalize(buffer.Length);
			}
			do
			{
				buffer = this.GetBuffer(++num);
				num2 = LargeArrayBuilder<T>.<CopyTo>g__CopyToCore|15_0(buffer, 0, ref CS$<>8__locals1);
			}
			while (CS$<>8__locals1.count > 0);
			return new CopyPosition(num, num2).Normalize(buffer.Length);
		}

		// Token: 0x06001BEA RID: 7146 RVA: 0x00051948 File Offset: 0x0004FB48
		public T[] GetBuffer(int index)
		{
			if (index == 0)
			{
				return this._first;
			}
			if (index > this._buffers.Count)
			{
				return this._current;
			}
			return this._buffers[index - 1];
		}

		// Token: 0x06001BEB RID: 7147 RVA: 0x00051977 File Offset: 0x0004FB77
		[MethodImpl(MethodImplOptions.NoInlining)]
		public void SlowAdd(T item)
		{
			this.Add(item);
		}

		// Token: 0x06001BEC RID: 7148 RVA: 0x00051980 File Offset: 0x0004FB80
		public T[] ToArray()
		{
			T[] array;
			if (this.TryMove(out array))
			{
				return array;
			}
			array = new T[this._count];
			this.CopyTo(array, 0, this._count);
			return array;
		}

		// Token: 0x06001BED RID: 7149 RVA: 0x000519B4 File Offset: 0x0004FBB4
		public bool TryMove(out T[] array)
		{
			array = this._first;
			return this._count == this._first.Length;
		}

		// Token: 0x06001BEE RID: 7150 RVA: 0x000519D0 File Offset: 0x0004FBD0
		private void AllocateBuffer()
		{
			if (this._count < 8)
			{
				int num = Math.Min((this._count == 0) ? 4 : (this._count * 2), this._maxCapacity);
				this._current = new T[num];
				Array.Copy(this._first, 0, this._current, 0, this._count);
				this._first = this._current;
				return;
			}
			int num2;
			if (this._count == 8)
			{
				num2 = 8;
			}
			else
			{
				this._buffers.Add(this._current);
				num2 = Math.Min(this._count, this._maxCapacity - this._count);
			}
			this._current = new T[num2];
			this._index = 0;
		}

		// Token: 0x06001BEF RID: 7151 RVA: 0x00051A84 File Offset: 0x0004FC84
		[CompilerGenerated]
		internal static int <CopyTo>g__CopyToCore|15_0(T[] sourceBuffer, int sourceIndex, ref LargeArrayBuilder<T>.<>c__DisplayClass15_0 A_2)
		{
			int num = Math.Min(sourceBuffer.Length - sourceIndex, A_2.count);
			Array.Copy(sourceBuffer, sourceIndex, A_2.array, A_2.arrayIndex, num);
			A_2.arrayIndex += num;
			A_2.count -= num;
			return num;
		}

		// Token: 0x04000CBC RID: 3260
		private const int StartingCapacity = 4;

		// Token: 0x04000CBD RID: 3261
		private const int ResizeLimit = 8;

		// Token: 0x04000CBE RID: 3262
		private readonly int _maxCapacity;

		// Token: 0x04000CBF RID: 3263
		private T[] _first;

		// Token: 0x04000CC0 RID: 3264
		private ArrayBuilder<T[]> _buffers;

		// Token: 0x04000CC1 RID: 3265
		private T[] _current;

		// Token: 0x04000CC2 RID: 3266
		private int _index;

		// Token: 0x04000CC3 RID: 3267
		private int _count;

		// Token: 0x02000479 RID: 1145
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <>c__DisplayClass15_0
		{
			// Token: 0x04000CC4 RID: 3268
			public int count;

			// Token: 0x04000CC5 RID: 3269
			public T[] array;

			// Token: 0x04000CC6 RID: 3270
			public int arrayIndex;
		}
	}
}
