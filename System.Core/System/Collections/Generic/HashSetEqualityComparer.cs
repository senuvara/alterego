﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x02000483 RID: 1155
	[Serializable]
	internal sealed class HashSetEqualityComparer<T> : IEqualityComparer<HashSet<T>>
	{
		// Token: 0x06001C4F RID: 7247 RVA: 0x000538DF File Offset: 0x00051ADF
		public HashSetEqualityComparer()
		{
			this._comparer = EqualityComparer<T>.Default;
		}

		// Token: 0x06001C50 RID: 7248 RVA: 0x000538F2 File Offset: 0x00051AF2
		public bool Equals(HashSet<T> x, HashSet<T> y)
		{
			return HashSet<T>.HashSetEquals(x, y, this._comparer);
		}

		// Token: 0x06001C51 RID: 7249 RVA: 0x00053904 File Offset: 0x00051B04
		public int GetHashCode(HashSet<T> obj)
		{
			int num = 0;
			if (obj != null)
			{
				foreach (T obj2 in obj)
				{
					num ^= (this._comparer.GetHashCode(obj2) & int.MaxValue);
				}
			}
			return num;
		}

		// Token: 0x06001C52 RID: 7250 RVA: 0x00053968 File Offset: 0x00051B68
		public override bool Equals(object obj)
		{
			HashSetEqualityComparer<T> hashSetEqualityComparer = obj as HashSetEqualityComparer<T>;
			return hashSetEqualityComparer != null && this._comparer == hashSetEqualityComparer._comparer;
		}

		// Token: 0x06001C53 RID: 7251 RVA: 0x0005398F File Offset: 0x00051B8F
		public override int GetHashCode()
		{
			return this._comparer.GetHashCode();
		}

		// Token: 0x04000CEE RID: 3310
		private readonly IEqualityComparer<T> _comparer;
	}
}
