﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace System.Collections.Generic
{
	// Token: 0x0200047C RID: 1148
	[DebuggerDisplay("{DebuggerDisplay,nq}")]
	internal struct Marker
	{
		// Token: 0x06001C00 RID: 7168 RVA: 0x00051E48 File Offset: 0x00050048
		public Marker(int count, int index)
		{
			this.Count = count;
			this.Index = index;
		}

		// Token: 0x1700056F RID: 1391
		// (get) Token: 0x06001C01 RID: 7169 RVA: 0x00051E58 File Offset: 0x00050058
		public int Count
		{
			[CompilerGenerated]
			get
			{
				return this.<Count>k__BackingField;
			}
		}

		// Token: 0x17000570 RID: 1392
		// (get) Token: 0x06001C02 RID: 7170 RVA: 0x00051E60 File Offset: 0x00050060
		public int Index
		{
			[CompilerGenerated]
			get
			{
				return this.<Index>k__BackingField;
			}
		}

		// Token: 0x17000571 RID: 1393
		// (get) Token: 0x06001C03 RID: 7171 RVA: 0x00051E68 File Offset: 0x00050068
		private string DebuggerDisplay
		{
			get
			{
				return string.Format("{0}: {1}, {2}: {3}", new object[]
				{
					"Index",
					this.Index,
					"Count",
					this.Count
				});
			}
		}

		// Token: 0x04000CCB RID: 3275
		[CompilerGenerated]
		private readonly int <Count>k__BackingField;

		// Token: 0x04000CCC RID: 3276
		[CompilerGenerated]
		private readonly int <Index>k__BackingField;
	}
}
