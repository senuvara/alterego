﻿using System;

namespace System.Threading
{
	// Token: 0x02000023 RID: 35
	internal class ReaderWriterCount
	{
		// Token: 0x0600006D RID: 109 RVA: 0x00002310 File Offset: 0x00000510
		public ReaderWriterCount()
		{
		}

		// Token: 0x040001D0 RID: 464
		public long lockID;

		// Token: 0x040001D1 RID: 465
		public int readercount;

		// Token: 0x040001D2 RID: 466
		public int writercount;

		// Token: 0x040001D3 RID: 467
		public int upgradecount;

		// Token: 0x040001D4 RID: 468
		public ReaderWriterCount next;
	}
}
