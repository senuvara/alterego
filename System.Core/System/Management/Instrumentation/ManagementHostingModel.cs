﻿using System;

namespace System.Management.Instrumentation
{
	/// <summary>Defines values that specify the hosting model for the provider.Note: the WMI .NET libraries are now considered in final state, and no further development, enhancements, or updates will be available for non-security related issues affecting these libraries. The MI APIs should be used for all new development.</summary>
	// Token: 0x020004A6 RID: 1190
	public enum ManagementHostingModel
	{
		/// <summary>Activates the provider as a decoupled provider.</summary>
		// Token: 0x04000D2D RID: 3373
		Decoupled,
		/// <summary>Activates the provider in the provider host process that is running under the LocalService account.</summary>
		// Token: 0x04000D2E RID: 3374
		LocalService = 2,
		/// <summary>Activates the provider in the provider host process that is running under the LocalSystem account.</summary>
		// Token: 0x04000D2F RID: 3375
		LocalSystem,
		/// <summary>Activates the provider in the provider host process that is running under the NetworkService account.</summary>
		// Token: 0x04000D30 RID: 3376
		NetworkService = 1
	}
}
