﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Management.Instrumentation
{
	/// <summary>The ManagementBind attribute indicates that a method is used to return the instance of a WMI class associated with a specific key value.Note: the WMI .NET libraries are now considered in final state, and no further development, enhancements, or updates will be available for non-security related issues affecting these libraries. The MI APIs should be used for all new development.</summary>
	// Token: 0x0200049D RID: 1181
	[AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = false)]
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class ManagementBindAttribute : ManagementNewInstanceAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Management.ManagementBindAttribute" /> class. This is the default constructor.</summary>
		// Token: 0x06001CC0 RID: 7360 RVA: 0x000039E8 File Offset: 0x00001BE8
		public ManagementBindAttribute()
		{
		}

		/// <summary>Gets or sets a value that defines the type of output that the method that is marked with the ManagementEnumerator attribute will output.</summary>
		/// <returns>A <see cref="T:System.Type" /> value that indicates the type of output that the method marked with the <see cref="ManagementBind" /> attribute will output.</returns>
		// Token: 0x1700059E RID: 1438
		// (get) Token: 0x06001CC1 RID: 7361 RVA: 0x0004B26D File Offset: 0x0004946D
		// (set) Token: 0x06001CC2 RID: 7362 RVA: 0x0000220F File Offset: 0x0000040F
		public Type Schema
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
