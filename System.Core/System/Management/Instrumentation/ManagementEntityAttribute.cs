﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Management.Instrumentation
{
	/// <summary>The ManagementEntity attribute indicates that a class provides management information exposed through a WMI provider.Note: the WMI .NET libraries are now considered in final state, and no further development, enhancements, or updates will be available for non-security related issues affecting these libraries. The MI APIs should be used for all new development.</summary>
	// Token: 0x020004A4 RID: 1188
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class ManagementEntityAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Management.ManagementEntityAttribute" /> class. This is the default constructor.</summary>
		// Token: 0x06001CCE RID: 7374 RVA: 0x000039E8 File Offset: 0x00001BE8
		public ManagementEntityAttribute()
		{
		}

		/// <summary>Gets or sets a value that specifies whether the class represents a WMI class in a provider implemented external to the current assembly.</summary>
		/// <returns>A boolean value that is true if the class represents an external WMI class and false otherwise.</returns>
		// Token: 0x170005A2 RID: 1442
		// (get) Token: 0x06001CCF RID: 7375 RVA: 0x00053BA8 File Offset: 0x00051DA8
		// (set) Token: 0x06001CD0 RID: 7376 RVA: 0x0000220F File Offset: 0x0000040F
		public bool External
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the WMI class.</summary>
		/// <returns>A string that contains the name of the WMI class.</returns>
		// Token: 0x170005A3 RID: 1443
		// (get) Token: 0x06001CD1 RID: 7377 RVA: 0x0004B26D File Offset: 0x0004946D
		// (set) Token: 0x06001CD2 RID: 7378 RVA: 0x0000220F File Offset: 0x0000040F
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Specifies whether the associated class represents a singleton WMI class.</summary>
		/// <returns>A boolean value that is true if the class represents a singleton WMI class and false otherwise.</returns>
		// Token: 0x170005A4 RID: 1444
		// (get) Token: 0x06001CD3 RID: 7379 RVA: 0x00053BC4 File Offset: 0x00051DC4
		// (set) Token: 0x06001CD4 RID: 7380 RVA: 0x0000220F File Offset: 0x0000040F
		public bool Singleton
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
