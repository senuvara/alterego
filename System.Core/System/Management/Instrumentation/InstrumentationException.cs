﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Unity;

namespace System.Management.Instrumentation
{
	/// <summary>Represents a provider-related exception.Note: the WMI .NET libraries are now considered in final state, and no further development, enhancements, or updates will be available for non-security related issues affecting these libraries. The MI APIs should be used for all new development.</summary>
	// Token: 0x0200049B RID: 1179
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	[Serializable]
	public class InstrumentationException : InstrumentationBaseException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Management.Instrumentation.InstrumentationException" /> class. This is the default constructor.</summary>
		// Token: 0x06001CB7 RID: 7351 RVA: 0x0000220F File Offset: 0x0000040F
		public InstrumentationException()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Management.Instrumentation.InstrumentationException" /> class with the System.Exception that caused the current exception.</summary>
		/// <param name="innerException">The Exception instance that caused the current exception.</param>
		// Token: 0x06001CB8 RID: 7352 RVA: 0x0000220F File Offset: 0x0000040F
		public InstrumentationException(Exception innerException)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Management.Instrumentation.InstrumentationException" /> class with serialization information.</summary>
		/// <param name="info">The data that is required to serialize or deserialize an object.</param>
		/// <param name="context">Description of the source and destination of the specified serialized stream.</param>
		// Token: 0x06001CB9 RID: 7353 RVA: 0x0000220F File Offset: 0x0000040F
		protected InstrumentationException(SerializationInfo info, StreamingContext context)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Management.Instrumentation.InstrumentationException" /> class with a message that describes the exception.</summary>
		/// <param name="message">Message that describes the exception.</param>
		// Token: 0x06001CBA RID: 7354 RVA: 0x0000220F File Offset: 0x0000040F
		public InstrumentationException(string message)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Management.Instrumentation.InstrumentationException" /> class with the specified string and exception.</summary>
		/// <param name="message">Message that describes the exception.</param>
		/// <param name="innerException">The Exception instance that caused the current exception.</param>
		// Token: 0x06001CBB RID: 7355 RVA: 0x0000220F File Offset: 0x0000040F
		public InstrumentationException(string message, Exception innerException)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
