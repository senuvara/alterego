﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Management.Instrumentation
{
	/// <summary>This class is used by the WMI.NET Provider Extensions framework. It is the base class for all the management attributes that can be applied to members.Note: the WMI .NET libraries are now considered in final state, and no further development, enhancements, or updates will be available for non-security related issues affecting these libraries. The MI APIs should be used for all new development.</summary>
	// Token: 0x0200049F RID: 1183
	[AttributeUsage(AttributeTargets.All)]
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public abstract class ManagementMemberAttribute : Attribute
	{
		/// <summary>Initializes a new instance of <see cref="T:System.Management.ManagementMemberAttribute" /> the class. This is the default constructor.</summary>
		// Token: 0x06001CC4 RID: 7364 RVA: 0x000039E8 File Offset: 0x00001BE8
		protected ManagementMemberAttribute()
		{
		}

		/// <summary>Gets or sets the name of the management attribute.</summary>
		/// <returns>Returns a string which is the name of the management attribute.</returns>
		// Token: 0x1700059F RID: 1439
		// (get) Token: 0x06001CC5 RID: 7365 RVA: 0x0004B26D File Offset: 0x0004946D
		// (set) Token: 0x06001CC6 RID: 7366 RVA: 0x0000220F File Offset: 0x0000040F
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
