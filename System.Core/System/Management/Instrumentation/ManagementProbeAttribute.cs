﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Management.Instrumentation
{
	/// <summary>The ManagementProbe attribute indicates that a property or field represents a read-only WMI property.Note: the WMI .NET libraries are now considered in final state, and no further development, enhancements, or updates will be available for non-security related issues affecting these libraries. The MI APIs should be used for all new development.</summary>
	// Token: 0x020004A9 RID: 1193
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class ManagementProbeAttribute : ManagementMemberAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Management.ManagementProbeAttribute" /> class. This is the default constructor for the class.</summary>
		// Token: 0x06001CDB RID: 7387 RVA: 0x000039E8 File Offset: 0x00001BE8
		public ManagementProbeAttribute()
		{
		}

		/// <summary>Gets or sets a value that defines the type of output that the property that is marked with the ManagementProbe attribute will output.</summary>
		/// <returns>A <see cref="T:System.Type" /> value that indicates the type of output that the property that is marked with the ManagementProbe attribute will output.</returns>
		// Token: 0x170005A7 RID: 1447
		// (get) Token: 0x06001CDC RID: 7388 RVA: 0x0004B26D File Offset: 0x0004946D
		// (set) Token: 0x06001CDD RID: 7389 RVA: 0x0000220F File Offset: 0x0000040F
		public Type Schema
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
