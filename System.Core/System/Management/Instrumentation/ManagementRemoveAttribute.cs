﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Management.Instrumentation
{
	/// <summary>The ManagementRemoveAttribute is used to indicate that a method cleans up an instance of a managed entity.Note: the WMI .NET libraries are now considered in final state, and no further development, enhancements, or updates will be available for non-security related issues affecting these libraries. The MI APIs should be used for all new development.</summary>
	// Token: 0x020004AB RID: 1195
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class ManagementRemoveAttribute : ManagementMemberAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Management.ManagementRemoveAttribute" /> class. This is the default constructor.</summary>
		// Token: 0x06001CE1 RID: 7393 RVA: 0x000039E8 File Offset: 0x00001BE8
		public ManagementRemoveAttribute()
		{
		}

		/// <summary>Gets or sets a value that defines the type of output that the object that is marked with the ManagementRemove attribute will output.</summary>
		/// <returns>A <see cref="T:System.Type" /> value that indicates the type of output that the object marked with the Remove attribute will output.</returns>
		// Token: 0x170005A9 RID: 1449
		// (get) Token: 0x06001CE2 RID: 7394 RVA: 0x0004B26D File Offset: 0x0004946D
		// (set) Token: 0x06001CE3 RID: 7395 RVA: 0x0000220F File Offset: 0x0000040F
		public Type Schema
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
