﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Management.Instrumentation
{
	/// <summary>The ManagementReferenceAttribute marks a class member, property or method parameter as a reference to another management object or class.Note: the WMI .NET libraries are now considered in final state, and no further development, enhancements, or updates will be available for non-security related issues affecting these libraries. The MI APIs should be used for all new development.</summary>
	// Token: 0x020004AA RID: 1194
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class ManagementReferenceAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Management.ManagementReferenceAttribute" /> class. This is the default constructor.</summary>
		// Token: 0x06001CDE RID: 7390 RVA: 0x000039E8 File Offset: 0x00001BE8
		public ManagementReferenceAttribute()
		{
		}

		/// <summary>Gets or sets the name of the referenced type.</summary>
		/// <returns>A string containing the name of the referenced type.</returns>
		// Token: 0x170005A8 RID: 1448
		// (get) Token: 0x06001CDF RID: 7391 RVA: 0x0004B26D File Offset: 0x0004946D
		// (set) Token: 0x06001CE0 RID: 7392 RVA: 0x0000220F File Offset: 0x0000040F
		public string Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
