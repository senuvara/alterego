﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Management.Instrumentation
{
	/// <summary>The ManagementEnumerator attribute marks a method that returns all the instances of a WMI class.Note: the WMI .NET libraries are now considered in final state, and no further development, enhancements, or updates will be available for non-security related issues affecting these libraries. The MI APIs should be used for all new development.</summary>
	// Token: 0x020004A5 RID: 1189
	[AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = false)]
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class ManagementEnumeratorAttribute : ManagementNewInstanceAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Management.ManagementEnumeratorAttribute" /> class.</summary>
		// Token: 0x06001CD5 RID: 7381 RVA: 0x000039E8 File Offset: 0x00001BE8
		public ManagementEnumeratorAttribute()
		{
		}

		/// <summary>Gets or sets a value that defines the type of output that the method that is marked with the ManagementEnumerator attribute will output.</summary>
		/// <returns>A <see cref="T:System.Type" /> value that indicates the type of output that the method marked with the <see cref="ManagementEnumerator" /> attribute will output.</returns>
		// Token: 0x170005A5 RID: 1445
		// (get) Token: 0x06001CD6 RID: 7382 RVA: 0x0004B26D File Offset: 0x0004946D
		// (set) Token: 0x06001CD7 RID: 7383 RVA: 0x0000220F File Offset: 0x0000040F
		public Type Schema
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
