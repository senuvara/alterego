﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Management.Instrumentation
{
	/// <summary>The ManagementConfiguration attribute indicates that a property or field represents a read-write WMI property.Note: the WMI .NET libraries are now considered in final state, and no further development, enhancements, or updates will be available for non-security related issues affecting these libraries. The MI APIs should be used for all new development.</summary>
	// Token: 0x020004A1 RID: 1185
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class ManagementConfigurationAttribute : ManagementMemberAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Management.ManagementConfigurationAttribute" /> class. This is the default constructor.</summary>
		// Token: 0x06001CC8 RID: 7368 RVA: 0x000039E8 File Offset: 0x00001BE8
		public ManagementConfigurationAttribute()
		{
		}

		/// <summary>Gets or sets the mode of the property, which specifies whether changes to it are applied as soon as possible or when a commit method is called.</summary>
		/// <returns>Returns a <see cref="T:System.Management.Instrumentation.ManagementConfigurationType" /> that indicates whether the WMI property uses <see cref="F:System.Management.Instrumentation.ManagementConfigurationType.Apply" /> or <see cref="F:System.Management.Instrumentation.ManagementConfigurationType.OnCommit" /> mode.</returns>
		// Token: 0x170005A0 RID: 1440
		// (get) Token: 0x06001CC9 RID: 7369 RVA: 0x00053B8C File Offset: 0x00051D8C
		// (set) Token: 0x06001CCA RID: 7370 RVA: 0x0000220F File Offset: 0x0000040F
		public ManagementConfigurationType Mode
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ManagementConfigurationType.Apply;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that defines the type of output that the property that is marked with the ManagementConfiguration attribute will return.</summary>
		/// <returns>A <see cref="T:System.Type" /> value representing the type of output that the property marked with the ManagementConfiguration attribute will return.</returns>
		// Token: 0x170005A1 RID: 1441
		// (get) Token: 0x06001CCB RID: 7371 RVA: 0x0004B26D File Offset: 0x0004946D
		// (set) Token: 0x06001CCC RID: 7372 RVA: 0x0000220F File Offset: 0x0000040F
		public Type Schema
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
