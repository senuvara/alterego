﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Management.Instrumentation
{
	/// <summary>The WmiConfiguration attribute indicates that an assembly contains code that implements a WMI provider by using the WMI.NET Provider Extensions model. The attribute accepts parameters that establish the high-level configuration of the implemented WMI provider. Note: the WMI .NET libraries are now considered in final state, and no further development, enhancements, or updates will be available for non-security related issues affecting these libraries. The MI APIs should be used for all new development.</summary>
	// Token: 0x020004AD RID: 1197
	[AttributeUsage(AttributeTargets.Assembly)]
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class WmiConfigurationAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Management.WmiConfigurationAttribute" /> class that specifies the WMI namespace in which the WMI provider will expose classes.</summary>
		/// <param name="scope">The WMI namespace in which the provider will expose classes. For example, "root\MyProviderNamespace".</param>
		// Token: 0x06001CE7 RID: 7399 RVA: 0x000039E8 File Offset: 0x00001BE8
		public WmiConfigurationAttribute(string scope)
		{
		}

		/// <summary>Gets or sets the hosting group for the WMI provider.</summary>
		/// <returns>A <see cref="T:System.String" /> value that indicates the hosting group for the WMI provider.</returns>
		// Token: 0x170005AB RID: 1451
		// (get) Token: 0x06001CE8 RID: 7400 RVA: 0x0004B26D File Offset: 0x0004946D
		// (set) Token: 0x06001CE9 RID: 7401 RVA: 0x0000220F File Offset: 0x0000040F
		public string HostingGroup
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the hosting model for the WMI provider.</summary>
		/// <returns>A <see cref="T:System.Management.Instrumentation.ManagementHostingModel" /> value that indicates the hosting model of the WMI provider.</returns>
		// Token: 0x170005AC RID: 1452
		// (get) Token: 0x06001CEA RID: 7402 RVA: 0x00053BE0 File Offset: 0x00051DE0
		// (set) Token: 0x06001CEB RID: 7403 RVA: 0x0000220F File Offset: 0x0000040F
		public ManagementHostingModel HostingModel
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ManagementHostingModel.Decoupled;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that specifies whether the WMI provider can impersonate its callers. If the value is false, the provider cannot impersonate, and if the value is true, the provider can impersonate.</summary>
		/// <returns>A Boolean value that indicates whether a provider can or cannot impersonate its callers. If the value is false, the provider cannot impersonate, and if the value is true, the provider can impersonate.</returns>
		// Token: 0x170005AD RID: 1453
		// (get) Token: 0x06001CEC RID: 7404 RVA: 0x00053BFC File Offset: 0x00051DFC
		// (set) Token: 0x06001CED RID: 7405 RVA: 0x0000220F File Offset: 0x0000040F
		public bool IdentifyLevel
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a Security Descriptor Definition Language (SDDL) string that specifies the security descriptor on the namespace in which the provider exposes management objects.</summary>
		/// <returns>An SDDL string that represents the security descriptor on the namespace in which the provider exposes management objects.</returns>
		// Token: 0x170005AE RID: 1454
		// (get) Token: 0x06001CEE RID: 7406 RVA: 0x0004B26D File Offset: 0x0004946D
		// (set) Token: 0x06001CEF RID: 7407 RVA: 0x0000220F File Offset: 0x0000040F
		public string NamespaceSecurity
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the WMI namespace in which the WMI provider exposes classes.</summary>
		/// <returns>A <see cref="T:System.String" /> value that indicates the namespace in which the WMI provider exposes classes.</returns>
		// Token: 0x170005AF RID: 1455
		// (get) Token: 0x06001CF0 RID: 7408 RVA: 0x0004B26D File Offset: 0x0004946D
		public string Scope
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets a security descriptor for the WMI provider. For more information, see the SecurityDescriptor property information in the "__Win32Provider" topic in the MSDN online library at http://www.msdn.com. </summary>
		/// <returns>A <see cref="T:System.String" /> value that contains the security descriptor for the WMI provider.</returns>
		// Token: 0x170005B0 RID: 1456
		// (get) Token: 0x06001CF1 RID: 7409 RVA: 0x0004B26D File Offset: 0x0004946D
		// (set) Token: 0x06001CF2 RID: 7410 RVA: 0x0000220F File Offset: 0x0000040F
		public string SecurityRestriction
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
