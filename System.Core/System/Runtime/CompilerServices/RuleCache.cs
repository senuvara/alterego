﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Dynamic.Utils;

namespace System.Runtime.CompilerServices
{
	/// <summary>Represents a cache of runtime binding rules.</summary>
	/// <typeparam name="T">The delegate type.</typeparam>
	// Token: 0x0200042F RID: 1071
	[EditorBrowsable(EditorBrowsableState.Never)]
	[DebuggerStepThrough]
	public class RuleCache<T> where T : class
	{
		// Token: 0x060019EA RID: 6634 RVA: 0x0004C8C8 File Offset: 0x0004AAC8
		internal RuleCache()
		{
		}

		// Token: 0x060019EB RID: 6635 RVA: 0x0004C8E6 File Offset: 0x0004AAE6
		internal T[] GetRules()
		{
			return this._rules;
		}

		// Token: 0x060019EC RID: 6636 RVA: 0x0004C8F0 File Offset: 0x0004AAF0
		internal void MoveRule(T rule, int i)
		{
			object cacheLock = this._cacheLock;
			lock (cacheLock)
			{
				int num = this._rules.Length - i;
				if (num > 8)
				{
					num = 8;
				}
				int num2 = -1;
				int num3 = Math.Min(this._rules.Length, i + num);
				for (int j = i; j < num3; j++)
				{
					if (this._rules[j] == rule)
					{
						num2 = j;
						break;
					}
				}
				if (num2 >= 2)
				{
					T t = this._rules[num2];
					this._rules[num2] = this._rules[num2 - 1];
					this._rules[num2 - 1] = this._rules[num2 - 2];
					this._rules[num2 - 2] = t;
				}
			}
		}

		// Token: 0x060019ED RID: 6637 RVA: 0x0004C9DC File Offset: 0x0004ABDC
		internal void AddRule(T newRule)
		{
			object cacheLock = this._cacheLock;
			lock (cacheLock)
			{
				this._rules = RuleCache<T>.AddOrInsert(this._rules, newRule);
			}
		}

		// Token: 0x060019EE RID: 6638 RVA: 0x0004CA28 File Offset: 0x0004AC28
		internal void ReplaceRule(T oldRule, T newRule)
		{
			object cacheLock = this._cacheLock;
			lock (cacheLock)
			{
				int num = Array.IndexOf<T>(this._rules, oldRule);
				if (num >= 0)
				{
					this._rules[num] = newRule;
				}
				else
				{
					this._rules = RuleCache<T>.AddOrInsert(this._rules, newRule);
				}
			}
		}

		// Token: 0x060019EF RID: 6639 RVA: 0x0004CA94 File Offset: 0x0004AC94
		private static T[] AddOrInsert(T[] rules, T item)
		{
			if (rules.Length < 64)
			{
				return rules.AddLast(item);
			}
			int num = rules.Length + 1;
			T[] array;
			if (num > 128)
			{
				num = 128;
				array = rules;
			}
			else
			{
				array = new T[num];
			}
			Array.Copy(rules, 0, array, 0, 64);
			array[64] = item;
			Array.Copy(rules, 64, array, 65, num - 64 - 1);
			return array;
		}

		// Token: 0x04000C2F RID: 3119
		private T[] _rules = Array.Empty<T>();

		// Token: 0x04000C30 RID: 3120
		private readonly object _cacheLock = new object();

		// Token: 0x04000C31 RID: 3121
		private const int MaxRules = 128;

		// Token: 0x04000C32 RID: 3122
		private const int InsertPosition = 64;
	}
}
