﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Dynamic;
using System.Linq.Expressions;
using System.Linq.Expressions.Compiler;
using Unity;

namespace System.Runtime.CompilerServices
{
	/// <summary>Contains helper methods called from dynamically generated methods.</summary>
	// Token: 0x0200041E RID: 1054
	[DebuggerStepThrough]
	[EditorBrowsable(EditorBrowsableState.Never)]
	public static class RuntimeOps
	{
		/// <summary>Gets the value of an item in an expando object.</summary>
		/// <param name="expando">The expando object.</param>
		/// <param name="indexClass">The class of the expando object.</param>
		/// <param name="index">The index of the member.</param>
		/// <param name="name">The name of the member.</param>
		/// <param name="ignoreCase">true if the name should be matched ignoring case; false otherwise.</param>
		/// <param name="value">The out parameter containing the value of the member.</param>
		/// <returns>True if the member exists in the expando object, otherwise false.</returns>
		// Token: 0x0600196E RID: 6510 RVA: 0x0004B216 File Offset: 0x00049416
		[Obsolete("do not use this method", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static bool ExpandoTryGetValue(ExpandoObject expando, object indexClass, int index, string name, bool ignoreCase, out object value)
		{
			return expando.TryGetValue(indexClass, index, name, ignoreCase, out value);
		}

		/// <summary>Sets the value of an item in an expando object.</summary>
		/// <param name="expando">The expando object.</param>
		/// <param name="indexClass">The class of the expando object.</param>
		/// <param name="index">The index of the member.</param>
		/// <param name="value">The value of the member.</param>
		/// <param name="name">The name of the member.</param>
		/// <param name="ignoreCase">true if the name should be matched ignoring case; false otherwise.</param>
		/// <returns>Returns the index for the set member.</returns>
		// Token: 0x0600196F RID: 6511 RVA: 0x0004B225 File Offset: 0x00049425
		[Obsolete("do not use this method", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static object ExpandoTrySetValue(ExpandoObject expando, object indexClass, int index, object value, string name, bool ignoreCase)
		{
			expando.TrySetValue(indexClass, index, value, name, ignoreCase, false);
			return value;
		}

		/// <summary>Deletes the value of an item in an expando object.</summary>
		/// <param name="expando">The expando object.</param>
		/// <param name="indexClass">The class of the expando object.</param>
		/// <param name="index">The index of the member.</param>
		/// <param name="name">The name of the member.</param>
		/// <param name="ignoreCase">true if the name should be matched ignoring case; false otherwise.</param>
		/// <returns>true if the item was successfully removed; otherwise, false.</returns>
		// Token: 0x06001970 RID: 6512 RVA: 0x0004B236 File Offset: 0x00049436
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("do not use this method", true)]
		public static bool ExpandoTryDeleteValue(ExpandoObject expando, object indexClass, int index, string name, bool ignoreCase)
		{
			return expando.TryDeleteValue(indexClass, index, name, ignoreCase, ExpandoObject.Uninitialized);
		}

		/// <summary>Checks the version of the Expando object.</summary>
		/// <param name="expando">The Expando object.</param>
		/// <param name="version">The version to check.</param>
		/// <returns>Returns true if the version is equal; otherwise, false.</returns>
		// Token: 0x06001971 RID: 6513 RVA: 0x0004B248 File Offset: 0x00049448
		[Obsolete("do not use this method", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static bool ExpandoCheckVersion(ExpandoObject expando, object version)
		{
			return expando.Class == version;
		}

		/// <summary>Promotes an Expando object from one class to a new class.</summary>
		/// <param name="expando">The Expando object.</param>
		/// <param name="oldClass">The old class of the Expando object.</param>
		/// <param name="newClass">The new class of the Expando object.</param>
		// Token: 0x06001972 RID: 6514 RVA: 0x0004B253 File Offset: 0x00049453
		[Obsolete("do not use this method", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void ExpandoPromoteClass(ExpandoObject expando, object oldClass, object newClass)
		{
			expando.PromoteClass(oldClass, newClass);
		}

		/// <summary>Creates an interface that can be used to modify closed over variables at runtime.</summary>
		/// <param name="data">The closure array.</param>
		/// <param name="indexes">An array of indicies into the closure array where variables are found.</param>
		/// <returns>An interface to access variables.</returns>
		// Token: 0x06001973 RID: 6515 RVA: 0x0004B25D File Offset: 0x0004945D
		[Obsolete("do not use this method", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static IRuntimeVariables CreateRuntimeVariables(object[] data, long[] indexes)
		{
			return new RuntimeOps.RuntimeVariableList(data, indexes);
		}

		/// <summary>Creates an interface that can be used to modify closed over variables at runtime.</summary>
		/// <returns>An interface to access variables.</returns>
		// Token: 0x06001974 RID: 6516 RVA: 0x0004B266 File Offset: 0x00049466
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("do not use this method", true)]
		public static IRuntimeVariables CreateRuntimeVariables()
		{
			return new RuntimeOps.EmptyRuntimeVariables();
		}

		/// <summary>Combines two runtime variable lists and returns a new list.</summary>
		/// <param name="first">The first list.</param>
		/// <param name="second">The second list.</param>
		/// <param name="indexes">The index array indicating which list to get variables from.</param>
		/// <returns>The merged runtime variables.</returns>
		// Token: 0x06001975 RID: 6517 RVA: 0x0004B26D File Offset: 0x0004946D
		[Obsolete("do not use this method", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static IRuntimeVariables MergeRuntimeVariables(IRuntimeVariables first, IRuntimeVariables second, int[] indexes)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Quotes the provided expression tree.</summary>
		/// <param name="expression">The expression to quote.</param>
		/// <param name="hoistedLocals">The hoisted local state provided by the compiler.</param>
		/// <param name="locals">The actual hoisted local values.</param>
		/// <returns>The quoted expression.</returns>
		// Token: 0x06001976 RID: 6518 RVA: 0x0004B26D File Offset: 0x0004946D
		[Obsolete("do not use this method", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static Expression Quote(Expression expression, object hoistedLocals, object[] locals)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x0200041F RID: 1055
		internal sealed class MergedRuntimeVariables : IRuntimeVariables
		{
			// Token: 0x06001977 RID: 6519 RVA: 0x0004B275 File Offset: 0x00049475
			internal MergedRuntimeVariables(IRuntimeVariables first, IRuntimeVariables second, int[] indexes)
			{
				this._first = first;
				this._second = second;
				this._indexes = indexes;
			}

			// Token: 0x170004EA RID: 1258
			// (get) Token: 0x06001978 RID: 6520 RVA: 0x0004B292 File Offset: 0x00049492
			public int Count
			{
				get
				{
					return this._indexes.Length;
				}
			}

			// Token: 0x170004EB RID: 1259
			public object this[int index]
			{
				get
				{
					index = this._indexes[index];
					if (index < 0)
					{
						return this._second[-1 - index];
					}
					return this._first[index];
				}
				set
				{
					index = this._indexes[index];
					if (index >= 0)
					{
						this._first[index] = value;
						return;
					}
					this._second[-1 - index] = value;
				}
			}

			// Token: 0x04000C0C RID: 3084
			private readonly IRuntimeVariables _first;

			// Token: 0x04000C0D RID: 3085
			private readonly IRuntimeVariables _second;

			// Token: 0x04000C0E RID: 3086
			private readonly int[] _indexes;
		}

		// Token: 0x02000420 RID: 1056
		private sealed class EmptyRuntimeVariables : IRuntimeVariables
		{
			// Token: 0x170004EC RID: 1260
			// (get) Token: 0x0600197B RID: 6523 RVA: 0x00002285 File Offset: 0x00000485
			int IRuntimeVariables.Count
			{
				get
				{
					return 0;
				}
			}

			// Token: 0x170004ED RID: 1261
			object IRuntimeVariables.this[int index]
			{
				get
				{
					throw new IndexOutOfRangeException();
				}
				set
				{
					throw new IndexOutOfRangeException();
				}
			}

			// Token: 0x0600197E RID: 6526 RVA: 0x00002310 File Offset: 0x00000510
			public EmptyRuntimeVariables()
			{
			}
		}

		// Token: 0x02000421 RID: 1057
		private sealed class RuntimeVariableList : IRuntimeVariables
		{
			// Token: 0x0600197F RID: 6527 RVA: 0x0004B2FB File Offset: 0x000494FB
			internal RuntimeVariableList(object[] data, long[] indexes)
			{
				this._data = data;
				this._indexes = indexes;
			}

			// Token: 0x170004EE RID: 1262
			// (get) Token: 0x06001980 RID: 6528 RVA: 0x0004B311 File Offset: 0x00049511
			public int Count
			{
				get
				{
					return this._indexes.Length;
				}
			}

			// Token: 0x170004EF RID: 1263
			public object this[int index]
			{
				get
				{
					return this.GetStrongBox(index).Value;
				}
				set
				{
					this.GetStrongBox(index).Value = value;
				}
			}

			// Token: 0x06001983 RID: 6531 RVA: 0x0004B338 File Offset: 0x00049538
			private IStrongBox GetStrongBox(int index)
			{
				long num = this._indexes[index];
				object[] array = this._data;
				for (int i = (int)(num >> 32); i > 0; i--)
				{
					array = HoistedLocals.GetParent(array);
				}
				return (IStrongBox)array[(int)num];
			}

			// Token: 0x04000C0F RID: 3087
			private readonly object[] _data;

			// Token: 0x04000C10 RID: 3088
			private readonly long[] _indexes;
		}

		// Token: 0x02000422 RID: 1058
		internal sealed class RuntimeVariables : IRuntimeVariables
		{
			// Token: 0x06001984 RID: 6532 RVA: 0x0004B375 File Offset: 0x00049575
			internal RuntimeVariables(IStrongBox[] boxes)
			{
				this._boxes = boxes;
			}

			// Token: 0x170004F0 RID: 1264
			// (get) Token: 0x06001985 RID: 6533 RVA: 0x0004B384 File Offset: 0x00049584
			int IRuntimeVariables.Count
			{
				get
				{
					return this._boxes.Length;
				}
			}

			// Token: 0x170004F1 RID: 1265
			object IRuntimeVariables.this[int index]
			{
				get
				{
					return this._boxes[index].Value;
				}
				set
				{
					this._boxes[index].Value = value;
				}
			}

			// Token: 0x04000C11 RID: 3089
			private readonly IStrongBox[] _boxes;
		}
	}
}
