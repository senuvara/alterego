﻿using System;
using System.ComponentModel;
using System.Diagnostics;

namespace System.Runtime.CompilerServices
{
	/// <summary>Represents the runtime state of a dynamically generated method.</summary>
	// Token: 0x0200042A RID: 1066
	[EditorBrowsable(EditorBrowsableState.Never)]
	[DebuggerStepThrough]
	public sealed class Closure
	{
		/// <summary>Creates an object to hold state of a dynamically generated method.</summary>
		/// <param name="constants">The constant values that are used by the method.</param>
		/// <param name="locals">The hoisted local variables from the parent context.</param>
		// Token: 0x060019B7 RID: 6583 RVA: 0x0004C09B File Offset: 0x0004A29B
		public Closure(object[] constants, object[] locals)
		{
			this.Constants = constants;
			this.Locals = locals;
		}

		/// <summary>Represents the non-trivial constants and locally executable expressions that are referenced by a dynamically generated method.</summary>
		// Token: 0x04000C25 RID: 3109
		public readonly object[] Constants;

		/// <summary>Represents the hoisted local variables from the parent context.</summary>
		// Token: 0x04000C26 RID: 3110
		public readonly object[] Locals;
	}
}
