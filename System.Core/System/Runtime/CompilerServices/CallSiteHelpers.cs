﻿using System;
using System.Dynamic;
using System.Reflection;

namespace System.Runtime.CompilerServices
{
	/// <summary>Class that contains helper methods for DLR CallSites.</summary>
	// Token: 0x02000428 RID: 1064
	public static class CallSiteHelpers
	{
		/// <summary>Checks if a <see cref="T:System.Reflection.MethodBase" /> is internally used by DLR and should not be displayed on the language code's stack.</summary>
		/// <param name="mb">The input <see cref="T:System.Reflection.MethodBase" /></param>
		/// <returns>True if the input <see cref="T:System.Reflection.MethodBase" /> is internally used by DLR and should not be displayed on the language code's stack. Otherwise, false.</returns>
		// Token: 0x060019AA RID: 6570 RVA: 0x0004BFB4 File Offset: 0x0004A1B4
		public static bool IsInternalFrame(MethodBase mb)
		{
			return (mb.Name == "CallSite.Target" && mb.GetType() != CallSiteHelpers.s_knownNonDynamicMethodType) || mb.DeclaringType == typeof(UpdateDelegates);
		}

		// Token: 0x060019AB RID: 6571 RVA: 0x0004C001 File Offset: 0x0004A201
		// Note: this type is marked as 'beforefieldinit'.
		static CallSiteHelpers()
		{
		}

		// Token: 0x04000C24 RID: 3108
		private static readonly Type s_knownNonDynamicMethodType = typeof(object).GetMethod("ToString").GetType();
	}
}
