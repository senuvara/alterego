﻿using System;
using System.Collections.ObjectModel;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000430 RID: 1072
	internal sealed class TrueReadOnlyCollection<T> : ReadOnlyCollection<T>
	{
		// Token: 0x060019F0 RID: 6640 RVA: 0x0004CAF5 File Offset: 0x0004ACF5
		public TrueReadOnlyCollection(params T[] list) : base(list)
		{
		}
	}
}
