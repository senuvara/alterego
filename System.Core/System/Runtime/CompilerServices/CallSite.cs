﻿using System;
using System.Dynamic.Utils;
using System.Linq.Expressions;
using System.Reflection;
using Unity;

namespace System.Runtime.CompilerServices
{
	/// <summary>A dynamic call site base class. This type is used as a parameter type to the dynamic site targets.</summary>
	// Token: 0x02000423 RID: 1059
	public class CallSite
	{
		// Token: 0x06001988 RID: 6536 RVA: 0x0004B3AD File Offset: 0x000495AD
		internal CallSite(CallSiteBinder binder)
		{
			this._binder = binder;
		}

		/// <summary>Class responsible for binding dynamic operations on the dynamic site.</summary>
		/// <returns>The <see cref="T:System.Runtime.CompilerServices.CallSiteBinder" /> object responsible for binding dynamic operations.</returns>
		// Token: 0x170004F2 RID: 1266
		// (get) Token: 0x06001989 RID: 6537 RVA: 0x0004B3BC File Offset: 0x000495BC
		public CallSiteBinder Binder
		{
			get
			{
				return this._binder;
			}
		}

		/// <summary>Creates a call site with the given delegate type and binder.</summary>
		/// <param name="delegateType">The call site delegate type.</param>
		/// <param name="binder">The call site binder.</param>
		/// <returns>The new call site.</returns>
		// Token: 0x0600198A RID: 6538 RVA: 0x0004B3C4 File Offset: 0x000495C4
		public static CallSite Create(Type delegateType, CallSiteBinder binder)
		{
			ContractUtils.RequiresNotNull(delegateType, "delegateType");
			ContractUtils.RequiresNotNull(binder, "binder");
			if (!delegateType.IsSubclassOf(typeof(MulticastDelegate)))
			{
				throw Error.TypeMustBeDerivedFromSystemDelegate();
			}
			CacheDict<Type, Func<CallSiteBinder, CallSite>> cacheDict = CallSite.s_siteCtors;
			if (cacheDict == null)
			{
				cacheDict = (CallSite.s_siteCtors = new CacheDict<Type, Func<CallSiteBinder, CallSite>>(100));
			}
			MethodInfo methodInfo = null;
			Func<CallSiteBinder, CallSite> func;
			if (!cacheDict.TryGetValue(delegateType, out func))
			{
				methodInfo = typeof(CallSite<>).MakeGenericType(new Type[]
				{
					delegateType
				}).GetMethod("Create");
				if (delegateType.CanCache())
				{
					func = (Func<CallSiteBinder, CallSite>)methodInfo.CreateDelegate(typeof(Func<CallSiteBinder, CallSite>));
					cacheDict.Add(delegateType, func);
				}
			}
			if (func != null)
			{
				return func(binder);
			}
			return (CallSite)methodInfo.Invoke(null, new object[]
			{
				binder
			});
		}

		// Token: 0x0600198B RID: 6539 RVA: 0x0000220F File Offset: 0x0000040F
		internal CallSite()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000C12 RID: 3090
		internal const string CallSiteTargetMethodName = "CallSite.Target";

		// Token: 0x04000C13 RID: 3091
		private static volatile CacheDict<Type, Func<CallSiteBinder, CallSite>> s_siteCtors;

		// Token: 0x04000C14 RID: 3092
		internal readonly CallSiteBinder _binder;

		// Token: 0x04000C15 RID: 3093
		internal bool _match;
	}
}
