﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Defines a property for accessing the value that an object references.</summary>
	// Token: 0x02000433 RID: 1075
	public interface IStrongBox
	{
		/// <summary>Gets or sets the value that an object references.</summary>
		/// <returns>The value that the object references.</returns>
		// Token: 0x17000504 RID: 1284
		// (get) Token: 0x060019F6 RID: 6646
		// (set) Token: 0x060019F7 RID: 6647
		object Value { get; set; }
	}
}
