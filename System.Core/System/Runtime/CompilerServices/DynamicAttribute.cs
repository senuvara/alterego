﻿using System;
using System.Collections.Generic;

namespace System.Runtime.CompilerServices
{
	/// <summary>Indicates that the use of <see cref="T:System.Object" /> on a member is meant to be treated as a dynamically dispatched type.</summary>
	// Token: 0x02000434 RID: 1076
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.ReturnValue)]
	public sealed class DynamicAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.DynamicAttribute" /> class.</summary>
		// Token: 0x060019F8 RID: 6648 RVA: 0x0004CB28 File Offset: 0x0004AD28
		public DynamicAttribute()
		{
			this._transformFlags = new bool[]
			{
				true
			};
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.DynamicAttribute" /> class.</summary>
		/// <param name="transformFlags">Specifies, in a prefix traversal of a type's construction, which <see cref="T:System.Object" /> occurrences are meant to be treated as a dynamically dispatched type.</param>
		// Token: 0x060019F9 RID: 6649 RVA: 0x0004CB40 File Offset: 0x0004AD40
		public DynamicAttribute(bool[] transformFlags)
		{
			if (transformFlags == null)
			{
				throw new ArgumentNullException("transformFlags");
			}
			this._transformFlags = transformFlags;
		}

		/// <summary>Specifies, in a prefix traversal of a type's construction, which <see cref="T:System.Object" /> occurrences are meant to be treated as a dynamically dispatched type.</summary>
		/// <returns>The list of <see cref="T:System.Object" /> occurrences that are meant to be treated as a dynamically dispatched type.</returns>
		// Token: 0x17000505 RID: 1285
		// (get) Token: 0x060019FA RID: 6650 RVA: 0x0004CB5D File Offset: 0x0004AD5D
		public IList<bool> TransformFlags
		{
			get
			{
				return Array.AsReadOnly<bool>(this._transformFlags);
			}
		}

		// Token: 0x04000C34 RID: 3124
		private readonly bool[] _transformFlags;
	}
}
