﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000431 RID: 1073
	internal class FriendAccessAllowedAttribute : Attribute
	{
		// Token: 0x060019F1 RID: 6641 RVA: 0x000022B6 File Offset: 0x000004B6
		public FriendAccessAllowedAttribute()
		{
		}
	}
}
