﻿using System;
using System.ComponentModel;
using System.Diagnostics;

namespace System.Runtime.CompilerServices
{
	/// <summary>Creates and caches binding rules.</summary>
	// Token: 0x02000429 RID: 1065
	[EditorBrowsable(EditorBrowsableState.Never)]
	[DebuggerStepThrough]
	public static class CallSiteOps
	{
		/// <summary>Creates an instance of a dynamic call site used for cache lookup.</summary>
		/// <param name="site">An instance of the dynamic call site.</param>
		/// <typeparam name="T">The type of the delegate of the <see cref="T:System.Runtime.CompilerServices.CallSite" />.</typeparam>
		/// <returns>The new call site.</returns>
		// Token: 0x060019AC RID: 6572 RVA: 0x0004C021 File Offset: 0x0004A221
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("do not use this method", true)]
		public static CallSite<T> CreateMatchmaker<T>(CallSite<T> site) where T : class
		{
			CallSite<T> callSite = site.CreateMatchMaker();
			callSite._match = true;
			return callSite;
		}

		/// <summary>Checks if a dynamic site requires an update.</summary>
		/// <param name="site">An instance of the dynamic call site.</param>
		/// <returns>true if rule does not need updating, false otherwise.</returns>
		// Token: 0x060019AD RID: 6573 RVA: 0x0004C030 File Offset: 0x0004A230
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("do not use this method", true)]
		public static bool SetNotMatched(CallSite site)
		{
			bool match = site._match;
			site._match = false;
			return match;
		}

		/// <summary>Checks whether the executed rule matched</summary>
		/// <param name="site">An instance of the dynamic call site.</param>
		/// <returns>true if rule matched, false otherwise.</returns>
		// Token: 0x060019AE RID: 6574 RVA: 0x0004C03F File Offset: 0x0004A23F
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("do not use this method", true)]
		public static bool GetMatch(CallSite site)
		{
			return site._match;
		}

		/// <summary>Clears the match flag on the matchmaker call site.</summary>
		/// <param name="site">An instance of the dynamic call site.</param>
		// Token: 0x060019AF RID: 6575 RVA: 0x0004C047 File Offset: 0x0004A247
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("do not use this method", true)]
		public static void ClearMatch(CallSite site)
		{
			site._match = true;
		}

		/// <summary>Adds a rule to the cache maintained on the dynamic call site.</summary>
		/// <param name="site">An instance of the dynamic call site.</param>
		/// <param name="rule">An instance of the call site rule.</param>
		/// <typeparam name="T">The type of the delegate of the <see cref="T:System.Runtime.CompilerServices.CallSite" />.</typeparam>
		// Token: 0x060019B0 RID: 6576 RVA: 0x0004C050 File Offset: 0x0004A250
		[Obsolete("do not use this method", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void AddRule<T>(CallSite<T> site, T rule) where T : class
		{
			site.AddRule(rule);
		}

		/// <summary>Updates rules in the cache.</summary>
		/// <param name="this">An instance of the dynamic call site.</param>
		/// <param name="matched">The matched rule index.</param>
		/// <typeparam name="T">The type of the delegate of the <see cref="T:System.Runtime.CompilerServices.CallSite" />.</typeparam>
		// Token: 0x060019B1 RID: 6577 RVA: 0x0004C059 File Offset: 0x0004A259
		[Obsolete("do not use this method", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void UpdateRules<T>(CallSite<T> @this, int matched) where T : class
		{
			if (matched > 1)
			{
				@this.MoveRule(matched);
			}
		}

		/// <summary>Gets the dynamic binding rules from the call site.</summary>
		/// <param name="site">An instance of the dynamic call site.</param>
		/// <typeparam name="T">The type of the delegate of the <see cref="T:System.Runtime.CompilerServices.CallSite" />.</typeparam>
		/// <returns>An array of dynamic binding rules.</returns>
		// Token: 0x060019B2 RID: 6578 RVA: 0x0004C066 File Offset: 0x0004A266
		[Obsolete("do not use this method", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static T[] GetRules<T>(CallSite<T> site) where T : class
		{
			return site.Rules;
		}

		/// <summary>Retrieves binding rule cache.</summary>
		/// <param name="site">An instance of the dynamic call site.</param>
		/// <typeparam name="T">The type of the delegate of the <see cref="T:System.Runtime.CompilerServices.CallSite" />.</typeparam>
		/// <returns>The cache.</returns>
		// Token: 0x060019B3 RID: 6579 RVA: 0x0004C06E File Offset: 0x0004A26E
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("do not use this method", true)]
		public static RuleCache<T> GetRuleCache<T>(CallSite<T> site) where T : class
		{
			return site.Binder.GetRuleCache<T>();
		}

		/// <summary>Moves the binding rule within the cache.</summary>
		/// <param name="cache">The call site rule cache.</param>
		/// <param name="rule">An instance of the call site rule.</param>
		/// <param name="i">An index of the call site rule.</param>
		/// <typeparam name="T">The type of the delegate of the <see cref="T:System.Runtime.CompilerServices.CallSite" />. </typeparam>
		// Token: 0x060019B4 RID: 6580 RVA: 0x0004C07B File Offset: 0x0004A27B
		[Obsolete("do not use this method", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void MoveRule<T>(RuleCache<T> cache, T rule, int i) where T : class
		{
			if (i > 1)
			{
				cache.MoveRule(rule, i);
			}
		}

		/// <summary>Searches the dynamic rule cache for rules applicable to the dynamic operation.</summary>
		/// <param name="cache">The cache.</param>
		/// <typeparam name="T">The type of the delegate of the <see cref="T:System.Runtime.CompilerServices.CallSite" />. </typeparam>
		/// <returns>The collection of applicable rules.</returns>
		// Token: 0x060019B5 RID: 6581 RVA: 0x0004C089 File Offset: 0x0004A289
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("do not use this method", true)]
		public static T[] GetCachedRules<T>(RuleCache<T> cache) where T : class
		{
			return cache.GetRules();
		}

		/// <summary>Updates the call site target with a new rule based on the arguments.</summary>
		/// <param name="binder">The call site binder.</param>
		/// <param name="site">An instance of the dynamic call site.</param>
		/// <param name="args">Arguments to the call site.</param>
		/// <typeparam name="T">The type of the delegate of the <see cref="T:System.Runtime.CompilerServices.CallSite" />.</typeparam>
		/// <returns>The new call site target.</returns>
		// Token: 0x060019B6 RID: 6582 RVA: 0x0004C091 File Offset: 0x0004A291
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("do not use this method", true)]
		public static T Bind<T>(CallSiteBinder binder, CallSite<T> site, object[] args) where T : class
		{
			return binder.BindCore<T>(site, args);
		}
	}
}
