﻿using System;
using System.Collections.Generic;
using System.Dynamic.Utils;
using System.Linq.Expressions;
using System.Reflection;

namespace System.Runtime.CompilerServices
{
	/// <summary>Dynamic site type.</summary>
	/// <typeparam name="T">The delegate type.</typeparam>
	// Token: 0x02000424 RID: 1060
	public class CallSite<T> : CallSite where T : class
	{
		/// <summary>The update delegate. Called when the dynamic site experiences cache miss.</summary>
		/// <returns>The update delegate.</returns>
		// Token: 0x170004F3 RID: 1267
		// (get) Token: 0x0600198C RID: 6540 RVA: 0x0004B492 File Offset: 0x00049692
		public T Update
		{
			get
			{
				if (this._match)
				{
					return CallSite<T>.s_cachedNoMatch;
				}
				return CallSite<T>.s_cachedUpdate;
			}
		}

		// Token: 0x0600198D RID: 6541 RVA: 0x0004B4A9 File Offset: 0x000496A9
		private CallSite(CallSiteBinder binder) : base(binder)
		{
			this.Target = this.GetUpdateDelegate();
		}

		// Token: 0x0600198E RID: 6542 RVA: 0x0004B4BE File Offset: 0x000496BE
		private CallSite() : base(null)
		{
		}

		// Token: 0x0600198F RID: 6543 RVA: 0x0004B4C7 File Offset: 0x000496C7
		internal CallSite<T> CreateMatchMaker()
		{
			return new CallSite<T>();
		}

		/// <summary>Creates an instance of the dynamic call site, initialized with the binder responsible for the runtime binding of the dynamic operations at this call site.</summary>
		/// <param name="binder">The binder responsible for the runtime binding of the dynamic operations at this call site.</param>
		/// <returns>The new instance of dynamic call site.</returns>
		// Token: 0x06001990 RID: 6544 RVA: 0x0004B4CE File Offset: 0x000496CE
		public static CallSite<T> Create(CallSiteBinder binder)
		{
			if (!typeof(T).IsSubclassOf(typeof(MulticastDelegate)))
			{
				throw Error.TypeMustBeDerivedFromSystemDelegate();
			}
			ContractUtils.RequiresNotNull(binder, "binder");
			return new CallSite<T>(binder);
		}

		// Token: 0x06001991 RID: 6545 RVA: 0x0004B502 File Offset: 0x00049702
		private T GetUpdateDelegate()
		{
			return this.GetUpdateDelegate(ref CallSite<T>.s_cachedUpdate);
		}

		// Token: 0x06001992 RID: 6546 RVA: 0x0004B50F File Offset: 0x0004970F
		private T GetUpdateDelegate(ref T addr)
		{
			if (addr == null)
			{
				addr = this.MakeUpdateDelegate();
			}
			return addr;
		}

		// Token: 0x06001993 RID: 6547 RVA: 0x0004B530 File Offset: 0x00049730
		private void ClearRuleCache()
		{
			base.Binder.GetRuleCache<T>();
			Dictionary<Type, object> cache = base.Binder.Cache;
			if (cache != null)
			{
				Dictionary<Type, object> obj = cache;
				lock (obj)
				{
					cache.Clear();
				}
			}
		}

		// Token: 0x06001994 RID: 6548 RVA: 0x0004B588 File Offset: 0x00049788
		internal void AddRule(T newRule)
		{
			T[] rules = this.Rules;
			if (rules == null)
			{
				this.Rules = new T[]
				{
					newRule
				};
				return;
			}
			T[] array;
			if (rules.Length < 9)
			{
				array = new T[rules.Length + 1];
				Array.Copy(rules, 0, array, 1, rules.Length);
			}
			else
			{
				array = new T[10];
				Array.Copy(rules, 0, array, 1, 9);
			}
			array[0] = newRule;
			this.Rules = array;
		}

		// Token: 0x06001995 RID: 6549 RVA: 0x0004B5F8 File Offset: 0x000497F8
		internal void MoveRule(int i)
		{
			if (i > 1)
			{
				T[] rules = this.Rules;
				T t = rules[i];
				rules[i] = rules[i - 1];
				rules[i - 1] = rules[i - 2];
				rules[i - 2] = t;
			}
		}

		// Token: 0x06001996 RID: 6550 RVA: 0x0004B644 File Offset: 0x00049844
		internal T MakeUpdateDelegate()
		{
			MethodInfo invokeMethod = typeof(T).GetInvokeMethod();
			CallSite<T>.s_cachedNoMatch = this.CreateCustomNoMatchDelegate(invokeMethod);
			return this.CreateCustomUpdateDelegate(invokeMethod);
		}

		// Token: 0x06001997 RID: 6551 RVA: 0x0004B678 File Offset: 0x00049878
		private T CreateCustomUpdateDelegate(MethodInfo invoke)
		{
			Type returnType = invoke.GetReturnType();
			bool flag = returnType == typeof(void);
			ArrayBuilder<Expression> builder = new ArrayBuilder<Expression>(13);
			ArrayBuilder<ParameterExpression> builder2 = new ArrayBuilder<ParameterExpression>(8 + (flag ? 0 : 1));
			ParameterExpression[] array = Array.ConvertAll<ParameterInfo, ParameterExpression>(invoke.GetParametersCached(), (ParameterInfo p) => Expression.Parameter(p.ParameterType, p.Name));
			LabelTarget labelTarget = Expression.Label(returnType);
			Type[] typeArguments = new Type[]
			{
				typeof(T)
			};
			ParameterExpression parameterExpression = array[0];
			ParameterExpression[] array2 = array.RemoveFirst<ParameterExpression>();
			ParameterExpression parameterExpression2 = Expression.Variable(typeof(CallSite<T>), "this");
			builder2.UncheckedAdd(parameterExpression2);
			builder.UncheckedAdd(Expression.Assign(parameterExpression2, Expression.Convert(parameterExpression, parameterExpression2.Type)));
			ParameterExpression parameterExpression3 = Expression.Variable(typeof(T[]), "applicable");
			builder2.UncheckedAdd(parameterExpression3);
			ParameterExpression parameterExpression4 = Expression.Variable(typeof(T), "rule");
			builder2.UncheckedAdd(parameterExpression4);
			ParameterExpression parameterExpression5 = Expression.Variable(typeof(T), "originalRule");
			builder2.UncheckedAdd(parameterExpression5);
			Expression expression = Expression.Field(parameterExpression2, "Target");
			builder.UncheckedAdd(Expression.Assign(parameterExpression5, expression));
			ParameterExpression parameterExpression6 = null;
			if (!flag)
			{
				builder2.UncheckedAdd(parameterExpression6 = Expression.Variable(labelTarget.Type, "result"));
			}
			ParameterExpression parameterExpression7 = Expression.Variable(typeof(int), "count");
			builder2.UncheckedAdd(parameterExpression7);
			ParameterExpression parameterExpression8 = Expression.Variable(typeof(int), "index");
			builder2.UncheckedAdd(parameterExpression8);
			builder.UncheckedAdd(Expression.Assign(parameterExpression, Expression.Call(CachedReflectionInfo.CallSiteOps_CreateMatchmaker.MakeGenericMethod(typeArguments), parameterExpression2)));
			Expression test = Expression.Call(CachedReflectionInfo.CallSiteOps_GetMatch, parameterExpression);
			Expression expression2 = Expression.Call(CachedReflectionInfo.CallSiteOps_ClearMatch, parameterExpression);
			Expression expression3 = Expression.Invoke(parameterExpression4, new TrueReadOnlyCollection<Expression>(array));
			Expression arg = Expression.Call(CachedReflectionInfo.CallSiteOps_UpdateRules.MakeGenericMethod(typeArguments), parameterExpression2, parameterExpression8);
			Expression expression4;
			if (flag)
			{
				expression4 = Expression.Block(expression3, Expression.IfThen(test, Expression.Block(arg, Expression.Return(labelTarget))));
			}
			else
			{
				expression4 = Expression.Block(Expression.Assign(parameterExpression6, expression3), Expression.IfThen(test, Expression.Block(arg, Expression.Return(labelTarget, parameterExpression6))));
			}
			Expression expression5 = Expression.Assign(parameterExpression4, Expression.ArrayAccess(parameterExpression3, new TrueReadOnlyCollection<Expression>(new Expression[]
			{
				parameterExpression8
			})));
			Expression arg2 = expression5;
			LabelTarget labelTarget2 = Expression.Label();
			Expression arg3 = Expression.IfThen(Expression.Equal(parameterExpression8, parameterExpression7), Expression.Break(labelTarget2));
			Expression expression6 = Expression.PreIncrementAssign(parameterExpression8);
			builder.UncheckedAdd(Expression.IfThen(Expression.NotEqual(Expression.Assign(parameterExpression3, Expression.Call(CachedReflectionInfo.CallSiteOps_GetRules.MakeGenericMethod(typeArguments), parameterExpression2)), Expression.Constant(null, parameterExpression3.Type)), Expression.Block(Expression.Assign(parameterExpression7, Expression.ArrayLength(parameterExpression3)), Expression.Assign(parameterExpression8, Utils.Constant(0)), Expression.Loop(Expression.Block(arg3, arg2, Expression.IfThen(Expression.NotEqual(Expression.Convert(parameterExpression4, typeof(object)), Expression.Convert(parameterExpression5, typeof(object))), Expression.Block(Expression.Assign(expression, parameterExpression4), expression4, expression2)), expression6), labelTarget2, null))));
			ParameterExpression parameterExpression9 = Expression.Variable(typeof(RuleCache<T>), "cache");
			builder2.UncheckedAdd(parameterExpression9);
			builder.UncheckedAdd(Expression.Assign(parameterExpression9, Expression.Call(CachedReflectionInfo.CallSiteOps_GetRuleCache.MakeGenericMethod(typeArguments), parameterExpression2)));
			builder.UncheckedAdd(Expression.Assign(parameterExpression3, Expression.Call(CachedReflectionInfo.CallSiteOps_GetCachedRules.MakeGenericMethod(typeArguments), parameterExpression9)));
			if (flag)
			{
				expression4 = Expression.Block(expression3, Expression.IfThen(test, Expression.Return(labelTarget)));
			}
			else
			{
				expression4 = Expression.Block(Expression.Assign(parameterExpression6, expression3), Expression.IfThen(test, Expression.Return(labelTarget, parameterExpression6)));
			}
			Expression arg4 = Expression.TryFinally(expression4, Expression.IfThen(test, Expression.Block(Expression.Call(CachedReflectionInfo.CallSiteOps_AddRule.MakeGenericMethod(typeArguments), parameterExpression2, parameterExpression4), Expression.Call(CachedReflectionInfo.CallSiteOps_MoveRule.MakeGenericMethod(typeArguments), parameterExpression9, parameterExpression4, parameterExpression8))));
			arg2 = Expression.Assign(expression, expression5);
			builder.UncheckedAdd(Expression.Assign(parameterExpression8, Utils.Constant(0)));
			builder.UncheckedAdd(Expression.Assign(parameterExpression7, Expression.ArrayLength(parameterExpression3)));
			builder.UncheckedAdd(Expression.Loop(Expression.Block(arg3, arg2, arg4, expression2, expression6), labelTarget2, null));
			builder.UncheckedAdd(Expression.Assign(parameterExpression4, Expression.Constant(null, parameterExpression4.Type)));
			ParameterExpression parameterExpression10 = Expression.Variable(typeof(object[]), "args");
			Expression[] list = Array.ConvertAll<ParameterExpression, Expression>(array2, (ParameterExpression p) => CallSite<T>.Convert(p, typeof(object)));
			builder2.UncheckedAdd(parameterExpression10);
			builder.UncheckedAdd(Expression.Assign(parameterExpression10, Expression.NewArrayInit(typeof(object), new TrueReadOnlyCollection<Expression>(list))));
			Expression arg5 = Expression.Assign(expression, parameterExpression5);
			arg2 = Expression.Assign(expression, Expression.Assign(parameterExpression4, Expression.Call(CachedReflectionInfo.CallSiteOps_Bind.MakeGenericMethod(typeArguments), Expression.Property(parameterExpression2, "Binder"), parameterExpression2, parameterExpression10)));
			arg4 = Expression.TryFinally(expression4, Expression.IfThen(test, Expression.Call(CachedReflectionInfo.CallSiteOps_AddRule.MakeGenericMethod(typeArguments), parameterExpression2, parameterExpression4)));
			builder.UncheckedAdd(Expression.Loop(Expression.Block(arg5, arg2, arg4, expression2), null, null));
			builder.UncheckedAdd(Expression.Default(labelTarget.Type));
			return Expression.Lambda<T>(Expression.Label(labelTarget, Expression.Block(builder2.ToReadOnly<ParameterExpression>(), builder.ToReadOnly<Expression>())), "CallSite.Target", true, new TrueReadOnlyCollection<ParameterExpression>(array)).Compile();
		}

		// Token: 0x06001998 RID: 6552 RVA: 0x0004BC34 File Offset: 0x00049E34
		private T CreateCustomNoMatchDelegate(MethodInfo invoke)
		{
			ParameterExpression[] array = Array.ConvertAll<ParameterInfo, ParameterExpression>(invoke.GetParametersCached(), (ParameterInfo p) => Expression.Parameter(p.ParameterType, p.Name));
			return Expression.Lambda<T>(Expression.Block(Expression.Call(typeof(CallSiteOps).GetMethod("SetNotMatched"), array[0]), Expression.Default(invoke.GetReturnType())), new TrueReadOnlyCollection<ParameterExpression>(array)).Compile();
		}

		// Token: 0x06001999 RID: 6553 RVA: 0x0004BCA8 File Offset: 0x00049EA8
		private static Expression Convert(Expression arg, Type type)
		{
			if (TypeUtils.AreReferenceAssignable(type, arg.Type))
			{
				return arg;
			}
			return Expression.Convert(arg, type);
		}

		/// <summary>The Level 0 cache - a delegate specialized based on the site history.</summary>
		// Token: 0x04000C16 RID: 3094
		public T Target;

		// Token: 0x04000C17 RID: 3095
		internal T[] Rules;

		// Token: 0x04000C18 RID: 3096
		private static T s_cachedUpdate;

		// Token: 0x04000C19 RID: 3097
		private static volatile T s_cachedNoMatch;

		// Token: 0x04000C1A RID: 3098
		private const int MaxRules = 10;

		// Token: 0x02000425 RID: 1061
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x0600199A RID: 6554 RVA: 0x0004BCC1 File Offset: 0x00049EC1
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x0600199B RID: 6555 RVA: 0x00002310 File Offset: 0x00000510
			public <>c()
			{
			}

			// Token: 0x0600199C RID: 6556 RVA: 0x0004BCCD File Offset: 0x00049ECD
			internal ParameterExpression <CreateCustomUpdateDelegate>b__17_0(ParameterInfo p)
			{
				return Expression.Parameter(p.ParameterType, p.Name);
			}

			// Token: 0x0600199D RID: 6557 RVA: 0x0004BCE0 File Offset: 0x00049EE0
			internal Expression <CreateCustomUpdateDelegate>b__17_1(ParameterExpression p)
			{
				return CallSite<T>.Convert(p, typeof(object));
			}

			// Token: 0x0600199E RID: 6558 RVA: 0x0004BCCD File Offset: 0x00049ECD
			internal ParameterExpression <CreateCustomNoMatchDelegate>b__18_0(ParameterInfo p)
			{
				return Expression.Parameter(p.ParameterType, p.Name);
			}

			// Token: 0x04000C1B RID: 3099
			public static readonly CallSite<T>.<>c <>9 = new CallSite<T>.<>c();

			// Token: 0x04000C1C RID: 3100
			public static Converter<ParameterInfo, ParameterExpression> <>9__17_0;

			// Token: 0x04000C1D RID: 3101
			public static Converter<ParameterExpression, Expression> <>9__17_1;

			// Token: 0x04000C1E RID: 3102
			public static Converter<ParameterInfo, ParameterExpression> <>9__18_0;
		}
	}
}
