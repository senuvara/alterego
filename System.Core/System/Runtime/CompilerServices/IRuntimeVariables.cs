﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Represents the values of run-time variables.</summary>
	// Token: 0x0200042C RID: 1068
	public interface IRuntimeVariables
	{
		/// <summary>Gets a count of the run-time variables.</summary>
		/// <returns>The number of run-time variables.</returns>
		// Token: 0x170004F6 RID: 1270
		// (get) Token: 0x060019BB RID: 6587
		int Count { get; }

		/// <summary>Gets the value of the run-time variable at the specified index.</summary>
		/// <param name="index">The zero-based index of the run-time variable whose value is to be returned.</param>
		/// <returns>The value of the run-time variable.</returns>
		// Token: 0x170004F7 RID: 1271
		object this[int index]
		{
			get;
			set;
		}
	}
}
