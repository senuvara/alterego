﻿using System;
using System.Linq.Expressions;

namespace System.Runtime.CompilerServices
{
	/// <summary>Generates debug information for lambda expressions in an expression tree.</summary>
	// Token: 0x0200042B RID: 1067
	public abstract class DebugInfoGenerator
	{
		/// <summary>Creates a program database (PDB) symbol generator.</summary>
		/// <returns>A PDB symbol generator.</returns>
		// Token: 0x060019B8 RID: 6584 RVA: 0x00003A76 File Offset: 0x00001C76
		public static DebugInfoGenerator CreatePdbGenerator()
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Marks a sequence point in Microsoft intermediate language (MSIL) code.</summary>
		/// <param name="method">The lambda expression that is generated.</param>
		/// <param name="ilOffset">The offset within MSIL code at which to mark the sequence point.</param>
		/// <param name="sequencePoint">Debug information that corresponds to the sequence point.</param>
		// Token: 0x060019B9 RID: 6585
		public abstract void MarkSequencePoint(LambdaExpression method, int ilOffset, DebugInfoExpression sequencePoint);

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.DebugInfoGenerator" /> class.</summary>
		// Token: 0x060019BA RID: 6586 RVA: 0x00002310 File Offset: 0x00000510
		protected DebugInfoGenerator()
		{
		}
	}
}
