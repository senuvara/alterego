﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Holds a reference to a value.</summary>
	/// <typeparam name="T">The type of the value that the <see cref="T:System.Runtime.CompilerServices.StrongBox`1" /> references.</typeparam>
	// Token: 0x02000432 RID: 1074
	public class StrongBox<T> : IStrongBox
	{
		/// <summary>Initializes a new StrongBox which can receive a value when used in a reference call.</summary>
		// Token: 0x060019F2 RID: 6642 RVA: 0x00002310 File Offset: 0x00000510
		public StrongBox()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.StrongBox`1" /> class by using the supplied value. </summary>
		/// <param name="value">A value that the <see cref="T:System.Runtime.CompilerServices.StrongBox`1" /> will reference.</param>
		// Token: 0x060019F3 RID: 6643 RVA: 0x0004CAFE File Offset: 0x0004ACFE
		public StrongBox(T value)
		{
			this.Value = value;
		}

		/// <summary>Gets or sets the value that the <see cref="T:System.Runtime.CompilerServices.StrongBox`1" /> references.</summary>
		/// <returns>The value that the <see cref="T:System.Runtime.CompilerServices.StrongBox`1" /> references.</returns>
		// Token: 0x17000503 RID: 1283
		// (get) Token: 0x060019F4 RID: 6644 RVA: 0x0004CB0D File Offset: 0x0004AD0D
		// (set) Token: 0x060019F5 RID: 6645 RVA: 0x0004CB1A File Offset: 0x0004AD1A
		object IStrongBox.Value
		{
			get
			{
				return this.Value;
			}
			set
			{
				this.Value = (T)((object)value);
			}
		}

		/// <summary>Represents the value that the <see cref="T:System.Runtime.CompilerServices.StrongBox`1" /> references.</summary>
		// Token: 0x04000C33 RID: 3123
		public T Value;
	}
}
