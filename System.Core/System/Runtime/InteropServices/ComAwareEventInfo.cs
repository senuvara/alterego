﻿using System;
using System.Reflection;

namespace System.Runtime.InteropServices
{
	/// <summary>Permits late-bound registration of an event handler.</summary>
	// Token: 0x0200041D RID: 1053
	public class ComAwareEventInfo : EventInfo
	{
		/// <summary>Gets the attributes for this event.</summary>
		/// <returns>The read-only attributes for this event.</returns>
		// Token: 0x170004E6 RID: 1254
		// (get) Token: 0x06001961 RID: 6497 RVA: 0x0000227E File Offset: 0x0000047E
		[MonoTODO]
		public override EventAttributes Attributes
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the class that declares this member.</summary>
		/// <returns>The <see cref="T:System.Type" /> object for the class that declares this member.</returns>
		// Token: 0x170004E7 RID: 1255
		// (get) Token: 0x06001962 RID: 6498 RVA: 0x0000227E File Offset: 0x0000047E
		[MonoTODO]
		public override Type DeclaringType
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the name of the current member.</summary>
		/// <returns>The name of this member.</returns>
		// Token: 0x170004E8 RID: 1256
		// (get) Token: 0x06001963 RID: 6499 RVA: 0x0000227E File Offset: 0x0000047E
		[MonoTODO]
		public override string Name
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.ComAwareEventInfo" /> class by using the specified type and a name of the event on the type.</summary>
		/// <param name="type">The type of object.</param>
		/// <param name="eventName">The name of an event on <paramref name="type" />.</param>
		// Token: 0x06001964 RID: 6500 RVA: 0x0004B209 File Offset: 0x00049409
		[MonoTODO]
		public ComAwareEventInfo(Type type, string eventName)
		{
			throw new NotImplementedException();
		}

		/// <summary>Attaches an event handler to a COM object.</summary>
		/// <param name="target">The target object that the event delegate should bind to.</param>
		/// <param name="handler">The event delegate.</param>
		// Token: 0x06001965 RID: 6501 RVA: 0x0000227E File Offset: 0x0000047E
		[MonoTODO]
		public override void AddEventHandler(object target, Delegate handler)
		{
			throw new NotImplementedException();
		}

		/// <summary>Detaches an event handler from a COM object.</summary>
		/// <param name="target">The target object that the event delegate is bound to.</param>
		/// <param name="handler">The event delegate.</param>
		/// <exception cref="T:System.InvalidOperationException">The event does not have a public <see langword="remove" /> accessor.</exception>
		/// <exception cref="T:System.ArgumentException">The handler that was passed in cannot be used.</exception>
		/// <exception cref="T:System.Reflection.TargetException">
		///           In the .NET for Windows Store apps or the Portable Class Library, catch <see cref="T:System.Exception" /> instead.The <paramref name="target" /> parameter is <see langword="null" /> and the event is not static.-or- The <see cref="T:System.Reflection.EventInfo" /> is not declared on the target.</exception>
		/// <exception cref="T:System.MethodAccessException">
		///           In the .NET for Windows Store apps or the Portable Class Library, catch the base class exception, <see cref="T:System.MemberAccessException" />, instead.The caller does not have access permission to the member.</exception>
		// Token: 0x06001966 RID: 6502 RVA: 0x0000227E File Offset: 0x0000047E
		[MonoTODO]
		public override void RemoveEventHandler(object target, Delegate handler)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets the method that was used to add an event handler delegate to the event source.</summary>
		/// <param name="nonPublic">
		///       <see langword="true" /> to return non-public methods; otherwise, <see langword="false" />.</param>
		/// <returns>The method that was used to add an event handler delegate to the event source.</returns>
		/// <exception cref="T:System.MethodAccessException">
		///         <paramref name="nonPublic" /> is <see langword="true" /> and the method used to add an event handler delegate is non-public, but the caller does not have permission to reflect on non-public methods.</exception>
		// Token: 0x06001967 RID: 6503 RVA: 0x0000227E File Offset: 0x0000047E
		[MonoTODO]
		public override MethodInfo GetAddMethod(bool nonPublic)
		{
			throw new NotImplementedException();
		}

		/// <summary>When overridden in a derived class, returns the method that was called when the event was raised.</summary>
		/// <param name="nonPublic">
		///       <see langword="true" /> to return non-public methods; otherwise, <see langword="false" />. </param>
		/// <returns>The object that was called when the event was raised.</returns>
		/// <exception cref="T:System.MethodAccessException">
		///         <paramref name="nonPublic" /> is <see langword="true" /> and the method used to add an event handler delegate is non-public, but the caller does not have permission to reflect on non-public methods. </exception>
		// Token: 0x06001968 RID: 6504 RVA: 0x0000227E File Offset: 0x0000047E
		[MonoTODO]
		public override MethodInfo GetRaiseMethod(bool nonPublic)
		{
			throw new NotImplementedException();
		}

		/// <summary>When overridden in a derived class, retrieves the <see cref="T:System.Reflection.MethodInfo" /> object for removing a method of the event.</summary>
		/// <param name="nonPublic">
		///       <see langword="true" /> to return non-public methods; otherwise, <see langword="false" />. </param>
		/// <returns>The method that was used to remove an event handler delegate from the event source.</returns>
		/// <exception cref="T:System.MethodAccessException">
		///         <paramref name="nonPublic" /> is <see langword="true" /> and the method used to add an event handler delegate is non-public, but the caller does not have permission to reflect on non-public methods. </exception>
		// Token: 0x06001969 RID: 6505 RVA: 0x0000227E File Offset: 0x0000047E
		[MonoTODO]
		public override MethodInfo GetRemoveMethod(bool nonPublic)
		{
			throw new NotImplementedException();
		}

		/// <summary>When overridden in a derived class, gets an array that contains all the custom attributes of the specified type that are applied to this member.</summary>
		/// <param name="attributeType">The attribute type to search for. Only attributes that are assignable to this type can be returned.</param>
		/// <param name="inherit">
		///       <see langword="true" /> to search this member's inheritance chain to find the attributes; otherwise, <see langword="false" />.</param>
		/// <returns>An array that contains all the custom attributes of the specified type, or an array that has no elements if no attributes were defined.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="attributeType" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">This member belongs to a type that is loaded into the reflection-only context. See How to: Load Assemblies into the Reflection-Only Context</exception>
		/// <exception cref="T:System.TypeLoadException">A custom attribute type cannot be loaded.</exception>
		// Token: 0x0600196A RID: 6506 RVA: 0x0000227E File Offset: 0x0000047E
		[MonoTODO]
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			throw new NotImplementedException();
		}

		/// <summary>When overridden in a derived class, gets an array that contains all the custom attributes that are applied to this member.</summary>
		/// <param name="inherit">
		///       <see langword="true" /> to search this member's inheritance chain to find the attributes; otherwise, <see langword="false" />.</param>
		/// <returns>An array that contains all the custom attributes, or an array that has no elements if no attributes were defined.</returns>
		/// <exception cref="T:System.InvalidOperationException">This member belongs to a type that is loaded into the reflection-only context. See How to: Load Assemblies into the Reflection-Only Context.</exception>
		/// <exception cref="T:System.TypeLoadException">A custom attribute type cannot be loaded.</exception>
		// Token: 0x0600196B RID: 6507 RVA: 0x0000227E File Offset: 0x0000047E
		[MonoTODO]
		public override object[] GetCustomAttributes(bool inherit)
		{
			throw new NotImplementedException();
		}

		/// <summary>Indicates whether one or more instances of the specified attribute are applied to this member.</summary>
		/// <param name="attributeType">The attribute type to search for.</param>
		/// <param name="inherit">
		///       <see langword="true" /> to search this member's inheritance chain to find the attributes; otherwise, <see langword="false" />.</param>
		/// <returns>
		///     <see langword="true" /> if the specified attribute has been applied to this member; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600196C RID: 6508 RVA: 0x0000227E File Offset: 0x0000047E
		[MonoTODO]
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets the class object that was used to initialize this instance.</summary>
		/// <returns>The <see cref="T:System.Type" /> object that was used to initialize the current object.</returns>
		// Token: 0x170004E9 RID: 1257
		// (get) Token: 0x0600196D RID: 6509 RVA: 0x0000227E File Offset: 0x0000047E
		[MonoTODO]
		public override Type ReflectedType
		{
			get
			{
				throw new NotImplementedException();
			}
		}
	}
}
