﻿using System;
using System.Security;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics.PerformanceData
{
	/// <summary>Creates an instance of the logical counters defined in the <see cref="T:System.Diagnostics.PerformanceData.CounterSet" /> class.</summary>
	// Token: 0x020004B5 RID: 1205
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class CounterSetInstance : IDisposable
	{
		// Token: 0x06001D0E RID: 7438 RVA: 0x0000220F File Offset: 0x0000040F
		internal CounterSetInstance()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves the collection of counter data for the counter set instance.</summary>
		/// <returns>A collection of the counter data contained in the counter set instance.</returns>
		// Token: 0x170005B8 RID: 1464
		// (get) Token: 0x06001D0F RID: 7439 RVA: 0x0004B26D File Offset: 0x0004946D
		public CounterSetInstanceCounterDataSet Counters
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Releases all unmanaged resources used by this object.</summary>
		// Token: 0x06001D10 RID: 7440 RVA: 0x0000220F File Offset: 0x0000040F
		[SecurityCritical]
		public void Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
