﻿using System;
using System.Security;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics.PerformanceData
{
	/// <summary>Contains the collection of counter values.</summary>
	// Token: 0x020004B6 RID: 1206
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class CounterSetInstanceCounterDataSet : IDisposable
	{
		// Token: 0x06001D11 RID: 7441 RVA: 0x0000220F File Offset: 0x0000040F
		internal CounterSetInstanceCounterDataSet()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06001D12 RID: 7442 RVA: 0x0004B26D File Offset: 0x0004946D
		public CounterData get_Item(int counterId)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Accesses a counter value in the collection by using the specified counter name.</summary>
		/// <param name="counterName">Name of the counter. This is the name that you used when you added the counter to the counter set.</param>
		/// <returns>The counter data.</returns>
		// Token: 0x170005B9 RID: 1465
		public CounterData this[string counterName]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Releases all unmanaged resources used by this object.</summary>
		// Token: 0x06001D14 RID: 7444 RVA: 0x0000220F File Offset: 0x0000040F
		[SecurityCritical]
		public void Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
