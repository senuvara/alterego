﻿using System;
using System.Security;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics.PerformanceData
{
	/// <summary>Contains the raw data for a counter.</summary>
	// Token: 0x020004B1 RID: 1201
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class CounterData
	{
		// Token: 0x06001D00 RID: 7424 RVA: 0x0000220F File Offset: 0x0000040F
		internal CounterData()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets or gets the raw counter data.</summary>
		/// <returns>The raw counter data.</returns>
		// Token: 0x170005B6 RID: 1462
		// (get) Token: 0x06001D01 RID: 7425 RVA: 0x00053C88 File Offset: 0x00051E88
		// (set) Token: 0x06001D02 RID: 7426 RVA: 0x0000220F File Offset: 0x0000040F
		public long RawValue
		{
			[SecurityCritical]
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0L;
			}
			[SecurityCritical]
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Sets or gets the counter data.</summary>
		/// <returns>The counter data.</returns>
		// Token: 0x170005B7 RID: 1463
		// (get) Token: 0x06001D03 RID: 7427 RVA: 0x00053CA4 File Offset: 0x00051EA4
		// (set) Token: 0x06001D04 RID: 7428 RVA: 0x0000220F File Offset: 0x0000040F
		public long Value
		{
			[SecurityCritical]
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0L;
			}
			[SecurityCritical]
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Decrements the counter value by 1.</summary>
		// Token: 0x06001D05 RID: 7429 RVA: 0x0000220F File Offset: 0x0000040F
		[SecurityCritical]
		public void Decrement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Increments the counter value by 1.</summary>
		// Token: 0x06001D06 RID: 7430 RVA: 0x0000220F File Offset: 0x0000040F
		[SecurityCritical]
		public void Increment()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Increments the counter value by the specified amount.</summary>
		/// <param name="value">The amount by which to increment the counter value. The increment value can be positive or negative.</param>
		// Token: 0x06001D07 RID: 7431 RVA: 0x0000220F File Offset: 0x0000040F
		[SecurityCritical]
		public void IncrementBy(long value)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
