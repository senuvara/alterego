﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Directs tracing or debugging output of end-to-end events to an XML-encoded, schema-compliant log file.</summary>
	// Token: 0x020004AE RID: 1198
	[HostProtection(SecurityAction.LinkDemand, Synchronization = true)]
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public class EventSchemaTraceListener : TextWriterTraceListener
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.EventSchemaTraceListener" /> class, using the specified file as the recipient of debugging and tracing output.</summary>
		/// <param name="fileName">The path for the log file.</param>
		// Token: 0x06001CF3 RID: 7411 RVA: 0x0000220F File Offset: 0x0000040F
		public EventSchemaTraceListener(string fileName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.EventSchemaTraceListener" /> class with the specified name, using the specified file as the recipient of debugging and tracing output.</summary>
		/// <param name="fileName">The path for the log file.</param>
		/// <param name="name">The name of the listener.</param>
		// Token: 0x06001CF4 RID: 7412 RVA: 0x0000220F File Offset: 0x0000040F
		public EventSchemaTraceListener(string fileName, string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.EventSchemaTraceListener" /> class with the specified name and specified buffer size, using the specified file as the recipient of debugging and tracing output.</summary>
		/// <param name="fileName">The path for the log file.</param>
		/// <param name="name">The name of the listener.</param>
		/// <param name="bufferSize">The size of the output buffer, in bytes.</param>
		// Token: 0x06001CF5 RID: 7413 RVA: 0x0000220F File Offset: 0x0000040F
		public EventSchemaTraceListener(string fileName, string name, int bufferSize)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.EventSchemaTraceListener" /> class with the specified name and specified buffer size, using the specified file with the specified log retention policy as the recipient of the debugging and tracing output.</summary>
		/// <param name="fileName">The path for the log file.</param>
		/// <param name="name">The name of the listener.</param>
		/// <param name="bufferSize">The size of the output buffer, in bytes.</param>
		/// <param name="logRetentionOption">One of the <see cref="T:System.Diagnostics.TraceLogRetentionOption" /> values. </param>
		// Token: 0x06001CF6 RID: 7414 RVA: 0x0000220F File Offset: 0x0000040F
		public EventSchemaTraceListener(string fileName, string name, int bufferSize, TraceLogRetentionOption logRetentionOption)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.EventSchemaTraceListener" /> class with the specified name and specified buffer size, using the specified file with the specified log retention policy and maximum size as the recipient of the debugging and tracing output.</summary>
		/// <param name="fileName">The path for the log file.</param>
		/// <param name="name">The name of the listener.</param>
		/// <param name="bufferSize">The size of the output buffer, in bytes.</param>
		/// <param name="logRetentionOption">One of the <see cref="T:System.Diagnostics.TraceLogRetentionOption" /> values.</param>
		/// <param name="maximumFileSize">The maximum file size, in bytes.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="maximumFileSize" /> is less than <paramref name="bufferSize" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="maximumFileSize" /> is a negative number.</exception>
		// Token: 0x06001CF7 RID: 7415 RVA: 0x0000220F File Offset: 0x0000040F
		public EventSchemaTraceListener(string fileName, string name, int bufferSize, TraceLogRetentionOption logRetentionOption, long maximumFileSize)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.EventSchemaTraceListener" /> class with the specified name and specified buffer size, using the specified file with the specified log retention policy, maximum size, and file count as the recipient of the debugging and tracing output.</summary>
		/// <param name="fileName">The path for the log file.</param>
		/// <param name="name">The name of the listener.</param>
		/// <param name="bufferSize">The size of the output buffer, in bytes.</param>
		/// <param name="logRetentionOption">One of the <see cref="T:System.Diagnostics.TraceLogRetentionOption" /> values.</param>
		/// <param name="maximumFileSize">The maximum file size, in bytes.</param>
		/// <param name="maximumNumberOfFiles">The maximum number of output log files.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="maximumFileSize" /> is less than <paramref name="bufferSize" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="maximumFileSize" /> is a negative number.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="maximumNumberOfFiles" /> is less than 1, and <paramref name="logRetentionOption" /> is <see cref="F:System.Diagnostics.TraceLogRetentionOption.LimitedSequentialFiles" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="maximumNumberOfFiles" /> is less than 2, and <paramref name="logRetentionOption" /> is <see cref="F:System.Diagnostics.TraceLogRetentionOption.LimitedCircularFiles" />.</exception>
		// Token: 0x06001CF8 RID: 7416 RVA: 0x0000220F File Offset: 0x0000040F
		public EventSchemaTraceListener(string fileName, string name, int bufferSize, TraceLogRetentionOption logRetentionOption, long maximumFileSize, int maximumNumberOfFiles)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the size of the output buffer.</summary>
		/// <returns>The size of the output buffer, in bytes. </returns>
		// Token: 0x170005B1 RID: 1457
		// (get) Token: 0x06001CF9 RID: 7417 RVA: 0x00053C18 File Offset: 0x00051E18
		public int BufferSize
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the maximum size of the log file.</summary>
		/// <returns>The maximum file size, in bytes.</returns>
		// Token: 0x170005B2 RID: 1458
		// (get) Token: 0x06001CFA RID: 7418 RVA: 0x00053C34 File Offset: 0x00051E34
		public long MaximumFileSize
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0L;
			}
		}

		/// <summary>Gets the maximum number of log files.</summary>
		/// <returns>The maximum number of log files, determined by the value of the <see cref="P:System.Diagnostics.EventSchemaTraceListener.TraceLogRetentionOption" /> property for the file.</returns>
		// Token: 0x170005B3 RID: 1459
		// (get) Token: 0x06001CFB RID: 7419 RVA: 0x00053C50 File Offset: 0x00051E50
		public int MaximumNumberOfFiles
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the trace log retention option for the file.</summary>
		/// <returns>One of the <see cref="T:System.Diagnostics.TraceLogRetentionOption" /> values. The default is <see cref="F:System.Diagnostics.TraceLogRetentionOption.SingleFileUnboundedSize" />. </returns>
		// Token: 0x170005B4 RID: 1460
		// (get) Token: 0x06001CFC RID: 7420 RVA: 0x00053C6C File Offset: 0x00051E6C
		public TraceLogRetentionOption TraceLogRetentionOption
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return TraceLogRetentionOption.UnlimitedSequentialFiles;
			}
		}
	}
}
