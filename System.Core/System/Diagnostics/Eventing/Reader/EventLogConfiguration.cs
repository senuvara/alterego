﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics.Eventing.Reader
{
	/// <summary>Contains static information and configuration settings for an event log. Many of the configurations settings were defined by the event provider that created the log.</summary>
	// Token: 0x020004BE RID: 1214
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public class EventLogConfiguration : IDisposable
	{
		/// <summary>Initializes a new <see cref="T:System.Diagnostics.Eventing.Reader.EventLogConfiguration" /> object by specifying the local event log for which to get information and configuration settings. </summary>
		/// <param name="logName">The name of the local event log for which to get information and configuration settings.</param>
		// Token: 0x06001D3F RID: 7487 RVA: 0x0000220F File Offset: 0x0000040F
		public EventLogConfiguration(string logName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new <see cref="T:System.Diagnostics.Eventing.Reader.EventLogConfiguration" /> object by specifying the name of the log for which to get information and configuration settings. The log can be on the local computer or a remote computer, based on the event log session specified.</summary>
		/// <param name="logName">The name of the event log for which to get information and configuration settings.</param>
		/// <param name="session">The event log session used to determine the event log service that the specified log belongs to. The session is either connected to the event log service on the local computer or a remote computer.</param>
		// Token: 0x06001D40 RID: 7488 RVA: 0x0000220F File Offset: 0x0000040F
		[SecurityCritical]
		public EventLogConfiguration(string logName, EventLogSession session)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the flag that indicates if the event log is a classic event log. A classic event log is one that has its events defined in a .mc file instead of a manifest (.xml file) used by the event provider.</summary>
		/// <returns>Returns <see langword="true" /> if the event log is a classic log, and returns <see langword="false" /> if the event log is not a classic log.</returns>
		// Token: 0x170005C8 RID: 1480
		// (get) Token: 0x06001D41 RID: 7489 RVA: 0x00053EF0 File Offset: 0x000520F0
		public bool IsClassicLog
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets or sets a Boolean value that determines whether the event log is enabled or disabled. An enabled log is one in which events can be logged, and a disabled log is one in which events cannot be logged.</summary>
		/// <returns>Returns <see langword="true" /> if the log is enabled, and returns <see langword="false" /> if the log is disabled.</returns>
		// Token: 0x170005C9 RID: 1481
		// (get) Token: 0x06001D42 RID: 7490 RVA: 0x00053F0C File Offset: 0x0005210C
		// (set) Token: 0x06001D43 RID: 7491 RVA: 0x0000220F File Offset: 0x0000040F
		public bool IsEnabled
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the file directory path to the location of the file where the events are stored for the log.</summary>
		/// <returns>Returns a string that contains the path to the event log file.</returns>
		// Token: 0x170005CA RID: 1482
		// (get) Token: 0x06001D44 RID: 7492 RVA: 0x0004B26D File Offset: 0x0004946D
		// (set) Token: 0x06001D45 RID: 7493 RVA: 0x0000220F File Offset: 0x0000040F
		public string LogFilePath
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets an <see cref="T:System.Diagnostics.Eventing.Reader.EventLogIsolation" /> value that specifies whether the event log is an application, system, or custom event log. </summary>
		/// <returns>Returns an <see cref="T:System.Diagnostics.Eventing.Reader.EventLogIsolation" /> value.</returns>
		// Token: 0x170005CB RID: 1483
		// (get) Token: 0x06001D46 RID: 7494 RVA: 0x00053F28 File Offset: 0x00052128
		public EventLogIsolation LogIsolation
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return EventLogIsolation.Application;
			}
		}

		/// <summary>Gets or sets an <see cref="T:System.Diagnostics.Eventing.Reader.EventLogMode" /> value that determines how events are handled when the event log becomes full.</summary>
		/// <returns>Returns an <see cref="T:System.Diagnostics.Eventing.Reader.EventLogMode" /> value.</returns>
		// Token: 0x170005CC RID: 1484
		// (get) Token: 0x06001D47 RID: 7495 RVA: 0x00053F44 File Offset: 0x00052144
		// (set) Token: 0x06001D48 RID: 7496 RVA: 0x0000220F File Offset: 0x0000040F
		public EventLogMode LogMode
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return EventLogMode.Circular;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the name of the event log.</summary>
		/// <returns>Returns a string that contains the name of the event log.</returns>
		// Token: 0x170005CD RID: 1485
		// (get) Token: 0x06001D49 RID: 7497 RVA: 0x0004B26D File Offset: 0x0004946D
		public string LogName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets an <see cref="T:System.Diagnostics.Eventing.Reader.EventLogType" /> value that determines the type of the event log.</summary>
		/// <returns>Returns an <see cref="T:System.Diagnostics.Eventing.Reader.EventLogType" /> value.</returns>
		// Token: 0x170005CE RID: 1486
		// (get) Token: 0x06001D4A RID: 7498 RVA: 0x00053F60 File Offset: 0x00052160
		public EventLogType LogType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return EventLogType.Administrative;
			}
		}

		/// <summary>Gets or sets the maximum size, in bytes, that the event log file is allowed to be. When the file reaches this maximum size, it is considered full.</summary>
		/// <returns>Returns a long value that represents the maximum size, in bytes, that the event log file is allowed to be.</returns>
		// Token: 0x170005CF RID: 1487
		// (get) Token: 0x06001D4B RID: 7499 RVA: 0x00053F7C File Offset: 0x0005217C
		// (set) Token: 0x06001D4C RID: 7500 RVA: 0x0000220F File Offset: 0x0000040F
		public long MaximumSizeInBytes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0L;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the name of the event provider that created this event log.</summary>
		/// <returns>Returns a string that contains the name of the event provider that created this event log.</returns>
		// Token: 0x170005D0 RID: 1488
		// (get) Token: 0x06001D4D RID: 7501 RVA: 0x0004B26D File Offset: 0x0004946D
		public string OwningProviderName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the size of the buffer that the event provider uses for publishing events to the log.</summary>
		/// <returns>Returns an integer value that can be null.</returns>
		// Token: 0x170005D1 RID: 1489
		// (get) Token: 0x06001D4E RID: 7502 RVA: 0x00053F98 File Offset: 0x00052198
		public int? ProviderBufferSize
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the control globally unique identifier (GUID) for the event log if the log is a debug log. If this log is not a debug log, this value will be null. </summary>
		/// <returns>Returns a GUID value or null.</returns>
		// Token: 0x170005D2 RID: 1490
		// (get) Token: 0x06001D4F RID: 7503 RVA: 0x00053FB4 File Offset: 0x000521B4
		public Guid? ProviderControlGuid
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets keyword mask used by the event provider.</summary>
		/// <returns>Returns a long value that can be null if the event provider did not define any keywords.</returns>
		// Token: 0x170005D3 RID: 1491
		// (get) Token: 0x06001D50 RID: 7504 RVA: 0x00053FD0 File Offset: 0x000521D0
		// (set) Token: 0x06001D51 RID: 7505 RVA: 0x0000220F File Offset: 0x0000040F
		public long? ProviderKeywords
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the maximum latency time used by the event provider when publishing events to the log.</summary>
		/// <returns>Returns an integer value that can be null if no latency time was specified by the event provider.</returns>
		// Token: 0x170005D4 RID: 1492
		// (get) Token: 0x06001D52 RID: 7506 RVA: 0x00053FEC File Offset: 0x000521EC
		public int? ProviderLatency
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the maximum event level (which defines the severity of the event) that is allowed to be logged in the event log. This value is defined by the event provider.</summary>
		/// <returns>Returns an integer value that can be null if the maximum event level was not defined in the event provider.</returns>
		// Token: 0x170005D5 RID: 1493
		// (get) Token: 0x06001D53 RID: 7507 RVA: 0x00054008 File Offset: 0x00052208
		// (set) Token: 0x06001D54 RID: 7508 RVA: 0x0000220F File Offset: 0x0000040F
		public int? ProviderLevel
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the maximum number of buffers used by the event provider to publish events to the event log.</summary>
		/// <returns>Returns an integer value that is the maximum number of buffers used by the event provider to publish events to the event log. This value can be null.</returns>
		// Token: 0x170005D6 RID: 1494
		// (get) Token: 0x06001D55 RID: 7509 RVA: 0x00054024 File Offset: 0x00052224
		public int? ProviderMaximumNumberOfBuffers
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the minimum number of buffers used by the event provider to publish events to the event log.</summary>
		/// <returns>Returns an integer value that is the minimum number of buffers used by the event provider to publish events to the event log. This value can be null.</returns>
		// Token: 0x170005D7 RID: 1495
		// (get) Token: 0x06001D56 RID: 7510 RVA: 0x00054040 File Offset: 0x00052240
		public int? ProviderMinimumNumberOfBuffers
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets an enumerable collection of the names of all the event providers that can publish events to this event log.</summary>
		/// <returns>Returns an enumerable collection of strings that contain the event provider names.</returns>
		// Token: 0x170005D8 RID: 1496
		// (get) Token: 0x06001D57 RID: 7511 RVA: 0x0005405B File Offset: 0x0005225B
		public IEnumerable<string> ProviderNames
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets or sets the security descriptor of the event log. The security descriptor defines the users and groups of users that can read and write to the event log.</summary>
		/// <returns>Returns a string that contains the security descriptor for the event log.</returns>
		// Token: 0x170005D9 RID: 1497
		// (get) Token: 0x06001D58 RID: 7512 RVA: 0x0004B26D File Offset: 0x0004946D
		// (set) Token: 0x06001D59 RID: 7513 RVA: 0x0000220F File Offset: 0x0000040F
		public string SecurityDescriptor
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Releases all the resources used by this object.</summary>
		// Token: 0x06001D5A RID: 7514 RVA: 0x0000220F File Offset: 0x0000040F
		public void Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Releases the unmanaged resources used by this object, and optionally releases the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources.</param>
		// Token: 0x06001D5B RID: 7515 RVA: 0x0000220F File Offset: 0x0000040F
		[SecuritySafeCritical]
		protected virtual void Dispose(bool disposing)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Saves the configuration settings that </summary>
		// Token: 0x06001D5C RID: 7516 RVA: 0x0000220F File Offset: 0x0000040F
		public void SaveChanges()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
