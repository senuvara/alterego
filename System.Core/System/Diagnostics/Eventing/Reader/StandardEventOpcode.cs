﻿using System;

namespace System.Diagnostics.Eventing.Reader
{
	/// <summary>Defines the standard opcodes that are attached to events by the event provider. For more information about opcodes, see <see cref="T:System.Diagnostics.Eventing.Reader.EventOpcode" />.</summary>
	// Token: 0x020004DB RID: 1243
	public enum StandardEventOpcode
	{
		/// <summary>An event with this opcode is a trace collection start event.</summary>
		// Token: 0x04000D8F RID: 3471
		DataCollectionStart = 3,
		/// <summary>An event with this opcode is a trace collection stop event.</summary>
		// Token: 0x04000D90 RID: 3472
		DataCollectionStop,
		/// <summary>An event with this opcode is an extension event.</summary>
		// Token: 0x04000D91 RID: 3473
		Extension,
		/// <summary>An event with this opcode is an informational event.</summary>
		// Token: 0x04000D92 RID: 3474
		Info = 0,
		/// <summary>An event with this opcode is published when one activity in an application receives data.</summary>
		// Token: 0x04000D93 RID: 3475
		Receive = 240,
		/// <summary>An event with this opcode is published after an activity in an application replies to an event.</summary>
		// Token: 0x04000D94 RID: 3476
		Reply = 6,
		/// <summary>An event with this opcode is published after an activity in an application resumes from a suspended state. The event should follow an event with the Suspend opcode.</summary>
		// Token: 0x04000D95 RID: 3477
		Resume,
		/// <summary>An event with this opcode is published when one activity in an application transfers data or system resources to another activity. </summary>
		// Token: 0x04000D96 RID: 3478
		Send = 9,
		/// <summary>An event with this opcode is published when an application starts a new transaction or activity. This can be embedded into another transaction or activity when multiple events with the Start opcode follow each other without an event with a Stop opcode.</summary>
		// Token: 0x04000D97 RID: 3479
		Start = 1,
		/// <summary>An event with this opcode is published when an activity or a transaction in an application ends. The event corresponds to the last unpaired event with a Start opcode.</summary>
		// Token: 0x04000D98 RID: 3480
		Stop,
		/// <summary>An event with this opcode is published when an activity in an application is suspended. </summary>
		// Token: 0x04000D99 RID: 3481
		Suspend = 8
	}
}
