﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics.Eventing.Reader
{
	/// <summary>Represents a link between an event provider and an event log that the provider publishes events into. This object cannot be instantiated.</summary>
	// Token: 0x020004C8 RID: 1224
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class EventLogLink
	{
		// Token: 0x06001D7F RID: 7551 RVA: 0x0000220F File Offset: 0x0000040F
		internal EventLogLink()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the localized name of the event log.</summary>
		/// <returns>Returns a string that contains the localized name of the event log.</returns>
		// Token: 0x170005E3 RID: 1507
		// (get) Token: 0x06001D80 RID: 7552 RVA: 0x0004B26D File Offset: 0x0004946D
		public string DisplayName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a Boolean value that determines whether the event log is imported, rather than defined in the event provider. An imported event log is defined in a different provider.</summary>
		/// <returns>Returns <see langword="true" /> if the event log is imported by the event provider, and returns <see langword="false" /> if the event log is not imported by the event provider.</returns>
		// Token: 0x170005E4 RID: 1508
		// (get) Token: 0x06001D81 RID: 7553 RVA: 0x00054144 File Offset: 0x00052344
		public bool IsImported
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets the non-localized name of the event log associated with this object.</summary>
		/// <returns>Returns a string that contains the non-localized name of the event log associated with this object.</returns>
		// Token: 0x170005E5 RID: 1509
		// (get) Token: 0x06001D82 RID: 7554 RVA: 0x0004B26D File Offset: 0x0004946D
		public string LogName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
