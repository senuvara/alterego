﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics.Eventing.Reader
{
	/// <summary>Contains an event opcode that is defined in an event provider. An opcode defines a numeric value that identifies the activity or a point within an activity that the application was performing when it raised the event.</summary>
	// Token: 0x020004D6 RID: 1238
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class EventOpcode
	{
		// Token: 0x06001E03 RID: 7683 RVA: 0x0000220F File Offset: 0x0000040F
		internal EventOpcode()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the localized name for an event opcode.</summary>
		/// <returns>Returns a string that contains the localized name for an event opcode.</returns>
		// Token: 0x1700062C RID: 1580
		// (get) Token: 0x06001E04 RID: 7684 RVA: 0x0004B26D File Offset: 0x0004946D
		public string DisplayName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the non-localized name for an event opcode.</summary>
		/// <returns>Returns a string that contains the non-localized name for an event opcode.</returns>
		// Token: 0x1700062D RID: 1581
		// (get) Token: 0x06001E05 RID: 7685 RVA: 0x0004B26D File Offset: 0x0004946D
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the numeric value associated with the event opcode.</summary>
		/// <returns>Returns an integer value.</returns>
		// Token: 0x1700062E RID: 1582
		// (get) Token: 0x06001E06 RID: 7686 RVA: 0x000543AC File Offset: 0x000525AC
		public int Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}
	}
}
