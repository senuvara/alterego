﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics.Eventing.Reader
{
	/// <summary>Represents a keyword for an event. Keywords are defined in an event provider and are used to group the event with other similar events (based on the usage of the events).</summary>
	// Token: 0x020004BC RID: 1212
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class EventKeyword
	{
		// Token: 0x06001D37 RID: 7479 RVA: 0x0000220F File Offset: 0x0000040F
		internal EventKeyword()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the localized name of the keyword.</summary>
		/// <returns>Returns a string that contains a localized name for this keyword.</returns>
		// Token: 0x170005C2 RID: 1474
		// (get) Token: 0x06001D38 RID: 7480 RVA: 0x0004B26D File Offset: 0x0004946D
		public string DisplayName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the non-localized name of the keyword.</summary>
		/// <returns>Returns a string that contains the non-localized name of this keyword.</returns>
		// Token: 0x170005C3 RID: 1475
		// (get) Token: 0x06001D39 RID: 7481 RVA: 0x0004B26D File Offset: 0x0004946D
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the numeric value associated with the keyword.</summary>
		/// <returns>Returns a <see langword="long" /> value.</returns>
		// Token: 0x170005C4 RID: 1476
		// (get) Token: 0x06001D3A RID: 7482 RVA: 0x00053EB8 File Offset: 0x000520B8
		public long Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0L;
			}
		}
	}
}
