﻿using System;

namespace System.Diagnostics.Eventing.Reader
{
	/// <summary>Determines the behavior for the event log service handles an event log when the log reaches its maximum allowed size (when the event log is full).</summary>
	// Token: 0x020004C4 RID: 1220
	public enum EventLogMode
	{
		/// <summary>Archive the log when full, do not overwrite events. The log is automatically archived when necessary. No events are overwritten. </summary>
		// Token: 0x04000D74 RID: 3444
		AutoBackup = 1,
		/// <summary>New events continue to be stored when the log file is full. Each new incoming event replaces the oldest event in the log.</summary>
		// Token: 0x04000D75 RID: 3445
		Circular = 0,
		/// <summary>Do not overwrite events. Clear the log manually rather than automatically.</summary>
		// Token: 0x04000D76 RID: 3446
		Retain = 2
	}
}
