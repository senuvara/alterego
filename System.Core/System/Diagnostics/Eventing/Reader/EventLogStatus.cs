﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics.Eventing.Reader
{
	/// <summary>Contains the status code or error code for a specific event log. This status can be used to determine if the event log is available for an operation.</summary>
	// Token: 0x020004CE RID: 1230
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class EventLogStatus
	{
		// Token: 0x06001DA5 RID: 7589 RVA: 0x0000220F File Offset: 0x0000040F
		internal EventLogStatus()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the name of the event log for which the status code is obtained.</summary>
		/// <returns>Returns a string that contains the name of the event log for which the status code is obtained.</returns>
		// Token: 0x170005EB RID: 1515
		// (get) Token: 0x06001DA6 RID: 7590 RVA: 0x0004B26D File Offset: 0x0004946D
		public string LogName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the status code or error code for the event log. This status or error is the result of a read or subscription operation on the event log.</summary>
		/// <returns>Returns an integer value.</returns>
		// Token: 0x170005EC RID: 1516
		// (get) Token: 0x06001DA7 RID: 7591 RVA: 0x000541B4 File Offset: 0x000523B4
		public int StatusCode
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}
	}
}
