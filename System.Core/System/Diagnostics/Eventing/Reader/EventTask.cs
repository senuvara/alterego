﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics.Eventing.Reader
{
	/// <summary>Contains an event task that is defined in an event provider. The task identifies a portion of an application or a component that publishes an event. A task is a 16-bit value with 16 top values reserved.</summary>
	// Token: 0x020004D7 RID: 1239
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class EventTask
	{
		// Token: 0x06001E07 RID: 7687 RVA: 0x0000220F File Offset: 0x0000040F
		internal EventTask()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the localized name for the event task.</summary>
		/// <returns>Returns a string that contains the localized name for the event task.</returns>
		// Token: 0x1700062F RID: 1583
		// (get) Token: 0x06001E08 RID: 7688 RVA: 0x0004B26D File Offset: 0x0004946D
		public string DisplayName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the event globally unique identifier (GUID) associated with the task. </summary>
		/// <returns>Returns a GUID value.</returns>
		// Token: 0x17000630 RID: 1584
		// (get) Token: 0x06001E09 RID: 7689 RVA: 0x000543C8 File Offset: 0x000525C8
		public Guid EventGuid
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Guid);
			}
		}

		/// <summary>Gets the non-localized name of the event task.</summary>
		/// <returns>Returns a string that contains the non-localized name of the event task.</returns>
		// Token: 0x17000631 RID: 1585
		// (get) Token: 0x06001E0A RID: 7690 RVA: 0x0004B26D File Offset: 0x0004946D
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the numeric value associated with the task.</summary>
		/// <returns>Returns an integer value.</returns>
		// Token: 0x17000632 RID: 1586
		// (get) Token: 0x06001E0B RID: 7691 RVA: 0x000543E4 File Offset: 0x000525E4
		public int Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}
	}
}
