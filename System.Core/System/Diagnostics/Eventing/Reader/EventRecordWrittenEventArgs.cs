﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics.Eventing.Reader
{
	/// <summary>When the <see cref="E:System.Diagnostics.Eventing.Reader.EventLogWatcher.EventRecordWritten" /> event is raised, an instance of this object is passed to the delegate method that handles the event. This object contains the event that was published to the event log or the exception that occurred when the event subscription failed. </summary>
	// Token: 0x020004D4 RID: 1236
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class EventRecordWrittenEventArgs : EventArgs
	{
		// Token: 0x06001DF6 RID: 7670 RVA: 0x0000220F File Offset: 0x0000040F
		internal EventRecordWrittenEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the exception that occurred when the event subscription failed. The exception has a description of why the subscription failed.</summary>
		/// <returns>Returns an <see cref="T:System.Exception" /> object.</returns>
		// Token: 0x17000621 RID: 1569
		// (get) Token: 0x06001DF7 RID: 7671 RVA: 0x0004B26D File Offset: 0x0004946D
		public Exception EventException
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the event record that is published to the event log. This event matches the criteria from the query specified in the event subscription.</summary>
		/// <returns>Returns a <see cref="T:System.Diagnostics.Eventing.Reader.EventRecord" /> object.</returns>
		// Token: 0x17000622 RID: 1570
		// (get) Token: 0x06001DF8 RID: 7672 RVA: 0x0004B26D File Offset: 0x0004946D
		public EventRecord EventRecord
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
