﻿using System;

namespace System.Diagnostics.Eventing.Reader
{
	/// <summary>Specifies that a string contains a name of an event log or the file system path to an event log file.</summary>
	// Token: 0x020004C1 RID: 1217
	public enum PathType
	{
		/// <summary>A path parameter contains the file system path to an event log file.</summary>
		// Token: 0x04000D6D RID: 3437
		FilePath = 2,
		/// <summary>A path parameter contains the name of the event log.</summary>
		// Token: 0x04000D6E RID: 3438
		LogName = 1
	}
}
