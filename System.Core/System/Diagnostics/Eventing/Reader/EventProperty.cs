﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics.Eventing.Reader
{
	/// <summary>Contains the value of an event property that is specified by the event provider when the event is published.</summary>
	// Token: 0x020004D0 RID: 1232
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class EventProperty
	{
		// Token: 0x06001DC6 RID: 7622 RVA: 0x0000220F File Offset: 0x0000040F
		internal EventProperty()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the value of the event property that is specified by the event provider when the event is published.</summary>
		/// <returns>Returns an object.</returns>
		// Token: 0x17000605 RID: 1541
		// (get) Token: 0x06001DC7 RID: 7623 RVA: 0x0004B26D File Offset: 0x0004946D
		public object Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
