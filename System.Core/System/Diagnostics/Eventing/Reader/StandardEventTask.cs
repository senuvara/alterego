﻿using System;

namespace System.Diagnostics.Eventing.Reader
{
	/// <summary>Defines the standard tasks that are attached to events by the event provider. For more information about tasks, see <see cref="T:System.Diagnostics.Eventing.Reader.EventTask" />.</summary>
	// Token: 0x020004DC RID: 1244
	public enum StandardEventTask
	{
		/// <summary>No task is used to identify a portion of an application that publishes an event.</summary>
		// Token: 0x04000D9B RID: 3483
		None
	}
}
