﻿using System;

namespace System.Diagnostics.Eventing.Reader
{
	/// <summary>Defines values for the type of authentication used during a Remote Procedure Call (RPC) login to a server. This login occurs when you create a <see cref="T:System.Diagnostics.Eventing.Reader.EventLogSession" /> object that specifies a connection to a remote computer.</summary>
	// Token: 0x020004C0 RID: 1216
	public enum SessionAuthentication
	{
		/// <summary>Use the default authentication method during RPC login. The default authentication is equivalent to Negotiate.</summary>
		// Token: 0x04000D68 RID: 3432
		Default,
		/// <summary>Use Kerberos authentication during RPC login. </summary>
		// Token: 0x04000D69 RID: 3433
		Kerberos = 2,
		/// <summary>Use the Negotiate authentication method during RPC login. This allows the client application to select the most appropriate authentication method (NTLM or Kerberos) for the situation. </summary>
		// Token: 0x04000D6A RID: 3434
		Negotiate = 1,
		/// <summary>Use Windows NT LAN Manager (NTLM) authentication during RPC login.</summary>
		// Token: 0x04000D6B RID: 3435
		Ntlm = 3
	}
}
