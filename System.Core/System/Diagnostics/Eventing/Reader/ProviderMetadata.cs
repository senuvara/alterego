﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Security;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics.Eventing.Reader
{
	/// <summary>Contains static information about an event provider, such as the name and id of the provider, and the collection of events defined in the provider.</summary>
	// Token: 0x020004D8 RID: 1240
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public class ProviderMetadata : IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Eventing.Reader.ProviderMetadata" /> class by specifying the name of the provider that you want to retrieve information about.</summary>
		/// <param name="providerName">The name of the event provider that you want to retrieve information about.</param>
		// Token: 0x06001E0C RID: 7692 RVA: 0x0000220F File Offset: 0x0000040F
		public ProviderMetadata(string providerName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Eventing.Reader.ProviderMetadata" /> class by specifying the name of the provider that you want to retrieve information about, the event log service that the provider is registered with, and the language that you want to return the information in.</summary>
		/// <param name="providerName">The name of the event provider that you want to retrieve information about.</param>
		/// <param name="session">The <see cref="T:System.Diagnostics.Eventing.Reader.EventLogSession" /> object that specifies whether to get the provider information from a provider on the local computer or a provider on a remote computer.</param>
		/// <param name="targetCultureInfo">The culture that specifies the language that the information should be returned in.</param>
		// Token: 0x06001E0D RID: 7693 RVA: 0x0000220F File Offset: 0x0000040F
		public ProviderMetadata(string providerName, EventLogSession session, CultureInfo targetCultureInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the localized name of the event provider.</summary>
		/// <returns>Returns a string that contains the localized name of the event provider.</returns>
		// Token: 0x17000633 RID: 1587
		// (get) Token: 0x06001E0E RID: 7694 RVA: 0x0004B26D File Offset: 0x0004946D
		public string DisplayName
		{
			[SecurityCritical]
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets an enumerable collection of <see cref="T:System.Diagnostics.Eventing.Reader.EventMetadata" /> objects, each of which represents an event that is defined in the provider.</summary>
		/// <returns>Returns an enumerable collection of <see cref="T:System.Diagnostics.Eventing.Reader.EventMetadata" /> objects.</returns>
		// Token: 0x17000634 RID: 1588
		// (get) Token: 0x06001E0F RID: 7695 RVA: 0x0005405B File Offset: 0x0005225B
		public IEnumerable<EventMetadata> Events
		{
			[SecurityCritical]
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the base of the URL used to form help requests for the events in this event provider.</summary>
		/// <returns>Returns a <see cref="T:System.Uri" /> value.</returns>
		// Token: 0x17000635 RID: 1589
		// (get) Token: 0x06001E10 RID: 7696 RVA: 0x0004B26D File Offset: 0x0004946D
		public Uri HelpLink
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the globally unique identifier (GUID) for the event provider.</summary>
		/// <returns>Returns the GUID value for the event provider.</returns>
		// Token: 0x17000636 RID: 1590
		// (get) Token: 0x06001E11 RID: 7697 RVA: 0x00054400 File Offset: 0x00052600
		public Guid Id
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Guid);
			}
		}

		/// <summary>Gets an enumerable collection of <see cref="T:System.Diagnostics.Eventing.Reader.EventKeyword" /> objects, each of which represent an event keyword that is defined in the event provider.</summary>
		/// <returns>Returns an enumerable collection of <see cref="T:System.Diagnostics.Eventing.Reader.EventKeyword" /> objects.</returns>
		// Token: 0x17000637 RID: 1591
		// (get) Token: 0x06001E12 RID: 7698 RVA: 0x0005405B File Offset: 0x0005225B
		public IList<EventKeyword> Keywords
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets an enumerable collection of <see cref="T:System.Diagnostics.Eventing.Reader.EventLevel" /> objects, each of which represent a level that is defined in the event provider.</summary>
		/// <returns>Returns an enumerable collection of <see cref="T:System.Diagnostics.Eventing.Reader.EventLevel" /> objects.</returns>
		// Token: 0x17000638 RID: 1592
		// (get) Token: 0x06001E13 RID: 7699 RVA: 0x0005405B File Offset: 0x0005225B
		public IList<EventLevel> Levels
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets an enumerable collection of <see cref="T:System.Diagnostics.Eventing.Reader.EventLogLink" /> objects, each of which represent a link to an event log that is used by the event provider.</summary>
		/// <returns>Returns an enumerable collection of <see cref="T:System.Diagnostics.Eventing.Reader.EventLogLink" /> objects.</returns>
		// Token: 0x17000639 RID: 1593
		// (get) Token: 0x06001E14 RID: 7700 RVA: 0x0005405B File Offset: 0x0005225B
		public IList<EventLogLink> LogLinks
		{
			[SecurityCritical]
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the path of the file that contains the message table resource that has the strings associated with the provider metadata.</summary>
		/// <returns>Returns a string that contains the path of the provider message file.</returns>
		// Token: 0x1700063A RID: 1594
		// (get) Token: 0x06001E15 RID: 7701 RVA: 0x0004B26D File Offset: 0x0004946D
		public string MessageFilePath
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the unique name of the event provider.</summary>
		/// <returns>Returns a string that contains the unique name of the event provider.</returns>
		// Token: 0x1700063B RID: 1595
		// (get) Token: 0x06001E16 RID: 7702 RVA: 0x0004B26D File Offset: 0x0004946D
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets an enumerable collection of <see cref="T:System.Diagnostics.Eventing.Reader.EventOpcode" /> objects, each of which represent an opcode that is defined in the event provider.</summary>
		/// <returns>Returns an enumerable collection of <see cref="T:System.Diagnostics.Eventing.Reader.EventOpcode" /> objects.</returns>
		// Token: 0x1700063C RID: 1596
		// (get) Token: 0x06001E17 RID: 7703 RVA: 0x0005405B File Offset: 0x0005225B
		public IList<EventOpcode> Opcodes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the path of the file that contains the message table resource that has the strings used for parameter substitutions in event descriptions.</summary>
		/// <returns>Returns a string that contains the path of the file that contains the message table resource that has the strings used for parameter substitutions in event descriptions.</returns>
		// Token: 0x1700063D RID: 1597
		// (get) Token: 0x06001E18 RID: 7704 RVA: 0x0004B26D File Offset: 0x0004946D
		public string ParameterFilePath
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the path to the file that contains the metadata associated with the provider.</summary>
		/// <returns>Returns a string that contains the path to the file that contains the metadata associated with the provider.</returns>
		// Token: 0x1700063E RID: 1598
		// (get) Token: 0x06001E19 RID: 7705 RVA: 0x0004B26D File Offset: 0x0004946D
		public string ResourceFilePath
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets an enumerable collection of <see cref="T:System.Diagnostics.Eventing.Reader.EventTask" /> objects, each of which represent a task that is defined in the event provider.</summary>
		/// <returns>Returns an enumerable collection of <see cref="T:System.Diagnostics.Eventing.Reader.EventTask" /> objects.</returns>
		// Token: 0x1700063F RID: 1599
		// (get) Token: 0x06001E1A RID: 7706 RVA: 0x0005405B File Offset: 0x0005225B
		public IList<EventTask> Tasks
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Releases all the resources used by this object.</summary>
		// Token: 0x06001E1B RID: 7707 RVA: 0x0000220F File Offset: 0x0000040F
		public void Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Releases the unmanaged resources used by this object, and optionally releases the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources.</param>
		// Token: 0x06001E1C RID: 7708 RVA: 0x0000220F File Offset: 0x0000040F
		[SecuritySafeCritical]
		protected virtual void Dispose(bool disposing)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
