﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics.Eventing.Reader
{
	/// <summary>Contains an event level that is defined in an event provider. The level signifies the severity of the event.</summary>
	// Token: 0x020004BD RID: 1213
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class EventLevel
	{
		// Token: 0x06001D3B RID: 7483 RVA: 0x0000220F File Offset: 0x0000040F
		internal EventLevel()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the localized name for the event level. The name describes what severity level of events this level is used for.</summary>
		/// <returns>Returns a string that contains the localized name for the event level.</returns>
		// Token: 0x170005C5 RID: 1477
		// (get) Token: 0x06001D3C RID: 7484 RVA: 0x0004B26D File Offset: 0x0004946D
		public string DisplayName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the non-localized name of the event level.</summary>
		/// <returns>Returns a string that contains the non-localized name of the event level.</returns>
		// Token: 0x170005C6 RID: 1478
		// (get) Token: 0x06001D3D RID: 7485 RVA: 0x0004B26D File Offset: 0x0004946D
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the numeric value of the event level.</summary>
		/// <returns>Returns an integer value.</returns>
		// Token: 0x170005C7 RID: 1479
		// (get) Token: 0x06001D3E RID: 7486 RVA: 0x00053ED4 File Offset: 0x000520D4
		public int Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}
	}
}
