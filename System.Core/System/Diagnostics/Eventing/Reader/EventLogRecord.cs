﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Security.Permissions;
using System.Security.Principal;
using Unity;

namespace System.Diagnostics.Eventing.Reader
{
	/// <summary>Contains the properties of an event instance for an event that is received from an <see cref="T:System.Diagnostics.Eventing.Reader.EventLogReader" /> object. The event properties provide information about the event such as the name of the computer where the event was logged and the time that the event was created.</summary>
	// Token: 0x020004D2 RID: 1234
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public class EventLogRecord : EventRecord
	{
		// Token: 0x06001DCC RID: 7628 RVA: 0x0000220F File Offset: 0x0000040F
		internal EventLogRecord()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the globally unique identifier (GUID) for the activity in process for which the event is involved. This allows consumers to group related activities.</summary>
		/// <returns>Returns a GUID value.</returns>
		// Token: 0x17000606 RID: 1542
		// (get) Token: 0x06001DCD RID: 7629 RVA: 0x000541D0 File Offset: 0x000523D0
		public override Guid? ActivityId
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a placeholder (bookmark) that corresponds to this event. This can be used as a placeholder in a stream of events.</summary>
		/// <returns>Returns a <see cref="T:System.Diagnostics.Eventing.Reader.EventBookmark" /> object.</returns>
		// Token: 0x17000607 RID: 1543
		// (get) Token: 0x06001DCE RID: 7630 RVA: 0x0004B26D File Offset: 0x0004946D
		public override EventBookmark Bookmark
		{
			[SecuritySafeCritical]
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the name of the event log or the event log file in which the event is stored.</summary>
		/// <returns>Returns a string that contains the name of the event log or the event log file in which the event is stored.</returns>
		// Token: 0x17000608 RID: 1544
		// (get) Token: 0x06001DCF RID: 7631 RVA: 0x0004B26D File Offset: 0x0004946D
		public string ContainerLog
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the identifier for this event. All events with this identifier value represent the same type of event.</summary>
		/// <returns>Returns an integer value. This value can be null.</returns>
		// Token: 0x17000609 RID: 1545
		// (get) Token: 0x06001DD0 RID: 7632 RVA: 0x000541EC File Offset: 0x000523EC
		public override int Id
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the keyword mask of the event. Get the value of the <see cref="P:System.Diagnostics.Eventing.Reader.EventLogRecord.KeywordsDisplayNames" /> property to get the name of the keywords used in this mask.</summary>
		/// <returns>Returns a long value. This value can be null.</returns>
		// Token: 0x1700060A RID: 1546
		// (get) Token: 0x06001DD1 RID: 7633 RVA: 0x00054208 File Offset: 0x00052408
		public override long? Keywords
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the display names of the keywords used in the keyword mask for this event.</summary>
		/// <returns>Returns an enumerable collection of strings that contain the display names of the keywords used in the keyword mask for this event.</returns>
		// Token: 0x1700060B RID: 1547
		// (get) Token: 0x06001DD2 RID: 7634 RVA: 0x0005405B File Offset: 0x0005225B
		public override IEnumerable<string> KeywordsDisplayNames
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the level of the event. The level signifies the severity of the event. For the name of the level, get the value of the <see cref="P:System.Diagnostics.Eventing.Reader.EventLogRecord.LevelDisplayName" /> property.</summary>
		/// <returns>Returns a byte value. This value can be null.</returns>
		// Token: 0x1700060C RID: 1548
		// (get) Token: 0x06001DD3 RID: 7635 RVA: 0x00054224 File Offset: 0x00052424
		public override byte? Level
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the display name of the level for this event.</summary>
		/// <returns>Returns a string that contains the display name of the level for this event.</returns>
		// Token: 0x1700060D RID: 1549
		// (get) Token: 0x06001DD4 RID: 7636 RVA: 0x0004B26D File Offset: 0x0004946D
		public override string LevelDisplayName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the name of the event log where this event is logged.</summary>
		/// <returns>Returns a string that contains a name of the event log that contains this event.</returns>
		// Token: 0x1700060E RID: 1550
		// (get) Token: 0x06001DD5 RID: 7637 RVA: 0x0004B26D File Offset: 0x0004946D
		public override string LogName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the name of the computer on which this event was logged.</summary>
		/// <returns>Returns a string that contains the name of the computer on which this event was logged.</returns>
		// Token: 0x1700060F RID: 1551
		// (get) Token: 0x06001DD6 RID: 7638 RVA: 0x0004B26D File Offset: 0x0004946D
		public override string MachineName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a list of query identifiers that this event matches. This event matches a query if the query would return this event.</summary>
		/// <returns>Returns an enumerable collection of integer values.</returns>
		// Token: 0x17000610 RID: 1552
		// (get) Token: 0x06001DD7 RID: 7639 RVA: 0x0005405B File Offset: 0x0005225B
		public IEnumerable<int> MatchedQueryIds
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the opcode of the event. The opcode defines a numeric value that identifies the activity or a point within an activity that the application was performing when it raised the event. For the name of the opcode, get the value of the <see cref="P:System.Diagnostics.Eventing.Reader.EventLogRecord.OpcodeDisplayName" /> property.</summary>
		/// <returns>Returns a short value. This value can be null.</returns>
		// Token: 0x17000611 RID: 1553
		// (get) Token: 0x06001DD8 RID: 7640 RVA: 0x00054240 File Offset: 0x00052440
		public override short? Opcode
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the display name of the opcode for this event.</summary>
		/// <returns>Returns a string that contains the display name of the opcode for this event.</returns>
		// Token: 0x17000612 RID: 1554
		// (get) Token: 0x06001DD9 RID: 7641 RVA: 0x0004B26D File Offset: 0x0004946D
		public override string OpcodeDisplayName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the process identifier for the event provider that logged this event.</summary>
		/// <returns>Returns an integer value. This value can be null.</returns>
		// Token: 0x17000613 RID: 1555
		// (get) Token: 0x06001DDA RID: 7642 RVA: 0x0005425C File Offset: 0x0005245C
		public override int? ProcessId
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the user-supplied properties of the event.</summary>
		/// <returns>Returns a list of <see cref="T:System.Diagnostics.Eventing.Reader.EventProperty" /> objects.</returns>
		// Token: 0x17000614 RID: 1556
		// (get) Token: 0x06001DDB RID: 7643 RVA: 0x0005405B File Offset: 0x0005225B
		public override IList<EventProperty> Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the globally unique identifier (GUID) of the event provider that published this event.</summary>
		/// <returns>Returns a GUID value. This value can be null.</returns>
		// Token: 0x17000615 RID: 1557
		// (get) Token: 0x06001DDC RID: 7644 RVA: 0x00054278 File Offset: 0x00052478
		public override Guid? ProviderId
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the name of the event provider that published this event.</summary>
		/// <returns>Returns a string that contains the name of the event provider that published this event.</returns>
		// Token: 0x17000616 RID: 1558
		// (get) Token: 0x06001DDD RID: 7645 RVA: 0x0004B26D File Offset: 0x0004946D
		public override string ProviderName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets qualifier numbers that are used for event identification.</summary>
		/// <returns>Returns an integer value. This value can be null.</returns>
		// Token: 0x17000617 RID: 1559
		// (get) Token: 0x06001DDE RID: 7646 RVA: 0x00054294 File Offset: 0x00052494
		public override int? Qualifiers
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the event record identifier of the event in the log.</summary>
		/// <returns>Returns a long value. This value can be null.</returns>
		// Token: 0x17000618 RID: 1560
		// (get) Token: 0x06001DDF RID: 7647 RVA: 0x000542B0 File Offset: 0x000524B0
		public override long? RecordId
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a globally unique identifier (GUID) for a related activity in a process for which an event is involved.</summary>
		/// <returns>Returns a GUID value. This value can be null.</returns>
		// Token: 0x17000619 RID: 1561
		// (get) Token: 0x06001DE0 RID: 7648 RVA: 0x000542CC File Offset: 0x000524CC
		public override Guid? RelatedActivityId
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a task identifier for a portion of an application or a component that publishes an event. A task is a 16-bit value with 16 top values reserved. This type allows any value between 0x0000 and 0xffef to be used. For the name of the task, get the value of the <see cref="P:System.Diagnostics.Eventing.Reader.EventLogRecord.TaskDisplayName" /> property.</summary>
		/// <returns>Returns an integer value. This value can be null.</returns>
		// Token: 0x1700061A RID: 1562
		// (get) Token: 0x06001DE1 RID: 7649 RVA: 0x000542E8 File Offset: 0x000524E8
		public override int? Task
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the display name of the task for the event.</summary>
		/// <returns>Returns a string that contains the display name of the task for the event.</returns>
		// Token: 0x1700061B RID: 1563
		// (get) Token: 0x06001DE2 RID: 7650 RVA: 0x0004B26D File Offset: 0x0004946D
		public override string TaskDisplayName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the thread identifier for the thread that the event provider is running in.</summary>
		/// <returns>Returns an integer value. This value can be null.</returns>
		// Token: 0x1700061C RID: 1564
		// (get) Token: 0x06001DE3 RID: 7651 RVA: 0x00054304 File Offset: 0x00052504
		public override int? ThreadId
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the time, in <see cref="T:System.DateTime" /> format, that the event was created.</summary>
		/// <returns>Returns a <see cref="T:System.DateTime" /> value. The value can be null.</returns>
		// Token: 0x1700061D RID: 1565
		// (get) Token: 0x06001DE4 RID: 7652 RVA: 0x00054320 File Offset: 0x00052520
		public override DateTime? TimeCreated
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the security descriptor of the user whose context is used to publish the event.</summary>
		/// <returns>Returns a <see cref="T:System.Security.Principal.SecurityIdentifier" /> value.</returns>
		// Token: 0x1700061E RID: 1566
		// (get) Token: 0x06001DE5 RID: 7653 RVA: 0x0004B26D File Offset: 0x0004946D
		public override SecurityIdentifier UserId
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the version number for the event.</summary>
		/// <returns>Returns a byte value. This value can be null.</returns>
		// Token: 0x1700061F RID: 1567
		// (get) Token: 0x06001DE6 RID: 7654 RVA: 0x0005433C File Offset: 0x0005253C
		public override byte? Version
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Releases the unmanaged resources used by this object, and optionally releases the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources.</param>
		// Token: 0x06001DE7 RID: 7655 RVA: 0x0000220F File Offset: 0x0000040F
		[SecuritySafeCritical]
		protected override void Dispose(bool disposing)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the event message in the current locale.</summary>
		/// <returns>Returns a string that contains the event message in the current locale.</returns>
		// Token: 0x06001DE8 RID: 7656 RVA: 0x0004B26D File Offset: 0x0004946D
		public override string FormatDescription()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the event message, replacing variables in the message with the specified values.</summary>
		/// <param name="values">The values used to replace variables in the event message. Variables are represented by %n, where n is a number.</param>
		/// <returns>Returns a string that contains the event message in the current locale.</returns>
		// Token: 0x06001DE9 RID: 7657 RVA: 0x0004B26D File Offset: 0x0004946D
		public override string FormatDescription(IEnumerable<object> values)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the enumeration of the values of the user-supplied event properties, or the results of XPath-based data if the event has XML representation.</summary>
		/// <param name="propertySelector">Selects the property values to return.</param>
		/// <returns>Returns a list of objects.</returns>
		// Token: 0x06001DEA RID: 7658 RVA: 0x0005405B File Offset: 0x0005225B
		public IList<object> GetPropertyValues(EventLogPropertySelector propertySelector)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Gets the XML representation of the event. All of the event properties are represented in the event's XML. The XML conforms to the event schema.</summary>
		/// <returns>Returns a string that contains the XML representation of the event.</returns>
		// Token: 0x06001DEB RID: 7659 RVA: 0x0004B26D File Offset: 0x0004946D
		[SecuritySafeCritical]
		public override string ToXml()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
