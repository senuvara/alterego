﻿using System;
using System.Collections.Generic;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics.Eventing.Reader
{
	/// <summary>Contains the metadata (properties and settings) for an event that is defined in an event provider. </summary>
	// Token: 0x020004D5 RID: 1237
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class EventMetadata
	{
		// Token: 0x06001DF9 RID: 7673 RVA: 0x0000220F File Offset: 0x0000040F
		internal EventMetadata()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the description template associated with the event using the current thread locale for the description language.</summary>
		/// <returns>Returns a string that contains the description template associated with the event.</returns>
		// Token: 0x17000623 RID: 1571
		// (get) Token: 0x06001DFA RID: 7674 RVA: 0x0004B26D File Offset: 0x0004946D
		public string Description
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the identifier of the event that is defined in the event provider.</summary>
		/// <returns>Returns a <see langword="long" /> value that is the event identifier.</returns>
		// Token: 0x17000624 RID: 1572
		// (get) Token: 0x06001DFB RID: 7675 RVA: 0x00054374 File Offset: 0x00052574
		public long Id
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0L;
			}
		}

		/// <summary>Gets the keywords associated with the event that is defined in the even provider.</summary>
		/// <returns>Returns an enumerable collection of <see cref="T:System.Diagnostics.Eventing.Reader.EventKeyword" /> objects.</returns>
		// Token: 0x17000625 RID: 1573
		// (get) Token: 0x06001DFC RID: 7676 RVA: 0x0005405B File Offset: 0x0005225B
		public IEnumerable<EventKeyword> Keywords
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the level associated with the event that is defined in the event provider. The level defines the severity of the event.</summary>
		/// <returns>Returns an <see cref="T:System.Diagnostics.Eventing.Reader.EventLevel" /> object.</returns>
		// Token: 0x17000626 RID: 1574
		// (get) Token: 0x06001DFD RID: 7677 RVA: 0x0004B26D File Offset: 0x0004946D
		public EventLevel Level
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a link to the event log that receives this event when the provider publishes this event.</summary>
		/// <returns>Returns a <see cref="T:System.Diagnostics.Eventing.Reader.EventLogLink" /> object.</returns>
		// Token: 0x17000627 RID: 1575
		// (get) Token: 0x06001DFE RID: 7678 RVA: 0x0004B26D File Offset: 0x0004946D
		public EventLogLink LogLink
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the opcode associated with this event that is defined by an event provider. The opcode defines a numeric value that identifies the activity or a point within an activity that the application was performing when it raised the event.</summary>
		/// <returns>Returns a <see cref="T:System.Diagnostics.Eventing.Reader.EventOpcode" /> object.</returns>
		// Token: 0x17000628 RID: 1576
		// (get) Token: 0x06001DFF RID: 7679 RVA: 0x0004B26D File Offset: 0x0004946D
		public EventOpcode Opcode
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the task associated with the event. A task identifies a portion of an application or a component that publishes an event. </summary>
		/// <returns>Returns a <see cref="T:System.Diagnostics.Eventing.Reader.EventTask" /> object.</returns>
		// Token: 0x17000629 RID: 1577
		// (get) Token: 0x06001E00 RID: 7680 RVA: 0x0004B26D File Offset: 0x0004946D
		public EventTask Task
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the template string for the event. Templates are used to describe data that is used by a provider when an event is published. Templates optionally specify XML that provides the structure of an event. The XML allows values that the event publisher provides to be inserted during the rendering of an event.</summary>
		/// <returns>Returns a string that contains the template for the event.</returns>
		// Token: 0x1700062A RID: 1578
		// (get) Token: 0x06001E01 RID: 7681 RVA: 0x0004B26D File Offset: 0x0004946D
		public string Template
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the version of the event that qualifies the event identifier.</summary>
		/// <returns>Returns a byte value.</returns>
		// Token: 0x1700062B RID: 1579
		// (get) Token: 0x06001E02 RID: 7682 RVA: 0x00054390 File Offset: 0x00052590
		public byte Version
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}
	}
}
