﻿using System;
using System.Collections.Generic;
using System.Security.Permissions;
using System.Security.Principal;
using Unity;

namespace System.Diagnostics.Eventing.Reader
{
	/// <summary>Defines the properties of an event instance for an event that is received from an <see cref="T:System.Diagnostics.Eventing.Reader.EventLogReader" /> object. The event properties provide information about the event such as the name of the computer where the event was logged and the time the event was created. This class is an abstract class. The <see cref="T:System.Diagnostics.Eventing.Reader.EventLogRecord" /> class implements this class.</summary>
	// Token: 0x020004CF RID: 1231
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public abstract class EventRecord : IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Eventing.Reader.EventRecord" /> class.</summary>
		// Token: 0x06001DA8 RID: 7592 RVA: 0x0000220F File Offset: 0x0000040F
		protected EventRecord()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the globally unique identifier (GUID) for the activity in process for which the event is involved. This allows consumers to group related activities.</summary>
		/// <returns>Returns a GUID value.</returns>
		// Token: 0x170005ED RID: 1517
		// (get) Token: 0x06001DA9 RID: 7593
		public abstract Guid? ActivityId { get; }

		/// <summary>Gets a placeholder (bookmark) that corresponds to this event. This can be used as a placeholder in a stream of events.</summary>
		/// <returns>Returns a <see cref="T:System.Diagnostics.Eventing.Reader.EventBookmark" /> object.</returns>
		// Token: 0x170005EE RID: 1518
		// (get) Token: 0x06001DAA RID: 7594
		public abstract EventBookmark Bookmark { get; }

		/// <summary>Gets the identifier for this event. All events with this identifier value represent the same type of event.</summary>
		/// <returns>Returns an integer value. This value can be null.</returns>
		// Token: 0x170005EF RID: 1519
		// (get) Token: 0x06001DAB RID: 7595
		public abstract int Id { get; }

		/// <summary>Gets the keyword mask of the event. Get the value of the <see cref="P:System.Diagnostics.Eventing.Reader.EventRecord.KeywordsDisplayNames" /> property to get the name of the keywords used in this mask.</summary>
		/// <returns>Returns a long value. This value can be null.</returns>
		// Token: 0x170005F0 RID: 1520
		// (get) Token: 0x06001DAC RID: 7596
		public abstract long? Keywords { get; }

		/// <summary>Gets the display names of the keywords used in the keyword mask for this event. </summary>
		/// <returns>Returns an enumerable collection of strings that contain the display names of the keywords used in the keyword mask for this event.</returns>
		// Token: 0x170005F1 RID: 1521
		// (get) Token: 0x06001DAD RID: 7597
		public abstract IEnumerable<string> KeywordsDisplayNames { get; }

		/// <summary>Gets the level of the event. The level signifies the severity of the event. For the name of the level, get the value of the <see cref="P:System.Diagnostics.Eventing.Reader.EventRecord.LevelDisplayName" /> property.</summary>
		/// <returns>Returns a byte value. This value can be null.</returns>
		// Token: 0x170005F2 RID: 1522
		// (get) Token: 0x06001DAE RID: 7598
		public abstract byte? Level { get; }

		/// <summary>Gets the display name of the level for this event.</summary>
		/// <returns>Returns a string that contains the display name of the level for this event.</returns>
		// Token: 0x170005F3 RID: 1523
		// (get) Token: 0x06001DAF RID: 7599
		public abstract string LevelDisplayName { get; }

		/// <summary>Gets the name of the event log where this event is logged.</summary>
		/// <returns>Returns a string that contains a name of the event log that contains this event.</returns>
		// Token: 0x170005F4 RID: 1524
		// (get) Token: 0x06001DB0 RID: 7600
		public abstract string LogName { get; }

		/// <summary>Gets the name of the computer on which this event was logged.</summary>
		/// <returns>Returns a string that contains the name of the computer on which this event was logged.</returns>
		// Token: 0x170005F5 RID: 1525
		// (get) Token: 0x06001DB1 RID: 7601
		public abstract string MachineName { get; }

		/// <summary>Gets the opcode of the event. The opcode defines a numeric value that identifies the activity or a point within an activity that the application was performing when it raised the event. For the name of the opcode, get the value of the <see cref="P:System.Diagnostics.Eventing.Reader.EventRecord.OpcodeDisplayName" /> property.</summary>
		/// <returns>Returns a short value. This value can be null.</returns>
		// Token: 0x170005F6 RID: 1526
		// (get) Token: 0x06001DB2 RID: 7602
		public abstract short? Opcode { get; }

		/// <summary>Gets the display name of the opcode for this event.</summary>
		/// <returns>Returns a string that contains the display name of the opcode for this event.</returns>
		// Token: 0x170005F7 RID: 1527
		// (get) Token: 0x06001DB3 RID: 7603
		public abstract string OpcodeDisplayName { get; }

		/// <summary>Gets the process identifier for the event provider that logged this event.</summary>
		/// <returns>Returns an integer value. This value can be null.</returns>
		// Token: 0x170005F8 RID: 1528
		// (get) Token: 0x06001DB4 RID: 7604
		public abstract int? ProcessId { get; }

		/// <summary>Gets the user-supplied properties of the event.</summary>
		/// <returns>Returns a list of <see cref="T:System.Diagnostics.Eventing.Reader.EventProperty" /> objects.</returns>
		// Token: 0x170005F9 RID: 1529
		// (get) Token: 0x06001DB5 RID: 7605
		public abstract IList<EventProperty> Properties { get; }

		/// <summary>Gets the globally unique identifier (GUID) of the event provider that published this event.</summary>
		/// <returns>Returns a GUID value. This value can be null.</returns>
		// Token: 0x170005FA RID: 1530
		// (get) Token: 0x06001DB6 RID: 7606
		public abstract Guid? ProviderId { get; }

		/// <summary>Gets the name of the event provider that published this event.</summary>
		/// <returns>Returns a string that contains the name of the event provider that published this event.</returns>
		// Token: 0x170005FB RID: 1531
		// (get) Token: 0x06001DB7 RID: 7607
		public abstract string ProviderName { get; }

		/// <summary>Gets qualifier numbers that are used for event identification.</summary>
		/// <returns>Returns an integer value. This value can be null.</returns>
		// Token: 0x170005FC RID: 1532
		// (get) Token: 0x06001DB8 RID: 7608
		public abstract int? Qualifiers { get; }

		/// <summary>Gets the event record identifier of the event in the log.</summary>
		/// <returns>Returns a long value. This value can be null.</returns>
		// Token: 0x170005FD RID: 1533
		// (get) Token: 0x06001DB9 RID: 7609
		public abstract long? RecordId { get; }

		/// <summary>Gets a globally unique identifier (GUID) for a related activity in a process for which an event is involved.</summary>
		/// <returns>Returns a GUID value. This value can be null.</returns>
		// Token: 0x170005FE RID: 1534
		// (get) Token: 0x06001DBA RID: 7610
		public abstract Guid? RelatedActivityId { get; }

		/// <summary>Gets a task identifier for a portion of an application or a component that publishes an event. A task is a 16-bit value with 16 top values reserved. This type allows any value between 0x0000 and 0xffef to be used. To obtain the task name, get the value of the <see cref="P:System.Diagnostics.Eventing.Reader.EventRecord.TaskDisplayName" /> property.</summary>
		/// <returns>Returns an integer value. This value can be null.</returns>
		// Token: 0x170005FF RID: 1535
		// (get) Token: 0x06001DBB RID: 7611
		public abstract int? Task { get; }

		/// <summary>Gets the display name of the task for the event.</summary>
		/// <returns>Returns a string that contains the display name of the task for the event.</returns>
		// Token: 0x17000600 RID: 1536
		// (get) Token: 0x06001DBC RID: 7612
		public abstract string TaskDisplayName { get; }

		/// <summary>Gets the thread identifier for the thread that the event provider is running in.</summary>
		/// <returns>Returns an integer value. This value can be null.</returns>
		// Token: 0x17000601 RID: 1537
		// (get) Token: 0x06001DBD RID: 7613
		public abstract int? ThreadId { get; }

		/// <summary>Gets the time, in <see cref="T:System.DateTime" /> format, that the event was created.</summary>
		/// <returns>Returns a <see cref="T:System.DateTime" /> value. The value can be null.</returns>
		// Token: 0x17000602 RID: 1538
		// (get) Token: 0x06001DBE RID: 7614
		public abstract DateTime? TimeCreated { get; }

		/// <summary>Gets the security descriptor of the user whose context is used to publish the event.</summary>
		/// <returns>Returns a <see cref="T:System.Security.Principal.SecurityIdentifier" /> value.</returns>
		// Token: 0x17000603 RID: 1539
		// (get) Token: 0x06001DBF RID: 7615
		public abstract SecurityIdentifier UserId { get; }

		/// <summary>Gets the version number for the event.</summary>
		/// <returns>Returns a byte value. This value can be null.</returns>
		// Token: 0x17000604 RID: 1540
		// (get) Token: 0x06001DC0 RID: 7616
		public abstract byte? Version { get; }

		/// <summary>Releases all the resources used by this object.</summary>
		// Token: 0x06001DC1 RID: 7617 RVA: 0x0000220F File Offset: 0x0000040F
		public void Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Releases the unmanaged resources used by this object, and optionally releases the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources.</param>
		// Token: 0x06001DC2 RID: 7618 RVA: 0x0000220F File Offset: 0x0000040F
		protected virtual void Dispose(bool disposing)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the event message in the current locale.</summary>
		/// <returns>Returns a string that contains the event message in the current locale.</returns>
		// Token: 0x06001DC3 RID: 7619
		public abstract string FormatDescription();

		/// <summary>Gets the event message, replacing variables in the message with the specified values.</summary>
		/// <param name="values">The values used to replace variables in the event message. Variables are represented by %n, where n is a number.</param>
		/// <returns>Returns a string that contains the event message in the current locale.</returns>
		// Token: 0x06001DC4 RID: 7620
		public abstract string FormatDescription(IEnumerable<object> values);

		/// <summary>Gets the XML representation of the event. All of the event properties are represented in the event XML. The XML conforms to the event schema.</summary>
		/// <returns>Returns a string that contains the XML representation of the event.</returns>
		// Token: 0x06001DC5 RID: 7621
		public abstract string ToXml();
	}
}
