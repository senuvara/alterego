﻿using System;

namespace System.Diagnostics.Eventing.Reader
{
	/// <summary>Defines the standard event levels that are used in the Event Log service. The level defines the severity of the event. Custom event levels can be defined beyond these standard levels. For more information about levels, see <see cref="T:System.Diagnostics.Eventing.Reader.EventLevel" />.</summary>
	// Token: 0x020004DA RID: 1242
	public enum StandardEventLevel
	{
		/// <summary>This level corresponds to critical errors, which is a serious error that has caused a major failure. </summary>
		// Token: 0x04000D88 RID: 3464
		Critical = 1,
		/// <summary>This level corresponds to normal errors that signify a problem. </summary>
		// Token: 0x04000D89 RID: 3465
		Error,
		/// <summary>This level corresponds to informational events or messages that are not errors. These events can help trace the progress or state of an application.</summary>
		// Token: 0x04000D8A RID: 3466
		Informational = 4,
		/// <summary>This value indicates that not filtering on the level is done during the event publishing.</summary>
		// Token: 0x04000D8B RID: 3467
		LogAlways = 0,
		/// <summary>This level corresponds to lengthy events or messages. </summary>
		// Token: 0x04000D8C RID: 3468
		Verbose = 5,
		/// <summary>This level corresponds to warning events. For example, an event that gets published because a disk is nearing full capacity is a warning event.</summary>
		// Token: 0x04000D8D RID: 3469
		Warning = 3
	}
}
