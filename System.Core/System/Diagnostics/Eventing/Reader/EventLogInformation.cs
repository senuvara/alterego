﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics.Eventing.Reader
{
	/// <summary>Allows you to access the run-time properties of active event logs and event log files. These properties include the number of events in the log, the size of the log, a value that determines whether the log is full, and the last time the log was written to or accessed.</summary>
	// Token: 0x020004C2 RID: 1218
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class EventLogInformation
	{
		// Token: 0x06001D6D RID: 7533 RVA: 0x0000220F File Offset: 0x0000040F
		internal EventLogInformation()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the file attributes of the log file associated with the log.</summary>
		/// <returns>Returns an integer value. This value can be null.</returns>
		// Token: 0x170005DB RID: 1499
		// (get) Token: 0x06001D6E RID: 7534 RVA: 0x00054064 File Offset: 0x00052264
		public int? Attributes
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the time that the log file associated with the event log was created.</summary>
		/// <returns>Returns a <see cref="T:System.DateTime" /> object. This value can be null.</returns>
		// Token: 0x170005DC RID: 1500
		// (get) Token: 0x06001D6F RID: 7535 RVA: 0x00054080 File Offset: 0x00052280
		public DateTime? CreationTime
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the size of the file, in bytes, associated with the event log.</summary>
		/// <returns>Returns a long value.</returns>
		// Token: 0x170005DD RID: 1501
		// (get) Token: 0x06001D70 RID: 7536 RVA: 0x0005409C File Offset: 0x0005229C
		public long? FileSize
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a Boolean value that determines whether the log file has reached its maximum size (the log is full).</summary>
		/// <returns>Returns <see langword="true" /> if the log is full, and returns <see langword="false" /> if the log is not full.</returns>
		// Token: 0x170005DE RID: 1502
		// (get) Token: 0x06001D71 RID: 7537 RVA: 0x000540B8 File Offset: 0x000522B8
		public bool? IsLogFull
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the last time the log file associated with the event log was accessed.</summary>
		/// <returns>Returns a <see cref="T:System.DateTime" /> object. This value can be null.</returns>
		// Token: 0x170005DF RID: 1503
		// (get) Token: 0x06001D72 RID: 7538 RVA: 0x000540D4 File Offset: 0x000522D4
		public DateTime? LastAccessTime
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the last time data was written to the log file associated with the event log.</summary>
		/// <returns>Returns a <see cref="T:System.DateTime" /> object. This value can be null.</returns>
		// Token: 0x170005E0 RID: 1504
		// (get) Token: 0x06001D73 RID: 7539 RVA: 0x000540F0 File Offset: 0x000522F0
		public DateTime? LastWriteTime
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the number of the oldest event record in the event log.</summary>
		/// <returns>Returns a long value that represents the number of the oldest event record in the event log. This value can be null.</returns>
		// Token: 0x170005E1 RID: 1505
		// (get) Token: 0x06001D74 RID: 7540 RVA: 0x0005410C File Offset: 0x0005230C
		public long? OldestRecordNumber
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the number of event records in the event log.</summary>
		/// <returns>Returns a long value that represents the number of event records in the event log. This value can be null.</returns>
		// Token: 0x170005E2 RID: 1506
		// (get) Token: 0x06001D75 RID: 7541 RVA: 0x00054128 File Offset: 0x00052328
		public long? RecordCount
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
