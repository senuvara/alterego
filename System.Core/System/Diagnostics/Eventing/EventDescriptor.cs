﻿using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics.Eventing
{
	/// <summary>Contains the metadata that defines an event.</summary>
	// Token: 0x020004B7 RID: 1207
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	[StructLayout(LayoutKind.Explicit, Size = 16)]
	public struct EventDescriptor
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Eventing.EventDescriptor" /> class.</summary>
		/// <param name="id">The event identifier.</param>
		/// <param name="version">Version of the event. The version indicates a revision to the event definition. You can use this member and the Id member to identify a unique event.</param>
		/// <param name="channel">Defines a potential target for the event.</param>
		/// <param name="level">Specifies the level of detail included in the event.</param>
		/// <param name="opcode">Operation being performed at the time the event is written.</param>
		/// <param name="task">Identifies a logical component of the application that is writing the event.</param>
		/// <param name="keywords">Bit mask that specifies the event category. The keyword can contain one or more provider-defined keywords, standard keywords, or both.</param>
		// Token: 0x06001D15 RID: 7445 RVA: 0x0000220F File Offset: 0x0000040F
		public EventDescriptor(int id, byte version, byte channel, byte level, byte opcode, int task, long keywords)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves the channel value from the event descriptor.</summary>
		/// <returns>The channel that defines a potential target for the event.</returns>
		// Token: 0x170005BA RID: 1466
		// (get) Token: 0x06001D16 RID: 7446 RVA: 0x00053CC0 File Offset: 0x00051EC0
		public byte Channel
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Retrieves the event identifier value from the event descriptor.</summary>
		/// <returns>The event identifier.</returns>
		// Token: 0x170005BB RID: 1467
		// (get) Token: 0x06001D17 RID: 7447 RVA: 0x00053CDC File Offset: 0x00051EDC
		public int EventId
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Retrieves the keyword value from the event descriptor.</summary>
		/// <returns>The keyword, which is a bit mask, that specifies the event category.</returns>
		// Token: 0x170005BC RID: 1468
		// (get) Token: 0x06001D18 RID: 7448 RVA: 0x00053CF8 File Offset: 0x00051EF8
		public long Keywords
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0L;
			}
		}

		/// <summary>Retrieves the level value from the event descriptor.</summary>
		/// <returns>The level of detail included in the event.</returns>
		// Token: 0x170005BD RID: 1469
		// (get) Token: 0x06001D19 RID: 7449 RVA: 0x00053D14 File Offset: 0x00051F14
		public byte Level
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Retrieves the operation code value from the event descriptor.</summary>
		/// <returns>The operation being performed at the time the event is written.</returns>
		// Token: 0x170005BE RID: 1470
		// (get) Token: 0x06001D1A RID: 7450 RVA: 0x00053D30 File Offset: 0x00051F30
		public byte Opcode
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Retrieves the task value from the event descriptor.</summary>
		/// <returns>The task that identifies the logical component of the application that is writing the event.</returns>
		// Token: 0x170005BF RID: 1471
		// (get) Token: 0x06001D1B RID: 7451 RVA: 0x00053D4C File Offset: 0x00051F4C
		public int Task
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Retrieves the version value from the event descriptor.</summary>
		/// <returns>The version of the event. </returns>
		// Token: 0x170005C0 RID: 1472
		// (get) Token: 0x06001D1C RID: 7452 RVA: 0x00053D68 File Offset: 0x00051F68
		public byte Version
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}
	}
}
