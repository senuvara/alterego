﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics.Eventing
{
	/// <summary>A listener for <see cref="T:System.Diagnostics.TraceSource" /> that writes events to the ETW subsytem. </summary>
	// Token: 0x020004BA RID: 1210
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public class EventProviderTraceListener : TraceListener
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Eventing.EventProviderTraceListener" /> class using the specified provider identifier.</summary>
		/// <param name="providerId">A unique string <see cref="T:System.Guid" /> that identifies the provider.</param>
		// Token: 0x06001D2D RID: 7469 RVA: 0x0000220F File Offset: 0x0000040F
		public EventProviderTraceListener(string providerId)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Eventing.EventProviderTraceListener" /> class using the specified provider identifier and name of the listener.</summary>
		/// <param name="providerId">A unique string <see cref="T:System.Guid" /> that identifies the provider.</param>
		/// <param name="name">Name of the listener.</param>
		// Token: 0x06001D2E RID: 7470 RVA: 0x0000220F File Offset: 0x0000040F
		public EventProviderTraceListener(string providerId, string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Eventing.EventProviderTraceListener" /> class using the specified provider identifier, name of the listener, and delimiter.</summary>
		/// <param name="providerId">A unique string <see cref="T:System.Guid" /> that identifies the provider.</param>
		/// <param name="name">Name of the listener.</param>
		/// <param name="delimiter">Delimiter used to delimit the event data. (For more details, see the <see cref="P:System.Diagnostics.Eventing.EventProviderTraceListener.Delimiter" /> property.)</param>
		// Token: 0x06001D2F RID: 7471 RVA: 0x0000220F File Offset: 0x0000040F
		public EventProviderTraceListener(string providerId, string name, string delimiter)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets and sets the delimiter used to delimit the event data that is written to the ETW subsystem.</summary>
		/// <returns>The delimiter used to delimit the event data. The default delimiter is a comma.</returns>
		// Token: 0x170005C1 RID: 1473
		// (get) Token: 0x06001D30 RID: 7472 RVA: 0x0004B26D File Offset: 0x0004946D
		// (set) Token: 0x06001D31 RID: 7473 RVA: 0x0000220F File Offset: 0x0000040F
		public string Delimiter
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>When overridden in a derived class, writes the specified message to the listener you create in the derived class.</summary>
		/// <param name="message">A message to write.</param>
		// Token: 0x06001D32 RID: 7474 RVA: 0x0000220F File Offset: 0x0000040F
		public sealed override void Write(string message)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>When overridden in a derived class, writes a message to the listener you create in the derived class, followed by a line terminator.</summary>
		/// <param name="message">A message to write. </param>
		// Token: 0x06001D33 RID: 7475 RVA: 0x0000220F File Offset: 0x0000040F
		public sealed override void WriteLine(string message)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
