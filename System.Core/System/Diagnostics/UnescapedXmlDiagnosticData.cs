﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Provides unescaped XML data for the logging of user-provided trace data.</summary>
	// Token: 0x020004B0 RID: 1200
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public class UnescapedXmlDiagnosticData
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.UnescapedXmlDiagnosticData" /> class by using the specified XML data string.</summary>
		/// <param name="xmlPayload">The XML data to be logged in the <see langword="UserData" /> node of the event schema.  </param>
		// Token: 0x06001CFD RID: 7421 RVA: 0x0000220F File Offset: 0x0000040F
		public UnescapedXmlDiagnosticData(string xmlPayload)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the unescaped XML data string.</summary>
		/// <returns>An unescaped XML string.</returns>
		// Token: 0x170005B5 RID: 1461
		// (get) Token: 0x06001CFE RID: 7422 RVA: 0x0004B26D File Offset: 0x0004946D
		// (set) Token: 0x06001CFF RID: 7423 RVA: 0x0000220F File Offset: 0x0000040F
		public string UnescapedXml
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
