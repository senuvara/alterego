﻿using System;

namespace System.Diagnostics
{
	/// <summary>Specifies the file structure that will be used for the <see cref="T:System.Diagnostics.EventSchemaTraceListener" /> log.</summary>
	// Token: 0x020004AF RID: 1199
	public enum TraceLogRetentionOption
	{
		/// <summary>A finite number of sequential files, each with a maximum file size. When the <see cref="P:System.Diagnostics.EventSchemaTraceListener.MaximumFileSize" /> property value is reached, writing starts in a new file with an incremented integer suffix. When the <see cref="P:System.Diagnostics.EventSchemaTraceListener.MaximumNumberOfFiles" /> property value is reached, the first file is cleared and overwritten. Files are then incrementally overwritten in a circular manner.</summary>
		// Token: 0x04000D32 RID: 3378
		LimitedCircularFiles = 1,
		/// <summary>A finite number of sequential files, each with a maximum file size. When the <see cref="P:System.Diagnostics.EventSchemaTraceListener.MaximumFileSize" /> property value is reached, writing starts in a new file with an incremented integer suffix.</summary>
		// Token: 0x04000D33 RID: 3379
		LimitedSequentialFiles = 3,
		/// <summary>One file with a maximum file size that is determined by the <see cref="P:System.Diagnostics.EventSchemaTraceListener.MaximumFileSize" /> property.</summary>
		// Token: 0x04000D34 RID: 3380
		SingleFileBoundedSize,
		/// <summary>One file with no maximum file size restriction.</summary>
		// Token: 0x04000D35 RID: 3381
		SingleFileUnboundedSize = 2,
		/// <summary>An unlimited number of sequential files, each with a maximum file size that is determined by the <see cref="P:System.Diagnostics.EventSchemaTraceListener.MaximumFileSize" /> property. There is no logical bound to the number or size of the files, but it is limited by the physical constraints imposed by the computer.</summary>
		// Token: 0x04000D36 RID: 3382
		UnlimitedSequentialFiles = 0
	}
}
