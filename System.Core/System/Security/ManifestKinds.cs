﻿using System;

namespace System.Security
{
	/// <summary>Represents the type of manifest that the signature information applies to.</summary>
	// Token: 0x02000489 RID: 1161
	[Flags]
	public enum ManifestKinds
	{
		/// <summary>The manifest is for an application. </summary>
		// Token: 0x04000CFC RID: 3324
		Application = 2,
		/// <summary>The manifest is for deployment and application. The is the default value for verifying signatures. </summary>
		// Token: 0x04000CFD RID: 3325
		ApplicationAndDeployment = 3,
		/// <summary>The manifest is for deployment only.</summary>
		// Token: 0x04000CFE RID: 3326
		Deployment = 1,
		/// <summary>The manifest is of no particular type. </summary>
		// Token: 0x04000CFF RID: 3327
		None = 0
	}
}
