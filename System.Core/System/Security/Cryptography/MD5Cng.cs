﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Security.Cryptography
{
	/// <summary>Provides a CNG (Cryptography Next Generation) implementation of the MD5 (Message Digest 5) 128-bit hashing algorithm.</summary>
	// Token: 0x02000494 RID: 1172
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class MD5Cng : MD5
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.MD5Cng" /> class. </summary>
		/// <exception cref="T:System.InvalidOperationException">This implementation is not part of the Windows Platform FIPS-validated cryptographic algorithms.</exception>
		// Token: 0x06001C9D RID: 7325 RVA: 0x0000220F File Offset: 0x0000040F
		public MD5Cng()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06001C9E RID: 7326 RVA: 0x0000220F File Offset: 0x0000040F
		protected override void HashCore(byte[] array, int ibStart, int cbSize)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06001C9F RID: 7327 RVA: 0x0004B26D File Offset: 0x0004946D
		protected override byte[] HashFinal()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Initializes, or re-initializes, the instance of the hash algorithm. </summary>
		// Token: 0x06001CA0 RID: 7328 RVA: 0x0000220F File Offset: 0x0000040F
		public override void Initialize()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
