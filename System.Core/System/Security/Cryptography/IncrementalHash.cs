﻿using System;
using Unity;

namespace System.Security.Cryptography
{
	/// <summary>Provides support for computing a hash or Hash-based Message Authentication Code (HMAC) value incrementally across several segments.</summary>
	// Token: 0x02000044 RID: 68
	public sealed class IncrementalHash : IDisposable
	{
		// Token: 0x0600017C RID: 380 RVA: 0x00004415 File Offset: 0x00002615
		private IncrementalHash(HashAlgorithmName name, HashAlgorithm hash)
		{
			this._algorithmName = name;
			this._hash = hash;
		}

		/// <summary>Gets the name of the algorithm being performed.</summary>
		/// <returns>The name of the algorithm being performed.</returns>
		// Token: 0x1700003A RID: 58
		// (get) Token: 0x0600017D RID: 381 RVA: 0x0000442B File Offset: 0x0000262B
		public HashAlgorithmName AlgorithmName
		{
			get
			{
				return this._algorithmName;
			}
		}

		/// <summary>Appends the specified data to the data already processed in the hash or HMAC.</summary>
		/// <param name="data">The data to process.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="data" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Security.Cryptography.IncrementalHash" /> object has already been disposed.</exception>
		// Token: 0x0600017E RID: 382 RVA: 0x00004433 File Offset: 0x00002633
		public void AppendData(byte[] data)
		{
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			this.AppendData(data, 0, data.Length);
		}

		/// <summary>Appends the specified number of bytes from the specified data, starting at the specified offset, to the data already processed in the hash or Hash-based Message Authentication Code (HMAC).</summary>
		/// <param name="data">The data to process.</param>
		/// <param name="offset">The offset into the byte array from which to begin using data.</param>
		/// <param name="count">The number of bytes to use from <paramref name="data" />.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="data" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="count" /> or <paramref name="offset" /> is negative.-or-<paramref name="count" /> is larger than the lenght of <paramref name="data" />.</exception>
		/// <exception cref="T:System.ArgumentException">The sum of <paramref name="offset" /> and <paramref name="count" /> is larger than the data length.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Security.Cryptography.IncrementalHash" /> object has already been disposed.</exception>
		// Token: 0x0600017F RID: 383 RVA: 0x00004450 File Offset: 0x00002650
		public void AppendData(byte[] data, int offset, int count)
		{
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", "Non negative number is required.");
			}
			if (count < 0 || count > data.Length)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (data.Length - count < offset)
			{
				throw new ArgumentException("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection.");
			}
			if (this._disposed)
			{
				throw new ObjectDisposedException(typeof(IncrementalHash).Name);
			}
			if (this._resetPending)
			{
				this._hash.Initialize();
				this._resetPending = false;
			}
			this._hash.TransformBlock(data, offset, count, null, 0);
		}

		/// <summary>Retrieves the hash or Hash-based Message Authentication Code (HMAC) for the data accumulated from prior calls to the
		///   <see cref="M:System.Security.Cryptography.IncrementalHash.AppendData(System.Byte[])" /> method,  and resets the object to its initial state.</summary>
		/// <returns>The computed hash or HMAC.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Security.Cryptography.IncrementalHash" /> object has already been disposed.</exception>
		// Token: 0x06000180 RID: 384 RVA: 0x000044F0 File Offset: 0x000026F0
		public byte[] GetHashAndReset()
		{
			if (this._disposed)
			{
				throw new ObjectDisposedException(typeof(IncrementalHash).Name);
			}
			if (this._resetPending)
			{
				this._hash.Initialize();
			}
			this._hash.TransformFinalBlock(Array.Empty<byte>(), 0, 0);
			byte[] hash = this._hash.Hash;
			this._resetPending = true;
			return hash;
		}

		/// <summary>Releases the resources used by the current instance of the <see cref="T:System.Security.Cryptography.IncrementalHash" /> class.</summary>
		// Token: 0x06000181 RID: 385 RVA: 0x00004552 File Offset: 0x00002752
		public void Dispose()
		{
			this._disposed = true;
			if (this._hash != null)
			{
				this._hash.Dispose();
				this._hash = null;
			}
		}

		/// <summary>Creates an <see cref="T:System.Security.Cryptography.IncrementalHash" /> for the specified algorithm.</summary>
		/// <param name="hashAlgorithm">The name of the hash algorithm to perform.</param>
		/// <returns>An <see cref="T:System.Security.Cryptography.IncrementalHash" /> instance ready to compute the hash algorithm specified by <paramref name="hashAlgorithm" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="hashAlgorithm" />.<see cref="P:System.Security.Cryptography.HashAlgorithmName.Name" /> is <see langword="null" /> or an empty string.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">
		///   <paramref name="hashAlgorithm" /> is not a known hash algorithm.</exception>
		// Token: 0x06000182 RID: 386 RVA: 0x00004575 File Offset: 0x00002775
		public static IncrementalHash CreateHash(HashAlgorithmName hashAlgorithm)
		{
			if (string.IsNullOrEmpty(hashAlgorithm.Name))
			{
				throw new ArgumentException("The hash algorithm name cannot be null or empty.", "hashAlgorithm");
			}
			return new IncrementalHash(hashAlgorithm, IncrementalHash.GetHashAlgorithm(hashAlgorithm));
		}

		/// <summary>Creates an <see cref="T:System.Security.Cryptography.IncrementalHash" /> for the Hash-based Message Authentication Code (HMAC)
		///   algorithm using the specified hash algorithm and key.</summary>
		/// <param name="hashAlgorithm">The name of the hash algorithm to perform within the HMAC.</param>
		/// <param name="key">     The secret key for the HMAC. The key can be of any length, but a key longer than the output size
		///   of the specified hash algorithm will be hashed to derive a correctly-sized key. Therefore,
		///   the recommended size of the secret key is the output size of the specified hash algorithm.</param>
		/// <returns>An instance of the <see cref="T:System.Security.Cryptography.IncrementalHash" /> class ready to compute the specified hash algorithm.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="key" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="hashAlgorithm" />.<see cref="P:System.Security.Cryptography.HashAlgorithmName.Name" /> is <see langword="null" /> or
		///   an empty string.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">
		///   <paramref name="hashAlgorithm" /> is not a known hash algorithm.</exception>
		// Token: 0x06000183 RID: 387 RVA: 0x000045A1 File Offset: 0x000027A1
		public static IncrementalHash CreateHMAC(HashAlgorithmName hashAlgorithm, byte[] key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			if (string.IsNullOrEmpty(hashAlgorithm.Name))
			{
				throw new ArgumentException("The hash algorithm name cannot be null or empty.", "hashAlgorithm");
			}
			return new IncrementalHash(hashAlgorithm, IncrementalHash.GetHMAC(hashAlgorithm, key));
		}

		// Token: 0x06000184 RID: 388 RVA: 0x000045DC File Offset: 0x000027DC
		private static HashAlgorithm GetHashAlgorithm(HashAlgorithmName hashAlgorithm)
		{
			if (hashAlgorithm == HashAlgorithmName.MD5)
			{
				return new MD5CryptoServiceProvider();
			}
			if (hashAlgorithm == HashAlgorithmName.SHA1)
			{
				return new SHA1CryptoServiceProvider();
			}
			if (hashAlgorithm == HashAlgorithmName.SHA256)
			{
				return new SHA256CryptoServiceProvider();
			}
			if (hashAlgorithm == HashAlgorithmName.SHA384)
			{
				return new SHA384CryptoServiceProvider();
			}
			if (hashAlgorithm == HashAlgorithmName.SHA512)
			{
				return new SHA512CryptoServiceProvider();
			}
			throw new CryptographicException(-2146893816);
		}

		// Token: 0x06000185 RID: 389 RVA: 0x00004654 File Offset: 0x00002854
		private static HashAlgorithm GetHMAC(HashAlgorithmName hashAlgorithm, byte[] key)
		{
			if (hashAlgorithm == HashAlgorithmName.MD5)
			{
				return new HMACMD5(key);
			}
			if (hashAlgorithm == HashAlgorithmName.SHA1)
			{
				return new HMACSHA1(key);
			}
			if (hashAlgorithm == HashAlgorithmName.SHA256)
			{
				return new HMACSHA256(key);
			}
			if (hashAlgorithm == HashAlgorithmName.SHA384)
			{
				return new HMACSHA384(key);
			}
			if (hashAlgorithm == HashAlgorithmName.SHA512)
			{
				return new HMACSHA512(key);
			}
			throw new CryptographicException(-2146893816);
		}

		// Token: 0x06000186 RID: 390 RVA: 0x0000220F File Offset: 0x0000040F
		internal IncrementalHash()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000246 RID: 582
		private const int NTE_BAD_ALGID = -2146893816;

		// Token: 0x04000247 RID: 583
		private readonly HashAlgorithmName _algorithmName;

		// Token: 0x04000248 RID: 584
		private HashAlgorithm _hash;

		// Token: 0x04000249 RID: 585
		private bool _disposed;

		// Token: 0x0400024A RID: 586
		private bool _resetPending;
	}
}
