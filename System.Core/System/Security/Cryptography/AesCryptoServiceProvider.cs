﻿using System;
using System.Security.Permissions;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	/// <summary>Performs symmetric encryption and decryption using the Cryptographic Application Programming Interfaces (CAPI) implementation of the Advanced Encryption Standard (AES) algorithm. </summary>
	// Token: 0x02000063 RID: 99
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class AesCryptoServiceProvider : Aes
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.AesCryptoServiceProvider" /> class. </summary>
		/// <exception cref="T:System.PlatformNotSupportedException">There is no supported key size for the current platform.</exception>
		// Token: 0x06000279 RID: 633 RVA: 0x000057BA File Offset: 0x000039BA
		public AesCryptoServiceProvider()
		{
			this.FeedbackSizeValue = 8;
		}

		/// <summary>Generates a random initialization vector (IV) to use for the algorithm.</summary>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The initialization vector (IV) could not be generated. </exception>
		// Token: 0x0600027A RID: 634 RVA: 0x000057C9 File Offset: 0x000039C9
		public override void GenerateIV()
		{
			this.IVValue = KeyBuilder.IV(this.BlockSizeValue >> 3);
		}

		/// <summary>Generates a random key to use for the algorithm. </summary>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The key could not be generated.</exception>
		// Token: 0x0600027B RID: 635 RVA: 0x000057DE File Offset: 0x000039DE
		public override void GenerateKey()
		{
			this.KeyValue = KeyBuilder.Key(this.KeySizeValue >> 3);
		}

		/// <summary>Creates a symmetric AES decryptor object using the specified key and initialization vector (IV).</summary>
		/// <param name="key">The secret key to use for the symmetric algorithm.</param>
		/// <param name="iv">The initialization vector to use for the symmetric algorithm.</param>
		/// <returns>A symmetric AES decryptor object.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="key" /> or <paramref name="iv" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="key" /> is invalid.</exception>
		// Token: 0x0600027C RID: 636 RVA: 0x000057F3 File Offset: 0x000039F3
		public override ICryptoTransform CreateDecryptor(byte[] key, byte[] iv)
		{
			if (this.Mode == CipherMode.CFB && this.FeedbackSize > 64)
			{
				throw new CryptographicException("CFB with Feedbaack > 64 bits");
			}
			return new AesTransform(this, false, key, iv);
		}

		/// <summary>Creates a symmetric encryptor object using the specified key and initialization vector (IV).</summary>
		/// <param name="key">The secret key to use for the symmetric algorithm.</param>
		/// <param name="iv">The initialization vector to use for the symmetric algorithm.</param>
		/// <returns>A symmetric AES encryptor object.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="key" /> or <paramref name="iv" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="key" /> is invalid.</exception>
		// Token: 0x0600027D RID: 637 RVA: 0x0000581C File Offset: 0x00003A1C
		public override ICryptoTransform CreateEncryptor(byte[] key, byte[] iv)
		{
			if (this.Mode == CipherMode.CFB && this.FeedbackSize > 64)
			{
				throw new CryptographicException("CFB with Feedbaack > 64 bits");
			}
			return new AesTransform(this, true, key, iv);
		}

		// Token: 0x1700008A RID: 138
		// (get) Token: 0x0600027E RID: 638 RVA: 0x00005845 File Offset: 0x00003A45
		// (set) Token: 0x0600027F RID: 639 RVA: 0x0000584D File Offset: 0x00003A4D
		public override byte[] IV
		{
			get
			{
				return base.IV;
			}
			set
			{
				base.IV = value;
			}
		}

		/// <summary>Gets or sets the symmetric key that is used for encryption and decryption.</summary>
		/// <returns>The symmetric key that is used for encryption and decryption.</returns>
		/// <exception cref="T:System.ArgumentNullException">The value for the key is <see langword="null" />.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The size of the key is invalid.</exception>
		// Token: 0x1700008B RID: 139
		// (get) Token: 0x06000280 RID: 640 RVA: 0x00005856 File Offset: 0x00003A56
		// (set) Token: 0x06000281 RID: 641 RVA: 0x0000585E File Offset: 0x00003A5E
		public override byte[] Key
		{
			get
			{
				return base.Key;
			}
			set
			{
				base.Key = value;
			}
		}

		/// <summary>Gets or sets the size, in bits, of the secret key. </summary>
		/// <returns>The size, in bits, of the key.</returns>
		// Token: 0x1700008C RID: 140
		// (get) Token: 0x06000282 RID: 642 RVA: 0x00005867 File Offset: 0x00003A67
		// (set) Token: 0x06000283 RID: 643 RVA: 0x0000586F File Offset: 0x00003A6F
		public override int KeySize
		{
			get
			{
				return base.KeySize;
			}
			set
			{
				base.KeySize = value;
			}
		}

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x06000284 RID: 644 RVA: 0x00005878 File Offset: 0x00003A78
		// (set) Token: 0x06000285 RID: 645 RVA: 0x00005880 File Offset: 0x00003A80
		public override int FeedbackSize
		{
			get
			{
				return base.FeedbackSize;
			}
			set
			{
				base.FeedbackSize = value;
			}
		}

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x06000286 RID: 646 RVA: 0x00005889 File Offset: 0x00003A89
		// (set) Token: 0x06000287 RID: 647 RVA: 0x00005891 File Offset: 0x00003A91
		public override CipherMode Mode
		{
			get
			{
				return base.Mode;
			}
			set
			{
				if (value == CipherMode.CTS)
				{
					throw new CryptographicException("CTS is not supported");
				}
				base.Mode = value;
			}
		}

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x06000288 RID: 648 RVA: 0x000058A9 File Offset: 0x00003AA9
		// (set) Token: 0x06000289 RID: 649 RVA: 0x000058B1 File Offset: 0x00003AB1
		public override PaddingMode Padding
		{
			get
			{
				return base.Padding;
			}
			set
			{
				base.Padding = value;
			}
		}

		/// <summary>Creates a symmetric AES decryptor object using the current key and initialization vector (IV).</summary>
		/// <returns>A symmetric AES decryptor object.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The current key is invalid or missing.</exception>
		// Token: 0x0600028A RID: 650 RVA: 0x000058BA File Offset: 0x00003ABA
		public override ICryptoTransform CreateDecryptor()
		{
			return this.CreateDecryptor(this.Key, this.IV);
		}

		/// <summary>Creates a symmetric AES encryptor object using the current key and initialization vector (IV).</summary>
		/// <returns>A symmetric AES encryptor object.</returns>
		// Token: 0x0600028B RID: 651 RVA: 0x000058CE File Offset: 0x00003ACE
		public override ICryptoTransform CreateEncryptor()
		{
			return this.CreateEncryptor(this.Key, this.IV);
		}

		// Token: 0x0600028C RID: 652 RVA: 0x000058E2 File Offset: 0x00003AE2
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
		}
	}
}
