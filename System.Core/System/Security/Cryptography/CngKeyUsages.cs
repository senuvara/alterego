﻿using System;

namespace System.Security.Cryptography
{
	/// <summary>Specifies the cryptographic operations that a Cryptography Next Generation (CNG) key may be used with. </summary>
	// Token: 0x0200005E RID: 94
	[Flags]
	public enum CngKeyUsages
	{
		/// <summary>No usage values are assigned to the key.</summary>
		// Token: 0x040002B1 RID: 689
		None = 0,
		/// <summary>The key can be used for encryption and decryption.</summary>
		// Token: 0x040002B2 RID: 690
		Decryption = 1,
		/// <summary>The key can be used for signing and verification.</summary>
		// Token: 0x040002B3 RID: 691
		Signing = 2,
		/// <summary>The key can be used for secret agreement generation and key exchange.</summary>
		// Token: 0x040002B4 RID: 692
		KeyAgreement = 4,
		/// <summary>The key can be used for all purposes.</summary>
		// Token: 0x040002B5 RID: 693
		AllUsages = 16777215
	}
}
