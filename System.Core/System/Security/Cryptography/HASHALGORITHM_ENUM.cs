﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x02000047 RID: 71
	internal enum HASHALGORITHM_ENUM
	{
		// Token: 0x04000252 RID: 594
		DSA_HASH_ALGORITHM_SHA1,
		// Token: 0x04000253 RID: 595
		DSA_HASH_ALGORITHM_SHA256,
		// Token: 0x04000254 RID: 596
		DSA_HASH_ALGORITHM_SHA512
	}
}
