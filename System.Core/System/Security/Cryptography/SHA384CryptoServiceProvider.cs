﻿using System;

namespace System.Security.Cryptography
{
	/// <summary>Defines a wrapper object to access the cryptographic service provider (CSP) implementation of the <see cref="T:System.Security.Cryptography.SHA384" /> algorithm. </summary>
	// Token: 0x0200006B RID: 107
	public sealed class SHA384CryptoServiceProvider : SHA384
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.SHA384CryptoServiceProvider" /> class. </summary>
		// Token: 0x060002B5 RID: 693 RVA: 0x00007690 File Offset: 0x00005890
		[SecurityCritical]
		public SHA384CryptoServiceProvider()
		{
			this.hash = new SHA384Managed();
		}

		/// <summary>Initializes, or reinitializes, an instance of a hash algorithm.</summary>
		// Token: 0x060002B6 RID: 694 RVA: 0x000076A3 File Offset: 0x000058A3
		[SecurityCritical]
		public override void Initialize()
		{
			this.hash.Initialize();
		}

		// Token: 0x060002B7 RID: 695 RVA: 0x000076B0 File Offset: 0x000058B0
		[SecurityCritical]
		protected override void HashCore(byte[] array, int ibStart, int cbSize)
		{
			this.hash.TransformBlock(array, ibStart, cbSize, null, 0);
		}

		// Token: 0x060002B8 RID: 696 RVA: 0x000076C3 File Offset: 0x000058C3
		[SecurityCritical]
		protected override byte[] HashFinal()
		{
			this.hash.TransformFinalBlock(SHA384CryptoServiceProvider.Empty, 0, 0);
			this.HashValue = this.hash.Hash;
			return this.HashValue;
		}

		// Token: 0x060002B9 RID: 697 RVA: 0x000076EF File Offset: 0x000058EF
		[SecurityCritical]
		protected override void Dispose(bool disposing)
		{
			((IDisposable)this.hash).Dispose();
			base.Dispose(disposing);
		}

		// Token: 0x060002BA RID: 698 RVA: 0x00007703 File Offset: 0x00005903
		// Note: this type is marked as 'beforefieldinit'.
		static SHA384CryptoServiceProvider()
		{
		}

		// Token: 0x040002E5 RID: 741
		private static byte[] Empty = new byte[0];

		// Token: 0x040002E6 RID: 742
		private SHA384 hash;
	}
}
