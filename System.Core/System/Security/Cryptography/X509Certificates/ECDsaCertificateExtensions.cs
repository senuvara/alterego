﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	/// <summary>Provides extension methods for retrieving <see cref="T:System.Security.Cryptography.ECDsa" /> implementations for the
	///     public and private keys of a <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate2" /> certificate.</summary>
	// Token: 0x0200006F RID: 111
	public static class ECDsaCertificateExtensions
	{
		/// <summary>Gets the <see cref="T:System.Security.Cryptography.ECDsa" /> private key from the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate2" /> certificate.</summary>
		/// <param name="certificate">The certificate. </param>
		/// <returns>The private key, or <see langword="null" /> if the certificate does not have an ECDsa private key. </returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="certificate" /> is <see langword="null" />. </exception>
		// Token: 0x060002D0 RID: 720 RVA: 0x0000227E File Offset: 0x0000047E
		[MonoTODO]
		public static ECDsa GetECDsaPrivateKey(this X509Certificate2 certificate)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets the <see cref="T:System.Security.Cryptography.ECDsa" /> public key from the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate2" /> certificate.</summary>
		/// <param name="certificate">The certificate. </param>
		/// <returns>The public key, or <see langword="null" /> if the certificate does not have an ECDsa public key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="certificate" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The handle is invalid. </exception>
		// Token: 0x060002D1 RID: 721 RVA: 0x0000227E File Offset: 0x0000047E
		[MonoTODO]
		public static ECDsa GetECDsaPublicKey(this X509Certificate2 certificate)
		{
			throw new NotImplementedException();
		}
	}
}
