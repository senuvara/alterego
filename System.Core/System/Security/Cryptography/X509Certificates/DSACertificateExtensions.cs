﻿using System;
using Unity;

namespace System.Security.Cryptography.X509Certificates
{
	/// <summary>Provides extension methods for retrieving <see cref="T:System.Security.Cryptography.DSA" /> implementations for the public and private keys of an <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate2" />. </summary>
	// Token: 0x02000499 RID: 1177
	public static class DSACertificateExtensions
	{
		/// <summary>Gets the <see cref="T:System.Security.Cryptography.DSA" /> private key from the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate2" />.</summary>
		/// <param name="certificate">The certificate. </param>
		/// <returns>The private key, or <see langword="null" /> if the certificate does not have a DSA private key. </returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="certificate" /> is <see langword="null" />. </exception>
		// Token: 0x06001CB1 RID: 7345 RVA: 0x0004B26D File Offset: 0x0004946D
		[SecuritySafeCritical]
		public static DSA GetDSAPrivateKey(this X509Certificate2 certificate)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the <see cref="T:System.Security.Cryptography.DSA" /> public key from the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate2" />.</summary>
		/// <param name="certificate">The certificate. </param>
		/// <returns>The public key, or <see langword="null" /> if the certificate does not have a DSA public key. </returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="certificate" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">Windows reports an error. See the <see cref="P:System.Exception.Message" /> property for more information. </exception>
		// Token: 0x06001CB2 RID: 7346 RVA: 0x0004B26D File Offset: 0x0004946D
		[SecuritySafeCritical]
		public static DSA GetDSAPublicKey(this X509Certificate2 certificate)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
