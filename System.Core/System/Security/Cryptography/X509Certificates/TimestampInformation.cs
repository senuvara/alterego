﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Security.Cryptography.X509Certificates
{
	/// <summary>Provides details about the time stamp that was applied to an Authenticode signature for a manifest. </summary>
	// Token: 0x02000490 RID: 1168
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class TimestampInformation
	{
		// Token: 0x06001C8E RID: 7310 RVA: 0x0000220F File Offset: 0x0000040F
		internal TimestampInformation()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the hash algorithm used to compute the time stamp signature.</summary>
		/// <returns>The hash algorithm used to compute the time stamp signature.</returns>
		// Token: 0x17000592 RID: 1426
		// (get) Token: 0x06001C8F RID: 7311 RVA: 0x0004B26D File Offset: 0x0004946D
		public string HashAlgorithm
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the HRESULT value that results from verifying the signature.</summary>
		/// <returns>The HRESULT value that results from verifying the signature.</returns>
		// Token: 0x17000593 RID: 1427
		// (get) Token: 0x06001C90 RID: 7312 RVA: 0x00053AC8 File Offset: 0x00051CC8
		public int HResult
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets a value indicating whether the time stamp of the signature is valid.</summary>
		/// <returns>
		///     <see langword="true" /> if the time stamp is valid; otherwise, <see langword="false" />. </returns>
		// Token: 0x17000594 RID: 1428
		// (get) Token: 0x06001C91 RID: 7313 RVA: 0x00053AE4 File Offset: 0x00051CE4
		public bool IsValid
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets the chain of certificates used to verify the time stamp of the signature.</summary>
		/// <returns>An <see cref="T:System.Security.Cryptography.X509Certificates.X509Chain" /> object that represents the certificate chain.</returns>
		// Token: 0x17000595 RID: 1429
		// (get) Token: 0x06001C92 RID: 7314 RVA: 0x0004B26D File Offset: 0x0004946D
		public X509Chain SignatureChain
		{
			[SecuritySafeCritical]
			[StorePermission(SecurityAction.Demand, OpenStore = true, EnumerateCertificates = true)]
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the certificate that signed the time stamp.</summary>
		/// <returns>An <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate2" /> object that represents the certificate.</returns>
		// Token: 0x17000596 RID: 1430
		// (get) Token: 0x06001C93 RID: 7315 RVA: 0x0004B26D File Offset: 0x0004946D
		public X509Certificate2 SigningCertificate
		{
			[SecuritySafeCritical]
			[StorePermission(SecurityAction.Demand, OpenStore = true, EnumerateCertificates = true)]
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the time stamp that was applied to the signature.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> object that represents the time stamp.</returns>
		// Token: 0x17000597 RID: 1431
		// (get) Token: 0x06001C94 RID: 7316 RVA: 0x00053B00 File Offset: 0x00051D00
		public DateTime Timestamp
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(DateTime);
			}
		}

		/// <summary>Gets the result of verifying the time stamp signature.</summary>
		/// <returns>One of the <see cref="T:System.Security.Cryptography.SignatureVerificationResult" /> values.</returns>
		// Token: 0x17000598 RID: 1432
		// (get) Token: 0x06001C95 RID: 7317 RVA: 0x00053B1C File Offset: 0x00051D1C
		public SignatureVerificationResult VerificationResult
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return SignatureVerificationResult.Valid;
			}
		}
	}
}
