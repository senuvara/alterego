﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Security.Cryptography.X509Certificates
{
	/// <summary>Provides information about an Authenticode signature for a manifest. </summary>
	// Token: 0x0200048F RID: 1167
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class AuthenticodeSignatureInformation
	{
		// Token: 0x06001C84 RID: 7300 RVA: 0x0000220F File Offset: 0x0000040F
		internal AuthenticodeSignatureInformation()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the description of the signing certificate.</summary>
		/// <returns>The description of the signing certificate.</returns>
		// Token: 0x17000589 RID: 1417
		// (get) Token: 0x06001C85 RID: 7301 RVA: 0x0004B26D File Offset: 0x0004946D
		public string Description
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the description URL of the signing certificate.</summary>
		/// <returns>The description URL of the signing certificate.</returns>
		// Token: 0x1700058A RID: 1418
		// (get) Token: 0x06001C86 RID: 7302 RVA: 0x0004B26D File Offset: 0x0004946D
		public Uri DescriptionUrl
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the hash algorithm used to compute the signature.</summary>
		/// <returns>The hash algorithm used to compute the signature.</returns>
		// Token: 0x1700058B RID: 1419
		// (get) Token: 0x06001C87 RID: 7303 RVA: 0x0004B26D File Offset: 0x0004946D
		public string HashAlgorithm
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the HRESULT value from verifying the signature.</summary>
		/// <returns>The HRESULT value from verifying the signature.</returns>
		// Token: 0x1700058C RID: 1420
		// (get) Token: 0x06001C88 RID: 7304 RVA: 0x00053A74 File Offset: 0x00051C74
		public int HResult
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the chain of certificates used to verify the Authenticode signature.</summary>
		/// <returns>An <see cref="T:System.Security.Cryptography.X509Certificates.X509Chain" /> object that contains the certificate chain.</returns>
		// Token: 0x1700058D RID: 1421
		// (get) Token: 0x06001C89 RID: 7305 RVA: 0x0004B26D File Offset: 0x0004946D
		public X509Chain SignatureChain
		{
			[SecuritySafeCritical]
			[StorePermission(SecurityAction.Demand, OpenStore = true, EnumerateCertificates = true)]
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the certificate that signed the manifest.</summary>
		/// <returns>An <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate2" /> object that represents the certificate.</returns>
		// Token: 0x1700058E RID: 1422
		// (get) Token: 0x06001C8A RID: 7306 RVA: 0x0004B26D File Offset: 0x0004946D
		public X509Certificate2 SigningCertificate
		{
			[SecuritySafeCritical]
			[StorePermission(SecurityAction.Demand, OpenStore = true, EnumerateCertificates = true)]
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the time stamp that was applied to the Authenticode signature.</summary>
		/// <returns>A <see cref="T:System.Security.Cryptography.X509Certificates.TimestampInformation" /> object that contains the signature time stamp.</returns>
		// Token: 0x1700058F RID: 1423
		// (get) Token: 0x06001C8B RID: 7307 RVA: 0x0004B26D File Offset: 0x0004946D
		public TimestampInformation Timestamp
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the trustworthiness of the Authenticode signature.</summary>
		/// <returns>One of the <see cref="T:System.Security.Cryptography.X509Certificates.TrustStatus" /> values. </returns>
		// Token: 0x17000590 RID: 1424
		// (get) Token: 0x06001C8C RID: 7308 RVA: 0x00053A90 File Offset: 0x00051C90
		public TrustStatus TrustStatus
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return TrustStatus.Untrusted;
			}
		}

		/// <summary>Gets the result of verifying the Authenticode signature.</summary>
		/// <returns>One of the <see cref="T:System.Security.Cryptography.SignatureVerificationResult" /> values.</returns>
		// Token: 0x17000591 RID: 1425
		// (get) Token: 0x06001C8D RID: 7309 RVA: 0x00053AAC File Offset: 0x00051CAC
		public SignatureVerificationResult VerificationResult
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return SignatureVerificationResult.Valid;
			}
		}
	}
}
