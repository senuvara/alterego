﻿using System;

namespace System.Security.Cryptography
{
	/// <summary>Represents the standard parameters for the elliptic curve cryptography (ECC) algorithm.</summary>
	// Token: 0x02000068 RID: 104
	public struct ECParameters
	{
		/// <summary>Validates the current object.</summary>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The key or curve parameters are not valid for the current curve type.</exception>
		// Token: 0x060002AE RID: 686 RVA: 0x0000227E File Offset: 0x0000047E
		public void Validate()
		{
			throw new NotImplementedException();
		}

		/// <summary>Represents the curve associated with the public key (<see cref="F:System.Security.Cryptography.ECParameters.Q" />) and the optional private key (<see cref="F:System.Security.Cryptography.ECParameters.D" />).</summary>
		/// <returns>The curve.</returns>
		// Token: 0x040002DE RID: 734
		public ECCurve Curve;

		/// <summary>Represents the private key <see langword="D" /> for the elliptic curve cryptography (ECC) algorithm, stored in big-endian format.</summary>
		/// <returns>The <see langword="D" /> parameter for the elliptic curve cryptography (ECC) algorithm.</returns>
		// Token: 0x040002DF RID: 735
		public byte[] D;

		/// <summary>Represents the public key <see langword="Q" /> for the elliptic curve cryptography (ECC) algorithm.</summary>
		/// <returns>The <see langword="Q" /> parameter for the elliptic curve cryptography (ECC) algorithm.</returns>
		// Token: 0x040002E0 RID: 736
		public ECPoint Q;
	}
}
