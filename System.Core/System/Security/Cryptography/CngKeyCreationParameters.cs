﻿using System;
using System.Security.Permissions;

namespace System.Security.Cryptography
{
	/// <summary>Contains advanced properties for key creation.</summary>
	// Token: 0x02000050 RID: 80
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class CngKeyCreationParameters
	{
		/// <summary>Gets or sets the key export policy.</summary>
		/// <returns>An object that specifies a key export policy. The default value is <see langword="null" />, which indicates that the key storage provider's default export policy is set.</returns>
		// Token: 0x1700006D RID: 109
		// (get) Token: 0x060001F1 RID: 497 RVA: 0x00004F93 File Offset: 0x00003193
		// (set) Token: 0x060001F2 RID: 498 RVA: 0x00004F9B File Offset: 0x0000319B
		public CngExportPolicies? ExportPolicy
		{
			get
			{
				return this.m_exportPolicy;
			}
			set
			{
				this.m_exportPolicy = value;
			}
		}

		/// <summary>Gets or sets the key creation options.</summary>
		/// <returns>An object that specifies options for creating keys. The default value is <see langword="null" />, which indicates that the key storage provider's default key creation options are set.</returns>
		// Token: 0x1700006E RID: 110
		// (get) Token: 0x060001F3 RID: 499 RVA: 0x00004FA4 File Offset: 0x000031A4
		// (set) Token: 0x060001F4 RID: 500 RVA: 0x00004FAC File Offset: 0x000031AC
		public CngKeyCreationOptions KeyCreationOptions
		{
			get
			{
				return this.m_keyCreationOptions;
			}
			set
			{
				this.m_keyCreationOptions = value;
			}
		}

		/// <summary>Gets or sets the cryptographic operations that apply to the current key. </summary>
		/// <returns>A bitwise combination of one or more enumeration values that specify key usage. The default value is <see langword="null" />, which indicates that the key storage provider's default key usage is set.</returns>
		// Token: 0x1700006F RID: 111
		// (get) Token: 0x060001F5 RID: 501 RVA: 0x00004FB5 File Offset: 0x000031B5
		// (set) Token: 0x060001F6 RID: 502 RVA: 0x00004FBD File Offset: 0x000031BD
		public CngKeyUsages? KeyUsage
		{
			get
			{
				return this.m_keyUsage;
			}
			set
			{
				this.m_keyUsage = value;
			}
		}

		/// <summary>Gets or sets the window handle that should be used as the parent window for dialog boxes that are created by Cryptography Next Generation (CNG) classes.</summary>
		/// <returns>The HWND of the parent window that is used for CNG dialog boxes.</returns>
		// Token: 0x17000070 RID: 112
		// (get) Token: 0x060001F7 RID: 503 RVA: 0x00004FC6 File Offset: 0x000031C6
		// (set) Token: 0x060001F8 RID: 504 RVA: 0x00004FCE File Offset: 0x000031CE
		public IntPtr ParentWindowHandle
		{
			get
			{
				return this.m_parentWindowHandle;
			}
			[SecuritySafeCritical]
			[SecurityPermission(SecurityAction.Demand, UnmanagedCode = true)]
			set
			{
				this.m_parentWindowHandle = value;
			}
		}

		/// <summary>Enables a <see cref="T:System.Security.Cryptography.CngKey" /> object to be created with additional properties that are set before the key is finalized.</summary>
		/// <returns>A collection object that contains any additional parameters that you must set on a <see cref="T:System.Security.Cryptography.CngKey" /> object during key creation.</returns>
		// Token: 0x17000071 RID: 113
		// (get) Token: 0x060001F9 RID: 505 RVA: 0x00004FD7 File Offset: 0x000031D7
		public CngPropertyCollection Parameters
		{
			[SecuritySafeCritical]
			[SecurityPermission(SecurityAction.Demand, UnmanagedCode = true)]
			get
			{
				return this.m_parameters;
			}
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x060001FA RID: 506 RVA: 0x00004FD7 File Offset: 0x000031D7
		internal CngPropertyCollection ParametersNoDemand
		{
			get
			{
				return this.m_parameters;
			}
		}

		/// <summary>Gets or sets the key storage provider (KSP) to create a key in.</summary>
		/// <returns>An object that specifies the KSP that a new key will be created in.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <see cref="P:System.Security.Cryptography.CngKeyCreationParameters.Provider" /> property is set to a <see langword="null" /> value.</exception>
		// Token: 0x17000073 RID: 115
		// (get) Token: 0x060001FB RID: 507 RVA: 0x00004FDF File Offset: 0x000031DF
		// (set) Token: 0x060001FC RID: 508 RVA: 0x00004FE7 File Offset: 0x000031E7
		public CngProvider Provider
		{
			get
			{
				return this.m_provider;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.m_provider = value;
			}
		}

		/// <summary>Gets or sets information about the user interface to display when a key is created or accessed.</summary>
		/// <returns>An object that contains details about the user interface shown by Cryptography Next Generation (CNG) classes when a key is created or accessed. A <see langword="null" /> value indicates that the key storage provider's default user interface policy is set.</returns>
		// Token: 0x17000074 RID: 116
		// (get) Token: 0x060001FD RID: 509 RVA: 0x00005004 File Offset: 0x00003204
		// (set) Token: 0x060001FE RID: 510 RVA: 0x0000500C File Offset: 0x0000320C
		public CngUIPolicy UIPolicy
		{
			get
			{
				return this.m_uiPolicy;
			}
			[SecuritySafeCritical]
			[HostProtection(SecurityAction.LinkDemand, UI = true)]
			[UIPermission(SecurityAction.Demand, Window = UIPermissionWindow.SafeSubWindows)]
			set
			{
				this.m_uiPolicy = value;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.CngKeyCreationParameters" /> class.</summary>
		// Token: 0x060001FF RID: 511 RVA: 0x00005015 File Offset: 0x00003215
		public CngKeyCreationParameters()
		{
		}

		// Token: 0x04000287 RID: 647
		private CngExportPolicies? m_exportPolicy;

		// Token: 0x04000288 RID: 648
		private CngKeyCreationOptions m_keyCreationOptions;

		// Token: 0x04000289 RID: 649
		private CngKeyUsages? m_keyUsage;

		// Token: 0x0400028A RID: 650
		private CngPropertyCollection m_parameters = new CngPropertyCollection();

		// Token: 0x0400028B RID: 651
		private IntPtr m_parentWindowHandle;

		// Token: 0x0400028C RID: 652
		private CngProvider m_provider = CngProvider.MicrosoftSoftwareKeyStorageProvider;

		// Token: 0x0400028D RID: 653
		private CngUIPolicy m_uiPolicy;
	}
}
