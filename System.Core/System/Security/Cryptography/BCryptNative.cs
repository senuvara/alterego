﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x02000049 RID: 73
	internal static class BCryptNative
	{
		// Token: 0x0200004A RID: 74
		internal static class AlgorithmName
		{
			// Token: 0x04000258 RID: 600
			public const string ECDH = "ECDH";

			// Token: 0x04000259 RID: 601
			public const string ECDHP256 = "ECDH_P256";

			// Token: 0x0400025A RID: 602
			public const string ECDHP384 = "ECDH_P384";

			// Token: 0x0400025B RID: 603
			public const string ECDHP521 = "ECDH_P521";

			// Token: 0x0400025C RID: 604
			public const string ECDsa = "ECDSA";

			// Token: 0x0400025D RID: 605
			public const string ECDsaP256 = "ECDSA_P256";

			// Token: 0x0400025E RID: 606
			public const string ECDsaP384 = "ECDSA_P384";

			// Token: 0x0400025F RID: 607
			public const string ECDsaP521 = "ECDSA_P521";

			// Token: 0x04000260 RID: 608
			public const string MD5 = "MD5";

			// Token: 0x04000261 RID: 609
			public const string Sha1 = "SHA1";

			// Token: 0x04000262 RID: 610
			public const string Sha256 = "SHA256";

			// Token: 0x04000263 RID: 611
			public const string Sha384 = "SHA384";

			// Token: 0x04000264 RID: 612
			public const string Sha512 = "SHA512";

			// Token: 0x04000265 RID: 613
			internal const string Rsa = "RSA";
		}
	}
}
