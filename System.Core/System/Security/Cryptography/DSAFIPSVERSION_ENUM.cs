﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x02000048 RID: 72
	internal enum DSAFIPSVERSION_ENUM
	{
		// Token: 0x04000256 RID: 598
		DSA_FIPS186_2,
		// Token: 0x04000257 RID: 599
		DSA_FIPS186_3
	}
}
