﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Security.Cryptography
{
	/// <summary>Provides a Cryptography Next Generation (CNG) implementation of the Secure Hash Algorithm (SHA).</summary>
	// Token: 0x02000495 RID: 1173
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class SHA1Cng : SHA1
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.SHA1Cng" /> class. </summary>
		// Token: 0x06001CA1 RID: 7329 RVA: 0x0000220F File Offset: 0x0000040F
		public SHA1Cng()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06001CA2 RID: 7330 RVA: 0x0000220F File Offset: 0x0000040F
		protected override void HashCore(byte[] array, int ibStart, int cbSize)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06001CA3 RID: 7331 RVA: 0x0004B26D File Offset: 0x0004946D
		protected override byte[] HashFinal()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Initializes, or re-initializes, the instance of the hash algorithm. </summary>
		// Token: 0x06001CA4 RID: 7332 RVA: 0x0000220F File Offset: 0x0000040F
		public override void Initialize()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
