﻿using System;

namespace System.Security.Cryptography
{
	/// <summary>Defines a wrapper object to access the cryptographic service provider (CSP) implementation of the <see cref="T:System.Security.Cryptography.SHA256" /> algorithm. </summary>
	// Token: 0x0200006A RID: 106
	public sealed class SHA256CryptoServiceProvider : SHA256
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.SHA256CryptoServiceProvider" /> class. </summary>
		// Token: 0x060002AF RID: 687 RVA: 0x00007610 File Offset: 0x00005810
		[SecurityCritical]
		public SHA256CryptoServiceProvider()
		{
			this.hash = new SHA256Managed();
		}

		/// <summary>Initializes, or reinitializes, an instance of a hash algorithm.</summary>
		// Token: 0x060002B0 RID: 688 RVA: 0x00007623 File Offset: 0x00005823
		[SecurityCritical]
		public override void Initialize()
		{
			this.hash.Initialize();
		}

		// Token: 0x060002B1 RID: 689 RVA: 0x00007630 File Offset: 0x00005830
		[SecurityCritical]
		protected override void HashCore(byte[] array, int ibStart, int cbSize)
		{
			this.hash.TransformBlock(array, ibStart, cbSize, null, 0);
		}

		// Token: 0x060002B2 RID: 690 RVA: 0x00007643 File Offset: 0x00005843
		[SecurityCritical]
		protected override byte[] HashFinal()
		{
			this.hash.TransformFinalBlock(SHA256CryptoServiceProvider.Empty, 0, 0);
			this.HashValue = this.hash.Hash;
			return this.HashValue;
		}

		// Token: 0x060002B3 RID: 691 RVA: 0x0000766F File Offset: 0x0000586F
		[SecurityCritical]
		protected override void Dispose(bool disposing)
		{
			((IDisposable)this.hash).Dispose();
			base.Dispose(disposing);
		}

		// Token: 0x060002B4 RID: 692 RVA: 0x00007683 File Offset: 0x00005883
		// Note: this type is marked as 'beforefieldinit'.
		static SHA256CryptoServiceProvider()
		{
		}

		// Token: 0x040002E3 RID: 739
		private static byte[] Empty = new byte[0];

		// Token: 0x040002E4 RID: 740
		private SHA256 hash;
	}
}
