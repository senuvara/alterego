﻿using System;
using System.Security.Permissions;

namespace System.Security.Cryptography
{
	/// <summary>Encapsulates the name of an encryption algorithm. </summary>
	// Token: 0x0200004B RID: 75
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	[Serializable]
	public sealed class CngAlgorithm : IEquatable<CngAlgorithm>
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.CngAlgorithm" /> class.</summary>
		/// <param name="algorithm">The name of the algorithm to initialize.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="algorithm" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="algorithm" /> parameter length is 0 (zero).</exception>
		// Token: 0x0600019B RID: 411 RVA: 0x00004930 File Offset: 0x00002B30
		public CngAlgorithm(string algorithm)
		{
			if (algorithm == null)
			{
				throw new ArgumentNullException("algorithm");
			}
			if (algorithm.Length == 0)
			{
				throw new ArgumentException(SR.GetString("The algorithm name '{0}' is invalid.", new object[]
				{
					algorithm
				}), "algorithm");
			}
			this.m_algorithm = algorithm;
		}

		/// <summary>Gets the algorithm name that the current <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object specifies.</summary>
		/// <returns>The embedded algorithm name.</returns>
		// Token: 0x17000041 RID: 65
		// (get) Token: 0x0600019C RID: 412 RVA: 0x0000497F File Offset: 0x00002B7F
		public string Algorithm
		{
			get
			{
				return this.m_algorithm;
			}
		}

		/// <summary>Determines whether two <see cref="T:System.Security.Cryptography.CngAlgorithm" /> objects specify the same algorithm name.</summary>
		/// <param name="left">An object that specifies an algorithm name.</param>
		/// <param name="right">A second object, to be compared to the object that is identified by the <paramref name="left" /> parameter.</param>
		/// <returns>
		///     <see langword="true" /> if the two objects specify the same algorithm name; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600019D RID: 413 RVA: 0x00004987 File Offset: 0x00002B87
		public static bool operator ==(CngAlgorithm left, CngAlgorithm right)
		{
			if (left == null)
			{
				return right == null;
			}
			return left.Equals(right);
		}

		/// <summary>Determines whether two <see cref="T:System.Security.Cryptography.CngAlgorithm" /> objects do not specify the same algorithm.</summary>
		/// <param name="left">An object that specifies an algorithm name.</param>
		/// <param name="right">A second object, to be compared to the object that is identified by the <paramref name="left" /> parameter.</param>
		/// <returns>
		///     <see langword="true" /> if the two objects do not specify the same algorithm name; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600019E RID: 414 RVA: 0x00004998 File Offset: 0x00002B98
		public static bool operator !=(CngAlgorithm left, CngAlgorithm right)
		{
			if (left == null)
			{
				return right != null;
			}
			return !left.Equals(right);
		}

		/// <summary>Compares the specified object to the current <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object.</summary>
		/// <param name="obj">An object to be compared to the current <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object.</param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="obj" /> parameter is a <see cref="T:System.Security.Cryptography.CngAlgorithm" /> that specifies the same algorithm as the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600019F RID: 415 RVA: 0x000049AC File Offset: 0x00002BAC
		public override bool Equals(object obj)
		{
			return this.Equals(obj as CngAlgorithm);
		}

		/// <summary>Compares the specified <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object to the current <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object. </summary>
		/// <param name="other">An object to be compared to the current <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object.</param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="other" /> parameter specifies the same algorithm as the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x060001A0 RID: 416 RVA: 0x000049BA File Offset: 0x00002BBA
		public bool Equals(CngAlgorithm other)
		{
			return other != null && this.m_algorithm.Equals(other.Algorithm);
		}

		/// <summary>Generates a hash value for the algorithm name that is embedded in the current <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object.</summary>
		/// <returns>The hash value of the embedded algorithm name.</returns>
		// Token: 0x060001A1 RID: 417 RVA: 0x000049D2 File Offset: 0x00002BD2
		public override int GetHashCode()
		{
			return this.m_algorithm.GetHashCode();
		}

		/// <summary>Gets the name of the algorithm that the current <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object specifies.</summary>
		/// <returns>The embedded algorithm name.</returns>
		// Token: 0x060001A2 RID: 418 RVA: 0x0000497F File Offset: 0x00002B7F
		public override string ToString()
		{
			return this.m_algorithm;
		}

		/// <summary>Gets a new <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object that specifies the RSA hash algorithm.</summary>
		/// <returns>An object that specifies the RSA algorithm.</returns>
		// Token: 0x17000042 RID: 66
		// (get) Token: 0x060001A3 RID: 419 RVA: 0x000049DF File Offset: 0x00002BDF
		public static CngAlgorithm Rsa
		{
			get
			{
				if (CngAlgorithm.s_rsa == null)
				{
					CngAlgorithm.s_rsa = new CngAlgorithm("RSA");
				}
				return CngAlgorithm.s_rsa;
			}
		}

		/// <summary>Gets a <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object that specifies an Elliptic Curve Diffie-Hellman (ECDH) key exchange algorithm whose curve is described via a key property.</summary>
		/// <returns>An object that specifies an ECDH key exchange algorithm whose curve is described via a key property.</returns>
		// Token: 0x17000043 RID: 67
		// (get) Token: 0x060001A4 RID: 420 RVA: 0x00004A08 File Offset: 0x00002C08
		public static CngAlgorithm ECDiffieHellman
		{
			get
			{
				if (CngAlgorithm.s_ecdh == null)
				{
					CngAlgorithm.s_ecdh = new CngAlgorithm("ECDH");
				}
				return CngAlgorithm.s_ecdh;
			}
		}

		/// <summary>Gets a <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object that specifies an Elliptic Curve Diffie-Hellman (ECDH) key exchange algorithm that uses the P-256 curve.</summary>
		/// <returns>An object that specifies an ECDH algorithm that uses the P-256 curve.</returns>
		// Token: 0x17000044 RID: 68
		// (get) Token: 0x060001A5 RID: 421 RVA: 0x00004A31 File Offset: 0x00002C31
		public static CngAlgorithm ECDiffieHellmanP256
		{
			get
			{
				if (CngAlgorithm.s_ecdhp256 == null)
				{
					CngAlgorithm.s_ecdhp256 = new CngAlgorithm("ECDH_P256");
				}
				return CngAlgorithm.s_ecdhp256;
			}
		}

		/// <summary>Gets a <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object that specifies an Elliptic Curve Diffie-Hellman (ECDH) key exchange algorithm that uses the P-384 curve.</summary>
		/// <returns>An object that specifies an ECDH algorithm that uses the P-384 curve.</returns>
		// Token: 0x17000045 RID: 69
		// (get) Token: 0x060001A6 RID: 422 RVA: 0x00004A5A File Offset: 0x00002C5A
		public static CngAlgorithm ECDiffieHellmanP384
		{
			get
			{
				if (CngAlgorithm.s_ecdhp384 == null)
				{
					CngAlgorithm.s_ecdhp384 = new CngAlgorithm("ECDH_P384");
				}
				return CngAlgorithm.s_ecdhp384;
			}
		}

		/// <summary>Gets a <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object that specifies an Elliptic Curve Diffie-Hellman (ECDH) key exchange algorithm that uses the P-521 curve.</summary>
		/// <returns>An object that specifies an ECDH algorithm that uses the P-521 curve.</returns>
		// Token: 0x17000046 RID: 70
		// (get) Token: 0x060001A7 RID: 423 RVA: 0x00004A83 File Offset: 0x00002C83
		public static CngAlgorithm ECDiffieHellmanP521
		{
			get
			{
				if (CngAlgorithm.s_ecdhp521 == null)
				{
					CngAlgorithm.s_ecdhp521 = new CngAlgorithm("ECDH_P521");
				}
				return CngAlgorithm.s_ecdhp521;
			}
		}

		/// <summary>Gets a <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object that specifies an Elliptic Curve Digital Signature Algorithm (ECDSA) whose curve is described via a key property.</summary>
		/// <returns>An object that specifies an ECDSA whose curve is described via a key property.</returns>
		// Token: 0x17000047 RID: 71
		// (get) Token: 0x060001A8 RID: 424 RVA: 0x00004AAC File Offset: 0x00002CAC
		public static CngAlgorithm ECDsa
		{
			get
			{
				if (CngAlgorithm.s_ecdsa == null)
				{
					CngAlgorithm.s_ecdsa = new CngAlgorithm("ECDSA");
				}
				return CngAlgorithm.s_ecdsa;
			}
		}

		/// <summary>Gets a <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object that specifies an Elliptic Curve Digital Signature Algorithm (ECDSA) that uses the P-256 curve.</summary>
		/// <returns>An object that specifies an ECDSA algorithm that uses the P-256 curve.</returns>
		// Token: 0x17000048 RID: 72
		// (get) Token: 0x060001A9 RID: 425 RVA: 0x00004AD5 File Offset: 0x00002CD5
		public static CngAlgorithm ECDsaP256
		{
			get
			{
				if (CngAlgorithm.s_ecdsap256 == null)
				{
					CngAlgorithm.s_ecdsap256 = new CngAlgorithm("ECDSA_P256");
				}
				return CngAlgorithm.s_ecdsap256;
			}
		}

		/// <summary>Gets a <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object that specifies an Elliptic Curve Digital Signature Algorithm (ECDSA) that uses the P-384 curve.</summary>
		/// <returns>An object that specifies an ECDSA algorithm that uses the P-384 curve.</returns>
		// Token: 0x17000049 RID: 73
		// (get) Token: 0x060001AA RID: 426 RVA: 0x00004AFE File Offset: 0x00002CFE
		public static CngAlgorithm ECDsaP384
		{
			get
			{
				if (CngAlgorithm.s_ecdsap384 == null)
				{
					CngAlgorithm.s_ecdsap384 = new CngAlgorithm("ECDSA_P384");
				}
				return CngAlgorithm.s_ecdsap384;
			}
		}

		/// <summary>Gets a new <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object that specifies an Elliptic Curve Digital Signature Algorithm (ECDSA) that uses the P-521 curve.</summary>
		/// <returns>An object that specifies an ECDSA algorithm that uses the P-521 curve.</returns>
		// Token: 0x1700004A RID: 74
		// (get) Token: 0x060001AB RID: 427 RVA: 0x00004B27 File Offset: 0x00002D27
		public static CngAlgorithm ECDsaP521
		{
			get
			{
				if (CngAlgorithm.s_ecdsap521 == null)
				{
					CngAlgorithm.s_ecdsap521 = new CngAlgorithm("ECDSA_P521");
				}
				return CngAlgorithm.s_ecdsap521;
			}
		}

		/// <summary>Gets a new <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object that specifies the Message Digest 5 (MD5) hash algorithm.</summary>
		/// <returns>An object that specifies the MD5 algorithm.</returns>
		// Token: 0x1700004B RID: 75
		// (get) Token: 0x060001AC RID: 428 RVA: 0x00004B50 File Offset: 0x00002D50
		public static CngAlgorithm MD5
		{
			get
			{
				if (CngAlgorithm.s_md5 == null)
				{
					CngAlgorithm.s_md5 = new CngAlgorithm("MD5");
				}
				return CngAlgorithm.s_md5;
			}
		}

		/// <summary>Gets a new <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object that specifies the Secure Hash Algorithm 1 (SHA-1) algorithm.</summary>
		/// <returns>An object that specifies the SHA-1 algorithm.</returns>
		// Token: 0x1700004C RID: 76
		// (get) Token: 0x060001AD RID: 429 RVA: 0x00004B79 File Offset: 0x00002D79
		public static CngAlgorithm Sha1
		{
			get
			{
				if (CngAlgorithm.s_sha1 == null)
				{
					CngAlgorithm.s_sha1 = new CngAlgorithm("SHA1");
				}
				return CngAlgorithm.s_sha1;
			}
		}

		/// <summary>Gets a new <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object that specifies the Secure Hash Algorithm 256 (SHA-256) algorithm.</summary>
		/// <returns>An object that specifies the SHA-256 algorithm.</returns>
		// Token: 0x1700004D RID: 77
		// (get) Token: 0x060001AE RID: 430 RVA: 0x00004BA2 File Offset: 0x00002DA2
		public static CngAlgorithm Sha256
		{
			get
			{
				if (CngAlgorithm.s_sha256 == null)
				{
					CngAlgorithm.s_sha256 = new CngAlgorithm("SHA256");
				}
				return CngAlgorithm.s_sha256;
			}
		}

		/// <summary>Gets a new <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object that specifies the Secure Hash Algorithm 384 (SHA-384) algorithm.</summary>
		/// <returns>An object that specifies the SHA-384 algorithm.</returns>
		// Token: 0x1700004E RID: 78
		// (get) Token: 0x060001AF RID: 431 RVA: 0x00004BCB File Offset: 0x00002DCB
		public static CngAlgorithm Sha384
		{
			get
			{
				if (CngAlgorithm.s_sha384 == null)
				{
					CngAlgorithm.s_sha384 = new CngAlgorithm("SHA384");
				}
				return CngAlgorithm.s_sha384;
			}
		}

		/// <summary>Gets a new <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object that specifies the Secure Hash Algorithm 512 (SHA-512) algorithm.</summary>
		/// <returns>An object that specifies the SHA-512 algorithm.</returns>
		// Token: 0x1700004F RID: 79
		// (get) Token: 0x060001B0 RID: 432 RVA: 0x00004BF4 File Offset: 0x00002DF4
		public static CngAlgorithm Sha512
		{
			get
			{
				if (CngAlgorithm.s_sha512 == null)
				{
					CngAlgorithm.s_sha512 = new CngAlgorithm("SHA512");
				}
				return CngAlgorithm.s_sha512;
			}
		}

		// Token: 0x04000266 RID: 614
		private static volatile CngAlgorithm s_ecdh;

		// Token: 0x04000267 RID: 615
		private static volatile CngAlgorithm s_ecdhp256;

		// Token: 0x04000268 RID: 616
		private static volatile CngAlgorithm s_ecdhp384;

		// Token: 0x04000269 RID: 617
		private static volatile CngAlgorithm s_ecdhp521;

		// Token: 0x0400026A RID: 618
		private static volatile CngAlgorithm s_ecdsa;

		// Token: 0x0400026B RID: 619
		private static volatile CngAlgorithm s_ecdsap256;

		// Token: 0x0400026C RID: 620
		private static volatile CngAlgorithm s_ecdsap384;

		// Token: 0x0400026D RID: 621
		private static volatile CngAlgorithm s_ecdsap521;

		// Token: 0x0400026E RID: 622
		private static volatile CngAlgorithm s_md5;

		// Token: 0x0400026F RID: 623
		private static volatile CngAlgorithm s_sha1;

		// Token: 0x04000270 RID: 624
		private static volatile CngAlgorithm s_sha256;

		// Token: 0x04000271 RID: 625
		private static volatile CngAlgorithm s_sha384;

		// Token: 0x04000272 RID: 626
		private static volatile CngAlgorithm s_sha512;

		// Token: 0x04000273 RID: 627
		private static volatile CngAlgorithm s_rsa;

		// Token: 0x04000274 RID: 628
		private string m_algorithm;
	}
}
