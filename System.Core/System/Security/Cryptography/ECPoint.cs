﻿using System;

namespace System.Security.Cryptography
{
	/// <summary>Represents a (X,Y) coordinate pair for elliptic curve cryptography (ECC) structures.</summary>
	// Token: 0x02000069 RID: 105
	public struct ECPoint
	{
		/// <summary>Represents the X coordinate.</summary>
		/// <returns>The X coordinate.</returns>
		// Token: 0x040002E1 RID: 737
		public byte[] X;

		/// <summary>Represents the Y coordinate.</summary>
		/// <returns>The Y coordinate.</returns>
		// Token: 0x040002E2 RID: 738
		public byte[] Y;
	}
}
