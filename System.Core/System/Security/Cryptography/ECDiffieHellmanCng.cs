﻿using System;
using System.Security.Permissions;
using Microsoft.Win32.SafeHandles;
using Unity;

namespace System.Security.Cryptography
{
	/// <summary>Provides a Cryptography Next Generation (CNG) implementation of the Elliptic Curve Diffie-Hellman (ECDH) algorithm. This class is used to perform cryptographic operations.</summary>
	// Token: 0x0200048B RID: 1163
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class ECDiffieHellmanCng : ECDiffieHellman
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.ECDiffieHellmanCng" /> class with a random key pair.</summary>
		// Token: 0x06001C5E RID: 7262 RVA: 0x0000220F File Offset: 0x0000040F
		public ECDiffieHellmanCng()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.ECDiffieHellmanCng" /> class with a random key pair, using the specified key size.</summary>
		/// <param name="keySize">The size of the key. Valid key sizes are 256, 384, and 521 bits.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="keySize" /> specifies an invalid length.</exception>
		/// <exception cref="T:System.PlatformNotSupportedException">Cryptography Next Generation (CNG) classes are not supported on this system.</exception>
		// Token: 0x06001C5F RID: 7263 RVA: 0x0000220F File Offset: 0x0000040F
		public ECDiffieHellmanCng(int keySize)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.ECDiffieHellmanCng" /> class by using the specified <see cref="T:System.Security.Cryptography.CngKey" /> object.</summary>
		/// <param name="key">The key that will be used as input to the cryptographic operations performed by the current object. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="key" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="key" /> does not specify an Elliptic Curve Diffie-Hellman (ECDH) algorithm group.</exception>
		/// <exception cref="T:System.PlatformNotSupportedException">Cryptography Next Generation (CNG) classes are not supported on this system.</exception>
		// Token: 0x06001C60 RID: 7264 RVA: 0x0000220F File Offset: 0x0000040F
		[SecuritySafeCritical]
		public ECDiffieHellmanCng(CngKey key)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Security.Cryptography.ECDiffieHellmanCng" /> class whose public/private key pair is generated over the specified curve. </summary>
		/// <param name="curve">The curve used to generate the public/private key pair. </param>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">
		///         <paramref name="curve" /> does not validate. </exception>
		// Token: 0x06001C61 RID: 7265 RVA: 0x0000220F File Offset: 0x0000040F
		public ECDiffieHellmanCng(ECCurve curve)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the hash algorithm to use when generating key material.</summary>
		/// <returns>An object that specifies the hash algorithm.</returns>
		/// <exception cref="T:System.ArgumentNullException">The value is <see langword="null." /></exception>
		// Token: 0x1700057B RID: 1403
		// (get) Token: 0x06001C62 RID: 7266 RVA: 0x0004B26D File Offset: 0x0004946D
		// (set) Token: 0x06001C63 RID: 7267 RVA: 0x0000220F File Offset: 0x0000040F
		public CngAlgorithm HashAlgorithm
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the Hash-based Message Authentication Code (HMAC) key to use when deriving key material.</summary>
		/// <returns>The Hash-based Message Authentication Code (HMAC) key to use when deriving key material.</returns>
		// Token: 0x1700057C RID: 1404
		// (get) Token: 0x06001C64 RID: 7268 RVA: 0x0004B26D File Offset: 0x0004946D
		// (set) Token: 0x06001C65 RID: 7269 RVA: 0x0000220F File Offset: 0x0000040F
		public byte[] HmacKey
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Specifies the <see cref="T:System.Security.Cryptography.CngKey" /> that is used by the current object for cryptographic operations.</summary>
		/// <returns>The key pair used by this object to perform cryptographic operations.</returns>
		// Token: 0x1700057D RID: 1405
		// (get) Token: 0x06001C66 RID: 7270 RVA: 0x0004B26D File Offset: 0x0004946D
		public CngKey Key
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the key derivation function for the <see cref="T:System.Security.Cryptography.ECDiffieHellmanCng" /> class.</summary>
		/// <returns>One of the <see cref="T:System.Security.Cryptography.ECDiffieHellmanKeyDerivationFunction" /> enumeration values: <see cref="F:System.Security.Cryptography.ECDiffieHellmanKeyDerivationFunction.Hash" />, <see cref="F:System.Security.Cryptography.ECDiffieHellmanKeyDerivationFunction.Hmac" />, or <see cref="F:System.Security.Cryptography.ECDiffieHellmanKeyDerivationFunction.Tls" />. The default value is <see cref="F:System.Security.Cryptography.ECDiffieHellmanKeyDerivationFunction.Hash" />.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The enumeration value is out of range.</exception>
		// Token: 0x1700057E RID: 1406
		// (get) Token: 0x06001C67 RID: 7271 RVA: 0x00053A20 File Offset: 0x00051C20
		// (set) Token: 0x06001C68 RID: 7272 RVA: 0x0000220F File Offset: 0x0000040F
		public ECDiffieHellmanKeyDerivationFunction KeyDerivationFunction
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ECDiffieHellmanKeyDerivationFunction.Hash;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the label value that is used for key derivation.</summary>
		/// <returns>The label value.</returns>
		// Token: 0x1700057F RID: 1407
		// (get) Token: 0x06001C69 RID: 7273 RVA: 0x0004B26D File Offset: 0x0004946D
		// (set) Token: 0x06001C6A RID: 7274 RVA: 0x0000220F File Offset: 0x0000040F
		public byte[] Label
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the public key that can be used by another <see cref="T:System.Security.Cryptography.ECDiffieHellmanCng" /> object to generate a shared secret agreement.</summary>
		/// <returns>The public key that is associated with this instance of the <see cref="T:System.Security.Cryptography.ECDiffieHellmanCng" /> object.</returns>
		// Token: 0x17000580 RID: 1408
		// (get) Token: 0x06001C6B RID: 7275 RVA: 0x0004B26D File Offset: 0x0004946D
		public override ECDiffieHellmanPublicKey PublicKey
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets a value that will be appended to the secret agreement when generating key material.</summary>
		/// <returns>The value that is appended to the secret agreement.</returns>
		// Token: 0x17000581 RID: 1409
		// (get) Token: 0x06001C6C RID: 7276 RVA: 0x0004B26D File Offset: 0x0004946D
		// (set) Token: 0x06001C6D RID: 7277 RVA: 0x0000220F File Offset: 0x0000040F
		public byte[] SecretAppend
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that will be added to the beginning of the secret agreement when deriving key material.</summary>
		/// <returns>The value that is appended to the beginning of the secret agreement during key derivation.</returns>
		// Token: 0x17000582 RID: 1410
		// (get) Token: 0x06001C6E RID: 7278 RVA: 0x0004B26D File Offset: 0x0004946D
		// (set) Token: 0x06001C6F RID: 7279 RVA: 0x0000220F File Offset: 0x0000040F
		public byte[] SecretPrepend
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the seed value that will be used when deriving key material.</summary>
		/// <returns>The seed value.</returns>
		// Token: 0x17000583 RID: 1411
		// (get) Token: 0x06001C70 RID: 7280 RVA: 0x0004B26D File Offset: 0x0004946D
		// (set) Token: 0x06001C71 RID: 7281 RVA: 0x0000220F File Offset: 0x0000040F
		public byte[] Seed
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a value that indicates whether the secret agreement is used as a Hash-based Message Authentication Code (HMAC) key to derive key material.</summary>
		/// <returns>
		///     <see langword="true" /> if the secret agreement is used as an HMAC key to derive key material; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000584 RID: 1412
		// (get) Token: 0x06001C72 RID: 7282 RVA: 0x00053A3C File Offset: 0x00051C3C
		public bool UseSecretAgreementAsHmacKey
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Derives the key material that is generated from the secret agreement between two parties, given a <see cref="T:System.Security.Cryptography.CngKey" /> object that contains the second party's public key. </summary>
		/// <param name="otherPartyPublicKey">An object that contains the public part of the Elliptic Curve Diffie-Hellman (ECDH) key from the other party in the key exchange.</param>
		/// <returns>A byte array that contains the key material. This information is generated from the secret agreement that is calculated from the current object's private key and the specified public key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="otherPartyPublicKey" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="otherPartyPublicKey" /> is invalid. Either its <see cref="P:System.Security.Cryptography.CngKey.AlgorithmGroup" /> property does not specify <see cref="P:System.Security.Cryptography.CngAlgorithmGroup.ECDiffieHellman" /> or its key size does not match the key size of this instance.</exception>
		/// <exception cref="T:System.InvalidOperationException">This object's <see cref="P:System.Security.Cryptography.ECDiffieHellmanCng.KeyDerivationFunction" /> property specifies the <see cref="F:System.Security.Cryptography.ECDiffieHellmanKeyDerivationFunction.Tls" /> key derivation function, but either <see cref="P:System.Security.Cryptography.ECDiffieHellmanCng.Label" /> or <see cref="P:System.Security.Cryptography.ECDiffieHellmanCng.Seed" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">All other errors.</exception>
		// Token: 0x06001C73 RID: 7283 RVA: 0x0004B26D File Offset: 0x0004946D
		[SecuritySafeCritical]
		public byte[] DeriveKeyMaterial(CngKey otherPartyPublicKey)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets a handle to the secret agreement generated between two parties, given a <see cref="T:System.Security.Cryptography.CngKey" /> object that contains the second party's public key.</summary>
		/// <param name="otherPartyPublicKey">An object that contains the public part of the Elliptic Curve Diffie-Hellman (ECDH) key from the other party in the key exchange.</param>
		/// <returns>A handle to the secret agreement. This information is calculated from the current object's private key and the specified public key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="otherPartyPublicKey" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="otherPartyPublicKey" /> is not an ECDH key, or it is not the correct size.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">All other errors.</exception>
		// Token: 0x06001C74 RID: 7284 RVA: 0x0004B26D File Offset: 0x0004946D
		[SecurityCritical]
		[SecurityPermission(SecurityAction.Demand, UnmanagedCode = true)]
		public SafeNCryptSecretHandle DeriveSecretAgreementHandle(CngKey otherPartyPublicKey)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets a handle to the secret agreement generated between two parties, given an <see cref="T:System.Security.Cryptography.ECDiffieHellmanPublicKey" /> object that contains the second party's public key.</summary>
		/// <param name="otherPartyPublicKey">The public key from the other party in the key exchange.</param>
		/// <returns>A handle to the secret agreement. This information is calculated from the current object's private key and the specified public key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="otherPartyPublicKey" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="otherPartyPublicKey" /> is not an <see cref="T:System.Security.Cryptography.ECDiffieHellmanPublicKey" /> key. </exception>
		// Token: 0x06001C75 RID: 7285 RVA: 0x0004B26D File Offset: 0x0004946D
		public SafeNCryptSecretHandle DeriveSecretAgreementHandle(ECDiffieHellmanPublicKey otherPartyPublicKey)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Deserializes the key information from an XML string by using the specified format.</summary>
		/// <param name="xml">The XML-based key information to be deserialized.</param>
		/// <param name="format">One of the enumeration values that specifies the format of the XML string. The only currently accepted format is <see cref="F:System.Security.Cryptography.ECKeyXmlFormat.Rfc4050" />.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="xml" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="xml" /> is malformed.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="format" /> specifies an invalid format. The only accepted value is <see cref="F:System.Security.Cryptography.ECKeyXmlFormat.Rfc4050" />.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">All other errors.</exception>
		// Token: 0x06001C76 RID: 7286 RVA: 0x0000220F File Offset: 0x0000040F
		public void FromXmlString(string xml, ECKeyXmlFormat format)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Serializes the key information to an XML string by using the specified format.</summary>
		/// <param name="format">One of the enumeration values that specifies the format of the XML string. The only currently accepted format is <see cref="F:System.Security.Cryptography.ECKeyXmlFormat.Rfc4050" />.</param>
		/// <returns>A string object that contains the key information, serialized to an XML string, according to the requested format.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="format" /> specifies an invalid format. The only accepted value is <see cref="F:System.Security.Cryptography.ECKeyXmlFormat.Rfc4050" />.</exception>
		// Token: 0x06001C77 RID: 7287 RVA: 0x0004B26D File Offset: 0x0004946D
		public string ToXmlString(ECKeyXmlFormat format)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
