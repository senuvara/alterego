﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Security.Permissions;
using Unity;

namespace System.Security.Cryptography
{
	/// <summary>Provides information for a manifest signature. </summary>
	// Token: 0x0200048E RID: 1166
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class ManifestSignatureInformation
	{
		// Token: 0x06001C7D RID: 7293 RVA: 0x0000220F File Offset: 0x0000040F
		internal ManifestSignatureInformation()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the Authenticode signature information for a manifest. </summary>
		/// <returns>An <see cref="T:System.Security.Cryptography.X509Certificates.AuthenticodeSignatureInformation" /> object that contains Authenticode signature information for the manifest, or <see langword="null" /> if there is no signature.</returns>
		// Token: 0x17000586 RID: 1414
		// (get) Token: 0x06001C7E RID: 7294 RVA: 0x0004B26D File Offset: 0x0004946D
		public AuthenticodeSignatureInformation AuthenticodeSignature
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the type of a manifest.</summary>
		/// <returns>One of the <see cref="T:System.Security.ManifestKinds" /> values.</returns>
		// Token: 0x17000587 RID: 1415
		// (get) Token: 0x06001C7F RID: 7295 RVA: 0x00053A58 File Offset: 0x00051C58
		public ManifestKinds Manifest
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ManifestKinds.None;
			}
		}

		/// <summary>Gets the details of the strong name signature of a manifest.</summary>
		/// <returns>A <see cref="P:System.Security.Cryptography.ManifestSignatureInformation.StrongNameSignature" /> object that contains the signature, or <see langword="null" /> if there is no strong name signature.</returns>
		// Token: 0x17000588 RID: 1416
		// (get) Token: 0x06001C80 RID: 7296 RVA: 0x0004B26D File Offset: 0x0004946D
		public StrongNameSignatureInformation StrongNameSignature
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gathers and verifies information about the signatures of manifests that belong to a specified activation context.</summary>
		/// <param name="application">The activation context of the manifest. Activation contexts belong to an application and contain multiple manifests.</param>
		/// <returns>A collection that contains a <see cref="T:System.Security.Cryptography.ManifestSignatureInformation" /> object for each manifest that is verified.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="application" /> parameter is <see langword="null" />.</exception>
		// Token: 0x06001C81 RID: 7297 RVA: 0x0004B26D File Offset: 0x0004946D
		public static ManifestSignatureInformationCollection VerifySignature(ActivationContext application)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gathers and verifies information about the signatures of manifests that belong to a specified activation context and manifest type.</summary>
		/// <param name="application">The activation context of the manifest. Activation contexts belong to an application and contain multiple manifests.</param>
		/// <param name="manifests">The type of manifest. This parameter specifies which manifests in the activation context you want to verify.</param>
		/// <returns>A collection that contains a <see cref="T:System.Security.Cryptography.ManifestSignatureInformation" /> object for each manifest that is verified.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="application" /> parameter is <see langword="null" />.</exception>
		// Token: 0x06001C82 RID: 7298 RVA: 0x0004B26D File Offset: 0x0004946D
		public static ManifestSignatureInformationCollection VerifySignature(ActivationContext application, ManifestKinds manifests)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gathers and verifies information about the signatures of manifests that belong to a specified activation context and manifest type, and allows certificates to be selected for revocation.</summary>
		/// <param name="application">The application context of the manifests. Activation contexts belong to an application and contain multiple manifests.</param>
		/// <param name="manifests">The type of manifest. This parameter specifies which manifests in the activation context you want to verify.</param>
		/// <param name="revocationFlag">One of the enumeration values that specifies which certificates in the chain are checked for revocation. The default is <see cref="F:System.Security.Cryptography.X509Certificates.X509RevocationFlag.ExcludeRoot" />.</param>
		/// <param name="revocationMode">Determines whether the X.509 verification should look online for revocation lists. </param>
		/// <returns>A collection that contains a <see cref="T:System.Security.Cryptography.ManifestSignatureInformation" /> object for each manifest that is verified.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="application" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">A value specified for the <paramref name="revocationFlag" /> or <paramref name="revocationMode" /> parameter is invalid.</exception>
		// Token: 0x06001C83 RID: 7299 RVA: 0x0004B26D File Offset: 0x0004946D
		[SecuritySafeCritical]
		public static ManifestSignatureInformationCollection VerifySignature(ActivationContext application, ManifestKinds manifests, X509RevocationFlag revocationFlag, X509RevocationMode revocationMode)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
