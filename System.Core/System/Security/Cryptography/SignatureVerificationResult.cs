﻿using System;

namespace System.Security.Cryptography
{
	/// <summary>Specifies most of the result codes for signature verification. </summary>
	// Token: 0x02000491 RID: 1169
	public enum SignatureVerificationResult
	{
		/// <summary>The identity of the assembly specified in the /asm:assembly/asm:assemblyIdentity node of the manifest does not match the identity of the assembly in the Authenticode signature in the /asm:assembly/ds:signature/ds:KeyInfo/msrel:RelData/r:license/r:grant/as:ManifestInformation/as:assemblyIdentity node.</summary>
		// Token: 0x04000D05 RID: 3333
		AssemblyIdentityMismatch = 1,
		/// <summary>The digital signature of the object did not verify.</summary>
		// Token: 0x04000D06 RID: 3334
		BadDigest = -2146869232,
		/// <summary>The signature format is invalid.</summary>
		// Token: 0x04000D07 RID: 3335
		BadSignatureFormat = -2146762749,
		/// <summary>The basic constraint extension of a certificate has not been observed.</summary>
		// Token: 0x04000D08 RID: 3336
		BasicConstraintsNotObserved = -2146869223,
		/// <summary>The certificate has expired.</summary>
		// Token: 0x04000D09 RID: 3337
		CertificateExpired = -2146762495,
		/// <summary>The certificate was explicitly marked as not trusted by the user.</summary>
		// Token: 0x04000D0A RID: 3338
		CertificateExplicitlyDistrusted = -2146762479,
		/// <summary>The certificate is missing or has an empty value for an important field, such as a subject or issuer name.</summary>
		// Token: 0x04000D0B RID: 3339
		CertificateMalformed = -2146762488,
		/// <summary>The certificate is not trusted explicitly.</summary>
		// Token: 0x04000D0C RID: 3340
		CertificateNotExplicitlyTrusted = -2146762748,
		/// <summary>The certificate has been revoked.</summary>
		// Token: 0x04000D0D RID: 3341
		CertificateRevoked = -2146762484,
		/// <summary>The certificate cannot be used for signing and verification.</summary>
		// Token: 0x04000D0E RID: 3342
		CertificateUsageNotAllowed = -2146762490,
		/// <summary>The strong name signature does not verify in the <see cref="T:System.Security.Cryptography.X509Certificates.AuthenticodeSignatureInformation" /> object. Because the strong name signature wraps the Authenticode signature, someone could replace the Authenticode signature with a signature of their choosing. To prevent this, this error code is returned if the strong name does not verify because substituting a part of the strong name signature will invalidate it.</summary>
		// Token: 0x04000D0F RID: 3343
		ContainingSignatureInvalid = 2,
		/// <summary>The chain could not be built.</summary>
		// Token: 0x04000D10 RID: 3344
		CouldNotBuildChain = -2146762486,
		/// <summary>There is a general trust failure with the certificate.</summary>
		// Token: 0x04000D11 RID: 3345
		GenericTrustFailure,
		/// <summary>The certificate has an invalid name. The name is either not included in the permitted list or is explicitly excluded.</summary>
		// Token: 0x04000D12 RID: 3346
		InvalidCertificateName = -2146762476,
		/// <summary>The certificate has an invalid policy.</summary>
		// Token: 0x04000D13 RID: 3347
		InvalidCertificatePolicy = -2146762477,
		/// <summary>The certificate has an invalid role.</summary>
		// Token: 0x04000D14 RID: 3348
		InvalidCertificateRole = -2146762493,
		/// <summary>The signature of the certificate cannot be verified.</summary>
		// Token: 0x04000D15 RID: 3349
		InvalidCertificateSignature = -2146869244,
		/// <summary>The certificate has an invalid usage.</summary>
		// Token: 0x04000D16 RID: 3350
		InvalidCertificateUsage = -2146762480,
		/// <summary>One of the counter signatures is invalid.</summary>
		// Token: 0x04000D17 RID: 3351
		InvalidCountersignature = -2146869245,
		/// <summary>The certificate for the signer of the message is invalid or not found.</summary>
		// Token: 0x04000D18 RID: 3352
		InvalidSignerCertificate = -2146869246,
		/// <summary>A certificate was issued after the issuing certificate has expired.</summary>
		// Token: 0x04000D19 RID: 3353
		InvalidTimePeriodNesting = -2146762494,
		/// <summary>The time stamp signature or certificate could not be verified or is malformed.</summary>
		// Token: 0x04000D1A RID: 3354
		InvalidTimestamp = -2146869243,
		/// <summary>A parent of a given certificate did not issue that child certificate.</summary>
		// Token: 0x04000D1B RID: 3355
		IssuerChainingError = -2146762489,
		/// <summary>The signature is missing.</summary>
		// Token: 0x04000D1C RID: 3356
		MissingSignature = -2146762496,
		/// <summary>A path length constraint in the certification chain has been violated.</summary>
		// Token: 0x04000D1D RID: 3357
		PathLengthConstraintViolated = -2146762492,
		/// <summary>The public key token from the manifest identity in the /asm:assembly/asm:AssemblyIdentity node does not match the public key token of the key that is used to sign the manifest.</summary>
		// Token: 0x04000D1E RID: 3358
		PublicKeyTokenMismatch = 3,
		/// <summary>The publisher name from /asm:assembly/asmv2:publisherIdentity does not match the subject name of the signing certificate, or the issuer key hash from the same publisherIdentity node does not match the key hash of the signing certificate.</summary>
		// Token: 0x04000D1F RID: 3359
		PublisherMismatch,
		/// <summary>The revocation check failed.</summary>
		// Token: 0x04000D20 RID: 3360
		RevocationCheckFailure = -2146762482,
		/// <summary>A system-level error occurred while verifying trust.</summary>
		// Token: 0x04000D21 RID: 3361
		SystemError = -2146869247,
		/// <summary>A certificate contains an unknown extension that is marked critical.</summary>
		// Token: 0x04000D22 RID: 3362
		UnknownCriticalExtension = -2146762491,
		/// <summary>The certificate has an unknown trust provider.</summary>
		// Token: 0x04000D23 RID: 3363
		UnknownTrustProvider = -2146762751,
		/// <summary>The certificate has an unknown verification action.</summary>
		// Token: 0x04000D24 RID: 3364
		UnknownVerificationAction,
		/// <summary>The certification chain processed correctly, but one of the CA certificates is not trusted by the policy provider.</summary>
		// Token: 0x04000D25 RID: 3365
		UntrustedCertificationAuthority = -2146762478,
		/// <summary>The root certificate is not trusted.</summary>
		// Token: 0x04000D26 RID: 3366
		UntrustedRootCertificate = -2146762487,
		/// <summary>The test root certificate is not trusted.</summary>
		// Token: 0x04000D27 RID: 3367
		UntrustedTestRootCertificate = -2146762483,
		/// <summary>The certificate verification result is valid.</summary>
		// Token: 0x04000D28 RID: 3368
		Valid = 0
	}
}
