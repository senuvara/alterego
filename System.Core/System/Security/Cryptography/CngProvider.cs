﻿using System;
using System.Security.Permissions;

namespace System.Security.Cryptography
{
	/// <summary>Encapsulates the name of a key storage provider (KSP) for use with Cryptography Next Generation (CNG) objects.</summary>
	// Token: 0x02000053 RID: 83
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	[Serializable]
	public sealed class CngProvider : IEquatable<CngProvider>
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.CngProvider" /> class.</summary>
		/// <param name="provider">The name of the key storage provider (KSP) to initialize.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="provider" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="provider" /> parameter length is 0 (zero).</exception>
		// Token: 0x0600020B RID: 523 RVA: 0x00005218 File Offset: 0x00003418
		public CngProvider(string provider)
		{
			if (provider == null)
			{
				throw new ArgumentNullException("provider");
			}
			if (provider.Length == 0)
			{
				throw new ArgumentException(SR.GetString("The provider name '{0}' is invalid.", new object[]
				{
					provider
				}), "provider");
			}
			this.m_provider = provider;
		}

		/// <summary>Gets the name of the key storage provider (KSP) that the current <see cref="T:System.Security.Cryptography.CngProvider" /> object specifies.</summary>
		/// <returns>The embedded KSP name.</returns>
		// Token: 0x17000078 RID: 120
		// (get) Token: 0x0600020C RID: 524 RVA: 0x00005267 File Offset: 0x00003467
		public string Provider
		{
			get
			{
				return this.m_provider;
			}
		}

		/// <summary>Determines whether two <see cref="T:System.Security.Cryptography.CngProvider" /> objects specify the same key storage provider (KSP).</summary>
		/// <param name="left">An object that specifies a KSP.</param>
		/// <param name="right">A second object, to be compared to the object that is identified by the <paramref name="left" /> parameter.</param>
		/// <returns>
		///     <see langword="true" /> if the two objects represent the same KSP; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600020D RID: 525 RVA: 0x0000526F File Offset: 0x0000346F
		public static bool operator ==(CngProvider left, CngProvider right)
		{
			if (left == null)
			{
				return right == null;
			}
			return left.Equals(right);
		}

		/// <summary>Determines whether two <see cref="T:System.Security.Cryptography.CngProvider" /> objects do not represent the same key storage provider (KSP).</summary>
		/// <param name="left">An object that specifies a KSP.</param>
		/// <param name="right">A second object, to be compared to the object that is identified by the <paramref name="left" /> parameter.</param>
		/// <returns>
		///     <see langword="true" /> if the two objects do not represent the same KSP; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600020E RID: 526 RVA: 0x00005280 File Offset: 0x00003480
		public static bool operator !=(CngProvider left, CngProvider right)
		{
			if (left == null)
			{
				return right != null;
			}
			return !left.Equals(right);
		}

		/// <summary>Compares the specified object to the current <see cref="T:System.Security.Cryptography.CngProvider" /> object.</summary>
		/// <param name="obj">An object to be compared to the current <see cref="T:System.Security.Cryptography.CngProvider" /> object.</param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="obj" /> parameter is a <see cref="T:System.Security.Cryptography.CngProvider" /> that specifies the same key storage provider(KSP) as the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600020F RID: 527 RVA: 0x00005294 File Offset: 0x00003494
		public override bool Equals(object obj)
		{
			return this.Equals(obj as CngProvider);
		}

		/// <summary>Compares the specified <see cref="T:System.Security.Cryptography.CngProvider" /> object to the current <see cref="T:System.Security.Cryptography.CngProvider" /> object.</summary>
		/// <param name="other">An object to be compared to the current <see cref="T:System.Security.Cryptography.CngProvider" /> object.</param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="other" /> parameter specifies the same key storage provider (KSP) as the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000210 RID: 528 RVA: 0x000052A2 File Offset: 0x000034A2
		public bool Equals(CngProvider other)
		{
			return other != null && this.m_provider.Equals(other.Provider);
		}

		/// <summary>Generates a hash value for the name of the key storage provider (KSP) that is embedded in the current <see cref="T:System.Security.Cryptography.CngProvider" /> object.</summary>
		/// <returns>The hash value of the embedded KSP name.</returns>
		// Token: 0x06000211 RID: 529 RVA: 0x000052BA File Offset: 0x000034BA
		public override int GetHashCode()
		{
			return this.m_provider.GetHashCode();
		}

		/// <summary>Gets the name of the key storage provider (KSP) that the current <see cref="T:System.Security.Cryptography.CngProvider" /> object specifies.</summary>
		/// <returns>The embedded KSP name.</returns>
		// Token: 0x06000212 RID: 530 RVA: 0x000052C7 File Offset: 0x000034C7
		public override string ToString()
		{
			return this.m_provider.ToString();
		}

		/// <summary>Gets a <see cref="T:System.Security.Cryptography.CngProvider" /> object that specifies the Microsoft Smart Card Key Storage Provider.</summary>
		/// <returns>An object that specifies the Microsoft Smart Card Key Storage Provider.</returns>
		// Token: 0x17000079 RID: 121
		// (get) Token: 0x06000213 RID: 531 RVA: 0x000052D4 File Offset: 0x000034D4
		public static CngProvider MicrosoftSmartCardKeyStorageProvider
		{
			get
			{
				if (CngProvider.s_msSmartCardKsp == null)
				{
					CngProvider.s_msSmartCardKsp = new CngProvider("Microsoft Smart Card Key Storage Provider");
				}
				return CngProvider.s_msSmartCardKsp;
			}
		}

		/// <summary>Gets a <see cref="T:System.Security.Cryptography.CngProvider" /> object that specifies the Microsoft Software Key Storage Provider.</summary>
		/// <returns>An object that specifies the Microsoft Software Key Storage Provider.</returns>
		// Token: 0x1700007A RID: 122
		// (get) Token: 0x06000214 RID: 532 RVA: 0x000052FD File Offset: 0x000034FD
		public static CngProvider MicrosoftSoftwareKeyStorageProvider
		{
			get
			{
				if (CngProvider.s_msSoftwareKsp == null)
				{
					CngProvider.s_msSoftwareKsp = new CngProvider("Microsoft Software Key Storage Provider");
				}
				return CngProvider.s_msSoftwareKsp;
			}
		}

		// Token: 0x04000292 RID: 658
		private static volatile CngProvider s_msSmartCardKsp;

		// Token: 0x04000293 RID: 659
		private static volatile CngProvider s_msSoftwareKsp;

		// Token: 0x04000294 RID: 660
		private string m_provider;
	}
}
