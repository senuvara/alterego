﻿using System;

namespace System.Security.Cryptography
{
	/// <summary>Specifies options for opening a key.</summary>
	// Token: 0x0200005C RID: 92
	[Flags]
	public enum CngKeyOpenOptions
	{
		/// <summary>No key open options are specified.</summary>
		// Token: 0x040002A9 RID: 681
		None = 0,
		/// <summary>If the <see cref="F:System.Security.Cryptography.CngKeyOpenOptions.MachineKey" /> value is not specified, a user key is opened instead.</summary>
		// Token: 0x040002AA RID: 682
		UserKey = 0,
		/// <summary>A machine-wide key is opened.</summary>
		// Token: 0x040002AB RID: 683
		MachineKey = 32,
		/// <summary>UI prompting is suppressed.</summary>
		// Token: 0x040002AC RID: 684
		Silent = 64
	}
}
