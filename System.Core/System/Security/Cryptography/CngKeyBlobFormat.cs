﻿using System;
using System.Security.Permissions;

namespace System.Security.Cryptography
{
	/// <summary>Specifies a key BLOB format for use with Microsoft Cryptography Next Generation (CNG) objects. </summary>
	// Token: 0x0200004F RID: 79
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	[Serializable]
	public sealed class CngKeyBlobFormat : IEquatable<CngKeyBlobFormat>
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.CngKeyBlobFormat" /> class by using the specified format.</summary>
		/// <param name="format">The key BLOB format to initialize.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="format" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="format" /> parameter length is 0 (zero).</exception>
		// Token: 0x060001E1 RID: 481 RVA: 0x00004D9C File Offset: 0x00002F9C
		public CngKeyBlobFormat(string format)
		{
			if (format == null)
			{
				throw new ArgumentNullException("format");
			}
			if (format.Length == 0)
			{
				throw new ArgumentException(SR.GetString("The key blob format '{0}' is invalid.", new object[]
				{
					format
				}), "format");
			}
			this.m_format = format;
		}

		/// <summary>Gets the name of the key BLOB format that the current <see cref="T:System.Security.Cryptography.CngKeyBlobFormat" /> object specifies.</summary>
		/// <returns>The embedded key BLOB format name.</returns>
		// Token: 0x17000064 RID: 100
		// (get) Token: 0x060001E2 RID: 482 RVA: 0x00004DEB File Offset: 0x00002FEB
		public string Format
		{
			get
			{
				return this.m_format;
			}
		}

		/// <summary>Determines whether two <see cref="T:System.Security.Cryptography.CngKeyBlobFormat" /> objects specify the same key BLOB format.</summary>
		/// <param name="left">An object that specifies a key BLOB format.</param>
		/// <param name="right">A second object, to be compared to the object identified by the <paramref name="left" /> parameter.</param>
		/// <returns>
		///     <see langword="true" /> if the two objects specify the same key BLOB format; otherwise, <see langword="false" />.</returns>
		// Token: 0x060001E3 RID: 483 RVA: 0x00004DF3 File Offset: 0x00002FF3
		public static bool operator ==(CngKeyBlobFormat left, CngKeyBlobFormat right)
		{
			if (left == null)
			{
				return right == null;
			}
			return left.Equals(right);
		}

		/// <summary>Determines whether two <see cref="T:System.Security.Cryptography.CngKeyBlobFormat" /> objects do not specify the same key BLOB format.</summary>
		/// <param name="left">An object that specifies a key BLOB format.</param>
		/// <param name="right">A second object, to be compared to the object identified by the <paramref name="left" /> parameter.</param>
		/// <returns>
		///     <see langword="true" /> if the two objects do not specify the same key BLOB format; otherwise, <see langword="false" />.</returns>
		// Token: 0x060001E4 RID: 484 RVA: 0x00004E04 File Offset: 0x00003004
		public static bool operator !=(CngKeyBlobFormat left, CngKeyBlobFormat right)
		{
			if (left == null)
			{
				return right != null;
			}
			return !left.Equals(right);
		}

		/// <summary>Compares the specified object to the current <see cref="T:System.Security.Cryptography.CngKeyBlobFormat" /> object.</summary>
		/// <param name="obj">An object to be compared to the current <see cref="T:System.Security.Cryptography.CngKeyBlobFormat" /> object.</param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="obj" /> parameter is a <see cref="T:System.Security.Cryptography.CngKeyBlobFormat" /> object that specifies the same key BLOB format as the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x060001E5 RID: 485 RVA: 0x00004E18 File Offset: 0x00003018
		public override bool Equals(object obj)
		{
			return this.Equals(obj as CngKeyBlobFormat);
		}

		/// <summary>Compares the specified <see cref="T:System.Security.Cryptography.CngKeyBlobFormat" /> object to the current <see cref="T:System.Security.Cryptography.CngKeyBlobFormat" /> object.</summary>
		/// <param name="other">An object to be compared to the current <see cref="T:System.Security.Cryptography.CngKeyBlobFormat" /> object.</param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="other" /> parameter specifies the same key BLOB format as the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x060001E6 RID: 486 RVA: 0x00004E26 File Offset: 0x00003026
		public bool Equals(CngKeyBlobFormat other)
		{
			return other != null && this.m_format.Equals(other.Format);
		}

		/// <summary>Generates a hash value for the embedded key BLOB format in the current <see cref="T:System.Security.Cryptography.CngKeyBlobFormat" /> object.</summary>
		/// <returns>The hash value of the embedded key BLOB format. </returns>
		// Token: 0x060001E7 RID: 487 RVA: 0x00004E3E File Offset: 0x0000303E
		public override int GetHashCode()
		{
			return this.m_format.GetHashCode();
		}

		/// <summary>Gets the name of the key BLOB format that the current <see cref="T:System.Security.Cryptography.CngKeyBlobFormat" /> object specifies.</summary>
		/// <returns>The embedded key BLOB format name.</returns>
		// Token: 0x060001E8 RID: 488 RVA: 0x00004DEB File Offset: 0x00002FEB
		public override string ToString()
		{
			return this.m_format;
		}

		/// <summary>Gets a <see cref="T:System.Security.Cryptography.CngKeyBlobFormat" /> object that specifies a private key BLOB for an elliptic curve cryptography (ECC) key.</summary>
		/// <returns>An object that specifies an ECC private key BLOB.</returns>
		// Token: 0x17000065 RID: 101
		// (get) Token: 0x060001E9 RID: 489 RVA: 0x00004E4B File Offset: 0x0000304B
		public static CngKeyBlobFormat EccPrivateBlob
		{
			get
			{
				if (CngKeyBlobFormat.s_eccPrivate == null)
				{
					CngKeyBlobFormat.s_eccPrivate = new CngKeyBlobFormat("ECCPRIVATEBLOB");
				}
				return CngKeyBlobFormat.s_eccPrivate;
			}
		}

		/// <summary>Gets a <see cref="T:System.Security.Cryptography.CngKeyBlobFormat" /> object that specifies a public key BLOB for an elliptic curve cryptography (ECC) key.</summary>
		/// <returns>An object that specifies an ECC public key BLOB.</returns>
		// Token: 0x17000066 RID: 102
		// (get) Token: 0x060001EA RID: 490 RVA: 0x00004E74 File Offset: 0x00003074
		public static CngKeyBlobFormat EccPublicBlob
		{
			get
			{
				if (CngKeyBlobFormat.s_eccPublic == null)
				{
					CngKeyBlobFormat.s_eccPublic = new CngKeyBlobFormat("ECCPUBLICBLOB");
				}
				return CngKeyBlobFormat.s_eccPublic;
			}
		}

		/// <summary>Gets a <see cref="T:System.Security.Cryptography.CngKeyBlobFormat" /> object that specifies a private key BLOB for an elliptic curve cryptography (ECC) key which contains explicit curve parameters.</summary>
		/// <returns>An object describing a private key BLOB.</returns>
		// Token: 0x17000067 RID: 103
		// (get) Token: 0x060001EB RID: 491 RVA: 0x00004E9D File Offset: 0x0000309D
		public static CngKeyBlobFormat EccFullPrivateBlob
		{
			get
			{
				if (CngKeyBlobFormat.s_eccFullPrivate == null)
				{
					CngKeyBlobFormat.s_eccFullPrivate = new CngKeyBlobFormat("ECCFULLPRIVATEBLOB");
				}
				return CngKeyBlobFormat.s_eccFullPrivate;
			}
		}

		/// <summary>Gets a <see cref="T:System.Security.Cryptography.CngKeyBlobFormat" /> object that specifies a public key BLOB for an elliptic curve cryptography (ECC) key which contains explicit curve parameters.</summary>
		/// <returns>An object describing a public key BLOB.</returns>
		// Token: 0x17000068 RID: 104
		// (get) Token: 0x060001EC RID: 492 RVA: 0x00004EC6 File Offset: 0x000030C6
		public static CngKeyBlobFormat EccFullPublicBlob
		{
			get
			{
				if (CngKeyBlobFormat.s_eccFullPublic == null)
				{
					CngKeyBlobFormat.s_eccFullPublic = new CngKeyBlobFormat("ECCFULLPUBLICBLOB");
				}
				return CngKeyBlobFormat.s_eccFullPublic;
			}
		}

		/// <summary>Gets a <see cref="T:System.Security.Cryptography.CngKeyBlobFormat" /> object that specifies a generic private key BLOB.</summary>
		/// <returns>An object that specifies a generic private key BLOB.</returns>
		// Token: 0x17000069 RID: 105
		// (get) Token: 0x060001ED RID: 493 RVA: 0x00004EEF File Offset: 0x000030EF
		public static CngKeyBlobFormat GenericPrivateBlob
		{
			get
			{
				if (CngKeyBlobFormat.s_genericPrivate == null)
				{
					CngKeyBlobFormat.s_genericPrivate = new CngKeyBlobFormat("PRIVATEBLOB");
				}
				return CngKeyBlobFormat.s_genericPrivate;
			}
		}

		/// <summary>Gets a <see cref="T:System.Security.Cryptography.CngKeyBlobFormat" /> object that specifies a generic public key BLOB.</summary>
		/// <returns>An object that specifies a generic public key BLOB.</returns>
		// Token: 0x1700006A RID: 106
		// (get) Token: 0x060001EE RID: 494 RVA: 0x00004F18 File Offset: 0x00003118
		public static CngKeyBlobFormat GenericPublicBlob
		{
			get
			{
				if (CngKeyBlobFormat.s_genericPublic == null)
				{
					CngKeyBlobFormat.s_genericPublic = new CngKeyBlobFormat("PUBLICBLOB");
				}
				return CngKeyBlobFormat.s_genericPublic;
			}
		}

		/// <summary>Gets a <see cref="T:System.Security.Cryptography.CngKeyBlobFormat" /> object that specifies an opaque transport key BLOB.</summary>
		/// <returns>An object that specifies an opaque transport key BLOB.</returns>
		// Token: 0x1700006B RID: 107
		// (get) Token: 0x060001EF RID: 495 RVA: 0x00004F41 File Offset: 0x00003141
		public static CngKeyBlobFormat OpaqueTransportBlob
		{
			get
			{
				if (CngKeyBlobFormat.s_opaqueTransport == null)
				{
					CngKeyBlobFormat.s_opaqueTransport = new CngKeyBlobFormat("OpaqueTransport");
				}
				return CngKeyBlobFormat.s_opaqueTransport;
			}
		}

		/// <summary>Gets a <see cref="T:System.Security.Cryptography.CngKeyBlobFormat" /> object that specifies a Private Key Information Syntax Standard (PKCS #8) key BLOB.</summary>
		/// <returns>An object that specifies a PKCS #8 private key BLOB.</returns>
		// Token: 0x1700006C RID: 108
		// (get) Token: 0x060001F0 RID: 496 RVA: 0x00004F6A File Offset: 0x0000316A
		public static CngKeyBlobFormat Pkcs8PrivateBlob
		{
			get
			{
				if (CngKeyBlobFormat.s_pkcs8Private == null)
				{
					CngKeyBlobFormat.s_pkcs8Private = new CngKeyBlobFormat("PKCS8_PRIVATEKEY");
				}
				return CngKeyBlobFormat.s_pkcs8Private;
			}
		}

		// Token: 0x0400027E RID: 638
		private static volatile CngKeyBlobFormat s_eccPrivate;

		// Token: 0x0400027F RID: 639
		private static volatile CngKeyBlobFormat s_eccPublic;

		// Token: 0x04000280 RID: 640
		private static volatile CngKeyBlobFormat s_eccFullPrivate;

		// Token: 0x04000281 RID: 641
		private static volatile CngKeyBlobFormat s_eccFullPublic;

		// Token: 0x04000282 RID: 642
		private static volatile CngKeyBlobFormat s_genericPrivate;

		// Token: 0x04000283 RID: 643
		private static volatile CngKeyBlobFormat s_genericPublic;

		// Token: 0x04000284 RID: 644
		private static volatile CngKeyBlobFormat s_opaqueTransport;

		// Token: 0x04000285 RID: 645
		private static volatile CngKeyBlobFormat s_pkcs8Private;

		// Token: 0x04000286 RID: 646
		private string m_format;
	}
}
