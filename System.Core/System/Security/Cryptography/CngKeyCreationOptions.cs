﻿using System;

namespace System.Security.Cryptography
{
	/// <summary>Specifies options used for key creation.</summary>
	// Token: 0x0200005B RID: 91
	[Flags]
	public enum CngKeyCreationOptions
	{
		/// <summary>No key creation options are used.</summary>
		// Token: 0x040002A5 RID: 677
		None = 0,
		/// <summary>A machine-wide key is created.</summary>
		// Token: 0x040002A6 RID: 678
		MachineKey = 32,
		/// <summary>The existing key is overwritten during key creation.</summary>
		// Token: 0x040002A7 RID: 679
		OverwriteExistingKey = 128
	}
}
