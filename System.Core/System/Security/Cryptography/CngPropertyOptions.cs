﻿using System;

namespace System.Security.Cryptography
{
	/// <summary>Specifies Cryptography Next Generation (CNG) key property options.</summary>
	// Token: 0x0200005F RID: 95
	[Flags]
	public enum CngPropertyOptions
	{
		/// <summary>The referenced property has no options.</summary>
		// Token: 0x040002B7 RID: 695
		None = 0,
		/// <summary>The property is not specified by CNG. Use this option to avoid future name conflicts with CNG properties.</summary>
		// Token: 0x040002B8 RID: 696
		CustomProperty = 1073741824,
		/// <summary>The property should be persisted.</summary>
		// Token: 0x040002B9 RID: 697
		Persist = -2147483648
	}
}
