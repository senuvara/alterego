﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x02000046 RID: 70
	internal enum AsymmetricPaddingMode
	{
		// Token: 0x0400024D RID: 589
		None = 1,
		// Token: 0x0400024E RID: 590
		Pkcs1,
		// Token: 0x0400024F RID: 591
		Oaep = 4,
		// Token: 0x04000250 RID: 592
		Pss = 8
	}
}
