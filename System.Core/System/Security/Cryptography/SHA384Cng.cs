﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Security.Cryptography
{
	/// <summary>Provides a Cryptography Next Generation (CNG) implementation of the Secure Hash Algorithm (SHA) for 384-bit hash values.</summary>
	// Token: 0x02000497 RID: 1175
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class SHA384Cng : SHA384
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.SHA384Cng" /> class. </summary>
		// Token: 0x06001CA9 RID: 7337 RVA: 0x0000220F File Offset: 0x0000040F
		public SHA384Cng()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06001CAA RID: 7338 RVA: 0x0000220F File Offset: 0x0000040F
		protected override void HashCore(byte[] array, int ibStart, int cbSize)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06001CAB RID: 7339 RVA: 0x0004B26D File Offset: 0x0004946D
		protected override byte[] HashFinal()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Initializes, or re-initializes, the instance of the hash algorithm. </summary>
		// Token: 0x06001CAC RID: 7340 RVA: 0x0000220F File Offset: 0x0000040F
		public override void Initialize()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
