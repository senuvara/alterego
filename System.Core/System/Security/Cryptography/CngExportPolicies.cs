﻿using System;

namespace System.Security.Cryptography
{
	/// <summary>Specifies the key export policies for a key. </summary>
	// Token: 0x0200005A RID: 90
	[Flags]
	public enum CngExportPolicies
	{
		/// <summary>No export policies are established. Key export is allowed without restriction.</summary>
		// Token: 0x0400029F RID: 671
		None = 0,
		/// <summary>The private key can be exported multiple times.</summary>
		// Token: 0x040002A0 RID: 672
		AllowExport = 1,
		/// <summary>The private key can be exported multiple times as plaintext.</summary>
		// Token: 0x040002A1 RID: 673
		AllowPlaintextExport = 2,
		/// <summary>The private key can be exported one time for archiving purposes.</summary>
		// Token: 0x040002A2 RID: 674
		AllowArchiving = 4,
		/// <summary>The private key can be exported one time as plaintext.</summary>
		// Token: 0x040002A3 RID: 675
		AllowPlaintextArchiving = 8
	}
}
