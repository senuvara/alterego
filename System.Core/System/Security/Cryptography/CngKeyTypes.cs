﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x0200005D RID: 93
	[Flags]
	internal enum CngKeyTypes
	{
		// Token: 0x040002AE RID: 686
		None = 0,
		// Token: 0x040002AF RID: 687
		MachineKey = 32
	}
}
