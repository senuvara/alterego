﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Security.Cryptography
{
	/// <summary>Provides a Cryptography Next Generation (CNG) implementation of the Secure Hash Algorithm (SHA) for 256-bit hash values.</summary>
	// Token: 0x02000496 RID: 1174
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class SHA256Cng : SHA256
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.SHA256Cng" /> class. </summary>
		// Token: 0x06001CA5 RID: 7333 RVA: 0x0000220F File Offset: 0x0000040F
		public SHA256Cng()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06001CA6 RID: 7334 RVA: 0x0000220F File Offset: 0x0000040F
		protected override void HashCore(byte[] array, int ibStart, int cbSize)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06001CA7 RID: 7335 RVA: 0x0004B26D File Offset: 0x0004946D
		protected override byte[] HashFinal()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Initializes, or re-initializes, the instance of the hash algorithm. </summary>
		// Token: 0x06001CA8 RID: 7336 RVA: 0x0000220F File Offset: 0x0000040F
		public override void Initialize()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
