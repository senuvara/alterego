﻿using System;

namespace System.Security.Cryptography
{
	/// <summary>Specifies the protection level for the key in user interface (UI) prompting scenarios.</summary>
	// Token: 0x02000060 RID: 96
	[Flags]
	public enum CngUIProtectionLevels
	{
		/// <summary>No UI prompt is displayed when the key is accessed.</summary>
		// Token: 0x040002BB RID: 699
		None = 0,
		/// <summary>A UI prompt is displayed the first time the key is accessed in a process.</summary>
		// Token: 0x040002BC RID: 700
		ProtectKey = 1,
		/// <summary>A UI prompt is displayed every time the key is accessed.</summary>
		// Token: 0x040002BD RID: 701
		ForceHighProtection = 2
	}
}
