﻿using System;
using System.Security.Permissions;

namespace System.Security.Cryptography
{
	/// <summary>Encapsulates the name of an encryption algorithm group. </summary>
	// Token: 0x0200004C RID: 76
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	[Serializable]
	public sealed class CngAlgorithmGroup : IEquatable<CngAlgorithmGroup>
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.CngAlgorithmGroup" /> class.</summary>
		/// <param name="algorithmGroup">The name of the algorithm group to initialize.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="algorithmGroup" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="algorithmGroup" /> parameter length is 0 (zero).</exception>
		// Token: 0x060001B1 RID: 433 RVA: 0x00004C20 File Offset: 0x00002E20
		public CngAlgorithmGroup(string algorithmGroup)
		{
			if (algorithmGroup == null)
			{
				throw new ArgumentNullException("algorithmGroup");
			}
			if (algorithmGroup.Length == 0)
			{
				throw new ArgumentException(SR.GetString("The algorithm group '{0}' is invalid.", new object[]
				{
					algorithmGroup
				}), "algorithmGroup");
			}
			this.m_algorithmGroup = algorithmGroup;
		}

		/// <summary>Gets the name of the algorithm group that the current <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object specifies.</summary>
		/// <returns>The embedded algorithm group name.</returns>
		// Token: 0x17000050 RID: 80
		// (get) Token: 0x060001B2 RID: 434 RVA: 0x00004C6F File Offset: 0x00002E6F
		public string AlgorithmGroup
		{
			get
			{
				return this.m_algorithmGroup;
			}
		}

		/// <summary>Determines whether two <see cref="T:System.Security.Cryptography.CngAlgorithmGroup" /> objects specify the same algorithm group.</summary>
		/// <param name="left">An object that specifies an algorithm group.</param>
		/// <param name="right">A second object, to be compared to the object that is identified by the <paramref name="left" /> parameter.</param>
		/// <returns>
		///     <see langword="true" /> if the two objects specify the same algorithm group; otherwise, <see langword="false" />.</returns>
		// Token: 0x060001B3 RID: 435 RVA: 0x00004C77 File Offset: 0x00002E77
		public static bool operator ==(CngAlgorithmGroup left, CngAlgorithmGroup right)
		{
			if (left == null)
			{
				return right == null;
			}
			return left.Equals(right);
		}

		/// <summary>Determines whether two <see cref="T:System.Security.Cryptography.CngAlgorithmGroup" /> objects do not specify the same algorithm group.</summary>
		/// <param name="left">An object that specifies an algorithm group.</param>
		/// <param name="right">A second object, to be compared to the object that is identified by the <paramref name="left" /> parameter.</param>
		/// <returns>
		///     <see langword="true" /> if the two objects do not specify the same algorithm group; otherwise, <see langword="false" />. </returns>
		// Token: 0x060001B4 RID: 436 RVA: 0x00004C88 File Offset: 0x00002E88
		public static bool operator !=(CngAlgorithmGroup left, CngAlgorithmGroup right)
		{
			if (left == null)
			{
				return right != null;
			}
			return !left.Equals(right);
		}

		/// <summary>Compares the specified object to the current <see cref="T:System.Security.Cryptography.CngAlgorithmGroup" /> object.</summary>
		/// <param name="obj">An object to be compared to the current <see cref="T:System.Security.Cryptography.CngAlgorithmGroup" /> object.</param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="obj" /> parameter is a <see cref="T:System.Security.Cryptography.CngAlgorithmGroup" /> that specifies the same algorithm group as the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x060001B5 RID: 437 RVA: 0x00004C9C File Offset: 0x00002E9C
		public override bool Equals(object obj)
		{
			return this.Equals(obj as CngAlgorithmGroup);
		}

		/// <summary>Compares the specified <see cref="T:System.Security.Cryptography.CngAlgorithmGroup" /> object to the current <see cref="T:System.Security.Cryptography.CngAlgorithmGroup" /> object.</summary>
		/// <param name="other">An object to be compared to the current <see cref="T:System.Security.Cryptography.CngAlgorithmGroup" /> object.</param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="other" /> parameter specifies the same algorithm group as the current object; otherwise, <see langword="false" />.</returns>
		// Token: 0x060001B6 RID: 438 RVA: 0x00004CAA File Offset: 0x00002EAA
		public bool Equals(CngAlgorithmGroup other)
		{
			return other != null && this.m_algorithmGroup.Equals(other.AlgorithmGroup);
		}

		/// <summary>Generates a hash value for the algorithm group name that is embedded in the current <see cref="T:System.Security.Cryptography.CngAlgorithmGroup" /> object.</summary>
		/// <returns>The hash value of the embedded algorithm group name.</returns>
		// Token: 0x060001B7 RID: 439 RVA: 0x00004CC2 File Offset: 0x00002EC2
		public override int GetHashCode()
		{
			return this.m_algorithmGroup.GetHashCode();
		}

		/// <summary>Gets the name of the algorithm group that the current <see cref="T:System.Security.Cryptography.CngAlgorithm" /> object specifies.</summary>
		/// <returns>The embedded algorithm group name.</returns>
		// Token: 0x060001B8 RID: 440 RVA: 0x00004C6F File Offset: 0x00002E6F
		public override string ToString()
		{
			return this.m_algorithmGroup;
		}

		/// <summary>Gets a <see cref="T:System.Security.Cryptography.CngAlgorithmGroup" /> object that specifies the Diffie-Hellman family of algorithms.</summary>
		/// <returns>An object that specifies the Diffie-Hellman family of algorithms.</returns>
		// Token: 0x17000051 RID: 81
		// (get) Token: 0x060001B9 RID: 441 RVA: 0x00004CCF File Offset: 0x00002ECF
		public static CngAlgorithmGroup DiffieHellman
		{
			get
			{
				if (CngAlgorithmGroup.s_dh == null)
				{
					CngAlgorithmGroup.s_dh = new CngAlgorithmGroup("DH");
				}
				return CngAlgorithmGroup.s_dh;
			}
		}

		/// <summary>Gets a <see cref="T:System.Security.Cryptography.CngAlgorithmGroup" /> object that specifies the Digital Signature Algorithm (DSA) family of algorithms.</summary>
		/// <returns>An object that specifies the DSA family of algorithms.</returns>
		// Token: 0x17000052 RID: 82
		// (get) Token: 0x060001BA RID: 442 RVA: 0x00004CF8 File Offset: 0x00002EF8
		public static CngAlgorithmGroup Dsa
		{
			get
			{
				if (CngAlgorithmGroup.s_dsa == null)
				{
					CngAlgorithmGroup.s_dsa = new CngAlgorithmGroup("DSA");
				}
				return CngAlgorithmGroup.s_dsa;
			}
		}

		/// <summary>Gets a <see cref="T:System.Security.Cryptography.CngAlgorithmGroup" /> object that specifies the Elliptic Curve Diffie-Hellman (ECDH) family of algorithms.</summary>
		/// <returns>An object that specifies the ECDH family of algorithms.</returns>
		// Token: 0x17000053 RID: 83
		// (get) Token: 0x060001BB RID: 443 RVA: 0x00004D21 File Offset: 0x00002F21
		public static CngAlgorithmGroup ECDiffieHellman
		{
			get
			{
				if (CngAlgorithmGroup.s_ecdh == null)
				{
					CngAlgorithmGroup.s_ecdh = new CngAlgorithmGroup("ECDH");
				}
				return CngAlgorithmGroup.s_ecdh;
			}
		}

		/// <summary>Gets a <see cref="T:System.Security.Cryptography.CngAlgorithmGroup" /> object that specifies the Elliptic Curve Digital Signature Algorithm (ECDSA) family of algorithms.</summary>
		/// <returns>An object that specifies the ECDSA family of algorithms.</returns>
		// Token: 0x17000054 RID: 84
		// (get) Token: 0x060001BC RID: 444 RVA: 0x00004D4A File Offset: 0x00002F4A
		public static CngAlgorithmGroup ECDsa
		{
			get
			{
				if (CngAlgorithmGroup.s_ecdsa == null)
				{
					CngAlgorithmGroup.s_ecdsa = new CngAlgorithmGroup("ECDSA");
				}
				return CngAlgorithmGroup.s_ecdsa;
			}
		}

		/// <summary>Gets a <see cref="T:System.Security.Cryptography.CngAlgorithmGroup" /> object that specifies the Rivest-Shamir-Adleman (RSA) family of algorithms.</summary>
		/// <returns>An object that specifies the RSA family of algorithms.</returns>
		// Token: 0x17000055 RID: 85
		// (get) Token: 0x060001BD RID: 445 RVA: 0x00004D73 File Offset: 0x00002F73
		public static CngAlgorithmGroup Rsa
		{
			get
			{
				if (CngAlgorithmGroup.s_rsa == null)
				{
					CngAlgorithmGroup.s_rsa = new CngAlgorithmGroup("RSA");
				}
				return CngAlgorithmGroup.s_rsa;
			}
		}

		// Token: 0x04000275 RID: 629
		private static volatile CngAlgorithmGroup s_dh;

		// Token: 0x04000276 RID: 630
		private static volatile CngAlgorithmGroup s_dsa;

		// Token: 0x04000277 RID: 631
		private static volatile CngAlgorithmGroup s_ecdh;

		// Token: 0x04000278 RID: 632
		private static volatile CngAlgorithmGroup s_ecdsa;

		// Token: 0x04000279 RID: 633
		private static volatile CngAlgorithmGroup s_rsa;

		// Token: 0x0400027A RID: 634
		private string m_algorithmGroup;
	}
}
