﻿using System;

namespace System.Security.Cryptography
{
	/// <summary>Specifies options for opening key handles.</summary>
	// Token: 0x0200004D RID: 77
	[Flags]
	public enum CngKeyHandleOpenOptions
	{
		/// <summary>The key handle being opened does not specify an ephemeral key.</summary>
		// Token: 0x0400027C RID: 636
		None = 0,
		/// <summary>The key handle being opened specifies an ephemeral key.</summary>
		// Token: 0x0400027D RID: 637
		EphemeralKey = 1
	}
}
