﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Security.Cryptography
{
	/// <summary>Provides a Cryptography Next Generation (CNG) implementation of the Secure Hash Algorithm (SHA) for 512-bit hash values.</summary>
	// Token: 0x02000498 RID: 1176
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class SHA512Cng : SHA512
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.SHA512Cng" /> class. </summary>
		// Token: 0x06001CAD RID: 7341 RVA: 0x0000220F File Offset: 0x0000040F
		public SHA512Cng()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06001CAE RID: 7342 RVA: 0x0000220F File Offset: 0x0000040F
		protected override void HashCore(byte[] array, int ibStart, int cbSize)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06001CAF RID: 7343 RVA: 0x0004B26D File Offset: 0x0004946D
		protected override byte[] HashFinal()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Initializes, or re-initializes, the instance of the hash algorithm. </summary>
		// Token: 0x06001CB0 RID: 7344 RVA: 0x0000220F File Offset: 0x0000040F
		public override void Initialize()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
