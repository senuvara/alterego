﻿using System;
using System.Security.Permissions;
using Microsoft.Win32.SafeHandles;

namespace System.Security.Cryptography
{
	/// <summary>Defines the core functionality for keys that are used with Cryptography Next Generation (CNG) objects.</summary>
	// Token: 0x0200004E RID: 78
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class CngKey : IDisposable
	{
		/// <summary>Gets the algorithm group that is used by the key.</summary>
		/// <returns>An object that specifies the name of an encryption algorithm group.</returns>
		// Token: 0x17000056 RID: 86
		// (get) Token: 0x060001BE RID: 446 RVA: 0x0000227E File Offset: 0x0000047E
		public CngAlgorithmGroup AlgorithmGroup
		{
			[SecuritySafeCritical]
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the algorithm that is used by the key.</summary>
		/// <returns>An object that specifies the name of an encryption algorithm.</returns>
		// Token: 0x17000057 RID: 87
		// (get) Token: 0x060001BF RID: 447 RVA: 0x0000227E File Offset: 0x0000047E
		public CngAlgorithm Algorithm
		{
			[SecuritySafeCritical]
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the export policy that is used by the key.</summary>
		/// <returns>An object that specifies the export policy for the key.</returns>
		// Token: 0x17000058 RID: 88
		// (get) Token: 0x060001C0 RID: 448 RVA: 0x0000227E File Offset: 0x0000047E
		public CngExportPolicies ExportPolicy
		{
			[SecuritySafeCritical]
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets a safe handle that represents a native key (NCRYPT_KEY_HANDLE). </summary>
		/// <returns>A safe handle that represents the key.</returns>
		// Token: 0x17000059 RID: 89
		// (get) Token: 0x060001C1 RID: 449 RVA: 0x0000227E File Offset: 0x0000047E
		public SafeNCryptKeyHandle Handle
		{
			[SecurityCritical]
			[SecurityPermission(SecurityAction.Demand, UnmanagedCode = true)]
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the persistence state of the key.</summary>
		/// <returns>
		///     <see langword="true" /> if the key is ephemeral; otherwise, <see langword="false" />. </returns>
		// Token: 0x1700005A RID: 90
		// (get) Token: 0x060001C2 RID: 450 RVA: 0x0000227E File Offset: 0x0000047E
		// (set) Token: 0x060001C3 RID: 451 RVA: 0x0000227E File Offset: 0x0000047E
		public bool IsEphemeral
		{
			[SecuritySafeCritical]
			get
			{
				throw new NotImplementedException();
			}
			[SecurityCritical]
			private set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the scope (machine or user) of the key.</summary>
		/// <returns>
		///     <see langword="true" /> if the key is available on a machine-wide basis; <see langword="false" /> if the key is only for the current user.</returns>
		// Token: 0x1700005B RID: 91
		// (get) Token: 0x060001C4 RID: 452 RVA: 0x0000227E File Offset: 0x0000047E
		public bool IsMachineKey
		{
			[SecuritySafeCritical]
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the name of the key.</summary>
		/// <returns>The name of the key. If the key is ephemeral, the value is <see langword="null" />.</returns>
		// Token: 0x1700005C RID: 92
		// (get) Token: 0x060001C5 RID: 453 RVA: 0x0000227E File Offset: 0x0000047E
		public string KeyName
		{
			[SecuritySafeCritical]
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the key size in bits.</summary>
		/// <returns>The key size in bits.</returns>
		// Token: 0x1700005D RID: 93
		// (get) Token: 0x060001C6 RID: 454 RVA: 0x0000227E File Offset: 0x0000047E
		public int KeySize
		{
			[SecuritySafeCritical]
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the cryptographic operations specified by the key.</summary>
		/// <returns>A bitwise combination of the enumeration values that specify the usages allowed for the key.</returns>
		// Token: 0x1700005E RID: 94
		// (get) Token: 0x060001C7 RID: 455 RVA: 0x0000227E File Offset: 0x0000047E
		public CngKeyUsages KeyUsage
		{
			[SecuritySafeCritical]
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets or sets the window handle (HWND) that should be used for user interface (UI) prompts caused by accessing the key.</summary>
		/// <returns>The parent window handle for the key.</returns>
		// Token: 0x1700005F RID: 95
		// (get) Token: 0x060001C8 RID: 456 RVA: 0x0000227E File Offset: 0x0000047E
		// (set) Token: 0x060001C9 RID: 457 RVA: 0x0000227E File Offset: 0x0000047E
		public IntPtr ParentWindowHandle
		{
			[SecuritySafeCritical]
			get
			{
				throw new NotImplementedException();
			}
			[SecuritySafeCritical]
			[SecurityPermission(SecurityAction.Demand, UnmanagedCode = true)]
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the key storage provider (KSP) that manages the key.</summary>
		/// <returns>The KSP that manages the key.</returns>
		// Token: 0x17000060 RID: 96
		// (get) Token: 0x060001CA RID: 458 RVA: 0x0000227E File Offset: 0x0000047E
		public CngProvider Provider
		{
			[SecuritySafeCritical]
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets a native handle (an NCRYPT_PROV_HANDLE) to the key storage provider (KSP).</summary>
		/// <returns>A handle to the KSP.</returns>
		// Token: 0x17000061 RID: 97
		// (get) Token: 0x060001CB RID: 459 RVA: 0x0000227E File Offset: 0x0000047E
		public SafeNCryptProviderHandle ProviderHandle
		{
			[SecurityCritical]
			[SecurityPermission(SecurityAction.Demand, UnmanagedCode = true)]
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the unique name for the key.</summary>
		/// <returns>An alternate name for the key. If the key is ephemeral, the value is <see langword="null" />.</returns>
		// Token: 0x17000062 RID: 98
		// (get) Token: 0x060001CC RID: 460 RVA: 0x0000227E File Offset: 0x0000047E
		public string UniqueName
		{
			[SecuritySafeCritical]
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets parameters that control the user interface (UI) for accessing the key.  </summary>
		/// <returns>An object that contains configuration parameters for displaying the UI.</returns>
		// Token: 0x17000063 RID: 99
		// (get) Token: 0x060001CD RID: 461 RVA: 0x0000227E File Offset: 0x0000047E
		public CngUIPolicy UIPolicy
		{
			[SecuritySafeCritical]
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Creates a <see cref="T:System.Security.Cryptography.CngKey" /> object that can be used with the specified algorithm.</summary>
		/// <param name="algorithm">The algorithm that the key will be used with.</param>
		/// <returns>An ephemeral key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="algorithm" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.PlatformNotSupportedException">Cryptography Next Generation (CNG) is not supported on this system.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">All other errors.</exception>
		// Token: 0x060001CE RID: 462 RVA: 0x0000227E File Offset: 0x0000047E
		public static CngKey Create(CngAlgorithm algorithm)
		{
			throw new NotImplementedException();
		}

		/// <summary>Creates a named <see cref="T:System.Security.Cryptography.CngKey" /> object that provides the specified algorithm.</summary>
		/// <param name="algorithm">The algorithm that the key will be used with.</param>
		/// <param name="keyName">The key name. If a name is not provided, the key will not be persisted.</param>
		/// <returns>A persisted or ephemeral key that provides the specified algorithm.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="algorithm" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.PlatformNotSupportedException">Cryptography Next Generation (CNG) is not supported on this system.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">All other errors.</exception>
		// Token: 0x060001CF RID: 463 RVA: 0x0000227E File Offset: 0x0000047E
		public static CngKey Create(CngAlgorithm algorithm, string keyName)
		{
			throw new NotImplementedException();
		}

		/// <summary>Creates a named <see cref="T:System.Security.Cryptography.CngKey" /> object that provides the specified algorithm, using the supplied key creation parameters.</summary>
		/// <param name="algorithm">The algorithm that the key will be used with.</param>
		/// <param name="keyName">The key name. If a name is not provided, the key will not be persisted.</param>
		/// <param name="creationParameters">An object that specifies advanced parameters for the method, including the <see cref="T:System.Security.Cryptography.CngProvider" />.</param>
		/// <returns>A persisted or ephemeral key that provides the specified algorithm.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="algorithm" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.PlatformNotSupportedException">Cryptography Next Generation (CNG) is not supported on this system.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">All other errors.</exception>
		// Token: 0x060001D0 RID: 464 RVA: 0x0000227E File Offset: 0x0000047E
		[SecuritySafeCritical]
		public static CngKey Create(CngAlgorithm algorithm, string keyName, CngKeyCreationParameters creationParameters)
		{
			throw new NotImplementedException();
		}

		/// <summary>Removes the key that is associated with the object.</summary>
		/// <exception cref="T:System.ObjectDisposedException">An attempt was made to access a deleted key.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">All other errors.</exception>
		// Token: 0x060001D1 RID: 465 RVA: 0x0000227E File Offset: 0x0000047E
		[SecuritySafeCritical]
		public void Delete()
		{
			throw new NotImplementedException();
		}

		/// <summary>Releases all resources used by the current instance of the <see cref="T:System.Security.Cryptography.CngKey" /> class.</summary>
		// Token: 0x060001D2 RID: 466 RVA: 0x0000227E File Offset: 0x0000047E
		[SecuritySafeCritical]
		public void Dispose()
		{
			throw new NotImplementedException();
		}

		/// <summary>Checks to see whether a named key exists in the default key storage provider (KSP).</summary>
		/// <param name="keyName">The key name.</param>
		/// <returns>
		///     <see langword="true" /> if the named key exists in the default KSP; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="keyName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.PlatformNotSupportedException">Cryptography Next Generation (CNG) is not supported on this system.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">All other errors.</exception>
		// Token: 0x060001D3 RID: 467 RVA: 0x0000227E File Offset: 0x0000047E
		public static bool Exists(string keyName)
		{
			throw new NotImplementedException();
		}

		/// <summary>Checks to see whether a named key exists in the specified key storage provider (KSP).</summary>
		/// <param name="keyName">The key name.</param>
		/// <param name="provider">The KSP to check for the key.</param>
		/// <returns>
		///     <see langword="true" /> if the named key exists in the specified provider; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="keyName" /> or <paramref name="provider" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.PlatformNotSupportedException">Cryptography Next Generation (CNG) is not supported on this system.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">All other errors.</exception>
		// Token: 0x060001D4 RID: 468 RVA: 0x0000227E File Offset: 0x0000047E
		public static bool Exists(string keyName, CngProvider provider)
		{
			throw new NotImplementedException();
		}

		/// <summary>Checks to see whether a named key exists in the specified key storage provider (KSP), according to the specified options.</summary>
		/// <param name="keyName">The key name.</param>
		/// <param name="provider">The KSP to search for the key.</param>
		/// <param name="options">A bitwise combination of the enumeration values that specify options for opening a key.</param>
		/// <returns>
		///     <see langword="true" /> if the named key exists in the specified provider; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="keyName" /> or <paramref name="provider" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.PlatformNotSupportedException">Cryptography Next Generation (CNG) is not supported on this system.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">All other errors.</exception>
		// Token: 0x060001D5 RID: 469 RVA: 0x0000227E File Offset: 0x0000047E
		[SecuritySafeCritical]
		public static bool Exists(string keyName, CngProvider provider, CngKeyOpenOptions options)
		{
			throw new NotImplementedException();
		}

		/// <summary>Creates a new key by importing the specified key material into the default key storage provider (KSP) and using the specified format.</summary>
		/// <param name="keyBlob">An array that contains the key information.</param>
		/// <param name="format">An object that specifies the format of the <paramref name="keyBlob" /> array.</param>
		/// <returns>A new key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="keyBlob" /> or <paramref name="format" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.PlatformNotSupportedException">Cryptography Next Generation (CNG) is not supported on this system.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">All other errors.</exception>
		// Token: 0x060001D6 RID: 470 RVA: 0x0000227E File Offset: 0x0000047E
		public static CngKey Import(byte[] keyBlob, CngKeyBlobFormat format)
		{
			throw new NotImplementedException();
		}

		/// <summary>Creates a new key by importing the specified key material into the specified key storage provider (KSP), using the specified format.</summary>
		/// <param name="keyBlob">An array that contains the key information.</param>
		/// <param name="format">An object that specifies the format of the <paramref name="keyBlob" /> array.</param>
		/// <param name="provider">The KSP.</param>
		/// <returns>A new key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="keyBlob" />, <paramref name="format" />, or <paramref name="provider" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.PlatformNotSupportedException">Cryptography Next Generation (CNG) is not supported on this system.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">All other errors.</exception>
		// Token: 0x060001D7 RID: 471 RVA: 0x0000227E File Offset: 0x0000047E
		[SecuritySafeCritical]
		public static CngKey Import(byte[] keyBlob, CngKeyBlobFormat format, CngProvider provider)
		{
			throw new NotImplementedException();
		}

		/// <summary>Exports the key material into a BLOB, in the specified format.</summary>
		/// <param name="format">An object that specifies the format of the key BLOB.</param>
		/// <returns>A BLOB that contains the key material in the specified format.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="format" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">All other errors. Typically, the <see cref="P:System.Security.Cryptography.CngKey.ExportPolicy" /> does not allow the key to be exported. </exception>
		// Token: 0x060001D8 RID: 472 RVA: 0x0000227E File Offset: 0x0000047E
		[SecuritySafeCritical]
		public byte[] Export(CngKeyBlobFormat format)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets a property, given a name and a set of property options.</summary>
		/// <param name="name">The name of the desired property.</param>
		/// <param name="options">A bitwise combination of the enumeration values that specify options for the named property.</param>
		/// <returns>An object that contains the raw value of the specified property.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">All other errors.</exception>
		// Token: 0x060001D9 RID: 473 RVA: 0x0000227E File Offset: 0x0000047E
		[SecuritySafeCritical]
		[SecurityPermission(SecurityAction.Demand, UnmanagedCode = true)]
		public CngProperty GetProperty(string name, CngPropertyOptions options)
		{
			throw new NotImplementedException();
		}

		/// <summary>Checks to see whether the specified property exists on the key.</summary>
		/// <param name="name">The property name to check.</param>
		/// <param name="options">A bitwise combination of the enumeration values that specify options for the named property.</param>
		/// <returns>
		///     <see langword="true" /> if the specified property is found; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />.</exception>
		// Token: 0x060001DA RID: 474 RVA: 0x0000227E File Offset: 0x0000047E
		[SecuritySafeCritical]
		[SecurityPermission(SecurityAction.Demand, UnmanagedCode = true)]
		public bool HasProperty(string name, CngPropertyOptions options)
		{
			throw new NotImplementedException();
		}

		/// <summary>Creates an instance of an <see cref="T:System.Security.Cryptography.CngKey" /> object that represents an existing named key.</summary>
		/// <param name="keyName">The name of the key.</param>
		/// <returns>An existing key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="keyName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.PlatformNotSupportedException">Cryptography Next Generation (CNG) is not supported on this system.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">All other errors.</exception>
		// Token: 0x060001DB RID: 475 RVA: 0x0000227E File Offset: 0x0000047E
		public static CngKey Open(string keyName)
		{
			throw new NotImplementedException();
		}

		/// <summary>Creates an instance of an <see cref="T:System.Security.Cryptography.CngKey" /> object that represents an existing named key, using the specified key storage provider (KSP).</summary>
		/// <param name="keyName">The name of the key.</param>
		/// <param name="provider">The KSP that contains the key.</param>
		/// <returns>An existing key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="keyName" /> or <paramref name="provider" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.PlatformNotSupportedException">Cryptography Next Generation (CNG) is not supported on this system.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">All other errors.</exception>
		// Token: 0x060001DC RID: 476 RVA: 0x0000227E File Offset: 0x0000047E
		public static CngKey Open(string keyName, CngProvider provider)
		{
			throw new NotImplementedException();
		}

		/// <summary>Creates an instance of an <see cref="T:System.Security.Cryptography.CngKey" /> object that represents an existing named key, using the specified key storage provider (KSP) and key open options.</summary>
		/// <param name="keyName">The name of the key.</param>
		/// <param name="provider">The KSP that contains the key.</param>
		/// <param name="openOptions">A bitwise combination of the enumeration values that specify options for opening the key, such as where the key is opened from (machine or user storage) and whether to suppress UI prompting.</param>
		/// <returns>An existing key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="keyName" /> or <paramref name="provider" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.PlatformNotSupportedException">Cryptography Next Generation (CNG) is not supported on this system.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">All other errors.</exception>
		// Token: 0x060001DD RID: 477 RVA: 0x0000227E File Offset: 0x0000047E
		[SecuritySafeCritical]
		public static CngKey Open(string keyName, CngProvider provider, CngKeyOpenOptions openOptions)
		{
			throw new NotImplementedException();
		}

		/// <summary>Creates an instance of an <see cref="T:System.Security.Cryptography.CngKey" /> object by using a handle to an existing key.</summary>
		/// <param name="keyHandle">A handle to an existing key.</param>
		/// <param name="keyHandleOpenOptions">One of the enumeration values that indicates whether <paramref name="keyHandle" /> represents an ephemeral key or a named key.</param>
		/// <returns>An existing key.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="keyHandle" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="keyHandle" /> is invalid or malformed, or it is already closed. This exception is also thrown if the key is an ephemeral key that is created by the common language runtime (CLR), but the <see cref="F:System.Security.Cryptography.CngKeyHandleOpenOptions.EphemeralKey" /> value is not specified.</exception>
		/// <exception cref="T:System.PlatformNotSupportedException">Cryptography Next Generation (CNG) is not supported on this system.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">All other errors.</exception>
		// Token: 0x060001DE RID: 478 RVA: 0x0000227E File Offset: 0x0000047E
		[SecurityCritical]
		[SecurityPermission(SecurityAction.Demand, UnmanagedCode = true)]
		public static CngKey Open(SafeNCryptKeyHandle keyHandle, CngKeyHandleOpenOptions keyHandleOpenOptions)
		{
			throw new NotImplementedException();
		}

		/// <summary>Sets a named property on the key.</summary>
		/// <param name="property">The key property to set.</param>
		// Token: 0x060001DF RID: 479 RVA: 0x0000227E File Offset: 0x0000047E
		[SecuritySafeCritical]
		[SecurityPermission(SecurityAction.Demand, UnmanagedCode = true)]
		public void SetProperty(CngProperty property)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060001E0 RID: 480 RVA: 0x00002310 File Offset: 0x00000510
		public CngKey()
		{
		}
	}
}
