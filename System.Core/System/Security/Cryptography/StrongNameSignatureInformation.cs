﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Security.Cryptography
{
	/// <summary>Holds the strong name signature information for a manifest.</summary>
	// Token: 0x02000492 RID: 1170
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class StrongNameSignatureInformation
	{
		// Token: 0x06001C96 RID: 7318 RVA: 0x0000220F File Offset: 0x0000040F
		internal StrongNameSignatureInformation()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the hash algorithm that is used to calculate the strong name signature.</summary>
		/// <returns>The name of the hash algorithm that is used to calculate the strong name signature.</returns>
		// Token: 0x17000599 RID: 1433
		// (get) Token: 0x06001C97 RID: 7319 RVA: 0x0004B26D File Offset: 0x0004946D
		public string HashAlgorithm
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the HRESULT value of the result code.</summary>
		/// <returns>The HRESULT value of the result code.</returns>
		// Token: 0x1700059A RID: 1434
		// (get) Token: 0x06001C98 RID: 7320 RVA: 0x00053B38 File Offset: 0x00051D38
		public int HResult
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets a value indicating whether the strong name signature is valid.</summary>
		/// <returns>
		///     <see langword="true" /> if the strong name signature is valid; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700059B RID: 1435
		// (get) Token: 0x06001C99 RID: 7321 RVA: 0x00053B54 File Offset: 0x00051D54
		public bool IsValid
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets the public key that is used to verify the signature.</summary>
		/// <returns>The public key that is used to verify the signature. </returns>
		// Token: 0x1700059C RID: 1436
		// (get) Token: 0x06001C9A RID: 7322 RVA: 0x0004B26D File Offset: 0x0004946D
		public AsymmetricAlgorithm PublicKey
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the results of verifying the strong name signature.</summary>
		/// <returns>The result codes for signature verification.</returns>
		// Token: 0x1700059D RID: 1437
		// (get) Token: 0x06001C9B RID: 7323 RVA: 0x00053B70 File Offset: 0x00051D70
		public SignatureVerificationResult VerificationResult
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return SignatureVerificationResult.Valid;
			}
		}
	}
}
