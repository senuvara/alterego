﻿using System;
using System.Collections.ObjectModel;
using System.Security.Permissions;
using Unity;

namespace System.Security.Cryptography
{
	/// <summary>Represents a read-only collection of <see cref="T:System.Security.Cryptography.ManifestSignatureInformation" /> objects.  </summary>
	// Token: 0x02000493 RID: 1171
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class ManifestSignatureInformationCollection : ReadOnlyCollection<ManifestSignatureInformation>
	{
		// Token: 0x06001C9C RID: 7324 RVA: 0x0000220F File Offset: 0x0000040F
		internal ManifestSignatureInformationCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
