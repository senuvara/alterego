﻿using System;
using System.Security.Permissions;

namespace System.Security.Cryptography
{
	/// <summary>Provides an abstract base class that Elliptic Curve Diffie-Hellman (ECDH) algorithm implementations can derive from. This class provides the basic set of operations that all ECDH implementations must support.</summary>
	// Token: 0x02000055 RID: 85
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public abstract class ECDiffieHellman : AsymmetricAlgorithm
	{
		/// <summary>Gets the name of the key exchange algorithm.</summary>
		/// <returns>The name of the key exchange algorithm. </returns>
		// Token: 0x17000080 RID: 128
		// (get) Token: 0x0600021F RID: 543 RVA: 0x000053AA File Offset: 0x000035AA
		public override string KeyExchangeAlgorithm
		{
			get
			{
				return "ECDiffieHellman";
			}
		}

		/// <summary>Gets the name of the signature algorithm.</summary>
		/// <returns>Always <see langword="null" />.</returns>
		// Token: 0x17000081 RID: 129
		// (get) Token: 0x06000220 RID: 544 RVA: 0x000053B1 File Offset: 0x000035B1
		public override string SignatureAlgorithm
		{
			get
			{
				return null;
			}
		}

		/// <summary>Creates a new instance of the default implementation of the Elliptic Curve Diffie-Hellman (ECDH) algorithm.</summary>
		/// <returns>A new instance of the default implementation of this class.</returns>
		// Token: 0x06000221 RID: 545 RVA: 0x0000227E File Offset: 0x0000047E
		public new static ECDiffieHellman Create()
		{
			throw new NotImplementedException();
		}

		/// <summary>Creates a new instance of the specified implementation of the Elliptic Curve Diffie-Hellman (ECDH) algorithm.</summary>
		/// <param name="algorithm">The name of an implementation of the ECDH algorithm.</param>
		/// <returns>A new instance of the specified implementation of this class. If the specified algorithm name does not map to an ECDH implementation, this method returns <see langword="null" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="algorithm" /> parameter is <see langword="null" />. </exception>
		// Token: 0x06000222 RID: 546 RVA: 0x000053B4 File Offset: 0x000035B4
		public new static ECDiffieHellman Create(string algorithm)
		{
			if (algorithm == null)
			{
				throw new ArgumentNullException("algorithm");
			}
			return CryptoConfig.CreateFromName(algorithm) as ECDiffieHellman;
		}

		/// <summary>Creates a new instance of the default implementation of the Elliptic Curve Diffie-Hellman (ECDH) algorithm with a new public/private key-pair generated over the specified curve. </summary>
		/// <param name="curve">The curve to use to generate a new public/private key-pair. </param>
		/// <returns>A new instance of the default implementation of the Elliptic Curve Diffie-Hellman (ECDH) algorithm. </returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">
		///         <paramref name="curve" /> does not validate. </exception>
		// Token: 0x06000223 RID: 547 RVA: 0x000053D0 File Offset: 0x000035D0
		public static ECDiffieHellman Create(ECCurve curve)
		{
			ECDiffieHellman ecdiffieHellman = ECDiffieHellman.Create();
			if (ecdiffieHellman != null)
			{
				try
				{
					ecdiffieHellman.GenerateKey(curve);
				}
				catch
				{
					ecdiffieHellman.Dispose();
					throw;
				}
			}
			return ecdiffieHellman;
		}

		/// <summary>Creates a new instance of the default implementation of the Elliptic Curve Diffie-Hellman (ECDH) algorithm with the key described by the specified  <see cref="T:System.Security.Cryptography.ECParameters" /> object. </summary>
		/// <param name="parameters">The parameters  for the elliptic curve cryptography (ECC) algorithm. </param>
		/// <returns>A new instance of the default implementation of the Elliptic Curve Diffie-Hellman (ECDH) algorithm. </returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">
		///         <paramref name="parameters" /> does not validate. </exception>
		// Token: 0x06000224 RID: 548 RVA: 0x0000540C File Offset: 0x0000360C
		public static ECDiffieHellman Create(ECParameters parameters)
		{
			ECDiffieHellman ecdiffieHellman = ECDiffieHellman.Create();
			if (ecdiffieHellman != null)
			{
				try
				{
					ecdiffieHellman.ImportParameters(parameters);
				}
				catch
				{
					ecdiffieHellman.Dispose();
					throw;
				}
			}
			return ecdiffieHellman;
		}

		/// <summary>Gets the public key that is being used by the current Elliptic Curve Diffie-Hellman (ECDH) instance.</summary>
		/// <returns>The public part of the ECDH key pair that is being used by this <see cref="T:System.Security.Cryptography.ECDiffieHellman" /> instance.</returns>
		// Token: 0x17000082 RID: 130
		// (get) Token: 0x06000225 RID: 549
		public abstract ECDiffieHellmanPublicKey PublicKey { get; }

		/// <summary>Derives bytes that can be used as a key, given another party's public key.</summary>
		/// <param name="otherPartyPublicKey">The other party's public key.</param>
		/// <returns>The key material from the key exchange with the other party’s public key.</returns>
		// Token: 0x06000226 RID: 550 RVA: 0x00005448 File Offset: 0x00003648
		public virtual byte[] DeriveKeyMaterial(ECDiffieHellmanPublicKey otherPartyPublicKey)
		{
			throw ECDiffieHellman.DerivedClassMustOverride();
		}

		/// <summary>Derives bytes that can be used as a key using a hash function, given another party's public key and hash algorithm's name.</summary>
		/// <param name="otherPartyPublicKey">The other party's public key.</param>
		/// <param name="hashAlgorithm">The hash algorithm  to use to derive the key material.</param>
		/// <returns>The key material from the key exchange with the other party’s public key.</returns>
		// Token: 0x06000227 RID: 551 RVA: 0x0000544F File Offset: 0x0000364F
		public byte[] DeriveKeyFromHash(ECDiffieHellmanPublicKey otherPartyPublicKey, HashAlgorithmName hashAlgorithm)
		{
			return this.DeriveKeyFromHash(otherPartyPublicKey, hashAlgorithm, null, null);
		}

		/// <summary>When implemented in a derived class, derives bytes that can be used as a key using a hash function, given another party's public key, hash algorithm's name, a prepend value and an append value.</summary>
		/// <param name="otherPartyPublicKey">The other party's public key.</param>
		/// <param name="hashAlgorithm">The hash algorithm  to use to derive the key material.</param>
		/// <param name="secretPrepend">A value to prepend to the derived secret before hashing.</param>
		/// <param name="secretAppend">A value to append to the derived secret before hashing.</param>
		/// <returns>The key material from the key exchange with the other party’s public key.</returns>
		/// <exception cref="T:System.NotImplementedException">A derived class must override this method.</exception>
		// Token: 0x06000228 RID: 552 RVA: 0x00005448 File Offset: 0x00003648
		public virtual byte[] DeriveKeyFromHash(ECDiffieHellmanPublicKey otherPartyPublicKey, HashAlgorithmName hashAlgorithm, byte[] secretPrepend, byte[] secretAppend)
		{
			throw ECDiffieHellman.DerivedClassMustOverride();
		}

		/// <summary>Derives bytes that can be used as a key using a Hash-based Message Authentication Code (HMAC).</summary>
		/// <param name="otherPartyPublicKey">The other party's public key.</param>
		/// <param name="hashAlgorithm">The hash algorithm to use to derive the key material.</param>
		/// <param name="hmacKey">The key for the HMAC.</param>
		/// <returns>The key material from the key exchange with the other party’s public key.</returns>
		// Token: 0x06000229 RID: 553 RVA: 0x0000545B File Offset: 0x0000365B
		public byte[] DeriveKeyFromHmac(ECDiffieHellmanPublicKey otherPartyPublicKey, HashAlgorithmName hashAlgorithm, byte[] hmacKey)
		{
			return this.DeriveKeyFromHmac(otherPartyPublicKey, hashAlgorithm, hmacKey, null, null);
		}

		/// <summary>When implemented in a derived class, derives bytes that can be used as a key using a Hash-based Message Authentication Code (HMAC).</summary>
		/// <param name="otherPartyPublicKey">The other party's public key.</param>
		/// <param name="hashAlgorithm">The hash algorithm to use to derive the key material.</param>
		/// <param name="hmacKey">The key for the HMAC.</param>
		/// <param name="secretPrepend">A value to prepend to the derived secret before hashing.</param>
		/// <param name="secretAppend">A value to append to the derived secret before hashing.</param>
		/// <returns>The key material from the key exchange with the other party’s public key.</returns>
		/// <exception cref="T:System.NotImplementedException">A derived class must override this method.</exception>
		// Token: 0x0600022A RID: 554 RVA: 0x00005448 File Offset: 0x00003648
		public virtual byte[] DeriveKeyFromHmac(ECDiffieHellmanPublicKey otherPartyPublicKey, HashAlgorithmName hashAlgorithm, byte[] hmacKey, byte[] secretPrepend, byte[] secretAppend)
		{
			throw ECDiffieHellman.DerivedClassMustOverride();
		}

		/// <summary>When implemented in a derived class, derives bytes that can be used as a key using a Transport Layer Security (TLS) Pseudo-Random Function (PRF) derivation algorithm.</summary>
		/// <param name="otherPartyPublicKey">The other party's public key.</param>
		/// <param name="prfLabel">The ASCII-encoded PRF label.</param>
		/// <param name="prfSeed">The 64-byte PRF seed.</param>
		/// <returns>The key material from the key exchange with the other party’s public key.</returns>
		/// <exception cref="T:System.NotImplementedException">A derived class must override this method.</exception>
		// Token: 0x0600022B RID: 555 RVA: 0x00005448 File Offset: 0x00003648
		public virtual byte[] DeriveKeyTls(ECDiffieHellmanPublicKey otherPartyPublicKey, byte[] prfLabel, byte[] prfSeed)
		{
			throw ECDiffieHellman.DerivedClassMustOverride();
		}

		// Token: 0x0600022C RID: 556 RVA: 0x00005468 File Offset: 0x00003668
		private static Exception DerivedClassMustOverride()
		{
			return new NotImplementedException(SR.GetString("Method not supported. Derived class must override."));
		}

		/// <summary>When overridden in a derived class, exports either the public or the public and private key information from a working <see cref="T:System.Security.Cryptography.ECDiffieHellman" /> key to an <see cref="T:System.Security.Cryptography.ECParameters" /> structure so that it can be passed to the <see cref="M:System.Security.Cryptography.ECDiffieHellman.ImportParameters(System.Security.Cryptography.ECParameters)" />   method. </summary>
		/// <param name="includePrivateParameters">
		///       <see langword="true" /> to include private parameters; otehrwise,  <see langword="false" /> to include public parameters only.</param>
		/// <returns>An object that represents the point on the curve for this key. It can be passed to the <see cref="M:System.Security.Cryptography.ECDiffieHellman.ImportParameters(System.Security.Cryptography.ECParameters)" /> method. </returns>
		/// <exception cref="T:System.NotImplementedException">A derived class must override this method. </exception>
		// Token: 0x0600022D RID: 557 RVA: 0x00005448 File Offset: 0x00003648
		public virtual ECParameters ExportParameters(bool includePrivateParameters)
		{
			throw ECDiffieHellman.DerivedClassMustOverride();
		}

		/// <summary>When overridden in a derived class, exports either the public or the public and private key information using the explicit curve form from a working <see cref="T:System.Security.Cryptography.ECDiffieHellman" /> key to an <see cref="T:System.Security.Cryptography.ECParameters" /> structure so that it can be passed to the <see cref="M:System.Security.Cryptography.ECDiffieHellman.ImportParameters(System.Security.Cryptography.ECParameters)" />   method. </summary>
		/// <param name="includePrivateParameters">
		///       <see langword="true" /> to include private parameters; otherwise, <see langword="false" />. </param>
		/// <returns>An object that represents the point on the curve for this key, using the explicit curve format. </returns>
		/// <exception cref="T:System.NotImplementedException">A derived class must override this method. </exception>
		// Token: 0x0600022E RID: 558 RVA: 0x00005448 File Offset: 0x00003648
		public virtual ECParameters ExportExplicitParameters(bool includePrivateParameters)
		{
			throw ECDiffieHellman.DerivedClassMustOverride();
		}

		/// <summary>When overridden in a derived class, imports the specified parameters for an <see cref="T:System.Security.Cryptography.ECCurve" /> as an ephemeral key into the current <see cref="T:System.Security.Cryptography.ECDiffieHellman" /> object. </summary>
		/// <param name="parameters">The curve's parameters to import. </param>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">
		///         <paramref name="parameters" /> does not validate. </exception>
		/// <exception cref="T:System.NotImplementedException">A derived class must override this method. </exception>
		// Token: 0x0600022F RID: 559 RVA: 0x00005448 File Offset: 0x00003648
		public virtual void ImportParameters(ECParameters parameters)
		{
			throw ECDiffieHellman.DerivedClassMustOverride();
		}

		/// <summary>When overridden in a derived class, generates a new ephemeral public/private key pair for the specified curve. </summary>
		/// <param name="curve">The curve used to generate an ephemeral public/private key pair. </param>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">
		///         <paramref name="curve" /> does not validate. </exception>
		/// <exception cref="T:System.NotImplementedException">A derived class must override this method. </exception>
		// Token: 0x06000230 RID: 560 RVA: 0x00005479 File Offset: 0x00003679
		public virtual void GenerateKey(ECCurve curve)
		{
			throw new NotSupportedException(SR.GetString("Method not supported. Derived class must override."));
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.ECDiffieHellman" /> class.</summary>
		// Token: 0x06000231 RID: 561 RVA: 0x0000548A File Offset: 0x0000368A
		protected ECDiffieHellman()
		{
		}
	}
}
