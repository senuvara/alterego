﻿using System;

namespace System.Security.Cryptography
{
	/// <summary>Provides a managed implementation of the Advanced Encryption Standard (AES) symmetric algorithm. </summary>
	// Token: 0x02000045 RID: 69
	public sealed class AesManaged : Aes
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.AesManaged" /> class. </summary>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The Windows security policy setting for FIPS is enabled.</exception>
		/// <exception cref="T:System.InvalidOperationException">This implementation is not part of the Windows Platform FIPS-validated cryptographic algorithms.</exception>
		// Token: 0x06000187 RID: 391 RVA: 0x000046D0 File Offset: 0x000028D0
		public AesManaged()
		{
			if (CryptoConfig.AllowOnlyFipsAlgorithms)
			{
				throw new InvalidOperationException(SR.GetString("This implementation is not part of the Windows Platform FIPS validated cryptographic algorithms."));
			}
			this.m_rijndael = new RijndaelManaged();
			this.m_rijndael.BlockSize = this.BlockSize;
			this.m_rijndael.KeySize = this.KeySize;
		}

		/// <summary>Gets or sets the number of bits to use as feedback. </summary>
		/// <returns>The feedback size, in bits.</returns>
		// Token: 0x1700003B RID: 59
		// (get) Token: 0x06000188 RID: 392 RVA: 0x00004727 File Offset: 0x00002927
		// (set) Token: 0x06000189 RID: 393 RVA: 0x00004734 File Offset: 0x00002934
		public override int FeedbackSize
		{
			get
			{
				return this.m_rijndael.FeedbackSize;
			}
			set
			{
				this.m_rijndael.FeedbackSize = value;
			}
		}

		/// <summary>Gets or sets the initialization vector (IV) to use for the symmetric algorithm. </summary>
		/// <returns>The initialization vector to use for the symmetric algorithm</returns>
		// Token: 0x1700003C RID: 60
		// (get) Token: 0x0600018A RID: 394 RVA: 0x00004742 File Offset: 0x00002942
		// (set) Token: 0x0600018B RID: 395 RVA: 0x0000474F File Offset: 0x0000294F
		public override byte[] IV
		{
			get
			{
				return this.m_rijndael.IV;
			}
			set
			{
				this.m_rijndael.IV = value;
			}
		}

		/// <summary>Gets or sets the secret key used for the symmetric algorithm.</summary>
		/// <returns>The key for the symmetric algorithm.</returns>
		// Token: 0x1700003D RID: 61
		// (get) Token: 0x0600018C RID: 396 RVA: 0x0000475D File Offset: 0x0000295D
		// (set) Token: 0x0600018D RID: 397 RVA: 0x0000476A File Offset: 0x0000296A
		public override byte[] Key
		{
			get
			{
				return this.m_rijndael.Key;
			}
			set
			{
				this.m_rijndael.Key = value;
			}
		}

		/// <summary>Gets or sets the size, in bits, of the secret key used for the symmetric algorithm. </summary>
		/// <returns>The size, in bits, of the key used by the symmetric algorithm.</returns>
		// Token: 0x1700003E RID: 62
		// (get) Token: 0x0600018E RID: 398 RVA: 0x00004778 File Offset: 0x00002978
		// (set) Token: 0x0600018F RID: 399 RVA: 0x00004785 File Offset: 0x00002985
		public override int KeySize
		{
			get
			{
				return this.m_rijndael.KeySize;
			}
			set
			{
				this.m_rijndael.KeySize = value;
			}
		}

		/// <summary>Gets or sets the mode for operation of the symmetric algorithm.</summary>
		/// <returns>One of the enumeration values that specifies the block cipher mode to use for encryption. The default is <see cref="F:System.Security.Cryptography.CipherMode.CBC" />.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">
		///         <see cref="P:System.Security.Cryptography.AesManaged.Mode" /> is set to <see cref="F:System.Security.Cryptography.CipherMode.CFB" /> or <see cref="F:System.Security.Cryptography.CipherMode.OFB" />.</exception>
		// Token: 0x1700003F RID: 63
		// (get) Token: 0x06000190 RID: 400 RVA: 0x00004793 File Offset: 0x00002993
		// (set) Token: 0x06000191 RID: 401 RVA: 0x000047A0 File Offset: 0x000029A0
		public override CipherMode Mode
		{
			get
			{
				return this.m_rijndael.Mode;
			}
			set
			{
				if (value == CipherMode.CFB || value == CipherMode.OFB)
				{
					throw new CryptographicException(SR.GetString("The specified cipher mode is not valid for this algorithm."));
				}
				this.m_rijndael.Mode = value;
			}
		}

		/// <summary>Gets or sets the padding mode used in the symmetric algorithm. </summary>
		/// <returns>One of the enumeration values that specifies the type of padding to apply. The default is <see cref="F:System.Security.Cryptography.PaddingMode.PKCS7" />.</returns>
		// Token: 0x17000040 RID: 64
		// (get) Token: 0x06000192 RID: 402 RVA: 0x000047C6 File Offset: 0x000029C6
		// (set) Token: 0x06000193 RID: 403 RVA: 0x000047D3 File Offset: 0x000029D3
		public override PaddingMode Padding
		{
			get
			{
				return this.m_rijndael.Padding;
			}
			set
			{
				this.m_rijndael.Padding = value;
			}
		}

		/// <summary>Creates a symmetric decryptor object using the current key and initialization vector (IV).</summary>
		/// <returns>A symmetric decryptor object.</returns>
		// Token: 0x06000194 RID: 404 RVA: 0x000047E1 File Offset: 0x000029E1
		public override ICryptoTransform CreateDecryptor()
		{
			return this.m_rijndael.CreateDecryptor();
		}

		/// <summary>Creates a symmetric decryptor object using the specified key and initialization vector (IV).</summary>
		/// <param name="key">The secret key to use for the symmetric algorithm.</param>
		/// <param name="iv">The initialization vector to use for the symmetric algorithm.</param>
		/// <returns>A symmetric decryptor object.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="key" /> or <paramref name="iv" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="key" /> is invalid.</exception>
		// Token: 0x06000195 RID: 405 RVA: 0x000047F0 File Offset: 0x000029F0
		public override ICryptoTransform CreateDecryptor(byte[] key, byte[] iv)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			if (!base.ValidKeySize(key.Length * 8))
			{
				throw new ArgumentException(SR.GetString("The specified key is not a valid size for this algorithm."), "key");
			}
			if (iv != null && iv.Length * 8 != this.BlockSizeValue)
			{
				throw new ArgumentException(SR.GetString("The specified initialization vector (IV) does not match the block size for this algorithm."), "iv");
			}
			return this.m_rijndael.CreateDecryptor(key, iv);
		}

		/// <summary>Creates a symmetric encryptor object using the current key and initialization vector (IV).</summary>
		/// <returns>A symmetric encryptor object.</returns>
		// Token: 0x06000196 RID: 406 RVA: 0x0000485F File Offset: 0x00002A5F
		public override ICryptoTransform CreateEncryptor()
		{
			return this.m_rijndael.CreateEncryptor();
		}

		/// <summary>Creates a symmetric encryptor object using the specified key and initialization vector (IV).</summary>
		/// <param name="key">The secret key to use for the symmetric algorithm.</param>
		/// <param name="iv">The initialization vector to use for the symmetric algorithm.</param>
		/// <returns>A symmetric encryptor object.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="key" /> or <paramref name="iv" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="key" /> is invalid.</exception>
		// Token: 0x06000197 RID: 407 RVA: 0x0000486C File Offset: 0x00002A6C
		public override ICryptoTransform CreateEncryptor(byte[] key, byte[] iv)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			if (!base.ValidKeySize(key.Length * 8))
			{
				throw new ArgumentException(SR.GetString("The specified key is not a valid size for this algorithm."), "key");
			}
			if (iv != null && iv.Length * 8 != this.BlockSizeValue)
			{
				throw new ArgumentException(SR.GetString("The specified initialization vector (IV) does not match the block size for this algorithm."), "iv");
			}
			return this.m_rijndael.CreateEncryptor(key, iv);
		}

		// Token: 0x06000198 RID: 408 RVA: 0x000048DC File Offset: 0x00002ADC
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
					((IDisposable)this.m_rijndael).Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		/// <summary>Generates a random initialization vector (IV) to use for the symmetric algorithm.</summary>
		// Token: 0x06000199 RID: 409 RVA: 0x00004914 File Offset: 0x00002B14
		public override void GenerateIV()
		{
			this.m_rijndael.GenerateIV();
		}

		/// <summary>Generates a random key to use for the symmetric algorithm. </summary>
		// Token: 0x0600019A RID: 410 RVA: 0x00004921 File Offset: 0x00002B21
		public override void GenerateKey()
		{
			this.m_rijndael.GenerateKey();
		}

		// Token: 0x0400024B RID: 587
		private RijndaelManaged m_rijndael;
	}
}
