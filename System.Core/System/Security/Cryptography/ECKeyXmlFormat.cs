﻿using System;

namespace System.Security.Cryptography
{
	/// <summary>Defines XML serialization formats for elliptic curve keys.</summary>
	// Token: 0x02000059 RID: 89
	public enum ECKeyXmlFormat
	{
		/// <summary>An XML serialization format described in RFC 4050, "Using the Elliptic Curve Signature Algorithm (ECDSA) for XML Digital Signatures."</summary>
		// Token: 0x0400029D RID: 669
		Rfc4050
	}
}
