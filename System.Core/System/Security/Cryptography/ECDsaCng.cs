﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Security.Permissions;

namespace System.Security.Cryptography
{
	/// <summary>Provides a Cryptography Next Generation (CNG) implementation of the Elliptic Curve Digital Signature Algorithm (ECDSA). </summary>
	// Token: 0x02000058 RID: 88
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class ECDsaCng : ECDsa
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.ECDsaCng" /> class with a random key pair.</summary>
		/// <exception cref="T:System.PlatformNotSupportedException">Cryptography Next Generation (CNG) classes are not supported on this system.</exception>
		// Token: 0x06000251 RID: 593 RVA: 0x00005768 File Offset: 0x00003968
		public ECDsaCng() : this(521)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.ECDsaCng" /> class with a random key pair, using the specified key size.</summary>
		/// <param name="keySize">The size of the key. Valid key sizes are 256, 384, and 521 bits.</param>
		/// <exception cref="T:System.PlatformNotSupportedException">Cryptography Next Generation (CNG) classes are not supported on this system.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">
		///         <paramref name="keySize" /> specifies an invalid length. </exception>
		// Token: 0x06000252 RID: 594 RVA: 0x00005775 File Offset: 0x00003975
		public ECDsaCng(int keySize)
		{
			throw new NotImplementedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.ECDsaCng" /> class by using the specified <see cref="T:System.Security.Cryptography.CngKey" /> object.</summary>
		/// <param name="key">The key that will be used as input to the cryptographic operations performed by the current object.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="key" /> does not specify an Elliptic Curve Digital Signature Algorithm (ECDSA) group.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="key" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.PlatformNotSupportedException">Cryptography Next Generation (CNG) classes are not supported on this system.</exception>
		// Token: 0x06000253 RID: 595 RVA: 0x00005775 File Offset: 0x00003975
		[SecuritySafeCritical]
		public ECDsaCng(CngKey key)
		{
			throw new NotImplementedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.ECDsaCng" /> class whose public/private key pair is generated over the specified curve.</summary>
		/// <param name="curve">The curve used to generate the public/private key pair. </param>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">
		///         <paramref name="curve" /> does not validate.</exception>
		// Token: 0x06000254 RID: 596 RVA: 0x00005775 File Offset: 0x00003975
		public ECDsaCng(ECCurve curve)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets or sets the hash algorithm to use when signing and verifying data.</summary>
		/// <returns>An object that specifies the hash algorithm.</returns>
		/// <exception cref="T:System.ArgumentNullException">The value is <see langword="null" />.</exception>
		// Token: 0x17000085 RID: 133
		// (get) Token: 0x06000255 RID: 597 RVA: 0x00005782 File Offset: 0x00003982
		// (set) Token: 0x06000256 RID: 598 RVA: 0x0000578A File Offset: 0x0000398A
		public CngAlgorithm HashAlgorithm
		{
			[CompilerGenerated]
			get
			{
				return this.<HashAlgorithm>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<HashAlgorithm>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets the key to use when signing and verifying data.</summary>
		/// <returns>An object that specifies the key.</returns>
		// Token: 0x17000086 RID: 134
		// (get) Token: 0x06000257 RID: 599 RVA: 0x0000227E File Offset: 0x0000047E
		// (set) Token: 0x06000258 RID: 600 RVA: 0x0000227E File Offset: 0x0000047E
		public CngKey Key
		{
			get
			{
				throw new NotImplementedException();
			}
			private set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Generates a signature for the specified hash value.</summary>
		/// <param name="hash">The hash value of the data to be signed.</param>
		/// <returns>A digital signature for the specified hash value.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="hash" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The key information that is associated with the instance does not have a private key.</exception>
		// Token: 0x06000259 RID: 601 RVA: 0x0000227E File Offset: 0x0000047E
		public override byte[] SignHash(byte[] hash)
		{
			throw new NotImplementedException();
		}

		/// <summary>Verifies the specified digital signature against a specified hash value.</summary>
		/// <param name="hash">The hash value of the data to be verified.</param>
		/// <param name="signature">The digital signature of the data to be verified against the hash value.</param>
		/// <returns>
		///     <see langword="true" /> if the signature is valid; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="hash" /> or <paramref name="signature" /> is <see langword="null" />.</exception>
		// Token: 0x0600025A RID: 602 RVA: 0x0000227E File Offset: 0x0000047E
		public override bool VerifyHash(byte[] hash, byte[] signature)
		{
			throw new NotImplementedException();
		}

		/// <summary>Deserializes the key information from an XML string by using the specified format.</summary>
		/// <param name="xml">The XML-based key information to be deserialized.</param>
		/// <param name="format">One of the enumeration values that specifies the format of the XML string. The only currently accepted format is <see cref="F:System.Security.Cryptography.ECKeyXmlFormat.Rfc4050" />.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="xml" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="format" /> specifies an invalid format. The only accepted value is <see cref="F:System.Security.Cryptography.ECKeyXmlFormat.Rfc4050" />.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">All other errors.</exception>
		// Token: 0x0600025B RID: 603 RVA: 0x0000227E File Offset: 0x0000047E
		public void FromXmlString(string xml, ECKeyXmlFormat format)
		{
			throw new NotImplementedException();
		}

		/// <summary>Generates a signature for the specified data.</summary>
		/// <param name="data">The message data to be signed.</param>
		/// <returns>A digital signature for the specified data.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="data" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The key information that is associated with the instance does not have a private key.</exception>
		// Token: 0x0600025C RID: 604 RVA: 0x0000227E File Offset: 0x0000047E
		public byte[] SignData(byte[] data)
		{
			throw new NotImplementedException();
		}

		/// <summary>Generates a signature for the specified data stream, reading to the end of the stream.</summary>
		/// <param name="data">The data stream to be signed.</param>
		/// <returns>A digital signature for the specified data stream.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="data" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The key information that is associated with the instance does not have a private key.</exception>
		// Token: 0x0600025D RID: 605 RVA: 0x0000227E File Offset: 0x0000047E
		public byte[] SignData(Stream data)
		{
			throw new NotImplementedException();
		}

		/// <summary>Generates a digital signature for the specified length of data, beginning at the specified offset. </summary>
		/// <param name="data">The message data to be signed.</param>
		/// <param name="offset">The location in the string at which to start signing.</param>
		/// <param name="count">The length of the string, in characters, following <paramref name="offset" /> that will be signed.</param>
		/// <returns>A digital signature for the specified length of data.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="data" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="count" /> or <paramref name="offset" /> caused reading outside the bounds of the data string. </exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The key information that is associated with the instance does not have a private key.</exception>
		// Token: 0x0600025E RID: 606 RVA: 0x0000227E File Offset: 0x0000047E
		public byte[] SignData(byte[] data, int offset, int count)
		{
			throw new NotImplementedException();
		}

		/// <summary>Serializes the key information to an XML string by using the specified format.</summary>
		/// <param name="format">One of the enumeration values that specifies the format of the XML string. The only currently accepted format is <see cref="F:System.Security.Cryptography.ECKeyXmlFormat.Rfc4050" />.</param>
		/// <returns>A string object that contains the key information, serialized to an XML string according to the requested format.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="format" /> specifies an invalid format. The only accepted value is <see cref="F:System.Security.Cryptography.ECKeyXmlFormat.Rfc4050" />.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">All other errors.</exception>
		// Token: 0x0600025F RID: 607 RVA: 0x0000227E File Offset: 0x0000047E
		public string ToXmlString(ECKeyXmlFormat format)
		{
			throw new NotImplementedException();
		}

		/// <summary>Verifies the digital signature of the specified data. </summary>
		/// <param name="data">The data that was signed.</param>
		/// <param name="signature">The signature to be verified.</param>
		/// <returns>
		///     <see langword="true" /> if the signature is valid; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="data" /> or <paramref name="signature" /> is <see langword="null" />.</exception>
		// Token: 0x06000260 RID: 608 RVA: 0x0000227E File Offset: 0x0000047E
		public bool VerifyData(byte[] data, byte[] signature)
		{
			throw new NotImplementedException();
		}

		/// <summary>Verifies the digital signature of the specified data stream, reading to the end of the stream.</summary>
		/// <param name="data">The data stream that was signed.</param>
		/// <param name="signature">The signature to be verified.</param>
		/// <returns>
		///     <see langword="true" /> if the signature is valid; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="data" /> or <paramref name="signature" /> is <see langword="null" />.</exception>
		// Token: 0x06000261 RID: 609 RVA: 0x0000227E File Offset: 0x0000047E
		public bool VerifyData(Stream data, byte[] signature)
		{
			throw new NotImplementedException();
		}

		/// <summary>Verifies a signature for the specified length of data, beginning at the specified offset.</summary>
		/// <param name="data">The data that was signed.</param>
		/// <param name="offset">The location in the data at which the signed data begins.</param>
		/// <param name="count">The length of the data, in characters, following <paramref name="offset" /> that will be signed.</param>
		/// <param name="signature">The signature to be verified.</param>
		/// <returns>
		///     <see langword="true" /> if the signature is valid; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> or <paramref name="count" /> is less then zero. -or-
		///         <paramref name="offset" /> or <paramref name="count" /> is larger than the length of the byte array passed in the <paramref name="data" /> parameter.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="data" /> or <paramref name="signature" /> is <see langword="null" />.</exception>
		// Token: 0x06000262 RID: 610 RVA: 0x0000227E File Offset: 0x0000047E
		public bool VerifyData(byte[] data, int offset, int count, byte[] signature)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0400029B RID: 667
		[CompilerGenerated]
		private CngAlgorithm <HashAlgorithm>k__BackingField;
	}
}
