﻿using System;
using System.Collections.ObjectModel;
using System.Security.Permissions;

namespace System.Security.Cryptography
{
	/// <summary>Provides a strongly typed collection of Cryptography Next Generation (CNG) properties.</summary>
	// Token: 0x02000052 RID: 82
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class CngPropertyCollection : Collection<CngProperty>
	{
		/// <summary>Initializes a new <see cref="T:System.Security.Cryptography.CngPropertyCollection" /> object.</summary>
		// Token: 0x0600020A RID: 522 RVA: 0x0000520F File Offset: 0x0000340F
		public CngPropertyCollection()
		{
		}
	}
}
