﻿using System;

namespace System.Security.Cryptography
{
	/// <summary>Defines a wrapper object to access the cryptographic service provider (CSP) implementation of the <see cref="T:System.Security.Cryptography.SHA512" /> algorithm. </summary>
	// Token: 0x0200006C RID: 108
	public sealed class SHA512CryptoServiceProvider : SHA512
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.SHA512CryptoServiceProvider" /> class. </summary>
		// Token: 0x060002BB RID: 699 RVA: 0x00007710 File Offset: 0x00005910
		[SecurityCritical]
		public SHA512CryptoServiceProvider()
		{
			this.hash = new SHA512Managed();
		}

		/// <summary>Initializes, or reinitializes, an instance of a hash algorithm.</summary>
		// Token: 0x060002BC RID: 700 RVA: 0x00007723 File Offset: 0x00005923
		[SecurityCritical]
		public override void Initialize()
		{
			this.hash.Initialize();
		}

		// Token: 0x060002BD RID: 701 RVA: 0x00007730 File Offset: 0x00005930
		[SecurityCritical]
		protected override void HashCore(byte[] array, int ibStart, int cbSize)
		{
			this.hash.TransformBlock(array, ibStart, cbSize, null, 0);
		}

		// Token: 0x060002BE RID: 702 RVA: 0x00007743 File Offset: 0x00005943
		[SecurityCritical]
		protected override byte[] HashFinal()
		{
			this.hash.TransformFinalBlock(SHA512CryptoServiceProvider.Empty, 0, 0);
			this.HashValue = this.hash.Hash;
			return this.HashValue;
		}

		// Token: 0x060002BF RID: 703 RVA: 0x0000776F File Offset: 0x0000596F
		[SecurityCritical]
		protected override void Dispose(bool disposing)
		{
			((IDisposable)this.hash).Dispose();
			base.Dispose(disposing);
		}

		// Token: 0x060002C0 RID: 704 RVA: 0x00007783 File Offset: 0x00005983
		// Note: this type is marked as 'beforefieldinit'.
		static SHA512CryptoServiceProvider()
		{
		}

		// Token: 0x040002E7 RID: 743
		private static byte[] Empty = new byte[0];

		// Token: 0x040002E8 RID: 744
		private SHA512 hash;
	}
}
