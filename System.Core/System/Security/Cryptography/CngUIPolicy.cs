﻿using System;
using System.Security.Permissions;

namespace System.Security.Cryptography
{
	/// <summary>Encapsulates optional configuration parameters for the user interface (UI) that Cryptography Next Generation (CNG) displays when you access a protected key.</summary>
	// Token: 0x02000054 RID: 84
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class CngUIPolicy
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.CngUIPolicy" /> class by using the specified protection level.</summary>
		/// <param name="protectionLevel">A bitwise combination of the enumeration values that specify the protection level.</param>
		// Token: 0x06000215 RID: 533 RVA: 0x00005326 File Offset: 0x00003526
		public CngUIPolicy(CngUIProtectionLevels protectionLevel) : this(protectionLevel, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.CngUIPolicy" /> class by using the specified protection level and friendly name.</summary>
		/// <param name="protectionLevel">A bitwise combination of the enumeration values that specify the protection level.  </param>
		/// <param name="friendlyName">A friendly name for the key to be used in the UI prompt. Specify a null string to use the default name.</param>
		// Token: 0x06000216 RID: 534 RVA: 0x00005330 File Offset: 0x00003530
		public CngUIPolicy(CngUIProtectionLevels protectionLevel, string friendlyName) : this(protectionLevel, friendlyName, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.CngUIPolicy" /> class by using the specified protection level, friendly name, and description.</summary>
		/// <param name="protectionLevel">A bitwise combination of the enumeration values that specify the protection level.  </param>
		/// <param name="friendlyName">A friendly name for the key to be used in the UI prompt. Specify a null string to use the default name.</param>
		/// <param name="description">The full-text description of the key. Specify a null string to use the default description.</param>
		// Token: 0x06000217 RID: 535 RVA: 0x0000533B File Offset: 0x0000353B
		public CngUIPolicy(CngUIProtectionLevels protectionLevel, string friendlyName, string description) : this(protectionLevel, friendlyName, description, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.CngUIPolicy" /> class by using the specified protection level, friendly name, description string, and use context.</summary>
		/// <param name="protectionLevel">A bitwise combination of the enumeration values that specify the protection level.  </param>
		/// <param name="friendlyName">A friendly name for the key to be used in the UI prompt. Specify a null string to use the default name.</param>
		/// <param name="description">The full-text description of the key. Specify a null string to use the default description.</param>
		/// <param name="useContext">A description of how the key will be used. Specify a null string to use the default description.</param>
		// Token: 0x06000218 RID: 536 RVA: 0x00005347 File Offset: 0x00003547
		public CngUIPolicy(CngUIProtectionLevels protectionLevel, string friendlyName, string description, string useContext) : this(protectionLevel, friendlyName, description, useContext, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.CngUIPolicy" /> class by using the specified protection level, friendly name, description string, use context, and title.</summary>
		/// <param name="protectionLevel">A bitwise combination of the enumeration values that specify the protection level.  </param>
		/// <param name="friendlyName">A friendly name for the key to be used in the UI prompt. Specify a null string to use the default name.</param>
		/// <param name="description">The full-text description of the key. Specify a null string to use the default description.</param>
		/// <param name="useContext">A description of how the key will be used. Specify a null string to use the default description.</param>
		/// <param name="creationTitle">The title for the dialog box that provides the UI prompt. Specify a null string to use the default title.</param>
		// Token: 0x06000219 RID: 537 RVA: 0x00005355 File Offset: 0x00003555
		public CngUIPolicy(CngUIProtectionLevels protectionLevel, string friendlyName, string description, string useContext, string creationTitle)
		{
			this.m_creationTitle = creationTitle;
			this.m_description = description;
			this.m_friendlyName = friendlyName;
			this.m_protectionLevel = protectionLevel;
			this.m_useContext = useContext;
		}

		/// <summary>Gets the title that is displayed by the UI prompt.</summary>
		/// <returns>The title of the dialog box that appears when the key is accessed.</returns>
		// Token: 0x1700007B RID: 123
		// (get) Token: 0x0600021A RID: 538 RVA: 0x00005382 File Offset: 0x00003582
		public string CreationTitle
		{
			get
			{
				return this.m_creationTitle;
			}
		}

		/// <summary>Gets the description string that is displayed by the UI prompt.</summary>
		/// <returns>The description text for the dialog box that appears when the key is accessed.</returns>
		// Token: 0x1700007C RID: 124
		// (get) Token: 0x0600021B RID: 539 RVA: 0x0000538A File Offset: 0x0000358A
		public string Description
		{
			get
			{
				return this.m_description;
			}
		}

		/// <summary>Gets the friendly name that is displayed by the UI prompt.</summary>
		/// <returns>The friendly name that is used to describe the key in the dialog box that appears when the key is accessed.</returns>
		// Token: 0x1700007D RID: 125
		// (get) Token: 0x0600021C RID: 540 RVA: 0x00005392 File Offset: 0x00003592
		public string FriendlyName
		{
			get
			{
				return this.m_friendlyName;
			}
		}

		/// <summary>Gets the UI protection level for the key.</summary>
		/// <returns>An object that describes the level of UI protection to apply to the key.</returns>
		// Token: 0x1700007E RID: 126
		// (get) Token: 0x0600021D RID: 541 RVA: 0x0000539A File Offset: 0x0000359A
		public CngUIProtectionLevels ProtectionLevel
		{
			get
			{
				return this.m_protectionLevel;
			}
		}

		/// <summary>Gets the description of how the key will be used.</summary>
		/// <returns>The description of how the key will be used.</returns>
		// Token: 0x1700007F RID: 127
		// (get) Token: 0x0600021E RID: 542 RVA: 0x000053A2 File Offset: 0x000035A2
		public string UseContext
		{
			get
			{
				return this.m_useContext;
			}
		}

		// Token: 0x04000295 RID: 661
		private string m_creationTitle;

		// Token: 0x04000296 RID: 662
		private string m_description;

		// Token: 0x04000297 RID: 663
		private string m_friendlyName;

		// Token: 0x04000298 RID: 664
		private CngUIProtectionLevels m_protectionLevel;

		// Token: 0x04000299 RID: 665
		private string m_useContext;
	}
}
