﻿using System;

namespace System.Security.Cryptography
{
	/// <summary>Represents an elliptic curve.</summary>
	// Token: 0x02000065 RID: 101
	public struct ECCurve
	{
		/// <summary>Gets a value that indicates whether the curve type indicates an explicit characteristic 2 curve.</summary>
		/// <returns>
		///     <see langword="true" /> if the curve is an explicit characteristic 2 curve; <see langword="false" /> if the curve is a named characteristic 2, prime, or implicit curve.</returns>
		// Token: 0x17000090 RID: 144
		// (get) Token: 0x06000294 RID: 660 RVA: 0x0000227E File Offset: 0x0000047E
		public bool IsCharacteristic2
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets a value that indicates whether the curve type indicates an explicit curve (either prime or characteristic 2).</summary>
		/// <returns>
		///     <see langword="true" /> if the curve is an explicit curve (either prime or characteristic 2); <see langword="false" /> if the curve is a named or implicit curve.</returns>
		// Token: 0x17000091 RID: 145
		// (get) Token: 0x06000295 RID: 661 RVA: 0x0000227E File Offset: 0x0000047E
		public bool IsExplicit
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets a value that indicates whether the curve type indicates a named curve.</summary>
		/// <returns>
		///     <see langword="true" /> if the curve is a named curve; <see langword="false" /> if the curve is an implict or an  explicit curve (either prime or characteristic 2).</returns>
		// Token: 0x17000092 RID: 146
		// (get) Token: 0x06000296 RID: 662 RVA: 0x0000227E File Offset: 0x0000047E
		public bool IsNamed
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets a value that indicates whether the curve type indicates an explicit prime curve.</summary>
		/// <returns>
		///     <see langword="true" /> if the curve is an explicit prime curve; <see langword="false" /> if the curve is a named prime, characteristic 2 or implicit curves.</returns>
		// Token: 0x17000093 RID: 147
		// (get) Token: 0x06000297 RID: 663 RVA: 0x0000227E File Offset: 0x0000047E
		public bool IsPrime
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the identifier of a named curve.</summary>
		/// <returns>The identifier of a named curve.</returns>
		// Token: 0x17000094 RID: 148
		// (get) Token: 0x06000298 RID: 664 RVA: 0x0000227E File Offset: 0x0000047E
		public Oid Oid
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Creates a named curve using the specified friendly name of the identifier.</summary>
		/// <param name="oidFriendlyName">The friendly name of the identifier.</param>
		/// <returns>An object representing the named curve.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="oidFriendlyName" /> is <see langword="null" />.</exception>
		// Token: 0x06000299 RID: 665 RVA: 0x0000227E File Offset: 0x0000047E
		public static ECCurve CreateFromFriendlyName(string oidFriendlyName)
		{
			throw new NotImplementedException();
		}

		/// <summary>Creates a named curve using the specified <see cref="T:System.Security.Cryptography.Oid" /> object.</summary>
		/// <param name="curveOid">The object identifier to use.</param>
		/// <returns>An object representing the named curve.</returns>
		// Token: 0x0600029A RID: 666 RVA: 0x0000227E File Offset: 0x0000047E
		public static ECCurve CreateFromOid(Oid curveOid)
		{
			throw new NotImplementedException();
		}

		/// <summary>Creates a named curve using the specified dotted-decimal representation of the identifier.</summary>
		/// <param name="oidValue">The dotted number of the identifier.</param>
		/// <returns>An object representing the named curve.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="oidValue" /> is <see langword="null" />.</exception>
		// Token: 0x0600029B RID: 667 RVA: 0x0000227E File Offset: 0x0000047E
		public static ECCurve CreateFromValue(string oidValue)
		{
			throw new NotImplementedException();
		}

		/// <summary>Validates the integrity of the current curve. Throws a <see cref="T:System.Security.Cryptography.CryptographicException" /> exception if the structure is not valid.</summary>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The curve parameters are not valid for the current curve type.</exception>
		// Token: 0x0600029C RID: 668 RVA: 0x0000227E File Offset: 0x0000047E
		public void Validate()
		{
			throw new NotImplementedException();
		}

		/// <summary>The first coefficient for an explicit curve. A for short Weierstrass, Montgomery, and Twisted Edwards curves.</summary>
		/// <returns>Coefficient A.</returns>
		// Token: 0x040002CD RID: 717
		public byte[] A;

		/// <summary>The second coefficient for an explicit curve. B for short Weierstrass and d for Twisted Edwards curves.</summary>
		/// <returns>Coefficient B.</returns>
		// Token: 0x040002CE RID: 718
		public byte[] B;

		/// <summary>The cofactor of the curve.</summary>
		/// <returns>The cofactor of the curve.</returns>
		// Token: 0x040002CF RID: 719
		public byte[] Cofactor;

		/// <summary>Identifies the composition of the <see cref="T:System.Security.Cryptography.ECCurve" /> object.</summary>
		/// <returns>The curve type.</returns>
		// Token: 0x040002D0 RID: 720
		public ECCurve.ECCurveType CurveType;

		/// <summary>The generator, or base point, for operations on the curve.</summary>
		/// <returns>The base point.</returns>
		// Token: 0x040002D1 RID: 721
		public ECPoint G;

		/// <summary>The name of the hash algorithm which was used to generate the curve coefficients (<see cref="F:System.Security.Cryptography.ECCurve.A" /> and <see cref="F:System.Security.Cryptography.ECCurve.B" />) from the <see cref="F:System.Security.Cryptography.ECCurve.Seed" /> under the ANSI X9.62 generation algorithm. Applies only to explicit curves.</summary>
		/// <returns>The name of the hash algorithm used to generate the curve coefficients.</returns>
		// Token: 0x040002D2 RID: 722
		public HashAlgorithmName? Hash;

		/// <summary>The order of the curve. Applies only to explicit curves.</summary>
		/// <returns>The order of the curve. </returns>
		// Token: 0x040002D3 RID: 723
		public byte[] Order;

		/// <summary>The curve polynomial. Applies only to characteristic 2 curves.</summary>
		/// <returns>The curve polynomial.</returns>
		// Token: 0x040002D4 RID: 724
		public byte[] Polynomial;

		/// <summary>The prime specifying the base field. Applies only to prime curves.</summary>
		/// <returns>The prime P.</returns>
		// Token: 0x040002D5 RID: 725
		public byte[] Prime;

		/// <summary>The seed value for coefficient generation under the ANSI X9.62 generation algorithm. Applies only to explicit curves.</summary>
		/// <returns>The seed value.</returns>
		// Token: 0x040002D6 RID: 726
		public byte[] Seed;

		/// <summary>Indicates how to interpret the data contained in an <see cref="T:System.Security.Cryptography.ECCurve" /> object.</summary>
		// Token: 0x02000066 RID: 102
		public enum ECCurveType
		{
			/// <summary>No curve data is interpreted. The caller is assumed to know what the curve is.</summary>
			// Token: 0x040002D8 RID: 728
			Implicit,
			/// <summary>The curve parameters represent a prime curve with the formula y^2 = x^3 + A*x + B in the prime field P.</summary>
			// Token: 0x040002D9 RID: 729
			PrimeShortWeierstrass,
			/// <summary>The curve parameters represent a prime curve with the formula A*x^2 + y^2 = 1 + B*x^2*y^2 in the prime field P.</summary>
			// Token: 0x040002DA RID: 730
			PrimeTwistedEdwards,
			/// <summary>The curve parameters represent a prime curve with the formula B*y^2 = x^3 + A*x^2 + x.</summary>
			// Token: 0x040002DB RID: 731
			PrimeMontgomery,
			/// <summary>The curve parameters represent a characteristic 2 curve.</summary>
			// Token: 0x040002DC RID: 732
			Characteristic2,
			/// <summary>The curve parameters represent a named curve.</summary>
			// Token: 0x040002DD RID: 733
			Named
		}

		/// <summary>Represents a factory class for creating named curves.</summary>
		// Token: 0x02000067 RID: 103
		public static class NamedCurves
		{
			/// <summary>Gets a brainpoolP160r1 named curve.</summary>
			/// <returns>A brainpoolP160r1 named curve.</returns>
			// Token: 0x17000095 RID: 149
			// (get) Token: 0x0600029D RID: 669 RVA: 0x0000227E File Offset: 0x0000047E
			public static ECCurve brainpoolP160r1
			{
				get
				{
					throw new NotImplementedException();
				}
			}

			/// <summary>Gets a brainpoolP160t1 named curve.</summary>
			/// <returns>A brainpoolP160t1 named curve.</returns>
			// Token: 0x17000096 RID: 150
			// (get) Token: 0x0600029E RID: 670 RVA: 0x0000227E File Offset: 0x0000047E
			public static ECCurve brainpoolP160t1
			{
				get
				{
					throw new NotImplementedException();
				}
			}

			/// <summary>Gets a brainpoolP192r1 named curve.</summary>
			/// <returns>A brainpoolP192r1 named curve.</returns>
			// Token: 0x17000097 RID: 151
			// (get) Token: 0x0600029F RID: 671 RVA: 0x0000227E File Offset: 0x0000047E
			public static ECCurve brainpoolP192r1
			{
				get
				{
					throw new NotImplementedException();
				}
			}

			/// <summary>Gets a brainpoolP192t1 named curve.</summary>
			/// <returns>A brainpoolP192t1 named curve.</returns>
			// Token: 0x17000098 RID: 152
			// (get) Token: 0x060002A0 RID: 672 RVA: 0x0000227E File Offset: 0x0000047E
			public static ECCurve brainpoolP192t1
			{
				get
				{
					throw new NotImplementedException();
				}
			}

			/// <summary>Gets a brainpoolP224r1 named curve.</summary>
			/// <returns>A brainpoolP224r1 named curve.</returns>
			// Token: 0x17000099 RID: 153
			// (get) Token: 0x060002A1 RID: 673 RVA: 0x0000227E File Offset: 0x0000047E
			public static ECCurve brainpoolP224r1
			{
				get
				{
					throw new NotImplementedException();
				}
			}

			/// <summary>Gets a brainpoolP224t1 named curve.</summary>
			/// <returns>A brainpoolP224t1 named curve.</returns>
			// Token: 0x1700009A RID: 154
			// (get) Token: 0x060002A2 RID: 674 RVA: 0x0000227E File Offset: 0x0000047E
			public static ECCurve brainpoolP224t1
			{
				get
				{
					throw new NotImplementedException();
				}
			}

			/// <summary>Gets a brainpoolP256r1 named curve.</summary>
			/// <returns>A brainpoolP256r1 named curve.</returns>
			// Token: 0x1700009B RID: 155
			// (get) Token: 0x060002A3 RID: 675 RVA: 0x0000227E File Offset: 0x0000047E
			public static ECCurve brainpoolP256r1
			{
				get
				{
					throw new NotImplementedException();
				}
			}

			/// <summary>Gets a brainpoolP256t1 named curve.</summary>
			/// <returns>A brainpoolP256t1 named curve.</returns>
			// Token: 0x1700009C RID: 156
			// (get) Token: 0x060002A4 RID: 676 RVA: 0x0000227E File Offset: 0x0000047E
			public static ECCurve brainpoolP256t1
			{
				get
				{
					throw new NotImplementedException();
				}
			}

			/// <summary>Gets a brainpoolP320r1 named curve.</summary>
			/// <returns>A brainpoolP320r1 named curve.</returns>
			// Token: 0x1700009D RID: 157
			// (get) Token: 0x060002A5 RID: 677 RVA: 0x0000227E File Offset: 0x0000047E
			public static ECCurve brainpoolP320r1
			{
				get
				{
					throw new NotImplementedException();
				}
			}

			/// <summary>Gets a brainpoolP320t1 named curve.</summary>
			/// <returns>A brainpoolP320t1 named curve.</returns>
			// Token: 0x1700009E RID: 158
			// (get) Token: 0x060002A6 RID: 678 RVA: 0x0000227E File Offset: 0x0000047E
			public static ECCurve brainpoolP320t1
			{
				get
				{
					throw new NotImplementedException();
				}
			}

			/// <summary>Gets a brainpoolP384r1 named curve.</summary>
			/// <returns>A brainpoolP384r1 named curve.</returns>
			// Token: 0x1700009F RID: 159
			// (get) Token: 0x060002A7 RID: 679 RVA: 0x0000227E File Offset: 0x0000047E
			public static ECCurve brainpoolP384r1
			{
				get
				{
					throw new NotImplementedException();
				}
			}

			/// <summary>Gets a brainpoolP384t1 named curve.</summary>
			/// <returns>A brainpoolP384t1 named curve.</returns>
			// Token: 0x170000A0 RID: 160
			// (get) Token: 0x060002A8 RID: 680 RVA: 0x0000227E File Offset: 0x0000047E
			public static ECCurve brainpoolP384t1
			{
				get
				{
					throw new NotImplementedException();
				}
			}

			/// <summary>Gets a brainpoolP512r1 named curve.</summary>
			/// <returns>A brainpoolP512r1 named curve.</returns>
			// Token: 0x170000A1 RID: 161
			// (get) Token: 0x060002A9 RID: 681 RVA: 0x0000227E File Offset: 0x0000047E
			public static ECCurve brainpoolP512r1
			{
				get
				{
					throw new NotImplementedException();
				}
			}

			/// <summary>Gets a brainpoolP512t1 named curve.</summary>
			/// <returns>A brainpoolP512t1 named curve.</returns>
			// Token: 0x170000A2 RID: 162
			// (get) Token: 0x060002AA RID: 682 RVA: 0x0000227E File Offset: 0x0000047E
			public static ECCurve brainpoolP512t1
			{
				get
				{
					throw new NotImplementedException();
				}
			}

			/// <summary>Gets a nistP256 named curve.</summary>
			/// <returns>A nistP256 named curve.</returns>
			// Token: 0x170000A3 RID: 163
			// (get) Token: 0x060002AB RID: 683 RVA: 0x0000227E File Offset: 0x0000047E
			public static ECCurve nistP256
			{
				get
				{
					throw new NotImplementedException();
				}
			}

			/// <summary>Gets a nistP384 named curve.</summary>
			/// <returns>A nistP384 named curve.</returns>
			// Token: 0x170000A4 RID: 164
			// (get) Token: 0x060002AC RID: 684 RVA: 0x0000227E File Offset: 0x0000047E
			public static ECCurve nistP384
			{
				get
				{
					throw new NotImplementedException();
				}
			}

			/// <summary>Gets a nistP521 named curve.</summary>
			/// <returns>A nistP521 named curve.</returns>
			// Token: 0x170000A5 RID: 165
			// (get) Token: 0x060002AD RID: 685 RVA: 0x0000227E File Offset: 0x0000047E
			public static ECCurve nistP521
			{
				get
				{
					throw new NotImplementedException();
				}
			}
		}
	}
}
