﻿using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.AccessControl;
using Microsoft.Win32.SafeHandles;

namespace System.IO.MemoryMappedFiles
{
	/// <summary>Represents the permissions that can be granted for file access and operations on memory-mapped files. </summary>
	// Token: 0x0200003E RID: 62
	public class MemoryMappedFileSecurity : ObjectSecurity<MemoryMappedFileRights>
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.IO.MemoryMappedFiles.MemoryMappedFileSecurity" /> class. </summary>
		// Token: 0x06000137 RID: 311 RVA: 0x00003B5D File Offset: 0x00001D5D
		public MemoryMappedFileSecurity() : base(false, ResourceType.KernelObject)
		{
		}

		// Token: 0x06000138 RID: 312 RVA: 0x00003B67 File Offset: 0x00001D67
		[SecuritySafeCritical]
		internal MemoryMappedFileSecurity(SafeMemoryMappedFileHandle safeHandle, AccessControlSections includeSections) : base(false, ResourceType.KernelObject, safeHandle, includeSections)
		{
		}

		// Token: 0x06000139 RID: 313 RVA: 0x00003B73 File Offset: 0x00001D73
		[SecuritySafeCritical]
		internal void PersistHandle(SafeHandle handle)
		{
			base.Persist(handle);
		}
	}
}
