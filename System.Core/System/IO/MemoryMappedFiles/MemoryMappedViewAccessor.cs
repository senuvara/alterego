﻿using System;
using System.Security;
using System.Security.Permissions;
using Microsoft.Win32.SafeHandles;
using Unity;

namespace System.IO.MemoryMappedFiles
{
	/// <summary>Represents a randomly accessed view of a memory-mapped file.</summary>
	// Token: 0x0200003F RID: 63
	public sealed class MemoryMappedViewAccessor : UnmanagedMemoryAccessor
	{
		// Token: 0x0600013A RID: 314 RVA: 0x00003B7C File Offset: 0x00001D7C
		[SecurityCritical]
		internal MemoryMappedViewAccessor(MemoryMappedView view)
		{
			this.m_view = view;
			base.Initialize(this.m_view.ViewHandle, this.m_view.PointerOffset, this.m_view.Size, MemoryMappedFile.GetFileAccess(this.m_view.Access));
		}

		/// <summary>Gets a handle to the view of a memory-mapped file.</summary>
		/// <returns>A wrapper for the operating system's handle to the view of the file. </returns>
		// Token: 0x17000030 RID: 48
		// (get) Token: 0x0600013B RID: 315 RVA: 0x00003BCD File Offset: 0x00001DCD
		public SafeMemoryMappedViewHandle SafeMemoryMappedViewHandle
		{
			[SecurityCritical]
			[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
			get
			{
				if (this.m_view == null)
				{
					return null;
				}
				return this.m_view.ViewHandle;
			}
		}

		/// <summary>[Supported in the .NET Framework 4.5.1 and later versions] Gets the number of bytes by which the starting position of this view is offset from the beginning of the memory-mapped file. </summary>
		/// <returns>The number of bytes between the starting position of this view and the beginning of the memory-mapped file. </returns>
		/// <exception cref="T:System.InvalidOperationException">The object from which this instance was created is <see langword="null" />. </exception>
		// Token: 0x17000031 RID: 49
		// (get) Token: 0x0600013C RID: 316 RVA: 0x00003BE4 File Offset: 0x00001DE4
		public long PointerOffset
		{
			get
			{
				if (this.m_view == null)
				{
					throw new InvalidOperationException(SR.GetString("The underlying MemoryMappedView object is null."));
				}
				return this.m_view.PointerOffset;
			}
		}

		// Token: 0x0600013D RID: 317 RVA: 0x00003C0C File Offset: 0x00001E0C
		[SecuritySafeCritical]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.m_view != null && !this.m_view.IsClosed)
				{
					this.Flush();
				}
			}
			finally
			{
				try
				{
					if (this.m_view != null)
					{
						this.m_view.Dispose();
					}
				}
				finally
				{
					base.Dispose(disposing);
				}
			}
		}

		/// <summary>Clears all buffers for this view and causes any buffered data to be written to the underlying file.</summary>
		/// <exception cref="T:System.ObjectDisposedException">Methods were called after the accessor was closed.</exception>
		// Token: 0x0600013E RID: 318 RVA: 0x00003C74 File Offset: 0x00001E74
		[SecurityCritical]
		public void Flush()
		{
			if (!base.IsOpen)
			{
				throw new ObjectDisposedException("MemoryMappedViewAccessor", SR.GetString("Cannot access a closed accessor."));
			}
			if (this.m_view != null)
			{
				this.m_view.Flush((IntPtr)base.Capacity);
			}
		}

		// Token: 0x0600013F RID: 319 RVA: 0x0000220F File Offset: 0x0000040F
		internal MemoryMappedViewAccessor()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0400023D RID: 573
		private MemoryMappedView m_view;
	}
}
