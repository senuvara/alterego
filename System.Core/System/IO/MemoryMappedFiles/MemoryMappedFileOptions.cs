﻿using System;

namespace System.IO.MemoryMappedFiles
{
	/// <summary>Provides memory allocation options for memory-mapped files.</summary>
	// Token: 0x0200003C RID: 60
	[Flags]
	[Serializable]
	public enum MemoryMappedFileOptions
	{
		/// <summary>No memory allocation options are applied.</summary>
		// Token: 0x0400022D RID: 557
		None = 0,
		/// <summary>Memory allocation is delayed until a view is created with either the <see cref="M:System.IO.MemoryMappedFiles.MemoryMappedFile.CreateViewAccessor" /> or <see cref="M:System.IO.MemoryMappedFiles.MemoryMappedFile.CreateViewStream" /> method.</summary>
		// Token: 0x0400022E RID: 558
		DelayAllocatePages = 67108864
	}
}
