﻿using System;
using Microsoft.Win32.SafeHandles;

namespace System.IO.MemoryMappedFiles
{
	/// <summary>Represents a memory-mapped file. </summary>
	// Token: 0x02000042 RID: 66
	public class MemoryMappedFile : IDisposable
	{
		/// <summary>Creates a memory-mapped file from a file on disk.</summary>
		/// <param name="path">The path to file to map.</param>
		/// <returns>A memory-mapped file.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="path" /> is an empty string, contains only white space, or has one or more invalid characters, as defined by the <see cref="M:System.IO.Path.GetInvalidFileNameChars" /> method. -or-
		///         <paramref name="path" /> refers to an invalid device. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.IO.IOException">An I/O error occurred.</exception>
		/// <exception cref="T:System.IO.PathTooLongException">
		///         <paramref name="path" /> exceeds the maximum length defined by the operating system. In Windows, paths must contain fewer than 248 characters, and file names must contain fewer than 260 characters.</exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permissions for the file.</exception>
		// Token: 0x06000152 RID: 338 RVA: 0x00003F43 File Offset: 0x00002143
		public static MemoryMappedFile CreateFromFile(string path)
		{
			return MemoryMappedFile.CreateFromFile(path, FileMode.Open, null, 0L, MemoryMappedFileAccess.ReadWrite);
		}

		/// <summary>Creates a memory-mapped file that has the specified access mode from a file on disk. </summary>
		/// <param name="path">The path to file to map.</param>
		/// <param name="mode">Access mode; must be <see cref="F:System.IO.FileMode.Open" />.</param>
		/// <returns>A memory-mapped file that has the specified access mode.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="path" /> is an empty string, contains only white space, or has one or more invalid characters, as defined by the <see cref="M:System.IO.Path.GetInvalidFileNameChars" /> method. -or-
		///         <paramref name="path" /> refers to an invalid device.-or-
		///         <paramref name="mode" /> is <see cref="F:System.IO.FileMode.Append" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.IO.IOException">
		///         <paramref name="mode" /> is <see cref="F:System.IO.FileMode.Create" />, <see cref="F:System.IO.FileMode.CreateNew" />, or <see cref="F:System.IO.FileMode.Truncate" />.-or-
		///         <paramref name="mode" /> is <see cref="F:System.IO.FileMode.OpenOrCreate" /> and the file on disk does not exist.-or-An I/O error occurred.</exception>
		/// <exception cref="T:System.IO.PathTooLongException">
		///         <paramref name="path" /> exceeds the maximum length defined by the operating system. In Windows, paths must contain fewer than 248 characters, and file names must contain fewer than 260 characters. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permissions for the file.</exception>
		// Token: 0x06000153 RID: 339 RVA: 0x00003F50 File Offset: 0x00002150
		public static MemoryMappedFile CreateFromFile(string path, FileMode mode)
		{
			long num = 0L;
			if (path == null)
			{
				throw new ArgumentNullException("path");
			}
			if (path.Length == 0)
			{
				throw new ArgumentException("path");
			}
			if (mode == FileMode.Append)
			{
				throw new ArgumentException("mode");
			}
			IntPtr preexistingHandle = MemoryMapImpl.OpenFile(path, mode, null, out num, MemoryMappedFileAccess.ReadWrite, MemoryMappedFileOptions.None);
			return new MemoryMappedFile
			{
				handle = new SafeMemoryMappedFileHandle(preexistingHandle, true)
			};
		}

		/// <summary>Creates a memory-mapped file that has the specified access mode and name from a file on disk.</summary>
		/// <param name="path">The path to the file to map.</param>
		/// <param name="mode">Access mode; must be <see cref="F:System.IO.FileMode.Open" />.</param>
		/// <param name="mapName">A name to assign to the memory-mapped file. </param>
		/// <returns>A memory-mapped file that has the specified name and access mode.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="path" /> is an empty string, contains only white space, or has one or more invalid characters, as defined by the <see cref="M:System.IO.Path.GetInvalidFileNameChars" /> method. -or-
		///         <paramref name="path" /> refers to an invalid device.-or-
		///         <paramref name="mapName" /> is an empty string.-or-
		///         <paramref name="mode" /> is <see cref="F:System.IO.FileMode.Append" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> or <paramref name="mapName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.IO.IOException">
		///         <paramref name="mode" /> is <see cref="F:System.IO.FileMode.Create" />, <see cref="F:System.IO.FileMode.CreateNew" />, or <see cref="F:System.IO.FileMode.Truncate" />.-or-
		///         <paramref name="mode" /> is <see cref="F:System.IO.FileMode.OpenOrCreate" /> and the file on disk does not exist.-or-An I/O error occurred.</exception>
		/// <exception cref="T:System.IO.PathTooLongException">
		///         <paramref name="path" /> exceeds the maximum length defined by the operating system. In Windows, paths must contain fewer than 248 characters, and file names must contain fewer than 260 characters.</exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permissions for the file.</exception>
		// Token: 0x06000154 RID: 340 RVA: 0x00003FAF File Offset: 0x000021AF
		public static MemoryMappedFile CreateFromFile(string path, FileMode mode, string mapName)
		{
			return MemoryMappedFile.CreateFromFile(path, mode, mapName, 0L, MemoryMappedFileAccess.ReadWrite);
		}

		/// <summary>Creates a memory-mapped file that has the specified access mode, name, and capacity from a file on disk.</summary>
		/// <param name="path">The path to the file to map.</param>
		/// <param name="mode">Access mode; can be any of the <see cref="T:System.IO.FileMode" /> enumeration values except <see cref="F:System.IO.FileMode.Append" />.</param>
		/// <param name="mapName">A name to assign to the memory-mapped file. </param>
		/// <param name="capacity">The maximum size, in bytes, to allocate to the memory-mapped file. Specify 0 to set the capacity to the size of the file on disk.</param>
		/// <returns>A memory-mapped file that has the specified characteristics.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="path" /> is an empty string, contains only white space, or has one or more invalid characters, as defined by the <see cref="M:System.IO.Path.GetInvalidFileNameChars" /> method. -or-
		///         <paramref name="path" /> refers to an invalid device.-or-
		///         <paramref name="mapName" /> is an empty string.-or-
		///         <paramref name="mode" /> is <see cref="F:System.IO.FileMode.Append" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> or <paramref name="mapName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="capacity" /> is greater than the size of the logical address space.-or-
		///         <paramref name="capacity" /> is less than zero.-or-
		///         <paramref name="capacity" /> is less than the file size (but not zero).-or-
		///         <paramref name="capacity" /> is zero, and the size of the file on disk is also zero.</exception>
		/// <exception cref="T:System.IO.IOException">An I/O error occurred.</exception>
		/// <exception cref="T:System.IO.PathTooLongException">
		///         <paramref name="path" /> exceeds the maximum length defined by the operating system. In Windows, paths must contain fewer than 248 characters, and file names must contain fewer than 260 characters.</exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permissions for the file.</exception>
		// Token: 0x06000155 RID: 341 RVA: 0x00003FBC File Offset: 0x000021BC
		public static MemoryMappedFile CreateFromFile(string path, FileMode mode, string mapName, long capacity)
		{
			return MemoryMappedFile.CreateFromFile(path, mode, mapName, capacity, MemoryMappedFileAccess.ReadWrite);
		}

		/// <summary>Creates a memory-mapped file that has the specified access mode, name, capacity, and access type from a file on disk.</summary>
		/// <param name="path">The path to the file to map.</param>
		/// <param name="mode">Access mode; can be any of the <see cref="T:System.IO.FileMode" /> enumeration values except <see cref="F:System.IO.FileMode.Append" />.</param>
		/// <param name="mapName">A name to assign to the memory-mapped file. </param>
		/// <param name="capacity">The maximum size, in bytes, to allocate to the memory-mapped file. Specify 0 to set the capacity to the size of the file on disk.</param>
		/// <param name="access">One of the enumeration values that specifies the type of access allowed to the memory-mapped file.</param>
		/// <returns>A memory-mapped file that has the specified characteristics.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="mapName" /> is an empty string.-or-
		///         <paramref name="access" /> is not an allowed value.-or-
		///         <paramref name="path" /> specifies an empty file.-or-
		///         <paramref name="access" /> is specified as <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.Read" /> and capacity is greater than the size of the file indicated by <paramref name="path" />.-or-
		///         <paramref name="mode" /> is <see cref="F:System.IO.FileMode.Append" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> or <paramref name="mapName" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="capacity" /> is greater than the size of the logical address space.-or-
		///         <paramref name="capacity" /> is less than zero.-or-
		///         <paramref name="capacity" /> is less than the file size (but not zero).-or-
		///         <paramref name="capacity" /> is zero, and the size of the file on disk is also zero.-or-
		///         <paramref name="access" /> is not a defined <see cref="T:System.IO.MemoryMappedFiles.MemoryMappedFileAccess" /> value.-or-The size of the file indicated by <paramref name="path" /> is greater than <paramref name="capacity" />.</exception>
		/// <exception cref="T:System.IO.IOException">-or-An I/O error occurred.</exception>
		/// <exception cref="T:System.IO.PathTooLongException">
		///         <paramref name="path" /> exceeds the maximum length defined by the operating system. In Windows, paths must contain fewer than 248 characters, and file names must contain fewer than 260 characters.</exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permissions for the file.</exception>
		// Token: 0x06000156 RID: 342 RVA: 0x00003FC8 File Offset: 0x000021C8
		public static MemoryMappedFile CreateFromFile(string path, FileMode mode, string mapName, long capacity, MemoryMappedFileAccess access)
		{
			if (path == null)
			{
				throw new ArgumentNullException("path");
			}
			if (path.Length == 0)
			{
				throw new ArgumentException("path");
			}
			if (mapName != null && mapName.Length == 0)
			{
				throw new ArgumentException("mapName");
			}
			if (mode == FileMode.Append)
			{
				throw new ArgumentException("mode");
			}
			if (capacity < 0L)
			{
				throw new ArgumentOutOfRangeException("capacity");
			}
			IntPtr preexistingHandle = MemoryMapImpl.OpenFile(path, mode, mapName, out capacity, access, MemoryMappedFileOptions.None);
			return new MemoryMappedFile
			{
				handle = new SafeMemoryMappedFileHandle(preexistingHandle, true)
			};
		}

		/// <summary>Creates a memory-mapped file from an existing file with the specified access mode, name, inheritability, and capacity.</summary>
		/// <param name="fileStream">The file stream of the existing file.</param>
		/// <param name="mapName">A name to assign to the memory-mapped file.</param>
		/// <param name="capacity">The maximum size, in bytes, to allocate to the memory-mapped file. Specify 0 to set the capacity to the size of <paramref name="filestream" />.</param>
		/// <param name="access">One of the enumeration values that specifies the type of access allowed to the memory-mapped file. This parameter can’t be set to <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.Write" />. </param>
		/// <param name="inheritability">One of the enumeration values that specifies whether a handle to the memory-mapped file can be inherited by a child process. The default is <see cref="F:System.IO.HandleInheritability.None" />.</param>
		/// <param name="leaveOpen">A value that indicates whether to close the source file stream when the <see cref="T:System.IO.MemoryMappedFiles.MemoryMappedFile" /> is disposed. </param>
		/// <returns>A memory-mapped file that has the specified characteristics.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="mapName" /> is <see langword="null" /> or an empty string.-or-
		///         <paramref name="capacity" /> and the length of the file are zero.-or-
		///         <paramref name="access" /> is set to <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.Write" /> or <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.Write" /> enumeration value, which is not allowed.-or-
		///         <paramref name="access" /> is set to <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.Read" /> and <paramref name="capacity" /> is larger than the length of <see langword="filestream" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="fileStream" />  is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="capacity" /> is less than zero.-or-
		///         <paramref name="capacity" /> is less than the file size.-or-
		///         <paramref name="access" /> is not a valid <see cref="T:System.IO.MemoryMappedFiles.MemoryMappedFileAccess" /> enumeration value.-or-
		///         <paramref name="inheritability" /> is not a valid <see cref="T:System.IO.HandleInheritability" /> enumeration value.</exception>
		// Token: 0x06000157 RID: 343 RVA: 0x0000404C File Offset: 0x0000224C
		public static MemoryMappedFile CreateFromFile(FileStream fileStream, string mapName, long capacity, MemoryMappedFileAccess access, HandleInheritability inheritability, bool leaveOpen)
		{
			if (fileStream == null)
			{
				throw new ArgumentNullException("fileStream");
			}
			if (mapName != null && mapName.Length == 0)
			{
				throw new ArgumentException("mapName");
			}
			if ((!MonoUtil.IsUnix && capacity == 0L && fileStream.Length == 0L) || capacity > fileStream.Length)
			{
				throw new ArgumentException("capacity");
			}
			IntPtr preexistingHandle = MemoryMapImpl.OpenHandle(fileStream.SafeFileHandle.DangerousGetHandle(), mapName, out capacity, access, MemoryMappedFileOptions.None);
			MemoryMapImpl.ConfigureHandleInheritability(preexistingHandle, inheritability);
			return new MemoryMappedFile
			{
				handle = new SafeMemoryMappedFileHandle(preexistingHandle, true),
				stream = fileStream,
				keepOpen = leaveOpen
			};
		}

		/// <summary>Creates a memory-mapped file that has the specified name, capacity, access type, security permissions, inheritability, and disposal requirement from a file on disk. </summary>
		/// <param name="fileStream">The <paramref name="fileStream" /> to the file to map.</param>
		/// <param name="mapName">A name to assign to the memory-mapped file.</param>
		/// <param name="capacity">The maximum size, in bytes, to allocate to the memory-mapped file. Specify 0 to set the capacity to the size of the file on disk.</param>
		/// <param name="access">One of the enumeration values that specifies the type of access allowed to the memory-mapped file. This parameter can’t be set to <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.Write" />. </param>
		/// <param name="memoryMappedFileSecurity">The permissions that can be granted for file access and operations on memory-mapped files.This parameter can be <see langword="null" />.</param>
		/// <param name="inheritability">One of the enumeration values that specifies whether a handle to the memory-mapped file can be inherited by a child process. The default is <see cref="F:System.IO.HandleInheritability.None" />.</param>
		/// <param name="leaveOpen">
		///       <see langword="true" /> to not dispose <paramref name="fileStream" /> after the <see cref="T:System.IO.MemoryMappedFiles.MemoryMappedFile" /> is closed; <see langword="false" /> to dispose <paramref name="fileStream" />.</param>
		/// <returns>A memory-mapped file that has the specified characteristics.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="mapName" /> is an empty string.-or-
		///         <paramref name="capacity" /> and the length of the file are zero.-or-
		///         <paramref name="access" /> is set to the <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.Read" /> or <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.Write" /> enumeration value, which is not allowed.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="fileStream" /> or <paramref name="mapname" />  is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="capacity" /> is less than zero.-or-
		///         <paramref name="capacity" /> is less than the file size.-or-
		///         <paramref name="access" /> is not a valid <see cref="T:System.IO.MemoryMappedFiles.MemoryMappedFileAccess" /> enumeration value.-or-
		///         <paramref name="inheritability" /> is not a valid <see cref="T:System.IO.HandleInheritability" /> enumeration value.</exception>
		/// <exception cref="T:System.ObjectDisposedException">
		///         <paramref name="fileStream" /> was closed.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">
		///         <paramref name="access" /> is set to <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.ReadWrite" /> when <paramref name="fileStream" />'s access is set to <see cref="F:System.IO.FileAccess.Read" /> or <see cref="F:System.IO.FileAccess.Write" />. </exception>
		/// <exception cref="T:System.IO.IOException">
		///         <paramref name="mapName" /> already exists.</exception>
		// Token: 0x06000158 RID: 344 RVA: 0x000040E4 File Offset: 0x000022E4
		[MonoLimitation("memoryMappedFileSecurity is currently ignored")]
		public static MemoryMappedFile CreateFromFile(FileStream fileStream, string mapName, long capacity, MemoryMappedFileAccess access, MemoryMappedFileSecurity memoryMappedFileSecurity, HandleInheritability inheritability, bool leaveOpen)
		{
			if (fileStream == null)
			{
				throw new ArgumentNullException("fileStream");
			}
			if (mapName != null && mapName.Length == 0)
			{
				throw new ArgumentException("mapName");
			}
			if ((!MonoUtil.IsUnix && capacity == 0L && fileStream.Length == 0L) || capacity > fileStream.Length)
			{
				throw new ArgumentException("capacity");
			}
			IntPtr preexistingHandle = MemoryMapImpl.OpenHandle(fileStream.SafeFileHandle.DangerousGetHandle(), mapName, out capacity, access, MemoryMappedFileOptions.None);
			MemoryMapImpl.ConfigureHandleInheritability(preexistingHandle, inheritability);
			return new MemoryMappedFile
			{
				handle = new SafeMemoryMappedFileHandle(preexistingHandle, true),
				stream = fileStream,
				keepOpen = leaveOpen
			};
		}

		// Token: 0x06000159 RID: 345 RVA: 0x0000417C File Offset: 0x0000237C
		private static MemoryMappedFile CoreShmCreate(string mapName, long capacity, MemoryMappedFileAccess access, MemoryMappedFileOptions options, MemoryMappedFileSecurity memoryMappedFileSecurity, HandleInheritability inheritability, FileMode mode)
		{
			if (mapName != null && mapName.Length == 0)
			{
				throw new ArgumentException("mapName");
			}
			if (capacity < 0L)
			{
				throw new ArgumentOutOfRangeException("capacity");
			}
			IntPtr preexistingHandle = MemoryMapImpl.OpenFile(null, mode, mapName, out capacity, access, options);
			return new MemoryMappedFile
			{
				handle = new SafeMemoryMappedFileHandle(preexistingHandle, true)
			};
		}

		/// <summary>Creates a memory-mapped file that has the specified capacity in system memory. </summary>
		/// <param name="mapName">A name to assign to the memory-mapped file.</param>
		/// <param name="capacity">The maximum size, in bytes, to allocate to the memory-mapped file.</param>
		/// <returns>A memory-mapped file that has the specified name and capacity.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="mapName" /> is an empty string. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="mapName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="capacity" /> is less than or equal to zero.</exception>
		// Token: 0x0600015A RID: 346 RVA: 0x000041CF File Offset: 0x000023CF
		[MonoLimitation("Named mappings scope is process local")]
		public static MemoryMappedFile CreateNew(string mapName, long capacity)
		{
			return MemoryMappedFile.CreateNew(mapName, capacity, MemoryMappedFileAccess.ReadWrite, MemoryMappedFileOptions.None, null, HandleInheritability.None);
		}

		/// <summary>Creates a memory-mapped file that has the specified capacity and access type in system memory. </summary>
		/// <param name="mapName">A name to assign to the memory-mapped file.</param>
		/// <param name="capacity">The maximum size, in bytes, to allocate to the memory-mapped file.</param>
		/// <param name="access">One of the enumeration values that specifies the type of access allowed to the memory-mapped file. The default is <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.ReadWrite" />. </param>
		/// <returns>A memory-mapped file that has the specified characteristics.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="mapName" /> is an empty string.-or-
		///         <paramref name="access" /> is set to write-only with the <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.Write" /> enumeration value. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="mapName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="capacity" /> is less than or equal to zero.-or-
		///         <paramref name="access" /> is not a valid <see cref="T:System.IO.MemoryMappedFiles.MemoryMappedFileAccess" /> enumeration value.</exception>
		// Token: 0x0600015B RID: 347 RVA: 0x000041DC File Offset: 0x000023DC
		[MonoLimitation("Named mappings scope is process local")]
		public static MemoryMappedFile CreateNew(string mapName, long capacity, MemoryMappedFileAccess access)
		{
			return MemoryMappedFile.CreateNew(mapName, capacity, access, MemoryMappedFileOptions.None, null, HandleInheritability.None);
		}

		/// <summary>Creates a memory-mapped file that has the specified name, capacity, access type, memory allocation options and inheritability.</summary>
		/// <param name="mapName">A name to assign to the memory-mapped file.</param>
		/// <param name="capacity">The maximum size, in bytes, to allocate to the memory-mapped file.</param>
		/// <param name="access">One of the enumeration values that specifies the type of access allowed to the memory-mapped file. The default is <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.ReadWrite" />.</param>
		/// <param name="options">A bitwise combination of enumeration values that specifies memory allocation options for the memory-mapped file.</param>
		/// <param name="inheritability">A value that specifies whether a handle to the memory-mapped file can be inherited by a child process. The default is <see cref="F:System.IO.HandleInheritability.None" />.</param>
		/// <returns>A memory-mapped file that has the specified characteristics.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="mapName" /> is an empty string.-or-
		///         <paramref name="access" /> is set to write-only with the <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.Write" /> enumeration value. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="mapName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="capacity" /> is less than or equal to zero.-or-
		///         <paramref name="access" /> is not a valid <see cref="T:System.IO.MemoryMappedFiles.MemoryMappedFileAccess" /> enumeration value.-or-
		///         <paramref name="inheritability" /> is not a valid <see cref="T:System.IO.HandleInheritability" /> value.</exception>
		// Token: 0x0600015C RID: 348 RVA: 0x000041E9 File Offset: 0x000023E9
		[MonoLimitation("Named mappings scope is process local; options is ignored")]
		public static MemoryMappedFile CreateNew(string mapName, long capacity, MemoryMappedFileAccess access, MemoryMappedFileOptions options, HandleInheritability inheritability)
		{
			return MemoryMappedFile.CreateNew(mapName, capacity, access, options, null, inheritability);
		}

		/// <summary>Creates a memory-mapped file that has the specified capacity, access type, memory allocation, security permissions, and inheritability in system memory.</summary>
		/// <param name="mapName">A name to assign to the memory-mapped file.</param>
		/// <param name="capacity">The maximum size, in bytes, to allocate to the memory-mapped file.</param>
		/// <param name="access">One of the enumeration values that specifies the type of access allowed to the memory-mapped file. The default is <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.ReadWrite" />. </param>
		/// <param name="options">A bitwise combination of enumeration values that specifies memory allocation options for the memory-mapped file.</param>
		/// <param name="memoryMappedFileSecurity">The permissions that can be granted for file access and operations on memory-mapped files.This parameter can be <see langword="null" />.</param>
		/// <param name="inheritability">One of the enumeration values that specifies whether a handle to the memory-mapped file can be inherited by a child process. The default is <see cref="F:System.IO.HandleInheritability.None" />.</param>
		/// <returns>A memory-mapped file that has the specified characteristics.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="mapName" /> is an empty string.-or-
		///         <paramref name="access" /> is set to write-only with the <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.Write" /> enumeration value.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="mapName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="capacity" /> is less than or equal to zero.-or-
		///         <paramref name="access" /> is not a valid <see cref="T:System.IO.MemoryMappedFiles.MemoryMappedFileAccess" /> enumeration value.-or-
		///         <paramref name="inheritability" /> is not a valid <see cref="T:System.IO.HandleInheritability" /> enumeration value.</exception>
		// Token: 0x0600015D RID: 349 RVA: 0x000041F7 File Offset: 0x000023F7
		[MonoLimitation("Named mappings scope is process local; options and memoryMappedFileSecurity are ignored")]
		public static MemoryMappedFile CreateNew(string mapName, long capacity, MemoryMappedFileAccess access, MemoryMappedFileOptions options, MemoryMappedFileSecurity memoryMappedFileSecurity, HandleInheritability inheritability)
		{
			return MemoryMappedFile.CoreShmCreate(mapName, capacity, access, options, memoryMappedFileSecurity, inheritability, FileMode.CreateNew);
		}

		/// <summary>Creates or opens a memory-mapped file that has the specified capacity in system memory.</summary>
		/// <param name="mapName">A name to assign to the memory-mapped file.</param>
		/// <param name="capacity">The maximum size, in bytes, to allocate to the memory-mapped file.</param>
		/// <returns>A memory-mapped file that has the specified name and size.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="mapName" /> is an empty string.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="mapName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="capacity" /> is greater than the size of the logical address space.-or-
		///         <paramref name="capacity" /> is less than or equal to zero.</exception>
		// Token: 0x0600015E RID: 350 RVA: 0x00004207 File Offset: 0x00002407
		[MonoLimitation("Named mappings scope is process local")]
		public static MemoryMappedFile CreateOrOpen(string mapName, long capacity)
		{
			return MemoryMappedFile.CreateOrOpen(mapName, capacity, MemoryMappedFileAccess.ReadWrite);
		}

		/// <summary>Creates or opens a memory-mapped file that has the specified capacity and access type in system memory. </summary>
		/// <param name="mapName">A name to assign to the memory-mapped file.</param>
		/// <param name="capacity">The maximum size, in bytes, to allocate to the memory-mapped file.</param>
		/// <param name="access">One of the enumeration values that specifies the type of access allowed to the memory-mapped file. The default is <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.ReadWrite" />. </param>
		/// <returns>A memory-mapped file that has the specified characteristics.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="mapName" /> is an empty string.-or-
		///         <paramref name="access" /> is set to write-only with the <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.Write" /> enumeration value.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="mapName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="capacity" /> is greater than the size of the logical address space.-or-
		///         <paramref name="capacity" /> is less than or equal to zero.-or-
		///         <paramref name="access" /> is not a valid <see cref="T:System.IO.MemoryMappedFiles.MemoryMappedFileAccess" /> enumeration value.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The operating system denied the specified access to the file; for example, access is set to <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.Write" /> or <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.ReadWrite" />, but the file or directory is read-only. </exception>
		// Token: 0x0600015F RID: 351 RVA: 0x00004211 File Offset: 0x00002411
		[MonoLimitation("Named mappings scope is process local")]
		public static MemoryMappedFile CreateOrOpen(string mapName, long capacity, MemoryMappedFileAccess access)
		{
			return MemoryMappedFile.CreateOrOpen(mapName, capacity, access, MemoryMappedFileOptions.None, null, HandleInheritability.None);
		}

		/// <summary>Creates a new empty memory mapped file or opens an existing memory mapped file if one exists with the same name. If opening an existing file, the capacity, options, and memory arguments will be ignored. </summary>
		/// <param name="mapName">A name to assign to the memory-mapped file.</param>
		/// <param name="capacity">The maximum size, in bytes, to allocate to the memory-mapped file.</param>
		/// <param name="access">One of the enumeration values that specifies the type of access allowed to the memory-mapped file. The default is <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.ReadWrite" />. </param>
		/// <param name="options">A bitwise combination of values that indicate the memory allocation options to apply to the file.</param>
		/// <param name="inheritability">A value that specifies whether a handle to the memory-mapped file can be inherited by a child process. The default is <see cref="F:System.IO.HandleInheritability.None" />.</param>
		/// <returns>A memory-mapped file that has the specified characteristics.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="mapName" /> is an empty string.-or-
		///         <paramref name="access" /> is set to write-only with the <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.Write" /> enumeration value.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="mapName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="capacity" /> is greater than the size of the logical address space.-or-
		///         <paramref name="capacity" /> is less than or equal to zero.-or-
		///         <paramref name="access" /> is not a valid <see cref="T:System.IO.MemoryMappedFiles.MemoryMappedFileAccess" /> enumeration value.-or-
		///         <paramref name="inheritability" /> is not a valid <see cref="T:System.IO.HandleInheritability" /> enumeration value.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The operating system denied the specified access to the file; for example, access is set to <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.Write" /> or <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.ReadWrite" />, but the file or directory is read-only. </exception>
		// Token: 0x06000160 RID: 352 RVA: 0x0000421E File Offset: 0x0000241E
		[MonoLimitation("Named mappings scope is process local")]
		public static MemoryMappedFile CreateOrOpen(string mapName, long capacity, MemoryMappedFileAccess access, MemoryMappedFileOptions options, HandleInheritability inheritability)
		{
			return MemoryMappedFile.CreateOrOpen(mapName, capacity, access, options, null, inheritability);
		}

		/// <summary>Creates or opens a memory-mapped file that has the specified capacity, access type, memory allocation, security permissions, and inheritability in system memory.</summary>
		/// <param name="mapName">A name to assign to the memory-mapped file.</param>
		/// <param name="capacity">The maximum size, in bytes, to allocate to the memory-mapped file.</param>
		/// <param name="access">One of the enumeration values that specifies the type of access allowed to the memory-mapped file. The default is <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.ReadWrite" />. </param>
		/// <param name="options">A bitwise combination of enumeration values that specifies memory allocation options for the memory-mapped file.</param>
		/// <param name="memoryMappedFileSecurity">The permissions that can be granted for file access and operations on memory-mapped files.This parameter can be <see langword="null" />.</param>
		/// <param name="inheritability">One of the enumeration values that specifies whether a handle to the memory-mapped file can be inherited by a child process. The default is <see cref="F:System.IO.HandleInheritability.None" />.</param>
		/// <returns>A memory-mapped file that has the specified characteristics.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="mapName" /> is an empty string. -or-
		///         <paramref name="access" /> is set to write-only with the <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.Write" /> enumeration value.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="mapName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="capacity" /> is greater than the size of the logical address space.-or-
		///         <paramref name="capacity" /> is less than or equal to zero.-or-
		///         <paramref name="access" /> is not a valid <see cref="T:System.IO.MemoryMappedFiles.MemoryMappedFileAccess" /> enumeration value.-or-
		///         <paramref name="inheritability" /> is not a valid <see cref="T:System.IO.HandleInheritability" /> enumeration value.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The operating system denied the specified <paramref name="access" /> to the file; for example, <paramref name="access" /> is set to <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.Write" /> or <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.ReadWrite" />, but the file or directory is read-only. </exception>
		// Token: 0x06000161 RID: 353 RVA: 0x0000422C File Offset: 0x0000242C
		[MonoLimitation("Named mappings scope is process local")]
		public static MemoryMappedFile CreateOrOpen(string mapName, long capacity, MemoryMappedFileAccess access, MemoryMappedFileOptions options, MemoryMappedFileSecurity memoryMappedFileSecurity, HandleInheritability inheritability)
		{
			return MemoryMappedFile.CoreShmCreate(mapName, capacity, access, options, memoryMappedFileSecurity, inheritability, FileMode.OpenOrCreate);
		}

		/// <summary>Opens an existing memory-mapped file that has the specified name in system memory.</summary>
		/// <param name="mapName">The name of the memory-mapped file to open.</param>
		/// <returns>A memory-mapped file that has the specified name. </returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="mapName" /> is an empty string.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="mapName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The file specified for <paramref name="mapName" /> does not exist.</exception>
		// Token: 0x06000162 RID: 354 RVA: 0x0000423C File Offset: 0x0000243C
		[MonoLimitation("Named mappings scope is process local")]
		public static MemoryMappedFile OpenExisting(string mapName)
		{
			return MemoryMappedFile.OpenExisting(mapName, MemoryMappedFileRights.ReadWrite);
		}

		/// <summary>Opens an existing memory-mapped file that has the specified name and access rights in system memory.</summary>
		/// <param name="mapName">The name of the memory-mapped file to open.</param>
		/// <param name="desiredAccessRights">One of the enumeration values that specifies the access rights to apply to the memory-mapped file.</param>
		/// <returns>A memory-mapped file that has the specified characteristics.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="mapName" /> is an empty string.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="mapName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="desiredAccessRights" /> is not a valid <see cref="T:System.IO.MemoryMappedFiles.MemoryMappedFileRights" /> enumeration value.</exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The file specified for <paramref name="mapName" /> does not exist.</exception>
		// Token: 0x06000163 RID: 355 RVA: 0x00004245 File Offset: 0x00002445
		[MonoLimitation("Named mappings scope is process local")]
		public static MemoryMappedFile OpenExisting(string mapName, MemoryMappedFileRights desiredAccessRights)
		{
			return MemoryMappedFile.OpenExisting(mapName, desiredAccessRights, HandleInheritability.None);
		}

		/// <summary>Opens an existing memory-mapped file that has the specified name, access rights, and inheritability in system memory.</summary>
		/// <param name="mapName">The name of the memory-mapped file to open.</param>
		/// <param name="desiredAccessRights">One of the enumeration values that specifies the access rights to apply to the memory-mapped file.</param>
		/// <param name="inheritability">One of the enumeration values that specifies whether a handle to the memory-mapped file can be inherited by a child process. The default is <see cref="F:System.IO.HandleInheritability.None" />.</param>
		/// <returns>A memory-mapped file that has the specified characteristics.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="mapName" /> is an empty string.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="mapName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="desiredAccessRights" /> is not a valid <see cref="T:System.IO.MemoryMappedFiles.MemoryMappedFileRights" /> enumeration value.-or-
		///         <paramref name="inheritability" /> is not a valid <see cref="T:System.IO.HandleInheritability" /> enumeration value.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The requested access is invalid for the memory-mapped file.</exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The file specified for <paramref name="mapName" /> does not exist.</exception>
		// Token: 0x06000164 RID: 356 RVA: 0x0000424F File Offset: 0x0000244F
		[MonoLimitation("Named mappings scope is process local")]
		public static MemoryMappedFile OpenExisting(string mapName, MemoryMappedFileRights desiredAccessRights, HandleInheritability inheritability)
		{
			return MemoryMappedFile.CoreShmCreate(mapName, 0L, MemoryMappedFileAccess.ReadWrite, MemoryMappedFileOptions.None, null, inheritability, FileMode.Open);
		}

		/// <summary>Creates a stream that maps to a view of the memory-mapped file.  </summary>
		/// <returns>A stream of memory.</returns>
		/// <exception cref="T:System.UnauthorizedAccessException">Access to the memory-mapped file is unauthorized.</exception>
		// Token: 0x06000165 RID: 357 RVA: 0x0000425E File Offset: 0x0000245E
		public MemoryMappedViewStream CreateViewStream()
		{
			return this.CreateViewStream(0L, 0L);
		}

		/// <summary>Creates a stream that maps to a view of the memory-mapped file, and that has the specified offset and size.</summary>
		/// <param name="offset">The byte at which to start the view.</param>
		/// <param name="size">The size of the view. Specify 0 (zero) to create a view that starts at <paramref name="offset" /> and ends approximately at the end of the memory-mapped file.</param>
		/// <returns>A stream of memory that has the specified offset and size.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> or <paramref name="size" /> is a negative value.-or-
		///         <paramref name="size" /> is greater than the logical address space.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">Access to the memory-mapped file is unauthorized.</exception>
		/// <exception cref="T:System.IO.IOException">
		///         <paramref name="size" /> is greater than the total virtual memory.</exception>
		// Token: 0x06000166 RID: 358 RVA: 0x0000426A File Offset: 0x0000246A
		public MemoryMappedViewStream CreateViewStream(long offset, long size)
		{
			return this.CreateViewStream(offset, size, MemoryMappedFileAccess.ReadWrite);
		}

		/// <summary>Creates a stream that maps to a view of the memory-mapped file, and that has the specified offset, size, and access type.</summary>
		/// <param name="offset">The byte at which to start the view.</param>
		/// <param name="size">The size of the view. Specify 0 (zero) to create a view that starts at <paramref name="offset" /> and ends approximately at the end of the memory-mapped file.</param>
		/// <param name="access">One of the enumeration values that specifies the type of access allowed to the memory-mapped file. The default is <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.ReadWrite" />.</param>
		/// <returns>A stream of memory that has the specified characteristics.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> or <paramref name="size" /> is a negative value.-or-
		///         <paramref name="size" /> is greater than the logical address space.-or-
		///         <paramref name="access " />is not a valid <see cref="T:System.IO.MemoryMappedFiles.MemoryMappedFileAccess" /> enumeration value.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">
		///         <paramref name="access" /> is invalid for the memory-mapped file.</exception>
		/// <exception cref="T:System.IO.IOException">
		///         <paramref name="size" /> is greater than the total virtual memory.-or-
		///         <paramref name="access" /> is invalid for the memory-mapped file.</exception>
		// Token: 0x06000167 RID: 359 RVA: 0x00004275 File Offset: 0x00002475
		public MemoryMappedViewStream CreateViewStream(long offset, long size, MemoryMappedFileAccess access)
		{
			return new MemoryMappedViewStream(MemoryMappedView.Create(this.handle.DangerousGetHandle(), offset, size, access));
		}

		/// <summary>Creates a <see cref="T:System.IO.MemoryMappedFiles.MemoryMappedViewAccessor" /> that maps to a view of the memory-mapped file.</summary>
		/// <returns>A randomly accessible block of memory.</returns>
		/// <exception cref="T:System.UnauthorizedAccessException">Access to the memory-mapped file is unauthorized.</exception>
		// Token: 0x06000168 RID: 360 RVA: 0x0000428F File Offset: 0x0000248F
		public MemoryMappedViewAccessor CreateViewAccessor()
		{
			return this.CreateViewAccessor(0L, 0L);
		}

		/// <summary>Creates a <see cref="T:System.IO.MemoryMappedFiles.MemoryMappedViewAccessor" /> that maps to a view of the memory-mapped file, and that has the specified offset and size.</summary>
		/// <param name="offset">The byte at which to start the view.</param>
		/// <param name="size">The size of the view. Specify 0 (zero) to create a view that starts at <paramref name="offset" /> and ends approximately at the end of the memory-mapped file.</param>
		/// <returns>A randomly accessible block of memory.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> or <paramref name="size" /> is a negative value.-or-
		///         <paramref name="size" /> is greater than the logical address space.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">Access to the memory-mapped file is unauthorized.</exception>
		/// <exception cref="T:System.IO.IOException">An I/O error occurred.</exception>
		// Token: 0x06000169 RID: 361 RVA: 0x0000429B File Offset: 0x0000249B
		public MemoryMappedViewAccessor CreateViewAccessor(long offset, long size)
		{
			return this.CreateViewAccessor(offset, size, MemoryMappedFileAccess.ReadWrite);
		}

		/// <summary>Creates a <see cref="T:System.IO.MemoryMappedFiles.MemoryMappedViewAccessor" /> that maps to a view of the memory-mapped file, and that has the specified offset, size, and access restrictions.</summary>
		/// <param name="offset">The byte at which to start the view.</param>
		/// <param name="size">The size of the view. Specify 0 (zero) to create a view that starts at <paramref name="offset" /> and ends approximately at the end of the memory-mapped file.</param>
		/// <param name="access">One of the enumeration values that specifies the type of access allowed to the memory-mapped file. The default is <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.ReadWrite" />.</param>
		/// <returns>A randomly accessible block of memory.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> or <paramref name="size" /> is a negative value.-or-
		///         <paramref name="size" /> is greater than the logical address space.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">
		///         <paramref name="access" /> is invalid for the memory-mapped file.</exception>
		/// <exception cref="T:System.IO.IOException">An I/O error occurred.</exception>
		// Token: 0x0600016A RID: 362 RVA: 0x000042A6 File Offset: 0x000024A6
		public MemoryMappedViewAccessor CreateViewAccessor(long offset, long size, MemoryMappedFileAccess access)
		{
			return new MemoryMappedViewAccessor(MemoryMappedView.Create(this.handle.DangerousGetHandle(), offset, size, access));
		}

		// Token: 0x0600016B RID: 363 RVA: 0x00002310 File Offset: 0x00000510
		private MemoryMappedFile()
		{
		}

		/// <summary>Releases all resources used by the <see cref="T:System.IO.MemoryMappedFiles.MemoryMappedFile" />. </summary>
		// Token: 0x0600016C RID: 364 RVA: 0x000042C0 File Offset: 0x000024C0
		public void Dispose()
		{
			this.Dispose(true);
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.IO.MemoryMappedFiles.MemoryMappedFile" /> and optionally releases the managed resources. </summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x0600016D RID: 365 RVA: 0x000042CC File Offset: 0x000024CC
		protected virtual void Dispose(bool disposing)
		{
			if (disposing && this.stream != null)
			{
				if (!this.keepOpen)
				{
					this.stream.Close();
				}
				this.stream = null;
			}
			if (this.handle != null)
			{
				this.handle.Dispose();
				this.handle = null;
			}
		}

		/// <summary>Gets the access control to the memory-mapped file resource.</summary>
		/// <returns>The permissions that can be granted for file access and operations on memory-mapped files.</returns>
		/// <exception cref="T:System.InvalidOperationException">An underlying call to set security information failed.</exception>
		/// <exception cref="T:System.NotSupportedException">An underlying call to set security information failed.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The memory-mapped file is closed.</exception>
		/// <exception cref="T:System.PlatformNotSupportedException">The current platform is Windows 98 or earlier.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">An underlying call to set security information failed.-or-The memory-mapped file was opened as <see cref="F:System.IO.MemoryMappedFiles.MemoryMappedFileAccess.Write" /> only.</exception>
		// Token: 0x0600016E RID: 366 RVA: 0x0000227E File Offset: 0x0000047E
		[MonoTODO]
		public MemoryMappedFileSecurity GetAccessControl()
		{
			throw new NotImplementedException();
		}

		/// <summary>Sets the access control to the memory-mapped file resource.</summary>
		/// <param name="memoryMappedFileSecurity">The permissions that can be granted for file access and operations on memory-mapped files.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="memoryMappedFileSecurity" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">An underlying call to set security information failed.</exception>
		/// <exception cref="T:System.NotSupportedException">An underlying call to set security information failed.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">An underlying call to set security information failed.</exception>
		// Token: 0x0600016F RID: 367 RVA: 0x0000227E File Offset: 0x0000047E
		[MonoTODO]
		public void SetAccessControl(MemoryMappedFileSecurity memoryMappedFileSecurity)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets the file handle of a memory-mapped file.</summary>
		/// <returns>The handle to the memory-mapped file.</returns>
		// Token: 0x17000034 RID: 52
		// (get) Token: 0x06000170 RID: 368 RVA: 0x00004318 File Offset: 0x00002518
		public SafeMemoryMappedFileHandle SafeMemoryMappedFileHandle
		{
			get
			{
				return this.handle;
			}
		}

		// Token: 0x06000171 RID: 369 RVA: 0x00004320 File Offset: 0x00002520
		internal static FileAccess GetFileAccess(MemoryMappedFileAccess access)
		{
			if (access == MemoryMappedFileAccess.Read)
			{
				return FileAccess.Read;
			}
			if (access == MemoryMappedFileAccess.Write)
			{
				return FileAccess.Write;
			}
			if (access == MemoryMappedFileAccess.ReadWrite)
			{
				return FileAccess.ReadWrite;
			}
			if (access == MemoryMappedFileAccess.CopyOnWrite)
			{
				return FileAccess.ReadWrite;
			}
			if (access == MemoryMappedFileAccess.ReadExecute)
			{
				return FileAccess.Read;
			}
			if (access == MemoryMappedFileAccess.ReadWriteExecute)
			{
				return FileAccess.ReadWrite;
			}
			throw new ArgumentOutOfRangeException("access");
		}

		// Token: 0x0400023F RID: 575
		private FileStream stream;

		// Token: 0x04000240 RID: 576
		private bool keepOpen;

		// Token: 0x04000241 RID: 577
		private SafeMemoryMappedFileHandle handle;
	}
}
