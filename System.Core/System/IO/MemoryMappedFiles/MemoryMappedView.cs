﻿using System;
using System.Security;
using Microsoft.Win32.SafeHandles;

namespace System.IO.MemoryMappedFiles
{
	// Token: 0x02000043 RID: 67
	internal class MemoryMappedView : IDisposable
	{
		// Token: 0x06000172 RID: 370 RVA: 0x0000434F File Offset: 0x0000254F
		[SecurityCritical]
		private MemoryMappedView(SafeMemoryMappedViewHandle viewHandle, long pointerOffset, long size, MemoryMappedFileAccess access)
		{
			this.m_viewHandle = viewHandle;
			this.m_pointerOffset = pointerOffset;
			this.m_size = size;
			this.m_access = access;
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x06000173 RID: 371 RVA: 0x00004374 File Offset: 0x00002574
		internal SafeMemoryMappedViewHandle ViewHandle
		{
			[SecurityCritical]
			get
			{
				return this.m_viewHandle;
			}
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x06000174 RID: 372 RVA: 0x0000437C File Offset: 0x0000257C
		internal long PointerOffset
		{
			get
			{
				return this.m_pointerOffset;
			}
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x06000175 RID: 373 RVA: 0x00004384 File Offset: 0x00002584
		internal long Size
		{
			get
			{
				return this.m_size;
			}
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x06000176 RID: 374 RVA: 0x0000438C File Offset: 0x0000258C
		internal MemoryMappedFileAccess Access
		{
			get
			{
				return this.m_access;
			}
		}

		// Token: 0x06000177 RID: 375 RVA: 0x00004394 File Offset: 0x00002594
		internal static MemoryMappedView Create(IntPtr handle, long offset, long size, MemoryMappedFileAccess access)
		{
			IntPtr mmap_handle;
			IntPtr base_address;
			MemoryMapImpl.Map(handle, offset, ref size, access, out mmap_handle, out base_address);
			return new MemoryMappedView(new SafeMemoryMappedViewHandle(mmap_handle, base_address, size), 0L, size, access);
		}

		// Token: 0x06000178 RID: 376 RVA: 0x000043C0 File Offset: 0x000025C0
		public void Flush(IntPtr capacity)
		{
			this.m_viewHandle.Flush();
		}

		// Token: 0x06000179 RID: 377 RVA: 0x000043CD File Offset: 0x000025CD
		protected virtual void Dispose(bool disposing)
		{
			if (this.m_viewHandle != null && !this.m_viewHandle.IsClosed)
			{
				this.m_viewHandle.Dispose();
			}
		}

		// Token: 0x0600017A RID: 378 RVA: 0x000043EF File Offset: 0x000025EF
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x0600017B RID: 379 RVA: 0x000043FE File Offset: 0x000025FE
		internal bool IsClosed
		{
			get
			{
				return this.m_viewHandle == null || this.m_viewHandle.IsClosed;
			}
		}

		// Token: 0x04000242 RID: 578
		private SafeMemoryMappedViewHandle m_viewHandle;

		// Token: 0x04000243 RID: 579
		private long m_pointerOffset;

		// Token: 0x04000244 RID: 580
		private long m_size;

		// Token: 0x04000245 RID: 581
		private MemoryMappedFileAccess m_access;
	}
}
