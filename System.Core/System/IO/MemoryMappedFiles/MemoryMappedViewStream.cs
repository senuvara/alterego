﻿using System;
using System.Security;
using System.Security.Permissions;
using Microsoft.Win32.SafeHandles;
using Unity;

namespace System.IO.MemoryMappedFiles
{
	/// <summary>Represents a view of a memory-mapped file as a sequentially accessed stream.</summary>
	// Token: 0x02000040 RID: 64
	public sealed class MemoryMappedViewStream : UnmanagedMemoryStream
	{
		// Token: 0x06000140 RID: 320 RVA: 0x00003CB4 File Offset: 0x00001EB4
		[SecurityCritical]
		internal MemoryMappedViewStream(MemoryMappedView view)
		{
			this.m_view = view;
			base.Initialize(this.m_view.ViewHandle, this.m_view.PointerOffset, this.m_view.Size, MemoryMappedFile.GetFileAccess(this.m_view.Access));
		}

		/// <summary>Gets a handle to the view of a memory-mapped file.</summary>
		/// <returns>A wrapper for the operating system's handle to the view of the file. </returns>
		// Token: 0x17000032 RID: 50
		// (get) Token: 0x06000141 RID: 321 RVA: 0x00003D05 File Offset: 0x00001F05
		public SafeMemoryMappedViewHandle SafeMemoryMappedViewHandle
		{
			[SecurityCritical]
			[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
			get
			{
				if (this.m_view == null)
				{
					return null;
				}
				return this.m_view.ViewHandle;
			}
		}

		/// <summary>Sets the length of the current stream.</summary>
		/// <param name="value">The desired length of the current stream in bytes.</param>
		/// <exception cref="T:System.NotSupportedException">This method is not supported.</exception>
		// Token: 0x06000142 RID: 322 RVA: 0x00003D1C File Offset: 0x00001F1C
		public override void SetLength(long value)
		{
			throw new NotSupportedException(SR.GetString("MemoryMappedViewStreams are fixed length."));
		}

		/// <summary>[Supported in the .NET Framework 4.5.1 and later versions] Gets the number of bytes by which the starting position of this view is offset from the beginning of the memory-mapped file.</summary>
		/// <returns>The number of bytes between the starting position of this view and the beginning of the memory-mapped file. </returns>
		/// <exception cref="T:System.InvalidOperationException">The object from which this instance was created is <see langword="null" />. </exception>
		// Token: 0x17000033 RID: 51
		// (get) Token: 0x06000143 RID: 323 RVA: 0x00003D2D File Offset: 0x00001F2D
		public long PointerOffset
		{
			get
			{
				if (this.m_view == null)
				{
					throw new InvalidOperationException(SR.GetString("The underlying MemoryMappedView object is null."));
				}
				return this.m_view.PointerOffset;
			}
		}

		// Token: 0x06000144 RID: 324 RVA: 0x00003D54 File Offset: 0x00001F54
		[SecuritySafeCritical]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.m_view != null && !this.m_view.IsClosed)
				{
					this.Flush();
				}
			}
			finally
			{
				try
				{
					if (this.m_view != null)
					{
						this.m_view.Dispose();
					}
				}
				finally
				{
					base.Dispose(disposing);
				}
			}
		}

		/// <summary>Clears all buffers for this stream and causes any buffered data to be written to the underlying file.</summary>
		// Token: 0x06000145 RID: 325 RVA: 0x00003DBC File Offset: 0x00001FBC
		[SecurityCritical]
		public override void Flush()
		{
			if (!this.CanSeek)
			{
				__Error.StreamIsClosed();
			}
			if (this.m_view != null)
			{
				this.m_view.Flush((IntPtr)base.Capacity);
			}
		}

		// Token: 0x06000146 RID: 326 RVA: 0x0000220F File Offset: 0x0000040F
		internal MemoryMappedViewStream()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0400023E RID: 574
		private MemoryMappedView m_view;
	}
}
