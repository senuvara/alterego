﻿using System;

namespace System.IO.MemoryMappedFiles
{
	/// <summary>Specifies access capabilities and restrictions for a memory-mapped file or view. </summary>
	// Token: 0x0200003B RID: 59
	[Serializable]
	public enum MemoryMappedFileAccess
	{
		/// <summary>Read and write access to the file.</summary>
		// Token: 0x04000226 RID: 550
		ReadWrite,
		/// <summary>Read-only access to the file.</summary>
		// Token: 0x04000227 RID: 551
		Read,
		/// <summary>Write-only access to file.</summary>
		// Token: 0x04000228 RID: 552
		Write,
		/// <summary>Read and write access to the file, with the restriction that any write operations will not be seen by other processes. </summary>
		// Token: 0x04000229 RID: 553
		CopyOnWrite,
		/// <summary>Read access to the file that can store and run executable code.</summary>
		// Token: 0x0400022A RID: 554
		ReadExecute,
		/// <summary>Read and write access to the file that can can store and run executable code.</summary>
		// Token: 0x0400022B RID: 555
		ReadWriteExecute
	}
}
