﻿using System;

namespace System.IO.Pipes
{
	/// <summary>Represents the method to call as the client.</summary>
	// Token: 0x02000039 RID: 57
	// (Invoke) Token: 0x06000134 RID: 308
	public delegate void PipeStreamImpersonationWorker();
}
