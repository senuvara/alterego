﻿using System;
using Microsoft.Win32.SafeHandles;

namespace System.IO.Pipes
{
	// Token: 0x02000033 RID: 51
	internal interface IAnonymousPipeServer : IPipe
	{
		// Token: 0x1700001B RID: 27
		// (get) Token: 0x060000EA RID: 234
		SafePipeHandle ClientHandle { get; }

		// Token: 0x060000EB RID: 235
		void DisposeLocalCopyOfClientHandle();
	}
}
