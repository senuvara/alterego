﻿using System;
using Microsoft.Win32.SafeHandles;

namespace System.IO.Pipes
{
	// Token: 0x02000031 RID: 49
	internal interface IPipe
	{
		// Token: 0x1700001A RID: 26
		// (get) Token: 0x060000E8 RID: 232
		SafePipeHandle Handle { get; }

		// Token: 0x060000E9 RID: 233
		void WaitForPipeDrain();
	}
}
