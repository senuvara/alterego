﻿using System;

namespace System.IO.Pipes
{
	// Token: 0x02000034 RID: 52
	internal interface INamedPipeClient : IPipe
	{
		// Token: 0x060000EC RID: 236
		void Connect();

		// Token: 0x060000ED RID: 237
		void Connect(int timeout);

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x060000EE RID: 238
		int NumberOfServerInstances { get; }

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x060000EF RID: 239
		bool IsAsync { get; }
	}
}
