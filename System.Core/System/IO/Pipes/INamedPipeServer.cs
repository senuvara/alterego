﻿using System;

namespace System.IO.Pipes
{
	// Token: 0x02000035 RID: 53
	internal interface INamedPipeServer : IPipe
	{
		// Token: 0x060000F0 RID: 240
		void Disconnect();

		// Token: 0x060000F1 RID: 241
		void WaitForConnection();
	}
}
