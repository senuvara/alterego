﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.Win32.SafeHandles;

namespace System.IO.Pipes
{
	/// <summary>Exposes a <see cref="T:System.IO.Stream" /> object around a pipe, which supports both anonymous and named pipes.</summary>
	// Token: 0x02000038 RID: 56
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	public abstract class PipeStream : Stream
	{
		// Token: 0x06000105 RID: 261 RVA: 0x0000383C File Offset: 0x00001A3C
		internal Exception ThrowACLException()
		{
			return new NotImplementedException("ACL is not supported in Mono");
		}

		// Token: 0x06000106 RID: 262 RVA: 0x00003848 File Offset: 0x00001A48
		internal static PipeAccessRights ToAccessRights(PipeDirection direction)
		{
			switch (direction)
			{
			case PipeDirection.In:
				return PipeAccessRights.ReadData;
			case PipeDirection.Out:
				return PipeAccessRights.WriteData;
			case PipeDirection.InOut:
				return PipeAccessRights.ReadData | PipeAccessRights.WriteData;
			default:
				throw new ArgumentOutOfRangeException();
			}
		}

		// Token: 0x06000107 RID: 263 RVA: 0x0000386C File Offset: 0x00001A6C
		internal static PipeDirection ToDirection(PipeAccessRights rights)
		{
			bool flag = (rights & PipeAccessRights.ReadData) > (PipeAccessRights)0;
			bool flag2 = (rights & PipeAccessRights.WriteData) > (PipeAccessRights)0;
			if (flag)
			{
				if (flag2)
				{
					return PipeDirection.InOut;
				}
				return PipeDirection.In;
			}
			else
			{
				if (flag2)
				{
					return PipeDirection.Out;
				}
				throw new ArgumentOutOfRangeException();
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.Pipes.PipeStream" /> class using the specified <see cref="T:System.IO.Pipes.PipeDirection" /> value and buffer size.</summary>
		/// <param name="direction">One of the <see cref="T:System.IO.Pipes.PipeDirection" /> values that indicates the direction of the pipe object.</param>
		/// <param name="bufferSize">A positive <see cref="T:System.Int32" /> value greater than or equal to 0 that indicates the buffer size.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="direction" /> is not a valid <see cref="T:System.IO.Pipes.PipeDirection" /> value.-or-
		///         <paramref name="bufferSize" /> is less than 0.</exception>
		// Token: 0x06000108 RID: 264 RVA: 0x00003899 File Offset: 0x00001A99
		protected PipeStream(PipeDirection direction, int bufferSize) : this(direction, PipeTransmissionMode.Byte, bufferSize)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.Pipes.PipeStream" /> class using the specified <see cref="T:System.IO.Pipes.PipeDirection" />, <see cref="T:System.IO.Pipes.PipeTransmissionMode" />, and buffer size.</summary>
		/// <param name="direction">One of the <see cref="T:System.IO.Pipes.PipeDirection" /> values that indicates the direction of the pipe object.</param>
		/// <param name="transmissionMode">One of the <see cref="T:System.IO.Pipes.PipeTransmissionMode" /> values that indicates the transmission mode of the pipe object.</param>
		/// <param name="outBufferSize">A positive <see cref="T:System.Int32" /> value greater than or equal to 0 that indicates the buffer size.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="direction" /> is not a valid <see cref="T:System.IO.Pipes.PipeDirection" /> value.-or-
		///         <paramref name="transmissionMode" /> is not a valid <see cref="T:System.IO.Pipes.PipeTransmissionMode" /> value.-or-
		///         <paramref name="bufferSize" /> is less than 0.</exception>
		// Token: 0x06000109 RID: 265 RVA: 0x000038A4 File Offset: 0x00001AA4
		protected PipeStream(PipeDirection direction, PipeTransmissionMode transmissionMode, int outBufferSize)
		{
			this.direction = direction;
			this.transmission_mode = transmissionMode;
			this.read_trans_mode = transmissionMode;
			if (outBufferSize <= 0)
			{
				throw new ArgumentOutOfRangeException("bufferSize must be greater than 0");
			}
			this.buffer_size = outBufferSize;
		}

		/// <summary>Gets a value indicating whether the current stream supports read operations.</summary>
		/// <returns>
		///     <see langword="true" /> if the stream supports read operations; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000021 RID: 33
		// (get) Token: 0x0600010A RID: 266 RVA: 0x000038D7 File Offset: 0x00001AD7
		public override bool CanRead
		{
			get
			{
				return (this.direction & PipeDirection.In) > (PipeDirection)0;
			}
		}

		/// <summary>Gets a value indicating whether the current stream supports seek operations.</summary>
		/// <returns>
		///     <see langword="false" /> in all cases.</returns>
		// Token: 0x17000022 RID: 34
		// (get) Token: 0x0600010B RID: 267 RVA: 0x00002285 File Offset: 0x00000485
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets a value indicating whether the current stream supports write operations.</summary>
		/// <returns>
		///     <see langword="true" /> if the stream supports write operations; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000023 RID: 35
		// (get) Token: 0x0600010C RID: 268 RVA: 0x000038E4 File Offset: 0x00001AE4
		public override bool CanWrite
		{
			get
			{
				return (this.direction & PipeDirection.Out) > (PipeDirection)0;
			}
		}

		/// <summary>Gets the size, in bytes, of the inbound buffer for a pipe.</summary>
		/// <returns>An integer value that represents the inbound buffer size, in bytes.</returns>
		/// <exception cref="T:System.NotSupportedException">The stream is unreadable.</exception>
		/// <exception cref="T:System.InvalidOperationException">The pipe is waiting to connect.</exception>
		/// <exception cref="T:System.IO.IOException">The pipe is broken or another I/O error occurred.</exception>
		// Token: 0x17000024 RID: 36
		// (get) Token: 0x0600010D RID: 269 RVA: 0x000038F1 File Offset: 0x00001AF1
		public virtual int InBufferSize
		{
			get
			{
				return this.buffer_size;
			}
		}

		/// <summary>Gets a value indicating whether a <see cref="T:System.IO.Pipes.PipeStream" /> object was opened asynchronously or synchronously.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.IO.Pipes.PipeStream" /> object was opened asynchronously; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000025 RID: 37
		// (get) Token: 0x0600010E RID: 270 RVA: 0x000038F9 File Offset: 0x00001AF9
		// (set) Token: 0x0600010F RID: 271 RVA: 0x00003901 File Offset: 0x00001B01
		public bool IsAsync
		{
			[CompilerGenerated]
			get
			{
				return this.<IsAsync>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<IsAsync>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether a <see cref="T:System.IO.Pipes.PipeStream" /> object is connected.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.IO.Pipes.PipeStream" /> object is connected; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000110 RID: 272 RVA: 0x0000390A File Offset: 0x00001B0A
		// (set) Token: 0x06000111 RID: 273 RVA: 0x00003912 File Offset: 0x00001B12
		public bool IsConnected
		{
			[CompilerGenerated]
			get
			{
				return this.<IsConnected>k__BackingField;
			}
			[CompilerGenerated]
			protected set
			{
				this.<IsConnected>k__BackingField = value;
			}
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000112 RID: 274 RVA: 0x0000391C File Offset: 0x00001B1C
		// (set) Token: 0x06000113 RID: 275 RVA: 0x00003984 File Offset: 0x00001B84
		internal Stream Stream
		{
			get
			{
				if (!this.IsConnected)
				{
					throw new InvalidOperationException("Pipe is not connected");
				}
				if (this.stream == null)
				{
					this.stream = new FileStream(this.handle.DangerousGetHandle(), this.CanRead ? (this.CanWrite ? FileAccess.ReadWrite : FileAccess.Read) : FileAccess.Write, false, this.buffer_size, this.IsAsync);
				}
				return this.stream;
			}
			set
			{
				this.stream = value;
			}
		}

		/// <summary>Gets a value indicating whether a handle to a <see cref="T:System.IO.Pipes.PipeStream" /> object is exposed.</summary>
		/// <returns>
		///     <see langword="true" /> if a handle to the <see cref="T:System.IO.Pipes.PipeStream" /> object is exposed; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000028 RID: 40
		// (get) Token: 0x06000114 RID: 276 RVA: 0x0000398D File Offset: 0x00001B8D
		// (set) Token: 0x06000115 RID: 277 RVA: 0x00003995 File Offset: 0x00001B95
		private protected bool IsHandleExposed
		{
			[CompilerGenerated]
			protected get
			{
				return this.<IsHandleExposed>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<IsHandleExposed>k__BackingField = value;
			}
		}

		/// <summary>Gets a value indicating whether there is more data in the message returned from the most recent read operation.</summary>
		/// <returns>
		///     <see langword="true" /> if there are no more characters to read in the message; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">The pipe is not connected.-or-The pipe handle has not been set.-or-The pipe's <see cref="P:System.IO.Pipes.PipeStream.ReadMode" /> property value is not <see cref="F:System.IO.Pipes.PipeTransmissionMode.Message" />.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The pipe is closed.</exception>
		// Token: 0x17000029 RID: 41
		// (get) Token: 0x06000116 RID: 278 RVA: 0x0000399E File Offset: 0x00001B9E
		// (set) Token: 0x06000117 RID: 279 RVA: 0x000039A6 File Offset: 0x00001BA6
		[MonoTODO]
		public bool IsMessageComplete
		{
			[CompilerGenerated]
			get
			{
				return this.<IsMessageComplete>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<IsMessageComplete>k__BackingField = value;
			}
		}

		/// <summary>Gets the size, in bytes, of the outbound buffer for a pipe.</summary>
		/// <returns>The outbound buffer size, in bytes.</returns>
		/// <exception cref="T:System.NotSupportedException">The stream is unwriteable.</exception>
		/// <exception cref="T:System.InvalidOperationException">The pipe is waiting to connect.</exception>
		/// <exception cref="T:System.IO.IOException">The pipe is broken or another I/O error occurred.</exception>
		// Token: 0x1700002A RID: 42
		// (get) Token: 0x06000118 RID: 280 RVA: 0x000038F1 File Offset: 0x00001AF1
		[MonoTODO]
		public virtual int OutBufferSize
		{
			get
			{
				return this.buffer_size;
			}
		}

		/// <summary>Gets or sets the reading mode for a <see cref="T:System.IO.Pipes.PipeStream" /> object.</summary>
		/// <returns>One of the <see cref="T:System.IO.Pipes.PipeTransmissionMode" /> values that indicates how the <see cref="T:System.IO.Pipes.PipeStream" /> object reads from the pipe.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The supplied value is not a valid <see cref="T:System.IO.Pipes.PipeTransmissionMode" /> value.</exception>
		/// <exception cref="T:System.NotSupportedException">The supplied value is not a supported <see cref="T:System.IO.Pipes.PipeTransmissionMode" /> value for this pipe stream.</exception>
		/// <exception cref="T:System.InvalidOperationException">The handle has not been set.-or-The pipe is waiting to connect with a named client.</exception>
		/// <exception cref="T:System.IO.IOException">The pipe is broken or an I/O error occurred with a named client.</exception>
		// Token: 0x1700002B RID: 43
		// (get) Token: 0x06000119 RID: 281 RVA: 0x000039AF File Offset: 0x00001BAF
		// (set) Token: 0x0600011A RID: 282 RVA: 0x000039BD File Offset: 0x00001BBD
		public virtual PipeTransmissionMode ReadMode
		{
			get
			{
				this.CheckPipePropertyOperations();
				return this.read_trans_mode;
			}
			set
			{
				this.CheckPipePropertyOperations();
				this.read_trans_mode = value;
			}
		}

		/// <summary>Gets the safe handle for the local end of the pipe that the current <see cref="T:System.IO.Pipes.PipeStream" /> object encapsulates.</summary>
		/// <returns>A <see cref="T:Microsoft.Win32.SafeHandles.SafePipeHandle" /> object for the pipe that is encapsulated by the current <see cref="T:System.IO.Pipes.PipeStream" /> object.</returns>
		/// <exception cref="T:System.InvalidOperationException">The pipe handle has not been set.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The pipe is closed.</exception>
		// Token: 0x1700002C RID: 44
		// (get) Token: 0x0600011B RID: 283 RVA: 0x000039CC File Offset: 0x00001BCC
		public SafePipeHandle SafePipeHandle
		{
			get
			{
				this.CheckPipePropertyOperations();
				return this.handle;
			}
		}

		/// <summary>Gets the pipe transmission mode supported by the current pipe.</summary>
		/// <returns>One of the <see cref="T:System.IO.Pipes.PipeTransmissionMode" /> values that indicates the transmission mode supported by the current pipe.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The pipe is closed.</exception>
		/// <exception cref="T:System.InvalidOperationException">The handle has not been set.-or-The pipe is waiting to connect in an anonymous client/server operation or with a named client. </exception>
		/// <exception cref="T:System.IO.IOException">The pipe is broken or another I/O error occurred.</exception>
		// Token: 0x1700002D RID: 45
		// (get) Token: 0x0600011C RID: 284 RVA: 0x000039DA File Offset: 0x00001BDA
		public virtual PipeTransmissionMode TransmissionMode
		{
			get
			{
				this.CheckPipePropertyOperations();
				return this.transmission_mode;
			}
		}

		/// <summary>Verifies that the pipe is in a proper state for getting or setting properties.</summary>
		// Token: 0x0600011D RID: 285 RVA: 0x000039E8 File Offset: 0x00001BE8
		[MonoTODO]
		protected internal virtual void CheckPipePropertyOperations()
		{
		}

		/// <summary>Verifies that the pipe is in a connected state for read operations.</summary>
		// Token: 0x0600011E RID: 286 RVA: 0x000039EA File Offset: 0x00001BEA
		[MonoTODO]
		protected internal void CheckReadOperations()
		{
			if (!this.IsConnected)
			{
				throw new InvalidOperationException("Pipe is not connected");
			}
			if (!this.CanRead)
			{
				throw new NotSupportedException("The pipe stream does not support read operations");
			}
		}

		/// <summary>Verifies that the pipe is in a connected state for write operations.</summary>
		// Token: 0x0600011F RID: 287 RVA: 0x00003A12 File Offset: 0x00001C12
		[MonoTODO]
		protected internal void CheckWriteOperations()
		{
			if (!this.IsConnected)
			{
				throw new InvalidOperationException("Pipe is not connected");
			}
			if (!this.CanWrite)
			{
				throw new NotSupportedException("The pipe stream does not support write operations");
			}
		}

		/// <summary>Initializes a <see cref="T:System.IO.Pipes.PipeStream" /> object from the specified <see cref="T:Microsoft.Win32.SafeHandles.SafePipeHandle" /> object.</summary>
		/// <param name="handle">The <see cref="T:Microsoft.Win32.SafeHandles.SafePipeHandle" /> object of the pipe to initialize.</param>
		/// <param name="isExposed">
		///       <see langword="true" /> to expose the handle; otherwise, <see langword="false" />.</param>
		/// <param name="isAsync">
		///       <see langword="true" /> to indicate that the handle was opened asynchronously; otherwise, <see langword="false" />.</param>
		/// <exception cref="T:System.IO.IOException">A handle cannot be bound to the pipe.</exception>
		// Token: 0x06000120 RID: 288 RVA: 0x00003A3A File Offset: 0x00001C3A
		protected void InitializeHandle(SafePipeHandle handle, bool isExposed, bool isAsync)
		{
			this.handle = handle;
			this.IsHandleExposed = isExposed;
			this.IsAsync = isAsync;
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.IO.Pipes.PipeStream" /> class and optionally releases the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources.</param>
		// Token: 0x06000121 RID: 289 RVA: 0x00003A51 File Offset: 0x00001C51
		protected override void Dispose(bool disposing)
		{
			if (this.handle != null && disposing)
			{
				this.handle.Dispose();
			}
		}

		/// <summary>Gets the length of a stream, in bytes.</summary>
		/// <returns>0 in all cases.</returns>
		/// <exception cref="T:System.NotSupportedException">Always thrown.</exception>
		// Token: 0x1700002E RID: 46
		// (get) Token: 0x06000122 RID: 290 RVA: 0x00003A6B File Offset: 0x00001C6B
		public override long Length
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		/// <summary>Gets or sets the current position of the current stream.</summary>
		/// <returns>0 in all cases.</returns>
		/// <exception cref="T:System.NotSupportedException">Always thrown.</exception>
		// Token: 0x1700002F RID: 47
		// (get) Token: 0x06000123 RID: 291 RVA: 0x00003A72 File Offset: 0x00001C72
		// (set) Token: 0x06000124 RID: 292 RVA: 0x00003A6B File Offset: 0x00001C6B
		public override long Position
		{
			get
			{
				return 0L;
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		/// <summary>Sets the length of the current stream to the specified value.</summary>
		/// <param name="value">The new length of the stream.</param>
		// Token: 0x06000125 RID: 293 RVA: 0x00003A6B File Offset: 0x00001C6B
		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		/// <summary>Sets the current position of the current stream to the specified value.</summary>
		/// <param name="offset">The point, relative to <paramref name="origin" />, to begin seeking from.</param>
		/// <param name="origin">Specifies the beginning, the end, or the current position as a reference point for <paramref name="offset" />, using a value of type <see cref="T:System.IO.SeekOrigin" />.</param>
		/// <returns>The new position in the stream.</returns>
		// Token: 0x06000126 RID: 294 RVA: 0x00003A6B File Offset: 0x00001C6B
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		/// <summary>Gets a <see cref="T:System.IO.Pipes.PipeSecurity" /> object that encapsulates the access control list (ACL) entries for the pipe described by the current <see cref="T:System.IO.Pipes.PipeStream" /> object.</summary>
		/// <returns>A <see cref="T:System.IO.Pipes.PipeSecurity" /> object that encapsulates the access control list (ACL) entries for the pipe described by the current <see cref="T:System.IO.Pipes.PipeStream" /> object.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The pipe is closed.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The underlying call to set security information failed.</exception>
		/// <exception cref="T:System.InvalidOperationException">The underlying call to set security information failed.</exception>
		/// <exception cref="T:System.NotSupportedException">The underlying call to set security information failed.</exception>
		// Token: 0x06000127 RID: 295 RVA: 0x00003A76 File Offset: 0x00001C76
		public PipeSecurity GetAccessControl()
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Applies the access control list (ACL) entries specified by a <see cref="T:System.IO.Pipes.PipeSecurity" /> object to the pipe specified by the current <see cref="T:System.IO.Pipes.PipeStream" /> object.</summary>
		/// <param name="pipeSecurity">A <see cref="T:System.IO.Pipes.PipeSecurity" /> object that specifies an access control list (ACL) entry to apply to the current pipe.</param>
		/// <exception cref="T:System.ObjectDisposedException">The pipe is closed.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pipeSecurity" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The underlying call to set security information failed.</exception>
		/// <exception cref="T:System.InvalidOperationException">The underlying call to set security information failed.</exception>
		/// <exception cref="T:System.NotSupportedException">The underlying call to set security information failed.</exception>
		// Token: 0x06000128 RID: 296 RVA: 0x00003A76 File Offset: 0x00001C76
		public void SetAccessControl(PipeSecurity pipeSecurity)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Waits for the other end of the pipe to read all sent bytes.</summary>
		/// <exception cref="T:System.ObjectDisposedException">The pipe is closed.</exception>
		/// <exception cref="T:System.NotSupportedException">The pipe does not support write operations.</exception>
		/// <exception cref="T:System.IO.IOException">The pipe is broken or another I/O error occurred.</exception>
		// Token: 0x06000129 RID: 297 RVA: 0x000039E8 File Offset: 0x00001BE8
		public void WaitForPipeDrain()
		{
		}

		/// <summary>Reads a block of bytes from a stream and writes the data to a specified buffer.</summary>
		/// <param name="buffer">When this method returns, contains the specified byte array with the values between <paramref name="offset" /> and (<paramref name="offset" /> + <paramref name="count" /> - 1) replaced by the bytes read from the current source.</param>
		/// <param name="offset">The byte offset in the <paramref name="buffer" /> array at which the bytes that are read will be placed.</param>
		/// <param name="count">The maximum number of bytes to read.</param>
		/// <returns>The total number of bytes that are read into <paramref name="buffer" />. This might be less than the number of bytes requested if that number of bytes is not currently available, or 0 if the end of the stream is reached.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="buffer" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is less than 0.-or-
		///         <paramref name="count" /> is less than 0.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="count" /> is greater than the number of bytes available in <paramref name="buffer" />.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The pipe is closed.</exception>
		/// <exception cref="T:System.NotSupportedException">The pipe does not support read operations.</exception>
		/// <exception cref="T:System.InvalidOperationException">The pipe is disconnected, waiting to connect, or the handle has not been set.</exception>
		/// <exception cref="T:System.IO.IOException">Any I/O error occurred.</exception>
		// Token: 0x0600012A RID: 298 RVA: 0x00003A7D File Offset: 0x00001C7D
		[MonoTODO]
		public override int Read([In] byte[] buffer, int offset, int count)
		{
			this.CheckReadOperations();
			return this.Stream.Read(buffer, offset, count);
		}

		/// <summary>Reads a byte from a pipe.</summary>
		/// <returns>The byte, cast to <see cref="T:System.Int32" />, or -1 indicates the end of the stream (the pipe has been closed).</returns>
		/// <exception cref="T:System.ObjectDisposedException">The pipe is closed.</exception>
		/// <exception cref="T:System.NotSupportedException">The pipe does not support read operations.</exception>
		/// <exception cref="T:System.InvalidOperationException">The pipe is disconnected, waiting to connect, or the handle has not been set.</exception>
		/// <exception cref="T:System.IO.IOException">Any I/O error occurred.</exception>
		// Token: 0x0600012B RID: 299 RVA: 0x00003A93 File Offset: 0x00001C93
		[MonoTODO]
		public override int ReadByte()
		{
			this.CheckReadOperations();
			return this.Stream.ReadByte();
		}

		/// <summary>Writes a block of bytes to the current stream using data from a buffer.</summary>
		/// <param name="buffer">The buffer that contains data to write to the pipe.</param>
		/// <param name="offset">The zero-based byte offset in <paramref name="buffer" /> at which to begin copying bytes to the current stream.</param>
		/// <param name="count">The maximum number of bytes to write to the current stream.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="buffer" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is less than 0.-or-
		///         <paramref name="count" /> is less than 0.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="count" /> is greater than the number of bytes available in <paramref name="buffer" />.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The pipe is closed.</exception>
		/// <exception cref="T:System.NotSupportedException">The pipe does not support write operations.</exception>
		/// <exception cref="T:System.IO.IOException">The pipe is broken or another I/O error occurred.</exception>
		// Token: 0x0600012C RID: 300 RVA: 0x00003AA6 File Offset: 0x00001CA6
		[MonoTODO]
		public override void Write(byte[] buffer, int offset, int count)
		{
			this.CheckWriteOperations();
			this.Stream.Write(buffer, offset, count);
		}

		/// <summary>Writes a byte to the current stream.</summary>
		/// <param name="value">The byte to write to the stream.</param>
		/// <exception cref="T:System.ObjectDisposedException">The pipe is closed.</exception>
		/// <exception cref="T:System.NotSupportedException">The pipe does not support write operations.</exception>
		/// <exception cref="T:System.InvalidOperationException">The pipe is disconnected, waiting to connect, or the handle has not been set.</exception>
		/// <exception cref="T:System.IO.IOException">The pipe is broken or another I/O error occurred.</exception>
		// Token: 0x0600012D RID: 301 RVA: 0x00003ABC File Offset: 0x00001CBC
		[MonoTODO]
		public override void WriteByte(byte value)
		{
			this.CheckWriteOperations();
			this.Stream.WriteByte(value);
		}

		/// <summary>Clears the buffer for the current stream and causes any buffered data to be written to the underlying device.</summary>
		/// <exception cref="T:System.ObjectDisposedException">The pipe is closed.</exception>
		/// <exception cref="T:System.NotSupportedException">The pipe does not support write operations.</exception>
		/// <exception cref="T:System.IO.IOException">The pipe is broken or another I/O error occurred.</exception>
		// Token: 0x0600012E RID: 302 RVA: 0x00003AD0 File Offset: 0x00001CD0
		[MonoTODO]
		public override void Flush()
		{
			this.CheckWriteOperations();
			this.Stream.Flush();
		}

		/// <summary>Begins an asynchronous read operation.</summary>
		/// <param name="buffer">The buffer to read data into.</param>
		/// <param name="offset">The byte offset in <paramref name="buffer" /> at which to begin reading.</param>
		/// <param name="count">The maximum number of bytes to read.</param>
		/// <param name="callback">The method to call when the asynchronous read operation is completed.</param>
		/// <param name="state">A user-provided object that distinguishes this particular asynchronous read request from other requests.</param>
		/// <returns>An <see cref="T:System.IAsyncResult" /> object that references the asynchronous read.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="buffer" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is less than 0.-or-
		///         <paramref name="count" /> is less than 0.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="count" /> is greater than the number of bytes available in <paramref name="buffer" />.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The pipe is closed.</exception>
		/// <exception cref="T:System.NotSupportedException">The pipe does not support read operations.</exception>
		/// <exception cref="T:System.InvalidOperationException">The pipe is disconnected, waiting to connect, or the handle has not been set.</exception>
		/// <exception cref="T:System.IO.IOException">The pipe is broken or another I/O error occurred.</exception>
		// Token: 0x0600012F RID: 303 RVA: 0x00003AE3 File Offset: 0x00001CE3
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			if (this.read_delegate == null)
			{
				this.read_delegate = new Func<byte[], int, int, int>(this.Read);
			}
			return this.read_delegate.BeginInvoke(buffer, offset, count, callback, state);
		}

		/// <summary>Begins an asynchronous write operation.</summary>
		/// <param name="buffer">The buffer that contains the data to write to the current stream.</param>
		/// <param name="offset">The zero-based byte offset in <paramref name="buffer" /> at which to begin copying bytes to the current stream.</param>
		/// <param name="count">The maximum number of bytes to write.</param>
		/// <param name="callback">The method to call when the asynchronous write operation is completed.</param>
		/// <param name="state">A user-provided object that distinguishes this particular asynchronous write request from other requests.</param>
		/// <returns>An <see cref="T:System.IAsyncResult" /> object that references the asynchronous write operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="buffer" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is less than 0.-or-
		///         <paramref name="count" /> is less than 0.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="count" /> is greater than the number of bytes available in <paramref name="buffer" />.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The pipe is closed.</exception>
		/// <exception cref="T:System.NotSupportedException">The pipe does not support write operations.</exception>
		/// <exception cref="T:System.InvalidOperationException">The pipe is disconnected, waiting to connect, or the handle has not been set.</exception>
		/// <exception cref="T:System.IO.IOException">The pipe is broken or another I/O error occurred.</exception>
		// Token: 0x06000130 RID: 304 RVA: 0x00003B12 File Offset: 0x00001D12
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			if (this.write_delegate == null)
			{
				this.write_delegate = new Action<byte[], int, int>(this.Write);
			}
			return this.write_delegate.BeginInvoke(buffer, offset, count, callback, state);
		}

		/// <summary>Ends a pending asynchronous read request.</summary>
		/// <param name="asyncResult">The reference to the pending asynchronous request.</param>
		/// <returns>The number of bytes that were read. A return value of 0 indicates the end of the stream (the pipe has been closed).</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="asyncResult" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="asyncResult" /> did not originate from a <see cref="M:System.IO.Pipes.PipeStream.BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)" /> method on the current stream. </exception>
		/// <exception cref="T:System.IO.IOException">The stream is closed or an internal error has occurred.</exception>
		// Token: 0x06000131 RID: 305 RVA: 0x00003B41 File Offset: 0x00001D41
		public override int EndRead(IAsyncResult asyncResult)
		{
			return this.read_delegate.EndInvoke(asyncResult);
		}

		/// <summary>Ends a pending asynchronous write request.</summary>
		/// <param name="asyncResult">The reference to the pending asynchronous request.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="asyncResult" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="asyncResult" /> did not originate from a <see cref="M:System.IO.Pipes.PipeStream.BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)" /> method on the current stream. </exception>
		/// <exception cref="T:System.IO.IOException">The stream is closed or an internal error has occurred.</exception>
		// Token: 0x06000132 RID: 306 RVA: 0x00003B4F File Offset: 0x00001D4F
		public override void EndWrite(IAsyncResult asyncResult)
		{
			this.write_delegate.EndInvoke(asyncResult);
		}

		// Token: 0x04000215 RID: 533
		internal const int DefaultBufferSize = 1024;

		// Token: 0x04000216 RID: 534
		private PipeDirection direction;

		// Token: 0x04000217 RID: 535
		private PipeTransmissionMode transmission_mode;

		// Token: 0x04000218 RID: 536
		private PipeTransmissionMode read_trans_mode;

		// Token: 0x04000219 RID: 537
		private int buffer_size;

		// Token: 0x0400021A RID: 538
		private SafePipeHandle handle;

		// Token: 0x0400021B RID: 539
		private Stream stream;

		// Token: 0x0400021C RID: 540
		[CompilerGenerated]
		private bool <IsAsync>k__BackingField;

		// Token: 0x0400021D RID: 541
		[CompilerGenerated]
		private bool <IsConnected>k__BackingField;

		// Token: 0x0400021E RID: 542
		[CompilerGenerated]
		private bool <IsHandleExposed>k__BackingField;

		// Token: 0x0400021F RID: 543
		[CompilerGenerated]
		private bool <IsMessageComplete>k__BackingField;

		// Token: 0x04000220 RID: 544
		private Func<byte[], int, int, int> read_delegate;

		// Token: 0x04000221 RID: 545
		private Action<byte[], int, int> write_delegate;
	}
}
