﻿using System;

namespace System
{
	// Token: 0x0200000F RID: 15
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
	internal class MonoLimitationAttribute : MonoTODOAttribute
	{
		// Token: 0x0600002A RID: 42 RVA: 0x000022D5 File Offset: 0x000004D5
		public MonoLimitationAttribute(string comment) : base(comment)
		{
		}
	}
}
