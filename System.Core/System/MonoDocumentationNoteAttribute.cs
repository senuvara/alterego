﻿using System;

namespace System
{
	// Token: 0x0200000C RID: 12
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
	internal class MonoDocumentationNoteAttribute : MonoTODOAttribute
	{
		// Token: 0x06000027 RID: 39 RVA: 0x000022D5 File Offset: 0x000004D5
		public MonoDocumentationNoteAttribute(string comment) : base(comment)
		{
		}
	}
}
