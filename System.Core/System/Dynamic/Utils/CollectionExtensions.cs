﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;

namespace System.Dynamic.Utils
{
	// Token: 0x0200046C RID: 1132
	internal static class CollectionExtensions
	{
		// Token: 0x06001B6A RID: 7018 RVA: 0x000500C8 File Offset: 0x0004E2C8
		public static TrueReadOnlyCollection<T> AddFirst<T>(this ReadOnlyCollection<T> list, T item)
		{
			T[] array = new T[list.Count + 1];
			array[0] = item;
			list.CopyTo(array, 1);
			return new TrueReadOnlyCollection<T>(array);
		}

		// Token: 0x06001B6B RID: 7019 RVA: 0x000500FC File Offset: 0x0004E2FC
		public static T[] AddFirst<T>(this T[] array, T item)
		{
			T[] array2 = new T[array.Length + 1];
			array2[0] = item;
			array.CopyTo(array2, 1);
			return array2;
		}

		// Token: 0x06001B6C RID: 7020 RVA: 0x00050128 File Offset: 0x0004E328
		public static T[] AddLast<T>(this T[] array, T item)
		{
			T[] array2 = new T[array.Length + 1];
			array.CopyTo(array2, 0);
			array2[array.Length] = item;
			return array2;
		}

		// Token: 0x06001B6D RID: 7021 RVA: 0x00050154 File Offset: 0x0004E354
		public static T[] RemoveFirst<T>(this T[] array)
		{
			T[] array2 = new T[array.Length - 1];
			Array.Copy(array, 1, array2, 0, array2.Length);
			return array2;
		}

		// Token: 0x06001B6E RID: 7022 RVA: 0x0005017C File Offset: 0x0004E37C
		public static T[] RemoveLast<T>(this T[] array)
		{
			T[] array2 = new T[array.Length - 1];
			Array.Copy(array, 0, array2, 0, array2.Length);
			return array2;
		}

		// Token: 0x06001B6F RID: 7023 RVA: 0x000501A4 File Offset: 0x0004E3A4
		public static ReadOnlyCollection<T> ToReadOnly<T>(this IEnumerable<T> enumerable)
		{
			if (enumerable == null)
			{
				return EmptyReadOnlyCollection<T>.Instance;
			}
			TrueReadOnlyCollection<T> trueReadOnlyCollection = enumerable as TrueReadOnlyCollection<T>;
			if (trueReadOnlyCollection != null)
			{
				return trueReadOnlyCollection;
			}
			ReadOnlyCollectionBuilder<T> readOnlyCollectionBuilder = enumerable as ReadOnlyCollectionBuilder<T>;
			if (readOnlyCollectionBuilder != null)
			{
				return readOnlyCollectionBuilder.ToReadOnlyCollection();
			}
			T[] array = EnumerableHelpers.ToArray<T>(enumerable);
			if (array.Length != 0)
			{
				return new TrueReadOnlyCollection<T>(array);
			}
			return EmptyReadOnlyCollection<T>.Instance;
		}

		// Token: 0x06001B70 RID: 7024 RVA: 0x000501F0 File Offset: 0x0004E3F0
		public static int ListHashCode<T>(this ReadOnlyCollection<T> list)
		{
			EqualityComparer<T> @default = EqualityComparer<T>.Default;
			int num = 6551;
			foreach (T obj in list)
			{
				num ^= (num << 5 ^ @default.GetHashCode(obj));
			}
			return num;
		}

		// Token: 0x06001B71 RID: 7025 RVA: 0x0005024C File Offset: 0x0004E44C
		public static bool ListEquals<T>(this ReadOnlyCollection<T> first, ReadOnlyCollection<T> second)
		{
			if (first == second)
			{
				return true;
			}
			int count = first.Count;
			if (count != second.Count)
			{
				return false;
			}
			EqualityComparer<T> @default = EqualityComparer<T>.Default;
			for (int num = 0; num != count; num++)
			{
				if (!@default.Equals(first[num], second[num]))
				{
					return false;
				}
			}
			return true;
		}
	}
}
