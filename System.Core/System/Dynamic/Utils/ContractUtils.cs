﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;

namespace System.Dynamic.Utils
{
	// Token: 0x0200046D RID: 1133
	internal static class ContractUtils
	{
		// Token: 0x1700055A RID: 1370
		// (get) Token: 0x06001B72 RID: 7026 RVA: 0x0005029C File Offset: 0x0004E49C
		[ExcludeFromCodeCoverage]
		public static Exception Unreachable
		{
			get
			{
				return new InvalidOperationException("Code supposed to be unreachable");
			}
		}

		// Token: 0x06001B73 RID: 7027 RVA: 0x000502A8 File Offset: 0x0004E4A8
		public static void Requires(bool precondition, string paramName)
		{
			if (!precondition)
			{
				throw Error.InvalidArgumentValue(paramName);
			}
		}

		// Token: 0x06001B74 RID: 7028 RVA: 0x000502B4 File Offset: 0x0004E4B4
		public static void RequiresNotNull(object value, string paramName)
		{
			if (value == null)
			{
				throw new ArgumentNullException(paramName);
			}
		}

		// Token: 0x06001B75 RID: 7029 RVA: 0x000502C0 File Offset: 0x0004E4C0
		public static void RequiresNotNull(object value, string paramName, int index)
		{
			if (value == null)
			{
				throw new ArgumentNullException(ContractUtils.GetParamName(paramName, index));
			}
		}

		// Token: 0x06001B76 RID: 7030 RVA: 0x000502D2 File Offset: 0x0004E4D2
		public static void RequiresNotEmpty<T>(ICollection<T> collection, string paramName)
		{
			ContractUtils.RequiresNotNull(collection, paramName);
			if (collection.Count == 0)
			{
				throw Error.NonEmptyCollectionRequired(paramName);
			}
		}

		// Token: 0x06001B77 RID: 7031 RVA: 0x000502EC File Offset: 0x0004E4EC
		public static void RequiresNotNullItems<T>(IList<T> array, string arrayName)
		{
			ContractUtils.RequiresNotNull(array, arrayName);
			int i = 0;
			int count = array.Count;
			while (i < count)
			{
				if (array[i] == null)
				{
					throw new ArgumentNullException(ContractUtils.GetParamName(arrayName, i));
				}
				i++;
			}
		}

		// Token: 0x06001B78 RID: 7032 RVA: 0x000039E8 File Offset: 0x00001BE8
		[Conditional("DEBUG")]
		public static void AssertLockHeld(object lockObject)
		{
		}

		// Token: 0x06001B79 RID: 7033 RVA: 0x0005032E File Offset: 0x0004E52E
		private static string GetParamName(string paramName, int index)
		{
			if (index < 0)
			{
				return paramName;
			}
			return string.Format("{0}[{1}]", paramName, index);
		}

		// Token: 0x06001B7A RID: 7034 RVA: 0x00050347 File Offset: 0x0004E547
		public static void RequiresArrayRange<T>(IList<T> array, int offset, int count, string offsetName, string countName)
		{
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException(countName);
			}
			if (offset < 0 || array.Count - offset < count)
			{
				throw new ArgumentOutOfRangeException(offsetName);
			}
		}
	}
}
