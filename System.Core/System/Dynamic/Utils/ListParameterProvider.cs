﻿using System;
using System.Linq.Expressions;

namespace System.Dynamic.Utils
{
	// Token: 0x02000474 RID: 1140
	internal sealed class ListParameterProvider : ListProvider<ParameterExpression>
	{
		// Token: 0x06001BA9 RID: 7081 RVA: 0x00050AD2 File Offset: 0x0004ECD2
		internal ListParameterProvider(IParameterProvider provider, ParameterExpression arg0)
		{
			this._provider = provider;
			this._arg0 = arg0;
		}

		// Token: 0x17000564 RID: 1380
		// (get) Token: 0x06001BAA RID: 7082 RVA: 0x00050AE8 File Offset: 0x0004ECE8
		protected override ParameterExpression First
		{
			get
			{
				return this._arg0;
			}
		}

		// Token: 0x17000565 RID: 1381
		// (get) Token: 0x06001BAB RID: 7083 RVA: 0x00050AF0 File Offset: 0x0004ECF0
		protected override int ElementCount
		{
			get
			{
				return this._provider.ParameterCount;
			}
		}

		// Token: 0x06001BAC RID: 7084 RVA: 0x00050AFD File Offset: 0x0004ECFD
		protected override ParameterExpression GetElement(int index)
		{
			return this._provider.GetParameter(index);
		}

		// Token: 0x04000CB6 RID: 3254
		private readonly IParameterProvider _provider;

		// Token: 0x04000CB7 RID: 3255
		private readonly ParameterExpression _arg0;
	}
}
