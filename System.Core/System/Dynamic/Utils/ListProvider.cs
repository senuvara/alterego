﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

namespace System.Dynamic.Utils
{
	// Token: 0x02000471 RID: 1137
	internal abstract class ListProvider<T> : IList<!0>, ICollection<!0>, IEnumerable<!0>, IEnumerable where T : class
	{
		// Token: 0x1700055B RID: 1371
		// (get) Token: 0x06001B8D RID: 7053
		protected abstract T First { get; }

		// Token: 0x1700055C RID: 1372
		// (get) Token: 0x06001B8E RID: 7054
		protected abstract int ElementCount { get; }

		// Token: 0x06001B8F RID: 7055
		protected abstract T GetElement(int index);

		// Token: 0x06001B90 RID: 7056 RVA: 0x000508D0 File Offset: 0x0004EAD0
		public int IndexOf(T item)
		{
			if (this.First == item)
			{
				return 0;
			}
			int i = 1;
			int elementCount = this.ElementCount;
			while (i < elementCount)
			{
				if (this.GetElement(i) == item)
				{
					return i;
				}
				i++;
			}
			return -1;
		}

		// Token: 0x06001B91 RID: 7057 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		public void Insert(int index, T item)
		{
			throw ContractUtils.Unreachable;
		}

		// Token: 0x06001B92 RID: 7058 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		public void RemoveAt(int index)
		{
			throw ContractUtils.Unreachable;
		}

		// Token: 0x1700055D RID: 1373
		public T this[int index]
		{
			get
			{
				if (index == 0)
				{
					return this.First;
				}
				return this.GetElement(index);
			}
			[ExcludeFromCodeCoverage]
			set
			{
				throw ContractUtils.Unreachable;
			}
		}

		// Token: 0x06001B95 RID: 7061 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		public void Add(T item)
		{
			throw ContractUtils.Unreachable;
		}

		// Token: 0x06001B96 RID: 7062 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		public void Clear()
		{
			throw ContractUtils.Unreachable;
		}

		// Token: 0x06001B97 RID: 7063 RVA: 0x0005092F File Offset: 0x0004EB2F
		public bool Contains(T item)
		{
			return this.IndexOf(item) != -1;
		}

		// Token: 0x06001B98 RID: 7064 RVA: 0x00050940 File Offset: 0x0004EB40
		public void CopyTo(T[] array, int index)
		{
			ContractUtils.RequiresNotNull(array, "array");
			if (index < 0)
			{
				throw Error.ArgumentOutOfRange("index");
			}
			int elementCount = this.ElementCount;
			if (index + elementCount > array.Length)
			{
				throw new ArgumentException();
			}
			array[index++] = this.First;
			for (int i = 1; i < elementCount; i++)
			{
				array[index++] = this.GetElement(i);
			}
		}

		// Token: 0x1700055E RID: 1374
		// (get) Token: 0x06001B99 RID: 7065 RVA: 0x000509AD File Offset: 0x0004EBAD
		public int Count
		{
			get
			{
				return this.ElementCount;
			}
		}

		// Token: 0x1700055F RID: 1375
		// (get) Token: 0x06001B9A RID: 7066 RVA: 0x00009CDF File Offset: 0x00007EDF
		[ExcludeFromCodeCoverage]
		public bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06001B9B RID: 7067 RVA: 0x0002ED20 File Offset: 0x0002CF20
		[ExcludeFromCodeCoverage]
		public bool Remove(T item)
		{
			throw ContractUtils.Unreachable;
		}

		// Token: 0x06001B9C RID: 7068 RVA: 0x000509B5 File Offset: 0x0004EBB5
		public IEnumerator<T> GetEnumerator()
		{
			yield return this.First;
			int i = 1;
			int j = this.ElementCount;
			while (i < j)
			{
				yield return this.GetElement(i);
				int num = i;
				i = num + 1;
			}
			yield break;
		}

		// Token: 0x06001B9D RID: 7069 RVA: 0x000509C4 File Offset: 0x0004EBC4
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x06001B9E RID: 7070 RVA: 0x00002310 File Offset: 0x00000510
		protected ListProvider()
		{
		}

		// Token: 0x02000472 RID: 1138
		[CompilerGenerated]
		private sealed class <GetEnumerator>d__20 : IEnumerator<!0>, IDisposable, IEnumerator
		{
			// Token: 0x06001B9F RID: 7071 RVA: 0x000509CC File Offset: 0x0004EBCC
			[DebuggerHidden]
			public <GetEnumerator>d__20(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x06001BA0 RID: 7072 RVA: 0x000039E8 File Offset: 0x00001BE8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06001BA1 RID: 7073 RVA: 0x000509DC File Offset: 0x0004EBDC
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				ListProvider<T> listProvider = this;
				switch (num)
				{
				case 0:
					this.<>1__state = -1;
					this.<>2__current = listProvider.First;
					this.<>1__state = 1;
					return true;
				case 1:
					this.<>1__state = -1;
					i = 1;
					j = listProvider.ElementCount;
					break;
				case 2:
				{
					this.<>1__state = -1;
					int num2 = i;
					i = num2 + 1;
					break;
				}
				default:
					return false;
				}
				if (i >= j)
				{
					return false;
				}
				this.<>2__current = listProvider.GetElement(i);
				this.<>1__state = 2;
				return true;
			}

			// Token: 0x17000560 RID: 1376
			// (get) Token: 0x06001BA2 RID: 7074 RVA: 0x00050A84 File Offset: 0x0004EC84
			T IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06001BA3 RID: 7075 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000561 RID: 1377
			// (get) Token: 0x06001BA4 RID: 7076 RVA: 0x00050A8C File Offset: 0x0004EC8C
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x04000CAF RID: 3247
			private int <>1__state;

			// Token: 0x04000CB0 RID: 3248
			private T <>2__current;

			// Token: 0x04000CB1 RID: 3249
			public ListProvider<T> <>4__this;

			// Token: 0x04000CB2 RID: 3250
			private int <i>5__1;

			// Token: 0x04000CB3 RID: 3251
			private int <n>5__2;
		}
	}
}
