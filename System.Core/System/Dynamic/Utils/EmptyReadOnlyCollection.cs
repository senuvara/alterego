﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;

namespace System.Dynamic.Utils
{
	// Token: 0x0200046E RID: 1134
	internal static class EmptyReadOnlyCollection<T>
	{
		// Token: 0x06001B7B RID: 7035 RVA: 0x0005036B File Offset: 0x0004E56B
		// Note: this type is marked as 'beforefieldinit'.
		static EmptyReadOnlyCollection()
		{
		}

		// Token: 0x04000CAE RID: 3246
		public static readonly ReadOnlyCollection<T> Instance = new TrueReadOnlyCollection<T>(Array.Empty<T>());
	}
}
