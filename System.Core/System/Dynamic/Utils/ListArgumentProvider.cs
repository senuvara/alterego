﻿using System;
using System.Linq.Expressions;

namespace System.Dynamic.Utils
{
	// Token: 0x02000473 RID: 1139
	internal sealed class ListArgumentProvider : ListProvider<Expression>
	{
		// Token: 0x06001BA5 RID: 7077 RVA: 0x00050A99 File Offset: 0x0004EC99
		internal ListArgumentProvider(IArgumentProvider provider, Expression arg0)
		{
			this._provider = provider;
			this._arg0 = arg0;
		}

		// Token: 0x17000562 RID: 1378
		// (get) Token: 0x06001BA6 RID: 7078 RVA: 0x00050AAF File Offset: 0x0004ECAF
		protected override Expression First
		{
			get
			{
				return this._arg0;
			}
		}

		// Token: 0x17000563 RID: 1379
		// (get) Token: 0x06001BA7 RID: 7079 RVA: 0x00050AB7 File Offset: 0x0004ECB7
		protected override int ElementCount
		{
			get
			{
				return this._provider.ArgumentCount;
			}
		}

		// Token: 0x06001BA8 RID: 7080 RVA: 0x00050AC4 File Offset: 0x0004ECC4
		protected override Expression GetElement(int index)
		{
			return this._provider.GetArgument(index);
		}

		// Token: 0x04000CB4 RID: 3252
		private readonly IArgumentProvider _provider;

		// Token: 0x04000CB5 RID: 3253
		private readonly Expression _arg0;
	}
}
