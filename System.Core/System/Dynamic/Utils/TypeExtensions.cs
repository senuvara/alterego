﻿using System;
using System.Reflection;

namespace System.Dynamic.Utils
{
	// Token: 0x02000475 RID: 1141
	internal static class TypeExtensions
	{
		// Token: 0x06001BAD RID: 7085 RVA: 0x00050B0C File Offset: 0x0004ED0C
		public static MethodInfo GetAnyStaticMethodValidated(this Type type, string name, Type[] types)
		{
			MethodInfo method = type.GetMethod(name, BindingFlags.DeclaredOnly | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, types, null);
			if (!method.MatchesArgumentTypes(types))
			{
				return null;
			}
			return method;
		}

		// Token: 0x06001BAE RID: 7086 RVA: 0x00050B34 File Offset: 0x0004ED34
		private static bool MatchesArgumentTypes(this MethodInfo mi, Type[] argTypes)
		{
			if (mi == null)
			{
				return false;
			}
			ParameterInfo[] parametersCached = mi.GetParametersCached();
			if (parametersCached.Length != argTypes.Length)
			{
				return false;
			}
			for (int i = 0; i < parametersCached.Length; i++)
			{
				if (!TypeUtils.AreReferenceAssignable(parametersCached[i].ParameterType, argTypes[i]))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06001BAF RID: 7087 RVA: 0x00050B80 File Offset: 0x0004ED80
		public static Type GetReturnType(this MethodBase mi)
		{
			if (!mi.IsConstructor)
			{
				return ((MethodInfo)mi).ReturnType;
			}
			return mi.DeclaringType;
		}

		// Token: 0x06001BB0 RID: 7088 RVA: 0x00050B9C File Offset: 0x0004ED9C
		public static TypeCode GetTypeCode(this Type type)
		{
			return Type.GetTypeCode(type);
		}

		// Token: 0x06001BB1 RID: 7089 RVA: 0x00050BA4 File Offset: 0x0004EDA4
		internal static ParameterInfo[] GetParametersCached(this MethodBase method)
		{
			CacheDict<MethodBase, ParameterInfo[]> cacheDict = TypeExtensions.s_paramInfoCache;
			ParameterInfo[] parameters;
			if (!cacheDict.TryGetValue(method, out parameters))
			{
				parameters = method.GetParameters();
				Type declaringType = method.DeclaringType;
				if (declaringType != null && declaringType.CanCache())
				{
					cacheDict[method] = parameters;
				}
			}
			return parameters;
		}

		// Token: 0x06001BB2 RID: 7090 RVA: 0x00050BEA File Offset: 0x0004EDEA
		// Note: this type is marked as 'beforefieldinit'.
		static TypeExtensions()
		{
		}

		// Token: 0x04000CB8 RID: 3256
		private static readonly CacheDict<MethodBase, ParameterInfo[]> s_paramInfoCache = new CacheDict<MethodBase, ParameterInfo[]>(75);
	}
}
