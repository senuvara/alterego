﻿using System;
using System.Threading;

namespace System.Dynamic.Utils
{
	// Token: 0x0200046A RID: 1130
	internal sealed class CacheDict<TKey, TValue>
	{
		// Token: 0x06001B64 RID: 7012 RVA: 0x0004FF54 File Offset: 0x0004E154
		internal CacheDict(int size)
		{
			int num = CacheDict<TKey, TValue>.AlignSize(size);
			this._mask = num - 1;
			this._entries = new CacheDict<TKey, TValue>.Entry[num];
		}

		// Token: 0x06001B65 RID: 7013 RVA: 0x0004FF83 File Offset: 0x0004E183
		private static int AlignSize(int size)
		{
			size--;
			size |= size >> 1;
			size |= size >> 2;
			size |= size >> 4;
			size |= size >> 8;
			size |= size >> 16;
			size++;
			return size;
		}

		// Token: 0x06001B66 RID: 7014 RVA: 0x0004FFB4 File Offset: 0x0004E1B4
		internal bool TryGetValue(TKey key, out TValue value)
		{
			int hashCode = key.GetHashCode();
			int num = hashCode & this._mask;
			CacheDict<TKey, TValue>.Entry entry = Volatile.Read<CacheDict<TKey, TValue>.Entry>(ref this._entries[num]);
			if (entry != null && entry._hash == hashCode)
			{
				TKey key2 = entry._key;
				if (key2.Equals(key))
				{
					value = entry._value;
					return true;
				}
			}
			value = default(TValue);
			return false;
		}

		// Token: 0x06001B67 RID: 7015 RVA: 0x00050028 File Offset: 0x0004E228
		internal void Add(TKey key, TValue value)
		{
			int hashCode = key.GetHashCode();
			int num = hashCode & this._mask;
			CacheDict<TKey, TValue>.Entry entry = Volatile.Read<CacheDict<TKey, TValue>.Entry>(ref this._entries[num]);
			if (entry != null && entry._hash == hashCode)
			{
				TKey key2 = entry._key;
				if (key2.Equals(key))
				{
					return;
				}
			}
			Volatile.Write<CacheDict<TKey, TValue>.Entry>(ref this._entries[num], new CacheDict<TKey, TValue>.Entry(hashCode, key, value));
		}

		// Token: 0x17000559 RID: 1369
		internal TValue this[TKey key]
		{
			set
			{
				this.Add(key, value);
			}
		}

		// Token: 0x04000CA9 RID: 3241
		private readonly int _mask;

		// Token: 0x04000CAA RID: 3242
		private readonly CacheDict<TKey, TValue>.Entry[] _entries;

		// Token: 0x0200046B RID: 1131
		private sealed class Entry
		{
			// Token: 0x06001B69 RID: 7017 RVA: 0x000500A9 File Offset: 0x0004E2A9
			internal Entry(int hash, TKey key, TValue value)
			{
				this._hash = hash;
				this._key = key;
				this._value = value;
			}

			// Token: 0x04000CAB RID: 3243
			internal readonly int _hash;

			// Token: 0x04000CAC RID: 3244
			internal readonly TKey _key;

			// Token: 0x04000CAD RID: 3245
			internal readonly TValue _value;
		}
	}
}
