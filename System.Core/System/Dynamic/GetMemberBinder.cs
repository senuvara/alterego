﻿using System;
using System.Dynamic.Utils;
using System.Runtime.CompilerServices;

namespace System.Dynamic
{
	/// <summary>Represents the dynamic get member operation at the call site, providing the binding semantic and the details about the operation.</summary>
	// Token: 0x02000461 RID: 1121
	public abstract class GetMemberBinder : DynamicMetaObjectBinder
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Dynamic.GetMemberBinder" />.</summary>
		/// <param name="name">The name of the member to obtain.</param>
		/// <param name="ignoreCase">Is true if the name should be matched ignoring case; false otherwise.</param>
		// Token: 0x06001B32 RID: 6962 RVA: 0x0004FC56 File Offset: 0x0004DE56
		protected GetMemberBinder(string name, bool ignoreCase)
		{
			ContractUtils.RequiresNotNull(name, "name");
			this.Name = name;
			this.IgnoreCase = ignoreCase;
		}

		/// <summary>The result type of the operation.</summary>
		/// <returns>The <see cref="T:System.Type" /> object representing the result type of the operation.</returns>
		// Token: 0x17000542 RID: 1346
		// (get) Token: 0x06001B33 RID: 6963 RVA: 0x000314CE File Offset: 0x0002F6CE
		public sealed override Type ReturnType
		{
			get
			{
				return typeof(object);
			}
		}

		/// <summary>Gets the name of the member to obtain.</summary>
		/// <returns>The name of the member to obtain.</returns>
		// Token: 0x17000543 RID: 1347
		// (get) Token: 0x06001B34 RID: 6964 RVA: 0x0004FC77 File Offset: 0x0004DE77
		public string Name
		{
			[CompilerGenerated]
			get
			{
				return this.<Name>k__BackingField;
			}
		}

		/// <summary>Gets the value indicating if the string comparison should ignore the case of the member name.</summary>
		/// <returns>True if the case is ignored, otherwise false.</returns>
		// Token: 0x17000544 RID: 1348
		// (get) Token: 0x06001B35 RID: 6965 RVA: 0x0004FC7F File Offset: 0x0004DE7F
		public bool IgnoreCase
		{
			[CompilerGenerated]
			get
			{
				return this.<IgnoreCase>k__BackingField;
			}
		}

		/// <summary>Performs the binding of the dynamic get member operation if the target dynamic object cannot bind.</summary>
		/// <param name="target">The target of the dynamic get member operation.</param>
		/// <returns>The <see cref="T:System.Dynamic.DynamicMetaObject" /> representing the result of the binding.</returns>
		// Token: 0x06001B36 RID: 6966 RVA: 0x0004FC87 File Offset: 0x0004DE87
		public DynamicMetaObject FallbackGetMember(DynamicMetaObject target)
		{
			return this.FallbackGetMember(target, null);
		}

		/// <summary>When overridden in the derived class, performs the binding of the dynamic get member operation if the target dynamic object cannot bind.</summary>
		/// <param name="target">The target of the dynamic get member operation.</param>
		/// <param name="errorSuggestion">The binding result to use if binding fails, or null.</param>
		/// <returns>The <see cref="T:System.Dynamic.DynamicMetaObject" /> representing the result of the binding.</returns>
		// Token: 0x06001B37 RID: 6967
		public abstract DynamicMetaObject FallbackGetMember(DynamicMetaObject target, DynamicMetaObject errorSuggestion);

		/// <summary>Performs the binding of the dynamic get member operation.</summary>
		/// <param name="target">The target of the dynamic get member operation.</param>
		/// <param name="args">An array of arguments of the dynamic get member operation.</param>
		/// <returns>The <see cref="T:System.Dynamic.DynamicMetaObject" /> representing the result of the binding.</returns>
		// Token: 0x06001B38 RID: 6968 RVA: 0x0004FC91 File Offset: 0x0004DE91
		public sealed override DynamicMetaObject Bind(DynamicMetaObject target, DynamicMetaObject[] args)
		{
			ContractUtils.RequiresNotNull(target, "target");
			ContractUtils.Requires(args == null || args.Length == 0, "args");
			return target.BindGetMember(this);
		}

		// Token: 0x17000545 RID: 1349
		// (get) Token: 0x06001B39 RID: 6969 RVA: 0x00009CDF File Offset: 0x00007EDF
		internal sealed override bool IsStandardBinder
		{
			get
			{
				return true;
			}
		}

		// Token: 0x04000C9F RID: 3231
		[CompilerGenerated]
		private readonly string <Name>k__BackingField;

		// Token: 0x04000CA0 RID: 3232
		[CompilerGenerated]
		private readonly bool <IgnoreCase>k__BackingField;
	}
}
