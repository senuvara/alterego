﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

namespace System.Dynamic
{
	/// <summary>Describes arguments in the dynamic binding process.</summary>
	// Token: 0x0200043F RID: 1087
	public sealed class CallInfo
	{
		/// <summary>Creates a new PositionalArgumentInfo.</summary>
		/// <param name="argCount">The number of arguments.</param>
		/// <param name="argNames">The argument names.</param>
		// Token: 0x06001A29 RID: 6697 RVA: 0x0004D1D6 File Offset: 0x0004B3D6
		public CallInfo(int argCount, params string[] argNames) : this(argCount, argNames)
		{
		}

		/// <summary>Creates a new CallInfo that represents arguments in the dynamic binding process.</summary>
		/// <param name="argCount">The number of arguments.</param>
		/// <param name="argNames">The argument names.</param>
		// Token: 0x06001A2A RID: 6698 RVA: 0x0004D1E0 File Offset: 0x0004B3E0
		public CallInfo(int argCount, IEnumerable<string> argNames)
		{
			ContractUtils.RequiresNotNull(argNames, "argNames");
			ReadOnlyCollection<string> readOnlyCollection = argNames.ToReadOnly<string>();
			if (argCount < readOnlyCollection.Count)
			{
				throw Error.ArgCntMustBeGreaterThanNameCnt();
			}
			ContractUtils.RequiresNotNullItems<string>(readOnlyCollection, "argNames");
			this.ArgumentCount = argCount;
			this.ArgumentNames = readOnlyCollection;
		}

		/// <summary>The number of arguments.</summary>
		/// <returns>The number of arguments.</returns>
		// Token: 0x1700050D RID: 1293
		// (get) Token: 0x06001A2B RID: 6699 RVA: 0x0004D22D File Offset: 0x0004B42D
		public int ArgumentCount
		{
			[CompilerGenerated]
			get
			{
				return this.<ArgumentCount>k__BackingField;
			}
		}

		/// <summary>The argument names.</summary>
		/// <returns>The read-only collection of argument names.</returns>
		// Token: 0x1700050E RID: 1294
		// (get) Token: 0x06001A2C RID: 6700 RVA: 0x0004D235 File Offset: 0x0004B435
		public ReadOnlyCollection<string> ArgumentNames
		{
			[CompilerGenerated]
			get
			{
				return this.<ArgumentNames>k__BackingField;
			}
		}

		/// <summary>Serves as a hash function for the current <see cref="T:System.Dynamic.CallInfo" />.</summary>
		/// <returns>A hash code for the current <see cref="T:System.Dynamic.CallInfo" />.</returns>
		// Token: 0x06001A2D RID: 6701 RVA: 0x0004D23D File Offset: 0x0004B43D
		public override int GetHashCode()
		{
			return this.ArgumentCount ^ this.ArgumentNames.ListHashCode<string>();
		}

		/// <summary>Determines whether the specified CallInfo instance is considered equal to the current.</summary>
		/// <param name="obj">The instance of <see cref="T:System.Dynamic.CallInfo" /> to compare with the current instance.</param>
		/// <returns>true if the specified instance is equal to the current one otherwise, false.</returns>
		// Token: 0x06001A2E RID: 6702 RVA: 0x0004D254 File Offset: 0x0004B454
		public override bool Equals(object obj)
		{
			CallInfo callInfo = obj as CallInfo;
			return callInfo != null && this.ArgumentCount == callInfo.ArgumentCount && this.ArgumentNames.ListEquals(callInfo.ArgumentNames);
		}

		// Token: 0x04000C49 RID: 3145
		[CompilerGenerated]
		private readonly int <ArgumentCount>k__BackingField;

		// Token: 0x04000C4A RID: 3146
		[CompilerGenerated]
		private readonly ReadOnlyCollection<string> <ArgumentNames>k__BackingField;
	}
}
