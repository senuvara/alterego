﻿using System;
using System.Dynamic.Utils;
using System.Runtime.CompilerServices;

namespace System.Dynamic
{
	/// <summary>Represents the convert dynamic operation at the call site, providing the binding semantic and the details about the operation.</summary>
	// Token: 0x02000440 RID: 1088
	public abstract class ConvertBinder : DynamicMetaObjectBinder
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Dynamic.ConvertBinder" />.</summary>
		/// <param name="type">The type to convert to.</param>
		/// <param name="explicit">Is true if the conversion should consider explicit conversions; otherwise, false.</param>
		// Token: 0x06001A2F RID: 6703 RVA: 0x0004D28C File Offset: 0x0004B48C
		protected ConvertBinder(Type type, bool @explicit)
		{
			ContractUtils.RequiresNotNull(type, "type");
			this.Type = type;
			this.Explicit = @explicit;
		}

		/// <summary>The type to convert to.</summary>
		/// <returns>The <see cref="T:System.Type" /> object that represents the type to convert to.</returns>
		// Token: 0x1700050F RID: 1295
		// (get) Token: 0x06001A30 RID: 6704 RVA: 0x0004D2AD File Offset: 0x0004B4AD
		public Type Type
		{
			[CompilerGenerated]
			get
			{
				return this.<Type>k__BackingField;
			}
		}

		/// <summary>Gets the value indicating if the conversion should consider explicit conversions.</summary>
		/// <returns>
		///     <see langword="True" /> if there is an explicit conversion, otherwise <see langword="false" />.</returns>
		// Token: 0x17000510 RID: 1296
		// (get) Token: 0x06001A31 RID: 6705 RVA: 0x0004D2B5 File Offset: 0x0004B4B5
		public bool Explicit
		{
			[CompilerGenerated]
			get
			{
				return this.<Explicit>k__BackingField;
			}
		}

		/// <summary>Performs the binding of the dynamic convert operation if the target dynamic object cannot bind.</summary>
		/// <param name="target">The target of the dynamic convert operation.</param>
		/// <returns>The <see cref="T:System.Dynamic.DynamicMetaObject" /> representing the result of the binding.</returns>
		// Token: 0x06001A32 RID: 6706 RVA: 0x0004D2BD File Offset: 0x0004B4BD
		public DynamicMetaObject FallbackConvert(DynamicMetaObject target)
		{
			return this.FallbackConvert(target, null);
		}

		/// <summary>When overridden in the derived class, performs the binding of the dynamic convert operation if the target dynamic object cannot bind.</summary>
		/// <param name="target">The target of the dynamic convert operation.</param>
		/// <param name="errorSuggestion">The binding result to use if binding fails, or null.</param>
		/// <returns>The <see cref="T:System.Dynamic.DynamicMetaObject" /> representing the result of the binding.</returns>
		// Token: 0x06001A33 RID: 6707
		public abstract DynamicMetaObject FallbackConvert(DynamicMetaObject target, DynamicMetaObject errorSuggestion);

		/// <summary>Performs the binding of the dynamic convert operation.</summary>
		/// <param name="target">The target of the dynamic convert operation.</param>
		/// <param name="args">An array of arguments of the dynamic convert operation.</param>
		/// <returns>The <see cref="T:System.Dynamic.DynamicMetaObject" /> representing the result of the binding.</returns>
		// Token: 0x06001A34 RID: 6708 RVA: 0x0004D2C7 File Offset: 0x0004B4C7
		public sealed override DynamicMetaObject Bind(DynamicMetaObject target, DynamicMetaObject[] args)
		{
			ContractUtils.RequiresNotNull(target, "target");
			ContractUtils.Requires(args == null || args.Length == 0, "args");
			return target.BindConvert(this);
		}

		// Token: 0x17000511 RID: 1297
		// (get) Token: 0x06001A35 RID: 6709 RVA: 0x00009CDF File Offset: 0x00007EDF
		internal sealed override bool IsStandardBinder
		{
			get
			{
				return true;
			}
		}

		/// <summary>The result type of the operation.</summary>
		/// <returns>The <see cref="T:System.Type" /> object representing the result type of the operation.</returns>
		// Token: 0x17000512 RID: 1298
		// (get) Token: 0x06001A36 RID: 6710 RVA: 0x0004D2F0 File Offset: 0x0004B4F0
		public sealed override Type ReturnType
		{
			get
			{
				return this.Type;
			}
		}

		// Token: 0x04000C4B RID: 3147
		[CompilerGenerated]
		private readonly Type <Type>k__BackingField;

		// Token: 0x04000C4C RID: 3148
		[CompilerGenerated]
		private readonly bool <Explicit>k__BackingField;
	}
}
