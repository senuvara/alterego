﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic.Utils;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

namespace System.Dynamic
{
	/// <summary>Represents a set of binding restrictions on the <see cref="T:System.Dynamic.DynamicMetaObject" /> under which the dynamic binding is valid.</summary>
	// Token: 0x02000437 RID: 1079
	[DebuggerTypeProxy(typeof(BindingRestrictions.BindingRestrictionsProxy))]
	[DebuggerDisplay("{DebugView}")]
	public abstract class BindingRestrictions
	{
		// Token: 0x06001A07 RID: 6663 RVA: 0x00002310 File Offset: 0x00000510
		private BindingRestrictions()
		{
		}

		// Token: 0x06001A08 RID: 6664
		internal abstract Expression GetExpression();

		/// <summary>Merges the set of binding restrictions with the current binding restrictions.</summary>
		/// <param name="restrictions">The set of restrictions with which to merge the current binding restrictions.</param>
		/// <returns>The new set of binding restrictions.</returns>
		// Token: 0x06001A09 RID: 6665 RVA: 0x0004CCA4 File Offset: 0x0004AEA4
		public BindingRestrictions Merge(BindingRestrictions restrictions)
		{
			ContractUtils.RequiresNotNull(restrictions, "restrictions");
			if (this == BindingRestrictions.Empty)
			{
				return restrictions;
			}
			if (restrictions == BindingRestrictions.Empty)
			{
				return this;
			}
			return new BindingRestrictions.MergedRestriction(this, restrictions);
		}

		/// <summary>Creates the binding restriction that check the expression for runtime type identity.</summary>
		/// <param name="expression">The expression to test.</param>
		/// <param name="type">The exact type to test.</param>
		/// <returns>The new binding restrictions.</returns>
		// Token: 0x06001A0A RID: 6666 RVA: 0x0004CCCC File Offset: 0x0004AECC
		public static BindingRestrictions GetTypeRestriction(Expression expression, Type type)
		{
			ContractUtils.RequiresNotNull(expression, "expression");
			ContractUtils.RequiresNotNull(type, "type");
			return new BindingRestrictions.TypeRestriction(expression, type);
		}

		// Token: 0x06001A0B RID: 6667 RVA: 0x0004CCEB File Offset: 0x0004AEEB
		internal static BindingRestrictions GetTypeRestriction(DynamicMetaObject obj)
		{
			if (obj.Value == null && obj.HasValue)
			{
				return BindingRestrictions.GetInstanceRestriction(obj.Expression, null);
			}
			return BindingRestrictions.GetTypeRestriction(obj.Expression, obj.LimitType);
		}

		/// <summary>Creates the binding restriction that checks the expression for object instance identity.</summary>
		/// <param name="expression">The expression to test.</param>
		/// <param name="instance">The exact object instance to test.</param>
		/// <returns>The new binding restrictions.</returns>
		// Token: 0x06001A0C RID: 6668 RVA: 0x0004CD1B File Offset: 0x0004AF1B
		public static BindingRestrictions GetInstanceRestriction(Expression expression, object instance)
		{
			ContractUtils.RequiresNotNull(expression, "expression");
			return new BindingRestrictions.InstanceRestriction(expression, instance);
		}

		/// <summary>Creates the binding restriction that checks the expression for arbitrary immutable properties.</summary>
		/// <param name="expression">The expression representing the restrictions.</param>
		/// <returns>The new binding restrictions.</returns>
		// Token: 0x06001A0D RID: 6669 RVA: 0x0004CD2F File Offset: 0x0004AF2F
		public static BindingRestrictions GetExpressionRestriction(Expression expression)
		{
			ContractUtils.RequiresNotNull(expression, "expression");
			ContractUtils.Requires(expression.Type == typeof(bool), "expression");
			return new BindingRestrictions.CustomRestriction(expression);
		}

		/// <summary>Combines binding restrictions from the list of <see cref="T:System.Dynamic.DynamicMetaObject" /> instances into one set of restrictions.</summary>
		/// <param name="contributingObjects">The list of <see cref="T:System.Dynamic.DynamicMetaObject" /> instances from which to combine restrictions.</param>
		/// <returns>The new set of binding restrictions.</returns>
		// Token: 0x06001A0E RID: 6670 RVA: 0x0004CD64 File Offset: 0x0004AF64
		public static BindingRestrictions Combine(IList<DynamicMetaObject> contributingObjects)
		{
			BindingRestrictions bindingRestrictions = BindingRestrictions.Empty;
			if (contributingObjects != null)
			{
				foreach (DynamicMetaObject dynamicMetaObject in contributingObjects)
				{
					if (dynamicMetaObject != null)
					{
						bindingRestrictions = bindingRestrictions.Merge(dynamicMetaObject.Restrictions);
					}
				}
			}
			return bindingRestrictions;
		}

		/// <summary>Creates the <see cref="T:System.Linq.Expressions.Expression" /> representing the binding restrictions.</summary>
		/// <returns>The expression tree representing the restrictions.</returns>
		// Token: 0x06001A0F RID: 6671 RVA: 0x0004CDC0 File Offset: 0x0004AFC0
		public Expression ToExpression()
		{
			return this.GetExpression();
		}

		// Token: 0x17000509 RID: 1289
		// (get) Token: 0x06001A10 RID: 6672 RVA: 0x0004CDC8 File Offset: 0x0004AFC8
		private string DebugView
		{
			get
			{
				return this.ToExpression().ToString();
			}
		}

		// Token: 0x06001A11 RID: 6673 RVA: 0x0004CDD5 File Offset: 0x0004AFD5
		// Note: this type is marked as 'beforefieldinit'.
		static BindingRestrictions()
		{
		}

		/// <summary>Represents an empty set of binding restrictions. This field is read only.</summary>
		// Token: 0x04000C39 RID: 3129
		public static readonly BindingRestrictions Empty = new BindingRestrictions.CustomRestriction(Utils.Constant(true));

		// Token: 0x04000C3A RID: 3130
		private const int TypeRestrictionHash = 1227133513;

		// Token: 0x04000C3B RID: 3131
		private const int InstanceRestrictionHash = -1840700270;

		// Token: 0x04000C3C RID: 3132
		private const int CustomRestrictionHash = 613566756;

		// Token: 0x02000438 RID: 1080
		private sealed class TestBuilder
		{
			// Token: 0x06001A12 RID: 6674 RVA: 0x0004CDE7 File Offset: 0x0004AFE7
			internal void Append(BindingRestrictions restrictions)
			{
				if (this._unique.Add(restrictions))
				{
					this.Push(restrictions.GetExpression(), 0);
				}
			}

			// Token: 0x06001A13 RID: 6675 RVA: 0x0004CE04 File Offset: 0x0004B004
			internal Expression ToExpression()
			{
				Expression expression = this._tests.Pop().Node;
				while (this._tests.Count > 0)
				{
					expression = Expression.AndAlso(this._tests.Pop().Node, expression);
				}
				return expression;
			}

			// Token: 0x06001A14 RID: 6676 RVA: 0x0004CE4C File Offset: 0x0004B04C
			private void Push(Expression node, int depth)
			{
				while (this._tests.Count > 0 && this._tests.Peek().Depth == depth)
				{
					node = Expression.AndAlso(this._tests.Pop().Node, node);
					depth++;
				}
				this._tests.Push(new BindingRestrictions.TestBuilder.AndNode
				{
					Node = node,
					Depth = depth
				});
			}

			// Token: 0x06001A15 RID: 6677 RVA: 0x0004CEBD File Offset: 0x0004B0BD
			public TestBuilder()
			{
			}

			// Token: 0x04000C3D RID: 3133
			private readonly HashSet<BindingRestrictions> _unique = new HashSet<BindingRestrictions>();

			// Token: 0x04000C3E RID: 3134
			private readonly Stack<BindingRestrictions.TestBuilder.AndNode> _tests = new Stack<BindingRestrictions.TestBuilder.AndNode>();

			// Token: 0x02000439 RID: 1081
			private struct AndNode
			{
				// Token: 0x04000C3F RID: 3135
				internal int Depth;

				// Token: 0x04000C40 RID: 3136
				internal Expression Node;
			}
		}

		// Token: 0x0200043A RID: 1082
		private sealed class MergedRestriction : BindingRestrictions
		{
			// Token: 0x06001A16 RID: 6678 RVA: 0x0004CEDB File Offset: 0x0004B0DB
			internal MergedRestriction(BindingRestrictions left, BindingRestrictions right)
			{
				this.Left = left;
				this.Right = right;
			}

			// Token: 0x06001A17 RID: 6679 RVA: 0x0004CEF4 File Offset: 0x0004B0F4
			internal override Expression GetExpression()
			{
				BindingRestrictions.TestBuilder testBuilder = new BindingRestrictions.TestBuilder();
				Stack<BindingRestrictions> stack = new Stack<BindingRestrictions>();
				BindingRestrictions bindingRestrictions = this;
				for (;;)
				{
					BindingRestrictions.MergedRestriction mergedRestriction = bindingRestrictions as BindingRestrictions.MergedRestriction;
					if (mergedRestriction != null)
					{
						stack.Push(mergedRestriction.Right);
						bindingRestrictions = mergedRestriction.Left;
					}
					else
					{
						testBuilder.Append(bindingRestrictions);
						if (stack.Count == 0)
						{
							break;
						}
						bindingRestrictions = stack.Pop();
					}
				}
				return testBuilder.ToExpression();
			}

			// Token: 0x04000C41 RID: 3137
			internal readonly BindingRestrictions Left;

			// Token: 0x04000C42 RID: 3138
			internal readonly BindingRestrictions Right;
		}

		// Token: 0x0200043B RID: 1083
		private sealed class CustomRestriction : BindingRestrictions
		{
			// Token: 0x06001A18 RID: 6680 RVA: 0x0004CF4C File Offset: 0x0004B14C
			internal CustomRestriction(Expression expression)
			{
				this._expression = expression;
			}

			// Token: 0x06001A19 RID: 6681 RVA: 0x0004CF5B File Offset: 0x0004B15B
			public override bool Equals(object obj)
			{
				BindingRestrictions.CustomRestriction customRestriction = obj as BindingRestrictions.CustomRestriction;
				return ((customRestriction != null) ? customRestriction._expression : null) == this._expression;
			}

			// Token: 0x06001A1A RID: 6682 RVA: 0x0004CF77 File Offset: 0x0004B177
			public override int GetHashCode()
			{
				return 613566756 ^ this._expression.GetHashCode();
			}

			// Token: 0x06001A1B RID: 6683 RVA: 0x0004CF8A File Offset: 0x0004B18A
			internal override Expression GetExpression()
			{
				return this._expression;
			}

			// Token: 0x04000C43 RID: 3139
			private readonly Expression _expression;
		}

		// Token: 0x0200043C RID: 1084
		private sealed class TypeRestriction : BindingRestrictions
		{
			// Token: 0x06001A1C RID: 6684 RVA: 0x0004CF92 File Offset: 0x0004B192
			internal TypeRestriction(Expression parameter, Type type)
			{
				this._expression = parameter;
				this._type = type;
			}

			// Token: 0x06001A1D RID: 6685 RVA: 0x0004CFA8 File Offset: 0x0004B1A8
			public override bool Equals(object obj)
			{
				BindingRestrictions.TypeRestriction typeRestriction = obj as BindingRestrictions.TypeRestriction;
				return ((typeRestriction != null) ? typeRestriction._expression : null) == this._expression && TypeUtils.AreEquivalent(typeRestriction._type, this._type);
			}

			// Token: 0x06001A1E RID: 6686 RVA: 0x0004CFE3 File Offset: 0x0004B1E3
			public override int GetHashCode()
			{
				return 1227133513 ^ this._expression.GetHashCode() ^ this._type.GetHashCode();
			}

			// Token: 0x06001A1F RID: 6687 RVA: 0x0004D002 File Offset: 0x0004B202
			internal override Expression GetExpression()
			{
				return Expression.TypeEqual(this._expression, this._type);
			}

			// Token: 0x04000C44 RID: 3140
			private readonly Expression _expression;

			// Token: 0x04000C45 RID: 3141
			private readonly Type _type;
		}

		// Token: 0x0200043D RID: 1085
		private sealed class InstanceRestriction : BindingRestrictions
		{
			// Token: 0x06001A20 RID: 6688 RVA: 0x0004D015 File Offset: 0x0004B215
			internal InstanceRestriction(Expression parameter, object instance)
			{
				this._expression = parameter;
				this._instance = instance;
			}

			// Token: 0x06001A21 RID: 6689 RVA: 0x0004D02C File Offset: 0x0004B22C
			public override bool Equals(object obj)
			{
				BindingRestrictions.InstanceRestriction instanceRestriction = obj as BindingRestrictions.InstanceRestriction;
				return ((instanceRestriction != null) ? instanceRestriction._expression : null) == this._expression && instanceRestriction._instance == this._instance;
			}

			// Token: 0x06001A22 RID: 6690 RVA: 0x0004D064 File Offset: 0x0004B264
			public override int GetHashCode()
			{
				return -1840700270 ^ RuntimeHelpers.GetHashCode(this._instance) ^ this._expression.GetHashCode();
			}

			// Token: 0x06001A23 RID: 6691 RVA: 0x0004D084 File Offset: 0x0004B284
			internal override Expression GetExpression()
			{
				if (this._instance == null)
				{
					return Expression.Equal(Expression.Convert(this._expression, typeof(object)), Utils.Null);
				}
				ParameterExpression parameterExpression = Expression.Parameter(typeof(object), null);
				return Expression.Block(new TrueReadOnlyCollection<ParameterExpression>(new ParameterExpression[]
				{
					parameterExpression
				}), new TrueReadOnlyCollection<Expression>(new Expression[]
				{
					Expression.Assign(parameterExpression, Expression.Constant(this._instance, typeof(object))),
					Expression.AndAlso(Expression.NotEqual(parameterExpression, Utils.Null), Expression.Equal(Expression.Convert(this._expression, typeof(object)), parameterExpression))
				}));
			}

			// Token: 0x04000C46 RID: 3142
			private readonly Expression _expression;

			// Token: 0x04000C47 RID: 3143
			private readonly object _instance;
		}

		// Token: 0x0200043E RID: 1086
		private sealed class BindingRestrictionsProxy
		{
			// Token: 0x06001A24 RID: 6692 RVA: 0x0004D135 File Offset: 0x0004B335
			public BindingRestrictionsProxy(BindingRestrictions node)
			{
				ContractUtils.RequiresNotNull(node, "node");
				this._node = node;
			}

			// Token: 0x1700050A RID: 1290
			// (get) Token: 0x06001A25 RID: 6693 RVA: 0x0004D14F File Offset: 0x0004B34F
			public bool IsEmpty
			{
				get
				{
					return this._node == BindingRestrictions.Empty;
				}
			}

			// Token: 0x1700050B RID: 1291
			// (get) Token: 0x06001A26 RID: 6694 RVA: 0x0004D15E File Offset: 0x0004B35E
			public Expression Test
			{
				get
				{
					return this._node.ToExpression();
				}
			}

			// Token: 0x1700050C RID: 1292
			// (get) Token: 0x06001A27 RID: 6695 RVA: 0x0004D16C File Offset: 0x0004B36C
			public BindingRestrictions[] Restrictions
			{
				get
				{
					List<BindingRestrictions> list = new List<BindingRestrictions>();
					Stack<BindingRestrictions> stack = new Stack<BindingRestrictions>();
					BindingRestrictions bindingRestrictions = this._node;
					for (;;)
					{
						BindingRestrictions.MergedRestriction mergedRestriction = bindingRestrictions as BindingRestrictions.MergedRestriction;
						if (mergedRestriction != null)
						{
							stack.Push(mergedRestriction.Right);
							bindingRestrictions = mergedRestriction.Left;
						}
						else
						{
							list.Add(bindingRestrictions);
							if (stack.Count == 0)
							{
								break;
							}
							bindingRestrictions = stack.Pop();
						}
					}
					return list.ToArray();
				}
			}

			// Token: 0x06001A28 RID: 6696 RVA: 0x0004D1C9 File Offset: 0x0004B3C9
			public override string ToString()
			{
				return this._node.DebugView;
			}

			// Token: 0x04000C48 RID: 3144
			private readonly BindingRestrictions _node;
		}
	}
}
