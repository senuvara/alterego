﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Dynamic.Utils;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace System.Dynamic
{
	/// <summary>Represents an object whose members can be dynamically added and removed at run time.</summary>
	// Token: 0x02000454 RID: 1108
	public sealed class ExpandoObject : IDynamicMetaObjectProvider, IDictionary<string, object>, ICollection<KeyValuePair<string, object>>, IEnumerable<KeyValuePair<string, object>>, IEnumerable, INotifyPropertyChanged
	{
		/// <summary>Initializes a new <see langword="ExpandoObject" /> that does not have members.</summary>
		// Token: 0x06001ABF RID: 6847 RVA: 0x0004E744 File Offset: 0x0004C944
		public ExpandoObject()
		{
			this._data = ExpandoObject.ExpandoData.Empty;
			this.LockObject = new object();
		}

		// Token: 0x06001AC0 RID: 6848 RVA: 0x0004E764 File Offset: 0x0004C964
		internal bool TryGetValue(object indexClass, int index, string name, bool ignoreCase, out object value)
		{
			ExpandoObject.ExpandoData data = this._data;
			if (data.Class != indexClass || ignoreCase)
			{
				index = data.Class.GetValueIndex(name, ignoreCase, this);
				if (index == -2)
				{
					throw Error.AmbiguousMatchInExpandoObject(name);
				}
			}
			if (index == -1)
			{
				value = null;
				return false;
			}
			object obj = data[index];
			if (obj == ExpandoObject.Uninitialized)
			{
				value = null;
				return false;
			}
			value = obj;
			return true;
		}

		// Token: 0x06001AC1 RID: 6849 RVA: 0x0004E7CC File Offset: 0x0004C9CC
		internal void TrySetValue(object indexClass, int index, object value, string name, bool ignoreCase, bool add)
		{
			object lockObject = this.LockObject;
			ExpandoObject.ExpandoData expandoData;
			object obj;
			lock (lockObject)
			{
				expandoData = this._data;
				if (expandoData.Class != indexClass || ignoreCase)
				{
					index = expandoData.Class.GetValueIndex(name, ignoreCase, this);
					if (index == -2)
					{
						throw Error.AmbiguousMatchInExpandoObject(name);
					}
					if (index == -1)
					{
						int num = ignoreCase ? expandoData.Class.GetValueIndexCaseSensitive(name) : index;
						if (num != -1)
						{
							index = num;
						}
						else
						{
							ExpandoClass newClass = expandoData.Class.FindNewClass(name);
							expandoData = this.PromoteClassCore(expandoData.Class, newClass);
							index = expandoData.Class.GetValueIndexCaseSensitive(name);
						}
					}
				}
				obj = expandoData[index];
				if (obj == ExpandoObject.Uninitialized)
				{
					this._count++;
				}
				else if (add)
				{
					throw Error.SameKeyExistsInExpando(name);
				}
				expandoData[index] = value;
			}
			PropertyChangedEventHandler propertyChanged = this._propertyChanged;
			if (propertyChanged != null && value != obj)
			{
				propertyChanged(this, new PropertyChangedEventArgs(expandoData.Class.Keys[index]));
			}
		}

		// Token: 0x06001AC2 RID: 6850 RVA: 0x0004E8EC File Offset: 0x0004CAEC
		internal bool TryDeleteValue(object indexClass, int index, string name, bool ignoreCase, object deleteValue)
		{
			object lockObject = this.LockObject;
			ExpandoObject.ExpandoData data;
			lock (lockObject)
			{
				data = this._data;
				if (data.Class != indexClass || ignoreCase)
				{
					index = data.Class.GetValueIndex(name, ignoreCase, this);
					if (index == -2)
					{
						throw Error.AmbiguousMatchInExpandoObject(name);
					}
				}
				if (index == -1)
				{
					return false;
				}
				object obj = data[index];
				if (obj == ExpandoObject.Uninitialized)
				{
					return false;
				}
				if (deleteValue != ExpandoObject.Uninitialized && !object.Equals(obj, deleteValue))
				{
					return false;
				}
				data[index] = ExpandoObject.Uninitialized;
				this._count--;
			}
			PropertyChangedEventHandler propertyChanged = this._propertyChanged;
			if (propertyChanged != null)
			{
				propertyChanged(this, new PropertyChangedEventArgs(data.Class.Keys[index]));
			}
			return true;
		}

		// Token: 0x06001AC3 RID: 6851 RVA: 0x0004E9D4 File Offset: 0x0004CBD4
		internal bool IsDeletedMember(int index)
		{
			return index != this._data.Length && this._data[index] == ExpandoObject.Uninitialized;
		}

		// Token: 0x17000527 RID: 1319
		// (get) Token: 0x06001AC4 RID: 6852 RVA: 0x0004E9F9 File Offset: 0x0004CBF9
		internal ExpandoClass Class
		{
			get
			{
				return this._data.Class;
			}
		}

		// Token: 0x06001AC5 RID: 6853 RVA: 0x0004EA06 File Offset: 0x0004CC06
		private ExpandoObject.ExpandoData PromoteClassCore(ExpandoClass oldClass, ExpandoClass newClass)
		{
			if (this._data.Class == oldClass)
			{
				this._data = this._data.UpdateClass(newClass);
			}
			return this._data;
		}

		// Token: 0x06001AC6 RID: 6854 RVA: 0x0004EA30 File Offset: 0x0004CC30
		internal void PromoteClass(object oldClass, object newClass)
		{
			object lockObject = this.LockObject;
			lock (lockObject)
			{
				this.PromoteClassCore((ExpandoClass)oldClass, (ExpandoClass)newClass);
			}
		}

		/// <summary>The provided MetaObject will dispatch to the dynamic virtual methods. The object can be encapsulated inside another MetaObject to provide custom behavior for individual actions.</summary>
		/// <param name="parameter">The expression that represents the MetaObject to dispatch to the Dynamic virtual methods.</param>
		/// <returns>The object of the <see cref="T:System.Dynamic.DynamicMetaObject" /> type.</returns>
		// Token: 0x06001AC7 RID: 6855 RVA: 0x0004EA80 File Offset: 0x0004CC80
		DynamicMetaObject IDynamicMetaObjectProvider.GetMetaObject(Expression parameter)
		{
			return new ExpandoObject.MetaExpando(parameter, this);
		}

		// Token: 0x06001AC8 RID: 6856 RVA: 0x0004EA89 File Offset: 0x0004CC89
		private void TryAddMember(string key, object value)
		{
			ContractUtils.RequiresNotNull(key, "key");
			this.TrySetValue(null, -1, value, key, false, true);
		}

		// Token: 0x06001AC9 RID: 6857 RVA: 0x0004EAA2 File Offset: 0x0004CCA2
		private bool TryGetValueForKey(string key, out object value)
		{
			return this.TryGetValue(null, -1, key, false, out value);
		}

		// Token: 0x06001ACA RID: 6858 RVA: 0x0004EAAF File Offset: 0x0004CCAF
		private bool ExpandoContainsKey(string key)
		{
			return this._data.Class.GetValueIndexCaseSensitive(key) >= 0;
		}

		// Token: 0x17000528 RID: 1320
		// (get) Token: 0x06001ACB RID: 6859 RVA: 0x0004EAC8 File Offset: 0x0004CCC8
		ICollection<string> IDictionary<string, object>.Keys
		{
			get
			{
				return new ExpandoObject.KeyCollection(this);
			}
		}

		// Token: 0x17000529 RID: 1321
		// (get) Token: 0x06001ACC RID: 6860 RVA: 0x0004EAD0 File Offset: 0x0004CCD0
		ICollection<object> IDictionary<string, object>.Values
		{
			get
			{
				return new ExpandoObject.ValueCollection(this);
			}
		}

		// Token: 0x1700052A RID: 1322
		object IDictionary<string, object>.this[string key]
		{
			get
			{
				object result;
				if (!this.TryGetValueForKey(key, out result))
				{
					throw Error.KeyDoesNotExistInExpando(key);
				}
				return result;
			}
			set
			{
				ContractUtils.RequiresNotNull(key, "key");
				this.TrySetValue(null, -1, value, key, false, false);
			}
		}

		// Token: 0x06001ACF RID: 6863 RVA: 0x0004EB11 File Offset: 0x0004CD11
		void IDictionary<string, object>.Add(string key, object value)
		{
			this.TryAddMember(key, value);
		}

		// Token: 0x06001AD0 RID: 6864 RVA: 0x0004EB1C File Offset: 0x0004CD1C
		bool IDictionary<string, object>.ContainsKey(string key)
		{
			ContractUtils.RequiresNotNull(key, "key");
			ExpandoObject.ExpandoData data = this._data;
			int valueIndexCaseSensitive = data.Class.GetValueIndexCaseSensitive(key);
			return valueIndexCaseSensitive >= 0 && data[valueIndexCaseSensitive] != ExpandoObject.Uninitialized;
		}

		// Token: 0x06001AD1 RID: 6865 RVA: 0x0004EB5F File Offset: 0x0004CD5F
		bool IDictionary<string, object>.Remove(string key)
		{
			ContractUtils.RequiresNotNull(key, "key");
			return this.TryDeleteValue(null, -1, key, false, ExpandoObject.Uninitialized);
		}

		// Token: 0x06001AD2 RID: 6866 RVA: 0x0004EB7B File Offset: 0x0004CD7B
		bool IDictionary<string, object>.TryGetValue(string key, out object value)
		{
			return this.TryGetValueForKey(key, out value);
		}

		// Token: 0x1700052B RID: 1323
		// (get) Token: 0x06001AD3 RID: 6867 RVA: 0x0004EB85 File Offset: 0x0004CD85
		int ICollection<KeyValuePair<string, object>>.Count
		{
			get
			{
				return this._count;
			}
		}

		// Token: 0x1700052C RID: 1324
		// (get) Token: 0x06001AD4 RID: 6868 RVA: 0x00002285 File Offset: 0x00000485
		bool ICollection<KeyValuePair<string, object>>.IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06001AD5 RID: 6869 RVA: 0x0004EB8D File Offset: 0x0004CD8D
		void ICollection<KeyValuePair<string, object>>.Add(KeyValuePair<string, object> item)
		{
			this.TryAddMember(item.Key, item.Value);
		}

		// Token: 0x06001AD6 RID: 6870 RVA: 0x0004EBA4 File Offset: 0x0004CDA4
		void ICollection<KeyValuePair<string, object>>.Clear()
		{
			object lockObject = this.LockObject;
			ExpandoObject.ExpandoData data;
			lock (lockObject)
			{
				data = this._data;
				this._data = ExpandoObject.ExpandoData.Empty;
				this._count = 0;
			}
			PropertyChangedEventHandler propertyChanged = this._propertyChanged;
			if (propertyChanged != null)
			{
				int i = 0;
				int num = data.Class.Keys.Length;
				while (i < num)
				{
					if (data[i] != ExpandoObject.Uninitialized)
					{
						propertyChanged(this, new PropertyChangedEventArgs(data.Class.Keys[i]));
					}
					i++;
				}
			}
		}

		// Token: 0x06001AD7 RID: 6871 RVA: 0x0004EC4C File Offset: 0x0004CE4C
		bool ICollection<KeyValuePair<string, object>>.Contains(KeyValuePair<string, object> item)
		{
			object objA;
			return this.TryGetValueForKey(item.Key, out objA) && object.Equals(objA, item.Value);
		}

		// Token: 0x06001AD8 RID: 6872 RVA: 0x0004EC7C File Offset: 0x0004CE7C
		void ICollection<KeyValuePair<string, object>>.CopyTo(KeyValuePair<string, object>[] array, int arrayIndex)
		{
			ContractUtils.RequiresNotNull(array, "array");
			object lockObject = this.LockObject;
			lock (lockObject)
			{
				ContractUtils.RequiresArrayRange<KeyValuePair<string, object>>(array, arrayIndex, this._count, "arrayIndex", "Count");
				foreach (KeyValuePair<string, object> keyValuePair in ((IEnumerable<KeyValuePair<string, object>>)this))
				{
					array[arrayIndex++] = keyValuePair;
				}
			}
		}

		// Token: 0x06001AD9 RID: 6873 RVA: 0x0004ED14 File Offset: 0x0004CF14
		bool ICollection<KeyValuePair<string, object>>.Remove(KeyValuePair<string, object> item)
		{
			return this.TryDeleteValue(null, -1, item.Key, false, item.Value);
		}

		// Token: 0x06001ADA RID: 6874 RVA: 0x0004ED30 File Offset: 0x0004CF30
		IEnumerator<KeyValuePair<string, object>> IEnumerable<KeyValuePair<string, object>>.GetEnumerator()
		{
			ExpandoObject.ExpandoData data = this._data;
			return this.GetExpandoEnumerator(data, data.Version);
		}

		/// <summary>Returns an enumerator that iterates through the collection.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> that can be used to iterate through the collection.</returns>
		// Token: 0x06001ADB RID: 6875 RVA: 0x0004ED54 File Offset: 0x0004CF54
		IEnumerator IEnumerable.GetEnumerator()
		{
			ExpandoObject.ExpandoData data = this._data;
			return this.GetExpandoEnumerator(data, data.Version);
		}

		// Token: 0x06001ADC RID: 6876 RVA: 0x0004ED75 File Offset: 0x0004CF75
		private IEnumerator<KeyValuePair<string, object>> GetExpandoEnumerator(ExpandoObject.ExpandoData data, int version)
		{
			int num;
			for (int i = 0; i < data.Class.Keys.Length; i = num + 1)
			{
				if (this._data.Version != version || data != this._data)
				{
					throw Error.CollectionModifiedWhileEnumerating();
				}
				object obj = data[i];
				if (obj != ExpandoObject.Uninitialized)
				{
					yield return new KeyValuePair<string, object>(data.Class.Keys[i], obj);
				}
				num = i;
			}
			yield break;
		}

		/// <summary>Occurs when a property value changes.</summary>
		// Token: 0x14000001 RID: 1
		// (add) Token: 0x06001ADD RID: 6877 RVA: 0x0004ED92 File Offset: 0x0004CF92
		// (remove) Token: 0x06001ADE RID: 6878 RVA: 0x0004EDAB File Offset: 0x0004CFAB
		event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
		{
			add
			{
				this._propertyChanged = (PropertyChangedEventHandler)Delegate.Combine(this._propertyChanged, value);
			}
			remove
			{
				this._propertyChanged = (PropertyChangedEventHandler)Delegate.Remove(this._propertyChanged, value);
			}
		}

		// Token: 0x06001ADF RID: 6879 RVA: 0x0004EDC4 File Offset: 0x0004CFC4
		// Note: this type is marked as 'beforefieldinit'.
		static ExpandoObject()
		{
		}

		// Token: 0x04000C6B RID: 3179
		private static readonly MethodInfo ExpandoTryGetValue = typeof(RuntimeOps).GetMethod("ExpandoTryGetValue");

		// Token: 0x04000C6C RID: 3180
		private static readonly MethodInfo ExpandoTrySetValue = typeof(RuntimeOps).GetMethod("ExpandoTrySetValue");

		// Token: 0x04000C6D RID: 3181
		private static readonly MethodInfo ExpandoTryDeleteValue = typeof(RuntimeOps).GetMethod("ExpandoTryDeleteValue");

		// Token: 0x04000C6E RID: 3182
		private static readonly MethodInfo ExpandoPromoteClass = typeof(RuntimeOps).GetMethod("ExpandoPromoteClass");

		// Token: 0x04000C6F RID: 3183
		private static readonly MethodInfo ExpandoCheckVersion = typeof(RuntimeOps).GetMethod("ExpandoCheckVersion");

		// Token: 0x04000C70 RID: 3184
		internal readonly object LockObject;

		// Token: 0x04000C71 RID: 3185
		private ExpandoObject.ExpandoData _data;

		// Token: 0x04000C72 RID: 3186
		private int _count;

		// Token: 0x04000C73 RID: 3187
		internal static readonly object Uninitialized = new object();

		// Token: 0x04000C74 RID: 3188
		internal const int AmbiguousMatchFound = -2;

		// Token: 0x04000C75 RID: 3189
		internal const int NoMatch = -1;

		// Token: 0x04000C76 RID: 3190
		private PropertyChangedEventHandler _propertyChanged;

		// Token: 0x02000455 RID: 1109
		private sealed class KeyCollectionDebugView
		{
			// Token: 0x06001AE0 RID: 6880 RVA: 0x0004EE58 File Offset: 0x0004D058
			public KeyCollectionDebugView(ICollection<string> collection)
			{
				ContractUtils.RequiresNotNull(collection, "collection");
				this._collection = collection;
			}

			// Token: 0x1700052D RID: 1325
			// (get) Token: 0x06001AE1 RID: 6881 RVA: 0x0004EE74 File Offset: 0x0004D074
			[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
			public string[] Items
			{
				get
				{
					string[] array = new string[this._collection.Count];
					this._collection.CopyTo(array, 0);
					return array;
				}
			}

			// Token: 0x04000C77 RID: 3191
			private readonly ICollection<string> _collection;
		}

		// Token: 0x02000456 RID: 1110
		[DebuggerTypeProxy(typeof(ExpandoObject.KeyCollectionDebugView))]
		[DebuggerDisplay("Count = {Count}")]
		private class KeyCollection : ICollection<string>, IEnumerable<string>, IEnumerable
		{
			// Token: 0x06001AE2 RID: 6882 RVA: 0x0004EEA0 File Offset: 0x0004D0A0
			internal KeyCollection(ExpandoObject expando)
			{
				object lockObject = expando.LockObject;
				lock (lockObject)
				{
					this._expando = expando;
					this._expandoVersion = expando._data.Version;
					this._expandoCount = expando._count;
					this._expandoData = expando._data;
				}
			}

			// Token: 0x06001AE3 RID: 6883 RVA: 0x0004EF10 File Offset: 0x0004D110
			private void CheckVersion()
			{
				if (this._expando._data.Version != this._expandoVersion || this._expandoData != this._expando._data)
				{
					throw Error.CollectionModifiedWhileEnumerating();
				}
			}

			// Token: 0x06001AE4 RID: 6884 RVA: 0x0004EF43 File Offset: 0x0004D143
			public void Add(string item)
			{
				throw Error.CollectionReadOnly();
			}

			// Token: 0x06001AE5 RID: 6885 RVA: 0x0004EF43 File Offset: 0x0004D143
			public void Clear()
			{
				throw Error.CollectionReadOnly();
			}

			// Token: 0x06001AE6 RID: 6886 RVA: 0x0004EF4C File Offset: 0x0004D14C
			public bool Contains(string item)
			{
				object lockObject = this._expando.LockObject;
				bool result;
				lock (lockObject)
				{
					this.CheckVersion();
					result = this._expando.ExpandoContainsKey(item);
				}
				return result;
			}

			// Token: 0x06001AE7 RID: 6887 RVA: 0x0004EFA0 File Offset: 0x0004D1A0
			public void CopyTo(string[] array, int arrayIndex)
			{
				ContractUtils.RequiresNotNull(array, "array");
				ContractUtils.RequiresArrayRange<string>(array, arrayIndex, this._expandoCount, "arrayIndex", "Count");
				object lockObject = this._expando.LockObject;
				lock (lockObject)
				{
					this.CheckVersion();
					ExpandoObject.ExpandoData data = this._expando._data;
					for (int i = 0; i < data.Class.Keys.Length; i++)
					{
						if (data[i] != ExpandoObject.Uninitialized)
						{
							array[arrayIndex++] = data.Class.Keys[i];
						}
					}
				}
			}

			// Token: 0x1700052E RID: 1326
			// (get) Token: 0x06001AE8 RID: 6888 RVA: 0x0004F050 File Offset: 0x0004D250
			public int Count
			{
				get
				{
					this.CheckVersion();
					return this._expandoCount;
				}
			}

			// Token: 0x1700052F RID: 1327
			// (get) Token: 0x06001AE9 RID: 6889 RVA: 0x00009CDF File Offset: 0x00007EDF
			public bool IsReadOnly
			{
				get
				{
					return true;
				}
			}

			// Token: 0x06001AEA RID: 6890 RVA: 0x0004EF43 File Offset: 0x0004D143
			public bool Remove(string item)
			{
				throw Error.CollectionReadOnly();
			}

			// Token: 0x06001AEB RID: 6891 RVA: 0x0004F05E File Offset: 0x0004D25E
			public IEnumerator<string> GetEnumerator()
			{
				int i = 0;
				int j = this._expandoData.Class.Keys.Length;
				while (i < j)
				{
					this.CheckVersion();
					if (this._expandoData[i] != ExpandoObject.Uninitialized)
					{
						yield return this._expandoData.Class.Keys[i];
					}
					int num = i;
					i = num + 1;
				}
				yield break;
			}

			// Token: 0x06001AEC RID: 6892 RVA: 0x0004F06D File Offset: 0x0004D26D
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			// Token: 0x04000C78 RID: 3192
			private readonly ExpandoObject _expando;

			// Token: 0x04000C79 RID: 3193
			private readonly int _expandoVersion;

			// Token: 0x04000C7A RID: 3194
			private readonly int _expandoCount;

			// Token: 0x04000C7B RID: 3195
			private readonly ExpandoObject.ExpandoData _expandoData;

			// Token: 0x02000457 RID: 1111
			[CompilerGenerated]
			private sealed class <GetEnumerator>d__15 : IEnumerator<string>, IDisposable, IEnumerator
			{
				// Token: 0x06001AED RID: 6893 RVA: 0x0004F075 File Offset: 0x0004D275
				[DebuggerHidden]
				public <GetEnumerator>d__15(int <>1__state)
				{
					this.<>1__state = <>1__state;
				}

				// Token: 0x06001AEE RID: 6894 RVA: 0x000039E8 File Offset: 0x00001BE8
				[DebuggerHidden]
				void IDisposable.Dispose()
				{
				}

				// Token: 0x06001AEF RID: 6895 RVA: 0x0004F084 File Offset: 0x0004D284
				bool IEnumerator.MoveNext()
				{
					int num = this.<>1__state;
					ExpandoObject.KeyCollection keyCollection = this;
					if (num == 0)
					{
						this.<>1__state = -1;
						i = 0;
						j = keyCollection._expandoData.Class.Keys.Length;
						goto IL_9A;
					}
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
					IL_8A:
					int num2 = i;
					i = num2 + 1;
					IL_9A:
					if (i >= j)
					{
						return false;
					}
					keyCollection.CheckVersion();
					if (keyCollection._expandoData[i] != ExpandoObject.Uninitialized)
					{
						this.<>2__current = keyCollection._expandoData.Class.Keys[i];
						this.<>1__state = 1;
						return true;
					}
					goto IL_8A;
				}

				// Token: 0x17000530 RID: 1328
				// (get) Token: 0x06001AF0 RID: 6896 RVA: 0x0004F13A File Offset: 0x0004D33A
				string IEnumerator<string>.Current
				{
					[DebuggerHidden]
					get
					{
						return this.<>2__current;
					}
				}

				// Token: 0x06001AF1 RID: 6897 RVA: 0x00003A6B File Offset: 0x00001C6B
				[DebuggerHidden]
				void IEnumerator.Reset()
				{
					throw new NotSupportedException();
				}

				// Token: 0x17000531 RID: 1329
				// (get) Token: 0x06001AF2 RID: 6898 RVA: 0x0004F13A File Offset: 0x0004D33A
				object IEnumerator.Current
				{
					[DebuggerHidden]
					get
					{
						return this.<>2__current;
					}
				}

				// Token: 0x04000C7C RID: 3196
				private int <>1__state;

				// Token: 0x04000C7D RID: 3197
				private string <>2__current;

				// Token: 0x04000C7E RID: 3198
				public ExpandoObject.KeyCollection <>4__this;

				// Token: 0x04000C7F RID: 3199
				private int <i>5__1;

				// Token: 0x04000C80 RID: 3200
				private int <n>5__2;
			}
		}

		// Token: 0x02000458 RID: 1112
		private sealed class ValueCollectionDebugView
		{
			// Token: 0x06001AF3 RID: 6899 RVA: 0x0004F142 File Offset: 0x0004D342
			public ValueCollectionDebugView(ICollection<object> collection)
			{
				ContractUtils.RequiresNotNull(collection, "collection");
				this._collection = collection;
			}

			// Token: 0x17000532 RID: 1330
			// (get) Token: 0x06001AF4 RID: 6900 RVA: 0x0004F15C File Offset: 0x0004D35C
			[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
			public object[] Items
			{
				get
				{
					object[] array = new object[this._collection.Count];
					this._collection.CopyTo(array, 0);
					return array;
				}
			}

			// Token: 0x04000C81 RID: 3201
			private readonly ICollection<object> _collection;
		}

		// Token: 0x02000459 RID: 1113
		[DebuggerDisplay("Count = {Count}")]
		[DebuggerTypeProxy(typeof(ExpandoObject.ValueCollectionDebugView))]
		private class ValueCollection : ICollection<object>, IEnumerable<object>, IEnumerable
		{
			// Token: 0x06001AF5 RID: 6901 RVA: 0x0004F188 File Offset: 0x0004D388
			internal ValueCollection(ExpandoObject expando)
			{
				object lockObject = expando.LockObject;
				lock (lockObject)
				{
					this._expando = expando;
					this._expandoVersion = expando._data.Version;
					this._expandoCount = expando._count;
					this._expandoData = expando._data;
				}
			}

			// Token: 0x06001AF6 RID: 6902 RVA: 0x0004F1F8 File Offset: 0x0004D3F8
			private void CheckVersion()
			{
				if (this._expando._data.Version != this._expandoVersion || this._expandoData != this._expando._data)
				{
					throw Error.CollectionModifiedWhileEnumerating();
				}
			}

			// Token: 0x06001AF7 RID: 6903 RVA: 0x0004EF43 File Offset: 0x0004D143
			public void Add(object item)
			{
				throw Error.CollectionReadOnly();
			}

			// Token: 0x06001AF8 RID: 6904 RVA: 0x0004EF43 File Offset: 0x0004D143
			public void Clear()
			{
				throw Error.CollectionReadOnly();
			}

			// Token: 0x06001AF9 RID: 6905 RVA: 0x0004F22C File Offset: 0x0004D42C
			public bool Contains(object item)
			{
				object lockObject = this._expando.LockObject;
				bool result;
				lock (lockObject)
				{
					this.CheckVersion();
					ExpandoObject.ExpandoData data = this._expando._data;
					for (int i = 0; i < data.Class.Keys.Length; i++)
					{
						if (object.Equals(data[i], item))
						{
							return true;
						}
					}
					result = false;
				}
				return result;
			}

			// Token: 0x06001AFA RID: 6906 RVA: 0x0004F2B0 File Offset: 0x0004D4B0
			public void CopyTo(object[] array, int arrayIndex)
			{
				ContractUtils.RequiresNotNull(array, "array");
				ContractUtils.RequiresArrayRange<object>(array, arrayIndex, this._expandoCount, "arrayIndex", "Count");
				object lockObject = this._expando.LockObject;
				lock (lockObject)
				{
					this.CheckVersion();
					ExpandoObject.ExpandoData data = this._expando._data;
					for (int i = 0; i < data.Class.Keys.Length; i++)
					{
						if (data[i] != ExpandoObject.Uninitialized)
						{
							array[arrayIndex++] = data[i];
						}
					}
				}
			}

			// Token: 0x17000533 RID: 1331
			// (get) Token: 0x06001AFB RID: 6907 RVA: 0x0004F358 File Offset: 0x0004D558
			public int Count
			{
				get
				{
					this.CheckVersion();
					return this._expandoCount;
				}
			}

			// Token: 0x17000534 RID: 1332
			// (get) Token: 0x06001AFC RID: 6908 RVA: 0x00009CDF File Offset: 0x00007EDF
			public bool IsReadOnly
			{
				get
				{
					return true;
				}
			}

			// Token: 0x06001AFD RID: 6909 RVA: 0x0004EF43 File Offset: 0x0004D143
			public bool Remove(object item)
			{
				throw Error.CollectionReadOnly();
			}

			// Token: 0x06001AFE RID: 6910 RVA: 0x0004F366 File Offset: 0x0004D566
			public IEnumerator<object> GetEnumerator()
			{
				ExpandoObject.ExpandoData data = this._expando._data;
				int num;
				for (int i = 0; i < data.Class.Keys.Length; i = num + 1)
				{
					this.CheckVersion();
					object obj = data[i];
					if (obj != ExpandoObject.Uninitialized)
					{
						yield return obj;
					}
					num = i;
				}
				yield break;
			}

			// Token: 0x06001AFF RID: 6911 RVA: 0x0004F375 File Offset: 0x0004D575
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			// Token: 0x04000C82 RID: 3202
			private readonly ExpandoObject _expando;

			// Token: 0x04000C83 RID: 3203
			private readonly int _expandoVersion;

			// Token: 0x04000C84 RID: 3204
			private readonly int _expandoCount;

			// Token: 0x04000C85 RID: 3205
			private readonly ExpandoObject.ExpandoData _expandoData;

			// Token: 0x0200045A RID: 1114
			[CompilerGenerated]
			private sealed class <GetEnumerator>d__15 : IEnumerator<object>, IDisposable, IEnumerator
			{
				// Token: 0x06001B00 RID: 6912 RVA: 0x0004F37D File Offset: 0x0004D57D
				[DebuggerHidden]
				public <GetEnumerator>d__15(int <>1__state)
				{
					this.<>1__state = <>1__state;
				}

				// Token: 0x06001B01 RID: 6913 RVA: 0x000039E8 File Offset: 0x00001BE8
				[DebuggerHidden]
				void IDisposable.Dispose()
				{
				}

				// Token: 0x06001B02 RID: 6914 RVA: 0x0004F38C File Offset: 0x0004D58C
				bool IEnumerator.MoveNext()
				{
					int num = this.<>1__state;
					ExpandoObject.ValueCollection valueCollection = this;
					if (num == 0)
					{
						this.<>1__state = -1;
						data = valueCollection._expando._data;
						i = 0;
						goto IL_7F;
					}
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
					IL_6F:
					int num2 = i;
					i = num2 + 1;
					IL_7F:
					if (i >= data.Class.Keys.Length)
					{
						return false;
					}
					valueCollection.CheckVersion();
					object obj = data[i];
					if (obj != ExpandoObject.Uninitialized)
					{
						this.<>2__current = obj;
						this.<>1__state = 1;
						return true;
					}
					goto IL_6F;
				}

				// Token: 0x17000535 RID: 1333
				// (get) Token: 0x06001B03 RID: 6915 RVA: 0x0004F433 File Offset: 0x0004D633
				object IEnumerator<object>.Current
				{
					[DebuggerHidden]
					get
					{
						return this.<>2__current;
					}
				}

				// Token: 0x06001B04 RID: 6916 RVA: 0x00003A6B File Offset: 0x00001C6B
				[DebuggerHidden]
				void IEnumerator.Reset()
				{
					throw new NotSupportedException();
				}

				// Token: 0x17000536 RID: 1334
				// (get) Token: 0x06001B05 RID: 6917 RVA: 0x0004F433 File Offset: 0x0004D633
				object IEnumerator.Current
				{
					[DebuggerHidden]
					get
					{
						return this.<>2__current;
					}
				}

				// Token: 0x04000C86 RID: 3206
				private int <>1__state;

				// Token: 0x04000C87 RID: 3207
				private object <>2__current;

				// Token: 0x04000C88 RID: 3208
				public ExpandoObject.ValueCollection <>4__this;

				// Token: 0x04000C89 RID: 3209
				private ExpandoObject.ExpandoData <data>5__1;

				// Token: 0x04000C8A RID: 3210
				private int <i>5__2;
			}
		}

		// Token: 0x0200045B RID: 1115
		private class MetaExpando : DynamicMetaObject
		{
			// Token: 0x06001B06 RID: 6918 RVA: 0x0004D917 File Offset: 0x0004BB17
			public MetaExpando(Expression expression, ExpandoObject value) : base(expression, BindingRestrictions.Empty, value)
			{
			}

			// Token: 0x06001B07 RID: 6919 RVA: 0x0004F43C File Offset: 0x0004D63C
			private DynamicMetaObject BindGetOrInvokeMember(DynamicMetaObjectBinder binder, string name, bool ignoreCase, DynamicMetaObject fallback, Func<DynamicMetaObject, DynamicMetaObject> fallbackInvoke)
			{
				ExpandoClass @class = this.Value.Class;
				int valueIndex = @class.GetValueIndex(name, ignoreCase, this.Value);
				ParameterExpression parameterExpression = Expression.Parameter(typeof(object), "value");
				Expression test = Expression.Call(ExpandoObject.ExpandoTryGetValue, new Expression[]
				{
					this.GetLimitedSelf(),
					Expression.Constant(@class, typeof(object)),
					Utils.Constant(valueIndex),
					Expression.Constant(name),
					Utils.Constant(ignoreCase),
					parameterExpression
				});
				DynamicMetaObject dynamicMetaObject = new DynamicMetaObject(parameterExpression, BindingRestrictions.Empty);
				if (fallbackInvoke != null)
				{
					dynamicMetaObject = fallbackInvoke(dynamicMetaObject);
				}
				dynamicMetaObject = new DynamicMetaObject(Expression.Block(new TrueReadOnlyCollection<ParameterExpression>(new ParameterExpression[]
				{
					parameterExpression
				}), new TrueReadOnlyCollection<Expression>(new Expression[]
				{
					Expression.Condition(test, dynamicMetaObject.Expression, fallback.Expression, typeof(object))
				})), dynamicMetaObject.Restrictions.Merge(fallback.Restrictions));
				return this.AddDynamicTestAndDefer(binder, this.Value.Class, null, dynamicMetaObject);
			}

			// Token: 0x06001B08 RID: 6920 RVA: 0x0004F550 File Offset: 0x0004D750
			public override DynamicMetaObject BindGetMember(GetMemberBinder binder)
			{
				ContractUtils.RequiresNotNull(binder, "binder");
				return this.BindGetOrInvokeMember(binder, binder.Name, binder.IgnoreCase, binder.FallbackGetMember(this), null);
			}

			// Token: 0x06001B09 RID: 6921 RVA: 0x0004F578 File Offset: 0x0004D778
			public override DynamicMetaObject BindInvokeMember(InvokeMemberBinder binder, DynamicMetaObject[] args)
			{
				ContractUtils.RequiresNotNull(binder, "binder");
				return this.BindGetOrInvokeMember(binder, binder.Name, binder.IgnoreCase, binder.FallbackInvokeMember(this, args), (DynamicMetaObject value) => binder.FallbackInvoke(value, args, null));
			}

			// Token: 0x06001B0A RID: 6922 RVA: 0x0004F5EC File Offset: 0x0004D7EC
			public override DynamicMetaObject BindSetMember(SetMemberBinder binder, DynamicMetaObject value)
			{
				ContractUtils.RequiresNotNull(binder, "binder");
				ContractUtils.RequiresNotNull(value, "value");
				ExpandoClass expandoClass;
				int value2;
				ExpandoClass classEnsureIndex = this.GetClassEnsureIndex(binder.Name, binder.IgnoreCase, this.Value, out expandoClass, out value2);
				return this.AddDynamicTestAndDefer(binder, expandoClass, classEnsureIndex, new DynamicMetaObject(Expression.Call(ExpandoObject.ExpandoTrySetValue, new Expression[]
				{
					this.GetLimitedSelf(),
					Expression.Constant(expandoClass, typeof(object)),
					Utils.Constant(value2),
					Expression.Convert(value.Expression, typeof(object)),
					Expression.Constant(binder.Name),
					Utils.Constant(binder.IgnoreCase)
				}), BindingRestrictions.Empty));
			}

			// Token: 0x06001B0B RID: 6923 RVA: 0x0004F6A8 File Offset: 0x0004D8A8
			public override DynamicMetaObject BindDeleteMember(DeleteMemberBinder binder)
			{
				ContractUtils.RequiresNotNull(binder, "binder");
				int valueIndex = this.Value.Class.GetValueIndex(binder.Name, binder.IgnoreCase, this.Value);
				Expression expression = Expression.Call(ExpandoObject.ExpandoTryDeleteValue, this.GetLimitedSelf(), Expression.Constant(this.Value.Class, typeof(object)), Utils.Constant(valueIndex), Expression.Constant(binder.Name), Utils.Constant(binder.IgnoreCase));
				DynamicMetaObject dynamicMetaObject = binder.FallbackDeleteMember(this);
				DynamicMetaObject succeeds = new DynamicMetaObject(Expression.IfThen(Expression.Not(expression), dynamicMetaObject.Expression), dynamicMetaObject.Restrictions);
				return this.AddDynamicTestAndDefer(binder, this.Value.Class, null, succeeds);
			}

			// Token: 0x06001B0C RID: 6924 RVA: 0x0004F761 File Offset: 0x0004D961
			public override IEnumerable<string> GetDynamicMemberNames()
			{
				ExpandoObject.ExpandoData expandoData = this.Value._data;
				ExpandoClass klass = expandoData.Class;
				int num;
				for (int i = 0; i < klass.Keys.Length; i = num + 1)
				{
					if (expandoData[i] != ExpandoObject.Uninitialized)
					{
						yield return klass.Keys[i];
					}
					num = i;
				}
				yield break;
			}

			// Token: 0x06001B0D RID: 6925 RVA: 0x0004F774 File Offset: 0x0004D974
			private DynamicMetaObject AddDynamicTestAndDefer(DynamicMetaObjectBinder binder, ExpandoClass klass, ExpandoClass originalClass, DynamicMetaObject succeeds)
			{
				Expression expression = succeeds.Expression;
				if (originalClass != null)
				{
					expression = Expression.Block(Expression.Call(null, ExpandoObject.ExpandoPromoteClass, this.GetLimitedSelf(), Expression.Constant(originalClass, typeof(object)), Expression.Constant(klass, typeof(object))), succeeds.Expression);
				}
				return new DynamicMetaObject(Expression.Condition(Expression.Call(null, ExpandoObject.ExpandoCheckVersion, this.GetLimitedSelf(), Expression.Constant(originalClass ?? klass, typeof(object))), expression, binder.GetUpdateExpression(expression.Type)), this.GetRestrictions().Merge(succeeds.Restrictions));
			}

			// Token: 0x06001B0E RID: 6926 RVA: 0x0004F81C File Offset: 0x0004DA1C
			private ExpandoClass GetClassEnsureIndex(string name, bool caseInsensitive, ExpandoObject obj, out ExpandoClass klass, out int index)
			{
				ExpandoClass @class = this.Value.Class;
				index = @class.GetValueIndex(name, caseInsensitive, obj);
				if (index == -2)
				{
					klass = @class;
					return null;
				}
				if (index == -1)
				{
					ExpandoClass expandoClass = @class.FindNewClass(name);
					klass = expandoClass;
					index = expandoClass.GetValueIndexCaseSensitive(name);
					return @class;
				}
				klass = @class;
				return null;
			}

			// Token: 0x06001B0F RID: 6927 RVA: 0x0004F871 File Offset: 0x0004DA71
			private Expression GetLimitedSelf()
			{
				if (TypeUtils.AreEquivalent(base.Expression.Type, base.LimitType))
				{
					return base.Expression;
				}
				return Expression.Convert(base.Expression, base.LimitType);
			}

			// Token: 0x06001B10 RID: 6928 RVA: 0x0004E3BE File Offset: 0x0004C5BE
			private BindingRestrictions GetRestrictions()
			{
				return BindingRestrictions.GetTypeRestriction(this);
			}

			// Token: 0x17000537 RID: 1335
			// (get) Token: 0x06001B11 RID: 6929 RVA: 0x0004F8A3 File Offset: 0x0004DAA3
			public new ExpandoObject Value
			{
				get
				{
					return (ExpandoObject)base.Value;
				}
			}

			// Token: 0x0200045C RID: 1116
			[CompilerGenerated]
			private sealed class <>c__DisplayClass3_0
			{
				// Token: 0x06001B12 RID: 6930 RVA: 0x00002310 File Offset: 0x00000510
				public <>c__DisplayClass3_0()
				{
				}

				// Token: 0x06001B13 RID: 6931 RVA: 0x0004F8B0 File Offset: 0x0004DAB0
				internal DynamicMetaObject <BindInvokeMember>b__0(DynamicMetaObject value)
				{
					return this.binder.FallbackInvoke(value, this.args, null);
				}

				// Token: 0x04000C8B RID: 3211
				public InvokeMemberBinder binder;

				// Token: 0x04000C8C RID: 3212
				public DynamicMetaObject[] args;
			}

			// Token: 0x0200045D RID: 1117
			[CompilerGenerated]
			private sealed class <GetDynamicMemberNames>d__6 : IEnumerable<string>, IEnumerable, IEnumerator<string>, IDisposable, IEnumerator
			{
				// Token: 0x06001B14 RID: 6932 RVA: 0x0004F8C5 File Offset: 0x0004DAC5
				[DebuggerHidden]
				public <GetDynamicMemberNames>d__6(int <>1__state)
				{
					this.<>1__state = <>1__state;
					this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
				}

				// Token: 0x06001B15 RID: 6933 RVA: 0x000039E8 File Offset: 0x00001BE8
				[DebuggerHidden]
				void IDisposable.Dispose()
				{
				}

				// Token: 0x06001B16 RID: 6934 RVA: 0x0004F8E0 File Offset: 0x0004DAE0
				bool IEnumerator.MoveNext()
				{
					int num = this.<>1__state;
					ExpandoObject.MetaExpando metaExpando = this;
					if (num == 0)
					{
						this.<>1__state = -1;
						expandoData = metaExpando.Value._data;
						klass = expandoData.Class;
						i = 0;
						goto IL_99;
					}
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
					IL_89:
					int num2 = i;
					i = num2 + 1;
					IL_99:
					if (i >= klass.Keys.Length)
					{
						return false;
					}
					if (expandoData[i] != ExpandoObject.Uninitialized)
					{
						this.<>2__current = klass.Keys[i];
						this.<>1__state = 1;
						return true;
					}
					goto IL_89;
				}

				// Token: 0x17000538 RID: 1336
				// (get) Token: 0x06001B17 RID: 6935 RVA: 0x0004F99C File Offset: 0x0004DB9C
				string IEnumerator<string>.Current
				{
					[DebuggerHidden]
					get
					{
						return this.<>2__current;
					}
				}

				// Token: 0x06001B18 RID: 6936 RVA: 0x00003A6B File Offset: 0x00001C6B
				[DebuggerHidden]
				void IEnumerator.Reset()
				{
					throw new NotSupportedException();
				}

				// Token: 0x17000539 RID: 1337
				// (get) Token: 0x06001B19 RID: 6937 RVA: 0x0004F99C File Offset: 0x0004DB9C
				object IEnumerator.Current
				{
					[DebuggerHidden]
					get
					{
						return this.<>2__current;
					}
				}

				// Token: 0x06001B1A RID: 6938 RVA: 0x0004F9A4 File Offset: 0x0004DBA4
				[DebuggerHidden]
				IEnumerator<string> IEnumerable<string>.GetEnumerator()
				{
					ExpandoObject.MetaExpando.<GetDynamicMemberNames>d__6 <GetDynamicMemberNames>d__;
					if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
					{
						this.<>1__state = 0;
						<GetDynamicMemberNames>d__ = this;
					}
					else
					{
						<GetDynamicMemberNames>d__ = new ExpandoObject.MetaExpando.<GetDynamicMemberNames>d__6(0);
						<GetDynamicMemberNames>d__.<>4__this = this;
					}
					return <GetDynamicMemberNames>d__;
				}

				// Token: 0x06001B1B RID: 6939 RVA: 0x0004F9E7 File Offset: 0x0004DBE7
				[DebuggerHidden]
				IEnumerator IEnumerable.GetEnumerator()
				{
					return this.System.Collections.Generic.IEnumerable<System.String>.GetEnumerator();
				}

				// Token: 0x04000C8D RID: 3213
				private int <>1__state;

				// Token: 0x04000C8E RID: 3214
				private string <>2__current;

				// Token: 0x04000C8F RID: 3215
				private int <>l__initialThreadId;

				// Token: 0x04000C90 RID: 3216
				public ExpandoObject.MetaExpando <>4__this;

				// Token: 0x04000C91 RID: 3217
				private ExpandoObject.ExpandoData <expandoData>5__1;

				// Token: 0x04000C92 RID: 3218
				private ExpandoClass <klass>5__2;

				// Token: 0x04000C93 RID: 3219
				private int <i>5__3;
			}
		}

		// Token: 0x0200045E RID: 1118
		private class ExpandoData
		{
			// Token: 0x1700053A RID: 1338
			internal object this[int index]
			{
				get
				{
					return this._dataArray[index];
				}
				set
				{
					this._version++;
					this._dataArray[index] = value;
				}
			}

			// Token: 0x1700053B RID: 1339
			// (get) Token: 0x06001B1E RID: 6942 RVA: 0x0004FA12 File Offset: 0x0004DC12
			internal int Version
			{
				get
				{
					return this._version;
				}
			}

			// Token: 0x1700053C RID: 1340
			// (get) Token: 0x06001B1F RID: 6943 RVA: 0x0004FA1A File Offset: 0x0004DC1A
			internal int Length
			{
				get
				{
					return this._dataArray.Length;
				}
			}

			// Token: 0x06001B20 RID: 6944 RVA: 0x0004FA24 File Offset: 0x0004DC24
			private ExpandoData()
			{
				this.Class = ExpandoClass.Empty;
				this._dataArray = Array.Empty<object>();
			}

			// Token: 0x06001B21 RID: 6945 RVA: 0x0004FA42 File Offset: 0x0004DC42
			internal ExpandoData(ExpandoClass klass, object[] data, int version)
			{
				this.Class = klass;
				this._dataArray = data;
				this._version = version;
			}

			// Token: 0x06001B22 RID: 6946 RVA: 0x0004FA60 File Offset: 0x0004DC60
			internal ExpandoObject.ExpandoData UpdateClass(ExpandoClass newClass)
			{
				if (this._dataArray.Length >= newClass.Keys.Length)
				{
					this[newClass.Keys.Length - 1] = ExpandoObject.Uninitialized;
					return new ExpandoObject.ExpandoData(newClass, this._dataArray, this._version);
				}
				int index = this._dataArray.Length;
				object[] array = new object[ExpandoObject.ExpandoData.GetAlignedSize(newClass.Keys.Length)];
				Array.Copy(this._dataArray, 0, array, 0, this._dataArray.Length);
				ExpandoObject.ExpandoData expandoData = new ExpandoObject.ExpandoData(newClass, array, this._version);
				expandoData[index] = ExpandoObject.Uninitialized;
				return expandoData;
			}

			// Token: 0x06001B23 RID: 6947 RVA: 0x0004FAF2 File Offset: 0x0004DCF2
			private static int GetAlignedSize(int len)
			{
				return len + 7 & -8;
			}

			// Token: 0x06001B24 RID: 6948 RVA: 0x0004FAFA File Offset: 0x0004DCFA
			// Note: this type is marked as 'beforefieldinit'.
			static ExpandoData()
			{
			}

			// Token: 0x04000C94 RID: 3220
			internal static ExpandoObject.ExpandoData Empty = new ExpandoObject.ExpandoData();

			// Token: 0x04000C95 RID: 3221
			internal readonly ExpandoClass Class;

			// Token: 0x04000C96 RID: 3222
			private readonly object[] _dataArray;

			// Token: 0x04000C97 RID: 3223
			private int _version;
		}

		// Token: 0x0200045F RID: 1119
		[CompilerGenerated]
		private sealed class <GetExpandoEnumerator>d__51 : IEnumerator<KeyValuePair<string, object>>, IDisposable, IEnumerator
		{
			// Token: 0x06001B25 RID: 6949 RVA: 0x0004FB06 File Offset: 0x0004DD06
			[DebuggerHidden]
			public <GetExpandoEnumerator>d__51(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x06001B26 RID: 6950 RVA: 0x000039E8 File Offset: 0x00001BE8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06001B27 RID: 6951 RVA: 0x0004FB18 File Offset: 0x0004DD18
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				ExpandoObject expandoObject = this;
				if (num == 0)
				{
					this.<>1__state = -1;
					i = 0;
					goto IL_B1;
				}
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				IL_A1:
				int num2 = i;
				i = num2 + 1;
				IL_B1:
				if (i >= data.Class.Keys.Length)
				{
					return false;
				}
				if (expandoObject._data.Version != version || data != expandoObject._data)
				{
					throw Error.CollectionModifiedWhileEnumerating();
				}
				object obj = data[i];
				if (obj != ExpandoObject.Uninitialized)
				{
					this.<>2__current = new KeyValuePair<string, object>(data.Class.Keys[i], obj);
					this.<>1__state = 1;
					return true;
				}
				goto IL_A1;
			}

			// Token: 0x1700053D RID: 1341
			// (get) Token: 0x06001B28 RID: 6952 RVA: 0x0004FBF4 File Offset: 0x0004DDF4
			KeyValuePair<string, object> IEnumerator<KeyValuePair<string, object>>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06001B29 RID: 6953 RVA: 0x00003A6B File Offset: 0x00001C6B
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700053E RID: 1342
			// (get) Token: 0x06001B2A RID: 6954 RVA: 0x0004FBFC File Offset: 0x0004DDFC
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x04000C98 RID: 3224
			private int <>1__state;

			// Token: 0x04000C99 RID: 3225
			private KeyValuePair<string, object> <>2__current;

			// Token: 0x04000C9A RID: 3226
			public ExpandoObject <>4__this;

			// Token: 0x04000C9B RID: 3227
			public int version;

			// Token: 0x04000C9C RID: 3228
			public ExpandoObject.ExpandoData data;

			// Token: 0x04000C9D RID: 3229
			private int <i>5__1;
		}
	}
}
