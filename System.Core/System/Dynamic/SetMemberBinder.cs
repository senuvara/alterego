﻿using System;
using System.Dynamic.Utils;
using System.Runtime.CompilerServices;

namespace System.Dynamic
{
	/// <summary>Represents the dynamic set member operation at the call site, providing the binding semantic and the details about the operation.</summary>
	// Token: 0x02000467 RID: 1127
	public abstract class SetMemberBinder : DynamicMetaObjectBinder
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Dynamic.SetMemberBinder" />.</summary>
		/// <param name="name">The name of the member to obtain.</param>
		/// <param name="ignoreCase">Is true if the name should be matched ignoring case; false otherwise.</param>
		// Token: 0x06001B54 RID: 6996 RVA: 0x0004FE10 File Offset: 0x0004E010
		protected SetMemberBinder(string name, bool ignoreCase)
		{
			ContractUtils.RequiresNotNull(name, "name");
			this.Name = name;
			this.IgnoreCase = ignoreCase;
		}

		/// <summary>The result type of the operation.</summary>
		/// <returns>The <see cref="T:System.Type" /> object representing the result type of the operation.</returns>
		// Token: 0x17000552 RID: 1362
		// (get) Token: 0x06001B55 RID: 6997 RVA: 0x000314CE File Offset: 0x0002F6CE
		public sealed override Type ReturnType
		{
			get
			{
				return typeof(object);
			}
		}

		/// <summary>Gets the name of the member to obtain.</summary>
		/// <returns>The name of the member to obtain.</returns>
		// Token: 0x17000553 RID: 1363
		// (get) Token: 0x06001B56 RID: 6998 RVA: 0x0004FE31 File Offset: 0x0004E031
		public string Name
		{
			[CompilerGenerated]
			get
			{
				return this.<Name>k__BackingField;
			}
		}

		/// <summary>Gets the value indicating if the string comparison should ignore the case of the member name.</summary>
		/// <returns>True if the case is ignored, otherwise false.</returns>
		// Token: 0x17000554 RID: 1364
		// (get) Token: 0x06001B57 RID: 6999 RVA: 0x0004FE39 File Offset: 0x0004E039
		public bool IgnoreCase
		{
			[CompilerGenerated]
			get
			{
				return this.<IgnoreCase>k__BackingField;
			}
		}

		/// <summary>Performs the binding of the dynamic set member operation.</summary>
		/// <param name="target">The target of the dynamic set member operation.</param>
		/// <param name="args">An array of arguments of the dynamic set member operation.</param>
		/// <returns>The <see cref="T:System.Dynamic.DynamicMetaObject" /> representing the result of the binding.</returns>
		// Token: 0x06001B58 RID: 7000 RVA: 0x0004FE44 File Offset: 0x0004E044
		public sealed override DynamicMetaObject Bind(DynamicMetaObject target, DynamicMetaObject[] args)
		{
			ContractUtils.RequiresNotNull(target, "target");
			ContractUtils.RequiresNotNull(args, "args");
			ContractUtils.Requires(args.Length == 1, "args");
			DynamicMetaObject value = args[0];
			ContractUtils.RequiresNotNull(value, "args");
			return target.BindSetMember(this, value);
		}

		// Token: 0x17000555 RID: 1365
		// (get) Token: 0x06001B59 RID: 7001 RVA: 0x00009CDF File Offset: 0x00007EDF
		internal sealed override bool IsStandardBinder
		{
			get
			{
				return true;
			}
		}

		/// <summary>Performs the binding of the dynamic set member operation if the target dynamic object cannot bind.</summary>
		/// <param name="target">The target of the dynamic set member operation.</param>
		/// <param name="value">The value to set to the member.</param>
		/// <returns>The <see cref="T:System.Dynamic.DynamicMetaObject" /> representing the result of the binding.</returns>
		// Token: 0x06001B5A RID: 7002 RVA: 0x0004FE8E File Offset: 0x0004E08E
		public DynamicMetaObject FallbackSetMember(DynamicMetaObject target, DynamicMetaObject value)
		{
			return this.FallbackSetMember(target, value, null);
		}

		/// <summary>Performs the binding of the dynamic set member operation if the target dynamic object cannot bind.</summary>
		/// <param name="target">The target of the dynamic set member operation.</param>
		/// <param name="value">The value to set to the member.</param>
		/// <param name="errorSuggestion">The binding result to use if binding fails, or null.</param>
		/// <returns>The <see cref="T:System.Dynamic.DynamicMetaObject" /> representing the result of the binding.</returns>
		// Token: 0x06001B5B RID: 7003
		public abstract DynamicMetaObject FallbackSetMember(DynamicMetaObject target, DynamicMetaObject value, DynamicMetaObject errorSuggestion);

		// Token: 0x04000CA6 RID: 3238
		[CompilerGenerated]
		private readonly string <Name>k__BackingField;

		// Token: 0x04000CA7 RID: 3239
		[CompilerGenerated]
		private readonly bool <IgnoreCase>k__BackingField;
	}
}
