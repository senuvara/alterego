﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic.Utils;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace System.Dynamic
{
	/// <summary>Provides a base class for specifying dynamic behavior at run time. This class must be inherited from; you cannot instantiate it directly.</summary>
	// Token: 0x02000446 RID: 1094
	[Serializable]
	public class DynamicObject : IDynamicMetaObjectProvider
	{
		/// <summary>Enables derived types to initialize a new instance of the <see cref="T:System.Dynamic.DynamicObject" /> type.</summary>
		// Token: 0x06001A6F RID: 6767 RVA: 0x00002310 File Offset: 0x00000510
		protected DynamicObject()
		{
		}

		/// <summary>Provides the implementation for operations that get member values. Classes derived from the <see cref="T:System.Dynamic.DynamicObject" /> class can override this method to specify dynamic behavior for operations such as getting a value for a property.</summary>
		/// <param name="binder">Provides information about the object that called the dynamic operation. The binder.Name property provides the name of the member on which the dynamic operation is performed. For example, for the Console.WriteLine(sampleObject.SampleProperty) statement, where sampleObject is an instance of the class derived from the <see cref="T:System.Dynamic.DynamicObject" /> class, binder.Name returns "SampleProperty". The binder.IgnoreCase property specifies whether the member name is case-sensitive.</param>
		/// <param name="result">The result of the get operation. For example, if the method is called for a property, you can assign the property value to <paramref name="result" />.</param>
		/// <returns>
		///     <see langword="true" /> if the operation is successful; otherwise, <see langword="false" />. If this method returns <see langword="false" />, the run-time binder of the language determines the behavior. (In most cases, a run-time exception is thrown.)</returns>
		// Token: 0x06001A70 RID: 6768 RVA: 0x0004D902 File Offset: 0x0004BB02
		public virtual bool TryGetMember(GetMemberBinder binder, out object result)
		{
			result = null;
			return false;
		}

		/// <summary>Provides the implementation for operations that set member values. Classes derived from the <see cref="T:System.Dynamic.DynamicObject" /> class can override this method to specify dynamic behavior for operations such as setting a value for a property.</summary>
		/// <param name="binder">Provides information about the object that called the dynamic operation. The binder.Name property provides the name of the member to which the value is being assigned. For example, for the statement sampleObject.SampleProperty = "Test", where sampleObject is an instance of the class derived from the <see cref="T:System.Dynamic.DynamicObject" /> class, binder.Name returns "SampleProperty". The binder.IgnoreCase property specifies whether the member name is case-sensitive.</param>
		/// <param name="value">The value to set to the member. For example, for sampleObject.SampleProperty = "Test", where sampleObject is an instance of the class derived from the <see cref="T:System.Dynamic.DynamicObject" /> class, the <paramref name="value" /> is "Test".</param>
		/// <returns>
		///     <see langword="true" /> if the operation is successful; otherwise, <see langword="false" />. If this method returns <see langword="false" />, the run-time binder of the language determines the behavior. (In most cases, a language-specific run-time exception is thrown.)</returns>
		// Token: 0x06001A71 RID: 6769 RVA: 0x00002285 File Offset: 0x00000485
		public virtual bool TrySetMember(SetMemberBinder binder, object value)
		{
			return false;
		}

		/// <summary>Provides the implementation for operations that delete an object member. This method is not intended for use in C# or Visual Basic.</summary>
		/// <param name="binder">Provides information about the deletion.</param>
		/// <returns>
		///     <see langword="true" /> if the operation is successful; otherwise, <see langword="false" />. If this method returns <see langword="false" />, the run-time binder of the language determines the behavior. (In most cases, a language-specific run-time exception is thrown.)</returns>
		// Token: 0x06001A72 RID: 6770 RVA: 0x00002285 File Offset: 0x00000485
		public virtual bool TryDeleteMember(DeleteMemberBinder binder)
		{
			return false;
		}

		/// <summary>Provides the implementation for operations that invoke a member. Classes derived from the <see cref="T:System.Dynamic.DynamicObject" /> class can override this method to specify dynamic behavior for operations such as calling a method.</summary>
		/// <param name="binder">Provides information about the dynamic operation. The binder.Name property provides the name of the member on which the dynamic operation is performed. For example, for the statement sampleObject.SampleMethod(100), where sampleObject is an instance of the class derived from the <see cref="T:System.Dynamic.DynamicObject" /> class, binder.Name returns "SampleMethod". The binder.IgnoreCase property specifies whether the member name is case-sensitive.</param>
		/// <param name="args">The arguments that are passed to the object member during the invoke operation. For example, for the statement sampleObject.SampleMethod(100), where sampleObject is derived from the <see cref="T:System.Dynamic.DynamicObject" /> class, <paramref name="args[0]" /> is equal to 100.</param>
		/// <param name="result">The result of the member invocation.</param>
		/// <returns>
		///     <see langword="true" /> if the operation is successful; otherwise, <see langword="false" />. If this method returns <see langword="false" />, the run-time binder of the language determines the behavior. (In most cases, a language-specific run-time exception is thrown.)</returns>
		// Token: 0x06001A73 RID: 6771 RVA: 0x0004D908 File Offset: 0x0004BB08
		public virtual bool TryInvokeMember(InvokeMemberBinder binder, object[] args, out object result)
		{
			result = null;
			return false;
		}

		/// <summary>Provides implementation for type conversion operations. Classes derived from the <see cref="T:System.Dynamic.DynamicObject" /> class can override this method to specify dynamic behavior for operations that convert an object from one type to another.</summary>
		/// <param name="binder">Provides information about the conversion operation. The binder.Type property provides the type to which the object must be converted. For example, for the statement (String)sampleObject in C# (CType(sampleObject, Type) in Visual Basic), where sampleObject is an instance of the class derived from the <see cref="T:System.Dynamic.DynamicObject" /> class, binder.Type returns the <see cref="T:System.String" /> type. The binder.Explicit property provides information about the kind of conversion that occurs. It returns <see langword="true" /> for explicit conversion and <see langword="false" /> for implicit conversion.</param>
		/// <param name="result">The result of the type conversion operation.</param>
		/// <returns>
		///     <see langword="true" /> if the operation is successful; otherwise, <see langword="false" />. If this method returns <see langword="false" />, the run-time binder of the language determines the behavior. (In most cases, a language-specific run-time exception is thrown.)</returns>
		// Token: 0x06001A74 RID: 6772 RVA: 0x0004D902 File Offset: 0x0004BB02
		public virtual bool TryConvert(ConvertBinder binder, out object result)
		{
			result = null;
			return false;
		}

		/// <summary>Provides the implementation for operations that initialize a new instance of a dynamic object. This method is not intended for use in C# or Visual Basic.</summary>
		/// <param name="binder">Provides information about the initialization operation.</param>
		/// <param name="args">The arguments that are passed to the object during initialization. For example, for the new SampleType(100) operation, where SampleType is the type derived from the <see cref="T:System.Dynamic.DynamicObject" /> class, <paramref name="args[0]" /> is equal to 100.</param>
		/// <param name="result">The result of the initialization.</param>
		/// <returns>
		///     <see langword="true" /> if the operation is successful; otherwise, <see langword="false" />. If this method returns <see langword="false" />, the run-time binder of the language determines the behavior. (In most cases, a language-specific run-time exception is thrown.)</returns>
		// Token: 0x06001A75 RID: 6773 RVA: 0x0004D908 File Offset: 0x0004BB08
		public virtual bool TryCreateInstance(CreateInstanceBinder binder, object[] args, out object result)
		{
			result = null;
			return false;
		}

		/// <summary>Provides the implementation for operations that invoke an object. Classes derived from the <see cref="T:System.Dynamic.DynamicObject" /> class can override this method to specify dynamic behavior for operations such as invoking an object or a delegate.</summary>
		/// <param name="binder">Provides information about the invoke operation.</param>
		/// <param name="args">The arguments that are passed to the object during the invoke operation. For example, for the sampleObject(100) operation, where sampleObject is derived from the <see cref="T:System.Dynamic.DynamicObject" /> class, <paramref name="args[0]" /> is equal to 100.</param>
		/// <param name="result">The result of the object invocation.</param>
		/// <returns>
		///     <see langword="true" /> if the operation is successful; otherwise, <see langword="false" />. If this method returns <see langword="false" />, the run-time binder of the language determines the behavior. (In most cases, a language-specific run-time exception is thrown.</returns>
		// Token: 0x06001A76 RID: 6774 RVA: 0x0004D908 File Offset: 0x0004BB08
		public virtual bool TryInvoke(InvokeBinder binder, object[] args, out object result)
		{
			result = null;
			return false;
		}

		/// <summary>Provides implementation for binary operations. Classes derived from the <see cref="T:System.Dynamic.DynamicObject" /> class can override this method to specify dynamic behavior for operations such as addition and multiplication.</summary>
		/// <param name="binder">Provides information about the binary operation. The binder.Operation property returns an <see cref="T:System.Linq.Expressions.ExpressionType" /> object. For example, for the sum = first + second statement, where first and second are derived from the <see langword="DynamicObject" /> class, binder.Operation returns ExpressionType.Add.</param>
		/// <param name="arg">The right operand for the binary operation. For example, for the sum = first + second statement, where first and second are derived from the <see langword="DynamicObject" /> class, <paramref name="arg" /> is equal to second.</param>
		/// <param name="result">The result of the binary operation.</param>
		/// <returns>
		///     <see langword="true" /> if the operation is successful; otherwise, <see langword="false" />. If this method returns <see langword="false" />, the run-time binder of the language determines the behavior. (In most cases, a language-specific run-time exception is thrown.)</returns>
		// Token: 0x06001A77 RID: 6775 RVA: 0x0004D908 File Offset: 0x0004BB08
		public virtual bool TryBinaryOperation(BinaryOperationBinder binder, object arg, out object result)
		{
			result = null;
			return false;
		}

		/// <summary>Provides implementation for unary operations. Classes derived from the <see cref="T:System.Dynamic.DynamicObject" /> class can override this method to specify dynamic behavior for operations such as negation, increment, or decrement.</summary>
		/// <param name="binder">Provides information about the unary operation. The binder.Operation property returns an <see cref="T:System.Linq.Expressions.ExpressionType" /> object. For example, for the negativeNumber = -number statement, where number is derived from the <see langword="DynamicObject" /> class, binder.Operation returns "Negate".</param>
		/// <param name="result">The result of the unary operation.</param>
		/// <returns>
		///     <see langword="true" /> if the operation is successful; otherwise, <see langword="false" />. If this method returns <see langword="false" />, the run-time binder of the language determines the behavior. (In most cases, a language-specific run-time exception is thrown.)</returns>
		// Token: 0x06001A78 RID: 6776 RVA: 0x0004D902 File Offset: 0x0004BB02
		public virtual bool TryUnaryOperation(UnaryOperationBinder binder, out object result)
		{
			result = null;
			return false;
		}

		/// <summary>Provides the implementation for operations that get a value by index. Classes derived from the <see cref="T:System.Dynamic.DynamicObject" /> class can override this method to specify dynamic behavior for indexing operations.</summary>
		/// <param name="binder">Provides information about the operation. </param>
		/// <param name="indexes">The indexes that are used in the operation. For example, for the sampleObject[3] operation in C# (sampleObject(3) in Visual Basic), where sampleObject is derived from the <see langword="DynamicObject" /> class, <paramref name="indexes[0]" /> is equal to 3.</param>
		/// <param name="result">The result of the index operation.</param>
		/// <returns>
		///     <see langword="true" /> if the operation is successful; otherwise, <see langword="false" />. If this method returns <see langword="false" />, the run-time binder of the language determines the behavior. (In most cases, a run-time exception is thrown.)</returns>
		// Token: 0x06001A79 RID: 6777 RVA: 0x0004D908 File Offset: 0x0004BB08
		public virtual bool TryGetIndex(GetIndexBinder binder, object[] indexes, out object result)
		{
			result = null;
			return false;
		}

		/// <summary>Provides the implementation for operations that set a value by index. Classes derived from the <see cref="T:System.Dynamic.DynamicObject" /> class can override this method to specify dynamic behavior for operations that access objects by a specified index.</summary>
		/// <param name="binder">Provides information about the operation. </param>
		/// <param name="indexes">The indexes that are used in the operation. For example, for the sampleObject[3] = 10 operation in C# (sampleObject(3) = 10 in Visual Basic), where sampleObject is derived from the <see cref="T:System.Dynamic.DynamicObject" /> class, <paramref name="indexes[0]" /> is equal to 3.</param>
		/// <param name="value">The value to set to the object that has the specified index. For example, for the sampleObject[3] = 10 operation in C# (sampleObject(3) = 10 in Visual Basic), where sampleObject is derived from the <see cref="T:System.Dynamic.DynamicObject" /> class, <paramref name="value" /> is equal to 10.</param>
		/// <returns>
		///     <see langword="true" /> if the operation is successful; otherwise, <see langword="false" />. If this method returns <see langword="false" />, the run-time binder of the language determines the behavior. (In most cases, a language-specific run-time exception is thrown.</returns>
		// Token: 0x06001A7A RID: 6778 RVA: 0x00002285 File Offset: 0x00000485
		public virtual bool TrySetIndex(SetIndexBinder binder, object[] indexes, object value)
		{
			return false;
		}

		/// <summary>Provides the implementation for operations that delete an object by index. This method is not intended for use in C# or Visual Basic.</summary>
		/// <param name="binder">Provides information about the deletion.</param>
		/// <param name="indexes">The indexes to be deleted.</param>
		/// <returns>
		///     <see langword="true" /> if the operation is successful; otherwise, <see langword="false" />. If this method returns <see langword="false" />, the run-time binder of the language determines the behavior. (In most cases, a language-specific run-time exception is thrown.)</returns>
		// Token: 0x06001A7B RID: 6779 RVA: 0x00002285 File Offset: 0x00000485
		public virtual bool TryDeleteIndex(DeleteIndexBinder binder, object[] indexes)
		{
			return false;
		}

		/// <summary>Returns the enumeration of all dynamic member names. </summary>
		/// <returns>A sequence that contains dynamic member names.</returns>
		// Token: 0x06001A7C RID: 6780 RVA: 0x0004D5AA File Offset: 0x0004B7AA
		public virtual IEnumerable<string> GetDynamicMemberNames()
		{
			return Array.Empty<string>();
		}

		/// <summary>Provides a <see cref="T:System.Dynamic.DynamicMetaObject" /> that dispatches to the dynamic virtual methods. The object can be encapsulated inside another <see cref="T:System.Dynamic.DynamicMetaObject" /> to provide custom behavior for individual actions. This method supports the Dynamic Language Runtime infrastructure for language implementers and it is not intended to be used directly from your code.</summary>
		/// <param name="parameter">The expression that represents <see cref="T:System.Dynamic.DynamicMetaObject" /> to dispatch to the dynamic virtual methods.</param>
		/// <returns>An object of the <see cref="T:System.Dynamic.DynamicMetaObject" /> type.</returns>
		// Token: 0x06001A7D RID: 6781 RVA: 0x0004D90E File Offset: 0x0004BB0E
		public virtual DynamicMetaObject GetMetaObject(Expression parameter)
		{
			return new DynamicObject.MetaDynamic(parameter, this);
		}

		// Token: 0x02000447 RID: 1095
		private sealed class MetaDynamic : DynamicMetaObject
		{
			// Token: 0x06001A7E RID: 6782 RVA: 0x0004D917 File Offset: 0x0004BB17
			internal MetaDynamic(Expression expression, DynamicObject value) : base(expression, BindingRestrictions.Empty, value)
			{
			}

			// Token: 0x06001A7F RID: 6783 RVA: 0x0004D926 File Offset: 0x0004BB26
			public override IEnumerable<string> GetDynamicMemberNames()
			{
				return this.Value.GetDynamicMemberNames();
			}

			// Token: 0x06001A80 RID: 6784 RVA: 0x0004D934 File Offset: 0x0004BB34
			public override DynamicMetaObject BindGetMember(GetMemberBinder binder)
			{
				if (this.IsOverridden(CachedReflectionInfo.DynamicObject_TryGetMember))
				{
					return this.CallMethodWithResult<GetMemberBinder>(CachedReflectionInfo.DynamicObject_TryGetMember, binder, DynamicObject.MetaDynamic.s_noArgs, (DynamicObject.MetaDynamic @this, GetMemberBinder b, DynamicMetaObject e) => b.FallbackGetMember(@this, e));
				}
				return base.BindGetMember(binder);
			}

			// Token: 0x06001A81 RID: 6785 RVA: 0x0004D988 File Offset: 0x0004BB88
			public override DynamicMetaObject BindSetMember(SetMemberBinder binder, DynamicMetaObject value)
			{
				if (this.IsOverridden(CachedReflectionInfo.DynamicObject_TrySetMember))
				{
					DynamicMetaObject localValue = value;
					return this.CallMethodReturnLast<SetMemberBinder>(CachedReflectionInfo.DynamicObject_TrySetMember, binder, DynamicObject.MetaDynamic.s_noArgs, value.Expression, (DynamicObject.MetaDynamic @this, SetMemberBinder b, DynamicMetaObject e) => b.FallbackSetMember(@this, localValue, e));
				}
				return base.BindSetMember(binder, value);
			}

			// Token: 0x06001A82 RID: 6786 RVA: 0x0004D9DC File Offset: 0x0004BBDC
			public override DynamicMetaObject BindDeleteMember(DeleteMemberBinder binder)
			{
				if (this.IsOverridden(CachedReflectionInfo.DynamicObject_TryDeleteMember))
				{
					return this.CallMethodNoResult<DeleteMemberBinder>(CachedReflectionInfo.DynamicObject_TryDeleteMember, binder, DynamicObject.MetaDynamic.s_noArgs, (DynamicObject.MetaDynamic @this, DeleteMemberBinder b, DynamicMetaObject e) => b.FallbackDeleteMember(@this, e));
				}
				return base.BindDeleteMember(binder);
			}

			// Token: 0x06001A83 RID: 6787 RVA: 0x0004DA30 File Offset: 0x0004BC30
			public override DynamicMetaObject BindConvert(ConvertBinder binder)
			{
				if (this.IsOverridden(CachedReflectionInfo.DynamicObject_TryConvert))
				{
					return this.CallMethodWithResult<ConvertBinder>(CachedReflectionInfo.DynamicObject_TryConvert, binder, DynamicObject.MetaDynamic.s_noArgs, (DynamicObject.MetaDynamic @this, ConvertBinder b, DynamicMetaObject e) => b.FallbackConvert(@this, e));
				}
				return base.BindConvert(binder);
			}

			// Token: 0x06001A84 RID: 6788 RVA: 0x0004DA84 File Offset: 0x0004BC84
			public override DynamicMetaObject BindInvokeMember(InvokeMemberBinder binder, DynamicMetaObject[] args)
			{
				DynamicMetaObject errorSuggestion = this.BuildCallMethodWithResult<InvokeMemberBinder>(CachedReflectionInfo.DynamicObject_TryInvokeMember, binder, DynamicMetaObject.GetExpressions(args), this.BuildCallMethodWithResult<GetMemberBinder>(CachedReflectionInfo.DynamicObject_TryGetMember, new DynamicObject.MetaDynamic.GetBinderAdapter(binder), DynamicObject.MetaDynamic.s_noArgs, binder.FallbackInvokeMember(this, args, null), (DynamicObject.MetaDynamic @this, GetMemberBinder ignored, DynamicMetaObject e) => binder.FallbackInvoke(e, args, null)), null);
				return binder.FallbackInvokeMember(this, args, errorSuggestion);
			}

			// Token: 0x06001A85 RID: 6789 RVA: 0x0004DB10 File Offset: 0x0004BD10
			public override DynamicMetaObject BindCreateInstance(CreateInstanceBinder binder, DynamicMetaObject[] args)
			{
				if (this.IsOverridden(CachedReflectionInfo.DynamicObject_TryCreateInstance))
				{
					DynamicMetaObject[] localArgs = args;
					return this.CallMethodWithResult<CreateInstanceBinder>(CachedReflectionInfo.DynamicObject_TryCreateInstance, binder, DynamicMetaObject.GetExpressions(args), (DynamicObject.MetaDynamic @this, CreateInstanceBinder b, DynamicMetaObject e) => b.FallbackCreateInstance(@this, localArgs, e));
				}
				return base.BindCreateInstance(binder, args);
			}

			// Token: 0x06001A86 RID: 6790 RVA: 0x0004DB60 File Offset: 0x0004BD60
			public override DynamicMetaObject BindInvoke(InvokeBinder binder, DynamicMetaObject[] args)
			{
				if (this.IsOverridden(CachedReflectionInfo.DynamicObject_TryInvoke))
				{
					DynamicMetaObject[] localArgs = args;
					return this.CallMethodWithResult<InvokeBinder>(CachedReflectionInfo.DynamicObject_TryInvoke, binder, DynamicMetaObject.GetExpressions(args), (DynamicObject.MetaDynamic @this, InvokeBinder b, DynamicMetaObject e) => b.FallbackInvoke(@this, localArgs, e));
				}
				return base.BindInvoke(binder, args);
			}

			// Token: 0x06001A87 RID: 6791 RVA: 0x0004DBB0 File Offset: 0x0004BDB0
			public override DynamicMetaObject BindBinaryOperation(BinaryOperationBinder binder, DynamicMetaObject arg)
			{
				if (this.IsOverridden(CachedReflectionInfo.DynamicObject_TryBinaryOperation))
				{
					DynamicMetaObject localArg = arg;
					return this.CallMethodWithResult<BinaryOperationBinder>(CachedReflectionInfo.DynamicObject_TryBinaryOperation, binder, new Expression[]
					{
						arg.Expression
					}, (DynamicObject.MetaDynamic @this, BinaryOperationBinder b, DynamicMetaObject e) => b.FallbackBinaryOperation(@this, localArg, e));
				}
				return base.BindBinaryOperation(binder, arg);
			}

			// Token: 0x06001A88 RID: 6792 RVA: 0x0004DC08 File Offset: 0x0004BE08
			public override DynamicMetaObject BindUnaryOperation(UnaryOperationBinder binder)
			{
				if (this.IsOverridden(CachedReflectionInfo.DynamicObject_TryUnaryOperation))
				{
					return this.CallMethodWithResult<UnaryOperationBinder>(CachedReflectionInfo.DynamicObject_TryUnaryOperation, binder, DynamicObject.MetaDynamic.s_noArgs, (DynamicObject.MetaDynamic @this, UnaryOperationBinder b, DynamicMetaObject e) => b.FallbackUnaryOperation(@this, e));
				}
				return base.BindUnaryOperation(binder);
			}

			// Token: 0x06001A89 RID: 6793 RVA: 0x0004DC5C File Offset: 0x0004BE5C
			public override DynamicMetaObject BindGetIndex(GetIndexBinder binder, DynamicMetaObject[] indexes)
			{
				if (this.IsOverridden(CachedReflectionInfo.DynamicObject_TryGetIndex))
				{
					DynamicMetaObject[] localIndexes = indexes;
					return this.CallMethodWithResult<GetIndexBinder>(CachedReflectionInfo.DynamicObject_TryGetIndex, binder, DynamicMetaObject.GetExpressions(indexes), (DynamicObject.MetaDynamic @this, GetIndexBinder b, DynamicMetaObject e) => b.FallbackGetIndex(@this, localIndexes, e));
				}
				return base.BindGetIndex(binder, indexes);
			}

			// Token: 0x06001A8A RID: 6794 RVA: 0x0004DCAC File Offset: 0x0004BEAC
			public override DynamicMetaObject BindSetIndex(SetIndexBinder binder, DynamicMetaObject[] indexes, DynamicMetaObject value)
			{
				if (this.IsOverridden(CachedReflectionInfo.DynamicObject_TrySetIndex))
				{
					DynamicMetaObject[] localIndexes = indexes;
					DynamicMetaObject localValue = value;
					return this.CallMethodReturnLast<SetIndexBinder>(CachedReflectionInfo.DynamicObject_TrySetIndex, binder, DynamicMetaObject.GetExpressions(indexes), value.Expression, (DynamicObject.MetaDynamic @this, SetIndexBinder b, DynamicMetaObject e) => b.FallbackSetIndex(@this, localIndexes, localValue, e));
				}
				return base.BindSetIndex(binder, indexes, value);
			}

			// Token: 0x06001A8B RID: 6795 RVA: 0x0004DD08 File Offset: 0x0004BF08
			public override DynamicMetaObject BindDeleteIndex(DeleteIndexBinder binder, DynamicMetaObject[] indexes)
			{
				if (this.IsOverridden(CachedReflectionInfo.DynamicObject_TryDeleteIndex))
				{
					DynamicMetaObject[] localIndexes = indexes;
					return this.CallMethodNoResult<DeleteIndexBinder>(CachedReflectionInfo.DynamicObject_TryDeleteIndex, binder, DynamicMetaObject.GetExpressions(indexes), (DynamicObject.MetaDynamic @this, DeleteIndexBinder b, DynamicMetaObject e) => b.FallbackDeleteIndex(@this, localIndexes, e));
				}
				return base.BindDeleteIndex(binder, indexes);
			}

			// Token: 0x06001A8C RID: 6796 RVA: 0x0004DD58 File Offset: 0x0004BF58
			private static ReadOnlyCollection<Expression> GetConvertedArgs(params Expression[] args)
			{
				Expression[] array = new Expression[args.Length];
				for (int i = 0; i < args.Length; i++)
				{
					array[i] = Expression.Convert(args[i], typeof(object));
				}
				return new TrueReadOnlyCollection<Expression>(array);
			}

			// Token: 0x06001A8D RID: 6797 RVA: 0x0004DD98 File Offset: 0x0004BF98
			private static Expression ReferenceArgAssign(Expression callArgs, Expression[] args)
			{
				ReadOnlyCollectionBuilder<Expression> readOnlyCollectionBuilder = null;
				for (int i = 0; i < args.Length; i++)
				{
					ParameterExpression parameterExpression = args[i] as ParameterExpression;
					ContractUtils.Requires(parameterExpression != null, "args");
					if (parameterExpression.IsByRef)
					{
						if (readOnlyCollectionBuilder == null)
						{
							readOnlyCollectionBuilder = new ReadOnlyCollectionBuilder<Expression>();
						}
						readOnlyCollectionBuilder.Add(Expression.Assign(parameterExpression, Expression.Convert(Expression.ArrayIndex(callArgs, Utils.Constant(i)), parameterExpression.Type)));
					}
				}
				if (readOnlyCollectionBuilder != null)
				{
					return Expression.Block(readOnlyCollectionBuilder);
				}
				return Utils.Empty;
			}

			// Token: 0x06001A8E RID: 6798 RVA: 0x0004DE10 File Offset: 0x0004C010
			private static Expression[] BuildCallArgs<TBinder>(TBinder binder, Expression[] parameters, Expression arg0, Expression arg1) where TBinder : DynamicMetaObjectBinder
			{
				if (parameters != DynamicObject.MetaDynamic.s_noArgs)
				{
					if (arg1 == null)
					{
						return new Expression[]
						{
							DynamicObject.MetaDynamic.Constant<TBinder>(binder),
							arg0
						};
					}
					return new Expression[]
					{
						DynamicObject.MetaDynamic.Constant<TBinder>(binder),
						arg0,
						arg1
					};
				}
				else
				{
					if (arg1 == null)
					{
						return new Expression[]
						{
							DynamicObject.MetaDynamic.Constant<TBinder>(binder)
						};
					}
					return new Expression[]
					{
						DynamicObject.MetaDynamic.Constant<TBinder>(binder),
						arg1
					};
				}
			}

			// Token: 0x06001A8F RID: 6799 RVA: 0x0004DE7A File Offset: 0x0004C07A
			private static ConstantExpression Constant<TBinder>(TBinder binder)
			{
				return Expression.Constant(binder, typeof(TBinder));
			}

			// Token: 0x06001A90 RID: 6800 RVA: 0x0004DE91 File Offset: 0x0004C091
			private DynamicMetaObject CallMethodWithResult<TBinder>(MethodInfo method, TBinder binder, Expression[] args, DynamicObject.MetaDynamic.Fallback<TBinder> fallback) where TBinder : DynamicMetaObjectBinder
			{
				return this.CallMethodWithResult<TBinder>(method, binder, args, fallback, null);
			}

			// Token: 0x06001A91 RID: 6801 RVA: 0x0004DEA0 File Offset: 0x0004C0A0
			private DynamicMetaObject CallMethodWithResult<TBinder>(MethodInfo method, TBinder binder, Expression[] args, DynamicObject.MetaDynamic.Fallback<TBinder> fallback, DynamicObject.MetaDynamic.Fallback<TBinder> fallbackInvoke) where TBinder : DynamicMetaObjectBinder
			{
				DynamicMetaObject fallbackResult = fallback(this, binder, null);
				DynamicMetaObject errorSuggestion = this.BuildCallMethodWithResult<TBinder>(method, binder, args, fallbackResult, fallbackInvoke);
				return fallback(this, binder, errorSuggestion);
			}

			// Token: 0x06001A92 RID: 6802 RVA: 0x0004DED0 File Offset: 0x0004C0D0
			private DynamicMetaObject BuildCallMethodWithResult<TBinder>(MethodInfo method, TBinder binder, Expression[] args, DynamicMetaObject fallbackResult, DynamicObject.MetaDynamic.Fallback<TBinder> fallbackInvoke) where TBinder : DynamicMetaObjectBinder
			{
				if (!this.IsOverridden(method))
				{
					return fallbackResult;
				}
				ParameterExpression parameterExpression = Expression.Parameter(typeof(object), null);
				ParameterExpression parameterExpression2 = (method != CachedReflectionInfo.DynamicObject_TryBinaryOperation) ? Expression.Parameter(typeof(object[]), null) : Expression.Parameter(typeof(object), null);
				ReadOnlyCollection<Expression> convertedArgs = DynamicObject.MetaDynamic.GetConvertedArgs(args);
				DynamicMetaObject dynamicMetaObject = new DynamicMetaObject(parameterExpression, BindingRestrictions.Empty);
				if (binder.ReturnType != typeof(object))
				{
					UnaryExpression ifTrue = Expression.Convert(dynamicMetaObject.Expression, binder.ReturnType);
					string value = Strings.DynamicObjectResultNotAssignable("{0}", this.Value.GetType(), binder.GetType(), binder.ReturnType);
					Expression test;
					if (binder.ReturnType.IsValueType && Nullable.GetUnderlyingType(binder.ReturnType) == null)
					{
						test = Expression.TypeIs(dynamicMetaObject.Expression, binder.ReturnType);
					}
					else
					{
						test = Expression.OrElse(Expression.Equal(dynamicMetaObject.Expression, Utils.Null), Expression.TypeIs(dynamicMetaObject.Expression, binder.ReturnType));
					}
					dynamicMetaObject = new DynamicMetaObject(Expression.Condition(test, ifTrue, Expression.Throw(Expression.New(CachedReflectionInfo.InvalidCastException_Ctor_String, new TrueReadOnlyCollection<Expression>(new Expression[]
					{
						Expression.Call(CachedReflectionInfo.String_Format_String_ObjectArray, Expression.Constant(value), Expression.NewArrayInit(typeof(object), new TrueReadOnlyCollection<Expression>(new Expression[]
						{
							Expression.Condition(Expression.Equal(dynamicMetaObject.Expression, Utils.Null), Expression.Constant("null"), Expression.Call(dynamicMetaObject.Expression, CachedReflectionInfo.Object_GetType), typeof(object))
						})))
					})), binder.ReturnType), binder.ReturnType), dynamicMetaObject.Restrictions);
				}
				if (fallbackInvoke != null)
				{
					dynamicMetaObject = fallbackInvoke(this, binder, dynamicMetaObject);
				}
				return new DynamicMetaObject(Expression.Block(new TrueReadOnlyCollection<ParameterExpression>(new ParameterExpression[]
				{
					parameterExpression,
					parameterExpression2
				}), new TrueReadOnlyCollection<Expression>(new Expression[]
				{
					(method != CachedReflectionInfo.DynamicObject_TryBinaryOperation) ? Expression.Assign(parameterExpression2, Expression.NewArrayInit(typeof(object), convertedArgs)) : Expression.Assign(parameterExpression2, convertedArgs[0]),
					Expression.Condition(Expression.Call(this.GetLimitedSelf(), method, DynamicObject.MetaDynamic.BuildCallArgs<TBinder>(binder, args, parameterExpression2, parameterExpression)), Expression.Block((method != CachedReflectionInfo.DynamicObject_TryBinaryOperation) ? DynamicObject.MetaDynamic.ReferenceArgAssign(parameterExpression2, args) : Utils.Empty, dynamicMetaObject.Expression), fallbackResult.Expression, binder.ReturnType)
				})), this.GetRestrictions().Merge(dynamicMetaObject.Restrictions).Merge(fallbackResult.Restrictions));
			}

			// Token: 0x06001A93 RID: 6803 RVA: 0x0004E1A0 File Offset: 0x0004C3A0
			private DynamicMetaObject CallMethodReturnLast<TBinder>(MethodInfo method, TBinder binder, Expression[] args, Expression value, DynamicObject.MetaDynamic.Fallback<TBinder> fallback) where TBinder : DynamicMetaObjectBinder
			{
				DynamicMetaObject dynamicMetaObject = fallback(this, binder, null);
				ParameterExpression parameterExpression = Expression.Parameter(typeof(object), null);
				ParameterExpression parameterExpression2 = Expression.Parameter(typeof(object[]), null);
				ReadOnlyCollection<Expression> convertedArgs = DynamicObject.MetaDynamic.GetConvertedArgs(args);
				DynamicMetaObject errorSuggestion = new DynamicMetaObject(Expression.Block(new TrueReadOnlyCollection<ParameterExpression>(new ParameterExpression[]
				{
					parameterExpression,
					parameterExpression2
				}), new TrueReadOnlyCollection<Expression>(new Expression[]
				{
					Expression.Assign(parameterExpression2, Expression.NewArrayInit(typeof(object), convertedArgs)),
					Expression.Condition(Expression.Call(this.GetLimitedSelf(), method, DynamicObject.MetaDynamic.BuildCallArgs<TBinder>(binder, args, parameterExpression2, Expression.Assign(parameterExpression, Expression.Convert(value, typeof(object))))), Expression.Block(DynamicObject.MetaDynamic.ReferenceArgAssign(parameterExpression2, args), parameterExpression), dynamicMetaObject.Expression, typeof(object))
				})), this.GetRestrictions().Merge(dynamicMetaObject.Restrictions));
				return fallback(this, binder, errorSuggestion);
			}

			// Token: 0x06001A94 RID: 6804 RVA: 0x0004E290 File Offset: 0x0004C490
			private DynamicMetaObject CallMethodNoResult<TBinder>(MethodInfo method, TBinder binder, Expression[] args, DynamicObject.MetaDynamic.Fallback<TBinder> fallback) where TBinder : DynamicMetaObjectBinder
			{
				DynamicMetaObject dynamicMetaObject = fallback(this, binder, null);
				ParameterExpression parameterExpression = Expression.Parameter(typeof(object[]), null);
				ReadOnlyCollection<Expression> convertedArgs = DynamicObject.MetaDynamic.GetConvertedArgs(args);
				DynamicMetaObject errorSuggestion = new DynamicMetaObject(Expression.Block(new TrueReadOnlyCollection<ParameterExpression>(new ParameterExpression[]
				{
					parameterExpression
				}), new TrueReadOnlyCollection<Expression>(new Expression[]
				{
					Expression.Assign(parameterExpression, Expression.NewArrayInit(typeof(object), convertedArgs)),
					Expression.Condition(Expression.Call(this.GetLimitedSelf(), method, DynamicObject.MetaDynamic.BuildCallArgs<TBinder>(binder, args, parameterExpression, null)), Expression.Block(DynamicObject.MetaDynamic.ReferenceArgAssign(parameterExpression, args), Utils.Empty), dynamicMetaObject.Expression, typeof(void))
				})), this.GetRestrictions().Merge(dynamicMetaObject.Restrictions));
				return fallback(this, binder, errorSuggestion);
			}

			// Token: 0x06001A95 RID: 6805 RVA: 0x0004E358 File Offset: 0x0004C558
			private bool IsOverridden(MethodInfo method)
			{
				foreach (MethodInfo methodInfo in this.Value.GetType().GetMember(method.Name, MemberTypes.Method, BindingFlags.Instance | BindingFlags.Public))
				{
					if (methodInfo.DeclaringType != typeof(DynamicObject) && methodInfo.GetBaseDefinition() == method)
					{
						return true;
					}
				}
				return false;
			}

			// Token: 0x06001A96 RID: 6806 RVA: 0x0004E3BE File Offset: 0x0004C5BE
			private BindingRestrictions GetRestrictions()
			{
				return BindingRestrictions.GetTypeRestriction(this);
			}

			// Token: 0x06001A97 RID: 6807 RVA: 0x0004E3C6 File Offset: 0x0004C5C6
			private Expression GetLimitedSelf()
			{
				if (TypeUtils.AreEquivalent(base.Expression.Type, typeof(DynamicObject)))
				{
					return base.Expression;
				}
				return Expression.Convert(base.Expression, typeof(DynamicObject));
			}

			// Token: 0x17000525 RID: 1317
			// (get) Token: 0x06001A98 RID: 6808 RVA: 0x0004E400 File Offset: 0x0004C600
			private new DynamicObject Value
			{
				get
				{
					return (DynamicObject)base.Value;
				}
			}

			// Token: 0x06001A99 RID: 6809 RVA: 0x0004E40D File Offset: 0x0004C60D
			// Note: this type is marked as 'beforefieldinit'.
			static MetaDynamic()
			{
			}

			// Token: 0x04000C56 RID: 3158
			private static readonly Expression[] s_noArgs = new Expression[0];

			// Token: 0x02000448 RID: 1096
			// (Invoke) Token: 0x06001A9B RID: 6811
			private delegate DynamicMetaObject Fallback<TBinder>(DynamicObject.MetaDynamic @this, TBinder binder, DynamicMetaObject errorSuggestion);

			// Token: 0x02000449 RID: 1097
			private sealed class GetBinderAdapter : GetMemberBinder
			{
				// Token: 0x06001A9E RID: 6814 RVA: 0x0004E41A File Offset: 0x0004C61A
				internal GetBinderAdapter(InvokeMemberBinder binder) : base(binder.Name, binder.IgnoreCase)
				{
				}

				// Token: 0x06001A9F RID: 6815 RVA: 0x00003A6B File Offset: 0x00001C6B
				public override DynamicMetaObject FallbackGetMember(DynamicMetaObject target, DynamicMetaObject errorSuggestion)
				{
					throw new NotSupportedException();
				}
			}

			// Token: 0x0200044A RID: 1098
			[CompilerGenerated]
			[Serializable]
			private sealed class <>c
			{
				// Token: 0x06001AA0 RID: 6816 RVA: 0x0004E42E File Offset: 0x0004C62E
				// Note: this type is marked as 'beforefieldinit'.
				static <>c()
				{
				}

				// Token: 0x06001AA1 RID: 6817 RVA: 0x00002310 File Offset: 0x00000510
				public <>c()
				{
				}

				// Token: 0x06001AA2 RID: 6818 RVA: 0x0004E43A File Offset: 0x0004C63A
				internal DynamicMetaObject <BindGetMember>b__2_0(DynamicObject.MetaDynamic @this, GetMemberBinder b, DynamicMetaObject e)
				{
					return b.FallbackGetMember(@this, e);
				}

				// Token: 0x06001AA3 RID: 6819 RVA: 0x0004E444 File Offset: 0x0004C644
				internal DynamicMetaObject <BindDeleteMember>b__4_0(DynamicObject.MetaDynamic @this, DeleteMemberBinder b, DynamicMetaObject e)
				{
					return b.FallbackDeleteMember(@this, e);
				}

				// Token: 0x06001AA4 RID: 6820 RVA: 0x0004E44E File Offset: 0x0004C64E
				internal DynamicMetaObject <BindConvert>b__5_0(DynamicObject.MetaDynamic @this, ConvertBinder b, DynamicMetaObject e)
				{
					return b.FallbackConvert(@this, e);
				}

				// Token: 0x06001AA5 RID: 6821 RVA: 0x0004E458 File Offset: 0x0004C658
				internal DynamicMetaObject <BindUnaryOperation>b__10_0(DynamicObject.MetaDynamic @this, UnaryOperationBinder b, DynamicMetaObject e)
				{
					return b.FallbackUnaryOperation(@this, e);
				}

				// Token: 0x04000C57 RID: 3159
				public static readonly DynamicObject.MetaDynamic.<>c <>9 = new DynamicObject.MetaDynamic.<>c();

				// Token: 0x04000C58 RID: 3160
				public static DynamicObject.MetaDynamic.Fallback<GetMemberBinder> <>9__2_0;

				// Token: 0x04000C59 RID: 3161
				public static DynamicObject.MetaDynamic.Fallback<DeleteMemberBinder> <>9__4_0;

				// Token: 0x04000C5A RID: 3162
				public static DynamicObject.MetaDynamic.Fallback<ConvertBinder> <>9__5_0;

				// Token: 0x04000C5B RID: 3163
				public static DynamicObject.MetaDynamic.Fallback<UnaryOperationBinder> <>9__10_0;
			}

			// Token: 0x0200044B RID: 1099
			[CompilerGenerated]
			private sealed class <>c__DisplayClass3_0
			{
				// Token: 0x06001AA6 RID: 6822 RVA: 0x00002310 File Offset: 0x00000510
				public <>c__DisplayClass3_0()
				{
				}

				// Token: 0x06001AA7 RID: 6823 RVA: 0x0004E462 File Offset: 0x0004C662
				internal DynamicMetaObject <BindSetMember>b__0(DynamicObject.MetaDynamic @this, SetMemberBinder b, DynamicMetaObject e)
				{
					return b.FallbackSetMember(@this, this.localValue, e);
				}

				// Token: 0x04000C5C RID: 3164
				public DynamicMetaObject localValue;
			}

			// Token: 0x0200044C RID: 1100
			[CompilerGenerated]
			private sealed class <>c__DisplayClass6_0
			{
				// Token: 0x06001AA8 RID: 6824 RVA: 0x00002310 File Offset: 0x00000510
				public <>c__DisplayClass6_0()
				{
				}

				// Token: 0x06001AA9 RID: 6825 RVA: 0x0004E472 File Offset: 0x0004C672
				internal DynamicMetaObject <BindInvokeMember>b__0(DynamicObject.MetaDynamic @this, GetMemberBinder ignored, DynamicMetaObject e)
				{
					return this.binder.FallbackInvoke(e, this.args, null);
				}

				// Token: 0x04000C5D RID: 3165
				public InvokeMemberBinder binder;

				// Token: 0x04000C5E RID: 3166
				public DynamicMetaObject[] args;
			}

			// Token: 0x0200044D RID: 1101
			[CompilerGenerated]
			private sealed class <>c__DisplayClass7_0
			{
				// Token: 0x06001AAA RID: 6826 RVA: 0x00002310 File Offset: 0x00000510
				public <>c__DisplayClass7_0()
				{
				}

				// Token: 0x06001AAB RID: 6827 RVA: 0x0004E487 File Offset: 0x0004C687
				internal DynamicMetaObject <BindCreateInstance>b__0(DynamicObject.MetaDynamic @this, CreateInstanceBinder b, DynamicMetaObject e)
				{
					return b.FallbackCreateInstance(@this, this.localArgs, e);
				}

				// Token: 0x04000C5F RID: 3167
				public DynamicMetaObject[] localArgs;
			}

			// Token: 0x0200044E RID: 1102
			[CompilerGenerated]
			private sealed class <>c__DisplayClass8_0
			{
				// Token: 0x06001AAC RID: 6828 RVA: 0x00002310 File Offset: 0x00000510
				public <>c__DisplayClass8_0()
				{
				}

				// Token: 0x06001AAD RID: 6829 RVA: 0x0004E497 File Offset: 0x0004C697
				internal DynamicMetaObject <BindInvoke>b__0(DynamicObject.MetaDynamic @this, InvokeBinder b, DynamicMetaObject e)
				{
					return b.FallbackInvoke(@this, this.localArgs, e);
				}

				// Token: 0x04000C60 RID: 3168
				public DynamicMetaObject[] localArgs;
			}

			// Token: 0x0200044F RID: 1103
			[CompilerGenerated]
			private sealed class <>c__DisplayClass9_0
			{
				// Token: 0x06001AAE RID: 6830 RVA: 0x00002310 File Offset: 0x00000510
				public <>c__DisplayClass9_0()
				{
				}

				// Token: 0x06001AAF RID: 6831 RVA: 0x0004E4A7 File Offset: 0x0004C6A7
				internal DynamicMetaObject <BindBinaryOperation>b__0(DynamicObject.MetaDynamic @this, BinaryOperationBinder b, DynamicMetaObject e)
				{
					return b.FallbackBinaryOperation(@this, this.localArg, e);
				}

				// Token: 0x04000C61 RID: 3169
				public DynamicMetaObject localArg;
			}

			// Token: 0x02000450 RID: 1104
			[CompilerGenerated]
			private sealed class <>c__DisplayClass11_0
			{
				// Token: 0x06001AB0 RID: 6832 RVA: 0x00002310 File Offset: 0x00000510
				public <>c__DisplayClass11_0()
				{
				}

				// Token: 0x06001AB1 RID: 6833 RVA: 0x0004E4B7 File Offset: 0x0004C6B7
				internal DynamicMetaObject <BindGetIndex>b__0(DynamicObject.MetaDynamic @this, GetIndexBinder b, DynamicMetaObject e)
				{
					return b.FallbackGetIndex(@this, this.localIndexes, e);
				}

				// Token: 0x04000C62 RID: 3170
				public DynamicMetaObject[] localIndexes;
			}

			// Token: 0x02000451 RID: 1105
			[CompilerGenerated]
			private sealed class <>c__DisplayClass12_0
			{
				// Token: 0x06001AB2 RID: 6834 RVA: 0x00002310 File Offset: 0x00000510
				public <>c__DisplayClass12_0()
				{
				}

				// Token: 0x06001AB3 RID: 6835 RVA: 0x0004E4C7 File Offset: 0x0004C6C7
				internal DynamicMetaObject <BindSetIndex>b__0(DynamicObject.MetaDynamic @this, SetIndexBinder b, DynamicMetaObject e)
				{
					return b.FallbackSetIndex(@this, this.localIndexes, this.localValue, e);
				}

				// Token: 0x04000C63 RID: 3171
				public DynamicMetaObject[] localIndexes;

				// Token: 0x04000C64 RID: 3172
				public DynamicMetaObject localValue;
			}

			// Token: 0x02000452 RID: 1106
			[CompilerGenerated]
			private sealed class <>c__DisplayClass13_0
			{
				// Token: 0x06001AB4 RID: 6836 RVA: 0x00002310 File Offset: 0x00000510
				public <>c__DisplayClass13_0()
				{
				}

				// Token: 0x06001AB5 RID: 6837 RVA: 0x0004E4DD File Offset: 0x0004C6DD
				internal DynamicMetaObject <BindDeleteIndex>b__0(DynamicObject.MetaDynamic @this, DeleteIndexBinder b, DynamicMetaObject e)
				{
					return b.FallbackDeleteIndex(@this, this.localIndexes, e);
				}

				// Token: 0x04000C65 RID: 3173
				public DynamicMetaObject[] localIndexes;
			}
		}
	}
}
