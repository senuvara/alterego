﻿using System;
using System.Collections.Generic;

namespace System.Dynamic
{
	// Token: 0x02000453 RID: 1107
	internal class ExpandoClass
	{
		// Token: 0x06001AB6 RID: 6838 RVA: 0x0004E4ED File Offset: 0x0004C6ED
		internal ExpandoClass()
		{
			this._hashCode = 6551;
			this._keys = Array.Empty<string>();
		}

		// Token: 0x06001AB7 RID: 6839 RVA: 0x0004E50B File Offset: 0x0004C70B
		internal ExpandoClass(string[] keys, int hashCode)
		{
			this._hashCode = hashCode;
			this._keys = keys;
		}

		// Token: 0x06001AB8 RID: 6840 RVA: 0x0004E524 File Offset: 0x0004C724
		internal ExpandoClass FindNewClass(string newKey)
		{
			int hashCode = this._hashCode ^ newKey.GetHashCode();
			ExpandoClass result;
			lock (this)
			{
				List<WeakReference> transitionList = this.GetTransitionList(hashCode);
				for (int i = 0; i < transitionList.Count; i++)
				{
					ExpandoClass expandoClass = transitionList[i].Target as ExpandoClass;
					if (expandoClass == null)
					{
						transitionList.RemoveAt(i);
						i--;
					}
					else if (string.Equals(expandoClass._keys[expandoClass._keys.Length - 1], newKey, StringComparison.Ordinal))
					{
						return expandoClass;
					}
				}
				string[] array = new string[this._keys.Length + 1];
				Array.Copy(this._keys, 0, array, 0, this._keys.Length);
				array[this._keys.Length] = newKey;
				ExpandoClass expandoClass2 = new ExpandoClass(array, hashCode);
				transitionList.Add(new WeakReference(expandoClass2));
				result = expandoClass2;
			}
			return result;
		}

		// Token: 0x06001AB9 RID: 6841 RVA: 0x0004E620 File Offset: 0x0004C820
		private List<WeakReference> GetTransitionList(int hashCode)
		{
			if (this._transitions == null)
			{
				this._transitions = new Dictionary<int, List<WeakReference>>();
			}
			List<WeakReference> result;
			if (!this._transitions.TryGetValue(hashCode, out result))
			{
				result = (this._transitions[hashCode] = new List<WeakReference>());
			}
			return result;
		}

		// Token: 0x06001ABA RID: 6842 RVA: 0x0004E664 File Offset: 0x0004C864
		internal int GetValueIndex(string name, bool caseInsensitive, ExpandoObject obj)
		{
			if (caseInsensitive)
			{
				return this.GetValueIndexCaseInsensitive(name, obj);
			}
			return this.GetValueIndexCaseSensitive(name);
		}

		// Token: 0x06001ABB RID: 6843 RVA: 0x0004E67C File Offset: 0x0004C87C
		internal int GetValueIndexCaseSensitive(string name)
		{
			for (int i = 0; i < this._keys.Length; i++)
			{
				if (string.Equals(this._keys[i], name, StringComparison.Ordinal))
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x06001ABC RID: 6844 RVA: 0x0004E6B0 File Offset: 0x0004C8B0
		private int GetValueIndexCaseInsensitive(string name, ExpandoObject obj)
		{
			int num = -1;
			object lockObject = obj.LockObject;
			lock (lockObject)
			{
				for (int i = this._keys.Length - 1; i >= 0; i--)
				{
					if (string.Equals(this._keys[i], name, StringComparison.OrdinalIgnoreCase) && !obj.IsDeletedMember(i))
					{
						if (num != -1)
						{
							return -2;
						}
						num = i;
					}
				}
			}
			return num;
		}

		// Token: 0x17000526 RID: 1318
		// (get) Token: 0x06001ABD RID: 6845 RVA: 0x0004E730 File Offset: 0x0004C930
		internal string[] Keys
		{
			get
			{
				return this._keys;
			}
		}

		// Token: 0x06001ABE RID: 6846 RVA: 0x0004E738 File Offset: 0x0004C938
		// Note: this type is marked as 'beforefieldinit'.
		static ExpandoClass()
		{
		}

		// Token: 0x04000C66 RID: 3174
		private readonly string[] _keys;

		// Token: 0x04000C67 RID: 3175
		private readonly int _hashCode;

		// Token: 0x04000C68 RID: 3176
		private Dictionary<int, List<WeakReference>> _transitions;

		// Token: 0x04000C69 RID: 3177
		private const int EmptyHashCode = 6551;

		// Token: 0x04000C6A RID: 3178
		internal static readonly ExpandoClass Empty = new ExpandoClass();
	}
}
