﻿using System;

namespace System
{
	// Token: 0x0200000D RID: 13
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
	internal class MonoExtensionAttribute : MonoTODOAttribute
	{
		// Token: 0x06000028 RID: 40 RVA: 0x000022D5 File Offset: 0x000004D5
		public MonoExtensionAttribute(string comment) : base(comment)
		{
		}
	}
}
