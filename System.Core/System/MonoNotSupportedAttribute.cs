﻿using System;

namespace System
{
	// Token: 0x02000010 RID: 16
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
	internal class MonoNotSupportedAttribute : MonoTODOAttribute
	{
		// Token: 0x0600002B RID: 43 RVA: 0x000022D5 File Offset: 0x000004D5
		public MonoNotSupportedAttribute(string comment) : base(comment)
		{
		}
	}
}
