﻿using System;

namespace Unity
{
	// Token: 0x020004DD RID: 1245
	internal sealed class ThrowStub : ObjectDisposedException
	{
		// Token: 0x06001E1D RID: 7709 RVA: 0x00003A76 File Offset: 0x00001C76
		public static void ThrowNotSupportedException()
		{
			throw new PlatformNotSupportedException();
		}
	}
}
