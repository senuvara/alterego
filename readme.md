#Alter Ego Dump

This is a public dump of Alter Ego. 

**However, please note, this is not a direct reverse engineering of Alter Ego at this time. **

**This code is only human readable for the most part, and will create multiple complier errors. **

**Another thing to note is that this is a complete dump, *including* dependencies and system files. **

The main game code can be found [under Assembly-CSharp.](https://bitbucket.org/senuvara/alterego/src/master/Assembly-CSharp/)

I will be documenting and altering this source to be machine readable, **and to create a full reverse engineered game of Alter Ego. **