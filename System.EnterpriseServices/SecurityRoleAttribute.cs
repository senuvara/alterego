﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Configures a role for an application or component. This class cannot be inherited.</summary>
	// Token: 0x02000042 RID: 66
	[ComVisible(false)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Interface, Inherited = true, AllowMultiple = true)]
	public sealed class SecurityRoleAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.SecurityRoleAttribute" /> class and sets the <see cref="P:System.EnterpriseServices.SecurityRoleAttribute.Role" /> property.</summary>
		/// <param name="role">A security role for the application, component, interface, or method. </param>
		// Token: 0x06000108 RID: 264 RVA: 0x00002050 File Offset: 0x00000250
		public SecurityRoleAttribute(string role)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.SecurityRoleAttribute" /> class and sets the <see cref="P:System.EnterpriseServices.SecurityRoleAttribute.Role" /> and <see cref="P:System.EnterpriseServices.SecurityRoleAttribute.SetEveryoneAccess" /> properties.</summary>
		/// <param name="role">A security role for the application, component, interface, or method. </param>
		/// <param name="everyone">
		///       <see langword="true" /> to require that the newly created role have the Everyone user group added as a user; otherwise, <see langword="false" />. </param>
		// Token: 0x06000109 RID: 265 RVA: 0x00002050 File Offset: 0x00000250
		public SecurityRoleAttribute(string role, bool everyone)
		{
		}

		/// <summary>Gets or sets the role description.</summary>
		/// <returns>The description for the role.</returns>
		// Token: 0x1700005E RID: 94
		// (get) Token: 0x0600010A RID: 266 RVA: 0x0000206F File Offset: 0x0000026F
		// (set) Token: 0x0600010B RID: 267 RVA: 0x00002077 File Offset: 0x00000277
		public string Description
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the security role.</summary>
		/// <returns>The security role applied to an application, component, interface, or method.</returns>
		// Token: 0x1700005F RID: 95
		// (get) Token: 0x0600010C RID: 268 RVA: 0x0000206F File Offset: 0x0000026F
		// (set) Token: 0x0600010D RID: 269 RVA: 0x00002077 File Offset: 0x00000277
		public string Role
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Sets a value indicating whether to add the Everyone user group as a user.</summary>
		/// <returns>
		///     <see langword="true" /> to require that a newly created role have the Everyone user group added as a user (roles that already exist on the application are not modified); otherwise, <see langword="false" /> to suppress adding the Everyone user group as a user.</returns>
		// Token: 0x17000060 RID: 96
		// (get) Token: 0x0600010E RID: 270 RVA: 0x0000280C File Offset: 0x00000A0C
		// (set) Token: 0x0600010F RID: 271 RVA: 0x00002077 File Offset: 0x00000277
		public bool SetEveryoneAccess
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
