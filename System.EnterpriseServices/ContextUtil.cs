﻿using System;
using System.Transactions;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Obtains information about the COM+ object context. This class cannot be inherited.</summary>
	// Token: 0x0200001E RID: 30
	public sealed class ContextUtil
	{
		// Token: 0x06000062 RID: 98 RVA: 0x00002077 File Offset: 0x00000277
		internal ContextUtil()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a GUID representing the activity containing the component.</summary>
		/// <returns>The GUID for an activity if the current context is part of an activity; otherwise, <see langword="GUID_NULL" />.</returns>
		/// <exception cref="T:System.Runtime.InteropServices.COMException">There is no COM+ context available. </exception>
		/// <exception cref="T:System.PlatformNotSupportedException">The platform is not Windows 2000 or later. </exception>
		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000063 RID: 99 RVA: 0x0000233C File Offset: 0x0000053C
		public static Guid ActivityId
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Guid);
			}
		}

		/// <summary>Gets a GUID for the current application.</summary>
		/// <returns>The GUID for the current application.</returns>
		/// <exception cref="T:System.Runtime.InteropServices.COMException">There is no COM+ context available. </exception>
		/// <exception cref="T:System.PlatformNotSupportedException">The platform is not Windows XP or later. </exception>
		// Token: 0x17000028 RID: 40
		// (get) Token: 0x06000064 RID: 100 RVA: 0x00002358 File Offset: 0x00000558
		public static Guid ApplicationId
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Guid);
			}
		}

		/// <summary>Gets a GUID for the current application instance.</summary>
		/// <returns>The GUID for the current application instance.</returns>
		/// <exception cref="T:System.Runtime.InteropServices.COMException">There is no COM+ context available. </exception>
		/// <exception cref="T:System.PlatformNotSupportedException">The platform is not Windows XP or later. </exception>
		// Token: 0x17000029 RID: 41
		// (get) Token: 0x06000065 RID: 101 RVA: 0x00002374 File Offset: 0x00000574
		public static Guid ApplicationInstanceId
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Guid);
			}
		}

		/// <summary>Gets a GUID for the current context.</summary>
		/// <returns>The GUID for the current context.</returns>
		/// <exception cref="T:System.Runtime.InteropServices.COMException">There is no COM+ context available. </exception>
		/// <exception cref="T:System.PlatformNotSupportedException">The platform is not Windows 2000 or later. </exception>
		// Token: 0x1700002A RID: 42
		// (get) Token: 0x06000066 RID: 102 RVA: 0x00002390 File Offset: 0x00000590
		public static Guid ContextId
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Guid);
			}
		}

		/// <summary>Gets or sets the <see langword="done" /> bit in the COM+ context.</summary>
		/// <returns>
		///     <see langword="true" /> if the object is to be deactivated when the method returns; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		/// <exception cref="T:System.Runtime.InteropServices.COMException">There is no COM+ context available. </exception>
		/// <exception cref="T:System.PlatformNotSupportedException">The platform is not Windows 2000 or later. </exception>
		// Token: 0x1700002B RID: 43
		// (get) Token: 0x06000067 RID: 103 RVA: 0x000023AC File Offset: 0x000005AC
		// (set) Token: 0x06000068 RID: 104 RVA: 0x00002077 File Offset: 0x00000277
		public static bool DeactivateOnReturn
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a value that indicates whether the current context is transactional.</summary>
		/// <returns>
		///     <see langword="true" /> if the current context is transactional; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.Runtime.InteropServices.COMException">There is no COM+ context available. </exception>
		// Token: 0x1700002C RID: 44
		// (get) Token: 0x06000069 RID: 105 RVA: 0x000023C8 File Offset: 0x000005C8
		public static bool IsInTransaction
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets a value that indicates whether role-based security is active in the current context.</summary>
		/// <returns>
		///     <see langword="true" /> if the current context has security enabled; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.Runtime.InteropServices.COMException">There is no COM+ context available. </exception>
		// Token: 0x1700002D RID: 45
		// (get) Token: 0x0600006A RID: 106 RVA: 0x000023E4 File Offset: 0x000005E4
		public static bool IsSecurityEnabled
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets or sets the <see langword="consistent" /> bit in the COM+ context.</summary>
		/// <returns>One of the <see cref="T:System.EnterpriseServices.TransactionVote" /> values, either <see langword="Commit" /> or <see langword="Abort" />.</returns>
		/// <exception cref="T:System.Runtime.InteropServices.COMException">There is no COM+ context available. </exception>
		/// <exception cref="T:System.PlatformNotSupportedException">The platform is not Windows 2000 or later.</exception>
		// Token: 0x1700002E RID: 46
		// (get) Token: 0x0600006B RID: 107 RVA: 0x00002400 File Offset: 0x00000600
		// (set) Token: 0x0600006C RID: 108 RVA: 0x00002077 File Offset: 0x00000277
		public static TransactionVote MyTransactionVote
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return TransactionVote.Commit;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets a GUID for the current partition.</summary>
		/// <returns>The GUID for the current partition.</returns>
		/// <exception cref="T:System.Runtime.InteropServices.COMException">There is no COM+ context available. </exception>
		/// <exception cref="T:System.PlatformNotSupportedException">The platform is not Windows XP or later. </exception>
		// Token: 0x1700002F RID: 47
		// (get) Token: 0x0600006D RID: 109 RVA: 0x0000241C File Offset: 0x0000061C
		public static Guid PartitionId
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Guid);
			}
		}

		/// <summary>Gets the current transaction context.</summary>
		/// <returns>A <see cref="T:System.Transactions.Transaction" /> that represents the current transaction context.</returns>
		/// <exception cref="T:System.Runtime.InteropServices.COMException">There is no COM+ context available. </exception>
		/// <exception cref="T:System.PlatformNotSupportedException">The platform is not Windows 2000 or later. </exception>
		// Token: 0x17000030 RID: 48
		// (get) Token: 0x0600006E RID: 110 RVA: 0x0000206F File Offset: 0x0000026F
		public static Transaction SystemTransaction
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets an object describing the current COM+ DTC transaction.</summary>
		/// <returns>An object that represents the current transaction.</returns>
		/// <exception cref="T:System.Runtime.InteropServices.COMException">There is no COM+ context available. </exception>
		/// <exception cref="T:System.PlatformNotSupportedException">The platform is not Windows 2000 or later. </exception>
		// Token: 0x17000031 RID: 49
		// (get) Token: 0x0600006F RID: 111 RVA: 0x0000206F File Offset: 0x0000026F
		public static object Transaction
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the GUID of the current COM+ DTC transaction.</summary>
		/// <returns>A GUID representing the current COM+ DTC transaction, if one exists.</returns>
		/// <exception cref="T:System.Runtime.InteropServices.COMException">There is no COM+ context available. </exception>
		/// <exception cref="T:System.PlatformNotSupportedException">The platform is not Windows 2000 or later. </exception>
		// Token: 0x17000032 RID: 50
		// (get) Token: 0x06000070 RID: 112 RVA: 0x00002438 File Offset: 0x00000638
		public static Guid TransactionId
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Guid);
			}
		}

		/// <summary>Sets both the <see langword="consistent" /> bit and the <see langword="done" /> bit to <see langword="false" /> in the COM+ context.</summary>
		/// <exception cref="T:System.Runtime.InteropServices.COMException">No COM+ context is available.</exception>
		// Token: 0x06000071 RID: 113 RVA: 0x00002077 File Offset: 0x00000277
		public static void DisableCommit()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the <see langword="consistent" /> bit to <see langword="true" /> and the <see langword="done" /> bit to <see langword="false" /> in the COM+ context.</summary>
		/// <exception cref="T:System.Runtime.InteropServices.COMException">No COM+ context is available. </exception>
		// Token: 0x06000072 RID: 114 RVA: 0x00002077 File Offset: 0x00000277
		public static void EnableCommit()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns a named property from the COM+ context.</summary>
		/// <param name="name">The name of the requested property. </param>
		/// <returns>The named property for the context.</returns>
		/// <exception cref="T:System.Runtime.InteropServices.COMException">There is no COM+ context available. </exception>
		/// <exception cref="T:System.PlatformNotSupportedException">The platform is not Windows 2000 or later. </exception>
		// Token: 0x06000073 RID: 115 RVA: 0x0000206F File Offset: 0x0000026F
		public static object GetNamedProperty(string name)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Determines whether the caller is in the specified role.</summary>
		/// <param name="role">The name of the role to check. </param>
		/// <returns>
		///     <see langword="true" /> if the caller is in the specified role; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.Runtime.InteropServices.COMException">There is no COM+ context available. </exception>
		// Token: 0x06000074 RID: 116 RVA: 0x00002454 File Offset: 0x00000654
		public static bool IsCallerInRole(string role)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Determines whether the serviced component is activated in the default context. Serviced components that do not have COM+ catalog information are activated in the default context.</summary>
		/// <returns>
		///     true if the serviced component is activated in the default context; otherwise, false.</returns>
		// Token: 0x06000075 RID: 117 RVA: 0x00002470 File Offset: 0x00000670
		public static bool IsDefaultContext()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Sets the <see langword="consistent" /> bit to <see langword="false" /> and the <see langword="done" /> bit to <see langword="true" /> in the COM+ context.</summary>
		/// <exception cref="T:System.Runtime.InteropServices.COMException">There is no COM+ context available. </exception>
		// Token: 0x06000076 RID: 118 RVA: 0x00002077 File Offset: 0x00000277
		public static void SetAbort()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the <see langword="consistent" /> bit and the <see langword="done" /> bit to <see langword="true" /> in the COM+ context.</summary>
		/// <exception cref="T:System.Runtime.InteropServices.COMException">There is no COM+ context available. </exception>
		// Token: 0x06000077 RID: 119 RVA: 0x00002077 File Offset: 0x00000277
		public static void SetComplete()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the named property for the COM+ context.</summary>
		/// <param name="name">The name of the property to set. </param>
		/// <param name="value">Object that represents the property value to set.</param>
		/// <exception cref="T:System.Runtime.InteropServices.COMException">There is no COM+ context available. </exception>
		/// <exception cref="T:System.PlatformNotSupportedException">The platform is not Windows 2000 or later. </exception>
		// Token: 0x06000078 RID: 120 RVA: 0x00002077 File Offset: 0x00000277
		public static void SetNamedProperty(string name, object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
