﻿using System;
using System.Runtime.InteropServices;
using System.Transactions;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Specifies and configures the services that are to be active in the domain which is entered when calling <see cref="M:System.EnterpriseServices.ServiceDomain.Enter(System.EnterpriseServices.ServiceConfig)" /> or creating an <see cref="T:System.EnterpriseServices.Activity" />. This class cannot be inherited.</summary>
	// Token: 0x0200000A RID: 10
	[ComVisible(false)]
	public sealed class ServiceConfig
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.ServiceConfig" /> class, setting the properties to configure the desired services.</summary>
		/// <exception cref="T:System.PlatformNotSupportedException">
		///         <see cref="T:System.EnterpriseServices.ServiceConfig" /> is not supported on the current platform. </exception>
		// Token: 0x0600000D RID: 13 RVA: 0x00002077 File Offset: 0x00000277
		public ServiceConfig()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the binding option, which indicates whether all work submitted by the activity is to be bound to only one single-threaded apartment (STA).</summary>
		/// <returns>One of the <see cref="T:System.EnterpriseServices.BindingOption" /> values. The default is <see cref="F:System.EnterpriseServices.BindingOption.NoBinding" />.</returns>
		// Token: 0x17000003 RID: 3
		// (get) Token: 0x0600000E RID: 14 RVA: 0x00002080 File Offset: 0x00000280
		// (set) Token: 0x0600000F RID: 15 RVA: 0x00002077 File Offset: 0x00000277
		public BindingOption Binding
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return BindingOption.NoBinding;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Transactions.Transaction" /> that represents an existing transaction that supplies the settings used to run the transaction identified by <see cref="T:System.EnterpriseServices.ServiceConfig" />.</summary>
		/// <returns>A <see cref="T:System.Transactions.Transaction" />. The default is <see langword="null" />.</returns>
		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000010 RID: 16 RVA: 0x0000206F File Offset: 0x0000026F
		// (set) Token: 0x06000011 RID: 17 RVA: 0x00002077 File Offset: 0x00000277
		public Transaction BringYourOwnSystemTransaction
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.EnterpriseServices.ITransaction" /> that represents an existing transaction that supplies the settings used to run the transaction identified by <see cref="T:System.EnterpriseServices.ServiceConfig" />.</summary>
		/// <returns>An <see cref="T:System.EnterpriseServices.ITransaction" />. The default is <see langword="null" />.</returns>
		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000012 RID: 18 RVA: 0x0000206F File Offset: 0x0000026F
		// (set) Token: 0x06000013 RID: 19 RVA: 0x00002077 File Offset: 0x00000277
		public ITransaction BringYourOwnTransaction
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates whether COM Transaction Integrator (COMTI) intrinsics are enabled.</summary>
		/// <returns>
		///     <see langword="true" /> if COMTI intrinsics are enabled; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000014 RID: 20 RVA: 0x0000209C File Offset: 0x0000029C
		// (set) Token: 0x06000015 RID: 21 RVA: 0x00002077 File Offset: 0x00000277
		public bool COMTIIntrinsicsEnabled
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates whether Internet Information Services (IIS) intrinsics are enabled.</summary>
		/// <returns>
		///     <see langword="true" /> if IIS intrinsics are enabled; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000016 RID: 22 RVA: 0x000020B8 File Offset: 0x000002B8
		// (set) Token: 0x06000017 RID: 23 RVA: 0x00002077 File Offset: 0x00000277
		public bool IISIntrinsicsEnabled
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates whether to construct a new context based on the current context or to create a new context based solely on the information in <see cref="T:System.EnterpriseServices.ServiceConfig" />.</summary>
		/// <returns>One of the <see cref="T:System.EnterpriseServices.InheritanceOption" /> values. The default is <see cref="F:System.EnterpriseServices.InheritanceOption.Inherit" />.</returns>
		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000018 RID: 24 RVA: 0x000020D4 File Offset: 0x000002D4
		// (set) Token: 0x06000019 RID: 25 RVA: 0x00002077 File Offset: 0x00000277
		public InheritanceOption Inheritance
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return InheritanceOption.Inherit;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the isolation level of the transaction.</summary>
		/// <returns>One of the <see cref="T:System.EnterpriseServices.TransactionIsolationLevel" /> values. The default is <see cref="F:System.EnterpriseServices.TransactionIsolationLevel.Any" />.</returns>
		// Token: 0x17000009 RID: 9
		// (get) Token: 0x0600001A RID: 26 RVA: 0x000020F0 File Offset: 0x000002F0
		// (set) Token: 0x0600001B RID: 27 RVA: 0x00002077 File Offset: 0x00000277
		public TransactionIsolationLevel IsolationLevel
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return TransactionIsolationLevel.Any;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the GUID for the COM+ partition that is to be used.</summary>
		/// <returns>The GUID for the partition to be used. The default is a zero GUID.</returns>
		// Token: 0x1700000A RID: 10
		// (get) Token: 0x0600001C RID: 28 RVA: 0x0000210C File Offset: 0x0000030C
		// (set) Token: 0x0600001D RID: 29 RVA: 0x00002077 File Offset: 0x00000277
		public Guid PartitionId
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Guid);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates how partitions are used for the enclosed work.</summary>
		/// <returns>One of the <see cref="T:System.EnterpriseServices.PartitionOption" /> values. The default is <see cref="F:System.EnterpriseServices.PartitionOption.Ignore" />.</returns>
		// Token: 0x1700000B RID: 11
		// (get) Token: 0x0600001E RID: 30 RVA: 0x00002128 File Offset: 0x00000328
		// (set) Token: 0x0600001F RID: 31 RVA: 0x00002077 File Offset: 0x00000277
		public PartitionOption PartitionOption
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return PartitionOption.Ignore;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the directory for the side-by-side assembly for the enclosed work.</summary>
		/// <returns>The name of the directory to be used for the side-by-side assembly. The default value is <see langword="null" />.</returns>
		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000020 RID: 32 RVA: 0x0000206F File Offset: 0x0000026F
		// (set) Token: 0x06000021 RID: 33 RVA: 0x00002077 File Offset: 0x00000277
		public string SxsDirectory
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the file name of the side-by-side assembly for the enclosed work.</summary>
		/// <returns>The file name of the side-by-side assembly. The default value is <see langword="null" />.</returns>
		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000022 RID: 34 RVA: 0x0000206F File Offset: 0x0000026F
		// (set) Token: 0x06000023 RID: 35 RVA: 0x00002077 File Offset: 0x00000277
		public string SxsName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates how to configure the side-by-side assembly.</summary>
		/// <returns>One of the <see cref="T:System.EnterpriseServices.SxsOption" /> values. The default is <see cref="F:System.EnterpriseServices.SxsOption.Ignore" />.</returns>
		// Token: 0x1700000E RID: 14
		// (get) Token: 0x06000024 RID: 36 RVA: 0x00002144 File Offset: 0x00000344
		// (set) Token: 0x06000025 RID: 37 RVA: 0x00002077 File Offset: 0x00000277
		public SxsOption SxsOption
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return SxsOption.Ignore;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value in that indicates the type of automatic synchronization requested by the component.</summary>
		/// <returns>One of the <see cref="T:System.EnterpriseServices.SynchronizationOption" /> values. The default is <see cref="F:System.EnterpriseServices.SynchronizationOption.Disabled" />.</returns>
		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000026 RID: 38 RVA: 0x00002160 File Offset: 0x00000360
		// (set) Token: 0x06000027 RID: 39 RVA: 0x00002077 File Offset: 0x00000277
		public SynchronizationOption Synchronization
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return SynchronizationOption.Disabled;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates the thread pool which runs the work submitted by the activity.</summary>
		/// <returns>One of the <see cref="T:System.EnterpriseServices.ThreadPoolOption" /> values. The default is <see cref="F:System.EnterpriseServices.ThreadPoolOption.None" />.</returns>
		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000028 RID: 40 RVA: 0x0000217C File Offset: 0x0000037C
		// (set) Token: 0x06000029 RID: 41 RVA: 0x00002077 File Offset: 0x00000277
		public ThreadPoolOption ThreadPool
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ThreadPoolOption.None;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the Transaction Internet Protocol (TIP) URL that allows the enclosed code to run in an existing transaction.</summary>
		/// <returns>A TIP URL. The default value is <see langword="null" />.</returns>
		// Token: 0x17000011 RID: 17
		// (get) Token: 0x0600002A RID: 42 RVA: 0x0000206F File Offset: 0x0000026F
		// (set) Token: 0x0600002B RID: 43 RVA: 0x00002077 File Offset: 0x00000277
		public string TipUrl
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a text string that corresponds to the application ID under which tracker information is reported.</summary>
		/// <returns>The application ID under which tracker information is reported. The default value is <see langword="null" />.</returns>
		// Token: 0x17000012 RID: 18
		// (get) Token: 0x0600002C RID: 44 RVA: 0x0000206F File Offset: 0x0000026F
		// (set) Token: 0x0600002D RID: 45 RVA: 0x00002077 File Offset: 0x00000277
		public string TrackingAppName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a text string that corresponds to the context name under which tracker information is reported.</summary>
		/// <returns>The context name under which tracker information is reported. The default value is <see langword="null" />.</returns>
		// Token: 0x17000013 RID: 19
		// (get) Token: 0x0600002E RID: 46 RVA: 0x0000206F File Offset: 0x0000026F
		// (set) Token: 0x0600002F RID: 47 RVA: 0x00002077 File Offset: 0x00000277
		public string TrackingComponentName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates whether tracking is enabled.</summary>
		/// <returns>
		///     <see langword="true" /> if tracking is enabled; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000030 RID: 48 RVA: 0x00002198 File Offset: 0x00000398
		// (set) Token: 0x06000031 RID: 49 RVA: 0x00002077 File Offset: 0x00000277
		public bool TrackingEnabled
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates how transactions are used in the enclosed work.</summary>
		/// <returns>One of the <see cref="T:System.EnterpriseServices.TransactionOption" /> values. The default is <see cref="F:System.EnterpriseServices.TransactionOption.Disabled" />.</returns>
		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000032 RID: 50 RVA: 0x000021B4 File Offset: 0x000003B4
		// (set) Token: 0x06000033 RID: 51 RVA: 0x00002077 File Offset: 0x00000277
		public TransactionOption Transaction
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return TransactionOption.Disabled;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name that is used when transaction statistics are displayed.</summary>
		/// <returns>The name used when transaction statistics are displayed. The default value is <see langword="null" />.</returns>
		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000034 RID: 52 RVA: 0x0000206F File Offset: 0x0000026F
		// (set) Token: 0x06000035 RID: 53 RVA: 0x00002077 File Offset: 0x00000277
		public string TransactionDescription
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the transaction time-out for a new transaction.</summary>
		/// <returns>The transaction time-out, in seconds.</returns>
		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000036 RID: 54 RVA: 0x000021D0 File Offset: 0x000003D0
		// (set) Token: 0x06000037 RID: 55 RVA: 0x00002077 File Offset: 0x00000277
		public int TransactionTimeout
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
