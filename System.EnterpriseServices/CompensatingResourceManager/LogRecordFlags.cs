﻿using System;

namespace System.EnterpriseServices.CompensatingResourceManager
{
	/// <summary>Describes the origin of a Compensating Resource Manager (CRM) log record.</summary>
	// Token: 0x02000069 RID: 105
	[Flags]
	[Serializable]
	public enum LogRecordFlags
	{
		/// <summary>Indicates the delivered record should be forgotten.</summary>
		// Token: 0x04000063 RID: 99
		ForgetTarget = 1,
		/// <summary>Log record was written when replay was in progress.</summary>
		// Token: 0x04000064 RID: 100
		ReplayInProgress = 64,
		/// <summary>Log record was written during abort.</summary>
		// Token: 0x04000065 RID: 101
		WrittenDuringAbort = 8,
		/// <summary>Log record was written during commit.</summary>
		// Token: 0x04000066 RID: 102
		WrittenDuringCommit = 4,
		/// <summary>Log record was written during prepare.</summary>
		// Token: 0x04000067 RID: 103
		WrittenDuringPrepare = 2,
		/// <summary>Log record was written during replay.</summary>
		// Token: 0x04000068 RID: 104
		WrittenDuringReplay = 32,
		/// <summary>Log record was written during recovery.</summary>
		// Token: 0x04000069 RID: 105
		WrittenDurringRecovery = 16
	}
}
