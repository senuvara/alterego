﻿using System;

namespace System.EnterpriseServices.CompensatingResourceManager
{
	/// <summary>Specifies the state of the current Compensating Resource Manager (CRM) transaction.</summary>
	// Token: 0x0200006A RID: 106
	[Serializable]
	public enum TransactionState
	{
		/// <summary>The transaction is aborted.</summary>
		// Token: 0x0400006B RID: 107
		Aborted = 2,
		/// <summary>The transaction is active.</summary>
		// Token: 0x0400006C RID: 108
		Active = 0,
		/// <summary>The transaction is commited.</summary>
		// Token: 0x0400006D RID: 109
		Committed,
		/// <summary>The transaction is in-doubt.</summary>
		// Token: 0x0400006E RID: 110
		Indoubt = 3
	}
}
