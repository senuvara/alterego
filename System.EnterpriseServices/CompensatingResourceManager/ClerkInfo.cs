﻿using System;
using Unity;

namespace System.EnterpriseServices.CompensatingResourceManager
{
	/// <summary>Contains information describing an active Compensating Resource Manager (CRM) Clerk object.</summary>
	// Token: 0x02000065 RID: 101
	public sealed class ClerkInfo
	{
		// Token: 0x06000186 RID: 390 RVA: 0x00002077 File Offset: 0x00000277
		internal ClerkInfo()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the activity ID of the current Compensating Resource Manager (CRM) Worker.</summary>
		/// <returns>Gets the activity ID of the current Compensating Resource Manager (CRM) Worker.</returns>
		// Token: 0x17000066 RID: 102
		// (get) Token: 0x06000187 RID: 391 RVA: 0x0000206F File Offset: 0x0000026F
		public string ActivityId
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets <see cref="F:System.Runtime.InteropServices.UnmanagedType.IUnknown" /> for the current Clerk.</summary>
		/// <returns>
		///     <see cref="F:System.Runtime.InteropServices.UnmanagedType.IUnknown" /> for the current Clerk.</returns>
		// Token: 0x17000067 RID: 103
		// (get) Token: 0x06000188 RID: 392 RVA: 0x0000206F File Offset: 0x0000026F
		public Clerk Clerk
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the ProgId of the Compensating Resource Manager (CRM) Compensator for the current CRM Clerk.</summary>
		/// <returns>The ProgId of the CRM Compensator for the current CRM Clerk.</returns>
		// Token: 0x17000068 RID: 104
		// (get) Token: 0x06000189 RID: 393 RVA: 0x0000206F File Offset: 0x0000026F
		public string Compensator
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the description of the Compensating Resource Manager (CRM) Compensator for the current CRM Clerk. The description string is the string that was provided by the <see langword="ICrmLogControl::RegisterCompensator" /> method.</summary>
		/// <returns>The description of the CRM Compensator for the current CRM Clerk.</returns>
		// Token: 0x17000069 RID: 105
		// (get) Token: 0x0600018A RID: 394 RVA: 0x0000206F File Offset: 0x0000026F
		public string Description
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the instance class ID (CLSID) of the current Compensating Resource Manager (CRM) Clerk.</summary>
		/// <returns>The instance CLSID of the current CRM Clerk.</returns>
		// Token: 0x1700006A RID: 106
		// (get) Token: 0x0600018B RID: 395 RVA: 0x0000206F File Offset: 0x0000026F
		public string InstanceId
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the unit of work (UOW) of the transaction for the current Compensating Resource Manager (CRM) Clerk.</summary>
		/// <returns>The UOW of the transaction for the current CRM Clerk.</returns>
		// Token: 0x1700006B RID: 107
		// (get) Token: 0x0600018C RID: 396 RVA: 0x0000206F File Offset: 0x0000026F
		public string TransactionUOW
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
