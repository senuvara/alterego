﻿using System;
using Unity;

namespace System.EnterpriseServices.CompensatingResourceManager
{
	/// <summary>Represents the base class for all Compensating Resource Manager (CRM) Compensators.</summary>
	// Token: 0x02000067 RID: 103
	public class Compensator : ServicedComponent
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.CompensatingResourceManager.Compensator" /> class.</summary>
		// Token: 0x06000193 RID: 403 RVA: 0x00002077 File Offset: 0x00000277
		public Compensator()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a value representing the Compensating Resource Manager (CRM) <see cref="T:System.EnterpriseServices.CompensatingResourceManager.Clerk" /> object.</summary>
		/// <returns>The <see cref="T:System.EnterpriseServices.CompensatingResourceManager.Clerk" /> object.</returns>
		// Token: 0x1700006E RID: 110
		// (get) Token: 0x06000194 RID: 404 RVA: 0x0000206F File Offset: 0x0000026F
		public Clerk Clerk
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Delivers a log record to the Compensating Resource Manager (CRM) Compensator during the abort phase.</summary>
		/// <param name="rec">The log record to be delivered. </param>
		/// <returns>
		///     <see langword="true" /> if the delivered record should be forgotten; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000195 RID: 405 RVA: 0x000028D0 File Offset: 0x00000AD0
		public virtual bool AbortRecord(LogRecord rec)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Notifies the Compensating Resource Manager (CRM) Compensator of the abort phase of the transaction completion, and the upcoming delivery of records.</summary>
		/// <param name="fRecovery">
		///       <see langword="true" /> to begin abort phase; otherwise, <see langword="false" />. </param>
		// Token: 0x06000196 RID: 406 RVA: 0x00002077 File Offset: 0x00000277
		public virtual void BeginAbort(bool fRecovery)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Notifies the Compensating Resource Manager (CRM) Compensator of the commit phase of the transaction completion and the upcoming delivery of records.</summary>
		/// <param name="fRecovery">
		///       <see langword="true" /> to begin commit phase; otherwise, <see langword="false" />. </param>
		// Token: 0x06000197 RID: 407 RVA: 0x00002077 File Offset: 0x00000277
		public virtual void BeginCommit(bool fRecovery)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Notifies the Compensating Resource Manager (CRM) Compensator of the prepare phase of the transaction completion and the upcoming delivery of records.</summary>
		// Token: 0x06000198 RID: 408 RVA: 0x00002077 File Offset: 0x00000277
		public virtual void BeginPrepare()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Delivers a log record in forward order during the commit phase.</summary>
		/// <param name="rec">The log record to forward. </param>
		/// <returns>
		///     <see langword="true" /> if the delivered record should be forgotten; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000199 RID: 409 RVA: 0x000028EC File Offset: 0x00000AEC
		public virtual bool CommitRecord(LogRecord rec)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Notifies the Compensating Resource Manager (CRM) Compensator that it has received all the log records available during the abort phase.</summary>
		// Token: 0x0600019A RID: 410 RVA: 0x00002077 File Offset: 0x00000277
		public virtual void EndAbort()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Notifies the Compensating Resource Manager (CRM) Compensator that it has delivered all the log records available during the commit phase.</summary>
		// Token: 0x0600019B RID: 411 RVA: 0x00002077 File Offset: 0x00000277
		public virtual void EndCommit()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Notifies the Compensating Resource Manager (CRM) Compensator that it has had all the log records available during the prepare phase.</summary>
		/// <returns>
		///     <see langword="true" /> if successful; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600019C RID: 412 RVA: 0x00002908 File Offset: 0x00000B08
		public virtual bool EndPrepare()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Delivers a log record in forward order during the prepare phase.</summary>
		/// <param name="rec">The log record to forward. </param>
		/// <returns>
		///     <see langword="true" /> if the delivered record should be forgotten; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600019D RID: 413 RVA: 0x00002924 File Offset: 0x00000B24
		public virtual bool PrepareRecord(LogRecord rec)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}
	}
}
