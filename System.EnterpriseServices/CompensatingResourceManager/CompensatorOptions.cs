﻿using System;

namespace System.EnterpriseServices.CompensatingResourceManager
{
	/// <summary>Specifies flags that control which phases of transaction completion should be received by the Compensating Resource Manager (CRM) Compensator, and whether recovery should fail if questionable transactions remain after recovery has been attempted.</summary>
	// Token: 0x02000064 RID: 100
	[Flags]
	[Serializable]
	public enum CompensatorOptions
	{
		/// <summary>Represents the abort phase.</summary>
		// Token: 0x0400005D RID: 93
		AbortPhase = 4,
		/// <summary>Represents all phases.</summary>
		// Token: 0x0400005E RID: 94
		AllPhases = 7,
		/// <summary>Represents the commit phase.</summary>
		// Token: 0x0400005F RID: 95
		CommitPhase = 2,
		/// <summary>Fails if in-doubt transactions remain after recovery has been attempted.</summary>
		// Token: 0x04000060 RID: 96
		FailIfInDoubtsRemain = 16,
		/// <summary>Represents the prepare phase.</summary>
		// Token: 0x04000061 RID: 97
		PreparePhase = 1
	}
}
