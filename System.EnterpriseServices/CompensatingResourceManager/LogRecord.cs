﻿using System;
using Unity;

namespace System.EnterpriseServices.CompensatingResourceManager
{
	/// <summary>Represents an unstructured log record delivered as a COM+ <see langword="CrmLogRecordRead" /> structure. This class cannot be inherited.</summary>
	// Token: 0x02000068 RID: 104
	public sealed class LogRecord
	{
		// Token: 0x0600019E RID: 414 RVA: 0x00002077 File Offset: 0x00000277
		internal LogRecord()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a value that indicates when the log record was written.</summary>
		/// <returns>A bitwise combination of the <see cref="T:System.EnterpriseServices.CompensatingResourceManager.LogRecordFlags" /> values which provides information about when this record was written.</returns>
		// Token: 0x1700006F RID: 111
		// (get) Token: 0x0600019F RID: 415 RVA: 0x00002940 File Offset: 0x00000B40
		public LogRecordFlags Flags
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (LogRecordFlags)0;
			}
		}

		/// <summary>Gets the log record user data.</summary>
		/// <returns>A single BLOB that contains the user data.</returns>
		// Token: 0x17000070 RID: 112
		// (get) Token: 0x060001A0 RID: 416 RVA: 0x0000206F File Offset: 0x0000026F
		public object Record
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>The sequence number of the log record.</summary>
		/// <returns>An integer value that specifies the sequence number of the log record.</returns>
		// Token: 0x17000071 RID: 113
		// (get) Token: 0x060001A1 RID: 417 RVA: 0x0000295C File Offset: 0x00000B5C
		public int Sequence
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}
	}
}
