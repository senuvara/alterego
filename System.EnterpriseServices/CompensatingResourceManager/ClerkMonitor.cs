﻿using System;
using System.Collections;
using Unity;

namespace System.EnterpriseServices.CompensatingResourceManager
{
	/// <summary>Contains a snapshot of all Clerks active in the process.</summary>
	// Token: 0x02000066 RID: 102
	public sealed class ClerkMonitor : IEnumerable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.CompensatingResourceManager.ClerkMonitor" /> class.</summary>
		// Token: 0x0600018D RID: 397 RVA: 0x00002077 File Offset: 0x00000277
		public ClerkMonitor()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the count of the Clerk monitors in the Compensating Resource Manager (CRM) monitor collection.</summary>
		/// <returns>The number of Clerk monitors in the CRM monitor collection.</returns>
		// Token: 0x1700006C RID: 108
		// (get) Token: 0x0600018E RID: 398 RVA: 0x000028B4 File Offset: 0x00000AB4
		public int Count
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		// Token: 0x0600018F RID: 399 RVA: 0x0000206F File Offset: 0x0000026F
		public ClerkInfo get_Item(int index)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the <see cref="T:System.EnterpriseServices.CompensatingResourceManager.ClerkInfo" /> object for this <see cref="T:System.EnterpriseServices.CompensatingResourceManager.ClerkMonitor" />.</summary>
		/// <param name="index">The numeric index that identifies the <see cref="T:System.EnterpriseServices.CompensatingResourceManager.ClerkMonitor" />. </param>
		/// <returns>The <see cref="T:System.EnterpriseServices.CompensatingResourceManager.ClerkInfo" /> object for this <see cref="T:System.EnterpriseServices.CompensatingResourceManager.ClerkMonitor" />.</returns>
		// Token: 0x1700006D RID: 109
		public ClerkInfo this[string index]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Returns the enumeration of the clerks in the Compensating Resource Manager (CRM) monitor collection.</summary>
		/// <returns>An enumerator describing the clerks in the collection.</returns>
		// Token: 0x06000191 RID: 401 RVA: 0x0000206F File Offset: 0x0000026F
		public IEnumerator GetEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the Clerks collection object, which is a snapshot of the current state of the Clerks.</summary>
		// Token: 0x06000192 RID: 402 RVA: 0x00002077 File Offset: 0x00000277
		public void Populate()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
