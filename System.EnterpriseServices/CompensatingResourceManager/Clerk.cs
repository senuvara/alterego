﻿using System;
using Unity;

namespace System.EnterpriseServices.CompensatingResourceManager
{
	/// <summary>Writes records of transactional actions to a log.</summary>
	// Token: 0x02000063 RID: 99
	public sealed class Clerk
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.CompensatingResourceManager.Clerk" /> class.</summary>
		/// <param name="compensator">The name of the compensator. </param>
		/// <param name="description">The description of the compensator. </param>
		/// <param name="flags">A bitwise combination of the <see cref="T:System.EnterpriseServices.CompensatingResourceManager.CompensatorOptions" /> values. </param>
		// Token: 0x0600017E RID: 382 RVA: 0x00002077 File Offset: 0x00000277
		public Clerk(string compensator, string description, CompensatorOptions flags)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.CompensatingResourceManager.Clerk" /> class.</summary>
		/// <param name="compensator">A type that represents the compensator. </param>
		/// <param name="description">The description of the compensator. </param>
		/// <param name="flags">A bitwise combination of the <see cref="T:System.EnterpriseServices.CompensatingResourceManager.CompensatorOptions" /> values. </param>
		// Token: 0x0600017F RID: 383 RVA: 0x00002077 File Offset: 0x00000277
		public Clerk(Type compensator, string description, CompensatorOptions flags)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the number of log records.</summary>
		/// <returns>The number of log records.</returns>
		// Token: 0x17000064 RID: 100
		// (get) Token: 0x06000180 RID: 384 RVA: 0x00002898 File Offset: 0x00000A98
		public int LogRecordCount
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets a value representing the transaction unit of work (UOW).</summary>
		/// <returns>A GUID representing the UOW.</returns>
		// Token: 0x17000065 RID: 101
		// (get) Token: 0x06000181 RID: 385 RVA: 0x0000206F File Offset: 0x0000026F
		public string TransactionUOW
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Forces all log records to disk.</summary>
		// Token: 0x06000182 RID: 386 RVA: 0x00002077 File Offset: 0x00000277
		public void ForceLog()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Performs an immediate abort call on the transaction.</summary>
		// Token: 0x06000183 RID: 387 RVA: 0x00002077 File Offset: 0x00000277
		public void ForceTransactionToAbort()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Does not deliver the last log record that was written by this instance of this interface.</summary>
		// Token: 0x06000184 RID: 388 RVA: 0x00002077 File Offset: 0x00000277
		public void ForgetLogRecord()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Writes unstructured log records to the log.</summary>
		/// <param name="record">The log record to write to the log. </param>
		// Token: 0x06000185 RID: 389 RVA: 0x00002077 File Offset: 0x00000277
		public void WriteLogRecord(object record)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
