﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices.CompensatingResourceManager
{
	/// <summary>Enables Compensating Resource Manger (CRM) on the tagged application.</summary>
	// Token: 0x02000062 RID: 98
	[AttributeUsage(AttributeTargets.Assembly, Inherited = true)]
	[ComVisible(false)]
	[ProgId("System.EnterpriseServices.Crm.ApplicationCrmEnabledAttribute")]
	public sealed class ApplicationCrmEnabledAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.CompensatingResourceManager.ApplicationCrmEnabledAttribute" /> class, setting the <see cref="P:System.EnterpriseServices.CompensatingResourceManager.ApplicationCrmEnabledAttribute.Value" /> property to <see langword="true" />.</summary>
		// Token: 0x0600017B RID: 379 RVA: 0x00002050 File Offset: 0x00000250
		public ApplicationCrmEnabledAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.CompensatingResourceManager.ApplicationCrmEnabledAttribute" /> class, optionally setting the <see cref="P:System.EnterpriseServices.CompensatingResourceManager.ApplicationCrmEnabledAttribute.Value" /> property to <see langword="false" />.</summary>
		/// <param name="val">
		///       <see langword="true" /> to enable Compensating Resource Manager (CRM); otherwise, <see langword="false" />. </param>
		// Token: 0x0600017C RID: 380 RVA: 0x00002050 File Offset: 0x00000250
		public ApplicationCrmEnabledAttribute(bool val)
		{
		}

		/// <summary>Enables or disables Compensating Resource Manager (CRM) on the tagged application.</summary>
		/// <returns>
		///     <see langword="true" /> if CRM is enabled; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x17000063 RID: 99
		// (get) Token: 0x0600017D RID: 381 RVA: 0x0000287C File Offset: 0x00000A7C
		public bool Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}
	}
}
