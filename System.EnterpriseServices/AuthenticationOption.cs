﻿using System;

namespace System.EnterpriseServices
{
	/// <summary>Specifies the remote procedure call (RPC) authentication mechanism. Applicable only when the <see cref="T:System.EnterpriseServices.ActivationOption" /> is set to <see langword="Server" />.</summary>
	// Token: 0x02000015 RID: 21
	[Serializable]
	public enum AuthenticationOption
	{
		/// <summary>Authenticates credentials at the beginning of every call.</summary>
		// Token: 0x04000035 RID: 53
		Call = 3,
		/// <summary>Authenticates credentials only when the connection is made.</summary>
		// Token: 0x04000036 RID: 54
		Connect = 2,
		/// <summary>Uses the default authentication level for the specified authentication service. In COM+, this setting is provided by the <see langword="DefaultAuthenticationLevel" /> property in the <see langword="LocalComputer" /> collection.</summary>
		// Token: 0x04000037 RID: 55
		Default = 0,
		/// <summary>Authenticates credentials and verifies that no call data has been modified in transit.</summary>
		// Token: 0x04000038 RID: 56
		Integrity = 5,
		/// <summary>Authentication does not occur.</summary>
		// Token: 0x04000039 RID: 57
		None = 1,
		/// <summary>Authenticates credentials and verifies that all call data is received.</summary>
		// Token: 0x0400003A RID: 58
		Packet = 4,
		/// <summary>Authenticates credentials and encrypts the packet, including the data and the sender's identity and signature.</summary>
		// Token: 0x0400003B RID: 59
		Privacy = 6
	}
}
