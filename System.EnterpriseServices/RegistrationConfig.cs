﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Provides configuration information for installing assemblies into the COM+ catalog.</summary>
	// Token: 0x02000035 RID: 53
	[Guid("36dcda30-dc3b-4d93-be42-90b2d74c64e7")]
	[Serializable]
	public class RegistrationConfig
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.RegistrationConfig" /> class.</summary>
		// Token: 0x060000B3 RID: 179 RVA: 0x00002077 File Offset: 0x00000277
		public RegistrationConfig()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the name of the COM+ application in which the assembly is to be installed.</summary>
		/// <returns>The name of the COM+ application in which the assembly is to be installed.</returns>
		// Token: 0x17000042 RID: 66
		// (get) Token: 0x060000B4 RID: 180 RVA: 0x0000206F File Offset: 0x0000026F
		// (set) Token: 0x060000B5 RID: 181 RVA: 0x00002077 File Offset: 0x00000277
		public string Application
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the root directory of the application.</summary>
		/// <returns>The name of the root directory of the application.</returns>
		// Token: 0x17000043 RID: 67
		// (get) Token: 0x060000B6 RID: 182 RVA: 0x0000206F File Offset: 0x0000026F
		// (set) Token: 0x060000B7 RID: 183 RVA: 0x00002077 File Offset: 0x00000277
		public string ApplicationRootDirectory
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the file name of the assembly to install.</summary>
		/// <returns>The file name of the assembly to install.</returns>
		// Token: 0x17000044 RID: 68
		// (get) Token: 0x060000B8 RID: 184 RVA: 0x0000206F File Offset: 0x0000026F
		// (set) Token: 0x060000B9 RID: 185 RVA: 0x00002077 File Offset: 0x00000277
		public string AssemblyFile
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a flag that indicates how to install the assembly.</summary>
		/// <returns>One of the <see cref="T:System.EnterpriseServices.InstallationFlags" /> values.</returns>
		// Token: 0x17000045 RID: 69
		// (get) Token: 0x060000BA RID: 186 RVA: 0x00002630 File Offset: 0x00000830
		// (set) Token: 0x060000BB RID: 187 RVA: 0x00002077 File Offset: 0x00000277
		public InstallationFlags InstallationFlags
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return InstallationFlags.Default;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the COM+ partition.</summary>
		/// <returns>The name of the COM+ partition.</returns>
		// Token: 0x17000046 RID: 70
		// (get) Token: 0x060000BC RID: 188 RVA: 0x0000206F File Offset: 0x0000026F
		// (set) Token: 0x060000BD RID: 189 RVA: 0x00002077 File Offset: 0x00000277
		public string Partition
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the output Type Library Exporter (Tlbexp.exe) file.</summary>
		/// <returns>The name of the output Type Library Exporter (Tlbexp.exe) file.</returns>
		// Token: 0x17000047 RID: 71
		// (get) Token: 0x060000BE RID: 190 RVA: 0x0000206F File Offset: 0x0000026F
		// (set) Token: 0x060000BF RID: 191 RVA: 0x00002077 File Offset: 0x00000277
		public string TypeLibrary
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
