﻿using System;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>The exception that is thrown when a registration error is detected.</summary>
	// Token: 0x02000037 RID: 55
	[Serializable]
	public sealed class RegistrationException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.RegistrationException" /> class.</summary>
		// Token: 0x060000C6 RID: 198 RVA: 0x00002077 File Offset: 0x00000277
		public RegistrationException()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.RegistrationException" /> class with a specified error message.</summary>
		/// <param name="msg">The message displayed to the client when the exception is thrown. </param>
		// Token: 0x060000C7 RID: 199 RVA: 0x00002077 File Offset: 0x00000277
		public RegistrationException(string msg)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.RegistrationException" /> class with a specified error message and nested exception.</summary>
		/// <param name="msg">The message displayed to the client when the exception is thrown. </param>
		/// <param name="inner">The nested exception.</param>
		// Token: 0x060000C8 RID: 200 RVA: 0x00002077 File Offset: 0x00000277
		public RegistrationException(string msg, Exception inner)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets an array of <see cref="T:System.EnterpriseServices.RegistrationErrorInfo" /> objects that describe registration errors.</summary>
		/// <returns>The array of <see cref="T:System.EnterpriseServices.RegistrationErrorInfo" /> objects.</returns>
		// Token: 0x1700004D RID: 77
		// (get) Token: 0x060000C9 RID: 201 RVA: 0x0000206F File Offset: 0x0000026F
		public RegistrationErrorInfo[] ErrorInfo
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
