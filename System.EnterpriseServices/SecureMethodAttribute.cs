﻿using System;
using System.Runtime.InteropServices;

namespace System.EnterpriseServices
{
	/// <summary>Ensures that the infrastructure calls through an interface for a method or for each method in a class when using the security service. Classes need to use interfaces to use security services. This class cannot be inherited.</summary>
	// Token: 0x0200003E RID: 62
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
	[ComVisible(false)]
	public sealed class SecureMethodAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.SecureMethodAttribute" /> class.</summary>
		// Token: 0x060000F4 RID: 244 RVA: 0x00002050 File Offset: 0x00000250
		public SecureMethodAttribute()
		{
		}
	}
}
