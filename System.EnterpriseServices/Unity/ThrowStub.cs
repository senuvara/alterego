﻿using System;

namespace Unity
{
	// Token: 0x0200006B RID: 107
	internal sealed class ThrowStub : ObjectDisposedException
	{
		// Token: 0x060001A2 RID: 418 RVA: 0x00002977 File Offset: 0x00000B77
		public static void ThrowNotSupportedException()
		{
			throw new PlatformNotSupportedException();
		}
	}
}
