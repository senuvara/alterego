﻿using System;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Contains information that regards an identity in a COM+ call chain.</summary>
	// Token: 0x02000041 RID: 65
	public sealed class SecurityIdentity
	{
		// Token: 0x06000103 RID: 259 RVA: 0x00002077 File Offset: 0x00000277
		internal SecurityIdentity()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the name of the user described by this identity.</summary>
		/// <returns>The name of the user described by this identity.</returns>
		// Token: 0x1700005A RID: 90
		// (get) Token: 0x06000104 RID: 260 RVA: 0x0000206F File Offset: 0x0000026F
		public string AccountName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the authentication level of the user described by this identity.</summary>
		/// <returns>One of the <see cref="T:System.EnterpriseServices.AuthenticationOption" /> values.</returns>
		// Token: 0x1700005B RID: 91
		// (get) Token: 0x06000105 RID: 261 RVA: 0x000027B8 File Offset: 0x000009B8
		public AuthenticationOption AuthenticationLevel
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return AuthenticationOption.Default;
			}
		}

		/// <summary>Gets the authentication service described by this identity.</summary>
		/// <returns>The authentication service described by this identity.</returns>
		// Token: 0x1700005C RID: 92
		// (get) Token: 0x06000106 RID: 262 RVA: 0x000027D4 File Offset: 0x000009D4
		public int AuthenticationService
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the impersonation level of the user described by this identity.</summary>
		/// <returns>A <see cref="T:System.EnterpriseServices.ImpersonationLevelOption" /> value.</returns>
		// Token: 0x1700005D RID: 93
		// (get) Token: 0x06000107 RID: 263 RVA: 0x000027F0 File Offset: 0x000009F0
		public ImpersonationLevelOption ImpersonationLevel
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ImpersonationLevelOption.Default;
			}
		}
	}
}
