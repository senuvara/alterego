﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Specifies the application ID (as a GUID) for this assembly. This class cannot be inherited.</summary>
	// Token: 0x02000002 RID: 2
	[AttributeUsage(AttributeTargets.Assembly, Inherited = true)]
	[ComVisible(false)]
	public sealed class ApplicationIDAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.ApplicationIDAttribute" /> class specifying the GUID representing the application ID for the COM+ application.</summary>
		/// <param name="guid">The GUID associated with the COM+ application. </param>
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public ApplicationIDAttribute(string guid)
		{
		}

		/// <summary>Gets the GUID of the COM+ application.</summary>
		/// <returns>The GUID representing the COM+ application.</returns>
		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000002 RID: 2 RVA: 0x00002054 File Offset: 0x00000254
		public Guid Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Guid);
			}
		}
	}
}
