﻿using System;

namespace System.EnterpriseServices
{
	/// <summary>Specifies the type of automatic synchronization requested by the component.</summary>
	// Token: 0x02000010 RID: 16
	[Serializable]
	public enum SynchronizationOption
	{
		/// <summary>COM+ ignores the synchronization requirements of the component when determining context for the object.</summary>
		// Token: 0x04000024 RID: 36
		Disabled,
		/// <summary>An object with this value never participates in synchronization, regardless of the status of its caller. This setting is only available for components that are non-transactional and do not use just-in-time (JIT) activation.</summary>
		// Token: 0x04000025 RID: 37
		NotSupported,
		/// <summary>Ensures that all objects created from the component are synchronized.</summary>
		// Token: 0x04000026 RID: 38
		Required = 3,
		/// <summary>An object with this value must participate in a new synchronization where COM+ manages contexts and apartments on behalf of all components involved in the call.</summary>
		// Token: 0x04000027 RID: 39
		RequiresNew,
		/// <summary>An object with this value participates in synchronization, if it exists.</summary>
		// Token: 0x04000028 RID: 40
		Supported = 2
	}
}
