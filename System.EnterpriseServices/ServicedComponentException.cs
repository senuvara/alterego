﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>The exception that is thrown when an error is detected in a serviced component.</summary>
	// Token: 0x02000043 RID: 67
	[ComVisible(false)]
	[Serializable]
	public sealed class ServicedComponentException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.ServicedComponentException" /> class.</summary>
		// Token: 0x06000110 RID: 272 RVA: 0x00002077 File Offset: 0x00000277
		public ServicedComponentException()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.ServicedComponentException" /> class with a specified error message.</summary>
		/// <param name="message">The message displayed to the client when the exception is thrown. </param>
		// Token: 0x06000111 RID: 273 RVA: 0x00002077 File Offset: 0x00000277
		public ServicedComponentException(string message)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.ServicedComponentException" /> class.</summary>
		/// <param name="message">The message displayed to the client when the exception is thrown. </param>
		/// <param name="innerException">The <see cref="P:System.Exception.InnerException" />, if any, that threw the current exception. </param>
		// Token: 0x06000112 RID: 274 RVA: 0x00002077 File Offset: 0x00000277
		public ServicedComponentException(string message, Exception innerException)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
