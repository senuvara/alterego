﻿using System;

namespace System.EnterpriseServices
{
	/// <summary>Specifies the manner in which serviced components are activated in the application.</summary>
	// Token: 0x02000008 RID: 8
	[Serializable]
	public enum ActivationOption
	{
		/// <summary>Specifies that serviced components in the marked application are activated in the creator's process.</summary>
		// Token: 0x0400000D RID: 13
		Library,
		/// <summary>Specifies that serviced components in the marked application are activated in a system-provided process.</summary>
		// Token: 0x0400000E RID: 14
		Server
	}
}
