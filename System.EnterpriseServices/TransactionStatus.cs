﻿using System;
using System.Runtime.InteropServices;

namespace System.EnterpriseServices
{
	/// <summary>Indicates the transaction status.</summary>
	// Token: 0x02000045 RID: 69
	[ComVisible(false)]
	[Serializable]
	public enum TransactionStatus
	{
		/// <summary>The transaction is aborted.</summary>
		// Token: 0x04000057 RID: 87
		Aborted = 4,
		/// <summary>The transaction is in the process of aborting.</summary>
		// Token: 0x04000058 RID: 88
		Aborting = 3,
		/// <summary>The transaction has committed.</summary>
		// Token: 0x04000059 RID: 89
		Commited = 0,
		/// <summary>The transaction has neither committed nor aborted.</summary>
		// Token: 0x0400005A RID: 90
		LocallyOk,
		/// <summary>No transactions are being used through <see cref="M:System.EnterpriseServices.ServiceDomain.Enter(System.EnterpriseServices.ServiceConfig)" />.</summary>
		// Token: 0x0400005B RID: 91
		NoTransaction
	}
}
