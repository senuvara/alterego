﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Controls access to shared property groups. This class cannot be inherited.</summary>
	// Token: 0x02000048 RID: 72
	[ComVisible(false)]
	public sealed class SharedPropertyGroupManager : IEnumerable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.SharedPropertyGroupManager" /> class.</summary>
		// Token: 0x0600011E RID: 286 RVA: 0x00002077 File Offset: 0x00000277
		public SharedPropertyGroupManager()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Finds or creates a property group with the given information.</summary>
		/// <param name="name">The name of requested property. </param>
		/// <param name="dwIsoMode">One of the <see cref="T:System.EnterpriseServices.PropertyLockMode" /> values. See the Remarks section for more information. </param>
		/// <param name="dwRelMode">One of the <see cref="T:System.EnterpriseServices.PropertyReleaseMode" /> values. See the Remarks section for more information. </param>
		/// <param name="fExist">When this method returns, contains <see langword="true" /> if the property already existed; <see langword="false" /> if the call created the property. </param>
		/// <returns>The requested <see cref="T:System.EnterpriseServices.SharedPropertyGroup" />.</returns>
		// Token: 0x0600011F RID: 287 RVA: 0x0000206F File Offset: 0x0000026F
		public SharedPropertyGroup CreatePropertyGroup(string name, ref PropertyLockMode dwIsoMode, ref PropertyReleaseMode dwRelMode, out bool fExist)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Retrieves the enumeration interface for the collection.</summary>
		/// <returns>The enumerator interface for the collection.</returns>
		// Token: 0x06000120 RID: 288 RVA: 0x0000206F File Offset: 0x0000026F
		public IEnumerator GetEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Finds the property group with the given name.</summary>
		/// <param name="name">The name of requested property. </param>
		/// <returns>The requested <see cref="T:System.EnterpriseServices.SharedPropertyGroup" />.</returns>
		// Token: 0x06000121 RID: 289 RVA: 0x0000206F File Offset: 0x0000026F
		public SharedPropertyGroup Group(string name)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
