﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices.Internal
{
	/// <summary>Switches into the given application domain, which the object should be bound to, and does a callback on the given function.</summary>
	// Token: 0x0200004A RID: 74
	[Guid("ef24f689-14f8-4d92-b4af-d7b1f0e70fd4")]
	public class AppDomainHelper
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.Internal.AppDomainHelper" /> class.</summary>
		// Token: 0x06000125 RID: 293 RVA: 0x00002077 File Offset: 0x00000277
		public AppDomainHelper()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
