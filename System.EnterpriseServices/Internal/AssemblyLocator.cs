﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices.Internal
{
	/// <summary>Locates an assembly and returns information about its modules.</summary>
	// Token: 0x0200004B RID: 75
	[Guid("458aa3b5-265a-4b75-bc05-9bea4630cf18")]
	public class AssemblyLocator : MarshalByRefObject
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.Internal.AssemblyLocator" /> class.</summary>
		// Token: 0x06000126 RID: 294 RVA: 0x00002077 File Offset: 0x00000277
		public AssemblyLocator()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
