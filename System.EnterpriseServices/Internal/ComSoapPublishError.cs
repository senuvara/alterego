﻿using System;
using Unity;

namespace System.EnterpriseServices.Internal
{
	/// <summary>Error handler for publishing SOAP-enabled services in COM+ applications.</summary>
	// Token: 0x02000051 RID: 81
	public class ComSoapPublishError
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.Internal.ComSoapPublishError" /> class.</summary>
		// Token: 0x06000137 RID: 311 RVA: 0x00002077 File Offset: 0x00000277
		public ComSoapPublishError()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Writes to an event log an error encountered while publishing SOAP-enabled COM interfaces in COM+ applications.</summary>
		/// <param name="s">An error message to be written to the event log.</param>
		// Token: 0x06000138 RID: 312 RVA: 0x00002077 File Offset: 0x00000277
		public static void Report(string s)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
