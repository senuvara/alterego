﻿using System;
using System.Runtime.InteropServices;

namespace System.EnterpriseServices
{
	/// <summary>Indicates whether all work submitted by <see cref="T:System.EnterpriseServices.Activity" /> should be bound to only one single-threaded apartment (STA). This enumeration has no impact on the multithreaded apartment (MTA).</summary>
	// Token: 0x0200000B RID: 11
	[ComVisible(false)]
	[Serializable]
	public enum BindingOption
	{
		/// <summary>The work submitted by the activity is bound to a single STA.</summary>
		// Token: 0x04000010 RID: 16
		BindingToPoolThread = 1,
		/// <summary>The work submitted by the activity is not bound to a single STA.</summary>
		// Token: 0x04000011 RID: 17
		NoBinding = 0
	}
}
