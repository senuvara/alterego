﻿using System;

namespace System.EnterpriseServices
{
	/// <summary>Flags used with the <see cref="T:System.EnterpriseServices.RegistrationHelper" /> class.</summary>
	// Token: 0x02000026 RID: 38
	[Flags]
	[Serializable]
	public enum InstallationFlags
	{
		/// <summary>Should not be used.</summary>
		// Token: 0x04000046 RID: 70
		Configure = 1024,
		/// <summary>Configures components only, do not configure methods or interfaces.</summary>
		// Token: 0x04000047 RID: 71
		ConfigureComponentsOnly = 16,
		/// <summary>Creates the target application. An error occurs if the target already exists.</summary>
		// Token: 0x04000048 RID: 72
		CreateTargetApplication = 2,
		/// <summary>Do the default installation, which configures, installs, and registers, and assumes that the application already exists.</summary>
		// Token: 0x04000049 RID: 73
		Default = 0,
		/// <summary>Do not export the type library; one can be found either by the generated or supplied type library name.</summary>
		// Token: 0x0400004A RID: 74
		ExpectExistingTypeLib = 1,
		/// <summary>Creates the application if it does not exist; otherwise use the existing application.</summary>
		// Token: 0x0400004B RID: 75
		FindOrCreateTargetApplication = 4,
		/// <summary>Should not be used.</summary>
		// Token: 0x0400004C RID: 76
		Install = 512,
		/// <summary>If using an existing application, ensures that the properties on this application match those in the assembly.</summary>
		// Token: 0x0400004D RID: 77
		ReconfigureExistingApplication = 8,
		/// <summary>Should not be used.</summary>
		// Token: 0x0400004E RID: 78
		Register = 256,
		/// <summary>When alert text is encountered, writes it to the Console.</summary>
		// Token: 0x0400004F RID: 79
		ReportWarningsToConsole = 32
	}
}
