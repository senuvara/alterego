﻿using System;
using System.Runtime.InteropServices;

namespace System.EnterpriseServices
{
	/// <summary>Specifies the mode for accessing shared properties in the shared property group manager.</summary>
	// Token: 0x02000033 RID: 51
	[ComVisible(false)]
	[Serializable]
	public enum PropertyLockMode
	{
		/// <summary>Locks all the properties in the shared property group for exclusive use by the caller, as long as the caller's current method is executing.</summary>
		// Token: 0x04000051 RID: 81
		Method = 1,
		/// <summary>Locks a property during a get or set, assuring that every get or set operation on a shared property is atomic.</summary>
		// Token: 0x04000052 RID: 82
		SetGet = 0
	}
}
