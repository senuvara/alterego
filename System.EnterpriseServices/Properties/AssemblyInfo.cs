﻿using System;
using System.Configuration.Assemblies;
using System.Diagnostics;
using System.EnterpriseServices;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

[assembly: AssemblyAlgorithmId(AssemblyHashAlgorithm.None)]
[assembly: AssemblyVersion("4.0.0.0")]
[assembly: SecurityRules(SecurityRuleSet.Level1)]
[assembly: ApplicationID("1e246775-2281-484f-8ad4-044c15b86eb7")]
[assembly: ApplicationName(".NET Utilities")]
[assembly: AssemblyCompany("Mono development team")]
[assembly: AssemblyCopyright("(c) Various Mono authors")]
[assembly: AssemblyDefaultAlias("System.EnterpriseServices.dll")]
[assembly: AssemblyDescription("System.EnterpriseServices.dll")]
[assembly: AssemblyFileVersion("4.7.2558.0")]
[assembly: AssemblyInformationalVersion("4.7.2558.0")]
[assembly: AssemblyProduct("Mono Common Language Infrastructure")]
[assembly: AssemblyTitle("System.EnterpriseServices.dll")]
[assembly: NeutralResourcesLanguage("en-US")]
[assembly: SatelliteContractVersion("4.0.0.0")]
[assembly: ComCompatibleVersion(1, 0, 3300, 0)]
[assembly: ComVisible(true)]
[assembly: DefaultDllImportSearchPaths(DllImportSearchPath.System32 | DllImportSearchPath.AssemblyDirectory)]
[assembly: Guid("4fb2d46f-efc8-4643-bcd0-6e5bfa6a174c")]
[assembly: CLSCompliant(true)]
[assembly: TypeLibVersion(2, 4)]
