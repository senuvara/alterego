﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Sets the queuing exception class for the queued class. This class cannot be inherited.</summary>
	// Token: 0x02000023 RID: 35
	[ComVisible(false)]
	[AttributeUsage(AttributeTargets.Class, Inherited = true)]
	public sealed class ExceptionClassAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.ExceptionClassAttribute" /> class.</summary>
		/// <param name="name">The name of the exception class for the player to activate and play back before the message is routed to the dead letter queue. </param>
		// Token: 0x06000084 RID: 132 RVA: 0x00002050 File Offset: 0x00000250
		public ExceptionClassAttribute(string name)
		{
		}

		/// <summary>Gets the name of the exception class for the player to activate and play back before the message is routed to the dead letter queue.</summary>
		/// <returns>The name of the exception class for the player to activate and play back before the message is routed to the dead letter queue.</returns>
		// Token: 0x17000037 RID: 55
		// (get) Token: 0x06000085 RID: 133 RVA: 0x0000206F File Offset: 0x0000026F
		public string Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
