﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Specifies the type of transaction that is available to the attributed object. Permissible values are members of the <see cref="T:System.EnterpriseServices.TransactionOption" /> enumeration.</summary>
	// Token: 0x0200003B RID: 59
	[AttributeUsage(AttributeTargets.Class, Inherited = true)]
	[ComVisible(false)]
	public sealed class TransactionAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.TransactionAttribute" /> class, setting the component's requested transaction type to <see cref="F:System.EnterpriseServices.TransactionOption.Required" />.</summary>
		// Token: 0x060000E6 RID: 230 RVA: 0x00002050 File Offset: 0x00000250
		public TransactionAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.TransactionAttribute" /> class, specifying the transaction type.</summary>
		/// <param name="val">The specified transaction type, a <see cref="T:System.EnterpriseServices.TransactionOption" /> value. </param>
		// Token: 0x060000E7 RID: 231 RVA: 0x00002050 File Offset: 0x00000250
		public TransactionAttribute(TransactionOption val)
		{
		}

		/// <summary>Gets or sets the transaction isolation level.</summary>
		/// <returns>One of the <see cref="T:System.EnterpriseServices.TransactionIsolationLevel" /> values.</returns>
		// Token: 0x1700004E RID: 78
		// (get) Token: 0x060000E8 RID: 232 RVA: 0x000026A0 File Offset: 0x000008A0
		// (set) Token: 0x060000E9 RID: 233 RVA: 0x00002077 File Offset: 0x00000277
		public TransactionIsolationLevel Isolation
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return TransactionIsolationLevel.Any;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the time-out for this transaction.</summary>
		/// <returns>The transaction time-out in seconds.</returns>
		// Token: 0x1700004F RID: 79
		// (get) Token: 0x060000EA RID: 234 RVA: 0x000026BC File Offset: 0x000008BC
		// (set) Token: 0x060000EB RID: 235 RVA: 0x00002077 File Offset: 0x00000277
		public int Timeout
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the <see cref="T:System.EnterpriseServices.TransactionOption" /> value for the transaction, optionally disabling the transaction service.</summary>
		/// <returns>The specified transaction type, a <see cref="T:System.EnterpriseServices.TransactionOption" /> value.</returns>
		// Token: 0x17000050 RID: 80
		// (get) Token: 0x060000EC RID: 236 RVA: 0x000026D8 File Offset: 0x000008D8
		public TransactionOption Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return TransactionOption.Disabled;
			}
		}
	}
}
