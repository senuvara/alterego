﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Sets the synchronization value of the component. This class cannot be inherited.</summary>
	// Token: 0x02000049 RID: 73
	[AttributeUsage(AttributeTargets.Class, Inherited = true)]
	[ComVisible(false)]
	public sealed class SynchronizationAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.SynchronizationAttribute" /> class with the default <see cref="T:System.EnterpriseServices.SynchronizationOption" />.</summary>
		// Token: 0x06000122 RID: 290 RVA: 0x00002050 File Offset: 0x00000250
		public SynchronizationAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.SynchronizationAttribute" /> class with the specified <see cref="T:System.EnterpriseServices.SynchronizationOption" />.</summary>
		/// <param name="val">One of the <see cref="T:System.EnterpriseServices.SynchronizationOption" /> values. </param>
		// Token: 0x06000123 RID: 291 RVA: 0x00002050 File Offset: 0x00000250
		public SynchronizationAttribute(SynchronizationOption val)
		{
		}

		/// <summary>Gets the current setting of the <see cref="P:System.EnterpriseServices.SynchronizationAttribute.Value" /> property.</summary>
		/// <returns>One of the <see cref="T:System.EnterpriseServices.SynchronizationOption" /> values. The default is <see langword="Required" />.</returns>
		// Token: 0x17000062 RID: 98
		// (get) Token: 0x06000124 RID: 292 RVA: 0x00002844 File Offset: 0x00000A44
		public SynchronizationOption Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return SynchronizationOption.Disabled;
			}
		}
	}
}
