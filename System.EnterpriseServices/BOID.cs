﻿using System;
using System.Runtime.InteropServices;

namespace System.EnterpriseServices
{
	/// <summary>Represents the unit of work associated with a transaction. This structure is used in <see cref="T:System.EnterpriseServices.XACTTRANSINFO" />.</summary>
	// Token: 0x02000005 RID: 5
	[ComVisible(false)]
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct BOID
	{
		/// <summary>Represents an array that contains the data.</summary>
		// Token: 0x04000001 RID: 1
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 0)]
		public byte[] rgb;
	}
}
