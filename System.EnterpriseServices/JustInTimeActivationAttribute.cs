﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Turns just-in-time (JIT) activation on or off. This class cannot be inherited.</summary>
	// Token: 0x0200002E RID: 46
	[ComVisible(false)]
	[AttributeUsage(AttributeTargets.Class, Inherited = true)]
	public sealed class JustInTimeActivationAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.JustInTimeActivationAttribute" /> class. The default constructor enables just-in-time (JIT) activation.</summary>
		// Token: 0x0600009A RID: 154 RVA: 0x00002050 File Offset: 0x00000250
		public JustInTimeActivationAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.JustInTimeActivationAttribute" /> class, optionally allowing the disabling of just-in-time (JIT) activation by passing <see langword="false" /> as the parameter.</summary>
		/// <param name="val">
		///       <see langword="true" /> to enable JIT activation; otherwise, <see langword="false" />. </param>
		// Token: 0x0600009B RID: 155 RVA: 0x00002050 File Offset: 0x00000250
		public JustInTimeActivationAttribute(bool val)
		{
		}

		/// <summary>Gets the value of the <see cref="T:System.EnterpriseServices.JustInTimeActivationAttribute" /> setting.</summary>
		/// <returns>
		///     <see langword="true" /> if JIT activation is enabled; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x1700003B RID: 59
		// (get) Token: 0x0600009C RID: 156 RVA: 0x00002518 File Offset: 0x00000718
		public bool Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}
	}
}
