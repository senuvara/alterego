﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Enables COM+ object construction support. This class cannot be inherited.</summary>
	// Token: 0x0200001D RID: 29
	[ComVisible(false)]
	[AttributeUsage(AttributeTargets.Class, Inherited = true)]
	public sealed class ConstructionEnabledAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.ConstructionEnabledAttribute" /> class and initializes the default settings for <see cref="P:System.EnterpriseServices.ConstructionEnabledAttribute.Enabled" /> and <see cref="P:System.EnterpriseServices.ConstructionEnabledAttribute.Default" />.</summary>
		// Token: 0x0600005C RID: 92 RVA: 0x00002050 File Offset: 0x00000250
		public ConstructionEnabledAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.ConstructionEnabledAttribute" /> class, setting <see cref="P:System.EnterpriseServices.ConstructionEnabledAttribute.Enabled" /> to the specified value.</summary>
		/// <param name="val">
		///       <see langword="true" /> to enable COM+ object construction support; otherwise, <see langword="false" />. </param>
		// Token: 0x0600005D RID: 93 RVA: 0x00002050 File Offset: 0x00000250
		public ConstructionEnabledAttribute(bool val)
		{
		}

		/// <summary>Gets or sets a default value for the constructor string.</summary>
		/// <returns>The value to be used for the default constructor string. The default is an empty string ("").</returns>
		// Token: 0x17000025 RID: 37
		// (get) Token: 0x0600005E RID: 94 RVA: 0x0000206F File Offset: 0x0000026F
		// (set) Token: 0x0600005F RID: 95 RVA: 0x00002077 File Offset: 0x00000277
		public string Default
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether COM+ object construction support is enabled.</summary>
		/// <returns>
		///     <see langword="true" /> if COM+ object construction support is enabled; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000060 RID: 96 RVA: 0x00002320 File Offset: 0x00000520
		// (set) Token: 0x06000061 RID: 97 RVA: 0x00002077 File Offset: 0x00000277
		public bool Enabled
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
