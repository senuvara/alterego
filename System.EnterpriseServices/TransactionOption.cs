﻿using System;

namespace System.EnterpriseServices
{
	/// <summary>Specifies the automatic transaction type requested by the component.</summary>
	// Token: 0x02000012 RID: 18
	[Serializable]
	public enum TransactionOption
	{
		/// <summary>Ignores any transaction in the current context.</summary>
		// Token: 0x0400002F RID: 47
		Disabled,
		/// <summary>Creates the component in a context with no governing transaction.</summary>
		// Token: 0x04000030 RID: 48
		NotSupported,
		/// <summary>Shares a transaction, if one exists, and creates a new transaction if necessary.</summary>
		// Token: 0x04000031 RID: 49
		Required = 3,
		/// <summary>Creates the component with a new transaction, regardless of the state of the current context.</summary>
		// Token: 0x04000032 RID: 50
		RequiresNew,
		/// <summary>Shares a transaction, if one exists.</summary>
		// Token: 0x04000033 RID: 51
		Supported = 2
	}
}
