﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Enables queuing support for the marked interface. This class cannot be inherited.</summary>
	// Token: 0x02000027 RID: 39
	[ComVisible(false)]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface, Inherited = true, AllowMultiple = true)]
	public sealed class InterfaceQueuingAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.InterfaceQueuingAttribute" /> class setting the <see cref="P:System.EnterpriseServices.InterfaceQueuingAttribute.Enabled" /> and <see cref="P:System.EnterpriseServices.InterfaceQueuingAttribute.Interface" /> properties to their default values.</summary>
		// Token: 0x0600008A RID: 138 RVA: 0x00002050 File Offset: 0x00000250
		public InterfaceQueuingAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.InterfaceQueuingAttribute" /> class, optionally disabling queuing support.</summary>
		/// <param name="enabled">
		///       <see langword="true" /> to enable queuing support; otherwise, <see langword="false" />. </param>
		// Token: 0x0600008B RID: 139 RVA: 0x00002050 File Offset: 0x00000250
		public InterfaceQueuingAttribute(bool enabled)
		{
		}

		/// <summary>Gets or sets a value indicating whether queuing support is enabled.</summary>
		/// <returns>
		///     <see langword="true" /> if queuing support is enabled; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x17000039 RID: 57
		// (get) Token: 0x0600008C RID: 140 RVA: 0x000024FC File Offset: 0x000006FC
		// (set) Token: 0x0600008D RID: 141 RVA: 0x00002077 File Offset: 0x00000277
		public bool Enabled
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the interface on which queuing is enabled.</summary>
		/// <returns>The name of the interface on which queuing is enabled.</returns>
		// Token: 0x1700003A RID: 58
		// (get) Token: 0x0600008E RID: 142 RVA: 0x0000206F File Offset: 0x0000026F
		// (set) Token: 0x0600008F RID: 143 RVA: 0x00002077 File Offset: 0x00000277
		public string Interface
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
