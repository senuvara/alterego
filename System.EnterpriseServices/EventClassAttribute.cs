﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Marks the attributed class as an event class. This class cannot be inherited.</summary>
	// Token: 0x02000021 RID: 33
	[ComVisible(false)]
	[AttributeUsage(AttributeTargets.Class, Inherited = true)]
	public sealed class EventClassAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.EventClassAttribute" /> class.</summary>
		// Token: 0x0600007A RID: 122 RVA: 0x00002050 File Offset: 0x00000250
		public EventClassAttribute()
		{
		}

		/// <summary>Gets or sets a value that indicates whether subscribers can be activated in the publisher's process.</summary>
		/// <returns>
		///     <see langword="true" /> if subscribers can be activated in the publisher's process; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000033 RID: 51
		// (get) Token: 0x0600007B RID: 123 RVA: 0x0000248C File Offset: 0x0000068C
		// (set) Token: 0x0600007C RID: 124 RVA: 0x00002077 File Offset: 0x00000277
		public bool AllowInprocSubscribers
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates whether events are to be delivered to subscribers in parallel.</summary>
		/// <returns>
		///     <see langword="true" /> if events are to be delivered to subscribers in parallel; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000034 RID: 52
		// (get) Token: 0x0600007D RID: 125 RVA: 0x000024A8 File Offset: 0x000006A8
		// (set) Token: 0x0600007E RID: 126 RVA: 0x00002077 File Offset: 0x00000277
		public bool FireInParallel
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a publisher filter for an event method.</summary>
		/// <returns>The publisher filter.</returns>
		// Token: 0x17000035 RID: 53
		// (get) Token: 0x0600007F RID: 127 RVA: 0x0000206F File Offset: 0x0000026F
		// (set) Token: 0x06000080 RID: 128 RVA: 0x00002077 File Offset: 0x00000277
		public string PublisherFilter
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
