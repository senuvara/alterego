﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Represents a collection of shared properties. This class cannot be inherited.</summary>
	// Token: 0x02000047 RID: 71
	[ComVisible(false)]
	public sealed class SharedPropertyGroup
	{
		// Token: 0x06000119 RID: 281 RVA: 0x00002077 File Offset: 0x00000277
		internal SharedPropertyGroup()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a property with the given name.</summary>
		/// <param name="name">The name of the new property. </param>
		/// <param name="fExists">Determines whether the property exists. Set to <see langword="true" /> on return if the property exists. </param>
		/// <returns>The requested <see cref="T:System.EnterpriseServices.SharedProperty" />.</returns>
		// Token: 0x0600011A RID: 282 RVA: 0x0000206F File Offset: 0x0000026F
		public SharedProperty CreateProperty(string name, out bool fExists)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates a property at the given position.</summary>
		/// <param name="position">The index of the new property </param>
		/// <param name="fExists">Determines whether the property exists. Set to <see langword="true" /> on return if the property exists. </param>
		/// <returns>The requested <see cref="T:System.EnterpriseServices.SharedProperty" />.</returns>
		// Token: 0x0600011B RID: 283 RVA: 0x0000206F File Offset: 0x0000026F
		public SharedProperty CreatePropertyByPosition(int position, out bool fExists)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the property with the given name.</summary>
		/// <param name="name">The name of requested property. </param>
		/// <returns>The requested <see cref="T:System.EnterpriseServices.SharedProperty" />.</returns>
		// Token: 0x0600011C RID: 284 RVA: 0x0000206F File Offset: 0x0000026F
		public SharedProperty Property(string name)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the property at the given position.</summary>
		/// <param name="position">The index of the property. </param>
		/// <returns>The requested <see cref="T:System.EnterpriseServices.SharedProperty" />.</returns>
		// Token: 0x0600011D RID: 285 RVA: 0x0000206F File Offset: 0x0000026F
		public SharedProperty PropertyByPosition(int position)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
