﻿using System;
using System.Runtime.InteropServices;

namespace System.EnterpriseServices
{
	/// <summary>Specifies the values allowed for transaction outcome voting.</summary>
	// Token: 0x0200001F RID: 31
	[ComVisible(false)]
	[Serializable]
	public enum TransactionVote
	{
		/// <summary>Aborts the current transaction.</summary>
		// Token: 0x04000043 RID: 67
		Abort = 1,
		/// <summary>Commits the current transaction.</summary>
		// Token: 0x04000044 RID: 68
		Commit = 0
	}
}
