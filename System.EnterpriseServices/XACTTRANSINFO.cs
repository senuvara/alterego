﻿using System;
using System.Runtime.InteropServices;

namespace System.EnterpriseServices
{
	/// <summary>Represents a structure used in the <see cref="T:System.EnterpriseServices.ITransaction" /> interface.</summary>
	// Token: 0x02000006 RID: 6
	[ComVisible(false)]
	[StructLayout(LayoutKind.Sequential, Pack = 4)]
	public struct XACTTRANSINFO
	{
		/// <summary>Specifies zero. This field is reserved.</summary>
		// Token: 0x04000002 RID: 2
		public int grfRMSupported;

		/// <summary>Specifies zero. This field is reserved.</summary>
		// Token: 0x04000003 RID: 3
		public int grfRMSupportedRetaining;

		/// <summary>Represents a bitmask that indicates which <see langword="grfTC" /> flags this transaction implementation supports.</summary>
		// Token: 0x04000004 RID: 4
		public int grfTCSupported;

		/// <summary>Specifies zero. This field is reserved.</summary>
		// Token: 0x04000005 RID: 5
		public int grfTCSupportedRetaining;

		/// <summary>Specifies zero. This field is reserved.</summary>
		// Token: 0x04000006 RID: 6
		public int isoFlags;

		/// <summary>Represents the isolation level associated with this transaction object. ISOLATIONLEVEL_UNSPECIFIED indicates that no isolation level was specified.</summary>
		// Token: 0x04000007 RID: 7
		public int isoLevel;

		/// <summary>Represents the unit of work associated with this transaction.</summary>
		// Token: 0x04000008 RID: 8
		public BOID uow;
	}
}
