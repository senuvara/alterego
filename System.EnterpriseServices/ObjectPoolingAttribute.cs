﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Enables and configures object pooling for a component. This class cannot be inherited.</summary>
	// Token: 0x02000031 RID: 49
	[AttributeUsage(AttributeTargets.Class, Inherited = true)]
	[ComVisible(false)]
	public sealed class ObjectPoolingAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.ObjectPoolingAttribute" /> class and sets the <see cref="P:System.EnterpriseServices.ObjectPoolingAttribute.Enabled" />, <see cref="P:System.EnterpriseServices.ObjectPoolingAttribute.MaxPoolSize" />, <see cref="P:System.EnterpriseServices.ObjectPoolingAttribute.MinPoolSize" />, and <see cref="P:System.EnterpriseServices.ObjectPoolingAttribute.CreationTimeout" /> properties to their default values.</summary>
		// Token: 0x060000A3 RID: 163 RVA: 0x00002050 File Offset: 0x00000250
		public ObjectPoolingAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.ObjectPoolingAttribute" /> class and sets the <see cref="P:System.EnterpriseServices.ObjectPoolingAttribute.Enabled" /> property.</summary>
		/// <param name="enable">
		///       <see langword="true" /> to enable object pooling; otherwise, <see langword="false" />. </param>
		// Token: 0x060000A4 RID: 164 RVA: 0x00002050 File Offset: 0x00000250
		public ObjectPoolingAttribute(bool enable)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.ObjectPoolingAttribute" /> class and sets the <see cref="P:System.EnterpriseServices.ObjectPoolingAttribute.Enabled" />, <see cref="P:System.EnterpriseServices.ObjectPoolingAttribute.MaxPoolSize" />, and <see cref="P:System.EnterpriseServices.ObjectPoolingAttribute.MinPoolSize" /> properties.</summary>
		/// <param name="enable">
		///       <see langword="true" /> to enable object pooling; otherwise, <see langword="false" />. </param>
		/// <param name="minPoolSize">The minimum pool size.</param>
		/// <param name="maxPoolSize">The maximum pool size.</param>
		// Token: 0x060000A5 RID: 165 RVA: 0x00002050 File Offset: 0x00000250
		public ObjectPoolingAttribute(bool enable, int minPoolSize, int maxPoolSize)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.ObjectPoolingAttribute" /> class and sets the <see cref="P:System.EnterpriseServices.ObjectPoolingAttribute.MaxPoolSize" /> and <see cref="P:System.EnterpriseServices.ObjectPoolingAttribute.MinPoolSize" /> properties.</summary>
		/// <param name="minPoolSize">The minimum pool size. </param>
		/// <param name="maxPoolSize">The maximum pool size. </param>
		// Token: 0x060000A6 RID: 166 RVA: 0x00002050 File Offset: 0x00000250
		public ObjectPoolingAttribute(int minPoolSize, int maxPoolSize)
		{
		}

		/// <summary>Gets or sets the length of time to wait for an object to become available in the pool before throwing an exception. This value is in milliseconds.</summary>
		/// <returns>The time-out value in milliseconds.</returns>
		// Token: 0x1700003E RID: 62
		// (get) Token: 0x060000A7 RID: 167 RVA: 0x0000256C File Offset: 0x0000076C
		// (set) Token: 0x060000A8 RID: 168 RVA: 0x00002077 File Offset: 0x00000277
		public int CreationTimeout
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates whether object pooling is enabled.</summary>
		/// <returns>
		///     <see langword="true" /> if object pooling is enabled; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x1700003F RID: 63
		// (get) Token: 0x060000A9 RID: 169 RVA: 0x00002588 File Offset: 0x00000788
		// (set) Token: 0x060000AA RID: 170 RVA: 0x00002077 File Offset: 0x00000277
		public bool Enabled
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the value for the maximum size of the pool.</summary>
		/// <returns>The maximum number of objects in the pool.</returns>
		// Token: 0x17000040 RID: 64
		// (get) Token: 0x060000AB RID: 171 RVA: 0x000025A4 File Offset: 0x000007A4
		// (set) Token: 0x060000AC RID: 172 RVA: 0x00002077 File Offset: 0x00000277
		public int MaxPoolSize
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the value for the minimum size of the pool.</summary>
		/// <returns>The minimum number of objects in the pool.</returns>
		// Token: 0x17000041 RID: 65
		// (get) Token: 0x060000AD RID: 173 RVA: 0x000025C0 File Offset: 0x000007C0
		// (set) Token: 0x060000AE RID: 174 RVA: 0x00002077 File Offset: 0x00000277
		public int MinPoolSize
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Called internally by the .NET Framework infrastructure while installing and configuring assemblies in the COM+ catalog.</summary>
		/// <param name="info">A hash table that contains internal objects referenced by internal keys.</param>
		/// <returns>
		///     <see langword="true" /> if the method has made changes.</returns>
		// Token: 0x060000AF RID: 175 RVA: 0x000025DC File Offset: 0x000007DC
		public bool AfterSaveChanges(Hashtable info)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Called internally by the .NET Framework infrastructure while applying the <see cref="T:System.EnterpriseServices.ObjectPoolingAttribute" /> class attribute to a serviced component.</summary>
		/// <param name="info">A hash table that contains an internal object to which object pooling properties are applied, referenced by an internal key.</param>
		/// <returns>
		///     <see langword="true " />if the method has made changes.</returns>
		// Token: 0x060000B0 RID: 176 RVA: 0x000025F8 File Offset: 0x000007F8
		public bool Apply(Hashtable info)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Called internally by the .NET Framework infrastructure while installing and configuring assemblies in the COM+ catalog.</summary>
		/// <param name="s">A string generated by the .NET Framework infrastructure that is checked for a special value that indicates a serviced component.</param>
		/// <returns>
		///     <see langword="true" /> if the attribute is applied to a serviced component class.</returns>
		// Token: 0x060000B1 RID: 177 RVA: 0x00002614 File Offset: 0x00000814
		public bool IsValidTarget(string s)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}
	}
}
