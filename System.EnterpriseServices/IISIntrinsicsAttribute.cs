﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Enables access to ASP intrinsic values from <see cref="M:System.EnterpriseServices.ContextUtil.GetNamedProperty(System.String)" />. This class cannot be inherited.</summary>
	// Token: 0x02000025 RID: 37
	[AttributeUsage(AttributeTargets.Class, Inherited = true)]
	[ComVisible(false)]
	public sealed class IISIntrinsicsAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.IISIntrinsicsAttribute" /> class, enabling access to the ASP intrinsic values.</summary>
		// Token: 0x06000087 RID: 135 RVA: 0x00002050 File Offset: 0x00000250
		public IISIntrinsicsAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.IISIntrinsicsAttribute" /> class, optionally disabling access to the ASP intrinsic values.</summary>
		/// <param name="val">
		///       <see langword="true" /> to enable access to the ASP intrinsic values; otherwise, <see langword="false" />. </param>
		// Token: 0x06000088 RID: 136 RVA: 0x00002050 File Offset: 0x00000250
		public IISIntrinsicsAttribute(bool val)
		{
		}

		/// <summary>Gets a value that indicates whether access to the ASP intrinsic values is enabled.</summary>
		/// <returns>
		///     <see langword="true" /> if access is enabled; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x17000038 RID: 56
		// (get) Token: 0x06000089 RID: 137 RVA: 0x000024E0 File Offset: 0x000006E0
		public bool Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}
	}
}
