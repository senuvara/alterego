﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Enables security checking on calls to a component. This class cannot be inherited.</summary>
	// Token: 0x0200001B RID: 27
	[AttributeUsage(AttributeTargets.Class, Inherited = true)]
	[ComVisible(false)]
	public sealed class ComponentAccessControlAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.ComponentAccessControlAttribute" /> class.</summary>
		// Token: 0x06000056 RID: 86 RVA: 0x00002050 File Offset: 0x00000250
		public ComponentAccessControlAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.ComponentAccessControlAttribute" /> class and sets the <see cref="P:System.EnterpriseServices.ComponentAccessControlAttribute.Value" /> property to indicate whether to enable COM+ security configuration.</summary>
		/// <param name="val">
		///       <see langword="true" /> to enable security checking on calls to a component; otherwise, <see langword="false" />. </param>
		// Token: 0x06000057 RID: 87 RVA: 0x00002050 File Offset: 0x00000250
		public ComponentAccessControlAttribute(bool val)
		{
		}

		/// <summary>Gets a value indicating whether to enable security checking on calls to a component.</summary>
		/// <returns>
		///     <see langword="true" /> if security checking is to be enabled; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000023 RID: 35
		// (get) Token: 0x06000058 RID: 88 RVA: 0x000022E8 File Offset: 0x000004E8
		public bool Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}
	}
}
