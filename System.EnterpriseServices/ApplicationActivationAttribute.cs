﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Specifies whether components in the assembly run in the creator's process or in a system process.</summary>
	// Token: 0x02000017 RID: 23
	[AttributeUsage(AttributeTargets.Assembly, Inherited = true)]
	[ComVisible(false)]
	public sealed class ApplicationActivationAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.ApplicationActivationAttribute" /> class, setting the specified <see cref="T:System.EnterpriseServices.ActivationOption" /> value.</summary>
		/// <param name="opt">One of the <see cref="T:System.EnterpriseServices.ActivationOption" /> values. </param>
		// Token: 0x06000043 RID: 67 RVA: 0x00002050 File Offset: 0x00000250
		public ApplicationActivationAttribute(ActivationOption opt)
		{
		}

		/// <summary>This property is not supported in the current version.</summary>
		/// <returns>This property is not supported in the current version.</returns>
		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000044 RID: 68 RVA: 0x0000206F File Offset: 0x0000026F
		// (set) Token: 0x06000045 RID: 69 RVA: 0x00002077 File Offset: 0x00000277
		public string SoapMailbox
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value representing a virtual root on the Web for the COM+ application.</summary>
		/// <returns>The virtual root on the Web for the COM+ application.</returns>
		// Token: 0x1700001D RID: 29
		// (get) Token: 0x06000046 RID: 70 RVA: 0x0000206F File Offset: 0x0000026F
		// (set) Token: 0x06000047 RID: 71 RVA: 0x00002077 File Offset: 0x00000277
		public string SoapVRoot
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets the specified <see cref="T:System.EnterpriseServices.ActivationOption" /> value.</summary>
		/// <returns>One of the <see cref="T:System.EnterpriseServices.ActivationOption" /> values, either <see langword="Library" /> or <see langword="Server" />.</returns>
		// Token: 0x1700001E RID: 30
		// (get) Token: 0x06000048 RID: 72 RVA: 0x0000225C File Offset: 0x0000045C
		public ActivationOption Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ActivationOption.Library;
			}
		}
	}
}
