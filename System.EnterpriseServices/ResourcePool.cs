﻿using System;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Stores objects in the current transaction. This class cannot be inherited.</summary>
	// Token: 0x0200003C RID: 60
	public sealed class ResourcePool
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.ResourcePool" /> class.</summary>
		/// <param name="cb">A <see cref="T:System.EnterpriseServices.ResourcePool.TransactionEndDelegate" />, that is called when a transaction is finished. All items currently stored in the transaction are handed back to the user through the delegate. </param>
		// Token: 0x060000ED RID: 237 RVA: 0x00002077 File Offset: 0x00000277
		public ResourcePool(ResourcePool.TransactionEndDelegate cb)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a resource from the current transaction.</summary>
		/// <returns>The resource object.</returns>
		// Token: 0x060000EE RID: 238 RVA: 0x0000206F File Offset: 0x0000026F
		public object GetResource()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Adds a resource to the current transaction.</summary>
		/// <param name="resource">The resource to add. </param>
		/// <returns>
		///     <see langword="true" /> if the resource object was added to the pool; otherwise, <see langword="false" />.</returns>
		// Token: 0x060000EF RID: 239 RVA: 0x000026F4 File Offset: 0x000008F4
		public bool PutResource(object resource)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Represents the method that handles the ending of a transaction.</summary>
		/// <param name="resource">The object that is passed back to the delegate. </param>
		// Token: 0x0200003D RID: 61
		public sealed class TransactionEndDelegate : MulticastDelegate
		{
			// Token: 0x060000F0 RID: 240 RVA: 0x00002077 File Offset: 0x00000277
			public TransactionEndDelegate(object @object, IntPtr method)
			{
				ThrowStub.ThrowNotSupportedException();
			}

			// Token: 0x060000F1 RID: 241 RVA: 0x00002077 File Offset: 0x00000277
			public void Invoke(object resource)
			{
				ThrowStub.ThrowNotSupportedException();
			}

			// Token: 0x060000F2 RID: 242 RVA: 0x0000206F File Offset: 0x0000026F
			public IAsyncResult BeginInvoke(object resource, AsyncCallback callback, object @object)
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}

			// Token: 0x060000F3 RID: 243 RVA: 0x00002077 File Offset: 0x00000277
			public void EndInvoke(IAsyncResult result)
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
