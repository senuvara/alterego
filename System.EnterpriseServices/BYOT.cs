﻿using System;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Wraps the COM+ <see langword="ByotServerEx" /> class and the COM+ DTC interfaces <see langword="ICreateWithTransactionEx" /> and <see langword="ICreateWithTipTransactionEx" />. This class cannot be inherited.</summary>
	// Token: 0x0200001A RID: 26
	public sealed class BYOT
	{
		// Token: 0x06000053 RID: 83 RVA: 0x00002077 File Offset: 0x00000277
		internal BYOT()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates an object that is enlisted within a manual transaction using the Transaction Internet Protocol (TIP).</summary>
		/// <param name="url">A TIP URL that specifies a transaction. </param>
		/// <param name="t">The type. </param>
		/// <returns>The requested transaction.</returns>
		// Token: 0x06000054 RID: 84 RVA: 0x0000206F File Offset: 0x0000026F
		public static object CreateWithTipTransaction(string url, Type t)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Creates an object that is enlisted within a manual transaction.</summary>
		/// <param name="transaction">The <see cref="T:System.EnterpriseServices.ITransaction" /> or <see cref="T:System.Transactions.Transaction" /> object that specifies a transaction. </param>
		/// <param name="t">The specified type. </param>
		/// <returns>The requested transaction.</returns>
		// Token: 0x06000055 RID: 85 RVA: 0x0000206F File Offset: 0x0000026F
		public static object CreateWithTransaction(object transaction, Type t)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
