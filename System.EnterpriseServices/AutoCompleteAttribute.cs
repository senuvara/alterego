﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Marks the attributed method as an <see langword="AutoComplete" /> object. This class cannot be inherited.</summary>
	// Token: 0x02000019 RID: 25
	[AttributeUsage(AttributeTargets.Method, Inherited = true)]
	[ComVisible(false)]
	public sealed class AutoCompleteAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.AutoCompleteAttribute" /> class, specifying that the application should automatically call <see cref="M:System.EnterpriseServices.ContextUtil.SetComplete" /> if the transaction completes successfully.</summary>
		// Token: 0x06000050 RID: 80 RVA: 0x00002050 File Offset: 0x00000250
		public AutoCompleteAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.AutoCompleteAttribute" /> class, specifying whether COM+ <see langword="AutoComplete" /> is enabled.</summary>
		/// <param name="val">
		///       <see langword="true" /> to enable <see langword="AutoComplete" /> in the COM+ object; otherwise, <see langword="false" />. </param>
		// Token: 0x06000051 RID: 81 RVA: 0x00002050 File Offset: 0x00000250
		public AutoCompleteAttribute(bool val)
		{
		}

		/// <summary>Gets a value indicating the setting of the <see langword="AutoComplete" /> option in COM+.</summary>
		/// <returns>
		///     <see langword="true" /> if <see langword="AutoComplete" /> is enabled in COM+; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000052 RID: 82 RVA: 0x000022CC File Offset: 0x000004CC
		public bool Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}
	}
}
