﻿using System;
using System.Runtime.InteropServices;

namespace System.EnterpriseServices
{
	/// <summary>Indicates the thread pool in which the work, submitted by <see cref="T:System.EnterpriseServices.Activity" />, runs.</summary>
	// Token: 0x02000011 RID: 17
	[ComVisible(false)]
	[Serializable]
	public enum ThreadPoolOption
	{
		/// <summary>The same type of thread pool apartment as the caller's thread apartment is used.</summary>
		// Token: 0x0400002A RID: 42
		Inherit = 1,
		/// <summary>A multithreaded apartment (MTA) is used.</summary>
		// Token: 0x0400002B RID: 43
		MTA = 3,
		/// <summary>No thread pool is used. If this value is used to configure a <see cref="T:System.EnterpriseServices.ServiceConfig" /> that is passed to an <see cref="T:System.EnterpriseServices.Activity" />, an exception is thrown.</summary>
		// Token: 0x0400002C RID: 44
		None = 0,
		/// <summary>A single-threaded apartment (STA) is used.</summary>
		// Token: 0x0400002D RID: 45
		STA = 2
	}
}
