﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Enables event tracking for a component. This class cannot be inherited.</summary>
	// Token: 0x02000022 RID: 34
	[ComVisible(false)]
	[AttributeUsage(AttributeTargets.Class, Inherited = true)]
	public sealed class EventTrackingEnabledAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.EventTrackingEnabledAttribute" /> class, enabling event tracking.</summary>
		// Token: 0x06000081 RID: 129 RVA: 0x00002050 File Offset: 0x00000250
		public EventTrackingEnabledAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.EventTrackingEnabledAttribute" /> class, optionally disabling event tracking.</summary>
		/// <param name="val">
		///       <see langword="true" /> to enable event tracking; otherwise, <see langword="false" />. </param>
		// Token: 0x06000082 RID: 130 RVA: 0x00002050 File Offset: 0x00000250
		public EventTrackingEnabledAttribute(bool val)
		{
		}

		/// <summary>Gets the value of the <see cref="P:System.EnterpriseServices.EventTrackingEnabledAttribute.Value" /> property, which indicates whether tracking is enabled.</summary>
		/// <returns>
		///     <see langword="true" /> if tracking is enabled; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x17000036 RID: 54
		// (get) Token: 0x06000083 RID: 131 RVA: 0x000024C4 File Offset: 0x000006C4
		public bool Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}
	}
}
