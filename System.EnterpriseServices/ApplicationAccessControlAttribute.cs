﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Specifies access controls to an assembly containing <see cref="T:System.EnterpriseServices.ServicedComponent" /> classes.</summary>
	// Token: 0x02000014 RID: 20
	[AttributeUsage(AttributeTargets.Assembly, Inherited = true)]
	[ComVisible(false)]
	public sealed class ApplicationAccessControlAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.ApplicationAccessControlAttribute" /> class, enabling the COM+ security configuration.</summary>
		// Token: 0x06000039 RID: 57 RVA: 0x00002050 File Offset: 0x00000250
		public ApplicationAccessControlAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.ApplicationAccessControlAttribute" /> class and sets the <see cref="P:System.EnterpriseServices.ApplicationAccessControlAttribute.Value" /> property indicating whether to enable COM security configuration.</summary>
		/// <param name="val">
		///       <see langword="true" /> to allow configuration of security; otherwise, <see langword="false" />. </param>
		// Token: 0x0600003A RID: 58 RVA: 0x00002050 File Offset: 0x00000250
		public ApplicationAccessControlAttribute(bool val)
		{
		}

		/// <summary>Gets or sets the access checking level to process level or to component level.</summary>
		/// <returns>One of the <see cref="T:System.EnterpriseServices.AccessChecksLevelOption" /> values.</returns>
		// Token: 0x17000018 RID: 24
		// (get) Token: 0x0600003B RID: 59 RVA: 0x000021EC File Offset: 0x000003EC
		// (set) Token: 0x0600003C RID: 60 RVA: 0x00002077 File Offset: 0x00000277
		public AccessChecksLevelOption AccessChecksLevel
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return AccessChecksLevelOption.Application;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the remote procedure call (RPC) authentication level.</summary>
		/// <returns>One of the <see cref="T:System.EnterpriseServices.AuthenticationOption" /> values.</returns>
		// Token: 0x17000019 RID: 25
		// (get) Token: 0x0600003D RID: 61 RVA: 0x00002208 File Offset: 0x00000408
		// (set) Token: 0x0600003E RID: 62 RVA: 0x00002077 File Offset: 0x00000277
		public AuthenticationOption Authentication
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return AuthenticationOption.Default;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the impersonation level that is allowed for calling targets of this application.</summary>
		/// <returns>One of the <see cref="T:System.EnterpriseServices.ImpersonationLevelOption" /> values.</returns>
		// Token: 0x1700001A RID: 26
		// (get) Token: 0x0600003F RID: 63 RVA: 0x00002224 File Offset: 0x00000424
		// (set) Token: 0x06000040 RID: 64 RVA: 0x00002077 File Offset: 0x00000277
		public ImpersonationLevelOption ImpersonationLevel
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return ImpersonationLevelOption.Default;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether to enable COM+ security configuration.</summary>
		/// <returns>
		///     <see langword="true" /> if COM+ security configuration is enabled; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000041 RID: 65 RVA: 0x00002240 File Offset: 0x00000440
		// (set) Token: 0x06000042 RID: 66 RVA: 0x00002077 File Offset: 0x00000277
		public bool Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
