﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Enables queuing support for the marked assembly and enables the application to read method calls from Message Queuing queues. This class cannot be inherited.</summary>
	// Token: 0x02000018 RID: 24
	[ComVisible(false)]
	[AttributeUsage(AttributeTargets.Assembly, Inherited = true)]
	public sealed class ApplicationQueuingAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.ApplicationQueuingAttribute" /> class, enabling queuing support for the assembly and initializing <see cref="P:System.EnterpriseServices.ApplicationQueuingAttribute.Enabled" />, <see cref="P:System.EnterpriseServices.ApplicationQueuingAttribute.QueueListenerEnabled" />, and <see cref="P:System.EnterpriseServices.ApplicationQueuingAttribute.MaxListenerThreads" />.</summary>
		// Token: 0x06000049 RID: 73 RVA: 0x00002050 File Offset: 0x00000250
		public ApplicationQueuingAttribute()
		{
		}

		/// <summary>Gets or sets a value indicating whether queuing support is enabled.</summary>
		/// <returns>
		///     <see langword="true" /> if queuing support is enabled; otherwise, <see langword="false" />. The default value set by the constructor is <see langword="true" />.</returns>
		// Token: 0x1700001F RID: 31
		// (get) Token: 0x0600004A RID: 74 RVA: 0x00002278 File Offset: 0x00000478
		// (set) Token: 0x0600004B RID: 75 RVA: 0x00002077 File Offset: 0x00000277
		public bool Enabled
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the number of threads used to extract messages from the queue and activate the corresponding component.</summary>
		/// <returns>The maximum number of threads to use for processing messages arriving in the queue. The default is zero.</returns>
		// Token: 0x17000020 RID: 32
		// (get) Token: 0x0600004C RID: 76 RVA: 0x00002294 File Offset: 0x00000494
		// (set) Token: 0x0600004D RID: 77 RVA: 0x00002077 File Offset: 0x00000277
		public int MaxListenerThreads
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value indicating whether the application will accept queued component calls from clients.</summary>
		/// <returns>
		///     <see langword="true" /> if the application accepts queued component calls; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000021 RID: 33
		// (get) Token: 0x0600004E RID: 78 RVA: 0x000022B0 File Offset: 0x000004B0
		// (set) Token: 0x0600004F RID: 79 RVA: 0x00002077 File Offset: 0x00000277
		public bool QueueListenerEnabled
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
