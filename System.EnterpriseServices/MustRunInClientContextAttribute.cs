﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Forces the attributed object to be created in the context of the creator, if possible. This class cannot be inherited.</summary>
	// Token: 0x02000030 RID: 48
	[AttributeUsage(AttributeTargets.Class, Inherited = true)]
	[ComVisible(false)]
	public sealed class MustRunInClientContextAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.MustRunInClientContextAttribute" /> class, requiring creation of the object in the context of the creator.</summary>
		// Token: 0x060000A0 RID: 160 RVA: 0x00002050 File Offset: 0x00000250
		public MustRunInClientContextAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.MustRunInClientContextAttribute" /> class, optionally not creating the object in the context of the creator.</summary>
		/// <param name="val">
		///       <see langword="true" /> to create the object in the context of the creator; otherwise, <see langword="false" />. </param>
		// Token: 0x060000A1 RID: 161 RVA: 0x00002050 File Offset: 0x00000250
		public MustRunInClientContextAttribute(bool val)
		{
		}

		/// <summary>Gets a value that indicates whether the attributed object is to be created in the context of the creator.</summary>
		/// <returns>
		///     <see langword="true" /> if the object is to be created in the context of the creator; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x1700003D RID: 61
		// (get) Token: 0x060000A2 RID: 162 RVA: 0x00002550 File Offset: 0x00000750
		public bool Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}
	}
}
