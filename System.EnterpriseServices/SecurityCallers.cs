﻿using System;
using System.Collections;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Provides an ordered collection of identities in the current call chain.</summary>
	// Token: 0x02000040 RID: 64
	public sealed class SecurityCallers : IEnumerable
	{
		// Token: 0x060000FF RID: 255 RVA: 0x00002077 File Offset: 0x00000277
		internal SecurityCallers()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the number of callers in the chain.</summary>
		/// <returns>The number of callers in the chain.</returns>
		// Token: 0x17000058 RID: 88
		// (get) Token: 0x06000100 RID: 256 RVA: 0x0000279C File Offset: 0x0000099C
		public int Count
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the specified <see cref="T:System.EnterpriseServices.SecurityIdentity" /> item.</summary>
		/// <param name="idx">The item to access using an index number. </param>
		/// <returns>A <see cref="T:System.EnterpriseServices.SecurityIdentity" /> object.</returns>
		// Token: 0x17000059 RID: 89
		public SecurityIdentity this[int idx]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Retrieves the enumeration interface for the object.</summary>
		/// <returns>The enumerator interface for the <see langword="ISecurityCallersColl" /> collection.</returns>
		// Token: 0x06000102 RID: 258 RVA: 0x0000206F File Offset: 0x0000026F
		public IEnumerator GetEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
