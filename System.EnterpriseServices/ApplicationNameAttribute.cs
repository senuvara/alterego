﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Specifies the name of the COM+ application to be used for the install of the components in the assembly. This class cannot be inherited.</summary>
	// Token: 0x02000003 RID: 3
	[ComVisible(false)]
	[AttributeUsage(AttributeTargets.Assembly, Inherited = true)]
	public sealed class ApplicationNameAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.ApplicationNameAttribute" /> class, specifying the name of the COM+ application to be used for the install of the components.</summary>
		/// <param name="name">The name of the COM+ application. </param>
		// Token: 0x06000003 RID: 3 RVA: 0x00002050 File Offset: 0x00000250
		public ApplicationNameAttribute(string name)
		{
		}

		/// <summary>Gets a value indicating the name of the COM+ application that contains the components in the assembly.</summary>
		/// <returns>The name of the COM+ application.</returns>
		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000004 RID: 4 RVA: 0x0000206F File Offset: 0x0000026F
		public string Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
