﻿using System;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Retrieves extended error information about methods related to multiple COM+ objects. This also includes methods that install, import, and export COM+ applications and components. This class cannot be inherited.</summary>
	// Token: 0x02000036 RID: 54
	[Serializable]
	public sealed class RegistrationErrorInfo
	{
		// Token: 0x060000C0 RID: 192 RVA: 0x00002077 File Offset: 0x00000277
		internal RegistrationErrorInfo()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the error code for the object or file.</summary>
		/// <returns>The error code for the object or file.</returns>
		// Token: 0x17000048 RID: 72
		// (get) Token: 0x060000C1 RID: 193 RVA: 0x0000264C File Offset: 0x0000084C
		public int ErrorCode
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the description of the <see cref="P:System.EnterpriseServices.RegistrationErrorInfo.ErrorCode" />.</summary>
		/// <returns>The description of the <see cref="P:System.EnterpriseServices.RegistrationErrorInfo.ErrorCode" />.</returns>
		// Token: 0x17000049 RID: 73
		// (get) Token: 0x060000C2 RID: 194 RVA: 0x0000206F File Offset: 0x0000026F
		public string ErrorString
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the key value for the object that caused the error, if applicable.</summary>
		/// <returns>The key value for the object that caused the error, if applicable.</returns>
		// Token: 0x1700004A RID: 74
		// (get) Token: 0x060000C3 RID: 195 RVA: 0x0000206F File Offset: 0x0000026F
		public string MajorRef
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a precise specification of the item that caused the error, such as a property name.</summary>
		/// <returns>A precise specification of the item, such as a property name, that caused the error. If multiple errors occurred, or this does not apply, <see cref="P:System.EnterpriseServices.RegistrationErrorInfo.MinorRef" /> returns the string "&lt;Invalid&gt;".</returns>
		// Token: 0x1700004B RID: 75
		// (get) Token: 0x060000C4 RID: 196 RVA: 0x0000206F File Offset: 0x0000026F
		public string MinorRef
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the name of the object or file that caused the error.</summary>
		/// <returns>The name of the object or file that caused the error.</returns>
		// Token: 0x1700004C RID: 76
		// (get) Token: 0x060000C5 RID: 197 RVA: 0x0000206F File Offset: 0x0000026F
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
