﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Determines whether the component participates in load balancing, if the component load balancing service is installed and enabled on the server.</summary>
	// Token: 0x0200002F RID: 47
	[ComVisible(false)]
	[AttributeUsage(AttributeTargets.Class, Inherited = true)]
	public sealed class LoadBalancingSupportedAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.LoadBalancingSupportedAttribute" /> class, specifying load balancing support.</summary>
		// Token: 0x0600009D RID: 157 RVA: 0x00002050 File Offset: 0x00000250
		public LoadBalancingSupportedAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.LoadBalancingSupportedAttribute" /> class, optionally disabling load balancing support.</summary>
		/// <param name="val">
		///       <see langword="true" /> to enable load balancing support; otherwise, <see langword="false" />. </param>
		// Token: 0x0600009E RID: 158 RVA: 0x00002050 File Offset: 0x00000250
		public LoadBalancingSupportedAttribute(bool val)
		{
		}

		/// <summary>Gets a value that indicates whether load balancing support is enabled.</summary>
		/// <returns>
		///     <see langword="true" /> if load balancing support is enabled; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x1700003C RID: 60
		// (get) Token: 0x0600009F RID: 159 RVA: 0x00002534 File Offset: 0x00000734
		public bool Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}
	}
}
