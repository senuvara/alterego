﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Accesses a shared property. This class cannot be inherited.</summary>
	// Token: 0x02000046 RID: 70
	[ComVisible(false)]
	public sealed class SharedProperty
	{
		// Token: 0x06000116 RID: 278 RVA: 0x00002077 File Offset: 0x00000277
		internal SharedProperty()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the value of the shared property.</summary>
		/// <returns>The value of the shared property.</returns>
		// Token: 0x17000061 RID: 97
		// (get) Token: 0x06000117 RID: 279 RVA: 0x0000206F File Offset: 0x0000026F
		// (set) Token: 0x06000118 RID: 280 RVA: 0x00002077 File Offset: 0x00000277
		public object Value
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
