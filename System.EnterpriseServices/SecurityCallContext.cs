﻿using System;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Describes the chain of callers leading up to the current method call.</summary>
	// Token: 0x0200003F RID: 63
	public sealed class SecurityCallContext
	{
		// Token: 0x060000F5 RID: 245 RVA: 0x00002077 File Offset: 0x00000277
		internal SecurityCallContext()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a <see cref="T:System.EnterpriseServices.SecurityCallers" /> object that describes the caller.</summary>
		/// <returns>The <see cref="T:System.EnterpriseServices.SecurityCallers" /> object that describes the caller.</returns>
		/// <exception cref="T:System.Runtime.InteropServices.COMException">There is no security context. </exception>
		// Token: 0x17000051 RID: 81
		// (get) Token: 0x060000F6 RID: 246 RVA: 0x0000206F File Offset: 0x0000026F
		public SecurityCallers Callers
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.EnterpriseServices.SecurityCallContext" /> object that describes the security call context.</summary>
		/// <returns>The <see cref="T:System.EnterpriseServices.SecurityCallContext" /> object that describes the security call context.</returns>
		// Token: 0x17000052 RID: 82
		// (get) Token: 0x060000F7 RID: 247 RVA: 0x0000206F File Offset: 0x0000026F
		public static SecurityCallContext CurrentCall
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets a <see cref="T:System.EnterpriseServices.SecurityIdentity" /> object that describes the direct caller of this method.</summary>
		/// <returns>A <see cref="T:System.EnterpriseServices.SecurityIdentity" /> value.</returns>
		// Token: 0x17000053 RID: 83
		// (get) Token: 0x060000F8 RID: 248 RVA: 0x0000206F File Offset: 0x0000026F
		public SecurityIdentity DirectCaller
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Determines whether security checks are enabled in the current context.</summary>
		/// <returns>
		///     <see langword="true" /> if security checks are enabled in the current context; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000054 RID: 84
		// (get) Token: 0x060000F9 RID: 249 RVA: 0x00002710 File Offset: 0x00000910
		public bool IsSecurityEnabled
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets the <see langword="MinAuthenticationLevel" /> value from the <see langword="ISecurityCallContext" /> collection in COM+.</summary>
		/// <returns>The <see langword="MinAuthenticationLevel" /> value from the <see langword="ISecurityCallContext" /> collection in COM+.</returns>
		// Token: 0x17000055 RID: 85
		// (get) Token: 0x060000FA RID: 250 RVA: 0x0000272C File Offset: 0x0000092C
		public int MinAuthenticationLevel
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the <see langword="NumCallers" /> value from the <see langword="ISecurityCallContext" /> collection in COM+.</summary>
		/// <returns>The <see langword="NumCallers" /> value from the <see langword="ISecurityCallContext" /> collection in COM+.</returns>
		// Token: 0x17000056 RID: 86
		// (get) Token: 0x060000FB RID: 251 RVA: 0x00002748 File Offset: 0x00000948
		public int NumCallers
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets a <see cref="T:System.EnterpriseServices.SecurityIdentity" /> that describes the original caller.</summary>
		/// <returns>One of the <see cref="T:System.EnterpriseServices.SecurityIdentity" /> values.</returns>
		// Token: 0x17000057 RID: 87
		// (get) Token: 0x060000FC RID: 252 RVA: 0x0000206F File Offset: 0x0000026F
		public SecurityIdentity OriginalCaller
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Verifies that the direct caller is a member of the specified role.</summary>
		/// <param name="role">The specified role. </param>
		/// <returns>
		///     <see langword="true" /> if the direct caller is a member of the specified role; otherwise, <see langword="false" />.</returns>
		// Token: 0x060000FD RID: 253 RVA: 0x00002764 File Offset: 0x00000964
		public bool IsCallerInRole(string role)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Verifies that the specified user is in the specified role.</summary>
		/// <param name="user">The specified user. </param>
		/// <param name="role">The specified role. </param>
		/// <returns>
		///     <see langword="true" /> if the specified user is a member of the specified role; otherwise, <see langword="false" />.</returns>
		// Token: 0x060000FE RID: 254 RVA: 0x00002780 File Offset: 0x00000980
		public bool IsUserInRole(string user, string role)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}
	}
}
