﻿using System;
using System.Runtime.InteropServices;

namespace System.EnterpriseServices
{
	/// <summary>Indicates whether to create a new context based on the current context or on the information in <see cref="T:System.EnterpriseServices.ServiceConfig" />.</summary>
	// Token: 0x0200000C RID: 12
	[ComVisible(false)]
	[Serializable]
	public enum InheritanceOption
	{
		/// <summary>The new context is created from the default context.</summary>
		// Token: 0x04000013 RID: 19
		Ignore = 1,
		/// <summary>The new context is created from the existing context. <see cref="F:System.EnterpriseServices.InheritanceOption.Inherit" /> is the default value for <see cref="P:System.EnterpriseServices.ServiceConfig.Inheritance" />.</summary>
		// Token: 0x04000014 RID: 20
		Inherit = 0
	}
}
