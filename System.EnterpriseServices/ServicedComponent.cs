﻿using System;
using Unity;

namespace System.EnterpriseServices
{
	/// <summary>Represents the base class of all classes using COM+ services.</summary>
	// Token: 0x0200003A RID: 58
	[Serializable]
	public abstract class ServicedComponent : ContextBoundObject, IRemoteDispatch, IServicedComponentInfo, IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EnterpriseServices.ServicedComponent" /> class.</summary>
		// Token: 0x060000DB RID: 219 RVA: 0x00002077 File Offset: 0x00000277
		public ServicedComponent()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Called by the infrastructure when the object is created or allocated from a pool. Override this method to add custom initialization code to objects.</summary>
		// Token: 0x060000DC RID: 220 RVA: 0x00002077 File Offset: 0x00000277
		protected internal virtual void Activate()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>This method is called by the infrastructure before the object is put back into the pool. Override this method to vote on whether the object is put back into the pool.</summary>
		/// <returns>
		///     <see langword="true" /> if the serviced component can be pooled; otherwise, <see langword="false" />.</returns>
		// Token: 0x060000DD RID: 221 RVA: 0x00002684 File Offset: 0x00000884
		protected internal virtual bool CanBePooled()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Called by the infrastructure just after the constructor is called, passing in the constructor string. Override this method to make use of the construction string value.</summary>
		/// <param name="s">The construction string. </param>
		// Token: 0x060000DE RID: 222 RVA: 0x00002077 File Offset: 0x00000277
		protected internal virtual void Construct(string s)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Called by the infrastructure when the object is about to be deactivated. Override this method to add custom finalization code to objects when just-in-time (JIT) compiled code or object pooling is used.</summary>
		// Token: 0x060000DF RID: 223 RVA: 0x00002077 File Offset: 0x00000277
		protected internal virtual void Deactivate()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Releases all resources used by the <see cref="T:System.EnterpriseServices.ServicedComponent" />.</summary>
		// Token: 0x060000E0 RID: 224 RVA: 0x00002077 File Offset: 0x00000277
		public void Dispose()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.EnterpriseServices.ServicedComponent" /> and optionally releases the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; otherwise, <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x060000E1 RID: 225 RVA: 0x00002077 File Offset: 0x00000277
		protected virtual void Dispose(bool disposing)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Finalizes the object and removes the associated COM+ reference.</summary>
		/// <param name="sc">The object to dispose. </param>
		// Token: 0x060000E2 RID: 226 RVA: 0x00002077 File Offset: 0x00000277
		public static void DisposeObject(ServicedComponent sc)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Ensures that, in the COM+ context, the <see cref="T:System.EnterpriseServices.ServicedComponent" /> class object's <see langword="done" /> bit is set to <see langword="true" /> after a remote method invocation.</summary>
		/// <param name="s">A string to be converted into a request object that implements the <see cref="T:System.Runtime.Remoting.Messaging.IMessage" /> interface.</param>
		/// <returns>A string converted from a response object that implements the <see cref="T:System.Runtime.Remoting.Messaging.IMethodReturnMessage" /> interface.</returns>
		// Token: 0x060000E3 RID: 227 RVA: 0x0000206F File Offset: 0x0000026F
		[AutoComplete(true)]
		string IRemoteDispatch.RemoteDispatchAutoDone(string s)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Does not ensure that, in the COM+ context, the <see cref="T:System.EnterpriseServices.ServicedComponent" /> class object's <see langword="done" /> bit is set to <see langword="true" /> after a remote method invocation.</summary>
		/// <param name="s">A string to be converted into a request object that implements the <see cref="T:System.Runtime.Remoting.Messaging.IMessage" /> interface.</param>
		/// <returns>A string converted from a response object that implements the <see cref="T:System.Runtime.Remoting.Messaging.IMethodReturnMessage" /> interface.</returns>
		// Token: 0x060000E4 RID: 228 RVA: 0x0000206F File Offset: 0x0000026F
		[AutoComplete(false)]
		string IRemoteDispatch.RemoteDispatchNotAutoDone(string s)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Obtains certain information about the <see cref="T:System.EnterpriseServices.ServicedComponent" /> class instance.</summary>
		/// <param name="infoMask">A bitmask where 0x00000001 is a key for the serviced component's process ID, 0x00000002 is a key for the application domain ID, and 0x00000004 is a key for the serviced component's remote URI.</param>
		/// <param name="infoArray">A string array that may contain any or all of the following, in order: the serviced component's process ID, the application domain ID, and the serviced component's remote URI.</param>
		// Token: 0x060000E5 RID: 229 RVA: 0x00002077 File Offset: 0x00000277
		void IServicedComponentInfo.GetComponentInfo(ref int infoMask, out string[] infoArray)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
