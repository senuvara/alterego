﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x02000012 RID: 18
	[EnumCase(EnumCase.Styles.Snake)]
	public enum StoreType
	{
		// Token: 0x040000A0 RID: 160
		Soft,
		// Token: 0x040000A1 RID: 161
		Premium
	}
}
