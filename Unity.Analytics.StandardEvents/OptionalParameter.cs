﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x02000007 RID: 7
	[AttributeUsage(AttributeTargets.Field)]
	public class OptionalParameter : AnalyticsEventParameter
	{
		// Token: 0x0600004F RID: 79 RVA: 0x00003E9F File Offset: 0x0000209F
		public OptionalParameter(string sendName, string tooltip) : base(sendName, tooltip, null)
		{
		}
	}
}
