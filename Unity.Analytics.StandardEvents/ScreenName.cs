﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x0200000F RID: 15
	[EnumCase(EnumCase.Styles.Snake)]
	public enum ScreenName
	{
		// Token: 0x04000079 RID: 121
		None,
		// Token: 0x0400007A RID: 122
		MainMenu,
		// Token: 0x0400007B RID: 123
		Settings,
		// Token: 0x0400007C RID: 124
		Map,
		// Token: 0x0400007D RID: 125
		Lose,
		// Token: 0x0400007E RID: 126
		Win,
		// Token: 0x0400007F RID: 127
		Credits,
		// Token: 0x04000080 RID: 128
		Title,
		// Token: 0x04000081 RID: 129
		IAPPromo,
		// Token: 0x04000082 RID: 130
		CrossPromo,
		// Token: 0x04000083 RID: 131
		FeaturePromo,
		// Token: 0x04000084 RID: 132
		Hint,
		// Token: 0x04000085 RID: 133
		Pause,
		// Token: 0x04000086 RID: 134
		Inventory,
		// Token: 0x04000087 RID: 135
		Leaderboard,
		// Token: 0x04000088 RID: 136
		Achievements,
		// Token: 0x04000089 RID: 137
		Lobby
	}
}
