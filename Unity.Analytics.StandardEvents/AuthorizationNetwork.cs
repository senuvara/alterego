﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x0200000E RID: 14
	[EnumCase(EnumCase.Styles.Lower)]
	public enum AuthorizationNetwork
	{
		// Token: 0x04000072 RID: 114
		None,
		// Token: 0x04000073 RID: 115
		Internal,
		// Token: 0x04000074 RID: 116
		Facebook,
		// Token: 0x04000075 RID: 117
		Twitter,
		// Token: 0x04000076 RID: 118
		Google,
		// Token: 0x04000077 RID: 119
		GameCenter
	}
}
