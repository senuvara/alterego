﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x0200000B RID: 11
	[EnumCase(EnumCase.Styles.Snake)]
	public enum AcquisitionSource
	{
		// Token: 0x04000013 RID: 19
		None,
		// Token: 0x04000014 RID: 20
		Store,
		// Token: 0x04000015 RID: 21
		Earned,
		// Token: 0x04000016 RID: 22
		Promotion,
		// Token: 0x04000017 RID: 23
		Gift,
		// Token: 0x04000018 RID: 24
		RewardedAd,
		// Token: 0x04000019 RID: 25
		TimedReward,
		// Token: 0x0400001A RID: 26
		SocialReward
	}
}
