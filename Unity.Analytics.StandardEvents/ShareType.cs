﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x02000010 RID: 16
	[EnumCase(EnumCase.Styles.Snake)]
	public enum ShareType
	{
		// Token: 0x0400008B RID: 139
		None,
		// Token: 0x0400008C RID: 140
		TextOnly,
		// Token: 0x0400008D RID: 141
		Image,
		// Token: 0x0400008E RID: 142
		Video,
		// Token: 0x0400008F RID: 143
		Invite,
		// Token: 0x04000090 RID: 144
		Achievement
	}
}
