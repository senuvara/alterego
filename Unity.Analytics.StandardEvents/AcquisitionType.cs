﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x0200000C RID: 12
	[EnumCase(EnumCase.Styles.Snake)]
	public enum AcquisitionType
	{
		// Token: 0x0400001C RID: 28
		Soft,
		// Token: 0x0400001D RID: 29
		Premium
	}
}
