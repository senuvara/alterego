﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x0200000D RID: 13
	[EnumCase(EnumCase.Styles.Lower)]
	public enum AdvertisingNetwork
	{
		// Token: 0x0400001F RID: 31
		None,
		// Token: 0x04000020 RID: 32
		Aarki,
		// Token: 0x04000021 RID: 33
		AdAction,
		// Token: 0x04000022 RID: 34
		AdapTv,
		// Token: 0x04000023 RID: 35
		Adcash,
		// Token: 0x04000024 RID: 36
		AdColony,
		// Token: 0x04000025 RID: 37
		AdMob,
		// Token: 0x04000026 RID: 38
		AerServ,
		// Token: 0x04000027 RID: 39
		Airpush,
		// Token: 0x04000028 RID: 40
		Altrooz,
		// Token: 0x04000029 RID: 41
		Ampush,
		// Token: 0x0400002A RID: 42
		AppleSearch,
		// Token: 0x0400002B RID: 43
		AppLift,
		// Token: 0x0400002C RID: 44
		AppLovin,
		// Token: 0x0400002D RID: 45
		Appnext,
		// Token: 0x0400002E RID: 46
		AppNexus,
		// Token: 0x0400002F RID: 47
		Appoday,
		// Token: 0x04000030 RID: 48
		Appodeal,
		// Token: 0x04000031 RID: 49
		AppsUnion,
		// Token: 0x04000032 RID: 50
		Avazu,
		// Token: 0x04000033 RID: 51
		BlueStacks,
		// Token: 0x04000034 RID: 52
		Chartboost,
		// Token: 0x04000035 RID: 53
		ClickDealer,
		// Token: 0x04000036 RID: 54
		CPAlead,
		// Token: 0x04000037 RID: 55
		CrossChannel,
		// Token: 0x04000038 RID: 56
		CrossInstall,
		// Token: 0x04000039 RID: 57
		Epom,
		// Token: 0x0400003A RID: 58
		Facebook,
		// Token: 0x0400003B RID: 59
		Fetch,
		// Token: 0x0400003C RID: 60
		Fiksu,
		// Token: 0x0400003D RID: 61
		Flurry,
		// Token: 0x0400003E RID: 62
		Fuse,
		// Token: 0x0400003F RID: 63
		Fyber,
		// Token: 0x04000040 RID: 64
		Glispa,
		// Token: 0x04000041 RID: 65
		Google,
		// Token: 0x04000042 RID: 66
		GrowMobile,
		// Token: 0x04000043 RID: 67
		HeyZap,
		// Token: 0x04000044 RID: 68
		HyperMX,
		// Token: 0x04000045 RID: 69
		Iddiction,
		// Token: 0x04000046 RID: 70
		IndexExchange,
		// Token: 0x04000047 RID: 71
		InMobi,
		// Token: 0x04000048 RID: 72
		Instagram,
		// Token: 0x04000049 RID: 73
		Instal,
		// Token: 0x0400004A RID: 74
		Ipsos,
		// Token: 0x0400004B RID: 75
		IronSource,
		// Token: 0x0400004C RID: 76
		Jirbo,
		// Token: 0x0400004D RID: 77
		Kimia,
		// Token: 0x0400004E RID: 78
		Leadbolt,
		// Token: 0x0400004F RID: 79
		Liftoff,
		// Token: 0x04000050 RID: 80
		Manage,
		// Token: 0x04000051 RID: 81
		Matomy,
		// Token: 0x04000052 RID: 82
		MediaBrix,
		// Token: 0x04000053 RID: 83
		MillenialMedia,
		// Token: 0x04000054 RID: 84
		Minimob,
		// Token: 0x04000055 RID: 85
		MobAir,
		// Token: 0x04000056 RID: 86
		MobileCore,
		// Token: 0x04000057 RID: 87
		Mobobeat,
		// Token: 0x04000058 RID: 88
		Mobusi,
		// Token: 0x04000059 RID: 89
		Mobvista,
		// Token: 0x0400005A RID: 90
		MoPub,
		// Token: 0x0400005B RID: 91
		Motive,
		// Token: 0x0400005C RID: 92
		Msales,
		// Token: 0x0400005D RID: 93
		NativeX,
		// Token: 0x0400005E RID: 94
		OpenX,
		// Token: 0x0400005F RID: 95
		Pandora,
		// Token: 0x04000060 RID: 96
		PropellerAds,
		// Token: 0x04000061 RID: 97
		Revmob,
		// Token: 0x04000062 RID: 98
		RubiconProject,
		// Token: 0x04000063 RID: 99
		SiriusAd,
		// Token: 0x04000064 RID: 100
		Smaato,
		// Token: 0x04000065 RID: 101
		SponsorPay,
		// Token: 0x04000066 RID: 102
		SpotXchange,
		// Token: 0x04000067 RID: 103
		StartApp,
		// Token: 0x04000068 RID: 104
		Tapjoy,
		// Token: 0x04000069 RID: 105
		Taptica,
		// Token: 0x0400006A RID: 106
		Tremor,
		// Token: 0x0400006B RID: 107
		TrialPay,
		// Token: 0x0400006C RID: 108
		Twitter,
		// Token: 0x0400006D RID: 109
		UnityAds,
		// Token: 0x0400006E RID: 110
		Vungle,
		// Token: 0x0400006F RID: 111
		Yeahmobi,
		// Token: 0x04000070 RID: 112
		YuMe
	}
}
