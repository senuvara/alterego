﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x0200001F RID: 31
	[StandardEventName("game_start", "Progression", "Send this event when gameplay starts. Usually used only in games with an identifiable conclusion.")]
	public struct GameStart
	{
	}
}
