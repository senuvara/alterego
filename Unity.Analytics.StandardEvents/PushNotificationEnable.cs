﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x0200002B RID: 43
	[StandardEventName("push_notification_enable", "Engagement", "Send this event when the player enables or grants permission for the game to use push notifications.")]
	public struct PushNotificationEnable
	{
	}
}
