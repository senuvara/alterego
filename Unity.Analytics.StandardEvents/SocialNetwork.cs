﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x02000011 RID: 17
	[EnumCase(EnumCase.Styles.Lower)]
	public enum SocialNetwork
	{
		// Token: 0x04000092 RID: 146
		None,
		// Token: 0x04000093 RID: 147
		Facebook,
		// Token: 0x04000094 RID: 148
		Twitter,
		// Token: 0x04000095 RID: 149
		Instagram,
		// Token: 0x04000096 RID: 150
		GooglePlus,
		// Token: 0x04000097 RID: 151
		Pinterest,
		// Token: 0x04000098 RID: 152
		WeChat,
		// Token: 0x04000099 RID: 153
		SinaWeibo,
		// Token: 0x0400009A RID: 154
		TencentWeibo,
		// Token: 0x0400009B RID: 155
		QQ,
		// Token: 0x0400009C RID: 156
		Zhihu,
		// Token: 0x0400009D RID: 157
		VK,
		// Token: 0x0400009E RID: 158
		OK_ru
	}
}
