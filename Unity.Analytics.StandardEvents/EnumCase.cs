﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x02000009 RID: 9
	public class EnumCase : AnalyticsEventAttribute
	{
		// Token: 0x06000051 RID: 81 RVA: 0x00003EB9 File Offset: 0x000020B9
		public EnumCase(EnumCase.Styles style)
		{
			this.Style = style;
		}

		// Token: 0x0400000D RID: 13
		public EnumCase.Styles Style;

		// Token: 0x0200000A RID: 10
		public enum Styles
		{
			// Token: 0x0400000F RID: 15
			None,
			// Token: 0x04000010 RID: 16
			Snake,
			// Token: 0x04000011 RID: 17
			Lower
		}
	}
}
