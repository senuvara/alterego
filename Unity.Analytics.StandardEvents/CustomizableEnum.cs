﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x02000008 RID: 8
	[AttributeUsage(AttributeTargets.Field)]
	public class CustomizableEnum : AnalyticsEventAttribute
	{
		// Token: 0x06000050 RID: 80 RVA: 0x00003EAA File Offset: 0x000020AA
		public CustomizableEnum(bool customizable)
		{
			this.Customizable = customizable;
		}

		// Token: 0x0400000C RID: 12
		public bool Customizable;
	}
}
