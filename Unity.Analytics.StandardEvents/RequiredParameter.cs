﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x02000006 RID: 6
	[AttributeUsage(AttributeTargets.Field)]
	public class RequiredParameter : AnalyticsEventParameter
	{
		// Token: 0x0600004E RID: 78 RVA: 0x00003E94 File Offset: 0x00002094
		public RequiredParameter(string sendName, string tooltip, string groupId = null) : base(sendName, tooltip, groupId)
		{
		}
	}
}
