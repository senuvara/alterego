﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x0200001B RID: 27
	[StandardEventName("chat_message_sent", "Engagement", "Send this event when the player sends a chat message in game.")]
	public struct ChatMessageSent
	{
	}
}
