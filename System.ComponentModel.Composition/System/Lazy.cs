﻿using System;
using System.Threading;

namespace System
{
	/// <summary>Provides a lazy indirect reference to an object and its associated metadata for use by the Managed Extensibility Framework.</summary>
	/// <typeparam name="T">The type of the object referenced.</typeparam>
	/// <typeparam name="TMetadata">The type of the metadata.</typeparam>
	// Token: 0x0200001C RID: 28
	[Serializable]
	public class Lazy<T, TMetadata> : Lazy<T>
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Lazy`2" /> class with the specified metadata that uses the specified function to get the referenced object.</summary>
		/// <param name="valueFactory">A function that returns the referenced object.</param>
		/// <param name="metadata">The metadata associated with the referenced object.</param>
		// Token: 0x060000F7 RID: 247 RVA: 0x000038CA File Offset: 0x00001ACA
		public Lazy(Func<T> valueFactory, TMetadata metadata) : base(valueFactory)
		{
			this._metadata = metadata;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Lazy`2" /> class with the specified metadata.</summary>
		/// <param name="metadata">The metadata associated with the referenced object.</param>
		// Token: 0x060000F8 RID: 248 RVA: 0x000038DA File Offset: 0x00001ADA
		public Lazy(TMetadata metadata)
		{
			this._metadata = metadata;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Lazy`2" /> class with the specified metadata and thread safety value.</summary>
		/// <param name="metadata">The metadata associated with the referenced object.</param>
		/// <param name="isThreadSafe">Indicates whether the <see cref="T:System.Lazy`2" /> object that is created will be thread-safe.</param>
		// Token: 0x060000F9 RID: 249 RVA: 0x000038E9 File Offset: 0x00001AE9
		public Lazy(TMetadata metadata, bool isThreadSafe) : base(isThreadSafe)
		{
			this._metadata = metadata;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Lazy`2" /> class with the specified metadata and thread safety value that uses the specified function to get the referenced object.</summary>
		/// <param name="valueFactory">A function that returns the referenced object.</param>
		/// <param name="metadata">The metadata associated with the referenced object.</param>
		/// <param name="isThreadSafe">Indicates whether the <see cref="T:System.Lazy`2" /> object that is created will be thread-safe.</param>
		// Token: 0x060000FA RID: 250 RVA: 0x000038F9 File Offset: 0x00001AF9
		public Lazy(Func<T> valueFactory, TMetadata metadata, bool isThreadSafe) : base(valueFactory, isThreadSafe)
		{
			this._metadata = metadata;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Lazy`2" /> class with the specified metadata and thread synchronization mode.</summary>
		/// <param name="metadata">The metadata associated with the referenced object.</param>
		/// <param name="mode">The thread synchronization mode.</param>
		// Token: 0x060000FB RID: 251 RVA: 0x0000390A File Offset: 0x00001B0A
		public Lazy(TMetadata metadata, LazyThreadSafetyMode mode) : base(mode)
		{
			this._metadata = metadata;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Lazy`2" /> class with the specified metadata and thread synchronization mode that uses the specified function to get the referenced object.</summary>
		/// <param name="valueFactory">A function that returns the referenced object</param>
		/// <param name="metadata">The metadata associated with the referenced object.</param>
		/// <param name="mode">The thread synchronization mode</param>
		// Token: 0x060000FC RID: 252 RVA: 0x0000391A File Offset: 0x00001B1A
		public Lazy(Func<T> valueFactory, TMetadata metadata, LazyThreadSafetyMode mode) : base(valueFactory, mode)
		{
			this._metadata = metadata;
		}

		/// <summary>Gets the metadata associated with the referenced object.</summary>
		/// <returns>The metadata associated with the referenced object.</returns>
		// Token: 0x1700007A RID: 122
		// (get) Token: 0x060000FD RID: 253 RVA: 0x0000392B File Offset: 0x00001B2B
		public TMetadata Metadata
		{
			get
			{
				return this._metadata;
			}
		}

		// Token: 0x04000053 RID: 83
		private TMetadata _metadata;
	}
}
