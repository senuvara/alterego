﻿using System;
using System.Diagnostics;

namespace System.ComponentModel
{
	// Token: 0x0200001D RID: 29
	[Conditional("NOT_FEATURE_LEGACYCOMPONENTMODEL")]
	internal sealed class LocalizableAttribute : Attribute
	{
		// Token: 0x060000FE RID: 254 RVA: 0x00003933 File Offset: 0x00001B33
		public LocalizableAttribute(bool isLocalizable)
		{
		}
	}
}
