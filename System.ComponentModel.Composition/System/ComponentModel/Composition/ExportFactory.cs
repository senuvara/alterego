﻿using System;
using System.ComponentModel.Composition.Primitives;

namespace System.ComponentModel.Composition
{
	/// <summary>A factory that creates new instances of a part that provides the specified export.</summary>
	/// <typeparam name="T">The type of the export.</typeparam>
	// Token: 0x02000036 RID: 54
	public class ExportFactory<T>
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.ExportFactory`1" /> class.</summary>
		/// <param name="exportLifetimeContextCreator">A function that returns the exported value and an <see cref="T:System.Action" /> that releases it.</param>
		// Token: 0x060001A3 RID: 419 RVA: 0x000052AB File Offset: 0x000034AB
		public ExportFactory(Func<Tuple<T, Action>> exportLifetimeContextCreator)
		{
			if (exportLifetimeContextCreator == null)
			{
				throw new ArgumentNullException("exportLifetimeContextCreator");
			}
			this._exportLifetimeContextCreator = exportLifetimeContextCreator;
		}

		/// <summary>Creates an instance of the factory's export type.</summary>
		/// <returns>A valid instance of the factory's exported type.</returns>
		// Token: 0x060001A4 RID: 420 RVA: 0x000052C8 File Offset: 0x000034C8
		public ExportLifetimeContext<T> CreateExport()
		{
			Tuple<T, Action> tuple = this._exportLifetimeContextCreator();
			return new ExportLifetimeContext<T>(tuple.Item1, tuple.Item2);
		}

		// Token: 0x060001A5 RID: 421 RVA: 0x000052F2 File Offset: 0x000034F2
		internal bool IncludeInScopedCatalog(ComposablePartDefinition composablePartDefinition)
		{
			return this.OnFilterScopedCatalog(composablePartDefinition);
		}

		// Token: 0x060001A6 RID: 422 RVA: 0x000052FB File Offset: 0x000034FB
		protected virtual bool OnFilterScopedCatalog(ComposablePartDefinition composablePartDefinition)
		{
			return true;
		}

		// Token: 0x040000A0 RID: 160
		private Func<Tuple<T, Action>> _exportLifetimeContextCreator;
	}
}
