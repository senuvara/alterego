﻿using System;
using System.Runtime.CompilerServices;

namespace System.ComponentModel.Composition
{
	/// <summary>Specifies metadata for a part.</summary>
	// Token: 0x0200004F RID: 79
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
	public sealed class PartMetadataAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.PartMetadataAttribute" /> class with the specified name and metadata value.</summary>
		/// <param name="name">A string that contains the name of the metadata value or <see langword="null" /> to use an empty string ("").</param>
		/// <param name="value">An object that contains the metadata value. This can be <see langword="null" />.</param>
		// Token: 0x0600020D RID: 525 RVA: 0x00005B33 File Offset: 0x00003D33
		public PartMetadataAttribute(string name, object value)
		{
			this.Name = (name ?? string.Empty);
			this.Value = value;
		}

		/// <summary>Gets the name of the metadata value.</summary>
		/// <returns> A string that contains the name of the metadata value.</returns>
		// Token: 0x170000AE RID: 174
		// (get) Token: 0x0600020E RID: 526 RVA: 0x00005B52 File Offset: 0x00003D52
		// (set) Token: 0x0600020F RID: 527 RVA: 0x00005B5A File Offset: 0x00003D5A
		public string Name
		{
			[CompilerGenerated]
			get
			{
				return this.<Name>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Name>k__BackingField = value;
			}
		}

		/// <summary>Gets the metadata value.</summary>
		/// <returns> An object that contains the metadata value.</returns>
		// Token: 0x170000AF RID: 175
		// (get) Token: 0x06000210 RID: 528 RVA: 0x00005B63 File Offset: 0x00003D63
		// (set) Token: 0x06000211 RID: 529 RVA: 0x00005B6B File Offset: 0x00003D6B
		public object Value
		{
			[CompilerGenerated]
			get
			{
				return this.<Value>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Value>k__BackingField = value;
			}
		}

		// Token: 0x040000C6 RID: 198
		[CompilerGenerated]
		private string <Name>k__BackingField;

		// Token: 0x040000C7 RID: 199
		[CompilerGenerated]
		private object <Value>k__BackingField;
	}
}
