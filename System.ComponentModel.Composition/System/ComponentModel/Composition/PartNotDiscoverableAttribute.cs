﻿using System;

namespace System.ComponentModel.Composition
{
	/// <summary>Specifies that this type’s exports won’t be included in a <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartCatalog" />.</summary>
	// Token: 0x02000050 RID: 80
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
	public sealed class PartNotDiscoverableAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.PartNotDiscoverableAttribute" /> class.</summary>
		// Token: 0x06000212 RID: 530 RVA: 0x00003933 File Offset: 0x00001B33
		public PartNotDiscoverableAttribute()
		{
		}
	}
}
