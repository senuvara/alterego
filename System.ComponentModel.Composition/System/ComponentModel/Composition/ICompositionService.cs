﻿using System;
using System.ComponentModel.Composition.Primitives;

namespace System.ComponentModel.Composition
{
	/// <summary>Provides methods to satisfy imports on an existing part instance.</summary>
	// Token: 0x02000041 RID: 65
	public interface ICompositionService
	{
		/// <summary>Composes the specified part, with recomposition and validation disabled.</summary>
		/// <param name="part">The part to compose.</param>
		// Token: 0x060001D2 RID: 466
		void SatisfyImportsOnce(ComposablePart part);
	}
}
