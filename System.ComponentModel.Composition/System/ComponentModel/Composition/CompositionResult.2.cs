﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Internal.Collections;

namespace System.ComponentModel.Composition
{
	// Token: 0x0200002E RID: 46
	internal struct CompositionResult<T>
	{
		// Token: 0x06000161 RID: 353 RVA: 0x000045C2 File Offset: 0x000027C2
		public CompositionResult(T value)
		{
			this = new CompositionResult<T>(value, null);
		}

		// Token: 0x06000162 RID: 354 RVA: 0x000045CC File Offset: 0x000027CC
		public CompositionResult(params CompositionError[] errors)
		{
			this = new CompositionResult<T>(default(T), errors);
		}

		// Token: 0x06000163 RID: 355 RVA: 0x000045EC File Offset: 0x000027EC
		public CompositionResult(IEnumerable<CompositionError> errors)
		{
			this = new CompositionResult<T>(default(T), errors);
		}

		// Token: 0x06000164 RID: 356 RVA: 0x00004609 File Offset: 0x00002809
		internal CompositionResult(T value, IEnumerable<CompositionError> errors)
		{
			this._errors = errors;
			this._value = value;
		}

		// Token: 0x1700008C RID: 140
		// (get) Token: 0x06000165 RID: 357 RVA: 0x00004619 File Offset: 0x00002819
		public bool Succeeded
		{
			get
			{
				return this._errors == null || !this._errors.FastAny<CompositionError>();
			}
		}

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x06000166 RID: 358 RVA: 0x00004633 File Offset: 0x00002833
		public IEnumerable<CompositionError> Errors
		{
			get
			{
				return this._errors ?? Enumerable.Empty<CompositionError>();
			}
		}

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x06000167 RID: 359 RVA: 0x00004644 File Offset: 0x00002844
		public T Value
		{
			get
			{
				this.ThrowOnErrors();
				return this._value;
			}
		}

		// Token: 0x06000168 RID: 360 RVA: 0x00004652 File Offset: 0x00002852
		internal CompositionResult<TValue> ToResult<TValue>()
		{
			return new CompositionResult<TValue>(this._errors);
		}

		// Token: 0x06000169 RID: 361 RVA: 0x0000465F File Offset: 0x0000285F
		internal CompositionResult ToResult()
		{
			return new CompositionResult(this._errors);
		}

		// Token: 0x0600016A RID: 362 RVA: 0x0000466C File Offset: 0x0000286C
		private void ThrowOnErrors()
		{
			if (!this.Succeeded)
			{
				throw new CompositionException(this._errors);
			}
		}

		// Token: 0x0400007E RID: 126
		private readonly IEnumerable<CompositionError> _errors;

		// Token: 0x0400007F RID: 127
		private readonly T _value;
	}
}
