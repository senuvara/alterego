﻿using System;
using System.Runtime.CompilerServices;

namespace System.ComponentModel.Composition
{
	/// <summary>Specifies that a type, property, field, or method provides a particular export.</summary>
	// Token: 0x02000034 RID: 52
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = true, Inherited = false)]
	public class ExportAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.ExportAttribute" /> class, exporting the type or member marked with this attribute under the default contract name.</summary>
		// Token: 0x0600019B RID: 411 RVA: 0x00005255 File Offset: 0x00003455
		public ExportAttribute() : this(null, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.ExportAttribute" /> class, exporting the type or member marked with this attribute under a contract name derived from the specified type.</summary>
		/// <param name="contractType">A type from which to derive the contract name that is used to export the type or member marked with this attribute, or <see langword="null" /> to use the default contract name.</param>
		// Token: 0x0600019C RID: 412 RVA: 0x0000525F File Offset: 0x0000345F
		public ExportAttribute(Type contractType) : this(null, contractType)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.ExportAttribute" /> class, exporting the type or member marked with this attribute under the specified contract name.</summary>
		/// <param name="contractName">The contract name that is used to export the type or member marked with this attribute, or <see langword="null" /> or an empty string ("") to use the default contract name.</param>
		// Token: 0x0600019D RID: 413 RVA: 0x00005269 File Offset: 0x00003469
		public ExportAttribute(string contractName) : this(contractName, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.ExportAttribute" /> class, exporting the specified type under the specified contract name.</summary>
		/// <param name="contractName">The contract name that is used to export the type or member marked with this attribute, or <see langword="null" /> or an empty string ("") to use the default contract name.</param>
		/// <param name="contractType">The type to export.</param>
		// Token: 0x0600019E RID: 414 RVA: 0x00005273 File Offset: 0x00003473
		public ExportAttribute(string contractName, Type contractType)
		{
			this.ContractName = contractName;
			this.ContractType = contractType;
		}

		/// <summary>Gets the contract name that is used to export the type or member marked with this attribute.</summary>
		/// <returns>The contract name that is used to export the type or member marked with this attribute. The default value is an empty string ("").</returns>
		// Token: 0x17000090 RID: 144
		// (get) Token: 0x0600019F RID: 415 RVA: 0x00005289 File Offset: 0x00003489
		// (set) Token: 0x060001A0 RID: 416 RVA: 0x00005291 File Offset: 0x00003491
		public string ContractName
		{
			[CompilerGenerated]
			get
			{
				return this.<ContractName>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ContractName>k__BackingField = value;
			}
		}

		/// <summary>Gets the contract type that is exported by the member that this attribute is attached to.</summary>
		/// <returns>The type of export that is be provided. The default value is <see langword="null" />, which means that the type will be obtained by looking at the type on the member that this export is attached to. </returns>
		// Token: 0x17000091 RID: 145
		// (get) Token: 0x060001A1 RID: 417 RVA: 0x0000529A File Offset: 0x0000349A
		// (set) Token: 0x060001A2 RID: 418 RVA: 0x000052A2 File Offset: 0x000034A2
		public Type ContractType
		{
			[CompilerGenerated]
			get
			{
				return this.<ContractType>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ContractType>k__BackingField = value;
			}
		}

		// Token: 0x0400009A RID: 154
		[CompilerGenerated]
		private string <ContractName>k__BackingField;

		// Token: 0x0400009B RID: 155
		[CompilerGenerated]
		private Type <ContractType>k__BackingField;
	}
}
