﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using Microsoft.Internal.Collections;

namespace System.ComponentModel.Composition
{
	// Token: 0x0200002D RID: 45
	internal struct CompositionResult
	{
		// Token: 0x06000156 RID: 342 RVA: 0x000044FC File Offset: 0x000026FC
		public CompositionResult(params CompositionError[] errors)
		{
			this = new CompositionResult(errors);
		}

		// Token: 0x06000157 RID: 343 RVA: 0x00004505 File Offset: 0x00002705
		public CompositionResult(IEnumerable<CompositionError> errors)
		{
			this._errors = errors;
		}

		// Token: 0x1700008A RID: 138
		// (get) Token: 0x06000158 RID: 344 RVA: 0x0000450E File Offset: 0x0000270E
		public bool Succeeded
		{
			get
			{
				return this._errors == null || !this._errors.FastAny<CompositionError>();
			}
		}

		// Token: 0x1700008B RID: 139
		// (get) Token: 0x06000159 RID: 345 RVA: 0x00004528 File Offset: 0x00002728
		public IEnumerable<CompositionError> Errors
		{
			get
			{
				return this._errors ?? Enumerable.Empty<CompositionError>();
			}
		}

		// Token: 0x0600015A RID: 346 RVA: 0x00004539 File Offset: 0x00002739
		public CompositionResult MergeResult(CompositionResult result)
		{
			if (this.Succeeded)
			{
				return result;
			}
			if (result.Succeeded)
			{
				return this;
			}
			return this.MergeErrors(result._errors);
		}

		// Token: 0x0600015B RID: 347 RVA: 0x00004561 File Offset: 0x00002761
		public CompositionResult MergeError(CompositionError error)
		{
			return this.MergeErrors(new CompositionError[]
			{
				error
			});
		}

		// Token: 0x0600015C RID: 348 RVA: 0x00004573 File Offset: 0x00002773
		public CompositionResult MergeErrors(IEnumerable<CompositionError> errors)
		{
			return new CompositionResult(this._errors.ConcatAllowingNull(errors));
		}

		// Token: 0x0600015D RID: 349 RVA: 0x00004586 File Offset: 0x00002786
		public CompositionResult<T> ToResult<T>(T value)
		{
			return new CompositionResult<T>(value, this._errors);
		}

		// Token: 0x0600015E RID: 350 RVA: 0x00004594 File Offset: 0x00002794
		public void ThrowOnErrors()
		{
			this.ThrowOnErrors(null);
		}

		// Token: 0x0600015F RID: 351 RVA: 0x0000459D File Offset: 0x0000279D
		public void ThrowOnErrors(AtomicComposition atomicComposition)
		{
			if (this.Succeeded)
			{
				return;
			}
			if (atomicComposition == null)
			{
				throw new CompositionException(this._errors);
			}
			throw new ChangeRejectedException(this._errors);
		}

		// Token: 0x06000160 RID: 352 RVA: 0x00002304 File Offset: 0x00000504
		// Note: this type is marked as 'beforefieldinit'.
		static CompositionResult()
		{
		}

		// Token: 0x0400007C RID: 124
		public static readonly CompositionResult SucceededResult;

		// Token: 0x0400007D RID: 125
		private readonly IEnumerable<CompositionError> _errors;
	}
}
