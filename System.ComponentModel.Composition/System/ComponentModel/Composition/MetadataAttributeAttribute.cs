﻿using System;

namespace System.ComponentModel.Composition
{
	/// <summary>Specifies that a custom attribute’s properties provide metadata for exports applied to the same type, property, field, or method.</summary>
	// Token: 0x0200004A RID: 74
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	public sealed class MetadataAttributeAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.MetadataAttributeAttribute" /> class.</summary>
		// Token: 0x06000200 RID: 512 RVA: 0x00003933 File Offset: 0x00001B33
		public MetadataAttributeAttribute()
		{
		}
	}
}
