﻿using System;

namespace System.ComponentModel.Composition
{
	// Token: 0x02000035 RID: 53
	internal enum ExportCardinalityCheckResult
	{
		// Token: 0x0400009D RID: 157
		Match,
		// Token: 0x0400009E RID: 158
		NoExports,
		// Token: 0x0400009F RID: 159
		TooManyExports
	}
}
