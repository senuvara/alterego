﻿using System;
using System.ComponentModel.Composition.Primitives;

namespace System.ComponentModel.Composition
{
	// Token: 0x02000040 RID: 64
	internal interface IAttributedImport
	{
		// Token: 0x17000097 RID: 151
		// (get) Token: 0x060001CC RID: 460
		string ContractName { get; }

		// Token: 0x17000098 RID: 152
		// (get) Token: 0x060001CD RID: 461
		Type ContractType { get; }

		// Token: 0x17000099 RID: 153
		// (get) Token: 0x060001CE RID: 462
		bool AllowRecomposition { get; }

		// Token: 0x1700009A RID: 154
		// (get) Token: 0x060001CF RID: 463
		CreationPolicy RequiredCreationPolicy { get; }

		// Token: 0x1700009B RID: 155
		// (get) Token: 0x060001D0 RID: 464
		ImportCardinality Cardinality { get; }

		// Token: 0x1700009C RID: 156
		// (get) Token: 0x060001D1 RID: 465
		ImportSource Source { get; }
	}
}
