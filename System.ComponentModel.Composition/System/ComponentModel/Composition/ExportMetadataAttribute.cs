﻿using System;
using System.Runtime.CompilerServices;

namespace System.ComponentModel.Composition
{
	/// <summary>Specifies metadata for a type, property, field, or method marked with the <see cref="T:System.ComponentModel.Composition.ExportAttribute" />.</summary>
	// Token: 0x02000039 RID: 57
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Interface, AllowMultiple = true, Inherited = false)]
	public sealed class ExportMetadataAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.ExportMetadataAttribute" /> with the specified name and metadata value.</summary>
		/// <param name="name">A string that contains the name of the metadata value, or <see langword="null" /> to set the <see cref="P:System.ComponentModel.Composition.ExportMetadataAttribute.Name" /> property to an empty string ("").</param>
		/// <param name="value">An object that contains the metadata value. This can be <see langword="null" />.</param>
		// Token: 0x060001AC RID: 428 RVA: 0x00005349 File Offset: 0x00003549
		public ExportMetadataAttribute(string name, object value)
		{
			this.Name = (name ?? string.Empty);
			this.Value = value;
		}

		/// <summary>Gets the name of the metadata value.</summary>
		/// <returns> A string that contains the name of the metadata value.</returns>
		// Token: 0x17000094 RID: 148
		// (get) Token: 0x060001AD RID: 429 RVA: 0x00005368 File Offset: 0x00003568
		// (set) Token: 0x060001AE RID: 430 RVA: 0x00005370 File Offset: 0x00003570
		public string Name
		{
			[CompilerGenerated]
			get
			{
				return this.<Name>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Name>k__BackingField = value;
			}
		}

		/// <summary>Gets the metadata value.</summary>
		/// <returns> An object that contains the metadata value.</returns>
		// Token: 0x17000095 RID: 149
		// (get) Token: 0x060001AF RID: 431 RVA: 0x00005379 File Offset: 0x00003579
		// (set) Token: 0x060001B0 RID: 432 RVA: 0x00005381 File Offset: 0x00003581
		public object Value
		{
			[CompilerGenerated]
			get
			{
				return this.<Value>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Value>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether this item is marked with this attribute more than once.</summary>
		/// <returns>
		///     <see langword="true" /> if the item is marked more than once; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000096 RID: 150
		// (get) Token: 0x060001B1 RID: 433 RVA: 0x0000538A File Offset: 0x0000358A
		// (set) Token: 0x060001B2 RID: 434 RVA: 0x00005392 File Offset: 0x00003592
		public bool IsMultiple
		{
			[CompilerGenerated]
			get
			{
				return this.<IsMultiple>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<IsMultiple>k__BackingField = value;
			}
		}

		// Token: 0x040000A4 RID: 164
		[CompilerGenerated]
		private string <Name>k__BackingField;

		// Token: 0x040000A5 RID: 165
		[CompilerGenerated]
		private object <Value>k__BackingField;

		// Token: 0x040000A6 RID: 166
		[CompilerGenerated]
		private bool <IsMultiple>k__BackingField;
	}
}
