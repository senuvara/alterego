﻿using System;
using Microsoft.Internal;

namespace System.ComponentModel.Composition
{
	// Token: 0x02000045 RID: 69
	internal class ImportCardinalityMismatchExceptionDebuggerProxy
	{
		// Token: 0x060001E9 RID: 489 RVA: 0x00005834 File Offset: 0x00003A34
		public ImportCardinalityMismatchExceptionDebuggerProxy(ImportCardinalityMismatchException exception)
		{
			Requires.NotNull<ImportCardinalityMismatchException>(exception, "exception");
			this._exception = exception;
		}

		// Token: 0x170000A4 RID: 164
		// (get) Token: 0x060001EA RID: 490 RVA: 0x0000584E File Offset: 0x00003A4E
		public Exception InnerException
		{
			get
			{
				return this._exception.InnerException;
			}
		}

		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x060001EB RID: 491 RVA: 0x0000585B File Offset: 0x00003A5B
		public string Message
		{
			get
			{
				return this._exception.Message;
			}
		}

		// Token: 0x040000B7 RID: 183
		private readonly ImportCardinalityMismatchException _exception;
	}
}
