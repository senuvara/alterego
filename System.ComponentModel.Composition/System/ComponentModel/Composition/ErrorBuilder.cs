﻿using System;
using System.ComponentModel.Composition.Primitives;
using Microsoft.Internal;

namespace System.ComponentModel.Composition
{
	// Token: 0x02000032 RID: 50
	internal static class ErrorBuilder
	{
		// Token: 0x06000189 RID: 393 RVA: 0x00004FAC File Offset: 0x000031AC
		public static CompositionError PreventedByExistingImport(ComposablePart part, ImportDefinition import)
		{
			return CompositionError.Create(CompositionErrorId.ImportEngine_PreventedByExistingImport, Strings.ImportEngine_PreventedByExistingImport, new object[]
			{
				import.ToElement().DisplayName,
				part.ToElement().DisplayName
			});
		}

		// Token: 0x0600018A RID: 394 RVA: 0x00004FDC File Offset: 0x000031DC
		public static CompositionError InvalidStateForRecompposition(ComposablePart part)
		{
			return CompositionError.Create(CompositionErrorId.ImportEngine_InvalidStateForRecomposition, Strings.ImportEngine_InvalidStateForRecomposition, new object[]
			{
				part.ToElement().DisplayName
			});
		}

		// Token: 0x0600018B RID: 395 RVA: 0x00004FFE File Offset: 0x000031FE
		public static CompositionError ComposeTookTooManyIterations(int maximumNumberOfCompositionIterations)
		{
			return CompositionError.Create(CompositionErrorId.ImportEngine_ComposeTookTooManyIterations, Strings.ImportEngine_ComposeTookTooManyIterations, new object[]
			{
				maximumNumberOfCompositionIterations
			});
		}

		// Token: 0x0600018C RID: 396 RVA: 0x0000501A File Offset: 0x0000321A
		public static CompositionError CreateImportCardinalityMismatch(ImportCardinalityMismatchException exception, ImportDefinition definition)
		{
			Assumes.NotNull<ImportCardinalityMismatchException, ImportDefinition>(exception, definition);
			CompositionErrorId id = CompositionErrorId.ImportEngine_ImportCardinalityMismatch;
			string message = exception.Message;
			object[] array = new object[2];
			array[0] = definition.ToElement();
			return CompositionError.Create(id, message, array);
		}

		// Token: 0x0600018D RID: 397 RVA: 0x00005040 File Offset: 0x00003240
		public static CompositionError CreatePartCannotActivate(ComposablePart part, Exception innerException)
		{
			Assumes.NotNull<ComposablePart, Exception>(part, innerException);
			ICompositionElement compositionElement = part.ToElement();
			return CompositionError.Create(CompositionErrorId.ImportEngine_PartCannotActivate, compositionElement, innerException, Strings.ImportEngine_PartCannotActivate, new object[]
			{
				compositionElement.DisplayName
			});
		}

		// Token: 0x0600018E RID: 398 RVA: 0x00005078 File Offset: 0x00003278
		public static CompositionError CreatePartCannotSetImport(ComposablePart part, ImportDefinition definition, Exception innerException)
		{
			Assumes.NotNull<ComposablePart, ImportDefinition, Exception>(part, definition, innerException);
			ICompositionElement compositionElement = definition.ToElement();
			return CompositionError.Create(CompositionErrorId.ImportEngine_PartCannotSetImport, compositionElement, innerException, Strings.ImportEngine_PartCannotSetImport, new object[]
			{
				compositionElement.DisplayName,
				part.ToElement().DisplayName
			});
		}

		// Token: 0x0600018F RID: 399 RVA: 0x000050C0 File Offset: 0x000032C0
		public static CompositionError CreateCannotGetExportedValue(ComposablePart part, ExportDefinition definition, Exception innerException)
		{
			Assumes.NotNull<ComposablePart, ExportDefinition, Exception>(part, definition, innerException);
			ICompositionElement compositionElement = definition.ToElement();
			return CompositionError.Create(CompositionErrorId.ImportEngine_PartCannotGetExportedValue, compositionElement, innerException, Strings.ImportEngine_PartCannotGetExportedValue, new object[]
			{
				compositionElement.DisplayName,
				part.ToElement().DisplayName
			});
		}

		// Token: 0x06000190 RID: 400 RVA: 0x00005108 File Offset: 0x00003308
		public static CompositionError CreatePartCycle(ComposablePart part)
		{
			Assumes.NotNull<ComposablePart>(part);
			ICompositionElement compositionElement = part.ToElement();
			return CompositionError.Create(CompositionErrorId.ImportEngine_PartCycle, compositionElement, Strings.ImportEngine_PartCycle, new object[]
			{
				compositionElement.DisplayName
			});
		}
	}
}
