﻿using System;
using System.ComponentModel.Composition.Primitives;
using System.Diagnostics;
using System.Globalization;

namespace System.ComponentModel.Composition
{
	/// <summary>Represents an error that occurred during composition.</summary>
	// Token: 0x02000024 RID: 36
	[DebuggerTypeProxy(typeof(CompositionErrorDebuggerProxy))]
	[Serializable]
	public class CompositionError
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.CompositionError" /> class with the specified error message.</summary>
		/// <param name="message">A message that describes the <see cref="T:System.ComponentModel.Composition.CompositionError" /> or <see langword="null" /> to set the <see cref="P:System.ComponentModel.Composition.CompositionError.Description" /> property to an empty string ("").</param>
		// Token: 0x06000126 RID: 294 RVA: 0x00003DE3 File Offset: 0x00001FE3
		public CompositionError(string message) : this(CompositionErrorId.Unknown, message, null, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.CompositionError" /> class with the specified error message and the composition element that is the cause of the composition error.</summary>
		/// <param name="message">A message that describes the <see cref="T:System.ComponentModel.Composition.CompositionError" /> or <see langword="null" /> to set the <see cref="P:System.ComponentModel.Composition.CompositionError.Description" /> property to an empty string ("").</param>
		/// <param name="element">The composition element that is the cause of the <see cref="T:System.ComponentModel.Composition.CompositionError" /> or <see langword="null" /> to set the <see cref="P:System.ComponentModel.Composition.CompositionError.Element" /> property to <see langword="null" />.</param>
		// Token: 0x06000127 RID: 295 RVA: 0x00003DEF File Offset: 0x00001FEF
		public CompositionError(string message, ICompositionElement element) : this(CompositionErrorId.Unknown, message, element, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.CompositionError" /> class with the specified error message and the exception that is the cause of the composition error.</summary>
		/// <param name="message">A message that describes the <see cref="T:System.ComponentModel.Composition.CompositionError" /> or <see langword="null" /> to set the <see cref="P:System.ComponentModel.Composition.CompositionError.Description" /> property to an empty string ("").</param>
		/// <param name="exception">The <see cref="P:System.ComponentModel.Composition.CompositionError.Exception" /> that is the underlying cause of the <see cref="T:System.ComponentModel.Composition.CompositionError" /> or <see langword="null" /> to set the <see cref="P:System.ComponentModel.Composition.CompositionError.Exception" /> property to <see langword="null" />.</param>
		// Token: 0x06000128 RID: 296 RVA: 0x00003DFB File Offset: 0x00001FFB
		public CompositionError(string message, Exception exception) : this(CompositionErrorId.Unknown, message, null, exception)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.CompositionError" /> class with the specified error message, and the composition element and exception that are the cause of the composition error.</summary>
		/// <param name="message">A message that describes the <see cref="T:System.ComponentModel.Composition.CompositionError" /> or <see langword="null" /> to set the <see cref="P:System.ComponentModel.Composition.CompositionError.Description" /> property to an empty string ("").</param>
		/// <param name="element">The composition element that is the cause of the <see cref="T:System.ComponentModel.Composition.CompositionError" /> or <see langword="null" /> to set the <see cref="P:System.ComponentModel.Composition.CompositionError.Element" /> property to <see langword="null" />.</param>
		/// <param name="exception">The <see cref="P:System.ComponentModel.Composition.CompositionError.Exception" /> that is the underlying cause of the <see cref="T:System.ComponentModel.Composition.CompositionError" /> or <see langword="null" /> to set the <see cref="P:System.ComponentModel.Composition.CompositionError.Exception" /> property to <see langword="null" />.</param>
		// Token: 0x06000129 RID: 297 RVA: 0x00003E07 File Offset: 0x00002007
		public CompositionError(string message, ICompositionElement element, Exception exception) : this(CompositionErrorId.Unknown, message, element, exception)
		{
		}

		// Token: 0x0600012A RID: 298 RVA: 0x00003E13 File Offset: 0x00002013
		internal CompositionError(CompositionErrorId id, string description, ICompositionElement element, Exception exception)
		{
			this._id = id;
			this._description = (description ?? string.Empty);
			this._element = element;
			this._exception = exception;
		}

		/// <summary>Gets the composition element that is the cause of the error.</summary>
		/// <returns>The composition element that is the cause of the <see cref="T:System.ComponentModel.Composition.CompositionError" />. The default is <see langword="null" />.</returns>
		// Token: 0x1700007C RID: 124
		// (get) Token: 0x0600012B RID: 299 RVA: 0x00003E41 File Offset: 0x00002041
		public ICompositionElement Element
		{
			get
			{
				return this._element;
			}
		}

		/// <summary>Gets a description of the composition error.</summary>
		/// <returns>A message that describes the <see cref="T:System.ComponentModel.Composition.CompositionError" />.</returns>
		// Token: 0x1700007D RID: 125
		// (get) Token: 0x0600012C RID: 300 RVA: 0x00003E49 File Offset: 0x00002049
		public string Description
		{
			get
			{
				return this._description;
			}
		}

		/// <summary>Gets the exception that is the underlying cause of the composition error.</summary>
		/// <returns>The exception that is the underlying cause of the <see cref="T:System.ComponentModel.Composition.CompositionError" />. The default is <see langword="null" />.</returns>
		// Token: 0x1700007E RID: 126
		// (get) Token: 0x0600012D RID: 301 RVA: 0x00003E51 File Offset: 0x00002051
		public Exception Exception
		{
			get
			{
				return this._exception;
			}
		}

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x0600012E RID: 302 RVA: 0x00003E59 File Offset: 0x00002059
		internal CompositionErrorId Id
		{
			get
			{
				return this._id;
			}
		}

		// Token: 0x17000080 RID: 128
		// (get) Token: 0x0600012F RID: 303 RVA: 0x00003E61 File Offset: 0x00002061
		internal Exception InnerException
		{
			get
			{
				return this.Exception;
			}
		}

		/// <summary>Returns a string representation of the composition error.</summary>
		/// <returns>A string that contains the <see cref="P:System.ComponentModel.Composition.CompositionError.Description" /> property.</returns>
		// Token: 0x06000130 RID: 304 RVA: 0x00003E69 File Offset: 0x00002069
		public override string ToString()
		{
			return this.Description;
		}

		// Token: 0x06000131 RID: 305 RVA: 0x00003E71 File Offset: 0x00002071
		internal static CompositionError Create(CompositionErrorId id, string format, params object[] parameters)
		{
			return CompositionError.Create(id, null, null, format, parameters);
		}

		// Token: 0x06000132 RID: 306 RVA: 0x00003E7D File Offset: 0x0000207D
		internal static CompositionError Create(CompositionErrorId id, ICompositionElement element, string format, params object[] parameters)
		{
			return CompositionError.Create(id, element, null, format, parameters);
		}

		// Token: 0x06000133 RID: 307 RVA: 0x00003E89 File Offset: 0x00002089
		internal static CompositionError Create(CompositionErrorId id, ICompositionElement element, Exception exception, string format, params object[] parameters)
		{
			return new CompositionError(id, string.Format(CultureInfo.CurrentCulture, format, parameters), element, exception);
		}

		// Token: 0x04000058 RID: 88
		private readonly CompositionErrorId _id;

		// Token: 0x04000059 RID: 89
		private readonly string _description;

		// Token: 0x0400005A RID: 90
		private readonly Exception _exception;

		// Token: 0x0400005B RID: 91
		private readonly ICompositionElement _element;
	}
}
