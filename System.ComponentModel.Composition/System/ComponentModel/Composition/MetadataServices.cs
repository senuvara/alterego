﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.Internal;

namespace System.ComponentModel.Composition
{
	// Token: 0x0200004B RID: 75
	internal static class MetadataServices
	{
		// Token: 0x06000201 RID: 513 RVA: 0x0000591B File Offset: 0x00003B1B
		public static IDictionary<string, object> AsReadOnly(this IDictionary<string, object> metadata)
		{
			if (metadata == null)
			{
				return MetadataServices.EmptyMetadata;
			}
			if (metadata is ReadOnlyDictionary<string, object>)
			{
				return metadata;
			}
			return new ReadOnlyDictionary<string, object>(metadata);
		}

		// Token: 0x06000202 RID: 514 RVA: 0x00005938 File Offset: 0x00003B38
		public static T GetValue<T>(this IDictionary<string, object> metadata, string key)
		{
			Assumes.NotNull<IDictionary<string, object>, string>(metadata, "metadata");
			object obj = true;
			if (!metadata.TryGetValue(key, out obj))
			{
				return default(T);
			}
			if (obj is T)
			{
				return (T)((object)obj);
			}
			return default(T);
		}

		// Token: 0x06000203 RID: 515 RVA: 0x00005984 File Offset: 0x00003B84
		// Note: this type is marked as 'beforefieldinit'.
		static MetadataServices()
		{
		}

		// Token: 0x040000C1 RID: 193
		public static readonly IDictionary<string, object> EmptyMetadata = new ReadOnlyDictionary<string, object>(new Dictionary<string, object>(0));
	}
}
