﻿using System;
using System.ComponentModel.Composition.Diagnostics;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.ComponentModel.Composition.ReflectionModel;
using System.Globalization;
using System.Reflection;
using System.Runtime.CompilerServices;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.AttributedModel
{
	// Token: 0x02000101 RID: 257
	internal static class AttributedModelDiscovery
	{
		// Token: 0x060006AE RID: 1710 RVA: 0x00013F44 File Offset: 0x00012144
		public static ComposablePartDefinition CreatePartDefinitionIfDiscoverable(Type type, ICompositionElement origin)
		{
			AttributedPartCreationInfo attributedPartCreationInfo = new AttributedPartCreationInfo(type, null, false, origin);
			if (!attributedPartCreationInfo.IsPartDiscoverable())
			{
				return null;
			}
			return new ReflectionComposablePartDefinition(attributedPartCreationInfo);
		}

		// Token: 0x060006AF RID: 1711 RVA: 0x00013F6B File Offset: 0x0001216B
		public static ReflectionComposablePartDefinition CreatePartDefinition(Type type, PartCreationPolicyAttribute partCreationPolicy, bool ignoreConstructorImports, ICompositionElement origin)
		{
			Assumes.NotNull<Type>(type);
			return new ReflectionComposablePartDefinition(new AttributedPartCreationInfo(type, partCreationPolicy, ignoreConstructorImports, origin));
		}

		// Token: 0x060006B0 RID: 1712 RVA: 0x00013F81 File Offset: 0x00012181
		public static ReflectionComposablePart CreatePart(object attributedPart)
		{
			Assumes.NotNull<object>(attributedPart);
			return new ReflectionComposablePart(AttributedModelDiscovery.CreatePartDefinition(attributedPart.GetType(), PartCreationPolicyAttribute.Shared, true, null), attributedPart);
		}

		// Token: 0x060006B1 RID: 1713 RVA: 0x00013FA4 File Offset: 0x000121A4
		public static ReflectionComposablePart CreatePart(object attributedPart, ReflectionContext reflectionContext)
		{
			Assumes.NotNull<object>(attributedPart);
			Assumes.NotNull<ReflectionContext>(reflectionContext);
			TypeInfo typeInfo = reflectionContext.MapType(attributedPart.GetType().GetTypeInfo());
			if (typeInfo.Assembly.ReflectionOnly)
			{
				throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, Strings.Argument_ReflectionContextReturnsReflectionOnlyType, "reflectionContext"), "reflectionContext");
			}
			return AttributedModelDiscovery.CreatePart(AttributedModelDiscovery.CreatePartDefinition(typeInfo, PartCreationPolicyAttribute.Shared, true, null), attributedPart);
		}

		// Token: 0x060006B2 RID: 1714 RVA: 0x0001400C File Offset: 0x0001220C
		public static ReflectionComposablePart CreatePart(ComposablePartDefinition partDefinition, object attributedPart)
		{
			Assumes.NotNull<ComposablePartDefinition>(partDefinition);
			Assumes.NotNull<object>(attributedPart);
			return new ReflectionComposablePart((ReflectionComposablePartDefinition)partDefinition, attributedPart);
		}

		// Token: 0x060006B3 RID: 1715 RVA: 0x00014028 File Offset: 0x00012228
		public static ReflectionParameterImportDefinition CreateParameterImportDefinition(ParameterInfo parameter, ICompositionElement origin)
		{
			Requires.NotNull<ParameterInfo>(parameter, "parameter");
			ReflectionParameter reflectionParameter = parameter.ToReflectionParameter();
			IAttributedImport attributedImport = AttributedModelDiscovery.GetAttributedImport(reflectionParameter, parameter);
			ImportType importType = new ImportType(reflectionParameter.ReturnType, attributedImport.Cardinality);
			if (importType.IsPartCreator)
			{
				return new PartCreatorParameterImportDefinition(new Lazy<ParameterInfo>(() => parameter), origin, new ContractBasedImportDefinition(attributedImport.GetContractNameFromImport(importType), attributedImport.GetTypeIdentityFromImport(importType), CompositionServices.GetRequiredMetadata(importType.MetadataViewType), attributedImport.Cardinality, false, true, (attributedImport.RequiredCreationPolicy != CreationPolicy.NewScope) ? CreationPolicy.NonShared : CreationPolicy.NewScope, CompositionServices.GetImportMetadata(importType, attributedImport)));
			}
			if (attributedImport.RequiredCreationPolicy == CreationPolicy.NewScope)
			{
				throw new ComposablePartException(string.Format(CultureInfo.CurrentCulture, Strings.InvalidPartCreationPolicyOnImport, attributedImport.RequiredCreationPolicy), origin);
			}
			return new ReflectionParameterImportDefinition(new Lazy<ParameterInfo>(() => parameter), attributedImport.GetContractNameFromImport(importType), attributedImport.GetTypeIdentityFromImport(importType), CompositionServices.GetRequiredMetadata(importType.MetadataViewType), attributedImport.Cardinality, attributedImport.RequiredCreationPolicy, CompositionServices.GetImportMetadata(importType, attributedImport), origin);
		}

		// Token: 0x060006B4 RID: 1716 RVA: 0x00014144 File Offset: 0x00012344
		public static ReflectionMemberImportDefinition CreateMemberImportDefinition(MemberInfo member, ICompositionElement origin)
		{
			Requires.NotNull<MemberInfo>(member, "member");
			ReflectionWritableMember reflectionWritableMember = member.ToReflectionWritableMember();
			IAttributedImport attributedImport = AttributedModelDiscovery.GetAttributedImport(reflectionWritableMember, member);
			ImportType importType = new ImportType(reflectionWritableMember.ReturnType, attributedImport.Cardinality);
			if (importType.IsPartCreator)
			{
				return new PartCreatorMemberImportDefinition(new LazyMemberInfo(member), origin, new ContractBasedImportDefinition(attributedImport.GetContractNameFromImport(importType), attributedImport.GetTypeIdentityFromImport(importType), CompositionServices.GetRequiredMetadata(importType.MetadataViewType), attributedImport.Cardinality, attributedImport.AllowRecomposition, false, (attributedImport.RequiredCreationPolicy != CreationPolicy.NewScope) ? CreationPolicy.NonShared : CreationPolicy.NewScope, CompositionServices.GetImportMetadata(importType, attributedImport)));
			}
			if (attributedImport.RequiredCreationPolicy == CreationPolicy.NewScope)
			{
				throw new ComposablePartException(string.Format(CultureInfo.CurrentCulture, Strings.InvalidPartCreationPolicyOnImport, attributedImport.RequiredCreationPolicy), origin);
			}
			bool isPrerequisite = member.GetAttributes<ExportAttribute>().Length != 0;
			return new ReflectionMemberImportDefinition(new LazyMemberInfo(member), attributedImport.GetContractNameFromImport(importType), attributedImport.GetTypeIdentityFromImport(importType), CompositionServices.GetRequiredMetadata(importType.MetadataViewType), attributedImport.Cardinality, attributedImport.AllowRecomposition, isPrerequisite, attributedImport.RequiredCreationPolicy, CompositionServices.GetImportMetadata(importType, attributedImport), origin);
		}

		// Token: 0x060006B5 RID: 1717 RVA: 0x00014244 File Offset: 0x00012444
		private static IAttributedImport GetAttributedImport(ReflectionItem item, ICustomAttributeProvider attributeProvider)
		{
			IAttributedImport[] attributes = attributeProvider.GetAttributes(false);
			if (attributes.Length == 0)
			{
				return new ImportAttribute();
			}
			if (attributes.Length > 1)
			{
				CompositionTrace.MemberMarkedWithMultipleImportAndImportMany(item);
			}
			return attributes[0];
		}

		// Token: 0x02000102 RID: 258
		[CompilerGenerated]
		private sealed class <>c__DisplayClass5_0
		{
			// Token: 0x060006B6 RID: 1718 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass5_0()
			{
			}

			// Token: 0x060006B7 RID: 1719 RVA: 0x00014272 File Offset: 0x00012472
			internal ParameterInfo <CreateParameterImportDefinition>b__0()
			{
				return this.parameter;
			}

			// Token: 0x060006B8 RID: 1720 RVA: 0x00014272 File Offset: 0x00012472
			internal ParameterInfo <CreateParameterImportDefinition>b__1()
			{
				return this.parameter;
			}

			// Token: 0x040002CB RID: 715
			public ParameterInfo parameter;
		}
	}
}
