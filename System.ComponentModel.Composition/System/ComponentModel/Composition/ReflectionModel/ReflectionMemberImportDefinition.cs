﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.Globalization;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x0200007A RID: 122
	internal class ReflectionMemberImportDefinition : ReflectionImportDefinition
	{
		// Token: 0x0600032A RID: 810 RVA: 0x0000911C File Offset: 0x0000731C
		public ReflectionMemberImportDefinition(LazyMemberInfo importingLazyMember, string contractName, string requiredTypeIdentity, IEnumerable<KeyValuePair<string, Type>> requiredMetadata, ImportCardinality cardinality, bool isRecomposable, bool isPrerequisite, CreationPolicy requiredCreationPolicy, IDictionary<string, object> metadata, ICompositionElement origin) : base(contractName, requiredTypeIdentity, requiredMetadata, cardinality, isRecomposable, isPrerequisite, requiredCreationPolicy, metadata, origin)
		{
			Assumes.NotNull<string>(contractName);
			this._importingLazyMember = importingLazyMember;
		}

		// Token: 0x0600032B RID: 811 RVA: 0x0000914C File Offset: 0x0000734C
		public override ImportingItem ToImportingItem()
		{
			ReflectionWritableMember reflectionWritableMember = this.ImportingLazyMember.ToReflectionWriteableMember();
			return new ImportingMember(this, reflectionWritableMember, new ImportType(reflectionWritableMember.ReturnType, this.Cardinality));
		}

		// Token: 0x170000ED RID: 237
		// (get) Token: 0x0600032C RID: 812 RVA: 0x0000917D File Offset: 0x0000737D
		public LazyMemberInfo ImportingLazyMember
		{
			get
			{
				return this._importingLazyMember;
			}
		}

		// Token: 0x0600032D RID: 813 RVA: 0x00009185 File Offset: 0x00007385
		protected override string GetDisplayName()
		{
			return string.Format(CultureInfo.CurrentCulture, "{0} (ContractName=\"{1}\")", this.ImportingLazyMember.ToReflectionMember().GetDisplayName(), this.ContractName);
		}

		// Token: 0x04000137 RID: 311
		private LazyMemberInfo _importingLazyMember;
	}
}
