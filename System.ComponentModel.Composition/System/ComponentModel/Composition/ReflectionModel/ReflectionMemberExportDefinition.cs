﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.Globalization;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x02000079 RID: 121
	internal class ReflectionMemberExportDefinition : ExportDefinition, ICompositionElement
	{
		// Token: 0x0600031F RID: 799 RVA: 0x00009057 File Offset: 0x00007257
		public ReflectionMemberExportDefinition(LazyMemberInfo member, ExportDefinition exportDefinition, ICompositionElement origin)
		{
			Assumes.NotNull<ExportDefinition>(exportDefinition);
			this._member = member;
			this._exportDefinition = exportDefinition;
			this._origin = origin;
		}

		// Token: 0x170000E8 RID: 232
		// (get) Token: 0x06000320 RID: 800 RVA: 0x0000907A File Offset: 0x0000727A
		public override string ContractName
		{
			get
			{
				return this._exportDefinition.ContractName;
			}
		}

		// Token: 0x170000E9 RID: 233
		// (get) Token: 0x06000321 RID: 801 RVA: 0x00009087 File Offset: 0x00007287
		public LazyMemberInfo ExportingLazyMember
		{
			get
			{
				return this._member;
			}
		}

		// Token: 0x170000EA RID: 234
		// (get) Token: 0x06000322 RID: 802 RVA: 0x0000908F File Offset: 0x0000728F
		public override IDictionary<string, object> Metadata
		{
			get
			{
				if (this._metadata == null)
				{
					this._metadata = this._exportDefinition.Metadata.AsReadOnly();
				}
				return this._metadata;
			}
		}

		// Token: 0x170000EB RID: 235
		// (get) Token: 0x06000323 RID: 803 RVA: 0x000090B5 File Offset: 0x000072B5
		string ICompositionElement.DisplayName
		{
			get
			{
				return this.GetDisplayName();
			}
		}

		// Token: 0x170000EC RID: 236
		// (get) Token: 0x06000324 RID: 804 RVA: 0x000090BD File Offset: 0x000072BD
		ICompositionElement ICompositionElement.Origin
		{
			get
			{
				return this._origin;
			}
		}

		// Token: 0x06000325 RID: 805 RVA: 0x000090B5 File Offset: 0x000072B5
		public override string ToString()
		{
			return this.GetDisplayName();
		}

		// Token: 0x06000326 RID: 806 RVA: 0x000090C5 File Offset: 0x000072C5
		public int GetIndex()
		{
			return this.ExportingLazyMember.ToReflectionMember().UnderlyingMember.MetadataToken;
		}

		// Token: 0x06000327 RID: 807 RVA: 0x000090DC File Offset: 0x000072DC
		public ExportingMember ToExportingMember()
		{
			return new ExportingMember(this, this.ToReflectionMember());
		}

		// Token: 0x06000328 RID: 808 RVA: 0x000090EA File Offset: 0x000072EA
		private ReflectionMember ToReflectionMember()
		{
			return this.ExportingLazyMember.ToReflectionMember();
		}

		// Token: 0x06000329 RID: 809 RVA: 0x000090F7 File Offset: 0x000072F7
		private string GetDisplayName()
		{
			return string.Format(CultureInfo.CurrentCulture, "{0} (ContractName=\"{1}\")", this.ToReflectionMember().GetDisplayName(), this.ContractName);
		}

		// Token: 0x04000133 RID: 307
		private readonly LazyMemberInfo _member;

		// Token: 0x04000134 RID: 308
		private readonly ExportDefinition _exportDefinition;

		// Token: 0x04000135 RID: 309
		private readonly ICompositionElement _origin;

		// Token: 0x04000136 RID: 310
		private IDictionary<string, object> _metadata;
	}
}
