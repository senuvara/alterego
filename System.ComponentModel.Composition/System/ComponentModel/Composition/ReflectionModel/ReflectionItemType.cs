﻿using System;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x02000077 RID: 119
	internal enum ReflectionItemType
	{
		// Token: 0x0400012E RID: 302
		Parameter,
		// Token: 0x0400012F RID: 303
		Field,
		// Token: 0x04000130 RID: 304
		Property,
		// Token: 0x04000131 RID: 305
		Method,
		// Token: 0x04000132 RID: 306
		Type
	}
}
