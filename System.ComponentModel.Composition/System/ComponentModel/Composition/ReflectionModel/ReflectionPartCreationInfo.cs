﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x0200007D RID: 125
	internal class ReflectionPartCreationInfo : IReflectionPartCreationInfo, ICompositionElement
	{
		// Token: 0x06000347 RID: 839 RVA: 0x00009572 File Offset: 0x00007772
		public ReflectionPartCreationInfo(Lazy<Type> partType, bool isDisposalRequired, Lazy<IEnumerable<ImportDefinition>> imports, Lazy<IEnumerable<ExportDefinition>> exports, Lazy<IDictionary<string, object>> metadata, ICompositionElement origin)
		{
			Assumes.NotNull<Lazy<Type>>(partType);
			this._partType = partType;
			this._isDisposalRequired = isDisposalRequired;
			this._imports = imports;
			this._exports = exports;
			this._metadata = metadata;
			this._origin = origin;
		}

		// Token: 0x06000348 RID: 840 RVA: 0x000095AD File Offset: 0x000077AD
		public Type GetPartType()
		{
			return this._partType.GetNotNullValue("type");
		}

		// Token: 0x06000349 RID: 841 RVA: 0x000095BF File Offset: 0x000077BF
		public Lazy<Type> GetLazyPartType()
		{
			return this._partType;
		}

		// Token: 0x0600034A RID: 842 RVA: 0x000095C8 File Offset: 0x000077C8
		public ConstructorInfo GetConstructor()
		{
			if (this._constructor == null)
			{
				ConstructorInfo[] array = (from parameterImport in this.GetImports().OfType<ReflectionParameterImportDefinition>()
				select parameterImport.ImportingLazyParameter.Value.Member).OfType<ConstructorInfo>().Distinct<ConstructorInfo>().ToArray<ConstructorInfo>();
				if (array.Length == 1)
				{
					this._constructor = array[0];
				}
				else if (array.Length == 0)
				{
					this._constructor = this.GetPartType().GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, Type.EmptyTypes, null);
				}
			}
			return this._constructor;
		}

		// Token: 0x170000F4 RID: 244
		// (get) Token: 0x0600034B RID: 843 RVA: 0x00009659 File Offset: 0x00007859
		public bool IsDisposalRequired
		{
			get
			{
				return this._isDisposalRequired;
			}
		}

		// Token: 0x0600034C RID: 844 RVA: 0x00009661 File Offset: 0x00007861
		public IDictionary<string, object> GetMetadata()
		{
			if (this._metadata == null)
			{
				return null;
			}
			return this._metadata.Value;
		}

		// Token: 0x0600034D RID: 845 RVA: 0x00009678 File Offset: 0x00007878
		public IEnumerable<ExportDefinition> GetExports()
		{
			if (this._exports == null)
			{
				yield break;
			}
			IEnumerable<ExportDefinition> value = this._exports.Value;
			if (value == null)
			{
				yield break;
			}
			foreach (ExportDefinition exportDefinition in value)
			{
				ReflectionMemberExportDefinition reflectionMemberExportDefinition = exportDefinition as ReflectionMemberExportDefinition;
				if (reflectionMemberExportDefinition == null)
				{
					throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, Strings.ReflectionModel_InvalidExportDefinition, exportDefinition.GetType()));
				}
				yield return reflectionMemberExportDefinition;
			}
			IEnumerator<ExportDefinition> enumerator = null;
			yield break;
			yield break;
		}

		// Token: 0x0600034E RID: 846 RVA: 0x00009688 File Offset: 0x00007888
		public IEnumerable<ImportDefinition> GetImports()
		{
			if (this._imports == null)
			{
				yield break;
			}
			IEnumerable<ImportDefinition> value = this._imports.Value;
			if (value == null)
			{
				yield break;
			}
			foreach (ImportDefinition importDefinition in value)
			{
				ReflectionImportDefinition reflectionImportDefinition = importDefinition as ReflectionImportDefinition;
				if (reflectionImportDefinition == null)
				{
					throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, Strings.ReflectionModel_InvalidMemberImportDefinition, importDefinition.GetType()));
				}
				yield return reflectionImportDefinition;
			}
			IEnumerator<ImportDefinition> enumerator = null;
			yield break;
			yield break;
		}

		// Token: 0x170000F5 RID: 245
		// (get) Token: 0x0600034F RID: 847 RVA: 0x00009698 File Offset: 0x00007898
		public string DisplayName
		{
			get
			{
				return this.GetPartType().GetDisplayName();
			}
		}

		// Token: 0x170000F6 RID: 246
		// (get) Token: 0x06000350 RID: 848 RVA: 0x000096A5 File Offset: 0x000078A5
		public ICompositionElement Origin
		{
			get
			{
				return this._origin;
			}
		}

		// Token: 0x04000139 RID: 313
		private readonly Lazy<Type> _partType;

		// Token: 0x0400013A RID: 314
		private readonly Lazy<IEnumerable<ImportDefinition>> _imports;

		// Token: 0x0400013B RID: 315
		private readonly Lazy<IEnumerable<ExportDefinition>> _exports;

		// Token: 0x0400013C RID: 316
		private readonly Lazy<IDictionary<string, object>> _metadata;

		// Token: 0x0400013D RID: 317
		private readonly ICompositionElement _origin;

		// Token: 0x0400013E RID: 318
		private ConstructorInfo _constructor;

		// Token: 0x0400013F RID: 319
		private bool _isDisposalRequired;

		// Token: 0x0200007E RID: 126
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000351 RID: 849 RVA: 0x000096AD File Offset: 0x000078AD
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000352 RID: 850 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c()
			{
			}

			// Token: 0x06000353 RID: 851 RVA: 0x000096B9 File Offset: 0x000078B9
			internal MemberInfo <GetConstructor>b__10_0(ReflectionParameterImportDefinition parameterImport)
			{
				return parameterImport.ImportingLazyParameter.Value.Member;
			}

			// Token: 0x04000140 RID: 320
			public static readonly ReflectionPartCreationInfo.<>c <>9 = new ReflectionPartCreationInfo.<>c();

			// Token: 0x04000141 RID: 321
			public static Func<ReflectionParameterImportDefinition, MemberInfo> <>9__10_0;
		}

		// Token: 0x0200007F RID: 127
		[CompilerGenerated]
		private sealed class <GetExports>d__14 : IEnumerable<ExportDefinition>, IEnumerable, IEnumerator<ExportDefinition>, IDisposable, IEnumerator
		{
			// Token: 0x06000354 RID: 852 RVA: 0x000096CB File Offset: 0x000078CB
			[DebuggerHidden]
			public <GetExports>d__14(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000355 RID: 853 RVA: 0x000096E8 File Offset: 0x000078E8
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x06000356 RID: 854 RVA: 0x00009720 File Offset: 0x00007920
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					ReflectionPartCreationInfo reflectionPartCreationInfo = this;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						if (reflectionPartCreationInfo._exports == null)
						{
							return false;
						}
						IEnumerable<ExportDefinition> value = reflectionPartCreationInfo._exports.Value;
						if (value == null)
						{
							return false;
						}
						enumerator = value.GetEnumerator();
						this.<>1__state = -3;
					}
					if (!enumerator.MoveNext())
					{
						this.<>m__Finally1();
						enumerator = null;
						result = false;
					}
					else
					{
						ExportDefinition exportDefinition = enumerator.Current;
						ReflectionMemberExportDefinition reflectionMemberExportDefinition = exportDefinition as ReflectionMemberExportDefinition;
						if (reflectionMemberExportDefinition == null)
						{
							throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, Strings.ReflectionModel_InvalidExportDefinition, exportDefinition.GetType()));
						}
						this.<>2__current = reflectionMemberExportDefinition;
						this.<>1__state = 1;
						result = true;
					}
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x06000357 RID: 855 RVA: 0x00009818 File Offset: 0x00007A18
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x170000F7 RID: 247
			// (get) Token: 0x06000358 RID: 856 RVA: 0x00009834 File Offset: 0x00007A34
			ExportDefinition IEnumerator<ExportDefinition>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000359 RID: 857 RVA: 0x0000266F File Offset: 0x0000086F
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000F8 RID: 248
			// (get) Token: 0x0600035A RID: 858 RVA: 0x00009834 File Offset: 0x00007A34
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600035B RID: 859 RVA: 0x0000983C File Offset: 0x00007A3C
			[DebuggerHidden]
			IEnumerator<ExportDefinition> IEnumerable<ExportDefinition>.GetEnumerator()
			{
				ReflectionPartCreationInfo.<GetExports>d__14 <GetExports>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<GetExports>d__ = this;
				}
				else
				{
					<GetExports>d__ = new ReflectionPartCreationInfo.<GetExports>d__14(0);
					<GetExports>d__.<>4__this = this;
				}
				return <GetExports>d__;
			}

			// Token: 0x0600035C RID: 860 RVA: 0x0000987F File Offset: 0x00007A7F
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.ComponentModel.Composition.Primitives.ExportDefinition>.GetEnumerator();
			}

			// Token: 0x04000142 RID: 322
			private int <>1__state;

			// Token: 0x04000143 RID: 323
			private ExportDefinition <>2__current;

			// Token: 0x04000144 RID: 324
			private int <>l__initialThreadId;

			// Token: 0x04000145 RID: 325
			public ReflectionPartCreationInfo <>4__this;

			// Token: 0x04000146 RID: 326
			private IEnumerator<ExportDefinition> <>7__wrap1;
		}

		// Token: 0x02000080 RID: 128
		[CompilerGenerated]
		private sealed class <GetImports>d__15 : IEnumerable<ImportDefinition>, IEnumerable, IEnumerator<ImportDefinition>, IDisposable, IEnumerator
		{
			// Token: 0x0600035D RID: 861 RVA: 0x00009887 File Offset: 0x00007A87
			[DebuggerHidden]
			public <GetImports>d__15(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x0600035E RID: 862 RVA: 0x000098A4 File Offset: 0x00007AA4
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x0600035F RID: 863 RVA: 0x000098DC File Offset: 0x00007ADC
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					ReflectionPartCreationInfo reflectionPartCreationInfo = this;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						if (reflectionPartCreationInfo._imports == null)
						{
							return false;
						}
						IEnumerable<ImportDefinition> value = reflectionPartCreationInfo._imports.Value;
						if (value == null)
						{
							return false;
						}
						enumerator = value.GetEnumerator();
						this.<>1__state = -3;
					}
					if (!enumerator.MoveNext())
					{
						this.<>m__Finally1();
						enumerator = null;
						result = false;
					}
					else
					{
						ImportDefinition importDefinition = enumerator.Current;
						ReflectionImportDefinition reflectionImportDefinition = importDefinition as ReflectionImportDefinition;
						if (reflectionImportDefinition == null)
						{
							throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, Strings.ReflectionModel_InvalidMemberImportDefinition, importDefinition.GetType()));
						}
						this.<>2__current = reflectionImportDefinition;
						this.<>1__state = 1;
						result = true;
					}
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x06000360 RID: 864 RVA: 0x000099D4 File Offset: 0x00007BD4
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x170000F9 RID: 249
			// (get) Token: 0x06000361 RID: 865 RVA: 0x000099F0 File Offset: 0x00007BF0
			ImportDefinition IEnumerator<ImportDefinition>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000362 RID: 866 RVA: 0x0000266F File Offset: 0x0000086F
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000FA RID: 250
			// (get) Token: 0x06000363 RID: 867 RVA: 0x000099F0 File Offset: 0x00007BF0
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000364 RID: 868 RVA: 0x000099F8 File Offset: 0x00007BF8
			[DebuggerHidden]
			IEnumerator<ImportDefinition> IEnumerable<ImportDefinition>.GetEnumerator()
			{
				ReflectionPartCreationInfo.<GetImports>d__15 <GetImports>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<GetImports>d__ = this;
				}
				else
				{
					<GetImports>d__ = new ReflectionPartCreationInfo.<GetImports>d__15(0);
					<GetImports>d__.<>4__this = this;
				}
				return <GetImports>d__;
			}

			// Token: 0x06000365 RID: 869 RVA: 0x00009A3B File Offset: 0x00007C3B
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.ComponentModel.Composition.Primitives.ImportDefinition>.GetEnumerator();
			}

			// Token: 0x04000147 RID: 327
			private int <>1__state;

			// Token: 0x04000148 RID: 328
			private ImportDefinition <>2__current;

			// Token: 0x04000149 RID: 329
			private int <>l__initialThreadId;

			// Token: 0x0400014A RID: 330
			public ReflectionPartCreationInfo <>4__this;

			// Token: 0x0400014B RID: 331
			private IEnumerator<ImportDefinition> <>7__wrap1;
		}
	}
}
