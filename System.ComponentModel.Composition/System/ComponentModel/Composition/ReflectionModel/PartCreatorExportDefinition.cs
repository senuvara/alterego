﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x0200006B RID: 107
	internal class PartCreatorExportDefinition : ExportDefinition
	{
		// Token: 0x060002A5 RID: 677 RVA: 0x00007BEF File Offset: 0x00005DEF
		public PartCreatorExportDefinition(ExportDefinition productDefinition)
		{
			this._productDefinition = productDefinition;
		}

		// Token: 0x170000C2 RID: 194
		// (get) Token: 0x060002A6 RID: 678 RVA: 0x00007BFE File Offset: 0x00005DFE
		public override string ContractName
		{
			get
			{
				return "System.ComponentModel.Composition.Contracts.ExportFactory";
			}
		}

		// Token: 0x170000C3 RID: 195
		// (get) Token: 0x060002A7 RID: 679 RVA: 0x00007C08 File Offset: 0x00005E08
		public override IDictionary<string, object> Metadata
		{
			get
			{
				if (this._metadata == null)
				{
					Dictionary<string, object> dictionary = new Dictionary<string, object>(this._productDefinition.Metadata);
					dictionary["ExportTypeIdentity"] = CompositionConstants.PartCreatorTypeIdentity;
					dictionary["ProductDefinition"] = this._productDefinition;
					this._metadata = dictionary.AsReadOnly();
				}
				return this._metadata;
			}
		}

		// Token: 0x060002A8 RID: 680 RVA: 0x00007C64 File Offset: 0x00005E64
		internal static bool IsProductConstraintSatisfiedBy(ImportDefinition productImportDefinition, ExportDefinition exportDefinition)
		{
			object obj = null;
			if (exportDefinition.Metadata.TryGetValue("ProductDefinition", out obj))
			{
				ExportDefinition exportDefinition2 = obj as ExportDefinition;
				if (exportDefinition2 != null)
				{
					return productImportDefinition.IsConstraintSatisfiedBy(exportDefinition2);
				}
			}
			return false;
		}

		// Token: 0x0400010B RID: 267
		private readonly ExportDefinition _productDefinition;

		// Token: 0x0400010C RID: 268
		private IDictionary<string, object> _metadata;
	}
}
