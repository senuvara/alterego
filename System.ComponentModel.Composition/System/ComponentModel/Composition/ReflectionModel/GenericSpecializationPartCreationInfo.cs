﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.Globalization;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using Microsoft.Internal;
using Microsoft.Internal.Collections;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x0200005F RID: 95
	internal class GenericSpecializationPartCreationInfo : IReflectionPartCreationInfo, ICompositionElement
	{
		// Token: 0x06000246 RID: 582 RVA: 0x00006444 File Offset: 0x00004644
		public GenericSpecializationPartCreationInfo(IReflectionPartCreationInfo originalPartCreationInfo, ReflectionComposablePartDefinition originalPart, Type[] specialization)
		{
			GenericSpecializationPartCreationInfo <>4__this = this;
			Assumes.NotNull<IReflectionPartCreationInfo>(originalPartCreationInfo);
			Assumes.NotNull<Type[]>(specialization);
			Assumes.NotNull<ReflectionComposablePartDefinition>(originalPart);
			this._originalPartCreationInfo = originalPartCreationInfo;
			this._originalPart = originalPart;
			this._specialization = specialization;
			this._specializationIdentities = new string[this._specialization.Length];
			for (int i = 0; i < this._specialization.Length; i++)
			{
				this._specializationIdentities[i] = AttributedModelServices.GetTypeIdentity(this._specialization[i]);
			}
			this._lazyPartType = new Lazy<Type>(() => <>4__this._originalPartCreationInfo.GetPartType().MakeGenericType(specialization), LazyThreadSafetyMode.PublicationOnly);
		}

		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x06000247 RID: 583 RVA: 0x000064FA File Offset: 0x000046FA
		public ReflectionComposablePartDefinition OriginalPart
		{
			get
			{
				return this._originalPart;
			}
		}

		// Token: 0x06000248 RID: 584 RVA: 0x00006502 File Offset: 0x00004702
		public Type GetPartType()
		{
			return this._lazyPartType.Value;
		}

		// Token: 0x06000249 RID: 585 RVA: 0x0000650F File Offset: 0x0000470F
		public Lazy<Type> GetLazyPartType()
		{
			return this._lazyPartType;
		}

		// Token: 0x0600024A RID: 586 RVA: 0x00006518 File Offset: 0x00004718
		public ConstructorInfo GetConstructor()
		{
			if (this._constructor == null)
			{
				ConstructorInfo constructor = this._originalPartCreationInfo.GetConstructor();
				ConstructorInfo constructor2 = null;
				if (constructor != null)
				{
					foreach (ConstructorInfo constructorInfo in this.GetPartType().GetConstructors(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
					{
						if (constructorInfo.MetadataToken == constructor.MetadataToken)
						{
							constructor2 = constructorInfo;
							break;
						}
					}
				}
				Thread.MemoryBarrier();
				object @lock = this._lock;
				lock (@lock)
				{
					if (this._constructor == null)
					{
						this._constructor = constructor2;
					}
				}
			}
			return this._constructor;
		}

		// Token: 0x0600024B RID: 587 RVA: 0x000065D4 File Offset: 0x000047D4
		public IDictionary<string, object> GetMetadata()
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>(this._originalPartCreationInfo.GetMetadata(), StringComparers.MetadataKeyNames);
			dictionary.Remove("System.ComponentModel.Composition.IsGenericPart");
			dictionary.Remove("System.ComponentModel.Composition.GenericPartArity");
			dictionary.Remove("System.ComponentModel.Composition.GenericParameterConstraints");
			dictionary.Remove("System.ComponentModel.Composition.GenericParameterAttributes");
			return dictionary;
		}

		// Token: 0x0600024C RID: 588 RVA: 0x00006626 File Offset: 0x00004826
		private MemberInfo[] GetAccessors(LazyMemberInfo originalLazyMember)
		{
			this.BuildTables();
			Assumes.NotNull<Dictionary<LazyMemberInfo, MemberInfo[]>>(this._membersTable);
			return this._membersTable[originalLazyMember];
		}

		// Token: 0x0600024D RID: 589 RVA: 0x00006645 File Offset: 0x00004845
		private ParameterInfo GetParameter(Lazy<ParameterInfo> originalParameter)
		{
			this.BuildTables();
			Assumes.NotNull<Dictionary<Lazy<ParameterInfo>, ParameterInfo>>(this._parametersTable);
			return this._parametersTable[originalParameter];
		}

		// Token: 0x0600024E RID: 590 RVA: 0x00006664 File Offset: 0x00004864
		private void BuildTables()
		{
			if (this._membersTable != null)
			{
				return;
			}
			this.PopulateImportsAndExports();
			List<LazyMemberInfo> list = null;
			List<Lazy<ParameterInfo>> parameters = null;
			object @lock = this._lock;
			lock (@lock)
			{
				if (this._membersTable == null)
				{
					list = this._members;
					parameters = this._parameters;
					Assumes.NotNull<List<LazyMemberInfo>>(list);
				}
			}
			Dictionary<LazyMemberInfo, MemberInfo[]> membersTable = this.BuildMembersTable(list);
			Dictionary<Lazy<ParameterInfo>, ParameterInfo> parametersTable = this.BuildParametersTable(parameters);
			@lock = this._lock;
			lock (@lock)
			{
				if (this._membersTable == null)
				{
					this._membersTable = membersTable;
					this._parametersTable = parametersTable;
					Thread.MemoryBarrier();
					this._parameters = null;
					this._members = null;
				}
			}
		}

		// Token: 0x0600024F RID: 591 RVA: 0x0000673C File Offset: 0x0000493C
		private Dictionary<LazyMemberInfo, MemberInfo[]> BuildMembersTable(List<LazyMemberInfo> members)
		{
			Assumes.NotNull<List<LazyMemberInfo>>(members);
			Dictionary<LazyMemberInfo, MemberInfo[]> dictionary = new Dictionary<LazyMemberInfo, MemberInfo[]>();
			Dictionary<int, MemberInfo> dictionary2 = new Dictionary<int, MemberInfo>();
			Type partType = this.GetPartType();
			dictionary2[partType.MetadataToken] = partType;
			foreach (MethodInfo methodInfo in partType.GetAllMethods())
			{
				dictionary2[methodInfo.MetadataToken] = methodInfo;
			}
			foreach (FieldInfo fieldInfo in partType.GetAllFields())
			{
				dictionary2[fieldInfo.MetadataToken] = fieldInfo;
			}
			foreach (LazyMemberInfo key in members)
			{
				MemberInfo[] accessors = key.GetAccessors();
				MemberInfo[] array = new MemberInfo[accessors.Length];
				for (int i = 0; i < accessors.Length; i++)
				{
					array[i] = ((accessors[i] != null) ? dictionary2[accessors[i].MetadataToken] : null);
				}
				dictionary[key] = array;
			}
			return dictionary;
		}

		// Token: 0x06000250 RID: 592 RVA: 0x00006894 File Offset: 0x00004A94
		private Dictionary<Lazy<ParameterInfo>, ParameterInfo> BuildParametersTable(List<Lazy<ParameterInfo>> parameters)
		{
			if (parameters != null)
			{
				Dictionary<Lazy<ParameterInfo>, ParameterInfo> dictionary = new Dictionary<Lazy<ParameterInfo>, ParameterInfo>();
				ParameterInfo[] parameters2 = this.GetConstructor().GetParameters();
				foreach (Lazy<ParameterInfo> lazy in parameters)
				{
					dictionary[lazy] = parameters2[lazy.Value.Position];
				}
				return dictionary;
			}
			return null;
		}

		// Token: 0x06000251 RID: 593 RVA: 0x00006908 File Offset: 0x00004B08
		private List<ImportDefinition> PopulateImports(List<LazyMemberInfo> members, List<Lazy<ParameterInfo>> parameters)
		{
			List<ImportDefinition> list = new List<ImportDefinition>();
			foreach (ImportDefinition importDefinition in this._originalPartCreationInfo.GetImports())
			{
				ReflectionImportDefinition reflectionImportDefinition = importDefinition as ReflectionImportDefinition;
				if (reflectionImportDefinition != null)
				{
					list.Add(this.TranslateImport(reflectionImportDefinition, members, parameters));
				}
			}
			return list;
		}

		// Token: 0x06000252 RID: 594 RVA: 0x00006974 File Offset: 0x00004B74
		private ImportDefinition TranslateImport(ReflectionImportDefinition reflectionImport, List<LazyMemberInfo> members, List<Lazy<ParameterInfo>> parameters)
		{
			bool flag = false;
			ContractBasedImportDefinition contractBasedImportDefinition = reflectionImport;
			IPartCreatorImportDefinition partCreatorImportDefinition = reflectionImport as IPartCreatorImportDefinition;
			if (partCreatorImportDefinition != null)
			{
				contractBasedImportDefinition = partCreatorImportDefinition.ProductImportDefinition;
				flag = true;
			}
			string contractName = this.Translate(contractBasedImportDefinition.ContractName);
			string requiredTypeIdentity = this.Translate(contractBasedImportDefinition.RequiredTypeIdentity);
			IDictionary<string, object> metadata = this.TranslateImportMetadata(contractBasedImportDefinition);
			ReflectionMemberImportDefinition reflectionMemberImportDefinition = reflectionImport as ReflectionMemberImportDefinition;
			ImportDefinition result;
			if (reflectionMemberImportDefinition != null)
			{
				LazyMemberInfo lazyMember = reflectionMemberImportDefinition.ImportingLazyMember;
				LazyMemberInfo importingLazyMember = new LazyMemberInfo(lazyMember.MemberType, () => this.GetAccessors(lazyMember));
				if (flag)
				{
					result = new PartCreatorMemberImportDefinition(importingLazyMember, ((ICompositionElement)reflectionMemberImportDefinition).Origin, new ContractBasedImportDefinition(contractName, requiredTypeIdentity, contractBasedImportDefinition.RequiredMetadata, contractBasedImportDefinition.Cardinality, contractBasedImportDefinition.IsRecomposable, false, CreationPolicy.NonShared, metadata));
				}
				else
				{
					result = new ReflectionMemberImportDefinition(importingLazyMember, contractName, requiredTypeIdentity, contractBasedImportDefinition.RequiredMetadata, contractBasedImportDefinition.Cardinality, contractBasedImportDefinition.IsRecomposable, false, contractBasedImportDefinition.RequiredCreationPolicy, metadata, ((ICompositionElement)reflectionMemberImportDefinition).Origin);
				}
				members.Add(lazyMember);
			}
			else
			{
				ReflectionParameterImportDefinition reflectionParameterImportDefinition = reflectionImport as ReflectionParameterImportDefinition;
				Assumes.NotNull<ReflectionParameterImportDefinition>(reflectionParameterImportDefinition);
				Lazy<ParameterInfo> lazyParameter = reflectionParameterImportDefinition.ImportingLazyParameter;
				Lazy<ParameterInfo> importingLazyParameter = new Lazy<ParameterInfo>(() => this.GetParameter(lazyParameter));
				if (flag)
				{
					result = new PartCreatorParameterImportDefinition(importingLazyParameter, ((ICompositionElement)reflectionParameterImportDefinition).Origin, new ContractBasedImportDefinition(contractName, requiredTypeIdentity, contractBasedImportDefinition.RequiredMetadata, contractBasedImportDefinition.Cardinality, false, true, CreationPolicy.NonShared, metadata));
				}
				else
				{
					result = new ReflectionParameterImportDefinition(importingLazyParameter, contractName, requiredTypeIdentity, contractBasedImportDefinition.RequiredMetadata, contractBasedImportDefinition.Cardinality, contractBasedImportDefinition.RequiredCreationPolicy, metadata, ((ICompositionElement)reflectionParameterImportDefinition).Origin);
				}
				parameters.Add(lazyParameter);
			}
			return result;
		}

		// Token: 0x06000253 RID: 595 RVA: 0x00006B24 File Offset: 0x00004D24
		private List<ExportDefinition> PopulateExports(List<LazyMemberInfo> members)
		{
			List<ExportDefinition> list = new List<ExportDefinition>();
			foreach (ExportDefinition exportDefinition in this._originalPartCreationInfo.GetExports())
			{
				ReflectionMemberExportDefinition reflectionMemberExportDefinition = exportDefinition as ReflectionMemberExportDefinition;
				if (reflectionMemberExportDefinition != null)
				{
					list.Add(this.TranslateExpot(reflectionMemberExportDefinition, members));
				}
			}
			return list;
		}

		// Token: 0x06000254 RID: 596 RVA: 0x00006B8C File Offset: 0x00004D8C
		public ExportDefinition TranslateExpot(ReflectionMemberExportDefinition reflectionExport, List<LazyMemberInfo> members)
		{
			LazyMemberInfo exportingLazyMember = reflectionExport.ExportingLazyMember;
			LazyMemberInfo capturedLazyMember = exportingLazyMember;
			ReflectionMemberExportDefinition capturedReflectionExport = reflectionExport;
			string contractName = this.Translate(reflectionExport.ContractName, reflectionExport.Metadata.GetValue("System.ComponentModel.Composition.GenericExportParametersOrderMetadataName"));
			LazyMemberInfo member = new LazyMemberInfo(capturedLazyMember.MemberType, () => this.GetAccessors(capturedLazyMember));
			Lazy<IDictionary<string, object>> metadata = new Lazy<IDictionary<string, object>>(() => this.TranslateExportMetadata(capturedReflectionExport));
			ExportDefinition result = new ReflectionMemberExportDefinition(member, new LazyExportDefinition(contractName, metadata), ((ICompositionElement)reflectionExport).Origin);
			members.Add(capturedLazyMember);
			return result;
		}

		// Token: 0x06000255 RID: 597 RVA: 0x00006C24 File Offset: 0x00004E24
		private string Translate(string originalValue, int[] genericParametersOrder)
		{
			if (genericParametersOrder != null)
			{
				string[] args = GenericServices.Reorder<string>(this._specializationIdentities, genericParametersOrder);
				return string.Format(CultureInfo.InvariantCulture, originalValue, args);
			}
			return this.Translate(originalValue);
		}

		// Token: 0x06000256 RID: 598 RVA: 0x00006C55 File Offset: 0x00004E55
		private string Translate(string originalValue)
		{
			return string.Format(CultureInfo.InvariantCulture, originalValue, this._specializationIdentities);
		}

		// Token: 0x06000257 RID: 599 RVA: 0x00006C68 File Offset: 0x00004E68
		private IDictionary<string, object> TranslateImportMetadata(ContractBasedImportDefinition originalImport)
		{
			int[] value = originalImport.Metadata.GetValue("System.ComponentModel.Composition.GenericImportParametersOrderMetadataName");
			if (value != null)
			{
				Dictionary<string, object> dictionary = new Dictionary<string, object>(originalImport.Metadata, StringComparers.MetadataKeyNames);
				dictionary["System.ComponentModel.Composition.GenericContractName"] = GenericServices.GetGenericName(originalImport.ContractName, value, this._specialization.Length);
				dictionary["System.ComponentModel.Composition.GenericParameters"] = GenericServices.Reorder<Type>(this._specialization, value);
				dictionary.Remove("System.ComponentModel.Composition.GenericImportParametersOrderMetadataName");
				return dictionary.AsReadOnly();
			}
			return originalImport.Metadata;
		}

		// Token: 0x06000258 RID: 600 RVA: 0x00006CE8 File Offset: 0x00004EE8
		private IDictionary<string, object> TranslateExportMetadata(ReflectionMemberExportDefinition originalExport)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>(originalExport.Metadata, StringComparers.MetadataKeyNames);
			string value = originalExport.Metadata.GetValue("ExportTypeIdentity");
			if (!string.IsNullOrEmpty(value))
			{
				dictionary["ExportTypeIdentity"] = this.Translate(value, originalExport.Metadata.GetValue("System.ComponentModel.Composition.GenericExportParametersOrderMetadataName"));
			}
			dictionary.Remove("System.ComponentModel.Composition.GenericExportParametersOrderMetadataName");
			return dictionary;
		}

		// Token: 0x06000259 RID: 601 RVA: 0x00006D50 File Offset: 0x00004F50
		private void PopulateImportsAndExports()
		{
			if (this._exports == null || this._imports == null)
			{
				List<LazyMemberInfo> members = new List<LazyMemberInfo>();
				List<Lazy<ParameterInfo>> list = new List<Lazy<ParameterInfo>>();
				List<ExportDefinition> exports = this.PopulateExports(members);
				List<ImportDefinition> imports = this.PopulateImports(members, list);
				Thread.MemoryBarrier();
				object @lock = this._lock;
				lock (@lock)
				{
					if (this._exports == null || this._imports == null)
					{
						this._members = members;
						if (list.Count > 0)
						{
							this._parameters = list;
						}
						this._exports = exports;
						this._imports = imports;
					}
				}
			}
		}

		// Token: 0x0600025A RID: 602 RVA: 0x00006DF8 File Offset: 0x00004FF8
		public IEnumerable<ExportDefinition> GetExports()
		{
			this.PopulateImportsAndExports();
			return this._exports;
		}

		// Token: 0x0600025B RID: 603 RVA: 0x00006E06 File Offset: 0x00005006
		public IEnumerable<ImportDefinition> GetImports()
		{
			this.PopulateImportsAndExports();
			return this._imports;
		}

		// Token: 0x170000B4 RID: 180
		// (get) Token: 0x0600025C RID: 604 RVA: 0x00006E14 File Offset: 0x00005014
		public bool IsDisposalRequired
		{
			get
			{
				return this._originalPartCreationInfo.IsDisposalRequired;
			}
		}

		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x0600025D RID: 605 RVA: 0x00006E21 File Offset: 0x00005021
		public string DisplayName
		{
			get
			{
				return this.Translate(this._originalPartCreationInfo.DisplayName);
			}
		}

		// Token: 0x170000B6 RID: 182
		// (get) Token: 0x0600025E RID: 606 RVA: 0x00006E34 File Offset: 0x00005034
		public ICompositionElement Origin
		{
			get
			{
				return this._originalPartCreationInfo.Origin;
			}
		}

		// Token: 0x0600025F RID: 607 RVA: 0x00006E44 File Offset: 0x00005044
		public override bool Equals(object obj)
		{
			GenericSpecializationPartCreationInfo genericSpecializationPartCreationInfo = obj as GenericSpecializationPartCreationInfo;
			return genericSpecializationPartCreationInfo != null && this._originalPartCreationInfo.Equals(genericSpecializationPartCreationInfo._originalPartCreationInfo) && this._specialization.IsArrayEqual(genericSpecializationPartCreationInfo._specialization);
		}

		// Token: 0x06000260 RID: 608 RVA: 0x00006E83 File Offset: 0x00005083
		public override int GetHashCode()
		{
			return this._originalPartCreationInfo.GetHashCode();
		}

		// Token: 0x06000261 RID: 609 RVA: 0x00006E90 File Offset: 0x00005090
		public static bool CanSpecialize(IDictionary<string, object> partMetadata, Type[] specialization)
		{
			int value = partMetadata.GetValue("System.ComponentModel.Composition.GenericPartArity");
			if (value != specialization.Length)
			{
				return false;
			}
			object[] value2 = partMetadata.GetValue("System.ComponentModel.Composition.GenericParameterConstraints");
			GenericParameterAttributes[] value3 = partMetadata.GetValue("System.ComponentModel.Composition.GenericParameterAttributes");
			if (value2 == null && value3 == null)
			{
				return true;
			}
			if (value2 != null && value2.Length != value)
			{
				return false;
			}
			if (value3 != null && value3.Length != value)
			{
				return false;
			}
			for (int i = 0; i < value; i++)
			{
				if (!GenericServices.CanSpecialize(specialization[i], (value2[i] as Type[]).CreateTypeSpecializations(specialization), value3[i]))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x040000DF RID: 223
		private readonly IReflectionPartCreationInfo _originalPartCreationInfo;

		// Token: 0x040000E0 RID: 224
		private readonly ReflectionComposablePartDefinition _originalPart;

		// Token: 0x040000E1 RID: 225
		private readonly Type[] _specialization;

		// Token: 0x040000E2 RID: 226
		private readonly string[] _specializationIdentities;

		// Token: 0x040000E3 RID: 227
		private IEnumerable<ExportDefinition> _exports;

		// Token: 0x040000E4 RID: 228
		private IEnumerable<ImportDefinition> _imports;

		// Token: 0x040000E5 RID: 229
		private readonly Lazy<Type> _lazyPartType;

		// Token: 0x040000E6 RID: 230
		private List<LazyMemberInfo> _members;

		// Token: 0x040000E7 RID: 231
		private List<Lazy<ParameterInfo>> _parameters;

		// Token: 0x040000E8 RID: 232
		private Dictionary<LazyMemberInfo, MemberInfo[]> _membersTable;

		// Token: 0x040000E9 RID: 233
		private Dictionary<Lazy<ParameterInfo>, ParameterInfo> _parametersTable;

		// Token: 0x040000EA RID: 234
		private ConstructorInfo _constructor;

		// Token: 0x040000EB RID: 235
		private object _lock = new object();

		// Token: 0x02000060 RID: 96
		[CompilerGenerated]
		private sealed class <>c__DisplayClass13_0
		{
			// Token: 0x06000262 RID: 610 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass13_0()
			{
			}

			// Token: 0x06000263 RID: 611 RVA: 0x00006F11 File Offset: 0x00005111
			internal Type <.ctor>b__0()
			{
				return this.<>4__this._originalPartCreationInfo.GetPartType().MakeGenericType(this.specialization);
			}

			// Token: 0x040000EC RID: 236
			public GenericSpecializationPartCreationInfo <>4__this;

			// Token: 0x040000ED RID: 237
			public Type[] specialization;
		}

		// Token: 0x02000061 RID: 97
		[CompilerGenerated]
		private sealed class <>c__DisplayClass26_0
		{
			// Token: 0x06000264 RID: 612 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass26_0()
			{
			}

			// Token: 0x06000265 RID: 613 RVA: 0x00006F2E File Offset: 0x0000512E
			internal MemberInfo[] <TranslateImport>b__0()
			{
				return this.<>4__this.GetAccessors(this.lazyMember);
			}

			// Token: 0x040000EE RID: 238
			public LazyMemberInfo lazyMember;

			// Token: 0x040000EF RID: 239
			public GenericSpecializationPartCreationInfo <>4__this;
		}

		// Token: 0x02000062 RID: 98
		[CompilerGenerated]
		private sealed class <>c__DisplayClass26_1
		{
			// Token: 0x06000266 RID: 614 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass26_1()
			{
			}

			// Token: 0x06000267 RID: 615 RVA: 0x00006F41 File Offset: 0x00005141
			internal ParameterInfo <TranslateImport>b__1()
			{
				return this.<>4__this.GetParameter(this.lazyParameter);
			}

			// Token: 0x040000F0 RID: 240
			public Lazy<ParameterInfo> lazyParameter;

			// Token: 0x040000F1 RID: 241
			public GenericSpecializationPartCreationInfo <>4__this;
		}

		// Token: 0x02000063 RID: 99
		[CompilerGenerated]
		private sealed class <>c__DisplayClass28_0
		{
			// Token: 0x06000268 RID: 616 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass28_0()
			{
			}

			// Token: 0x06000269 RID: 617 RVA: 0x00006F54 File Offset: 0x00005154
			internal MemberInfo[] <TranslateExpot>b__0()
			{
				return this.<>4__this.GetAccessors(this.capturedLazyMember);
			}

			// Token: 0x0600026A RID: 618 RVA: 0x00006F67 File Offset: 0x00005167
			internal IDictionary<string, object> <TranslateExpot>b__1()
			{
				return this.<>4__this.TranslateExportMetadata(this.capturedReflectionExport);
			}

			// Token: 0x040000F2 RID: 242
			public GenericSpecializationPartCreationInfo <>4__this;

			// Token: 0x040000F3 RID: 243
			public LazyMemberInfo capturedLazyMember;

			// Token: 0x040000F4 RID: 244
			public ReflectionMemberExportDefinition capturedReflectionExport;
		}
	}
}
