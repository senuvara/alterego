﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x02000081 RID: 129
	internal class LazyExportDefinition : ExportDefinition
	{
		// Token: 0x06000366 RID: 870 RVA: 0x00009A43 File Offset: 0x00007C43
		public LazyExportDefinition(string contractName, Lazy<IDictionary<string, object>> metadata) : base(contractName, null)
		{
			this._metadata = metadata;
		}

		// Token: 0x170000FB RID: 251
		// (get) Token: 0x06000367 RID: 871 RVA: 0x00009A54 File Offset: 0x00007C54
		public override IDictionary<string, object> Metadata
		{
			get
			{
				return this._metadata.Value ?? MetadataServices.EmptyMetadata;
			}
		}

		// Token: 0x0400014C RID: 332
		private readonly Lazy<IDictionary<string, object>> _metadata;
	}
}
