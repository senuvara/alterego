﻿using System;
using System.Reflection;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x02000078 RID: 120
	internal abstract class ReflectionMember : ReflectionItem
	{
		// Token: 0x170000E3 RID: 227
		// (get) Token: 0x06000317 RID: 791
		public abstract bool CanRead { get; }

		// Token: 0x170000E4 RID: 228
		// (get) Token: 0x06000318 RID: 792 RVA: 0x00009028 File Offset: 0x00007228
		public Type DeclaringType
		{
			get
			{
				return this.UnderlyingMember.DeclaringType;
			}
		}

		// Token: 0x170000E5 RID: 229
		// (get) Token: 0x06000319 RID: 793 RVA: 0x00009035 File Offset: 0x00007235
		public override string Name
		{
			get
			{
				return this.UnderlyingMember.Name;
			}
		}

		// Token: 0x0600031A RID: 794 RVA: 0x00009042 File Offset: 0x00007242
		public override string GetDisplayName()
		{
			return this.UnderlyingMember.GetDisplayName();
		}

		// Token: 0x170000E6 RID: 230
		// (get) Token: 0x0600031B RID: 795
		public abstract bool RequiresInstance { get; }

		// Token: 0x170000E7 RID: 231
		// (get) Token: 0x0600031C RID: 796
		public abstract MemberInfo UnderlyingMember { get; }

		// Token: 0x0600031D RID: 797
		public abstract object GetValue(object instance);

		// Token: 0x0600031E RID: 798 RVA: 0x0000904F File Offset: 0x0000724F
		protected ReflectionMember()
		{
		}
	}
}
