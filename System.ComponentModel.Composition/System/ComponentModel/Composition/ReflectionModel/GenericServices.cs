﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x0200005A RID: 90
	internal static class GenericServices
	{
		// Token: 0x06000232 RID: 562 RVA: 0x000060DC File Offset: 0x000042DC
		internal static IList<Type> GetPureGenericParameters(this Type type)
		{
			Assumes.NotNull<Type>(type);
			if (type.IsGenericType && type.ContainsGenericParameters)
			{
				List<Type> pureGenericParameters = new List<Type>();
				GenericServices.TraverseGenericType(type, delegate(Type t)
				{
					if (t.IsGenericParameter)
					{
						pureGenericParameters.Add(t);
					}
				});
				return pureGenericParameters;
			}
			return Type.EmptyTypes;
		}

		// Token: 0x06000233 RID: 563 RVA: 0x00006130 File Offset: 0x00004330
		internal static int GetPureGenericArity(this Type type)
		{
			Assumes.NotNull<Type>(type);
			int genericArity = 0;
			if (type.IsGenericType && type.ContainsGenericParameters)
			{
				new List<Type>();
				GenericServices.TraverseGenericType(type, delegate(Type t)
				{
					if (t.IsGenericParameter)
					{
						int genericArity = genericArity;
						genericArity++;
					}
				});
			}
			return genericArity;
		}

		// Token: 0x06000234 RID: 564 RVA: 0x00006180 File Offset: 0x00004380
		private static void TraverseGenericType(Type type, Action<Type> onType)
		{
			if (type.IsGenericType)
			{
				Type[] genericArguments = type.GetGenericArguments();
				for (int i = 0; i < genericArguments.Length; i++)
				{
					GenericServices.TraverseGenericType(genericArguments[i], onType);
				}
			}
			onType(type);
		}

		// Token: 0x06000235 RID: 565 RVA: 0x000061BA File Offset: 0x000043BA
		public static int[] GetGenericParametersOrder(Type type)
		{
			return (from parameter in type.GetPureGenericParameters()
			select parameter.GenericParameterPosition).ToArray<int>();
		}

		// Token: 0x06000236 RID: 566 RVA: 0x000061EC File Offset: 0x000043EC
		public static string GetGenericName(string originalGenericName, int[] genericParametersOrder, int genericArity)
		{
			string[] array = new string[genericArity];
			for (int i = 0; i < genericParametersOrder.Length; i++)
			{
				array[genericParametersOrder[i]] = string.Format(CultureInfo.InvariantCulture, "{{{0}}}", i);
			}
			return string.Format(CultureInfo.InvariantCulture, originalGenericName, array);
		}

		// Token: 0x06000237 RID: 567 RVA: 0x00006234 File Offset: 0x00004434
		public static T[] Reorder<T>(T[] original, int[] genericParametersOrder)
		{
			T[] array = new T[genericParametersOrder.Length];
			for (int i = 0; i < genericParametersOrder.Length; i++)
			{
				array[i] = original[genericParametersOrder[i]];
			}
			return array;
		}

		// Token: 0x06000238 RID: 568 RVA: 0x0000626C File Offset: 0x0000446C
		public static IEnumerable<Type> CreateTypeSpecializations(this Type[] types, Type[] specializationTypes)
		{
			if (types == null)
			{
				return null;
			}
			return from type in types
			select type.CreateTypeSpecialization(specializationTypes);
		}

		// Token: 0x06000239 RID: 569 RVA: 0x000062A0 File Offset: 0x000044A0
		public static Type CreateTypeSpecialization(this Type type, Type[] specializationTypes)
		{
			if (!type.ContainsGenericParameters)
			{
				return type;
			}
			if (type.IsGenericParameter)
			{
				return specializationTypes[type.GenericParameterPosition];
			}
			Type[] genericArguments = type.GetGenericArguments();
			Type[] array = new Type[genericArguments.Length];
			for (int i = 0; i < genericArguments.Length; i++)
			{
				Type type2 = genericArguments[i];
				array[i] = (type2.IsGenericParameter ? specializationTypes[type2.GenericParameterPosition] : type2);
			}
			return type.GetGenericTypeDefinition().MakeGenericType(array);
		}

		// Token: 0x0600023A RID: 570 RVA: 0x0000630C File Offset: 0x0000450C
		public static bool CanSpecialize(Type type, IEnumerable<Type> constraints, GenericParameterAttributes attributes)
		{
			return GenericServices.CanSpecialize(type, constraints) && GenericServices.CanSpecialize(type, attributes);
		}

		// Token: 0x0600023B RID: 571 RVA: 0x00006320 File Offset: 0x00004520
		public static bool CanSpecialize(Type type, IEnumerable<Type> constraintTypes)
		{
			if (constraintTypes == null)
			{
				return true;
			}
			foreach (Type type2 in constraintTypes)
			{
				if (type2 != null && !type2.IsAssignableFrom(type))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x0600023C RID: 572 RVA: 0x00006380 File Offset: 0x00004580
		public static bool CanSpecialize(Type type, GenericParameterAttributes attributes)
		{
			if (attributes == GenericParameterAttributes.None)
			{
				return true;
			}
			if ((attributes & GenericParameterAttributes.ReferenceTypeConstraint) != GenericParameterAttributes.None && type.IsValueType)
			{
				return false;
			}
			if ((attributes & GenericParameterAttributes.DefaultConstructorConstraint) != GenericParameterAttributes.None && !type.IsValueType && type.GetConstructor(Type.EmptyTypes) == null)
			{
				return false;
			}
			if ((attributes & GenericParameterAttributes.NotNullableValueTypeConstraint) != GenericParameterAttributes.None)
			{
				if (!type.IsValueType)
				{
					return false;
				}
				if (Nullable.GetUnderlyingType(type) != null)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x0200005B RID: 91
		[CompilerGenerated]
		private sealed class <>c__DisplayClass0_0
		{
			// Token: 0x0600023D RID: 573 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass0_0()
			{
			}

			// Token: 0x0600023E RID: 574 RVA: 0x000063E4 File Offset: 0x000045E4
			internal void <GetPureGenericParameters>b__0(Type t)
			{
				if (t.IsGenericParameter)
				{
					this.pureGenericParameters.Add(t);
				}
			}

			// Token: 0x040000DA RID: 218
			public List<Type> pureGenericParameters;
		}

		// Token: 0x0200005C RID: 92
		[CompilerGenerated]
		private sealed class <>c__DisplayClass1_0
		{
			// Token: 0x0600023F RID: 575 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass1_0()
			{
			}

			// Token: 0x06000240 RID: 576 RVA: 0x000063FC File Offset: 0x000045FC
			internal void <GetPureGenericArity>b__0(Type t)
			{
				if (t.IsGenericParameter)
				{
					int num = this.genericArity;
					this.genericArity = num + 1;
				}
			}

			// Token: 0x040000DB RID: 219
			public int genericArity;
		}

		// Token: 0x0200005D RID: 93
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000241 RID: 577 RVA: 0x00006421 File Offset: 0x00004621
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000242 RID: 578 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c()
			{
			}

			// Token: 0x06000243 RID: 579 RVA: 0x0000642D File Offset: 0x0000462D
			internal int <GetGenericParametersOrder>b__3_0(Type parameter)
			{
				return parameter.GenericParameterPosition;
			}

			// Token: 0x040000DC RID: 220
			public static readonly GenericServices.<>c <>9 = new GenericServices.<>c();

			// Token: 0x040000DD RID: 221
			public static Func<Type, int> <>9__3_0;
		}

		// Token: 0x0200005E RID: 94
		[CompilerGenerated]
		private sealed class <>c__DisplayClass6_0
		{
			// Token: 0x06000244 RID: 580 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass6_0()
			{
			}

			// Token: 0x06000245 RID: 581 RVA: 0x00006435 File Offset: 0x00004635
			internal Type <CreateTypeSpecializations>b__0(Type type)
			{
				return type.CreateTypeSpecialization(this.specializationTypes);
			}

			// Token: 0x040000DE RID: 222
			public Type[] specializationTypes;
		}
	}
}
