﻿using System;
using System.Globalization;
using System.Reflection;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x02000082 RID: 130
	internal class ReflectionParameter : ReflectionItem
	{
		// Token: 0x06000368 RID: 872 RVA: 0x00009A6A File Offset: 0x00007C6A
		public ReflectionParameter(ParameterInfo parameter)
		{
			Assumes.NotNull<ParameterInfo>(parameter);
			this._parameter = parameter;
		}

		// Token: 0x170000FC RID: 252
		// (get) Token: 0x06000369 RID: 873 RVA: 0x00009A7F File Offset: 0x00007C7F
		public ParameterInfo UnderlyingParameter
		{
			get
			{
				return this._parameter;
			}
		}

		// Token: 0x170000FD RID: 253
		// (get) Token: 0x0600036A RID: 874 RVA: 0x00009A87 File Offset: 0x00007C87
		public override string Name
		{
			get
			{
				return this.UnderlyingParameter.Name;
			}
		}

		// Token: 0x0600036B RID: 875 RVA: 0x00009A94 File Offset: 0x00007C94
		public override string GetDisplayName()
		{
			return string.Format(CultureInfo.CurrentCulture, "{0} (Parameter=\"{1}\")", this.UnderlyingParameter.Member.GetDisplayName(), this.UnderlyingParameter.Name);
		}

		// Token: 0x170000FE RID: 254
		// (get) Token: 0x0600036C RID: 876 RVA: 0x00009AC0 File Offset: 0x00007CC0
		public override Type ReturnType
		{
			get
			{
				return this.UnderlyingParameter.ParameterType;
			}
		}

		// Token: 0x170000FF RID: 255
		// (get) Token: 0x0600036D RID: 877 RVA: 0x00009ACD File Offset: 0x00007CCD
		public override ReflectionItemType ItemType
		{
			get
			{
				return ReflectionItemType.Parameter;
			}
		}

		// Token: 0x0400014D RID: 333
		private readonly ParameterInfo _parameter;
	}
}
