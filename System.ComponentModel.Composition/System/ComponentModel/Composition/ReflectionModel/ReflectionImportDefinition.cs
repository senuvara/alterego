﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x02000075 RID: 117
	internal abstract class ReflectionImportDefinition : ContractBasedImportDefinition, ICompositionElement
	{
		// Token: 0x0600030D RID: 781 RVA: 0x00008FF0 File Offset: 0x000071F0
		public ReflectionImportDefinition(string contractName, string requiredTypeIdentity, IEnumerable<KeyValuePair<string, Type>> requiredMetadata, ImportCardinality cardinality, bool isRecomposable, bool isPrerequisite, CreationPolicy requiredCreationPolicy, IDictionary<string, object> metadata, ICompositionElement origin) : base(contractName, requiredTypeIdentity, requiredMetadata, cardinality, isRecomposable, isPrerequisite, requiredCreationPolicy, metadata)
		{
			this._origin = origin;
		}

		// Token: 0x170000DE RID: 222
		// (get) Token: 0x0600030E RID: 782 RVA: 0x00009018 File Offset: 0x00007218
		string ICompositionElement.DisplayName
		{
			get
			{
				return this.GetDisplayName();
			}
		}

		// Token: 0x170000DF RID: 223
		// (get) Token: 0x0600030F RID: 783 RVA: 0x00009020 File Offset: 0x00007220
		ICompositionElement ICompositionElement.Origin
		{
			get
			{
				return this._origin;
			}
		}

		// Token: 0x06000310 RID: 784
		public abstract ImportingItem ToImportingItem();

		// Token: 0x06000311 RID: 785
		protected abstract string GetDisplayName();

		// Token: 0x0400012C RID: 300
		private readonly ICompositionElement _origin;
	}
}
