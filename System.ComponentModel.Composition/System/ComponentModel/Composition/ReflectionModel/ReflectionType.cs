﻿using System;
using System.Reflection;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x02000085 RID: 133
	internal class ReflectionType : ReflectionMember
	{
		// Token: 0x0600037F RID: 895 RVA: 0x00009CB6 File Offset: 0x00007EB6
		public ReflectionType(Type type)
		{
			Assumes.NotNull<Type>(type);
			this._type = type;
		}

		// Token: 0x1700010A RID: 266
		// (get) Token: 0x06000380 RID: 896 RVA: 0x00009CCB File Offset: 0x00007ECB
		public override MemberInfo UnderlyingMember
		{
			get
			{
				return this._type;
			}
		}

		// Token: 0x1700010B RID: 267
		// (get) Token: 0x06000381 RID: 897 RVA: 0x000052FB File Offset: 0x000034FB
		public override bool CanRead
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700010C RID: 268
		// (get) Token: 0x06000382 RID: 898 RVA: 0x000052FB File Offset: 0x000034FB
		public override bool RequiresInstance
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700010D RID: 269
		// (get) Token: 0x06000383 RID: 899 RVA: 0x00009CCB File Offset: 0x00007ECB
		public override Type ReturnType
		{
			get
			{
				return this._type;
			}
		}

		// Token: 0x1700010E RID: 270
		// (get) Token: 0x06000384 RID: 900 RVA: 0x00009CD3 File Offset: 0x00007ED3
		public override ReflectionItemType ItemType
		{
			get
			{
				return ReflectionItemType.Type;
			}
		}

		// Token: 0x06000385 RID: 901 RVA: 0x00009CD6 File Offset: 0x00007ED6
		public override object GetValue(object instance)
		{
			return instance;
		}

		// Token: 0x04000151 RID: 337
		private Type _type;
	}
}
