﻿using System;
using System.ComponentModel.Composition.Primitives;
using System.Runtime.CompilerServices;
using Microsoft.Internal;
using Microsoft.Internal.Collections;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x02000065 RID: 101
	internal class ImportType
	{
		// Token: 0x06000272 RID: 626 RVA: 0x00006F7C File Offset: 0x0000517C
		public ImportType(Type type, ImportCardinality cardinality)
		{
			Assumes.NotNull<Type>(type);
			this._type = type;
			this._contractType = type;
			if (cardinality == ImportCardinality.ZeroOrMore)
			{
				this._isAssignableCollectionType = ImportType.IsTypeAssignableCollectionType(type);
				this._contractType = this.CheckForCollection(type);
			}
			this._isOpenGeneric = type.ContainsGenericParameters;
			this._contractType = this.CheckForLazyAndPartCreator(this._contractType);
		}

		// Token: 0x170000B8 RID: 184
		// (get) Token: 0x06000273 RID: 627 RVA: 0x00006FDE File Offset: 0x000051DE
		public bool IsAssignableCollectionType
		{
			get
			{
				return this._isAssignableCollectionType;
			}
		}

		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x06000274 RID: 628 RVA: 0x00006FE6 File Offset: 0x000051E6
		// (set) Token: 0x06000275 RID: 629 RVA: 0x00006FEE File Offset: 0x000051EE
		public Type ElementType
		{
			[CompilerGenerated]
			get
			{
				return this.<ElementType>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ElementType>k__BackingField = value;
			}
		}

		// Token: 0x170000BA RID: 186
		// (get) Token: 0x06000276 RID: 630 RVA: 0x00006FF7 File Offset: 0x000051F7
		public Type ActualType
		{
			get
			{
				return this._type;
			}
		}

		// Token: 0x170000BB RID: 187
		// (get) Token: 0x06000277 RID: 631 RVA: 0x00006FFF File Offset: 0x000051FF
		// (set) Token: 0x06000278 RID: 632 RVA: 0x00007007 File Offset: 0x00005207
		public bool IsPartCreator
		{
			[CompilerGenerated]
			get
			{
				return this.<IsPartCreator>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<IsPartCreator>k__BackingField = value;
			}
		}

		// Token: 0x170000BC RID: 188
		// (get) Token: 0x06000279 RID: 633 RVA: 0x00007010 File Offset: 0x00005210
		public Type ContractType
		{
			get
			{
				return this._contractType;
			}
		}

		// Token: 0x170000BD RID: 189
		// (get) Token: 0x0600027A RID: 634 RVA: 0x00007018 File Offset: 0x00005218
		public Func<Export, object> CastExport
		{
			get
			{
				Assumes.IsTrue(!this._isOpenGeneric);
				return this._castSingleValue;
			}
		}

		// Token: 0x170000BE RID: 190
		// (get) Token: 0x0600027B RID: 635 RVA: 0x0000702E File Offset: 0x0000522E
		// (set) Token: 0x0600027C RID: 636 RVA: 0x00007036 File Offset: 0x00005236
		public Type MetadataViewType
		{
			[CompilerGenerated]
			get
			{
				return this.<MetadataViewType>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<MetadataViewType>k__BackingField = value;
			}
		}

		// Token: 0x0600027D RID: 637 RVA: 0x0000703F File Offset: 0x0000523F
		private Type CheckForCollection(Type type)
		{
			this.ElementType = CollectionServices.GetEnumerableElementType(type);
			if (this.ElementType != null)
			{
				return this.ElementType;
			}
			return type;
		}

		// Token: 0x0600027E RID: 638 RVA: 0x00007064 File Offset: 0x00005264
		private static bool IsGenericDescendentOf(Type type, Type baseGenericTypeDefinition)
		{
			return !(type == typeof(object)) && !(type == null) && ((type.IsGenericType && type.GetGenericTypeDefinition() == baseGenericTypeDefinition) || ImportType.IsGenericDescendentOf(type.BaseType, baseGenericTypeDefinition));
		}

		// Token: 0x0600027F RID: 639 RVA: 0x000070B2 File Offset: 0x000052B2
		public static bool IsDescendentOf(Type type, Type baseType)
		{
			Assumes.NotNull<Type>(type);
			Assumes.NotNull<Type>(baseType);
			if (!baseType.IsGenericTypeDefinition)
			{
				return baseType.IsAssignableFrom(type);
			}
			return ImportType.IsGenericDescendentOf(type, baseType.GetGenericTypeDefinition());
		}

		// Token: 0x06000280 RID: 640 RVA: 0x000070DC File Offset: 0x000052DC
		private Type CheckForLazyAndPartCreator(Type type)
		{
			if (type.IsGenericType)
			{
				Type underlyingSystemType = type.GetGenericTypeDefinition().UnderlyingSystemType;
				Type[] genericArguments = type.GetGenericArguments();
				if (underlyingSystemType == ImportType.LazyOfTType)
				{
					if (!this._isOpenGeneric)
					{
						this._castSingleValue = ExportServices.CreateStronglyTypedLazyFactory(genericArguments[0].UnderlyingSystemType, null);
					}
					return genericArguments[0];
				}
				if (underlyingSystemType == ImportType.LazyOfTMType)
				{
					this.MetadataViewType = genericArguments[1];
					if (!this._isOpenGeneric)
					{
						this._castSingleValue = ExportServices.CreateStronglyTypedLazyFactory(genericArguments[0].UnderlyingSystemType, genericArguments[1].UnderlyingSystemType);
					}
					return genericArguments[0];
				}
				if (underlyingSystemType != null && ImportType.IsDescendentOf(underlyingSystemType, ImportType.ExportFactoryOfTType))
				{
					this.IsPartCreator = true;
					if (genericArguments.Length == 1)
					{
						if (!this._isOpenGeneric)
						{
							this._castSingleValue = new ExportFactoryCreator(underlyingSystemType).CreateStronglyTypedExportFactoryFactory(genericArguments[0].UnderlyingSystemType, null);
						}
					}
					else
					{
						if (genericArguments.Length != 2)
						{
							throw ExceptionBuilder.ExportFactory_TooManyGenericParameters(underlyingSystemType.FullName);
						}
						if (!this._isOpenGeneric)
						{
							this._castSingleValue = new ExportFactoryCreator(underlyingSystemType).CreateStronglyTypedExportFactoryFactory(genericArguments[0].UnderlyingSystemType, genericArguments[1].UnderlyingSystemType);
						}
						this.MetadataViewType = genericArguments[1];
					}
					return genericArguments[0];
				}
			}
			return type;
		}

		// Token: 0x06000281 RID: 641 RVA: 0x00007206 File Offset: 0x00005406
		private static bool IsTypeAssignableCollectionType(Type type)
		{
			return type.IsArray || CollectionServices.IsEnumerableOfT(type);
		}

		// Token: 0x06000282 RID: 642 RVA: 0x0000721B File Offset: 0x0000541B
		// Note: this type is marked as 'beforefieldinit'.
		static ImportType()
		{
		}

		// Token: 0x040000F5 RID: 245
		private static readonly Type LazyOfTType = typeof(Lazy<>);

		// Token: 0x040000F6 RID: 246
		private static readonly Type LazyOfTMType = typeof(Lazy<, >);

		// Token: 0x040000F7 RID: 247
		private static readonly Type ExportFactoryOfTType = typeof(ExportFactory<>);

		// Token: 0x040000F8 RID: 248
		private static readonly Type ExportFactoryOfTMType = typeof(ExportFactory<, >);

		// Token: 0x040000F9 RID: 249
		private readonly Type _type;

		// Token: 0x040000FA RID: 250
		private readonly bool _isAssignableCollectionType;

		// Token: 0x040000FB RID: 251
		private readonly Type _contractType;

		// Token: 0x040000FC RID: 252
		private Func<Export, object> _castSingleValue;

		// Token: 0x040000FD RID: 253
		private bool _isOpenGeneric;

		// Token: 0x040000FE RID: 254
		[CompilerGenerated]
		private Type <ElementType>k__BackingField;

		// Token: 0x040000FF RID: 255
		[CompilerGenerated]
		private bool <IsPartCreator>k__BackingField;

		// Token: 0x04000100 RID: 256
		[CompilerGenerated]
		private Type <MetadataViewType>k__BackingField;
	}
}
