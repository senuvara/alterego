﻿using System;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x02000052 RID: 82
	internal sealed class ExportFactoryCreator
	{
		// Token: 0x06000217 RID: 535 RVA: 0x00005BD4 File Offset: 0x00003DD4
		public ExportFactoryCreator(Type exportFactoryType)
		{
			Assumes.NotNull<Type>(exportFactoryType);
			this._exportFactoryType = exportFactoryType;
		}

		// Token: 0x06000218 RID: 536 RVA: 0x00005BEC File Offset: 0x00003DEC
		public Func<Export, object> CreateStronglyTypedExportFactoryFactory(Type exportType, Type metadataViewType)
		{
			MethodInfo methodInfo;
			if (metadataViewType == null)
			{
				methodInfo = ExportFactoryCreator._createStronglyTypedExportFactoryOfT.MakeGenericMethod(new Type[]
				{
					exportType
				});
			}
			else
			{
				methodInfo = ExportFactoryCreator._createStronglyTypedExportFactoryOfTM.MakeGenericMethod(new Type[]
				{
					exportType,
					metadataViewType
				});
			}
			Assumes.NotNull<MethodInfo>(methodInfo);
			Func<Export, object> exportFactoryFactory = (Func<Export, object>)Delegate.CreateDelegate(typeof(Func<Export, object>), this, methodInfo);
			return (Export e) => exportFactoryFactory(e);
		}

		// Token: 0x06000219 RID: 537 RVA: 0x00005C68 File Offset: 0x00003E68
		private object CreateStronglyTypedExportFactoryOfT<T>(Export export)
		{
			Type[] typeArguments = new Type[]
			{
				typeof(T)
			};
			Type type = this._exportFactoryType.MakeGenericType(typeArguments);
			ExportFactoryCreator.LifetimeContext lifetimeContext = new ExportFactoryCreator.LifetimeContext();
			Func<Tuple<T, Action>> func = () => lifetimeContext.GetExportLifetimeContextFromExport<T>(export);
			object[] args = new object[]
			{
				func
			};
			object obj = Activator.CreateInstance(type, args);
			lifetimeContext.SetInstance(obj);
			return obj;
		}

		// Token: 0x0600021A RID: 538 RVA: 0x00005CE0 File Offset: 0x00003EE0
		private object CreateStronglyTypedExportFactoryOfTM<T, M>(Export export)
		{
			Type[] typeArguments = new Type[]
			{
				typeof(T),
				typeof(M)
			};
			Type type = this._exportFactoryType.MakeGenericType(typeArguments);
			ExportFactoryCreator.LifetimeContext lifetimeContext = new ExportFactoryCreator.LifetimeContext();
			Func<Tuple<T, Action>> func = () => lifetimeContext.GetExportLifetimeContextFromExport<T>(export);
			M metadataView = AttributedModelServices.GetMetadataView<M>(export.Metadata);
			object[] args = new object[]
			{
				func,
				metadataView
			};
			object obj = Activator.CreateInstance(type, args);
			lifetimeContext.SetInstance(obj);
			return obj;
		}

		// Token: 0x0600021B RID: 539 RVA: 0x00005D81 File Offset: 0x00003F81
		// Note: this type is marked as 'beforefieldinit'.
		static ExportFactoryCreator()
		{
		}

		// Token: 0x040000C9 RID: 201
		private static readonly MethodInfo _createStronglyTypedExportFactoryOfT = typeof(ExportFactoryCreator).GetMethod("CreateStronglyTypedExportFactoryOfT", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

		// Token: 0x040000CA RID: 202
		private static readonly MethodInfo _createStronglyTypedExportFactoryOfTM = typeof(ExportFactoryCreator).GetMethod("CreateStronglyTypedExportFactoryOfTM", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

		// Token: 0x040000CB RID: 203
		private Type _exportFactoryType;

		// Token: 0x02000053 RID: 83
		private class LifetimeContext
		{
			// Token: 0x170000B0 RID: 176
			// (get) Token: 0x0600021C RID: 540 RVA: 0x00005DB9 File Offset: 0x00003FB9
			// (set) Token: 0x0600021D RID: 541 RVA: 0x00005DC1 File Offset: 0x00003FC1
			public Func<ComposablePartDefinition, bool> CatalogFilter
			{
				[CompilerGenerated]
				get
				{
					return this.<CatalogFilter>k__BackingField;
				}
				[CompilerGenerated]
				private set
				{
					this.<CatalogFilter>k__BackingField = value;
				}
			}

			// Token: 0x0600021E RID: 542 RVA: 0x00005DCC File Offset: 0x00003FCC
			public void SetInstance(object instance)
			{
				Assumes.NotNull<object>(instance);
				MethodInfo method = instance.GetType().GetMethod("IncludeInScopedCatalog", BindingFlags.Instance | BindingFlags.NonPublic, null, ExportFactoryCreator.LifetimeContext.types, null);
				this.CatalogFilter = (Func<ComposablePartDefinition, bool>)Delegate.CreateDelegate(typeof(Func<ComposablePartDefinition, bool>), instance, method);
			}

			// Token: 0x0600021F RID: 543 RVA: 0x00005E18 File Offset: 0x00004018
			public Tuple<T, Action> GetExportLifetimeContextFromExport<T>(Export export)
			{
				IDisposable disposable = null;
				CatalogExportProvider.ScopeFactoryExport scopeFactoryExport = export as CatalogExportProvider.ScopeFactoryExport;
				T item;
				if (scopeFactoryExport != null)
				{
					Export export2 = scopeFactoryExport.CreateExportProduct(this.CatalogFilter);
					item = ExportServices.GetCastedExportedValue<T>(export2);
					disposable = (export2 as IDisposable);
				}
				else
				{
					CatalogExportProvider.FactoryExport factoryExport = export as CatalogExportProvider.FactoryExport;
					if (factoryExport != null)
					{
						Export export3 = factoryExport.CreateExportProduct();
						item = ExportServices.GetCastedExportedValue<T>(export3);
						disposable = (export3 as IDisposable);
					}
					else
					{
						ComposablePartDefinition castedExportedValue = ExportServices.GetCastedExportedValue<ComposablePartDefinition>(export);
						ComposablePart composablePart = castedExportedValue.CreatePart();
						ExportDefinition definition = castedExportedValue.ExportDefinitions.Single<ExportDefinition>();
						item = ExportServices.CastExportedValue<T>(composablePart.ToElement(), composablePart.GetExportedValue(definition));
						disposable = (composablePart as IDisposable);
					}
				}
				Action item2;
				if (disposable != null)
				{
					item2 = delegate()
					{
						disposable.Dispose();
					};
				}
				else
				{
					item2 = delegate()
					{
					};
				}
				return new Tuple<T, Action>(item, item2);
			}

			// Token: 0x06000220 RID: 544 RVA: 0x000025B0 File Offset: 0x000007B0
			public LifetimeContext()
			{
			}

			// Token: 0x06000221 RID: 545 RVA: 0x00005F08 File Offset: 0x00004108
			// Note: this type is marked as 'beforefieldinit'.
			static LifetimeContext()
			{
			}

			// Token: 0x040000CC RID: 204
			private static Type[] types = new Type[]
			{
				typeof(ComposablePartDefinition)
			};

			// Token: 0x040000CD RID: 205
			[CompilerGenerated]
			private Func<ComposablePartDefinition, bool> <CatalogFilter>k__BackingField;

			// Token: 0x02000054 RID: 84
			[CompilerGenerated]
			private sealed class <>c__DisplayClass6_0<T>
			{
				// Token: 0x06000222 RID: 546 RVA: 0x000025B0 File Offset: 0x000007B0
				public <>c__DisplayClass6_0()
				{
				}

				// Token: 0x06000223 RID: 547 RVA: 0x00005F22 File Offset: 0x00004122
				internal void <GetExportLifetimeContextFromExport>b__0()
				{
					this.disposable.Dispose();
				}

				// Token: 0x040000CE RID: 206
				public IDisposable disposable;
			}

			// Token: 0x02000055 RID: 85
			[CompilerGenerated]
			[Serializable]
			private sealed class <>c__6<T>
			{
				// Token: 0x06000224 RID: 548 RVA: 0x00005F2F File Offset: 0x0000412F
				// Note: this type is marked as 'beforefieldinit'.
				static <>c__6()
				{
				}

				// Token: 0x06000225 RID: 549 RVA: 0x000025B0 File Offset: 0x000007B0
				public <>c__6()
				{
				}

				// Token: 0x06000226 RID: 550 RVA: 0x00002304 File Offset: 0x00000504
				internal void <GetExportLifetimeContextFromExport>b__6_1()
				{
				}

				// Token: 0x040000CF RID: 207
				public static readonly ExportFactoryCreator.LifetimeContext.<>c__6<T> <>9 = new ExportFactoryCreator.LifetimeContext.<>c__6<T>();

				// Token: 0x040000D0 RID: 208
				public static Action <>9__6_1;
			}
		}

		// Token: 0x02000056 RID: 86
		[CompilerGenerated]
		private sealed class <>c__DisplayClass5_0
		{
			// Token: 0x06000227 RID: 551 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass5_0()
			{
			}

			// Token: 0x06000228 RID: 552 RVA: 0x00005F3B File Offset: 0x0000413B
			internal object <CreateStronglyTypedExportFactoryFactory>b__0(Export e)
			{
				return this.exportFactoryFactory(e);
			}

			// Token: 0x040000D1 RID: 209
			public Func<Export, object> exportFactoryFactory;
		}

		// Token: 0x02000057 RID: 87
		[CompilerGenerated]
		private sealed class <>c__DisplayClass6_0<T>
		{
			// Token: 0x06000229 RID: 553 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass6_0()
			{
			}

			// Token: 0x0600022A RID: 554 RVA: 0x00005F49 File Offset: 0x00004149
			internal Tuple<T, Action> <CreateStronglyTypedExportFactoryOfT>b__0()
			{
				return this.lifetimeContext.GetExportLifetimeContextFromExport<T>(this.export);
			}

			// Token: 0x040000D2 RID: 210
			public ExportFactoryCreator.LifetimeContext lifetimeContext;

			// Token: 0x040000D3 RID: 211
			public Export export;
		}

		// Token: 0x02000058 RID: 88
		[CompilerGenerated]
		private sealed class <>c__DisplayClass7_0<T, M>
		{
			// Token: 0x0600022B RID: 555 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass7_0()
			{
			}

			// Token: 0x0600022C RID: 556 RVA: 0x00005F5C File Offset: 0x0000415C
			internal Tuple<T, Action> <CreateStronglyTypedExportFactoryOfTM>b__0()
			{
				return this.lifetimeContext.GetExportLifetimeContextFromExport<T>(this.export);
			}

			// Token: 0x040000D4 RID: 212
			public ExportFactoryCreator.LifetimeContext lifetimeContext;

			// Token: 0x040000D5 RID: 213
			public Export export;
		}
	}
}
