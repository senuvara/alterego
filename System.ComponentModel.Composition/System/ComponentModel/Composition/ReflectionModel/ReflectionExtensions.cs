﻿using System;
using System.Reflection;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x02000073 RID: 115
	internal static class ReflectionExtensions
	{
		// Token: 0x060002F9 RID: 761 RVA: 0x00008DC4 File Offset: 0x00006FC4
		public static ReflectionMember ToReflectionMember(this LazyMemberInfo lazyMember)
		{
			MemberInfo[] accessors = lazyMember.GetAccessors();
			MemberTypes memberType = lazyMember.MemberType;
			if (memberType <= MemberTypes.Property)
			{
				if (memberType == MemberTypes.Field)
				{
					Assumes.IsTrue(accessors.Length == 1);
					return ((FieldInfo)accessors[0]).ToReflectionField();
				}
				if (memberType == MemberTypes.Property)
				{
					Assumes.IsTrue(accessors.Length == 2);
					return ReflectionExtensions.CreateReflectionProperty((MethodInfo)accessors[0], (MethodInfo)accessors[1]);
				}
			}
			else if (memberType == MemberTypes.TypeInfo || memberType == MemberTypes.NestedType)
			{
				return ((Type)accessors[0]).ToReflectionType();
			}
			Assumes.IsTrue(memberType == MemberTypes.Method);
			return ((MethodInfo)accessors[0]).ToReflectionMethod();
		}

		// Token: 0x060002FA RID: 762 RVA: 0x00008E60 File Offset: 0x00007060
		public static LazyMemberInfo ToLazyMember(this MemberInfo member)
		{
			Assumes.NotNull<MemberInfo>(member);
			if (member.MemberType == MemberTypes.Property)
			{
				PropertyInfo propertyInfo = member as PropertyInfo;
				Assumes.NotNull<PropertyInfo>(propertyInfo);
				MemberInfo[] accessors = new MemberInfo[]
				{
					propertyInfo.GetGetMethod(true),
					propertyInfo.GetSetMethod(true)
				};
				return new LazyMemberInfo(MemberTypes.Property, accessors);
			}
			return new LazyMemberInfo(member);
		}

		// Token: 0x060002FB RID: 763 RVA: 0x00008EB4 File Offset: 0x000070B4
		public static ReflectionWritableMember ToReflectionWriteableMember(this LazyMemberInfo lazyMember)
		{
			Assumes.IsTrue(lazyMember.MemberType == MemberTypes.Field || lazyMember.MemberType == MemberTypes.Property);
			ReflectionWritableMember reflectionWritableMember = lazyMember.ToReflectionMember() as ReflectionWritableMember;
			Assumes.NotNull<ReflectionWritableMember>(reflectionWritableMember);
			return reflectionWritableMember;
		}

		// Token: 0x060002FC RID: 764 RVA: 0x00008EE4 File Offset: 0x000070E4
		public static ReflectionProperty ToReflectionProperty(this PropertyInfo property)
		{
			Assumes.NotNull<PropertyInfo>(property);
			return ReflectionExtensions.CreateReflectionProperty(property.GetGetMethod(true), property.GetSetMethod(true));
		}

		// Token: 0x060002FD RID: 765 RVA: 0x00008EFF File Offset: 0x000070FF
		public static ReflectionProperty CreateReflectionProperty(MethodInfo getMethod, MethodInfo setMethod)
		{
			Assumes.IsTrue(getMethod != null || setMethod != null);
			return new ReflectionProperty(getMethod, setMethod);
		}

		// Token: 0x060002FE RID: 766 RVA: 0x00008F20 File Offset: 0x00007120
		public static ReflectionParameter ToReflectionParameter(this ParameterInfo parameter)
		{
			Assumes.NotNull<ParameterInfo>(parameter);
			return new ReflectionParameter(parameter);
		}

		// Token: 0x060002FF RID: 767 RVA: 0x00008F2E File Offset: 0x0000712E
		public static ReflectionMethod ToReflectionMethod(this MethodInfo method)
		{
			Assumes.NotNull<MethodInfo>(method);
			return new ReflectionMethod(method);
		}

		// Token: 0x06000300 RID: 768 RVA: 0x00008F3C File Offset: 0x0000713C
		public static ReflectionField ToReflectionField(this FieldInfo field)
		{
			Assumes.NotNull<FieldInfo>(field);
			return new ReflectionField(field);
		}

		// Token: 0x06000301 RID: 769 RVA: 0x00008F4A File Offset: 0x0000714A
		public static ReflectionType ToReflectionType(this Type type)
		{
			Assumes.NotNull<Type>(type);
			return new ReflectionType(type);
		}

		// Token: 0x06000302 RID: 770 RVA: 0x00008F58 File Offset: 0x00007158
		public static ReflectionWritableMember ToReflectionWritableMember(this MemberInfo member)
		{
			Assumes.NotNull<MemberInfo>(member);
			if (member.MemberType == MemberTypes.Property)
			{
				return ((PropertyInfo)member).ToReflectionProperty();
			}
			return ((FieldInfo)member).ToReflectionField();
		}
	}
}
