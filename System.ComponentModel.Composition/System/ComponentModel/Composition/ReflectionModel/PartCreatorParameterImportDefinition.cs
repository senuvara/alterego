﻿using System;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x0200006D RID: 109
	internal class PartCreatorParameterImportDefinition : ReflectionParameterImportDefinition, IPartCreatorImportDefinition
	{
		// Token: 0x060002AD RID: 685 RVA: 0x00007D1C File Offset: 0x00005F1C
		public PartCreatorParameterImportDefinition(Lazy<ParameterInfo> importingLazyParameter, ICompositionElement origin, ContractBasedImportDefinition productImportDefinition) : base(importingLazyParameter, "System.ComponentModel.Composition.Contracts.ExportFactory", CompositionConstants.PartCreatorTypeIdentity, productImportDefinition.RequiredMetadata, productImportDefinition.Cardinality, CreationPolicy.Any, MetadataServices.EmptyMetadata, origin)
		{
			Assumes.NotNull<ContractBasedImportDefinition>(productImportDefinition);
			this._productImportDefinition = productImportDefinition;
		}

		// Token: 0x170000C6 RID: 198
		// (get) Token: 0x060002AE RID: 686 RVA: 0x00007D5A File Offset: 0x00005F5A
		public ContractBasedImportDefinition ProductImportDefinition
		{
			get
			{
				return this._productImportDefinition;
			}
		}

		// Token: 0x060002AF RID: 687 RVA: 0x00007D62 File Offset: 0x00005F62
		public override bool IsConstraintSatisfiedBy(ExportDefinition exportDefinition)
		{
			return base.IsConstraintSatisfiedBy(exportDefinition) && PartCreatorExportDefinition.IsProductConstraintSatisfiedBy(this._productImportDefinition, exportDefinition);
		}

		// Token: 0x170000C7 RID: 199
		// (get) Token: 0x060002B0 RID: 688 RVA: 0x00007D7B File Offset: 0x00005F7B
		public override Expression<Func<ExportDefinition, bool>> Constraint
		{
			get
			{
				return ConstraintServices.CreatePartCreatorConstraint(base.Constraint, this._productImportDefinition);
			}
		}

		// Token: 0x0400010E RID: 270
		private readonly ContractBasedImportDefinition _productImportDefinition;
	}
}
