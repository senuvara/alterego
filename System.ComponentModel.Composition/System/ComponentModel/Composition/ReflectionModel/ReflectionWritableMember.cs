﻿using System;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x02000086 RID: 134
	internal abstract class ReflectionWritableMember : ReflectionMember
	{
		// Token: 0x1700010F RID: 271
		// (get) Token: 0x06000386 RID: 902
		public abstract bool CanWrite { get; }

		// Token: 0x06000387 RID: 903
		public abstract void SetValue(object instance, object value);

		// Token: 0x06000388 RID: 904 RVA: 0x00009CD9 File Offset: 0x00007ED9
		protected ReflectionWritableMember()
		{
		}
	}
}
