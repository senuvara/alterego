﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using Microsoft.Internal;
using Microsoft.Internal.Collections;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x02000071 RID: 113
	internal class ReflectionComposablePartDefinition : ComposablePartDefinition, ICompositionElement
	{
		// Token: 0x060002DD RID: 733 RVA: 0x00008824 File Offset: 0x00006A24
		public ReflectionComposablePartDefinition(IReflectionPartCreationInfo creationInfo)
		{
			Assumes.NotNull<IReflectionPartCreationInfo>(creationInfo);
			this._creationInfo = creationInfo;
		}

		// Token: 0x060002DE RID: 734 RVA: 0x00008844 File Offset: 0x00006A44
		public Type GetPartType()
		{
			return this._creationInfo.GetPartType();
		}

		// Token: 0x060002DF RID: 735 RVA: 0x00008851 File Offset: 0x00006A51
		public Lazy<Type> GetLazyPartType()
		{
			return this._creationInfo.GetLazyPartType();
		}

		// Token: 0x060002E0 RID: 736 RVA: 0x00008860 File Offset: 0x00006A60
		public ConstructorInfo GetConstructor()
		{
			if (this._constructor == null)
			{
				ConstructorInfo constructor = this._creationInfo.GetConstructor();
				object @lock = this._lock;
				lock (@lock)
				{
					if (this._constructor == null)
					{
						this._constructor = constructor;
					}
				}
			}
			return this._constructor;
		}

		// Token: 0x170000CF RID: 207
		// (get) Token: 0x060002E1 RID: 737 RVA: 0x000088D8 File Offset: 0x00006AD8
		public override IEnumerable<ExportDefinition> ExportDefinitions
		{
			get
			{
				if (this._exports == null)
				{
					ExportDefinition[] exports = this._creationInfo.GetExports().ToArray<ExportDefinition>();
					object @lock = this._lock;
					lock (@lock)
					{
						if (this._exports == null)
						{
							this._exports = exports;
						}
					}
				}
				return this._exports;
			}
		}

		// Token: 0x170000D0 RID: 208
		// (get) Token: 0x060002E2 RID: 738 RVA: 0x00008948 File Offset: 0x00006B48
		public override IEnumerable<ImportDefinition> ImportDefinitions
		{
			get
			{
				if (this._imports == null)
				{
					ImportDefinition[] imports = this._creationInfo.GetImports().ToArray<ImportDefinition>();
					object @lock = this._lock;
					lock (@lock)
					{
						if (this._imports == null)
						{
							this._imports = imports;
						}
					}
				}
				return this._imports;
			}
		}

		// Token: 0x170000D1 RID: 209
		// (get) Token: 0x060002E3 RID: 739 RVA: 0x000089B8 File Offset: 0x00006BB8
		public override IDictionary<string, object> Metadata
		{
			get
			{
				if (this._metadata == null)
				{
					IDictionary<string, object> metadata = this._creationInfo.GetMetadata().AsReadOnly();
					object @lock = this._lock;
					lock (@lock)
					{
						if (this._metadata == null)
						{
							this._metadata = metadata;
						}
					}
				}
				return this._metadata;
			}
		}

		// Token: 0x170000D2 RID: 210
		// (get) Token: 0x060002E4 RID: 740 RVA: 0x00008A28 File Offset: 0x00006C28
		internal bool IsDisposalRequired
		{
			get
			{
				return this._creationInfo.IsDisposalRequired;
			}
		}

		// Token: 0x060002E5 RID: 741 RVA: 0x00008A35 File Offset: 0x00006C35
		public override ComposablePart CreatePart()
		{
			if (this.IsDisposalRequired)
			{
				return new DisposableReflectionComposablePart(this);
			}
			return new ReflectionComposablePart(this);
		}

		// Token: 0x060002E6 RID: 742 RVA: 0x00008A4C File Offset: 0x00006C4C
		internal override ComposablePartDefinition GetGenericPartDefinition()
		{
			GenericSpecializationPartCreationInfo genericSpecializationPartCreationInfo = this._creationInfo as GenericSpecializationPartCreationInfo;
			if (genericSpecializationPartCreationInfo != null)
			{
				return genericSpecializationPartCreationInfo.OriginalPart;
			}
			return null;
		}

		// Token: 0x060002E7 RID: 743 RVA: 0x00008A70 File Offset: 0x00006C70
		internal override IEnumerable<Tuple<ComposablePartDefinition, ExportDefinition>> GetExports(ImportDefinition definition)
		{
			if (this.IsGeneric())
			{
				List<Tuple<ComposablePartDefinition, ExportDefinition>> list = null;
				IEnumerable<object> enumerable = (definition.Metadata.Count > 0) ? definition.Metadata.GetValue("System.ComponentModel.Composition.GenericParameters") : null;
				if (enumerable != null)
				{
					Type[] genericParameters = null;
					if (ReflectionComposablePartDefinition.TryGetGenericTypeParameters(enumerable, out genericParameters))
					{
						foreach (Type[] genericTypeParameters in this.GetCandidateParameters(genericParameters))
						{
							ComposablePartDefinition composablePartDefinition = null;
							if (this.TryMakeGenericPartDefinition(genericTypeParameters, out composablePartDefinition))
							{
								IEnumerable<Tuple<ComposablePartDefinition, ExportDefinition>> exports = composablePartDefinition.GetExports(definition);
								if (exports != ComposablePartDefinition._EmptyExports)
								{
									list = list.FastAppendToListAllowNulls(exports);
								}
							}
						}
					}
				}
				IEnumerable<Tuple<ComposablePartDefinition, ExportDefinition>> enumerable2 = list;
				return enumerable2 ?? ComposablePartDefinition._EmptyExports;
			}
			return base.GetExports(definition);
		}

		// Token: 0x060002E8 RID: 744 RVA: 0x00008B38 File Offset: 0x00006D38
		private IEnumerable<Type[]> GetCandidateParameters(Type[] genericParameters)
		{
			foreach (ExportDefinition exportDefinition in this.ExportDefinitions)
			{
				int[] value = exportDefinition.Metadata.GetValue("System.ComponentModel.Composition.GenericExportParametersOrderMetadataName");
				if (value != null && value.Length == genericParameters.Length)
				{
					yield return GenericServices.Reorder<Type>(genericParameters, value);
				}
			}
			IEnumerator<ExportDefinition> enumerator = null;
			yield break;
			yield break;
		}

		// Token: 0x060002E9 RID: 745 RVA: 0x00008B50 File Offset: 0x00006D50
		private static bool TryGetGenericTypeParameters(IEnumerable<object> genericParameters, out Type[] genericTypeParameters)
		{
			genericTypeParameters = (genericParameters as Type[]);
			if (genericTypeParameters == null)
			{
				object[] array = genericParameters.AsArray<object>();
				genericTypeParameters = new Type[array.Length];
				for (int i = 0; i < array.Length; i++)
				{
					genericTypeParameters[i] = (array[i] as Type);
					if (genericTypeParameters[i] == null)
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x060002EA RID: 746 RVA: 0x00008BA3 File Offset: 0x00006DA3
		internal bool TryMakeGenericPartDefinition(Type[] genericTypeParameters, out ComposablePartDefinition genericPartDefinition)
		{
			genericPartDefinition = null;
			if (!GenericSpecializationPartCreationInfo.CanSpecialize(this.Metadata, genericTypeParameters))
			{
				return false;
			}
			genericPartDefinition = new ReflectionComposablePartDefinition(new GenericSpecializationPartCreationInfo(this._creationInfo, this, genericTypeParameters));
			return true;
		}

		// Token: 0x170000D3 RID: 211
		// (get) Token: 0x060002EB RID: 747 RVA: 0x00008BCD File Offset: 0x00006DCD
		string ICompositionElement.DisplayName
		{
			get
			{
				return this._creationInfo.DisplayName;
			}
		}

		// Token: 0x170000D4 RID: 212
		// (get) Token: 0x060002EC RID: 748 RVA: 0x00008BDA File Offset: 0x00006DDA
		ICompositionElement ICompositionElement.Origin
		{
			get
			{
				return this._creationInfo.Origin;
			}
		}

		// Token: 0x060002ED RID: 749 RVA: 0x00008BCD File Offset: 0x00006DCD
		public override string ToString()
		{
			return this._creationInfo.DisplayName;
		}

		// Token: 0x060002EE RID: 750 RVA: 0x00008BE8 File Offset: 0x00006DE8
		public override bool Equals(object obj)
		{
			ReflectionComposablePartDefinition reflectionComposablePartDefinition = obj as ReflectionComposablePartDefinition;
			return reflectionComposablePartDefinition != null && this._creationInfo.Equals(reflectionComposablePartDefinition._creationInfo);
		}

		// Token: 0x060002EF RID: 751 RVA: 0x00008C12 File Offset: 0x00006E12
		public override int GetHashCode()
		{
			return this._creationInfo.GetHashCode();
		}

		// Token: 0x0400011E RID: 286
		private readonly IReflectionPartCreationInfo _creationInfo;

		// Token: 0x0400011F RID: 287
		private volatile IEnumerable<ImportDefinition> _imports;

		// Token: 0x04000120 RID: 288
		private volatile IEnumerable<ExportDefinition> _exports;

		// Token: 0x04000121 RID: 289
		private volatile IDictionary<string, object> _metadata;

		// Token: 0x04000122 RID: 290
		private volatile ConstructorInfo _constructor;

		// Token: 0x04000123 RID: 291
		private object _lock = new object();

		// Token: 0x02000072 RID: 114
		[CompilerGenerated]
		private sealed class <GetCandidateParameters>d__21 : IEnumerable<Type[]>, IEnumerable, IEnumerator<Type[]>, IDisposable, IEnumerator
		{
			// Token: 0x060002F0 RID: 752 RVA: 0x00008C1F File Offset: 0x00006E1F
			[DebuggerHidden]
			public <GetCandidateParameters>d__21(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060002F1 RID: 753 RVA: 0x00008C3C File Offset: 0x00006E3C
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x060002F2 RID: 754 RVA: 0x00008C74 File Offset: 0x00006E74
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					ReflectionComposablePartDefinition reflectionComposablePartDefinition = this;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						enumerator = reflectionComposablePartDefinition.ExportDefinitions.GetEnumerator();
						this.<>1__state = -3;
					}
					while (enumerator.MoveNext())
					{
						int[] value = enumerator.Current.Metadata.GetValue("System.ComponentModel.Composition.GenericExportParametersOrderMetadataName");
						if (value != null && value.Length == genericParameters.Length)
						{
							this.<>2__current = GenericServices.Reorder<Type>(genericParameters, value);
							this.<>1__state = 1;
							return true;
						}
					}
					this.<>m__Finally1();
					enumerator = null;
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x060002F3 RID: 755 RVA: 0x00008D48 File Offset: 0x00006F48
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x170000D5 RID: 213
			// (get) Token: 0x060002F4 RID: 756 RVA: 0x00008D64 File Offset: 0x00006F64
			Type[] IEnumerator<Type[]>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060002F5 RID: 757 RVA: 0x0000266F File Offset: 0x0000086F
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170000D6 RID: 214
			// (get) Token: 0x060002F6 RID: 758 RVA: 0x00008D64 File Offset: 0x00006F64
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060002F7 RID: 759 RVA: 0x00008D6C File Offset: 0x00006F6C
			[DebuggerHidden]
			IEnumerator<Type[]> IEnumerable<Type[]>.GetEnumerator()
			{
				ReflectionComposablePartDefinition.<GetCandidateParameters>d__21 <GetCandidateParameters>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<GetCandidateParameters>d__ = this;
				}
				else
				{
					<GetCandidateParameters>d__ = new ReflectionComposablePartDefinition.<GetCandidateParameters>d__21(0);
					<GetCandidateParameters>d__.<>4__this = this;
				}
				<GetCandidateParameters>d__.genericParameters = genericParameters;
				return <GetCandidateParameters>d__;
			}

			// Token: 0x060002F8 RID: 760 RVA: 0x00008DBB File Offset: 0x00006FBB
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Type[]>.GetEnumerator();
			}

			// Token: 0x04000124 RID: 292
			private int <>1__state;

			// Token: 0x04000125 RID: 293
			private Type[] <>2__current;

			// Token: 0x04000126 RID: 294
			private int <>l__initialThreadId;

			// Token: 0x04000127 RID: 295
			public ReflectionComposablePartDefinition <>4__this;

			// Token: 0x04000128 RID: 296
			private Type[] genericParameters;

			// Token: 0x04000129 RID: 297
			public Type[] <>3__genericParameters;

			// Token: 0x0400012A RID: 298
			private IEnumerator<ExportDefinition> <>7__wrap1;
		}
	}
}
