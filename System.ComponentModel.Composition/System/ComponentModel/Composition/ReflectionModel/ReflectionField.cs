﻿using System;
using System.Reflection;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x02000074 RID: 116
	internal class ReflectionField : ReflectionWritableMember
	{
		// Token: 0x06000303 RID: 771 RVA: 0x00008F81 File Offset: 0x00007181
		public ReflectionField(FieldInfo field)
		{
			Assumes.NotNull<FieldInfo>(field);
			this._field = field;
		}

		// Token: 0x170000D7 RID: 215
		// (get) Token: 0x06000304 RID: 772 RVA: 0x00008F96 File Offset: 0x00007196
		public FieldInfo UndelyingField
		{
			get
			{
				return this._field;
			}
		}

		// Token: 0x170000D8 RID: 216
		// (get) Token: 0x06000305 RID: 773 RVA: 0x00008F9E File Offset: 0x0000719E
		public override MemberInfo UnderlyingMember
		{
			get
			{
				return this.UndelyingField;
			}
		}

		// Token: 0x170000D9 RID: 217
		// (get) Token: 0x06000306 RID: 774 RVA: 0x000052FB File Offset: 0x000034FB
		public override bool CanRead
		{
			get
			{
				return true;
			}
		}

		// Token: 0x170000DA RID: 218
		// (get) Token: 0x06000307 RID: 775 RVA: 0x00008FA6 File Offset: 0x000071A6
		public override bool CanWrite
		{
			get
			{
				return !this.UndelyingField.IsInitOnly;
			}
		}

		// Token: 0x170000DB RID: 219
		// (get) Token: 0x06000308 RID: 776 RVA: 0x00008FB6 File Offset: 0x000071B6
		public override bool RequiresInstance
		{
			get
			{
				return !this.UndelyingField.IsStatic;
			}
		}

		// Token: 0x170000DC RID: 220
		// (get) Token: 0x06000309 RID: 777 RVA: 0x00008FC6 File Offset: 0x000071C6
		public override Type ReturnType
		{
			get
			{
				return this.UndelyingField.FieldType;
			}
		}

		// Token: 0x170000DD RID: 221
		// (get) Token: 0x0600030A RID: 778 RVA: 0x000052FB File Offset: 0x000034FB
		public override ReflectionItemType ItemType
		{
			get
			{
				return ReflectionItemType.Field;
			}
		}

		// Token: 0x0600030B RID: 779 RVA: 0x00008FD3 File Offset: 0x000071D3
		public override object GetValue(object instance)
		{
			return this.UndelyingField.SafeGetValue(instance);
		}

		// Token: 0x0600030C RID: 780 RVA: 0x00008FE1 File Offset: 0x000071E1
		public override void SetValue(object instance, object value)
		{
			this.UndelyingField.SafeSetValue(instance, value);
		}

		// Token: 0x0400012B RID: 299
		private readonly FieldInfo _field;
	}
}
