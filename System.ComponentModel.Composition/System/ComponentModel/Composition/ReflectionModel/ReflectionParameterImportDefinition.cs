﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.Globalization;
using System.Reflection;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x02000083 RID: 131
	internal class ReflectionParameterImportDefinition : ReflectionImportDefinition
	{
		// Token: 0x0600036E RID: 878 RVA: 0x00009AD0 File Offset: 0x00007CD0
		public ReflectionParameterImportDefinition(Lazy<ParameterInfo> importingLazyParameter, string contractName, string requiredTypeIdentity, IEnumerable<KeyValuePair<string, Type>> requiredMetadata, ImportCardinality cardinality, CreationPolicy requiredCreationPolicy, IDictionary<string, object> metadata, ICompositionElement origin) : base(contractName, requiredTypeIdentity, requiredMetadata, cardinality, false, true, requiredCreationPolicy, metadata, origin)
		{
			Assumes.NotNull<Lazy<ParameterInfo>>(importingLazyParameter);
			this._importingLazyParameter = importingLazyParameter;
		}

		// Token: 0x0600036F RID: 879 RVA: 0x00009AFE File Offset: 0x00007CFE
		public override ImportingItem ToImportingItem()
		{
			return new ImportingParameter(this, new ImportType(this.ImportingLazyParameter.GetNotNullValue("parameter").ParameterType, this.Cardinality));
		}

		// Token: 0x17000100 RID: 256
		// (get) Token: 0x06000370 RID: 880 RVA: 0x00009B26 File Offset: 0x00007D26
		public Lazy<ParameterInfo> ImportingLazyParameter
		{
			get
			{
				return this._importingLazyParameter;
			}
		}

		// Token: 0x06000371 RID: 881 RVA: 0x00009B30 File Offset: 0x00007D30
		protected override string GetDisplayName()
		{
			ParameterInfo notNullValue = this.ImportingLazyParameter.GetNotNullValue("parameter");
			return string.Format(CultureInfo.CurrentCulture, "{0} (Parameter=\"{1}\", ContractName=\"{2}\")", notNullValue.Member.GetDisplayName(), notNullValue.Name, this.ContractName);
		}

		// Token: 0x0400014E RID: 334
		private Lazy<ParameterInfo> _importingLazyParameter;
	}
}
