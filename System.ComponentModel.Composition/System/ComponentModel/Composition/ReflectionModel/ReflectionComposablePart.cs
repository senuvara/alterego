﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using Microsoft.Internal;
using Microsoft.Internal.Collections;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x0200006E RID: 110
	internal class ReflectionComposablePart : ComposablePart, ICompositionElement
	{
		// Token: 0x060002B1 RID: 689 RVA: 0x00007D90 File Offset: 0x00005F90
		public ReflectionComposablePart(ReflectionComposablePartDefinition definition)
		{
			Requires.NotNull<ReflectionComposablePartDefinition>(definition, "definition");
			this._definition = definition;
		}

		// Token: 0x060002B2 RID: 690 RVA: 0x00007DE8 File Offset: 0x00005FE8
		public ReflectionComposablePart(ReflectionComposablePartDefinition definition, object attributedPart)
		{
			Requires.NotNull<ReflectionComposablePartDefinition>(definition, "definition");
			Requires.NotNull<object>(attributedPart, "attributedPart");
			this._definition = definition;
			if (attributedPart is ValueType)
			{
				throw new ArgumentException(Strings.ArgumentValueType, "attributedPart");
			}
			this._cachedInstance = attributedPart;
		}

		// Token: 0x060002B3 RID: 691 RVA: 0x00002304 File Offset: 0x00000504
		protected virtual void EnsureRunning()
		{
		}

		// Token: 0x060002B4 RID: 692 RVA: 0x00007E6C File Offset: 0x0000606C
		protected void RequiresRunning()
		{
			this.EnsureRunning();
		}

		// Token: 0x060002B5 RID: 693 RVA: 0x00002304 File Offset: 0x00000504
		protected virtual void ReleaseInstanceIfNecessary(object instance)
		{
		}

		// Token: 0x170000C8 RID: 200
		// (get) Token: 0x060002B6 RID: 694 RVA: 0x00007E74 File Offset: 0x00006074
		protected object CachedInstance
		{
			get
			{
				object @lock = this._lock;
				object cachedInstance;
				lock (@lock)
				{
					cachedInstance = this._cachedInstance;
				}
				return cachedInstance;
			}
		}

		// Token: 0x170000C9 RID: 201
		// (get) Token: 0x060002B7 RID: 695 RVA: 0x00007EB8 File Offset: 0x000060B8
		public ReflectionComposablePartDefinition Definition
		{
			get
			{
				this.RequiresRunning();
				return this._definition;
			}
		}

		// Token: 0x170000CA RID: 202
		// (get) Token: 0x060002B8 RID: 696 RVA: 0x00007EC6 File Offset: 0x000060C6
		public override IDictionary<string, object> Metadata
		{
			get
			{
				this.RequiresRunning();
				return this.Definition.Metadata;
			}
		}

		// Token: 0x170000CB RID: 203
		// (get) Token: 0x060002B9 RID: 697 RVA: 0x00007ED9 File Offset: 0x000060D9
		public sealed override IEnumerable<ImportDefinition> ImportDefinitions
		{
			get
			{
				this.RequiresRunning();
				return this.Definition.ImportDefinitions;
			}
		}

		// Token: 0x170000CC RID: 204
		// (get) Token: 0x060002BA RID: 698 RVA: 0x00007EEC File Offset: 0x000060EC
		public sealed override IEnumerable<ExportDefinition> ExportDefinitions
		{
			get
			{
				this.RequiresRunning();
				return this.Definition.ExportDefinitions;
			}
		}

		// Token: 0x170000CD RID: 205
		// (get) Token: 0x060002BB RID: 699 RVA: 0x00007EFF File Offset: 0x000060FF
		string ICompositionElement.DisplayName
		{
			get
			{
				return this.GetDisplayName();
			}
		}

		// Token: 0x170000CE RID: 206
		// (get) Token: 0x060002BC RID: 700 RVA: 0x00007F07 File Offset: 0x00006107
		ICompositionElement ICompositionElement.Origin
		{
			get
			{
				return this.Definition;
			}
		}

		// Token: 0x060002BD RID: 701 RVA: 0x00007F10 File Offset: 0x00006110
		public override object GetExportedValue(ExportDefinition definition)
		{
			this.RequiresRunning();
			Requires.NotNull<ExportDefinition>(definition, "definition");
			ExportingMember exportingMember = null;
			object @lock = this._lock;
			lock (@lock)
			{
				exportingMember = this.GetExportingMemberFromDefinition(definition);
				if (exportingMember == null)
				{
					throw ExceptionBuilder.CreateExportDefinitionNotOnThisComposablePart("definition");
				}
				this.EnsureGettable();
			}
			return this.GetExportedValue(exportingMember);
		}

		// Token: 0x060002BE RID: 702 RVA: 0x00007F80 File Offset: 0x00006180
		public override void SetImport(ImportDefinition definition, IEnumerable<Export> exports)
		{
			this.RequiresRunning();
			Requires.NotNull<ImportDefinition>(definition, "definition");
			Requires.NotNull<IEnumerable<Export>>(exports, "exports");
			ImportingItem importingItemFromDefinition = this.GetImportingItemFromDefinition(definition);
			if (importingItemFromDefinition == null)
			{
				throw ExceptionBuilder.CreateImportDefinitionNotOnThisComposablePart("definition");
			}
			this.EnsureSettable(definition);
			Export[] exports2 = exports.AsArray<Export>();
			ReflectionComposablePart.EnsureCardinality(definition, exports2);
			this.SetImport(importingItemFromDefinition, exports2);
		}

		// Token: 0x060002BF RID: 703 RVA: 0x00007FDC File Offset: 0x000061DC
		public override void Activate()
		{
			this.RequiresRunning();
			this.SetNonPrerequisiteImports();
			this.NotifyImportSatisfied();
			object @lock = this._lock;
			lock (@lock)
			{
				this._initialCompositionComplete = true;
			}
		}

		// Token: 0x060002C0 RID: 704 RVA: 0x00007EFF File Offset: 0x000060FF
		public override string ToString()
		{
			return this.GetDisplayName();
		}

		// Token: 0x060002C1 RID: 705 RVA: 0x00008030 File Offset: 0x00006230
		private object GetExportedValue(ExportingMember member)
		{
			object instance = null;
			if (member.RequiresInstance)
			{
				instance = this.GetInstanceActivatingIfNeeded();
			}
			return member.GetExportedValue(instance, this._lock);
		}

		// Token: 0x060002C2 RID: 706 RVA: 0x0000805C File Offset: 0x0000625C
		private void SetImport(ImportingItem item, Export[] exports)
		{
			object value = item.CastExportsToImportType(exports);
			object @lock = this._lock;
			lock (@lock)
			{
				this._invokeImportsSatisfied = true;
				this._importValues[item.Definition] = value;
			}
		}

		// Token: 0x060002C3 RID: 707 RVA: 0x000080B8 File Offset: 0x000062B8
		private object GetInstanceActivatingIfNeeded()
		{
			if (this._cachedInstance != null)
			{
				return this._cachedInstance;
			}
			ConstructorInfo constructorInfo = null;
			object[] arguments = null;
			object @lock = this._lock;
			lock (@lock)
			{
				if (!this.RequiresActivation())
				{
					return null;
				}
				constructorInfo = this.Definition.GetConstructor();
				if (constructorInfo == null)
				{
					throw new ComposablePartException(string.Format(CultureInfo.CurrentCulture, Strings.ReflectionModel_PartConstructorMissing, this.Definition.GetPartType().FullName), this.Definition.ToElement());
				}
				arguments = this.GetConstructorArguments();
			}
			object obj = this.CreateInstance(constructorInfo, arguments);
			this.SetPrerequisiteImports();
			@lock = this._lock;
			lock (@lock)
			{
				if (this._cachedInstance == null)
				{
					this._cachedInstance = obj;
					obj = null;
				}
			}
			if (obj == null)
			{
				this.ReleaseInstanceIfNecessary(obj);
			}
			return this._cachedInstance;
		}

		// Token: 0x060002C4 RID: 708 RVA: 0x000081CC File Offset: 0x000063CC
		private object[] GetConstructorArguments()
		{
			ReflectionParameterImportDefinition[] array = this.ImportDefinitions.OfType<ReflectionParameterImportDefinition>().ToArray<ReflectionParameterImportDefinition>();
			object[] arguments = new object[array.Length];
			this.UseImportedValues<ReflectionParameterImportDefinition>(array, delegate(ImportingItem import, ReflectionParameterImportDefinition definition, object value)
			{
				if (definition.Cardinality == ImportCardinality.ZeroOrMore && !import.ImportType.IsAssignableCollectionType)
				{
					throw new ComposablePartException(string.Format(CultureInfo.CurrentCulture, Strings.ReflectionModel_ImportManyOnParameterCanOnlyBeAssigned, this.Definition.GetPartType().FullName, definition.ImportingLazyParameter.Value.Name), this.Definition.ToElement());
				}
				arguments[definition.ImportingLazyParameter.Value.Position] = value;
			}, true);
			return arguments;
		}

		// Token: 0x060002C5 RID: 709 RVA: 0x0000821F File Offset: 0x0000641F
		private bool RequiresActivation()
		{
			return this.ImportDefinitions.Any<ImportDefinition>() || this.ExportDefinitions.Any((ExportDefinition definition) => this.GetExportingMemberFromDefinition(definition).RequiresInstance);
		}

		// Token: 0x060002C6 RID: 710 RVA: 0x00008248 File Offset: 0x00006448
		private void EnsureGettable()
		{
			if (this._initialCompositionComplete)
			{
				return;
			}
			foreach (ImportDefinition importDefinition in from definition in this.ImportDefinitions
			where definition.IsPrerequisite
			select definition)
			{
				if (!this._importValues.ContainsKey(importDefinition))
				{
					throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, Strings.InvalidOperation_GetExportedValueBeforePrereqImportSet, importDefinition.ToElement().DisplayName));
				}
			}
		}

		// Token: 0x060002C7 RID: 711 RVA: 0x000082EC File Offset: 0x000064EC
		private void EnsureSettable(ImportDefinition definition)
		{
			object @lock = this._lock;
			lock (@lock)
			{
				if (this._initialCompositionComplete && !definition.IsRecomposable)
				{
					throw new InvalidOperationException(Strings.InvalidOperation_DefinitionCannotBeRecomposed);
				}
			}
		}

		// Token: 0x060002C8 RID: 712 RVA: 0x00008344 File Offset: 0x00006544
		private static void EnsureCardinality(ImportDefinition definition, Export[] exports)
		{
			Requires.NullOrNotNullElements<Export>(exports, "exports");
			ExportCardinalityCheckResult exportCardinalityCheckResult = ExportServices.CheckCardinality<Export>(definition, exports);
			if (exportCardinalityCheckResult == ExportCardinalityCheckResult.NoExports)
			{
				throw new ArgumentException(Strings.Argument_ExportsEmpty, "exports");
			}
			if (exportCardinalityCheckResult != ExportCardinalityCheckResult.TooManyExports)
			{
				Assumes.IsTrue(exportCardinalityCheckResult == ExportCardinalityCheckResult.Match);
				return;
			}
			throw new ArgumentException(Strings.Argument_ExportsTooMany, "exports");
		}

		// Token: 0x060002C9 RID: 713 RVA: 0x00008398 File Offset: 0x00006598
		private object CreateInstance(ConstructorInfo constructor, object[] arguments)
		{
			Exception ex = null;
			object result = null;
			try
			{
				result = constructor.SafeInvoke(arguments);
			}
			catch (TypeInitializationException ex)
			{
			}
			catch (TargetInvocationException ex2)
			{
				ex = ex2.InnerException;
			}
			if (ex != null)
			{
				throw new ComposablePartException(string.Format(CultureInfo.CurrentCulture, Strings.ReflectionModel_PartConstructorThrewException, this.Definition.GetPartType().FullName), this.Definition.ToElement(), ex);
			}
			return result;
		}

		// Token: 0x060002CA RID: 714 RVA: 0x00008410 File Offset: 0x00006610
		private void SetNonPrerequisiteImports()
		{
			IEnumerable<ImportDefinition> definitions = from import in this.ImportDefinitions
			where !import.IsPrerequisite
			select import;
			this.UseImportedValues<ImportDefinition>(definitions, new Action<ImportingItem, ImportDefinition, object>(this.SetExportedValueForImport), false);
		}

		// Token: 0x060002CB RID: 715 RVA: 0x0000845C File Offset: 0x0000665C
		private void SetPrerequisiteImports()
		{
			IEnumerable<ImportDefinition> definitions = from import in this.ImportDefinitions
			where import.IsPrerequisite
			select import;
			this.UseImportedValues<ImportDefinition>(definitions, new Action<ImportingItem, ImportDefinition, object>(this.SetExportedValueForImport), false);
		}

		// Token: 0x060002CC RID: 716 RVA: 0x000084A8 File Offset: 0x000066A8
		private void SetExportedValueForImport(ImportingItem import, ImportDefinition definition, object value)
		{
			ImportingMember importingMember = (ImportingMember)import;
			object instanceActivatingIfNeeded = this.GetInstanceActivatingIfNeeded();
			importingMember.SetExportedValue(instanceActivatingIfNeeded, value);
		}

		// Token: 0x060002CD RID: 717 RVA: 0x000084CC File Offset: 0x000066CC
		private void UseImportedValues<TImportDefinition>(IEnumerable<TImportDefinition> definitions, Action<ImportingItem, TImportDefinition, object> useImportValue, bool errorIfMissing) where TImportDefinition : ImportDefinition
		{
			CompositionResult compositionResult = CompositionResult.SucceededResult;
			foreach (TImportDefinition timportDefinition in definitions)
			{
				ImportingItem importingItemFromDefinition = this.GetImportingItemFromDefinition(timportDefinition);
				object arg;
				if (!this.TryGetImportValue(timportDefinition, out arg))
				{
					if (!errorIfMissing)
					{
						continue;
					}
					if (timportDefinition.Cardinality == ImportCardinality.ExactlyOne)
					{
						CompositionError error = CompositionError.Create(CompositionErrorId.ImportNotSetOnPart, Strings.ImportNotSetOnPart, new object[]
						{
							this.Definition.GetPartType().FullName,
							timportDefinition.ToString()
						});
						compositionResult = compositionResult.MergeError(error);
						continue;
					}
					arg = importingItemFromDefinition.CastExportsToImportType(new Export[0]);
				}
				useImportValue(importingItemFromDefinition, timportDefinition, arg);
			}
			compositionResult.ThrowOnErrors();
		}

		// Token: 0x060002CE RID: 718 RVA: 0x000085A8 File Offset: 0x000067A8
		private bool TryGetImportValue(ImportDefinition definition, out object value)
		{
			object @lock = this._lock;
			lock (@lock)
			{
				if (this._importValues.TryGetValue(definition, out value))
				{
					this._importValues.Remove(definition);
					return true;
				}
			}
			value = null;
			return false;
		}

		// Token: 0x060002CF RID: 719 RVA: 0x00008608 File Offset: 0x00006808
		private void NotifyImportSatisfied()
		{
			if (this._invokeImportsSatisfied && !this._invokingImportsSatisfied)
			{
				IPartImportsSatisfiedNotification partImportsSatisfiedNotification = this.GetInstanceActivatingIfNeeded() as IPartImportsSatisfiedNotification;
				if (partImportsSatisfiedNotification != null)
				{
					try
					{
						this._invokingImportsSatisfied = true;
						partImportsSatisfiedNotification.OnImportsSatisfied();
					}
					catch (Exception innerException)
					{
						throw new ComposablePartException(string.Format(CultureInfo.CurrentCulture, Strings.ReflectionModel_PartOnImportsSatisfiedThrewException, this.Definition.GetPartType().FullName), this.Definition.ToElement(), innerException);
					}
					finally
					{
						this._invokingImportsSatisfied = false;
					}
					this._invokeImportsSatisfied = false;
				}
			}
		}

		// Token: 0x060002D0 RID: 720 RVA: 0x000086A0 File Offset: 0x000068A0
		private ExportingMember GetExportingMemberFromDefinition(ExportDefinition definition)
		{
			ReflectionMemberExportDefinition reflectionMemberExportDefinition = definition as ReflectionMemberExportDefinition;
			if (reflectionMemberExportDefinition == null)
			{
				return null;
			}
			int index = reflectionMemberExportDefinition.GetIndex();
			ExportingMember exportingMember;
			if (!this._exportsCache.TryGetValue(index, out exportingMember))
			{
				exportingMember = ReflectionComposablePart.GetExportingMember(definition);
				if (exportingMember != null)
				{
					this._exportsCache[index] = exportingMember;
				}
			}
			return exportingMember;
		}

		// Token: 0x060002D1 RID: 721 RVA: 0x000086E8 File Offset: 0x000068E8
		private ImportingItem GetImportingItemFromDefinition(ImportDefinition definition)
		{
			ImportingItem importingItem;
			if (!this._importsCache.TryGetValue(definition, out importingItem))
			{
				importingItem = ReflectionComposablePart.GetImportingItem(definition);
				if (importingItem != null)
				{
					this._importsCache[definition] = importingItem;
				}
			}
			return importingItem;
		}

		// Token: 0x060002D2 RID: 722 RVA: 0x00008720 File Offset: 0x00006920
		private static ImportingItem GetImportingItem(ImportDefinition definition)
		{
			ReflectionImportDefinition reflectionImportDefinition = definition as ReflectionImportDefinition;
			if (reflectionImportDefinition != null)
			{
				return reflectionImportDefinition.ToImportingItem();
			}
			return null;
		}

		// Token: 0x060002D3 RID: 723 RVA: 0x00008740 File Offset: 0x00006940
		private static ExportingMember GetExportingMember(ExportDefinition definition)
		{
			ReflectionMemberExportDefinition reflectionMemberExportDefinition = definition as ReflectionMemberExportDefinition;
			if (reflectionMemberExportDefinition != null)
			{
				return reflectionMemberExportDefinition.ToExportingMember();
			}
			return null;
		}

		// Token: 0x060002D4 RID: 724 RVA: 0x0000875F File Offset: 0x0000695F
		private string GetDisplayName()
		{
			return this._definition.GetPartType().GetDisplayName();
		}

		// Token: 0x060002D5 RID: 725 RVA: 0x00008771 File Offset: 0x00006971
		[CompilerGenerated]
		private bool <RequiresActivation>b__36_0(ExportDefinition definition)
		{
			return this.GetExportingMemberFromDefinition(definition).RequiresInstance;
		}

		// Token: 0x0400010F RID: 271
		private readonly ReflectionComposablePartDefinition _definition;

		// Token: 0x04000110 RID: 272
		private readonly Dictionary<ImportDefinition, object> _importValues = new Dictionary<ImportDefinition, object>();

		// Token: 0x04000111 RID: 273
		private readonly Dictionary<ImportDefinition, ImportingItem> _importsCache = new Dictionary<ImportDefinition, ImportingItem>();

		// Token: 0x04000112 RID: 274
		private readonly Dictionary<int, ExportingMember> _exportsCache = new Dictionary<int, ExportingMember>();

		// Token: 0x04000113 RID: 275
		private bool _invokeImportsSatisfied = true;

		// Token: 0x04000114 RID: 276
		private bool _invokingImportsSatisfied;

		// Token: 0x04000115 RID: 277
		private bool _initialCompositionComplete;

		// Token: 0x04000116 RID: 278
		private volatile object _cachedInstance;

		// Token: 0x04000117 RID: 279
		private object _lock = new object();

		// Token: 0x0200006F RID: 111
		[CompilerGenerated]
		private sealed class <>c__DisplayClass35_0
		{
			// Token: 0x060002D6 RID: 726 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass35_0()
			{
			}

			// Token: 0x060002D7 RID: 727 RVA: 0x00008780 File Offset: 0x00006980
			internal void <GetConstructorArguments>b__0(ImportingItem import, ReflectionParameterImportDefinition definition, object value)
			{
				if (definition.Cardinality == ImportCardinality.ZeroOrMore && !import.ImportType.IsAssignableCollectionType)
				{
					throw new ComposablePartException(string.Format(CultureInfo.CurrentCulture, Strings.ReflectionModel_ImportManyOnParameterCanOnlyBeAssigned, this.<>4__this.Definition.GetPartType().FullName, definition.ImportingLazyParameter.Value.Name), this.<>4__this.Definition.ToElement());
				}
				this.arguments[definition.ImportingLazyParameter.Value.Position] = value;
			}

			// Token: 0x04000118 RID: 280
			public ReflectionComposablePart <>4__this;

			// Token: 0x04000119 RID: 281
			public object[] arguments;
		}

		// Token: 0x02000070 RID: 112
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x060002D8 RID: 728 RVA: 0x00008805 File Offset: 0x00006A05
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x060002D9 RID: 729 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c()
			{
			}

			// Token: 0x060002DA RID: 730 RVA: 0x00008811 File Offset: 0x00006A11
			internal bool <EnsureGettable>b__37_0(ImportDefinition definition)
			{
				return definition.IsPrerequisite;
			}

			// Token: 0x060002DB RID: 731 RVA: 0x00008819 File Offset: 0x00006A19
			internal bool <SetNonPrerequisiteImports>b__41_0(ImportDefinition import)
			{
				return !import.IsPrerequisite;
			}

			// Token: 0x060002DC RID: 732 RVA: 0x00008811 File Offset: 0x00006A11
			internal bool <SetPrerequisiteImports>b__42_0(ImportDefinition import)
			{
				return import.IsPrerequisite;
			}

			// Token: 0x0400011A RID: 282
			public static readonly ReflectionComposablePart.<>c <>9 = new ReflectionComposablePart.<>c();

			// Token: 0x0400011B RID: 283
			public static Func<ImportDefinition, bool> <>9__37_0;

			// Token: 0x0400011C RID: 284
			public static Func<ImportDefinition, bool> <>9__41_0;

			// Token: 0x0400011D RID: 285
			public static Func<ImportDefinition, bool> <>9__42_0;
		}
	}
}
