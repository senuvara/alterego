﻿using System;
using System.Reflection;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x02000084 RID: 132
	internal class ReflectionProperty : ReflectionWritableMember
	{
		// Token: 0x06000372 RID: 882 RVA: 0x00009B74 File Offset: 0x00007D74
		public ReflectionProperty(MethodInfo getMethod, MethodInfo setMethod)
		{
			Assumes.IsTrue(getMethod != null || setMethod != null);
			this._getMethod = getMethod;
			this._setMethod = setMethod;
		}

		// Token: 0x17000101 RID: 257
		// (get) Token: 0x06000373 RID: 883 RVA: 0x00009BA2 File Offset: 0x00007DA2
		public override MemberInfo UnderlyingMember
		{
			get
			{
				return this.UnderlyingGetMethod ?? this.UnderlyingSetMethod;
			}
		}

		// Token: 0x17000102 RID: 258
		// (get) Token: 0x06000374 RID: 884 RVA: 0x00009BB4 File Offset: 0x00007DB4
		public override bool CanRead
		{
			get
			{
				return this.UnderlyingGetMethod != null;
			}
		}

		// Token: 0x17000103 RID: 259
		// (get) Token: 0x06000375 RID: 885 RVA: 0x00009BC2 File Offset: 0x00007DC2
		public override bool CanWrite
		{
			get
			{
				return this.UnderlyingSetMethod != null;
			}
		}

		// Token: 0x17000104 RID: 260
		// (get) Token: 0x06000376 RID: 886 RVA: 0x00009BD0 File Offset: 0x00007DD0
		public MethodInfo UnderlyingGetMethod
		{
			get
			{
				return this._getMethod;
			}
		}

		// Token: 0x17000105 RID: 261
		// (get) Token: 0x06000377 RID: 887 RVA: 0x00009BD8 File Offset: 0x00007DD8
		public MethodInfo UnderlyingSetMethod
		{
			get
			{
				return this._setMethod;
			}
		}

		// Token: 0x17000106 RID: 262
		// (get) Token: 0x06000378 RID: 888 RVA: 0x00009BE0 File Offset: 0x00007DE0
		public override string Name
		{
			get
			{
				string name = (this.UnderlyingGetMethod ?? this.UnderlyingSetMethod).Name;
				Assumes.IsTrue(name.Length > 4);
				return name.Substring(4);
			}
		}

		// Token: 0x06000379 RID: 889 RVA: 0x00009C0B File Offset: 0x00007E0B
		public override string GetDisplayName()
		{
			return ReflectionServices.GetDisplayName(base.DeclaringType, this.Name);
		}

		// Token: 0x17000107 RID: 263
		// (get) Token: 0x0600037A RID: 890 RVA: 0x00009C1E File Offset: 0x00007E1E
		public override bool RequiresInstance
		{
			get
			{
				return !(this.UnderlyingGetMethod ?? this.UnderlyingSetMethod).IsStatic;
			}
		}

		// Token: 0x17000108 RID: 264
		// (get) Token: 0x0600037B RID: 891 RVA: 0x00009C38 File Offset: 0x00007E38
		public override Type ReturnType
		{
			get
			{
				if (this.UnderlyingGetMethod != null)
				{
					return this.UnderlyingGetMethod.ReturnType;
				}
				ParameterInfo[] parameters = this.UnderlyingSetMethod.GetParameters();
				Assumes.IsTrue(parameters.Length != 0);
				return parameters[parameters.Length - 1].ParameterType;
			}
		}

		// Token: 0x17000109 RID: 265
		// (get) Token: 0x0600037C RID: 892 RVA: 0x000058F0 File Offset: 0x00003AF0
		public override ReflectionItemType ItemType
		{
			get
			{
				return ReflectionItemType.Property;
			}
		}

		// Token: 0x0600037D RID: 893 RVA: 0x00009C74 File Offset: 0x00007E74
		public override object GetValue(object instance)
		{
			Assumes.NotNull<MethodInfo>(this._getMethod);
			return this.UnderlyingGetMethod.SafeInvoke(instance, Array.Empty<object>());
		}

		// Token: 0x0600037E RID: 894 RVA: 0x00009C92 File Offset: 0x00007E92
		public override void SetValue(object instance, object value)
		{
			Assumes.NotNull<MethodInfo>(this._setMethod);
			this.UnderlyingSetMethod.SafeInvoke(instance, new object[]
			{
				value
			});
		}

		// Token: 0x0400014F RID: 335
		private readonly MethodInfo _getMethod;

		// Token: 0x04000150 RID: 336
		private readonly MethodInfo _setMethod;
	}
}
