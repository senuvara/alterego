﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.Globalization;
using System.Reflection;
using Microsoft.Internal;
using Microsoft.Internal.Collections;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x02000067 RID: 103
	internal class ImportingMember : ImportingItem
	{
		// Token: 0x0600028B RID: 651 RVA: 0x000073A9 File Offset: 0x000055A9
		public ImportingMember(ContractBasedImportDefinition definition, ReflectionWritableMember member, ImportType importType) : base(definition, importType)
		{
			Assumes.NotNull<ContractBasedImportDefinition, ReflectionWritableMember>(definition, member);
			this._member = member;
		}

		// Token: 0x0600028C RID: 652 RVA: 0x000073C1 File Offset: 0x000055C1
		public void SetExportedValue(object instance, object value)
		{
			if (this.RequiresCollectionNormalization())
			{
				this.SetCollectionMemberValue(instance, (IEnumerable)value);
				return;
			}
			this.SetSingleMemberValue(instance, value);
		}

		// Token: 0x0600028D RID: 653 RVA: 0x000073E1 File Offset: 0x000055E1
		private bool RequiresCollectionNormalization()
		{
			return base.Definition.Cardinality == ImportCardinality.ZeroOrMore && (!this._member.CanWrite || !base.ImportType.IsAssignableCollectionType);
		}

		// Token: 0x0600028E RID: 654 RVA: 0x00007410 File Offset: 0x00005610
		private void SetSingleMemberValue(object instance, object value)
		{
			this.EnsureWritable();
			try
			{
				this._member.SetValue(instance, value);
			}
			catch (TargetInvocationException ex)
			{
				throw new ComposablePartException(string.Format(CultureInfo.CurrentCulture, Strings.ReflectionModel_ImportThrewException, this._member.GetDisplayName()), base.Definition.ToElement(), ex.InnerException);
			}
			catch (TargetParameterCountException ex2)
			{
				throw new ComposablePartException(string.Format(CultureInfo.CurrentCulture, Strings.ImportNotValidOnIndexers, this._member.GetDisplayName()), base.Definition.ToElement(), ex2.InnerException);
			}
		}

		// Token: 0x0600028F RID: 655 RVA: 0x000074B4 File Offset: 0x000056B4
		private void EnsureWritable()
		{
			if (!this._member.CanWrite)
			{
				throw new ComposablePartException(string.Format(CultureInfo.CurrentCulture, Strings.ReflectionModel_ImportNotWritable, this._member.GetDisplayName()), base.Definition.ToElement());
			}
		}

		// Token: 0x06000290 RID: 656 RVA: 0x000074F0 File Offset: 0x000056F0
		private void SetCollectionMemberValue(object instance, IEnumerable values)
		{
			Assumes.NotNull<IEnumerable>(values);
			ICollection<object> collection = null;
			Type collectionElementType = CollectionServices.GetCollectionElementType(base.ImportType.ActualType);
			if (collectionElementType != null)
			{
				collection = this.GetNormalizedCollection(collectionElementType, instance);
			}
			this.EnsureCollectionIsWritable(collection);
			this.PopulateCollection(collection, values);
		}

		// Token: 0x06000291 RID: 657 RVA: 0x00007538 File Offset: 0x00005738
		private ICollection<object> GetNormalizedCollection(Type itemType, object instance)
		{
			Assumes.NotNull<Type>(itemType);
			object obj = null;
			if (this._member.CanRead)
			{
				try
				{
					obj = this._member.GetValue(instance);
				}
				catch (TargetInvocationException ex)
				{
					throw new ComposablePartException(string.Format(CultureInfo.CurrentCulture, Strings.ReflectionModel_ImportCollectionGetThrewException, this._member.GetDisplayName()), base.Definition.ToElement(), ex.InnerException);
				}
			}
			if (obj == null)
			{
				ConstructorInfo constructor = base.ImportType.ActualType.GetConstructor(Type.EmptyTypes);
				if (constructor != null)
				{
					try
					{
						obj = constructor.SafeInvoke(Array.Empty<object>());
					}
					catch (TargetInvocationException ex2)
					{
						throw new ComposablePartException(string.Format(CultureInfo.CurrentCulture, Strings.ReflectionModel_ImportCollectionConstructionThrewException, this._member.GetDisplayName(), base.ImportType.ActualType.FullName), base.Definition.ToElement(), ex2.InnerException);
					}
					this.SetSingleMemberValue(instance, obj);
				}
			}
			if (obj == null)
			{
				throw new ComposablePartException(string.Format(CultureInfo.CurrentCulture, Strings.ReflectionModel_ImportCollectionNull, this._member.GetDisplayName()), base.Definition.ToElement());
			}
			return CollectionServices.GetCollectionWrapper(itemType, obj);
		}

		// Token: 0x06000292 RID: 658 RVA: 0x00007668 File Offset: 0x00005868
		private void EnsureCollectionIsWritable(ICollection<object> collection)
		{
			bool flag = true;
			try
			{
				if (collection != null)
				{
					flag = collection.IsReadOnly;
				}
			}
			catch (Exception innerException)
			{
				throw new ComposablePartException(string.Format(CultureInfo.CurrentCulture, Strings.ReflectionModel_ImportCollectionIsReadOnlyThrewException, this._member.GetDisplayName(), collection.GetType().FullName), base.Definition.ToElement(), innerException);
			}
			if (flag)
			{
				throw new ComposablePartException(string.Format(CultureInfo.CurrentCulture, Strings.ReflectionModel_ImportCollectionNotWritable, this._member.GetDisplayName()), base.Definition.ToElement());
			}
		}

		// Token: 0x06000293 RID: 659 RVA: 0x000076FC File Offset: 0x000058FC
		private void PopulateCollection(ICollection<object> collection, IEnumerable values)
		{
			Assumes.NotNull<ICollection<object>, IEnumerable>(collection, values);
			try
			{
				collection.Clear();
			}
			catch (Exception innerException)
			{
				throw new ComposablePartException(string.Format(CultureInfo.CurrentCulture, Strings.ReflectionModel_ImportCollectionClearThrewException, this._member.GetDisplayName(), collection.GetType().FullName), base.Definition.ToElement(), innerException);
			}
			foreach (object item in values)
			{
				try
				{
					collection.Add(item);
				}
				catch (Exception innerException2)
				{
					throw new ComposablePartException(string.Format(CultureInfo.CurrentCulture, Strings.ReflectionModel_ImportCollectionAddThrewException, this._member.GetDisplayName(), collection.GetType().FullName), base.Definition.ToElement(), innerException2);
				}
			}
		}

		// Token: 0x04000103 RID: 259
		private readonly ReflectionWritableMember _member;
	}
}
