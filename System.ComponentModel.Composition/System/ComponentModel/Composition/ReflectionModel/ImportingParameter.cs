﻿using System;
using System.ComponentModel.Composition.Primitives;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x02000068 RID: 104
	internal class ImportingParameter : ImportingItem
	{
		// Token: 0x06000294 RID: 660 RVA: 0x000077E8 File Offset: 0x000059E8
		public ImportingParameter(ContractBasedImportDefinition definition, ImportType importType) : base(definition, importType)
		{
		}
	}
}
