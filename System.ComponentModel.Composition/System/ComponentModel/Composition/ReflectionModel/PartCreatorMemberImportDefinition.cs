﻿using System;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Linq.Expressions;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x0200006C RID: 108
	internal class PartCreatorMemberImportDefinition : ReflectionMemberImportDefinition, IPartCreatorImportDefinition
	{
		// Token: 0x060002A9 RID: 681 RVA: 0x00007C9C File Offset: 0x00005E9C
		public PartCreatorMemberImportDefinition(LazyMemberInfo importingLazyMember, ICompositionElement origin, ContractBasedImportDefinition productImportDefinition) : base(importingLazyMember, "System.ComponentModel.Composition.Contracts.ExportFactory", CompositionConstants.PartCreatorTypeIdentity, productImportDefinition.RequiredMetadata, productImportDefinition.Cardinality, productImportDefinition.IsRecomposable, false, productImportDefinition.RequiredCreationPolicy, MetadataServices.EmptyMetadata, origin)
		{
			Assumes.NotNull<ContractBasedImportDefinition>(productImportDefinition);
			this._productImportDefinition = productImportDefinition;
		}

		// Token: 0x170000C4 RID: 196
		// (get) Token: 0x060002AA RID: 682 RVA: 0x00007CE6 File Offset: 0x00005EE6
		public ContractBasedImportDefinition ProductImportDefinition
		{
			get
			{
				return this._productImportDefinition;
			}
		}

		// Token: 0x060002AB RID: 683 RVA: 0x00007CEE File Offset: 0x00005EEE
		public override bool IsConstraintSatisfiedBy(ExportDefinition exportDefinition)
		{
			return base.IsConstraintSatisfiedBy(exportDefinition) && PartCreatorExportDefinition.IsProductConstraintSatisfiedBy(this._productImportDefinition, exportDefinition);
		}

		// Token: 0x170000C5 RID: 197
		// (get) Token: 0x060002AC RID: 684 RVA: 0x00007D07 File Offset: 0x00005F07
		public override Expression<Func<ExportDefinition, bool>> Constraint
		{
			get
			{
				return ConstraintServices.CreatePartCreatorConstraint(base.Constraint, this._productImportDefinition);
			}
		}

		// Token: 0x0400010D RID: 269
		private readonly ContractBasedImportDefinition _productImportDefinition;
	}
}
