﻿using System;
using System.ComponentModel.Composition.Primitives;
using System.Globalization;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x02000066 RID: 102
	internal abstract class ImportingItem
	{
		// Token: 0x06000283 RID: 643 RVA: 0x00007259 File Offset: 0x00005459
		protected ImportingItem(ContractBasedImportDefinition definition, ImportType importType)
		{
			Assumes.NotNull<ContractBasedImportDefinition>(definition);
			this._definition = definition;
			this._importType = importType;
		}

		// Token: 0x170000BF RID: 191
		// (get) Token: 0x06000284 RID: 644 RVA: 0x00007275 File Offset: 0x00005475
		public ContractBasedImportDefinition Definition
		{
			get
			{
				return this._definition;
			}
		}

		// Token: 0x170000C0 RID: 192
		// (get) Token: 0x06000285 RID: 645 RVA: 0x0000727D File Offset: 0x0000547D
		public ImportType ImportType
		{
			get
			{
				return this._importType;
			}
		}

		// Token: 0x06000286 RID: 646 RVA: 0x00007285 File Offset: 0x00005485
		public object CastExportsToImportType(Export[] exports)
		{
			if (this.Definition.Cardinality == ImportCardinality.ZeroOrMore)
			{
				return this.CastExportsToCollectionImportType(exports);
			}
			return this.CastExportsToSingleImportType(exports);
		}

		// Token: 0x06000287 RID: 647 RVA: 0x000072A4 File Offset: 0x000054A4
		private object CastExportsToCollectionImportType(Export[] exports)
		{
			Assumes.NotNull<Export[]>(exports);
			Type type = this.ImportType.ElementType ?? typeof(object);
			Array array = Array.CreateInstance(type, exports.Length);
			for (int i = 0; i < array.Length; i++)
			{
				object value = this.CastSingleExportToImportType(type, exports[i]);
				array.SetValue(value, i);
			}
			return array;
		}

		// Token: 0x06000288 RID: 648 RVA: 0x00007300 File Offset: 0x00005500
		private object CastExportsToSingleImportType(Export[] exports)
		{
			Assumes.NotNull<Export[]>(exports);
			Assumes.IsTrue(exports.Length < 2);
			if (exports.Length == 0)
			{
				return null;
			}
			return this.CastSingleExportToImportType(this.ImportType.ActualType, exports[0]);
		}

		// Token: 0x06000289 RID: 649 RVA: 0x0000732D File Offset: 0x0000552D
		private object CastSingleExportToImportType(Type type, Export export)
		{
			if (this.ImportType.CastExport != null)
			{
				return this.ImportType.CastExport(export);
			}
			return this.Cast(type, export);
		}

		// Token: 0x0600028A RID: 650 RVA: 0x00007358 File Offset: 0x00005558
		private object Cast(Type type, Export export)
		{
			object value = export.Value;
			object result;
			if (!ContractServices.TryCast(type, value, out result))
			{
				throw new ComposablePartException(string.Format(CultureInfo.CurrentCulture, Strings.ReflectionModel_ImportNotAssignableFromExport, export.ToElement().DisplayName, type.FullName), this.Definition.ToElement());
			}
			return result;
		}

		// Token: 0x04000101 RID: 257
		private readonly ContractBasedImportDefinition _definition;

		// Token: 0x04000102 RID: 258
		private readonly ImportType _importType;
	}
}
