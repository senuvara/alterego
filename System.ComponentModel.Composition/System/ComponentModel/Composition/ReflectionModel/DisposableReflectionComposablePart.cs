﻿using System;
using System.Threading;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x02000051 RID: 81
	internal sealed class DisposableReflectionComposablePart : ReflectionComposablePart, IDisposable
	{
		// Token: 0x06000213 RID: 531 RVA: 0x00005B74 File Offset: 0x00003D74
		public DisposableReflectionComposablePart(ReflectionComposablePartDefinition definition) : base(definition)
		{
		}

		// Token: 0x06000214 RID: 532 RVA: 0x00005B80 File Offset: 0x00003D80
		protected override void ReleaseInstanceIfNecessary(object instance)
		{
			IDisposable disposable = instance as IDisposable;
			if (disposable != null)
			{
				disposable.Dispose();
			}
		}

		// Token: 0x06000215 RID: 533 RVA: 0x00005B9D File Offset: 0x00003D9D
		protected override void EnsureRunning()
		{
			base.EnsureRunning();
			if (this._isDisposed == 1)
			{
				throw ExceptionBuilder.CreateObjectDisposed(this);
			}
		}

		// Token: 0x06000216 RID: 534 RVA: 0x00005BB7 File Offset: 0x00003DB7
		void IDisposable.Dispose()
		{
			if (Interlocked.CompareExchange(ref this._isDisposed, 1, 0) == 0)
			{
				this.ReleaseInstanceIfNecessary(base.CachedInstance);
			}
		}

		// Token: 0x040000C8 RID: 200
		private volatile int _isDisposed;
	}
}
