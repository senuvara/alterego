﻿using System;
using System.ComponentModel.Composition.Primitives;
using System.Reflection;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x0200007B RID: 123
	internal class ReflectionMethod : ReflectionMember
	{
		// Token: 0x0600032E RID: 814 RVA: 0x000091AC File Offset: 0x000073AC
		public ReflectionMethod(MethodInfo method)
		{
			Assumes.NotNull<MethodInfo>(method);
			this._method = method;
		}

		// Token: 0x170000EE RID: 238
		// (get) Token: 0x0600032F RID: 815 RVA: 0x000091C1 File Offset: 0x000073C1
		public MethodInfo UnderlyingMethod
		{
			get
			{
				return this._method;
			}
		}

		// Token: 0x170000EF RID: 239
		// (get) Token: 0x06000330 RID: 816 RVA: 0x000091C9 File Offset: 0x000073C9
		public override MemberInfo UnderlyingMember
		{
			get
			{
				return this.UnderlyingMethod;
			}
		}

		// Token: 0x170000F0 RID: 240
		// (get) Token: 0x06000331 RID: 817 RVA: 0x000052FB File Offset: 0x000034FB
		public override bool CanRead
		{
			get
			{
				return true;
			}
		}

		// Token: 0x170000F1 RID: 241
		// (get) Token: 0x06000332 RID: 818 RVA: 0x000091D1 File Offset: 0x000073D1
		public override bool RequiresInstance
		{
			get
			{
				return !this.UnderlyingMethod.IsStatic;
			}
		}

		// Token: 0x170000F2 RID: 242
		// (get) Token: 0x06000333 RID: 819 RVA: 0x000091E1 File Offset: 0x000073E1
		public override Type ReturnType
		{
			get
			{
				return this.UnderlyingMethod.ReturnType;
			}
		}

		// Token: 0x170000F3 RID: 243
		// (get) Token: 0x06000334 RID: 820 RVA: 0x000091EE File Offset: 0x000073EE
		public override ReflectionItemType ItemType
		{
			get
			{
				return ReflectionItemType.Method;
			}
		}

		// Token: 0x06000335 RID: 821 RVA: 0x000091F1 File Offset: 0x000073F1
		public override object GetValue(object instance)
		{
			return ReflectionMethod.SafeCreateExportedDelegate(instance, this._method);
		}

		// Token: 0x06000336 RID: 822 RVA: 0x000091FF File Offset: 0x000073FF
		private static ExportedDelegate SafeCreateExportedDelegate(object instance, MethodInfo method)
		{
			ReflectionInvoke.DemandMemberAccessIfNeeded(method);
			return new ExportedDelegate(instance, method);
		}

		// Token: 0x04000138 RID: 312
		private readonly MethodInfo _method;
	}
}
