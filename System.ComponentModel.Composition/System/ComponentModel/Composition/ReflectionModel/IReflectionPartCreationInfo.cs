﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.Reflection;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x02000064 RID: 100
	internal interface IReflectionPartCreationInfo : ICompositionElement
	{
		// Token: 0x0600026B RID: 619
		Type GetPartType();

		// Token: 0x0600026C RID: 620
		Lazy<Type> GetLazyPartType();

		// Token: 0x0600026D RID: 621
		ConstructorInfo GetConstructor();

		// Token: 0x0600026E RID: 622
		IDictionary<string, object> GetMetadata();

		// Token: 0x0600026F RID: 623
		IEnumerable<ExportDefinition> GetExports();

		// Token: 0x06000270 RID: 624
		IEnumerable<ImportDefinition> GetImports();

		// Token: 0x170000B7 RID: 183
		// (get) Token: 0x06000271 RID: 625
		bool IsDisposalRequired { get; }
	}
}
