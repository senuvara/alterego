﻿using System;
using System.ComponentModel.Composition.Primitives;
using System.Globalization;
using System.Reflection;
using System.Threading;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x02000059 RID: 89
	internal class ExportingMember
	{
		// Token: 0x0600022D RID: 557 RVA: 0x00005F6F File Offset: 0x0000416F
		public ExportingMember(ExportDefinition definition, ReflectionMember member)
		{
			Assumes.NotNull<ExportDefinition, ReflectionMember>(definition, member);
			this._definition = definition;
			this._member = member;
		}

		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x0600022E RID: 558 RVA: 0x00005F8C File Offset: 0x0000418C
		public bool RequiresInstance
		{
			get
			{
				return this._member.RequiresInstance;
			}
		}

		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x0600022F RID: 559 RVA: 0x00005F99 File Offset: 0x00004199
		public ExportDefinition Definition
		{
			get
			{
				return this._definition;
			}
		}

		// Token: 0x06000230 RID: 560 RVA: 0x00005FA4 File Offset: 0x000041A4
		public object GetExportedValue(object instance, object @lock)
		{
			this.EnsureReadable();
			if (!this._isValueCached)
			{
				object value;
				try
				{
					value = this._member.GetValue(instance);
				}
				catch (TargetInvocationException ex)
				{
					throw new ComposablePartException(string.Format(CultureInfo.CurrentCulture, Strings.ReflectionModel_ExportThrewException, this._member.GetDisplayName()), this.Definition.ToElement(), ex.InnerException);
				}
				catch (TargetParameterCountException ex2)
				{
					throw new ComposablePartException(string.Format(CultureInfo.CurrentCulture, Strings.ExportNotValidOnIndexers, this._member.GetDisplayName()), this.Definition.ToElement(), ex2.InnerException);
				}
				lock (@lock)
				{
					if (!this._isValueCached)
					{
						this._cachedValue = value;
						Thread.MemoryBarrier();
						this._isValueCached = true;
					}
				}
			}
			return this._cachedValue;
		}

		// Token: 0x06000231 RID: 561 RVA: 0x000060A0 File Offset: 0x000042A0
		private void EnsureReadable()
		{
			if (!this._member.CanRead)
			{
				throw new ComposablePartException(string.Format(CultureInfo.CurrentCulture, Strings.ReflectionModel_ExportNotReadable, this._member.GetDisplayName()), this.Definition.ToElement());
			}
		}

		// Token: 0x040000D6 RID: 214
		private readonly ExportDefinition _definition;

		// Token: 0x040000D7 RID: 215
		private readonly ReflectionMember _member;

		// Token: 0x040000D8 RID: 216
		private object _cachedValue;

		// Token: 0x040000D9 RID: 217
		private volatile bool _isValueCached;
	}
}
