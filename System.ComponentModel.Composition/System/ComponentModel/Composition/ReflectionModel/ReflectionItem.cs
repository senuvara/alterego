﻿using System;

namespace System.ComponentModel.Composition.ReflectionModel
{
	// Token: 0x02000076 RID: 118
	internal abstract class ReflectionItem
	{
		// Token: 0x170000E0 RID: 224
		// (get) Token: 0x06000312 RID: 786
		public abstract string Name { get; }

		// Token: 0x06000313 RID: 787
		public abstract string GetDisplayName();

		// Token: 0x170000E1 RID: 225
		// (get) Token: 0x06000314 RID: 788
		public abstract Type ReturnType { get; }

		// Token: 0x170000E2 RID: 226
		// (get) Token: 0x06000315 RID: 789
		public abstract ReflectionItemType ItemType { get; }

		// Token: 0x06000316 RID: 790 RVA: 0x000025B0 File Offset: 0x000007B0
		protected ReflectionItem()
		{
		}
	}
}
