﻿using System;

namespace System.ComponentModel.Composition
{
	/// <summary>Specifies values that indicate how the MEF composition engine searches for imports.</summary>
	// Token: 0x02000047 RID: 71
	public enum ImportSource
	{
		/// <summary>Imports may be satisfied from the current scope or any ancestor scope.</summary>
		// Token: 0x040000BE RID: 190
		Any,
		/// <summary>Imports may be satisfied only from the current scope.</summary>
		// Token: 0x040000BF RID: 191
		Local,
		/// <summary>Imports may be satisfied only from an ancestor scope.</summary>
		// Token: 0x040000C0 RID: 192
		NonLocal
	}
}
