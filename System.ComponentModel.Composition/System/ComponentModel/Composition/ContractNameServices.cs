﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Microsoft.Internal;

namespace System.ComponentModel.Composition
{
	// Token: 0x02000030 RID: 48
	internal static class ContractNameServices
	{
		// Token: 0x1700008F RID: 143
		// (get) Token: 0x06000175 RID: 373 RVA: 0x00004A8A File Offset: 0x00002C8A
		private static Dictionary<Type, string> TypeIdentityCache
		{
			get
			{
				return ContractNameServices.typeIdentityCache = (ContractNameServices.typeIdentityCache ?? new Dictionary<Type, string>());
			}
		}

		// Token: 0x06000176 RID: 374 RVA: 0x00004AA0 File Offset: 0x00002CA0
		internal static string GetTypeIdentity(Type type)
		{
			return ContractNameServices.GetTypeIdentity(type, true);
		}

		// Token: 0x06000177 RID: 375 RVA: 0x00004AAC File Offset: 0x00002CAC
		internal static string GetTypeIdentity(Type type, bool formatGenericName)
		{
			Assumes.NotNull<Type>(type);
			string text = null;
			if (!ContractNameServices.TypeIdentityCache.TryGetValue(type, out text))
			{
				if (!type.IsAbstract && type.IsSubclassOf(typeof(Delegate)))
				{
					text = ContractNameServices.GetTypeIdentityFromMethod(type.GetMethod("Invoke"));
				}
				else if (type.IsGenericParameter)
				{
					StringBuilder stringBuilder = new StringBuilder();
					ContractNameServices.WriteTypeArgument(stringBuilder, false, type, formatGenericName);
					stringBuilder.Remove(stringBuilder.Length - 1, 1);
					text = stringBuilder.ToString();
				}
				else
				{
					StringBuilder stringBuilder2 = new StringBuilder();
					ContractNameServices.WriteTypeWithNamespace(stringBuilder2, type, formatGenericName);
					text = stringBuilder2.ToString();
				}
				Assumes.IsTrue(!string.IsNullOrEmpty(text));
				ContractNameServices.TypeIdentityCache.Add(type, text);
			}
			return text;
		}

		// Token: 0x06000178 RID: 376 RVA: 0x00004B5C File Offset: 0x00002D5C
		internal static string GetTypeIdentityFromMethod(MethodInfo method)
		{
			return ContractNameServices.GetTypeIdentityFromMethod(method, true);
		}

		// Token: 0x06000179 RID: 377 RVA: 0x00004B68 File Offset: 0x00002D68
		internal static string GetTypeIdentityFromMethod(MethodInfo method, bool formatGenericName)
		{
			StringBuilder stringBuilder = new StringBuilder();
			ContractNameServices.WriteTypeWithNamespace(stringBuilder, method.ReturnType, formatGenericName);
			stringBuilder.Append("(");
			ParameterInfo[] parameters = method.GetParameters();
			for (int i = 0; i < parameters.Length; i++)
			{
				if (i != 0)
				{
					stringBuilder.Append(",");
				}
				ContractNameServices.WriteTypeWithNamespace(stringBuilder, parameters[i].ParameterType, formatGenericName);
			}
			stringBuilder.Append(")");
			return stringBuilder.ToString();
		}

		// Token: 0x0600017A RID: 378 RVA: 0x00004BD9 File Offset: 0x00002DD9
		private static void WriteTypeWithNamespace(StringBuilder typeName, Type type, bool formatGenericName)
		{
			if (!string.IsNullOrEmpty(type.Namespace))
			{
				typeName.Append(type.Namespace);
				typeName.Append('.');
			}
			ContractNameServices.WriteType(typeName, type, formatGenericName);
		}

		// Token: 0x0600017B RID: 379 RVA: 0x00004C08 File Offset: 0x00002E08
		private static void WriteType(StringBuilder typeName, Type type, bool formatGenericName)
		{
			if (type.IsGenericType)
			{
				Queue<Type> queue = new Queue<Type>(type.GetGenericArguments());
				ContractNameServices.WriteGenericType(typeName, type, type.IsGenericTypeDefinition, queue, formatGenericName);
				Assumes.IsTrue(queue.Count == 0, "Expecting genericTypeArguments queue to be empty.");
				return;
			}
			ContractNameServices.WriteNonGenericType(typeName, type, formatGenericName);
		}

		// Token: 0x0600017C RID: 380 RVA: 0x00004C54 File Offset: 0x00002E54
		private static void WriteNonGenericType(StringBuilder typeName, Type type, bool formatGenericName)
		{
			if (type.DeclaringType != null)
			{
				ContractNameServices.WriteType(typeName, type.DeclaringType, formatGenericName);
				typeName.Append('+');
			}
			if (type.IsArray)
			{
				ContractNameServices.WriteArrayType(typeName, type, formatGenericName);
				return;
			}
			if (type.IsPointer)
			{
				ContractNameServices.WritePointerType(typeName, type, formatGenericName);
				return;
			}
			if (type.IsByRef)
			{
				ContractNameServices.WriteByRefType(typeName, type, formatGenericName);
				return;
			}
			typeName.Append(type.Name);
		}

		// Token: 0x0600017D RID: 381 RVA: 0x00004CC8 File Offset: 0x00002EC8
		private static void WriteArrayType(StringBuilder typeName, Type type, bool formatGenericName)
		{
			Type type2 = ContractNameServices.FindArrayElementType(type);
			ContractNameServices.WriteType(typeName, type2, formatGenericName);
			Type type3 = type;
			do
			{
				ContractNameServices.WriteArrayTypeDimensions(typeName, type3);
			}
			while ((type3 = type3.GetElementType()) != null && type3.IsArray);
		}

		// Token: 0x0600017E RID: 382 RVA: 0x00004D05 File Offset: 0x00002F05
		private static void WritePointerType(StringBuilder typeName, Type type, bool formatGenericName)
		{
			ContractNameServices.WriteType(typeName, type.GetElementType(), formatGenericName);
			typeName.Append('*');
		}

		// Token: 0x0600017F RID: 383 RVA: 0x00004D1D File Offset: 0x00002F1D
		private static void WriteByRefType(StringBuilder typeName, Type type, bool formatGenericName)
		{
			ContractNameServices.WriteType(typeName, type.GetElementType(), formatGenericName);
			typeName.Append('&');
		}

		// Token: 0x06000180 RID: 384 RVA: 0x00004D38 File Offset: 0x00002F38
		private static void WriteArrayTypeDimensions(StringBuilder typeName, Type type)
		{
			typeName.Append('[');
			int arrayRank = type.GetArrayRank();
			for (int i = 1; i < arrayRank; i++)
			{
				typeName.Append(',');
			}
			typeName.Append(']');
		}

		// Token: 0x06000181 RID: 385 RVA: 0x00004D74 File Offset: 0x00002F74
		private static void WriteGenericType(StringBuilder typeName, Type type, bool isDefinition, Queue<Type> genericTypeArguments, bool formatGenericName)
		{
			if (type.DeclaringType != null)
			{
				if (type.DeclaringType.IsGenericType)
				{
					ContractNameServices.WriteGenericType(typeName, type.DeclaringType, isDefinition, genericTypeArguments, formatGenericName);
				}
				else
				{
					ContractNameServices.WriteNonGenericType(typeName, type.DeclaringType, formatGenericName);
				}
				typeName.Append('+');
			}
			ContractNameServices.WriteGenericTypeName(typeName, type, isDefinition, genericTypeArguments, formatGenericName);
		}

		// Token: 0x06000182 RID: 386 RVA: 0x00004DD0 File Offset: 0x00002FD0
		private static void WriteGenericTypeName(StringBuilder typeName, Type type, bool isDefinition, Queue<Type> genericTypeArguments, bool formatGenericName)
		{
			Assumes.IsTrue(type.IsGenericType, "Expecting type to be a generic type");
			int genericArity = ContractNameServices.GetGenericArity(type);
			string value = ContractNameServices.FindGenericTypeName(type.GetGenericTypeDefinition().Name);
			typeName.Append(value);
			ContractNameServices.WriteTypeArgumentsString(typeName, genericArity, isDefinition, genericTypeArguments, formatGenericName);
		}

		// Token: 0x06000183 RID: 387 RVA: 0x00004E18 File Offset: 0x00003018
		private static void WriteTypeArgumentsString(StringBuilder typeName, int argumentsCount, bool isDefinition, Queue<Type> genericTypeArguments, bool formatGenericName)
		{
			if (argumentsCount == 0)
			{
				return;
			}
			typeName.Append('(');
			for (int i = 0; i < argumentsCount; i++)
			{
				Assumes.IsTrue(genericTypeArguments.Count > 0, "Expecting genericTypeArguments to contain at least one Type");
				Type genericTypeArgument = genericTypeArguments.Dequeue();
				ContractNameServices.WriteTypeArgument(typeName, isDefinition, genericTypeArgument, formatGenericName);
			}
			typeName.Remove(typeName.Length - 1, 1);
			typeName.Append(')');
		}

		// Token: 0x06000184 RID: 388 RVA: 0x00004E7C File Offset: 0x0000307C
		private static void WriteTypeArgument(StringBuilder typeName, bool isDefinition, Type genericTypeArgument, bool formatGenericName)
		{
			if (!isDefinition && !genericTypeArgument.IsGenericParameter)
			{
				ContractNameServices.WriteTypeWithNamespace(typeName, genericTypeArgument, formatGenericName);
			}
			if (formatGenericName && genericTypeArgument.IsGenericParameter)
			{
				typeName.Append('{');
				typeName.Append(genericTypeArgument.GenericParameterPosition);
				typeName.Append('}');
			}
			typeName.Append(',');
		}

		// Token: 0x06000185 RID: 389 RVA: 0x00004ED0 File Offset: 0x000030D0
		internal static void WriteCustomModifiers(StringBuilder typeName, string customKeyword, Type[] types, bool formatGenericName)
		{
			typeName.Append(' ');
			typeName.Append(customKeyword);
			Queue<Type> queue = new Queue<Type>(types);
			ContractNameServices.WriteTypeArgumentsString(typeName, types.Length, false, queue, formatGenericName);
			Assumes.IsTrue(queue.Count == 0, "Expecting genericTypeArguments queue to be empty.");
		}

		// Token: 0x06000186 RID: 390 RVA: 0x00004F14 File Offset: 0x00003114
		private static Type FindArrayElementType(Type type)
		{
			Type type2 = type;
			while ((type2 = type2.GetElementType()) != null && type2.IsArray)
			{
			}
			return type2;
		}

		// Token: 0x06000187 RID: 391 RVA: 0x00004F3C File Offset: 0x0000313C
		private static string FindGenericTypeName(string genericName)
		{
			int num = genericName.IndexOf('`');
			if (num > -1)
			{
				genericName = genericName.Substring(0, num);
			}
			return genericName;
		}

		// Token: 0x06000188 RID: 392 RVA: 0x00004F64 File Offset: 0x00003164
		private static int GetGenericArity(Type type)
		{
			if (type.DeclaringType == null)
			{
				return type.GetGenericArguments().Length;
			}
			int num = type.DeclaringType.GetGenericArguments().Length;
			int num2 = type.GetGenericArguments().Length;
			Assumes.IsTrue(num2 >= num);
			return num2 - num;
		}

		// Token: 0x04000086 RID: 134
		private const char NamespaceSeparator = '.';

		// Token: 0x04000087 RID: 135
		private const char ArrayOpeningBracket = '[';

		// Token: 0x04000088 RID: 136
		private const char ArrayClosingBracket = ']';

		// Token: 0x04000089 RID: 137
		private const char ArraySeparator = ',';

		// Token: 0x0400008A RID: 138
		private const char PointerSymbol = '*';

		// Token: 0x0400008B RID: 139
		private const char ReferenceSymbol = '&';

		// Token: 0x0400008C RID: 140
		private const char GenericArityBackQuote = '`';

		// Token: 0x0400008D RID: 141
		private const char NestedClassSeparator = '+';

		// Token: 0x0400008E RID: 142
		private const char ContractNameGenericOpeningBracket = '(';

		// Token: 0x0400008F RID: 143
		private const char ContractNameGenericClosingBracket = ')';

		// Token: 0x04000090 RID: 144
		private const char ContractNameGenericArgumentSeparator = ',';

		// Token: 0x04000091 RID: 145
		private const char CustomModifiersSeparator = ' ';

		// Token: 0x04000092 RID: 146
		private const char GenericFormatOpeningBracket = '{';

		// Token: 0x04000093 RID: 147
		private const char GenericFormatClosingBracket = '}';

		// Token: 0x04000094 RID: 148
		[ThreadStatic]
		private static Dictionary<Type, string> typeIdentityCache;
	}
}
