﻿using System;
using System.ComponentModel.Composition.Primitives;
using System.Globalization;
using Microsoft.Internal;

namespace System.ComponentModel.Composition
{
	// Token: 0x02000033 RID: 51
	internal static class ExceptionBuilder
	{
		// Token: 0x06000191 RID: 401 RVA: 0x0000513D File Offset: 0x0000333D
		public static Exception CreateDiscoveryException(string messageFormat, params string[] arguments)
		{
			return new InvalidOperationException(ExceptionBuilder.Format(messageFormat, arguments));
		}

		// Token: 0x06000192 RID: 402 RVA: 0x0000514B File Offset: 0x0000334B
		public static ArgumentException CreateContainsNullElement(string parameterName)
		{
			Assumes.NotNull<string>(parameterName);
			return new ArgumentException(ExceptionBuilder.Format(Strings.Argument_NullElement, new string[]
			{
				parameterName
			}), parameterName);
		}

		// Token: 0x06000193 RID: 403 RVA: 0x0000516D File Offset: 0x0000336D
		public static ObjectDisposedException CreateObjectDisposed(object instance)
		{
			Assumes.NotNull<object>(instance);
			return new ObjectDisposedException(instance.GetType().ToString());
		}

		// Token: 0x06000194 RID: 404 RVA: 0x00005185 File Offset: 0x00003385
		public static NotImplementedException CreateNotOverriddenByDerived(string memberName)
		{
			Assumes.NotNullOrEmpty(memberName);
			return new NotImplementedException(ExceptionBuilder.Format(Strings.NotImplemented_NotOverriddenByDerived, new string[]
			{
				memberName
			}));
		}

		// Token: 0x06000195 RID: 405 RVA: 0x000051A6 File Offset: 0x000033A6
		public static ArgumentException CreateExportDefinitionNotOnThisComposablePart(string parameterName)
		{
			Assumes.NotNullOrEmpty(parameterName);
			return new ArgumentException(ExceptionBuilder.Format(Strings.ExportDefinitionNotOnThisComposablePart, new string[]
			{
				parameterName
			}), parameterName);
		}

		// Token: 0x06000196 RID: 406 RVA: 0x000051C8 File Offset: 0x000033C8
		public static ArgumentException CreateImportDefinitionNotOnThisComposablePart(string parameterName)
		{
			Assumes.NotNullOrEmpty(parameterName);
			return new ArgumentException(ExceptionBuilder.Format(Strings.ImportDefinitionNotOnThisComposablePart, new string[]
			{
				parameterName
			}), parameterName);
		}

		// Token: 0x06000197 RID: 407 RVA: 0x000051EA File Offset: 0x000033EA
		public static CompositionException CreateCannotGetExportedValue(ComposablePart part, ExportDefinition definition, Exception innerException)
		{
			Assumes.NotNull<ComposablePart, ExportDefinition, Exception>(part, definition, innerException);
			return new CompositionException(ErrorBuilder.CreateCannotGetExportedValue(part, definition, innerException));
		}

		// Token: 0x06000198 RID: 408 RVA: 0x00005201 File Offset: 0x00003401
		public static ArgumentException CreateReflectionModelInvalidPartDefinition(string parameterName, Type partDefinitionType)
		{
			Assumes.NotNullOrEmpty(parameterName);
			Assumes.NotNull<Type>(partDefinitionType);
			return new ArgumentException(string.Format(CultureInfo.CurrentCulture, Strings.ReflectionModel_InvalidPartDefinition, partDefinitionType), parameterName);
		}

		// Token: 0x06000199 RID: 409 RVA: 0x00005225 File Offset: 0x00003425
		public static ArgumentException ExportFactory_TooManyGenericParameters(string typeName)
		{
			Assumes.NotNullOrEmpty(typeName);
			return new ArgumentException(ExceptionBuilder.Format(Strings.ExportFactory_TooManyGenericParameters, new string[]
			{
				typeName
			}), typeName);
		}

		// Token: 0x0600019A RID: 410 RVA: 0x00005247 File Offset: 0x00003447
		private static string Format(string format, params string[] arguments)
		{
			return string.Format(CultureInfo.CurrentCulture, format, arguments);
		}
	}
}
