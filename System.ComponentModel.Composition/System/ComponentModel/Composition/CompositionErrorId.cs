﻿using System;

namespace System.ComponentModel.Composition
{
	// Token: 0x02000026 RID: 38
	internal enum CompositionErrorId
	{
		// Token: 0x0400005E RID: 94
		Unknown,
		// Token: 0x0400005F RID: 95
		InvalidExportMetadata,
		// Token: 0x04000060 RID: 96
		ImportNotSetOnPart,
		// Token: 0x04000061 RID: 97
		ImportEngine_ComposeTookTooManyIterations,
		// Token: 0x04000062 RID: 98
		ImportEngine_ImportCardinalityMismatch,
		// Token: 0x04000063 RID: 99
		ImportEngine_PartCycle,
		// Token: 0x04000064 RID: 100
		ImportEngine_PartCannotSetImport,
		// Token: 0x04000065 RID: 101
		ImportEngine_PartCannotGetExportedValue,
		// Token: 0x04000066 RID: 102
		ImportEngine_PartCannotActivate,
		// Token: 0x04000067 RID: 103
		ImportEngine_PreventedByExistingImport,
		// Token: 0x04000068 RID: 104
		ImportEngine_InvalidStateForRecomposition,
		// Token: 0x04000069 RID: 105
		ReflectionModel_ImportThrewException,
		// Token: 0x0400006A RID: 106
		ReflectionModel_ImportNotAssignableFromExport,
		// Token: 0x0400006B RID: 107
		ReflectionModel_ImportCollectionNull,
		// Token: 0x0400006C RID: 108
		ReflectionModel_ImportCollectionNotWritable,
		// Token: 0x0400006D RID: 109
		ReflectionModel_ImportCollectionConstructionThrewException,
		// Token: 0x0400006E RID: 110
		ReflectionModel_ImportCollectionGetThrewException,
		// Token: 0x0400006F RID: 111
		ReflectionModel_ImportCollectionIsReadOnlyThrewException,
		// Token: 0x04000070 RID: 112
		ReflectionModel_ImportCollectionClearThrewException,
		// Token: 0x04000071 RID: 113
		ReflectionModel_ImportCollectionAddThrewException,
		// Token: 0x04000072 RID: 114
		ReflectionModel_ImportManyOnParameterCanOnlyBeAssigned
	}
}
