﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Globalization;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using Microsoft.Internal;
using Microsoft.Internal.Collections;

namespace System.ComponentModel.Composition
{
	// Token: 0x0200003A RID: 58
	internal static class ExportServices
	{
		// Token: 0x060001B3 RID: 435 RVA: 0x0000539B File Offset: 0x0000359B
		internal static bool IsDefaultMetadataViewType(Type metadataViewType)
		{
			Assumes.NotNull<Type>(metadataViewType);
			return metadataViewType.IsAssignableFrom(ExportServices.DefaultMetadataViewType);
		}

		// Token: 0x060001B4 RID: 436 RVA: 0x000053AE File Offset: 0x000035AE
		internal static bool IsDictionaryConstructorViewType(Type metadataViewType)
		{
			Assumes.NotNull<Type>(metadataViewType);
			return metadataViewType.GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, Type.DefaultBinder, new Type[]
			{
				typeof(IDictionary<string, object>)
			}, new ParameterModifier[0]) != null;
		}

		// Token: 0x060001B5 RID: 437 RVA: 0x000053E4 File Offset: 0x000035E4
		internal static Func<Export, object> CreateStronglyTypedLazyFactory(Type exportType, Type metadataViewType)
		{
			MethodInfo methodInfo;
			if (metadataViewType != null)
			{
				methodInfo = ExportServices._createStronglyTypedLazyOfTM.MakeGenericMethod(new Type[]
				{
					exportType ?? ExportServices.DefaultExportedValueType,
					metadataViewType
				});
			}
			else
			{
				methodInfo = ExportServices._createStronglyTypedLazyOfT.MakeGenericMethod(new Type[]
				{
					exportType ?? ExportServices.DefaultExportedValueType
				});
			}
			Assumes.NotNull<MethodInfo>(methodInfo);
			return (Func<Export, object>)Delegate.CreateDelegate(typeof(Func<Export, object>), methodInfo);
		}

		// Token: 0x060001B6 RID: 438 RVA: 0x0000545C File Offset: 0x0000365C
		internal static Func<Export, Lazy<object, object>> CreateSemiStronglyTypedLazyFactory(Type exportType, Type metadataViewType)
		{
			MethodInfo methodInfo = ExportServices._createSemiStronglyTypedLazy.MakeGenericMethod(new Type[]
			{
				exportType ?? ExportServices.DefaultExportedValueType,
				metadataViewType ?? ExportServices.DefaultMetadataViewType
			});
			Assumes.NotNull<MethodInfo>(methodInfo);
			return (Func<Export, Lazy<object, object>>)Delegate.CreateDelegate(typeof(Func<Export, Lazy<object, object>>), methodInfo);
		}

		// Token: 0x060001B7 RID: 439 RVA: 0x000054B0 File Offset: 0x000036B0
		internal static Lazy<T, M> CreateStronglyTypedLazyOfTM<T, M>(Export export)
		{
			IDisposable disposable = export as IDisposable;
			if (disposable != null)
			{
				return new ExportServices.DisposableLazy<T, M>(() => ExportServices.GetCastedExportedValue<T>(export), AttributedModelServices.GetMetadataView<M>(export.Metadata), disposable, LazyThreadSafetyMode.PublicationOnly);
			}
			return new Lazy<T, M>(() => ExportServices.GetCastedExportedValue<T>(export), AttributedModelServices.GetMetadataView<M>(export.Metadata), LazyThreadSafetyMode.PublicationOnly);
		}

		// Token: 0x060001B8 RID: 440 RVA: 0x00005520 File Offset: 0x00003720
		internal static Lazy<T> CreateStronglyTypedLazyOfT<T>(Export export)
		{
			IDisposable disposable = export as IDisposable;
			if (disposable != null)
			{
				return new ExportServices.DisposableLazy<T>(() => ExportServices.GetCastedExportedValue<T>(export), disposable, LazyThreadSafetyMode.PublicationOnly);
			}
			return new Lazy<T>(() => ExportServices.GetCastedExportedValue<T>(export), LazyThreadSafetyMode.PublicationOnly);
		}

		// Token: 0x060001B9 RID: 441 RVA: 0x00005570 File Offset: 0x00003770
		internal static Lazy<object, object> CreateSemiStronglyTypedLazy<T, M>(Export export)
		{
			IDisposable disposable = export as IDisposable;
			if (disposable != null)
			{
				return new ExportServices.DisposableLazy<object, object>(() => ExportServices.GetCastedExportedValue<T>(export), AttributedModelServices.GetMetadataView<M>(export.Metadata), disposable, LazyThreadSafetyMode.PublicationOnly);
			}
			return new Lazy<object, object>(() => ExportServices.GetCastedExportedValue<T>(export), AttributedModelServices.GetMetadataView<M>(export.Metadata), LazyThreadSafetyMode.PublicationOnly);
		}

		// Token: 0x060001BA RID: 442 RVA: 0x000055E9 File Offset: 0x000037E9
		internal static T GetCastedExportedValue<T>(Export export)
		{
			return ExportServices.CastExportedValue<T>(export.ToElement(), export.Value);
		}

		// Token: 0x060001BB RID: 443 RVA: 0x000055FC File Offset: 0x000037FC
		internal static T CastExportedValue<T>(ICompositionElement element, object exportedValue)
		{
			object obj = null;
			if (!ContractServices.TryCast(typeof(T), exportedValue, out obj))
			{
				throw new CompositionContractMismatchException(string.Format(CultureInfo.CurrentCulture, Strings.ContractMismatch_ExportedValueCannotBeCastToT, element.DisplayName, typeof(T)));
			}
			return (T)((object)obj);
		}

		// Token: 0x060001BC RID: 444 RVA: 0x0000564A File Offset: 0x0000384A
		internal static ExportCardinalityCheckResult CheckCardinality<T>(ImportDefinition definition, IEnumerable<T> enumerable)
		{
			return ExportServices.MatchCardinality((enumerable != null) ? enumerable.GetCardinality<T>() : EnumerableCardinality.Zero, definition.Cardinality);
		}

		// Token: 0x060001BD RID: 445 RVA: 0x00005663 File Offset: 0x00003863
		private static ExportCardinalityCheckResult MatchCardinality(EnumerableCardinality actualCardinality, ImportCardinality importCardinality)
		{
			if (actualCardinality != EnumerableCardinality.Zero)
			{
				if (actualCardinality != EnumerableCardinality.TwoOrMore)
				{
					Assumes.IsTrue(actualCardinality == EnumerableCardinality.One);
				}
				else if (importCardinality.IsAtMostOne())
				{
					return ExportCardinalityCheckResult.TooManyExports;
				}
			}
			else if (importCardinality == ImportCardinality.ExactlyOne)
			{
				return ExportCardinalityCheckResult.NoExports;
			}
			return ExportCardinalityCheckResult.Match;
		}

		// Token: 0x060001BE RID: 446 RVA: 0x00005688 File Offset: 0x00003888
		// Note: this type is marked as 'beforefieldinit'.
		static ExportServices()
		{
		}

		// Token: 0x040000A7 RID: 167
		private static readonly MethodInfo _createStronglyTypedLazyOfTM = typeof(ExportServices).GetMethod("CreateStronglyTypedLazyOfTM", BindingFlags.Static | BindingFlags.NonPublic);

		// Token: 0x040000A8 RID: 168
		private static readonly MethodInfo _createStronglyTypedLazyOfT = typeof(ExportServices).GetMethod("CreateStronglyTypedLazyOfT", BindingFlags.Static | BindingFlags.NonPublic);

		// Token: 0x040000A9 RID: 169
		private static readonly MethodInfo _createSemiStronglyTypedLazy = typeof(ExportServices).GetMethod("CreateSemiStronglyTypedLazy", BindingFlags.Static | BindingFlags.NonPublic);

		// Token: 0x040000AA RID: 170
		internal static readonly Type DefaultMetadataViewType = typeof(IDictionary<string, object>);

		// Token: 0x040000AB RID: 171
		internal static readonly Type DefaultExportedValueType = typeof(object);

		// Token: 0x0200003B RID: 59
		private sealed class DisposableLazy<T, TMetadataView> : Lazy<T, TMetadataView>, IDisposable
		{
			// Token: 0x060001BF RID: 447 RVA: 0x00005704 File Offset: 0x00003904
			public DisposableLazy(Func<T> valueFactory, TMetadataView metadataView, IDisposable disposable, LazyThreadSafetyMode mode) : base(valueFactory, metadataView, mode)
			{
				Assumes.NotNull<IDisposable>(disposable);
				this._disposable = disposable;
			}

			// Token: 0x060001C0 RID: 448 RVA: 0x0000571D File Offset: 0x0000391D
			void IDisposable.Dispose()
			{
				this._disposable.Dispose();
			}

			// Token: 0x040000AC RID: 172
			private IDisposable _disposable;
		}

		// Token: 0x0200003C RID: 60
		private sealed class DisposableLazy<T> : Lazy<T>, IDisposable
		{
			// Token: 0x060001C1 RID: 449 RVA: 0x0000572A File Offset: 0x0000392A
			public DisposableLazy(Func<T> valueFactory, IDisposable disposable, LazyThreadSafetyMode mode) : base(valueFactory, mode)
			{
				Assumes.NotNull<IDisposable>(disposable);
				this._disposable = disposable;
			}

			// Token: 0x060001C2 RID: 450 RVA: 0x00005741 File Offset: 0x00003941
			void IDisposable.Dispose()
			{
				this._disposable.Dispose();
			}

			// Token: 0x040000AD RID: 173
			private IDisposable _disposable;
		}

		// Token: 0x0200003D RID: 61
		[CompilerGenerated]
		private sealed class <>c__DisplayClass11_0<T, M>
		{
			// Token: 0x060001C3 RID: 451 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass11_0()
			{
			}

			// Token: 0x060001C4 RID: 452 RVA: 0x0000574E File Offset: 0x0000394E
			internal T <CreateStronglyTypedLazyOfTM>b__0()
			{
				return ExportServices.GetCastedExportedValue<T>(this.export);
			}

			// Token: 0x060001C5 RID: 453 RVA: 0x0000574E File Offset: 0x0000394E
			internal T <CreateStronglyTypedLazyOfTM>b__1()
			{
				return ExportServices.GetCastedExportedValue<T>(this.export);
			}

			// Token: 0x040000AE RID: 174
			public Export export;
		}

		// Token: 0x0200003E RID: 62
		[CompilerGenerated]
		private sealed class <>c__DisplayClass12_0<T>
		{
			// Token: 0x060001C6 RID: 454 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass12_0()
			{
			}

			// Token: 0x060001C7 RID: 455 RVA: 0x0000575B File Offset: 0x0000395B
			internal T <CreateStronglyTypedLazyOfT>b__0()
			{
				return ExportServices.GetCastedExportedValue<T>(this.export);
			}

			// Token: 0x060001C8 RID: 456 RVA: 0x0000575B File Offset: 0x0000395B
			internal T <CreateStronglyTypedLazyOfT>b__1()
			{
				return ExportServices.GetCastedExportedValue<T>(this.export);
			}

			// Token: 0x040000AF RID: 175
			public Export export;
		}

		// Token: 0x0200003F RID: 63
		[CompilerGenerated]
		private sealed class <>c__DisplayClass13_0<T, M>
		{
			// Token: 0x060001C9 RID: 457 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass13_0()
			{
			}

			// Token: 0x060001CA RID: 458 RVA: 0x00005768 File Offset: 0x00003968
			internal object <CreateSemiStronglyTypedLazy>b__0()
			{
				return ExportServices.GetCastedExportedValue<T>(this.export);
			}

			// Token: 0x060001CB RID: 459 RVA: 0x00005768 File Offset: 0x00003968
			internal object <CreateSemiStronglyTypedLazy>b__1()
			{
				return ExportServices.GetCastedExportedValue<T>(this.export);
			}

			// Token: 0x040000B0 RID: 176
			public Export export;
		}
	}
}
