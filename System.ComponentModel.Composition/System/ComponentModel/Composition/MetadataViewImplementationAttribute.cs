﻿using System;
using System.Runtime.CompilerServices;

namespace System.ComponentModel.Composition
{
	/// <summary>Specifies the type used to implement a metadata view.</summary>
	// Token: 0x0200004C RID: 76
	[AttributeUsage(AttributeTargets.Interface, AllowMultiple = false, Inherited = false)]
	public sealed class MetadataViewImplementationAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.MetadataViewImplementationAttribute" /> class.</summary>
		/// <param name="implementationType">The type of the metadata view.</param>
		// Token: 0x06000204 RID: 516 RVA: 0x00005996 File Offset: 0x00003B96
		public MetadataViewImplementationAttribute(Type implementationType)
		{
			this.ImplementationType = implementationType;
		}

		/// <summary>Gets the type of the metadata view.</summary>
		/// <returns>The type of the metadata view.</returns>
		// Token: 0x170000AC RID: 172
		// (get) Token: 0x06000205 RID: 517 RVA: 0x000059A5 File Offset: 0x00003BA5
		// (set) Token: 0x06000206 RID: 518 RVA: 0x000059AD File Offset: 0x00003BAD
		public Type ImplementationType
		{
			[CompilerGenerated]
			get
			{
				return this.<ImplementationType>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ImplementationType>k__BackingField = value;
			}
		}

		// Token: 0x040000C2 RID: 194
		[CompilerGenerated]
		private Type <ImplementationType>k__BackingField;
	}
}
