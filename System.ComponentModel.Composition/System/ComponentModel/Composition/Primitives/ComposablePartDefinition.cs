﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace System.ComponentModel.Composition.Primitives
{
	/// <summary>Defines an abstract base class for composable part definitions, which describe and enable the creation of <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePart" /> objects.</summary>
	// Token: 0x0200008A RID: 138
	public abstract class ComposablePartDefinition
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartDefinition" /> class.</summary>
		// Token: 0x0600039C RID: 924 RVA: 0x000025B0 File Offset: 0x000007B0
		protected ComposablePartDefinition()
		{
		}

		/// <summary>Gets a collection of <see cref="T:System.ComponentModel.Composition.Primitives.ExportDefinition" /> objects that describe the objects exported by the part defined by this <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartDefinition" /> object.</summary>
		/// <returns>A collection of <see cref="T:System.ComponentModel.Composition.Primitives.ExportDefinition" /> objects that describe the exported objects provided by <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePart" /> objects created by the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartDefinition" />.</returns>
		// Token: 0x17000115 RID: 277
		// (get) Token: 0x0600039D RID: 925
		public abstract IEnumerable<ExportDefinition> ExportDefinitions { get; }

		/// <summary>Gets a collection of <see cref="T:System.ComponentModel.Composition.Primitives.ImportDefinition" /> objects that describe the imports required by the part defined by this <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartDefinition" /> object.</summary>
		/// <returns>A collection of <see cref="T:System.ComponentModel.Composition.Primitives.ImportDefinition" /> objects that describe the imports required by <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePart" /> objects created by the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartDefinition" />.</returns>
		// Token: 0x17000116 RID: 278
		// (get) Token: 0x0600039E RID: 926
		public abstract IEnumerable<ImportDefinition> ImportDefinitions { get; }

		/// <summary>Gets a collection of the metadata for this <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartDefinition" /> object.</summary>
		/// <returns>A collection that contains the metadata for the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartDefinition" />. The default is an empty, read-only <see cref="T:System.Collections.Generic.IDictionary`2" /> object.</returns>
		// Token: 0x17000117 RID: 279
		// (get) Token: 0x0600039F RID: 927 RVA: 0x00009CE1 File Offset: 0x00007EE1
		public virtual IDictionary<string, object> Metadata
		{
			get
			{
				return MetadataServices.EmptyMetadata;
			}
		}

		/// <summary>Creates a new instance of a part that the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartDefinition" /> describes.</summary>
		/// <returns>The created part.</returns>
		// Token: 0x060003A0 RID: 928
		public abstract ComposablePart CreatePart();

		// Token: 0x060003A1 RID: 929 RVA: 0x00009E4C File Offset: 0x0000804C
		internal virtual IEnumerable<Tuple<ComposablePartDefinition, ExportDefinition>> GetExports(ImportDefinition definition)
		{
			List<Tuple<ComposablePartDefinition, ExportDefinition>> list = null;
			foreach (ExportDefinition exportDefinition in this.ExportDefinitions)
			{
				if (definition.IsConstraintSatisfiedBy(exportDefinition))
				{
					if (list == null)
					{
						list = new List<Tuple<ComposablePartDefinition, ExportDefinition>>();
					}
					list.Add(new Tuple<ComposablePartDefinition, ExportDefinition>(this, exportDefinition));
				}
			}
			IEnumerable<Tuple<ComposablePartDefinition, ExportDefinition>> enumerable = list;
			return enumerable ?? ComposablePartDefinition._EmptyExports;
		}

		// Token: 0x060003A2 RID: 930 RVA: 0x00009EC0 File Offset: 0x000080C0
		internal virtual ComposablePartDefinition GetGenericPartDefinition()
		{
			return null;
		}

		// Token: 0x060003A3 RID: 931 RVA: 0x00009EC3 File Offset: 0x000080C3
		// Note: this type is marked as 'beforefieldinit'.
		static ComposablePartDefinition()
		{
		}

		// Token: 0x04000156 RID: 342
		internal static readonly IEnumerable<Tuple<ComposablePartDefinition, ExportDefinition>> _EmptyExports = Enumerable.Empty<Tuple<ComposablePartDefinition, ExportDefinition>>();
	}
}
