﻿using System;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.Primitives
{
	// Token: 0x0200008C RID: 140
	internal class ComposablePartExceptionDebuggerProxy
	{
		// Token: 0x060003AC RID: 940 RVA: 0x00009F4F File Offset: 0x0000814F
		public ComposablePartExceptionDebuggerProxy(ComposablePartException exception)
		{
			Requires.NotNull<ComposablePartException>(exception, "exception");
			this._exception = exception;
		}

		// Token: 0x17000119 RID: 281
		// (get) Token: 0x060003AD RID: 941 RVA: 0x00009F69 File Offset: 0x00008169
		public ICompositionElement Element
		{
			get
			{
				return this._exception.Element;
			}
		}

		// Token: 0x1700011A RID: 282
		// (get) Token: 0x060003AE RID: 942 RVA: 0x00009F76 File Offset: 0x00008176
		public Exception InnerException
		{
			get
			{
				return this._exception.InnerException;
			}
		}

		// Token: 0x1700011B RID: 283
		// (get) Token: 0x060003AF RID: 943 RVA: 0x00009F83 File Offset: 0x00008183
		public string Message
		{
			get
			{
				return this._exception.Message;
			}
		}

		// Token: 0x04000158 RID: 344
		private readonly ComposablePartException _exception;
	}
}
