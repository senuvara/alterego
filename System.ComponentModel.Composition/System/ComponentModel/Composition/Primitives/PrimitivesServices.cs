﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition.ReflectionModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;

namespace System.ComponentModel.Composition.Primitives
{
	// Token: 0x02000098 RID: 152
	internal static class PrimitivesServices
	{
		// Token: 0x060003EE RID: 1006 RVA: 0x0000A78B File Offset: 0x0000898B
		public static bool IsGeneric(this ComposablePartDefinition part)
		{
			return part.Metadata.GetValue("System.ComponentModel.Composition.IsGenericPart");
		}

		// Token: 0x060003EF RID: 1007 RVA: 0x0000A7A0 File Offset: 0x000089A0
		public static ImportDefinition GetProductImportDefinition(this ImportDefinition import)
		{
			IPartCreatorImportDefinition partCreatorImportDefinition = import as IPartCreatorImportDefinition;
			if (partCreatorImportDefinition != null)
			{
				return partCreatorImportDefinition.ProductImportDefinition;
			}
			return import;
		}

		// Token: 0x060003F0 RID: 1008 RVA: 0x0000A7BF File Offset: 0x000089BF
		internal static IEnumerable<string> GetCandidateContractNames(this ImportDefinition import, ComposablePartDefinition part)
		{
			import = import.GetProductImportDefinition();
			string text = import.ContractName;
			string genericContractName = import.Metadata.GetValue("System.ComponentModel.Composition.GenericContractName");
			int[] value = import.Metadata.GetValue("System.ComponentModel.Composition.GenericImportParametersOrderMetadataName");
			if (value != null)
			{
				int value2 = part.Metadata.GetValue("System.ComponentModel.Composition.GenericPartArity");
				if (value2 > 0)
				{
					text = GenericServices.GetGenericName(text, value, value2);
				}
			}
			yield return text;
			if (!string.IsNullOrEmpty(genericContractName))
			{
				yield return genericContractName;
			}
			yield break;
		}

		// Token: 0x060003F1 RID: 1009 RVA: 0x0000A7D6 File Offset: 0x000089D6
		internal static bool IsImportDependentOnPart(this ImportDefinition import, ComposablePartDefinition part, ExportDefinition export, bool expandGenerics)
		{
			import = import.GetProductImportDefinition();
			if (expandGenerics)
			{
				return part.GetExports(import).Any<Tuple<ComposablePartDefinition, ExportDefinition>>();
			}
			return PrimitivesServices.TranslateImport(import, part).IsConstraintSatisfiedBy(export);
		}

		// Token: 0x060003F2 RID: 1010 RVA: 0x0000A800 File Offset: 0x00008A00
		private static ImportDefinition TranslateImport(ImportDefinition import, ComposablePartDefinition part)
		{
			ContractBasedImportDefinition contractBasedImportDefinition = import as ContractBasedImportDefinition;
			if (contractBasedImportDefinition == null)
			{
				return import;
			}
			int[] value = contractBasedImportDefinition.Metadata.GetValue("System.ComponentModel.Composition.GenericImportParametersOrderMetadataName");
			if (value == null)
			{
				return import;
			}
			int value2 = part.Metadata.GetValue("System.ComponentModel.Composition.GenericPartArity");
			if (value2 == 0)
			{
				return import;
			}
			string genericName = GenericServices.GetGenericName(contractBasedImportDefinition.ContractName, value, value2);
			string genericName2 = GenericServices.GetGenericName(contractBasedImportDefinition.RequiredTypeIdentity, value, value2);
			return new ContractBasedImportDefinition(genericName, genericName2, contractBasedImportDefinition.RequiredMetadata, contractBasedImportDefinition.Cardinality, contractBasedImportDefinition.IsRecomposable, false, contractBasedImportDefinition.RequiredCreationPolicy, contractBasedImportDefinition.Metadata);
		}

		// Token: 0x02000099 RID: 153
		[CompilerGenerated]
		private sealed class <GetCandidateContractNames>d__2 : IEnumerable<string>, IEnumerable, IEnumerator<string>, IDisposable, IEnumerator
		{
			// Token: 0x060003F3 RID: 1011 RVA: 0x0000A885 File Offset: 0x00008A85
			[DebuggerHidden]
			public <GetCandidateContractNames>d__2(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060003F4 RID: 1012 RVA: 0x00002304 File Offset: 0x00000504
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x060003F5 RID: 1013 RVA: 0x0000A8A0 File Offset: 0x00008AA0
			bool IEnumerator.MoveNext()
			{
				switch (this.<>1__state)
				{
				case 0:
				{
					this.<>1__state = -1;
					import = import.GetProductImportDefinition();
					string originalGenericName = import.ContractName;
					genericContractName = import.Metadata.GetValue("System.ComponentModel.Composition.GenericContractName");
					int[] value = import.Metadata.GetValue("System.ComponentModel.Composition.GenericImportParametersOrderMetadataName");
					if (value != null)
					{
						int value2 = part.Metadata.GetValue("System.ComponentModel.Composition.GenericPartArity");
						if (value2 > 0)
						{
							originalGenericName = GenericServices.GetGenericName(originalGenericName, value, value2);
						}
					}
					this.<>2__current = originalGenericName;
					this.<>1__state = 1;
					return true;
				}
				case 1:
					this.<>1__state = -1;
					if (!string.IsNullOrEmpty(genericContractName))
					{
						this.<>2__current = genericContractName;
						this.<>1__state = 2;
						return true;
					}
					break;
				case 2:
					this.<>1__state = -1;
					break;
				default:
					return false;
				}
				return false;
			}

			// Token: 0x17000132 RID: 306
			// (get) Token: 0x060003F6 RID: 1014 RVA: 0x0000A984 File Offset: 0x00008B84
			string IEnumerator<string>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060003F7 RID: 1015 RVA: 0x0000266F File Offset: 0x0000086F
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000133 RID: 307
			// (get) Token: 0x060003F8 RID: 1016 RVA: 0x0000A984 File Offset: 0x00008B84
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060003F9 RID: 1017 RVA: 0x0000A98C File Offset: 0x00008B8C
			[DebuggerHidden]
			IEnumerator<string> IEnumerable<string>.GetEnumerator()
			{
				PrimitivesServices.<GetCandidateContractNames>d__2 <GetCandidateContractNames>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<GetCandidateContractNames>d__ = this;
				}
				else
				{
					<GetCandidateContractNames>d__ = new PrimitivesServices.<GetCandidateContractNames>d__2(0);
				}
				<GetCandidateContractNames>d__.import = import;
				<GetCandidateContractNames>d__.part = part;
				return <GetCandidateContractNames>d__;
			}

			// Token: 0x060003FA RID: 1018 RVA: 0x0000A9DB File Offset: 0x00008BDB
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.String>.GetEnumerator();
			}

			// Token: 0x04000175 RID: 373
			private int <>1__state;

			// Token: 0x04000176 RID: 374
			private string <>2__current;

			// Token: 0x04000177 RID: 375
			private int <>l__initialThreadId;

			// Token: 0x04000178 RID: 376
			private ImportDefinition import;

			// Token: 0x04000179 RID: 377
			public ImportDefinition <>3__import;

			// Token: 0x0400017A RID: 378
			private ComposablePartDefinition part;

			// Token: 0x0400017B RID: 379
			public ComposablePartDefinition <>3__part;

			// Token: 0x0400017C RID: 380
			private string <genericContractName>5__1;
		}
	}
}
