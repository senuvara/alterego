﻿using System;
using System.Reflection;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.Primitives
{
	/// <summary>Represents a function exported by a <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePart" />.</summary>
	// Token: 0x02000093 RID: 147
	public class ExportedDelegate
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Primitives.ExportedDelegate" /> class. </summary>
		// Token: 0x060003DA RID: 986 RVA: 0x000025B0 File Offset: 0x000007B0
		protected ExportedDelegate()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Primitives.ExportedDelegate" /> class for the specified part and method. </summary>
		/// <param name="instance">The part exporting the method.</param>
		/// <param name="method">The method to be exported.</param>
		// Token: 0x060003DB RID: 987 RVA: 0x0000A55B File Offset: 0x0000875B
		public ExportedDelegate(object instance, MethodInfo method)
		{
			Requires.NotNull<MethodInfo>(method, "method");
			this._instance = instance;
			this._method = method;
		}

		/// <summary>Gets a delegate of the specified type.</summary>
		/// <param name="delegateType">The type of the delegate to return.</param>
		/// <returns>A delegate of the specified type, or <see langword="null" /> if no such delegate can be created.</returns>
		// Token: 0x060003DC RID: 988 RVA: 0x0000A57C File Offset: 0x0000877C
		public virtual Delegate CreateDelegate(Type delegateType)
		{
			Requires.NotNull<Type>(delegateType, "delegateType");
			if (delegateType == typeof(Delegate) || delegateType == typeof(MulticastDelegate))
			{
				delegateType = this.CreateStandardDelegateType();
			}
			return Delegate.CreateDelegate(delegateType, this._instance, this._method, false);
		}

		// Token: 0x060003DD RID: 989 RVA: 0x0000A5D3 File Offset: 0x000087D3
		private Type CreateStandardDelegateType()
		{
			throw new NotImplementedException();
		}

		// Token: 0x04000167 RID: 359
		private object _instance;

		// Token: 0x04000168 RID: 360
		private MethodInfo _method;
	}
}
