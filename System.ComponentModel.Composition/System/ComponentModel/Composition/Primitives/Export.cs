﻿using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.Primitives
{
	/// <summary>Represents an export, which is a type that consists of a delay-created exported object and the metadata that describes that object.</summary>
	// Token: 0x02000091 RID: 145
	public class Export
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Primitives.Export" /> class.</summary>
		// Token: 0x060003CC RID: 972 RVA: 0x0000A3E8 File Offset: 0x000085E8
		protected Export()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Primitives.Export" /> class with the specified contract name and exported value getter.</summary>
		/// <param name="contractName">The contract name of the <see cref="T:System.ComponentModel.Composition.Primitives.Export" /> object.</param>
		/// <param name="exportedValueGetter">A method that is called to create the exported object of the <see cref="T:System.ComponentModel.Composition.Primitives.Export" />. This delays the creation of the object until the <see cref="P:System.ComponentModel.Composition.Primitives.Export.Value" /> method is called.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="contractName" /> is <see langword="null" />.-or-
		///         <paramref name="exportedObjectGetter" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="contractName" /> is an empty string ("").</exception>
		// Token: 0x060003CD RID: 973 RVA: 0x0000A3FD File Offset: 0x000085FD
		public Export(string contractName, Func<object> exportedValueGetter) : this(new ExportDefinition(contractName, null), exportedValueGetter)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Primitives.Export" /> class with the specified contract name, metadata, and exported value getter.</summary>
		/// <param name="contractName">The contract name of the <see cref="T:System.ComponentModel.Composition.Primitives.Export" /> object.</param>
		/// <param name="metadata">The metadata of the <see cref="T:System.ComponentModel.Composition.Primitives.Export" /> object or <see langword="null" /> to set the <see cref="P:System.ComponentModel.Composition.Primitives.Export.Metadata" /> property to an empty, read-only <see cref="T:System.Collections.Generic.IDictionary`2" /> object.</param>
		/// <param name="exportedValueGetter">A method that is called to create the exported object of the <see cref="T:System.ComponentModel.Composition.Primitives.Export" />. This delays the creation of the object until the <see cref="P:System.ComponentModel.Composition.Primitives.Export.Value" /> method is called.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="contractName" /> is <see langword="null" />.-or-
		///         <paramref name="exportedObjectGetter" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="contractName" /> is an empty string ("").</exception>
		// Token: 0x060003CE RID: 974 RVA: 0x0000A40D File Offset: 0x0000860D
		public Export(string contractName, IDictionary<string, object> metadata, Func<object> exportedValueGetter) : this(new ExportDefinition(contractName, metadata), exportedValueGetter)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Primitives.Export" /> class with the specified export definition and exported object getter.</summary>
		/// <param name="definition">An object that describes the contract that the <see cref="T:System.ComponentModel.Composition.Primitives.Export" /> object satisfies.</param>
		/// <param name="exportedValueGetter">A method that is called to create the exported object of the <see cref="T:System.ComponentModel.Composition.Primitives.Export" />. This delays the creation of the object until the <see cref="P:System.ComponentModel.Composition.Primitives.Export.Value" /> property is called. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="definition" /> is <see langword="null" />.-or-
		///         <paramref name="exportedObjectGetter" /> is <see langword="null" />.</exception>
		// Token: 0x060003CF RID: 975 RVA: 0x0000A41D File Offset: 0x0000861D
		public Export(ExportDefinition definition, Func<object> exportedValueGetter)
		{
			Requires.NotNull<ExportDefinition>(definition, "definition");
			Requires.NotNull<Func<object>>(exportedValueGetter, "exportedValueGetter");
			this._definition = definition;
			this._exportedValueGetter = exportedValueGetter;
		}

		/// <summary>Gets the definition that describes the contract that the export satisfies.</summary>
		/// <returns>A definition that describes the contract that the <see cref="T:System.ComponentModel.Composition.Primitives.Export" /> object satisfies.</returns>
		/// <exception cref="T:System.NotImplementedException">This property was not overridden by a derived class.</exception>
		// Token: 0x17000124 RID: 292
		// (get) Token: 0x060003D0 RID: 976 RVA: 0x0000A456 File Offset: 0x00008656
		public virtual ExportDefinition Definition
		{
			get
			{
				if (this._definition != null)
				{
					return this._definition;
				}
				throw ExceptionBuilder.CreateNotOverriddenByDerived("Definition");
			}
		}

		/// <summary>Gets the metadata for the export.</summary>
		/// <returns>The metadata of the <see cref="T:System.ComponentModel.Composition.Primitives.Export" />.</returns>
		/// <exception cref="T:System.NotImplementedException">The <see cref="P:System.ComponentModel.Composition.Primitives.Export.Definition" /> property was not overridden by a derived class.</exception>
		// Token: 0x17000125 RID: 293
		// (get) Token: 0x060003D1 RID: 977 RVA: 0x0000A471 File Offset: 0x00008671
		public IDictionary<string, object> Metadata
		{
			get
			{
				return this.Definition.Metadata;
			}
		}

		/// <summary>Provides the object this export represents.</summary>
		/// <returns>The object this export represents.</returns>
		// Token: 0x17000126 RID: 294
		// (get) Token: 0x060003D2 RID: 978 RVA: 0x0000A480 File Offset: 0x00008680
		public object Value
		{
			get
			{
				if (this._exportedValue == Export._EmptyValue)
				{
					object exportedValueCore = this.GetExportedValueCore();
					Interlocked.CompareExchange(ref this._exportedValue, exportedValueCore, Export._EmptyValue);
				}
				return this._exportedValue;
			}
		}

		/// <summary>Returns the exported object the export provides.</summary>
		/// <returns>The exported object the export provides.</returns>
		/// <exception cref="T:System.NotImplementedException">The <see cref="M:System.ComponentModel.Composition.Primitives.Export.GetExportedValueCore" /> method was not overridden by a derived class.</exception>
		/// <exception cref="T:System.ComponentModel.Composition.CompositionException">An error occurred during composition. <see cref="P:System.ComponentModel.Composition.CompositionException.Errors" /> will contain a collection of errors that occurred.</exception>
		// Token: 0x060003D3 RID: 979 RVA: 0x0000A4BD File Offset: 0x000086BD
		protected virtual object GetExportedValueCore()
		{
			if (this._exportedValueGetter != null)
			{
				return this._exportedValueGetter();
			}
			throw ExceptionBuilder.CreateNotOverriddenByDerived("GetExportedValueCore");
		}

		// Token: 0x060003D4 RID: 980 RVA: 0x0000A4DD File Offset: 0x000086DD
		// Note: this type is marked as 'beforefieldinit'.
		static Export()
		{
		}

		// Token: 0x04000161 RID: 353
		private readonly ExportDefinition _definition;

		// Token: 0x04000162 RID: 354
		private readonly Func<object> _exportedValueGetter;

		// Token: 0x04000163 RID: 355
		private static readonly object _EmptyValue = new object();

		// Token: 0x04000164 RID: 356
		private volatile object _exportedValue = Export._EmptyValue;
	}
}
