﻿using System;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Security;
using Microsoft.Internal.Runtime.Serialization;

namespace System.ComponentModel.Composition.Primitives
{
	/// <summary>The exception that is thrown when an error occurs when calling methods on a <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePart" /> object.</summary>
	// Token: 0x0200008B RID: 139
	[DebuggerTypeProxy(typeof(ComposablePartExceptionDebuggerProxy))]
	[DebuggerDisplay("{Message}")]
	[Serializable]
	public class ComposablePartException : Exception
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartException" /> class.</summary>
		// Token: 0x060003A4 RID: 932 RVA: 0x00009ECF File Offset: 0x000080CF
		public ComposablePartException() : this(null, null, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartException" /> class with the specified error message.</summary>
		/// <param name="message">A message that describes the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartException" />, or <see langword="null" /> to set the <see cref="P:System.Exception.Message" /> property to its default value.</param>
		// Token: 0x060003A5 RID: 933 RVA: 0x00009EDA File Offset: 0x000080DA
		public ComposablePartException(string message) : this(message, null, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartException" /> class with the specified error message and the composition element that is the cause of the exception.</summary>
		/// <param name="message">A message that describes the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartException" />, or <see langword="null" /> to set the <see cref="P:System.Exception.Message" /> property to its default value.</param>
		/// <param name="element">The composition element that is the cause of the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartException" />, or <see langword="null" /> to set the <see cref="P:System.ComponentModel.Composition.Primitives.ComposablePartException.Element" /> property to <see langword="null" />.</param>
		// Token: 0x060003A6 RID: 934 RVA: 0x00009EE5 File Offset: 0x000080E5
		public ComposablePartException(string message, ICompositionElement element) : this(message, element, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartException" /> class with the specified error message and the exception that is the cause of this exception.</summary>
		/// <param name="message">A message that describes the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartException" />, or <see langword="null" /> to set the <see cref="P:System.Exception.Message" /> property to its default value.</param>
		/// <param name="innerException">The exception that is the underlying cause of the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartException" />, or <see langword="null" /> to set the <see cref="P:System.Exception.InnerException" /> property to <see langword="null" />.</param>
		// Token: 0x060003A7 RID: 935 RVA: 0x00009EF0 File Offset: 0x000080F0
		public ComposablePartException(string message, Exception innerException) : this(message, null, innerException)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartException" /> class with the specified error message, and the composition element and exception that are the cause of this exception.</summary>
		/// <param name="message">A message that describes the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartException" />, or <see langword="null" /> to set the <see cref="P:System.Exception.Message" /> property to its default value.</param>
		/// <param name="element">The composition element that is the cause of the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartException" />, or <see langword="null" /> to set the <see cref="P:System.ComponentModel.Composition.Primitives.ComposablePartException.Element" /> property to <see langword="null" />.</param>
		/// <param name="innerException">The exception that is the underlying cause of the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartException" />, or <see langword="null" /> to set the <see cref="P:System.Exception.InnerException" /> property to <see langword="null" />.</param>
		// Token: 0x060003A8 RID: 936 RVA: 0x00009EFB File Offset: 0x000080FB
		public ComposablePartException(string message, ICompositionElement element, Exception innerException) : base(message, innerException)
		{
			this._element = element;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartException" /> class with the specified serialization data.</summary>
		/// <param name="info">An object that holds the serialized object data for the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartException" />.</param>
		/// <param name="context">An object that contains contextual information about the source or destination.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="info" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">
		///         <paramref name="info" /> is missing a required value.</exception>
		/// <exception cref="T:System.InvalidCastException">
		///         <paramref name="info" /> contains a value that cannot be cast to the correct type.</exception>
		// Token: 0x060003A9 RID: 937 RVA: 0x00009F0C File Offset: 0x0000810C
		[SecuritySafeCritical]
		protected ComposablePartException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this._element = info.GetValue("Element");
		}

		/// <summary>Gets the composition element that is the cause of the exception.</summary>
		/// <returns>The compositional element that is the cause of the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartException" />. The default is <see langword="null" />.</returns>
		// Token: 0x17000118 RID: 280
		// (get) Token: 0x060003AA RID: 938 RVA: 0x00009F27 File Offset: 0x00008127
		public ICompositionElement Element
		{
			get
			{
				return this._element;
			}
		}

		/// <summary>Gets the serialization data for the exception.</summary>
		/// <param name="info">After calling the method, contains serialized object data about the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartException" />.</param>
		/// <param name="context">After calling the method, contains contextual information about the source or destination.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="info" /> is <see langword="null" />.</exception>
		// Token: 0x060003AB RID: 939 RVA: 0x00009F2F File Offset: 0x0000812F
		[SecurityCritical]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("Element", this._element.ToSerializableElement());
		}

		// Token: 0x04000157 RID: 343
		private readonly ICompositionElement _element;
	}
}
