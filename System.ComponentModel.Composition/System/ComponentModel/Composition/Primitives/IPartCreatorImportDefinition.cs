﻿using System;

namespace System.ComponentModel.Composition.Primitives
{
	// Token: 0x02000095 RID: 149
	internal interface IPartCreatorImportDefinition
	{
		// Token: 0x1700012B RID: 299
		// (get) Token: 0x060003E0 RID: 992
		ContractBasedImportDefinition ProductImportDefinition { get; }
	}
}
