﻿using System;
using System.Collections.ObjectModel;
using Microsoft.Internal;
using Microsoft.Internal.Collections;

namespace System.ComponentModel.Composition.Primitives
{
	// Token: 0x02000089 RID: 137
	internal class ComposablePartCatalogDebuggerProxy
	{
		// Token: 0x0600039A RID: 922 RVA: 0x00009E20 File Offset: 0x00008020
		public ComposablePartCatalogDebuggerProxy(ComposablePartCatalog catalog)
		{
			Requires.NotNull<ComposablePartCatalog>(catalog, "catalog");
			this._catalog = catalog;
		}

		// Token: 0x17000114 RID: 276
		// (get) Token: 0x0600039B RID: 923 RVA: 0x00009E3A File Offset: 0x0000803A
		public ReadOnlyCollection<ComposablePartDefinition> Parts
		{
			get
			{
				return this._catalog.Parts.ToReadOnlyCollection<ComposablePartDefinition>();
			}
		}

		// Token: 0x04000155 RID: 341
		private readonly ComposablePartCatalog _catalog;
	}
}
