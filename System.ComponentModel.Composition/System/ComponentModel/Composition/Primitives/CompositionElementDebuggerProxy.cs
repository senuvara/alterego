﻿using System;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.Primitives
{
	// Token: 0x0200008E RID: 142
	internal class CompositionElementDebuggerProxy
	{
		// Token: 0x060003B3 RID: 947 RVA: 0x00009FC4 File Offset: 0x000081C4
		public CompositionElementDebuggerProxy(CompositionElement element)
		{
			Requires.NotNull<CompositionElement>(element, "element");
			this._element = element;
		}

		// Token: 0x1700011D RID: 285
		// (get) Token: 0x060003B4 RID: 948 RVA: 0x00009FDE File Offset: 0x000081DE
		public string DisplayName
		{
			get
			{
				return this._element.DisplayName;
			}
		}

		// Token: 0x1700011E RID: 286
		// (get) Token: 0x060003B5 RID: 949 RVA: 0x00009FEB File Offset: 0x000081EB
		public ICompositionElement Origin
		{
			get
			{
				return this._element.Origin;
			}
		}

		// Token: 0x1700011F RID: 287
		// (get) Token: 0x060003B6 RID: 950 RVA: 0x00009FF8 File Offset: 0x000081F8
		public object UnderlyingObject
		{
			get
			{
				return this._element.UnderlyingObject;
			}
		}

		// Token: 0x0400015B RID: 347
		private readonly CompositionElement _element;
	}
}
