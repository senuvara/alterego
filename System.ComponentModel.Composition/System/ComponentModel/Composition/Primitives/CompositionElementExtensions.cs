﻿using System;

namespace System.ComponentModel.Composition.Primitives
{
	// Token: 0x0200008F RID: 143
	internal static class CompositionElementExtensions
	{
		// Token: 0x060003B7 RID: 951 RVA: 0x0000A005 File Offset: 0x00008205
		public static ICompositionElement ToSerializableElement(this ICompositionElement element)
		{
			return SerializableCompositionElement.FromICompositionElement(element);
		}

		// Token: 0x060003B8 RID: 952 RVA: 0x0000A010 File Offset: 0x00008210
		public static ICompositionElement ToElement(this Export export)
		{
			ICompositionElement compositionElement = export as ICompositionElement;
			if (compositionElement != null)
			{
				return compositionElement;
			}
			return export.Definition.ToElement();
		}

		// Token: 0x060003B9 RID: 953 RVA: 0x0000A034 File Offset: 0x00008234
		public static ICompositionElement ToElement(this ExportDefinition definition)
		{
			return CompositionElementExtensions.ToElementCore(definition);
		}

		// Token: 0x060003BA RID: 954 RVA: 0x0000A034 File Offset: 0x00008234
		public static ICompositionElement ToElement(this ImportDefinition definition)
		{
			return CompositionElementExtensions.ToElementCore(definition);
		}

		// Token: 0x060003BB RID: 955 RVA: 0x0000A034 File Offset: 0x00008234
		public static ICompositionElement ToElement(this ComposablePart part)
		{
			return CompositionElementExtensions.ToElementCore(part);
		}

		// Token: 0x060003BC RID: 956 RVA: 0x0000A034 File Offset: 0x00008234
		public static ICompositionElement ToElement(this ComposablePartDefinition definition)
		{
			return CompositionElementExtensions.ToElementCore(definition);
		}

		// Token: 0x060003BD RID: 957 RVA: 0x0000A03C File Offset: 0x0000823C
		public static string GetDisplayName(this ComposablePartDefinition definition)
		{
			return CompositionElementExtensions.GetDisplayNameCore(definition);
		}

		// Token: 0x060003BE RID: 958 RVA: 0x0000A03C File Offset: 0x0000823C
		public static string GetDisplayName(this ComposablePartCatalog catalog)
		{
			return CompositionElementExtensions.GetDisplayNameCore(catalog);
		}

		// Token: 0x060003BF RID: 959 RVA: 0x0000A044 File Offset: 0x00008244
		private static string GetDisplayNameCore(object value)
		{
			ICompositionElement compositionElement = value as ICompositionElement;
			if (compositionElement != null)
			{
				return compositionElement.DisplayName;
			}
			return value.ToString();
		}

		// Token: 0x060003C0 RID: 960 RVA: 0x0000A068 File Offset: 0x00008268
		private static ICompositionElement ToElementCore(object value)
		{
			ICompositionElement compositionElement = value as ICompositionElement;
			if (compositionElement != null)
			{
				return compositionElement;
			}
			return new CompositionElement(value);
		}
	}
}
