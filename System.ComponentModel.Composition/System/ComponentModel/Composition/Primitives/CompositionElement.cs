﻿using System;
using System.Diagnostics;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.Primitives
{
	// Token: 0x0200008D RID: 141
	[DebuggerTypeProxy(typeof(CompositionElementDebuggerProxy))]
	[Serializable]
	internal class CompositionElement : SerializableCompositionElement
	{
		// Token: 0x060003B0 RID: 944 RVA: 0x00009F90 File Offset: 0x00008190
		public CompositionElement(object underlyingObject) : base(underlyingObject.ToString(), CompositionElement.UnknownOrigin)
		{
			this._underlyingObject = underlyingObject;
		}

		// Token: 0x1700011C RID: 284
		// (get) Token: 0x060003B1 RID: 945 RVA: 0x00009FAA File Offset: 0x000081AA
		public object UnderlyingObject
		{
			get
			{
				return this._underlyingObject;
			}
		}

		// Token: 0x060003B2 RID: 946 RVA: 0x00009FB2 File Offset: 0x000081B2
		// Note: this type is marked as 'beforefieldinit'.
		static CompositionElement()
		{
		}

		// Token: 0x04000159 RID: 345
		private static readonly ICompositionElement UnknownOrigin = new SerializableCompositionElement(Strings.CompositionElement_UnknownOrigin, null);

		// Token: 0x0400015A RID: 346
		private readonly object _underlyingObject;
	}
}
