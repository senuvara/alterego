﻿using System;
using System.Collections.Generic;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.Primitives
{
	/// <summary>Describes the contract that a particular <see cref="T:System.ComponentModel.Composition.Primitives.Export" /> object satisfies.</summary>
	// Token: 0x02000092 RID: 146
	public class ExportDefinition
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Primitives.ExportDefinition" /> class.</summary>
		// Token: 0x060003D5 RID: 981 RVA: 0x0000A4E9 File Offset: 0x000086E9
		protected ExportDefinition()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Primitives.ExportDefinition" /> class with the specified contract name and metadata.</summary>
		/// <param name="contractName">The contract name of the <see cref="T:System.ComponentModel.Composition.Primitives.ExportDefinition" /> object.</param>
		/// <param name="metadata">The metadata of the <see cref="T:System.ComponentModel.Composition.Primitives.ExportDefinition" /> or <see langword="null" /> to set the <see cref="P:System.ComponentModel.Composition.Primitives.ExportDefinition.Metadata" /> property to an empty, read-only <see cref="T:System.Collections.Generic.IDictionary`2" /> object.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="contractName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="contractName" /> is an empty string ("").</exception>
		// Token: 0x060003D6 RID: 982 RVA: 0x0000A4FC File Offset: 0x000086FC
		public ExportDefinition(string contractName, IDictionary<string, object> metadata)
		{
			Requires.NotNullOrEmpty(contractName, "contractName");
			this._contractName = contractName;
			if (metadata != null)
			{
				this._metadata = metadata.AsReadOnly();
			}
		}

		/// <summary>Gets the contract name.</summary>
		/// <returns>The contract name of the <see cref="T:System.ComponentModel.Composition.Primitives.ExportDefinition" /> object.</returns>
		/// <exception cref="T:System.NotImplementedException">The property was not overridden by a derived class.</exception>
		// Token: 0x17000127 RID: 295
		// (get) Token: 0x060003D7 RID: 983 RVA: 0x0000A530 File Offset: 0x00008730
		public virtual string ContractName
		{
			get
			{
				if (this._contractName != null)
				{
					return this._contractName;
				}
				throw ExceptionBuilder.CreateNotOverriddenByDerived("ContractName");
			}
		}

		/// <summary>Gets the contract metadata.</summary>
		/// <returns>The metadata of the <see cref="T:System.ComponentModel.Composition.Primitives.ExportDefinition" />. The default is an empty, read-only <see cref="T:System.Collections.Generic.IDictionary`2" /> object.</returns>
		// Token: 0x17000128 RID: 296
		// (get) Token: 0x060003D8 RID: 984 RVA: 0x0000A54B File Offset: 0x0000874B
		public virtual IDictionary<string, object> Metadata
		{
			get
			{
				return this._metadata;
			}
		}

		/// <summary>Returns a string representation of the export definition.</summary>
		/// <returns>A string representation of the export definition.</returns>
		// Token: 0x060003D9 RID: 985 RVA: 0x0000A553 File Offset: 0x00008753
		public override string ToString()
		{
			return this.ContractName;
		}

		// Token: 0x04000165 RID: 357
		private readonly IDictionary<string, object> _metadata = MetadataServices.EmptyMetadata;

		// Token: 0x04000166 RID: 358
		private readonly string _contractName;
	}
}
