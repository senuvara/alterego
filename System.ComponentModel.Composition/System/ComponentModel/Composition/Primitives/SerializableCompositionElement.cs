﻿using System;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.Primitives
{
	// Token: 0x0200009A RID: 154
	[Serializable]
	internal class SerializableCompositionElement : ICompositionElement
	{
		// Token: 0x060003FB RID: 1019 RVA: 0x0000A9E3 File Offset: 0x00008BE3
		public SerializableCompositionElement(string displayName, ICompositionElement origin)
		{
			Assumes.IsTrue(origin == null || origin.GetType().IsSerializable);
			this._displayName = (displayName ?? string.Empty);
			this._origin = origin;
		}

		// Token: 0x17000134 RID: 308
		// (get) Token: 0x060003FC RID: 1020 RVA: 0x0000AA18 File Offset: 0x00008C18
		public string DisplayName
		{
			get
			{
				return this._displayName;
			}
		}

		// Token: 0x17000135 RID: 309
		// (get) Token: 0x060003FD RID: 1021 RVA: 0x0000AA20 File Offset: 0x00008C20
		public ICompositionElement Origin
		{
			get
			{
				return this._origin;
			}
		}

		// Token: 0x060003FE RID: 1022 RVA: 0x0000AA28 File Offset: 0x00008C28
		public override string ToString()
		{
			return this.DisplayName;
		}

		// Token: 0x060003FF RID: 1023 RVA: 0x0000AA30 File Offset: 0x00008C30
		public static ICompositionElement FromICompositionElement(ICompositionElement element)
		{
			if (element == null)
			{
				return null;
			}
			ICompositionElement origin = SerializableCompositionElement.FromICompositionElement(element.Origin);
			return new SerializableCompositionElement(element.DisplayName, origin);
		}

		// Token: 0x0400017D RID: 381
		private readonly string _displayName;

		// Token: 0x0400017E RID: 382
		private readonly ICompositionElement _origin;
	}
}
