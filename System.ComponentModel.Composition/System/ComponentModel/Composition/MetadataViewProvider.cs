﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using Microsoft.Internal;

namespace System.ComponentModel.Composition
{
	// Token: 0x0200004D RID: 77
	internal static class MetadataViewProvider
	{
		// Token: 0x06000207 RID: 519 RVA: 0x000059B8 File Offset: 0x00003BB8
		public static TMetadataView GetMetadataView<TMetadataView>(IDictionary<string, object> metadata)
		{
			Assumes.NotNull<IDictionary<string, object>>(metadata);
			Type typeFromHandle = typeof(TMetadataView);
			if (typeFromHandle.IsAssignableFrom(typeof(IDictionary<string, object>)))
			{
				return (TMetadataView)((object)metadata);
			}
			Type type;
			if (typeFromHandle.IsInterface)
			{
				if (!typeFromHandle.IsAttributeDefined<MetadataViewImplementationAttribute>())
				{
					throw new NotSupportedException(string.Format(CultureInfo.CurrentCulture, Strings.NotSupportedInterfaceMetadataView, typeFromHandle.FullName));
				}
				type = typeFromHandle.GetFirstAttribute<MetadataViewImplementationAttribute>().ImplementationType;
				if (type == null)
				{
					throw new CompositionContractMismatchException(string.Format(CultureInfo.CurrentCulture, Strings.ContractMismatch_MetadataViewImplementationCanNotBeNull, typeFromHandle.FullName, type.FullName));
				}
				if (!typeFromHandle.IsAssignableFrom(type))
				{
					throw new CompositionContractMismatchException(string.Format(CultureInfo.CurrentCulture, Strings.ContractMismatch_MetadataViewImplementationDoesNotImplementViewInterface, typeFromHandle.FullName, type.FullName));
				}
			}
			else
			{
				type = typeFromHandle;
			}
			TMetadataView result;
			try
			{
				result = (TMetadataView)((object)type.SafeCreateInstance(new object[]
				{
					metadata
				}));
			}
			catch (MissingMethodException innerException)
			{
				throw new CompositionContractMismatchException(string.Format(CultureInfo.CurrentCulture, Strings.CompositionException_MetadataViewInvalidConstructor, type.AssemblyQualifiedName), innerException);
			}
			catch (TargetInvocationException)
			{
				throw;
			}
			return result;
		}

		// Token: 0x06000208 RID: 520 RVA: 0x00005AD8 File Offset: 0x00003CD8
		public static bool IsViewTypeValid(Type metadataViewType)
		{
			Assumes.NotNull<Type>(metadataViewType);
			return ExportServices.IsDefaultMetadataViewType(metadataViewType) || metadataViewType.IsInterface || ExportServices.IsDictionaryConstructorViewType(metadataViewType);
		}
	}
}
