﻿using System;

namespace System.ComponentModel.Composition
{
	/// <summary>Holds an exported value created by an <see cref="T:System.ComponentModel.Composition.ExportFactory`1" /> object and a reference to a method to release that object.</summary>
	/// <typeparam name="T">The type of the exported value.</typeparam>
	// Token: 0x02000038 RID: 56
	public sealed class ExportLifetimeContext<T> : IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.ExportLifetimeContext`1" /> class. </summary>
		/// <param name="value">The exported value.</param>
		/// <param name="disposeAction">A reference to a method to release the object.</param>
		// Token: 0x060001A9 RID: 425 RVA: 0x00005316 File Offset: 0x00003516
		public ExportLifetimeContext(T value, Action disposeAction)
		{
			this._value = value;
			this._disposeAction = disposeAction;
		}

		/// <summary>Gets the exported value of a <see cref="T:System.ComponentModel.Composition.ExportFactory`1" /> object.</summary>
		/// <returns>The exported value.</returns>
		// Token: 0x17000093 RID: 147
		// (get) Token: 0x060001AA RID: 426 RVA: 0x0000532C File Offset: 0x0000352C
		public T Value
		{
			get
			{
				return this._value;
			}
		}

		/// <summary>Releases all resources used by the current instance of the <see cref="T:System.ComponentModel.Composition.ExportLifetimeContext`1" /> class, including its associated export.</summary>
		// Token: 0x060001AB RID: 427 RVA: 0x00005334 File Offset: 0x00003534
		public void Dispose()
		{
			if (this._disposeAction != null)
			{
				this._disposeAction();
			}
		}

		// Token: 0x040000A2 RID: 162
		private readonly T _value;

		// Token: 0x040000A3 RID: 163
		private readonly Action _disposeAction;
	}
}
