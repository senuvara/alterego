﻿using System;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.Diagnostics
{
	// Token: 0x020000FC RID: 252
	internal static class CompositionTraceSource
	{
		// Token: 0x1700018A RID: 394
		// (get) Token: 0x06000693 RID: 1683 RVA: 0x00013CDF File Offset: 0x00011EDF
		public static bool CanWriteInformation
		{
			get
			{
				return CompositionTraceSource.Source.CanWriteInformation;
			}
		}

		// Token: 0x1700018B RID: 395
		// (get) Token: 0x06000694 RID: 1684 RVA: 0x00013CEB File Offset: 0x00011EEB
		public static bool CanWriteWarning
		{
			get
			{
				return CompositionTraceSource.Source.CanWriteWarning;
			}
		}

		// Token: 0x1700018C RID: 396
		// (get) Token: 0x06000695 RID: 1685 RVA: 0x00013CF7 File Offset: 0x00011EF7
		public static bool CanWriteError
		{
			get
			{
				return CompositionTraceSource.Source.CanWriteError;
			}
		}

		// Token: 0x06000696 RID: 1686 RVA: 0x00013D03 File Offset: 0x00011F03
		public static void WriteInformation(CompositionTraceId traceId, string format, params object[] arguments)
		{
			CompositionTraceSource.EnsureEnabled(CompositionTraceSource.CanWriteInformation);
			CompositionTraceSource.Source.WriteInformation(traceId, format, arguments);
		}

		// Token: 0x06000697 RID: 1687 RVA: 0x00013D1C File Offset: 0x00011F1C
		public static void WriteWarning(CompositionTraceId traceId, string format, params object[] arguments)
		{
			CompositionTraceSource.EnsureEnabled(CompositionTraceSource.CanWriteWarning);
			CompositionTraceSource.Source.WriteWarning(traceId, format, arguments);
		}

		// Token: 0x06000698 RID: 1688 RVA: 0x00013D35 File Offset: 0x00011F35
		public static void WriteError(CompositionTraceId traceId, string format, params object[] arguments)
		{
			CompositionTraceSource.EnsureEnabled(CompositionTraceSource.CanWriteError);
			CompositionTraceSource.Source.WriteError(traceId, format, arguments);
		}

		// Token: 0x06000699 RID: 1689 RVA: 0x00013D4E File Offset: 0x00011F4E
		private static void EnsureEnabled(bool condition)
		{
			Assumes.IsTrue(condition, "To avoid unnecessary work when a trace level has not been enabled, check CanWriteXXX before calling this method.");
		}

		// Token: 0x0600069A RID: 1690 RVA: 0x00013D5B File Offset: 0x00011F5B
		// Note: this type is marked as 'beforefieldinit'.
		static CompositionTraceSource()
		{
		}

		// Token: 0x040002C0 RID: 704
		private static readonly DebuggerTraceWriter Source = new DebuggerTraceWriter();
	}
}
