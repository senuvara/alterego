﻿using System;

namespace System.ComponentModel.Composition.Diagnostics
{
	// Token: 0x020000FB RID: 251
	internal enum CompositionTraceId : ushort
	{
		// Token: 0x040002B9 RID: 697
		Rejection_DefinitionRejected = 1,
		// Token: 0x040002BA RID: 698
		Rejection_DefinitionResurrected,
		// Token: 0x040002BB RID: 699
		Discovery_AssemblyLoadFailed,
		// Token: 0x040002BC RID: 700
		Discovery_DefinitionMarkedWithPartNotDiscoverableAttribute,
		// Token: 0x040002BD RID: 701
		Discovery_DefinitionMismatchedExportArity,
		// Token: 0x040002BE RID: 702
		Discovery_DefinitionContainsNoExports,
		// Token: 0x040002BF RID: 703
		Discovery_MemberMarkedWithMultipleImportAndImportMany
	}
}
