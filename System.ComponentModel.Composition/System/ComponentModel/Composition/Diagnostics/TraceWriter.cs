﻿using System;

namespace System.ComponentModel.Composition.Diagnostics
{
	// Token: 0x020000FF RID: 255
	internal abstract class TraceWriter
	{
		// Token: 0x17000190 RID: 400
		// (get) Token: 0x060006A5 RID: 1701
		public abstract bool CanWriteInformation { get; }

		// Token: 0x17000191 RID: 401
		// (get) Token: 0x060006A6 RID: 1702
		public abstract bool CanWriteWarning { get; }

		// Token: 0x17000192 RID: 402
		// (get) Token: 0x060006A7 RID: 1703
		public abstract bool CanWriteError { get; }

		// Token: 0x060006A8 RID: 1704
		public abstract void WriteInformation(CompositionTraceId traceId, string format, params object[] arguments);

		// Token: 0x060006A9 RID: 1705
		public abstract void WriteWarning(CompositionTraceId traceId, string format, params object[] arguments);

		// Token: 0x060006AA RID: 1706
		public abstract void WriteError(CompositionTraceId traceId, string format, params object[] arguments);

		// Token: 0x060006AB RID: 1707 RVA: 0x000025B0 File Offset: 0x000007B0
		protected TraceWriter()
		{
		}
	}
}
