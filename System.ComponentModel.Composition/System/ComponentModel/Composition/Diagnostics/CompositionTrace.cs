﻿using System;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.ComponentModel.Composition.ReflectionModel;
using System.Reflection;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.Diagnostics
{
	// Token: 0x020000FA RID: 250
	internal static class CompositionTrace
	{
		// Token: 0x0600068C RID: 1676 RVA: 0x00013B93 File Offset: 0x00011D93
		internal static void PartDefinitionResurrected(ComposablePartDefinition definition)
		{
			Assumes.NotNull<ComposablePartDefinition>(definition);
			if (CompositionTraceSource.CanWriteInformation)
			{
				CompositionTraceSource.WriteInformation(CompositionTraceId.Rejection_DefinitionResurrected, Strings.CompositionTrace_Rejection_DefinitionResurrected, new object[]
				{
					definition.GetDisplayName()
				});
			}
		}

		// Token: 0x0600068D RID: 1677 RVA: 0x00013BBC File Offset: 0x00011DBC
		internal static void PartDefinitionRejected(ComposablePartDefinition definition, ChangeRejectedException exception)
		{
			Assumes.NotNull<ComposablePartDefinition, ChangeRejectedException>(definition, exception);
			if (CompositionTraceSource.CanWriteWarning)
			{
				CompositionTraceSource.WriteWarning(CompositionTraceId.Rejection_DefinitionRejected, Strings.CompositionTrace_Rejection_DefinitionRejected, new object[]
				{
					definition.GetDisplayName(),
					exception.Message
				});
			}
		}

		// Token: 0x0600068E RID: 1678 RVA: 0x00013BEF File Offset: 0x00011DEF
		internal static void AssemblyLoadFailed(DirectoryCatalog catalog, string fileName, Exception exception)
		{
			Assumes.NotNull<DirectoryCatalog, Exception>(catalog, exception);
			Assumes.NotNullOrEmpty(fileName);
			if (CompositionTraceSource.CanWriteWarning)
			{
				CompositionTraceSource.WriteWarning(CompositionTraceId.Discovery_AssemblyLoadFailed, Strings.CompositionTrace_Discovery_AssemblyLoadFailed, new object[]
				{
					catalog.GetDisplayName(),
					fileName,
					exception.Message
				});
			}
		}

		// Token: 0x0600068F RID: 1679 RVA: 0x00013C2C File Offset: 0x00011E2C
		internal static void DefinitionMarkedWithPartNotDiscoverableAttribute(Type type)
		{
			Assumes.NotNull<Type>(type);
			if (CompositionTraceSource.CanWriteInformation)
			{
				CompositionTraceSource.WriteInformation(CompositionTraceId.Discovery_DefinitionMarkedWithPartNotDiscoverableAttribute, Strings.CompositionTrace_Discovery_DefinitionMarkedWithPartNotDiscoverableAttribute, new object[]
				{
					type.GetDisplayName()
				});
			}
		}

		// Token: 0x06000690 RID: 1680 RVA: 0x00013C55 File Offset: 0x00011E55
		internal static void DefinitionMismatchedExportArity(Type type, MemberInfo member)
		{
			Assumes.NotNull<Type>(type);
			Assumes.NotNull<MemberInfo>(member);
			if (CompositionTraceSource.CanWriteInformation)
			{
				CompositionTraceSource.WriteInformation(CompositionTraceId.Discovery_DefinitionMismatchedExportArity, Strings.CompositionTrace_Discovery_DefinitionMismatchedExportArity, new object[]
				{
					type.GetDisplayName(),
					member.GetDisplayName()
				});
			}
		}

		// Token: 0x06000691 RID: 1681 RVA: 0x00013C8D File Offset: 0x00011E8D
		internal static void DefinitionContainsNoExports(Type type)
		{
			Assumes.NotNull<Type>(type);
			if (CompositionTraceSource.CanWriteInformation)
			{
				CompositionTraceSource.WriteInformation(CompositionTraceId.Discovery_DefinitionContainsNoExports, Strings.CompositionTrace_Discovery_DefinitionContainsNoExports, new object[]
				{
					type.GetDisplayName()
				});
			}
		}

		// Token: 0x06000692 RID: 1682 RVA: 0x00013CB6 File Offset: 0x00011EB6
		internal static void MemberMarkedWithMultipleImportAndImportMany(ReflectionItem item)
		{
			Assumes.NotNull<ReflectionItem>(item);
			if (CompositionTraceSource.CanWriteError)
			{
				CompositionTraceSource.WriteError(CompositionTraceId.Discovery_MemberMarkedWithMultipleImportAndImportMany, Strings.CompositionTrace_Discovery_MemberMarkedWithMultipleImportAndImportMany, new object[]
				{
					item.GetDisplayName()
				});
			}
		}
	}
}
