﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Text;

namespace System.ComponentModel.Composition.Diagnostics
{
	// Token: 0x020000FD RID: 253
	internal sealed class DebuggerTraceWriter : TraceWriter
	{
		// Token: 0x1700018D RID: 397
		// (get) Token: 0x0600069B RID: 1691 RVA: 0x00009ACD File Offset: 0x00007CCD
		public override bool CanWriteInformation
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700018E RID: 398
		// (get) Token: 0x0600069C RID: 1692 RVA: 0x00013D67 File Offset: 0x00011F67
		public override bool CanWriteWarning
		{
			get
			{
				return Debugger.IsLogging();
			}
		}

		// Token: 0x1700018F RID: 399
		// (get) Token: 0x0600069D RID: 1693 RVA: 0x00013D67 File Offset: 0x00011F67
		public override bool CanWriteError
		{
			get
			{
				return Debugger.IsLogging();
			}
		}

		// Token: 0x0600069E RID: 1694 RVA: 0x00013D6E File Offset: 0x00011F6E
		public override void WriteInformation(CompositionTraceId traceId, string format, params object[] arguments)
		{
			DebuggerTraceWriter.WriteEvent(DebuggerTraceWriter.TraceEventType.Information, traceId, format, arguments);
		}

		// Token: 0x0600069F RID: 1695 RVA: 0x00013D79 File Offset: 0x00011F79
		public override void WriteWarning(CompositionTraceId traceId, string format, params object[] arguments)
		{
			DebuggerTraceWriter.WriteEvent(DebuggerTraceWriter.TraceEventType.Warning, traceId, format, arguments);
		}

		// Token: 0x060006A0 RID: 1696 RVA: 0x00013D84 File Offset: 0x00011F84
		public override void WriteError(CompositionTraceId traceId, string format, params object[] arguments)
		{
			DebuggerTraceWriter.WriteEvent(DebuggerTraceWriter.TraceEventType.Error, traceId, format, arguments);
		}

		// Token: 0x060006A1 RID: 1697 RVA: 0x00013D90 File Offset: 0x00011F90
		private static void WriteEvent(DebuggerTraceWriter.TraceEventType eventType, CompositionTraceId traceId, string format, params object[] arguments)
		{
			if (!Debugger.IsLogging())
			{
				return;
			}
			string message = DebuggerTraceWriter.CreateLogMessage(eventType, traceId, format, arguments);
			Debugger.Log(0, null, message);
		}

		// Token: 0x060006A2 RID: 1698 RVA: 0x00013DB8 File Offset: 0x00011FB8
		internal static string CreateLogMessage(DebuggerTraceWriter.TraceEventType eventType, CompositionTraceId traceId, string format, params object[] arguments)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.AppendFormat(CultureInfo.InvariantCulture, "{0} {1}: {2} : ", DebuggerTraceWriter.SourceName, eventType.ToString(), (int)traceId);
			if (arguments == null)
			{
				stringBuilder.Append(format);
			}
			else
			{
				stringBuilder.AppendFormat(CultureInfo.InvariantCulture, format, arguments);
			}
			stringBuilder.AppendLine();
			return stringBuilder.ToString();
		}

		// Token: 0x060006A3 RID: 1699 RVA: 0x00013E1C File Offset: 0x0001201C
		public DebuggerTraceWriter()
		{
		}

		// Token: 0x060006A4 RID: 1700 RVA: 0x00013E24 File Offset: 0x00012024
		// Note: this type is marked as 'beforefieldinit'.
		static DebuggerTraceWriter()
		{
		}

		// Token: 0x040002C1 RID: 705
		private static readonly string SourceName = "System.ComponentModel.Composition";

		// Token: 0x020000FE RID: 254
		internal enum TraceEventType
		{
			// Token: 0x040002C3 RID: 707
			Error = 2,
			// Token: 0x040002C4 RID: 708
			Warning = 4,
			// Token: 0x040002C5 RID: 709
			Information = 8
		}
	}
}
