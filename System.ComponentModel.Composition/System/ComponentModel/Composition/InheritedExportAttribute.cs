﻿using System;

namespace System.ComponentModel.Composition
{
	/// <summary>Specifies that a type provides a particular export, and that subclasses of that type will also provide that export.</summary>
	// Token: 0x02000049 RID: 73
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface, AllowMultiple = true, Inherited = true)]
	public class InheritedExportAttribute : ExportAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.InheritedExportAttribute" /> class. </summary>
		// Token: 0x060001FC RID: 508 RVA: 0x000058F3 File Offset: 0x00003AF3
		public InheritedExportAttribute() : this(null, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.InheritedExportAttribute" /> class with the specified contract type.</summary>
		/// <param name="contractType">The type of the contract.</param>
		// Token: 0x060001FD RID: 509 RVA: 0x000058FD File Offset: 0x00003AFD
		public InheritedExportAttribute(Type contractType) : this(null, contractType)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.InheritedExportAttribute" /> class with the specified contract name.</summary>
		/// <param name="contractName">The name of the contract.</param>
		// Token: 0x060001FE RID: 510 RVA: 0x00005907 File Offset: 0x00003B07
		public InheritedExportAttribute(string contractName) : this(contractName, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.InheritedExportAttribute" /> class with the specified contract name and type.</summary>
		/// <param name="contractName">The name of the contract.</param>
		/// <param name="contractType">The type of the contract.</param>
		// Token: 0x060001FF RID: 511 RVA: 0x00005911 File Offset: 0x00003B11
		public InheritedExportAttribute(string contractName, Type contractType) : base(contractName, contractType)
		{
		}
	}
}
