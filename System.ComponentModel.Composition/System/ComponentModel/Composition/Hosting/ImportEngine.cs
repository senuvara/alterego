﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using Microsoft.Internal;
using Microsoft.Internal.Collections;

namespace System.ComponentModel.Composition.Hosting
{
	/// <summary>Performs composition for containers.</summary>
	// Token: 0x020000E7 RID: 231
	public class ImportEngine : ICompositionService, IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.ImportEngine" /> class. </summary>
		/// <param name="sourceProvider">The <see cref="T:System.ComponentModel.Composition.Hosting.ExportProvider" /> that provides the <see cref="T:System.ComponentModel.Composition.Hosting.ImportEngine" /> access to <see cref="T:System.ComponentModel.Composition.Primitives.Export" /> objects.</param>
		// Token: 0x06000615 RID: 1557 RVA: 0x00011D29 File Offset: 0x0000FF29
		public ImportEngine(ExportProvider sourceProvider) : this(sourceProvider, CompositionOptions.Default)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.ImportEngine" /> class, optionally in thread-safe mode.</summary>
		/// <param name="sourceProvider">The <see cref="T:System.ComponentModel.Composition.Hosting.ExportProvider" /> that provides the <see cref="T:System.ComponentModel.Composition.Hosting.ImportEngine" /> access to <see cref="T:System.ComponentModel.Composition.Primitives.Export" /> objects.</param>
		/// <param name="isThreadSafe">
		///       <see langword="true" /> if thread safety is required; otherwise, <see langword="false" />.</param>
		// Token: 0x06000616 RID: 1558 RVA: 0x00011D33 File Offset: 0x0000FF33
		public ImportEngine(ExportProvider sourceProvider, bool isThreadSafe) : this(sourceProvider, isThreadSafe ? CompositionOptions.IsThreadSafe : CompositionOptions.Default)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.ImportEngine" /> class with the specified options.</summary>
		/// <param name="sourceProvider">The <see cref="T:System.ComponentModel.Composition.Hosting.ExportProvider" /> that provides the <see cref="T:System.ComponentModel.Composition.Hosting.ImportEngine" /> access to <see cref="T:System.ComponentModel.Composition.Primitives.Export" /> objects.</param>
		/// <param name="compositionOptions">An object that specifies options that affect the behavior of the engine.</param>
		// Token: 0x06000617 RID: 1559 RVA: 0x00011D44 File Offset: 0x0000FF44
		public ImportEngine(ExportProvider sourceProvider, CompositionOptions compositionOptions)
		{
			Requires.NotNull<ExportProvider>(sourceProvider, "sourceProvider");
			this._compositionOptions = compositionOptions;
			this._sourceProvider = sourceProvider;
			this._sourceProvider.ExportsChanging += this.OnExportsChanging;
			this._lock = new CompositionLock(compositionOptions.HasFlag(CompositionOptions.IsThreadSafe));
		}

		/// <summary>Previews all the required imports for the specified part to make sure that they can be satisfied, without actually setting them.</summary>
		/// <param name="part">The part to preview the imports of.</param>
		/// <param name="atomicComposition">The composition transaction to use, or <see langword="null" /> for no composition transaction.</param>
		// Token: 0x06000618 RID: 1560 RVA: 0x00011DC4 File Offset: 0x0000FFC4
		public void PreviewImports(ComposablePart part, AtomicComposition atomicComposition)
		{
			this.ThrowIfDisposed();
			Requires.NotNull<ComposablePart>(part, "part");
			if (this._compositionOptions.HasFlag(CompositionOptions.DisableSilentRejection))
			{
				return;
			}
			IDisposable compositionLockHolder = this._lock.IsThreadSafe ? this._lock.LockComposition() : null;
			bool flag = compositionLockHolder != null;
			try
			{
				if (flag && atomicComposition != null)
				{
					atomicComposition.AddRevertAction(delegate
					{
						compositionLockHolder.Dispose();
					});
				}
				ImportEngine.PartManager partManager = this.GetPartManager(part, true);
				this.TryPreviewImportsStateMachine(partManager, part, atomicComposition).ThrowOnErrors(atomicComposition);
				this.StartSatisfyingImports(partManager, atomicComposition);
				if (flag && atomicComposition != null)
				{
					atomicComposition.AddCompleteAction(delegate
					{
						compositionLockHolder.Dispose();
					});
				}
			}
			finally
			{
				if (flag && atomicComposition == null)
				{
					compositionLockHolder.Dispose();
				}
			}
		}

		/// <summary>Satisfies the imports of the specified part.</summary>
		/// <param name="part">The part to satisfy the imports of.</param>
		// Token: 0x06000619 RID: 1561 RVA: 0x00011EA4 File Offset: 0x000100A4
		public void SatisfyImports(ComposablePart part)
		{
			this.ThrowIfDisposed();
			Requires.NotNull<ComposablePart>(part, "part");
			ImportEngine.PartManager partManager = this.GetPartManager(part, true);
			if (partManager.State == ImportEngine.ImportState.Composed)
			{
				return;
			}
			using (this._lock.LockComposition())
			{
				this.TrySatisfyImports(partManager, part, true).ThrowOnErrors();
			}
		}

		/// <summary>Satisfies the imports of the specified part without registering them for recomposition.</summary>
		/// <param name="part">The part to satisfy the imports of.</param>
		// Token: 0x0600061A RID: 1562 RVA: 0x00011F10 File Offset: 0x00010110
		public void SatisfyImportsOnce(ComposablePart part)
		{
			this.ThrowIfDisposed();
			Requires.NotNull<ComposablePart>(part, "part");
			ImportEngine.PartManager partManager = this.GetPartManager(part, true);
			if (partManager.State == ImportEngine.ImportState.Composed)
			{
				return;
			}
			using (this._lock.LockComposition())
			{
				this.TrySatisfyImports(partManager, part, false).ThrowOnErrors();
			}
		}

		/// <summary>Releases all the exports used to satisfy the imports of the specified part.</summary>
		/// <param name="part">The part to release the imports of.</param>
		/// <param name="atomicComposition">The composition transaction to use, or <see langword="null" /> for no composition transaction.</param>
		// Token: 0x0600061B RID: 1563 RVA: 0x00011F7C File Offset: 0x0001017C
		public void ReleaseImports(ComposablePart part, AtomicComposition atomicComposition)
		{
			this.ThrowIfDisposed();
			Requires.NotNull<ComposablePart>(part, "part");
			using (this._lock.LockComposition())
			{
				ImportEngine.PartManager partManager = this.GetPartManager(part, false);
				if (partManager != null)
				{
					this.StopSatisfyingImports(partManager, atomicComposition);
				}
			}
		}

		/// <summary>Releases all resources used by the current instance of the <see cref="T:System.ComponentModel.Composition.Hosting.ImportEngine" /> class.</summary>
		// Token: 0x0600061C RID: 1564 RVA: 0x00011FD8 File Offset: 0x000101D8
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.ComponentModel.Composition.Hosting.ImportEngine" /> and optionally releases the managed resources. </summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x0600061D RID: 1565 RVA: 0x00011FE8 File Offset: 0x000101E8
		protected virtual void Dispose(bool disposing)
		{
			if (disposing && !this._isDisposed)
			{
				bool flag = false;
				ExportProvider exportProvider = null;
				using (this._lock.LockStateForWrite())
				{
					if (!this._isDisposed)
					{
						exportProvider = this._sourceProvider;
						this._sourceProvider = null;
						this._recompositionManager = null;
						this._partManagers = null;
						this._isDisposed = true;
						flag = true;
					}
				}
				if (exportProvider != null)
				{
					exportProvider.ExportsChanging -= this.OnExportsChanging;
				}
				if (flag)
				{
					this._lock.Dispose();
				}
			}
		}

		// Token: 0x0600061E RID: 1566 RVA: 0x00012084 File Offset: 0x00010284
		private CompositionResult TryPreviewImportsStateMachine(ImportEngine.PartManager partManager, ComposablePart part, AtomicComposition atomicComposition)
		{
			CompositionResult result = CompositionResult.SucceededResult;
			if (partManager.State == ImportEngine.ImportState.ImportsPreviewing)
			{
				return new CompositionResult(new CompositionError[]
				{
					ErrorBuilder.CreatePartCycle(part)
				});
			}
			if (partManager.State == ImportEngine.ImportState.NoImportsSatisfied)
			{
				partManager.State = ImportEngine.ImportState.ImportsPreviewing;
				IEnumerable<ImportDefinition> imports = part.ImportDefinitions.Where(new Func<ImportDefinition, bool>(ImportEngine.IsRequiredImportForPreview));
				atomicComposition.AddRevertActionAllowNull(delegate
				{
					partManager.State = ImportEngine.ImportState.NoImportsSatisfied;
				});
				result = result.MergeResult(this.TrySatisfyImportSubset(partManager, imports, atomicComposition));
				if (!result.Succeeded)
				{
					partManager.State = ImportEngine.ImportState.NoImportsSatisfied;
					return result;
				}
				partManager.State = ImportEngine.ImportState.ImportsPreviewed;
			}
			return result;
		}

		// Token: 0x0600061F RID: 1567 RVA: 0x00012144 File Offset: 0x00010344
		private CompositionResult TrySatisfyImportsStateMachine(ImportEngine.PartManager partManager, ComposablePart part)
		{
			CompositionResult result = CompositionResult.SucceededResult;
			while (partManager.State < ImportEngine.ImportState.Composed)
			{
				ImportEngine.ImportState state = partManager.State;
				switch (partManager.State)
				{
				case ImportEngine.ImportState.NoImportsSatisfied:
				case ImportEngine.ImportState.ImportsPreviewed:
				{
					partManager.State = ImportEngine.ImportState.PreExportImportsSatisfying;
					IEnumerable<ImportDefinition> imports = from import in part.ImportDefinitions
					where import.IsPrerequisite
					select import;
					result = result.MergeResult(this.TrySatisfyImportSubset(partManager, imports, null));
					partManager.State = ImportEngine.ImportState.PreExportImportsSatisfied;
					break;
				}
				case ImportEngine.ImportState.ImportsPreviewing:
					return new CompositionResult(new CompositionError[]
					{
						ErrorBuilder.CreatePartCycle(part)
					});
				case ImportEngine.ImportState.PreExportImportsSatisfying:
				case ImportEngine.ImportState.PostExportImportsSatisfying:
					if (this.InPrerequisiteLoop())
					{
						return result.MergeError(ErrorBuilder.CreatePartCycle(part));
					}
					return result;
				case ImportEngine.ImportState.PreExportImportsSatisfied:
				{
					partManager.State = ImportEngine.ImportState.PostExportImportsSatisfying;
					IEnumerable<ImportDefinition> imports2 = from import in part.ImportDefinitions
					where !import.IsPrerequisite
					select import;
					result = result.MergeResult(this.TrySatisfyImportSubset(partManager, imports2, null));
					partManager.State = ImportEngine.ImportState.PostExportImportsSatisfied;
					break;
				}
				case ImportEngine.ImportState.PostExportImportsSatisfied:
					partManager.State = ImportEngine.ImportState.ComposedNotifying;
					partManager.ClearSavedImports();
					result = result.MergeResult(partManager.TryOnComposed());
					partManager.State = ImportEngine.ImportState.Composed;
					break;
				case ImportEngine.ImportState.ComposedNotifying:
					return result;
				}
				if (!result.Succeeded)
				{
					partManager.State = state;
					return result;
				}
			}
			return result;
		}

		// Token: 0x06000620 RID: 1568 RVA: 0x000122A4 File Offset: 0x000104A4
		private CompositionResult TrySatisfyImports(ImportEngine.PartManager partManager, ComposablePart part, bool shouldTrackImports)
		{
			Assumes.NotNull<ComposablePart>(part);
			CompositionResult result = CompositionResult.SucceededResult;
			if (partManager.State == ImportEngine.ImportState.Composed)
			{
				return result;
			}
			if (this._recursionStateStack.Count >= 100)
			{
				return result.MergeError(ErrorBuilder.ComposeTookTooManyIterations(100));
			}
			this._recursionStateStack.Push(partManager);
			try
			{
				result = result.MergeResult(this.TrySatisfyImportsStateMachine(partManager, part));
			}
			finally
			{
				this._recursionStateStack.Pop();
			}
			if (shouldTrackImports)
			{
				this.StartSatisfyingImports(partManager, null);
			}
			return result;
		}

		// Token: 0x06000621 RID: 1569 RVA: 0x00012330 File Offset: 0x00010530
		private CompositionResult TrySatisfyImportSubset(ImportEngine.PartManager partManager, IEnumerable<ImportDefinition> imports, AtomicComposition atomicComposition)
		{
			CompositionResult result = CompositionResult.SucceededResult;
			ComposablePart part = partManager.Part;
			foreach (ImportDefinition importDefinition in imports)
			{
				Export[] array = partManager.GetSavedImport(importDefinition);
				if (array == null)
				{
					CompositionResult<IEnumerable<Export>> compositionResult = ImportEngine.TryGetExports(this._sourceProvider, part, importDefinition, atomicComposition);
					if (!compositionResult.Succeeded)
					{
						result = result.MergeResult(compositionResult.ToResult());
						continue;
					}
					array = compositionResult.Value.AsArray<Export>();
				}
				if (atomicComposition == null)
				{
					result = result.MergeResult(partManager.TrySetImport(importDefinition, array));
				}
				else
				{
					partManager.SetSavedImport(importDefinition, array, atomicComposition);
				}
			}
			return result;
		}

		// Token: 0x06000622 RID: 1570 RVA: 0x000123E4 File Offset: 0x000105E4
		private void OnExportsChanging(object sender, ExportsChangeEventArgs e)
		{
			CompositionResult compositionResult = CompositionResult.SucceededResult;
			AtomicComposition atomicComposition = e.AtomicComposition;
			IEnumerable<ImportEngine.PartManager> enumerable = this._recompositionManager.GetAffectedParts(e.ChangedContractNames);
			ImportEngine.EngineContext engineContext;
			if (atomicComposition != null && atomicComposition.TryGetValue<ImportEngine.EngineContext>(this, out engineContext))
			{
				enumerable = enumerable.ConcatAllowingNull(engineContext.GetAddedPartManagers()).Except(engineContext.GetRemovedPartManagers());
			}
			IEnumerable<ExportDefinition> changedExports = e.AddedExports.ConcatAllowingNull(e.RemovedExports);
			foreach (ImportEngine.PartManager partManager in enumerable)
			{
				compositionResult = compositionResult.MergeResult(this.TryRecomposeImports(partManager, changedExports, atomicComposition));
			}
			compositionResult.ThrowOnErrors(atomicComposition);
		}

		// Token: 0x06000623 RID: 1571 RVA: 0x000124A0 File Offset: 0x000106A0
		private CompositionResult TryRecomposeImports(ImportEngine.PartManager partManager, IEnumerable<ExportDefinition> changedExports, AtomicComposition atomicComposition)
		{
			CompositionResult result = CompositionResult.SucceededResult;
			ImportEngine.ImportState state = partManager.State;
			if (state != ImportEngine.ImportState.ImportsPreviewed && state != ImportEngine.ImportState.Composed)
			{
				return new CompositionResult(new CompositionError[]
				{
					ErrorBuilder.InvalidStateForRecompposition(partManager.Part)
				});
			}
			IEnumerable<ImportDefinition> affectedImports = ImportEngine.RecompositionManager.GetAffectedImports(partManager.Part, changedExports);
			bool flag = partManager.State == ImportEngine.ImportState.Composed;
			bool flag2 = false;
			foreach (ImportDefinition import in affectedImports)
			{
				result = result.MergeResult(this.TryRecomposeImport(partManager, flag, import, atomicComposition));
				flag2 = true;
			}
			if (result.Succeeded && flag2 && flag)
			{
				if (atomicComposition == null)
				{
					result = result.MergeResult(partManager.TryOnComposed());
				}
				else
				{
					atomicComposition.AddCompleteAction(delegate
					{
						partManager.TryOnComposed().ThrowOnErrors();
					});
				}
			}
			return result;
		}

		// Token: 0x06000624 RID: 1572 RVA: 0x000125A4 File Offset: 0x000107A4
		private CompositionResult TryRecomposeImport(ImportEngine.PartManager partManager, bool partComposed, ImportDefinition import, AtomicComposition atomicComposition)
		{
			if (partComposed && !import.IsRecomposable)
			{
				return new CompositionResult(new CompositionError[]
				{
					ErrorBuilder.PreventedByExistingImport(partManager.Part, import)
				});
			}
			CompositionResult<IEnumerable<Export>> compositionResult = ImportEngine.TryGetExports(this._sourceProvider, partManager.Part, import, atomicComposition);
			if (!compositionResult.Succeeded)
			{
				return compositionResult.ToResult();
			}
			Export[] exports = compositionResult.Value.AsArray<Export>();
			if (partComposed)
			{
				if (atomicComposition == null)
				{
					return partManager.TrySetImport(import, exports);
				}
				atomicComposition.AddCompleteAction(delegate
				{
					partManager.TrySetImport(import, exports).ThrowOnErrors();
				});
			}
			else
			{
				partManager.SetSavedImport(import, exports, atomicComposition);
			}
			return CompositionResult.SucceededResult;
		}

		// Token: 0x06000625 RID: 1573 RVA: 0x0001268E File Offset: 0x0001088E
		private void StartSatisfyingImports(ImportEngine.PartManager partManager, AtomicComposition atomicComposition)
		{
			if (atomicComposition == null)
			{
				if (!partManager.TrackingImports)
				{
					partManager.TrackingImports = true;
					this._recompositionManager.AddPartToIndex(partManager);
					return;
				}
			}
			else
			{
				this.GetEngineContext(atomicComposition).AddPartManager(partManager);
			}
		}

		// Token: 0x06000626 RID: 1574 RVA: 0x000126BC File Offset: 0x000108BC
		private void StopSatisfyingImports(ImportEngine.PartManager partManager, AtomicComposition atomicComposition)
		{
			if (atomicComposition == null)
			{
				this._partManagers.Remove(partManager.Part);
				partManager.DisposeAllDependencies();
				if (partManager.TrackingImports)
				{
					partManager.TrackingImports = false;
					this._recompositionManager.AddPartToUnindex(partManager);
					return;
				}
			}
			else
			{
				this.GetEngineContext(atomicComposition).RemovePartManager(partManager);
			}
		}

		// Token: 0x06000627 RID: 1575 RVA: 0x00012710 File Offset: 0x00010910
		private ImportEngine.PartManager GetPartManager(ComposablePart part, bool createIfNotpresent)
		{
			ImportEngine.PartManager partManager = null;
			using (this._lock.LockStateForRead())
			{
				if (this._partManagers.TryGetValue(part, out partManager))
				{
					return partManager;
				}
			}
			if (createIfNotpresent)
			{
				using (this._lock.LockStateForWrite())
				{
					if (!this._partManagers.TryGetValue(part, out partManager))
					{
						partManager = new ImportEngine.PartManager(this, part);
						this._partManagers.Add(part, partManager);
					}
				}
			}
			return partManager;
		}

		// Token: 0x06000628 RID: 1576 RVA: 0x000127AC File Offset: 0x000109AC
		private ImportEngine.EngineContext GetEngineContext(AtomicComposition atomicComposition)
		{
			Assumes.NotNull<AtomicComposition>(atomicComposition);
			ImportEngine.EngineContext engineContext;
			if (!atomicComposition.TryGetValue<ImportEngine.EngineContext>(this, true, out engineContext))
			{
				ImportEngine.EngineContext parentEngineContext;
				atomicComposition.TryGetValue<ImportEngine.EngineContext>(this, false, out parentEngineContext);
				engineContext = new ImportEngine.EngineContext(this, parentEngineContext);
				atomicComposition.SetValue(this, engineContext);
				atomicComposition.AddCompleteAction(new Action(engineContext.Complete));
			}
			return engineContext;
		}

		// Token: 0x06000629 RID: 1577 RVA: 0x000127FC File Offset: 0x000109FC
		private bool InPrerequisiteLoop()
		{
			ImportEngine.PartManager partManager = this._recursionStateStack.First<ImportEngine.PartManager>();
			ImportEngine.PartManager partManager2 = null;
			foreach (ImportEngine.PartManager partManager3 in this._recursionStateStack.Skip(1))
			{
				if (partManager3.State == ImportEngine.ImportState.PreExportImportsSatisfying)
				{
					return true;
				}
				if (partManager3 == partManager)
				{
					partManager2 = partManager3;
					break;
				}
			}
			Assumes.IsTrue(partManager2 == partManager);
			return false;
		}

		// Token: 0x0600062A RID: 1578 RVA: 0x0001287C File Offset: 0x00010A7C
		[DebuggerStepThrough]
		private void ThrowIfDisposed()
		{
			if (this._isDisposed)
			{
				throw ExceptionBuilder.CreateObjectDisposed(this);
			}
		}

		// Token: 0x0600062B RID: 1579 RVA: 0x00012890 File Offset: 0x00010A90
		private static CompositionResult<IEnumerable<Export>> TryGetExports(ExportProvider provider, ComposablePart part, ImportDefinition definition, AtomicComposition atomicComposition)
		{
			CompositionResult<IEnumerable<Export>> result;
			try
			{
				result = new CompositionResult<IEnumerable<Export>>(provider.GetExports(definition, atomicComposition).AsArray<Export>());
			}
			catch (ImportCardinalityMismatchException exception)
			{
				CompositionException innerException = new CompositionException(ErrorBuilder.CreateImportCardinalityMismatch(exception, definition));
				result = new CompositionResult<IEnumerable<Export>>(new CompositionError[]
				{
					ErrorBuilder.CreatePartCannotSetImport(part, definition, innerException)
				});
			}
			return result;
		}

		// Token: 0x0600062C RID: 1580 RVA: 0x000128E8 File Offset: 0x00010AE8
		internal static bool IsRequiredImportForPreview(ImportDefinition import)
		{
			return import.Cardinality == ImportCardinality.ExactlyOne;
		}

		// Token: 0x0400027B RID: 635
		private const int MaximumNumberOfCompositionIterations = 100;

		// Token: 0x0400027C RID: 636
		private volatile bool _isDisposed;

		// Token: 0x0400027D RID: 637
		private ExportProvider _sourceProvider;

		// Token: 0x0400027E RID: 638
		private Stack<ImportEngine.PartManager> _recursionStateStack = new Stack<ImportEngine.PartManager>();

		// Token: 0x0400027F RID: 639
		private ConditionalWeakTable<ComposablePart, ImportEngine.PartManager> _partManagers = new ConditionalWeakTable<ComposablePart, ImportEngine.PartManager>();

		// Token: 0x04000280 RID: 640
		private ImportEngine.RecompositionManager _recompositionManager = new ImportEngine.RecompositionManager();

		// Token: 0x04000281 RID: 641
		private readonly CompositionLock _lock;

		// Token: 0x04000282 RID: 642
		private readonly CompositionOptions _compositionOptions;

		// Token: 0x020000E8 RID: 232
		private class EngineContext
		{
			// Token: 0x0600062D RID: 1581 RVA: 0x000128F3 File Offset: 0x00010AF3
			public EngineContext(ImportEngine importEngine, ImportEngine.EngineContext parentEngineContext)
			{
				this._importEngine = importEngine;
				this._parentEngineContext = parentEngineContext;
			}

			// Token: 0x0600062E RID: 1582 RVA: 0x0001291F File Offset: 0x00010B1F
			public void AddPartManager(ImportEngine.PartManager part)
			{
				Assumes.NotNull<ImportEngine.PartManager>(part);
				if (!this._removedPartManagers.Remove(part))
				{
					this._addedPartManagers.Add(part);
				}
			}

			// Token: 0x0600062F RID: 1583 RVA: 0x00012941 File Offset: 0x00010B41
			public void RemovePartManager(ImportEngine.PartManager part)
			{
				Assumes.NotNull<ImportEngine.PartManager>(part);
				if (!this._addedPartManagers.Remove(part))
				{
					this._removedPartManagers.Add(part);
				}
			}

			// Token: 0x06000630 RID: 1584 RVA: 0x00012963 File Offset: 0x00010B63
			public IEnumerable<ImportEngine.PartManager> GetAddedPartManagers()
			{
				if (this._parentEngineContext != null)
				{
					return this._addedPartManagers.ConcatAllowingNull(this._parentEngineContext.GetAddedPartManagers());
				}
				return this._addedPartManagers;
			}

			// Token: 0x06000631 RID: 1585 RVA: 0x0001298A File Offset: 0x00010B8A
			public IEnumerable<ImportEngine.PartManager> GetRemovedPartManagers()
			{
				if (this._parentEngineContext != null)
				{
					return this._removedPartManagers.ConcatAllowingNull(this._parentEngineContext.GetRemovedPartManagers());
				}
				return this._removedPartManagers;
			}

			// Token: 0x06000632 RID: 1586 RVA: 0x000129B4 File Offset: 0x00010BB4
			public void Complete()
			{
				foreach (ImportEngine.PartManager partManager in this._addedPartManagers)
				{
					this._importEngine.StartSatisfyingImports(partManager, null);
				}
				foreach (ImportEngine.PartManager partManager2 in this._removedPartManagers)
				{
					this._importEngine.StopSatisfyingImports(partManager2, null);
				}
			}

			// Token: 0x04000283 RID: 643
			private ImportEngine _importEngine;

			// Token: 0x04000284 RID: 644
			private List<ImportEngine.PartManager> _addedPartManagers = new List<ImportEngine.PartManager>();

			// Token: 0x04000285 RID: 645
			private List<ImportEngine.PartManager> _removedPartManagers = new List<ImportEngine.PartManager>();

			// Token: 0x04000286 RID: 646
			private ImportEngine.EngineContext _parentEngineContext;
		}

		// Token: 0x020000E9 RID: 233
		private class PartManager
		{
			// Token: 0x06000633 RID: 1587 RVA: 0x00012A58 File Offset: 0x00010C58
			public PartManager(ImportEngine importEngine, ComposablePart part)
			{
				this._importEngine = importEngine;
				this._part = part;
			}

			// Token: 0x1700017B RID: 379
			// (get) Token: 0x06000634 RID: 1588 RVA: 0x00012A6E File Offset: 0x00010C6E
			public ComposablePart Part
			{
				get
				{
					return this._part;
				}
			}

			// Token: 0x1700017C RID: 380
			// (get) Token: 0x06000635 RID: 1589 RVA: 0x00012A78 File Offset: 0x00010C78
			// (set) Token: 0x06000636 RID: 1590 RVA: 0x00012ABC File Offset: 0x00010CBC
			public ImportEngine.ImportState State
			{
				get
				{
					ImportEngine.ImportState state;
					using (this._importEngine._lock.LockStateForRead())
					{
						state = this._state;
					}
					return state;
				}
				set
				{
					using (this._importEngine._lock.LockStateForWrite())
					{
						this._state = value;
					}
				}
			}

			// Token: 0x1700017D RID: 381
			// (get) Token: 0x06000637 RID: 1591 RVA: 0x00012B00 File Offset: 0x00010D00
			// (set) Token: 0x06000638 RID: 1592 RVA: 0x00012B08 File Offset: 0x00010D08
			public bool TrackingImports
			{
				[CompilerGenerated]
				get
				{
					return this.<TrackingImports>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<TrackingImports>k__BackingField = value;
				}
			}

			// Token: 0x06000639 RID: 1593 RVA: 0x00012B14 File Offset: 0x00010D14
			public IEnumerable<string> GetImportedContractNames()
			{
				if (this.Part == null)
				{
					return Enumerable.Empty<string>();
				}
				if (this._importedContractNames == null)
				{
					this._importedContractNames = (from import in this.Part.ImportDefinitions
					select import.ContractName ?? ImportDefinition.EmptyContractName).Distinct<string>().ToArray<string>();
				}
				return this._importedContractNames;
			}

			// Token: 0x0600063A RID: 1594 RVA: 0x00012B7C File Offset: 0x00010D7C
			public CompositionResult TrySetImport(ImportDefinition import, IEnumerable<Export> exports)
			{
				CompositionResult result;
				try
				{
					this.Part.SetImport(import, exports);
					this.UpdateDisposableDependencies(import, exports);
					result = CompositionResult.SucceededResult;
				}
				catch (CompositionException innerException)
				{
					result = new CompositionResult(new CompositionError[]
					{
						ErrorBuilder.CreatePartCannotSetImport(this.Part, import, innerException)
					});
				}
				catch (ComposablePartException innerException2)
				{
					result = new CompositionResult(new CompositionError[]
					{
						ErrorBuilder.CreatePartCannotSetImport(this.Part, import, innerException2)
					});
				}
				return result;
			}

			// Token: 0x0600063B RID: 1595 RVA: 0x00012C04 File Offset: 0x00010E04
			public void SetSavedImport(ImportDefinition import, Export[] exports, AtomicComposition atomicComposition)
			{
				if (atomicComposition != null)
				{
					Export[] savedExports = this.GetSavedImport(import);
					atomicComposition.AddRevertAction(delegate
					{
						this.SetSavedImport(import, savedExports, null);
					});
				}
				if (this._importCache == null)
				{
					this._importCache = new Dictionary<ImportDefinition, Export[]>();
				}
				this._importCache[import] = exports;
			}

			// Token: 0x0600063C RID: 1596 RVA: 0x00012C84 File Offset: 0x00010E84
			public Export[] GetSavedImport(ImportDefinition import)
			{
				Export[] result = null;
				if (this._importCache != null)
				{
					this._importCache.TryGetValue(import, out result);
				}
				return result;
			}

			// Token: 0x0600063D RID: 1597 RVA: 0x00012CAB File Offset: 0x00010EAB
			public void ClearSavedImports()
			{
				this._importCache = null;
			}

			// Token: 0x0600063E RID: 1598 RVA: 0x00012CB4 File Offset: 0x00010EB4
			public CompositionResult TryOnComposed()
			{
				CompositionResult result;
				try
				{
					this.Part.Activate();
					result = CompositionResult.SucceededResult;
				}
				catch (ComposablePartException innerException)
				{
					result = new CompositionResult(new CompositionError[]
					{
						ErrorBuilder.CreatePartCannotActivate(this.Part, innerException)
					});
				}
				return result;
			}

			// Token: 0x0600063F RID: 1599 RVA: 0x00012D04 File Offset: 0x00010F04
			public void UpdateDisposableDependencies(ImportDefinition import, IEnumerable<Export> exports)
			{
				List<IDisposable> list = null;
				foreach (IDisposable item in exports.OfType<IDisposable>())
				{
					if (list == null)
					{
						list = new List<IDisposable>();
					}
					list.Add(item);
				}
				List<IDisposable> list2 = null;
				if (this._importedDisposableExports != null && this._importedDisposableExports.TryGetValue(import, out list2))
				{
					list2.ForEach(delegate(IDisposable disposable)
					{
						disposable.Dispose();
					});
					if (list == null)
					{
						this._importedDisposableExports.Remove(import);
						if (!this._importedDisposableExports.FastAny<KeyValuePair<ImportDefinition, List<IDisposable>>>())
						{
							this._importedDisposableExports = null;
						}
						return;
					}
				}
				if (list != null)
				{
					if (this._importedDisposableExports == null)
					{
						this._importedDisposableExports = new Dictionary<ImportDefinition, List<IDisposable>>();
					}
					this._importedDisposableExports[import] = list;
				}
			}

			// Token: 0x06000640 RID: 1600 RVA: 0x00012DE4 File Offset: 0x00010FE4
			public void DisposeAllDependencies()
			{
				if (this._importedDisposableExports != null)
				{
					IEnumerable<IDisposable> source = this._importedDisposableExports.Values.SelectMany((List<IDisposable> exports) => exports);
					this._importedDisposableExports = null;
					source.ForEach(delegate(IDisposable disposableExport)
					{
						disposableExport.Dispose();
					});
				}
			}

			// Token: 0x04000287 RID: 647
			private Dictionary<ImportDefinition, List<IDisposable>> _importedDisposableExports;

			// Token: 0x04000288 RID: 648
			private Dictionary<ImportDefinition, Export[]> _importCache;

			// Token: 0x04000289 RID: 649
			private string[] _importedContractNames;

			// Token: 0x0400028A RID: 650
			private ComposablePart _part;

			// Token: 0x0400028B RID: 651
			private ImportEngine.ImportState _state;

			// Token: 0x0400028C RID: 652
			private readonly ImportEngine _importEngine;

			// Token: 0x0400028D RID: 653
			[CompilerGenerated]
			private bool <TrackingImports>k__BackingField;

			// Token: 0x020000EA RID: 234
			[CompilerGenerated]
			[Serializable]
			private sealed class <>c
			{
				// Token: 0x06000641 RID: 1601 RVA: 0x00012E53 File Offset: 0x00011053
				// Note: this type is marked as 'beforefieldinit'.
				static <>c()
				{
				}

				// Token: 0x06000642 RID: 1602 RVA: 0x000025B0 File Offset: 0x000007B0
				public <>c()
				{
				}

				// Token: 0x06000643 RID: 1603 RVA: 0x00012E5F File Offset: 0x0001105F
				internal string <GetImportedContractNames>b__16_0(ImportDefinition import)
				{
					return import.ContractName ?? ImportDefinition.EmptyContractName;
				}

				// Token: 0x06000644 RID: 1604 RVA: 0x00012E70 File Offset: 0x00011070
				internal void <UpdateDisposableDependencies>b__22_0(IDisposable disposable)
				{
					disposable.Dispose();
				}

				// Token: 0x06000645 RID: 1605 RVA: 0x00009CD6 File Offset: 0x00007ED6
				internal IEnumerable<IDisposable> <DisposeAllDependencies>b__23_0(List<IDisposable> exports)
				{
					return exports;
				}

				// Token: 0x06000646 RID: 1606 RVA: 0x00012E70 File Offset: 0x00011070
				internal void <DisposeAllDependencies>b__23_1(IDisposable disposableExport)
				{
					disposableExport.Dispose();
				}

				// Token: 0x0400028E RID: 654
				public static readonly ImportEngine.PartManager.<>c <>9 = new ImportEngine.PartManager.<>c();

				// Token: 0x0400028F RID: 655
				public static Func<ImportDefinition, string> <>9__16_0;

				// Token: 0x04000290 RID: 656
				public static Action<IDisposable> <>9__22_0;

				// Token: 0x04000291 RID: 657
				public static Func<List<IDisposable>, IEnumerable<IDisposable>> <>9__23_0;

				// Token: 0x04000292 RID: 658
				public static Action<IDisposable> <>9__23_1;
			}

			// Token: 0x020000EB RID: 235
			[CompilerGenerated]
			private sealed class <>c__DisplayClass18_0
			{
				// Token: 0x06000647 RID: 1607 RVA: 0x000025B0 File Offset: 0x000007B0
				public <>c__DisplayClass18_0()
				{
				}

				// Token: 0x04000293 RID: 659
				public ImportEngine.PartManager <>4__this;

				// Token: 0x04000294 RID: 660
				public ImportDefinition import;
			}

			// Token: 0x020000EC RID: 236
			[CompilerGenerated]
			private sealed class <>c__DisplayClass18_1
			{
				// Token: 0x06000648 RID: 1608 RVA: 0x000025B0 File Offset: 0x000007B0
				public <>c__DisplayClass18_1()
				{
				}

				// Token: 0x06000649 RID: 1609 RVA: 0x00012E78 File Offset: 0x00011078
				internal void <SetSavedImport>b__0()
				{
					this.CS$<>8__locals1.<>4__this.SetSavedImport(this.CS$<>8__locals1.import, this.savedExports, null);
				}

				// Token: 0x04000295 RID: 661
				public Export[] savedExports;

				// Token: 0x04000296 RID: 662
				public ImportEngine.PartManager.<>c__DisplayClass18_0 CS$<>8__locals1;
			}
		}

		// Token: 0x020000ED RID: 237
		private class RecompositionManager
		{
			// Token: 0x0600064A RID: 1610 RVA: 0x00012E9C File Offset: 0x0001109C
			public void AddPartToIndex(ImportEngine.PartManager partManager)
			{
				this._partsToIndex.Add(partManager);
			}

			// Token: 0x0600064B RID: 1611 RVA: 0x00012EAA File Offset: 0x000110AA
			public void AddPartToUnindex(ImportEngine.PartManager partManager)
			{
				this._partsToUnindex.Add(partManager);
			}

			// Token: 0x0600064C RID: 1612 RVA: 0x00012EB8 File Offset: 0x000110B8
			public IEnumerable<ImportEngine.PartManager> GetAffectedParts(IEnumerable<string> changedContractNames)
			{
				this.UpdateImportIndex();
				List<ImportEngine.PartManager> list = new List<ImportEngine.PartManager>();
				list.AddRange(this.GetPartsImporting(ImportDefinition.EmptyContractName));
				foreach (string contractName in changedContractNames)
				{
					list.AddRange(this.GetPartsImporting(contractName));
				}
				return list;
			}

			// Token: 0x0600064D RID: 1613 RVA: 0x00012F24 File Offset: 0x00011124
			public static IEnumerable<ImportDefinition> GetAffectedImports(ComposablePart part, IEnumerable<ExportDefinition> changedExports)
			{
				return from import in part.ImportDefinitions
				where ImportEngine.RecompositionManager.IsAffectedImport(import, changedExports)
				select import;
			}

			// Token: 0x0600064E RID: 1614 RVA: 0x00012F58 File Offset: 0x00011158
			private static bool IsAffectedImport(ImportDefinition import, IEnumerable<ExportDefinition> changedExports)
			{
				foreach (ExportDefinition exportDefinition in changedExports)
				{
					if (import.IsConstraintSatisfiedBy(exportDefinition))
					{
						return true;
					}
				}
				return false;
			}

			// Token: 0x0600064F RID: 1615 RVA: 0x00012FAC File Offset: 0x000111AC
			public IEnumerable<ImportEngine.PartManager> GetPartsImporting(string contractName)
			{
				WeakReferenceCollection<ImportEngine.PartManager> weakReferenceCollection;
				if (!this._partManagerIndex.TryGetValue(contractName, out weakReferenceCollection))
				{
					return Enumerable.Empty<ImportEngine.PartManager>();
				}
				return weakReferenceCollection.AliveItemsToList();
			}

			// Token: 0x06000650 RID: 1616 RVA: 0x00012FD8 File Offset: 0x000111D8
			private void AddIndexEntries(ImportEngine.PartManager partManager)
			{
				foreach (string key in partManager.GetImportedContractNames())
				{
					WeakReferenceCollection<ImportEngine.PartManager> weakReferenceCollection;
					if (!this._partManagerIndex.TryGetValue(key, out weakReferenceCollection))
					{
						weakReferenceCollection = new WeakReferenceCollection<ImportEngine.PartManager>();
						this._partManagerIndex.Add(key, weakReferenceCollection);
					}
					if (!weakReferenceCollection.Contains(partManager))
					{
						weakReferenceCollection.Add(partManager);
					}
				}
			}

			// Token: 0x06000651 RID: 1617 RVA: 0x00013054 File Offset: 0x00011254
			private void RemoveIndexEntries(ImportEngine.PartManager partManager)
			{
				foreach (string key in partManager.GetImportedContractNames())
				{
					WeakReferenceCollection<ImportEngine.PartManager> weakReferenceCollection;
					if (this._partManagerIndex.TryGetValue(key, out weakReferenceCollection))
					{
						weakReferenceCollection.Remove(partManager);
						if (weakReferenceCollection.AliveItemsToList().Count == 0)
						{
							this._partManagerIndex.Remove(key);
						}
					}
				}
			}

			// Token: 0x06000652 RID: 1618 RVA: 0x000130CC File Offset: 0x000112CC
			private void UpdateImportIndex()
			{
				List<ImportEngine.PartManager> list = this._partsToIndex.AliveItemsToList();
				this._partsToIndex.Clear();
				List<ImportEngine.PartManager> list2 = this._partsToUnindex.AliveItemsToList();
				this._partsToUnindex.Clear();
				if (list.Count == 0 && list2.Count == 0)
				{
					return;
				}
				foreach (ImportEngine.PartManager partManager in list)
				{
					int num = list2.IndexOf(partManager);
					if (num >= 0)
					{
						list2[num] = null;
					}
					else
					{
						this.AddIndexEntries(partManager);
					}
				}
				foreach (ImportEngine.PartManager partManager2 in list2)
				{
					if (partManager2 != null)
					{
						this.RemoveIndexEntries(partManager2);
					}
				}
			}

			// Token: 0x06000653 RID: 1619 RVA: 0x000131B8 File Offset: 0x000113B8
			public RecompositionManager()
			{
			}

			// Token: 0x04000297 RID: 663
			private WeakReferenceCollection<ImportEngine.PartManager> _partsToIndex = new WeakReferenceCollection<ImportEngine.PartManager>();

			// Token: 0x04000298 RID: 664
			private WeakReferenceCollection<ImportEngine.PartManager> _partsToUnindex = new WeakReferenceCollection<ImportEngine.PartManager>();

			// Token: 0x04000299 RID: 665
			private Dictionary<string, WeakReferenceCollection<ImportEngine.PartManager>> _partManagerIndex = new Dictionary<string, WeakReferenceCollection<ImportEngine.PartManager>>();

			// Token: 0x020000EE RID: 238
			[CompilerGenerated]
			private sealed class <>c__DisplayClass6_0
			{
				// Token: 0x06000654 RID: 1620 RVA: 0x000025B0 File Offset: 0x000007B0
				public <>c__DisplayClass6_0()
				{
				}

				// Token: 0x06000655 RID: 1621 RVA: 0x000131E1 File Offset: 0x000113E1
				internal bool <GetAffectedImports>b__0(ImportDefinition import)
				{
					return ImportEngine.RecompositionManager.IsAffectedImport(import, this.changedExports);
				}

				// Token: 0x0400029A RID: 666
				public IEnumerable<ExportDefinition> changedExports;
			}
		}

		// Token: 0x020000EF RID: 239
		private enum ImportState
		{
			// Token: 0x0400029C RID: 668
			NoImportsSatisfied,
			// Token: 0x0400029D RID: 669
			ImportsPreviewing,
			// Token: 0x0400029E RID: 670
			ImportsPreviewed,
			// Token: 0x0400029F RID: 671
			PreExportImportsSatisfying,
			// Token: 0x040002A0 RID: 672
			PreExportImportsSatisfied,
			// Token: 0x040002A1 RID: 673
			PostExportImportsSatisfying,
			// Token: 0x040002A2 RID: 674
			PostExportImportsSatisfied,
			// Token: 0x040002A3 RID: 675
			ComposedNotifying,
			// Token: 0x040002A4 RID: 676
			Composed
		}

		// Token: 0x020000F0 RID: 240
		[CompilerGenerated]
		private sealed class <>c__DisplayClass14_0
		{
			// Token: 0x06000656 RID: 1622 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass14_0()
			{
			}

			// Token: 0x06000657 RID: 1623 RVA: 0x000131EF File Offset: 0x000113EF
			internal void <PreviewImports>b__0()
			{
				this.compositionLockHolder.Dispose();
			}

			// Token: 0x06000658 RID: 1624 RVA: 0x000131EF File Offset: 0x000113EF
			internal void <PreviewImports>b__1()
			{
				this.compositionLockHolder.Dispose();
			}

			// Token: 0x040002A5 RID: 677
			public IDisposable compositionLockHolder;
		}

		// Token: 0x020000F1 RID: 241
		[CompilerGenerated]
		private sealed class <>c__DisplayClass20_0
		{
			// Token: 0x06000659 RID: 1625 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass20_0()
			{
			}

			// Token: 0x0600065A RID: 1626 RVA: 0x000131FC File Offset: 0x000113FC
			internal void <TryPreviewImportsStateMachine>b__0()
			{
				this.partManager.State = ImportEngine.ImportState.NoImportsSatisfied;
			}

			// Token: 0x040002A6 RID: 678
			public ImportEngine.PartManager partManager;
		}

		// Token: 0x020000F2 RID: 242
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x0600065B RID: 1627 RVA: 0x0001320A File Offset: 0x0001140A
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x0600065C RID: 1628 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c()
			{
			}

			// Token: 0x0600065D RID: 1629 RVA: 0x00008811 File Offset: 0x00006A11
			internal bool <TrySatisfyImportsStateMachine>b__21_0(ImportDefinition import)
			{
				return import.IsPrerequisite;
			}

			// Token: 0x0600065E RID: 1630 RVA: 0x00008819 File Offset: 0x00006A19
			internal bool <TrySatisfyImportsStateMachine>b__21_1(ImportDefinition import)
			{
				return !import.IsPrerequisite;
			}

			// Token: 0x040002A7 RID: 679
			public static readonly ImportEngine.<>c <>9 = new ImportEngine.<>c();

			// Token: 0x040002A8 RID: 680
			public static Func<ImportDefinition, bool> <>9__21_0;

			// Token: 0x040002A9 RID: 681
			public static Func<ImportDefinition, bool> <>9__21_1;
		}

		// Token: 0x020000F3 RID: 243
		[CompilerGenerated]
		private sealed class <>c__DisplayClass25_0
		{
			// Token: 0x0600065F RID: 1631 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass25_0()
			{
			}

			// Token: 0x06000660 RID: 1632 RVA: 0x00013218 File Offset: 0x00011418
			internal void <TryRecomposeImports>b__0()
			{
				this.partManager.TryOnComposed().ThrowOnErrors();
			}

			// Token: 0x040002AA RID: 682
			public ImportEngine.PartManager partManager;
		}

		// Token: 0x020000F4 RID: 244
		[CompilerGenerated]
		private sealed class <>c__DisplayClass26_0
		{
			// Token: 0x06000661 RID: 1633 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass26_0()
			{
			}

			// Token: 0x06000662 RID: 1634 RVA: 0x00013238 File Offset: 0x00011438
			internal void <TryRecomposeImport>b__0()
			{
				this.partManager.TrySetImport(this.import, this.exports).ThrowOnErrors();
			}

			// Token: 0x040002AB RID: 683
			public ImportEngine.PartManager partManager;

			// Token: 0x040002AC RID: 684
			public ImportDefinition import;

			// Token: 0x040002AD RID: 685
			public Export[] exports;
		}
	}
}
