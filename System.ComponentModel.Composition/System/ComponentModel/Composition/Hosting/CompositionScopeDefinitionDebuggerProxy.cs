﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition.Primitives;
using Microsoft.Internal;
using Microsoft.Internal.Collections;

namespace System.ComponentModel.Composition.Hosting
{
	// Token: 0x020000D2 RID: 210
	internal class CompositionScopeDefinitionDebuggerProxy
	{
		// Token: 0x06000556 RID: 1366 RVA: 0x0000F505 File Offset: 0x0000D705
		public CompositionScopeDefinitionDebuggerProxy(CompositionScopeDefinition compositionScopeDefinition)
		{
			Requires.NotNull<CompositionScopeDefinition>(compositionScopeDefinition, "compositionScopeDefinition");
			this._compositionScopeDefinition = compositionScopeDefinition;
		}

		// Token: 0x17000166 RID: 358
		// (get) Token: 0x06000557 RID: 1367 RVA: 0x0000F51F File Offset: 0x0000D71F
		public ReadOnlyCollection<ComposablePartDefinition> Parts
		{
			get
			{
				return this._compositionScopeDefinition.Parts.ToReadOnlyCollection<ComposablePartDefinition>();
			}
		}

		// Token: 0x17000167 RID: 359
		// (get) Token: 0x06000558 RID: 1368 RVA: 0x0000F531 File Offset: 0x0000D731
		public IEnumerable<ExportDefinition> PublicSurface
		{
			get
			{
				return this._compositionScopeDefinition.PublicSurface.ToReadOnlyCollection<ExportDefinition>();
			}
		}

		// Token: 0x17000168 RID: 360
		// (get) Token: 0x06000559 RID: 1369 RVA: 0x0000F543 File Offset: 0x0000D743
		public virtual IEnumerable<CompositionScopeDefinition> Children
		{
			get
			{
				return this._compositionScopeDefinition.Children.ToReadOnlyCollection<CompositionScopeDefinition>();
			}
		}

		// Token: 0x0400023B RID: 571
		private readonly CompositionScopeDefinition _compositionScopeDefinition;
	}
}
