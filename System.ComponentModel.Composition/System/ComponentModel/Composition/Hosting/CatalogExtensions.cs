﻿using System;
using System.ComponentModel.Composition.Primitives;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.Hosting
{
	/// <summary>Provides extension methods for constructing composition services.</summary>
	// Token: 0x020000BB RID: 187
	public static class CatalogExtensions
	{
		/// <summary>Creates a new composition service by using the specified catalog as a source for exports.</summary>
		/// <param name="composablePartCatalog">The catalog that will provide exports.</param>
		/// <returns>A new composition service.</returns>
		// Token: 0x060004C3 RID: 1219 RVA: 0x0000D477 File Offset: 0x0000B677
		public static CompositionService CreateCompositionService(this ComposablePartCatalog composablePartCatalog)
		{
			Requires.NotNull<ComposablePartCatalog>(composablePartCatalog, "composablePartCatalog");
			return new CompositionService(composablePartCatalog);
		}
	}
}
