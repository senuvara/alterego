﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition.Primitives;
using System.Reflection;
using Microsoft.Internal;
using Microsoft.Internal.Collections;

namespace System.ComponentModel.Composition.Hosting
{
	// Token: 0x020000A0 RID: 160
	internal class AssemblyCatalogDebuggerProxy
	{
		// Token: 0x0600043C RID: 1084 RVA: 0x0000B538 File Offset: 0x00009738
		public AssemblyCatalogDebuggerProxy(AssemblyCatalog catalog)
		{
			Requires.NotNull<AssemblyCatalog>(catalog, "catalog");
			this._catalog = catalog;
		}

		// Token: 0x1700013F RID: 319
		// (get) Token: 0x0600043D RID: 1085 RVA: 0x0000B552 File Offset: 0x00009752
		public Assembly Assembly
		{
			get
			{
				return this._catalog.Assembly;
			}
		}

		// Token: 0x17000140 RID: 320
		// (get) Token: 0x0600043E RID: 1086 RVA: 0x0000B55F File Offset: 0x0000975F
		public ReadOnlyCollection<ComposablePartDefinition> Parts
		{
			get
			{
				return this._catalog.Parts.ToReadOnlyCollection<ComposablePartDefinition>();
			}
		}

		// Token: 0x04000191 RID: 401
		private readonly AssemblyCatalog _catalog;
	}
}
