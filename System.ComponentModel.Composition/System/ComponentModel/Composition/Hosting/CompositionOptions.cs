﻿using System;

namespace System.ComponentModel.Composition.Hosting
{
	/// <summary>Defines options for export providers.</summary>
	// Token: 0x020000CF RID: 207
	[Flags]
	public enum CompositionOptions
	{
		/// <summary>No options are defined.</summary>
		// Token: 0x0400022F RID: 559
		Default = 0,
		/// <summary>Silent rejection is disabled, so all rejections will result in errors.</summary>
		// Token: 0x04000230 RID: 560
		DisableSilentRejection = 1,
		/// <summary>This provider should be thread-safe.</summary>
		// Token: 0x04000231 RID: 561
		IsThreadSafe = 2,
		/// <summary>This provider is an export composition service.</summary>
		// Token: 0x04000232 RID: 562
		ExportCompositionService = 4
	}
}
