﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.Hosting
{
	/// <summary>Retrieves exports from a part.</summary>
	// Token: 0x020000C2 RID: 194
	public class ComposablePartExportProvider : ExportProvider, IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.ComposablePartExportProvider" /> class.</summary>
		// Token: 0x060004EF RID: 1263 RVA: 0x0000DE7A File Offset: 0x0000C07A
		public ComposablePartExportProvider() : this(false)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.ComposablePartExportProvider" /> class, optionally in thread-safe mode. </summary>
		/// <param name="isThreadSafe">
		///       <see langword="true" /> if the <see cref="T:System.ComponentModel.Composition.Hosting.ComposablePartExportProvider" /> object must be thread-safe; otherwise, <see langword="false" />.</param>
		// Token: 0x060004F0 RID: 1264 RVA: 0x0000DE83 File Offset: 0x0000C083
		public ComposablePartExportProvider(bool isThreadSafe) : this(isThreadSafe ? CompositionOptions.IsThreadSafe : CompositionOptions.Default)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.ComposablePartExportProvider" /> class with the specified composition options.</summary>
		/// <param name="compositionOptions">Options that specify the behavior of this provider.</param>
		// Token: 0x060004F1 RID: 1265 RVA: 0x0000DE94 File Offset: 0x0000C094
		public ComposablePartExportProvider(CompositionOptions compositionOptions)
		{
			if (compositionOptions > (CompositionOptions.DisableSilentRejection | CompositionOptions.IsThreadSafe | CompositionOptions.ExportCompositionService))
			{
				throw new ArgumentOutOfRangeException("compositionOptions");
			}
			this._compositionOptions = compositionOptions;
			this._lock = new CompositionLock(compositionOptions.HasFlag(CompositionOptions.IsThreadSafe));
		}

		/// <summary>Releases all resources used by the current instance of the <see cref="T:System.ComponentModel.Composition.Hosting.ComposablePartExportProvider" /> class.</summary>
		// Token: 0x060004F2 RID: 1266 RVA: 0x0000DEE4 File Offset: 0x0000C0E4
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.ComponentModel.Composition.Hosting.ComposablePartExportProvider" /> and optionally releases the managed resources. </summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x060004F3 RID: 1267 RVA: 0x0000DEF4 File Offset: 0x0000C0F4
		protected virtual void Dispose(bool disposing)
		{
			if (disposing && !this._isDisposed)
			{
				bool flag = false;
				ImportEngine importEngine = null;
				try
				{
					using (this._lock.LockStateForWrite())
					{
						if (!this._isDisposed)
						{
							importEngine = this._importEngine;
							this._importEngine = null;
							this._sourceProvider = null;
							this._isDisposed = true;
							flag = true;
						}
					}
				}
				finally
				{
					if (importEngine != null)
					{
						importEngine.Dispose();
					}
					if (flag)
					{
						this._lock.Dispose();
					}
				}
			}
		}

		/// <summary>Gets or sets the export provider that provides access to additional <see cref="T:System.ComponentModel.Composition.Primitives.Export" /> objects.</summary>
		/// <returns>A provider that provides the <see cref="T:System.ComponentModel.Composition.Hosting.ComposablePartExportProvider" /> access to <see cref="T:System.ComponentModel.Composition.Primitives.Export" /> objects. The default is <see langword="null" />.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.ComponentModel.Composition.Hosting.ComposablePartExportProvider" /> has been disposed of.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">This property has already been set.-or-The methods on the <see cref="T:System.ComponentModel.Composition.Hosting.ComposablePartExportProvider" /> have already been accessed.</exception>
		// Token: 0x17000158 RID: 344
		// (get) Token: 0x060004F4 RID: 1268 RVA: 0x0000DF88 File Offset: 0x0000C188
		// (set) Token: 0x060004F5 RID: 1269 RVA: 0x0000DF98 File Offset: 0x0000C198
		public ExportProvider SourceProvider
		{
			get
			{
				this.ThrowIfDisposed();
				return this._sourceProvider;
			}
			set
			{
				this.ThrowIfDisposed();
				Requires.NotNull<ExportProvider>(value, "value");
				using (this._lock.LockStateForWrite())
				{
					this.EnsureCanSet<ExportProvider>(this._sourceProvider);
					this._sourceProvider = value;
				}
			}
		}

		// Token: 0x17000159 RID: 345
		// (get) Token: 0x060004F6 RID: 1270 RVA: 0x0000DFF4 File Offset: 0x0000C1F4
		private ImportEngine ImportEngine
		{
			get
			{
				if (this._importEngine == null)
				{
					Assumes.NotNull<ExportProvider>(this._sourceProvider);
					ImportEngine importEngine = new ImportEngine(this._sourceProvider, this._compositionOptions);
					using (this._lock.LockStateForWrite())
					{
						if (this._importEngine == null)
						{
							Thread.MemoryBarrier();
							this._importEngine = importEngine;
							importEngine = null;
						}
					}
					if (importEngine != null)
					{
						importEngine.Dispose();
					}
				}
				return this._importEngine;
			}
		}

		/// <summary>Gets a collection of all exports in this provider that match the conditions of the specified import.</summary>
		/// <param name="definition">The <see cref="T:System.ComponentModel.Composition.Primitives.ImportDefinition" /> that defines the conditions of the <see cref="T:System.ComponentModel.Composition.Primitives.Export" /> to get.</param>
		/// <param name="atomicComposition">The composition transaction to use, or <see langword="null" /> to disable transactional composition.</param>
		/// <returns>A collection of all exports in this provider that match the specified conditions.</returns>
		// Token: 0x060004F7 RID: 1271 RVA: 0x0000E074 File Offset: 0x0000C274
		protected override IEnumerable<Export> GetExportsCore(ImportDefinition definition, AtomicComposition atomicComposition)
		{
			this.ThrowIfDisposed();
			this.EnsureRunning();
			List<ComposablePart> list = null;
			using (this._lock.LockStateForRead())
			{
				list = atomicComposition.GetValueAllowNull(this, this._parts);
			}
			if (list.Count == 0)
			{
				return null;
			}
			List<Export> list2 = new List<Export>();
			foreach (ComposablePart composablePart in list)
			{
				foreach (ExportDefinition exportDefinition in composablePart.ExportDefinitions)
				{
					if (definition.IsConstraintSatisfiedBy(exportDefinition))
					{
						list2.Add(this.CreateExport(composablePart, exportDefinition));
					}
				}
			}
			return list2;
		}

		/// <summary>Executes composition on the specified batch.</summary>
		/// <param name="batch">The batch to execute composition on.</param>
		/// <exception cref="T:System.InvalidOperationException">The container is already in the process of composing.</exception>
		// Token: 0x060004F8 RID: 1272 RVA: 0x0000E164 File Offset: 0x0000C364
		public void Compose(CompositionBatch batch)
		{
			this.ThrowIfDisposed();
			this.EnsureRunning();
			Requires.NotNull<CompositionBatch>(batch, "batch");
			if (batch.PartsToAdd.Count == 0 && batch.PartsToRemove.Count == 0)
			{
				return;
			}
			CompositionResult compositionResult = CompositionResult.SucceededResult;
			List<ComposablePart> updatedPartsList = this.GetUpdatedPartsList(ref batch);
			using (AtomicComposition atomicComposition = new AtomicComposition())
			{
				if (this._currentlyComposing)
				{
					throw new InvalidOperationException(Strings.ReentrantCompose);
				}
				this._currentlyComposing = true;
				try
				{
					atomicComposition.SetValue(this, updatedPartsList);
					this.Recompose(batch, atomicComposition);
					foreach (ComposablePart part2 in batch.PartsToAdd)
					{
						try
						{
							this.ImportEngine.PreviewImports(part2, atomicComposition);
						}
						catch (ChangeRejectedException ex)
						{
							compositionResult = compositionResult.MergeResult(new CompositionResult(ex.Errors));
						}
					}
					compositionResult.ThrowOnErrors(atomicComposition);
					using (this._lock.LockStateForWrite())
					{
						this._parts = updatedPartsList;
					}
					atomicComposition.Complete();
				}
				finally
				{
					this._currentlyComposing = false;
				}
			}
			using (IEnumerator<ComposablePart> enumerator = batch.PartsToAdd.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					ComposablePart part = enumerator.Current;
					compositionResult = compositionResult.MergeResult(CompositionServices.TryInvoke(delegate
					{
						this.ImportEngine.SatisfyImports(part);
					}));
				}
			}
			compositionResult.ThrowOnErrors();
		}

		// Token: 0x060004F9 RID: 1273 RVA: 0x0000E32C File Offset: 0x0000C52C
		private List<ComposablePart> GetUpdatedPartsList(ref CompositionBatch batch)
		{
			Assumes.NotNull<CompositionBatch>(batch);
			List<ComposablePart> list = null;
			using (this._lock.LockStateForRead())
			{
				list = this._parts.ToList<ComposablePart>();
			}
			foreach (ComposablePart item in batch.PartsToAdd)
			{
				list.Add(item);
			}
			List<ComposablePart> list2 = null;
			foreach (ComposablePart item2 in batch.PartsToRemove)
			{
				if (list.Remove(item2))
				{
					if (list2 == null)
					{
						list2 = new List<ComposablePart>();
					}
					list2.Add(item2);
				}
			}
			batch = new CompositionBatch(batch.PartsToAdd, list2);
			return list;
		}

		// Token: 0x060004FA RID: 1274 RVA: 0x0000E41C File Offset: 0x0000C61C
		private void Recompose(CompositionBatch batch, AtomicComposition atomicComposition)
		{
			ComposablePartExportProvider.<>c__DisplayClass21_0 CS$<>8__locals1 = new ComposablePartExportProvider.<>c__DisplayClass21_0();
			CS$<>8__locals1.<>4__this = this;
			Assumes.NotNull<CompositionBatch>(batch);
			foreach (ComposablePart part2 in batch.PartsToRemove)
			{
				this.ImportEngine.ReleaseImports(part2, atomicComposition);
			}
			ComposablePartExportProvider.<>c__DisplayClass21_0 CS$<>8__locals2 = CS$<>8__locals1;
			IEnumerable<ExportDefinition> addedExports;
			if (batch.PartsToAdd.Count == 0)
			{
				addedExports = new ExportDefinition[0];
			}
			else
			{
				addedExports = batch.PartsToAdd.SelectMany((ComposablePart part) => part.ExportDefinitions).ToArray<ExportDefinition>();
			}
			CS$<>8__locals2.addedExports = addedExports;
			ComposablePartExportProvider.<>c__DisplayClass21_0 CS$<>8__locals3 = CS$<>8__locals1;
			IEnumerable<ExportDefinition> removedExports;
			if (batch.PartsToRemove.Count == 0)
			{
				removedExports = new ExportDefinition[0];
			}
			else
			{
				removedExports = batch.PartsToRemove.SelectMany((ComposablePart part) => part.ExportDefinitions).ToArray<ExportDefinition>();
			}
			CS$<>8__locals3.removedExports = removedExports;
			this.OnExportsChanging(new ExportsChangeEventArgs(CS$<>8__locals1.addedExports, CS$<>8__locals1.removedExports, atomicComposition));
			atomicComposition.AddCompleteAction(delegate
			{
				CS$<>8__locals1.<>4__this.OnExportsChanged(new ExportsChangeEventArgs(CS$<>8__locals1.addedExports, CS$<>8__locals1.removedExports, null));
			});
		}

		// Token: 0x060004FB RID: 1275 RVA: 0x0000E540 File Offset: 0x0000C740
		private Export CreateExport(ComposablePart part, ExportDefinition export)
		{
			return new Export(export, () => this.GetExportedValue(part, export));
		}

		// Token: 0x060004FC RID: 1276 RVA: 0x0000E57F File Offset: 0x0000C77F
		private object GetExportedValue(ComposablePart part, ExportDefinition export)
		{
			this.ThrowIfDisposed();
			this.EnsureRunning();
			return CompositionServices.GetExportedValueFromComposedPart(this.ImportEngine, part, export);
		}

		// Token: 0x060004FD RID: 1277 RVA: 0x0000E59A File Offset: 0x0000C79A
		[DebuggerStepThrough]
		private void ThrowIfDisposed()
		{
			if (this._isDisposed)
			{
				throw new ObjectDisposedException(base.GetType().Name);
			}
		}

		// Token: 0x060004FE RID: 1278 RVA: 0x0000E5B7 File Offset: 0x0000C7B7
		[DebuggerStepThrough]
		private void EnsureCanRun()
		{
			if (this._sourceProvider == null)
			{
				throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, Strings.ObjectMustBeInitialized, "SourceProvider"));
			}
		}

		// Token: 0x060004FF RID: 1279 RVA: 0x0000E5DC File Offset: 0x0000C7DC
		[DebuggerStepThrough]
		private void EnsureRunning()
		{
			if (!this._isRunning)
			{
				using (this._lock.LockStateForWrite())
				{
					if (!this._isRunning)
					{
						this.EnsureCanRun();
						this._isRunning = true;
					}
				}
			}
		}

		// Token: 0x06000500 RID: 1280 RVA: 0x0000E634 File Offset: 0x0000C834
		[DebuggerStepThrough]
		private void EnsureCanSet<T>(T currentValue) where T : class
		{
			if (this._isRunning || currentValue != null)
			{
				throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, Strings.ObjectAlreadyInitialized, Array.Empty<object>()));
			}
		}

		// Token: 0x040001F1 RID: 497
		private List<ComposablePart> _parts = new List<ComposablePart>();

		// Token: 0x040001F2 RID: 498
		private volatile bool _isDisposed;

		// Token: 0x040001F3 RID: 499
		private volatile bool _isRunning;

		// Token: 0x040001F4 RID: 500
		private CompositionLock _lock;

		// Token: 0x040001F5 RID: 501
		private ExportProvider _sourceProvider;

		// Token: 0x040001F6 RID: 502
		private ImportEngine _importEngine;

		// Token: 0x040001F7 RID: 503
		private volatile bool _currentlyComposing;

		// Token: 0x040001F8 RID: 504
		private CompositionOptions _compositionOptions;

		// Token: 0x020000C3 RID: 195
		[CompilerGenerated]
		private sealed class <>c__DisplayClass19_0
		{
			// Token: 0x06000501 RID: 1281 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass19_0()
			{
			}

			// Token: 0x06000502 RID: 1282 RVA: 0x0000E662 File Offset: 0x0000C862
			internal void <Compose>b__0()
			{
				this.<>4__this.ImportEngine.SatisfyImports(this.part);
			}

			// Token: 0x040001F9 RID: 505
			public ComposablePart part;

			// Token: 0x040001FA RID: 506
			public ComposablePartExportProvider <>4__this;
		}

		// Token: 0x020000C4 RID: 196
		[CompilerGenerated]
		private sealed class <>c__DisplayClass21_0
		{
			// Token: 0x06000503 RID: 1283 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass21_0()
			{
			}

			// Token: 0x06000504 RID: 1284 RVA: 0x0000E67A File Offset: 0x0000C87A
			internal void <Recompose>b__2()
			{
				this.<>4__this.OnExportsChanged(new ExportsChangeEventArgs(this.addedExports, this.removedExports, null));
			}

			// Token: 0x040001FB RID: 507
			public ComposablePartExportProvider <>4__this;

			// Token: 0x040001FC RID: 508
			public IEnumerable<ExportDefinition> addedExports;

			// Token: 0x040001FD RID: 509
			public IEnumerable<ExportDefinition> removedExports;
		}

		// Token: 0x020000C5 RID: 197
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000505 RID: 1285 RVA: 0x0000E699 File Offset: 0x0000C899
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000506 RID: 1286 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c()
			{
			}

			// Token: 0x06000507 RID: 1287 RVA: 0x0000E6A5 File Offset: 0x0000C8A5
			internal IEnumerable<ExportDefinition> <Recompose>b__21_0(ComposablePart part)
			{
				return part.ExportDefinitions;
			}

			// Token: 0x06000508 RID: 1288 RVA: 0x0000E6A5 File Offset: 0x0000C8A5
			internal IEnumerable<ExportDefinition> <Recompose>b__21_1(ComposablePart part)
			{
				return part.ExportDefinitions;
			}

			// Token: 0x040001FE RID: 510
			public static readonly ComposablePartExportProvider.<>c <>9 = new ComposablePartExportProvider.<>c();

			// Token: 0x040001FF RID: 511
			public static Func<ComposablePart, IEnumerable<ExportDefinition>> <>9__21_0;

			// Token: 0x04000200 RID: 512
			public static Func<ComposablePart, IEnumerable<ExportDefinition>> <>9__21_1;
		}

		// Token: 0x020000C6 RID: 198
		[CompilerGenerated]
		private sealed class <>c__DisplayClass22_0
		{
			// Token: 0x06000509 RID: 1289 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass22_0()
			{
			}

			// Token: 0x0600050A RID: 1290 RVA: 0x0000E6AD File Offset: 0x0000C8AD
			internal object <CreateExport>b__0()
			{
				return this.<>4__this.GetExportedValue(this.part, this.export);
			}

			// Token: 0x04000201 RID: 513
			public ComposablePartExportProvider <>4__this;

			// Token: 0x04000202 RID: 514
			public ComposablePart part;

			// Token: 0x04000203 RID: 515
			public ExportDefinition export;
		}
	}
}
