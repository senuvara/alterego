﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.AttributedModel;
using System.ComponentModel.Composition.Primitives;
using System.ComponentModel.Composition.ReflectionModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using Microsoft.Internal;
using Microsoft.Internal.Collections;

namespace System.ComponentModel.Composition.Hosting
{
	/// <summary>Discovers attributed parts from a collection of types.</summary>
	// Token: 0x020000F8 RID: 248
	[DebuggerTypeProxy(typeof(ComposablePartCatalogDebuggerProxy))]
	public class TypeCatalog : ComposablePartCatalog, ICompositionElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.TypeCatalog" /> class with the specified types.</summary>
		/// <param name="types">An array of attributed <see cref="T:System.Type" /> objects to add to the <see cref="T:System.ComponentModel.Composition.Hosting.TypeCatalog" /> object.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="types" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="types" /> contains an element that is <see langword="null" />.-or-
		///         <paramref name="types" /> contains an element that was loaded in the reflection-only context.</exception>
		// Token: 0x06000676 RID: 1654 RVA: 0x00013571 File Offset: 0x00011771
		public TypeCatalog(params Type[] types) : this(types)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.TypeCatalog" /> class with the specified types.</summary>
		/// <param name="types">A collection of attributed <see cref="T:System.Type" /> objects to add to the <see cref="T:System.ComponentModel.Composition.Hosting.TypeCatalog" /> object.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="types" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="types" /> contains an element that is <see langword="null" />.-or-
		///         <paramref name="types" /> contains an element that was loaded in the reflection-only context.</exception>
		// Token: 0x06000677 RID: 1655 RVA: 0x0001357C File Offset: 0x0001177C
		public TypeCatalog(IEnumerable<Type> types)
		{
			this._thisLock = new object();
			base..ctor();
			Requires.NotNull<IEnumerable<Type>>(types, "types");
			this.InitializeTypeCatalog(types);
			this._definitionOrigin = this;
			this._contractPartIndex = new Lazy<IDictionary<string, List<ComposablePartDefinition>>>(new Func<IDictionary<string, List<ComposablePartDefinition>>>(this.CreateIndex), true);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.TypeCatalog" /> class with the specified types and source for parts.</summary>
		/// <param name="types">A collection of attributed <see cref="T:System.Type" /> objects to add to the <see cref="T:System.ComponentModel.Composition.Hosting.TypeCatalog" /> object.</param>
		/// <param name="definitionOrigin">An element used by diagnostics to identify the source for parts.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="types" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="types" /> contains an element that is <see langword="null" />.-or-
		///         <paramref name="types" /> contains an element that was loaded in the reflection-only context.</exception>
		// Token: 0x06000678 RID: 1656 RVA: 0x000135CC File Offset: 0x000117CC
		public TypeCatalog(IEnumerable<Type> types, ICompositionElement definitionOrigin)
		{
			this._thisLock = new object();
			base..ctor();
			Requires.NotNull<IEnumerable<Type>>(types, "types");
			Requires.NotNull<ICompositionElement>(definitionOrigin, "definitionOrigin");
			this.InitializeTypeCatalog(types);
			this._definitionOrigin = definitionOrigin;
			this._contractPartIndex = new Lazy<IDictionary<string, List<ComposablePartDefinition>>>(new Func<IDictionary<string, List<ComposablePartDefinition>>>(this.CreateIndex), true);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.TypeCatalog" /> class with the specified types in the specified reflection context.</summary>
		/// <param name="types">A collection of attributed <see cref="T:System.Type" /> objects to add to the <see cref="T:System.ComponentModel.Composition.Hosting.TypeCatalog" /> object.</param>
		/// <param name="reflectionContext">The context used to interpret the types.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="types" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="types" /> contains an element that is <see langword="null" />.-or-
		///         <paramref name="types" /> contains an element that was loaded in the reflection-only context.</exception>
		// Token: 0x06000679 RID: 1657 RVA: 0x00013628 File Offset: 0x00011828
		public TypeCatalog(IEnumerable<Type> types, ReflectionContext reflectionContext)
		{
			this._thisLock = new object();
			base..ctor();
			Requires.NotNull<IEnumerable<Type>>(types, "types");
			Requires.NotNull<ReflectionContext>(reflectionContext, "reflectionContext");
			this.InitializeTypeCatalog(types, reflectionContext);
			this._definitionOrigin = this;
			this._contractPartIndex = new Lazy<IDictionary<string, List<ComposablePartDefinition>>>(new Func<IDictionary<string, List<ComposablePartDefinition>>>(this.CreateIndex), true);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.TypeCatalog" /> class with the specified types in the specified reflection context and source for parts.</summary>
		/// <param name="types">A collection of attributed <see cref="T:System.Type" /> objects to add to the <see cref="T:System.ComponentModel.Composition.Hosting.TypeCatalog" /> object.</param>
		/// <param name="reflectionContext">The context used to interpret the types.</param>
		/// <param name="definitionOrigin">An element used by diagnostics to identify the source for parts.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="types" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="types" /> contains an element that is <see langword="null" />.-or-
		///         <paramref name="types" /> contains an element that was loaded in the reflection-only context.</exception>
		// Token: 0x0600067A RID: 1658 RVA: 0x00013684 File Offset: 0x00011884
		public TypeCatalog(IEnumerable<Type> types, ReflectionContext reflectionContext, ICompositionElement definitionOrigin)
		{
			this._thisLock = new object();
			base..ctor();
			Requires.NotNull<IEnumerable<Type>>(types, "types");
			Requires.NotNull<ReflectionContext>(reflectionContext, "reflectionContext");
			Requires.NotNull<ICompositionElement>(definitionOrigin, "definitionOrigin");
			this.InitializeTypeCatalog(types, reflectionContext);
			this._definitionOrigin = definitionOrigin;
			this._contractPartIndex = new Lazy<IDictionary<string, List<ComposablePartDefinition>>>(new Func<IDictionary<string, List<ComposablePartDefinition>>>(this.CreateIndex), true);
		}

		// Token: 0x0600067B RID: 1659 RVA: 0x000136EC File Offset: 0x000118EC
		private void InitializeTypeCatalog(IEnumerable<Type> types, ReflectionContext reflectionContext)
		{
			List<Type> list = new List<Type>();
			foreach (Type type in types)
			{
				if (type == null)
				{
					throw ExceptionBuilder.CreateContainsNullElement("types");
				}
				if (type.Assembly.ReflectionOnly)
				{
					throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, Strings.Argument_ElementReflectionOnlyType, "types"), "types");
				}
				TypeInfo typeInfo = type.GetTypeInfo();
				TypeInfo typeInfo2 = (reflectionContext != null) ? reflectionContext.MapType(typeInfo) : typeInfo;
				if (typeInfo2 != null)
				{
					if (typeInfo2.Assembly.ReflectionOnly)
					{
						throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, Strings.Argument_ReflectionContextReturnsReflectionOnlyType, "reflectionContext"), "reflectionContext");
					}
					list.Add(typeInfo2);
				}
			}
			this._types = list.ToArray();
		}

		// Token: 0x0600067C RID: 1660 RVA: 0x000137D4 File Offset: 0x000119D4
		private void InitializeTypeCatalog(IEnumerable<Type> types)
		{
			using (IEnumerator<Type> enumerator = types.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					if (enumerator.Current == null)
					{
						throw ExceptionBuilder.CreateContainsNullElement("types");
					}
				}
			}
			this._types = types.ToArray<Type>();
		}

		/// <summary>Returns an enumerator that iterates through the catalog.</summary>
		/// <returns>An enumerator that can be used to iterate through the catalog.</returns>
		// Token: 0x0600067D RID: 1661 RVA: 0x00013834 File Offset: 0x00011A34
		public override IEnumerator<ComposablePartDefinition> GetEnumerator()
		{
			this.ThrowIfDisposed();
			return this.PartsInternal.GetEnumerator();
		}

		/// <summary>Gets the display name of the type catalog.</summary>
		/// <returns>A string containing a human-readable display name of the <see cref="T:System.ComponentModel.Composition.Hosting.TypeCatalog" />.</returns>
		// Token: 0x17000187 RID: 391
		// (get) Token: 0x0600067E RID: 1662 RVA: 0x00013847 File Offset: 0x00011A47
		string ICompositionElement.DisplayName
		{
			get
			{
				return this.GetDisplayName();
			}
		}

		/// <summary>Gets the composition element from which the type catalog originated.</summary>
		/// <returns>Always <see langword="null" />. </returns>
		// Token: 0x17000188 RID: 392
		// (get) Token: 0x0600067F RID: 1663 RVA: 0x00009EC0 File Offset: 0x000080C0
		ICompositionElement ICompositionElement.Origin
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000189 RID: 393
		// (get) Token: 0x06000680 RID: 1664 RVA: 0x00013850 File Offset: 0x00011A50
		private IEnumerable<ComposablePartDefinition> PartsInternal
		{
			get
			{
				if (this._parts == null)
				{
					object thisLock = this._thisLock;
					lock (thisLock)
					{
						if (this._parts == null)
						{
							Assumes.NotNull<Type[]>(this._types);
							List<ComposablePartDefinition> list = new List<ComposablePartDefinition>();
							Type[] types = this._types;
							for (int i = 0; i < types.Length; i++)
							{
								ComposablePartDefinition composablePartDefinition = AttributedModelDiscovery.CreatePartDefinitionIfDiscoverable(types[i], this._definitionOrigin);
								if (composablePartDefinition != null)
								{
									list.Add(composablePartDefinition);
								}
							}
							Thread.MemoryBarrier();
							this._types = null;
							this._parts = list;
						}
					}
				}
				return this._parts;
			}
		}

		// Token: 0x06000681 RID: 1665 RVA: 0x00013908 File Offset: 0x00011B08
		internal override IEnumerable<ComposablePartDefinition> GetCandidateParts(ImportDefinition definition)
		{
			Assumes.NotNull<ImportDefinition>(definition);
			string contractName = definition.ContractName;
			if (string.IsNullOrEmpty(contractName))
			{
				return this.PartsInternal;
			}
			string value = definition.Metadata.GetValue("System.ComponentModel.Composition.GenericContractName");
			ICollection<ComposablePartDefinition> candidateParts = this.GetCandidateParts(contractName);
			List<ComposablePartDefinition> candidateParts2 = this.GetCandidateParts(value);
			return candidateParts.ConcatAllowingNull(candidateParts2);
		}

		// Token: 0x06000682 RID: 1666 RVA: 0x00013958 File Offset: 0x00011B58
		private List<ComposablePartDefinition> GetCandidateParts(string contractName)
		{
			if (contractName == null)
			{
				return null;
			}
			List<ComposablePartDefinition> result = null;
			this._contractPartIndex.Value.TryGetValue(contractName, out result);
			return result;
		}

		// Token: 0x06000683 RID: 1667 RVA: 0x00013984 File Offset: 0x00011B84
		private IDictionary<string, List<ComposablePartDefinition>> CreateIndex()
		{
			Dictionary<string, List<ComposablePartDefinition>> dictionary = new Dictionary<string, List<ComposablePartDefinition>>(StringComparers.ContractName);
			foreach (ComposablePartDefinition composablePartDefinition in this.PartsInternal)
			{
				foreach (string key in (from export in composablePartDefinition.ExportDefinitions
				select export.ContractName).Distinct<string>())
				{
					List<ComposablePartDefinition> list = null;
					if (!dictionary.TryGetValue(key, out list))
					{
						list = new List<ComposablePartDefinition>();
						dictionary.Add(key, list);
					}
					list.Add(composablePartDefinition);
				}
			}
			return dictionary;
		}

		/// <summary>Returns a string representation of the type catalog.</summary>
		/// <returns>A string representation of the type catalog.</returns>
		// Token: 0x06000684 RID: 1668 RVA: 0x00013847 File Offset: 0x00011A47
		public override string ToString()
		{
			return this.GetDisplayName();
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.ComponentModel.Composition.Hosting.TypeCatalog" /> and optionally releases the managed resources. </summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x06000685 RID: 1669 RVA: 0x00013A64 File Offset: 0x00011C64
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				this._isDisposed = true;
			}
			base.Dispose(disposing);
		}

		// Token: 0x06000686 RID: 1670 RVA: 0x00013A79 File Offset: 0x00011C79
		private string GetDisplayName()
		{
			return string.Format(CultureInfo.CurrentCulture, Strings.TypeCatalog_DisplayNameFormat, base.GetType().Name, this.GetTypesDisplay());
		}

		// Token: 0x06000687 RID: 1671 RVA: 0x00013A9C File Offset: 0x00011C9C
		private string GetTypesDisplay()
		{
			int num = this.PartsInternal.Count<ComposablePartDefinition>();
			if (num == 0)
			{
				return Strings.TypeCatalog_Empty;
			}
			StringBuilder stringBuilder = new StringBuilder();
			foreach (ComposablePartDefinition composablePartDefinition in this.PartsInternal.Take(2))
			{
				ReflectionComposablePartDefinition reflectionComposablePartDefinition = (ReflectionComposablePartDefinition)composablePartDefinition;
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(CultureInfo.CurrentCulture.TextInfo.ListSeparator);
					stringBuilder.Append(" ");
				}
				stringBuilder.Append(reflectionComposablePartDefinition.GetPartType().GetDisplayName());
			}
			if (num > 2)
			{
				stringBuilder.Append(CultureInfo.CurrentCulture.TextInfo.ListSeparator);
				stringBuilder.Append(" ...");
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000688 RID: 1672 RVA: 0x00013B74 File Offset: 0x00011D74
		[DebuggerStepThrough]
		private void ThrowIfDisposed()
		{
			if (this._isDisposed)
			{
				throw ExceptionBuilder.CreateObjectDisposed(this);
			}
		}

		// Token: 0x040002B0 RID: 688
		private readonly object _thisLock;

		// Token: 0x040002B1 RID: 689
		private Type[] _types;

		// Token: 0x040002B2 RID: 690
		private volatile List<ComposablePartDefinition> _parts;

		// Token: 0x040002B3 RID: 691
		private volatile bool _isDisposed;

		// Token: 0x040002B4 RID: 692
		private readonly ICompositionElement _definitionOrigin;

		// Token: 0x040002B5 RID: 693
		private readonly Lazy<IDictionary<string, List<ComposablePartDefinition>>> _contractPartIndex;

		// Token: 0x020000F9 RID: 249
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000689 RID: 1673 RVA: 0x00013B87 File Offset: 0x00011D87
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x0600068A RID: 1674 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c()
			{
			}

			// Token: 0x0600068B RID: 1675 RVA: 0x000111DD File Offset: 0x0000F3DD
			internal string <CreateIndex>b__22_0(ExportDefinition export)
			{
				return export.ContractName;
			}

			// Token: 0x040002B6 RID: 694
			public static readonly TypeCatalog.<>c <>9 = new TypeCatalog.<>c();

			// Token: 0x040002B7 RID: 695
			public static Func<ExportDefinition, string> <>9__22_0;
		}
	}
}
