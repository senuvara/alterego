﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using Microsoft.Internal;
using Microsoft.Internal.Collections;

namespace System.ComponentModel.Composition.Hosting
{
	// Token: 0x020000BD RID: 189
	internal class ComposablePartCatalogCollection : ICollection<ComposablePartCatalog>, IEnumerable<ComposablePartCatalog>, IEnumerable, INotifyComposablePartCatalogChanged, IDisposable
	{
		// Token: 0x060004C9 RID: 1225 RVA: 0x0000D4E8 File Offset: 0x0000B6E8
		public ComposablePartCatalogCollection(IEnumerable<ComposablePartCatalog> catalogs, Action<ComposablePartCatalogChangeEventArgs> onChanged, Action<ComposablePartCatalogChangeEventArgs> onChanging)
		{
			catalogs = (catalogs ?? Enumerable.Empty<ComposablePartCatalog>());
			this._catalogs = new List<ComposablePartCatalog>(catalogs);
			this._onChanged = onChanged;
			this._onChanging = onChanging;
			this.SubscribeToCatalogNotifications(catalogs);
		}

		// Token: 0x060004CA RID: 1226 RVA: 0x0000D540 File Offset: 0x0000B740
		public void Add(ComposablePartCatalog item)
		{
			Requires.NotNull<ComposablePartCatalog>(item, "item");
			this.ThrowIfDisposed();
			Lazy<IEnumerable<ComposablePartDefinition>> addedDefinitions = new Lazy<IEnumerable<ComposablePartDefinition>>(() => item.ToArray<ComposablePartDefinition>(), LazyThreadSafetyMode.PublicationOnly);
			using (AtomicComposition atomicComposition = new AtomicComposition())
			{
				this.RaiseChangingEvent(addedDefinitions, null, atomicComposition);
				using (new WriteLock(this._lock))
				{
					if (this._isCopyNeeded)
					{
						this._catalogs = new List<ComposablePartCatalog>(this._catalogs);
						this._isCopyNeeded = false;
					}
					this._hasChanged = true;
					this._catalogs.Add(item);
				}
				this.SubscribeToCatalogNotifications(item);
				atomicComposition.Complete();
			}
			this.RaiseChangedEvent(addedDefinitions, null);
		}

		// Token: 0x14000003 RID: 3
		// (add) Token: 0x060004CB RID: 1227 RVA: 0x0000D62C File Offset: 0x0000B82C
		// (remove) Token: 0x060004CC RID: 1228 RVA: 0x0000D664 File Offset: 0x0000B864
		public event EventHandler<ComposablePartCatalogChangeEventArgs> Changed
		{
			[CompilerGenerated]
			add
			{
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler = this.Changed;
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<ComposablePartCatalogChangeEventArgs> value2 = (EventHandler<ComposablePartCatalogChangeEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<ComposablePartCatalogChangeEventArgs>>(ref this.Changed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler = this.Changed;
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<ComposablePartCatalogChangeEventArgs> value2 = (EventHandler<ComposablePartCatalogChangeEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<ComposablePartCatalogChangeEventArgs>>(ref this.Changed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x14000004 RID: 4
		// (add) Token: 0x060004CD RID: 1229 RVA: 0x0000D69C File Offset: 0x0000B89C
		// (remove) Token: 0x060004CE RID: 1230 RVA: 0x0000D6D4 File Offset: 0x0000B8D4
		public event EventHandler<ComposablePartCatalogChangeEventArgs> Changing
		{
			[CompilerGenerated]
			add
			{
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler = this.Changing;
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<ComposablePartCatalogChangeEventArgs> value2 = (EventHandler<ComposablePartCatalogChangeEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<ComposablePartCatalogChangeEventArgs>>(ref this.Changing, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler = this.Changing;
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<ComposablePartCatalogChangeEventArgs> value2 = (EventHandler<ComposablePartCatalogChangeEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<ComposablePartCatalogChangeEventArgs>>(ref this.Changing, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x060004CF RID: 1231 RVA: 0x0000D70C File Offset: 0x0000B90C
		public void Clear()
		{
			this.ThrowIfDisposed();
			ComposablePartCatalog[] catalogs = null;
			using (new ReadLock(this._lock))
			{
				if (this._catalogs.Count == 0)
				{
					return;
				}
				catalogs = this._catalogs.ToArray();
			}
			Lazy<IEnumerable<ComposablePartDefinition>> removedDefinitions = new Lazy<IEnumerable<ComposablePartDefinition>>(() => catalogs.SelectMany((ComposablePartCatalog catalog) => catalog).ToArray<ComposablePartDefinition>(), LazyThreadSafetyMode.PublicationOnly);
			using (AtomicComposition atomicComposition = new AtomicComposition())
			{
				this.RaiseChangingEvent(null, removedDefinitions, atomicComposition);
				this.UnsubscribeFromCatalogNotifications(catalogs);
				using (new WriteLock(this._lock))
				{
					this._catalogs = new List<ComposablePartCatalog>();
					this._isCopyNeeded = false;
					this._hasChanged = true;
				}
				atomicComposition.Complete();
			}
			this.RaiseChangedEvent(null, removedDefinitions);
		}

		// Token: 0x060004D0 RID: 1232 RVA: 0x0000D814 File Offset: 0x0000BA14
		public bool Contains(ComposablePartCatalog item)
		{
			Requires.NotNull<ComposablePartCatalog>(item, "item");
			this.ThrowIfDisposed();
			bool result;
			using (new ReadLock(this._lock))
			{
				result = this._catalogs.Contains(item);
			}
			return result;
		}

		// Token: 0x060004D1 RID: 1233 RVA: 0x0000D870 File Offset: 0x0000BA70
		public void CopyTo(ComposablePartCatalog[] array, int arrayIndex)
		{
			this.ThrowIfDisposed();
			using (new ReadLock(this._lock))
			{
				this._catalogs.CopyTo(array, arrayIndex);
			}
		}

		// Token: 0x17000155 RID: 341
		// (get) Token: 0x060004D2 RID: 1234 RVA: 0x0000D8C0 File Offset: 0x0000BAC0
		public int Count
		{
			get
			{
				this.ThrowIfDisposed();
				int count;
				using (new ReadLock(this._lock))
				{
					count = this._catalogs.Count;
				}
				return count;
			}
		}

		// Token: 0x17000156 RID: 342
		// (get) Token: 0x060004D3 RID: 1235 RVA: 0x0000D910 File Offset: 0x0000BB10
		public bool IsReadOnly
		{
			get
			{
				this.ThrowIfDisposed();
				return false;
			}
		}

		// Token: 0x060004D4 RID: 1236 RVA: 0x0000D91C File Offset: 0x0000BB1C
		public bool Remove(ComposablePartCatalog item)
		{
			Requires.NotNull<ComposablePartCatalog>(item, "item");
			this.ThrowIfDisposed();
			using (new ReadLock(this._lock))
			{
				if (!this._catalogs.Contains(item))
				{
					return false;
				}
			}
			bool flag = false;
			Lazy<IEnumerable<ComposablePartDefinition>> removedDefinitions = new Lazy<IEnumerable<ComposablePartDefinition>>(() => item.ToArray<ComposablePartDefinition>(), LazyThreadSafetyMode.PublicationOnly);
			using (AtomicComposition atomicComposition = new AtomicComposition())
			{
				this.RaiseChangingEvent(null, removedDefinitions, atomicComposition);
				using (new WriteLock(this._lock))
				{
					if (this._isCopyNeeded)
					{
						this._catalogs = new List<ComposablePartCatalog>(this._catalogs);
						this._isCopyNeeded = false;
					}
					flag = this._catalogs.Remove(item);
					if (flag)
					{
						this._hasChanged = true;
					}
				}
				this.UnsubscribeFromCatalogNotifications(item);
				atomicComposition.Complete();
			}
			this.RaiseChangedEvent(null, removedDefinitions);
			return flag;
		}

		// Token: 0x17000157 RID: 343
		// (get) Token: 0x060004D5 RID: 1237 RVA: 0x0000DA5C File Offset: 0x0000BC5C
		internal bool HasChanged
		{
			get
			{
				this.ThrowIfDisposed();
				bool hasChanged;
				using (new ReadLock(this._lock))
				{
					hasChanged = this._hasChanged;
				}
				return hasChanged;
			}
		}

		// Token: 0x060004D6 RID: 1238 RVA: 0x0000DAA4 File Offset: 0x0000BCA4
		public IEnumerator<ComposablePartCatalog> GetEnumerator()
		{
			this.ThrowIfDisposed();
			IEnumerator<ComposablePartCatalog> result;
			using (new WriteLock(this._lock))
			{
				IEnumerator<ComposablePartCatalog> enumerator = this._catalogs.GetEnumerator();
				this._isCopyNeeded = true;
				result = enumerator;
			}
			return result;
		}

		// Token: 0x060004D7 RID: 1239 RVA: 0x0000DB00 File Offset: 0x0000BD00
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x060004D8 RID: 1240 RVA: 0x0000DB08 File Offset: 0x0000BD08
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x060004D9 RID: 1241 RVA: 0x0000DB18 File Offset: 0x0000BD18
		protected virtual void Dispose(bool disposing)
		{
			if (disposing && !this._isDisposed)
			{
				bool flag = false;
				IEnumerable<ComposablePartCatalog> enumerable = null;
				try
				{
					using (new WriteLock(this._lock))
					{
						if (!this._isDisposed)
						{
							flag = true;
							enumerable = this._catalogs;
							this._catalogs = null;
							this._isDisposed = true;
						}
					}
				}
				finally
				{
					if (enumerable != null)
					{
						this.UnsubscribeFromCatalogNotifications(enumerable);
						enumerable.ForEach(delegate(ComposablePartCatalog catalog)
						{
							catalog.Dispose();
						});
					}
					if (flag)
					{
						this._lock.Dispose();
					}
				}
			}
		}

		// Token: 0x060004DA RID: 1242 RVA: 0x0000DBD8 File Offset: 0x0000BDD8
		private void RaiseChangedEvent(Lazy<IEnumerable<ComposablePartDefinition>> addedDefinitions, Lazy<IEnumerable<ComposablePartDefinition>> removedDefinitions)
		{
			if (this._onChanged == null || this.Changed == null)
			{
				return;
			}
			IEnumerable<ComposablePartDefinition> addedDefinitions2 = (addedDefinitions == null) ? Enumerable.Empty<ComposablePartDefinition>() : addedDefinitions.Value;
			IEnumerable<ComposablePartDefinition> removedDefinitions2 = (removedDefinitions == null) ? Enumerable.Empty<ComposablePartDefinition>() : removedDefinitions.Value;
			this._onChanged(new ComposablePartCatalogChangeEventArgs(addedDefinitions2, removedDefinitions2, null));
		}

		// Token: 0x060004DB RID: 1243 RVA: 0x0000DC2C File Offset: 0x0000BE2C
		public void OnChanged(object sender, ComposablePartCatalogChangeEventArgs e)
		{
			EventHandler<ComposablePartCatalogChangeEventArgs> changed = this.Changed;
			if (changed != null)
			{
				changed(sender, e);
			}
		}

		// Token: 0x060004DC RID: 1244 RVA: 0x0000DC4C File Offset: 0x0000BE4C
		private void RaiseChangingEvent(Lazy<IEnumerable<ComposablePartDefinition>> addedDefinitions, Lazy<IEnumerable<ComposablePartDefinition>> removedDefinitions, AtomicComposition atomicComposition)
		{
			if (this._onChanging == null || this.Changing == null)
			{
				return;
			}
			IEnumerable<ComposablePartDefinition> addedDefinitions2 = (addedDefinitions == null) ? Enumerable.Empty<ComposablePartDefinition>() : addedDefinitions.Value;
			IEnumerable<ComposablePartDefinition> removedDefinitions2 = (removedDefinitions == null) ? Enumerable.Empty<ComposablePartDefinition>() : removedDefinitions.Value;
			this._onChanging(new ComposablePartCatalogChangeEventArgs(addedDefinitions2, removedDefinitions2, atomicComposition));
		}

		// Token: 0x060004DD RID: 1245 RVA: 0x0000DCA0 File Offset: 0x0000BEA0
		public void OnChanging(object sender, ComposablePartCatalogChangeEventArgs e)
		{
			EventHandler<ComposablePartCatalogChangeEventArgs> changing = this.Changing;
			if (changing != null)
			{
				changing(sender, e);
			}
		}

		// Token: 0x060004DE RID: 1246 RVA: 0x0000DCBF File Offset: 0x0000BEBF
		private void OnContainedCatalogChanged(object sender, ComposablePartCatalogChangeEventArgs e)
		{
			if (this._onChanged == null || this.Changed == null)
			{
				return;
			}
			this._onChanged(e);
		}

		// Token: 0x060004DF RID: 1247 RVA: 0x0000DCDE File Offset: 0x0000BEDE
		private void OnContainedCatalogChanging(object sender, ComposablePartCatalogChangeEventArgs e)
		{
			if (this._onChanging == null || this.Changing == null)
			{
				return;
			}
			this._onChanging(e);
		}

		// Token: 0x060004E0 RID: 1248 RVA: 0x0000DD00 File Offset: 0x0000BF00
		private void SubscribeToCatalogNotifications(ComposablePartCatalog catalog)
		{
			INotifyComposablePartCatalogChanged notifyComposablePartCatalogChanged = catalog as INotifyComposablePartCatalogChanged;
			if (notifyComposablePartCatalogChanged != null)
			{
				notifyComposablePartCatalogChanged.Changed += this.OnContainedCatalogChanged;
				notifyComposablePartCatalogChanged.Changing += this.OnContainedCatalogChanging;
			}
		}

		// Token: 0x060004E1 RID: 1249 RVA: 0x0000DD3C File Offset: 0x0000BF3C
		private void SubscribeToCatalogNotifications(IEnumerable<ComposablePartCatalog> catalogs)
		{
			foreach (ComposablePartCatalog catalog in catalogs)
			{
				this.SubscribeToCatalogNotifications(catalog);
			}
		}

		// Token: 0x060004E2 RID: 1250 RVA: 0x0000DD84 File Offset: 0x0000BF84
		private void UnsubscribeFromCatalogNotifications(ComposablePartCatalog catalog)
		{
			INotifyComposablePartCatalogChanged notifyComposablePartCatalogChanged = catalog as INotifyComposablePartCatalogChanged;
			if (notifyComposablePartCatalogChanged != null)
			{
				notifyComposablePartCatalogChanged.Changed -= this.OnContainedCatalogChanged;
				notifyComposablePartCatalogChanged.Changing -= this.OnContainedCatalogChanging;
			}
		}

		// Token: 0x060004E3 RID: 1251 RVA: 0x0000DDC0 File Offset: 0x0000BFC0
		private void UnsubscribeFromCatalogNotifications(IEnumerable<ComposablePartCatalog> catalogs)
		{
			foreach (ComposablePartCatalog catalog in catalogs)
			{
				this.UnsubscribeFromCatalogNotifications(catalog);
			}
		}

		// Token: 0x060004E4 RID: 1252 RVA: 0x0000DE08 File Offset: 0x0000C008
		private void ThrowIfDisposed()
		{
			if (this._isDisposed)
			{
				throw ExceptionBuilder.CreateObjectDisposed(this);
			}
		}

		// Token: 0x040001E2 RID: 482
		private readonly Lock _lock = new Lock();

		// Token: 0x040001E3 RID: 483
		private Action<ComposablePartCatalogChangeEventArgs> _onChanged;

		// Token: 0x040001E4 RID: 484
		private Action<ComposablePartCatalogChangeEventArgs> _onChanging;

		// Token: 0x040001E5 RID: 485
		private List<ComposablePartCatalog> _catalogs = new List<ComposablePartCatalog>();

		// Token: 0x040001E6 RID: 486
		private volatile bool _isCopyNeeded;

		// Token: 0x040001E7 RID: 487
		private volatile bool _isDisposed;

		// Token: 0x040001E8 RID: 488
		private bool _hasChanged;

		// Token: 0x040001E9 RID: 489
		[CompilerGenerated]
		private EventHandler<ComposablePartCatalogChangeEventArgs> Changed;

		// Token: 0x040001EA RID: 490
		[CompilerGenerated]
		private EventHandler<ComposablePartCatalogChangeEventArgs> Changing;

		// Token: 0x020000BE RID: 190
		[CompilerGenerated]
		private sealed class <>c__DisplayClass8_0
		{
			// Token: 0x060004E5 RID: 1253 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass8_0()
			{
			}

			// Token: 0x060004E6 RID: 1254 RVA: 0x0000DE1B File Offset: 0x0000C01B
			internal IEnumerable<ComposablePartDefinition> <Add>b__0()
			{
				return this.item.ToArray<ComposablePartDefinition>();
			}

			// Token: 0x040001EB RID: 491
			public ComposablePartCatalog item;
		}

		// Token: 0x020000BF RID: 191
		[CompilerGenerated]
		private sealed class <>c__DisplayClass15_0
		{
			// Token: 0x060004E7 RID: 1255 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass15_0()
			{
			}

			// Token: 0x060004E8 RID: 1256 RVA: 0x0000DE28 File Offset: 0x0000C028
			internal IEnumerable<ComposablePartDefinition> <Clear>b__0()
			{
				return this.catalogs.SelectMany(new Func<ComposablePartCatalog, IEnumerable<ComposablePartDefinition>>(ComposablePartCatalogCollection.<>c.<>9.<Clear>b__15_1)).ToArray<ComposablePartDefinition>();
			}

			// Token: 0x040001EC RID: 492
			public ComposablePartCatalog[] catalogs;
		}

		// Token: 0x020000C0 RID: 192
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x060004E9 RID: 1257 RVA: 0x0000DE59 File Offset: 0x0000C059
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x060004EA RID: 1258 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c()
			{
			}

			// Token: 0x060004EB RID: 1259 RVA: 0x00009CD6 File Offset: 0x00007ED6
			internal IEnumerable<ComposablePartDefinition> <Clear>b__15_1(ComposablePartCatalog catalog)
			{
				return catalog;
			}

			// Token: 0x060004EC RID: 1260 RVA: 0x0000DE65 File Offset: 0x0000C065
			internal void <Dispose>b__28_0(ComposablePartCatalog catalog)
			{
				catalog.Dispose();
			}

			// Token: 0x040001ED RID: 493
			public static readonly ComposablePartCatalogCollection.<>c <>9 = new ComposablePartCatalogCollection.<>c();

			// Token: 0x040001EE RID: 494
			public static Func<ComposablePartCatalog, IEnumerable<ComposablePartDefinition>> <>9__15_1;

			// Token: 0x040001EF RID: 495
			public static Action<ComposablePartCatalog> <>9__28_0;
		}

		// Token: 0x020000C1 RID: 193
		[CompilerGenerated]
		private sealed class <>c__DisplayClass22_0
		{
			// Token: 0x060004ED RID: 1261 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass22_0()
			{
			}

			// Token: 0x060004EE RID: 1262 RVA: 0x0000DE6D File Offset: 0x0000C06D
			internal IEnumerable<ComposablePartDefinition> <Remove>b__0()
			{
				return this.item.ToArray<ComposablePartDefinition>();
			}

			// Token: 0x040001F0 RID: 496
			public ComposablePartCatalog item;
		}
	}
}
