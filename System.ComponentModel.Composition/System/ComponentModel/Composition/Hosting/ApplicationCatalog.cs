﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.Hosting
{
	/// <summary>Discovers attributed parts in the dynamic link library (DLL) and EXE files in an application's directory and path.</summary>
	// Token: 0x0200009E RID: 158
	public class ApplicationCatalog : ComposablePartCatalog, ICompositionElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.ApplicationCatalog" /> class.</summary>
		// Token: 0x0600041A RID: 1050 RVA: 0x0000AE4E File Offset: 0x0000904E
		public ApplicationCatalog()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.ApplicationCatalog" /> class by using the specified source for parts.</summary>
		/// <param name="definitionOrigin">The element used by diagnostics to identify the source for parts.</param>
		// Token: 0x0600041B RID: 1051 RVA: 0x0000AE61 File Offset: 0x00009061
		public ApplicationCatalog(ICompositionElement definitionOrigin)
		{
			Requires.NotNull<ICompositionElement>(definitionOrigin, "definitionOrigin");
			this._definitionOrigin = definitionOrigin;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.ApplicationCatalog" /> class by using the specified reflection context.</summary>
		/// <param name="reflectionContext">The reflection context.</param>
		// Token: 0x0600041C RID: 1052 RVA: 0x0000AE86 File Offset: 0x00009086
		public ApplicationCatalog(ReflectionContext reflectionContext)
		{
			Requires.NotNull<ReflectionContext>(reflectionContext, "reflectionContext");
			this._reflectionContext = reflectionContext;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.ApplicationCatalog" /> class by using the specified reflection context and source for parts.</summary>
		/// <param name="reflectionContext">The reflection context.</param>
		/// <param name="definitionOrigin">The element used by diagnostics to identify the source for parts.</param>
		// Token: 0x0600041D RID: 1053 RVA: 0x0000AEAB File Offset: 0x000090AB
		public ApplicationCatalog(ReflectionContext reflectionContext, ICompositionElement definitionOrigin)
		{
			Requires.NotNull<ReflectionContext>(reflectionContext, "reflectionContext");
			Requires.NotNull<ICompositionElement>(definitionOrigin, "definitionOrigin");
			this._reflectionContext = reflectionContext;
			this._definitionOrigin = definitionOrigin;
		}

		// Token: 0x0600041E RID: 1054 RVA: 0x0000AEE4 File Offset: 0x000090E4
		internal ComposablePartCatalog CreateCatalog(string location, string pattern)
		{
			if (this._reflectionContext != null)
			{
				if (this._definitionOrigin == null)
				{
					return new DirectoryCatalog(location, pattern, this._reflectionContext);
				}
				return new DirectoryCatalog(location, pattern, this._reflectionContext, this._definitionOrigin);
			}
			else
			{
				if (this._definitionOrigin == null)
				{
					return new DirectoryCatalog(location, pattern);
				}
				return new DirectoryCatalog(location, pattern, this._definitionOrigin);
			}
		}

		// Token: 0x17000138 RID: 312
		// (get) Token: 0x0600041F RID: 1055 RVA: 0x0000AF40 File Offset: 0x00009140
		private AggregateCatalog InnerCatalog
		{
			get
			{
				if (this._innerCatalog == null)
				{
					object thisLock = this._thisLock;
					lock (thisLock)
					{
						if (this._innerCatalog == null)
						{
							string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
							Assumes.NotNull<string>(baseDirectory);
							List<ComposablePartCatalog> list = new List<ComposablePartCatalog>();
							list.Add(this.CreateCatalog(baseDirectory, "*.exe"));
							list.Add(this.CreateCatalog(baseDirectory, "*.dll"));
							string relativeSearchPath = AppDomain.CurrentDomain.RelativeSearchPath;
							if (!string.IsNullOrEmpty(relativeSearchPath))
							{
								foreach (string path in relativeSearchPath.Split(new char[]
								{
									';'
								}, StringSplitOptions.RemoveEmptyEntries))
								{
									string text = Path.Combine(baseDirectory, path);
									if (Directory.Exists(text))
									{
										list.Add(this.CreateCatalog(text, "*.dll"));
									}
								}
							}
							AggregateCatalog innerCatalog = new AggregateCatalog(list);
							this._innerCatalog = innerCatalog;
						}
					}
				}
				return this._innerCatalog;
			}
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.ComponentModel.Composition.Hosting.DirectoryCatalog" /> and optionally releases the managed resources. </summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x06000420 RID: 1056 RVA: 0x0000B054 File Offset: 0x00009254
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (!this._isDisposed)
				{
					IDisposable disposable = null;
					object thisLock = this._thisLock;
					lock (thisLock)
					{
						disposable = this._innerCatalog;
						this._innerCatalog = null;
						this._isDisposed = true;
					}
					if (disposable != null)
					{
						disposable.Dispose();
					}
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		/// <summary>Returns an enumerator that iterates through the collection.</summary>
		/// <returns>An enumerator that can be used to iterate through the collection.</returns>
		// Token: 0x06000421 RID: 1057 RVA: 0x0000B0D0 File Offset: 0x000092D0
		public override IEnumerator<ComposablePartDefinition> GetEnumerator()
		{
			this.ThrowIfDisposed();
			return this.InnerCatalog.GetEnumerator();
		}

		/// <summary>Gets the export definitions that match the constraint expressed by the specified import definition.</summary>
		/// <param name="definition">The conditions of the <see cref="T:System.ComponentModel.Composition.Primitives.ExportDefinition" /> objects to be returned.</param>
		/// <returns>A collection of objects that contain the <see cref="T:System.ComponentModel.Composition.Primitives.ExportDefinition" /> objects and their associated <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartDefinition" /> objects that match the specified constraint.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.ComponentModel.Composition.Hosting.DirectoryCatalog" /> object has been disposed of.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="definition" /> is <see langword="null" />.</exception>
		// Token: 0x06000422 RID: 1058 RVA: 0x0000B0E3 File Offset: 0x000092E3
		public override IEnumerable<Tuple<ComposablePartDefinition, ExportDefinition>> GetExports(ImportDefinition definition)
		{
			this.ThrowIfDisposed();
			Requires.NotNull<ImportDefinition>(definition, "definition");
			return this.InnerCatalog.GetExports(definition);
		}

		// Token: 0x06000423 RID: 1059 RVA: 0x0000B102 File Offset: 0x00009302
		[DebuggerStepThrough]
		private void ThrowIfDisposed()
		{
			if (this._isDisposed)
			{
				throw ExceptionBuilder.CreateObjectDisposed(this);
			}
		}

		// Token: 0x06000424 RID: 1060 RVA: 0x0000B113 File Offset: 0x00009313
		private string GetDisplayName()
		{
			return string.Format(CultureInfo.CurrentCulture, "{0} (Path=\"{1}\") (PrivateProbingPath=\"{2}\")", base.GetType().Name, AppDomain.CurrentDomain.BaseDirectory, AppDomain.CurrentDomain.RelativeSearchPath);
		}

		/// <summary>Retrieves a string representation of the application catalog.</summary>
		/// <returns>A string representation of the catalog.</returns>
		// Token: 0x06000425 RID: 1061 RVA: 0x0000B143 File Offset: 0x00009343
		public override string ToString()
		{
			return this.GetDisplayName();
		}

		/// <summary>Gets the display name of the application catalog.</summary>
		/// <returns>A string that contains a human-readable display name of the <see cref="T:System.ComponentModel.Composition.Hosting.DirectoryCatalog" /> object.</returns>
		// Token: 0x17000139 RID: 313
		// (get) Token: 0x06000426 RID: 1062 RVA: 0x0000B143 File Offset: 0x00009343
		string ICompositionElement.DisplayName
		{
			get
			{
				return this.GetDisplayName();
			}
		}

		/// <summary>Gets the composition element from which the application catalog originated.</summary>
		/// <returns>Always <see langword="null" />.</returns>
		// Token: 0x1700013A RID: 314
		// (get) Token: 0x06000427 RID: 1063 RVA: 0x00009EC0 File Offset: 0x000080C0
		ICompositionElement ICompositionElement.Origin
		{
			get
			{
				return null;
			}
		}

		// Token: 0x04000186 RID: 390
		private bool _isDisposed;

		// Token: 0x04000187 RID: 391
		private volatile AggregateCatalog _innerCatalog;

		// Token: 0x04000188 RID: 392
		private readonly object _thisLock = new object();

		// Token: 0x04000189 RID: 393
		private ICompositionElement _definitionOrigin;

		// Token: 0x0400018A RID: 394
		private ReflectionContext _reflectionContext;
	}
}
