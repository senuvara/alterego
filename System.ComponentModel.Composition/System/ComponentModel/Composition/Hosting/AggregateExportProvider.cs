﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition.Primitives;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Microsoft.Internal.Collections;

namespace System.ComponentModel.Composition.Hosting
{
	/// <summary>Retrieves exports provided by a collection of <see cref="T:System.ComponentModel.Composition.Hosting.ExportProvider" /> objects.</summary>
	// Token: 0x0200009D RID: 157
	public class AggregateExportProvider : ExportProvider, IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.AggregateExportProvider" /> class.</summary>
		/// <param name="providers">The prioritized list of export providers.</param>
		// Token: 0x06000411 RID: 1041 RVA: 0x0000AC40 File Offset: 0x00008E40
		public AggregateExportProvider(params ExportProvider[] providers)
		{
			ExportProvider[] array;
			if (providers != null)
			{
				array = new ExportProvider[providers.Length];
				for (int i = 0; i < providers.Length; i++)
				{
					ExportProvider exportProvider = providers[i];
					if (exportProvider == null)
					{
						throw ExceptionBuilder.CreateContainsNullElement("providers");
					}
					array[i] = exportProvider;
					exportProvider.ExportsChanged += this.OnExportChangedInternal;
					exportProvider.ExportsChanging += this.OnExportChangingInternal;
				}
			}
			else
			{
				array = new ExportProvider[0];
			}
			this._providers = array;
			this._readOnlyProviders = new ReadOnlyCollection<ExportProvider>(this._providers);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.AggregateExportProvider" /> class.</summary>
		/// <param name="providers">The prioritized list of export providers. The providers are consulted in the order in which they are supplied.</param>
		/// <exception cref="T:System.ArgumentException">One or more elements of <paramref name="providers" /> are <see langword="null" />.</exception>
		// Token: 0x06000412 RID: 1042 RVA: 0x0000ACCA File Offset: 0x00008ECA
		public AggregateExportProvider(IEnumerable<ExportProvider> providers) : this((providers != null) ? providers.AsArray<ExportProvider>() : null)
		{
		}

		/// <summary>Releases all resources used by the current instance of the <see cref="T:System.ComponentModel.Composition.Hosting.AggregateExportProvider" /> class. </summary>
		// Token: 0x06000413 RID: 1043 RVA: 0x0000ACDE File Offset: 0x00008EDE
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.ComponentModel.Composition.Hosting.AggregateExportProvider" /> class and optionally releases the managed resources. </summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x06000414 RID: 1044 RVA: 0x0000ACF0 File Offset: 0x00008EF0
		protected virtual void Dispose(bool disposing)
		{
			if (disposing && Interlocked.CompareExchange(ref this._isDisposed, 1, 0) == 0)
			{
				foreach (ExportProvider exportProvider in this._providers)
				{
					exportProvider.ExportsChanged -= this.OnExportChangedInternal;
					exportProvider.ExportsChanging -= this.OnExportChangingInternal;
				}
			}
		}

		/// <summary>Gets a collection that contains the providers that the <see cref="T:System.ComponentModel.Composition.Hosting.AggregateExportProvider" /> object aggregates.</summary>
		/// <returns>A collection of the <see cref="T:System.ComponentModel.Composition.Hosting.ExportProvider" /> objects that the <see cref="T:System.ComponentModel.Composition.Hosting.AggregateExportProvider" /> aggregates.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.ComponentModel.Composition.Hosting.AggregateExportProvider" /> object has been disposed of.</exception>
		// Token: 0x17000137 RID: 311
		// (get) Token: 0x06000415 RID: 1045 RVA: 0x0000AD4A File Offset: 0x00008F4A
		public ReadOnlyCollection<ExportProvider> Providers
		{
			get
			{
				this.ThrowIfDisposed();
				return this._readOnlyProviders;
			}
		}

		/// <summary>Gets all the exports that match the conditions of the specified import.</summary>
		/// <param name="definition">The conditions of the <see cref="T:System.ComponentModel.Composition.Primitives.Export" /> objects to be returned.</param>
		/// <param name="atomicComposition">The transactional container for the composition.</param>
		/// <returns>A collection that contains all the exports that match the specified condition.</returns>
		// Token: 0x06000416 RID: 1046 RVA: 0x0000AD58 File Offset: 0x00008F58
		protected override IEnumerable<Export> GetExportsCore(ImportDefinition definition, AtomicComposition atomicComposition)
		{
			this.ThrowIfDisposed();
			ExportProvider[] providers;
			if (definition.Cardinality == ImportCardinality.ZeroOrMore)
			{
				List<Export> list = new List<Export>();
				providers = this._providers;
				for (int i = 0; i < providers.Length; i++)
				{
					foreach (Export item in providers[i].GetExports(definition, atomicComposition))
					{
						list.Add(item);
					}
				}
				return list;
			}
			IEnumerable<Export> enumerable = null;
			providers = this._providers;
			for (int i = 0; i < providers.Length; i++)
			{
				IEnumerable<Export> enumerable2;
				bool flag = providers[i].TryGetExports(definition, atomicComposition, out enumerable2);
				bool flag2 = enumerable2.FastAny<Export>();
				if (flag && flag2)
				{
					return enumerable2;
				}
				if (flag2)
				{
					enumerable = ((enumerable != null) ? enumerable.Concat(enumerable2) : enumerable2);
				}
			}
			return enumerable;
		}

		// Token: 0x06000417 RID: 1047 RVA: 0x0000AE28 File Offset: 0x00009028
		private void OnExportChangedInternal(object sender, ExportsChangeEventArgs e)
		{
			this.OnExportsChanged(e);
		}

		// Token: 0x06000418 RID: 1048 RVA: 0x0000AE31 File Offset: 0x00009031
		private void OnExportChangingInternal(object sender, ExportsChangeEventArgs e)
		{
			this.OnExportsChanging(e);
		}

		// Token: 0x06000419 RID: 1049 RVA: 0x0000AE3A File Offset: 0x0000903A
		[DebuggerStepThrough]
		private void ThrowIfDisposed()
		{
			if (this._isDisposed == 1)
			{
				throw ExceptionBuilder.CreateObjectDisposed(this);
			}
		}

		// Token: 0x04000183 RID: 387
		private readonly ReadOnlyCollection<ExportProvider> _readOnlyProviders;

		// Token: 0x04000184 RID: 388
		private readonly ExportProvider[] _providers;

		// Token: 0x04000185 RID: 389
		private volatile int _isDisposed;
	}
}
