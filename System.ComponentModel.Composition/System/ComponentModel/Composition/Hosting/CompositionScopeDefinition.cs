﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.Hosting
{
	/// <summary>Represents a node in a tree of scoped catalogs, reflecting an underlying catalog and its child scopes.</summary>
	// Token: 0x020000D0 RID: 208
	[DebuggerTypeProxy(typeof(CompositionScopeDefinitionDebuggerProxy))]
	public class CompositionScopeDefinition : ComposablePartCatalog, INotifyComposablePartCatalogChanged
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.CompositionScopeDefinition" /> class.</summary>
		// Token: 0x06000540 RID: 1344 RVA: 0x0000F146 File Offset: 0x0000D346
		protected CompositionScopeDefinition()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.CompositionScopeDefinition" /> class with the specified underlying catalog and children.</summary>
		/// <param name="catalog">The underlying catalog for this catalog.</param>
		/// <param name="children">A collection of the child scopes of this catalog.</param>
		// Token: 0x06000541 RID: 1345 RVA: 0x0000F159 File Offset: 0x0000D359
		public CompositionScopeDefinition(ComposablePartCatalog catalog, IEnumerable<CompositionScopeDefinition> children)
		{
			Requires.NotNull<ComposablePartCatalog>(catalog, "catalog");
			Requires.NullOrNotNullElements<CompositionScopeDefinition>(children, "children");
			this.InitializeCompositionScopeDefinition(catalog, children, null);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.CompositionScopeDefinition" /> class with the specified underlying catalog, children, and public surface.</summary>
		/// <param name="catalog">The underlying catalog for this catalog.</param>
		/// <param name="children">A collection of the child scopes of this catalog.</param>
		/// <param name="publicSurface">The public surface for this catalog.</param>
		// Token: 0x06000542 RID: 1346 RVA: 0x0000F18B File Offset: 0x0000D38B
		public CompositionScopeDefinition(ComposablePartCatalog catalog, IEnumerable<CompositionScopeDefinition> children, IEnumerable<ExportDefinition> publicSurface)
		{
			Requires.NotNull<ComposablePartCatalog>(catalog, "catalog");
			Requires.NullOrNotNullElements<CompositionScopeDefinition>(children, "children");
			Requires.NullOrNotNullElements<ExportDefinition>(publicSurface, "publicSurface");
			this.InitializeCompositionScopeDefinition(catalog, children, publicSurface);
		}

		// Token: 0x06000543 RID: 1347 RVA: 0x0000F1C8 File Offset: 0x0000D3C8
		private void InitializeCompositionScopeDefinition(ComposablePartCatalog catalog, IEnumerable<CompositionScopeDefinition> children, IEnumerable<ExportDefinition> publicSurface)
		{
			this._catalog = catalog;
			if (children != null)
			{
				this._children = children.ToArray<CompositionScopeDefinition>();
			}
			if (publicSurface != null)
			{
				this._publicSurface = publicSurface;
			}
			INotifyComposablePartCatalogChanged notifyComposablePartCatalogChanged = this._catalog as INotifyComposablePartCatalogChanged;
			if (notifyComposablePartCatalogChanged != null)
			{
				notifyComposablePartCatalogChanged.Changed += this.OnChangedInternal;
				notifyComposablePartCatalogChanged.Changing += this.OnChangingInternal;
			}
		}

		/// <summary>Called by the <see langword="Dispose()" /> and <see langword="Finalize()" /> methods to release the managed and unmanaged resources used by the current instance of the <see cref="T:System.ComponentModel.Composition.Hosting.CompositionScopeDefinition" /> class.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources.</param>
		// Token: 0x06000544 RID: 1348 RVA: 0x0000F228 File Offset: 0x0000D428
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && Interlocked.CompareExchange(ref this._isDisposed, 1, 0) == 0)
				{
					INotifyComposablePartCatalogChanged notifyComposablePartCatalogChanged = this._catalog as INotifyComposablePartCatalogChanged;
					if (notifyComposablePartCatalogChanged != null)
					{
						notifyComposablePartCatalogChanged.Changed -= this.OnChangedInternal;
						notifyComposablePartCatalogChanged.Changing -= this.OnChangingInternal;
					}
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		/// <summary>Gets the child scopes of this catalog.</summary>
		/// <returns>A collection of the child scopes of this catalog.</returns>
		// Token: 0x17000164 RID: 356
		// (get) Token: 0x06000545 RID: 1349 RVA: 0x0000F294 File Offset: 0x0000D494
		public virtual IEnumerable<CompositionScopeDefinition> Children
		{
			get
			{
				this.ThrowIfDisposed();
				return this._children;
			}
		}

		/// <summary>Gets a collection of parts visible to the parent scope of this catalog.</summary>
		/// <returns>A collection of parts visible to the parent scope of this catalog.</returns>
		// Token: 0x17000165 RID: 357
		// (get) Token: 0x06000546 RID: 1350 RVA: 0x0000F2A2 File Offset: 0x0000D4A2
		public virtual IEnumerable<ExportDefinition> PublicSurface
		{
			get
			{
				this.ThrowIfDisposed();
				if (this._publicSurface == null)
				{
					return this.SelectMany((ComposablePartDefinition p) => p.ExportDefinitions);
				}
				return this._publicSurface;
			}
		}

		/// <summary>Returns an enumerator that iterates through the catalog.</summary>
		/// <returns>An enumerator that can be used to iterate through the catalog.</returns>
		// Token: 0x06000547 RID: 1351 RVA: 0x0000F2DE File Offset: 0x0000D4DE
		public override IEnumerator<ComposablePartDefinition> GetEnumerator()
		{
			return this._catalog.GetEnumerator();
		}

		/// <summary>Gets a collection of exports that match the conditions specified by the import definition.</summary>
		/// <param name="definition">Conditions that specify which exports to match.</param>
		/// <returns>A collection of exports that match the specified conditions.</returns>
		// Token: 0x06000548 RID: 1352 RVA: 0x0000F2EB File Offset: 0x0000D4EB
		public override IEnumerable<Tuple<ComposablePartDefinition, ExportDefinition>> GetExports(ImportDefinition definition)
		{
			this.ThrowIfDisposed();
			return this._catalog.GetExports(definition);
		}

		// Token: 0x06000549 RID: 1353 RVA: 0x0000F300 File Offset: 0x0000D500
		internal IEnumerable<Tuple<ComposablePartDefinition, ExportDefinition>> GetExportsFromPublicSurface(ImportDefinition definition)
		{
			Assumes.NotNull<ImportDefinition, string>(definition, "definition");
			List<Tuple<ComposablePartDefinition, ExportDefinition>> list = new List<Tuple<ComposablePartDefinition, ExportDefinition>>();
			foreach (ExportDefinition exportDefinition in this.PublicSurface)
			{
				if (definition.IsConstraintSatisfiedBy(exportDefinition))
				{
					foreach (Tuple<ComposablePartDefinition, ExportDefinition> tuple in this.GetExports(definition))
					{
						if (tuple.Item2 == exportDefinition)
						{
							list.Add(tuple);
							break;
						}
					}
				}
			}
			return list;
		}

		/// <summary>Occurs when the underlying catalog has changed, if that catalog supports notifications.</summary>
		// Token: 0x14000005 RID: 5
		// (add) Token: 0x0600054A RID: 1354 RVA: 0x0000F3AC File Offset: 0x0000D5AC
		// (remove) Token: 0x0600054B RID: 1355 RVA: 0x0000F3E4 File Offset: 0x0000D5E4
		public event EventHandler<ComposablePartCatalogChangeEventArgs> Changed
		{
			[CompilerGenerated]
			add
			{
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler = this.Changed;
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<ComposablePartCatalogChangeEventArgs> value2 = (EventHandler<ComposablePartCatalogChangeEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<ComposablePartCatalogChangeEventArgs>>(ref this.Changed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler = this.Changed;
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<ComposablePartCatalogChangeEventArgs> value2 = (EventHandler<ComposablePartCatalogChangeEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<ComposablePartCatalogChangeEventArgs>>(ref this.Changed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		/// <summary>Occurs when the underlying catalog is changing, if that catalog supports notifications.</summary>
		// Token: 0x14000006 RID: 6
		// (add) Token: 0x0600054C RID: 1356 RVA: 0x0000F41C File Offset: 0x0000D61C
		// (remove) Token: 0x0600054D RID: 1357 RVA: 0x0000F454 File Offset: 0x0000D654
		public event EventHandler<ComposablePartCatalogChangeEventArgs> Changing
		{
			[CompilerGenerated]
			add
			{
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler = this.Changing;
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<ComposablePartCatalogChangeEventArgs> value2 = (EventHandler<ComposablePartCatalogChangeEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<ComposablePartCatalogChangeEventArgs>>(ref this.Changing, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler = this.Changing;
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<ComposablePartCatalogChangeEventArgs> value2 = (EventHandler<ComposablePartCatalogChangeEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<ComposablePartCatalogChangeEventArgs>>(ref this.Changing, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		/// <summary>Raises the <see cref="E:System.ComponentModel.Composition.Hosting.CompositionScopeDefinition.Changed" /> event.</summary>
		/// <param name="e">Contains data for the <see cref="E:System.ComponentModel.Composition.Hosting.CompositionScopeDefinition.Changed" /> event.</param>
		// Token: 0x0600054E RID: 1358 RVA: 0x0000F48C File Offset: 0x0000D68C
		protected virtual void OnChanged(ComposablePartCatalogChangeEventArgs e)
		{
			EventHandler<ComposablePartCatalogChangeEventArgs> changed = this.Changed;
			if (changed != null)
			{
				changed(this, e);
			}
		}

		/// <summary>Raises the <see cref="E:System.ComponentModel.Composition.Hosting.CompositionScopeDefinition.Changing" /> event.</summary>
		/// <param name="e">Contains data for the <see cref="E:System.ComponentModel.Composition.Hosting.CompositionScopeDefinition.Changing" /> event.</param>
		// Token: 0x0600054F RID: 1359 RVA: 0x0000F4AC File Offset: 0x0000D6AC
		protected virtual void OnChanging(ComposablePartCatalogChangeEventArgs e)
		{
			EventHandler<ComposablePartCatalogChangeEventArgs> changing = this.Changing;
			if (changing != null)
			{
				changing(this, e);
			}
		}

		// Token: 0x06000550 RID: 1360 RVA: 0x0000F4CB File Offset: 0x0000D6CB
		private void OnChangedInternal(object sender, ComposablePartCatalogChangeEventArgs e)
		{
			this.OnChanged(e);
		}

		// Token: 0x06000551 RID: 1361 RVA: 0x0000F4D4 File Offset: 0x0000D6D4
		private void OnChangingInternal(object sender, ComposablePartCatalogChangeEventArgs e)
		{
			this.OnChanging(e);
		}

		// Token: 0x06000552 RID: 1362 RVA: 0x0000F4DD File Offset: 0x0000D6DD
		[DebuggerStepThrough]
		private void ThrowIfDisposed()
		{
			if (this._isDisposed == 1)
			{
				throw ExceptionBuilder.CreateObjectDisposed(this);
			}
		}

		// Token: 0x04000233 RID: 563
		private ComposablePartCatalog _catalog;

		// Token: 0x04000234 RID: 564
		private IEnumerable<ExportDefinition> _publicSurface;

		// Token: 0x04000235 RID: 565
		private IEnumerable<CompositionScopeDefinition> _children = Enumerable.Empty<CompositionScopeDefinition>();

		// Token: 0x04000236 RID: 566
		private volatile int _isDisposed;

		// Token: 0x04000237 RID: 567
		[CompilerGenerated]
		private EventHandler<ComposablePartCatalogChangeEventArgs> Changed;

		// Token: 0x04000238 RID: 568
		[CompilerGenerated]
		private EventHandler<ComposablePartCatalogChangeEventArgs> Changing;

		// Token: 0x020000D1 RID: 209
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000553 RID: 1363 RVA: 0x0000F4F1 File Offset: 0x0000D6F1
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000554 RID: 1364 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c()
			{
			}

			// Token: 0x06000555 RID: 1365 RVA: 0x0000F4FD File Offset: 0x0000D6FD
			internal IEnumerable<ExportDefinition> <get_PublicSurface>b__12_0(ComposablePartDefinition p)
			{
				return p.ExportDefinitions;
			}

			// Token: 0x04000239 RID: 569
			public static readonly CompositionScopeDefinition.<>c <>9 = new CompositionScopeDefinition.<>c();

			// Token: 0x0400023A RID: 570
			public static Func<ComposablePartDefinition, IEnumerable<ExportDefinition>> <>9__12_0;
		}
	}
}
