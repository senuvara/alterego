﻿using System;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.Hosting
{
	// Token: 0x020000A2 RID: 162
	internal static class AtomicCompositionExtensions
	{
		// Token: 0x06000451 RID: 1105 RVA: 0x0000BA05 File Offset: 0x00009C05
		internal static T GetValueAllowNull<T>(this AtomicComposition atomicComposition, T defaultResultAndKey) where T : class
		{
			Assumes.NotNull<T>(defaultResultAndKey);
			return atomicComposition.GetValueAllowNull(defaultResultAndKey, defaultResultAndKey);
		}

		// Token: 0x06000452 RID: 1106 RVA: 0x0000BA1C File Offset: 0x00009C1C
		internal static T GetValueAllowNull<T>(this AtomicComposition atomicComposition, object key, T defaultResult)
		{
			T result;
			if (atomicComposition != null && atomicComposition.TryGetValue<T>(key, out result))
			{
				return result;
			}
			return defaultResult;
		}

		// Token: 0x06000453 RID: 1107 RVA: 0x0000BA3A File Offset: 0x00009C3A
		internal static void AddRevertActionAllowNull(this AtomicComposition atomicComposition, Action action)
		{
			Assumes.NotNull<Action>(action);
			if (atomicComposition == null)
			{
				action();
				return;
			}
			atomicComposition.AddRevertAction(action);
		}

		// Token: 0x06000454 RID: 1108 RVA: 0x0000BA53 File Offset: 0x00009C53
		internal static void AddCompleteActionAllowNull(this AtomicComposition atomicComposition, Action action)
		{
			Assumes.NotNull<Action>(action);
			if (atomicComposition == null)
			{
				action();
				return;
			}
			atomicComposition.AddCompleteAction(action);
		}
	}
}
