﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.Linq;
using System.Runtime.CompilerServices;
using Microsoft.Internal;
using Microsoft.Internal.Collections;

namespace System.ComponentModel.Composition.Hosting
{
	/// <summary>Provides data for the <see cref="E:System.ComponentModel.Composition.Hosting.ExportProvider.ExportsChanging" /> and <see cref="E:System.ComponentModel.Composition.Hosting.ExportProvider.ExportsChanged" /> event. </summary>
	// Token: 0x020000DD RID: 221
	public class ExportsChangeEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.ExportsChangeEventArgs" /> class. </summary>
		/// <param name="addedExports">The events that were added.</param>
		/// <param name="removedExports">The events that were removed.</param>
		/// <param name="atomicComposition">The composition transaction that contains the change.</param>
		// Token: 0x060005D9 RID: 1497 RVA: 0x00011110 File Offset: 0x0000F310
		public ExportsChangeEventArgs(IEnumerable<ExportDefinition> addedExports, IEnumerable<ExportDefinition> removedExports, AtomicComposition atomicComposition)
		{
			Requires.NotNull<IEnumerable<ExportDefinition>>(addedExports, "addedExports");
			Requires.NotNull<IEnumerable<ExportDefinition>>(removedExports, "removedExports");
			this._addedExports = addedExports.AsArray<ExportDefinition>();
			this._removedExports = removedExports.AsArray<ExportDefinition>();
			this.AtomicComposition = atomicComposition;
		}

		/// <summary>Gets the exports that were added in this change.</summary>
		/// <returns>A collection of the exports that were added.</returns>
		// Token: 0x17000176 RID: 374
		// (get) Token: 0x060005DA RID: 1498 RVA: 0x0001114D File Offset: 0x0000F34D
		public IEnumerable<ExportDefinition> AddedExports
		{
			get
			{
				return this._addedExports;
			}
		}

		/// <summary>Gets the exports that were removed in the change.</summary>
		/// <returns>A collection of the removed exports.</returns>
		// Token: 0x17000177 RID: 375
		// (get) Token: 0x060005DB RID: 1499 RVA: 0x00011155 File Offset: 0x0000F355
		public IEnumerable<ExportDefinition> RemovedExports
		{
			get
			{
				return this._removedExports;
			}
		}

		/// <summary>Gets the contract names that were altered in the change.</summary>
		/// <returns>A collection of the altered contract names.</returns>
		// Token: 0x17000178 RID: 376
		// (get) Token: 0x060005DC RID: 1500 RVA: 0x00011160 File Offset: 0x0000F360
		public IEnumerable<string> ChangedContractNames
		{
			get
			{
				if (this._changedContractNames == null)
				{
					this._changedContractNames = (from export in this.AddedExports.Concat(this.RemovedExports)
					select export.ContractName).Distinct<string>().ToArray<string>();
				}
				return this._changedContractNames;
			}
		}

		/// <summary>Gets the composition transaction of the change, if any.</summary>
		/// <returns>A reference to the composition transaction associated with the change, or <see langword="null" /> if no transaction is being used.</returns>
		// Token: 0x17000179 RID: 377
		// (get) Token: 0x060005DD RID: 1501 RVA: 0x000111C0 File Offset: 0x0000F3C0
		// (set) Token: 0x060005DE RID: 1502 RVA: 0x000111C8 File Offset: 0x0000F3C8
		public AtomicComposition AtomicComposition
		{
			[CompilerGenerated]
			get
			{
				return this.<AtomicComposition>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<AtomicComposition>k__BackingField = value;
			}
		}

		// Token: 0x04000263 RID: 611
		private readonly IEnumerable<ExportDefinition> _addedExports;

		// Token: 0x04000264 RID: 612
		private readonly IEnumerable<ExportDefinition> _removedExports;

		// Token: 0x04000265 RID: 613
		private IEnumerable<string> _changedContractNames;

		// Token: 0x04000266 RID: 614
		[CompilerGenerated]
		private AtomicComposition <AtomicComposition>k__BackingField;

		// Token: 0x020000DE RID: 222
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x060005DF RID: 1503 RVA: 0x000111D1 File Offset: 0x0000F3D1
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x060005E0 RID: 1504 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c()
			{
			}

			// Token: 0x060005E1 RID: 1505 RVA: 0x000111DD File Offset: 0x0000F3DD
			internal string <get_ChangedContractNames>b__9_0(ExportDefinition export)
			{
				return export.ContractName;
			}

			// Token: 0x04000267 RID: 615
			public static readonly ExportsChangeEventArgs.<>c <>9 = new ExportsChangeEventArgs.<>c();

			// Token: 0x04000268 RID: 616
			public static Func<ExportDefinition, string> <>9__9_0;
		}
	}
}
