﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition.Primitives;
using System.ComponentModel.Composition.ReflectionModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.Hosting
{
	// Token: 0x020000D4 RID: 212
	internal static class CompositionServices
	{
		// Token: 0x0600055F RID: 1375 RVA: 0x0000F658 File Offset: 0x0000D858
		internal static Type GetDefaultTypeFromMember(this MemberInfo member)
		{
			Assumes.NotNull<MemberInfo>(member);
			MemberTypes memberType = member.MemberType;
			if (memberType <= MemberTypes.Property)
			{
				if (memberType != MemberTypes.Field)
				{
					if (memberType == MemberTypes.Property)
					{
						return ((PropertyInfo)member).PropertyType;
					}
				}
			}
			else if (memberType == MemberTypes.TypeInfo || memberType == MemberTypes.NestedType)
			{
				return (Type)member;
			}
			Assumes.IsTrue(member.MemberType == MemberTypes.Field);
			return ((FieldInfo)member).FieldType;
		}

		// Token: 0x06000560 RID: 1376 RVA: 0x0000F6BD File Offset: 0x0000D8BD
		internal static Type AdjustSpecifiedTypeIdentityType(this Type specifiedContractType, MemberInfo member)
		{
			if (member.MemberType == MemberTypes.Method)
			{
				return specifiedContractType;
			}
			return specifiedContractType.AdjustSpecifiedTypeIdentityType(member.GetDefaultTypeFromMember());
		}

		// Token: 0x06000561 RID: 1377 RVA: 0x0000F6D8 File Offset: 0x0000D8D8
		internal static Type AdjustSpecifiedTypeIdentityType(this Type specifiedContractType, Type memberType)
		{
			Assumes.NotNull<Type>(specifiedContractType);
			if (memberType != null && memberType.IsGenericType && specifiedContractType.IsGenericType)
			{
				if (specifiedContractType.ContainsGenericParameters && !memberType.ContainsGenericParameters)
				{
					Type[] genericArguments = memberType.GetGenericArguments();
					Type[] genericArguments2 = specifiedContractType.GetGenericArguments();
					if (genericArguments.Length == genericArguments2.Length)
					{
						return specifiedContractType.MakeGenericType(genericArguments);
					}
				}
				else if (specifiedContractType.ContainsGenericParameters && memberType.ContainsGenericParameters)
				{
					IList<Type> pureGenericParameters = memberType.GetPureGenericParameters();
					if (specifiedContractType.GetPureGenericArity() == pureGenericParameters.Count)
					{
						return specifiedContractType.GetGenericTypeDefinition().MakeGenericType(pureGenericParameters.ToArray<Type>());
					}
				}
			}
			return specifiedContractType;
		}

		// Token: 0x06000562 RID: 1378 RVA: 0x0000F76A File Offset: 0x0000D96A
		private static string AdjustTypeIdentity(string originalTypeIdentity, Type typeIdentityType)
		{
			return GenericServices.GetGenericName(originalTypeIdentity, GenericServices.GetGenericParametersOrder(typeIdentityType), typeIdentityType.GetPureGenericArity());
		}

		// Token: 0x06000563 RID: 1379 RVA: 0x0000F77E File Offset: 0x0000D97E
		internal static void GetContractInfoFromExport(this MemberInfo member, ExportAttribute export, out Type typeIdentityType, out string contractName)
		{
			typeIdentityType = member.GetTypeIdentityTypeFromExport(export);
			if (!string.IsNullOrEmpty(export.ContractName))
			{
				contractName = export.ContractName;
				return;
			}
			contractName = member.GetTypeIdentityFromExport(typeIdentityType);
		}

		// Token: 0x06000564 RID: 1380 RVA: 0x0000F7AC File Offset: 0x0000D9AC
		internal static string GetTypeIdentityFromExport(this MemberInfo member, Type typeIdentityType)
		{
			if (typeIdentityType != null)
			{
				string text = AttributedModelServices.GetTypeIdentity(typeIdentityType);
				if (typeIdentityType.ContainsGenericParameters)
				{
					text = CompositionServices.AdjustTypeIdentity(text, typeIdentityType);
				}
				return text;
			}
			MethodInfo methodInfo = member as MethodInfo;
			Assumes.NotNull<MethodInfo>(methodInfo);
			return AttributedModelServices.GetTypeIdentity(methodInfo);
		}

		// Token: 0x06000565 RID: 1381 RVA: 0x0000F7EC File Offset: 0x0000D9EC
		private static Type GetTypeIdentityTypeFromExport(this MemberInfo member, ExportAttribute export)
		{
			if (export.ContractType != null)
			{
				return export.ContractType.AdjustSpecifiedTypeIdentityType(member);
			}
			if (member.MemberType == MemberTypes.Method)
			{
				return null;
			}
			return member.GetDefaultTypeFromMember();
		}

		// Token: 0x06000566 RID: 1382 RVA: 0x0000F81A File Offset: 0x0000DA1A
		internal static bool IsContractNameSameAsTypeIdentity(this ExportAttribute export)
		{
			return string.IsNullOrEmpty(export.ContractName);
		}

		// Token: 0x06000567 RID: 1383 RVA: 0x0000F827 File Offset: 0x0000DA27
		internal static Type GetContractTypeFromImport(this IAttributedImport import, ImportType importType)
		{
			if (import.ContractType != null)
			{
				return import.ContractType.AdjustSpecifiedTypeIdentityType(importType.ContractType);
			}
			return importType.ContractType;
		}

		// Token: 0x06000568 RID: 1384 RVA: 0x0000F84F File Offset: 0x0000DA4F
		internal static string GetContractNameFromImport(this IAttributedImport import, ImportType importType)
		{
			if (!string.IsNullOrEmpty(import.ContractName))
			{
				return import.ContractName;
			}
			return AttributedModelServices.GetContractName(import.GetContractTypeFromImport(importType));
		}

		// Token: 0x06000569 RID: 1385 RVA: 0x0000F874 File Offset: 0x0000DA74
		internal static string GetTypeIdentityFromImport(this IAttributedImport import, ImportType importType)
		{
			Type contractTypeFromImport = import.GetContractTypeFromImport(importType);
			if (contractTypeFromImport == CompositionServices.ObjectType)
			{
				return null;
			}
			return AttributedModelServices.GetTypeIdentity(contractTypeFromImport);
		}

		// Token: 0x0600056A RID: 1386 RVA: 0x0000F8A0 File Offset: 0x0000DAA0
		internal static IDictionary<string, object> GetPartMetadataForType(this Type type, CreationPolicy creationPolicy)
		{
			IDictionary<string, object> dictionary = new Dictionary<string, object>(StringComparers.MetadataKeyNames);
			if (creationPolicy != CreationPolicy.Any)
			{
				dictionary.Add("System.ComponentModel.Composition.CreationPolicy", creationPolicy);
			}
			foreach (PartMetadataAttribute partMetadataAttribute in type.GetAttributes<PartMetadataAttribute>())
			{
				if (!CompositionServices.reservedMetadataNames.Contains(partMetadataAttribute.Name, StringComparers.MetadataKeyNames) && !dictionary.ContainsKey(partMetadataAttribute.Name))
				{
					dictionary.Add(partMetadataAttribute.Name, partMetadataAttribute.Value);
				}
			}
			if (type.ContainsGenericParameters)
			{
				dictionary.Add("System.ComponentModel.Composition.IsGenericPart", true);
				Type[] genericArguments = type.GetGenericArguments();
				dictionary.Add("System.ComponentModel.Composition.GenericPartArity", genericArguments.Length);
				bool flag = false;
				object[] array = new object[genericArguments.Length];
				GenericParameterAttributes[] array2 = new GenericParameterAttributes[genericArguments.Length];
				for (int j = 0; j < genericArguments.Length; j++)
				{
					Type type2 = genericArguments[j];
					Type[] array3 = type2.GetGenericParameterConstraints();
					if (array3.Length == 0)
					{
						array3 = null;
					}
					GenericParameterAttributes genericParameterAttributes = type2.GenericParameterAttributes;
					if (array3 != null || genericParameterAttributes != GenericParameterAttributes.None)
					{
						array[j] = array3;
						array2[j] = genericParameterAttributes;
						flag = true;
					}
				}
				if (flag)
				{
					dictionary.Add("System.ComponentModel.Composition.GenericParameterConstraints", array);
					dictionary.Add("System.ComponentModel.Composition.GenericParameterAttributes", array2);
				}
			}
			if (dictionary.Count == 0)
			{
				return MetadataServices.EmptyMetadata;
			}
			return dictionary;
		}

		// Token: 0x0600056B RID: 1387 RVA: 0x0000F9E4 File Offset: 0x0000DBE4
		internal static void TryExportMetadataForMember(this MemberInfo member, out IDictionary<string, object> dictionary)
		{
			dictionary = new Dictionary<string, object>();
			foreach (Attribute attribute in member.GetAttributes<Attribute>())
			{
				ExportMetadataAttribute exportMetadataAttribute = attribute as ExportMetadataAttribute;
				if (exportMetadataAttribute != null)
				{
					if (CompositionServices.reservedMetadataNames.Contains(exportMetadataAttribute.Name, StringComparers.MetadataKeyNames))
					{
						throw ExceptionBuilder.CreateDiscoveryException(Strings.Discovery_ReservedMetadataNameUsed, new string[]
						{
							member.GetDisplayName(),
							exportMetadataAttribute.Name
						});
					}
					if (!dictionary.TryContributeMetadataValue(exportMetadataAttribute.Name, exportMetadataAttribute.Value, null, exportMetadataAttribute.IsMultiple))
					{
						throw ExceptionBuilder.CreateDiscoveryException(Strings.Discovery_DuplicateMetadataNameValues, new string[]
						{
							member.GetDisplayName(),
							exportMetadataAttribute.Name
						});
					}
				}
				else
				{
					Type type = attribute.GetType();
					if (type != CompositionServices.ExportAttributeType && type.IsAttributeDefined(true))
					{
						bool allowsMultiple = false;
						AttributeUsageAttribute firstAttribute = type.GetFirstAttribute(true);
						if (firstAttribute != null)
						{
							allowsMultiple = firstAttribute.AllowMultiple;
						}
						foreach (PropertyInfo propertyInfo in type.GetProperties())
						{
							if (!(propertyInfo.DeclaringType == CompositionServices.ExportAttributeType) && !(propertyInfo.DeclaringType == CompositionServices.AttributeType))
							{
								if (CompositionServices.reservedMetadataNames.Contains(propertyInfo.Name, StringComparers.MetadataKeyNames))
								{
									throw ExceptionBuilder.CreateDiscoveryException(Strings.Discovery_ReservedMetadataNameUsed, new string[]
									{
										member.GetDisplayName(),
										exportMetadataAttribute.Name
									});
								}
								object value = propertyInfo.GetValue(attribute, null);
								if (value != null && !CompositionServices.IsValidAttributeType(value.GetType()))
								{
									throw ExceptionBuilder.CreateDiscoveryException(Strings.Discovery_MetadataContainsValueWithInvalidType, new string[]
									{
										propertyInfo.GetDisplayName(),
										value.GetType().GetDisplayName()
									});
								}
								if (!dictionary.TryContributeMetadataValue(propertyInfo.Name, value, propertyInfo.PropertyType, allowsMultiple))
								{
									throw ExceptionBuilder.CreateDiscoveryException(Strings.Discovery_DuplicateMetadataNameValues, new string[]
									{
										member.GetDisplayName(),
										propertyInfo.Name
									});
								}
							}
						}
					}
				}
			}
			foreach (string key in dictionary.Keys.ToArray<string>())
			{
				CompositionServices.MetadataList metadataList = dictionary[key] as CompositionServices.MetadataList;
				if (metadataList != null)
				{
					dictionary[key] = metadataList.ToArray();
				}
			}
		}

		// Token: 0x0600056C RID: 1388 RVA: 0x0000FC3C File Offset: 0x0000DE3C
		private static bool TryContributeMetadataValue(this IDictionary<string, object> dictionary, string name, object value, Type valueType, bool allowsMultiple)
		{
			object obj;
			if (!dictionary.TryGetValue(name, out obj))
			{
				if (allowsMultiple)
				{
					CompositionServices.MetadataList metadataList = new CompositionServices.MetadataList();
					metadataList.Add(value, valueType);
					value = metadataList;
				}
				dictionary.Add(name, value);
			}
			else
			{
				CompositionServices.MetadataList metadataList2 = obj as CompositionServices.MetadataList;
				if (!allowsMultiple || metadataList2 == null)
				{
					dictionary.Remove(name);
					return false;
				}
				metadataList2.Add(value, valueType);
			}
			return true;
		}

		// Token: 0x0600056D RID: 1389 RVA: 0x0000FC94 File Offset: 0x0000DE94
		internal static IEnumerable<KeyValuePair<string, Type>> GetRequiredMetadata(Type metadataViewType)
		{
			if (metadataViewType == null || ExportServices.IsDefaultMetadataViewType(metadataViewType) || ExportServices.IsDictionaryConstructorViewType(metadataViewType) || !metadataViewType.IsInterface)
			{
				return Enumerable.Empty<KeyValuePair<string, Type>>();
			}
			return from property in (from property in metadataViewType.GetAllProperties()
			where property.GetFirstAttribute<DefaultValueAttribute>() == null
			select property).ToList<PropertyInfo>()
			select new KeyValuePair<string, Type>(property.Name, property.PropertyType);
		}

		// Token: 0x0600056E RID: 1390 RVA: 0x0000FD1B File Offset: 0x0000DF1B
		internal static IDictionary<string, object> GetImportMetadata(ImportType importType, IAttributedImport attributedImport)
		{
			return CompositionServices.GetImportMetadata(importType.ContractType, attributedImport);
		}

		// Token: 0x0600056F RID: 1391 RVA: 0x0000FD2C File Offset: 0x0000DF2C
		internal static IDictionary<string, object> GetImportMetadata(Type type, IAttributedImport attributedImport)
		{
			Dictionary<string, object> dictionary = null;
			if (type.IsGenericType)
			{
				dictionary = new Dictionary<string, object>();
				if (type.ContainsGenericParameters)
				{
					dictionary["System.ComponentModel.Composition.GenericImportParametersOrderMetadataName"] = GenericServices.GetGenericParametersOrder(type);
				}
				else
				{
					dictionary["System.ComponentModel.Composition.GenericContractName"] = ContractNameServices.GetTypeIdentity(type.GetGenericTypeDefinition());
					dictionary["System.ComponentModel.Composition.GenericParameters"] = type.GetGenericArguments();
				}
			}
			if (attributedImport != null && attributedImport.Source != ImportSource.Any)
			{
				if (dictionary == null)
				{
					dictionary = new Dictionary<string, object>();
				}
				dictionary["System.ComponentModel.Composition.ImportSource"] = attributedImport.Source;
			}
			if (dictionary != null)
			{
				return dictionary.AsReadOnly();
			}
			return MetadataServices.EmptyMetadata;
		}

		// Token: 0x06000570 RID: 1392 RVA: 0x0000FDC4 File Offset: 0x0000DFC4
		internal static object GetExportedValueFromComposedPart(ImportEngine engine, ComposablePart part, ExportDefinition definition)
		{
			if (engine != null)
			{
				try
				{
					engine.SatisfyImports(part);
				}
				catch (CompositionException innerException)
				{
					throw ExceptionBuilder.CreateCannotGetExportedValue(part, definition, innerException);
				}
			}
			object exportedValue;
			try
			{
				exportedValue = part.GetExportedValue(definition);
			}
			catch (ComposablePartException innerException2)
			{
				throw ExceptionBuilder.CreateCannotGetExportedValue(part, definition, innerException2);
			}
			return exportedValue;
		}

		// Token: 0x06000571 RID: 1393 RVA: 0x0000FE1C File Offset: 0x0000E01C
		internal static bool IsRecomposable(this ComposablePart part)
		{
			return part.ImportDefinitions.Any((ImportDefinition import) => import.IsRecomposable);
		}

		// Token: 0x06000572 RID: 1394 RVA: 0x0000FE48 File Offset: 0x0000E048
		internal static CompositionResult TryInvoke(Action action)
		{
			CompositionResult result;
			try
			{
				action();
				result = CompositionResult.SucceededResult;
			}
			catch (CompositionException ex)
			{
				result = new CompositionResult(ex.Errors);
			}
			return result;
		}

		// Token: 0x06000573 RID: 1395 RVA: 0x0000FE84 File Offset: 0x0000E084
		internal static CompositionResult TryFire<TEventArgs>(EventHandler<TEventArgs> _delegate, object sender, TEventArgs e) where TEventArgs : EventArgs
		{
			CompositionResult result = CompositionResult.SucceededResult;
			foreach (EventHandler<TEventArgs> eventHandler in _delegate.GetInvocationList())
			{
				try
				{
					eventHandler(sender, e);
				}
				catch (CompositionException ex)
				{
					result = result.MergeErrors(ex.Errors);
				}
			}
			return result;
		}

		// Token: 0x06000574 RID: 1396 RVA: 0x0000FEE4 File Offset: 0x0000E0E4
		internal static CreationPolicy GetRequiredCreationPolicy(this ImportDefinition definition)
		{
			ContractBasedImportDefinition contractBasedImportDefinition = definition as ContractBasedImportDefinition;
			if (contractBasedImportDefinition != null)
			{
				return contractBasedImportDefinition.RequiredCreationPolicy;
			}
			return CreationPolicy.Any;
		}

		// Token: 0x06000575 RID: 1397 RVA: 0x0000FF03 File Offset: 0x0000E103
		internal static bool IsAtMostOne(this ImportCardinality cardinality)
		{
			return cardinality == ImportCardinality.ZeroOrOne || cardinality == ImportCardinality.ExactlyOne;
		}

		// Token: 0x06000576 RID: 1398 RVA: 0x0000FF0E File Offset: 0x0000E10E
		private static bool IsValidAttributeType(Type type)
		{
			return CompositionServices.IsValidAttributeType(type, true);
		}

		// Token: 0x06000577 RID: 1399 RVA: 0x0000FF18 File Offset: 0x0000E118
		private static bool IsValidAttributeType(Type type, bool arrayAllowed)
		{
			Assumes.NotNull<Type>(type);
			return type.IsPrimitive || type == typeof(string) || (type.IsEnum && type.IsVisible) || typeof(Type).IsAssignableFrom(type) || (arrayAllowed && type.IsArray && type.GetArrayRank() == 1 && CompositionServices.IsValidAttributeType(type.GetElementType(), false));
		}

		// Token: 0x06000578 RID: 1400 RVA: 0x0000FF94 File Offset: 0x0000E194
		// Note: this type is marked as 'beforefieldinit'.
		static CompositionServices()
		{
		}

		// Token: 0x0400023E RID: 574
		internal static readonly Type InheritedExportAttributeType = typeof(InheritedExportAttribute);

		// Token: 0x0400023F RID: 575
		internal static readonly Type ExportAttributeType = typeof(ExportAttribute);

		// Token: 0x04000240 RID: 576
		internal static readonly Type AttributeType = typeof(Attribute);

		// Token: 0x04000241 RID: 577
		internal static readonly Type ObjectType = typeof(object);

		// Token: 0x04000242 RID: 578
		private static readonly string[] reservedMetadataNames = new string[]
		{
			"System.ComponentModel.Composition.CreationPolicy"
		};

		// Token: 0x020000D5 RID: 213
		private class MetadataList
		{
			// Token: 0x06000579 RID: 1401 RVA: 0x0000FFF0 File Offset: 0x0000E1F0
			public void Add(object item, Type itemType)
			{
				this._containsNulls |= (item == null);
				if (itemType == CompositionServices.MetadataList.ObjectType)
				{
					itemType = null;
				}
				if (itemType == null && item != null)
				{
					itemType = item.GetType();
				}
				if (item is Type)
				{
					itemType = CompositionServices.MetadataList.TypeType;
				}
				if (itemType != null)
				{
					this.InferArrayType(itemType);
				}
				this._innerList.Add(item);
			}

			// Token: 0x0600057A RID: 1402 RVA: 0x0001005D File Offset: 0x0000E25D
			private void InferArrayType(Type itemType)
			{
				Assumes.NotNull<Type>(itemType);
				if (this._arrayType == null)
				{
					this._arrayType = itemType;
					return;
				}
				if (this._arrayType != itemType)
				{
					this._arrayType = CompositionServices.MetadataList.ObjectType;
				}
			}

			// Token: 0x0600057B RID: 1403 RVA: 0x00010094 File Offset: 0x0000E294
			public Array ToArray()
			{
				if (this._arrayType == null)
				{
					this._arrayType = CompositionServices.MetadataList.ObjectType;
				}
				else if (this._containsNulls && this._arrayType.IsValueType)
				{
					this._arrayType = CompositionServices.MetadataList.ObjectType;
				}
				Array array = Array.CreateInstance(this._arrayType, this._innerList.Count);
				for (int i = 0; i < array.Length; i++)
				{
					array.SetValue(this._innerList[i], i);
				}
				return array;
			}

			// Token: 0x0600057C RID: 1404 RVA: 0x00010118 File Offset: 0x0000E318
			public MetadataList()
			{
			}

			// Token: 0x0600057D RID: 1405 RVA: 0x0001012B File Offset: 0x0000E32B
			// Note: this type is marked as 'beforefieldinit'.
			static MetadataList()
			{
			}

			// Token: 0x04000243 RID: 579
			private Type _arrayType;

			// Token: 0x04000244 RID: 580
			private bool _containsNulls;

			// Token: 0x04000245 RID: 581
			private static readonly Type ObjectType = typeof(object);

			// Token: 0x04000246 RID: 582
			private static readonly Type TypeType = typeof(Type);

			// Token: 0x04000247 RID: 583
			private Collection<object> _innerList = new Collection<object>();
		}

		// Token: 0x020000D6 RID: 214
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x0600057E RID: 1406 RVA: 0x0001014B File Offset: 0x0000E34B
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x0600057F RID: 1407 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c()
			{
			}

			// Token: 0x06000580 RID: 1408 RVA: 0x00010157 File Offset: 0x0000E357
			internal bool <GetRequiredMetadata>b__20_0(PropertyInfo property)
			{
				return property.GetFirstAttribute<DefaultValueAttribute>() == null;
			}

			// Token: 0x06000581 RID: 1409 RVA: 0x00010162 File Offset: 0x0000E362
			internal KeyValuePair<string, Type> <GetRequiredMetadata>b__20_1(PropertyInfo property)
			{
				return new KeyValuePair<string, Type>(property.Name, property.PropertyType);
			}

			// Token: 0x06000582 RID: 1410 RVA: 0x00010175 File Offset: 0x0000E375
			internal bool <IsRecomposable>b__24_0(ImportDefinition import)
			{
				return import.IsRecomposable;
			}

			// Token: 0x04000248 RID: 584
			public static readonly CompositionServices.<>c <>9 = new CompositionServices.<>c();

			// Token: 0x04000249 RID: 585
			public static Func<PropertyInfo, bool> <>9__20_0;

			// Token: 0x0400024A RID: 586
			public static Func<PropertyInfo, KeyValuePair<string, Type>> <>9__20_1;

			// Token: 0x0400024B RID: 587
			public static Func<ImportDefinition, bool> <>9__24_0;
		}
	}
}
