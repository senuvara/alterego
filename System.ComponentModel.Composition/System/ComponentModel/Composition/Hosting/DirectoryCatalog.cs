﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition.Diagnostics;
using System.ComponentModel.Composition.Primitives;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using Microsoft.Internal;
using Microsoft.Internal.Collections;

namespace System.ComponentModel.Composition.Hosting
{
	/// <summary>Discovers attributed parts in the assemblies in a specified directory.</summary>
	// Token: 0x020000D7 RID: 215
	[DebuggerTypeProxy(typeof(DirectoryCatalog.DirectoryCatalogDebuggerProxy))]
	public class DirectoryCatalog : ComposablePartCatalog, INotifyComposablePartCatalogChanged, ICompositionElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.DirectoryCatalog" /> class by using <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartDefinition" /> objects based on all the DLL files in the specified directory path.</summary>
		/// <param name="path">The path to the directory to scan for assemblies to add to the catalog.The path must be absolute or relative to <see cref="P:System.AppDomain.BaseDirectory" />.</param>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified <paramref name="path" /> is invalid (for example, it is on an unmapped drive). </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The caller does not have the required permission. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="path" /> is a zero-length string, contains only white space, or contains one or more implementation-specific invalid characters.</exception>
		/// <exception cref="T:System.IO.PathTooLongException">The specified <paramref name="path" />, file name, or both exceed the system-defined maximum length. For example, on Windows-based computers, paths must be less than 248 characters and file names must be less than 260 characters. </exception>
		// Token: 0x06000583 RID: 1411 RVA: 0x0001017D File Offset: 0x0000E37D
		public DirectoryCatalog(string path) : this(path, "*.dll")
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.DirectoryCatalog" /> class by using <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartDefinition" /> objects based on all the DLL files in the specified directory path, in the specified reflection context.</summary>
		/// <param name="path">The path to the directory to scan for assemblies to add to the catalog.The path must be absolute or relative to <see cref="P:System.AppDomain.BaseDirectory" />.</param>
		/// <param name="reflectionContext">The context used to create parts.</param>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified <paramref name="path" /> is invalid (for example, it is on an unmapped drive). </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The caller does not have the required permission. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="path" /> is a zero-length string, contains only white space, or contains one or more implementation-specific invalid characters.</exception>
		/// <exception cref="T:System.IO.PathTooLongException">The specified <paramref name="path" />, file name, or both exceed the system-defined maximum length. For example, on Windows-based computers, paths must be less than 248 characters and file names must be less than 260 characters. </exception>
		// Token: 0x06000584 RID: 1412 RVA: 0x0001018B File Offset: 0x0000E38B
		public DirectoryCatalog(string path, ReflectionContext reflectionContext) : this(path, "*.dll", reflectionContext)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.DirectoryCatalog" /> class by using <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartDefinition" /> objects based on all the DLL files in the specified directory path with the specified source for parts.</summary>
		/// <param name="path">The path to the directory to scan for assemblies to add to the catalog.The path must be absolute or relative to <see cref="P:System.AppDomain.BaseDirectory" />.</param>
		/// <param name="definitionOrigin">The element used by diagnostics to identify the source for parts.</param>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified <paramref name="path" /> is invalid (for example, it is on an unmapped drive). </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The caller does not have the required permission. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="path" /> is a zero-length string, contains only white space, or contains one or more implementation-specific invalid characters.</exception>
		/// <exception cref="T:System.IO.PathTooLongException">The specified <paramref name="path" />, file name, or both exceed the system-defined maximum length. For example, on Windows-based computers, paths must be less than 248 characters and file names must be less than 260 characters. </exception>
		// Token: 0x06000585 RID: 1413 RVA: 0x0001019A File Offset: 0x0000E39A
		public DirectoryCatalog(string path, ICompositionElement definitionOrigin) : this(path, "*.dll", definitionOrigin)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.DirectoryCatalog" /> class by  using <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartDefinition" /> objects based on all the DLL files in the specified directory path, in the specified reflection context.</summary>
		/// <param name="path">The path to the directory to scan for assemblies to add to the catalog.The path must be absolute or relative to <see cref="P:System.AppDomain.BaseDirectory" />.</param>
		/// <param name="reflectionContext">The context used to create parts.</param>
		/// <param name="definitionOrigin">The element used by diagnostics to identify the source for parts.</param>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified <paramref name="path" /> is invalid (for example, it is on an unmapped drive). </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The caller does not have the required permission. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="path" /> is a zero-length string, contains only white space, or contains one or more implementation-specific invalid characters.</exception>
		/// <exception cref="T:System.IO.PathTooLongException">The specified <paramref name="path" />, file name, or both exceed the system-defined maximum length. For example, on Windows-based computers, paths must be less than 248 characters and file names must be less than 260 characters. </exception>
		// Token: 0x06000586 RID: 1414 RVA: 0x000101A9 File Offset: 0x0000E3A9
		public DirectoryCatalog(string path, ReflectionContext reflectionContext, ICompositionElement definitionOrigin) : this(path, "*.dll", reflectionContext, definitionOrigin)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.DirectoryCatalog" /> class by using <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartDefinition" /> objects that match a specified search pattern in the specified directory path.</summary>
		/// <param name="path">The path to the directory to scan for assemblies to add to the catalog.The path must be absolute or relative to <see cref="P:System.AppDomain.BaseDirectory" />.</param>
		/// <param name="searchPattern">The search string. The format of the string should be the same as specified for the <see cref="M:System.IO.Directory.GetFiles(System.String,System.String)" /> method.</param>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified <paramref name="path" /> is invalid (for example, it is on an unmapped drive).</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> or <paramref name="searchPattern" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The caller does not have the required permission.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="path" /> is a zero-length string, contains only white space, or contains one or more implementation-specific invalid characters.-or-
		///         <paramref name="searchPattern" /> does not contain a valid pattern.</exception>
		/// <exception cref="T:System.IO.PathTooLongException">The specified <paramref name="path" />, file name, or both exceed the system-defined maximum length. For example, on Windows-based computers, paths must be less than 248 characters and file names must be less than 260 characters.</exception>
		// Token: 0x06000587 RID: 1415 RVA: 0x000101B9 File Offset: 0x0000E3B9
		public DirectoryCatalog(string path, string searchPattern)
		{
			this._thisLock = new Lock();
			base..ctor();
			Requires.NotNullOrEmpty(path, "path");
			Requires.NotNullOrEmpty(searchPattern, "searchPattern");
			this._definitionOrigin = this;
			this.Initialize(path, searchPattern);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.DirectoryCatalog" /> class by using <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartDefinition" /> objects based on the specified search pattern in the specified directory path with the specified source for parts.</summary>
		/// <param name="path">The path to the directory to scan for assemblies to add to the catalog.The path must be absolute or relative to <see cref="P:System.AppDomain.BaseDirectory" />.</param>
		/// <param name="searchPattern">The search string. The format of the string should be the same as specified for the <see cref="M:System.IO.Directory.GetFiles(System.String,System.String)" /> method.</param>
		/// <param name="definitionOrigin">The element used by diagnostics to identify the source for parts.</param>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified <paramref name="path" /> is invalid (for example, it is on an unmapped drive).</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> or <paramref name="searchPattern" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The caller does not have the required permission.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="path" /> is a zero-length string, contains only white space, or contains one or more implementation-specific invalid characters. -or-
		///         <paramref name="searchPattern" /> does not contain a valid pattern.</exception>
		/// <exception cref="T:System.IO.PathTooLongException">The specified <paramref name="path" />, file name, or both exceed the system-defined maximum length. For example, on Windows-based computers, paths must be less than 248 characters and file names must be less than 260 characters.</exception>
		// Token: 0x06000588 RID: 1416 RVA: 0x000101F4 File Offset: 0x0000E3F4
		public DirectoryCatalog(string path, string searchPattern, ICompositionElement definitionOrigin)
		{
			this._thisLock = new Lock();
			base..ctor();
			Requires.NotNullOrEmpty(path, "path");
			Requires.NotNullOrEmpty(searchPattern, "searchPattern");
			Requires.NotNull<ICompositionElement>(definitionOrigin, "definitionOrigin");
			this._definitionOrigin = definitionOrigin;
			this.Initialize(path, searchPattern);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.DirectoryCatalog" /> class by using <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartDefinition" /> objects based on the specified search pattern in the specified directory path, using the specified reflection context.</summary>
		/// <param name="path">The path to the directory to scan for assemblies to add to the catalog.The path must be absolute or relative to <see cref="P:System.AppDomain.BaseDirectory" />.</param>
		/// <param name="searchPattern">The search string. The format of the string should be the same as specified for the <see cref="M:System.IO.Directory.GetFiles(System.String,System.String)" /> method.</param>
		/// <param name="reflectionContext">The context used to create parts.</param>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified <paramref name="path" /> is invalid (for example, it is on an unmapped drive).</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> or <paramref name="searchPattern" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The caller does not have the required permission.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="path" /> is a zero-length string, contains only white space, or contains one or more implementation-specific invalid characters.-or-
		///         <paramref name="searchPattern" /> does not contain a valid pattern.</exception>
		/// <exception cref="T:System.IO.PathTooLongException">The specified <paramref name="path" />, file name, or both exceed the system-defined maximum length. For example, on Windows-based computers, paths must be less than 248 characters and file names must be less than 260 characters.</exception>
		// Token: 0x06000589 RID: 1417 RVA: 0x00010244 File Offset: 0x0000E444
		public DirectoryCatalog(string path, string searchPattern, ReflectionContext reflectionContext)
		{
			this._thisLock = new Lock();
			base..ctor();
			Requires.NotNullOrEmpty(path, "path");
			Requires.NotNullOrEmpty(searchPattern, "searchPattern");
			Requires.NotNull<ReflectionContext>(reflectionContext, "reflectionContext");
			this._reflectionContext = reflectionContext;
			this._definitionOrigin = this;
			this.Initialize(path, searchPattern);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.DirectoryCatalog" /> class by using <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartDefinition" /> objects based on the specified search pattern in the specified directory path, using the specified reflection context.</summary>
		/// <param name="path">The path to the directory to scan for assemblies to add to the catalog.The path must be absolute or relative to <see cref="P:System.AppDomain.BaseDirectory" />.</param>
		/// <param name="searchPattern">The search string. The format of the string should be the same as specified for the <see cref="M:System.IO.Directory.GetFiles(System.String,System.String)" /> method.</param>
		/// <param name="reflectionContext">The context used to create parts.</param>
		/// <param name="definitionOrigin">The element used by diagnostics to identify the source for parts.</param>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified <paramref name="path" /> is invalid (for example, it is on an unmapped drive).</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> or <paramref name="searchPattern" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The caller does not have the required permission.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="path" /> is a zero-length string, contains only white space, or contains one or more implementation-specific invalid characters.-or-
		///         <paramref name="searchPattern" /> does not contain a valid pattern.</exception>
		/// <exception cref="T:System.IO.PathTooLongException">The specified <paramref name="path" />, file name, or both exceed the system-defined maximum length. For example, on Windows-based computers, paths must be less than 248 characters and file names must be less than 260 characters.</exception>
		// Token: 0x0600058A RID: 1418 RVA: 0x0001029C File Offset: 0x0000E49C
		public DirectoryCatalog(string path, string searchPattern, ReflectionContext reflectionContext, ICompositionElement definitionOrigin)
		{
			this._thisLock = new Lock();
			base..ctor();
			Requires.NotNullOrEmpty(path, "path");
			Requires.NotNullOrEmpty(searchPattern, "searchPattern");
			Requires.NotNull<ReflectionContext>(reflectionContext, "reflectionContext");
			Requires.NotNull<ICompositionElement>(definitionOrigin, "definitionOrigin");
			this._reflectionContext = reflectionContext;
			this._definitionOrigin = definitionOrigin;
			this.Initialize(path, searchPattern);
		}

		/// <summary>Gets the translated absolute path observed by the <see cref="T:System.ComponentModel.Composition.Hosting.DirectoryCatalog" /> object.</summary>
		/// <returns>The translated absolute path observed by the catalog.</returns>
		// Token: 0x17000169 RID: 361
		// (get) Token: 0x0600058B RID: 1419 RVA: 0x000102FE File Offset: 0x0000E4FE
		public string FullPath
		{
			get
			{
				return this._fullPath;
			}
		}

		/// <summary>Gets the collection of files currently loaded in the catalog.</summary>
		/// <returns>A collection of files currently loaded in the catalog.</returns>
		// Token: 0x1700016A RID: 362
		// (get) Token: 0x0600058C RID: 1420 RVA: 0x00010308 File Offset: 0x0000E508
		public ReadOnlyCollection<string> LoadedFiles
		{
			get
			{
				ReadOnlyCollection<string> loadedFiles;
				using (new ReadLock(this._thisLock))
				{
					loadedFiles = this._loadedFiles;
				}
				return loadedFiles;
			}
		}

		/// <summary>Gets the path observed by the <see cref="T:System.ComponentModel.Composition.Hosting.DirectoryCatalog" /> object.</summary>
		/// <returns>The path observed by the catalog.</returns>
		// Token: 0x1700016B RID: 363
		// (get) Token: 0x0600058D RID: 1421 RVA: 0x0001034C File Offset: 0x0000E54C
		public string Path
		{
			get
			{
				return this._path;
			}
		}

		/// <summary>Gets the search pattern that is passed into the constructor of the <see cref="T:System.ComponentModel.Composition.Hosting.DirectoryCatalog" /> object.</summary>
		/// <returns>The search pattern the catalog uses to find files. The default is *.dll, which returns all DLL files.</returns>
		// Token: 0x1700016C RID: 364
		// (get) Token: 0x0600058E RID: 1422 RVA: 0x00010354 File Offset: 0x0000E554
		public string SearchPattern
		{
			get
			{
				return this._searchPattern;
			}
		}

		/// <summary>Occurs when the contents of the catalog has changed.</summary>
		// Token: 0x14000007 RID: 7
		// (add) Token: 0x0600058F RID: 1423 RVA: 0x0001035C File Offset: 0x0000E55C
		// (remove) Token: 0x06000590 RID: 1424 RVA: 0x00010394 File Offset: 0x0000E594
		public event EventHandler<ComposablePartCatalogChangeEventArgs> Changed
		{
			[CompilerGenerated]
			add
			{
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler = this.Changed;
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<ComposablePartCatalogChangeEventArgs> value2 = (EventHandler<ComposablePartCatalogChangeEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<ComposablePartCatalogChangeEventArgs>>(ref this.Changed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler = this.Changed;
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<ComposablePartCatalogChangeEventArgs> value2 = (EventHandler<ComposablePartCatalogChangeEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<ComposablePartCatalogChangeEventArgs>>(ref this.Changed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		/// <summary>Occurs when the catalog is changing.</summary>
		// Token: 0x14000008 RID: 8
		// (add) Token: 0x06000591 RID: 1425 RVA: 0x000103CC File Offset: 0x0000E5CC
		// (remove) Token: 0x06000592 RID: 1426 RVA: 0x00010404 File Offset: 0x0000E604
		public event EventHandler<ComposablePartCatalogChangeEventArgs> Changing
		{
			[CompilerGenerated]
			add
			{
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler = this.Changing;
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<ComposablePartCatalogChangeEventArgs> value2 = (EventHandler<ComposablePartCatalogChangeEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<ComposablePartCatalogChangeEventArgs>>(ref this.Changing, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler = this.Changing;
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<ComposablePartCatalogChangeEventArgs> value2 = (EventHandler<ComposablePartCatalogChangeEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<ComposablePartCatalogChangeEventArgs>>(ref this.Changing, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.ComponentModel.Composition.Hosting.DirectoryCatalog" /> and optionally releases the managed resources. </summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x06000593 RID: 1427 RVA: 0x0001043C File Offset: 0x0000E63C
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && !this._isDisposed)
				{
					bool flag = false;
					ComposablePartCatalogCollection composablePartCatalogCollection = null;
					try
					{
						using (new WriteLock(this._thisLock))
						{
							if (!this._isDisposed)
							{
								flag = true;
								composablePartCatalogCollection = this._catalogCollection;
								this._catalogCollection = null;
								this._assemblyCatalogs = null;
								this._isDisposed = true;
							}
						}
					}
					finally
					{
						if (composablePartCatalogCollection != null)
						{
							composablePartCatalogCollection.Dispose();
						}
						if (flag)
						{
							this._thisLock.Dispose();
						}
					}
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		/// <summary>Returns an enumerator that iterates through the catalog.</summary>
		/// <returns>An enumerator that can be used to iterate through the catalog.</returns>
		// Token: 0x06000594 RID: 1428 RVA: 0x000104EC File Offset: 0x0000E6EC
		public override IEnumerator<ComposablePartDefinition> GetEnumerator()
		{
			return this._catalogCollection.SelectMany((ComposablePartCatalog catalog) => catalog).GetEnumerator();
		}

		/// <summary>Gets the export definitions that match the constraint expressed by the specified import definition.</summary>
		/// <param name="definition">The conditions of the <see cref="T:System.ComponentModel.Composition.Primitives.ExportDefinition" /> objects to be returned.</param>
		/// <returns>A collection of objects that contain the <see cref="T:System.ComponentModel.Composition.Primitives.ExportDefinition" /> objects and their associated <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartDefinition" /> objects that match the constraint specified by <paramref name="definition" />.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.ComponentModel.Composition.Hosting.DirectoryCatalog" /> object has been disposed.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="definition" /> is <see langword="null" />.</exception>
		// Token: 0x06000595 RID: 1429 RVA: 0x00010520 File Offset: 0x0000E720
		public override IEnumerable<Tuple<ComposablePartDefinition, ExportDefinition>> GetExports(ImportDefinition definition)
		{
			this.ThrowIfDisposed();
			Requires.NotNull<ImportDefinition>(definition, "definition");
			return this._catalogCollection.SelectMany((ComposablePartCatalog catalog) => catalog.GetExports(definition));
		}

		/// <summary>Raises the <see cref="E:System.ComponentModel.Composition.Hosting.DirectoryCatalog.Changed" /> event.</summary>
		/// <param name="e">An object  that contains the event data. </param>
		// Token: 0x06000596 RID: 1430 RVA: 0x00010568 File Offset: 0x0000E768
		protected virtual void OnChanged(ComposablePartCatalogChangeEventArgs e)
		{
			EventHandler<ComposablePartCatalogChangeEventArgs> changed = this.Changed;
			if (changed != null)
			{
				changed(this, e);
			}
		}

		/// <summary>Raises the <see cref="E:System.ComponentModel.Composition.Hosting.DirectoryCatalog.Changing" /> event.</summary>
		/// <param name="e">An object  that contains the event data.</param>
		// Token: 0x06000597 RID: 1431 RVA: 0x00010588 File Offset: 0x0000E788
		protected virtual void OnChanging(ComposablePartCatalogChangeEventArgs e)
		{
			EventHandler<ComposablePartCatalogChangeEventArgs> changing = this.Changing;
			if (changing != null)
			{
				changing(this, e);
			}
		}

		/// <summary>Refreshes the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartDefinition" /> objects with the latest files in the directory that match the search pattern. </summary>
		// Token: 0x06000598 RID: 1432 RVA: 0x000105A8 File Offset: 0x0000E7A8
		public void Refresh()
		{
			this.ThrowIfDisposed();
			Assumes.NotNull<ReadOnlyCollection<string>>(this._loadedFiles);
			ComposablePartDefinition[] addedDefinitions;
			ComposablePartDefinition[] removedDefinitions;
			for (;;)
			{
				string[] files = this.GetFiles();
				object loadedFiles;
				string[] beforeFiles;
				using (new ReadLock(this._thisLock))
				{
					loadedFiles = this._loadedFiles;
					beforeFiles = this._loadedFiles.ToArray<string>();
				}
				List<Tuple<string, AssemblyCatalog>> list;
				List<Tuple<string, AssemblyCatalog>> list2;
				this.DiffChanges(beforeFiles, files, out list, out list2);
				if (list.Count == 0 && list2.Count == 0)
				{
					break;
				}
				addedDefinitions = list.SelectMany((Tuple<string, AssemblyCatalog> cat) => cat.Item2).ToArray<ComposablePartDefinition>();
				removedDefinitions = list2.SelectMany((Tuple<string, AssemblyCatalog> cat) => cat.Item2).ToArray<ComposablePartDefinition>();
				using (AtomicComposition atomicComposition = new AtomicComposition())
				{
					ComposablePartCatalogChangeEventArgs e = new ComposablePartCatalogChangeEventArgs(addedDefinitions, removedDefinitions, atomicComposition);
					this.OnChanging(e);
					using (new WriteLock(this._thisLock))
					{
						if (loadedFiles != this._loadedFiles)
						{
							continue;
						}
						foreach (Tuple<string, AssemblyCatalog> tuple in list)
						{
							this._assemblyCatalogs.Add(tuple.Item1, tuple.Item2);
							this._catalogCollection.Add(tuple.Item2);
						}
						foreach (Tuple<string, AssemblyCatalog> tuple2 in list2)
						{
							this._assemblyCatalogs.Remove(tuple2.Item1);
							this._catalogCollection.Remove(tuple2.Item2);
						}
						this._loadedFiles = files.ToReadOnlyCollection<string>();
						atomicComposition.Complete();
					}
				}
				goto IL_1CF;
			}
			return;
			IL_1CF:
			ComposablePartCatalogChangeEventArgs e2 = new ComposablePartCatalogChangeEventArgs(addedDefinitions, removedDefinitions, null);
			this.OnChanged(e2);
		}

		/// <summary>Gets a string representation of the directory catalog.</summary>
		/// <returns>A string representation of the catalog.</returns>
		// Token: 0x06000599 RID: 1433 RVA: 0x000107D8 File Offset: 0x0000E9D8
		public override string ToString()
		{
			return this.GetDisplayName();
		}

		// Token: 0x0600059A RID: 1434 RVA: 0x000107E0 File Offset: 0x0000E9E0
		private AssemblyCatalog CreateAssemblyCatalogGuarded(string assemblyFilePath)
		{
			Exception exception = null;
			try
			{
				return (this._reflectionContext != null) ? new AssemblyCatalog(assemblyFilePath, this._reflectionContext, this) : new AssemblyCatalog(assemblyFilePath, this);
			}
			catch (FileNotFoundException exception)
			{
			}
			catch (FileLoadException exception)
			{
			}
			catch (BadImageFormatException exception)
			{
			}
			catch (ReflectionTypeLoadException exception)
			{
			}
			CompositionTrace.AssemblyLoadFailed(this, assemblyFilePath, exception);
			return null;
		}

		// Token: 0x0600059B RID: 1435 RVA: 0x0001085C File Offset: 0x0000EA5C
		private void DiffChanges(string[] beforeFiles, string[] afterFiles, out List<Tuple<string, AssemblyCatalog>> catalogsToAdd, out List<Tuple<string, AssemblyCatalog>> catalogsToRemove)
		{
			catalogsToAdd = new List<Tuple<string, AssemblyCatalog>>();
			catalogsToRemove = new List<Tuple<string, AssemblyCatalog>>();
			foreach (string text in afterFiles.Except(beforeFiles))
			{
				AssemblyCatalog assemblyCatalog = this.CreateAssemblyCatalogGuarded(text);
				if (assemblyCatalog != null)
				{
					catalogsToAdd.Add(new Tuple<string, AssemblyCatalog>(text, assemblyCatalog));
				}
			}
			IEnumerable<string> enumerable = beforeFiles.Except(afterFiles);
			using (new ReadLock(this._thisLock))
			{
				foreach (string text2 in enumerable)
				{
					AssemblyCatalog item;
					if (this._assemblyCatalogs.TryGetValue(text2, out item))
					{
						catalogsToRemove.Add(new Tuple<string, AssemblyCatalog>(text2, item));
					}
				}
			}
		}

		// Token: 0x0600059C RID: 1436 RVA: 0x00010950 File Offset: 0x0000EB50
		private string GetDisplayName()
		{
			return string.Format(CultureInfo.CurrentCulture, "{0} (Path=\"{1}\")", base.GetType().Name, this._path);
		}

		// Token: 0x0600059D RID: 1437 RVA: 0x00010972 File Offset: 0x0000EB72
		private string[] GetFiles()
		{
			return Directory.GetFiles(this._fullPath, this._searchPattern);
		}

		// Token: 0x0600059E RID: 1438 RVA: 0x00010985 File Offset: 0x0000EB85
		private static string GetFullPath(string path)
		{
			if (!System.IO.Path.IsPathRooted(path) && AppDomain.CurrentDomain.BaseDirectory != null)
			{
				path = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path);
			}
			return System.IO.Path.GetFullPath(path);
		}

		// Token: 0x0600059F RID: 1439 RVA: 0x000109B4 File Offset: 0x0000EBB4
		private void Initialize(string path, string searchPattern)
		{
			this._path = path;
			this._fullPath = DirectoryCatalog.GetFullPath(path);
			this._searchPattern = searchPattern;
			this._assemblyCatalogs = new Dictionary<string, AssemblyCatalog>();
			this._catalogCollection = new ComposablePartCatalogCollection(null, null, null);
			this._loadedFiles = this.GetFiles().ToReadOnlyCollection<string>();
			foreach (string text in this._loadedFiles)
			{
				AssemblyCatalog assemblyCatalog = this.CreateAssemblyCatalogGuarded(text);
				if (assemblyCatalog != null)
				{
					this._assemblyCatalogs.Add(text, assemblyCatalog);
					this._catalogCollection.Add(assemblyCatalog);
				}
			}
		}

		// Token: 0x060005A0 RID: 1440 RVA: 0x00010A64 File Offset: 0x0000EC64
		[DebuggerStepThrough]
		private void ThrowIfDisposed()
		{
			if (this._isDisposed)
			{
				throw ExceptionBuilder.CreateObjectDisposed(this);
			}
		}

		/// <summary>Gets the display name of the directory catalog.</summary>
		/// <returns>A string that contains a human-readable display name of the directory catalog.</returns>
		// Token: 0x1700016D RID: 365
		// (get) Token: 0x060005A1 RID: 1441 RVA: 0x000107D8 File Offset: 0x0000E9D8
		string ICompositionElement.DisplayName
		{
			get
			{
				return this.GetDisplayName();
			}
		}

		/// <summary>Gets the composition element from which the directory catalog originated.</summary>
		/// <returns>Always <see langword="null" />.</returns>
		// Token: 0x1700016E RID: 366
		// (get) Token: 0x060005A2 RID: 1442 RVA: 0x00009EC0 File Offset: 0x000080C0
		ICompositionElement ICompositionElement.Origin
		{
			get
			{
				return null;
			}
		}

		// Token: 0x0400024C RID: 588
		private readonly Lock _thisLock;

		// Token: 0x0400024D RID: 589
		private readonly ICompositionElement _definitionOrigin;

		// Token: 0x0400024E RID: 590
		private ComposablePartCatalogCollection _catalogCollection;

		// Token: 0x0400024F RID: 591
		private Dictionary<string, AssemblyCatalog> _assemblyCatalogs;

		// Token: 0x04000250 RID: 592
		private volatile bool _isDisposed;

		// Token: 0x04000251 RID: 593
		private string _path;

		// Token: 0x04000252 RID: 594
		private string _fullPath;

		// Token: 0x04000253 RID: 595
		private string _searchPattern;

		// Token: 0x04000254 RID: 596
		private ReadOnlyCollection<string> _loadedFiles;

		// Token: 0x04000255 RID: 597
		private readonly ReflectionContext _reflectionContext;

		// Token: 0x04000256 RID: 598
		[CompilerGenerated]
		private EventHandler<ComposablePartCatalogChangeEventArgs> Changed;

		// Token: 0x04000257 RID: 599
		[CompilerGenerated]
		private EventHandler<ComposablePartCatalogChangeEventArgs> Changing;

		// Token: 0x020000D8 RID: 216
		internal class DirectoryCatalogDebuggerProxy
		{
			// Token: 0x060005A3 RID: 1443 RVA: 0x00010A77 File Offset: 0x0000EC77
			public DirectoryCatalogDebuggerProxy(DirectoryCatalog catalog)
			{
				Requires.NotNull<DirectoryCatalog>(catalog, "catalog");
				this._catalog = catalog;
			}

			// Token: 0x1700016F RID: 367
			// (get) Token: 0x060005A4 RID: 1444 RVA: 0x00010A91 File Offset: 0x0000EC91
			public ReadOnlyCollection<Assembly> Assemblies
			{
				get
				{
					return (from catalog in this._catalog._assemblyCatalogs.Values
					select catalog.Assembly).ToReadOnlyCollection<Assembly>();
				}
			}

			// Token: 0x17000170 RID: 368
			// (get) Token: 0x060005A5 RID: 1445 RVA: 0x00010ACC File Offset: 0x0000ECCC
			public ReflectionContext ReflectionContext
			{
				get
				{
					return this._catalog._reflectionContext;
				}
			}

			// Token: 0x17000171 RID: 369
			// (get) Token: 0x060005A6 RID: 1446 RVA: 0x00010AD9 File Offset: 0x0000ECD9
			public string SearchPattern
			{
				get
				{
					return this._catalog.SearchPattern;
				}
			}

			// Token: 0x17000172 RID: 370
			// (get) Token: 0x060005A7 RID: 1447 RVA: 0x00010AE6 File Offset: 0x0000ECE6
			public string Path
			{
				get
				{
					return this._catalog._path;
				}
			}

			// Token: 0x17000173 RID: 371
			// (get) Token: 0x060005A8 RID: 1448 RVA: 0x00010AF3 File Offset: 0x0000ECF3
			public string FullPath
			{
				get
				{
					return this._catalog._fullPath;
				}
			}

			// Token: 0x17000174 RID: 372
			// (get) Token: 0x060005A9 RID: 1449 RVA: 0x00010B00 File Offset: 0x0000ED00
			public ReadOnlyCollection<string> LoadedFiles
			{
				get
				{
					return this._catalog._loadedFiles;
				}
			}

			// Token: 0x17000175 RID: 373
			// (get) Token: 0x060005AA RID: 1450 RVA: 0x00010B0D File Offset: 0x0000ED0D
			public ReadOnlyCollection<ComposablePartDefinition> Parts
			{
				get
				{
					return this._catalog.Parts.ToReadOnlyCollection<ComposablePartDefinition>();
				}
			}

			// Token: 0x04000258 RID: 600
			private readonly DirectoryCatalog _catalog;

			// Token: 0x020000D9 RID: 217
			[CompilerGenerated]
			[Serializable]
			private sealed class <>c
			{
				// Token: 0x060005AB RID: 1451 RVA: 0x00010B1F File Offset: 0x0000ED1F
				// Note: this type is marked as 'beforefieldinit'.
				static <>c()
				{
				}

				// Token: 0x060005AC RID: 1452 RVA: 0x000025B0 File Offset: 0x000007B0
				public <>c()
				{
				}

				// Token: 0x060005AD RID: 1453 RVA: 0x00010B2B File Offset: 0x0000ED2B
				internal Assembly <get_Assemblies>b__3_0(AssemblyCatalog catalog)
				{
					return catalog.Assembly;
				}

				// Token: 0x04000259 RID: 601
				public static readonly DirectoryCatalog.DirectoryCatalogDebuggerProxy.<>c <>9 = new DirectoryCatalog.DirectoryCatalogDebuggerProxy.<>c();

				// Token: 0x0400025A RID: 602
				public static Func<AssemblyCatalog, Assembly> <>9__3_0;
			}
		}

		// Token: 0x020000DA RID: 218
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x060005AE RID: 1454 RVA: 0x00010B33 File Offset: 0x0000ED33
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x060005AF RID: 1455 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c()
			{
			}

			// Token: 0x060005B0 RID: 1456 RVA: 0x00009CD6 File Offset: 0x00007ED6
			internal IEnumerable<ComposablePartDefinition> <GetEnumerator>b__34_0(ComposablePartCatalog catalog)
			{
				return catalog;
			}

			// Token: 0x060005B1 RID: 1457 RVA: 0x00010B3F File Offset: 0x0000ED3F
			internal IEnumerable<ComposablePartDefinition> <Refresh>b__38_0(Tuple<string, AssemblyCatalog> cat)
			{
				return cat.Item2;
			}

			// Token: 0x060005B2 RID: 1458 RVA: 0x00010B3F File Offset: 0x0000ED3F
			internal IEnumerable<ComposablePartDefinition> <Refresh>b__38_1(Tuple<string, AssemblyCatalog> cat)
			{
				return cat.Item2;
			}

			// Token: 0x0400025B RID: 603
			public static readonly DirectoryCatalog.<>c <>9 = new DirectoryCatalog.<>c();

			// Token: 0x0400025C RID: 604
			public static Func<ComposablePartCatalog, IEnumerable<ComposablePartDefinition>> <>9__34_0;

			// Token: 0x0400025D RID: 605
			public static Func<Tuple<string, AssemblyCatalog>, IEnumerable<ComposablePartDefinition>> <>9__38_0;

			// Token: 0x0400025E RID: 606
			public static Func<Tuple<string, AssemblyCatalog>, IEnumerable<ComposablePartDefinition>> <>9__38_1;
		}

		// Token: 0x020000DB RID: 219
		[CompilerGenerated]
		private sealed class <>c__DisplayClass35_0
		{
			// Token: 0x060005B3 RID: 1459 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass35_0()
			{
			}

			// Token: 0x060005B4 RID: 1460 RVA: 0x00010B47 File Offset: 0x0000ED47
			internal IEnumerable<Tuple<ComposablePartDefinition, ExportDefinition>> <GetExports>b__0(ComposablePartCatalog catalog)
			{
				return catalog.GetExports(this.definition);
			}

			// Token: 0x0400025F RID: 607
			public ImportDefinition definition;
		}
	}
}
