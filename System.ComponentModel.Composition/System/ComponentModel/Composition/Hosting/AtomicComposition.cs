﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.Hosting
{
	/// <summary>Represents a single composition operation for transactional composition.</summary>
	// Token: 0x020000A1 RID: 161
	public class AtomicComposition : IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.AtomicComposition" /> class. </summary>
		// Token: 0x0600043F RID: 1087 RVA: 0x0000B571 File Offset: 0x00009771
		public AtomicComposition() : this(null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.AtomicComposition" /> class with the specified parent <see cref="T:System.ComponentModel.Composition.Hosting.AtomicComposition" />.</summary>
		/// <param name="outerAtomicComposition">The parent of this composition operation.</param>
		// Token: 0x06000440 RID: 1088 RVA: 0x0000B57A File Offset: 0x0000977A
		public AtomicComposition(AtomicComposition outerAtomicComposition)
		{
			if (outerAtomicComposition != null)
			{
				this._outerAtomicComposition = outerAtomicComposition;
				this._outerAtomicComposition.ContainsInnerAtomicComposition = true;
			}
		}

		/// <summary>Saves a key-value pair in the transaction to track tentative state.</summary>
		/// <param name="key">The key to save.</param>
		/// <param name="value">The value to save.</param>
		// Token: 0x06000441 RID: 1089 RVA: 0x0000B598 File Offset: 0x00009798
		public void SetValue(object key, object value)
		{
			this.ThrowIfDisposed();
			this.ThrowIfCompleted();
			this.ThrowIfContainsInnerAtomicComposition();
			Requires.NotNull<object>(key, "key");
			this.SetValueInternal(key, value);
		}

		/// <summary>Gets a value saved by the <see cref="M:System.ComponentModel.Composition.Hosting.AtomicComposition.SetValue(System.Object,System.Object)" /> method.</summary>
		/// <param name="key">The key to retrieve from.</param>
		/// <param name="value">The retrieved value.</param>
		/// <typeparam name="T">The type of the value to be retrieved.</typeparam>
		/// <returns>
		///     <see langword="true" /> if the value was successfully retrieved; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000442 RID: 1090 RVA: 0x0000B5BF File Offset: 0x000097BF
		public bool TryGetValue<T>(object key, out T value)
		{
			return this.TryGetValue<T>(key, false, out value);
		}

		/// <summary>Gets a value saved by the <see cref="M:System.ComponentModel.Composition.Hosting.AtomicComposition.SetValue(System.Object,System.Object)" /> method, with the option of not searching parent transactions.</summary>
		/// <param name="key">The key to retrieve from.</param>
		/// <param name="localAtomicCompositionOnly">
		///       <see langword="true" /> to exclude parent transactions; otherwise, <see langword="false" />.</param>
		/// <param name="value">The retrieved value.</param>
		/// <typeparam name="T">The type of the value to be retrieved.</typeparam>
		/// <returns>
		///     <see langword="true" /> if the value was successfully retrieved; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000443 RID: 1091 RVA: 0x0000B5CA File Offset: 0x000097CA
		public bool TryGetValue<T>(object key, bool localAtomicCompositionOnly, out T value)
		{
			this.ThrowIfDisposed();
			this.ThrowIfCompleted();
			Requires.NotNull<object>(key, "key");
			return this.TryGetValueInternal<T>(key, localAtomicCompositionOnly, out value);
		}

		/// <summary>Adds an action to be executed when the overall composition operation completes successfully.</summary>
		/// <param name="completeAction">The action to be executed.</param>
		// Token: 0x06000444 RID: 1092 RVA: 0x0000B5EC File Offset: 0x000097EC
		public void AddCompleteAction(Action completeAction)
		{
			this.ThrowIfDisposed();
			this.ThrowIfCompleted();
			this.ThrowIfContainsInnerAtomicComposition();
			Requires.NotNull<Action>(completeAction, "completeAction");
			if (this._completeActionList == null)
			{
				this._completeActionList = new List<Action>();
			}
			this._completeActionList.Add(completeAction);
		}

		/// <summary>Adds an action to be executed if the overall composition operation fails.</summary>
		/// <param name="revertAction">The action to be executed.</param>
		// Token: 0x06000445 RID: 1093 RVA: 0x0000B62A File Offset: 0x0000982A
		public void AddRevertAction(Action revertAction)
		{
			this.ThrowIfDisposed();
			this.ThrowIfCompleted();
			this.ThrowIfContainsInnerAtomicComposition();
			Requires.NotNull<Action>(revertAction, "revertAction");
			if (this._revertActionList == null)
			{
				this._revertActionList = new List<Action>();
			}
			this._revertActionList.Add(revertAction);
		}

		/// <summary>Marks this composition operation as complete.</summary>
		// Token: 0x06000446 RID: 1094 RVA: 0x0000B668 File Offset: 0x00009868
		public void Complete()
		{
			this.ThrowIfDisposed();
			this.ThrowIfCompleted();
			if (this._outerAtomicComposition == null)
			{
				this.FinalComplete();
			}
			else
			{
				this.CopyComplete();
			}
			this._isCompleted = true;
		}

		/// <summary>Releases all resources used by the current instance of the <see cref="T:System.ComponentModel.Composition.Hosting.AtomicComposition" /> class, and mark this composition operation as failed.</summary>
		// Token: 0x06000447 RID: 1095 RVA: 0x0000B693 File Offset: 0x00009893
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.ComponentModel.Composition.Hosting.AtomicComposition" /> and optionally releases the managed resources. </summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x06000448 RID: 1096 RVA: 0x0000B6A4 File Offset: 0x000098A4
		protected virtual void Dispose(bool disposing)
		{
			this.ThrowIfDisposed();
			this._isDisposed = true;
			if (this._outerAtomicComposition != null)
			{
				this._outerAtomicComposition.ContainsInnerAtomicComposition = false;
			}
			if (!this._isCompleted && this._revertActionList != null)
			{
				for (int i = this._revertActionList.Count - 1; i >= 0; i--)
				{
					this._revertActionList[i]();
				}
				this._revertActionList = null;
			}
		}

		// Token: 0x06000449 RID: 1097 RVA: 0x0000B714 File Offset: 0x00009914
		private void FinalComplete()
		{
			if (this._completeActionList != null)
			{
				foreach (Action action in this._completeActionList)
				{
					action();
				}
				this._completeActionList = null;
			}
		}

		// Token: 0x0600044A RID: 1098 RVA: 0x0000B774 File Offset: 0x00009974
		private void CopyComplete()
		{
			Assumes.NotNull<AtomicComposition>(this._outerAtomicComposition);
			this._outerAtomicComposition.ContainsInnerAtomicComposition = false;
			if (this._completeActionList != null)
			{
				foreach (Action completeAction in this._completeActionList)
				{
					this._outerAtomicComposition.AddCompleteAction(completeAction);
				}
			}
			if (this._revertActionList != null)
			{
				foreach (Action revertAction in this._revertActionList)
				{
					this._outerAtomicComposition.AddRevertAction(revertAction);
				}
			}
			for (int i = 0; i < this._valueCount; i++)
			{
				this._outerAtomicComposition.SetValueInternal(this._values[i].Key, this._values[i].Value);
			}
		}

		// Token: 0x17000141 RID: 321
		// (set) Token: 0x0600044B RID: 1099 RVA: 0x0000B878 File Offset: 0x00009A78
		private bool ContainsInnerAtomicComposition
		{
			set
			{
				if (value && this._containsInnerAtomicComposition)
				{
					throw new InvalidOperationException(Strings.AtomicComposition_AlreadyNested);
				}
				this._containsInnerAtomicComposition = value;
			}
		}

		// Token: 0x0600044C RID: 1100 RVA: 0x0000B898 File Offset: 0x00009A98
		private bool TryGetValueInternal<T>(object key, bool localAtomicCompositionOnly, out T value)
		{
			for (int i = 0; i < this._valueCount; i++)
			{
				if (this._values[i].Key == key)
				{
					value = (T)((object)this._values[i].Value);
					return true;
				}
			}
			if (!localAtomicCompositionOnly && this._outerAtomicComposition != null)
			{
				return this._outerAtomicComposition.TryGetValueInternal<T>(key, localAtomicCompositionOnly, out value);
			}
			value = default(T);
			return false;
		}

		// Token: 0x0600044D RID: 1101 RVA: 0x0000B90C File Offset: 0x00009B0C
		private void SetValueInternal(object key, object value)
		{
			for (int i = 0; i < this._valueCount; i++)
			{
				if (this._values[i].Key == key)
				{
					this._values[i] = new KeyValuePair<object, object>(key, value);
					return;
				}
			}
			if (this._values == null || this._valueCount == this._values.Length)
			{
				KeyValuePair<object, object>[] array = new KeyValuePair<object, object>[(this._valueCount == 0) ? 5 : (this._valueCount * 2)];
				if (this._values != null)
				{
					Array.Copy(this._values, array, this._valueCount);
				}
				this._values = array;
			}
			this._values[this._valueCount] = new KeyValuePair<object, object>(key, value);
			this._valueCount++;
		}

		// Token: 0x0600044E RID: 1102 RVA: 0x0000B9CA File Offset: 0x00009BCA
		[DebuggerStepThrough]
		private void ThrowIfContainsInnerAtomicComposition()
		{
			if (this._containsInnerAtomicComposition)
			{
				throw new InvalidOperationException(Strings.AtomicComposition_PartOfAnotherAtomicComposition);
			}
		}

		// Token: 0x0600044F RID: 1103 RVA: 0x0000B9DF File Offset: 0x00009BDF
		[DebuggerStepThrough]
		private void ThrowIfCompleted()
		{
			if (this._isCompleted)
			{
				throw new InvalidOperationException(Strings.AtomicComposition_AlreadyCompleted);
			}
		}

		// Token: 0x06000450 RID: 1104 RVA: 0x0000B9F4 File Offset: 0x00009BF4
		[DebuggerStepThrough]
		private void ThrowIfDisposed()
		{
			if (this._isDisposed)
			{
				throw ExceptionBuilder.CreateObjectDisposed(this);
			}
		}

		// Token: 0x04000192 RID: 402
		private readonly AtomicComposition _outerAtomicComposition;

		// Token: 0x04000193 RID: 403
		private KeyValuePair<object, object>[] _values;

		// Token: 0x04000194 RID: 404
		private int _valueCount;

		// Token: 0x04000195 RID: 405
		private List<Action> _completeActionList;

		// Token: 0x04000196 RID: 406
		private List<Action> _revertActionList;

		// Token: 0x04000197 RID: 407
		private bool _isDisposed;

		// Token: 0x04000198 RID: 408
		private bool _isCompleted;

		// Token: 0x04000199 RID: 409
		private bool _containsInnerAtomicComposition;
	}
}
