﻿using System;
using System.ComponentModel.Composition.Primitives;
using Microsoft.Internal;
using Unity;

namespace System.ComponentModel.Composition.Hosting
{
	/// <summary>Provides methods to satisfy imports on an existing part instance.</summary>
	// Token: 0x020000D3 RID: 211
	public class CompositionService : ICompositionService, IDisposable
	{
		// Token: 0x0600055A RID: 1370 RVA: 0x0000F558 File Offset: 0x0000D758
		internal CompositionService(ComposablePartCatalog composablePartCatalog)
		{
			Assumes.NotNull<ComposablePartCatalog>(composablePartCatalog);
			this._notifyCatalog = (composablePartCatalog as INotifyComposablePartCatalogChanged);
			try
			{
				if (this._notifyCatalog != null)
				{
					this._notifyCatalog.Changing += this.OnCatalogChanging;
				}
				CompositionOptions compositionOptions = CompositionOptions.DisableSilentRejection | CompositionOptions.IsThreadSafe | CompositionOptions.ExportCompositionService;
				CompositionContainer compositionContainer = new CompositionContainer(composablePartCatalog, compositionOptions, Array.Empty<ExportProvider>());
				this._compositionContainer = compositionContainer;
			}
			catch
			{
				if (this._notifyCatalog != null)
				{
					this._notifyCatalog.Changing -= this.OnCatalogChanging;
				}
				throw;
			}
		}

		/// <summary>Composes the specified part, with recomposition and validation disabled.</summary>
		/// <param name="part">The part to compose.</param>
		// Token: 0x0600055B RID: 1371 RVA: 0x0000F5E8 File Offset: 0x0000D7E8
		public void SatisfyImportsOnce(ComposablePart part)
		{
			Requires.NotNull<ComposablePart>(part, "part");
			Assumes.NotNull<CompositionContainer>(this._compositionContainer);
			this._compositionContainer.SatisfyImportsOnce(part);
		}

		/// <summary>Releases all resources used by the current instance of the <see cref="T:System.ComponentModel.Composition.Hosting.CompositionContainer" /> class.</summary>
		// Token: 0x0600055C RID: 1372 RVA: 0x0000F60C File Offset: 0x0000D80C
		public void Dispose()
		{
			Assumes.NotNull<CompositionContainer>(this._compositionContainer);
			if (this._notifyCatalog != null)
			{
				this._notifyCatalog.Changing -= this.OnCatalogChanging;
			}
			this._compositionContainer.Dispose();
		}

		// Token: 0x0600055D RID: 1373 RVA: 0x0000F643 File Offset: 0x0000D843
		private void OnCatalogChanging(object sender, ComposablePartCatalogChangeEventArgs e)
		{
			throw new ChangeRejectedException(Strings.NotSupportedCatalogChanges);
		}

		// Token: 0x0600055E RID: 1374 RVA: 0x0000F64F File Offset: 0x0000D84F
		internal CompositionService()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0400023C RID: 572
		private CompositionContainer _compositionContainer;

		// Token: 0x0400023D RID: 573
		private INotifyComposablePartCatalogChanged _notifyCatalog;
	}
}
