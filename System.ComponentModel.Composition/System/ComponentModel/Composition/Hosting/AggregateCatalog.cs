﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.Hosting
{
	/// <summary>A catalog that combines the elements of <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartCatalog" /> objects. </summary>
	// Token: 0x0200009B RID: 155
	public class AggregateCatalog : ComposablePartCatalog, INotifyComposablePartCatalogChanged
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.AggregateCatalog" /> class.</summary>
		// Token: 0x06000400 RID: 1024 RVA: 0x0000AA5A File Offset: 0x00008C5A
		public AggregateCatalog() : this(null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.AggregateCatalog" /> class with the specified catalogs.</summary>
		/// <param name="catalogs">A array of <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartCatalog" /> objects to add to the <see cref="T:System.ComponentModel.Composition.Hosting.AggregateCatalog" />.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="catalogs" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="catalogs" /> contains an element that is <see langword="null" />.</exception>
		// Token: 0x06000401 RID: 1025 RVA: 0x0000AA63 File Offset: 0x00008C63
		public AggregateCatalog(params ComposablePartCatalog[] catalogs) : this(catalogs)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.AggregateCatalog" /> class with the specified catalogs.</summary>
		/// <param name="catalogs">A collection of <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartCatalog" /> objects to add to the <see cref="T:System.ComponentModel.Composition.Hosting.AggregateCatalog" /> or <see langword="null" /> to create an empty <see cref="T:System.ComponentModel.Composition.Hosting.AggregateCatalog" />. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="catalogs" /> contains an element that is <see langword="null" />.</exception>
		// Token: 0x06000402 RID: 1026 RVA: 0x0000AA6C File Offset: 0x00008C6C
		public AggregateCatalog(IEnumerable<ComposablePartCatalog> catalogs)
		{
			Requires.NullOrNotNullElements<ComposablePartCatalog>(catalogs, "catalogs");
			this._catalogs = new ComposablePartCatalogCollection(catalogs, new Action<ComposablePartCatalogChangeEventArgs>(this.OnChanged), new Action<ComposablePartCatalogChangeEventArgs>(this.OnChanging));
		}

		/// <summary>Occurs when the contents of the <see cref="T:System.ComponentModel.Composition.Hosting.AggregateCatalog" /> object have changed.</summary>
		// Token: 0x14000001 RID: 1
		// (add) Token: 0x06000403 RID: 1027 RVA: 0x0000AAA5 File Offset: 0x00008CA5
		// (remove) Token: 0x06000404 RID: 1028 RVA: 0x0000AAB3 File Offset: 0x00008CB3
		public event EventHandler<ComposablePartCatalogChangeEventArgs> Changed
		{
			add
			{
				this._catalogs.Changed += value;
			}
			remove
			{
				this._catalogs.Changed -= value;
			}
		}

		/// <summary>Occurs when the contents of the <see cref="T:System.ComponentModel.Composition.Hosting.AggregateCatalog" /> object are changing.</summary>
		// Token: 0x14000002 RID: 2
		// (add) Token: 0x06000405 RID: 1029 RVA: 0x0000AAC1 File Offset: 0x00008CC1
		// (remove) Token: 0x06000406 RID: 1030 RVA: 0x0000AACF File Offset: 0x00008CCF
		public event EventHandler<ComposablePartCatalogChangeEventArgs> Changing
		{
			add
			{
				this._catalogs.Changing += value;
			}
			remove
			{
				this._catalogs.Changing -= value;
			}
		}

		/// <summary>Gets the export definitions that match the constraint expressed by the specified definition.</summary>
		/// <param name="definition">The conditions of the <see cref="T:System.ComponentModel.Composition.Primitives.ExportDefinition" /> objects to be returned.</param>
		/// <returns>A collection of <see cref="T:System.Tuple`2" /> containing the <see cref="T:System.ComponentModel.Composition.Primitives.ExportDefinition" /> objects and their associated <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartDefinition" /> objects for objects that match the constraint specified by <paramref name="definition" />.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.ComponentModel.Composition.Hosting.AggregateCatalog" /> object has been disposed of.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="definition" /> is <see langword="null" />.</exception>
		// Token: 0x06000407 RID: 1031 RVA: 0x0000AAE0 File Offset: 0x00008CE0
		public override IEnumerable<Tuple<ComposablePartDefinition, ExportDefinition>> GetExports(ImportDefinition definition)
		{
			this.ThrowIfDisposed();
			Requires.NotNull<ImportDefinition>(definition, "definition");
			List<Tuple<ComposablePartDefinition, ExportDefinition>> list = new List<Tuple<ComposablePartDefinition, ExportDefinition>>();
			foreach (ComposablePartCatalog composablePartCatalog in this._catalogs)
			{
				foreach (Tuple<ComposablePartDefinition, ExportDefinition> item in composablePartCatalog.GetExports(definition))
				{
					list.Add(item);
				}
			}
			return list;
		}

		/// <summary>Gets the underlying catalogs of the <see cref="T:System.ComponentModel.Composition.Hosting.AggregateCatalog" /> object.</summary>
		/// <returns>A collection of <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartCatalog" /> objects that underlie the <see cref="T:System.ComponentModel.Composition.Hosting.AggregateCatalog" /> object.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.ComponentModel.Composition.Hosting.AggregateCatalog" /> object has been disposed of.</exception>
		// Token: 0x17000136 RID: 310
		// (get) Token: 0x06000408 RID: 1032 RVA: 0x0000AB7C File Offset: 0x00008D7C
		public ICollection<ComposablePartCatalog> Catalogs
		{
			get
			{
				this.ThrowIfDisposed();
				return this._catalogs;
			}
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.ComponentModel.Composition.Hosting.AggregateCatalog" /> and optionally releases the managed resources. </summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x06000409 RID: 1033 RVA: 0x0000AB8C File Offset: 0x00008D8C
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && Interlocked.CompareExchange(ref this._isDisposed, 1, 0) == 0)
				{
					this._catalogs.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		/// <summary>Returns an enumerator that iterates through the catalog.</summary>
		/// <returns>An enumerator that can be used to iterate through the catalog.</returns>
		// Token: 0x0600040A RID: 1034 RVA: 0x0000ABD0 File Offset: 0x00008DD0
		public override IEnumerator<ComposablePartDefinition> GetEnumerator()
		{
			return this._catalogs.SelectMany((ComposablePartCatalog catalog) => catalog).GetEnumerator();
		}

		/// <summary>Raises the <see cref="E:System.ComponentModel.Composition.Hosting.AggregateCatalog.Changed" /> event.</summary>
		/// <param name="e">A <see cref="T:System.ComponentModel.Composition.Hosting.ComposablePartCatalogChangeEventArgs" /> object that contains the event data. </param>
		// Token: 0x0600040B RID: 1035 RVA: 0x0000AC01 File Offset: 0x00008E01
		protected virtual void OnChanged(ComposablePartCatalogChangeEventArgs e)
		{
			this._catalogs.OnChanged(this, e);
		}

		/// <summary>Raises the <see cref="E:System.ComponentModel.Composition.Hosting.AggregateCatalog.Changing" /> event.</summary>
		/// <param name="e">A <see cref="T:System.ComponentModel.Composition.Hosting.ComposablePartCatalogChangeEventArgs" /> object that contains the event data. </param>
		// Token: 0x0600040C RID: 1036 RVA: 0x0000AC10 File Offset: 0x00008E10
		protected virtual void OnChanging(ComposablePartCatalogChangeEventArgs e)
		{
			this._catalogs.OnChanging(this, e);
		}

		// Token: 0x0600040D RID: 1037 RVA: 0x0000AC1F File Offset: 0x00008E1F
		[DebuggerStepThrough]
		private void ThrowIfDisposed()
		{
			if (this._isDisposed == 1)
			{
				throw ExceptionBuilder.CreateObjectDisposed(this);
			}
		}

		// Token: 0x0400017F RID: 383
		private ComposablePartCatalogCollection _catalogs;

		// Token: 0x04000180 RID: 384
		private volatile int _isDisposed;

		// Token: 0x0200009C RID: 156
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x0600040E RID: 1038 RVA: 0x0000AC33 File Offset: 0x00008E33
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x0600040F RID: 1039 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c()
			{
			}

			// Token: 0x06000410 RID: 1040 RVA: 0x00009CD6 File Offset: 0x00007ED6
			internal IEnumerable<ComposablePartDefinition> <GetEnumerator>b__15_0(ComposablePartCatalog catalog)
			{
				return catalog;
			}

			// Token: 0x04000181 RID: 385
			public static readonly AggregateCatalog.<>c <>9 = new AggregateCatalog.<>c();

			// Token: 0x04000182 RID: 386
			public static Func<ComposablePartCatalog, IEnumerable<ComposablePartDefinition>> <>9__15_0;
		}
	}
}
