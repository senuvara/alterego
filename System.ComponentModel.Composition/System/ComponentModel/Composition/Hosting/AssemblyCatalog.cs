﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Threading;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.Hosting
{
	/// <summary>Discovers attributed parts in a managed code assembly.</summary>
	// Token: 0x0200009F RID: 159
	[DebuggerTypeProxy(typeof(AssemblyCatalogDebuggerProxy))]
	public class AssemblyCatalog : ComposablePartCatalog, ICompositionElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.AssemblyCatalog" /> class with the specified code base.</summary>
		/// <param name="codeBase">A string that specifies the code base of the assembly (that is, the path to the assembly file) that contains the attributed <see cref="T:System.Type" /> objects to add to the <see cref="T:System.ComponentModel.Composition.Hosting.AssemblyCatalog" /> object.</param>
		/// <exception cref="T:System.BadImageFormatException">
		///         <paramref name="codeBase" /> is not a valid assembly. -or-Version 2.0 or earlier of the common language runtime is currently loaded and <paramref name="codeBase" /> was compiled with a later version.</exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have path discovery permission. </exception>
		/// <exception cref="T:System.IO.FileLoadException">
		///         <paramref name="codeBase" /> could not be loaded.-or-
		///         <paramref name="codeBase" /> specified a directory.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="codeBase" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.IO.FileNotFoundException">
		///         <paramref name="codeBase" /> is not found.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="codeBase" /> is a zero-length string, contains only white space, or contains one or more invalid characters as defined by <see cref="F:System.IO.Path.InvalidPathChars" />.</exception>
		/// <exception cref="T:System.IO.PathTooLongException">The specified path, file name, or both exceed the system-defined maximum length. </exception>
		// Token: 0x06000428 RID: 1064 RVA: 0x0000B14B File Offset: 0x0000934B
		public AssemblyCatalog(string codeBase)
		{
			Requires.NotNullOrEmpty(codeBase, "codeBase");
			this.InitializeAssemblyCatalog(AssemblyCatalog.LoadAssembly(codeBase));
			this._definitionOrigin = this;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.AssemblyCatalog" /> class with the specified code base and reflection context.</summary>
		/// <param name="codeBase">A string that specifies the code base of the assembly (that is, the path to the assembly file) that contains the attributed <see cref="T:System.Type" /> objects to add to the <see cref="T:System.ComponentModel.Composition.Hosting.AssemblyCatalog" /> object.</param>
		/// <param name="reflectionContext">The context used by the catalog to interpret types.</param>
		/// <exception cref="T:System.BadImageFormatException">
		///         <paramref name="codeBase" /> is not a valid assembly. -or-Version 2.0 or later of the common language runtime is currently loaded and <paramref name="codeBase" /> was compiled with a later version.</exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have path discovery permission. </exception>
		/// <exception cref="T:System.IO.FileLoadException">
		///         <paramref name="codeBase" /> could not be loaded.-or-
		///         <paramref name="codeBase" /> specified a directory.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="codebase" /> or <paramref name="reflectionContext" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.IO.FileNotFoundException">
		///         <paramref name="codeBase" /> is not found.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="codeBase" /> is a zero-length string, contains only white space, or contains one or more invalid characters as defined by <see cref="F:System.IO.Path.InvalidPathChars" />.</exception>
		/// <exception cref="T:System.IO.PathTooLongException">The specified path, file name, or both exceed the system-defined maximum length. </exception>
		// Token: 0x06000429 RID: 1065 RVA: 0x0000B17C File Offset: 0x0000937C
		public AssemblyCatalog(string codeBase, ReflectionContext reflectionContext)
		{
			Requires.NotNullOrEmpty(codeBase, "codeBase");
			Requires.NotNull<ReflectionContext>(reflectionContext, "reflectionContext");
			this.InitializeAssemblyCatalog(AssemblyCatalog.LoadAssembly(codeBase));
			this._reflectionContext = reflectionContext;
			this._definitionOrigin = this;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.AssemblyCatalog" /> class with the specified code base.</summary>
		/// <param name="codeBase">A string that specifies the code base of the assembly (that is, the path to the assembly file) that contains the attributed <see cref="T:System.Type" /> objects to add to the <see cref="T:System.ComponentModel.Composition.Hosting.AssemblyCatalog" /> object.</param>
		/// <param name="definitionOrigin">The element used by diagnostics to identify the sources of parts.</param>
		/// <exception cref="T:System.BadImageFormatException">
		///         <paramref name="codeBase" /> is not a valid assembly. -or-Version 2.0 or later of the common language runtime is currently loaded and <paramref name="codeBase" /> was compiled with a later version.</exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have path discovery permission. </exception>
		/// <exception cref="T:System.IO.FileLoadException">
		///         <paramref name="codeBase" /> could not be loaded.-or-
		///         <paramref name="codeBase" /> specified a directory.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="codebase" /> or <paramref name="definitionOrigin" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.IO.FileNotFoundException">
		///         <paramref name="codeBase" /> is not found.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="codeBase" /> is a zero-length string, contains only white space, or contains one or more invalid characters as defined by <see cref="F:System.IO.Path.InvalidPathChars" />.</exception>
		/// <exception cref="T:System.IO.PathTooLongException">The specified path, file name, or both exceed the system-defined maximum length. </exception>
		// Token: 0x0600042A RID: 1066 RVA: 0x0000B1CA File Offset: 0x000093CA
		public AssemblyCatalog(string codeBase, ICompositionElement definitionOrigin)
		{
			Requires.NotNullOrEmpty(codeBase, "codeBase");
			Requires.NotNull<ICompositionElement>(definitionOrigin, "definitionOrigin");
			this.InitializeAssemblyCatalog(AssemblyCatalog.LoadAssembly(codeBase));
			this._definitionOrigin = definitionOrigin;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.AssemblyCatalog" /> class with the specified code base and reflection context.</summary>
		/// <param name="codeBase">A string that specifies the code base of the assembly (that is, the path to the assembly file) that contains the attributed <see cref="T:System.Type" /> objects to add to the <see cref="T:System.ComponentModel.Composition.Hosting.AssemblyCatalog" /> object.</param>
		/// <param name="reflectionContext">The context used by the catalog to interpret types.</param>
		/// <param name="definitionOrigin">The element used by diagnostics to identify the sources of parts.</param>
		/// <exception cref="T:System.BadImageFormatException">
		///         <paramref name="codeBase" /> is not a valid assembly. -or-Version 2.0 or later of the common language runtime is currently loaded and <paramref name="codeBase" /> was compiled with a later version.</exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have path discovery permission. </exception>
		/// <exception cref="T:System.IO.FileLoadException">
		///         <paramref name="codeBase" /> could not be loaded.-or-
		///         <paramref name="codeBase" /> specified a directory.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="codebase" />, <paramref name="definitionOrigin" /> or <paramref name="reflectionContext" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.IO.FileNotFoundException">
		///         <paramref name="codeBase" /> is not found.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="codeBase" /> is a zero-length string, contains only white space, or contains one or more invalid characters as defined by <see cref="F:System.IO.Path.InvalidPathChars" />.</exception>
		/// <exception cref="T:System.IO.PathTooLongException">The specified path, file name, or both exceed the system-defined maximum length. </exception>
		// Token: 0x0600042B RID: 1067 RVA: 0x0000B208 File Offset: 0x00009408
		public AssemblyCatalog(string codeBase, ReflectionContext reflectionContext, ICompositionElement definitionOrigin)
		{
			Requires.NotNullOrEmpty(codeBase, "codeBase");
			Requires.NotNull<ReflectionContext>(reflectionContext, "reflectionContext");
			Requires.NotNull<ICompositionElement>(definitionOrigin, "definitionOrigin");
			this.InitializeAssemblyCatalog(AssemblyCatalog.LoadAssembly(codeBase));
			this._reflectionContext = reflectionContext;
			this._definitionOrigin = definitionOrigin;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.AssemblyCatalog" /> class with the specified assembly and reflection context.</summary>
		/// <param name="assembly">The assembly that contains the attributed <see cref="T:System.Type" /> objects to add to the <see cref="T:System.ComponentModel.Composition.Hosting.AssemblyCatalog" /> object.</param>
		/// <param name="reflectionContext">The context used by the catalog to interpret types.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="assembly" /> or <paramref name="reflectionContext" /> is <see langword="null" />.-or-
		///         <paramref name="assembly" /> was loaded in the reflection-only context.</exception>
		// Token: 0x0600042C RID: 1068 RVA: 0x0000B261 File Offset: 0x00009461
		public AssemblyCatalog(Assembly assembly, ReflectionContext reflectionContext)
		{
			Requires.NotNull<Assembly>(assembly, "assembly");
			Requires.NotNull<ReflectionContext>(reflectionContext, "reflectionContext");
			this.InitializeAssemblyCatalog(assembly);
			this._reflectionContext = reflectionContext;
			this._definitionOrigin = this;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.AssemblyCatalog" /> class with the specified assembly and reflection context.</summary>
		/// <param name="assembly">The assembly that contains the attributed <see cref="T:System.Type" /> objects to add to the <see cref="T:System.ComponentModel.Composition.Hosting.AssemblyCatalog" /> object.</param>
		/// <param name="reflectionContext">The context used by the catalog to interpret types.</param>
		/// <param name="definitionOrigin">The element used by diagnostics to identify the sources of parts.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="assembly" />, <paramref name="definitionOrigin" />, or <paramref name="reflectionContext" /> is <see langword="null" />.-or-
		///         <paramref name="assembly" /> was loaded in the reflection-only context.</exception>
		// Token: 0x0600042D RID: 1069 RVA: 0x0000B2A0 File Offset: 0x000094A0
		public AssemblyCatalog(Assembly assembly, ReflectionContext reflectionContext, ICompositionElement definitionOrigin)
		{
			Requires.NotNull<Assembly>(assembly, "assembly");
			Requires.NotNull<ReflectionContext>(reflectionContext, "reflectionContext");
			Requires.NotNull<ICompositionElement>(definitionOrigin, "definitionOrigin");
			this.InitializeAssemblyCatalog(assembly);
			this._reflectionContext = reflectionContext;
			this._definitionOrigin = definitionOrigin;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.AssemblyCatalog" /> class with the specified assembly.</summary>
		/// <param name="assembly">The assembly that contains the attributed <see cref="T:System.Type" /> objects to add to the <see cref="T:System.ComponentModel.Composition.Hosting.AssemblyCatalog" /> object.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="assembly" /> is <see langword="null" />.-or-
		///         <paramref name="assembly" /> was loaded in the reflection-only context.</exception>
		// Token: 0x0600042E RID: 1070 RVA: 0x0000B2F4 File Offset: 0x000094F4
		public AssemblyCatalog(Assembly assembly)
		{
			Requires.NotNull<Assembly>(assembly, "assembly");
			this.InitializeAssemblyCatalog(assembly);
			this._definitionOrigin = this;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.AssemblyCatalog" /> class with the specified assembly.</summary>
		/// <param name="assembly">The assembly that contains the attributed <see cref="T:System.Type" /> objects to add to the <see cref="T:System.ComponentModel.Composition.Hosting.AssemblyCatalog" /> object.</param>
		/// <param name="definitionOrigin">The element used by diagnostics to identify the sources of parts.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="assembly" /> or <paramref name="definitionOrigin" /> is <see langword="null" />.-or-
		///         <paramref name="assembly" /> was loaded in the reflection-only context.</exception>
		// Token: 0x0600042F RID: 1071 RVA: 0x0000B320 File Offset: 0x00009520
		public AssemblyCatalog(Assembly assembly, ICompositionElement definitionOrigin)
		{
			Requires.NotNull<Assembly>(assembly, "assembly");
			Requires.NotNull<ICompositionElement>(definitionOrigin, "definitionOrigin");
			this.InitializeAssemblyCatalog(assembly);
			this._definitionOrigin = definitionOrigin;
		}

		// Token: 0x06000430 RID: 1072 RVA: 0x0000B357 File Offset: 0x00009557
		private void InitializeAssemblyCatalog(Assembly assembly)
		{
			this._assembly = assembly;
		}

		/// <summary>Gets a collection of exports that match the conditions specified by the import definition.</summary>
		/// <param name="definition">Conditions that specify which exports to match.</param>
		/// <returns>A collection of exports that match the conditions specified by <paramref name="definition" />.</returns>
		// Token: 0x06000431 RID: 1073 RVA: 0x0000B362 File Offset: 0x00009562
		public override IEnumerable<Tuple<ComposablePartDefinition, ExportDefinition>> GetExports(ImportDefinition definition)
		{
			return this.InnerCatalog.GetExports(definition);
		}

		// Token: 0x1700013B RID: 315
		// (get) Token: 0x06000432 RID: 1074 RVA: 0x0000B370 File Offset: 0x00009570
		private ComposablePartCatalog InnerCatalog
		{
			get
			{
				this.ThrowIfDisposed();
				if (this._innerCatalog == null)
				{
					CatalogReflectionContextAttribute firstAttribute = this._assembly.GetFirstAttribute<CatalogReflectionContextAttribute>();
					Assembly assembly = (firstAttribute != null) ? firstAttribute.CreateReflectionContext().MapAssembly(this._assembly) : this._assembly;
					object thisLock = this._thisLock;
					lock (thisLock)
					{
						if (this._innerCatalog == null)
						{
							TypeCatalog innerCatalog = (this._reflectionContext != null) ? new TypeCatalog(assembly.GetTypes(), this._reflectionContext, this._definitionOrigin) : new TypeCatalog(assembly.GetTypes(), this._definitionOrigin);
							Thread.MemoryBarrier();
							this._innerCatalog = innerCatalog;
						}
					}
				}
				return this._innerCatalog;
			}
		}

		/// <summary>Gets the assembly whose attributed types are contained in the assembly catalog.</summary>
		/// <returns>The assembly whose attributed <see cref="T:System.Type" /> objects are contained in the <see cref="T:System.ComponentModel.Composition.Hosting.AssemblyCatalog" />.</returns>
		// Token: 0x1700013C RID: 316
		// (get) Token: 0x06000433 RID: 1075 RVA: 0x0000B444 File Offset: 0x00009644
		public Assembly Assembly
		{
			get
			{
				return this._assembly;
			}
		}

		/// <summary>Gets the display name of the <see cref="T:System.ComponentModel.Composition.Hosting.AssemblyCatalog" /> object.</summary>
		/// <returns>A string that represents the type and assembly of this <see cref="T:System.ComponentModel.Composition.Hosting.AssemblyCatalog" /> object.</returns>
		// Token: 0x1700013D RID: 317
		// (get) Token: 0x06000434 RID: 1076 RVA: 0x0000B44E File Offset: 0x0000964E
		string ICompositionElement.DisplayName
		{
			get
			{
				return this.GetDisplayName();
			}
		}

		/// <summary>Gets the composition element that this element originated from.</summary>
		/// <returns>Always <see langword="null" />.</returns>
		// Token: 0x1700013E RID: 318
		// (get) Token: 0x06000435 RID: 1077 RVA: 0x00009EC0 File Offset: 0x000080C0
		ICompositionElement ICompositionElement.Origin
		{
			get
			{
				return null;
			}
		}

		/// <summary>Gets a string representation of the assembly catalog.</summary>
		/// <returns>A representation of the assembly catalog.</returns>
		// Token: 0x06000436 RID: 1078 RVA: 0x0000B44E File Offset: 0x0000964E
		public override string ToString()
		{
			return this.GetDisplayName();
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.ComponentModel.Composition.Hosting.AssemblyCatalog" /> and optionally releases the managed resources. </summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x06000437 RID: 1079 RVA: 0x0000B458 File Offset: 0x00009658
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (Interlocked.CompareExchange(ref this._isDisposed, 1, 0) == 0 && disposing && this._innerCatalog != null)
				{
					this._innerCatalog.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		/// <summary>Returns an enumerator that iterates through the catalog.</summary>
		/// <returns>An enumerator that can be used to iterate through the catalog.</returns>
		// Token: 0x06000438 RID: 1080 RVA: 0x0000B4A8 File Offset: 0x000096A8
		public override IEnumerator<ComposablePartDefinition> GetEnumerator()
		{
			return this.InnerCatalog.GetEnumerator();
		}

		// Token: 0x06000439 RID: 1081 RVA: 0x0000B4B5 File Offset: 0x000096B5
		private void ThrowIfDisposed()
		{
			if (this._isDisposed == 1)
			{
				throw ExceptionBuilder.CreateObjectDisposed(this);
			}
		}

		// Token: 0x0600043A RID: 1082 RVA: 0x0000B4C7 File Offset: 0x000096C7
		private string GetDisplayName()
		{
			return string.Format(CultureInfo.CurrentCulture, "{0} (Assembly=\"{1}\")", base.GetType().Name, this.Assembly.FullName);
		}

		// Token: 0x0600043B RID: 1083 RVA: 0x0000B4F0 File Offset: 0x000096F0
		private static Assembly LoadAssembly(string codeBase)
		{
			Requires.NotNullOrEmpty(codeBase, "codeBase");
			AssemblyName assemblyName;
			try
			{
				assemblyName = AssemblyName.GetAssemblyName(codeBase);
			}
			catch (ArgumentException)
			{
				assemblyName = new AssemblyName();
				assemblyName.CodeBase = codeBase;
			}
			return Assembly.Load(assemblyName);
		}

		// Token: 0x0400018B RID: 395
		private readonly object _thisLock = new object();

		// Token: 0x0400018C RID: 396
		private readonly ICompositionElement _definitionOrigin;

		// Token: 0x0400018D RID: 397
		private volatile Assembly _assembly;

		// Token: 0x0400018E RID: 398
		private volatile ComposablePartCatalog _innerCatalog;

		// Token: 0x0400018F RID: 399
		private int _isDisposed;

		// Token: 0x04000190 RID: 400
		private ReflectionContext _reflectionContext;
	}
}
