﻿using System;

namespace System.ComponentModel.Composition.Hosting
{
	/// <summary>Provides notifications when a <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartCatalog" /> changes.</summary>
	// Token: 0x020000E6 RID: 230
	public interface INotifyComposablePartCatalogChanged
	{
		/// <summary>Occurs when a <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartCatalog" /> has changed.</summary>
		// Token: 0x1400000D RID: 13
		// (add) Token: 0x06000611 RID: 1553
		// (remove) Token: 0x06000612 RID: 1554
		event EventHandler<ComposablePartCatalogChangeEventArgs> Changed;

		/// <summary>Occurs when a <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartCatalog" /> is changing.</summary>
		// Token: 0x1400000E RID: 14
		// (add) Token: 0x06000613 RID: 1555
		// (remove) Token: 0x06000614 RID: 1556
		event EventHandler<ComposablePartCatalogChangeEventArgs> Changing;
	}
}
