﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.Runtime.CompilerServices;
using Microsoft.Internal;
using Microsoft.Internal.Collections;

namespace System.ComponentModel.Composition.Hosting
{
	/// <summary>Provides data for the <see cref="E:System.ComponentModel.Composition.Hosting.INotifyComposablePartCatalogChanged.Changed" /> event.</summary>
	// Token: 0x020000BC RID: 188
	public class ComposablePartCatalogChangeEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.ComposablePartCatalogChangeEventArgs" /> class with the specified changes.</summary>
		/// <param name="addedDefinitions">The part definitions that were added to the catalog.</param>
		/// <param name="removedDefinitions">The part definitions that were removed from the catalog.</param>
		/// <param name="atomicComposition">The composition transaction to use, or <see langword="null" /> to disable transactional composition.</param>
		// Token: 0x060004C4 RID: 1220 RVA: 0x0000D48A File Offset: 0x0000B68A
		public ComposablePartCatalogChangeEventArgs(IEnumerable<ComposablePartDefinition> addedDefinitions, IEnumerable<ComposablePartDefinition> removedDefinitions, AtomicComposition atomicComposition)
		{
			Requires.NotNull<IEnumerable<ComposablePartDefinition>>(addedDefinitions, "addedDefinitions");
			Requires.NotNull<IEnumerable<ComposablePartDefinition>>(removedDefinitions, "removedDefinitions");
			this._addedDefinitions = addedDefinitions.AsArray<ComposablePartDefinition>();
			this._removedDefinitions = removedDefinitions.AsArray<ComposablePartDefinition>();
			this.AtomicComposition = atomicComposition;
		}

		/// <summary>Gets a collection of definitions added to the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartCatalog" /> in this change.</summary>
		/// <returns>A collection of definitions added to the catalog.</returns>
		// Token: 0x17000152 RID: 338
		// (get) Token: 0x060004C5 RID: 1221 RVA: 0x0000D4C7 File Offset: 0x0000B6C7
		public IEnumerable<ComposablePartDefinition> AddedDefinitions
		{
			get
			{
				return this._addedDefinitions;
			}
		}

		/// <summary>Gets a collection of definitions removed from the <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePartCatalog" /> in this change.</summary>
		/// <returns>A collection of definitions removed from the catalog in this change.</returns>
		// Token: 0x17000153 RID: 339
		// (get) Token: 0x060004C6 RID: 1222 RVA: 0x0000D4CF File Offset: 0x0000B6CF
		public IEnumerable<ComposablePartDefinition> RemovedDefinitions
		{
			get
			{
				return this._removedDefinitions;
			}
		}

		/// <summary>Gets the composition transaction for this change.</summary>
		/// <returns>The composition transaction for this change.</returns>
		// Token: 0x17000154 RID: 340
		// (get) Token: 0x060004C7 RID: 1223 RVA: 0x0000D4D7 File Offset: 0x0000B6D7
		// (set) Token: 0x060004C8 RID: 1224 RVA: 0x0000D4DF File Offset: 0x0000B6DF
		public AtomicComposition AtomicComposition
		{
			[CompilerGenerated]
			get
			{
				return this.<AtomicComposition>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<AtomicComposition>k__BackingField = value;
			}
		}

		// Token: 0x040001DF RID: 479
		private readonly IEnumerable<ComposablePartDefinition> _addedDefinitions;

		// Token: 0x040001E0 RID: 480
		private readonly IEnumerable<ComposablePartDefinition> _removedDefinitions;

		// Token: 0x040001E1 RID: 481
		[CompilerGenerated]
		private AtomicComposition <AtomicComposition>k__BackingField;
	}
}
