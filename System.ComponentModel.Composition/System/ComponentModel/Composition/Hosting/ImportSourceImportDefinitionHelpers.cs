﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.Linq.Expressions;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.Hosting
{
	// Token: 0x020000F5 RID: 245
	internal static class ImportSourceImportDefinitionHelpers
	{
		// Token: 0x06000663 RID: 1635 RVA: 0x00013264 File Offset: 0x00011464
		public static ImportDefinition RemoveImportSource(this ImportDefinition definition)
		{
			ContractBasedImportDefinition contractBasedImportDefinition = definition as ContractBasedImportDefinition;
			if (contractBasedImportDefinition == null)
			{
				return definition;
			}
			return new ImportSourceImportDefinitionHelpers.NonImportSourceImportDefinition(contractBasedImportDefinition);
		}

		// Token: 0x020000F6 RID: 246
		internal class NonImportSourceImportDefinition : ContractBasedImportDefinition
		{
			// Token: 0x06000664 RID: 1636 RVA: 0x00013283 File Offset: 0x00011483
			public NonImportSourceImportDefinition(ContractBasedImportDefinition sourceDefinition)
			{
				Assumes.NotNull<ContractBasedImportDefinition>(sourceDefinition);
				this._sourceDefinition = sourceDefinition;
				this._metadata = null;
			}

			// Token: 0x1700017E RID: 382
			// (get) Token: 0x06000665 RID: 1637 RVA: 0x0001329F File Offset: 0x0001149F
			public override string ContractName
			{
				get
				{
					return this._sourceDefinition.ContractName;
				}
			}

			// Token: 0x1700017F RID: 383
			// (get) Token: 0x06000666 RID: 1638 RVA: 0x000132AC File Offset: 0x000114AC
			public override IDictionary<string, object> Metadata
			{
				get
				{
					IDictionary<string, object> dictionary = this._metadata;
					if (dictionary == null)
					{
						dictionary = new Dictionary<string, object>(this._sourceDefinition.Metadata);
						dictionary.Remove("System.ComponentModel.Composition.ImportSource");
						this._metadata = dictionary;
					}
					return dictionary;
				}
			}

			// Token: 0x17000180 RID: 384
			// (get) Token: 0x06000667 RID: 1639 RVA: 0x000132E8 File Offset: 0x000114E8
			public override ImportCardinality Cardinality
			{
				get
				{
					return this._sourceDefinition.Cardinality;
				}
			}

			// Token: 0x17000181 RID: 385
			// (get) Token: 0x06000668 RID: 1640 RVA: 0x000132F5 File Offset: 0x000114F5
			public override Expression<Func<ExportDefinition, bool>> Constraint
			{
				get
				{
					return this._sourceDefinition.Constraint;
				}
			}

			// Token: 0x17000182 RID: 386
			// (get) Token: 0x06000669 RID: 1641 RVA: 0x00013302 File Offset: 0x00011502
			public override bool IsPrerequisite
			{
				get
				{
					return this._sourceDefinition.IsPrerequisite;
				}
			}

			// Token: 0x17000183 RID: 387
			// (get) Token: 0x0600066A RID: 1642 RVA: 0x0001330F File Offset: 0x0001150F
			public override bool IsRecomposable
			{
				get
				{
					return this._sourceDefinition.IsRecomposable;
				}
			}

			// Token: 0x0600066B RID: 1643 RVA: 0x0001331C File Offset: 0x0001151C
			public override bool IsConstraintSatisfiedBy(ExportDefinition exportDefinition)
			{
				Requires.NotNull<ExportDefinition>(exportDefinition, "exportDefinition");
				return this._sourceDefinition.IsConstraintSatisfiedBy(exportDefinition);
			}

			// Token: 0x0600066C RID: 1644 RVA: 0x00013335 File Offset: 0x00011535
			public override string ToString()
			{
				return this._sourceDefinition.ToString();
			}

			// Token: 0x17000184 RID: 388
			// (get) Token: 0x0600066D RID: 1645 RVA: 0x00013342 File Offset: 0x00011542
			public override string RequiredTypeIdentity
			{
				get
				{
					return this._sourceDefinition.RequiredTypeIdentity;
				}
			}

			// Token: 0x17000185 RID: 389
			// (get) Token: 0x0600066E RID: 1646 RVA: 0x0001334F File Offset: 0x0001154F
			public override IEnumerable<KeyValuePair<string, Type>> RequiredMetadata
			{
				get
				{
					return this._sourceDefinition.RequiredMetadata;
				}
			}

			// Token: 0x17000186 RID: 390
			// (get) Token: 0x0600066F RID: 1647 RVA: 0x0001335C File Offset: 0x0001155C
			public override CreationPolicy RequiredCreationPolicy
			{
				get
				{
					return this._sourceDefinition.RequiredCreationPolicy;
				}
			}

			// Token: 0x040002AE RID: 686
			private ContractBasedImportDefinition _sourceDefinition;

			// Token: 0x040002AF RID: 687
			private IDictionary<string, object> _metadata;
		}
	}
}
