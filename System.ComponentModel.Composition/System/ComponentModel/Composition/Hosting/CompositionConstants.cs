﻿using System;
using System.ComponentModel.Composition.Primitives;

namespace System.ComponentModel.Composition.Hosting
{
	/// <summary>Contains static metadata keys used by the composition system.</summary>
	// Token: 0x020000C9 RID: 201
	public static class CompositionConstants
	{
		// Token: 0x06000518 RID: 1304 RVA: 0x0000EA09 File Offset: 0x0000CC09
		// Note: this type is marked as 'beforefieldinit'.
		static CompositionConstants()
		{
		}

		// Token: 0x0400020C RID: 524
		private const string CompositionNamespace = "System.ComponentModel.Composition";

		/// <summary>Specifies the metadata key created by the composition system to mark a part with a creation policy.</summary>
		// Token: 0x0400020D RID: 525
		public const string PartCreationPolicyMetadataName = "System.ComponentModel.Composition.CreationPolicy";

		/// <summary>Specifies the metadata key created by the composition system to mark an import source.</summary>
		// Token: 0x0400020E RID: 526
		public const string ImportSourceMetadataName = "System.ComponentModel.Composition.ImportSource";

		/// <summary>Specifies the metadata key created by the composition system to mark an <see langword="IsGenericPart" /> method.</summary>
		// Token: 0x0400020F RID: 527
		public const string IsGenericPartMetadataName = "System.ComponentModel.Composition.IsGenericPart";

		/// <summary>Specifies the metadata key created by the composition system to mark a generic contract.</summary>
		// Token: 0x04000210 RID: 528
		public const string GenericContractMetadataName = "System.ComponentModel.Composition.GenericContractName";

		/// <summary>Specifies the metadata key created by the composition system to mark generic parameters.</summary>
		// Token: 0x04000211 RID: 529
		public const string GenericParametersMetadataName = "System.ComponentModel.Composition.GenericParameters";

		/// <summary>Specifies the metadata key created by the composition system to mark a part with a unique identifier.</summary>
		// Token: 0x04000212 RID: 530
		public const string ExportTypeIdentityMetadataName = "ExportTypeIdentity";

		// Token: 0x04000213 RID: 531
		internal const string GenericImportParametersOrderMetadataName = "System.ComponentModel.Composition.GenericImportParametersOrderMetadataName";

		// Token: 0x04000214 RID: 532
		internal const string GenericExportParametersOrderMetadataName = "System.ComponentModel.Composition.GenericExportParametersOrderMetadataName";

		// Token: 0x04000215 RID: 533
		internal const string GenericPartArityMetadataName = "System.ComponentModel.Composition.GenericPartArity";

		// Token: 0x04000216 RID: 534
		internal const string GenericParameterConstraintsMetadataName = "System.ComponentModel.Composition.GenericParameterConstraints";

		// Token: 0x04000217 RID: 535
		internal const string GenericParameterAttributesMetadataName = "System.ComponentModel.Composition.GenericParameterAttributes";

		// Token: 0x04000218 RID: 536
		internal const string ProductDefinitionMetadataName = "ProductDefinition";

		// Token: 0x04000219 RID: 537
		internal const string PartCreatorContractName = "System.ComponentModel.Composition.Contracts.ExportFactory";

		// Token: 0x0400021A RID: 538
		internal static readonly string PartCreatorTypeIdentity = AttributedModelServices.GetTypeIdentity(typeof(ComposablePartDefinition));
	}
}
