﻿using System;
using System.Threading;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.Hosting
{
	// Token: 0x020000CC RID: 204
	internal sealed class CompositionLock : IDisposable
	{
		// Token: 0x06000533 RID: 1331 RVA: 0x0000F02C File Offset: 0x0000D22C
		public CompositionLock(bool isThreadSafe)
		{
			this._isThreadSafe = isThreadSafe;
			if (isThreadSafe)
			{
				this._stateLock = new Lock();
			}
		}

		// Token: 0x06000534 RID: 1332 RVA: 0x0000F049 File Offset: 0x0000D249
		public void Dispose()
		{
			if (this._isThreadSafe && Interlocked.CompareExchange(ref this._isDisposed, 1, 0) == 0)
			{
				this._stateLock.Dispose();
			}
		}

		// Token: 0x17000163 RID: 355
		// (get) Token: 0x06000535 RID: 1333 RVA: 0x0000F06D File Offset: 0x0000D26D
		public bool IsThreadSafe
		{
			get
			{
				return this._isThreadSafe;
			}
		}

		// Token: 0x06000536 RID: 1334 RVA: 0x0000F075 File Offset: 0x0000D275
		private void EnterCompositionLock()
		{
			if (this._isThreadSafe)
			{
				Monitor.Enter(CompositionLock._compositionLock);
			}
		}

		// Token: 0x06000537 RID: 1335 RVA: 0x0000F089 File Offset: 0x0000D289
		private void ExitCompositionLock()
		{
			if (this._isThreadSafe)
			{
				Monitor.Exit(CompositionLock._compositionLock);
			}
		}

		// Token: 0x06000538 RID: 1336 RVA: 0x0000F09D File Offset: 0x0000D29D
		public IDisposable LockComposition()
		{
			if (this._isThreadSafe)
			{
				return new CompositionLock.CompositionLockHolder(this);
			}
			return CompositionLock._EmptyLockHolder;
		}

		// Token: 0x06000539 RID: 1337 RVA: 0x0000F0B3 File Offset: 0x0000D2B3
		public IDisposable LockStateForRead()
		{
			if (this._isThreadSafe)
			{
				return new ReadLock(this._stateLock);
			}
			return CompositionLock._EmptyLockHolder;
		}

		// Token: 0x0600053A RID: 1338 RVA: 0x0000F0D3 File Offset: 0x0000D2D3
		public IDisposable LockStateForWrite()
		{
			if (this._isThreadSafe)
			{
				return new WriteLock(this._stateLock);
			}
			return CompositionLock._EmptyLockHolder;
		}

		// Token: 0x0600053B RID: 1339 RVA: 0x0000F0F3 File Offset: 0x0000D2F3
		// Note: this type is marked as 'beforefieldinit'.
		static CompositionLock()
		{
		}

		// Token: 0x04000227 RID: 551
		private readonly Lock _stateLock;

		// Token: 0x04000228 RID: 552
		private static object _compositionLock = new object();

		// Token: 0x04000229 RID: 553
		private int _isDisposed;

		// Token: 0x0400022A RID: 554
		private bool _isThreadSafe;

		// Token: 0x0400022B RID: 555
		private static readonly CompositionLock.EmptyLockHolder _EmptyLockHolder = new CompositionLock.EmptyLockHolder();

		// Token: 0x020000CD RID: 205
		public sealed class CompositionLockHolder : IDisposable
		{
			// Token: 0x0600053C RID: 1340 RVA: 0x0000F109 File Offset: 0x0000D309
			public CompositionLockHolder(CompositionLock @lock)
			{
				this._lock = @lock;
				this._isDisposed = 0;
				this._lock.EnterCompositionLock();
			}

			// Token: 0x0600053D RID: 1341 RVA: 0x0000F12A File Offset: 0x0000D32A
			public void Dispose()
			{
				if (Interlocked.CompareExchange(ref this._isDisposed, 1, 0) == 0)
				{
					this._lock.ExitCompositionLock();
				}
			}

			// Token: 0x0400022C RID: 556
			private CompositionLock _lock;

			// Token: 0x0400022D RID: 557
			private int _isDisposed;
		}

		// Token: 0x020000CE RID: 206
		private sealed class EmptyLockHolder : IDisposable
		{
			// Token: 0x0600053E RID: 1342 RVA: 0x00002304 File Offset: 0x00000504
			public void Dispose()
			{
			}

			// Token: 0x0600053F RID: 1343 RVA: 0x000025B0 File Offset: 0x000007B0
			public EmptyLockHolder()
			{
			}
		}
	}
}
