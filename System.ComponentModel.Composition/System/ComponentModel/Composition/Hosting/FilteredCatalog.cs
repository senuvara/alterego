﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Primitives;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using Microsoft.Internal;
using Microsoft.Internal.Collections;

namespace System.ComponentModel.Composition.Hosting
{
	/// <summary>Represents a catalog after a filter function is applied to it.</summary>
	// Token: 0x020000DF RID: 223
	public class FilteredCatalog : ComposablePartCatalog, INotifyComposablePartCatalogChanged
	{
		/// <summary>Gets a new <see cref="T:System.ComponentModel.Composition.Hosting.FilteredCatalog" /> object that contains all the parts from this catalog and all their dependencies.</summary>
		/// <returns>The new catalog.</returns>
		// Token: 0x060005E2 RID: 1506 RVA: 0x000111E5 File Offset: 0x0000F3E5
		public FilteredCatalog IncludeDependencies()
		{
			return this.IncludeDependencies((ImportDefinition i) => i.Cardinality == ImportCardinality.ExactlyOne);
		}

		/// <summary>Gets a new <see cref="T:System.ComponentModel.Composition.Hosting.FilteredCatalog" /> object that contains all the parts from this catalog and all dependencies that can be reached through imports that match the specified filter.</summary>
		/// <param name="importFilter">The filter for imports.</param>
		/// <returns>The new catalog.</returns>
		// Token: 0x060005E3 RID: 1507 RVA: 0x0001120C File Offset: 0x0000F40C
		public FilteredCatalog IncludeDependencies(Func<ImportDefinition, bool> importFilter)
		{
			Requires.NotNull<Func<ImportDefinition, bool>>(importFilter, "importFilter");
			this.ThrowIfDisposed();
			return this.Traverse(new FilteredCatalog.DependenciesTraversal(this, importFilter));
		}

		/// <summary>Gets a new <see cref="T:System.ComponentModel.Composition.Hosting.FilteredCatalog" /> object that contains all the parts from this catalog and all their dependents.</summary>
		/// <returns>The new catalog.</returns>
		// Token: 0x060005E4 RID: 1508 RVA: 0x0001122C File Offset: 0x0000F42C
		public FilteredCatalog IncludeDependents()
		{
			return this.IncludeDependents((ImportDefinition i) => i.Cardinality == ImportCardinality.ExactlyOne);
		}

		/// <summary>Gets a new <see cref="T:System.ComponentModel.Composition.Hosting.FilteredCatalog" /> object that contains all the parts from this catalog and all dependents that can be reached through imports that match the specified filter.</summary>
		/// <param name="importFilter">The filter for imports.</param>
		/// <returns>The new catalog.</returns>
		// Token: 0x060005E5 RID: 1509 RVA: 0x00011253 File Offset: 0x0000F453
		public FilteredCatalog IncludeDependents(Func<ImportDefinition, bool> importFilter)
		{
			Requires.NotNull<Func<ImportDefinition, bool>>(importFilter, "importFilter");
			this.ThrowIfDisposed();
			return this.Traverse(new FilteredCatalog.DependentsTraversal(this, importFilter));
		}

		// Token: 0x060005E6 RID: 1510 RVA: 0x00011274 File Offset: 0x0000F474
		private FilteredCatalog Traverse(FilteredCatalog.IComposablePartCatalogTraversal traversal)
		{
			Assumes.NotNull<FilteredCatalog.IComposablePartCatalogTraversal>(traversal);
			this.FreezeInnerCatalog();
			FilteredCatalog result;
			try
			{
				traversal.Initialize();
				HashSet<ComposablePartDefinition> traversalClosure = FilteredCatalog.GetTraversalClosure(this._innerCatalog.Where(this._filter), traversal);
				result = new FilteredCatalog(this._innerCatalog, (ComposablePartDefinition p) => traversalClosure.Contains(p));
			}
			finally
			{
				this.UnfreezeInnerCatalog();
			}
			return result;
		}

		// Token: 0x060005E7 RID: 1511 RVA: 0x000112E8 File Offset: 0x0000F4E8
		private static HashSet<ComposablePartDefinition> GetTraversalClosure(IEnumerable<ComposablePartDefinition> parts, FilteredCatalog.IComposablePartCatalogTraversal traversal)
		{
			Assumes.NotNull<FilteredCatalog.IComposablePartCatalogTraversal>(traversal);
			HashSet<ComposablePartDefinition> hashSet = new HashSet<ComposablePartDefinition>();
			FilteredCatalog.GetTraversalClosure(parts, hashSet, traversal);
			return hashSet;
		}

		// Token: 0x060005E8 RID: 1512 RVA: 0x0001130C File Offset: 0x0000F50C
		private static void GetTraversalClosure(IEnumerable<ComposablePartDefinition> parts, HashSet<ComposablePartDefinition> traversedParts, FilteredCatalog.IComposablePartCatalogTraversal traversal)
		{
			foreach (ComposablePartDefinition composablePartDefinition in parts)
			{
				if (traversedParts.Add(composablePartDefinition))
				{
					IEnumerable<ComposablePartDefinition> parts2 = null;
					if (traversal.TryTraverse(composablePartDefinition, out parts2))
					{
						FilteredCatalog.GetTraversalClosure(parts2, traversedParts, traversal);
					}
				}
			}
		}

		// Token: 0x060005E9 RID: 1513 RVA: 0x0001136C File Offset: 0x0000F56C
		private void FreezeInnerCatalog()
		{
			INotifyComposablePartCatalogChanged notifyComposablePartCatalogChanged = this._innerCatalog as INotifyComposablePartCatalogChanged;
			if (notifyComposablePartCatalogChanged != null)
			{
				notifyComposablePartCatalogChanged.Changing += FilteredCatalog.ThrowOnRecomposition;
			}
		}

		// Token: 0x060005EA RID: 1514 RVA: 0x0001139C File Offset: 0x0000F59C
		private void UnfreezeInnerCatalog()
		{
			INotifyComposablePartCatalogChanged notifyComposablePartCatalogChanged = this._innerCatalog as INotifyComposablePartCatalogChanged;
			if (notifyComposablePartCatalogChanged != null)
			{
				notifyComposablePartCatalogChanged.Changing -= FilteredCatalog.ThrowOnRecomposition;
			}
		}

		// Token: 0x060005EB RID: 1515 RVA: 0x000113CA File Offset: 0x0000F5CA
		private static void ThrowOnRecomposition(object sender, ComposablePartCatalogChangeEventArgs e)
		{
			throw new ChangeRejectedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.FilteredCatalog" /> class with the specified underlying catalog and filter.</summary>
		/// <param name="catalog">The underlying catalog.</param>
		/// <param name="filter">The function to filter parts.</param>
		// Token: 0x060005EC RID: 1516 RVA: 0x000113D1 File Offset: 0x0000F5D1
		public FilteredCatalog(ComposablePartCatalog catalog, Func<ComposablePartDefinition, bool> filter) : this(catalog, filter, null)
		{
		}

		// Token: 0x060005ED RID: 1517 RVA: 0x000113DC File Offset: 0x0000F5DC
		internal FilteredCatalog(ComposablePartCatalog catalog, Func<ComposablePartDefinition, bool> filter, FilteredCatalog complement)
		{
			Requires.NotNull<ComposablePartCatalog>(catalog, "catalog");
			Requires.NotNull<Func<ComposablePartDefinition, bool>>(filter, "filter");
			this._innerCatalog = catalog;
			this._filter = ((ComposablePartDefinition p) => filter(p.GetGenericPartDefinition() ?? p));
			this._complement = complement;
			INotifyComposablePartCatalogChanged notifyComposablePartCatalogChanged = this._innerCatalog as INotifyComposablePartCatalogChanged;
			if (notifyComposablePartCatalogChanged != null)
			{
				notifyComposablePartCatalogChanged.Changed += this.OnChangedInternal;
				notifyComposablePartCatalogChanged.Changing += this.OnChangingInternal;
			}
		}

		/// <summary>Called by the <see langword="Dispose()" /> and <see langword="Finalize()" /> methods to release the managed and unmanaged resources used by the current instance of the <see cref="T:System.ComponentModel.Composition.Hosting.FilteredCatalog" /> class.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources.</param>
		// Token: 0x060005EE RID: 1518 RVA: 0x00011478 File Offset: 0x0000F678
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && !this._isDisposed)
				{
					INotifyComposablePartCatalogChanged notifyComposablePartCatalogChanged = null;
					try
					{
						object @lock = this._lock;
						lock (@lock)
						{
							if (!this._isDisposed)
							{
								this._isDisposed = true;
								notifyComposablePartCatalogChanged = (this._innerCatalog as INotifyComposablePartCatalogChanged);
								this._innerCatalog = null;
							}
						}
					}
					finally
					{
						if (notifyComposablePartCatalogChanged != null)
						{
							notifyComposablePartCatalogChanged.Changed -= this.OnChangedInternal;
							notifyComposablePartCatalogChanged.Changing -= this.OnChangingInternal;
						}
					}
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		/// <summary>Returns an enumerator that iterates through the catalog.</summary>
		/// <returns>An enumerator that can be used to iterate through the catalog.</returns>
		// Token: 0x060005EF RID: 1519 RVA: 0x00011534 File Offset: 0x0000F734
		public override IEnumerator<ComposablePartDefinition> GetEnumerator()
		{
			return this._innerCatalog.Where(this._filter).GetEnumerator();
		}

		/// <summary>Gets a catalog that contains parts that are present in the underlying catalog but that were filtered out by the filter function.</summary>
		/// <returns>A catalog that contains the complement of this catalog.</returns>
		// Token: 0x1700017A RID: 378
		// (get) Token: 0x060005F0 RID: 1520 RVA: 0x0001154C File Offset: 0x0000F74C
		public FilteredCatalog Complement
		{
			get
			{
				this.ThrowIfDisposed();
				if (this._complement == null)
				{
					FilteredCatalog filteredCatalog = new FilteredCatalog(this._innerCatalog, (ComposablePartDefinition p) => !this._filter(p), this);
					object @lock = this._lock;
					lock (@lock)
					{
						if (this._complement == null)
						{
							Thread.MemoryBarrier();
							this._complement = filteredCatalog;
							filteredCatalog = null;
						}
					}
					if (filteredCatalog != null)
					{
						filteredCatalog.Dispose();
					}
				}
				return this._complement;
			}
		}

		/// <summary>Gets the exported parts from this catalog that match the specified import.</summary>
		/// <param name="definition">The import to match.</param>
		/// <returns>A collection of matching parts.</returns>
		// Token: 0x060005F1 RID: 1521 RVA: 0x000115D4 File Offset: 0x0000F7D4
		public override IEnumerable<Tuple<ComposablePartDefinition, ExportDefinition>> GetExports(ImportDefinition definition)
		{
			this.ThrowIfDisposed();
			Requires.NotNull<ImportDefinition>(definition, "definition");
			List<Tuple<ComposablePartDefinition, ExportDefinition>> list = new List<Tuple<ComposablePartDefinition, ExportDefinition>>();
			foreach (Tuple<ComposablePartDefinition, ExportDefinition> tuple in this._innerCatalog.GetExports(definition))
			{
				if (this._filter(tuple.Item1))
				{
					list.Add(tuple);
				}
			}
			return list;
		}

		/// <summary>Occurs when the underlying catalog has changed.</summary>
		// Token: 0x1400000B RID: 11
		// (add) Token: 0x060005F2 RID: 1522 RVA: 0x00011654 File Offset: 0x0000F854
		// (remove) Token: 0x060005F3 RID: 1523 RVA: 0x0001168C File Offset: 0x0000F88C
		public event EventHandler<ComposablePartCatalogChangeEventArgs> Changed
		{
			[CompilerGenerated]
			add
			{
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler = this.Changed;
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<ComposablePartCatalogChangeEventArgs> value2 = (EventHandler<ComposablePartCatalogChangeEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<ComposablePartCatalogChangeEventArgs>>(ref this.Changed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler = this.Changed;
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<ComposablePartCatalogChangeEventArgs> value2 = (EventHandler<ComposablePartCatalogChangeEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<ComposablePartCatalogChangeEventArgs>>(ref this.Changed, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		/// <summary>Occurs when the underlying catalog is changing.</summary>
		// Token: 0x1400000C RID: 12
		// (add) Token: 0x060005F4 RID: 1524 RVA: 0x000116C4 File Offset: 0x0000F8C4
		// (remove) Token: 0x060005F5 RID: 1525 RVA: 0x000116FC File Offset: 0x0000F8FC
		public event EventHandler<ComposablePartCatalogChangeEventArgs> Changing
		{
			[CompilerGenerated]
			add
			{
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler = this.Changing;
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<ComposablePartCatalogChangeEventArgs> value2 = (EventHandler<ComposablePartCatalogChangeEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<ComposablePartCatalogChangeEventArgs>>(ref this.Changing, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler = this.Changing;
				EventHandler<ComposablePartCatalogChangeEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<ComposablePartCatalogChangeEventArgs> value2 = (EventHandler<ComposablePartCatalogChangeEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<ComposablePartCatalogChangeEventArgs>>(ref this.Changing, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		/// <summary>Raises the <see cref="E:System.ComponentModel.Composition.Hosting.FilteredCatalog.Changed" /> event.</summary>
		/// <param name="e">Provides data for the event.</param>
		// Token: 0x060005F6 RID: 1526 RVA: 0x00011734 File Offset: 0x0000F934
		protected virtual void OnChanged(ComposablePartCatalogChangeEventArgs e)
		{
			EventHandler<ComposablePartCatalogChangeEventArgs> changed = this.Changed;
			if (changed != null)
			{
				changed(this, e);
			}
		}

		/// <summary>Raises the <see cref="E:System.ComponentModel.Composition.Hosting.FilteredCatalog.Changing" /> event.</summary>
		/// <param name="e">Provides data for the event.</param>
		// Token: 0x060005F7 RID: 1527 RVA: 0x00011754 File Offset: 0x0000F954
		protected virtual void OnChanging(ComposablePartCatalogChangeEventArgs e)
		{
			EventHandler<ComposablePartCatalogChangeEventArgs> changing = this.Changing;
			if (changing != null)
			{
				changing(this, e);
			}
		}

		// Token: 0x060005F8 RID: 1528 RVA: 0x00011774 File Offset: 0x0000F974
		private void OnChangedInternal(object sender, ComposablePartCatalogChangeEventArgs e)
		{
			ComposablePartCatalogChangeEventArgs composablePartCatalogChangeEventArgs = this.ProcessEventArgs(e);
			if (composablePartCatalogChangeEventArgs != null)
			{
				this.OnChanged(this.ProcessEventArgs(composablePartCatalogChangeEventArgs));
			}
		}

		// Token: 0x060005F9 RID: 1529 RVA: 0x0001179C File Offset: 0x0000F99C
		private void OnChangingInternal(object sender, ComposablePartCatalogChangeEventArgs e)
		{
			ComposablePartCatalogChangeEventArgs composablePartCatalogChangeEventArgs = this.ProcessEventArgs(e);
			if (composablePartCatalogChangeEventArgs != null)
			{
				this.OnChanging(this.ProcessEventArgs(composablePartCatalogChangeEventArgs));
			}
		}

		// Token: 0x060005FA RID: 1530 RVA: 0x000117C4 File Offset: 0x0000F9C4
		private ComposablePartCatalogChangeEventArgs ProcessEventArgs(ComposablePartCatalogChangeEventArgs e)
		{
			ComposablePartCatalogChangeEventArgs composablePartCatalogChangeEventArgs = new ComposablePartCatalogChangeEventArgs(e.AddedDefinitions.Where(this._filter), e.RemovedDefinitions.Where(this._filter), e.AtomicComposition);
			if (composablePartCatalogChangeEventArgs.AddedDefinitions.FastAny<ComposablePartDefinition>() || composablePartCatalogChangeEventArgs.RemovedDefinitions.FastAny<ComposablePartDefinition>())
			{
				return composablePartCatalogChangeEventArgs;
			}
			return null;
		}

		// Token: 0x060005FB RID: 1531 RVA: 0x0001181C File Offset: 0x0000FA1C
		[DebuggerStepThrough]
		private void ThrowIfDisposed()
		{
			if (this._isDisposed)
			{
				throw ExceptionBuilder.CreateObjectDisposed(this);
			}
		}

		// Token: 0x060005FC RID: 1532 RVA: 0x0001182F File Offset: 0x0000FA2F
		[CompilerGenerated]
		private bool <get_Complement>b__23_0(ComposablePartDefinition p)
		{
			return !this._filter(p);
		}

		// Token: 0x04000269 RID: 617
		private Func<ComposablePartDefinition, bool> _filter;

		// Token: 0x0400026A RID: 618
		private ComposablePartCatalog _innerCatalog;

		// Token: 0x0400026B RID: 619
		private FilteredCatalog _complement;

		// Token: 0x0400026C RID: 620
		private object _lock = new object();

		// Token: 0x0400026D RID: 621
		private volatile bool _isDisposed;

		// Token: 0x0400026E RID: 622
		[CompilerGenerated]
		private EventHandler<ComposablePartCatalogChangeEventArgs> Changed;

		// Token: 0x0400026F RID: 623
		[CompilerGenerated]
		private EventHandler<ComposablePartCatalogChangeEventArgs> Changing;

		// Token: 0x020000E0 RID: 224
		internal class DependenciesTraversal : FilteredCatalog.IComposablePartCatalogTraversal
		{
			// Token: 0x060005FD RID: 1533 RVA: 0x00011840 File Offset: 0x0000FA40
			public DependenciesTraversal(FilteredCatalog catalog, Func<ImportDefinition, bool> importFilter)
			{
				Assumes.NotNull<FilteredCatalog>(catalog);
				Assumes.NotNull<Func<ImportDefinition, bool>>(importFilter);
				this._parts = catalog._innerCatalog;
				this._importFilter = importFilter;
			}

			// Token: 0x060005FE RID: 1534 RVA: 0x00011867 File Offset: 0x0000FA67
			public void Initialize()
			{
				this.BuildExportersIndex();
			}

			// Token: 0x060005FF RID: 1535 RVA: 0x00011870 File Offset: 0x0000FA70
			private void BuildExportersIndex()
			{
				this._exportersIndex = new Dictionary<string, List<ComposablePartDefinition>>();
				foreach (ComposablePartDefinition composablePartDefinition in this._parts)
				{
					foreach (ExportDefinition exportDefinition in composablePartDefinition.ExportDefinitions)
					{
						this.AddToExportersIndex(exportDefinition.ContractName, composablePartDefinition);
					}
				}
			}

			// Token: 0x06000600 RID: 1536 RVA: 0x00011904 File Offset: 0x0000FB04
			private void AddToExportersIndex(string contractName, ComposablePartDefinition part)
			{
				List<ComposablePartDefinition> list = null;
				if (!this._exportersIndex.TryGetValue(contractName, out list))
				{
					list = new List<ComposablePartDefinition>();
					this._exportersIndex.Add(contractName, list);
				}
				list.Add(part);
			}

			// Token: 0x06000601 RID: 1537 RVA: 0x00011940 File Offset: 0x0000FB40
			public bool TryTraverse(ComposablePartDefinition part, out IEnumerable<ComposablePartDefinition> reachableParts)
			{
				reachableParts = null;
				List<ComposablePartDefinition> list = null;
				foreach (ImportDefinition import in part.ImportDefinitions.Where(this._importFilter))
				{
					List<ComposablePartDefinition> list2 = null;
					foreach (string key in import.GetCandidateContractNames(part))
					{
						if (this._exportersIndex.TryGetValue(key, out list2))
						{
							foreach (ComposablePartDefinition composablePartDefinition in list2)
							{
								foreach (ExportDefinition export in composablePartDefinition.ExportDefinitions)
								{
									if (import.IsImportDependentOnPart(composablePartDefinition, export, part.IsGeneric() != composablePartDefinition.IsGeneric()))
									{
										if (list == null)
										{
											list = new List<ComposablePartDefinition>();
										}
										list.Add(composablePartDefinition);
									}
								}
							}
						}
					}
				}
				reachableParts = list;
				return reachableParts != null;
			}

			// Token: 0x04000270 RID: 624
			private IEnumerable<ComposablePartDefinition> _parts;

			// Token: 0x04000271 RID: 625
			private Func<ImportDefinition, bool> _importFilter;

			// Token: 0x04000272 RID: 626
			private Dictionary<string, List<ComposablePartDefinition>> _exportersIndex;
		}

		// Token: 0x020000E1 RID: 225
		internal class DependentsTraversal : FilteredCatalog.IComposablePartCatalogTraversal
		{
			// Token: 0x06000602 RID: 1538 RVA: 0x00011AA0 File Offset: 0x0000FCA0
			public DependentsTraversal(FilteredCatalog catalog, Func<ImportDefinition, bool> importFilter)
			{
				Assumes.NotNull<FilteredCatalog>(catalog);
				Assumes.NotNull<Func<ImportDefinition, bool>>(importFilter);
				this._parts = catalog._innerCatalog;
				this._importFilter = importFilter;
			}

			// Token: 0x06000603 RID: 1539 RVA: 0x00011AC7 File Offset: 0x0000FCC7
			public void Initialize()
			{
				this.BuildImportersIndex();
			}

			// Token: 0x06000604 RID: 1540 RVA: 0x00011AD0 File Offset: 0x0000FCD0
			private void BuildImportersIndex()
			{
				this._importersIndex = new Dictionary<string, List<ComposablePartDefinition>>();
				foreach (ComposablePartDefinition composablePartDefinition in this._parts)
				{
					foreach (ImportDefinition import in composablePartDefinition.ImportDefinitions)
					{
						foreach (string contractName in import.GetCandidateContractNames(composablePartDefinition))
						{
							this.AddToImportersIndex(contractName, composablePartDefinition);
						}
					}
				}
			}

			// Token: 0x06000605 RID: 1541 RVA: 0x00011B94 File Offset: 0x0000FD94
			private void AddToImportersIndex(string contractName, ComposablePartDefinition part)
			{
				List<ComposablePartDefinition> list = null;
				if (!this._importersIndex.TryGetValue(contractName, out list))
				{
					list = new List<ComposablePartDefinition>();
					this._importersIndex.Add(contractName, list);
				}
				list.Add(part);
			}

			// Token: 0x06000606 RID: 1542 RVA: 0x00011BD0 File Offset: 0x0000FDD0
			public bool TryTraverse(ComposablePartDefinition part, out IEnumerable<ComposablePartDefinition> reachableParts)
			{
				reachableParts = null;
				List<ComposablePartDefinition> list = null;
				foreach (ExportDefinition exportDefinition in part.ExportDefinitions)
				{
					List<ComposablePartDefinition> list2 = null;
					if (this._importersIndex.TryGetValue(exportDefinition.ContractName, out list2))
					{
						foreach (ComposablePartDefinition composablePartDefinition in list2)
						{
							using (IEnumerator<ImportDefinition> enumerator3 = composablePartDefinition.ImportDefinitions.Where(this._importFilter).GetEnumerator())
							{
								while (enumerator3.MoveNext())
								{
									if (enumerator3.Current.IsImportDependentOnPart(part, exportDefinition, part.IsGeneric() != composablePartDefinition.IsGeneric()))
									{
										if (list == null)
										{
											list = new List<ComposablePartDefinition>();
										}
										list.Add(composablePartDefinition);
									}
								}
							}
						}
					}
				}
				reachableParts = list;
				return reachableParts != null;
			}

			// Token: 0x04000273 RID: 627
			private IEnumerable<ComposablePartDefinition> _parts;

			// Token: 0x04000274 RID: 628
			private Func<ImportDefinition, bool> _importFilter;

			// Token: 0x04000275 RID: 629
			private Dictionary<string, List<ComposablePartDefinition>> _importersIndex;
		}

		// Token: 0x020000E2 RID: 226
		internal interface IComposablePartCatalogTraversal
		{
			// Token: 0x06000607 RID: 1543
			void Initialize();

			// Token: 0x06000608 RID: 1544
			bool TryTraverse(ComposablePartDefinition part, out IEnumerable<ComposablePartDefinition> reachableParts);
		}

		// Token: 0x020000E3 RID: 227
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000609 RID: 1545 RVA: 0x00011CEC File Offset: 0x0000FEEC
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x0600060A RID: 1546 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c()
			{
			}

			// Token: 0x0600060B RID: 1547 RVA: 0x00011CF8 File Offset: 0x0000FEF8
			internal bool <IncludeDependencies>b__3_0(ImportDefinition i)
			{
				return i.Cardinality == ImportCardinality.ExactlyOne;
			}

			// Token: 0x0600060C RID: 1548 RVA: 0x00011CF8 File Offset: 0x0000FEF8
			internal bool <IncludeDependents>b__5_0(ImportDefinition i)
			{
				return i.Cardinality == ImportCardinality.ExactlyOne;
			}

			// Token: 0x04000276 RID: 630
			public static readonly FilteredCatalog.<>c <>9 = new FilteredCatalog.<>c();

			// Token: 0x04000277 RID: 631
			public static Func<ImportDefinition, bool> <>9__3_0;

			// Token: 0x04000278 RID: 632
			public static Func<ImportDefinition, bool> <>9__5_0;
		}

		// Token: 0x020000E4 RID: 228
		[CompilerGenerated]
		private sealed class <>c__DisplayClass7_0
		{
			// Token: 0x0600060D RID: 1549 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass7_0()
			{
			}

			// Token: 0x0600060E RID: 1550 RVA: 0x00011D03 File Offset: 0x0000FF03
			internal bool <Traverse>b__0(ComposablePartDefinition p)
			{
				return this.traversalClosure.Contains(p);
			}

			// Token: 0x04000279 RID: 633
			public HashSet<ComposablePartDefinition> traversalClosure;
		}

		// Token: 0x020000E5 RID: 229
		[CompilerGenerated]
		private sealed class <>c__DisplayClass19_0
		{
			// Token: 0x0600060F RID: 1551 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__DisplayClass19_0()
			{
			}

			// Token: 0x06000610 RID: 1552 RVA: 0x00011D11 File Offset: 0x0000FF11
			internal bool <.ctor>b__0(ComposablePartDefinition p)
			{
				return this.filter(p.GetGenericPartDefinition() ?? p);
			}

			// Token: 0x0400027A RID: 634
			public Func<ComposablePartDefinition, bool> filter;
		}
	}
}
