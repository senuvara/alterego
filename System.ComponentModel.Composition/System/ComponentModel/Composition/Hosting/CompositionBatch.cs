﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition.Primitives;
using System.Linq;
using Microsoft.Internal;

namespace System.ComponentModel.Composition.Hosting
{
	/// <summary>Represents a set of <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePart" /> objects which will be added or removed from the container in a single transactional composition.</summary>
	// Token: 0x020000C7 RID: 199
	public class CompositionBatch
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.CompositionBatch" /> class.</summary>
		// Token: 0x0600050B RID: 1291 RVA: 0x0000E6C6 File Offset: 0x0000C8C6
		public CompositionBatch() : this(null, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.Hosting.CompositionBatch" /> class with the specified parts for addition and removal.</summary>
		/// <param name="partsToAdd">A collection of <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePart" /> objects to add.</param>
		/// <param name="partsToRemove">A collection of <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePart" /> objects to remove.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="partsToAdd" /> is <see langword="null" />.-or-
		///         <paramref name="partsToRemove" /> is <see langword="null" />.</exception>
		// Token: 0x0600050C RID: 1292 RVA: 0x0000E6D0 File Offset: 0x0000C8D0
		public CompositionBatch(IEnumerable<ComposablePart> partsToAdd, IEnumerable<ComposablePart> partsToRemove)
		{
			this._partsToAdd = new List<ComposablePart>();
			if (partsToAdd != null)
			{
				foreach (ComposablePart composablePart in partsToAdd)
				{
					if (composablePart == null)
					{
						throw ExceptionBuilder.CreateContainsNullElement("partsToAdd");
					}
					this._partsToAdd.Add(composablePart);
				}
			}
			this._readOnlyPartsToAdd = this._partsToAdd.AsReadOnly();
			this._partsToRemove = new List<ComposablePart>();
			if (partsToRemove != null)
			{
				foreach (ComposablePart composablePart2 in partsToRemove)
				{
					if (composablePart2 == null)
					{
						throw ExceptionBuilder.CreateContainsNullElement("partsToRemove");
					}
					this._partsToRemove.Add(composablePart2);
				}
			}
			this._readOnlyPartsToRemove = this._partsToRemove.AsReadOnly();
		}

		/// <summary>Gets the collection of <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePart" /> objects to be added.</summary>
		/// <returns>A collection of parts to be added.</returns>
		// Token: 0x1700015A RID: 346
		// (get) Token: 0x0600050D RID: 1293 RVA: 0x0000E7C4 File Offset: 0x0000C9C4
		public ReadOnlyCollection<ComposablePart> PartsToAdd
		{
			get
			{
				object @lock = this._lock;
				ReadOnlyCollection<ComposablePart> readOnlyPartsToAdd;
				lock (@lock)
				{
					this._copyNeededForAdd = true;
					readOnlyPartsToAdd = this._readOnlyPartsToAdd;
				}
				return readOnlyPartsToAdd;
			}
		}

		/// <summary>Gets the collection of <see cref="T:System.ComponentModel.Composition.Primitives.ComposablePart" /> objects to be removed.</summary>
		/// <returns>A collection of parts to be removed.</returns>
		// Token: 0x1700015B RID: 347
		// (get) Token: 0x0600050E RID: 1294 RVA: 0x0000E810 File Offset: 0x0000CA10
		public ReadOnlyCollection<ComposablePart> PartsToRemove
		{
			get
			{
				object @lock = this._lock;
				ReadOnlyCollection<ComposablePart> readOnlyPartsToRemove;
				lock (@lock)
				{
					this._copyNeededForRemove = true;
					readOnlyPartsToRemove = this._readOnlyPartsToRemove;
				}
				return readOnlyPartsToRemove;
			}
		}

		/// <summary>Adds the specified part to the <see cref="T:System.ComponentModel.Composition.Hosting.CompositionBatch" /> object.</summary>
		/// <param name="part">The part to add.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="part" /> is <see langword="null" />.</exception>
		// Token: 0x0600050F RID: 1295 RVA: 0x0000E85C File Offset: 0x0000CA5C
		public void AddPart(ComposablePart part)
		{
			Requires.NotNull<ComposablePart>(part, "part");
			object @lock = this._lock;
			lock (@lock)
			{
				if (this._copyNeededForAdd)
				{
					this._partsToAdd = new List<ComposablePart>(this._partsToAdd);
					this._readOnlyPartsToAdd = this._partsToAdd.AsReadOnly();
					this._copyNeededForAdd = false;
				}
				this._partsToAdd.Add(part);
			}
		}

		/// <summary>Puts the specified part on the list of parts to remove.</summary>
		/// <param name="part">The part to be removed.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="part" /> is <see langword="null" />.</exception>
		// Token: 0x06000510 RID: 1296 RVA: 0x0000E8E0 File Offset: 0x0000CAE0
		public void RemovePart(ComposablePart part)
		{
			Requires.NotNull<ComposablePart>(part, "part");
			object @lock = this._lock;
			lock (@lock)
			{
				if (this._copyNeededForRemove)
				{
					this._partsToRemove = new List<ComposablePart>(this._partsToRemove);
					this._readOnlyPartsToRemove = this._partsToRemove.AsReadOnly();
					this._copyNeededForRemove = false;
				}
				this._partsToRemove.Add(part);
			}
		}

		/// <summary>Adds the specified export to the <see cref="T:System.ComponentModel.Composition.Hosting.CompositionBatch" /> object.</summary>
		/// <param name="export">The export to add to the <see cref="T:System.ComponentModel.Composition.Hosting.CompositionBatch" /> object.</param>
		/// <returns>The part added.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="export" /> is <see langword="null" />.</exception>
		// Token: 0x06000511 RID: 1297 RVA: 0x0000E964 File Offset: 0x0000CB64
		public ComposablePart AddExport(Export export)
		{
			Requires.NotNull<Export>(export, "export");
			ComposablePart composablePart = new CompositionBatch.SingleExportComposablePart(export);
			this.AddPart(composablePart);
			return composablePart;
		}

		// Token: 0x04000204 RID: 516
		private object _lock = new object();

		// Token: 0x04000205 RID: 517
		private bool _copyNeededForAdd;

		// Token: 0x04000206 RID: 518
		private bool _copyNeededForRemove;

		// Token: 0x04000207 RID: 519
		private List<ComposablePart> _partsToAdd;

		// Token: 0x04000208 RID: 520
		private ReadOnlyCollection<ComposablePart> _readOnlyPartsToAdd;

		// Token: 0x04000209 RID: 521
		private List<ComposablePart> _partsToRemove;

		// Token: 0x0400020A RID: 522
		private ReadOnlyCollection<ComposablePart> _readOnlyPartsToRemove;

		// Token: 0x020000C8 RID: 200
		private class SingleExportComposablePart : ComposablePart
		{
			// Token: 0x06000512 RID: 1298 RVA: 0x0000E98B File Offset: 0x0000CB8B
			public SingleExportComposablePart(Export export)
			{
				Assumes.NotNull<Export>(export);
				this._export = export;
			}

			// Token: 0x1700015C RID: 348
			// (get) Token: 0x06000513 RID: 1299 RVA: 0x00009CE1 File Offset: 0x00007EE1
			public override IDictionary<string, object> Metadata
			{
				get
				{
					return MetadataServices.EmptyMetadata;
				}
			}

			// Token: 0x1700015D RID: 349
			// (get) Token: 0x06000514 RID: 1300 RVA: 0x0000E9A0 File Offset: 0x0000CBA0
			public override IEnumerable<ExportDefinition> ExportDefinitions
			{
				get
				{
					return new ExportDefinition[]
					{
						this._export.Definition
					};
				}
			}

			// Token: 0x1700015E RID: 350
			// (get) Token: 0x06000515 RID: 1301 RVA: 0x0000CCCB File Offset: 0x0000AECB
			public override IEnumerable<ImportDefinition> ImportDefinitions
			{
				get
				{
					return Enumerable.Empty<ImportDefinition>();
				}
			}

			// Token: 0x06000516 RID: 1302 RVA: 0x0000E9B6 File Offset: 0x0000CBB6
			public override object GetExportedValue(ExportDefinition definition)
			{
				Requires.NotNull<ExportDefinition>(definition, "definition");
				if (definition != this._export.Definition)
				{
					throw ExceptionBuilder.CreateExportDefinitionNotOnThisComposablePart("definition");
				}
				return this._export.Value;
			}

			// Token: 0x06000517 RID: 1303 RVA: 0x0000E9E7 File Offset: 0x0000CBE7
			public override void SetImport(ImportDefinition definition, IEnumerable<Export> exports)
			{
				Requires.NotNull<ImportDefinition>(definition, "definition");
				Requires.NotNullOrNullElements<Export>(exports, "exports");
				throw ExceptionBuilder.CreateImportDefinitionNotOnThisComposablePart("definition");
			}

			// Token: 0x0400020B RID: 523
			private readonly Export _export;
		}
	}
}
