﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.Internal;
using Microsoft.Internal.Collections;

namespace System.ComponentModel.Composition
{
	// Token: 0x0200002C RID: 44
	internal class CompositionExceptionDebuggerProxy
	{
		// Token: 0x06000152 RID: 338 RVA: 0x000043C5 File Offset: 0x000025C5
		public CompositionExceptionDebuggerProxy(CompositionException exception)
		{
			Requires.NotNull<CompositionException>(exception, "exception");
			this._exception = exception;
		}

		// Token: 0x17000087 RID: 135
		// (get) Token: 0x06000153 RID: 339 RVA: 0x000043E0 File Offset: 0x000025E0
		public ReadOnlyCollection<Exception> Exceptions
		{
			get
			{
				List<Exception> list = new List<Exception>();
				foreach (CompositionError compositionError in this._exception.Errors)
				{
					if (compositionError.Exception != null)
					{
						list.Add(compositionError.Exception);
					}
				}
				return list.ToReadOnlyCollection<Exception>();
			}
		}

		// Token: 0x17000088 RID: 136
		// (get) Token: 0x06000154 RID: 340 RVA: 0x0000444C File Offset: 0x0000264C
		public string Message
		{
			get
			{
				return this._exception.Message;
			}
		}

		// Token: 0x17000089 RID: 137
		// (get) Token: 0x06000155 RID: 341 RVA: 0x0000445C File Offset: 0x0000265C
		public ReadOnlyCollection<Exception> RootCauses
		{
			get
			{
				List<Exception> list = new List<Exception>();
				foreach (CompositionError compositionError in this._exception.Errors)
				{
					if (compositionError.Exception != null)
					{
						CompositionException ex = compositionError.Exception as CompositionException;
						if (ex != null)
						{
							CompositionExceptionDebuggerProxy compositionExceptionDebuggerProxy = new CompositionExceptionDebuggerProxy(ex);
							if (compositionExceptionDebuggerProxy.RootCauses.Count > 0)
							{
								list.AddRange(compositionExceptionDebuggerProxy.RootCauses);
								continue;
							}
						}
						list.Add(compositionError.Exception);
					}
				}
				return list.ToReadOnlyCollection<Exception>();
			}
		}

		// Token: 0x0400007B RID: 123
		private readonly CompositionException _exception;
	}
}
