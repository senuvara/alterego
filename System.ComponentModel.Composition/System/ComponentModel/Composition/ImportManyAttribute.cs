﻿using System;
using System.ComponentModel.Composition.Primitives;
using System.Runtime.CompilerServices;

namespace System.ComponentModel.Composition
{
	/// <summary>Specifies that a property, field, or parameter should be populated with all matching exports by the <see cref="T:System.ComponentModel.Composition.Hosting.CompositionContainer" /> object.</summary>
	// Token: 0x02000046 RID: 70
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false, Inherited = false)]
	public class ImportManyAttribute : Attribute, IAttributedImport
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.ImportManyAttribute" /> class, importing the set of exports with the default contract name.</summary>
		// Token: 0x060001EC RID: 492 RVA: 0x00005868 File Offset: 0x00003A68
		public ImportManyAttribute() : this(null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.ImportManyAttribute" /> class, importing the set of exports with the contract name derived from the specified type.</summary>
		/// <param name="contractType">The type to derive the contract name of the exports to import, or <see langword="null" /> to use the default contract name.</param>
		// Token: 0x060001ED RID: 493 RVA: 0x00005871 File Offset: 0x00003A71
		public ImportManyAttribute(Type contractType) : this(null, contractType)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.ImportManyAttribute" /> class, importing the set of exports with the specified contract name.</summary>
		/// <param name="contractName">The contract name of the exports to import, or <see langword="null" /> or an empty string ("") to use the default contract name.</param>
		// Token: 0x060001EE RID: 494 RVA: 0x0000587B File Offset: 0x00003A7B
		public ImportManyAttribute(string contractName) : this(contractName, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.ImportManyAttribute" /> class, importing the set of exports with the specified contract name and contract type.</summary>
		/// <param name="contractName">The contract name of the exports to import, or <see langword="null" /> or an empty string ("") to use the default contract name.</param>
		/// <param name="contractType">The type of the export to import.</param>
		// Token: 0x060001EF RID: 495 RVA: 0x00005885 File Offset: 0x00003A85
		public ImportManyAttribute(string contractName, Type contractType)
		{
			this.ContractName = contractName;
			this.ContractType = contractType;
		}

		/// <summary>Gets the contract name of the exports to import.</summary>
		/// <returns>The contract name of the exports to import. The default value is an empty string ("").</returns>
		// Token: 0x170000A6 RID: 166
		// (get) Token: 0x060001F0 RID: 496 RVA: 0x0000589B File Offset: 0x00003A9B
		// (set) Token: 0x060001F1 RID: 497 RVA: 0x000058A3 File Offset: 0x00003AA3
		public string ContractName
		{
			[CompilerGenerated]
			get
			{
				return this.<ContractName>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ContractName>k__BackingField = value;
			}
		}

		/// <summary>Gets the contract type of the export to import.</summary>
		/// <returns>The type of the export that this import is expecting. The default value is <see langword="null" />, which means that the type will be obtained by looking at the type on the member that this import is attached to. If the type is <see cref="T:System.Object" />, the import will match any exported type.</returns>
		// Token: 0x170000A7 RID: 167
		// (get) Token: 0x060001F2 RID: 498 RVA: 0x000058AC File Offset: 0x00003AAC
		// (set) Token: 0x060001F3 RID: 499 RVA: 0x000058B4 File Offset: 0x00003AB4
		public Type ContractType
		{
			[CompilerGenerated]
			get
			{
				return this.<ContractType>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ContractType>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether the decorated property or field will be recomposed when exports that provide the matching contract change.</summary>
		/// <returns>
		///     <see langword="true" /> if the property or field allows for recomposition when exports that provide the same <see cref="P:System.ComponentModel.Composition.ImportManyAttribute.ContractName" /> are added or removed from the <see cref="T:System.ComponentModel.Composition.Hosting.CompositionContainer" />; otherwise, <see langword="false" />.The default value is <see langword="false" />.</returns>
		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x060001F4 RID: 500 RVA: 0x000058BD File Offset: 0x00003ABD
		// (set) Token: 0x060001F5 RID: 501 RVA: 0x000058C5 File Offset: 0x00003AC5
		public bool AllowRecomposition
		{
			[CompilerGenerated]
			get
			{
				return this.<AllowRecomposition>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<AllowRecomposition>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets a value that indicates that the importer requires a specific <see cref="T:System.ComponentModel.Composition.CreationPolicy" /> for the exports used to satisfy this import. </summary>
		/// <returns>One of the following values:
		///     <see cref="F:System.ComponentModel.Composition.CreationPolicy.Any" />, if the importer does not require a specific <see cref="T:System.ComponentModel.Composition.CreationPolicy" />. This is the default.
		///     <see cref="F:System.ComponentModel.Composition.CreationPolicy.Shared" /> to require that all used exports be shared by all parts in the container.
		///     <see cref="F:System.ComponentModel.Composition.CreationPolicy.NonShared" /> to require that all used exports be non-shared in a container. In this case, each part receives their own instance.</returns>
		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x060001F6 RID: 502 RVA: 0x000058CE File Offset: 0x00003ACE
		// (set) Token: 0x060001F7 RID: 503 RVA: 0x000058D6 File Offset: 0x00003AD6
		public CreationPolicy RequiredCreationPolicy
		{
			[CompilerGenerated]
			get
			{
				return this.<RequiredCreationPolicy>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<RequiredCreationPolicy>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets a value that specifies the scopes from which this import may be satisfied.</summary>
		/// <returns>A value that specifies the scopes from which this import may be satisfied.</returns>
		// Token: 0x170000AA RID: 170
		// (get) Token: 0x060001F8 RID: 504 RVA: 0x000058DF File Offset: 0x00003ADF
		// (set) Token: 0x060001F9 RID: 505 RVA: 0x000058E7 File Offset: 0x00003AE7
		public ImportSource Source
		{
			[CompilerGenerated]
			get
			{
				return this.<Source>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Source>k__BackingField = value;
			}
		}

		// Token: 0x170000AB RID: 171
		// (get) Token: 0x060001FA RID: 506 RVA: 0x000058F0 File Offset: 0x00003AF0
		ImportCardinality IAttributedImport.Cardinality
		{
			get
			{
				return ImportCardinality.ZeroOrMore;
			}
		}

		// Token: 0x040000B8 RID: 184
		[CompilerGenerated]
		private string <ContractName>k__BackingField;

		// Token: 0x040000B9 RID: 185
		[CompilerGenerated]
		private Type <ContractType>k__BackingField;

		// Token: 0x040000BA RID: 186
		[CompilerGenerated]
		private bool <AllowRecomposition>k__BackingField;

		// Token: 0x040000BB RID: 187
		[CompilerGenerated]
		private CreationPolicy <RequiredCreationPolicy>k__BackingField;

		// Token: 0x040000BC RID: 188
		[CompilerGenerated]
		private ImportSource <Source>k__BackingField;
	}
}
