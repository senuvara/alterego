﻿using System;
using System.ComponentModel.Composition.Primitives;
using Microsoft.Internal;

namespace System.ComponentModel.Composition
{
	// Token: 0x02000025 RID: 37
	internal class CompositionErrorDebuggerProxy
	{
		// Token: 0x06000134 RID: 308 RVA: 0x00003EA0 File Offset: 0x000020A0
		public CompositionErrorDebuggerProxy(CompositionError error)
		{
			Requires.NotNull<CompositionError>(error, "error");
			this._error = error;
		}

		// Token: 0x17000081 RID: 129
		// (get) Token: 0x06000135 RID: 309 RVA: 0x00003EBA File Offset: 0x000020BA
		public string Description
		{
			get
			{
				return this._error.Description;
			}
		}

		// Token: 0x17000082 RID: 130
		// (get) Token: 0x06000136 RID: 310 RVA: 0x00003EC7 File Offset: 0x000020C7
		public Exception Exception
		{
			get
			{
				return this._error.Exception;
			}
		}

		// Token: 0x17000083 RID: 131
		// (get) Token: 0x06000137 RID: 311 RVA: 0x00003ED4 File Offset: 0x000020D4
		public ICompositionElement Element
		{
			get
			{
				return this._error.Element;
			}
		}

		// Token: 0x0400005C RID: 92
		private readonly CompositionError _error;
	}
}
