﻿using System;

namespace System.ComponentModel.Composition
{
	/// <summary>Specifies which constructor should be used when creating a part.</summary>
	// Token: 0x02000048 RID: 72
	[AttributeUsage(AttributeTargets.Constructor, AllowMultiple = false, Inherited = false)]
	public class ImportingConstructorAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.ImportingConstructorAttribute" /> class.</summary>
		// Token: 0x060001FB RID: 507 RVA: 0x00003933 File Offset: 0x00001B33
		public ImportingConstructorAttribute()
		{
		}
	}
}
