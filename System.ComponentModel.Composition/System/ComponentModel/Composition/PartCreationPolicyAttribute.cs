﻿using System;
using System.Runtime.CompilerServices;

namespace System.ComponentModel.Composition
{
	/// <summary>Specifies the <see cref="P:System.ComponentModel.Composition.PartCreationPolicyAttribute.CreationPolicy" /> for a part.</summary>
	// Token: 0x0200004E RID: 78
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
	public sealed class PartCreationPolicyAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.PartCreationPolicyAttribute" /> class with the specified creation policy.</summary>
		/// <param name="creationPolicy">The creation policy to use.</param>
		// Token: 0x06000209 RID: 521 RVA: 0x00005AFB File Offset: 0x00003CFB
		public PartCreationPolicyAttribute(CreationPolicy creationPolicy)
		{
			this.CreationPolicy = creationPolicy;
		}

		/// <summary>Gets or sets a value that indicates the creation policy of the attributed part.</summary>
		/// <returns>One of the <see cref="P:System.ComponentModel.Composition.PartCreationPolicyAttribute.CreationPolicy" /> values that indicates the creation policy of the attributed part. The default is <see cref="F:System.ComponentModel.Composition.CreationPolicy.Any" />.</returns>
		// Token: 0x170000AD RID: 173
		// (get) Token: 0x0600020A RID: 522 RVA: 0x00005B0A File Offset: 0x00003D0A
		// (set) Token: 0x0600020B RID: 523 RVA: 0x00005B12 File Offset: 0x00003D12
		public CreationPolicy CreationPolicy
		{
			[CompilerGenerated]
			get
			{
				return this.<CreationPolicy>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<CreationPolicy>k__BackingField = value;
			}
		}

		// Token: 0x0600020C RID: 524 RVA: 0x00005B1B File Offset: 0x00003D1B
		// Note: this type is marked as 'beforefieldinit'.
		static PartCreationPolicyAttribute()
		{
		}

		// Token: 0x040000C3 RID: 195
		internal static PartCreationPolicyAttribute Default = new PartCreationPolicyAttribute(CreationPolicy.Any);

		// Token: 0x040000C4 RID: 196
		internal static PartCreationPolicyAttribute Shared = new PartCreationPolicyAttribute(CreationPolicy.Shared);

		// Token: 0x040000C5 RID: 197
		[CompilerGenerated]
		private CreationPolicy <CreationPolicy>k__BackingField;
	}
}
