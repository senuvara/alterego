﻿using System;
using System.ComponentModel.Composition.Primitives;
using System.Runtime.CompilerServices;

namespace System.ComponentModel.Composition
{
	/// <summary>Specifies that a property, field, or parameter value should be provided by the <see cref="T:System.ComponentModel.Composition.Hosting.CompositionContainer" />.object</summary>
	// Token: 0x02000043 RID: 67
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false, Inherited = false)]
	public class ImportAttribute : Attribute, IAttributedImport
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.ImportAttribute" /> class, importing the export with the default contract name.</summary>
		// Token: 0x060001D4 RID: 468 RVA: 0x0000577A File Offset: 0x0000397A
		public ImportAttribute() : this(null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.ImportAttribute" /> class, importing the export with the contract name derived from the specified type.</summary>
		/// <param name="contractType">The type to derive the contract name of the export from, or <see langword="null" /> to use the default contract name.</param>
		// Token: 0x060001D5 RID: 469 RVA: 0x00005783 File Offset: 0x00003983
		public ImportAttribute(Type contractType) : this(null, contractType)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.ImportAttribute" /> class, importing the export with the specified contract name.</summary>
		/// <param name="contractName">The contract name of the export to import, or <see langword="null" /> or an empty string ("") to use the default contract name.</param>
		// Token: 0x060001D6 RID: 470 RVA: 0x0000578D File Offset: 0x0000398D
		public ImportAttribute(string contractName) : this(contractName, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ComponentModel.Composition.ImportAttribute" /> class, importing the export with the specified contract name and type.</summary>
		/// <param name="contractName">The contract name of the export to import, or <see langword="null" /> or an empty string ("") to use the default contract name.</param>
		/// <param name="contractType">The type of the export to import.</param>
		// Token: 0x060001D7 RID: 471 RVA: 0x00005797 File Offset: 0x00003997
		public ImportAttribute(string contractName, Type contractType)
		{
			this.ContractName = contractName;
			this.ContractType = contractType;
		}

		/// <summary>Gets the contract name of the export to import.</summary>
		/// <returns>The contract name of the export to import. The default is an empty string ("").</returns>
		// Token: 0x1700009D RID: 157
		// (get) Token: 0x060001D8 RID: 472 RVA: 0x000057AD File Offset: 0x000039AD
		// (set) Token: 0x060001D9 RID: 473 RVA: 0x000057B5 File Offset: 0x000039B5
		public string ContractName
		{
			[CompilerGenerated]
			get
			{
				return this.<ContractName>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ContractName>k__BackingField = value;
			}
		}

		/// <summary>Gets the type of the export to import.</summary>
		/// <returns>The type of the export to import.</returns>
		// Token: 0x1700009E RID: 158
		// (get) Token: 0x060001DA RID: 474 RVA: 0x000057BE File Offset: 0x000039BE
		// (set) Token: 0x060001DB RID: 475 RVA: 0x000057C6 File Offset: 0x000039C6
		public Type ContractType
		{
			[CompilerGenerated]
			get
			{
				return this.<ContractType>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ContractType>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether the property, field, or parameter will be set to its type's default value when an export with the contract name is not present in the container.</summary>
		/// <returns>
		///     <see langword="true" /> if the property, field, or parameter will be set to its type's default value when there is no export with the <see cref="P:System.ComponentModel.Composition.ImportAttribute.ContractName" /> in the <see cref="T:System.ComponentModel.Composition.Hosting.CompositionContainer" />; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x1700009F RID: 159
		// (get) Token: 0x060001DC RID: 476 RVA: 0x000057CF File Offset: 0x000039CF
		// (set) Token: 0x060001DD RID: 477 RVA: 0x000057D7 File Offset: 0x000039D7
		public bool AllowDefault
		{
			[CompilerGenerated]
			get
			{
				return this.<AllowDefault>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<AllowDefault>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether the property or field will be recomposed when exports with a matching contract have changed in the container.</summary>
		/// <returns>
		///     <see langword="true" /> if the property or field allows recomposition when exports with a matching <see cref="P:System.ComponentModel.Composition.ImportAttribute.ContractName" /> are added or removed from the <see cref="T:System.ComponentModel.Composition.Hosting.CompositionContainer" />; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170000A0 RID: 160
		// (get) Token: 0x060001DE RID: 478 RVA: 0x000057E0 File Offset: 0x000039E0
		// (set) Token: 0x060001DF RID: 479 RVA: 0x000057E8 File Offset: 0x000039E8
		public bool AllowRecomposition
		{
			[CompilerGenerated]
			get
			{
				return this.<AllowRecomposition>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<AllowRecomposition>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets a value that indicates that the importer requires a specific <see cref="T:System.ComponentModel.Composition.CreationPolicy" /> for the exports used to satisfy this import. </summary>
		/// <returns>One of the following values:
		///     <see cref="F:System.ComponentModel.Composition.CreationPolicy.Any" />, if the importer does not require a specific <see cref="T:System.ComponentModel.Composition.CreationPolicy" />. This is the default.
		///     <see cref="F:System.ComponentModel.Composition.CreationPolicy.Shared" /> to require that all used exports be shared by all parts in the container.
		///     <see cref="F:System.ComponentModel.Composition.CreationPolicy.NonShared" /> to require that all used exports be non-shared in a container. In this case, each part receives their own instance.</returns>
		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x060001E0 RID: 480 RVA: 0x000057F1 File Offset: 0x000039F1
		// (set) Token: 0x060001E1 RID: 481 RVA: 0x000057F9 File Offset: 0x000039F9
		public CreationPolicy RequiredCreationPolicy
		{
			[CompilerGenerated]
			get
			{
				return this.<RequiredCreationPolicy>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<RequiredCreationPolicy>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets a value that specifies the scopes from which this import may be satisfied.</summary>
		/// <returns>A value that specifies the scopes from which this import may be satisfied.</returns>
		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x060001E2 RID: 482 RVA: 0x00005802 File Offset: 0x00003A02
		// (set) Token: 0x060001E3 RID: 483 RVA: 0x0000580A File Offset: 0x00003A0A
		public ImportSource Source
		{
			[CompilerGenerated]
			get
			{
				return this.<Source>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Source>k__BackingField = value;
			}
		}

		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x060001E4 RID: 484 RVA: 0x00005813 File Offset: 0x00003A13
		ImportCardinality IAttributedImport.Cardinality
		{
			get
			{
				if (this.AllowDefault)
				{
					return ImportCardinality.ZeroOrOne;
				}
				return ImportCardinality.ExactlyOne;
			}
		}

		// Token: 0x040000B1 RID: 177
		[CompilerGenerated]
		private string <ContractName>k__BackingField;

		// Token: 0x040000B2 RID: 178
		[CompilerGenerated]
		private Type <ContractType>k__BackingField;

		// Token: 0x040000B3 RID: 179
		[CompilerGenerated]
		private bool <AllowDefault>k__BackingField;

		// Token: 0x040000B4 RID: 180
		[CompilerGenerated]
		private bool <AllowRecomposition>k__BackingField;

		// Token: 0x040000B5 RID: 181
		[CompilerGenerated]
		private CreationPolicy <RequiredCreationPolicy>k__BackingField;

		// Token: 0x040000B6 RID: 182
		[CompilerGenerated]
		private ImportSource <Source>k__BackingField;
	}
}
