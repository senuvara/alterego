﻿using System;
using System.Runtime.Serialization;

namespace Microsoft.Internal.Runtime.Serialization
{
	// Token: 0x02000015 RID: 21
	internal static class SerializationServices
	{
		// Token: 0x060000C6 RID: 198 RVA: 0x0000329D File Offset: 0x0000149D
		public static T GetValue<T>(this SerializationInfo info, string name)
		{
			Assumes.NotNull<SerializationInfo, string>(info, name);
			return (T)((object)info.GetValue(name, typeof(T)));
		}
	}
}
