﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Microsoft.Internal
{
	// Token: 0x02000010 RID: 16
	internal static class Requires
	{
		// Token: 0x06000045 RID: 69 RVA: 0x000027BF File Offset: 0x000009BF
		[DebuggerStepThrough]
		public static void NotNull<T>(T value, string parameterName) where T : class
		{
			if (value == null)
			{
				throw new ArgumentNullException(parameterName);
			}
		}

		// Token: 0x06000046 RID: 70 RVA: 0x000027D0 File Offset: 0x000009D0
		[DebuggerStepThrough]
		public static void NotNullOrEmpty(string value, string parameterName)
		{
			Requires.NotNull<string>(value, parameterName);
			if (value.Length == 0)
			{
				throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, Strings.ArgumentException_EmptyString, parameterName), parameterName);
			}
		}

		// Token: 0x06000047 RID: 71 RVA: 0x000027F8 File Offset: 0x000009F8
		[DebuggerStepThrough]
		public static void NotNullOrNullElements<T>(IEnumerable<T> values, string parameterName) where T : class
		{
			Requires.NotNull<IEnumerable<T>>(values, parameterName);
			Requires.NotNullElements<T>(values, parameterName);
		}

		// Token: 0x06000048 RID: 72 RVA: 0x00002808 File Offset: 0x00000A08
		[DebuggerStepThrough]
		public static void NullOrNotNullElements<TKey, TValue>(IEnumerable<KeyValuePair<TKey, TValue>> values, string parameterName) where TKey : class where TValue : class
		{
			Requires.NotNullElements<TKey, TValue>(values, parameterName);
		}

		// Token: 0x06000049 RID: 73 RVA: 0x00002811 File Offset: 0x00000A11
		[DebuggerStepThrough]
		public static void NullOrNotNullElements<T>(IEnumerable<T> values, string parameterName) where T : class
		{
			Requires.NotNullElements<T>(values, parameterName);
		}

		// Token: 0x0600004A RID: 74 RVA: 0x0000281A File Offset: 0x00000A1A
		private static void NotNullElements<T>(IEnumerable<T> values, string parameterName) where T : class
		{
			if (values != null)
			{
				if (!Contract.ForAll<T>(values, (T value) => value != null))
				{
					throw ExceptionBuilder.CreateContainsNullElement(parameterName);
				}
			}
		}

		// Token: 0x0600004B RID: 75 RVA: 0x0000284D File Offset: 0x00000A4D
		private static void NotNullElements<TKey, TValue>(IEnumerable<KeyValuePair<TKey, TValue>> values, string parameterName) where TKey : class where TValue : class
		{
			if (values != null)
			{
				if (!Contract.ForAll<KeyValuePair<TKey, TValue>>(values, (KeyValuePair<TKey, TValue> keyValue) => keyValue.Key != null && keyValue.Value != null))
				{
					throw ExceptionBuilder.CreateContainsNullElement(parameterName);
				}
			}
		}

		// Token: 0x0600004C RID: 76 RVA: 0x00002880 File Offset: 0x00000A80
		[DebuggerStepThrough]
		public static void IsInMembertypeSet(MemberTypes value, string parameterName, MemberTypes enumFlagSet)
		{
			if ((value & enumFlagSet) != value || (value & value - 1) != (MemberTypes)0)
			{
				throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, Strings.ArgumentOutOfRange_InvalidEnumInSet, parameterName, value, enumFlagSet.ToString()), parameterName);
			}
		}

		// Token: 0x02000011 RID: 17
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c__5<T> where T : class
		{
			// Token: 0x0600004D RID: 77 RVA: 0x000028B9 File Offset: 0x00000AB9
			// Note: this type is marked as 'beforefieldinit'.
			static <>c__5()
			{
			}

			// Token: 0x0600004E RID: 78 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__5()
			{
			}

			// Token: 0x0600004F RID: 79 RVA: 0x000028C5 File Offset: 0x00000AC5
			internal bool <NotNullElements>b__5_0(T value)
			{
				return value != null;
			}

			// Token: 0x04000040 RID: 64
			public static readonly Requires.<>c__5<T> <>9 = new Requires.<>c__5<T>();

			// Token: 0x04000041 RID: 65
			public static Predicate<T> <>9__5_0;
		}

		// Token: 0x02000012 RID: 18
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c__6<TKey, TValue> where TKey : class where TValue : class
		{
			// Token: 0x06000050 RID: 80 RVA: 0x000028D0 File Offset: 0x00000AD0
			// Note: this type is marked as 'beforefieldinit'.
			static <>c__6()
			{
			}

			// Token: 0x06000051 RID: 81 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c__6()
			{
			}

			// Token: 0x06000052 RID: 82 RVA: 0x000028DC File Offset: 0x00000ADC
			internal bool <NotNullElements>b__6_0(KeyValuePair<TKey, TValue> keyValue)
			{
				return keyValue.Key != null && keyValue.Value != null;
			}

			// Token: 0x04000042 RID: 66
			public static readonly Requires.<>c__6<TKey, TValue> <>9 = new Requires.<>c__6<TKey, TValue>();

			// Token: 0x04000043 RID: 67
			public static Predicate<KeyValuePair<TKey, TValue>> <>9__6_0;
		}
	}
}
