﻿using System;
using System.Threading;

namespace Microsoft.Internal
{
	// Token: 0x02000008 RID: 8
	internal struct ReadLock : IDisposable
	{
		// Token: 0x06000013 RID: 19 RVA: 0x000021E5 File Offset: 0x000003E5
		public ReadLock(Lock @lock)
		{
			this._isDisposed = 0;
			this._lock = @lock;
			this._lock.EnterReadLock();
		}

		// Token: 0x06000014 RID: 20 RVA: 0x00002200 File Offset: 0x00000400
		public void Dispose()
		{
			if (Interlocked.CompareExchange(ref this._isDisposed, 1, 0) == 0)
			{
				this._lock.ExitReadLock();
			}
		}

		// Token: 0x0400002A RID: 42
		private readonly Lock _lock;

		// Token: 0x0400002B RID: 43
		private int _isDisposed;
	}
}
