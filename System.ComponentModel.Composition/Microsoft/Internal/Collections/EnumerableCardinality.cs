﻿using System;

namespace Microsoft.Internal.Collections
{
	// Token: 0x02000019 RID: 25
	internal enum EnumerableCardinality
	{
		// Token: 0x0400004D RID: 77
		Zero,
		// Token: 0x0400004E RID: 78
		One,
		// Token: 0x0400004F RID: 79
		TwoOrMore
	}
}
