﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Microsoft.Internal.Collections
{
	// Token: 0x02000016 RID: 22
	internal static class CollectionServices
	{
		// Token: 0x060000C7 RID: 199 RVA: 0x000032BC File Offset: 0x000014BC
		public static ICollection<object> GetCollectionWrapper(Type itemType, object collectionObject)
		{
			Assumes.NotNull<Type, object>(itemType, collectionObject);
			Type underlyingSystemType = itemType.UnderlyingSystemType;
			if (underlyingSystemType == typeof(object))
			{
				return (ICollection<object>)collectionObject;
			}
			if (typeof(IList).IsAssignableFrom(collectionObject.GetType()))
			{
				return new CollectionServices.CollectionOfObjectList((IList)collectionObject);
			}
			return (ICollection<object>)Activator.CreateInstance(typeof(CollectionServices.CollectionOfObject<>).MakeGenericType(new Type[]
			{
				underlyingSystemType
			}), new object[]
			{
				collectionObject
			});
		}

		// Token: 0x060000C8 RID: 200 RVA: 0x00003340 File Offset: 0x00001540
		public static bool IsEnumerableOfT(Type type)
		{
			return type.IsGenericType && type.GetGenericTypeDefinition().UnderlyingSystemType == CollectionServices.IEnumerableOfTType;
		}

		// Token: 0x060000C9 RID: 201 RVA: 0x00003364 File Offset: 0x00001564
		public static Type GetEnumerableElementType(Type type)
		{
			if (type.UnderlyingSystemType == CollectionServices.StringType || !CollectionServices.IEnumerableType.IsAssignableFrom(type))
			{
				return null;
			}
			Type type2;
			if (ReflectionServices.TryGetGenericInterfaceType(type, CollectionServices.IEnumerableOfTType, out type2))
			{
				return type2.GetGenericArguments()[0];
			}
			return null;
		}

		// Token: 0x060000CA RID: 202 RVA: 0x000033AC File Offset: 0x000015AC
		public static Type GetCollectionElementType(Type type)
		{
			Type type2;
			if (ReflectionServices.TryGetGenericInterfaceType(type, CollectionServices.ICollectionOfTType, out type2))
			{
				return type2.GetGenericArguments()[0];
			}
			return null;
		}

		// Token: 0x060000CB RID: 203 RVA: 0x000033D2 File Offset: 0x000015D2
		public static ReadOnlyCollection<T> ToReadOnlyCollection<T>(this IEnumerable<T> source)
		{
			Assumes.NotNull<IEnumerable<T>>(source);
			return new ReadOnlyCollection<T>(source.AsArray<T>());
		}

		// Token: 0x060000CC RID: 204 RVA: 0x000033E5 File Offset: 0x000015E5
		public static IEnumerable<T> ConcatAllowingNull<T>(this IEnumerable<T> source, IEnumerable<T> second)
		{
			if (second == null || !second.FastAny<T>())
			{
				return source;
			}
			if (source == null || !source.FastAny<T>())
			{
				return second;
			}
			return source.Concat(second);
		}

		// Token: 0x060000CD RID: 205 RVA: 0x00003408 File Offset: 0x00001608
		public static ICollection<T> ConcatAllowingNull<T>(this ICollection<T> source, ICollection<T> second)
		{
			if (second == null || second.Count == 0)
			{
				return source;
			}
			if (source == null || source.Count == 0)
			{
				return second;
			}
			List<T> list = new List<T>(source);
			list.AddRange(second);
			return list;
		}

		// Token: 0x060000CE RID: 206 RVA: 0x00003434 File Offset: 0x00001634
		public static List<T> FastAppendToListAllowNulls<T>(this List<T> source, IEnumerable<T> second)
		{
			if (second == null)
			{
				return source;
			}
			if (source == null || source.Count == 0)
			{
				return second.AsList<T>();
			}
			List<T> list = second as List<T>;
			if (list != null)
			{
				if (list.Count == 0)
				{
					return source;
				}
				if (list.Count == 1)
				{
					source.Add(list[0]);
					return source;
				}
			}
			source.AddRange(second);
			return source;
		}

		// Token: 0x060000CF RID: 207 RVA: 0x0000348C File Offset: 0x0000168C
		public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
		{
			foreach (T obj in source)
			{
				action(obj);
			}
		}

		// Token: 0x060000D0 RID: 208 RVA: 0x000034D4 File Offset: 0x000016D4
		public static EnumerableCardinality GetCardinality<T>(this IEnumerable<T> source)
		{
			Assumes.NotNull<IEnumerable<T>>(source);
			ICollection collection = source as ICollection;
			if (collection == null)
			{
				EnumerableCardinality result;
				using (IEnumerator<T> enumerator = source.GetEnumerator())
				{
					if (!enumerator.MoveNext())
					{
						result = EnumerableCardinality.Zero;
					}
					else if (!enumerator.MoveNext())
					{
						result = EnumerableCardinality.One;
					}
					else
					{
						result = EnumerableCardinality.TwoOrMore;
					}
				}
				return result;
			}
			int count = collection.Count;
			if (count == 0)
			{
				return EnumerableCardinality.Zero;
			}
			if (count != 1)
			{
				return EnumerableCardinality.TwoOrMore;
			}
			return EnumerableCardinality.One;
		}

		// Token: 0x060000D1 RID: 209 RVA: 0x00003548 File Offset: 0x00001748
		public static bool FastAny<T>(this IEnumerable<T> source)
		{
			ICollection collection = source as ICollection;
			if (collection != null)
			{
				return collection.Count > 0;
			}
			return source.Any<T>();
		}

		// Token: 0x060000D2 RID: 210 RVA: 0x0000356F File Offset: 0x0000176F
		public static Stack<T> Copy<T>(this Stack<T> stack)
		{
			Assumes.NotNull<Stack<T>>(stack);
			return new Stack<T>(stack.Reverse<T>());
		}

		// Token: 0x060000D3 RID: 211 RVA: 0x00003584 File Offset: 0x00001784
		public static T[] AsArray<T>(this IEnumerable<T> enumerable)
		{
			T[] array = enumerable as T[];
			if (array != null)
			{
				return array;
			}
			return enumerable.ToArray<T>();
		}

		// Token: 0x060000D4 RID: 212 RVA: 0x000035A4 File Offset: 0x000017A4
		public static List<T> AsList<T>(this IEnumerable<T> enumerable)
		{
			List<T> list = enumerable as List<T>;
			if (list != null)
			{
				return list;
			}
			return enumerable.ToList<T>();
		}

		// Token: 0x060000D5 RID: 213 RVA: 0x000035C4 File Offset: 0x000017C4
		public static bool IsArrayEqual<T>(this T[] thisArray, T[] thatArray)
		{
			if (thisArray.Length != thatArray.Length)
			{
				return false;
			}
			for (int i = 0; i < thisArray.Length; i++)
			{
				if (!thisArray[i].Equals(thatArray[i]))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x060000D6 RID: 214 RVA: 0x00003610 File Offset: 0x00001810
		public static bool IsCollectionEqual<T>(this IList<T> thisList, IList<T> thatList)
		{
			if (thisList.Count != thatList.Count)
			{
				return false;
			}
			for (int i = 0; i < thisList.Count; i++)
			{
				T t = thisList[i];
				if (!t.Equals(thatList[i]))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x060000D7 RID: 215 RVA: 0x00003664 File Offset: 0x00001864
		// Note: this type is marked as 'beforefieldinit'.
		static CollectionServices()
		{
		}

		// Token: 0x04000046 RID: 70
		private static readonly Type StringType = typeof(string);

		// Token: 0x04000047 RID: 71
		private static readonly Type IEnumerableType = typeof(IEnumerable);

		// Token: 0x04000048 RID: 72
		private static readonly Type IEnumerableOfTType = typeof(IEnumerable<>);

		// Token: 0x04000049 RID: 73
		private static readonly Type ICollectionOfTType = typeof(ICollection<>);

		// Token: 0x02000017 RID: 23
		private class CollectionOfObjectList : ICollection<object>, IEnumerable<object>, IEnumerable
		{
			// Token: 0x060000D8 RID: 216 RVA: 0x000036A2 File Offset: 0x000018A2
			public CollectionOfObjectList(IList list)
			{
				this._list = list;
			}

			// Token: 0x060000D9 RID: 217 RVA: 0x000036B1 File Offset: 0x000018B1
			public void Add(object item)
			{
				this._list.Add(item);
			}

			// Token: 0x060000DA RID: 218 RVA: 0x000036C0 File Offset: 0x000018C0
			public void Clear()
			{
				this._list.Clear();
			}

			// Token: 0x060000DB RID: 219 RVA: 0x000036CD File Offset: 0x000018CD
			public bool Contains(object item)
			{
				return Assumes.NotReachable<bool>();
			}

			// Token: 0x060000DC RID: 220 RVA: 0x000036D4 File Offset: 0x000018D4
			public void CopyTo(object[] array, int arrayIndex)
			{
				Assumes.NotReachable<object>();
			}

			// Token: 0x17000076 RID: 118
			// (get) Token: 0x060000DD RID: 221 RVA: 0x000036DC File Offset: 0x000018DC
			public int Count
			{
				get
				{
					return Assumes.NotReachable<int>();
				}
			}

			// Token: 0x17000077 RID: 119
			// (get) Token: 0x060000DE RID: 222 RVA: 0x000036E3 File Offset: 0x000018E3
			public bool IsReadOnly
			{
				get
				{
					return this._list.IsReadOnly;
				}
			}

			// Token: 0x060000DF RID: 223 RVA: 0x000036CD File Offset: 0x000018CD
			public bool Remove(object item)
			{
				return Assumes.NotReachable<bool>();
			}

			// Token: 0x060000E0 RID: 224 RVA: 0x000036F0 File Offset: 0x000018F0
			public IEnumerator<object> GetEnumerator()
			{
				return Assumes.NotReachable<IEnumerator<object>>();
			}

			// Token: 0x060000E1 RID: 225 RVA: 0x000036F7 File Offset: 0x000018F7
			IEnumerator IEnumerable.GetEnumerator()
			{
				return Assumes.NotReachable<IEnumerator>();
			}

			// Token: 0x0400004A RID: 74
			private readonly IList _list;
		}

		// Token: 0x02000018 RID: 24
		private class CollectionOfObject<T> : ICollection<object>, IEnumerable<object>, IEnumerable
		{
			// Token: 0x060000E2 RID: 226 RVA: 0x000036FE File Offset: 0x000018FE
			public CollectionOfObject(object collectionOfT)
			{
				this._collectionOfT = (ICollection<T>)collectionOfT;
			}

			// Token: 0x060000E3 RID: 227 RVA: 0x00003712 File Offset: 0x00001912
			public void Add(object item)
			{
				this._collectionOfT.Add((T)((object)item));
			}

			// Token: 0x060000E4 RID: 228 RVA: 0x00003725 File Offset: 0x00001925
			public void Clear()
			{
				this._collectionOfT.Clear();
			}

			// Token: 0x060000E5 RID: 229 RVA: 0x000036CD File Offset: 0x000018CD
			public bool Contains(object item)
			{
				return Assumes.NotReachable<bool>();
			}

			// Token: 0x060000E6 RID: 230 RVA: 0x000036D4 File Offset: 0x000018D4
			public void CopyTo(object[] array, int arrayIndex)
			{
				Assumes.NotReachable<object>();
			}

			// Token: 0x17000078 RID: 120
			// (get) Token: 0x060000E7 RID: 231 RVA: 0x000036DC File Offset: 0x000018DC
			public int Count
			{
				get
				{
					return Assumes.NotReachable<int>();
				}
			}

			// Token: 0x17000079 RID: 121
			// (get) Token: 0x060000E8 RID: 232 RVA: 0x00003732 File Offset: 0x00001932
			public bool IsReadOnly
			{
				get
				{
					return this._collectionOfT.IsReadOnly;
				}
			}

			// Token: 0x060000E9 RID: 233 RVA: 0x000036CD File Offset: 0x000018CD
			public bool Remove(object item)
			{
				return Assumes.NotReachable<bool>();
			}

			// Token: 0x060000EA RID: 234 RVA: 0x000036F0 File Offset: 0x000018F0
			public IEnumerator<object> GetEnumerator()
			{
				return Assumes.NotReachable<IEnumerator<object>>();
			}

			// Token: 0x060000EB RID: 235 RVA: 0x000036F7 File Offset: 0x000018F7
			IEnumerator IEnumerable.GetEnumerator()
			{
				return Assumes.NotReachable<IEnumerator>();
			}

			// Token: 0x0400004B RID: 75
			private readonly ICollection<T> _collectionOfT;
		}
	}
}
