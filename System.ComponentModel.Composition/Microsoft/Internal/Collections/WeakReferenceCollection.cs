﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Microsoft.Internal.Collections
{
	// Token: 0x0200001A RID: 26
	internal class WeakReferenceCollection<T> where T : class
	{
		// Token: 0x060000EC RID: 236 RVA: 0x0000373F File Offset: 0x0000193F
		public void Add(T item)
		{
			if (this._items.Capacity == this._items.Count)
			{
				this.CleanupDeadReferences();
			}
			this._items.Add(new WeakReference(item));
		}

		// Token: 0x060000ED RID: 237 RVA: 0x00003778 File Offset: 0x00001978
		public void Remove(T item)
		{
			int num = this.IndexOf(item);
			if (num != -1)
			{
				this._items.RemoveAt(num);
			}
		}

		// Token: 0x060000EE RID: 238 RVA: 0x0000379D File Offset: 0x0000199D
		public bool Contains(T item)
		{
			return this.IndexOf(item) >= 0;
		}

		// Token: 0x060000EF RID: 239 RVA: 0x000037AC File Offset: 0x000019AC
		public void Clear()
		{
			this._items.Clear();
		}

		// Token: 0x060000F0 RID: 240 RVA: 0x000037BC File Offset: 0x000019BC
		private int IndexOf(T item)
		{
			int count = this._items.Count;
			for (int i = 0; i < count; i++)
			{
				if (this._items[i].Target == item)
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x060000F1 RID: 241 RVA: 0x000037FD File Offset: 0x000019FD
		private void CleanupDeadReferences()
		{
			this._items.RemoveAll((WeakReference w) => !w.IsAlive);
		}

		// Token: 0x060000F2 RID: 242 RVA: 0x0000382C File Offset: 0x00001A2C
		public List<T> AliveItemsToList()
		{
			List<T> list = new List<T>();
			foreach (WeakReference weakReference in this._items)
			{
				T t = weakReference.Target as T;
				if (t != null)
				{
					list.Add(t);
				}
			}
			return list;
		}

		// Token: 0x060000F3 RID: 243 RVA: 0x000038A0 File Offset: 0x00001AA0
		public WeakReferenceCollection()
		{
		}

		// Token: 0x04000050 RID: 80
		private readonly List<WeakReference> _items = new List<WeakReference>();

		// Token: 0x0200001B RID: 27
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x060000F4 RID: 244 RVA: 0x000038B3 File Offset: 0x00001AB3
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x060000F5 RID: 245 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c()
			{
			}

			// Token: 0x060000F6 RID: 246 RVA: 0x000038BF File Offset: 0x00001ABF
			internal bool <CleanupDeadReferences>b__6_0(WeakReference w)
			{
				return !w.IsAlive;
			}

			// Token: 0x04000051 RID: 81
			public static readonly WeakReferenceCollection<T>.<>c <>9 = new WeakReferenceCollection<T>.<>c();

			// Token: 0x04000052 RID: 82
			public static Predicate<WeakReference> <>9__6_0;
		}
	}
}
