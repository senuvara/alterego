﻿using System;

namespace Microsoft.Internal
{
	// Token: 0x02000013 RID: 19
	internal static class StringComparers
	{
		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000053 RID: 83 RVA: 0x000028FD File Offset: 0x00000AFD
		public static StringComparer ContractName
		{
			get
			{
				return StringComparer.Ordinal;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000054 RID: 84 RVA: 0x000028FD File Offset: 0x00000AFD
		public static StringComparer MetadataKeyNames
		{
			get
			{
				return StringComparer.Ordinal;
			}
		}
	}
}
