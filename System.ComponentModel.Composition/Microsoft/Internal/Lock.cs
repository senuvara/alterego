﻿using System;
using System.Threading;

namespace Microsoft.Internal
{
	// Token: 0x0200000A RID: 10
	internal sealed class Lock : IDisposable
	{
		// Token: 0x06000017 RID: 23 RVA: 0x00002253 File Offset: 0x00000453
		public void EnterReadLock()
		{
			this._thisLock.EnterReadLock();
		}

		// Token: 0x06000018 RID: 24 RVA: 0x00002260 File Offset: 0x00000460
		public void EnterWriteLock()
		{
			this._thisLock.EnterWriteLock();
		}

		// Token: 0x06000019 RID: 25 RVA: 0x0000226D File Offset: 0x0000046D
		public void ExitReadLock()
		{
			this._thisLock.ExitReadLock();
		}

		// Token: 0x0600001A RID: 26 RVA: 0x0000227A File Offset: 0x0000047A
		public void ExitWriteLock()
		{
			this._thisLock.ExitWriteLock();
		}

		// Token: 0x0600001B RID: 27 RVA: 0x00002287 File Offset: 0x00000487
		public void Dispose()
		{
			if (Interlocked.CompareExchange(ref this._isDisposed, 1, 0) == 0)
			{
				this._thisLock.Dispose();
			}
		}

		// Token: 0x0600001C RID: 28 RVA: 0x000022A3 File Offset: 0x000004A3
		public Lock()
		{
		}

		// Token: 0x0400002E RID: 46
		private ReaderWriterLockSlim _thisLock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);

		// Token: 0x0400002F RID: 47
		private int _isDisposed;
	}
}
