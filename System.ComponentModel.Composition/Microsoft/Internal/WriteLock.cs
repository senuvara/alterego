﻿using System;
using System.Threading;

namespace Microsoft.Internal
{
	// Token: 0x02000009 RID: 9
	internal struct WriteLock : IDisposable
	{
		// Token: 0x06000015 RID: 21 RVA: 0x0000221C File Offset: 0x0000041C
		public WriteLock(Lock @lock)
		{
			this._isDisposed = 0;
			this._lock = @lock;
			this._lock.EnterWriteLock();
		}

		// Token: 0x06000016 RID: 22 RVA: 0x00002237 File Offset: 0x00000437
		public void Dispose()
		{
			if (Interlocked.CompareExchange(ref this._isDisposed, 1, 0) == 0)
			{
				this._lock.ExitWriteLock();
			}
		}

		// Token: 0x0400002C RID: 44
		private readonly Lock _lock;

		// Token: 0x0400002D RID: 45
		private int _isDisposed;
	}
}
