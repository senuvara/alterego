﻿using System;
using System.Globalization;

namespace Microsoft.Internal
{
	// Token: 0x02000007 RID: 7
	internal static class LazyServices
	{
		// Token: 0x06000012 RID: 18 RVA: 0x000021AF File Offset: 0x000003AF
		public static T GetNotNullValue<T>(this Lazy<T> lazy, string argument) where T : class
		{
			Assumes.NotNull<Lazy<T>>(lazy);
			T value = lazy.Value;
			if (value == null)
			{
				throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, Strings.LazyServices_LazyResolvesToNull, typeof(T), argument));
			}
			return value;
		}
	}
}
