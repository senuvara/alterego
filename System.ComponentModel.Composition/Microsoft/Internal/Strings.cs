﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace Microsoft.Internal
{
	// Token: 0x02000014 RID: 20
	[DebuggerNonUserCode]
	[CompilerGenerated]
	[GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
	internal class Strings
	{
		// Token: 0x06000055 RID: 85 RVA: 0x000025B0 File Offset: 0x000007B0
		internal Strings()
		{
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000056 RID: 86 RVA: 0x00002904 File Offset: 0x00000B04
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static ResourceManager ResourceManager
		{
			get
			{
				if (Strings.resourceMan == null)
				{
					Strings.resourceMan = new ResourceManager("Microsoft.Internal.Strings", typeof(Strings).Assembly);
				}
				return Strings.resourceMan;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000057 RID: 87 RVA: 0x00002930 File Offset: 0x00000B30
		// (set) Token: 0x06000058 RID: 88 RVA: 0x00002937 File Offset: 0x00000B37
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static CultureInfo Culture
		{
			get
			{
				return Strings.resourceCulture;
			}
			set
			{
				Strings.resourceCulture = value;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000059 RID: 89 RVA: 0x0000293F File Offset: 0x00000B3F
		internal static string Argument_AssemblyReflectionOnly
		{
			get
			{
				return Strings.ResourceManager.GetString("Argument_AssemblyReflectionOnly", Strings.resourceCulture);
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x0600005A RID: 90 RVA: 0x00002955 File Offset: 0x00000B55
		internal static string Argument_ElementReflectionOnlyType
		{
			get
			{
				return Strings.ResourceManager.GetString("Argument_ElementReflectionOnlyType", Strings.resourceCulture);
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x0600005B RID: 91 RVA: 0x0000296B File Offset: 0x00000B6B
		internal static string Argument_ExportsEmpty
		{
			get
			{
				return Strings.ResourceManager.GetString("Argument_ExportsEmpty", Strings.resourceCulture);
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x0600005C RID: 92 RVA: 0x00002981 File Offset: 0x00000B81
		internal static string Argument_ExportsTooMany
		{
			get
			{
				return Strings.ResourceManager.GetString("Argument_ExportsTooMany", Strings.resourceCulture);
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600005D RID: 93 RVA: 0x00002997 File Offset: 0x00000B97
		internal static string Argument_NullElement
		{
			get
			{
				return Strings.ResourceManager.GetString("Argument_NullElement", Strings.resourceCulture);
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x0600005E RID: 94 RVA: 0x000029AD File Offset: 0x00000BAD
		internal static string Argument_ReflectionContextReturnsReflectionOnlyType
		{
			get
			{
				return Strings.ResourceManager.GetString("Argument_ReflectionContextReturnsReflectionOnlyType", Strings.resourceCulture);
			}
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x0600005F RID: 95 RVA: 0x000029C3 File Offset: 0x00000BC3
		internal static string ArgumentException_EmptyString
		{
			get
			{
				return Strings.ResourceManager.GetString("ArgumentException_EmptyString", Strings.resourceCulture);
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000060 RID: 96 RVA: 0x000029D9 File Offset: 0x00000BD9
		internal static string ArgumentOutOfRange_InvalidEnum
		{
			get
			{
				return Strings.ResourceManager.GetString("ArgumentOutOfRange_InvalidEnum", Strings.resourceCulture);
			}
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000061 RID: 97 RVA: 0x000029EF File Offset: 0x00000BEF
		internal static string ArgumentOutOfRange_InvalidEnumInSet
		{
			get
			{
				return Strings.ResourceManager.GetString("ArgumentOutOfRange_InvalidEnumInSet", Strings.resourceCulture);
			}
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000062 RID: 98 RVA: 0x00002A05 File Offset: 0x00000C05
		internal static string ArgumentValueType
		{
			get
			{
				return Strings.ResourceManager.GetString("ArgumentValueType", Strings.resourceCulture);
			}
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000063 RID: 99 RVA: 0x00002A1B File Offset: 0x00000C1B
		internal static string AssemblyFileNotFoundOrWrongType
		{
			get
			{
				return Strings.ResourceManager.GetString("AssemblyFileNotFoundOrWrongType", Strings.resourceCulture);
			}
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000064 RID: 100 RVA: 0x00002A31 File Offset: 0x00000C31
		internal static string AtomicComposition_AlreadyCompleted
		{
			get
			{
				return Strings.ResourceManager.GetString("AtomicComposition_AlreadyCompleted", Strings.resourceCulture);
			}
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000065 RID: 101 RVA: 0x00002A47 File Offset: 0x00000C47
		internal static string AtomicComposition_AlreadyNested
		{
			get
			{
				return Strings.ResourceManager.GetString("AtomicComposition_AlreadyNested", Strings.resourceCulture);
			}
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000066 RID: 102 RVA: 0x00002A5D File Offset: 0x00000C5D
		internal static string AtomicComposition_PartOfAnotherAtomicComposition
		{
			get
			{
				return Strings.ResourceManager.GetString("AtomicComposition_PartOfAnotherAtomicComposition", Strings.resourceCulture);
			}
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000067 RID: 103 RVA: 0x00002A73 File Offset: 0x00000C73
		internal static string CardinalityMismatch_NoExports
		{
			get
			{
				return Strings.ResourceManager.GetString("CardinalityMismatch_NoExports", Strings.resourceCulture);
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000068 RID: 104 RVA: 0x00002A89 File Offset: 0x00000C89
		internal static string CardinalityMismatch_TooManyExports
		{
			get
			{
				return Strings.ResourceManager.GetString("CardinalityMismatch_TooManyExports", Strings.resourceCulture);
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000069 RID: 105 RVA: 0x00002A9F File Offset: 0x00000C9F
		internal static string CatalogMutation_Invalid
		{
			get
			{
				return Strings.ResourceManager.GetString("CatalogMutation_Invalid", Strings.resourceCulture);
			}
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x0600006A RID: 106 RVA: 0x00002AB5 File Offset: 0x00000CB5
		internal static string CompositionElement_UnknownOrigin
		{
			get
			{
				return Strings.ResourceManager.GetString("CompositionElement_UnknownOrigin", Strings.resourceCulture);
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x0600006B RID: 107 RVA: 0x00002ACB File Offset: 0x00000CCB
		internal static string CompositionException_ChangesRejected
		{
			get
			{
				return Strings.ResourceManager.GetString("CompositionException_ChangesRejected", Strings.resourceCulture);
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x0600006C RID: 108 RVA: 0x00002AE1 File Offset: 0x00000CE1
		internal static string CompositionException_ElementPrefix
		{
			get
			{
				return Strings.ResourceManager.GetString("CompositionException_ElementPrefix", Strings.resourceCulture);
			}
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x0600006D RID: 109 RVA: 0x00002AF7 File Offset: 0x00000CF7
		internal static string CompositionException_ErrorPrefix
		{
			get
			{
				return Strings.ResourceManager.GetString("CompositionException_ErrorPrefix", Strings.resourceCulture);
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x0600006E RID: 110 RVA: 0x00002B0D File Offset: 0x00000D0D
		internal static string CompositionException_MetadataViewInvalidConstructor
		{
			get
			{
				return Strings.ResourceManager.GetString("CompositionException_MetadataViewInvalidConstructor", Strings.resourceCulture);
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x0600006F RID: 111 RVA: 0x00002B23 File Offset: 0x00000D23
		internal static string CompositionException_MultipleErrorsWithMultiplePaths
		{
			get
			{
				return Strings.ResourceManager.GetString("CompositionException_MultipleErrorsWithMultiplePaths", Strings.resourceCulture);
			}
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x06000070 RID: 112 RVA: 0x00002B39 File Offset: 0x00000D39
		internal static string CompositionException_OriginFormat
		{
			get
			{
				return Strings.ResourceManager.GetString("CompositionException_OriginFormat", Strings.resourceCulture);
			}
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x06000071 RID: 113 RVA: 0x00002B4F File Offset: 0x00000D4F
		internal static string CompositionException_OriginSeparator
		{
			get
			{
				return Strings.ResourceManager.GetString("CompositionException_OriginSeparator", Strings.resourceCulture);
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000072 RID: 114 RVA: 0x00002B65 File Offset: 0x00000D65
		internal static string CompositionException_PathsCountSeparator
		{
			get
			{
				return Strings.ResourceManager.GetString("CompositionException_PathsCountSeparator", Strings.resourceCulture);
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x06000073 RID: 115 RVA: 0x00002B7B File Offset: 0x00000D7B
		internal static string CompositionException_ReviewErrorProperty
		{
			get
			{
				return Strings.ResourceManager.GetString("CompositionException_ReviewErrorProperty", Strings.resourceCulture);
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x06000074 RID: 116 RVA: 0x00002B91 File Offset: 0x00000D91
		internal static string CompositionException_SingleErrorWithMultiplePaths
		{
			get
			{
				return Strings.ResourceManager.GetString("CompositionException_SingleErrorWithMultiplePaths", Strings.resourceCulture);
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x06000075 RID: 117 RVA: 0x00002BA7 File Offset: 0x00000DA7
		internal static string CompositionException_SingleErrorWithSinglePath
		{
			get
			{
				return Strings.ResourceManager.GetString("CompositionException_SingleErrorWithSinglePath", Strings.resourceCulture);
			}
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000076 RID: 118 RVA: 0x00002BBD File Offset: 0x00000DBD
		internal static string CompositionTrace_Discovery_AssemblyLoadFailed
		{
			get
			{
				return Strings.ResourceManager.GetString("CompositionTrace_Discovery_AssemblyLoadFailed", Strings.resourceCulture);
			}
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000077 RID: 119 RVA: 0x00002BD3 File Offset: 0x00000DD3
		internal static string CompositionTrace_Discovery_DefinitionContainsNoExports
		{
			get
			{
				return Strings.ResourceManager.GetString("CompositionTrace_Discovery_DefinitionContainsNoExports", Strings.resourceCulture);
			}
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x06000078 RID: 120 RVA: 0x00002BE9 File Offset: 0x00000DE9
		internal static string CompositionTrace_Discovery_DefinitionMarkedWithPartNotDiscoverableAttribute
		{
			get
			{
				return Strings.ResourceManager.GetString("CompositionTrace_Discovery_DefinitionMarkedWithPartNotDiscoverableAttribute", Strings.resourceCulture);
			}
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x06000079 RID: 121 RVA: 0x00002BFF File Offset: 0x00000DFF
		internal static string CompositionTrace_Discovery_DefinitionMismatchedExportArity
		{
			get
			{
				return Strings.ResourceManager.GetString("CompositionTrace_Discovery_DefinitionMismatchedExportArity", Strings.resourceCulture);
			}
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x0600007A RID: 122 RVA: 0x00002C15 File Offset: 0x00000E15
		internal static string CompositionTrace_Discovery_MemberMarkedWithMultipleImportAndImportMany
		{
			get
			{
				return Strings.ResourceManager.GetString("CompositionTrace_Discovery_MemberMarkedWithMultipleImportAndImportMany", Strings.resourceCulture);
			}
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x0600007B RID: 123 RVA: 0x00002C2B File Offset: 0x00000E2B
		internal static string CompositionTrace_Rejection_DefinitionRejected
		{
			get
			{
				return Strings.ResourceManager.GetString("CompositionTrace_Rejection_DefinitionRejected", Strings.resourceCulture);
			}
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x0600007C RID: 124 RVA: 0x00002C41 File Offset: 0x00000E41
		internal static string CompositionTrace_Rejection_DefinitionResurrected
		{
			get
			{
				return Strings.ResourceManager.GetString("CompositionTrace_Rejection_DefinitionResurrected", Strings.resourceCulture);
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x0600007D RID: 125 RVA: 0x00002C57 File Offset: 0x00000E57
		internal static string ContractMismatch_ExportedValueCannotBeCastToT
		{
			get
			{
				return Strings.ResourceManager.GetString("ContractMismatch_ExportedValueCannotBeCastToT", Strings.resourceCulture);
			}
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x0600007E RID: 126 RVA: 0x00002C6D File Offset: 0x00000E6D
		internal static string ContractMismatch_InvalidCastOnMetadataField
		{
			get
			{
				return Strings.ResourceManager.GetString("ContractMismatch_InvalidCastOnMetadataField", Strings.resourceCulture);
			}
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x0600007F RID: 127 RVA: 0x00002C83 File Offset: 0x00000E83
		internal static string ContractMismatch_MetadataViewImplementationCanNotBeNull
		{
			get
			{
				return Strings.ResourceManager.GetString("ContractMismatch_MetadataViewImplementationCanNotBeNull", Strings.resourceCulture);
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x06000080 RID: 128 RVA: 0x00002C99 File Offset: 0x00000E99
		internal static string ContractMismatch_MetadataViewImplementationDoesNotImplementViewInterface
		{
			get
			{
				return Strings.ResourceManager.GetString("ContractMismatch_MetadataViewImplementationDoesNotImplementViewInterface", Strings.resourceCulture);
			}
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x06000081 RID: 129 RVA: 0x00002CAF File Offset: 0x00000EAF
		internal static string ContractMismatch_NullReferenceOnMetadataField
		{
			get
			{
				return Strings.ResourceManager.GetString("ContractMismatch_NullReferenceOnMetadataField", Strings.resourceCulture);
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x06000082 RID: 130 RVA: 0x00002CC5 File Offset: 0x00000EC5
		internal static string DirectoryNotFound
		{
			get
			{
				return Strings.ResourceManager.GetString("DirectoryNotFound", Strings.resourceCulture);
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x06000083 RID: 131 RVA: 0x00002CDB File Offset: 0x00000EDB
		internal static string Discovery_DuplicateMetadataNameValues
		{
			get
			{
				return Strings.ResourceManager.GetString("Discovery_DuplicateMetadataNameValues", Strings.resourceCulture);
			}
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x06000084 RID: 132 RVA: 0x00002CF1 File Offset: 0x00000EF1
		internal static string Discovery_MetadataContainsValueWithInvalidType
		{
			get
			{
				return Strings.ResourceManager.GetString("Discovery_MetadataContainsValueWithInvalidType", Strings.resourceCulture);
			}
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x06000085 RID: 133 RVA: 0x00002D07 File Offset: 0x00000F07
		internal static string Discovery_ReservedMetadataNameUsed
		{
			get
			{
				return Strings.ResourceManager.GetString("Discovery_ReservedMetadataNameUsed", Strings.resourceCulture);
			}
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x06000086 RID: 134 RVA: 0x00002D1D File Offset: 0x00000F1D
		internal static string ExportDefinitionNotOnThisComposablePart
		{
			get
			{
				return Strings.ResourceManager.GetString("ExportDefinitionNotOnThisComposablePart", Strings.resourceCulture);
			}
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x06000087 RID: 135 RVA: 0x00002D33 File Offset: 0x00000F33
		internal static string ExportFactory_TooManyGenericParameters
		{
			get
			{
				return Strings.ResourceManager.GetString("ExportFactory_TooManyGenericParameters", Strings.resourceCulture);
			}
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x06000088 RID: 136 RVA: 0x00002D49 File Offset: 0x00000F49
		internal static string ExportNotValidOnIndexers
		{
			get
			{
				return Strings.ResourceManager.GetString("ExportNotValidOnIndexers", Strings.resourceCulture);
			}
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x06000089 RID: 137 RVA: 0x00002D5F File Offset: 0x00000F5F
		internal static string ImportDefinitionNotOnThisComposablePart
		{
			get
			{
				return Strings.ResourceManager.GetString("ImportDefinitionNotOnThisComposablePart", Strings.resourceCulture);
			}
		}

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x0600008A RID: 138 RVA: 0x00002D75 File Offset: 0x00000F75
		internal static string ImportEngine_ComposeTookTooManyIterations
		{
			get
			{
				return Strings.ResourceManager.GetString("ImportEngine_ComposeTookTooManyIterations", Strings.resourceCulture);
			}
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x0600008B RID: 139 RVA: 0x00002D8B File Offset: 0x00000F8B
		internal static string ImportEngine_InvalidStateForRecomposition
		{
			get
			{
				return Strings.ResourceManager.GetString("ImportEngine_InvalidStateForRecomposition", Strings.resourceCulture);
			}
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x0600008C RID: 140 RVA: 0x00002DA1 File Offset: 0x00000FA1
		internal static string ImportEngine_PartCannotActivate
		{
			get
			{
				return Strings.ResourceManager.GetString("ImportEngine_PartCannotActivate", Strings.resourceCulture);
			}
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x0600008D RID: 141 RVA: 0x00002DB7 File Offset: 0x00000FB7
		internal static string ImportEngine_PartCannotGetExportedValue
		{
			get
			{
				return Strings.ResourceManager.GetString("ImportEngine_PartCannotGetExportedValue", Strings.resourceCulture);
			}
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x0600008E RID: 142 RVA: 0x00002DCD File Offset: 0x00000FCD
		internal static string ImportEngine_PartCannotSetImport
		{
			get
			{
				return Strings.ResourceManager.GetString("ImportEngine_PartCannotSetImport", Strings.resourceCulture);
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x0600008F RID: 143 RVA: 0x00002DE3 File Offset: 0x00000FE3
		internal static string ImportEngine_PartCycle
		{
			get
			{
				return Strings.ResourceManager.GetString("ImportEngine_PartCycle", Strings.resourceCulture);
			}
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x06000090 RID: 144 RVA: 0x00002DF9 File Offset: 0x00000FF9
		internal static string ImportEngine_PreventedByExistingImport
		{
			get
			{
				return Strings.ResourceManager.GetString("ImportEngine_PreventedByExistingImport", Strings.resourceCulture);
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x06000091 RID: 145 RVA: 0x00002E0F File Offset: 0x0000100F
		internal static string ImportNotSetOnPart
		{
			get
			{
				return Strings.ResourceManager.GetString("ImportNotSetOnPart", Strings.resourceCulture);
			}
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x06000092 RID: 146 RVA: 0x00002E25 File Offset: 0x00001025
		internal static string ImportNotValidOnIndexers
		{
			get
			{
				return Strings.ResourceManager.GetString("ImportNotValidOnIndexers", Strings.resourceCulture);
			}
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x06000093 RID: 147 RVA: 0x00002E3B File Offset: 0x0000103B
		internal static string InternalExceptionMessage
		{
			get
			{
				return Strings.ResourceManager.GetString("InternalExceptionMessage", Strings.resourceCulture);
			}
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x06000094 RID: 148 RVA: 0x00002E51 File Offset: 0x00001051
		internal static string InvalidArgument_ReflectionContext
		{
			get
			{
				return Strings.ResourceManager.GetString("InvalidArgument_ReflectionContext", Strings.resourceCulture);
			}
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x06000095 RID: 149 RVA: 0x00002E67 File Offset: 0x00001067
		internal static string InvalidMetadataValue
		{
			get
			{
				return Strings.ResourceManager.GetString("InvalidMetadataValue", Strings.resourceCulture);
			}
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x06000096 RID: 150 RVA: 0x00002E7D File Offset: 0x0000107D
		internal static string InvalidMetadataView
		{
			get
			{
				return Strings.ResourceManager.GetString("InvalidMetadataView", Strings.resourceCulture);
			}
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x06000097 RID: 151 RVA: 0x00002E93 File Offset: 0x00001093
		internal static string InvalidOperation_DefinitionCannotBeRecomposed
		{
			get
			{
				return Strings.ResourceManager.GetString("InvalidOperation_DefinitionCannotBeRecomposed", Strings.resourceCulture);
			}
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x06000098 RID: 152 RVA: 0x00002EA9 File Offset: 0x000010A9
		internal static string InvalidOperation_GetExportedValueBeforePrereqImportSet
		{
			get
			{
				return Strings.ResourceManager.GetString("InvalidOperation_GetExportedValueBeforePrereqImportSet", Strings.resourceCulture);
			}
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x06000099 RID: 153 RVA: 0x00002EBF File Offset: 0x000010BF
		internal static string InvalidOperationReentrantCompose
		{
			get
			{
				return Strings.ResourceManager.GetString("InvalidOperationReentrantCompose", Strings.resourceCulture);
			}
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x0600009A RID: 154 RVA: 0x00002ED5 File Offset: 0x000010D5
		internal static string InvalidPartCreationPolicyOnImport
		{
			get
			{
				return Strings.ResourceManager.GetString("InvalidPartCreationPolicyOnImport", Strings.resourceCulture);
			}
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x0600009B RID: 155 RVA: 0x00002EEB File Offset: 0x000010EB
		internal static string InvalidPartCreationPolicyOnPart
		{
			get
			{
				return Strings.ResourceManager.GetString("InvalidPartCreationPolicyOnPart", Strings.resourceCulture);
			}
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x0600009C RID: 156 RVA: 0x00002F01 File Offset: 0x00001101
		internal static string InvalidSetterOnMetadataField
		{
			get
			{
				return Strings.ResourceManager.GetString("InvalidSetterOnMetadataField", Strings.resourceCulture);
			}
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x0600009D RID: 157 RVA: 0x00002F17 File Offset: 0x00001117
		internal static string LazyMemberInfo_AccessorsNull
		{
			get
			{
				return Strings.ResourceManager.GetString("LazyMemberInfo_AccessorsNull", Strings.resourceCulture);
			}
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x0600009E RID: 158 RVA: 0x00002F2D File Offset: 0x0000112D
		internal static string LazyMemberInfo_InvalidAccessorOnSimpleMember
		{
			get
			{
				return Strings.ResourceManager.GetString("LazyMemberInfo_InvalidAccessorOnSimpleMember", Strings.resourceCulture);
			}
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x0600009F RID: 159 RVA: 0x00002F43 File Offset: 0x00001143
		internal static string LazyMemberinfo_InvalidEventAccessors_AccessorType
		{
			get
			{
				return Strings.ResourceManager.GetString("LazyMemberinfo_InvalidEventAccessors_AccessorType", Strings.resourceCulture);
			}
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x060000A0 RID: 160 RVA: 0x00002F59 File Offset: 0x00001159
		internal static string LazyMemberInfo_InvalidEventAccessors_Cardinality
		{
			get
			{
				return Strings.ResourceManager.GetString("LazyMemberInfo_InvalidEventAccessors_Cardinality", Strings.resourceCulture);
			}
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x060000A1 RID: 161 RVA: 0x00002F6F File Offset: 0x0000116F
		internal static string LazyMemberinfo_InvalidPropertyAccessors_AccessorType
		{
			get
			{
				return Strings.ResourceManager.GetString("LazyMemberinfo_InvalidPropertyAccessors_AccessorType", Strings.resourceCulture);
			}
		}

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x060000A2 RID: 162 RVA: 0x00002F85 File Offset: 0x00001185
		internal static string LazyMemberInfo_InvalidPropertyAccessors_Cardinality
		{
			get
			{
				return Strings.ResourceManager.GetString("LazyMemberInfo_InvalidPropertyAccessors_Cardinality", Strings.resourceCulture);
			}
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x060000A3 RID: 163 RVA: 0x00002F9B File Offset: 0x0000119B
		internal static string LazyMemberInfo_NoAccessors
		{
			get
			{
				return Strings.ResourceManager.GetString("LazyMemberInfo_NoAccessors", Strings.resourceCulture);
			}
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x060000A4 RID: 164 RVA: 0x00002FB1 File Offset: 0x000011B1
		internal static string LazyServices_LazyResolvesToNull
		{
			get
			{
				return Strings.ResourceManager.GetString("LazyServices_LazyResolvesToNull", Strings.resourceCulture);
			}
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x060000A5 RID: 165 RVA: 0x00002FC7 File Offset: 0x000011C7
		internal static string MetadataItemNotSupported
		{
			get
			{
				return Strings.ResourceManager.GetString("MetadataItemNotSupported", Strings.resourceCulture);
			}
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x060000A6 RID: 166 RVA: 0x00002FDD File Offset: 0x000011DD
		internal static string NotImplemented_NotOverriddenByDerived
		{
			get
			{
				return Strings.ResourceManager.GetString("NotImplemented_NotOverriddenByDerived", Strings.resourceCulture);
			}
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x060000A7 RID: 167 RVA: 0x00002FF3 File Offset: 0x000011F3
		internal static string NotSupportedCatalogChanges
		{
			get
			{
				return Strings.ResourceManager.GetString("NotSupportedCatalogChanges", Strings.resourceCulture);
			}
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x060000A8 RID: 168 RVA: 0x00003009 File Offset: 0x00001209
		internal static string NotSupportedInterfaceMetadataView
		{
			get
			{
				return Strings.ResourceManager.GetString("NotSupportedInterfaceMetadataView", Strings.resourceCulture);
			}
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x060000A9 RID: 169 RVA: 0x0000301F File Offset: 0x0000121F
		internal static string NotSupportedReadOnlyDictionary
		{
			get
			{
				return Strings.ResourceManager.GetString("NotSupportedReadOnlyDictionary", Strings.resourceCulture);
			}
		}

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x060000AA RID: 170 RVA: 0x00003035 File Offset: 0x00001235
		internal static string ObjectAlreadyInitialized
		{
			get
			{
				return Strings.ResourceManager.GetString("ObjectAlreadyInitialized", Strings.resourceCulture);
			}
		}

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x060000AB RID: 171 RVA: 0x0000304B File Offset: 0x0000124B
		internal static string ObjectMustBeInitialized
		{
			get
			{
				return Strings.ResourceManager.GetString("ObjectMustBeInitialized", Strings.resourceCulture);
			}
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x060000AC RID: 172 RVA: 0x00003061 File Offset: 0x00001261
		internal static string ReentrantCompose
		{
			get
			{
				return Strings.ResourceManager.GetString("ReentrantCompose", Strings.resourceCulture);
			}
		}

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x060000AD RID: 173 RVA: 0x00003077 File Offset: 0x00001277
		internal static string ReflectionContext_Requires_DefaultConstructor
		{
			get
			{
				return Strings.ResourceManager.GetString("ReflectionContext_Requires_DefaultConstructor", Strings.resourceCulture);
			}
		}

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x060000AE RID: 174 RVA: 0x0000308D File Offset: 0x0000128D
		internal static string ReflectionContext_Type_Required
		{
			get
			{
				return Strings.ResourceManager.GetString("ReflectionContext_Type_Required", Strings.resourceCulture);
			}
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x060000AF RID: 175 RVA: 0x000030A3 File Offset: 0x000012A3
		internal static string ReflectionModel_ExportNotReadable
		{
			get
			{
				return Strings.ResourceManager.GetString("ReflectionModel_ExportNotReadable", Strings.resourceCulture);
			}
		}

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x060000B0 RID: 176 RVA: 0x000030B9 File Offset: 0x000012B9
		internal static string ReflectionModel_ExportThrewException
		{
			get
			{
				return Strings.ResourceManager.GetString("ReflectionModel_ExportThrewException", Strings.resourceCulture);
			}
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x060000B1 RID: 177 RVA: 0x000030CF File Offset: 0x000012CF
		internal static string ReflectionModel_ImportCollectionAddThrewException
		{
			get
			{
				return Strings.ResourceManager.GetString("ReflectionModel_ImportCollectionAddThrewException", Strings.resourceCulture);
			}
		}

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x060000B2 RID: 178 RVA: 0x000030E5 File Offset: 0x000012E5
		internal static string ReflectionModel_ImportCollectionClearThrewException
		{
			get
			{
				return Strings.ResourceManager.GetString("ReflectionModel_ImportCollectionClearThrewException", Strings.resourceCulture);
			}
		}

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x060000B3 RID: 179 RVA: 0x000030FB File Offset: 0x000012FB
		internal static string ReflectionModel_ImportCollectionConstructionThrewException
		{
			get
			{
				return Strings.ResourceManager.GetString("ReflectionModel_ImportCollectionConstructionThrewException", Strings.resourceCulture);
			}
		}

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x060000B4 RID: 180 RVA: 0x00003111 File Offset: 0x00001311
		internal static string ReflectionModel_ImportCollectionGetThrewException
		{
			get
			{
				return Strings.ResourceManager.GetString("ReflectionModel_ImportCollectionGetThrewException", Strings.resourceCulture);
			}
		}

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x060000B5 RID: 181 RVA: 0x00003127 File Offset: 0x00001327
		internal static string ReflectionModel_ImportCollectionIsReadOnlyThrewException
		{
			get
			{
				return Strings.ResourceManager.GetString("ReflectionModel_ImportCollectionIsReadOnlyThrewException", Strings.resourceCulture);
			}
		}

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x060000B6 RID: 182 RVA: 0x0000313D File Offset: 0x0000133D
		internal static string ReflectionModel_ImportCollectionNotWritable
		{
			get
			{
				return Strings.ResourceManager.GetString("ReflectionModel_ImportCollectionNotWritable", Strings.resourceCulture);
			}
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x060000B7 RID: 183 RVA: 0x00003153 File Offset: 0x00001353
		internal static string ReflectionModel_ImportCollectionNull
		{
			get
			{
				return Strings.ResourceManager.GetString("ReflectionModel_ImportCollectionNull", Strings.resourceCulture);
			}
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x060000B8 RID: 184 RVA: 0x00003169 File Offset: 0x00001369
		internal static string ReflectionModel_ImportManyOnParameterCanOnlyBeAssigned
		{
			get
			{
				return Strings.ResourceManager.GetString("ReflectionModel_ImportManyOnParameterCanOnlyBeAssigned", Strings.resourceCulture);
			}
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x060000B9 RID: 185 RVA: 0x0000317F File Offset: 0x0000137F
		internal static string ReflectionModel_ImportNotAssignableFromExport
		{
			get
			{
				return Strings.ResourceManager.GetString("ReflectionModel_ImportNotAssignableFromExport", Strings.resourceCulture);
			}
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x060000BA RID: 186 RVA: 0x00003195 File Offset: 0x00001395
		internal static string ReflectionModel_ImportNotWritable
		{
			get
			{
				return Strings.ResourceManager.GetString("ReflectionModel_ImportNotWritable", Strings.resourceCulture);
			}
		}

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x060000BB RID: 187 RVA: 0x000031AB File Offset: 0x000013AB
		internal static string ReflectionModel_ImportThrewException
		{
			get
			{
				return Strings.ResourceManager.GetString("ReflectionModel_ImportThrewException", Strings.resourceCulture);
			}
		}

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x060000BC RID: 188 RVA: 0x000031C1 File Offset: 0x000013C1
		internal static string ReflectionModel_InvalidExportDefinition
		{
			get
			{
				return Strings.ResourceManager.GetString("ReflectionModel_InvalidExportDefinition", Strings.resourceCulture);
			}
		}

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x060000BD RID: 189 RVA: 0x000031D7 File Offset: 0x000013D7
		internal static string ReflectionModel_InvalidImportDefinition
		{
			get
			{
				return Strings.ResourceManager.GetString("ReflectionModel_InvalidImportDefinition", Strings.resourceCulture);
			}
		}

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x060000BE RID: 190 RVA: 0x000031ED File Offset: 0x000013ED
		internal static string ReflectionModel_InvalidMemberImportDefinition
		{
			get
			{
				return Strings.ResourceManager.GetString("ReflectionModel_InvalidMemberImportDefinition", Strings.resourceCulture);
			}
		}

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x060000BF RID: 191 RVA: 0x00003203 File Offset: 0x00001403
		internal static string ReflectionModel_InvalidParameterImportDefinition
		{
			get
			{
				return Strings.ResourceManager.GetString("ReflectionModel_InvalidParameterImportDefinition", Strings.resourceCulture);
			}
		}

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x060000C0 RID: 192 RVA: 0x00003219 File Offset: 0x00001419
		internal static string ReflectionModel_InvalidPartDefinition
		{
			get
			{
				return Strings.ResourceManager.GetString("ReflectionModel_InvalidPartDefinition", Strings.resourceCulture);
			}
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x060000C1 RID: 193 RVA: 0x0000322F File Offset: 0x0000142F
		internal static string ReflectionModel_PartConstructorMissing
		{
			get
			{
				return Strings.ResourceManager.GetString("ReflectionModel_PartConstructorMissing", Strings.resourceCulture);
			}
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x060000C2 RID: 194 RVA: 0x00003245 File Offset: 0x00001445
		internal static string ReflectionModel_PartConstructorThrewException
		{
			get
			{
				return Strings.ResourceManager.GetString("ReflectionModel_PartConstructorThrewException", Strings.resourceCulture);
			}
		}

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x060000C3 RID: 195 RVA: 0x0000325B File Offset: 0x0000145B
		internal static string ReflectionModel_PartOnImportsSatisfiedThrewException
		{
			get
			{
				return Strings.ResourceManager.GetString("ReflectionModel_PartOnImportsSatisfiedThrewException", Strings.resourceCulture);
			}
		}

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x060000C4 RID: 196 RVA: 0x00003271 File Offset: 0x00001471
		internal static string TypeCatalog_DisplayNameFormat
		{
			get
			{
				return Strings.ResourceManager.GetString("TypeCatalog_DisplayNameFormat", Strings.resourceCulture);
			}
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x060000C5 RID: 197 RVA: 0x00003287 File Offset: 0x00001487
		internal static string TypeCatalog_Empty
		{
			get
			{
				return Strings.ResourceManager.GetString("TypeCatalog_Empty", Strings.resourceCulture);
			}
		}

		// Token: 0x04000044 RID: 68
		private static ResourceManager resourceMan;

		// Token: 0x04000045 RID: 69
		private static CultureInfo resourceCulture;
	}
}
