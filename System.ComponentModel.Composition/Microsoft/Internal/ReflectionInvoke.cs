﻿using System;
using System.Reflection;

namespace Microsoft.Internal
{
	// Token: 0x0200000B RID: 11
	internal static class ReflectionInvoke
	{
		// Token: 0x0600001D RID: 29 RVA: 0x000022B7 File Offset: 0x000004B7
		public static object SafeCreateInstance(this Type type, params object[] arguments)
		{
			ReflectionInvoke.DemandMemberAccessIfNeeded(type);
			return Activator.CreateInstance(type, arguments);
		}

		// Token: 0x0600001E RID: 30 RVA: 0x000022C6 File Offset: 0x000004C6
		public static object SafeInvoke(this ConstructorInfo constructor, params object[] arguments)
		{
			ReflectionInvoke.DemandMemberAccessIfNeeded(constructor);
			return constructor.Invoke(arguments);
		}

		// Token: 0x0600001F RID: 31 RVA: 0x000022D5 File Offset: 0x000004D5
		public static object SafeInvoke(this MethodInfo method, object instance, params object[] arguments)
		{
			ReflectionInvoke.DemandMemberAccessIfNeeded(method);
			return method.Invoke(instance, arguments);
		}

		// Token: 0x06000020 RID: 32 RVA: 0x000022E5 File Offset: 0x000004E5
		public static object SafeGetValue(this FieldInfo field, object instance)
		{
			ReflectionInvoke.DemandMemberAccessIfNeeded(field);
			return field.GetValue(instance);
		}

		// Token: 0x06000021 RID: 33 RVA: 0x000022F4 File Offset: 0x000004F4
		public static void SafeSetValue(this FieldInfo field, object instance, object value)
		{
			ReflectionInvoke.DemandMemberAccessIfNeeded(field);
			field.SetValue(instance, value);
		}

		// Token: 0x06000022 RID: 34 RVA: 0x00002304 File Offset: 0x00000504
		public static void DemandMemberAccessIfNeeded(MethodInfo method)
		{
		}

		// Token: 0x06000023 RID: 35 RVA: 0x00002304 File Offset: 0x00000504
		private static void DemandMemberAccessIfNeeded(ConstructorInfo constructor)
		{
		}

		// Token: 0x06000024 RID: 36 RVA: 0x00002304 File Offset: 0x00000504
		private static void DemandMemberAccessIfNeeded(FieldInfo field)
		{
		}

		// Token: 0x06000025 RID: 37 RVA: 0x00002304 File Offset: 0x00000504
		public static void DemandMemberAccessIfNeeded(Type type)
		{
		}
	}
}
