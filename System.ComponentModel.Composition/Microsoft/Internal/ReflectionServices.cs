﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Microsoft.Internal
{
	// Token: 0x0200000C RID: 12
	internal static class ReflectionServices
	{
		// Token: 0x06000026 RID: 38 RVA: 0x00002308 File Offset: 0x00000508
		public static Assembly Assembly(this MemberInfo member)
		{
			Type type = member as Type;
			if (type != null)
			{
				return type.Assembly;
			}
			return member.DeclaringType.Assembly;
		}

		// Token: 0x06000027 RID: 39 RVA: 0x00002337 File Offset: 0x00000537
		public static bool IsVisible(this ConstructorInfo constructor)
		{
			return constructor.DeclaringType.IsVisible && constructor.IsPublic;
		}

		// Token: 0x06000028 RID: 40 RVA: 0x0000234E File Offset: 0x0000054E
		public static bool IsVisible(this FieldInfo field)
		{
			return field.DeclaringType.IsVisible && field.IsPublic;
		}

		// Token: 0x06000029 RID: 41 RVA: 0x00002368 File Offset: 0x00000568
		public static bool IsVisible(this MethodInfo method)
		{
			if (!method.DeclaringType.IsVisible)
			{
				return false;
			}
			if (!method.IsPublic)
			{
				return false;
			}
			if (method.IsGenericMethod)
			{
				Type[] genericArguments = method.GetGenericArguments();
				for (int i = 0; i < genericArguments.Length; i++)
				{
					if (!genericArguments[i].IsVisible)
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x0600002A RID: 42 RVA: 0x000023B8 File Offset: 0x000005B8
		public static string GetDisplayName(Type declaringType, string name)
		{
			Assumes.NotNull<Type>(declaringType);
			return declaringType.GetDisplayName() + "." + name;
		}

		// Token: 0x0600002B RID: 43 RVA: 0x000023D4 File Offset: 0x000005D4
		public static string GetDisplayName(this MemberInfo member)
		{
			Assumes.NotNull<MemberInfo>(member);
			MemberTypes memberType = member.MemberType;
			if (memberType == MemberTypes.TypeInfo || memberType == MemberTypes.NestedType)
			{
				return AttributedModelServices.GetTypeIdentity((Type)member);
			}
			return ReflectionServices.GetDisplayName(member.DeclaringType, member.Name);
		}

		// Token: 0x0600002C RID: 44 RVA: 0x00002418 File Offset: 0x00000618
		internal static bool TryGetGenericInterfaceType(Type instanceType, Type targetOpenInterfaceType, out Type targetClosedInterfaceType)
		{
			Assumes.IsTrue(targetOpenInterfaceType.IsInterface);
			Assumes.IsTrue(targetOpenInterfaceType.IsGenericTypeDefinition);
			Assumes.IsTrue(!instanceType.IsGenericTypeDefinition);
			if (instanceType.IsInterface && instanceType.IsGenericType && instanceType.UnderlyingSystemType.GetGenericTypeDefinition() == targetOpenInterfaceType.UnderlyingSystemType)
			{
				targetClosedInterfaceType = instanceType;
				return true;
			}
			try
			{
				Type @interface = instanceType.GetInterface(targetOpenInterfaceType.Name, false);
				if (@interface != null && @interface.UnderlyingSystemType.GetGenericTypeDefinition() == targetOpenInterfaceType.UnderlyingSystemType)
				{
					targetClosedInterfaceType = @interface;
					return true;
				}
			}
			catch (AmbiguousMatchException)
			{
			}
			targetClosedInterfaceType = null;
			return false;
		}

		// Token: 0x0600002D RID: 45 RVA: 0x000024C8 File Offset: 0x000006C8
		internal static IEnumerable<PropertyInfo> GetAllProperties(this Type type)
		{
			return type.GetInterfaces().Concat(new Type[]
			{
				type
			}).SelectMany((Type itf) => itf.GetProperties());
		}

		// Token: 0x0600002E RID: 46 RVA: 0x00002504 File Offset: 0x00000704
		internal static IEnumerable<MethodInfo> GetAllMethods(this Type type)
		{
			IEnumerable<MethodInfo> declaredMethods = type.GetDeclaredMethods();
			Type baseType = type.BaseType;
			if (baseType.UnderlyingSystemType != typeof(object))
			{
				return declaredMethods.Concat(baseType.GetAllMethods());
			}
			return declaredMethods;
		}

		// Token: 0x0600002F RID: 47 RVA: 0x00002544 File Offset: 0x00000744
		private static IEnumerable<MethodInfo> GetDeclaredMethods(this Type type)
		{
			foreach (MethodInfo methodInfo in type.GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic))
			{
				yield return methodInfo;
			}
			MethodInfo[] array = null;
			yield break;
		}

		// Token: 0x06000030 RID: 48 RVA: 0x00002554 File Offset: 0x00000754
		public static IEnumerable<FieldInfo> GetAllFields(this Type type)
		{
			IEnumerable<FieldInfo> declaredFields = type.GetDeclaredFields();
			Type baseType = type.BaseType;
			if (baseType.UnderlyingSystemType != typeof(object))
			{
				return declaredFields.Concat(baseType.GetAllFields());
			}
			return declaredFields;
		}

		// Token: 0x06000031 RID: 49 RVA: 0x00002594 File Offset: 0x00000794
		private static IEnumerable<FieldInfo> GetDeclaredFields(this Type type)
		{
			foreach (FieldInfo fieldInfo in type.GetFields(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic))
			{
				yield return fieldInfo;
			}
			FieldInfo[] array = null;
			yield break;
		}

		// Token: 0x0200000D RID: 13
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000032 RID: 50 RVA: 0x000025A4 File Offset: 0x000007A4
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06000033 RID: 51 RVA: 0x000025B0 File Offset: 0x000007B0
			public <>c()
			{
			}

			// Token: 0x06000034 RID: 52 RVA: 0x000025B8 File Offset: 0x000007B8
			internal IEnumerable<PropertyInfo> <GetAllProperties>b__7_0(Type itf)
			{
				return itf.GetProperties();
			}

			// Token: 0x04000030 RID: 48
			public static readonly ReflectionServices.<>c <>9 = new ReflectionServices.<>c();

			// Token: 0x04000031 RID: 49
			public static Func<Type, IEnumerable<PropertyInfo>> <>9__7_0;
		}

		// Token: 0x0200000E RID: 14
		[CompilerGenerated]
		private sealed class <GetDeclaredMethods>d__9 : IEnumerable<MethodInfo>, IEnumerable, IEnumerator<MethodInfo>, IDisposable, IEnumerator
		{
			// Token: 0x06000035 RID: 53 RVA: 0x000025C0 File Offset: 0x000007C0
			[DebuggerHidden]
			public <GetDeclaredMethods>d__9(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06000036 RID: 54 RVA: 0x00002304 File Offset: 0x00000504
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06000037 RID: 55 RVA: 0x000025DC File Offset: 0x000007DC
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
					i++;
				}
				else
				{
					this.<>1__state = -1;
					array = type.GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
					i = 0;
				}
				if (i >= array.Length)
				{
					array = null;
					return false;
				}
				MethodInfo methodInfo = array[i];
				this.<>2__current = methodInfo;
				this.<>1__state = 1;
				return true;
			}

			// Token: 0x17000001 RID: 1
			// (get) Token: 0x06000038 RID: 56 RVA: 0x00002667 File Offset: 0x00000867
			MethodInfo IEnumerator<MethodInfo>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000039 RID: 57 RVA: 0x0000266F File Offset: 0x0000086F
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000002 RID: 2
			// (get) Token: 0x0600003A RID: 58 RVA: 0x00002667 File Offset: 0x00000867
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600003B RID: 59 RVA: 0x00002678 File Offset: 0x00000878
			[DebuggerHidden]
			IEnumerator<MethodInfo> IEnumerable<MethodInfo>.GetEnumerator()
			{
				ReflectionServices.<GetDeclaredMethods>d__9 <GetDeclaredMethods>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<GetDeclaredMethods>d__ = this;
				}
				else
				{
					<GetDeclaredMethods>d__ = new ReflectionServices.<GetDeclaredMethods>d__9(0);
				}
				<GetDeclaredMethods>d__.type = type;
				return <GetDeclaredMethods>d__;
			}

			// Token: 0x0600003C RID: 60 RVA: 0x000026BB File Offset: 0x000008BB
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Reflection.MethodInfo>.GetEnumerator();
			}

			// Token: 0x04000032 RID: 50
			private int <>1__state;

			// Token: 0x04000033 RID: 51
			private MethodInfo <>2__current;

			// Token: 0x04000034 RID: 52
			private int <>l__initialThreadId;

			// Token: 0x04000035 RID: 53
			private Type type;

			// Token: 0x04000036 RID: 54
			public Type <>3__type;

			// Token: 0x04000037 RID: 55
			private MethodInfo[] <>7__wrap1;

			// Token: 0x04000038 RID: 56
			private int <>7__wrap2;
		}

		// Token: 0x0200000F RID: 15
		[CompilerGenerated]
		private sealed class <GetDeclaredFields>d__11 : IEnumerable<FieldInfo>, IEnumerable, IEnumerator<FieldInfo>, IDisposable, IEnumerator
		{
			// Token: 0x0600003D RID: 61 RVA: 0x000026C3 File Offset: 0x000008C3
			[DebuggerHidden]
			public <GetDeclaredFields>d__11(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x0600003E RID: 62 RVA: 0x00002304 File Offset: 0x00000504
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x0600003F RID: 63 RVA: 0x000026E0 File Offset: 0x000008E0
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
					i++;
				}
				else
				{
					this.<>1__state = -1;
					array = type.GetFields(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
					i = 0;
				}
				if (i >= array.Length)
				{
					array = null;
					return false;
				}
				FieldInfo fieldInfo = array[i];
				this.<>2__current = fieldInfo;
				this.<>1__state = 1;
				return true;
			}

			// Token: 0x17000003 RID: 3
			// (get) Token: 0x06000040 RID: 64 RVA: 0x0000276B File Offset: 0x0000096B
			FieldInfo IEnumerator<FieldInfo>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000041 RID: 65 RVA: 0x0000266F File Offset: 0x0000086F
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000004 RID: 4
			// (get) Token: 0x06000042 RID: 66 RVA: 0x0000276B File Offset: 0x0000096B
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000043 RID: 67 RVA: 0x00002774 File Offset: 0x00000974
			[DebuggerHidden]
			IEnumerator<FieldInfo> IEnumerable<FieldInfo>.GetEnumerator()
			{
				ReflectionServices.<GetDeclaredFields>d__11 <GetDeclaredFields>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<GetDeclaredFields>d__ = this;
				}
				else
				{
					<GetDeclaredFields>d__ = new ReflectionServices.<GetDeclaredFields>d__11(0);
				}
				<GetDeclaredFields>d__.type = type;
				return <GetDeclaredFields>d__;
			}

			// Token: 0x06000044 RID: 68 RVA: 0x000027B7 File Offset: 0x000009B7
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Reflection.FieldInfo>.GetEnumerator();
			}

			// Token: 0x04000039 RID: 57
			private int <>1__state;

			// Token: 0x0400003A RID: 58
			private FieldInfo <>2__current;

			// Token: 0x0400003B RID: 59
			private int <>l__initialThreadId;

			// Token: 0x0400003C RID: 60
			private Type type;

			// Token: 0x0400003D RID: 61
			public Type <>3__type;

			// Token: 0x0400003E RID: 62
			private FieldInfo[] <>7__wrap1;

			// Token: 0x0400003F RID: 63
			private int <>7__wrap2;
		}
	}
}
