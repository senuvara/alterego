﻿using System;

namespace Unity
{
	// Token: 0x02000108 RID: 264
	internal sealed class ThrowStub : ObjectDisposedException
	{
		// Token: 0x060006F7 RID: 1783 RVA: 0x000150B3 File Offset: 0x000132B3
		public static void ThrowNotSupportedException()
		{
			throw new PlatformNotSupportedException();
		}
	}
}
