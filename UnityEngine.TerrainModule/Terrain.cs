﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Rendering;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000009 RID: 9
	[UsedByNativeCode]
	[NativeHeader("Modules/Terrain/Public/Terrain.h")]
	[NativeHeader("Runtime/Interfaces/ITerrainManager.h")]
	[NativeHeader("TerrainScriptingClasses.h")]
	[StaticAccessor("GetITerrainManager()", StaticAccessorType.Arrow)]
	public sealed class Terrain : Behaviour
	{
		// Token: 0x0600002B RID: 43 RVA: 0x00003B0F File Offset: 0x00001D0F
		public Terrain()
		{
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x0600002C RID: 44
		// (set) Token: 0x0600002D RID: 45
		public extern TerrainData terrainData { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x0600002E RID: 46
		// (set) Token: 0x0600002F RID: 47
		public extern float treeDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000030 RID: 48
		// (set) Token: 0x06000031 RID: 49
		public extern float treeBillboardDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000032 RID: 50
		// (set) Token: 0x06000033 RID: 51
		public extern float treeCrossFadeLength { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000034 RID: 52
		// (set) Token: 0x06000035 RID: 53
		public extern int treeMaximumFullLODCount { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000036 RID: 54
		// (set) Token: 0x06000037 RID: 55
		public extern float detailObjectDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000038 RID: 56
		// (set) Token: 0x06000039 RID: 57
		public extern float detailObjectDensity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x0600003A RID: 58
		// (set) Token: 0x0600003B RID: 59
		public extern float heightmapPixelError { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x0600003C RID: 60
		// (set) Token: 0x0600003D RID: 61
		public extern int heightmapMaximumLOD { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x0600003E RID: 62
		// (set) Token: 0x0600003F RID: 63
		public extern float basemapDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000040 RID: 64 RVA: 0x00003B18 File Offset: 0x00001D18
		// (set) Token: 0x06000041 RID: 65 RVA: 0x00003B33 File Offset: 0x00001D33
		[Obsolete("splatmapDistance is deprecated, please use basemapDistance instead. (UnityUpgradable) -> basemapDistance", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public float splatmapDistance
		{
			get
			{
				return this.basemapDistance;
			}
			set
			{
				this.basemapDistance = value;
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000042 RID: 66
		// (set) Token: 0x06000043 RID: 67
		[NativeProperty("StaticLightmapIndexInt")]
		public extern int lightmapIndex { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000044 RID: 68
		// (set) Token: 0x06000045 RID: 69
		[NativeProperty("DynamicLightmapIndexInt")]
		public extern int realtimeLightmapIndex { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x06000046 RID: 70 RVA: 0x00003B40 File Offset: 0x00001D40
		// (set) Token: 0x06000047 RID: 71 RVA: 0x00003B56 File Offset: 0x00001D56
		[NativeProperty("StaticLightmapST")]
		public Vector4 lightmapScaleOffset
		{
			get
			{
				Vector4 result;
				this.get_lightmapScaleOffset_Injected(out result);
				return result;
			}
			set
			{
				this.set_lightmapScaleOffset_Injected(ref value);
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x06000048 RID: 72 RVA: 0x00003B60 File Offset: 0x00001D60
		// (set) Token: 0x06000049 RID: 73 RVA: 0x00003B76 File Offset: 0x00001D76
		[NativeProperty("DynamicLightmapST")]
		public Vector4 realtimeLightmapScaleOffset
		{
			get
			{
				Vector4 result;
				this.get_realtimeLightmapScaleOffset_Injected(out result);
				return result;
			}
			set
			{
				this.set_realtimeLightmapScaleOffset_Injected(ref value);
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x0600004A RID: 74
		// (set) Token: 0x0600004B RID: 75
		[NativeProperty("GarbageCollectCameraData")]
		public extern bool freeUnusedRenderingResources { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x0600004C RID: 76
		// (set) Token: 0x0600004D RID: 77
		public extern bool castShadows { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x0600004E RID: 78
		// (set) Token: 0x0600004F RID: 79
		public extern ReflectionProbeUsage reflectionProbeUsage { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000050 RID: 80
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void GetClosestReflectionProbes(List<ReflectionProbeBlendInfo> result);

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000051 RID: 81
		// (set) Token: 0x06000052 RID: 82
		public extern Terrain.MaterialType materialType { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x06000053 RID: 83
		// (set) Token: 0x06000054 RID: 84
		public extern Material materialTemplate { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x06000055 RID: 85 RVA: 0x00003B80 File Offset: 0x00001D80
		// (set) Token: 0x06000056 RID: 86 RVA: 0x00003B96 File Offset: 0x00001D96
		public Color legacySpecular
		{
			get
			{
				Color result;
				this.get_legacySpecular_Injected(out result);
				return result;
			}
			set
			{
				this.set_legacySpecular_Injected(ref value);
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x06000057 RID: 87
		// (set) Token: 0x06000058 RID: 88
		public extern float legacyShininess { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000059 RID: 89
		// (set) Token: 0x0600005A RID: 90
		public extern bool drawHeightmap { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x0600005B RID: 91
		// (set) Token: 0x0600005C RID: 92
		public extern bool allowAutoConnect { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x0600005D RID: 93
		// (set) Token: 0x0600005E RID: 94
		public extern int groupingID { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x0600005F RID: 95
		// (set) Token: 0x06000060 RID: 96
		public extern bool drawInstanced { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x06000061 RID: 97
		public extern RenderTexture normalmapTexture { [NativeMethod("TryGetNormalMapTexture")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x06000062 RID: 98
		// (set) Token: 0x06000063 RID: 99
		public extern bool drawTreesAndFoliage { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x06000064 RID: 100 RVA: 0x00003BA0 File Offset: 0x00001DA0
		// (set) Token: 0x06000065 RID: 101 RVA: 0x00003BB6 File Offset: 0x00001DB6
		public Vector3 patchBoundsMultiplier
		{
			get
			{
				Vector3 result;
				this.get_patchBoundsMultiplier_Injected(out result);
				return result;
			}
			set
			{
				this.set_patchBoundsMultiplier_Injected(ref value);
			}
		}

		// Token: 0x06000066 RID: 102 RVA: 0x00003BC0 File Offset: 0x00001DC0
		public float SampleHeight(Vector3 worldPosition)
		{
			return this.SampleHeight_Injected(ref worldPosition);
		}

		// Token: 0x06000067 RID: 103
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ApplyDelayedHeightmapModification();

		// Token: 0x06000068 RID: 104 RVA: 0x00003BCA File Offset: 0x00001DCA
		public void AddTreeInstance(TreeInstance instance)
		{
			this.AddTreeInstance_Injected(ref instance);
		}

		// Token: 0x06000069 RID: 105
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetNeighbors(Terrain left, Terrain top, Terrain right, Terrain bottom);

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x0600006A RID: 106
		// (set) Token: 0x0600006B RID: 107
		public extern float treeLODBiasMultiplier { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x0600006C RID: 108
		// (set) Token: 0x0600006D RID: 109
		public extern bool collectDetailPatches { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x0600006E RID: 110
		// (set) Token: 0x0600006F RID: 111
		public extern TerrainRenderFlags editorRenderFlags { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000070 RID: 112 RVA: 0x00003BD4 File Offset: 0x00001DD4
		public Vector3 GetPosition()
		{
			Vector3 result;
			this.GetPosition_Injected(out result);
			return result;
		}

		// Token: 0x06000071 RID: 113
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Flush();

		// Token: 0x06000072 RID: 114 RVA: 0x00003BEA File Offset: 0x00001DEA
		internal void RemoveTrees(Vector2 position, float radius, int prototypeIndex)
		{
			this.RemoveTrees_Injected(ref position, radius, prototypeIndex);
		}

		// Token: 0x06000073 RID: 115
		[NativeMethod("CopySplatMaterialCustomProps")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetSplatMaterialPropertyBlock(MaterialPropertyBlock properties);

		// Token: 0x06000074 RID: 116 RVA: 0x00003BF6 File Offset: 0x00001DF6
		public void GetSplatMaterialPropertyBlock(MaterialPropertyBlock dest)
		{
			if (dest == null)
			{
				throw new ArgumentNullException("dest");
			}
			this.Internal_GetSplatMaterialPropertyBlock(dest);
		}

		// Token: 0x06000075 RID: 117
		[NativeMethod("GetSplatMaterialCustomProps")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_GetSplatMaterialPropertyBlock(MaterialPropertyBlock dest);

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x06000076 RID: 118
		// (set) Token: 0x06000077 RID: 119
		public extern bool preserveTreePrototypeLayers { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x06000078 RID: 120
		[StaticAccessor("Terrain", StaticAccessorType.DoubleColon)]
		public static extern TextureFormat heightmapTextureFormat { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x06000079 RID: 121
		[StaticAccessor("Terrain", StaticAccessorType.DoubleColon)]
		public static extern RenderTextureFormat heightmapRenderTextureFormat { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x0600007A RID: 122
		public static extern Terrain activeTerrain { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600007B RID: 123
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetConnectivityDirty();

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x0600007C RID: 124
		[NativeProperty("ActiveTerrainsScriptingArray")]
		public static extern Terrain[] activeTerrains { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600007D RID: 125
		[UsedByNativeCode]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern GameObject CreateTerrainGameObject(TerrainData assignTerrain);

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x0600007E RID: 126
		public extern Terrain leftNeighbor { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x0600007F RID: 127
		public extern Terrain rightNeighbor { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x06000080 RID: 128
		public extern Terrain topNeighbor { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x06000081 RID: 129
		public extern Terrain bottomNeighbor { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000082 RID: 130
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_lightmapScaleOffset_Injected(out Vector4 ret);

		// Token: 0x06000083 RID: 131
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_lightmapScaleOffset_Injected(ref Vector4 value);

		// Token: 0x06000084 RID: 132
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_realtimeLightmapScaleOffset_Injected(out Vector4 ret);

		// Token: 0x06000085 RID: 133
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_realtimeLightmapScaleOffset_Injected(ref Vector4 value);

		// Token: 0x06000086 RID: 134
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_legacySpecular_Injected(out Color ret);

		// Token: 0x06000087 RID: 135
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_legacySpecular_Injected(ref Color value);

		// Token: 0x06000088 RID: 136
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_patchBoundsMultiplier_Injected(out Vector3 ret);

		// Token: 0x06000089 RID: 137
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_patchBoundsMultiplier_Injected(ref Vector3 value);

		// Token: 0x0600008A RID: 138
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern float SampleHeight_Injected(ref Vector3 worldPosition);

		// Token: 0x0600008B RID: 139
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void AddTreeInstance_Injected(ref TreeInstance instance);

		// Token: 0x0600008C RID: 140
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetPosition_Injected(out Vector3 ret);

		// Token: 0x0600008D RID: 141
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void RemoveTrees_Injected(ref Vector2 position, float radius, int prototypeIndex);

		// Token: 0x0200000A RID: 10
		public enum MaterialType
		{
			// Token: 0x0400002F RID: 47
			BuiltInStandard,
			// Token: 0x04000030 RID: 48
			BuiltInLegacyDiffuse,
			// Token: 0x04000031 RID: 49
			BuiltInLegacySpecular,
			// Token: 0x04000032 RID: 50
			Custom
		}
	}
}
