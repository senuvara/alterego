﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000014 RID: 20
	[UsedByNativeCode]
	[NativeHeader("Modules/Terrain/Public/TerrainDataScriptingInterface.h")]
	[NativeHeader("TerrainScriptingClasses.h")]
	public sealed class TerrainData : Object
	{
		// Token: 0x060000D0 RID: 208 RVA: 0x0000458D File Offset: 0x0000278D
		public TerrainData()
		{
			TerrainData.Internal_Create(this);
		}

		// Token: 0x060000D1 RID: 209
		[StaticAccessor("TerrainDataScriptingInterface", StaticAccessorType.DoubleColon)]
		[ThreadSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetBoundaryValue(TerrainData.BoundaryValueType type);

		// Token: 0x060000D2 RID: 210
		[FreeFunction("TerrainDataScriptingInterface::Create")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Create([Writable] TerrainData terrainData);

		// Token: 0x060000D3 RID: 211
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void UpdateDirtyRegion(int x, int y, int width, int height, bool syncHeightmapTextureImmediately);

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x060000D4 RID: 212
		public extern int heightmapWidth { [NativeName("GetHeightmap().GetWidth")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x060000D5 RID: 213
		public extern int heightmapHeight { [NativeName("GetHeightmap().GetHeight")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x060000D6 RID: 214
		public extern RenderTexture heightmapTexture { [NativeName("GetHeightmap().GetHeightmapTexture")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x060000D7 RID: 215 RVA: 0x0000459C File Offset: 0x0000279C
		// (set) Token: 0x060000D8 RID: 216 RVA: 0x000045B8 File Offset: 0x000027B8
		public int heightmapResolution
		{
			get
			{
				return this.internalHeightmapResolution;
			}
			set
			{
				int internalHeightmapResolution = value;
				if (value < 0 || value > TerrainData.k_MaximumResolution)
				{
					Debug.LogWarning("heightmapResolution is clamped to the range of [0, " + TerrainData.k_MaximumResolution + "].");
					internalHeightmapResolution = Math.Min(TerrainData.k_MaximumResolution, Math.Max(value, 0));
				}
				this.internalHeightmapResolution = internalHeightmapResolution;
			}
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x060000D9 RID: 217
		// (set) Token: 0x060000DA RID: 218
		private extern int internalHeightmapResolution { [NativeName("GetHeightmap().GetResolution")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeName("GetHeightmap().SetResolution")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x060000DB RID: 219 RVA: 0x00004614 File Offset: 0x00002814
		public Vector3 heightmapScale
		{
			[NativeName("GetHeightmap().GetScale")]
			get
			{
				Vector3 result;
				this.get_heightmapScale_Injected(out result);
				return result;
			}
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x060000DC RID: 220 RVA: 0x0000462C File Offset: 0x0000282C
		// (set) Token: 0x060000DD RID: 221 RVA: 0x00004642 File Offset: 0x00002842
		public Vector3 size
		{
			[NativeName("GetHeightmap().GetSize")]
			get
			{
				Vector3 result;
				this.get_size_Injected(out result);
				return result;
			}
			[NativeName("GetHeightmap().SetSize")]
			set
			{
				this.set_size_Injected(ref value);
			}
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x060000DE RID: 222 RVA: 0x0000464C File Offset: 0x0000284C
		public Bounds bounds
		{
			[NativeName("GetHeightmap().CalculateBounds")]
			get
			{
				Bounds result;
				this.get_bounds_Injected(out result);
				return result;
			}
		}

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x060000DF RID: 223
		// (set) Token: 0x060000E0 RID: 224
		public extern float thickness { [NativeName("GetHeightmap().GetThickness")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeName("GetHeightmap().SetThickness")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060000E1 RID: 225
		[NativeName("GetHeightmap().GetHeight")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetHeight(int x, int y);

		// Token: 0x060000E2 RID: 226
		[NativeName("GetHeightmap().GetInterpolatedHeight")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetInterpolatedHeight(float x, float y);

		// Token: 0x060000E3 RID: 227 RVA: 0x00004664 File Offset: 0x00002864
		public float[,] GetHeights(int xBase, int yBase, int width, int height)
		{
			if (xBase < 0 || yBase < 0 || xBase + width < 0 || yBase + height < 0 || xBase + width > this.heightmapWidth || yBase + height > this.heightmapHeight)
			{
				throw new ArgumentException("Trying to access out-of-bounds terrain height information.");
			}
			return this.Internal_GetHeights(xBase, yBase, width, height);
		}

		// Token: 0x060000E4 RID: 228
		[FreeFunction("TerrainDataScriptingInterface::GetHeights", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern float[,] Internal_GetHeights(int xBase, int yBase, int width, int height);

		// Token: 0x060000E5 RID: 229 RVA: 0x000046D0 File Offset: 0x000028D0
		public void SetHeights(int xBase, int yBase, float[,] heights)
		{
			if (heights == null)
			{
				throw new NullReferenceException();
			}
			if (xBase + heights.GetLength(1) > this.heightmapWidth || xBase + heights.GetLength(1) < 0 || yBase + heights.GetLength(0) < 0 || xBase < 0 || yBase < 0 || yBase + heights.GetLength(0) > this.heightmapHeight)
			{
				throw new ArgumentException(UnityString.Format("X or Y base out of bounds. Setting up to {0}x{1} while map size is {2}x{3}", new object[]
				{
					xBase + heights.GetLength(1),
					yBase + heights.GetLength(0),
					this.heightmapWidth,
					this.heightmapHeight
				}));
			}
			this.Internal_SetHeights(xBase, yBase, heights.GetLength(1), heights.GetLength(0), heights);
		}

		// Token: 0x060000E6 RID: 230
		[FreeFunction("TerrainDataScriptingInterface::SetHeights", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_SetHeights(int xBase, int yBase, int width, int height, float[,] heights);

		// Token: 0x060000E7 RID: 231
		[FreeFunction("TerrainDataScriptingInterface::GetPatchMinMaxHeights", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern PatchExtents[] GetPatchMinMaxHeights();

		// Token: 0x060000E8 RID: 232
		[FreeFunction("TerrainDataScriptingInterface::OverrideMinMaxPatchHeights", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void OverrideMinMaxPatchHeights(PatchExtents[] minMaxHeights);

		// Token: 0x060000E9 RID: 233
		[FreeFunction("TerrainDataScriptingInterface::GetMaximumHeightError", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float[] GetMaximumHeightError();

		// Token: 0x060000EA RID: 234
		[FreeFunction("TerrainDataScriptingInterface::OverrideMaximumHeightError", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void OverrideMaximumHeightError(float[] maxError);

		// Token: 0x060000EB RID: 235 RVA: 0x000047AC File Offset: 0x000029AC
		public void SetHeightsDelayLOD(int xBase, int yBase, float[,] heights)
		{
			if (heights == null)
			{
				throw new ArgumentNullException("heights");
			}
			int length = heights.GetLength(0);
			int length2 = heights.GetLength(1);
			if (xBase < 0 || xBase + length2 < 0 || xBase + length2 > this.heightmapWidth)
			{
				throw new ArgumentException(UnityString.Format("X out of bounds - trying to set {0}-{1} but the terrain ranges from 0-{2}", new object[]
				{
					xBase,
					xBase + length2,
					this.heightmapWidth
				}));
			}
			if (yBase < 0 || yBase + length < 0 || yBase + length > this.heightmapHeight)
			{
				throw new ArgumentException(UnityString.Format("Y out of bounds - trying to set {0}-{1} but the terrain ranges from 0-{2}", new object[]
				{
					yBase,
					yBase + length,
					this.heightmapHeight
				}));
			}
			this.Internal_SetHeightsDelayLOD(xBase, yBase, length2, length, heights);
		}

		// Token: 0x060000EC RID: 236
		[FreeFunction("TerrainDataScriptingInterface::SetHeightsDelayLOD", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_SetHeightsDelayLOD(int xBase, int yBase, int width, int height, float[,] heights);

		// Token: 0x060000ED RID: 237
		[NativeName("GetHeightmap().GetSteepness")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetSteepness(float x, float y);

		// Token: 0x060000EE RID: 238 RVA: 0x00004894 File Offset: 0x00002A94
		[NativeName("GetHeightmap().GetInterpolatedNormal")]
		public Vector3 GetInterpolatedNormal(float x, float y)
		{
			Vector3 result;
			this.GetInterpolatedNormal_Injected(x, y, out result);
			return result;
		}

		// Token: 0x060000EF RID: 239
		[NativeName("GetHeightmap().GetAdjustedSize")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern int GetAdjustedSize(int size);

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x060000F0 RID: 240
		// (set) Token: 0x060000F1 RID: 241
		public extern float wavingGrassStrength { [NativeName("GetDetailDatabase().GetWavingGrassStrength")] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("TerrainDataScriptingInterface::SetWavingGrassStrength", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x060000F2 RID: 242
		// (set) Token: 0x060000F3 RID: 243
		public extern float wavingGrassAmount { [NativeName("GetDetailDatabase().GetWavingGrassAmount")] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("TerrainDataScriptingInterface::SetWavingGrassAmount", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x060000F4 RID: 244
		// (set) Token: 0x060000F5 RID: 245
		public extern float wavingGrassSpeed { [NativeName("GetDetailDatabase().GetWavingGrassSpeed")] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("TerrainDataScriptingInterface::SetWavingGrassSpeed", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x060000F6 RID: 246 RVA: 0x000048AC File Offset: 0x00002AAC
		// (set) Token: 0x060000F7 RID: 247 RVA: 0x000048C2 File Offset: 0x00002AC2
		public Color wavingGrassTint
		{
			[NativeName("GetDetailDatabase().GetWavingGrassTint")]
			get
			{
				Color result;
				this.get_wavingGrassTint_Injected(out result);
				return result;
			}
			[FreeFunction("TerrainDataScriptingInterface::SetWavingGrassTint", HasExplicitThis = true)]
			set
			{
				this.set_wavingGrassTint_Injected(ref value);
			}
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x060000F8 RID: 248
		public extern int detailWidth { [NativeName("GetDetailDatabase().GetWidth")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x060000F9 RID: 249
		public extern int detailHeight { [NativeName("GetDetailDatabase().GetHeight")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060000FA RID: 250 RVA: 0x000048CC File Offset: 0x00002ACC
		public void SetDetailResolution(int detailResolution, int resolutionPerPatch)
		{
			if (detailResolution < 0)
			{
				Debug.LogWarning("detailResolution must not be negative.");
				detailResolution = 0;
			}
			if (resolutionPerPatch < TerrainData.k_MinimumDetailResolutionPerPatch || resolutionPerPatch > TerrainData.k_MaximumDetailResolutionPerPatch)
			{
				Debug.LogWarning(string.Concat(new object[]
				{
					"resolutionPerPatch is clamped to the range of [",
					TerrainData.k_MinimumDetailResolutionPerPatch,
					", ",
					TerrainData.k_MaximumDetailResolutionPerPatch,
					"]."
				}));
				resolutionPerPatch = Math.Min(TerrainData.k_MaximumDetailResolutionPerPatch, Math.Max(resolutionPerPatch, TerrainData.k_MinimumDetailResolutionPerPatch));
			}
			int num = detailResolution / resolutionPerPatch;
			if (num > TerrainData.k_MaximumDetailPatchCount)
			{
				Debug.LogWarning("Patch count (detailResolution / resolutionPerPatch) is clamped to the range of [0, " + TerrainData.k_MaximumDetailPatchCount + "].");
				num = Math.Min(TerrainData.k_MaximumDetailPatchCount, Math.Max(num, 0));
			}
			this.Internal_SetDetailResolution(num, resolutionPerPatch);
		}

		// Token: 0x060000FB RID: 251
		[NativeName("GetDetailDatabase().SetDetailResolution")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_SetDetailResolution(int patchCount, int resolutionPerPatch);

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x060000FC RID: 252
		public extern int detailPatchCount { [NativeName("GetDetailDatabase().GetPatchCount")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x060000FD RID: 253
		public extern int detailResolution { [NativeName("GetDetailDatabase().GetResolution")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x060000FE RID: 254
		public extern int detailResolutionPerPatch { [NativeName("GetDetailDatabase().GetResolutionPerPatch")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060000FF RID: 255
		[NativeName("GetDetailDatabase().ResetDirtyDetails")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void ResetDirtyDetails();

		// Token: 0x06000100 RID: 256
		[FreeFunction("TerrainDataScriptingInterface::RefreshPrototypes", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RefreshPrototypes();

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x06000101 RID: 257
		// (set) Token: 0x06000102 RID: 258
		public extern DetailPrototype[] detailPrototypes { [FreeFunction("TerrainDataScriptingInterface::GetDetailPrototypes", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("TerrainDataScriptingInterface::SetDetailPrototypes", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000103 RID: 259
		[FreeFunction("TerrainDataScriptingInterface::GetSupportedLayers", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int[] GetSupportedLayers(int xBase, int yBase, int totalWidth, int totalHeight);

		// Token: 0x06000104 RID: 260
		[FreeFunction("TerrainDataScriptingInterface::GetDetailLayer", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int[,] GetDetailLayer(int xBase, int yBase, int width, int height, int layer);

		// Token: 0x06000105 RID: 261 RVA: 0x000049AA File Offset: 0x00002BAA
		public void SetDetailLayer(int xBase, int yBase, int layer, int[,] details)
		{
			this.Internal_SetDetailLayer(xBase, yBase, details.GetLength(1), details.GetLength(0), layer, details);
		}

		// Token: 0x06000106 RID: 262
		[FreeFunction("TerrainDataScriptingInterface::SetDetailLayer", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_SetDetailLayer(int xBase, int yBase, int totalWidth, int totalHeight, int detailIndex, int[,] data);

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x06000107 RID: 263 RVA: 0x000049C8 File Offset: 0x00002BC8
		// (set) Token: 0x06000108 RID: 264 RVA: 0x000049E3 File Offset: 0x00002BE3
		public TreeInstance[] treeInstances
		{
			get
			{
				return this.Internal_GetTreeInstances();
			}
			set
			{
				this.Internal_SetTreeInstances(value);
			}
		}

		// Token: 0x06000109 RID: 265
		[NativeName("GetTreeDatabase().GetInstances")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern TreeInstance[] Internal_GetTreeInstances();

		// Token: 0x0600010A RID: 266
		[FreeFunction("TerrainDataScriptingInterface::SetTreeInstances", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_SetTreeInstances([NotNull] TreeInstance[] instances);

		// Token: 0x0600010B RID: 267 RVA: 0x000049F0 File Offset: 0x00002BF0
		public TreeInstance GetTreeInstance(int index)
		{
			if (index < 0 || index >= this.treeInstanceCount)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			return this.Internal_GetTreeInstance(index);
		}

		// Token: 0x0600010C RID: 268 RVA: 0x00004A2C File Offset: 0x00002C2C
		[FreeFunction("TerrainDataScriptingInterface::GetTreeInstance", HasExplicitThis = true)]
		private TreeInstance Internal_GetTreeInstance(int index)
		{
			TreeInstance result;
			this.Internal_GetTreeInstance_Injected(index, out result);
			return result;
		}

		// Token: 0x0600010D RID: 269 RVA: 0x00004A43 File Offset: 0x00002C43
		[NativeThrows]
		[FreeFunction("TerrainDataScriptingInterface::SetTreeInstance", HasExplicitThis = true)]
		public void SetTreeInstance(int index, TreeInstance instance)
		{
			this.SetTreeInstance_Injected(index, ref instance);
		}

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x0600010E RID: 270
		public extern int treeInstanceCount { [NativeName("GetTreeDatabase().GetInstances().size")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x0600010F RID: 271
		// (set) Token: 0x06000110 RID: 272
		public extern TreePrototype[] treePrototypes { [FreeFunction("TerrainDataScriptingInterface::GetTreePrototypes", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("TerrainDataScriptingInterface::SetTreePrototypes", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000111 RID: 273
		[NativeName("GetTreeDatabase().RemoveTreePrototype")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void RemoveTreePrototype(int index);

		// Token: 0x06000112 RID: 274
		[NativeName("GetTreeDatabase().RecalculateTreePositions")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void RecalculateTreePositions();

		// Token: 0x06000113 RID: 275
		[NativeName("GetDetailDatabase().RemoveDetailPrototype")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void RemoveDetailPrototype(int index);

		// Token: 0x06000114 RID: 276
		[NativeName("GetTreeDatabase().NeedUpgradeScaledPrototypes")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern bool NeedUpgradeScaledTreePrototypes();

		// Token: 0x06000115 RID: 277
		[FreeFunction("TerrainDataScriptingInterface::UpgradeScaledTreePrototype", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void UpgradeScaledTreePrototype();

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x06000116 RID: 278
		public extern int alphamapLayers { [NativeName("GetSplatDatabase().GetSplatCount")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000117 RID: 279 RVA: 0x00004A50 File Offset: 0x00002C50
		public float[,,] GetAlphamaps(int x, int y, int width, int height)
		{
			if (x < 0 || y < 0 || width < 0 || height < 0)
			{
				throw new ArgumentException("Invalid argument for GetAlphaMaps");
			}
			return this.Internal_GetAlphamaps(x, y, width, height);
		}

		// Token: 0x06000118 RID: 280
		[FreeFunction("TerrainDataScriptingInterface::GetAlphamaps", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern float[,,] Internal_GetAlphamaps(int x, int y, int width, int height);

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x06000119 RID: 281 RVA: 0x00004A98 File Offset: 0x00002C98
		// (set) Token: 0x0600011A RID: 282 RVA: 0x00004AB4 File Offset: 0x00002CB4
		public int alphamapResolution
		{
			get
			{
				return this.Internal_alphamapResolution;
			}
			set
			{
				int internal_alphamapResolution = value;
				if (value < TerrainData.k_MinimumAlphamapResolution || value > TerrainData.k_MaximumAlphamapResolution)
				{
					Debug.LogWarning(string.Concat(new object[]
					{
						"alphamapResolution is clamped to the range of [",
						TerrainData.k_MinimumAlphamapResolution,
						", ",
						TerrainData.k_MaximumAlphamapResolution,
						"]."
					}));
					internal_alphamapResolution = Math.Min(TerrainData.k_MaximumAlphamapResolution, Math.Max(value, TerrainData.k_MinimumAlphamapResolution));
				}
				this.Internal_alphamapResolution = internal_alphamapResolution;
			}
		}

		// Token: 0x0600011B RID: 283
		[NativeName("GetSplatDatabase().GetAlphamapResolution")]
		[RequiredByNativeCode]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern float GetAlphamapResolutionInternal();

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x0600011C RID: 284
		// (set) Token: 0x0600011D RID: 285
		private extern int Internal_alphamapResolution { [NativeName("GetSplatDatabase().GetAlphamapResolution")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeName("GetSplatDatabase().SetAlphamapResolution")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x0600011E RID: 286 RVA: 0x00004B3C File Offset: 0x00002D3C
		public int alphamapWidth
		{
			get
			{
				return this.alphamapResolution;
			}
		}

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x0600011F RID: 287 RVA: 0x00004B58 File Offset: 0x00002D58
		public int alphamapHeight
		{
			get
			{
				return this.alphamapResolution;
			}
		}

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x06000120 RID: 288 RVA: 0x00004B74 File Offset: 0x00002D74
		// (set) Token: 0x06000121 RID: 289 RVA: 0x00004B90 File Offset: 0x00002D90
		public int baseMapResolution
		{
			get
			{
				return this.Internal_baseMapResolution;
			}
			set
			{
				int internal_baseMapResolution = value;
				if (value < TerrainData.k_MinimumBaseMapResolution || value > TerrainData.k_MaximumBaseMapResolution)
				{
					Debug.LogWarning(string.Concat(new object[]
					{
						"baseMapResolution is clamped to the range of [",
						TerrainData.k_MinimumBaseMapResolution,
						", ",
						TerrainData.k_MaximumBaseMapResolution,
						"]."
					}));
					internal_baseMapResolution = Math.Min(TerrainData.k_MaximumBaseMapResolution, Math.Max(value, TerrainData.k_MinimumBaseMapResolution));
				}
				this.Internal_baseMapResolution = internal_baseMapResolution;
			}
		}

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x06000122 RID: 290
		// (set) Token: 0x06000123 RID: 291
		private extern int Internal_baseMapResolution { [NativeName("GetSplatDatabase().GetBaseMapResolution")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeName("GetSplatDatabase().SetBaseMapResolution")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000124 RID: 292 RVA: 0x00004C18 File Offset: 0x00002E18
		public void SetAlphamaps(int x, int y, float[,,] map)
		{
			if (map.GetLength(2) != this.alphamapLayers)
			{
				throw new Exception(UnityString.Format("Float array size wrong (layers should be {0})", new object[]
				{
					this.alphamapLayers
				}));
			}
			this.Internal_SetAlphamaps(x, y, map.GetLength(1), map.GetLength(0), map);
		}

		// Token: 0x06000125 RID: 293
		[FreeFunction("TerrainDataScriptingInterface::SetAlphamaps", HasExplicitThis = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_SetAlphamaps(int x, int y, int width, int height, float[,,] map);

		// Token: 0x06000126 RID: 294
		[NativeName("GetSplatDatabase().SetBaseMapsDirty")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetBaseMapDirty();

		// Token: 0x06000127 RID: 295
		[NativeName("GetSplatDatabase().GetAlphaTexture")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Texture2D GetAlphamapTexture(int index);

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x06000128 RID: 296
		public extern int alphamapTextureCount { [NativeName("GetSplatDatabase().GetAlphaTextureCount")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x06000129 RID: 297 RVA: 0x00004C74 File Offset: 0x00002E74
		public Texture2D[] alphamapTextures
		{
			get
			{
				Texture2D[] array = new Texture2D[this.alphamapTextureCount];
				for (int i = 0; i < array.Length; i++)
				{
					array[i] = this.GetAlphamapTexture(i);
				}
				return array;
			}
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x0600012A RID: 298
		// (set) Token: 0x0600012B RID: 299
		[Obsolete("Please use the terrainLayers API instead.", false)]
		public extern SplatPrototype[] splatPrototypes { [FreeFunction("TerrainDataScriptingInterface::GetSplatPrototypes", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("TerrainDataScriptingInterface::SetSplatPrototypes", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x0600012C RID: 300
		// (set) Token: 0x0600012D RID: 301
		public extern TerrainLayer[] terrainLayers { [FreeFunction("TerrainDataScriptingInterface::GetTerrainLayers", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; [FreeFunction("TerrainDataScriptingInterface::SetTerrainLayers", HasExplicitThis = true)] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600012E RID: 302
		[NativeName("GetTreeDatabase().AddTree")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void AddTree(ref TreeInstance tree);

		// Token: 0x0600012F RID: 303 RVA: 0x00004CB4 File Offset: 0x00002EB4
		[NativeName("GetTreeDatabase().RemoveTrees")]
		internal int RemoveTrees(Vector2 position, float radius, int prototypeIndex)
		{
			return this.RemoveTrees_Injected(ref position, radius, prototypeIndex);
		}

		// Token: 0x06000130 RID: 304 RVA: 0x00004CC0 File Offset: 0x00002EC0
		// Note: this type is marked as 'beforefieldinit'.
		static TerrainData()
		{
		}

		// Token: 0x06000131 RID: 305
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_heightmapScale_Injected(out Vector3 ret);

		// Token: 0x06000132 RID: 306
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_size_Injected(out Vector3 ret);

		// Token: 0x06000133 RID: 307
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_size_Injected(ref Vector3 value);

		// Token: 0x06000134 RID: 308
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_bounds_Injected(out Bounds ret);

		// Token: 0x06000135 RID: 309
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetInterpolatedNormal_Injected(float x, float y, out Vector3 ret);

		// Token: 0x06000136 RID: 310
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_wavingGrassTint_Injected(out Color ret);

		// Token: 0x06000137 RID: 311
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_wavingGrassTint_Injected(ref Color value);

		// Token: 0x06000138 RID: 312
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_GetTreeInstance_Injected(int index, out TreeInstance ret);

		// Token: 0x06000139 RID: 313
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetTreeInstance_Injected(int index, ref TreeInstance instance);

		// Token: 0x0600013A RID: 314
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int RemoveTrees_Injected(ref Vector2 position, float radius, int prototypeIndex);

		// Token: 0x04000055 RID: 85
		private const string k_ScriptingInterfaceName = "TerrainDataScriptingInterface";

		// Token: 0x04000056 RID: 86
		private const string k_ScriptingInterfacePrefix = "TerrainDataScriptingInterface::";

		// Token: 0x04000057 RID: 87
		private const string k_HeightmapPrefix = "GetHeightmap().";

		// Token: 0x04000058 RID: 88
		private const string k_DetailDatabasePrefix = "GetDetailDatabase().";

		// Token: 0x04000059 RID: 89
		private const string k_TreeDatabasePrefix = "GetTreeDatabase().";

		// Token: 0x0400005A RID: 90
		private const string k_SplatDatabasePrefix = "GetSplatDatabase().";

		// Token: 0x0400005B RID: 91
		private static readonly int k_MaximumResolution = TerrainData.GetBoundaryValue(TerrainData.BoundaryValueType.MaxHeightmapRes);

		// Token: 0x0400005C RID: 92
		private static readonly int k_MinimumDetailResolutionPerPatch = TerrainData.GetBoundaryValue(TerrainData.BoundaryValueType.MinDetailResPerPatch);

		// Token: 0x0400005D RID: 93
		private static readonly int k_MaximumDetailResolutionPerPatch = TerrainData.GetBoundaryValue(TerrainData.BoundaryValueType.MaxDetailResPerPatch);

		// Token: 0x0400005E RID: 94
		private static readonly int k_MaximumDetailPatchCount = TerrainData.GetBoundaryValue(TerrainData.BoundaryValueType.MaxDetailPatchCount);

		// Token: 0x0400005F RID: 95
		private static readonly int k_MinimumAlphamapResolution = TerrainData.GetBoundaryValue(TerrainData.BoundaryValueType.MinAlphamapRes);

		// Token: 0x04000060 RID: 96
		private static readonly int k_MaximumAlphamapResolution = TerrainData.GetBoundaryValue(TerrainData.BoundaryValueType.MaxAlphamapRes);

		// Token: 0x04000061 RID: 97
		private static readonly int k_MinimumBaseMapResolution = TerrainData.GetBoundaryValue(TerrainData.BoundaryValueType.MinBaseMapRes);

		// Token: 0x04000062 RID: 98
		private static readonly int k_MaximumBaseMapResolution = TerrainData.GetBoundaryValue(TerrainData.BoundaryValueType.MaxBaseMapRes);

		// Token: 0x02000015 RID: 21
		private enum BoundaryValueType
		{
			// Token: 0x04000064 RID: 100
			MaxHeightmapRes,
			// Token: 0x04000065 RID: 101
			MinDetailResPerPatch,
			// Token: 0x04000066 RID: 102
			MaxDetailResPerPatch,
			// Token: 0x04000067 RID: 103
			MaxDetailPatchCount,
			// Token: 0x04000068 RID: 104
			MinAlphamapRes,
			// Token: 0x04000069 RID: 105
			MaxAlphamapRes,
			// Token: 0x0400006A RID: 106
			MinBaseMapRes,
			// Token: 0x0400006B RID: 107
			MaxBaseMapRes
		}
	}
}
