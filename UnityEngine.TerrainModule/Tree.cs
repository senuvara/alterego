﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200000C RID: 12
	[NativeHeader("Modules/Terrain/Public/Tree.h")]
	public sealed class Tree : Component
	{
		// Token: 0x06000092 RID: 146 RVA: 0x00003CD3 File Offset: 0x00001ED3
		public Tree()
		{
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x06000093 RID: 147
		// (set) Token: 0x06000094 RID: 148
		[NativeProperty("TreeData")]
		public extern ScriptableObject data { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x06000095 RID: 149
		public extern bool hasSpeedTreeWind { [NativeMethod("HasSpeedTreeWind")] [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
