﻿using System;

namespace UnityEngine
{
	// Token: 0x02000007 RID: 7
	[Flags]
	public enum TerrainChangedFlags
	{
		// Token: 0x0400001F RID: 31
		Heightmap = 1,
		// Token: 0x04000020 RID: 32
		TreeInstances = 2,
		// Token: 0x04000021 RID: 33
		DelayedHeightmapUpdate = 4,
		// Token: 0x04000022 RID: 34
		FlushEverythingImmediately = 8,
		// Token: 0x04000023 RID: 35
		RemoveDirtyDetailsImmediately = 16,
		// Token: 0x04000024 RID: 36
		WillBeDestroyed = 256
	}
}
