﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000011 RID: 17
	[UsedByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class SplatPrototype
	{
		// Token: 0x060000BD RID: 189 RVA: 0x0000436C File Offset: 0x0000256C
		public SplatPrototype()
		{
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x060000BE RID: 190 RVA: 0x000043D4 File Offset: 0x000025D4
		// (set) Token: 0x060000BF RID: 191 RVA: 0x000043EF File Offset: 0x000025EF
		public Texture2D texture
		{
			get
			{
				return this.m_Texture;
			}
			set
			{
				this.m_Texture = value;
			}
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x060000C0 RID: 192 RVA: 0x000043FC File Offset: 0x000025FC
		// (set) Token: 0x060000C1 RID: 193 RVA: 0x00004417 File Offset: 0x00002617
		public Texture2D normalMap
		{
			get
			{
				return this.m_NormalMap;
			}
			set
			{
				this.m_NormalMap = value;
			}
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x060000C2 RID: 194 RVA: 0x00004424 File Offset: 0x00002624
		// (set) Token: 0x060000C3 RID: 195 RVA: 0x0000443F File Offset: 0x0000263F
		public Vector2 tileSize
		{
			get
			{
				return this.m_TileSize;
			}
			set
			{
				this.m_TileSize = value;
			}
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x060000C4 RID: 196 RVA: 0x0000444C File Offset: 0x0000264C
		// (set) Token: 0x060000C5 RID: 197 RVA: 0x00004467 File Offset: 0x00002667
		public Vector2 tileOffset
		{
			get
			{
				return this.m_TileOffset;
			}
			set
			{
				this.m_TileOffset = value;
			}
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x060000C6 RID: 198 RVA: 0x00004474 File Offset: 0x00002674
		// (set) Token: 0x060000C7 RID: 199 RVA: 0x000044AF File Offset: 0x000026AF
		public Color specular
		{
			get
			{
				return new Color(this.m_SpecularMetallic.x, this.m_SpecularMetallic.y, this.m_SpecularMetallic.z);
			}
			set
			{
				this.m_SpecularMetallic.x = value.r;
				this.m_SpecularMetallic.y = value.g;
				this.m_SpecularMetallic.z = value.b;
			}
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x060000C8 RID: 200 RVA: 0x000044E8 File Offset: 0x000026E8
		// (set) Token: 0x060000C9 RID: 201 RVA: 0x00004508 File Offset: 0x00002708
		public float metallic
		{
			get
			{
				return this.m_SpecularMetallic.w;
			}
			set
			{
				this.m_SpecularMetallic.w = value;
			}
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x060000CA RID: 202 RVA: 0x00004518 File Offset: 0x00002718
		// (set) Token: 0x060000CB RID: 203 RVA: 0x00004533 File Offset: 0x00002733
		public float smoothness
		{
			get
			{
				return this.m_Smoothness;
			}
			set
			{
				this.m_Smoothness = value;
			}
		}

		// Token: 0x04000045 RID: 69
		internal Texture2D m_Texture;

		// Token: 0x04000046 RID: 70
		internal Texture2D m_NormalMap;

		// Token: 0x04000047 RID: 71
		internal Vector2 m_TileSize = new Vector2(15f, 15f);

		// Token: 0x04000048 RID: 72
		internal Vector2 m_TileOffset = new Vector2(0f, 0f);

		// Token: 0x04000049 RID: 73
		internal Vector4 m_SpecularMetallic = new Vector4(0f, 0f, 0f, 0f);

		// Token: 0x0400004A RID: 74
		internal float m_Smoothness = 0f;
	}
}
