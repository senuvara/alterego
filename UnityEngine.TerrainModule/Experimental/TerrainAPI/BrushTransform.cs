﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.TerrainAPI
{
	// Token: 0x02000002 RID: 2
	public struct BrushTransform
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public BrushTransform(Vector2 brushOrigin, Vector2 brushU, Vector2 brushV)
		{
			float num = brushU.x * brushV.y - brushU.y * brushV.x;
			float d = (!Mathf.Approximately(num, 0f)) ? (1f / num) : 1f;
			Vector2 vector = new Vector2(brushV.y, -brushU.y) * d;
			Vector2 vector2 = new Vector2(-brushV.x, brushU.x) * d;
			Vector2 vector3 = -brushOrigin.x * vector - brushOrigin.y * vector2;
			this.brushOrigin = brushOrigin;
			this.brushU = brushU;
			this.brushV = brushV;
			this.targetOrigin = vector3;
			this.targetX = vector;
			this.targetY = vector2;
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000002 RID: 2 RVA: 0x00002124 File Offset: 0x00000324
		public Vector2 brushOrigin
		{
			[CompilerGenerated]
			get
			{
				return this.<brushOrigin>k__BackingField;
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000003 RID: 3 RVA: 0x00002140 File Offset: 0x00000340
		public Vector2 brushU
		{
			[CompilerGenerated]
			get
			{
				return this.<brushU>k__BackingField;
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000004 RID: 4 RVA: 0x0000215C File Offset: 0x0000035C
		public Vector2 brushV
		{
			[CompilerGenerated]
			get
			{
				return this.<brushV>k__BackingField;
			}
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000005 RID: 5 RVA: 0x00002178 File Offset: 0x00000378
		public Vector2 targetOrigin
		{
			[CompilerGenerated]
			get
			{
				return this.<targetOrigin>k__BackingField;
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000006 RID: 6 RVA: 0x00002194 File Offset: 0x00000394
		public Vector2 targetX
		{
			[CompilerGenerated]
			get
			{
				return this.<targetX>k__BackingField;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000007 RID: 7 RVA: 0x000021B0 File Offset: 0x000003B0
		public Vector2 targetY
		{
			[CompilerGenerated]
			get
			{
				return this.<targetY>k__BackingField;
			}
		}

		// Token: 0x06000008 RID: 8 RVA: 0x000021CC File Offset: 0x000003CC
		public Rect GetBrushXYBounds()
		{
			Vector2 vector = this.brushOrigin + this.brushU;
			Vector2 vector2 = this.brushOrigin + this.brushV;
			Vector2 vector3 = this.brushOrigin + this.brushU + this.brushV;
			float xmin = Mathf.Min(Mathf.Min(this.brushOrigin.x, vector.x), Mathf.Min(vector2.x, vector3.x));
			float xmax = Mathf.Max(Mathf.Max(this.brushOrigin.x, vector.x), Mathf.Max(vector2.x, vector3.x));
			float ymin = Mathf.Min(Mathf.Min(this.brushOrigin.y, vector.y), Mathf.Min(vector2.y, vector3.y));
			float ymax = Mathf.Max(Mathf.Max(this.brushOrigin.y, vector.y), Mathf.Max(vector2.y, vector3.y));
			return Rect.MinMaxRect(xmin, ymin, xmax, ymax);
		}

		// Token: 0x06000009 RID: 9 RVA: 0x00002304 File Offset: 0x00000504
		public static BrushTransform FromRect(Rect brushRect)
		{
			Vector2 min = brushRect.min;
			Vector2 brushU = new Vector2(brushRect.width, 0f);
			Vector2 brushV = new Vector2(0f, brushRect.height);
			return new BrushTransform(min, brushU, brushV);
		}

		// Token: 0x0600000A RID: 10 RVA: 0x00002350 File Offset: 0x00000550
		public Vector2 ToBrushUV(Vector2 targetXY)
		{
			return targetXY.x * this.targetX + targetXY.y * this.targetY + this.targetOrigin;
		}

		// Token: 0x0600000B RID: 11 RVA: 0x0000239C File Offset: 0x0000059C
		public Vector2 FromBrushUV(Vector2 brushUV)
		{
			return brushUV.x * this.brushU + brushUV.y * this.brushV + this.brushOrigin;
		}

		// Token: 0x04000001 RID: 1
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private readonly Vector2 <brushOrigin>k__BackingField;

		// Token: 0x04000002 RID: 2
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private readonly Vector2 <brushU>k__BackingField;

		// Token: 0x04000003 RID: 3
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private readonly Vector2 <brushV>k__BackingField;

		// Token: 0x04000004 RID: 4
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private readonly Vector2 <targetOrigin>k__BackingField;

		// Token: 0x04000005 RID: 5
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private readonly Vector2 <targetX>k__BackingField;

		// Token: 0x04000006 RID: 6
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private readonly Vector2 <targetY>k__BackingField;
	}
}
