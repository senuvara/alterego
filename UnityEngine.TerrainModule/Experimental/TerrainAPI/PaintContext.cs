﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine.Rendering;

namespace UnityEngine.Experimental.TerrainAPI
{
	// Token: 0x02000003 RID: 3
	public class PaintContext
	{
		// Token: 0x0600000C RID: 12 RVA: 0x000023E8 File Offset: 0x000005E8
		public PaintContext(Terrain terrain, RectInt pixelRect, int targetTextureWidth, int targetTextureHeight)
		{
			this.originTerrain = terrain;
			this.pixelRect = pixelRect;
			this.targetTextureWidth = targetTextureWidth;
			this.targetTextureHeight = targetTextureHeight;
			TerrainData terrainData = terrain.terrainData;
			this.pixelSize = new Vector2(terrainData.size.x / ((float)targetTextureWidth - 1f), terrainData.size.z / ((float)targetTextureHeight - 1f));
			this.FindTerrainTiles();
			this.ClipTerrainTiles();
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600000D RID: 13 RVA: 0x00002468 File Offset: 0x00000668
		public Terrain originTerrain
		{
			[CompilerGenerated]
			get
			{
				return this.<originTerrain>k__BackingField;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x0600000E RID: 14 RVA: 0x00002484 File Offset: 0x00000684
		public RectInt pixelRect
		{
			[CompilerGenerated]
			get
			{
				return this.<pixelRect>k__BackingField;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x0600000F RID: 15 RVA: 0x000024A0 File Offset: 0x000006A0
		public int targetTextureWidth
		{
			[CompilerGenerated]
			get
			{
				return this.<targetTextureWidth>k__BackingField;
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000010 RID: 16 RVA: 0x000024BC File Offset: 0x000006BC
		public int targetTextureHeight
		{
			[CompilerGenerated]
			get
			{
				return this.<targetTextureHeight>k__BackingField;
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000011 RID: 17 RVA: 0x000024D8 File Offset: 0x000006D8
		public Vector2 pixelSize
		{
			[CompilerGenerated]
			get
			{
				return this.<pixelSize>k__BackingField;
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000012 RID: 18 RVA: 0x000024F4 File Offset: 0x000006F4
		public RenderTexture sourceRenderTexture
		{
			get
			{
				return this.m_SourceRenderTexture;
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000013 RID: 19 RVA: 0x00002510 File Offset: 0x00000710
		public RenderTexture destinationRenderTexture
		{
			get
			{
				return this.m_DestinationRenderTexture;
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x06000014 RID: 20 RVA: 0x0000252C File Offset: 0x0000072C
		public RenderTexture oldRenderTexture
		{
			get
			{
				return this.m_OldRenderTexture;
			}
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000015 RID: 21 RVA: 0x00002548 File Offset: 0x00000748
		public int terrainCount
		{
			get
			{
				return this.m_TerrainTiles.Count;
			}
		}

		// Token: 0x06000016 RID: 22 RVA: 0x00002568 File Offset: 0x00000768
		public Terrain GetTerrain(int terrainIndex)
		{
			return this.m_TerrainTiles[terrainIndex].terrain;
		}

		// Token: 0x06000017 RID: 23 RVA: 0x00002590 File Offset: 0x00000790
		public RectInt GetClippedPixelRectInTerrainPixels(int terrainIndex)
		{
			return this.m_TerrainTiles[terrainIndex].clippedLocalPixels;
		}

		// Token: 0x06000018 RID: 24 RVA: 0x000025B8 File Offset: 0x000007B8
		public RectInt GetClippedPixelRectInRenderTexturePixels(int terrainIndex)
		{
			return this.m_TerrainTiles[terrainIndex].clippedPCPixels;
		}

		// Token: 0x14000001 RID: 1
		// (add) Token: 0x06000019 RID: 25 RVA: 0x000025E0 File Offset: 0x000007E0
		// (remove) Token: 0x0600001A RID: 26 RVA: 0x00002614 File Offset: 0x00000814
		internal static event Action<PaintContext.TerrainTile, PaintContext.ToolAction, string> onTerrainTileBeforePaint
		{
			add
			{
				Action<PaintContext.TerrainTile, PaintContext.ToolAction, string> action = PaintContext.onTerrainTileBeforePaint;
				Action<PaintContext.TerrainTile, PaintContext.ToolAction, string> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<PaintContext.TerrainTile, PaintContext.ToolAction, string>>(ref PaintContext.onTerrainTileBeforePaint, (Action<PaintContext.TerrainTile, PaintContext.ToolAction, string>)Delegate.Combine(action2, value), action);
				}
				while (action != action2);
			}
			remove
			{
				Action<PaintContext.TerrainTile, PaintContext.ToolAction, string> action = PaintContext.onTerrainTileBeforePaint;
				Action<PaintContext.TerrainTile, PaintContext.ToolAction, string> action2;
				do
				{
					action2 = action;
					action = Interlocked.CompareExchange<Action<PaintContext.TerrainTile, PaintContext.ToolAction, string>>(ref PaintContext.onTerrainTileBeforePaint, (Action<PaintContext.TerrainTile, PaintContext.ToolAction, string>)Delegate.Remove(action2, value), action);
				}
				while (action != action2);
			}
		}

		// Token: 0x0600001B RID: 27 RVA: 0x00002648 File Offset: 0x00000848
		public static PaintContext CreateFromBounds(Terrain terrain, Rect boundsInTerrainSpace, int inputTextureWidth, int inputTextureHeight, int extraBorderPixels = 0)
		{
			return new PaintContext(terrain, TerrainPaintUtility.CalcPixelRectFromBounds(terrain, boundsInTerrainSpace, inputTextureWidth, inputTextureHeight, extraBorderPixels), inputTextureWidth, inputTextureHeight);
		}

		// Token: 0x0600001C RID: 28 RVA: 0x00002670 File Offset: 0x00000870
		internal void FindTerrainTiles()
		{
			this.m_TerrainTiles = new List<PaintContext.TerrainTile>();
			Terrain leftNeighbor = this.originTerrain.leftNeighbor;
			Terrain rightNeighbor = this.originTerrain.rightNeighbor;
			Terrain topNeighbor = this.originTerrain.topNeighbor;
			Terrain bottomNeighbor = this.originTerrain.bottomNeighbor;
			bool flag = this.pixelRect.x < 0;
			bool flag2 = this.pixelRect.xMax > this.targetTextureWidth - 1;
			bool flag3 = this.pixelRect.yMax > this.targetTextureHeight - 1;
			bool flag4 = this.pixelRect.y < 0;
			if (flag && flag2)
			{
				Debug.LogWarning("PaintContext pixelRect is too large!  It should touch a maximum of 2 Terrains horizontally.");
				flag2 = false;
			}
			if (flag3 && flag4)
			{
				Debug.LogWarning("PaintContext pixelRect is too large!  It should touch a maximum of 2 Terrains vertically.");
				flag4 = false;
			}
			PaintContext.TerrainTile item = new PaintContext.TerrainTile(this.originTerrain, 0, 0);
			this.m_TerrainTiles.Add(item);
			Terrain terrain = null;
			Terrain terrain2 = null;
			Terrain terrain3 = null;
			int num = 0;
			int num2 = 0;
			if (flag)
			{
				num = -1;
				terrain = leftNeighbor;
			}
			else if (flag2)
			{
				num = 1;
				terrain = rightNeighbor;
			}
			if (flag3)
			{
				num2 = 1;
				terrain2 = topNeighbor;
			}
			else if (flag4)
			{
				num2 = -1;
				terrain2 = bottomNeighbor;
			}
			if (terrain)
			{
				item = new PaintContext.TerrainTile(terrain, num * (this.targetTextureWidth - 1), 0);
				this.m_TerrainTiles.Add(item);
				if (flag3 && terrain.topNeighbor)
				{
					terrain3 = terrain.topNeighbor;
				}
				else if (flag4 && terrain.bottomNeighbor)
				{
					terrain3 = terrain.bottomNeighbor;
				}
			}
			if (terrain2)
			{
				item = new PaintContext.TerrainTile(terrain2, 0, num2 * (this.targetTextureHeight - 1));
				this.m_TerrainTiles.Add(item);
				if (flag && terrain2.leftNeighbor)
				{
					terrain3 = terrain2.leftNeighbor;
				}
				else if (flag2 && terrain2.rightNeighbor)
				{
					terrain3 = terrain2.rightNeighbor;
				}
			}
			if (terrain3 != null)
			{
				item = new PaintContext.TerrainTile(terrain3, num * (this.targetTextureWidth - 1), num2 * (this.targetTextureHeight - 1));
				this.m_TerrainTiles.Add(item);
			}
		}

		// Token: 0x0600001D RID: 29 RVA: 0x000028EC File Offset: 0x00000AEC
		internal void ClipTerrainTiles()
		{
			for (int i = 0; i < this.m_TerrainTiles.Count; i++)
			{
				PaintContext.TerrainTile terrainTile = this.m_TerrainTiles[i];
				terrainTile.clippedLocalPixels = default(RectInt);
				terrainTile.clippedLocalPixels.x = Mathf.Max(0, this.pixelRect.x - terrainTile.tileOriginPixels.x);
				terrainTile.clippedLocalPixels.y = Mathf.Max(0, this.pixelRect.y - terrainTile.tileOriginPixels.y);
				terrainTile.clippedLocalPixels.xMax = Mathf.Min(this.targetTextureWidth, this.pixelRect.xMax - terrainTile.tileOriginPixels.x);
				terrainTile.clippedLocalPixels.yMax = Mathf.Min(this.targetTextureHeight, this.pixelRect.yMax - terrainTile.tileOriginPixels.y);
				terrainTile.clippedPCPixels = new RectInt(terrainTile.clippedLocalPixels.x + terrainTile.tileOriginPixels.x - this.pixelRect.x, terrainTile.clippedLocalPixels.y + terrainTile.tileOriginPixels.y - this.pixelRect.y, terrainTile.clippedLocalPixels.width, terrainTile.clippedLocalPixels.height);
			}
		}

		// Token: 0x0600001E RID: 30 RVA: 0x00002A5C File Offset: 0x00000C5C
		public void CreateRenderTargets(RenderTextureFormat colorFormat)
		{
			this.m_SourceRenderTexture = RenderTexture.GetTemporary(this.pixelRect.width, this.pixelRect.height, 0, colorFormat, RenderTextureReadWrite.Linear);
			this.m_DestinationRenderTexture = RenderTexture.GetTemporary(this.pixelRect.width, this.pixelRect.height, 0, colorFormat, RenderTextureReadWrite.Linear);
			this.m_SourceRenderTexture.wrapMode = TextureWrapMode.Clamp;
			this.m_SourceRenderTexture.filterMode = FilterMode.Point;
			this.m_OldRenderTexture = RenderTexture.active;
		}

		// Token: 0x0600001F RID: 31 RVA: 0x00002AE4 File Offset: 0x00000CE4
		public void Cleanup(bool restoreRenderTexture = true)
		{
			if (restoreRenderTexture)
			{
				RenderTexture.active = this.m_OldRenderTexture;
			}
			RenderTexture.ReleaseTemporary(this.m_SourceRenderTexture);
			RenderTexture.ReleaseTemporary(this.m_DestinationRenderTexture);
			this.m_SourceRenderTexture = null;
			this.m_DestinationRenderTexture = null;
			this.m_OldRenderTexture = null;
		}

		// Token: 0x06000020 RID: 32 RVA: 0x00002B30 File Offset: 0x00000D30
		public void GatherHeightmap()
		{
			Material blitMaterial = TerrainPaintUtility.GetBlitMaterial();
			RenderTexture.active = this.sourceRenderTexture;
			GL.Clear(false, true, new Color(0f, 0f, 0f, 0f));
			GL.PushMatrix();
			GL.LoadPixelMatrix(0f, (float)this.pixelRect.width, 0f, (float)this.pixelRect.height);
			for (int i = 0; i < this.m_TerrainTiles.Count; i++)
			{
				PaintContext.TerrainTile terrainTile = this.m_TerrainTiles[i];
				if (terrainTile.clippedLocalPixels.width != 0 && terrainTile.clippedLocalPixels.height != 0)
				{
					Texture heightmapTexture = terrainTile.terrain.terrainData.heightmapTexture;
					if (heightmapTexture.width != this.targetTextureWidth || heightmapTexture.height != this.targetTextureHeight)
					{
						Debug.LogWarning("PaintContext heightmap operations must use the same resolution for all Terrains - mismatched Terrains are ignored.", terrainTile.terrain);
					}
					else
					{
						FilterMode filterMode = heightmapTexture.filterMode;
						heightmapTexture.filterMode = FilterMode.Point;
						blitMaterial.SetTexture("_MainTex", heightmapTexture);
						blitMaterial.SetPass(0);
						TerrainPaintUtility.DrawQuad(terrainTile.clippedPCPixels, terrainTile.clippedLocalPixels, heightmapTexture);
						heightmapTexture.filterMode = filterMode;
					}
				}
			}
			GL.PopMatrix();
			RenderTexture.active = this.oldRenderTexture;
		}

		// Token: 0x06000021 RID: 33 RVA: 0x00002C98 File Offset: 0x00000E98
		public void ScatterHeightmap(string editorUndoName)
		{
			Material blitMaterial = TerrainPaintUtility.GetBlitMaterial();
			for (int i = 0; i < this.m_TerrainTiles.Count; i++)
			{
				PaintContext.TerrainTile terrainTile = this.m_TerrainTiles[i];
				if (terrainTile.clippedLocalPixels.width != 0 && terrainTile.clippedLocalPixels.height != 0)
				{
					RenderTexture heightmapTexture = terrainTile.terrain.terrainData.heightmapTexture;
					if (heightmapTexture.width != this.targetTextureWidth || heightmapTexture.height != this.targetTextureHeight)
					{
						Debug.LogWarning("PaintContext heightmap operations must use the same resolution for all Terrains - mismatched Terrains are ignored.", terrainTile.terrain);
					}
					else
					{
						if (PaintContext.onTerrainTileBeforePaint != null)
						{
							PaintContext.onTerrainTileBeforePaint(terrainTile, PaintContext.ToolAction.PaintHeightmap, editorUndoName);
						}
						RenderTexture.active = heightmapTexture;
						GL.PushMatrix();
						GL.LoadPixelMatrix(0f, (float)heightmapTexture.width, 0f, (float)heightmapTexture.height);
						this.destinationRenderTexture.filterMode = FilterMode.Point;
						blitMaterial.SetTexture("_MainTex", this.destinationRenderTexture);
						blitMaterial.SetPass(0);
						TerrainPaintUtility.DrawQuad(terrainTile.clippedLocalPixels, terrainTile.clippedPCPixels, this.destinationRenderTexture);
						GL.PopMatrix();
						terrainTile.terrain.terrainData.UpdateDirtyRegion(terrainTile.clippedLocalPixels.x, terrainTile.clippedLocalPixels.y, terrainTile.clippedLocalPixels.width, terrainTile.clippedLocalPixels.height, !terrainTile.terrain.drawInstanced);
						PaintContext.OnTerrainPainted(terrainTile, PaintContext.ToolAction.PaintHeightmap);
					}
				}
			}
		}

		// Token: 0x06000022 RID: 34 RVA: 0x00002E18 File Offset: 0x00001018
		public void GatherNormals()
		{
			RenderTexture normalmapTexture = this.originTerrain.normalmapTexture;
			Material blitMaterial = TerrainPaintUtility.GetBlitMaterial();
			RenderTexture.active = this.sourceRenderTexture;
			GL.Clear(false, true, new Color(0.5f, 0.5f, 0.5f, 0.5f));
			GL.PushMatrix();
			GL.LoadPixelMatrix(0f, (float)this.pixelRect.width, 0f, (float)this.pixelRect.height);
			for (int i = 0; i < this.m_TerrainTiles.Count; i++)
			{
				PaintContext.TerrainTile terrainTile = this.m_TerrainTiles[i];
				if (terrainTile.clippedLocalPixels.width != 0 && terrainTile.clippedLocalPixels.height != 0)
				{
					Texture normalmapTexture2 = terrainTile.terrain.normalmapTexture;
					if (normalmapTexture2.width != this.targetTextureWidth || normalmapTexture2.height != this.targetTextureHeight)
					{
						Debug.LogWarning("PaintContext normalmap operations must use the same resolution for all Terrains - mismatched Terrains are ignored.", terrainTile.terrain);
					}
					else
					{
						FilterMode filterMode = normalmapTexture2.filterMode;
						normalmapTexture2.filterMode = FilterMode.Point;
						blitMaterial.SetTexture("_MainTex", normalmapTexture2);
						blitMaterial.SetPass(0);
						TerrainPaintUtility.DrawQuad(terrainTile.clippedPCPixels, terrainTile.clippedLocalPixels, normalmapTexture2);
						normalmapTexture2.filterMode = filterMode;
					}
				}
			}
			GL.PopMatrix();
			RenderTexture.active = this.oldRenderTexture;
		}

		// Token: 0x06000023 RID: 35 RVA: 0x00002F8C File Offset: 0x0000118C
		public void GatherAlphamap(TerrainLayer inputLayer, bool addLayerIfDoesntExist = true)
		{
			if (!(inputLayer == null))
			{
				int num = TerrainPaintUtility.FindTerrainLayerIndex(this.originTerrain, inputLayer);
				if (num == -1 && addLayerIfDoesntExist)
				{
					num = TerrainPaintUtility.AddTerrainLayer(this.originTerrain, inputLayer);
				}
				RenderTexture.active = this.sourceRenderTexture;
				GL.Clear(false, true, new Color(0f, 0f, 0f, 0f));
				GL.PushMatrix();
				GL.LoadPixelMatrix(0f, (float)this.pixelRect.width, 0f, (float)this.pixelRect.height);
				Vector4[] array = new Vector4[]
				{
					new Vector4(1f, 0f, 0f, 0f),
					new Vector4(0f, 1f, 0f, 0f),
					new Vector4(0f, 0f, 1f, 0f),
					new Vector4(0f, 0f, 0f, 1f)
				};
				Material copyTerrainLayerMaterial = TerrainPaintUtility.GetCopyTerrainLayerMaterial();
				for (int i = 0; i < this.m_TerrainTiles.Count; i++)
				{
					PaintContext.TerrainTile terrainTile = this.m_TerrainTiles[i];
					if (terrainTile.clippedLocalPixels.width != 0 && terrainTile.clippedLocalPixels.height != 0)
					{
						int num2 = TerrainPaintUtility.FindTerrainLayerIndex(terrainTile.terrain, inputLayer);
						if (num2 == -1)
						{
							if (!addLayerIfDoesntExist)
							{
								terrainTile.clippedLocalPixels.width = 0;
								terrainTile.clippedLocalPixels.height = 0;
								terrainTile.clippedPCPixels.width = 0;
								terrainTile.clippedPCPixels.height = 0;
								goto IL_313;
							}
							num2 = TerrainPaintUtility.AddTerrainLayer(terrainTile.terrain, inputLayer);
						}
						terrainTile.mapIndex = num2 >> 2;
						terrainTile.channelIndex = (num2 & 3);
						Texture terrainAlphaMapChecked = TerrainPaintUtility.GetTerrainAlphaMapChecked(terrainTile.terrain, terrainTile.mapIndex);
						if (terrainAlphaMapChecked.width != this.targetTextureWidth || terrainAlphaMapChecked.height != this.targetTextureHeight)
						{
							Debug.LogWarning(string.Concat(new object[]
							{
								"PaintContext alphamap operations must use the same resolution for all Terrains - mismatched Terrains are ignored. (",
								terrainAlphaMapChecked.width,
								" x ",
								terrainAlphaMapChecked.height,
								") != (",
								this.targetTextureWidth,
								" x ",
								this.targetTextureHeight,
								")"
							}), terrainTile.terrain);
						}
						else
						{
							FilterMode filterMode = terrainAlphaMapChecked.filterMode;
							terrainAlphaMapChecked.filterMode = FilterMode.Point;
							copyTerrainLayerMaterial.SetVector("_LayerMask", array[terrainTile.channelIndex]);
							copyTerrainLayerMaterial.SetTexture("_MainTex", terrainAlphaMapChecked);
							copyTerrainLayerMaterial.SetPass(0);
							TerrainPaintUtility.DrawQuad(terrainTile.clippedPCPixels, terrainTile.clippedLocalPixels, terrainAlphaMapChecked);
							terrainAlphaMapChecked.filterMode = filterMode;
						}
					}
					IL_313:;
				}
				GL.PopMatrix();
				RenderTexture.active = this.oldRenderTexture;
			}
		}

		// Token: 0x06000024 RID: 36 RVA: 0x000032D4 File Offset: 0x000014D4
		public void ScatterAlphamap(string editorUndoName)
		{
			Vector4[] array = new Vector4[]
			{
				new Vector4(1f, 0f, 0f, 0f),
				new Vector4(0f, 1f, 0f, 0f),
				new Vector4(0f, 0f, 1f, 0f),
				new Vector4(0f, 0f, 0f, 1f)
			};
			Material copyTerrainLayerMaterial = TerrainPaintUtility.GetCopyTerrainLayerMaterial();
			for (int i = 0; i < this.m_TerrainTiles.Count; i++)
			{
				PaintContext.TerrainTile terrainTile = this.m_TerrainTiles[i];
				if (terrainTile.clippedLocalPixels.width != 0 && terrainTile.clippedLocalPixels.height != 0)
				{
					if (PaintContext.onTerrainTileBeforePaint != null)
					{
						PaintContext.onTerrainTileBeforePaint(terrainTile, PaintContext.ToolAction.PaintTexture, editorUndoName);
					}
					RenderTexture temporary = RenderTexture.GetTemporary(new RenderTextureDescriptor(this.destinationRenderTexture.width, this.destinationRenderTexture.height, RenderTextureFormat.ARGB32)
					{
						sRGB = false,
						useMipMap = false,
						autoGenerateMips = false
					});
					RenderTexture.active = temporary;
					RectInt clippedPCPixels = terrainTile.clippedPCPixels;
					Rect rect = new Rect((float)clippedPCPixels.x / (float)this.pixelRect.width, (float)clippedPCPixels.y / (float)this.pixelRect.height, (float)clippedPCPixels.width / (float)this.pixelRect.width, (float)clippedPCPixels.height / (float)this.pixelRect.height);
					this.destinationRenderTexture.filterMode = FilterMode.Point;
					int mapIndex = terrainTile.mapIndex;
					int channelIndex = terrainTile.channelIndex;
					Texture2D value = terrainTile.terrain.terrainData.alphamapTextures[mapIndex];
					for (int j = 0; j < terrainTile.terrain.terrainData.alphamapTextureCount; j++)
					{
						Texture2D texture2D = terrainTile.terrain.terrainData.alphamapTextures[j];
						if (texture2D.width != this.targetTextureWidth || texture2D.height != this.targetTextureHeight)
						{
							Debug.LogWarning("PaintContext alphamap operations must use the same resolution for all Terrains - mismatched Terrains are ignored.", terrainTile.terrain);
						}
						else
						{
							Rect rect2 = new Rect((float)terrainTile.clippedLocalPixels.x / (float)texture2D.width, (float)terrainTile.clippedLocalPixels.y / (float)texture2D.height, (float)terrainTile.clippedLocalPixels.width / (float)texture2D.width, (float)terrainTile.clippedLocalPixels.height / (float)texture2D.height);
							copyTerrainLayerMaterial.SetTexture("_MainTex", this.destinationRenderTexture);
							copyTerrainLayerMaterial.SetTexture("_OldAlphaMapTexture", this.sourceRenderTexture);
							copyTerrainLayerMaterial.SetTexture("_OriginalTargetAlphaMap", value);
							copyTerrainLayerMaterial.SetTexture("_AlphaMapTexture", texture2D);
							copyTerrainLayerMaterial.SetVector("_LayerMask", (j != mapIndex) ? Vector4.zero : array[channelIndex]);
							copyTerrainLayerMaterial.SetVector("_OriginalTargetAlphaMask", array[channelIndex]);
							copyTerrainLayerMaterial.SetPass(1);
							GL.PushMatrix();
							GL.LoadPixelMatrix(0f, (float)temporary.width, 0f, (float)temporary.height);
							GL.Begin(7);
							GL.Color(new Color(1f, 1f, 1f, 1f));
							GL.MultiTexCoord2(0, rect.x, rect.y);
							GL.MultiTexCoord2(1, rect2.x, rect2.y);
							GL.Vertex3((float)clippedPCPixels.x, (float)clippedPCPixels.y, 0f);
							GL.MultiTexCoord2(0, rect.x, rect.yMax);
							GL.MultiTexCoord2(1, rect2.x, rect2.yMax);
							GL.Vertex3((float)clippedPCPixels.x, (float)clippedPCPixels.yMax, 0f);
							GL.MultiTexCoord2(0, rect.xMax, rect.yMax);
							GL.MultiTexCoord2(1, rect2.xMax, rect2.yMax);
							GL.Vertex3((float)clippedPCPixels.xMax, (float)clippedPCPixels.yMax, 0f);
							GL.MultiTexCoord2(0, rect.xMax, rect.y);
							GL.MultiTexCoord2(1, rect2.xMax, rect2.y);
							GL.Vertex3((float)clippedPCPixels.xMax, (float)clippedPCPixels.y, 0f);
							GL.End();
							GL.PopMatrix();
							if (TerrainPaintUtility.paintTextureUsesCopyTexture)
							{
								RenderTexture temporary2 = RenderTexture.GetTemporary(new RenderTextureDescriptor(texture2D.width, texture2D.height, RenderTextureFormat.ARGB32)
								{
									sRGB = false,
									useMipMap = true,
									autoGenerateMips = false
								});
								if (!temporary2.IsCreated())
								{
									temporary2.Create();
								}
								Graphics.CopyTexture(texture2D, 0, 0, temporary2, 0, 0);
								Graphics.CopyTexture(temporary, 0, 0, clippedPCPixels.x, clippedPCPixels.y, clippedPCPixels.width, clippedPCPixels.height, temporary2, 0, 0, terrainTile.clippedLocalPixels.x, terrainTile.clippedLocalPixels.y);
								temporary2.GenerateMips();
								Graphics.CopyTexture(temporary2, texture2D);
								RenderTexture.ReleaseTemporary(temporary2);
							}
							else
							{
								GraphicsDeviceType graphicsDeviceType = SystemInfo.graphicsDeviceType;
								if (graphicsDeviceType == GraphicsDeviceType.Metal || graphicsDeviceType == GraphicsDeviceType.OpenGLCore)
								{
									texture2D.ReadPixels(new Rect((float)clippedPCPixels.x, (float)clippedPCPixels.y, (float)clippedPCPixels.width, (float)clippedPCPixels.height), terrainTile.clippedLocalPixels.x, terrainTile.clippedLocalPixels.y);
								}
								else
								{
									texture2D.ReadPixels(new Rect((float)clippedPCPixels.x, (float)(temporary.height - clippedPCPixels.y - clippedPCPixels.height), (float)clippedPCPixels.width, (float)clippedPCPixels.height), terrainTile.clippedLocalPixels.x, terrainTile.clippedLocalPixels.y);
								}
								texture2D.Apply();
							}
						}
					}
					RenderTexture.active = null;
					RenderTexture.ReleaseTemporary(temporary);
					PaintContext.OnTerrainPainted(terrainTile, PaintContext.ToolAction.PaintTexture);
				}
			}
		}

		// Token: 0x06000025 RID: 37 RVA: 0x00003914 File Offset: 0x00001B14
		private static void OnTerrainPainted(PaintContext.TerrainTile tile, PaintContext.ToolAction action)
		{
			for (int i = 0; i < PaintContext.s_PaintedTerrain.Count; i++)
			{
				if (tile.terrain == PaintContext.s_PaintedTerrain[i].terrain)
				{
					PaintContext.s_PaintedTerrain[i].action |= action;
					return;
				}
			}
			PaintContext.s_PaintedTerrain.Add(new PaintContext.PaintedTerrain
			{
				terrain = tile.terrain,
				action = action
			});
		}

		// Token: 0x06000026 RID: 38 RVA: 0x000039A4 File Offset: 0x00001BA4
		public static void ApplyDelayedActions()
		{
			int i = 0;
			while (i < PaintContext.s_PaintedTerrain.Count)
			{
				PaintContext.PaintedTerrain paintedTerrain = PaintContext.s_PaintedTerrain[i];
				if ((paintedTerrain.action & PaintContext.ToolAction.PaintHeightmap) != PaintContext.ToolAction.None)
				{
					paintedTerrain.terrain.ApplyDelayedHeightmapModification();
				}
				if ((paintedTerrain.action & PaintContext.ToolAction.PaintTexture) != PaintContext.ToolAction.None)
				{
					TerrainData terrainData = paintedTerrain.terrain.terrainData;
					if (!(terrainData == null))
					{
						terrainData.SetBaseMapDirty();
						if (TerrainPaintUtility.paintTextureUsesCopyTexture)
						{
							RenderTextureDescriptor desc = new RenderTextureDescriptor(terrainData.alphamapResolution, terrainData.alphamapResolution, RenderTextureFormat.ARGB32)
							{
								sRGB = false,
								useMipMap = false,
								autoGenerateMips = false
							};
							RenderTexture temporary = RenderTexture.GetTemporary(desc);
							for (int j = 0; j < terrainData.alphamapTextureCount; j++)
							{
								Graphics.Blit(terrainData.alphamapTextures[j], temporary);
								terrainData.alphamapTextures[j].ReadPixels(new Rect(0f, 0f, (float)desc.width, (float)desc.height), 0, 0, true);
							}
							RenderTexture.ReleaseTemporary(temporary);
						}
					}
				}
				IL_106:
				i++;
				continue;
				goto IL_106;
			}
			PaintContext.s_PaintedTerrain.Clear();
		}

		// Token: 0x06000027 RID: 39 RVA: 0x00003AD5 File Offset: 0x00001CD5
		// Note: this type is marked as 'beforefieldinit'.
		static PaintContext()
		{
		}

		// Token: 0x04000007 RID: 7
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private readonly Terrain <originTerrain>k__BackingField;

		// Token: 0x04000008 RID: 8
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private readonly RectInt <pixelRect>k__BackingField;

		// Token: 0x04000009 RID: 9
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private readonly int <targetTextureWidth>k__BackingField;

		// Token: 0x0400000A RID: 10
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private readonly int <targetTextureHeight>k__BackingField;

		// Token: 0x0400000B RID: 11
		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private readonly Vector2 <pixelSize>k__BackingField;

		// Token: 0x0400000C RID: 12
		private List<PaintContext.TerrainTile> m_TerrainTiles;

		// Token: 0x0400000D RID: 13
		private RenderTexture m_SourceRenderTexture;

		// Token: 0x0400000E RID: 14
		private RenderTexture m_DestinationRenderTexture;

		// Token: 0x0400000F RID: 15
		private RenderTexture m_OldRenderTexture;

		// Token: 0x04000010 RID: 16
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[CompilerGenerated]
		private static Action<PaintContext.TerrainTile, PaintContext.ToolAction, string> onTerrainTileBeforePaint;

		// Token: 0x04000011 RID: 17
		private static List<PaintContext.PaintedTerrain> s_PaintedTerrain = new List<PaintContext.PaintedTerrain>();

		// Token: 0x02000004 RID: 4
		internal class TerrainTile
		{
			// Token: 0x06000028 RID: 40 RVA: 0x00003AE1 File Offset: 0x00001CE1
			public TerrainTile()
			{
			}

			// Token: 0x06000029 RID: 41 RVA: 0x00003AEA File Offset: 0x00001CEA
			public TerrainTile(Terrain newTerrain, int tileOriginPixelsX, int tileOriginPixelsY)
			{
				this.terrain = newTerrain;
				this.tileOriginPixels = new Vector2Int(tileOriginPixelsX, tileOriginPixelsY);
			}

			// Token: 0x04000012 RID: 18
			public Terrain terrain;

			// Token: 0x04000013 RID: 19
			public Vector2Int tileOriginPixels;

			// Token: 0x04000014 RID: 20
			public RectInt clippedLocalPixels;

			// Token: 0x04000015 RID: 21
			public RectInt clippedPCPixels;

			// Token: 0x04000016 RID: 22
			public int mapIndex;

			// Token: 0x04000017 RID: 23
			public int channelIndex;
		}

		// Token: 0x02000005 RID: 5
		[Flags]
		internal enum ToolAction
		{
			// Token: 0x04000019 RID: 25
			None = 0,
			// Token: 0x0400001A RID: 26
			PaintHeightmap = 1,
			// Token: 0x0400001B RID: 27
			PaintTexture = 2
		}

		// Token: 0x02000006 RID: 6
		private class PaintedTerrain
		{
			// Token: 0x0600002A RID: 42 RVA: 0x00003B07 File Offset: 0x00001D07
			public PaintedTerrain()
			{
			}

			// Token: 0x0400001C RID: 28
			public Terrain terrain;

			// Token: 0x0400001D RID: 29
			public PaintContext.ToolAction action;
		}
	}
}
