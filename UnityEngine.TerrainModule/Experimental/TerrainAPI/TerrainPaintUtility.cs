﻿using System;
using UnityEngine.Rendering;

namespace UnityEngine.Experimental.TerrainAPI
{
	// Token: 0x02000017 RID: 23
	public static class TerrainPaintUtility
	{
		// Token: 0x06000165 RID: 357 RVA: 0x00004E14 File Offset: 0x00003014
		public static Material GetBuiltinPaintMaterial()
		{
			if (TerrainPaintUtility.s_BuiltinPaintMaterial == null)
			{
				TerrainPaintUtility.s_BuiltinPaintMaterial = new Material(Shader.Find("Hidden/TerrainEngine/PaintHeight"));
			}
			return TerrainPaintUtility.s_BuiltinPaintMaterial;
		}

		// Token: 0x06000166 RID: 358 RVA: 0x00004E54 File Offset: 0x00003054
		public static BrushTransform CalculateBrushTransform(Terrain terrain, Vector2 brushCenterTerrainUV, float brushSize, float brushRotationDegrees)
		{
			float f = brushRotationDegrees * 0.017453292f;
			float num = Mathf.Cos(f);
			float num2 = Mathf.Sin(f);
			Vector2 vector = new Vector2(num, -num2) * brushSize;
			Vector2 vector2 = new Vector2(num2, num) * brushSize;
			Vector3 size = terrain.terrainData.size;
			Vector2 a = brushCenterTerrainUV * new Vector2(size.x, size.z);
			Vector2 brushOrigin = a - 0.5f * vector - 0.5f * vector2;
			BrushTransform result = new BrushTransform(brushOrigin, vector, vector2);
			return result;
		}

		// Token: 0x06000167 RID: 359 RVA: 0x00004EFC File Offset: 0x000030FC
		public static void BuildTransformPaintContextUVToPaintContextUV(PaintContext src, PaintContext dst, out Vector4 scaleOffset)
		{
			float num = ((float)src.pixelRect.xMin - 0.5f) * src.pixelSize.x;
			float num2 = ((float)src.pixelRect.yMin - 0.5f) * src.pixelSize.y;
			float num3 = (float)src.pixelRect.width * src.pixelSize.x;
			float num4 = (float)src.pixelRect.height * src.pixelSize.y;
			float num5 = ((float)dst.pixelRect.xMin - 0.5f) * dst.pixelSize.x;
			float num6 = ((float)dst.pixelRect.yMin - 0.5f) * dst.pixelSize.y;
			float num7 = (float)dst.pixelRect.width * dst.pixelSize.x;
			float num8 = (float)dst.pixelRect.height * dst.pixelSize.y;
			scaleOffset = new Vector4(num3 / num7, num4 / num8, (num - num5) / num7, (num2 - num6) / num8);
		}

		// Token: 0x06000168 RID: 360 RVA: 0x0000504C File Offset: 0x0000324C
		public static void SetupTerrainToolMaterialProperties(PaintContext paintContext, BrushTransform brushXform, Material material)
		{
			float d = ((float)paintContext.pixelRect.xMin - 0.5f) * paintContext.pixelSize.x;
			float d2 = ((float)paintContext.pixelRect.yMin - 0.5f) * paintContext.pixelSize.y;
			float d3 = (float)paintContext.pixelRect.width * paintContext.pixelSize.x;
			float d4 = (float)paintContext.pixelRect.height * paintContext.pixelSize.y;
			Vector2 vector = d3 * brushXform.targetX;
			Vector2 vector2 = d4 * brushXform.targetY;
			Vector2 vector3 = brushXform.targetOrigin + d * brushXform.targetX + d2 * brushXform.targetY;
			material.SetVector("_PCUVToBrushUVScales", new Vector4(vector.x, vector.y, vector2.x, vector2.y));
			material.SetVector("_PCUVToBrushUVOffset", new Vector4(vector3.x, vector3.y, 0f, 0f));
		}

		// Token: 0x17000080 RID: 128
		// (get) Token: 0x06000169 RID: 361 RVA: 0x0000518C File Offset: 0x0000338C
		internal static bool paintTextureUsesCopyTexture
		{
			get
			{
				return (SystemInfo.copyTextureSupport & (CopyTextureSupport.TextureToRT | CopyTextureSupport.RTToTexture)) == (CopyTextureSupport.TextureToRT | CopyTextureSupport.RTToTexture);
			}
		}

		// Token: 0x0600016A RID: 362 RVA: 0x000051B0 File Offset: 0x000033B0
		private static PaintContext InitializePaintContext(Terrain terrain, Texture target, RenderTextureFormat pcFormat, Rect boundsInTerrainSpace, int extraBorderPixels = 0)
		{
			PaintContext paintContext = PaintContext.CreateFromBounds(terrain, boundsInTerrainSpace, target.width, target.height, extraBorderPixels);
			paintContext.CreateRenderTargets(pcFormat);
			return paintContext;
		}

		// Token: 0x0600016B RID: 363 RVA: 0x000051E3 File Offset: 0x000033E3
		public static void ReleaseContextResources(PaintContext ctx)
		{
			ctx.Cleanup(true);
		}

		// Token: 0x0600016C RID: 364 RVA: 0x000051F0 File Offset: 0x000033F0
		public static PaintContext BeginPaintHeightmap(Terrain terrain, Rect boundsInTerrainSpace, int extraBorderPixels = 0)
		{
			RenderTexture heightmapTexture = terrain.terrainData.heightmapTexture;
			PaintContext paintContext = TerrainPaintUtility.InitializePaintContext(terrain, heightmapTexture, heightmapTexture.format, boundsInTerrainSpace, extraBorderPixels);
			paintContext.GatherHeightmap();
			return paintContext;
		}

		// Token: 0x0600016D RID: 365 RVA: 0x00005228 File Offset: 0x00003428
		public static void EndPaintHeightmap(PaintContext ctx, string editorUndoName)
		{
			ctx.ScatterHeightmap(editorUndoName);
			ctx.Cleanup(true);
		}

		// Token: 0x0600016E RID: 366 RVA: 0x0000523C File Offset: 0x0000343C
		public static PaintContext CollectNormals(Terrain terrain, Rect boundsInTerrainSpace, int extraBorderPixels = 0)
		{
			RenderTexture normalmapTexture = terrain.normalmapTexture;
			PaintContext paintContext = TerrainPaintUtility.InitializePaintContext(terrain, normalmapTexture, normalmapTexture.format, boundsInTerrainSpace, extraBorderPixels);
			paintContext.GatherNormals();
			return paintContext;
		}

		// Token: 0x0600016F RID: 367 RVA: 0x00005270 File Offset: 0x00003470
		public static PaintContext BeginPaintTexture(Terrain terrain, Rect boundsInTerrainSpace, TerrainLayer inputLayer, int extraBorderPixels = 0)
		{
			PaintContext result;
			if (inputLayer == null)
			{
				result = null;
			}
			else
			{
				int num = TerrainPaintUtility.FindTerrainLayerIndex(terrain, inputLayer);
				if (num == -1)
				{
					num = TerrainPaintUtility.AddTerrainLayer(terrain, inputLayer);
				}
				Texture2D terrainAlphaMapChecked = TerrainPaintUtility.GetTerrainAlphaMapChecked(terrain, num >> 2);
				PaintContext paintContext = TerrainPaintUtility.InitializePaintContext(terrain, terrainAlphaMapChecked, RenderTextureFormat.R8, boundsInTerrainSpace, extraBorderPixels);
				paintContext.GatherAlphamap(inputLayer, true);
				result = paintContext;
			}
			return result;
		}

		// Token: 0x06000170 RID: 368 RVA: 0x000052CE File Offset: 0x000034CE
		public static void EndPaintTexture(PaintContext ctx, string editorUndoName)
		{
			ctx.ScatterAlphamap(editorUndoName);
			ctx.Cleanup(true);
		}

		// Token: 0x06000171 RID: 369 RVA: 0x000052E0 File Offset: 0x000034E0
		public static Material GetBlitMaterial()
		{
			if (!TerrainPaintUtility.m_BlitMaterial)
			{
				TerrainPaintUtility.m_BlitMaterial = new Material(Shader.Find("Hidden/BlitCopy"));
			}
			return TerrainPaintUtility.m_BlitMaterial;
		}

		// Token: 0x06000172 RID: 370 RVA: 0x00005320 File Offset: 0x00003520
		public static Material GetCopyTerrainLayerMaterial()
		{
			if (!TerrainPaintUtility.m_CopyTerrainLayerMaterial)
			{
				TerrainPaintUtility.m_CopyTerrainLayerMaterial = new Material(Shader.Find("Hidden/TerrainEngine/TerrainLayerUtils"));
			}
			return TerrainPaintUtility.m_CopyTerrainLayerMaterial;
		}

		// Token: 0x06000173 RID: 371 RVA: 0x00005360 File Offset: 0x00003560
		internal static void DrawQuad(RectInt destinationPixels, RectInt sourcePixels, Texture sourceTexture)
		{
			if (destinationPixels.width > 0 && destinationPixels.height > 0)
			{
				Rect rect = new Rect((float)sourcePixels.x / (float)sourceTexture.width, (float)sourcePixels.y / (float)sourceTexture.height, (float)sourcePixels.width / (float)sourceTexture.width, (float)sourcePixels.height / (float)sourceTexture.height);
				GL.Begin(7);
				GL.Color(new Color(1f, 1f, 1f, 1f));
				GL.TexCoord2(rect.x, rect.y);
				GL.Vertex3((float)destinationPixels.x, (float)destinationPixels.y, 0f);
				GL.TexCoord2(rect.x, rect.yMax);
				GL.Vertex3((float)destinationPixels.x, (float)destinationPixels.yMax, 0f);
				GL.TexCoord2(rect.xMax, rect.yMax);
				GL.Vertex3((float)destinationPixels.xMax, (float)destinationPixels.yMax, 0f);
				GL.TexCoord2(rect.xMax, rect.y);
				GL.Vertex3((float)destinationPixels.xMax, (float)destinationPixels.y, 0f);
				GL.End();
			}
		}

		// Token: 0x06000174 RID: 372 RVA: 0x000054B0 File Offset: 0x000036B0
		internal static RectInt CalcPixelRectFromBounds(Terrain terrain, Rect boundsInTerrainSpace, int textureWidth, int textureHeight, int extraBorderPixels)
		{
			float num = ((float)textureWidth - 1f) / terrain.terrainData.size.x;
			float num2 = ((float)textureHeight - 1f) / terrain.terrainData.size.z;
			int num3 = Mathf.FloorToInt(boundsInTerrainSpace.xMin * num) - extraBorderPixels;
			int num4 = Mathf.FloorToInt(boundsInTerrainSpace.yMin * num2) - extraBorderPixels;
			int num5 = Mathf.CeilToInt(boundsInTerrainSpace.xMax * num) + extraBorderPixels;
			int num6 = Mathf.CeilToInt(boundsInTerrainSpace.yMax * num2) + extraBorderPixels;
			return new RectInt(num3, num4, num5 - num3 + 1, num6 - num4 + 1);
		}

		// Token: 0x06000175 RID: 373 RVA: 0x00005564 File Offset: 0x00003764
		public static Texture2D GetTerrainAlphaMapChecked(Terrain terrain, int mapIndex)
		{
			if (mapIndex >= terrain.terrainData.alphamapTextureCount)
			{
				throw new ArgumentException("Trying to access out-of-bounds terrain alphamap information.");
			}
			return terrain.terrainData.alphamapTextures[mapIndex];
		}

		// Token: 0x06000176 RID: 374 RVA: 0x000055A4 File Offset: 0x000037A4
		public static int FindTerrainLayerIndex(Terrain terrain, TerrainLayer inputLayer)
		{
			for (int i = 0; i < terrain.terrainData.terrainLayers.Length; i++)
			{
				if (terrain.terrainData.terrainLayers[i] == inputLayer)
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x06000177 RID: 375 RVA: 0x000055FC File Offset: 0x000037FC
		internal static int AddTerrainLayer(Terrain terrain, TerrainLayer inputLayer)
		{
			int num = terrain.terrainData.terrainLayers.Length;
			TerrainLayer[] array = new TerrainLayer[num + 1];
			Array.Copy(terrain.terrainData.terrainLayers, 0, array, 0, num);
			array[num] = inputLayer;
			terrain.terrainData.terrainLayers = array;
			return num;
		}

		// Token: 0x06000178 RID: 376 RVA: 0x0000564D File Offset: 0x0000384D
		// Note: this type is marked as 'beforefieldinit'.
		static TerrainPaintUtility()
		{
		}

		// Token: 0x0400006C RID: 108
		private static Material s_BuiltinPaintMaterial = null;

		// Token: 0x0400006D RID: 109
		private static Material m_BlitMaterial = null;

		// Token: 0x0400006E RID: 110
		private static Material m_CopyTerrainLayerMaterial = null;

		// Token: 0x02000018 RID: 24
		public enum BuiltinPaintMaterialPasses
		{
			// Token: 0x04000070 RID: 112
			RaiseLowerHeight,
			// Token: 0x04000071 RID: 113
			StampHeight,
			// Token: 0x04000072 RID: 114
			SetHeights,
			// Token: 0x04000073 RID: 115
			SmoothHeights,
			// Token: 0x04000074 RID: 116
			PaintTexture
		}
	}
}
