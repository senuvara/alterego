﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.TerrainAPI
{
	// Token: 0x02000019 RID: 25
	public static class TerrainUtility
	{
		// Token: 0x06000179 RID: 377 RVA: 0x00005664 File Offset: 0x00003864
		internal static bool HasValidTerrains()
		{
			return Terrain.activeTerrains != null && Terrain.activeTerrains.Length > 0;
		}

		// Token: 0x0600017A RID: 378 RVA: 0x00005690 File Offset: 0x00003890
		internal static void ClearConnectivity()
		{
			foreach (Terrain terrain in Terrain.activeTerrains)
			{
				terrain.SetNeighbors(null, null, null, null);
			}
		}

		// Token: 0x0600017B RID: 379 RVA: 0x000056C8 File Offset: 0x000038C8
		internal static TerrainUtility.TerrainGroups CollectTerrains(bool onlyAutoConnectedTerrains = true)
		{
			TerrainUtility.TerrainGroups result;
			if (!TerrainUtility.HasValidTerrains())
			{
				result = null;
			}
			else
			{
				TerrainUtility.TerrainGroups terrainGroups = new TerrainUtility.TerrainGroups();
				Terrain[] activeTerrains = Terrain.activeTerrains;
				for (int i = 0; i < activeTerrains.Length; i++)
				{
					Terrain t = activeTerrains[i];
					if (!onlyAutoConnectedTerrains || t.allowAutoConnect)
					{
						if (!terrainGroups.ContainsKey(t.groupingID))
						{
							TerrainUtility.TerrainMap terrainMap = TerrainUtility.TerrainMap.CreateFromPlacement(t, (Terrain x) => x.groupingID == t.groupingID && (!onlyAutoConnectedTerrains || x.allowAutoConnect), true);
							if (terrainMap != null)
							{
								terrainGroups.Add(t.groupingID, terrainMap);
							}
						}
					}
				}
				result = ((terrainGroups.Count == 0) ? null : terrainGroups);
			}
			return result;
		}

		// Token: 0x0600017C RID: 380 RVA: 0x000057C0 File Offset: 0x000039C0
		[RequiredByNativeCode]
		public static void AutoConnect()
		{
			if (TerrainUtility.HasValidTerrains())
			{
				TerrainUtility.ClearConnectivity();
				TerrainUtility.TerrainGroups terrainGroups = TerrainUtility.CollectTerrains(true);
				if (terrainGroups != null)
				{
					foreach (KeyValuePair<int, TerrainUtility.TerrainMap> keyValuePair in terrainGroups)
					{
						int key = keyValuePair.Key;
						TerrainUtility.TerrainMap value = keyValuePair.Value;
						foreach (KeyValuePair<TerrainUtility.TerrainMap.TileCoord, Terrain> keyValuePair2 in value.m_terrainTiles)
						{
							TerrainUtility.TerrainMap.TileCoord key2 = keyValuePair2.Key;
							Terrain terrain = value.GetTerrain(key2.tileX, key2.tileZ);
							Terrain terrain2 = value.GetTerrain(key2.tileX - 1, key2.tileZ);
							Terrain terrain3 = value.GetTerrain(key2.tileX + 1, key2.tileZ);
							Terrain terrain4 = value.GetTerrain(key2.tileX, key2.tileZ + 1);
							Terrain terrain5 = value.GetTerrain(key2.tileX, key2.tileZ - 1);
							terrain.SetNeighbors(terrain2, terrain4, terrain3, terrain5);
						}
					}
				}
			}
		}

		// Token: 0x0200001A RID: 26
		public class TerrainMap
		{
			// Token: 0x0600017D RID: 381 RVA: 0x00005940 File Offset: 0x00003B40
			public TerrainMap()
			{
				this.m_errorCode = TerrainUtility.TerrainMap.ErrorCode.OK;
				this.m_terrainTiles = new Dictionary<TerrainUtility.TerrainMap.TileCoord, Terrain>();
			}

			// Token: 0x0600017E RID: 382 RVA: 0x0000595C File Offset: 0x00003B5C
			public Terrain GetTerrain(int tileX, int tileZ)
			{
				Terrain result = null;
				this.m_terrainTiles.TryGetValue(new TerrainUtility.TerrainMap.TileCoord(tileX, tileZ), out result);
				return result;
			}

			// Token: 0x0600017F RID: 383 RVA: 0x0000598C File Offset: 0x00003B8C
			public static TerrainUtility.TerrainMap CreateFromPlacement(Terrain originTerrain, TerrainUtility.TerrainMap.TerrainFilter filter = null, bool fullValidation = true)
			{
				TerrainUtility.TerrainMap result;
				if (Terrain.activeTerrains == null || Terrain.activeTerrains.Length == 0 || originTerrain == null)
				{
					result = null;
				}
				else if (originTerrain.terrainData == null)
				{
					result = null;
				}
				else
				{
					int groupID = originTerrain.groupingID;
					float x3 = originTerrain.transform.position.x;
					float z = originTerrain.transform.position.z;
					float x2 = originTerrain.terrainData.size.x;
					float z2 = originTerrain.terrainData.size.z;
					if (filter == null)
					{
						filter = ((Terrain x) => x.groupingID == groupID);
					}
					result = TerrainUtility.TerrainMap.CreateFromPlacement(new Vector2(x3, z), new Vector2(x2, z2), filter, fullValidation);
				}
				return result;
			}

			// Token: 0x06000180 RID: 384 RVA: 0x00005A78 File Offset: 0x00003C78
			public static TerrainUtility.TerrainMap CreateFromPlacement(Vector2 gridOrigin, Vector2 gridSize, TerrainUtility.TerrainMap.TerrainFilter filter = null, bool fullValidation = true)
			{
				TerrainUtility.TerrainMap result;
				if (Terrain.activeTerrains == null || Terrain.activeTerrains.Length == 0)
				{
					result = null;
				}
				else
				{
					TerrainUtility.TerrainMap terrainMap = new TerrainUtility.TerrainMap();
					float num = 1f / gridSize.x;
					float num2 = 1f / gridSize.y;
					foreach (Terrain terrain in Terrain.activeTerrains)
					{
						if (!(terrain.terrainData == null))
						{
							if (filter == null || filter(terrain))
							{
								Vector3 position = terrain.transform.position;
								int tileX = Mathf.RoundToInt((position.x - gridOrigin.x) * num);
								int tileZ = Mathf.RoundToInt((position.z - gridOrigin.y) * num2);
								terrainMap.TryToAddTerrain(tileX, tileZ, terrain);
							}
						}
					}
					if (fullValidation)
					{
						terrainMap.Validate();
					}
					result = ((terrainMap.m_terrainTiles.Count <= 0) ? null : terrainMap);
				}
				return result;
			}

			// Token: 0x06000181 RID: 385 RVA: 0x00005B94 File Offset: 0x00003D94
			private void AddTerrainInternal(int x, int z, Terrain terrain)
			{
				if (this.m_terrainTiles.Count == 0)
				{
					this.m_patchSize = terrain.terrainData.size;
				}
				else if (terrain.terrainData.size != this.m_patchSize)
				{
					this.m_errorCode |= TerrainUtility.TerrainMap.ErrorCode.SizeMismatch;
				}
				this.m_terrainTiles.Add(new TerrainUtility.TerrainMap.TileCoord(x, z), terrain);
			}

			// Token: 0x06000182 RID: 386 RVA: 0x00005C08 File Offset: 0x00003E08
			private bool TryToAddTerrain(int tileX, int tileZ, Terrain terrain)
			{
				bool result = false;
				if (terrain != null)
				{
					Terrain terrain2 = this.GetTerrain(tileX, tileZ);
					if (terrain2 != null)
					{
						if (terrain2 != terrain)
						{
							this.m_errorCode |= TerrainUtility.TerrainMap.ErrorCode.Overlapping;
						}
					}
					else
					{
						this.AddTerrainInternal(tileX, tileZ, terrain);
						result = true;
					}
				}
				return result;
			}

			// Token: 0x06000183 RID: 387 RVA: 0x00005C74 File Offset: 0x00003E74
			private void ValidateTerrain(int tileX, int tileZ)
			{
				Terrain terrain = this.GetTerrain(tileX, tileZ);
				if (terrain != null)
				{
					Terrain terrain2 = this.GetTerrain(tileX - 1, tileZ);
					Terrain terrain3 = this.GetTerrain(tileX + 1, tileZ);
					Terrain terrain4 = this.GetTerrain(tileX, tileZ + 1);
					Terrain terrain5 = this.GetTerrain(tileX, tileZ - 1);
					if (terrain2)
					{
						if (!Mathf.Approximately(terrain.transform.position.x, terrain2.transform.position.x + terrain2.terrainData.size.x) || !Mathf.Approximately(terrain.transform.position.z, terrain2.transform.position.z))
						{
							this.m_errorCode |= TerrainUtility.TerrainMap.ErrorCode.EdgeAlignmentMismatch;
						}
					}
					if (terrain3)
					{
						if (!Mathf.Approximately(terrain.transform.position.x + terrain.terrainData.size.x, terrain3.transform.position.x) || !Mathf.Approximately(terrain.transform.position.z, terrain3.transform.position.z))
						{
							this.m_errorCode |= TerrainUtility.TerrainMap.ErrorCode.EdgeAlignmentMismatch;
						}
					}
					if (terrain4)
					{
						if (!Mathf.Approximately(terrain.transform.position.x, terrain4.transform.position.x) || !Mathf.Approximately(terrain.transform.position.z + terrain.terrainData.size.z, terrain4.transform.position.z))
						{
							this.m_errorCode |= TerrainUtility.TerrainMap.ErrorCode.EdgeAlignmentMismatch;
						}
					}
					if (terrain5)
					{
						if (!Mathf.Approximately(terrain.transform.position.x, terrain5.transform.position.x) || !Mathf.Approximately(terrain.transform.position.z, terrain5.transform.position.z + terrain5.terrainData.size.z))
						{
							this.m_errorCode |= TerrainUtility.TerrainMap.ErrorCode.EdgeAlignmentMismatch;
						}
					}
				}
			}

			// Token: 0x06000184 RID: 388 RVA: 0x00005F24 File Offset: 0x00004124
			private TerrainUtility.TerrainMap.ErrorCode Validate()
			{
				foreach (TerrainUtility.TerrainMap.TileCoord tileCoord in this.m_terrainTiles.Keys)
				{
					this.ValidateTerrain(tileCoord.tileX, tileCoord.tileZ);
				}
				return this.m_errorCode;
			}

			// Token: 0x04000075 RID: 117
			private Vector3 m_patchSize;

			// Token: 0x04000076 RID: 118
			public TerrainUtility.TerrainMap.ErrorCode m_errorCode;

			// Token: 0x04000077 RID: 119
			public Dictionary<TerrainUtility.TerrainMap.TileCoord, Terrain> m_terrainTiles;

			// Token: 0x0200001B RID: 27
			// (Invoke) Token: 0x06000186 RID: 390
			public delegate bool TerrainFilter(Terrain terrain);

			// Token: 0x0200001C RID: 28
			public struct TileCoord
			{
				// Token: 0x06000189 RID: 393 RVA: 0x00005FA4 File Offset: 0x000041A4
				public TileCoord(int tileX, int tileZ)
				{
					this.tileX = tileX;
					this.tileZ = tileZ;
				}

				// Token: 0x04000078 RID: 120
				public readonly int tileX;

				// Token: 0x04000079 RID: 121
				public readonly int tileZ;
			}

			// Token: 0x0200001D RID: 29
			public enum ErrorCode
			{
				// Token: 0x0400007B RID: 123
				OK,
				// Token: 0x0400007C RID: 124
				Overlapping,
				// Token: 0x0400007D RID: 125
				SizeMismatch = 4,
				// Token: 0x0400007E RID: 126
				EdgeAlignmentMismatch = 8
			}

			// Token: 0x0200001E RID: 30
			[CompilerGenerated]
			private sealed class <CreateFromPlacement>c__AnonStorey0
			{
				// Token: 0x0600018A RID: 394 RVA: 0x00003B07 File Offset: 0x00001D07
				public <CreateFromPlacement>c__AnonStorey0()
				{
				}

				// Token: 0x0600018B RID: 395 RVA: 0x00005FB8 File Offset: 0x000041B8
				internal bool <>m__0(Terrain x)
				{
					return x.groupingID == this.groupID;
				}

				// Token: 0x0400007F RID: 127
				internal int groupID;
			}
		}

		// Token: 0x0200001F RID: 31
		public class TerrainGroups : Dictionary<int, TerrainUtility.TerrainMap>
		{
			// Token: 0x0600018C RID: 396 RVA: 0x00005FDA File Offset: 0x000041DA
			public TerrainGroups()
			{
			}
		}

		// Token: 0x02000020 RID: 32
		[CompilerGenerated]
		private sealed class <CollectTerrains>c__AnonStorey1
		{
			// Token: 0x0600018D RID: 397 RVA: 0x00003B07 File Offset: 0x00001D07
			public <CollectTerrains>c__AnonStorey1()
			{
			}

			// Token: 0x04000080 RID: 128
			internal bool onlyAutoConnectedTerrains;
		}

		// Token: 0x02000021 RID: 33
		[CompilerGenerated]
		private sealed class <CollectTerrains>c__AnonStorey0
		{
			// Token: 0x0600018E RID: 398 RVA: 0x00003B07 File Offset: 0x00001D07
			public <CollectTerrains>c__AnonStorey0()
			{
			}

			// Token: 0x0600018F RID: 399 RVA: 0x00005FE4 File Offset: 0x000041E4
			internal bool <>m__0(Terrain x)
			{
				return x.groupingID == this.t.groupingID && (!this.<>f__ref$1.onlyAutoConnectedTerrains || x.allowAutoConnect);
			}

			// Token: 0x04000081 RID: 129
			internal Terrain t;

			// Token: 0x04000082 RID: 130
			internal TerrainUtility.<CollectTerrains>c__AnonStorey1 <>f__ref$1;
		}
	}
}
