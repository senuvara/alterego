﻿using System;

namespace UnityEngine
{
	// Token: 0x0200000F RID: 15
	public enum DetailRenderMode
	{
		// Token: 0x04000036 RID: 54
		GrassBillboard,
		// Token: 0x04000037 RID: 55
		VertexLit,
		// Token: 0x04000038 RID: 56
		Grass
	}
}
