﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000016 RID: 22
	[NativeHeader("TerrainScriptingClasses.h")]
	[UsedByNativeCode]
	[NativeHeader("Modules/Terrain/Public/TerrainLayerScriptingInterface.h")]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class TerrainLayer : Object
	{
		// Token: 0x0600013B RID: 315 RVA: 0x00004D25 File Offset: 0x00002F25
		public TerrainLayer()
		{
			TerrainLayer.Internal_Create(this);
		}

		// Token: 0x0600013C RID: 316
		[FreeFunction("TerrainLayerScriptingInterface::Create")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Create([Writable] TerrainLayer layer);

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x0600013D RID: 317
		// (set) Token: 0x0600013E RID: 318
		public extern Texture2D diffuseTexture { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x0600013F RID: 319
		// (set) Token: 0x06000140 RID: 320
		public extern Texture2D normalMapTexture { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x06000141 RID: 321
		// (set) Token: 0x06000142 RID: 322
		public extern Texture2D maskMapTexture { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x06000143 RID: 323 RVA: 0x00004D34 File Offset: 0x00002F34
		// (set) Token: 0x06000144 RID: 324 RVA: 0x00004D4A File Offset: 0x00002F4A
		public Vector2 tileSize
		{
			get
			{
				Vector2 result;
				this.get_tileSize_Injected(out result);
				return result;
			}
			set
			{
				this.set_tileSize_Injected(ref value);
			}
		}

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x06000145 RID: 325 RVA: 0x00004D54 File Offset: 0x00002F54
		// (set) Token: 0x06000146 RID: 326 RVA: 0x00004D6A File Offset: 0x00002F6A
		public Vector2 tileOffset
		{
			get
			{
				Vector2 result;
				this.get_tileOffset_Injected(out result);
				return result;
			}
			set
			{
				this.set_tileOffset_Injected(ref value);
			}
		}

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x06000147 RID: 327 RVA: 0x00004D74 File Offset: 0x00002F74
		// (set) Token: 0x06000148 RID: 328 RVA: 0x00004D8A File Offset: 0x00002F8A
		[NativeProperty("SpecularColor")]
		public Color specular
		{
			get
			{
				Color result;
				this.get_specular_Injected(out result);
				return result;
			}
			set
			{
				this.set_specular_Injected(ref value);
			}
		}

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x06000149 RID: 329
		// (set) Token: 0x0600014A RID: 330
		public extern float metallic { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x0600014B RID: 331
		// (set) Token: 0x0600014C RID: 332
		public extern float smoothness { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x0600014D RID: 333
		// (set) Token: 0x0600014E RID: 334
		public extern float normalScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x0600014F RID: 335 RVA: 0x00004D94 File Offset: 0x00002F94
		// (set) Token: 0x06000150 RID: 336 RVA: 0x00004DAA File Offset: 0x00002FAA
		public Vector4 diffuseRemapMin
		{
			get
			{
				Vector4 result;
				this.get_diffuseRemapMin_Injected(out result);
				return result;
			}
			set
			{
				this.set_diffuseRemapMin_Injected(ref value);
			}
		}

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x06000151 RID: 337 RVA: 0x00004DB4 File Offset: 0x00002FB4
		// (set) Token: 0x06000152 RID: 338 RVA: 0x00004DCA File Offset: 0x00002FCA
		public Vector4 diffuseRemapMax
		{
			get
			{
				Vector4 result;
				this.get_diffuseRemapMax_Injected(out result);
				return result;
			}
			set
			{
				this.set_diffuseRemapMax_Injected(ref value);
			}
		}

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x06000153 RID: 339 RVA: 0x00004DD4 File Offset: 0x00002FD4
		// (set) Token: 0x06000154 RID: 340 RVA: 0x00004DEA File Offset: 0x00002FEA
		public Vector4 maskMapRemapMin
		{
			get
			{
				Vector4 result;
				this.get_maskMapRemapMin_Injected(out result);
				return result;
			}
			set
			{
				this.set_maskMapRemapMin_Injected(ref value);
			}
		}

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x06000155 RID: 341 RVA: 0x00004DF4 File Offset: 0x00002FF4
		// (set) Token: 0x06000156 RID: 342 RVA: 0x00004E0A File Offset: 0x0000300A
		public Vector4 maskMapRemapMax
		{
			get
			{
				Vector4 result;
				this.get_maskMapRemapMax_Injected(out result);
				return result;
			}
			set
			{
				this.set_maskMapRemapMax_Injected(ref value);
			}
		}

		// Token: 0x06000157 RID: 343
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_tileSize_Injected(out Vector2 ret);

		// Token: 0x06000158 RID: 344
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_tileSize_Injected(ref Vector2 value);

		// Token: 0x06000159 RID: 345
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_tileOffset_Injected(out Vector2 ret);

		// Token: 0x0600015A RID: 346
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_tileOffset_Injected(ref Vector2 value);

		// Token: 0x0600015B RID: 347
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_specular_Injected(out Color ret);

		// Token: 0x0600015C RID: 348
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_specular_Injected(ref Color value);

		// Token: 0x0600015D RID: 349
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_diffuseRemapMin_Injected(out Vector4 ret);

		// Token: 0x0600015E RID: 350
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_diffuseRemapMin_Injected(ref Vector4 value);

		// Token: 0x0600015F RID: 351
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_diffuseRemapMax_Injected(out Vector4 ret);

		// Token: 0x06000160 RID: 352
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_diffuseRemapMax_Injected(ref Vector4 value);

		// Token: 0x06000161 RID: 353
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_maskMapRemapMin_Injected(out Vector4 ret);

		// Token: 0x06000162 RID: 354
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_maskMapRemapMin_Injected(ref Vector4 value);

		// Token: 0x06000163 RID: 355
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_maskMapRemapMax_Injected(out Vector4 ret);

		// Token: 0x06000164 RID: 356
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_maskMapRemapMax_Injected(ref Vector4 value);
	}
}
