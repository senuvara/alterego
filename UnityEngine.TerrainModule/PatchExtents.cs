﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000013 RID: 19
	[UsedByNativeCode]
	public struct PatchExtents
	{
		// Token: 0x17000050 RID: 80
		// (get) Token: 0x060000CC RID: 204 RVA: 0x00004540 File Offset: 0x00002740
		// (set) Token: 0x060000CD RID: 205 RVA: 0x0000455B File Offset: 0x0000275B
		public float min
		{
			get
			{
				return this.m_min;
			}
			set
			{
				this.m_min = value;
			}
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x060000CE RID: 206 RVA: 0x00004568 File Offset: 0x00002768
		// (set) Token: 0x060000CF RID: 207 RVA: 0x00004583 File Offset: 0x00002783
		public float max
		{
			get
			{
				return this.m_max;
			}
			set
			{
				this.m_max = value;
			}
		}

		// Token: 0x04000053 RID: 83
		internal float m_min;

		// Token: 0x04000054 RID: 84
		internal float m_max;
	}
}
