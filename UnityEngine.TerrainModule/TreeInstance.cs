﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000012 RID: 18
	[UsedByNativeCode]
	public struct TreeInstance
	{
		// Token: 0x0400004B RID: 75
		public Vector3 position;

		// Token: 0x0400004C RID: 76
		public float widthScale;

		// Token: 0x0400004D RID: 77
		public float heightScale;

		// Token: 0x0400004E RID: 78
		public float rotation;

		// Token: 0x0400004F RID: 79
		public Color32 color;

		// Token: 0x04000050 RID: 80
		public Color32 lightmapColor;

		// Token: 0x04000051 RID: 81
		public int prototypeIndex;

		// Token: 0x04000052 RID: 82
		internal float temporaryDistance;
	}
}
