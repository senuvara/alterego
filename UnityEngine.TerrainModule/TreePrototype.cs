﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200000E RID: 14
	[UsedByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class TreePrototype
	{
		// Token: 0x06000097 RID: 151 RVA: 0x00003AE1 File Offset: 0x00001CE1
		public TreePrototype()
		{
		}

		// Token: 0x06000098 RID: 152 RVA: 0x00003CE4 File Offset: 0x00001EE4
		public TreePrototype(TreePrototype other)
		{
			this.prefab = other.prefab;
			this.bendFactor = other.bendFactor;
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x06000099 RID: 153 RVA: 0x00003D08 File Offset: 0x00001F08
		// (set) Token: 0x0600009A RID: 154 RVA: 0x00003D23 File Offset: 0x00001F23
		public GameObject prefab
		{
			get
			{
				return this.m_Prefab;
			}
			set
			{
				this.m_Prefab = value;
			}
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x0600009B RID: 155 RVA: 0x00003D30 File Offset: 0x00001F30
		// (set) Token: 0x0600009C RID: 156 RVA: 0x00003D4B File Offset: 0x00001F4B
		public float bendFactor
		{
			get
			{
				return this.m_BendFactor;
			}
			set
			{
				this.m_BendFactor = value;
			}
		}

		// Token: 0x0600009D RID: 157 RVA: 0x00003D58 File Offset: 0x00001F58
		public override bool Equals(object obj)
		{
			return this.Equals(obj as TreePrototype);
		}

		// Token: 0x0600009E RID: 158 RVA: 0x00003D7C File Offset: 0x00001F7C
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x0600009F RID: 159 RVA: 0x00003D98 File Offset: 0x00001F98
		private bool Equals(TreePrototype other)
		{
			bool result;
			if (object.ReferenceEquals(other, null))
			{
				result = false;
			}
			else if (object.ReferenceEquals(other, this))
			{
				result = true;
			}
			else if (base.GetType() != other.GetType())
			{
				result = false;
			}
			else
			{
				bool flag = this.prefab == other.prefab && this.bendFactor == other.bendFactor;
				result = flag;
			}
			return result;
		}

		// Token: 0x04000033 RID: 51
		internal GameObject m_Prefab;

		// Token: 0x04000034 RID: 52
		internal float m_BendFactor;
	}
}
