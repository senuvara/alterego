﻿using System;

namespace UnityEngine
{
	// Token: 0x02000008 RID: 8
	[Flags]
	public enum TerrainRenderFlags
	{
		// Token: 0x04000026 RID: 38
		[Obsolete("TerrainRenderFlags.heightmap is obsolete, use TerrainRenderFlags.Heightmap instead. (UnityUpgradable) -> Heightmap")]
		heightmap = 1,
		// Token: 0x04000027 RID: 39
		[Obsolete("TerrainRenderFlags.trees is obsolete, use TerrainRenderFlags.Trees instead. (UnityUpgradable) -> Trees")]
		trees = 2,
		// Token: 0x04000028 RID: 40
		[Obsolete("TerrainRenderFlags.details is obsolete, use TerrainRenderFlags.Details instead. (UnityUpgradable) -> Details")]
		details = 4,
		// Token: 0x04000029 RID: 41
		[Obsolete("TerrainRenderFlags.all is obsolete, use TerrainRenderFlags.All instead. (UnityUpgradable) -> All")]
		all = 7,
		// Token: 0x0400002A RID: 42
		Heightmap = 1,
		// Token: 0x0400002B RID: 43
		Trees = 2,
		// Token: 0x0400002C RID: 44
		Details = 4,
		// Token: 0x0400002D RID: 45
		All = 7
	}
}
