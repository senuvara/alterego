﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000010 RID: 16
	[UsedByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class DetailPrototype
	{
		// Token: 0x060000A0 RID: 160 RVA: 0x00003E14 File Offset: 0x00002014
		public DetailPrototype()
		{
		}

		// Token: 0x060000A1 RID: 161 RVA: 0x00003EC4 File Offset: 0x000020C4
		public DetailPrototype(DetailPrototype other)
		{
			this.m_Prototype = other.m_Prototype;
			this.m_PrototypeTexture = other.m_PrototypeTexture;
			this.m_HealthyColor = other.m_HealthyColor;
			this.m_DryColor = other.m_DryColor;
			this.m_MinWidth = other.m_MinWidth;
			this.m_MaxWidth = other.m_MaxWidth;
			this.m_MinHeight = other.m_MinHeight;
			this.m_MaxHeight = other.m_MaxHeight;
			this.m_NoiseSpread = other.m_NoiseSpread;
			this.m_BendFactor = other.m_BendFactor;
			this.m_RenderMode = other.m_RenderMode;
			this.m_UsePrototypeMesh = other.m_UsePrototypeMesh;
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x060000A2 RID: 162 RVA: 0x00004004 File Offset: 0x00002204
		// (set) Token: 0x060000A3 RID: 163 RVA: 0x0000401F File Offset: 0x0000221F
		public GameObject prototype
		{
			get
			{
				return this.m_Prototype;
			}
			set
			{
				this.m_Prototype = value;
			}
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x060000A4 RID: 164 RVA: 0x0000402C File Offset: 0x0000222C
		// (set) Token: 0x060000A5 RID: 165 RVA: 0x00004047 File Offset: 0x00002247
		public Texture2D prototypeTexture
		{
			get
			{
				return this.m_PrototypeTexture;
			}
			set
			{
				this.m_PrototypeTexture = value;
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x060000A6 RID: 166 RVA: 0x00004054 File Offset: 0x00002254
		// (set) Token: 0x060000A7 RID: 167 RVA: 0x0000406F File Offset: 0x0000226F
		public float minWidth
		{
			get
			{
				return this.m_MinWidth;
			}
			set
			{
				this.m_MinWidth = value;
			}
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x060000A8 RID: 168 RVA: 0x0000407C File Offset: 0x0000227C
		// (set) Token: 0x060000A9 RID: 169 RVA: 0x00004097 File Offset: 0x00002297
		public float maxWidth
		{
			get
			{
				return this.m_MaxWidth;
			}
			set
			{
				this.m_MaxWidth = value;
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x060000AA RID: 170 RVA: 0x000040A4 File Offset: 0x000022A4
		// (set) Token: 0x060000AB RID: 171 RVA: 0x000040BF File Offset: 0x000022BF
		public float minHeight
		{
			get
			{
				return this.m_MinHeight;
			}
			set
			{
				this.m_MinHeight = value;
			}
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x060000AC RID: 172 RVA: 0x000040CC File Offset: 0x000022CC
		// (set) Token: 0x060000AD RID: 173 RVA: 0x000040E7 File Offset: 0x000022E7
		public float maxHeight
		{
			get
			{
				return this.m_MaxHeight;
			}
			set
			{
				this.m_MaxHeight = value;
			}
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x060000AE RID: 174 RVA: 0x000040F4 File Offset: 0x000022F4
		// (set) Token: 0x060000AF RID: 175 RVA: 0x0000410F File Offset: 0x0000230F
		public float noiseSpread
		{
			get
			{
				return this.m_NoiseSpread;
			}
			set
			{
				this.m_NoiseSpread = value;
			}
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x060000B0 RID: 176 RVA: 0x0000411C File Offset: 0x0000231C
		// (set) Token: 0x060000B1 RID: 177 RVA: 0x00004137 File Offset: 0x00002337
		public float bendFactor
		{
			get
			{
				return this.m_BendFactor;
			}
			set
			{
				this.m_BendFactor = value;
			}
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x060000B2 RID: 178 RVA: 0x00004144 File Offset: 0x00002344
		// (set) Token: 0x060000B3 RID: 179 RVA: 0x0000415F File Offset: 0x0000235F
		public Color healthyColor
		{
			get
			{
				return this.m_HealthyColor;
			}
			set
			{
				this.m_HealthyColor = value;
			}
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x060000B4 RID: 180 RVA: 0x0000416C File Offset: 0x0000236C
		// (set) Token: 0x060000B5 RID: 181 RVA: 0x00004187 File Offset: 0x00002387
		public Color dryColor
		{
			get
			{
				return this.m_DryColor;
			}
			set
			{
				this.m_DryColor = value;
			}
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x060000B6 RID: 182 RVA: 0x00004194 File Offset: 0x00002394
		// (set) Token: 0x060000B7 RID: 183 RVA: 0x000041AF File Offset: 0x000023AF
		public DetailRenderMode renderMode
		{
			get
			{
				return (DetailRenderMode)this.m_RenderMode;
			}
			set
			{
				this.m_RenderMode = (int)value;
			}
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x060000B8 RID: 184 RVA: 0x000041BC File Offset: 0x000023BC
		// (set) Token: 0x060000B9 RID: 185 RVA: 0x000041DD File Offset: 0x000023DD
		public bool usePrototypeMesh
		{
			get
			{
				return this.m_UsePrototypeMesh != 0;
			}
			set
			{
				this.m_UsePrototypeMesh = ((!value) ? 0 : 1);
			}
		}

		// Token: 0x060000BA RID: 186 RVA: 0x000041F4 File Offset: 0x000023F4
		public override bool Equals(object obj)
		{
			return this.Equals(obj as DetailPrototype);
		}

		// Token: 0x060000BB RID: 187 RVA: 0x00004218 File Offset: 0x00002418
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x060000BC RID: 188 RVA: 0x00004234 File Offset: 0x00002434
		private bool Equals(DetailPrototype other)
		{
			bool result;
			if (object.ReferenceEquals(other, null))
			{
				result = false;
			}
			else if (object.ReferenceEquals(other, this))
			{
				result = true;
			}
			else if (base.GetType() != other.GetType())
			{
				result = false;
			}
			else
			{
				bool flag = this.m_Prototype == other.m_Prototype && this.m_PrototypeTexture == other.m_PrototypeTexture && this.m_HealthyColor == other.m_HealthyColor && this.m_DryColor == other.m_DryColor && this.m_MinWidth == other.m_MinWidth && this.m_MaxWidth == other.m_MaxWidth && this.m_MinHeight == other.m_MinHeight && this.m_MaxHeight == other.m_MaxHeight && this.m_NoiseSpread == other.m_NoiseSpread && this.m_BendFactor == other.m_BendFactor && this.m_RenderMode == other.m_RenderMode && this.m_UsePrototypeMesh == other.m_UsePrototypeMesh;
				result = flag;
			}
			return result;
		}

		// Token: 0x04000039 RID: 57
		internal GameObject m_Prototype = null;

		// Token: 0x0400003A RID: 58
		internal Texture2D m_PrototypeTexture = null;

		// Token: 0x0400003B RID: 59
		internal Color m_HealthyColor = new Color(0.2627451f, 0.9764706f, 0.16470589f, 1f);

		// Token: 0x0400003C RID: 60
		internal Color m_DryColor = new Color(0.8039216f, 0.7372549f, 0.101960786f, 1f);

		// Token: 0x0400003D RID: 61
		internal float m_MinWidth = 1f;

		// Token: 0x0400003E RID: 62
		internal float m_MaxWidth = 2f;

		// Token: 0x0400003F RID: 63
		internal float m_MinHeight = 1f;

		// Token: 0x04000040 RID: 64
		internal float m_MaxHeight = 2f;

		// Token: 0x04000041 RID: 65
		internal float m_NoiseSpread = 0.1f;

		// Token: 0x04000042 RID: 66
		internal float m_BendFactor = 0.1f;

		// Token: 0x04000043 RID: 67
		internal int m_RenderMode = 2;

		// Token: 0x04000044 RID: 68
		internal int m_UsePrototypeMesh = 0;
	}
}
