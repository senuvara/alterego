﻿using System;

namespace System
{
	// Token: 0x02000005 RID: 5
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
	internal class MonoInternalNoteAttribute : MonoTODOAttribute
	{
		// Token: 0x06000006 RID: 6 RVA: 0x00002081 File Offset: 0x00000281
		public MonoInternalNoteAttribute(string comment) : base(comment)
		{
		}
	}
}
