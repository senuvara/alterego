﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000011 RID: 17
	[NativeClass("ContactFilter", "struct ContactFilter;")]
	[NativeHeader("Modules/Physics2D/Public/Collider2D.h")]
	[RequiredByNativeCode(Optional = true, GenerateProxy = true)]
	[Serializable]
	public struct ContactFilter2D
	{
		// Token: 0x060001A0 RID: 416 RVA: 0x0000533C File Offset: 0x0000353C
		public ContactFilter2D NoFilter()
		{
			this.useTriggers = true;
			this.useLayerMask = false;
			this.layerMask = -1;
			this.useDepth = false;
			this.useOutsideDepth = false;
			this.minDepth = float.NegativeInfinity;
			this.maxDepth = float.PositiveInfinity;
			this.useNormalAngle = false;
			this.useOutsideNormalAngle = false;
			this.minNormalAngle = 0f;
			this.maxNormalAngle = 359.9999f;
			return this;
		}

		// Token: 0x060001A1 RID: 417 RVA: 0x000053BC File Offset: 0x000035BC
		private void CheckConsistency()
		{
			this.minDepth = ((this.minDepth != float.NegativeInfinity && this.minDepth != float.PositiveInfinity && !float.IsNaN(this.minDepth)) ? this.minDepth : float.MinValue);
			this.maxDepth = ((this.maxDepth != float.NegativeInfinity && this.maxDepth != float.PositiveInfinity && !float.IsNaN(this.maxDepth)) ? this.maxDepth : float.MaxValue);
			if (this.minDepth > this.maxDepth)
			{
				float num = this.minDepth;
				this.minDepth = this.maxDepth;
				this.maxDepth = num;
			}
			this.minNormalAngle = ((!float.IsNaN(this.minNormalAngle)) ? Mathf.Clamp(this.minNormalAngle, 0f, 359.9999f) : 0f);
			this.maxNormalAngle = ((!float.IsNaN(this.maxNormalAngle)) ? Mathf.Clamp(this.maxNormalAngle, 0f, 359.9999f) : 359.9999f);
			if (this.minNormalAngle > this.maxNormalAngle)
			{
				float num2 = this.minNormalAngle;
				this.minNormalAngle = this.maxNormalAngle;
				this.maxNormalAngle = num2;
			}
		}

		// Token: 0x060001A2 RID: 418 RVA: 0x0000551A File Offset: 0x0000371A
		public void ClearLayerMask()
		{
			this.useLayerMask = false;
		}

		// Token: 0x060001A3 RID: 419 RVA: 0x00005524 File Offset: 0x00003724
		public void SetLayerMask(LayerMask layerMask)
		{
			this.layerMask = layerMask;
			this.useLayerMask = true;
		}

		// Token: 0x060001A4 RID: 420 RVA: 0x00005535 File Offset: 0x00003735
		public void ClearDepth()
		{
			this.useDepth = false;
		}

		// Token: 0x060001A5 RID: 421 RVA: 0x0000553F File Offset: 0x0000373F
		public void SetDepth(float minDepth, float maxDepth)
		{
			this.minDepth = minDepth;
			this.maxDepth = maxDepth;
			this.useDepth = true;
			this.CheckConsistency();
		}

		// Token: 0x060001A6 RID: 422 RVA: 0x0000555D File Offset: 0x0000375D
		public void ClearNormalAngle()
		{
			this.useNormalAngle = false;
		}

		// Token: 0x060001A7 RID: 423 RVA: 0x00005567 File Offset: 0x00003767
		public void SetNormalAngle(float minNormalAngle, float maxNormalAngle)
		{
			this.minNormalAngle = minNormalAngle;
			this.maxNormalAngle = maxNormalAngle;
			this.useNormalAngle = true;
			this.CheckConsistency();
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x060001A8 RID: 424 RVA: 0x00005588 File Offset: 0x00003788
		public bool isFiltering
		{
			get
			{
				return !this.useTriggers || this.useLayerMask || this.useDepth || this.useNormalAngle;
			}
		}

		// Token: 0x060001A9 RID: 425 RVA: 0x000055C8 File Offset: 0x000037C8
		public bool IsFilteringTrigger([Writable] Collider2D collider)
		{
			return !this.useTriggers && collider.isTrigger;
		}

		// Token: 0x060001AA RID: 426 RVA: 0x000055F4 File Offset: 0x000037F4
		public bool IsFilteringLayerMask(GameObject obj)
		{
			return this.useLayerMask && (this.layerMask & 1 << obj.layer) == 0;
		}

		// Token: 0x060001AB RID: 427 RVA: 0x00005634 File Offset: 0x00003834
		public bool IsFilteringDepth(GameObject obj)
		{
			bool result;
			if (!this.useDepth)
			{
				result = false;
			}
			else
			{
				if (this.minDepth > this.maxDepth)
				{
					float num = this.minDepth;
					this.minDepth = this.maxDepth;
					this.maxDepth = num;
				}
				float z = obj.transform.position.z;
				bool flag = z < this.minDepth || z > this.maxDepth;
				if (this.useOutsideDepth)
				{
					result = !flag;
				}
				else
				{
					result = flag;
				}
			}
			return result;
		}

		// Token: 0x060001AC RID: 428 RVA: 0x000056D0 File Offset: 0x000038D0
		public bool IsFilteringNormalAngle(Vector2 normal)
		{
			float angle = Mathf.Atan2(normal.y, normal.x) * 57.29578f;
			return this.IsFilteringNormalAngle(angle);
		}

		// Token: 0x060001AD RID: 429 RVA: 0x00005708 File Offset: 0x00003908
		public bool IsFilteringNormalAngle(float angle)
		{
			angle -= Mathf.Floor(angle / 359.9999f) * 359.9999f;
			float num = Mathf.Clamp(this.minNormalAngle, 0f, 359.9999f);
			float num2 = Mathf.Clamp(this.maxNormalAngle, 0f, 359.9999f);
			if (num > num2)
			{
				float num3 = num;
				num = num2;
				num2 = num3;
			}
			bool flag = angle < num || angle > num2;
			bool result;
			if (this.useOutsideNormalAngle)
			{
				result = !flag;
			}
			else
			{
				result = flag;
			}
			return result;
		}

		// Token: 0x060001AE RID: 430 RVA: 0x00005798 File Offset: 0x00003998
		internal static ContactFilter2D CreateLegacyFilter(int layerMask, float minDepth, float maxDepth)
		{
			ContactFilter2D result = default(ContactFilter2D);
			result.useTriggers = Physics2D.queriesHitTriggers;
			result.SetLayerMask(layerMask);
			result.SetDepth(minDepth, maxDepth);
			return result;
		}

		// Token: 0x04000038 RID: 56
		[NativeName("m_UseTriggers")]
		public bool useTriggers;

		// Token: 0x04000039 RID: 57
		[NativeName("m_UseLayerMask")]
		public bool useLayerMask;

		// Token: 0x0400003A RID: 58
		[NativeName("m_UseDepth")]
		public bool useDepth;

		// Token: 0x0400003B RID: 59
		[NativeName("m_UseOutsideDepth")]
		public bool useOutsideDepth;

		// Token: 0x0400003C RID: 60
		[NativeName("m_UseNormalAngle")]
		public bool useNormalAngle;

		// Token: 0x0400003D RID: 61
		[NativeName("m_UseOutsideNormalAngle")]
		public bool useOutsideNormalAngle;

		// Token: 0x0400003E RID: 62
		[NativeName("m_LayerMask")]
		public LayerMask layerMask;

		// Token: 0x0400003F RID: 63
		[NativeName("m_MinDepth")]
		public float minDepth;

		// Token: 0x04000040 RID: 64
		[NativeName("m_MaxDepth")]
		public float maxDepth;

		// Token: 0x04000041 RID: 65
		[NativeName("m_MinNormalAngle")]
		public float minNormalAngle;

		// Token: 0x04000042 RID: 66
		[NativeName("m_MaxNormalAngle")]
		public float maxNormalAngle;

		// Token: 0x04000043 RID: 67
		public const float NormalAngleUpperLimit = 359.9999f;
	}
}
