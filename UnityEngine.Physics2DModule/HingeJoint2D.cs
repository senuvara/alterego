﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000029 RID: 41
	[NativeHeader("Modules/Physics2D/HingeJoint2D.h")]
	public sealed class HingeJoint2D : AnchoredJoint2D
	{
		// Token: 0x06000323 RID: 803 RVA: 0x00006CA0 File Offset: 0x00004EA0
		public HingeJoint2D()
		{
		}

		// Token: 0x170000AA RID: 170
		// (get) Token: 0x06000324 RID: 804
		// (set) Token: 0x06000325 RID: 805
		public extern bool useMotor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000AB RID: 171
		// (get) Token: 0x06000326 RID: 806
		// (set) Token: 0x06000327 RID: 807
		public extern bool useLimits { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000AC RID: 172
		// (get) Token: 0x06000328 RID: 808 RVA: 0x00006CA8 File Offset: 0x00004EA8
		// (set) Token: 0x06000329 RID: 809 RVA: 0x00006CBE File Offset: 0x00004EBE
		public JointMotor2D motor
		{
			get
			{
				JointMotor2D result;
				this.get_motor_Injected(out result);
				return result;
			}
			set
			{
				this.set_motor_Injected(ref value);
			}
		}

		// Token: 0x170000AD RID: 173
		// (get) Token: 0x0600032A RID: 810 RVA: 0x00006CC8 File Offset: 0x00004EC8
		// (set) Token: 0x0600032B RID: 811 RVA: 0x00006CDE File Offset: 0x00004EDE
		public JointAngleLimits2D limits
		{
			get
			{
				JointAngleLimits2D result;
				this.get_limits_Injected(out result);
				return result;
			}
			set
			{
				this.set_limits_Injected(ref value);
			}
		}

		// Token: 0x170000AE RID: 174
		// (get) Token: 0x0600032C RID: 812
		public extern JointLimitState2D limitState { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000AF RID: 175
		// (get) Token: 0x0600032D RID: 813
		public extern float referenceAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x0600032E RID: 814
		public extern float jointAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x0600032F RID: 815
		public extern float jointSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000330 RID: 816
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetMotorTorque(float timeStep);

		// Token: 0x06000331 RID: 817
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_motor_Injected(out JointMotor2D ret);

		// Token: 0x06000332 RID: 818
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_motor_Injected(ref JointMotor2D value);

		// Token: 0x06000333 RID: 819
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_limits_Injected(out JointAngleLimits2D ret);

		// Token: 0x06000334 RID: 820
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_limits_Injected(ref JointAngleLimits2D value);
	}
}
