﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.SceneManagement;

namespace UnityEngine
{
	// Token: 0x02000003 RID: 3
	public static class PhysicsSceneExtensions2D
	{
		// Token: 0x06000066 RID: 102 RVA: 0x00002CD0 File Offset: 0x00000ED0
		public static PhysicsScene2D GetPhysicsScene2D(this Scene scene)
		{
			if (!scene.IsValid())
			{
				throw new ArgumentException("Cannot get physics scene; Unity scene is invalid.", "scene");
			}
			PhysicsScene2D physicsScene_Internal = PhysicsSceneExtensions2D.GetPhysicsScene_Internal(scene);
			if (physicsScene_Internal.IsValid())
			{
				return physicsScene_Internal;
			}
			throw new Exception("The physics scene associated with the Unity scene is invalid.");
		}

		// Token: 0x06000067 RID: 103 RVA: 0x00002D20 File Offset: 0x00000F20
		[StaticAccessor("GetPhysicsManager2D()", StaticAccessorType.Arrow)]
		[NativeMethod("GetPhysicsSceneFromUnityScene")]
		private static PhysicsScene2D GetPhysicsScene_Internal(Scene scene)
		{
			PhysicsScene2D result;
			PhysicsSceneExtensions2D.GetPhysicsScene_Internal_Injected(ref scene, out result);
			return result;
		}

		// Token: 0x06000068 RID: 104
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetPhysicsScene_Internal_Injected(ref Scene scene, out PhysicsScene2D ret);
	}
}
