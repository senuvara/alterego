﻿using System;

namespace UnityEngine
{
	// Token: 0x02000014 RID: 20
	public struct JointAngleLimits2D
	{
		// Token: 0x1700003C RID: 60
		// (get) Token: 0x060001C8 RID: 456 RVA: 0x00005BE4 File Offset: 0x00003DE4
		// (set) Token: 0x060001C9 RID: 457 RVA: 0x00005BFF File Offset: 0x00003DFF
		public float min
		{
			get
			{
				return this.m_LowerAngle;
			}
			set
			{
				this.m_LowerAngle = value;
			}
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x060001CA RID: 458 RVA: 0x00005C0C File Offset: 0x00003E0C
		// (set) Token: 0x060001CB RID: 459 RVA: 0x00005C27 File Offset: 0x00003E27
		public float max
		{
			get
			{
				return this.m_UpperAngle;
			}
			set
			{
				this.m_UpperAngle = value;
			}
		}

		// Token: 0x04000058 RID: 88
		private float m_LowerAngle;

		// Token: 0x04000059 RID: 89
		private float m_UpperAngle;
	}
}
