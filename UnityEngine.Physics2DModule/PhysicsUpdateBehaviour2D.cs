﻿using System;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000035 RID: 53
	[NativeHeader("Modules/Physics2D/PhysicsUpdateBehaviour2D.h")]
	public class PhysicsUpdateBehaviour2D : Behaviour
	{
		// Token: 0x060003D6 RID: 982 RVA: 0x000065BB File Offset: 0x000047BB
		public PhysicsUpdateBehaviour2D()
		{
		}
	}
}
