﻿using System;

namespace UnityEngine
{
	// Token: 0x0200000F RID: 15
	public enum EffectorForceMode2D
	{
		// Token: 0x04000030 RID: 48
		Constant,
		// Token: 0x04000031 RID: 49
		InverseLinear,
		// Token: 0x04000032 RID: 50
		InverseSquared
	}
}
