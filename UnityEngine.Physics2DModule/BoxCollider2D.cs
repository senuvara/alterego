﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200001F RID: 31
	[NativeHeader("Modules/Physics2D/Public/BoxCollider2D.h")]
	public sealed class BoxCollider2D : Collider2D
	{
		// Token: 0x060002C9 RID: 713 RVA: 0x00006A03 File Offset: 0x00004C03
		public BoxCollider2D()
		{
		}

		// Token: 0x1700008B RID: 139
		// (get) Token: 0x060002CA RID: 714 RVA: 0x00006A2C File Offset: 0x00004C2C
		// (set) Token: 0x060002CB RID: 715 RVA: 0x00006A42 File Offset: 0x00004C42
		public Vector2 size
		{
			get
			{
				Vector2 result;
				this.get_size_Injected(out result);
				return result;
			}
			set
			{
				this.set_size_Injected(ref value);
			}
		}

		// Token: 0x1700008C RID: 140
		// (get) Token: 0x060002CC RID: 716
		// (set) Token: 0x060002CD RID: 717
		public extern float edgeRadius { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x060002CE RID: 718
		// (set) Token: 0x060002CF RID: 719
		public extern bool autoTiling { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060002D0 RID: 720
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_size_Injected(out Vector2 ret);

		// Token: 0x060002D1 RID: 721
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_size_Injected(ref Vector2 value);
	}
}
