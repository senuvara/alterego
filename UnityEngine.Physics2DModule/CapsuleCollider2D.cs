﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200001D RID: 29
	[NativeHeader("Modules/Physics2D/Public/CapsuleCollider2D.h")]
	public sealed class CapsuleCollider2D : Collider2D
	{
		// Token: 0x060002BA RID: 698 RVA: 0x00006A03 File Offset: 0x00004C03
		public CapsuleCollider2D()
		{
		}

		// Token: 0x17000085 RID: 133
		// (get) Token: 0x060002BB RID: 699 RVA: 0x00006A0C File Offset: 0x00004C0C
		// (set) Token: 0x060002BC RID: 700 RVA: 0x00006A22 File Offset: 0x00004C22
		public Vector2 size
		{
			get
			{
				Vector2 result;
				this.get_size_Injected(out result);
				return result;
			}
			set
			{
				this.set_size_Injected(ref value);
			}
		}

		// Token: 0x17000086 RID: 134
		// (get) Token: 0x060002BD RID: 701
		// (set) Token: 0x060002BE RID: 702
		public extern CapsuleDirection2D direction { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060002BF RID: 703
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_size_Injected(out Vector2 ret);

		// Token: 0x060002C0 RID: 704
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_size_Injected(ref Vector2 value);
	}
}
