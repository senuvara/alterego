﻿using System;

namespace UnityEngine
{
	// Token: 0x02000015 RID: 21
	public struct JointTranslationLimits2D
	{
		// Token: 0x1700003E RID: 62
		// (get) Token: 0x060001CC RID: 460 RVA: 0x00005C34 File Offset: 0x00003E34
		// (set) Token: 0x060001CD RID: 461 RVA: 0x00005C4F File Offset: 0x00003E4F
		public float min
		{
			get
			{
				return this.m_LowerTranslation;
			}
			set
			{
				this.m_LowerTranslation = value;
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x060001CE RID: 462 RVA: 0x00005C5C File Offset: 0x00003E5C
		// (set) Token: 0x060001CF RID: 463 RVA: 0x00005C77 File Offset: 0x00003E77
		public float max
		{
			get
			{
				return this.m_UpperTranslation;
			}
			set
			{
				this.m_UpperTranslation = value;
			}
		}

		// Token: 0x0400005A RID: 90
		private float m_LowerTranslation;

		// Token: 0x0400005B RID: 91
		private float m_UpperTranslation;
	}
}
