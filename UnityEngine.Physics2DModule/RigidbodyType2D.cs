﻿using System;

namespace UnityEngine
{
	// Token: 0x0200000A RID: 10
	public enum RigidbodyType2D
	{
		// Token: 0x0400001D RID: 29
		Dynamic,
		// Token: 0x0400001E RID: 30
		Kinematic,
		// Token: 0x0400001F RID: 31
		Static
	}
}
