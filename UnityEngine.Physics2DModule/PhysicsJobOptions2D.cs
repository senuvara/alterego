﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000019 RID: 25
	[RequiredByNativeCode(Optional = true, GenerateProxy = true)]
	[NativeHeader("Modules/Physics2D/Public/Physics2DSettings.h")]
	[NativeClass("PhysicsJobOptions2D", "struct PhysicsJobOptions2D;")]
	public struct PhysicsJobOptions2D
	{
		// Token: 0x1700004D RID: 77
		// (get) Token: 0x060001E9 RID: 489 RVA: 0x00005F4C File Offset: 0x0000414C
		// (set) Token: 0x060001EA RID: 490 RVA: 0x00005F67 File Offset: 0x00004167
		public bool useMultithreading
		{
			get
			{
				return this.m_UseMultithreading;
			}
			set
			{
				this.m_UseMultithreading = value;
			}
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x060001EB RID: 491 RVA: 0x00005F74 File Offset: 0x00004174
		// (set) Token: 0x060001EC RID: 492 RVA: 0x00005F8F File Offset: 0x0000418F
		public bool useConsistencySorting
		{
			get
			{
				return this.m_UseConsistencySorting;
			}
			set
			{
				this.m_UseConsistencySorting = value;
			}
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x060001ED RID: 493 RVA: 0x00005F9C File Offset: 0x0000419C
		// (set) Token: 0x060001EE RID: 494 RVA: 0x00005FB7 File Offset: 0x000041B7
		public int interpolationPosesPerJob
		{
			get
			{
				return this.m_InterpolationPosesPerJob;
			}
			set
			{
				this.m_InterpolationPosesPerJob = value;
			}
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x060001EF RID: 495 RVA: 0x00005FC4 File Offset: 0x000041C4
		// (set) Token: 0x060001F0 RID: 496 RVA: 0x00005FDF File Offset: 0x000041DF
		public int newContactsPerJob
		{
			get
			{
				return this.m_NewContactsPerJob;
			}
			set
			{
				this.m_NewContactsPerJob = value;
			}
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x060001F1 RID: 497 RVA: 0x00005FEC File Offset: 0x000041EC
		// (set) Token: 0x060001F2 RID: 498 RVA: 0x00006007 File Offset: 0x00004207
		public int collideContactsPerJob
		{
			get
			{
				return this.m_CollideContactsPerJob;
			}
			set
			{
				this.m_CollideContactsPerJob = value;
			}
		}

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x060001F3 RID: 499 RVA: 0x00006014 File Offset: 0x00004214
		// (set) Token: 0x060001F4 RID: 500 RVA: 0x0000602F File Offset: 0x0000422F
		public int clearFlagsPerJob
		{
			get
			{
				return this.m_ClearFlagsPerJob;
			}
			set
			{
				this.m_ClearFlagsPerJob = value;
			}
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x060001F5 RID: 501 RVA: 0x0000603C File Offset: 0x0000423C
		// (set) Token: 0x060001F6 RID: 502 RVA: 0x00006057 File Offset: 0x00004257
		public int clearBodyForcesPerJob
		{
			get
			{
				return this.m_ClearBodyForcesPerJob;
			}
			set
			{
				this.m_ClearBodyForcesPerJob = value;
			}
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x060001F7 RID: 503 RVA: 0x00006064 File Offset: 0x00004264
		// (set) Token: 0x060001F8 RID: 504 RVA: 0x0000607F File Offset: 0x0000427F
		public int syncDiscreteFixturesPerJob
		{
			get
			{
				return this.m_SyncDiscreteFixturesPerJob;
			}
			set
			{
				this.m_SyncDiscreteFixturesPerJob = value;
			}
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x060001F9 RID: 505 RVA: 0x0000608C File Offset: 0x0000428C
		// (set) Token: 0x060001FA RID: 506 RVA: 0x000060A7 File Offset: 0x000042A7
		public int syncContinuousFixturesPerJob
		{
			get
			{
				return this.m_SyncContinuousFixturesPerJob;
			}
			set
			{
				this.m_SyncContinuousFixturesPerJob = value;
			}
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x060001FB RID: 507 RVA: 0x000060B4 File Offset: 0x000042B4
		// (set) Token: 0x060001FC RID: 508 RVA: 0x000060CF File Offset: 0x000042CF
		public int findNearestContactsPerJob
		{
			get
			{
				return this.m_FindNearestContactsPerJob;
			}
			set
			{
				this.m_FindNearestContactsPerJob = value;
			}
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x060001FD RID: 509 RVA: 0x000060DC File Offset: 0x000042DC
		// (set) Token: 0x060001FE RID: 510 RVA: 0x000060F7 File Offset: 0x000042F7
		public int updateTriggerContactsPerJob
		{
			get
			{
				return this.m_UpdateTriggerContactsPerJob;
			}
			set
			{
				this.m_UpdateTriggerContactsPerJob = value;
			}
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x060001FF RID: 511 RVA: 0x00006104 File Offset: 0x00004304
		// (set) Token: 0x06000200 RID: 512 RVA: 0x0000611F File Offset: 0x0000431F
		public int islandSolverCostThreshold
		{
			get
			{
				return this.m_IslandSolverCostThreshold;
			}
			set
			{
				this.m_IslandSolverCostThreshold = value;
			}
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x06000201 RID: 513 RVA: 0x0000612C File Offset: 0x0000432C
		// (set) Token: 0x06000202 RID: 514 RVA: 0x00006147 File Offset: 0x00004347
		public int islandSolverBodyCostScale
		{
			get
			{
				return this.m_IslandSolverBodyCostScale;
			}
			set
			{
				this.m_IslandSolverBodyCostScale = value;
			}
		}

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x06000203 RID: 515 RVA: 0x00006154 File Offset: 0x00004354
		// (set) Token: 0x06000204 RID: 516 RVA: 0x0000616F File Offset: 0x0000436F
		public int islandSolverContactCostScale
		{
			get
			{
				return this.m_IslandSolverContactCostScale;
			}
			set
			{
				this.m_IslandSolverContactCostScale = value;
			}
		}

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x06000205 RID: 517 RVA: 0x0000617C File Offset: 0x0000437C
		// (set) Token: 0x06000206 RID: 518 RVA: 0x00006197 File Offset: 0x00004397
		public int islandSolverJointCostScale
		{
			get
			{
				return this.m_IslandSolverJointCostScale;
			}
			set
			{
				this.m_IslandSolverJointCostScale = value;
			}
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x06000207 RID: 519 RVA: 0x000061A4 File Offset: 0x000043A4
		// (set) Token: 0x06000208 RID: 520 RVA: 0x000061BF File Offset: 0x000043BF
		public int islandSolverBodiesPerJob
		{
			get
			{
				return this.m_IslandSolverBodiesPerJob;
			}
			set
			{
				this.m_IslandSolverBodiesPerJob = value;
			}
		}

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x06000209 RID: 521 RVA: 0x000061CC File Offset: 0x000043CC
		// (set) Token: 0x0600020A RID: 522 RVA: 0x000061E7 File Offset: 0x000043E7
		public int islandSolverContactsPerJob
		{
			get
			{
				return this.m_IslandSolverContactsPerJob;
			}
			set
			{
				this.m_IslandSolverContactsPerJob = value;
			}
		}

		// Token: 0x04000067 RID: 103
		private bool m_UseMultithreading;

		// Token: 0x04000068 RID: 104
		private bool m_UseConsistencySorting;

		// Token: 0x04000069 RID: 105
		private int m_InterpolationPosesPerJob;

		// Token: 0x0400006A RID: 106
		private int m_NewContactsPerJob;

		// Token: 0x0400006B RID: 107
		private int m_CollideContactsPerJob;

		// Token: 0x0400006C RID: 108
		private int m_ClearFlagsPerJob;

		// Token: 0x0400006D RID: 109
		private int m_ClearBodyForcesPerJob;

		// Token: 0x0400006E RID: 110
		private int m_SyncDiscreteFixturesPerJob;

		// Token: 0x0400006F RID: 111
		private int m_SyncContinuousFixturesPerJob;

		// Token: 0x04000070 RID: 112
		private int m_FindNearestContactsPerJob;

		// Token: 0x04000071 RID: 113
		private int m_UpdateTriggerContactsPerJob;

		// Token: 0x04000072 RID: 114
		private int m_IslandSolverCostThreshold;

		// Token: 0x04000073 RID: 115
		private int m_IslandSolverBodyCostScale;

		// Token: 0x04000074 RID: 116
		private int m_IslandSolverContactCostScale;

		// Token: 0x04000075 RID: 117
		private int m_IslandSolverJointCostScale;

		// Token: 0x04000076 RID: 118
		private int m_IslandSolverBodiesPerJob;

		// Token: 0x04000077 RID: 119
		private int m_IslandSolverContactsPerJob;
	}
}
