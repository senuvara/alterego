﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000036 RID: 54
	[RequireComponent(typeof(Rigidbody2D))]
	[NativeHeader("Modules/Physics2D/ConstantForce2D.h")]
	public sealed class ConstantForce2D : PhysicsUpdateBehaviour2D
	{
		// Token: 0x060003D7 RID: 983 RVA: 0x00006DE8 File Offset: 0x00004FE8
		public ConstantForce2D()
		{
		}

		// Token: 0x170000FB RID: 251
		// (get) Token: 0x060003D8 RID: 984 RVA: 0x00006DF0 File Offset: 0x00004FF0
		// (set) Token: 0x060003D9 RID: 985 RVA: 0x00006E06 File Offset: 0x00005006
		public Vector2 force
		{
			get
			{
				Vector2 result;
				this.get_force_Injected(out result);
				return result;
			}
			set
			{
				this.set_force_Injected(ref value);
			}
		}

		// Token: 0x170000FC RID: 252
		// (get) Token: 0x060003DA RID: 986 RVA: 0x00006E10 File Offset: 0x00005010
		// (set) Token: 0x060003DB RID: 987 RVA: 0x00006E26 File Offset: 0x00005026
		public Vector2 relativeForce
		{
			get
			{
				Vector2 result;
				this.get_relativeForce_Injected(out result);
				return result;
			}
			set
			{
				this.set_relativeForce_Injected(ref value);
			}
		}

		// Token: 0x170000FD RID: 253
		// (get) Token: 0x060003DC RID: 988
		// (set) Token: 0x060003DD RID: 989
		public extern float torque { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060003DE RID: 990
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_force_Injected(out Vector2 ret);

		// Token: 0x060003DF RID: 991
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_force_Injected(ref Vector2 value);

		// Token: 0x060003E0 RID: 992
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_relativeForce_Injected(out Vector2 ret);

		// Token: 0x060003E1 RID: 993
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_relativeForce_Injected(ref Vector2 value);
	}
}
