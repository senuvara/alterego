﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000037 RID: 55
	[NativeHeader("Modules/Physics2D/Public/PhysicsMaterial2D.h")]
	public sealed class PhysicsMaterial2D : Object
	{
		// Token: 0x060003E2 RID: 994 RVA: 0x00006E30 File Offset: 0x00005030
		public PhysicsMaterial2D()
		{
			PhysicsMaterial2D.Create_Internal(this, null);
		}

		// Token: 0x060003E3 RID: 995 RVA: 0x00006E40 File Offset: 0x00005040
		public PhysicsMaterial2D(string name)
		{
			PhysicsMaterial2D.Create_Internal(this, name);
		}

		// Token: 0x060003E4 RID: 996
		[NativeMethod("Create_Binding")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Create_Internal([Writable] PhysicsMaterial2D scriptMaterial, string name);

		// Token: 0x170000FE RID: 254
		// (get) Token: 0x060003E5 RID: 997
		// (set) Token: 0x060003E6 RID: 998
		public extern float bounciness { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000FF RID: 255
		// (get) Token: 0x060003E7 RID: 999
		// (set) Token: 0x060003E8 RID: 1000
		public extern float friction { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
