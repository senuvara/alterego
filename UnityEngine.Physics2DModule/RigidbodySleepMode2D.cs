﻿using System;

namespace UnityEngine
{
	// Token: 0x02000008 RID: 8
	public enum RigidbodySleepMode2D
	{
		// Token: 0x04000015 RID: 21
		NeverSleep,
		// Token: 0x04000016 RID: 22
		StartAwake,
		// Token: 0x04000017 RID: 23
		StartAsleep
	}
}
