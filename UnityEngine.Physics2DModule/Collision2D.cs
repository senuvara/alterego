﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000012 RID: 18
	[RequiredByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public class Collision2D
	{
		// Token: 0x060001AF RID: 431 RVA: 0x00002D37 File Offset: 0x00000F37
		public Collision2D()
		{
		}

		// Token: 0x060001B0 RID: 432 RVA: 0x000057D8 File Offset: 0x000039D8
		private ContactPoint2D[] GetContacts_Internal()
		{
			return (this.m_LegacyContacts != null) ? this.m_LegacyContacts : this.m_RecycledContacts;
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x060001B1 RID: 433 RVA: 0x0000580C File Offset: 0x00003A0C
		public Collider2D collider
		{
			get
			{
				return Object.FindObjectFromInstanceID(this.m_Collider) as Collider2D;
			}
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x060001B2 RID: 434 RVA: 0x00005834 File Offset: 0x00003A34
		public Collider2D otherCollider
		{
			get
			{
				return Object.FindObjectFromInstanceID(this.m_OtherCollider) as Collider2D;
			}
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x060001B3 RID: 435 RVA: 0x0000585C File Offset: 0x00003A5C
		public Rigidbody2D rigidbody
		{
			get
			{
				return Object.FindObjectFromInstanceID(this.m_Rigidbody) as Rigidbody2D;
			}
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x060001B4 RID: 436 RVA: 0x00005884 File Offset: 0x00003A84
		public Rigidbody2D otherRigidbody
		{
			get
			{
				return Object.FindObjectFromInstanceID(this.m_OtherRigidbody) as Rigidbody2D;
			}
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x060001B5 RID: 437 RVA: 0x000058AC File Offset: 0x00003AAC
		public Transform transform
		{
			get
			{
				return (!(this.rigidbody != null)) ? this.collider.transform : this.rigidbody.transform;
			}
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x060001B6 RID: 438 RVA: 0x000058F0 File Offset: 0x00003AF0
		public GameObject gameObject
		{
			get
			{
				return (!(this.rigidbody != null)) ? this.collider.gameObject : this.rigidbody.gameObject;
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x060001B7 RID: 439 RVA: 0x00005934 File Offset: 0x00003B34
		public Vector2 relativeVelocity
		{
			get
			{
				return this.m_RelativeVelocity;
			}
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x060001B8 RID: 440 RVA: 0x00005950 File Offset: 0x00003B50
		public bool enabled
		{
			get
			{
				return this.m_Enabled == 1;
			}
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x060001B9 RID: 441 RVA: 0x00005970 File Offset: 0x00003B70
		public ContactPoint2D[] contacts
		{
			get
			{
				if (this.m_LegacyContacts == null)
				{
					this.m_LegacyContacts = new ContactPoint2D[this.m_ContactCount];
					Array.Copy(this.m_RecycledContacts, this.m_LegacyContacts, this.m_ContactCount);
				}
				return this.m_LegacyContacts;
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x060001BA RID: 442 RVA: 0x000059C0 File Offset: 0x00003BC0
		public int contactCount
		{
			get
			{
				return this.m_ContactCount;
			}
		}

		// Token: 0x060001BB RID: 443 RVA: 0x000059DC File Offset: 0x00003BDC
		public ContactPoint2D GetContact(int index)
		{
			if (index < 0 || index >= this.m_ContactCount)
			{
				throw new ArgumentOutOfRangeException(string.Format("Cannot get contact at index {0}. There are {1} contact(s).", index, this.m_ContactCount));
			}
			return this.GetContacts_Internal()[index];
		}

		// Token: 0x060001BC RID: 444 RVA: 0x00005A38 File Offset: 0x00003C38
		public int GetContacts(ContactPoint2D[] contacts)
		{
			if (contacts == null)
			{
				throw new NullReferenceException("Cannot get contacts as the provided array is NULL.");
			}
			int num = Mathf.Min(this.m_ContactCount, contacts.Length);
			Array.Copy(this.GetContacts_Internal(), contacts, num);
			return num;
		}

		// Token: 0x04000044 RID: 68
		internal int m_Collider;

		// Token: 0x04000045 RID: 69
		internal int m_OtherCollider;

		// Token: 0x04000046 RID: 70
		internal int m_Rigidbody;

		// Token: 0x04000047 RID: 71
		internal int m_OtherRigidbody;

		// Token: 0x04000048 RID: 72
		internal Vector2 m_RelativeVelocity;

		// Token: 0x04000049 RID: 73
		internal int m_Enabled;

		// Token: 0x0400004A RID: 74
		internal int m_ContactCount;

		// Token: 0x0400004B RID: 75
		internal ContactPoint2D[] m_RecycledContacts;

		// Token: 0x0400004C RID: 76
		internal ContactPoint2D[] m_LegacyContacts;
	}
}
