﻿using System;

namespace UnityEngine
{
	// Token: 0x02000006 RID: 6
	[Flags]
	public enum RigidbodyConstraints2D
	{
		// Token: 0x0400000A RID: 10
		None = 0,
		// Token: 0x0400000B RID: 11
		FreezePositionX = 1,
		// Token: 0x0400000C RID: 12
		FreezePositionY = 2,
		// Token: 0x0400000D RID: 13
		FreezeRotation = 4,
		// Token: 0x0400000E RID: 14
		FreezePosition = 3,
		// Token: 0x0400000F RID: 15
		FreezeAll = 7
	}
}
