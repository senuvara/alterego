﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000028 RID: 40
	[NativeHeader("Modules/Physics2D/FrictionJoint2D.h")]
	public sealed class FrictionJoint2D : AnchoredJoint2D
	{
		// Token: 0x0600031E RID: 798 RVA: 0x00006CA0 File Offset: 0x00004EA0
		public FrictionJoint2D()
		{
		}

		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x0600031F RID: 799
		// (set) Token: 0x06000320 RID: 800
		public extern float maxForce { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x06000321 RID: 801
		// (set) Token: 0x06000322 RID: 802
		public extern float maxTorque { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
