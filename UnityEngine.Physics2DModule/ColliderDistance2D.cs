﻿using System;

namespace UnityEngine
{
	// Token: 0x02000010 RID: 16
	public struct ColliderDistance2D
	{
		// Token: 0x17000020 RID: 32
		// (get) Token: 0x06000196 RID: 406 RVA: 0x0000524C File Offset: 0x0000344C
		// (set) Token: 0x06000197 RID: 407 RVA: 0x00005267 File Offset: 0x00003467
		public Vector2 pointA
		{
			get
			{
				return this.m_PointA;
			}
			set
			{
				this.m_PointA = value;
			}
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x06000198 RID: 408 RVA: 0x00005274 File Offset: 0x00003474
		// (set) Token: 0x06000199 RID: 409 RVA: 0x0000528F File Offset: 0x0000348F
		public Vector2 pointB
		{
			get
			{
				return this.m_PointB;
			}
			set
			{
				this.m_PointB = value;
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x0600019A RID: 410 RVA: 0x0000529C File Offset: 0x0000349C
		public Vector2 normal
		{
			get
			{
				return this.m_Normal;
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x0600019B RID: 411 RVA: 0x000052B8 File Offset: 0x000034B8
		// (set) Token: 0x0600019C RID: 412 RVA: 0x000052D3 File Offset: 0x000034D3
		public float distance
		{
			get
			{
				return this.m_Distance;
			}
			set
			{
				this.m_Distance = value;
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x0600019D RID: 413 RVA: 0x000052E0 File Offset: 0x000034E0
		public bool isOverlapped
		{
			get
			{
				return this.m_Distance < 0f;
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x0600019E RID: 414 RVA: 0x00005304 File Offset: 0x00003504
		// (set) Token: 0x0600019F RID: 415 RVA: 0x00005325 File Offset: 0x00003525
		public bool isValid
		{
			get
			{
				return this.m_IsValid != 0;
			}
			set
			{
				this.m_IsValid = ((!value) ? 0 : 1);
			}
		}

		// Token: 0x04000033 RID: 51
		private Vector2 m_PointA;

		// Token: 0x04000034 RID: 52
		private Vector2 m_PointB;

		// Token: 0x04000035 RID: 53
		private Vector2 m_Normal;

		// Token: 0x04000036 RID: 54
		private float m_Distance;

		// Token: 0x04000037 RID: 55
		private int m_IsValid;
	}
}
