﻿using System;

namespace UnityEngine
{
	// Token: 0x02000005 RID: 5
	public enum CapsuleDirection2D
	{
		// Token: 0x04000007 RID: 7
		Vertical,
		// Token: 0x04000008 RID: 8
		Horizontal
	}
}
