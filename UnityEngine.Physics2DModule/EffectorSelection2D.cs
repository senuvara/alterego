﻿using System;

namespace UnityEngine
{
	// Token: 0x0200000E RID: 14
	public enum EffectorSelection2D
	{
		// Token: 0x0400002D RID: 45
		Rigidbody,
		// Token: 0x0400002E RID: 46
		Collider
	}
}
