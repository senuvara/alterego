﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200001B RID: 27
	[RequireComponent(typeof(Transform))]
	[NativeHeader("Modules/Physics2D/Public/Collider2D.h")]
	[RequiredByNativeCode(Optional = true)]
	public class Collider2D : Behaviour
	{
		// Token: 0x0600027D RID: 637 RVA: 0x000065BB File Offset: 0x000047BB
		public Collider2D()
		{
		}

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x0600027E RID: 638
		// (set) Token: 0x0600027F RID: 639
		public extern float density { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x06000280 RID: 640
		// (set) Token: 0x06000281 RID: 641
		public extern bool isTrigger { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x06000282 RID: 642
		// (set) Token: 0x06000283 RID: 643
		public extern bool usedByEffector { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x06000284 RID: 644
		// (set) Token: 0x06000285 RID: 645
		public extern bool usedByComposite { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x06000286 RID: 646
		public extern CompositeCollider2D composite { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x06000287 RID: 647 RVA: 0x000065C4 File Offset: 0x000047C4
		// (set) Token: 0x06000288 RID: 648 RVA: 0x000065DA File Offset: 0x000047DA
		public Vector2 offset
		{
			get
			{
				Vector2 result;
				this.get_offset_Injected(out result);
				return result;
			}
			set
			{
				this.set_offset_Injected(ref value);
			}
		}

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x06000289 RID: 649
		public extern Rigidbody2D attachedRigidbody { [NativeMethod("GetAttachedRigidbody_Binding")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x0600028A RID: 650
		public extern int shapeCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x0600028B RID: 651 RVA: 0x000065E4 File Offset: 0x000047E4
		public Bounds bounds
		{
			get
			{
				Bounds result;
				this.get_bounds_Injected(out result);
				return result;
			}
		}

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x0600028C RID: 652
		internal extern ColliderErrorState2D errorState { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000080 RID: 128
		// (get) Token: 0x0600028D RID: 653
		internal extern bool compositeCapable { [NativeMethod("GetCompositeCapable_Binding")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000081 RID: 129
		// (get) Token: 0x0600028E RID: 654
		// (set) Token: 0x0600028F RID: 655
		public extern PhysicsMaterial2D sharedMaterial { [NativeMethod("GetMaterial")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeMethod("SetMaterial")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000082 RID: 130
		// (get) Token: 0x06000290 RID: 656
		public extern float friction { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000083 RID: 131
		// (get) Token: 0x06000291 RID: 657
		public extern float bounciness { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000292 RID: 658
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsTouching([Writable] [NotNull] Collider2D collider);

		// Token: 0x06000293 RID: 659 RVA: 0x000065FC File Offset: 0x000047FC
		public bool IsTouching([Writable] Collider2D collider, ContactFilter2D contactFilter)
		{
			return this.IsTouching_OtherColliderWithFilter(collider, contactFilter);
		}

		// Token: 0x06000294 RID: 660 RVA: 0x00006619 File Offset: 0x00004819
		[NativeMethod("IsTouching")]
		private bool IsTouching_OtherColliderWithFilter([Writable] [NotNull] Collider2D collider, ContactFilter2D contactFilter)
		{
			return this.IsTouching_OtherColliderWithFilter_Injected(collider, ref contactFilter);
		}

		// Token: 0x06000295 RID: 661 RVA: 0x00006624 File Offset: 0x00004824
		public bool IsTouching(ContactFilter2D contactFilter)
		{
			return this.IsTouching_AnyColliderWithFilter(contactFilter);
		}

		// Token: 0x06000296 RID: 662 RVA: 0x00006640 File Offset: 0x00004840
		[NativeMethod("IsTouching")]
		private bool IsTouching_AnyColliderWithFilter(ContactFilter2D contactFilter)
		{
			return this.IsTouching_AnyColliderWithFilter_Injected(ref contactFilter);
		}

		// Token: 0x06000297 RID: 663 RVA: 0x0000664C File Offset: 0x0000484C
		[ExcludeFromDocs]
		public bool IsTouchingLayers()
		{
			return this.IsTouchingLayers(-1);
		}

		// Token: 0x06000298 RID: 664
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsTouchingLayers([DefaultValue("Physics2D.AllLayers")] int layerMask);

		// Token: 0x06000299 RID: 665 RVA: 0x00006668 File Offset: 0x00004868
		public bool OverlapPoint(Vector2 point)
		{
			return this.OverlapPoint_Injected(ref point);
		}

		// Token: 0x0600029A RID: 666 RVA: 0x00006674 File Offset: 0x00004874
		public ColliderDistance2D Distance([Writable] Collider2D collider)
		{
			return Physics2D.Distance(this, collider);
		}

		// Token: 0x0600029B RID: 667 RVA: 0x00006690 File Offset: 0x00004890
		public int OverlapCollider(ContactFilter2D contactFilter, Collider2D[] results)
		{
			return PhysicsScene2D.OverlapCollider(this, contactFilter, results);
		}

		// Token: 0x0600029C RID: 668 RVA: 0x000066B0 File Offset: 0x000048B0
		public int GetContacts(ContactPoint2D[] contacts)
		{
			return Physics2D.GetContacts(this, default(ContactFilter2D).NoFilter(), contacts);
		}

		// Token: 0x0600029D RID: 669 RVA: 0x000066DC File Offset: 0x000048DC
		public int GetContacts(ContactFilter2D contactFilter, ContactPoint2D[] contacts)
		{
			return Physics2D.GetContacts(this, contactFilter, contacts);
		}

		// Token: 0x0600029E RID: 670 RVA: 0x000066FC File Offset: 0x000048FC
		public int GetContacts(Collider2D[] colliders)
		{
			return Physics2D.GetContacts(this, default(ContactFilter2D).NoFilter(), colliders);
		}

		// Token: 0x0600029F RID: 671 RVA: 0x00006728 File Offset: 0x00004928
		public int GetContacts(ContactFilter2D contactFilter, Collider2D[] colliders)
		{
			return Physics2D.GetContacts(this, contactFilter, colliders);
		}

		// Token: 0x060002A0 RID: 672 RVA: 0x00006748 File Offset: 0x00004948
		[ExcludeFromDocs]
		public int Cast(Vector2 direction, RaycastHit2D[] results)
		{
			ContactFilter2D contactFilter = default(ContactFilter2D);
			contactFilter.useTriggers = Physics2D.queriesHitTriggers;
			contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(base.gameObject.layer));
			return this.Cast_Internal(direction, float.PositiveInfinity, contactFilter, true, results);
		}

		// Token: 0x060002A1 RID: 673 RVA: 0x0000679C File Offset: 0x0000499C
		[ExcludeFromDocs]
		public int Cast(Vector2 direction, RaycastHit2D[] results, float distance)
		{
			ContactFilter2D contactFilter = default(ContactFilter2D);
			contactFilter.useTriggers = Physics2D.queriesHitTriggers;
			contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(base.gameObject.layer));
			return this.Cast_Internal(direction, distance, contactFilter, true, results);
		}

		// Token: 0x060002A2 RID: 674 RVA: 0x000067EC File Offset: 0x000049EC
		public int Cast(Vector2 direction, RaycastHit2D[] results, [DefaultValue("Mathf.Infinity")] float distance, [DefaultValue("true")] bool ignoreSiblingColliders)
		{
			ContactFilter2D contactFilter = default(ContactFilter2D);
			contactFilter.useTriggers = Physics2D.queriesHitTriggers;
			contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(base.gameObject.layer));
			return this.Cast_Internal(direction, distance, contactFilter, ignoreSiblingColliders, results);
		}

		// Token: 0x060002A3 RID: 675 RVA: 0x00006840 File Offset: 0x00004A40
		[ExcludeFromDocs]
		public int Cast(Vector2 direction, ContactFilter2D contactFilter, RaycastHit2D[] results)
		{
			return this.Cast_Internal(direction, float.PositiveInfinity, contactFilter, true, results);
		}

		// Token: 0x060002A4 RID: 676 RVA: 0x00006864 File Offset: 0x00004A64
		[ExcludeFromDocs]
		public int Cast(Vector2 direction, ContactFilter2D contactFilter, RaycastHit2D[] results, float distance)
		{
			return this.Cast_Internal(direction, distance, contactFilter, true, results);
		}

		// Token: 0x060002A5 RID: 677 RVA: 0x00006888 File Offset: 0x00004A88
		public int Cast(Vector2 direction, ContactFilter2D contactFilter, RaycastHit2D[] results, [DefaultValue("Mathf.Infinity")] float distance, [DefaultValue("true")] bool ignoreSiblingColliders)
		{
			return this.Cast_Internal(direction, distance, contactFilter, ignoreSiblingColliders, results);
		}

		// Token: 0x060002A6 RID: 678 RVA: 0x000068AA File Offset: 0x00004AAA
		[NativeMethod("Cast_Binding")]
		private int Cast_Internal(Vector2 direction, float distance, ContactFilter2D contactFilter, bool ignoreSiblingColliders, [Out] RaycastHit2D[] results)
		{
			return this.Cast_Internal_Injected(ref direction, distance, ref contactFilter, ignoreSiblingColliders, results);
		}

		// Token: 0x060002A7 RID: 679 RVA: 0x000068BC File Offset: 0x00004ABC
		[ExcludeFromDocs]
		public int Raycast(Vector2 direction, RaycastHit2D[] results)
		{
			ContactFilter2D contactFilter = ContactFilter2D.CreateLegacyFilter(-1, float.NegativeInfinity, float.PositiveInfinity);
			return this.Raycast_Internal(direction, float.PositiveInfinity, contactFilter, results);
		}

		// Token: 0x060002A8 RID: 680 RVA: 0x000068F0 File Offset: 0x00004AF0
		[ExcludeFromDocs]
		public int Raycast(Vector2 direction, RaycastHit2D[] results, float distance)
		{
			ContactFilter2D contactFilter = ContactFilter2D.CreateLegacyFilter(-1, float.NegativeInfinity, float.PositiveInfinity);
			return this.Raycast_Internal(direction, distance, contactFilter, results);
		}

		// Token: 0x060002A9 RID: 681 RVA: 0x00006920 File Offset: 0x00004B20
		[ExcludeFromDocs]
		public int Raycast(Vector2 direction, RaycastHit2D[] results, float distance, int layerMask)
		{
			ContactFilter2D contactFilter = ContactFilter2D.CreateLegacyFilter(layerMask, float.NegativeInfinity, float.PositiveInfinity);
			return this.Raycast_Internal(direction, distance, contactFilter, results);
		}

		// Token: 0x060002AA RID: 682 RVA: 0x00006954 File Offset: 0x00004B54
		[ExcludeFromDocs]
		public int Raycast(Vector2 direction, RaycastHit2D[] results, float distance, int layerMask, float minDepth)
		{
			ContactFilter2D contactFilter = ContactFilter2D.CreateLegacyFilter(layerMask, minDepth, float.PositiveInfinity);
			return this.Raycast_Internal(direction, distance, contactFilter, results);
		}

		// Token: 0x060002AB RID: 683 RVA: 0x00006984 File Offset: 0x00004B84
		public int Raycast(Vector2 direction, RaycastHit2D[] results, [DefaultValue("Mathf.Infinity")] float distance, [DefaultValue("Physics2D.AllLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			ContactFilter2D contactFilter = ContactFilter2D.CreateLegacyFilter(layerMask, minDepth, maxDepth);
			return this.Raycast_Internal(direction, distance, contactFilter, results);
		}

		// Token: 0x060002AC RID: 684 RVA: 0x000069B0 File Offset: 0x00004BB0
		[ExcludeFromDocs]
		public int Raycast(Vector2 direction, ContactFilter2D contactFilter, RaycastHit2D[] results)
		{
			return this.Raycast_Internal(direction, float.PositiveInfinity, contactFilter, results);
		}

		// Token: 0x060002AD RID: 685 RVA: 0x000069D4 File Offset: 0x00004BD4
		public int Raycast(Vector2 direction, ContactFilter2D contactFilter, RaycastHit2D[] results, [DefaultValue("Mathf.Infinity")] float distance)
		{
			return this.Raycast_Internal(direction, distance, contactFilter, results);
		}

		// Token: 0x060002AE RID: 686 RVA: 0x000069F4 File Offset: 0x00004BF4
		[NativeMethod("Raycast_Binding")]
		private int Raycast_Internal(Vector2 direction, float distance, ContactFilter2D contactFilter, [Out] RaycastHit2D[] results)
		{
			return this.Raycast_Internal_Injected(ref direction, distance, ref contactFilter, results);
		}

		// Token: 0x060002AF RID: 687
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_offset_Injected(out Vector2 ret);

		// Token: 0x060002B0 RID: 688
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_offset_Injected(ref Vector2 value);

		// Token: 0x060002B1 RID: 689
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_bounds_Injected(out Bounds ret);

		// Token: 0x060002B2 RID: 690
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool IsTouching_OtherColliderWithFilter_Injected([Writable] Collider2D collider, ref ContactFilter2D contactFilter);

		// Token: 0x060002B3 RID: 691
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool IsTouching_AnyColliderWithFilter_Injected(ref ContactFilter2D contactFilter);

		// Token: 0x060002B4 RID: 692
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool OverlapPoint_Injected(ref Vector2 point);

		// Token: 0x060002B5 RID: 693
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int Cast_Internal_Injected(ref Vector2 direction, float distance, ref ContactFilter2D contactFilter, bool ignoreSiblingColliders, [Out] RaycastHit2D[] results);

		// Token: 0x060002B6 RID: 694
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int Raycast_Internal_Injected(ref Vector2 direction, float distance, ref ContactFilter2D contactFilter, [Out] RaycastHit2D[] results);
	}
}
