﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200002F RID: 47
	[NativeHeader("Modules/Physics2D/Effector2D.h")]
	public class Effector2D : Behaviour
	{
		// Token: 0x06000383 RID: 899 RVA: 0x000065BB File Offset: 0x000047BB
		public Effector2D()
		{
		}

		// Token: 0x170000D3 RID: 211
		// (get) Token: 0x06000384 RID: 900
		// (set) Token: 0x06000385 RID: 901
		public extern bool useColliderMask { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000D4 RID: 212
		// (get) Token: 0x06000386 RID: 902
		// (set) Token: 0x06000387 RID: 903
		public extern int colliderMask { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000D5 RID: 213
		// (get) Token: 0x06000388 RID: 904
		internal extern bool requiresCollider { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000D6 RID: 214
		// (get) Token: 0x06000389 RID: 905
		internal extern bool designedForTrigger { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000D7 RID: 215
		// (get) Token: 0x0600038A RID: 906
		internal extern bool designedForNonTrigger { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
