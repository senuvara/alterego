﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000026 RID: 38
	[NativeHeader("Modules/Physics2D/SpringJoint2D.h")]
	public sealed class SpringJoint2D : AnchoredJoint2D
	{
		// Token: 0x0600030E RID: 782 RVA: 0x00006CA0 File Offset: 0x00004EA0
		public SpringJoint2D()
		{
		}

		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x0600030F RID: 783
		// (set) Token: 0x06000310 RID: 784
		public extern bool autoConfigureDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x06000311 RID: 785
		// (set) Token: 0x06000312 RID: 786
		public extern float distance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x06000313 RID: 787
		// (set) Token: 0x06000314 RID: 788
		public extern float dampingRatio { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000A4 RID: 164
		// (get) Token: 0x06000315 RID: 789
		// (set) Token: 0x06000316 RID: 790
		public extern float frequency { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
