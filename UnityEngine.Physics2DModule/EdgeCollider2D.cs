﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200001E RID: 30
	[NativeHeader("Modules/Physics2D/Public/EdgeCollider2D.h")]
	public sealed class EdgeCollider2D : Collider2D
	{
		// Token: 0x060002C1 RID: 705 RVA: 0x00006A03 File Offset: 0x00004C03
		public EdgeCollider2D()
		{
		}

		// Token: 0x060002C2 RID: 706
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Reset();

		// Token: 0x17000087 RID: 135
		// (get) Token: 0x060002C3 RID: 707
		// (set) Token: 0x060002C4 RID: 708
		public extern float edgeRadius { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000088 RID: 136
		// (get) Token: 0x060002C5 RID: 709
		public extern int edgeCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000089 RID: 137
		// (get) Token: 0x060002C6 RID: 710
		public extern int pointCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700008A RID: 138
		// (get) Token: 0x060002C7 RID: 711
		// (set) Token: 0x060002C8 RID: 712
		public extern Vector2[] points { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
