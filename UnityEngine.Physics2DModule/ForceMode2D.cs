﻿using System;

namespace UnityEngine
{
	// Token: 0x0200000B RID: 11
	public enum ForceMode2D
	{
		// Token: 0x04000021 RID: 33
		Force,
		// Token: 0x04000022 RID: 34
		Impulse
	}
}
