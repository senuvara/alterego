﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000027 RID: 39
	[NativeHeader("Modules/Physics2D/DistanceJoint2D.h")]
	public sealed class DistanceJoint2D : AnchoredJoint2D
	{
		// Token: 0x06000317 RID: 791 RVA: 0x00006CA0 File Offset: 0x00004EA0
		public DistanceJoint2D()
		{
		}

		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x06000318 RID: 792
		// (set) Token: 0x06000319 RID: 793
		public extern bool autoConfigureDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000A6 RID: 166
		// (get) Token: 0x0600031A RID: 794
		// (set) Token: 0x0600031B RID: 795
		public extern float distance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000A7 RID: 167
		// (get) Token: 0x0600031C RID: 796
		// (set) Token: 0x0600031D RID: 797
		public extern bool maxDistanceOnly { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
