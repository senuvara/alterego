﻿using System;

namespace UnityEngine
{
	// Token: 0x0200000D RID: 13
	public enum JointLimitState2D
	{
		// Token: 0x04000028 RID: 40
		Inactive,
		// Token: 0x04000029 RID: 41
		LowerLimit,
		// Token: 0x0400002A RID: 42
		UpperLimit,
		// Token: 0x0400002B RID: 43
		EqualLimits
	}
}
