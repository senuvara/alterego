﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200002D RID: 45
	[NativeHeader("Modules/Physics2D/FixedJoint2D.h")]
	public sealed class FixedJoint2D : AnchoredJoint2D
	{
		// Token: 0x0600036D RID: 877 RVA: 0x00006CA0 File Offset: 0x00004EA0
		public FixedJoint2D()
		{
		}

		// Token: 0x170000C9 RID: 201
		// (get) Token: 0x0600036E RID: 878
		// (set) Token: 0x0600036F RID: 879
		public extern float dampingRatio { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000CA RID: 202
		// (get) Token: 0x06000370 RID: 880
		// (set) Token: 0x06000371 RID: 881
		public extern float frequency { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000CB RID: 203
		// (get) Token: 0x06000372 RID: 882
		public extern float referenceAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
