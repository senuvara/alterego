﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000013 RID: 19
	[NativeClass("ScriptingContactPoint2D", "struct ScriptingContactPoint2D;")]
	[RequiredByNativeCode(Optional = false, GenerateProxy = true)]
	[NativeHeader("Modules/Physics2D/Public/PhysicsScripting2D.h")]
	public struct ContactPoint2D
	{
		// Token: 0x17000031 RID: 49
		// (get) Token: 0x060001BD RID: 445 RVA: 0x00005A7C File Offset: 0x00003C7C
		public Vector2 point
		{
			get
			{
				return this.m_Point;
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x060001BE RID: 446 RVA: 0x00005A98 File Offset: 0x00003C98
		public Vector2 normal
		{
			get
			{
				return this.m_Normal;
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x060001BF RID: 447 RVA: 0x00005AB4 File Offset: 0x00003CB4
		public float separation
		{
			get
			{
				return this.m_Separation;
			}
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x060001C0 RID: 448 RVA: 0x00005AD0 File Offset: 0x00003CD0
		public float normalImpulse
		{
			get
			{
				return this.m_NormalImpulse;
			}
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x060001C1 RID: 449 RVA: 0x00005AEC File Offset: 0x00003CEC
		public float tangentImpulse
		{
			get
			{
				return this.m_TangentImpulse;
			}
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x060001C2 RID: 450 RVA: 0x00005B08 File Offset: 0x00003D08
		public Vector2 relativeVelocity
		{
			get
			{
				return this.m_RelativeVelocity;
			}
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x060001C3 RID: 451 RVA: 0x00005B24 File Offset: 0x00003D24
		public Collider2D collider
		{
			get
			{
				return Object.FindObjectFromInstanceID(this.m_Collider) as Collider2D;
			}
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x060001C4 RID: 452 RVA: 0x00005B4C File Offset: 0x00003D4C
		public Collider2D otherCollider
		{
			get
			{
				return Object.FindObjectFromInstanceID(this.m_OtherCollider) as Collider2D;
			}
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x060001C5 RID: 453 RVA: 0x00005B74 File Offset: 0x00003D74
		public Rigidbody2D rigidbody
		{
			get
			{
				return Object.FindObjectFromInstanceID(this.m_Rigidbody) as Rigidbody2D;
			}
		}

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x060001C6 RID: 454 RVA: 0x00005B9C File Offset: 0x00003D9C
		public Rigidbody2D otherRigidbody
		{
			get
			{
				return Object.FindObjectFromInstanceID(this.m_OtherRigidbody) as Rigidbody2D;
			}
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x060001C7 RID: 455 RVA: 0x00005BC4 File Offset: 0x00003DC4
		public bool enabled
		{
			get
			{
				return this.m_Enabled == 1;
			}
		}

		// Token: 0x0400004D RID: 77
		[NativeName("point")]
		private Vector2 m_Point;

		// Token: 0x0400004E RID: 78
		[NativeName("normal")]
		private Vector2 m_Normal;

		// Token: 0x0400004F RID: 79
		[NativeName("relativeVelocity")]
		private Vector2 m_RelativeVelocity;

		// Token: 0x04000050 RID: 80
		[NativeName("separation")]
		private float m_Separation;

		// Token: 0x04000051 RID: 81
		[NativeName("normalImpulse")]
		private float m_NormalImpulse;

		// Token: 0x04000052 RID: 82
		[NativeName("tangentImpulse")]
		private float m_TangentImpulse;

		// Token: 0x04000053 RID: 83
		[NativeName("collider")]
		private int m_Collider;

		// Token: 0x04000054 RID: 84
		[NativeName("otherCollider")]
		private int m_OtherCollider;

		// Token: 0x04000055 RID: 85
		[NativeName("rigidbody")]
		private int m_Rigidbody;

		// Token: 0x04000056 RID: 86
		[NativeName("otherRigidbody")]
		private int m_OtherRigidbody;

		// Token: 0x04000057 RID: 87
		[NativeName("enabled")]
		private int m_Enabled;
	}
}
