﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200002B RID: 43
	[NativeHeader("Modules/Physics2D/SliderJoint2D.h")]
	public sealed class SliderJoint2D : AnchoredJoint2D
	{
		// Token: 0x06000346 RID: 838 RVA: 0x00006CA0 File Offset: 0x00004EA0
		public SliderJoint2D()
		{
		}

		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x06000347 RID: 839
		// (set) Token: 0x06000348 RID: 840
		public extern bool autoConfigureAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000BA RID: 186
		// (get) Token: 0x06000349 RID: 841
		// (set) Token: 0x0600034A RID: 842
		public extern float angle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000BB RID: 187
		// (get) Token: 0x0600034B RID: 843
		// (set) Token: 0x0600034C RID: 844
		public extern bool useMotor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000BC RID: 188
		// (get) Token: 0x0600034D RID: 845
		// (set) Token: 0x0600034E RID: 846
		public extern bool useLimits { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000BD RID: 189
		// (get) Token: 0x0600034F RID: 847 RVA: 0x00006D20 File Offset: 0x00004F20
		// (set) Token: 0x06000350 RID: 848 RVA: 0x00006D36 File Offset: 0x00004F36
		public JointMotor2D motor
		{
			get
			{
				JointMotor2D result;
				this.get_motor_Injected(out result);
				return result;
			}
			set
			{
				this.set_motor_Injected(ref value);
			}
		}

		// Token: 0x170000BE RID: 190
		// (get) Token: 0x06000351 RID: 849 RVA: 0x00006D40 File Offset: 0x00004F40
		// (set) Token: 0x06000352 RID: 850 RVA: 0x00006D56 File Offset: 0x00004F56
		public JointTranslationLimits2D limits
		{
			get
			{
				JointTranslationLimits2D result;
				this.get_limits_Injected(out result);
				return result;
			}
			set
			{
				this.set_limits_Injected(ref value);
			}
		}

		// Token: 0x170000BF RID: 191
		// (get) Token: 0x06000353 RID: 851
		public extern JointLimitState2D limitState { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000C0 RID: 192
		// (get) Token: 0x06000354 RID: 852
		public extern float referenceAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000C1 RID: 193
		// (get) Token: 0x06000355 RID: 853
		public extern float jointTranslation { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000C2 RID: 194
		// (get) Token: 0x06000356 RID: 854
		public extern float jointSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000357 RID: 855
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetMotorForce(float timeStep);

		// Token: 0x06000358 RID: 856
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_motor_Injected(out JointMotor2D ret);

		// Token: 0x06000359 RID: 857
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_motor_Injected(ref JointMotor2D value);

		// Token: 0x0600035A RID: 858
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_limits_Injected(out JointTranslationLimits2D ret);

		// Token: 0x0600035B RID: 859
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_limits_Injected(ref JointTranslationLimits2D value);
	}
}
