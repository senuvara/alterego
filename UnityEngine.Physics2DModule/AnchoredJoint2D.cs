﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000025 RID: 37
	[NativeHeader("Modules/Physics2D/AnchoredJoint2D.h")]
	public class AnchoredJoint2D : Joint2D
	{
		// Token: 0x06000303 RID: 771 RVA: 0x00006C57 File Offset: 0x00004E57
		public AnchoredJoint2D()
		{
		}

		// Token: 0x1700009E RID: 158
		// (get) Token: 0x06000304 RID: 772 RVA: 0x00006C60 File Offset: 0x00004E60
		// (set) Token: 0x06000305 RID: 773 RVA: 0x00006C76 File Offset: 0x00004E76
		public Vector2 anchor
		{
			get
			{
				Vector2 result;
				this.get_anchor_Injected(out result);
				return result;
			}
			set
			{
				this.set_anchor_Injected(ref value);
			}
		}

		// Token: 0x1700009F RID: 159
		// (get) Token: 0x06000306 RID: 774 RVA: 0x00006C80 File Offset: 0x00004E80
		// (set) Token: 0x06000307 RID: 775 RVA: 0x00006C96 File Offset: 0x00004E96
		public Vector2 connectedAnchor
		{
			get
			{
				Vector2 result;
				this.get_connectedAnchor_Injected(out result);
				return result;
			}
			set
			{
				this.set_connectedAnchor_Injected(ref value);
			}
		}

		// Token: 0x170000A0 RID: 160
		// (get) Token: 0x06000308 RID: 776
		// (set) Token: 0x06000309 RID: 777
		public extern bool autoConfigureConnectedAnchor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600030A RID: 778
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_anchor_Injected(out Vector2 ret);

		// Token: 0x0600030B RID: 779
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_anchor_Injected(ref Vector2 value);

		// Token: 0x0600030C RID: 780
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_connectedAnchor_Injected(out Vector2 ret);

		// Token: 0x0600030D RID: 781
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_connectedAnchor_Injected(ref Vector2 value);
	}
}
