﻿using System;

namespace UnityEngine
{
	// Token: 0x02000017 RID: 23
	public struct JointSuspension2D
	{
		// Token: 0x17000042 RID: 66
		// (get) Token: 0x060001D4 RID: 468 RVA: 0x00005CD4 File Offset: 0x00003ED4
		// (set) Token: 0x060001D5 RID: 469 RVA: 0x00005CEF File Offset: 0x00003EEF
		public float dampingRatio
		{
			get
			{
				return this.m_DampingRatio;
			}
			set
			{
				this.m_DampingRatio = value;
			}
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x060001D6 RID: 470 RVA: 0x00005CFC File Offset: 0x00003EFC
		// (set) Token: 0x060001D7 RID: 471 RVA: 0x00005D17 File Offset: 0x00003F17
		public float frequency
		{
			get
			{
				return this.m_Frequency;
			}
			set
			{
				this.m_Frequency = value;
			}
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x060001D8 RID: 472 RVA: 0x00005D24 File Offset: 0x00003F24
		// (set) Token: 0x060001D9 RID: 473 RVA: 0x00005D3F File Offset: 0x00003F3F
		public float angle
		{
			get
			{
				return this.m_Angle;
			}
			set
			{
				this.m_Angle = value;
			}
		}

		// Token: 0x0400005E RID: 94
		private float m_DampingRatio;

		// Token: 0x0400005F RID: 95
		private float m_Frequency;

		// Token: 0x04000060 RID: 96
		private float m_Angle;
	}
}
