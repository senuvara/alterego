﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000024 RID: 36
	[RequireComponent(typeof(Transform), typeof(Rigidbody2D))]
	[NativeHeader("Modules/Physics2D/Joint2D.h")]
	public class Joint2D : Behaviour
	{
		// Token: 0x060002F3 RID: 755 RVA: 0x000065BB File Offset: 0x000047BB
		public Joint2D()
		{
		}

		// Token: 0x17000097 RID: 151
		// (get) Token: 0x060002F4 RID: 756
		public extern Rigidbody2D attachedRigidbody { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000098 RID: 152
		// (get) Token: 0x060002F5 RID: 757
		// (set) Token: 0x060002F6 RID: 758
		public extern Rigidbody2D connectedBody { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000099 RID: 153
		// (get) Token: 0x060002F7 RID: 759
		// (set) Token: 0x060002F8 RID: 760
		public extern bool enableCollision { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700009A RID: 154
		// (get) Token: 0x060002F9 RID: 761
		// (set) Token: 0x060002FA RID: 762
		public extern float breakForce { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700009B RID: 155
		// (get) Token: 0x060002FB RID: 763
		// (set) Token: 0x060002FC RID: 764
		public extern float breakTorque { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700009C RID: 156
		// (get) Token: 0x060002FD RID: 765 RVA: 0x00006C28 File Offset: 0x00004E28
		public Vector2 reactionForce
		{
			[NativeMethod("GetReactionForceFixedTime")]
			get
			{
				Vector2 result;
				this.get_reactionForce_Injected(out result);
				return result;
			}
		}

		// Token: 0x1700009D RID: 157
		// (get) Token: 0x060002FE RID: 766
		public extern float reactionTorque { [NativeMethod("GetReactionTorqueFixedTime")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060002FF RID: 767 RVA: 0x00006C40 File Offset: 0x00004E40
		public Vector2 GetReactionForce(float timeStep)
		{
			Vector2 result;
			this.GetReactionForce_Injected(timeStep, out result);
			return result;
		}

		// Token: 0x06000300 RID: 768
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetReactionTorque(float timeStep);

		// Token: 0x06000301 RID: 769
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_reactionForce_Injected(out Vector2 ret);

		// Token: 0x06000302 RID: 770
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetReactionForce_Injected(float timeStep, out Vector2 ret);
	}
}
