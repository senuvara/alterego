﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000021 RID: 33
	[NativeHeader("Modules/Physics2D/Public/CompositeCollider2D.h")]
	[RequireComponent(typeof(Rigidbody2D))]
	public sealed class CompositeCollider2D : Collider2D
	{
		// Token: 0x060002E3 RID: 739 RVA: 0x00006A03 File Offset: 0x00004C03
		public CompositeCollider2D()
		{
		}

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x060002E4 RID: 740
		// (set) Token: 0x060002E5 RID: 741
		public extern CompositeCollider2D.GeometryType geometryType { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x060002E6 RID: 742
		// (set) Token: 0x060002E7 RID: 743
		public extern CompositeCollider2D.GenerationType generationType { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000093 RID: 147
		// (get) Token: 0x060002E8 RID: 744
		// (set) Token: 0x060002E9 RID: 745
		public extern float vertexDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000094 RID: 148
		// (get) Token: 0x060002EA RID: 746
		// (set) Token: 0x060002EB RID: 747
		public extern float edgeRadius { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060002EC RID: 748
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void GenerateGeometry();

		// Token: 0x060002ED RID: 749 RVA: 0x00006B68 File Offset: 0x00004D68
		public int GetPathPointCount(int index)
		{
			int num = this.pathCount - 1;
			if (index < 0 || index > num)
			{
				throw new ArgumentOutOfRangeException("index", string.Format("Path index {0} must be in the range of 0 to {1}.", index, num));
			}
			return this.GetPathPointCount_Internal(index);
		}

		// Token: 0x060002EE RID: 750
		[NativeMethod("GetPathPointCount_Binding")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int GetPathPointCount_Internal(int index);

		// Token: 0x17000095 RID: 149
		// (get) Token: 0x060002EF RID: 751
		public extern int pathCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000096 RID: 150
		// (get) Token: 0x060002F0 RID: 752
		public extern int pointCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060002F1 RID: 753 RVA: 0x00006BBC File Offset: 0x00004DBC
		public int GetPath(int index, Vector2[] points)
		{
			if (index < 0 || index >= this.pathCount)
			{
				throw new ArgumentOutOfRangeException("index", string.Format("Path index {0} must be in the range of 0 to {1}.", index, this.pathCount - 1));
			}
			if (points == null)
			{
				throw new ArgumentNullException("points");
			}
			return this.GetPath_Internal(index, points);
		}

		// Token: 0x060002F2 RID: 754
		[NativeMethod("GetPath_Binding")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int GetPath_Internal(int index, [Out] Vector2[] points);

		// Token: 0x02000022 RID: 34
		public enum GeometryType
		{
			// Token: 0x04000079 RID: 121
			Outlines,
			// Token: 0x0400007A RID: 122
			Polygons
		}

		// Token: 0x02000023 RID: 35
		public enum GenerationType
		{
			// Token: 0x0400007C RID: 124
			Synchronous,
			// Token: 0x0400007D RID: 125
			Manual
		}
	}
}
