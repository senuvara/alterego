﻿using System;

namespace UnityEngine
{
	// Token: 0x0200000C RID: 12
	internal enum ColliderErrorState2D
	{
		// Token: 0x04000024 RID: 36
		None,
		// Token: 0x04000025 RID: 37
		NoShapes,
		// Token: 0x04000026 RID: 38
		RemovedShapes
	}
}
