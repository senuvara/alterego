﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000030 RID: 48
	[NativeHeader("Modules/Physics2D/AreaEffector2D.h")]
	public class AreaEffector2D : Effector2D
	{
		// Token: 0x0600038B RID: 907 RVA: 0x00006DE0 File Offset: 0x00004FE0
		public AreaEffector2D()
		{
		}

		// Token: 0x170000D8 RID: 216
		// (get) Token: 0x0600038C RID: 908
		// (set) Token: 0x0600038D RID: 909
		public extern float forceAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000D9 RID: 217
		// (get) Token: 0x0600038E RID: 910
		// (set) Token: 0x0600038F RID: 911
		public extern bool useGlobalAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000DA RID: 218
		// (get) Token: 0x06000390 RID: 912
		// (set) Token: 0x06000391 RID: 913
		public extern float forceMagnitude { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000DB RID: 219
		// (get) Token: 0x06000392 RID: 914
		// (set) Token: 0x06000393 RID: 915
		public extern float forceVariation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000DC RID: 220
		// (get) Token: 0x06000394 RID: 916
		// (set) Token: 0x06000395 RID: 917
		public extern float drag { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000DD RID: 221
		// (get) Token: 0x06000396 RID: 918
		// (set) Token: 0x06000397 RID: 919
		public extern float angularDrag { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000DE RID: 222
		// (get) Token: 0x06000398 RID: 920
		// (set) Token: 0x06000399 RID: 921
		public extern EffectorSelection2D forceTarget { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
