﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x02000032 RID: 50
	[NativeHeader("Modules/Physics2D/PointEffector2D.h")]
	public class PointEffector2D : Effector2D
	{
		// Token: 0x060003A9 RID: 937 RVA: 0x00006DE0 File Offset: 0x00004FE0
		public PointEffector2D()
		{
		}

		// Token: 0x170000E6 RID: 230
		// (get) Token: 0x060003AA RID: 938
		// (set) Token: 0x060003AB RID: 939
		public extern float forceMagnitude { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000E7 RID: 231
		// (get) Token: 0x060003AC RID: 940
		// (set) Token: 0x060003AD RID: 941
		public extern float forceVariation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000E8 RID: 232
		// (get) Token: 0x060003AE RID: 942
		// (set) Token: 0x060003AF RID: 943
		public extern float distanceScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000E9 RID: 233
		// (get) Token: 0x060003B0 RID: 944
		// (set) Token: 0x060003B1 RID: 945
		public extern float drag { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000EA RID: 234
		// (get) Token: 0x060003B2 RID: 946
		// (set) Token: 0x060003B3 RID: 947
		public extern float angularDrag { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000EB RID: 235
		// (get) Token: 0x060003B4 RID: 948
		// (set) Token: 0x060003B5 RID: 949
		public extern EffectorSelection2D forceSource { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000EC RID: 236
		// (get) Token: 0x060003B6 RID: 950
		// (set) Token: 0x060003B7 RID: 951
		public extern EffectorSelection2D forceTarget { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000ED RID: 237
		// (get) Token: 0x060003B8 RID: 952
		// (set) Token: 0x060003B9 RID: 953
		public extern EffectorForceMode2D forceMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
