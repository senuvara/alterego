﻿using System;

namespace UnityEngine
{
	// Token: 0x02000016 RID: 22
	public struct JointMotor2D
	{
		// Token: 0x17000040 RID: 64
		// (get) Token: 0x060001D0 RID: 464 RVA: 0x00005C84 File Offset: 0x00003E84
		// (set) Token: 0x060001D1 RID: 465 RVA: 0x00005C9F File Offset: 0x00003E9F
		public float motorSpeed
		{
			get
			{
				return this.m_MotorSpeed;
			}
			set
			{
				this.m_MotorSpeed = value;
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x060001D2 RID: 466 RVA: 0x00005CAC File Offset: 0x00003EAC
		// (set) Token: 0x060001D3 RID: 467 RVA: 0x00005CC7 File Offset: 0x00003EC7
		public float maxMotorTorque
		{
			get
			{
				return this.m_MaximumMotorTorque;
			}
			set
			{
				this.m_MaximumMotorTorque = value;
			}
		}

		// Token: 0x0400005C RID: 92
		private float m_MotorSpeed;

		// Token: 0x0400005D RID: 93
		private float m_MaximumMotorTorque;
	}
}
