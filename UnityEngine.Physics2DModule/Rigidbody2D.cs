﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x0200001A RID: 26
	[NativeHeader("Modules/Physics2D/Public/Rigidbody2D.h")]
	[RequireComponent(typeof(Transform))]
	public sealed class Rigidbody2D : Component
	{
		// Token: 0x0600020B RID: 523 RVA: 0x000061F1 File Offset: 0x000043F1
		public Rigidbody2D()
		{
		}

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x0600020C RID: 524 RVA: 0x000061FC File Offset: 0x000043FC
		// (set) Token: 0x0600020D RID: 525 RVA: 0x00006212 File Offset: 0x00004412
		public Vector2 position
		{
			get
			{
				Vector2 result;
				this.get_position_Injected(out result);
				return result;
			}
			set
			{
				this.set_position_Injected(ref value);
			}
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x0600020E RID: 526
		// (set) Token: 0x0600020F RID: 527
		public extern float rotation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000210 RID: 528 RVA: 0x0000621C File Offset: 0x0000441C
		public void MovePosition(Vector2 position)
		{
			this.MovePosition_Injected(ref position);
		}

		// Token: 0x06000211 RID: 529
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void MoveRotation(float angle);

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x06000212 RID: 530 RVA: 0x00006228 File Offset: 0x00004428
		// (set) Token: 0x06000213 RID: 531 RVA: 0x0000623E File Offset: 0x0000443E
		public Vector2 velocity
		{
			get
			{
				Vector2 result;
				this.get_velocity_Injected(out result);
				return result;
			}
			set
			{
				this.set_velocity_Injected(ref value);
			}
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x06000214 RID: 532
		// (set) Token: 0x06000215 RID: 533
		public extern float angularVelocity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x06000216 RID: 534
		// (set) Token: 0x06000217 RID: 535
		public extern bool useAutoMass { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x06000218 RID: 536
		// (set) Token: 0x06000219 RID: 537
		public extern float mass { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x0600021A RID: 538
		// (set) Token: 0x0600021B RID: 539
		[NativeMethod("Material")]
		public extern PhysicsMaterial2D sharedMaterial { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x0600021C RID: 540 RVA: 0x00006248 File Offset: 0x00004448
		// (set) Token: 0x0600021D RID: 541 RVA: 0x0000625E File Offset: 0x0000445E
		public Vector2 centerOfMass
		{
			get
			{
				Vector2 result;
				this.get_centerOfMass_Injected(out result);
				return result;
			}
			set
			{
				this.set_centerOfMass_Injected(ref value);
			}
		}

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x0600021E RID: 542 RVA: 0x00006268 File Offset: 0x00004468
		public Vector2 worldCenterOfMass
		{
			get
			{
				Vector2 result;
				this.get_worldCenterOfMass_Injected(out result);
				return result;
			}
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x0600021F RID: 543
		// (set) Token: 0x06000220 RID: 544
		public extern float inertia { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x06000221 RID: 545
		// (set) Token: 0x06000222 RID: 546
		public extern float drag { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x06000223 RID: 547
		// (set) Token: 0x06000224 RID: 548
		public extern float angularDrag { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x06000225 RID: 549
		// (set) Token: 0x06000226 RID: 550
		public extern float gravityScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x06000227 RID: 551
		// (set) Token: 0x06000228 RID: 552
		public extern RigidbodyType2D bodyType { [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeMethod("SetBodyType_Binding")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000229 RID: 553
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetDragBehaviour(bool dragged);

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x0600022A RID: 554
		// (set) Token: 0x0600022B RID: 555
		public extern bool useFullKinematicContacts { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x0600022C RID: 556 RVA: 0x00006280 File Offset: 0x00004480
		// (set) Token: 0x0600022D RID: 557 RVA: 0x0000629E File Offset: 0x0000449E
		public bool isKinematic
		{
			get
			{
				return this.bodyType == RigidbodyType2D.Kinematic;
			}
			set
			{
				this.bodyType = ((!value) ? RigidbodyType2D.Dynamic : RigidbodyType2D.Kinematic);
			}
		}

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x0600022E RID: 558
		// (set) Token: 0x0600022F RID: 559
		[NativeMethod("FreezeRotation")]
		[Obsolete("'fixedAngle' is no longer supported. Use constraints instead.", false)]
		public extern bool fixedAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x06000230 RID: 560
		// (set) Token: 0x06000231 RID: 561
		public extern bool freezeRotation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x06000232 RID: 562
		// (set) Token: 0x06000233 RID: 563
		public extern RigidbodyConstraints2D constraints { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000234 RID: 564
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsSleeping();

		// Token: 0x06000235 RID: 565
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsAwake();

		// Token: 0x06000236 RID: 566
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Sleep();

		// Token: 0x06000237 RID: 567
		[NativeMethod("Wake")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void WakeUp();

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x06000238 RID: 568
		// (set) Token: 0x06000239 RID: 569
		public extern bool simulated { [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeMethod("SetSimulated_Binding")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x0600023A RID: 570
		// (set) Token: 0x0600023B RID: 571
		public extern RigidbodyInterpolation2D interpolation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x0600023C RID: 572
		// (set) Token: 0x0600023D RID: 573
		public extern RigidbodySleepMode2D sleepMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x0600023E RID: 574
		// (set) Token: 0x0600023F RID: 575
		public extern CollisionDetectionMode2D collisionDetectionMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x06000240 RID: 576
		public extern int attachedColliderCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000241 RID: 577
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsTouching([NotNull] [Writable] Collider2D collider);

		// Token: 0x06000242 RID: 578 RVA: 0x000062B4 File Offset: 0x000044B4
		public bool IsTouching([Writable] Collider2D collider, ContactFilter2D contactFilter)
		{
			return this.IsTouching_OtherColliderWithFilter_Internal(collider, contactFilter);
		}

		// Token: 0x06000243 RID: 579 RVA: 0x000062D1 File Offset: 0x000044D1
		[NativeMethod("IsTouching")]
		private bool IsTouching_OtherColliderWithFilter_Internal([NotNull] [Writable] Collider2D collider, ContactFilter2D contactFilter)
		{
			return this.IsTouching_OtherColliderWithFilter_Internal_Injected(collider, ref contactFilter);
		}

		// Token: 0x06000244 RID: 580 RVA: 0x000062DC File Offset: 0x000044DC
		public bool IsTouching(ContactFilter2D contactFilter)
		{
			return this.IsTouching_AnyColliderWithFilter_Internal(contactFilter);
		}

		// Token: 0x06000245 RID: 581 RVA: 0x000062F8 File Offset: 0x000044F8
		[NativeMethod("IsTouching")]
		private bool IsTouching_AnyColliderWithFilter_Internal(ContactFilter2D contactFilter)
		{
			return this.IsTouching_AnyColliderWithFilter_Internal_Injected(ref contactFilter);
		}

		// Token: 0x06000246 RID: 582 RVA: 0x00006304 File Offset: 0x00004504
		[ExcludeFromDocs]
		public bool IsTouchingLayers()
		{
			return this.IsTouchingLayers(-1);
		}

		// Token: 0x06000247 RID: 583
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsTouchingLayers([DefaultValue("Physics2D.AllLayers")] int layerMask);

		// Token: 0x06000248 RID: 584 RVA: 0x00006320 File Offset: 0x00004520
		public bool OverlapPoint(Vector2 point)
		{
			return this.OverlapPoint_Injected(ref point);
		}

		// Token: 0x06000249 RID: 585 RVA: 0x0000632C File Offset: 0x0000452C
		public ColliderDistance2D Distance([Writable] Collider2D collider)
		{
			if (collider == null)
			{
				throw new ArgumentNullException("Collider cannot be null.");
			}
			if (collider.attachedRigidbody == this)
			{
				throw new ArgumentException("The collider cannot be attached to the Rigidbody2D being searched.");
			}
			return this.Distance_Internal(collider);
		}

		// Token: 0x0600024A RID: 586 RVA: 0x0000637C File Offset: 0x0000457C
		[NativeMethod("Distance")]
		private ColliderDistance2D Distance_Internal([NotNull] [Writable] Collider2D collider)
		{
			ColliderDistance2D result;
			this.Distance_Internal_Injected(collider, out result);
			return result;
		}

		// Token: 0x0600024B RID: 587 RVA: 0x00006393 File Offset: 0x00004593
		[ExcludeFromDocs]
		public void AddForce(Vector2 force)
		{
			this.AddForce(force, ForceMode2D.Force);
		}

		// Token: 0x0600024C RID: 588 RVA: 0x0000639E File Offset: 0x0000459E
		public void AddForce(Vector2 force, [DefaultValue("ForceMode2D.Force")] ForceMode2D mode)
		{
			this.AddForce_Injected(ref force, mode);
		}

		// Token: 0x0600024D RID: 589 RVA: 0x000063A9 File Offset: 0x000045A9
		[ExcludeFromDocs]
		public void AddRelativeForce(Vector2 relativeForce)
		{
			this.AddRelativeForce(relativeForce, ForceMode2D.Force);
		}

		// Token: 0x0600024E RID: 590 RVA: 0x000063B4 File Offset: 0x000045B4
		public void AddRelativeForce(Vector2 relativeForce, [DefaultValue("ForceMode2D.Force")] ForceMode2D mode)
		{
			this.AddRelativeForce_Injected(ref relativeForce, mode);
		}

		// Token: 0x0600024F RID: 591 RVA: 0x000063BF File Offset: 0x000045BF
		[ExcludeFromDocs]
		public void AddForceAtPosition(Vector2 force, Vector2 position)
		{
			this.AddForceAtPosition(force, position, ForceMode2D.Force);
		}

		// Token: 0x06000250 RID: 592 RVA: 0x000063CB File Offset: 0x000045CB
		public void AddForceAtPosition(Vector2 force, Vector2 position, [DefaultValue("ForceMode2D.Force")] ForceMode2D mode)
		{
			this.AddForceAtPosition_Injected(ref force, ref position, mode);
		}

		// Token: 0x06000251 RID: 593 RVA: 0x000063D8 File Offset: 0x000045D8
		[ExcludeFromDocs]
		public void AddTorque(float torque)
		{
			this.AddTorque(torque, ForceMode2D.Force);
		}

		// Token: 0x06000252 RID: 594
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void AddTorque(float torque, [DefaultValue("ForceMode2D.Force")] ForceMode2D mode);

		// Token: 0x06000253 RID: 595 RVA: 0x000063E4 File Offset: 0x000045E4
		public Vector2 GetPoint(Vector2 point)
		{
			Vector2 result;
			this.GetPoint_Injected(ref point, out result);
			return result;
		}

		// Token: 0x06000254 RID: 596 RVA: 0x000063FC File Offset: 0x000045FC
		public Vector2 GetRelativePoint(Vector2 relativePoint)
		{
			Vector2 result;
			this.GetRelativePoint_Injected(ref relativePoint, out result);
			return result;
		}

		// Token: 0x06000255 RID: 597 RVA: 0x00006414 File Offset: 0x00004614
		public Vector2 GetVector(Vector2 vector)
		{
			Vector2 result;
			this.GetVector_Injected(ref vector, out result);
			return result;
		}

		// Token: 0x06000256 RID: 598 RVA: 0x0000642C File Offset: 0x0000462C
		public Vector2 GetRelativeVector(Vector2 relativeVector)
		{
			Vector2 result;
			this.GetRelativeVector_Injected(ref relativeVector, out result);
			return result;
		}

		// Token: 0x06000257 RID: 599 RVA: 0x00006444 File Offset: 0x00004644
		public Vector2 GetPointVelocity(Vector2 point)
		{
			Vector2 result;
			this.GetPointVelocity_Injected(ref point, out result);
			return result;
		}

		// Token: 0x06000258 RID: 600 RVA: 0x0000645C File Offset: 0x0000465C
		public Vector2 GetRelativePointVelocity(Vector2 relativePoint)
		{
			Vector2 result;
			this.GetRelativePointVelocity_Injected(ref relativePoint, out result);
			return result;
		}

		// Token: 0x06000259 RID: 601 RVA: 0x00006474 File Offset: 0x00004674
		[NativeMethod("OverlapCollider_Binding")]
		public int OverlapCollider(ContactFilter2D contactFilter, [Out] Collider2D[] results)
		{
			return this.OverlapCollider_Injected(ref contactFilter, results);
		}

		// Token: 0x0600025A RID: 602 RVA: 0x00006480 File Offset: 0x00004680
		public int GetContacts(ContactPoint2D[] contacts)
		{
			return Physics2D.GetContacts(this, default(ContactFilter2D).NoFilter(), contacts);
		}

		// Token: 0x0600025B RID: 603 RVA: 0x000064AC File Offset: 0x000046AC
		public int GetContacts(ContactFilter2D contactFilter, ContactPoint2D[] contacts)
		{
			return Physics2D.GetContacts(this, contactFilter, contacts);
		}

		// Token: 0x0600025C RID: 604 RVA: 0x000064CC File Offset: 0x000046CC
		public int GetContacts(Collider2D[] colliders)
		{
			return Physics2D.GetContacts(this, default(ContactFilter2D).NoFilter(), colliders);
		}

		// Token: 0x0600025D RID: 605 RVA: 0x000064F8 File Offset: 0x000046F8
		public int GetContacts(ContactFilter2D contactFilter, Collider2D[] colliders)
		{
			return Physics2D.GetContacts(this, contactFilter, colliders);
		}

		// Token: 0x0600025E RID: 606
		[NativeMethod("GetAttachedColliders_Binding")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetAttachedColliders([Out] Collider2D[] results);

		// Token: 0x0600025F RID: 607 RVA: 0x00006518 File Offset: 0x00004718
		[ExcludeFromDocs]
		public int Cast(Vector2 direction, RaycastHit2D[] results)
		{
			return this.Cast_Internal(direction, float.PositiveInfinity, results);
		}

		// Token: 0x06000260 RID: 608 RVA: 0x0000653C File Offset: 0x0000473C
		public int Cast(Vector2 direction, RaycastHit2D[] results, [DefaultValue("Mathf.Infinity")] float distance)
		{
			return this.Cast_Internal(direction, distance, results);
		}

		// Token: 0x06000261 RID: 609 RVA: 0x0000655C File Offset: 0x0000475C
		[ExcludeFromDocs]
		public int Cast(Vector2 direction, ContactFilter2D contactFilter, RaycastHit2D[] results)
		{
			return this.CastFiltered_Internal(direction, float.PositiveInfinity, contactFilter, results);
		}

		// Token: 0x06000262 RID: 610 RVA: 0x00006580 File Offset: 0x00004780
		public int Cast(Vector2 direction, ContactFilter2D contactFilter, RaycastHit2D[] results, [DefaultValue("Mathf.Infinity")] float distance)
		{
			return this.CastFiltered_Internal(direction, distance, contactFilter, results);
		}

		// Token: 0x06000263 RID: 611 RVA: 0x000065A0 File Offset: 0x000047A0
		[NativeMethod("Cast_Binding")]
		private int Cast_Internal(Vector2 direction, [DefaultValue("Mathf.Infinity")] float distance, [Out] RaycastHit2D[] results)
		{
			return this.Cast_Internal_Injected(ref direction, distance, results);
		}

		// Token: 0x06000264 RID: 612 RVA: 0x000065AC File Offset: 0x000047AC
		[NativeMethod("CastFiltered_Binding")]
		private int CastFiltered_Internal(Vector2 direction, float distance, ContactFilter2D contactFilter, [Out] RaycastHit2D[] results)
		{
			return this.CastFiltered_Internal_Injected(ref direction, distance, ref contactFilter, results);
		}

		// Token: 0x06000265 RID: 613
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_position_Injected(out Vector2 ret);

		// Token: 0x06000266 RID: 614
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_position_Injected(ref Vector2 value);

		// Token: 0x06000267 RID: 615
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void MovePosition_Injected(ref Vector2 position);

		// Token: 0x06000268 RID: 616
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_velocity_Injected(out Vector2 ret);

		// Token: 0x06000269 RID: 617
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_velocity_Injected(ref Vector2 value);

		// Token: 0x0600026A RID: 618
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_centerOfMass_Injected(out Vector2 ret);

		// Token: 0x0600026B RID: 619
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_centerOfMass_Injected(ref Vector2 value);

		// Token: 0x0600026C RID: 620
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_worldCenterOfMass_Injected(out Vector2 ret);

		// Token: 0x0600026D RID: 621
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool IsTouching_OtherColliderWithFilter_Internal_Injected([Writable] Collider2D collider, ref ContactFilter2D contactFilter);

		// Token: 0x0600026E RID: 622
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool IsTouching_AnyColliderWithFilter_Internal_Injected(ref ContactFilter2D contactFilter);

		// Token: 0x0600026F RID: 623
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool OverlapPoint_Injected(ref Vector2 point);

		// Token: 0x06000270 RID: 624
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Distance_Internal_Injected([Writable] Collider2D collider, out ColliderDistance2D ret);

		// Token: 0x06000271 RID: 625
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void AddForce_Injected(ref Vector2 force, [DefaultValue("ForceMode2D.Force")] ForceMode2D mode);

		// Token: 0x06000272 RID: 626
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void AddRelativeForce_Injected(ref Vector2 relativeForce, [DefaultValue("ForceMode2D.Force")] ForceMode2D mode);

		// Token: 0x06000273 RID: 627
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void AddForceAtPosition_Injected(ref Vector2 force, ref Vector2 position, [DefaultValue("ForceMode2D.Force")] ForceMode2D mode);

		// Token: 0x06000274 RID: 628
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetPoint_Injected(ref Vector2 point, out Vector2 ret);

		// Token: 0x06000275 RID: 629
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetRelativePoint_Injected(ref Vector2 relativePoint, out Vector2 ret);

		// Token: 0x06000276 RID: 630
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetVector_Injected(ref Vector2 vector, out Vector2 ret);

		// Token: 0x06000277 RID: 631
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetRelativeVector_Injected(ref Vector2 relativeVector, out Vector2 ret);

		// Token: 0x06000278 RID: 632
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetPointVelocity_Injected(ref Vector2 point, out Vector2 ret);

		// Token: 0x06000279 RID: 633
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetRelativePointVelocity_Injected(ref Vector2 relativePoint, out Vector2 ret);

		// Token: 0x0600027A RID: 634
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int OverlapCollider_Injected(ref ContactFilter2D contactFilter, [Out] Collider2D[] results);

		// Token: 0x0600027B RID: 635
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int Cast_Internal_Injected(ref Vector2 direction, [DefaultValue("Mathf.Infinity")] float distance, [Out] RaycastHit2D[] results);

		// Token: 0x0600027C RID: 636
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int CastFiltered_Internal_Injected(ref Vector2 direction, float distance, ref ContactFilter2D contactFilter, [Out] RaycastHit2D[] results);
	}
}
