﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x02000020 RID: 32
	[NativeHeader("Modules/Physics2D/Public/PolygonCollider2D.h")]
	public sealed class PolygonCollider2D : Collider2D
	{
		// Token: 0x060002D2 RID: 722 RVA: 0x00006A03 File Offset: 0x00004C03
		public PolygonCollider2D()
		{
		}

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x060002D3 RID: 723
		// (set) Token: 0x060002D4 RID: 724
		public extern bool autoTiling { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060002D5 RID: 725
		[NativeMethod("GetPointCount")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetTotalPointCount();

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x060002D6 RID: 726
		// (set) Token: 0x060002D7 RID: 727
		public extern Vector2[] points { [NativeMethod("GetPoints_Binding")] [MethodImpl(MethodImplOptions.InternalCall)] get; [NativeMethod("SetPoints_Binding")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x060002D8 RID: 728
		// (set) Token: 0x060002D9 RID: 729
		public extern int pathCount { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060002DA RID: 730 RVA: 0x00006A4C File Offset: 0x00004C4C
		public Vector2[] GetPath(int index)
		{
			if (index >= this.pathCount)
			{
				throw new ArgumentOutOfRangeException(string.Format("Path {0} does not exist.", index));
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException(string.Format("Path {0} does not exist; negative path index is invalid.", index));
			}
			return this.GetPath_Internal(index);
		}

		// Token: 0x060002DB RID: 731 RVA: 0x00006AA7 File Offset: 0x00004CA7
		public void SetPath(int index, Vector2[] points)
		{
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException(string.Format("Negative path index {0} is invalid.", index));
			}
			this.SetPath_Internal(index, points);
		}

		// Token: 0x060002DC RID: 732
		[NativeMethod("GetPath_Binding")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Vector2[] GetPath_Internal(int index);

		// Token: 0x060002DD RID: 733
		[NativeMethod("SetPath_Binding")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetPath_Internal(int index, Vector2[] points);

		// Token: 0x060002DE RID: 734 RVA: 0x00006ACF File Offset: 0x00004CCF
		[ExcludeFromDocs]
		public void CreatePrimitive(int sides)
		{
			this.CreatePrimitive(sides, Vector2.one, Vector2.zero);
		}

		// Token: 0x060002DF RID: 735 RVA: 0x00006AE3 File Offset: 0x00004CE3
		[ExcludeFromDocs]
		public void CreatePrimitive(int sides, Vector2 scale)
		{
			this.CreatePrimitive(sides, scale, Vector2.zero);
		}

		// Token: 0x060002E0 RID: 736 RVA: 0x00006AF4 File Offset: 0x00004CF4
		public void CreatePrimitive(int sides, [DefaultValue("Vector2.one")] Vector2 scale, [DefaultValue("Vector2.zero")] Vector2 offset)
		{
			if (sides < 3)
			{
				Debug.LogWarning("Cannot create a 2D polygon primitive collider with less than two sides.", this);
			}
			else if (scale.x <= 0f || scale.y <= 0f)
			{
				Debug.LogWarning("Cannot create a 2D polygon primitive collider with an axis scale less than or equal to zero.", this);
			}
			else
			{
				this.CreatePrimitive_Internal(sides, scale, offset, true);
			}
		}

		// Token: 0x060002E1 RID: 737 RVA: 0x00006B57 File Offset: 0x00004D57
		[NativeMethod("CreatePrimitive")]
		private void CreatePrimitive_Internal(int sides, [DefaultValue("Vector2.one")] Vector2 scale, [DefaultValue("Vector2.zero")] Vector2 offset, bool autoRefresh)
		{
			this.CreatePrimitive_Internal_Injected(sides, ref scale, ref offset, autoRefresh);
		}

		// Token: 0x060002E2 RID: 738
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void CreatePrimitive_Internal_Injected(int sides, [DefaultValue("Vector2.one")] ref Vector2 scale, [DefaultValue("Vector2.zero")] ref Vector2 offset, bool autoRefresh);
	}
}
