﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200002C RID: 44
	[NativeHeader("Modules/Physics2D/TargetJoint2D.h")]
	public sealed class TargetJoint2D : Joint2D
	{
		// Token: 0x0600035C RID: 860 RVA: 0x00006C57 File Offset: 0x00004E57
		public TargetJoint2D()
		{
		}

		// Token: 0x170000C3 RID: 195
		// (get) Token: 0x0600035D RID: 861 RVA: 0x00006D60 File Offset: 0x00004F60
		// (set) Token: 0x0600035E RID: 862 RVA: 0x00006D76 File Offset: 0x00004F76
		public Vector2 anchor
		{
			get
			{
				Vector2 result;
				this.get_anchor_Injected(out result);
				return result;
			}
			set
			{
				this.set_anchor_Injected(ref value);
			}
		}

		// Token: 0x170000C4 RID: 196
		// (get) Token: 0x0600035F RID: 863 RVA: 0x00006D80 File Offset: 0x00004F80
		// (set) Token: 0x06000360 RID: 864 RVA: 0x00006D96 File Offset: 0x00004F96
		public Vector2 target
		{
			get
			{
				Vector2 result;
				this.get_target_Injected(out result);
				return result;
			}
			set
			{
				this.set_target_Injected(ref value);
			}
		}

		// Token: 0x170000C5 RID: 197
		// (get) Token: 0x06000361 RID: 865
		// (set) Token: 0x06000362 RID: 866
		public extern bool autoConfigureTarget { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000C6 RID: 198
		// (get) Token: 0x06000363 RID: 867
		// (set) Token: 0x06000364 RID: 868
		public extern float maxForce { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000C7 RID: 199
		// (get) Token: 0x06000365 RID: 869
		// (set) Token: 0x06000366 RID: 870
		public extern float dampingRatio { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000C8 RID: 200
		// (get) Token: 0x06000367 RID: 871
		// (set) Token: 0x06000368 RID: 872
		public extern float frequency { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000369 RID: 873
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_anchor_Injected(out Vector2 ret);

		// Token: 0x0600036A RID: 874
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_anchor_Injected(ref Vector2 value);

		// Token: 0x0600036B RID: 875
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_target_Injected(out Vector2 ret);

		// Token: 0x0600036C RID: 876
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_target_Injected(ref Vector2 value);
	}
}
