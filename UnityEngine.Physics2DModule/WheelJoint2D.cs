﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200002E RID: 46
	[NativeHeader("Modules/Physics2D/WheelJoint2D.h")]
	public sealed class WheelJoint2D : AnchoredJoint2D
	{
		// Token: 0x06000373 RID: 883 RVA: 0x00006CA0 File Offset: 0x00004EA0
		public WheelJoint2D()
		{
		}

		// Token: 0x170000CC RID: 204
		// (get) Token: 0x06000374 RID: 884 RVA: 0x00006DA0 File Offset: 0x00004FA0
		// (set) Token: 0x06000375 RID: 885 RVA: 0x00006DB6 File Offset: 0x00004FB6
		public JointSuspension2D suspension
		{
			get
			{
				JointSuspension2D result;
				this.get_suspension_Injected(out result);
				return result;
			}
			set
			{
				this.set_suspension_Injected(ref value);
			}
		}

		// Token: 0x170000CD RID: 205
		// (get) Token: 0x06000376 RID: 886
		// (set) Token: 0x06000377 RID: 887
		public extern bool useMotor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000CE RID: 206
		// (get) Token: 0x06000378 RID: 888 RVA: 0x00006DC0 File Offset: 0x00004FC0
		// (set) Token: 0x06000379 RID: 889 RVA: 0x00006DD6 File Offset: 0x00004FD6
		public JointMotor2D motor
		{
			get
			{
				JointMotor2D result;
				this.get_motor_Injected(out result);
				return result;
			}
			set
			{
				this.set_motor_Injected(ref value);
			}
		}

		// Token: 0x170000CF RID: 207
		// (get) Token: 0x0600037A RID: 890
		public extern float jointTranslation { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000D0 RID: 208
		// (get) Token: 0x0600037B RID: 891
		public extern float jointLinearSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000D1 RID: 209
		// (get) Token: 0x0600037C RID: 892
		public extern float jointSpeed { [NativeMethod("GetJointAngularSpeed")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000D2 RID: 210
		// (get) Token: 0x0600037D RID: 893
		public extern float jointAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600037E RID: 894
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetMotorTorque(float timeStep);

		// Token: 0x0600037F RID: 895
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_suspension_Injected(out JointSuspension2D ret);

		// Token: 0x06000380 RID: 896
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_suspension_Injected(ref JointSuspension2D value);

		// Token: 0x06000381 RID: 897
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_motor_Injected(out JointMotor2D ret);

		// Token: 0x06000382 RID: 898
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_motor_Injected(ref JointMotor2D value);
	}
}
