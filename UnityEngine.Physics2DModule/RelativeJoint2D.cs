﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine
{
	// Token: 0x0200002A RID: 42
	[NativeHeader("Modules/Physics2D/RelativeJoint2D.h")]
	public sealed class RelativeJoint2D : Joint2D
	{
		// Token: 0x06000335 RID: 821 RVA: 0x00006C57 File Offset: 0x00004E57
		public RelativeJoint2D()
		{
		}

		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x06000336 RID: 822
		// (set) Token: 0x06000337 RID: 823
		public extern float maxForce { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x06000338 RID: 824
		// (set) Token: 0x06000339 RID: 825
		public extern float maxTorque { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000B4 RID: 180
		// (get) Token: 0x0600033A RID: 826
		// (set) Token: 0x0600033B RID: 827
		public extern float correctionScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x0600033C RID: 828
		// (set) Token: 0x0600033D RID: 829
		public extern bool autoConfigureOffset { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000B6 RID: 182
		// (get) Token: 0x0600033E RID: 830 RVA: 0x00006CE8 File Offset: 0x00004EE8
		// (set) Token: 0x0600033F RID: 831 RVA: 0x00006CFE File Offset: 0x00004EFE
		public Vector2 linearOffset
		{
			get
			{
				Vector2 result;
				this.get_linearOffset_Injected(out result);
				return result;
			}
			set
			{
				this.set_linearOffset_Injected(ref value);
			}
		}

		// Token: 0x170000B7 RID: 183
		// (get) Token: 0x06000340 RID: 832
		// (set) Token: 0x06000341 RID: 833
		public extern float angularOffset { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000B8 RID: 184
		// (get) Token: 0x06000342 RID: 834 RVA: 0x00006D08 File Offset: 0x00004F08
		public Vector2 target
		{
			get
			{
				Vector2 result;
				this.get_target_Injected(out result);
				return result;
			}
		}

		// Token: 0x06000343 RID: 835
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_linearOffset_Injected(out Vector2 ret);

		// Token: 0x06000344 RID: 836
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_linearOffset_Injected(ref Vector2 value);

		// Token: 0x06000345 RID: 837
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_target_Injected(out Vector2 ret);
	}
}
