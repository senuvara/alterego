﻿using System;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000018 RID: 24
	[NativeClass("RaycastHit2D", "struct RaycastHit2D;")]
	[NativeHeader("Runtime/Interfaces/IPhysics2D.h")]
	[RequiredByNativeCode(Optional = true, GenerateProxy = true)]
	public struct RaycastHit2D
	{
		// Token: 0x17000045 RID: 69
		// (get) Token: 0x060001DA RID: 474 RVA: 0x00005D4C File Offset: 0x00003F4C
		// (set) Token: 0x060001DB RID: 475 RVA: 0x00005D67 File Offset: 0x00003F67
		public Vector2 centroid
		{
			get
			{
				return this.m_Centroid;
			}
			set
			{
				this.m_Centroid = value;
			}
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x060001DC RID: 476 RVA: 0x00005D74 File Offset: 0x00003F74
		// (set) Token: 0x060001DD RID: 477 RVA: 0x00005D8F File Offset: 0x00003F8F
		public Vector2 point
		{
			get
			{
				return this.m_Point;
			}
			set
			{
				this.m_Point = value;
			}
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x060001DE RID: 478 RVA: 0x00005D9C File Offset: 0x00003F9C
		// (set) Token: 0x060001DF RID: 479 RVA: 0x00005DB7 File Offset: 0x00003FB7
		public Vector2 normal
		{
			get
			{
				return this.m_Normal;
			}
			set
			{
				this.m_Normal = value;
			}
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x060001E0 RID: 480 RVA: 0x00005DC4 File Offset: 0x00003FC4
		// (set) Token: 0x060001E1 RID: 481 RVA: 0x00005DDF File Offset: 0x00003FDF
		public float distance
		{
			get
			{
				return this.m_Distance;
			}
			set
			{
				this.m_Distance = value;
			}
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x060001E2 RID: 482 RVA: 0x00005DEC File Offset: 0x00003FEC
		// (set) Token: 0x060001E3 RID: 483 RVA: 0x00005E07 File Offset: 0x00004007
		public float fraction
		{
			get
			{
				return this.m_Fraction;
			}
			set
			{
				this.m_Fraction = value;
			}
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x060001E4 RID: 484 RVA: 0x00005E14 File Offset: 0x00004014
		public Collider2D collider
		{
			get
			{
				return Object.FindObjectFromInstanceID(this.m_Collider) as Collider2D;
			}
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x060001E5 RID: 485 RVA: 0x00005E3C File Offset: 0x0000403C
		public Rigidbody2D rigidbody
		{
			get
			{
				return (!(this.collider != null)) ? null : this.collider.attachedRigidbody;
			}
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x060001E6 RID: 486 RVA: 0x00005E74 File Offset: 0x00004074
		public Transform transform
		{
			get
			{
				Rigidbody2D rigidbody = this.rigidbody;
				Transform result;
				if (rigidbody != null)
				{
					result = rigidbody.transform;
				}
				else if (this.collider != null)
				{
					result = this.collider.transform;
				}
				else
				{
					result = null;
				}
				return result;
			}
		}

		// Token: 0x060001E7 RID: 487 RVA: 0x00005ECC File Offset: 0x000040CC
		public static implicit operator bool(RaycastHit2D hit)
		{
			return hit.collider != null;
		}

		// Token: 0x060001E8 RID: 488 RVA: 0x00005EF0 File Offset: 0x000040F0
		public int CompareTo(RaycastHit2D other)
		{
			int result;
			if (this.collider == null)
			{
				result = 1;
			}
			else if (other.collider == null)
			{
				result = -1;
			}
			else
			{
				result = this.fraction.CompareTo(other.fraction);
			}
			return result;
		}

		// Token: 0x04000061 RID: 97
		[NativeName("centroid")]
		private Vector2 m_Centroid;

		// Token: 0x04000062 RID: 98
		[NativeName("point")]
		private Vector2 m_Point;

		// Token: 0x04000063 RID: 99
		[NativeName("normal")]
		private Vector2 m_Normal;

		// Token: 0x04000064 RID: 100
		[NativeName("distance")]
		private float m_Distance;

		// Token: 0x04000065 RID: 101
		[NativeName("fraction")]
		private float m_Fraction;

		// Token: 0x04000066 RID: 102
		[NativeName("collider")]
		private int m_Collider;
	}
}
