﻿using System;

namespace UnityEngine
{
	// Token: 0x02000007 RID: 7
	public enum RigidbodyInterpolation2D
	{
		// Token: 0x04000011 RID: 17
		None,
		// Token: 0x04000012 RID: 18
		Interpolate,
		// Token: 0x04000013 RID: 19
		Extrapolate
	}
}
