﻿using System;
using System.ComponentModel;

namespace UnityEngine
{
	// Token: 0x02000009 RID: 9
	public enum CollisionDetectionMode2D
	{
		// Token: 0x04000019 RID: 25
		[Obsolete("Enum member CollisionDetectionMode2D.None has been deprecated. Use CollisionDetectionMode2D.Discrete instead (UnityUpgradable) -> Discrete", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		None,
		// Token: 0x0400001A RID: 26
		Discrete = 0,
		// Token: 0x0400001B RID: 27
		Continuous
	}
}
