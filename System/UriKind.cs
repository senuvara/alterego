﻿using System;

namespace System
{
	/// <summary>Defines the kinds of <see cref="T:System.Uri" />s for the <see cref="M:System.Uri.IsWellFormedUriString(System.String,System.UriKind)" /> and several <see cref="Overload:System.Uri.#ctor" /> methods.</summary>
	// Token: 0x02000099 RID: 153
	public enum UriKind
	{
		/// <summary>The kind of the Uri is indeterminate.</summary>
		// Token: 0x040008C4 RID: 2244
		RelativeOrAbsolute,
		/// <summary>The Uri is an absolute Uri.</summary>
		// Token: 0x040008C5 RID: 2245
		Absolute,
		/// <summary>The Uri is a relative Uri.</summary>
		// Token: 0x040008C6 RID: 2246
		Relative
	}
}
