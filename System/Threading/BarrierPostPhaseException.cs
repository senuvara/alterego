﻿using System;
using System.Runtime.Serialization;
using System.Security;

namespace System.Threading
{
	/// <summary>The exception that is thrown when the post-phase action of a <see cref="T:System.Threading.Barrier" /> fails</summary>
	// Token: 0x020000BD RID: 189
	[Serializable]
	public class BarrierPostPhaseException : Exception
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Threading.BarrierPostPhaseException" /> class with a system-supplied message that describes the error.</summary>
		// Token: 0x0600045A RID: 1114 RVA: 0x000159F9 File Offset: 0x00013BF9
		public BarrierPostPhaseException() : this(null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Threading.BarrierPostPhaseException" /> class with the specified inner exception.</summary>
		/// <param name="innerException">The exception that is the cause of the current exception.</param>
		// Token: 0x0600045B RID: 1115 RVA: 0x00015A02 File Offset: 0x00013C02
		public BarrierPostPhaseException(Exception innerException) : this(null, innerException)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Threading.BarrierPostPhaseException" /> class with a specified message that describes the error.</summary>
		/// <param name="message">The message that describes the exception. The caller of this constructor is required to ensure that this string has been localized for the current system culture.</param>
		// Token: 0x0600045C RID: 1116 RVA: 0x00015A0C File Offset: 0x00013C0C
		public BarrierPostPhaseException(string message) : this(message, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Threading.BarrierPostPhaseException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The message that describes the exception. The caller of this constructor is required to ensure that this string has been localized for the current system culture. </param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not <see langword="null" />, the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x0600045D RID: 1117 RVA: 0x00015A16 File Offset: 0x00013C16
		public BarrierPostPhaseException(string message, Exception innerException) : base((message == null) ? SR.GetString("The postPhaseAction failed with an exception.") : message, innerException)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Threading.BarrierPostPhaseException" /> class with serialized data.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		// Token: 0x0600045E RID: 1118 RVA: 0x00015A2F File Offset: 0x00013C2F
		[SecurityCritical]
		protected BarrierPostPhaseException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
