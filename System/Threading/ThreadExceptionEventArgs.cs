﻿using System;

namespace System.Threading
{
	/// <summary>Provides data for the <see cref="E:System.Windows.Forms.Application.ThreadException" /> event.</summary>
	// Token: 0x020000C1 RID: 193
	public class ThreadExceptionEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Threading.ThreadExceptionEventArgs" /> class.</summary>
		/// <param name="t">The <see cref="T:System.Exception" /> that occurred. </param>
		// Token: 0x0600048A RID: 1162 RVA: 0x00016659 File Offset: 0x00014859
		public ThreadExceptionEventArgs(Exception t)
		{
			this.exception = t;
		}

		/// <summary>Gets the <see cref="T:System.Exception" /> that occurred.</summary>
		/// <returns>The <see cref="T:System.Exception" /> that occurred.</returns>
		// Token: 0x170000D8 RID: 216
		// (get) Token: 0x0600048B RID: 1163 RVA: 0x00016668 File Offset: 0x00014868
		public Exception Exception
		{
			get
			{
				return this.exception;
			}
		}

		// Token: 0x04000987 RID: 2439
		private Exception exception;
	}
}
