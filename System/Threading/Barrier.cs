﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;

namespace System.Threading
{
	/// <summary>Enables multiple tasks to cooperatively work on an algorithm in parallel through multiple phases.</summary>
	// Token: 0x020000BE RID: 190
	[ComVisible(false)]
	[DebuggerDisplay("Participant Count={ParticipantCount},Participants Remaining={ParticipantsRemaining}")]
	[HostProtection(SecurityAction.LinkDemand, Synchronization = true, ExternalThreading = true)]
	public class Barrier : IDisposable
	{
		/// <summary>Gets the number of participants in the barrier that haven’t yet signaled in the current phase.</summary>
		/// <returns>Returns the number of participants in the barrier that haven’t yet signaled in the current phase.</returns>
		// Token: 0x170000D5 RID: 213
		// (get) Token: 0x0600045F RID: 1119 RVA: 0x00015A3C File Offset: 0x00013C3C
		public int ParticipantsRemaining
		{
			get
			{
				int currentTotalCount = this.m_currentTotalCount;
				int num = currentTotalCount & 32767;
				int num2 = (currentTotalCount & 2147418112) >> 16;
				return num - num2;
			}
		}

		/// <summary>Gets the total number of participants in the barrier.</summary>
		/// <returns>Returns the total number of participants in the barrier.</returns>
		// Token: 0x170000D6 RID: 214
		// (get) Token: 0x06000460 RID: 1120 RVA: 0x00015A66 File Offset: 0x00013C66
		public int ParticipantCount
		{
			get
			{
				return this.m_currentTotalCount & 32767;
			}
		}

		/// <summary>Gets the number of the barrier's current phase.</summary>
		/// <returns>Returns the number of the barrier's current phase.</returns>
		// Token: 0x170000D7 RID: 215
		// (get) Token: 0x06000461 RID: 1121 RVA: 0x00015A76 File Offset: 0x00013C76
		// (set) Token: 0x06000462 RID: 1122 RVA: 0x00015A83 File Offset: 0x00013C83
		public long CurrentPhaseNumber
		{
			get
			{
				return Volatile.Read(ref this.m_currentPhase);
			}
			internal set
			{
				Volatile.Write(ref this.m_currentPhase, value);
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Threading.Barrier" /> class.</summary>
		/// <param name="participantCount">The number of participating threads.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="participantCount" /> is less than 0 or greater than 32,767.</exception>
		// Token: 0x06000463 RID: 1123 RVA: 0x00015A91 File Offset: 0x00013C91
		public Barrier(int participantCount) : this(participantCount, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Threading.Barrier" /> class.</summary>
		/// <param name="participantCount">The number of participating threads.</param>
		/// <param name="postPhaseAction">The <see cref="T:System.Action`1" /> to be executed after each phase. null (Nothing in Visual Basic) may be passed to indicate no action is taken.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="participantCount" /> is less than 0 or greater than 32,767.</exception>
		// Token: 0x06000464 RID: 1124 RVA: 0x00015A9C File Offset: 0x00013C9C
		public Barrier(int participantCount, Action<Barrier> postPhaseAction)
		{
			if (participantCount < 0 || participantCount > 32767)
			{
				throw new ArgumentOutOfRangeException("participantCount", participantCount, SR.GetString("The participantCount argument must be non-negative and less than or equal to 32767."));
			}
			this.m_currentTotalCount = participantCount;
			this.m_postPhaseAction = postPhaseAction;
			this.m_oddEvent = new ManualResetEventSlim(true);
			this.m_evenEvent = new ManualResetEventSlim(false);
			if (postPhaseAction != null && !ExecutionContext.IsFlowSuppressed())
			{
				this.m_ownerThreadContext = ExecutionContext.Capture();
			}
			this.m_actionCallerID = 0;
		}

		// Token: 0x06000465 RID: 1125 RVA: 0x00015B1A File Offset: 0x00013D1A
		private void GetCurrentTotal(int currentTotal, out int current, out int total, out bool sense)
		{
			total = (currentTotal & 32767);
			current = (currentTotal & 2147418112) >> 16;
			sense = ((currentTotal & int.MinValue) == 0);
		}

		// Token: 0x06000466 RID: 1126 RVA: 0x00015B44 File Offset: 0x00013D44
		private bool SetCurrentTotal(int currentTotal, int current, int total, bool sense)
		{
			int num = current << 16 | total;
			if (!sense)
			{
				num |= int.MinValue;
			}
			return Interlocked.CompareExchange(ref this.m_currentTotalCount, num, currentTotal) == currentTotal;
		}

		/// <summary>Notifies the <see cref="T:System.Threading.Barrier" /> that there will be an additional participant.</summary>
		/// <returns>The phase number of the barrier in which the new participants will first participate.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The current instance has already been disposed.</exception>
		/// <exception cref="T:System.InvalidOperationException">Adding a participant would cause the barrier's participant count to exceed 32,767.-or-The method was invoked from within a post-phase action.</exception>
		// Token: 0x06000467 RID: 1127 RVA: 0x00015B74 File Offset: 0x00013D74
		public long AddParticipant()
		{
			long result;
			try
			{
				result = this.AddParticipants(1);
			}
			catch (ArgumentOutOfRangeException)
			{
				throw new InvalidOperationException(SR.GetString("Adding participantCount participants would result in the number of participants exceeding the maximum number allowed."));
			}
			return result;
		}

		/// <summary>Notifies the <see cref="T:System.Threading.Barrier" /> that there will be additional participants.</summary>
		/// <param name="participantCount">The number of additional participants to add to the barrier.</param>
		/// <returns>The phase number of the barrier in which the new participants will first participate.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The current instance has already been disposed.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="participantCount" /> is less than 0.-or-Adding <paramref name="participantCount" /> participants would cause the barrier's participant count to exceed 32,767.</exception>
		/// <exception cref="T:System.InvalidOperationException">The method was invoked from within a post-phase action.</exception>
		// Token: 0x06000468 RID: 1128 RVA: 0x00015BB0 File Offset: 0x00013DB0
		public long AddParticipants(int participantCount)
		{
			this.ThrowIfDisposed();
			if (participantCount < 1)
			{
				throw new ArgumentOutOfRangeException("participantCount", participantCount, SR.GetString("The participantCount argument must be a positive value."));
			}
			if (participantCount > 32767)
			{
				throw new ArgumentOutOfRangeException("participantCount", SR.GetString("Adding participantCount participants would result in the number of participants exceeding the maximum number allowed."));
			}
			if (this.m_actionCallerID != 0 && Thread.CurrentThread.ManagedThreadId == this.m_actionCallerID)
			{
				throw new InvalidOperationException(SR.GetString("This method may not be called from within the postPhaseAction."));
			}
			SpinWait spinWait = default(SpinWait);
			bool flag;
			for (;;)
			{
				int currentTotalCount = this.m_currentTotalCount;
				int current;
				int num;
				this.GetCurrentTotal(currentTotalCount, out current, out num, out flag);
				if (participantCount + num > 32767)
				{
					break;
				}
				if (this.SetCurrentTotal(currentTotalCount, current, num + participantCount, flag))
				{
					goto Block_6;
				}
				spinWait.SpinOnce();
			}
			throw new ArgumentOutOfRangeException("participantCount", SR.GetString("Adding participantCount participants would result in the number of participants exceeding the maximum number allowed."));
			Block_6:
			long currentPhaseNumber = this.CurrentPhaseNumber;
			long num2 = (flag != (currentPhaseNumber % 2L == 0L)) ? (currentPhaseNumber + 1L) : currentPhaseNumber;
			if (num2 != currentPhaseNumber)
			{
				if (flag)
				{
					this.m_oddEvent.Wait();
				}
				else
				{
					this.m_evenEvent.Wait();
				}
			}
			else if (flag && this.m_evenEvent.IsSet)
			{
				this.m_evenEvent.Reset();
			}
			else if (!flag && this.m_oddEvent.IsSet)
			{
				this.m_oddEvent.Reset();
			}
			return num2;
		}

		/// <summary>Notifies the <see cref="T:System.Threading.Barrier" /> that there will be one less participant.</summary>
		/// <exception cref="T:System.ObjectDisposedException">The current instance has already been disposed.</exception>
		/// <exception cref="T:System.InvalidOperationException">The barrier already has 0 participants.-or-The method was invoked from within a post-phase action.</exception>
		// Token: 0x06000469 RID: 1129 RVA: 0x00015D04 File Offset: 0x00013F04
		public void RemoveParticipant()
		{
			this.RemoveParticipants(1);
		}

		/// <summary>Notifies the <see cref="T:System.Threading.Barrier" /> that there will be fewer participants.</summary>
		/// <param name="participantCount">The number of additional participants to remove from the barrier.</param>
		/// <exception cref="T:System.ObjectDisposedException">The current instance has already been disposed.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="participantCount" /> is less than 0.</exception>
		/// <exception cref="T:System.InvalidOperationException">The barrier already has 0 participants.-or-The method was invoked from within a post-phase action. -or-current participant count is less than the specified participantCount</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The total participant count is less than the specified<paramref name=" participantCount" /></exception>
		// Token: 0x0600046A RID: 1130 RVA: 0x00015D10 File Offset: 0x00013F10
		public void RemoveParticipants(int participantCount)
		{
			this.ThrowIfDisposed();
			if (participantCount < 1)
			{
				throw new ArgumentOutOfRangeException("participantCount", participantCount, SR.GetString("The participantCount argument must be a positive value."));
			}
			if (this.m_actionCallerID != 0 && Thread.CurrentThread.ManagedThreadId == this.m_actionCallerID)
			{
				throw new InvalidOperationException(SR.GetString("This method may not be called from within the postPhaseAction."));
			}
			SpinWait spinWait = default(SpinWait);
			bool flag;
			for (;;)
			{
				int currentTotalCount = this.m_currentTotalCount;
				int num;
				int num2;
				this.GetCurrentTotal(currentTotalCount, out num, out num2, out flag);
				if (num2 < participantCount)
				{
					break;
				}
				if (num2 - participantCount < num)
				{
					goto Block_5;
				}
				int num3 = num2 - participantCount;
				if (num3 > 0 && num == num3)
				{
					if (this.SetCurrentTotal(currentTotalCount, 0, num2 - participantCount, !flag))
					{
						goto Block_8;
					}
				}
				else if (this.SetCurrentTotal(currentTotalCount, num, num2 - participantCount, flag))
				{
					return;
				}
				spinWait.SpinOnce();
			}
			throw new ArgumentOutOfRangeException("participantCount", SR.GetString("The participantCount argument must be less than or equal the number of participants."));
			Block_5:
			throw new InvalidOperationException(SR.GetString("The participantCount argument is greater than the number of participants that haven't yet arrived at the barrier in this phase."));
			Block_8:
			this.FinishPhase(flag);
		}

		/// <summary>Signals that a participant has reached the barrier and waits for all other participants to reach the barrier as well.</summary>
		/// <exception cref="T:System.ObjectDisposedException">The current instance has already been disposed.</exception>
		/// <exception cref="T:System.InvalidOperationException">The method was invoked from within a post-phase action, the barrier currently has 0 participants, or the barrier is signaled by more threads than are registered as participants.</exception>
		/// <exception cref="T:System.Threading.BarrierPostPhaseException">If an exception is thrown from the post phase action of a Barrier after all participating threads have called SignalAndWait, the exception will be wrapped in a BarrierPostPhaseException and be thrown on all participating threads.</exception>
		// Token: 0x0600046B RID: 1131 RVA: 0x00015E00 File Offset: 0x00014000
		public void SignalAndWait()
		{
			this.SignalAndWait(default(CancellationToken));
		}

		/// <summary>Signals that a participant has reached the barrier and waits for all other participants to reach the barrier, while observing a cancellation token.</summary>
		/// <param name="cancellationToken">The <see cref="T:System.Threading.CancellationToken" /> to observe.</param>
		/// <exception cref="T:System.OperationCanceledException">
		///         <paramref name="cancellationToken" /> has been canceled.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The current instance has already been disposed.</exception>
		/// <exception cref="T:System.InvalidOperationException">The method was invoked from within a post-phase action, the barrier currently has 0 participants, or the barrier is signaled by more threads than are registered as participants.</exception>
		// Token: 0x0600046C RID: 1132 RVA: 0x00015E1C File Offset: 0x0001401C
		public void SignalAndWait(CancellationToken cancellationToken)
		{
			this.SignalAndWait(-1, cancellationToken);
		}

		/// <summary>Signals that a participant has reached the barrier and waits for all other participants to reach the barrier as well, using a <see cref="T:System.TimeSpan" /> object to measure the time interval.</summary>
		/// <param name="timeout">A <see cref="T:System.TimeSpan" /> that represents the number of milliseconds to wait, or a <see cref="T:System.TimeSpan" /> that represents -1 milliseconds to wait indefinitely.</param>
		/// <returns>true if all other participants reached the barrier; otherwise, false.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The current instance has already been disposed.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="timeout" />is a negative number other than -1 milliseconds, which represents an infinite time-out, or it is greater than 32,767.</exception>
		/// <exception cref="T:System.InvalidOperationException">The method was invoked from within a post-phase action, the barrier currently has 0 participants, or the barrier is signaled by more threads than are registered as participants.</exception>
		// Token: 0x0600046D RID: 1133 RVA: 0x00015E28 File Offset: 0x00014028
		public bool SignalAndWait(TimeSpan timeout)
		{
			return this.SignalAndWait(timeout, default(CancellationToken));
		}

		/// <summary>Signals that a participant has reached the barrier and waits for all other participants to reach the barrier as well, using a <see cref="T:System.TimeSpan" /> object to measure the time interval, while observing a cancellation token.</summary>
		/// <param name="timeout">A <see cref="T:System.TimeSpan" /> that represents the number of milliseconds to wait, or a <see cref="T:System.TimeSpan" /> that represents -1 milliseconds to wait indefinitely.</param>
		/// <param name="cancellationToken">The <see cref="T:System.Threading.CancellationToken" /> to observe.</param>
		/// <returns>true if all other participants reached the barrier; otherwise, false.</returns>
		/// <exception cref="T:System.OperationCanceledException">
		///         <paramref name="cancellationToken" /> has been canceled.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The current instance has already been disposed.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="timeout" />is a negative number other than -1 milliseconds, which represents an infinite time-out.</exception>
		/// <exception cref="T:System.InvalidOperationException">The method was invoked from within a post-phase action, the barrier currently has 0 participants, or the barrier is signaled by more threads than are registered as participants.</exception>
		// Token: 0x0600046E RID: 1134 RVA: 0x00015E48 File Offset: 0x00014048
		public bool SignalAndWait(TimeSpan timeout, CancellationToken cancellationToken)
		{
			long num = (long)timeout.TotalMilliseconds;
			if (num < -1L || num > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("timeout", timeout, SR.GetString("The specified timeout must represent a value between -1 and Int32.MaxValue, inclusive."));
			}
			return this.SignalAndWait((int)timeout.TotalMilliseconds, cancellationToken);
		}

		/// <summary>Signals that a participant has reached the barrier and waits for all other participants to reach the barrier as well, using a 32-bit signed integer to measure the timeout.</summary>
		/// <param name="millisecondsTimeout">The number of milliseconds to wait, or <see cref="F:System.Threading.Timeout.Infinite" />(-1) to wait indefinitely.</param>
		/// <returns>if all participants reached the barrier within the specified time; otherwise false.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The current instance has already been disposed.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="millisecondsTimeout" /> is a negative number other than -1, which represents an infinite time-out.</exception>
		/// <exception cref="T:System.InvalidOperationException">The method was invoked from within a post-phase action, the barrier currently has 0 participants, or the barrier is signaled by more threads than are registered as participants.</exception>
		/// <exception cref="T:System.Threading.BarrierPostPhaseException">If an exception is thrown from the post phase action of a Barrier after all participating threads have called SignalAndWait, the exception will be wrapped in a BarrierPostPhaseException and be thrown on all participating threads.</exception>
		// Token: 0x0600046F RID: 1135 RVA: 0x00015E98 File Offset: 0x00014098
		public bool SignalAndWait(int millisecondsTimeout)
		{
			return this.SignalAndWait(millisecondsTimeout, default(CancellationToken));
		}

		/// <summary>Signals that a participant has reached the barrier and waits for all other participants to reach the barrier as well, using a 32-bit signed integer to measure the timeout, while observing a cancellation token.</summary>
		/// <param name="millisecondsTimeout">The number of milliseconds to wait, or <see cref="F:System.Threading.Timeout.Infinite" />(-1) to wait indefinitely.</param>
		/// <param name="cancellationToken">The <see cref="T:System.Threading.CancellationToken" /> to observe.</param>
		/// <returns>if all participants reached the barrier within the specified time; otherwise false</returns>
		/// <exception cref="T:System.OperationCanceledException">
		///         <paramref name="cancellationToken" /> has been canceled.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The current instance has already been disposed.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="millisecondsTimeout" /> is a negative number other than -1, which represents an infinite time-out.</exception>
		/// <exception cref="T:System.InvalidOperationException">The method was invoked from within a post-phase action, the barrier currently has 0 participants, or the barrier is signaled by more threads than are registered as participants.</exception>
		// Token: 0x06000470 RID: 1136 RVA: 0x00015EB8 File Offset: 0x000140B8
		public bool SignalAndWait(int millisecondsTimeout, CancellationToken cancellationToken)
		{
			this.ThrowIfDisposed();
			cancellationToken.ThrowIfCancellationRequested();
			if (millisecondsTimeout < -1)
			{
				throw new ArgumentOutOfRangeException("millisecondsTimeout", millisecondsTimeout, SR.GetString("The specified timeout must represent a value between -1 and Int32.MaxValue, inclusive."));
			}
			if (this.m_actionCallerID != 0 && Thread.CurrentThread.ManagedThreadId == this.m_actionCallerID)
			{
				throw new InvalidOperationException(SR.GetString("This method may not be called from within the postPhaseAction."));
			}
			SpinWait spinWait = default(SpinWait);
			bool flag;
			long currentPhaseNumber;
			for (;;)
			{
				int currentTotalCount = this.m_currentTotalCount;
				int num;
				int num2;
				this.GetCurrentTotal(currentTotalCount, out num, out num2, out flag);
				currentPhaseNumber = this.CurrentPhaseNumber;
				if (num2 == 0)
				{
					break;
				}
				if (num == 0 && flag != (this.CurrentPhaseNumber % 2L == 0L))
				{
					goto Block_6;
				}
				if (num + 1 == num2)
				{
					if (this.SetCurrentTotal(currentTotalCount, 0, num2, !flag))
					{
						goto Block_8;
					}
				}
				else if (this.SetCurrentTotal(currentTotalCount, num + 1, num2, flag))
				{
					goto IL_EA;
				}
				spinWait.SpinOnce();
			}
			throw new InvalidOperationException(SR.GetString("The barrier has no registered participants."));
			Block_6:
			throw new InvalidOperationException(SR.GetString("The number of threads using the barrier exceeded the total number of registered participants."));
			Block_8:
			this.FinishPhase(flag);
			return true;
			IL_EA:
			ManualResetEventSlim currentPhaseEvent = flag ? this.m_evenEvent : this.m_oddEvent;
			bool flag2 = false;
			bool flag3 = false;
			try
			{
				flag3 = this.DiscontinuousWait(currentPhaseEvent, millisecondsTimeout, cancellationToken, currentPhaseNumber);
			}
			catch (OperationCanceledException)
			{
				flag2 = true;
			}
			catch (ObjectDisposedException)
			{
				if (currentPhaseNumber >= this.CurrentPhaseNumber)
				{
					throw;
				}
				flag3 = true;
			}
			if (!flag3)
			{
				spinWait.Reset();
				for (;;)
				{
					int currentTotalCount = this.m_currentTotalCount;
					int num;
					int num2;
					bool flag4;
					this.GetCurrentTotal(currentTotalCount, out num, out num2, out flag4);
					if (currentPhaseNumber < this.CurrentPhaseNumber || flag != flag4)
					{
						break;
					}
					if (this.SetCurrentTotal(currentTotalCount, num - 1, num2, flag))
					{
						goto Block_13;
					}
					spinWait.SpinOnce();
				}
				this.WaitCurrentPhase(currentPhaseEvent, currentPhaseNumber);
				goto IL_197;
				Block_13:
				if (flag2)
				{
					throw new OperationCanceledException(SR.GetString("The operation was canceled."), cancellationToken);
				}
				return false;
			}
			IL_197:
			if (this.m_exception != null)
			{
				throw new BarrierPostPhaseException(this.m_exception);
			}
			return true;
		}

		// Token: 0x06000471 RID: 1137 RVA: 0x00016090 File Offset: 0x00014290
		[SecuritySafeCritical]
		private void FinishPhase(bool observedSense)
		{
			if (this.m_postPhaseAction != null)
			{
				try
				{
					this.m_actionCallerID = Thread.CurrentThread.ManagedThreadId;
					if (this.m_ownerThreadContext != null)
					{
						ExecutionContext ownerThreadContext = this.m_ownerThreadContext;
						this.m_ownerThreadContext = this.m_ownerThreadContext.CreateCopy();
						ContextCallback contextCallback = Barrier.s_invokePostPhaseAction;
						if (contextCallback == null)
						{
							contextCallback = (Barrier.s_invokePostPhaseAction = new ContextCallback(Barrier.InvokePostPhaseAction));
						}
						ExecutionContext.Run(ownerThreadContext, contextCallback, this);
						ownerThreadContext.Dispose();
					}
					else
					{
						this.m_postPhaseAction(this);
					}
					this.m_exception = null;
					return;
				}
				catch (Exception exception)
				{
					this.m_exception = exception;
					return;
				}
				finally
				{
					this.m_actionCallerID = 0;
					this.SetResetEvents(observedSense);
					if (this.m_exception != null)
					{
						throw new BarrierPostPhaseException(this.m_exception);
					}
				}
			}
			this.SetResetEvents(observedSense);
		}

		// Token: 0x06000472 RID: 1138 RVA: 0x00016168 File Offset: 0x00014368
		[SecurityCritical]
		private static void InvokePostPhaseAction(object obj)
		{
			Barrier barrier = (Barrier)obj;
			barrier.m_postPhaseAction(barrier);
		}

		// Token: 0x06000473 RID: 1139 RVA: 0x00016188 File Offset: 0x00014388
		private void SetResetEvents(bool observedSense)
		{
			this.CurrentPhaseNumber += 1L;
			if (observedSense)
			{
				this.m_oddEvent.Reset();
				this.m_evenEvent.Set();
				return;
			}
			this.m_evenEvent.Reset();
			this.m_oddEvent.Set();
		}

		// Token: 0x06000474 RID: 1140 RVA: 0x000161D4 File Offset: 0x000143D4
		private void WaitCurrentPhase(ManualResetEventSlim currentPhaseEvent, long observedPhase)
		{
			SpinWait spinWait = default(SpinWait);
			while (!currentPhaseEvent.IsSet && this.CurrentPhaseNumber - observedPhase <= 1L)
			{
				spinWait.SpinOnce();
			}
		}

		// Token: 0x06000475 RID: 1141 RVA: 0x00016208 File Offset: 0x00014408
		private bool DiscontinuousWait(ManualResetEventSlim currentPhaseEvent, int totalTimeout, CancellationToken token, long observedPhase)
		{
			int num = 100;
			int num2 = 10000;
			while (observedPhase == this.CurrentPhaseNumber)
			{
				int num3 = (totalTimeout == -1) ? num : Math.Min(num, totalTimeout);
				if (currentPhaseEvent.Wait(num3, token))
				{
					return true;
				}
				if (totalTimeout != -1)
				{
					totalTimeout -= num3;
					if (totalTimeout <= 0)
					{
						return false;
					}
				}
				num = ((num >= num2) ? num2 : Math.Min(num << 1, num2));
			}
			this.WaitCurrentPhase(currentPhaseEvent, observedPhase);
			return true;
		}

		/// <summary>Releases all resources used by the current instance of the <see cref="T:System.Threading.Barrier" /> class.</summary>
		/// <exception cref="T:System.InvalidOperationException">The method was invoked from within a post-phase action.</exception>
		// Token: 0x06000476 RID: 1142 RVA: 0x0001626F File Offset: 0x0001446F
		public void Dispose()
		{
			if (this.m_actionCallerID != 0 && Thread.CurrentThread.ManagedThreadId == this.m_actionCallerID)
			{
				throw new InvalidOperationException(SR.GetString("This method may not be called from within the postPhaseAction."));
			}
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Threading.Barrier" />, and optionally releases the managed resources.</summary>
		/// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
		// Token: 0x06000477 RID: 1143 RVA: 0x000162A8 File Offset: 0x000144A8
		protected virtual void Dispose(bool disposing)
		{
			if (!this.m_disposed)
			{
				if (disposing)
				{
					this.m_oddEvent.Dispose();
					this.m_evenEvent.Dispose();
					if (this.m_ownerThreadContext != null)
					{
						this.m_ownerThreadContext.Dispose();
						this.m_ownerThreadContext = null;
					}
				}
				this.m_disposed = true;
			}
		}

		// Token: 0x06000478 RID: 1144 RVA: 0x000162F7 File Offset: 0x000144F7
		private void ThrowIfDisposed()
		{
			if (this.m_disposed)
			{
				throw new ObjectDisposedException("Barrier", SR.GetString("The barrier has been disposed."));
			}
		}

		// Token: 0x04000973 RID: 2419
		private volatile int m_currentTotalCount;

		// Token: 0x04000974 RID: 2420
		private const int CURRENT_MASK = 2147418112;

		// Token: 0x04000975 RID: 2421
		private const int TOTAL_MASK = 32767;

		// Token: 0x04000976 RID: 2422
		private const int SENSE_MASK = -2147483648;

		// Token: 0x04000977 RID: 2423
		private const int MAX_PARTICIPANTS = 32767;

		// Token: 0x04000978 RID: 2424
		private long m_currentPhase;

		// Token: 0x04000979 RID: 2425
		private bool m_disposed;

		// Token: 0x0400097A RID: 2426
		private ManualResetEventSlim m_oddEvent;

		// Token: 0x0400097B RID: 2427
		private ManualResetEventSlim m_evenEvent;

		// Token: 0x0400097C RID: 2428
		private ExecutionContext m_ownerThreadContext;

		// Token: 0x0400097D RID: 2429
		[SecurityCritical]
		private static ContextCallback s_invokePostPhaseAction;

		// Token: 0x0400097E RID: 2430
		private Action<Barrier> m_postPhaseAction;

		// Token: 0x0400097F RID: 2431
		private Exception m_exception;

		// Token: 0x04000980 RID: 2432
		private int m_actionCallerID;
	}
}
