﻿using System;

namespace System
{
	/// <summary>A parser based on the NetPipe scheme for the "Indigo" system.</summary>
	// Token: 0x020000A7 RID: 167
	public class NetPipeStyleUriParser : UriParser
	{
		/// <summary>Create a parser based on the NetPipe scheme for the "Indigo" system.</summary>
		// Token: 0x060003ED RID: 1005 RVA: 0x00013751 File Offset: 0x00011951
		public NetPipeStyleUriParser() : base(UriParser.NetPipeUri.Flags)
		{
		}
	}
}
