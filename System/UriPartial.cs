﻿using System;

namespace System
{
	/// <summary>Defines the parts of a URI for the <see cref="M:System.Uri.GetLeftPart(System.UriPartial)" /> method.</summary>
	// Token: 0x02000098 RID: 152
	public enum UriPartial
	{
		/// <summary>The scheme segment of the URI.</summary>
		// Token: 0x040008BF RID: 2239
		Scheme,
		/// <summary>The scheme and authority segments of the URI.</summary>
		// Token: 0x040008C0 RID: 2240
		Authority,
		/// <summary>The scheme, authority, and path segments of the URI.</summary>
		// Token: 0x040008C1 RID: 2241
		Path,
		/// <summary>The scheme, authority, path, and query segments of the URI.</summary>
		// Token: 0x040008C2 RID: 2242
		Query
	}
}
