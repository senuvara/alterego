﻿using System;
using System.Collections;
using System.Globalization;

namespace System
{
	// Token: 0x0200008D RID: 141
	[Serializable]
	internal class InvariantComparer : IComparer
	{
		// Token: 0x06000335 RID: 821 RVA: 0x00009EFD File Offset: 0x000080FD
		internal InvariantComparer()
		{
			this.m_compareInfo = CultureInfo.InvariantCulture.CompareInfo;
		}

		// Token: 0x06000336 RID: 822 RVA: 0x00009F18 File Offset: 0x00008118
		public int Compare(object a, object b)
		{
			string text = a as string;
			string text2 = b as string;
			if (text != null && text2 != null)
			{
				return this.m_compareInfo.Compare(text, text2);
			}
			return Comparer.Default.Compare(a, b);
		}

		// Token: 0x06000337 RID: 823 RVA: 0x00009F53 File Offset: 0x00008153
		// Note: this type is marked as 'beforefieldinit'.
		static InvariantComparer()
		{
		}

		// Token: 0x04000834 RID: 2100
		private CompareInfo m_compareInfo;

		// Token: 0x04000835 RID: 2101
		internal static readonly InvariantComparer Default = new InvariantComparer();
	}
}
