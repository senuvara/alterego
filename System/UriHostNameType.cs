﻿using System;

namespace System
{
	/// <summary>Defines host name types for the <see cref="M:System.Uri.CheckHostName(System.String)" /> method.</summary>
	// Token: 0x020000A0 RID: 160
	public enum UriHostNameType
	{
		/// <summary>The type of the host name is not supplied.</summary>
		// Token: 0x04000902 RID: 2306
		Unknown,
		/// <summary>The host is set, but the type cannot be determined.</summary>
		// Token: 0x04000903 RID: 2307
		Basic,
		/// <summary>The host name is a domain name system (DNS) style host name.</summary>
		// Token: 0x04000904 RID: 2308
		Dns,
		/// <summary>The host name is an Internet Protocol (IP) version 4 host address.</summary>
		// Token: 0x04000905 RID: 2309
		IPv4,
		/// <summary>The host name is an Internet Protocol (IP) version 6 host address.</summary>
		// Token: 0x04000906 RID: 2310
		IPv6
	}
}
