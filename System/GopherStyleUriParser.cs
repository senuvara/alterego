﻿using System;

namespace System
{
	/// <summary>A customizable parser based on the Gopher scheme.</summary>
	// Token: 0x020000A5 RID: 165
	public class GopherStyleUriParser : UriParser
	{
		/// <summary>Creates a customizable parser based on the Gopher scheme.</summary>
		// Token: 0x060003EB RID: 1003 RVA: 0x0001372D File Offset: 0x0001192D
		public GopherStyleUriParser() : base(UriParser.GopherUri.Flags)
		{
		}
	}
}
