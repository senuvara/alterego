﻿using System;

namespace System
{
	/// <summary>Controls how URI information is escaped.</summary>
	// Token: 0x0200009B RID: 155
	public enum UriFormat
	{
		/// <summary>Escaping is performed according to the rules in RFC 2396.</summary>
		// Token: 0x040008DA RID: 2266
		UriEscaped = 1,
		/// <summary>No escaping is performed.</summary>
		// Token: 0x040008DB RID: 2267
		Unescaped,
		/// <summary>Characters that have a reserved meaning in the requested URI components remain escaped. All others are not escaped. See Remarks.</summary>
		// Token: 0x040008DC RID: 2268
		SafeUnescaped
	}
}
