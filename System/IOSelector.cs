﻿using System;
using System.Runtime.CompilerServices;

namespace System
{
	// Token: 0x020000B7 RID: 183
	internal static class IOSelector
	{
		// Token: 0x06000444 RID: 1092
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Add(IntPtr handle, IOSelectorJob job);

		// Token: 0x06000445 RID: 1093
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Remove(IntPtr handle);
	}
}
