﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Unity;

namespace System.Media
{
	/// <summary>Controls playback of a sound from a .wav file.</summary>
	// Token: 0x02000654 RID: 1620
	[ToolboxItem(false)]
	[HostProtection(SecurityAction.LinkDemand, UI = true)]
	[Serializable]
	public class SoundPlayer : Component, ISerializable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Media.SoundPlayer" /> class.</summary>
		// Token: 0x060033DB RID: 13275 RVA: 0x000092E2 File Offset: 0x000074E2
		public SoundPlayer()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Media.SoundPlayer" /> class, and attaches the .wav file within the specified <see cref="T:System.IO.Stream" />.</summary>
		/// <param name="stream">A <see cref="T:System.IO.Stream" /> to a .wav file.</param>
		// Token: 0x060033DC RID: 13276 RVA: 0x000092E2 File Offset: 0x000074E2
		public SoundPlayer(Stream stream)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Media.SoundPlayer" /> class.</summary>
		/// <param name="serializationInfo">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to be used for deserialization.</param>
		/// <param name="context">The destination to be used for deserialization.</param>
		/// <exception cref="T:System.UriFormatException">The <see cref="P:System.Media.SoundPlayer.SoundLocation" /> specified in <paramref name="serializationInfo" /> cannot be resolved.</exception>
		// Token: 0x060033DD RID: 13277 RVA: 0x000092E2 File Offset: 0x000074E2
		protected SoundPlayer(SerializationInfo serializationInfo, StreamingContext context)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Media.SoundPlayer" /> class, and attaches the specified .wav file.</summary>
		/// <param name="soundLocation">The location of a .wav file to load.</param>
		/// <exception cref="T:System.UriFormatException">The URL value specified by <paramref name="soundLocation" /> cannot be resolved.</exception>
		// Token: 0x060033DE RID: 13278 RVA: 0x000092E2 File Offset: 0x000074E2
		public SoundPlayer(string soundLocation)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a value indicating whether loading of a .wav file has successfully completed.</summary>
		/// <returns>
		///     <see langword="true" /> if a .wav file is loaded; <see langword="false" /> if a .wav file has not yet been loaded.</returns>
		// Token: 0x17000D1D RID: 3357
		// (get) Token: 0x060033DF RID: 13279 RVA: 0x000A50F8 File Offset: 0x000A32F8
		public bool IsLoadCompleted
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
		}

		/// <summary>Gets or sets the time, in milliseconds, in which the .wav file must load.</summary>
		/// <returns>The number of milliseconds to wait. The default is 10000 (10 seconds).</returns>
		// Token: 0x17000D1E RID: 3358
		// (get) Token: 0x060033E0 RID: 13280 RVA: 0x000A5114 File Offset: 0x000A3314
		// (set) Token: 0x060033E1 RID: 13281 RVA: 0x000092E2 File Offset: 0x000074E2
		public int LoadTimeout
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the file path or URL of the .wav file to load.</summary>
		/// <returns>The file path or URL from which to load a .wav file, or <see cref="F:System.String.Empty" /> if no file path is present. The default is <see cref="F:System.String.Empty" />.</returns>
		// Token: 0x17000D1F RID: 3359
		// (get) Token: 0x060033E2 RID: 13282 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060033E3 RID: 13283 RVA: 0x000092E2 File Offset: 0x000074E2
		public string SoundLocation
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.IO.Stream" /> from which to load the .wav file.</summary>
		/// <returns>A <see cref="T:System.IO.Stream" /> from which to load the .wav file, or <see langword="null" /> if no stream is available. The default is <see langword="null" />.</returns>
		// Token: 0x17000D20 RID: 3360
		// (get) Token: 0x060033E4 RID: 13284 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060033E5 RID: 13285 RVA: 0x000092E2 File Offset: 0x000074E2
		public Stream Stream
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Object" /> that contains data about the <see cref="T:System.Media.SoundPlayer" />.</summary>
		/// <returns>An <see cref="T:System.Object" /> that contains data about the <see cref="T:System.Media.SoundPlayer" />.</returns>
		// Token: 0x17000D21 RID: 3361
		// (get) Token: 0x060033E6 RID: 13286 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060033E7 RID: 13287 RVA: 0x000092E2 File Offset: 0x000074E2
		public object Tag
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs when a .wav file has been successfully or unsuccessfully loaded.</summary>
		// Token: 0x1400005B RID: 91
		// (add) Token: 0x060033E8 RID: 13288 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x060033E9 RID: 13289 RVA: 0x000092E2 File Offset: 0x000074E2
		public event AsyncCompletedEventHandler LoadCompleted
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs when a new audio source path for this <see cref="T:System.Media.SoundPlayer" /> has been set.</summary>
		// Token: 0x1400005C RID: 92
		// (add) Token: 0x060033EA RID: 13290 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x060033EB RID: 13291 RVA: 0x000092E2 File Offset: 0x000074E2
		public event EventHandler SoundLocationChanged
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs when a new <see cref="T:System.IO.Stream" /> audio source for this <see cref="T:System.Media.SoundPlayer" /> has been set.</summary>
		// Token: 0x1400005D RID: 93
		// (add) Token: 0x060033EC RID: 13292 RVA: 0x000092E2 File Offset: 0x000074E2
		// (remove) Token: 0x060033ED RID: 13293 RVA: 0x000092E2 File Offset: 0x000074E2
		public event EventHandler StreamChanged
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Loads a sound synchronously.</summary>
		/// <exception cref="T:System.ServiceProcess.TimeoutException">The elapsed time during loading exceeds the time, in milliseconds, specified by <see cref="P:System.Media.SoundPlayer.LoadTimeout" />. </exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The file specified by <see cref="P:System.Media.SoundPlayer.SoundLocation" /> cannot be found.</exception>
		// Token: 0x060033EE RID: 13294 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Load()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Loads a .wav file from a stream or a Web resource using a new thread.</summary>
		/// <exception cref="T:System.ServiceProcess.TimeoutException">The elapsed time during loading exceeds the time, in milliseconds, specified by <see cref="P:System.Media.SoundPlayer.LoadTimeout" />. </exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The file specified by <see cref="P:System.Media.SoundPlayer.SoundLocation" /> cannot be found.</exception>
		// Token: 0x060033EF RID: 13295 RVA: 0x000092E2 File Offset: 0x000074E2
		public void LoadAsync()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Raises the <see cref="E:System.Media.SoundPlayer.LoadCompleted" /> event.</summary>
		/// <param name="e">An <see cref="T:System.ComponentModel.AsyncCompletedEventArgs" />  that contains the event data. </param>
		// Token: 0x060033F0 RID: 13296 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OnLoadCompleted(AsyncCompletedEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Raises the <see cref="E:System.Media.SoundPlayer.SoundLocationChanged" /> event.</summary>
		/// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
		// Token: 0x060033F1 RID: 13297 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OnSoundLocationChanged(EventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Raises the <see cref="E:System.Media.SoundPlayer.StreamChanged" /> event.</summary>
		/// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
		// Token: 0x060033F2 RID: 13298 RVA: 0x000092E2 File Offset: 0x000074E2
		protected virtual void OnStreamChanged(EventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Plays the .wav file using a new thread, and loads the .wav file first if it has not been loaded.</summary>
		/// <exception cref="T:System.ServiceProcess.TimeoutException">The elapsed time during loading exceeds the time, in milliseconds, specified by <see cref="P:System.Media.SoundPlayer.LoadTimeout" />. </exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The file specified by <see cref="P:System.Media.SoundPlayer.SoundLocation" /> cannot be found.</exception>
		/// <exception cref="T:System.InvalidOperationException">The .wav header is corrupted; the file specified by <see cref="P:System.Media.SoundPlayer.SoundLocation" /> is not a PCM .wav file.</exception>
		// Token: 0x060033F3 RID: 13299 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Play()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Plays and loops the .wav file using a new thread, and loads the .wav file first if it has not been loaded.</summary>
		/// <exception cref="T:System.ServiceProcess.TimeoutException">The elapsed time during loading exceeds the time, in milliseconds, specified by <see cref="P:System.Media.SoundPlayer.LoadTimeout" />. </exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The file specified by <see cref="P:System.Media.SoundPlayer.SoundLocation" /> cannot be found.</exception>
		/// <exception cref="T:System.InvalidOperationException">The .wav header is corrupted; the file specified by <see cref="P:System.Media.SoundPlayer.SoundLocation" /> is not a PCM .wav file.</exception>
		// Token: 0x060033F4 RID: 13300 RVA: 0x000092E2 File Offset: 0x000074E2
		public void PlayLooping()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Plays the .wav file and loads the .wav file first if it has not been loaded.</summary>
		/// <exception cref="T:System.ServiceProcess.TimeoutException">The elapsed time during loading exceeds the time, in milliseconds, specified by <see cref="P:System.Media.SoundPlayer.LoadTimeout" />. </exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The file specified by <see cref="P:System.Media.SoundPlayer.SoundLocation" /> cannot be found.</exception>
		/// <exception cref="T:System.InvalidOperationException">The .wav header is corrupted; the file specified by <see cref="P:System.Media.SoundPlayer.SoundLocation" /> is not a PCM .wav file.</exception>
		// Token: 0x060033F5 RID: 13301 RVA: 0x000092E2 File Offset: 0x000074E2
		public void PlaySync()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Stops playback of the sound if playback is occurring.</summary>
		// Token: 0x060033F6 RID: 13302 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Stop()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>For a description of this member, see the <see cref="M:System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)" /> method.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" />  to populate with data.</param>
		/// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this serialization.</param>
		// Token: 0x060033F7 RID: 13303 RVA: 0x000092E2 File Offset: 0x000074E2
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
