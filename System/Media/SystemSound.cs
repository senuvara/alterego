﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Media
{
	/// <summary>Represents a system sound type.</summary>
	// Token: 0x02000655 RID: 1621
	[HostProtection(SecurityAction.LinkDemand, UI = true)]
	public class SystemSound
	{
		// Token: 0x060033F8 RID: 13304 RVA: 0x000092E2 File Offset: 0x000074E2
		internal SystemSound()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Plays the system sound type.</summary>
		// Token: 0x060033F9 RID: 13305 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Play()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
