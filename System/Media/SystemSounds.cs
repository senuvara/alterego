﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Media
{
	/// <summary>Retrieves sounds associated with a set of Windows operating system sound-event types. This class cannot be inherited.</summary>
	// Token: 0x02000656 RID: 1622
	[HostProtection(SecurityAction.LinkDemand, UI = true)]
	public sealed class SystemSounds
	{
		// Token: 0x060033FA RID: 13306 RVA: 0x000092E2 File Offset: 0x000074E2
		internal SystemSounds()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the sound associated with the <see langword="Asterisk" /> program event in the current Windows sound scheme.</summary>
		/// <returns>A <see cref="T:System.Media.SystemSound" /> associated with the <see langword="Asterisk" /> program event in the current Windows sound scheme.</returns>
		// Token: 0x17000D22 RID: 3362
		// (get) Token: 0x060033FB RID: 13307 RVA: 0x00043C3C File Offset: 0x00041E3C
		public static SystemSound Asterisk
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the sound associated with the <see langword="Beep" /> program event in the current Windows sound scheme.</summary>
		/// <returns>A <see cref="T:System.Media.SystemSound" /> associated with the <see langword="Beep" /> program event in the current Windows sound scheme.</returns>
		// Token: 0x17000D23 RID: 3363
		// (get) Token: 0x060033FC RID: 13308 RVA: 0x00043C3C File Offset: 0x00041E3C
		public static SystemSound Beep
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the sound associated with the <see langword="Exclamation" /> program event in the current Windows sound scheme.</summary>
		/// <returns>A <see cref="T:System.Media.SystemSound" /> associated with the <see langword="Exclamation" /> program event in the current Windows sound scheme.</returns>
		// Token: 0x17000D24 RID: 3364
		// (get) Token: 0x060033FD RID: 13309 RVA: 0x00043C3C File Offset: 0x00041E3C
		public static SystemSound Exclamation
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the sound associated with the <see langword="Hand" /> program event in the current Windows sound scheme.</summary>
		/// <returns>A <see cref="T:System.Media.SystemSound" /> associated with the <see langword="Hand" /> program event in the current Windows sound scheme.</returns>
		// Token: 0x17000D25 RID: 3365
		// (get) Token: 0x060033FE RID: 13310 RVA: 0x00043C3C File Offset: 0x00041E3C
		public static SystemSound Hand
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the sound associated with the <see langword="Question" /> program event in the current Windows sound scheme.</summary>
		/// <returns>A <see cref="T:System.Media.SystemSound" /> associated with the <see langword="Question" /> program event in the current Windows sound scheme.</returns>
		// Token: 0x17000D26 RID: 3366
		// (get) Token: 0x060033FF RID: 13311 RVA: 0x00043C3C File Offset: 0x00041E3C
		public static SystemSound Question
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
