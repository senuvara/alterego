﻿using System;

namespace System
{
	// Token: 0x0200009E RID: 158
	[Flags]
	internal enum UnescapeMode
	{
		// Token: 0x040008F1 RID: 2289
		CopyOnly = 0,
		// Token: 0x040008F2 RID: 2290
		Escape = 1,
		// Token: 0x040008F3 RID: 2291
		Unescape = 2,
		// Token: 0x040008F4 RID: 2292
		EscapeUnescape = 3,
		// Token: 0x040008F5 RID: 2293
		V1ToStringFlag = 4,
		// Token: 0x040008F6 RID: 2294
		UnescapeAll = 8,
		// Token: 0x040008F7 RID: 2295
		UnescapeAllOrThrow = 24
	}
}
