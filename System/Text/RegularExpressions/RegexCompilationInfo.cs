﻿using System;
using System.Runtime.Serialization;

namespace System.Text.RegularExpressions
{
	/// <summary>Provides information about a regular expression that is used to compile a regular expression to a stand-alone assembly. </summary>
	// Token: 0x020000D5 RID: 213
	[Serializable]
	public class RegexCompilationInfo
	{
		// Token: 0x0600054F RID: 1359 RVA: 0x0001ADB9 File Offset: 0x00018FB9
		[OnDeserializing]
		private void InitMatchTimeoutDefaultForOldVersionDeserialization(StreamingContext unusedContext)
		{
			this.matchTimeout = Regex.DefaultMatchTimeout;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Text.RegularExpressions.RegexCompilationInfo" /> class that contains information about a regular expression to be included in an assembly. </summary>
		/// <param name="pattern">The regular expression to compile. </param>
		/// <param name="options">The regular expression options to use when compiling the regular expression. </param>
		/// <param name="name">The name of the type that represents the compiled regular expression. </param>
		/// <param name="fullnamespace">The namespace to which the new type belongs. </param>
		/// <param name="ispublic">
		///       <see langword="true" /> to make the compiled regular expression publicly visible; otherwise, <see langword="false" />. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="name" /> is <see cref="F:System.String.Empty" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pattern" /> is <see langword="null" />.-or-
		///         <paramref name="name" /> is <see langword="null" />.-or-
		///         <paramref name="fullnamespace" /> is <see langword="null" />.</exception>
		// Token: 0x06000550 RID: 1360 RVA: 0x0001ADC6 File Offset: 0x00018FC6
		public RegexCompilationInfo(string pattern, RegexOptions options, string name, string fullnamespace, bool ispublic) : this(pattern, options, name, fullnamespace, ispublic, Regex.DefaultMatchTimeout)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Text.RegularExpressions.RegexCompilationInfo" /> class that contains information about a regular expression with a specified time-out value to be included in an assembly.</summary>
		/// <param name="pattern">The regular expression to compile.</param>
		/// <param name="options">The regular expression options to use when compiling the regular expression.</param>
		/// <param name="name">The name of the type that represents the compiled regular expression.</param>
		/// <param name="fullnamespace">The namespace to which the new type belongs.</param>
		/// <param name="ispublic">
		///       <see langword="true" /> to make the compiled regular expression publicly visible; otherwise, <see langword="false" />.</param>
		/// <param name="matchTimeout">The default time-out interval for the regular expression.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="name" /> is <see cref="F:System.String.Empty" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pattern" /> is <see langword="null" />.-or-
		///         <paramref name="name" /> is <see langword="null" />.-or-
		///         <paramref name="fullnamespace" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="matchTimeout" /> is negative, zero, or greater than approximately 24 days.</exception>
		// Token: 0x06000551 RID: 1361 RVA: 0x0001ADDA File Offset: 0x00018FDA
		public RegexCompilationInfo(string pattern, RegexOptions options, string name, string fullnamespace, bool ispublic, TimeSpan matchTimeout)
		{
			this.Pattern = pattern;
			this.Name = name;
			this.Namespace = fullnamespace;
			this.options = options;
			this.isPublic = ispublic;
			this.MatchTimeout = matchTimeout;
		}

		/// <summary>Gets or sets the regular expression to compile.</summary>
		/// <returns>The regular expression to compile.</returns>
		/// <exception cref="T:System.ArgumentNullException">The value for this property is <see langword="null" />.</exception>
		// Token: 0x170000F2 RID: 242
		// (get) Token: 0x06000552 RID: 1362 RVA: 0x0001AE0F File Offset: 0x0001900F
		// (set) Token: 0x06000553 RID: 1363 RVA: 0x0001AE17 File Offset: 0x00019017
		public string Pattern
		{
			get
			{
				return this.pattern;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.pattern = value;
			}
		}

		/// <summary>Gets or sets the options to use when compiling the regular expression.</summary>
		/// <returns>A bitwise combination of the enumeration values.</returns>
		// Token: 0x170000F3 RID: 243
		// (get) Token: 0x06000554 RID: 1364 RVA: 0x0001AE2E File Offset: 0x0001902E
		// (set) Token: 0x06000555 RID: 1365 RVA: 0x0001AE36 File Offset: 0x00019036
		public RegexOptions Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		/// <summary>Gets or sets the name of the type that represents the compiled regular expression.</summary>
		/// <returns>The name of the new type.</returns>
		/// <exception cref="T:System.ArgumentNullException">The value for this property is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">The value for this property is an empty string.</exception>
		// Token: 0x170000F4 RID: 244
		// (get) Token: 0x06000556 RID: 1366 RVA: 0x0001AE3F File Offset: 0x0001903F
		// (set) Token: 0x06000557 RID: 1367 RVA: 0x0001AE48 File Offset: 0x00019048
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				if (value.Length == 0)
				{
					throw new ArgumentException(SR.GetString("Argument {0} cannot be null or zero-length.", new object[]
					{
						"value"
					}), "value");
				}
				this.name = value;
			}
		}

		/// <summary>Gets or sets the namespace to which the new type belongs.</summary>
		/// <returns>The namespace of the new type.</returns>
		/// <exception cref="T:System.ArgumentNullException">The value for this property is <see langword="null" />.</exception>
		// Token: 0x170000F5 RID: 245
		// (get) Token: 0x06000558 RID: 1368 RVA: 0x0001AE95 File Offset: 0x00019095
		// (set) Token: 0x06000559 RID: 1369 RVA: 0x0001AE9D File Offset: 0x0001909D
		public string Namespace
		{
			get
			{
				return this.nspace;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.nspace = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether the compiled regular expression has public visibility.</summary>
		/// <returns>
		///     <see langword="true" /> if the regular expression has public visibility; otherwise, <see langword="false" />.</returns>
		// Token: 0x170000F6 RID: 246
		// (get) Token: 0x0600055A RID: 1370 RVA: 0x0001AEB4 File Offset: 0x000190B4
		// (set) Token: 0x0600055B RID: 1371 RVA: 0x0001AEBC File Offset: 0x000190BC
		public bool IsPublic
		{
			get
			{
				return this.isPublic;
			}
			set
			{
				this.isPublic = value;
			}
		}

		/// <summary>Gets or sets the regular expression's default time-out interval.</summary>
		/// <returns>The default maximum time interval that can elapse in a pattern-matching operation before a <see cref="T:System.Text.RegularExpressions.RegexMatchTimeoutException" /> is thrown, or <see cref="F:System.Text.RegularExpressions.Regex.InfiniteMatchTimeout" /> if time-outs are disabled.</returns>
		// Token: 0x170000F7 RID: 247
		// (get) Token: 0x0600055C RID: 1372 RVA: 0x0001AEC5 File Offset: 0x000190C5
		// (set) Token: 0x0600055D RID: 1373 RVA: 0x0001AECD File Offset: 0x000190CD
		public TimeSpan MatchTimeout
		{
			get
			{
				return this.matchTimeout;
			}
			set
			{
				Regex.ValidateMatchTimeout(value);
				this.matchTimeout = value;
			}
		}

		// Token: 0x04000A38 RID: 2616
		private string pattern;

		// Token: 0x04000A39 RID: 2617
		private RegexOptions options;

		// Token: 0x04000A3A RID: 2618
		private string name;

		// Token: 0x04000A3B RID: 2619
		private string nspace;

		// Token: 0x04000A3C RID: 2620
		private bool isPublic;

		// Token: 0x04000A3D RID: 2621
		[OptionalField(VersionAdded = 2)]
		private TimeSpan matchTimeout;
	}
}
