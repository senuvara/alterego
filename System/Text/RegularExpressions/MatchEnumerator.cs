﻿using System;
using System.Collections;

namespace System.Text.RegularExpressions
{
	// Token: 0x020000E0 RID: 224
	[Serializable]
	internal class MatchEnumerator : IEnumerator
	{
		// Token: 0x060005D8 RID: 1496 RVA: 0x0001DB2C File Offset: 0x0001BD2C
		internal MatchEnumerator(MatchCollection matchcoll)
		{
			this._matchcoll = matchcoll;
		}

		// Token: 0x060005D9 RID: 1497 RVA: 0x0001DB3C File Offset: 0x0001BD3C
		public bool MoveNext()
		{
			if (this._done)
			{
				return false;
			}
			this._match = this._matchcoll.GetMatch(this._curindex);
			this._curindex++;
			if (this._match == null)
			{
				this._done = true;
				return false;
			}
			return true;
		}

		// Token: 0x1700010E RID: 270
		// (get) Token: 0x060005DA RID: 1498 RVA: 0x0001DB8A File Offset: 0x0001BD8A
		public object Current
		{
			get
			{
				if (this._match == null)
				{
					throw new InvalidOperationException(SR.GetString("Enumeration has either not started or has already finished."));
				}
				return this._match;
			}
		}

		// Token: 0x060005DB RID: 1499 RVA: 0x0001DBAA File Offset: 0x0001BDAA
		public void Reset()
		{
			this._curindex = 0;
			this._done = false;
			this._match = null;
		}

		// Token: 0x04000A7E RID: 2686
		internal MatchCollection _matchcoll;

		// Token: 0x04000A7F RID: 2687
		internal Match _match;

		// Token: 0x04000A80 RID: 2688
		internal int _curindex;

		// Token: 0x04000A81 RID: 2689
		internal bool _done;
	}
}
