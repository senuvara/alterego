﻿using System;
using System.Threading;

namespace System.Text.RegularExpressions
{
	// Token: 0x020000CB RID: 203
	internal sealed class SharedReference
	{
		// Token: 0x060004FF RID: 1279 RVA: 0x00017B29 File Offset: 0x00015D29
		internal object Get()
		{
			if (Interlocked.Exchange(ref this._locked, 1) == 0)
			{
				object target = this._ref.Target;
				this._locked = 0;
				return target;
			}
			return null;
		}

		// Token: 0x06000500 RID: 1280 RVA: 0x00017B4D File Offset: 0x00015D4D
		internal void Cache(object obj)
		{
			if (Interlocked.Exchange(ref this._locked, 1) == 0)
			{
				this._ref.Target = obj;
				this._locked = 0;
			}
		}

		// Token: 0x06000501 RID: 1281 RVA: 0x00017B70 File Offset: 0x00015D70
		public SharedReference()
		{
		}

		// Token: 0x040009B5 RID: 2485
		private WeakReference _ref = new WeakReference(null);

		// Token: 0x040009B6 RID: 2486
		private int _locked;
	}
}
