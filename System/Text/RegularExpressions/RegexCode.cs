﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;

namespace System.Text.RegularExpressions
{
	// Token: 0x020000D4 RID: 212
	internal sealed class RegexCode
	{
		// Token: 0x0600054B RID: 1355 RVA: 0x0001AB98 File Offset: 0x00018D98
		internal RegexCode(int[] codes, List<string> stringlist, int trackcount, Hashtable caps, int capsize, RegexBoyerMoore bmPrefix, RegexPrefix fcPrefix, int anchors, bool rightToLeft)
		{
			this._codes = codes;
			this._strings = new string[stringlist.Count];
			this._trackcount = trackcount;
			this._caps = caps;
			this._capsize = capsize;
			this._bmPrefix = bmPrefix;
			this._fcPrefix = fcPrefix;
			this._anchors = anchors;
			this._rightToLeft = rightToLeft;
			stringlist.CopyTo(0, this._strings, 0, stringlist.Count);
		}

		// Token: 0x0600054C RID: 1356 RVA: 0x0001AC10 File Offset: 0x00018E10
		internal static bool OpcodeBacktracks(int Op)
		{
			Op &= 63;
			switch (Op)
			{
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 23:
			case 24:
			case 25:
			case 26:
			case 27:
			case 28:
			case 29:
			case 31:
			case 32:
			case 33:
			case 34:
			case 35:
			case 36:
			case 38:
				return true;
			}
			return false;
		}

		// Token: 0x0600054D RID: 1357 RVA: 0x0001ACC0 File Offset: 0x00018EC0
		internal static int OpcodeSize(int Opcode)
		{
			Opcode &= 63;
			switch (Opcode)
			{
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 28:
			case 29:
			case 32:
				return 3;
			case 9:
			case 10:
			case 11:
			case 12:
			case 13:
			case 23:
			case 24:
			case 25:
			case 26:
			case 27:
			case 37:
			case 38:
			case 39:
				return 2;
			case 14:
			case 15:
			case 16:
			case 17:
			case 18:
			case 19:
			case 20:
			case 21:
			case 22:
			case 30:
			case 31:
			case 33:
			case 34:
			case 35:
			case 36:
			case 40:
			case 41:
			case 42:
				return 1;
			default:
				throw RegexCode.MakeException(SR.GetString("Unexpected opcode in regular expression generation: {0}.", new object[]
				{
					Opcode.ToString(CultureInfo.CurrentCulture)
				}));
			}
		}

		// Token: 0x0600054E RID: 1358 RVA: 0x0001ADB1 File Offset: 0x00018FB1
		internal static ArgumentException MakeException(string message)
		{
			return new ArgumentException(message);
		}

		// Token: 0x040009FF RID: 2559
		internal const int Onerep = 0;

		// Token: 0x04000A00 RID: 2560
		internal const int Notonerep = 1;

		// Token: 0x04000A01 RID: 2561
		internal const int Setrep = 2;

		// Token: 0x04000A02 RID: 2562
		internal const int Oneloop = 3;

		// Token: 0x04000A03 RID: 2563
		internal const int Notoneloop = 4;

		// Token: 0x04000A04 RID: 2564
		internal const int Setloop = 5;

		// Token: 0x04000A05 RID: 2565
		internal const int Onelazy = 6;

		// Token: 0x04000A06 RID: 2566
		internal const int Notonelazy = 7;

		// Token: 0x04000A07 RID: 2567
		internal const int Setlazy = 8;

		// Token: 0x04000A08 RID: 2568
		internal const int One = 9;

		// Token: 0x04000A09 RID: 2569
		internal const int Notone = 10;

		// Token: 0x04000A0A RID: 2570
		internal const int Set = 11;

		// Token: 0x04000A0B RID: 2571
		internal const int Multi = 12;

		// Token: 0x04000A0C RID: 2572
		internal const int Ref = 13;

		// Token: 0x04000A0D RID: 2573
		internal const int Bol = 14;

		// Token: 0x04000A0E RID: 2574
		internal const int Eol = 15;

		// Token: 0x04000A0F RID: 2575
		internal const int Boundary = 16;

		// Token: 0x04000A10 RID: 2576
		internal const int Nonboundary = 17;

		// Token: 0x04000A11 RID: 2577
		internal const int Beginning = 18;

		// Token: 0x04000A12 RID: 2578
		internal const int Start = 19;

		// Token: 0x04000A13 RID: 2579
		internal const int EndZ = 20;

		// Token: 0x04000A14 RID: 2580
		internal const int End = 21;

		// Token: 0x04000A15 RID: 2581
		internal const int Nothing = 22;

		// Token: 0x04000A16 RID: 2582
		internal const int Lazybranch = 23;

		// Token: 0x04000A17 RID: 2583
		internal const int Branchmark = 24;

		// Token: 0x04000A18 RID: 2584
		internal const int Lazybranchmark = 25;

		// Token: 0x04000A19 RID: 2585
		internal const int Nullcount = 26;

		// Token: 0x04000A1A RID: 2586
		internal const int Setcount = 27;

		// Token: 0x04000A1B RID: 2587
		internal const int Branchcount = 28;

		// Token: 0x04000A1C RID: 2588
		internal const int Lazybranchcount = 29;

		// Token: 0x04000A1D RID: 2589
		internal const int Nullmark = 30;

		// Token: 0x04000A1E RID: 2590
		internal const int Setmark = 31;

		// Token: 0x04000A1F RID: 2591
		internal const int Capturemark = 32;

		// Token: 0x04000A20 RID: 2592
		internal const int Getmark = 33;

		// Token: 0x04000A21 RID: 2593
		internal const int Setjump = 34;

		// Token: 0x04000A22 RID: 2594
		internal const int Backjump = 35;

		// Token: 0x04000A23 RID: 2595
		internal const int Forejump = 36;

		// Token: 0x04000A24 RID: 2596
		internal const int Testref = 37;

		// Token: 0x04000A25 RID: 2597
		internal const int Goto = 38;

		// Token: 0x04000A26 RID: 2598
		internal const int Prune = 39;

		// Token: 0x04000A27 RID: 2599
		internal const int Stop = 40;

		// Token: 0x04000A28 RID: 2600
		internal const int ECMABoundary = 41;

		// Token: 0x04000A29 RID: 2601
		internal const int NonECMABoundary = 42;

		// Token: 0x04000A2A RID: 2602
		internal const int Mask = 63;

		// Token: 0x04000A2B RID: 2603
		internal const int Rtl = 64;

		// Token: 0x04000A2C RID: 2604
		internal const int Back = 128;

		// Token: 0x04000A2D RID: 2605
		internal const int Back2 = 256;

		// Token: 0x04000A2E RID: 2606
		internal const int Ci = 512;

		// Token: 0x04000A2F RID: 2607
		internal int[] _codes;

		// Token: 0x04000A30 RID: 2608
		internal string[] _strings;

		// Token: 0x04000A31 RID: 2609
		internal int _trackcount;

		// Token: 0x04000A32 RID: 2610
		internal Hashtable _caps;

		// Token: 0x04000A33 RID: 2611
		internal int _capsize;

		// Token: 0x04000A34 RID: 2612
		internal RegexPrefix _fcPrefix;

		// Token: 0x04000A35 RID: 2613
		internal RegexBoyerMoore _bmPrefix;

		// Token: 0x04000A36 RID: 2614
		internal int _anchors;

		// Token: 0x04000A37 RID: 2615
		internal bool _rightToLeft;
	}
}
