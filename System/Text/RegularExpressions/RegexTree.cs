﻿using System;
using System.Collections;

namespace System.Text.RegularExpressions
{
	// Token: 0x020000E8 RID: 232
	internal sealed class RegexTree
	{
		// Token: 0x06000670 RID: 1648 RVA: 0x00021F32 File Offset: 0x00020132
		internal RegexTree(RegexNode root, Hashtable caps, int[] capnumlist, int captop, Hashtable capnames, string[] capslist, RegexOptions opts)
		{
			this._root = root;
			this._caps = caps;
			this._capnumlist = capnumlist;
			this._capnames = capnames;
			this._capslist = capslist;
			this._captop = captop;
			this._options = opts;
		}

		// Token: 0x04000AF0 RID: 2800
		internal RegexNode _root;

		// Token: 0x04000AF1 RID: 2801
		internal Hashtable _caps;

		// Token: 0x04000AF2 RID: 2802
		internal int[] _capnumlist;

		// Token: 0x04000AF3 RID: 2803
		internal Hashtable _capnames;

		// Token: 0x04000AF4 RID: 2804
		internal string[] _capslist;

		// Token: 0x04000AF5 RID: 2805
		internal RegexOptions _options;

		// Token: 0x04000AF6 RID: 2806
		internal int _captop;
	}
}
