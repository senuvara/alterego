﻿using System;
using System.Globalization;

namespace System.Text.RegularExpressions
{
	// Token: 0x020000D6 RID: 214
	internal sealed class RegexFCD
	{
		// Token: 0x0600055E RID: 1374 RVA: 0x0001AEDC File Offset: 0x000190DC
		internal static RegexPrefix FirstChars(RegexTree t)
		{
			RegexFC regexFC = new RegexFCD().RegexFCFromRegexTree(t);
			if (regexFC == null || regexFC._nullable)
			{
				return null;
			}
			CultureInfo culture = ((t._options & RegexOptions.CultureInvariant) != RegexOptions.None) ? CultureInfo.InvariantCulture : CultureInfo.CurrentCulture;
			return new RegexPrefix(regexFC.GetFirstChars(culture), regexFC.IsCaseInsensitive());
		}

		// Token: 0x0600055F RID: 1375 RVA: 0x0001AF30 File Offset: 0x00019130
		internal static RegexPrefix Prefix(RegexTree tree)
		{
			RegexNode regexNode = null;
			int num = 0;
			RegexNode regexNode2 = tree._root;
			for (;;)
			{
				int type = regexNode2._type;
				switch (type)
				{
				case 3:
				case 6:
					goto IL_C3;
				case 4:
				case 5:
				case 7:
				case 8:
				case 10:
				case 11:
				case 13:
				case 17:
				case 22:
				case 24:
				case 26:
				case 27:
				case 29:
					goto IL_131;
				case 9:
					goto IL_F9;
				case 12:
					goto IL_11A;
				case 14:
				case 15:
				case 16:
				case 18:
				case 19:
				case 20:
				case 21:
				case 23:
				case 30:
				case 31:
					break;
				case 25:
					if (regexNode2.ChildCount() > 0)
					{
						regexNode = regexNode2;
						num = 0;
					}
					break;
				case 28:
				case 32:
					regexNode2 = regexNode2.Child(0);
					regexNode = null;
					continue;
				default:
					if (type != 41)
					{
						goto Block_2;
					}
					break;
				}
				if (regexNode == null || num >= regexNode.ChildCount())
				{
					goto IL_143;
				}
				regexNode2 = regexNode.Child(num++);
			}
			Block_2:
			goto IL_131;
			IL_C3:
			if (regexNode2._m > 0)
			{
				return new RegexPrefix(string.Empty.PadRight(regexNode2._m, regexNode2._ch), (regexNode2._options & RegexOptions.IgnoreCase) > RegexOptions.None);
			}
			return RegexPrefix.Empty;
			IL_F9:
			return new RegexPrefix(regexNode2._ch.ToString(CultureInfo.InvariantCulture), (regexNode2._options & RegexOptions.IgnoreCase) > RegexOptions.None);
			IL_11A:
			return new RegexPrefix(regexNode2._str, (regexNode2._options & RegexOptions.IgnoreCase) > RegexOptions.None);
			IL_131:
			return RegexPrefix.Empty;
			IL_143:
			return RegexPrefix.Empty;
		}

		// Token: 0x06000560 RID: 1376 RVA: 0x0001B098 File Offset: 0x00019298
		internal static int Anchors(RegexTree tree)
		{
			RegexNode regexNode = null;
			int num = 0;
			int num2 = 0;
			RegexNode regexNode2 = tree._root;
			int type;
			for (;;)
			{
				type = regexNode2._type;
				switch (type)
				{
				case 14:
				case 15:
				case 16:
				case 18:
				case 19:
				case 20:
				case 21:
					goto IL_91;
				case 17:
				case 22:
				case 24:
				case 26:
				case 27:
				case 29:
					return num2;
				case 23:
				case 30:
				case 31:
					goto IL_A1;
				case 25:
					if (regexNode2.ChildCount() > 0)
					{
						regexNode = regexNode2;
						num = 0;
						goto IL_A1;
					}
					goto IL_A1;
				case 28:
				case 32:
					regexNode2 = regexNode2.Child(0);
					regexNode = null;
					continue;
				}
				break;
				IL_A1:
				if (regexNode == null || num >= regexNode.ChildCount())
				{
					return num2;
				}
				regexNode2 = regexNode.Child(num++);
			}
			if (type != 41)
			{
				return num2;
			}
			IL_91:
			return num2 | RegexFCD.AnchorFromType(regexNode2._type);
		}

		// Token: 0x06000561 RID: 1377 RVA: 0x0001B164 File Offset: 0x00019364
		private static int AnchorFromType(int type)
		{
			switch (type)
			{
			case 14:
				return 2;
			case 15:
				return 8;
			case 16:
				return 64;
			case 17:
				break;
			case 18:
				return 1;
			case 19:
				return 4;
			case 20:
				return 16;
			case 21:
				return 32;
			default:
				if (type == 41)
				{
					return 128;
				}
				break;
			}
			return 0;
		}

		// Token: 0x06000562 RID: 1378 RVA: 0x0001B1B9 File Offset: 0x000193B9
		private RegexFCD()
		{
			this._fcStack = new RegexFC[32];
			this._intStack = new int[32];
		}

		// Token: 0x06000563 RID: 1379 RVA: 0x0001B1DC File Offset: 0x000193DC
		private void PushInt(int I)
		{
			if (this._intDepth >= this._intStack.Length)
			{
				int[] array = new int[this._intDepth * 2];
				Array.Copy(this._intStack, 0, array, 0, this._intDepth);
				this._intStack = array;
			}
			int[] intStack = this._intStack;
			int intDepth = this._intDepth;
			this._intDepth = intDepth + 1;
			intStack[intDepth] = I;
		}

		// Token: 0x06000564 RID: 1380 RVA: 0x0001B23B File Offset: 0x0001943B
		private bool IntIsEmpty()
		{
			return this._intDepth == 0;
		}

		// Token: 0x06000565 RID: 1381 RVA: 0x0001B248 File Offset: 0x00019448
		private int PopInt()
		{
			int[] intStack = this._intStack;
			int num = this._intDepth - 1;
			this._intDepth = num;
			return intStack[num];
		}

		// Token: 0x06000566 RID: 1382 RVA: 0x0001B270 File Offset: 0x00019470
		private void PushFC(RegexFC fc)
		{
			if (this._fcDepth >= this._fcStack.Length)
			{
				RegexFC[] array = new RegexFC[this._fcDepth * 2];
				Array.Copy(this._fcStack, 0, array, 0, this._fcDepth);
				this._fcStack = array;
			}
			RegexFC[] fcStack = this._fcStack;
			int fcDepth = this._fcDepth;
			this._fcDepth = fcDepth + 1;
			fcStack[fcDepth] = fc;
		}

		// Token: 0x06000567 RID: 1383 RVA: 0x0001B2CF File Offset: 0x000194CF
		private bool FCIsEmpty()
		{
			return this._fcDepth == 0;
		}

		// Token: 0x06000568 RID: 1384 RVA: 0x0001B2DC File Offset: 0x000194DC
		private RegexFC PopFC()
		{
			RegexFC[] fcStack = this._fcStack;
			int num = this._fcDepth - 1;
			this._fcDepth = num;
			return fcStack[num];
		}

		// Token: 0x06000569 RID: 1385 RVA: 0x0001B301 File Offset: 0x00019501
		private RegexFC TopFC()
		{
			return this._fcStack[this._fcDepth - 1];
		}

		// Token: 0x0600056A RID: 1386 RVA: 0x0001B314 File Offset: 0x00019514
		private RegexFC RegexFCFromRegexTree(RegexTree tree)
		{
			RegexNode regexNode = tree._root;
			int num = 0;
			for (;;)
			{
				if (regexNode._children == null)
				{
					this.CalculateFC(regexNode._type, regexNode, 0);
				}
				else if (num < regexNode._children.Count && !this._skipAllChildren)
				{
					this.CalculateFC(regexNode._type | 64, regexNode, num);
					if (!this._skipchild)
					{
						regexNode = regexNode._children[num];
						this.PushInt(num);
						num = 0;
						continue;
					}
					num++;
					this._skipchild = false;
					continue;
				}
				this._skipAllChildren = false;
				if (this.IntIsEmpty())
				{
					goto IL_B9;
				}
				num = this.PopInt();
				regexNode = regexNode._next;
				this.CalculateFC(regexNode._type | 128, regexNode, num);
				if (this._failed)
				{
					break;
				}
				num++;
			}
			return null;
			IL_B9:
			if (this.FCIsEmpty())
			{
				return null;
			}
			return this.PopFC();
		}

		// Token: 0x0600056B RID: 1387 RVA: 0x0001B3EA File Offset: 0x000195EA
		private void SkipChild()
		{
			this._skipchild = true;
		}

		// Token: 0x0600056C RID: 1388 RVA: 0x0001B3F4 File Offset: 0x000195F4
		private void CalculateFC(int NodeType, RegexNode node, int CurIndex)
		{
			bool caseInsensitive = false;
			bool flag = false;
			if (NodeType <= 13)
			{
				if ((node._options & RegexOptions.IgnoreCase) != RegexOptions.None)
				{
					caseInsensitive = true;
				}
				if ((node._options & RegexOptions.RightToLeft) != RegexOptions.None)
				{
					flag = true;
				}
			}
			switch (NodeType)
			{
			case 3:
			case 6:
				this.PushFC(new RegexFC(node._ch, false, node._m == 0, caseInsensitive));
				return;
			case 4:
			case 7:
				this.PushFC(new RegexFC(node._ch, true, node._m == 0, caseInsensitive));
				return;
			case 5:
			case 8:
				this.PushFC(new RegexFC(node._str, node._m == 0, caseInsensitive));
				return;
			case 9:
			case 10:
				this.PushFC(new RegexFC(node._ch, NodeType == 10, false, caseInsensitive));
				return;
			case 11:
				this.PushFC(new RegexFC(node._str, false, caseInsensitive));
				return;
			case 12:
				if (node._str.Length == 0)
				{
					this.PushFC(new RegexFC(true));
					return;
				}
				if (!flag)
				{
					this.PushFC(new RegexFC(node._str[0], false, false, caseInsensitive));
					return;
				}
				this.PushFC(new RegexFC(node._str[node._str.Length - 1], false, false, caseInsensitive));
				return;
			case 13:
				this.PushFC(new RegexFC("\0\u0001\0\0", true, false));
				return;
			case 14:
			case 15:
			case 16:
			case 17:
			case 18:
			case 19:
			case 20:
			case 21:
			case 22:
			case 41:
			case 42:
				this.PushFC(new RegexFC(true));
				return;
			case 23:
				this.PushFC(new RegexFC(true));
				return;
			case 24:
			case 25:
			case 26:
			case 27:
			case 28:
			case 29:
			case 30:
			case 31:
			case 32:
			case 33:
			case 34:
			case 35:
			case 36:
			case 37:
			case 38:
			case 39:
			case 40:
				break;
			default:
				switch (NodeType)
				{
				case 88:
				case 89:
				case 90:
				case 91:
				case 92:
				case 93:
				case 96:
				case 97:
					break;
				case 94:
				case 95:
					this.SkipChild();
					this.PushFC(new RegexFC(true));
					return;
				case 98:
					if (CurIndex == 0)
					{
						this.SkipChild();
						return;
					}
					break;
				default:
					switch (NodeType)
					{
					case 152:
					case 161:
						if (CurIndex != 0)
						{
							RegexFC fc = this.PopFC();
							RegexFC regexFC = this.TopFC();
							this._failed = !regexFC.AddFC(fc, false);
							return;
						}
						break;
					case 153:
						if (CurIndex != 0)
						{
							RegexFC fc2 = this.PopFC();
							RegexFC regexFC2 = this.TopFC();
							this._failed = !regexFC2.AddFC(fc2, true);
						}
						if (!this.TopFC()._nullable)
						{
							this._skipAllChildren = true;
							return;
						}
						break;
					case 154:
					case 155:
						if (node._m == 0)
						{
							this.TopFC()._nullable = true;
							return;
						}
						break;
					case 156:
					case 157:
					case 158:
					case 159:
					case 160:
						break;
					case 162:
						if (CurIndex > 1)
						{
							RegexFC fc3 = this.PopFC();
							RegexFC regexFC3 = this.TopFC();
							this._failed = !regexFC3.AddFC(fc3, false);
							return;
						}
						break;
					default:
						goto IL_312;
					}
					break;
				}
				return;
			}
			IL_312:
			throw new ArgumentException(SR.GetString("Unexpected opcode in regular expression generation: {0}.", new object[]
			{
				NodeType.ToString(CultureInfo.CurrentCulture)
			}));
		}

		// Token: 0x04000A3E RID: 2622
		private int[] _intStack;

		// Token: 0x04000A3F RID: 2623
		private int _intDepth;

		// Token: 0x04000A40 RID: 2624
		private RegexFC[] _fcStack;

		// Token: 0x04000A41 RID: 2625
		private int _fcDepth;

		// Token: 0x04000A42 RID: 2626
		private bool _skipAllChildren;

		// Token: 0x04000A43 RID: 2627
		private bool _skipchild;

		// Token: 0x04000A44 RID: 2628
		private bool _failed;

		// Token: 0x04000A45 RID: 2629
		private const int BeforeChild = 64;

		// Token: 0x04000A46 RID: 2630
		private const int AfterChild = 128;

		// Token: 0x04000A47 RID: 2631
		internal const int Beginning = 1;

		// Token: 0x04000A48 RID: 2632
		internal const int Bol = 2;

		// Token: 0x04000A49 RID: 2633
		internal const int Start = 4;

		// Token: 0x04000A4A RID: 2634
		internal const int Eol = 8;

		// Token: 0x04000A4B RID: 2635
		internal const int EndZ = 16;

		// Token: 0x04000A4C RID: 2636
		internal const int End = 32;

		// Token: 0x04000A4D RID: 2637
		internal const int Boundary = 64;

		// Token: 0x04000A4E RID: 2638
		internal const int ECMABoundary = 128;
	}
}
