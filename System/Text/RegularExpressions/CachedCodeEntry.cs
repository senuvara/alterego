﻿using System;
using System.Collections;

namespace System.Text.RegularExpressions
{
	// Token: 0x020000C9 RID: 201
	internal sealed class CachedCodeEntry
	{
		// Token: 0x060004FA RID: 1274 RVA: 0x00017A2C File Offset: 0x00015C2C
		internal CachedCodeEntry(string key, Hashtable capnames, string[] capslist, RegexCode code, Hashtable caps, int capsize, ExclusiveReference runner, SharedReference repl)
		{
			this._key = key;
			this._capnames = capnames;
			this._capslist = capslist;
			this._code = code;
			this._caps = caps;
			this._capsize = capsize;
			this._runnerref = runner;
			this._replref = repl;
		}

		// Token: 0x060004FB RID: 1275 RVA: 0x00017A7C File Offset: 0x00015C7C
		internal void AddCompiled(RegexRunnerFactory factory)
		{
			this._factory = factory;
			this._code = null;
		}

		// Token: 0x040009A9 RID: 2473
		internal string _key;

		// Token: 0x040009AA RID: 2474
		internal RegexCode _code;

		// Token: 0x040009AB RID: 2475
		internal Hashtable _caps;

		// Token: 0x040009AC RID: 2476
		internal Hashtable _capnames;

		// Token: 0x040009AD RID: 2477
		internal string[] _capslist;

		// Token: 0x040009AE RID: 2478
		internal int _capsize;

		// Token: 0x040009AF RID: 2479
		internal RegexRunnerFactory _factory;

		// Token: 0x040009B0 RID: 2480
		internal ExclusiveReference _runnerref;

		// Token: 0x040009B1 RID: 2481
		internal SharedReference _replref;
	}
}
