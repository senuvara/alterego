﻿using System;
using System.Globalization;

namespace System.Text.RegularExpressions
{
	// Token: 0x020000D7 RID: 215
	internal sealed class RegexFC
	{
		// Token: 0x0600056D RID: 1389 RVA: 0x0001B738 File Offset: 0x00019938
		internal RegexFC(bool nullable)
		{
			this._cc = new RegexCharClass();
			this._nullable = nullable;
		}

		// Token: 0x0600056E RID: 1390 RVA: 0x0001B754 File Offset: 0x00019954
		internal RegexFC(char ch, bool not, bool nullable, bool caseInsensitive)
		{
			this._cc = new RegexCharClass();
			if (not)
			{
				if (ch > '\0')
				{
					this._cc.AddRange('\0', ch - '\u0001');
				}
				if (ch < '￿')
				{
					this._cc.AddRange(ch + '\u0001', char.MaxValue);
				}
			}
			else
			{
				this._cc.AddRange(ch, ch);
			}
			this._caseInsensitive = caseInsensitive;
			this._nullable = nullable;
		}

		// Token: 0x0600056F RID: 1391 RVA: 0x0001B7C3 File Offset: 0x000199C3
		internal RegexFC(string charClass, bool nullable, bool caseInsensitive)
		{
			this._cc = RegexCharClass.Parse(charClass);
			this._nullable = nullable;
			this._caseInsensitive = caseInsensitive;
		}

		// Token: 0x06000570 RID: 1392 RVA: 0x0001B7E8 File Offset: 0x000199E8
		internal bool AddFC(RegexFC fc, bool concatenate)
		{
			if (!this._cc.CanMerge || !fc._cc.CanMerge)
			{
				return false;
			}
			if (concatenate)
			{
				if (!this._nullable)
				{
					return true;
				}
				if (!fc._nullable)
				{
					this._nullable = false;
				}
			}
			else if (fc._nullable)
			{
				this._nullable = true;
			}
			this._caseInsensitive |= fc._caseInsensitive;
			this._cc.AddCharClass(fc._cc);
			return true;
		}

		// Token: 0x06000571 RID: 1393 RVA: 0x0001B863 File Offset: 0x00019A63
		internal string GetFirstChars(CultureInfo culture)
		{
			if (this._caseInsensitive)
			{
				this._cc.AddLowercase(culture);
			}
			return this._cc.ToStringClass();
		}

		// Token: 0x06000572 RID: 1394 RVA: 0x0001B884 File Offset: 0x00019A84
		internal bool IsCaseInsensitive()
		{
			return this._caseInsensitive;
		}

		// Token: 0x04000A4F RID: 2639
		internal RegexCharClass _cc;

		// Token: 0x04000A50 RID: 2640
		internal bool _nullable;

		// Token: 0x04000A51 RID: 2641
		internal bool _caseInsensitive;
	}
}
