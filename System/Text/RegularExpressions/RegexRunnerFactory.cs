﻿using System;
using System.ComponentModel;

namespace System.Text.RegularExpressions
{
	/// <summary>Creates a <see cref="T:System.Text.RegularExpressions.RegexRunner" /> class for a compiled regular expression.</summary>
	// Token: 0x020000E7 RID: 231
	[EditorBrowsable(EditorBrowsableState.Never)]
	public abstract class RegexRunnerFactory
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Text.RegularExpressions.RegexRunnerFactory" /> class. </summary>
		// Token: 0x0600066E RID: 1646 RVA: 0x0000232F File Offset: 0x0000052F
		protected RegexRunnerFactory()
		{
		}

		/// <summary>When overridden in a derived class, creates a <see cref="T:System.Text.RegularExpressions.RegexRunner" /> object for a specific compiled regular expression.</summary>
		/// <returns>A <see cref="T:System.Text.RegularExpressions.RegexRunner" /> object designed to execute a specific compiled regular expression. </returns>
		// Token: 0x0600066F RID: 1647
		protected internal abstract RegexRunner CreateInstance();
	}
}
