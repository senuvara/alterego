﻿using System;
using Unity;

namespace System.Text.RegularExpressions
{
	/// <summary>Represents the results from a single successful subexpression capture. </summary>
	// Token: 0x020000CD RID: 205
	[Serializable]
	public class Capture
	{
		// Token: 0x06000507 RID: 1287 RVA: 0x000180B8 File Offset: 0x000162B8
		internal Capture(string text, int i, int l)
		{
			this._text = text;
			this._index = i;
			this._length = l;
		}

		/// <summary>The position in the original string where the first character of the captured substring is found.</summary>
		/// <returns>The zero-based starting position in the original string where the captured substring is found.</returns>
		// Token: 0x170000E6 RID: 230
		// (get) Token: 0x06000508 RID: 1288 RVA: 0x000180D5 File Offset: 0x000162D5
		public int Index
		{
			get
			{
				return this._index;
			}
		}

		/// <summary>Gets the length of the captured substring.</summary>
		/// <returns>The length of the captured substring.</returns>
		// Token: 0x170000E7 RID: 231
		// (get) Token: 0x06000509 RID: 1289 RVA: 0x000180DD File Offset: 0x000162DD
		public int Length
		{
			get
			{
				return this._length;
			}
		}

		/// <summary>Gets the captured substring from the input string.</summary>
		/// <returns>The substring that is captured by the match.</returns>
		// Token: 0x170000E8 RID: 232
		// (get) Token: 0x0600050A RID: 1290 RVA: 0x000180E5 File Offset: 0x000162E5
		public string Value
		{
			get
			{
				return this._text.Substring(this._index, this._length);
			}
		}

		/// <summary>Retrieves the captured substring from the input string by calling the <see cref="P:System.Text.RegularExpressions.Capture.Value" /> property. </summary>
		/// <returns>The substring that was captured by the match.</returns>
		// Token: 0x0600050B RID: 1291 RVA: 0x000180FE File Offset: 0x000162FE
		public override string ToString()
		{
			return this.Value;
		}

		// Token: 0x0600050C RID: 1292 RVA: 0x00018106 File Offset: 0x00016306
		internal string GetOriginalString()
		{
			return this._text;
		}

		// Token: 0x0600050D RID: 1293 RVA: 0x0001810E File Offset: 0x0001630E
		internal string GetLeftSubstring()
		{
			return this._text.Substring(0, this._index);
		}

		// Token: 0x0600050E RID: 1294 RVA: 0x00018122 File Offset: 0x00016322
		internal string GetRightSubstring()
		{
			return this._text.Substring(this._index + this._length, this._text.Length - this._index - this._length);
		}

		// Token: 0x0600050F RID: 1295 RVA: 0x000092E2 File Offset: 0x000074E2
		internal Capture()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040009C1 RID: 2497
		internal string _text;

		// Token: 0x040009C2 RID: 2498
		internal int _index;

		// Token: 0x040009C3 RID: 2499
		internal int _length;
	}
}
