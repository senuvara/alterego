﻿using System;
using System.Collections;
using Unity;

namespace System.Text.RegularExpressions
{
	/// <summary>Represents the set of captures made by a single capturing group. </summary>
	// Token: 0x020000CE RID: 206
	[Serializable]
	public class CaptureCollection : ICollection, IEnumerable
	{
		// Token: 0x06000510 RID: 1296 RVA: 0x00018155 File Offset: 0x00016355
		internal CaptureCollection(Group group)
		{
			this._group = group;
			this._capcount = this._group._capcount;
		}

		/// <summary>Gets an object that can be used to synchronize access to the collection.</summary>
		/// <returns>An object that can be used to synchronize access to the collection.</returns>
		// Token: 0x170000E9 RID: 233
		// (get) Token: 0x06000511 RID: 1297 RVA: 0x00018175 File Offset: 0x00016375
		public object SyncRoot
		{
			get
			{
				return this._group;
			}
		}

		/// <summary>Gets a value that indicates whether access to the collection is synchronized (thread-safe).</summary>
		/// <returns>
		///     <see langword="false" /> in all cases.</returns>
		// Token: 0x170000EA RID: 234
		// (get) Token: 0x06000512 RID: 1298 RVA: 0x00005AFA File Offset: 0x00003CFA
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets a value that indicates whether the collection is read only.</summary>
		/// <returns>
		///     <see langword="true" /> in all cases.</returns>
		// Token: 0x170000EB RID: 235
		// (get) Token: 0x06000513 RID: 1299 RVA: 0x00003298 File Offset: 0x00001498
		public bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		/// <summary>Gets the number of substrings captured by the group.</summary>
		/// <returns>The number of items in the <see cref="T:System.Text.RegularExpressions.CaptureCollection" />.</returns>
		// Token: 0x170000EC RID: 236
		// (get) Token: 0x06000514 RID: 1300 RVA: 0x0001817D File Offset: 0x0001637D
		public int Count
		{
			get
			{
				return this._capcount;
			}
		}

		/// <summary>Gets an individual member of the collection.</summary>
		/// <param name="i">Index into the capture collection. </param>
		/// <returns>The captured substring at position <paramref name="i" /> in the collection.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="i" /> is less than 0 or greater than <see cref="P:System.Text.RegularExpressions.CaptureCollection.Count" />. </exception>
		// Token: 0x170000ED RID: 237
		public Capture this[int i]
		{
			get
			{
				return this.GetCapture(i);
			}
		}

		/// <summary>Copies all the elements of the collection to the given array beginning at the given index.</summary>
		/// <param name="array">The array the collection is to be copied into. </param>
		/// <param name="arrayIndex">The position in the destination array where copying is to begin. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array " />is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="arrayIndex" /> is outside the bounds of <paramref name="array" />. -or-
		///         <paramref name="arrayIndex" /> plus <see cref="P:System.Text.RegularExpressions.CaptureCollection.Count" /> is outside the bounds of <paramref name="array" />. </exception>
		// Token: 0x06000516 RID: 1302 RVA: 0x00018190 File Offset: 0x00016390
		public void CopyTo(Array array, int arrayIndex)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			int num = arrayIndex;
			for (int i = 0; i < this.Count; i++)
			{
				array.SetValue(this[i], num);
				num++;
			}
		}

		/// <summary>Provides an enumerator that iterates through the collection.</summary>
		/// <returns>An object that contains all <see cref="T:System.Text.RegularExpressions.Capture" /> objects within the <see cref="T:System.Text.RegularExpressions.CaptureCollection" />.</returns>
		// Token: 0x06000517 RID: 1303 RVA: 0x000181D0 File Offset: 0x000163D0
		public IEnumerator GetEnumerator()
		{
			return new CaptureEnumerator(this);
		}

		// Token: 0x06000518 RID: 1304 RVA: 0x000181D8 File Offset: 0x000163D8
		internal Capture GetCapture(int i)
		{
			if (i == this._capcount - 1 && i >= 0)
			{
				return this._group;
			}
			if (i >= this._capcount || i < 0)
			{
				throw new ArgumentOutOfRangeException("i");
			}
			if (this._captures == null)
			{
				this._captures = new Capture[this._capcount];
				for (int j = 0; j < this._capcount - 1; j++)
				{
					this._captures[j] = new Capture(this._group._text, this._group._caps[j * 2], this._group._caps[j * 2 + 1]);
				}
			}
			return this._captures[i];
		}

		// Token: 0x06000519 RID: 1305 RVA: 0x000092E2 File Offset: 0x000074E2
		internal CaptureCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040009C4 RID: 2500
		internal Group _group;

		// Token: 0x040009C5 RID: 2501
		internal int _capcount;

		// Token: 0x040009C6 RID: 2502
		internal Capture[] _captures;
	}
}
