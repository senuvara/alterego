﻿using System;

namespace System.Text.RegularExpressions
{
	// Token: 0x020000D8 RID: 216
	internal sealed class RegexPrefix
	{
		// Token: 0x06000573 RID: 1395 RVA: 0x0001B88C File Offset: 0x00019A8C
		internal RegexPrefix(string prefix, bool ci)
		{
			this._prefix = prefix;
			this._caseInsensitive = ci;
		}

		// Token: 0x170000F8 RID: 248
		// (get) Token: 0x06000574 RID: 1396 RVA: 0x0001B8A2 File Offset: 0x00019AA2
		internal string Prefix
		{
			get
			{
				return this._prefix;
			}
		}

		// Token: 0x170000F9 RID: 249
		// (get) Token: 0x06000575 RID: 1397 RVA: 0x0001B8AA File Offset: 0x00019AAA
		internal bool CaseInsensitive
		{
			get
			{
				return this._caseInsensitive;
			}
		}

		// Token: 0x170000FA RID: 250
		// (get) Token: 0x06000576 RID: 1398 RVA: 0x0001B8B2 File Offset: 0x00019AB2
		internal static RegexPrefix Empty
		{
			get
			{
				return RegexPrefix._empty;
			}
		}

		// Token: 0x06000577 RID: 1399 RVA: 0x0001B8B9 File Offset: 0x00019AB9
		// Note: this type is marked as 'beforefieldinit'.
		static RegexPrefix()
		{
		}

		// Token: 0x04000A52 RID: 2642
		internal string _prefix;

		// Token: 0x04000A53 RID: 2643
		internal bool _caseInsensitive;

		// Token: 0x04000A54 RID: 2644
		internal static RegexPrefix _empty = new RegexPrefix(string.Empty, false);
	}
}
