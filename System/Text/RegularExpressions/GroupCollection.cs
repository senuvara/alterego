﻿using System;
using System.Collections;
using Unity;

namespace System.Text.RegularExpressions
{
	/// <summary>Returns the set of captured groups in a single match.</summary>
	// Token: 0x020000DA RID: 218
	[Serializable]
	public class GroupCollection : ICollection, IEnumerable
	{
		// Token: 0x0600057F RID: 1407 RVA: 0x0001B985 File Offset: 0x00019B85
		internal GroupCollection(Match match, Hashtable caps)
		{
			this._match = match;
			this._captureMap = caps;
		}

		/// <summary>Gets an object that can be used to synchronize access to the <see cref="T:System.Text.RegularExpressions.GroupCollection" />.</summary>
		/// <returns>A copy of the <see cref="T:System.Text.RegularExpressions.Match" /> object to synchronize.</returns>
		// Token: 0x170000FE RID: 254
		// (get) Token: 0x06000580 RID: 1408 RVA: 0x0001B99B File Offset: 0x00019B9B
		public object SyncRoot
		{
			get
			{
				return this._match;
			}
		}

		/// <summary>Gets a value that indicates whether access to the <see cref="T:System.Text.RegularExpressions.GroupCollection" /> is synchronized (thread-safe).</summary>
		/// <returns>
		///     <see langword="false" /> in all cases.</returns>
		// Token: 0x170000FF RID: 255
		// (get) Token: 0x06000581 RID: 1409 RVA: 0x00005AFA File Offset: 0x00003CFA
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets a value that indicates whether the collection is read-only.</summary>
		/// <returns>
		///     <see langword="true" /> in all cases.</returns>
		// Token: 0x17000100 RID: 256
		// (get) Token: 0x06000582 RID: 1410 RVA: 0x00003298 File Offset: 0x00001498
		public bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		/// <summary>Returns the number of groups in the collection.</summary>
		/// <returns>The number of groups in the collection.</returns>
		// Token: 0x17000101 RID: 257
		// (get) Token: 0x06000583 RID: 1411 RVA: 0x0001B9A3 File Offset: 0x00019BA3
		public int Count
		{
			get
			{
				return this._match._matchcount.Length;
			}
		}

		/// <summary>Enables access to a member of the collection by integer index.</summary>
		/// <param name="groupnum">The zero-based index of the collection member to be retrieved. </param>
		/// <returns>The member of the collection specified by <paramref name="groupnum" />.</returns>
		// Token: 0x17000102 RID: 258
		public Group this[int groupnum]
		{
			get
			{
				return this.GetGroup(groupnum);
			}
		}

		/// <summary>Enables access to a member of the collection by string index.</summary>
		/// <param name="groupname">The name of a capturing group. </param>
		/// <returns>The member of the collection specified by <paramref name="groupname" />.</returns>
		// Token: 0x17000103 RID: 259
		public Group this[string groupname]
		{
			get
			{
				if (this._match._regex == null)
				{
					return Group._emptygroup;
				}
				return this.GetGroup(this._match._regex.GroupNumberFromName(groupname));
			}
		}

		// Token: 0x06000586 RID: 1414 RVA: 0x0001B9E8 File Offset: 0x00019BE8
		internal Group GetGroup(int groupnum)
		{
			if (this._captureMap != null)
			{
				object obj = this._captureMap[groupnum];
				if (obj == null)
				{
					return Group._emptygroup;
				}
				return this.GetGroupImpl((int)obj);
			}
			else
			{
				if (groupnum >= this._match._matchcount.Length || groupnum < 0)
				{
					return Group._emptygroup;
				}
				return this.GetGroupImpl(groupnum);
			}
		}

		// Token: 0x06000587 RID: 1415 RVA: 0x0001BA48 File Offset: 0x00019C48
		internal Group GetGroupImpl(int groupnum)
		{
			if (groupnum == 0)
			{
				return this._match;
			}
			if (this._groups == null)
			{
				this._groups = new Group[this._match._matchcount.Length - 1];
				for (int i = 0; i < this._groups.Length; i++)
				{
					string name = this._match._regex.GroupNameFromNumber(i + 1);
					this._groups[i] = new Group(this._match._text, this._match._matches[i + 1], this._match._matchcount[i + 1], name);
				}
			}
			return this._groups[groupnum - 1];
		}

		/// <summary>Copies all the elements of the collection to the given array beginning at the given index.</summary>
		/// <param name="array">The array the collection is to be copied into. </param>
		/// <param name="arrayIndex">The position in the destination array where the copying is to begin. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.IndexOutOfRangeException">
		///         <paramref name="arrayIndex" /> is outside the bounds of <paramref name="array" />.-or-
		///         <paramref name="arrayIndex" /> plus <see cref="P:System.Text.RegularExpressions.GroupCollection.Count" /> is outside the bounds of <paramref name="array" />.</exception>
		// Token: 0x06000588 RID: 1416 RVA: 0x0001BAEC File Offset: 0x00019CEC
		public void CopyTo(Array array, int arrayIndex)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			int num = arrayIndex;
			for (int i = 0; i < this.Count; i++)
			{
				array.SetValue(this[i], num);
				num++;
			}
		}

		/// <summary>Provides an enumerator that iterates through the collection.</summary>
		/// <returns>An enumerator that contains all <see cref="T:System.Text.RegularExpressions.Group" /> objects in the <see cref="T:System.Text.RegularExpressions.GroupCollection" />.</returns>
		// Token: 0x06000589 RID: 1417 RVA: 0x0001BB2C File Offset: 0x00019D2C
		public IEnumerator GetEnumerator()
		{
			return new GroupEnumerator(this);
		}

		// Token: 0x0600058A RID: 1418 RVA: 0x000092E2 File Offset: 0x000074E2
		internal GroupCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000A5A RID: 2650
		internal Match _match;

		// Token: 0x04000A5B RID: 2651
		internal Hashtable _captureMap;

		// Token: 0x04000A5C RID: 2652
		internal Group[] _groups;
	}
}
