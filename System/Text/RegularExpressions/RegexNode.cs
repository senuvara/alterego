﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace System.Text.RegularExpressions
{
	// Token: 0x020000E2 RID: 226
	internal sealed class RegexNode
	{
		// Token: 0x060005E7 RID: 1511 RVA: 0x0001DD29 File Offset: 0x0001BF29
		internal RegexNode(int type, RegexOptions options)
		{
			this._type = type;
			this._options = options;
		}

		// Token: 0x060005E8 RID: 1512 RVA: 0x0001DD3F File Offset: 0x0001BF3F
		internal RegexNode(int type, RegexOptions options, char ch)
		{
			this._type = type;
			this._options = options;
			this._ch = ch;
		}

		// Token: 0x060005E9 RID: 1513 RVA: 0x0001DD5C File Offset: 0x0001BF5C
		internal RegexNode(int type, RegexOptions options, string str)
		{
			this._type = type;
			this._options = options;
			this._str = str;
		}

		// Token: 0x060005EA RID: 1514 RVA: 0x0001DD79 File Offset: 0x0001BF79
		internal RegexNode(int type, RegexOptions options, int m)
		{
			this._type = type;
			this._options = options;
			this._m = m;
		}

		// Token: 0x060005EB RID: 1515 RVA: 0x0001DD96 File Offset: 0x0001BF96
		internal RegexNode(int type, RegexOptions options, int m, int n)
		{
			this._type = type;
			this._options = options;
			this._m = m;
			this._n = n;
		}

		// Token: 0x060005EC RID: 1516 RVA: 0x0001DDBB File Offset: 0x0001BFBB
		internal bool UseOptionR()
		{
			return (this._options & RegexOptions.RightToLeft) > RegexOptions.None;
		}

		// Token: 0x060005ED RID: 1517 RVA: 0x0001DDC9 File Offset: 0x0001BFC9
		internal RegexNode ReverseLeft()
		{
			if (this.UseOptionR() && this._type == 25 && this._children != null)
			{
				this._children.Reverse(0, this._children.Count);
			}
			return this;
		}

		// Token: 0x060005EE RID: 1518 RVA: 0x0001DDFD File Offset: 0x0001BFFD
		internal void MakeRep(int type, int min, int max)
		{
			this._type += type - 9;
			this._m = min;
			this._n = max;
		}

		// Token: 0x060005EF RID: 1519 RVA: 0x0001DE20 File Offset: 0x0001C020
		internal RegexNode Reduce()
		{
			int num = this.Type();
			RegexNode result;
			if (num != 5 && num != 11)
			{
				switch (num)
				{
				case 24:
					return this.ReduceAlternation();
				case 25:
					return this.ReduceConcatenation();
				case 26:
				case 27:
					return this.ReduceRep();
				case 29:
					return this.ReduceGroup();
				}
				result = this;
			}
			else
			{
				result = this.ReduceSet();
			}
			return result;
		}

		// Token: 0x060005F0 RID: 1520 RVA: 0x0001DE90 File Offset: 0x0001C090
		internal RegexNode StripEnation(int emptyType)
		{
			int num = this.ChildCount();
			if (num == 0)
			{
				return new RegexNode(emptyType, this._options);
			}
			if (num != 1)
			{
				return this;
			}
			return this.Child(0);
		}

		// Token: 0x060005F1 RID: 1521 RVA: 0x0001DEC4 File Offset: 0x0001C0C4
		internal RegexNode ReduceGroup()
		{
			RegexNode regexNode = this;
			while (regexNode.Type() == 29)
			{
				regexNode = regexNode.Child(0);
			}
			return regexNode;
		}

		// Token: 0x060005F2 RID: 1522 RVA: 0x0001DEE8 File Offset: 0x0001C0E8
		internal RegexNode ReduceRep()
		{
			RegexNode regexNode = this;
			int num = this.Type();
			int num2 = this._m;
			int num3 = this._n;
			while (regexNode.ChildCount() != 0)
			{
				RegexNode regexNode2 = regexNode.Child(0);
				if (regexNode2.Type() != num)
				{
					int num4 = regexNode2.Type();
					if ((num4 < 3 || num4 > 5 || num != 26) && (num4 < 6 || num4 > 8 || num != 27))
					{
						break;
					}
				}
				if ((regexNode._m == 0 && regexNode2._m > 1) || regexNode2._n < regexNode2._m * 2)
				{
					break;
				}
				regexNode = regexNode2;
				if (regexNode._m > 0)
				{
					num2 = (regexNode._m = ((2147483646 / regexNode._m < num2) ? int.MaxValue : (regexNode._m * num2)));
				}
				if (regexNode._n > 0)
				{
					num3 = (regexNode._n = ((2147483646 / regexNode._n < num3) ? int.MaxValue : (regexNode._n * num3)));
				}
			}
			if (num2 != 2147483647)
			{
				return regexNode;
			}
			return new RegexNode(22, this._options);
		}

		// Token: 0x060005F3 RID: 1523 RVA: 0x0001DFFC File Offset: 0x0001C1FC
		internal RegexNode ReduceSet()
		{
			if (RegexCharClass.IsEmpty(this._str))
			{
				this._type = 22;
				this._str = null;
			}
			else if (RegexCharClass.IsSingleton(this._str))
			{
				this._ch = RegexCharClass.SingletonChar(this._str);
				this._str = null;
				this._type += -2;
			}
			else if (RegexCharClass.IsSingletonInverse(this._str))
			{
				this._ch = RegexCharClass.SingletonChar(this._str);
				this._str = null;
				this._type += -1;
			}
			return this;
		}

		// Token: 0x060005F4 RID: 1524 RVA: 0x0001E094 File Offset: 0x0001C294
		internal RegexNode ReduceAlternation()
		{
			if (this._children == null)
			{
				return new RegexNode(22, this._options);
			}
			bool flag = false;
			bool flag2 = false;
			RegexOptions regexOptions = RegexOptions.None;
			int i = 0;
			int num = 0;
			while (i < this._children.Count)
			{
				RegexNode regexNode = this._children[i];
				if (num < i)
				{
					this._children[num] = regexNode;
				}
				if (regexNode._type == 24)
				{
					for (int j = 0; j < regexNode._children.Count; j++)
					{
						regexNode._children[j]._next = this;
					}
					this._children.InsertRange(i + 1, regexNode._children);
					num--;
				}
				else if (regexNode._type == 11 || regexNode._type == 9)
				{
					RegexOptions regexOptions2 = regexNode._options & (RegexOptions.IgnoreCase | RegexOptions.RightToLeft);
					if (regexNode._type == 11)
					{
						if (!flag || regexOptions != regexOptions2 || flag2 || !RegexCharClass.IsMergeable(regexNode._str))
						{
							flag = true;
							flag2 = !RegexCharClass.IsMergeable(regexNode._str);
							regexOptions = regexOptions2;
							goto IL_1D0;
						}
					}
					else if (!flag || regexOptions != regexOptions2 || flag2)
					{
						flag = true;
						flag2 = false;
						regexOptions = regexOptions2;
						goto IL_1D0;
					}
					num--;
					RegexNode regexNode2 = this._children[num];
					RegexCharClass regexCharClass;
					if (regexNode2._type == 9)
					{
						regexCharClass = new RegexCharClass();
						regexCharClass.AddChar(regexNode2._ch);
					}
					else
					{
						regexCharClass = RegexCharClass.Parse(regexNode2._str);
					}
					if (regexNode._type == 9)
					{
						regexCharClass.AddChar(regexNode._ch);
					}
					else
					{
						RegexCharClass cc = RegexCharClass.Parse(regexNode._str);
						regexCharClass.AddCharClass(cc);
					}
					regexNode2._type = 11;
					regexNode2._str = regexCharClass.ToStringClass();
				}
				else if (regexNode._type == 22)
				{
					num--;
				}
				else
				{
					flag = false;
					flag2 = false;
				}
				IL_1D0:
				i++;
				num++;
			}
			if (num < i)
			{
				this._children.RemoveRange(num, i - num);
			}
			return this.StripEnation(22);
		}

		// Token: 0x060005F5 RID: 1525 RVA: 0x0001E2B0 File Offset: 0x0001C4B0
		internal RegexNode ReduceConcatenation()
		{
			if (this._children == null)
			{
				return new RegexNode(23, this._options);
			}
			bool flag = false;
			RegexOptions regexOptions = RegexOptions.None;
			int i = 0;
			int num = 0;
			while (i < this._children.Count)
			{
				RegexNode regexNode = this._children[i];
				if (num < i)
				{
					this._children[num] = regexNode;
				}
				if (regexNode._type == 25 && (regexNode._options & RegexOptions.RightToLeft) == (this._options & RegexOptions.RightToLeft))
				{
					for (int j = 0; j < regexNode._children.Count; j++)
					{
						regexNode._children[j]._next = this;
					}
					this._children.InsertRange(i + 1, regexNode._children);
					num--;
				}
				else if (regexNode._type == 12 || regexNode._type == 9)
				{
					RegexOptions regexOptions2 = regexNode._options & (RegexOptions.IgnoreCase | RegexOptions.RightToLeft);
					if (!flag || regexOptions != regexOptions2)
					{
						flag = true;
						regexOptions = regexOptions2;
					}
					else
					{
						RegexNode regexNode2 = this._children[--num];
						if (regexNode2._type == 9)
						{
							regexNode2._type = 12;
							regexNode2._str = Convert.ToString(regexNode2._ch, CultureInfo.InvariantCulture);
						}
						if ((regexOptions2 & RegexOptions.RightToLeft) == RegexOptions.None)
						{
							if (regexNode._type == 9)
							{
								RegexNode regexNode3 = regexNode2;
								regexNode3._str += regexNode._ch.ToString();
							}
							else
							{
								RegexNode regexNode4 = regexNode2;
								regexNode4._str += regexNode._str;
							}
						}
						else if (regexNode._type == 9)
						{
							regexNode2._str = regexNode._ch.ToString() + regexNode2._str;
						}
						else
						{
							regexNode2._str = regexNode._str + regexNode2._str;
						}
					}
				}
				else if (regexNode._type == 23)
				{
					num--;
				}
				else
				{
					flag = false;
				}
				i++;
				num++;
			}
			if (num < i)
			{
				this._children.RemoveRange(num, i - num);
			}
			return this.StripEnation(23);
		}

		// Token: 0x060005F6 RID: 1526 RVA: 0x0001E4C8 File Offset: 0x0001C6C8
		internal RegexNode MakeQuantifier(bool lazy, int min, int max)
		{
			if (min == 0 && max == 0)
			{
				return new RegexNode(23, this._options);
			}
			if (min == 1 && max == 1)
			{
				return this;
			}
			int type = this._type;
			if (type - 9 <= 2)
			{
				this.MakeRep(lazy ? 6 : 3, min, max);
				return this;
			}
			RegexNode regexNode = new RegexNode(lazy ? 27 : 26, this._options, min, max);
			regexNode.AddChild(this);
			return regexNode;
		}

		// Token: 0x060005F7 RID: 1527 RVA: 0x0001E530 File Offset: 0x0001C730
		internal void AddChild(RegexNode newChild)
		{
			if (this._children == null)
			{
				this._children = new List<RegexNode>(4);
			}
			RegexNode regexNode = newChild.Reduce();
			this._children.Add(regexNode);
			regexNode._next = this;
		}

		// Token: 0x060005F8 RID: 1528 RVA: 0x0001E56B File Offset: 0x0001C76B
		internal RegexNode Child(int i)
		{
			return this._children[i];
		}

		// Token: 0x060005F9 RID: 1529 RVA: 0x0001E579 File Offset: 0x0001C779
		internal int ChildCount()
		{
			if (this._children != null)
			{
				return this._children.Count;
			}
			return 0;
		}

		// Token: 0x060005FA RID: 1530 RVA: 0x0001E590 File Offset: 0x0001C790
		internal int Type()
		{
			return this._type;
		}

		// Token: 0x04000A85 RID: 2693
		internal const int Oneloop = 3;

		// Token: 0x04000A86 RID: 2694
		internal const int Notoneloop = 4;

		// Token: 0x04000A87 RID: 2695
		internal const int Setloop = 5;

		// Token: 0x04000A88 RID: 2696
		internal const int Onelazy = 6;

		// Token: 0x04000A89 RID: 2697
		internal const int Notonelazy = 7;

		// Token: 0x04000A8A RID: 2698
		internal const int Setlazy = 8;

		// Token: 0x04000A8B RID: 2699
		internal const int One = 9;

		// Token: 0x04000A8C RID: 2700
		internal const int Notone = 10;

		// Token: 0x04000A8D RID: 2701
		internal const int Set = 11;

		// Token: 0x04000A8E RID: 2702
		internal const int Multi = 12;

		// Token: 0x04000A8F RID: 2703
		internal const int Ref = 13;

		// Token: 0x04000A90 RID: 2704
		internal const int Bol = 14;

		// Token: 0x04000A91 RID: 2705
		internal const int Eol = 15;

		// Token: 0x04000A92 RID: 2706
		internal const int Boundary = 16;

		// Token: 0x04000A93 RID: 2707
		internal const int Nonboundary = 17;

		// Token: 0x04000A94 RID: 2708
		internal const int ECMABoundary = 41;

		// Token: 0x04000A95 RID: 2709
		internal const int NonECMABoundary = 42;

		// Token: 0x04000A96 RID: 2710
		internal const int Beginning = 18;

		// Token: 0x04000A97 RID: 2711
		internal const int Start = 19;

		// Token: 0x04000A98 RID: 2712
		internal const int EndZ = 20;

		// Token: 0x04000A99 RID: 2713
		internal const int End = 21;

		// Token: 0x04000A9A RID: 2714
		internal const int Nothing = 22;

		// Token: 0x04000A9B RID: 2715
		internal const int Empty = 23;

		// Token: 0x04000A9C RID: 2716
		internal const int Alternate = 24;

		// Token: 0x04000A9D RID: 2717
		internal const int Concatenate = 25;

		// Token: 0x04000A9E RID: 2718
		internal const int Loop = 26;

		// Token: 0x04000A9F RID: 2719
		internal const int Lazyloop = 27;

		// Token: 0x04000AA0 RID: 2720
		internal const int Capture = 28;

		// Token: 0x04000AA1 RID: 2721
		internal const int Group = 29;

		// Token: 0x04000AA2 RID: 2722
		internal const int Require = 30;

		// Token: 0x04000AA3 RID: 2723
		internal const int Prevent = 31;

		// Token: 0x04000AA4 RID: 2724
		internal const int Greedy = 32;

		// Token: 0x04000AA5 RID: 2725
		internal const int Testref = 33;

		// Token: 0x04000AA6 RID: 2726
		internal const int Testgroup = 34;

		// Token: 0x04000AA7 RID: 2727
		internal int _type;

		// Token: 0x04000AA8 RID: 2728
		internal List<RegexNode> _children;

		// Token: 0x04000AA9 RID: 2729
		internal string _str;

		// Token: 0x04000AAA RID: 2730
		internal char _ch;

		// Token: 0x04000AAB RID: 2731
		internal int _m;

		// Token: 0x04000AAC RID: 2732
		internal int _n;

		// Token: 0x04000AAD RID: 2733
		internal RegexOptions _options;

		// Token: 0x04000AAE RID: 2734
		internal RegexNode _next;
	}
}
