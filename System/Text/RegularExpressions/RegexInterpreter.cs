﻿using System;
using System.Globalization;

namespace System.Text.RegularExpressions
{
	// Token: 0x020000DC RID: 220
	internal sealed class RegexInterpreter : RegexRunner
	{
		// Token: 0x06000590 RID: 1424 RVA: 0x0001BBD8 File Offset: 0x00019DD8
		internal RegexInterpreter(RegexCode code, CultureInfo culture)
		{
			this.runcode = code;
			this.runcodes = code._codes;
			this.runstrings = code._strings;
			this.runfcPrefix = code._fcPrefix;
			this.runbmPrefix = code._bmPrefix;
			this.runanchors = code._anchors;
			this.runculture = culture;
		}

		// Token: 0x06000591 RID: 1425 RVA: 0x0001BC35 File Offset: 0x00019E35
		protected override void InitTrackCount()
		{
			this.runtrackcount = this.runcode._trackcount;
		}

		// Token: 0x06000592 RID: 1426 RVA: 0x0001BC48 File Offset: 0x00019E48
		private void Advance()
		{
			this.Advance(0);
		}

		// Token: 0x06000593 RID: 1427 RVA: 0x0001BC51 File Offset: 0x00019E51
		private void Advance(int i)
		{
			this.runcodepos += i + 1;
			this.SetOperator(this.runcodes[this.runcodepos]);
		}

		// Token: 0x06000594 RID: 1428 RVA: 0x0001BC76 File Offset: 0x00019E76
		private void Goto(int newpos)
		{
			if (newpos < this.runcodepos)
			{
				base.EnsureStorage();
			}
			this.SetOperator(this.runcodes[newpos]);
			this.runcodepos = newpos;
		}

		// Token: 0x06000595 RID: 1429 RVA: 0x0001BC9C File Offset: 0x00019E9C
		private void Textto(int newpos)
		{
			this.runtextpos = newpos;
		}

		// Token: 0x06000596 RID: 1430 RVA: 0x0001BCA5 File Offset: 0x00019EA5
		private void Trackto(int newpos)
		{
			this.runtrackpos = this.runtrack.Length - newpos;
		}

		// Token: 0x06000597 RID: 1431 RVA: 0x0001BCB7 File Offset: 0x00019EB7
		private int Textstart()
		{
			return this.runtextstart;
		}

		// Token: 0x06000598 RID: 1432 RVA: 0x0001BCBF File Offset: 0x00019EBF
		private int Textpos()
		{
			return this.runtextpos;
		}

		// Token: 0x06000599 RID: 1433 RVA: 0x0001BCC7 File Offset: 0x00019EC7
		private int Trackpos()
		{
			return this.runtrack.Length - this.runtrackpos;
		}

		// Token: 0x0600059A RID: 1434 RVA: 0x0001BCD8 File Offset: 0x00019ED8
		private void TrackPush()
		{
			int[] runtrack = this.runtrack;
			int num = this.runtrackpos - 1;
			this.runtrackpos = num;
			runtrack[num] = this.runcodepos;
		}

		// Token: 0x0600059B RID: 1435 RVA: 0x0001BD04 File Offset: 0x00019F04
		private void TrackPush(int I1)
		{
			int[] runtrack = this.runtrack;
			int num = this.runtrackpos - 1;
			this.runtrackpos = num;
			runtrack[num] = I1;
			int[] runtrack2 = this.runtrack;
			num = this.runtrackpos - 1;
			this.runtrackpos = num;
			runtrack2[num] = this.runcodepos;
		}

		// Token: 0x0600059C RID: 1436 RVA: 0x0001BD48 File Offset: 0x00019F48
		private void TrackPush(int I1, int I2)
		{
			int[] runtrack = this.runtrack;
			int num = this.runtrackpos - 1;
			this.runtrackpos = num;
			runtrack[num] = I1;
			int[] runtrack2 = this.runtrack;
			num = this.runtrackpos - 1;
			this.runtrackpos = num;
			runtrack2[num] = I2;
			int[] runtrack3 = this.runtrack;
			num = this.runtrackpos - 1;
			this.runtrackpos = num;
			runtrack3[num] = this.runcodepos;
		}

		// Token: 0x0600059D RID: 1437 RVA: 0x0001BDA8 File Offset: 0x00019FA8
		private void TrackPush(int I1, int I2, int I3)
		{
			int[] runtrack = this.runtrack;
			int num = this.runtrackpos - 1;
			this.runtrackpos = num;
			runtrack[num] = I1;
			int[] runtrack2 = this.runtrack;
			num = this.runtrackpos - 1;
			this.runtrackpos = num;
			runtrack2[num] = I2;
			int[] runtrack3 = this.runtrack;
			num = this.runtrackpos - 1;
			this.runtrackpos = num;
			runtrack3[num] = I3;
			int[] runtrack4 = this.runtrack;
			num = this.runtrackpos - 1;
			this.runtrackpos = num;
			runtrack4[num] = this.runcodepos;
		}

		// Token: 0x0600059E RID: 1438 RVA: 0x0001BE20 File Offset: 0x0001A020
		private void TrackPush2(int I1)
		{
			int[] runtrack = this.runtrack;
			int num = this.runtrackpos - 1;
			this.runtrackpos = num;
			runtrack[num] = I1;
			int[] runtrack2 = this.runtrack;
			num = this.runtrackpos - 1;
			this.runtrackpos = num;
			runtrack2[num] = -this.runcodepos;
		}

		// Token: 0x0600059F RID: 1439 RVA: 0x0001BE68 File Offset: 0x0001A068
		private void TrackPush2(int I1, int I2)
		{
			int[] runtrack = this.runtrack;
			int num = this.runtrackpos - 1;
			this.runtrackpos = num;
			runtrack[num] = I1;
			int[] runtrack2 = this.runtrack;
			num = this.runtrackpos - 1;
			this.runtrackpos = num;
			runtrack2[num] = I2;
			int[] runtrack3 = this.runtrack;
			num = this.runtrackpos - 1;
			this.runtrackpos = num;
			runtrack3[num] = -this.runcodepos;
		}

		// Token: 0x060005A0 RID: 1440 RVA: 0x0001BEC8 File Offset: 0x0001A0C8
		private void Backtrack()
		{
			int[] runtrack = this.runtrack;
			int runtrackpos = this.runtrackpos;
			this.runtrackpos = runtrackpos + 1;
			int num = runtrack[runtrackpos];
			if (num < 0)
			{
				num = -num;
				this.SetOperator(this.runcodes[num] | 256);
			}
			else
			{
				this.SetOperator(this.runcodes[num] | 128);
			}
			if (num < this.runcodepos)
			{
				base.EnsureStorage();
			}
			this.runcodepos = num;
		}

		// Token: 0x060005A1 RID: 1441 RVA: 0x0001BF35 File Offset: 0x0001A135
		private void SetOperator(int op)
		{
			this.runci = ((op & 512) != 0);
			this.runrtl = ((op & 64) != 0);
			this.runoperator = (op & -577);
		}

		// Token: 0x060005A2 RID: 1442 RVA: 0x0001BF61 File Offset: 0x0001A161
		private void TrackPop()
		{
			this.runtrackpos++;
		}

		// Token: 0x060005A3 RID: 1443 RVA: 0x0001BF71 File Offset: 0x0001A171
		private void TrackPop(int framesize)
		{
			this.runtrackpos += framesize;
		}

		// Token: 0x060005A4 RID: 1444 RVA: 0x0001BF81 File Offset: 0x0001A181
		private int TrackPeek()
		{
			return this.runtrack[this.runtrackpos - 1];
		}

		// Token: 0x060005A5 RID: 1445 RVA: 0x0001BF92 File Offset: 0x0001A192
		private int TrackPeek(int i)
		{
			return this.runtrack[this.runtrackpos - i - 1];
		}

		// Token: 0x060005A6 RID: 1446 RVA: 0x0001BFA8 File Offset: 0x0001A1A8
		private void StackPush(int I1)
		{
			int[] runstack = this.runstack;
			int num = this.runstackpos - 1;
			this.runstackpos = num;
			runstack[num] = I1;
		}

		// Token: 0x060005A7 RID: 1447 RVA: 0x0001BFD0 File Offset: 0x0001A1D0
		private void StackPush(int I1, int I2)
		{
			int[] runstack = this.runstack;
			int num = this.runstackpos - 1;
			this.runstackpos = num;
			runstack[num] = I1;
			int[] runstack2 = this.runstack;
			num = this.runstackpos - 1;
			this.runstackpos = num;
			runstack2[num] = I2;
		}

		// Token: 0x060005A8 RID: 1448 RVA: 0x0001C00F File Offset: 0x0001A20F
		private void StackPop()
		{
			this.runstackpos++;
		}

		// Token: 0x060005A9 RID: 1449 RVA: 0x0001C01F File Offset: 0x0001A21F
		private void StackPop(int framesize)
		{
			this.runstackpos += framesize;
		}

		// Token: 0x060005AA RID: 1450 RVA: 0x0001C02F File Offset: 0x0001A22F
		private int StackPeek()
		{
			return this.runstack[this.runstackpos - 1];
		}

		// Token: 0x060005AB RID: 1451 RVA: 0x0001C040 File Offset: 0x0001A240
		private int StackPeek(int i)
		{
			return this.runstack[this.runstackpos - i - 1];
		}

		// Token: 0x060005AC RID: 1452 RVA: 0x0001C053 File Offset: 0x0001A253
		private int Operator()
		{
			return this.runoperator;
		}

		// Token: 0x060005AD RID: 1453 RVA: 0x0001C05B File Offset: 0x0001A25B
		private int Operand(int i)
		{
			return this.runcodes[this.runcodepos + i + 1];
		}

		// Token: 0x060005AE RID: 1454 RVA: 0x0001C06E File Offset: 0x0001A26E
		private int Leftchars()
		{
			return this.runtextpos - this.runtextbeg;
		}

		// Token: 0x060005AF RID: 1455 RVA: 0x0001C07D File Offset: 0x0001A27D
		private int Rightchars()
		{
			return this.runtextend - this.runtextpos;
		}

		// Token: 0x060005B0 RID: 1456 RVA: 0x0001C08C File Offset: 0x0001A28C
		private int Bump()
		{
			if (!this.runrtl)
			{
				return 1;
			}
			return -1;
		}

		// Token: 0x060005B1 RID: 1457 RVA: 0x0001C099 File Offset: 0x0001A299
		private int Forwardchars()
		{
			if (!this.runrtl)
			{
				return this.runtextend - this.runtextpos;
			}
			return this.runtextpos - this.runtextbeg;
		}

		// Token: 0x060005B2 RID: 1458 RVA: 0x0001C0C0 File Offset: 0x0001A2C0
		private char Forwardcharnext()
		{
			char c;
			if (!this.runrtl)
			{
				string runtext = this.runtext;
				int num = this.runtextpos;
				this.runtextpos = num + 1;
				c = runtext[num];
			}
			else
			{
				string runtext2 = this.runtext;
				int num = this.runtextpos - 1;
				this.runtextpos = num;
				c = runtext2[num];
			}
			char c2 = c;
			if (!this.runci)
			{
				return c2;
			}
			return char.ToLower(c2, this.runculture);
		}

		// Token: 0x060005B3 RID: 1459 RVA: 0x0001C128 File Offset: 0x0001A328
		private bool Stringmatch(string str)
		{
			int num;
			int num2;
			if (!this.runrtl)
			{
				if (this.runtextend - this.runtextpos < (num = str.Length))
				{
					return false;
				}
				num2 = this.runtextpos + num;
			}
			else
			{
				if (this.runtextpos - this.runtextbeg < (num = str.Length))
				{
					return false;
				}
				num2 = this.runtextpos;
			}
			if (!this.runci)
			{
				while (num != 0)
				{
					if (str[--num] != this.runtext[--num2])
					{
						return false;
					}
				}
			}
			else
			{
				while (num != 0)
				{
					if (str[--num] != char.ToLower(this.runtext[--num2], this.runculture))
					{
						return false;
					}
				}
			}
			if (!this.runrtl)
			{
				num2 += str.Length;
			}
			this.runtextpos = num2;
			return true;
		}

		// Token: 0x060005B4 RID: 1460 RVA: 0x0001C1F8 File Offset: 0x0001A3F8
		private bool Refmatch(int index, int len)
		{
			int num;
			if (!this.runrtl)
			{
				if (this.runtextend - this.runtextpos < len)
				{
					return false;
				}
				num = this.runtextpos + len;
			}
			else
			{
				if (this.runtextpos - this.runtextbeg < len)
				{
					return false;
				}
				num = this.runtextpos;
			}
			int num2 = index + len;
			int num3 = len;
			if (!this.runci)
			{
				while (num3-- != 0)
				{
					if (this.runtext[--num2] != this.runtext[--num])
					{
						return false;
					}
				}
			}
			else
			{
				while (num3-- != 0)
				{
					if (char.ToLower(this.runtext[--num2], this.runculture) != char.ToLower(this.runtext[--num], this.runculture))
					{
						return false;
					}
				}
			}
			if (!this.runrtl)
			{
				num += len;
			}
			this.runtextpos = num;
			return true;
		}

		// Token: 0x060005B5 RID: 1461 RVA: 0x0001C2D5 File Offset: 0x0001A4D5
		private void Backwardnext()
		{
			this.runtextpos += (this.runrtl ? 1 : -1);
		}

		// Token: 0x060005B6 RID: 1462 RVA: 0x0001C2F0 File Offset: 0x0001A4F0
		private char CharAt(int j)
		{
			return this.runtext[j];
		}

		// Token: 0x060005B7 RID: 1463 RVA: 0x0001C300 File Offset: 0x0001A500
		protected override bool FindFirstChar()
		{
			if ((this.runanchors & 53) != 0)
			{
				if (!this.runcode._rightToLeft)
				{
					if (((this.runanchors & 1) != 0 && this.runtextpos > this.runtextbeg) || ((this.runanchors & 4) != 0 && this.runtextpos > this.runtextstart))
					{
						this.runtextpos = this.runtextend;
						return false;
					}
					if ((this.runanchors & 16) != 0 && this.runtextpos < this.runtextend - 1)
					{
						this.runtextpos = this.runtextend - 1;
					}
					else if ((this.runanchors & 32) != 0 && this.runtextpos < this.runtextend)
					{
						this.runtextpos = this.runtextend;
					}
				}
				else
				{
					if (((this.runanchors & 32) != 0 && this.runtextpos < this.runtextend) || ((this.runanchors & 16) != 0 && (this.runtextpos < this.runtextend - 1 || (this.runtextpos == this.runtextend - 1 && this.CharAt(this.runtextpos) != '\n'))) || ((this.runanchors & 4) != 0 && this.runtextpos < this.runtextstart))
					{
						this.runtextpos = this.runtextbeg;
						return false;
					}
					if ((this.runanchors & 1) != 0 && this.runtextpos > this.runtextbeg)
					{
						this.runtextpos = this.runtextbeg;
					}
				}
				return this.runbmPrefix == null || this.runbmPrefix.IsMatch(this.runtext, this.runtextpos, this.runtextbeg, this.runtextend);
			}
			if (this.runbmPrefix != null)
			{
				this.runtextpos = this.runbmPrefix.Scan(this.runtext, this.runtextpos, this.runtextbeg, this.runtextend);
				if (this.runtextpos == -1)
				{
					this.runtextpos = (this.runcode._rightToLeft ? this.runtextbeg : this.runtextend);
					return false;
				}
				return true;
			}
			else
			{
				if (this.runfcPrefix == null)
				{
					return true;
				}
				this.runrtl = this.runcode._rightToLeft;
				this.runci = this.runfcPrefix.CaseInsensitive;
				string prefix = this.runfcPrefix.Prefix;
				if (RegexCharClass.IsSingleton(prefix))
				{
					char c = RegexCharClass.SingletonChar(prefix);
					for (int i = this.Forwardchars(); i > 0; i--)
					{
						if (c == this.Forwardcharnext())
						{
							this.Backwardnext();
							return true;
						}
					}
				}
				else
				{
					for (int i = this.Forwardchars(); i > 0; i--)
					{
						if (RegexCharClass.CharInClass(this.Forwardcharnext(), prefix))
						{
							this.Backwardnext();
							return true;
						}
					}
				}
				return false;
			}
		}

		// Token: 0x060005B8 RID: 1464 RVA: 0x0001C588 File Offset: 0x0001A788
		protected override void Go()
		{
			this.Goto(0);
			for (;;)
			{
				base.CheckTimeout();
				int num = this.Operator();
				switch (num)
				{
				case 0:
				{
					int num2 = this.Operand(1);
					if (this.Forwardchars() >= num2)
					{
						char c = (char)this.Operand(0);
						while (num2-- > 0)
						{
							if (this.Forwardcharnext() != c)
							{
								goto IL_E4E;
							}
						}
						this.Advance(2);
						continue;
					}
					break;
				}
				case 1:
				{
					int num3 = this.Operand(1);
					if (this.Forwardchars() >= num3)
					{
						char c2 = (char)this.Operand(0);
						while (num3-- > 0)
						{
							if (this.Forwardcharnext() == c2)
							{
								goto IL_E4E;
							}
						}
						this.Advance(2);
						continue;
					}
					break;
				}
				case 2:
				{
					int num4 = this.Operand(1);
					if (this.Forwardchars() >= num4)
					{
						string set = this.runstrings[this.Operand(0)];
						while (num4-- > 0)
						{
							if (!RegexCharClass.CharInClass(this.Forwardcharnext(), set))
							{
								goto IL_E4E;
							}
						}
						this.Advance(2);
						continue;
					}
					break;
				}
				case 3:
				{
					int num5 = this.Operand(1);
					if (num5 > this.Forwardchars())
					{
						num5 = this.Forwardchars();
					}
					char c3 = (char)this.Operand(0);
					int i;
					for (i = num5; i > 0; i--)
					{
						if (this.Forwardcharnext() != c3)
						{
							this.Backwardnext();
							break;
						}
					}
					if (num5 > i)
					{
						this.TrackPush(num5 - i - 1, this.Textpos() - this.Bump());
					}
					this.Advance(2);
					continue;
				}
				case 4:
				{
					int num6 = this.Operand(1);
					if (num6 > this.Forwardchars())
					{
						num6 = this.Forwardchars();
					}
					char c4 = (char)this.Operand(0);
					int j;
					for (j = num6; j > 0; j--)
					{
						if (this.Forwardcharnext() == c4)
						{
							this.Backwardnext();
							break;
						}
					}
					if (num6 > j)
					{
						this.TrackPush(num6 - j - 1, this.Textpos() - this.Bump());
					}
					this.Advance(2);
					continue;
				}
				case 5:
				{
					int num7 = this.Operand(1);
					if (num7 > this.Forwardchars())
					{
						num7 = this.Forwardchars();
					}
					string set2 = this.runstrings[this.Operand(0)];
					int k;
					for (k = num7; k > 0; k--)
					{
						if (!RegexCharClass.CharInClass(this.Forwardcharnext(), set2))
						{
							this.Backwardnext();
							break;
						}
					}
					if (num7 > k)
					{
						this.TrackPush(num7 - k - 1, this.Textpos() - this.Bump());
					}
					this.Advance(2);
					continue;
				}
				case 6:
				case 7:
				{
					int num8 = this.Operand(1);
					if (num8 > this.Forwardchars())
					{
						num8 = this.Forwardchars();
					}
					if (num8 > 0)
					{
						this.TrackPush(num8 - 1, this.Textpos());
					}
					this.Advance(2);
					continue;
				}
				case 8:
				{
					int num9 = this.Operand(1);
					if (num9 > this.Forwardchars())
					{
						num9 = this.Forwardchars();
					}
					if (num9 > 0)
					{
						this.TrackPush(num9 - 1, this.Textpos());
					}
					this.Advance(2);
					continue;
				}
				case 9:
					if (this.Forwardchars() >= 1 && this.Forwardcharnext() == (char)this.Operand(0))
					{
						this.Advance(1);
						continue;
					}
					break;
				case 10:
					if (this.Forwardchars() >= 1 && this.Forwardcharnext() != (char)this.Operand(0))
					{
						this.Advance(1);
						continue;
					}
					break;
				case 11:
					if (this.Forwardchars() >= 1 && RegexCharClass.CharInClass(this.Forwardcharnext(), this.runstrings[this.Operand(0)]))
					{
						this.Advance(1);
						continue;
					}
					break;
				case 12:
					if (this.Stringmatch(this.runstrings[this.Operand(0)]))
					{
						this.Advance(1);
						continue;
					}
					break;
				case 13:
				{
					int cap = this.Operand(0);
					if (base.IsMatched(cap))
					{
						if (!this.Refmatch(base.MatchIndex(cap), base.MatchLength(cap)))
						{
							break;
						}
					}
					else if ((this.runregex.roptions & RegexOptions.ECMAScript) == RegexOptions.None)
					{
						break;
					}
					this.Advance(1);
					continue;
				}
				case 14:
					if (this.Leftchars() <= 0 || this.CharAt(this.Textpos() - 1) == '\n')
					{
						this.Advance();
						continue;
					}
					break;
				case 15:
					if (this.Rightchars() <= 0 || this.CharAt(this.Textpos()) == '\n')
					{
						this.Advance();
						continue;
					}
					break;
				case 16:
					if (base.IsBoundary(this.Textpos(), this.runtextbeg, this.runtextend))
					{
						this.Advance();
						continue;
					}
					break;
				case 17:
					if (!base.IsBoundary(this.Textpos(), this.runtextbeg, this.runtextend))
					{
						this.Advance();
						continue;
					}
					break;
				case 18:
					if (this.Leftchars() <= 0)
					{
						this.Advance();
						continue;
					}
					break;
				case 19:
					if (this.Textpos() == this.Textstart())
					{
						this.Advance();
						continue;
					}
					break;
				case 20:
					if (this.Rightchars() <= 1 && (this.Rightchars() != 1 || this.CharAt(this.Textpos()) == '\n'))
					{
						this.Advance();
						continue;
					}
					break;
				case 21:
					if (this.Rightchars() <= 0)
					{
						this.Advance();
						continue;
					}
					break;
				case 22:
					break;
				case 23:
					this.TrackPush(this.Textpos());
					this.Advance(1);
					continue;
				case 24:
					this.StackPop();
					if (this.Textpos() - this.StackPeek() != 0)
					{
						this.TrackPush(this.StackPeek(), this.Textpos());
						this.StackPush(this.Textpos());
						this.Goto(this.Operand(0));
						continue;
					}
					this.TrackPush2(this.StackPeek());
					this.Advance(1);
					continue;
				case 25:
				{
					this.StackPop();
					int num10 = this.StackPeek();
					if (this.Textpos() != num10)
					{
						if (num10 != -1)
						{
							this.TrackPush(num10, this.Textpos());
						}
						else
						{
							this.TrackPush(this.Textpos(), this.Textpos());
						}
					}
					else
					{
						this.StackPush(num10);
						this.TrackPush2(this.StackPeek());
					}
					this.Advance(1);
					continue;
				}
				case 26:
					this.StackPush(-1, this.Operand(0));
					this.TrackPush();
					this.Advance(1);
					continue;
				case 27:
					this.StackPush(this.Textpos(), this.Operand(0));
					this.TrackPush();
					this.Advance(1);
					continue;
				case 28:
				{
					this.StackPop(2);
					int num11 = this.StackPeek();
					int num12 = this.StackPeek(1);
					int num13 = this.Textpos() - num11;
					if (num12 >= this.Operand(1) || (num13 == 0 && num12 >= 0))
					{
						this.TrackPush2(num11, num12);
						this.Advance(2);
						continue;
					}
					this.TrackPush(num11);
					this.StackPush(this.Textpos(), num12 + 1);
					this.Goto(this.Operand(0));
					continue;
				}
				case 29:
				{
					this.StackPop(2);
					int i2 = this.StackPeek();
					int num14 = this.StackPeek(1);
					if (num14 < 0)
					{
						this.TrackPush2(i2);
						this.StackPush(this.Textpos(), num14 + 1);
						this.Goto(this.Operand(0));
						continue;
					}
					this.TrackPush(i2, num14, this.Textpos());
					this.Advance(2);
					continue;
				}
				case 30:
					this.StackPush(-1);
					this.TrackPush();
					this.Advance();
					continue;
				case 31:
					this.StackPush(this.Textpos());
					this.TrackPush();
					this.Advance();
					continue;
				case 32:
					if (this.Operand(1) == -1 || base.IsMatched(this.Operand(1)))
					{
						this.StackPop();
						if (this.Operand(1) != -1)
						{
							base.TransferCapture(this.Operand(0), this.Operand(1), this.StackPeek(), this.Textpos());
						}
						else
						{
							base.Capture(this.Operand(0), this.StackPeek(), this.Textpos());
						}
						this.TrackPush(this.StackPeek());
						this.Advance(2);
						continue;
					}
					break;
				case 33:
					this.StackPop();
					this.TrackPush(this.StackPeek());
					this.Textto(this.StackPeek());
					this.Advance();
					continue;
				case 34:
					this.StackPush(this.Trackpos(), base.Crawlpos());
					this.TrackPush();
					this.Advance();
					continue;
				case 35:
					this.StackPop(2);
					this.Trackto(this.StackPeek());
					while (base.Crawlpos() != this.StackPeek(1))
					{
						base.Uncapture();
					}
					break;
				case 36:
					this.StackPop(2);
					this.Trackto(this.StackPeek());
					this.TrackPush(this.StackPeek(1));
					this.Advance();
					continue;
				case 37:
					if (base.IsMatched(this.Operand(0)))
					{
						this.Advance(1);
						continue;
					}
					break;
				case 38:
					this.Goto(this.Operand(0));
					continue;
				case 39:
					goto IL_E3E;
				case 40:
					return;
				case 41:
					if (base.IsECMABoundary(this.Textpos(), this.runtextbeg, this.runtextend))
					{
						this.Advance();
						continue;
					}
					break;
				case 42:
					if (!base.IsECMABoundary(this.Textpos(), this.runtextbeg, this.runtextend))
					{
						this.Advance();
						continue;
					}
					break;
				default:
					switch (num)
					{
					case 131:
					case 132:
					{
						this.TrackPop(2);
						int num15 = this.TrackPeek();
						int num16 = this.TrackPeek(1);
						this.Textto(num16);
						if (num15 > 0)
						{
							this.TrackPush(num15 - 1, num16 - this.Bump());
						}
						this.Advance(2);
						continue;
					}
					case 133:
					{
						this.TrackPop(2);
						int num17 = this.TrackPeek();
						int num18 = this.TrackPeek(1);
						this.Textto(num18);
						if (num17 > 0)
						{
							this.TrackPush(num17 - 1, num18 - this.Bump());
						}
						this.Advance(2);
						continue;
					}
					case 134:
					{
						this.TrackPop(2);
						int num19 = this.TrackPeek(1);
						this.Textto(num19);
						if (this.Forwardcharnext() == (char)this.Operand(0))
						{
							int num20 = this.TrackPeek();
							if (num20 > 0)
							{
								this.TrackPush(num20 - 1, num19 + this.Bump());
							}
							this.Advance(2);
							continue;
						}
						break;
					}
					case 135:
					{
						this.TrackPop(2);
						int num21 = this.TrackPeek(1);
						this.Textto(num21);
						if (this.Forwardcharnext() != (char)this.Operand(0))
						{
							int num22 = this.TrackPeek();
							if (num22 > 0)
							{
								this.TrackPush(num22 - 1, num21 + this.Bump());
							}
							this.Advance(2);
							continue;
						}
						break;
					}
					case 136:
					{
						this.TrackPop(2);
						int num23 = this.TrackPeek(1);
						this.Textto(num23);
						if (RegexCharClass.CharInClass(this.Forwardcharnext(), this.runstrings[this.Operand(0)]))
						{
							int num24 = this.TrackPeek();
							if (num24 > 0)
							{
								this.TrackPush(num24 - 1, num23 + this.Bump());
							}
							this.Advance(2);
							continue;
						}
						break;
					}
					case 137:
					case 138:
					case 139:
					case 140:
					case 141:
					case 142:
					case 143:
					case 144:
					case 145:
					case 146:
					case 147:
					case 148:
					case 149:
					case 150:
					case 163:
						goto IL_E3E;
					case 151:
						this.TrackPop();
						this.Textto(this.TrackPeek());
						this.Goto(this.Operand(0));
						continue;
					case 152:
						this.TrackPop(2);
						this.StackPop();
						this.Textto(this.TrackPeek(1));
						this.TrackPush2(this.TrackPeek());
						this.Advance(1);
						continue;
					case 153:
					{
						this.TrackPop(2);
						int num25 = this.TrackPeek(1);
						this.TrackPush2(this.TrackPeek());
						this.StackPush(num25);
						this.Textto(num25);
						this.Goto(this.Operand(0));
						continue;
					}
					case 154:
						this.StackPop(2);
						break;
					case 155:
						this.StackPop(2);
						break;
					case 156:
						this.TrackPop();
						this.StackPop(2);
						if (this.StackPeek(1) > 0)
						{
							this.Textto(this.StackPeek());
							this.TrackPush2(this.TrackPeek(), this.StackPeek(1) - 1);
							this.Advance(2);
							continue;
						}
						this.StackPush(this.TrackPeek(), this.StackPeek(1) - 1);
						break;
					case 157:
					{
						this.TrackPop(3);
						int num26 = this.TrackPeek();
						int num27 = this.TrackPeek(2);
						if (this.TrackPeek(1) < this.Operand(1) && num27 != num26)
						{
							this.Textto(num27);
							this.StackPush(num27, this.TrackPeek(1) + 1);
							this.TrackPush2(num26);
							this.Goto(this.Operand(0));
							continue;
						}
						this.StackPush(this.TrackPeek(), this.TrackPeek(1));
						break;
					}
					case 158:
					case 159:
						this.StackPop();
						break;
					case 160:
						this.TrackPop();
						this.StackPush(this.TrackPeek());
						base.Uncapture();
						if (this.Operand(0) != -1 && this.Operand(1) != -1)
						{
							base.Uncapture();
						}
						break;
					case 161:
						this.TrackPop();
						this.StackPush(this.TrackPeek());
						break;
					case 162:
						this.StackPop(2);
						break;
					case 164:
						this.TrackPop();
						while (base.Crawlpos() != this.TrackPeek())
						{
							base.Uncapture();
						}
						break;
					default:
						switch (num)
						{
						case 280:
							this.TrackPop();
							this.StackPush(this.TrackPeek());
							goto IL_E4E;
						case 281:
							this.StackPop();
							this.TrackPop();
							this.StackPush(this.TrackPeek());
							goto IL_E4E;
						case 284:
							this.TrackPop(2);
							this.StackPush(this.TrackPeek(), this.TrackPeek(1));
							goto IL_E4E;
						case 285:
							this.TrackPop();
							this.StackPop(2);
							this.StackPush(this.TrackPeek(), this.StackPeek(1) - 1);
							goto IL_E4E;
						}
						goto Block_3;
					}
					break;
				}
				IL_E4E:
				this.Backtrack();
			}
			Block_3:
			IL_E3E:
			throw new NotImplementedException(SR.GetString("Unimplemented state."));
		}

		// Token: 0x04000A5F RID: 2655
		internal int runoperator;

		// Token: 0x04000A60 RID: 2656
		internal int[] runcodes;

		// Token: 0x04000A61 RID: 2657
		internal int runcodepos;

		// Token: 0x04000A62 RID: 2658
		internal string[] runstrings;

		// Token: 0x04000A63 RID: 2659
		internal RegexCode runcode;

		// Token: 0x04000A64 RID: 2660
		internal RegexPrefix runfcPrefix;

		// Token: 0x04000A65 RID: 2661
		internal RegexBoyerMoore runbmPrefix;

		// Token: 0x04000A66 RID: 2662
		internal int runanchors;

		// Token: 0x04000A67 RID: 2663
		internal bool runrtl;

		// Token: 0x04000A68 RID: 2664
		internal bool runci;

		// Token: 0x04000A69 RID: 2665
		internal CultureInfo runculture;
	}
}
