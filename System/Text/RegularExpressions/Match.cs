﻿using System;
using Unity;

namespace System.Text.RegularExpressions
{
	/// <summary>Represents the results from a single regular expression match.</summary>
	// Token: 0x020000DD RID: 221
	[Serializable]
	public class Match : Group
	{
		/// <summary>Gets the empty group. All failed matches return this empty match.</summary>
		/// <returns>An empty match.</returns>
		// Token: 0x17000106 RID: 262
		// (get) Token: 0x060005B9 RID: 1465 RVA: 0x0001D3ED File Offset: 0x0001B5ED
		public static Match Empty
		{
			get
			{
				return Match._empty;
			}
		}

		// Token: 0x060005BA RID: 1466 RVA: 0x0001D3F4 File Offset: 0x0001B5F4
		internal Match(Regex regex, int capcount, string text, int begpos, int len, int startpos) : base(text, new int[2], 0, "0")
		{
			this._regex = regex;
			this._matchcount = new int[capcount];
			this._matches = new int[capcount][];
			this._matches[0] = this._caps;
			this._textbeg = begpos;
			this._textend = begpos + len;
			this._textstart = startpos;
			this._balancing = false;
		}

		// Token: 0x060005BB RID: 1467 RVA: 0x0001D464 File Offset: 0x0001B664
		internal virtual void Reset(Regex regex, string text, int textbeg, int textend, int textstart)
		{
			this._regex = regex;
			this._text = text;
			this._textbeg = textbeg;
			this._textend = textend;
			this._textstart = textstart;
			for (int i = 0; i < this._matchcount.Length; i++)
			{
				this._matchcount[i] = 0;
			}
			this._balancing = false;
		}

		/// <summary>Gets a collection of groups matched by the regular expression.</summary>
		/// <returns>The character groups matched by the pattern.</returns>
		// Token: 0x17000107 RID: 263
		// (get) Token: 0x060005BC RID: 1468 RVA: 0x0001D4B9 File Offset: 0x0001B6B9
		public virtual GroupCollection Groups
		{
			get
			{
				if (this._groupcoll == null)
				{
					this._groupcoll = new GroupCollection(this, null);
				}
				return this._groupcoll;
			}
		}

		/// <summary>Returns a new <see cref="T:System.Text.RegularExpressions.Match" /> object with the results for the next match, starting at the position at which the last match ended (at the character after the last matched character).</summary>
		/// <returns>The next regular expression match.</returns>
		/// <exception cref="T:System.Text.RegularExpressions.RegexMatchTimeoutException">A time-out occurred.</exception>
		// Token: 0x060005BD RID: 1469 RVA: 0x0001D4D6 File Offset: 0x0001B6D6
		public Match NextMatch()
		{
			if (this._regex == null)
			{
				return this;
			}
			return this._regex.Run(false, this._length, this._text, this._textbeg, this._textend - this._textbeg, this._textpos);
		}

		/// <summary>Returns the expansion of the specified replacement pattern. </summary>
		/// <param name="replacement">The replacement pattern to use. </param>
		/// <returns>The expanded version of the <paramref name="replacement" /> parameter.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="replacement" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.NotSupportedException">Expansion is not allowed for this pattern.</exception>
		// Token: 0x060005BE RID: 1470 RVA: 0x0001D514 File Offset: 0x0001B714
		public virtual string Result(string replacement)
		{
			if (replacement == null)
			{
				throw new ArgumentNullException("replacement");
			}
			if (this._regex == null)
			{
				throw new NotSupportedException(SR.GetString("Result cannot be called on a failed Match."));
			}
			RegexReplacement regexReplacement = (RegexReplacement)this._regex.replref.Get();
			if (regexReplacement == null || !regexReplacement.Pattern.Equals(replacement))
			{
				regexReplacement = RegexParser.ParseReplacement(replacement, this._regex.caps, this._regex.capsize, this._regex.capnames, this._regex.roptions);
				this._regex.replref.Cache(regexReplacement);
			}
			return regexReplacement.Replacement(this);
		}

		// Token: 0x060005BF RID: 1471 RVA: 0x0001D5BC File Offset: 0x0001B7BC
		internal virtual string GroupToStringImpl(int groupnum)
		{
			int num = this._matchcount[groupnum];
			if (num == 0)
			{
				return string.Empty;
			}
			int[] array = this._matches[groupnum];
			return this._text.Substring(array[(num - 1) * 2], array[num * 2 - 1]);
		}

		// Token: 0x060005C0 RID: 1472 RVA: 0x0001D5FD File Offset: 0x0001B7FD
		internal string LastGroupToStringImpl()
		{
			return this.GroupToStringImpl(this._matchcount.Length - 1);
		}

		/// <summary>Returns a <see cref="T:System.Text.RegularExpressions.Match" /> instance equivalent to the one supplied that is suitable to share between multiple threads.</summary>
		/// <param name="inner">A regular expression match equivalent to the one expected. </param>
		/// <returns>A regular expression match that is suitable to share between multiple threads. </returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="inner" /> is <see langword="null" />.</exception>
		// Token: 0x060005C1 RID: 1473 RVA: 0x0001D610 File Offset: 0x0001B810
		public static Match Synchronized(Match inner)
		{
			if (inner == null)
			{
				throw new ArgumentNullException("inner");
			}
			int num = inner._matchcount.Length;
			for (int i = 0; i < num; i++)
			{
				Group.Synchronized(inner.Groups[i]);
			}
			return inner;
		}

		// Token: 0x060005C2 RID: 1474 RVA: 0x0001D654 File Offset: 0x0001B854
		internal virtual void AddMatch(int cap, int start, int len)
		{
			if (this._matches[cap] == null)
			{
				this._matches[cap] = new int[2];
			}
			int num = this._matchcount[cap];
			if (num * 2 + 2 > this._matches[cap].Length)
			{
				int[] array = this._matches[cap];
				int[] array2 = new int[num * 8];
				for (int i = 0; i < num * 2; i++)
				{
					array2[i] = array[i];
				}
				this._matches[cap] = array2;
			}
			this._matches[cap][num * 2] = start;
			this._matches[cap][num * 2 + 1] = len;
			this._matchcount[cap] = num + 1;
		}

		// Token: 0x060005C3 RID: 1475 RVA: 0x0001D6EC File Offset: 0x0001B8EC
		internal virtual void BalanceMatch(int cap)
		{
			this._balancing = true;
			int num = this._matchcount[cap] * 2 - 2;
			if (this._matches[cap][num] < 0)
			{
				num = -3 - this._matches[cap][num];
			}
			num -= 2;
			if (num >= 0 && this._matches[cap][num] < 0)
			{
				this.AddMatch(cap, this._matches[cap][num], this._matches[cap][num + 1]);
				return;
			}
			this.AddMatch(cap, -3 - num, -4 - num);
		}

		// Token: 0x060005C4 RID: 1476 RVA: 0x0001D76A File Offset: 0x0001B96A
		internal virtual void RemoveMatch(int cap)
		{
			this._matchcount[cap]--;
		}

		// Token: 0x060005C5 RID: 1477 RVA: 0x0001D77D File Offset: 0x0001B97D
		internal virtual bool IsMatched(int cap)
		{
			return cap < this._matchcount.Length && this._matchcount[cap] > 0 && this._matches[cap][this._matchcount[cap] * 2 - 1] != -2;
		}

		// Token: 0x060005C6 RID: 1478 RVA: 0x0001D7B4 File Offset: 0x0001B9B4
		internal virtual int MatchIndex(int cap)
		{
			int num = this._matches[cap][this._matchcount[cap] * 2 - 2];
			if (num >= 0)
			{
				return num;
			}
			return this._matches[cap][-3 - num];
		}

		// Token: 0x060005C7 RID: 1479 RVA: 0x0001D7EC File Offset: 0x0001B9EC
		internal virtual int MatchLength(int cap)
		{
			int num = this._matches[cap][this._matchcount[cap] * 2 - 1];
			if (num >= 0)
			{
				return num;
			}
			return this._matches[cap][-3 - num];
		}

		// Token: 0x060005C8 RID: 1480 RVA: 0x0001D824 File Offset: 0x0001BA24
		internal virtual void Tidy(int textpos)
		{
			int[] array = this._matches[0];
			this._index = array[0];
			this._length = array[1];
			this._textpos = textpos;
			this._capcount = this._matchcount[0];
			if (this._balancing)
			{
				for (int i = 0; i < this._matchcount.Length; i++)
				{
					int num = this._matchcount[i] * 2;
					int[] array2 = this._matches[i];
					int j = 0;
					while (j < num && array2[j] >= 0)
					{
						j++;
					}
					int num2 = j;
					while (j < num)
					{
						if (array2[j] < 0)
						{
							num2--;
						}
						else
						{
							if (j != num2)
							{
								array2[num2] = array2[j];
							}
							num2++;
						}
						j++;
					}
					this._matchcount[i] = num2 / 2;
				}
				this._balancing = false;
			}
		}

		// Token: 0x060005C9 RID: 1481 RVA: 0x0001D8F4 File Offset: 0x0001BAF4
		// Note: this type is marked as 'beforefieldinit'.
		static Match()
		{
		}

		// Token: 0x060005CA RID: 1482 RVA: 0x000092E2 File Offset: 0x000074E2
		internal Match()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000A6A RID: 2666
		internal static Match _empty = new Match(null, 1, string.Empty, 0, 0, 0);

		// Token: 0x04000A6B RID: 2667
		internal GroupCollection _groupcoll;

		// Token: 0x04000A6C RID: 2668
		internal Regex _regex;

		// Token: 0x04000A6D RID: 2669
		internal int _textbeg;

		// Token: 0x04000A6E RID: 2670
		internal int _textpos;

		// Token: 0x04000A6F RID: 2671
		internal int _textend;

		// Token: 0x04000A70 RID: 2672
		internal int _textstart;

		// Token: 0x04000A71 RID: 2673
		internal int[][] _matches;

		// Token: 0x04000A72 RID: 2674
		internal int[] _matchcount;

		// Token: 0x04000A73 RID: 2675
		internal bool _balancing;
	}
}
