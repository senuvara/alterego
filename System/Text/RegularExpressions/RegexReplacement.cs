﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Text.RegularExpressions
{
	// Token: 0x020000E5 RID: 229
	internal sealed class RegexReplacement
	{
		// Token: 0x0600064A RID: 1610 RVA: 0x00021044 File Offset: 0x0001F244
		internal RegexReplacement(string rep, RegexNode concat, Hashtable _caps)
		{
			this._rep = rep;
			if (concat.Type() != 25)
			{
				throw new ArgumentException(SR.GetString("Replacement pattern error."));
			}
			StringBuilder stringBuilder = new StringBuilder();
			List<string> list = new List<string>();
			List<int> list2 = new List<int>();
			int i = 0;
			while (i < concat.ChildCount())
			{
				RegexNode regexNode = concat.Child(i);
				switch (regexNode.Type())
				{
				case 9:
					stringBuilder.Append(regexNode._ch);
					break;
				case 10:
				case 11:
					goto IL_E9;
				case 12:
					stringBuilder.Append(regexNode._str);
					break;
				case 13:
				{
					if (stringBuilder.Length > 0)
					{
						list2.Add(list.Count);
						list.Add(stringBuilder.ToString());
						stringBuilder.Length = 0;
					}
					int num = regexNode._m;
					if (_caps != null && num >= 0)
					{
						num = (int)_caps[num];
					}
					list2.Add(-5 - num);
					break;
				}
				default:
					goto IL_E9;
				}
				i++;
				continue;
				IL_E9:
				throw new ArgumentException(SR.GetString("Replacement pattern error."));
			}
			if (stringBuilder.Length > 0)
			{
				list2.Add(list.Count);
				list.Add(stringBuilder.ToString());
			}
			this._strings = list;
			this._rules = list2;
		}

		// Token: 0x0600064B RID: 1611 RVA: 0x0002118C File Offset: 0x0001F38C
		private void ReplacementImpl(StringBuilder sb, Match match)
		{
			for (int i = 0; i < this._rules.Count; i++)
			{
				int num = this._rules[i];
				if (num >= 0)
				{
					sb.Append(this._strings[num]);
				}
				else if (num < -4)
				{
					sb.Append(match.GroupToStringImpl(-5 - num));
				}
				else
				{
					switch (-5 - num)
					{
					case -4:
						sb.Append(match.GetOriginalString());
						break;
					case -3:
						sb.Append(match.LastGroupToStringImpl());
						break;
					case -2:
						sb.Append(match.GetRightSubstring());
						break;
					case -1:
						sb.Append(match.GetLeftSubstring());
						break;
					}
				}
			}
		}

		// Token: 0x0600064C RID: 1612 RVA: 0x00021250 File Offset: 0x0001F450
		private void ReplacementImplRTL(List<string> al, Match match)
		{
			for (int i = this._rules.Count - 1; i >= 0; i--)
			{
				int num = this._rules[i];
				if (num >= 0)
				{
					al.Add(this._strings[num]);
				}
				else if (num < -4)
				{
					al.Add(match.GroupToStringImpl(-5 - num));
				}
				else
				{
					switch (-5 - num)
					{
					case -4:
						al.Add(match.GetOriginalString());
						break;
					case -3:
						al.Add(match.LastGroupToStringImpl());
						break;
					case -2:
						al.Add(match.GetRightSubstring());
						break;
					case -1:
						al.Add(match.GetLeftSubstring());
						break;
					}
				}
			}
		}

		// Token: 0x17000112 RID: 274
		// (get) Token: 0x0600064D RID: 1613 RVA: 0x0002130D File Offset: 0x0001F50D
		internal string Pattern
		{
			get
			{
				return this._rep;
			}
		}

		// Token: 0x0600064E RID: 1614 RVA: 0x00021318 File Offset: 0x0001F518
		internal string Replacement(Match match)
		{
			StringBuilder stringBuilder = new StringBuilder();
			this.ReplacementImpl(stringBuilder, match);
			return stringBuilder.ToString();
		}

		// Token: 0x0600064F RID: 1615 RVA: 0x0002133C File Offset: 0x0001F53C
		internal string Replace(Regex regex, string input, int count, int startat)
		{
			if (count < -1)
			{
				throw new ArgumentOutOfRangeException("count", SR.GetString("Count cannot be less than -1."));
			}
			if (startat < 0 || startat > input.Length)
			{
				throw new ArgumentOutOfRangeException("startat", SR.GetString("Start index cannot be less than 0 or greater than input length."));
			}
			if (count == 0)
			{
				return input;
			}
			Match match = regex.Match(input, startat);
			if (!match.Success)
			{
				return input;
			}
			StringBuilder stringBuilder;
			if (!regex.RightToLeft)
			{
				stringBuilder = new StringBuilder();
				int num = 0;
				do
				{
					if (match.Index != num)
					{
						stringBuilder.Append(input, num, match.Index - num);
					}
					num = match.Index + match.Length;
					this.ReplacementImpl(stringBuilder, match);
					if (--count == 0)
					{
						break;
					}
					match = match.NextMatch();
				}
				while (match.Success);
				if (num < input.Length)
				{
					stringBuilder.Append(input, num, input.Length - num);
				}
			}
			else
			{
				List<string> list = new List<string>();
				int num2 = input.Length;
				do
				{
					if (match.Index + match.Length != num2)
					{
						list.Add(input.Substring(match.Index + match.Length, num2 - match.Index - match.Length));
					}
					num2 = match.Index;
					this.ReplacementImplRTL(list, match);
					if (--count == 0)
					{
						break;
					}
					match = match.NextMatch();
				}
				while (match.Success);
				stringBuilder = new StringBuilder();
				if (num2 > 0)
				{
					stringBuilder.Append(input, 0, num2);
				}
				for (int i = list.Count - 1; i >= 0; i--)
				{
					stringBuilder.Append(list[i]);
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000650 RID: 1616 RVA: 0x000214CC File Offset: 0x0001F6CC
		internal static string Replace(MatchEvaluator evaluator, Regex regex, string input, int count, int startat)
		{
			if (evaluator == null)
			{
				throw new ArgumentNullException("evaluator");
			}
			if (count < -1)
			{
				throw new ArgumentOutOfRangeException("count", SR.GetString("Count cannot be less than -1."));
			}
			if (startat < 0 || startat > input.Length)
			{
				throw new ArgumentOutOfRangeException("startat", SR.GetString("Start index cannot be less than 0 or greater than input length."));
			}
			if (count == 0)
			{
				return input;
			}
			Match match = regex.Match(input, startat);
			if (!match.Success)
			{
				return input;
			}
			StringBuilder stringBuilder;
			if (!regex.RightToLeft)
			{
				stringBuilder = new StringBuilder();
				int num = 0;
				do
				{
					if (match.Index != num)
					{
						stringBuilder.Append(input, num, match.Index - num);
					}
					num = match.Index + match.Length;
					stringBuilder.Append(evaluator(match));
					if (--count == 0)
					{
						break;
					}
					match = match.NextMatch();
				}
				while (match.Success);
				if (num < input.Length)
				{
					stringBuilder.Append(input, num, input.Length - num);
				}
			}
			else
			{
				List<string> list = new List<string>();
				int num2 = input.Length;
				do
				{
					if (match.Index + match.Length != num2)
					{
						list.Add(input.Substring(match.Index + match.Length, num2 - match.Index - match.Length));
					}
					num2 = match.Index;
					list.Add(evaluator(match));
					if (--count == 0)
					{
						break;
					}
					match = match.NextMatch();
				}
				while (match.Success);
				stringBuilder = new StringBuilder();
				if (num2 > 0)
				{
					stringBuilder.Append(input, 0, num2);
				}
				for (int i = list.Count - 1; i >= 0; i--)
				{
					stringBuilder.Append(list[i]);
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000651 RID: 1617 RVA: 0x00021674 File Offset: 0x0001F874
		internal static string[] Split(Regex regex, string input, int count, int startat)
		{
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", SR.GetString("Count cannot be less than -1."));
			}
			if (startat < 0 || startat > input.Length)
			{
				throw new ArgumentOutOfRangeException("startat", SR.GetString("Start index cannot be less than 0 or greater than input length."));
			}
			if (count == 1)
			{
				return new string[]
				{
					input
				};
			}
			count--;
			Match match = regex.Match(input, startat);
			if (!match.Success)
			{
				return new string[]
				{
					input
				};
			}
			List<string> list = new List<string>();
			if (!regex.RightToLeft)
			{
				int num = 0;
				do
				{
					list.Add(input.Substring(num, match.Index - num));
					num = match.Index + match.Length;
					for (int i = 1; i < match.Groups.Count; i++)
					{
						if (match.IsMatched(i))
						{
							list.Add(match.Groups[i].ToString());
						}
					}
					if (--count == 0)
					{
						break;
					}
					match = match.NextMatch();
				}
				while (match.Success);
				list.Add(input.Substring(num, input.Length - num));
			}
			else
			{
				int num2 = input.Length;
				do
				{
					list.Add(input.Substring(match.Index + match.Length, num2 - match.Index - match.Length));
					num2 = match.Index;
					for (int j = 1; j < match.Groups.Count; j++)
					{
						if (match.IsMatched(j))
						{
							list.Add(match.Groups[j].ToString());
						}
					}
					if (--count == 0)
					{
						break;
					}
					match = match.NextMatch();
				}
				while (match.Success);
				list.Add(input.Substring(0, num2));
				list.Reverse(0, list.Count);
			}
			return list.ToArray();
		}

		// Token: 0x04000AD5 RID: 2773
		internal string _rep;

		// Token: 0x04000AD6 RID: 2774
		internal List<string> _strings;

		// Token: 0x04000AD7 RID: 2775
		internal List<int> _rules;

		// Token: 0x04000AD8 RID: 2776
		internal const int Specials = 4;

		// Token: 0x04000AD9 RID: 2777
		internal const int LeftPortion = -1;

		// Token: 0x04000ADA RID: 2778
		internal const int RightPortion = -2;

		// Token: 0x04000ADB RID: 2779
		internal const int LastGroup = -3;

		// Token: 0x04000ADC RID: 2780
		internal const int WholeString = -4;
	}
}
