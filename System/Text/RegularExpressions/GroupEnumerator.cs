﻿using System;
using System.Collections;

namespace System.Text.RegularExpressions
{
	// Token: 0x020000DB RID: 219
	internal class GroupEnumerator : IEnumerator
	{
		// Token: 0x0600058B RID: 1419 RVA: 0x0001BB34 File Offset: 0x00019D34
		internal GroupEnumerator(GroupCollection rgc)
		{
			this._curindex = -1;
			this._rgc = rgc;
		}

		// Token: 0x0600058C RID: 1420 RVA: 0x0001BB4C File Offset: 0x00019D4C
		public bool MoveNext()
		{
			int count = this._rgc.Count;
			if (this._curindex >= count)
			{
				return false;
			}
			this._curindex++;
			return this._curindex < count;
		}

		// Token: 0x17000104 RID: 260
		// (get) Token: 0x0600058D RID: 1421 RVA: 0x0001BB87 File Offset: 0x00019D87
		public object Current
		{
			get
			{
				return this.Capture;
			}
		}

		// Token: 0x17000105 RID: 261
		// (get) Token: 0x0600058E RID: 1422 RVA: 0x0001BB8F File Offset: 0x00019D8F
		public Capture Capture
		{
			get
			{
				if (this._curindex < 0 || this._curindex >= this._rgc.Count)
				{
					throw new InvalidOperationException(SR.GetString("Enumeration has either not started or has already finished."));
				}
				return this._rgc[this._curindex];
			}
		}

		// Token: 0x0600058F RID: 1423 RVA: 0x0001BBCE File Offset: 0x00019DCE
		public void Reset()
		{
			this._curindex = -1;
		}

		// Token: 0x04000A5D RID: 2653
		internal GroupCollection _rgc;

		// Token: 0x04000A5E RID: 2654
		internal int _curindex;
	}
}
