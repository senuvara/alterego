﻿using System;
using System.Collections;

namespace System.Text.RegularExpressions
{
	// Token: 0x020000DE RID: 222
	internal class MatchSparse : Match
	{
		// Token: 0x060005CB RID: 1483 RVA: 0x0001D90A File Offset: 0x0001BB0A
		internal MatchSparse(Regex regex, Hashtable caps, int capcount, string text, int begpos, int len, int startpos) : base(regex, capcount, text, begpos, len, startpos)
		{
			this._caps = caps;
		}

		// Token: 0x17000108 RID: 264
		// (get) Token: 0x060005CC RID: 1484 RVA: 0x0001D923 File Offset: 0x0001BB23
		public override GroupCollection Groups
		{
			get
			{
				if (this._groupcoll == null)
				{
					this._groupcoll = new GroupCollection(this, this._caps);
				}
				return this._groupcoll;
			}
		}

		// Token: 0x04000A74 RID: 2676
		internal new Hashtable _caps;
	}
}
