﻿using System;
using System.Runtime.Serialization;
using Unity;

namespace System.Text.RegularExpressions
{
	/// <summary>Represents the results from a single capturing group. </summary>
	// Token: 0x020000D9 RID: 217
	[Serializable]
	public class Group : Capture
	{
		// Token: 0x06000578 RID: 1400 RVA: 0x0001B8CB File Offset: 0x00019ACB
		internal Group(string text, int[] caps, int capcount, string name) : base(text, (capcount == 0) ? 0 : caps[(capcount - 1) * 2], (capcount == 0) ? 0 : caps[capcount * 2 - 1])
		{
			this._caps = caps;
			this._capcount = capcount;
			this._name = name;
		}

		/// <summary>Gets a value indicating whether the match is successful.</summary>
		/// <returns>
		///     <see langword="true" /> if the match is successful; otherwise, <see langword="false" />.</returns>
		// Token: 0x170000FB RID: 251
		// (get) Token: 0x06000579 RID: 1401 RVA: 0x0001B904 File Offset: 0x00019B04
		public bool Success
		{
			get
			{
				return this._capcount != 0;
			}
		}

		/// <summary>Returns the name of the capturing group representing by the current instance.</summary>
		/// <returns>The name of the capturing group represented by the current instance.  </returns>
		// Token: 0x170000FC RID: 252
		// (get) Token: 0x0600057A RID: 1402 RVA: 0x0001B90F File Offset: 0x00019B0F
		public string Name
		{
			get
			{
				return this._name;
			}
		}

		/// <summary>Gets a collection of all the captures matched by the capturing group, in innermost-leftmost-first order (or innermost-rightmost-first order if the regular expression is modified with the <see cref="F:System.Text.RegularExpressions.RegexOptions.RightToLeft" /> option). The collection may have zero or more items.</summary>
		/// <returns>The collection of substrings matched by the group.</returns>
		// Token: 0x170000FD RID: 253
		// (get) Token: 0x0600057B RID: 1403 RVA: 0x0001B917 File Offset: 0x00019B17
		public CaptureCollection Captures
		{
			get
			{
				if (this._capcoll == null)
				{
					this._capcoll = new CaptureCollection(this);
				}
				return this._capcoll;
			}
		}

		/// <summary>Returns a <see langword="Group" /> object equivalent to the one supplied that is safe to share between multiple threads.</summary>
		/// <param name="inner">The input <see cref="T:System.Text.RegularExpressions.Group" /> object.</param>
		/// <returns>A regular expression <see langword="Group" /> object. </returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="inner" /> is <see langword="null" />.</exception>
		// Token: 0x0600057C RID: 1404 RVA: 0x0001B934 File Offset: 0x00019B34
		public static Group Synchronized(Group inner)
		{
			if (inner == null)
			{
				throw new ArgumentNullException("inner");
			}
			CaptureCollection captures = inner.Captures;
			if (inner._capcount > 0)
			{
				Capture capture = captures[0];
			}
			return inner;
		}

		// Token: 0x0600057D RID: 1405 RVA: 0x0001B968 File Offset: 0x00019B68
		// Note: this type is marked as 'beforefieldinit'.
		static Group()
		{
		}

		// Token: 0x0600057E RID: 1406 RVA: 0x000092E2 File Offset: 0x000074E2
		internal Group()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000A55 RID: 2645
		internal static Group _emptygroup = new Group(string.Empty, new int[0], 0, string.Empty);

		// Token: 0x04000A56 RID: 2646
		internal int[] _caps;

		// Token: 0x04000A57 RID: 2647
		internal int _capcount;

		// Token: 0x04000A58 RID: 2648
		internal CaptureCollection _capcoll;

		// Token: 0x04000A59 RID: 2649
		[OptionalField]
		internal string _name;
	}
}
