﻿using System;
using System.Collections;

namespace System.Text.RegularExpressions
{
	// Token: 0x020000CF RID: 207
	[Serializable]
	internal class CaptureEnumerator : IEnumerator
	{
		// Token: 0x0600051A RID: 1306 RVA: 0x0001827F File Offset: 0x0001647F
		internal CaptureEnumerator(CaptureCollection rcc)
		{
			this._curindex = -1;
			this._rcc = rcc;
		}

		// Token: 0x0600051B RID: 1307 RVA: 0x00018298 File Offset: 0x00016498
		public bool MoveNext()
		{
			int count = this._rcc.Count;
			if (this._curindex >= count)
			{
				return false;
			}
			this._curindex++;
			return this._curindex < count;
		}

		// Token: 0x170000EE RID: 238
		// (get) Token: 0x0600051C RID: 1308 RVA: 0x000182D3 File Offset: 0x000164D3
		public object Current
		{
			get
			{
				return this.Capture;
			}
		}

		// Token: 0x170000EF RID: 239
		// (get) Token: 0x0600051D RID: 1309 RVA: 0x000182DB File Offset: 0x000164DB
		public Capture Capture
		{
			get
			{
				if (this._curindex < 0 || this._curindex >= this._rcc.Count)
				{
					throw new InvalidOperationException(SR.GetString("Enumeration has either not started or has already finished."));
				}
				return this._rcc[this._curindex];
			}
		}

		// Token: 0x0600051E RID: 1310 RVA: 0x0001831A File Offset: 0x0001651A
		public void Reset()
		{
			this._curindex = -1;
		}

		// Token: 0x040009C7 RID: 2503
		internal CaptureCollection _rcc;

		// Token: 0x040009C8 RID: 2504
		internal int _curindex;
	}
}
