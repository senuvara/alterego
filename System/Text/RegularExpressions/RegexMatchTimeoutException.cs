﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace System.Text.RegularExpressions
{
	/// <summary>The exception that is thrown when the execution time of a regular expression pattern-matching method exceeds its time-out interval.</summary>
	// Token: 0x020000E1 RID: 225
	[Serializable]
	public class RegexMatchTimeoutException : TimeoutException, ISerializable
	{
		/// <summary>
		///     Initializes a new instance of the <see cref="T:System.Text.RegularExpressions.RegexMatchTimeoutException" /> class with information about the regular expression pattern, the input text, and the time-out interval.</summary>
		/// <param name="regexInput">The input text processed by the regular expression engine when the time-out occurred.</param>
		/// <param name="regexPattern">The pattern used by the regular expression engine when the time-out occurred.</param>
		/// <param name="matchTimeout">The time-out interval.</param>
		// Token: 0x060005DC RID: 1500 RVA: 0x0001DBC1 File Offset: 0x0001BDC1
		public RegexMatchTimeoutException(string regexInput, string regexPattern, TimeSpan matchTimeout) : base(SR.GetString("The RegEx engine has timed out while trying to match a pattern to an input string. This can occur for many reasons, including very large inputs or excessive backtracking caused by nested quantifiers, back-references and other factors."))
		{
			this.Init(regexInput, regexPattern, matchTimeout);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Text.RegularExpressions.RegexMatchTimeoutException" /> class with a system-supplied message.</summary>
		// Token: 0x060005DD RID: 1501 RVA: 0x0001DBE9 File Offset: 0x0001BDE9
		public RegexMatchTimeoutException()
		{
			this.Init();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Text.RegularExpressions.RegexMatchTimeoutException" /> class with the specified message string.</summary>
		/// <param name="message">A string that describes the exception.</param>
		// Token: 0x060005DE RID: 1502 RVA: 0x0001DC04 File Offset: 0x0001BE04
		public RegexMatchTimeoutException(string message) : base(message)
		{
			this.Init();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Text.RegularExpressions.RegexMatchTimeoutException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">A string that describes the exception.</param>
		/// <param name="inner">The exception that is the cause of the current exception.</param>
		// Token: 0x060005DF RID: 1503 RVA: 0x0001DC20 File Offset: 0x0001BE20
		public RegexMatchTimeoutException(string message, Exception inner) : base(message, inner)
		{
			this.Init();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Text.RegularExpressions.RegexMatchTimeoutException" /> class with serialized data.</summary>
		/// <param name="info">The object that contains the serialized data.</param>
		/// <param name="context">The stream that contains the serialized data.</param>
		// Token: 0x060005E0 RID: 1504 RVA: 0x0001DC40 File Offset: 0x0001BE40
		[SecurityPermission(SecurityAction.LinkDemand, SerializationFormatter = true)]
		protected RegexMatchTimeoutException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			string @string = info.GetString("regexInput");
			string string2 = info.GetString("regexPattern");
			TimeSpan timeout = TimeSpan.FromTicks(info.GetInt64("timeoutTicks"));
			this.Init(@string, string2, timeout);
		}

		/// <summary>Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object with the data needed to serialize a <see cref="T:System.Text.RegularExpressions.RegexMatchTimeoutException" /> object.</summary>
		/// <param name="si">The object to populate with data.</param>
		/// <param name="context">The destination for this serialization.</param>
		// Token: 0x060005E1 RID: 1505 RVA: 0x0001DC94 File Offset: 0x0001BE94
		[SecurityPermission(SecurityAction.LinkDemand, SerializationFormatter = true)]
		void ISerializable.GetObjectData(SerializationInfo si, StreamingContext context)
		{
			base.GetObjectData(si, context);
			si.AddValue("regexInput", this.regexInput);
			si.AddValue("regexPattern", this.regexPattern);
			si.AddValue("timeoutTicks", this.matchTimeout.Ticks);
		}

		// Token: 0x060005E2 RID: 1506 RVA: 0x0001DCE1 File Offset: 0x0001BEE1
		private void Init()
		{
			this.Init("", "", TimeSpan.FromTicks(-1L));
		}

		// Token: 0x060005E3 RID: 1507 RVA: 0x0001DCFA File Offset: 0x0001BEFA
		private void Init(string input, string pattern, TimeSpan timeout)
		{
			this.regexInput = input;
			this.regexPattern = pattern;
			this.matchTimeout = timeout;
		}

		/// <summary>Gets the regular expression pattern that was used in the matching operation when the time-out occurred.</summary>
		/// <returns>The regular expression pattern.</returns>
		// Token: 0x1700010F RID: 271
		// (get) Token: 0x060005E4 RID: 1508 RVA: 0x0001DD11 File Offset: 0x0001BF11
		public string Pattern
		{
			[PermissionSet(SecurityAction.LinkDemand, Unrestricted = true)]
			get
			{
				return this.regexPattern;
			}
		}

		/// <summary>Gets the input text that the regular expression engine was processing when the time-out occurred.</summary>
		/// <returns>The regular expression input text.</returns>
		// Token: 0x17000110 RID: 272
		// (get) Token: 0x060005E5 RID: 1509 RVA: 0x0001DD19 File Offset: 0x0001BF19
		public string Input
		{
			[PermissionSet(SecurityAction.LinkDemand, Unrestricted = true)]
			get
			{
				return this.regexInput;
			}
		}

		/// <summary>Gets the time-out interval for a regular expression match.</summary>
		/// <returns>The time-out interval.</returns>
		// Token: 0x17000111 RID: 273
		// (get) Token: 0x060005E6 RID: 1510 RVA: 0x0001DD21 File Offset: 0x0001BF21
		public TimeSpan MatchTimeout
		{
			[PermissionSet(SecurityAction.LinkDemand, Unrestricted = true)]
			get
			{
				return this.matchTimeout;
			}
		}

		// Token: 0x04000A82 RID: 2690
		private string regexInput;

		// Token: 0x04000A83 RID: 2691
		private string regexPattern;

		// Token: 0x04000A84 RID: 2692
		private TimeSpan matchTimeout = TimeSpan.FromTicks(-1L);
	}
}
