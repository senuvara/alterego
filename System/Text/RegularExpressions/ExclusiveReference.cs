﻿using System;
using System.Threading;

namespace System.Text.RegularExpressions
{
	// Token: 0x020000CA RID: 202
	internal sealed class ExclusiveReference
	{
		// Token: 0x060004FC RID: 1276 RVA: 0x00017A8C File Offset: 0x00015C8C
		internal object Get()
		{
			if (Interlocked.Exchange(ref this._locked, 1) != 0)
			{
				return null;
			}
			object @ref = this._ref;
			if (@ref == null)
			{
				this._locked = 0;
				return null;
			}
			this._obj = @ref;
			return @ref;
		}

		// Token: 0x060004FD RID: 1277 RVA: 0x00017AC4 File Offset: 0x00015CC4
		internal void Release(object obj)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj");
			}
			if (this._obj == obj)
			{
				this._obj = null;
				this._locked = 0;
				return;
			}
			if (this._obj == null && Interlocked.Exchange(ref this._locked, 1) == 0)
			{
				if (this._ref == null)
				{
					this._ref = (RegexRunner)obj;
				}
				this._locked = 0;
				return;
			}
		}

		// Token: 0x060004FE RID: 1278 RVA: 0x0000232F File Offset: 0x0000052F
		public ExclusiveReference()
		{
		}

		// Token: 0x040009B2 RID: 2482
		private RegexRunner _ref;

		// Token: 0x040009B3 RID: 2483
		private object _obj;

		// Token: 0x040009B4 RID: 2484
		private int _locked;
	}
}
