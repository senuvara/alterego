﻿using System;
using System.Collections;
using Unity;

namespace System.Text.RegularExpressions
{
	/// <summary>Represents the set of successful matches found by iteratively applying a regular expression pattern to the input string.</summary>
	// Token: 0x020000DF RID: 223
	[Serializable]
	public class MatchCollection : ICollection, IEnumerable
	{
		// Token: 0x060005CD RID: 1485 RVA: 0x0001D948 File Offset: 0x0001BB48
		internal MatchCollection(Regex regex, string input, int beginning, int length, int startat)
		{
			if (startat < 0 || startat > input.Length)
			{
				throw new ArgumentOutOfRangeException("startat", SR.GetString("Start index cannot be less than 0 or greater than input length."));
			}
			this._regex = regex;
			this._input = input;
			this._beginning = beginning;
			this._length = length;
			this._startat = startat;
			this._prevlen = -1;
			this._matches = new ArrayList();
			this._done = false;
		}

		// Token: 0x060005CE RID: 1486 RVA: 0x0001D9C0 File Offset: 0x0001BBC0
		internal Match GetMatch(int i)
		{
			if (i < 0)
			{
				return null;
			}
			if (this._matches.Count > i)
			{
				return (Match)this._matches[i];
			}
			if (this._done)
			{
				return null;
			}
			for (;;)
			{
				Match match = this._regex.Run(false, this._prevlen, this._input, this._beginning, this._length, this._startat);
				if (!match.Success)
				{
					break;
				}
				this._matches.Add(match);
				this._prevlen = match._length;
				this._startat = match._textpos;
				if (this._matches.Count > i)
				{
					return match;
				}
			}
			this._done = true;
			return null;
		}

		/// <summary>Gets the number of matches.</summary>
		/// <returns>The number of matches.</returns>
		/// <exception cref="T:System.Text.RegularExpressions.RegexMatchTimeoutException">A time-out occurred.</exception>
		// Token: 0x17000109 RID: 265
		// (get) Token: 0x060005CF RID: 1487 RVA: 0x0001DA6D File Offset: 0x0001BC6D
		public int Count
		{
			get
			{
				if (this._done)
				{
					return this._matches.Count;
				}
				this.GetMatch(MatchCollection.infinite);
				return this._matches.Count;
			}
		}

		/// <summary>Gets an object that can be used to synchronize access to the collection.</summary>
		/// <returns>An object that can be used to synchronize access to the collection. This property always returns the object itself.</returns>
		// Token: 0x1700010A RID: 266
		// (get) Token: 0x060005D0 RID: 1488 RVA: 0x00002068 File Offset: 0x00000268
		public object SyncRoot
		{
			get
			{
				return this;
			}
		}

		/// <summary>Gets a value indicating whether access to the collection is synchronized (thread-safe).</summary>
		/// <returns>
		///     <see langword="false" /> in all cases.</returns>
		// Token: 0x1700010B RID: 267
		// (get) Token: 0x060005D1 RID: 1489 RVA: 0x00005AFA File Offset: 0x00003CFA
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets a value that indicates whether the collection is read only.</summary>
		/// <returns>
		///     <see langword="true" /> in all cases. </returns>
		// Token: 0x1700010C RID: 268
		// (get) Token: 0x060005D2 RID: 1490 RVA: 0x00003298 File Offset: 0x00001498
		public bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		/// <summary>Gets an individual member of the collection.</summary>
		/// <param name="i">Index into the <see cref="T:System.Text.RegularExpressions.Match" /> collection. </param>
		/// <returns>The captured substring at position <paramref name="i" /> in the collection.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="i" /> is less than 0 or greater than or equal to <see cref="P:System.Text.RegularExpressions.MatchCollection.Count" />. </exception>
		/// <exception cref="T:System.Text.RegularExpressions.RegexMatchTimeoutException">A time-out occurred.</exception>
		// Token: 0x1700010D RID: 269
		public virtual Match this[int i]
		{
			get
			{
				Match match = this.GetMatch(i);
				if (match == null)
				{
					throw new ArgumentOutOfRangeException("i");
				}
				return match;
			}
		}

		/// <summary>Copies all the elements of the collection to the given array starting at the given index.</summary>
		/// <param name="array">The array the collection is to be copied into. </param>
		/// <param name="arrayIndex">The position in the array where copying is to begin. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="array" /> is a multi-dimensional array.</exception>
		/// <exception cref="T:System.IndexOutOfRangeException">
		///         <paramref name="arrayIndex" /> is outside the bounds of <paramref name="array" />.-or-
		///         <paramref name="arrayIndex" /> plus <see cref="P:System.Text.RegularExpressions.MatchCollection.Count" /> is outside the bounds of <paramref name="array" />.</exception>
		/// <exception cref="T:System.Text.RegularExpressions.RegexMatchTimeoutException">A time-out occurred.</exception>
		// Token: 0x060005D4 RID: 1492 RVA: 0x0001DAB4 File Offset: 0x0001BCB4
		public void CopyTo(Array array, int arrayIndex)
		{
			if (array != null && array.Rank != 1)
			{
				throw new ArgumentException(SR.GetString("Only single dimensional arrays are supported for the requested action."));
			}
			int count = this.Count;
			try
			{
				this._matches.CopyTo(array, arrayIndex);
			}
			catch (ArrayTypeMismatchException innerException)
			{
				throw new ArgumentException(SR.GetString("Target array type is not compatible with the type of items in the collection."), innerException);
			}
		}

		/// <summary>Provides an enumerator that iterates through the collection.</summary>
		/// <returns>An object that contains all <see cref="T:System.Text.RegularExpressions.Match" /> objects within the <see cref="T:System.Text.RegularExpressions.MatchCollection" />.</returns>
		/// <exception cref="T:System.Text.RegularExpressions.RegexMatchTimeoutException">A time-out occurred.</exception>
		// Token: 0x060005D5 RID: 1493 RVA: 0x0001DB18 File Offset: 0x0001BD18
		public IEnumerator GetEnumerator()
		{
			return new MatchEnumerator(this);
		}

		// Token: 0x060005D6 RID: 1494 RVA: 0x0001DB20 File Offset: 0x0001BD20
		// Note: this type is marked as 'beforefieldinit'.
		static MatchCollection()
		{
		}

		// Token: 0x060005D7 RID: 1495 RVA: 0x000092E2 File Offset: 0x000074E2
		internal MatchCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000A75 RID: 2677
		internal Regex _regex;

		// Token: 0x04000A76 RID: 2678
		internal ArrayList _matches;

		// Token: 0x04000A77 RID: 2679
		internal bool _done;

		// Token: 0x04000A78 RID: 2680
		internal string _input;

		// Token: 0x04000A79 RID: 2681
		internal int _beginning;

		// Token: 0x04000A7A RID: 2682
		internal int _length;

		// Token: 0x04000A7B RID: 2683
		internal int _startat;

		// Token: 0x04000A7C RID: 2684
		internal int _prevlen;

		// Token: 0x04000A7D RID: 2685
		private static int infinite = int.MaxValue;
	}
}
