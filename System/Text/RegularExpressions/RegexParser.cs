﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;

namespace System.Text.RegularExpressions
{
	// Token: 0x020000E4 RID: 228
	internal sealed class RegexParser
	{
		// Token: 0x060005FB RID: 1531 RVA: 0x0001E598 File Offset: 0x0001C798
		internal static RegexTree Parse(string re, RegexOptions op)
		{
			RegexParser regexParser = new RegexParser(((op & RegexOptions.CultureInvariant) != RegexOptions.None) ? CultureInfo.InvariantCulture : CultureInfo.CurrentCulture);
			regexParser._options = op;
			regexParser.SetPattern(re);
			regexParser.CountCaptures();
			regexParser.Reset(op);
			RegexNode root = regexParser.ScanRegex();
			string[] capslist;
			if (regexParser._capnamelist == null)
			{
				capslist = null;
			}
			else
			{
				capslist = regexParser._capnamelist.ToArray();
			}
			return new RegexTree(root, regexParser._caps, regexParser._capnumlist, regexParser._captop, regexParser._capnames, capslist, op);
		}

		// Token: 0x060005FC RID: 1532 RVA: 0x0001E618 File Offset: 0x0001C818
		internal static RegexReplacement ParseReplacement(string rep, Hashtable caps, int capsize, Hashtable capnames, RegexOptions op)
		{
			RegexParser regexParser = new RegexParser(((op & RegexOptions.CultureInvariant) != RegexOptions.None) ? CultureInfo.InvariantCulture : CultureInfo.CurrentCulture);
			regexParser._options = op;
			regexParser.NoteCaptures(caps, capsize, capnames);
			regexParser.SetPattern(rep);
			RegexNode concat = regexParser.ScanReplacement();
			return new RegexReplacement(rep, concat, caps);
		}

		// Token: 0x060005FD RID: 1533 RVA: 0x0001E668 File Offset: 0x0001C868
		internal static string Escape(string input)
		{
			for (int i = 0; i < input.Length; i++)
			{
				if (RegexParser.IsMetachar(input[i]))
				{
					StringBuilder stringBuilder = new StringBuilder();
					char c = input[i];
					stringBuilder.Append(input, 0, i);
					do
					{
						stringBuilder.Append('\\');
						switch (c)
						{
						case '\t':
							c = 't';
							break;
						case '\n':
							c = 'n';
							break;
						case '\f':
							c = 'f';
							break;
						case '\r':
							c = 'r';
							break;
						}
						stringBuilder.Append(c);
						i++;
						int num = i;
						while (i < input.Length)
						{
							c = input[i];
							if (RegexParser.IsMetachar(c))
							{
								break;
							}
							i++;
						}
						stringBuilder.Append(input, num, i - num);
					}
					while (i < input.Length);
					return stringBuilder.ToString();
				}
			}
			return input;
		}

		// Token: 0x060005FE RID: 1534 RVA: 0x0001E73C File Offset: 0x0001C93C
		internal static string Unescape(string input)
		{
			for (int i = 0; i < input.Length; i++)
			{
				if (input[i] == '\\')
				{
					StringBuilder stringBuilder = new StringBuilder();
					RegexParser regexParser = new RegexParser(CultureInfo.InvariantCulture);
					regexParser.SetPattern(input);
					stringBuilder.Append(input, 0, i);
					do
					{
						i++;
						regexParser.Textto(i);
						if (i < input.Length)
						{
							stringBuilder.Append(regexParser.ScanCharEscape());
						}
						i = regexParser.Textpos();
						int num = i;
						while (i < input.Length && input[i] != '\\')
						{
							i++;
						}
						stringBuilder.Append(input, num, i - num);
					}
					while (i < input.Length);
					return stringBuilder.ToString();
				}
			}
			return input;
		}

		// Token: 0x060005FF RID: 1535 RVA: 0x0001E7F1 File Offset: 0x0001C9F1
		private RegexParser(CultureInfo culture)
		{
			this._culture = culture;
			this._optionsStack = new List<RegexOptions>();
			this._caps = new Hashtable();
		}

		// Token: 0x06000600 RID: 1536 RVA: 0x0001E816 File Offset: 0x0001CA16
		internal void SetPattern(string Re)
		{
			if (Re == null)
			{
				Re = string.Empty;
			}
			this._pattern = Re;
			this._currentPos = 0;
		}

		// Token: 0x06000601 RID: 1537 RVA: 0x0001E830 File Offset: 0x0001CA30
		internal void Reset(RegexOptions topopts)
		{
			this._currentPos = 0;
			this._autocap = 1;
			this._ignoreNextParen = false;
			if (this._optionsStack.Count > 0)
			{
				this._optionsStack.RemoveRange(0, this._optionsStack.Count - 1);
			}
			this._options = topopts;
			this._stack = null;
		}

		// Token: 0x06000602 RID: 1538 RVA: 0x0001E888 File Offset: 0x0001CA88
		internal RegexNode ScanRegex()
		{
			bool flag = false;
			this.StartGroup(new RegexNode(28, this._options, 0, -1));
			while (this.CharsRight() > 0)
			{
				bool flag2 = flag;
				flag = false;
				this.ScanBlank();
				int num = this.Textpos();
				char c;
				if (this.UseOptionX())
				{
					while (this.CharsRight() > 0)
					{
						if (RegexParser.IsStopperX(c = this.RightChar()))
						{
							if (c != '{')
							{
								break;
							}
							if (this.IsTrueQuantifier())
							{
								break;
							}
						}
						this.MoveRight();
					}
				}
				else
				{
					while (this.CharsRight() > 0 && (!RegexParser.IsSpecial(c = this.RightChar()) || (c == '{' && !this.IsTrueQuantifier())))
					{
						this.MoveRight();
					}
				}
				int num2 = this.Textpos();
				this.ScanBlank();
				if (this.CharsRight() == 0)
				{
					c = '!';
				}
				else if (RegexParser.IsSpecial(c = this.RightChar()))
				{
					flag = RegexParser.IsQuantifier(c);
					this.MoveRight();
				}
				else
				{
					c = ' ';
				}
				if (num < num2)
				{
					int num3 = num2 - num - (flag ? 1 : 0);
					flag2 = false;
					if (num3 > 0)
					{
						this.AddConcatenate(num, num3, false);
					}
					if (flag)
					{
						this.AddUnitOne(this.CharAt(num2 - 1));
					}
				}
				if (c <= '?')
				{
					switch (c)
					{
					case ' ':
						continue;
					case '!':
						goto IL_437;
					case '"':
					case '#':
					case '%':
					case '&':
					case '\'':
					case ',':
					case '-':
						goto IL_2B7;
					case '$':
						this.AddUnitType(this.UseOptionM() ? 15 : 20);
						break;
					case '(':
					{
						this.PushOptions();
						RegexNode openGroup;
						if ((openGroup = this.ScanGroupOpen()) == null)
						{
							this.PopKeepOptions();
							continue;
						}
						this.PushGroup();
						this.StartGroup(openGroup);
						continue;
					}
					case ')':
						if (this.EmptyStack())
						{
							throw this.MakeException(SR.GetString("Too many )'s."));
						}
						this.AddGroup();
						this.PopGroup();
						this.PopOptions();
						if (this.Unit() == null)
						{
							continue;
						}
						break;
					case '*':
					case '+':
						goto IL_277;
					case '.':
						if (this.UseOptionS())
						{
							this.AddUnitSet("\0\u0001\0\0");
						}
						else
						{
							this.AddUnitNotone('\n');
						}
						break;
					default:
						if (c != '?')
						{
							goto IL_2B7;
						}
						goto IL_277;
					}
				}
				else
				{
					switch (c)
					{
					case '[':
						this.AddUnitSet(this.ScanCharClass(this.UseOptionI()).ToStringClass());
						break;
					case '\\':
						this.AddUnitNode(this.ScanBackslash());
						break;
					case ']':
						goto IL_2B7;
					case '^':
						this.AddUnitType(this.UseOptionM() ? 14 : 18);
						break;
					default:
						if (c == '{')
						{
							goto IL_277;
						}
						if (c != '|')
						{
							goto IL_2B7;
						}
						this.AddAlternate();
						continue;
					}
				}
				IL_2C8:
				this.ScanBlank();
				if (this.CharsRight() == 0 || !(flag = this.IsTrueQuantifier()))
				{
					this.AddConcatenate();
					continue;
				}
				c = this.MoveRightGetChar();
				while (this.Unit() != null)
				{
					int num4;
					int num5;
					if (c <= '+')
					{
						if (c != '*')
						{
							if (c != '+')
							{
								goto IL_3C6;
							}
							num4 = 1;
							num5 = int.MaxValue;
						}
						else
						{
							num4 = 0;
							num5 = int.MaxValue;
						}
					}
					else if (c != '?')
					{
						if (c != '{')
						{
							goto IL_3C6;
						}
						num = this.Textpos();
						num4 = (num5 = this.ScanDecimal());
						if (num < this.Textpos() && this.CharsRight() > 0 && this.RightChar() == ',')
						{
							this.MoveRight();
							if (this.CharsRight() == 0 || this.RightChar() == '}')
							{
								num5 = int.MaxValue;
							}
							else
							{
								num5 = this.ScanDecimal();
							}
						}
						if (num == this.Textpos() || this.CharsRight() == 0 || this.MoveRightGetChar() != '}')
						{
							this.AddConcatenate();
							this.Textto(num - 1);
							break;
						}
					}
					else
					{
						num4 = 0;
						num5 = 1;
					}
					this.ScanBlank();
					bool lazy;
					if (this.CharsRight() == 0 || this.RightChar() != '?')
					{
						lazy = false;
					}
					else
					{
						this.MoveRight();
						lazy = true;
					}
					if (num4 > num5)
					{
						throw this.MakeException(SR.GetString("Illegal {x,y} with x > y."));
					}
					this.AddConcatenate(lazy, num4, num5);
					continue;
					IL_3C6:
					throw this.MakeException(SR.GetString("Internal error in ScanRegex."));
				}
				continue;
				IL_277:
				if (this.Unit() == null)
				{
					throw this.MakeException(flag2 ? SR.GetString("Nested quantifier {0}.", new object[]
					{
						c.ToString()
					}) : SR.GetString("Quantifier {x,y} following nothing."));
				}
				this.MoveLeft();
				goto IL_2C8;
				IL_2B7:
				throw this.MakeException(SR.GetString("Internal error in ScanRegex."));
			}
			IL_437:
			if (!this.EmptyStack())
			{
				throw this.MakeException(SR.GetString("Not enough )'s."));
			}
			this.AddGroup();
			return this.Unit();
		}

		// Token: 0x06000603 RID: 1539 RVA: 0x0001ECF4 File Offset: 0x0001CEF4
		internal RegexNode ScanReplacement()
		{
			this._concatenation = new RegexNode(25, this._options);
			for (;;)
			{
				int num = this.CharsRight();
				if (num == 0)
				{
					break;
				}
				int num2 = this.Textpos();
				while (num > 0 && this.RightChar() != '$')
				{
					this.MoveRight();
					num--;
				}
				this.AddConcatenate(num2, this.Textpos() - num2, true);
				if (num > 0)
				{
					if (this.MoveRightGetChar() == '$')
					{
						this.AddUnitNode(this.ScanDollar());
					}
					this.AddConcatenate();
				}
			}
			return this._concatenation;
		}

		// Token: 0x06000604 RID: 1540 RVA: 0x0001ED77 File Offset: 0x0001CF77
		internal RegexCharClass ScanCharClass(bool caseInsensitive)
		{
			return this.ScanCharClass(caseInsensitive, false);
		}

		// Token: 0x06000605 RID: 1541 RVA: 0x0001ED84 File Offset: 0x0001CF84
		internal RegexCharClass ScanCharClass(bool caseInsensitive, bool scanOnly)
		{
			char c = '\0';
			bool flag = false;
			bool flag2 = true;
			bool flag3 = false;
			RegexCharClass regexCharClass = scanOnly ? null : new RegexCharClass();
			if (this.CharsRight() > 0 && this.RightChar() == '^')
			{
				this.MoveRight();
				if (!scanOnly)
				{
					regexCharClass.Negate = true;
				}
			}
			while (this.CharsRight() > 0)
			{
				bool flag4 = false;
				char c2 = this.MoveRightGetChar();
				if (c2 == ']')
				{
					if (!flag2)
					{
						flag3 = true;
						break;
					}
					goto IL_28B;
				}
				else
				{
					if (c2 == '\\' && this.CharsRight() > 0)
					{
						char c3;
						c2 = (c3 = this.MoveRightGetChar());
						if (c3 <= 'S')
						{
							if (c3 <= 'D')
							{
								if (c3 != '-')
								{
									if (c3 != 'D')
									{
										goto IL_224;
									}
								}
								else
								{
									if (!scanOnly)
									{
										regexCharClass.AddRange(c2, c2);
										goto IL_3AA;
									}
									goto IL_3AA;
								}
							}
							else
							{
								if (c3 == 'P')
								{
									goto IL_1BC;
								}
								if (c3 != 'S')
								{
									goto IL_224;
								}
								goto IL_13A;
							}
						}
						else
						{
							if (c3 <= 'd')
							{
								if (c3 != 'W')
								{
									if (c3 != 'd')
									{
										goto IL_224;
									}
									goto IL_F3;
								}
							}
							else
							{
								if (c3 == 'p')
								{
									goto IL_1BC;
								}
								if (c3 == 's')
								{
									goto IL_13A;
								}
								if (c3 != 'w')
								{
									goto IL_224;
								}
							}
							if (scanOnly)
							{
								goto IL_3AA;
							}
							if (flag)
							{
								throw this.MakeException(SR.GetString("Cannot include class \\{0} in character range.", new object[]
								{
									c2.ToString()
								}));
							}
							regexCharClass.AddWord(this.UseOptionE(), c2 == 'W');
							goto IL_3AA;
						}
						IL_F3:
						if (scanOnly)
						{
							goto IL_3AA;
						}
						if (flag)
						{
							throw this.MakeException(SR.GetString("Cannot include class \\{0} in character range.", new object[]
							{
								c2.ToString()
							}));
						}
						regexCharClass.AddDigit(this.UseOptionE(), c2 == 'D', this._pattern);
						goto IL_3AA;
						IL_13A:
						if (scanOnly)
						{
							goto IL_3AA;
						}
						if (flag)
						{
							throw this.MakeException(SR.GetString("Cannot include class \\{0} in character range.", new object[]
							{
								c2.ToString()
							}));
						}
						regexCharClass.AddSpace(this.UseOptionE(), c2 == 'S');
						goto IL_3AA;
						IL_1BC:
						if (scanOnly)
						{
							this.ParseProperty();
							goto IL_3AA;
						}
						if (flag)
						{
							throw this.MakeException(SR.GetString("Cannot include class \\{0} in character range.", new object[]
							{
								c2.ToString()
							}));
						}
						regexCharClass.AddCategoryFromName(this.ParseProperty(), c2 != 'p', caseInsensitive, this._pattern);
						goto IL_3AA;
						IL_224:
						this.MoveLeft();
						c2 = this.ScanCharEscape();
						flag4 = true;
						goto IL_28B;
					}
					if (c2 != '[' || this.CharsRight() <= 0 || this.RightChar() != ':' || flag)
					{
						goto IL_28B;
					}
					int pos = this.Textpos();
					this.MoveRight();
					this.ScanCapname();
					if (this.CharsRight() < 2 || this.MoveRightGetChar() != ':' || this.MoveRightGetChar() != ']')
					{
						this.Textto(pos);
						goto IL_28B;
					}
					goto IL_28B;
				}
				IL_3AA:
				flag2 = false;
				continue;
				IL_28B:
				if (flag)
				{
					flag = false;
					if (scanOnly)
					{
						goto IL_3AA;
					}
					if (c2 == '[' && !flag4 && !flag2)
					{
						regexCharClass.AddChar(c);
						regexCharClass.AddSubtraction(this.ScanCharClass(caseInsensitive, false));
						if (this.CharsRight() > 0 && this.RightChar() != ']')
						{
							throw this.MakeException(SR.GetString("A subtraction must be the last element in a character class."));
						}
						goto IL_3AA;
					}
					else
					{
						if (c > c2)
						{
							throw this.MakeException(SR.GetString("[x-y] range in reverse order."));
						}
						regexCharClass.AddRange(c, c2);
						goto IL_3AA;
					}
				}
				else
				{
					if (this.CharsRight() >= 2 && this.RightChar() == '-' && this.RightChar(1) != ']')
					{
						c = c2;
						flag = true;
						this.MoveRight();
						goto IL_3AA;
					}
					if (this.CharsRight() >= 1 && c2 == '-' && !flag4 && this.RightChar() == '[' && !flag2)
					{
						if (scanOnly)
						{
							this.MoveRight(1);
							this.ScanCharClass(caseInsensitive, true);
							goto IL_3AA;
						}
						this.MoveRight(1);
						regexCharClass.AddSubtraction(this.ScanCharClass(caseInsensitive, false));
						if (this.CharsRight() > 0 && this.RightChar() != ']')
						{
							throw this.MakeException(SR.GetString("A subtraction must be the last element in a character class."));
						}
						goto IL_3AA;
					}
					else
					{
						if (!scanOnly)
						{
							regexCharClass.AddRange(c2, c2);
							goto IL_3AA;
						}
						goto IL_3AA;
					}
				}
			}
			if (!flag3)
			{
				throw this.MakeException(SR.GetString("Unterminated [] set."));
			}
			if (!scanOnly && caseInsensitive)
			{
				regexCharClass.AddLowercase(this._culture);
			}
			return regexCharClass;
		}

		// Token: 0x06000606 RID: 1542 RVA: 0x0001F178 File Offset: 0x0001D378
		internal RegexNode ScanGroupOpen()
		{
			char c = '>';
			if (this.CharsRight() != 0 && this.RightChar() == '?' && (this.RightChar() != '?' || this.CharsRight() <= 1 || this.RightChar(1) != ')'))
			{
				this.MoveRight();
				if (this.CharsRight() != 0)
				{
					char c2 = this.MoveRightGetChar();
					int type;
					char c3;
					if (c2 <= '\'')
					{
						if (c2 == '!')
						{
							this._options &= ~RegexOptions.RightToLeft;
							type = 31;
							goto IL_551;
						}
						if (c2 != '\'')
						{
							goto IL_527;
						}
						c = '\'';
					}
					else if (c2 != '(')
					{
						switch (c2)
						{
						case ':':
							type = 29;
							goto IL_551;
						case ';':
							goto IL_527;
						case '<':
							break;
						case '=':
							this._options &= ~RegexOptions.RightToLeft;
							type = 30;
							goto IL_551;
						case '>':
							type = 32;
							goto IL_551;
						default:
							goto IL_527;
						}
					}
					else
					{
						int num = this.Textpos();
						if (this.CharsRight() > 0)
						{
							c3 = this.RightChar();
							if (c3 >= '0' && c3 <= '9')
							{
								int num2 = this.ScanDecimal();
								if (this.CharsRight() <= 0 || this.MoveRightGetChar() != ')')
								{
									throw this.MakeException(SR.GetString("(?({0}) ) malformed.", new object[]
									{
										num2.ToString(CultureInfo.CurrentCulture)
									}));
								}
								if (this.IsCaptureSlot(num2))
								{
									return new RegexNode(33, this._options, num2);
								}
								throw this.MakeException(SR.GetString("(?({0}) ) reference to undefined group.", new object[]
								{
									num2.ToString(CultureInfo.CurrentCulture)
								}));
							}
							else if (RegexCharClass.IsWordChar(c3))
							{
								string capname = this.ScanCapname();
								if (this.IsCaptureName(capname) && this.CharsRight() > 0 && this.MoveRightGetChar() == ')')
								{
									return new RegexNode(33, this._options, this.CaptureSlotFromName(capname));
								}
							}
						}
						type = 34;
						this.Textto(num - 1);
						this._ignoreNextParen = true;
						int num3 = this.CharsRight();
						if (num3 < 3 || this.RightChar(1) != '?')
						{
							goto IL_551;
						}
						char c4 = this.RightChar(2);
						if (c4 == '#')
						{
							throw this.MakeException(SR.GetString("Alternation conditions cannot be comments."));
						}
						if (c4 == '\'')
						{
							throw this.MakeException(SR.GetString("Alternation conditions do not capture and cannot be named."));
						}
						if (num3 >= 4 && c4 == '<' && this.RightChar(3) != '!' && this.RightChar(3) != '=')
						{
							throw this.MakeException(SR.GetString("Alternation conditions do not capture and cannot be named."));
						}
						goto IL_551;
					}
					if (this.CharsRight() == 0)
					{
						goto IL_55E;
					}
					c3 = (c2 = this.MoveRightGetChar());
					if (c2 != '!')
					{
						if (c2 == '=')
						{
							if (c != '\'')
							{
								this._options |= RegexOptions.RightToLeft;
								type = 30;
								goto IL_551;
							}
							goto IL_55E;
						}
						else
						{
							this.MoveLeft();
							int num4 = -1;
							int num5 = -1;
							bool flag = false;
							if (c3 >= '0' && c3 <= '9')
							{
								num4 = this.ScanDecimal();
								if (!this.IsCaptureSlot(num4))
								{
									num4 = -1;
								}
								if (this.CharsRight() > 0 && this.RightChar() != c && this.RightChar() != '-')
								{
									throw this.MakeException(SR.GetString("Invalid group name: Group names must begin with a word character."));
								}
								if (num4 == 0)
								{
									throw this.MakeException(SR.GetString("Capture number cannot be zero."));
								}
							}
							else if (RegexCharClass.IsWordChar(c3))
							{
								string capname2 = this.ScanCapname();
								if (this.IsCaptureName(capname2))
								{
									num4 = this.CaptureSlotFromName(capname2);
								}
								if (this.CharsRight() > 0 && this.RightChar() != c && this.RightChar() != '-')
								{
									throw this.MakeException(SR.GetString("Invalid group name: Group names must begin with a word character."));
								}
							}
							else
							{
								if (c3 != '-')
								{
									throw this.MakeException(SR.GetString("Invalid group name: Group names must begin with a word character."));
								}
								flag = true;
							}
							if ((num4 != -1 || flag) && this.CharsRight() > 0 && this.RightChar() == '-')
							{
								this.MoveRight();
								c3 = this.RightChar();
								if (c3 >= '0' && c3 <= '9')
								{
									num5 = this.ScanDecimal();
									if (!this.IsCaptureSlot(num5))
									{
										throw this.MakeException(SR.GetString("Reference to undefined group number {0}.", new object[]
										{
											num5
										}));
									}
									if (this.CharsRight() > 0 && this.RightChar() != c)
									{
										throw this.MakeException(SR.GetString("Invalid group name: Group names must begin with a word character."));
									}
								}
								else
								{
									if (!RegexCharClass.IsWordChar(c3))
									{
										throw this.MakeException(SR.GetString("Invalid group name: Group names must begin with a word character."));
									}
									string text = this.ScanCapname();
									if (!this.IsCaptureName(text))
									{
										throw this.MakeException(SR.GetString("Reference to undefined group name {0}.", new object[]
										{
											text
										}));
									}
									num5 = this.CaptureSlotFromName(text);
									if (this.CharsRight() > 0 && this.RightChar() != c)
									{
										throw this.MakeException(SR.GetString("Invalid group name: Group names must begin with a word character."));
									}
								}
							}
							if ((num4 != -1 || num5 != -1) && this.CharsRight() > 0 && this.MoveRightGetChar() == c)
							{
								return new RegexNode(28, this._options, num4, num5);
							}
							goto IL_55E;
						}
					}
					else
					{
						if (c != '\'')
						{
							this._options |= RegexOptions.RightToLeft;
							type = 31;
							goto IL_551;
						}
						goto IL_55E;
					}
					IL_527:
					this.MoveLeft();
					type = 29;
					this.ScanOptions();
					if (this.CharsRight() == 0)
					{
						goto IL_55E;
					}
					if ((c3 = this.MoveRightGetChar()) == ')')
					{
						return null;
					}
					if (c3 != ':')
					{
						goto IL_55E;
					}
					IL_551:
					return new RegexNode(type, this._options);
				}
				IL_55E:
				throw this.MakeException(SR.GetString("Unrecognized grouping construct."));
			}
			if (this.UseOptionN() || this._ignoreNextParen)
			{
				this._ignoreNextParen = false;
				return new RegexNode(29, this._options);
			}
			int type2 = 28;
			RegexOptions options = this._options;
			int autocap = this._autocap;
			this._autocap = autocap + 1;
			return new RegexNode(type2, options, autocap, -1);
		}

		// Token: 0x06000607 RID: 1543 RVA: 0x0001F6F4 File Offset: 0x0001D8F4
		internal void ScanBlank()
		{
			if (this.UseOptionX())
			{
				for (;;)
				{
					if (this.CharsRight() <= 0 || !RegexParser.IsSpace(this.RightChar()))
					{
						if (this.CharsRight() == 0)
						{
							return;
						}
						if (this.RightChar() == '#')
						{
							while (this.CharsRight() > 0)
							{
								if (this.RightChar() == '\n')
								{
									break;
								}
								this.MoveRight();
							}
						}
						else
						{
							if (this.CharsRight() < 3 || this.RightChar(2) != '#' || this.RightChar(1) != '?' || this.RightChar() != '(')
							{
								return;
							}
							while (this.CharsRight() > 0 && this.RightChar() != ')')
							{
								this.MoveRight();
							}
							if (this.CharsRight() == 0)
							{
								break;
							}
							this.MoveRight();
						}
					}
					else
					{
						this.MoveRight();
					}
				}
				throw this.MakeException(SR.GetString("Unterminated (?#...) comment."));
			}
			while (this.CharsRight() >= 3 && this.RightChar(2) == '#' && this.RightChar(1) == '?' && this.RightChar() == '(')
			{
				while (this.CharsRight() > 0 && this.RightChar() != ')')
				{
					this.MoveRight();
				}
				if (this.CharsRight() == 0)
				{
					throw this.MakeException(SR.GetString("Unterminated (?#...) comment."));
				}
				this.MoveRight();
			}
		}

		// Token: 0x06000608 RID: 1544 RVA: 0x0001F834 File Offset: 0x0001DA34
		internal RegexNode ScanBackslash()
		{
			if (this.CharsRight() == 0)
			{
				throw this.MakeException(SR.GetString("Illegal \\ at end of pattern."));
			}
			char c2;
			char c = c2 = this.RightChar();
			if (c2 <= 'Z')
			{
				if (c2 <= 'P')
				{
					switch (c2)
					{
					case 'A':
					case 'B':
					case 'G':
						break;
					case 'C':
					case 'E':
					case 'F':
						goto IL_251;
					case 'D':
						this.MoveRight();
						if (this.UseOptionE())
						{
							return new RegexNode(11, this._options, "\u0001\u0002\00:");
						}
						return new RegexNode(11, this._options, RegexCharClass.NotDigitClass);
					default:
						if (c2 != 'P')
						{
							goto IL_251;
						}
						goto IL_1FD;
					}
				}
				else if (c2 != 'S')
				{
					if (c2 != 'W')
					{
						if (c2 != 'Z')
						{
							goto IL_251;
						}
					}
					else
					{
						this.MoveRight();
						if (this.UseOptionE())
						{
							return new RegexNode(11, this._options, "\u0001\n\00:A[_`a{İı");
						}
						return new RegexNode(11, this._options, RegexCharClass.NotWordClass);
					}
				}
				else
				{
					this.MoveRight();
					if (this.UseOptionE())
					{
						return new RegexNode(11, this._options, "\u0001\u0004\0\t\u000e !");
					}
					return new RegexNode(11, this._options, RegexCharClass.NotSpaceClass);
				}
			}
			else if (c2 <= 'p')
			{
				if (c2 != 'b')
				{
					if (c2 != 'd')
					{
						if (c2 != 'p')
						{
							goto IL_251;
						}
						goto IL_1FD;
					}
					else
					{
						this.MoveRight();
						if (this.UseOptionE())
						{
							return new RegexNode(11, this._options, "\0\u0002\00:");
						}
						return new RegexNode(11, this._options, RegexCharClass.DigitClass);
					}
				}
			}
			else if (c2 != 's')
			{
				if (c2 != 'w')
				{
					if (c2 != 'z')
					{
						goto IL_251;
					}
				}
				else
				{
					this.MoveRight();
					if (this.UseOptionE())
					{
						return new RegexNode(11, this._options, "\0\n\00:A[_`a{İı");
					}
					return new RegexNode(11, this._options, RegexCharClass.WordClass);
				}
			}
			else
			{
				this.MoveRight();
				if (this.UseOptionE())
				{
					return new RegexNode(11, this._options, "\0\u0004\0\t\u000e !");
				}
				return new RegexNode(11, this._options, RegexCharClass.SpaceClass);
			}
			this.MoveRight();
			return new RegexNode(this.TypeFromCode(c), this._options);
			IL_1FD:
			this.MoveRight();
			RegexCharClass regexCharClass = new RegexCharClass();
			regexCharClass.AddCategoryFromName(this.ParseProperty(), c != 'p', this.UseOptionI(), this._pattern);
			if (this.UseOptionI())
			{
				regexCharClass.AddLowercase(this._culture);
			}
			return new RegexNode(11, this._options, regexCharClass.ToStringClass());
			IL_251:
			return this.ScanBasicBackslash();
		}

		// Token: 0x06000609 RID: 1545 RVA: 0x0001FA98 File Offset: 0x0001DC98
		internal RegexNode ScanBasicBackslash()
		{
			if (this.CharsRight() == 0)
			{
				throw this.MakeException(SR.GetString("Illegal \\ at end of pattern."));
			}
			bool flag = false;
			char c = '\0';
			int pos = this.Textpos();
			char c2 = this.RightChar();
			if (c2 == 'k')
			{
				if (this.CharsRight() >= 2)
				{
					this.MoveRight();
					c2 = this.MoveRightGetChar();
					if (c2 == '<' || c2 == '\'')
					{
						flag = true;
						c = ((c2 == '\'') ? '\'' : '>');
					}
				}
				if (!flag || this.CharsRight() <= 0)
				{
					throw this.MakeException(SR.GetString("Malformed \\k<...> named back reference."));
				}
				c2 = this.RightChar();
			}
			else if ((c2 == '<' || c2 == '\'') && this.CharsRight() > 1)
			{
				flag = true;
				c = ((c2 == '\'') ? '\'' : '>');
				this.MoveRight();
				c2 = this.RightChar();
			}
			if (flag && c2 >= '0' && c2 <= '9')
			{
				int num = this.ScanDecimal();
				if (this.CharsRight() > 0 && this.MoveRightGetChar() == c)
				{
					if (this.IsCaptureSlot(num))
					{
						return new RegexNode(13, this._options, num);
					}
					throw this.MakeException(SR.GetString("Reference to undefined group number {0}.", new object[]
					{
						num.ToString(CultureInfo.CurrentCulture)
					}));
				}
			}
			else if (!flag && c2 >= '1' && c2 <= '9')
			{
				if (this.UseOptionE())
				{
					int num2 = -1;
					int i = (int)(c2 - '0');
					int num3 = this.Textpos() - 1;
					while (i <= this._captop)
					{
						if (this.IsCaptureSlot(i) && (this._caps == null || (int)this._caps[i] < num3))
						{
							num2 = i;
						}
						this.MoveRight();
						if (this.CharsRight() == 0 || (c2 = this.RightChar()) < '0' || c2 > '9')
						{
							break;
						}
						i = i * 10 + (int)(c2 - '0');
					}
					if (num2 >= 0)
					{
						return new RegexNode(13, this._options, num2);
					}
				}
				else
				{
					int num4 = this.ScanDecimal();
					if (this.IsCaptureSlot(num4))
					{
						return new RegexNode(13, this._options, num4);
					}
					if (num4 <= 9)
					{
						throw this.MakeException(SR.GetString("Reference to undefined group number {0}.", new object[]
						{
							num4.ToString(CultureInfo.CurrentCulture)
						}));
					}
				}
			}
			else if (flag && RegexCharClass.IsWordChar(c2))
			{
				string text = this.ScanCapname();
				if (this.CharsRight() > 0 && this.MoveRightGetChar() == c)
				{
					if (this.IsCaptureName(text))
					{
						return new RegexNode(13, this._options, this.CaptureSlotFromName(text));
					}
					throw this.MakeException(SR.GetString("Reference to undefined group name {0}.", new object[]
					{
						text
					}));
				}
			}
			this.Textto(pos);
			c2 = this.ScanCharEscape();
			if (this.UseOptionI())
			{
				c2 = char.ToLower(c2, this._culture);
			}
			return new RegexNode(9, this._options, c2);
		}

		// Token: 0x0600060A RID: 1546 RVA: 0x0001FD5C File Offset: 0x0001DF5C
		internal RegexNode ScanDollar()
		{
			if (this.CharsRight() == 0)
			{
				return new RegexNode(9, this._options, '$');
			}
			char c = this.RightChar();
			int num = this.Textpos();
			int pos = num;
			bool flag;
			if (c == '{' && this.CharsRight() > 1)
			{
				flag = true;
				this.MoveRight();
				c = this.RightChar();
			}
			else
			{
				flag = false;
			}
			if (c >= '0' && c <= '9')
			{
				if (!flag && this.UseOptionE())
				{
					int num2 = -1;
					int num3 = (int)(c - '0');
					this.MoveRight();
					if (this.IsCaptureSlot(num3))
					{
						num2 = num3;
						pos = this.Textpos();
					}
					while (this.CharsRight() > 0 && (c = this.RightChar()) >= '0' && c <= '9')
					{
						int num4 = (int)(c - '0');
						if (num3 > 214748364 || (num3 == 214748364 && num4 > 7))
						{
							throw this.MakeException(SR.GetString("Capture group numbers must be less than or equal to Int32.MaxValue."));
						}
						num3 = num3 * 10 + num4;
						this.MoveRight();
						if (this.IsCaptureSlot(num3))
						{
							num2 = num3;
							pos = this.Textpos();
						}
					}
					this.Textto(pos);
					if (num2 >= 0)
					{
						return new RegexNode(13, this._options, num2);
					}
				}
				else
				{
					int num5 = this.ScanDecimal();
					if ((!flag || (this.CharsRight() > 0 && this.MoveRightGetChar() == '}')) && this.IsCaptureSlot(num5))
					{
						return new RegexNode(13, this._options, num5);
					}
				}
			}
			else if (flag && RegexCharClass.IsWordChar(c))
			{
				string capname = this.ScanCapname();
				if (this.CharsRight() > 0 && this.MoveRightGetChar() == '}' && this.IsCaptureName(capname))
				{
					return new RegexNode(13, this._options, this.CaptureSlotFromName(capname));
				}
			}
			else if (!flag)
			{
				int num6 = 1;
				if (c <= '+')
				{
					switch (c)
					{
					case '$':
						this.MoveRight();
						return new RegexNode(9, this._options, '$');
					case '%':
						break;
					case '&':
						num6 = 0;
						break;
					case '\'':
						num6 = -2;
						break;
					default:
						if (c == '+')
						{
							num6 = -3;
						}
						break;
					}
				}
				else if (c != '_')
				{
					if (c == '`')
					{
						num6 = -1;
					}
				}
				else
				{
					num6 = -4;
				}
				if (num6 != 1)
				{
					this.MoveRight();
					return new RegexNode(13, this._options, num6);
				}
			}
			this.Textto(num);
			return new RegexNode(9, this._options, '$');
		}

		// Token: 0x0600060B RID: 1547 RVA: 0x0001FFB0 File Offset: 0x0001E1B0
		internal string ScanCapname()
		{
			int num = this.Textpos();
			while (this.CharsRight() > 0)
			{
				if (!RegexCharClass.IsWordChar(this.MoveRightGetChar()))
				{
					this.MoveLeft();
					break;
				}
			}
			return this._pattern.Substring(num, this.Textpos() - num);
		}

		// Token: 0x0600060C RID: 1548 RVA: 0x0001FFF8 File Offset: 0x0001E1F8
		internal char ScanOctal()
		{
			int num = 3;
			if (num > this.CharsRight())
			{
				num = this.CharsRight();
			}
			int num2 = 0;
			int num3;
			while (num > 0 && (num3 = (int)(this.RightChar() - '0')) <= 7)
			{
				this.MoveRight();
				num2 *= 8;
				num2 += num3;
				if (this.UseOptionE() && num2 >= 32)
				{
					break;
				}
				num--;
			}
			num2 &= 255;
			return (char)num2;
		}

		// Token: 0x0600060D RID: 1549 RVA: 0x00020058 File Offset: 0x0001E258
		internal int ScanDecimal()
		{
			int num = 0;
			int num2;
			while (this.CharsRight() > 0 && (num2 = (int)((ushort)(this.RightChar() - '0'))) <= 9)
			{
				this.MoveRight();
				if (num > 214748364 || (num == 214748364 && num2 > 7))
				{
					throw this.MakeException(SR.GetString("Capture group numbers must be less than or equal to Int32.MaxValue."));
				}
				num *= 10;
				num += num2;
			}
			return num;
		}

		// Token: 0x0600060E RID: 1550 RVA: 0x000200B8 File Offset: 0x0001E2B8
		internal char ScanHex(int c)
		{
			int num = 0;
			if (this.CharsRight() >= c)
			{
				int num2;
				while (c > 0 && (num2 = RegexParser.HexDigit(this.MoveRightGetChar())) >= 0)
				{
					num *= 16;
					num += num2;
					c--;
				}
			}
			if (c > 0)
			{
				throw this.MakeException(SR.GetString("Insufficient hexadecimal digits."));
			}
			return (char)num;
		}

		// Token: 0x0600060F RID: 1551 RVA: 0x0002010C File Offset: 0x0001E30C
		internal static int HexDigit(char ch)
		{
			int num;
			if ((num = (int)(ch - '0')) <= 9)
			{
				return num;
			}
			if ((num = (int)(ch - 'a')) <= 5)
			{
				return num + 10;
			}
			if ((num = (int)(ch - 'A')) <= 5)
			{
				return num + 10;
			}
			return -1;
		}

		// Token: 0x06000610 RID: 1552 RVA: 0x00020144 File Offset: 0x0001E344
		internal char ScanControl()
		{
			if (this.CharsRight() <= 0)
			{
				throw this.MakeException(SR.GetString("Missing control character."));
			}
			char c = this.MoveRightGetChar();
			if (c >= 'a' && c <= 'z')
			{
				c -= ' ';
			}
			if ((c -= '@') < ' ')
			{
				return c;
			}
			throw this.MakeException(SR.GetString("Unrecognized control character."));
		}

		// Token: 0x06000611 RID: 1553 RVA: 0x0002019F File Offset: 0x0001E39F
		internal bool IsOnlyTopOption(RegexOptions option)
		{
			return option == RegexOptions.RightToLeft || option == RegexOptions.CultureInvariant || option == RegexOptions.ECMAScript;
		}

		// Token: 0x06000612 RID: 1554 RVA: 0x000201B8 File Offset: 0x0001E3B8
		internal void ScanOptions()
		{
			bool flag = false;
			while (this.CharsRight() > 0)
			{
				char c = this.RightChar();
				if (c == '-')
				{
					flag = true;
				}
				else if (c == '+')
				{
					flag = false;
				}
				else
				{
					RegexOptions regexOptions = RegexParser.OptionFromCode(c);
					if (regexOptions == RegexOptions.None || this.IsOnlyTopOption(regexOptions))
					{
						return;
					}
					if (flag)
					{
						this._options &= ~regexOptions;
					}
					else
					{
						this._options |= regexOptions;
					}
				}
				this.MoveRight();
			}
		}

		// Token: 0x06000613 RID: 1555 RVA: 0x00020228 File Offset: 0x0001E428
		internal char ScanCharEscape()
		{
			char c = this.MoveRightGetChar();
			if (c >= '0' && c <= '7')
			{
				this.MoveLeft();
				return this.ScanOctal();
			}
			switch (c)
			{
			case 'a':
				return '\a';
			case 'b':
				return '\b';
			case 'c':
				return this.ScanControl();
			case 'd':
				break;
			case 'e':
				return '\u001b';
			case 'f':
				return '\f';
			default:
				switch (c)
				{
				case 'n':
					return '\n';
				case 'r':
					return '\r';
				case 't':
					return '\t';
				case 'u':
					return this.ScanHex(4);
				case 'v':
					return '\v';
				case 'x':
					return this.ScanHex(2);
				}
				break;
			}
			if (!this.UseOptionE() && RegexCharClass.IsWordChar(c))
			{
				throw this.MakeException(SR.GetString("Unrecognized escape sequence \\{0}.", new object[]
				{
					c.ToString()
				}));
			}
			return c;
		}

		// Token: 0x06000614 RID: 1556 RVA: 0x0002030C File Offset: 0x0001E50C
		internal string ParseProperty()
		{
			if (this.CharsRight() < 3)
			{
				throw this.MakeException(SR.GetString("Incomplete \\p{X} character escape."));
			}
			char c = this.MoveRightGetChar();
			if (c != '{')
			{
				throw this.MakeException(SR.GetString("Malformed \\p{X} character escape."));
			}
			int num = this.Textpos();
			while (this.CharsRight() > 0)
			{
				c = this.MoveRightGetChar();
				if (!RegexCharClass.IsWordChar(c) && c != '-')
				{
					this.MoveLeft();
					break;
				}
			}
			string result = this._pattern.Substring(num, this.Textpos() - num);
			if (this.CharsRight() == 0 || this.MoveRightGetChar() != '}')
			{
				throw this.MakeException(SR.GetString("Incomplete \\p{X} character escape."));
			}
			return result;
		}

		// Token: 0x06000615 RID: 1557 RVA: 0x000203B8 File Offset: 0x0001E5B8
		internal int TypeFromCode(char ch)
		{
			if (ch <= 'G')
			{
				if (ch == 'A')
				{
					return 18;
				}
				if (ch != 'B')
				{
					if (ch == 'G')
					{
						return 19;
					}
				}
				else
				{
					if (!this.UseOptionE())
					{
						return 17;
					}
					return 42;
				}
			}
			else
			{
				if (ch == 'Z')
				{
					return 20;
				}
				if (ch != 'b')
				{
					if (ch == 'z')
					{
						return 21;
					}
				}
				else
				{
					if (!this.UseOptionE())
					{
						return 16;
					}
					return 41;
				}
			}
			return 22;
		}

		// Token: 0x06000616 RID: 1558 RVA: 0x00020418 File Offset: 0x0001E618
		internal static RegexOptions OptionFromCode(char ch)
		{
			if (ch >= 'A' && ch <= 'Z')
			{
				ch += ' ';
			}
			if (ch <= 'i')
			{
				if (ch == 'e')
				{
					return RegexOptions.ECMAScript;
				}
				if (ch == 'i')
				{
					return RegexOptions.IgnoreCase;
				}
			}
			else
			{
				switch (ch)
				{
				case 'm':
					return RegexOptions.Multiline;
				case 'n':
					return RegexOptions.ExplicitCapture;
				case 'o':
				case 'p':
				case 'q':
					break;
				case 'r':
					return RegexOptions.RightToLeft;
				case 's':
					return RegexOptions.Singleline;
				default:
					if (ch == 'x')
					{
						return RegexOptions.IgnorePatternWhitespace;
					}
					break;
				}
			}
			return RegexOptions.None;
		}

		// Token: 0x06000617 RID: 1559 RVA: 0x0002048C File Offset: 0x0001E68C
		internal void CountCaptures()
		{
			this.NoteCaptureSlot(0, 0);
			this._autocap = 1;
			while (this.CharsRight() > 0)
			{
				int pos = this.Textpos();
				char c = this.MoveRightGetChar();
				if (c <= '(')
				{
					if (c != '#')
					{
						if (c == '(')
						{
							if (this.CharsRight() >= 2 && this.RightChar(1) == '#' && this.RightChar() == '?')
							{
								this.MoveLeft();
								this.ScanBlank();
							}
							else
							{
								this.PushOptions();
								if (this.CharsRight() > 0 && this.RightChar() == '?')
								{
									this.MoveRight();
									if (this.CharsRight() > 1 && (this.RightChar() == '<' || this.RightChar() == '\''))
									{
										this.MoveRight();
										c = this.RightChar();
										if (c != '0' && RegexCharClass.IsWordChar(c))
										{
											if (c >= '1' && c <= '9')
											{
												this.NoteCaptureSlot(this.ScanDecimal(), pos);
											}
											else
											{
												this.NoteCaptureName(this.ScanCapname(), pos);
											}
										}
									}
									else
									{
										this.ScanOptions();
										if (this.CharsRight() > 0)
										{
											if (this.RightChar() == ')')
											{
												this.MoveRight();
												this.PopKeepOptions();
											}
											else if (this.RightChar() == '(')
											{
												this._ignoreNextParen = true;
												continue;
											}
										}
									}
								}
								else if (!this.UseOptionN() && !this._ignoreNextParen)
								{
									int autocap = this._autocap;
									this._autocap = autocap + 1;
									this.NoteCaptureSlot(autocap, pos);
								}
							}
							this._ignoreNextParen = false;
						}
					}
					else if (this.UseOptionX())
					{
						this.MoveLeft();
						this.ScanBlank();
					}
				}
				else if (c != ')')
				{
					if (c != '[')
					{
						if (c == '\\' && this.CharsRight() > 0)
						{
							this.MoveRight();
						}
					}
					else
					{
						this.ScanCharClass(false, true);
					}
				}
				else if (!this.EmptyOptionsStack())
				{
					this.PopOptions();
				}
			}
			this.AssignNameSlots();
		}

		// Token: 0x06000618 RID: 1560 RVA: 0x0002066C File Offset: 0x0001E86C
		internal void NoteCaptureSlot(int i, int pos)
		{
			if (!this._caps.ContainsKey(i))
			{
				this._caps.Add(i, pos);
				this._capcount++;
				if (this._captop <= i)
				{
					if (i == 2147483647)
					{
						this._captop = i;
						return;
					}
					this._captop = i + 1;
				}
			}
		}

		// Token: 0x06000619 RID: 1561 RVA: 0x000206D4 File Offset: 0x0001E8D4
		internal void NoteCaptureName(string name, int pos)
		{
			if (this._capnames == null)
			{
				this._capnames = new Hashtable();
				this._capnamelist = new List<string>();
			}
			if (!this._capnames.ContainsKey(name))
			{
				this._capnames.Add(name, pos);
				this._capnamelist.Add(name);
			}
		}

		// Token: 0x0600061A RID: 1562 RVA: 0x0002072B File Offset: 0x0001E92B
		internal void NoteCaptures(Hashtable caps, int capsize, Hashtable capnames)
		{
			this._caps = caps;
			this._capsize = capsize;
			this._capnames = capnames;
		}

		// Token: 0x0600061B RID: 1563 RVA: 0x00020744 File Offset: 0x0001E944
		internal void AssignNameSlots()
		{
			if (this._capnames != null)
			{
				for (int i = 0; i < this._capnamelist.Count; i++)
				{
					while (this.IsCaptureSlot(this._autocap))
					{
						this._autocap++;
					}
					string key = this._capnamelist[i];
					int pos = (int)this._capnames[key];
					this._capnames[key] = this._autocap;
					this.NoteCaptureSlot(this._autocap, pos);
					this._autocap++;
				}
			}
			if (this._capcount < this._captop)
			{
				this._capnumlist = new int[this._capcount];
				int num = 0;
				IDictionaryEnumerator enumerator = this._caps.GetEnumerator();
				while (enumerator.MoveNext())
				{
					this._capnumlist[num++] = (int)enumerator.Key;
				}
				Array.Sort<int>(this._capnumlist, Comparer<int>.Default);
			}
			if (this._capnames != null || this._capnumlist != null)
			{
				int num2 = 0;
				List<string> list;
				int num3;
				if (this._capnames == null)
				{
					list = null;
					this._capnames = new Hashtable();
					this._capnamelist = new List<string>();
					num3 = -1;
				}
				else
				{
					list = this._capnamelist;
					this._capnamelist = new List<string>();
					num3 = (int)this._capnames[list[0]];
				}
				for (int j = 0; j < this._capcount; j++)
				{
					int num4 = (this._capnumlist == null) ? j : this._capnumlist[j];
					if (num3 == num4)
					{
						this._capnamelist.Add(list[num2++]);
						num3 = ((num2 == list.Count) ? -1 : ((int)this._capnames[list[num2]]));
					}
					else
					{
						string text = Convert.ToString(num4, this._culture);
						this._capnamelist.Add(text);
						this._capnames[text] = num4;
					}
				}
			}
		}

		// Token: 0x0600061C RID: 1564 RVA: 0x00020955 File Offset: 0x0001EB55
		internal int CaptureSlotFromName(string capname)
		{
			return (int)this._capnames[capname];
		}

		// Token: 0x0600061D RID: 1565 RVA: 0x00020968 File Offset: 0x0001EB68
		internal bool IsCaptureSlot(int i)
		{
			if (this._caps != null)
			{
				return this._caps.ContainsKey(i);
			}
			return i >= 0 && i < this._capsize;
		}

		// Token: 0x0600061E RID: 1566 RVA: 0x00020993 File Offset: 0x0001EB93
		internal bool IsCaptureName(string capname)
		{
			return this._capnames != null && this._capnames.ContainsKey(capname);
		}

		// Token: 0x0600061F RID: 1567 RVA: 0x000209AB File Offset: 0x0001EBAB
		internal bool UseOptionN()
		{
			return (this._options & RegexOptions.ExplicitCapture) > RegexOptions.None;
		}

		// Token: 0x06000620 RID: 1568 RVA: 0x000209B8 File Offset: 0x0001EBB8
		internal bool UseOptionI()
		{
			return (this._options & RegexOptions.IgnoreCase) > RegexOptions.None;
		}

		// Token: 0x06000621 RID: 1569 RVA: 0x000209C5 File Offset: 0x0001EBC5
		internal bool UseOptionM()
		{
			return (this._options & RegexOptions.Multiline) > RegexOptions.None;
		}

		// Token: 0x06000622 RID: 1570 RVA: 0x000209D2 File Offset: 0x0001EBD2
		internal bool UseOptionS()
		{
			return (this._options & RegexOptions.Singleline) > RegexOptions.None;
		}

		// Token: 0x06000623 RID: 1571 RVA: 0x000209E0 File Offset: 0x0001EBE0
		internal bool UseOptionX()
		{
			return (this._options & RegexOptions.IgnorePatternWhitespace) > RegexOptions.None;
		}

		// Token: 0x06000624 RID: 1572 RVA: 0x000209EE File Offset: 0x0001EBEE
		internal bool UseOptionE()
		{
			return (this._options & RegexOptions.ECMAScript) > RegexOptions.None;
		}

		// Token: 0x06000625 RID: 1573 RVA: 0x000209FF File Offset: 0x0001EBFF
		internal static bool IsSpecial(char ch)
		{
			return ch <= '|' && RegexParser._category[(int)ch] >= 4;
		}

		// Token: 0x06000626 RID: 1574 RVA: 0x00020A15 File Offset: 0x0001EC15
		internal static bool IsStopperX(char ch)
		{
			return ch <= '|' && RegexParser._category[(int)ch] >= 2;
		}

		// Token: 0x06000627 RID: 1575 RVA: 0x00020A2B File Offset: 0x0001EC2B
		internal static bool IsQuantifier(char ch)
		{
			return ch <= '{' && RegexParser._category[(int)ch] >= 5;
		}

		// Token: 0x06000628 RID: 1576 RVA: 0x00020A44 File Offset: 0x0001EC44
		internal bool IsTrueQuantifier()
		{
			int num = this.CharsRight();
			if (num == 0)
			{
				return false;
			}
			int num2 = this.Textpos();
			char c = this.CharAt(num2);
			if (c != '{')
			{
				return c <= '{' && RegexParser._category[(int)c] >= 5;
			}
			int num3 = num2;
			while (--num > 0 && (c = this.CharAt(++num3)) >= '0' && c <= '9')
			{
			}
			if (num == 0 || num3 - num2 == 1)
			{
				return false;
			}
			if (c == '}')
			{
				return true;
			}
			if (c != ',')
			{
				return false;
			}
			while (--num > 0 && (c = this.CharAt(++num3)) >= '0' && c <= '9')
			{
			}
			return num > 0 && c == '}';
		}

		// Token: 0x06000629 RID: 1577 RVA: 0x00020AE8 File Offset: 0x0001ECE8
		internal static bool IsSpace(char ch)
		{
			return ch <= ' ' && RegexParser._category[(int)ch] == 2;
		}

		// Token: 0x0600062A RID: 1578 RVA: 0x00020AFB File Offset: 0x0001ECFB
		internal static bool IsMetachar(char ch)
		{
			return ch <= '|' && RegexParser._category[(int)ch] >= 1;
		}

		// Token: 0x0600062B RID: 1579 RVA: 0x00020B14 File Offset: 0x0001ED14
		internal void AddConcatenate(int pos, int cch, bool isReplacement)
		{
			if (cch == 0)
			{
				return;
			}
			RegexNode newChild;
			if (cch > 1)
			{
				string text = this._pattern.Substring(pos, cch);
				if (this.UseOptionI() && !isReplacement)
				{
					StringBuilder stringBuilder = new StringBuilder(text.Length);
					for (int i = 0; i < text.Length; i++)
					{
						stringBuilder.Append(char.ToLower(text[i], this._culture));
					}
					text = stringBuilder.ToString();
				}
				newChild = new RegexNode(12, this._options, text);
			}
			else
			{
				char c = this._pattern[pos];
				if (this.UseOptionI() && !isReplacement)
				{
					c = char.ToLower(c, this._culture);
				}
				newChild = new RegexNode(9, this._options, c);
			}
			this._concatenation.AddChild(newChild);
		}

		// Token: 0x0600062C RID: 1580 RVA: 0x00020BD4 File Offset: 0x0001EDD4
		internal void PushGroup()
		{
			this._group._next = this._stack;
			this._alternation._next = this._group;
			this._concatenation._next = this._alternation;
			this._stack = this._concatenation;
		}

		// Token: 0x0600062D RID: 1581 RVA: 0x00020C20 File Offset: 0x0001EE20
		internal void PopGroup()
		{
			this._concatenation = this._stack;
			this._alternation = this._concatenation._next;
			this._group = this._alternation._next;
			this._stack = this._group._next;
			if (this._group.Type() == 34 && this._group.ChildCount() == 0)
			{
				if (this._unit == null)
				{
					throw this.MakeException(SR.GetString("Illegal conditional (?(...)) expression."));
				}
				this._group.AddChild(this._unit);
				this._unit = null;
			}
		}

		// Token: 0x0600062E RID: 1582 RVA: 0x00020CB9 File Offset: 0x0001EEB9
		internal bool EmptyStack()
		{
			return this._stack == null;
		}

		// Token: 0x0600062F RID: 1583 RVA: 0x00020CC4 File Offset: 0x0001EEC4
		internal void StartGroup(RegexNode openGroup)
		{
			this._group = openGroup;
			this._alternation = new RegexNode(24, this._options);
			this._concatenation = new RegexNode(25, this._options);
		}

		// Token: 0x06000630 RID: 1584 RVA: 0x00020CF4 File Offset: 0x0001EEF4
		internal void AddAlternate()
		{
			if (this._group.Type() == 34 || this._group.Type() == 33)
			{
				this._group.AddChild(this._concatenation.ReverseLeft());
			}
			else
			{
				this._alternation.AddChild(this._concatenation.ReverseLeft());
			}
			this._concatenation = new RegexNode(25, this._options);
		}

		// Token: 0x06000631 RID: 1585 RVA: 0x00020D60 File Offset: 0x0001EF60
		internal void AddConcatenate()
		{
			this._concatenation.AddChild(this._unit);
			this._unit = null;
		}

		// Token: 0x06000632 RID: 1586 RVA: 0x00020D7A File Offset: 0x0001EF7A
		internal void AddConcatenate(bool lazy, int min, int max)
		{
			this._concatenation.AddChild(this._unit.MakeQuantifier(lazy, min, max));
			this._unit = null;
		}

		// Token: 0x06000633 RID: 1587 RVA: 0x00020D9C File Offset: 0x0001EF9C
		internal RegexNode Unit()
		{
			return this._unit;
		}

		// Token: 0x06000634 RID: 1588 RVA: 0x00020DA4 File Offset: 0x0001EFA4
		internal void AddUnitOne(char ch)
		{
			if (this.UseOptionI())
			{
				ch = char.ToLower(ch, this._culture);
			}
			this._unit = new RegexNode(9, this._options, ch);
		}

		// Token: 0x06000635 RID: 1589 RVA: 0x00020DD0 File Offset: 0x0001EFD0
		internal void AddUnitNotone(char ch)
		{
			if (this.UseOptionI())
			{
				ch = char.ToLower(ch, this._culture);
			}
			this._unit = new RegexNode(10, this._options, ch);
		}

		// Token: 0x06000636 RID: 1590 RVA: 0x00020DFC File Offset: 0x0001EFFC
		internal void AddUnitSet(string cc)
		{
			this._unit = new RegexNode(11, this._options, cc);
		}

		// Token: 0x06000637 RID: 1591 RVA: 0x00020E12 File Offset: 0x0001F012
		internal void AddUnitNode(RegexNode node)
		{
			this._unit = node;
		}

		// Token: 0x06000638 RID: 1592 RVA: 0x00020E1B File Offset: 0x0001F01B
		internal void AddUnitType(int type)
		{
			this._unit = new RegexNode(type, this._options);
		}

		// Token: 0x06000639 RID: 1593 RVA: 0x00020E30 File Offset: 0x0001F030
		internal void AddGroup()
		{
			if (this._group.Type() == 34 || this._group.Type() == 33)
			{
				this._group.AddChild(this._concatenation.ReverseLeft());
				if ((this._group.Type() == 33 && this._group.ChildCount() > 2) || this._group.ChildCount() > 3)
				{
					throw this.MakeException(SR.GetString("Too many | in (?()|)."));
				}
			}
			else
			{
				this._alternation.AddChild(this._concatenation.ReverseLeft());
				this._group.AddChild(this._alternation);
			}
			this._unit = this._group;
		}

		// Token: 0x0600063A RID: 1594 RVA: 0x00020EE0 File Offset: 0x0001F0E0
		internal void PushOptions()
		{
			this._optionsStack.Add(this._options);
		}

		// Token: 0x0600063B RID: 1595 RVA: 0x00020EF3 File Offset: 0x0001F0F3
		internal void PopOptions()
		{
			this._options = this._optionsStack[this._optionsStack.Count - 1];
			this._optionsStack.RemoveAt(this._optionsStack.Count - 1);
		}

		// Token: 0x0600063C RID: 1596 RVA: 0x00020F2B File Offset: 0x0001F12B
		internal bool EmptyOptionsStack()
		{
			return this._optionsStack.Count == 0;
		}

		// Token: 0x0600063D RID: 1597 RVA: 0x00020F3B File Offset: 0x0001F13B
		internal void PopKeepOptions()
		{
			this._optionsStack.RemoveAt(this._optionsStack.Count - 1);
		}

		// Token: 0x0600063E RID: 1598 RVA: 0x00020F55 File Offset: 0x0001F155
		internal ArgumentException MakeException(string message)
		{
			return new ArgumentException(SR.GetString("parsing \"{0}\" - {1}", new object[]
			{
				this._pattern,
				message
			}));
		}

		// Token: 0x0600063F RID: 1599 RVA: 0x00020F79 File Offset: 0x0001F179
		internal int Textpos()
		{
			return this._currentPos;
		}

		// Token: 0x06000640 RID: 1600 RVA: 0x00020F81 File Offset: 0x0001F181
		internal void Textto(int pos)
		{
			this._currentPos = pos;
		}

		// Token: 0x06000641 RID: 1601 RVA: 0x00020F8C File Offset: 0x0001F18C
		internal char MoveRightGetChar()
		{
			string pattern = this._pattern;
			int currentPos = this._currentPos;
			this._currentPos = currentPos + 1;
			return pattern[currentPos];
		}

		// Token: 0x06000642 RID: 1602 RVA: 0x00020FB5 File Offset: 0x0001F1B5
		internal void MoveRight()
		{
			this.MoveRight(1);
		}

		// Token: 0x06000643 RID: 1603 RVA: 0x00020FBE File Offset: 0x0001F1BE
		internal void MoveRight(int i)
		{
			this._currentPos += i;
		}

		// Token: 0x06000644 RID: 1604 RVA: 0x00020FCE File Offset: 0x0001F1CE
		internal void MoveLeft()
		{
			this._currentPos--;
		}

		// Token: 0x06000645 RID: 1605 RVA: 0x00020FDE File Offset: 0x0001F1DE
		internal char CharAt(int i)
		{
			return this._pattern[i];
		}

		// Token: 0x06000646 RID: 1606 RVA: 0x00020FEC File Offset: 0x0001F1EC
		internal char RightChar()
		{
			return this._pattern[this._currentPos];
		}

		// Token: 0x06000647 RID: 1607 RVA: 0x00020FFF File Offset: 0x0001F1FF
		internal char RightChar(int i)
		{
			return this._pattern[this._currentPos + i];
		}

		// Token: 0x06000648 RID: 1608 RVA: 0x00021014 File Offset: 0x0001F214
		internal int CharsRight()
		{
			return this._pattern.Length - this._currentPos;
		}

		// Token: 0x06000649 RID: 1609 RVA: 0x00021028 File Offset: 0x0001F228
		// Note: this type is marked as 'beforefieldinit'.
		static RegexParser()
		{
		}

		// Token: 0x04000ABA RID: 2746
		internal RegexNode _stack;

		// Token: 0x04000ABB RID: 2747
		internal RegexNode _group;

		// Token: 0x04000ABC RID: 2748
		internal RegexNode _alternation;

		// Token: 0x04000ABD RID: 2749
		internal RegexNode _concatenation;

		// Token: 0x04000ABE RID: 2750
		internal RegexNode _unit;

		// Token: 0x04000ABF RID: 2751
		internal string _pattern;

		// Token: 0x04000AC0 RID: 2752
		internal int _currentPos;

		// Token: 0x04000AC1 RID: 2753
		internal CultureInfo _culture;

		// Token: 0x04000AC2 RID: 2754
		internal int _autocap;

		// Token: 0x04000AC3 RID: 2755
		internal int _capcount;

		// Token: 0x04000AC4 RID: 2756
		internal int _captop;

		// Token: 0x04000AC5 RID: 2757
		internal int _capsize;

		// Token: 0x04000AC6 RID: 2758
		internal Hashtable _caps;

		// Token: 0x04000AC7 RID: 2759
		internal Hashtable _capnames;

		// Token: 0x04000AC8 RID: 2760
		internal int[] _capnumlist;

		// Token: 0x04000AC9 RID: 2761
		internal List<string> _capnamelist;

		// Token: 0x04000ACA RID: 2762
		internal RegexOptions _options;

		// Token: 0x04000ACB RID: 2763
		internal List<RegexOptions> _optionsStack;

		// Token: 0x04000ACC RID: 2764
		internal bool _ignoreNextParen;

		// Token: 0x04000ACD RID: 2765
		internal const int MaxValueDiv10 = 214748364;

		// Token: 0x04000ACE RID: 2766
		internal const int MaxValueMod10 = 7;

		// Token: 0x04000ACF RID: 2767
		internal const byte Q = 5;

		// Token: 0x04000AD0 RID: 2768
		internal const byte S = 4;

		// Token: 0x04000AD1 RID: 2769
		internal const byte Z = 3;

		// Token: 0x04000AD2 RID: 2770
		internal const byte X = 2;

		// Token: 0x04000AD3 RID: 2771
		internal const byte E = 1;

		// Token: 0x04000AD4 RID: 2772
		internal static readonly byte[] _category = new byte[]
		{
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			2,
			2,
			0,
			2,
			2,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			2,
			0,
			0,
			3,
			4,
			0,
			0,
			0,
			4,
			4,
			5,
			5,
			0,
			0,
			4,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			5,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			4,
			4,
			0,
			4,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			5,
			4,
			0,
			0,
			0
		};
	}
}
