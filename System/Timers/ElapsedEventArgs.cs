﻿using System;
using Unity;

namespace System.Timers
{
	/// <summary>Provides data for the <see cref="E:System.Timers.Timer.Elapsed" /> event.</summary>
	// Token: 0x020000C6 RID: 198
	public class ElapsedEventArgs : EventArgs
	{
		// Token: 0x060004AD RID: 1197 RVA: 0x00016B24 File Offset: 0x00014D24
		internal ElapsedEventArgs(DateTime time)
		{
			this.time = time;
		}

		/// <summary>Gets the date/time when the <see cref="E:System.Timers.Timer.Elapsed" /> event was raised.</summary>
		/// <returns>The time the <see cref="E:System.Timers.Timer.Elapsed" /> event was raised.</returns>
		// Token: 0x170000DF RID: 223
		// (get) Token: 0x060004AE RID: 1198 RVA: 0x00016B33 File Offset: 0x00014D33
		public DateTime SignalTime
		{
			get
			{
				return this.time;
			}
		}

		// Token: 0x060004AF RID: 1199 RVA: 0x000092E2 File Offset: 0x000074E2
		internal ElapsedEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000994 RID: 2452
		private DateTime time;
	}
}
