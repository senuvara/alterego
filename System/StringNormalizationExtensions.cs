﻿using System;
using System.ComponentModel;
using System.Security;
using System.Text;
using Unity;

namespace System
{
	/// <summary>Provides extension methods to work with normalized strings. </summary>
	// Token: 0x02000623 RID: 1571
	[EditorBrowsable(EditorBrowsableState.Never)]
	public static class StringNormalizationExtensions
	{
		/// <summary>Indicates whether the specified string is in Unicode normalization form C. </summary>
		/// <param name="value">A string. </param>
		/// <returns>
		///   <see langword="true" /> if <paramref name="value" /> is in normalization form C; otherwise, <see langword="false" />. </returns>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="value" /> contains invalid Unicode characters. </exception>
		// Token: 0x0600327D RID: 12925 RVA: 0x000A48C4 File Offset: 0x000A2AC4
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static bool IsNormalized(this string value)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether a string is in a specified Unicode normalization form. </summary>
		/// <param name="value">A string. </param>
		/// <param name="normalizationForm">A Unicode normalization form. </param>
		/// <returns>
		///   <see langword="true" /> if <paramref name="value" /> is in normalization form <paramref name="normalizationForm" />; otherwise, <see langword="false" />. </returns>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="value" /> contains invalid Unicode characters. </exception>
		// Token: 0x0600327E RID: 12926 RVA: 0x000A48E0 File Offset: 0x000A2AE0
		[EditorBrowsable(EditorBrowsableState.Never)]
		[SecurityCritical]
		public static bool IsNormalized(this string value, NormalizationForm normalizationForm)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Normalizes a string to a Unicode normalization form C. </summary>
		/// <param name="value">The string to normalize. </param>
		/// <returns>A new string whose textual value is the same as <paramref name="value" /> but whose binary representation is in Unicode normalization form C. </returns>
		// Token: 0x0600327F RID: 12927 RVA: 0x00043C3C File Offset: 0x00041E3C
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static string Normalize(this string value)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Normalizes a string to the specified Unicode normalization form. </summary>
		/// <param name="value">The string to normalize. </param>
		/// <param name="normalizationForm">The Unicode normalization form. </param>
		/// <returns>A new string whose textual value is the same as <paramref name="value" /> but whose binary representation is in the <paramref name="normalizationForm" /> normalization form. </returns>
		// Token: 0x06003280 RID: 12928 RVA: 0x00043C3C File Offset: 0x00041E3C
		[SecurityCritical]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static string Normalize(this string value, NormalizationForm normalizationForm)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
