﻿using System;

namespace System.Reflection
{
	/// <summary>Represents an object that provides a custom type.</summary>
	// Token: 0x0200062C RID: 1580
	public interface ICustomTypeProvider
	{
		/// <summary>Gets the custom type provided by this object.</summary>
		/// <returns>The custom type. </returns>
		// Token: 0x060032B2 RID: 12978
		Type GetCustomType();
	}
}
