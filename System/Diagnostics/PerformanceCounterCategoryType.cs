﻿using System;

namespace System.Diagnostics
{
	/// <summary>Indicates whether the performance counter category can have multiple instances.</summary>
	// Token: 0x020005FD RID: 1533
	public enum PerformanceCounterCategoryType
	{
		/// <summary>The performance counter category can have multiple instances.</summary>
		// Token: 0x0400249C RID: 9372
		MultiInstance = 1,
		/// <summary>The performance counter category can have only a single instance.</summary>
		// Token: 0x0400249D RID: 9373
		SingleInstance = 0,
		/// <summary>The instance functionality for the performance counter category is unknown. </summary>
		// Token: 0x0400249E RID: 9374
		Unknown = -1
	}
}
