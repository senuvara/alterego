﻿using System;
using System.Security;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Allows declaritive performance counter permission checks. </summary>
	// Token: 0x0200067A RID: 1658
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Event, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public class PerformanceCounterPermissionAttribute : CodeAccessSecurityAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.PerformanceCounterPermissionAttribute" /> class.</summary>
		/// <param name="action">One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values. </param>
		// Token: 0x060034F5 RID: 13557 RVA: 0x0000232D File Offset: 0x0000052D
		public PerformanceCounterPermissionAttribute(SecurityAction action)
		{
		}

		/// <summary>Gets or sets the name of the performance counter category.</summary>
		/// <returns>The name of the performance counter category (performance object).</returns>
		/// <exception cref="T:System.ArgumentNullException">The value is <see langword="null" />. </exception>
		// Token: 0x17000D6E RID: 3438
		// (get) Token: 0x060034F6 RID: 13558 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060034F7 RID: 13559 RVA: 0x000092E2 File Offset: 0x000074E2
		public string CategoryName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the computer name for the performance counter.</summary>
		/// <returns>The server on which the category of the performance counter resides.</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Diagnostics.PerformanceCounterPermissionAttribute.MachineName" /> format is invalid. </exception>
		// Token: 0x17000D6F RID: 3439
		// (get) Token: 0x060034F8 RID: 13560 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x060034F9 RID: 13561 RVA: 0x000092E2 File Offset: 0x000074E2
		public string MachineName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the access levels used in the permissions request.</summary>
		/// <returns>A bitwise combination of the <see cref="T:System.Diagnostics.PerformanceCounterPermissionAccess" /> values. The default is <see cref="F:System.Diagnostics.EventLogPermissionAccess.Write" />.</returns>
		// Token: 0x17000D70 RID: 3440
		// (get) Token: 0x060034FA RID: 13562 RVA: 0x000A5948 File Offset: 0x000A3B48
		// (set) Token: 0x060034FB RID: 13563 RVA: 0x000092E2 File Offset: 0x000074E2
		public PerformanceCounterPermissionAccess PermissionAccess
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return PerformanceCounterPermissionAccess.None;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Creates the permission based on the requested access levels that are set through the <see cref="P:System.Diagnostics.PerformanceCounterPermissionAttribute.PermissionAccess" /> property on the attribute.</summary>
		/// <returns>An <see cref="T:System.Security.IPermission" /> that represents the created permission.</returns>
		// Token: 0x060034FC RID: 13564 RVA: 0x00043C3C File Offset: 0x00041E3C
		public override IPermission CreatePermission()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
