﻿using System;

namespace System.Diagnostics
{
	/// <summary>Specifies the lifetime of a performance counter instance.</summary>
	// Token: 0x02000673 RID: 1651
	public enum PerformanceCounterInstanceLifetime
	{
		/// <summary>Remove the performance counter instance when no counters are using the process category.</summary>
		// Token: 0x04002518 RID: 9496
		Global,
		/// <summary>Remove the performance counter instance when the process is closed.</summary>
		// Token: 0x04002519 RID: 9497
		Process
	}
}
