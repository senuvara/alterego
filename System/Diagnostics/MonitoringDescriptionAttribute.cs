﻿using System;
using System.ComponentModel;

namespace System.Diagnostics
{
	/// <summary>Specifies a description for a property or event.</summary>
	// Token: 0x02000115 RID: 277
	[AttributeUsage(AttributeTargets.All)]
	public class MonitoringDescriptionAttribute : DescriptionAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.MonitoringDescriptionAttribute" /> class, using the specified description.</summary>
		/// <param name="description">The application-defined description text. </param>
		// Token: 0x060008EE RID: 2286 RVA: 0x000157FB File Offset: 0x000139FB
		public MonitoringDescriptionAttribute(string description) : base(description)
		{
		}

		/// <summary>Gets description text associated with the item monitored.</summary>
		/// <returns>An application-defined description.</returns>
		// Token: 0x170001C8 RID: 456
		// (get) Token: 0x060008EF RID: 2287 RVA: 0x00029E20 File Offset: 0x00028020
		public override string Description
		{
			get
			{
				return base.Description;
			}
		}
	}
}
