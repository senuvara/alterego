﻿using System;
using System.Security.Permissions;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Controls code access permissions for event logging.</summary>
	// Token: 0x02000668 RID: 1640
	[Serializable]
	public sealed class EventLogPermission : ResourcePermissionBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.EventLogPermission" /> class.</summary>
		// Token: 0x0600346F RID: 13423 RVA: 0x000092E2 File Offset: 0x000074E2
		public EventLogPermission()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.EventLogPermission" /> class with the specified access levels and the name of the computer to use.</summary>
		/// <param name="permissionAccess">One of the enumeration values that specifies an access level. </param>
		/// <param name="machineName">The name of the computer on which to read or write events. </param>
		// Token: 0x06003470 RID: 13424 RVA: 0x000092E2 File Offset: 0x000074E2
		public EventLogPermission(EventLogPermissionAccess permissionAccess, string machineName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.EventLogPermission" /> class with the specified permission entries.</summary>
		/// <param name="permissionAccessEntries">An array of  objects that represent permission entries. The <see cref="P:System.Diagnostics.EventLogPermission.PermissionEntries" /> property is set to this value. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="permissionAccessEntries" /> is <see langword="null" />.</exception>
		// Token: 0x06003471 RID: 13425 RVA: 0x000092E2 File Offset: 0x000074E2
		public EventLogPermission(EventLogPermissionEntry[] permissionAccessEntries)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.EventLogPermission" /> class with the specified permission state.</summary>
		/// <param name="state">One of the enumeration values that specifies the permission state (full access or no access to resources). </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="state" /> parameter is not a valid value of <see cref="T:System.Security.Permissions.PermissionState" />. </exception>
		// Token: 0x06003472 RID: 13426 RVA: 0x000092E2 File Offset: 0x000074E2
		public EventLogPermission(PermissionState state)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the collection of permission entries for this permissions request.</summary>
		/// <returns>A collection that contains the permission entries for this permissions request.</returns>
		// Token: 0x17000D4B RID: 3403
		// (get) Token: 0x06003473 RID: 13427 RVA: 0x00043C3C File Offset: 0x00041E3C
		public EventLogPermissionEntryCollection PermissionEntries
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
