﻿using System;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Represents the configuration settings used to create an event log source on the local computer or a remote computer.</summary>
	// Token: 0x0200061F RID: 1567
	public class EventSourceCreationData
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.EventSourceCreationData" /> class with a specified event source and event log name.</summary>
		/// <param name="source">The name to register with the event log as a source of entries. </param>
		/// <param name="logName">The name of the log to which entries from the source are written. </param>
		// Token: 0x0600325F RID: 12895 RVA: 0x000092E2 File Offset: 0x000074E2
		public EventSourceCreationData(string source, string logName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the number of categories in the category resource file.</summary>
		/// <returns>The number of categories in the category resource file. The default value is zero.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The property is set to a negative value or to a value larger than <see cref="F:System.UInt16.MaxValue" />. </exception>
		// Token: 0x17000C8C RID: 3212
		// (get) Token: 0x06003260 RID: 12896 RVA: 0x000A4854 File Offset: 0x000A2A54
		// (set) Token: 0x06003261 RID: 12897 RVA: 0x000092E2 File Offset: 0x000074E2
		public int CategoryCount
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the path of the resource file that contains category strings for the source.</summary>
		/// <returns>The path of the category resource file. The default is an empty string ("").</returns>
		// Token: 0x17000C8D RID: 3213
		// (get) Token: 0x06003262 RID: 12898 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003263 RID: 12899 RVA: 0x000092E2 File Offset: 0x000074E2
		public string CategoryResourceFile
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the event log to which the source writes entries.</summary>
		/// <returns>The name of the event log. This can be Application, System, or a custom log name. The default value is "Application."</returns>
		// Token: 0x17000C8E RID: 3214
		// (get) Token: 0x06003264 RID: 12900 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003265 RID: 12901 RVA: 0x000092E2 File Offset: 0x000074E2
		public string LogName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the computer on which to register the event source.</summary>
		/// <returns>The name of the system on which to register the event source. The default is the local computer (".").</returns>
		/// <exception cref="T:System.ArgumentException">The computer name is invalid. </exception>
		// Token: 0x17000C8F RID: 3215
		// (get) Token: 0x06003266 RID: 12902 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003267 RID: 12903 RVA: 0x000092E2 File Offset: 0x000074E2
		public string MachineName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the path of the message resource file that contains message formatting strings for the source.</summary>
		/// <returns>The path of the message resource file. The default is an empty string ("").</returns>
		// Token: 0x17000C90 RID: 3216
		// (get) Token: 0x06003268 RID: 12904 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x06003269 RID: 12905 RVA: 0x000092E2 File Offset: 0x000074E2
		public string MessageResourceFile
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the path of the resource file that contains message parameter strings for the source.</summary>
		/// <returns>The path of the parameter resource file. The default is an empty string ("").</returns>
		// Token: 0x17000C91 RID: 3217
		// (get) Token: 0x0600326A RID: 12906 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600326B RID: 12907 RVA: 0x000092E2 File Offset: 0x000074E2
		public string ParameterResourceFile
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name to register with the event log as an event source.</summary>
		/// <returns>The name to register with the event log as a source of entries. The default is an empty string ("").</returns>
		// Token: 0x17000C92 RID: 3218
		// (get) Token: 0x0600326C RID: 12908 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600326D RID: 12909 RVA: 0x000092E2 File Offset: 0x000074E2
		public string Source
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
