﻿using System;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Provides a set of utility functions for interpreting performance counter data.</summary>
	// Token: 0x02000666 RID: 1638
	public static class CounterSampleCalculator
	{
		/// <summary>Computes the calculated value of a single raw counter sample.</summary>
		/// <param name="newSample">A <see cref="T:System.Diagnostics.CounterSample" /> that indicates the most recent sample the system has taken. </param>
		/// <returns>A floating-point representation of the performance counter's calculated value.</returns>
		// Token: 0x0600346B RID: 13419 RVA: 0x000A55AC File Offset: 0x000A37AC
		public static float ComputeCounterValue(CounterSample newSample)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0f;
		}

		/// <summary>Computes the calculated value of two raw counter samples.</summary>
		/// <param name="oldSample">A <see cref="T:System.Diagnostics.CounterSample" /> that indicates a previous sample the system has taken. </param>
		/// <param name="newSample">A <see cref="T:System.Diagnostics.CounterSample" /> that indicates the most recent sample the system has taken. </param>
		/// <returns>A floating-point representation of the performance counter's calculated value.</returns>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="oldSample" /> uses a counter type that is different from <paramref name="newSample" />. </exception>
		/// <exception cref="T:System.ComponentModel.Win32Exception">
		///         <paramref name="newSample" /> counter type has a Performance Data Helper (PDH) error. For more information, see "Checking PDH Interface Return Values" in the Win32 and COM Development section of this documentation.</exception>
		// Token: 0x0600346C RID: 13420 RVA: 0x000A55C8 File Offset: 0x000A37C8
		public static float ComputeCounterValue(CounterSample oldSample, CounterSample newSample)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0f;
		}
	}
}
