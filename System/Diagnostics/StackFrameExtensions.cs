﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Provides extension methods for the <see cref="T:System.Diagnostics.StackFrame" /> class, which represents a function call on the call stack for the current thread.  </summary>
	// Token: 0x0200067B RID: 1659
	[EditorBrowsable(EditorBrowsableState.Never)]
	public static class StackFrameExtensions
	{
		/// <summary>Returns a pointer to the base address of the native just-in-time (JIT)-compiled image that this stack frame is executing. </summary>
		/// <param name="stackFrame">A stack frame. </param>
		/// <returns>This method always returns <see cref="F:System.IntPtr.Zero" />. </returns>
		// Token: 0x060034FD RID: 13565 RVA: 0x000A5964 File Offset: 0x000A3B64
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static IntPtr GetNativeImageBase(this StackFrame stackFrame)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Gets an interface pointer to the start of the native code for the method that is being executed. </summary>
		/// <param name="stackFrame">A stack frame. </param>
		/// <returns>This method always returns  <see cref="F:System.IntPtr.Zero" />.</returns>
		// Token: 0x060034FE RID: 13566 RVA: 0x000A5980 File Offset: 0x000A3B80
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static IntPtr GetNativeIP(this StackFrame stackFrame)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Indicates whether an offset from the start of the IL code for the method that is executing is available. </summary>
		/// <param name="stackFrame">A stack frame. </param>
		/// <returns>
		///   <see langword="true" /> if the offset is available; otherwise, <see langword="false" />. </returns>
		// Token: 0x060034FF RID: 13567 RVA: 0x000A599C File Offset: 0x000A3B9C
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static bool HasILOffset(this StackFrame stackFrame)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether information about the method in which the specified frame is executing is available. </summary>
		/// <param name="stackFrame">A stack frame. </param>
		/// <returns>
		///   <see langword="true" /> if information about the method in which the current frame is executing is available; otherwise, <see langword="false" />. </returns>
		// Token: 0x06003500 RID: 13568 RVA: 0x000A59B8 File Offset: 0x000A3BB8
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static bool HasMethod(this StackFrame stackFrame)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the native just-in-time (JIT)-compiled image is available for the specified stack frame. </summary>
		/// <param name="stackFrame">A stack frame. </param>
		/// <returns>
		///   <see langword="true" /> if a native image is available for this stack frame; otherwise, <see langword="false" />. </returns>
		// Token: 0x06003501 RID: 13569 RVA: 0x000A59D4 File Offset: 0x000A3BD4
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static bool HasNativeImage(this StackFrame stackFrame)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether the file that contains the code that the specified stack frame is executing is available. </summary>
		/// <param name="stackFrame">A stack frame. </param>
		/// <returns>
		///   <see langword="true" /> if the code that the specified stack frame is executing is available; otherwise, <see langword="false" />. </returns>
		// Token: 0x06003502 RID: 13570 RVA: 0x000A59F0 File Offset: 0x000A3BF0
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static bool HasSource(this StackFrame stackFrame)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}
	}
}
