﻿using System;

namespace System.Diagnostics
{
	// Token: 0x02000110 RID: 272
	internal class AssertWrapper
	{
		// Token: 0x060008B4 RID: 2228 RVA: 0x000297C6 File Offset: 0x000279C6
		public static void ShowAssert(string stackTrace, StackFrame frame, string message, string detailMessage)
		{
			new DefaultTraceListener().Fail(message, detailMessage);
		}

		// Token: 0x060008B5 RID: 2229 RVA: 0x0000232F File Offset: 0x0000052F
		public AssertWrapper()
		{
		}
	}
}
