﻿using System;

namespace System.Diagnostics
{
	// Token: 0x0200010B RID: 267
	internal class ProcessThreadTimes
	{
		// Token: 0x1700018C RID: 396
		// (get) Token: 0x0600086F RID: 2159 RVA: 0x00029362 File Offset: 0x00027562
		public DateTime StartTime
		{
			get
			{
				return DateTime.FromFileTime(this.create);
			}
		}

		// Token: 0x1700018D RID: 397
		// (get) Token: 0x06000870 RID: 2160 RVA: 0x0002936F File Offset: 0x0002756F
		public DateTime ExitTime
		{
			get
			{
				return DateTime.FromFileTime(this.exit);
			}
		}

		// Token: 0x1700018E RID: 398
		// (get) Token: 0x06000871 RID: 2161 RVA: 0x0002937C File Offset: 0x0002757C
		public TimeSpan PrivilegedProcessorTime
		{
			get
			{
				return new TimeSpan(this.kernel);
			}
		}

		// Token: 0x1700018F RID: 399
		// (get) Token: 0x06000872 RID: 2162 RVA: 0x00029389 File Offset: 0x00027589
		public TimeSpan UserProcessorTime
		{
			get
			{
				return new TimeSpan(this.user);
			}
		}

		// Token: 0x17000190 RID: 400
		// (get) Token: 0x06000873 RID: 2163 RVA: 0x00029396 File Offset: 0x00027596
		public TimeSpan TotalProcessorTime
		{
			get
			{
				return new TimeSpan(this.user + this.kernel);
			}
		}

		// Token: 0x06000874 RID: 2164 RVA: 0x0000232F File Offset: 0x0000052F
		public ProcessThreadTimes()
		{
		}

		// Token: 0x04000BAF RID: 2991
		internal long create;

		// Token: 0x04000BB0 RID: 2992
		internal long exit;

		// Token: 0x04000BB1 RID: 2993
		internal long kernel;

		// Token: 0x04000BB2 RID: 2994
		internal long user;
	}
}
