﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Defines the counter type, name, and Help string for a custom counter.</summary>
	// Token: 0x020005FF RID: 1535
	[TypeConverter("System.Diagnostics.Design.CounterCreationDataConverter, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
	[Serializable]
	public class CounterCreationData
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.CounterCreationData" /> class, to a counter of type <see langword="NumberOfItems32" />, and with empty name and help strings.</summary>
		// Token: 0x06003138 RID: 12600 RVA: 0x000092E2 File Offset: 0x000074E2
		public CounterCreationData()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.CounterCreationData" /> class, to a counter of the specified type, using the specified counter name and Help strings.</summary>
		/// <param name="counterName">The name of the counter, which must be unique within its category. </param>
		/// <param name="counterHelp">The text that describes the counter's behavior. </param>
		/// <param name="counterType">A <see cref="T:System.Diagnostics.PerformanceCounterType" /> that identifies the counter's behavior. </param>
		/// <exception cref="T:System.ComponentModel.InvalidEnumArgumentException">You have specified a value for <paramref name="counterType" /> that is not a member of the <see cref="T:System.Diagnostics.PerformanceCounterType" /> enumeration. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="counterHelp" /> is <see langword="null" />. </exception>
		// Token: 0x06003139 RID: 12601 RVA: 0x000092E2 File Offset: 0x000074E2
		public CounterCreationData(string counterName, string counterHelp, PerformanceCounterType counterType)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the custom counter's description.</summary>
		/// <returns>The text that describes the counter's behavior.</returns>
		/// <exception cref="T:System.ArgumentNullException">The specified value is <see langword="null" />.</exception>
		// Token: 0x17000C33 RID: 3123
		// (get) Token: 0x0600313A RID: 12602 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600313B RID: 12603 RVA: 0x000092E2 File Offset: 0x000074E2
		public string CounterHelp
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the name of the custom counter.</summary>
		/// <returns>A name for the counter, which is unique in its category.</returns>
		/// <exception cref="T:System.ArgumentNullException">The specified value is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">The specified value is not between 1 and 80 characters long or contains double quotes, control characters or leading or trailing spaces.</exception>
		// Token: 0x17000C34 RID: 3124
		// (get) Token: 0x0600313C RID: 12604 RVA: 0x00043C3C File Offset: 0x00041E3C
		// (set) Token: 0x0600313D RID: 12605 RVA: 0x000092E2 File Offset: 0x000074E2
		public string CounterName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets the performance counter type of the custom counter.</summary>
		/// <returns>A <see cref="T:System.Diagnostics.PerformanceCounterType" /> that defines the behavior of the performance counter.</returns>
		/// <exception cref="T:System.ComponentModel.InvalidEnumArgumentException">You have specified a type that is not a member of the <see cref="T:System.Diagnostics.PerformanceCounterType" /> enumeration. </exception>
		// Token: 0x17000C35 RID: 3125
		// (get) Token: 0x0600313E RID: 12606 RVA: 0x000A4330 File Offset: 0x000A2530
		// (set) Token: 0x0600313F RID: 12607 RVA: 0x000092E2 File Offset: 0x000074E2
		public PerformanceCounterType CounterType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return PerformanceCounterType.NumberOfItemsHEX32;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
