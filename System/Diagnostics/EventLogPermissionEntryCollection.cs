﻿using System;
using System.Collections;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Contains a strongly typed collection of <see cref="T:System.Diagnostics.EventLogPermissionEntry" /> objects.</summary>
	// Token: 0x0200066B RID: 1643
	[Serializable]
	public class EventLogPermissionEntryCollection : CollectionBase
	{
		// Token: 0x06003477 RID: 13431 RVA: 0x000092E2 File Offset: 0x000074E2
		internal EventLogPermissionEntryCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the object at a specified index.</summary>
		/// <param name="index">The zero-based index into the collection. </param>
		/// <returns>The <see cref="T:System.Diagnostics.EventLogPermissionEntry" /> that exists at the specified index.</returns>
		// Token: 0x17000D4E RID: 3406
		public EventLogPermissionEntry this[int index]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Adds a specified <see cref="T:System.Diagnostics.EventLogPermissionEntry" /> to this collection.</summary>
		/// <param name="value">The <see cref="T:System.Diagnostics.EventLogPermissionEntry" /> to add. </param>
		/// <returns>The zero-based index of the added <see cref="T:System.Diagnostics.EventLogPermissionEntry" />.</returns>
		// Token: 0x0600347A RID: 13434 RVA: 0x000A5600 File Offset: 0x000A3800
		public int Add(EventLogPermissionEntry value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Appends a set of specified permission entries to this collection.</summary>
		/// <param name="value">A <see cref="T:System.Diagnostics.EventLogPermissionEntryCollection" /> that contains the permission entries to add. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x0600347B RID: 13435 RVA: 0x000092E2 File Offset: 0x000074E2
		public void AddRange(EventLogPermissionEntryCollection value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Appends a set of specified permission entries to this collection.</summary>
		/// <param name="value">An array of type <see cref="T:System.Diagnostics.EventLogPermissionEntry" /> objects that contains the permission entries to add. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x0600347C RID: 13436 RVA: 0x000092E2 File Offset: 0x000074E2
		public void AddRange(EventLogPermissionEntry[] value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Determines whether this collection contains a specified <see cref="T:System.Diagnostics.EventLogPermissionEntry" />.</summary>
		/// <param name="value">The <see cref="T:System.Diagnostics.EventLogPermissionEntry" /> to find. </param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.Diagnostics.EventLogPermissionEntry" /> belongs to this collection; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600347D RID: 13437 RVA: 0x000A561C File Offset: 0x000A381C
		public bool Contains(EventLogPermissionEntry value)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Copies the permission entries from this collection to an array, starting at a particular index of the array.</summary>
		/// <param name="array">An array of type <see cref="T:System.Diagnostics.EventLogPermissionEntry" /> that receives this collection's permission entries. </param>
		/// <param name="index">The zero-based index at which to begin copying the permission entries. </param>
		// Token: 0x0600347E RID: 13438 RVA: 0x000092E2 File Offset: 0x000074E2
		public void CopyTo(EventLogPermissionEntry[] array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Determines the index of a specified permission entry in this collection.</summary>
		/// <param name="value">The permission entry to search for. </param>
		/// <returns>The zero-based index of the specified permission entry, or -1 if the permission entry was not found in the collection.</returns>
		// Token: 0x0600347F RID: 13439 RVA: 0x000A5638 File Offset: 0x000A3838
		public int IndexOf(EventLogPermissionEntry value)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Inserts a permission entry into this collection at a specified index.</summary>
		/// <param name="index">The zero-based index of the collection at which to insert the permission entry. </param>
		/// <param name="value">The permission entry to insert into this collection. </param>
		// Token: 0x06003480 RID: 13440 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Insert(int index, EventLogPermissionEntry value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes a specified permission entry from this collection.</summary>
		/// <param name="value">The permission entry to remove. </param>
		// Token: 0x06003481 RID: 13441 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Remove(EventLogPermissionEntry value)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
