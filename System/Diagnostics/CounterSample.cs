﻿using System;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Defines a structure that holds the raw data for a performance counter.</summary>
	// Token: 0x02000665 RID: 1637
	public struct CounterSample
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.CounterSample" /> structure and sets the <see cref="P:System.Diagnostics.CounterSample.CounterTimeStamp" /> property to 0 (zero).</summary>
		/// <param name="rawValue">The numeric value associated with the performance counter sample. </param>
		/// <param name="baseValue">An optional, base raw value for the counter, to use only if the sample is based on multiple counters. </param>
		/// <param name="counterFrequency">The frequency with which the counter is read. </param>
		/// <param name="systemFrequency">The frequency with which the system reads from the counter. </param>
		/// <param name="timeStamp">The raw time stamp. </param>
		/// <param name="timeStamp100nSec">The raw, high-fidelity time stamp. </param>
		/// <param name="counterType">A <see cref="T:System.Diagnostics.PerformanceCounterType" /> object that indicates the type of the counter for which this sample is a snapshot. </param>
		// Token: 0x0600345C RID: 13404 RVA: 0x000092E2 File Offset: 0x000074E2
		public CounterSample(long rawValue, long baseValue, long counterFrequency, long systemFrequency, long timeStamp, long timeStamp100nSec, PerformanceCounterType counterType)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.CounterSample" /> structure and sets the <see cref="P:System.Diagnostics.CounterSample.CounterTimeStamp" /> property to the value that is passed in.</summary>
		/// <param name="rawValue">The numeric value associated with the performance counter sample. </param>
		/// <param name="baseValue">An optional, base raw value for the counter, to use only if the sample is based on multiple counters. </param>
		/// <param name="counterFrequency">The frequency with which the counter is read. </param>
		/// <param name="systemFrequency">The frequency with which the system reads from the counter. </param>
		/// <param name="timeStamp">The raw time stamp. </param>
		/// <param name="timeStamp100nSec">The raw, high-fidelity time stamp. </param>
		/// <param name="counterType">A <see cref="T:System.Diagnostics.PerformanceCounterType" /> object that indicates the type of the counter for which this sample is a snapshot. </param>
		/// <param name="counterTimeStamp">The time at which the sample was taken. </param>
		// Token: 0x0600345D RID: 13405 RVA: 0x000092E2 File Offset: 0x000074E2
		public CounterSample(long rawValue, long baseValue, long counterFrequency, long systemFrequency, long timeStamp, long timeStamp100nSec, PerformanceCounterType counterType, long counterTimeStamp)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets an optional, base raw value for the counter.</summary>
		/// <returns>The base raw value, which is used only if the sample is based on multiple counters.</returns>
		// Token: 0x17000D43 RID: 3395
		// (get) Token: 0x0600345E RID: 13406 RVA: 0x000A5440 File Offset: 0x000A3640
		public long BaseValue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0L;
			}
		}

		/// <summary>Gets the raw counter frequency.</summary>
		/// <returns>The frequency with which the counter is read.</returns>
		// Token: 0x17000D44 RID: 3396
		// (get) Token: 0x0600345F RID: 13407 RVA: 0x000A545C File Offset: 0x000A365C
		public long CounterFrequency
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0L;
			}
		}

		/// <summary>Gets the counter's time stamp.</summary>
		/// <returns>The time at which the sample was taken.</returns>
		// Token: 0x17000D45 RID: 3397
		// (get) Token: 0x06003460 RID: 13408 RVA: 0x000A5478 File Offset: 0x000A3678
		public long CounterTimeStamp
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0L;
			}
		}

		/// <summary>Gets the performance counter type.</summary>
		/// <returns>A <see cref="T:System.Diagnostics.PerformanceCounterType" /> object that indicates the type of the counter for which this sample is a snapshot.</returns>
		// Token: 0x17000D46 RID: 3398
		// (get) Token: 0x06003461 RID: 13409 RVA: 0x000A5494 File Offset: 0x000A3694
		public PerformanceCounterType CounterType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return PerformanceCounterType.NumberOfItemsHEX32;
			}
		}

		/// <summary>Gets the raw value of the counter.</summary>
		/// <returns>The numeric value that is associated with the performance counter sample.</returns>
		// Token: 0x17000D47 RID: 3399
		// (get) Token: 0x06003462 RID: 13410 RVA: 0x000A54B0 File Offset: 0x000A36B0
		public long RawValue
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0L;
			}
		}

		/// <summary>Gets the raw system frequency.</summary>
		/// <returns>The frequency with which the system reads from the counter.</returns>
		// Token: 0x17000D48 RID: 3400
		// (get) Token: 0x06003463 RID: 13411 RVA: 0x000A54CC File Offset: 0x000A36CC
		public long SystemFrequency
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0L;
			}
		}

		/// <summary>Gets the raw time stamp.</summary>
		/// <returns>The system time stamp.</returns>
		// Token: 0x17000D49 RID: 3401
		// (get) Token: 0x06003464 RID: 13412 RVA: 0x000A54E8 File Offset: 0x000A36E8
		public long TimeStamp
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0L;
			}
		}

		/// <summary>Gets the raw, high-fidelity time stamp.</summary>
		/// <returns>The system time stamp, represented within 0.1 millisecond.</returns>
		// Token: 0x17000D4A RID: 3402
		// (get) Token: 0x06003465 RID: 13413 RVA: 0x000A5504 File Offset: 0x000A3704
		public long TimeStamp100nSec
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0L;
			}
		}

		/// <summary>Calculates the performance data of the counter, using a single sample point. This method is generally used for uncalculated performance counter types.</summary>
		/// <param name="counterSample">The <see cref="T:System.Diagnostics.CounterSample" /> structure to use as a base point for calculating performance data. </param>
		/// <returns>The calculated performance value.</returns>
		// Token: 0x06003466 RID: 13414 RVA: 0x000A5520 File Offset: 0x000A3720
		public static float Calculate(CounterSample counterSample)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0f;
		}

		/// <summary>Calculates the performance data of the counter, using two sample points. This method is generally used for calculated performance counter types, such as averages.</summary>
		/// <param name="counterSample">The <see cref="T:System.Diagnostics.CounterSample" /> structure to use as a base point for calculating performance data. </param>
		/// <param name="nextCounterSample">The <see cref="T:System.Diagnostics.CounterSample" /> structure to use as an ending point for calculating performance data. </param>
		/// <returns>The calculated performance value.</returns>
		// Token: 0x06003467 RID: 13415 RVA: 0x000A553C File Offset: 0x000A373C
		public static float Calculate(CounterSample counterSample, CounterSample nextCounterSample)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0f;
		}

		/// <summary>Indicates whether the specified <see cref="T:System.Diagnostics.CounterSample" /> structure is equal to the current <see cref="T:System.Diagnostics.CounterSample" /> structure.</summary>
		/// <param name="sample">The <see cref="T:System.Diagnostics.CounterSample" /> structure to be compared with this instance.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="sample" /> is equal to the current instance; otherwise, <see langword="false" />. </returns>
		// Token: 0x06003468 RID: 13416 RVA: 0x000A5558 File Offset: 0x000A3758
		public bool Equals(CounterSample sample)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a value that indicates whether two <see cref="T:System.Diagnostics.CounterSample" /> structures are equal.</summary>
		/// <param name="a">A <see cref="T:System.Diagnostics.CounterSample" /> structure.</param>
		/// <param name="b">Another <see cref="T:System.Diagnostics.CounterSample" /> structure to be compared to the structure specified by the <paramref name="a" /> parameter.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="a" /> and <paramref name="b" /> are equal; otherwise, <see langword="false" />.</returns>
		// Token: 0x06003469 RID: 13417 RVA: 0x000A5574 File Offset: 0x000A3774
		public static bool operator ==(CounterSample a, CounterSample b)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Returns a value that indicates whether two <see cref="T:System.Diagnostics.CounterSample" /> structures are not equal.</summary>
		/// <param name="a">A <see cref="T:System.Diagnostics.CounterSample" /> structure.</param>
		/// <param name="b">Another <see cref="T:System.Diagnostics.CounterSample" /> structure to be compared to the structure specified by the <paramref name="a" /> parameter.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="a" /> and <paramref name="b" /> are not equal; otherwise, <see langword="false" /></returns>
		// Token: 0x0600346A RID: 13418 RVA: 0x000A5590 File Offset: 0x000A3790
		public static bool operator !=(CounterSample a, CounterSample b)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Defines an empty, uninitialized performance counter sample of type <see langword="NumberOfItems32" />.</summary>
		// Token: 0x0400250E RID: 9486
		public static CounterSample Empty;
	}
}
