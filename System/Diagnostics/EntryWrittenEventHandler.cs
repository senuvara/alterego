﻿using System;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Represents the method that will handle the <see cref="E:System.Diagnostics.EventLog.EntryWritten" /> event of an <see cref="T:System.Diagnostics.EventLog" />.</summary>
	/// <param name="sender">The source of the event. </param>
	/// <param name="e">An <see cref="T:System.Diagnostics.EntryWrittenEventArgs" /> that contains the event data. </param>
	// Token: 0x0200061D RID: 1565
	public sealed class EntryWrittenEventHandler : MulticastDelegate
	{
		// Token: 0x06003258 RID: 12888 RVA: 0x000092E2 File Offset: 0x000074E2
		public EntryWrittenEventHandler(object @object, IntPtr method)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06003259 RID: 12889 RVA: 0x000092E2 File Offset: 0x000074E2
		public void Invoke(object sender, EntryWrittenEventArgs e)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0600325A RID: 12890 RVA: 0x00043C3C File Offset: 0x00041E3C
		public IAsyncResult BeginInvoke(object sender, EntryWrittenEventArgs e, AsyncCallback callback, object @object)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x0600325B RID: 12891 RVA: 0x000092E2 File Offset: 0x000074E2
		public void EndInvoke(IAsyncResult result)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
