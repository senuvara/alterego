﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Represents a.dll or .exe file that is loaded into a particular process.</summary>
	// Token: 0x02000116 RID: 278
	[Designer("System.Diagnostics.Design.ProcessModuleDesigner, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
	public class ProcessModule : Component
	{
		// Token: 0x060008F0 RID: 2288 RVA: 0x00029E28 File Offset: 0x00028028
		internal ProcessModule(IntPtr baseaddr, IntPtr entryaddr, string filename, FileVersionInfo version_info, int memory_size, string modulename)
		{
			this.baseaddr = baseaddr;
			this.entryaddr = entryaddr;
			this.filename = filename;
			this.version_info = version_info;
			this.memory_size = memory_size;
			this.modulename = modulename;
		}

		/// <summary>Gets the memory address where the module was loaded.</summary>
		/// <returns>The load address of the module.</returns>
		// Token: 0x170001C9 RID: 457
		// (get) Token: 0x060008F1 RID: 2289 RVA: 0x00029E5D File Offset: 0x0002805D
		[MonitoringDescription("The base memory address of this module")]
		public IntPtr BaseAddress
		{
			get
			{
				return this.baseaddr;
			}
		}

		/// <summary>Gets the memory address for the function that runs when the system loads and runs the module.</summary>
		/// <returns>The entry point of the module.</returns>
		// Token: 0x170001CA RID: 458
		// (get) Token: 0x060008F2 RID: 2290 RVA: 0x00029E65 File Offset: 0x00028065
		[MonitoringDescription("The base memory address of the entry point of this module")]
		public IntPtr EntryPointAddress
		{
			get
			{
				return this.entryaddr;
			}
		}

		/// <summary>Gets the full path to the module.</summary>
		/// <returns>The fully qualified path that defines the location of the module.</returns>
		// Token: 0x170001CB RID: 459
		// (get) Token: 0x060008F3 RID: 2291 RVA: 0x00029E6D File Offset: 0x0002806D
		[MonitoringDescription("The file name of this module")]
		public string FileName
		{
			get
			{
				return this.filename;
			}
		}

		/// <summary>Gets version information about the module.</summary>
		/// <returns>A <see cref="T:System.Diagnostics.FileVersionInfo" /> that contains the module's version information.</returns>
		// Token: 0x170001CC RID: 460
		// (get) Token: 0x060008F4 RID: 2292 RVA: 0x00029E75 File Offset: 0x00028075
		[Browsable(false)]
		public FileVersionInfo FileVersionInfo
		{
			get
			{
				return this.version_info;
			}
		}

		/// <summary>Gets the amount of memory that is required to load the module.</summary>
		/// <returns>The size, in bytes, of the memory that the module occupies.</returns>
		// Token: 0x170001CD RID: 461
		// (get) Token: 0x060008F5 RID: 2293 RVA: 0x00029E7D File Offset: 0x0002807D
		[MonitoringDescription("The memory needed by this module")]
		public int ModuleMemorySize
		{
			get
			{
				return this.memory_size;
			}
		}

		/// <summary>Gets the name of the process module.</summary>
		/// <returns>The name of the module.</returns>
		// Token: 0x170001CE RID: 462
		// (get) Token: 0x060008F6 RID: 2294 RVA: 0x00029E85 File Offset: 0x00028085
		[MonitoringDescription("The name of this module")]
		public string ModuleName
		{
			get
			{
				return this.modulename;
			}
		}

		/// <summary>Converts the name of the module to a string.</summary>
		/// <returns>The value of the <see cref="P:System.Diagnostics.ProcessModule.ModuleName" /> property.</returns>
		// Token: 0x060008F7 RID: 2295 RVA: 0x00029E8D File Offset: 0x0002808D
		public override string ToString()
		{
			return this.ModuleName;
		}

		// Token: 0x060008F8 RID: 2296 RVA: 0x000092E2 File Offset: 0x000074E2
		internal ProcessModule()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000BED RID: 3053
		private IntPtr baseaddr;

		// Token: 0x04000BEE RID: 3054
		private IntPtr entryaddr;

		// Token: 0x04000BEF RID: 3055
		private string filename;

		// Token: 0x04000BF0 RID: 3056
		private FileVersionInfo version_info;

		// Token: 0x04000BF1 RID: 3057
		private int memory_size;

		// Token: 0x04000BF2 RID: 3058
		private string modulename;
	}
}
