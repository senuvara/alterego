﻿using System;
using System.Collections;

namespace System.Diagnostics
{
	// Token: 0x0200010A RID: 266
	internal class OrdinalCaseInsensitiveComparer : IComparer
	{
		// Token: 0x0600086C RID: 2156 RVA: 0x00029320 File Offset: 0x00027520
		public int Compare(object a, object b)
		{
			string text = a as string;
			string text2 = b as string;
			if (text != null && text2 != null)
			{
				return string.Compare(text, text2, StringComparison.OrdinalIgnoreCase);
			}
			return Comparer.Default.Compare(a, b);
		}

		// Token: 0x0600086D RID: 2157 RVA: 0x0000232F File Offset: 0x0000052F
		public OrdinalCaseInsensitiveComparer()
		{
		}

		// Token: 0x0600086E RID: 2158 RVA: 0x00029356 File Offset: 0x00027556
		// Note: this type is marked as 'beforefieldinit'.
		static OrdinalCaseInsensitiveComparer()
		{
		}

		// Token: 0x04000BAE RID: 2990
		internal static readonly OrdinalCaseInsensitiveComparer Default = new OrdinalCaseInsensitiveComparer();
	}
}
