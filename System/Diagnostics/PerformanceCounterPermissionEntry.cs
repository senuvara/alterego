﻿using System;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Defines the smallest unit of a code access security permission that is set for a <see cref="T:System.Diagnostics.PerformanceCounter" />.</summary>
	// Token: 0x02000678 RID: 1656
	[Serializable]
	public class PerformanceCounterPermissionEntry
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.PerformanceCounterPermissionEntry" /> class.</summary>
		/// <param name="permissionAccess">A bitwise combination of the <see cref="T:System.Diagnostics.PerformanceCounterPermissionAccess" /> values. The <see cref="P:System.Diagnostics.PerformanceCounterPermissionEntry.PermissionAccess" /> property is set to this value. </param>
		/// <param name="machineName">The server on which the category of the performance counter resides. </param>
		/// <param name="categoryName">The name of the performance counter category (performance object) with which this performance counter is associated. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="categoryName" /> is <see langword="null" />.-or-
		///         <paramref name="machineName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="permissionAccess" /> is not a valid <see cref="T:System.Diagnostics.PerformanceCounterPermissionAccess" /> value.-or-
		///         <paramref name="machineName" /> is not a valid computer name.</exception>
		// Token: 0x060034E6 RID: 13542 RVA: 0x000092E2 File Offset: 0x000074E2
		public PerformanceCounterPermissionEntry(PerformanceCounterPermissionAccess permissionAccess, string machineName, string categoryName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the name of the performance counter category (performance object).</summary>
		/// <returns>The name of the performance counter category (performance object).</returns>
		// Token: 0x17000D6A RID: 3434
		// (get) Token: 0x060034E7 RID: 13543 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string CategoryName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the name of the server on which the category of the performance counter resides.</summary>
		/// <returns>The name of the server on which the category resides.</returns>
		// Token: 0x17000D6B RID: 3435
		// (get) Token: 0x060034E8 RID: 13544 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string MachineName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the permission access level of the entry.</summary>
		/// <returns>A bitwise combination of the <see cref="T:System.Diagnostics.PerformanceCounterPermissionAccess" /> values.</returns>
		// Token: 0x17000D6C RID: 3436
		// (get) Token: 0x060034E9 RID: 13545 RVA: 0x000A58D8 File Offset: 0x000A3AD8
		public PerformanceCounterPermissionAccess PermissionAccess
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return PerformanceCounterPermissionAccess.None;
			}
		}
	}
}
