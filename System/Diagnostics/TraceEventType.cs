﻿using System;
using System.ComponentModel;

namespace System.Diagnostics
{
	/// <summary>Identifies the type of event that has caused the trace.</summary>
	// Token: 0x020000F8 RID: 248
	public enum TraceEventType
	{
		/// <summary>Fatal error or application crash.</summary>
		// Token: 0x04000B2C RID: 2860
		Critical = 1,
		/// <summary>Recoverable error.</summary>
		// Token: 0x04000B2D RID: 2861
		Error,
		/// <summary>Noncritical problem.</summary>
		// Token: 0x04000B2E RID: 2862
		Warning = 4,
		/// <summary>Informational message.</summary>
		// Token: 0x04000B2F RID: 2863
		Information = 8,
		/// <summary>Debugging trace.</summary>
		// Token: 0x04000B30 RID: 2864
		Verbose = 16,
		/// <summary>Starting of a logical operation.</summary>
		// Token: 0x04000B31 RID: 2865
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		Start = 256,
		/// <summary>Stopping of a logical operation.</summary>
		// Token: 0x04000B32 RID: 2866
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		Stop = 512,
		/// <summary>Suspension of a logical operation.</summary>
		// Token: 0x04000B33 RID: 2867
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		Suspend = 1024,
		/// <summary>Resumption of a logical operation.</summary>
		// Token: 0x04000B34 RID: 2868
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		Resume = 2048,
		/// <summary>Changing of correlation identity.</summary>
		// Token: 0x04000B35 RID: 2869
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		Transfer = 4096
	}
}
