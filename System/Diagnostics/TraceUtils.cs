﻿using System;
using System.Collections;
using System.Globalization;

namespace System.Diagnostics
{
	// Token: 0x02000101 RID: 257
	internal static class TraceUtils
	{
		// Token: 0x060007D1 RID: 2001 RVA: 0x00008B3F File Offset: 0x00006D3F
		internal static object GetRuntimeObject(string className, Type baseType, string initializeData)
		{
			return null;
		}

		// Token: 0x060007D2 RID: 2002 RVA: 0x00026CFE File Offset: 0x00024EFE
		private static object ConvertToBaseTypeOrEnum(string value, Type type)
		{
			if (type.IsEnum)
			{
				return Enum.Parse(type, value, false);
			}
			return Convert.ChangeType(value, type, CultureInfo.InvariantCulture);
		}

		// Token: 0x060007D3 RID: 2003 RVA: 0x0000232D File Offset: 0x0000052D
		internal static void VerifyAttributes(IDictionary attributes, string[] supportedAttributes, object parent)
		{
		}
	}
}
