﻿using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace System.Diagnostics
{
	/// <summary>Provides the default output methods and behavior for tracing.</summary>
	// Token: 0x02000113 RID: 275
	public class DefaultTraceListener : TraceListener
	{
		// Token: 0x060008BD RID: 2237 RVA: 0x000297EC File Offset: 0x000279EC
		static DefaultTraceListener()
		{
			if (!DefaultTraceListener.OnWin32)
			{
				string text = Environment.GetEnvironmentVariable("MONO_TRACE_LISTENER");
				if (text == null)
				{
					text = "Console.Out";
				}
				if (text != null)
				{
					string monoTraceFile;
					string monoTracePrefix;
					if (text.StartsWith("Console.Out"))
					{
						monoTraceFile = "Console.Out";
						monoTracePrefix = DefaultTraceListener.GetPrefix(text, "Console.Out");
					}
					else if (text.StartsWith("Console.Error"))
					{
						monoTraceFile = "Console.Error";
						monoTracePrefix = DefaultTraceListener.GetPrefix(text, "Console.Error");
					}
					else
					{
						monoTraceFile = text;
						monoTracePrefix = "";
					}
					DefaultTraceListener.MonoTraceFile = monoTraceFile;
					DefaultTraceListener.MonoTracePrefix = monoTracePrefix;
				}
			}
		}

		// Token: 0x060008BE RID: 2238 RVA: 0x0002987F File Offset: 0x00027A7F
		private static string GetPrefix(string var, string target)
		{
			if (var.Length > target.Length)
			{
				return var.Substring(target.Length + 1);
			}
			return "";
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.DefaultTraceListener" /> class with "Default" as its <see cref="P:System.Diagnostics.TraceListener.Name" /> property value.</summary>
		// Token: 0x060008BF RID: 2239 RVA: 0x000298A3 File Offset: 0x00027AA3
		public DefaultTraceListener() : base("Default")
		{
		}

		/// <summary>Gets or sets a value indicating whether the application is running in user-interface mode.</summary>
		/// <returns>
		///     <see langword="true" /> if user-interface mode is enabled; otherwise, <see langword="false" />.</returns>
		// Token: 0x170001AB RID: 427
		// (get) Token: 0x060008C0 RID: 2240 RVA: 0x000298B0 File Offset: 0x00027AB0
		// (set) Token: 0x060008C1 RID: 2241 RVA: 0x000298B8 File Offset: 0x00027AB8
		[MonoTODO("AssertUiEnabled defaults to False; should follow Environment.UserInteractive.")]
		public bool AssertUiEnabled
		{
			get
			{
				return this.assertUiEnabled;
			}
			set
			{
				this.assertUiEnabled = value;
			}
		}

		/// <summary>Gets or sets the name of a log file to write trace or debug messages to.</summary>
		/// <returns>The name of a log file to write trace or debug messages to.</returns>
		// Token: 0x170001AC RID: 428
		// (get) Token: 0x060008C2 RID: 2242 RVA: 0x000298C1 File Offset: 0x00027AC1
		// (set) Token: 0x060008C3 RID: 2243 RVA: 0x000298C9 File Offset: 0x00027AC9
		[MonoTODO]
		public string LogFileName
		{
			get
			{
				return this.logFileName;
			}
			set
			{
				this.logFileName = value;
			}
		}

		/// <summary>Emits or displays a message and a stack trace for an assertion that always fails.</summary>
		/// <param name="message">The message to emit or display. </param>
		// Token: 0x060008C4 RID: 2244 RVA: 0x000298D2 File Offset: 0x00027AD2
		public override void Fail(string message)
		{
			base.Fail(message);
		}

		/// <summary>Emits or displays detailed messages and a stack trace for an assertion that always fails.</summary>
		/// <param name="message">The message to emit or display. </param>
		/// <param name="detailMessage">The detailed message to emit or display. </param>
		// Token: 0x060008C5 RID: 2245 RVA: 0x000298DB File Offset: 0x00027ADB
		public override void Fail(string message, string detailMessage)
		{
			base.Fail(message, detailMessage);
			this.WriteLine(new StackTrace().ToString());
		}

		// Token: 0x060008C6 RID: 2246
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void WriteWindowsDebugString(string message);

		// Token: 0x060008C7 RID: 2247 RVA: 0x000298F5 File Offset: 0x00027AF5
		private void WriteDebugString(string message)
		{
			if (DefaultTraceListener.OnWin32)
			{
				DefaultTraceListener.WriteWindowsDebugString(message);
				return;
			}
			this.WriteMonoTrace(message);
		}

		// Token: 0x060008C8 RID: 2248 RVA: 0x0002990C File Offset: 0x00027B0C
		private void WriteMonoTrace(string message)
		{
			string monoTraceFile = DefaultTraceListener.MonoTraceFile;
			if (monoTraceFile == "Console.Out")
			{
				Console.Out.Write(message);
				return;
			}
			if (!(monoTraceFile == "Console.Error"))
			{
				this.WriteLogFile(message, DefaultTraceListener.MonoTraceFile);
				return;
			}
			Console.Error.Write(message);
		}

		// Token: 0x060008C9 RID: 2249 RVA: 0x0002995F File Offset: 0x00027B5F
		private void WritePrefix()
		{
			if (!DefaultTraceListener.OnWin32)
			{
				this.WriteMonoTrace(DefaultTraceListener.MonoTracePrefix);
			}
		}

		// Token: 0x060008CA RID: 2250 RVA: 0x00029973 File Offset: 0x00027B73
		private void WriteImpl(string message)
		{
			if (base.NeedIndent)
			{
				this.WriteIndent();
				this.WritePrefix();
			}
			if (Debugger.IsLogging())
			{
				Debugger.Log(0, null, message);
			}
			else
			{
				this.WriteDebugString(message);
			}
			this.WriteLogFile(message, this.LogFileName);
		}

		// Token: 0x060008CB RID: 2251 RVA: 0x000299B0 File Offset: 0x00027BB0
		private void WriteLogFile(string message, string logFile)
		{
			if (logFile != null && logFile.Length != 0)
			{
				FileInfo fileInfo = new FileInfo(logFile);
				StreamWriter streamWriter = null;
				try
				{
					if (fileInfo.Exists)
					{
						streamWriter = fileInfo.AppendText();
					}
					else
					{
						streamWriter = fileInfo.CreateText();
					}
				}
				catch
				{
					return;
				}
				using (streamWriter)
				{
					streamWriter.Write(message);
					streamWriter.Flush();
				}
			}
		}

		/// <summary>Writes the output to the <see langword="OutputDebugString" /> function and to the <see cref="M:System.Diagnostics.Debugger.Log(System.Int32,System.String,System.String)" /> method.</summary>
		/// <param name="message">The message to write to <see langword="OutputDebugString" /> and <see cref="M:System.Diagnostics.Debugger.Log(System.Int32,System.String,System.String)" />. </param>
		// Token: 0x060008CC RID: 2252 RVA: 0x00029A28 File Offset: 0x00027C28
		public override void Write(string message)
		{
			this.WriteImpl(message);
		}

		/// <summary>Writes the output to the <see langword="OutputDebugString" /> function and to the <see cref="M:System.Diagnostics.Debugger.Log(System.Int32,System.String,System.String)" /> method, followed by a carriage return and line feed (\r\n).</summary>
		/// <param name="message">The message to write to <see langword="OutputDebugString" /> and <see cref="M:System.Diagnostics.Debugger.Log(System.Int32,System.String,System.String)" />. </param>
		// Token: 0x060008CD RID: 2253 RVA: 0x00029A34 File Offset: 0x00027C34
		public override void WriteLine(string message)
		{
			string message2 = message + Environment.NewLine;
			this.WriteImpl(message2);
			base.NeedIndent = true;
		}

		// Token: 0x04000BCB RID: 3019
		private static readonly bool OnWin32 = Path.DirectorySeparatorChar == '\\';

		// Token: 0x04000BCC RID: 3020
		private const string ConsoleOutTrace = "Console.Out";

		// Token: 0x04000BCD RID: 3021
		private const string ConsoleErrorTrace = "Console.Error";

		// Token: 0x04000BCE RID: 3022
		private static readonly string MonoTracePrefix;

		// Token: 0x04000BCF RID: 3023
		private static readonly string MonoTraceFile;

		// Token: 0x04000BD0 RID: 3024
		private string logFileName;

		// Token: 0x04000BD1 RID: 3025
		private bool assertUiEnabled;
	}
}
