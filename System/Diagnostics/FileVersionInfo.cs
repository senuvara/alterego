﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Security.Permissions;
using System.Text;

namespace System.Diagnostics
{
	/// <summary>Provides version information for a physical file on disk.</summary>
	// Token: 0x02000114 RID: 276
	[PermissionSet(SecurityAction.LinkDemand, Unrestricted = true)]
	public sealed class FileVersionInfo
	{
		// Token: 0x060008CE RID: 2254 RVA: 0x00029A5C File Offset: 0x00027C5C
		private FileVersionInfo()
		{
			this.comments = null;
			this.companyname = null;
			this.filedescription = null;
			this.filename = null;
			this.fileversion = null;
			this.internalname = null;
			this.language = null;
			this.legalcopyright = null;
			this.legaltrademarks = null;
			this.originalfilename = null;
			this.privatebuild = null;
			this.productname = null;
			this.productversion = null;
			this.specialbuild = null;
			this.isdebug = false;
			this.ispatched = false;
			this.isprerelease = false;
			this.isprivatebuild = false;
			this.isspecialbuild = false;
			this.filemajorpart = 0;
			this.fileminorpart = 0;
			this.filebuildpart = 0;
			this.fileprivatepart = 0;
			this.productmajorpart = 0;
			this.productminorpart = 0;
			this.productbuildpart = 0;
			this.productprivatepart = 0;
		}

		/// <summary>Gets the comments associated with the file.</summary>
		/// <returns>The comments associated with the file or <see langword="null" /> if the file did not contain version information.</returns>
		// Token: 0x170001AD RID: 429
		// (get) Token: 0x060008CF RID: 2255 RVA: 0x00029B2C File Offset: 0x00027D2C
		public string Comments
		{
			get
			{
				return this.comments;
			}
		}

		/// <summary>Gets the name of the company that produced the file.</summary>
		/// <returns>The name of the company that produced the file or <see langword="null" /> if the file did not contain version information.</returns>
		// Token: 0x170001AE RID: 430
		// (get) Token: 0x060008D0 RID: 2256 RVA: 0x00029B34 File Offset: 0x00027D34
		public string CompanyName
		{
			get
			{
				return this.companyname;
			}
		}

		/// <summary>Gets the build number of the file.</summary>
		/// <returns>A value representing the build number of the file or <see langword="null" /> if the file did not contain version information.</returns>
		// Token: 0x170001AF RID: 431
		// (get) Token: 0x060008D1 RID: 2257 RVA: 0x00029B3C File Offset: 0x00027D3C
		public int FileBuildPart
		{
			get
			{
				return this.filebuildpart;
			}
		}

		/// <summary>Gets the description of the file.</summary>
		/// <returns>The description of the file or <see langword="null" /> if the file did not contain version information.</returns>
		// Token: 0x170001B0 RID: 432
		// (get) Token: 0x060008D2 RID: 2258 RVA: 0x00029B44 File Offset: 0x00027D44
		public string FileDescription
		{
			get
			{
				return this.filedescription;
			}
		}

		/// <summary>Gets the major part of the version number.</summary>
		/// <returns>A value representing the major part of the version number or 0 (zero) if the file did not contain version information.</returns>
		// Token: 0x170001B1 RID: 433
		// (get) Token: 0x060008D3 RID: 2259 RVA: 0x00029B4C File Offset: 0x00027D4C
		public int FileMajorPart
		{
			get
			{
				return this.filemajorpart;
			}
		}

		/// <summary>Gets the minor part of the version number of the file.</summary>
		/// <returns>A value representing the minor part of the version number of the file or 0 (zero) if the file did not contain version information.</returns>
		// Token: 0x170001B2 RID: 434
		// (get) Token: 0x060008D4 RID: 2260 RVA: 0x00029B54 File Offset: 0x00027D54
		public int FileMinorPart
		{
			get
			{
				return this.fileminorpart;
			}
		}

		/// <summary>Gets the name of the file that this instance of <see cref="T:System.Diagnostics.FileVersionInfo" /> describes.</summary>
		/// <returns>The name of the file described by this instance of <see cref="T:System.Diagnostics.FileVersionInfo" />.</returns>
		// Token: 0x170001B3 RID: 435
		// (get) Token: 0x060008D5 RID: 2261 RVA: 0x00029B5C File Offset: 0x00027D5C
		public string FileName
		{
			get
			{
				return this.filename;
			}
		}

		/// <summary>Gets the file private part number.</summary>
		/// <returns>A value representing the file private part number or <see langword="null" /> if the file did not contain version information.</returns>
		// Token: 0x170001B4 RID: 436
		// (get) Token: 0x060008D6 RID: 2262 RVA: 0x00029B64 File Offset: 0x00027D64
		public int FilePrivatePart
		{
			get
			{
				return this.fileprivatepart;
			}
		}

		/// <summary>Gets the file version number.</summary>
		/// <returns>The version number of the file or <see langword="null" /> if the file did not contain version information.</returns>
		// Token: 0x170001B5 RID: 437
		// (get) Token: 0x060008D7 RID: 2263 RVA: 0x00029B6C File Offset: 0x00027D6C
		public string FileVersion
		{
			get
			{
				return this.fileversion;
			}
		}

		/// <summary>Gets the internal name of the file, if one exists.</summary>
		/// <returns>The internal name of the file. If none exists, this property will contain the original name of the file without the extension.</returns>
		// Token: 0x170001B6 RID: 438
		// (get) Token: 0x060008D8 RID: 2264 RVA: 0x00029B74 File Offset: 0x00027D74
		public string InternalName
		{
			get
			{
				return this.internalname;
			}
		}

		/// <summary>Gets a value that specifies whether the file contains debugging information or is compiled with debugging features enabled.</summary>
		/// <returns>
		///     <see langword="true" /> if the file contains debugging information or is compiled with debugging features enabled; otherwise, <see langword="false" />.</returns>
		// Token: 0x170001B7 RID: 439
		// (get) Token: 0x060008D9 RID: 2265 RVA: 0x00029B7C File Offset: 0x00027D7C
		public bool IsDebug
		{
			get
			{
				return this.isdebug;
			}
		}

		/// <summary>Gets a value that specifies whether the file has been modified and is not identical to the original shipping file of the same version number.</summary>
		/// <returns>
		///     <see langword="true" /> if the file is patched; otherwise, <see langword="false" />.</returns>
		// Token: 0x170001B8 RID: 440
		// (get) Token: 0x060008DA RID: 2266 RVA: 0x00029B84 File Offset: 0x00027D84
		public bool IsPatched
		{
			get
			{
				return this.ispatched;
			}
		}

		/// <summary>Gets a value that specifies whether the file is a development version, rather than a commercially released product.</summary>
		/// <returns>
		///     <see langword="true" /> if the file is prerelease; otherwise, <see langword="false" />.</returns>
		// Token: 0x170001B9 RID: 441
		// (get) Token: 0x060008DB RID: 2267 RVA: 0x00029B8C File Offset: 0x00027D8C
		public bool IsPreRelease
		{
			get
			{
				return this.isprerelease;
			}
		}

		/// <summary>Gets a value that specifies whether the file was built using standard release procedures.</summary>
		/// <returns>
		///     <see langword="true" /> if the file is a private build; <see langword="false" /> if the file was built using standard release procedures or if the file did not contain version information.</returns>
		// Token: 0x170001BA RID: 442
		// (get) Token: 0x060008DC RID: 2268 RVA: 0x00029B94 File Offset: 0x00027D94
		public bool IsPrivateBuild
		{
			get
			{
				return this.isprivatebuild;
			}
		}

		/// <summary>Gets a value that specifies whether the file is a special build.</summary>
		/// <returns>
		///     <see langword="true" /> if the file is a special build; otherwise, <see langword="false" />.</returns>
		// Token: 0x170001BB RID: 443
		// (get) Token: 0x060008DD RID: 2269 RVA: 0x00029B9C File Offset: 0x00027D9C
		public bool IsSpecialBuild
		{
			get
			{
				return this.isspecialbuild;
			}
		}

		/// <summary>Gets the default language string for the version info block.</summary>
		/// <returns>The description string for the Microsoft Language Identifier in the version resource or <see langword="null" /> if the file did not contain version information.</returns>
		// Token: 0x170001BC RID: 444
		// (get) Token: 0x060008DE RID: 2270 RVA: 0x00029BA4 File Offset: 0x00027DA4
		public string Language
		{
			get
			{
				return this.language;
			}
		}

		/// <summary>Gets all copyright notices that apply to the specified file.</summary>
		/// <returns>The copyright notices that apply to the specified file.</returns>
		// Token: 0x170001BD RID: 445
		// (get) Token: 0x060008DF RID: 2271 RVA: 0x00029BAC File Offset: 0x00027DAC
		public string LegalCopyright
		{
			get
			{
				return this.legalcopyright;
			}
		}

		/// <summary>Gets the trademarks and registered trademarks that apply to the file.</summary>
		/// <returns>The trademarks and registered trademarks that apply to the file or <see langword="null" /> if the file did not contain version information.</returns>
		// Token: 0x170001BE RID: 446
		// (get) Token: 0x060008E0 RID: 2272 RVA: 0x00029BB4 File Offset: 0x00027DB4
		public string LegalTrademarks
		{
			get
			{
				return this.legaltrademarks;
			}
		}

		/// <summary>Gets the name the file was created with.</summary>
		/// <returns>The name the file was created with or <see langword="null" /> if the file did not contain version information.</returns>
		// Token: 0x170001BF RID: 447
		// (get) Token: 0x060008E1 RID: 2273 RVA: 0x00029BBC File Offset: 0x00027DBC
		public string OriginalFilename
		{
			get
			{
				return this.originalfilename;
			}
		}

		/// <summary>Gets information about a private version of the file.</summary>
		/// <returns>Information about a private version of the file or <see langword="null" /> if the file did not contain version information.</returns>
		// Token: 0x170001C0 RID: 448
		// (get) Token: 0x060008E2 RID: 2274 RVA: 0x00029BC4 File Offset: 0x00027DC4
		public string PrivateBuild
		{
			get
			{
				return this.privatebuild;
			}
		}

		/// <summary>Gets the build number of the product this file is associated with.</summary>
		/// <returns>A value representing the build number of the product this file is associated with or <see langword="null" /> if the file did not contain version information.</returns>
		// Token: 0x170001C1 RID: 449
		// (get) Token: 0x060008E3 RID: 2275 RVA: 0x00029BCC File Offset: 0x00027DCC
		public int ProductBuildPart
		{
			get
			{
				return this.productbuildpart;
			}
		}

		/// <summary>Gets the major part of the version number for the product this file is associated with.</summary>
		/// <returns>A value representing the major part of the product version number or <see langword="null" /> if the file did not contain version information.</returns>
		// Token: 0x170001C2 RID: 450
		// (get) Token: 0x060008E4 RID: 2276 RVA: 0x00029BD4 File Offset: 0x00027DD4
		public int ProductMajorPart
		{
			get
			{
				return this.productmajorpart;
			}
		}

		/// <summary>Gets the minor part of the version number for the product the file is associated with.</summary>
		/// <returns>A value representing the minor part of the product version number or <see langword="null" /> if the file did not contain version information.</returns>
		// Token: 0x170001C3 RID: 451
		// (get) Token: 0x060008E5 RID: 2277 RVA: 0x00029BDC File Offset: 0x00027DDC
		public int ProductMinorPart
		{
			get
			{
				return this.productminorpart;
			}
		}

		/// <summary>Gets the name of the product this file is distributed with.</summary>
		/// <returns>The name of the product this file is distributed with or <see langword="null" /> if the file did not contain version information.</returns>
		// Token: 0x170001C4 RID: 452
		// (get) Token: 0x060008E6 RID: 2278 RVA: 0x00029BE4 File Offset: 0x00027DE4
		public string ProductName
		{
			get
			{
				return this.productname;
			}
		}

		/// <summary>Gets the private part number of the product this file is associated with.</summary>
		/// <returns>A value representing the private part number of the product this file is associated with or <see langword="null" /> if the file did not contain version information.</returns>
		// Token: 0x170001C5 RID: 453
		// (get) Token: 0x060008E7 RID: 2279 RVA: 0x00029BEC File Offset: 0x00027DEC
		public int ProductPrivatePart
		{
			get
			{
				return this.productprivatepart;
			}
		}

		/// <summary>Gets the version of the product this file is distributed with.</summary>
		/// <returns>The version of the product this file is distributed with or <see langword="null" /> if the file did not contain version information.</returns>
		// Token: 0x170001C6 RID: 454
		// (get) Token: 0x060008E8 RID: 2280 RVA: 0x00029BF4 File Offset: 0x00027DF4
		public string ProductVersion
		{
			get
			{
				return this.productversion;
			}
		}

		/// <summary>Gets the special build information for the file.</summary>
		/// <returns>The special build information for the file or <see langword="null" /> if the file did not contain version information.</returns>
		// Token: 0x170001C7 RID: 455
		// (get) Token: 0x060008E9 RID: 2281 RVA: 0x00029BFC File Offset: 0x00027DFC
		public string SpecialBuild
		{
			get
			{
				return this.specialbuild;
			}
		}

		// Token: 0x060008EA RID: 2282
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetVersionInfo_internal(string fileName);

		/// <summary>Returns a <see cref="T:System.Diagnostics.FileVersionInfo" /> representing the version information associated with the specified file.</summary>
		/// <param name="fileName">The fully qualified path and name of the file to retrieve the version information for. </param>
		/// <returns>A <see cref="T:System.Diagnostics.FileVersionInfo" /> containing information about the file. If the file did not contain version information, the <see cref="T:System.Diagnostics.FileVersionInfo" /> contains only the name of the file requested.</returns>
		/// <exception cref="T:System.IO.FileNotFoundException">The file specified cannot be found. </exception>
		// Token: 0x060008EB RID: 2283 RVA: 0x00029C04 File Offset: 0x00027E04
		public static FileVersionInfo GetVersionInfo(string fileName)
		{
			if (!File.Exists(Path.GetFullPath(fileName)))
			{
				throw new FileNotFoundException(fileName);
			}
			FileVersionInfo fileVersionInfo = new FileVersionInfo();
			fileVersionInfo.GetVersionInfo_internal(fileName);
			return fileVersionInfo;
		}

		// Token: 0x060008EC RID: 2284 RVA: 0x00029C26 File Offset: 0x00027E26
		private static void AppendFormat(StringBuilder sb, string format, params object[] args)
		{
			sb.AppendFormat(format, args);
		}

		/// <summary>Returns a partial list of properties in the <see cref="T:System.Diagnostics.FileVersionInfo" /> and their values.</summary>
		/// <returns>A list of the following properties in this class and their values: 
		///     <see cref="P:System.Diagnostics.FileVersionInfo.FileName" />, <see cref="P:System.Diagnostics.FileVersionInfo.InternalName" />, <see cref="P:System.Diagnostics.FileVersionInfo.OriginalFilename" />, <see cref="P:System.Diagnostics.FileVersionInfo.FileVersion" />, <see cref="P:System.Diagnostics.FileVersionInfo.FileDescription" />, <see cref="P:System.Diagnostics.FileVersionInfo.ProductName" />, <see cref="P:System.Diagnostics.FileVersionInfo.ProductVersion" />, <see cref="P:System.Diagnostics.FileVersionInfo.IsDebug" />, <see cref="P:System.Diagnostics.FileVersionInfo.IsPatched" />, <see cref="P:System.Diagnostics.FileVersionInfo.IsPreRelease" />, <see cref="P:System.Diagnostics.FileVersionInfo.IsPrivateBuild" />, <see cref="P:System.Diagnostics.FileVersionInfo.IsSpecialBuild" />,
		///     <see cref="P:System.Diagnostics.FileVersionInfo.Language" />.If the file did not contain version information, this list will contain only the name of the requested file. Boolean values will be <see langword="false" />, and all other entries will be <see langword="null" />.</returns>
		// Token: 0x060008ED RID: 2285 RVA: 0x00029C34 File Offset: 0x00027E34
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			FileVersionInfo.AppendFormat(stringBuilder, "File:             {0}{1}", new object[]
			{
				this.FileName,
				Environment.NewLine
			});
			FileVersionInfo.AppendFormat(stringBuilder, "InternalName:     {0}{1}", new object[]
			{
				this.internalname,
				Environment.NewLine
			});
			FileVersionInfo.AppendFormat(stringBuilder, "OriginalFilename: {0}{1}", new object[]
			{
				this.originalfilename,
				Environment.NewLine
			});
			FileVersionInfo.AppendFormat(stringBuilder, "FileVersion:      {0}{1}", new object[]
			{
				this.fileversion,
				Environment.NewLine
			});
			FileVersionInfo.AppendFormat(stringBuilder, "FileDescription:  {0}{1}", new object[]
			{
				this.filedescription,
				Environment.NewLine
			});
			FileVersionInfo.AppendFormat(stringBuilder, "Product:          {0}{1}", new object[]
			{
				this.productname,
				Environment.NewLine
			});
			FileVersionInfo.AppendFormat(stringBuilder, "ProductVersion:   {0}{1}", new object[]
			{
				this.productversion,
				Environment.NewLine
			});
			FileVersionInfo.AppendFormat(stringBuilder, "Debug:            {0}{1}", new object[]
			{
				this.isdebug,
				Environment.NewLine
			});
			FileVersionInfo.AppendFormat(stringBuilder, "Patched:          {0}{1}", new object[]
			{
				this.ispatched,
				Environment.NewLine
			});
			FileVersionInfo.AppendFormat(stringBuilder, "PreRelease:       {0}{1}", new object[]
			{
				this.isprerelease,
				Environment.NewLine
			});
			FileVersionInfo.AppendFormat(stringBuilder, "PrivateBuild:     {0}{1}", new object[]
			{
				this.isprivatebuild,
				Environment.NewLine
			});
			FileVersionInfo.AppendFormat(stringBuilder, "SpecialBuild:     {0}{1}", new object[]
			{
				this.isspecialbuild,
				Environment.NewLine
			});
			FileVersionInfo.AppendFormat(stringBuilder, "Language          {0}{1}", new object[]
			{
				this.language,
				Environment.NewLine
			});
			return stringBuilder.ToString();
		}

		// Token: 0x04000BD2 RID: 3026
		private string comments;

		// Token: 0x04000BD3 RID: 3027
		private string companyname;

		// Token: 0x04000BD4 RID: 3028
		private string filedescription;

		// Token: 0x04000BD5 RID: 3029
		private string filename;

		// Token: 0x04000BD6 RID: 3030
		private string fileversion;

		// Token: 0x04000BD7 RID: 3031
		private string internalname;

		// Token: 0x04000BD8 RID: 3032
		private string language;

		// Token: 0x04000BD9 RID: 3033
		private string legalcopyright;

		// Token: 0x04000BDA RID: 3034
		private string legaltrademarks;

		// Token: 0x04000BDB RID: 3035
		private string originalfilename;

		// Token: 0x04000BDC RID: 3036
		private string privatebuild;

		// Token: 0x04000BDD RID: 3037
		private string productname;

		// Token: 0x04000BDE RID: 3038
		private string productversion;

		// Token: 0x04000BDF RID: 3039
		private string specialbuild;

		// Token: 0x04000BE0 RID: 3040
		private bool isdebug;

		// Token: 0x04000BE1 RID: 3041
		private bool ispatched;

		// Token: 0x04000BE2 RID: 3042
		private bool isprerelease;

		// Token: 0x04000BE3 RID: 3043
		private bool isprivatebuild;

		// Token: 0x04000BE4 RID: 3044
		private bool isspecialbuild;

		// Token: 0x04000BE5 RID: 3045
		private int filemajorpart;

		// Token: 0x04000BE6 RID: 3046
		private int fileminorpart;

		// Token: 0x04000BE7 RID: 3047
		private int filebuildpart;

		// Token: 0x04000BE8 RID: 3048
		private int fileprivatepart;

		// Token: 0x04000BE9 RID: 3049
		private int productmajorpart;

		// Token: 0x04000BEA RID: 3050
		private int productminorpart;

		// Token: 0x04000BEB RID: 3051
		private int productbuildpart;

		// Token: 0x04000BEC RID: 3052
		private int productprivatepart;
	}
}
