﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Encapsulates a single record in the event log. This class cannot be inherited.</summary>
	// Token: 0x0200061A RID: 1562
	[ToolboxItem(false)]
	[DesignTimeVisible(false)]
	[Serializable]
	public sealed class EventLogEntry : Component, ISerializable
	{
		// Token: 0x06003247 RID: 12871 RVA: 0x000092E2 File Offset: 0x000074E2
		internal EventLogEntry()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the text associated with the <see cref="P:System.Diagnostics.EventLogEntry.CategoryNumber" /> property for this entry.</summary>
		/// <returns>The application-specific category text.</returns>
		/// <exception cref="T:System.Exception">The space could not be allocated for one of the insertion strings associated with the category. </exception>
		// Token: 0x17000C7D RID: 3197
		// (get) Token: 0x06003248 RID: 12872 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string Category
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the category number of the event log entry.</summary>
		/// <returns>The application-specific category number for this entry.</returns>
		// Token: 0x17000C7E RID: 3198
		// (get) Token: 0x06003249 RID: 12873 RVA: 0x000A4774 File Offset: 0x000A2974
		public short CategoryNumber
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the binary data associated with the entry.</summary>
		/// <returns>An array of bytes that holds the binary data associated with the entry.</returns>
		// Token: 0x17000C7F RID: 3199
		// (get) Token: 0x0600324A RID: 12874 RVA: 0x00043C3C File Offset: 0x00041E3C
		public byte[] Data
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the event type of this entry.</summary>
		/// <returns>The event type that is associated with the entry in the event log.</returns>
		// Token: 0x17000C80 RID: 3200
		// (get) Token: 0x0600324B RID: 12875 RVA: 0x000A4790 File Offset: 0x000A2990
		public EventLogEntryType EntryType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (EventLogEntryType)0;
			}
		}

		/// <summary>Gets the application-specific event identifier for the current event entry.</summary>
		/// <returns>The application-specific identifier for the event message.</returns>
		// Token: 0x17000C81 RID: 3201
		// (get) Token: 0x0600324C RID: 12876 RVA: 0x000A47AC File Offset: 0x000A29AC
		public int EventID
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the index of this entry in the event log.</summary>
		/// <returns>The index of this entry in the event log.</returns>
		// Token: 0x17000C82 RID: 3202
		// (get) Token: 0x0600324D RID: 12877 RVA: 0x000A47C8 File Offset: 0x000A29C8
		public int Index
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Gets the resource identifier that designates the message text of the event entry.</summary>
		/// <returns>A resource identifier that corresponds to a string definition in the message resource file of the event source.</returns>
		// Token: 0x17000C83 RID: 3203
		// (get) Token: 0x0600324E RID: 12878 RVA: 0x000A47E4 File Offset: 0x000A29E4
		public long InstanceId
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0L;
			}
		}

		/// <summary>Gets the name of the computer on which this entry was generated.</summary>
		/// <returns>The name of the computer that contains the event log.</returns>
		// Token: 0x17000C84 RID: 3204
		// (get) Token: 0x0600324F RID: 12879 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string MachineName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the localized message associated with this event entry.</summary>
		/// <returns>The formatted, localized text for the message. This includes associated replacement strings.</returns>
		/// <exception cref="T:System.Exception">The space could not be allocated for one of the insertion strings associated with the message. </exception>
		// Token: 0x17000C85 RID: 3205
		// (get) Token: 0x06003250 RID: 12880 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string Message
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the replacement strings associated with the event log entry.</summary>
		/// <returns>An array that holds the replacement strings stored in the event entry.</returns>
		// Token: 0x17000C86 RID: 3206
		// (get) Token: 0x06003251 RID: 12881 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string[] ReplacementStrings
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the name of the application that generated this event.</summary>
		/// <returns>The name registered with the event log as the source of this event.</returns>
		// Token: 0x17000C87 RID: 3207
		// (get) Token: 0x06003252 RID: 12882 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string Source
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the local time at which this event was generated.</summary>
		/// <returns>The local time at which this event was generated.</returns>
		// Token: 0x17000C88 RID: 3208
		// (get) Token: 0x06003253 RID: 12883 RVA: 0x000A4800 File Offset: 0x000A2A00
		public DateTime TimeGenerated
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(DateTime);
			}
		}

		/// <summary>Gets the local time at which this event was written to the log.</summary>
		/// <returns>The local time at which this event was written to the log.</returns>
		// Token: 0x17000C89 RID: 3209
		// (get) Token: 0x06003254 RID: 12884 RVA: 0x000A481C File Offset: 0x000A2A1C
		public DateTime TimeWritten
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(DateTime);
			}
		}

		/// <summary>Gets the name of the user who is responsible for this event.</summary>
		/// <returns>The security identifier (SID) that uniquely identifies a user or group.</returns>
		/// <exception cref="T:System.SystemException">Account information could not be obtained for the user's SID. </exception>
		// Token: 0x17000C8A RID: 3210
		// (get) Token: 0x06003255 RID: 12885 RVA: 0x00043C3C File Offset: 0x00041E3C
		public string UserName
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Performs a comparison between two event log entries.</summary>
		/// <param name="otherEntry">The <see cref="T:System.Diagnostics.EventLogEntry" /> to compare. </param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Diagnostics.EventLogEntry" /> objects are identical; otherwise, <see langword="false" />.</returns>
		// Token: 0x06003256 RID: 12886 RVA: 0x000A4838 File Offset: 0x000A2A38
		public bool Equals(EventLogEntry otherEntry)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with the data needed to serialize the target object.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data. </param>
		/// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this serialization. </param>
		// Token: 0x06003257 RID: 12887 RVA: 0x000092E2 File Offset: 0x000074E2
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
