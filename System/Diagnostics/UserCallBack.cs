﻿using System;

namespace System.Diagnostics
{
	// Token: 0x02000102 RID: 258
	// (Invoke) Token: 0x060007D5 RID: 2005
	internal delegate void UserCallBack(string data);
}
