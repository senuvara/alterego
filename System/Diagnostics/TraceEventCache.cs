﻿using System;
using System.Collections;
using System.Globalization;
using System.Threading;

namespace System.Diagnostics
{
	/// <summary>Provides trace event data specific to a thread and a process.</summary>
	// Token: 0x020000F7 RID: 247
	public class TraceEventCache
	{
		// Token: 0x1700012F RID: 303
		// (get) Token: 0x0600072D RID: 1837 RVA: 0x00023D6C File Offset: 0x00021F6C
		internal Guid ActivityId
		{
			get
			{
				return Trace.CorrelationManager.ActivityId;
			}
		}

		/// <summary>Gets the call stack for the current thread.</summary>
		/// <returns>A string containing stack trace information. This value can be an empty string ("").</returns>
		// Token: 0x17000130 RID: 304
		// (get) Token: 0x0600072E RID: 1838 RVA: 0x00023D78 File Offset: 0x00021F78
		public string Callstack
		{
			get
			{
				if (this.stackTrace == null)
				{
					this.stackTrace = Environment.StackTrace;
				}
				return this.stackTrace;
			}
		}

		/// <summary>Gets the correlation data, contained in a stack. </summary>
		/// <returns>A <see cref="T:System.Collections.Stack" /> containing correlation data.</returns>
		// Token: 0x17000131 RID: 305
		// (get) Token: 0x0600072F RID: 1839 RVA: 0x00023D93 File Offset: 0x00021F93
		public Stack LogicalOperationStack
		{
			get
			{
				return Trace.CorrelationManager.LogicalOperationStack;
			}
		}

		/// <summary>Gets the date and time at which the event trace occurred.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> structure whose value is a date and time expressed in Coordinated Universal Time (UTC).</returns>
		// Token: 0x17000132 RID: 306
		// (get) Token: 0x06000730 RID: 1840 RVA: 0x00023D9F File Offset: 0x00021F9F
		public DateTime DateTime
		{
			get
			{
				if (this.dateTime == DateTime.MinValue)
				{
					this.dateTime = DateTime.UtcNow;
				}
				return this.dateTime;
			}
		}

		/// <summary>Gets the unique identifier of the current process.</summary>
		/// <returns>The system-generated unique identifier of the current process.</returns>
		// Token: 0x17000133 RID: 307
		// (get) Token: 0x06000731 RID: 1841 RVA: 0x00023DC4 File Offset: 0x00021FC4
		public int ProcessId
		{
			get
			{
				return TraceEventCache.GetProcessId();
			}
		}

		/// <summary>Gets a unique identifier for the current managed thread.  </summary>
		/// <returns>A string that represents a unique integer identifier for this managed thread.</returns>
		// Token: 0x17000134 RID: 308
		// (get) Token: 0x06000732 RID: 1842 RVA: 0x00023DCC File Offset: 0x00021FCC
		public string ThreadId
		{
			get
			{
				return TraceEventCache.GetThreadId().ToString(CultureInfo.InvariantCulture);
			}
		}

		/// <summary>Gets the current number of ticks in the timer mechanism.</summary>
		/// <returns>The tick counter value of the underlying timer mechanism.</returns>
		// Token: 0x17000135 RID: 309
		// (get) Token: 0x06000733 RID: 1843 RVA: 0x00023DEB File Offset: 0x00021FEB
		public long Timestamp
		{
			get
			{
				if (this.timeStamp == -1L)
				{
					this.timeStamp = Stopwatch.GetTimestamp();
				}
				return this.timeStamp;
			}
		}

		// Token: 0x06000734 RID: 1844 RVA: 0x00023E08 File Offset: 0x00022008
		private static void InitProcessInfo()
		{
			if (TraceEventCache.processName == null)
			{
				Process currentProcess = Process.GetCurrentProcess();
				try
				{
					TraceEventCache.processId = currentProcess.Id;
					TraceEventCache.processName = currentProcess.ProcessName;
				}
				finally
				{
					currentProcess.Dispose();
				}
			}
		}

		// Token: 0x06000735 RID: 1845 RVA: 0x00023E58 File Offset: 0x00022058
		internal static int GetProcessId()
		{
			TraceEventCache.InitProcessInfo();
			return TraceEventCache.processId;
		}

		// Token: 0x06000736 RID: 1846 RVA: 0x00023E66 File Offset: 0x00022066
		internal static string GetProcessName()
		{
			TraceEventCache.InitProcessInfo();
			return TraceEventCache.processName;
		}

		// Token: 0x06000737 RID: 1847 RVA: 0x00023E74 File Offset: 0x00022074
		internal static int GetThreadId()
		{
			return Thread.CurrentThread.ManagedThreadId;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.TraceEventCache" /> class. </summary>
		// Token: 0x06000738 RID: 1848 RVA: 0x00023E80 File Offset: 0x00022080
		public TraceEventCache()
		{
		}

		// Token: 0x04000B26 RID: 2854
		private static volatile int processId;

		// Token: 0x04000B27 RID: 2855
		private static volatile string processName;

		// Token: 0x04000B28 RID: 2856
		private long timeStamp = -1L;

		// Token: 0x04000B29 RID: 2857
		private DateTime dateTime = DateTime.MinValue;

		// Token: 0x04000B2A RID: 2858
		private string stackTrace;
	}
}
