﻿using System;
using System.Collections;
using Unity;

namespace System.Diagnostics
{
	/// <summary>Provides a strongly typed collection of <see cref="T:System.Diagnostics.InstanceDataCollection" /> objects.</summary>
	// Token: 0x02000671 RID: 1649
	public class InstanceDataCollectionCollection : DictionaryBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.InstanceDataCollectionCollection" /> class.</summary>
		// Token: 0x0600349C RID: 13468 RVA: 0x000092E2 File Offset: 0x000074E2
		[Obsolete("This constructor has been deprecated.  Please use System.Diagnostics.PerformanceCounterCategory.ReadCategory() to get an instance of this collection instead.  http://go.microsoft.com/fwlink/?linkid=14202")]
		public InstanceDataCollectionCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the instance data for the specified counter.</summary>
		/// <param name="counterName">The name of the performance counter. </param>
		/// <returns>An <see cref="T:System.Diagnostics.InstanceDataCollection" /> item, by which the <see cref="T:System.Diagnostics.InstanceDataCollectionCollection" /> object is indexed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="counterName" /> parameter is <see langword="null" />. </exception>
		// Token: 0x17000D59 RID: 3417
		public InstanceDataCollection this[string counterName]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the object and counter registry keys for the objects associated with this instance data collection.</summary>
		/// <returns>An <see cref="T:System.Collections.ICollection" /> that represents a set of object-specific registry keys.</returns>
		// Token: 0x17000D5A RID: 3418
		// (get) Token: 0x0600349E RID: 13470 RVA: 0x00043C3C File Offset: 0x00041E3C
		public ICollection Keys
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the instance data values that comprise the collection of instances for the counter.</summary>
		/// <returns>An <see cref="T:System.Collections.ICollection" /> that represents the counter's instances and their associated data values.</returns>
		// Token: 0x17000D5B RID: 3419
		// (get) Token: 0x0600349F RID: 13471 RVA: 0x00043C3C File Offset: 0x00041E3C
		public ICollection Values
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Determines whether an instance data collection for the specified counter (identified by one of the indexed <see cref="T:System.Diagnostics.InstanceDataCollection" /> objects) exists in the collection.</summary>
		/// <param name="counterName">The name of the performance counter. </param>
		/// <returns>
		///     <see langword="true" /> if an instance data collection containing the specified counter exists in the collection; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="counterName" /> parameter is <see langword="null" />. </exception>
		// Token: 0x060034A0 RID: 13472 RVA: 0x000A56C4 File Offset: 0x000A38C4
		public bool Contains(string counterName)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Copies an array of <see cref="T:System.Diagnostics.InstanceDataCollection" /> instances to the collection, at the specified index.</summary>
		/// <param name="counters">An array of <see cref="T:System.Diagnostics.InstanceDataCollection" /> instances (identified by the counters they contain) to add to the collection. </param>
		/// <param name="index">The location at which to add the new instances. </param>
		// Token: 0x060034A1 RID: 13473 RVA: 0x000092E2 File Offset: 0x000074E2
		public void CopyTo(InstanceDataCollection[] counters, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
